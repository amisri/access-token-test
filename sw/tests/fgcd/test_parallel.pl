#!/usr/bin/perl

use Test::Utils;
use File::Basename;
use File::Spec;
use File::Temp;
use strict;

my %params = (devices=>undef, script=>undef, protocol=>'CMW');
%params = parse_opts %params;

authenticate($params{authenticate});

# Check the script

ok(-e $params{script}, "File '$params{script}' exists");
ok(-x $params{script}, "File '$params{script}' is excutable");

# Retrieve matching devices

my %devices;

foreach my $device_expr (split(',',$params{devices}))
{
    foreach my $device (get_devices($device_expr))
    {
        $devices{$device} = undef; 
    }
}

ok(scalar(keys %devices), scalar(keys %devices) . " devices matching '$params{devices}'");

# Remove offline devices  

for my $device (sort keys %devices)
{
    eval{ get($device,"TIME.NOW") };
    
    if ($@)
    {
        delete $devices{$device}; 
    }
    
    
}

ok(scalar(keys %devices), scalar(keys %devices) . " online devices matching '$params{devices}'");

# Test devices

my %files;

for my $device (sort keys %devices)
{
    # Create temporal file
    
    my $tmp = File::Temp->new();

    $files{$device} = $tmp;

    # create child process
    
    my $cmd = "$params{script} --device=$device --protocol=$params{protocol} &> " . $tmp->filename;
    
    my $pid = fork() or exec($cmd);
    
    ok(defined($pid), "Process spawned: $params{script} --device=$device --protocol=$params{protocol}...");
         
    $devices{$device} = $pid;
}

# Wait for childs

for my $device (sort keys %devices)
{
    waitpid($devices{$device}, 0);
    
    if (($? >>8 ) == 0)
    {
        ok(1,"$params{script} --device=$device --protocol=$params{protocol}"); 
    }
    else
    {
        # Print error contents from child
        
        my $content = "ERROR: $params{script} --device=$device --protocol=$params{protocol} (exit = " . ($? >>8) . "):\n";
        
        open(my $outFile, '<', $files{$device}->filename) or die "ERROR: Unable to open temporal file '" . $files{$device}->filename . "': $!";
        
        while ( my $line = <$outFile> )
        {
            $content = $content . "\t$line";
        }
        
        die $content; 
        
    }
    
}

exit(0);
