#!/usr/bin/perl

use Test::Utils;
use strict;

my %params = (devices=>undef, protocol=>'CMW');
%params = parse_opts %params;

authenticate($params{authenticate});

# Retrieve matching devices

my %devices;

foreach my $device (get_devices($params{devices}))
{
    $devices{$device} = undef; 
}

ok(scalar(keys %devices), scalar(keys %devices) . " devices matching '$params{devices}'");

# Remove offline devices  

for my $device (sort keys %devices)
{
    eval{ get($device,"TIME.NOW") };
    
    if ($@)
    {
        delete $devices{$device}; 
    }
    
    
}

ok(scalar(keys %devices), scalar(keys %devices) . " online devices matching '$params{devices}'");

# Create device list

my $device_list = join(',', keys %devices);

# Create command

my $cmd = "/acc/local/Linux/bin/rda-subscribe -r location -p SUB -t 10000 -d \'$device_list\'";

my $output = `$cmd`;

ok( $? == 0, "Execution of command: $cmd");
ok( $output =~ /ST_WARNINGS/, "SUB contains ST_WARNINGS field");
ok( $output =~ /ST_FAULTS/, "SUB contains ST_FAULTS");


exit(0);
