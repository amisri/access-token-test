#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;

sub max ($$) { $_[$_[0] < $_[1]] }

sub min ($$) { $_[$_[0] > $_[1]] }

my %params = (device=>undef, config=>'', protocol=>'TCP');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# For the time being class 62 and 63 are not coverred for this test

if(get($d,"DEVICE.CLASS_ID") == 63 || get($d,"DEVICE.CLASS_ID") == 62)
{
    exit 0;
}

# Reset

set($d,"PC","OFF");
set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") =~ /OFF/ } );

# Reset FGC

set($d,"PC","OFF");
set($d,"PC","OFF");

# OFF -> SB -> IDLE --> OFF

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'SB');
wait_until( sub { get($d,"STATE.PC") eq 'ON_STANDBY' } , 120 );
set($d, 'PC', 'IDLE');
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } , 120 );

# Check that we can set the current to some value (+/- 0.1%)

my $pos = get($d, 'LIMITS.I.POS[0]') * 0.01;
my $minimum = get($d, 'LIMITS.I.MIN[0]');
my $ref = ($pos - $minimum)*0.1 + $minimum;

set($d, "REF","now,$ref");

wait_until( sub { abs(get($d,"MEAS.I") - $ref) < $pos*0.001  }, 120 );
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );


set($d, 'PC', 'OFF');
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

ok(1, "OFF -> SB -> IDLE --> (change reference from $minimum to $ref +/- 0.1%) --> OFF");

# OFF -> SB -> IDLE --> SLOW_ABORT --> IDLE --> SLOW_ABORT --> ON_STANBY --> SLOW_ABORT --> OFF

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'IDLE');
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } , 120);
set($d, "REF","now,$ref");

wait_until( sub { abs(get($d,"MEAS.I") - $ref) < $pos*0.001  }, 120 );
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' });

set($d, 'PC', 'SLOW_ABORT');
set($d, 'PC', 'IDLE');
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

set($d, 'PC', 'SLOW_ABORT');

wait_until( sub { get($d,"STATE.PC") eq 'OFF' } , 120 );

ok(1,'OFF -> SB -> IDLE --> SLOW_ABORT --> --> IDLE --> SLOW_ABORT --> ON_STANBY --> SLOW_ABORT --> OFF');

# OFF -> IDLE --> FLT_OFF --> POST_MORTEM --> OFF

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'IDLE');
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
set($d, 'FGC.FAULTS', 'FGC_STATE');
wait_until( sub { get($d,"STATE.PC") eq 'FLT_OFF' } );
wait_until( sub { get($d,"STATUS.FAULTS") =~ /FGC_STATE/ } );

# The end of the post mortem should not stop the test (in case the front-end disables the post-mortem)

eval{ wait_until( sub { get($d,"STATUS.ST_UNLATCHED") !~ /POST_MORTEM/ } , 120); };

eval{ set($d, 'LOG', 'RESET'); };

ok(sub { get($d,"STATUS.ST_UNLATCHED") !~ /POST_MORTEM/  } );

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

ok(1,'OFF -> IDLE --> FLT_OFF --> POST_MORTEM --> OFF');

# 

# OFF -> SB -> DIRECT --> OFF (class 63)

if (get($d, 'DEVICE.CLASS_ID') == 63)
{
    set($d,"PC","OFF");
    wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
    set($d, 'PC', 'SB');
    wait_until( sub { get($d,"STATE.PC") eq 'ON_STANDBY' } , 120);
    set($d, 'PC', 'DIRECT');
    wait_until( sub { get($d,"STATE.PC") eq 'DIRECT' } , 120);

    # Check that we can set the current to some value (+/- 0.1%)

    set($d, 'REF.CCV.VALUE', "$minimum");

    wait_until( sub { abs(get($d,"MEAS.I") - $minimum) < $pos*0.001  } , 120 );

    set($d, "REF.CCV.VALUE","$ref");

    wait_until( sub { abs(get($d,"MEAS.I") - $ref) < $pos*0.001  } , 120 );

    
 
    set($d, 'PC', 'OFF');
    wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
    
    ok(1,'OFF -> SB -> DIRECT --> (change reference +/- 0.1%) --> OFF');
     
}

# OFF --> CY --> OFF

if(get($d, 'DEVICE.CLASS_ID') == 91 || get($d, 'DEVICE.CLASS_ID') == 94 || get($d, 'DEVICE.CLASS_ID') == 63)
{
    # if device is in building 866 then set a trapezoid table for each user
    
    if ($d =~ /(RFNA|RFM|MUGEF)\.866\./)
    {
        for my $i (1 .. 32)
        {
            set($d,"REF.FUNC.REG_MODE($i)","I");
            set($d,"REF.TABLE.FUNCTION($i)","0|$minimum,0.2|$minimum,0.4|$pos,0.6|$pos,,1.0|$minimum");
        }
    }

    set($d,"PC","OFF");
    wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
    set($d, 'PC', 'CY');
    wait_until( sub { get($d,"STATE.PC") =~ /CYCLING|ECONOMY/ } );
    
    # sleep a bit to let the converter cycle a bit
    
    sleep 10;
        
    set($d, 'PC', 'OFF');
    wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

    ok(1,'OFF --> CY --> OFF');

    # OFF --> CY --> SLOW_ABORT

    set($d,"PC","OFF");
    wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
    set($d, 'PC', 'CY');
    wait_until( sub { get($d,"STATE.PC") =~ /CYCLING|ECONOMY/ } );
    set($d, 'PC', 'SLOW_ABORT');
    wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

    ok(1,'OFF -> CY -> SLOW_ABORT');
}
