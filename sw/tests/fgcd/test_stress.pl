#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;

sub max ($$) { $_[$_[0] < $_[1]] }

sub min ($$) { $_[$_[0] > $_[1]] }

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# For the time being class 62 and 63 are not coverred for this test

if(get($d,"DEVICE.PLATFORM_ID") == 60)
{
    exit 0;
}

# Retrieve minimum and maximum

my $pos = get($d, 'LIMITS.I.POS[0]') * 0.1;
my $minimum = get($d, 'LIMITS.I.MIN[0]');
my $rate = get($d, 'LIMITS.I.RATE[0]')*0.1;

# Stress Get small commands (5 min)

my $N     = 100;
my $count = 1;

while($count != $N)
{
    get($d,"TIME.NOW"); 
    print "$d GET TIME.NOW: $count/$N\r";
    ++$count;
}

ok(1,"$d GET TIME.NOW: $N requests");

# Stress Set small commands (5 min)

$count = 1;

while($count != $N)
{
    set($d,"TEST.FLOAT",(rand(2)-1)*rand(10**30)); 
    print "$d SET TEST.FLOAT: $count/$N\r";
    ++$count;
}

ok(1,"$d SET TEST.FLOAT: $N requests");

# Stress test big GET commands

$count = 1;

while($count != $N)
{
    get($d,"DEVICE"); 
    get($d,"STATUS"); 
    print "$d GET REF, DEVICE, and STATUS: $count/$N\r";
    ++$count;
}


ok(1,"$d GET REF, DEVICE, and STATUS: $N requests");


# Stress Set big commands (5 min)

$N = 10;

# FGC to OFF (just in case)

eval{set($d,"MODE.PC","OFF");};
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
eval{set($d,"MODE.PC","ON_STANDBY");};
wait_until( sub { get($d,"STATE.PC") eq 'ON_STANDBY' } , 60*5);
eval{set($d,"MODE.PC","IDLE");};
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

# Create table

my @values;

my $value = $minimum;
push(@values, "0|$value");
for(my $i = 1; $i < 1000; $i++)
{
    $value += (rand(0.9) - 0.45)*$rate*0.01;
    $value  = min($pos,max($minimum,$value));
    my $time  = $i*0.1;

    push(@values, "$time|$value");
}
$value = join(',', @values);
    
$count = 1;

while($count != $N)
{
    set($d,"REF.TABLE.FUNCTION",$value); 

    wait_until( sub { get($d,"STATE.PC") eq 'ARMED' } );
    eval{set($d,"MODE.PC","IDLE");};
    wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

    print "$d SET REF.TABLE.FUNCTION: $count/$N\r";
    ++$count;
}

ok(1,"$d SET REF.TABLE.FUNCTION: $N requests");

# Stress Get big command (5 min)

$count = 1;

while($count != $N)
{
    get($d,"REF.TABLE.FUNCTION"); 
    print "$d GET REF.TABLE.FUNCTION: $count/$N\r";
    ++$count;
}

ok(1,"$d GET TEST.TABLE.FUNCTION: $N requests");

# Stop 

eval{set($d,"MODE.PC","OFF");};
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

# EOF
