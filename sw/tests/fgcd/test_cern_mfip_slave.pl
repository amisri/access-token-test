#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;

sub max ($$) { $_[$_[0] < $_[1]] }

sub min ($$) { $_[$_[0] > $_[1]] }

my %params = (device=>undef, config=>'', protocol=>'tcpgitk');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Reset fieldbus counters

get($d,"FIELDBUS ZERO");

# Check that we are testing only FGC2s, OFF, and SIMULATION

ok(sub { get($d,"DEVICE.CLASS_ID") == 51 }, "DEVICE.CLASS_ID == 51");
ok(get($d,"STATE.PC") =~ /OFF/, "STATE.PC =~ OFF" );

# Stress Get small commands (5 min)

my $N     = 100;
my $count = 1;

while($count != $N)
{
    get($d,"TIME.NOW"); 
    print "$d GET TIME.NOW: $count/$N\r";
    ++$count;
}

ok(1,"$d GET TIME.NOW: $N requests");

# Stress TEST.FLOAT

$count = 1;

while($count != $N)
{
    my $number = (rand(2)-1)*rand(10**30);
    set($d,"TEST.FLOAT",$number); 
    my $response = get($d,"TEST.FLOAT");

    if( abs($number - $response) < abs($number/100000)) 
    {
        print "$d TEST.FLOAT SET ~ GET: $count/$N\r";
    } 
    else
    {
        ok( $number == $response, "TEST.FLOAT SET ~ GET ($number ~ $response)");
    }
    ++$count;
}

ok(1,"$d SET+GET TEST.FLOAT: $N requests");

# Stress test TEST.FLOAT[16]

$count = 1;

while($count != $N)
{
    my @numbers;
    for (my $i=0; $i < 16; $i++) 
    {
        push(@numbers, (rand(2)-1)*rand(10**30) );
    }
    
    my $request = join(",",@numbers);
    set($d,"TEST.FLOAT", $request); 
    my $response = get($d,"TEST.FLOAT"); 

    my $success = 1;
    my @rnumbers = split(",",$response);
    for(my $i=0; $i < 16; ++$i) 
    {
        if( abs($numbers[$i] - $rnumbers[$i]) > abs($numbers[$i]/100000)) 
        {
            $success = 0;
            last;
        }
    }

    if($success) 
    {
        print "$d TEST.FLOAT[16] SET ~ GET: $count/$N\r";
    } 
    else
    {
        ok( "$request" eq "$response", "TEST.FLOAT[16] SET ~ GET ($request ~ $response)");
    }
    ++$count;
}

ok(1,"$d SET+GET TEST.FLOAT[16]: $N requests");

# Stress test big GET commands

$count = 1;

while($count != $N)
{
    if(get($d,"DEVICE.NAME") ne $d)
    {
        ok(sub { get($d,"DEVICE.NAME") eq $d }, "DEVICE.NAME == $d");
    }

    get($d,"DEVICE"); 
    get($d,"STATUS"); 
    
    print "$d GET DEVICE and STATUS, and check DEVICE.NAME: $count/$N\r";
    ++$count;
}


ok(1,"$d GET DEVICE and STATUS: $N requests");

# Check fieldbus errors

my $fieldbus = get($d,"FIELDBUS.WFIP");
$fieldbus =~ s/\n/ /g;

ok( $fieldbus =~ /V7MISSED:0.*V7NOACCESS:0.*V7REPEATED:0.*CLASH_INT_SRC:0.*RDMISSED:0.*ERR_ART:0.*MSGSNDTIMOUT:0/ , "FIELDBUS property without errors: $fieldbus");

# EOF
