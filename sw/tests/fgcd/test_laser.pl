#!/usr/bin/perl

use Test::Utils;
use strict;

my %params = (device=>undef, devices=>undef, protocol=>'CMW');
%params = parse_opts %params;

authenticate($params{authenticate});

# Retrieve matching devices

my $device = uc $params{device};

ok(get($device,"TIME.NOW"), "FGCD '$device' is online");

# Check LASER listener script (smoke test)

my $cmd = "/user/alaser/laser-lw2/tools/bin/strlisten.sh -s $device -p -t 10000";
my $output = `$cmd`;

ok( $output =~ /INFO.*$device/, "Alarm source '$device' is online");


exit(0);
