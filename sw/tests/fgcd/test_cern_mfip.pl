#!/usr/bin/perl

use Test::Utils;
use File::Basename;
use File::Spec;
use File::Temp;
use strict;

# Default Point 6 and 7 FECs and devices to be tested with the CERN MasterFIP

my %params = (fecs=>"CFC-SR6-RL6A,CFC-SR6-RL6B,CFC-SR6-RR6C,CFC-SR6-RR6D,CFC-SR7-RL7A,CFC-SR7-RL7B,CFC-SR7-RR7C,CFC-SR7-RR7D", 
			  devices=>"RPTM.SR6.RMSD.LR6B1,RPTM.SR6.RMSD.LR6B2,RFM.UA63.SPARE.1,RFM.UA63.SPARE.2,RPLB.UA63.RCBYH4.L6B2,RPLB.UA63.RCBYV4.L6B1,RPHH.UA63.RQ4.L6B2,RPHH.UA63.RQ4.L6B1,RPHH.UA63.RQ5.L6B1,RPHH.UA63.RQ5.L6B2,RPTE.UA63.RB.A56,RPHE.UA63.RQF.A56,RPHE.UA63.RQD.A56,RPMBB.UA63.RSD2.A56B1,RPMBB.UA63.RSD2.A56B2,RPMBB.UA63.RSD1.A56B1,RPMBB.UA63.RSD1.A56B2,RPMBB.UA63.RSF2.A56B1,RPMBB.UA63.RSF2.A56B2,RPMBB.UA63.RSF1.A56B1,RPMBB.UA63.RSF1.A56B2,RPMBB.UA63.RCD.A56B1,RPMBB.UA63.RCD.A56B2,RPMBB.UA63.RCS.A56B1,RPMBB.UA63.RCS.A56B2,RPMBB.UA63.RQTD.A56B1,RPMBB.UA63.RQTD.A56B2,RFM.SR6.SPARE.1,RPMBB.UA63.RQTF.A56B1,RPMBB.UA63.RQTF.A56B2,RPMBA.UA63.RQS.L6B1,RPMBA.UA63.RQT13.L6B1,RPMBA.UA63.RQT13.L6B2,RPMBA.UA63.RQT12.L6B1,RPMBA.UA63.RQT12.L6B2,RPMBA.UA63.RQTL11.L6B1,RPMBA.UA63.RQTL11.L6B2,RPLB.UA63.RCO.A56B1,RPLB.UA63.RCO.A56B2,RPLB.UA63.RCBCH8.L6B2,RPLB.UA63.RCBCV8.L6B1,RPLB.UA63.RCBCH10.L6B2,RPLB.UA63.RCBCV10.L6B1,RPLB.UA63.RCBCV9.L6B2,RPLB.UA63.RCBCH9.L6B1,RPLB.UA63.RCBYV5.L6B2,RPLB.UA63.RCBYH5.L6B1,RPHGA.UJ63.RQ8.L6B2,RPHGA.UJ63.RQ8.L6B1,RPHGA.UJ63.RQ9.L6B1,RPHGA.UJ63.RQ9.L6B2,RPHGA.UJ63.RQ10.L6B2,RPHGA.UJ63.RQ10.L6B1,RPLB.UA67.RCBYH4.R6B1,RPLB.UA67.RCBYV4.R6B2,RPHH.UA67.RQ4.R6B2,RPHH.UA67.RQ4.R6B1,RPHH.UA67.RQ5.R6B1,RPHH.UA67.RQ5.R6B2,RPTE.UA67.RB.A67,RPHE.UA67.RQF.A67,RPHE.UA67.RQD.A67,RPMBB.UA67.RCD.A67B2,RPMBB.UA67.RCD.A67B1,RPMBB.UA67.RCS.A67B2,RPMBB.UA67.RCS.A67B1,RPMBB.UA67.RSD1.A67B2,RPMBB.UA67.RSD1.A67B1,RPMBB.UA67.RSF1.A67B2,RPMBB.UA67.RSF1.A67B1,RPMBB.UA67.RSD2.A67B2,RPMBB.UA67.RSD2.A67B1,RPMBB.UA67.RSF2.A67B2,RPMBB.UA67.RSF2.A67B1,RPMBB.UA67.RQTD.A67B2,RPMBB.UA67.RQTD.A67B1,RPMBB.UA67.RQTF.A67B2,RPMBB.UA67.RQTF.A67B1,RPMBA.UA67.RQS.R6B2,RPMBA.UA67.RQTL11.R6B2,RPMBA.UA67.RQTL11.R6B1,RPMBA.UA67.RQT12.R6B2,RPMBA.UA67.RQT12.R6B1,RPMBA.UA67.RQT13.R6B2,RPMBA.UA67.RQT13.R6B1,RPLB.UA67.RCBYV5.R6B1,RPLB.UA67.RCBYH5.R6B2,RPLB.UA67.RCBCH10.R6B1,RPLB.UA67.RCBCV10.R6B2,RPLB.UA67.RCBCV9.R6B1,RPLB.UA67.RCBCH9.R6B2,RPLB.UA67.RCBCH8.R6B1,RPLB.UA67.RCBCV8.R6B2,RPLB.UA67.RCO.A67B1,RPLB.UA67.RCO.A67B2,RPHGA.UJ67.RQ8.R6B2,RPHGA.UJ67.RQ8.R6B1,RPHGA.UJ67.RQ9.R6B1,RPHGA.UJ67.RQ9.R6B2,RPHGA.UJ67.RQ10.R6B2,RPHGA.UJ67.RQ10.R6B1,RPTG.SR7.RD34.LR7,RPTF.SR7.RQ5.LR7,RPTF.SR7.RQ4.LR7,RFM.SR7.SPARE.1,RPMBA.RR73.RQTL10.L7B2,RPMBA.RR73.RQTL10.L7B1,RPMBA.RR73.RQTL11.L7B2,RPMBA.RR73.RQTL11.L7B1,RPMBA.RR73.RQT12.L7B2,RPMBA.RR73.RQT12.L7B1,RPMBA.RR73.RQT13.L7B2,RPMBA.RR73.RQT13.L7B1,RPLB.RR73.RCBCH8.L7B1,RPLB.RR73.RCBCV8.L7B2,RPLB.RR73.RCBCV7.L7B1,RPLB.RR73.RCBCH7.L7B2,RPLB.RR73.RCBCH10.L7B1,RPLB.RR73.RCBCV10.L7B2,RPLB.RR73.RCBCV9.L7B1,RPLB.RR73.RCBCH9.L7B2,RPLB.RR73.RCBCH6.L7B1,RPLB.RR73.RCBCV6.L7B2,RPMBB.RR73.RQ6.L7B2,RPMBB.RR73.RQ6.L7B1,RPMBB.RR73.ROF.A67B2,RPMBB.RR73.ROF.A67B1,RPMBB.RR73.ROD.A67B2,RPMBB.RR73.ROD.A67B1,RPMBB.RR73.RSS.A67B2,RPMBB.RR73.RSS.A67B1,RPMBA.RR73.RQS.L7B2,RPMBA.RR73.RQS.A67B1,RPMBA.RR73.RQTL7.L7B2,RPMBA.RR73.RQTL7.L7B1,RPMBA.RR73.RQTL8.L7B2,RPMBA.RR73.RQTL8.L7B1,RPMBB.RR73.RQTL9.L7B2,RPMBB.RR73.RQTL9.L7B1,RFM.RR73.SPARE.1,RFM.TZ76.SPARE.1,RPMC.TZ76.RQT4.L7,RPMC.TZ76.RQT4.R7,RPMC.TZ76.RQT5.L7,RPMC.TZ76.RQT5.R7,RPMC.TZ76.RCBWV4.L7B2,RPMC.TZ76.RCBWV4.R7B1,RPMC.TZ76.RCBWH4.L7B1,RPMC.TZ76.RCBWH4.R7B2,RPMC.TZ76.RCBWV5.L7B1,RPMC.TZ76.RCBWV5.R7B2,RPMC.TZ76.RCBWH5.L7B2,RPMC.TZ76.RCBWH5.R7B1,RPMBB.RR77.RSS.A78B2,RPMBB.RR77.RSS.A78B1,RPMBB.RR77.ROD.A78B2,RPMBB.RR77.ROD.A78B1,RPMBB.RR77.ROF.A78B2,RPMBB.RR77.ROF.A78B1,RPMBB.RR77.RQ6.R7B2,RPMBB.RR77.RQ6.R7B1,RPLB.RR77.RCBCV6.R7B1,RPLB.RR77.RCBCH6.R7B2,RFM.RR77.SPARE.1,RPLB.RR77.RCBCH9.R7B1,RPLB.RR77.RCBCV9.R7B2,RPLB.RR77.RCBCV10.R7B1,RPLB.RR77.RCBCH10.R7B2,RPLB.RR77.RCBCH7.R7B1,RPLB.RR77.RCBCV7.R7B2,RPLB.RR77.RCBCV8.R7B1,RPLB.RR77.RCBCH8.R7B2,RPMBA.RR77.RQT13.R7B2,RPMBA.RR77.RQT13.R7B1,RPMBA.RR77.RQT12.R7B2,RPMBA.RR77.RQT12.R7B1,RPMBA.RR77.RQTL11.R7B2,RPMBA.RR77.RQTL11.R7B1,RPMBA.RR77.RQTL10.R7B2,RPMBA.RR77.RQTL10.R7B1,RPMBB.RR77.RQTL9.R7B2,RPMBB.RR77.RQTL9.R7B1,RPMBA.RR77.RQTL8.R7B2,RPMBA.RR77.RQTL8.R7B1,RPMBA.RR77.RQTL7.R7B2,RPMBA.RR77.RQTL7.R7B1,RPMBA.RR77.RQS.A78B2,RPMBA.RR77.RQS.R7B1",
			  script=>undef, 
			  protocol=>'tcp');

%params = parse_opts %params;

authenticate($params{authenticate});

# Check the script

ok(-e $params{script}, "File '$params{script}' exists");
ok(-x $params{script}, "File '$params{script}' is excutable");

# Retrieve matching devices

my %devices;

foreach my $device_expr (split(',',$params{devices}))
{
    foreach my $device (get_devices($device_expr))
    {
        $devices{$device} = undef; 
    }
}

ok(scalar(keys %devices), scalar(keys %devices) . " devices matching '$params{devices}'");

# Check that all devices are online

for my $device (sort keys %devices)
{
    get($device,"TIME.NOW");
}

ok(scalar(keys %devices), scalar(keys %devices) . " online devices matching '$params{devices}'");

# Retrieve matching fecs

my %fecs;

foreach my $fec_expr (split(',',$params{fecs}))
{
    foreach my $fec (get_devices($fec_expr))
    {
        $fecs{$fec} = undef; 
    }
}

ok(scalar(keys %fecs), scalar(keys %fecs) . " FECs matching '$params{fecs}'");

# Check that all devices are online

for my $fec (sort keys %fecs)
{
    get($fec,"TIME.NOW");
}

ok(scalar(keys %fecs), scalar(keys %fecs) . " online devices matching '$params{fecs}'");

# Reset all fieldbus counters

for my $fec (sort keys %fecs)
{
    get($fec,"FIELDBUS ZERO");
    get($fec,"GW.FGC.FIELDBUS ZERO");
}

# Test devices

my %files;

for my $device (sort keys %devices)
{
    # Create temporal file
    
    my $tmp = File::Temp->new();

    $files{$device} = $tmp;

    # create child process
    
    my $cmd = "$params{script} --device=$device --protocol=$params{protocol} &> " . $tmp->filename;
    
    my $pid = fork() or exec($cmd);
    
    ok(defined($pid), "Process spawned: $params{script} --device=$device --protocol=$params{protocol}...");
         
    $devices{$device} = $pid;
}

# Wait for childs

for my $device (sort keys %devices)
{
    waitpid($devices{$device}, 0);
    
    if (($? >>8 ) == 0)
    {
        ok(1,"$params{script} --device=$device --protocol=$params{protocol}"); 
    }
    else
    {
        # Print error contents from child
        
        my $content = "ERROR: $params{script} --device=$device --protocol=$params{protocol} (exit = " . ($? >>8) . "):\n";
        
        open(my $outFile, '<', $files{$device}->filename) or die "ERROR: Unable to open temporal file '" . $files{$device}->filename . "': $!";
        
        while ( my $line = <$outFile> )
        {
            $content = $content . "\t$line";
        }
        
        die $content; 
        
    }
    
}

# Check fieldbus

for my $fec (sort keys %fecs)
{
    my $fieldbus = get($fec,"FIELDBUS.WFIP");
	$fieldbus =~ s/\n/ /g;
	ok( $fieldbus =~ /DIAG_DEV.VAR_MISS:0.*DIAG_DEV.PROMPT_FAIL:0.*DIAG_DEV.SIG_FLT:0.*DIAG_DEV.USER_ERROR:0.*ERRORS.DETECTED:0.*ERRORS.FGC.ACK_MISS:0.*ERRORS.FGC.RD_MISS:0.*ERRORS.FGC.TIME_MISS:0.*ERRORS.FGC.NONSIG_FLT:0.*ERRORS.FGC.PROMPT_FAIL:0.*ERRORS.FGC.SIG_FLT:0.*ERRORS.FGC.USER_ERROR:0.*ERRORS.INTERFACE.ERR_COUNT:0/ , "$fec FIELDBUS.FGC without errors");
}

exit(0);

