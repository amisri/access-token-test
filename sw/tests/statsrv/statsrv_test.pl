#!/usr/bin/perl -w
#
# Name:    statsrv_test.pl
# Purpose: Test getting data for a single gateway from FGC status server
# Author:  Stephen Page

use FGC::StatSrv;
use IO::Socket;
use strict;

# Connect to status server

my $socket = FGC::StatSrv::connect();
die "Unable to connect to status server: $!\n" if(!defined($socket));

# Get status for devices

my $status = FGC::StatSrv::getstatus($socket, "CFC-866-RFIP1");
die "Failed to get status\n" if(!defined($status));

# Disconnect from status server

FGC::StatSrv::disconnect($socket);

# Print data

printf("host=$status->{hostname}\nsequence=$status->{sequence}\ntime=$status->{time_sec}.%06i\n", $status->{time_usec});

for my $device (@{$status->{channels}})
{
    next if(!$device->{DATA_STATUS}->{DATA_VALID});

    for my $key (sort(keys(%{$device})))
    {
        # Check whether field is a reference to a hash (ie a symlist)

        if(ref($device->{$key}) eq "HASH")
        {
            print "$key=[";

            for my $symbol (sort(keys(%{$device->{$key}})))
            {
                next if($symbol eq "mask");

                print "$symbol " if($device->{$key}->{$symbol});
            }

            print "] ";
        }
        else # Field is not a reference to a hash
        {
            print "$key=$device->{$key} ";
        }
    }
    print "\n";
}

# EOF
