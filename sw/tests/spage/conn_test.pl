#!/usr/bin/perl -w
#
# Name:    conn_test.pl
# Purpose: Test gateway connection handling
# Author:  Stephen Page

use strict;

use FGC::Sync;

# Print usage if incorrect number of arguments

die "\nUsage: $0 <gateway>\n\n" if(@ARGV < 1);
my ($gateway) = @ARGV;

my $num_conns = 30;
my @sockets;

for(my $i = 1 ; $i <= $num_conns ; $i++)
{
    print "Connect $i...\n";

    $sockets[$i] = FGC::Sync::connect($gateway);
    print "Unable to connect to gateway $gateway: $!\n" if($sockets[$i] < 0);
}

sleep(5);

for(my $i = 1 ; $i <= $num_conns ; $i++)
{
    print "Disconnect $i...\n";
    FGC::Sync::disconnect($sockets[$i]);
}
print "Done\n";

# EOF
