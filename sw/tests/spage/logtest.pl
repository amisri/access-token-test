#!/usr/bin/perl -w

use strict;

use FGC::Consts;
use FGC::Sync;
use FGC::Class::51::Binary_logs;

die "Usage $0 <gateway> <username> <password> <channel>\n" if(@ARGV != 4);

my ($gateway, $username, $password, $channel) = @ARGV;

# Connect to gateway

my $socket = FGC::Sync::connect($gateway, $username, $password);
die "Unable to connect to gateway $gateway: $!\n" if($socket < 0);

my $response = FGC::Sync::get($socket, $channel, "LOG.IAB BIN");

FGC::Sync::disconnect($socket);

my $log = FGC::Class::51::Binary_logs::read_iab(\$response->{value});

for my $value (@$log)
{
    printf("%d.%06d %f %f\n", $value->{TIMESTAMP_SEC}, $value->{TIMESTAMP_USEC}, $value->{I_A}, $value->{I_B});
}

# EOF
