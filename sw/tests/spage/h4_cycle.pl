#!/usr/bin/perl -w
#
# Name:     h4_cycle.pl
# Purpose:  Cycle a power converter for H4 radiation tests
# Author:   Stephen Page


use FGC::Consts;
use FGC::Names;
use FGC::RBAC;
use FGC::Sync;
use POSIX qw(strftime);
use strict;

my $device_name;
my $devices;
my $gateways;
my $i_pos;
my $password;
my $socket;
my $username;
my $rbac_token;
my $rbac_last_auth_time = 0;

# Sleep function as sleep() seems to take too long

sub my_sleep($)
{
    my ($delay) = @_;
    system("sleep $delay");
}

# Check RBAC token

sub check_rbac_token()
{
    do
    {
        # Check whether RBAC token is less than one hour from expiry

        if(!defined($rbac_token) || time > ($rbac_token->{ExpirationTime} - 3600))
        {
            # Check whether it is at least one minute since last authentication

            if($rbac_last_auth_time < (time - 60))
            {
                warn "Getting new RBAC token at ", scalar(localtime), "\n\n";

                get_rbac_token() or warn "RBAC authentication failed at ", scalar(localtime), "\n\n";
            }

            if(defined($rbac_token) && $rbac_token->{ExpirationTime} >= time)
            {
                # Set RBAC token on gateway

                my $response;
                eval { $response = FGC::Sync::set($socket, 0, "CLIENT.TOKEN", \$rbac_token->{binary}, 1) };
                die "Failed to send command to set RBAC token on gateway\n"     if($@);
                die "Error setting RBAC token on gateway: $response->{value}\n" if($response->{error});
            }
            else
            {
                my_sleep(60);
            }
        }
    } while(!defined($rbac_token) || $rbac_token->{ExpirationTime} < time);
}

# Authenticate and obtain RBAC token

sub get_rbac_token()
{
    $rbac_last_auth_time    = time;

    my $binary_token        = FGC::RBAC::authenticate($username, $password);
    return(0) if(!defined($binary_token));

    $rbac_token             = FGC::RBAC::decode_token(\$binary_token);
    $rbac_token->{binary}   = $binary_token;

    return(1);
}

sub read_values(%$$)
{
    my ($device, $step, $i_ref) = @_;

    my $time = strftime("%d/%m/%Y %H:%M:%S", localtime());
    print "$time,$step,$i_ref";

    my @properties = (
                        "MEAS.I",
                        "STATE.PC",
                        "MEAS.I_DIFF_MA",
                        "MEAS.I_EARTH",
                        "MEAS.MAX_I_EARTH",
                        "REF.V",
                        "MEAS.V",
                        "ADC.AMPS_200MS",
                        "ADC.PP_VOLTS_1S",
                        "ILOOP.MAX_ABS_ERR ZERO",
                        "MEAS.U_LEADS",
                        "MEAS.MAX_U_LEADS ZERO",
                        "DEVICE.PSU_ANA",
                        "DEVICE.PSU_FGC",
                        "MEM.NUM_ERRORS",
                        "MEM.ERROR_TIME",
                        "ADC.PP_VOLTS_1S",
                     );

    for my $property (@properties)
    {
        my $response = FGC::Sync::get($socket, $device->{channel}, $property);
        print ",$response->{value}";
    }
    print "\n";
}

die "Usage $0 <device name> <I_POS>\n" if(@ARGV != 2);
($device_name, $i_pos) = @ARGV;

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

my $device = $devices->{$device_name};
die "Unknown device $device_name\n" if(!defined($device));

# Prompt for username

print STDERR "\nUsername: ";
chomp($username = <STDIN>);

# Prompt for password

print STDERR "Password: ";
system("stty -echo");
chomp($password = <STDIN>);
system("stty echo");
print "\n\n";

# Flush stdout immediately

$| = 1;

# Connect to gateway

$socket = FGC::Sync::connect($device->{gateway}->{name});
die "Unable to connect to gateway $device->{gateway}: $!\n" if($socket < 0);

# Print column headers

print "Time,Step,REF.I,MEAS.I,STATE.PC,MEAS.I_DIFF_MA,MEAS.I_EARTH,MEAS.MAX_I_EARTH,REF.V,MEAS.V,ADC.AMPS_200MS[0],ADC.AMPS_200MS[1],ADC.PP_VOLTS_1S[0],ADC.PP_VOLTS_1S[1],ILOOP.MAX_ABS_ERR,MEAS.U_LEADS[0],MEAS.U_LEADS[1],MEAS.MAX_U_LEADS[0],MEAS.MAX_U_LEADS[1],DEVICE.PSU_ANA[0],DEVICE.PSU_ANA[1],DEVICE.PSU_ANA[2],DEVICE.PSU_FGC[0],DEVICE.PSU_FGC[1],DEVICE.PSU_FGC[2],MEM.NUM_ERRORS,MEM.ERROR_TIME,ADC.PP_VOLTS_1S[0],ADC.PP_VOLTS_1S[1]\n";

my @sequence =  (
                    {
                        action  => 'ref',
                        current => $i_pos * 0.5,
                        wait    => 1200,
                    },
                    {
                        action  => 'ref',
                        current => -$i_pos * 0.5,
                        wait    => 1200,
                    },
                    {
                        action  => 'ref',
                        current => $i_pos * 0.5,
                        wait    => 1200,
                    },
                    {
                        action  => 'ref',
                        current => $i_pos * 0.1,
                        wait    => 1200,
                    },
                    {
                        action  => 'ref',
                        current => $i_pos,
                        wait    => 1200,
                    },
                    {
                        action  => 'ref',
                        current => -$i_pos * 0.1,
                        wait    => 1200,
                    },
                    {
                        action  => 'ref',
                        current => -$i_pos,
                        wait    => 1200,
                    },
                    {
                        action  => 'ref',
                        current => 0,
                        wait    => 1200,
                    },
                );

my $i_ref = 0;
for(my $i = 0 ; ; $i = ($i + 1) % @sequence)
{
    my $element = $sequence[$i];

    check_rbac_token();

    if($element->{action} eq 'on')
    {
        my $response = FGC::Sync::set($socket, $device->{channel}, 'STATE.PC', 'ON_STANDBY');
        warn "Error setting STATE.PC to ON_STANDBY: $response->{value}\n", my_sleep(600), next if($response->{error});

        my_sleep(10);

        $response = FGC::Sync::set($socket, $device->{channel}, 'STATE.PC', 'IDLE');
        warn "Error setting STATE.PC to IDLE: $response->{value}\n", my_sleep(600), next if($response->{error});
    }
    elsif($element->{action} eq 'off')
    {
        my $response = FGC::Sync::set($socket, $device->{channel}, 'STATE.PC', 'OFF');
        warn "Error setting STATE.PC to OFF: $response->{value}\n", my_sleep(600), next if($response->{error});

        my_sleep(30);
    }
    elsif($element->{action} eq 'ref')
    {
        read_values($device, $i * 2, $i_ref);

        $i_ref = $element->{current};

        my $response = FGC::Sync::set($socket, $device->{channel}, 'REF', "NOW,$i_ref");
        warn "Error setting reference to $i_ref: $response->{value}\n", my_sleep(600), next if($response->{error});

        my_sleep(10);

        $response = FGC::Sync::get($socket, $device->{channel}, 'REF.REMAINING');
        warn "Error getting REF.REMAINING: $response->{value}\n", my_sleep(600), next if($response->{error});
        my_sleep($response->{value});
    }
    else # Unknown action
    {
        die "Unknown action $element->{action}\n";
    }

    check_rbac_token();
    read_values($device, ($i * 2) + 1, $i_ref);

    if(defined($element->{wait}))
    {
        my_sleep($element->{wait});
    }
}
FGC::Sync::disconnect($socket);

# EOF
