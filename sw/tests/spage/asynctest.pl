#!/usr/bin/perl -w

use FGC::Async;
use FGC::Names;
use strict;
use Time::HiRes qw(gettimeofday tv_interval);

my $devices;
my $gateways;

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
($devices, $gateways) = FGC::Names::read_web()  if(!defined($devices));
die "Unable to read FGC name file\n"            if(!defined($devices));

FGC::Async::init($devices, $gateways);
FGC::Async::connect($gateways->{pcgw08}, $ARGV[0], $ARGV[1]);
FGC::Async::connect($gateways->{pcgw19}, $ARGV[0], $ARGV[1]);

for my $device (@{$gateways->{pcgw08}->{channels}})
{
    next if(!defined($device->{name}));

    FGC::Async::get($device, "TIME.NOW");
}

for my $device (@{$gateways->{pcgw19}->{channels}})
{
    next if(!defined($device->{name}));

    FGC::Async::get($device, "TIME.NOW");
}

my $start_time  = [gettimeofday];
my $responses   = FGC::Async::read;
my $elapsed     = tv_interval($start_time);
print "$elapsed\n";

FGC::Async::disconnect($gateways->{pcgw08});
FGC::Async::disconnect($gateways->{pcgw19});

for my $command (values(%$responses))
{
    print "TEST DEVICE=[$command->{device}->{name}] PROPERTY=[$command->{property}] TAG=[$command->{tag}] ERROR=[$command->{response}->{error}] BINARY=[$command->{response}->{binary}] VALUE=[$command->{response}->{value}]\n";
}

# EOF
