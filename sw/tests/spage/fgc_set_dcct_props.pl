#!/usr/bin/perl -w
#
# Name:    fgc_set_dcct_props.pl
# Purpose: Locate DCCTs and set properties for their calibration from a file
# Author:  Stephen Page

use strict;

use FGC::Async;
use FGC::Names;

sub read_file($)
{
    my ($filename) = @_;

    open(FILE, "<", $filename) or return(undef);

    my %values;

    while(<FILE>)
    {
        s/[\012\015]//g; # Remove newline

        my ($barcode, $dallas, $value)  = split(",");
        $values{$barcode}               = $value;
    }
    close(FILE);

    return(\%values);
}

# End of functions


die "\nUsage: $0 <username> <password> <property> <value file>\n\n" if(@ARGV < 4);
my ($username, $password, $property, $filename) = @ARGV;

# Read FGC name file

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Read values from file

my $values = read_file($filename) or die "Error reading file $filename: $!\n";

# Initialise FGC asynchronous communication module

FGC::Async::init($devices, $gateways);

# Connect to all gateways

for my $gateway (values(%$gateways))
{
    if(FGC::Async::connect($gateway, $username, $password) < 0)
    {
        warn "Unable to connect to gateway $gateway->{name}\n";
    }
}

# Read all DCCT barcodes

for my $bus ("A", "B")
{
    for my $subst_property ("BARCODE.?.DCCT.HEAD", "BARCODE.?.DCCT.ELEC")
    {
        (my $property = $subst_property) =~ s/\?/$bus/;

        for my $device (grep { $_->{class} == 51 } values(%$devices))
        {
            next if(!defined($device->{gateway}->{socket}));

            eval { FGC::Async::get($device, $property) };

            # Check whether an error occurred

            if($@) # An error occurred
            {
                warn "Error getting property $property from $device->{name}: $@\n";
                next;
            }
        }
    }
}

# Read responses

my %barcodes;
my $commands = FGC::Async::read();

for my $command (values(%$commands))
{
    if($command->{response}->{error} && $command->{response}->{value} !~ /^\d+ dev not ready$/)
    {
        warn "Error getting property $command->{property} from $command->{device}->{name}: $command->{response}->{value}\n";
        next;
    }

    # Remove commas in character arrays

    $command->{response}->{value} =~ s/,//g if($command->{response}->{value} =~ /(\D),(\S),(\S+)/);

    $barcodes{$command->{response}->{value}}->{device}  = $command->{device};
    ($barcodes{$command->{response}->{value}}->{bus}    = $command->{property}) =~ s/BARCODE\.(.).*/$1/;
}

for my $barcode (keys(%$values))
{
    next if(!defined($barcodes{$barcode})); # Skip barcodes for which the device is not known

    (my $subst_property = $property) =~ s/\?/$barcodes{$barcode}->{bus}/g;

    my $device = $barcodes{$barcode}->{device};

    print "$barcode:$device->{name} S $subst_property $values->{$barcode}\n";
    eval { FGC::Async::set($device, $subst_property, $values->{$barcode} ) };

    # Check whether an error occurred

    if($@) # An error occurred
    {
        warn "Error setting property $property for $device->{name} with value $values->{$barcode}: $@\n";
        next;
    }
}

# Read responses

$commands = FGC::Async::read();

for my $command (values(%$commands))
{
    if($command->{response}->{error} && $command->{response}->{value} !~ /^\d+ dev not ready$/)
    {
        warn "Error setting property $command->{property} for $command->{device}->{name} with value $command->{value}: $command->{response}->{value}\n";
        next;
    }
}

# Disconnect from all gateways

for my $gateway (values(%$gateways))
{
    FGC::Async::disconnect($gateway);
}

# EOF
