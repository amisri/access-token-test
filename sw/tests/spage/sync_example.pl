#!/usr/bin/perl -w
#
# Name:     sync_example.pl
# Purpose:  A simple example of how synchronous communication with FGC devices
# Author:   Stephen Page

use FGC::Names;
use FGC::RBAC;
use FGC::Sync;
use strict;

# Authenticate and return an RBAC token

sub authenticate()
{
    # Prompt for username

    print "\nUsername: ";
    chomp(my $username = <STDIN>);

    # Prompt for password

    print "Password: ";
    system("stty -echo");
    chomp(my $password = <STDIN>);
    system("stty echo");
    print "\n\n";

    # Authenticate to RBAC server

    my $token = FGC::RBAC::authenticate($username, $password);
    return($token);
}

# End of functions


# Validate arguments

die "Usage $0 <device name>\n" if(@ARGV != 1);
my ($device_name) = @ARGV;

# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name data.\n" if(!defined($devices));

# Check that the requested device exists

my $device = $devices->{$device_name};
if(!defined($device))
{
    die "Device $device_name does not exist.\n";
}

# Perform RBAC authentication

my $rbac_token = authenticate()
    or die "RBAC authentication failed.\n";

# Connect to the gateway

my $gateway_name = $device->{gateway}->{name};
my $socket = FGC::Sync::connect($gateway_name);
die "Unable to connect to gateway $gateway_name: $!\n" if($socket < 0);

# Set RBAC token on gateway

my $response = FGC::Sync::set($socket, "", "CLIENT.TOKEN", \$rbac_token, 1);
if($response->{error})
{
    FGC::Sync::disconnect($socket);
    die "Error setting RBAC token: $response->{value}\n";
}

# Get the device's name

$response = FGC::Sync::get($socket, $device->{channel}, "DEVICE.NAME");
print "DEVICE.NAME = \"$response->{value}\"\n";

# Disconnect from the gateway

FGC::Sync::disconnect($socket);

# EOF
