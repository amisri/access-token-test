#!/usr/bin/perl -w

use Socket;
use strict;

use FGC::Async;
use FGC::Consts;
use FGC::Names;
use FGC::RBAC;
use FGC::Sub;
use FGC::Sync;

# Print usage if incorrect number of arguments

die "Usage: $0 <username> <password> <period (20ms ticks)> <port>\n" if(@ARGV < 4);
my ($username, $password, $period, $port_number) = @ARGV;

# Authenticate and obtain RBAC token

my $rbac_token = FGC::RBAC::authenticate($username, $password);
die "RBAC authentication failed\n" if(!defined($rbac_token));

# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Create UDP socket

my $udpsock;
socket($udpsock, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

# Create address structure

my $address = sockaddr_in($port_number, INADDR_ANY);

# Bind to port

bind($udpsock, $address) or die "Unable to bind to UDP port $port_number : $!\n";

# Connect to gateways

my %gateways_by_ip;
for my $gateway (values(%$gateways))
{
    next if(!($gateway->{channels}->[0]->{sector_mask} & 0x0100));

    (($gateway->{tcp_socket} = FGC::Async::connect($gateway)) >= 0)
        or die "Unable to connect to gateway $gateway->{name} : $!\n";

    $gateways_by_ip{$gateway->{ip}} = $gateway;

    # Set RBAC token on gateway

    FGC::Sync::set($gateway->{tcp_socket}, "", "client.token", \$rbac_token, 1);

    # Set up subscription

    FGC::Sync::set($gateway->{tcp_socket}, "", "client.udp.sub.period", $period);
    FGC::Sync::set($gateway->{tcp_socket}, "", "client.udp.sub.port",   $port_number);
}
# Receive data

my $expected_sequence   = 0;
while(my ($sockaddr_in, $sequence, $time_sec, $time_usec, $status) = FGC::Sub::read($udpsock))
{
    next if(!defined($sockaddr_in)); # Error receiving packet

    my $gateway = $gateways_by_ip{inet_ntoa((sockaddr_in($sockaddr_in))[1])};
    next if(!defined($gateway));

    for(my $i = 0 ; $i < FGC_MAX_DEVS_PER_GW ; $i++)
    {
        next if(!$status->[$i]->{DATA_STATUS}->{DATA_VALID});

        my $device  = $gateway->{channels}->[$i];
        my $stat    = $status->[$i];

        next if(!defined($device));
        next if($device->{class}    != 51);
        next if($device->{name}     =~ /^RFM\./);
        next if($device->{name}     eq "RPTH.SX2.RXSOL.ALICE");

        if($stat->{I_MEAS} > 1)
        {
            printf("%s %s %s %.03f\n", scalar(localtime), $device->{name}, $stat->{STATE_PC}, $stat->{I_MEAS});
        }
    }
}
close($udpsock);

for my $gateway (values(%$gateways))
{
    FGC::Sync::disconnect($gateway->{tcp_socket}) if(defined($gateway->{tcp_socket}));
}

# EOF
