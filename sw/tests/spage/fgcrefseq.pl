#!/usr/bin/perl -w
#
# Name:    fgcrefseq.pl
# Purpose: Run a sequence of references
# Author:  Stephen Page
# Notes:   Initial state of power converters should be IDLE

use Socket;
use strict;

use FGC::Async;
use FGC::Consts;
use FGC::Names;
use FGC::Sub;
use FGC::Sync;

# Global variables

#my @device_names = qw/RFM.866.21.DEV RFM.866.27.DEV/;
my @device_names = qw/RPHF.UA83.RD2.L8/;

my @references = (
                    "PLEP,6000,5,18",
                    "PLEP,350,5,18",
                 );

my ($devices, $gateways);
my %target_devices;
my $udpsock;


# Functions

# Monitor running of references until completion

sub monitor_references()
{
    # Start subscriptions

    for my $device (values(%target_devices))
    {
        $device->{ramp_state} = "PRE";
        FGC::Sync::set($device->{gateway}->{socket}, "", "CLIENT.UDP.SUB.PERIOD", 50);
        print "$device->{name}: ARMED\n";
    }

    my $num_devices_ramping = keys(%target_devices);

    # Receive data

    while($num_devices_ramping && (my ($sockaddr_in, $sequence, $time_sec, $time_usec, $status) = FGC::Sub::read($udpsock)))
    {
        next if(!defined($sockaddr_in)); # Error receiving packet

        my ($port, $address)    = unpack_sockaddr_in($sockaddr_in);
        my $host                = gethostbyaddr($address, PF_INET);
        $host                   =~ s/(\w+)\..*/$1/;
        my $gateway             = $gateways->{$host};

        for my $device (grep { $_->{gateway}->{name} eq $gateway->{name} } values(%target_devices))
        {
            next if(!$status->[$device->{channel}]->{DATA_STATUS}->{DATA_VALID} || !$status->[$device->{channel}]->{DATA_STATUS}->{CLASS_VALID});

            $device->{status} = $status->[$device->{channel}];

            if($device->{ramp_state} eq "PRE")
            {
                if($device->{status}->{STATE_PC} eq "RUNNING")
                {
                    $device->{ramp_state} = "RUN";
                    print "$device->{name}: RUNNING\n";
                }
                elsif($device->{status}->{STATE_PC} ne "ARMED")
                {
                    warn "$device->{name} failed\n";
                    return(1);
                }
            }
            elsif($device->{ramp_state} eq "RUN")
            {
                if($device->{status}->{STATE_PC} eq "IDLE")
                {
                    $device->{ramp_state} = "IDLE";
                    $num_devices_ramping--;
                    print "$device->{name}: END\n";
                }
                elsif($device->{status}->{STATE_PC} eq "END")
                {
                }
                elsif($device->{status}->{STATE_PC} ne "RUNNING")
                {
                    warn "$device->{name} failed\n";
                    return(1);
                }
            }
        }
    }

    # Stop subscriptions

    for my $device (values(%target_devices))
    {
        FGC::Sync::set($device->{gateway}->{socket}, "", "CLIENT.UDP.SUB.PERIOD", 0);
    }

    return(0);
}

# End of functions


# Print usage if incorrect number of arguments

die "Usage: $0 <username> <password>\n" if(@ARGV < 2);
my ($username, $password) = @ARGV;

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
($devices, $gateways) = FGC::Names::read_web()  if(!defined($devices));
die "Unable to read FGC name file\n"            if(!defined($devices));

# Initialise FGC::Async module

FGC::Async::init($devices, $gateways);

# Build hash of target devices

for my $name (@device_names)
{
    my $device = $devices->{$name};
    die "Unknown device $name\n" if(!defined($device));

    $target_devices{$name} = $device;
}

# Create UDP socket

socket($udpsock, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

# Bind to port

my $port_number;
for($port_number = 1024 ; $port_number <= 65535 ; $port_number++)
{
    bind($udpsock, sockaddr_in($port_number, INADDR_ANY)) && last;
}

if($port_number == 65536)
{
    close($udpsock);
    die "Unable to find a free UDP port to bind to for subscription\n";
}

# Build hash of gateways to connect to

my %gateways_to_connect;
for my $device (values(%target_devices))
{
    $gateways_to_connect{$device->{gateway}->{name}} = $device->{gateway};
}

# Connect to gateways

for my $gateway (values(%gateways_to_connect))
{
    if(FGC::Async::connect($gateway, $username, $password) < 0)
    {
        die "Unable to connect to gateway $gateway->{name}: $!\n";
    }

    # Set up subscription

    FGC::Sync::set($gateway->{socket}, "", "CLIENT.UDP.SUB.PORT", $port_number);
}

# Run reference sequence

while(1)
{
    for my $reference (@references)
    {
        print "\nReference - $reference\n\n";

        # Arm converters with references

        for my $device (values(%target_devices))
        {
            FGC::Async::set($device, "REF", $reference);
        }
        my $commands = FGC::Async::read();

        # Check whether references were accepted

        for my $command (values(%$commands))
        {
            die "$command->{device}->{name} failed to accept reference \"$reference\": $command->{response}->{value}\n" if($command->{response}->{error});
        }

        # Start converters at the same time

        my $time = time + 10;
        for my $device (values(%target_devices))
        {
            FGC::Async::set($device, "REF.RUN", $time);
        }
        $commands = FGC::Async::read();

        # Monitor references until completion

        die if(monitor_references());
    }
}

# Clean-up

close($udpsock);

for my $gateway (values(%gateways_to_connect))
{
    FGC::Sync::disconnect($gateway->{socket});
}

# EOF
