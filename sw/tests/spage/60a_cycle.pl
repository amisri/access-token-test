#!/usr/bin/perl -w
#
# Name:     60a_cycle.pl
# Purpose:  Cycle a 60A power converter
# Author:   Stephen Page

use POSIX qw(strftime);
use strict;

use FGC::Consts;
use FGC::Names;
use FGC::RBAC;
use FGC::Sync;

my $device_name;
my $devices;
my $gateways;
my $password;
my $socket;
my $username;
my $rbac_token;
my $rbac_last_auth_time = 0;

# Check RBAC token

sub check_rbac_token()
{
    do
    {
        # Check whether RBAC token is less than one hour from expiry

        if(!defined($rbac_token) || time > ($rbac_token->{ExpirationTime} - 3600))
        {
            # Check whether it is at least one minute since last authentication

            if($rbac_last_auth_time < (time - 60))
            {
                warn "Getting new RBAC token at ", scalar(localtime), "\n\n";

                get_rbac_token() or warn "RBAC authentication failed at ", scalar(localtime), "\n\n";
            }

            if(defined($rbac_token) && $rbac_token->{ExpirationTime} >= time)
            {
                # Set RBAC token on gateway

                my $response;
                eval { $response = FGC::Sync::set($socket, 0, "CLIENT.TOKEN", \$rbac_token->{binary}, 1) };
                die "Failed to send command to set RBAC token on gateway\n"     if($@);
                die "Error setting RBAC token on gateway: $response->{value}\n" if($response->{error});
            }
            else
            {
                sleep(60);
            }
        }
    } while(!defined($rbac_token) || $rbac_token->{ExpirationTime} < time);
}

# Authenticate and obtain RBAC token

sub get_rbac_token()
{
    $rbac_last_auth_time    = time;

    my $binary_token        = FGC::RBAC::authenticate($username, $password);
    return(0) if(!defined($binary_token));

    $rbac_token             = FGC::RBAC::decode_token(\$binary_token);
    $rbac_token->{binary}   = $binary_token;

    return(1);
}

die "Usage $0 <device name>\n" if(@ARGV != 1);
($device_name) = @ARGV;

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

my $device = $devices->{$device_name};
die "Unknown device $device_name\n" if(!defined($device));

# Prompt for username

print STDERR "\nUsername: ";
chomp($username = <STDIN>);

# Prompt for password

print STDERR "Password: ";
system("stty -echo");
chomp($password = <STDIN>);
system("stty echo");
print "\n\n";

# Flush stdout immediately

$| = 1;

# Connect to gateway

$socket = FGC::Sync::connect($device->{gateway}->{name});
die "Unable to connect to gateway $device->{gateway}: $!\n" if($socket < 0);

my @sequence =  (
                    {
                        action  => 'ref',
                        current => 1,
                        wait    => 210,
                    },
                    {
                        action  => 'ref',
                        current => 20,
                        wait    => 210,
                    },
                );

my $i_ref = 0;
for(my $i = 0 ; ; $i = ($i + 1) % @sequence)
{
    my $element = $sequence[$i];

    check_rbac_token();

    if($element->{action} eq 'on')
    {
        my $response = FGC::Sync::set($socket, $device->{channel}, 'STATE.PC', 'ON_STANDBY');
        warn "Error setting STATE.PC to ON_STANDBY: $response->{value}\n", sleep(600), next if($response->{error});

        sleep(10);

        $response = FGC::Sync::set($socket, $device->{channel}, 'STATE.PC', 'IDLE');
        warn "Error setting STATE.PC to IDLE: $response->{value}\n", sleep(600), next if($response->{error});
    }
    elsif($element->{action} eq 'off')
    {
        my $response = FGC::Sync::set($socket, $device->{channel}, 'STATE.PC', 'OFF');
        warn "Error setting STATE.PC to OFF: $response->{value}\n", sleep(600), next if($response->{error});

        sleep(30);
    }
    elsif($element->{action} eq 'ref')
    {
        $i_ref = $element->{current};

        my $response = FGC::Sync::set($socket, $device->{channel}, 'REF', "NOW,$i_ref");
        warn "Error setting reference to $i_ref: $response->{value}\n", sleep(600), next if($response->{error});

        sleep(1);

        $response = FGC::Sync::get($socket, $device->{channel}, 'REF.REMAINING');
        warn "Error getting REF.REMAINING: $response->{value}\n", sleep(600), next if($response->{error});
        sleep(ord($response->{value}) + 1);
    }
    else # Unknown action
    {
        die "Unknown action $element->{action}\n";
    }

    check_rbac_token();

    if(defined($element->{wait}))
    {
        sleep($element->{wait});
    }
}
FGC::Sync::disconnect($socket);

# EOF
