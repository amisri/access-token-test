#!/usr/bin/perl -w
#
# Name:     lsa_diffs.pl
# Purpose:  Compare FGC configurations with LSA
# Author:   Stephen Page

use FGC::Async;
use FGC::LSADB;
use FGC::Names;
use FGC::RBAC;

use strict;

my $devices;
my $gateways;
my %lsa;

sub read_lsa_data()
{
    # Connect to LSA database

    my $dbh = FGC::LSADB::connect();

    if(!defined($dbh))
    {
        die "Unable to connect to LSA database\n";
    }

    for my $device (values(%$devices))
    {
        # Read parameters from LSA database

        my %lsa_parms;

        for my $parameter (qw/ACC_PNO DIDT_PNO I_PNO/)
        {
            my $value = FGC::LSADB::get_converter_parameter($dbh, $device->{name}, $parameter);

            if(defined($value))
            {
                $lsa_parms{$parameter} = $value;
            }
            else
            {
                die "Parameter $parameter for device $device->{name} not defined in LSA database\n";
            }
        }

        my %lsa_config = (
                            'LOAD.DIDT.STARTING[0]'     => $lsa_parms{DIDT_PNO} * 0.7,
                            'LOAD.DIDT.STOPPING[0]'     => $lsa_parms{DIDT_PNO},
                            'LOAD.DIDT.TO_STANDBY[0]'   => $lsa_parms{DIDT_PNO},
                            'LOAD.LIMITS.D2IDT2[0]'     => $lsa_parms{ACC_PNO},
                            'LOAD.LIMITS.DIDT[0]'       => $lsa_parms{DIDT_PNO},
                            'LOAD.LIMITS.I_POS[0]'      => $lsa_parms{I_PNO},
                            'REF.IREF.ACCELERATION[0]'  => $lsa_parms{ACC_PNO},
                            'REF.IREF.LINEAR_RATE[0]'   => $lsa_parms{DIDT_PNO},
                         );

        $lsa_config{$_}         =~ s/^\s+// for keys(%lsa_config);
        $lsa{$device->{name}}   = \%lsa_config;
    }
}

# End of functions


my @properties = (
                    'LOAD.DIDT.STARTING[0]',
                    'LOAD.DIDT.STOPPING[0]',
                    'LOAD.DIDT.TO_STANDBY[0]',
                    'LOAD.LIMITS.D2IDT2[0]',
                    'LOAD.LIMITS.DIDT[0]',
                    'LOAD.LIMITS.I_POS[0]',
                    'REF.IREF.ACCELERATION[0]',
                    'REF.IREF.LINEAR_RATE[0]',
                 );


# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Authenticate to RBAC server

my $rbac_token = FGC::RBAC::authenticate($username, $password)
                    or die "Failed to authenticate\n";

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Remove unneeded devices from name data

for my $device (grep { $_->{class}              != 51       ||
                       $_->{name}               =~ /CNGS/   || # Ignore CNGS radiation test
                       $_->{name}               !~ /^RP/    || # Ignore non-power converters
                       $_->{name}               =~ /^RPHFA/ || # Ignore ATLAS solenoid
                       $_->{name}               =~ /^RPHK/  || # Ignore ATLAS toroid
                       $_->{name}               =~ /^RPTK/  || # Ignore klystron converters
                       $_->{name}               =~ /^RPTH/  || # Ignore ALICE solenoid
                       $_->{name}               =~ /^RPTI/  || # Ignore ALICE and LHCb toroids
                       $_->{name}               =~ /^RPTJ/  || # Ignore CMS solenoid
                       $_->{gateway}->{name}    !~ /^cfc/   || # Only operational gateways
                       $_->{gateway}->{name}    =~ /^cfc-sm18/ # Ignore SM18
                     } values(%$devices))
{
    delete($devices->{$device->{name}});
}

# Read LSA database data

read_lsa_data();

# Initialise asynchronous command module

FGC::Async::init($devices, $gateways);

# Connect to all gateways

for my $gateway (values(%$gateways))
{
    if(FGC::Async::connect($gateway) < 0)
    {
        warn "Unable to connect to gateway $gateway->{name}\n";
        next;
    }

    # Set RBAC token on gateway

    eval { FGC::Async::set($gateway->{channels}->[0], "CLIENT.TOKEN", \$rbac_token, 1) };
    warn "Failed to send command to set RBAC token on gateway $gateway->{name}\n" if($@);
}

# Read responses to RBAC token set commands

my $commands = FGC::Async::read();
for my $command (values(%$commands))
{
    if($command->{response}->{error})
    {
        die "Error setting RBAC token on gateway $command->{device}->{name}: $command->{response}->{value}\n";
    }
}

# Read properties from all devices

for my $device (values(%$devices))
{
    for my $property (@properties)
    {
        FGC::Async::get($device, $property);
    }
}
my $responses = FGC::Async::read;

# Disconnect from all gateways

for my $gateway (values(%$gateways))
{
    FGC::Async::disconnect($gateway);
}

my %fgc;

for my $command (values(%$responses))
{
    $fgc{$command->{device}->{name}}->{$command->{property}} = $command->{response}->{value};
}

# Compare FGC and LSA values

my %diffs;
for my $device (sort { $a->{name} cmp $b->{name} } values(%$devices))
{
    for my $property (sort(@properties))
    {
        my $fgc_value = sprintf("%.5e", $fgc{$device->{name}}->{$property});
        my $lsa_value = sprintf("%.5e", $lsa{$device->{name}}->{$property});

        if($fgc_value ne $lsa_value)
        {
            $diffs{$device->{name}}->{$property} = sprintf("%-30s %s", $property, "$fgc_value != $lsa_value");
        }
    }
}

for my $device (sort(keys(%diffs)))
{
    for my $property (sort(keys(%{$diffs{$device}})))
    {
        printf("%-30s %s\n", $device, $diffs{$device}->{$property});
    }
}

# EOF
