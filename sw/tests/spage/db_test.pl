#!/usr/bin/perl -w
#
# Name:    db_test.pl
# Purpose: Test FGC database module
# Author:  Stephen Page

use FGC::Consts;
use FGC::DB;
use strict;

my %systems = (
                "RFM.866.21.DEV"    => {},
                "RFM.866.22.DEV"    => {},
              );

# Connect to database

my $dbh = FGC::DB::connect(FGC_DB_USERNAME, FGC_DB_PASSWORD) or die "Unable to connect to database\n";

# Retrieve data from database

my $start_time = time;

for my $system_name (keys(%systems))
{
    my $system      = $systems{$system_name};
    $system->{name} = $system_name;

    # Get equipment installed in system

    $system->{components} = FGC::DB::get_system_components($dbh, $system->{name})
        or warn "Failed to get components for system $system->{name}\n", next;

    # Get system properties

    $system->{properties} = FGC::DB::get_system_properties($dbh, $system->{name})
        or warn "Failed to get properties for system $system->{name}\n", next;

    # Get component properties

    for my $component (values(%{$system->{components}}))
    {
        $component->{type}              = substr($component->{COMPONENT_MTF_ID}, 0, 10);
        $component->{properties}        = FGC::DB::get_component_properties($dbh, $component->{COMPONENT_MTF_ID}) or warn "Failed to get properties for component $component->{COMPONENT_MTF_ID}\n" and next;
        $component->{type_properties}   = FGC::DB::get_type_properties( $dbh, $component->{type})             or warn "Failed to get properties for type $component->{type}\n"        and next;
    }
}
print "Elapsed = ", time - $start_time, " seconds\n\n";

my $properties = FGC::DB::get_property_details($dbh);

# Disconnect from database

$dbh->disconnect;

# Print data

for my $system (sort { $a->{name} cmp $b->{name} } values(%systems))
{
    print   "$system->{name}\n",
            "------------------------\n";

    my @sorted_components = sort { $a->{COMPONENT_MTF_ID} cmp $b->{COMPONENT_MTF_ID} } values(%{$system->{components}});

    # Print system components

    for my $component (@sorted_components)
    {
        printf("%s %d %-10s\n", $component->{COMPONENT_MTF_ID}, $component->{TP_MULTI_SYS}, $component->{COMPONENT_CHANNEL} || "");
    }
    print "\n";

    # Print system properties

    for my $property (sort(keys(%{$system->{properties}})))
    {
        print "SYSTEM $property=\"", $system->{properties}->{$property} || "", "\"\n";
    }
    print "\n";

    # Print component properties

    for my $component (@sorted_components)
    {
        next if(!defined($component->{properties}));

        my $component_properties = $component->{properties};
        for my $property (sort(keys(%$component_properties)))
        {
            print "COMPONENT $component->{COMPONENT_MTF_ID} $property=\"", $component_properties->{$property} || "", "\"\n";
        }

        my $type_properties = $component->{type_properties};
        for my $property (sort(keys(%$type_properties)))
        {
            print "TYPE $component->{TP_NAME} $property=\"", $type_properties->{$property} || "", "\"\n";
        }
    }
    print "\n";
}

for my $property (@$properties)
{
    print "$property->{PRO_NAME}\n";
}

# EOF
