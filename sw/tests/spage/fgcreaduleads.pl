#!/usr/bin/perl -w
#
# Name:     fgcreaduleads.pl
# Purpose:  Periodically read current lead voltages from FGCs
# Author:   Stephen Page

use POSIX qw(strftime);
use strict;

use FGC::Consts;
use FGC::Sync;

die "Usage $0 <gateway> <username> <password>\n" if(@ARGV != 3);

my ($gateway, $username, $password) = @ARGV;

# Flush stdout immediately

$| = 1;

while(1)
{
    # Connect to gateway

    my $socket = FGC::Sync::connect($gateway, $username, $password);
    die "Unable to connect to gateway $gateway: $!\n" if($socket < 0);

    # Check each channel for log data

    for my $channel (1..(FGC_MAX_DEVS_PER_GW - 1))
    {
        my $response;

        # Read device name

        $response = FGC::Sync::get($socket, $channel, "DEVICE.NAME");
        next if($response->{error}); # Response was an error
        (my $device_name = $response->{value}) =~ s/,//g;

        # Ignore non-RPLBs

        next if($device_name !~ /^RPLB/);

        # Read time

        $response = FGC::Sync::get($socket, $channel, "TIME.NOW");
        next if($response->{error}); # Response was an error
        my $time = $response->{value};

        $time =~ /^\s*(\d+)\.(\d+)$/;
        my $time_sec    = $1;
        my $time_usec   = $2;

        $time = sprintf("%s.%03d", strftime("%d/%m/%Y %H:%M:%S", localtime($time_sec)), $time_usec);

        # Read values

        $response = FGC::Sync::get($socket, $channel, "MEAS.U_LEADS");
        next if($response->{error}); # Response was an error

        print "$device_name,$time_sec.$time_usec,$time,$response->{value}\n";
    }

    FGC::Sync::disconnect($socket);

    sleep(120);
}

# EOF
