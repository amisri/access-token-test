#!/bin/sh

progpath=`dirname $0`

if [ $# -ne 1 ]; then
    echo "Usage: $0 <gateway>";
    exit
fi

gateway=$1

i=1

while [ true ]
do
    $progpath/sendcmds.pl $gateway $progpath/cmdfiles/wfiprestart

    if [ $? -ne 0 ]; then
        exit
    fi

    echo $i
    let i=i+1
done

# EOF
