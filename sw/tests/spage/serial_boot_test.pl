#!/usr/bin/perl -w
#
# Name:    serial_boot_test.pl
# Purpose: Test serial module communication with FGC boot program
# Author:  Stephen Page

use strict;

use FGC::Platform::Fgc2::Auto;
use FGC::Serial;

# Print usage if incorrect number of arguments

die "\nUsage: $0 <port name (e.g. \\\\.\\COM2 for Windows or /dev/ttyUSB0 for Linux)>\n\n" if(@ARGV < 1);
my ($port_name) = @ARGV;

# Connect to FGC

my $port = FGC::Serial::connect($port_name);

die "Unable to connect to FGC: $!\n" if(!defined($port));

# Send a command

defined(my $command = $FGC::Platform::Fgc2::Auto::boot_menu_options{FAULTS}) or die "Command not defined";
my $response = FGC::Serial::boot_cmd($port, $command);

# Print response

print   join("\n", @{$response->{value}}), "\n",
        "STATUS=$response->{status}\n";

# Disconnect from FGC

FGC::Serial::disconnect($port);

# EOF
