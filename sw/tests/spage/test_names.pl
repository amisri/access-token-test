#!/usr/bin/perl -w

use strict;

use FGC::Consts;
use FGC::Names;

sub group_test(%);
sub group_test(%)
{
    my ($group) = @_;

    print "$group->{name}\n";
    for my $gateway (values(%{$group->{gateways}}))
    {
        print "    $gateway->{name}\n";
    }

    for my $child_group (sort { $a->{name} cmp $b->{name} } values(%{$group->{groups}}))
    {
        group_test($child_group);
    }
}

# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

map { print "$_->{name}\n" } sort { $a->{name} cmp $b->{name} } (values(%$devices));

my $gateway = $gateways->{"cfc-sr4-a45a"};

FGC::Names::read_sub($devices, $gateways, $gateway->{name});

my $groups = FGC::Names::read_group($devices, $gateways);

if(!defined($groups))
{
    print "Failed to read groups\n";
    exit(1);
}

for my $device (@{$gateway->{channels}})
{
    next if(!defined($device));

    if(defined($device->{sub_devices}))
    {
        print "$device->{alias}\n";

        for(my $i = 1 ; $i <= @{$device->{sub_devices}} ; $i++)
        {
            my $sub_device = $device->{sub_devices}->[$i];
            next if(!defined($sub_device));

            printf("    %2d:%s\n", $i, $sub_device->{alias});
        }
    }
}

group_test($groups);

# EOF
