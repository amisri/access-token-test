#!/usr/bin/perl -w
#
# Name:    alarmdb_test.pl
# Purpose: Test FGC Alarm Database module
# Author:  Stephen Page

use strict;

use FGC::AlarmDB;

die "Usage: $0 <password>\n" if(@ARGV < 1);

my ($password) = @ARGV;

# Connect to database

my $dbh = FGC::AlarmDB::connect($password) or die "Unable to connect to database: $!\n";

# Get fault families

my $ff_row;
for $ff_row (@{FGC::AlarmDB::get_fault_families($dbh)})
{
    my $ff                      =  $ff_row->{FF};
    (my $fgc_platform_id = $ff) =~ s/FGC_(\d+)/$1/;

    print   "Fault family = $ff\n",
            "FGC platform = $fgc_platform_id\n",
            "\n";

    my $row;

    # Get fault codes

    for $row (@{FGC::AlarmDB::get_fault_codes($dbh, $fgc_platform_id)})
    {
        my $key;
        for $key (sort (keys(%$row)))
        {
            print "$key=\"$row->{$key}\" " if(defined($row->{$key}));
        }
        print "\n";
    }

    print "\n";

    # Get fault members

    for $row (@{FGC::AlarmDB::get_fault_members($dbh, $fgc_platform_id)})
    {
        my $key;
        for $key (sort (keys(%$row)))
        {
            print "$key=\"$row->{$key}\" " if(defined($row->{$key}));
        }
        print "\n";
    }

    print   "\n",
            "--------------------------------------------------------------------------------\n",
            "\n";
}

# Disconnect from database

FGC::AlarmDB::disconnect($dbh);

# EOF
