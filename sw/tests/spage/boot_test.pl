#!/usr/bin/perl -w
#
# Name:    boot_test.pl
# Purpose: Runs tests with the FGC Boot
# Author:  Stephen Page

use strict;

use FGC::Boot;
use FGC::Platform::Fgc2::Auto;
use FGC::Sync;

# Print usage if incorrect number of arguments

die "\nUsage: $0 <gateway> <username> <password> <channel>\n\n" if(@ARGV < 4);
my ($gateway, $username, $password, $channel) = @ARGV;

# Connect to gateway

my $socket = FGC::Sync::connect($gateway, $username, $password);
die "Unable to connect to gateway $gateway: $!\n" if($socket < 0);

# Start remote terminal

FGC::Sync::rtermstart($socket, $channel);

# Send a command

defined(my $command = $FGC::Platform::Fgc2::Auto::boot_menu_options{FAULTS}) or die "Command not defined";
my $response = FGC::Boot::command($socket, $command);

# Print response

print   join("\n", @{$response->{value}}), "\n",
        "STATUS=$response->{status}\n";

# Stop remote terminal

FGC::Sync::rtermstop($socket);

# Disconnect from gateway

FGC::Sync::disconnect($socket);

# EOF
