#!/usr/bin/perl -w

use Socket;
use strict;

use FGC::Consts;
use FGC::RBAC;
use FGC::Sub;
use FGC::Sync;

# Print usage if incorrect number of arguments

die "Usage: $0 <gateway> <username> <password> <period (20ms ticks)> <port>\n" if(@ARGV < 5);
my ($gateway, $username, $password, $period, $port_number) = @ARGV;

# Authenticate and obtain RBAC token

my $rbac_token = FGC::RBAC::authenticate($username, $password);
die "RBAC authentication failed\n" if(!defined($rbac_token));

# Create UDP socket

my $udpsock;
socket($udpsock, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

# Create address structure

my $address = sockaddr_in($port_number, INADDR_ANY);

# Bind to port

bind($udpsock, $address) or die "Unable to bind to UDP port $port_number : $!\n";

# Connect to gateway

((my $tcp_socket = FGC::Sync::connect($gateway)) >= 0) or die "Unable to connect to gateway $gateway : $!\n";

# Set RBAC token on gateway

FGC::Sync::set($tcp_socket, "", "client.token", \$rbac_token, 1);

# Set up subscription

FGC::Sync::set($tcp_socket, "", "client.udp.sub.period",    $period);
FGC::Sync::set($tcp_socket, "", "client.udp.sub.port",      $port_number);

# Receive data

my $expected_sequence   = 0;
my $sequence_errors     = 0;
while(my ($sockaddr_in, $sequence, $time_sec, $time_usec, $status) = FGC::Sub::read($udpsock))
{
    next if(!defined($sockaddr_in)); # Error receiving packet

    $sequence_errors++, warn "Unexpected sequence $sequence ($expected_sequence). $sequence_errors errors.\n" if($sequence != $expected_sequence);
    $expected_sequence = ($sequence + 1);

    my ($port, $address)    = unpack_sockaddr_in($sockaddr_in);
    my $host                = gethostbyaddr($address, PF_INET);

    printf("host=$host\nsequence=$sequence\ntime=$time_sec.%06i\n", $time_usec);

    for(my $i = 0 ; $i < FGC_MAX_DEVS_PER_GW ; $i++)
    {
        next if(!$status->[$i]->{DATA_STATUS}->{DATA_VALID});

        printf("CHANNEL=%-2i ", $i);

        for my $key (sort(keys(%{$status->[$i]})))
        {
            # Check whether field is a reference to a hash (i.e. a symlist)

            if(ref($status->[$i]->{$key}) eq "HASH")
            {
                print "$key=[";

                for my $symbol (sort(keys(%{$status->[$i]->{$key}})))
                {
                    next if($symbol eq "mask");

                    print "$symbol " if($status->[$i]->{$key}->{$symbol});
                }
                print "] ";
            }
            else # Field is not a reference to a hash
            {
                print "$key=$status->[$i]->{$key} ";
            }
        }
        print "\n";
    }
    print "********************************************************************************\n";
}
close($udpsock);
FGC::Sync::disconnect($tcp_socket);

# EOF
