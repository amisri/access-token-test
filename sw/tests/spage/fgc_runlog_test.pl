#!/usr/bin/perl -w

use FGC::Runlog;
use strict;

die "Usage: $0 <file>\n" if(@ARGV != 1);
my ($file) = @ARGV;

open(FILE, '<', $file)
    or die "Error opening file $file: $!\n";

# Read runlog file into $buffer

my $buffer;
read(FILE, $buffer, 16384);
close(FILE);

my $entries = FGC::Runlog::decode(\$buffer);

for my $entry (@$entries)
{
    print $entry->{label};
    if(defined($entry->{data}))
    {
        printf(":0x%08X", $entry->{data});
    }
    print "\n";
}

# EOF
