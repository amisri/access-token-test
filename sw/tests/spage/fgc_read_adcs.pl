#!/usr/bin/perl -w
#
# Name:     fgc_read_adcs.pl
# Purpose:  Periodically read ADC temperature data and measured values
# Author:   Stephen Page

use POSIX qw(strftime);
use strict;

use FGC::Consts;
use FGC::Sync;

die "Usage $0 <gateway> <username> <password>\n" if(@ARGV != 3);

my ($gateway, $username, $password) = @ARGV;

# Flush stdout immediately

$| = 1;

# Connect to gateway

my $socket = FGC::Sync::connect($gateway, $username, $password);
die "Unable to connect to gateway $gateway: $!\n" if($socket < 0);

print join(',', qw(
                    Device name
                    Unix time
                    Local time
                    Channel A mod barcode
                    Channel A mod temp
                    Channel A PSU temp
                    Channel A value
                    Channel B mod barcode
                    Channel B mod temp
                    Channel B PSU temp
                    Channel B value
                  ))."\n";

while(1)
{
    # Check each channel

    for my $channel (1..(FGC_MAX_DEVS_PER_GW - 1))
    {
        my $response;

        # Read device name

        $response = FGC::Sync::get($socket, $channel, "DEVICE.NAME");
        next if($response->{error}); # Response was an error
        (my $device_name = $response->{value}) =~ s/,//g;

        # Read time

        $response = FGC::Sync::get($socket, $channel, "TIME.NOW");
        next if($response->{error}); # Response was an error
        my $time = $response->{value};

        $time =~ /^\s*(\d+)\.(\d+)$/;
        my $time_sec    = $1;

        $time = strftime("%d/%m/%Y %H:%M:%S", localtime($time_sec));

        my $adcs_found = 0;

        my %buses;

        for my $bus ("A", "B")
        {
            # Read EXADC barcode

            $response = FGC::Sync::get($socket, $channel, "BARCODE.$bus.EXADC.MOD");
            next if($response->{error}); # Response was an error
            ($buses{$bus}->{barcode_mod} = $response->{value}) =~ s/,//g;

            # Skip bus if barcode is not known

            next if($buses{$bus}->{barcode_mod} eq "");

            # Read EXADC modulator temp

            $response = FGC::Sync::get($socket, $channel, "TEMP.$bus.EXADC.MOD");
            next if($response->{error}); # Response was an error
            $buses{$bus}->{temp_mod} = $response->{value};

            # Read EXADC PSU temp

            $response = FGC::Sync::get($socket, $channel, "TEMP.$bus.EXADC.PSU");
            next if($response->{error}); # Response was an error
            $buses{$bus}->{temp_psu} = $response->{value};

            # Read value

            $response = FGC::Sync::get($socket, $channel, "MEAS.$bus.VOLTS_1S");
            next if($response->{error}); # Response was an error
            $buses{$bus}->{value} = $response->{value};

            $adcs_found++;
        }

        next if(!$adcs_found);

        # Write data

        print "$device_name,$time_sec,$time";

        for my $bus ("A", "B")
        {
            if(!defined($buses{$bus}->{value}))
            {
                print ",,,";
                next;
            }

            print ",$buses{$bus}->{barcode_mod},$buses{$bus}->{temp_mod},$buses{$bus}->{temp_psu},$buses{$bus}->{value}";
        }
        print "\n";
    }

    sleep(60);
}

FGC::Sync::disconnect($socket);

# EOF
