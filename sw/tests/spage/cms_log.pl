#!/usr/bin/perl -w
#
# Name:     cms_log.pl
# Purpose:  Temporary 1Hz logging for CMS
# Author:   Stephen Page

use strict;
use Socket;

use FGC::Consts;
use FGC::Names;
use FGC::Sync;
use POSIX qw(strftime);

# Print usage if incorrect number of arguments

die "Usage: $0 <gateway> <username> <password>\n" if(@ARGV < 3);
my ($gateway, $username, $password) = @ARGV;

# Flush stdout immediately

$| = 1;

sub write_headers(%)
{
    my ($data) = @_;

    # Write data headings

    for my $key (sort keys(%$data))
    {
        print "$key,";
    }
}

sub write_data(%)
{
    my ($data) = @_;

    # Write data

    for my $key (sort keys(%$data))
    {
        # Check whether field is a reference to a hash (ie a symlist)

        if(ref($data->{$key}) eq "HASH")
        {
            for my $symbol (sort(keys(%{$data->{$key}})))
            {
                next if($symbol eq "mask");

                print "$symbol " if($data->{$key}->{$symbol});
            }
        }
        else # Field is not a reference to a hash
        {
            print "$data->{$key}";
        }
        print ",";
    }
}

# End of functions


# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

die "$gateway is not a valid gateway\n" if(!defined($gateways->{$gateway}));
$gateway = $gateways->{$gateway};

# Write data headings

# Create UDP socket

socket(my $udpsock, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

my $port_number;
for($port_number = 1024 ; $port_number <= 65535 ; $port_number++)
{
    # Bind to port

    bind($udpsock, sockaddr_in($port_number, INADDR_ANY)) && last;
}
die "Unable to bind to a free UDP port\n" if($port_number == 65536);

# Connect to gateway

my $tcp_socket;
if(($tcp_socket = FGC::Sync::connect($gateway->{name}, $username, $password)) < 0)
{
    die "Unable to connect to gateway $gateway->{name} : $!\n";
}

# Set up subscription

FGC::Sync::set($tcp_socket, "", "client.udp.sub.period",    50);
FGC::Sync::set($tcp_socket, "", "client.udp.sub.port",      $port_number);

# Receive data

my $first_cycle = 1;
while(my ($sockaddr_in, $sequence, $time_sec, $time_usec, $status) = FGC::Sync::readpub($udpsock))
{
    next if(!defined($sockaddr_in)); # Error receiving packet

    my ($port, $address)    = unpack_sockaddr_in($sockaddr_in);
    my $host                = gethostbyaddr($address, PF_INET);

    if($first_cycle)
    {
        print "SAMPLE_TIME,";
        for(my $i = 1 ; $i < FGC_MAX_DEVS_PER_GW ; $i++)
        {
            write_headers($status->[$i]) if(defined($gateway->{channels}->[$i]->{name}));
        }
        print "\n";
        $first_cycle = 0;
    }

    printf("%s.%06d,", strftime("%d/%m/%Y %H:%M:%S", localtime($time_sec)), $time_usec);
    for(my $i = 1 ; $i < FGC_MAX_DEVS_PER_GW ; $i++)
    {
        if(defined($gateway->{channels}->[$i]->{name}) && defined($status->[$i]) && $status->[$i]->{DATA_STATUS}->{CLASS_VALID})
        {
            write_data($status->[$i]);
        }
        else
        {
            for(keys(%{$status->[$i]}))
            {
                print ",";
            }
        }
    }
    print "\n";
}

close($udpsock);
FGC::Sync::disconnect($tcp_socket);

# EOF
