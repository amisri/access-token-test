#!/usr/bin/perl -w
#
# Name:    controlsdb_test.pl
# Purpose: Test FGC Controls Database module
# Author:  Stephen Page

use strict;

use FGC::ControlsDB;

# Check arguments

die "Usage: $0 <username> <password>\n" if(@ARGV < 2);
my ($username, $password) = @ARGV;

# Connect to database

my $dbh = FGC::ControlsDB::connect($username, $password) or die "Unable to connect to database: $!\n";

# Get devices

for my $device (@{FGC::ControlsDB::get_all_devices($dbh)})
{
    for my $key (sort (keys(%$device)))
    {
        print "$key=\"$device->{$key}\" " if(defined($device->{$key}));
    }
    print "\n";
}

# Disconnect from database

FGC::ControlsDB::disconnect($dbh);

# EOF
