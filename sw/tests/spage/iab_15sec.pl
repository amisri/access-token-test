#!/usr/bin/perl -w
#
# Name:     iab_15sec.pl
# Purpose:  Read I_A and I_B every 15 seconds
# Author:   Stephen Page

use strict;

use FGC::Async;
use FGC::Consts;
use FGC::Names;
use POSIX qw(strftime);

die "Usage $0 <converter name> <username> <password>\n" if(@ARGV != 3);

my ($converter_name, $username, $password) = @ARGV;

# Flush stdout immediately

$| = 1;

my ($devices, $gateways);
($devices, $gateways) = FGC::Names::read();
($devices, $gateways) = FGC::Names::read_web()  if(!defined($devices));
die "Unable to read FGC name file\n"            if(!defined($devices));

# Initialise FGC::Async module

FGC::Async::init($devices, $gateways);

my $device = $devices->{$converter_name} || die "Unknown device $converter_name\n";

# Connect to gateway

if(FGC::Async::connect($device->{gateway}, $username, $password) < 0)
{
    die "Unable to connect to gateway $device->{gateway}->{name}: $!\n";
}

print "TIME,VALUE\015\012";

while(1)
{
    my $response;

    FGC::Async::get($device, "ADC.VOLTS_200MS");

    my $commands = FGC::Async::read();

    my $error = 0;
    for my $command (values(%$commands))
    {
        if($command->{response}->{error}) # Response was an error
        {
            print "Device $command->{device}->{name} returned error \"$command->{response}->{value}\" when getting \"$command->{property}\"\n";
            $error = 1;
        }
    }

    if($error)
    {
        sleep(15);
        next;
    }

    print strftime("%d/%m/%Y %H:%M:%S", localtime), ",";

    for my $command (values(%$commands))
    {
        if($command->{property} eq "ADC.VOLTS_200MS")
        {
            print "$command->{response}->{value}\n";
        }
    }

    sleep(15);
}

# Disconnect from gateway

FGC::Async::disconnect($device->{gateway});

# EOF
