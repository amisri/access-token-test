#!/usr/bin/perl -w

use strict;
use Socket;

use FGC::Sub;
use FGC::Sync;

my $channel;
my $gateway     = "pcgw19";
my $period      = 50;
my $port_number = 2000;

# Create UDP socket

my $udpsock;
socket($udpsock, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

# Create address structure

my $address = sockaddr_in($port_number, INADDR_ANY);

# Bind to port

bind($udpsock, $address) or die "Unable to bind to UDP port $port_number : $!\n";

# Connect to gateway

my $tcp_socket;

if(($tcp_socket = FGC::Sync::connect($gateway)) < 0)
{
    die "Unable to connect to gateway $gateway : $!\n";
}

# Set up subscription

FGC::Sync::set($tcp_socket, "", "client.udp.sub.period",    $period);
FGC::Sync::set($tcp_socket, "", "client.udp.sub.port",      $port_number);

sub waitstate($$)
{
    my ($waitstate, $timeout) = @_;

    my $sockaddr_in;
    my $sequence;
    my $time_sec;
    my $time_usec;
    my $status;

    print "Wait for state $waitstate with timeout $timeout\n";

    while(($sockaddr_in, $sequence, $time_sec, $time_usec, $status) = FGC::Sub::read($udpsock))
    {
        next if(!defined($sockaddr_in)); # Error receiving packet

        my ($port, $address)    = unpack_sockaddr_in($sockaddr_in);
        my $host                = gethostbyaddr($address, PF_INET);

        next if(!defined($status->[$channel]->{status}));

        my $state = $status->[$channel]->{vs_pc_states} & 0x0F;

        last if($state == $waitstate);

        die "State timeout: wait=$waitstate, actual=$state\n" if(!$timeout--);
    }
}

sub waitcurrent($$)
{
    my ($waitcurrent, $timeout) = @_;

    print "Wait for current $waitcurrent with timeout $timeout\n";

    my $sockaddr_in;
    my $sequence;
    my $time_sec;
    my $time_usec;
    my $status;

    while(($sockaddr_in, $sequence, $time_sec, $time_usec, $status) = FGC::Sub::read($udpsock))
    {
        next if(!defined($sockaddr_in)); # Error receiving packet

        my ($port, $address)    = unpack_sockaddr_in($sockaddr_in);
        my $host                = gethostbyaddr($address, PF_INET);

        next if(!defined($status->[$channel]->{status}));

        my $current = $status->[$channel]->{imeas0};

        last if($current <= $waitcurrent);

        die "Current timeout: wait=$waitcurrent, actual=$current\n" if(!$timeout--);
    }
}

my $cycle_num = 0;
my $response;

for($channel = 1 ; ; $channel = 1 + (($channel) % 18))
{
    print "\nCycle ", ++$cycle_num, "\n\n";

    if(!(($cycle_num - $channel) % 4))
    {
        print "s $channel:system reset\n";
        $response = FGC::Sync::set($tcp_socket, $channel, "system", "reset");
        sleep(10);
    }

    print "s $channel:state.pc off\n";
    $response = FGC::Sync::set($tcp_socket, $channel, "state.pc", "off");
    die "Error [$response]\n" if($response =~ /!/);
    waitstate(1, 30);
    waitcurrent(1, 2000);

    print "s $channel:state.pc on_standby\n";
    $response = FGC::Sync::set($tcp_socket, $channel, "state.pc", "on_standby");
    die "Error [$response]\n" if($response =~ /!/);
    waitstate(6, 30);

    print "s $channel:state.pc idle\n";
    $response = FGC::Sync::set($tcp_socket, $channel, "state.pc", "idle");
    die "Error [$response]\n" if($response =~ /!/);
    waitstate(7, 10);

    print "s $channel:ref pelp,5\n";
    $response = FGC::Sync::set($tcp_socket, $channel, "ref", "pelp", "5");
    die "Error [$response]\n" if($response =~ /!/);
    waitstate(9, 10);

    print "s $channel:time.run\n";
    $response = FGC::Sync::set($tcp_socket, $channel, "time.run", "");
    die "Error [$response]\n" if($response =~ /!/);
    waitstate(10, 10);

    # Wait for converter to finish running and return to idle

    waitstate(7, 120);

    print "s $channel:state.pc on_standby\n";
    $response = FGC::Sync::set($tcp_socket, $channel, "state.pc", "on_standby");
    die "Error [$response]\n" if($response =~ /!/);
    waitstate(6, 90);

    print "s $channel:state.pc off\n";
    $response = FGC::Sync::set($tcp_socket, $channel, "state.pc", "off");
    die "Error [$response]\n" if($response =~ /!/);
    waitstate(1, 30);
}

close($udpsock);
FGC::Sync::disconnect($tcp_socket);

# EOF
