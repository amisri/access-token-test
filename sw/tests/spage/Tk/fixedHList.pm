#!/usr/local/bin/perl -w
#
# Name:    fixedHList+.pl
# Purpose: Sub-class of Tk::HList with corrected yview as Tk::HList::yview is not properly implemented
# Author:  Stephen Page
# Notes:   List item paths must be row numbers for this to work

package Tk::fixedHList;

use strict;

use base        qw(Tk::Derived Tk::HList);
use subs        qw(yview);

Construct Tk::Widget 'fixedHList';

sub yview
{
    my ($self, @args) = @_;

    # Let superclass handle all calls with arguments

    return($self->SUPER::yview(@args)) if(@args);

    my $children    = $self->info("children");
    my $numels      = defined($children) ? scalar(@$children) : 0;

    if($numels <= 1)
    {
        return(0, 1);
    }
    else
    {
        return($self->nearest(10)                       / $numels,
               ($self->nearest($self->height - 10) + 1) / $numels
              );
    }
}

# EOF
