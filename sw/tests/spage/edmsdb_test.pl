#!/usr/bin/perl -w
#
# Name:    edmsdb_test.pl
# Purpose: Test FGC EDMS database module
# Author:  Stephen Page

use strict;

use FGC::EDMSDB;

# Check arguments

# Connect to database

my $dbh = FGC::EDMSDB::connect or die "Unable to connect to database: $!\n";

# Get WorldFIP addresses

my $row;
for $row (@{FGC::EDMSDB::get_all_worldfip_addresses($dbh)})
{
    my $key;
    for $key (sort (keys(%$row)))
    {
        print "$key=\"$row->{$key}\" " if(defined($row->{$key}));
    }
    print "\n";
}
FGC::EDMSDB::disconnect($dbh);

# EOF
