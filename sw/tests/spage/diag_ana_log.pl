#!/usr/bin/perl -w
#
# Name:     diag_ana_log.pl
# Purpose:  Periodically read analogue diag data from FGC
# Author:   Stephen Page

use POSIX qw(strftime);
use strict;

use FGC::Consts;
use FGC::RBAC;
use FGC::Sync;

my $gateway;
my $password;
my $username;
my $rbac_token;
my $rbac_last_auth_time = 0;

# Authenticate and obtain RBAC token

sub get_rbac_token()
{
    $rbac_last_auth_time    = time;

    my $binary_token        = FGC::RBAC::authenticate($username, $password);
    return(1) if(!defined($binary_token));

    $rbac_token             = FGC::RBAC::decode_token(\$binary_token);
    $rbac_token->{binary}   = $binary_token;

    return(0);
}

die "Usage $0 <gateway> <username> <password>\n" if(@ARGV != 3);
($gateway, $username, $password) = @ARGV;

# Flush stdout immediately

$| = 1;

while(1)
{
    # Check whether RBAC token is less than one hour from expiry

    if(!defined($rbac_token) || time > ($rbac_token->{ExpirationTime} - 3600))
    {
        # Check whether it is at least one minute since last authentication

        if($rbac_last_auth_time < (time - 60))
        {
            print "Getting new RBAC token at ", scalar(localtime), "\n\n";

            if(get_rbac_token()) # RBAC authentication failed
            {
                warn "RBAC authentication failed at ", scalar(localtime), "\n\n";
            }
        }

        # If there is no token or token has expired, wait then retry

        sleep(60), next if(!defined($rbac_token) || $rbac_token->{ExpirationTime} < time);
    }

    # Connect to gateway

    my $socket = FGC::Sync::connect($gateway);
    die "Unable to connect to gateway $gateway: $!\n" if($socket < 0);

    # Set RBAC token on gateway

    my $response;
    eval { $response = FGC::Sync::set($socket, 0, "CLIENT.TOKEN", \$rbac_token->{binary}, 1) };
    die "Failed to send command to set RBAC token on gateway $gateway\n" if($@);
    die "Error setting RBAC token on gateway $gateway: $response->{value}\n" if($response->{error});

    # Check each channel for log data

    for my $channel (1..(FGC_MAX_DEVS_PER_GW - 1))
    {
        next if($channel != 3);

        my $response;

        # Read device name

        $response = FGC::Sync::get($socket, $channel, "DEVICE.NAME");
        next if($response->{error}); # Response was an error
        (my $device_name = $response->{value}) =~ s/,//g;

        # Read time

        $response = FGC::Sync::get($socket, $channel, "TIME.NOW");
        next if($response->{error}); # Response was an error
        my $time = $response->{value};

        $time =~ /^\s*(\d+)\.(\d+)$/;
        my $time_sec    = $1;
        my $time_usec   = $2;

        $time = sprintf("%s.%03d", strftime("%d/%m/%Y %H:%M:%S", localtime($time_sec)), $time_usec);

        # Read MEAS.V

        $response = FGC::Sync::get($socket, $channel, "MEAS.V");
        next if($response->{error}); # Response was an error
        (my $meas_v = $response->{value}) =~ s/,//g;

        # Read MEAS.I

        $response = FGC::Sync::get($socket, $channel, "MEAS.I");
        next if($response->{error}); # Response was an error
        (my $meas_i = $response->{value}) =~ s/,//g;

        # Read MEAS.I_EARTH

        $response = FGC::Sync::get($socket, $channel, "MEAS.I_EARTH");
        next if($response->{error}); # Response was an error
        (my $meas_i_earth = $response->{value}) =~ s/,//g;

        # Read analogue diag data

        $response = FGC::Sync::get($socket, $channel, "DIAG.ANA");
        next if($response->{error}); # Response was an error
        my $diag_ana = $response->{value};

        my @sub;
        my @subb;

        for(my $i = 1 ; $i <= 5 ; $i++)
        {
            $response = FGC::Sync::get($socket, $channel, "DIAG.ANA.SUB$i");
            last if($response->{error}); # Response was an error
            $sub[$i - 1] = $response->{value};

            $response = FGC::Sync::get($socket, $channel, "DIAG.ANA.SUB${i}B");
            last if($response->{error}); # Response was an error
            $subb[$i - 1] = $response->{value};
        }

        print "$device_name,$time_sec.$time_usec,$time,$meas_v,$meas_i,$meas_i_earth,";
        print "$sub[$_]," for(0..4);
        print "$subb[$_]," for(0..4);
        print "\n";
    }

    FGC::Sync::disconnect($socket);

    sleep(60);
}

# EOF
