#!/usr/bin/perl -w

use FGC::Sync;
use strict;

die "Usage $0 <gateway> <device>\n" if(@ARGV != 2);

my ($gateway, $device) = @ARGV;

# Connect to gateway

my $socket = FGC::Sync::connect($gateway);
die "Unable to connect to gateway $gateway: $!\n" if($socket < 0);

my $response = FGC::Sync::get($socket, $device, "DEVICE.NAME");
FGC::Sync::disconnect($socket);

print "Value=\"$response->{value}\"\n";

# EOF
