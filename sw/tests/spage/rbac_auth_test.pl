#!/usr/bin/perl -w
#
# Name:     rbac_auth_test.pm
# Purpose:  Authenticate to RBAC server and print resulting token contents
# Author:   Stephen Page

use FGC::RBAC;
use strict;

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n\n";

# Authenticate to RBAC server

my $token = FGC::RBAC::authenticate($username, $password)
    or die "Failed to authenticate\n";

# Print token contents

print "Original token:\n\n";
FGC::RBAC::print_token(FGC::RBAC::decode_token(\$token));
print "\n";

# Request token with reduced roles

my $reduced_token = FGC::RBAC::reduce_roles($token, ["PO-FGC-Expert", "PO-GW-Expert"])
    or die "Failed to reduce roles\n";

# Print token contents

print "Reduced token:\n\n";
FGC::RBAC::print_token(FGC::RBAC::decode_token(\$reduced_token));
print "\n";

# EOF
