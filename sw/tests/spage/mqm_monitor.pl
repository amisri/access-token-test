#!/usr/bin/perl -w
#
# Name:     mqm_monitor.pl
# Purpose:  Monitors the value of I_ERR of two MQM converters and logs when a threshold is exceeded.
# Author:   Stephen Page

use strict;
use Socket;

use FGC::Consts;
use FGC::Names;
use FGC::Sub;
use FGC::Sync;

my @logged_fields       = qw/V_REF V_MEAS I_REF I_MEAS I_ERR_MA TIMESTAMP_SEC TIMESTAMP_USEC/;  # Fields to log
my $pre_post_samples    = 500;                                                                  # Number of samples to take before and after threshold exceeded

my @converters;
my $password;
my $tolerance;
my $username;

# Print usage if incorrect number of arguments

die "Usage: $0 <username> <password> <I_ERR_MA tolerance> <device 1> <device 2>\n" if(@ARGV < 5);
($username, $password, $tolerance, $converters[0], $converters[1]) = @ARGV;


sub write_headers($%)
{
    my ($file, $converter) = @_;

    # Write data headings

    for my $key (@logged_fields)
    {
        print $file "$converter->{name}:$key,";
    }
}

sub write_data($%)
{
    my ($file, $data) = @_;

    # Write data

    for my $key (@logged_fields)
    {
        # Check whether field is a reference to a hash (ie a symlist)

        if(ref($data->{$key}) eq "HASH")
        {
            for my $symbol (sort(keys(%{$data->{$key}})))
            {
                next if($symbol eq "mask");

                print $file "$symbol " if($data->{$key}->{$symbol});
            }
        }
        else # Field is not a reference to a hash
        {
            print $file "$data->{$key}";
        }
        print $file ",";
    }
}

# End of functions


# Read FGC name data

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Check that supplied device names are valid

for(@converters)
{
    die "$_ is not a valid FGC device\n" if(!defined($devices->{$_}));
    $_ = $devices->{$_};
}

die "Devices are on different gateways\n" if($converters[0]->{gateway} ne $converters[1]->{gateway});
my $gateway = $converters[0]->{gateway};

# Create UDP socket

socket(my $udpsock, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

my $port_number;
for($port_number = 1024 ; $port_number <= 65535 ; $port_number++)
{
    # Bind to port

    bind($udpsock, sockaddr_in($port_number, INADDR_ANY)) && last;
}
die "Unable to bind to a free UDP port\n" if($port_number == 65536);

# Connect to gateway

my $tcp_socket;
if(($tcp_socket = FGC::Sync::connect($gateway->{name}, $username, $password)) < 0)
{
    die "Unable to connect to gateway $gateway->{name} : $!\n";
}

# Set up subscription

FGC::Sync::set($tcp_socket, "", "client.udp.sub.period",    1);
FGC::Sync::set($tcp_socket, "", "client.udp.sub.port",      $port_number);

# Receive data

my $file;
my $logging         = 0;
my $logging_prev    = 0;
my $post_samples    = 0;
my $sample_time     = 0;

while(my ($sockaddr_in, $sequence, $time_sec, $time_usec, $status) = FGC::Sub::read($udpsock))
{
    next if(!defined($sockaddr_in)); # Error receiving packet

    my ($port, $address)    = unpack_sockaddr_in($sockaddr_in);
    my $host                = gethostbyaddr($address, PF_INET);

    for my $converter (@converters)
    {
        $converter->{data} = $status->[$converter->{channel}];

        # Add timestamp to data

        $converter->{data}->{TIMESTAMP_SEC}     = $time_sec;
        $converter->{data}->{TIMESTAMP_USEC}    = $time_usec;

        # Create I_ERR_MA signal

        $converter->{data}->{I_ERR_MA} = int(($converter->{data}->{I_MEAS} - $converter->{data}->{I_REF}) * 1000);

        # Append data to history

        push(@{$converter->{history}}, $converter->{data});
        shift(@{$converter->{history}}) if(@{$converter->{history}} > $pre_post_samples);
    }

    # Check each converter to see whether I_ERR_MA tolerance has been exceeded

    $logging = 0;
    for my $converter (@converters)
    {
        if($converter->{data}->{I_ERR_MA} > $tolerance || $converter->{data}->{I_ERR_MA} < (0 - $tolerance)) # Tolerance has been exceeded
        {
            $post_samples = $pre_post_samples;
            $logging = 1;
            last;
        }
    }

    # Check whether post-sampling

    if(!$logging && $post_samples) # Post-samples are being taken
    {
        $post_samples--;
        $logging = 1;
    }

    # Check whether currently logging

    if($logging) # Logging is in progress
    {
        if(!$logging_prev) # This is the first logging cycle
        {
            # Open file

            my $filename = sprintf("%d.%03d.csv", $time_sec, $time_usec / 1000);
            open($file, ">", $filename) or die "Unable to open $filename for write\n";

            print "Starting logging to file $filename...\n";

            # Write data headings

            print $file "SAMPLE_TIME,";
            write_headers($file, $_) for @converters;
            print $file "\n";

            # Write pre-samples

            for(my $i = 0, $sample_time = 0 ; $i < (@{$converters[0]->{history}} - 1) ; $i++)
            {
                printf $file ("%.07E,", $sample_time);
                write_data($file, $_->{history}->[$i]) for @converters;
                print $file "\n";
                $sample_time += 0.020;
            }
        }

        printf $file ("%.07E,", $sample_time);
        write_data($file, $_->{data}) for @converters;
        print $file "\n";
        $sample_time += 0.020;
    }
    else # Not currently logging
    {
        if($logging_prev) # Logging was taking place in the previous cycle
        {
            # Close file

            close($file);

            print "Logging ended.\n";
        }
    }

    $logging_prev = $logging
}

close($udpsock);
FGC::Sync::disconnect($tcp_socket);

# EOF
