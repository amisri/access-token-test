#!/usr/bin/perl -w

use strict;

use FGC::Async;
use FGC::Names;

my $devices;
my $gateways;

# Read FGC name data

($devices, $gateways) = FGC::Names::read();
($devices, $gateways) = FGC::Names::read_web()  if(!defined($devices));
die "Unable to read FGC name file\n"            if(!defined($devices));

FGC::Async::init($devices, $gateways);
FGC::Async::connect($gateways->{pcgw19});

my $value = pack("C*", (1,2,3,4,5));

FGC::Async::set($devices->{DEVGW1}, "TEST.BIN", \$value, 1);
FGC::Async::get($devices->{DEVGW1}, "TEST.BIN BIN");

my $responses = FGC::Async::read;

FGC::Async::disconnect($gateways->{pcgw19});

for my $command (values(%$responses))
{
    print length($command->{response}->{value}), "\n";

    my (@data) = unpack("C*", $command->{response}->{value});
    print "@data\n";
}

# EOF
