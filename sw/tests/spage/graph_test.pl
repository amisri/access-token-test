#!/usr/bin/perl -w

use Socket;
use strict;

use FGC::Consts;
use FGC::Log;
use FGC::RBAC;
use FGC::Sync;
use Tk;
use Tk::PlotDataset;
use Tk::LineGraphDataset;

my $gateway = 'cfc-866-rfip2';

# Prompt for username

print "\nUsername: ";
chomp(my $username = <STDIN>);

# Prompt for password

print "Password: ";
system("stty -echo");
chomp(my $password = <STDIN>);
system("stty echo");
print "\n";
print "\n";

# Authenticate and obtain RBAC token

my $rbac_token = FGC::RBAC::authenticate($username, $password)
                 or die "RBAC authentication failed\n";

# Connect to gateway

((my $socket = FGC::Sync::connect($gateway)) >= 0)
               or die "Unable to connect to gateway $gateway : $!\n";

# Set RBAC token on gateway

FGC::Sync::set($socket, "", "client.token", \$rbac_token, 1);

my $log_name = 'IAB';

my $response = FGC::Sync::get($socket, "1", "LOG.OP.$log_name BIN");
die "ERROR: $response->{value}\n" if($response->{error});

FGC::Sync::disconnect($socket);

my $data = FGC::Log::decode(\$response->{value})
           or die "Log decode failed\n";

my @datasets;
for my $signal (sort { $a->{label} cmp $b->{label} } @{$data->{header}->{signals}})
{
    my @times;
    for(my $i = 0 ; $i < $data->{header}->{n_samples} ; $i++)
    {
        push(@times, $signal->{times}->[$i]->{sec} + ($signal->{times}->[$i]->{usec} / 1000000));

        printf("%s,%.3f%s,%d.%06d\n", $signal->{label},
                                      $signal->{samples}->[$i],
                                      $signal->{units},
                                      $signal->{times}->[$i]->{sec},
                                      $signal->{times}->[$i]->{usec});
    }

    my $dataset = LineGraphDataset->new(
                                        -name   => $signal->{label},
                                        -xData  => \@times,
                                        -yData  => $signal->{samples},
                                       );

    push(@datasets, $dataset);
}

my $mw      = new MainWindow;
my $graph   = $mw->PlotDataset(
                                -background     => 'white',
                                -height         => 600,
                                -plotTitle      => [$log_name, 15],
                                -width          => 800,
                                -xTickFormat    => '%.3f',
                                -yTickFormat    => '%.3f',
                              )->pack(-fill => 'both', -expand => 1);

$graph->addDatasets(@datasets);
$graph->plot;

MainLoop;

# EOF
