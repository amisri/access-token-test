#!/usr/bin/perl -w
#
# Name:     example.pl
# Purpose:  Example of setting converter references in perl
# Author:   Stephen Page

use strict;

use FGC::Async;
use FGC::Consts;
use FGC::Names;

my @converters;

# Read responses to commands

sub read_responses()
{
    my $commands = FGC::Async::read();

    my $error = 0;
    for my $command (values(%$commands))
    {
        if($command->{response}->{error}) # Response was an error
        {
            print "Device $command->{device}->{name} returned error \"$command->{response}->{value}\" when setting \"$command->{property}\"\n";
            $error = 1;
        }
    }

    if($error)
    {
        FGC::Async::disconnect($_->{gateway}) for(@converters);
        die "\n";
    }
}

die "Usage $0 <converter 1 name> <converter 2 name> <username> <password>\n" if(@ARGV != 4);

my ($converter_one_name, $converter_two_name, $username, $password) = @ARGV;

my ($devices, $gateways);
($devices, $gateways) = FGC::Names::read();
($devices, $gateways) = FGC::Names::read_web()  if(!defined($devices));
die "Unable to read FGC name file\n"            if(!defined($devices));

# Initialise FGC::Async module

FGC::Async::init($devices, $gateways);

$converters[0] = $devices->{$converter_one_name} || die "Unknown device $converter_one_name\n";
$converters[1] = $devices->{$converter_two_name} || die "Unknown device $converter_two_name\n";

# Connect to gateways

for my $converter (@converters)
{
    if(FGC::Async::connect($converter->{gateway}, $username, $password) < 0)
    {
        die "Unable to connect to gateway $converter->{gateway}->{name} for converter $converter->{name}: $!\n";
    }
}

# Set table references

print "Setting table reference...\n";

for my $converter (@converters)
{
    FGC::Async::set($converter, "REF.TABLE.R", [0, 1, 1,     2]);
    FGC::Async::set($converter, "REF.TABLE.T", [0, 1, 5.999, 6]);
    FGC::Async::set($converter, "REF",         "TABLE");
}
read_responses();

# Set time of ramp start

my $time = time + 10;
print "Setting ramp start to ", scalar(localtime($time)), "...\n";
FGC::Async::set($_, "TIME.RUN", $time) for(@converters);
read_responses();

# Disconnect from gateways

FGC::Async::disconnect($_->{gateway}) for(@converters);

print "DONE\n";

# EOF
