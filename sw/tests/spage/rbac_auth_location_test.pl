#!/usr/bin/perl -w
#
# Name:     rbac_auth_location_test.pm
# Purpose:  Authenticate to RBAC server by location and print resulting token contents
# Author:   Stephen Page

use FGC::RBAC;
use strict;

# Authenticate to RBAC server

my $token = FGC::RBAC::authenticate_by_location()
            or die "Failed to authenticate\n";

# Print token contents

print "Token:\n\n";
FGC::RBAC::print_token(FGC::RBAC::decode_token(\$token));
print "\n";

# EOF
