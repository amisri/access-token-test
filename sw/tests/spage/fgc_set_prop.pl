#!/usr/bin/perl -w
#
# Name:    fgc_set_prop.pl
# Purpose: Set property values on devices from file
# Author:  Stephen Page

use strict;

use FGC::Async;
use FGC::Names;
use FGC::RBAC;
use FGC::Sync;

sub read_file($)
{
    my ($filename) = @_;

    open(FILE, "<", $filename) or return(undef);

    my %values;

    while(<FILE>)
    {
        s/[\012\015]//g; # Remove newline

        my ($device, $value)    = split(",");
        $values{$device}        = $value;
    }
    close(FILE);

    return(\%values);
}

# End of functions


die "\nUsage: $0 <username> <password> <property> <value file>\n\n" if(@ARGV < 4);
my ($username, $password, $property, $filename) = @ARGV;

# Authenticate and obtain RBAC token

my $rbac_token = FGC::RBAC::authenticate($username, $password);
die "RBAC authentication failed\n" if(!defined($rbac_token));

# Read FGC name file

my ($devices, $gateways) = FGC::Names::read();
die "Unable to read FGC name file\n" if(!defined($devices));

# Read values from file

my $values = read_file($filename) or die "Error reading file $filename: $!\n";

# Initialise FGC asynchronous communication module

FGC::Async::init($devices, $gateways);

# Connect to all gateways

for my $gateway (values(%$gateways))
{
    if(FGC::Async::connect($gateway) < 0)
    {
        warn "Unable to connect to gateway $gateway->{name}\n";
        next;
    }

    my $response;
    eval { $response = FGC::Sync::set($gateway->{socket}, 0, "CLIENT.TOKEN", \$rbac_token, 1) };

    # Check whether an error occurred

    if($@) # An error occurred sending the command
    {
        print "Error sending RBAC token to $gateway->{name}\n";
        next;
    }
    elsif($response->{error}) # The device returned an error
    {
        print "Error setting RBAC token on $gateway->{name}: $response->{value}\n";
        next;
    }
}

for my $device (keys(%$values))
{
    my $value   = $values->{$device};
    $device     = $devices->{$device};

    print "$device->{name} S $property $value\n";
    eval { FGC::Async::set($device, $property, $value) };

    # Check whether an error occurred

    if($@) # An error occurred
    {
        warn "Error setting property $property for $device->{name} with value $value: $@\n";
        next;
    }
}

# Read responses

my $commands = FGC::Async::read();

for my $command (values(%$commands))
{
    if($command->{response}->{error} && $command->{response}->{value} !~ /^\d+ dev not ready$/)
    {
        warn "Error setting property $command->{property} for $command->{device}->{name} with value $command->{value}: $command->{response}->{value}\n";
        next;
    }
}

# Disconnect from all gateways

for my $gateway (values(%$gateways))
{
    FGC::Async::disconnect($gateway);
}

# EOF
