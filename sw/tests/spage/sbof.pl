#!/usr/bin/perl -w
#
# Name:    sertest.pl
# Purpose: Sends SB->OF->SB->OF sequence repeatedly to a converter
# Author:  Stephen Page

use FGC::Serial;
use strict;

# Print usage if incorrect number of arguments

die "Usage: $0 <port> <num cycles>\n" if(scalar(@ARGV) < 2);
my ($port_number, $cycles) = @ARGV;

# Connect to FGC

my $port = FGC::Serial::connect($port_number);

die "Unable to connect to FGC: $!\n" if(!defined($port));

# Command sending loop

my $response;

FGC::Serial::set($port, "PC", "OF");
sleep(2);
FGC::Serial::set($port, "PC", "OF");

my $i;
for($i = 1 ; $i <= $cycles ; $i++)
{
    $response = FGC::Serial::set($port, "PC", "OF");
    last if($response->{error});

    sleep(15);

    $response = FGC::Serial::set($port, "PC", "SB");
    last if($response->{error});

    print "$i/$cycles cycle", $cycles >= 2 ? "s" : "", "\n";

    sleep(15);

    $response = FGC::Serial::get($port, "PC");
    last if($response->{error});
    if($response->{value} ne "ON_STANDBY")
    {
        print "Error: converter state is $response->{value} (should be ON_STANDBY)\n";
        last;
    }
}

# Disconnect from FGC

FGC::Serial::disconnect($port);

if($response->{error})
{
    print "Error: $response->{value}\n";
}

# EOF
