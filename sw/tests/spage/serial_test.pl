#!/usr/bin/perl -w
#
# Name:    serial_test.pl
# Purpose: Test FGC serial port module
# Author:  Stephen Page

use strict;

use FGC::Serial;

# Connect to FGC

my $port = FGC::Serial::connect("/dev/ttyUSB0");

die "Unable to connect to FGC: $!\n" if(!defined($port));

# Set test property then read it back

my $response;
$response = FGC::Serial::set($port, "TEST.INT8U", [1,2,3,4,5]);
$response = FGC::Serial::get($port, "TEST.INT8U");
die "Failed to get property\n" if(!defined($response));

# Print value

print "Error: " if($response->{error});
print "$response->{value}\n";

# Get status data from FGC

my $status;
for(my $i = 0 ; $i < 10 ; $i++)
{
    $status = FGC::Serial::read_pub($port);

    printf("%d.%06d %.07e\n", $status->{TIME_SEC}, $status->{TIME_USEC}, $status->{I_MEAS});
}

# Disconnect from FGC

FGC::Serial::disconnect($port);

# EOF
