#!/usr/bin/perl -w

use Socket;
use strict;

use FGC::Consts;
use FGC::RBAC;
use FGC::Sub;
use FGC::Sync;

# Print usage if incorrect number of arguments

die "Usage: $0 <gateway> <username> <password> <port>\n" if(@ARGV < 4);
my ($gateway, $username, $password, $port_number) = @ARGV;

# Authenticate and obtain RBAC token

my $rbac_token = FGC::RBAC::authenticate($username, $password);
die "RBAC authentication failed\n" if(!defined($rbac_token));

# Create UDP socket

my $udpsock;
socket($udpsock, PF_INET, SOCK_DGRAM, getprotobyname('udp'));

# Create address structure

my $address = sockaddr_in($port_number, INADDR_ANY);

# Bind to port

bind($udpsock, $address) or die "Unable to bind to UDP port $port_number : $!\n";

# Connect to gateway

((my $tcp_socket = FGC::Sync::connect($gateway)) >= 0) or die "Unable to connect to gateway $gateway : $!\n";

# Set RBAC token on gateway

FGC::Sync::set($tcp_socket, "", "client.token", \$rbac_token, 1);

# Set up subscription

FGC::Sync::set($tcp_socket, "", "client.udp.sub.period",    1);
FGC::Sync::set($tcp_socket, "", "client.udp.sub.port",      $port_number);

# Clear unnecessary decode functions
# This will cause only the headers to be decoded by FGC::Sub::read() where class content not needed

for(my $i = 0 ; $i < @fgc_decode_func ; $i++)
{
    next if($i == 1);
    delete($fgc_decode_func[$i]);
}

# Compare two runlog entries

sub compare_entries(%%)
{
    my ($entry_a, $entry_b) = @_;

    if(
        $entry_a->{seq}     == $entry_b->{seq}  &&
        $entry_a->{id}      == $entry_b->{id}   &&
        $entry_a->{data}    == $entry_b->{data}
      )
    {
        return(0);
    }
    else
    {
        return(1);
    }
}

# Write an entry

sub write_entry($%)
{
    my ($address, $entry) = @_;

return if($address != 3);

    printf("%2d[%3d]: %3d, 0x%02X, 0x%08X  %s\n",
            $address,
            $entry->{idx},
            $entry->{seq},
            $entry->{id},
            $entry->{data}, $entry->{id} > 0x2C ? "******" : "");
}

# Receive data

my @channels;
while(my ($sockaddr_in, $sequence, $time_sec, $time_usec, $status) = FGC::Sub::read($udpsock))
{
    next if(!defined($sockaddr_in)); # Error receiving packet

    my ($port, $address)    = unpack_sockaddr_in($sockaddr_in);
    my $host                = gethostbyaddr($address, PF_INET);
    my $runlog_idx          = $status->[0]->{RUNLOG_IDX};
    my $runlog_el_idx       = int($runlog_idx / FGC_RUNLOG_EL_SIZE);

    #printf("host=$host\nsequence=$sequence\ntime=$time_sec.%06i\nrunlog_idx=$runlog_idx\n", $time_usec);
    #print("$runlog_idx $runlog_el_idx\n");

    for(my $address = 1 ; $address < FGC_MAX_DEVS_PER_GW ; $address++)
    {
        next if(!$status->[$address]->{DATA_STATUS}->{DATA_VALID});

        printf("IDX=%3d,CHANNEL=%2d,RUNLOG=0x%02X\n", $runlog_idx, $address, $status->[$address]->{RUNLOG_BYTE}) if($address == 3);

        $channels[$address]     = {}                if(!defined($channels[$address]));
        my $channel             = $channels[$address];
        $channel->{runlog_idx}  = $runlog_idx - 1   if(!defined($channel->{runlog_idx}));

        # Check whether byte is for expected position within element

        if($runlog_idx % FGC_RUNLOG_EL_SIZE != ($channel->{runlog_idx} + 1) % FGC_RUNLOG_EL_SIZE)
        {
            $channel->{runlog_entry}    = '';
            $channel->{runlog_idx}      = FGC_RUNLOG_EL_SIZE - 1;
            next;
        }
        $channel->{runlog_idx} = $runlog_idx;

        # Append byte to runlog entry

        $channel->{runlog_entry} .= pack('C', $status->[$address]->{RUNLOG_BYTE});

        # Check whether an entry boundary has been reached

        if(!(($runlog_idx + 1) % FGC_RUNLOG_EL_SIZE))
        {
            # Skip entry if incomplete

            $channel->{runlog_entry} = '', next if(length($channel->{runlog_entry}) != FGC_RUNLOG_EL_SIZE);

            # Decode entry

            my %entry;
            (
                $entry{seq},
                $entry{id},
                $entry{data},
            ) = unpack('CCN', $channel->{runlog_entry});
            $channel->{runlog_entry}    = '';
            $entry{idx}                 = $runlog_el_idx;

            write_entry($address, \%entry) if($address == 3);
            my $old_entry = $channel->{buffer}->[$runlog_el_idx];
            if(0 && $address == 3)
            {
                printf("%2d[%3d]: %3d %3d, 0x%02X 0x%02X, 0x%08X 0x%08X  %s\n",
                $address,
                $entry{idx},
                $old_entry->{seq},
                $entry{seq},
                $old_entry->{id},
                $entry{id},
                $old_entry->{data}, $entry{data}, $entry{id} > 0x2C ? "******" : "");
                exit if($entry{id} > 0x2C);
            }
            $channel->{buffer}->[$runlog_el_idx] = \%entry;

            if(0 && defined($channel->{buffer}->[$runlog_el_idx]))
            {
                # Queue entries from buffer waiting to be sent

                for my $buffer_entry (sort { $a->{seq} <=> $b->{seq} } grep { !defined($_->{queued}) } @{$channel->{buffer}})
                {
                    push(@{$channel->{output_queue}}, $buffer_entry);
                    $buffer_entry->{queued} = 1;
                    write_entry($address, $buffer_entry);
                }

                # Check whether entry is new

                if(compare_entries($channel->{buffer}->[$runlog_el_idx], \%entry)) # Entry is new
                {
                    $channel->{buffer}->[$runlog_el_idx] = \%entry;
                    push(@{$channel->{output_queue}}, \%entry);
                    $entry{queued} = 1;
                    write_entry($address, \%entry);
                }
            }
            else
            {
                # Add entry to buffer

                $channel->{buffer}->[$runlog_el_idx] = \%entry;
            }
        }
    }
    #print "\n";
}
close($udpsock);
FGC::Sync::disconnect($tcp_socket);

# EOF
