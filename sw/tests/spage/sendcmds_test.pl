#!/usr/bin/perl -w
#
# Name:     sendcmds.pl
# Purpose:  Reads commands from files or STDIN and submits them to a gateway
# Author:   Stephen Page

use FGC::Consts;
use FGC::RBAC;
use FGC::Sync;
use IO::Socket;
use strict;

die "\nUsage: $0 <gateway> [<file1> <file2> ...]\n\n" if(@ARGV < 2);

my $gateway = shift;
my $filename = shift;
my $cmdcount;
my $socket;

# Function to process a single line

sub processline($)
{
    $_ = shift;

    # Ignore empty lines

    if(/^\s*$/o)
    {
        return;
    }

    # Check whether command should be applied to all FGCs

    if(/\*:/o)
    {
        my $base_command = $_;

        # Send command to all FGCs

        for(1..FGC_MAX_FGCS_PER_GW)
        {
            # Substitute channel number for '*' in command

            my $command = $base_command;
            $command    =~ s/\*/$_/g;

            print $socket $command;
            $cmdcount++;
        }
    }
    else
    {
        print $socket $_;
        $cmdcount++;
    }
}

# End of functions


# Prompt for username

my $username;
warn "\nUsername:\n";
chomp($username = <STDIN>);

# Prompt for password

my $password;
warn "Password:\n";
system("stty -echo");
chomp($password = <STDIN>);
system("stty echo");
warn "\n\n";

my $binary_token = FGC::RBAC::authenticate($username, $password);
die "Failed to obtain token\n" if(!defined($binary_token));

# Open a socket to the gateway

$socket = IO::Socket::INET->new("$gateway:".FGC_FGCD_PORT)
            or die "Unable to connect to $gateway: $!\n";

# Check whether the gateway is busy

read($socket, my $status, 1, 0);
die "Gateway $gateway is busy\n" if($status eq '-');

print $socket '+';

# Set RBAC token on gateway

my $response;
eval { $response = FGC::Sync::set($socket, 0, "CLIENT.TOKEN", \$binary_token, 1) };
warn "Failed to send command to set RBAC token on gateway\n",       next if($@);
warn "Error setting RBAC token on gateway: $response->{value}\n",   next if($response->{error});

while(1)
{
    # Get the commands and submit them to the gateway

    open(FILE, '<', $filename) or die "Failed to open $filename: $!\n";

    while(<FILE>)
    {
        processline($_);
    }
    close(FILE);

    # Print responses

    my $byte;
    while(read($socket, $byte, 1, 0))
    {
        print $byte;

        # Count down remaining responses

        if($byte eq ';')
        {
            --$cmdcount or last;
        }
    }
}
close $socket;
die "Gateway closed socket unexpectedly\n";

# EOF
