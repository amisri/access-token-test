#!/usr/bin/perl -w
#
# Name:    measiab.pl
# Purpose: Read MEAS.IA/B continuously
# Author:  Stephen Page

use FGC::Names;
use FGC::Sync;

die "Usage: $0 <device> <username> <password> <channel>\n" if(@ARGV < 4);

my ($device, $username, $password, $channel) = @ARGV;

# Read FGC name data

my ($devices, $gateways)    = FGC::Names::read();
($devices, $gateways)       = FGC::Names::read_web()    if(!defined($devices));
die "Unable to read FGC name file\n"                    if(!defined($devices));
die "Unknown device\n"                                  if(!defined($devices->{$device}));
$device = $devices->{$device};

$socket = FGC::Sync::connect($device->{gateway}->{name}, $username, $password);
die "Unable to connect to gateway: $!\n" if($socket < 0);

my $ms  = 0;
my $pkt  = 0;
printf("TIME,%s:I%s,PKT_NUM\n",$device->{name},$channel);
print STDERR "Starting...\n";

# Read initial zero-length value

# FGC::Sync::get($socket, $device->{channel}, "MEAS.$channel.AMPS_1MS BIN");
while(1)
{
    # Send command to read buffer

    my $response;
    eval { $response = FGC::Sync::get($socket, $device->{channel}, "MEAS.$channel.AMPS_1MS BIN") };

    # Check whether an error occurred

    die "Error sending command to $device->{name}\n"    if($@);                                 # An error occurred sending the command
    die "Error from device: $response->{value}\n"       if($response->{error});                 # The device returned an error;
#    die "Zero-length data\n"                            if(length($response->{value}) == 0);    # Zero-length data returned

    # Byte swap and extract floats

    $pkt++;

    my (@values) = unpack("f*", pack("I*", unpack("N*", $response->{value})));

    # Print received values

    for my $value (@values)
    {
        printf("%7E,%7E,%u\n", (0.001 * $ms), $value, $pkt);
	  $ms++;
    }
}
FGC::Sync::disconnect($socket);

# EOF
