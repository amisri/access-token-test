#!/usr/bin/perl -w
#
# Name:    spy_test.pl
# Purpose: Test FGC spy module
# Author:  Stephen Page

use FGC::Spy;
use strict;

die "Usage: $0 <port (e.g. COM4)> <number of samples\n" if(@ARGV != 1);
my ($port_name, $num_samples) = @ARGV;

# Connect to spy interface

my $port = FGC::Spy::connect($port_name)
    or die "Unable to connect to FGC: $!\n";

# Read from spy interface

my $data = FGC::Spy::read($port, 1, $num_samples);

# Disconnect from spy interface

FGC::Spy::disconnect($port);

# Print header

print "TIME,";
for(my $i = 0 ; $i < FGC::Spy::FGC_N_SPY_CHANS ; $i++)
{
    print "SPY.MPX[$i],";
}
print "\n";

# Print samples

my $time = 0;
for my $sample (@$data)
{
    printf("%.03f,", $time);
    for my $field (@$sample)
    {
        printf("%.7e,", $field);
    }
    print "\n";

    # Add 1ms to $time

    $time += 0.001;
}

# EOF
