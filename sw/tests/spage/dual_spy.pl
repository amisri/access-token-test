#!/usr/bin/perl -w
#
# Name:    dual_spy.pl
# Purpose: Acquire data from 2 spy interfaces
# Author:  Stephen Page

use strict;

use FGC::Sync;

# Open configuration file

open(CONFIG, "<", "dual_spy_config.txt") or die "Unable to open dual_spy_config.txt : $!\n";

# Create a hash to contain configuration data

my %config;

# Parse configuration options

while(<CONFIG>)
{
    s/[\012\015]//g; # Remove newline
    $config{$1} = $2 if(/(\w*)=(.*)/);
}

# Close configuration file

close(CONFIG);

# Connect to gateway

my $socket = FGC::Sync::connect($config{gateway}, $config{username}, $config{password});
die "Unable to connect to gateway $config{gateway}: $!\n" if($socket < 0);

# Set spy times

my $time = time + 10;
FGC::Sync::set($socket, $config{channel1}, "time.spy", $time);
FGC::Sync::set($socket, $config{channel2}, "time.spy", $time);

# Disconnect from gateway

FGC::Sync::disconnect($socket);

# Start acquisitions

system("start perl -S fgcreadspy.pl $config{port1} $config{period} $config{numsamples} $config{file1}\n");
system("start perl -S fgcreadspy.pl $config{port2} $config{period} $config{numsamples} $config{file2}\n");

# EOF
