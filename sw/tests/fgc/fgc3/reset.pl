#!/usr/bin/perl

# File:    reset.pl
# Purpose: Test DEVICE.RESET. It should only be possible when in OFF, 
#          FLT_OFF, STOPPING and FLT_STOPPING

use Test::Utils;

use strict;
use warnings;
use diagnostics;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "SIMULATION");

# Reset the FGC when in OFF

setPc($d, "OF");
set($d, "DEVICE.RESET", "");
ok(1, "Reset in OFF");

assureMain($d);

# Reset the FGC when in FLT_OFF
# After the reset it should start in FLT_OFF

set($d, "FGC.FAULTS", "FAST_ABORT");
wait_until( sub { get($d,"STATE.PC") eq 'FLT_OFF' } );
set($d, "DEVICE.RESET", "");
ok(1, "Reset in FLT_OFF");

# Reset the FGC when in STOPPING

assureMainInOff($d, "SIMULATION");

setPc($d, "BK");
set($d, "PC", "OF");
wait_until( sub { get($d,"STATE.PC") eq 'STOPPING' } );
set($d, "DEVICE.RESET", "");
ok(1, "Reset in STOPPING");

# Reset the FGC when in FLT_STOPPING

assureMainInOff($d, "SIMULATION");

setPc($d, "BK");
set($d, "FGC.FAULTS", "VS_FAULT");
wait_until( sub { get($d,"STATE.PC") eq 'FLT_STOPPING' } );
set($d, "DEVICE.RESET", "");
ok(1, "Reset in FLT_STOPPING");

# Try to reset when in CYCLING

assureMainInOff($d, "SIMULATION");

setPc($d, "CY");
wait_until( sub { get($d,"STATE.PC") eq 'TO_CYCLING' or get($d,"STATE.PC") eq 'CYCLING' });
dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in CYCLING failed";


# Only for class 63

my $class_id = get($d, "DEVICE.CLASS_ID");

if ($class_id == "63")
{
    # Try to reset the FGC when in STANDBY

    assureMainInOff($d, "SIMULATION");

    setPc($d, "SB");
    wait_until( sub { get($d,"STATE.PC") eq 'ON_STANDBY' } );
    set($d, "DEVICE.RESET", "");
    ok(1, "Reset in ON_STANDBY");

    # Try to reset the FGC when in IDLE

    assureMainInOff($d, "SIMULATION");

    setPc($d, "IL");
    dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in IDLE failed";

    # Try to reset the FGC when in ARMED

    assureMainInOff($d, "SIMULATION");
    
    setPc($d, "IL");
    set($d, "REF.FUNC.REG_MODE" , "I");
    set($d, "REF.TABLE.FUNCTION", "0|0,0.5|5,3.5|5,4|0");
    wait_until( sub { get($d,"STATE.PC") eq 'ARMED' } );
    dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in ARMED failed";

    # Try to reset the FGC when in RUNNING

    assureMainInOff($d, "SIMULATION");

    setPc($d, "IL");
    set($d, "REF.FUNC.REG_MODE" , "I");
    set($d, "REF.TABLE.FUNCTION", "0|0,0.5|5,3.5|5,4|0");
    set($d, "REF.RUN", "");
    wait_until( sub { get($d,"STATE.PC") eq 'RUNNING' } );
    dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in RUNNING failed";

    # Try to reset when in DIRECT

    assureMainInOff($d, "SIMULATION");

    setPc($d, "DT");
    dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in DIRECT failed";
}

setPc($d, "OF");


# EOF