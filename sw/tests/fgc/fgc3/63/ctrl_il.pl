#!/usr/bin/perl

# File:    ctrl_cyc.pl
# Purpose: Tests the control when in cycling

use Test::Utils;


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Prepare FGC3

if (index($d, "RFNA") != -1) 
{
    assureMainInOff($d, "SIMULATION");
    set($d, "MEAS.SIM", "ENABLED");
}
else
{
    assureMainInOff($d, "NORMAL");
    set($d, "MEAS.SIM", "DISABLED");
}

# # Load config file

load_config($d, 'config/rpaan.txt');

# Start

set($d, "VS", "RESET");

# Go to IDLE

set($d, "REG.MODE", "V");

setPc($d, "IL");

set($d, "REF.FUNC.PLAY", "ENABLED");

# Test voltage regulation mode

set($d, "REG.MODE", "V");

# Use REF NOW,ref

set($d, "REF", "NOW,0");
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

set($d, "REF", "NOW,2");
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
wait_until( sub { (abs(get($d, "MEAS.V.VALUE") - '2.0') < 0.1)}, 3, 0.2 );
ok(1,'IDLE (V): REF NOW,2');

set($d, "REF", "NOW,0");
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
wait_until( sub { (abs(get($d, "MEAS.V.VALUE") - '0.0') < 0.1)}, 3, 0.2 );
ok(1,'IDLE (V): REF NOW,0');

# Use function TABLE 

set($d, "REF.TABLE.FUNC.VALUE", "0.0|0.0,0.2|0.0,0.4|2,0.6|2,0.8|0.0");
set($d, "REF.FUNC.TYPE", "TABLE");

wait_until( sub { get($d,"STATE.PC") eq 'ARMED' } );

set($d, "REF.RUN", "");

wait_until( sub { get($d,"STATE.PC") eq 'RUNNING' } );
wait_until( sub { (abs(get($d, "MEAS.V.VALUE") - '2.0') < 0.1)}, 3, 0.2 );

wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
wait_until( sub { (abs(get($d, "MEAS.V.VALUE") - '0.0') < 0.1)}, 3, 0.2 );

ok(1,'IDLE (V): TABLE');


# Test current regulation mode

set($d, "REG.MODE", "I");

# Use REF NOW,ref

set($d, "REF", "NOW,0");
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

set($d, "REF", "NOW,10");
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
wait_until( sub { (abs(get($d, "MEAS.I.VALUE") - '10.0') < 0.1)}, 3, 0.2 );
ok(1,'IDLE (I): REF NOW,10');

set($d, "REF", "NOW,0");
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
wait_until( sub { (abs(get($d, "MEAS.I.VALUE") - '0.0') < 0.1)}, 3, 0.2 );
ok(1,'IDLE (I): REF NOW,0');

# Use function TABLE 

set($d, "REF.TABLE.FUNC.VALUE", "0.0|0.0,0.2|0.0,0.4|2,0.6|2,0.8|0.0");
set($d, "REF.FUNC.TYPE", "TABLE");
wait_until( sub { get($d,"STATE.PC") eq 'ARMED' } );

set($d, "REF.RUN", "");

wait_until( sub { get($d,"STATE.PC") eq 'RUNNING' } );
wait_until( sub { (abs(get($d, "MEAS.I.VALUE") - '2.0') < 0.1)}, 3, 0.2 );

wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
wait_until( sub { (abs(get($d, "MEAS.I.VALUE") - '0.0') < 0.1)}, 3, 0.2 );

ok(1,'IDLE (I): TABLE');

# Test field regulation mode

# Use REF NOW,ref

set($d, "REF", "NOW,0");
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
# set($d, "REG.MODE", "B");

# set($d, "REF", "NOW,0.1");
# wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
# wait_until( sub { (abs(get($d, "MEAS.B.VALUE") - '0.1') < 0.01)}, 3, 0.2 );
# ok(1,'IDLE (B): REF NOW,0.1');

# set($d, "REF", "NOW,0");
# wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
# wait_until( sub { (abs(get($d, "MEAS.B.VALUE") - '0.0') < 0.01)}, 3, 0.2 );
# ok(1,'IDLE (B): REF NOW,0');

# Use function TABLE 

# set($d, "REF.TABLE.FUNC.VALUE", "0.0|0.0,0.2|0.0,0.4|0.02,0.6|0.02,0.8|0.0");
# set($d, "REF.FUNC.TYPE", "TABLE");
# wait_until( sub { get($d,"STATE.PC") eq 'ARMED' } );

# set($d, "REF.RUN", "");

# wait_until( sub { get($d,"STATE.PC") eq 'RUNNING' } );
# wait_until( sub { (abs(get($d, "MEAS.B.VALUE") - '0.02') < 0.01)}, 3, 0.2 );

# wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );
# wait_until( sub { (abs(get($d, "MEAS.B.VALUE") - '0.0') < 0.01)}, 3, 0.2 );

# ok(1,'IDLE (B): TABLE');

# Reset state

set($d, "PC", "OF");


# EOF
