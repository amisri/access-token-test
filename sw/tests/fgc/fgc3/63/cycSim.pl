#!/usr/bin/perl

# File:    cyc_sim.pl
# Purpose: Tests FGC.CYC.SIM propertY

use Test::Utils;

use Time::HiRes qw(usleep time);

use strict;
use warnings;
use diagnostics;
use Switch;


sub getUser 
{
    my ($d) = @_;

    my $log = get($d, "LOG.TIMING");

    my @log_values = split /,/, $log;

    my @cycle_info = split /\s+/, $log_values[scalar @log_values - 1];

    my $cyc_sel;
    my $log_ssc;

    if (length($cycle_info[1]) > 2) 
    {
        my @cyc_sel_array = split /|/, $cycle_info[1];

        $cyc_sel = $cyc_sel_array[1];
        $log_ssc = $cycle_info[2];
    }
    else
    {
        $cyc_sel = $cycle_info[2];
        $log_ssc = $cycle_info[3]; 
    }

    my ($log_user, $ssc) = ($cyc_sel, $log_ssc);

    return ($log_user, $ssc);
}



sub getIMeas
{
    my ($d, $event_type, $bp, $user) = @_;

    my $log = get($d, sprintf("log.oasis.i_meas.data(%d)", $user));

    my @current_info = split /,/, $log;

    my $counter = 0;

    # The values returned correspond to the event times specified in init_class.c for
    # injection and extraction events.

    switch ($event_type) 
    {   
        case 0  {return $current_info[280]}
        case 1  {return $current_info[$bp * 1200 - 200]}
        case 2  {return $current_info[1]}
    }
}



sub syncUser 
{
    my ($d) = @_;

    my ($user, $ssc) = getUser($d);

    if ($user eq "7") 
    {
        return 1;
    }
    else
    {
        return 0;
    }
}



sub checkUser 
{
    my ($d, $user, $bp, $i, $start) = @_;
    
    my ($log_user, $ssc) = getUser($d);

    cmp_ok($log_user, 'eq', $user);

    checkSSC($i, $ssc);

    my $end = time();

    usleep($bp * 1200 * 1000 - (($end - $start) * 1000 * 1000));
}



sub checkUserWithEvent 
{
    my ($d, $user, $bp_wait, $i, $event_type, $bp_cycle, $start) = @_;

    my $end;
    
    my ($log_user, $ssc) = getUser($d);

    cmp_ok($log_user, 'eq', $user);

    checkSSC($i, $ssc);
    
    my $currentStr = getIMeas($d, $event_type, $bp_cycle, $user);

    my $test_value;

    $test_value = $user * 1.0;

    my $current = $currentStr * 1.0;

    cmp_ok($current, '>', $test_value - 0.1);
    cmp_ok($current, '<', $test_value + 0.1);

    $end = time();

    usleep($bp_wait * 1200 * 1000 - (($end - $start) * 1000 * 1000));
}



sub checkSSC 
{
    my ($i, $ssc) = @_;

    if ($i == 0) 
    {
        cmp_ok($ssc, 'eq', "SSC");
    }
    else
    {
        cmp_ok($ssc, 'ne', "SSC");
    }
}



sub checkTimingEventWarning
{
    my ($d, $on) = @_;

    my $warnings_log = get($d,"STATUS.WARNINGS");

    my @warnings = split /\s+/, $warnings_log;

    my $te_warning = "OFF";

    for (my $i = 0; $i < scalar @warnings; $i++) {
        if ($warnings[$i] eq "TIMING_EVT") 
        {
            $te_warning = $warnings[$i];
        }
    }

    if ($on eq "ON") 
    {
        cmp_ok($te_warning, 'eq', "TIMING_EVT");
    }
    else
    {
        cmp_ok($te_warning, 'eq', "OFF");
    }
}



sub syncCycles
{
    my ($d) = @_;

    my ($user, $ssc) = getUser($d);

    while ($user eq "7")
    {
        ($user, $ssc) = getUser($d);
    }

    my $counter = 0;
    my $user_found = 0;

    print "Synching cycles...\n";

    while ($counter < 6000000) 
    {
        $user_found = syncUser($d);
        if ($user_found == 1) 
        {
            last;
        }
        $counter = $counter + 1000;
        usleep(1000);
    }
}


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};

# assureMainInOff($d, "SIMULATION");
set($d, "PC", "OFF");
wait_until(sub { get($d,"STATE.PC") eq ("OFF") }, 20, 0.5);

set($d, "FGC.CYC.SIM", "");
sleep(5);
checkTimingEventWarning($d, "OFF");


set($d, "PC", "SB");
wait_until(sub { get($d,"STATE.PC") eq ("ON_STANDBY") }, 20, 0.5);


# First cycle is used for synching the simulation.
# If changed, the value in subroutine syncUser() must also be changed.

my $super_cycle = "0701,2002,1803,1702,1605,1502";

my @cycles = split /,/, $super_cycle;

my $cycle_user;

# Set up a pulse function for each user with amplitude corresponding to the user's number

for (my $i = 0; $i < scalar @cycles; $i++) {
    
    $cycle_user = int($cycles[$i]/100);

    set($d, sprintf("ref.func.play(%d)", $cycle_user), "enabled");
    set($d, sprintf("ref.func.reg_mode(%d)", $cycle_user), "i");
    set($d, sprintf("ref.pulse.duration(%d)", $cycle_user), "0.2");
    set($d, sprintf("ref.pulse.ref.value(%d)", $cycle_user), sprintf("%d", $cycle_user));
    set($d, sprintf("ref.func.type(%d)", $cycle_user), "pulse");
}

set($d, "ref.event_cyc", "injection");


set($d, "FGC.CYC.SIM", $super_cycle);
sleep(5);
checkTimingEventWarning($d, "ON");

set($d, "PC", "CY");

wait_until(sub { get($d,"STATE.PC") eq ("TO_CYCLING") }, 120, 0.1);


print "Beginning the test for injection events.\n";
# sleep(30);
syncCycles($d);

my $start;
my $end;

my $counter_sc = 0;

for (my $i = 0; $i < scalar @cycles; $i++) 
{
    # If it is the last cycle in the super-cycle

    if ($i == (scalar @cycles - 1)) 
    {
        printf("USER: %d\n", int($cycles[$i]/ 100));
        $start = time();
        checkUserWithEvent($d, int($cycles[$i]/ 100), int($cycles[0]%100), $i, $counter_sc, int($cycles[$i]%100), $start);
        # $end = time();
        # printf("%.2f\n", $end - $start);
        
        $i = -1;
        $counter_sc++;

        if ($counter_sc == 1) 
        {
            print "Beginning the test for extraction events.\n";
            set($d, "PC", "SB");
            wait_until(sub { get($d,"STATE.PC") eq ("ON_STANDBY") }, 120, 0.1); 
            set($d, "ref.event_cyc", "ejection");    
            set($d, "PC", "CY"); 
            wait_until(sub { get($d,"STATE.PC") eq ("TO_CYCLING") }, 120, 0.1);
            syncCycles($d);
        }
        elsif ($counter_sc == 2) 
        {
            print "Beginning the test for start cycle events.\n";
            set($d, "PC", "SB");
            wait_until(sub { get($d,"STATE.PC") eq ("ON_STANDBY") }, 120, 0.1); 
            set($d, "ref.event_cyc", "start_cycle");    
            set($d, "PC", "CY"); 
            wait_until(sub { get($d,"STATE.PC") eq ("TO_CYCLING") }, 120, 0.1);
            syncCycles($d);
        }
        else
        {
            last;
        }
    }
    else
    {
        printf("USER: %d\n", int($cycles[$i]/ 100));
        $start = time();
        checkUserWithEvent($d, int($cycles[$i]/ 100), int($cycles[$i + 1]%100), $i, $counter_sc, int($cycles[$i]%100), $start);
        # $end = time();
        # printf("%.2f\n", $end - $start);
    }
}
