#!/usr/bin/perl

# File:    ctrl_dt.pl
# Purpose: Tests the control when in DIRECT

use POSIX qw/floor/;
use Test::Utils;


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Prepare FGC3

if (index($d, "RFNA") != -1) 
{
    assureMainInOff($d, "SIMULATION");
    set($d, "MEAS.SIM", "ENABLED");
}
else
{
    assureMainInOff($d, "NORMAL");
    set($d, "MEAS.SIM", "DISABLED");
}

# Load config file

load_config($d, 'config/rpaan.txt');

# Start

set($d, "VS", "RESET");

# Set the values to zero

set($d, "REF.DIRECT.V.VALUE", "0");
set($d, "REF.DIRECT.I.VALUE", "0");
set($d, "REF.DIRECT.B.VALUE", "0");

setPc($d, "DT");

# Field regulation mode

my $ref_b = floor((get($d,"LIMITS.OP.B.POS") / 5));

set($d, "REG.MODE", "B");
set($d, "REF.DIRECT.B.VALUE", $ref_b);

wait_until( sub { (abs(get($d, "MEAS.B.VALUE") - $ref_b) < 0.01)}, 10, 0.5);
ok(1,'REF.DIRECT.B.VALUE');

set($d, "REF.DIRECT.B.VALUE", "0");

# Current regulation mode

my $ref_i = floor((get($d,"LIMITS.OP.I.POS") / 5));

set($d, "REG.MODE", "I");
set($d, "REF.DIRECT.I.VALUE", $ref_i);

wait_until( sub { (abs(get($d, "MEAS.I.VALUE") - $ref_i) < 0.01)}, 10, 0.5);
ok(1,'REF.DIRECT.I.VALUE');

set($d, "REF.DIRECT.I.VALUE", "0");

# Voltage regulation mode

my $ref_v = floor((get($d,"LIMITS.OP.V.POS") / 5));

set($d, "REG.MODE", "V");
set($d, "REF.DIRECT.V.VALUE", $ref_v);

wait_until( sub { (abs(get($d, "MEAS.V.VALUE") - $ref_v) < 0.1)}, 10, 0.5);
ok(1,'REF.DIRECT.V.VALUE');

set($d, "REF.DIRECT.V.VALUE", "0");

# Test regulation mode transitions

set($d, "REF.DIRECT.V.VALUE", $ref_v);
set($d, "REF.DIRECT.I.VALUE", $ref_i);
set($d, "REF.DIRECT.B.VALUE", $ref_b);

# V -> I

set($d, "REG.MODE", "I");
wait_until( sub { (abs(get($d, "MEAS.I.VALUE") - $ref_i) < 0.01)}, 10, 0.5);
ok(1,'V -> I');

# I -> V

set($d, "REG.MODE", "V");
wait_until( sub { (abs(get($d, "MEAS.V.VALUE") - $ref_v) < 0.1)}, 10, 0.5);
ok(1,'I -> V');

# V -> B

# set($d, "REG.MODE", "B");
# wait_until( sub { (abs(get($d, "MEAS.B.VALUE") - $ref_b) < 0.01)}, 10, 0.5);
# ok(1,'V -> B');

# B -> I

set($d, "REG.MODE", "I");
wait_until( sub { (abs(get($d, "MEAS.I.VALUE") - $ref_i) < 0.01)}, 10, 0.5);
ok(1,'B -> I');

# I -> B

# set($d, "REG.MODE", "B");
# wait_until( sub { (abs(get($d, "MEAS.B.VALUE") - $ref_b) < 0.01)}, 10, 0.5);
# ok(1,'I -> B');

# B -> V

set($d, "REG.MODE", "V");
wait_until( sub { (abs(get($d, "MEAS.V.VALUE") - $ref_v) < 0.1)}, 10, 0.5);
ok(1,'B -> V');

# Reest ref values

set($d, "REG.MODE", "I");

set($d, "REF.DIRECT.V.VALUE", "0");
set($d, "REF.DIRECT.I.VALUE", "0");
set($d, "REF.DIRECT.B.VALUE", "0");

set($d, "PC", "OF");

# EOF
