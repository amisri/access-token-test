#!/usr/bin/perl

# File:    digitalIo.pl
# Purpose: Tests the inputs and outputs of the FGC in simulation, depending on STATE.PC.

use Test::Utils;

use strict;
use warnings;
use diagnostics;
use Switch;



sub testInputs
{
    my ($d, $state) = @_;

    my $inputs_log = get($d, "DIG.STATUS");

    printf("%s: %s\n",$state, $inputs_log);

    my @inputsTotal = split /\s+/, $inputs_log;

    my @inputs;
    my $j = 0;

    for (my $i = 0; $i < scalar @inputsTotal; $i++) 
    {
        if (   $inputsTotal[$i] eq "OP_BLOCKED"
            || $inputsTotal[$i] eq "PC_PERMIT"
            || $inputsTotal[$i] eq "VS_RUN"
            || $inputsTotal[$i] eq "VS_READY"
            || $inputsTotal[$i] eq "VS_POWER_ON") 
        {
            $inputs[$j] = $inputsTotal[$i];
            $j++;
        }
    }

    switch($state)
    {
        case "OFF"          {cmp_ok(scalar @inputs, '==', 2);
                             cmp_ok($inputs[0], 'eq', "OP_BLOCKED");
                             cmp_ok($inputs[1], 'eq', "PC_PERMIT");}
        
        case "BLOCKING"     {cmp_ok(scalar @inputs, '==', 5);
                             cmp_ok($inputs[0], 'eq', "OP_BLOCKED");
                             cmp_ok($inputs[1], 'eq', "PC_PERMIT");
                             cmp_ok($inputs[2], 'eq', "VS_READY");
                             cmp_ok($inputs[3], 'eq', "VS_POWER_ON");
                             cmp_ok($inputs[4], 'eq', "VS_RUN");}
        
        case "DIRECT"       {cmp_ok(scalar @inputs, '==', 4);
                             cmp_ok($inputs[0], 'eq', "PC_PERMIT");
                             cmp_ok($inputs[1], 'eq', "VS_READY");
                             cmp_ok($inputs[2], 'eq', "VS_POWER_ON");
                             cmp_ok($inputs[3], 'eq', "VS_RUN");}
        
        case "IDLE"         {cmp_ok(scalar @inputs, '==', 4);
                             cmp_ok($inputs[0], 'eq', "PC_PERMIT");
                             cmp_ok($inputs[1], 'eq', "VS_READY");
                             cmp_ok($inputs[2], 'eq', "VS_POWER_ON");
                             cmp_ok($inputs[3], 'eq', "VS_RUN");}
        
        case "ON_STANDBY"   {cmp_ok(scalar @inputs, '==', 4);
                             cmp_ok($inputs[0], 'eq', "PC_PERMIT");
                             cmp_ok($inputs[1], 'eq', "VS_READY");
                             cmp_ok($inputs[2], 'eq', "VS_POWER_ON");
                             cmp_ok($inputs[3], 'eq', "VS_RUN");}
        
        case "CYCLING"      {cmp_ok(scalar @inputs, '==', 4);
                             cmp_ok($inputs[0], 'eq', "PC_PERMIT");
                             cmp_ok($inputs[1], 'eq', "VS_READY");
                             cmp_ok($inputs[2], 'eq', "VS_POWER_ON");
                             cmp_ok($inputs[3], 'eq', "VS_RUN");}
        
        case "STARTING"     {cmp_ok(scalar @inputs, '==', 4);
                             cmp_ok($inputs[0], 'eq', "OP_BLOCKED");
                             cmp_ok($inputs[1], 'eq', "PC_PERMIT");
                             cmp_ok($inputs[2], 'eq', "VS_POWER_ON");
                             cmp_ok($inputs[3], 'eq', "VS_RUN");}
        
        case "STOPPING"     {cmp_ok(scalar @inputs, '==', 3);
                             cmp_ok($inputs[0], 'eq', "OP_BLOCKED");
                             cmp_ok($inputs[1], 'eq', "PC_PERMIT");
                             cmp_ok($inputs[2], 'eq', "VS_POWER_ON");}

        case "FLT_STOPPING" {cmp_ok(scalar @inputs, '==', 3);
                             cmp_ok($inputs[0], 'eq', "OP_BLOCKED");
                             cmp_ok($inputs[1], 'eq', "PC_PERMIT");
                             cmp_ok($inputs[2], 'eq', "VS_POWER_ON");}

        case "FLT_OFF"      {cmp_ok(scalar @inputs, '==', 2);
                             cmp_ok($inputs[0], 'eq', "OP_BLOCKED");
                             cmp_ok($inputs[1], 'eq', "PC_PERMIT");}
    }
}



sub testOutputs
{
    my ($d, $state) = @_;

    my $outputs_log = get($d, "DIG.COMMANDS");

    printf("%s: %s\n",$state, $outputs_log);

    my @outputsTotal = split /\s+/, $outputs_log;

    my @outputs;
    my $j = 0;

    for (my $i = 0; $i < scalar @outputsTotal; $i++) 
    {
        if (   $outputsTotal[$i] eq "FGC_OK"
            || $outputsTotal[$i] eq "UNBLOCK"
            || $outputsTotal[$i] eq "VS_RUN") 
        {
            $outputs[$j] = $outputsTotal[$i];
            $j++;
        }
    }


    switch($state)
    {
        case "OFF"          {cmp_ok(scalar @outputs, '==', 1);
                             cmp_ok($outputs[0], 'eq', "FGC_OK");}
        
        case "BLOCKING"     {cmp_ok(scalar @outputs, '==', 2);
                             cmp_ok($outputs[0], 'eq', "VS_RUN");
                             cmp_ok($outputs[1], 'eq', "FGC_OK");}
        
        case "DIRECT"       {cmp_ok(scalar @outputs, '==', 3);
                             cmp_ok($outputs[0], 'eq', "VS_RUN");
                             cmp_ok($outputs[1], 'eq', "UNBLOCK");
                             cmp_ok($outputs[2], 'eq', "FGC_OK");}
        
        case "IDLE"         {cmp_ok(scalar @outputs, '==', 3);
                             cmp_ok($outputs[0], 'eq', "VS_RUN");
                             cmp_ok($outputs[1], 'eq', "UNBLOCK");
                             cmp_ok($outputs[2], 'eq', "FGC_OK");}
        
        case "ON_STANDBY"   {cmp_ok(scalar @outputs, '==', 3);
                             cmp_ok($outputs[0], 'eq', "VS_RUN");
                             cmp_ok($outputs[1], 'eq', "UNBLOCK");
                             cmp_ok($outputs[2], 'eq', "FGC_OK");}
        
        case "CYCLING"      {cmp_ok(scalar @outputs, '==', 3);
                             cmp_ok($outputs[0], 'eq', "VS_RUN");
                             cmp_ok($outputs[1], 'eq', "UNBLOCK");
                             cmp_ok($outputs[2], 'eq', "FGC_OK");}
        
        case "STARTING"     {cmp_ok(scalar @outputs, '==', 2);
                             cmp_ok($outputs[0], 'eq', "VS_RUN");
                             cmp_ok($outputs[1], 'eq', "FGC_OK");}
        
        case "STOPPING"     {cmp_ok(scalar @outputs, '==', 1);
                             cmp_ok($outputs[0], 'eq', "FGC_OK");}

        case "FLT_STOPPING" {cmp_ok(scalar @outputs, '==', 1);
                             cmp_ok($outputs[0], 'eq', "FGC_OK");}

        case "FLT_OFF"      {cmp_ok(scalar @outputs, '==', 1);
                             cmp_ok($outputs[0], 'eq', "FGC_OK");}
    }
}



# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};


# STATE.PC OFF

set($d, "PC", "OF");
wait_until(sub { get($d,"STATE.PC") eq ("OFF") }, 120, 0.1);
testInputs($d, "OFF");
testOutputs($d, "OFF");

# STATE.PC STARTING AND BLOCKING

set($d, "VS.BLOCKABLE", "ENABLED");
set($d, "PC", "BK");
wait_until(sub { get($d,"STATE.PC") eq ("STARTING") }, 120, 0.1);
wait_until(sub { get($d,"VS.STATE") eq ("STARTING") }, 120, 0.1);
testInputs($d, "STARTING");
testOutputs($d, "STARTING");
wait_until(sub { get($d,"STATE.PC") eq ("BLOCKING") }, 120, 0.1);
testInputs($d, "BLOCKING");
testOutputs($d, "BLOCKING");

# STATE.PC DIRECT

set($d, "PC", "DT");
wait_until(sub { get($d,"STATE.PC") eq ("DIRECT") }, 120, 0.1);
testInputs($d, "DIRECT");
testOutputs($d, "DIRECT");

# STATE.PC IDLE

set($d, "PC", "IL");
wait_until(sub { get($d,"STATE.PC") eq ("IDLE") }, 120, 0.1);
testInputs($d, "IDLE");
testOutputs($d, "IDLE");

# STATE.PC ON_STANDBY

set($d, "PC", "SB");
wait_until(sub { get($d,"STATE.PC") eq ("ON_STANDBY") }, 120, 0.1);
testInputs($d, "ON_STANDBY");
testOutputs($d, "ON_STANDBY");

# STATE.PC CYCLING

set($d, "PC", "CY");
wait_until(sub { get($d,"STATE.PC") eq ("CYCLING") || get($d,"STATE.PC") eq ("TO_CYCLING") }, 120, 0.1);
testInputs($d, "CYCLING");
testOutputs($d, "CYCLING");

# STATE.PC STOPPING

set($d, "PC", "OF");
wait_until(sub { get($d,"STATE.PC") eq ("STOPPING") }, 120, 0.1);
testInputs($d, "STOPPING");
testOutputs($d, "STOPPING");
wait_until(sub { get($d,"STATE.PC") eq ("OFF") }, 120, 0.1);

# STATE.PC FLT_STOPPING AND FLT_OFF

set($d, "PC", "IL");
wait_until(sub { get($d,"STATE.PC") eq ("IDLE") }, 120, 0.1);
set($d, "FGC.FAULTS", "FAST_ABORT");
wait_until(sub { get($d,"STATE.PC") eq ("FLT_STOPPING") }, 120, 0.1);
testInputs($d, "FLT_STOPPING");
testOutputs($d, "FLT_STOPPING");
wait_until(sub { get($d,"STATE.PC") eq ("FLT_OFF") }, 120, 0.1);
testInputs($d, "FLT_OFF");
testOutputs($d, "FLT_OFF");