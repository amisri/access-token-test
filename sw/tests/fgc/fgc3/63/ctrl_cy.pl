#!/usr/bin/perl

# File:    ctrl_cy.pl
# Purpose: Tests the control when in CYCLING

use Test::Utils;


sub getLastCycSel
{
    my ($d) = @_;

    # LOG.TIMING format: '1508340216.065000  2     I TABLE    OK'

    my $log_timing = get($d, "LOG.TIMING[47]");

    return (substr $log_timing, 21, 2);
}


sub checkCycle
{
    my ($d) = @_;

    my $cyc_sel         = getLastCycSel($d);
    my $cyc_status      = get($d, "REF.CYC.STATUS($cyc_sel)");;
    my $cyc_max_abs_err = get($d, "REF.CYC.MAX_ABS_ERR($cyc_sel)");;

    cmp_ok($cyc_max_abs_err, '<',  0.1, "REF.CYC.MAX_ABS_ERR($cyc_sel) < 0.1");
    cmp_ok($cyc_status,      'eq', "REG_OK");
}


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Prepare FGC3

if (index($d, "RFNA") != -1) 
{
    assureMainInOff($d, "SIMULATION");
    set($d, "MEAS.SIM", "ENABLED");
}
else
{
    assureMainInOff($d, "NORMAL");
    set($d, "MEAS.SIM", "DISABLED");
}

# Load config file

load_config($d, 'config/rpaan.txt');

# Start

set($d, "VS", "RESET");

# Set up function with TABLE

for my $i (1..24)
{
    set($d, "REF.FUNC.PLAY($i)", "ENABLED");
    set($d, "REF.TABLE.FUNC.VALUE($i)", "0.0000|0.00000E+00,0.4000|1.00000E+01,0.8000|1.00000E+01,1.1000|0.00000E+00");
    set($d, "REF.FUNC.TYPE($i)", "TABLE");
}

setPc($d, "CY");

# Assume a cycle duration of 1 BP: 1.2 seconds.  

for my $i (1..10)
{
    checkCycle($d);
    sleep(1.2);
}

setPc($d, "BK");

sleep(2);

setPc($d, "CY");

for my $i (1..10)
{
    checkCycle($d);
    sleep(1.2);
}


# Mix regulation modes in the same super-cycle

setPc($d, "SB");

set($d, "FGC.CYC.SIM", "0101,0201,0301,0201,0101,0301,0101");

# Voltage regulation

set($d, "MODE.REG_CYC", "V");
set($d, "REF.TABLE.FUNC.VALUE(1)", "0.0000|0.00000E+00,0.2000|1.00000E+00,0.6000|1.00000E+00,0.8000|0.00000E+00");

# Current regulation

set($d, "MODE.REG_CYC", "I");
set($d, "REF.TABLE.FUNC.VALUE(2)", "0.0000|0.00000E+00,0.2000|1.00000E+01,0.6000|1.00000E+01,0.8000|0.00000E+00");

# Field regulation

# set($d, "MODE.REG_CYC", "B");
# set($d, "REF.TABLE.FUNC.VALUE(3)", "0.0000|0.00000E+00,0.2000|1.00000E+00,0.6000|1.00000E+00,0.8000|0.00000E+00");

setPc($d, "CY");

for my $i (1..20)
{
    checkCycle($d);
    sleep(1.2);
}

# Reset state

setPc($d, "BK");
set($d, "FGC.CYC.SIM", "");
setPc($d, "OF");


# EOF
