#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;



use constant CYCLE_START_EVENT  => 12;
use constant PAUSE_EVENT        => 13;
use constant RESUME_EVENT       => 14;



sub waitForStatePc
{
    my ($d, $state) = @_;

    wait_until( sub { get($d, "STATE.PC") =~ /$state/ }, 60, 1 );
    ok(1, "$state");
}



sub waitForRefI
{
    my ($d, $ref) = @_;

    wait_until( sub { abs(get($d, "REF.I") - $ref) < 0.001 });
}



sub testSupportForPausedInFunclists
{
    my ($d) = @_;

    assureMainInOff($d, "SIMULATION");

    my @ref_vector1  = (0, 10, 20);
    my @time_vector1 = (0, 1,  2 );

    my @ref_vector2  = (20, 10, 0);
    my @time_vector2 = (0,  1,  2);

    my $function1 = zipTable(\@time_vector1, \@ref_vector1);
    my $function2 = zipTable(\@time_vector2, \@ref_vector2);

    my $funclist = "<<$function1>,<$function2>>";

    set($d, "REF.TABLE.FUNCLIST(1)", $funclist);

    my $result = get($d, "REF.TABLE.FUNCLIST(1)");

    is("<$result>", $funclist, "REF.TABLE.FUNCLIST supports paused syntax");

    set($d, "PC", "CY");
    wait_until( sub { get($d, "STATE.PC") =~ /CYCLING|TO_CYCLING/ } );
}



sub sendEvent
{
    my ($g, $event_type, $payload, $delay_ms) = @_;

    set($g, "FIELDBUS.EVENT", "$event_type,$payload,$delay_ms");
}



sub testPausedTimingEvents
{
    my ($d) = @_;

    assureMainInOff($d, "SIMULATION");

    set($d, "FGC.CYC.SIM", "");

    my @ref_vector  = (0, 10, 10, 0);
    my @time_vector = (0, 1,  9,  10);

    my $function = zipTable(\@time_vector, \@ref_vector);

    set($d, "REF.FUNC.REG_MODE(1)",  "I");
    set($d, "REF.TABLE.FUNCTION(1)", $function);
    set($d, "REF.FUNC.PLAY(1)",      "ENABLED");

    my $result = get($d, "REF.TABLE.FUNCTION(1)");

    is($result, $function, "REF.TABLE.FUNCTION set");

    set($d, "PC", "CY");
    waitForStatePc($d, "TO_CYCLING");

    my $g = get($d, "DEVICE.HOSTNAME");

    sendEvent($g, CYCLE_START_EVENT, 1, 1000);
    sleep(1);
    waitForStatePc($d, "CYCLING");

    sendEvent($g, PAUSE_EVENT, 0, 1000);
    waitForStatePc($d, "PAUSED");

    sendEvent($g, RESUME_EVENT, 0, 1000);
    waitForStatePc($d, "CYCLING");
}



sub testFunclistSinglePause
{
    my ($d) = @_;

    assureMainInOff($d, "SIMULATION");

    set($d, "FGC.CYC.SIM", "");

    my @ref_vector1  = (0, 10);
    my @time_vector1 = (0, 5);

    my @ref_vector2  = (10, 0);
    my @time_vector2 = (0,  5);

    my $function1 = zipTable(\@time_vector1, \@ref_vector1);
    my $function2 = zipTable(\@time_vector2, \@ref_vector2);

    my $funclist = "<<$function1>,<$function2>>";

    set($d, "REF.FUNC.REG_MODE(1)",  "I");
    set($d, "REF.TABLE.FUNCLIST(1)", $funclist);
    set($d, "REF.FUNC.PLAY(1)",      "ENABLED");

    my $result = get($d, "REF.TABLE.FUNCLIST(1)");

    is("<$result>", $funclist, "REF.TABLE.FUNCTION set");

    set($d, "PC", "CY");
    waitForStatePc($d, "TO_CYCLING");

    my $g = get($d, "DEVICE.HOSTNAME");

    sendEvent($g, CYCLE_START_EVENT, 1, 1000);
    sleep(1);
    waitForStatePc($d, "CYCLING");
    waitForStatePc($d, "PAUSED");

    waitForRefI($d, 10);

    sendEvent($g, RESUME_EVENT, 0, 1000);
    waitForStatePc($d, "CYCLING");
}



sub testFunclistMultiplePauses
{
    my ($d) = @_;

    assureMainInOff($d, "SIMULATION");

    set($d, "FGC.CYC.SIM", "");

    my $pause1_ref = 2;
    my $pause2_ref = 3;
    my $pause3_ref = 4;

    my @ref_vector1  = (0, $pause1_ref);
    my @time_vector1 = (0, 2);

    my @ref_vector2  = ($pause1_ref, $pause2_ref);
    my @time_vector2 = (0,           2);

    my @ref_vector3  = ($pause2_ref, $pause3_ref);
    my @time_vector3 = (0,           2);

    my @ref_vector4  = ($pause3_ref, 0);
    my @time_vector4 = (0,           2);

    my $function1 = zipTable(\@time_vector1, \@ref_vector1);
    my $function2 = zipTable(\@time_vector2, \@ref_vector2);
    my $function3 = zipTable(\@time_vector3, \@ref_vector3);
    my $function4 = zipTable(\@time_vector4, \@ref_vector4);

    my $funclist = "<<$function1>,<$function2>,<$function3>,<$function4>>";

    set($d, "REF.FUNC.REG_MODE(1)",  "I");
    set($d, "REF.TABLE.FUNCLIST(1)", $funclist);
    set($d, "REF.FUNC.PLAY(1)",      "ENABLED");

    my $result = get($d, "REF.TABLE.FUNCLIST(1)");

    is("<$result>", $funclist, "REF.TABLE.FUNCTION set");

    set($d, "PC", "CY");
    waitForStatePc($d, "TO_CYCLING");

    my $g = get($d, "DEVICE.HOSTNAME");

    sendEvent($g, CYCLE_START_EVENT, 1, 1000);
    sleep(1);
    waitForStatePc($d, "CYCLING");

    waitForStatePc($d, "PAUSED");
    waitForRefI($d, $pause1_ref);

    sendEvent($g, RESUME_EVENT, 0, 1000);
    waitForStatePc($d, "CYCLING");

    waitForStatePc($d, "PAUSED");
    waitForRefI($d, $pause2_ref);

    sendEvent($g, RESUME_EVENT, 0, 1000);
    waitForStatePc($d, "CYCLING");

    waitForStatePc($d, "PAUSED");
    waitForRefI($d, $pause3_ref);

    sendEvent($g, RESUME_EVENT, 0, 1000);
    waitForStatePc($d, "CYCLING");
}



sub testPausedUseArmNowDisabled
{
    my ($d) = @_;

    assureMainInOff($d, "SIMULATION");

    set($d, "FGC.CYC.SIM", "");

    my @ref_vector1  = (0, 10);
    my @time_vector1 = (0, 5);

    my @ref_vector2  = (10, 0);
    my @time_vector2 = (0,  5);

    my $function1 = zipTable(\@time_vector1, \@ref_vector1);
    my $function2 = zipTable(\@time_vector2, \@ref_vector2);

    my $funclist = "<<$function1>,<$function2>>";

    set($d, "REF.FUNC.REG_MODE(1)",  "I");
    set($d, "REF.TABLE.FUNCLIST(1)", $funclist);
    set($d, "REF.FUNC.PLAY(1)",      "ENABLED");
    set($d, "MODE.USE_ARM_NOW",      "DISABLED");

    my $result = get($d, "REF.TABLE.FUNCLIST(1)");

    is("<$result>", $funclist, "REF.TABLE.FUNCTION set");

    set($d, "PC", "CY");
    waitForStatePc($d, "TO_CYCLING");

    my $g = get($d, "DEVICE.HOSTNAME");

    sendEvent($g, CYCLE_START_EVENT, 1, 1000);
    sleep(1);
    waitForStatePc($d, "CYCLING");
    waitForStatePc($d, "PAUSED");

    waitForRefI($d, 10);

    @ref_vector2  = (8, 0);
    @time_vector2 = (0, 5);

    $function2 = zipTable(\@time_vector2, \@ref_vector2);

    $funclist = "<<$function1>,<$function2>>";

    set($d, "REF.TABLE.FUNCLIST(1)", $funclist);

    # The reference shouldn't change

    sleep(3);
    waitForRefI($d, 10);

    sendEvent($g, RESUME_EVENT, 0, 1000);

    waitForStatePc($d, "CYCLING");
}



sub testPausedUseArmNowEnabled
{
    my ($d) = @_;

    assureMainInOff($d, "SIMULATION");

    set($d, "FGC.CYC.SIM", "");

    my @ref_vector1  = (0, 10);
    my @time_vector1 = (0, 5);

    my @ref_vector2  = (10, 0);
    my @time_vector2 = (0,  5);

    my $function1 = zipTable(\@time_vector1, \@ref_vector1);
    my $function2 = zipTable(\@time_vector2, \@ref_vector2);

    my $funclist = "<<$function1>,<$function2>>";

    set($d, "REF.FUNC.REG_MODE(1)",  "I");
    set($d, "REF.TABLE.FUNCLIST(1)", $funclist);
    set($d, "REF.FUNC.PLAY(1)",      "ENABLED");
    set($d, "MODE.USE_ARM_NOW",      "ENABLED");

    my $result = get($d, "REF.TABLE.FUNCLIST(1)");

    is("<$result>", $funclist, "REF.TABLE.FUNCTION set");

    set($d, "PC", "CY");
    waitForStatePc($d, "TO_CYCLING");

    my $g = get($d, "DEVICE.HOSTNAME");

    sendEvent($g, CYCLE_START_EVENT, 1, 1000);
    sleep(1);
    waitForStatePc($d, "CYCLING");
    waitForStatePc($d, "PAUSED");

    waitForRefI($d, 10);

    @ref_vector2  = (8, 0);
    @time_vector2 = (0, 5);

    $function2 = zipTable(\@time_vector2, \@ref_vector2);

    $funclist = "<<$function1>,<$function2>>";

    set($d, "REF.TABLE.FUNCLIST(1)", $funclist);

    waitForRefI($d, 8);

    sendEvent($g, RESUME_EVENT, 0, 1000);

    waitForStatePc($d, "CYCLING");
}



my %params = (device=>undef, config=>'', protocol=>'');
   %params = parse_opts %params;

my $d = $params{device};

authenticate($params{authenticate});

testSupportForPausedInFunclists($d);
testPausedTimingEvents($d);
testFunclistSinglePause($d);
testFunclistMultiplePauses($d);
testPausedUseArmNowDisabled($d);
testPausedUseArmNowEnabled($d);

# EOF
