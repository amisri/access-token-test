#!/usr/bin/perl

use Test::Utils;
use Test::InterFGC;
use List::Util qw(shuffle);
use Array::Utils qw(array_diff intersect);

use strict;
use warnings;
use diagnostics;



sub testFieldReg
{
    my (@devices) = @_;

    # Check that each of the devices can handle field regulation

    foreach my $d (@devices)
    {
        assureMainInOff($d, "SIMULATION");

        setPc($d, "IL");

        my $final_ref = 5;

        set($d, "REF.FUNC.REG_MODE", "B");
        set($d, "REF", "NOW,$final_ref");
        sleep(1);

        fgc_wait_until(sub { get($d, "STATE.PC") eq "IDLE" });

        cmp_ok(get($d, "REF.B"), "==", $final_ref, "Final reference reached");

        my $b_meas = get($d, "MEAS.B");

        cmp_ok($b_meas, "<", $final_ref + 0.1, "MEAS_B matches the final reference");
        cmp_ok($b_meas, ">", $final_ref - 0.1, "MEAS_B matches the final reference");
    }
}



sub rampTo
{
    my ($d, $final_ref, $reg_mode) = @_;

    my %xcv_props =
    (
        V => "VCV",
        I => "CCV",
        B => "BCV",
    );

    my $property = $xcv_props{$reg_mode};

    set($d, "REF.FUNC.REG_MODE", $reg_mode);
    setPc($d, "DT");
    set($d, "REF.$property.VALUE", $final_ref);
    fgc_wait_until( sub { get($d, "REF.$reg_mode") == $final_ref } );
}



sub testMeasBasic
{
    my ($producer, $consumer, $signal, $reg_mode, $final_ref) = @_;

    assureMainInOff($producer, "SIMULATION");
    assureMainInOff($consumer, "SIMULATION");

    setRole($producer, "PRODUCER", [$consumer]);
    setSignals($producer, $signal);
    set($producer, "MEAS.SIM", "ENABLED");

    if($signal eq "B_MEAS")
    {
        set($consumer, "ADC.INTERNAL.SIGNALS", "I_DCCT_A,I_DCCT_B,V_MEAS,NONE");
    }
    elsif($signal eq "I_MEAS")
    {
        set($consumer, "ADC.INTERNAL.SIGNALS", "NONE,NONE,V_MEAS,B_PROBE_A");
    }
    else
    {
        set($consumer, "ADC.INTERNAL.SIGNALS", "I_DCCT_A,I_DCCT_B,NONE,B_PROBE_A");
    }

    setRole($consumer, "CONSUMER", [$signal . "\@" . $producer]);
    set($consumer, "MEAS.SIM", "DISABLED");

    setPc($consumer, "OF");
    setPc($producer, "OF");

    # Ramp producer to requested reference with desired reg mode

    rampTo($producer, $final_ref, $reg_mode);

    # Check if measurement on the consumer matches the requested reference

    my $consumer_meas = get($consumer, "MEAS.$reg_mode");

    cmp_ok($consumer_meas, "<", $final_ref + 0.1, "$signal on consumer ok");
    cmp_ok($consumer_meas, ">", $final_ref - 0.1, "$signal on consumer ok");
}



sub testSignalBMeasBasic
{
    my ($producer, $consumer) = @_;

    testMeasBasic($producer, $consumer, "B_MEAS", "B", 3);
}



sub testSignalIMeasBasic
{
    my ($producer, $consumer) = @_;

    testMeasBasic($producer, $consumer, "I_MEAS", "I", 6);
}



sub testSignalVLoadBasic
{
    my ($producer, $consumer) = @_;

    testMeasBasic($producer, $consumer, "V_LOAD", "V", 5);
}



sub armTable
{
    my ($d, $reg_mode, $function) = @_;

    set($d, "REF.FUNC.REG_MODE",  $reg_mode);
    set($d, "REF.TABLE.FUNCTION", $function);
    fgc_wait_until(sub { get($d, "STATE.PC") eq "ARMED" });
}



sub testFieldRegRemoteBMeas
{
    my ($producer, $consumer) = @_;

    assureMainInOff($producer, "SIMULATION");
    assureMainInOff($consumer, "SIMULATION");

    # Configure producer

    setRole($producer, "PRODUCER", [$consumer]);
    setSignals($producer, "B_MEAS,I_MEAS,V_LOAD");
    set($consumer, "MEAS.SIM", "ENABLED");

    # Configure consumer

    setRole($consumer, "CONSUMER", ["B_MEAS\@" . $producer,
                                    "I_MEAS\@" . $producer,
                                    "V_LOAD\@" . $producer]);
    set($consumer, "MEAS.SIM", "DISABLED");

    setPc($consumer, "OF");
    setPc($producer, "OF");

    # Put all devices in IDLE

    setPc($producer, "IDLE");
    setPc($consumer, "IDLE");

    # Prepare reference function

    my $reg_mode = "B";

    my $ref_vector  = [0, 10, 10, 0 ];
    my $time_vector = [0, 10, 20, 30];

    my $function = zipTable($time_vector, $ref_vector);

    # Arm the same function on all devices

    armTable($producer, $reg_mode, $function);
    armTable($consumer, $reg_mode, $function);

    my $g = get($producer, "DEVICE.HOSTNAME");

    # Send an event to all devices - this allows to have the
    # reference functions synchronised

    set($g, "FIELDBUS.EVENT", "2,0,1000");

    fgc_wait_until(sub { get($producer, "STATE.PC") eq "RUNNING" });
    fgc_wait_until(sub { get($consumer, "STATE.PC") eq "RUNNING" });

    fgc_wait_until(sub { get($producer, "STATE.PC") eq "IDLE" });
    fgc_wait_until(sub { get($consumer, "STATE.PC") eq "IDLE" });
}



sub testFieldRegRemoteBProbe
{
    my ($producer, $consumer_a, $consumer_b) = @_;

    assureMainInOff($producer,   "SIMULATION");
    assureMainInOff($consumer_a, "SIMULATION");
    assureMainInOff($consumer_b, "SIMULATION");

    # Configure producer

    setRole($producer, "PRODUCER", [$consumer_a, $consumer_b]);
    setSignals($producer, "B_PROBE_A,B_PROBE_B,I_MEAS,V_LOAD");
    set($producer, "MEAS.SIM", "ENABLED");

    # Configure consumer A

    setRole($consumer_a, "CONSUMER", ["B_PROBE_A\@" . $producer,
                                      "I_MEAS\@"    . $producer,
                                      "V_LOAD\@"    . $producer]);
    set($consumer_a, "MEAS.SIM", "DISABLED");
    set($consumer_a, "B_PROBE.SELECT", "A");

    # Configure consumer B

    setRole($consumer_b, "CONSUMER", ["B_PROBE_B\@" . $producer,
                                      "I_MEAS\@"    . $producer,
                                      "V_LOAD\@"    . $producer]);
    set($consumer_b, "MEAS.SIM", "DISABLED");
    set($consumer_b, "B_PROBE.SELECT", "B");

    setPc($producer,   "OFF");
    setPc($consumer_a, "OFF");
    setPc($consumer_b, "OFF");

    # Put all devices in IDLE

    setPc($producer,   "IDLE");
    setPc($consumer_a, "IDLE");
    setPc($consumer_b, "IDLE");

    # Prepare reference function

    my $reg_mode = "B";

    my $ref_vector  = [0, 10, 10, 0 ];
    my $time_vector = [0, 10, 20, 30];

    my $function = zipTable($time_vector, $ref_vector);

    # Arm the same function on all devices

    armTable($producer,   $reg_mode, $function);
    armTable($consumer_a, $reg_mode, $function);
    armTable($consumer_b, $reg_mode, $function);

    my $g = get($producer, "DEVICE.HOSTNAME");

    # Send an event to all devices - this allows to have the
    # reference functions synchronised

    set($g, "FIELDBUS.EVENT", "2,0,100");

    fgc_wait_until(sub { get($producer,   "STATE.PC") eq "RUNNING" });
    fgc_wait_until(sub { get($consumer_a, "STATE.PC") eq "RUNNING" });
    fgc_wait_until(sub { get($consumer_b, "STATE.PC") eq "RUNNING" });

    fgc_wait_until(sub { get($producer,   "STATE.PC") eq "IDLE" });
    fgc_wait_until(sub { get($consumer_a, "STATE.PC") eq "IDLE" });
    fgc_wait_until(sub { get($consumer_b, "STATE.PC") eq "IDLE" });
}



my %params = (master=>undef, slaves=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my @devices = ("RFNA.866.01.RETH5", "RFNA.866.02.RETH5", "RFNA.866.03.RETH5");

my @tests =
(
    \&testFieldReg,
    \&testSignalBMeasBasic,
    \&testSignalIMeasBasic,
    \&testSignalVLoadBasic,
    \&testFieldRegRemoteBMeas,
    \&testFieldRegRemoteBProbe,
);

foreach my $test (@tests)
{
    lives_ok
    {
        prepareTest(@devices, "RFNA.866.11.RETH5");
        setBroadcast(\@devices, "ENABLED");
        $test->(@devices);
    };
}

# EOF
