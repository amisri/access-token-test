#!/usr/bin/perl

use Test::Utils;
use Data::Dumper;

use strict;
use warnings;
use diagnostics;



sub getRandomDevice
{
    my ($gateway) = @_;

    my @devices = getDevicesFromGateway($gateway);

    return $devices[rand @devices];
}



sub getRandomDevices
{
    my ($gateway, $num_elements) = @_;

    my @devices = ();

    for(my $i = 0; $i < $num_elements; $i++)
    {
        push @devices, getRandomDevice(get($gateway, "DEVICE.HOSTNAME"));
    }

    return @devices;
}



sub getRandomSignal
{
    my @signals = qw(I_MEAS B_MEAS);

    return $signals[rand @signals];
}



sub getRandomSignalDevice
{
    my ($gateway) = @_;

    my $device = getRandomDevice($gateway);

    return getRandomSignal() . "@" . $device;
}



sub getRandomSignalDevices
{
    my ($gateway, $num_elements) = @_;

    my @devices = getRandomDevices($gateway, $num_elements);

    my @sig_devs = map { getRandomSignal() . "@" . $_ } @devices;

    return @sig_devs;
}



sub testNameOk
{
    my ($d, $name, $label) = @_;

    set($d, "TEST.DEV_NAME", $name);
    is(get($d, "TEST.DEV_NAME"), uc($name), $label);
}



sub testNameNotOk
{
    my ($d, $name, $exception, $label) = @_;

    throws_ok { set($d, "TEST.DEV_NAME", $name) } qr/$exception/, $label;
    isnt(get($d, "TEST.DEV_NAME"), $name, "Dev name not set");
}



sub testSignalNameOk
{
    my ($d, $name, $label) = @_;

    set($d, "TEST.DEV_NAME_SYM", $name);
    is(get($d, "TEST.DEV_NAME_SYM"), uc($name), $label);
}



sub testSignalNameNotOk
{
    my ($d, $name, $exception, $label) = @_;

    throws_ok { set($d, "TEST.DEV_NAME_SYM", $name) } qr/$exception/, $label;
    isnt(get($d, "TEST.DEV_NAME_SYM"), $name, "Dev name not set");
}



sub testValidName
{
    my ($d) = @_;

    my $name = "rFnA.866.05.REtH5";

    testNameOk($d, $name,     "Valid name mixed case");
    testNameOk($d, uc($name), "Valid name upper case");
    testNameOk($d, lc($name), "Valid name lower case");
}



sub testNameNotInNameDb
{
    my ($d) = @_;

    my $name = "RFNA.866.09.ETH5";

    testNameNotOk($d, $name, "unknown dev", "Name not in NameDB");
}



sub testNameLength
{
    my ($d) = @_;

    my $name = "01234567890123456789012";

    testNameNotOk($d, $name, "unknown dev", "Name max length");

    $name = "012345678901234567890123";

    testNameNotOk($d, $name, "dev name too long", "Name too long");

    $name = "012345678901234567890123456789012345678901234567890123456789";

    testNameNotOk($d, $name, "dev name too long", "Name too long");
}



sub testValidMac
{
    my ($d) = @_;

    my $mac1 = "02-aB-Cd-ef-76-F0";

    testNameOk($d, $mac1,     "Valid MAC (digits and letters) mixed case");
    testNameOk($d, uc($mac1), "Valid MAC (digits and letters) upper case");
    testNameOk($d, lc($mac1), "Valid MAC (digits and letters) lower case");

    my $mac2 = "aB-Cd-EF-ab-cd-EF";

    testNameOk($d, $mac2,     "Valid MAC (letters only) mixed case");
    testNameOk($d, uc($mac2), "Valid MAC (letters only) upper case");
    testNameOk($d, lc($mac2), "Valid MAC (letters only) lower case");

    my $mac3 = "01-23-45-67-89-01";

    testNameOk($d, $mac3,     "Valid MAC (digits only)");
}



sub testInvalidMac
{
    my ($d) = @_;

    my $mac = "02-aBb-Cd-ef-76-F0";

    testNameNotOk($d, $mac, "bad address", "Too many digits");

    $mac = "01-23-45-67-89-0106";

    testNameNotOk($d, $mac, "bad address", "Too many digits");

    $mac = "01-23-45-67-89-1";

    testNameNotOk($d, $mac, "bad address", "Too few digits");

    $mac = "01-23--67-89-10";

    testNameNotOk($d, $mac, "bad address", "Empty octets");

    $mac = "01-23-45";

    testNameNotOk($d, $mac, "bad address", "Missing octets");

    $mac = "01-23-45-67-89-01-23";

    testNameNotOk($d, $mac, "bad address", "Extra octets");

    $mac = "01";

    testNameNotOk($d, $mac, "unknown dev", "Missing octet delimiter");

    $mac = "02-aG-Cd-ef-76-F0";

    testNameNotOk($d, $mac, "bad address", "Digits out of range");
}



sub testValidSignalAtName
{
    my ($d) = @_;

    my $name = "I_MEAS\@RFNA.866.05.RETH5";

    testSignalNameOk($d, $name, "Signal with valid name");

    $name = "I_MEAS\@RFNA.866.09.ETH2";

    testSignalNameNotOk($d, $name, "unknown dev", "Signal with invalid name");
}



sub testValidSignalAtMac
{
    my ($d) = @_;

    my $name = "I_MEAS\@ab-cd-ef-01-23-45";

    testSignalNameOk($d, $name, "Signal with valid MAC");

    $name = "I_MEAS\@ab-cd-ef-01-23-45jj";

    testSignalNameNotOk($d, $name, "bad address", "Signal with invalid MAC");

    $name = "I_MEAS\@ab-";

    testSignalNameNotOk($d, $name, "bad address", "Signal with invalid MAC");
}



sub testSignalWithEmptyDev
{
    my ($d) = @_;

    my $name = "I_MEAS\@";

    testSignalNameNotOk($d, $name, "unknown dev", "Signal without device name");
}



sub testEmptySignal
{
    my ($d) = @_;

    my $name = "\@ab-cd-ef-10-32-54";

    testSignalNameNotOk($d, $name, "unknown sym", "Empty signal");
}



sub testSignalNameLength
{
    my ($d) = @_;

    my $name = "I_MEAS\@01234567890123456789012";

    testSignalNameNotOk($d, $name, "unknown dev", "Name max length");

    $name = "I_MEAS\@012345678901234567890123";

    testSignalNameNotOk($d, $name, "dev name too long", "Name too long");

    $name = "I_MEAS\@012345678901234567890123456789012345678901234567890123456789";

    testSignalNameNotOk($d, $name, "dev name too long", "Name too long");

    $name = "01234567890123456789\@RFNA.866.05.ETH2";

    testSignalNameNotOk($d, $name, "unknown sym", "Signal max length");

    $name = "012345678901234567890\@RFNA.866.05.ETH2";

    testSignalNameNotOk($d, $name, "unknown sym", "Signal too long");

    $name = "01234567890123456789012345678901234567890123456789\@RFNA.866.05.ETH2";

    testSignalNameNotOk($d, $name, "unknown sym", "Signal too long");
}



sub testSignalNotInSymlist
{
    my ($d) = @_;

    my $name = "kkk\@RFNA.866.05.ETH2";

    testSignalNameNotOk($d, $name, "unknown sym", "Invalid signal");
}



sub prepareDevName
{
    my ($d, @devices) = @_;

    my $value = join ',', @devices;

    set($d, "TEST.DEV_NAME", $value);
    is(get($d, "TEST.DEV_NAME"), $value, "TEST.DEV_NAME prepared");
    cmp_ok(getPropertyInfo($d, "TEST.DEV_NAME")->{n_elements}, "==", scalar @devices);
}



sub prepareDevNameSym
{
    my ($d, @sig_devs) = @_;

    my $value = join ',', @sig_devs;

    set($d, "TEST.DEV_NAME_SYM", $value);
    is(get($d, "TEST.DEV_NAME_SYM"), $value, "TEST.DEV_NAME_SYM prepared");
}



sub getRand
{
    my ($min, $max) = @_;

    return int(rand($max - $min)) + $min;
}




sub testArrayDevName
{
    my ($d) = @_;

    my $gateway = get($d, "DEVICE.HOSTNAME");
    my $property = "TEST.DEV_NAME";
    my @devices;
    my $max_elements = getPropertyInfo($d, $property)->{max_elements};
    my $num_elements;

    # Test array clearing

    $num_elements = getRand(1, $max_elements);
    @devices      = getRandomDevices($gateway, $num_elements);
    prepareDevName($d, @devices);
    testNameOk($d, "", "Array cleared");
    cmp_ok(getPropertyInfo($d, $property)->{n_elements}, "==", 0);

    # Test array shrinking

    $num_elements = getRand(1, $max_elements);
    @devices      = getRandomDevices($gateway, $num_elements);
    prepareDevName($d, @devices);
    pop @devices;
    testNameOk($d, join(',', @devices), "Array shrinked");
    cmp_ok(getPropertyInfo($d, $property)->{n_elements}, "==", $num_elements - 1);

    # Set middle array element to null element

    $num_elements = getRand(3, $max_elements);
    @devices      = getRandomDevices($gateway, $num_elements);
    prepareDevName($d, @devices);
    set($d, $property . "[1]", "");
    is(get($d, $property), join(',', @devices), "Array element didn't change");
    cmp_ok(getPropertyInfo($d, $property)->{n_elements}, "==", $num_elements);

    # Set the first element to null element

    $num_elements = getRand(1, $max_elements);
    @devices      = getRandomDevices($gateway, $num_elements);
    prepareDevName($d, @devices);
    set($d, $property . "[0]", "");
    is(get($d, $property), "", "Array cleared");
    cmp_ok(getPropertyInfo($d, $property)->{n_elements}, "==", 0);

    # Set the last element to null element

    $num_elements = getRand(2, $max_elements);
    @devices      = getRandomDevices($gateway, $num_elements);
    prepareDevName($d, @devices);
    set($d, $property . "[" . ($num_elements - 1) . "]", "");
    is(get($d, $property), join(',', @devices), "Array element didn't change");
    cmp_ok(getPropertyInfo($d, $property)->{n_elements}, "==", $num_elements);

    # Set value at index > num_elements + 1

    $num_elements = getRand(1, $max_elements - 2);
    @devices      = getRandomDevices($gateway, $num_elements);
    prepareDevName($d, @devices);
    my $device = getRandomDevice($gateway);
    set($d, $property . "[" . ($num_elements + 1) . "]", $device);
    push @devices, "";
    push @devices, $device;
    is(get($d, $property), join(',', @devices), "Array element set");
    cmp_ok(getPropertyInfo($d, $property)->{n_elements}, "==", $num_elements + 2);

    # Check element omitting

    $num_elements = getRand(3, $max_elements);
    @devices      = getRandomDevices($gateway, 3);
    prepareDevName($d, @devices);
    my @values = getRandomDevices($gateway, 3);
    $values[1] = "";
    set($d, $property, join(',', @values));
    $values[1] = $devices[1];
    is(get($d, $property), join(',', @values), "Array element omitted");
    cmp_ok(getPropertyInfo($d, $property)->{n_elements}, "==", $num_elements);

    # Add empty element at the end of array

    $num_elements = getRand(1, $max_elements - 1);
    @devices      = getRandomDevices($gateway, $num_elements);
    prepareDevName($d, @devices);
    push @devices, "";
    set($d, $property, join(',', @devices));
    is(get($d, $property), join(',', @devices), "Array extended with empty name");
    cmp_ok(getPropertyInfo($d, $property)->{n_elements}, "==", $num_elements + 1);
}



sub testArrayDevNameSym
{
    my ($d) = @_;

    my $gateway = get($d, "DEVICE.HOSTNAME");
    my @sig_devs;
    my $name;

    # Test property crearing

    @sig_devs = getRandomSignalDevices($gateway, 3);
    prepareDevNameSym($d, @sig_devs);
    $name = "";
    testSignalNameOk($d, $name, "Array cleared");

    # Test array shrinking

    @sig_devs = getRandomSignalDevices($gateway, 3);
    prepareDevNameSym($d, @sig_devs);
    pop @sig_devs;
    testSignalNameOk($d, join(',', @sig_devs), "Array shrinked");

    # Set element with index that is neither the last nor the first

    @sig_devs = getRandomSignalDevices($gateway, 3);
    prepareDevNameSym($d, @sig_devs);
    set($d, "TEST.DEV_NAME_SYM[1]", "");
    is(get($d, "TEST.DEV_NAME_SYM"), join(',', @sig_devs), "Array element didn't change");

    # Set element with index 0

    @sig_devs = getRandomSignalDevices($gateway, 3);
    prepareDevNameSym($d, @sig_devs);
    set($d, "TEST.DEV_NAME_SYM[0]", "");
    is(get($d, "TEST.DEV_NAME_SYM"), "", "Array cleared");

    # Change element with the last index

    @sig_devs = getRandomSignalDevices($gateway, 3);
    prepareDevNameSym($d, @sig_devs);
    set($d, "TEST.DEV_NAME_SYM[2]", "");
    is(get($d, "TEST.DEV_NAME_SYM"), join(',', @sig_devs), "Array element didn't change");

    # Set value at index > num_elements + 1

    @sig_devs = getRandomSignalDevices($gateway, 2);
    prepareDevNameSym($d, @sig_devs);
    my $device = getRandomSignalDevice($gateway);
    set($d, "TEST.DEV_NAME_SYM[3]", $device);
    push @sig_devs, "";
    push @sig_devs, $device;
    is(get($d, "TEST.DEV_NAME_SYM"), join(',', @sig_devs), "Array element set");

    # Check element omitting

    @sig_devs = getRandomSignalDevices($gateway, 3);
    prepareDevNameSym($d, @sig_devs);
    my @values = getRandomSignalDevices($gateway, 3);
    $values[1] = "";
    set($d, "TEST.DEV_NAME_SYM", join(',', @values));
    $values[1] = $sig_devs[1];
    is(get($d, "TEST.DEV_NAME_SYM"), join(',', @values), "Array element omitted");

    # Add empty element at the end of array

    @sig_devs = getRandomSignalDevices($gateway, 2);
    prepareDevNameSym($d, @sig_devs);
    push @sig_devs, "";
    set($d, "TEST.DEV_NAME_SYM", join(',', @sig_devs));
    is(get($d, "TEST.DEV_NAME_SYM"), join(',', @sig_devs), "Array extended with empty name");

    # Check that the RANGE get option is working

    $name = "";
    testSignalNameOk($d, $name, "Array cleared");
    my $response = get($d, "TEST.DEV_NAME_SYM RANGE");
    my (undef, $range) = split /\(|\)/, $response;
    is($range, "I_MEAS B_MEAS", "Range get option");
}



my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = "RFNA.866.01.RETH5";

#assureMainInOff($d, "SIMULATION");

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

lives_ok
{

testValidName($d);
testNameNotInNameDb($d);
testNameLength($d);
testValidMac($d);
testInvalidMac($d);
testValidSignalAtName($d);
testValidSignalAtMac($d);
testSignalWithEmptyDev($d);
testEmptySignal($d);
testSignalNameLength($d);
testSignalNotInSymlist($d);
testArrayDevName($d);
testArrayDevNameSym($d);

};

# EOF
