#!/usr/bin/perl

# File:    bis.pl
# Purpose: Tests the BIS interface

use POSIX;
use Test::Utils;

# Constants 

use constant {
    HOME   => 'Home',
    WORK   => 'Work',
    MOBILE => 'Mobile',
};


sub setState
{
    my ($d, $state) = @_;

    set($d, "PC", "$state");
    wait_until(sub { get($d,"STATE.PC") eq ("$state") }, 20, 0.5);   
}



sub getUser 
{
    my ($d) = @_;

    my $log = get($d, "LOG.TIMING");

    my @log_values = split /,/, $log;

    my @cycle_info = split /\s+/, $log_values[scalar @log_values - 1];

    return $cycle_info[1];
}



sub validateCyclingSingleChannel
{
    my ($d, $user, $expected, $expected_fail) = @_;

    # Configure the super-cycle to test the first channel
 
    my $expected_str_ok   = sprintf "0x%08X", $expected;
    my $expected_str_fail = sprintf "0x%08X", $expected_fail;
    my $ccv = $user * 50;
    
    setState($d, "ON_STANDBY");

    set($d, "FGC.CYC.SIM", $user * 100 + 5);

    set($d, "REF.FUNC.PLAY($user)", "ENABLED");
    set($d, "REF.FUNC.REG_MODE($user)", "I");
    set($d, "REF.TABLE.FUNCTION($user)", "0|0,0.2|20,2.5|20,3|$ccv,5.8|$ccv,6|0");

    setState($d, "CYCLING");

    wait_until(sub { getUser($d) eq "$user" }, 20, 0.5);

    wait_until(sub { ceil(get($d,"MEAS.I")) eq ("20") }, 20, 0.5);

    cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', $expected_str_fail);

    wait_until(sub { ceil(get($d,"MEAS.I")) eq ($ccv) }, 40, 0.5);
    cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', $expected_str_ok);
}


sub validateRate
{
    my ($d, $reg_mode, $max, $expected) = @_;

    while (ceil(get($d,"MEAS.$reg_mode")) < $max)
    {
        sleep (0.5);
        cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "$expected");   
    }
} 


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "SIMULATION");

set($d, "FGC.CYC.SIM", "");

# Configure the BIS properties

set($d, "BIS.PPM", "ENABLED");

# Test I_MEAS and V_MEAS checks on different channels for differnet users

set($d, "BIS.CONTROL(1)",       "CONDITIONAL,OFF,NOT_OK,OK");
set($d, "BIS.PC_STATE(1)",      "ENABLED,DISABLED,DISABLED,DISABLED");
set($d, "BIS.MAX_REG_ERROR(1)", "0.01,0.0,0.0,0.0");
set($d, "BIS.V_MID(1)",         "3.18,0,0,0");
set($d, "BIS.V_DELTA(1)",       "0.5,0,0,0");
set($d, "BIS.V_MAX_RATE(1)",    "0,0,0,0");
set($d, "BIS.I_MID(1)",         "50,0,0,0");
set($d, "BIS.I_DELTA(1)",       "2,0,0,0");
set($d, "BIS.I_MAX_RATE(1)",    "0,0,0,0");

set($d, "BIS.CONTROL(2)",       "OK,CONDITIONAL,OFF,NOT_OK");
set($d, "BIS.PC_STATE(2)",      "DISABLED,ENABLED,DISABLED,DISABLED");
set($d, "BIS.MAX_REG_ERROR(2)", "0.0,0.01,0.0,0.0");
set($d, "BIS.V_MID(2)",         "0,6.35,0,0");
set($d, "BIS.V_DELTA(2)",       "0,0.5,0,0");
set($d, "BIS.V_MAX_RATE(2)",    "0,0,0,0");
set($d, "BIS.I_MID(2)",         "0,100,0,0");
set($d, "BIS.I_DELTA(2)",       "0,2,0,0");
set($d, "BIS.I_MAX_RATE(2)",    "0,0,0,0");

set($d, "BIS.CONTROL(3)",       "NOT_OK,OK,CONDITIONAL,OFF");
set($d, "BIS.PC_STATE(3)",      "DISABLED,DISABLED,ENABLED,DISABLED");
set($d, "BIS.MAX_REG_ERROR(3)", "0.0,0.0,0.01,0.0");
set($d, "BIS.V_MID(3)",         "0,0,9.53,0");
set($d, "BIS.V_DELTA(3)",       "0,0,0.5,0");
set($d, "BIS.V_MAX_RATE(3)",    "0,0,0,0");
set($d, "BIS.I_MID(3)",         "0,0,150,0");
set($d, "BIS.I_DELTA(3)",       "0,0,2,0");
set($d, "BIS.I_MAX_RATE(3)",    "0,0,0,0");

set($d, "BIS.CONTROL(4)",       "OFF,NOT_OK,OK,CONDITIONAL");
set($d, "BIS.PC_STATE(4)",      "DISABLED,DISABLED,DISABLED,ENABLED");
set($d, "BIS.MAX_REG_ERROR(4)", "0.0,0.0,0.0,0.01");
set($d, "BIS.V_MID(4)",         "0,0,0,12.7");
set($d, "BIS.V_DELTA(4)",       "0,0,0,0.5");
set($d, "BIS.V_MAX_RATE(4)",    "0,0,0,0");
set($d, "BIS.I_MID(4)",         "0,0,0,200");
set($d, "BIS.I_DELTA(4)",       "0,0,0,2");
set($d, "BIS.I_MAX_RATE(4)",    "0,0,0,0");

validateCyclingSingleChannel($d, 1, 0xFF7F00FF, 0xFF7F002B);
validateCyclingSingleChannel($d, 2, 0x7F00FFFF, 0x7F002BFF);
validateCyclingSingleChannel($d, 3, 0x00FFFF7F, 0x002BFF7F);
validateCyclingSingleChannel($d, 4, 0xFFFF7F00, 0x2BFF7F00);

setState($d, "ON_STANDBY");
set($d, "FGC.CYC.SIM", "801");
set($d, "PC", "CYCLING");
wait_until(sub { get($d,"STATE.PC") eq ("TO_CYCLING") }, 20, 0.5);
wait_until(sub { getUser($d) eq "8" }, 20, 0.5);

# Test the STATE_PC (CYCLING has been tested before)

setState($d, "OFF");

set($d, "BIS.PPM",           "DISABLED");
set($d, "BIS.CONTROL",       "CONDITIONAL,CONDITIONAL,CONDITIONAL,CONDITIONAL");
set($d, "BIS.PC_STATE",      "ENABLED,ENABLED,ENABLED,ENABLED");
set($d, "BIS.MAX_REG_ERROR", "0,0,0,0");
set($d, "BIS.V_MID",         "0,0,0,0");
set($d, "BIS.V_DELTA",       "0,0,0,0");
set($d, "BIS.V_MAX_RATE",    "0,0,0,0");
set($d, "BIS.I_MID",         "0,0,0,0");
set($d, "BIS.I_DELTA",       "0,0,0,0");
set($d, "BIS.I_MAX_RATE",    "0,0,0,0");

set($d, "REF.CCV.VALUE", "5");
set($d, "REF.FUNC.REG_MODE", "I");
set($d, "LIMITS.I.MIN", "5");
set($d, "REF.DEFAULTS.I.LINEAR_RATE", "1");

sleep(2);

cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0x3E3E3E3E");

set($d, "PC", "BLOCKING");
wait_until(sub { get($d,"STATE.PC") eq ("STARTING") }, 20, 0.5);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0x3E3E3E3E");

wait_until(sub { get($d,"STATE.PC") eq ("BLOCKING") }, 20, 0.5);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0x3E3E3E3E");

set($d, "PC", "ON_STANDBY");
wait_until(sub { get($d,"STATE.PC") eq ("TO_STANDBY") }, 20, 0.5);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0x3E3E3E3E");

wait_until(sub { get($d,"STATE.PC") eq ("ON_STANDBY") }, 20, 0.5);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0x3E3E3E3E");

setState($d, "IDLE");
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0xFFFFFFFF");

set($d, "REF.TABLE.FUNCTION($user)", "0|5,0.2|5,2|0");
wait_until(sub { get($d,"STATE.PC") eq ("ARMED") }, 20, 0.5);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0xFFFFFFFF");

set($d, "REF.RUN", "0");
wait_until(sub { get($d,"STATE.PC") eq ("RUNNING") }, 20, 0.5);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0xFFFFFFFF");
wait_until(sub { get($d,"STATE.PC") eq ("IDLE") }, 20, 0.5);

setState($d, "DIRECT");
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0xFFFFFFFF");
sleep(5);

setState($d, "SLOW_ABORT");
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0x3E3E3E3E");

set($d, "PC", "OFF");
wait_until(sub { get($d,"STATE.PC") eq ("STOPPING") }, 20, 0.1);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0x3E3E3E3E");

wait_until(sub { get($d,"STATE.PC") eq ("OFF") }, 20, 0.1);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0x3E3E3E3E");

sleep (2);

set($d, "BIS.CONTROL", "CONDITIONAL,CONDITIONAL,CONDITIONAL,CONDITIONAL");
set($d, "BIS.PC_STATE", "DISABLED,DISABLED,DISABLED,DISABLED");

# Test I_RATE

set($d, "REF.DEFAULTS.I.LINEAR_RATE", "25");
set($d, "REF.FUNC.REG_MODE", "I");
set($d, "REF.CCV.VALUE", "0");

setState($d, "DIRECT");

set($d, "BIS.I_MAX_RATE", "1,1,1,1");
sleep (2);

set($d, "REF.CCV.VALUE", "100");
validateRate($d, "I", "70", "0x1F1F1F1F");
set($d, "REF.CCV.VALUE", "0");
wait_until(sub { floor(get($d,"MEAS.I")) eq ("0") }, 20, 0.5);

set($d, "BIS.I_MAX_RATE", "30,30,30,30");
sleep (2);

set($d, "REF.CCV.VALUE", "100");
validateRate($d, "I", "70", "0xFFFFFFFF");
set($d, "REF.CCV.VALUE", "0");
wait_until(sub { floor(get($d,"MEAS.I")) eq ("0") }, 20, 0.5);

set($d, "BIS.I_MAX_RATE", "0,0,0,0");
sleep (2);

# Test V_RATE

set($d, "REF.DEFAULTS.V.LINEAR_RATE", "2");
set($d, "REF.FUNC.REG_MODE", "V");
set($d, "REF.VCV.VALUE", "0");

setState($d, "DIRECT");

set($d, "BIS.V_MAX_RATE", "1,1,1,1");
sleep (2);

set($d, "REF.VCV.VALUE", "10");
validateRate($d, "V", "7", "0x37373737");
set($d, "REF.VCV.VALUE", "0");
wait_until(sub { floor(get($d,"MEAS.V")) eq ("0") }, 20, 0.5);

set($d, "BIS.V_MAX_RATE", "4,4,4,4");
sleep (2);

set($d, "REF.VCV.VALUE", "10");
validateRate($d, "V", "7", "0xFFFFFFFF");
set($d, "REF.VCV.VALUE", "0");
wait_until(sub { floor(get($d,"MEAS.V")) eq ("0") }, 20, 0.5);

set($d, "BIS.V_MAX_RATE", "0,0,0,0");
sleep (2);

# Test MAX_REG_ERROR

setState($d, "OFF");

set($d, "BIS.MAX_REG_ERROR", "1,1,1,1");
set($d, "REF.FUNC.REG_MODE", "I");
set($d, "LIMITS.V.POS", "5");

setState($d, "IDLE");
set($d, "REF", "STEPS,200");
wait_until(sub { get($d,"STATE.PC") eq ("ARMED") }, 20, 0.5);
set($d, "REF.RUN", "0");

wait_until(sub { get($d,"STATE.PC") eq ("RUNNING") }, 20, 0.5);
wait_until(sub { get($d,"STATE.PC") eq ("IDLE") }, 20, 0.5);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0x3D3D3D3D");

set($d, "REF", "NOW,0");
wait_until(sub { get($d,"STATE.PC") eq ("IDLE") }, 20, 0.5);
cmp_ok(get($d,"BIS.STATUS HEX"), 'eq', "0xFFFFFFFF");

# Restore the original state

setState($d, "OFF");

set($d, "CONFIG.MODE", "SYNC_FGC");
set($d, "FGC.CYC.SIM", "");
