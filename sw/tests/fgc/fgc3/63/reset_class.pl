#!/usr/bin/perl

# File:    reset.pl
# Author:  Krzysztof Lebioda
# Purpose: Reset testing
#          Reset should be possible only in OFF, FLT_OFF, STOPPING and FLT_STOPPING

# TODO: Add cycling

use Test::Utils;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "SIMULATION");

# Try to reset the FGC when in IDLE

setPc($d, "IL");
dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in IDLE failed";

# Try to reset the FGC when in ARMED

set($d, "REF.FUNC.REG_MODE" , "I");
set($d, "REF.TABLE.FUNCTION", "0|5,0.5|7,3.5|7,4|5");
wait_until( sub { get($d,"STATE.PC") eq 'ARMED' } );
dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in ARMED failed";

# Try to reset the FGC when in RUNNING

set($d, "REF.RUN", "");
wait_until( sub { get($d,"STATE.PC") eq 'RUNNING' } );
dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in RUNNING failed";

# Try to reset the FGC when in CYCLING

set($d, "MODE.PC" , "CY");
wait_until( sub { get($d,"STATE.PC") eq 'CYCLING' } );
dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in CYCLING failed";

# Try to reset the FGC when in DIRECT

set($d, "MODE.PC" , "DT");
wait_until( sub { get($d,"STATE.PC") eq 'DIRECT' } );
dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in DIRECT failed";


# EOF