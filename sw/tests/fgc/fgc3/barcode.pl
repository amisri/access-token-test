#!/usr/bin/perl

# File:    barcode.pl
# Purpose: Tests BARCODE.* properties

use Test::Utils;

# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "SIMULATION");

# Check dallas num errors

ok(get($d, 'FGC.DLS.NUM_ERRORS') eq '0', 'FGC.DLS.NUM_ERRORS is zero');

# Check we found IDs on local and crate bus

ok(get($d, 'FGC.ID.LOCAL') ne "", 'FGC.ID.LOCAL is not empty');
ok(get($d, 'FGC.ID.CRATE') ne "", 'FGC.ID.CRATE is not empty');
ok(get($d, 'FGC.ID.VS.A') ne "", 'FGC.ID.VS.A is not empty');


# BARCODE.MATCHED
# First element should have the main board
# Second element should have the communication card
# Third element should have analogue card

@matched = split(/,/, get($d, "BARCODE.MATCHED"));

cmp_ok(index(@matched[0], "01/01"), '!=', '-1', "BARCODE.MATCHED != '01/01'");
cmp_ok(index(@matched[0], "Mainboard"), '!=', '-1', "BARCODE.MATCHED != 'Mainboard'");
cmp_ok(index(@matched[1], "01/01"), '!=', '-1', "BARCODE.MATCHED != '01/01'");
cmp_ok(index(@matched[1], "Network"), '!=', '-1', "BARCODE.MATCHED != 'Network'");
cmp_ok(index(@matched[2], "01/01"), '!=', '-1', "BARCODE.MATCHED != '01/01'");
cmp_ok(index(@matched[2], "Analog"), '!=', '-1', "BARCODE.MATCHED != 'Analog'");


# BARCODE.MISMATCHED

my $mismatched = get($d, "BARCODE.MISMATCHED");

# cmp_ok($mismatched, 'eq', '', "BARCODE.MISMATCHED != ''");


my $barcode_regex = qr/^HCR[A-Z]{4}(\d{3}|___)-[A-Z]{2}\d{6}$/;

# BARCODE.FGC.CASSETTE

my $cassette = get($d, "BARCODE.FGC.CASSETTE");
my $cassette_ok = index($cassette, "HCRFNAE002") + index($cassette, "HCRFBNG001");

cmp_ok($cassette_ok, '==', '-1', "BARCODE.FGC.CASSETTE != 'HCRFNAE002' or 'HCRFBNG001'");
ok($cassette =~ $barcode_regex, 'FGC ANA card barcode');


# BARCODE.FGC.ANA

my $ana = get($d, "BARCODE.FGC.ANA");
my $ana_ok = index($ana, "HCRFBGD00") + index($ana, "HCRAMET00") + index($ana, "HCRFBGF001");

cmp_ok($ana_ok, '==', -2, "BARCODE.FGC.ANA");
ok($ana =~ $barcode_regex, 'FGC ANA card barcode');

