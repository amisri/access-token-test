#!/usr/bin/perl

# File:    array.pl
# Purpose: Tests array properties

use Test::Utils;


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};

# assureMainInOff($d, "SIMULATION");


# Test shrinkable arrays

# Test setting an array with the syntax S PROP 1,2,3,4. Element size: 4

set($d, "TEST.INT32S", "1,2,3,4");

cmp_ok(getPropertyInfo($d, "TEST.INT32S")->{n_elements}, "==", '4');
cmp_ok(get($d, "TEST.INT32S"), "eq", '1,2,3,4');

# Test setting an array with the syntax S PROP[1,] 2,3.  Element size: 3.

set($d, "TEST.INT32S[1,]", "5,6");

cmp_ok(getPropertyInfo($d, "TEST.INT32S")->{n_elements}, "==", '3');
cmp_ok(get($d, "TEST.INT32S"), "eq", '1,5,6');

# Test setting an array with the syntax S PROP[1,2] 2,3.  Element size: old.

set($d, "TEST.INT32S", "1,2,3,4");
set($d, "TEST.INT32S[1,2]", "7,8");

cmp_ok(getPropertyInfo($d, "TEST.INT32S")->{n_elements}, "==", '4');
cmp_ok(get($d, "TEST.INT32S"), "eq", '1,7,8,4');

# Test non-shrinkable arrays

my $value = get($d, "FGC.DEBUG.CORE_ADDR");

set($d, "FGC.DEBUG.CORE_ADDR", "1,2,3,4");
cmp_ok(getPropertyInfo($d, "FGC.DEBUG.CORE_ADDR")->{n_elements}, "==", '4');

set($d, "FGC.DEBUG.CORE_ADDR", "1,2");
cmp_ok(getPropertyInfo($d, "FGC.DEBUG.CORE_ADDR")->{n_elements}, "==", '4');

set($d, "FGC.DEBUG.CORE_ADDR", "$value");

# Test out of limits

set($d, "TEST.INT32S", "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16");

dies_ok sub { set($d, "TEST.INT32S", "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17") }, "Out of limits TEST.INT32S        corretcly failed";
dies_ok sub { set($d, "TEST.INT32S[14,17]", "1,2,3,4") }, "Out of limits TEST.INT32S[14,17] corretcly failed";
dies_ok sub { set($d, "TEST.INT32S[14,16]", "1,2,3,4") }, "Out of limits TEST.INT32S[14,16] corretcly failed";
dies_ok sub { set($d, "TEST.INT32S[14,]"  , "1,2,3,4") }, "Out of limits TEST.INT32S[14,]   corretcly failed";
