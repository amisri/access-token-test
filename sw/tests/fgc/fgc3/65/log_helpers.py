import re

import pyfgc_log


# Adapted from https://gitlab.cern.ch/ccs/fgc/-/blob/dev/powerspy/sw/clients/web/app/powerspy/server/util.py
def pyfgc_get_log(fgc_session, log_property, log_menu, cycle=0, time_offset=0.0, duration=0.0, time_origin=0,
                  previous_first_sample_time=0.0):
    """
    Acquires the desired buffer from the fgc.

    :device: device name as a string
    :log_property: device property as a string
    :cycle: wanted cycle number as int
    :time_offset: duration/pre-cycle time for the log in seconds
    :duration: duration/pre-cycle time for the log in seconds
    :time_origin: duration/pre-cycle time for the log in seconds
    :previous_first_sample_time: first sample time from the previous acquisition
    :returns: the buffer as a string and the data version
    """

    # Do preliminary actions based on the log kind
    version = None
    buffer = None
    get_option = 'bin'
    if re.match(r'^LOG\.SPY.+', log_property):
        # LOG.SPY.NC, LOG.SPY.CY, LOG.SPY.DATA

        # Add cycle in case log_property is LOG.SPY.CY()
        log_property = re.sub(r'(LOG\.SPY\.CY\()(\))', r'\g<1>%d\g<2>' % cycle, log_property)

        # Add cycle in case log_property is LOG.SPY.DATA
        log_property = re.sub(r'(LOG\.SPY\.DATA)(\[)', r'\g<1>(%d)\g<2>' % cycle, log_property)

        # If new class 62/63
        if "PM_TIME" in log_menu:
            if cycle == 0:
                get_option = 'BIN DATA|%.6f,%d,%d,%.6f' % (time_origin, 0, duration * 1000, previous_first_sample_time)
            else:
                get_option = 'BIN DATA|%.6f,%d,%d,%.6f' % (
                0, time_offset * 1000, duration * 1000, previous_first_sample_time)
        else:
            # Else, set time offset (to be removed in future with the data get option)
            fgc_session.set('LOG.SPY.TIME_OFFSET', duration)
            fgc_session.set('LOG.SPY.TIME_ORIGIN', time_origin)

    else:
        # Property not registered
        raise Exception('Invalid property %s' % log_property)

    # If buffer was not already fetched by one of the handler above
    if not buffer:
        # Get binary log
        buffer = pyfgc_log.extract_buffer(fgc_session.get(log_property, get_option=get_option))

    # If version was not found in one of the handler above, infer it from the buffer
    if version is None:
        version = pyfgc_log.extract_buffer_version(buffer)

    return buffer, version
