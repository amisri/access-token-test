# Based on the installed DIMDB revisison, return the appropriate EPIC.DEVICE_x for 1st device
def get_epic_node(fgc):
    dimdb_ver = fgc.get("CODE.VERSION.DIMDB").value

    # Verify that device is enabled
    assert fgc.get("EPIC.DEVICE_1.NAME.DEV").value != "NONE"

    # lol just like update your DimDB
    assert dimdb_ver >= "1625735444"
    return "EPIC.DEVICE_1"
