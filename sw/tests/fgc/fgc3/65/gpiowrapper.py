import csv
from dataclasses import dataclass
from pathlib import Path
from typing import Any, List, Dict
import sys

sys.path.append(r"C:\Local_git_testers\testers\system_testing\EPIC_3U_TESTER")

from PXI import DigitalInputPinDiff, DigitalOutputPin, DigitalOutputPinDiff
from testerTools import Signal


@dataclass
class Mapping:
    diff_object: DigitalOutputPinDiff
    property_name: str
    symbol_name: str
    log_signal_name: str
    inverted: bool

    def clear(self):
        if hasattr(self.diff_object, "clear_do_diff"):
            self.diff_object.clear_do_diff()
        else:
            self.diff_object.clear_do()

    def set(self):
        if hasattr(self.diff_object, "set_do_diff"):
            self.diff_object.set_do_diff()
        else:
            self.diff_object.set_do()


class GpioWrapper:
    mappings: List[Mapping]
    all_pins: Dict[str, Any]

    def __init__(self):
        # Collect mapping of input names to (property name, sym name) tuples
        self.mappings = []
        self.all_pins = dict()

        with open(Path(__name__).parent / "pinmapping.csv", "rt") as f:
            reader = csv.reader(f)

            next(reader) # skip first row

            for row in reader:
                if not row or not row[0]:
                    continue  # skip empty rows

                Signal_Name_Template,PXI_Signal_Type,PXI_Slot,PXI_Port,PXI_Pin,Property,Symbols,Logged_signal,Skip = row

                if Skip == "x":
                    continue

                pins = PXI_Pin.split(",")

                # Is differential?
                if len(pins) == 2:
                    sig_p = Signal(Signal_Name_Template.replace("*", "+"), PXI_Signal_Type, PXI_Slot, PXI_Port, pins[0])
                    sig_n = Signal(Signal_Name_Template.replace("*", "-"), PXI_Signal_Type, PXI_Slot, PXI_Port, pins[1])
                else:
                    sig = Signal(Signal_Name_Template, PXI_Signal_Type, PXI_Slot, PXI_Port, PXI_Pin)

                if PXI_Signal_Type == "DIG_OUT":
                    # Instantiate
                    if len(pins) == 2:
                        out_diff_object = DigitalOutputPinDiff(sig_p.pxi_slot, sig_p.pxi_port, sig_p.pxi_pin,
                                                            sig_n.pxi_slot, sig_n.pxi_port, sig_n.pxi_pin)
                    else:
                        out_diff_object = DigitalOutputPin(sig.pxi_slot, sig.pxi_port, sig.pxi_pin)

                    if Symbols.startswith("!"):
                        Symbols = Symbols[1:]
                        inverted = True
                    else:
                        inverted = False

                    self.mappings.append(Mapping(out_diff_object, Property,
                                                 symbol_name=Symbols,
                                                 log_signal_name=Logged_signal or None,
                                                 inverted=inverted,
                                                 ))

                    self.all_pins[Signal_Name_Template] = out_diff_object
                elif PXI_Signal_Type == "DIG_IN":
                    # Instantiate
                    if len(pins) == 2:
                        inst = DigitalInputPinDiff(sig_p.pxi_slot, sig_p.pxi_port, sig_p.pxi_pin,
                                                sig_n.pxi_slot, sig_n.pxi_port, sig_n.pxi_pin)
                    else:
                        inst = DigitalInputPin(sig.pxi_slot, sig.pxi_port, sig.pxi_pin)

                    self.all_pins[Signal_Name_Template] = inst
                else:
                    raise ValueError(PXI_Signal_Type)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        for inst in self.all_pins.values():
            if hasattr(inst, 'close_do_diff'):
                inst.close_do_diff()
            elif hasattr(inst, 'close_digital_output'):
                inst.close_digital_output()
            elif hasattr(inst, 'close_di_diff'):
                inst.close_di_diff()
            elif hasattr(inst, 'close_digital_input'):
                inst.close_digital_input()
            else:
                raise Exception()
