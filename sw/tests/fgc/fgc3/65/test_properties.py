from datetime import datetime
import os
import time

import pytest

import pyfgc
import pyfgc_log
import pyfgc_test_framework


def assert_ok(resp: pyfgc.fgc_response.FgcResponse):
    if resp.err_code != "":
        # logger.info("Failing response: %s", resp)
        raise pyfgc.FgcResponseError(f"{resp.err_code}: {resp.err_msg}")


def set_and_check(fgc, prop, value):
    assert_ok(fgc.set(prop, value))

    resp = fgc.get(prop)

    if resp.value != value:
        raise Exception(f"Mismatch: {resp} vs expected '{value}'")


def clear_set_clear(fgc, prop, value, clear_value=""):
    set_and_check(fgc, prop, clear_value)
    set_and_check(fgc, prop, value)
    set_and_check(fgc, prop, clear_value)


def set_for_range(fgc, prop_format, range_, value):
    for x in range_:
        assert_ok(fgc.set(prop_format % x, value))


def event_log_contains(fgc, property, value, action, since: datetime):
    response = fgc.get("LOG.EVT", get_option="bin")
    binary_buffer = pyfgc_log.extract_buffer(response)
    version = pyfgc_log.BufferVersion.EVENT_LOG.value
    log = pyfgc_log.decode(binary_buffer, version)

    # for entry in log:
    #     if datetime.fromtimestamp(entry["time_seconds"] + entry['time_microseconds'] / 1e6 + 1) >= since:
    #         print(entry)

    TOL = 0.2
    return any(entry["property"] == property and
               entry["value"] == value and
               entry["action"] == action and
               datetime.fromtimestamp(entry["time_seconds"] + entry['time_microseconds'] / 1e6 + TOL) >= since
               for entry in log)


def test_properties(device_name):
    with pyfgc.fgc(device_name) as fgc:
        clear_set_clear(fgc, "EPIC.DEVICE_1.NAME.DEV", "FOO", clear_value="NONE")
        clear_set_clear(fgc, "EPIC.DEVICE_1.NAME.DEV", "12345678901234567890123", clear_value="NONE") # at least 23 characters must be accepted
        clear_set_clear(fgc, "EPIC.DEVICE_1.NAME.GROUP.CH_1", "BAR", clear_value="NONE")
        clear_set_clear(fgc, "EPIC.DEVICE_1.NAME.GROUP.CH_2", "BAZ", clear_value="NONE")

        for i in range(10):
            assert_ok(fgc.set(f"EPIC.DEVICE_1.NAME.PC_IN[{i}]", f"NONE"))
        for i in range(10):
            assert_ok(fgc.set(f"EPIC.DEVICE_1.NAME.PC_IN[{i}]", f"PC_{i}"))
        for i in range(10):
            assert fgc.get(f"EPIC.DEVICE_1.NAME.PC_IN[{i}]").value == f"PC_{i}"

        clear_set_clear(fgc, "EPIC.DEVICE_1.NAME.FEI_IN", "FOO,BAR")
        clear_set_clear(fgc, "EPIC.DEVICE_1.NAME.CIBU_OUT", "AAA,BBB")
        clear_set_clear(fgc, "EPIC.DEVICE_1.NAME.EPIC_OUT", "EPICEPIC", clear_value="NONE")

        clear_set_clear(fgc, "EPIC.DEVICE_1.FEI_1_OUT_CH", "XYZW")
        clear_set_clear(fgc, "EPIC.DEVICE_1.FEI_1_OUT_CH", "ABC")
        clear_set_clear(fgc, "EPIC.DEVICE_1.FEI_2_OUT_CH", "9999")

        # Configure just DEVICE_1
        set_for_range(fgc, "EPIC.DEVICE_%d.NAME.DEV", range(1, 9), "NONE")
        assert_ok(fgc.set("EPIC.DEVICE_1.NAME.DEV", "FOOBAR"))

        # Check stuff, expect no warning
        assert fgc.get("EPIC.DEBUG_DEV_MASK").value == str(0x01)
        assert "EPIC_CONFIG" not in fgc.get("STATUS.WARNINGS").value

        # Configure also DEVICE_3
        assert_ok(fgc.set("EPIC.DEVICE_3.NAME.DEV", "BAZQUUX"))

        # Check stuff, expect EPIC_CONFIG warning because DEVICE_2 was skipped
        assert fgc.get("EPIC.DEBUG_DEV_MASK").value == str(0x01 | 0x04)
        assert "EPIC_CONFIG" in fgc.get("STATUS.WARNINGS").value

        # Clean up
        set_for_range(fgc, "EPIC.DEVICE_%d.NAME.DEV", range(1, 9), "NONE")
        assert_ok(fgc.set("EPIC.DEVICE_1.NAME.DEV", "FOOBAR"))


def test_faults_reset1(device_name):
    with pyfgc.fgc(device_name) as fgc:
        if fgc.get("STATE.OP").value == "UNCONFIGURED":
            raise Exception("converter not fully configured; cannot test event log")

        # Clear all faults
        assert_ok(fgc.set("VS", "RESET"))
        time.sleep(0.2)  # apparently the command takes a while to truly finish

        # Inject faults
        start = datetime.now()
        set_and_check(fgc, "EPIC.DEVICE_1.FAULTS", "DIF_A DCF_B")
        assert "EPIC_1" in fgc.get("STATUS.FAULTS").value

        # Check event log
        time.sleep(2)   # need to wait longer still for event log to settle
        assert event_log_contains(fgc, "EPIC_1_FAULTS", "DIF_A", "SET_BIT", start)
        assert event_log_contains(fgc, "EPIC_1_FAULTS", "DCF_B", "SET_BIT", start)
        assert not event_log_contains(fgc, "EPIC_1_FAULTS", "DOF_A", "SET_BIT", start)

        # Reset VS
        assert_ok(fgc.set("VS", "RESET"))
        time.sleep(0.1)

        # Verify fault disappears
        assert fgc.get("EPIC.DEVICE_1.FAULTS").value == ""
        assert "EPIC_1" not in fgc.get("STATUS.FAULTS").value


def test_faults_reset2(device_name):
    with pyfgc.fgc(device_name) as fgc:
        # Clear all faults
        assert_ok(fgc.set("VS", "RESET"))
        time.sleep(0.2)  # apparently the command takes a while to truly finish

        # Inject faults
        set_and_check(fgc, "EPIC.DEVICE_1.FAULTS", "DIF_A DCF_B")
        assert "EPIC_1" in fgc.get("STATUS.FAULTS").value

        # Reset via property
        set_and_check(fgc, "EPIC.DEVICE_1.FAULTS", "")

        # Verify fault disappears on STATUS.FAULTS
        assert fgc.get("EPIC.DEVICE_1.FAULTS").value == ""
        assert "EPIC_1" not in fgc.get("STATUS.FAULTS").value
