import pytest

# From sw/clients/python/fgc_tests/config/conftest.py:
# create command line options
def pytest_addoption(parser):
    parser.addoption("--device", action="store", help="device: fgc device name")
    # parser.addoption("--protocol", action="store", default="sync", help="protocol: sync|serial")
    # parser.addoption("--authenticate", action="store", default="location", help="authenticate: authentication mode")

@pytest.fixture()
def device_name(request):
    """FGC device to test - Imported from the command line option --device"""
    return request.config.getoption("--device")

# @pytest.fixture()
# def protocol(request):
#     """Protocol to use to query the FGCs: sync|serial"""
#     return request.config.getoption("--protocol")

# @pytest.fixture()
# def authenticate(request):
#     """Authentication method to use: location|token"""
#     return request.config.getoption("--authenticate")
