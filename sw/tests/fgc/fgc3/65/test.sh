#!/bin/sh

cd $(dirname $(readlink -f $0))

# Temporary work-around for https://gitlab.cern.ch/ccs/fgc/-/issues/34: ignore DeprecationWarning
env ~pclhc/bin/python/ccs_python -m pytest -W ignore::DeprecationWarning:pyfgc_test_framework.oldpyfgc.utils "$@"
