import base64
import logging
import time
from datetime import datetime, timedelta
import os

import pyfgc
import pyfgc_log
import pyfgc_rbac
import pytest

from fgc_65_util import get_epic_node
from gpiowrapper import GpioWrapper

logger = logging.getLogger(__name__)

DELAY = 0.05    # delay to set inputs -> read results... theoretical minimum is 20ms, but practice says we need more


@pytest.fixture
def fgc_name() -> str:
    return "REPIC.866.DIAG.1"


@pytest.fixture
def rbac_token() -> bytes:
    return pyfgc_rbac.get_token_login(os.environ["RBAC_USERNAME"],
                                      base64.b64decode(os.environ["RBAC_PASSWORD"]))


def assert_presence(needle, haystack, in_):
    if (in_ and needle not in haystack) or (not in_ and needle in haystack):
        if in_:
            raise Exception(f"Error, expected '{needle}', but got '{haystack}'")
        else:
            raise Exception(f"Error, expected NOT '{needle}', but got '{haystack}'")


def mask_all_inputs(gpio: GpioWrapper):
    for name, inst in gpio.all_pins.items():
        if name.startswith("nDISABLE_"):
            inst.clear_do()


def unmask_all_inputs(gpio: GpioWrapper):
    for name, inst in gpio.all_pins.items():
        if name.startswith("nDISABLE_"):
            inst.set_do()


def clear_all_inputs(gpio: GpioWrapper):
    for name, inst in gpio.all_pins.items():
        if name.startswith("USER_PERMIT_"):
            inst.clear_do_diff()


def test_properties(fgc_name: str, rbac_token: bytes):
    with GpioWrapper() as gpio, pyfgc.fgc(fgc_name, rbac_token=rbac_token) as fgc:
        unmask_all_inputs(gpio)

        # Iterate inputs
        # - set one at a time
        # - validate all property values

        PREFIX = get_epic_node(fgc) + "."

        for mapping in gpio.mappings:
            logger.info("Test %s %s", mapping.property_name, mapping.symbol_name)

            # Clear signal
            mapping.clear(); time.sleep(DELAY)
            assert_presence(mapping.symbol_name, fgc.get(PREFIX + mapping.property_name).value, in_=False if not mapping.inverted else True)
            # print(fgc.get("EPIC.DEVICE_1." + mapping.property_name).value)

            # Set signal
            mapping.set(); time.sleep(DELAY)
            assert_presence(mapping.symbol_name, fgc.get(PREFIX + mapping.property_name).value, in_=True if not mapping.inverted else False)
            # print(fgc.get("EPIC.DEVICE_1." + mapping.property_name).value)

            mapping.clear()

        # if False:
        #     print("Now free-running ...")

        #     while True:
        #         for mapping in mappings:
        #             mapping.set(); time.sleep(DT)
        #             mapping.clear(); time.sleep(DT)


def test_logs(fgc_name: str, rbac_token: bytes):
    with GpioWrapper() as gpio, pyfgc.fgc(fgc_name, rbac_token=rbac_token) as fgc:
        mask_all_inputs(gpio)   # we just want all nDISABLEs in zero, so that we can strobe them
        clear_all_inputs(gpio)

        start_time = datetime.now()# - timedelta(milliseconds=100)
        time.sleep(0.2)

        for mapping in gpio.mappings:
            mapping.clear()
            time.sleep(DELAY)
            mapping.set()
            time.sleep(DELAY)

            mapping.clear()

        end_time = datetime.now()

        # Grab the logs and ensure signals rose in the expected order
        # Juicy goodies for logs extraction:
        # https://gitlab.cern.ch/ccs/fgc/-/blob/master/sw/clients/powerspy/app/server/powerspy/controller.py#L110

        log_menu = pyfgc_log.parse_log_menu(pyfgc_log.extract_buffer(fgc.get('LOG.MENU')))

        # Iterate mappings, for each signal find the corresponding log, and save the pair.
        # This also implicitly captures the set of all relevant logs
        log_signal_name_to_propery_name_and_index = dict()

        def get_property_name_and_index_for_signal(signal_name):
            for menu_item in log_menu["LOG_DATA"].values():
                for i, candidate in enumerate(menu_item.get("SIGNALS", [])):
                    if candidate == signal_name:
                        return (menu_item["PROP"], i)

            raise Exception("Couldn't find " + signal_name)

        all_property_names = set()

        for mapping in gpio.mappings:
            if mapping.log_signal_name is None:
                continue

            property_name_and_index = get_property_name_and_index_for_signal(mapping.log_signal_name)
            log_signal_name_to_propery_name_and_index[mapping.log_signal_name] = property_name_and_index
            all_property_names.add(property_name_and_index[0])

        # Determine the acquisition timestamp and grab the logs
        from log_helpers import pyfgc_get_log

        logs = dict()

        duration = end_time - start_time
        logger.info("Duration: %.1f seconds", duration.total_seconds())

        for property_name in all_property_names:
            buffer, version = pyfgc_get_log(fgc, property_name, log_menu,
                                            time_offset=int(start_time.timestamp()), # boh, something like this
                                            duration=duration.total_seconds(),
                                            )
            decoded_buffer = pyfgc_log.decode(buffer, version)
            logs[property_name] = decoded_buffer

        pos = 0
        big_dict = dict()
        exception = None

        for mapping in gpio.mappings:
            if mapping.log_signal_name is None:
                continue

            property_name, signal_index = log_signal_name_to_propery_name_and_index[mapping.log_signal_name]
            decoded_buffer = logs[property_name]

            signal_log = decoded_buffer["signals"][signal_index]
            assert signal_log["name"] == mapping.log_signal_name

            # look for transition. index must be >= pos
            samples = signal_log["samples"].split(",")

            try:
                index = samples.index("1", pos)
                logger.info("Ok %s", signal_log["name"])
            except ValueError as ex:
                logger.error("Couldn't find transition for signal %s", signal_log["name"])
                logger.info("%s", signal_log["samples"])

                exception = exception or ex
                index = pos # resume from here

            pos = index + 1

            big_dict[mapping.log_signal_name] = dict(period=decoded_buffer["period_microseconds"],
                                                        samples=signal_log["samples"],
                                                        markers=[index]
                                                        )

        with open("logs_processed.json", "wt") as f:
            import json

            json.dump(big_dict, f)

        if exception is not None:
            raise exception


def test_all_disable(fgc_name: str, rbac_token: bytes):
    with GpioWrapper() as gpio, pyfgc.fgc(fgc_name, rbac_token=rbac_token) as fgc:
        PREFIX = get_epic_node(fgc) + "."

        mask_all_inputs(gpio); time.sleep(DELAY)

        # All permit inputs should be now masked (ignored), so we expect the permit to be given

        assert gpio.all_pins["CL_OUT_A_*_W1"].read_di_diff() is True
        assert_presence("PERMIT_1A", fgc.get(PREFIX + "STATUS.OUT").value, in_=True)

        # ENABLE one input, expect permit to be removed

        gpio.all_pins["nDISABLE_PC_A_1"].set_do()
        gpio.all_pins["USER_PERMIT_W1_A_*_1"].clear_do_diff()
        time.sleep(DELAY)

        assert gpio.all_pins["CL_OUT_A_*_W1"].read_di_diff() is False
        assert_presence("PERMIT_1A", fgc.get(PREFIX + "STATUS.OUT").value, in_=False)
