#!/usr/bin/perl

# File:    cal.pl
# Purpose: Tests CAL.* properties

use Test::Utils;


sub getCalAdcErrors
{
    my ($d) = @_;

    my %cal = ();

    $cal{"CAL.A.ADC.INTERNAL.GAIN"} = get($d,"CAL.A.ADC.INTERNAL.GAIN");
    $cal{"CAL.B.ADC.INTERNAL.GAIN"} = get($d,"CAL.B.ADC.INTERNAL.GAIN");
    $cal{"CAL.C.ADC.INTERNAL.GAIN"} = get($d,"CAL.C.ADC.INTERNAL.GAIN");
    $cal{"CAL.D.ADC.INTERNAL.GAIN"} = get($d,"CAL.D.ADC.INTERNAL.GAIN");

    $cal{"CAL.A.ADC.INTERNAL.ERR[0]"} = get($d,"CAL.A.ADC.INTERNAL.ERR[0]");
    $cal{"CAL.B.ADC.INTERNAL.ERR[0]"} = get($d,"CAL.B.ADC.INTERNAL.ERR[0]");
    $cal{"CAL.C.ADC.INTERNAL.ERR[0]"} = get($d,"CAL.C.ADC.INTERNAL.ERR[0]");
    $cal{"CAL.D.ADC.INTERNAL.ERR[0]"} = get($d,"CAL.D.ADC.INTERNAL.ERR[0]");

    $cal{"CAL.A.ADC.INTERNAL.ERR[1]"} = get($d,"CAL.A.ADC.INTERNAL.ERR[1]");
    $cal{"CAL.B.ADC.INTERNAL.ERR[1]"} = get($d,"CAL.B.ADC.INTERNAL.ERR[1]");
    $cal{"CAL.C.ADC.INTERNAL.ERR[1]"} = get($d,"CAL.C.ADC.INTERNAL.ERR[1]");
    $cal{"CAL.D.ADC.INTERNAL.ERR[1]"} = get($d,"CAL.D.ADC.INTERNAL.ERR[1]");
    
    $cal{"CAL.A.ADC.INTERNAL.ERR[2]"} = get($d,"CAL.A.ADC.INTERNAL.ERR[2]");
    $cal{"CAL.B.ADC.INTERNAL.ERR[2]"} = get($d,"CAL.B.ADC.INTERNAL.ERR[2]");
    $cal{"CAL.C.ADC.INTERNAL.ERR[2]"} = get($d,"CAL.C.ADC.INTERNAL.ERR[2]");
    $cal{"CAL.D.ADC.INTERNAL.ERR[2]"} = get($d,"CAL.D.ADC.INTERNAL.ERR[2]");

    $cal{"CAL.A.ADC.INTERNAL.VRAW[0]"} = get($d,"CAL.A.ADC.INTERNAL.VRAW[0]");
    $cal{"CAL.B.ADC.INTERNAL.VRAW[0]"} = get($d,"CAL.B.ADC.INTERNAL.VRAW[0]");
    $cal{"CAL.C.ADC.INTERNAL.VRAW[0]"} = get($d,"CAL.C.ADC.INTERNAL.VRAW[0]");
    $cal{"CAL.D.ADC.INTERNAL.VRAW[0]"} = get($d,"CAL.D.ADC.INTERNAL.VRAW[0]");

    $cal{"CAL.A.ADC.INTERNAL.VRAW[2]"} = get($d,"CAL.A.ADC.INTERNAL.VRAW[1]");
    $cal{"CAL.B.ADC.INTERNAL.VRAW[2]"} = get($d,"CAL.B.ADC.INTERNAL.VRAW[1]");
    $cal{"CAL.C.ADC.INTERNAL.VRAW[2]"} = get($d,"CAL.C.ADC.INTERNAL.VRAW[1]");
    $cal{"CAL.D.ADC.INTERNAL.VRAW[2]"} = get($d,"CAL.D.ADC.INTERNAL.VRAW[1]");

    $cal{"CAL.A.ADC.INTERNAL.VRAW[2]"} = get($d,"CAL.A.ADC.INTERNAL.VRAW[2]");
    $cal{"CAL.B.ADC.INTERNAL.VRAW[2]"} = get($d,"CAL.B.ADC.INTERNAL.VRAW[2]");
    $cal{"CAL.C.ADC.INTERNAL.VRAW[2]"} = get($d,"CAL.C.ADC.INTERNAL.VRAW[2]");
    $cal{"CAL.D.ADC.INTERNAL.VRAW[2]"} = get($d,"CAL.D.ADC.INTERNAL.VRAW[2]");

    return %cal;
}


sub getCalDacErrors
{
    my ($d) = @_;

    my %cal = ();

    $cal{"CAL.A.DAC[0]"} = get($d,"CAL.A.DAC[0]");
    $cal{"CAL.B.DAC[0]"} = get($d,"CAL.B.DAC[0]");

    $cal{"CAL.A.DAC[1]"} = get($d,"CAL.A.DAC[1]");
    $cal{"CAL.B.DAC[1]"} = get($d,"CAL.B.DAC[1]");

    $cal{"CAL.A.DAC[2]"} = get($d,"CAL.A.DAC[2]");
    $cal{"CAL.B.DAC[2]"} = get($d,"CAL.B.DAC[2]");

    return %cal;
}


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "NORMAL");


my %cal_b = ();
my %cal_a = ();

# Do a first ADC calibration

set($d, "CAL", "ADCS");

wait_until( sub { get($d,"STATE.OP") eq ("NORMAL") }, 120, 1 );


# Get the calibration errors

%cal_a = getCalAdcErrors($d);

# Do a second ADC calibration

set($d, "CAL", "ADCS");

wait_until( sub { get($d,"STATE.OP") eq ("NORMAL") }, 120, 1 );

# Get the calibration errors

%cal_b = getCalAdcErrors($d);

foreach my $prop (sort keys %cal_b)
{
    my $value_b = $cal_b{$prop};
    my $value_a = $cal_a{$prop};
    my $diff    = $value_b / $value_a;

    # The values cannot be zero and the error before and after calibration less than 10% 

    cmp_ok($value_a, '!=', "0", "$prop != 0");
    cmp_ok(($diff > 0.8 && $diff < 1.2),  'eq', "1", "$prop < 10%");
}

# Do a first DAC calibration

set($d, "CAL", "DACS");

wait_until( sub { get($d,"STATE.OP") eq ("NORMAL") }, 120, 1 );

%cal_a = getCalDacErrors($d);

# Do a second DAC calibration

set($d, "CAL", "DACS");

wait_until( sub { get($d,"STATE.OP") eq ("NORMAL") }, 120, 1 );

%cal_b = getCalDacErrors($d);

foreach my $prop (sort keys %cal_b)
{
    my $value_b = $cal_b{$prop};
    my $value_a = $cal_a{$prop};
    my $diff    = $value_b / $value_a;

    # The values cannot be zero and the error before and after calibration less than 10% 

    cmp_ok($value_a, '!=', "0", "$prop != 0");
    cmp_ok(($diff > 0.8 && $diff < 1.2),  'eq', "1", "$prop < 10%");
}

# Calibrate the DCCTs
