#!/usr/bin/perl

# File:    dallasTask.pl
# Purpose: Tests the state machine for Dallas task

use Test::Utils;

use Time::HiRes qw(usleep);

use strict;
use warnings;
use diagnostics;
use Switch;



sub processLine
{
    my ($line) = @_;

    my @aux = split / /, $line;

    my $name = $aux[1];

    @aux = split /:/, $aux[0];

    my $temp = $aux[1];

    my $id = substr $aux[0], -2;

    my @data;

    $data[0] = $id;
    $data[1] = $temp;
    $data[2] = $name;

    return \@data;
}



sub testComponents
{
    my ($id_log, $location) = @_;

    my @id_local = split (",", $id_log);

    my $entries = scalar @id_local;

    my @local_data;
    my $success_count = 0;

    for (my $i = 0; $i < $entries; $i++) 
    {
        $local_data[$i] = processLine($id_local[$i]);

        if(   $local_data[$i]->[2] eq "Mainboard"
           || $local_data[$i]->[2] eq "Network"
           || $local_data[$i]->[2] eq "Analog")
        {
            cmp_ok($local_data[$i]->[0], '==', 28);
            cmp_ok($local_data[$i]->[1], '>=', 25);
            cmp_ok($local_data[$i]->[1], '<=', 50);
            $success_count++;
        }
    }

    if ($location eq "LOCAL") {
        cmp_ok($success_count, '>=', 3);
    }
}



# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};


my $id_log = get($d, "FGC.ID.LOCAL");

testComponents($id_log, "LOCAL");

$id_log = get($d, "FGC.ID.MEAS.A");

testComponents($id_log, "A");

$id_log = get($d, "FGC.ID.MEAS.B");

testComponents($id_log, "B");