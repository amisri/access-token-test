#!/usr/bin/perl

# File:    cyc.pl
# Purpose: Tests the control when in CYCLING

use Test::Utils;


sub getLastCycSel
{
    my ($d) = @_;

    # LOG.TIMING format: '1508340216.065000  2     I TABLE    OK'

    my $log_timing = get($d, "LOG.TIMING[47]");

    return (substr $log_timing, 18, 2);
}


sub checkCycle
{
    my ($d) = @_;

    my $cyc_sel         = getLastCycSel($d);
    my $cyc_status      = get($d, "LOG.CYC.STATUS($cyc_sel)");;
    my $cyc_max_abs_err = get($d, "LOG.CYC.MAX_ABS_ERR($cyc_sel)");;

    cmp_ok($cyc_max_abs_err, '<',  0.1, "LOG.CYC.MAX_ABS_ERR($cyc_sel) < 0.1");
    cmp_ok($cyc_status,      'eq', "OK");
}


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

assureMainInOff($d, "NORMAL");

# Set up the PULSE function 

for my $i (1..24)
{
    set($d, "REF.PULSE.REF.VALUE($i)", "$i");
    set($d, "REF.FUNC.PLAY($i)", "ENABLED");
}

# Simulate the super-cycle

set($d, "FGC.CYC.SIM", "0101,0201,0301");

setPc($d, "CY");
wait_until(sub { get($d,"STATE.PC") eq ("CYCLING") }, 60, 1);

sleep(5);

my $diff = 1 - get($d, "MEAS.ACQ_VALUE(1)");
cmp_ok(($diff < 0.1 && $diff > -0.1),  'eq', "1", "MEAS.ACQ_VALUE(1) = $diff");

$diff = 2 - get($d, "MEAS.ACQ_VALUE(2)");
cmp_ok(($diff < 0.1 && $diff > -0.1),  'eq', "1", "MEAS.ACQ_VALUE(2) = $diff");

$diff = 3 - get($d, "MEAS.ACQ_VALUE(3)");
cmp_ok(($diff < 0.1 && $diff > -0.1),  'eq', "1", "MEAS.ACQ_VALUE(3) = $diff");


# Test REF.PULSE.REF_LOCAL 

set($d, "REF.PULSE.REF_LOCAL", "5");

sleep(5);

my $warnings = get($d,"STATUS.WARNINGS");
cmp_ok(index($warnings, "REF_LOCAL"),  '!=', "-1", "STATUS.WARNINGS == REF_LOCAL");
cmp_ok(index($warnings, "TIMING_EVT"), '!=', "-1", "STATUS.WARNINGS != TIMING_EVT");


my $diff = 5 - get($d, "MEAS.ACQ_VALUE(1)");
cmp_ok(($diff < 0.1 && $diff > -0.1),  'eq', "1", "REF_LOCAL(1) = $diff");

$diff = 5 - get($d, "MEAS.ACQ_VALUE(2)");
cmp_ok(($diff < 0.1 && $diff > -0.1),  'eq', "1", "REF_LOCAL(2) = $diff");

$diff = 5 - get($d, "MEAS.ACQ_VALUE(3)");
cmp_ok(($diff < 0.1 && $diff > -0.1),  'eq', "1", "REF_LOCAL(3) = $diff");

set($d, "REF.PULSE.REF_LOCAL", "0");

sleep(2);

$warnings = get($d,"STATUS.WARNINGS");
cmp_ok(index($warnings, "REF_LOCAL"), '==', "-1", "STATUS.WARNINGS != REF_LOCAL");


set($d, "REF.PULSE.REF_LOCAL", "5");

sleep(2);

my $warnings = get($d,"STATUS.WARNINGS");
cmp_ok(index($warnings, "REF_LOCAL"), '!=', "-1", "STATUS.WARNINGS == REF_LOCAL");


# Power off

set($d, "PC", "OFF");
wait_until(sub { get($d,"STATE.PC") eq ("OFF") }, 100, 1);

sleep(5);

set($d, "FGC.CYC.SIM", "");

sleep(2);

$warnings = get($d,"STATUS.WARNINGS");
cmp_ok(index($warnings, "REF_LOCAL"),  '==', "-1", "STATUS.WARNINGS != REF_LOCAL");
cmp_ok(index($warnings, "TIMING_EVT"), '==', "-1", "STATUS.WARNINGS == TIMING_EVT");


# EOF
