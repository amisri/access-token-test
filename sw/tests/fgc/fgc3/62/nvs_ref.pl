#!/usr/bin/perl

# File:    cyc.pl
# Purpose: Tests the control when in CYCLING

use Test::Utils;


sub getLastCycSel
{
    my ($d) = @_;

    # LOG.TIMING format: '1508340216.065000  2     I TABLE    OK'

    my $log_timing = get($d, "LOG.TIMING[47]");

    return (substr $log_timing, 18, 2);
}


sub checkCycle
{
    my ($d) = @_;

    my $cyc_sel         = getLastCycSel($d);
    my $cyc_status      = get($d, "LOG.CYC.STATUS($cyc_sel)");;
    my $cyc_max_abs_err = get($d, "LOG.CYC.MAX_ABS_ERR($cyc_sel)");;

    cmp_ok($cyc_max_abs_err, '<',  0.1, "LOG.CYC.MAX_ABS_ERR($cyc_sel) < 0.1");
    cmp_ok($cyc_status,      'eq', "OK");
}


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

assureMainInOff($d, "SIMULATION");

my $limits = get($d,"LIMITS");

# Set up the PULSE function on odd users

for (my $i = 1; $i < 24; $i += 2)
{
    $value = 0.5 * $i;
    set($d, "REF.PULSE.REF.VALUE($i)", "$value");
    set($d, "REF.FUNC.PLAY($i)", "ENABLED");
}

# Simulate the super-cycle

set($d, "FGC.CYC.SIM", "0101,0301,0501,0701,0901,1101,1301,1501,1701,1901,2101,2301");

setPc($d, "CY");
wait_until(sub { get($d,"STATE.PC") eq ("CYCLING") }, 60, 1);

sleep(30);

for (my $i = 1; $i < 24; $i += 2)
{
    my $diff = 0.5 * $i - get($d, "MEAS.ACQ_VALUE($i)");

    my $tmp = ($diff < 0.1 && $diff > -0.1);
    my $tmp1 = $diff < 0.1;
    my $tmp2 = $diff > -0.1;


    cmp_ok($tmp, 'eq', "1", "MEAS.ACQ_VALUE($i) = $diff $tmp $tmp1 $tmp2");
}

setPc($d, "OFF");

# Reset the FGC3 and make sure the values are still in NVS and correclty armed

set($d, "DEVICE.RESET", "");

assureMainInOff($d, "SIMULATION");

for (my $i = 1; $i < 24; $i += 2)
{
    $value = 0.5 * $i;

    my $fgc_value = sprintf("%.5g", get($d, "REF.PULSE.REF.VALUE($i)"));

    cmp_ok($fgc_value, 'eq', "$value", "REF.PULSE.REF.VALUE($i) == $value");
    cmp_ok(get($d, "REF.FUNC.PLAY($i)"), 'eq', "ENABLED", "REF.FUNC.PLAY($i) == ENABLED");
}

cmp_ok(get($d, "LIMITS"), 'eq', "$limits", "LIMITS ok");

set($d, "FGC.CYC.SIM", "0101,0301,0501,0701,0901,1101,1301,1501,1701,1901,2101,2301");

setPc($d, "CY");
wait_until(sub { get($d,"STATE.PC") eq ("CYCLING") }, 60, 1);

sleep(20);

for (my $i = 1; $i < 24; $i += 2)
{
    my $diff = 0.5 * $i - get($d, "MEAS.ACQ_VALUE(1)");

    cmp_ok(($diff < 0.1 && $diff > -0.1),  'eq', "1", "MEAS.ACQ_VALUE($i) = $diff");
}

setPc($d, "OFF");


# EOF
