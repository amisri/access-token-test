#!/usr/bin/perl

# File:    config.pl
# Purpose: Tests CONFIG.* properties

use Test::Utils;


# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "NORMAL");


# Reset CONFIG.STATE to start with a clean state

set($d, "CONFIG.STATE", "RESET");

cmp_ok(get($d,"CONFIG.STATE"), 'eq', "SYNCHRONISED", "CONFIG.STATE != SYNCHRONISED");

# Modify a config property and verify the state changes accordingly

my $value = get($d, "LOG.OASIS.SUBSAMPLE");

set($d, "LOG.OASIS.SUBSAMPLE",  $value + 1);
set($d, "LOG.OASIS.SUBSAMPLE",  $value);

cmp_ok(get($d,"CONFIG.STATE"), 'eq', "UNSYNCED", "CONFIG.STATE != UNSYNCED");

my $value_ok;

$value_ok = get($d,"CONFIG.CHANGED");
cmp_ok(index($value_ok, "LOG.OASIS.SUBSAMPLE"), '!=', "-1", "CONFIG.CHANGED != LOG.OASIS.SUBSAMPLE");

$value_ok = get($d,"STATUS.WARNINGS");
cmp_ok(index($value_ok, "CONFIG"), '!=', "-1", "STATUS.WARNINGS != CONFIG");


# CONFIG.SET # CONFIG.UNSET

set($d, "CONFIG.UNSET",  "");
set($d, "DEVICE.RESET",  "");

assureMain($d);

$value = get($d,"CONFIG.UNSET") =~ tr/\n//;
cmp_ok($value, '>', "40", "CONFIG.UNSET > 40");
cmp_ok($value, '<', "200", "CONFIG.UNSET < 200");

$value_ok = get($d,"STATUS.ST_UNLATCHED");
cmp_ok(index($value_ok, "SYNC_PLEASE"), '!=', "-1", "STATUS.ST_UNLATCHED == SYNC_PLEASE");

$value_ok = get($d,"STATUS.WARNINGS");
cmp_ok(index($value_ok, "CONFIG"), '!=', "-1", "STATUS.WARNINGS == CONFIG");

wait_until(sub { get($d,"CONFIG.UNSET") eq ("") }, 60, 1);

$value = get($d,"CONFIG.SET") =~ tr/\n//;

cmp_ok($value, '>', "70", "CONFIG.SET > 70");
cmp_ok($value, '<', "200", "CONFIG.SET < 200");

$value_ok = get($d,"STATUS.ST_UNLATCHED");
cmp_ok(index($value_ok, "SYNC_PLEASE"), '==', "-1", "STATUS.ST_UNLATCHED != SYNC_PLEASE");

$value_ok = get($d,"STATUS.WARNINGS");
cmp_ok(index($value_ok, "CONFIG"), '==', "-1", "STATUS.WARNINGS != CONFIG");

cmp_ok(get($d,"CONFIG.STATE"), 'eq', "SYNCHRONISED", "CONFIG.STATE == SYNCHRONISED");
