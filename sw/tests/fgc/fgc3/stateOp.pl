#!/usr/bin/perl

# File:    stateOp.pl
# Purpose: Tests the state machine for the operational state of the FGC

use Test::Utils;

use Time::HiRes qw(usleep);

use strict;
use warnings;
use diagnostics;



sub checkNormal 
{
    my ($d) = @_;

    my $state = get($d, "STATE.OP");

    cmp_ok($state, 'eq', "NORMAL");

    checkFgcStateFault($d, $state);
    checkSimulationWarning($d, $state);
}



sub checkSimulation 
{
    my ($d) = @_;

    my $state = get($d, "STATE.OP");

    cmp_ok($state, 'eq', "SIMULATION");

    checkFgcStateFault($d, $state);
    checkSimulationWarning($d, $state);
}



sub checkTesting 
{
    my ($d) = @_;

    my $state = get($d, "STATE.OP");

    cmp_ok($state, 'eq', "TEST");

    checkFgcStateFault($d, $state);
    checkSimulationWarning($d, $state);
}



sub checkCalibrating 
{
    my ($d) = @_;

    my $state = get($d, "STATE.OP");

    cmp_ok($state, 'eq', "CALIBRATING");

    checkFgcStateFault($d, $state);
    checkSimulationWarning($d, $state);
}



sub checkUnconfigured 
{
    my ($d) = @_;

    my $state = get($d, "STATE.OP");

    cmp_ok($state, 'eq', "UNCONFIGURED");

    checkFgcStateFault($d, $state);
    checkSimulationWarning($d, $state);
}



sub checkFgcStateFault 
{
    my ($d, $state) = @_;

    my $faultsLog = get($d, "STATUS.FAULTS");

    my @faults = split / /, $faultsLog;

    my $fgcStateFault = "";

    for (my $i = 0; $i < scalar @faults; $i++) {
        if ($faults[$i] eq "FGC_STATE") 
        {
            $fgcStateFault = $faults[$i];
            last;
        }
    }

    if (   $state eq "TEST"
        || $state eq "UNCONFIGURED") 
    {
        cmp_ok($fgcStateFault, 'eq', "FGC_STATE");
    }
    else
    {
        cmp_ok($fgcStateFault, 'ne', "FGC_STATE");
    }
}



sub checkSimulationWarning 
{
    my ($d, $state) = @_;

    my $warningsLog = get($d, "STATUS.WARNINGS");

    my @warnings = split / /, $warningsLog;

    my $simWarning = "";

    for (my $i = 0; $i < scalar @warnings; $i++) {
        if ($warnings[$i] eq "SIMULATION") 
        {
            $simWarning = $warnings[$i];
            last;
        }
    }

    if ($state eq "SIMULATION") 
    {
        cmp_ok($simWarning, 'eq', "SIMULATION");
    }
    else
    {
        cmp_ok($simWarning, 'ne', "SIMULATION");
    }
}



sub testRefDacSet
{
    my ($d, $state) = @_;

    if ($state eq "TEST") 
    {
        set($d, "REF.DAC", "1000,1000");

        my $refDacLog = get($d, "REF.DAC");

        # my @refDac = split /,/, $refDacLog;

        cmp_ok($refDacLog, 'eq', "1000,1000");
    }
    else
    {
        throws_ok { set($d, "REF.DAC", "1000,1000") } qr/bad state/, "Failed to set REF.DAC";
    }
}


sub testRefDacReset
{
    my ($d) = @_;

    my $refDacLog = get($d, "REF.DAC");

    cmp_ok($refDacLog, 'eq', "0,0");
}



# Main body

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};

# assureMainInOff($d, "SIMULATION");
my $state = get($d, "STATE.OP");

if ($state ne "SIMULATION") {
    set($d, "MODE.OP", "SIMULATION");
    wait_until(sub { get($d,"STATE.OP") eq ("SIMULATION") }, 20, 0.5);
}

set($d, "PC", "OFF");
wait_until(sub { get($d,"STATE.PC") eq ("OFF") }, 20, 0.5);

testRefDacSet($d, "SIMULATION");

# SM to NL

set($d, "MODE.OP", "NORMAL");
wait_until(sub { get($d,"STATE.OP") eq ("NORMAL") }, 20, 0.5);
checkNormal($d);
testRefDacSet($d, "NORMAL");

# NL to TT

set($d, "MODE.OP", "TEST");
wait_until(sub { get($d,"STATE.OP") eq ("TEST") }, 20, 0.5);
checkTesting($d);
testRefDacSet($d, "TEST");

# TT to NL

set($d, "MODE.OP", "NORMAL");
wait_until(sub { get($d,"STATE.OP") eq ("NORMAL") }, 20, 0.5);
checkNormal($d);
testRefDacReset($d);

# NL to CL (ADCS)
set($d, "CAL", "ADCS");
wait_until(sub { get($d,"STATE.OP") eq ("CALIBRATING") }, 20, 0.5);
checkCalibrating($d);
print "Calibrating ADCS...\n";
sleep(20);

# CL to NL

checkNormal($d);

# NL to CL (DACS)

set($d, "CAL", "DACS");
wait_until(sub { get($d,"STATE.OP") eq ("CALIBRATING") }, 20, 0.5);
checkCalibrating($d);
print "Calibrating DACS...\n";
sleep(10);

#CL to NL

checkNormal($d);

# NL to SM

set($d, "MODE.OP", "SIMULATION");
wait_until(sub { get($d,"STATE.OP") eq ("SIMULATION") }, 20, 0.5);
checkSimulation($d);

# SM to TT

set($d, "MODE.OP", "TEST");
wait_until(sub { get($d,"STATE.OP") eq ("TEST") }, 20, 0.5);
checkTesting($d);

# TT to CL (ADCS)

throws_ok { set($d, "CAL", "ADCS") } qr/bad state/, "Failed to go into Calibration (ADCS) from TEST";
sleep(1);
checkTesting($d);

# TT to CL (DACS)

throws_ok { set($d, "CAL", "DACS") } qr/bad state/, "Failed to go into Calibration (DACS) from TEST";
sleep(1);
checkTesting($d);
testRefDacSet($d, "TEST");


# TT to SM

set($d, "MODE.OP", "SIMULATION");
wait_until(sub { get($d,"STATE.OP") eq ("SIMULATION") }, 20, 0.5);
checkSimulation($d);
testRefDacReset($d);

# SM to CL (ADCS)

throws_ok { set($d, "CAL", "ADCS") } qr/bad state/, "Failed to go into Calibration (ADCS) from SIMULATION";
sleep(1);
checkSimulation($d);

# SM to CL (ADCS)

throws_ok { set($d, "CAL", "DACS") } qr/bad state/, "Failed to go into Calibration (DACS) from SIMULATION";
sleep(1);
checkSimulation($d);

# Test that it is not possible to go into TEST from these states

my $class = get($d, "DEVICE.CLASS_ID"); 

set($d, "PC", "OF");
set($d, "VS.BLOCKABLE", "ENABLED");

set($d, "PC", "BK");
wait_until(sub { get($d,"STATE.PC") eq ("BLOCKING") }, 20, 0.5);
throws_ok { set($d, "MODE.OP", "TEST") } qr/bad state/, "Cannot go into TEST from STATE.PC BK";

set($d, "PC", "CY");
wait_until(sub { get($d,"STATE.PC") eq ("TO_CYCLING") }, 20, 0.5);
throws_ok { set($d, "MODE.OP", "TEST") } qr/bad state/, "Cannot go into TEST from STATE.PC CY";

if ($class != 62) {
    set($d, "PC", "IL");
    wait_until(sub { get($d,"STATE.PC") eq ("IDLE") }, 20, 0.5);
    throws_ok { set($d, "MODE.OP", "TEST") } qr/bad state/, "Cannot go into TEST from STATE.PC IL";

    set($d, "PC", "DT");
    wait_until(sub { get($d,"STATE.PC") eq ("DIRECT") }, 20, 0.5);
    throws_ok { set($d, "MODE.OP", "TEST") } qr/bad state/, "Cannot go into TEST from STATE.PC DT";

    set($d, "PC", "SB");
    wait_until(sub { get($d,"STATE.PC") eq ("ON_STANDBY") }, 20, 0.5);
    throws_ok { set($d, "MODE.OP", "TEST") } qr/bad state/, "Cannot go into TEST from STATE.PC SB";
}

set($d, "PC", "OF");
wait_until(sub { get($d,"STATE.PC") eq ("OFF") }, 20, 0.5);

# Test Polarity Switch

# my $polSwitchTimeout = get($d, "SWITCH.POLARITY.TIMEOUT");
# set($d, "SWITCH.POLARITY.TIMEOUT", "5");
# set($d, "SWITCH.POLARITY.AUTO", "DISABLED");
# set($d, "SWITCH.POLARITY.MODE", "POSITIVE");
# sleep(5);
# my $polSwitch = get($d, "SWITCH.POLARITY.STATE");
# cmp_ok($polSwitch, 'eq', "POSITIVE");

# set($d, "MODE.OP", "NORMAL");
# sleep(5);
# $polSwitch = get($d, "SWITCH.POLARITY.STATE");
# cmp_ok($polSwitch, 'eq', "NEGATIVE");
# set($d, "SWITCH.POLARITY.TIMEOUT", $polSwitchTimeout);

# UNCONFIGURED

my $a_gain   = get($d, "DCCT.A.GAIN");
my $b_gain   = get($d, "DCCT.A.GAIN");
my $tau_temp = get($d, "DCCT.TAU_TEMP");

my $pulse_pos;
my $pulse_neg;
my $vsrdy_timeout;
my $adc_int_sig;

if ($class == 62) {
    $pulse_pos     = get($d, "LIMITS.PULSE.POS");
    $pulse_neg     = get($d, "LIMITS.PULSE.NEG");
    $vsrdy_timeout = get($d, "VS.VSRDY_TIMEOUT");
    $adc_int_sig   = get($d, "ADC.INTERNAL.SIGNALS");
}

set($d, "CONFIG.UNSET");

my $gateway = get($d, "DEVICE.HOSTNAME");

foreach my $slot (1..30)
{
    set($gateway, "CODE.SLOT$slot", "");
}

set($d, "DEVICE.RESET");
sleep(30);
checkUnconfigured($d);

set($d, "DCCT.A.GAIN",   sprintf("%d", $a_gain));
set($d, "DCCT.B.GAIN",   sprintf("%d", $b_gain));
set($d, "DCCT.TAU_TEMP", sprintf("%d", $tau_temp));

if ($class == 62) {
    set($d, "LIMITS.PULSE.POS",     sprintf("%d", $pulse_pos));
    set($d, "LIMITS.PULSE.NEG",     sprintf("%d", $pulse_neg));
    set($d, "VS.VSRDY_TIMEOUT",     sprintf("%d", $vsrdy_timeout));
    set($d, "ADC.INTERNAL.SIGNALS", sprintf("%s", $adc_int_sig));
}
