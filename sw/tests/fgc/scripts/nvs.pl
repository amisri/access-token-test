#!/usr/bin/perl

# File:    nvs.pl
# Purpose: Tests the NVRAM memory

use feature qw(switch);
use Test::Utils;


# Global variables

my @prop_type_size = (8, 1, 1, 44, 8, 24, 4, 8, 2, 2, 4, 4, 1, 1, 1, 76, 52, 12, 6, 0, 0, 8, 768, 4, 4);

my $data;
my $addr;
my $idx        = 0;
my $bytes_left = 0;
my $bytes_max  = 256;


# Init FGC state

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file

load_config($d, $params{config}) if $params{config};



# Subroutines

sub retrieveData
{
    set($d, "FGC.DEBUG.MEM.MCU", $addr);
    
    $data = get($d, "FGC.DEBUG.MEM.MCU");
    $data =~ s/[,]//g;
    $data =~ s/0x//g;

    $bytes_left = $bytes_max;
    $addr       = $addr + $bytes_max;
    $idx        = 0;
}



sub getData
{
    my ($num_bytes) = @_;

    my $retval;

    while ($num_bytes > 0)
    {
        if ($num_bytes > $bytes_left)
        {
            $retval     = $retval . substr($data, $idx * 2, $bytes_left);
            $num_bytes  = $num_bytes - $bytes_left;

            retrieveData();
        }
        else
        {
            $retval = $retval . substr($data, $idx * 2, $num_bytes * 2);
            $bytes_left = $bytes_left - $num_bytes;
            $idx        = $idx + $num_bytes;
            $num_bytes  = 0;
        }
    }

    return $retval;
}


sub printValue
{    
    my ($type, $raw) = @_;

    given ($type)
    {
        when(0)  { printf("%d ", hex($raw));                           }   #  0 ABSTIME
        when(1)  { printf("%s ", (hex($raw) == 0 ? "False" : "True")); }   #  1 BOOL
        when(2)  { printf("%c ", hex($raw));                           }   #  2 CHAR
        when(3)  { printf("%d ", hex($raw));                           }   #  3 DEV_NAME
        when(4)  { printf("%f ", (unpack "f", pack "L", hex($raw)))    }   #  4 DOUBLE
        when(5)  { printf("Invalid ");                                 }   #  5 FIFOADC
        when(6)  { printf("%f ", (unpack "f", pack "L", hex($raw)));   }   #  6 FLOAT
        when(7)  { printf("%f ", (unpack "f", pack "L", hex($raw)));   }   #  7 FL_POINT
        when(8)  { printf("%d ", hex($raw));                           }   #  8 INT16S
        when(9)  { printf("%d ", hex($raw));                           }   #  9 INT16U
        when(10) { printf("%d ", hex($raw));                           }   # 10 INT32S
        when(11) { printf("%d ", hex($raw));                           }   # 11 INT32U
        when(12) { printf("%d ", hex($raw));                           }   # 12 INT8S
        when(13) { printf("%d ", hex($raw));                           }   # 13 INT8U
        when(14) { printf("Invalid ");                                 }   # 14 LOG
        when(15) { printf("Invalid ");                                 }   # 15 LOGEVT
        when(16) { printf("Invalid ");                                 }   # 16 LOGPSU
        when(17) { printf("Invalid ");                                 }   # 17 LOGTIMING
        when(18) { printf("%s ", hex($raw));                           }   # 18 MAC        
        when(19) { printf("Invalid ");                                 }   # 19 NULL
        when(20) { printf("Invalid ");                                 }   # 20 PARENT
        when(21) { printf("%f|%f ", (unpack "f", pack "L", hex(substr($raw, 2, 8))), (unpack "f", pack "L", hex(substr($raw, 0, 8)))); } # 21 POINT
        when(22) { printf("Invalid ");                                 }   # 22 RUNLOG
        when(23) { printf("%s ", hex($raw));                           }   # 23 STRING
        when(24) { printf("%d ", hex($raw));                           }   # 24 UNIXTIME
        default  { printf("Invalid ");                                 }
    }
}



# Read all the records with data

for (my $address = 0x0507D008; $address < 0x05080000; $address = $address + 48)
{
    set($d, "FGC.DEBUG.MEM.MCU", $address);

    # Retrieve record

    my $record = get($d, "FGC.DEBUG.MEM.MCU");

    $record =~ s/[,]//g;
    $record =~ s/0x//g;

    $record = substr($record ,   0, 96);
    
    my $prop = substr($record,  0, 8);
       $addr = '0x' . substr($record,  8, 8);
    my $size = substr($record, 16, 4);
    my $name = substr($record, 20);

    $name =~ s/00//g;

    if ($name eq "")
    {
        next;
    }


    $name = unpack("a*", pack('U0H*', $name));

    # Retrieve property metadata

    my $prop_info   = get($d, $name . ' INFO');
    my $flags       = substr($prop_info, index($prop_info, "flags:") + 6, 6);
    my $nvs_idx     = substr($prop_info, index($prop_info, "nvs_idx:") + 8, 4);
    my $type        = int(substr($prop_info, index($prop_info, "type:") + 5, 2));
    my $max_els     = int(substr($prop_info, index($prop_info, "max_elements:") + 13, 4));
    my $size        = $prop_type_size[$type];
    my $max_cyc_sel = 0;
    my $max_sub_sel = 0;

    if ((eval $flags) & 0x1000)
    {
         $max_sub_sel = 4;
    }

    if ((eval $flags) & 0x0400)
    {
         $max_cyc_sel = 31;
    }

    if (hex($nvs_idx) < 0x9f)
    {
        next
    }


    printf("%s\n", $name);
    printf("Indx: %s    Type: %s   Size: %d   Max: %d   Prop: %s  Address: %s   Flags: %s\n", 
           $nvs_idx, $type, $size, $max_els, $prop, $addr, $flags);

    $addr = hex($addr);

    retrieveData();

    # print $data;

    for (my $sub_sel = 0; $sub_sel <= $max_sub_sel; $sub_sel++) 
    {
         for (my $cyc_sel = 0; $cyc_sel <= $max_cyc_sel; $cyc_sel++) 
         {
            my $num_els_str = getData(2);
            my $num_els = hex($num_els_str);

            if ($num_els == 0xFFFF)
            {
                $num_els_str = "NotSet";
                $num_els = 0;
            }

            if ($num_els == 0xFFFE)
            {
                $num_els_str = "Corrupted";
                $num_els = 0;
            }

            printf("%d %2d %s: ", $sub_sel, $cyc_sel, $num_els_str);

            for (my $element = 0; $element < $num_els; $element++)            
            {
                my $raw = '0x'.getData($size);

                printValue($type, $raw);
            }

            # Disacard unset elements

            getData(($max_els - $num_els) * $size);

            printf("\n");
         }
     }

    printf("\n");
}
