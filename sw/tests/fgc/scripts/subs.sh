#!/bin/sh

for i in {1..10} 
do
    echo "[$i] Subscribing...";
    rda-subscribe -d ----- -p MEAS.ACQ_VALUE -r location -c PSB.USER.ALL&
    rda-subscribe -d ----- -p MEAS.ACQ_VALUE -r location -c PSB.USER.ALL&

    sleep 5

    echo "[$i] Killing...";

    kill %1
    kill %2

    sleep 1
done
