#!/usr/bin/perl

use Test::Utils;

my %params = (device=>'RPPEL.866.MBI', config=>'', protocol=>'TCP');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# [1] Configure device

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") =~ /OFF/ } );

ok(1,'OFF done');

set($d,"MODE.OP","SIMULATION");
wait_until( sub { get($d,"MODE.OP") =~ /SIMULATION/ } );

ok(1,'OP SIMULATION done');


# [2] Arming in IDLE: test function

set($d,"PC","IL");
wait_until( sub { get($d,"STATE.PC") =~ /IDLE/ } );

ok(1,'PC IL done');

set($d,"REF","NOW,500");

sleep 3;

ok(abs(get($d,"MEAS.I") - 500) < 0.5, "IDLE current ok");

set($d,"REF.TEST.AMPLITUDE"  , "10");
set($d,"REF.TEST.NUM_PERIODS", "5");
set($d,"REF.TEST.PERIOD"     , "0.1");

set($d,"REF.FUNC.TYPE"		 , "SINE");

ok(get($d,"REF.INFO.DURATION") == 0.5,"REF DURATION ok");

sleep 1;

# [3] Arming in IDLE: table function

set($d, "PC", "IL");
wait_until( sub { get($d,"STATE.PC") =~ /IDLE/ } );

set($d,"REF.TABLE.FUNCTION","0|500,2|520,4|510");

ok(get($d,"REF.INFO.DURATION")  == 4   , "REF DURATION ok");
ok(get($d,"REF.INFO.START_REF") == 500 , "REF START ok");
ok(get($d,"REF.INFO.END_REF")   == 510 , "REF END ok");
ok(get($d,"REF.INFO.MIN_REF")   == 500 , "REF MIN_REF ok");
ok(get($d,"REF.INFO.MAX_REF")   == 520 , "REF MIN_REF ok");


# [4] Arming in IDLE: run armed function to check if END_REF is reached

set($d, "REF.RUN","");

sleep 5;

ok(abs(get($d,"MEAS.I") - 510) < 0.5, "MEAS.I ok");

set($d, "PC", "OF");

sleep 5;

# [5] PPM: Test arming functions for different users

set($d,"REF.TABLE.FUNCTION(1)","0|1000,2|1020,4|1010");

set($d,"REF.TABLE.FUNCTION(2)","0|2000,4|2020,8|2010");

# [6] PPM: Check that armed functions correspond with what we requested

ok(get($d,"REF.INFO.DURATION(1)")  == 4   ,  "REF DURATION ok");
ok(get($d,"REF.INFO.START_REF(1)") == 1000 , "REF START ok");
ok(get($d,"REF.INFO.END_REF(1)")   == 1010 , "REF END ok");
ok(get($d,"REF.INFO.MIN_REF(1)")   == 1000 , "REF MIN_REF ok");
ok(get($d,"REF.INFO.MAX_REF(1)")   == 1020 , "REF MIN_REF ok");

ok(get($d,"REF.INFO.DURATION(2)")  == 8   ,  "REF DURATION ok");
ok(get($d,"REF.INFO.START_REF(2)") == 2000 , "REF START ok");
ok(get($d,"REF.INFO.END_REF(2)")   == 2010 , "REF END ok");
ok(get($d,"REF.INFO.MIN_REF(2)")   == 2000 , "REF MIN_REF ok");
ok(get($d,"REF.INFO.MAX_REF(2)")   == 2020 , "REF MIN_REF ok");