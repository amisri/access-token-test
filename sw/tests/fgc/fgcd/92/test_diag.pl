#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;

my %params = (device=>undef, config=>'', protocol=>'TCP');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

ok(get($d, 'DIAG.DIG') =~ /FGC_PSU.*STA.*DIAG/, 'DIAG.DIG not empty');
ok(get($d, 'DIAG.ANA') =~ /FGC_PSU.*PSU.*PSU.*PSU.*PSU/, 'DIAG.ANA not empty');
ok(get($d, 'DIAG.DIMS_EXPECTED') !~ /0,0/, 'DIAG.DIMS_EXPECTED not empty');
