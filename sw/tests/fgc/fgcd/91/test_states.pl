#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;

sub max ($$) { $_[$_[0] < $_[1]] }

sub min ($$) { $_[$_[0] > $_[1]] }

my %params = (device=>undef, config=>'', protocol=>'TCP');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# For the time being class 62 and 63 are not coverred for this test

if(get($d,"DEVICE.CLASS_ID") == 63 || get($d,"DEVICE.CLASS_ID") == 62)
{
    exit 0;
}

# Reset

set($d,"PC","OFF");
set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") =~ /OFF/ } );

# Reset FGC

set($d,"PC","OFF");
set($d,"PC","OFF");

# OFF -> SB -> IDLE --> OFF

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'SB');
wait_until( sub { get($d,"STATE.PC") eq 'ON_STANDBY' } , 120 );
set($d, 'PC', 'IDLE');
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } , 120 );

# Check that we can set the current to some value (+/- 0.1%)

my $pos = get($d, 'LIMITS.I.POS[0]');
my $minimum = get($d, 'LIMITS.I.MIN[0]');
my $ref = ($pos - $minimum)*0.1 + $minimum;

set($d, "REF","now,$ref");

wait_until( sub { abs(get($d,"MEAS.I") - $ref) < $pos*0.001  }, 120 );
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );


set($d, 'PC', 'OFF');
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

ok(1, "OFF -> SB -> IDLE --> (change reference from $minimum to $ref +/- 0.1%) --> OFF");

# OFF -> SB -> IDLE --> SLOW_ABORT --> IDLE --> SLOW_ABORT --> ON_STANBY --> SLOW_ABORT --> OFF

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'IDLE');
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } , 120);
set($d, "REF","now,$ref");

wait_until( sub { abs(get($d,"MEAS.I") - $ref) < $pos*0.001  }, 120 );
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' });

set($d, 'PC', 'SLOW_ABORT');
set($d, 'PC', 'IDLE');
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

set($d, 'PC', 'SLOW_ABORT');

wait_until( sub { get($d,"STATE.PC") eq 'OFF' } , 120 );

ok(1,'OFF -> SB -> IDLE --> SLOW_ABORT --> --> IDLE --> SLOW_ABORT --> ON_STANBY --> SLOW_ABORT --> OFF');

# OFF --> CY --> OFF

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'CYCLING');
wait_until( sub { get($d,"STATE.PC") =~ /CYCLING|ECONOMY/ } );

for my $i (1 .. 31)
{
    set($d,"REF.FUNC.REG_MODE($i)","I");
    set($d,"REF.TABLE.FUNCTION($i)","0|$minimum,0.2|$minimum,0.4|$pos,0.6|$pos,1.0|$minimum");
    set($d,"REF.FUNC.TYPE($i)","TABLE");
}


# sleep a bit to let the converter cycle a bit

sleep 10;
    
set($d, 'PC', 'OFF');
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

ok(1,'OFF --> CY --> OFF');

# OFF --> CY --> SLOW_ABORT

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'CY');
wait_until( sub { get($d,"STATE.PC") =~ /CYCLING|ECONOMY/ } );
set($d, 'PC', 'SLOW_ABORT');
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

ok(1,'OFF -> CY -> SLOW_ABORT');
