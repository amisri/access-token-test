#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;

sub max ($$) { $_[$_[0] < $_[1]] }

sub min ($$) { $_[$_[0] > $_[1]] }

my %params = (device=>undef, config=>'', protocol=>'TCP');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# For the time being class 62 and 63 are not coverred for this test

if(get($d,"DEVICE.CLASS_ID") != 91)
{
    exit 0;
}

# Reset

set($d,"PC","OFF");
set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") =~ /OFF/ } );

# Retrieve limits

my $pos = get($d, 'LIMITS.I.POS[0]');
my $minimum = get($d, 'LIMITS.I.MIN[0]');
my $maximum = ($pos - $minimum)*0.1 + $minimum;

# OFF -> IDLE --> OFF

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'IDLE');
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } , 120 );

# Check that we can set the current to some value (+/- 0.1%)

set($d, "REF.CCV.VALUE","$maximum");
sleep 1;

wait_until( sub { abs(get($d,"MEAS.I") - $maximum) < $pos*0.01  }, 120 );
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

my $r = rand()*($maximum-$minimum) + $minimum;

set($d, "REF.CCV.VALUE","$r");
sleep 1;

wait_until( sub { abs(get($d,"MEAS.I") - $r) < $pos*0.01 } , 120);
wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } , 120);

set($d, 'PC', 'OFF');
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

ok(1, "OFF -> SB -> IDLE --> (change reference +/- 0.1%) --> OFF");

