#!/usr/bin/perl

use Test::Utils;

my %params = (device=>undef, config=>'', protocol=>'TCP');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") =~ /OFF/ } );

ok(1,'OFF done');

sleep 1;


# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Retrieve limits

my $pos = get($d, 'LIMITS.V.POS[0]') * 0.1;
ok(1,'Limits values retrieved');

set($d,'REF.VCV.VALUE',$pos);
ok(1,'set the voltage to some value (+/- 0.1%) done');

# OFF -> DT

set($d,"PC","OFF");
sleep 1;
set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'DT');
wait_until( sub { get($d,"STATE.PC") eq 'DIRECT' } );

ok(1,'# OFF -> SB -> DT done');



# Simulate a Fault

set($d, "FGC.FAULTS", "VS_FAULT");
ok(1,'Fault is done');

wait_until( sub { 
                  my $meas = get($d,"MEAS.V");
                  $meas < 10 
                  and (get($d,"STATE.PC") eq 'FLT_OFF')
                } );

sleep 1;

ok(1,'PC set to FLT_OFF after stopping');

# Reset the Fault

set($d, "VS", "RESET");
sleep 1;
set($d, "VS", "RESET");

wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

ok(1,'Fault has been RESET successfully. TEST FAULT OK.');

