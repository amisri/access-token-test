#!/usr/bin/perl

use Test::Utils;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Stress Get small commands (5 min)

my $N     = 100;
my $count = 1;

while($count != $N)
{
    get($d,"TIME.NOW"); 
    print "$d GET TIME.NOW: $count/$N\r";
    ++$count;
}

ok(1,"$d GET TIME.NOW: $N requests");

# Stress Set small commands (5 min)

$count = 1;

while($count != $N)
{
    set($d,"TEST.FLOAT",(rand(2)-1)*rand(10**30)); 
    print "$d SET TEST.FLOAT: $count/$N\r";
    ++$count;
}

ok(1,"$d SET TEST.FLOAT: $N requests");

# Stress Set big commands (5 min)

# FGC to OFF

set($d,"MODE.PC","OFF");

$count = 1;

while($count != $N)
{
    get($d,"REF.VCV"); 
    print "$d GET REF.VCV: $count/$N\r";
    ++$count;
}

ok(1,"$d GET REF.VCV: $N requests");




# EOF
