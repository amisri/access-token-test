#!/usr/bin/perl

use Test::Utils;

my %params = (device=>undef, config=>'', protocol=>'TCP');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") =~ /OFF/ } );

ok(1,'OFF done');

sleep 1;

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Retrieve limits

my $pos = get($d, 'LIMITS.V.POS[0]') * 0.2;
my $minimum = get($d, 'LIMITS.V.MIN[0]');
my $neg = get($d, 'LIMITS.V.NEG[0]') * 0.2;

ok(1,'Limits values retrieved');

# OFF -> SB -> DT--> OFF

set($d,"PC","OFF");
sleep 1;
set($d,"PC","OFF");
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );
set($d, 'PC', 'SB');
wait_until( sub { get($d,"STATE.PC") eq 'ON_STANDBY' } );


ok(1,'# OFF -> SB done');


# Check that we can set the voltage to some value (+/- 0.1%)

set($d,'REF.VCV.VALUE',$pos);
ok(1,'set the voltage to some value (+/- 0.1%) done');

set($d, 'PC', 'DT');
wait_until( sub { get($d,"STATE.PC") eq 'DIRECT' } );
ok(1,'# DT done');

wait_until( sub { 
                  my $meas = get($d,"MEAS.V");
                  $meas > ($pos-10) and $meas < ($pos+10) 
                } );

set($d,"REF.VCV.VALUE",$neg);
ok(1,'set the voltage to some value (+/- 0.1%) done');

wait_until( sub { 
                  my $meas = get($d,"MEAS.V");
                  $meas < ($neg+10) and $meas > ($neg-10)
                } );

set($d, 'PC', 'OFF');
wait_until( sub { get($d,"STATE.PC") eq 'OFF' } );

ok(1,'OFF -> SB -> DT (change reference +/- 0.1%) --> OFF');
