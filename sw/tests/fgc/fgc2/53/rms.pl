#!/usr/bin/perl

# File:    table.pl

use Test::Utils;

use strict;
use warnings;
use diagnostics;



sub testTable
{
    my ($time_vector, $ref_vector, $result_time_vector, $result_ref_vector, $label) = @_;

    for my $i (0..$#{$time_vector})
    {
        cmp_ok($time_vector->[$i], '==', $result_time_vector->[$i], "${label}Time of point $i ok");
        cmp_ok($ref_vector->[$i],  '==', $result_ref_vector->[$i],  "${label}Ref of point $i ok");
    }
}



sub startConverterAndCycle
{
    my ($d) = @_;

    set($d, "PAL", "RESET");
    set($d, "PC", "OFF");
    set($d, "PAL.LINKS.STATUS", "STARTING");
    set($d, "PAL.LINKS.STATUS", "ON");

    wait_until( sub { get($d, "STATE.PC") eq "ON_STANDBY"} );
    setPc($d, "CY");
}



my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

set($d, "PAL.LINKS.STATUS", "FAULT");

assureMainInOff($d, "SIMULATION");

load_config($d, $params{config}) if $params{config};

my $user = 5;

set($d, "FGC.CYC.SIM", "${user}04");

my @time_vector = (0, 0.2, 4.5, 4.7);
my @ref_vector  = (0, 300, 300, 0);

my $function = zipTable(\@time_vector, \@ref_vector);

set($d, "REF.FUNC.REG_MODE($user)", "I");
set($d, "REF.TABLE.FUNCTION($user)", $function);

startConverterAndCycle($d);

# Wait for supercycle to kick in and for RMS to stabilise

sleep(20);

# Expected RMS was calculated manually in rms.ods

my $rms          = get($d, "MEAS.I_RMS");
my $expected_rms = 288.3;
my $tolerance    = 0.1;

cmp_ok($rms, '<', $expected_rms + $tolerance, "RMS 1 ok");
cmp_ok($rms, '>', $expected_rms - $tolerance, "RMS 1 ok");

setPc($d, "SB");

set($d, "PAL.LINKS.STATUS", "FAULT");

set($d, "FGC.CYC.SIM", "${user}04,${user}04,${user}04");

startConverterAndCycle($d);

# Wait for supercycle to kick in and for RMS to stabilise

sleep(30);

# For three repeated cycles, RMS should be the same as for one cycle

$rms = get($d, "MEAS.I_RMS");

cmp_ok($rms, '<', $expected_rms + $tolerance, "RMS 2 ok");
cmp_ok($rms, '>', $expected_rms - $tolerance, "RMS 2 ok");

# EOF
