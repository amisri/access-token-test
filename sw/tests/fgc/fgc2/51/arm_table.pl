#!/usr/bin/perl

# File:    arm_table.pl
# Author:  Krzysztof Lebioda
# Purpose: Testing of function arming in class 51
#          When a funcion is successfuly armed then it should be recovered from the NVS
#          after a reset

# TODO Review if this test makes sense with the new table format!!!

use Test::Utils;
use Math::Round;

use strict;
use warnings;
use diagnostics;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Prepare table data

my @table_values_f = (5.00000E+02,
                      6.00000E+02 + rand(50),
                      6.00000E+02 + rand(50),
                      5.00000E+02);

my @table_values_s;

foreach my $value (@table_values_f)
{
    push @table_values_s, sprintf("%.5E", $value);
}

my $table_ref1      = join(",", @table_values_s);
my $table_ref2      = "5.00000E+02,7.00000E+02,7.00000E+02,5.00000E+02,5.00000E+02";
my $table_ref_time1 = "0.00000E+00,5.00000E-01,3.50000E+00,4.00000E+00";
my $table_ref_time2 = "0.00000E+00,1.00000E+00,4.40000E+00";
my $reg_mode1       = "I";
my $reg_mode2       = "V";

# Go to IDLE

assureMainInOff($d, "SIMULATION");
setPc($d, "IL");
ok(1, "In IDLE");

# set($d, "REF.PPPL.ACCELERATION1", 1 + rand()); 
# set($d, "REF.PPPL.ACCELERATION2", 1 + rand()); 
# set($d, "REF.PPPL.ACCELERATION3", 1 + rand()); 
# set($d, "REF.PPPL.RATE2",         1 + rand()); 
# set($d, "REF.PPPL.RATE4",         1 + rand()); 
# set($d, "REF.PPPL.REF4",          1 + rand()); 
# set($d, "REF.PPPL.DURATION4",     1 + rand());

# Arm a function

set($d, "REF.FUNC.REG_MODE",  $reg_mode1);
set($d, "REF.TABLE.REF",      $table_ref1);
set($d, "REF.TABLE.REF_TIME", $table_ref_time1);
set($d, "REF.FUNC.TYPE",      "TABLE");
wait_until( sub { get($d,"STATE.PC") eq 'ARMED' } );
ok(1, "In ARMED");

# Reset the FGC

setPc($d, "OF");
set($d, "DEVICE.RESET", "");
sleep(20);

# After the reset the function should be recovered from the NVS

is(get($d, "REF.FUNC.REG_MODE"),  $reg_mode1,       "REF.FUNC.REG_MODE after first reset");
is(get($d, "REF.TABLE.REF"),      $table_ref1,      "REF.TABLE.REF after first reset");
is(get($d, "REF.TABLE.REF_TIME"), $table_ref_time1, "REF.TABLE.REF_TIME after first reset");
is(get($d, "REF.FUNC.TYPE"),      "NONE",           "REF.FUNC.TYPE after first reset");

# is(get($d, "REF.PPPL.ACCELERATION1"), "1.00000E+00", "REF.PPPL.ACCELERATION1 after first reset");
# is(get($d, "REF.PPPL.ACCELERATION2"), "1.00000E+00", "REF.PPPL.ACCELERATION2 after first reset");
# is(get($d, "REF.PPPL.ACCELERATION3"), "1.00000E+00", "REF.PPPL.ACCELERATION3 after first reset");
# is(get($d, "REF.PPPL.RATE2"),         "1.00000E+00", "REF.PPPL.RATE2 after first reset");
# is(get($d, "REF.PPPL.RATE4"),         "1.00000E+00", "REF.PPPL.RATE4 after first reset");
# is(get($d, "REF.PPPL.REF4"),          "1.00000E+00", "REF.PPPL.REF4 after first reset"); 
# is(get($d, "REF.PPPL.DURATION4"),     "1.00000E+00", "REF.PPPL.DURATION4 after first reset");

# Go to IDLE

assureMainInOff($d, "SIMULATION");
setPc($d, "IL");
ok(1, "In IDLE");

# Change the table to an invalid one (ref array is longer) and try to arm

set($d, "REF.FUNC.REG_MODE",  $reg_mode2);
set($d, "REF.TABLE.REF",      $table_ref2);
set($d, "REF.TABLE.REF_TIME", $table_ref_time2);
dies_ok sub { set($d, "REF.FUNC.TYPE", "TABLE"); }, "Arm request failed";

# Invalid function should still be in the properties

is(get($d, "REF.FUNC.REG_MODE"),  $reg_mode2,       "REF.FUNC.REG_MODE after failed arm request");
is(get($d, "REF.TABLE.REF"),      $table_ref2,      "REF.TABLE.REF after failed arm request");
is(get($d, "REF.TABLE.REF_TIME"), $table_ref_time2, "REF.TABLE.REF_TIME after failed arm request");
is(get($d, "REF.FUNC.TYPE"),      "NONE",           "REF.FUNC.TYPE after failed arm request");

# Reset the FGC

setPc($d, "OF");
set($d, "DEVICE.RESET", "");
sleep(20);

# After the reset the valid function should be recovered from the NVS

is(get($d, "REF.FUNC.REG_MODE"),  $reg_mode1,       "REF.FUNC.REG_MODE after second reset");
is(get($d, "REF.TABLE.REF"),      $table_ref1,      "REF.TABLE.REF after second reset");
is(get($d, "REF.TABLE.REF_TIME"), $table_ref_time1, "REF.TABLE.REF_TIME after second reset");
is(get($d, "REF.FUNC.TYPE"),      "NONE",           "REF.FUNC.TYPE after second reset");

# EOF