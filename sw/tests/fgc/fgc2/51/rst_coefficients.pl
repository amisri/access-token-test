#!/usr/bin/perl

# File:    rst_coefficients.pl
# Purpose: Testing of manual and default RST coefficients
#          This test uses a config file taken from a real converter, which uses a
#          set of manual RST coefficients

use Test::Utils;

use strict;
use warnings;
use diagnostics;



sub testManualAndOp($$$)
{
    # Make sure that the operational RST coefficients are pointing to the manual ones
    # We need to clip operational coefficients to 5 digits, because it's a non-config property, while
    # REG.I.MANUAL.* are config. The FGC returns 7 digits for non-config properties and 5 for config ones.

    my ($d, $manual, $op) = @_;

    is  (get($d, "$manual"), sprintf("%.5E", get($d, "$op")), "$manual and $op are equal");
    isnt(get($d, "$manual"), 10,                              "$manual overwritten by config");
}

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Go to OFF and run in full simulation mode

assureMainInOff($d, "SIMULATION");

# Sanity checks

ok(get($d, "LOAD.SELECT") == 0, "Load select 0");

ok(get($d, "REG.I.CLBW[0]") > 0, "Default RST coefficients enabled");

ok(getPropertyInfo($d, "REG.I.OP.R")->{"max_elements"} == 16, "Max number of operational R coefficients");
ok(getPropertyInfo($d, "REG.I.OP.S")->{"max_elements"} == 16, "Max number of operational S coefficients");
ok(getPropertyInfo($d, "REG.I.OP.T")->{"max_elements"} == 16, "Max number of operational T coefficients");

ok(getPropertyInfo($d, "REG.I.MANUAL.R")->{"max_elements"} == 16, "Max number of manual R coefficients");
ok(getPropertyInfo($d, "REG.I.MANUAL.S")->{"max_elements"} == 16, "Max number of manual S coefficients");
ok(getPropertyInfo($d, "REG.I.MANUAL.T")->{"max_elements"} == 16, "Max number of manual T coefficients");

foreach my $i (0..15)
{
    set($d, "REG.I.MANUAL.R[$i]", 10);
    ok(get($d, "REG.I.MANUAL.R[$i]") == 10, "REG.I.MANUAL.R[$i] set");
    set($d, "REG.I.MANUAL.S[$i]", 10);
    ok(get($d, "REG.I.MANUAL.S[$i]") == 10, "REG.I.MANUAL.S[$i] set");
    set($d, "REG.I.MANUAL.T[$i]", 10);
    ok(get($d, "REG.I.MANUAL.T[$i]") == 10, "REG.I.MANUAL.T[$i] set");
}

foreach my $i (0..15)
{
    ok(get($d, "REG.I.MANUAL.R[$i]") != get($d, "REG.I.OP.R[$i]"), "REG.I.MANUAL.R[$i] and REG.I.OP.R[$i] differ");
    ok(get($d, "REG.I.MANUAL.S[$i]") != get($d, "REG.I.OP.S[$i]"), "REG.I.MANUAL.S[$i] and REG.I.OP.S[$i] differ");
    ok(get($d, "REG.I.MANUAL.T[$i]") != get($d, "REG.I.OP.T[$i]"), "REG.I.MANUAL.T[$i] and REG.I.OP.T[$i] differ");
}

# First test regulation with default RST coefficients

# Go to STANDBY and then IDLE

setPc($d, "SB");
ok(get($d, "REF.I") == 500, "Standby reference reached");
setPc($d, "IL");

# Ramp to 1000

set($d, "REF", "NOW,1000");
wait_until( sub { get($d, "STATE.PC") eq "RUNNING"});
wait_until( sub { get($d, "STATE.PC") eq "IDLE"});
ok(get($d, "REF.I") == 1000, "Requested reference reached");

# Go to OFF

setPc($d, "OF");

# Load config with manual RST coefficients

load_config($d, $params{config}) if $params{config};

ok(get($d, "REG.I.CLBW[0]") == 0, "Manual RST coefficients enabled");

foreach my $i (0..15)
{
    testManualAndOp($d, "REG.I.MANUAL.R[$i]", "REG.I.OP.R[$i]");
    testManualAndOp($d, "REG.I.MANUAL.S[$i]", "REG.I.OP.S[$i]");
    testManualAndOp($d, "REG.I.MANUAL.T[$i]", "REG.I.OP.T[$i]");
}

# Go to STANDBY and then IDLE

setPc($d, "SB");
ok(get($d, "REF.I") == 0, "Standby reference reached");
setPc($d, "IL");

# Ramp to 5. The load from the config has a big time constant, but it should fit into the 60 default timeout of wait_until

set($d, "REF", "NOW,5");
wait_until( sub { get($d, "STATE.PC") eq "RUNNING"});
wait_until( sub { get($d, "STATE.PC") eq "IDLE"});
ok(get($d, "REF.I") == 5, "Requested reference reached");

# Turn off the converter and restore the original config

setPc($d, "OF");
syncFgc($d);

# EOF
