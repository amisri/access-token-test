#!/usr/bin/perl

# File:    arm_table.pl
# Author:  Krzysztof Lebioda
# Purpose: Reset testing
#          Reset should be possible only in OFF, FLT_OFF, STOPPING and FLT_STOPPING

# TODO: Add cycling

use Test::Utils;

use strict;
use warnings;
use diagnostics;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "SIMULATION");

# Reset the FGC when in OFF

setPc($d, "OF");
set($d, "DEVICE.RESET", "");
ok(1, "Reset in OFF");

assureMain($d);

# Reset the FGC when in FLT_OFF
# After the reset it should start in FLT_OFF

wait_until( sub { get($d,"STATE.PC") eq 'FLT_OFF' } );
set($d, "DEVICE.RESET", "");
ok(1, "Reset in FLT_OFF");

# Reset the FGC when in STOPPING

assureMainInOff($d, "SIMULATION");

setPc($d, "SB");
set($d, "PC", "OF");
wait_until( sub { get($d,"STATE.PC") eq 'STOPPING' } );
set($d, "DEVICE.RESET", "");
ok(1, "Reset in STOPPING");

# Reset the FGC when in FLT_STOPPING

assureMainInOff($d, "SIMULATION");

setPc($d, "SB");
set($d, "FGC.FAULTS", "VS_FAULT");
wait_until( sub { get($d,"STATE.PC") eq 'FLT_STOPPING' } );
set($d, "DEVICE.RESET", "");
ok(1, "Reset in FLT_STOPPING");

# Reset the FGC when in STANDBY

assureMainInOff($d, "SIMULATION");

setPc($d, "SB");
set($d, "DEVICE.RESET", "");
ok(1, "Reset request in ON_STANDBY");

# Try to reset the FGC when in IDLE

setPc($d, "IL");
dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in IDLE failed";

# Try to reset the FGC when in ARMED

set($d, "REF.FUNC.REG_MODE" , "I");
set($d, "REF.TABLE.FUNCTION", "0|500,0.5|700,3.5|700,4|500");
wait_until( sub { get($d,"STATE.PC") eq 'ARMED' } );
dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in ARMED failed";

# Try to reset the FGC when in RUNNING

set($d, "REF.RUN", "");
wait_until( sub { get($d,"STATE.PC") eq 'RUNNING' } );
dies_ok sub { set($d, "DEVICE.RESET", "") }, "Reset request in RUNNING failed";

# EOF