#!/usr/bin/perl

# File:    device.pl
# Purpose: Testing of DEVICE properties

use Test::Utils;

use strict;
use warnings;
use diagnostics;



sub fgcReset
{
    my ($d) = @_;

    set($d, "DEVICE.RESET", "");
    sleep(2);
    assureMain($d);
}



sub testClassAndPlatform
{
    my ($d) = @_;

    my $class_id = get($d, "DEVICE.CLASS_ID");

    ok($class_id =~ m/^(51|53|59|62|63|64)$/i, "Class ID seems valid");

    my %class_nm =
    (
        51 => "FGC2_LHC_PC",
        53 => "FGC2_POPS",
        59 => "FGC2_RF_GEN",
        62 => "FGC3_FP_PC",
        63 => "FGC3_PC_CCLIBS",
        64 => "FGC3_PC_SVC",
    );

    is(get($d, "DEVICE.CLASS_NM"), $class_nm{$class_id}, "Class name matches class ID");

    my %platform_id =
    (
        51 => 50,
        53 => 50,
        59 => 50,
        62 => 60,
        63 => 60,
        64 => 60,
    );

    is(get($d, "DEVICE.PLATFORM_ID"), $platform_id{$class_id}, "Platform ID matches class ID");

    my %platform_nm =
    (
        50 => "FGC2",
        60 => "FGC3",
    );

    is(get($d, "DEVICE.PLATFORM_NM"), $platform_nm{$platform_id{$class_id}}, "Platform name matches platform ID");
}



sub testKnownType
{
    my ($d) = @_;

    assureMain($d);

    # Reset and restore the original device type in case it was changed

    fgcReset($d);

    my $type = get($d, "DEVICE.TYPE");
    my $name = get($d, "DEVICE.NAME");

    # Type should match the first segment of the name (part of the name before the first dot)

    is($type, (split(/\./, $name))[0], "DEVICE.TYPE matches DEVICE.NAME");

    # Change the type. The property value will change, but after reset it should match the name again

    my $fake_type = "RPHK";

    set($d, "DEVICE.TYPE", $fake_type);
    is(get($d, "DEVICE.TYPE"), $fake_type, "DEVICE.TYPE changed to $fake_type");

    fgcReset($d);

    # The type should be recovered

    $type = get($d, "DEVICE.TYPE");
    $name = get($d, "DEVICE.NAME");

    is($type, (split(/\./, $name))[0], "DEVICE.TYPE matches DEVICE.NAME");
}



sub testUnknownType
{
    my ($d, $valid_type, $invalid_type) = @_;

    assureMain($d);

    # Set the type to a type that's not in SysDB.

    set($d, "DEVICE.TYPE", $invalid_type);
    is(get($d, "DEVICE.TYPE"), $invalid_type, "DEVICE.TYPE changed to $invalid_type");

    # After reset, the type should be set to UKNWN

    fgcReset($d);

    is(get($d, "DEVICE.TYPE"), "UKNWN", "DEVICE.TYPE is UKNWN");

    # Set the type to a type that is in SysDB

    set($d, "DEVICE.TYPE", $valid_type);
    is(get($d, "DEVICE.TYPE"), $valid_type, "DEVICE.TYPE changed to $valid_type");

    # After reset, the type should be preserved

    fgcReset($d);

    is(get($d, "DEVICE.TYPE"), $valid_type, "DEVICE.TYPE is still $valid_type");
}



my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $device = $params{device};

assureMain($device);

testClassAndPlatform($device);

my $platform = get($device, "DEVICE.PLATFORM_ID");

my $dev_known_type;      # Device with a known system type embedded in the name
my $dev_unknown_type;    # Device with a unknown system type embedded in the name
my $valid_type;          # Name of type that should be in SysDB
my $invalid_type;        # Name of type that shouldn't be in SysDB

# That's a hack to distinguish for which platform the test should be run
# The device passed as argument will not be used in the tests!

if($platform == 50)
{
    $dev_known_type   = "RFMZ.866.27.DEV";
    $dev_unknown_type = "RPH_.866.23.DEV";
    $valid_type       = "RPHK";
    $invalid_type     = "RPH_";
}
else
{
    $dev_known_type   = "RFNA.866.01.RETH5";
    $dev_unknown_type = "RPH_.866.08.RETH5";
    $valid_type       = "RPAGO";
    $invalid_type     = "RPH_";
}

testKnownType($dev_known_type);
testUnknownType($dev_unknown_type, $valid_type, $invalid_type);

# EOF