#!/usr/bin/perl

# File:    table.pl

use Scalar::Util qw(looks_like_number);

use Test::Utils;

use strict;
use warnings;
use diagnostics;



sub TOLERANCE() { 1e-5 }



sub cmp_float
{
    my ($got, $operator, $expected, $label) = @_;

    $got      = sprintf("%.5E", $got);
    $expected = sprintf("%.5E", $expected);

    if($operator eq "==")
    {
        cmp_ok(abs($got - $expected), "<", TOLERANCE, $label . ", got: $got, expected: $expected");
    }
    else
    {
        die "not implemented!";
    }
}



sub testTable
{
    my ($time_vector, $ref_vector, $result_time_vector, $result_ref_vector, $label) = @_;

    for my $i (0..$#{$time_vector})
    {
        cmp_float($result_time_vector->[$i], '==', $time_vector->[$i], "${label}Time of point $i ok");
        cmp_float($result_ref_vector->[$i],  '==', $ref_vector->[$i],  "${label}Ref of point $i ok");
    }
}



sub prepareTest
{
    my ($d, $time_vector, $ref_vector) = @_;

    my $standby = get($d, "LIMITS.I.MIN[0]");

    for my $ref (@$ref_vector)
    {
        if(looks_like_number($ref))
        {
            $ref += $standby;
        }
    }

    my $function = zipTable($time_vector, $ref_vector);

    setPc($d, "SB");
    setPc($d, "IL");

    return $function;
}



sub testBasic
{
    my ($d, $label) = @_;

    my @time_vector = (0, 1,  2,  3,  4,  5,  6,    7,      8,  9);
    my @ref_vector  = (0, 10, 19, 18, 17, 10, 10.5, 10.516, 15, 20);

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    set($d, "REF.TABLE.FUNCTION", $function);

    is(get($d, "STATE.PC"), "ARMED", "${label}Function armed");

    my $result = get($d, "REF.TABLE.FUNCTION");

    testTable(\@time_vector, \@ref_vector, unzipTable($result), $label);

    set($d, "REF.RUN", "");
    wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

    my $tolerance = 1E-4;
    my $ref_i     = get($d, "REF.I");

    cmp_ok(get($d, "REF.I"), ">", $ref_vector[-1] - $tolerance, "${label}Final reference reached");
    cmp_ok(get($d, "REF.I"), "<", $ref_vector[-1] + $tolerance, "${label}Final reference reached");
}



sub testRecoveryAfterReset
{
    my ($d, $label) = @_;

    # Put a big table in the property, in order to test inter-processor communication

    my @time_vector = (0);
    my @ref_vector  = (0);

    foreach my $i (1..999)
    {
        push @time_vector, $i * 0.1;
        push @ref_vector,  $i * 0.01;
    }

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    set($d, "REF.TABLE.FUNCTION", $function);
    is(get($d, "STATE.PC"), "ARMED", "${label}Function armed");
    setPc($d, "OFF");
    set($d, "DEVICE.RESET", "");
    sleep(2);
    assureMainInOff($d, "SIMULATION");
    ok(1, "Reset");

    # Check that the table has been recovered from the NVS

    my $result = get($d, "REF.TABLE.FUNCTION");
    testTable(\@time_vector, \@ref_vector, unzipTable($result), $label);
}



sub testArmDisarm
{
    my ($d, $label) = @_;

    my @time_vector = (0, 1,  2,  3,  4,  5,  6,    7,      8,  9);
    my @ref_vector  = (0, 10, 19, 18, 17, 10, 10.5, 10.516, 15, 20);

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    setPc($d, "OFF");
    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/bad state/, "${label}Failed to set table in OFF";

    setPc($d, "SB");
    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/bad state/, "${label}Failed to set table in STANDBY";

    setPc($d, "IL");
    set($d, "REF.TABLE.FUNCTION", $function);

    is(get($d, "STATE.PC"), "ARMED", "${label}Table armed");
    is(get($d, "REF.FUNC.TYPE"), "TABLE", "${label}Function type ok");

    throws_ok { set($d, "REF.FUNC.TYPE", "NONE") } qr/bad state/, "${label}Failed to change REF.FUNC.TYPE in ARMED";
    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/bad state/, "${label}Failed to set table in ARMED";

    set($d, "REF.RUN", "");
    wait_until( sub { get($d,"STATE.PC") eq 'RUNNING' } );

    throws_ok { set($d, "REF.FUNC.TYPE", "NONE") } qr/bad state/, "${label}Failed to change REF.FUNC.TYPE in RUNNING";
    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/bad state/, "${label}Failed to set table in RUNNING";

    wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

    setPc($d, "SB");
    setPc($d, "IL");

    # Rearm by setting function type

    set($d, "REF.FUNC.TYPE", "TABLE");
    is(get($d, "STATE.PC"), "ARMED", "${label}Table armed");
    is(get($d, "REF.FUNC.TYPE"), "TABLE", "${label}Function type ok");
}



sub testAbort
{
    my ($d, $label) = @_;

    my @time_vector = (0, 1,  2,  3,  4,  5,  6,    7,      8,  9);
    my @ref_vector  = (0, 10, 19, 18, 17, 10, 10.5, 10.516, 15, 20);

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    set($d, "REF.TABLE.FUNCTION", $function);

    is(get($d, "STATE.PC"), "ARMED", "${label}Table armed");
    is(get($d, "REF.FUNC.TYPE"), "TABLE", "${label}Function type ok");

    set($d, "REF.RUN", "");
    wait_until( sub { get($d,"STATE.PC") eq 'RUNNING' } );

    set($d, "REF.ABORT", "");
    wait_until( sub { get($d,"STATE.PC") eq 'IDLE' } );

    my $ref_i     = get($d, "REF.I");
    my $tolerance = 1E-4;

    ok($ref_i < $ref_vector[-1] - $tolerance || $ref_i > $ref_vector[-1] + $tolerance, "${label}Final reference not reached");
}



sub testInvalidInitialTime
{
    my ($d, $label) = @_;

    # Set time of the first point to non-zero value

    my @time_vector = (1, 1.1, 2,  3,  4,  5,  6,    7,      8,  9);
    my @ref_vector  = (0, 10,  19, 18, 17, 10, 10.5, 10.516, 15, 20);

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    # Arming should fail, but table should be set

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/invalid time/, "${label}Failed to set invalid initial time";

    my $result = get($d, "REF.TABLE.FUNCTION");

    testTable(\@time_vector, \@ref_vector, unzipTable($result), $label);
}



sub testInvalidInitialRef
{
    my ($d, $label) = @_;

    # Set reference of first point to value different from standby level

    my @time_vector = (0,   1,  2,  3,  4,  5,  6,    7,      8,  9);
    my @ref_vector  = (-10, 10, 19, 18, 17, 10, 10.5, 10.516, 15, 20);

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    # Arming should fail, but table should be set

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/ref mismatch/, "${label}Failed to set invalid initial ref";

    my $result = get($d, "REF.TABLE.FUNCTION");

    testTable(\@time_vector, \@ref_vector, unzipTable($result), $label);
}



sub testNegativeTime
{
    # Negative values of time should be caught by parsing function, so table shouldn't be set

    my ($d, $label) = @_;

    my @time_vector = (0, -1,  2,  3,  4,  5,  6,    7,      8,  9);
    my @ref_vector  = (0, 10, 19, 18, 17, 10, 10.5, 10.516, 15, 20);

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    my $old_function = get($d, "REF.TABLE.FUNCTION");

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/out of limits/, "${label}Failed to set negative time";

    testTable(unzipTable(get($d, "REF.TABLE.FUNCTION")), unzipTable($old_function), $label);

    @time_vector = (0, -1, -2, -3, -4, -5, -6, -7, -8, -9);

    $function = prepareTest($d, \@time_vector, \@ref_vector);

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/out of limits/, "${label}Failed to set negative time";

    testTable(unzipTable(get($d, "REF.TABLE.FUNCTION")), unzipTable($old_function), $label);
}



sub testSmallTime
{
    my ($d, $label) = @_;

    my @time_vector = (0, 1E-20, 1E-4, 1E-3, 4,  5,  6,    7,      8,  9);
    my @ref_vector  = (0, 10,    19,   18,   17, 10, 10.5, 10.516, 15, 20);

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/invalid time/, "${label}Failed to set small time value";

    my $result = get($d, "REF.TABLE.FUNCTION");

    print $result."\n";

    $time_vector[1] = 0;

    # There's a bug in FGC2 sprintf function that causes value of 1E-4 to be displayed as 1E-5

    if(get($d, "DEVICE.PLATFORM_ID") eq "50")
    {
        $time_vector[2] = 1E-5;
    }

    testTable(\@time_vector, \@ref_vector, unzipTable($result), $label);
}



sub testHugeTime
{
    # Huge values of time should be caught by parsing function, so table shouldn't be set

    my ($d, $label) = @_;

    my @time_vector = (0, 1E20,  2,  3,  4,  5,  6,    7,      8,  9);
    my @ref_vector  = (0, 10,   19, 18, 17, 10, 10.5, 10.516, 15, 20);

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    my $old_function = get($d, "REF.TABLE.FUNCTION");

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/out of limits/, "${label}Failed to set huge time value";

    testTable(unzipTable(get($d, "REF.TABLE.FUNCTION")), unzipTable($old_function), $label);
}



sub testInvalidFormats
{
    my ($d, $label) = @_;

    my @time_vector = ("", 1,  2,  3,  4,  5,  6,    7,      8,  9);
    my @ref_vector  = (0, 10, 19, 18, 17, 10, 10.5, 10.516, 15, 20);

    my $function = prepareTest($d, \@time_vector, \@ref_vector);

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/bad point/, "${label}Failed to set invalid point";

    @time_vector = (0, 1,  2,  3,  4,  5,  6,    7,      8,  9);
    @ref_vector  = (0, 10, 19, 18, "kkk", 10, 10.5, 10.516, 15, 20);

    $function = prepareTest($d, \@time_vector, \@ref_vector);

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/bad point/, "${label}Failed to set invalid point";

    @time_vector = (0, 1,  2,  3,  4,  5,  6,    7,      8,  9);
    @ref_vector  = (0, 10, 19, 18, "nan", 10, 10.5, 10.516, 15, 20);

    $function = prepareTest($d, \@time_vector, \@ref_vector);

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/bad point/, "${label}Failed to set invalid point";

    @time_vector = (0, 1,  2,  "inf",  4,  5,  6,    7,      8,  9);
    @ref_vector  = (0, 10, 19, 18,    17, 10, 10.5, 10.516, 15, 20);

    $function = prepareTest($d, \@time_vector, \@ref_vector);

    throws_ok { set($d, "REF.TABLE.FUNCTION", $function) } qr/bad point/, "${label}Failed to set invalid point";
}



sub decorateLabel
{
    my $label = shift;

    return "$label: ";
}



my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "SIMULATION");

testBasic             ($d, decorateLabel("Test 1"));
testRecoveryAfterReset($d, decorateLabel("Test 2"));
testArmDisarm         ($d, decorateLabel("Test 3"));
testAbort             ($d, decorateLabel("Test 4"));
testInvalidInitialTime($d, decorateLabel("Test 5"));
testInvalidInitialRef ($d, decorateLabel("Test 6"));
testNegativeTime      ($d, decorateLabel("Test 7"));
testSmallTime         ($d, decorateLabel("Test 8"));
testHugeTime          ($d, decorateLabel("Test 9"));
testInvalidFormats    ($d, decorateLabel("Test 10"));

# EOF
