#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

ok( get($d,"FIELDBUS.WFIP.ERR_ART") == 0, "FIELDBUS.WFIP.ERR_ART == 0");
#ok( get($d,"FIELDBUS.WFIP.RDMISSED") == 0, "FIELDBUS.WFIP.RDMISSED == 0");
ok( get($d,"FIELDBUS.WFIP.MSGSNDTIMOUT") == 0, "FIELDBUS.WFIP.MSGSNDTIMOUT == 0");