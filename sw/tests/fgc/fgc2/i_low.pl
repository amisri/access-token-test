#!/usr/bin/perl

# File:    i_low.pl
# Purpose: Testing of LOW_CURRENT flag in status unlatched, and LIMITS.I.LOW property

use Test::Utils;

use strict;
use warnings;
use diagnostics;



sub testLowCurrentFlagAtRef
{
    my ($d, $ref, $low_current_expected) = @_;

    set($d, "REF", "NOW,$ref");
    wait_until( sub { get($d, "STATE.PC") eq "IDLE" } );

    if($low_current_expected)
    {
        ok(get($d, "STATUS.ST_UNLATCHED") =~ /LOW_CURRENT/, "Low current flag present");
    }
    else
    {
        ok(get($d, "STATUS.ST_UNLATCHED") !~ /LOW_CURRENT/, "Low current flag not present");
    }
}



my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Test main program first

assureMainInOff($d, "SIMULATION");

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

setPc($d, "OF");

my $i_pos            = get($d, "LIMITS.I.POS[0]");
my $i_low            = 0.1 * $i_pos;
my $i_low_hysteresis = 0.09 * $i_pos;

# Set load select to 0 and low current limit to 10 % of pos limit

set($d, "LOAD.SELECT", 0);
set($d, "LIMITS.I.LOW", $i_low);

ok(get($d, "STATUS.ST_UNLATCHED") =~ /LOW_CURRENT/, "Low current flag present");

setPc($d, "SB");
setPc($d, "IL");

testLowCurrentFlagAtRef($d, $i_low_hysteresis, 1);
testLowCurrentFlagAtRef($d, ($i_low_hysteresis + $i_low) * 0.5, 1);
testLowCurrentFlagAtRef($d, $i_low + 1, 0);

setPc($d, "OFF");

# Wait for the measurement to drop down below 10% of pos limit (actual limit is 9% of
# pos limit, because of hysteresis)

wait_until( sub { get($d, "MEAS.I") < 0.09 * $i_pos }, 60, 0.5 );

ok(get($d, "STATUS.ST_UNLATCHED") =~ /LOW_CURRENT/, "Low current flag present");

set($d, "LIMITS.I.LOW", 0.0);
$i_low = get($d, "LIMITS.I.LOW");
cmp_ok($i_low, "==", 0.0, "LIMITS.I.LOW is 0.0");

# LIMITS.I.LOW is set to 0, but internally, the FGC should force the limit to be 1% of LIMITS.I.POS[0]

$i_low            = 0.01 * $i_pos;
$i_low_hysteresis = 0.009 * $i_pos;

# Wait for the measurement to drop down below 1% of pos limit (actual limit is 0.9% of
# pos limit, because of hysteresis)

wait_until( sub { get($d, "MEAS.I") < 0.009 * $i_pos }, 60, 0.5 );

ok(get($d, "STATUS.ST_UNLATCHED") =~ /LOW_CURRENT/, "Low current flag present");

setPc($d, "SB");
setPc($d, "IL");

testLowCurrentFlagAtRef($d, $i_low + 0.3, 0);
testLowCurrentFlagAtRef($d, ($i_low_hysteresis + $i_low) * 0.5, 0);
testLowCurrentFlagAtRef($d, $i_low_hysteresis - 0.3, 1);

# TODO Test negative reference values
# TODO Test other load selectors
# TODO Test cycling

# EOF
