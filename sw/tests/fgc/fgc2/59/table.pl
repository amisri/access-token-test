#!/usr/bin/perl

# File:    table.pl

use Test::Utils;

use strict;
use warnings;
use diagnostics;



my @sub_devices =
(
    "ADTH.866.1.DEV",
    "ADTH.866.2.DEV",
    "ADTH.866.3.DEV",
    "ADTH.866.4.DEV",
    "ADTH.866.5.DEV",
    "ADTH.866.6.DEV",
    "ADTH.866.7.DEV",
    "ADTH.866.8.DEV",
    "ADTV.866.9.DEV",
    "ADTV.866.10.DEV",
    "ADTV.866.11.DEV",
    "ADTV.866.12.DEV",
    "ADTV.866.13.DEV",
    "ADTV.866.14.DEV",
    "ADTV.866.15.DEV",
    "ADTV.866.16.DEV",
);



sub getPreparedChannel
{
    my ($d, $label) = @_;

    my $channel = int(rand(16));

    set($d, "FGCONF.TRIGGER[$channel]", "ENABLED");
    is(get_sub_dev($d, $channel, "FGCONF.TRIGGER"), "ENABLED", "${label}Trigger enabled");

    set($d, "FGRT.CONTROL[$channel]", "0");
    set($d, "FGFUNC.ABORT[$channel]", "");
    set($d, "FGFUNC", "NOW,$channel,0");
    wait_until( sub { get_sub_dev($d, $channel, "FGFUNC.IVAL") == 0 &&
                      get_sub_dev($d, $channel, "FGSTATE.FUNC") eq "IDLE" } );
    ok(1, "${label}Reference zeroed");
    ok(1, "${label}Channel $channel ready");

    return($channel);
}



sub setTable
{
    my ($d, $channel, $time_vector, $ref_vector) = @_;

    my $function = zipTable($time_vector, $ref_vector);

    my $method = int(rand(3));

    # There are three methods to set a sub-dev property:
    # 1) Accessing the property array in the main device
    # 2) Setting the sub-device property on the sub device
    # 3) Setting the sub-device property on the main device by adding a sub-device number on the front

    if($method == 0)
    {
        set($d, "FGFUNC.TABLE.CHAN${channel}.FUNCTION", $function);
    }
    elsif($method == 1)
    {
        my $sd = $sub_devices[$channel];

        set($sd, "FG.FUNC.TABLE.FUNCTION", $function);
    }
    else
    {
        my $subdev_index = $channel + 1;

        set($d, "$subdev_index:FG.FUNC.TABLE.FUNCTION", $function);
    }
}



sub setTableSuccess
{
    my ($d, $channel, $time_vector, $ref_vector, $label) = @_;

    is(get($d, "FGSTATE.FUNC[$channel]"), "IDLE", "${label}Channel is IDLE");

    setTable($d, $channel, $time_vector, $ref_vector);

    is(get($d, "FGSTATE.FUNC[$channel]"), "ARMED", "${label}Channel is ARMED");
}



sub setTableFailure
{
    my ($d, $channel, $time_vector, $ref_vector, $initial_state, $exception, $comment, $label) = @_;

    is(get($d, "FGSTATE.FUNC[$channel]"), $initial_state, "${label}Channel is $initial_state");

    throws_ok { setTable($d, $channel, $time_vector, $ref_vector); } $exception, "${label}$comment";

    is(get($d, "FGSTATE.FUNC[$channel]"), $initial_state, "${label}Channel is still $initial_state");
}



sub testTable
{
    my ($time_vector, $ref_vector, $result_time_vector, $result_ref_vector, $label) = @_;

    for my $i (0..$#{$time_vector})
    {
        cmp_ok($time_vector->[$i], '==', $result_time_vector->[$i], "${label}Time of point $i ok");
        cmp_ok($ref_vector->[$i],  '==', $result_ref_vector->[$i],  "${label}Ref of point $i ok");
    }
}



sub getTable
{
    my ($d, $channel) = @_;

    my $method = int(rand(2));
    my $sd     = $sub_devices[$channel];

    my $result;

    if($method == 0)
    {
        $result = get($d, "FGFUNC.TABLE.CHAN${channel}.FUNCTION");
    }
    else
    {
        $result = get($sd, "FG.FUNC.TABLE.FUNCTION");
    }

    return(unzipTable($result));
}



sub testBasicTable
{
    my ($d, $label) = @_;

    my $channel = getPreparedChannel($d, $label);

    my @time_vector = (0, 1,   2,   2.1, 2.2, 3.75);
    my @ref_vector  = (0, 100, 200, 200, 200, 195.76);

    setTableSuccess($d, $channel, \@time_vector, \@ref_vector, $label);
    my ($result_time, $result_ref) = getTable($d, $channel);
    testTable(\@time_vector, \@ref_vector, $result_time, $result_ref, $label);
}



sub testArmDisarm
{
    my ($d, $label) = @_;

    my $channel = int(rand(16));

    # Try to arm disabled channel

    set($d, "FGCONF.TRIGGER[$channel]", "DISABLED");
    is(get_sub_dev($d, $channel, "FGCONF.TRIGGER"), "DISABLED", "${label}Trigger disabled");
    ok(1, "${label}Channel $channel disabled");

    my @time_vector = (0, 1,   2,   2.1, 2.2, 3.75);
    my @ref_vector  = (0, 100, 200, 200, 200, 195.76);

    my ($result_time1, $result_ref1) = getTable($d, $channel);
    setTableFailure($d, $channel, \@time_vector, \@ref_vector, "OFF", qr/bad state/, "Can't arm disabled channel", $label);
    throws_ok { set($d, "FGFUNC.TYPE[$channel]", "TABLE"); } qr/bad state/, "${label}Can't change function type on disabled channel";
    my ($result_time2, $result_ref2) = getTable($d, $channel);

    testTable($result_time1, $result_ref1, $result_time2, $result_ref2, $label);

    # Arm channel

    $channel = getPreparedChannel($d, $label);

    @time_vector = (0, 1,  1.9, 2.1, 2.2, 4);
    @ref_vector  = (0, 90, 100, 100, 100, 195.76);

    setTableSuccess($d, $channel, \@time_vector, \@ref_vector, $label);
    ($result_time1, $result_ref1) = getTable($d, $channel);
    testTable(\@time_vector, \@ref_vector, $result_time1, $result_ref1, $label);

    # Try to rearm armed channel

    @time_vector = (0, 1,   2,   2.1, 2.2, 3.75);
    @ref_vector  = (0, 100, 200, 200, 200, 195.76);

    ($result_time1, $result_ref1) = getTable($d, $channel);
    setTableFailure($d, $channel, \@time_vector, \@ref_vector, "ARMED", qr/bad state/, "Can't rearm armed channel", $label);
    throws_ok { set($d, "FGFUNC.TYPE[$channel]", "TABLE"); } qr/bad state/, "${label}Can't change function type on armed channel";
    ($result_time2, $result_ref2) = getTable($d, $channel);

    testTable($result_time1, $result_ref1, $result_time2, $result_ref2, $label);

    # Try to arm running channel

    @time_vector = (0, 1,  1.9, 2.1, 2.2, 4);
    @ref_vector  = (0, 90, 100, 100, 100, 195.76);

    ($result_time1, $result_ref1) = getTable($d, $channel);

    set($d, "FGFUNC.RUN[$channel]", "");
    wait_until( sub { get($d, "FGSTATE.FUNC[$channel]") eq 'RUNNING' } );

    setTableFailure($d, $channel, \@time_vector, \@ref_vector, "RUNNING", qr/bad state/, "Can't arm running channel", $label);
    throws_ok { set($d, "FGFUNC.TYPE[$channel]", "TABLE"); } qr/bad state/, "${label}Can't change function type on running channel";
    ($result_time2, $result_ref2) = getTable($d, $channel);

    testTable($result_time1, $result_ref1, $result_time2, $result_ref2, $label);

    wait_until( sub { get($d, "FGSTATE.FUNC[$channel]") eq 'IDLE' } );
}



sub testAbort
{
    my ($d, $label) = @_;

    my $channel = getPreparedChannel($d, $label);

    my @time_vector = (0,   10,   20);
    my @ref_vector  = (000, 100, 200);

    setTableSuccess($d, $channel, \@time_vector, \@ref_vector, $label);
    my ($result_time, $result_ref) = getTable($d, $channel);
    testTable(\@time_vector, \@ref_vector, $result_time, $result_ref, $label);

    set($d, "FGFUNC.RUN[$channel]", "");
    set($d, "FGFUNC.ABORT[$channel]", "");

    wait_until( sub { get($d, "FGSTATE.FUNC[$channel]") eq 'IDLE' } );

    cmp_ok(get_sub_dev($d, $channel, "FGFUNC.IVAL"), '!=', $ref_vector[-1], "${label}Didn't reach final reference after aborting");
}



sub testNoteEnoughPoints
{
    my ($d, $label) = @_;

    my $channel = getPreparedChannel($d, $label);

    my @time_vector = (0);
    my @ref_vector  = (0);

    setTableFailure($d, $channel, \@time_vector, \@ref_vector, "IDLE", qr/bad array len/, "Too few points", $label);
    my ($result_time, $result_ref) = getTable($d, $channel);
    testTable(\@time_vector, \@ref_vector, $result_time, $result_ref, $label);
}



sub testNonZeroInitialTime
{
    my ($d, $label) = @_;

    my $channel = getPreparedChannel($d, $label);

    my @time_vector = (1, 2,   3);
    my @ref_vector  = (0, 100, 200);

    setTableFailure($d, $channel, \@time_vector, \@ref_vector, "IDLE", qr/invalid time/, "Invalid initial time", $label);
    my ($result_time, $result_ref) = getTable($d, $channel);
    testTable(\@time_vector, \@ref_vector, $result_time, $result_ref, $label);
}



sub testMinimalTimeInterval
{
    my ($d, $label) = @_;

    my $channel = getPreparedChannel($d, $label);

    my @time_vector = ();
    my @ref_vector  = ();

    for my $i (0..1000)
    {
        push @time_vector, $i * 0.001;
        push @ref_vector,  $i;
    }

    setTableSuccess($d, $channel, \@time_vector, \@ref_vector, $label);

    $channel = getPreparedChannel($d, $label);

    @time_vector = ();
    @ref_vector  = ();

    for my $i (0..1000)
    {
        push @time_vector, $i * 0.0009;
        push @ref_vector,  $i;
    }

    setTableFailure($d, $channel, \@time_vector, \@ref_vector, "IDLE", qr/invalid time/, "Time interval too short", $label);
}



sub testInvalidInitialRef
{
    my ($d, $label) = @_;

    my $channel = getPreparedChannel($d, $label);

    my @time_vector = (0,   2,   3);
    my @ref_vector  = (100, 100, 200);

    setTableFailure($d, $channel, \@time_vector, \@ref_vector, "IDLE", qr/ref mismatch/, "Initial reference mismatch", $label);
    my ($result_time, $result_ref) = getTable($d, $channel);
    testTable(\@time_vector, \@ref_vector, $result_time, $result_ref, $label);
}



sub testRunDelayAndRunPeriod
{
    my ($d, $label) = @_;

    my $channel = getPreparedChannel($d, $label);

    my @time_vector = (0,   10,   20);
    my @ref_vector  = (000, 100, 200);

    my $run_delay = 10;

    set($d, "FGFUNC.RUN_DELAY[$channel]", "$run_delay");

    setTableSuccess($d, $channel, \@time_vector, \@ref_vector, $label);
    my ($result_time, $result_ref) = getTable($d, $channel);
    testTable(\@time_vector, \@ref_vector, $result_time, $result_ref, $label);

    my $run_period = get($d, "FGFUNC.RUN_PERIOD[$channel]");

    cmp_ok($run_period, '==', $time_vector[-1] + $run_delay, "${label}Run period");

    set($d, "FGFUNC.RUN[$channel]", "");

    sleep(5);

    is(get($d, "FGSTATE.FUNC[$channel]"), "RUNNING", "${label}Table running");
    cmp_ok(get_sub_dev($d, $channel, "FGFUNC.IVAL"), '==', 0, "${label}IVAL == 0");

    sleep(10);

    is(get($d, "FGSTATE.FUNC[$channel]"), "RUNNING", "${label}Table running");
    cmp_ok(get_sub_dev($d, $channel, "FGFUNC.IVAL"), '<', 70, "${label}IVAL < 70");
    cmp_ok(get_sub_dev($d, $channel, "FGFUNC.IVAL"), '>', 30, "${label}IVAL > 30");

    sleep(8);

    is(get($d, "FGSTATE.FUNC[$channel]"), "RUNNING", "${label}Table running");
    cmp_ok(get_sub_dev($d, $channel, "FGFUNC.IVAL"), '<', 170, "${label}IVAL < 170");
    cmp_ok(get_sub_dev($d, $channel, "FGFUNC.IVAL"), '>', 130, "${label}IVAL > 130");

    sleep(5);

    is(get($d, "FGSTATE.FUNC[$channel]"), "IDLE", "${label}Channel IDLE");
    cmp_ok(get_sub_dev($d, $channel, "FGFUNC.IVAL"), '==', 200, "${label}IVAL == 200");
}



sub decorateLabel
{
    my $label = shift;

    return "$label: ";
}



my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "SIMULATION");

lives_ok
{

testBasicTable          ($d, decorateLabel("Test 1"));
testArmDisarm           ($d, decorateLabel("Test 2"));
testAbort               ($d, decorateLabel("Test 3"));
testNoteEnoughPoints    ($d, decorateLabel("Test 4"));
testNonZeroInitialTime  ($d, decorateLabel("Test 5"));
testMinimalTimeInterval ($d, decorateLabel("Test 6"));
testInvalidInitialRef   ($d, decorateLabel("Test 7"));
testRunDelayAndRunPeriod($d, decorateLabel("Test 8"));

};

# EOF
