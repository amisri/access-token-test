#!/usr/bin/perl

# File:    event_groups.pl

use Test::Utils;

use strict;
use warnings;
use diagnostics;



use constant START_EVENT => 2;
use constant ABORT_EVENT => 3;

sub waitForFunc
{
    my ($d, %channels_refs) = @_;

    wait_until( sub
                {
                    my $finished = 1;

                    while (my ($channel, $final_ref) = each %channels_refs)
                    {
                       my $ival  = get($d, "FGFUNC.IVAL[$channel]");
                       my $state = get($d, "FGSTATE.FUNC[$channel]");

                       if(not($ival == $final_ref && $state eq "IDLE"))
                       {
                           $finished = 0;
                       }
                    }

                    return $finished;
                }
              );
}



sub setEventGroup
{
    my ($d, $channel, $group) = @_;

    set($d, "FGFUNC.EVENT_GROUP[$channel]", "$group");
    cmp_ok(get($d, "FGFUNC.EVENT_GROUP[$channel]"), "==", $group, "Event group $group set on channel $channel");
}



sub armPlp
{
    my ($d, $channel, $final_ref) = @_;

    set($d, "FGFUNC.PLP.FINAL[$channel]", $final_ref);
    set($d, "FGFUNC.PLP.ACCELERATION[$channel]", 100);
    set($d, "FGFUNC.PLP.LINEAR_RATE[$channel]", 100);
    set($d, "FGFUNC.TYPE[$channel]", "PLP");
    is(get($d, "FGSTATE.FUNC[$channel]"), "ARMED", "Armed PLP on channel $channel");
}



sub sendEventAndWait
{
    my ($g, $event_type, $event_group, $delay) = @_;

    $delay ||= 1000;

    set($g, "FIELDBUS.EVENT", "$event_type,$event_group,$delay");

    sleep($delay / 1000);
}



sub zeroChannels
{
    my $d        = shift;
    my @channels = @_;

    my %channels_refs;

    for my $channel (@channels)
    {
        $channels_refs{$channel} = 0;

        # Abort any armed functions and go to zero

        set($d, "FGFUNC.ABORT[$channel]", "");
        set($d, "FGFUNC.PLP.FINAL[$channel]", 0);
        set($d, "FGFUNC.PLP.ACCELERATION[$channel]", 100);
        set($d, "FGFUNC.PLP.LINEAR_RATE[$channel]", 100);
        set($d, "FGFUNC.TYPE[$channel]", "PLP");
        set($d, "FGFUNC.RUN[$channel]", 0);
    }

    waitForFunc($d, %channels_refs);
}



sub checkFuncState
{
    my ($d, $channel, $state) = @_;

    is(get($d, "FGSTATE.FUNC[$channel]"), $state, "Function on channel $channel is $state");
}



sub getNonGlobalEventGroup
{
    return(int(rand(255)) + 1);
}



sub getEnabledChannel
{
    my $d       = shift;
    my $channel = int(rand(16));

    set($d, "FGCONF.TRIGGER[$channel]", "ENABLED");
    is(get($d, "FGCONF.TRIGGER[$channel]"), "ENABLED", "Channel $channel enabled");

    return($channel);
}



sub getTwoUniqueEnabledChannels
{
    my $d = shift;

    my $channel1 = getEnabledChannel($d);
    my $channel2;

    do
    {
        $channel2 = getEnabledChannel($d);
    } while($channel2 == $channel1);

    return ($channel1, $channel2);
}


sub testEventGroupInIdle
{
    my $d           = shift;
    my $channel     = getEnabledChannel($d);
    my $event_group = getNonGlobalEventGroup();

    zeroChannels($d, $channel);

    # It shouldn't be possible to set event group for channels without armed function

    checkFuncState($d, $channel, "IDLE");
    set($d, "FGFUNC.EVENT_GROUP[$channel]", "$event_group");
    cmp_ok(get($d, "FGFUNC.EVENT_GROUP[$channel]"), "==", 0, "Non-global event group $event_group couldn't be set");
}



sub testGlobalStartEvent
{
    my ($d, $g)               = @_;
    my ($channel1, $channel2) = getTwoUniqueEnabledChannels($d);
    my $event_group           = getNonGlobalEventGroup();

    my %channels_refs = ( $channel1 => 400,
                          $channel2 => 400 );

    zeroChannels($d, $channel1, $channel2);

    armPlp($d, $channel1, 400);
    armPlp($d, $channel2, 400);

    setEventGroup($d, $channel1, 0);
    setEventGroup($d, $channel2, $event_group);

    sendEventAndWait($g, START_EVENT, 0);
    waitForFunc($d, %channels_refs);

    ok(1, "Global start event");
}



sub testNonGlobalStartEvent
{
    my ($d, $g)               = @_;
    my ($channel1, $channel2) = getTwoUniqueEnabledChannels($d);
    my $event_group           = getNonGlobalEventGroup();

    my %channels_refs = ( $channel2 => -400 );

    zeroChannels($d, $channel1, $channel2);

    armPlp($d, $channel1, -400);
    armPlp($d, $channel2, -400);

    setEventGroup($d, $channel1, 0);
    setEventGroup($d, $channel2, $event_group);

    sendEventAndWait($g, START_EVENT, $event_group);

    waitForFunc($d, %channels_refs);
    checkFuncState($d, $channel1, "ARMED");

    ok(1, "Non-global start event");
}



sub testGlobalAbortEventInArmed
{
    my ($d, $g)               = @_;
    my ($channel1, $channel2) = getTwoUniqueEnabledChannels($d);
    my $event_group           = getNonGlobalEventGroup();

    zeroChannels($d, $channel1, $channel2);

    armPlp($d, $channel1, -400);
    armPlp($d, $channel2, -400);

    setEventGroup($d, $channel1, 0);
    setEventGroup($d, $channel2, $event_group);

    sendEventAndWait($g, ABORT_EVENT, 0);

    checkFuncState($d, $channel1, "IDLE");
    checkFuncState($d, $channel2, "IDLE");

    ok(1, "Global abort event in armed");
}



sub testNonGlobalAbortEventInArmed
{
    my ($d, $g)               = @_;
    my ($channel1, $channel2) = getTwoUniqueEnabledChannels($d);
    my $event_group           = getNonGlobalEventGroup();

    zeroChannels($d, $channel1, $channel2);

    armPlp($d, $channel1, 400);
    armPlp($d, $channel2, 400);

    setEventGroup($d, $channel1, 0);
    setEventGroup($d, $channel2, $event_group);

    sendEventAndWait($g, ABORT_EVENT, $event_group);

    checkFuncState($d, $channel1, "ARMED");
    checkFuncState($d, $channel2, "IDLE");

    ok(1, "Non-global abort event in armed");
}



sub testGlobalAbortEventInRunning
{
    my ($d, $g)               = @_;
    my ($channel1, $channel2) = getTwoUniqueEnabledChannels($d);
    my $event_group           = getNonGlobalEventGroup();

    # Zero the channels

    zeroChannels($d, $channel1, $channel2);

    armPlp($d, $channel1, -400);
    armPlp($d, $channel2, -400);

    setEventGroup($d, $channel1, 0);
    setEventGroup($d, $channel2, $event_group);

    sendEventAndWait($g, START_EVENT, 0);
    sendEventAndWait($g, ABORT_EVENT, 0);

    checkFuncState($d, $channel1, "ABORTING");
    checkFuncState($d, $channel2, "ABORTING");

    sleep(10);

    checkFuncState($d, $channel1, "IDLE");
    checkFuncState($d, $channel2, "IDLE");

    ok(1, "Global abort event in running");
}



sub testNonGlobalAbortEventInRunning
{
    my ($d, $g)               = @_;
    my ($channel1, $channel2) = getTwoUniqueEnabledChannels($d);
    my $event_group           = getNonGlobalEventGroup();

    my %channels_refs = ( $channel1 => 400 );

    zeroChannels($d, $channel1, $channel2);

    armPlp($d, $channel1, 400);
    armPlp($d, $channel2, 400);

    setEventGroup($d, $channel1, 0);
    setEventGroup($d, $channel2, $event_group);

    sendEventAndWait($g, START_EVENT, 0);
    sendEventAndWait($g, ABORT_EVENT, $event_group);

    checkFuncState($d, $channel1, "RUNNING");
    checkFuncState($d, $channel2, "ABORTING");

    waitForFunc($d, %channels_refs);
    wait_until( sub { get($d, "FGSTATE.FUNC[$channel2]") eq "IDLE" } );
    checkFuncState($d, $channel2, "IDLE");

    ok(1, "Non-global abort event in running");
}



my %params = (device=>undef, config=>'', protocol=>'CMW');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "SIMULATION");

my $g = get($d, "DEVICE.HOSTNAME");

lives_ok
{

testEventGroupInIdle            ($d);
testGlobalStartEvent            ($d, $g);
testNonGlobalStartEvent         ($d, $g);
testGlobalAbortEventInArmed     ($d, $g);
testNonGlobalAbortEventInArmed  ($d, $g);
testGlobalAbortEventInRunning   ($d, $g);
testNonGlobalAbortEventInRunning($d, $g);

};

# EOF
