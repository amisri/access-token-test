#!/usr/bin/perl

# File:    limit_user_max.pl
# Purpose: FGSTATUS.LIMIT.USER.MAX property testing

use Test::Utils;

use strict;
use warnings;
use diagnostics;



sub runToRef
{
    my ($d, $channel, $final_ref) = @_;

    set($d, "FGFUNC.PLP.FINAL[$channel]", $final_ref);
    set($d, "FGFUNC.PLP.ACCELERATION[$channel]", 1000);
    set($d, "FGFUNC.PLP.LINEAR_RATE[$channel]", 1000);
    set($d, "FGFUNC.TYPE[$channel]", "PLP");
    set($d, "FGFUNC.RUN[$channel]", 0);
}



sub prepareChannel
{
    my ($d, $channel, $label) = @_;

    set($d, "FGCONF.TRIGGER[$channel]", "ENABLED");
    is(get_sub_dev($d, $channel, "FGCONF.TRIGGER"), "ENABLED", "$label: Trigger enabled");

    set($d, "FGRT.CONTROL[$channel]", "0");
    set($d, "FGFUNC.ABORT[$channel]", "");
    runToRef($d, $channel, 0);
    wait_until( sub { get_sub_dev($d, $channel, "FGFUNC.IVAL") == 0 &&
                      get_sub_dev($d, $channel, "FGSTATE.FUNC") eq "IDLE" } );
    ok(1, "$label: Reference zeroed");
}



sub setUserLimit
{
    my ($d, $channel, $limit_type, $value, $label) = @_;

    set($d, "FGCONF.LIMIT.USER.${limit_type}[$channel]", $value);
    cmp_ok(get_sub_dev($d, $channel, "FGCONF.LIMIT.USER.$limit_type"), "==", $value, "$label: User limit set to $value");
}



sub setRateLimit
{
    my ($d, $channel, $limit_type, $value, $label) = @_;

    set($d, "FGCONF.LIMIT.RATE.${limit_type}[$channel]", $value);
    cmp_ok(get_sub_dev($d, $channel, "FGCONF.LIMIT.RATE.$limit_type"), "==", $value, "$label: Rate limit set to $value");
}



sub setRtLimit
{
    my ($d, $channel, $limit_type, $value, $label) = @_;

    set($d, "FGCONF.LIMIT.RT.${limit_type}[$channel]", $value);
    cmp_ok(get($d, "FGCONF.LIMIT.RT.${limit_type}[$channel]"), "==", $value, "$label: RT limit set to $value");
}



sub armPlp
{
    my ($d, $channel, $final_ref, $label) = @_;

    set($d, "FGFUNC.PLP.FINAL[$channel]", $final_ref);
    set($d, "FGFUNC.PLP.ACCELERATION[$channel]", 10);
    set($d, "FGFUNC.PLP.LINEAR_RATE[$channel]", 25);
    set($d, "FGFUNC.TYPE[$channel]", "PLP");
    is(get($d, "FGSTATE.FUNC[$channel]"), "ARMED", "$label: PLP armed");
}



sub testFloat
{
    my ($value, $lower_bound, $upper_bound, $label) = @_;

    cmp_ok($value, ">=", $lower_bound, "$label");
    cmp_ok($value, "<=", $upper_bound, "$label");
}



sub setRtRef
{
    my ($d, $g, $channel, $value, $label) = @_;

    set($g, "FGCD.RT.REF[0]", $value);
    set($d, "FGRT.CONTROL[$channel]", "1");
    cmp_ok(get($d, "FGRT.WVAL[1]"), "==", $value, "$label: wval = RT ref ($value)");
}



sub testFgStatus
{
    my ($d, $rt_pos, $rt_neg, $chan_max, $chan_min, $rate_pos, $rate_neg, $label) = @_;

    cmp_ok(get($d, "FGSTATUS.LIMIT.RT.POS"),   "==", $rt_pos,   "$label: RT pos limit mask = $rt_pos");
    cmp_ok(get($d, "FGSTATUS.LIMIT.RT.NEG"),   "==", $rt_neg,   "$label: RT neg limit mask = $rt_neg");
    cmp_ok(get($d, "FGSTATUS.LIMIT.CHAN.MAX"), "==", $chan_max, "$label: channel max limit mask = $chan_max");
    cmp_ok(get($d, "FGSTATUS.LIMIT.CHAN.MIN"), "==", $chan_min, "$label: channel min limit mask = $chan_min");
    cmp_ok(get($d, "FGSTATUS.LIMIT.RATE.POS"), "==", $rate_pos, "$label: rate pos limit mask = $rate_pos");
    cmp_ok(get($d, "FGSTATUS.LIMIT.RATE.NEG"), "==", $rate_neg, "$label: rate neg limit mask = $rate_neg");
}



sub testUserLimit
{
    my ($d, $limit_type, $label) = @_;

    my $user_limit1  = ($limit_type eq "MIN") ? -100 : 100;
    my $user_limit2  = ($limit_type eq "MIN") ?  -60 : 60;
    my $channel      = int(rand(16));
    my $channel_mask = (1 << $channel);

    $label .= " ($channel)";

    syncFgc($d);
    prepareChannel($d, $channel, $label);

    # Set user limit to +-100

    setUserLimit($d, $channel, $limit_type, $user_limit1, $label);

    # Arm a PLP and set the limit to a value that is above/below the final ref

    armPlp($d, $channel, $user_limit1, $label);
    setUserLimit($d, $channel, $limit_type, $user_limit2, $label);

    # Run the function

    testFgStatus($d, 0, 0, 0, 0, 0, 0, $label);

    set($d, "FGFUNC.RUN[$channel]", "");
    wait_until( sub { get($d, "FGSTATUS.FUNC.RUNNING") eq 0 } );

    # Ival should be clipped at +-60 (with some margin for rounding errors)

    my $ival      = get_sub_dev($d, $channel, "FGFUNC.IVAL");
    my $tolerance = 1;

    if($limit_type eq "MAX")
    {
        testFloat($ival,
                  $user_limit2,
                  $user_limit2 + $tolerance,
                  "$label: ival ($ival) ~ user limit ($user_limit2)");

        testFgStatus($d, 0, 0, $channel_mask, 0, 0, 0, $label);
    }
    else
    {
        testFloat($ival,
                  $user_limit2 - $tolerance,
                  $user_limit2,
                  "$label: ival ($ival) ~ user limit ($user_limit2)");

        testFgStatus($d, 0, 0, 0, $channel_mask, 0, 0, $label);
    }

    # With the limit set to +-60 you should not be able to arm a function which final reference value exceeding it

    my $final_ref = 2 * $user_limit1;

    throws_ok sub { armPlp($d, $channel, $final_ref) }, qr/out of limits/, "$label: Failed to arm PLP with final ref ($final_ref) out of limits";

    ok(1, "$label: FGCONF.LIMIT.USER.${limit_type}");
}



sub testUserLimits
{
    my ($d, $label) = @_;
    my $channel     = int(rand(16));
    my $min_limit   = 50;
    my $max_limit   = 100;

    $label .= " ($channel)";

    syncFgc($d);
    prepareChannel($d, $channel, $label);

    cmp_ok(get($d, "FGFUNC.FVAL[$channel]"), "==", 0, "$label: Fval = 0");
    cmp_ok(get($d, "FGFUNC.CVAL[$channel]"), "==", 0, "$label: Cval = 0");
    cmp_ok(get($d, "FGFUNC.IVAL[$channel]"), "==", 0, "$label: Ival = 0");

    # Set both min and max user limits to positive values such that min < max

    setUserLimit($d, $channel, "MIN", $min_limit, $label);
    setUserLimit($d, $channel, "MAX", $max_limit, $label);

    # The reference value should jump from 0 to min

    cmp_ok(get($d, "FGFUNC.FVAL[$channel]"), "==", $min_limit, "$label: Fval = min channel limit ($min_limit)");
    cmp_ok(get($d, "FGFUNC.CVAL[$channel]"), "==", $min_limit, "$label: Cval = min channel limit ($min_limit)");
    cmp_ok(get($d, "FGFUNC.IVAL[$channel]"), "==", $min_limit, "$label: Ival = min channel limit ($min_limit)");

    # You should be able to arm functions with final ref in [min, max] range only

    armPlp($d, $channel, $min_limit, $label);
    set($d, "FGFUNC.ABORT[$channel]", "");
    armPlp($d, $channel, $max_limit, $label);
    set($d, "FGFUNC.ABORT[$channel]", "");
    armPlp($d, $channel, ($min_limit + $max_limit) / 2, $label);
    set($d, "FGFUNC.ABORT[$channel]", "");
    throws_ok( sub { armPlp($d, $channel, $min_limit + $max_limit) },
               qr/out of limits/,
               "$label: Failed to arm PLP with final ref ($min_limit + $max_limit) out of max channel limit ($max_limit)");
    throws_ok( sub { armPlp($d, $channel, $min_limit - $max_limit) },
               qr/out of limits/,
               "$label: Failed to arm PLP with final ref ($min_limit - $max_limit) out of min channel limit ($min_limit)");

    # Make max limit < min limit
    # Such setting is allowed, but it doesn't make much sense, as you won't be able to arm any function

    $min_limit = -$min_limit;
    $max_limit = -$max_limit;

    setUserLimit($d, $channel, "MIN", $min_limit, $label);
    setUserLimit($d, $channel, "MAX", $max_limit, $label);

    throws_ok( sub { armPlp($d, $channel, $min_limit) },
               qr/out of limits/,
               "$label: Failed to arm PLP with final ref ($min_limit) out of limits");
    throws_ok( sub { armPlp($d, $channel, $max_limit) },
               qr/out of limits/,
               "$label: Failed to arm PLP with final ref ($max_limit) out of limits");
    throws_ok( sub { armPlp($d, $channel, ($min_limit + $max_limit) / 2) },
               qr/out of limits/,
               "$label: Failed to arm PLP with final ref (($min_limit + $max_limit) / 2) out of limits");
    throws_ok( sub { armPlp($d, $channel, $min_limit + $max_limit) },
               qr/out of limits/,
               "$label: Failed to arm PLP with final ref ($min_limit + $max_limit) out of limits");
    throws_ok( sub { armPlp($d, $channel, $min_limit - $max_limit) },
               qr/out of limits/,
               "$label: Failed to arm PLP with final ref ($min_limit - $max_limit) out of limits");

    # Set both min and max user limits to negative values such that min < max

    $min_limit = -100;
    $max_limit = -50;

    setUserLimit($d, $channel, "MIN", $min_limit, $label);
    setUserLimit($d, $channel, "MAX", $max_limit, $label);

    # Rerefence values should jump to min

    cmp_ok(get($d, "FGFUNC.FVAL[$channel]"), "==", $min_limit, "$label: Fval = min channel limit ($min_limit)");
    cmp_ok(get($d, "FGFUNC.CVAL[$channel]"), "==", $min_limit, "$label: Cval = min channel limit ($min_limit)");
    cmp_ok(get($d, "FGFUNC.IVAL[$channel]"), "==", $min_limit, "$label: Ival = min channel limit ($min_limit)");

    # It should be possible to arm function in [min, max] range

    armPlp($d, $channel, $min_limit, $label);
    set($d, "FGFUNC.ABORT[$channel]", "");
    armPlp($d, $channel, $max_limit, $label);
    set($d, "FGFUNC.ABORT[$channel]", "");
    armPlp($d, $channel, ($min_limit + $max_limit) / 2, $label);
    set($d, "FGFUNC.ABORT[$channel]", "");
    throws_ok( sub { armPlp($d, $channel, $max_limit + $min_limit) },
               qr/out of limits/,
               "$label: Failed to arm PLP with final ref ($max_limit + $min_limit) out of min channel limit ($min_limit)");
    throws_ok( sub { armPlp($d, $channel, $max_limit - $min_limit) },
               qr/out of limits/,
               "$label: Failed to arm PLP with final ref ($max_limit - $min_limit) out of max channel limit ($max_limit)");
}



sub testRtLimit
{
    my ($d, $g, $limit_type, $label) = @_;

    my $channel      = int(rand(16));
    my $channel_mask = (1 << $channel);
    my $limit1       = ($limit_type eq "NEG") ? -100 : 100;
    my $limit2       = ($limit_type eq "NEG") ?  -50 : 50;

    $label .= " ($channel)";

    syncFgc($d);
    prepareChannel($d, $channel, $label);

    # Set RT ref and RT limit to +-100 on a zeroed channel
    # The reference should jump to the RT value

    setRtLimit($d, $channel, $limit_type, $limit1, $label);
    setRtRef($d, $g, $channel, $limit1, $label);
    cmp_ok(get($d, "FGFUNC.IVAL[$channel]"), "==", $limit1, "$label: ival = RT limit ($limit1)");
    testFgStatus($d, 0, 0, 0, 0, 0, 0, $label);

    # Decrease the limit to +-50
    # Wval should stay the same as before (+-100), but the reference should be clipped by the RT limit

    setRtLimit($d, $channel, $limit_type, $limit2, $label);

    cmp_ok(get($d, "FGRT.WVAL[1]"), "==", $limit1, "$label: wval = RT reference before limiting ($limit1)");
    cmp_ok(get($d, "FGFUNC.IVAL[$channel]"), "==", $limit2, "$label: ival = RT limit ($limit2)");

    if($limit_type eq "NEG")
    {
        testFgStatus($d, 0, $channel_mask, 0, 0, 0, 0, $label);
    }
    else
    {
        testFgStatus($d, $channel_mask, 0, 0, 0, 0, 0, $label);
    }

    ok(1, "$label: FGCONF.LIMIT.RT.${limit_type}");
}



sub testRateLimit
{
    my ($d, $limit_type, $label) = @_;

    my $channel      = int(rand(16));
    my $channel_mask = (1 << $channel);

    $label .= " ($channel)";

    my $limit      = ($limit_type eq "POS") ?  10 : -10;
    my $final_ref1 = ($limit_type eq "POS") ? 200 : -200;
    my $final_ref2 = ($limit_type eq "POS") ? 400 : -400;

    syncFgc($d);
    prepareChannel($d, $channel, $label);

    # Arm a PLP to +-200

    armPlp($d, $channel, $final_ref1, $label);

    # Set rate limit to +-10

    setRateLimit($d, $channel, $limit_type, $limit, $label);

    testFgStatus($d, 0, 0, 0, 0, 0, 0, $label);

    # Run the PLP, its rate should be limited

    set($d, "FGFUNC.RUN[$channel]", "");
    sleep(2);
    if($limit_type eq "NEG")
    {
        testFgStatus($d, 0, 0, 0, 0, 0, $channel_mask, $label);
    }
    else
    {
        testFgStatus($d, 0, 0, 0, 0, $channel_mask, 0, $label);
    }
    wait_until( sub { get_sub_dev($d, $channel, "FGFUNC.IVAL") == $final_ref1 &&
                      get_sub_dev($d, $channel, "FGSTATE.FUNC") eq "IDLE" } );

    testFgStatus($d, 0, 0, 0, 0, 0, 0, $label);

    # You shouldn't be able to arm anything with rate out of limits

    throws_ok sub { armPlp($d, $channel, $final_ref2) }, qr/out of rate limits/, "$label: Failed to arm PLP with final ref ($final_ref2) out of limits";

    ok(1, "$label: FGCONF.LIMIT.RATE.${limit_type}");
}



sub testChanLimit
{
    my ($d, $limit_type, $label) = @_;

    my $channel    = int(rand(16));
    my $offset     = get($d, "FGCONF.OFFSET[$channel]");
    my $gain       = get($d, "FGCONF.GAIN[$channel]");
    my $user_limit = ($limit_type eq "MAX") ? 100000 : -100000;
    my $cval_max   = $offset;
    $cval_max     += ($limit_type eq "MAX") ? (32767.0 / $gain) : (-32768.0 * $gain);

    $label .= " ($channel)";

    syncFgc($d);
    prepareChannel($d, $channel, $label);

    # Set user limit that is greater than Cval max
    # Channel limit should equal Cval max in that case

    setUserLimit($d, $channel, $limit_type, $user_limit, $label);

    my $chan_limit = get($d, "FGCONF.LIMIT.CHAN.${limit_type}[$channel]");

    if($limit_type eq "MAX")
    {
        testFloat($chan_limit,
                  $cval_max,
                  $cval_max + $gain,
                  "$label: Channel limit ($chan_limit) ~ gain limit ($cval_max)");
    }
    else
    {
        testFloat($chan_limit,
                  $cval_max - $gain,
                  $cval_max,
                  "$label: Channel limit ($chan_limit) ~ gain limit ($cval_max)");
    }

    armPlp($d, $channel, $cval_max, $label);
    set($d, "FGFUNC.ABORT[$channel]", "");
    throws_ok sub { armPlp($d, $channel, $user_limit) }, qr/out of limits/, "$label: Failed to arm PLP with final ref ($user_limit) out of limits";

    # Set user limit that is less than Cval max
    # Channel limit should equal user limit in that case

    $user_limit = ($limit_type eq "MAX") ? 100 : -100;

    setUserLimit($d, $channel, $limit_type, $user_limit, $label);

    $chan_limit = get($d, "FGCONF.LIMIT.CHAN.${limit_type}[$channel]");

    if($limit_type eq "MAX")
    {
        testFloat($chan_limit,
                  $user_limit,
                  $user_limit + $gain,
                  "$label: Channel limit ($chan_limit) ~ user limit ($user_limit)");
    }
    else
    {
        testFloat($chan_limit,
                  $user_limit - $gain,
                  $user_limit,
                  "$label: Channel limit ($chan_limit) ~ user limit ($user_limit)");
    }

    armPlp($d, $channel, $user_limit, $label);
    set($d, "FGFUNC.ABORT[$channel]", "");
    throws_ok sub { armPlp($d, $channel, $cval_max) }, qr/out of limits/, "$label: Failed to arm PLP with final ref ($cval_max) out of limits";

    ok(1, "$label: FGCONF.LIMIT.CHAN.${limit_type}");
}



sub testChanLimitAndRtRef
{
    my ($d, $g, $limit_type, $label) = @_;

    my $channel      = int(rand(16));
    my $channel_mask = (1 << $channel);
    my $gain         = get($d, "FGCONF.GAIN[$channel]");
    my $rt_value     = ($limit_type eq "MAX") ? 50 : -50;
    my $user_limit   = ($limit_type eq "MAX") ? 100 : -100;

    $label .= " ($channel)";

    syncFgc($d);
    prepareChannel($d, $channel, $label);

    # Set channel limit to +-100 and RT reference to +-50

    setUserLimit($d, $channel, $limit_type, $user_limit, $label);
    setRtRef($d, $g, $channel, $rt_value, $label);

    testFgStatus($d, 0, 0, 0, 0, 0, 0, $label);

    # Run a PLP to +-100

    armPlp($d, $channel, $user_limit, $label);
    set($d, "FGFUNC.RUN[$channel]", "");

    if($limit_type eq "MAX")
    {
        wait_until( sub { get_sub_dev($d, $channel, "FGFUNC.IVAL") >= $user_limit &&
                          get_sub_dev($d, $channel, "FGFUNC.IVAL") <= $user_limit + $gain &&
                          get_sub_dev($d, $channel, "FGSTATE.FUNC") eq "IDLE" } );

        # Fval and Ival should be 100, Sval should be 150

        testFloat(get($d, "FGFUNC.IVAL[$channel]"),
                  $user_limit,
                  $user_limit + $gain,
                  "$label: ival ~ user limit ($user_limit)");

        testFloat(get($d, "FGFUNC.FVAL[$channel]"),
                  $user_limit,
                  $user_limit + $gain,
                  "$label: fval ~ user limit ($user_limit)");

        testFloat(get($d, "FGFUNC.SVAL[$channel]"),
                  $user_limit + $rt_value,
                  $user_limit + $rt_value + $gain,
                  "$label: sval ~ user limit + RT ref ($user_limit + $rt_value)");

        testFgStatus($d, 0, 0, $channel_mask, 0, 0, 0, $label);
    }
    else
    {
        wait_until( sub { get_sub_dev($d, $channel, "FGFUNC.IVAL") <= $user_limit &&
                          get_sub_dev($d, $channel, "FGFUNC.IVAL") >= $user_limit - $gain &&
                          get_sub_dev($d, $channel, "FGSTATE.FUNC") eq "IDLE" } );

        # Fval and Ival should be -100, Sval should be -150

        testFloat(get($d, "FGFUNC.IVAL[$channel]"),
                  $user_limit - $gain,
                  $user_limit,
                  "$label: ival ~ user limit ($user_limit)");

        testFloat(get($d, "FGFUNC.FVAL[$channel]"),
                  $user_limit - $gain,
                  $user_limit,
                  "$label: fval ~ user limit ($user_limit)");

        testFloat(get($d, "FGFUNC.SVAL[$channel]"),
                  $user_limit + $rt_value - $gain,
                  $user_limit + $rt_value,
                  "$label: sval ~ user limit + RT ref ($user_limit + $rt_value)");

        testFgStatus($d, 0, 0, 0, $channel_mask, 0, 0, $label);
    }

    my $final_ref = $user_limit + 0.5 * $rt_value;

    throws_ok sub { armPlp($d, $channel, $final_ref) }, qr/out of limits/, "$label: Failed to arm PLP with final ref ($final_ref) out of limits";

    $final_ref = $user_limit + $rt_value;

    throws_ok sub { armPlp($d, $channel, $final_ref) }, qr/out of limits/, "$label: Failed to arm PLP with final ref ($final_ref) out of limits";

    ok(1, "$label: FGCONF.LIMIT.CHAN.${limit_type} with RT REF");
}



my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Set proper STATE.OP and reset the limit

assureMainInOff($d, "SIMULATION");

my $g = get($d, "DEVICE.HOSTNAME");

lives_ok
{

testUserLimit($d, "MIN", "Test 1");
testUserLimit($d, "MAX", "Test 2");

testUserLimits($d, "Test 3");

testRtLimit($d, $g, "NEG", "Test 4");
testRtLimit($d, $g, "POS", "Test 5");

testRateLimit($d, "POS", "Test 6");
testRateLimit($d, "NEG", "Test 7");

testChanLimit($d, "MIN", "Test 8");
testChanLimit($d, "MAX", "Test 9");

testChanLimitAndRtRef($d, $g, "MIN", "Test 10");
testChanLimitAndRtRef($d, $g, "MAX", "Test 11");

};

# EOF
