#!/usr/bin/perl

# File:    nack_and_not_ack.pl
# Purpose: FGSTATUS.ACK, FGSTATUS.NACK and FGSTATUS.NOT_ACK masks testing

use Test::Utils;

use feature "state";

use strict;
use warnings;
use diagnostics;



sub testFgStatus
{
    my ($d, $ack_bitmask, $nack_bitmask, $not_ack_bitmask) = @_;

    state $test_no = 0;

    for my $i (1..5)
    {
        cmp_ok(get($d, "FGSTATUS.ACK"),     "==", $ack_bitmask,     "$test_no ACK check");
        cmp_ok(get($d, "FGSTATUS.NACK"),    "==", $nack_bitmask,    "$test_no NACK check");
        cmp_ok(get($d, "FGSTATUS.NOT_ACK"), "==", $not_ack_bitmask, "$test_no NOT_ACK check");

        $test_no++;
    }
}

sub setTrigger
{
    my ($d, $channel, $value) = @_;

    set($d, "FGCONF.TRIGGER[$channel]", $value);
    is(get($d, "FGCONF.TRIGGER[$channel]"), $value, "Trigger $channel $value");
}

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Start in normal mode to test NACK and NOT_ACK

assureMainInOff($d, "NORMAL");

# Wait for NOT_ACK status to stabilise

sleep(20);

my $channel1 = int(rand(16));
my $channel2 = int(rand(16));

my $channel_mask  = (1 << $channel1) | (1 << $channel2);
my $channel2_mask = ($channel1 == $channel2) ? 0 : (1 << $channel2);

# Enable both channels

setTrigger($d, $channel1, "ENABLED");
setTrigger($d, $channel2, "ENABLED");

testFgStatus($d, 0, $channel_mask, $channel_mask);

# Disable channel 1
# NACK should change immediately, NOT_ACK should change around 12s later

setTrigger($d, $channel1, "DISABLED");
testFgStatus($d, 0, $channel2_mask, $channel_mask);
wait_until( sub { get($d, "FGSTATUS.NOT_ACK") == $channel2_mask }, 20);

# Disable channel 2
# NACK should change immediately, NOT_ACK should change around 12s later

setTrigger($d, $channel2, "DISABLED");
testFgStatus($d, 0, 0, $channel2_mask);
wait_until( sub { get($d, "FGSTATUS.NOT_ACK") == 0 }, 20);

# Switch to simulation to test ACK

assureMainInOff($d, "SIMULATION");

# Enable both channels

setTrigger($d, $channel1, "ENABLED");
setTrigger($d, $channel2, "ENABLED");

testFgStatus($d, $channel_mask, 0, 0);

# EOF
