#!/usr/bin/perl

# File:    sub_dev_props.pl
# Purpose: Test of setting and getting sub-device properties
#
# There are four types of sub-dev properties that need to be tested:
# 1) MCU config
# 2) MCU non-config
# 3) DSP config
# 4) DSP non-config
#
# The file test one of each type by setting and getting middle and boundary indexes. Three methods
# of setting and getting a property are used:
# 1) Accessing the property array in the main device
# 2) Accessing the sub-device property on the sub device
# 3) Accessing the sub-device property on the main device by adding a sub-device number on the front
#
# TODO:
# - Uncomment the third method in testSubDevProperty when it's fixed

use Test::Utils;

use strict;
use warnings;
use diagnostics;



my @sub_devices =
(
    "ADTH.866.1.DEV",
    "ADTH.866.2.DEV",
    "ADTH.866.3.DEV",
    "ADTH.866.4.DEV",
    "ADTH.866.5.DEV",
    "ADTH.866.6.DEV",
    "ADTH.866.7.DEV",
    "ADTH.866.8.DEV",
    "ADTV.866.9.DEV",
    "ADTV.866.10.DEV",
    "ADTV.866.11.DEV",
    "ADTV.866.12.DEV",
    "ADTV.866.13.DEV",
    "ADTV.866.14.DEV",
    "ADTV.866.15.DEV",
    "ADTV.866.16.DEV",
);



sub runToRef
{
    my ($d, $channel, $final_ref, $run) = @_;

    set($d, "FGFUNC.PLP.FINAL[$channel]", $final_ref);
    set($d, "FGFUNC.PLP.ACCELERATION[$channel]", 1000);
    set($d, "FGFUNC.PLP.LINEAR_RATE[$channel]", 1000);
    set($d, "FGFUNC.TYPE[$channel]", "PLP");

    if(defined $run)
    {
        set($d, "FGFUNC.RUN[$channel]", 0);
    }
}



sub testSubDevProperty
{
    my ($d, $channel, $property, $operator, $value) = @_;

    # Create a sub dev property name by inserting a dot (.) after two first characters (FG)

    my $subdev_property = $property;
    substr($subdev_property, 2, 0) = '.';

    my $sd = $sub_devices[$channel];
    #my $subdev_index = $channel + 1;

    # Use three different methods to test the property value

    cmp_ok(get($d,  "${property}[${channel}]"), $operator, $value, "Property ${property}[${channel}] is $value");
    cmp_ok(get($sd, $subdev_property),          $operator, $value, "Property $subdev_property is $value");
    #cmp_ok(get($d,  "$subdev_index:$subdev_property"), $operator, $value, "Property $subdev_index:$subdev_property is $value");
}



sub setSubDevProperty
{
    my ($d, $channel, $property, $value, $method) = @_;

    # Create a sub dev property name by inserting a dot (.) after two first characters (FG)

    my $subdev_property = $property;
    substr($subdev_property, 2, 0) = '.';

    # There are three methods to set a sub-dev property:
    # 1) Accessing the property array in the main device
    # 2) Setting the sub-device property on the sub device
    # 3) Setting the sub-device property on the main device by adding a sub-device number on the front

    if($method == 1)
    {
        set($d, "${property}[${channel}]", $value);
    }
    elsif($method == 2)
    {
        my $sd = $sub_devices[$channel];

        set($sd, $subdev_property, $value);
    }
    else
    {
        my $subdev_index = $channel + 1;

        set($d, "$subdev_index:$subdev_property", $value);
    }
}



sub setTrigger
{
    my ($d, $channel, $value, $method) = @_;

    setSubDevProperty($d, $channel, "FGCONF.TRIGGER", $value, $method);
}



sub testTrigger
{
    my ($d, $channel, $value) = @_;

    testSubDevProperty($d, $channel, "FGCONF.TRIGGER", "eq", $value);
}



sub setRunDelay
{
    my ($d, $channel, $value, $method) = @_;

    setSubDevProperty($d, $channel, "FGFUNC.RUN_DELAY", $value, $method);
}



sub testRunDelay
{
    my ($d, $channel, $value) = @_;

    testSubDevProperty($d, $channel, "FGFUNC.RUN_DELAY", "==", $value);
}



sub setUnits
{
    my ($d, $channel, $value, $method) = @_;

    setSubDevProperty($d, $channel, "FGCONF.CHAN.UNITS", $value, $method);
}



sub testUnits
{
    my ($d, $channel, $value) = @_;

    testSubDevProperty($d, $channel, "FGCONF.CHAN.UNITS", "eq", $value);
}



sub setEventGroup
{
    my ($d, $channel, $value, $method) = @_;

    setSubDevProperty($d, $channel, "FGFUNC.EVENT_GROUP", $value, $method);
}



sub testEventGroup
{
    my ($d, $channel, $value) = @_;

    testSubDevProperty($d, $channel, "FGFUNC.EVENT_GROUP", "==", $value);
}



sub testCrossTalkAndCleanup
{
    my ($d, $channel, $set, $test, $value) = @_;

    # Exclude the tested channel from the list of channels

    my @channels = (0..15);
    @channels = grep { $_ != $channel } @channels;

    # Test for cross-talk between channels

    for my $c (@channels)
    {
        $test->($d, $c, $value);
    }

    # Clean up

    $set->($d, $channel, $value, 1);
    $test->($d, $channel, $value);
}



sub testNonConfigDspProperty
{
    my ($d, $channel) = @_;

    # FG.FUNC.RUN_DELAY is a non-config DSP property

    setRunDelay ($d, $channel, 1.0, 1);
    testRunDelay($d, $channel, 1.0);
    setRunDelay ($d, $channel, 2.0, 2);
    testRunDelay($d, $channel, 2.0);
    setRunDelay ($d, $channel, 3.0, 3);
    testRunDelay($d, $channel, 3.0);

    testCrossTalkAndCleanup($d, $channel, \&setRunDelay, \&testRunDelay, 0.0);
}



sub testConfigDspProperty
{
    my ($d, $channel) = @_;

    # FGCONF.TRIGGER is a config DSP property
    # It's prohibited to set config sub-dev properties using sub-device name, that's why methods 2 and 3 should fail

    testTrigger($d, $channel, "DISABLED");

    setTrigger ($d, $channel, "ENABLED", 1);
    testTrigger($d, $channel, "ENABLED");
    dies_ok { setTrigger($d, $channel, "DISABLED", 2) } "Not allowed to set FG.CONF.TRIGGER";
    dies_ok { setTrigger($d, $channel, "DISABLED", 3) } "Not allowed to set $channel:FG.CONF.TRIGGER";
    testTrigger($d, $channel, "ENABLED");

    testCrossTalkAndCleanup($d, $channel, \&setTrigger, \&testTrigger, "DISABLED");
}



sub testNonConfigMcuProperty
{
    my ($d, $channel) = @_;

    # FGFUNC.EVENT_GROUP is a non-config MCU property
    # In order to be set, the property requires an armed function
    # First, we zero the reference on the channel, then we arm a PLP and test the property

    setTrigger($d, $channel, "ENABLED", 1);
    runToRef($d, $channel, 0, 1);
    wait_until( sub { get($d, "FGFUNC.CVAL[$channel]") > -1e-4 &&
                      get($d, "FGFUNC.CVAL[$channel]") <  1e-4 &&
                      get($d, "FGSTATE.FUNC[$channel]") eq "IDLE" }, 30);
    runToRef($d, $channel, 100);
    wait_until( sub { get($d, "FGSTATE.FUNC[$channel]") eq "ARMED" }, 10);

    setEventGroup ($d, $channel, 1, 1);
    testEventGroup($d, $channel, 1);
    setEventGroup ($d, $channel, 2, 2);
    testEventGroup($d, $channel, 2);
    setEventGroup ($d, $channel, 3, 3);
    testEventGroup($d, $channel, 3);

    testCrossTalkAndCleanup($d, $channel, \&setEventGroup, \&testEventGroup, 0);
}



sub testConfigMcuProperty
{
    my ($d, $channel) = @_;

    # FGCONF.CHAN.UNITS is a config MCU property
    # It's prohibited to set config sub-dev properties using sub-device name, that's why methods 2 and 3 should fail

    testUnits($d, $channel, "NONE");

    setUnits ($d, $channel, "HZ", 1);
    testUnits($d, $channel, "HZ");
    dies_ok { setUnits($d, $channel, "HZ_M",   2) } "Not allowed to set FGCONF.CHAN.UNITS";
    dies_ok { setUnits($d, $channel, "HZ_RAD", 3) } "Not allowed to set $channel:FGCONF.CHAN.UNITS";
    testUnits($d, $channel, "HZ");

    testCrossTalkAndCleanup($d, $channel, \&setUnits, \&testUnits, "NONE");
}



sub getRandomMiddleChannel
{
    return int(rand(14)) + 1;
}



my %params = (device=>undef, config=>'', protocol=>"CMW");
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

assureMainInOff($d, "SIMULATION");

lives_ok
{

testNonConfigDspProperty($d, 0);
testNonConfigDspProperty($d, 15);
testNonConfigDspProperty($d, getRandomMiddleChannel());

testConfigDspProperty($d, 0);
testConfigDspProperty($d, 15);
testConfigDspProperty($d, getRandomMiddleChannel());

testNonConfigMcuProperty($d, 0);
testNonConfigMcuProperty($d, 15);
testNonConfigMcuProperty($d, getRandomMiddleChannel());

testConfigMcuProperty($d, 0);
testConfigMcuProperty($d, 15);
testConfigMcuProperty($d, getRandomMiddleChannel());

};

# EOF
