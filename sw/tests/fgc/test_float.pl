#!/usr/bin/perl

# Test program for: FLOAT

use Test::Utils;

use strict;
use warnings;
use diagnostics;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

set($d,'TEST.FLOAT',0);
is(get($d, 'TEST.FLOAT')+0, 0, "TEST.FLOAT set to 0");

set($d,'TEST.FLOAT',1);
is(get($d, 'TEST.FLOAT')+0, 1, "TEST.FLOAT set to 1");

set($d,'TEST.FLOAT',sprintf('%.7E', 0));
is(get($d, 'TEST.FLOAT')+0, 0, "TEST.FLOAT set to ".(sprintf('%.7E', 0)));

set($d,'TEST.FLOAT',sprintf('%.7E', 1));
is(get($d, 'TEST.FLOAT')+0, 1, "TEST.FLOAT set to ".(sprintf('%.7E', 1)));

my $value = rand();
set($d,'TEST.FLOAT',$value);
is(sprintf('%.3f', get($d, 'TEST.FLOAT')), sprintf('%.3f', $value), "TEST.FLOAT set to rand $value");

# Test random arrays of numbers and random size

my $n = int(rand(16));
my @values;
for(my $i = 0; $i < $n; $i++)
{
    my $v = sprintf("%.5E", (rand(2)-1)*rand(10**30));
    push(@values,$v);
}
my $expected = join(',', @values);
set($d,"TEST.FLOAT",$expected);

@values = map(sprintf("%.5E",$_), split(/,/, get($d, "TEST.FLOAT")));
my $got = join(",",@values);
is($got, $expected, "TEST.FLOAT set to random array");

# EOF
