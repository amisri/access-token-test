#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Simple set/get on TEST.CHAR property

set($d, "TEST.CHAR", "A");
is(get($d, "TEST.CHAR"), "A", "TEST.CHAR simple set/get A");

# Simple set on TEST.CHAR property. Empty set value

set($d, "TEST.CHAR", "");
is(get($d, "TEST.CHAR"), "", "TEST.CHAR set empty value");


# Property too long

dies_ok
    sub { set($d, "TEST.CHAR", "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF") },
    'TEST.CHAR should fail if it is too big';

TODO: {
    local $TODO = 'EPCCCS-2378 TCP protocol should not send replies with commas as separator in case of array of characters' if$params{protocol} =~ /TCP/i;

    set($d, "TEST.CHAR", "AB");
    is(get($d, "TEST.CHAR"), "AB", "TEST.CHAR simple set/get AB");

    set($d, "TEST.CHAR", "ABC");
    is(get($d, "TEST.CHAR"), "ABC", "TEST.CHAR simple set/get ABC");

    # It always returns uppercase

    set($d, "TEST.CHAR", "ABC");
    is(get($d, "TEST.CHAR"), "ABC", "TEST.CHAR simple set/get abc");

    # Simple set on TEST.CHAR property. Values without commas as separators
    # NOTE: With this style you're able to set only up to 31 characters at once

    set($d, "TEST.CHAR", "0123456789");
    is(get($d, "TEST.CHAR"), "0123456789", "TEST.CHAR set values not separated with commas");

    # When setting TEST.CHAR property without commas then max set size is 31 chars
    # Otherwise it's the buffer size (64 chars in case of TEST.CHAR)

    set($d, "TEST.CHAR", "0123456789ABCDEF0123456789ABCDE");
    is(get($d, "TEST.CHAR"), "0123456789ABCDEF0123456789ABCDE", "TEST.CHAR set max buffer size (without commas)");

    # TEST.CHAR set max buffer size (with commas)

    my $values = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,".
                 "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,".
                 "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,".
                 "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F";
    set($d, "TEST.CHAR", $values);
    $values =~ s/,//g;
    is(get($d, "TEST.CHAR"), $values, "TEST.CHAR set max buffer size (with commas)");

}


# EOF
