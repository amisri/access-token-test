/*
 *  Filename: plltest.c
 *
 *  Purpose:  test script for the FGC2 PLL
 */

// Test system headers

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include "test/test.h"

// System under test headers

#include "pll_main.h"
#include "fbs.h"
#include "dpcom.h"
#include "mst.h"

// Overriden variables placed in specific memory locations

#include "memmap_mcu.h"

// Local variable definition to avoid dependency with embedded .c files

struct fbs_vars          fbs;
volatile struct dpcom    dpcom;
struct mst_vars          mst;

// Access to the correct member of the class_data union

#define CLASS_FIELD         c51

static void PllSpyHeader(void)
{
    // Write FGC Spy header

    fprintf(stderr,"time");
    fprintf(stderr,",state_pll");
    fprintf(stderr,",pll.Terr");
    fprintf(stderr,",pll.qtics_per_ms");
    fprintf(stderr,",pll.int_qtics_per_ms");
    fprintf(stderr,",fbs.sync.tic");
    fprintf(stderr,",mst.tic0");
    fprintf(stderr,",pll.sync_f");
    fprintf(stderr,"\n");
}

static void PllSpyData(int iter_idx)
{
    // Write data in FGC Spy format

    fprintf(stderr,"%.3f", 0.02 * iter_idx);
    fprintf(stderr,",%d", fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll);
    fprintf(stderr,",%d", pll.Terr);
    fprintf(stderr,",%ld", pll.qtics_per_ms - PLL_QTICS_PER_MS);
    fprintf(stderr,",%ld", pll.int_qtics_per_ms-PLL_QTICS_PER_MS);
    fprintf(stderr,",%u", fbs.sync.tic0);
    fprintf(stderr,",%u", mst.tic0);
    fprintf(stderr,",%u", pll.sync_f?1:0);

    fprintf(stderr,"\n");
}

void print_usage(char **argv)
{
    printf("Usage: %s [options]\n"
        "      -h,-?                   This help\n"
        "      -v                      verbose\n"
        "      -r <seed>               Random generator seed (default random seed)\n"
        "      -j <stdev jitter>       Standard deviation of the jiter us\n"
        "      -n <iterations>         Number of iterations (default 500 i.e. 10 sec)\n"
        "      -u <probability>        Probability of loosing the time signal"

        , argv[0]
    );
    printf("\nTest the FGC2 PLL. The SPy data is sent through the stderr\n");
}


int strtoint(char *ch_str, int *num)
{
    char * endptr;

    // Check that the option argument was specified

    if (!ch_str)
    {
        fprintf(stderr, "ERROR: Missing string\n");
        return -1;
    }

    // Convert the string to number

    errno = 0;
    *num = strtol(ch_str,&endptr,0);

    // Check that suceeded

    if (!errno && *ch_str != '\0' && *endptr == '\0')
    {
        return 0;
    }
    else
    {
        return -1;
    }

}

int strtofloat(char *ch_str, float *num)
{
    char * endptr;

    // Check that the option argument was specified

    if (!ch_str)
    {
        fprintf(stderr, "ERROR: Missing string\n");
        return -1;
    }

    // Convert the string to number

    errno = 0;
    *num = strtof(ch_str,&endptr);

    // Check that suceeded

    if (!errno && *ch_str != '\0' && *endptr == '\0')
    {
        return 0;
    }
    else
    {
        return -1;
    }

}

void simulate(unsigned int i, float std_jitter_us,float prob_unsync)
{
    int jitter;
    static int prev_jitter = 0;
    uint64_t qtics_count_mst,qtics_count_fbs;
    static uint64_t qtics_carry_mst,qtics_carry_fbs;

    // Initialize counter if the first iteration

    if (!i)
    {
        mst.tic0      = rand() % UINT16_MAX;
        fbs.sync.tic0 = rand() % UINT16_MAX;

        //! This allows the transition from FAILED to NO_SYNC

        SM_UXTIME_P = 0xFFFF;
    }

    // Simulate jitter

    jitter = ((rand() % 3) - 1)*sqrt(6)*std_jitter_us*rand()*PLL_QTICS_PER_MS/1000/RAND_MAX;

    // Simulate loosing  the time packet

    fbs.sync.tic0_f = (rand() < prob_unsync * RAND_MAX)?0:1;

    // Execute FGC2 code

    PllSync();

    // Simulate next millisecond 4th tics

    qtics_count_mst = pll.qtics_per_ms*20 + qtics_carry_mst;
    qtics_carry_mst = qtics_count_mst - (( qtics_count_mst >> 16) << 16);
    mst.tic0 += (qtics_count_mst >> 16);

    // Simulate next fieldbus tics

    qtics_count_fbs = PLL_QTICS_PER_MS*20 + qtics_carry_fbs + jitter - prev_jitter;
    qtics_carry_fbs = qtics_count_fbs - (( qtics_count_fbs >> 16) << 16);
    fbs.sync.tic0  += (qtics_count_fbs >> 16);

    prev_jitter = jitter;

}

int main(int argc, char **argv)
{

    struct timeval time;
    float          std_jitter_us = 5.0;
    float          prob_unsync   = 0.0;
    int            seed          = 0;
    unsigned int   i;
    unsigned int   time_to_lock  = 0;
    int   n                      = 50000;
    int            c;
    uint8_t        old_state_pll, locked_remains_locked = 1, small_offset_when_locked = 1;
    float          var_jitter_out= 0;

    // Parse input parameters

    while ((c = getopt(argc, argv, "?hvu:j:n:r:")) != -1)
    {
        switch (c)
        {
            case 'r':
                if (strtoint(optarg,&seed))
                {
                    fprintf(stderr,"ERROR: '%s' seed is not an integer\n",optarg);
                    exit(1);
                }

                break;
            case 'j':
                if (strtofloat(optarg,&std_jitter_us))
                {
                    fprintf(stderr,"ERROR: '%s' Standard deviation of jitter is not a float\n",optarg);
                    exit(1);
                }

                break;
            case 'u':
                if (strtofloat(optarg,&prob_unsync))
                {
                    fprintf(stderr,"ERROR: '%s' unsync probability is not a float\n",optarg);
                    exit(1);
                }

                if (prob_unsync < 0 || prob_unsync > 1)
                {
                    fprintf(stderr,"ERROR: Unsync probability out of range ( 0 >= p >= 1)\n");
                    exit(1);
                }

                break;
            case 'n':
                if (strtoint(optarg,&n))
                {
                    fprintf(stderr,"ERROR: '%s' seed is not an integer\n",optarg);
                    exit(1);
                }

                if (n<=0)
                {
                    fprintf(stderr,"ERROR: Number of iterations out of range ( n > 0) \n");
                    exit(1);
                }

                break;
            case '?':
            case 'h':
                print_usage(argv);
                exit(1);
                break;

            default:
                fprintf(stderr,"ERROR: Unknown otpion '%c'\n",c);
                print_usage(argv);
                exit(1);

        }
    }

    // Initialize random seed

    if (seed)
    {
        srand((unsigned int)seed);
    }
    else
    {
        gettimeofday(&time,NULL);
        srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
    }

    // Spy headers

    PllSpyHeader();

    // Initialize strctures

    PllInit();

    for(i=0; i < n; ++i)
    {
        old_state_pll = fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll;

        simulate(i,std_jitter_us,prob_unsync);

        // Spy Data

        PllSpyData(i);

        // If it was locked remained locked

        locked_remains_locked = locked_remains_locked &&
            (!(old_state_pll == FGC_PLL_LOCKED && prob_unsync < 0.0001) || (fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED));

        // If locked then the offset error is less than 50us

        small_offset_when_locked = small_offset_when_locked &&
            (!(old_state_pll == FGC_PLL_LOCKED) || (pll.Terr <100));

        if (!time_to_lock && fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED)
        {
            time_to_lock = i;
        }

        // Compute moving averages

        var_jitter_out  +=  1/1000.0*(((float)pll.Terr)/((float)PLD_MASTER_CLOCK_KHZ)*1000)*(((float)pll.Terr)/((float)PLD_MASTER_CLOCK_KHZ)*1000 - var_jitter_out);
    }

    TEST_OK(locked_remains_locked);
    TEST_OK(small_offset_when_locked);

    // We should end up locked if we are in normal conditions

    TEST_OK( fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED );

    if (fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED)
    {
        printf("PLL Locked in %f s (%u iterations)\n",time_to_lock*0.002,time_to_lock);

    }

    // Calculate variance

    printf("PLL Input noise var = %e us, Output noise std = %e us (Noise BW = %e)\n",
        std_jitter_us,
        sqrt(var_jitter_out),
        var_jitter_out/(std_jitter_us*std_jitter_us));

    return TEST_RESULT;
}

// EOF
