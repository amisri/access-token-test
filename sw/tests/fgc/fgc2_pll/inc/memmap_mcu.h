#ifndef FC2PLL_MEMMAP_MCU_H  // header encapsulation
#define FC2PLL_MEMMAP_MCU_H

#include <stdint.h>

//! Variable definition to allow the testing of the FGC2 PLL code

extern uint16_t sm_mstime;
extern uint16_t sm_uxtime;

#define SM_UXTIME_P sm_uxtime
#define SM_MSTIME_P sm_mstime


#endif
