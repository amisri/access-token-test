#!/usr/bin/perl

# Test program for: DSP.INT32S DSP.INT32U

use Test::Utils;

use strict;
use warnings;
use diagnostics;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Test DSP.INT32S

set($d,'TEST.DSP.INT32S',0);
is(get($d, 'TEST.DSP.INT32S'), 0, "TEST.DSP.INT32S set to 0");

set($d,'TEST.DSP.INT32S',1);
is(get($d, 'TEST.DSP.INT32S'), 1, "TEST.DSP.INT32S set to 1");

# Test maximum number

my $value = 2**(31)-1;
set($d,'TEST.DSP.INT32S',$value);
is(get($d, 'TEST.DSP.INT32S'), $value, "TEST.DSP.INT32S set to $value");

$value = $value + 1;
dies_ok( sub { set($d,'TEST.DSP.INT32S',$value) },"TEST.DSP.INT32S set to $value fails");

# Test minimum number

$value = -2**(31);

set($d,'TEST.DSP.INT32S',$value);
is(get($d, 'TEST.DSP.INT32S'), $value, "TEST.DSP.INT32S set to $value");

$value = $value - 1;

dies_ok( sub { set($d,'TEST.DSP.INT32S',$value) },"TEST.DSP.INT32S set to $value fails");

# Test random number

$value = int(rand(2**(32)-1))-2**(32)/2+1;
set($d,'TEST.DSP.INT32S',$value);
is(get($d, 'TEST.DSP.INT32S'), $value, "TEST.DSP.INT32S set to random $value");

# Test DSP.INT32U

set($d,'TEST.DSP.INT32U',0);
is(get($d, 'TEST.DSP.INT32U'), 0, "TEST.DSP.INT32U set to 0");

set($d,'TEST.DSP.INT32U',1);
is(get($d, 'TEST.DSP.INT32U'), 1, "TEST.DSP.INT32U set to 1");

# Test maximum number

$value = 2**(32)-1;
set($d,'TEST.DSP.INT32U',$value);
is(get($d, 'TEST.DSP.INT32U'), $value, "TEST.DSP.INT32U set to $value");

dies_ok( sub { set($d,'TEST.DSP.INT32U',$value+1) },"TEST.DSP.INT32U set to $value+1 fails");

# Test minimum number

$value = 0;

set($d,'TEST.DSP.INT32U',$value);
is(get($d, 'TEST.DSP.INT32U'), $value, "TEST.DSP.INT32U set to $value");

TODO:
{
    local $TODO = "EPCCCS-2525 Set property TEST.DSP.INT32U to $value-1 does not fail on 61 and 62 class devices" if get($d,'DEVICE.CLASS_ID') =~ /61|62/;

    dies_ok( sub { set($d,'TEST.DSP.INT32U',$value-1) },"TEST.DSP.INT32U set to $value-1 fails");
}

# Test random number

$value = int(rand(2**(32)));
set($d,'TEST.DSP.INT32U',$value);
is(get($d, 'TEST.DSP.INT32U'), $value, "TEST.DSP.INT32U set to $value");

# Test DSP.FLOAT

set($d,'TEST.DSP.FLOAT',0);
is(get($d, 'TEST.DSP.FLOAT')+0, 0, "TEST.DSP.FLOAT set to 0");

set($d,'TEST.DSP.FLOAT',1);
is(get($d, 'TEST.DSP.FLOAT')+0, 1, "TEST.DSP.FLOAT set to 1");

set($d,'TEST.DSP.FLOAT',sprintf('%.7E', 0));
is(get($d, 'TEST.DSP.FLOAT')+0, 0, "TEST.DSP.FLOAT set to ".(sprintf('%.7E', 0)));

set($d,'TEST.DSP.FLOAT',sprintf('%.7E', 1));
is(get($d, 'TEST.DSP.FLOAT')+0, 1, "TEST.DSP.FLOAT set to ".(sprintf('%.7E', 1)));

# EOF
