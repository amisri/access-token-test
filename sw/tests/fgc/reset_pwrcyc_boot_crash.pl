#!/usr/bin/perl

# File:    resets_and_power_cycle.pl
# Purpose: Testing of basic functionality - reset, boot, crash and power cycle

use Test::Utils;
use Array::Utils qw(array_diff);

use strict;
use warnings;
use diagnostics;



sub getAvailableResetTypes
{
    my $platform = shift;

    my @reset_types = ("ALL", "SLOWWD", "FASTWD", "MANUAL", "PROGRAM", "POWER");

    if($platform == 50)
    {
        push @reset_types, "C32RAM";
        push @reset_types, "HC16RAM";
    }
    else
    {
        push @reset_types, "DONGLE";
    }

    return \@reset_types;
}



sub getNumResets
{
    my ($d) = @_;

    my $platform    = get($d, "DEVICE.PLATFORM_ID");
    my $reset_types = getAvailableResetTypes($platform);

    my %resets = ();

    foreach my $type (@$reset_types)
    {
        $resets{$type} = get($d, "FGC.NUM_RESETS.$type");
    }

    # Fast watchdog is disabled in FGC3, so it should always be 0

    if($platform == 60)
    {
        $resets{"FASTWD"} = 0;
    }

    return \%resets;
}



sub testResetCounters
{
    my ($d, $action, $resets, @expected) = @_;

    my $platform    = get($d, "DEVICE.PLATFORM_ID");
    my $reset_types = getAvailableResetTypes($platform);

    foreach my $type (@expected)
    {
        cmp_ok(get($d, "FGC.NUM_RESETS.".$type), '==', $resets->{$type} + 1, "Number of resets ($type) after $action");
    }

    my @excluded = array_diff(@$reset_types, @expected);

    foreach my $type (@excluded)
    {
        cmp_ok(get($d, "FGC.NUM_RESETS.".$type), '==', $resets->{$type}, "Number of resets ($type) after $action");
    }
}



sub getPowerCycleDuration
{
    my $platform = shift;

    # It shouldn't take longer than 25 seconds to finish a power cycle, but let's have some margin

    return ($platform == 60) ? 15 : 30;
}



sub getMinCrashTime
{
    my $platform = shift;

    return ($platform == 60) ? 6 : 10;
}



sub fgcPowerCycle
{
    my ($d) = @_;

    set($d, "DEVICE.PWRCYC", "");

    # Power cycle request takes some time, during which the FGC is still in main, so we have
    # to give it some time

    sleep(2);

    wait_until( sub { isInMain($d) } );
}



sub fgcReset
{
    my ($d) = @_;

    set($d, "DEVICE.RESET", "");

    # Reset request takes some time, during which the FGC is still in main, so we have
    # to give it some time

    sleep(2);

    wait_until( sub { isInMain($d) } );
}



sub testPowerCycle
{
    my ($d) = @_;

    my $platform             = get($d, "DEVICE.PLATFORM_ID");
    my $power_cycle_duration = getPowerCycleDuration($platform);

    my $resets = getNumResets($d);

    fgcPowerCycle($d);
    ok(1, "Power cycle");

    my $run_time = get($d, "DEVICE.RUN_TIME");
    my $pwr_time = get($d, "DEVICE.PWR_TIME");

    # Run time and power time should be reset after a power cycle and they should be roughly equal

    cmp_ok($pwr_time, '<', $power_cycle_duration, "Power time after power cycle");
    cmp_ok($run_time, '<', $power_cycle_duration, "Run time after power cycle");
    cmp_ok(abs($pwr_time - $run_time), '<', 2, "Difference between run time and pwr time after power cycle");

    testResetCounters($d, "power cycle", $resets, "ALL", "POWER");
}



sub testReset
{
    my ($d) = @_;

    my $platform             = get($d, "DEVICE.PLATFORM_ID");
    my $power_cycle_duration = getPowerCycleDuration($platform);

    my $resets = getNumResets($d);

    fgcReset($d);
    ok(1, "Reset");

    my $run_time = get($d, "DEVICE.RUN_TIME");
    my $pwr_time = get($d, "DEVICE.PWR_TIME");

    # Only run time should be reset

    cmp_ok($run_time, '<', $power_cycle_duration, "Run time after reset");
    cmp_ok(abs($pwr_time - $run_time), '>', $power_cycle_duration / 3, "Difference between run time and pwr time after reset");

    testResetCounters($d, "reset", $resets, "ALL", "PROGRAM");
}



sub testCrash
{
    my ($d) = @_;

    my $platform = get($d, "DEVICE.PLATFORM_ID");
    my $resets   = getNumResets($d);

    set($d, "DEVICE.CRASH", "");
    wait_until( sub { isInBoot($d) || isInMain($d) } );
    sleep(1);
    ok(1, "Crash");

    assureMain($d);

    my $run_time       = get($d, "DEVICE.RUN_TIME");
    my $pwr_time       = get($d, "DEVICE.PWR_TIME");
    my $min_crash_time = getMinCrashTime($platform);

    # Only run time is reset after a crash

    cmp_ok($run_time, '<', 30, "Run time after crash");
    cmp_ok(abs($pwr_time - $run_time), '>', $min_crash_time, "Difference between run time and pwr time after crash");

    # Fast watchdog is disabled in FGC3, so it should be killed by slow watchdog instead

    testResetCounters($d, "crash", $resets, "ALL", ($platform == 50) ? "FASTWD" : "SLOWWD");
}



sub testBoot
{
    my ($d) = @_;

    my $resets = getNumResets($d);

    set($d, "DEVICE.BOOT", "");
    wait_until( sub { isInBoot($d) } );
    ok(1, "Boot");

    assureMain($d);

    testResetCounters($d, "boot", $resets, "ALL", "PROGRAM");
}



sub testPropertyReset
{
    my ($d) = @_;

    set($d, "FGC.NUM_RESETS", "RESET");

    my $resets = getNumResets($d);

    testResetCounters($d, "property reset", $resets);
}



my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Test main program first

assureMainInOff($d, "SIMULATION");

lives_ok
{

testPowerCycle($d);
testReset($d);
testCrash($d);
testBoot($d);
testPropertyReset($d);

};

## Test boot now
#
## Go to boot (+1 total resets, +1 program resets)
#
#set($d, "DEVICE.BOOT", "");
#wait_until( sub { isInBoot($d) } );
#sleep(2);
#
## Power cycle from boot (+1 total resets, +1 power resets)
#
#sendCommandNoWait($d, "620");
#wait_until( sub { isInBoot($d) } );
#
## FGC3 ends up in boot, while FGC2 goes to main
#
#assureMain($d);
#wait_until( sub { isInMain($d) } );
#ok(1, "Power cycle from boot");
#
## Go to boot
#
#set($d, "DEVICE.BOOT", "");
#wait_until( sub { isInBoot($d) } );
#sleep(2);
#
## Reset from boot (+1 total resets, +1 program resets)
#
#sendCommandNoWait($d, "621");
#wait_until( sub { isInBoot($d) } );
#
## FGC3 ends up in boot, while FGC2 goes to main
#
#assureMain($d);
#wait_until( sub { isInMain($d) } );
#ok(1, "Reset from boot");
#
## Go to boot
#
#set($d, "DEVICE.BOOT", "");
#wait_until( sub { isInBoot($d) } );
#sleep(2);
#
## Boot -> main transition
#
#sendCommandNoWait($d, "0");
#wait_until( sub { isInMain($d) } );
#ok(1, "Boot to main transition from boot");
#
#cmp_ok(get($d, "FGC.NUM_RESETS.ALL"),     '==', $num_all_resets     + 9, "Total number of resets");
#cmp_ok(get($d, "FGC.NUM_RESETS.PROGRAM"), '==', $num_program_resets + 6, "Total number of program resets");
#cmp_ok(get($d, "FGC.NUM_RESETS.MANUAL"),  '==', $num_manual_resets,      "Total number of manual resets");
#cmp_ok(get($d, "FGC.NUM_RESETS.POWER"),   '==', $num_power_resets   + 2, "Total number of power resets");
#
#my $total_resets = get($d, "FGC.NUM_RESETS.PROGRAM") + get($d, "FGC.NUM_RESETS.MANUAL") + get($d, "FGC.NUM_RESETS.POWER");
#
#if($platform == 50)
#{
#    cmp_ok(get($d, "FGC.NUM_RESETS.SLOWWD"),  '==', $num_slowwd_resets,     "Total number of slow watchdog resets");
#    cmp_ok(get($d, "FGC.NUM_RESETS.FASTWD"),  '==', $num_fastwd_resets + 1, "Total number of fast watchdog resets");
#    cmp_ok(get($d, "FGC.NUM_RESETS.C32RAM"),  '==', $num_c32ram_resets,     "Total number of c32 RAM resets");
#    cmp_ok(get($d, "FGC.NUM_RESETS.HC16RAM"), '==', $num_c32ram_resets,     "Total number of hc16 RAM resets");
#
#    $total_resets += get($d, "FGC.NUM_RESETS.SLOWWD") + get($d, "FGC.NUM_RESETS.FASTWD") +
#                     get($d, "FGC.NUM_RESETS.C32RAM") + get($d, "FGC.NUM_RESETS.HC16RAM");
#}
#else
#{
#    cmp_ok(get($d, "FGC.NUM_RESETS.SLOWWD"), '==', $num_slowwd_resets + 1, "Total number of slow watchdog resets");
#    cmp_ok(get($d, "FGC.NUM_RESETS.FASTWD"), '==', $num_fastwd_resets,     "Total number of fast watchdog resets");
#    cmp_ok(get($d, "FGC.NUM_RESETS.DONGLE"), '==', $num_dongle_resets,     "Total number of dongle resets");
#
#    $total_resets += get($d, "FGC.NUM_RESETS.SLOWWD") + get($d, "FGC.NUM_RESETS.FASTWD") + get($d, "FGC.NUM_RESETS.DONGLE");
#}
#
#cmp_ok($total_resets, '==', get($d, "FGC.NUM_RESETS.ALL"), "Sum of resets equal to NUM_RESETS.ALL");

# EOF
