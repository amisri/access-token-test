import pyfgc

import datetime
import time
import sys


def assert_ok(resp: pyfgc.fgc_response.FgcResponse):
    if resp.err_code != "" or resp.err_msg != "":       # For protocol="rda", only err_msg is set
        # logger.info("Failing response: %s", resp)
        raise pyfgc.FgcResponseError(f"{resp.err_code}: {resp.err_msg}")

def l(what):
    print(what)
    return what

def assert_value(resp, value):
    print(resp, "vs", value)
    assert resp.value == value

def now():
    return f"{datetime.datetime.now().strftime('%T.%f')}"

def set_and_check(fgc, prop, value):
    assert_ok(fgc.set(prop, value))

    resp = fgc.get(prop)

    if resp.value != value:
        raise Exception(f"Mismatch: {resp} vs expected '{value}'")


_, dev = sys.argv


print("Using", dev)

with pyfgc.fgc(dev) as fgc:
    assert l(fgc.get("REF.RUN RANGE")).value.startswith("(0 4294967295) ")

    # fgc3: 27: invalid time
    # fgcd: 28: out of limits
    assert l(fgc.set("REF.RUN", "-1")).err_code in {"27", "28"}

    # fgc3: '0' in seconds is interpreted as "now" (see ParsScanAbsTime)
    # fgcd: '0' is taken literally
    # fgc3: 2^32-1 raises 18: bad integer (ParsScanInteger)
    for exponent in range(1, 32 + 1):
        val_str = "%.6f" % (2 ** exponent - 1)
        print(val_str)
        set_and_check(fgc, "REF.RUN", val_str)

    # fgc3: 18: bad integer
    # fgcd: 28: out of limits
    assert l(fgc.set("REF.RUN", "4294967296")).err_code in {"18", "28"}

    print("done")
