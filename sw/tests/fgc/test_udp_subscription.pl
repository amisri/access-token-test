#!/usr/bin/perl

use Test::Utils;

use strict;
use warnings;
use diagnostics;



my %params = (device=>'RFMZ.866.27.DEV', config=>'', protocol=>'TCP');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

subscribe($d);

ok( get_next_publication($d), 'Publication is not empty' );
ok( get_next_publication($d), 'Publication is not empty' );
ok( get_next_publication($d) =~ /DATA_STATUS.DATA_VALID:2/, 'Publication has valid status' );
ok( get_next_publication($d) =~ /CYCLE_NUM/, 'Publication has CYCLE_NUM field' );
ok( get_next_publication($d) =~ /CYCLE_USER/, 'Publication has CYCLE_USER field' );

unsubscribe($d);

# EOF
