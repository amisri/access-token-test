/*---------------------------------------------------------------------------------------------------------*\
  File:     plltest.c

  Purpose:  Test program for pll.c used in the FGC3 MCU program to run a phase-locked loop using a DAC
            driven VXCO.

  Authors:  Quentin.King@cern.ch
\*---------------------------------------------------------------------------------------------------------*/

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <math.h>
#include <test/test.h>

// FGC 3 specific

#include "pll.h"
#include "memmap_mcu.h"        // For PLL register masks
#include "defconst.h"


#define PLL_TICKS_TO_SECONDS            25000000  // How many ticks a s represents.

#define random rand             // Required by MinGW

// Constants

#define SYNC_REG_MASK          (~0xFFF)                         // Mask for top 20 bits

// Variables simulating hardware registers

INT32U  PLL_INTSYNCTIME;                        // Simulated PLL internal sync time register
INT32U  PLL_EXTSYNCTIME;                        // Simulated PLL external sync time register
INT32U  PLL_FIPSYNCTIME;                        // Simulated PLL FIP sync time register
INT32U  PLL_ETH18SYNCTIME;                      // Simulated PLL Ethernet 0 sync time register
INT32U  PLL_ETH19SYNCTIME;                      // Simulated PLL Ethernet 1 sync time register

// Global variables containing main arguments

int     seed               = 0;
int     iter_sync_lost     = -1;
int     verbose            = 0;
int     n                  = 1000;
double  vcxo_freq_err_ppm  = 0;
double  ext_sync_jitter_ns = 10;
double  net_sync_jitter_ns = 10000;
double  prob_unsync        = 0.0;

static void PllSpyHeader(void)
{
    // Write FGC Spy header

    fprintf(stderr,"time");
    fprintf(stderr,",pll.state");

    fprintf(stderr,",pll.pi_err");
    fprintf(stderr,",pll.network.phase_err_average");
    fprintf(stderr,",pll.external.phase_err_average");
    fprintf(stderr,",pll.eth_sync_cal");

    fprintf(stderr,",pll.integrator_ppm");
    fprintf(stderr,",pll.filtered_integrator_ppm");
    fprintf(stderr,",pll.dac_ppm");

    fprintf(stderr,"\n");
}

static void PllSpyData(int iter_idx)
{
    // Write data in FGC Spy format

    fprintf(stderr,"%.3f",PLL_ITER_PERIOD * iter_idx);
    fprintf(stderr,",%d",pll.state);

    fprintf(stderr,",%.6f",pll.pi_err/PLL_TICKS_TO_SECONDS);
    fprintf(stderr,",%.6f",pll.network.phase_err_average/PLL_TICKS_TO_SECONDS);

    fprintf(stderr,",%.6f",pll.external.phase_err_average/PLL_TICKS_TO_SECONDS);
    fprintf(stderr,",%.6f",pll.eth_sync_cal/PLL_TICKS_TO_SECONDS);

    fprintf(stderr,",%.6f",             pll.integrator_ppm);
    fprintf(stderr,",%.6f",             pll.filtered_integrator_ppm);
    fprintf(stderr,",%.6f",             pll.dac_ppm);

    fprintf(stderr,"\n");
}

static void PllSimInit(void)
{
    // Initialise after a reset to 1 (this inhibits 50 Hz internal sync)

    PLL_INTSYNCTIME =  PLL_INTSYNCTIME_RST_MASK32;

    // Initialise sync times in seconds

    PLL_FIPSYNCTIME   = (2 * PLL_ITER_PERIOD_TICKS - PLL_FIP_PHASE_REF)        << PLL_INTSYNCTIME_LATCH_SHIFT;
    PLL_EXTSYNCTIME   = (2 * PLL_ITER_PERIOD_TICKS - PLL_EXT_PHASE_REF)        << PLL_INTSYNCTIME_LATCH_SHIFT;
    PLL_ETH18SYNCTIME = (2 * PLL_ITER_PERIOD_TICKS - PLL_ETH18_PHASE_REF)      << PLL_INTSYNCTIME_LATCH_SHIFT;
    PLL_ETH19SYNCTIME = (2 * PLL_ITER_PERIOD_TICKS - PLL_ETH19_PHASE_REF)      << PLL_INTSYNCTIME_LATCH_SHIFT;

    // Initialise sync times to indicate that syncs are old (not fresh)

    pll.ext.sync_time   = PLL_EXTSYNCTIME_OLD_MASK32;
    pll.eth18.sync_time = PLL_NETIRQTIME_OLD_MASK32;
    pll.eth19.sync_time = PLL_NETIRQTIME_OLD_MASK32;
}

static double PllJitter(double std_jitter_ns)
{
    return ((random() % 3) - 1)*sqrt(6)*std_jitter_ns*random()*1.0e-9/RAND_MAX;
}

static void PllSim(int iter_idx)
{
    double  jitter_s;
    double  vcxo_period_s;
    double  delta_vcxo_freq;

    // Variables to generetae white noise (instead of brownian/wienner)

    static int prev_ext_jitter_tics = 0;
    static int prev_eth18_jitter_tics = 0;
    static int prev_eth19_jitter_tics = 0;

    // Wait 5 iterations before providing valid syncs

    if(iter_idx < 5)
    {
        return;
    }

    // Set VXCO frequency based on DAC

    delta_vcxo_freq = PLL_VCXO_FREQ * 1.0E-6 * (vcxo_freq_err_ppm + pll.dac_ppm);

    vcxo_period_s = 1.0 / (PLL_VCXO_FREQ + delta_vcxo_freq);

    // Advance INT SYNC TIME register by 20 ms once it is not in reset

    if(!(PLL_INTSYNCTIME & PLL_INTSYNCTIME_RST_MASK32))
    {
        PLL_INTSYNCTIME += (PLL_ITER_PERIOD_TICKS << PLL_INTSYNCTIME_LATCH_SHIFT);
    }

   if(iter_sync_lost == -1 || iter_idx < iter_sync_lost)
    {
        jitter_s = PllJitter(ext_sync_jitter_ns);

        PLL_EXTSYNCTIME += (INT32U)(4096.0 * (PLL_ITER_PERIOD + jitter_s) / vcxo_period_s) - prev_ext_jitter_tics;

        pll.ext.sync_time = PLL_EXTSYNCTIME & SYNC_REG_MASK;

        prev_ext_jitter_tics = (INT32U)(4096.0 * (PLL_ITER_PERIOD + jitter_s) / vcxo_period_s)
            - (INT32U)(4096.0 * (PLL_ITER_PERIOD) / vcxo_period_s);

    }
    else
    {
        // Simulate failure of external sync

        pll.ext.sync_time = (PLL_EXTSYNCTIME & SYNC_REG_MASK) | 1;
    }

    // First Ethernet sync

    jitter_s = PllJitter(net_sync_jitter_ns);

    PLL_ETH18SYNCTIME += (INT32U)(4096.0 * (PLL_ITER_PERIOD + jitter_s) / vcxo_period_s) - prev_eth18_jitter_tics;

    pll.eth18.sync_time = PLL_ETH18SYNCTIME & SYNC_REG_MASK;

    prev_eth18_jitter_tics = (INT32U)(4096.0 * (PLL_ITER_PERIOD + jitter_s) / vcxo_period_s)
                    - (INT32U)(4096.0 * (PLL_ITER_PERIOD) / vcxo_period_s);

    // Second Ethernet sync

    jitter_s = PllJitter(net_sync_jitter_ns);

    PLL_ETH19SYNCTIME += (INT32U)(4096.0 * (PLL_ITER_PERIOD + jitter_s) / vcxo_period_s) - prev_eth19_jitter_tics;

    pll.eth19.sync_time = PLL_ETH19SYNCTIME & SYNC_REG_MASK;

    prev_eth19_jitter_tics = (INT32U)(4096.0 * (PLL_ITER_PERIOD + jitter_s) / vcxo_period_s)
                    - (INT32U)(4096.0 * (PLL_ITER_PERIOD) / vcxo_period_s);
}

void print_usage(char **argv)
{
    // Process command line arguments

    printf("Test the FGC3 PLL\n\n");
    printf("Usage: %s [options]\n"
        "      -h,-?                      This help\n"
        "      -r <seed>                  Random generator seed (default random seed)\n"
        "      -o <vcxo_freq_offset_ppm>  VCXO Frequency offset error in ppm (default 0)\n"
        "      -f                         WorldFip or Ethrnet fieldbus (default)\n"
        "      -j <ext_sync_jitter_ns>    Jitter of the 50Hz external timing signal (default 10ns)\n"
        "      -w <net_sync_jitter_ns>    Jitter of Ethernet time packet (default 10000ns)\n"
        "      -r <seed>                  Random generator seed\n"
        "      -n <iterations>            Number of iterations (default 1000 i.e. 20 sec)\n"
        "      -l <iter_sync_lost>        Iteration where the sync is lost (default not lost)\n\n"

        , argv[0]
    );
}

int strtoint(char *ch_str, int *num)
{
    char * endptr;

    // Check that the option argument was specified

    if (!ch_str)
    {
        fprintf(stderr, "ERROR: Missing string\n");
        return -1;
    }

    // Convert the string to number

    errno = 0;
    *num = strtol(ch_str,&endptr,0);

    // Check that suceeded

    if (!errno && *ch_str != '\0' && *endptr == '\0')
    {
        return 0;
    }
    else
    {
        return -1;
    }

}

int strtodouble(char *ch_str, double *num)
{
    char * endptr;

    // Check that the option argument was specified

    if (!ch_str)
    {
        fprintf(stderr, "ERROR: Missing string\n");
        return -1;
    }

    // Convert the string to number

    errno = 0;
    *num = strtod(ch_str,&endptr);

    // Check that suceeded

    if (!errno && *ch_str != '\0' && *endptr == '\0')
    {
        return 0;
    }
    else
    {
        return -1;
    }

}

int main(int argc, char **argv)
{
    int     i,c;
    struct timeval time;

    // Parse input parameters

    while ((c = getopt(argc, argv, "?hvr:o:fj:w:n:u:l:")) != -1)
    {
        switch (c)
        {
            case 'v':
                verbose = 1;

                break;
            case 'r':
                if (strtoint(optarg,&seed))
                {
                    fprintf(stderr,"ERROR: '%s' seed is not an integer\n",optarg);
                    exit(1);
                }

                break;
            case 'o':
                if (strtodouble(optarg,&vcxo_freq_err_ppm))
                {
                    fprintf(stderr,"ERROR: '%s' VCXO Frequency offset is not a double\n",optarg);
                    exit(1);
                }

                break;
            case 'j':
                if (strtodouble(optarg,&ext_sync_jitter_ns))
                {
                    fprintf(stderr,"ERROR: '%s' 50Hz signal jitter is not a double\n",optarg);
                    exit(1);
                }

                if (ext_sync_jitter_ns < 0)
                {
                    fprintf(stderr,"ERROR: 50Hz signal jitter out of range ( j >= 0)\n");
                    exit(1);
                }

                break;
            case 'w':
                if (strtodouble(optarg,&net_sync_jitter_ns))
                {
                    fprintf(stderr,"ERROR: '%s' Ethernet time packet arrival time jitter is not a double\n",optarg);
                    exit(1);
                }

                if (net_sync_jitter_ns < 0)
                {
                    fprintf(stderr,"ERROR: Ethernet time packet arrival time jitter out of range ( w >= 0)\n");
                    exit(1);
                }

                break;
            case 'n':
                if (strtoint(optarg,&n))
                {
                    fprintf(stderr,"ERROR: '%s' seed is not an integer\n",optarg);
                    exit(1);
                }

                if (n<=0)
                {
                    fprintf(stderr,"ERROR: Number of iterations out of range ( n > 0) \n");
                    exit(1);
                }

                break;
            case 'l':
                if (strtoint(optarg,&iter_sync_lost))
                {
                    fprintf(stderr,"ERROR: '%s' Iteration when the 50 Hz signal is lost is not an int\n",optarg);
                    exit(1);
                }

                if (iter_sync_lost < 0)
                {
                    fprintf(stderr,"ERROR: Iteration when the 50 Hz signal is lost out of range ( l >= 0)\n");
                    exit(1);
                }

                break;
            case '?':
            case 'h':
                print_usage(argv);
                exit(1);
                break;

            default:
                fprintf(stderr,"ERROR: Unknown otpion '%c'\n",c);
                print_usage(argv);
                exit(1);

        }
    }

    // Initialize random seed

    if (seed)
    {
        srand((unsigned int)seed);
    }
    else
    {
        gettimeofday(&time,NULL);
        srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
    }

    // Prepare simulation

    PllSimInit();

    // Initialise PLL with the integrator set to 0 ppm.

    PllInit(0.0, 0.0);

    // Write FGC Spy header

    PllSpyHeader();


    // Auxiliary test variables

    int locked_remains_locked    = 1;
    int small_offset_when_locked = 1;
    int time_to_lock             = 0;
    double var_jitter_out        = 0;

    // Run simulation

    for(i=0 ; i < n ; i++)
    {
        INT16U old_state_pll = pll.state;

        // Simulate PLL registers in FGC3

        PllSim(i);

        // Provide INTSYNCTIME register for PllCalc

        PLL_INTSYNCTIME = PllCalc(PLL_INTSYNCTIME);

        // Write data in FGC Spy format

        PllSpyData(i);

        // If it was locked remained locked

        locked_remains_locked = locked_remains_locked &&
            (!(old_state_pll == FGC_PLL_LOCKED && iter_sync_lost == -1) || (pll.state == FGC_PLL_LOCKED));

        // If locked then the offset error is less than 50us

        small_offset_when_locked = small_offset_when_locked &&
            (!(old_state_pll == FGC_PLL_LOCKED) || (pll.pi_err < 200));

        if (!time_to_lock && pll.state == FGC_PLL_LOCKED)
        {
            time_to_lock = i;
        }

        // Compute moving averages

        var_jitter_out  += 1/100.0*(((float)pll.pi_err)/((float)PLL_TICKS_TO_SECONDS)*1000000)*(((float)pll.pi_err)/((float)PLL_TICKS_TO_SECONDS)*1000000 - var_jitter_out);
    }

    TEST_OK(locked_remains_locked);
    TEST_OK(small_offset_when_locked);

    // We should end up locked if we are in normal conditions

    TEST_OK( pll.state == FGC_PLL_LOCKED );

    if (pll.state == FGC_PLL_LOCKED)
    {
        printf("PLL Locked in %f s (%u iterations)\n",time_to_lock*0.002,time_to_lock);

    }

    // Calculate variance

    printf("PLL Input noise var = %e us, Output noise std = %e us (Noise BW = %e)\n",
        ext_sync_jitter_ns/1000.0,
        sqrt(var_jitter_out),
        var_jitter_out/(ext_sync_jitter_ns*ext_sync_jitter_ns)*1000000.0);

    return TEST_RESULT;
}

