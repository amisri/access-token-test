#!/usr/bin/perl

# Test program for: INT8U, INT8S, INT16U, INT16S, INT32U, INT32S,

use Test::Utils;

use strict;
use warnings;
use diagnostics;

my %params = (device=>undef, config=>'', protocol=>'');
%params = parse_opts %params;

authenticate($params{authenticate});

my $d = $params{device};

# Load config file if specified by CLI

load_config($d, $params{config}) if $params{config};

# Test the three integer sizes

for my $size (8,16,32)
{
    my $prop_sign   = "TEST.INT" . $size . "S";
    my $prop_unsign = "TEST.INT" . $size . "U";

    # Test number 0

    set($d,$prop_sign,0);
    is(get($d, $prop_sign), 0, "$prop_sign set to 0");

    set($d,$prop_unsign,0);
    is(get($d, $prop_unsign), 0, "$prop_unsign set to 0");

    # Test maximum number

    my $value = 2**($size-1)-1;

    set($d,$prop_sign,$value);
    is(get($d, $prop_sign), $value, "$prop_sign set to $value");
    dies_ok( sub { set($d,$prop_sign,$value+1) },"$prop_sign set to $value+1 should fail");

    $value = 2**($size)-1;

    set($d,$prop_unsign,$value);
    is(get($d, $prop_unsign), $value, "$prop_unsign set to $value");

    dies_ok( sub { set($d,$prop_unsign,$value+1) },"$prop_unsign set to $value+1 should fail");

    # Test minimum number

    $value = -2**($size-1);

    dies_ok( sub { set($d,$prop_sign,$value-1) },"$prop_sign set to $value-1 should fail");

    $value = -2**($size-1)+1;

    set($d,$prop_sign,$value);
    is(get($d, $prop_sign), $value, "$prop_sign set to $value");

    # Test random arrays of numbers and random size

    my $n = int(rand(64*8/$size));
    my @values;
    for(my $i = 0; $i < $n; $i++)
    {
        push @values, int(rand(2**($size)-1));
    }
    $value = join(',', @values);
    set($d,$prop_unsign,$value);
    is(get($d, $prop_unsign), $value, "$prop_unsign set to random $value");

    $n = int(rand(64*8/$size));
    @values = ();
    for(my $i = 0; $i < $n; $i++)
    {
        push @values, int(rand(2**($size)-1))-2**($size)/2;
    }
    $value = join(',', @values);
    set($d,$prop_sign,$value);
    is(get($d, $prop_sign), $value, "$prop_sign set to random $value");
}

# EOF
