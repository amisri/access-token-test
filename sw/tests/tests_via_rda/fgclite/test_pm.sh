#! /usr/bin/bash

# test_exceptions.sh

# Set arguments vars and include rda test functions

nargs=1
arglist="device"

cd `dirname $0`

source ../test_via_rda.sh

#   test_set             "$user"  $property  $value  $exception
#   test_tr_set  $tr_id  "$user"  $property  $value  $exception
#
#   test_get             "$user"  $property  $exception_or_value
#   test_tr_get  $tr_id  "$user"  $property  $exception_or_value
#
#   test_sub             "$user"  $property  $exception_or_value
#   test_tr_sub  $tr_id  "$user"  $property  $exception_or_value

device=$1

# Prepare the FGC for the series of tests

test_set         ""        MODE.PC                OFF        "SET"
test_get         ""        STATE.PC               OFF

test_set         ""        MODE.OP                NORMAL     "SET"
test_set         ""        REG.MODE               I          "SET"
test_set         ""        REG.MODE_CYC           I          "SET"

test_set         ""        REF.RAMP.FINAL         10         "SET"
test_set         ""        REF.RAMP.ACCELERATION  10         "SET"
test_set         ""        REF.RAMP.DECELERATION  10         "SET"
test_set         ""        REF.RAMP.LINEAR_RATE   1          "SET"

# Run the tests

for i in {1..50..1}
do 
   echo "$i. Starting test"

    test_set         ""        MODE.PC                IDLE       "SET"
    sleep 6
    test_get         ""        STATE.PC               IDLE

    test_set         ""        REF.FUNC.TYPE          RAMP       "SET"
    test_set         ""        REF.RUN                0          "SET"
    sleep 6
    test_set         ""        FGC.FAULTS             FGC_STATE  "SET"
    sleep 62
    test_set         ""        MODE.PC                OFF        "SET"
    test_get         ""        STATE.PC               OFF
done

# EOF

