# test_via_rda.sh - provide functions to test an FGC class via RDA
#
# Globals inputs: 
#   $nargs - number of required arguments
#   $arglist - argument list for usage message
#   $device - Device to test
#
# Global output: 
#   $test_number - number of the last executed 
#   $rsp - response from RDA command (value or exception)
#
# Test functions:
#
#   test_set             "$user"  $property  $value  $exception
#   test_tr_set  $tr_id  "$user"  $property  $value  $exception
#
#   test_get             "$user"  $property  $exception_or_value
#   test_tr_get  $tr_id  "$user"  $property  $exception_or_value
#
#   test_sub             "$user"  $property  $exception_or_value
#   test_tr_sub  $tr_id  "$user"  $property  $exception_or_value
#
# $user must include "-c " prefix or can be empty "" if no user is specfied

# Initialisation

export PATH=/bin:/usr/bin:/mcr/bin

script=`basename $0`

if [ $# -ne $nargs ]; then

    echo "usage: $script $arglist"
    exit 1
fi

test_number=0
rsp="none"

# Get an RBAC token

if [ -z "$RBAC_TOKEN_SERIALIZED" ]; then

    eval $(rbac-authenticate -e)

    if [ -z "$RBAC_TOKEN_SERIALIZED" ]; then

        echo "Failed to acquire an RBAC token"
        exit 1
    fi
fi

# Test functions

test_set() 
{
    local user=$1
    local property=$2
    local value=$3
    local exception=$4

    let test_number++

    rsp=$(rda-set -r reuse -d "$device" $user -p "$property" -v "$value" 2>&1)

    if [[ $exception != "" && ! $rsp =~ "$exception" ]]; then
        echo "not ok $test_number - S $device:$property $value - exp:$exception got:$rsp"
        exit 1
    fi

    echo "ok $test_number - S $device:$property $value"
}



test_tr_set() 
{
    local tr_id=$1
    local user=$2
    local property=$3
    local value=$4
    local exception=$5

    let test_number++

    rsp=$(rda-set -r reuse -d "$device" $user -p "$property" -v "$value" \
          --fname transactionId,filterStartIndex --ftype int32,int32 --fvalue "$tr_id",0 2>&1)

    if [[ $exception != "" && ! $rsp =~ "$exception" ]]; then
        echo "not ok $test_number - S $device:$property<$tr_id> $value - exp:$exception got:$rsp"
        exit 1
    fi

    echo "ok $test_number - S $device:$property<$tr_id> $value"
}



test_get() 
{
    local user=$1
    local property=$2
    local exception=$3

    let test_number++

    rsp=$(rda-get -v -r reuse -d "$device" $user -p "$property" 2>&1)

    if [[ $exception != "" && ! $rsp =~ "$exception" ]]; then
        echo "not ok $test_number - G $device:$property - exp:$exception got:$rsp"
        exit 1
    fi
    
    echo "ok $test_number - G $device:$property = $exception"
}



test_tr_get() 
{
    local tr_id=$1
    local user=$2
    local property=$3
    local exception=$4

    let test_number++

    rsp=$(rda-get -v -r reuse -d "$device" $user -p "$property" \
          --fname transactionId,filterStartIndex --ftype int32,int32 --fvalue "$tr_id",0 2>&1)

    if [[ $exception != "" && ! $rsp =~ "$exception" ]]; then
        echo "not ok $test_number - G $device:$property<$tr_id> - exp:$exception got:$rsp"
        exit 1
    fi
    
    echo "ok $test_number - G $device:$property<$tr_id> = $exception"
}



test_sub() 
{
    local user=$1
    local property=$2
    local exception=$3

    let test_number++

    rsp=$(rda-subscribe -v -r reuse -n 1 -d "$device" $user -p "$property" \
          --fname filterStartIndex --ftype int32 --fvalue 0 2>&1)

    if [[ $exception != "" && ! $rsp =~ "$exception" ]]; then
        echo "not ok $test_number - SUB $device:$property - exp:$exception got:$rsp"
        exit 1
    fi
    
    echo "ok $test_number - SUB $device:$property = $exception"
}



test_tr_sub() 
{
    local tr_id=$1
    local user=$2
    local property=$3
    local exception=$4

    let test_number++

    rsp=$(rda-subscribe -v -r reuse -n 1 -d "$device" $user -p "$property" \
          --fname transactionId,filterStartIndex --ftype int32,int32 --fvalue "$tr_id",0 2>&1)

    if [[ $exception != "" && ! $rsp =~ "$exception" ]]; then
        echo "not ok $test_number - SUB $device:$property<$tr_id> $value - exp:$exception got:$rsp"
        exit 1
    fi
    
    echo "ok $test_number - SUB $device:$property<$tr_id> = $exception"
}

# EOF

