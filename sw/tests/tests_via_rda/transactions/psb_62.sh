#! /usr/bin/bash

# psb_62.sh

cd `dirname $0`

source ../test_via_rda.sh

#   test_set             "$user"  $property  $value  $exception
#   test_tr_set  $tr_id  "$user"  $property  $value  $exception
#
#   test_get             "$user"  $property  $exception_or_value
#   test_tr_get  $tr_id  "$user"  $property  $exception_or_value
#
#   test_sub             "$user"  $property  $exception_or_value
#   test_tr_sub  $tr_id  "$user"  $property  $exception_or_value

# $user must include "-c " prefix or can be empty "" if no user is specfied

device="RPZEQ.866.06.ETH8"
user1="-c PSB.USER.ALL"
user2="-c PSB.USER.ZERO"
user3="-c PSB.USER.MD10"


# Start simulated converter

test_set         ""        MODE.PC               OFF            "SET"

sleep 2

test_get         ""        STATE.PC              OFF

test_set         ""        MODE.OP               SIMULATION "SET"

# PPM settings

test_set         ""        DEVICE.PPM            ENABLED    ""

# ROLLBACK to start cleanly ------------

test_set         ""        TRANSACTION.ROLLBACK  5          ""

test_set         "$user2"  REF.PULSE.REF.VALUE   2          "SET"
test_set         "$user3"  REF.PULSE.REF.VALUE   3          "SET"

# TRANSACTION.ID tests ------------------

test_set         "$user2"  TRANSACTION.ID        -1         "invalid transaction ID"
test_set         "$user2"  TRANSACTION.ID        0          "invalid transaction ID"
test_set         "$user2"  TRANSACTION.ID        65536      "invalid transaction ID"
test_set         "$user2"  TRANSACTION.ID        5          "SET"
test_set         "$user2"  TRANSACTION.ID        5          "transaction in progress"


# NON-TRANSACTIONAL SET TESTS --------------------

test_set         "$user2"  REF.FUNC.PLAY          ENABLED    "SET"
test_set         "$user3"  REF.FUNC.PLAY          ENABLED    "SET"

test_set         "$user2"  REF.PULSE.REF.VALUE    5          "transaction in progress"
test_set         "$user3"  REF.PULSE.REF.VALUE    6          "SET"

# TRANSACTIONAL SET TESTS --------------------

test_tr_set  5   "$user2"  REF.FUNC.PLAY          ENABLED    "property not transactional"
test_tr_set  5   "$user3"  REF.FUNC.PLAY          ENABLED    "property not transactional"

test_tr_set  5   "$user2"  REF.PULSE.REF.VALUE    8          "SET"
test_tr_set  5   "$user3"  REF.PULSE.REF.VALUE    9          "transaction not in progress"

test_tr_set  99  "$user2"  REF.FUNC.PLAY          ENABLED    "property not transactional"
test_tr_set  99  "$user3"  REF.FUNC.PLAY          ENABLED    "property not transactional"

test_tr_set  99  "$user2"  REF.PULSE.REF.VALUE    11         "incorrect transaction ID"
test_tr_set  99  "$user3"  REF.PULSE.REF.VALUE    12         "transaction not in progress"

# NON-TRANSACTIONAL GET TESTS --------------------

test_get         "$user2"  REF.FUNC.PLAY          ENABLED
test_get         "$user3"  REF.FUNC.PLAY          ENABLED

test_get         "$user2"  REF.PULSE.REF.VALUE    8
test_get         "$user3"  REF.PULSE.REF.VALUE    6

# TRANSACTIONAL GET TESTS --------------------

test_tr_get  5   "$user2"  REF.FUNC.PLAY          "property not transactional"
test_tr_get  5   "$user3"  REF.FUNC.PLAY          "property not transactional"

test_tr_get  5   "$user2"  REF.PULSE.REF.VALUE    8
test_tr_get  5   "$user3"  REF.PULSE.REF.VALUE    "transaction not in progress"

test_tr_get  99  "$user2"  REF.FUNC.PLAY          "property not transactional"
test_tr_get  99  "$user3"  REF.FUNC.PLAY          "property not transactional"

test_tr_get  99  "$user2"  REF.PULSE.REF.VALUE    "incorrect transaction ID"
test_tr_get  99  "$user3"  REF.PULSE.REF.VALUE    "transaction not in progress"

# ROLLBACK to clean up ------------

test_set         ""        TRANSACTION.ROLLBACK   5           ""

# NON-TRANSACTIONAL SUB TESTS --------------------

test_sub         "$user2"  REF.FUNC.PLAY          ENABLED
test_sub         "$user3"  REF.FUNC.PLAY          ENABLED

test_sub         "$user2"  REF.PULSE.REF.VALUE    2
test_sub         "$user3"  REF.PULSE.REF.VALUE    6

# TRANSACTIONAL SUB TESTS --------------------

# test_tr_get  5   "$user1"  REF.FUNC.PLAY          "transactional subscriptions forbidden"
test_tr_sub  5   "$user2"  REF.FUNC.PLAY          "transactional subscriptions forbidden"
test_tr_sub  5   "$user3"  REF.FUNC.PLAY          "transactional subscriptions forbidden"

# test_tr_get  5   "$user1"  REF.PULSE.REF.VALUE    "transactional subscriptions forbidden"
test_tr_sub  5   "$user2"  REF.PULSE.REF.VALUE    "transactional subscriptions forbidden"
test_tr_sub  5   "$user3"  REF.PULSE.REF.VALUE    "transactional subscriptions forbidden"

# Non-PPM settings

test_set         ""        DEVICE.PPM            DISABLED   ""

# ROLLBACK to start cleanly ------------

test_set         ""        TRANSACTION.ROLLBACK  5          ""
test_set         "$user1"  REF.PULSE.REF.VALUE   1          "SET"

# TRANSACTION.ID tests ------------------

test_set         "$user1"  TRANSACTION.ID        -1         "invalid transaction ID"
test_set         "$user1"  TRANSACTION.ID        0          "invalid transaction ID"
test_set         "$user1"  TRANSACTION.ID        65536      "invalid transaction ID"
test_set         "$user1"  TRANSACTION.ID        5          "SET"
test_set         "$user1"  TRANSACTION.ID        5          "transaction in progress"

# NON-TRANSACTIONAL SET TESTS --------------------

test_set         "$user1"  REF.FUNC.PLAY          ENABLED    "SET"
test_set         "$user1"  REF.PULSE.REF.VALUE    4          "transaction in progress"

# TRANSACTIONAL SET TESTS --------------------

test_tr_set  5   "$user1"  REF.FUNC.PLAY          ENABLED    "property not transactional"
test_tr_set  5   "$user1"  REF.PULSE.REF.VALUE    7          "SET"
test_tr_set  99  "$user1"  REF.FUNC.PLAY          ENABLED    "property not transactional"
test_tr_set  99  "$user1"  REF.PULSE.REF.VALUE    10         "incorrect transaction ID"

# NON-TRANSACTIONAL GET TESTS --------------------

test_get         "$user1"  REF.FUNC.PLAY          ENABLED
test_get         "$user1"  REF.PULSE.REF.VALUE    7
test_tr_get  5   "$user1"  REF.FUNC.PLAY          "property not transactional"
test_tr_get  5   "$user1"  REF.PULSE.REF.VALUE    7
test_tr_get  99  "$user1"  REF.FUNC.PLAY          "property not transactional"
test_tr_get  99  "$user1"  REF.PULSE.REF.VALUE    "incorrect transaction ID"

# ROLLBACK to clean up ------------

test_set         ""        TRANSACTION.ROLLBACK   5           ""

test_set         ""        DEVICE.PPM             ENABLED     ""

# Test REF.PULSE.REF_LOCAL, which always use the non-PPM user

test_set         ""        MODE.PC                BLOCKING   "SET"

test_set         "$user1"  TRANSACTION.ID         5          "SET"
test_tr_set  5   ""        REF.PULSE.REF_LOCAL    1          ""
test_get         ""        TRANSACTION.STATE                 NOT_TESTED

test_set         ""        TRANSACTION.TEST       5          "SET"
test_get         ""        TRANSACTION.STATE                 TEST_OK

test_set         ""        TRANSACTION.COMMIT     5          "SET"
test_get         ""        TRANSACTION.STATE                 NONE

test_set         ""        MODE.PC                OFF        "SET"

# Set transactional properties before transaction starts -----------------

test_set         "$user2"  REF.PULSE.REF.VALUE        3              "SET"
test_set         "$user3"  REF.PULSE.REF.VALUE        4              "SET"

# Start transaction 5 --------------

test_set         "$user2"  TRANSACTION.ID             5              "SET"
test_get         "$user2"  TRANSACTION.ID             5
test_get         "$user2"  TRANSACTION.STATE          NOT_TESTED

test_set         "$user3"  TRANSACTION.ID             5              "SET"
test_get         "$user3"  TRANSACTION.ID             5
test_get         "$user3"  TRANSACTION.STATE          NOT_TESTED

# Try a commit when both users are NOT_TESTED ------------

test_set         ""        TRANSACTION.COMMIT         5              "transaction commit failed"

test_get         "$user2"  TRANSACTION.ID             0
test_get         "$user2"  TRANSACTION.STATE          COMMIT_FAILED

test_get         "$user3"  TRANSACTION.ID             0
test_get         "$user3"  TRANSACTION.STATE          COMMIT_FAILED

# Check acknowledging the COMMIT_FAILED states ------------

test_set         "$user2"  TRANSACTION.ACK_COMMIT_FAIL ""            "SET"

test_get         "$user2"  TRANSACTION.STATE          NONE
test_get         "$user3"  TRANSACTION.STATE          COMMIT_FAILED

test_set         "$user3"  TRANSACTION.ACK_COMMIT_FAIL ""            "SET"

test_get         "$user2"  TRANSACTION.STATE          NONE
test_get         "$user3"  TRANSACTION.STATE          NONE

# Start transaction 5 again with non-PPM and a PPM user --------------

test_set         "$user2"   TRANSACTION.ID             5              "SET"
test_set         "$user3"   TRANSACTION.ID             5              "SET"

# Set and read back transactional properties  -----------------

test_tr_set  5   "$user2"   REF.PULSE.REF.VALUE        13             "SET"
test_get         "$user2"   REF.PULSE.REF.VALUE        13
test_tr_get  5   "$user2"   REF.PULSE.REF.VALUE        13

test_tr_set  5   "$user3"   REF.PULSE.REF.VALUE        14             "SET"
test_get         "$user3"   REF.PULSE.REF.VALUE        14
test_tr_get  5   "$user3"   REF.PULSE.REF.VALUE        14

# Rollback and check that old properties are restored -----------------

test_set         ""         TRANSACTION.ROLLBACK       5              ""

test_get         "$user2"   TRANSACTION.ID             0
test_get         "$user2"   TRANSACTION.STATE          NONE
test_get         "$user3"   TRANSACTION.ID             0
test_get         "$user3"   TRANSACTION.STATE          NONE

test_get         "$user2"   REF.PULSE.REF.VALUE        3
test_get         "$user3"   REF.PULSE.REF.VALUE        4

# Restart transaction and prepare an out of limits PULSE -----------------

test_set         "$user2"   TRANSACTION.ID             5              "SET"
test_set         "$user3"   TRANSACTION.ID             5              "SET"

test_tr_set  5   "$user2"   REF.PULSE.REF.VALUE        1000           "SET"
test_tr_set  5   "$user3"   REF.PULSE.REF.VALUE        1000           "SET"

test_set         ""         TRANSACTION.TEST           5              "out of limits"
test_get         "$user2"   TRANSACTION.STATE          TEST_FAILED
test_get         "$user3"   TRANSACTION.STATE          NOT_TESTED

# Correct the PULSE for user2 and try again - should fail again with out of limits because of user3

test_tr_set  5   "$user2"   REF.PULSE.REF.VALUE        13             "SET"

test_set         ""         TRANSACTION.TEST           5              "out of limits"
test_get         "$user2"   TRANSACTION.STATE          TEST_OK
test_get         "$user3"   TRANSACTION.STATE          TEST_FAILED

# Load a valid PULSE and test again -------------

test_tr_set  5   "$user3"   REF.PULSE.REF.VALUE        14             "SET"
test_get         "$user3"   TRANSACTION.STATE          NOT_TESTED

test_set         ""         TRANSACTION.TEST           5               "SET"
test_get         "$user3"   TRANSACTION.STATE          TEST_OK
test_get         "$user3"   TRANSACTION.STATE          TEST_OK

# Try to commit -------------

test_set         ""         TRANSACTION.COMMIT         5               "SET"

test_get         "$user2"   TRANSACTION.ID             0
test_get         "$user2"   TRANSACTION.STATE          NONE

test_get         "$user3"   TRANSACTION.ID             0
test_get         "$user3"   TRANSACTION.STATE          NONE

test_get         "$user2"   REF.PULSE.REF.VALUE        13
test_get         "$user3"   REF.PULSE.REF.VALUE        14


# Clean up

test_set         ""        MODE.OP               SIMULATION "SET"

exit 0


# EOF

