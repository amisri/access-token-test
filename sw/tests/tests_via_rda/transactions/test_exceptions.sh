#! /usr/bin/bash

# test_exceptions.sh

# Set arguments vars and include rda test functions

nargs=4
arglist="device accelerator user1 user2"

cd `dirname $0`

source ../test_via_rda.sh

#   test_set             "$user"  $property  $value  $exception
#   test_tr_set  $tr_id  "$user"  $property  $value  $exception
#
#   test_get             "$user"  $property  $exception_or_value
#   test_tr_get  $tr_id  "$user"  $property  $exception_or_value
#
#   test_sub             "$user"  $property  $exception_or_value
#   test_tr_sub  $tr_id  "$user"  $property  $exception_or_value

# $user must include "-c " prefix or can be empty "" if no user is specfied

device=$1
accelerator=$2
user1="-c $accelerator.USER.ALL"
user2="-c $accelerator.USER.$3"
user3="-c $accelerator.USER.$4"

# Succesfully ARM a function to properly ROLLBACK TRIM parameters

test_set         ""        MODE.OP                SIMULATION "SET"
test_set         ""        MODE.PC                OFF        "SET"
test_get         ""        STATE.PC               OFF

test_set         ""        REG.MODE               V          "SET"
test_set         ""        REG.MODE_CYC           V          "SET"

test_set         ""        MODE.PC                IDLE       "SET"
sleep 6
test_get         ""        STATE.PC               IDLE

test_set         "$user1"  REF.TRIM.INITIAL       0          "SET"
test_set         "$user1"  REF.TRIM.FINAL         1          "SET"
test_set         "$user1"  REF.TRIM.DURATION      1
test_set         "$user1"  REF.FUNC.TYPE          CTRIM      "SET"

test_set         "$user2"  REF.TRIM.INITIAL       0          "SET"
test_set         "$user2"  REF.TRIM.FINAL         1          "SET"
test_set         "$user2"  REF.TRIM.DURATION      2
test_set         "$user2"  REF.FUNC.TYPE          CTRIM      "SET"

test_set         ""        MODE.PC                OFF        "SET"
sleep 6
test_get         ""        STATE.PC               OFF

test_set         ""        REG.MODE               I          "SET"
test_set         ""        REG.MODE_CYC           I          "SET"

# ROLLBACK to start cleanly ------------

test_set         ""        TRANSACTION.ROLLBACK  5          ""

test_set         "$user1"  REF.TRIM.DURATION      1          "SET"
test_set         "$user2"  REF.TRIM.DURATION      2          "SET"
test_set         "$user3"  REF.TRIM.DURATION      3          "SET"

# TRANSACTION.ID tests ------------------

test_set         "$user1"  TRANSACTION.ID        -1         "invalid transaction ID"
test_set         "$user1"  TRANSACTION.ID        0          "invalid transaction ID"
test_set         "$user1"  TRANSACTION.ID        65536      "invalid transaction ID"
test_set         "$user1"  TRANSACTION.ID        5          "SET"
test_set         "$user1"  TRANSACTION.ID        5          "transaction in progress"

test_set         "$user2"  TRANSACTION.ID        -1         "invalid transaction ID"
test_set         "$user2"  TRANSACTION.ID        0          "invalid transaction ID"
test_set         "$user2"  TRANSACTION.ID        65536      "invalid transaction ID"
test_set         "$user2"  TRANSACTION.ID        5          "SET"
test_set         "$user2"  TRANSACTION.ID        5          "transaction in progress"


# NON-TRANSACTIONAL SET TESTS --------------------

test_set         "$user1"  REF.FUNC.PLAY          ENABLED    "SET"
test_set         "$user2"  REF.FUNC.PLAY          ENABLED    "SET"
test_set         "$user3"  REF.FUNC.PLAY          ENABLED    "SET"

test_set         "$user1"  REF.TRIM.DURATION      4          "transaction in progress"
test_set         "$user2"  REF.TRIM.DURATION      5          "transaction in progress"
test_set         "$user3"  REF.TRIM.DURATION      6          "SET"

# TRANSACTIONAL SET TESTS --------------------

test_tr_set  5   "$user1"  REF.FUNC.PLAY          ENABLED    "property not transactional"
test_tr_set  5   "$user2"  REF.FUNC.PLAY          ENABLED    "property not transactional"
test_tr_set  5   "$user3"  REF.FUNC.PLAY          ENABLED    "property not transactional"

test_tr_set  5   "$user1"  REF.TRIM.DURATION      7          "SET"
test_tr_set  5   "$user2"  REF.TRIM.DURATION      8          "SET"
test_tr_set  5   "$user3"  REF.TRIM.DURATION      9          "transaction not in progress"

test_tr_set  99  "$user1"  REF.FUNC.PLAY          ENABLED    "property not transactional"
test_tr_set  99  "$user2"  REF.FUNC.PLAY          ENABLED    "property not transactional"
test_tr_set  99  "$user3"  REF.FUNC.PLAY          ENABLED    "property not transactional"

test_tr_set  99  "$user1"  REF.TRIM.DURATION      10         "incorrect transaction ID"
test_tr_set  99  "$user2"  REF.TRIM.DURATION      11         "incorrect transaction ID"
test_tr_set  99  "$user3"  REF.TRIM.DURATION      12         "transaction not in progress"

# NON-TRANSACTIONAL GET TESTS --------------------

test_get         "$user1"  REF.FUNC.PLAY          ENABLED
test_get         "$user2"  REF.FUNC.PLAY          ENABLED
test_get         "$user3"  REF.FUNC.PLAY          ENABLED

test_get         "$user1"  REF.TRIM.DURATION      7
test_get         "$user2"  REF.TRIM.DURATION      8
test_get         "$user3"  REF.TRIM.DURATION      6

# TRANSACTIONAL GET TESTS --------------------

test_tr_get  5   "$user1"  REF.FUNC.PLAY          "property not transactional"
test_tr_get  5   "$user2"  REF.FUNC.PLAY          "property not transactional"
test_tr_get  5   "$user3"  REF.FUNC.PLAY          "property not transactional"

test_tr_get  5   "$user1"  REF.TRIM.DURATION      7
test_tr_get  5   "$user2"  REF.TRIM.DURATION      8
test_tr_get  5   "$user3"  REF.TRIM.DURATION      "transaction not in progress"

test_tr_get  99  "$user1"  REF.FUNC.PLAY          "property not transactional"
test_tr_get  99  "$user2"  REF.FUNC.PLAY          "property not transactional"
test_tr_get  99  "$user3"  REF.FUNC.PLAY          "property not transactional"

test_tr_get  99  "$user1"  REF.TRIM.DURATION      "incorrect transaction ID"
test_tr_get  99  "$user2"  REF.TRIM.DURATION      "incorrect transaction ID"
test_tr_get  99  "$user3"  REF.TRIM.DURATION      "transaction not in progress"

# ROLLBACK to clean up ------------

test_set         ""        TRANSACTION.ROLLBACK   5           ""

# NON-TRANSACTIONAL SUB TESTS --------------------

test_sub         "$user2"  REF.FUNC.PLAY          ENABLED
test_sub         "$user3"  REF.FUNC.PLAY          ENABLED

test_sub         "$user2"  REF.TRIM.DURATION      2
test_sub         "$user3"  REF.TRIM.DURATION      6

# TRANSACTIONAL SUB TESTS --------------------

# test_tr_get  5   "$user1"  REF.FUNC.PLAY          "transactional subscriptions forbidden"
test_tr_sub  5   "$user2"  REF.FUNC.PLAY          "transactional subscriptions forbidden"
test_tr_sub  5   "$user3"  REF.FUNC.PLAY          "transactional subscriptions forbidden"

# test_tr_get  5   "$user1"  REF.TRIM.DURATION      "transactional subscriptions forbidden"
test_tr_sub  5   "$user2"  REF.TRIM.DURATION      "transactional subscriptions forbidden"
test_tr_sub  5   "$user3"  REF.TRIM.DURATION      "transactional subscriptions forbidden"

# EOF

