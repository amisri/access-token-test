#! /usr/bin/bash

# test_commit.sh

# Set arguments vars and include rda test functions

nargs=4
arglist="gateway fgc1 fgc2 fgc3"

cd `dirname $0`

source ../test_via_rda.sh

#   test_set             "$user"  $property  $value  $exception
#   test_tr_set  $tr_id  "$user"  $property  $value  $exception
#
#   test_get             "$user"  $property  $exception_or_value
#   test_tr_get  $tr_id  "$user"  $property  $exception_or_value
#
#   test_sub             "$user"  $property  $exception_or_value
#   test_tr_sub  $tr_id  "$user"  $property  $exception_or_value

# $user must include "-c " prefix or can be empty "" if no user is specfied

gateway=$1
fgc1=$2
fgc2=$3
fgc3=$4
device=$1

# ZERO is user 1 

user="-c SPS.USER.ZERO"


table1="<0|0,1|1,2|0>"

test_commit()
{
    device=$1

    # ROLLBACK to start cleanly ------------

    test_set         ""        TRANSACTION.ROLLBACK       5              ""
    test_set         "$user"   REF.FUNC.TYPE              NONE           "SET"
    test_get         ""        STATE.PC                   IDLE

    # Start transaction 5 with non-PPM and a PPM user --------------

    test_set         ""        TRANSACTION.ID             5              "SET"
    test_get         ""        TRANSACTION.ID             5
    test_get         ""        TRANSACTION.STATE          NOT_TESTED

    test_set         "$user"   TRANSACTION.ID             5              "SET"
    test_get         "$user"   TRANSACTION.ID             5
    test_get         "$user"   TRANSACTION.STATE          NOT_TESTED

    # Set and read back transactional non-PPM properties  -----------------

    test_tr_set  5   ""        REF.TABLE.FUNC.VALUE       "$table1"      "SET"
    test_tr_get  5   ""        REF.TABLE.FUNC.VALUE       "$table1"

    # Set and read back transactional PPM properties  -----------------

    test_tr_set  5   "$user"   REF.TABLE.FUNC.VALUE       "$table1"      "SET"
    test_tr_get  5   "$user"   REF.TABLE.FUNC.VALUE       "$table1"

    # Execute a transaction test - this should arm the non-PPM function -------------

    test_set         ""        TRANSACTION.TEST           5               "SET"
    test_get         ""        TRANSACTION.STATE          TEST_OK
    test_get         "$user"   TRANSACTION.STATE          TEST_OK
    test_get         ""        STATE.PC                   ARMED
}

check_commit()
{
    device=$1

    # Check that commit event was processed

    test_get         ""        STATE.PC                   RUNNING

    test_get         ""        TRANSACTION.ID             0
    test_get         ""        TRANSACTION.STATE          NONE

    test_get         "$user"   TRANSACTION.ID             0
    test_get         "$user"   TRANSACTION.STATE          NONE
    test_get         "$user"   REF.FUNC.TYPE              TABLE

}

# Prepare transaction on three FGCs

test_commit $fgc1
test_commit $fgc2
test_commit $fgc3

# Test commit event through the gateway device

device=$gateway

test_set         ""        TRANSACTION.COMMIT_TEST         5          "SET"
sleep 1

# Check that commit was processed

check_commit $fgc1
check_commit $fgc2
check_commit $fgc3

exit 0

# EOF
