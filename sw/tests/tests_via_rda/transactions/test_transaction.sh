#! /usr/bin/bash

# test_transaction.sh

# Set arguments vars and include rda test functions

nargs=3
arglist="device accelerator user"

cd `dirname $0`

source ../test_via_rda.sh

#   test_set             "$user"  $property  $value  $exception
#   test_tr_set  $tr_id  "$user"  $property  $value  $exception
#
#   test_get             "$user"  $property  $exception_or_value
#   test_tr_get  $tr_id  "$user"  $property  $exception_or_value
#
#   test_sub             "$user"  $property  $exception_or_value
#   test_tr_sub  $tr_id  "$user"  $property  $exception_or_value

# $user must include "-c " prefix or can be empty "" if no user is specfied

device=$1
accelerator=$2
user="-c $accelerator.USER.$3"

table1="<0|55,1|56>"
table2="<0|57,0.5|58,1|56>"
table3="<0|57,0.5|100000,1|56>"
table4="<0|57,0.5|59,1|58>"

# Succesfully ARM a function to properly ROLLBACK PLEP parameters

test_set         ""        MODE.OP                    SIMULATION     "SET"
test_set         ""        MODE.PC                    OFF            "SET"
test_get         ""        STATE.PC                   OFF

test_set         ""        TRANSACTION.ROLLBACK       5              ""

test_set         ""        MODE.PC                    IDLE           "SET"
sleep 6
test_get         ""        STATE.PC                   IDLE

test_set         ""        REF.PLEP.FINAL             50             "SET"
test_set         ""        REF.PLEP.ACCELERATION      51             "SET"
test_set         ""        REF.PLEP.LINEAR_RATE       52             "SET"
test_set         ""        REF.FUNC.TYPE              PLEP           "SET"

test_set         "$user"   REF.PLEP.INITIAL           49             "SET"
test_set         "$user"   REF.PLEP.FINAL             50             "SET"
test_set         "$user"   REF.PLEP.ACCELERATION      51             "SET"
test_set         "$user"   REF.PLEP.LINEAR_RATE       52             "SET"
test_set         "$user"   REF.FUNC.TYPE              PLEP           "SET"

# Start simulated converter

test_set         ""        MODE.PC                    OFF            "SET"
sleep 6
test_get         ""        STATE.PC                   OFF
test_set         ""        MODE.PC                    ON_STANDBY     "SET"

# ROLLBACK to start cleanly ------------

test_set         ""        TRANSACTION.ROLLBACK       5              ""

# Set transactional properties before transaction starts -----------------

test_set         ""        REF.PLEP.FINAL             50             "SET"
test_set         ""        REF.PLEP.ACCELERATION      51             "SET"
test_set         ""        REF.PLEP.LINEAR_RATE       52             "SET"
test_get         ""        REF.PLEP.FINAL             50

# Set table and arm it -----------------

test_set         "$user"   REF.TABLE.FUNC.VALUE       "$table1"      "SET"
test_get         "$user"   REF.TABLE.FUNC.VALUE       "$table1"
test_set         "$user"   REF.FUNC.TYPE              TABLE
test_get         "$user"   REF.FUNC.TYPE              TABLE

# Start transaction 5 with non-PPM and a PPM user --------------

test_set         ""        TRANSACTION.ID             5              "SET"
test_get         ""        TRANSACTION.ID             5
test_get         ""        TRANSACTION.STATE          NOT_TESTED

test_set         "$user"   TRANSACTION.ID             5              "SET"
test_get         "$user"   TRANSACTION.ID             5
test_get         "$user"   TRANSACTION.STATE          NOT_TESTED

# Try a commit when both users are NOT_TESTED ------------

test_set         ""        TRANSACTION.COMMIT         5               "transaction commit failed"

test_get         ""        TRANSACTION.ID             0
test_get         ""        TRANSACTION.STATE          COMMIT_FAILED

test_get         "$user"   TRANSACTION.ID             0
test_get         "$user"   TRANSACTION.STATE          COMMIT_FAILED

# Check acknowledging the COMMIT_FAILED states ------------

test_set         ""        TRANSACTION.ACK_COMMIT_FAIL ""            "SET"

test_get         ""        TRANSACTION.STATE          NONE
test_get         "$user"   TRANSACTION.STATE          COMMIT_FAILED

test_set         "$user"   TRANSACTION.ACK_COMMIT_FAIL ""            "SET"

test_get         ""        TRANSACTION.STATE          NONE
test_get         "$user"   TRANSACTION.STATE          NONE

# Start transaction 5 again with non-PPM and a PPM user --------------

test_set         ""        TRANSACTION.ID             5              "SET"
test_set         "$user"   TRANSACTION.ID             5              "SET"

# Set and read back transactional non-PPM properties  -----------------

test_tr_set  5   ""        REF.PLEP.FINAL             55             "SET"
test_tr_set  5   ""        REF.PLEP.ACCELERATION      61             "SET"
test_tr_set  5   ""        REF.PLEP.LINEAR_RATE       62             "SET"

test_get         ""        REF.PLEP.FINAL             55
test_tr_get  5   ""        REF.PLEP.FINAL             55

# Set and read back transactional PPM properties  -----------------

test_tr_set  5   "$user"   REF.TABLE.FUNC.VALUE       "$table2"      "SET"
test_tr_get  5   "$user"   REF.TABLE.FUNC.VALUE       "$table2"

# Rollback and check that old properties are restored -----------------

test_set         ""        TRANSACTION.ROLLBACK       5              ""

test_get         ""        TRANSACTION.ID             0
test_get         ""        TRANSACTION.STATE          NONE
test_get         "$user"   TRANSACTION.ID             0
test_get         "$user"   TRANSACTION.STATE          NONE

test_get         ""        REF.PLEP.FINAL             50
test_get         ""        REF.PLEP.ACCELERATION      51
test_get         ""        REF.PLEP.LINEAR_RATE       52

test_get         "$user"   REF.TABLE.FUNC.VALUE       "$table1"

# Restart transaction and prepare an out of limits PLEP and TABLE -----------------

test_set         ""        TRANSACTION.ID             5              "SET"
test_set         "$user"   TRANSACTION.ID             5              "SET"

test_tr_set  5   ""        REF.PLEP.FINAL             100000         "SET"
test_tr_set  5   ""        REF.PLEP.ACCELERATION      61             "SET"
test_tr_set  5   ""        REF.PLEP.LINEAR_RATE       62             "SET"

test_tr_set  5   "$user"   REF.TABLE.FUNC.VALUE       "$table3"      "SET"

# Test commit from STANDBY should fail with bad state -------------

test_set         ""        TRANSACTION.TEST           5              "bad state"
test_get         ""        TRANSACTION.STATE          TEST_FAILED
test_get         "$user"   TRANSACTION.STATE          NOT_TESTED

# Change to IDLE and try again - the PLEP should be out of limits -------------

test_set         ""        MODE.PC                    IDLE           "SET"

sleep 7

test_get         ""        STATE.PC                   IDLE

test_set         ""        TRANSACTION.TEST           5              "out of limits"
test_get         ""        TRANSACTION.STATE          TEST_FAILED
test_get         "$user"   TRANSACTION.STATE          NOT_TESTED

# Correct the PLEP and try again - should fail again with out of limits because of the TABLE -------------

test_tr_set  5   ""        REF.PLEP.FINAL             55              "SET"

test_get         ""        TRANSACTION.STATE          NOT_TESTED

test_set         ""        TRANSACTION.TEST           5               "out of limits"
test_get         ""        TRANSACTION.STATE          TEST_OK
test_get         "$user"   TRANSACTION.STATE          TEST_FAILED
test_get         ""        STATE.PC                   ARMED

# Change PLEP value - this should disarm the non-PPP reference (STATE.PC back to IDLE) -------------

test_tr_set  5   ""        REF.PLEP.FINAL             56              "SET"
test_get         ""        TRANSACTION.STATE          NOT_TESTED
test_get         "$user"   TRANSACTION.STATE          TEST_FAILED
test_get         ""        STATE.PC                   IDLE

# Load a valid TABLE and test again -------------

test_tr_set  5   "$user"   REF.TABLE.FUNC.VALUE       "$table2"       "SET"
test_get         "$user"   TRANSACTION.STATE          NOT_TESTED

test_set         ""        TRANSACTION.TEST           5               "SET"
test_get         ""        TRANSACTION.STATE          TEST_OK
test_get         "$user"   TRANSACTION.STATE          TEST_OK
test_get         ""        STATE.PC                   ARMED

test_get         ""        REF.FUNC.TYPE              PLEP
test_get         "$user"   REF.FUNC.TYPE              TABLE
test_get         "$user"   REF.TABLE.FUNC.VALUE       "$table2"

# Try to commit -------------

test_set         ""        TRANSACTION.COMMIT         5               "SET"

test_get         ""        TRANSACTION.ID             0
test_get         ""        TRANSACTION.STATE          NONE

test_get         "$user"   TRANSACTION.ID             0
test_get         "$user"   TRANSACTION.STATE          NONE

test_get         ""        REF.PLEP.FINAL             56
test_get         ""        REF.PLEP.ACCELERATION      61
test_get         ""        REF.PLEP.LINEAR_RATE       62

test_get         "$user"   REF.FUNC.TYPE              TABLE
test_get         "$user"   REF.TABLE.FUNC.VALUE       "$table2"

sleep 5

test_get         ""        STATE.PC                   IDLE
test_get         ""        REF.FUNC.TYPE              NONE

# Prepare another transaction -------------

test_set         ""        TRANSACTION.ID             5              "SET"
test_set         "$user"   TRANSACTION.ID             5              "SET"

test_tr_set  5   ""        REF.PLEP.FINAL             55             "SET"
test_tr_set  5   "$user"   REF.TABLE.FUNC.VALUE       "$table4"      "SET"

test_set         ""        TRANSACTION.TEST           5              "SET"
test_get         ""        TRANSACTION.STATE          TEST_OK
test_get         "$user"   TRANSACTION.STATE          TEST_OK
test_get         ""        STATE.PC                   ARMED

test_get         ""        REF.FUNC.TYPE              PLEP
test_get         "$user"   REF.FUNC.TYPE              TABLE
test_get         "$user"   REF.TABLE.FUNC.VALUE       "$table4"

# Change state from ARMED to ON_STANDBY to cause the commit to fail for the non-PPM user only ------------

test_set         ""        MODE.PC                    ON_STANDBY     "SET"

sleep 3

test_get         ""        STATE.PC                   ON_STANDBY
test_get         ""        REF.FUNC.TYPE              PLEP

test_set         ""        TRANSACTION.COMMIT         5               "transaction commit failed"

test_get         ""        TRANSACTION.ID             0
test_get         ""        TRANSACTION.STATE          COMMIT_FAILED

test_get         "$user"   TRANSACTION.ID             0
test_get         "$user"   TRANSACTION.STATE          NONE

test_get         "$user"   REF.FUNC.TYPE              TABLE
test_get         "$user"   REF.TABLE.FUNC.VALUE       "$table4"

test_set         ""        MODE.PC                    OFF             "SET"

exit 0

# EOF
