/*---------------------------------------------------------------------------------------------------------*\
  File:         isr.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ISR_H   // header encapsulation
#define ISR_H

#ifdef ISR_GLOBALS
    #define ISR_VARS_EXT
#else
    #define ISR_VARS_EXT extern
#endif

#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)

//-----------------------------------------------------------------------------------------------------------

struct TIsr
{
    BOOLEAN                     busy_signaled_by_tx_queue;
    BOOLEAN                     is_slow_command;
    BOOLEAN                     cmd_in_progress;
    INT16U                      mcu_isr;
    INT16U                      tx_isr;
    INT16U                      dallas_isr;
};

//-----------------------------------------------------------------------------------------------------------

INT8U   UnknownCmd              (INT8U arg);
INT8U   SlowCmd                 (INT8U arg);
void    IsrTermTx               (void);
void    IsrDallasRx             (void);
void    IsrByteFromMcu          (void);

//-----------------------------------------------------------------------------------------------------------

ISR_VARS_EXT struct TIsr        isr;

//-----------------------------------------------------------------------------------------------------------

#endif  // ISR_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.h
\*---------------------------------------------------------------------------------------------------------*/

