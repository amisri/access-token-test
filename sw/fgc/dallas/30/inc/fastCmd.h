/*---------------------------------------------------------------------------------------------------------*\
  File:         fastCmd.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FASTCMD_H       // header encapsulation
#define FASTCMD_H

#ifdef FASTCMD_GLOBALS
    #define FASTCMD_VARS_EXT
#else
    #define FASTCMD_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)

//-----------------------------------------------------------------------------------------------------------

INT8U   FastCmdSetBigEndian     (INT8U arg);
INT8U   FastCmdSetLittleEndian  (INT8U arg);
INT8U   FastCmdGetResponse      (INT8U arg);
INT8U   FastCmdSetAddress       (INT8U arg);
INT8U   FastCmdSetData          (INT8U arg);
INT8U   FastCmdSetBlockMask     (INT8U arg);
INT8U   FastCmdSetCrcLen        (INT8U arg);
INT8U   FastCmdSetDebugLevel    (INT8U arg);
void FastCmdSetGetBlockMaskFuncC62A(void);
void FastCmdSetGetBlockMaskFuncC62P(void);

//-----------------------------------------------------------------------------------------------------------

#endif  // FASTCMD_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: fastCmd.h
\*---------------------------------------------------------------------------------------------------------*/
