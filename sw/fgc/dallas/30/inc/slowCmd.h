/*---------------------------------------------------------------------------------------------------------*\
  File:         slowCmd.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef SLOWCMD_H       // header encapsulation
#define SLOWCMD_H

#ifdef SLOWCMD_GLOBALS
    #define SLOWCMD_VARS_EXT
#else
    #define SLOWCMD_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)

//-----------------------------------------------------------------------------------------------------------

#define C62_FLASHOK             0x0380  // Flash status when erase or program was successful

//-----------------------------------------------------------------------------------------------------------

INT16U  BgTsk                   (void);
void    UnknownSlowCmd          (void);
void    SlowCmdSwitchProg       (void);
void    SlowCmdEraseFlash       (void);
void    SlowCmdProgFlash        (void);
void    SlowCmdCalcCrc          (void);

//-----------------------------------------------------------------------------------------------------------

#endif  // SLOWCMD_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: slowCmd.h
\*---------------------------------------------------------------------------------------------------------*/

