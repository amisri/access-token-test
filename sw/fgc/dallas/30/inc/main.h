/*---------------------------------------------------------------------------------------------------------*\
  File:         main.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MAIN_H  // header encapsulation
#define MAIN_H

#ifdef MAIN_GLOBALS
    #define MAIN_VARS_EXT
#else
    #define MAIN_VARS_EXT extern
#endif

#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)
#include <m16c62_link.h>        // for union M16C62linkResponseArea, for struct mcu_link_cmd

//-----------------------------------------------------------------------------------------------------------

#define C62BOOT_VERSION         4               // Boot version

//-----------------------------------------------------------------------------------------------------------

struct data_exchange
{
    INT8U FAR *                         memoryPtr;              // Pointer to next response data character
    volatile INT16U                     rsp_bytes_in_buffer;
    union M16C62linkResponseArea        rsp;                    // Response buffer
    BOOLEAN                             has_error;              // Error flag
    INT8U                               branch_idx;             // Selected branch index
};

// ----- CRC len data -----

union crc
{
    INT8U       bytes[4];               // CRC calculation length in separate bytes
    INT32U      long_word;              // CRC calculation length as a long word (little endian)
};

struct THardware
{
    enum M16C62_hardware        model;
//  INT16U                      n_branches;     // not used by the boot
    INT8U NEAR *                mcu_link_tx;    // port used to transmit to MCU
    INT8U NEAR *                mcu_link_rx;    // port used to receive from MCU
//  INT8U NEAR *                id_bus_tx;      // UART used for the ID bus
//  union uartc_def NEAR *      id_bus_tx_ctrl;
    INT8U NEAR *                term_tx;        // UART used for the TERMINAL
    BOOLEAN                     mcu_transmit_in_big_endian;
};

//-----------------------------------------------------------------------------------------------------------

void    main    (void);

//-----------------------------------------------------------------------------------------------------------

MAIN_VARS_EXT struct mcu_link_cmd       cmd;            // Command data
MAIN_VARS_EXT struct data_exchange      data_exchange;  // Response data
MAIN_VARS_EXT union address             address;

MAIN_VARS_EXT union crc                 crc;            // CRC len data

MAIN_VARS_EXT struct THardware          hardware;       // specificities for each hardware
//-----------------------------------------------------------------------------------------------------------

#endif  // MAIN_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
