/*---------------------------------------------------------------------------------------------------------*\
  File:         flash.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FLASH_H // header encapsulation
#define FLASH_H

#ifdef FLASH_GLOBALS
    #define FLASH_VARS_EXT
#else
    #define FLASH_VARS_EXT extern
#endif


#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)
#include <string.h>             // String library (required for memcpy)


#define FLASH_C62_TYPES			2
#define FLASH_BLOCKS_NUMBER		13
#define WAIT_MCU_SCAN           1000            //  1ms, theorically MCU polls at 1KHz

//-----------------------------------------------------------------------------------------------------------


typedef enum c62_flash_type
{
	FLASH_M16C62_P = 0x02,
	FLASH_M16C62_A = 0x03

} c62_flash_type_t;



struct TFlash_block_info
{
    INT32U              start;                  // first address of the block
    INT32U              end;                    // last word of the block (even number) where the flash state machine resides
    BOOLEAN             erasable;               // if we allow to erase this block
};



struct c62_flash
{
    INT32U              write_address;          // Start of sector address
    INT16U              block_mask;             // Flash erase block mask
    INT8U               buf[256];               // Sector buffer
    c62_flash_type_t 	type;
    INT8U				block_idx;
};



void    FlashCpuInit            (void);
void    FlashCpuRestore         (void);
void 	FlashTransfer			(void);
INT16U  FlashErase              (INT32U addr);
INT16U  FlashWrite              (void);
void    FlashSetFlashTransferC62A(void);
void    FlashSetFlashTransferC62P(void);



FLASH_VARS_EXT struct c62_flash                 flash;          // Flash related variables

FLASH_VARS_EXT const struct TFlash_block_info   flash_block_info[FLASH_C62_TYPES][FLASH_BLOCKS_NUMBER]
#ifdef FLASH_GLOBALS
=
{
// Block 0 not allowed to be erased as we decided to flash it only via the Renesas Boot Loader
// and it is the block where this program runs from

// the end is the last word of the block (even number) where the flash state machine resides
	{
		{C62P_BLK0_ADDR,  0xFFFFE, FALSE},
		{C62P_BLK1_ADDR,  0xFEFFE, FALSE},
		{C62P_BLK2_ADDR,  0xFDFFE, FALSE},
		{C62P_BLK3_ADDR,  0xFBFFE, TRUE},
		{C62P_BLK4_ADDR,  0xF9FFE, TRUE},
		{C62P_BLK5_ADDR,  0xF7FFE, TRUE},
		{C62P_BLK6_ADDR,  0xEFFFE, TRUE},
		{C62P_BLK7_ADDR,  0xDFFFE, TRUE},
		{C62P_BLK8_ADDR,  0xCFFFE, TRUE},
		{C62P_BLK9_ADDR,  0xBFFFE, TRUE},
		{C62P_BLK10_ADDR, 0xAFFFE, TRUE},
		{C62P_BLK11_ADDR, 0x9FFFE, TRUE},
		{C62P_BLK12_ADDR, 0x8FFFE, TRUE}
	},
	{
		{C62_BLK0_ADDR,     0xFFFFE, FALSE},        // Block 0 : 16Kb       Fixed Vector + 30 IdBoot (or Debug Monitor)
		{C62_BLK1_ADDR,     0xFBFFE, TRUE },        // Block 1 :  8Kb       PTDB - PART TYPE database
		{C62_BLK2_ADDR,     0xF9FFE, TRUE },        // Block 2 :  8Kb       Scan results
		{C62_BLK3_ADDR,     0xF7FFE, TRUE },        // Block 3 : 32Kb       31 IdMain Prog
		{C62_BLK4_ADDR,     0xEFFFE, TRUE },        // Block 4 : 64Kb
		{C62_BLK5_ADDR,     0xDFFFE, TRUE },        // Block 5 : 64Kb
		{C62_BLK456_ADDR,   0xCFFFE, TRUE }         // Block 6 : 64Kb       IDB - Database directory and ID database
	}

}
#endif
;
//-----------------------------------------------------------------------------------------------------------

#endif  // FLASH_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: flash.h
\*---------------------------------------------------------------------------------------------------------*/
