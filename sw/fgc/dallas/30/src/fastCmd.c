/*---------------------------------------------------------------------------------------------------------*\
  File:         fastCmd.c

  Purpose:      M16C62 Boot program

  Notes:        fast response functions to MCU commands

                these are the fast commands that are executed inside the ISR

         Warning!!! fprintf() is not reentrant!
         if you plan to output debug information remember that these functions run inside the interrupt
\*---------------------------------------------------------------------------------------------------------*/

#include <fastCmd.h>
#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)
#include <iodefines.h>          // include sfr definitions
#include <flash.h>          // for c62_flash, flash_block_info[], flash global variables
#include <main.h>               // for hardware, address global variable
#include <term.h>               // for term global variable



typedef INT16U (* fast_cmd_set_block_mask_func_t )(const INT8U arg);

typedef enum fastcmd_flash_masks
{
	FASTCMD_PTDB 	= 0x02,
	FASTCMD_IDPROG 	= 0x08,
	FASTCMD_IDDB  	= 0x70
} fastcmd_flash_masks_p;


static fast_cmd_set_block_mask_func_t fast_cmd_set_block_mask;


static INT16U FastCmdGetBlockMaskC62P(const INT8U arg);


/*---------------------------------------------------------------------------------------------------------*/
INT8U FastCmdSetDebugLevel(INT8U arg)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_SET_DEBUG_LEVEL, 19
  arg: 1,  [db_lvl]

  This function will receive the debug level (0-1).

  Previously existing in the IdProg as C62_SET_DB but this one has the same value as C62_SET_CRC_LEN
  so I create a new one in both IdBoot and IdProg and later C62_SET_DB value will be used as C62_SET_CRC_LEN
  for both
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( cmd.n_args_expected == 0 )             // If start of Set Debug level command
    {
        cmd.n_args_expected = 1;                // Request 1 byte
    }
    else
    {
        if ( arg > 3 )                          // If Debug level is invalid
        {
            data_exchange.has_error = TRUE;
            return(M16C62_BAD_ARG);             // Report error
        }
        else
        {
            term.db_lvl = arg;
        }

        cmd.n_args_expected = 0;                // Command is complete - no more arguments expected
    }

    return(cmd.n_args_expected);                // Return number of arguments expected
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U FastCmdSetBigEndian(INT8U arg)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_SET_BIG_ENDIAN, 17
  arg: no
\*---------------------------------------------------------------------------------------------------------*/
{
//  cmd.n_args_expected = 0;            // No more arg expected

    hardware.mcu_transmit_in_big_endian = TRUE;

    return( hardware.mcu_transmit_in_big_endian );      // Return byte to MCU
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U FastCmdSetLittleEndian(INT8U arg)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_SET_LITTLE_ENDIAN, 18
  arg: no
\*---------------------------------------------------------------------------------------------------------*/
{
//  cmd.n_args_expected = 0;            // No more arg expected

    hardware.mcu_transmit_in_big_endian = FALSE;

    return( hardware.mcu_transmit_in_big_endian );      // Return byte to MCU
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U FastCmdGetResponse(INT8U arg)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_GET_RESPONSE, 0
  arg: no

  This function will return the next byte of the response data.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U       tmp;

    if ( data_exchange.rsp_bytes_in_buffer > 0 )
    {
        data_exchange.rsp_bytes_in_buffer--;                            // Decrement length remaining
        tmp = *(data_exchange.memoryPtr++);                     // Return byte to MCU
    }
    else
    {
        data_exchange.has_error = TRUE;
        data_exchange.rsp_bytes_in_buffer = 0;  // for safety
        data_exchange.memoryPtr = data_exchange.rsp.bytes;      // Link response pointer to response buffer
        tmp = M16C62_NO_MORE_DATA;                      // Report error
    }
    return(tmp);
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U FastCmdSetBlockMask(INT8U arg)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_SET_BLOCK_MASK, 3
  arg: 1,  [block_mask]

  This function will receive the block mask that can be used with FlashErase.
  The Flash is divided into seven blocks (0-6). Each bit of the mask represents one flash block,
  b0 represents Block 0 ... b6 represents Block 6
  b7 is not used
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U       ii;
    INT16U      temp;

    if ( cmd.n_args_expected == 0 )             // If start of Set block mask command
    {
        cmd.n_args_expected = 1;                // Request 1 byte
    }
    else                                        // else arg is block mask
    {
        arg &= 0x7F;    // clear b7 which is not used
        temp = fast_cmd_set_block_mask(arg);     // make a copy


        for ( ii = 0; temp; ii++, temp >>= 1 )
        {
            if ( (temp & 1) != 0 )                      // check if bit is active
            {
                if ( flash_block_info[flash.block_idx][ii].erasable == FALSE )
                {
                    data_exchange.has_error = TRUE;
                    return(M16C62_BAD_ARG);     // Report error
                }
            }
        }

        // Save argument as block mask
        flash.block_mask = fast_cmd_set_block_mask(arg);

        // Command is complete - no more arguments expected
        cmd.n_args_expected = 0;
    }

    return(cmd.n_args_expected);
}


static INT16U FastCmdGetBlockMaskC62A(const INT8U arg)
{
	INT16U mask;

	switch(arg)
	{

	case FASTCMD_PTDB:

		mask = 0x0002;
		break;

	case FASTCMD_IDPROG:

		mask = 0x0008;
		break;

	case FASTCMD_IDDB:

		mask = 0x0070;
		break;

	default:
		mask = 0x0000;
		break;

	}
	return mask;
}



static INT16U FastCmdGetBlockMaskC62P(const INT8U arg)
{
	INT16U mask;

	switch(arg)
	{

	case FASTCMD_PTDB:

		mask = 0x0008;
		break;

	case FASTCMD_IDPROG:

		mask = 0x0020;
		break;

	case FASTCMD_IDDB:

		mask = 0x01C0;
		break;

	default:

		mask = 0x0000;
		break;

	}
	return mask;
}


/*---------------------------------------------------------------------------------------------------------*/
INT8U FastCmdSetAddress(INT8U arg)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_SET_ADDRESS, 1
  arg: 3 [addr_h, addr_m, addr_l] from FGC2
         [addr_l, addr_m, addr_h] from FGC3

  This function will receive the address as three bytes, addr_h, addr_m, addr_l.
  The address will be used to set the response pointer.
  After C62_SET_ADDRESS getting response bytes will readout that sector of memory.
  The address will also be used to set the flash sector address (by rounding down to the nearest
  sector boundary).

\*---------------------------------------------------------------------------------------------------------*/
{
    switch(cmd.n_args_expected)
    {
        default:                                        // Start of command
            cmd.n_args_expected = 3;                    // 3 args expected
            break;

        case 3:                                 // First argument
            cmd.n_args_expected = 2;            // Two more args expected

            // Save 1st argument (either addr_l or addr_h)
            if ( hardware.mcu_transmit_in_big_endian == TRUE )
            {
                // the flash area of M16C62 goes from 0C0000 to 0FFFFF
                // so the highest byte can't be greater than 0F (or must be below 0x10)
                if ( arg < 0x10 )                       // If arg is a valid page index
                {
                    address.bytes[2] = arg; // addr_h
                }
                else
                {
                    data_exchange.has_error = TRUE;
                    return(M16C62_BAD_ARG);             // Report error
                }
            }
            else
            {
                address.bytes[0] = arg;         // addr_l
            }
            break;

        case 2:
            cmd.n_args_expected = 1;
            address.bytes[1] = arg;     // Save 2nd argument addr_m
            break;

        case 1:
            cmd.n_args_expected = 0;                    // No more arg expected

            // Save 3rd argument (either addr_h or addr_l)
            if ( hardware.mcu_transmit_in_big_endian == TRUE )
            {
                address.bytes[0] = arg;         // addr_l
            }
            else
            {
                // the flash area of M16C62 goes from 0C0000 to 0FFFFF
                // so the highest byte can't be greater than 0F (or must be below 0x10)
                if ( arg < 0x10 )                       // If arg is a valid page index
                {
                    address.bytes[2] = arg; // addr_h
                }
                else
                {
                    data_exchange.has_error = TRUE;
                    return(M16C62_BAD_ARG);             // Report error
                }
            }

            // the flash can be written in blocks of 256 bytes, starting in 256 (0x100) boundaries
            flash.write_address = address.long_word & 0xFFF00;  // Set address to start of flash sector

            // in case they want to read the memory area
            data_exchange.rsp_bytes_in_buffer = 0x100;                  // Set length to one sector
            data_exchange.memoryPtr = (INT8U FAR *)address.long_word;   // Link address to response pointer

            break;
    }

    return(cmd.n_args_expected);                // Return number of arguments expected
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U FastCmdSetData(INT8U arg)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_SET_DATA, 2
  arg: 256, [byte00, byte01, byte02, ..., byteFE, byteFF]

  This function will receive 0x100 data bytes from the MCU and will store them in the sector
  buffer.
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( cmd.n_args_expected == 0 )             // If start of set data command
    {
        cmd.n_args_expected = 0x100;                    // Request 256 bytes
    }
    else                                        // else arg is sector data byte
    {
        if ( cmd.n_args_expected > 0x100 ) // check to not overwrite variables beyond the array size
                {
            cmd.n_args_expected = 1;
            }
        flash.buf[0xFF - (--cmd.n_args_expected)] = arg;        // Save data in sector buffer
    }

    return(cmd.n_args_expected);                // Return number of arguments expected
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U FastCmdSetCrcLen(INT8U arg)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_SET_CRC_LEN, 4
  arg: 3,  [len_h, len_m, len_l] (CRC zone length in bytes - even only)      FGC2
           [len_l, len_m, len_h]                                             FGC3

  This function will receive the CRC zone length in bytes.
  It should be even and it is assumed that the last two bytes of the zone contain the saved CRC word
  (in big-endian order), calculated from all the  bytes in the zone, EXCEPT the last two.
  The length value is 24-bits long and therefore requires three argument bytes to transfer.
  For the FGC2 (HC16) , the transfer is big-endian (H/M/L)
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( cmd.n_args_expected == 0 )             // If start of Set CRC length command
    {
        cmd.n_args_expected = 3;                // Request 3 bytes
    }
    else                                        // else arg is CRC length data byte
    {
        // Save data in CRC length union buffer
        if ( hardware.mcu_transmit_in_big_endian == TRUE )
        {
            crc.bytes[--cmd.n_args_expected] = arg;
        }
        else
        {
            crc.bytes[ 3 - (cmd.n_args_expected--) ] = arg;
        }
    }

    return(cmd.n_args_expected);                // Return number of arguments expected
}


void FastCmdSetGetBlockMaskFuncC62A(void)
{
	fast_cmd_set_block_mask = FastCmdGetBlockMaskC62A;
}


void FastCmdSetGetBlockMaskFuncC62P(void)
{
	fast_cmd_set_block_mask = FastCmdGetBlockMaskC62P;
}

