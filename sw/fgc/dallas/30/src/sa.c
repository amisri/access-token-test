/*---------------------------------------------------------------------------------------------------------*\
  File:         sa.c

  Purpose:      M16C62 Boot ID program
                Standalone operation functions

  History:

\*---------------------------------------------------------------------------------------------------------*/

#include <sa.h>
#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)
#include <iodefines.h>          // include sfr definitions
#include <mcu_dependant.h>      // for OS_EXIT_CRITICAL(), OS_ENTER_CRITICAL()
#include <main.h>               // for hardware global variable
#include <term.h>               // for term global variable
#include <stdio.h>              // for fprintf()

/*---------------------------------------------------------------------------------------------------------*/
// if I use INT16S as 'signed short'
// I get an incomptible pointer warning (????)
// when assigning SaSendCh to the FILE structure    int (* _func_out)(int);

INT16S SaSendCh(INT16S ch)
/*---------------------------------------------------------------------------------------------------------*\
  it is used as the callback for the terminal stream [term.f._func_out = SaSendCh] in main.c

  This function is called to submit a character to the terminal transmission queue.  If the queue is full,
  the function will block by polling the number of characters in the queue, waiting for it to be emptied
  by IsrTermTx().
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                      // Allocate storage for CPU status register
    OS_CPU_SR   cpu_sr = 0;
#endif

    // slow down terminal output to let the remote terminal to poll without loosing chars
    if ( term.db_lvl > 1 )
    {
        USLEEP(1800);   // at higher debug levels we have a big load
    }
    else
    {
        USLEEP(400);
    }

    while ( term.n_ch == TERM_BUF_SIZE );       // Wait while the queue is full
    switch (hardware.model)
    {
        case FGC3_DALLAS_DS2480 :
        {
            // UART0 handles the TERMINAL
            while ( ti_u0c1 == 0 ); // while transmit buffer empty flag = 0 (transmision buffer not empty)
            break;
        }
        case FGC2 :
        default :
        {
            // UART2 handles the TERMINAL
            while ( ti_u2c1 == 0 ); // while transmit buffer empty flag = 0 (transmision buffer not empty)
            break;
        }
    } // end switch

    OS_ENTER_CRITICAL();

    if ( term.n_ch == 0 )                       // If queue is empty
    {
        // the first byte is output here, the ones following (in the buffer) will be output by IsrTermTx()
        *(hardware.term_tx) = term.uart_copy = ch; // Tx char to TERMINAL
                                                // and make transmitted byte available to MCU
    }
    else
    {
        term.n_ch++;
        term.buf[term.in++] = ch;               // Insert character in the queue
    }

    OS_EXIT_CRITICAL();

    return(TRUE);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sa.c
\*---------------------------------------------------------------------------------------------------------*/
