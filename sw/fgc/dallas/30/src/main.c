/*---------------------------------------------------------------------------------------------------------*\
  File:         main.c

  Purpose:      M16C62 Boot program - main

  Notes:        The boot has two mode: STANDALONE and NORMAL.  The mode is determined by the byte it reads
                from the M68HC16 when it starts.  If it finds the C62_SWITCH_PROG byte, it will operate in
                NORMAL mode.  It returns the complement of the handshake byte, prepares the version byte in
                the response buffer and waits for new commands.

                In STANDALONE, it runs the main Id Program directly.

                The M68HC16 is a big endian processor while the M16C62 is a little endian processor.
                All communication between the two follows the M68HC16 standard and is big endian.

                Definitions of terms:

                PAGE    A page is a 64KB zone within the memory, aligned on a 64KB boundary.
                        The M16C62 is like the M68HC16 and has 20 bit addressing, which allows for 16
                        pages to be addressed (0x00000 to 0xFFFFF).

                BLOCK   A block is a zone within the M16C62 flash memory that can be erased in one
                        operation.  The M16C62 has 4 pages of flash divided into seven blocks:

                                Block   Size    Address                 Use
                                  0      16K    0xFC000 - 0xFFFFF       C62 boot (top 4KB only) or DB monitor
                                  1       8K    0xFA000 - 0xFBFFF       Part type database
                                  2       8K    0xF8000 - 0xF9FFF       Scan results (Couplers+Devices)
                                  3      32K    0xF0000 - 0xF7FFF       Code 2 containing ID Program (P6)
                                  4      64K    0xE0000 - 0xEFFFF       Dallas to part index database
                                  5      64K    0xD0000 - 0xDFFFF       Dallas to part index database
                                  6      64K    0xC0000 - 0xCFFFF       Dallas to part index database

                        The C62 boot will only allow erase and program operations on blocks 1-6.

                SECTOR  A sector is a zone of 256 bytes within the M16C62 flash memory that can be
                        programmed in one operation.  Sectors are aligned on 256 bytes addresses.
                        There are 256 sectors per page.

                        Note that within the M16C62 documentation, a sector is referred to as a page.

  History:

    15/01/04    af      Created
    15/07/04    qak     Updated to support Normal and Standalone operation
\*---------------------------------------------------------------------------------------------------------*/

#define MAIN_GLOBALS
#define M16C62_GLOBALS
#define TERM_GLOBALS
#define FLASH_GLOBALS
#define ISR_GLOBALS

#include <main.h>
#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)
#include <iodefines.h>          // include sfr definitions
#include <term.h>               // for term global variable
#include <flash.h>              // for c62_flash, flash_block_info[] global variables
#include <isr.h>                // for isr global variable
#include <slowCmd.h>            // for BgTsk()
#include <sa.h>                 // for SaSendCh()
#include <mcu_dependant.h>      // for PLD_CMD_BUSY_SIGNAL
#include <fastCmd.h>

/*---------------------------------------------------------------------------------------------------------*/

#ifdef DEBUG_FGC3
/*---------------------------------------------------------------------------------------------------------*/
void Fgc3_debug_init(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    // --- UART2 - Terminal ---

    u2mr        =   0x05;       // Internal clk, 8 data bits, 1 stop bit, no parity
    u2c0        =   0x10;       // Use f1, CTS/RTS disabled, transfer LSB first
//  u2brg       =   0x67;       // Set u0brg = f1/(16*baud) - 1   (0x67 = 9600 when f1=16MHz)
    u2brg       =   0x8;        // Set u0brg = f1/(16*baud) - 1   (0x8 = 115200 when f1=16MHz)
    u2c1        =   0x01;       // Enable Tx
//  sprintf(std,"\33c     \33[?7h\a\rID Boot Test \r\n");
//  Fgc3_debug_print();
}
/*---------------------------------------------------------------------------------------------------------*/
void Fgc3_debug_print(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U loop;

    for ( loop = 0; std[loop] != 0; loop++ )
    {
        u2tb = std[loop];

        while (ti_u2c1 == 0)
        {}
    }

}
/*---------------------------------------------------------------------------------------------------------*/
#endif

/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*\
  The M16C62 boot program will wait for bytes from the HC16.  They will be interpreted by the interrupt
  routine.  Some commands are too slow to be executed at interrupt level and are passed to the background
  task for execution.
\*---------------------------------------------------------------------------------------------------------*/
{


	INT8U c62_type;
/*
  environment identification : Port 10 (pins 97,95,94,93,92,91,90,89)
                                       (     P100/AN0, P101/AN1, P102/AN2, P103/AN3, P104/AN4/#Ki0, P105/AN5/#Ki1, P106/AN6/#Ki2, P107/AN7/#Ki3)

  FGC2 :  bits are all floating, read as "1"s
  FGC3 : Port 10
                 bit 0 has a 1Kohm pull down (to GND), read as a "0" (Internal pull up required to sense ground on FGC3)
                 bits 1 to 6 are floating
                 bit 7 connected to a test point (B11)
                       normally floating (Dallas DS2480 old technology bus driver chip)
                       pulled down for Dallas new technology bus driver chips
*/
    hardware.model = FGC2; // by default
    hardware.mcu_transmit_in_big_endian = TRUE; // by default


    firp = 0xFF;
    c62_type = firp & 0x03;

    if ( c62_type == FLASH_M16C62_P )
    {
    	flash.type = FLASH_M16C62_P;
    	flash.block_idx = 0;
    	FlashSetFlashTransferC62P();
    	FastCmdSetGetBlockMaskFuncC62P();
    }
    else
    {
    	fira = 0xFF;
    	c62_type = fira & 0x03;

    	if ( c62_type == FLASH_M16C62_A )
    	{
    		flash.type = FLASH_M16C62_A;
    		flash.block_idx = 1;
    		FlashSetFlashTransferC62A();
    		FastCmdSetGetBlockMaskFuncC62A();
    	}
    	else
    	{
    		//Todo: error handling if chip not A/P/other working version
    	}
    }




//  isr.busy_signaled_by_tx_queue = FALSE;

    // this avoids to execute the ISR until the initialisation is finish, BgTsk() ready
    isr.cmd_in_progress = TRUE;

//  pd10 = 0x00; // Port P10 direction register, all input

//  pur2 - Pull-up control register 2
    pu24 = 1; // P10.0 to P10.3 pull-up
//  pu25 = 1; // P10.4 to P10.7 pull-up


/*
    if (p10 == 0xFF)
    {
        FGC2
    }
    if (p10_7 == 1) // (Dallas DS2480 old technology bus driver chip)
    {
        (Dallas DS2480 old technology bus driver chip)
    }
*/
    if (p10_0 == 0)
    {
        hardware.model = FGC3_DALLAS_DS2480;
//      hardware.mcu_transmit_in_big_endian = FALSE; // for FGC 3.0
    }

    // --- Port 1 and Port 3 ---

    pd1         = 0x80;                 // Set port 1 bit 7 as output (CMDBUSY), the rest as inputs (INT3 included)
    pd3         = 0x03;                 // Set port 3 bit 0 (DATARDY) and 1 (CMDERR) as outputs


    // the transition from HIGH to LOW of this byte is the information taken by the PLD to set it register bits
    // prepare the signal to 1 (so ready for the next falling edge)
//  PLD_DATA_READY_SIGNAL = 1;
//  PLD_CMD_ERROR_SIGNAL  = 1;

    p3          = 0x03;         // set p3.0 DATARDY and p3.1 CMDERR lines HIGH
    PLD_CMD_BUSY_SIGNAL = 1;    // set p1.7 CMDBUSY to HIGH

    // --- Timer A0 - 1 Mhz clock (f1/16) ---

    ta0         =   15;                 // 1MHz free running timer using 16MHz clock
    ta0s        = 0x01;                 // Start timer A0

    // --- Timer A1 - Free running counter at 1 Mhz (uses Timer A0 as input) - used by USLEEP() macro ---

    ta1mr       = 0x41;                 // Timer mode register: event counter mode, free running
    trgsr       = 0x02;                 // Trigger select register: Timer A1 uses TA0 overflow
    udf         = 0x02;                 // Increment counter for Timer A1
    ta1s        = 0x01;                 // Start timer A1

    // --- UART0 - for FGC2 : Dallas 2480 line driver ---
    // --- UART0 - for FGC3 : Terminal ---

    u0mr        =   0x05;       // Internal clk, 8 data bits, 1 stop bit, no parity
    u0c0        =   0x10;       // Use f1, CTS/RTS disabled, transfer LSB first
    u0brg       =   0x67;       // Set u0brg = f1/(16*baud) - 1   (0x67 = 9600 when f1=16MHz)
    u0c1        =   0x05;       // Enable Rx and Tx


    // --- UART2 - for FGC2 : Terminal ---
    // --- UART2 - for FGC3 : Dallas 2480 line driver ---

    u2mr        =   0x05;       // Internal clk, 8 data bits, 1 stop bit, no parity
    u2c0        =   0x10;       // Use f1, CTS/RTS disabled, transfer LSB first
    u2brg       =   0x67;       // Set u0brg = f1/(16*baud) - 1   (0x67 = 9600 when f1=16MHz)
    u2c1        =   0x05;       // Enable Rx and Tx

    // --- Port 0 and Port 2 ---

    switch (hardware.model)
    {
        case FGC3_DALLAS_DS2480 :
        {
            hardware.mcu_link_rx = (INT8U NEAR *) &p2;  // Rx link with MCU - Set port p2 direction to all inputs
            pd2 = 0x00;                 // Rx - Set port p2 direction to all inputs

            hardware.mcu_link_tx = (INT8U NEAR *) &p0;  // Tx link with MCU - Set port p0 direction to all outputs
            pd0 = 0xff;                 // Tx - Set port p0 direction to all outputs

            hardware.term_tx   = (INT8U NEAR *) &u0tbl;

            s0tic = 0x01;       // Select interrupt level 1 for UART0 transmission register empty, IsrTermTx()
            s2ric = 0x02;       // Select interrupt level 2 for UART2 data reception, IsrDallasRx()
            break;
        }
        case FGC2 :
        default :
        {
            hardware.mcu_link_rx = (INT8U NEAR *) &p0;  // Rx link with MCU - Set port p0 direction to all inputs
            pd0 = 0x00;                 // Rx - Set port p0 direction to all inputs

            hardware.mcu_link_tx = (INT8U NEAR *) &p2;  // Tx link with MCU - Set port p2 direction to all outputs
            pd2 = 0xff;                 // Tx - Set port p2 direction to all outputs

            hardware.term_tx   = (INT8U NEAR *) &u2tbl;

            s0ric = 0x02;       // Select interrupt level 2 for UART0 data reception, IsrDallasRx()
            s2tic = 0x01;       // Select interrupt level 1 for UART2 transmission register empty, IsrTermTx()
            break;
        }
    } // end switch

    // --- Prepare terminal stream ---

    term.f._flag         = _IOWRT;                      // Set stream flag to write-only
    term.f._mod          = _BIN;                        // Set stream mode to binary
    term.f._func_out     = SaSendCh;                    // Link character output function to stream
//  term.db_lvl      = 0;
//  term.n_ch        = 0;                       // Number of characters in the buffer

//  *(hardware.term_tx) = 'B'; // Tx char to TERMINAL

    // --- Start in mode according to 1st data byte read ---

    // if the last cmd send by the MCU to IdProg was C62_SWITCH_PROG it remains in *(hardware.mcu_link_rx)
    // and IdProg will jump here without changing it, signalling that we must run the BgTsk()
    // and not let go trough back to IdProg

    // if the MCU is powering on the M16C62 it must first write C62_SWITCH_PROG to *(hardware.mcu_link_rx)
    // if it wants IdBoot to run the BgTsk() and not let go trough to IdProg

    int3ic = 3;                         // Set INT3 priority level to 3 (MCU commands)

    if ( *(hardware.mcu_link_rx) == C62_SWITCH_PROG )   // stay in IdBoot ?
    {
        *(hardware.mcu_link_tx) = ~C62_SWITCH_PROG;     // Return complement to say "I'm alive"
//      *(hardware.mcu_link_tx) = ~ *(hardware.mcu_link_rx);

        asm("fset i");                                  // Enable  interrupts

        BgTsk();                                        // Run background task and wait for MCU commands
    }
    else                                                // else standalone operation (with Dev Front Panel)
    {
//      this is done at IdProg
//      *(hardware.mcu_link_tx) = ~ *(hardware.mcu_link_rx); // Return complement to say "I'm alive"

        asm("fclr i");                                  // for safety, disable interrupts

//      *(hardware.term_tx) = 'X'; // Tx char to TERMINAL

        ((void(*)(void))IDPROG_START)();                // Run Id Program directly

        // the clear of busy is done by IdProg when starts
//      PLD_CMD_BUSY_SIGNAL     = 0;                    // set p1.7 CMDBUSY to LOW
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/
