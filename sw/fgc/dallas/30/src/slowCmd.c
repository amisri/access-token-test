/*---------------------------------------------------------------------------------------------------------*\
  File:         slowCmd.c

  Purpose:      M16C62 Boot program
                background task
                MCU slow response functions

  Notes:        These functions provide the slow (background) command processing.
                They are used for Boot commands that take too long to run at interrupt level.
                All slow (background)  functions report their results via the response buffer,
                that the MCU can read out using the C62_GET_RESPONSE command.
                All responses start with an errno byte (constants are defined in c62_consts.h).
                Additional bytes may be included according to the value of the first byte.

  History:

    28/04/04    qak     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <slowCmd.h>
#include <cc_types.h>                   // basic typedefs for Renesas NC30 compiler (M16C target)
#include <term.h>                       // for term global variable
#include <mcu_dependant.h>              // for USLEEP()
#include <iodefines.h>                  // include sfr definitions
#include <flash.h>                      // for c62_flash, flash_block_info[], flash global variables
#include <main.h>                       // for hardware, address global variable
#include <structs_bits_little.h>        // INTEL bits, bytes, words, dwords ...
#include <stdio.h>                      // for fprintf()
#include <isr.h>                        // for isr global variable

/*---------------------------------------------------------------------------------------------------------*/
INT16U BgTsk(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the background task.
  It will wait indefinitely for commands to be passed from the interrupt handler IsrByteFromMcu().
  This is used for all the M16C62 IdBoot commands that are too slow to run at interrupt level.
  All slow (background) commands prepare a response buffer that can be read out by
  the MCU using the C62_GET_RESPONSE command.
\*---------------------------------------------------------------------------------------------------------*/
{
    static const void (* const Arr_Command_Functions[])(void) =
    {
        UnknownSlowCmd,         //  0, C62_GET_RESPONSE
        UnknownSlowCmd,         //  1, C62_SET_ADDRESS
        UnknownSlowCmd,         //  2, C62_SET_DATA
        UnknownSlowCmd,         //  3, C62_SET_BLOCK_MASK
        UnknownSlowCmd,         //  4, C62_SET_CRC_LEN,         C62_SET_DB
        UnknownSlowCmd,         //  5, C62_SET_BRANCH
        UnknownSlowCmd,         //  6, C62_SET_DEV
        UnknownSlowCmd,         //  7, C62_SET_KBD_CHAR
        UnknownSlowCmd,         //  8, C62_GET_TERM_CHAR
        UnknownSlowCmd,         //  9,
        SlowCmdSwitchProg,      // 10, C62_SWITCH_PROG
        SlowCmdEraseFlash,      // 11, C62_ERASE_FLASH
        SlowCmdProgFlash,       // 12, C62_PROG_FLASH
        SlowCmdCalcCrc,         // 13, C62_CALC_CRC,            C62_READ_IDS
        UnknownSlowCmd,         // 14, C62_READ_TEMPS
        UnknownSlowCmd,         // 15, C62_READ_TEMPS_BRANCH
        UnknownSlowCmd,         // 16, C62_RUN_MONITOR
        UnknownSlowCmd,         // 17, C62_SET_BIG_ENDIAN
        UnknownSlowCmd,         // 18, C62_SET_LITTLE_ENDIAN
        UnknownSlowCmd          // 19, C62_SET_DEBUG_LEVEL
    };

    // IdProg has executed command C62_SWITCH_PROG leaving PLD_CMD_BUSY_SIGNAL active
    // the MCU is still waiting for the command to finish
    // we signal here that the command has finished
    // returning the version

//  data_exchange.rsp.err.code = M16C62_NO_ERROR;       // Prepare default OK response in response buffer
//  data_exchange.rsp.bytes[0] = 0; the previous line is similar to this and as the RAM was zeroed, it is not needed

    // actually this is not read by the MCU as for status M16C62_NO_ERROR no extra byte is read
    data_exchange.rsp.bytes[1] = C62BOOT_VERSION;               // Prepare boot version word in response buffer
    data_exchange.rsp_bytes_in_buffer = 2;
    data_exchange.memoryPtr = data_exchange.rsp.bytes;  // Link response pointer to response buffer

//  cmd.slow_cmd_idx = 0;       // to be sure to stop in the while() ahead while MCU is reading the version

    // do the falling edge to clear the last BUSY leave by the IdProg

    PLD_DATA_READY_SIGNAL = 0;  // set p3.0 DATARDY to LOW
    PLD_CMD_BUSY_SIGNAL = 0;    // set p1.7 CMDBUSY to LOW

    // prepare the signal to 1 (so ready for the next falling edge)
    PLD_DATA_READY_SIGNAL = 1;  // set p3.0 DATARDY to HIGH
    PLD_CMD_BUSY_SIGNAL = 1;    // set p1.7 CMDBUSY to HIGH

    isr.cmd_in_progress = FALSE; // now we are ready to run

    // --- Loop forever to process background commands ---
    for(;;)
    {
        while( cmd.slow_cmd_idx == 0 ); // Wait for command index from IsrByteFromMcu()

        if ( term.db_lvl > 1 )
        {
            fprintf(TX_TERM, "CMD:%u\n\r", cmd.slow_cmd_idx); // fprintf() is not reentrant!!!
        }

//      can't overwrite the first use at startup needed for the version
//      data_exchange.rsp_bytes_in_buffer = 1;
//      these can be overwritten without any harm
        data_exchange.rsp.err.code = M16C62_NO_ERROR;       // Prepare default OK response in response buffer
        data_exchange.memoryPtr = data_exchange.rsp.bytes;  // Link response pointer to response buffer

        Arr_Command_Functions[cmd.slow_cmd_idx](); // Call command handler

        if ( term.db_lvl > 0 )
        {
            fprintf(TX_TERM,"Response: bytes:%u  sts:%u\n\r", data_exchange.rsp_bytes_in_buffer, (INT16U) data_exchange.rsp.err.code);
        }

        cmd.slow_cmd_idx = 0;                   // Clear command index

        if ( term.n_ch == 0 )                   // if nothing in the transmission queue
        {
            isr.cmd_in_progress = FALSE;
            // this produce the falling edge of the signal, so the PLD will clear the CMDBUSY bit to 0 in it register
            PLD_CMD_BUSY_SIGNAL  = 0;
        }
        else
        {
            isr.busy_signaled_by_tx_queue = TRUE;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void UnknownSlowCmd(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    data_exchange.rsp.err.code = M16C62_UNKNOWN_CMD;
    data_exchange.rsp_bytes_in_buffer = 1;
}
/*---------------------------------------------------------------------------------------------------------*/
void SlowCmdEraseFlash(void)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_ERASE_FLASH, 11

  arg: no
      (but you need C62_SET_BLOCK_MASK command to fill flash.block_mask previously)

  response : errno, flsh_stat h/l, [bad_val h/l, bad_addr h/m/l]


  This function will erase the flash blocks specified via the C62_SET_BLOCK_MASK command.
  Six of the seven flash blocks may be erased.
  The function will try up to twice to erase a block.  In case of two failures,
  it will search for the word that could not be erased.

  each bit of the mask represents one flash block, b0 represents Block 0 ... b6 represents Block 6
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      ii;
    INT16U      stat;
    INT32U      addr;
    INT16U      mask;


    mask = flash.block_mask;

    // --- Clear flash status report in response buffer ---

//  this is done by the caller
//  data_exchange.rsp.err.code = M16C62_NO_ERROR;
//  data_exchange.rsp_bytes_in_buffer = 1;

    data_exchange.rsp.err.v.erase.flash_stat.word = 0;

    if ( mask == 0 )                                            // If nothing to erase
    {
        return;
    }

    // --- Transfer Flash function to RAM at 0x400 ---

    FlashTransfer();

    // --- Try to erase all blocks specified in flash.block_mask ---

    for ( ii = 0; mask; ii++, mask >>= 1 )
    {
        if ( (mask & 1) != 0 )                                  // If block been selected for erasure
        {
            FlashCpuInit(); // set processor for programming
            stat = FlashErase( flash_block_info[flash.block_idx][ii].end );      // Erase block
            FlashCpuRestore();                                  // Restore processor modes

            if ( stat != C62_FLASHOK )                          // If erase failed
            {
                // Report bad flash status
                if ( hardware.mcu_transmit_in_big_endian == TRUE )
                {
                    data_exchange.rsp.err.v.erase.flash_stat.be.hi = (INT8U) (stat >> 8);
                    data_exchange.rsp.err.v.erase.flash_stat.be.lo = (INT8U) stat;
                }
                else
                {
                    data_exchange.rsp.err.v.erase.flash_stat.word = stat;
                }


                FlashCpuInit();                                 // set processor for programming
                stat = FlashErase( flash_block_info[flash.block_idx][ii].end );  // Erase block
                FlashCpuRestore();                              // Restore processor modes

                if ( stat != C62_FLASHOK )                      // If erase failed twice
                {
                    // Report bad flash status
                    if ( hardware.mcu_transmit_in_big_endian == TRUE )
                    {
                        data_exchange.rsp.err.v.erase.flash_stat.be.hi = (INT8U) (stat >> 8);
                        data_exchange.rsp.err.v.erase.flash_stat.be.lo = (INT8U) stat;
                    }
                    else
                    {
                        data_exchange.rsp.err.v.erase.flash_stat.word = stat;
                    }

                    for(addr  = flash_block_info[flash.block_idx][ii].start;     // Set index to start of block
                        addr <= flash_block_info[flash.block_idx][ii].end &&     // Scan block for non-erased locations
                        *((INT16U FAR *)addr) == 0xFFFF;
                        addr += 2 );
                    {
                    }

                    if ( addr <= flash_block_info[flash.block_idx][ii].end )     // If erase failure detected
                    {
                        data_exchange.rsp.err.code = M16C62_ERASE_FAILED;               // Report erase failure

                        if ( hardware.mcu_transmit_in_big_endian == TRUE )
                        {
                            data_exchange.rsp.err.v.erase.bad_value.be.hi = ((INT8U FAR *)addr)[1];     //  Bad word value
                            data_exchange.rsp.err.v.erase.bad_value.be.lo = ((INT8U FAR *)addr)[0];

                            data_exchange.rsp.err.v.erase.address.be.hi_w_hi_b = addr >> 16;    //  Address high byte
                            data_exchange.rsp.err.v.erase.address.be.hi_w_lo_b = addr >> 8;     //  Address mid byte
                            data_exchange.rsp.err.v.erase.address.be.lo_w_hi_b = addr;  //  Address low byte
                        }
                        else
                        {
                            data_exchange.rsp.err.v.erase.bad_value.word = *((INT16U FAR *)addr);       //  Bad word value
                            data_exchange.rsp.err.v.erase.address.int32u = addr;
                        }

                        data_exchange.rsp_bytes_in_buffer = 3; // the last byte off address is not sent
                        return;
                    }
                }

            }
        }
    }
    data_exchange.rsp_bytes_in_buffer = 3; // err.code = M16C62_NO_ERROR and flash_stat = 0
}
/*---------------------------------------------------------------------------------------------------------*/
void SlowCmdProgFlash(void)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_PROG_FLASH, 12

  arg: no
       (but you need C62_SET_DATA to fill flash.buf[] and C62_SET_ADDRESS to fill flash.write_address previously)

  response : errno, flsh_stat h/l, [exp_val, bad_val, bad_addr h/m/l]

  This function will program the current sector of 256 bytes into flash at the specified address.
  If the attempt to save the sector fails twice, it will search for and report the bad byte.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      ii;
    INT16U      stat;
    INT16U      idx;
    INT8U FAR * addr;
    BOOLEAN     valid_addr;


//  data_exchange.rsp.err.code = M16C62_NO_ERROR;  this is done at BgTsk()

    // --- Check sector address is valid ---

    valid_addr = FALSE;         // by default


    for ( ii = 0; ii < FLASH_BLOCKS_NUMBER; ii++)
    {
        if ( flash_block_info[flash.block_idx][ii].erasable == TRUE )
        {
            if (    flash.write_address >=  flash_block_info[flash.block_idx][ii].start
                 && flash.write_address <= (flash_block_info[flash.block_idx][ii].end & 0xFFFFFF00)
               )
            {
                valid_addr = TRUE;      // it belongs to a single sector (not crossing sector boundary)
            }
        }
    }

    if ( valid_addr == FALSE )
    {
        data_exchange.rsp.err.code = M16C62_INVALID_SECTOR;     // Report invalid sector

        if ( hardware.mcu_transmit_in_big_endian == TRUE )
        {
            data_exchange.rsp.err.v.address.be.hi_w_hi_b = (INT8U) flash.write_address >> 16;   //  Address high byte
            data_exchange.rsp.err.v.address.be.hi_w_lo_b = (INT8U) flash.write_address >> 8;    //  Address mid byte
            data_exchange.rsp.err.v.address.be.lo_w_hi_b = (INT8U) flash.write_address; //  Address low byte
        }
        else
        {
            data_exchange.rsp.err.v.address.int32u = flash.write_address;
        }
        data_exchange.rsp_bytes_in_buffer = 4; // the last byte off address is not sent
        return;
    }

    // --- Clear flash status report in response buffer ---
//  data_exchange.rsp.err.code = M16C62_NO_ERROR;  this is done at BgTsk()
    data_exchange.rsp.err.v.write.flash_stat.word = 0;

    // --- Transfer Flash function to RAM at 0x400 ---

    FlashTransfer();

    // --- Try up to twice to save sector in flash ---

    FlashCpuInit();                                     // set processor for programming
    stat = FlashWrite();                                // write 256 bytes into flash
    FlashCpuRestore();                                  // Restore processor modes

    if ( stat != C62_FLASHOK )                          // If write failed
    {
        FlashCpuInit();                                 // set processor for programming
        stat = FlashWrite();                            // write 256 bytes into flash
        FlashCpuRestore();                              // Restore processor modes

        if(stat != C62_FLASHOK)                         // If write failed
        {
            // Report bad flash status
            if ( hardware.mcu_transmit_in_big_endian == TRUE )
            {
                data_exchange.rsp.err.v.write.flash_stat.be.hi = (INT8U) (stat >> 8);
                data_exchange.rsp.err.v.write.flash_stat.be.lo = (INT8U) stat;
            }
            else
            {
                data_exchange.rsp.err.v.write.flash_stat.word = stat;
            }

            addr = (INT8U FAR *)flash.write_address;            // Set pointer to start of code page

            // Scan page for mismatch locations
            for ( idx = 0; (idx < 0x100) && (*addr == flash.buf[idx]); idx++, addr++);
            {
            }

            if ( idx < 0x100 )                                  // If page scanned with error
            {
                data_exchange.rsp.err.code = M16C62_PROG_FAILED;
                data_exchange.rsp.err.v.write.exp_value  = flash.buf[idx];      //  Expected byte value
                data_exchange.rsp.err.v.write.bad_value  = *addr;               //  Bad byte value

                if ( hardware.mcu_transmit_in_big_endian == TRUE )
                {
                    data_exchange.rsp.err.v.write.address.be.hi_w_hi_b = (INT8U) flash.write_address >> 16;     //  Address high byte
                    data_exchange.rsp.err.v.write.address.be.hi_w_lo_b = (INT8U) flash.write_address >> 8;      //  Address mid byte
                    data_exchange.rsp.err.v.write.address.be.lo_w_hi_b = (INT8U) idx;                   //  Address inside sector
                }
                else
                {
                    data_exchange.rsp.err.v.write.address.int32u = (INT32U) addr; // equivalent to = flash.write_address + idx;
                }
                data_exchange.rsp_bytes_in_buffer = 8; // the last byte off address is not sent
                return;
            }
        }
    }
    data_exchange.rsp_bytes_in_buffer = 3; // err.code + flash_stat
}
/*---------------------------------------------------------------------------------------------------------*/
void SlowCmdCalcCrc(void)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_CALC_CRC, 13

  arg: no
       (but you need C62_SET_ADDRESS to fill address.long_word previously
        and C62_SET_CRC_LEN to fill crc.long_word)

  response : errno, Hi(calcd_CRC), Lo(calcd_CRC), Hi(saved_CRC), Lo(saved_CRC) // FGC2
             errno, Lo(calcd_CRC), Hi(calcd_CRC), Lo(saved_CRC), Hi(saved_CRC) // FGC3

  This function will calculate and check the CRC checksum for a zone of memory defined by the specified
  address and CRC length.  The length will be rounded down to a word boundary. The function returns the
  calculated  CRC and saved CRC (assumed to be in the last word of the specified zone).  The errno will
  indicate if they match or not.

\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U              crc_len_words;
    INT8U  FAR *        addr;


    crc_len_words = crc.long_word / 2;

    addr = (INT8U FAR *) address.long_word;

//  data_exchange.rsp.bytes[0]= M16C62_NO_ERROR; // done in the background Task

    crcd = 0x0000;                      		// Initialise M16C62's CRC results register

    // reverse inputs for M16C62P

    while( --crc_len_words != 0 )               // For all words in the zone excluding last (saved CRC)
    {
        crcin = *(addr++);                      // Transfer word to CRC engine
        crcin = *(addr++);
    }

    if ( hardware.mcu_transmit_in_big_endian == TRUE )
    {
        data_exchange.rsp.err.v.crc.calculated.be.hi = crcdh;
        data_exchange.rsp.err.v.crc.calculated.be.lo = crcdl;

        data_exchange.rsp.err.v.crc.hardcoded.be.hi = *(addr++);        // Transfer next word (most likely it is the saved CRC)
        data_exchange.rsp.err.v.crc.hardcoded.be.lo = *(addr++);
    }
    else
    {
        data_exchange.rsp.err.v.crc.calculated.word = crcd; // Transfer resulting CRC into response buffer

        data_exchange.rsp.err.v.crc.hardcoded.le.hi = *(addr++);        // Transfer next word (most likely it is the saved CRC)
        data_exchange.rsp.err.v.crc.hardcoded.le.lo = *(addr++);
    }

    // If calculated CRC doesn't match saved CRC
    if ( data_exchange.rsp.err.v.crc.calculated.word != data_exchange.rsp.err.v.crc.hardcoded.word )
    {
        data_exchange.rsp.err.code = M16C62_CRC_FAILED;         // Set errno to CRC failed code
    }

    data_exchange.rsp_bytes_in_buffer = 5;
}


/*---------------------------------------------------------------------------------------------------------*/
void SlowCmdSwitchProg(void)
/*---------------------------------------------------------------------------------------------------------*\
  cmd: C62_SWITCH_PROG, 10
  arg: no
  response : 2 bytes, [0] success [1] version number

    This function will run the main ID program at the start of page F (block 3).
    It is a slow (background) command so that the transfer is made at task level rather than interrupt level.
    Interrupts are disabled before starting the ID program,
    which is also responsible for clearing the BUSY signal.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm("fclr i");                              // for safety, disable interrupts

//  *(hardware.term_tx) = 'X';                  // Tx char to TERMINAL

//  The cmd send by the MCU was C62_SWITCH_PROG so this remains
//  in *(hardware.mcu_link_rx) and will signal IdProg to start BgTsk() (and not to run the monitor)

    ((void(*)(void))IDPROG_START)();            // Jump to run Id Program code

    // the clear of busy is done by IdProg when starts
//  PLD_CMD_BUSY_SIGNAL = 0;                    // set p1.7 CMDBUSY to LOW

    // Never reached
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: slowCmd.c
\*---------------------------------------------------------------------------------------------------------*/
