/*---------------------------------------------------------------------------------------------------------*\
  File:         m16c62_link.h

  Purpose:      FGC M16C62 program constants
                used by:
                 FGC2/FGC3 M16C62 boot (P30)
                 FGC2/FGC3 M16C62 ID program (P31)
                 FGC2   HC16 boot
                 FGC2   HC16 main
                 FGC3   RX610 boot
                 FGC3   RX610 main
                 NDI tester

  Notes:        Definitions of terms:

                PAGE    A page is a 64KB zone within a memory space, aligned on a 64KB boundary.
                        The M16C62 is like the HC16 and has 20 bit addressing, which allows for 16
                        pages to be addressed (0x00000 to 0xFFFFF).

                BLOCK   A block is a zone within the M16C62 flash memory that can be erased in one
                        operation.  The M16C62 has 4 pages of flash (0xC-0xF) divided into seven blocks:

                                Block   Size    Address                 Use
                                  0      16K    0xFC000 - 0xFFFFF       M16C62 boot (top 2.25KB only) or monitor
                                  1       8K    0xFA000 - 0xFBFFF       Part type database
                                  2       8K    0xF8000 - 0xF9FFF       FGC/VS/ADC Dallas ID numbers
                                  3      32K    0xF0000 - 0xF7FFF       ID Program (P31) inside Code 2
                                  4      64K    0xE0000 - 0xEFFFF       Dallas to part number database
                                  5      64K    0xD0000 - 0xDFFFF       Dallas to part number database
                                  6      64K    0xC0000 - 0xCFFFF       Dallas to part number database

                        The M16C62 boot will only allow erase and program operations on blocks 1-6.

                SECTOR  A sector is a zone of 256 bytes within the M16C62 flash memory that can be
                        programmed in one operation.  Sectors are aligned on 256 bytes addresses.
                        There are 256 sectors per page, and the sector index is the top byte of
                        the address of the first byte of sector within the page.

                        Note that within the M16C62 documentation, a sector is refered to as a page.

                Interrupt Commands can have arguments and always run in less than 20us.

                Background Commands cannot have arguments and may take many seconds to complete.

                For each interrupt command listed below, the number of argument bytes expected is
                given in square brackets: [nnn].  The contents of the argument bytes is also given
                in the comment.

                For each background command, the response is described in the comments.

                For temperature sensors (DS18B20), the reading is done with maximum resolution (12 bit)
                giving units of 1/16 C or 0.0625 C.  The value is returned in two bytes, one giving
                the integer temperature in degrees (temp_C) the other the fractional part (0-15) in
                units of 0.0625C (temp_C_16).  The value in temp_C also indicates the status:

                    temp_C
                      0    = Not a temperature device
                      5    = 5C or less
                      6-94 = Measured temperature in C (temp_C_16 has fractional part)
                      95   = 95C or more
                      99   = Bad reading (CRC check failed 5 times)




  Hardware identification : Port 10 (pins 97,95,94,93,92,91,90,89)
          (P100/AN0, P101/AN1, P102/AN2, P103/AN3, P104/AN4/#Ki0, P105/AN5/#Ki1, P106/AN6/#Ki2, P107/AN7/#Ki3)

  FGC2 :  bits are all floating, read as "1"s
  FGC3 :  Port 10
                 bit 0 has a 1Kohm pull down (to GND), read as a "0" (Internal pull up required to sense gnd)
                 bits 1 to 6 are floating
                 bit 7 connected to a test point (B11)
                       normally floating (Dallas DS2480 old technology bus driver chip)
                       pulled down for Dallas new technology bus driver chips

  the chip model is M30624FGAFP (aka M16C62-A)
\*---------------------------------------------------------------------------------------------------------*/

#ifndef M16C62_LINK_H   // header encapsulation
#define M16C62_LINK_H

#ifdef M16C62_LINK_GLOBALS
#define M16C62_LINK_VARS_EXT
#else
#define M16C62_LINK_VARS_EXT extern
#endif

// ------------------------------------------------------------------------------------------------

// __M32C83__ and __M32C87__ are used by the testers projects
#if defined(__M32C83__) || defined(__M16C62__) || defined(__M32C87__)
#include <structs_bits_little.h>
#endif

#if defined(__HC16__) || defined(__RX610__)
#include <structs_bits_big.h>
#endif


#include <fgc_code.h>           // for struct fgc_code_info

// ------------------------------------------------------------------------------------------------

enum M16C62_hardware
{
    FGC2,
  FGC3_DALLAS_DS2480,
  FGC3_DS2480_MAX3648
};


/*
    FGC2
            Port 0 is Rx link with MCU
            Port 1 is
            Port 2 is Tx link with MCU
            Port 3 is

            UART0 linked to the Dallas DS2480 line driver, handling the Dallas ID bus
            UART2 handles the TERMINAL

    FGC3_DALLAS_DS2480
            Port 0 is Tx link with MCU
            Port 1 is
            Port 2 is Rx link with MCU
            Port 3 is

            UART0 handles the TERMINAL
            UART2 linked to the Dallas DS2480 line driver, handling the Dallas ID bus
*/
// ----------------------------

// --- M16C62A flash blocks ---
#define C62_BLK456_ADDR         0xC0000 // Block 6 (32Kb), Database directory and ID database
#define C62_BLK5_ADDR           0xD0000 // block 5 (32Kb)
#define C62_BLK4_ADDR           0xE0000 // block 4 (32Kb)
#define C62_BLK3_ADDR           0xF0000 // block 3 (32Kb), IdMain prog
#define C62_BLK2_ADDR           0xF8000 // block 2 (8Kb),  Scan results
#define C62_BLK1_ADDR           0xFA000 // block 1 (8Kb),  PART TYPE database
#define C62_BLK0_ADDR           0xFC000 // block 0 (16Kb)  IdBootProg or Debug Monitor
#define IDPROG_START            0xF0100 // start of ID program (in 32 Kb flash block 3); must match to the PRG_ADR variable in the 31/src/start.a30 of the IdProg
#define IDPROG_VERSION_ADR      0xF7FF6 // version double word at the end of block 3




// --- M16C62P flash blocks ---

#define C62P_BLK0_ADDR           0xFF000
#define C62P_BLK1_ADDR           0xFE000
#define C62P_BLK2_ADDR           0xFC000
#define C62P_BLK3_ADDR           0xFA000
#define C62P_BLK4_ADDR           0xF8000
#define C62P_BLK5_ADDR           0xF0000
#define C62P_BLK6_ADDR           0xE0000
#define C62P_BLK7_ADDR           0xD0000
#define C62P_BLK8_ADDR           0xC0000
#define C62P_BLK9_ADDR           0xB0000
#define C62P_BLK10_ADDR          0xA0000
#define C62P_BLK11_ADDR          0x90000
#define C62P_BLK12_ADDR          0x80000



// these constants are the mask values used to drive SlowCmdEraseFlash()
// each bit of the mask represents one flash block, b0 represents Block 0 ... b6 represents Block 6

#define C62_BLK_PT_DB           0x02    // 00000010 PTDB owns Block 1, Part Type database
#define C62_BLK_ID_PROG         0x08    // 00001000 IDPROG owns Block 3, ID Main Prog
#define C62_BLK_ID_DB           0x70    // 01110000 IDDB owns Blocks 6, 5 and 4

#define C62_MAX_ISR_US          50      // Maximum ISR time in uS

// used also by testers
#define C62_RSP_BUF_LEN         256     // Response data buffer length (bytes)

#define C62_READID_TIMEOUT      20      // C62_READ_IDS command timeout used by MCU (s)
#define C62_READTEMP_TIMEOUT    2       // C62_READ_TEMPS_BRANCH command timeout used by MCU (s)

// ------------------------------------------------------------------------------------------------

// the M16C62 copy a ROM sector (256 bytes) to the buffer when set address
#define M16C62_RSP_BUF_LEN         256     // Response data buffer length (bytes)

// responses from M16C62 to MCU

enum m16c62_response_codes
{
  M16C62_NO_ERROR                       = 0,    // [F] Command completed without error
  M16C62_BUSY                           = 1,    // [F] Already running command (no message in buffer)
  M16C62_UNKNOWN_CMD                    = 2,    // [F] Unknown command
  M16C62_BAD_ARG                        = 3,    // [F] Bad argument
  M16C62_NO_MORE_DATA                   = 4,    // [F] Response data buffer empty
  M16C62_CRC_FAILED                     = 10,   // [S] extra info: calcd_CRC h/l, saved_CRC h/l  FGC2
  M16C62_ERASE_FAILED                   = 11,   // [S] extra info: flsh_stat h/l, bad_val h/l, addr h/m/l  FGC2
  M16C62_PROG_FAILED                    = 12,   // [S] extra info: flsh_stat h/l, exp_val,bad_val, bad_addr h/m/l
  M16C62_INVALID_SECTOR                 = 13,   // [S] extra info: bad_addr h/m/l
  ERR_MICROLAN_DS2480_TIMEOUT           = 14,   // [S]
  ERR_MICROLAN_TOP_LINK_SHORTED         = 15,   // [S]
  ERR_MICROLAN_LOGICAL_PATH_NO_DEVICES  = 16,   // [S] extra info: branch_idx (This may be seen if bus is shorted)
  ERR_MICROLAN_COUPLER_UNKNOW_STATUS    = 17,   // [S] extra info: status
  ERR_MICROLAN_COUPLERS_FOUND           = 18,   // [S] extra info: n_couplers
  ERR_MICROLAN_UNEXPECTED_DEVICES       = 19,   // [S] extra info: n_unexpected
  ERR_MICROLAN_COUPLER_CONTROL_LINES    = 21,   // [S] extra info: c_ctrl_h, c_ctrl_l
  ERR_MICROLAN_UNRELIABLE_FIND          = 22,   // [S] extra info: branch_idx
  ERR_DS2480_LINK_NOT_OK                = 23,
  ERR_MICROLAN_COUPLERS_READBACK        = 24,   // [S] extra info: port1 value
  M16C62_NO_RSP                         = 99    // No response from M16C62 (not returned by M16C62 obviously)
};


enum m16c62_types
{
  M16C62_A,
  M16C62_P,
  M16C62_N,
  M16C62_UNKNOWN
};

// ------------------------------------------------------------------------------------------------

// ----- Command data -----

struct mcu_link_cmd
{
    INT16U              n_args_expected;        // Number of arguments expected for current command
    INT8U               fast_cmd_idx;           // fast command index
    volatile INT8U    slow_cmd_idx;        // slow command index (background) this is changed inside the interrupt
};

union address
{
    INT8U               bytes[4];               // Address in separate bytes
    INT32U              long_word;              // Address as a long word (little endian)
};

// ------------------------------------------------------------------------------------------------
// M16C62 - Command structures used in the communication M16C62 <-> MCU (Rx610, M32C87, M68HC16)

struct M16C62rsp_CRC_FAILED
{
    union TUnion16Bits  calculated;
    union TUnion16Bits  hardcoded;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;

struct M16C62rsp_ERASE_FAILED
{
    union TUnion16Bits  flash_stat; // only when flash_stat != 0 the following two are filled
    union TUnion16Bits  bad_value;
    union TUnion32Bits  address;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;

struct M16C62rsp_PROG_FAILED
{
    union TUnion16Bits  flash_stat;     // only when mismatch the later field are used
    INT8U               exp_value;
    INT8U               bad_value;
    union TUnion32Bits  address;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;

union M16C62rsp_ErrorsVarianPart
{
    INT8U                               state;          // M16C62rsp_UNKNOWN_STAT
    INT8U                               logical_path;   // M16C62rsp_UNRELIABLE_FIND, M16C62rsp_NO_DEV
    INT8U                               devices_number; // M16C62rsp_UNEXPECTED_DEV
    INT8U                               number_of_couplers; // M16C62rsp_NUMB_COUPLER_FAIL
    union TUnion16Bits                  control;        // M16C62rsp_COUPLER_CTRL_FAIL
    union TUnion32Bits                  address;        // M16C62rsp_INVALID_SECTOR
    struct M16C62rsp_CRC_FAILED         crc;
    struct M16C62rsp_ERASE_FAILED       erase;
    struct M16C62rsp_PROG_FAILED        write;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;

struct M16C62rsp_Errors
{
    INT8U                               code;
    union M16C62rsp_ErrorsVarianPart    v;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;

// This structure is used by the MCU (M68HC16, M32C87B, RX610) and by the M16C62

union M16C62linkResponseArea
{
    INT8U                               bytes[M16C62_RSP_BUF_LEN];      // Response buffer for background commands
    struct M16C62rsp_Errors             err;
    struct fgc_code_info                code_info;              // special response to the C62_SET_ADDRESS
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;


// try to delete the unnecessary constants
#define C62_BG_CMD0             10      // Offset to slow (background) commands
/*
    These constants are not used in FGC3 software nor M16C62 IdBoot, IdProg
    only in FGC2, once removed we can have a clean enum

    C62_P56_END_I
    C62_P5_END_I
    C62_P56_END_BG
    C62_P5_END_BG
    C62_P6_END_BG
*/

/*
 * Fast command comment format: // CMD_NUMBER, IdProg (used in 31), IdBoot (used in 30); [#args] arg0,arg1,...
 * Slow command comment format: // CMD_NUMBER, IdProb (used in 31), IdBoot (used in 30); "Action" {response} [optional pars]
 */
typedef enum m16c62_commands
{
    // --- Fast Commands (processed inside the ISR) ---
    C62_GET_RESPONSE   = 0,             // 0, IdProg, IdBoot;
    C62_SET_ADDRESS    = 1,             // 1, IdProg, IdBoot; [  3]  addr_h, addr_m, addr_l   (big endian), addr_l, addr_m, addr_h   (little endian)
    C62_SET_DATA       = 2,             // 2, IdProg, IdBoot; [256]  byte00, byte01, byte02, ..., byteFE, byteFF
    C62_SET_BLOCK_MASK = 3,             // 3, IdProg, IdBoot; [  1]  block_mask
    C62_P56_END_I      = 4,             // 4, end of shared, IdProg, IdBoot
    C62_SET_DB         = C62_P56_END_I, // 4, IdProg; [  1]  db_lvl
    C62_SET_CRC_LEN    = C62_P56_END_I, // 4, IdBoot; [  3]  len_h, len_m, len_l (CRC zone length in bytes - even only)(big endian)
                                        // len_l, len_m, len_h    (little endian)
    C62_P5_END_I       = 5,             // 5, end of fast, IdBoot
    C62_SET_BRANCH     = C62_P5_END_I,  // 5, IdProg; [  1]  branch_idx
    C62_SET_DEV        = 6,             // 6, IdProg; [  1]  dev_idx
    C62_SET_KBD_CHAR   = 7,             // 7, IdProg; [  1]  keyboard character
    C62_GET_TERM_CHAR  = 8,             // 8, IdProg; [0]
    C62_IDENTIFY_TYPE  = 9,             // 9, end of fast, IdProg


    // --- Slow (background) Commands ---
  /*
   * When powering on the M16C62 if the MCU wants to run IdBoot it must put previously on the bus
   * the value C62_SWITCH_PROG otherwise idBoot will jump to IdProg which in turn will run the its monitor
   */
    C62_SWITCH_PROG = 10,               // 10, IdProg, IdBoot; "Switch between Boot and ID program" {} []
    C62_ERASE_FLASH,                    // 11, IdProg, IdBoot; "Erase block(s)" {errnum, flsh_stat h/l} [bad_val h/l, bad_addr h/m/l]
    C62_PROG_FLASH,                     // 12, IdProg, IdBoot; "Flash sector"   {errnum, flsh_stat h/l} [exp_val, bad_val, bad_addr h/m/l]

    C62_P56_END_BG,                     // 13, end of shared, IdProg, IdBoot
    C62_READ_IDS = C62_P56_END_BG,      // 13, IdProg; "Read device IDs" {errnum} [n_devs for each branch or error info]
    C62_CALC_CRC = C62_P56_END_BG,      // 13, IdBoot; "Calc checksum"  {errnum, calcd_CRC h/l, saved_CRC h/l}

    C62_P5_END_BG,                      // 14, end of slow, IdBoot
    C62_READ_TEMPS = C62_P5_END_BG,     // 14, IdProg; "Read device temps" {errnum} [error info]

    C62_READ_TEMPS_BRANCH,              // 15, IdProg; "Read device temps for one branch" {errnum} [error info]
    C62_RUN_MONITOR,                    // 16, IdProg; "Run the monitor function to allow bebugging from MCU" {} []
    C62_P6_END_BG,                      // 17, end of slow, IdProg

    C62_SET_BIG_ENDIAN = C62_P6_END_BG, // 17, IdProg, IdBoot   Fast Command; [0]
    C62_SET_LITTLE_ENDIAN,              // 18, IdProg, IdBoot   Fast Command; [0]
    C62_SET_EXTERNAL_ID,                // 19, IdProg           Fast Command
    C62_GET_BARCODE_FOR_EXT_ID,         // 20, IdProg           Slow Command
    C62_SET_DEBUG_LEVEL,                // 21, IdProg, IdBoot   Fast Command; [  1]  db_lvl
} m16c62_commands_t;



#endif
