/*!
 *
 * File:         microlan_1wire.h
 * @brief
 */

#ifndef MICROLAN_1WIRE_H        // header encapsulation
#define MICROLAN_1WIRE_H

#ifdef MICROLAN_1WIRE_GLOBALS
#define MICROLAN_1WIRE_VARS_EXT
#else
#define MICROLAN_1WIRE_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>


// Constants

/*!
 * Part barcode length
 */
#define FGC_ELEMENT_BARCODE_LEN 20

/*!
 * Maximum number of 1-W slaves in the microlan network
 */
#define MICROLAN_NETWORK_ALL_ELEMENTS_MAX   100

/*

 FGC3 supports 6 branches
 FGC2 supports 5 branches (no magnet branch)

logical_path Coupler  ReadBack Branch       Use
    ---------------------------------------------------------------------------------------------
       0     0 Bus A   P1.1    Main         Voltage source DIM bus A
       1     0 Bus A   P1.1    Aux          Channel A ADC/DCCT
       2     1 Bus B   P1.2    Main         Voltage source DIM bus B
       3     1 Bus B   P1.2    Aux          Channel B ADC/DCCT
       4     2 Local   P1.0    Main         All FGC cards, chassis back plane, FGC and DCCT PSUs.
       5     2 Local   P1.0    Aux          All FGC cards, chassis back plane, FGC and DCCT PSUs.
       6       None            Direct       Link between DS2480 driver and DS2409 couplers
    ---------------------------------------------------------------------------------------------

    The identity of the couplers is determined using the switched coupler control output that
    is linked back to the M16C62 Port 1, bits 0 (local), 1 (A) and 2 (B).
*/

#define MICROLAN_NETWORK_LOGICAL_PATHS    6    // for FGC3 Todo is there a difference with FGC_ID_N_BRANCHES??

#define ID_LOGICAL_PATH_FOR_VS_A          0       // logical path for VS A
#define ID_LOGICAL_PATH_FOR_MEAS_A        1       // logical path for meas A
#define ID_LOGICAL_PATH_FOR_VS_B          2       // logical path for VS B
#define ID_LOGICAL_PATH_FOR_MEAS_B        3       // logical path for meas B
#define ID_LOGICAL_PATH_FOR_LOCAL         4       // logical path for local
#define ID_LOGICAL_PATH_FOR_CRATE         5       // logical path for crate


/*! Number of couplers DS2409 MicroLAN Coupler (connected to the DS2480 Serial 1-Wire Line Driver)
 * FGC2 has 3 DS2409 MicroLAN Coupler on the NDI-150 board
 * FGC3 has 3 DS2409 MicroLAN Coupler on the MB-100 Motherboard
 */
#define MICROLAN_NETWORK_INSTALLED_COUPLERS     3 // specific

// ----------------------------------------------------------------------------------------------
// MicroLAN (1-Wire bus) elements, 64 bits unique serial identification handling commands

#define MICROLAN_UNIQUE_SERIAL_CODE_LEN          8   // 64 bits


// --- Temperature macros ---

#define ID_MIN_TEMP             5       // Valid temp sensor range
#define ID_MAX_TEMP             95      // in Celcius
#define ID_BAD_TEMP             99      // in Celcius
#define ID_TEMP_GAIN_PER_C      16      // Raw/degree (i.e. 0.0625C per bit)

#define ID_TEMP_FRAC_C(raw_temperature)     ( ( ( raw_temperature & 0x0F ) * 25 + 2 ) / 4 )
#define ID_TEMP_FLOAT(raw_temperature)      (1.0 / ID_TEMP_GAIN_PER_C) * (FP32) raw_temperature


// External structures, unions and enumerations


enum microlan_1wire_family_codes
{
	MICROLAN_DEVICE_DS2409_COUPLER        =  0x1F, // specific
	MICROLAN_DEVICE_DS2401_ONLY_ID        =  0x01,
	MICROLAN_DEVICE_DS18B20_THERMOMETER   =  0x28,
	MICROLAN_DEVICE_DS2450_ADC            =  0x20
};

/*!
 * MicroLAN (1-Wire bus) elements have an unique 64 bits serial code, composed of
 * Family code (LS Byte)
 * Unique ID (6 bytes)
 * CRC of the previous bytes (MS Byte)
 */
struct TMicroLAN_unique_serial_code_pieces
{
    INT8U               family;
    INT8U               usc[6];
    INT8U               crc;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;

union TMicroLAN_unique_serial_code
{
    INT8U                                       b[MICROLAN_UNIQUE_SERIAL_CODE_LEN];
    struct TMicroLAN_unique_serial_code_pieces  p;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;

/*!
 * Stores information about the microlan network
 */
struct TBranchSummary
{
    INT8U               total_devices;                  //!< Number of Dallas devices detected in total
    INT8U               ds2409s;                        //!< Number of coupler devices (DS2409) detected ---> try to remove
    INT8U               ds2401s;                        //!< Number of ID only devices (DS2401) detected
    INT8U               ds18b20s;                       //!< Number of ID+temp devices (DS18B20) detected
    INT8U               unknow_devices;                 //1< Number of other devices detected
    INT8U               max_reads;                      //!< Max number of reads required on the branch
    INT8U               start_in_elements_summary;      //!<s Table index for first device for this branch
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;



struct barcode
{
    char                c[FGC_ELEMENT_BARCODE_LEN];     //!< 20 chars, Type code string: e.g. "HCFBMA___-GL\n"
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;

struct TMicroLAN_element_summary
{
    INT8U               idx_in_branch;                  //!< [0..MICROLAN_NETWORK_ELEMENTS_PER_BRANCH_MAX]
    INT8U               logical_path;                   //!< in which logical path is located
    INT8U               temp_C;                         //!< Temperature (Units: C) (0=No dev or no data)
    INT8U               temp_C_16;                      //!< Temperature fraction (Units: 1/16 C)
    union  TMicroLAN_unique_serial_code unique_serial_code;
    struct barcode      barcode;                        //!< Barcode
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(1)))
#endif
;


struct TNetworkInfo
{
    union  TMicroLAN_unique_serial_code  couplers_ids[MICROLAN_NETWORK_INSTALLED_COUPLERS]; // 0:A 1:B 2:Local
    struct TBranchSummary                branches_summary[MICROLAN_NETWORK_LOGICAL_PATHS];
    struct TMicroLAN_element_summary     elements_summary[MICROLAN_NETWORK_ALL_ELEMENTS_MAX];
};

// External variables

/*!
 * CRC polynomial X^8 + X^5 + X^4 + 1
 */
MICROLAN_1WIRE_VARS_EXT const INT8U               microlan_crc_table[256]
#ifdef MICROLAN_1WIRE_GLOBALS
    = { 0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
        157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
        35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
        190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
        70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
        219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
        101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
        248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
        140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
        17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
        175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
        50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
        202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
        87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
        233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
        116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
      }
#endif
      ;

#endif  // MICROLAN_1WIRE_H end of header encapsulation

