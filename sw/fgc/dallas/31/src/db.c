/*---------------------------------------------------------------------------------------------------------*\
  File:         db.c

  Purpose:      C62 ID program - Database functions

  Notes:        The C62 ID program reads the unique ID number for every component on every branch
                of the 1-wire bus linked to the Dallas DS2480 line driver.  These numbers correspond
                to physical electronics cards, DCCTs heads or electronics or ADCs.  The relationship
                between the ID number and the type and serial number of the tagged thing is stored in
                the AB-PO Oracle database.  However, it useful for an FGC to be able to report exactly
                what is connected to it, and what it is composed of, so a local copy of the database
                is stored in the C62 flash memory.  The C62 has 4 pages of 64KB of flash.  Of these,
                three pages, plus one 8KB block in the fourth page are available to store the database.
                The LHC powering system will have a large number of identified devices:

                        FGC cards               7 x 2000        14000
                        DIM cards               1 x 3600         3600
                        Backplanes 60A          1 x  850          800
                        Backplanes              2 x  750         1500
                        FGC PSUs                2 x  700         1400
                        DCCT PSUs               2 x  400          800
                        DCCTs/ADCs              3 x 1000         2000
                        Switched PCs             ~ 12000        12000
                        Thyristor PCs                400          400
                        TOTAL                                   36500

                190000/36500 allows 5.2 bytes per device entry.  Taking 5 bytes allows up to 390000
                devices to be in the database.  An ID number is 8 bytes long, so serious compression is
                required.  Here is a typical number:

                                        7F 00 00 0A 24 E6 98 01
                                         | --+--  | ----+---  |
                                         |   |    |     |     +-- Type byte (01=IdOnly 28=IdTemp etc...)
                                         |   |    |     +-------- Essential 24-bits of ID
                                         |   |    +-------------- Table index byte
                                         |   +------------------- Assumed to be always zero
                                         +----------------------- Checksum byte

                So although the ID contains 8 bytes, we can organise the database into two major blocks,
                one for type 01 and the other for type 28, and then into tables for each group of IDs
                that have the same Table Index byte.  Finally, within the ID table, only the essential
                24-bit value must be stored, in numerical order to enable a binary search, together
                with a part index word.

                Once the search locates the ID, the part index will be used to search for the part
                type and serial number in a second database table stored in the 8KB flash block (1)
                in page F.
\*---------------------------------------------------------------------------------------------------------*/

#include <db.h>

#include <term.h>


#include <string.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------------------*/
void DbLookup(struct TMicroLAN_element_summary *dev)
/*---------------------------------------------------------------------------------------------------------*\
  This function will look up the device identified by dev in the C62 internal database of LHC parts.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      idx;
    INT16U      first_idx;
    INT16U      last_idx;
    INT8U       type;
    INT8U       tbl;
    INT8U       id3;
    INT16U      id12;
    INT8U       db_id3;
    INT16U      db_id12;
    INT16U      part_idx;

    // --- Check if DB is installed ---

    if ( (DB_DIR[0].tbl_byte != 0) || (DB_PT[0].pt_base != 0) ) // If no database installed
    {
        strcpy(dev->barcode.c,"no_ID_DB_installed");
        return;
    }

    // --- Linear search DIR2 for type record ---

    type     = dev->unique_serial_code.p.family;// Extract type byte from device ID
    last_idx = DB_DIR[0].last_idx;              // Extract last  index for DIR2 zone from DIR1 record
    idx      = DB_DIR[0].first_idx;             // Extract first index for DIR2 zone from DIR1 record

    if ( term.db_lvl >= 2 )
    {
        fprintf(TX_TERM, "type=%02x first_dir2=%u last_dir2=%u\n\r", type, idx, last_idx);
    }

    while (    (idx  <= last_idx)                       // Linear search DIR2 looking for type byte
            && (type >  DB_DIR[idx].tbl_byte)
          )
    {
        idx++;
    }

    if (    (idx > last_idx)                    // If no match for type byte
         || (type != DB_DIR[idx].tbl_byte)
        )
    {
        strcpy(dev->barcode.c,"unknown_dev_type");
        return;
    }

    if ( term.db_lvl >= 2 )
    {
        fprintf(TX_TERM, "dir2_idx=%u\n\r", idx);
    }

    // --- Linear search DIR3 for ID4/TBL record ---

    tbl      = dev->unique_serial_code.b[4];    // Extract ID4/TBL byte from device ID
    last_idx = DB_DIR[idx].last_idx;            // Extract last index for DIR3 zone from DIR2 record
    idx      = DB_DIR[idx].first_idx;           // Extract first index for DIR3 zone from DIR2 record

    if ( term.db_lvl >= 2 )
    {
        fprintf(TX_TERM,"tbl=%02x first_dir3=%u last_dir3=%u\n\r", tbl, idx, last_idx);
    }

    while (    (idx <= last_idx)                        // Linear search DIR3 zone looking for ID4/TBL byte
            && (tbl > DB_DIR[idx].tbl_byte)
          )
    {
        idx++;
    }

    if (    (idx > last_idx)                    // If no match for ID4/TBL byte
         || (tbl != DB_DIR[idx].tbl_byte)
        )
    {
        strcpy(dev->barcode.c,"unknown_ID4_byte");
        return;
    }

    if ( term.db_lvl >= 2 )
    {
        fprintf(TX_TERM,"dir3_idx=%u\n\r", idx);
    }

    // --- Binary search ID for ID123 record ---

    id3       = dev->unique_serial_code.b[3];                   // Extract ID3 byte from device ID
    id12      = *((INT16U *)(dev->unique_serial_code.b + 1));   // Extract ID12 word from device ID
    last_idx  = DB_DIR[idx].last_idx;           // Extract last index for ID zone from DIR3 record
    first_idx = DB_DIR[idx].first_idx;          // Extract first index for ID zone from DIR3 record

    if ( term.db_lvl >= 2 )
    {
        fprintf(TX_TERM,"id3=%02x id12=%04x first_idx=%u last_idx=%u\n\r", id3, id12, first_idx, last_idx);
    }

    do
    {
        idx = ((INT32U)first_idx + (INT32U)last_idx) / 2; // Choose record in the middle of the zone (32-bit)

        db_id3 = DB_ID[idx].id3;                // Extract ID3 byte from DB record

        if ( term.db_lvl >= 3 )
        {
            fprintf(TX_TERM,"idx=%u id3=%02x first_idx=%u last_idx=%u\n\r", idx, db_id3, first_idx, last_idx);
        }

        if ( id3 == db_id3 )                    // If device ID3 matches DB ID3
        {
            db_id12 = DB_ID[idx].id12;                  // Extract ID12 word from ID record

            if ( id12 == db_id12 )                              // If device ID12 matches DB ID12
            {
                part_idx = DB_ID[idx].part_idx;                 // Extract part type index from DB record

                for(idx=0;part_idx >= DB_PT[idx+1].pt_base;idx++);// Linear search PT database for index
                {
                }

                sprintf(dev->barcode.c,"%s%06lu",               // Transfer barcode to device scan result
                                        DB_PT[idx].type_code,
                                        DB_PT[idx].serial_base + (INT32U)(part_idx - DB_PT[idx].pt_base));
                return;                                         // Return
            }

            if ( id12 < db_id12 )                               // else if device ID12 is less than DB ID12
            {
                last_idx = idx - 1;                             // Shift end of zone down to current position
            }
            else                                        // else device ID12 is greater than DB ID12
            {
                first_idx = idx + 1;                            // Shift start of zone up to current position
            }
        }
        else
        {
            if(id3 < db_id3)                            // else if device ID3 is less than DB ID3
        {
            last_idx = idx - 1;                         // Shift end of zone down to current position
        }
        else                                    // else device ID3 is greater than DB ID3
        {
            first_idx = idx + 1;                        // Shift start of zone up to current position
            }
        }
    }
    while(first_idx <= last_idx);               // Loop while zone has not disappeared completely

    strcpy(dev->barcode.c,"unknown_ID123_bytes");
    return;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: db.c
\*---------------------------------------------------------------------------------------------------------*/
