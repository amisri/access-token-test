/*!
 * @file oneWire.c
 * @brief 
 */

#define ONEWIRE_MASTER_GLOBALS


// Includes

#include <onewire_master.h>

#include <ds2409.h>
#include <ds2480.h>
#include <iodefines.h>
#include <max4638.h>
#include <main.h>
#include <mcu_dependant.h>
#include <term.h>

#include <stdint.h>
#include <string.h>

// Constants


// Types

/*!
 * Type defining a function to send a byte to the one wire master.
 * @param tx_data. Byte to transmit to the one wire master.
 * @param expected_bytes. Number of expected bytes from the one wire master after sending a command.
 * @param timeout_ms. Maximum time in milliseconds to execute the command and get the response.
 * @return true if the command was executed with success.
 */
typedef bool (*onewire_maseter_send_byte_handler)(const uint8_t tx_data, const uint8_t expected_bytes, const uint16_t timeout_ms);


/*
 * Type defining a pointer to a function accepting no parameters and returning bool.
 * It is the Dallas ID Find function prototype, and also the enable and disable pull up functions.
 * @return true if the command was executed with success.
 */
typedef bool (*onewire_master_no_args_handler)(void);


/*!
 * Type defining a function pointer to issue commands on a branch (convert temperature).
 * @param The logical path (branch) to which the convert temperature command will be issued.
 * @return true if the command was executed with success.
 */
typedef bool (*onewire_master_one_branch_handler)(const uint8_t logical_path);


/*!
 * Type defining a pointer to a function accepting two parameters and returning bool.
 * It is the Read Temperature function prototype.
 * @param start_branch. First branch to read temperature from.
 * @param start_branch. Last branch to read temperature from.
 * @return true if the command was executed with success.
 */
typedef bool (*onewire_master_read_temps_handler)(const uint8_t start_branch, const uint8_t end_branch);


/*!
 * Type defining a pointer to a function accepting one bool parameter.
 * It is used for the one wire master and 1-W reset function, which may accept a boolean parameter
 * asking for a soft or hard reset.
 */
typedef bool (*onewire_master_reset_handler)(const bool request_hard_reset);


/*!
 * Type defining a function pointer to the function that searches for the Dallas IDs.
 */
typedef bool (*onewire_master_find_id_handler)(const uint8_t logical_path, uint16_t* logical_element);




// Internal structures, unions and enumerations 


// Internal function declarations


static bool oneWireMasterSendByte(const uint8_t tx_data, const uint8_t expected_bytes, const uint16_t timeout_ms);


/*!
 * This function will read the pre-converted temperature from the specified DS18B20 device. It will retry in case of CRC mismatch.
 * The temperatures are reported as follows:
 * 0    = Not a temperature device
 * 5    = 5C or less
 * 6-94 = Measured temperature
 * 95   = 95C or more
 * 99   = Bad reading (CRC check failed 5 times)
 * If there are no errors it will return TRUE.
 */
static bool oneWireMasterReadOneTemp(const uint8_t logical_element);


/*!
 * This function makes the one wire master to send a convert temperature order to the temperature sensors.
 * @return True if the operation finishes successfully.
 */
static bool oneWireConvertTempCommon(void);


// Internal variable definitions


/*!
 * Variable set to point to a function to send a byte to the one wire master, depending on the HW architecture.
 */
static onewire_maseter_send_byte_handler onewire_master_send_byte;


/*!
 * Variable set to point to a function to reset the 1-W master chip and send a 1-W reset over the 1-W line.
 */
static onewire_master_reset_handler onewire_master_reset;


/*!
 * Variable set to point the tasks needed to be executed prior to the dallas ID search.
 * These tasks depend on the HW platform.
 */
static onewire_master_no_args_handler onewire_master_find_ids_pre;


/*!
 * Variable set to point to a Find Dallas ID function, depending on the HW architecture.
 */
static onewire_master_find_id_handler onewire_master_find_ids;



/*!
 * Variable set to point to a enable pull up function, depending on the HW architecture.
 */
static onewire_master_no_args_handler onewire_master_enable_pullup;


/*!
 * Variable set to point to a disable pull up function, depending on the HW architecture.
 */
static onewire_master_no_args_handler onewire_master_disable_pullup;


/*!
 * Variable set to point to a convert temperature function, depending on the HW architecture.
 */
static onewire_master_one_branch_handler onewire_master_convert_temp;


/*!
 * Variable set to point to a Read Temperature function, depending on the HW architecture.
 */
static onewire_master_no_args_handler onewire_master_read_temps_pre;


// External function definitions

void oneWireMasterSetMax4638Functions(void)
{
	onewire_master_send_byte		= ds2480SendByte;
	onewire_master_reset			= ds2480Detect;
	onewire_master_enable_pullup	= ds2480EnablePullUp;
	onewire_master_disable_pullup	= ds2480DisablePullUp;

	// Nothing to do prior to the search in this case
	onewire_master_find_ids_pre 	= NULL;
	onewire_master_find_ids 		= max4638FindIDs;
	onewire_master_convert_temp 	= max4638ConvertTemp;
	onewire_master_read_temps_pre   = max4638ReadTempsPre;
}



void oneWireMasterSetDs2409Functions(void)
{
	onewire_master_send_byte		= ds2480SendByte;
	onewire_master_reset			= ds2480Detect;
	onewire_master_enable_pullup	= ds2480EnablePullUp;
	onewire_master_disable_pullup	= ds2480DisablePullUp;
	onewire_master_find_ids_pre		= ds2409FindIDsPre;
	onewire_master_find_ids 		= ds2409FindIDs;
	onewire_master_convert_temp 	= ds2409ConvertTemp;
	onewire_master_read_temps_pre   = ds2409ReadTempsPre;

}



onewire_master_cmd_results_t oneWireMasterReset(const bool request_hard_reset)
{
	if (!onewire_master_reset(request_hard_reset))
	{
		return ONEWIRE_MASTER_COMMAND_ERROR;
	}

	return ONEWIRE_MASTER_COMMAND_OK;
}



onewire_master_cmd_results_t oneWireMasterSelectDeviceInMicroLAN(const union TMicroLAN_unique_serial_code FAR *serial_code_ptr)
{
    uint8_t ii;

    if (term.db_lvl >= 2)
    {
        fprintf(TX_TERM, "SelectDevice(0x%lX->[%s])\n\r", (uint32_t) serial_code_ptr, utilitiesDallasIdToString(serial_code_ptr));
    }

    if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_SOFT_RESET) != ONEWIRE_MASTER_COMMAND_OK )
    {
        return ONEWIRE_MASTER_COMMAND_ERROR;
    }

    onewire_master_send_byte(ONEWIRE_MASTER_MATCH_ROM, 0, 0);

    for (ii = 0; ii < MICROLAN_UNIQUE_SERIAL_CODE_LEN; ii++)
    {
    	onewire_master_send_byte(serial_code_ptr->b[ii], 0, 0);
    }

    USLEEP(DS2480_WAIT_RESET_US);

    return ONEWIRE_MASTER_COMMAND_OK;
}



onewire_master_cmd_results_t oneWireMasterFindIDs(void)
{

	uint8_t  logical_path;
	uint16_t logical_element;

	logical_element = 0;

	if ( onewire_master_find_ids_pre != NULL )
	{
		onewire_master_find_ids_pre();
	}


	for (logical_path = 0; logical_path < hardware.n_branches; logical_path++)
	{

		if (!onewire_master_find_ids(logical_path, &logical_element))
		{
			return ONEWIRE_MASTER_COMMAND_ERROR;
		}

	}

	// If scan is different to last time
	if (memcmp((void FAR *)&one_wire_net, (void FAR *) C62_BLK2_ADDR, sizeof(struct TNetworkInfo)) != 0)
	{
		data_exchange.flash_needs_refresh = true;
	}


	if ( (oneWireMasterReadTemperatures( 0, hardware.n_branches - 1)) != ONEWIRE_MASTER_COMMAND_OK)
	{
		return false;
	}


	data_exchange.rsp.err.code = M16C62_NO_ERROR;

	for (logical_path = 0; logical_path < hardware.n_branches; logical_path++)
	{
		data_exchange.rsp.bytes[1 + logical_path] =
			one_wire_net.branches_summary[logical_path].total_devices;
	}

	data_exchange.rsp_bytes_in_buffer = 1 + hardware.n_branches;

	return ONEWIRE_MASTER_COMMAND_OK;
}



onewire_master_cmd_results_t oneWireMasterReadTemperatures(const uint8_t start_branch, const uint8_t end_branch)
{
	uint8_t ii;
	uint8_t logical_element;
	uint8_t logical_path;

	onewire_master_read_temps_pre();

	if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_HARD_RESET) != ONEWIRE_MASTER_COMMAND_OK )
	{
		return ONEWIRE_MASTER_COMMAND_ERROR;
	}

	for (logical_path = start_branch; logical_path <= end_branch; logical_path++)
	{
		if (one_wire_net.branches_summary[logical_path].ds18b20s == 0)
		{
			continue;
		}

		if (!onewire_master_convert_temp(logical_path))
		{
			return ONEWIRE_MASTER_COMMAND_ERROR;
		}

		if (!oneWireConvertTempCommon())
		{
			return ONEWIRE_MASTER_COMMAND_ERROR;
		}

		for (ii = 0; ii < one_wire_net.branches_summary[logical_path].total_devices; ii++)
		{
			// Get index to device data
			logical_element = one_wire_net.branches_summary[logical_path].start_in_elements_summary + ii;

			if (one_wire_net.elements_summary[logical_element].unique_serial_code.p.family == MICROLAN_DEVICE_DS18B20_THERMOMETER)
			{
				if (!oneWireMasterReadOneTemp(logical_element))
				{
					return ONEWIRE_MASTER_COMMAND_ERROR;
				}
			}
		}

	}

	return ONEWIRE_MASTER_COMMAND_OK;
}



// Internal function definitions


static bool oneWireConvertTempCommon(void)
{

	if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_SOFT_RESET) != ONEWIRE_MASTER_COMMAND_OK )
	{
		return false;
	}

	if (!ds2480SendByte(ONEWIRE_MASTER_SKIP_ROM, 1, DS2480_RESPONSE_TIMEOUT_MS))
	{
		return false;
	}

	if (!ds2480SendByte(ONEWIRE_MASTER_DS18B20_CMD_WRITE_SCRATCHPAD, 1, DS2480_RESPONSE_TIMEOUT_MS))
	{
		return false;
	}

	if (!ds2480SendByte(0x4B, 1, DS2480_RESPONSE_TIMEOUT_MS))
	{
		return false;
	}

	if (!ds2480SendByte(0x46, 1, DS2480_RESPONSE_TIMEOUT_MS))
	{
		return false;
	}

	if (!ds2480SendByte(ONEWIRE_MASTER_DS18B20_CONFIG_12BITS_750MS, 1, DS2480_RESPONSE_TIMEOUT_MS))
	{
		return false;
	}

	if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_SOFT_RESET) != ONEWIRE_MASTER_COMMAND_OK )
	{
		return false;
	}

	if (!ds2480SendByte(ONEWIRE_MASTER_SKIP_ROM, 1, DS2480_RESPONSE_TIMEOUT_MS))
	{
		return false;
	}

	if (!onewire_master_enable_pullup())
	{
		return false;
	}

	if (!ds2480SendByte(ONEWIRE_MASTER_DS18B20_CMD_CONVERT_TEMP, 2, ONEWIRE_MASTER_DS18B20_ACQ_TEMPERATURE_TIMEOUT))
	{
		return false;
	}

	if (!onewire_master_disable_pullup())
	{
		return false;
	}

	return true;
}



static bool oneWireMasterSendByte(const uint8_t tx_data, const uint8_t expected_bytes, const uint16_t timeout_ms)
{
	return onewire_master_send_byte(tx_data, expected_bytes, timeout_ms);
}



static bool oneWireMasterReadOneTemp(const uint8_t logical_element)
{

    uint8_t ii;
    uint8_t reads = 0;
    int16_t temp;
    uint8_t crc;
    uint8_t scrpad[ONE_WIRE_MASTER_DS18B20_SCRATCHPAD_SIZE];

    if (term.db_lvl >= 2)
    {
        fprintf(TX_TERM, "ReadOneTemp(%u)\n\r", logical_element);
    }


    while (reads < ONE_WIRE_MASTER_MAX_RETRIES)
    {

        if ( oneWireMasterSelectDeviceInMicroLAN(&one_wire_net.elements_summary[logical_element].unique_serial_code) != ONEWIRE_MASTER_COMMAND_OK )
        {
            return false;
        }

        oneWireMasterSendByte(ONEWIRE_MASTER_DS18B20_CMD_READ_SCRATCHPAD, 1, DS2480_RESPONSE_TIMEOUT_MS);

        // Read 9-byte scratchpad (temp is in first two bytes)
        for (crc = ii = 0; ii < ONE_WIRE_MASTER_DS18B20_SCRATCHPAD_SIZE; ii++)
        {
        	oneWireMasterSendByte(ONEWIRE_MASTER_DS18B20_CONFIRM_SCRATCHPAD_READ, 1, DS2480_RESPONSE_TIMEOUT_MS);

            scrpad[ii] = ds2480_state.data_buffer[0];

            // Calculate checksum
            if (ii < 8)
            {
            	crc = utilitiesCalculateCRC8(crc, ds2480_state.data_buffer[0]);
            }
        }

        // If CRC is OK
        if (crc == scrpad[8])
        {
        	// Save temp in raw units (0.0625C)
            temp = *((int16_t *)scrpad);

            // Clip below 5C
            if (temp < (ID_MIN_TEMP * ID_TEMP_GAIN_PER_C))
            {
                one_wire_net.elements_summary[logical_element].temp_C = ID_MIN_TEMP;
            }
            else
            {
            	// Clip above 95C
                if (temp > (ID_MAX_TEMP * ID_TEMP_GAIN_PER_C))
                {
                    one_wire_net.elements_summary[logical_element].temp_C = ID_MAX_TEMP;
                }
                else
                {
                	// Temp in Celcius
                    one_wire_net.elements_summary[logical_element].temp_C    = (temp >> 4);

                    // Fractional temp in 1/16 Celcius
                    one_wire_net.elements_summary[logical_element].temp_C_16 = (temp & 0x0F);
                }
            }

            if (term.db_lvl != 0)
            {
                fprintf(TX_TERM, "ReadOneTemp(%s): 0x%04X %3u.%02u C\n\r",
                        utilitiesDallasIdToString(&one_wire_net.elements_summary[logical_element].unique_serial_code),
                        temp,
                        one_wire_net.elements_summary[logical_element].temp_C,
                        ID_TEMP_FRAC_C(one_wire_net.elements_summary[logical_element].temp_C_16));
            }

            return true;
        }
        else
        {
            reads++;
        }

    }


    if (term.db_lvl != 0)
    {
        fprintf(TX_TERM, "ReadOneTemp(%u): bad CRC\n\r", logical_element);
    }

    // 99C = Bad reading
    one_wire_net.elements_summary[logical_element].temp_C = ID_BAD_TEMP;

    // Do not report error, just signal temp is bad by 99C
    return true;

}

// EOF
