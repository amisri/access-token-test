/*---------------------------------------------------------------------------------------------------------*\
  File:         ds2480.c

  Purpose:      Dallas bus functions

  Notes:        This software communicates with a Dallas MicroLan network through a
                DS2480 Serial 1-Wire Line Driver which is connected to

        UART0 for FGC2
        UART2 for FGC3


                The output from the DS2480 (Serial 1-Wire Line Driver) goes to three DS2409 MicroLan
                couplers called Bus A, Bus B and Local.
                Each coupler has two output branches and a digital control signal which can be switched
                on command.
                Each output branch from a DS2409 can be enabled or disabled independently.
                Only one branch from the local bus coupler is used (main), while both branches from the
                Bus A and B couplers are used (main and aux).
                Thus, there are five Dallas 1-wire branches in the FGC2 cassette,
                together with the direct "link/branch" between the DS2480 and the three DS2409s:

            logical path Coupler  ReadBack Branch       Use
                ---------------------------------------------------------------------------------------------
                   0     0    Bus A     P1.1    Main    Voltage source DIM bus A
                   1     0    Bus A     P1.1    Aux     Channel A ADC/DCCT
                   2     1    Bus B     P1.2    Main    Voltage source DIM bus B
                   3     1    Bus B     P1.2    Aux     Channel B ADC/DCCT
                   4     2    Local     P1.0    Main    All FGC cards, chassis back plane, FGC and DCCT PSUs.
                ---------------------------------------------------------------------------------------------
                FGC2
                   5     -    None              Direct   Link between DS2480 driver and DS2409 couplers
                ---------------------------------------------------------------------------------------------
                FGC3
                   5     2    Local     P1.0    Aux     All FGC cards, chassis back plane, FGC and DCCT PSUs.
                   6     -    None              Direct  Link between DS2480 driver and DS2409 couplers
                ---------------------------------------------------------------------------------------------

                The identity of the couplers is determined using the switched coupler control output that
                is linked back to the M16C62 Port 1, P1.b0=Local, P1.b1=Bus A and P1.b2=Bus B.

\*---------------------------------------------------------------------------------------------------------*/

#define DS2409_GLOBALS

// Includes

#include <ds2409.h>

#include <ds2480.h>
#include <iodefines.h>
#include <main.h>
#include <mcu_dependant.h>
#include <onewire_master.h>
#include <term.h>

#include <stdio.h>
#include <string.h>


// Constants

/*!
 * Indicates that the couplers (DS2409) prensence must be checked when the
 * function ds2480FindMicroLANDevicesCommon is used.
 */
static const bool DS2409_CHECK_COUPLERS_PRESENCE = true;


/*!
 * Tells whether to send an error code back to the M16C62
 */
static const bool DS2409_SEND_ERROR_CODE = true;


/*!
 * Delay required for PS MPS long cables
 */
static const uint16_t DS2409_WAIT_LONG_CABLES_US  = 1000;



// Internal structures, unions and enumerations

/*!
 * Commands sent from the DS2480B to the DS2409 couplers
 */
static enum ds2409_commands
{
	DS2409_CTRL_FUNC_CMD_ALL_LINES_OFF         = 0x66,
	DS2409_CTRL_FUNC_CMD_ACTIVATE_MAIN_BRANCH  = 0xCC,    //!< SMART-ON MAIN (limit the number of devices participating in a Search ROM command)
	DS2409_CTRL_FUNC_CMD_ACTIVATE_AUX_BRANCH   = 0x33,    //!< SMART-ON AUXILIARY (limit the number of devices participating in a Search ROM command)
	DS2409_CTRL_FUNC_CMD_R_W_STATUS            = 0x5A,    //!< STATUS READ/WRITE
	DS2409_CTRL_DISABLE_OUTPUTS    			   = 0x20,
	DS2409_CTRL_ENABLE_OUTPUTS    			   = 0xA0,
	DS2409_RESET_STIMULUS    	   			   = 0xFF
};



// Internal function declarations

/*!
 * We assume that the DS2480 is in DATA mode.
 * This function will send the STATUS READ/WRITE command to the selected coupler(s).
 * This command should be sent to the device after powering up unless the default settings
 * are adequate for the application.
 * It will return the received (via the UART RX interrupt) status byte in ds2480_state.data_buffer
 */
static void ds2409CouplerSequence(const uint8_t ctrl);


/*!
 * From the logical path, a branch of one coupler is selected:
 * logical path Coupler  ReadBack Branch
	  ----------------------------------
		 0     0 Bus A   p1.1    Main
		 1     0 Bus A   p1.1    Aux
		 2     1 Bus B   p1.2    Main
		 3     1 Bus B   p1.2    Aux
		 4     2 Local   p1.0    Main
		 5     2 Local   p1.0    Aux
		 6       None            Direct
 * @param Logical path. Each logical path is a branch on the 1-W network.
 * @param send_error_code: whether to send error back to the M16C62 or not.
 */
static bool ds2409SelectBranch(const uint8_t logical_path, const bool send_error_code);



// Internal variable definitions
// Internal function definitions


static void ds2409CouplerSequence(const uint8_t ctrl)
{

    ds2480SendByte(DS2409_CTRL_FUNC_CMD_R_W_STATUS, 1, DS2480_RESPONSE_TIMEOUT_MS);

    ds2480SendByte(ctrl, 1, DS2480_RESPONSE_TIMEOUT_MS);

    ds2480SendByte(DS2409_RESET_STIMULUS, 1, DS2480_RESPONSE_TIMEOUT_MS);
}



static bool ds2409SelectBranch(const uint8_t logical_path, const bool send_error_code)
{

	if ( oneWireMasterSelectDeviceInMicroLAN(&one_wire_net.couplers_ids[logical_path >> 1]) != ONEWIRE_MASTER_COMMAND_OK )
	{
		return false;
	}


	// paths 0,2,4 are Main Branch
	if ((logical_path & 0x01) == 0)
	{
		if ( ! ds2480SendByte(DS2409_CTRL_FUNC_CMD_ACTIVATE_MAIN_BRANCH, 1, DS2480_RESPONSE_TIMEOUT_MS) )
		{
			return false;
		}
	}
	else
	{
		if ( ! ds2480SendByte(DS2409_CTRL_FUNC_CMD_ACTIVATE_AUX_BRANCH, 1, DS2480_RESPONSE_TIMEOUT_MS) )
		{
			return false;
		}
	}

	if ( ! ds2480SendByte(DS2409_RESET_STIMULUS, 1, DS2480_RESPONSE_TIMEOUT_MS) )
	{
		return false;
	}

	USLEEP(DS2409_WAIT_LONG_CABLES_US);

	if ( ! ds2480SendByte(DS2409_RESET_STIMULUS, 1, DS2480_RESPONSE_TIMEOUT_MS) )
	{
		return false;
	}


	// If no presence pulses detected
	if (ds2480_state.data_buffer[0] == 0xFF)
	{
		if (term.db_lvl != 0)
		{
			fputs("Find devices: no presence\n\r", TX_TERM);
		}

		if ( send_error_code )
		{
			data_exchange.rsp.err.code      		= ERR_MICROLAN_LOGICAL_PATH_NO_DEVICES;
			data_exchange.rsp.err.v.logical_path  	= logical_path;
			data_exchange.rsp_bytes_in_buffer 		= 2;
			return false;
		}
		else
		{
			// ok to have no devices
			return true;
		}
	}

	ds2480SendByte(DS2409_RESET_STIMULUS, 1, DS2480_RESPONSE_TIMEOUT_MS);

	return true;
}



// External function definitions


bool ds2409CheckIdentifyCouplers(void)
{
    uint8_t  coupler;
    uint8_t  ii;
    uint8_t  readback_when_high_port1;
    uint8_t  readback_when_low_port1;

    const uint8_t port1_value_to_coupler_index[8] = {3, 2, 0, 3, 1, 3, 3, 3};
    const char FAR * const DS2409_NAME = "ABL";


    // Do a Master reset and turn off all coupler branches
    if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_HARD_RESET) != ONEWIRE_MASTER_COMMAND_OK )
    {
        return false;
    }

    ds2480SendByte(ONEWIRE_MASTER_SKIP_ROM, 1, DS2480_RESPONSE_TIMEOUT_MS);

    ds2480SendByte(DS2409_CTRL_FUNC_CMD_ALL_LINES_OFF, 1, DS2480_RESPONSE_TIMEOUT_MS);


    // Set all coupler control lines to high and readback via Port 1 bits [b2 b2 b0]
    if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_SOFT_RESET) != ONEWIRE_MASTER_COMMAND_OK )
    {
        return false;
    }

    ds2480SendByte(ONEWIRE_MASTER_SKIP_ROM, 1, DS2480_RESPONSE_TIMEOUT_MS);

    // Set coupler control outputs inactive (high)
    ds2409CouplerSequence(DS2409_CTRL_DISABLE_OUTPUTS);

    readback_when_high_port1 = (p1 & 0x07);


    // Set all coupler control lines to low and readback via Port 1 bits [b2 b2 b0]
    if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_SOFT_RESET) != ONEWIRE_MASTER_COMMAND_OK )
    {
        return false;
    }

    ds2480SendByte(ONEWIRE_MASTER_SKIP_ROM, 1, DS2480_RESPONSE_TIMEOUT_MS);

    ds2409CouplerSequence(DS2409_CTRL_ENABLE_OUTPUTS);

    readback_when_low_port1 = (p1 & 0x07);


    // if lines are NOT OK
    if ((readback_when_high_port1 != 0x07)
        || (readback_when_low_port1  != 0x00)
       )
    {
        if (term.db_lvl != 0)
        {
            fprintf(TX_TERM, "DS2409 Control line fault: in H:0x%02X in L:0x%02X\n\r", readback_when_high_port1,
                    readback_when_low_port1);
        }

        data_exchange.rsp.err.code = ERR_MICROLAN_COUPLER_CONTROL_LINES;

        if (hardware.mcu_transmit_in_big_endian == true)
        {
            data_exchange.rsp.err.v.control.be.hi = readback_when_high_port1;
            data_exchange.rsp.err.v.control.be.lo = readback_when_low_port1;
        }
        else
        {
        	// Store control high value (should be 0)
            data_exchange.rsp.err.v.control.part.lo = readback_when_low_port1;

            // Store control high value (should be 7)
            data_exchange.rsp.err.v.control.part.hi = readback_when_high_port1;
        }

        data_exchange.rsp_bytes_in_buffer = 3;
        return false;
    }


    if (!ds2480FindMicroLANDevicesCommon(hardware.n_branches, DS2409_CHECK_COUPLERS_PRESENCE))
    {
        return false;
    }

    if (ds2480_state.branch_summary.ds2409s != MICROLAN_NETWORK_INSTALLED_COUPLERS)
    {
        if (term.db_lvl != 0)
        {
            fprintf(TX_TERM, "DS2409 Invalid number of couplers:%u\n\r", ds2480_state.branch_summary.ds2409s);
        }

        data_exchange.rsp.err.code = ERR_MICROLAN_COUPLERS_FOUND;

        // Store number of couplers detected in response buffer
        data_exchange.rsp.err.v.number_of_couplers = ds2480_state.branch_summary.ds2409s;
        data_exchange.rsp_bytes_in_buffer      = 2;
        return false;
    }

    if (ds2480_state.branch_summary.total_devices != MICROLAN_NETWORK_INSTALLED_COUPLERS)
    {
        if (term.db_lvl != 0)
        {
            fprintf(TX_TERM, "Scan DS2409: Unexpected devices:%u\n\r",
                    (ds2480_state.branch_summary.total_devices - MICROLAN_NETWORK_INSTALLED_COUPLERS));
        }

        data_exchange.rsp.err.code = ERR_MICROLAN_UNEXPECTED_DEVICES;

        // Store number of unexpected devices detected
        data_exchange.rsp.err.v.devices_number =
        		ds2480_state.branch_summary.total_devices - MICROLAN_NETWORK_INSTALLED_COUPLERS;

        data_exchange.rsp_bytes_in_buffer  = 2;
        return false;
    }



    for (coupler = 0; coupler < MICROLAN_NETWORK_INSTALLED_COUPLERS; coupler++)
    {

        // Set coupler control outputs inactive (high)
        if ( oneWireMasterSelectDeviceInMicroLAN(&ds2480_state.branch_ids[coupler]) != ONEWIRE_MASTER_COMMAND_OK )
        {
            return false;
        }

        ds2409CouplerSequence(DS2409_CTRL_DISABLE_OUTPUTS);

        if (term.db_lvl >= 2)
        {
            fprintf(TX_TERM, "Scan DS2409: %u - P1:0x%02X\n\r", coupler, (p1 & 0x07));
        }

        ii = port1_value_to_coupler_index[p1 & 0x07];

        if (ii == 3)
        {
            data_exchange.rsp.err.code = ERR_MICROLAN_COUPLERS_READBACK;
            data_exchange.rsp.err.v.state = p1 & 0x07;
            data_exchange.rsp_bytes_in_buffer = 2;
            return false;
        }


        // if something new
        if (memcmp(&ds2480_state.branch_ids[coupler], &one_wire_net.couplers_ids[ii], MICROLAN_UNIQUE_SERIAL_CODE_LEN) != 0)
        {
            one_wire_net.couplers_ids[ii] = ds2480_state.branch_ids[coupler];
            data_exchange.flash_needs_refresh  = true;
        }


        if (term.db_lvl != 0)
        {
            fprintf(TX_TERM, "Scan DS2409: %c: %s %c\n\r",
                    DS2409_NAME[ii],
                    utilitiesDallasIdToString(&ds2480_state.branch_ids[coupler]),
                    (data_exchange.flash_needs_refresh ? '*' : '-')
                   );
        }


        // Set coupler control outputs active (low)
        if ( oneWireMasterSelectDeviceInMicroLAN(&ds2480_state.branch_ids[coupler]) != ONEWIRE_MASTER_COMMAND_OK )
        {
            return false;
        }

        ds2409CouplerSequence(DS2409_CTRL_ENABLE_OUTPUTS);

    }

    data_exchange.rsp_bytes_in_buffer  = 1;

    return true;
}



bool ds2409FindIDsPre(void)
{

	if (!ds2409CheckIdentifyCouplers())
	{
		return false;
	}

	if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_HARD_RESET) != ONEWIRE_MASTER_COMMAND_OK )
	{
		return false;
	}

	return true;

}



bool ds2409FindIDs(const uint8_t logical_path, uint16_t* logical_element)
{

	if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_SOFT_RESET) != ONEWIRE_MASTER_COMMAND_OK )
	{
		return false;
	}

	ds2480SendByte(ONEWIRE_MASTER_SKIP_ROM, 1, DS2480_RESPONSE_TIMEOUT_MS);

	ds2480SendByte(DS2409_CTRL_FUNC_CMD_ALL_LINES_OFF, 1, DS2480_RESPONSE_TIMEOUT_MS);

	if (!ds2409SelectBranch(logical_path, !DS2409_SEND_ERROR_CODE))
	{
		return false;
	}

	if (!ds2480FindMicroLANDevicesCommon(logical_path, DS2409_CHECK_COUPLERS_PRESENCE))
	{
		return false;
	}

	ds2480BuildDevicesSummary(logical_path, logical_element);

	return true;
}



bool ds2409ReadTempsPre(void)
{

	if (term.db_lvl >= 2)
	{
		fprintf(TX_TERM, "ds2409ReadTemps... \n");
	}


	if (!ds2480IsFlashWithData())
	{
		return false;
	}

	return true;
}



bool ds2409ConvertTemp(const uint8_t logical_path)
{
    if (term.db_lvl >= 2)
    {
        fprintf(TX_TERM, "ConvertTempCouplers(%u)\n\r", logical_path);
    }

    if ( oneWireMasterReset(ONEWIRE_MASTER_INIT_SOFT_RESET) != ONEWIRE_MASTER_COMMAND_OK )
    {
        return false;
    }

    if (!ds2480SendByte(ONEWIRE_MASTER_SKIP_ROM, 1, DS2480_RESPONSE_TIMEOUT_MS))
    {
        return false;
    }


    if (!ds2480SendByte(DS2409_CTRL_FUNC_CMD_ALL_LINES_OFF, 1, DS2480_RESPONSE_TIMEOUT_MS))
    {
        return false;
    }

    ds2409SelectBranch(logical_path, DS2409_SEND_ERROR_CODE);

    return true;
}
