#define MAX4638_GLOBALS

// Includes

#include <max4638.h>

#include <ds2480.h>
#include <iodefines.h>
#include <main.h>
#include <onewire_master.h>
#include <term.h>

#include <string.h>


// Constants

/*!
 * Meaningful name for functions of port 1 for this HW platform.
 * Bits 0-2 of port #1 are used as output signals to select the MAX4638 branch.
 */
#define SELECT_BRANCH_C62_PORT p1


/*!
 * Indicates that the couplers (DS2409) prensence must be checked when the
 * function ds2480FindMicroLANDevicesCommon is used.
 */
static const bool MAX4638_CHECK_COUPLERS_PRESENCE = true;


// Internal structures, unions and enumerations
// Internal function declarations


/*!
 * Selects the physical branch of the MAX4638 (1-W channel) from a logical path.
 * @param A logical path that needs to be translated into a physical branch.
 */
static void max4638SelectBranch(const uint8_t logical_path);


// Internal variable definitions
// Internal function definitions

static void max4638SelectBranch(const uint8_t logical_path)
{

	uint8_t physical_branch;
	static uint8_t logical_to_physical[MICROLAN_NETWORK_LOGICAL_PATHS] = {2, 3, 1, 0, 7, 6};

	physical_branch = logical_to_physical[logical_path];

	SELECT_BRANCH_C62_PORT = (SELECT_BRANCH_C62_PORT & 0xF8) | (physical_branch & 0x07);

	if ( term.db_lvl >= 2 )
	{
		fprintf(TX_TERM, "%s %u => %s \n", "Selected branch", physical_branch, branch_labels[logical_path]);
	}

}



// External function definitions

bool max4638FindIDs(const uint8_t logical_path, uint16_t* logical_element)
{

	max4638SelectBranch(logical_path);

	oneWireMasterReset(ONEWIRE_MASTER_INIT_HARD_RESET);

	oneWireMasterReset(ONEWIRE_MASTER_INIT_SOFT_RESET);

	ds2480FindMicroLANDevicesCommon(logical_path, !MAX4638_CHECK_COUPLERS_PRESENCE);

	ds2480BuildDevicesSummary(logical_path, logical_element);


	return true;
}



bool max4638ConvertTemp(const uint8_t logical_path)
{
	if (term.db_lvl >= 2)
	{
		fprintf(TX_TERM, "ConvertTempMAX4638(%u)\n\r", logical_path);
	}

	max4638SelectBranch(logical_path);

	return true;
}



bool max4638ReadTempsPre(void)
{

	if (term.db_lvl >= 2)
	{
		fprintf(TX_TERM, "max4638ReadTempsPre... \n");
	}

	// Start with the multiplexer pointing to a branch with Dallas ID devices.
	// The local branch will always have.
	max4638SelectBranch(ID_LOGICAL_PATH_FOR_LOCAL);

	return true;
}
