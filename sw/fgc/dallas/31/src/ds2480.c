
#define DS2480_GLOBALS

#include <ds2480.h>

#include <db.h>
#include <iodefines.h>
#include <main.h>
#include <mcu_dependant.h>
#include <onewire_master.h>
#include <term.h>

#include <stdbool.h>
#include <stdio.h>
#include <string.h>


// Constants

/*!
 * 16 bytes are received when the dallas search is performed.
 * The Dallas ID is extracted from these pattern.
 */
#define DS2480_SEARCH_ACCELERATOR_PATTERN_LEN 16



// Internal structures, unions and enumerations

/*!
 * Working modes of the DS2480
 */
static enum ds2480_modes
{
	DS2480_COMMAND_MODE = 0xE3,	  //<! DS2480, force transition from data mode to check mode, eventually command mode
	DS2480_DATA_MODE    = 0xE1    //<! DS2480, force transition from command mode to data mode
};


static enum ds2480_times
{
	DS2480_WAIT_MODE_CHANGE_US 	= 2000, 		//!< 2ms, Note: last correction when FGC PS Main
	DS2480_WAIT_BEFORE_TX_US    = 200
};


/*!
 * DS2480B main commands
 */
static enum ds2480_commands
{
	DS2480_SEARCH_ACCELERATOR_ON   = 0xB1,    //!< Accelerator ON + regular speed
	DS2480_SEARCH_ACCELERATOR_OFF  = 0xA1,    //!< Accelerator OFF + regular speed
	DS2480_RESET_AND_FLEX_SPEED    = 0xC5,    //!< RESET + Flex speed
	DS2480_RESET_AND_REGULAR_SPEED = 0xC1,     //!< RESET + regular speed
	DS2408_CONFIRM_SCRATCHPAD_READ     	    = 0xFF,	   //!< Confirm reception of scratchpad byte when reading temperature
	DS2480_ARM_PULSE_5V_STRONG_PULLUP 		= 0xEF,    //!< PULSE 5V with strong pull-up; arm SPU for data bytes
	DS2480_DISARM_PULSE_5V_STRONG_PULLUP  	= 0xED,    //!< PULSE 5V with strong pull-up; disarm SPU for data bytes
	DS2480_TERMINATE_PULSE_PREMATURELY 		= 0xF1 	   //!< Terminate a strong pull-up or programming pulse prematurely
};


/*!
 * DS2480B configurations
 */
static enum ds2480_config
{
	DS2480_CONFIGURE_PDSCR_1_37Vus    		= 0x17,  //!< PDSCR    1.37 V/us
	DS2480_CONFIGURE_W1LT_11us        		= 0x47,  //!< W1LT     11 us
	DS2480_CONFIGURE_SAMPLING_10us    		= 0x5F,  //!< DSO/W0RT 10 us
	DS2480_CONFIGURE_STRONG_PULLUP_1048ms   = 0x3B   //!< SPUD     1048 ms
};


// Internal function declarations

/*!
 * Evolves in the DS2480 state machine
 * The expected values are 0xE3 to force transition from data mode to check mode, later command mode
 * and 0xE1 to force transition from command mode to data mode
 */
static void ds2480SetMode(const uint8_t mode);


/*!ds2480IdRecoverDevice takes the 128 bits of data received from a scan (in search_var_ptr) and extracts the ID code for
 * the current device.It checks the CRC and if bad, repeats the previous scan. If the device ID has repeated,
 * it repeats the previous-but-one scan. If the ID is good, it calculates the new scan pattern directly in search_var_ptr,
 * ready for transmission on the next iteration.
 * The function returns zero when all possible IDs have been found and 1 if there are more to look for.
 * Some reception errors will result in the search terminating early.
 */
static bool ds2480IdRecoverDevice(int8_t * device_index, uint8_t * search_var_ptr);


/*!
 * Sets the UART0 for TX / RX for the FGC2 platform
 */
static void ds2480SetUart0(void);


/*!
 * Sets the UART2 for TX / RX for the FGC3 platform
 */
static void ds2480SetUart2(void);


/*!
 * Reports to the M16C62 that the ID scan was unreliable (maximum attempts reached).
 */
static void ds2480UnreliableScan(const uint8_t logical_path);



// Internal variable definitions

/*!
 * Sets UART2 (FGC3) or UART0 (FGC2) for communication with the M16C62
 */
static utilities_fgc_platform_handler ds2480_set_uart;



// Internal function definitions

static void ds2480SetMode(const uint8_t mode)
{
    if (term.db_lvl >= 3)
    {
        fprintf(TX_TERM, "DS2480mode from 0x%02X to 0x%02X\n\r", ds2480_state.last_mode_set, mode);
    }

    if (mode != ds2480_state.last_mode_set)
    {
        USLEEP(DS2480_WAIT_MODE_CHANGE_US);

        // Send mode command to Dallas DS2480 line driver handling the MicroLan network
        *(hardware.id_bus_tx) = mode;
        ds2480_state.last_mode_set = mode;
    }
}



static bool ds2480IdRecoverDevice(int8_t * device_index, uint8_t * search_var_ptr)
{
    uint8_t   ii;
    uint8_t   crc;
    uint8_t   id_mask;
    uint8_t   dis_mask;
    uint8_t   type;
    uint8_t   pattern_index;
    uint8_t   search_var_index;
    uint16_t* search_var_as_word;
    uint16_t  id_byte = 0;

    static uint8_t pattern[DS2480_SEARCH_ACCELERATOR_PATTERN_LEN]; 				// keeps track of which discrepancy bits have been flipped
    static uint8_t previous_pattern[DS2480_SEARCH_ACCELERATOR_PATTERN_LEN]; 	// pattern from last transmission
    static uint8_t previous_search_var[DS2480_SEARCH_ACCELERATOR_PATTERN_LEN];
    static uint8_t prev_previous_search_var[DS2480_SEARCH_ACCELERATOR_PATTERN_LEN];

    if (term.db_lvl >= 2)
    {
        fprintf(TX_TERM, "ds2480IdRecoverDevice(%d)\n\r", *device_index);
    }

    if (*device_index < 0)                               // If this is the first call for a scan
    {
        *device_index = 0;
        memset(pattern, 0x00, DS2480_SEARCH_ACCELERATOR_PATTERN_LEN);             // Clear pattern table
        memset(search_var_ptr, 0x00, DS2480_SEARCH_ACCELERATOR_PATTERN_LEN);      // Clear id search_var_ptr table
        memset(previous_search_var, 0x00, DS2480_SEARCH_ACCELERATOR_PATTERN_LEN); // Clear old id data table
        memset(ds2480_state.search_accelerator_retries, 0x00,
               MICROLAN_NETWORK_ALL_ELEMENTS_MAX);  // Clear number of reads per device table
        return true; // there are more to look
    }

    search_var_as_word = (uint16_t *)search_var_ptr;                               // Initialise word pointer

    // MicroLAN (1-Wire bus) elements have an unique 64 bits serial code (identification)
    // the number includes
    // 8-bit family code
    // unique 48-bit serial number
    // 8-bit CRC (X^8 + X^5 + X^4 + 1)

    // De-interleave the received search_var_ptr into the new device ID and calculate checksum
    for (crc = ii = 0; ii < MICROLAN_UNIQUE_SERIAL_CODE_LEN; ii++, search_var_as_word++)
    {
        id_byte = (*search_var_as_word & 0x0002) >> 1 |
                  (*search_var_as_word & 0x0008) >> 2 |
                  (*search_var_as_word & 0x0020) >> 3 |
                  (*search_var_as_word & 0x0080) >> 4 |
                  (*search_var_as_word & 0x0200) >> 5 |
                  (*search_var_as_word & 0x0800) >> 6 |
                  (*search_var_as_word & 0x2000) >> 7 |
                  (*search_var_as_word & 0x8000) >> 8;

        if (ii < MICROLAN_UNIQUE_SERIAL_CODE_LEN - 1)                      // If not the last byte
        {
        	crc = utilitiesCalculateCRC8(crc, id_byte);
        }

        ds2480_state.branch_ids[*device_index].b[ii] = id_byte;
    }

    // If checksum not valid or device_index type is zero
    if ((crc != id_byte) || (ds2480_state.branch_ids[*device_index].p.family == 0))
    {
        // go one scan back
        memcpy(search_var_ptr, previous_search_var, DS2480_SEARCH_ACCELERATOR_PATTERN_LEN); // Repeat previous scan
        return true; // there are more to look
    }

    // if device ID has repeated (identical to previous found)
    if ((*device_index != 0)
        && (memcmp(&ds2480_state.branch_ids[*device_index], &ds2480_state.branch_ids[(*device_index) - 1],
                   MICROLAN_UNIQUE_SERIAL_CODE_LEN) == 0)
       )
    {
        // go two scans back
        (*device_index)--;
        memcpy(previous_search_var, prev_previous_search_var, DS2480_SEARCH_ACCELERATOR_PATTERN_LEN);
        memcpy(search_var_ptr,      previous_search_var,      DS2480_SEARCH_ACCELERATOR_PATTERN_LEN);

        memcpy(pattern,  previous_pattern, DS2480_SEARCH_ACCELERATOR_PATTERN_LEN);

        return true; // there are more to look
    }

    type = ds2480_state.branch_ids[(*device_index)].p.family;
    (*device_index)++;

    switch (type)
    {
        case MICROLAN_DEVICE_DS2409_COUPLER:
            ds2480_state.branch_summary.ds2409s++;
            break;

        case MICROLAN_DEVICE_DS2401_ONLY_ID:
            ds2480_state.branch_summary.ds2401s++;
            break;

        case MICROLAN_DEVICE_DS18B20_THERMOMETER:
            ds2480_state.branch_summary.ds18b20s++;
            break;

        default:
            ds2480_state.branch_summary.unknow_devices++;
            break;
    }

    // Prepare to scan received data and build for transmission the new search_var_ptr
    pattern_index    = DS2480_SEARCH_ACCELERATOR_PATTERN_LEN;
    search_var_index = DS2480_SEARCH_ACCELERATOR_PATTERN_LEN;

    // Save last pattern
    memcpy(previous_pattern, pattern, DS2480_SEARCH_ACCELERATOR_PATTERN_LEN);

    // Process received data to build next scan pattern
    for (ii = 0; ii < DS2480_SEARCH_ACCELERATOR_PATTERN_LEN; ii++)
    {
        pattern_index--;
        search_var_index--;

        for (id_mask = 0x80, dis_mask = 0x40; id_mask; id_mask >>= 2, dis_mask >>= 2)
        {
            if ((search_var_ptr[search_var_index] & dis_mask) != 0)
            {
                if ((pattern[pattern_index] & id_mask) != 0)
                {
                    pattern[pattern_index] &= ~id_mask;
                }
                else
                {
                    pattern[pattern_index] |= id_mask;
                    search_var_ptr[search_var_index] ^= id_mask;

                    // Save for the last two scans
                    memcpy(prev_previous_search_var, previous_search_var, DS2480_SEARCH_ACCELERATOR_PATTERN_LEN);
                    memcpy(previous_search_var,      search_var_ptr,      DS2480_SEARCH_ACCELERATOR_PATTERN_LEN);
                    return true;
                }
            }

            search_var_ptr[search_var_index] &= ~id_mask;
        }
    }

    return false;                         // Scan completed - no more devices to find
}


static void ds2480SetUart0(void)
{
	// Enable even parity (8 bits, 1 stop bit, even parity)
	// Enable Rx and Tx, interrupt on u4tb register empty
	// Write zero with even parity to trigger BREAK response in DS2480
	u0mr = 0x65;
	u0c1 = 0x05;
	*(hardware.id_bus_tx) = 0x00;

	// Wait 4ms for DS2480 to complete reset
	USLEEP( DS2480_WAIT_RESET_US );

	// Disable parity bit (8 bits, 1 stop bit, no parity)
	u0mr = 0x05;
}



static void ds2480SetUart2(void)
{
	// Enable even parity (8 bits, 1 stop bit, even parity)
	// Enable Rx and Tx, interrupt on u4tb register empty
	// Write zero with even parity to trigger BREAK response in DS2480

	u2mr = 0x65;
	u2c1 = 0x05;
	*(hardware.id_bus_tx) = 0x00;

	// Wait 4ms for DS2480 to complete reset
	USLEEP( DS2480_WAIT_RESET_US );

	// Disable parity bit (8 bits, 1 stop bit, no parity)
	u2mr = 0x05;
}



static void ds2480UnreliableScan(const uint8_t logical_path)
{
	if (term.db_lvl != 0)
	{
		fprintf(TX_TERM, "Find devices (%s): Unreliable scan: max_reads:%u/%u  couplers:%u\n",
				branch_labels[logical_path],
				ds2480_state.branch_summary.max_reads,
				ONE_WIRE_MASTER_MAX_RETRIES,
				ds2480_state.branch_summary.ds2409s);
	}

	data_exchange.rsp.err.code  = ERR_MICROLAN_UNRELIABLE_FIND;
	data_exchange.rsp.err.v.logical_path = logical_path;
	data_exchange.rsp_bytes_in_buffer = 2;
}



// External function definitions

void ds2480SetUartFGC2(void)
{
	ds2480_set_uart = ds2480SetUart0;
}



void ds2480SetUartFGC3(void)
{
	ds2480_set_uart = ds2480SetUart2;
}



bool ds2480SendByte(const uint8_t tx_data, const uint8_t expected_bytes, const uint16_t timeout_ms)
{
    uint8_t  ii;
    uint16_t waste_time_count;

    if (!ds2480_state.link_ok)
    {
        return false;
    }

    waste_time_count = 0;

    ds2480_state.cmd.expected_bytes = expected_bytes;
    ds2480_state.cmd.received_bytes = 0;

    if (term.db_lvl >= 3)
    {
        fprintf(TX_TERM, "Tx:0x%02X ", tx_data);
    }

    // Wait for UART tx buffer to be empty (linked to the DS2480)
    while (hardware.id_bus_tx_ctrl->bit.txept == 0)
    {
    };

    USLEEP(DS2480_WAIT_BEFORE_TX_US);               // wait 200us

    *(hardware.id_bus_tx) = tx_data;            // Write data to UART

    // changing from DATA to CMD mode ?
    if ((ds2480_state.last_mode_set == DS2480_DATA_MODE)
        && (tx_data == DS2480_COMMAND_MODE)
       )
    {
        // Repeat transmission to prevent driver returning
        while (hardware.id_bus_tx_ctrl->bit.txept == 0)
        {
        };

        USLEEP(DS2480_WAIT_BEFORE_TX_US);           // wait 200us

        *(hardware.id_bus_tx) = tx_data;
    }

    if (expected_bytes == 0)
    {
        if (term.db_lvl >= 3)
        {
            fputs("\n\r", TX_TERM);
        }
    }
    else
    {
        // wait for the response or timeout

        while ((ds2480_state.cmd.received_bytes < expected_bytes)
               && (waste_time_count < timeout_ms)
              )
        {
            USLEEP(1000);
            waste_time_count++;
        }

        // If response was not complete
        if (ds2480_state.cmd.received_bytes < expected_bytes)
        {
            ds2480_state.link_ok 				= false;
            ds2480_state.cmd.expected_bytes 	= 0;

            data_exchange.rsp.err.code 			= ERR_DS2480_LINK_NOT_OK;
            data_exchange.rsp.err.v.state 		= tx_data;
            data_exchange.rsp_bytes_in_buffer 	= 2;

            if (term.db_lvl != 0)
            {
                fprintf(TX_TERM, "Tx: Timeout sending 0x%02X after %u ms: ", tx_data, timeout_ms);

                for (ii = 0; ii < ds2480_state.cmd.received_bytes; ii++)
                {
                    fprintf(TX_TERM, "0x%02X ", ds2480_state.data_buffer[ii]);
                }

                fputs("\n\r", TX_TERM);
            }

            return false;
        }
        else
        {
            if (term.db_lvl >= 3)
            {
                fprintf(TX_TERM, "Rx after %u/%u ms: ", waste_time_count, timeout_ms);

                for (ii = 0; ii < ds2480_state.cmd.received_bytes; ii++)
                {
                    fprintf(TX_TERM, "0x%02X ", ds2480_state.data_buffer[ii]);
                }

                fputs("\n\r", TX_TERM);
            }
        }
    }

    return true;
}



bool ds2480Detect(const bool want_master_reset)
{
    if (!want_master_reset)
    {
        if (term.db_lvl >= 2)
        {
            fputs("MicroLAN soft reset\n\r", TX_TERM);
        }

        ds2480SetMode(DS2480_COMMAND_MODE);

        ds2480SendByte(DS2480_RESET_AND_FLEX_SPEED, 1, DS2480_RESPONSE_TIMEOUT_MS);
    }
    else
    {

        if (term.db_lvl >= 2)
        {
            fputs("MicroLAN master reset\n\r", TX_TERM);
        }

        ds2480_state.link_ok = TRUE;
        ds2480_state.last_mode_set = DS2480_COMMAND_MODE;  // after RESET we will end in COMMAND mode


        // Wait for any active transmission to complete
        while (hardware.id_bus_tx_ctrl->bit.txept == 0)
        {
        };

        ds2480_set_uart();

        // Send reset pulse for timing calibration
        *(hardware.id_bus_tx) = DS2480_RESET_AND_REGULAR_SPEED;

        // Wait 4ms for DS2480 to complete reset
        USLEEP(DS2480_WAIT_RESET_US);

        ds2480SendByte(DS2480_RESET_AND_FLEX_SPEED, 1, DS2480_RESPONSE_TIMEOUT_MS);

    }


    if (!ds2480_state.link_ok)
    {
    	// Report bus driver failed to respond
        data_exchange.rsp.err.code 			= ERR_MICROLAN_DS2480_TIMEOUT;
        data_exchange.rsp_bytes_in_buffer 	= 1;
        return false;
    }

    // RESET response - 11 p vvv ee
    // b7 1
    // b6 1
    // b5 0: prg ok, 1 : a programming voltage is present on the VPP pin
    // b4 b3 b2
    //  v  v  v   chip revision
    // b1 b0
    //  00  1-Wire shorted
    //  01  presence pulse
    //  10  alarming presence pulse
    //  11  no presence pulse

    // 0xCC - 11 0 011 00  prg ok, v3, 1-Wire shorted
    // 0xCD - 11 0 011 01  prg ok, v3, presence pulse
    // 0xCF - 11 0 011 11  prg ok, v3, no presence pulse

    switch (ds2480_state.data_buffer[0])                        // Interpret returned data
    {
        case 0xCD:
            if (want_master_reset)
            {
                // configure and go for DATA mode
                ds2480SendByte(DS2480_CONFIGURE_PDSCR_1_37Vus, 1, DS2480_RESPONSE_TIMEOUT_MS);
                ds2480SendByte(DS2480_CONFIGURE_W1LT_11us, 1, DS2480_RESPONSE_TIMEOUT_MS);
                ds2480SendByte(DS2480_CONFIGURE_SAMPLING_10us, 1, DS2480_RESPONSE_TIMEOUT_MS);
            }

            ds2480SetMode(DS2480_DATA_MODE);
            return true;

        case 0xCC:
            if (term.db_lvl != 0)
            {
                fputs("MicroLAN shorted\n\r", TX_TERM);     // Bus shorted
            }

            data_exchange.rsp.err.code = ERR_MICROLAN_TOP_LINK_SHORTED;
            data_exchange.rsp_bytes_in_buffer = 1;
            return false;

        case 0xCF:
            if (term.db_lvl != 0)
            {
                fputs("MicroLAN No devices\n\r", TX_TERM);  // No devices visible
            }

            data_exchange.rsp.err.code = ERR_MICROLAN_LOGICAL_PATH_NO_DEVICES;
            data_exchange.rsp_bytes_in_buffer = 1;
            return false;
    }

    if (term.db_lvl != 0)
    {
        fprintf(TX_TERM, "MicroLAN unknown status:0x%02X\n\r", ds2480_state.data_buffer[0]);    // Unknown error
    }

    data_exchange.rsp.err.code = ERR_MICROLAN_COUPLER_UNKNOW_STATUS;
    data_exchange.rsp.err.v.state = ds2480_state.data_buffer[0]; // Put unknown error byte in the response buffer
    data_exchange.rsp_bytes_in_buffer = 2;

    return false;
}



bool ds2480FindMicroLANDevicesCommon(const uint8_t logical_path, const bool check_couplers_presence)

{
    uint8_t ii;
    int8_t  device_index;

    // Tx/Rx buffer for scanning 1wire bus
    uint8_t search_result[DS2480_SEARCH_ACCELERATOR_PATTERN_LEN];


    if (term.db_lvl >= 2)
    {
        fprintf(TX_TERM, "Find devices in path %s\n\r", branch_labels[logical_path]);
    }

    // Clear device counters for this branch
    memset(&ds2480_state.branch_summary, 0, sizeof(struct TBranchSummary));

    // Find devices using accelerated search pattern
    device_index = -1;

    // Loop while there are more devices to find
    while ((ds2480IdRecoverDevice(&device_index, search_result) != 0)
           && (ds2480_state.search_accelerator_retries[device_index] < ONE_WIRE_MASTER_MAX_RETRIES))
    {
        if ( !ds2480Detect(ONEWIRE_MASTER_INIT_SOFT_RESET) )
        {
            return false;
        }

        ds2480SendByte(ONEWIRE_MASTER_SEARCH_ROM, 1, DS2480_RESPONSE_TIMEOUT_MS);

        ds2480SetMode(DS2480_COMMAND_MODE);
        ds2480SendByte(DS2480_SEARCH_ACCELERATOR_ON, 0, 0);
        ds2480SetMode(DS2480_DATA_MODE);

        for (ii = 0; ii < DS2480_SEARCH_ACCELERATOR_PATTERN_LEN; ii++)
        {
        	// Send tx data byte
            ds2480SendByte((search_result[ii] & 0xAA), 1, DS2480_RESPONSE_TIMEOUT_MS);

            // Save rx data byte
            search_result[ii] = ds2480_state.data_buffer[0];
        }

        ds2480SetMode(DS2480_COMMAND_MODE);
        ds2480SendByte(DS2480_SEARCH_ACCELERATOR_OFF, 0, 0);

        ds2480_state.search_accelerator_retries[device_index]++;

        // Maintain max reads counter for this scan
        if (ds2480_state.search_accelerator_retries[device_index] > ds2480_state.branch_summary.max_reads)
        {
            ds2480_state.branch_summary.max_reads = ds2480_state.search_accelerator_retries[device_index];
        }
    }


    if (ds2480_state.branch_summary.max_reads == ONE_WIRE_MASTER_MAX_RETRIES)
    {
    	ds2480UnreliableScan(logical_path);
    	return false;
    }

    if (check_couplers_presence && (ds2480_state.branch_summary.ds2409s != MICROLAN_NETWORK_INSTALLED_COUPLERS))
    {
    	ds2480UnreliableScan(logical_path);
    	return false;
    }


    if (term.db_lvl >= 2)
    {
        for (ii = 0; ii < device_index; ii++)
        {
            fprintf(TX_TERM, " %2u: %s [%2u]\n\r",
                    ii,
                    utilitiesDallasIdToString(&ds2480_state.branch_ids[ii]),
                    (uint16_t) ds2480_state.search_accelerator_retries[ii]);
        }
    }

    if (logical_path != hardware.n_branches)
    {
        if (term.db_lvl != 0)
        {
            fprintf(TX_TERM, "Find devices (%-6s) \n *COUPLERS (DS2409):%7u \n *REST:%20u \n *ID ONLY (DS2401):%8u \n *TEMP SENSORS (DS18B20):%2u \n *OTHER:%19u \n *READS (MAX = 5):%9u\n\r",
                    branch_labels[logical_path],
                    ds2480_state.branch_summary.ds2409s,
                    (device_index - ds2480_state.branch_summary.ds2409s),
                    ds2480_state.branch_summary.ds2401s,
                    ds2480_state.branch_summary.ds18b20s,
                    ds2480_state.branch_summary.unknow_devices,
                    ds2480_state.branch_summary.max_reads);
        }
    }

    ds2480_state.branch_summary.total_devices = device_index;

    return true;
}



bool ds2480EnablePullUp(void)
{

	ds2480SetMode(DS2480_COMMAND_MODE);

	if (!ds2480SendByte(DS2480_CONFIGURE_STRONG_PULLUP_1048ms, 1, DS2480_RESPONSE_TIMEOUT_MS))
	{
		return false;
	}

	if (!ds2480SendByte(DS2480_ARM_PULSE_5V_STRONG_PULLUP, 0, 0))
	{
		return false;
	}

	if (!ds2480SendByte(DS2480_TERMINATE_PULSE_PREMATURELY, 1, DS2480_RESPONSE_TIMEOUT_MS))
	{
		return false;
	}

	ds2480SetMode(DS2480_DATA_MODE);

	return true;
}



bool ds2480DisablePullUp(void)
{
	ds2480SetMode(DS2480_COMMAND_MODE);

	if (!ds2480SendByte(DS2480_DISARM_PULSE_5V_STRONG_PULLUP, 0, 0))
	{
		return false;
	}

	if (!ds2480SendByte(DS2480_TERMINATE_PULSE_PREMATURELY, 1, DS2480_RESPONSE_TIMEOUT_MS))
	{
		return false;
	}

	return true;

}



void ds2480BuildDevicesSummary(const uint8_t logical_path, uint16_t *const logical_element)
{
	uint8_t ii = 0;

	 // Take off couplers from number of devices found
	ds2480_state.branch_summary.total_devices -= ds2480_state.branch_summary.ds2409s;

	// Remember the number of devices found
	one_wire_net.branches_summary[logical_path] = ds2480_state.branch_summary;


	// Link branch to data device data records
	one_wire_net.branches_summary[logical_path].start_in_elements_summary = *logical_element;

	for (ii = 0; ii < ds2480_state.branch_summary.total_devices; ii++)
	{
		// If not a coupler, prepare ID data
		if (ds2480_state.branch_ids[ii].p.family != MICROLAN_DEVICE_DS2409_COUPLER)
		{
			one_wire_net.elements_summary[*logical_element].logical_path       = logical_path;
			one_wire_net.elements_summary[*logical_element].idx_in_branch      = *logical_element -
																	   one_wire_net.branches_summary[logical_path].start_in_elements_summary;
			one_wire_net.elements_summary[*logical_element].unique_serial_code = ds2480_state.branch_ids[ii];
			// clear other fields to start with proper values
			one_wire_net.elements_summary[*logical_element].temp_C = 0;       //by default 0=No dev or no data
			one_wire_net.elements_summary[*logical_element].temp_C_16 = 0;
			one_wire_net.elements_summary[*logical_element].barcode.c[0] = 0;

			DbLookup(&one_wire_net.elements_summary[*logical_element]);               // Look up device barcode
			(*logical_element)++;
		}
	}
}



bool ds2480IsFlashWithData(void)
{

    if (one_wire_net.couplers_ids[0].p.family != MICROLAN_DEVICE_DS2409_COUPLER ||
        one_wire_net.couplers_ids[1].p.family != MICROLAN_DEVICE_DS2409_COUPLER ||
        one_wire_net.couplers_ids[2].p.family != MICROLAN_DEVICE_DS2409_COUPLER)
    {
        if (term.db_lvl != 0)
        {
            fputs("CheckCouplers: Coupler IDs in flash are not valid\n\r", TX_TERM);
        }

        data_exchange.rsp.err.code = ERR_MICROLAN_COUPLERS_FOUND;       // Report error with number of known couplers
        data_exchange.rsp_bytes_in_buffer = 1;
        return false;
    }

    return true;
}

