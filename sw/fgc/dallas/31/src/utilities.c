#define MICROLAN_1WIRE_GLOBALS

#include <utilities.h>

#include <iodefines.h>
#include <term.h>

#include <ctype.h>
#include <stdio.h>


// Constants

// Internal structures, unions and enumerations

// Internal function declarations

/*!
 * Gets a character from the keyboard using the UART2
 */
static void utilitiesGetCharacterFGC2(void* ch);


/*!
 * Gets a character from the keyboard using the UART0
 */
static void utilitiesGetCharacterFGC3(void* ch);


// Internal variable definitions

static utilities_fgc_platform_handler_arg utilities_get_char;

// Internal function definitions


static void utilitiesGetCharacterFGC2(void* ch_arg)
{

	uint8_t* ch = (uint8_t*) ch_arg;

	// Wait for character to be received
	while ( (ri_u2c1 == 0) && (term.kbd_ch == 0) );

	// Read character if it is waiting in TERMINAL UART,
	// else read it from MCU terminal keyboard
	if ( ri_u2c1 != 0 )
	{
		*ch = toupper(u2rbl);
	}
	else
	{
		*ch = toupper(term.kbd_ch);
		term.kbd_ch = 0;
	}
}



static void utilitiesGetCharacterFGC3(void* ch_arg)
{
	uint8_t* ch = (uint8_t*) ch_arg;

	// Wait for character to be received
	while ( (ri_u0c1 == 0) && (term.kbd_ch == 0) );

	// Read character if it is waiting in TERMINAL UART,
	// else read it from MCU terminal keyboard
	if ( ri_u0c1 != 0 )
	{
		*ch = toupper(u0rbl);
	}
	else
	{
		*ch = toupper(term.kbd_ch);
		term.kbd_ch = 0;
	}
}

// External function definitions


void utilitiesSetGetCharacterFunctionFGC2(void)
{
	utilities_get_char = utilitiesGetCharacterFGC2;
}



void utilitiesSetGetCharacterFunctionFGC3(void)
{
	utilities_get_char = utilitiesGetCharacterFGC3;
}



uint8_t utilitiesGetChar(void)
{
    uint8_t       ch;
    utilities_get_char(&ch);

    fputc(ch, TX_TERM);

    fputc('\r',TX_TERM);

    return ch;
}



const char* utilitiesDallasIdToString(union TMicroLAN_unique_serial_code FAR * devid)
{
    static uint8_t id_string[ 2 * MICROLAN_UNIQUE_SERIAL_CODE_LEN + 1 ];

    sprintf((const char*) id_string, "%02X%02X%02X%02X%02X%02X%02X%02X",
            (uint8_t) devid->b[7], (uint8_t)devid->b[6], (uint8_t)devid->b[5], (uint8_t)devid->b[4],
            (uint8_t) devid->b[3], (uint8_t)devid->b[2], (uint8_t)devid->b[1], (uint8_t)devid->b[0]);

    return ((const char*) id_string);
}



uint8_t utilitiesCalculateCRC8(uint8_t crc, uint8_t dallas_byte)
{
	uint8_t position;

	position = crc ^ dallas_byte;
	return microlan_crc_table[position];
}

