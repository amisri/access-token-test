/*---------------------------------------------------------------------------------------------------------*\
  File:         sa.c

  Purpose:      M16C62 ID program
                Standalone operation functions

  Notes:        The ID program has two mode: STANDALONE and NORMAL.  In STANDALONE mode, the C62 is linked
                to the development front panel, and should be connected to a termial and a development PC.
                The program includes a simple monitor that displays a menu and lets the user choose between
                various options.  The monitor maintains a debug level variable, term.db_lvl, which controls
                the display of debug information on the terminal.

  History:

    20/07/04    qak     Created from M16C62 Boot main.c
    25/03/08    qak     SaSendCh() and SaGetCh() modified to allow remote terminal via MCU
    29/04/09    pfr     Modified to swap Uart0 & 2
\*---------------------------------------------------------------------------------------------------------*/

#include <sa.h>

#include <db.h>
#include <ds2409.h>
#include <ds2480.h>
#include <flash.h>
#include <iodefines.h>
#include <isr.h>
#include <m16c62_link.h>
#include <main.h>
#include <mcu_dependant.h>
#include <slow_cmd.h>
#include <term.h>
#include <onewire_master.h>


// Internal function declarations

/*!
 * Waits for the UART0 TX buffer is empty
 */
static void saSetWaitTxQueueFGC3(void);


/*!
 * Waits for the UART2 TX buffer is empty
 */
static void saSetWaitTxQueueFGC2(void);


// Internal variable definitions

static utilities_fgc_platform_handler sa_wait_tx_queue_empty;



// Internal function definitions

static void saSetWaitTxQueueFGC3(void)
{
	 while ( ti_u0c1 == 0 );
}


static void saSetWaitTxQueueFGC2(void)
{
	 while ( ti_u2c1 == 0 );
}


// External function definitions


void saSetWaitTxQueueFunctionFGC2(void)
{
	sa_wait_tx_queue_empty = saSetWaitTxQueueFGC2;
}


void saSetWaitTxQueueFunctionFGC3(void)
{
	sa_wait_tx_queue_empty = saSetWaitTxQueueFGC3;
}



void saDS2480TestSuite(void)
{
	uint8_t key;
	bool invalid_cmd;

	const char* ds2480Menu[15] = {
			"0-3) Debug level", "4) Couplers", "5) IDs",     "6) Temps",
			"F) Flash",         "P) PT DB",    "I) ID DB",
			"A) VS A",          "B) VS B",     "C) IMEAS A", "D) IMEAS B", "L) LOCAL", "R) CRATE",
			"T) Test",          "V) Version"};

	for (;;)
	{
		fprintf(TX_TERM, "%-18s%-13s%-12s%-12s\n", ds2480Menu[0], ds2480Menu[1], ds2480Menu[2], ds2480Menu[3]);
		fprintf(TX_TERM, "%-18s%-13s%-12s%\n", ds2480Menu[4], ds2480Menu[5], ds2480Menu[6]);
		fprintf(TX_TERM, "%-18s%-13s%-12s%-12s%-10s%-10s\n", ds2480Menu[7], ds2480Menu[8], ds2480Menu[9], ds2480Menu[10], ds2480Menu[11], ds2480Menu[12]);
		fprintf(TX_TERM, "%-18s%-13s", ds2480Menu[13], ds2480Menu[14]);
		fprintf(TX_TERM,  "\n\n\r");

		do
		{
			invalid_cmd = false;
			key = utilitiesGetChar();

			switch(key)
			{
			case '4':
				ds2409CheckIdentifyCouplers();
				break;

			case '5':
				slowCmdReadIds();
				break;

			case '6':
				oneWireMasterReadTemperatures( 0, hardware.n_branches - 1);
				break;

			case 'A':
				SaShowBranch(0);
				break;

			case 'B':
				SaShowBranch(2);
				break;

			case 'C':
				SaShowBranch(1);
				break;

			case 'D':
				SaShowBranch(3);
				break;

			case 'L':
				SaShowBranch(4);
				break;

			case 'R':
				SaShowBranch(5);
				break;

			case 'F':
				SaShowFlash();
				break;

			case 'P':
				SaShowPtDb();
				break;

			case 'I':
				SaShowIdDb();
				break;

			case 'T':
				SaTest();
				break;

			case 'V':
				{
					union TUnion32Bits converter;
					uint8_t c62_type = (flash.type == FLASH_M16C62_A) ? 'A' : 'P';

					converter.byte[0] = ((INT8U FAR *)IDPROG_VERSION_ADR)[3];
					converter.byte[1] = ((INT8U FAR *)IDPROG_VERSION_ADR)[2];
					converter.byte[2] = ((INT8U FAR *)IDPROG_VERSION_ADR)[1];
					converter.byte[3] = ((INT8U FAR *)IDPROG_VERSION_ADR)[0];

					fprintf(TX_TERM, "\nM16C62 - %c \n", c62_type);
					fprintf(TX_TERM, "\n\rId Prog Version %lu\n\r", converter.int32u);
				}
				break;

			default:

				if ( key >= '0' && key <= '3' )     // If debug level specified
				{
					term.db_lvl = (key - '0');      // Set debug level
				}
				else                                // else unknown key
				{
					invalid_cmd = TRUE;             // bad boy, please type something we know
					fputs("\a \r",TX_TERM); // Ring bell
				}
			  break;
			}
		}
		while (invalid_cmd == true);
	}
}




/*---------------------------------------------------------------------------------------------------------*/
void SaMonitor(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function provides a very simple monitor program for use in standalone mode from the terminal.  This
  can also be run from the MCU boot on request.

  The Dallas bus branches corresponds

  Menu                 FGC 3 back connector
  --------            ---------
  A Main ............ ID_VS_A  (voltage source = power converter)
  B Main ............ ID_VS_B  (voltage source = power converter)

  A Aux ............. ID_MEAS_A  ( measurement = DCCT )
  B Aux ............. ID_MEAS_B

  Local ............. ID_LOCAL
  Magnet ............ ID_RSV    (reserved)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       key;
    bool     invalid_cmd;

    term.db_lvl = 1;                            // Default debug level is 1

    // data_exchange.rsp.err.code = M16C62_NO_ERROR;       // Prepare default OK response in response buffer
    data_exchange.rsp_bytes_in_buffer = 1;      // Prepare to return M16C62_NO_ERROR in response data
    data_exchange.memory_ptr = data_exchange.rsp.bytes;  // Link response pointer to response buffer

    // do the falling edge to clear the last BUSY leave by the IdBoot, in case of C62_SWITCH_PROG
    // do the falling edge to clear the last BUSY leave by the IdProg, in case of C62_RUN_MONITOR

    PLD_DATA_READY_SIGNAL = 0;  // set p3.0 DATARDY to LOW
    PLD_CMD_BUSY_SIGNAL = 0;    // set p1.7 CMDBUSY to LOW

    // prepare the signal to 1 (so ready for the next falling edge)
    PLD_DATA_READY_SIGNAL = 1;  // set p3.0 DATARDY to HIGH
    PLD_CMD_BUSY_SIGNAL = 1;    // set p1.7 CMDBUSY to HIGH

    isr.cmd_in_progress = FALSE; // now we are ready to run

    USLEEP(WAIT_BEFORE_MONITOR);        // Wait 20ms for MCU to be ready to read terminal chars

    saDS2480TestSuite();

}
/*---------------------------------------------------------------------------------------------------------*/
void SaShowBranch(INT16U branch_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the scan results for the specified branch.  For each device it will display
  the ID, temp, barcode and type label.  If the device is not a temp sensor, then the temp value will be
  zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      ii;
    INT16U      dev_idx;

    if ( branch_idx > (hardware.n_branches - 1) )
    {
        return;                         // Return immediately
    }

    fprintf(TX_TERM,"Info branch %s \n *TOTAL IDs:%15u  \n *COUPLERS (DS2409):%7u \n *ID ONLY (DS2401):%8u  \n *TEMP SENSORS (DS18B20):%2u  \n *OTHER:%19u  \n *READS (MAX = 5):%9u\n\n\r",
                branch_labels[branch_idx],
                one_wire_net.branches_summary[branch_idx].total_devices,
                one_wire_net.branches_summary[branch_idx].ds2409s,
                one_wire_net.branches_summary[branch_idx].ds2401s,
                one_wire_net.branches_summary[branch_idx].ds18b20s,
                one_wire_net.branches_summary[branch_idx].unknow_devices,
                one_wire_net.branches_summary[branch_idx].max_reads);

    dev_idx = one_wire_net.branches_summary[branch_idx].start_in_elements_summary;

    for ( ii = 0; ii < one_wire_net.branches_summary[branch_idx].total_devices; ii++, dev_idx++ )
    {
        fprintf(TX_TERM,"%s %3u.%02u %-19s\n\r",
                utilitiesDallasIdToString(&one_wire_net.elements_summary[dev_idx].unique_serial_code),
                one_wire_net.elements_summary[dev_idx].temp_C,
                ID_TEMP_FRAC_C(one_wire_net.elements_summary[dev_idx].temp_C_16),
                (FAR char *)one_wire_net.elements_summary[dev_idx].barcode.c);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void SaShowFlash(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will show the first part of each flash block
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      ii;
    INT16U      block = flash.block_idx ? (FLASHC62A_BLOCKS_NUMBER - 1) : (FLASHC62P_BLOCKS_NUMBER - 1);

    do
    {
        fprintf(TX_TERM,"%u:0x%05lX: ", block, flash_block_info[flash.block_idx][block].start); // print start address

        // print the first 16 bytes
        for ( ii = 0; ii < 16; ii++ )
        {
            fprintf(TX_TERM,"%02X ",((INT8U FAR *)(flash_block_info[flash.block_idx][block].start))[ii]);
        }
        fprintf(TX_TERM,"\n\r");
    }
    while( block-- != 0 );
}
/*---------------------------------------------------------------------------------------------------------*/
void SaShowPtDb(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will show the Part Type database
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      idx;
    INT32U      serial_base;
    INT16U      sb_low, sb_high;

    if ( DB_PT[0].pt_base != 0 )                // If no database installed
    {
        fprintf(TX_TERM,"No Part Type database installed\n\n\r");
        return;
    }

    for ( idx = 0; DB_PT[idx].pt_base != 0xFFFF; idx++ )
    {
        serial_base = DB_PT[idx].serial_base;

        sb_low  = serial_base & 0xFFFF;
        sb_high = serial_base >> 16;

        fprintf(TX_TERM,"%3u:%05u:0x%04X%04X:%s\n\r",idx,
                DB_PT[idx].pt_base,
                sb_high,sb_low,
                DB_PT[idx].type_code);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void SaShowIdDb(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will show the ID database
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      idx[4];
    INT16U      first_idx[4];
    INT16U      last_idx[4];

    if ( DB_DIR[0].tbl_byte != 0 )                      // If no database installed
    {
        fprintf(TX_TERM,"No ID database installed\n\n\r");
        return;
    }

    SaShowIdDbRec(1, 0);                        // Show DB recursively
}
/*---------------------------------------------------------------------------------------------------------*/
void SaShowIdDbRec(INT16U level, INT16U idx)
/*---------------------------------------------------------------------------------------------------------*\
  This recursive function will be used to display the ID database
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      ii;
    INT16U      next_idx;
    INT16U      first_idx;
    INT16U      last_idx;
    INT16U      end1;
    INT16U      start2;


    for ( ii = 1; ii < level; ii++ )    // Indent according to level
    {
        fputc(' ',TX_TERM);
    }

    if ( level == 4 )           // If ID record
    {
        fprintf(TX_TERM, "ID:%04u:%05u:%02X %04X\n\r", idx, DB_ID[idx].part_idx, DB_ID[idx].id3, DB_ID[idx].id12);
        return;
    }

    first_idx = DB_DIR[idx].first_idx;
    last_idx  = DB_DIR[idx].last_idx;

    fprintf(TX_TERM, "DIR%u:%04u:%02X:%05u:%05u\n\r", level, idx, DB_DIR[idx].tbl_byte, first_idx, last_idx);

    if ( (last_idx - first_idx) < 4 )   // If up to 4 ID records in zone
    {
        for ( next_idx = first_idx; next_idx <= last_idx; next_idx++ )          // Display all records
        {
            SaShowIdDbRec(level+1, next_idx);
        }
    }
    else                                // else show only first and last pair of records
    {
        SaShowIdDbRec(level+1, first_idx);
        SaShowIdDbRec(level+1, first_idx+1);
        fputs("            ...\n\r",TX_TERM);
        SaShowIdDbRec(level+1, last_idx-1);
        SaShowIdDbRec(level+1, last_idx);
    }

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void SaTest(void)
/*---------------------------------------------------------------------------------------------------------*\
  This test function will do a test database look up for debugging purposes
\*---------------------------------------------------------------------------------------------------------*/
{
    struct TMicroLAN_element_summary    test_dev;

    test_dev.unique_serial_code.b[0] = 0x28;
    test_dev.unique_serial_code.b[1] = 0xD9;
    test_dev.unique_serial_code.b[2] = 0x11;
    test_dev.unique_serial_code.b[3] = 0x57;
    test_dev.unique_serial_code.b[4] = 0x01;

    DbLookup(&test_dev);                                // Look up test device barcode

    return;
}


/*---------------------------------------------------------------------------------------------------------*/
// if I use INT16S as 'signed short'
// I get an incompatible pointer warning (????)
// when assigning SaSendCh to the FILE structure    int (* _func_out)(int);

INT16S SaSendCh(INT16S ch)
/*---------------------------------------------------------------------------------------------------------*\
  it is used as the callback for the terminal stream [term.f._func_out = SaSendCh] in main.c

  This function is called to submit a character to the terminal transmission queue.  If the queue is full,
  the function will block by polling the number of characters in the queue, waiting for it to be emptied
  by IsrTermTx().
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                      // Allocate storage for CPU status register
    OS_CPU_SR   cpu_sr = 0;
#endif

    // slow down terminal output to let the remote terminal to poll without loosing chars
    if ( term.db_lvl > 1 )
    {
        USLEEP(1990);   // at higher debug levels we have a big load
    }
    else
    {
        USLEEP(400);
    }

    while ( term.n_ch == TERM_BUF_SIZE );       // Wait while the queue is full


    sa_wait_tx_queue_empty();


    OS_ENTER_CRITICAL();

    if ( term.n_ch == 0 )                       // If queue is empty
    {
        // the first byte is output here, the ones following (in the buffer) will be output by IsrTermTx()
        *(hardware.term_tx) = term.uart_copy = ch; // Tx char to TERMINAL
                                                // and make transmitted byte available to MCU
    }
    else
    {
        term.n_ch++;
        term.buf[term.in++] = ch;               // Insert character in the queue
    }

    OS_EXIT_CRITICAL();

    return(TRUE);
}
