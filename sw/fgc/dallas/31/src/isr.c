/*---------------------------------------------------------------------------------------------------------*\
  File:         isr.c

  Purpose:      M16C62 ID program
            interrupt function

  Notes:        IsrByteFromMcu() will be triggered when a byte is received from the MCU
                IsrDallasRx() will be triggered when a byte is received
                      from UART0 for FGC2
                      from UART2 for FGC3
                IsrTermTx() will be triggered when
                    the UART2 for FGC2
                    the UART0 for FGC3
                    transmit buffer empties

  History:

    15/01/04    af      Created
    25/04/04    qak     Adapted to use new command protocol
    24/04/08    qak     FastCmdSetKbdChar() and FastCmdGetTermChar() added to support terminal access from MCU
    29/04/09    pfr     Modified to swap Uart0 & 2
\*---------------------------------------------------------------------------------------------------------*/

#include <isr.h>

#include <ds2480.h>
#include <fast_cmd.h>
#include <iodefines.h>
#include <m16c62_link.h>
#include <main.h>
#include <mcu_dependant.h>
#include <term.h>

// Internal function declarations

/*!
 *
 */
static void isrGetDallasByteFGC3(void* arg);


/*!
 *
 */
static void isrGetDallasByteFGC2(void* arg);



// Internal variable definitions


static utilities_fgc_platform_handler_arg byte_from_dallas_func = 0;


// Internal function definitions


static void isrGetDallasByteFGC3(void* arg)
{
	INT8U* tmp = (INT8U*) arg;
	*tmp = u2rbl;
}



static void isrGetDallasByteFGC2(void* arg)
{
	INT8U* tmp = (INT8U*) arg;
	*tmp = u0rbl;
}

// External function definitions


void isrSetGetDallasByteFunctionFGC2(void)
{
	byte_from_dallas_func = isrGetDallasByteFGC2;
}


void isrSetGetDallasByteFunctionFGC3(void)
{
	byte_from_dallas_func = isrGetDallasByteFGC3;
}







INT8U UnknownCmd(INT8U arg)
{
    data_exchange.has_error = TRUE;
    return(M16C62_UNKNOWN_CMD);
}



INT8U SlowCmd(INT8U arg)
{
    cmd.slow_cmd_idx = cmd.fast_cmd_idx;
    isr.is_slow_command = TRUE;
    return(M16C62_NO_ERROR);
}



#pragma INTERRUPT IsrByteFromMcu        not used as the entry point is in start.a30 to allow push of all registers

void IsrByteFromMcu(void)
/*---------------------------------------------------------------------------------------------------------*\
  This interrupt function is linked to the INT3 interrupt request.

    INT3 associated with Port 1 bit 3
  and will be set low when the MCU writes to the M16C62 register.

    The byte may be either
      a command code or a command argument.

    The number of arguments required depends upon the command.

   - Commands that take more than a few microseconds to execute are run in the background and
     may not have arguments

   - Command that take less than a few microseconds to execute are run at interrupt level and
     may accept arguments
\*---------------------------------------------------------------------------------------------------------*/
{
    static const INT8U (* const Arr_Command_Functions[])(INT8U) =
    {
        FastCmdGetResponse,     //  0, C62_GET_RESPONSE
        FastCmdSetAddress,      //  1, C62_SET_ADDRESS
        FastCmdSetData,         //  2, C62_SET_DATA
        FastCmdSetBlockMask,    //  3, C62_SET_BLOCK_MASK
        FastCmdSetDebugLevel,   //  4, C62_SET_DB,              C62_SET_CRC_LEN
        FastCmdSetBranchIdx,    //  5, C62_SET_BRANCH
        FastCmdSetDevIdx,       //  6, C62_SET_DEV
        FastCmdSetKbdChar,      //  7, C62_SET_KBD_CHAR
        FastCmdGetTermChar,     //  8, C62_GET_TERM_CHAR
        UnknownCmd,             //  9,
        SlowCmd,                // 10, C62_SWITCH_PROG
        SlowCmd,                // 11, C62_ERASE_FLASH
        SlowCmd,                // 12, C62_PROG_FLASH
        SlowCmd,                // 13, C62_READ_IDS,            C62_CALC_CRC
        SlowCmd,                // 14, C62_READ_TEMPS
        SlowCmd,                // 15, C62_READ_TEMPS_BRANCH
        SlowCmd,                // 16, C62_RUN_MONITOR
        FastCmdSetBigEndian,    // 17, C62_SET_BIG_ENDIAN
        FastCmdSetLittleEndian, // 18, C62_SET_LITTLE_ENDIAN
        FastCmdSetExternalId,   // 19, C62_SET_EXTERNAL_ID
        SlowCmd,                // 20, C62_GET_BARCODE_FOR_EXT_ID
        FastCmdSetDebugLevel,   // 21, C62_SET_DEBUG_LEVEL
    };

    INT8U       rx_byte;
    INT8U       answer;


// this is done automatically when the ISR vector is activated
//    ir_int3ic  = 0;                   // Clear INT3 interrupt request

    // --- Acknowledge interrupt and prepare to handle byte from MCU ---


    data_exchange.has_error   = FALSE;
    isr.is_slow_command = FALSE;

    // When the MCU writes the PLD register
    // the PLD
    //   clear CMDERROR
    //   clear DATARDY
    //   set   CMDBUSY
    //   and fires this interrupt

    // the transition from HIGH to LOW of these signals is the information taken by the PLD to set it register bits
    // prepare the signals to HIGH (so ready for the falling edge)
    PLD_CMD_ERROR_SIGNAL  = 1;
    PLD_DATA_READY_SIGNAL = 1;
    PLD_CMD_BUSY_SIGNAL   = 1;

    // Rx - get data - it may be either a command code or an argument
    rx_byte = *(hardware.mcu_link_rx);

    // --- Analyse byte received from MCU ---

    answer = 0;
    if ( isr.cmd_in_progress == TRUE )
    {
        answer = M16C62_BUSY;                           // Report busy error
        data_exchange.has_error = TRUE;
    }
    else
    {
        // new command? or only more parameters?
        if ( cmd.n_args_expected != 0 )
        {
            // we are still processing the previous command
            answer = Arr_Command_Functions[cmd.fast_cmd_idx](rx_byte); // Call command handler
        }
        else    // here we pass when cmd.n_args_expected == 0, so new command
        {
            cmd.fast_cmd_idx = rx_byte; // can be a slow_cmd but will not affect
            isr.cmd_in_progress = TRUE;

            if ( cmd.fast_cmd_idx > C62_SET_DEBUG_LEVEL ) // check against last, to not go beyond limit
            {
                answer = UnknownCmd(cmd.fast_cmd_idx);
            }
            else
            {
                answer = Arr_Command_Functions[cmd.fast_cmd_idx](0); // Call command handler
            }
        }

        // for slow command the clear of CMDBUSY bit is done at BgTsk() when they end their processing
        if (isr.is_slow_command == FALSE)
        {
            isr.cmd_in_progress = FALSE;
            // this produce the falling edge of the signal, so the PLD will clear the CMDBUSY bit to 0 in it register
            PLD_CMD_BUSY_SIGNAL = 0;
        }
    }

    *(hardware.mcu_link_tx) = answer;

    if ( data_exchange.has_error == TRUE )
    {
        cmd.n_args_expected = 0;                        // Clear args expected
        // this produce the falling edge of the signal, so the PLD will set the CMDERR bit to 1 in it register
        PLD_CMD_ERROR_SIGNAL = 0;
    }
    else
    {
        // this produce the falling edge of the signal, so the PLD will set the DATARDY bit to 1 in it register
        PLD_DATA_READY_SIGNAL = 0;
    }
//  isr.mcu_isr++;
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma INTERRUPT IsrTermTx     not used as the entry point is in start.a30 to allow push of all registers

void IsrTermTx(void)
/*---------------------------------------------------------------------------------------------------------*\
  in the vector table (start.a30) both
  IRQ 15 UART2 Transmission Interrupt
  IRQ 17 UART0 Transmission Interrupt
  are directed here

  but for FGC2
  IRQ 15 UART2 Transmission Interrupt   generation is enabled - Terminal
  IRQ 17 UART0 Transmission Interrupt   generation is disabled

  and for FGC3
  IRQ 15 UART2 Transmission Interrupt   generation is disabled
  IRQ 17 UART0 Transmission Interrupt   generation is enabled - Terminal


  This interrupt function will be called when a transmission to the terminal completes.
  It will send the next character in the queue to the UART.
  The previously transmitted byte is put in the variable that will be used when the MCU ask via the
  FastCmdGetTermChar() command.

    FGC2
            UART2 handles the TERMINAL

    FGC3_DALLAS_DS2480
            UART0 handles the TERMINAL

\*---------------------------------------------------------------------------------------------------------*/
{
    term.term_ch = term.uart_copy;      // Pass last sent character to MCU

    if ( term.n_ch > 0 )                // If more characters are waiting in the queue
    {
        // this is u0tbl or u2tbl
        *(hardware.term_tx) = term.uart_copy = term.buf[term.out++];    // Tx next char and keep copy
        term.n_ch--;
        // term.buf[] is 256 (TERM_BUF_SIZE) and term.out is INT8U so can't go beyond the range
    }

    // if the queue is now empty
    if ( ( term.n_ch == 0 ) && (isr.busy_signaled_by_tx_queue == TRUE) )
    {
        isr.busy_signaled_by_tx_queue = FALSE;
        isr.cmd_in_progress = FALSE;
        // this produce the falling edge of the signal, so the PLD will clear the CMDBUSY bit to 0 in it register
        PLD_CMD_BUSY_SIGNAL = 0;
    }

//  isr.tx_isr++;
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma INTERRUPT IsrDallasRx   not used as the entry point is in start.a30 to allow push of all registers

void IsrDallasRx(void)
/*---------------------------------------------------------------------------------------------------------*\
  in the vector table (start.a30) both
  IRQ 16 UART2 receive Interrupt
  IRQ 18 UART0 receive Interrupt
  are directed here

  but for FGC2
  IRQ 16 UART2 receive Interrupt        generation is disabled but data polled at SaGetCh() - Terminal
  IRQ 18 UART0 receive Interrupt        generation is enabled - DallasRx

  and for FGC3
  IRQ 16 UART2 receive Interrupt        generation is enabled - DallasRx
  IRQ 18 UART0 receive Interrupt        generation is disabled but data polled at SaGetCh() - Terminal


  This interrupt function will receive the character pending on the UART
  linked to the Dallas DS2480 line driver, handling the Dallas ID bus

  FGC2,                 UART0
  FGC3_DALLAS_DS2480,   UART2

\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U       tmp;

    // read UART to clear interrupt request
    byte_from_dallas_func(&tmp);


    if ( ds2480_state.cmd.received_bytes < ds2480_state.cmd.expected_bytes )    // If more response characters are expected
    {
        ds2480_state.data_buffer[ds2480_state.cmd.received_bytes++] = tmp;              // Save received character in buffer
    }


}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.c
\*---------------------------------------------------------------------------------------------------------*/
