/*---------------------------------------------------------------------------------------------------------*\
  File:     slowCmd.c

  Purpose:  M16C62 ID program
                background task
        MCU slow response functions

  Notes:    These functions provide the slow (background) command processing.
                They are used for ID program commands that take too long to run at interrupt level.
        All slow (background) functions report their results via the response buffer,
        that the MCU can read out using the C62_GET_RESPONSE command.
        All responses start with an errno byte (constants are defined in c62_consts.h).
        Additional bytes may be included according to the value of the first byte.

  History:

    28/04/04    qak Created
    25/03/08    qak slowCmdRunMonitor() added to allow remote terminal from MCU
\*---------------------------------------------------------------------------------------------------------*/

#include <slow_cmd.h>

#include <db.h>
#include <flash.h>
#include <iodefines.h>
#include <isr.h>
#include <m16c62_link.h>
#include <main.h>
#include <mcu_dependant.h>
#include <onewire_master.h>
#include <sa.h>
#include <term.h>

// Constants


/*!
 * Flash status when erase or program was successful
 */
static const uint16_t C62_FLASHOK = 0x0380;


// Internal function declarations

/*!
 * Gets executed upon the reception of the M16C62A command C62_READ_TEMPS, 14
 * This function will read the temperature from all the temp sensors connected to the Dallas branches.
 */
static void slowCmdReadTemps(void);


/*!
 * Gets executed upon the reception of the M16C62A command C62_READ_TEMPS_BRANCH, 15
 * This function will read the temperature from all the temp sensors connected to the Dallas branch
 * previously specified by C62_SET_BRANCH
 */
static void slowCmdReadTempsBranch(void);


/*!
 * Gets executed upon the reception of the M16C62A command C62_READ_TEMPS_BRANCH, 15
 * This function will run the monitor so that remote debugging of the operation of the IdProg can be
 * performed via the MCU boot.
 */
static void slowCmdRunMonitor(void);


/*!
 * Handler to manage "unknown" slow commands
 */
static void slowCmdUnknownCmd(void);


/*!
 * Gets executed upon the reception of the M16C62A command C62_SWITCH_PROG, 10
 * Response : 3 bytes, [0] success [1] version number [2] version number
 * This function will switch back to the M16C62 IdBoot program.
 * Interrupts are disabled before starting the M16C62 IdBoot program, which is also responsible for clearing the BUSY signal.
 */
static void slowCmdSwitchProg(void);


/*
 * Gets executed upon the reception of the M16C62A command C62_ERASE_FLASH, 11
 * The command C62_SET_BLOCK_MASK is needed to fill flash.block_mask previously
 * Response : errno, flsh_stat h/l, [bad_val h/l, bad_addr h/m/l]
 * This function will erase the flash blocks specified via the C62_SET_BLOCK_MASK command.
 * Six of the seven flash blocks may be erased. The function will try up to twice to erase a block.  In case of two failures,
 * it will search for the word that could not be erased.
 * Each bit of the mask represents one flash block, b0 represents Block 0 ... b6 represents Block 6
 */
static void slowCmdEraseFlash(void);



/*!
 * @brief Queries the IDDB for barcode for the external ID and returns the result to the MCU
 *
 * This slow command should be called after fast command FastCmdSetExternalId was executed. It queries the IDDB
 * for barcode matching the external Dallas ID passed earlier to the m16c62 by the MCU. After that it prepares
 * the response buffer which can be read by the MCU. The response is 20 bytes long (length of the barcode)
 *
 * @see FastCmdSetExternalId(INT8U arg)
 */
static void slowCmdGetBarcodeForExtId(void);

// Internal variable definitions


// Internal function definitions

void slowCmdUnknownCmd(void)
{
    data_exchange.rsp.err.code = M16C62_UNKNOWN_CMD;
    data_exchange.rsp_bytes_in_buffer = 1;
}



void slowCmdEraseFlash(void)
{
    INT16U  ii;
    INT16U  stat;
    INT32U  addr;
    INT8U   mask;


    mask = flash.block_mask;

    if ( term.db_lvl > 1 )
    {
        fprintf(TX_TERM,"slowCmdEraseFlash: mask:0x%X\n\r", (INT16U) mask);
    }

    // --- Clear flash status report in response buffer ---

//  this is done by the caller
//  data_exchange.rsp.err.code = M16C62_NO_ERROR;
//  data_exchange.rsp_bytes_in_buffer = 1;

    data_exchange.rsp.err.v.erase.flash_stat.word = 0;

    if ( mask == 0 )                        // If nothing to erase
    {
        return;
    }


    // --- Transfer Flash function to RAM at 0x400 ---

    FlashTransfer();

    // --- Try to erase all blocks specified in flash.block_mask ---

    for ( ii = 0; mask; ii++, mask >>= 1 )
    {
        if ( (mask & 1) != 0 )                      // If block been selected for erasure
        {
            FlashCpuInit();             // set processor for programming
            stat = FlashErase(flash_block_info[flash.block_idx][ii].end);    // Erase block
            FlashCpuRestore();              // Restore processor modes

            if ( term.db_lvl > 0 )
            {
                fprintf(TX_TERM, "Erased blk:%p\n\r", flash_block_info[flash.block_idx][ii].end);
            }

            if ( stat != C62_FLASHOK )          // If erase failed
            {
                // Report bad flash status
                if ( hardware.mcu_transmit_in_big_endian == TRUE )
                {
                    data_exchange.rsp.err.v.erase.flash_stat.be.hi = (INT8U) (stat >> 8);
                    data_exchange.rsp.err.v.erase.flash_stat.be.lo = (INT8U) stat;
                }
                else
                {
                    data_exchange.rsp.err.v.erase.flash_stat.word = stat;
                }

                FlashCpuInit();                 // set processor for programming
                stat = FlashErase(flash_block_info[flash.block_idx][ii].end);    // Erase block
                FlashCpuRestore();              // Restore processor modes

                if ( stat != C62_FLASHOK )          // If erase failed twice
                {
                    // Report bad flash status
                    if ( hardware.mcu_transmit_in_big_endian == TRUE )
                    {
                        data_exchange.rsp.err.v.erase.flash_stat.be.hi = (INT8U) (stat >> 8);
                        data_exchange.rsp.err.v.erase.flash_stat.be.lo = (INT8U) stat;
                    }
                    else
                    {
                        data_exchange.rsp.err.v.erase.flash_stat.word = stat;
                    }

                    for(addr  = flash_block_info[flash.block_idx][ii].start; // Set index to start of block
                        addr <= flash_block_info[flash.block_idx][ii].end && // Scan block for non-erased locations
                        *((INT16U FAR *)addr) == 0xFFFF;
                        addr += 2);
                    {
                    }

                    if ( addr <= flash_block_info[flash.block_idx][ii].end ) // If erase failure detected
                    {
                        data_exchange.rsp.err.code = M16C62_ERASE_FAILED;       // Report erase failure

                        if ( hardware.mcu_transmit_in_big_endian == TRUE )
                        {
                            data_exchange.rsp.err.v.erase.bad_value.be.hi = ((INT8U FAR *)addr)[1]; //  Bad word value
                            data_exchange.rsp.err.v.erase.bad_value.be.lo = ((INT8U FAR *)addr)[0];

                            data_exchange.rsp.err.v.erase.address.be.hi_w_hi_b = addr >> 16;    //  Address high byte
                            data_exchange.rsp.err.v.erase.address.be.hi_w_lo_b = addr >> 8; //  Address mid byte
                            data_exchange.rsp.err.v.erase.address.be.lo_w_hi_b = addr;  //  Address low byte
                        }
                        else
                        {
                            data_exchange.rsp.err.v.erase.bad_value.word = *((INT16U FAR *)addr);   //  Bad word value
                            data_exchange.rsp.err.v.erase.address.int32u = addr;
                        }

                        if ( term.db_lvl > 0 )
                        {
                            fprintf(TX_TERM,"slowCmdEraseFlash: failed:0x%04X @ 0x%lX\n\r",
                                    *((INT16U FAR *)addr), addr);
                        }

                        data_exchange.rsp_bytes_in_buffer = 8; // the last byte off address is not sent

                        return;
                    }
                }
            }
        }
    }
    data_exchange.rsp_bytes_in_buffer = 3; // err.code = M16C62_NO_ERROR and flash_stat = 0
}



static void slowCmdReadTemps(void)
{
	oneWireMasterReadTemperatures(0, hardware.n_branches - 1);
}



static void slowCmdReadTempsBranch(void)
{
	oneWireMasterReadTemperatures(data_exchange.branch_idx, data_exchange.branch_idx);
}



static void slowCmdRunMonitor(void)
{
    cmd.slow_cmd_idx = 0;       // Clear command index
    term.term_ch   = 0;         // Clear term char buffer

    SaMonitor();            // Run Monitor (never returns)
}



static void slowCmdSwitchProg(void)
{
    asm("fclr i");              // for safety, disable interrupts

//  *(hardware.term_tx) = 'X';          // Tx char to TERMINAL

//  The cmd send by the MCU was C62_SWITCH_PROG so this remains
//  in *(hardware.mcu_link_rx) and will signal IdBoot to start BgTsk() (and not let go trough back to IdProg)

#pragma ASM

    ; jmpi.a 0ffffch is not allowed but jmpi.a 0ffffch[A0] YES????!!!
    ; so do it with A0=0

    ; jump to the reset vector

    xor.w A0, A0                                ; A0 = 0
    jmpi.a 0ffffch[A0]              ; Jump to run M16C62 boot code

#pragma ENDASM

}



static void slowCmdGetBarcodeForExtId(void)
{
    INT8U i = 0;

    // A dummy device structure. To get the barcode we need only the ID field to be filled.
    // The rest is not used and can be empty.
    struct TMicroLAN_element_summary dev = {0};

    // Copy the external Dallas ID to the dummy structure
    for (i = 0; i < 8; i++)
    {
        dev.unique_serial_code.b[i] = external_id[i];
    }

    // Get the barcode. It will be saved in dev.barcode.c array
    DbLookup(&dev);

    // Copy the barcode to the response buffer
    for (i = 0; i < 20; i++)
    {
        data_exchange.rsp.bytes[i] = dev.barcode.c[i];
    }

    // Prepare the structure to send the barcode back to the MCU

    data_exchange.has_error = FALSE;
    data_exchange.memory_ptr = data_exchange.rsp.bytes;
    data_exchange.rsp_bytes_in_buffer = 20;
}



// External functions definitions

void slowCmdBgTsk(void)

{
    static const void (* const Arr_Command_Functions[])(void) =
    {
        slowCmdUnknownCmd,             //  0, C62_GET_RESPONSE
        slowCmdUnknownCmd,             //  1, C62_SET_ADDRESS
        slowCmdUnknownCmd,             //  2, C62_SET_DATA
        slowCmdUnknownCmd,             //  3, C62_SET_BLOCK_MASK
        slowCmdUnknownCmd,             //  4, C62_SET_CRC_LEN,     C62_SET_DB
        slowCmdUnknownCmd,             //  5, C62_SET_BRANCH
        slowCmdUnknownCmd,             //  6, C62_SET_DEV
        slowCmdUnknownCmd,             //  7, C62_SET_KBD_CHAR
        slowCmdUnknownCmd,             //  8, C62_GET_TERM_CHAR
        slowCmdUnknownCmd,             //  9,
        slowCmdSwitchProg,             // 10, C62_SWITCH_PROG
        slowCmdEraseFlash,             // 11, C62_ERASE_FLASH
        slowCmdProgFlash,           // 12, C62_PROG_FLASH
        slowCmdReadIds,             // 13, C62_READ_IDS,        C62_CALC_CRC
        slowCmdReadTemps,           // 14, C62_READ_TEMPS
        slowCmdReadTempsBranch,     // 15, C62_READ_TEMPS_BRANCH
        slowCmdRunMonitor,          // 16, C62_RUN_MONITOR      never returns
        slowCmdUnknownCmd,             // 17, C62_SET_BIG_ENDIAN
        slowCmdUnknownCmd,             // 18, C62_SET_LITTLE_ENDIAN
        slowCmdUnknownCmd,             // 19, C62_SET_EXTERNAL_ID
        slowCmdGetBarcodeForExtId,  // 20, C62_GET_BARCODE_FOR_EXT_ID
        slowCmdUnknownCmd              // 21, C62_SET_DEBUG_LEVEL
    };

    // IdBoot has executed command C62_SWITCH_PROG leaving PLD_CMD_BUSY_SIGNAL active
    // the MCU is still waiting for the command to finish
    // we signal here that the command has finished
    // returning the version

//  data_exchange.rsp.err.code = M16C62_NO_ERROR;   // Prepare default OK response in response buffer
//  data_exchange.rsp.bytes[0]  = 0; the previous line is similar to this and as the RAM was zeroed, it is not needed

    // actually this is not read by the MCU as for status M16C62_NO_ERROR no extra byte is read
    data_exchange.rsp.bytes[1] = ((INT8U FAR *)IDPROG_VERSION_ADR)[0];  // Prepare program version word in response buffer
    data_exchange.rsp.bytes[2] = ((INT8U FAR *)IDPROG_VERSION_ADR)[1];  // Version word is big endian
    data_exchange.rsp_bytes_in_buffer = 3;
    data_exchange.memory_ptr = data_exchange.rsp.bytes;  // Link response pointer to response buffer

//  cmd.slow_cmd_idx = 0;       // to be sure to stop in the while() ahead while MCU is reading the version

    // do the falling edge to clear the last BUSY leave by the IdBoot

    PLD_DATA_READY_SIGNAL = 0;  // set p3.0 DATARDY to LOW
    PLD_CMD_BUSY_SIGNAL = 0;    // set p1.7 CMDBUSY to LOW

    // prepare the signal to 1 (so ready for the next falling edge)
    PLD_DATA_READY_SIGNAL = 1;  // set p3.0 DATARDY to HIGH
    PLD_CMD_BUSY_SIGNAL = 1;    // set p1.7 CMDBUSY to HIGH

    isr.cmd_in_progress = FALSE; // now we are ready to run

    // --- Loop forever to process background commands ---
    for(;;)
    {
        while( cmd.slow_cmd_idx == 0 );     // Wait for command index from IsrByteFromMcu()

        if ( term.db_lvl > 1 )
        {
            fprintf(TX_TERM,"CMD:%u\n\r", cmd.slow_cmd_idx); // fprintf() is not reentrant!!!
        }

//      can't overwrite the first use at startup needed for the version
//      data_exchange.rsp_bytes_in_buffer = 1;
//      these can be overwritten without any harm
        data_exchange.rsp.err.code = M16C62_NO_ERROR;       // Prepare default OK response in response buffer
        data_exchange.memory_ptr = data_exchange.rsp.bytes;  // Link response pointer to response buffer

        // Run command function
        Arr_Command_Functions[cmd.slow_cmd_idx](); // Call command handler

        if ( term.db_lvl > 0 )
        {
            fprintf(TX_TERM,"Response: bytes:%u  sts:%u\n\r", data_exchange.rsp_bytes_in_buffer, (INT16U) data_exchange.rsp.err.code);
        }

        cmd.slow_cmd_idx = 0;           // Clear command index

        if ( term.n_ch == 0 )           // if nothing in the transmission queue
        {
            isr.cmd_in_progress = FALSE;
            // this produce the falling edge of the signal, so the PLD will clear the CMDBUSY bit to 0 in its register
            PLD_CMD_BUSY_SIGNAL  = 0;
        }
        else
        {
            isr.busy_signaled_by_tx_queue = TRUE;
        }
    }
}



void slowCmdProgFlash(void)
{
    INT16U  ii;
    INT16U  stat;
    INT16U  idx;
    INT8U FAR * addr;
    BOOLEAN valid_addr;


    if ( term.db_lvl > 1 )
    {
        fprintf(TX_TERM,"SlowCmdProgFlash: Addr:0x%lX\n\r", flash.write_address);
    }

//  data_exchange.rsp.err.code = M16C62_NO_ERROR;  this is done at BgTsk()
//  data_exchange.rsp_bytes_in_buffer = 1;

    // --- Check sector address is valid ---

    valid_addr = FALSE;     // by default

    for ( ii = 0; ii < FLASHC62P_BLOCKS_NUMBER; ii++)
    {
        if ( flash_block_info[flash.block_idx][ii].erasable == TRUE )
        {
            if (    flash.write_address >=  flash_block_info[flash.block_idx][ii].start
             && flash.write_address <= (flash_block_info[flash.block_idx][ii].end & 0xFFFFFF00)
               )
            {
                valid_addr = TRUE;  // it belongs to a single sector (not crossing sector boundary)
            }
        }
    }

    if ( valid_addr == FALSE )
    {
        data_exchange.rsp.err.code = M16C62_INVALID_SECTOR; // Report invalid sector

        if ( hardware.mcu_transmit_in_big_endian == TRUE )
        {
            data_exchange.rsp.err.v.address.be.hi_w_hi_b = (INT8U) flash.write_address >> 16;   //  Address high byte
            data_exchange.rsp.err.v.address.be.hi_w_lo_b = (INT8U) flash.write_address >> 8;    //  Address mid byte
            data_exchange.rsp.err.v.address.be.lo_w_hi_b = (INT8U) flash.write_address; //  Address low byte
        }
        else
        {
            data_exchange.rsp.err.v.address.int32u = flash.write_address;
        }

        if ( term.db_lvl > 0 )
        {
            fprintf(TX_TERM,"SlowCmdProgFlash: Invalid sector:0x%lX\n\r",(flash.write_address & 0xFFF00));
        }
        data_exchange.rsp_bytes_in_buffer = 4; // the last byte off address is not sent
        return;
    }

    // --- Clear flash status report in response buffer ---
//  data_exchange.rsp.err.code = M16C62_NO_ERROR;  this is done at BgTsk()
    data_exchange.rsp.err.v.write.flash_stat.word = 0;

    // --- Transfer Flash function to RAM at 0x400 ---

    FlashTransfer();

    // --- Try up to twice to save sector in flash ---

    FlashCpuInit();                 // set processor for programming
    stat = FlashWrite();                // write 256 bytes into flash
    FlashCpuRestore();                  // Restore processor modes

    if ( stat != C62_FLASHOK )              // If write failed
    {
        FlashCpuInit();                 // set processor for programming
        stat = FlashWrite();                // write 256 bytes into flash
        FlashCpuRestore();              // Restore processor modes

        if ( stat != C62_FLASHOK )          // If write failed
        {
            // Report bad flash status
            if ( hardware.mcu_transmit_in_big_endian == TRUE )
            {
                data_exchange.rsp.err.v.write.flash_stat.be.hi = (INT8U) (stat >> 8);
                data_exchange.rsp.err.v.write.flash_stat.be.lo = (INT8U) stat;
            }
            else
            {
                data_exchange.rsp.err.v.write.flash_stat.word = stat;
            }

            addr = (INT8U FAR *)flash.write_address;        // Set pointer to start of code page

            // Scan page for mismatch locations
            for ( idx = 0; (idx < 0x100) && (*addr == flash.buf[idx]); idx++, addr++);
            {
            }

            if ( idx < 0x100 )                  // If page scanned with error
            {
                data_exchange.rsp.err.code = M16C62_PROG_FAILED;
                data_exchange.rsp.err.v.write.exp_value  = flash.buf[idx];  //  Expected byte value
                data_exchange.rsp.err.v.write.bad_value  = *addr;       //  Bad byte value

                if ( hardware.mcu_transmit_in_big_endian == TRUE )
                {
                    data_exchange.rsp.err.v.write.address.be.hi_w_hi_b = (INT8U) flash.write_address >> 16; //  Address high byte
                    data_exchange.rsp.err.v.write.address.be.hi_w_lo_b = (INT8U) flash.write_address >> 8;  //  Address mid byte
                    data_exchange.rsp.err.v.write.address.be.lo_w_hi_b = (INT8U) idx;           //  Address inside sector
                }
                else
                {
                    data_exchange.rsp.err.v.write.address.int32u = (INT32U) addr; // equivalent to = flash.write_address + idx;
                }


                if ( term.db_lvl > 0 )
                {
                    fprintf(TX_TERM,"SlowCmdProgFlash: failed @ 0x%lX%02X  Exp:0x%02X  Got:0x%02X\n\r",
                            (flash.write_address >> 8), idx,flash.buf[idx], *addr);
                }
                data_exchange.rsp_bytes_in_buffer = 8; // the last byte off address is not sent
                return;
            }
        }
    }
    data_exchange.rsp_bytes_in_buffer = 3; // err.code + flash_stat
}



void slowCmdReadIds(void)
{
	oneWireMasterFindIDs();

    // update the information we store in flash in case of change
    if ( data_exchange.rsp.err.code == M16C62_NO_ERROR )
    {
        if ( data_exchange.flash_needs_refresh )
        {

        	if (flash.type == FLASH_M16C62_P)
        	{
        		flash.block_mask = 0x10; // Block #4
        	}
        	else if (flash.type == FLASH_M16C62_A)
        	{
        		flash.block_mask = 0x04; // Set bit 2 to request that BLOCK_2 be erased
        	}
            

            slowCmdEraseFlash();    // Attempt to erase BLOCK_2 8Kb

            if ( data_exchange.rsp.err.code == M16C62_NO_ERROR )    // only if ok
            {
                FlashStore(C62_BLK2_ADDR, (INT8U *)&one_wire_net, ( (sizeof( struct TNetworkInfo ) + 0x0FF) / 0x100));
                data_exchange.flash_needs_refresh = FALSE;
                if ( data_exchange.rsp.err.code != M16C62_NO_ERROR )
                {
//                  return(FALSE);
                }
            }
        }
    }
}
