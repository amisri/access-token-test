/*---------------------------------------------------------------------------------------------------------*\
  File:         flash.c

  Purpose:      C62 Boot program - flash access functions

  Notes:        These functions were orignially written by Murray Voakes of Mitsubishi Electronics
                America, Inc.  The FlashFunc() function will be copied to the start of RAM at 0x400.
                0x100 bytes have been reserved in the memory map for the function (see start.a30).

  History:

    03/23/00    mv      Written
    29/04/04    qak     Adapted for C62 boot
\*---------------------------------------------------------------------------------------------------------*/

#include <flash.h>
#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)
#include <term.h>               // for term global variable
#include <main.h>
#include <m16c62_link.h>
#include <mcu_dependant.h>      // for OS_EXIT_CRITICAL(), OS_ENTER_CRITICAL(), USLEEP()
#include <iodefines.h>          // include sfr definitions
#include <slow_cmd.h>
#include <string.h>             // String library, for memcpy()
#include <stdio.h>              // for fprintf()

/*---------------------------------------------------------------------------------------------------------*/

// --- Static variables - file scope ---

static INT32U           flsh_addr;              // Address of sector to program in the flash
static INT16U           data_buf;               // Address of data buffer containing 256 bytes to be written
static INT16U           flsh_stat;              // Flash status returned by FlashFunc()
static INT8U            transfer_f;             // Flag that FlashFunc() has been transfered to RAM
static INT8U            cmd_num;                // Command to be run by FlashFunc()
static INT8U            pm1sav;                 // Storage for pm1 register
static INT8U            cm0sav;                 // Storage for cm0 register
static INT8U            cm1sav;                 // Storage for cm1 register

// --- External references ---

extern FAR INT8U        copy_start_c62a;
extern FAR INT8U        copy_end_c62a;
extern FAR INT8U        copy_start_c62p;
extern FAR INT8U        copy_end_c62p;

static void (*dummy_ptr)(void);

// --- Macros ---

#define ERASE           1
#define PROGRAM         2


typedef void (* flash_transfer_func_t )(void);

static flash_transfer_func_t flash_transfer_function;

static void  FlashTransferC62A(void);

static void  FlashTransferC62P(void);


// --- Assembler function in RAM ---

void    FlashCopiedFunc (void);

#pragma ADDRESS         FlashCopiedFunc 400h

/*---------------------------------------------------------------------------------------------------------*/
void FlashFuncC62A(void)
/*---------------------------------------------------------------------------------------------------------*\
    Inputs:

        cmd_num:    ERASE       = 1 = erase block
                    PROGRAM     = 2 = program page

        flsh_addr:  PROGRAM - address of sector to write   ERASE - address of last word of block
        data_buf:   PROGRAM - Address of 256 bytes of data to be written to flash

    Outputs:

        flsh_stat:  Low byte:   Flash block status (0x80 if no errors)
                    High byte:  1 = reset flash failed
                                2 = unknown command (not ERASE or PROGRAM)
                                3 = flash block status in low byte (0x0380 = operation OK)

 actually it is 97h bytes long, and we reserved 100h
 check again if this routine is modified
\*---------------------------------------------------------------------------------------------------------*/
{

#pragma ASM
;flash memory commands

	cstat_cmd   .equ    0050h                   ; Clear status
	rstat_cmd   .equ    0070h                   ; Read block status
	wrt_cmd     .equ    0041h                   ; Write sector of 256 bytes
	erase_cmd   .equ    0020h                   ; Erase block
	cfm_cmd     .equ    00D0h                   ; Confirm erase
	rda_cmd     .equ    00ffh                   ; Read array
	fmcr        .equ    03b7h                   ; M16C62 flash control register
	bsy_rdy     .btequ  0,fmcr                  ; Busy/ready bit in flash control register
	cpu_rwrt    .btequ  1,fmcr                  ; CPU rewrite mode bit in flash control register
	lock_dis    .btequ  2,fmcr                  ; Lock bit disable bit in flash control register
	reset       .btequ  3,fmcr                  ; Flash reset bit in flash control register

_copy_start_c62a:

; first do instructions common to all commands (save RAM space)

	mov.w       _flsh_addr+2,a1
	mov.w       _flsh_addr,a0           ;get block address
	bclr        cpu_rwrt
	bset        cpu_rwrt                ;set CPU rewrite mode

; reset flash and check that it is not blocked

	mov.w       #0100h,r0               ;prepare status word = 0x0100
	bset        reset                   ;reset flash memory
	bclr        reset
	btst        bsy_rdy                 ;check that flash is ready
	jz          ReturnStatus            ;return status = 0x0100 if flash is blocked

; sequence to reset flash and clear status register

	mov.b       #rda_cmd,r0l            ;read array resets flash
	ste.w       r0,[a1a0]
	mov.b       #cstat_cmd,r0l          ;clear block status
	ste.w       r0,[a1a0]

; select command to run (ERASE or PROGRAM)

Select:
	cmp.b       #ERASE,_cmd_num
	jeq         EraseFlash              ;erase block?
	cmp.b       #PROGRAM,_cmd_num
	jeq         ProgramFlash            ;write page?
	mov.w       #0200h,r0               ;prepare status word = 0x0200
	jmp         ReturnStatus

EraseFlash:

	bclr        lock_dis
	bset        lock_dis                ;unlock all blocks
	mov.b       #erase_cmd,r0l
	ste.w       r0,[a1a0]               ;send erase command
	mov.b       #cfm_cmd,r0l
	ste.w       r0,[a1a0]               ;send confirm byte

; erase or program command sent, now wait for flash to finish action

WaitFlash:

	btst        bsy_rdy                 ;wait for flash to be ready
	jz          WaitFlash

ReadStatus:
	mov.b       #rstat_cmd,r0l          ;read block status
	ste.w       r0,[a1a0]
	lde.w       [a1a0],r0
	mov.b       #03,r0h                 ;set high byte to 3 to indicate post-operation status
	mov.b       #rda_cmd,r1l            ;Read array resets flash
	ste.w       r1,[a1a0]

ReturnStatus:
	mov.w       r0,_flsh_stat           ;save block status in variable
	bclr        lock_dis
	bclr        cpu_rwrt
	rts                                 ;return to code in ROM

ProgramFlash:

	bclr        lock_dis
	bset        lock_dis                ;unlock all blocks
	mov.b       #wrt_cmd,r0l            ;Page program command
	ste.w       r0,[a1a0]
	mov.w       _data_buf,r1            ;get ram buffer address
	mov.w       #100h,r2                ;Prepare to loop for 256 bytes

ProgramLoop:

	xchg.w      r1,a0                   ;save flash address
	mov.w       [a0],r3                 ;get data
	xchg.w      r1,a0                   ;get write to address
	ste.w       r3,[a1a0]               ;and write
	add.w       #2,a0                   ;move flash address to next word
	add.w       #2,r1                   ;move data address to next word
	sub.w       #2,r2                   ;decrement bytes remaining counter
	jnz         ProgramLoop             ;loop until no bytes remaining
	mov.w       _flsh_addr,a0           ;reset block address
	jmp         WaitFlash               ;jump to wait for flash to finish saving block of 256 bytes

_copy_end_c62a:

#pragma ENDASM

}



void FlashFuncC62P(void)
{

#pragma ASM

;flash memory commands

    cstat_cmd   .equ    0050h                   ; Clear status
    rstat_cmd   .equ    0070h                   ; Read block status
    wrt_cmd     .equ    0040h                   ; Write sector of 256 bytes
    erase_cmd   .equ    0020h                   ; Erase block
    cfm_cmd     .equ    00D0h                   ; Confirm erase
    rda_cmd     .equ    00FFh                   ; Read array

;registers and relevant bits

    fmcr0       	.equ    01B7h               ; M16C62 flash control register
    fmcr1			.equ 	01B5h
    bsy_rdyp     	.btequ  0,fmcr0             ; Busy/ready bit in flash control register
    cpu_rwrt_p    	.btequ  1,fmcr0             ; CPU rewrite mode bit in flash control register
    lock_dis_p    	.btequ  2,fmcr0             ; Lock bit disable bit in flash control register
    fmr04			.btequ	4,fmcr0
	stat_flag   	.btequ  6,fmcr0
	eras_flag		.btequ  7,fmcr0
	rw0_flag		.btequ  1,fmcr1

_copy_start_c62p:

; first do instructions common to all commands (save RAM space)

    mov.w       _flsh_addr+2,a1
    mov.w       _flsh_addr,a0           		;get block address
	bclr		fmr04
    bclr        cpu_rwrt_p
    bset        cpu_rwrt_p                		;set CPU rewrite mode
    bclr		rw0_flag

; reset flash and check that it is not blocked

    mov.w       #0100h,r0               	;prepare status word = 0x0100
    btst        bsy_rdyp                 	;check that flash is ready
    jz          ReturnStatus_P            	;return status = 0x0100 if flash is blocked

; sequence to reset flash and clear status register

    mov.b       #rda_cmd,r0l            	;read array resets flash
    ste.w       r0,[a1a0]
    mov.b       #cstat_cmd,r0l          	;clear block status
    ste.w       r0,[a1a0]

; select command to run (ERASE or PROGRAM)

Select_P:
    cmp.b       #ERASE,_cmd_num
    jeq         EraseFlash_P              	;erase block?
    cmp.b       #PROGRAM,_cmd_num
    jeq         ProgramFlash_P            	;write page?
    mov.w       #0200h,r0               	;prepare status word = 0x0200
    jmp         ReturnStatus_P

EraseFlash_P:

    bclr        lock_dis_p
    bset        lock_dis_p                	;unlock all blocks
    mov.b       #erase_cmd,r0l
    ste.w       r0,[a1a0]               	;send erase command
    mov.b       #cfm_cmd,r0l
    ste.w       r0,[a1a0]               	;send confirm byte

; erase or program command sent, now wait for flash to finish action

WaitFlash_P:

    btst        bsy_rdyp                 	;wait for flash to be ready
    jz          WaitFlash_P

StatusCheck:
	btst		stat_flag
	jnz			ClearStatus
	btst		eras_flag
	jnz			ClearStatus
	jmp			ReadStatus_P

ClearStatus:
	mov.b	#cstat_cmd,r0l
	ste.w	r0,[a1a0]

ReadStatus_P:
    mov.b       #rstat_cmd,r0l          ;read block status
    ste.w       r0,[a1a0]
    lde.w       [a1a0],r0
    mov.b       #03,r0h                 ;set high byte to 3 to indicate post-operation status
    mov.b       #rda_cmd,r1l            ;Read array resets flash
    ste.w       r1,[a1a0]

ReturnStatus_P:
    mov.w       r0,_flsh_stat           ;save block status in variable
    bclr        lock_dis_p
    bclr        cpu_rwrt_p
    rts                                 ;return to code in ROM

ProgramFlash_P:

    bclr        lock_dis_p
    bset        lock_dis_p                ;unlock all blocks
    mov.w       _data_buf,r1            ;get ram buffer address
    mov.w       #100h,r2                ;Prepare to loop for 256 bytes

ProgramLoop_P:

	mov.b       #wrt_cmd,r0l            ;Page program command
	ste.w       r0,[a1a0]
    xchg.w      r1,a0                   ;save flash address
    mov.w       [a0],r3                 ;get data
    xchg.w      r1,a0                   ;get write to address
    ste.w       r3,[a1a0]               ;and write

WaitFlash2:
    btst        bsy_rdyp                 ;wait for flash to be ready
    jz          WaitFlash2

ContinueLoop:
	btst		stat_flag
	jnz			ClearStatus
	btst		eras_flag
	jnz			ClearStatus

    add.w       #2,a0                   ;move flash address to next word
    add.w       #2,r1                   ;move data address to next word
    sub.w       #2,r2                   ;decrement bytes remaining counter
    jnz         ProgramLoop_P             ;loop until no bytes remaining
    mov.w       _flsh_addr,a0           ;reset block address
    jmp         WaitFlash_P               ;jump to wait for flash to finish saving block of 256 bytes

_copy_end_c62p:

#pragma ENDASM

}
/*---------------------------------------------------------------------------------------------------------*/
void FlashCpuInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function sets the processor mode for programming flash. A wait state is added and the clock is set
  to the input frequency/2 (Xin/2). Original configuration saved in globals: cm0sav, cm1sav, pm01sav.
\*---------------------------------------------------------------------------------------------------------*/
{
    cm0sav      = cm0;                                  // save current CPU modes and clock setting
    cm1sav      = cm1;
    pm1sav      = pm1;

    // prcr, pm1, cm0 & cm1: same address for A and P
    prcr        = 0x03;                                 // unprotect CM0 - CM1 (bit 0), PM0 - PM1 (bit 1)

    cm0 = 0x08;
    cm17 = 0;
    cm16 = 1;
    pm17 = 1;


    prcr        = 0x00;                                 // CM0, CM1, PM0, PM1 write protected
}
/*---------------------------------------------------------------------------------------------------------*/
void FlashCpuRestore(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function restores the processor mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    prcr        = 0x03;                                 // unprotect CM0, CM1, PM0, PM1

    pm1         = pm1sav;                               // Restore cpu registers
    cm1         = cm1sav;
    cm0         = cm0sav;

    prcr        = 0x00;                                 // CM0, CM1, PM0, PM1 write protected
}



void FlashSetFlashTransferC62A(void)
{
	flash_transfer_function = FlashTransferC62A;
}



void FlashSetFlashTransferC62P(void)
{
	flash_transfer_function = FlashTransferC62P;
}


void FlashTransfer(void)
{
	flash_transfer_function();
}


void FlashTransferC62A(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function copies the flasher code to RAM on the first call.
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( transfer_f == 0 )                                      // If not yet copied
    {
        memcpy( (const void FAR *)FlashCopiedFunc,              // To RAM at 400h
                (const void FAR *)&copy_start_c62a,                  // From code area in flash
                (size_t)(&copy_end_c62a - &copy_start_c62a));               // for length of assembler code

        transfer_f = 1;
    }
}


void FlashTransferC62P(void)
{
	if ( transfer_f == 0)
	{
		memcpy( (const void FAR *) FlashCopiedFunc,
				(const void FAR *) &copy_start_c62p,
				(size_t) ( &copy_end_c62p - &copy_start_c62p));

		transfer_f = 1;
	}
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U FlashErase(INT32U addr)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases the flash block for which addr is the address of the last word.  It returns
  the flash block status.  If the erase is successful, the status will be 0x0380.
\*---------------------------------------------------------------------------------------------------------*/
{
#if OS_CRITICAL_METHOD == 3                      // Allocate storage for CPU status register
    OS_CPU_SR   cpu_sr = 0;
#endif

    while( term.n_ch != 0 );                    // Wait for terminal characters to all be sent

    if (term.term_ch != 0)                      // if the MCU still has a ch to read lets give it a chance
    {
        USLEEP(WAIT_MCU_SCAN);
    }

    // fill global variables used by the assembler routine in RAM
    cmd_num   = ERASE;
    flsh_addr = addr;

    OS_ENTER_CRITICAL();
    FlashCopiedFunc();
    OS_EXIT_CRITICAL();

    return(flsh_stat);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U FlashWrite(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write the 256 bytes of data in flash.buf into the flash at flash.write_address.
  If the programming is successful, the status will be 0x0380.
\*---------------------------------------------------------------------------------------------------------*/
{

#if OS_CRITICAL_METHOD == 3                      // Allocate storage for CPU status register
    OS_CPU_SR   cpu_sr = 0;
#endif

    while( term.n_ch != 0 );                    // Wait for terminal characters to all be sent

    if (term.term_ch != 0)                      // if the MCU still has a ch to read lets give it a chance
    {
        USLEEP(WAIT_MCU_SCAN);
    }

    // Dummy assignments to get rid of compilation warnings about unused functions

    dummy_ptr = FlashFuncC62A;
    dummy_ptr = FlashFuncC62P;

    data_buf = (INT16U)&flash.buf;

    // fill global variables used by the assembler routine in RAM
    cmd_num   = PROGRAM;
    flsh_addr = flash.write_address;

    OS_ENTER_CRITICAL();
    FlashCopiedFunc();
    OS_EXIT_CRITICAL();

    return(flsh_stat);
}


/*---------------------------------------------------------------------------------------------------------*/
void FlashStore(INT32U flash_addr, INT8U *data, INT16U sectors)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write the specified number of sectors of 256 byte of data into the flash memory
  starting at flash_addr.  This address should be on a sector boundary.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      ii;

    if ( term.db_lvl >= 2 )
    {
        fprintf(TX_TERM,"FlashStore(0x%lX,0x%X,%u)\n\r", flash_addr, (INT16U) data, sectors);
    }

    for ( ii = 0; ii < sectors; ii++)
    {
        memcpy(flash.buf, data, 0x100);

        flash.write_address = flash_addr;       // Set flash programming address
        slowCmdProgFlash();                     // Try to write the sector into flash


        if ( data_exchange.rsp.err.code != M16C62_NO_ERROR )                    // If erase failed
        {
            return;
        }

        flash_addr += 0x100;                            // Advance to next sector
        data += 0x100;
    }
}
