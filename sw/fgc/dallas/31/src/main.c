/*---------------------------------------------------------------------------------------------------------*\
  File:         main.c

  Purpose:      M16C62 ID program
                main

  Notes:        The ID program has two mode: STANDALONE and NORMAL.  The mode is determined by the byte
                it reads from the MCU when it starts.  If it finds the MAGIC_IdBoot,
                it will operate in NORMAL mode.  It returns the handshake byte (the NOT of the command
                byte) and waits for new commands.

                In STANDALONE, it runs the ID program monitor which provides the user with a simple menu
                of options to help with debugging.  The monitor communicates through the serial port.

                It is possible to run the ID program monitor when in NORMAL mode.  This can be done from
                the MCU boot and allows detailed debugging of the ID program in the field.  In this
                case the keyboard and terminal characters are exchanged via the parallel data link
                with the MCU and relayed to the FGC terminal by the MCU boot program.

                The MCU (HC16) is a big endian processor while the M16C62 is a little endian processor.  All
                communication between the two follows the MCU standard and is big endian.

                Definitions of terms:

                PAGE    A page is a 64KB zone within the memory, aligned on a 64KB boundary.
                        The M16C62 is like the MCU and has 20 bit addressing, which allows for 16
                        pages to be addressed (0x00000 to 0xFFFFF).

                BLOCK   A block is a zone within the M16C62 flash memory that can be erased in one
                        operation.  The M16C62 has 4 pages of flash divided into seven blocks:

                                Block   Size    Address                 Use
                                  0      16K    0xFC000 - 0xFFFFF       M16C62 boot (top 4KB only) or DB monitor
                                  1       8K    0xFA000 - 0xFBFFF       Part type database
                                  2       8K    0xF8000 - 0xF9FFF       Scan results (Couplers+Devices)
                                  3      32K    0xF0000 - 0xF7FFF       Code 2 containing ID Program (P6)
                                  4      64K    0xE0000 - 0xEFFFF       Dallas to part index database
                                  5      64K    0xD0000 - 0xDFFFF       Dallas to part index database
                                  6      64K    0xC0000 - 0xCFFFF       Dallas to part index database

                        The M16C62 boot will only allow erase and program operations on blocks 1-6.

                SECTOR  A sector is a zone of 256 bytes within the M16C62 flash memory that can be
                        programmed in one operation.  Sectors are aligned on 256 bytes addresses.
                        There are 256 sectors per page, and the sector index is the top byte of
                        the address of the first byte of sector within the page.

                        Note that within the M16C62 documentation, a sector is referred to as a page.
  History:

    20/07/04    qak     Created from M16C62 Boot main.c
    29/04/09    pfr     Modified to swap Uart0 & 2
\*---------------------------------------------------------------------------------------------------------*/

#define MAIN_GLOBALS
#define M16C62_GLOBALS
#define TERM_GLOBALS
#define FLASH_GLOBALS
#define ISR_GLOBALS


#include <main.h>

#include <ds2480.h>
#include <flash.h>
#include <iodefines.h>
#include <isr.h>
#include <mcu_dependant.h>
#include <onewire_master.h>
#include <sa.h>
#include <slow_cmd.h>
#include <term.h>
#include <utilities.h>

#include <stdbool.h>
#include <string.h>


#ifdef DEBUG_FGC3

void Fgc3_debug_init(void)
{
    // --- UART2 - Terminal ---

    u2mr        =   0x05;       // Internal clk, 8 data bits, 1 stop bit, no parity
    u2c0        =   0x10;       // Use f1, CTS/RTS disabled, transfer LSB first
    u2brg       =   0x8;        // Set u0brg = f1/(16*baud) - 1   (0x8 = 115200 when f1=16MHz)
    u2c1        =   0x01;       // Enable Tx

}


void Fgc3_debug_print(void)

#endif


// Internal function declarations

/*!
 * Sets the hardware type to the corresponding platform (FGC2 or FGC3)
 */
static void setHardwareType(void);


/*!
 * Performs common settings for both FGC platforms
 */
static void doCommonSettings(void);


/*!
 * Initialization with platform FGC2 and 1-Wire bridge DS2480
 */
static void initFGC2WithDS2480(void);


/*!
 * Initialization with platform FGC3 and 1-Wire bridge DS2480
 */
static void initFGC3WithDS2480(void);


/*!
 *	Sets the value for different function pointers that will be used across the different modules.
 *	This decouples the HW platform from the SW.
 */
static void setFGCPlatformHandlers(struct t_hardware* hw);


// Internal variable definitions

utilities_fgc_platform_handler initFGC;


// Internal function definitions

static void setHardwareType(void)
{

	uint8_t c62_type;

	hardware.model = FGC2;

	if ( p10_0 == 0)
	{
		hardware.model = FGC3_DALLAS_DS2480;
	}

	if ( p10_7 == 0)
	{
		hardware.model = FGC3_DS2480_MAX3648;
	}

	hardware.mcu_transmit_in_big_endian = TRUE;


	// M16C62; A or P?
	firp = 0xFF;
	c62_type = firp & 0x03;

	if ( c62_type == FLASH_M16C62_P )
	{
		flash.type = FLASH_M16C62_P;
		flash.block_idx = 0;
		FlashSetFlashTransferC62P();

	}
	else
	{
		fira = 0xFF;
		c62_type = fira & 0x03;

		if ( c62_type == FLASH_M16C62_A )
		{
			flash.type = FLASH_M16C62_A;
			flash.block_idx = 1;
			FlashSetFlashTransferC62A();

		}
		else
		{
			//Todo: error handling if chip not A/P/other working version
		}
	}
}


static void doCommonSettings(void)
{

	// P10_0 to P10_3 pull-up
	pu24 = 1;

	// P10_4 to P10_7 pull-up
	pu25 = 1;

	// --- Port 1 and Port 3 ---
	pd1         = 0x80;         // Set port 1 bit 7 as output (CMDBUSY), the rest as inputs (INT3 included)
	pd3         = 0x03;         // Set port 3 bit 0 (DATARDY) and 1 (CMDERR) as outputs

	p3          = 0x03;         // set p3.0 DATARDY and p3.1 CMDERR lines HIGH
	PLD_CMD_BUSY_SIGNAL = 1;    // set p1.7 CMDBUSY to HIGH

	// --- Timer A0 - 1 Mhz clock (f1/16) ---
	ta0         =   15;         // 1MHz free running timer using 16MHz clock
	ta0s        = 0x01;         // Start timer A0

	// --- Timer A1 - Free running counter at 1 Mhz (uses Timer A0 as input) - used by usleep() macro ---
	ta1mr       =   0x41;       // Timer mode register: event counter mode, free running
	trgsr       =   0x02;       // Trigger select register: Timer A1 uses TA0 overflow
	udf         =   0x02;       // Increment counter for Timer A1
	ta1s        =   0x01;       // Start timer A1

	// --- UART0 - for FGC2 : Dallas 2480 line driver ---
	// --- UART0 - for FGC3 : Terminal ---
	u0mr        =   0x05;       // Internal clk, 8 data bits, 1 stop bit, no parity
	u0c0        =   0x10;       // Use f1, CTS/RTS disabled, transfer LSB first
	u0brg       =   0x67;       // Set u0brg = f1/(16*baud) - 1   (0x67 = 9600 when f1=16MHz)
	u0c1        =   0x05;       // Enable Rx and Tx


	// --- UART2 - for FGC2 : Terminal ---
	// --- UART2 - for FGC3 : Dallas 2480 line driver ---
	u2mr        =   0x05;       // Internal clk, 8 data bits, 1 stop bit, no parity
	u2c0        =   0x10;       // Use f1, CTS/RTS disabled, transfer LSB first
	u2brg       =   0x67;       // Set u0brg = f1/(16*baud) - 1   (0x67 = 9600 when f1=16MHz)
	u2c1        =   0x05;       // Enable Rx and Tx
}


static void initFGC2WithDS2480(void)
{
	hardware.n_branches =  5;

	hardware.mcu_link_rx = (INT8U NEAR *) &p0;  // Rx link with MCU - Set port p0 direction to all inputs
	pd0 = 0x00;                 // Rx - Set port p0 direction to all inputs

	hardware.mcu_link_tx = (INT8U NEAR *) &p2;  // Tx link with MCU - Set port p2 direction to all outputs
	pd2 = 0xff;                 // Tx - Set port p2 direction to all outputs

	hardware.id_bus_tx = (INT16U NEAR *) &u0tbl;
	hardware.id_bus_tx_ctrl = (union uartc_def NEAR *) &u0c0_addr;

	hardware.term_tx   = (INT8U NEAR *) &u2tbl;

	s0ric = 0x02;       // Select interrupt level 2 for UART0 data reception, IsrDallasRx()
	s2tic = 0x01;       // Select interrupt level 1 for UART2 transmission register empty, IsrTermTx()
}



static void initFGC3WithDS2480(void)
{
	hardware.n_branches =  6;

	hardware.mcu_link_rx = (INT8U NEAR *) &p2;  // Rx link with MCU - Set port p2 direction to all inputs
	pd2 = 0x00;                 // Rx - Set port p2 direction to all inputs

	hardware.mcu_link_tx = (INT8U NEAR *) &p0;  // Tx link with MCU - Set port p0 direction to all outputs
	pd0 = 0xff;                 // Tx - Set port p0 direction to all outputs

	hardware.id_bus_tx = (INT16U NEAR *) &u2tbl;
	hardware.id_bus_tx_ctrl = (union uartc_def NEAR *) &u2c0_addr;

	hardware.term_tx   = (INT8U NEAR *) &u0tbl;

	s0tic = 0x01;       // Select interrupt level 1 for UART0 transmission register empty, IsrTermTx()
	s2ric = 0x02;       // Select interrupt level 2 for UART2 data reception, IsrDallasRx()


	if (hardware.model == FGC3_DS2480_MAX3648)
	{
		pd1 = 0x87;
	}
}


static void setFGCPlatformHandlers(struct t_hardware* hw)
{

	switch(hw->model)
	{
		case FGC3_DS2480_MAX3648:
			initFGC = initFGC3WithDS2480;

			ds2480SetUartFGC3();
			utilitiesSetGetCharacterFunctionFGC3();
			saSetWaitTxQueueFunctionFGC3();
			isrSetGetDallasByteFunctionFGC3();

			oneWireMasterSetMax4638Functions();

			break;

		case FGC3_DALLAS_DS2480:
			initFGC = initFGC3WithDS2480;

			ds2480SetUartFGC3();
			utilitiesSetGetCharacterFunctionFGC3();
			saSetWaitTxQueueFunctionFGC3();
			isrSetGetDallasByteFunctionFGC3();

			oneWireMasterSetDs2409Functions();

			break;

		case FGC2:
			initFGC = initFGC2WithDS2480;

			ds2480SetUartFGC2();
			utilitiesSetGetCharacterFunctionFGC2();
			saSetWaitTxQueueFunctionFGC2();
			isrSetGetDallasByteFunctionFGC2();

			oneWireMasterSetDs2409Functions();

			break;

		default:
			break;
	}

}


void main(void)
{

	// Avoids to execute the ISR until the initialization is finish, BgTsk() ready
	isr.cmd_in_progress = TRUE;

	doCommonSettings();

	setHardwareType();

    setFGCPlatformHandlers( &hardware );

    initFGC();

    // --- Prepare terminal stream ---
    term.f._flag         = _IOWRT;      // Set stream flag to write-only
    term.f._mod          = _BIN;        // Set stream mode to binary
    term.f._func_out     = SaSendCh;    // Link character output function to stream



    // --- Start in mode according to 1st data byte read ---

    // if the last cmd send by the MCU to IdBoot was C62_SWITCH_PROG it remains in *(hardware.mcu_link_rx)
    // and IdBoot will jump here without changing it, signaling that we must run the BgTsk()
    // and not the monitor
    *(hardware.mcu_link_tx) = ~ *(hardware.mcu_link_rx); // Return complement to say "I'm alive"


    // recover MicroLAN data saved in FLASH
    memcpy( &one_wire_net, (void FAR *) C62_BLK2_ADDR, sizeof( struct TNetworkInfo ) );
    data_exchange.flash_needs_refresh = FALSE;

    // Set INT3 priority level to 3 (MCU commands)
    int3ic = 3;

    if ( *(hardware.mcu_link_rx) == C62_SWITCH_PROG )   // stay in IdProg ?
    {
        USLEEP( DS2480_WAIT_RESET_US );                        // Wait 4ms for DS2480 to complete reset

        asm("fset i");                                  // Enable interrupts

        slowCmdBgTsk();                        // Run background task and wait for MCU commands
    }
    else                                // else standalone operation (with Dev Front Panel)
    {
        asm("fset i");                                  // Enable interrupts

        SaMonitor();                    // Run monitor and wait for terminal commands
    }
} // End main
