/*!
 * @file	utilities.c
 * @brief	Some utilities functions from the ds2480 and ds2482 translation units
 *
 */

#ifndef UTILITIES_H
#define UTILITIES_H

#ifdef UTILITIES_GLOBALS
#define UTILITIES_VARS_EXT
#else
#define UTILITIES_VARS_EXT extern
#endif

#include <microlan_1wire.h>

#include <stdint.h>
#include <stdbool.h>


// Constants

// Types

typedef void (* utilities_fgc_platform_handler)(void);

typedef void (* utilities_fgc_platform_handler_arg)(void* arg);

typedef void (* utilities_fgc_platform_handler_2args)(const uint8_t arg1, const uint8_t arg2);

typedef bool (* utilities_fgc_platform_temp_func_handler)(const uint8_t arg);

typedef bool (* utilities_fgc_platform_temp_func_handler_2args)(const uint8_t arg1, const uint8_t arg2);


// External structures, unions and enumerations

// Platform/class specific functions

// External function declarations


/*!
 * This function will translate the supplied device ID into an ASCII string so that it can be printed with
 * the most significant byte (CRC) first.
 */
const char* utilitiesDallasIdToString(union TMicroLAN_unique_serial_code FAR * devid);


/*!
 * Computes the CRC8 value based on the Maxim's documentation (see Maxim's application note 27;
 * "Understanding and Using Cyclic Redundancy Checks with Maxim iButton Products").
 */
uint8_t utilitiesCalculateCRC8(const uint8_t crc, const uint8_t dallas_byte);


/*!
 * Sets the sa_get_char variable with the appropriate handler for the FGC2 platform
 */
void utilitiesSetGetCharacterFunctionFGC2(void);


/*!
 * Sets the sa_get_char variable with the appropriate handler for the FGC3 platform
 */
void utilitiesSetGetCharacterFunctionFGC3(void);



/*!
 * Waits for a character from the terminal keyboard on UART2 for the FGC2 UART0 for the FGC3
 * or the remote terminal keyboard linked via the MCU boot.
 * @return The read character
 */
uint8_t utilitiesGetChar(void);


// External variable definitions

#endif /* UTILITIES_H_ */
