/*---------------------------------------------------------------------------------------------------------*\
  File:         term.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef TERM_H  // header encapsulation
#define TERM_H

#ifdef TERM_GLOBALS
    #define TERM_VARS_EXT
#else
    #define TERM_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs for Renesas NC30 compiler (M16C target)
#include <stdio.h>      // Standard IO library, for FILE

//-----------------------------------------------------------------------------------------------------------

#define TX_TERM                 &term.f         // Terminal stream pointer
#define TERM_BUF_SIZE           256             // Terminal circular buffer size

//-----------------------------------------------------------------------------------------------------------

struct term
{
    INT16U              db_lvl;                 // Debug level
    INT16U              n_ch;                   // Number of characters in the buffer
    INT8S               kbd_ch;                 // Keyboard character from HC16
    INT8U               uart_copy;              // Copy of UART transmit buffer (it is write only)
    INT8U               term_ch;                // Terminal character to HC16
    INT8U               in;                     // Buffer input index
    INT8U               out;                    // Buffer output index
    INT8U               pad;                    // Pad to compensate misalignment in FILE
    FILE                f;                      // Buffered IO stream
    INT8S               buf[TERM_BUF_SIZE];     // Pointer to transmission buffer
};

//-----------------------------------------------------------------------------------------------------------

TERM_VARS_EXT struct term       term;           // Terminal Variables

//-----------------------------------------------------------------------------------------------------------

#endif  // TERM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: term.h
\*---------------------------------------------------------------------------------------------------------*/
