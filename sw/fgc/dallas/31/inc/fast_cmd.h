/*---------------------------------------------------------------------------------------------------------*\
  File:         fastCmd.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FASTCMD_H       // header encapsulation
#define FASTCMD_H

#ifdef FASTCMD_GLOBALS
    #define FASTCMD_VARS_EXT
#else
    #define FASTCMD_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)

//-----------------------------------------------------------------------------------------------------------

INT8U   FastCmdSetBigEndian     (INT8U arg);
INT8U   FastCmdSetLittleEndian  (INT8U arg);
INT8U   FastCmdGetResponse      (INT8U arg);
INT8U   FastCmdSetAddress       (INT8U arg);
INT8U   FastCmdSetData          (INT8U arg);
INT8U   FastCmdSetBlockMask     (INT8U arg);
//INT8U FastCmdSetCrcLen        (INT8U arg);
INT8U   FastCmdSetDebugLevel    (INT8U arg);
INT8U   FastCmdSetBranchIdx     (INT8U arg);
INT8U   FastCmdSetDevIdx        (INT8U arg);
INT8U   FastCmdSetKbdChar       (INT8U arg);
INT8U   FastCmdGetTermChar      (INT8U arg);

/*!
 *  @brief Saves external Dallas ID passed by the MCU
 *
 *  The command saves external Dallas ID passed to m16c62 by the MCU. Dallas ID consists of 8 bytes
 *  passed to the command one by one. First call initializes the command (the argument is not used),
 *  so to pass the ID the command should be called 9 times in total.
 *
 *  @param[in] arg Next byte of Dallas ID
 *
 *  @return Number of bytes still expected by the command
 *
 */
INT8U   FastCmdSetExternalId    (INT8U arg);

//-----------------------------------------------------------------------------------------------------------

#endif  // FASTCMD_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: fastCmd.h
\*---------------------------------------------------------------------------------------------------------*/
