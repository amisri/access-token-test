/*!
 * @file onewire_master.h
 * @brief 
 */

#ifndef ONEWIRE_MASTER_H
#define ONEWIRE_MASTER_H

#ifdef ONEWIRE_MASTER_GLOBALS
    #define ONEWIRE_MASTER_VARS_EXT
#else
    #define ONEWIRE_MASTER_VARS_EXT extern
#endif


// Includes
#include <microlan_1wire.h>

#include <stdbool.h>
#include <stdint.h>



// Constants


/*!
 * Maximum read attempts for ID and temperature scans
 */
#define ONE_WIRE_MASTER_MAX_RETRIES 5


/*!
 * Size in bytes of the DS18B20 temperature sensor's scratchpad.
 */
#define ONE_WIRE_MASTER_DS18B20_SCRATCHPAD_SIZE 9


// External structures, unions and enumerations 


/*!
 * Reset modes of the one wire master, soft and hard (or master). Both also send a 1-Wire reset
 * pulse to the 1-Wire line.
 */
typedef enum onewire_master_reset_modes
{
	ONEWIRE_MASTER_INIT_SOFT_RESET 		= false,
	ONEWIRE_MASTER_INIT_HARD_RESET 		= true

} onewire_master_reset_modes_t;


/*!
 * 1-Wire generic ROM commands.
 */
typedef enum onewire_master_rom_commands
{
	ONEWIRE_MASTER_READ_ROM   =  0x33, //!< Read the ID of a 1-W slave
	ONEWIRE_MASTER_SKIP_ROM   =  0xCC, //!< Broadcast command to the 1-W line
	ONEWIRE_MASTER_MATCH_ROM  =  0x55, //!< Send a command to a specific slave
	ONEWIRE_MASTER_SEARCH_ROM =  0xF0  //!< Command to initiate the ID search
} onewire_master_rom_commands_t;


/*!
 * Return types of the onewire_master API
 */
typedef enum onewire_master_cmd_results
{
	ONEWIRE_MASTER_COMMAND_OK,
	ONEWIRE_MASTER_COMMAND_TIMEOUT,
	ONEWIRE_MASTER_COMMAND_CRC_ERROR,
	ONEWIRE_MASTER_COMMAND_MAX_ATTEMPTS,
	ONEWIRE_MASTER_COMMAND_ERROR
} onewire_master_cmd_results_t;


/*!
 * Commands sent to the DS18B20's
 */
typedef enum onewire_master_ds18b20
{
	ONEWIRE_MASTER_DS18B20_CMD_CONVERT_TEMP  		=  0x44,  //!< Convert Temperature
	ONEWIRE_MASTER_DS18B20_CMD_WRITE_SCRATCHPAD   	=  0x4E,  //!< Write Scratchpad
	ONEWIRE_MASTER_DS18B20_CMD_READ_SCRATCHPAD   	=  0xBE,  //!< Read Scratchpad
	ONEWIRE_MASTER_DS18B20_CONFIG_12BITS_750MS		=  0x7F,  //!< Temperature resolution
	ONEWIRE_MASTER_DS18B20_ACQ_TEMPERATURE_TIMEOUT	=  1500,  //!< 1500 ms, 1048 strong pulse + idle time
	ONEWIRE_MASTER_DS18B20_CONFIRM_SCRATCHPAD_READ  =  0xFF	  //!< Sent after receiving a scratchpard byte to continue reception
} onewire_master_ds18b20_t;


// External function declarations

/*!
 * Sets the one wire master functions (find IDs, temperature reading) to the appropriate
 * one according to the HW (MAX4638 with DS2480B).
 */
void oneWireMasterSetMax4638Functions(void);


/*!
 * Sets the one wire master functions (find IDs, temperature reading) to the appropriate
 * one according to the HW (3 x DS2409 with DS2480B).
 */
void oneWireMasterSetDs2409Functions(void);


/*!
 * Executes the 1-Wire master chip reset and sends a 1-Wire reset pulse to the 1-Wire line.
 * @return Result of command execution.
 */
onewire_master_cmd_results_t oneWireMasterReset(const bool request_hard_reset);


/*!
 * This function will select a particular device on the bus.
 * If there are no errors it will return TRUE.
 */
onewire_master_cmd_results_t oneWireMasterSelectDeviceInMicroLAN(const union TMicroLAN_unique_serial_code FAR *serial_code_ptr);


/*!
 * This function will loop through all the 1-Wire channels and will find all
 * devices connected to it, making use of the function pointed by onewire_master_find_ids, specific of the HW platform.
 * It also reads the temperature of the DS18B20 temperature sensors.
 * Finally, it compares the search results with what was previously stored in the flash memory: if it has found
 * something different, it will set a flag so that the flash memory is refreshed.
 * It also prepares the structure used to exchange information with the M16C62 with the data.
 */
onewire_master_cmd_results_t oneWireMasterFindIDs(void);


/*!
 * Issues the necessary commands to the DS2480 to command the DS18B20 1-W slaves to initiate the temperature conversion.
 * In 'parasite power' mode the devices needs to be supplied by a strong pulled-up on the 1-wire bus during the conversion.
 * The strong pull-up is armed immediately following every byte on the 1-wire bus.
 * The 18B20s should convert at 12-bit resolution by default, but this does not always seem to be true, so
 * the configuration register is actively written to DS18B20_CONFIG_12BITS_750MS.
 * @return Whether the temperature conversion was successful or not
 */


/*!
 * Executes the set Read Temperature function, looping through all logical paths.
 * On branches where there are temperature sensors, it will first issue a convert temperature command.
 * Then it will address every sensor individually, asking them to send their scratchpad data.
 * @param Logical path where to start reading temperatures
 * @param Logical path where to end reading temperatures
 * @return Command result
 */
onewire_master_cmd_results_t oneWireMasterReadTemperatures(const uint8_t start_branch, const uint8_t end_branch);


// External variable definitions

/*!
 * Structure storing all the 1-Wire network information
 */
ONEWIRE_MASTER_VARS_EXT struct TNetworkInfo one_wire_net;

/*!
 * Labels the logical paths into the branches' names
 */
ONEWIRE_MASTER_VARS_EXT const char FAR* const branch_labels[MICROLAN_NETWORK_LOGICAL_PATHS]
#ifdef ONEWIRE_MASTER_GLOBALS
=
{
		"VS A",
		"IMEAS A",
		"VS B",
		"IMEAS B",
		"LOCAL",
		"CRATE"
}
#endif
;

#endif
// EOF
