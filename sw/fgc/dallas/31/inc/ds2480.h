
#ifndef DS2480_H
#define DS2480_H

#ifdef DS2480_GLOBALS
#define DS2480_VARS_EXT
#else
#define DS2480_VARS_EXT extern
#endif


// Includes

#include <microlan_1wire.h>
#include <utilities.h>

#include <stdint.h>


// Constants


/*!
 * For the DS2480 to complete the reset. Public, as it is used in the main
 */
#define DS2480_WAIT_RESET_US 4500


/*!
 * DS2480's RX data buffer size in bytes
 */
#define DS2480_RESPONSE_MAX_LEN_BYTES 4


/*!
 * Maximum number of 1-W slaves on a single branch
 */
#define MICROLAN_NETWORK_ELEMENTS_PER_BRANCH_MAX 80



// Types


/*!
 * Timeouts used when sending bytes to the DS2480B. Default is 20 ms, although
 * a certain number of sent commands are set with no timeout (zero ms value).
 */
typedef enum ds2480_timeouts
{

	DS2480_RESPONSE_NO_TIMEOUT_MS 	= 0,
	DS2480_RESPONSE_TIMEOUT_MS 		= 20

}
ds2480_timeouts_t;



// External structures, unions and enumerations


/*!
 * Keeps track of the expected bytes and the actual received bytes after issuing a command
 * to the DS2480B.
 */
struct ds2480_cmd_bytes
{
    uint8_t       received_bytes;
    uint8_t       expected_bytes;
};


/*!
 * Structure that represents the global state of the DS2480B.
 */
struct ds2480_state_info
{
    uint8_t                  data_buffer[DS2480_RESPONSE_MAX_LEN_BYTES]; //!< Buffer to store received data from the 1-Wire line
    bool                     link_ok;                       			 //!< Link alive, no timeout
    uint8_t                  last_mode_set;                  			 //!< CMD or DATA
    struct ds2480_cmd_bytes  cmd;										 //!< Expected and actual received bytes from a command
    struct TBranchSummary    branch_summary;							 //!< Inventory of 1-Wire slaves found in a branch
    uint8_t search_accelerator_retries[MICROLAN_NETWORK_ELEMENTS_PER_BRANCH_MAX];     //!< Read attempts counter
    union TMicroLAN_unique_serial_code branch_ids[MICROLAN_NETWORK_ELEMENTS_PER_BRANCH_MAX]; //!< Dallas IDs for each 1-Wire slave found
};


// External function declarations

/*!
 *  Sets the uart_handler variable with the appropriate handler for the FGC3 platform
 */
void ds2480SetUartFGC3(void);


/*!
 *  Sets the uart_handler variable with the appropriate handler for the FGC2 platform
 */
void ds2480SetUartFGC2(void);


/*!
 * This function will send a byte to the DS2480 Dallas Serial 1-Wire Line Driver
 * via UART0 for FGC2
 * via UART2 for FGC3

 * If bytes are expected in reply, it will wait up to the specified timeout for the complete response.
 * If a timeout occurs  ds2480_state.link_ok will be FALSE.
 * Subsequent transmissions will be blocked until a full reset is requested.

 * If a response is received, the last data byte received (via the UART RX interrupt) will be in ds2480_state.data_buffer.
 */
bool ds2480SendByte(const uint8_t tx_data, const uint8_t expected_bytes, const uint16_t timeout_ms);


/*!
 * The reset command must be used to begin all 1-wire communication. If want_master_reset is set,
 * then it will request a master reset cycle from the DS2480 and will clear the timeout flag.
 * Otherwise it will do a soft reset of the 1-wire bus, but not of the DS2480.
 * If there are no errors it will return TRUE.
*/
bool ds2480Detect(const bool want_master_reset);


/*!
 * Copies the ID search's results, stored in the temporary variable ds2480_state into one_wire_net, which
 * is the structure that will be copied into flash memory; resets the temperature for every DS18B20 sensor to zero
 * and finally looks for the part code (HCR) associated to every Dallas ID found using the function DbLookup.
 */
void ds2480BuildDevicesSummary(const uint8_t logical_path, uint16_t *const logical_element);


/*!
 * The host must learn the individual ROM codes for each of the devices on the 1-Wire bus.
 * This is accomplished by running a Search ROM algorithm.
 *
 * The host sends the Search ROM command to the slaves and places the 1-Wire Master
 * in Search ROM Accelerator mode.
 * The host transmits the 16 byte search value based on the last ROM value found. These 16 bytes are 0x00 for the first run.
 * The 16 bytes returned contain the new ROM code and are also used to generate the next 16 bytes to transmit.
 * This process is repeated until serial numbers duplicate to find all devices.
 * The ds2480IdRecoverDevice function will generate the new data to transmit and extract the
 * new ROM code from the data just received.
 * The function can scan the top level bus between the DS2480 and the DS2409s (logical_path = MICROLAN_NETWORK_LOGICAL_PATHS)
 * or a branch of a DS2409 (logical_path = [0..4/5].
 * If there are no errors it will return TRUE.
 */
bool ds2480FindMicroLANDevicesCommon(const uint8_t logical_path, const bool check_couplers_presence);


/*!
 * Enables strong pull up in the 1-Wire line. Necessary for temperature conversion.
 */
bool ds2480EnablePullUp(void);


/*!
 * Disables strong pull up in the 1-Wire line.
 */
bool ds2480DisablePullUp(void);


/*!
 * The net variable is recover from Flash at start up, the coupler family number is used as
 * a magic number to check if there was loaded something coherent.
 * @return
 */
bool ds2480IsFlashWithData(void);



// External variable definitions

/*!
 * 1-Wire bus reception variables structure
 */
DS2480_VARS_EXT struct ds2480_state_info ds2480_state;


#endif
