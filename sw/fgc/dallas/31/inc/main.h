/*---------------------------------------------------------------------------------------------------------*\
  File:         main.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MAIN_H
#define MAIN_H

#ifdef MAIN_GLOBALS
    #define MAIN_VARS_EXT
#else
    #define MAIN_VARS_EXT extern
#endif


#include <m16c62_link.h>

#include <stdbool.h>
#include <stdint.h>


/*!
 * This struct is used for the communication between the MCU and the M16C62
 */
struct data_exchange
{
    uint8_t FAR *                         memory_ptr;             //!< Pointer to next response data character
    volatile uint16_t                     rsp_bytes_in_buffer;	  //!< Bytes in the reponse buffer
    union M16C62linkResponseArea          rsp;                    //!< Response buffer
    bool                             	  has_error;              //!< Error flag
    uint8_t                               branch_idx;             //!< Selected branch index
    bool                             	  flash_needs_refresh;
};


/*!
 * Represents the type of hardware
 */
struct t_hardware
{
    enum M16C62_hardware        model;			//!< FGC2 or FGC3 converter controller type
    enum OneWireHost			one_wire_host;	//!< type of 1-W host
    uint8_t                     n_branches;     //!< not used by the boot
    uint8_t NEAR *              mcu_link_tx;    //!< port used to transmit to MCU
    uint8_t NEAR *              mcu_link_rx;    //!< port used to receive from MCU
    uint16_t NEAR *             id_bus_tx;      //!< UART used for the ID bus
    union uartc_def NEAR *      id_bus_tx_ctrl; //!< UART control register
    uint8_t NEAR *              term_tx;        //!< UART used for the TERMINAL
    bool 	                    mcu_transmit_in_big_endian; //!< Whether MCU uses big or little endian for communication
};


/*!
 * Main program.
 * The ID program has two modes: STANDALONE and NORMAL.  The mode is determined by the byte
 * it reads from the MCU when it starts.  If it finds the MAGIC_IdBoot,
 * it will operate in NORMAL mode.  It returns the handshake byte (the NOT of the command
 * byte) and waits for new commands.
 * In STANDALONE, it runs the ID program monitor which provides the user with a simple menu
 * of options to help with debugging.  The monitor communicates through the serial port.
 *   The M16C62 boot program will wait for bytes from the MCU or may run standalone with a terminal.
  Commands from the MCU are received via an interrupt INT3 and port P0.  Response are sent back on port 2.
  Ports 1 and 3 are used for handshake lines.  Port 1 also has the Dallas coupler switch lines to allow
  the couplers to be identified.  Some MCU commands are too slow to be executed at interrupt level and
  are passed to the background task for execution.
 */
void    main    (void);

/*!
 * Represents a command from the MCU
 */
MAIN_VARS_EXT struct mcu_link_cmd       cmd;

/*!
 * Used between the MCU and M16C62
 */
MAIN_VARS_EXT struct data_exchange      data_exchange;

/*!
 *
 */
MAIN_VARS_EXT union address             address;



/*!
 * Represents the type of hardware, described in the struct t_hardware
 */
MAIN_VARS_EXT struct t_hardware          hardware;

/*!
 * @brief Array holding external Dallas ID passed from the MCU
 *
 * @see FastCmdSetExternalId(INT8U arg)
 * @see SlowCmdGetBarcodeForExtId(void)
 */
MAIN_VARS_EXT INT8U                     external_id[8];

#endif  // end MAIN_H

