/*---------------------------------------------------------------------------------------------------------*\
  File:         mcu_dependant.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MCU_DEPENDANT_H // header encapsulation
#define MCU_DEPENDANT_H

#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)


#define PLD_DATA_READY_SIGNAL   p3_0    // the transition from HIGH to LOW (falling edge) set the bit in the PLD register
#define PLD_CMD_ERROR_SIGNAL    p3_1    // the transition from HIGH to LOW (falling edge) set the bit in the PLD register
#define PLD_CMD_BUSY_SIGNAL     p1_7    // the transition from HIGH to LOW (falling edge) CLEAR the bit in the PLD register


#define USLEEP(usecs)           {INT16U t0=ta1,tn=(INT16U)(usecs);while((ta1-t0)<tn);}



/*
 Method #1:  Disable/Enable interrupts using simple instructions.  After critical section, interrupts
             will be enabled even if they were disabled before entering the critical section.

 Method #2:  Disable/Enable interrupts by preserving the state of interrupts.  In other words, if
             interrupts were disabled before entering the critical section, they will be disabled when
             leaving the critical section.
             Note: this also restores the other flags (apart from I) from the point they were pushed

 Method #3:  Disable/Enable interrupts by preserving the state of interrupts.  Generally speaking you
             would store the state of the interrupt disable flag in the local variable 'cpu_sr' and then
             disable interrupts.  'cpu_sr' is allocated in all of uC/OS-II's functions that need to
             disable interrupts.  You would restore the interrupt disable state by copying back 'cpu_sr'
             into the CPU's status register.
             Note: this also restores the other flags (apart from I) from the point they were saved

*/

// users choice
#define  OS_CRITICAL_METHOD    3


typedef INT16U                  OS_CPU_SR;      // Saved registers for level 3 OS_ENTER_CRITICAL

#if      OS_CRITICAL_METHOD == 1
#define  OS_ENTER_CRITICAL()    asm("fclr i")                     // Disable interrupts
#define  OS_EXIT_CRITICAL()     asm("fset i")                     // Enable  interrupts
#endif

#if      OS_CRITICAL_METHOD == 2
#define  OS_ENTER_CRITICAL()    asm("pushc flg"); asm("fclr i")   // Disable interrupts
#define  OS_EXIT_CRITICAL()     asm("popc flg")                   // Enable  interrupts
#endif

#if      OS_CRITICAL_METHOD == 3
#define  OS_ENTER_CRITICAL()    asm("stc flg, $@", cpu_sr);asm("fclr i")   // Disable interrupts
#define  OS_EXIT_CRITICAL()     asm("ldc $@, flg", cpu_sr)                 // Enable  interrupts
#endif

//-----------------------------------------------------------------------------------------------------------

#endif  // MCU_DEPENDANT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mcu_dependant.h
\*---------------------------------------------------------------------------------------------------------*/
