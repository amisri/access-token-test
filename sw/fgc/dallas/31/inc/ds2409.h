#ifndef DS2409_H
#define DS2409_H

#ifdef DS2409_GLOBALS
#define DS2409_EXTERN VARS
#else
#define DS2409_EXTERN_VARS exter
#endif

// Includes

#include <stdbool.h>
#include <stdint.h>

// Constants
// Types
// External structures, unions and enumerations
// External function declarations


/*!
 * This function will search the MicroLAN Network for the three DS2409 MicroLan couplers.
 * In the response buffer may be an error report or may be the ID numbers of the three
 * couplers in the order Local, Bus A, Bus B.
 * The three less significant bits of M16C62's P1 are used to determine which coupler is connected to
 * which 1-Wire line:
 *
 *           b2 b1 b0    coupler
 *        0   0  0  0    3 - error
 *        1   0  0  1    2 - Local
 *        2   0  1  0    0 - Bus A
 *        3   0  1  1    3 - error
 *        4   1  0  0    1 - Bus B
 *        5   1  0  1    3 - error
 *        6   1  1  0    3 - error
 *        7   1  1  1    3 - error
 *
 * Map between the logical path, the coupler and its branches
 *
 *     logical path Coupler  ReadBack Branch
 *         ----------------------------------
 *            0     0 Bus A   p1.1    Main
 *            1     0 Bus A   p1.1    Aux
 *            2     1 Bus B   p1.2    Main
 *            3     1 Bus B   p1.2    Aux
 *            4     2 Local   p1.0    Main
 *            5     2 Local   p1.0    Aux
 *            6       None            Direct       Link between DS2480 driver and DS2409 couplers
 *
 * @return true if no errors
 */
bool ds2409CheckIdentifyCouplers(void);


/*!
 * This function contains the specific actions for this HW needed to perform BEFORE the 1-Wire ID search.
 * It is called from onewire_master.
 */
bool ds2409FindIDsPre(void);


/*!
 * This function contains the specific actions for this HW needed to perform the 1-Wire ID search.
 * It is called from onewire_master.
 */
bool ds2409FindIDs(const uint8_t logical_path, uint16_t* logical_element);


/*!
 * This function will read all the temperature sensors connected to the 1-wire branches within the range
 * first to last.
 * This is done one branch at a time, starting with a broadcast to all sensors on a branch
 * to convert the temperature with 12-bit resolution (0.0125C steps).
 * Each sensor is then addressed in turn and the results of the conversion read out.
 * The results are stored in the id structure with units of 0.0125C.
 * @return true if no errors
*/
bool ds2409ReadTempsPre(void);


/*!
 * Does the specificities of converting temperature with the couplers setup, just before
 * calling the common convert temperature function. This function is called by the onewire_master
 * oneWireMasterReadTemperatures.
 */
bool ds2409ConvertTemp(const uint8_t logical_path);

// External variable definitions

#endif
