#ifndef MAX4638_H
#define MAX4638_H

#ifdef MAX4638_GLOBALS
#define MAX4638_EXTERN_VARS
#else
#define MAX4638_EXTERN_VARS extern
#endif


// Includes

#include <stdbool.h>
#include <stdint.h>

// Constants
// Types
// External structures, unions and enumerations
// External function declarations


/*!
 * This function contains the specific actions for this HW needed to perform the 1-Wire ID search.
 * It is called from onewire_master.
 */
bool max4638FindIDs(const uint8_t logical_path, uint16_t* logical_element);


/*!
 * Does the specificities of converting temperature with the MAX4638 setup, just before
 * calling the common convert temperature function. This function is called by the onewire_master
 * oneWireMasterReadTemperatures.
 */
bool max4638ConvertTemp(const uint8_t logical_path);


/*!
 * Does the specificities of reading temperature with the couplers setup, just before
 * calling the common convert temperature function.
 */
bool max4638ReadTempsPre(void);

// External variable definitions

#endif
