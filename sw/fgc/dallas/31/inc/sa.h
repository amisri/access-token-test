/*---------------------------------------------------------------------------------------------------------*\
  File:         sa.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef SA_H    // header encapsulation
#define SA_H

#ifdef SA_GLOBALS
    #define SA_VARS_EXT
#else
    #define SA_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs for Renesas NC30 compiler (M16C target)
#include <utilities.h>

//-----------------------------------------------------------------------------------------------------------

#define WAIT_BEFORE_MONITOR     20000        // Wait 20ms for MCU to be ready to read terminal chars

//-----------------------------------------------------------------------------------------------------------

/*!
 * Sets the sa_wait_tx_queue variable with the appropriate handler for the FGC2 platform
 */
void saSetWaitTxQueueFunctionFGC2(void);


/*!
 * Sets the sa_wait_tx_queue variable with the appropriate handler for the FGC3 platform
 */
void saSetWaitTxQueueFunctionFGC3(void);


void saDS2480TestSuite(void);


/*!
 * Sets the sa_get_char variable with the appropriate handler
 * @param Function pointer that will either point to saGetCharacterFGC2 or
 * saGetCharacterFGC3, depending whether the HW is a FGC2 or FGC3
 */
void saSetGetCharacterFunction(utilities_fgc_platform_handler_arg handler);

/*!
 * Gets a character from the keyboard using the UART2
 */
void saGetCharacterFGC2(void* ch);

/*!
 * Gets a character from the keyboard using the UART0
 */
void saGetCharacterFGC3(void* ch);


void    SaMonitor               (void);
void    SaShowBranch            (INT16U branch_idx);
void    SaShowFlash             (void);
void    SaShowPtDb              (void);
void    SaShowIdDb              (void);
void    SaShowIdDbRec           (INT16U level, INT16U idx);
void    SaTest                  (void);
INT16S  SaSendCh                (INT16S ch);



//-----------------------------------------------------------------------------------------------------------

#endif  // SA_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sa.h
\*---------------------------------------------------------------------------------------------------------*/

