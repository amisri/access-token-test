/*---------------------------------------------------------------------------------------------------------*\
  File:         db.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DB_H    // header encapsulation
#define DB_H

#ifdef DB_GLOBALS
    #define DB_VARS_EXT
#else
    #define DB_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>
#include <microlan_1wire.h>
#include <m16c62_link.h>

//-----------------------------------------------------------------------------------------------------------

#define ID_DB_TYPE_CODE_LEN     14      // Type code length in Part Type database

//-----------------------------------------------------------------------------------------------------------

struct dir_rec
{
    INT16U              first_idx;                      // Index of first record in table
    INT16U              last_idx;                       // Index of last record in table
    INT8U               tbl_byte;                       // Table byte
};

struct id_rec
{
    INT16U              part_idx;                       // Part index = serial_num + pt_base - serial_base
    INT16U              id12;                           // Dallas ID bytes 1 - 2 (least significant)
    INT8U               id3;                            // Dallas ID byte 3 (most significant)
};

struct pt_rec
{
    INT16U              pt_base;                        // Lowest index for part type
    INT32U              serial_base;                    // Base serial number
    INT8U               type_code[ID_DB_TYPE_CODE_LEN]; // Type code (e.g. "HCRFBMA___-GL\0")
};

//-----------------------------------------------------------------------------------------------------------

#define DB_DIR                  ((struct dir_rec FAR *)C62_BLK456_ADDR) // Database directory
#define DB_ID                   ((struct id_rec  FAR *)C62_BLK456_ADDR) // ID database
#define DB_PT                   ((struct pt_rec  FAR *)C62_BLK1_ADDR)   // Part type database

//-----------------------------------------------------------------------------------------------------------

void    DbLookup                (struct TMicroLAN_element_summary * dev);

//-----------------------------------------------------------------------------------------------------------

#endif  // DB_H end
