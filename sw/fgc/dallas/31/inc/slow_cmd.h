/*---------------------------------------------------------------------------------------------------------*\
  File:         slowCmd.h

  Purpose:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef SLOWCMD_H       // header encapsulation
#define SLOWCMD_H

#ifdef SLOWCMD_GLOBALS
    #define SLOWCMD_VARS_EXT
#else
    #define SLOWCMD_VARS_EXT extern
#endif


// #include <utilities.h>



/*!
 * This function is the background task.
 * It will wait indefinitely for commands to be passed from the interrupt handler IsrByteFromMcu().
 * This is used for all the M16C62 IProg commands that are too slow to run at interrupt level.
 * All slow (background) commands prepare a response buffer that can be read out by the MCU using the C62_GET_RESPONSE command.
 */
void slowCmdBgTsk(void);


/*!
 * Gets executed upon the reception of the M16C62A command C62_PROG_FLASH, 12
 * Need C62_SET_DATA to fill flash.buf[] and C62_SET_ADDRESS to fill flash.write_address previously
 * Response : errno, flsh_stat h/l, [exp_val, bad_val, bad_addr h/m/l]
 * This function will program the current sector of 256 bytes into flash at the specified address.
 * If the attempt to save the sector fails twice, it will search for and report the bad byte.
 * Block 3 many not be written because it holds the ID program itself.
 */
void slowCmdProgFlash(void);


/*!
 * Gets executed upon the reception of the M16C62A command C62_READ_IDS, 13
 * Response : errnum, [n_devs for each branch or error info]
 * This function will scan all the Dallas buses for components and will return the number of components
 * found on each bus.
 */
void slowCmdReadIds(void);

#endif
