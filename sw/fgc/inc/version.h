/*!
 *  @file      version.h
 *  @brief     Declarations for version information
 */

#ifndef FGC_VERSION
#define FGC_VERSION

// Includes

#include <stdint.h>
#include <definfo.h> 


#if FGC_PLATFORM_ID == 50
  #ifndef __TMS320C32__
    #define FAR far
  #else
    #define FAR
  #endif
#else
  #define FAR
#endif



// External structures, unions and enumerations

/*!
 * Struct containing version information
 */
struct version
{
    uint32_t     unixtime;      //!< Build timestamp
    const char * FAR unixtime_a;    //!< Build timestamp in ASCII
    const char * FAR hostname;      //!< Host name of machine used for the build
    const char * FAR architecture;  //!< Architecture of the machine used for the build
    const char * FAR cc_version;    //!< C compiler version
    const char * FAR user_group;    //!< User and group that performed the build
    const char * FAR directory;     //!< Build directory
};



// External variable definitions

// Declare version information
// (defined in version_def.txt)

extern struct version version;

#endif // FGC_VERSION

// EOF
