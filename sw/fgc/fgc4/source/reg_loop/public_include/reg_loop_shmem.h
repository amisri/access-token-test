#pragma once

#include "libref.h"
#include "libreg.h"
#include "libsig.h"

#include "logStructs.h"
#include "sigStructs.h"

#define SHMEM_BADDR             0x7A000000

#define CC_FILTER_BUF_LEN           3000                // Max length of FIR and Extrapolation buffers

#define TABLE_LEN    5000

struct RtCoreSelfProfile {
    // in units of CNTPCT (20 ns on DIOT, should be more explicit in code)

    uint32_t start;
    uint32_t adc;
    uint32_t cclibs;
    uint32_t dac;
    uint32_t after_cmd;
};

struct Equipdev_ppm_transactional
{
    struct REF_ramp_pars        ramp;                   //!< REF.RAMP.*
    struct REF_pulse_pars       pulse;                  //!< REF.PULSE.*
    struct REF_plep_pars        plep;                   //!< REF.PLEP.*
    struct REF_pppl_pars        pppl;                   //!< REF.PPPL.*
    struct REF_cubexp_pars      cubexp;                 //!< REF.CUBEXP.*
    struct REF_trim_pars        trim;                   //!< REF.TRIM.*
    struct REF_prbs_pars        prbs;                   //!< REF.PRBS.*

    struct REF_table_pars
    {
        struct FG_point             function[TABLE_LEN];   //!< REF.TABLE.FUNC.VALUE
        uintptr_t            num_elements;              //!< Number of elements in REF.TABLE.FUNC.VALUE
    } table;

    struct REF_test_pars        test;
};

#define CC_NUM_SUB_SELS             1                   // Max number of sub devices
#define CC_MAX_CYC_SEL              0                  // Max cycle selector
#define CC_NUM_CYC_SELS             1                  // CC_MAX_CYC_SEL + 1

struct ShmemTenant {
    struct POLSWITCH_mgr the_polswitch_mgr;

    struct REG_mgr reg_mgr;
    struct REF_mgr ref_mgr;
    struct SIG_struct sig_struct;

    struct LOG_mgr log_mgr;         // it seems that this only needs to be shared because LOG_Menus refers to it
    struct LOG_structs log_structs;

    int32_t i_meas_filter_buffer[CC_FILTER_BUF_LEN];
    int32_t b_meas_filter_buffer[CC_FILTER_BUF_LEN];

    // almost certainly this needs to be in shmem
    struct Equipdev_ppm_transactional transactional;

    struct FG_error fg_error[CC_NUM_CYC_SELS * CC_NUM_SUB_SELS];
    struct REF_armed ref_armed[CC_NUM_CYC_SELS * CC_NUM_SUB_SELS];

    // 2022-11 experiments stuff
    uint32_t pb1_gpio0_pir;
    uint32_t vs_cmd;
    uint32_t dac_data;
    double dac_value;
    int32_t adc0_raw[4];
    double adc0_data[4];
    int32_t adc0_zero, adc0_pos10v, adc0_neg10v;

    // big -- keep at the end

    struct LOG_buffers log_buffers;
};

// zeroed in Fgc4Shmem::initIpcDataStructures
struct Shmem {
    // TODO: improve naming of fields and group them by direction, same as in IPC block

    int cpu1_state;

    int iter_cpu1;      // iteration ACK
    struct CC_ns_time iter_time_cpu1[2];
    int iter_time_rdptr_cpu1;

    int missed_cycles;
    uint64_t missed_cycle_goal, missed_cycle_actual;

    int cpu0_cmd;
    int cpu0_cmd_seq;

    struct RtCoreSelfProfile rt_core_last_profile;
    uint64_t longest_iter_20ns;

    uint64_t tenant_enable_mask;
    struct ShmemTenant tenants[CC_NUM_SUB_SELS];
};

//#define SHMEM           (*(struct Shmem volatile*) SHMEM_BADDR)
#define SHMEM_VOL       (*(struct Shmem volatile*) SHMEM_BADDR)
#define SHMEM_NONVOL    (*(struct Shmem*) SHMEM_BADDR)

#define memory_read_reorder_barrier() __asm volatile ("dmb ishld" : : : "memory")
#define memory_write_reorder_barrier() __asm volatile ("dmb ishst" : : : "memory")

enum {
    CMD_START_REG,
    CMD_STOP_REG,

    CMD_SET_VS_CMD,
    CMD_SET_DAC0,
    CMD_SET_DAC1,
    CMD_SET_ADC_INPUT_EXT,
    CMD_SET_ADC_INPUT_DAC,
};

enum {
    CPU1_STATE_NOTREADY,
    CPU1_STATE_REG_STOPPED,
    CPU1_STATE_REG_RUNNING,
};

inline struct CC_ns_time cpu0_get_iter_time() {
//    struct CC_us_time ret;
//    memcpy(&ret, (const void*) &SHMEM_VOL.iter_time_cpu1[SHMEM_NONVOL.iter_time_rdptr_cpu1], sizeof(ret));
//    return ret;

    return SHMEM_NONVOL.iter_time_cpu1[SHMEM_NONVOL.iter_time_rdptr_cpu1];
}

inline void cpu1_write_iter_time(struct CC_ns_time iter_time) {
    int new_rdptr = (SHMEM_NONVOL.iter_time_rdptr_cpu1 + 1) % 2;
    SHMEM_NONVOL.iter_time_cpu1[new_rdptr] = iter_time;
    memory_write_reorder_barrier();
    SHMEM_NONVOL.iter_time_rdptr_cpu1 = new_rdptr;
}

inline void cpu0_send_cmd(int cmd) {
    SHMEM_NONVOL.cpu0_cmd = cmd;
    memory_write_reorder_barrier();
    SHMEM_NONVOL.cpu0_cmd_seq++;
}

inline void cpu0_await_cpu1_state(int state) {
    while (SHMEM_VOL.cpu1_state != state) {
        // busy wait
    }
}
