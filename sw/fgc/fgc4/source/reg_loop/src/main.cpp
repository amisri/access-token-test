#include <stdio.h>

#include "converter_interface.h"
#include "peripheral_board.h"
#include "reg_loop_shmem.h"

#include <bmboot/payload_runtime.hpp>

#include "logMenus.h"
#include "logStructs.h"

static float dummy_float = 42.0f;
static float sig_cpu_usage_us = -66.0f;
static uint32_t dummy_bitfield = 0x55555555;

#define DEVICE_INDEX 0

#include "logMenusInit.h"
#include "logStructsInit.h"

static AdcSerial adc = peripheral_board_0_adc0();
static DacSerial dac0 = peripheral_board_0_dac0();
static DacSerial dac1 = peripheral_board_0_dac1();
static InOut mux = peripheral_board_0_gpio0();
static InOut pb1_gpio0 = peripheral_board_1_gpio0();

static void ccInitLogging();
static void ccRunLogSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary, CC_ns_time iter_time);
static void ccRunLogContinuousSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary, CC_ns_time iter_time);

static void ADC_Test() {
    // Configure all ADC/DAC/MUX drivers to ANA200 parameters.
    //
    // Must be the first function called after init.

    int BUSY_SRC_EXT = 0;

    // ANA200 has four LTC2738 ADCs sharing common control lines
    puts("adc_set_config");
    adc.set_config(0, 1, 1, 0,
                   1, BUSY_SRC_EXT,
                   0, 0,
                   ADC_WIDTH);

    puts("adc_reset");
    adc.reset();

    // Two completely separate MAX5719 chips
    // They require to pulse LDAC after 1.5 us after SPI transfer, which
    // is about 50 SPI clock cycles of dead time
    dac0.set_config(0, 0, 1, 0, 4,
                   DAC_WIDTH, 50, 2);
    dac0.reset();

    dac1.set_config(0, 0, 1, 0, 4,
                    DAC_WIDTH, 50, 2);
    dac1.reset();

    // Simple, fixed GPIO for controlling SW/MUX settings
    mux.set_dir(0xFFF);

    for (int i = 1; i < 5; i++) {
        sw_set_src(mux, i, SW_SRC_MUX);
    }

    ana_mpxa_enable(mux, true);

    auto print_adcs = [](AdcReadout const& meas) {
        // only print the 4 that we care about (out of a total of MAX_DIN_PORTS)
        for (int i = 0; i < 4; i++) {
//            auto sign_bit = (1 << (ADC_WIDTH - 1));
//            auto sign_ext_mask = ~((1 << ADC_WIDTH) - 1);
//            auto sign_ext = meas[i] | ((meas[i] & sign_bit) ? sign_ext_mask : 0);
            printf("ADC[%d] = %08X ~ %+6d\n", i, meas[i], meas[i]);
        }
    };

    printf("\nGND measurement\n");
    ana_mpxa_set_chan(mux, CHAN_GND);
    adc.start();
    auto meas = adc.read_data();
    print_adcs(meas);

    printf("\nPositive 10V reference measurement\n");
    ana_mpxa_set_chan(mux, CHAN_REF_10VP);
    adc.start();
    meas = adc.read_data();
    print_adcs(meas);

    printf("\nNegative 10V reference measurement\n");
    ana_mpxa_set_chan(mux, CHAN_REF_10VN);
    adc.start();
    meas = adc.read_data();
    print_adcs(meas);

    printf("\nStarting loopback DAC->ADC test through ANA multiplexer\n");
    printf("Testing DAC1\n");
    ana_mpxa_set_chan(mux, CHAN_DAC1);

    for (int i = 0; i < 16; i++) {
        int setpoint = 0x11111 * i;
        dac0.write_data(setpoint);
        dac0.start();

        usleep(1000);

        adc.start();
        meas = adc.read_data();
        print_adcs(meas);
    }

    // Aim to set output to around 1 volt
    // TODO: why?
    dac0.write_data(0x22222);
    dac0.start();
}

AdcCalibResult AnalogCalibration::self_calibrate_adc() {
    // Simple, fixed GPIO for controlling SW/MUX settings
    mux.set_dir(0xFFF);

    for (int i = 1; i < 5; i++) {
        sw_set_src(mux, i, SW_SRC_MUX);
    }

    ana_mpxa_enable(mux, true);

    auto& adc = m_adc;

    ana_mpxa_set_chan(mux, CHAN_GND);
    adc.start();
    auto meas = adc.read_data();

    AdcCalibResult res;
    static_assert(meas.size() == res.size());

    for (size_t i = 0; i < meas.size(); i++) {
        res[i].zero = meas[i];
    }

    ana_mpxa_set_chan(mux, CHAN_REF_10VP);
    adc.start();
    meas = adc.read_data();

    for (int i = 0; i < meas.size(); i++) {
        res[i].pos10v = meas[i];
    }

    ana_mpxa_set_chan(mux, CHAN_REF_10VN);
    adc.start();
    meas = adc.read_data();

    for (int i = 0; i < meas.size(); i++) {
        res[i].neg10v = meas[i];
    }

    // FIXME: doesn't really belong here
    ana_mpxa_set_chan(mux, CHAN_DAC1);

    return res;
}

DacChannelCalibResult AnalogCalibration::calibrate_dac0(const AdcChannelCalibResult &adc_calib) {
    auto zero_code = 0x80000;
    auto low_code = zero_code / 4;
    auto high_code = zero_code / 4 * 7;

    auto one_iter = [&](uint32_t code) {
        m_dac.write_data(code);
        m_dac.start();
        m_dac.busy_wait_while_busy();
        usleep(1000);
        m_adc.start();
        auto meas = m_adc.read_data();
        auto volts = adc_to_volts(meas[0], adc_calib);
        printf("%08X -> %08X or %d mV\n", code, meas[0], (int)(volts * 1000));
        return volts;
    };

    auto val_zero = one_iter(zero_code);
    auto val_low = one_iter(low_code);
    auto val_high = one_iter(high_code);

    // TODO: some sanity checking?
    DacChannelCalibResult res;
    res.offset_v = val_zero;
    res.gain_p = (val_high - val_zero) / (high_code - zero_code);
    res.gain_n = (val_low - val_zero) / (low_code - zero_code);

    return res;
}

// TODO: precompute transfer function
double adc_to_volts(int32_t raw, AdcChannelCalibResult const& calib) {
    if (raw > calib.zero) {
        return (raw - calib.zero) * 10.0 / (calib.pos10v - calib.zero);
    }
    else {
        return (raw - calib.zero) * -10.0 / (calib.neg10v - calib.zero);
    }
}

uint32_t volts_to_dac(double volts, DacChannelCalibResult const& calib) {
    if (volts > calib.offset_v) {
        return 0x80000 + (volts - calib.offset_v) / calib.gain_p;
    }
    else {
        return 0x80000 + (volts - calib.offset_v) / calib.gain_n;
    }
}

// NOTE: should use CNTPCTSS_EL0 & ISB; see D10.2.1 in AArch64 ARM
static uint64_t read_CNTPCT() {
    uint64_t cntval;
    // asm statement MUST be volatile, otherwise compiler will do weird, wrong things like coalescing the access
    asm volatile("mrs %0, CNTPCT_EL0" : "=r" (cntval));
    return cntval;
}

int main()
{
    bmboot::notifyPayloadStarted();

    puts("");
    puts("Hello world from cpu1");

    auto& tenant = SHMEM_NONVOL.tenants[DEVICE_INDEX];

    ADC_Test();

    AnalogCalibration cal(adc, dac0);
    auto cal_adc = cal.self_calibrate_adc();
    tenant.adc0_zero = cal_adc[0].zero;
    tenant.adc0_pos10v = cal_adc[0].pos10v;
    tenant.adc0_neg10v = cal_adc[0].neg10v;

    auto cal_dac0 = cal.calibrate_dac0(cal_adc[0]);

    // Init liblog structures before anything else -- these don't depend on anything from CPU0
    // TODO: we might want to reset them between runs

    ccInitLogging();

    memory_write_reorder_barrier();

    SHMEM_NONVOL.cpu1_state = CPU1_STATE_REG_STOPPED;

    struct ControlLoopState {
        struct CC_ns_time iter_time;
        uint64_t last_cycle_start;          // in units of CNTPCT

        uint32_t iter_ns_time_mod_200Mns;
    };

    ControlLoopState st {};

    int last_cmd_seq = 0;

    pb1_gpio0.set_dir(PB1_GPIO0_VS_CMD_MASK | PB1_GPIO0_PRECHG);

    auto set_vs_cmd = [&](uint8_t cmd) {
        auto rest = (pb1_gpio0.get_output() & ~(PB1_GPIO0_VS_CMD_MASK | PB1_GPIO0_PRECHG));
        pb1_gpio0.set_output(rest | (cmd << PB1_GPIO0_VS_CMD_OFFSET));
    };

    auto set_dac_nowait = [&](DacSerial& dac, DacChannelCalibResult const& cal, double volts) {
        auto code = volts_to_dac(volts, cal);
        dac.busy_wait_while_busy();
        dac.write_data(code);
        dac.start();
    };

    set_vs_cmd(0x00);
    set_dac_nowait(dac0, cal_dac0, 0.0);

    for (int i = 1; i < 5; i++) {
        sw_set_src(mux, i, SW_SRC_IN);
    }

    uint64_t prof_floor = 0;
    RtCoreSelfProfile curr_profile;

    while (1)
    {
        if (SHMEM_VOL.cpu0_cmd_seq > last_cmd_seq) {
            // do not use a full command ringbuffer for this, as we want to be maximally lightweight

//            printf("process cmd seq=%d cmd=%d\n", SHMEM_VOL.cpu0_cmd_seq, SHMEM_NONVOL.cpu0_cmd);

            switch (SHMEM_NONVOL.cpu0_cmd) {
                case CMD_START_REG:
                    memset(&st, 0, sizeof(st));

                    cpu1_write_iter_time(st.iter_time);        // make sure to propagate cleared value before updating state

                    sig_cpu_usage_us = 0;
                    SHMEM_NONVOL.longest_iter_20ns = 0;

                    memory_write_reorder_barrier();
                    SHMEM_NONVOL.cpu1_state = CPU1_STATE_REG_RUNNING;
                    break;

                case CMD_STOP_REG:
                    SHMEM_NONVOL.cpu1_state = CPU1_STATE_REG_STOPPED;
                    break;

                case CMD_SET_VS_CMD:
                    set_vs_cmd(tenant.vs_cmd);
                    break;

                case CMD_SET_DAC0: {
                    set_dac_nowait(dac0, cal_dac0, tenant.dac_value);
                    break;
                }

                case CMD_SET_DAC1:
                    dac1.write_data(tenant.dac_data);
                    dac1.start();
                    break;

                case CMD_SET_ADC_INPUT_EXT:
                    for (int i = 1; i < 5; i++) {
                        sw_set_src(mux, i, SW_SRC_IN);
                    }
                    break;

                case CMD_SET_ADC_INPUT_DAC:
                    ana_mpxa_set_chan(mux, CHAN_DAC1);
                    for (int i = 1; i < 5; i++) {
                        sw_set_src(mux, i, SW_SRC_MUX);
                    }
                    break;
            }

            last_cmd_seq = SHMEM_NONVOL.cpu0_cmd_seq;
        }

        if (SHMEM_NONVOL.cpu1_state != CPU1_STATE_REG_RUNNING) {
            continue;
        }

//            memory_read_reorder_barrier();

        curr_profile.after_cmd = read_CNTPCT() - prof_floor;
        SHMEM_NONVOL.rt_core_last_profile = curr_profile;

        // did we miss the iteration start?
        constexpr int steps_per_iter = 50*50; // (50 MHz = 20 ns) -> 100 us
        auto cntval = read_CNTPCT();

        if (cntval >= st.last_cycle_start + steps_per_iter) {
            // don't worry about this for the first iteration after starting
            if (st.last_cycle_start > 0) {
                SHMEM_NONVOL.missed_cycles++;
                SHMEM_NONVOL.missed_cycle_goal = st.last_cycle_start + steps_per_iter;
                SHMEM_NONVOL.missed_cycle_actual = cntval;
            }
            st.last_cycle_start = cntval;
        }
        else {
            do  {
                cntval = read_CNTPCT();
            }
            while (cntval < st.last_cycle_start + steps_per_iter);
            st.last_cycle_start += steps_per_iter;
        }

        // This is where the iteration *actually* starts, but command processing comes first,
        // because in STOP state it is the only thing being done

        prof_floor = read_CNTPCT();
        curr_profile.start = 0;

        adc.start();
        auto meas = adc.read_data();
        for (int i = 0; i < 4; i++) {
            tenant.adc0_raw[i] = meas[i];
            tenant.adc0_data[i] = adc_to_volts(meas[i], cal_adc[i]);
        }

        curr_profile.adc = read_CNTPCT() - prof_floor;

        bool iter_1s_boundary = false;
        st.iter_time.ns += regMgrParValue(&tenant.reg_mgr, ITER_PERIOD_NS);

        if(st.iter_time.ns >= 1'000'000'000)
        {
            st.iter_time.secs.abs++;
            st.iter_time.ns    -= 1'000'000'000;
            iter_1s_boundary = true;
        }

        uint32_t  const iter_ns_time_mod_200Mns = st.iter_time.ns % 200000000;
        bool const iter_200ms_boundary = (iter_ns_time_mod_200Mns < st.iter_ns_time_mod_200Mns);
        st.iter_ns_time_mod_200Mns = iter_ns_time_mod_200Mns;

        sigMgrRT(&tenant.sig_struct.mgr, iter_200ms_boundary, st.iter_time);

        // Set measurements in libreg
        // TODO: why is this done 'by hand'? is this like that in classes as well?

        constexpr bool closed_loop = true;

        regMgrMeasSetBmeasRT   (&tenant.reg_mgr, sigVarValue(&tenant.sig_struct, transducer, b_probe , TRANSDUCER_MEAS_SIGNAL));

        if (closed_loop) {
            regMgrMeasSetImeasRT(&tenant.reg_mgr, CC_meas_signal { .signal = (float)tenant.adc0_data[0] * 1.25f, .is_valid = true });
        }
        else {
            regMgrMeasSetImeasRT(&tenant.reg_mgr, sigVarValue(&tenant.sig_struct, select, i_meas, TRANSDUCER_MEAS_SIGNAL));
        }

//        regMgrMeasSetImagSatRT (&reg_mgr, ccpars_sim.i_mag_sat);
        regMgrMeasSetIcapaRT   (&tenant.reg_mgr, sigVarValue(&tenant.sig_struct, transducer, i_probe , TRANSDUCER_MEAS_SIGNAL));

        if (closed_loop) {
            regMgrMeasSetVmeasRT(&tenant.reg_mgr,
                                 CC_meas_signal{.signal = (float) tenant.adc0_data[2] * 2.0f, .is_valid = true});
        }
        else {
            regMgrMeasSetVmeasRT   (&tenant.reg_mgr, sigVarValue(&tenant.sig_struct, transducer, v_probe , TRANSDUCER_MEAS_SIGNAL));
        }

        regMgrMeasSetVacRT     (&tenant.reg_mgr, sigVarValue(&tenant.sig_struct, transducer, v_ac    , TRANSDUCER_MEAS_SIGNAL));

        // Run CCLIBS real-time activity for this iteration

        refRtRegulationRT(&tenant.ref_mgr, st.iter_time);
        refRtStateRT(&tenant.ref_mgr);

#if USE_LIBLOG
        ccRunLogSignals(iter_1s_boundary, iter_200ms_boundary, st.iter_time);
#endif

        curr_profile.cclibs = read_CNTPCT() - prof_floor;

        if (closed_loop) {
            // Propagate V_REF to DAC
            set_dac_nowait(dac0, cal_dac0, *regMgrVarPointer(&tenant.reg_mgr, V_REF));
        }
        else {
            // Propagate I_MEAS_REG to DAC (= play function in open loop)
            set_dac_nowait(dac0, cal_dac0, *regMgrVarPointer(&tenant.reg_mgr, I_MEAS_REG));
        }

        curr_profile.dac = read_CNTPCT() - prof_floor;

        if (SHMEM_NONVOL.longest_iter_20ns < curr_profile.dac - curr_profile.start) {
            SHMEM_NONVOL.longest_iter_20ns = curr_profile.dac - curr_profile.start;
        }

        tenant.pb1_gpio0_pir = pb1_gpio0.get_input();

        cpu1_write_iter_time(st.iter_time);

        SHMEM_VOL.iter_cpu1++;
    }
}

static void ccInitLogging(void)
{
    auto& tenant = SHMEM_NONVOL.tenants[DEVICE_INDEX];

    // Initialize the liblog structures with a pattern to check that logStructsInit() correctly resets them to zero

    memset(&tenant.log_mgr,     0x55, sizeof(tenant.log_mgr));
    memset(&tenant.log_structs, 0xAA, sizeof(tenant.log_structs));
    memset(&tenant.log_buffers, 0xAA, sizeof(tenant.log_buffers));

    // Initialize liblog structures (log_mgr, log_structs and log_read) for this application

    logStructsInit(&tenant.log_mgr, &tenant.log_structs, &tenant.log_buffers);     // logStructsInit() is generated automatically from def/log.csv

    // Set number of samples per cycle in the discontinuous DIM logs

//    ccrun.dim.dc_num_samples = log_structs.log[LOG_DCDIM].num_samples_per_sig / 2;
//    ccrun.dim.dc_counter     = ccrun.dim.dc_num_samples;

    // Adjust log_mgr periods which are defined in the log.ods file assuming an iter_period of 1000

//    for(uint32_t log_index = 0 ; log_index < LOG_NUM_LOGS ; log_index++)
//    {
//        log_mgr.period_us[log_index] = (log_mgr.period_us[log_index] * timer.iter_period_us) / CC_ITER_PERIOD_US;
//    }

    // Adjust polarity switch periods - they are defined by the event log period in the converter CLOCK file

//    log_mgr.period_us[LOG_POLSWITCH]   = timer.iter_period_us * timer.event_log_period_iters;
//    log_mgr.period_us[LOG_POLSWIFLAGS] = log_mgr.period_us[LOG_POLSWITCH];
}

static void ccRunLogSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary, CC_ns_time iter_time)
{
    // Check if new cycle has started (iter time is >= start_cycle_time) - order of conditions matters!

//    if(ccrun.cycle_running == false              &&
//       ccsim.supercycle.active.event.cyc_sel > 0 &&
//       ccrun.iter_time_s >= ccsim.supercycle.active.event_time_s)
//    {
//        ccrun.cycle_running    = true;
//        ccrun.acq_disc_counter = -100;  // iterations till start of discontinuous logging
//
//        // Inform liblog about new cycle
//
//        logStoreStartContinuousRT(log_read.log, ccrun.iter_time_s, ccsim.supercycle.active.event.cyc_sel);
//    }

    // Log continuous and discontinuous logs

    ccRunLogContinuousSignals(iter_1s_boundary, iter_200ms_boundary, iter_time);
//    ccRunLogDiscontinuousSignals();
//    ccRunLogDimSignals();

    // Write PMD file if needed

//    if(logPostmortemLogsFroze(&log_mgr))
//    {
//        ccLogWakeThread(LOG_READ_GET_PM_BUF, NULL);
//    }
}

static void ccRunLogContinuousSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary, CC_ns_time iter_time)
{
    auto& tenant = SHMEM_NONVOL.tenants[DEVICE_INDEX];

    // log_debug_sig can be assigned to anything of interest to assist in debugging

//    log_debug_sig = (cc_float)ref_mgr.fsm.off.init_rst_timer;

    // Save these signals as floats for logging purposes
    // Scale function type to better match other debugging signals
    // Negate the values for improved readability when WAIT_CMDS signal is visible

//    ccrun.log.debug.reg_mode        =  (cc_float)regMgrVarGetValue(&reg_mgr,REG_MODE )          / 3.0 - 6.0;                      // 0 ... 3  ->  -6.0  ... -5.0
//    ccrun.log.debug.ref_state       =  (cc_float)refMgrVarGetValue(&ref_mgr,REF_STATE)          * 0.1;                            // 0 ... 14 ->   0.0  ...  1.4
//    ccrun.log.debug.cyc_sel         =  (cc_float)refMgrVarGetValue(&ref_mgr,EVENT_CYC_SEL)      * 0.1;                            // 0 ... 32 ->   0.0  ...  3.2
//    ccrun.log.debug.fg_type         = -(cc_float)refMgrVarGetValue(&ref_mgr,REF_FG_TYPE)        * 0.1;                            // 0 ... 13 ->   0.0  ... -1.3
//    ccrun.log.debug.fg_status       = -(cc_float)refMgrVarGetValue(&ref_mgr,REF_FG_STATUS)      / 4.0 - 2.0;                      // 0 ... 3  ->  -2.0  ... -2.75
//    ccrun.log.debug.ramp_mgr_status = -(cc_float)refMgrVarGetValue(&ref_mgr,REF_RAMP_MGR)       / 3.0 - 3.0;                      // 0 ... 2  ->  -3.0  ... -3.66
//    ccrun.log.debug.cycle_status    = -(cc_float)refMgrVarGetValue(&ref_mgr,REF_CYCLING_STATUS) / 3.0 - 4.0;                      // 0 ... 2  ->  -4.0  ... -4.66
//    ccrun.log.debug.dcct_user_sel   = -(cc_float)sigVarGetValue(&sig_struct, select, i_meas, SELECT_SELECTOR)        / 3.0 - 3.0; // 0 ... 2  ->  -3.0  ... -3.66
//    ccrun.log.debug.dcct_act_sel    = -(cc_float)sigVarGetValue(&sig_struct, select, i_meas, SELECT_ACTUAL_SELECTOR) / 3.0 - 4.0; // 0 ... 3  ->  -4.0  ... -5.00
//    ccrun.log.debug.iter_time_us    =  (cc_float)ccrun.iter_time.us * 1.0E-6;
//    ccrun.log.debug.iter_index_i    =  (cc_float)regMgrVarGetValue(&reg_mgr,IREG_ITER_INDEX);
//    ccrun.log.debug.iter_index_b    =  (cc_float)regMgrVarGetValue(&reg_mgr,BREG_ITER_INDEX);

    // Use mutex to log and reset the event type atomically

//    pthread_mutex_lock(&ccrun.log.event_type_mutex);

//    ccrun.log.debug.event_type = (cc_float)ccrun.log.event_type;
//    ccrun.log.event_type       = REF_EVENT_NONE;
//
//    pthread_mutex_unlock(&ccrun.log.event_type_mutex);

    struct LOG_log * const log = tenant.log_structs.log;

    // Store the following logs on every iteration

    logStoreContinuousRT(&log[LOG_ACQ],        iter_time);
//    logStoreContinuousRT(&log[LOG_DEBUG],      ccrun.iter_time_s);
    logStoreContinuousRT(&log[LOG_B_MEAS],     iter_time);
    logStoreContinuousRT(&log[LOG_I_MEAS],     iter_time);
    logStoreContinuousRT(&log[LOG_V_REF],      iter_time);
    logStoreContinuousRT(&log[LOG_V_REG],      iter_time);
//    logStoreContinuousRT(&log[LOG_FAULTS],     ccrun.iter_time_s);
//    logStoreContinuousRT(&log[LOG_WARNINGS],   ccrun.iter_time_s);
    logStoreContinuousRT(&log[LOG_FLAGS],      iter_time);
//    logStoreContinuousRT(&log[LOG_LOGRUNNING], ccrun.iter_time_s);

//    ccRunTestLogStateMachine(&log[LOG_ACQ]    );
//    ccRunTestLogStateMachine(&log[LOG_FLAGS]  );

    // Store ACQ_FLTR every 10th iteration

//    if((ccrun.iteration_counter % 10) == 0)
//    {
//        logStoreContinuousRT(&log[LOG_ACQ_FLTR], ccrun.iter_time_s);
//        ccRunTestLogStateMachine(&log[LOG_ACQ_FLTR]);
//    }

    // Store field regulation signals in log at field regulation rate

//    if(regMgrVarGetValue(&reg_mgr, BREG_ITER_INDEX) == 0)
//    {
//        // Log field regulation
//
//        logStoreContinuousRT(&log[LOG_B_REG], iter_time_s);
//
//        if(reg_mgr.reg_mode == REG_FIELD)
//        {
//            log_mgr.period_us[LOG_ILC_FLAGS] = log_mgr.period_us[LOG_B_REG];
//
//            logStoreContinuousRT(&log[LOG_ILC_FLAGS], iter_time_s);
//        }
//
//        ccRunTestLogStateMachine(&log[LOG_B_REG]);
//    }

    // Store current regulation signals in log at current regulation rate

    if(regMgrVarValue(&tenant.reg_mgr, IREG_ITER_INDEX) == 0)
    {
        // Log current regulation

        logStoreContinuousRT(&log[LOG_I_REG], iter_time);

        if(tenant.reg_mgr.reg_mode == REG_CURRENT)
        {
//            log_mgr.period_us[LOG_ILC_FLAGS] = log_mgr.period_us[LOG_I_REG];
//
//            logStoreContinuousRT(&log[LOG_ILC_FLAGS], iter_time_s);
        }

//        ccRunTestLogStateMachine(&log[LOG_I_REG]);
    }

    // Store ILC cyclic RMS signals when requested by refIlcStateToCycling

//    cc_double const ilc_log_time_stamp_s = refMgrVarGetValue(&ref_mgr, ILC_LOG_TIME_STAMP_S);
//
//    if(ilc_log_time_stamp_s != 0.0)
//    {
//        logStoreContinuousRT(&log[LOG_ILC_CYC], ilc_log_time_stamp_s);
//
//        refMgrVarGetValue(&ref_mgr, ILC_LOG_TIME_STAMP_S) = 0.0;
//    }

    // Store temperatures when the measurements get ready in the event log thread (~200ms intervals)
    // The time stamp should match the 200ms time stamp.

//    if(ccrun.log.temp_meas_ready)
//    {
//        logStoreContinuousRT(&log[LOG_TEMP], iter_200ms_timestamp_s);
//
//        ccRunTestLogStateMachine(&log[LOG_TEMP]);
//
//        ccrun.log.temp_meas_ready = false;
//
//        // Store the limits in the limits log at 5Hz
//
//        logStoreContinuousRT(&log[LOG_LIMITS], ccrun.iter_200ms_timestamp_s);
//
//        ccRunTestLogStateMachine(&log[LOG_LIMITS]);
//
//        // Store the V_AC frequency measurement at 5Hz
//
//        logStoreContinuousRT(&log[LOG_V_AC_HZ], iter_200ms_timestamp_s);
//
//        ccRunTestLogStateMachine(&log[LOG_V_AC_HZ]);
//    }

}
