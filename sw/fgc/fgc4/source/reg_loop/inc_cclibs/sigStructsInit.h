//! @file  inc_cclibs/sigStructsInit.h
//!
//! @brief Converter Control Signals library : Structures initialization header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Default values to use if pointers are NULL

// Initialize signals structures per device 
//
// @param[in]    sig                     Pointer to signals structure to initialize
// @param[in]    polswitch_mgr           Pointer to polarity switch structure
// @param[in]    device_index            Device index allow initialization of multiple devices (NULL if not required)

static void sigStructsInitDevice(struct SIG_struct    * const sig,
                                 struct POLSWITCH_mgr * const polswitch_mgr,
                                 uint32_t               const device_index)
{
    // Reset sig structure

    memset(sig, 0, sizeof(struct SIG_struct));

    // Initialize sig_mgr structure

    sig->mgr.cal_limits            = &sig->cal_limit.array[0];
    sig->mgr.temp_filters          = &sig->temp_filter.array[0];
    sig->mgr.cal_refs              = &sig->cal_ref.array[0];
    sig->mgr.adcs                  = &sig->adc.array[0];
    sig->mgr.transducers           = &sig->transducer.array[0];
    sig->mgr.selects               = &sig->select.array[0];
    sig->mgr.num_temp_filters      = 3;
    sig->mgr.num_cal_refs          = 2;
    sig->mgr.num_cal_limits        = 0;
    sig->mgr.num_adcs              = 4;
    sig->mgr.num_transducers       = 6;
    sig->mgr.num_selects           = 1;
    sig->mgr.polswitch_mgr         = polswitch_mgr;


    // Initialize temperature filters

    uint32_t i;

    for(i = 0 ; i < 3 ; i++)
    {
        sigTempInit(&sig->mgr.temp_filters[i]);
    }

    // Initialize ADC calibration pointers

    for(i = 0 ; i < 4 ; i++)
    {
        sigCalInit(&sig->mgr.adcs[i].cal);
    }

    // Initialize transducer calibration pointers

    for(i = 0 ; i < 6 ; i++)
    {
        sigCalInit(&sig->mgr.transducers[i].cal);
    }

    // Initialize sig structure elements

    sig->cal_limit.named.adc_a.nominal_offset_ppm = 10;
    sig->cal_limit.named.adc_a.offset_warning_ppm = 100;
    sig->cal_limit.named.adc_a.offset_fault_ppm = 1000;
    sig->cal_limit.named.adc_a.gain_err_warning_ppm = 200;
    sig->cal_limit.named.adc_a.gain_err_fault_ppm = 2000;
    sig->cal_limit.named.adc_b.nominal_offset_ppm = -20;
    sig->cal_limit.named.adc_b.offset_warning_ppm = 105;
    sig->cal_limit.named.adc_b.offset_fault_ppm = 1100;
    sig->cal_limit.named.adc_b.gain_err_warning_ppm = 210;
    sig->cal_limit.named.adc_b.gain_err_fault_ppm = 2100;
    sig->cal_limit.named.adc_c.nominal_offset_ppm = 30;
    sig->cal_limit.named.adc_c.offset_warning_ppm = 110;
    sig->cal_limit.named.adc_c.offset_fault_ppm = 1200;
    sig->cal_limit.named.adc_c.gain_err_warning_ppm = 220;
    sig->cal_limit.named.adc_c.gain_err_fault_ppm = 2200;
    sig->cal_limit.named.adc_d.nominal_offset_ppm = -40;
    sig->cal_limit.named.adc_d.offset_warning_ppm = 115;
    sig->cal_limit.named.adc_d.offset_fault_ppm = 1300;
    sig->cal_limit.named.adc_d.gain_err_warning_ppm = 230;
    sig->cal_limit.named.adc_d.gain_err_fault_ppm = 2300;
    sig->cal_limit.named.dcct_a.nominal_offset_ppm = 15;
    sig->cal_limit.named.dcct_a.offset_warning_ppm = 200;
    sig->cal_limit.named.dcct_a.offset_fault_ppm = 1400;
    sig->cal_limit.named.dcct_a.gain_err_warning_ppm = 205;
    sig->cal_limit.named.dcct_a.gain_err_fault_ppm = 2005;
    sig->cal_limit.named.dcct_b.nominal_offset_ppm = -25;
    sig->cal_limit.named.dcct_b.offset_warning_ppm = 205;
    sig->cal_limit.named.dcct_b.offset_fault_ppm = 1500;
    sig->cal_limit.named.dcct_b.gain_err_warning_ppm = 215;
    sig->cal_limit.named.dcct_b.gain_err_fault_ppm = 2105;
    sig->cal_limit.named.v_probe.nominal_offset_ppm = 35;
    sig->cal_limit.named.v_probe.offset_warning_ppm = 210;
    sig->cal_limit.named.v_probe.offset_fault_ppm = 1600;
    sig->cal_limit.named.v_probe.gain_err_warning_ppm = 245;
    sig->cal_limit.named.v_probe.gain_err_fault_ppm = 2405;
    sig->cal_limit.named.b_probe.nominal_offset_ppm = -45;
    sig->cal_limit.named.b_probe.offset_warning_ppm = 215;
    sig->cal_limit.named.b_probe.offset_fault_ppm = 1700;
    sig->cal_limit.named.b_probe.gain_err_warning_ppm = 225;
    sig->cal_limit.named.b_probe.gain_err_fault_ppm = 2205;
    sig->cal_limit.named.i_probe.nominal_offset_ppm = 55;
    sig->cal_limit.named.i_probe.offset_warning_ppm = 220;
    sig->cal_limit.named.i_probe.offset_fault_ppm = 1800;
    sig->cal_limit.named.i_probe.gain_err_warning_ppm = 255;
    sig->cal_limit.named.i_probe.gain_err_fault_ppm = 2505;
    sig->temp_filter.named.internal.filter_period_s = 0.2;
    sig->temp_filter.named.internal.time_constant_s = 1;
    sig->temp_filter.named.internal.warning.min_c = 15;
    sig->temp_filter.named.internal.warning.max_c = 35;
    sig->temp_filter.named.internal.invalid.min_c = 5;
    sig->temp_filter.named.internal.invalid.max_c = 60;
    sig->temp_filter.named.internal.max_delta_temp_c = 1;
    sig->temp_filter.named.dcct_a.filter_period_s = 0.2;
    sig->temp_filter.named.dcct_a.time_constant_s = 1.5;
    sig->temp_filter.named.dcct_a.warning.min_c = 16;
    sig->temp_filter.named.dcct_a.warning.max_c = 36;
    sig->temp_filter.named.dcct_a.invalid.min_c = 6;
    sig->temp_filter.named.dcct_a.invalid.max_c = 61;
    sig->temp_filter.named.dcct_a.max_delta_temp_c = 2;
    sig->temp_filter.named.dcct_b.filter_period_s = 0.2;
    sig->temp_filter.named.dcct_b.time_constant_s = 2;
    sig->temp_filter.named.dcct_b.warning.min_c = 17;
    sig->temp_filter.named.dcct_b.warning.max_c = 37;
    sig->temp_filter.named.dcct_b.invalid.min_c = 7;
    sig->temp_filter.named.dcct_b.invalid.max_c = 62;
    sig->temp_filter.named.dcct_b.max_delta_temp_c = 3;
    sig->cal_ref.named.internal.temp_c = NULL;
    sig->cal_ref.named.external.temp_c = NULL;
    sig->adc.named.adc_a.nominal_gain = 10000000;
    sig->adc.named.adc_a.cal.cal_ref_index = 0;
    sig->adc.named.adc_a.cal.temp_filter_index = 0;
    sig->adc.named.adc_a.cal.cal_limit_index = 0;
    sig->adc.named.adc_b.nominal_gain = 10001000;
    sig->adc.named.adc_b.cal.cal_ref_index = 0;
    sig->adc.named.adc_b.cal.temp_filter_index = 0;
    sig->adc.named.adc_b.cal.cal_limit_index = 1;
    sig->adc.named.adc_c.nominal_gain = 10002000;
    sig->adc.named.adc_c.cal.cal_ref_index = 0;
    sig->adc.named.adc_c.cal.temp_filter_index = 0;
    sig->adc.named.adc_c.cal.cal_limit_index = 2;
    sig->adc.named.adc_d.nominal_gain = 10003000;
    sig->adc.named.adc_d.cal.cal_ref_index = 0;
    sig->adc.named.adc_d.cal.temp_filter_index = 0;
    sig->adc.named.adc_d.cal.cal_limit_index = 3;
    sig->transducer.named.dcct_a.position = SIG_TRANSDUCER_POSITION_CONVERTER;
    sig->transducer.named.dcct_a.nominal_gain = 10;
    sig->transducer.named.dcct_a.cal.cal_ref_index = 1;
    sig->transducer.named.dcct_a.cal.temp_filter_index = 1;
    sig->transducer.named.dcct_a.cal.cal_limit_index = 4;
    sig->transducer.named.dcct_b.position = SIG_TRANSDUCER_POSITION_LOAD;
    sig->transducer.named.dcct_b.nominal_gain = 10;
    sig->transducer.named.dcct_b.cal.cal_ref_index = 1;
    sig->transducer.named.dcct_b.cal.temp_filter_index = 2;
    sig->transducer.named.dcct_b.cal.cal_limit_index = 5;
    sig->transducer.named.v_probe.position = SIG_TRANSDUCER_POSITION_LOAD;
    sig->transducer.named.v_probe.nominal_gain = 10;
    sig->transducer.named.v_probe.cal.cal_ref_index = SIG_NOT_IN_USE;
    sig->transducer.named.v_probe.cal.temp_filter_index = SIG_NOT_IN_USE;
    sig->transducer.named.v_probe.cal.cal_limit_index = 6;
    sig->transducer.named.b_probe.position = SIG_TRANSDUCER_POSITION_LOAD;
    sig->transducer.named.b_probe.nominal_gain = 1;
    sig->transducer.named.b_probe.cal.cal_ref_index = SIG_NOT_IN_USE;
    sig->transducer.named.b_probe.cal.temp_filter_index = SIG_NOT_IN_USE;
    sig->transducer.named.b_probe.cal.cal_limit_index = 7;
    sig->transducer.named.i_probe.position = SIG_TRANSDUCER_POSITION_LOAD;
    sig->transducer.named.i_probe.nominal_gain = 1;
    sig->transducer.named.i_probe.cal.cal_ref_index = SIG_NOT_IN_USE;
    sig->transducer.named.i_probe.cal.temp_filter_index = SIG_NOT_IN_USE;
    sig->transducer.named.i_probe.cal.cal_limit_index = 8;
    sig->transducer.named.v_ac.position = SIG_TRANSDUCER_POSITION_NORMAL;
    sig->transducer.named.v_ac.nominal_gain = 1;
    sig->transducer.named.v_ac.cal.cal_ref_index = SIG_NOT_IN_USE;
    sig->transducer.named.v_ac.cal.temp_filter_index = SIG_NOT_IN_USE;
    sig->transducer.named.v_ac.cal.cal_limit_index = SIG_NOT_IN_USE;
    sig->select.named.i_meas.selector = SIG_AB;
    sig->select.named.i_meas.transducer_a_index = 0;
    sig->select.named.i_meas.transducer_b_index = 1;
    sig->select.named.i_meas.diff_warn_limit = 0.5;
    sig->select.named.i_meas.diff_fault_warn_ratio = 3;
}

// Initialize signals structure
//
// Wrapper around sigStructsInitDevice() to initialize applications with one device
//
// @param[in]    sig                     Pointer to signals structure to initialize
// @param[in]    polswitch_mgr           Pointer to polarity switch structure

static inline void sigStructsInit(struct SIG_struct    * const sig,
                                  struct POLSWITCH_mgr * const polswitch_mgr)
{
    sigStructsInitDevice(sig, polswitch_mgr, 0);
}

// EOF
