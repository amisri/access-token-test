//! @file  inc_cclibs/logMenusInit.h
//!
//! @brief Converter Control Logging library : Menu structures initialization header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! Initialize log structures per device
//!
//! @param[in]      log_mgr           Pointer to log manager structures
//! @param[in,out]  log_menus         Pointer to log menus structures to initialize
//! @param[in]      max_buffer_size   Max readout buffer size (header+data). Zero for no limit.

static inline void logMenusInit(struct LOG_mgr * const log_mgr, struct LOG_menus * const log_menus, uint32_t const max_buffer_size)
{
    log_menus->log_mgr         = log_mgr;
    log_menus->max_buffer_size = max_buffer_size;

    log_menus->names              [LOG_MENU_ACQ]                      = "ACQ";
    log_menus->properties         [LOG_MENU_ACQ]                      = "LOG.SPY.DATA[0]";
    log_menus->log_index          [LOG_MENU_ACQ]                      = LOG_ACQ;
    log_menus->sig_sel_bit_mask   [LOG_MENU_ACQ]                      = 0x00000003;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_ACQ]                      = 0x00000003;
    log_menus->status_bit_mask    [LOG_MENU_ACQ]                      = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_PM_BUF_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_ACQ]                      = 1;

    log_menus->names              [LOG_MENU_ACQ_MPX]                  = "ACQ_MPX";
    log_menus->properties         [LOG_MENU_ACQ_MPX]                  = "LOG.SPY.DATA[1]";
    log_menus->log_index          [LOG_MENU_ACQ_MPX]                  = LOG_ACQ;
    log_menus->sig_sel_bit_mask   [LOG_MENU_ACQ_MPX]                  = 0x0000000C;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_ACQ_MPX]                  = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_ACQ_MPX]                  = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_MPX_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_ACQ_MPX]                  = 0;

    log_menus->names              [LOG_MENU_B_MEAS]                   = "B_MEAS";
    log_menus->properties         [LOG_MENU_B_MEAS]                   = "LOG.SPY.DATA[2]";
    log_menus->log_index          [LOG_MENU_B_MEAS]                   = LOG_B_MEAS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_B_MEAS]                   = 0x00000007;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_B_MEAS]                   = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_B_MEAS]                   = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_B_MEAS]                   = 1;

    log_menus->names              [LOG_MENU_B_MEAS_MPX]               = "B_MEAS_MPX";
    log_menus->properties         [LOG_MENU_B_MEAS_MPX]               = "LOG.SPY.DATA[3]";
    log_menus->log_index          [LOG_MENU_B_MEAS_MPX]               = LOG_B_MEAS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_B_MEAS_MPX]               = 0x00000078;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_B_MEAS_MPX]               = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_B_MEAS_MPX]               = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_MPX_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_B_MEAS_MPX]               = 0;

    log_menus->names              [LOG_MENU_I_MEAS]                   = "I_MEAS";
    log_menus->properties         [LOG_MENU_I_MEAS]                   = "LOG.SPY.DATA[4]";
    log_menus->log_index          [LOG_MENU_I_MEAS]                   = LOG_I_MEAS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_I_MEAS]                   = 0x00000007;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_I_MEAS]                   = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_I_MEAS]                   = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_I_MEAS]                   = 1;

    log_menus->names              [LOG_MENU_I_MEAS_MPX]               = "I_MEAS_MPX";
    log_menus->properties         [LOG_MENU_I_MEAS_MPX]               = "LOG.SPY.DATA[5]";
    log_menus->log_index          [LOG_MENU_I_MEAS_MPX]               = LOG_I_MEAS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_I_MEAS_MPX]               = 0x00000078;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_I_MEAS_MPX]               = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_I_MEAS_MPX]               = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_MPX_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_I_MEAS_MPX]               = 0;

    log_menus->names              [LOG_MENU_B_REG]                    = "B_REG";
    log_menus->properties         [LOG_MENU_B_REG]                    = "LOG.SPY.DATA[6]";
    log_menus->log_index          [LOG_MENU_B_REG]                    = LOG_B_REG;
    log_menus->sig_sel_bit_mask   [LOG_MENU_B_REG]                    = 0x000000FF;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_B_REG]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_B_REG]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_B_REG]                    = 1;

    log_menus->names              [LOG_MENU_B_REG_MPX]                = "B_REG_MPX";
    log_menus->properties         [LOG_MENU_B_REG_MPX]                = "LOG.SPY.DATA[7]";
    log_menus->log_index          [LOG_MENU_B_REG_MPX]                = LOG_B_REG;
    log_menus->sig_sel_bit_mask   [LOG_MENU_B_REG_MPX]                = 0x00000F00;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_B_REG_MPX]                = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_B_REG_MPX]                = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_MPX_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_B_REG_MPX]                = 0;

    log_menus->names              [LOG_MENU_I_REG]                    = "I_REG";
    log_menus->properties         [LOG_MENU_I_REG]                    = "LOG.SPY.DATA[8]";
    log_menus->log_index          [LOG_MENU_I_REG]                    = LOG_I_REG;
    log_menus->sig_sel_bit_mask   [LOG_MENU_I_REG]                    = 0x000000FF;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_I_REG]                    = 0x000000C9;
    log_menus->status_bit_mask    [LOG_MENU_I_REG]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_PM_BUF_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_I_REG]                    = 1;

    log_menus->names              [LOG_MENU_I_REG_MPX]                = "I_REG_MPX";
    log_menus->properties         [LOG_MENU_I_REG_MPX]                = "LOG.SPY.DATA[9]";
    log_menus->log_index          [LOG_MENU_I_REG_MPX]                = LOG_I_REG;
    log_menus->sig_sel_bit_mask   [LOG_MENU_I_REG_MPX]                = 0x00000F00;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_I_REG_MPX]                = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_I_REG_MPX]                = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_MPX_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_I_REG_MPX]                = 0;

    log_menus->names              [LOG_MENU_V_REF]                    = "V_REF";
    log_menus->properties         [LOG_MENU_V_REF]                    = "LOG.SPY.DATA[10]";
    log_menus->log_index          [LOG_MENU_V_REF]                    = LOG_V_REF;
    log_menus->sig_sel_bit_mask   [LOG_MENU_V_REF]                    = 0x0000001F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_V_REF]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_V_REF]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_V_REF]                    = 1;

    log_menus->names              [LOG_MENU_V_REF_MPX]                = "V_REF_MPX";
    log_menus->properties         [LOG_MENU_V_REF_MPX]                = "LOG.SPY.DATA[11]";
    log_menus->log_index          [LOG_MENU_V_REF_MPX]                = LOG_V_REF;
    log_menus->sig_sel_bit_mask   [LOG_MENU_V_REF_MPX]                = 0x00000060;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_V_REF_MPX]                = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_V_REF_MPX]                = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_MPX_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_V_REF_MPX]                = 0;

    log_menus->names              [LOG_MENU_V_REG]                    = "V_REG";
    log_menus->properties         [LOG_MENU_V_REG]                    = "LOG.SPY.DATA[12]";
    log_menus->log_index          [LOG_MENU_V_REG]                    = LOG_V_REG;
    log_menus->sig_sel_bit_mask   [LOG_MENU_V_REG]                    = 0x00000007;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_V_REG]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_V_REG]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_V_REG]                    = 1;

    log_menus->names              [LOG_MENU_V_REG_MPX]                = "V_REG_MPX";
    log_menus->properties         [LOG_MENU_V_REG_MPX]                = "LOG.SPY.DATA[13]";
    log_menus->log_index          [LOG_MENU_V_REG_MPX]                = LOG_V_REG;
    log_menus->sig_sel_bit_mask   [LOG_MENU_V_REG_MPX]                = 0x00000038;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_V_REG_MPX]                = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_V_REG_MPX]                = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_MPX_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_V_REG_MPX]                = 0;

    log_menus->names              [LOG_MENU_I_1KHZ]                   = "I_1KHZ";
    log_menus->properties         [LOG_MENU_I_1KHZ]                   = "LOG.SPY.DATA[14]";
    log_menus->log_index          [LOG_MENU_I_1KHZ]                   = LOG_I_1KHZ;
    log_menus->sig_sel_bit_mask   [LOG_MENU_I_1KHZ]                   = 0x00000003;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_I_1KHZ]                   = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_I_1KHZ]                   = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_I_1KHZ]                   = 0;

    log_menus->names              [LOG_MENU_I_EARTH]                  = "I_EARTH";
    log_menus->properties         [LOG_MENU_I_EARTH]                  = "LOG.SPY.DATA[15]";
    log_menus->log_index          [LOG_MENU_I_EARTH]                  = LOG_I_EARTH;
    log_menus->sig_sel_bit_mask   [LOG_MENU_I_EARTH]                  = 0x00000001;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_I_EARTH]                  = 0x00000001;
    log_menus->status_bit_mask    [LOG_MENU_I_EARTH]                  = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_PM_BUF_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_I_EARTH]                  = 1;

    log_menus->names              [LOG_MENU_TEMP]                     = "TEMP";
    log_menus->properties         [LOG_MENU_TEMP]                     = "LOG.SPY.DATA[16]";
    log_menus->log_index          [LOG_MENU_TEMP]                     = LOG_TEMP;
    log_menus->sig_sel_bit_mask   [LOG_MENU_TEMP]                     = 0x0000001F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_TEMP]                     = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_TEMP]                     = 0 | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_TEMP]                     = 1;

    log_menus->names              [LOG_MENU_V_AC_HZ]                  = "V_AC_HZ";
    log_menus->properties         [LOG_MENU_V_AC_HZ]                  = "LOG.SPY.DATA[17]";
    log_menus->log_index          [LOG_MENU_V_AC_HZ]                  = LOG_TEMP;
    log_menus->sig_sel_bit_mask   [LOG_MENU_V_AC_HZ]                  = 0x00000020;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_V_AC_HZ]                  = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_V_AC_HZ]                  = 0 | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_V_AC_HZ]                  = 0;

    log_menus->names              [LOG_MENU_FLAGS]                    = "FLAGS";
    log_menus->properties         [LOG_MENU_FLAGS]                    = "LOG.SPY.DATA[18]";
    log_menus->log_index          [LOG_MENU_FLAGS]                    = LOG_FLAGS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_FLAGS]                    = 0x03FFFFFF;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_FLAGS]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_FLAGS]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_FLAGS]                    = 1;

    log_menus->names              [LOG_MENU_BIS_CH1_OK]               = "BIS_CH1_OK";
    log_menus->properties         [LOG_MENU_BIS_CH1_OK]               = "LOG.SPY.DATA[19]";
    log_menus->log_index          [LOG_MENU_BIS_CH1_OK]               = LOG_BIS_FLAGS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_CH1_OK]               = 0x00000080;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_CH1_OK]               = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_CH1_OK]               = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_CH1_OK]               = 0;

    log_menus->names              [LOG_MENU_BIS_CH2_OK]               = "BIS_CH2_OK";
    log_menus->properties         [LOG_MENU_BIS_CH2_OK]               = "LOG.SPY.DATA[20]";
    log_menus->log_index          [LOG_MENU_BIS_CH2_OK]               = LOG_BIS_FLAGS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_CH2_OK]               = 0x00008000;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_CH2_OK]               = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_CH2_OK]               = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_CH2_OK]               = 0;

    log_menus->names              [LOG_MENU_BIS_CH3_OK]               = "BIS_CH3_OK";
    log_menus->properties         [LOG_MENU_BIS_CH3_OK]               = "LOG.SPY.DATA[21]";
    log_menus->log_index          [LOG_MENU_BIS_CH3_OK]               = LOG_BIS_FLAGS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_CH3_OK]               = 0x00800000;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_CH3_OK]               = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_CH3_OK]               = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_CH3_OK]               = 0;

    log_menus->names              [LOG_MENU_BIS_CH4_OK]               = "BIS_CH4_OK";
    log_menus->properties         [LOG_MENU_BIS_CH4_OK]               = "LOG.SPY.DATA[22]";
    log_menus->log_index          [LOG_MENU_BIS_CH4_OK]               = LOG_BIS_FLAGS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_CH4_OK]               = 0x80000000;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_CH4_OK]               = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_CH4_OK]               = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_CH4_OK]               = 0;

    log_menus->names              [LOG_MENU_BIS_CH1_TESTS]            = "BIS_CH1_TESTS";
    log_menus->properties         [LOG_MENU_BIS_CH1_TESTS]            = "LOG.SPY.DATA[23]";
    log_menus->log_index          [LOG_MENU_BIS_CH1_TESTS]            = LOG_BIS_FLAGS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_CH1_TESTS]            = 0x0000007F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_CH1_TESTS]            = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_CH1_TESTS]            = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_CH1_TESTS]            = 0;

    log_menus->names              [LOG_MENU_BIS_CH2_TESTS]            = "BIS_CH2_TESTS";
    log_menus->properties         [LOG_MENU_BIS_CH2_TESTS]            = "LOG.SPY.DATA[24]";
    log_menus->log_index          [LOG_MENU_BIS_CH2_TESTS]            = LOG_BIS_FLAGS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_CH2_TESTS]            = 0x00007F00;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_CH2_TESTS]            = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_CH2_TESTS]            = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_CH2_TESTS]            = 0;

    log_menus->names              [LOG_MENU_BIS_CH3_TESTS]            = "BIS_CH3_TESTS";
    log_menus->properties         [LOG_MENU_BIS_CH3_TESTS]            = "LOG.SPY.DATA[25]";
    log_menus->log_index          [LOG_MENU_BIS_CH3_TESTS]            = LOG_BIS_FLAGS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_CH3_TESTS]            = 0x007F0000;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_CH3_TESTS]            = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_CH3_TESTS]            = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_CH3_TESTS]            = 0;

    log_menus->names              [LOG_MENU_BIS_CH4_TESTS]            = "BIS_CH4_TESTS";
    log_menus->properties         [LOG_MENU_BIS_CH4_TESTS]            = "LOG.SPY.DATA[26]";
    log_menus->log_index          [LOG_MENU_BIS_CH4_TESTS]            = LOG_BIS_FLAGS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_CH4_TESTS]            = 0x7F000000;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_CH4_TESTS]            = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_CH4_TESTS]            = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_CH4_TESTS]            = 0;

    log_menus->names              [LOG_MENU_BIS_LIMITS_CH1]           = "BIS_LIMITS_CH1";
    log_menus->properties         [LOG_MENU_BIS_LIMITS_CH1]           = "LOG.SPY.DATA[27]";
    log_menus->log_index          [LOG_MENU_BIS_LIMITS_CH1]           = LOG_BIS_LIMITS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_LIMITS_CH1]           = 0x0000007F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_LIMITS_CH1]           = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_LIMITS_CH1]           = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_LIMITS_CH1]           = 0;

    log_menus->names              [LOG_MENU_BIS_LIMITS_CH2]           = "BIS_LIMITS_CH2";
    log_menus->properties         [LOG_MENU_BIS_LIMITS_CH2]           = "LOG.SPY.DATA[28]";
    log_menus->log_index          [LOG_MENU_BIS_LIMITS_CH2]           = LOG_BIS_LIMITS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_LIMITS_CH2]           = 0x00003F80;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_LIMITS_CH2]           = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_LIMITS_CH2]           = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_LIMITS_CH2]           = 0;

    log_menus->names              [LOG_MENU_BIS_LIMITS_CH3]           = "BIS_LIMITS_CH3";
    log_menus->properties         [LOG_MENU_BIS_LIMITS_CH3]           = "LOG.SPY.DATA[29]";
    log_menus->log_index          [LOG_MENU_BIS_LIMITS_CH3]           = LOG_BIS_LIMITS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_LIMITS_CH3]           = 0x001FC000;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_LIMITS_CH3]           = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_LIMITS_CH3]           = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_LIMITS_CH3]           = 0;

    log_menus->names              [LOG_MENU_BIS_LIMITS_CH4]           = "BIS_LIMITS_CH4";
    log_menus->properties         [LOG_MENU_BIS_LIMITS_CH4]           = "LOG.SPY.DATA[30]";
    log_menus->log_index          [LOG_MENU_BIS_LIMITS_CH4]           = LOG_BIS_LIMITS;
    log_menus->sig_sel_bit_mask   [LOG_MENU_BIS_LIMITS_CH4]           = 0x0FE00000;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_BIS_LIMITS_CH4]           = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_BIS_LIMITS_CH4]           = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_BIS_LIMITS_CH4]           = 0;

    log_menus->names              [LOG_MENU_ILC_CYC]                  = "ILC_CYC";
    log_menus->properties         [LOG_MENU_ILC_CYC]                  = "LOG.SPY.DATA[31]";
    log_menus->log_index          [LOG_MENU_ILC_CYC]                  = LOG_ILC_CYC;
    log_menus->sig_sel_bit_mask   [LOG_MENU_ILC_CYC]                  = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_ILC_CYC]                  = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_ILC_CYC]                  = 0 | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_ILC_CYC]                  = 0;

    log_menus->names              [LOG_MENU_DIM]                      = "DIM";
    log_menus->properties         [LOG_MENU_DIM]                      = "LOG.SPY.DATA[32]";
    log_menus->log_index          [LOG_MENU_DIM]                      = LOG_DIM;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM]                      = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM]                      = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM]                      = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM]                      = 0;

    log_menus->names              [LOG_MENU_DIM2]                     = "DIM2";
    log_menus->properties         [LOG_MENU_DIM2]                     = "LOG.SPY.DATA[33]";
    log_menus->log_index          [LOG_MENU_DIM2]                     = LOG_DIM2;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM2]                     = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM2]                     = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM2]                     = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM2]                     = 0;

    log_menus->names              [LOG_MENU_DIM3]                     = "DIM3";
    log_menus->properties         [LOG_MENU_DIM3]                     = "LOG.SPY.DATA[34]";
    log_menus->log_index          [LOG_MENU_DIM3]                     = LOG_DIM3;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM3]                     = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM3]                     = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM3]                     = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM3]                     = 0;

    log_menus->names              [LOG_MENU_DIM4]                     = "DIM4";
    log_menus->properties         [LOG_MENU_DIM4]                     = "LOG.SPY.DATA[35]";
    log_menus->log_index          [LOG_MENU_DIM4]                     = LOG_DIM4;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM4]                     = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM4]                     = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM4]                     = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM4]                     = 0;

    log_menus->names              [LOG_MENU_DIM5]                     = "DIM5";
    log_menus->properties         [LOG_MENU_DIM5]                     = "LOG.SPY.DATA[36]";
    log_menus->log_index          [LOG_MENU_DIM5]                     = LOG_DIM5;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM5]                     = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM5]                     = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM5]                     = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM5]                     = 0;

    log_menus->names              [LOG_MENU_DIM6]                     = "DIM6";
    log_menus->properties         [LOG_MENU_DIM6]                     = "LOG.SPY.DATA[37]";
    log_menus->log_index          [LOG_MENU_DIM6]                     = LOG_DIM6;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM6]                     = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM6]                     = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM6]                     = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM6]                     = 0;

    log_menus->names              [LOG_MENU_DIM7]                     = "DIM7";
    log_menus->properties         [LOG_MENU_DIM7]                     = "LOG.SPY.DATA[38]";
    log_menus->log_index          [LOG_MENU_DIM7]                     = LOG_DIM7;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM7]                     = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM7]                     = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM7]                     = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM7]                     = 0;

    log_menus->names              [LOG_MENU_DIM8]                     = "DIM8";
    log_menus->properties         [LOG_MENU_DIM8]                     = "LOG.SPY.DATA[39]";
    log_menus->log_index          [LOG_MENU_DIM8]                     = LOG_DIM8;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM8]                     = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM8]                     = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM8]                     = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM8]                     = 0;

    log_menus->names              [LOG_MENU_DIM9]                     = "DIM9";
    log_menus->properties         [LOG_MENU_DIM9]                     = "LOG.SPY.DATA[40]";
    log_menus->log_index          [LOG_MENU_DIM9]                     = LOG_DIM9;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM9]                     = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM9]                     = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM9]                     = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM9]                     = 0;

    log_menus->names              [LOG_MENU_DIM10]                    = "DIM10";
    log_menus->properties         [LOG_MENU_DIM10]                    = "LOG.SPY.DATA[41]";
    log_menus->log_index          [LOG_MENU_DIM10]                    = LOG_DIM10;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM10]                    = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM10]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM10]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM10]                    = 0;

    log_menus->names              [LOG_MENU_DIM11]                    = "DIM11";
    log_menus->properties         [LOG_MENU_DIM11]                    = "LOG.SPY.DATA[42]";
    log_menus->log_index          [LOG_MENU_DIM11]                    = LOG_DIM11;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM11]                    = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM11]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM11]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM11]                    = 0;

    log_menus->names              [LOG_MENU_DIM12]                    = "DIM12";
    log_menus->properties         [LOG_MENU_DIM12]                    = "LOG.SPY.DATA[43]";
    log_menus->log_index          [LOG_MENU_DIM12]                    = LOG_DIM12;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM12]                    = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM12]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM12]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM12]                    = 0;

    log_menus->names              [LOG_MENU_DIM13]                    = "DIM13";
    log_menus->properties         [LOG_MENU_DIM13]                    = "LOG.SPY.DATA[44]";
    log_menus->log_index          [LOG_MENU_DIM13]                    = LOG_DIM13;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM13]                    = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM13]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM13]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM13]                    = 0;

    log_menus->names              [LOG_MENU_DIM14]                    = "DIM14";
    log_menus->properties         [LOG_MENU_DIM14]                    = "LOG.SPY.DATA[45]";
    log_menus->log_index          [LOG_MENU_DIM14]                    = LOG_DIM14;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM14]                    = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM14]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM14]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM14]                    = 0;

    log_menus->names              [LOG_MENU_DIM15]                    = "DIM15";
    log_menus->properties         [LOG_MENU_DIM15]                    = "LOG.SPY.DATA[46]";
    log_menus->log_index          [LOG_MENU_DIM15]                    = LOG_DIM15;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM15]                    = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM15]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM15]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM15]                    = 0;

    log_menus->names              [LOG_MENU_DIM16]                    = "DIM16";
    log_menus->properties         [LOG_MENU_DIM16]                    = "LOG.SPY.DATA[47]";
    log_menus->log_index          [LOG_MENU_DIM16]                    = LOG_DIM16;
    log_menus->sig_sel_bit_mask   [LOG_MENU_DIM16]                    = 0x0000000F;
    log_menus->pm_buf_sel_bit_mask[LOG_MENU_DIM16]                    = 0x00000000;
    log_menus->status_bit_mask    [LOG_MENU_DIM16]                    = 0 | LOG_MENU_STATUS_CYCLIC_BIT_MASK | LOG_MENU_STATUS_ANALOG_BIT_MASK | LOG_MENU_STATUS_CONTINUOUS_BIT_MASK | LOG_MENU_STATUS_FREEZABLE_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK;
    log_menus->init_postmortem    [LOG_MENU_DIM16]                    = 0;

    log_menus->names           [LOG_MENU_EVENTS]                   = "EVENTS";
    log_menus->properties      [LOG_MENU_EVENTS]                   = "LOG.EVT";
    log_menus->status_bit_mask [LOG_MENU_EVENTS]                   = 0 | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK;

    log_menus->names           [LOG_MENU_CYCLES]                   = "CYCLES";
    log_menus->properties      [LOG_MENU_CYCLES]                   = "LOG.CYCLES";
    log_menus->status_bit_mask [LOG_MENU_CYCLES]                   = 0 | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK;

    log_menus->names           [LOG_MENU_TIMING]                   = "TIMING";
    log_menus->properties      [LOG_MENU_TIMING]                   = "LOG.TIMING";
    log_menus->status_bit_mask [LOG_MENU_TIMING]                   = 0 | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK;

    log_menus->names           [LOG_MENU_CONFIGURATION]            = "CONFIGURATION";
    log_menus->properties      [LOG_MENU_CONFIGURATION]            = "CONFIG.SET";
    log_menus->status_bit_mask [LOG_MENU_CONFIGURATION]            = 0 | LOG_MENU_STATUS_POSTMORTEM_BIT_MASK;

// Initialize log menu status and period from log_mgr

    logMenuUpdate(log_mgr, log_menus);
}

// EOF
