//! @file  inc_cclibs/logMenus.h
//!
//! @brief Converter Control Logging library : Menu structures header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Include liblog client header file

#include "liblog/logClient.h"

// Global log constants (LENs are in elements, SIZEs are in bytes)

#define LOG_MAX_LOG_LEN                             2800000
#define LOG_BUFFERS_LEN                             15166000
#define LOG_PM_BUF_SIZE                             594244
#define LOG_PM_BUF_VERSION                          1

// Individual log constants

#define LOG_ACQ_NUM_LOGS                            1
#define LOG_ACQ_NUM_SIG_BUFS                        4
#define LOG_ACQ_NUM_POSTMORTEM_SIG_BUFS             2
#define LOG_ACQ_NUM_CYCLES                          33

#define LOG_B_MEAS_NUM_LOGS                         1
#define LOG_B_MEAS_NUM_SIG_BUFS                     7
#define LOG_B_MEAS_NUM_POSTMORTEM_SIG_BUFS          3
#define LOG_B_MEAS_NUM_CYCLES                       33

#define LOG_I_MEAS_NUM_LOGS                         1
#define LOG_I_MEAS_NUM_SIG_BUFS                     7
#define LOG_I_MEAS_NUM_POSTMORTEM_SIG_BUFS          3
#define LOG_I_MEAS_NUM_CYCLES                       33

#define LOG_B_REG_NUM_LOGS                          1
#define LOG_B_REG_NUM_SIG_BUFS                      12
#define LOG_B_REG_NUM_POSTMORTEM_SIG_BUFS           8
#define LOG_B_REG_NUM_CYCLES                        33

#define LOG_I_REG_NUM_LOGS                          1
#define LOG_I_REG_NUM_SIG_BUFS                      12
#define LOG_I_REG_NUM_POSTMORTEM_SIG_BUFS           8
#define LOG_I_REG_NUM_CYCLES                        33

#define LOG_V_REF_NUM_LOGS                          1
#define LOG_V_REF_NUM_SIG_BUFS                      7
#define LOG_V_REF_NUM_POSTMORTEM_SIG_BUFS           5
#define LOG_V_REF_NUM_CYCLES                        33

#define LOG_V_REG_NUM_LOGS                          1
#define LOG_V_REG_NUM_SIG_BUFS                      6
#define LOG_V_REG_NUM_POSTMORTEM_SIG_BUFS           3
#define LOG_V_REG_NUM_CYCLES                        33

#define LOG_I_1KHZ_NUM_LOGS                         1
#define LOG_I_1KHZ_NUM_SIG_BUFS                     2
#define LOG_I_1KHZ_NUM_POSTMORTEM_SIG_BUFS          0
#define LOG_I_1KHZ_NUM_CYCLES                       33

#define LOG_I_EARTH_NUM_LOGS                        1
#define LOG_I_EARTH_NUM_SIG_BUFS                    1
#define LOG_I_EARTH_NUM_POSTMORTEM_SIG_BUFS         1
#define LOG_I_EARTH_NUM_CYCLES                      33

#define LOG_TEMP_NUM_LOGS                           1
#define LOG_TEMP_NUM_SIG_BUFS                       6
#define LOG_TEMP_NUM_POSTMORTEM_SIG_BUFS            5
#define LOG_TEMP_NUM_CYCLES                         1

#define LOG_FLAGS_NUM_LOGS                          1
#define LOG_FLAGS_NUM_SIG_BUFS                      1
#define LOG_FLAGS_NUM_POSTMORTEM_SIG_BUFS           1
#define LOG_FLAGS_NUM_CYCLES                        33

#define LOG_BIS_FLAGS_NUM_LOGS                      1
#define LOG_BIS_FLAGS_NUM_SIG_BUFS                  1
#define LOG_BIS_FLAGS_NUM_POSTMORTEM_SIG_BUFS       0
#define LOG_BIS_FLAGS_NUM_CYCLES                    33

#define LOG_BIS_LIMITS_NUM_LOGS                     1
#define LOG_BIS_LIMITS_NUM_SIG_BUFS                 28
#define LOG_BIS_LIMITS_NUM_POSTMORTEM_SIG_BUFS      0
#define LOG_BIS_LIMITS_NUM_CYCLES                   33

#define LOG_ILC_CYC_NUM_LOGS                        1
#define LOG_ILC_CYC_NUM_SIG_BUFS                    4
#define LOG_ILC_CYC_NUM_POSTMORTEM_SIG_BUFS         0
#define LOG_ILC_CYC_NUM_CYCLES                      1

#define LOG_DIM_NUM_LOGS                            16
#define LOG_DIM_NUM_SIG_BUFS                        4
#define LOG_DIM_NUM_POSTMORTEM_SIG_BUFS             0
#define LOG_DIM_NUM_CYCLES                          33

// Analog log menu constants

#define LOG_MENU_ACQ_IDX                            0
#define LOG_MENU_ACQ_LEN                            2
#define LOG_MENU_ACQ_MPX_IDX                        2
#define LOG_MENU_ACQ_MPX_LEN                        2
#define LOG_MENU_B_MEAS_IDX                         0
#define LOG_MENU_B_MEAS_LEN                         3
#define LOG_MENU_B_MEAS_MPX_IDX                     3
#define LOG_MENU_B_MEAS_MPX_LEN                     4
#define LOG_MENU_I_MEAS_IDX                         0
#define LOG_MENU_I_MEAS_LEN                         3
#define LOG_MENU_I_MEAS_MPX_IDX                     3
#define LOG_MENU_I_MEAS_MPX_LEN                     4
#define LOG_MENU_B_REG_IDX                          0
#define LOG_MENU_B_REG_LEN                          8
#define LOG_MENU_B_REG_MPX_IDX                      8
#define LOG_MENU_B_REG_MPX_LEN                      4
#define LOG_MENU_I_REG_IDX                          0
#define LOG_MENU_I_REG_LEN                          8
#define LOG_MENU_I_REG_MPX_IDX                      8
#define LOG_MENU_I_REG_MPX_LEN                      4
#define LOG_MENU_V_REF_IDX                          0
#define LOG_MENU_V_REF_LEN                          5
#define LOG_MENU_V_REF_MPX_IDX                      5
#define LOG_MENU_V_REF_MPX_LEN                      2
#define LOG_MENU_V_REG_IDX                          0
#define LOG_MENU_V_REG_LEN                          3
#define LOG_MENU_V_REG_MPX_IDX                      3
#define LOG_MENU_V_REG_MPX_LEN                      3
#define LOG_MENU_I_1KHZ_IDX                         0
#define LOG_MENU_I_1KHZ_LEN                         2
#define LOG_MENU_I_EARTH_IDX                        0
#define LOG_MENU_I_EARTH_LEN                        1
#define LOG_MENU_TEMP_IDX                           0
#define LOG_MENU_TEMP_LEN                           5
#define LOG_MENU_V_AC_HZ_IDX                        5
#define LOG_MENU_V_AC_HZ_LEN                        1
#define LOG_MENU_BIS_LIMITS_CH1_IDX                 0
#define LOG_MENU_BIS_LIMITS_CH1_LEN                 7
#define LOG_MENU_BIS_LIMITS_CH2_IDX                 7
#define LOG_MENU_BIS_LIMITS_CH2_LEN                 7
#define LOG_MENU_BIS_LIMITS_CH3_IDX                 14
#define LOG_MENU_BIS_LIMITS_CH3_LEN                 7
#define LOG_MENU_BIS_LIMITS_CH4_IDX                 21
#define LOG_MENU_BIS_LIMITS_CH4_LEN                 7
#define LOG_MENU_ILC_CYC_IDX                        0
#define LOG_MENU_ILC_CYC_LEN                        4
#define LOG_MENU_DIM_IDX                            0
#define LOG_MENU_DIM_LEN                            4

// Enums

enum LOG_index
{
    LOG_ACQ,
    LOG_B_MEAS,
    LOG_I_MEAS,
    LOG_B_REG,
    LOG_I_REG,
    LOG_V_REF,
    LOG_V_REG,
    LOG_I_1KHZ,
    LOG_I_EARTH,
    LOG_TEMP,
    LOG_FLAGS,
    LOG_BIS_FLAGS,
    LOG_BIS_LIMITS,
    LOG_ILC_CYC,
    LOG_DIM,
    LOG_DIM2,
    LOG_DIM3,
    LOG_DIM4,
    LOG_DIM5,
    LOG_DIM6,
    LOG_DIM7,
    LOG_DIM8,
    LOG_DIM9,
    LOG_DIM10,
    LOG_DIM11,
    LOG_DIM12,
    LOG_DIM13,
    LOG_DIM14,
    LOG_DIM15,
    LOG_DIM16,
    LOG_NUM_LOGS
};

enum LOG_menu_index
{
    LOG_MENU_ACQ,
    LOG_MENU_ACQ_MPX,
    LOG_MENU_B_MEAS,
    LOG_MENU_B_MEAS_MPX,
    LOG_MENU_I_MEAS,
    LOG_MENU_I_MEAS_MPX,
    LOG_MENU_B_REG,
    LOG_MENU_B_REG_MPX,
    LOG_MENU_I_REG,
    LOG_MENU_I_REG_MPX,
    LOG_MENU_V_REF,
    LOG_MENU_V_REF_MPX,
    LOG_MENU_V_REG,
    LOG_MENU_V_REG_MPX,
    LOG_MENU_I_1KHZ,
    LOG_MENU_I_EARTH,
    LOG_MENU_TEMP,
    LOG_MENU_V_AC_HZ,
    LOG_MENU_FLAGS,
    LOG_MENU_BIS_CH1_OK,
    LOG_MENU_BIS_CH2_OK,
    LOG_MENU_BIS_CH3_OK,
    LOG_MENU_BIS_CH4_OK,
    LOG_MENU_BIS_CH1_TESTS,
    LOG_MENU_BIS_CH2_TESTS,
    LOG_MENU_BIS_CH3_TESTS,
    LOG_MENU_BIS_CH4_TESTS,
    LOG_MENU_BIS_LIMITS_CH1,
    LOG_MENU_BIS_LIMITS_CH2,
    LOG_MENU_BIS_LIMITS_CH3,
    LOG_MENU_BIS_LIMITS_CH4,
    LOG_MENU_ILC_CYC,
    LOG_MENU_DIM,
    LOG_MENU_DIM2,
    LOG_MENU_DIM3,
    LOG_MENU_DIM4,
    LOG_MENU_DIM5,
    LOG_MENU_DIM6,
    LOG_MENU_DIM7,
    LOG_MENU_DIM8,
    LOG_MENU_DIM9,
    LOG_MENU_DIM10,
    LOG_MENU_DIM11,
    LOG_MENU_DIM12,
    LOG_MENU_DIM13,
    LOG_MENU_DIM14,
    LOG_MENU_DIM15,
    LOG_MENU_DIM16,
    LOG_MENU_EVENTS,
    LOG_MENU_CYCLES,
    LOG_MENU_TIMING,
    LOG_MENU_CONFIGURATION,
    LOG_NUM_MENUS,
    LOG_NUM_LIBLOG_MENUS = LOG_MENU_EVENTS
};

enum LOG_acq_sig_index
{
    LOG_ACQ_V_ADC_A,
    LOG_ACQ_V_ADC_B,
    LOG_ACQ_V_ADC_C,
    LOG_ACQ_V_ADC_D,
    LOG_ACQ_I_DCCT_A,
    LOG_ACQ_I_DCCT_B,
    LOG_ACQ_I_DIFF,
    LOG_ACQ_DSP_RT_PROF_0,
    LOG_ACQ_DSP_RT_PROF_1,
    LOG_ACQ_DSP_RT_CPU,
    LOG_ACQ_NUM_SIGNALS,
};

enum LOG_acq_log_sig_index
{
    LOG_SIG_IDX_ACQ_V_ADC_A               = 0x0000,
    LOG_SIG_IDX_ACQ_V_ADC_B               = 0x0001,
    LOG_SIG_IDX_ACQ_V_ADC_C               = 0x0002,
    LOG_SIG_IDX_ACQ_V_ADC_D               = 0x0003,
    LOG_SIG_IDX_ACQ_I_DCCT_A              = 0x0004,
    LOG_SIG_IDX_ACQ_I_DCCT_B              = 0x0005,
    LOG_SIG_IDX_ACQ_I_DIFF                = 0x0006,
    LOG_SIG_IDX_ACQ_DSP_RT_PROF_0         = 0x0007,
    LOG_SIG_IDX_ACQ_DSP_RT_PROF_1         = 0x0008,
    LOG_SIG_IDX_ACQ_DSP_RT_CPU            = 0x0009,
    LOG_SIG_IDX_ACQ_NUM_SIGNALS           = 10,
};

enum LOG_b_meas_sig_index
{
    LOG_B_MEAS_B_MEAS,
    LOG_B_MEAS_B_MEAS_FLTR,
    LOG_B_MEAS_B_MEAS_EXTR,
    LOG_B_MEAS_B_REF_DELAYED,
    LOG_B_MEAS_B_ERR,
    LOG_B_MEAS_B_MAX_ABS_ERR,
    LOG_B_MEAS_B_MEAS_SIM,
    LOG_B_MEAS_I_MEAS,
    LOG_B_MEAS_V_REF,
    LOG_B_MEAS_V_MEAS,
    LOG_B_MEAS_NUM_SIGNALS,
};

enum LOG_b_meas_log_sig_index
{
    LOG_SIG_IDX_B_MEAS_B_MEAS             = 0x0100,
    LOG_SIG_IDX_B_MEAS_B_MEAS_FLTR        = 0x0101,
    LOG_SIG_IDX_B_MEAS_B_MEAS_EXTR        = 0x0102,
    LOG_SIG_IDX_B_MEAS_B_REF_DELAYED      = 0x0103,
    LOG_SIG_IDX_B_MEAS_B_ERR              = 0x0104,
    LOG_SIG_IDX_B_MEAS_B_MAX_ABS_ERR      = 0x0105,
    LOG_SIG_IDX_B_MEAS_B_MEAS_SIM         = 0x0106,
    LOG_SIG_IDX_B_MEAS_I_MEAS             = 0x0107,
    LOG_SIG_IDX_B_MEAS_V_REF              = 0x0108,
    LOG_SIG_IDX_B_MEAS_V_MEAS             = 0x0109,
    LOG_SIG_IDX_B_MEAS_NUM_SIGNALS        = 10,
};

enum LOG_i_meas_sig_index
{
    LOG_I_MEAS_I_MEAS,
    LOG_I_MEAS_I_MEAS_FLTR,
    LOG_I_MEAS_I_MEAS_EXTR,
    LOG_I_MEAS_I_REF_DELAYED,
    LOG_I_MEAS_I_ERR,
    LOG_I_MEAS_I_MAX_ABS_ERR,
    LOG_I_MEAS_I_MEAS_SIM,
    LOG_I_MEAS_I_RMS,
    LOG_I_MEAS_I_RMS_LOAD,
    LOG_I_MEAS_V_REF,
    LOG_I_MEAS_V_MEAS,
    LOG_I_MEAS_I_CAPA_MEAS,
    LOG_I_MEAS_B_MEAS,
    LOG_I_MEAS_NUM_SIGNALS,
};

enum LOG_i_meas_log_sig_index
{
    LOG_SIG_IDX_I_MEAS_I_MEAS             = 0x0200,
    LOG_SIG_IDX_I_MEAS_I_MEAS_FLTR        = 0x0201,
    LOG_SIG_IDX_I_MEAS_I_MEAS_EXTR        = 0x0202,
    LOG_SIG_IDX_I_MEAS_I_REF_DELAYED      = 0x0203,
    LOG_SIG_IDX_I_MEAS_I_ERR              = 0x0204,
    LOG_SIG_IDX_I_MEAS_I_MAX_ABS_ERR      = 0x0205,
    LOG_SIG_IDX_I_MEAS_I_MEAS_SIM         = 0x0206,
    LOG_SIG_IDX_I_MEAS_I_RMS              = 0x0207,
    LOG_SIG_IDX_I_MEAS_I_RMS_LOAD         = 0x0208,
    LOG_SIG_IDX_I_MEAS_V_REF              = 0x0209,
    LOG_SIG_IDX_I_MEAS_V_MEAS             = 0x020A,
    LOG_SIG_IDX_I_MEAS_I_CAPA_MEAS        = 0x020B,
    LOG_SIG_IDX_I_MEAS_B_MEAS             = 0x020C,
    LOG_SIG_IDX_I_MEAS_NUM_SIGNALS        = 13,
};

enum LOG_b_reg_sig_index
{
    LOG_B_REG_B_MEAS_REG,
    LOG_B_REG_B_MEAS_RATE,
    LOG_B_REG_B_REF_DIRECT,
    LOG_B_REG_B_REF_USER,
    LOG_B_REG_B_REF_ADV,
    LOG_B_REG_B_REF_LIMITED,
    LOG_B_REG_B_REF_CLOSED,
    LOG_B_REG_B_REF_OPEN,
    LOG_B_REG_B_REF_RATE,
    LOG_B_REG_B_REF_ILC,
    LOG_B_REG_B_ERR_ILC,
    LOG_B_REG_B_ERR,
    LOG_B_REG_B_TRACK_DELAY,
    LOG_B_REG_B_RMS_ERR,
    LOG_B_REG_V_REF,
    LOG_B_REG_V_MEAS,
    LOG_B_REG_FG_TIME,
    LOG_B_REG_FG_TIME_ADV,
    LOG_B_REG_REF_ADVANCE,
    LOG_B_REG_NUM_SIGNALS,
};

enum LOG_b_reg_log_sig_index
{
    LOG_SIG_IDX_B_REG_B_MEAS_REG          = 0x0300,
    LOG_SIG_IDX_B_REG_B_MEAS_RATE         = 0x0301,
    LOG_SIG_IDX_B_REG_B_REF_DIRECT        = 0x0302,
    LOG_SIG_IDX_B_REG_B_REF_USER          = 0x0303,
    LOG_SIG_IDX_B_REG_B_REF_ADV           = 0x0304,
    LOG_SIG_IDX_B_REG_B_REF_LIMITED       = 0x0305,
    LOG_SIG_IDX_B_REG_B_REF_CLOSED        = 0x0306,
    LOG_SIG_IDX_B_REG_B_REF_OPEN          = 0x0307,
    LOG_SIG_IDX_B_REG_B_REF_RATE          = 0x0308,
    LOG_SIG_IDX_B_REG_B_REF_ILC           = 0x0309,
    LOG_SIG_IDX_B_REG_B_ERR_ILC           = 0x030A,
    LOG_SIG_IDX_B_REG_B_ERR               = 0x030B,
    LOG_SIG_IDX_B_REG_B_TRACK_DELAY       = 0x030C,
    LOG_SIG_IDX_B_REG_B_RMS_ERR           = 0x030D,
    LOG_SIG_IDX_B_REG_V_REF               = 0x030E,
    LOG_SIG_IDX_B_REG_V_MEAS              = 0x030F,
    LOG_SIG_IDX_B_REG_FG_TIME             = 0x0310,
    LOG_SIG_IDX_B_REG_FG_TIME_ADV         = 0x0311,
    LOG_SIG_IDX_B_REG_REF_ADVANCE         = 0x0312,
    LOG_SIG_IDX_B_REG_NUM_SIGNALS         = 19,
};

enum LOG_i_reg_sig_index
{
    LOG_I_REG_I_MEAS_REG,
    LOG_I_REG_I_MEAS_RATE,
    LOG_I_REG_I_REF_DIRECT,
    LOG_I_REG_I_REF_USER,
    LOG_I_REG_I_REF_ADV,
    LOG_I_REG_I_REF_LIMITED,
    LOG_I_REG_I_REF_CLOSED,
    LOG_I_REG_I_REF_OPEN,
    LOG_I_REG_I_REF_RATE,
    LOG_I_REG_I_REF_ILC,
    LOG_I_REG_I_ERR_ILC,
    LOG_I_REG_I_ERR,
    LOG_I_REG_I_TRACK_DELAY,
    LOG_I_REG_I_RMS_ERR,
    LOG_I_REG_SAT_FACTOR,
    LOG_I_REG_I_MAG_SAT,
    LOG_I_REG_MEAS_OHMS,
    LOG_I_REG_MEAS_HENRYS,
    LOG_I_REG_HENRYS_SAT,
    LOG_I_REG_POWER,
    LOG_I_REG_V_REF,
    LOG_I_REG_V_MEAS,
    LOG_I_REG_FG_TIME,
    LOG_I_REG_FG_TIME_ADV,
    LOG_I_REG_REF_ADVANCE,
    LOG_I_REG_NUM_SIGNALS,
};

enum LOG_i_reg_log_sig_index
{
    LOG_SIG_IDX_I_REG_I_MEAS_REG          = 0x0400,
    LOG_SIG_IDX_I_REG_I_MEAS_RATE         = 0x0401,
    LOG_SIG_IDX_I_REG_I_REF_DIRECT        = 0x0402,
    LOG_SIG_IDX_I_REG_I_REF_USER          = 0x0403,
    LOG_SIG_IDX_I_REG_I_REF_ADV           = 0x0404,
    LOG_SIG_IDX_I_REG_I_REF_LIMITED       = 0x0405,
    LOG_SIG_IDX_I_REG_I_REF_CLOSED        = 0x0406,
    LOG_SIG_IDX_I_REG_I_REF_OPEN          = 0x0407,
    LOG_SIG_IDX_I_REG_I_REF_RATE          = 0x0408,
    LOG_SIG_IDX_I_REG_I_REF_ILC           = 0x0409,
    LOG_SIG_IDX_I_REG_I_ERR_ILC           = 0x040A,
    LOG_SIG_IDX_I_REG_I_ERR               = 0x040B,
    LOG_SIG_IDX_I_REG_I_TRACK_DELAY       = 0x040C,
    LOG_SIG_IDX_I_REG_I_RMS_ERR           = 0x040D,
    LOG_SIG_IDX_I_REG_SAT_FACTOR          = 0x040E,
    LOG_SIG_IDX_I_REG_I_MAG_SAT           = 0x040F,
    LOG_SIG_IDX_I_REG_MEAS_OHMS           = 0x0410,
    LOG_SIG_IDX_I_REG_MEAS_HENRYS         = 0x0411,
    LOG_SIG_IDX_I_REG_HENRYS_SAT          = 0x0412,
    LOG_SIG_IDX_I_REG_POWER               = 0x0413,
    LOG_SIG_IDX_I_REG_V_REF               = 0x0414,
    LOG_SIG_IDX_I_REG_V_MEAS              = 0x0415,
    LOG_SIG_IDX_I_REG_FG_TIME             = 0x0416,
    LOG_SIG_IDX_I_REG_FG_TIME_ADV         = 0x0417,
    LOG_SIG_IDX_I_REG_REF_ADVANCE         = 0x0418,
    LOG_SIG_IDX_I_REG_NUM_SIGNALS         = 25,
};

enum LOG_v_ref_sig_index
{
    LOG_V_REF_V_MEAS,
    LOG_V_REF_V_REF_REG,
    LOG_V_REF_V_REF_SAT,
    LOG_V_REF_V_REF_FF,
    LOG_V_REF_V_REF_DECO,
    LOG_V_REF_V_REF,
    LOG_V_REF_V_REF_VS,
    LOG_V_REF_V_REF_DAC,
    LOG_V_REF_V_REF_RATE,
    LOG_V_REF_V_RATE_RMS,
    LOG_V_REF_V_FEEDFWD,
    LOG_V_REF_V_FF,
    LOG_V_REF_V_FF_FLTR,
    LOG_V_REF_V_HARMONICS,
    LOG_V_REF_V_ERR,
    LOG_V_REF_V_MAX_ABS_ERR,
    LOG_V_REF_V_MEAS_SIM,
    LOG_V_REF_FG_REF,
    LOG_V_REF_RT_REF,
    LOG_V_REF_B_MEAS,
    LOG_V_REF_I_MEAS,
    LOG_V_REF_NUM_SIGNALS,
};

enum LOG_v_ref_log_sig_index
{
    LOG_SIG_IDX_V_REF_V_MEAS              = 0x0500,
    LOG_SIG_IDX_V_REF_V_REF_REG           = 0x0501,
    LOG_SIG_IDX_V_REF_V_REF_SAT           = 0x0502,
    LOG_SIG_IDX_V_REF_V_REF_FF            = 0x0503,
    LOG_SIG_IDX_V_REF_V_REF_DECO          = 0x0504,
    LOG_SIG_IDX_V_REF_V_REF               = 0x0505,
    LOG_SIG_IDX_V_REF_V_REF_VS            = 0x0506,
    LOG_SIG_IDX_V_REF_V_REF_DAC           = 0x0507,
    LOG_SIG_IDX_V_REF_V_REF_RATE          = 0x0508,
    LOG_SIG_IDX_V_REF_V_RATE_RMS          = 0x0509,
    LOG_SIG_IDX_V_REF_V_FEEDFWD           = 0x050A,
    LOG_SIG_IDX_V_REF_V_FF                = 0x050B,
    LOG_SIG_IDX_V_REF_V_FF_FLTR           = 0x050C,
    LOG_SIG_IDX_V_REF_V_HARMONICS         = 0x050D,
    LOG_SIG_IDX_V_REF_V_ERR               = 0x050E,
    LOG_SIG_IDX_V_REF_V_MAX_ABS_ERR       = 0x050F,
    LOG_SIG_IDX_V_REF_V_MEAS_SIM          = 0x0510,
    LOG_SIG_IDX_V_REF_FG_REF              = 0x0511,
    LOG_SIG_IDX_V_REF_RT_REF              = 0x0512,
    LOG_SIG_IDX_V_REF_B_MEAS              = 0x0513,
    LOG_SIG_IDX_V_REF_I_MEAS              = 0x0514,
    LOG_SIG_IDX_V_REF_NUM_SIGNALS         = 21,
};

enum LOG_v_reg_sig_index
{
    LOG_V_REG_V_MEAS,
    LOG_V_REG_V_MEAS_REG,
    LOG_V_REG_V_REG_ERR,
    LOG_V_REG_V_INTEGRATOR,
    LOG_V_REG_D_REF,
    LOG_V_REG_D_MEAS,
    LOG_V_REG_F_REF_REG,
    LOG_V_REG_F_REF_LIMITED,
    LOG_V_REG_F_REF,
    LOG_V_REG_I_CAPA_REG,
    LOG_V_REG_I_CAPA,
    LOG_V_REG_I_CAPA_MEAS,
    LOG_V_REG_I_CAPA_SIM,
    LOG_V_REG_NUM_SIGNALS,
};

enum LOG_v_reg_log_sig_index
{
    LOG_SIG_IDX_V_REG_V_MEAS              = 0x0600,
    LOG_SIG_IDX_V_REG_V_MEAS_REG          = 0x0601,
    LOG_SIG_IDX_V_REG_V_REG_ERR           = 0x0602,
    LOG_SIG_IDX_V_REG_V_INTEGRATOR        = 0x0603,
    LOG_SIG_IDX_V_REG_D_REF               = 0x0604,
    LOG_SIG_IDX_V_REG_D_MEAS              = 0x0605,
    LOG_SIG_IDX_V_REG_F_REF_REG           = 0x0606,
    LOG_SIG_IDX_V_REG_F_REF_LIMITED       = 0x0607,
    LOG_SIG_IDX_V_REG_F_REF               = 0x0608,
    LOG_SIG_IDX_V_REG_I_CAPA_REG          = 0x0609,
    LOG_SIG_IDX_V_REG_I_CAPA              = 0x060A,
    LOG_SIG_IDX_V_REG_I_CAPA_MEAS         = 0x060B,
    LOG_SIG_IDX_V_REG_I_CAPA_SIM          = 0x060C,
    LOG_SIG_IDX_V_REG_NUM_SIGNALS         = 13,
};

enum LOG_i_1khz_sig_index
{
    LOG_I_1KHZ_I_REF,
    LOG_I_1KHZ_I_MEAS,
    LOG_I_1KHZ_NUM_SIGNALS,
};

enum LOG_i_1khz_log_sig_index
{
    LOG_SIG_IDX_I_1KHZ_I_REF              = 0x0700,
    LOG_SIG_IDX_I_1KHZ_I_MEAS             = 0x0701,
    LOG_SIG_IDX_I_1KHZ_NUM_SIGNALS        = 2,
};

enum LOG_i_earth_sig_index
{
    LOG_I_EARTH_I_EARTH,
    LOG_I_EARTH_NUM_SIGNALS,
};

enum LOG_i_earth_log_sig_index
{
    LOG_SIG_IDX_I_EARTH_I_EARTH           = 0x0800,
    LOG_SIG_IDX_I_EARTH_NUM_SIGNALS       = 1,
};

enum LOG_temp_sig_index
{
    LOG_TEMP_T_FGC_IN,
    LOG_TEMP_T_FGC_OUT,
    LOG_TEMP_T_INTERNAL,
    LOG_TEMP_T_DCCT_A,
    LOG_TEMP_T_DCCT_B,
    LOG_TEMP_V_AC_FREQUENCY,
    LOG_TEMP_NUM_SIGNALS,
};

enum LOG_temp_log_sig_index
{
    LOG_SIG_IDX_TEMP_T_FGC_IN             = 0x0900,
    LOG_SIG_IDX_TEMP_T_FGC_OUT            = 0x0901,
    LOG_SIG_IDX_TEMP_T_INTERNAL           = 0x0902,
    LOG_SIG_IDX_TEMP_T_DCCT_A             = 0x0903,
    LOG_SIG_IDX_TEMP_T_DCCT_B             = 0x0904,
    LOG_SIG_IDX_TEMP_V_AC_FREQUENCY       = 0x0905,
    LOG_SIG_IDX_TEMP_NUM_SIGNALS          = 6,
};

enum LOG_flags_sig_index
{
    LOG_FLAGS_B_MEAS_VALID,
    LOG_FLAGS_I_MEAS_VALID,
    LOG_FLAGS_I_MEAS_ZERO,
    LOG_FLAGS_I_DIFF_WRN,
    LOG_FLAGS_I_DIFF_FLT,
    LOG_FLAGS_V_MEAS_VALID,
    LOG_FLAGS_I_CAPA_VALID,
    LOG_FLAGS_V_FEEDFWD_VALID,
    LOG_FLAGS_FORCE_OPENLOOP,
    LOG_FLAGS_REG_OPENLOOP,
    LOG_FLAGS_ENABLE_REG_ERR,
    LOG_FLAGS_EXT_EVT_RCVD,
    LOG_FLAGS_EXT_EVT_OK,
    LOG_FLAGS_INTERNAL_EVT,
    LOG_FLAGS_START_FUNC,
    LOG_FLAGS_USE_ARM_NOW,
    LOG_FLAGS_TEST_RST,
    LOG_FLAGS_ILC_ERR,
    LOG_FLAGS_ILC_CALC,
    LOG_FLAGS_ILC_RUN,
    LOG_FLAGS_ECO_ALLOWED,
    LOG_FLAGS_FULL_ECO_REQ,
    LOG_FLAGS_ONCE_ECO_REQ,
    LOG_FLAGS_DYN_ECO_REQ,
    LOG_FLAGS_DYN_ECO_ARM_WRN,
    LOG_FLAGS_DYN_ECO_RUN_WRN,
    LOG_FLAGS_NUM_SIGNALS,
};

enum LOG_flags_log_sig_index
{
    LOG_SIG_IDX_FLAGS_B_MEAS_VALID        = 0x0A00,
    LOG_SIG_IDX_FLAGS_I_MEAS_VALID        = 0x0A01,
    LOG_SIG_IDX_FLAGS_I_MEAS_ZERO         = 0x0A02,
    LOG_SIG_IDX_FLAGS_I_DIFF_WRN          = 0x0A03,
    LOG_SIG_IDX_FLAGS_I_DIFF_FLT          = 0x0A04,
    LOG_SIG_IDX_FLAGS_V_MEAS_VALID        = 0x0A05,
    LOG_SIG_IDX_FLAGS_I_CAPA_VALID        = 0x0A06,
    LOG_SIG_IDX_FLAGS_V_FEEDFWD_VALID     = 0x0A07,
    LOG_SIG_IDX_FLAGS_FORCE_OPENLOOP      = 0x0A08,
    LOG_SIG_IDX_FLAGS_REG_OPENLOOP        = 0x0A09,
    LOG_SIG_IDX_FLAGS_ENABLE_REG_ERR      = 0x0A0A,
    LOG_SIG_IDX_FLAGS_EXT_EVT_RCVD        = 0x0A0B,
    LOG_SIG_IDX_FLAGS_EXT_EVT_OK          = 0x0A0C,
    LOG_SIG_IDX_FLAGS_INTERNAL_EVT        = 0x0A0D,
    LOG_SIG_IDX_FLAGS_START_FUNC          = 0x0A0E,
    LOG_SIG_IDX_FLAGS_USE_ARM_NOW         = 0x0A0F,
    LOG_SIG_IDX_FLAGS_TEST_RST            = 0x0A10,
    LOG_SIG_IDX_FLAGS_ILC_ERR             = 0x0A11,
    LOG_SIG_IDX_FLAGS_ILC_CALC            = 0x0A12,
    LOG_SIG_IDX_FLAGS_ILC_RUN             = 0x0A13,
    LOG_SIG_IDX_FLAGS_ECO_ALLOWED         = 0x0A14,
    LOG_SIG_IDX_FLAGS_FULL_ECO_REQ        = 0x0A15,
    LOG_SIG_IDX_FLAGS_ONCE_ECO_REQ        = 0x0A16,
    LOG_SIG_IDX_FLAGS_DYN_ECO_REQ         = 0x0A17,
    LOG_SIG_IDX_FLAGS_DYN_ECO_ARM_WRN     = 0x0A18,
    LOG_SIG_IDX_FLAGS_DYN_ECO_RUN_WRN     = 0x0A19,
    LOG_SIG_IDX_FLAGS_NUM_SIGNALS         = 26,
};

enum LOG_bis_flags_sig_index
{
    LOG_BIS_FLAGS_CH1_STATE_OK,
    LOG_BIS_FLAGS_CH1_REG_ERR_OK,
    LOG_BIS_FLAGS_CH1_V_MEAS_OK,
    LOG_BIS_FLAGS_CH1_V_RATE_OK,
    LOG_BIS_FLAGS_CH1_I_MEAS_OK,
    LOG_BIS_FLAGS_CH1_I_RATE_OK,
    LOG_BIS_FLAGS_CH1_TESTS_OK,
    LOG_BIS_FLAGS_CH1_CHAN_OK,
    LOG_BIS_FLAGS_CH2_STATE_OK,
    LOG_BIS_FLAGS_CH2_REG_ERR_OK,
    LOG_BIS_FLAGS_CH2_V_MEAS_OK,
    LOG_BIS_FLAGS_CH2_V_RATE_OK,
    LOG_BIS_FLAGS_CH2_I_MEAS_OK,
    LOG_BIS_FLAGS_CH2_I_RATE_OK,
    LOG_BIS_FLAGS_CH2_TESTS_OK,
    LOG_BIS_FLAGS_CH2_CHAN_OK,
    LOG_BIS_FLAGS_CH3_STATE_OK,
    LOG_BIS_FLAGS_CH3_REG_ERR_OK,
    LOG_BIS_FLAGS_CH3_V_MEAS_OK,
    LOG_BIS_FLAGS_CH3_V_RATE_OK,
    LOG_BIS_FLAGS_CH3_I_MEAS_OK,
    LOG_BIS_FLAGS_CH3_I_RATE_OK,
    LOG_BIS_FLAGS_CH3_TESTS_OK,
    LOG_BIS_FLAGS_CH3_CHAN_OK,
    LOG_BIS_FLAGS_CH4_STATE_OK,
    LOG_BIS_FLAGS_CH4_REG_ERR_OK,
    LOG_BIS_FLAGS_CH4_V_MEAS_OK,
    LOG_BIS_FLAGS_CH4_V_RATE_OK,
    LOG_BIS_FLAGS_CH4_I_MEAS_OK,
    LOG_BIS_FLAGS_CH4_I_RATE_OK,
    LOG_BIS_FLAGS_CH4_TESTS_OK,
    LOG_BIS_FLAGS_CH4_CHAN_OK,
    LOG_BIS_FLAGS_NUM_SIGNALS,
};

enum LOG_bis_flags_log_sig_index
{
    LOG_SIG_IDX_BIS_FLAGS_CH1_STATE_OK    = 0x0B00,
    LOG_SIG_IDX_BIS_FLAGS_CH1_REG_ERR_OK  = 0x0B01,
    LOG_SIG_IDX_BIS_FLAGS_CH1_V_MEAS_OK   = 0x0B02,
    LOG_SIG_IDX_BIS_FLAGS_CH1_V_RATE_OK   = 0x0B03,
    LOG_SIG_IDX_BIS_FLAGS_CH1_I_MEAS_OK   = 0x0B04,
    LOG_SIG_IDX_BIS_FLAGS_CH1_I_RATE_OK   = 0x0B05,
    LOG_SIG_IDX_BIS_FLAGS_CH1_TESTS_OK    = 0x0B06,
    LOG_SIG_IDX_BIS_FLAGS_CH1_CHAN_OK     = 0x0B07,
    LOG_SIG_IDX_BIS_FLAGS_CH2_STATE_OK    = 0x0B08,
    LOG_SIG_IDX_BIS_FLAGS_CH2_REG_ERR_OK  = 0x0B09,
    LOG_SIG_IDX_BIS_FLAGS_CH2_V_MEAS_OK   = 0x0B0A,
    LOG_SIG_IDX_BIS_FLAGS_CH2_V_RATE_OK   = 0x0B0B,
    LOG_SIG_IDX_BIS_FLAGS_CH2_I_MEAS_OK   = 0x0B0C,
    LOG_SIG_IDX_BIS_FLAGS_CH2_I_RATE_OK   = 0x0B0D,
    LOG_SIG_IDX_BIS_FLAGS_CH2_TESTS_OK    = 0x0B0E,
    LOG_SIG_IDX_BIS_FLAGS_CH2_CHAN_OK     = 0x0B0F,
    LOG_SIG_IDX_BIS_FLAGS_CH3_STATE_OK    = 0x0B10,
    LOG_SIG_IDX_BIS_FLAGS_CH3_REG_ERR_OK  = 0x0B11,
    LOG_SIG_IDX_BIS_FLAGS_CH3_V_MEAS_OK   = 0x0B12,
    LOG_SIG_IDX_BIS_FLAGS_CH3_V_RATE_OK   = 0x0B13,
    LOG_SIG_IDX_BIS_FLAGS_CH3_I_MEAS_OK   = 0x0B14,
    LOG_SIG_IDX_BIS_FLAGS_CH3_I_RATE_OK   = 0x0B15,
    LOG_SIG_IDX_BIS_FLAGS_CH3_TESTS_OK    = 0x0B16,
    LOG_SIG_IDX_BIS_FLAGS_CH3_CHAN_OK     = 0x0B17,
    LOG_SIG_IDX_BIS_FLAGS_CH4_STATE_OK    = 0x0B18,
    LOG_SIG_IDX_BIS_FLAGS_CH4_REG_ERR_OK  = 0x0B19,
    LOG_SIG_IDX_BIS_FLAGS_CH4_V_MEAS_OK   = 0x0B1A,
    LOG_SIG_IDX_BIS_FLAGS_CH4_V_RATE_OK   = 0x0B1B,
    LOG_SIG_IDX_BIS_FLAGS_CH4_I_MEAS_OK   = 0x0B1C,
    LOG_SIG_IDX_BIS_FLAGS_CH4_I_RATE_OK   = 0x0B1D,
    LOG_SIG_IDX_BIS_FLAGS_CH4_TESTS_OK    = 0x0B1E,
    LOG_SIG_IDX_BIS_FLAGS_CH4_CHAN_OK     = 0x0B1F,
    LOG_SIG_IDX_BIS_FLAGS_NUM_SIGNALS     = 32,
};

enum LOG_bis_limits_sig_index
{
    LOG_BIS_LIMITS_CH1_MAX_REG_ERR,
    LOG_BIS_LIMITS_CH1_MIN_V_MEAS,
    LOG_BIS_LIMITS_CH1_MAX_V_MEAS,
    LOG_BIS_LIMITS_CH1_MAX_V_RATE,
    LOG_BIS_LIMITS_CH1_MIN_I_MEAS,
    LOG_BIS_LIMITS_CH1_MAX_I_MEAS,
    LOG_BIS_LIMITS_CH1_MAX_I_RATE,
    LOG_BIS_LIMITS_CH2_MAX_REG_ERR,
    LOG_BIS_LIMITS_CH2_MIN_V_MEAS,
    LOG_BIS_LIMITS_CH2_MAX_V_MEAS,
    LOG_BIS_LIMITS_CH2_MAX_V_RATE,
    LOG_BIS_LIMITS_CH2_MIN_I_MEAS,
    LOG_BIS_LIMITS_CH2_MAX_I_MEAS,
    LOG_BIS_LIMITS_CH2_MAX_I_RATE,
    LOG_BIS_LIMITS_CH3_MAX_REG_ERR,
    LOG_BIS_LIMITS_CH3_MIN_V_MEAS,
    LOG_BIS_LIMITS_CH3_MAX_V_MEAS,
    LOG_BIS_LIMITS_CH3_MAX_V_RATE,
    LOG_BIS_LIMITS_CH3_MIN_I_MEAS,
    LOG_BIS_LIMITS_CH3_MAX_I_MEAS,
    LOG_BIS_LIMITS_CH3_MAX_I_RATE,
    LOG_BIS_LIMITS_CH4_MAX_REG_ERR,
    LOG_BIS_LIMITS_CH4_MIN_V_MEAS,
    LOG_BIS_LIMITS_CH4_MAX_V_MEAS,
    LOG_BIS_LIMITS_CH4_MAX_V_RATE,
    LOG_BIS_LIMITS_CH4_MIN_I_MEAS,
    LOG_BIS_LIMITS_CH4_MAX_I_MEAS,
    LOG_BIS_LIMITS_CH4_MAX_I_RATE,
    LOG_BIS_LIMITS_NUM_SIGNALS,
};

enum LOG_bis_limits_log_sig_index
{
    LOG_SIG_IDX_BIS_LIMITS_CH1_MAX_REG_ERR = 0x0C00,
    LOG_SIG_IDX_BIS_LIMITS_CH1_MIN_V_MEAS = 0x0C01,
    LOG_SIG_IDX_BIS_LIMITS_CH1_MAX_V_MEAS = 0x0C02,
    LOG_SIG_IDX_BIS_LIMITS_CH1_MAX_V_RATE = 0x0C03,
    LOG_SIG_IDX_BIS_LIMITS_CH1_MIN_I_MEAS = 0x0C04,
    LOG_SIG_IDX_BIS_LIMITS_CH1_MAX_I_MEAS = 0x0C05,
    LOG_SIG_IDX_BIS_LIMITS_CH1_MAX_I_RATE = 0x0C06,
    LOG_SIG_IDX_BIS_LIMITS_CH2_MAX_REG_ERR = 0x0C07,
    LOG_SIG_IDX_BIS_LIMITS_CH2_MIN_V_MEAS = 0x0C08,
    LOG_SIG_IDX_BIS_LIMITS_CH2_MAX_V_MEAS = 0x0C09,
    LOG_SIG_IDX_BIS_LIMITS_CH2_MAX_V_RATE = 0x0C0A,
    LOG_SIG_IDX_BIS_LIMITS_CH2_MIN_I_MEAS = 0x0C0B,
    LOG_SIG_IDX_BIS_LIMITS_CH2_MAX_I_MEAS = 0x0C0C,
    LOG_SIG_IDX_BIS_LIMITS_CH2_MAX_I_RATE = 0x0C0D,
    LOG_SIG_IDX_BIS_LIMITS_CH3_MAX_REG_ERR = 0x0C0E,
    LOG_SIG_IDX_BIS_LIMITS_CH3_MIN_V_MEAS = 0x0C0F,
    LOG_SIG_IDX_BIS_LIMITS_CH3_MAX_V_MEAS = 0x0C10,
    LOG_SIG_IDX_BIS_LIMITS_CH3_MAX_V_RATE = 0x0C11,
    LOG_SIG_IDX_BIS_LIMITS_CH3_MIN_I_MEAS = 0x0C12,
    LOG_SIG_IDX_BIS_LIMITS_CH3_MAX_I_MEAS = 0x0C13,
    LOG_SIG_IDX_BIS_LIMITS_CH3_MAX_I_RATE = 0x0C14,
    LOG_SIG_IDX_BIS_LIMITS_CH4_MAX_REG_ERR = 0x0C15,
    LOG_SIG_IDX_BIS_LIMITS_CH4_MIN_V_MEAS = 0x0C16,
    LOG_SIG_IDX_BIS_LIMITS_CH4_MAX_V_MEAS = 0x0C17,
    LOG_SIG_IDX_BIS_LIMITS_CH4_MAX_V_RATE = 0x0C18,
    LOG_SIG_IDX_BIS_LIMITS_CH4_MIN_I_MEAS = 0x0C19,
    LOG_SIG_IDX_BIS_LIMITS_CH4_MAX_I_MEAS = 0x0C1A,
    LOG_SIG_IDX_BIS_LIMITS_CH4_MAX_I_RATE = 0x0C1B,
    LOG_SIG_IDX_BIS_LIMITS_NUM_SIGNALS    = 28,
};

enum LOG_ilc_cyc_sig_index
{
    LOG_ILC_CYC_RMS_ERR_ILC,
    LOG_ILC_CYC_INIT_RMS_ERR_ILC,
    LOG_ILC_CYC_RMS_V_RATE,
    LOG_ILC_CYC_RMS_V_RATE_LIM,
    LOG_ILC_CYC_NUM_SIGNALS,
};

enum LOG_ilc_cyc_log_sig_index
{
    LOG_SIG_IDX_ILC_CYC_RMS_ERR_ILC       = 0x0D00,
    LOG_SIG_IDX_ILC_CYC_INIT_RMS_ERR_ILC  = 0x0D01,
    LOG_SIG_IDX_ILC_CYC_RMS_V_RATE        = 0x0D02,
    LOG_SIG_IDX_ILC_CYC_RMS_V_RATE_LIM    = 0x0D03,
    LOG_SIG_IDX_ILC_CYC_NUM_SIGNALS       = 4,
};

enum LOG_dim_sig_index
{
    LOG_DIM_ANA_A,
    LOG_DIM_ANA_B,
    LOG_DIM_ANA_C,
    LOG_DIM_ANA_D,
    LOG_DIM_NUM_SIGNALS,
};

enum LOG_dim_log_sig_index
{
    LOG_SIG_IDX_DIM_ANA_A                 = 0x0E00,
    LOG_SIG_IDX_DIM_ANA_B                 = 0x0E01,
    LOG_SIG_IDX_DIM_ANA_C                 = 0x0E02,
    LOG_SIG_IDX_DIM_ANA_D                 = 0x0E03,
    LOG_SIG_IDX_DIM_NUM_SIGNALS           = 4,
};

// Log menus structure - structure of arrays to be compatible with FGC properties

struct LOG_menus
{
    struct LOG_mgr    * log_mgr;
    uint32_t            max_buffer_size;                     // Filled in by liblog from log.ods
    char        const * names              [LOG_NUM_MENUS];
    char        const * properties         [LOG_NUM_MENUS];
    uint32_t            log_index          [LOG_NUM_MENUS];
    float               period             [LOG_NUM_MENUS];
    uint32_t            sig_sel_bit_mask   [LOG_NUM_MENUS];
    uint32_t            pm_buf_sel_bit_mask[LOG_NUM_MENUS];
    uint32_t            status_bit_mask    [LOG_NUM_MENUS];
    uint32_t            init_postmortem    [LOG_NUM_MENUS];
    uint32_t            ctrl_postmortem    [LOG_NUM_MENUS];
};

// Static inline functions

//! Return log index from a log menu index
//!
//! @param[in]      log_menus           Pointer to log_menus structure
//! @param[in]      menu_index          Index of menu

static inline uint32_t logMenuIndexToLogIndex(struct LOG_menus const * const log_menus,
                                              uint32_t                 const menu_index)
{
    return log_menus->log_index[menu_index];
}


//! Test if a log menu contributes to the postmortem data
//!
//! @param[in]      log_menus           Pointer to log_menus structure
//! @param[in]      menu_index          Index of menu to test

static inline bool logMenuIsInPostmortem(struct LOG_menus const * const log_menus,
                                         uint32_t                 const menu_index)
{
    return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_POSTMORTEM_BIT_MASK) != 0;
}


//! Test if a log menu is multiplexed
//!
//! @param[in]      log_menus           Pointer to log_menus structure
//! @param[in]      menu_index          Index of menu to test

static inline bool logMenuIsMpx(struct LOG_menus const * const log_menus,
                                uint32_t                 const menu_index)
{
    return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_MPX_BIT_MASK) != 0;
}


//! Test if a log menu is in the PM_BUF
//!
//! @param[in]      log_menus           Pointer to log_menus structure
//! @param[in]      menu_index          Index of menu to test

static inline bool logMenuIsInPmBuf(struct LOG_menus const * const log_menus,
                                    uint32_t                 const menu_index)
{
    return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_PM_BUF_BIT_MASK) != 0;
}


//! Prepare read_request structure to read out signals for a log menu
//!
//! Before calling logReadRequest(), the application must prepare the read_request structure.
//! If the application wants to read out the signals for a log menu, then this function can
//! be used.
//!
//! @param[in]    log_menus             Pointer to LOG_menus structure
//! @param[in]    menu_index            Index of menu whose signals should be read out
//! @param[in]    action                LOG_READ_GET, LOG_READ_GET_PM_BUF or LOG_READ_GET_SPY
//! @param[in]    cyc_sel               Cycle selector
//! @param[in]    timing                Pointer to LOG_read_timing structure
//! @param[in]    sub_sampling_period   Sub-sampling period in units of log periods  (zero = automatic)
//! @param[out]   read_request          Pointer to read_request structure to initialize

static inline void logPrepareReadMenuRequest(struct LOG_menus   const * const log_menus,
                                             uint32_t                   const menu_index,
                                             enum LOG_read_action const action,
                                             uint32_t                   const cyc_sel,
                                             struct LOG_read_timing   * const timing,
                                             uint32_t                   const sub_sampling_period,
                                             struct LOG_read_request  * const read_request)
{
    read_request->log_index = log_menus->log_index[menu_index];
    read_request->action    = action;

    read_request->u.get.cyc_sel             = cyc_sel;
    read_request->u.get.timing              = *timing;
    read_request->u.get.max_buffer_size     = log_menus->max_buffer_size;
    read_request->u.get.sub_sampling_period = sub_sampling_period;

    if((log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_ANALOG_BIT_MASK) != 0)
    {
        read_request->u.get.selectors_bit_mask = log_menus->sig_sel_bit_mask[menu_index];
        read_request->u.get.signals_bit_mask   = 0;
    }
    else
    {
        read_request->u.get.selectors_bit_mask = 0;
        read_request->u.get.signals_bit_mask   = log_menus->sig_sel_bit_mask[menu_index];
    }
}


//! Prepare read_request structure to read out all possible or the actual signal names for a menu
//!
//! Before calling logReadRequest(), the application must prepare the read_request structure.
//! If the application wants to read out the list of signals names for a log menu, then this function can
//! be used.
//!
//! The response will be in the header only - no log data is provided. Two get requests are supported:
//!
//! LOG_READ_GET_SIG_NAMES - Prepare read_request to return the actual signal names, separated by spaces
//!
//! LOG_READ_GET_ALL_SIG_NAMES - Only if the menu is multiplexed, the function will prepare the
//!                              read_request to return all possible signal names, otherwise the
//!                              function returns false and the application should return an empty string
//!
//! @param[in]    log_menus       Pointer to log_menus structure
//! @param[in]    menu_index      Index of menu whose signals should be read out
//! @param[in]    action          LOG_READ_GET_SIG_NAMES or LOG_READ_GET_ALL_SIG_NAMES
//! @param[out]   read_request    Pointer to read_request structure to initialize
//! @retval       true            read_request is ready to use
//! @retval       false           application should return an empty string

static inline bool logPrepareReadMenuSigNamesRequest(struct LOG_menus  const * const log_menus,
                                                     uint32_t                  const menu_index,
                                                     enum LOG_read_action      const action,
                                                     struct LOG_read_request * const read_request)
{
    if(action == LOG_READ_GET_SIG_NAMES ||
      (action == LOG_READ_GET_ALL_SIG_NAMES && logMenuIsMpx(log_menus,menu_index)))
    {
        // Generate read_request to retrieve all or actual signal names

        struct LOG_read_timing timing = { { { 0 } } };

        logPrepareReadMenuRequest(log_menus,
                                  menu_index,
                                  action,
                                  0,              // cyc_sel
                                  &timing,
                                  0,              // sub_sampling_period
                                  read_request);

        return true;
    }

    // read_request not generated so return false - the application should return an empty string

    return false;
}


//! Prepare read_request structure to read out one signal from a specified log
//!
//! Before calling logReadRequest(), the application must prepare the read_request structure.
//! If the application wants to read out one signal from a specified log, then this function can
//! be used.
//!
//! @param[in]    log_menus             Pointer to LOG_menus structure
//! @param[in]    log_sig_index         Combined indexes of log and signal 0xLLSS to be read out
//! @param[in]    action                LOG_READ_GET, LOG_READ_GET_PM_BUF or LOG_READ_GET_SPY
//! @param[in]    cyc_sel               Cycle selector
//! @param[in]    timing                Pointer to LOG_read_timing structure
//! @param[in]    sub_sampling_period   Sub-sampling period in units of log periods (zero = automatic)
//! @param[out]   read_request          Pointer to read_request structure to initialize

static inline void logPrepareReadSignalRequest(struct LOG_menus  const * const log_menus,
                                               uint32_t                  const log_sig_index,
                                               enum LOG_read_action      const action,
                                               uint32_t                  const cyc_sel,
                                               struct LOG_read_timing  * const timing,
                                               uint32_t                  const sub_sampling_period,
                                               struct LOG_read_request * const read_request)
{
    read_request->log_index = log_sig_index >> 8;
    read_request->action    = action;

    read_request->u.get.cyc_sel             = cyc_sel;
    read_request->u.get.timing              = *timing;
    read_request->u.get.max_buffer_size     = log_menus->max_buffer_size;
    read_request->u.get.sub_sampling_period = sub_sampling_period;
    read_request->u.get.selectors_bit_mask  = 0;
    read_request->u.get.signals_bit_mask    = (1 << (log_sig_index & 0xFF));
}


//! Prepare read_request structure to read out the PM_BUF signals for a log menu
//!
//! Before calling logReadRequest(), the application must prepare the read_request structure.
//! If the application wants to read out the PM_BUF signals for a log menu, then this function can
//! be used.
//!
//! @param[in]    log_menus             Pointer to LOG_menus structure
//! @param[in]    menu_index            Index of menu whose signals should be read out
//! @param[in]    timing                Pointer to LOG_read_timing structure
//! @param[out]   read_request          Pointer to read_request structure to initialize

static inline void logPrepareReadPmBufRequest(struct LOG_menus  const * const log_menus,
                                              uint32_t                  const menu_index,
                                              struct LOG_read_timing  * const timing,
                                              struct LOG_read_request * const read_request)
{
    read_request->log_index = log_menus->log_index[menu_index];
    read_request->action    = LOG_READ_GET_PM_BUF;

    read_request->u.get.cyc_sel             = 0;
    read_request->u.get.timing              = *timing;
    read_request->u.get.max_buffer_size     = log_menus->max_buffer_size;
    read_request->u.get.sub_sampling_period = 1;
    read_request->u.get.selectors_bit_mask  = log_menus->pm_buf_sel_bit_mask[menu_index];
    read_request->u.get.signals_bit_mask    = 0;
}


//! Prepare read_request structure to change a signal's name and/or units.
//!
//! Before calling logReadRequest(), the application must prepare the read_request structure.
//! If the application wants to change a signal name or units, then this function
//! shoud be used. If sig_name or units are a nul string then the previous name or units
//! are not changed. If the name or units strings are too long, they will be truncated.
//!
//! @param[in]    log_menus             Pointer to LOG_menus structure
//! @param[in]    log_index             Index of the log containing the signal
//! @param[in]    sig_index             Index of the signal within the log
//! @param[in]    sig_name              Pointer to the new signal name.
//! @param[in]    units                 Pointer to the new units.
//! @param[out]   read_request          Pointer to read_request structure to initialize

static inline void logPrepareStoreSigNameAndUnitsRequest(struct LOG_menus  const * const log_menus,
                                                         uint32_t                  const log_index,
                                                         uint32_t                  const sig_index,
                                                         char                    * const sig_name,
                                                         char                    * const units,
                                                         struct LOG_read_request * const read_request)
{
    read_request->log_index = log_index;
    read_request->action = LOG_READ_STORE_SIG_NAMES_AND_UNITS;
    read_request->u.set.sig_index = sig_index;

    strncpy(read_request->u.set.sig_name, sig_name, LOG_SIG_NAME_LEN-1);
    strncpy(read_request->u.set.units,    units,    LOG_UNITS_LEN-1);

    read_request->u.set.sig_name[LOG_SIG_NAME_LEN-1] = '\0';
    read_request->u.set.units   [LOG_UNITS_LEN-1]    = '\0';
}


//! Change a log menu name pointer
//!
//! The application can replace the automatically generated log menu name string pointer
//! using this function. Note that the menu name string is not changed, only the pointer
//! to the name.
//!
//! @param[in,out]  log_menus           Pointer to log_menus structure
//! @param[in]      menu_index          Index of menu whose name pointer should be replaced
//! @param[in]      menu_name           Pointer to new menu name string

static inline void logChangeMenuName(struct LOG_menus * const log_menus,
                                     uint32_t           const menu_index,
                                     char       const * const menu_name)
{
    log_menus->names[menu_index] = menu_name;
}


//! Get the log menu signals selector mask
//!
//! @param[in,out]  log_menus           Pointer to log_menus structure
//! @param[in]      menu_index          Index of menu whose name pointer should be replaced
//! @retval         sig_sel_bit_mask for the log menu

static inline uint32_t logGetMenuSigSelMask(struct LOG_menus * const log_menus,
                                            uint32_t           const menu_index)
{
    return log_menus->sig_sel_bit_mask[menu_index];
}


//! Modify a log menu name signals selector mask
//!
//! The application can modify the signals selector mask for a log menu using this function.
//! The mask will be anded with the existing mask, so it is not possible to set bits that are
//! currently zero.
//!
//! @param[in,out]  log_menus           Pointer to log_menus structure
//! @param[in]      menu_index          Index of menu whose name pointer should be replaced
//! @param[in]      sig_sel_bit_mask    Signal/selector bit mask for the menu

static inline void logModifyMenuSigSelMask(struct LOG_menus * const log_menus,
                                           uint32_t           const menu_index,
                                           uint32_t           const sig_sel_bit_mask)
{
    log_menus->sig_sel_bit_mask[menu_index] &= sig_sel_bit_mask;
}


//! Update log menu status bit masks to reflect running and disabled statuses of the associated log
//! and transfer the sampling periods from log_mgr to log_menus.
//!
//! The log menu bit masks have some static bits configured from the log definition spreadsheet.
//! It also has two dynamic bits: RUNNING and DISABLED. This function will set/clear the RUNNING bit
//! based on the same bit for log that the log menu is associated with.
//!
//! The DISABLED bit will be set if the log that the log menu is associated with is DISABLED. However,
//! if the log is re-enabled, the log menu will not be automatically re-enabled, since the application
//! can choose to disable a log menu while other log menus associated with the same log remain enabled.
//!
//! The function will also transfer the sampling period (s+ns) from log_mgr to the log menu float period (s)
//!
//! @param[in]      log_mgr             Pointer to the log_mgr structure
//! @param[in,out]  log_menus           Pointer to the log_menus structure

static inline void logMenuUpdate(struct LOG_mgr   * const log_mgr,
                                 struct LOG_menus * const log_menus)
{
    uint32_t const running_logs_mask  = log_mgr->running_logs_mask;
    uint32_t const disabled_logs_mask = log_mgr->disabled_logs_mask;
    uint32_t       menu_index;

    for(menu_index = 0 ; menu_index < LOG_NUM_LIBLOG_MENUS ; menu_index++)
    {
        uint32_t const log_index    = log_menus->log_index[menu_index];
        uint32_t const log_bit_mask = (1 << log_index);

        log_menus->period[menu_index] = (float)log_mgr->period[log_index].secs.rel
                                      + 1.0E-9 * (float)log_mgr->period[log_index].ns;

        if(log_menus->init_postmortem[menu_index] != 0 || log_menus->ctrl_postmortem[menu_index] != 0)
        {
            log_menus->status_bit_mask[menu_index] |=  LOG_MENU_STATUS_POSTMORTEM_BIT_MASK;
        }
        else
        {
            log_menus->status_bit_mask[menu_index] &= ~LOG_MENU_STATUS_POSTMORTEM_BIT_MASK;
        }

        if((running_logs_mask & log_bit_mask) != 0)
        {
            log_menus->status_bit_mask[menu_index] |=  LOG_MENU_STATUS_RUNNING_BIT_MASK;
        }
        else
        {
            log_menus->status_bit_mask[menu_index] &= ~LOG_MENU_STATUS_RUNNING_BIT_MASK;
        }

        if((disabled_logs_mask & log_bit_mask) != 0)
        {
            log_menus->status_bit_mask[menu_index] |=  LOG_MENU_STATUS_DISABLED_BIT_MASK;
        }
    }
}


//! Enable a log menu by clearing the DISABLED status bit
//!
//! This function will clear the DISABLED log menu status bit for the specified log menu.
//! This does not guarantee that the associated log is actually enabled. That can be done with the
//! logEnable() function from logClient.h.
//!
//! @param[in,out]  log_menus           Pointer to the log_menus structure
//! @param[in]      menu_index          Index of log menu to enable

static inline void logMenuEnable(struct LOG_menus * const log_menus,
                                 uint32_t           const menu_index)
{
     log_menus->status_bit_mask[menu_index] &= ~LOG_MENU_STATUS_DISABLED_BIT_MASK;
}


//! Disable a log menu by setting the DISABLED status bit
//!
//! This function will set the DISABLED log menu status bit for the specified log menu.
//! This does not automatically disable the associated log. That can be done with the
//! logDisable() function from logClient.h.
//!
//! @param[in,out]  log_menus           Pointer to the log_menus structure
//! @param[in]      menu_index          Index of log menu to disable

static inline void logMenuDisable(struct LOG_menus * const log_menus,
                                  uint32_t           const menu_index)
{
     log_menus->status_bit_mask[menu_index] |= LOG_MENU_STATUS_DISABLED_BIT_MASK;
}


//! Test if a log menu is DISABLED
//!
//! @param[in,out]  log_menus           Pointer to the log_menus structure
//! @param[in]      menu_index          Index of log menu to verify
//! @retval         true                Log menu is DISABLED
//! @retval         false               Log menu is ENABLED

static inline bool logMenuIsDisabled(struct LOG_menus * const log_menus,
                                     uint32_t           const menu_index)
{
     return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_DISABLED_BIT_MASK) != 0;
}


//! Test if a log menu is ALIAS
//!
//! @param[in,out]  log_menus           Pointer to the log_menus structure
//! @param[in]      menu_index          Index of log menu to verify
//! @retval         true                Log menu is ALIAS
//! @retval         false               Log menu is not ALIAS

static inline bool logMenuIsAlias(struct LOG_menus * const log_menus,
                                  uint32_t           const menu_index)
{
     return (log_menus->status_bit_mask[menu_index] & LOG_MENU_STATUS_ALIAS_BIT_MASK) != 0;
}


//! Test if the log menu has to be saved. This is the case for all PM and DIM logs that
//! are not DISABLED
//!
//! @param[in,out]  log_menus           Pointer to the log_menus structure
//! @param[in]      menu_index          Index of log menu to test
//! @retval         true                Log menu needs to be SAVE
//! @retval         false               Log menu does not need to be SAVE

static inline bool logMenuIsSave(struct LOG_menus * const log_menus,
                                 uint32_t           const menu_index)
{
    return    logMenuIsDisabled(log_menus, menu_index) == false
           && (log_menus->status_bit_mask[menu_index] & (LOG_MENU_STATUS_POSTMORTEM_BIT_MASK | LOG_MENU_STATUS_DIM_BIT_MASK)) != 0; 
}


//! Set bit(s) in the log menu status
//!
//! This function will OR the supplied bit mask into the status mask for the specified log menu.
//!
//! @param[in,out]  log_menus           Pointer to the log_menus structure
//! @param[in]      menu_index          Index of log menu
//! @param[in]      bit_mask            Bit mask to OR into log menu status

static inline void logMenuStatusSetBitMask(struct LOG_menus * const log_menus,
                                           uint32_t           const menu_index,
                                           uint32_t           const bit_mask)
{
     log_menus->status_bit_mask[menu_index] |= bit_mask;
}


//! Reset bit(s) in the log menu status
//!
//! This function will AND the complement of the supplied bit mask into the status mask for the
//! specified log menu.
//!
//! @param[in,out]  log_menus           Pointer to the log_menus structure
//! @param[in]      menu_index          Index of log menu
//! @param[in]      bit_mask            Bit mask to complement and AND into log menu status

static inline void logMenuStatusResetBitMask(struct LOG_menus * const log_menus,
                                             uint32_t           const menu_index,
                                             uint32_t           const bit_mask)
{
     log_menus->status_bit_mask[menu_index] &= ~bit_mask;
}

// EOF
