//! @file  inc_cclibs/log_mpx_pars.h
//!
//! @brief Converter Control Logging library : ccrt log mpx pars header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

{ "ACQ"               , PAR_ENUM, LOG_MENU_ACQ_LEN             , enum_log_acq                 , { .u = &log_buffers.acq.selectors[0]          [LOG_MENU_ACQ_IDX]             }, LOG_MENU_ACQ_LEN             ,  PARS_FIXLEN|PARS_RO                       },
{ "ACQ_MPX"           , PAR_ENUM, LOG_MENU_ACQ_MPX_LEN         , enum_log_acq_mpx             , { .u = &log_buffers.acq.selectors[0]          [LOG_MENU_ACQ_MPX_IDX]         }, LOG_MENU_ACQ_MPX_LEN         ,  PARS_FIXLEN|PARS_RW|PARS_CFG|PARS_LOG_MPX },
{ "B_MEAS"            , PAR_ENUM, LOG_MENU_B_MEAS_LEN          , enum_log_b_meas              , { .u = &log_buffers.b_meas.selectors[0]       [LOG_MENU_B_MEAS_IDX]          }, LOG_MENU_B_MEAS_LEN          ,  PARS_FIXLEN|PARS_RO                       },
{ "B_MEAS_MPX"        , PAR_ENUM, LOG_MENU_B_MEAS_MPX_LEN      , enum_log_b_meas_mpx          , { .u = &log_buffers.b_meas.selectors[0]       [LOG_MENU_B_MEAS_MPX_IDX]      }, LOG_MENU_B_MEAS_MPX_LEN      ,  PARS_FIXLEN|PARS_RW|PARS_CFG|PARS_LOG_MPX },
{ "I_MEAS"            , PAR_ENUM, LOG_MENU_I_MEAS_LEN          , enum_log_i_meas              , { .u = &log_buffers.i_meas.selectors[0]       [LOG_MENU_I_MEAS_IDX]          }, LOG_MENU_I_MEAS_LEN          ,  PARS_FIXLEN|PARS_RO                       },
{ "I_MEAS_MPX"        , PAR_ENUM, LOG_MENU_I_MEAS_MPX_LEN      , enum_log_i_meas_mpx          , { .u = &log_buffers.i_meas.selectors[0]       [LOG_MENU_I_MEAS_MPX_IDX]      }, LOG_MENU_I_MEAS_MPX_LEN      ,  PARS_FIXLEN|PARS_RW|PARS_CFG|PARS_LOG_MPX },
{ "B_REG"             , PAR_ENUM, LOG_MENU_B_REG_LEN           , enum_log_b_reg               , { .u = &log_buffers.b_reg.selectors[0]        [LOG_MENU_B_REG_IDX]           }, LOG_MENU_B_REG_LEN           ,  PARS_FIXLEN|PARS_RO                       },
{ "B_REG_MPX"         , PAR_ENUM, LOG_MENU_B_REG_MPX_LEN       , enum_log_b_reg_mpx           , { .u = &log_buffers.b_reg.selectors[0]        [LOG_MENU_B_REG_MPX_IDX]       }, LOG_MENU_B_REG_MPX_LEN       ,  PARS_FIXLEN|PARS_RW|PARS_CFG|PARS_LOG_MPX },
{ "I_REG"             , PAR_ENUM, LOG_MENU_I_REG_LEN           , enum_log_i_reg               , { .u = &log_buffers.i_reg.selectors[0]        [LOG_MENU_I_REG_IDX]           }, LOG_MENU_I_REG_LEN           ,  PARS_FIXLEN|PARS_RO                       },
{ "I_REG_MPX"         , PAR_ENUM, LOG_MENU_I_REG_MPX_LEN       , enum_log_i_reg_mpx           , { .u = &log_buffers.i_reg.selectors[0]        [LOG_MENU_I_REG_MPX_IDX]       }, LOG_MENU_I_REG_MPX_LEN       ,  PARS_FIXLEN|PARS_RW|PARS_CFG|PARS_LOG_MPX },
{ "V_REF"             , PAR_ENUM, LOG_MENU_V_REF_LEN           , enum_log_v_ref               , { .u = &log_buffers.v_ref.selectors[0]        [LOG_MENU_V_REF_IDX]           }, LOG_MENU_V_REF_LEN           ,  PARS_FIXLEN|PARS_RO                       },
{ "V_REF_MPX"         , PAR_ENUM, LOG_MENU_V_REF_MPX_LEN       , enum_log_v_ref_mpx           , { .u = &log_buffers.v_ref.selectors[0]        [LOG_MENU_V_REF_MPX_IDX]       }, LOG_MENU_V_REF_MPX_LEN       ,  PARS_FIXLEN|PARS_RW|PARS_CFG|PARS_LOG_MPX },
{ "V_REG"             , PAR_ENUM, LOG_MENU_V_REG_LEN           , enum_log_v_reg               , { .u = &log_buffers.v_reg.selectors[0]        [LOG_MENU_V_REG_IDX]           }, LOG_MENU_V_REG_LEN           ,  PARS_FIXLEN|PARS_RO                       },
{ "V_REG_MPX"         , PAR_ENUM, LOG_MENU_V_REG_MPX_LEN       , enum_log_v_reg_mpx           , { .u = &log_buffers.v_reg.selectors[0]        [LOG_MENU_V_REG_MPX_IDX]       }, LOG_MENU_V_REG_MPX_LEN       ,  PARS_FIXLEN|PARS_RW|PARS_CFG|PARS_LOG_MPX },
{ "I_1KHZ"            , PAR_ENUM, LOG_MENU_I_1KHZ_LEN          , enum_log_i_1khz              , { .u = &log_buffers.i_1khz.selectors[0]       [LOG_MENU_I_1KHZ_IDX]          }, LOG_MENU_I_1KHZ_LEN          ,  PARS_FIXLEN|PARS_RO                       },
{ "I_EARTH"           , PAR_ENUM, LOG_MENU_I_EARTH_LEN         , enum_log_i_earth             , { .u = &log_buffers.i_earth.selectors[0]      [LOG_MENU_I_EARTH_IDX]         }, LOG_MENU_I_EARTH_LEN         ,  PARS_FIXLEN|PARS_RO                       },
{ "TEMP"              , PAR_ENUM, LOG_MENU_TEMP_LEN            , enum_log_temp                , { .u = &log_buffers.temp.selectors[0]         [LOG_MENU_TEMP_IDX]            }, LOG_MENU_TEMP_LEN            ,  PARS_FIXLEN|PARS_RO                       },
{ "V_AC_HZ"           , PAR_ENUM, LOG_MENU_V_AC_HZ_LEN         , enum_log_v_ac_hz             , { .u = &log_buffers.temp.selectors[0]         [LOG_MENU_V_AC_HZ_IDX]         }, LOG_MENU_V_AC_HZ_LEN         ,  PARS_FIXLEN|PARS_RO                       },
{ "BIS_LIMITS_CH1"    , PAR_ENUM, LOG_MENU_BIS_LIMITS_CH1_LEN  , enum_log_bis_limits_ch1      , { .u = &log_buffers.bis_limits.selectors[0]   [LOG_MENU_BIS_LIMITS_CH1_IDX]  }, LOG_MENU_BIS_LIMITS_CH1_LEN  ,  PARS_FIXLEN|PARS_RO                       },
{ "BIS_LIMITS_CH2"    , PAR_ENUM, LOG_MENU_BIS_LIMITS_CH2_LEN  , enum_log_bis_limits_ch2      , { .u = &log_buffers.bis_limits.selectors[0]   [LOG_MENU_BIS_LIMITS_CH2_IDX]  }, LOG_MENU_BIS_LIMITS_CH2_LEN  ,  PARS_FIXLEN|PARS_RO                       },
{ "BIS_LIMITS_CH3"    , PAR_ENUM, LOG_MENU_BIS_LIMITS_CH3_LEN  , enum_log_bis_limits_ch3      , { .u = &log_buffers.bis_limits.selectors[0]   [LOG_MENU_BIS_LIMITS_CH3_IDX]  }, LOG_MENU_BIS_LIMITS_CH3_LEN  ,  PARS_FIXLEN|PARS_RO                       },
{ "BIS_LIMITS_CH4"    , PAR_ENUM, LOG_MENU_BIS_LIMITS_CH4_LEN  , enum_log_bis_limits_ch4      , { .u = &log_buffers.bis_limits.selectors[0]   [LOG_MENU_BIS_LIMITS_CH4_IDX]  }, LOG_MENU_BIS_LIMITS_CH4_LEN  ,  PARS_FIXLEN|PARS_RO                       },
{ "ILC_CYC"           , PAR_ENUM, LOG_MENU_ILC_CYC_LEN         , enum_log_ilc_cyc             , { .u = &log_buffers.ilc_cyc.selectors[0]      [LOG_MENU_ILC_CYC_IDX]         }, LOG_MENU_ILC_CYC_LEN         ,  PARS_FIXLEN|PARS_RO                       },
{ NULL }

// EOF
