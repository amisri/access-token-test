//! @file  inc_cclibs/pm_buf_63.h
//!
//! @brief Converter Control Logging library : PM_BUF data structures header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdint.h>
#include <string.h>

// Version and size of PM_buf_63 structure

#define PM_BUF_63_VERSION              1
#define PM_BUF_63_SIZE                 594244

// PM_buf_63 log lengths

#define PM_BUF_63_EVTLOG_LEN           1292
#define PM_BUF_63_EVTLOG_PROPERTY_LEN  24
#define PM_BUF_63_EVTLOG_SYMBOL_LEN    36
#define PM_BUF_63_EVTLOG_ACTION_LEN    7
#define PM_BUF_63_IAB_LEN              20000
#define PM_BUF_63_ILOOP_LEN            20000
#define PM_BUF_63_IEARTH_LEN           4000
#define PM_BUF_63_MAX_LEN              20000

// Event log record structure - Network Byte Order

struct PM_buf_63_evtlog
{
    uint32_t timestamp_s;
    uint32_t timestamp_us;

    char     property[PM_BUF_63_EVTLOG_PROPERTY_LEN];
    char     symbol  [PM_BUF_63_EVTLOG_SYMBOL_LEN];
    char     action  [PM_BUF_63_EVTLOG_ACTION_LEN];
    char     status;
};

// Liblog signal log header structure - Little Endian

struct PM_buf_63_signals_header
{
    uint32_t first_sample_time_s;
    uint32_t first_sample_time_ns;
    uint32_t period_s;
    uint32_t period_ns;
};

// PM_buf structure

struct PM_buf_63
{
    // PM_buf structure version in Network Byte Order - set to PM_BUF_63_VERSION in the FGC

    uint32_t version;

    // Event log - Network Byte Order

    struct PM_buf_63_evtlog evtlog[PM_BUF_63_EVTLOG_LEN];

    // Log IAB - Little Endian

    struct PM_buf_63_iab
    {
        struct PM_buf_63_signals_header header;

        struct PM_buf_63_iab_signals
        {
            float i_a[PM_BUF_63_IAB_LEN];
            float i_b[PM_BUF_63_IAB_LEN];
        } signals;
    } iab;

    // Log ILOOP - Little Endian

    struct PM_buf_63_iloop
    {
        struct PM_buf_63_signals_header header;

        struct PM_buf_63_iloop_signals
        {
            float i_meas[PM_BUF_63_ILOOP_LEN];
            float i_ref[PM_BUF_63_ILOOP_LEN];
            float v_ref[PM_BUF_63_ILOOP_LEN];
            float v_meas[PM_BUF_63_ILOOP_LEN];
        } signals;
    } iloop;

    // Log IEARTH - Little Endian

    struct PM_buf_63_iearth
    {
        struct PM_buf_63_signals_header header;

        struct PM_buf_63_iearth_signals
        {
            float i_earth[PM_BUF_63_IEARTH_LEN];
        } signals;
    } iearth;
};

// PM_buf union to work around strict aliasing rules

union  PM_buf_63_union
{
    char             buffer[PM_BUF_63_SIZE];
    struct PM_buf_63 self;
};

// Data converted structure

struct PM_buf_63_converted
{
    // Event log arrays

    int64_t      evtlog_timestamps_ns[PM_BUF_63_EVTLOG_LEN];
    char         evtlog_property     [PM_BUF_63_EVTLOG_LEN][PM_BUF_63_EVTLOG_PROPERTY_LEN+1];
    char         evtlog_symbol       [PM_BUF_63_EVTLOG_LEN][PM_BUF_63_EVTLOG_SYMBOL_LEN+1];
    char         evtlog_action       [PM_BUF_63_EVTLOG_LEN][PM_BUF_63_EVTLOG_ACTION_LEN+1];
    char       * evtlog_property_ptr [PM_BUF_63_EVTLOG_LEN];
    char       * evtlog_symbol_ptr   [PM_BUF_63_EVTLOG_LEN];
    char       * evtlog_action_ptr   [PM_BUF_63_EVTLOG_LEN];
    const char * evtlog_status_ptr   [PM_BUF_63_EVTLOG_LEN];

    // Liblog timestamp arrays

    int64_t iab_timestamps_ns[PM_BUF_63_IAB_LEN];
    int64_t iloop_timestamps_ns[PM_BUF_63_ILOOP_LEN];
    int64_t iearth_timestamps_ns[PM_BUF_63_IEARTH_LEN];

} pm_buf_63_converted;

// Event log status strings

enum PM_buf_63_evtlog_status
{
    PM_BUF_63_EVTLOG_STATUS_DROPPED                   ,
    PM_BUF_63_EVTLOG_STATUS_NORMAL                    ,
    PM_BUF_63_EVTLOG_STATUS_NORMAL_FILTERED           ,
    PM_BUF_63_EVTLOG_STATUS_NORMAL_TO_FREQ            ,
    PM_BUF_63_EVTLOG_STATUS_NORMAL_TO_FREQ_FILTERED   ,
    PM_BUF_63_EVTLOG_STATUS_NORMAL_FROM_FREQ          ,
    PM_BUF_63_EVTLOG_STATUS_NORMAL_FROM_FREQ_FILTERED ,
    PM_BUF_63_EVTLOG_STATUS_FREQ                      ,
    PM_BUF_63_EVTLOG_STATUS_FREQ_FILTERED             ,
    PM_BUF_63_EVTLOG_STATUS_UNKNOWN                   ,
};

static const char * pm_buf_63_evtlog_status[] =
{
    "DROPPED"                    ,
    "NORMAL"                     ,
    "NORMAL_FILTERED"            ,
    "NORMAL_TO_FREQ"             ,
    "NORMAL_TO_FREQ_FILTERED"    ,
    "NORMAL_FROM_FREQ"           ,
    "NORMAL_FROM_FREQ_FILTERED"  ,
    "FREQ"                       ,
    "FREQ_FILTERED"              ,
    "UNKNOWN"                    ,
};

// PM_BUF Static functions - these must be C functions to be tested by ccrt

static int64_t pm_buf_63_ConvertNsTimestamp(const uint32_t timestamp_s, const uint32_t timestamp_ns)
{
    return (int64_t)timestamp_s * 1000000000LL + (int64_t)timestamp_ns;
}



static int64_t pm_buf_63_ConvertUsTimestamp(const uint32_t timestamp_s, const uint32_t timestamp_us)
{
    return (int64_t)timestamp_s * 1000000000LL + (int64_t)timestamp_us * 1000LL;
}



static void pm_buf_63_GenerateTimestamps(struct PM_buf_63_signals_header * const header, int64_t * timestamps_ns, uint32_t num_samples)
{
    int64_t       first_sample_time_ns = pm_buf_63_ConvertNsTimestamp(header->first_sample_time_s, header->first_sample_time_ns);
    const int64_t period_ns = (int64_t)header->period_s * 1000000000LL + (int64_t)header->period_ns;

    while(num_samples--)
    {
        *(timestamps_ns++)    = first_sample_time_ns;
        first_sample_time_ns += period_ns;
    }
}



static void pm_buf_63_ConvertEvtlog(struct PM_buf_63 * const pm_buf)
{
    for(uint32_t i = 0; i < PM_BUF_63_EVTLOG_LEN; i++)
    {
        pm_buf_63_converted.evtlog_timestamps_ns[i] = pm_buf_63_ConvertUsTimestamp(ntohl(pm_buf->evtlog[i].timestamp_s), ntohl(pm_buf->evtlog[i].timestamp_us));
        pm_buf_63_converted.evtlog_property_ptr [i] = (char *)memcpy(pm_buf_63_converted.evtlog_property[i],pm_buf->evtlog[i].property, PM_BUF_63_EVTLOG_PROPERTY_LEN);
        pm_buf_63_converted.evtlog_symbol_ptr   [i] = (char *)memcpy(pm_buf_63_converted.evtlog_symbol  [i],pm_buf->evtlog[i].symbol,   PM_BUF_63_EVTLOG_SYMBOL_LEN);
        pm_buf_63_converted.evtlog_action_ptr   [i] = (char *)memcpy(pm_buf_63_converted.evtlog_action  [i],pm_buf->evtlog[i].action,   PM_BUF_63_EVTLOG_ACTION_LEN);

        switch(pm_buf->evtlog[i].status)
        {
            case '-': pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_DROPPED];                   break;
            case ' ': pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_NORMAL];                    break;
            case '*': pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_NORMAL_FILTERED];           break;
            case '^': pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_NORMAL_TO_FREQ];            break;
            case 't': pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_NORMAL_TO_FREQ_FILTERED];   break;
            case 'v': pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_NORMAL_FROM_FREQ];          break;
            case 'f': pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_NORMAL_FROM_FREQ_FILTERED]; break;
            case '+': pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_FREQ];                      break;
            case '#': pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_FREQ_FILTERED];             break;
            default : pm_buf_63_converted.evtlog_status_ptr[i] = pm_buf_63_evtlog_status[PM_BUF_63_EVTLOG_STATUS_UNKNOWN];                   break;
        }
    }
}



static void pm_buf_63_ConvertLiblog(struct PM_buf_63 * const pm_buf)
{
    pm_buf_63_GenerateTimestamps(&pm_buf->iab.header, pm_buf_63_converted.iab_timestamps_ns, PM_BUF_63_IAB_LEN);
    pm_buf_63_GenerateTimestamps(&pm_buf->iloop.header, pm_buf_63_converted.iloop_timestamps_ns, PM_BUF_63_ILOOP_LEN);
    pm_buf_63_GenerateTimestamps(&pm_buf->iearth.header, pm_buf_63_converted.iearth_timestamps_ns, PM_BUF_63_IEARTH_LEN);
}


// PMD Functions - these use C++ features and are not tested by ccrt

#ifndef PM_BUF_NO_PMD_FUNCTIONS

static void pmd_63AddFgc(PMData &pmdata)
{
    // Use a union to circumvent strict-aliasing warning to cast to the PM_buf_63 structure pointer

    union  PM_buf_63_union * const self_union = (union PM_buf_63_union *)pm_globals.buffer.self.std.fgc_buffers;
    struct PM_buf_63       * const self       = &self_union->self;

    // Check the data version (always in network byte order)

    if(ntohl(self->version) != PM_BUF_63_VERSION)
    {
        throw std::runtime_error("FGC's LOG.PM.BUF data version is not compatible with FGCD (1 expected)");
    }

    // Reset the converted data arrays

    memset(&pm_buf_63_converted, 0, sizeof(struct PM_buf_63_converted));

    // Convert event log data

    pm_buf_63_ConvertEvtlog(self);

    // Convert liblog data

    pm_buf_63_ConvertLiblog(self);

    // Submit event log data to PM system - evtlog timestamps are always in network byte order

    pmdata.registerArray("EVENTS.TIMESTAMP", pm_buf_63_converted.evtlog_timestamps_ns, PM_BUF_63_EVTLOG_LEN);

    pmdata.registerArray("EVENTS.PROPERTY", const_cast<const char **>(pm_buf_63_converted.evtlog_property_ptr), PM_BUF_63_EVTLOG_LEN);
    pmdata.addAttribute ("EVENTS.PROPERTY", "timescale", "EVENTS");
    pmdata.addAttribute ("EVENTS.PROPERTY", "timestamps", "EVENTS.TIMESTAMP",true);

    pmdata.registerArray("EVENTS.SYMBOL", const_cast<const char **>(pm_buf_63_converted.evtlog_symbol_ptr), PM_BUF_63_EVTLOG_LEN);
    pmdata.addAttribute ("EVENTS.SYMBOL", "timescale", "EVENTS");
    pmdata.addAttribute ("EVENTS.SYMBOL", "timestamps", "EVENTS.TIMESTAMP",true);

    pmdata.registerArray("EVENTS.ACTION", const_cast<const char **>(pm_buf_63_converted.evtlog_action_ptr), PM_BUF_63_EVTLOG_LEN);
    pmdata.addAttribute ("EVENTS.ACTION", "timescale", "EVENTS");
    pmdata.addAttribute ("EVENTS.ACTION", "timestamps", "EVENTS.TIMESTAMP",true);

    pmdata.registerArray("EVENTS.STATUS", const_cast<const char **>(pm_buf_63_converted.evtlog_status_ptr), PM_BUF_63_EVTLOG_LEN);
    pmdata.addAttribute ("EVENTS.STATUS", "timescale", "EVENTS");
    pmdata.addAttribute ("EVENTS.STATUS", "timestamps", "EVENTS.TIMESTAMP",true);

    // Submit liblog data to PM system - the data is always in little endian byte order

    pmdata.registerArray("IAB.TIMESTAMP", pm_buf_63_converted.iab_timestamps_ns, PM_BUF_63_IAB_LEN);

    pmdata.registerArray("IAB.I_A", self->iab.signals.i_a, PM_BUF_63_IAB_LEN);
    pmdata.addAttribute ("IAB.I_A", "timescale", "IAB");
    pmdata.addAttribute ("IAB.I_A", "timestamps", "IAB.TIMESTAMP", true);
    pmdata.addUnits     ("IAB.I_A", "A");

    pmdata.registerArray("IAB.I_B", self->iab.signals.i_b, PM_BUF_63_IAB_LEN);
    pmdata.addAttribute ("IAB.I_B", "timescale", "IAB");
    pmdata.addAttribute ("IAB.I_B", "timestamps", "IAB.TIMESTAMP", true);
    pmdata.addUnits     ("IAB.I_B", "A");

    pmdata.registerArray("ILOOP.TIMESTAMP", pm_buf_63_converted.iloop_timestamps_ns, PM_BUF_63_ILOOP_LEN);

    pmdata.registerArray("ILOOP.I_MEAS", self->iloop.signals.i_meas, PM_BUF_63_ILOOP_LEN);
    pmdata.addAttribute ("ILOOP.I_MEAS", "timescale", "ILOOP");
    pmdata.addAttribute ("ILOOP.I_MEAS", "timestamps", "ILOOP.TIMESTAMP", true);
    pmdata.addUnits     ("ILOOP.I_MEAS", "A");

    pmdata.registerArray("ILOOP.I_REF", self->iloop.signals.i_ref, PM_BUF_63_ILOOP_LEN);
    pmdata.addAttribute ("ILOOP.I_REF", "timescale", "ILOOP");
    pmdata.addAttribute ("ILOOP.I_REF", "timestamps", "ILOOP.TIMESTAMP", true);
    pmdata.addUnits     ("ILOOP.I_REF", "A");

    pmdata.registerArray("ILOOP.V_REF", self->iloop.signals.v_ref, PM_BUF_63_ILOOP_LEN);
    pmdata.addAttribute ("ILOOP.V_REF", "timescale", "ILOOP");
    pmdata.addAttribute ("ILOOP.V_REF", "timestamps", "ILOOP.TIMESTAMP", true);
    pmdata.addUnits     ("ILOOP.V_REF", "V");

    pmdata.registerArray("ILOOP.V_MEAS", self->iloop.signals.v_meas, PM_BUF_63_ILOOP_LEN);
    pmdata.addAttribute ("ILOOP.V_MEAS", "timescale", "ILOOP");
    pmdata.addAttribute ("ILOOP.V_MEAS", "timestamps", "ILOOP.TIMESTAMP", true);
    pmdata.addUnits     ("ILOOP.V_MEAS", "V");

    pmdata.registerArray("IEARTH.TIMESTAMP", pm_buf_63_converted.iearth_timestamps_ns, PM_BUF_63_IEARTH_LEN);

    pmdata.registerArray("IEARTH.I_EARTH", self->iearth.signals.i_earth, PM_BUF_63_IEARTH_LEN);
    pmdata.addAttribute ("IEARTH.I_EARTH", "timescale", "IEARTH");
    pmdata.addAttribute ("IEARTH.I_EARTH", "timestamps", "IEARTH.TIMESTAMP", true);
    pmdata.addUnits     ("IEARTH.I_EARTH", "A");

};

#endif // PM_BUF_NO_FUNCTIONS

// EOF
