//! @file  inc_cclibs/logStructsInit.h
//!
//! @brief Converter Control Logging library : Structures initialization header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! This value is used as an analog signal source when it's marked as NULL in the definition file.

static cc_float const dummy_zero_float_value = 0.0F;

//! This value is used as a digital signal source when it's marked as NULL in the definition file.

static bool const dummy_zero_bool_value = false;

//! Declare static array of pointers to the log signal names and units

static char const log_sig_names_and_units[] =
    ".V_ADC_A:V.V_ADC_B:V.V_ADC_C:V.V_ADC_D:V.I_DCCT_A:A.I_DCCT_B:A.I_DIFF:A.DSP_RT_PROF_0:us.DSP_RT_PROF_1:us.DSP_RT_CPU:us;"
    ".B_MEAS:G.B_MEAS_FLTR:G.B_MEAS_EXTR:G.B_REF_DELAYED:G.B_ERR:G.B_MAX_ABS_ERR:G.B_MEAS_SIM:G.I_MEAS:A.V_REF:V.V_MEAS:V;"
    ".I_MEAS:A.I_MEAS_FLTR:A.I_MEAS_EXTR:A.I_REF_DELAYED:A.I_ERR:A.I_MAX_ABS_ERR:A.I_MEAS_SIM:A.I_RMS:A.I_RMS_LOAD:A.V_REF:V.V_MEAS:V.I_CAPA_MEAS:A.B_MEAS:G;"
    ".B_MEAS_REG:G.B_MEAS_RATE:G/s.B_REF_DIRECT:G.B_REF_USER:G.B_REF_ADV:G.B_REF_LIMITED:G.B_REF_CLOSED:G.B_REF_OPEN:G.B_REF_RATE:G/s.B_REF_ILC:G.B_ERR_ILC:G.B_ERR:G.B_TRACK_DELAY:Periods.B_RMS_ERR:G.V_REF:V.V_MEAS:V.FG_TIME:s.FG_TIME_ADV:s.REF_ADVANCE:s;"
    ".I_MEAS_REG:A.I_MEAS_RATE:A/s.I_REF_DIRECT:A.I_REF_USER:A.I_REF_ADV:A.I_REF_LIMITED:A.I_REF_CLOSED:A.I_REF_OPEN:A.I_REF_RATE:A/s.I_REF_ILC:A.I_ERR_ILC:A.I_ERR:A.I_TRACK_DELAY:Periods.I_RMS_ERR:A.SAT_FACTOR.I_MAG_SAT:A.MEAS_OHMS.MEAS_HENRYS.HENRYS_SAT.POWER:W.V_REF:V.V_MEAS:V.FG_TIME:s.FG_TIME_ADV:s.REF_ADVANCE:s;"
    ".V_MEAS:V.V_REF_REG:V.V_REF_SAT:V.V_REF_FF:V.V_REF_DECO:V.V_REF:V.V_REF_VS:V.V_REF_DAC:V.V_REF_RATE:V/s.V_RATE_RMS:V/s.V_FEEDFWD:V.V_FF:V.V_FF_FLTR:V.V_HARMONICS:V.V_ERR:V.V_MAX_ABS_ERR:V.V_MEAS_SIM:V.FG_REF.RT_REF.B_MEAS:G.I_MEAS:A;"
    ".V_MEAS:V.V_MEAS_REG:V.V_REG_ERR:V.V_INTEGRATOR:V.D_REF:V.D_MEAS:V.F_REF_REG:V.F_REF_LIMITED:V.F_REF:V.I_CAPA_REG:A.I_CAPA:A.I_CAPA_MEAS:A.I_CAPA_SIM:A;"
    ".I_REF:A.I_MEAS:A;"
    ".I_EARTH:A;"
    ".T_FGC_IN:C.T_FGC_OUT:C.T_INTERNAL:C.T_DCCT_A:C.T_DCCT_B:C.V_AC_FREQUENCY:HZ;"
    ".B_MEAS_VALID.I_MEAS_VALID.I_MEAS_ZERO.I_DIFF_WRN.I_DIFF_FLT.V_MEAS_VALID.I_CAPA_VALID.V_FEEDFWD_VALID.FORCE_OPENLOOP.REG_OPENLOOP.ENABLE_REG_ERR.EXT_EVT_RCVD.EXT_EVT_OK.INTERNAL_EVT.START_FUNC.USE_ARM_NOW.TEST_RST.ILC_ERR.ILC_CALC.ILC_RUN.ECO_ALLOWED.FULL_ECO_REQ.ONCE_ECO_REQ.DYN_ECO_REQ.DYN_ECO_ARM_WRN.DYN_ECO_RUN_WRN;"
    ".CH1_STATE_OK.CH1_REG_ERR_OK.CH1_V_MEAS_OK.CH1_V_RATE_OK.CH1_I_MEAS_OK.CH1_I_RATE_OK.CH1_TESTS_OK.CH1_CHAN_OK.CH2_STATE_OK.CH2_REG_ERR_OK.CH2_V_MEAS_OK.CH2_V_RATE_OK.CH2_I_MEAS_OK.CH2_I_RATE_OK.CH2_TESTS_OK.CH2_CHAN_OK.CH3_STATE_OK.CH3_REG_ERR_OK.CH3_V_MEAS_OK.CH3_V_RATE_OK.CH3_I_MEAS_OK.CH3_I_RATE_OK.CH3_TESTS_OK.CH3_CHAN_OK.CH4_STATE_OK.CH4_REG_ERR_OK.CH4_V_MEAS_OK.CH4_V_RATE_OK.CH4_I_MEAS_OK.CH4_I_RATE_OK.CH4_TESTS_OK.CH4_CHAN_OK;"
    ".CH1_MAX_REG_ERR.CH1_MIN_V_MEAS.CH1_MAX_V_MEAS.CH1_MAX_V_RATE.CH1_MIN_I_MEAS.CH1_MAX_I_MEAS.CH1_MAX_I_RATE.CH2_MAX_REG_ERR.CH2_MIN_V_MEAS.CH2_MAX_V_MEAS.CH2_MAX_V_RATE.CH2_MIN_I_MEAS.CH2_MAX_I_MEAS.CH2_MAX_I_RATE.CH3_MAX_REG_ERR.CH3_MIN_V_MEAS.CH3_MAX_V_MEAS.CH3_MAX_V_RATE.CH3_MIN_I_MEAS.CH3_MAX_I_MEAS.CH3_MAX_I_RATE.CH4_MAX_REG_ERR.CH4_MIN_V_MEAS.CH4_MAX_V_MEAS.CH4_MAX_V_RATE.CH4_MIN_I_MEAS.CH4_MAX_I_MEAS.CH4_MAX_I_RATE;"
    ".RMS_ERR_ILC.INIT_RMS_ERR_ILC.RMS_V_RATE:V/s.RMS_V_RATE_LIM:V/s;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
    ".ANA_A.ANA_B.ANA_C.ANA_D;"
;


//! Initialize log structures per device
//!
//! @param[in]    log_mgr        Pointer to log manager structure to initialize
//! @param[in]    log_structs    Pointer to log structs structure to initialize
//! @param[in]    log_buffers    Pointer to log buffers structure
//! @param[in]    device_index   Hook for calling application, to allow initialization of multiple devices

static void logStructsInitDevice(struct LOG_mgr     * const log_mgr,
                                 struct LOG_structs * const log_structs,
                                 struct LOG_buffers * const log_buffers,
                                 uint32_t             const device_index)
{
    // Reset log structures

    memset(log_mgr,     0, sizeof(*log_mgr));
    memset(log_structs, 0, sizeof(*log_structs));
    memset(log_buffers, 0, sizeof(*log_buffers));

    // Initialize log_mgr structure

    log_mgr->num_logs             = 30;
    log_mgr->read_delay_ms        = 5;
    log_mgr->read_rate            = 400000;
    log_mgr->cyclic_logs_mask     = 0x3FFFDDFF;
    log_mgr->analog_logs_mask     = 0x3FFFF3FF;
    log_mgr->postmortem_logs_mask = 0x0000077F;
    log_mgr->continuous_logs_mask = 0x3FFFFFFF;
    log_mgr->freezable_logs_mask  = 0x3FFFC57F;
    log_mgr->running_logs_mask    = 0x3FFFC57F;

    log_mgr->period[LOG_ACQ         ].secs.rel = 0;
    log_mgr->period[LOG_ACQ         ].ns       = 0;
    log_mgr->period[LOG_B_MEAS      ].secs.rel = 0;
    log_mgr->period[LOG_B_MEAS      ].ns       = 0;
    log_mgr->period[LOG_I_MEAS      ].secs.rel = 0;
    log_mgr->period[LOG_I_MEAS      ].ns       = 0;
    log_mgr->period[LOG_B_REG       ].secs.rel = 0;
    log_mgr->period[LOG_B_REG       ].ns       = 0;
    log_mgr->period[LOG_I_REG       ].secs.rel = 0;
    log_mgr->period[LOG_I_REG       ].ns       = 0;
    log_mgr->period[LOG_V_REF       ].secs.rel = 0;
    log_mgr->period[LOG_V_REF       ].ns       = 0;
    log_mgr->period[LOG_V_REG       ].secs.rel = 0;
    log_mgr->period[LOG_V_REG       ].ns       = 0;
    log_mgr->period[LOG_I_1KHZ      ].secs.rel = 0;
    log_mgr->period[LOG_I_1KHZ      ].ns       = 0;
    log_mgr->period[LOG_I_EARTH     ].secs.rel = 0;
    log_mgr->period[LOG_I_EARTH     ].ns       = 0;
    log_mgr->period[LOG_TEMP        ].secs.rel = 0;
    log_mgr->period[LOG_TEMP        ].ns       = 0;
    log_mgr->period[LOG_FLAGS       ].secs.rel = 0;
    log_mgr->period[LOG_FLAGS       ].ns       = 0;
    log_mgr->period[LOG_BIS_FLAGS   ].secs.rel = 0;
    log_mgr->period[LOG_BIS_FLAGS   ].ns       = 0;
    log_mgr->period[LOG_BIS_LIMITS  ].secs.rel = 0;
    log_mgr->period[LOG_BIS_LIMITS  ].ns       = 0;
    log_mgr->period[LOG_ILC_CYC     ].secs.rel = 0;
    log_mgr->period[LOG_ILC_CYC     ].ns       = 0;
    log_mgr->period[LOG_DIM         ].secs.rel = 0;
    log_mgr->period[LOG_DIM         ].ns       = 0;
    log_mgr->period[LOG_DIM2        ].secs.rel = 0;
    log_mgr->period[LOG_DIM2        ].ns       = 0;
    log_mgr->period[LOG_DIM3        ].secs.rel = 0;
    log_mgr->period[LOG_DIM3        ].ns       = 0;
    log_mgr->period[LOG_DIM4        ].secs.rel = 0;
    log_mgr->period[LOG_DIM4        ].ns       = 0;
    log_mgr->period[LOG_DIM5        ].secs.rel = 0;
    log_mgr->period[LOG_DIM5        ].ns       = 0;
    log_mgr->period[LOG_DIM6        ].secs.rel = 0;
    log_mgr->period[LOG_DIM6        ].ns       = 0;
    log_mgr->period[LOG_DIM7        ].secs.rel = 0;
    log_mgr->period[LOG_DIM7        ].ns       = 0;
    log_mgr->period[LOG_DIM8        ].secs.rel = 0;
    log_mgr->period[LOG_DIM8        ].ns       = 0;
    log_mgr->period[LOG_DIM9        ].secs.rel = 0;
    log_mgr->period[LOG_DIM9        ].ns       = 0;
    log_mgr->period[LOG_DIM10       ].secs.rel = 0;
    log_mgr->period[LOG_DIM10       ].ns       = 0;
    log_mgr->period[LOG_DIM11       ].secs.rel = 0;
    log_mgr->period[LOG_DIM11       ].ns       = 0;
    log_mgr->period[LOG_DIM12       ].secs.rel = 0;
    log_mgr->period[LOG_DIM12       ].ns       = 0;
    log_mgr->period[LOG_DIM13       ].secs.rel = 0;
    log_mgr->period[LOG_DIM13       ].ns       = 0;
    log_mgr->period[LOG_DIM14       ].secs.rel = 0;
    log_mgr->period[LOG_DIM14       ].ns       = 0;
    log_mgr->period[LOG_DIM15       ].secs.rel = 0;
    log_mgr->period[LOG_DIM15       ].ns       = 0;
    log_mgr->period[LOG_DIM16       ].secs.rel = 0;
    log_mgr->period[LOG_DIM16       ].ns       = 0;


    // Initialize log structure array

    struct LOG_log     * log  = log_structs->log;
    struct LOG_private * priv = log_buffers->priv;

    // Log 1 in log_struct.log[0]: ACQ - acquisition log

    priv->name                    = "ACQ";
    priv->sig_bufs_offset         = 0;
    priv->sig_bufs_len            = 480000;
    priv->num_postmortem_sig_bufs = LOG_ACQ_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 20000;
    priv->num_signals             = LOG_ACQ_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->acq.signals[0];
    priv->ad.analog.selectors     = log_buffers->acq.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_ACQ;
    log->num_cycles               = LOG_ACQ_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_ACQ);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_ACQ_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 120000;
    log->last_sample_marker.index = 119999;
    log->sig_bufs                 = &log_buffers->buffers[0];
    log->cycles                   = log_buffers->acq.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->acq[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 10000;

    priv->ad.analog.selectors[ 0] =  4;  // I_DCCT_A;
    priv->ad.analog.selectors[ 1] =  5;  // I_DCCT_B;
    priv->ad.analog.selectors[ 2] =  7;  // DSP_RT_PROF_0;
    priv->ad.analog.selectors[ 3] =  8;  // DSP_RT_PROF_1;
    priv->ad.analog.signals[ 0].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), adc, adc_a, ADC_MEAS_UNFILTERED);
    priv->ad.analog.signals[ 1].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), adc, adc_b, ADC_MEAS_UNFILTERED);
    priv->ad.analog.signals[ 2].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), adc, adc_c, ADC_MEAS_UNFILTERED);
    priv->ad.analog.signals[ 3].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), adc, adc_d, ADC_MEAS_UNFILTERED);
    priv->ad.analog.signals[ 4].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), transducer, dcct_a, TRANSDUCER_MEAS_UNFILTERED);
    priv->ad.analog.signals[ 5].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), transducer, dcct_b, TRANSDUCER_MEAS_UNFILTERED);
    priv->ad.analog.signals[ 6].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), select, i_meas, SELECT_ABS_DIFF);
    priv->ad.analog.signals[ 6].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 7].source = &dummy_float;
    priv->ad.analog.signals[ 7].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 8].source = &dummy_float;
    priv->ad.analog.signals[ 8].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 9].source = &dummy_float;
    priv->ad.analog.signals[ 9].meta   = SPY_SIG_META_STEPS;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 2 in log_struct.log[1]: B_MEAS - field measurement log

    priv->name                    = "B_MEAS";
    priv->sig_bufs_offset         = 480000;
    priv->sig_bufs_len            = 2800000;
    priv->num_postmortem_sig_bufs = LOG_B_MEAS_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 20000;
    priv->num_signals             = LOG_B_MEAS_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->b_meas.signals[0];
    priv->ad.analog.selectors     = log_buffers->b_meas.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_B_MEAS;
    log->num_cycles               = LOG_B_MEAS_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_B_MEAS);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_B_MEAS_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 400000;
    log->last_sample_marker.index = 399999;
    log->sig_bufs                 = &log_buffers->buffers[480000];
    log->cycles                   = log_buffers->b_meas.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->b_meas[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 10000;

    priv->ad.analog.selectors[ 0] =  0;  // B_MEAS;
    priv->ad.analog.selectors[ 1] =  1;  // B_MEAS_FLTR;
    priv->ad.analog.selectors[ 2] =  4;  // B_ERR;
    priv->ad.analog.selectors[ 3] =  0;  // B_MEAS;
    priv->ad.analog.selectors[ 4] =  3;  // B_REF_DELAYED;
    priv->ad.analog.selectors[ 5] =  8;  // V_REF;
    priv->ad.analog.selectors[ 6] =  9;  // V_MEAS;
    priv->ad.analog.signals[ 0].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_B_UNFILTERED);
    priv->ad.analog.signals[ 1].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_B_FILTERED);
    priv->ad.analog.signals[ 2].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_B_EXTRAPOLATED);
    priv->ad.analog.signals[ 3].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_REF_DELAYED);
    priv->ad.analog.signals[ 3].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 4].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_ERR);
    priv->ad.analog.signals[ 4].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 5].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_MAX_ABS_ERR);
    priv->ad.analog.signals[ 5].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 6].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), SIM_B_MEAS);
    priv->ad.analog.signals[ 7].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_I_UNFILTERED);
    priv->ad.analog.signals[ 8].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF);
    priv->ad.analog.signals[ 8].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 9].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_V_UNFILTERED);

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 3 in log_struct.log[2]: I_MEAS - current measurement log

    priv->name                    = "I_MEAS";
    priv->sig_bufs_offset         = 3280000;
    priv->sig_bufs_len            = 2800000;
    priv->num_postmortem_sig_bufs = LOG_I_MEAS_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 20000;
    priv->num_signals             = LOG_I_MEAS_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->i_meas.signals[0];
    priv->ad.analog.selectors     = log_buffers->i_meas.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_I_MEAS;
    log->num_cycles               = LOG_I_MEAS_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_I_MEAS);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_I_MEAS_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 400000;
    log->last_sample_marker.index = 399999;
    log->sig_bufs                 = &log_buffers->buffers[3280000];
    log->cycles                   = log_buffers->i_meas.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->i_meas[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 10000;

    priv->ad.analog.selectors[ 0] =  0;  // I_MEAS;
    priv->ad.analog.selectors[ 1] =  3;  // I_REF_DELAYED;
    priv->ad.analog.selectors[ 2] =  4;  // I_ERR;
    priv->ad.analog.selectors[ 3] =  0;  // I_MEAS;
    priv->ad.analog.selectors[ 4] =  3;  // I_REF_DELAYED;
    priv->ad.analog.selectors[ 5] =  9;  // V_REF;
    priv->ad.analog.selectors[ 6] = 10;  // V_MEAS;
    priv->ad.analog.signals[ 0].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_I_UNFILTERED);
    priv->ad.analog.signals[ 1].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_I_FILTERED);
    priv->ad.analog.signals[ 2].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_I_EXTRAPOLATED);
    priv->ad.analog.signals[ 3].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_REF_DELAYED);
    priv->ad.analog.signals[ 3].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 4].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_ERR);
    priv->ad.analog.signals[ 4].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 5].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_MAX_ABS_ERR);
    priv->ad.analog.signals[ 5].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 6].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), SIM_I_MEAS);
    priv->ad.analog.signals[ 7].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_RMS);
    priv->ad.analog.signals[ 8].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_RMS_LOAD);
    priv->ad.analog.signals[ 9].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF);
    priv->ad.analog.signals[ 9].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[10].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_V_UNFILTERED);
    priv->ad.analog.signals[11].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_I_CAPA_UNFILTERED);
    priv->ad.analog.signals[12].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_B_UNFILTERED);

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 4 in log_struct.log[3]: B_REG - field regulation log

    priv->name                    = "B_REG";
    priv->sig_bufs_offset         = 6080000;
    priv->sig_bufs_len            = 1440000;
    priv->num_postmortem_sig_bufs = LOG_B_REG_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 20000;
    priv->num_signals             = LOG_B_REG_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->b_reg.signals[0];
    priv->ad.analog.selectors     = log_buffers->b_reg.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_B_REG;
    log->num_cycles               = LOG_B_REG_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_B_REG);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_B_REG_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 120000;
    log->last_sample_marker.index = 119999;
    log->sig_bufs                 = &log_buffers->buffers[6080000];
    log->cycles                   = log_buffers->b_reg.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->b_reg[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 100;

    priv->ad.analog.selectors[ 0] =  0;  // B_MEAS_REG;
    priv->ad.analog.selectors[ 1] =  4;  // B_REF_ADV;
    priv->ad.analog.selectors[ 2] =  5;  // B_REF_LIMITED;
    priv->ad.analog.selectors[ 3] =  6;  // B_REF_CLOSED;
    priv->ad.analog.selectors[ 4] =  7;  // B_REF_OPEN;
    priv->ad.analog.selectors[ 5] = 11;  // B_ERR;
    priv->ad.analog.selectors[ 6] = 14;  // V_REF;
    priv->ad.analog.selectors[ 7] = 15;  // V_MEAS;
    priv->ad.analog.selectors[ 8] =  0;  // B_MEAS_REG;
    priv->ad.analog.selectors[ 9] =  3;  // B_REF_USER;
    priv->ad.analog.selectors[10] = 14;  // V_REF;
    priv->ad.analog.selectors[11] = 15;  // V_MEAS;
    priv->ad.analog.signals[ 0].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_MEAS_REG);
    priv->ad.analog.signals[ 1].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_B_RATE);
    priv->ad.analog.signals[ 2].source = refMgrParPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), DIRECT_B_REF);
    priv->ad.analog.signals[ 2].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 3].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_REF_ADV);
    priv->ad.analog.signals[ 3].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 3].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), BREG_OP_REF_ADVANCE);
    priv->ad.analog.signals[ 4].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_REF_ADV);
    priv->ad.analog.signals[ 4].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 5].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_REF_LIMITED);
    priv->ad.analog.signals[ 5].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 6].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_REF_CLOSED);
    priv->ad.analog.signals[ 6].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 6].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), BREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[ 7].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_REF_OPEN);
    priv->ad.analog.signals[ 7].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 7].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), BREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[ 8].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_REF_RATE);
    priv->ad.analog.signals[ 8].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 8].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), BREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[ 9].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_REF);
    priv->ad.analog.signals[ 9].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[10].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_ERR);
    priv->ad.analog.signals[10].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[11].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_ERR);
    priv->ad.analog.signals[11].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[12].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_TRACK_DELAY_PERIODS);
    priv->ad.analog.signals[12].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[13].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), B_RMS_ERR);
    priv->ad.analog.signals[13].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[13].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), BREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[14].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF);
    priv->ad.analog.signals[14].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[14].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), BREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[15].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_V_UNFILTERED);
    priv->ad.analog.signals[16].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_FG_TIME);
    priv->ad.analog.signals[16].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[16].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), BREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[17].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_FG_TIME_ADV);
    priv->ad.analog.signals[17].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[17].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), BREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[18].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), REF_ADVANCE);
    priv->ad.analog.signals[18].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[18].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), BREG_LOG_TIME_OFFSET);

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 5 in log_struct.log[4]: I_REG - current regulation log

    priv->name                    = "I_REG";
    priv->sig_bufs_offset         = 7520000;
    priv->sig_bufs_len            = 2400000;
    priv->num_postmortem_sig_bufs = LOG_I_REG_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 20000;
    priv->num_signals             = LOG_I_REG_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->i_reg.signals[0];
    priv->ad.analog.selectors     = log_buffers->i_reg.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_I_REG;
    log->num_cycles               = LOG_I_REG_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_I_REG);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_I_REG_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 200000;
    log->last_sample_marker.index = 199999;
    log->sig_bufs                 = &log_buffers->buffers[7520000];
    log->cycles                   = log_buffers->i_reg.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->i_reg[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 100;

    priv->ad.analog.selectors[ 0] =  0;  // I_MEAS_REG;
    priv->ad.analog.selectors[ 1] =  4;  // I_REF_ADV;
    priv->ad.analog.selectors[ 2] =  5;  // I_REF_LIMITED;
    priv->ad.analog.selectors[ 3] =  6;  // I_REF_CLOSED;
    priv->ad.analog.selectors[ 4] =  7;  // I_REF_OPEN;
    priv->ad.analog.selectors[ 5] = 11;  // I_ERR;
    priv->ad.analog.selectors[ 6] = 20;  // V_REF;
    priv->ad.analog.selectors[ 7] = 21;  // V_MEAS;
    priv->ad.analog.selectors[ 8] =  0;  // I_MEAS_REG;
    priv->ad.analog.selectors[ 9] =  3;  // I_REF_USER;
    priv->ad.analog.selectors[10] = 20;  // V_REF;
    priv->ad.analog.selectors[11] = 21;  // V_MEAS;
    priv->ad.analog.signals[ 0].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_MEAS_REG);
    priv->ad.analog.signals[ 1].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_I_RATE);
    priv->ad.analog.signals[ 2].source = refMgrParPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), DIRECT_I_REF);
    priv->ad.analog.signals[ 2].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 3].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_REF_ADV);
    priv->ad.analog.signals[ 3].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 3].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_OP_REF_ADVANCE);
    priv->ad.analog.signals[ 4].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_REF_ADV);
    priv->ad.analog.signals[ 4].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 5].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_REF_LIMITED);
    priv->ad.analog.signals[ 5].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 6].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_REF_CLOSED);
    priv->ad.analog.signals[ 6].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 6].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[ 7].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_REF_OPEN);
    priv->ad.analog.signals[ 7].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 7].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[ 8].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_REF_RATE);
    priv->ad.analog.signals[ 8].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 8].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[ 9].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_REF);
    priv->ad.analog.signals[ 9].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[10].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_ERR);
    priv->ad.analog.signals[10].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[11].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_ERR);
    priv->ad.analog.signals[11].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[12].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_TRACK_DELAY_PERIODS);
    priv->ad.analog.signals[12].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[13].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_RMS_ERR);
    priv->ad.analog.signals[13].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[13].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[14].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), LOAD_SAT_FACTOR);
    priv->ad.analog.signals[14].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[15].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), LOAD_I_MAG_SAT);
    priv->ad.analog.signals[16].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), LOAD_MEAS_OHMS);
    priv->ad.analog.signals[17].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), LOAD_MEAS_HENRYS);
    priv->ad.analog.signals[18].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), LOAD_MEAS_HENRYS_SAT);
    priv->ad.analog.signals[19].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_POWER);
    priv->ad.analog.signals[20].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF);
    priv->ad.analog.signals[20].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[20].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[21].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_V_UNFILTERED);
    priv->ad.analog.signals[22].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_FG_TIME);
    priv->ad.analog.signals[22].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[22].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[23].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_FG_TIME_ADV);
    priv->ad.analog.signals[23].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[23].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_LOG_TIME_OFFSET);
    priv->ad.analog.signals[24].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), REF_ADVANCE);
    priv->ad.analog.signals[24].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[24].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_LOG_TIME_OFFSET);

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 6 in log_struct.log[5]: V_REF - voltage reference log

    priv->name                    = "V_REF";
    priv->sig_bufs_offset         = 9920000;
    priv->sig_bufs_len            = 2800000;
    priv->num_postmortem_sig_bufs = LOG_V_REF_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 20000;
    priv->num_signals             = LOG_V_REF_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->v_ref.signals[0];
    priv->ad.analog.selectors     = log_buffers->v_ref.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_V_REF;
    log->num_cycles               = LOG_V_REF_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_V_REF);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_V_REF_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 400000;
    log->last_sample_marker.index = 399999;
    log->sig_bufs                 = &log_buffers->buffers[9920000];
    log->cycles                   = log_buffers->v_ref.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->v_ref[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 100;

    priv->ad.analog.selectors[ 0] =  0;  // V_MEAS;
    priv->ad.analog.selectors[ 1] =  1;  // V_REF_REG;
    priv->ad.analog.selectors[ 2] =  2;  // V_REF_SAT;
    priv->ad.analog.selectors[ 3] =  5;  // V_REF;
    priv->ad.analog.selectors[ 4] = 16;  // V_MEAS_SIM;
    priv->ad.analog.selectors[ 5] =  0;  // V_MEAS;
    priv->ad.analog.selectors[ 6] =  5;  // V_REF;
    priv->ad.analog.signals[ 0].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_V_UNFILTERED);
    priv->ad.analog.signals[ 1].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF_REG);
    priv->ad.analog.signals[ 1].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 2].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF_SAT);
    priv->ad.analog.signals[ 2].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 3].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF_FF);
    priv->ad.analog.signals[ 3].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 4].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF_DECO);
    priv->ad.analog.signals[ 4].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 5].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF);
    priv->ad.analog.signals[ 5].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 6].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF_VS);
    priv->ad.analog.signals[ 6].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 7].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_DAC);
    priv->ad.analog.signals[ 7].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 8].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_REF_RATE);
    priv->ad.analog.signals[ 8].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 9].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_RATE_RMS);
    priv->ad.analog.signals[ 9].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[10].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_VFEEDFWD);
    priv->ad.analog.signals[10].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[11].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_FF);
    priv->ad.analog.signals[11].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[12].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_FF_FLTR);
    priv->ad.analog.signals[12].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[13].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_HARMONICS);
    priv->ad.analog.signals[13].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[14].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_ERR);
    priv->ad.analog.signals[14].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[15].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), V_MAX_ABS_ERR);
    priv->ad.analog.signals[15].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[16].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), SIM_V_MEAS);
    priv->ad.analog.signals[17].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_FG);
    priv->ad.analog.signals[17].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[18].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_RT);
    priv->ad.analog.signals[18].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[19].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_B_UNFILTERED);
    priv->ad.analog.signals[20].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_I_UNFILTERED);

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 7 in log_struct.log[6]: V_REG - voltage regulation log

    priv->name                    = "V_REG";
    priv->sig_bufs_offset         = 12720000;
    priv->sig_bufs_len            = 1200000;
    priv->num_postmortem_sig_bufs = LOG_V_REG_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 20000;
    priv->num_signals             = LOG_V_REG_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->v_reg.signals[0];
    priv->ad.analog.selectors     = log_buffers->v_reg.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_V_REG;
    log->num_cycles               = LOG_V_REG_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_V_REG);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_V_REG_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 200000;
    log->last_sample_marker.index = 199999;
    log->sig_bufs                 = &log_buffers->buffers[12720000];
    log->cycles                   = log_buffers->v_reg.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->v_reg[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 100;

    priv->ad.analog.selectors[ 0] =  6;  // F_REF_REG;
    priv->ad.analog.selectors[ 1] =  7;  // F_REF_LIMITED;
    priv->ad.analog.selectors[ 2] =  8;  // F_REF;
    priv->ad.analog.selectors[ 3] =  1;  // V_MEAS_REG;
    priv->ad.analog.selectors[ 4] =  2;  // V_REG_ERR;
    priv->ad.analog.selectors[ 5] =  9;  // I_CAPA_REG;
    priv->ad.analog.signals[ 0].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_V_UNFILTERED);
    priv->ad.analog.signals[ 1].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), VFILTER_V_MEAS_REG);
    priv->ad.analog.signals[ 2].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), VREG_V_REG_ERR);
    priv->ad.analog.signals[ 2].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 3].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), VREG_V_INTEGRATOR);
    priv->ad.analog.signals[ 3].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 4].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), VFILTER_D_REF);
    priv->ad.analog.signals[ 4].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 5].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), VFILTER_D_MEAS);
    priv->ad.analog.signals[ 6].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), FIRING_F_REF_REG);
    priv->ad.analog.signals[ 6].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 7].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), FIRING_F_REF_LIMITED);
    priv->ad.analog.signals[ 7].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 8].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), FIRING_F_REF);
    priv->ad.analog.signals[ 8].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 9].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), VFILTER_I_CAPA_REG);
    priv->ad.analog.signals[10].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), VFILTER_I_CAPA);
    priv->ad.analog.signals[11].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_I_CAPA_UNFILTERED);
    priv->ad.analog.signals[12].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), SIM_I_CAPA_MEAS);

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 8 in log_struct.log[7]: I_1KHZ - 1 KHZ current log

    priv->name                    = "I_1KHZ";
    priv->sig_bufs_offset         = 13920000;
    priv->sig_bufs_len            = 180000;
    priv->num_postmortem_sig_bufs = LOG_I_1KHZ_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 2000;
    priv->num_signals             = LOG_I_1KHZ_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->i_1khz.signals[0];
    priv->ad.analog.selectors     = log_buffers->i_1khz.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_I_1KHZ;
    log->num_cycles               = LOG_I_1KHZ_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_I_1KHZ);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK;
    log->num_sig_bufs             = LOG_I_1KHZ_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 90000;
    log->last_sample_marker.index = 89999;
    log->sig_bufs                 = &log_buffers->buffers[13920000];
    log->cycles                   = log_buffers->i_1khz.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->i_1khz[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 0;

    priv->ad.analog.selectors[ 0] =  0;  // I_REF;
    priv->ad.analog.selectors[ 1] =  1;  // I_MEAS;
    priv->ad.analog.signals[ 0].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), I_REF_ADV);
    priv->ad.analog.signals[ 0].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 0].time_offset = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), IREG_OP_REF_ADVANCE);
    priv->ad.analog.signals[ 1].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_I_UNFILTERED);

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 9 in log_struct.log[8]: I_EARTH - earth current log

    priv->name                    = "I_EARTH";
    priv->sig_bufs_offset         = 14100000;
    priv->sig_bufs_len            = 8000;
    priv->num_postmortem_sig_bufs = LOG_I_EARTH_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 4000;
    priv->num_signals             = LOG_I_EARTH_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->i_earth.signals[0];
    priv->ad.analog.selectors     = log_buffers->i_earth.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_I_EARTH;
    log->num_cycles               = LOG_I_EARTH_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_I_EARTH);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_I_EARTH_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 8000;
    log->last_sample_marker.index = 7999;
    log->sig_bufs                 = &log_buffers->buffers[14100000];
    log->cycles                   = log_buffers->i_earth.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->i_earth[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // I_EARTH;
    priv->ad.analog.signals[ 0].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 10 in log_struct.log[9]: TEMP - temperature log

    priv->name                    = "TEMP";
    priv->sig_bufs_offset         = 14108000;
    priv->sig_bufs_len            = 60000;
    priv->num_postmortem_sig_bufs = LOG_TEMP_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 360;
    priv->num_signals             = LOG_TEMP_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->temp.signals[0];
    priv->ad.analog.selectors     = log_buffers->temp.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_TEMP;
    log->num_cycles               = LOG_TEMP_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_TEMP);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK;
    log->num_sig_bufs             = LOG_TEMP_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 10000;
    log->last_sample_marker.index = 9999;
    log->sig_bufs                 = &log_buffers->buffers[14108000];
    log->cycles                   = log_buffers->temp.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->temp[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 0;

    priv->ad.analog.selectors[ 0] =  0;  // T_FGC_IN;
    priv->ad.analog.selectors[ 1] =  1;  // T_FGC_OUT;
    priv->ad.analog.selectors[ 2] =  2;  // T_INTERNAL;
    priv->ad.analog.selectors[ 3] =  3;  // T_DCCT_A;
    priv->ad.analog.selectors[ 4] =  4;  // T_DCCT_B;
    priv->ad.analog.selectors[ 5] =  5;  // V_AC_FREQUENCY;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), temp_filter, internal, TEMP_FILTER_TEMP_C);
    priv->ad.analog.signals[ 3].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), temp_filter, dcct_a, TEMP_FILTER_TEMP_C);
    priv->ad.analog.signals[ 4].source = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), temp_filter, dcct_b, TEMP_FILTER_TEMP_C);
    priv->ad.analog.signals[ 5].source = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), MEAS_V_AC_HZ_FLTR);

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 11 in log_struct.log[10]: FLAGS - digital flags log

    priv->name                    = "FLAGS";
    priv->sig_bufs_offset         = 14168000;
    priv->sig_bufs_len            = 400000;
    priv->num_postmortem_sig_bufs = LOG_FLAGS_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 20000;
    priv->num_signals             = LOG_FLAGS_NUM_SIGNALS;
    priv->ad.digital.signals      = log_buffers->flags.signals[0];

    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_FLAGS;
    log->num_cycles               = LOG_FLAGS_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_FLAGS);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_POSTMORTEM_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_FLAGS_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 400000;
    log->last_sample_marker.index = 399999;
    log->sig_bufs                 = &log_buffers->buffers[14168000];
    log->cycles                   = log_buffers->flags.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.digital.signals       = log_structs->flags[0].signals;
    log->ad.digital.num_signals   = LOG_FLAGS_NUM_SIGNALS;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 2000;
    log->ad.digital.signals[ 0].source.boolean = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), FLAG_B_MEAS_VALID);
    log->ad.digital.signals[ 0].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[ 1].source.boolean = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), FLAG_I_MEAS_VALID);
    log->ad.digital.signals[ 1].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[ 2].source.boolean = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), FLAG_I_MEAS_ZERO);
    log->ad.digital.signals[ 2].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[ 3].source.uint32 = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), select, i_meas, SELECT_WARNINGS);
    log->ad.digital.signals[ 3].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[ 3].bit_mask = (1 << SIG_SELECT_DIFF_WARN_BIT);
    log->ad.digital.signals[ 4].source.uint32 = sigVarPointer(&(SHMEM_NONVOL.tenants[device_index].sig_struct), select, i_meas, SELECT_FAULTS);
    log->ad.digital.signals[ 4].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[ 4].bit_mask = (1 << SIG_SELECT_DIFF_FLT_BIT);
    log->ad.digital.signals[ 5].source.boolean = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), FLAG_V_MEAS_VALID);
    log->ad.digital.signals[ 5].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[ 6].source.boolean = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), FLAG_I_CAPA_VALID);
    log->ad.digital.signals[ 6].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[ 7].source.boolean = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), FLAG_VFEEDFWD_VALID);
    log->ad.digital.signals[ 7].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[ 8].source.boolean = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), FLAG_FORCE_OPENLOOP);
    log->ad.digital.signals[ 8].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[ 9].source.boolean = regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), FLAG_OPENLOOP);
    log->ad.digital.signals[ 9].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[10].source.boolean = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), FLAG_ENABLE_REG_ERR);
    log->ad.digital.signals[10].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[11].source.uint8 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), FLAG_EXT_EVT_RCVD);
    log->ad.digital.signals[11].source_type = LOG_DIG_SOURCE_TYPE_UINT8;
    log->ad.digital.signals[11].bit_mask = (1 << 0);
    log->ad.digital.signals[12].source.uint8 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), FLAG_EXT_EVT_OK);
    log->ad.digital.signals[12].source_type = LOG_DIG_SOURCE_TYPE_UINT8;
    log->ad.digital.signals[12].bit_mask = (1 << 0);
    log->ad.digital.signals[13].source.uint8 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), FLAG_FG_INTERNAL_EVT);
    log->ad.digital.signals[13].source_type = LOG_DIG_SOURCE_TYPE_UINT8;
    log->ad.digital.signals[13].bit_mask = (1 << 0);
    log->ad.digital.signals[14].source.uint8 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), FLAG_FG_START_FUNC);
    log->ad.digital.signals[14].source_type = LOG_DIG_SOURCE_TYPE_UINT8;
    log->ad.digital.signals[14].bit_mask = (1 << 0);
    log->ad.digital.signals[15].source.uint8 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), FLAG_FG_USE_ARM_NOW);
    log->ad.digital.signals[15].source_type = LOG_DIG_SOURCE_TYPE_UINT8;
    log->ad.digital.signals[15].bit_mask = (1 << 0);
    log->ad.digital.signals[16].source.uint32 = (uint32_t*)regMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].reg_mgr), REG_RST_SOURCE);
    log->ad.digital.signals[16].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[16].bit_mask = (1 << 0);
    log->ad.digital.signals[17].source.uint32 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_RT_STATUS);
    log->ad.digital.signals[17].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[17].bit_mask = (1 << REF_ILC_ERR_BIT);
    log->ad.digital.signals[18].source.uint32 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_RT_STATUS);
    log->ad.digital.signals[18].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[18].bit_mask = (1 << REF_ILC_Q_CALC_BIT);
    log->ad.digital.signals[19].source.uint32 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_RT_STATUS);
    log->ad.digital.signals[19].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[19].bit_mask = (1 << REF_ILC_REF_RUN_BIT);
    log->ad.digital.signals[20].source.uint32 = (uint32_t*)refMgrParPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), MODE_ECONOMY);
    log->ad.digital.signals[20].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[20].bit_mask = (1 << 0);
    log->ad.digital.signals[21].source.uint32 = (uint32_t*)refMgrParPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ECONOMY_FULL);
    log->ad.digital.signals[21].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[21].bit_mask = (1 << 0);
    log->ad.digital.signals[22].source.boolean = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), FLAG_ECO_ONCE);
    log->ad.digital.signals[22].source_type = LOG_DIG_SOURCE_TYPE_BOOLEAN;
    log->ad.digital.signals[23].source.uint32 = (uint32_t*)refMgrParPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ECONOMY_DYNAMIC);
    log->ad.digital.signals[23].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[23].bit_mask = (1 << 0);
    log->ad.digital.signals[24].source.uint32 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_WARNINGS);
    log->ad.digital.signals[24].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[24].bit_mask = (1 << REF_DYN_ECO_ARM_WARNING_BIT);
    log->ad.digital.signals[25].source.uint32 = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), REF_WARNINGS);
    log->ad.digital.signals[25].source_type = LOG_DIG_SOURCE_TYPE_UINT32;
    log->ad.digital.signals[25].bit_mask = (1 << REF_DYN_ECO_RUN_WARNING_BIT);

    log++;
    priv++;

    // Log 12 in log_struct.log[11]: BIS_FLAGS - BIS evaluation result

    priv->name                    = "BIS_FLAGS";
    priv->sig_bufs_offset         = 14568000;
    priv->sig_bufs_len            = 400000;
    priv->num_postmortem_sig_bufs = LOG_BIS_FLAGS_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 50000;
    priv->num_signals             = LOG_BIS_FLAGS_NUM_SIGNALS;
    priv->ad.digital.signals      = log_buffers->bis_flags.signals[0];

    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_BIS_FLAGS;
    log->num_cycles               = LOG_BIS_FLAGS_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_BIS_FLAGS);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK;
    log->num_sig_bufs             = LOG_BIS_FLAGS_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 400000;
    log->last_sample_marker.index = 399999;
    log->sig_bufs                 = &log_buffers->buffers[14568000];
    log->cycles                   = log_buffers->bis_flags.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.digital.source        = &dummy_bitfield;
    log->ad.digital.num_signals   = LOG_BIS_FLAGS_NUM_SIGNALS;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 0;

    log++;
    priv++;

    // Log 13 in log_struct.log[12]: BIS_LIMITS - BIS limits

    priv->name                    = "BIS_LIMITS";
    priv->sig_bufs_offset         = 14968000;
    priv->sig_bufs_len            = 5600;
    priv->num_postmortem_sig_bufs = LOG_BIS_LIMITS_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 25;
    priv->num_signals             = LOG_BIS_LIMITS_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->bis_limits.signals[0];
    priv->ad.analog.selectors     = log_buffers->bis_limits.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_BIS_LIMITS;
    log->num_cycles               = LOG_BIS_LIMITS_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_BIS_LIMITS);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK;
    log->num_sig_bufs             = LOG_BIS_LIMITS_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 200;
    log->last_sample_marker.index = 199;
    log->sig_bufs                 = &log_buffers->buffers[14968000];
    log->cycles                   = log_buffers->bis_limits.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->bis_limits[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 0;

    priv->ad.analog.selectors[ 0] =  0;  // CH1_MAX_REG_ERR;
    priv->ad.analog.selectors[ 1] =  1;  // CH1_MIN_V_MEAS;
    priv->ad.analog.selectors[ 2] =  2;  // CH1_MAX_V_MEAS;
    priv->ad.analog.selectors[ 3] =  3;  // CH1_MAX_V_RATE;
    priv->ad.analog.selectors[ 4] =  4;  // CH1_MIN_I_MEAS;
    priv->ad.analog.selectors[ 5] =  5;  // CH1_MAX_I_MEAS;
    priv->ad.analog.selectors[ 6] =  6;  // CH1_MAX_I_RATE;
    priv->ad.analog.selectors[ 7] =  7;  // CH2_MAX_REG_ERR;
    priv->ad.analog.selectors[ 8] =  8;  // CH2_MIN_V_MEAS;
    priv->ad.analog.selectors[ 9] =  9;  // CH2_MAX_V_MEAS;
    priv->ad.analog.selectors[10] = 10;  // CH2_MAX_V_RATE;
    priv->ad.analog.selectors[11] = 11;  // CH2_MIN_I_MEAS;
    priv->ad.analog.selectors[12] = 12;  // CH2_MAX_I_MEAS;
    priv->ad.analog.selectors[13] = 13;  // CH2_MAX_I_RATE;
    priv->ad.analog.selectors[14] = 14;  // CH3_MAX_REG_ERR;
    priv->ad.analog.selectors[15] = 15;  // CH3_MIN_V_MEAS;
    priv->ad.analog.selectors[16] = 16;  // CH3_MAX_V_MEAS;
    priv->ad.analog.selectors[17] = 17;  // CH3_MAX_V_RATE;
    priv->ad.analog.selectors[18] = 18;  // CH3_MIN_I_MEAS;
    priv->ad.analog.selectors[19] = 19;  // CH3_MAX_I_MEAS;
    priv->ad.analog.selectors[20] = 20;  // CH3_MAX_I_RATE;
    priv->ad.analog.selectors[21] = 21;  // CH4_MAX_REG_ERR;
    priv->ad.analog.selectors[22] = 22;  // CH4_MIN_V_MEAS;
    priv->ad.analog.selectors[23] = 23;  // CH4_MAX_V_MEAS;
    priv->ad.analog.selectors[24] = 24;  // CH4_MAX_V_RATE;
    priv->ad.analog.selectors[25] = 25;  // CH4_MIN_I_MEAS;
    priv->ad.analog.selectors[26] = 26;  // CH4_MAX_I_MEAS;
    priv->ad.analog.selectors[27] = 27;  // CH4_MAX_I_RATE;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 0].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 1].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 2].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 3].source = &dummy_float;
    priv->ad.analog.signals[ 3].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 4].source = &dummy_float;
    priv->ad.analog.signals[ 4].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 5].source = &dummy_float;
    priv->ad.analog.signals[ 5].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 6].source = &dummy_float;
    priv->ad.analog.signals[ 6].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 7].source = &dummy_float;
    priv->ad.analog.signals[ 7].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 8].source = &dummy_float;
    priv->ad.analog.signals[ 8].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 9].source = &dummy_float;
    priv->ad.analog.signals[ 9].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[10].source = &dummy_float;
    priv->ad.analog.signals[10].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[11].source = &dummy_float;
    priv->ad.analog.signals[11].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[12].source = &dummy_float;
    priv->ad.analog.signals[12].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[13].source = &dummy_float;
    priv->ad.analog.signals[13].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[14].source = &dummy_float;
    priv->ad.analog.signals[14].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[15].source = &dummy_float;
    priv->ad.analog.signals[15].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[16].source = &dummy_float;
    priv->ad.analog.signals[16].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[17].source = &dummy_float;
    priv->ad.analog.signals[17].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[18].source = &dummy_float;
    priv->ad.analog.signals[18].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[19].source = &dummy_float;
    priv->ad.analog.signals[19].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[20].source = &dummy_float;
    priv->ad.analog.signals[20].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[21].source = &dummy_float;
    priv->ad.analog.signals[21].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[22].source = &dummy_float;
    priv->ad.analog.signals[22].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[23].source = &dummy_float;
    priv->ad.analog.signals[23].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[24].source = &dummy_float;
    priv->ad.analog.signals[24].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[25].source = &dummy_float;
    priv->ad.analog.signals[25].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[26].source = &dummy_float;
    priv->ad.analog.signals[26].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[27].source = &dummy_float;
    priv->ad.analog.signals[27].meta   = SPY_SIG_META_STEPS;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 14 in log_struct.log[13]: ILC_CYC - ILC cycle log

    priv->name                    = "ILC_CYC";
    priv->sig_bufs_offset         = 14973600;
    priv->sig_bufs_len            = 400;
    priv->num_postmortem_sig_bufs = LOG_ILC_CYC_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 30;
    priv->num_signals             = LOG_ILC_CYC_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->ilc_cyc.signals[0];
    priv->ad.analog.selectors     = log_buffers->ilc_cyc.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_ILC_CYC;
    log->num_cycles               = LOG_ILC_CYC_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_ILC_CYC);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK;
    log->num_sig_bufs             = LOG_ILC_CYC_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 100;
    log->last_sample_marker.index = 99;
    log->sig_bufs                 = &log_buffers->buffers[14973600];
    log->cycles                   = log_buffers->ilc_cyc.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->ilc_cyc[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 0;

    priv->ad.analog.selectors[ 0] =  0;  // RMS_ERR_ILC;
    priv->ad.analog.selectors[ 1] =  1;  // INIT_RMS_ERR_ILC;
    priv->ad.analog.selectors[ 2] =  2;  // RMS_V_RATE;
    priv->ad.analog.selectors[ 3] =  3;  // RMS_V_RATE_LIM;
    priv->ad.analog.signals[ 0].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_RMS_ERR);
    priv->ad.analog.signals[ 0].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 1].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_INITIAL_RMS_ERR);
    priv->ad.analog.signals[ 1].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 2].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_RMS_V_REF_RATE);
    priv->ad.analog.signals[ 2].meta   = SPY_SIG_META_STEPS;
    priv->ad.analog.signals[ 3].source = refMgrVarPointer(&(SHMEM_NONVOL.tenants[device_index].ref_mgr), ILC_RMS_V_REF_RATE_LIMIT);
    priv->ad.analog.signals[ 3].meta   = SPY_SIG_META_STEPS;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 15 in log_struct.log[14]: DIM - DIM analog signals log

    priv->name                    = "DIM";
    priv->sig_bufs_offset         = 14974000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[0];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[0];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[14974000];
    log->cycles                   = log_buffers->dim.cycles[0];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[0].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 16 in log_struct.log[15]: DIM2 - DIM analog signals log

    priv->name                    = "DIM2";
    priv->sig_bufs_offset         = 14986000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[1];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[1];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM2;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM2);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[14986000];
    log->cycles                   = log_buffers->dim.cycles[1];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[1].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 17 in log_struct.log[16]: DIM3 - DIM analog signals log

    priv->name                    = "DIM3";
    priv->sig_bufs_offset         = 14998000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[2];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[2];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM3;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM3);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[14998000];
    log->cycles                   = log_buffers->dim.cycles[2];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[2].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 18 in log_struct.log[17]: DIM4 - DIM analog signals log

    priv->name                    = "DIM4";
    priv->sig_bufs_offset         = 15010000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[3];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[3];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM4;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM4);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15010000];
    log->cycles                   = log_buffers->dim.cycles[3];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[3].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 19 in log_struct.log[18]: DIM5 - DIM analog signals log

    priv->name                    = "DIM5";
    priv->sig_bufs_offset         = 15022000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[4];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[4];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM5;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM5);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15022000];
    log->cycles                   = log_buffers->dim.cycles[4];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[4].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 20 in log_struct.log[19]: DIM6 - DIM analog signals log

    priv->name                    = "DIM6";
    priv->sig_bufs_offset         = 15034000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[5];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[5];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM6;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM6);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15034000];
    log->cycles                   = log_buffers->dim.cycles[5];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[5].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 21 in log_struct.log[20]: DIM7 - DIM analog signals log

    priv->name                    = "DIM7";
    priv->sig_bufs_offset         = 15046000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[6];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[6];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM7;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM7);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15046000];
    log->cycles                   = log_buffers->dim.cycles[6];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[6].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 22 in log_struct.log[21]: DIM8 - DIM analog signals log

    priv->name                    = "DIM8";
    priv->sig_bufs_offset         = 15058000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[7];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[7];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM8;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM8);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15058000];
    log->cycles                   = log_buffers->dim.cycles[7];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[7].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 23 in log_struct.log[22]: DIM9 - DIM analog signals log

    priv->name                    = "DIM9";
    priv->sig_bufs_offset         = 15070000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[8];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[8];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM9;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM9);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15070000];
    log->cycles                   = log_buffers->dim.cycles[8];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[8].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 24 in log_struct.log[23]: DIM10 - DIM analog signals log

    priv->name                    = "DIM10";
    priv->sig_bufs_offset         = 15082000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[9];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[9];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM10;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM10);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15082000];
    log->cycles                   = log_buffers->dim.cycles[9];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[9].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 25 in log_struct.log[24]: DIM11 - DIM analog signals log

    priv->name                    = "DIM11";
    priv->sig_bufs_offset         = 15094000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[10];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[10];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM11;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM11);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15094000];
    log->cycles                   = log_buffers->dim.cycles[10];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[10].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 26 in log_struct.log[25]: DIM12 - DIM analog signals log

    priv->name                    = "DIM12";
    priv->sig_bufs_offset         = 15106000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[11];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[11];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM12;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM12);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15106000];
    log->cycles                   = log_buffers->dim.cycles[11];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[11].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 27 in log_struct.log[26]: DIM13 - DIM analog signals log

    priv->name                    = "DIM13";
    priv->sig_bufs_offset         = 15118000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[12];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[12];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM13;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM13);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15118000];
    log->cycles                   = log_buffers->dim.cycles[12];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[12].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 28 in log_struct.log[27]: DIM14 - DIM analog signals log

    priv->name                    = "DIM14";
    priv->sig_bufs_offset         = 15130000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[13];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[13];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM14;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM14);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15130000];
    log->cycles                   = log_buffers->dim.cycles[13];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[13].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 29 in log_struct.log[28]: DIM15 - DIM analog signals log

    priv->name                    = "DIM15";
    priv->sig_bufs_offset         = 15142000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[14];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[14];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM15;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM15);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15142000];
    log->cycles                   = log_buffers->dim.cycles[14];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[14].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Log 30 in log_struct.log[29]: DIM16 - DIM analog signals log

    priv->name                    = "DIM16";
    priv->sig_bufs_offset         = 15154000;
    priv->sig_bufs_len            = 12000;
    priv->num_postmortem_sig_bufs = LOG_DIM_NUM_POSTMORTEM_SIG_BUFS;
    priv->num_postmortem_samples  = 1500;
    priv->num_signals             = LOG_DIM_NUM_SIGNALS;

    priv->ad.analog.signals       = log_buffers->dim.signals[15];
    priv->ad.analog.selectors     = log_buffers->dim.selectors[15];
    log->log_mgr                  = log_mgr;
    log->priv                     = priv;
    log->start_cycle              = &log_structs->start_cycle;
    log->index                    = LOG_DIM16;
    log->num_cycles               = LOG_DIM_NUM_CYCLES;
    log->bit_mask                 = (uint32_t)(1 << LOG_DIM16);
    log->meta                     = 0|LOG_META_LITTLE_ENDIAN_BIT_MASK|LOG_META_CYCLIC_BIT_MASK|LOG_META_ANALOG_BIT_MASK|LOG_META_CONTINUOUS_BIT_MASK|LOG_META_FREEZABLE_BIT_MASK;
    log->num_sig_bufs             = LOG_DIM_NUM_SIG_BUFS;
    log->num_samples_per_sig      = 3000;
    log->last_sample_marker.index = 2999;
    log->sig_bufs                 = &log_buffers->buffers[15154000];
    log->cycles                   = log_buffers->dim.cycles[15];
    log->cur_cycle                = LOG_CYCLE_NOT_ACTIVE;

    log->ad.analog.selector_ptrs  = log_structs->dim[15].selector_ptrs;

    log->cd.continuous.state                   = LOG_RUNNING;
    log->cd.continuous.cur_time_base           = LOG_CYCLE_NOT_ACTIVE;
    log->cd.continuous.num_post_freeze_samples = 500;

    priv->ad.analog.selectors[ 0] =  0;  // ANA_A;
    priv->ad.analog.selectors[ 1] =  1;  // ANA_B;
    priv->ad.analog.selectors[ 2] =  2;  // ANA_C;
    priv->ad.analog.selectors[ 3] =  3;  // ANA_D;
    priv->ad.analog.signals[ 0].source = &dummy_float;
    priv->ad.analog.signals[ 1].source = &dummy_float;
    priv->ad.analog.signals[ 2].source = &dummy_float;
    priv->ad.analog.signals[ 3].source = &dummy_float;

    logStoreAnalogSelector(log);

    log++;
    priv++;

    // Initialize signal names and units

    logStoreAllSigNamesAndUnits(log_structs->log, log_sig_names_and_units);
}


//! Initialize log structures
//!
//! Wrapper around logStructsInitDevice() to initialize applications with one device
//!
//! @param[in]    log_mgr        Pointer to log manager structure to initialize
//! @param[in]    log_structs    Pointer to log structs structure to initialize
//! @param[in]    log_buffers    Pointer to log buffers structure

static inline void logStructsInit(struct LOG_mgr     * const log_mgr,
                                  struct LOG_structs * const log_structs,
                                  struct LOG_buffers * const log_buffers)
{
    logStructsInitDevice(log_mgr, log_structs, log_buffers, 0);
}


//! Initialize log_read structure
//!
//! This function will clear the log_read structure and set the links to the log_structs and log_buffers.
//!
//! @param[in]    log_read       Pointer to log read structure to initialize
//! @param[in]    log_structs    Pointer to log structs structure
//! @param[in]    log_buffers    Pointer to log buffers structure

static inline void logReadInit(struct LOG_read    * const log_read,
                               struct LOG_structs * const log_structs,
                               struct LOG_buffers * const log_buffers)
{
    memset(log_read, 0, sizeof(*log_read));

    log_read->log     = log_structs->log;
    log_read->buffers = log_buffers->buffers;
}

// EOF
