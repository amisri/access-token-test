# FGC4 bare-metal: Main file of the building system.

# **********************************************************
# Include the main file of the FGCD2 building system
    include("${CMAKE_CURRENT_LIST_DIR}/../../../fgcd2/cmake/fgcd.cmake")

# **********************************************************
# Change root path to the directories in the FGC4 directory
    set(FGC4_ROOT          "${CMAKE_CURRENT_LIST_DIR}/..")
    set(FGC4_SCRIPTS       "${FGC4_ROOT}/scripts/build")
    set(FGCD_MODULES_ROOT  "${FGC4_ROOT}/source/modules")
    set(FGCD_PROFILES_ROOT "${FGC4_ROOT}/cmake/profiles")
    set(FGCD_LIBS_ROOT     "${FGC4_ROOT}/cmake/libs")

# **********************************************************
# Simplify paths
    fgcdSimplifyPath(FGC4_ROOT)
    fgcdSimplifyPaths()
    
# **********************************************************
# Include all *.cmake files within 'commands' subdirectory of the FGC4 cmake
    file(GLOB FGCD_COMMANDS_FILES "${FGC4_ROOT}/cmake/commands/*.cmake")

    foreach(commandFile ${FGCD_COMMANDS_FILES})
        include("${commandFile}")
    endforeach(commandFile)


# EOF
