# FGC4: Adds post build commands to run after build

macro(fgc4RegisterPostBuild target)
    fgcdRequireVar(FGCD_PROFILE_LOADED "Call 'fgcdLoadProfile' before calling 'fgc4RegisterPostBuild'")    

    set(elf_path "${CMAKE_CURRENT_BINARY_DIR}/${target}.elf")
    set(bin_path "${CMAKE_CURRENT_BINARY_DIR}/${target}.bin")

    add_custom_command(TARGET FGCD POST_BUILD
                       COMMAND ${FGCD_PROFILE_OBJCOPY_PATH} -O binary ${elf_path} ${bin_path}
                       WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                       COMMENT "Converting ELF to BIN"
    )

    add_custom_command(TARGET FGCD POST_BUILD
                       COMMAND ${FGCD_PROFILE_SIZE_PATH} -B -d ${elf_path}
                       WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                       COMMENT "Binary size summary:"
    )

endmacro()

# EOF