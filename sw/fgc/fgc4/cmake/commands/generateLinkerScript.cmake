# FGC4: Generates linker script for the bare-metal targets based on cmake variables.

macro(fgc4GenerateLinkerScript target)
    fgcdRequireVar(FGC4_LINKER_RAM_BASE   "Define it before calling fgc4GenerateLinkerScript.")
    fgcdRequireVar(FGC4_LINKER_RAM_SIZE   "Define it before calling fgc4GenerateLinkerScript.")
    fgcdRequireVar(FGC4_LINKER_STACK_SIZE "Define it before calling fgc4GenerateLinkerScript.")
    fgcdRequireVar(FGC4_LINKER_HEAP_SIZE  "Define it before calling fgc4GenerateLinkerScript.")

    # Load the full linker script
    set(full_script_path "${FGC4_ROOT}/source/modules/base/core/startup/linker.ld")

    if (NOT EXISTS "${full_script_path}")
        message(FATAL_ERROR "FGCD error: The full linker script at '${full_script_path}' does not exist.")
    endif()

    file (READ "${full_script_path}" full_script)

    # Set the path for the generated linker script
    set(FGC4_LINKER_SCRIPT_PATH "${CMAKE_CURRENT_BINARY_DIR}/${target}_linker_script.ld")
    
    # Generate the linker script
    file(GENERATE OUTPUT "${FGC4_LINKER_SCRIPT_PATH}" CONTENT "/* Auto-generated for target '${target}':*/

        __RAM_BASE   = ${FGC4_LINKER_RAM_BASE};
        __RAM_SIZE   = ${FGC4_LINKER_RAM_SIZE};
        __STACK_SIZE = ${FGC4_LINKER_STACK_SIZE};
        __HEAP_SIZE  = ${FGC4_LINKER_HEAP_SIZE};
        
        \n${full_script}
    ")

endmacro()

# EOF