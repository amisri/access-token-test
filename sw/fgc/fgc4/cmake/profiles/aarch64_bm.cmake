# FGC4 bare-metal: Compilation profile for bare-metal cores on the aarch64 architecture.

fgcdRequireVar(FGC4_LINKER_SCRIPT_PATH "Call 'fgc4GenerateLinkerScript' before loading this profile.")

# Set path to the GCC C compiler
fgcdProfileSetCPath("${FGCD_GCC_AARCH64_BM}/bin/aarch64-none-elf-gcc")

# Set path to the GCC C++ compiler
fgcdProfileSetCxxPath("${FGCD_GCC_AARCH64_BM}/bin/aarch64-none-elf-g++")

# Set path to the objdump
fgcdProfileSetObjdumpPath("${FGCD_GCC_AARCH64_BM}/bin/aarch64-none-elf-objdump")

# Set path to the objcopy
fgcdProfileSetObjcopyPath("${FGCD_GCC_AARCH64_BM}/bin/aarch64-none-elf-objcopy")

# Set path to the size
fgcdProfileSetSizePath("${FGCD_GCC_AARCH64_BM}/bin/aarch64-none-elf-size")

# Set additional C/C++ flags
fgcdProfileAddFlag("-O2")
fgcdProfileAddFlag("-Wall")

fgcdProfileAddFlag("-mtune=cortex-a72")
fgcdProfileAddFlag("-nostartfiles")
fgcdProfileAddFlag("-ffreestanding")
fgcdProfileAddFlag("-specs=nosys.specs")
fgcdProfileAddFlag("-Wl,-T,${FGC4_LINKER_SCRIPT_PATH}")

# Set additional C flags
fgcdProfileAddCFlag("-std=c99")

# Set additional C++ flags
fgcdProfileAddCxxFlag("-std=c++17")
fgcdProfileAddCxxFlag("-fno-rtti")
fgcdProfileAddCxxFlag("-fno-exceptions")

# Add base modules implicitly
fgcdDependOnModules(base)

# EOF