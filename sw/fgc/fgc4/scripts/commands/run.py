from utils.findTargetExe import *
from projectControl.utils import *


class Command:
    @classmethod
    def init(cls, parser):
        cls.subparser = parser.add_parser("run", description="Runs target's binary on DIOT.",
                                          help="Runs target's binary on DIOT.")

        cls.subparser.add_argument("target", help="Target to run.")

    @classmethod
    def run(cls, args, cfg, project_args):
        target_exe = find_target_exe(cfg, project_args, args.target)

        # Do something here to reboot the board etc.





