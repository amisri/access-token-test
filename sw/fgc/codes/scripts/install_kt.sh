#!/bin/bash
#
# Filename: install_kt.sh
#
# Purpose:  Install fgc code files on the kt cernbox directory
#

helpmsg='
    Usage: install_kt.sh [options] [ <code> [ <version> ] ]

    This script installs a given release in the cernbox. That is, the script copies a released code(s) (the ones under $release_access:$release_path) or local code(s) to the knowledge transfer cernbox directory ($kt_release_access:$kt_release_path).

    Options:
    -h This help.
    -l Install codes from the local filesystem instead of released ones. Possible only for development destinations.
    -f Force to install codes, even if the version is already installed.
    -y Do not ask for confirmation (useful when executing from scripts).

    Arguments (if they are not set a menu appears):
    <code> Code id (e.g. C002_31_IDProg).
    <version> This is the version id (where \"latest\" means the latest version).
'

# Access to cernbox
kt_release_access=${kt_release_access:-'lxplus.cern.ch'}
kt_release_path=${kt_release_path:-'/eos/user/q/qking/Knowledge_Transfer/KT-FGC/FGC3_Firmware'}
# kt_release_path=${kt_release_path:-'/eos/user/t/tlivisia/Knowledge_Transfer/KT-FGC/FGC3_Firmware'}

# Access to relesae server should be from the user that runs the script not from poccdev
release_access='cs-ccr-teepc2.cern.ch'

operate_on_kt_codes()
{
    cmd="$1"

    ssh -A "$kt_release_access" "cd '$kt_release_path' && $cmd"
}

# Checks if any code in the target has been changed

diff_codes_kt()
{
    for code_file in $($operate_on_choosen_codes "ls $code/fieldbus/$version/ | egrep '^C'"); do

        # check if code file already exists
        operate_on_kt_codes "test -e '$code/$version/$code_file'"    
        if [ $? -eq 0 ]; then
            # and then check if it differs from the release version
            diff_codes operate_on_released_codes "$code/fieldbus/$version/$code_file" operate_on_kt_codes "$code/$version/$code_file"

            if [ $? -eq 1 ]; then
                return 1
            fi
        else
            return 1
        fi
    done

    return 0
}

create_path_in_cernbox()
{
    ssh -A "$kt_release_access" "cd '$kt_release_path' && mkdir -p '$1'"
}

# Secure copy the particular version of choosen code to the kt cernbox directory

update_codes()
{
    if [ -z "$force_install" ]; then
    
        diff_codes_kt
    
        if [ $? -eq 0 ]; then
            echo "INFO: The selected release is identical to the codes installed in cernbox. No need to update"
            echo "INFO: If you want to install the codes anyway, use the -f option"
            echo
            return
        fi
    fi

    echo "Releasing code $code in version $version to cernbox.."

    create_path_in_cernbox "$code/$version"

    if ! "$operate_on_choosen_codes" "scp '$code/fieldbus/$version/'* '$kt_release_access:$kt_release_path/$code/$version/'"; then
        echo >&2 "Error: Cannot secure copy files from '$code/fieldbus/$version/' to host '$kt_release_access' into '$kt_release_path/$code/$version' dir"
        exit 1
    fi
}


# Parse arguments, if needed show help message and exit

parse_args()
{
    # Parse options

    while getopts ":hlyf" opt; do
        case "$opt" in
            h)
                show_help='show_help'
                ;;
            l)
                dev_sending_case='dev_sending_case'
                ;;
            f)
                force_install='force_install'
                ;;    
            y)
                dont_ask='dont_ask'
                ;;                
            \?)
                echo >&2 "Error: Invalid option: -$OPTARG"
                exit 1
                ;;
        esac
    done


    # Shift already parsed positional parameters

    shift $((OPTIND-1))

    # Get the operands

    # Second parameter is code to sent if specified

    code="$1"

    # Optionally $version could be 'latest', points it directly or be empty to ask user

    version="$2"

    # Show help message if needed, then exit

    if [ -n "$show_help" ] ; then
        eval "echo \"$helpmsg\""
        exit 2
    fi
}

if [ "${BASH_SOURCE[0]}" = "${0}" ]; then
    # Save path to dir where the script is run from

    scripts_path="$(dirname "$0")"

    (
        # Change working directory in a subshell to one where scripts and codes are located

        cd "$scripts_path"

        # Import some common functions

        if ! source "./common.sh"; then
            echo >&2 "Error: Common functions could not be read: `pwd`/common.sh"
            exit 1
        fi

        # Parse arguments, set defaults, if needed show help message and exit

        parse_args "$@"

        # Choose the source of the codes. It could be taken from local filesystem or released on the server depends on sending case

        choose_codes_to_use

        # Choose code to send

        choose_code "$code"
        code="${codes[0]}"

        # Choose one of of the available versions

        choose_version "$version"

        # Collect codes info about installing codes

        collect_codes_info


        # Send files

        update_codes

        # Script ends successful

        echo -e "\nSuccessful"
    )
fi

# EOF

