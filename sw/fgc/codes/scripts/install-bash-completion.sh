_fgc_install()
{
    # Prepare environment

    # Get rid of error messages

    exec 2>/dev/null

    local opts
    local scripts_path="$(dirname "${COMP_WORDS[0]}")"

    # Source all nescessary functions

    source "$scripts_path/install.sh"
    source "$scripts_path/common.sh"
    source "$scripts_path/destinations.sh"

    # Overwrite sourced menu function, this one will get rid of description of the menu in the first arg, but leaves the options

    menu() {
        shift
        printf '%s\n' "$@"
    }

    # Unset some used variable to not overwrite defaults

    unset dev_sending_case quiet show_help

    # Reset position of curently parsing word

    OPTIND=1

    # Getting parsed arguments is run in subshell

    # Because there is a possibility to fail on parsing unknown option which will exit current shell

    read dev_sending_case < <(parse_args "${COMP_WORDS[@]:1}"; echo "$dev_sending_case")

    # Choosing codes source depends on parsed args. Also ommit script completion filename in first arg

    read code target version < <(parse_args "${COMP_WORDS[@]:1}"; echo "$code $target $version")

    # Get number of passed options

    read OPTIND < <(parse_args "${COMP_WORDS[@]:1}"; echo "$OPTIND")

    # Use choosen codes source

    choose_codes_to_use

    # Make completion depends on current argument index

    case $((COMP_CWORD-OPTIND+1)) in
        1)
            # Show available codes or options if dash is typed

            if [ "${COMP_WORDS[$COMP_CWORD]}" = '-' ]; then
                opts='-h -q -l'
            else
                opts="$(choose_code 2>/dev/null)"
            fi
            ;;
        2)
            # Choose code function is also sourcing the code.sh file with possible destinations

            opts="$(choose_code "$code" && choose_target)"
            ;;
        3)
            # Ommit human-readable descritpion of unixtime

            opts="$(choose_version | while read unixtime _; do echo "$unixtime"; done)"
            ;;
    esac

    # Return for bash completion list filtered by begging letters of the current word

    COMPREPLY=( $( compgen -W "$opts" -- "${COMP_WORDS[$COMP_CWORD]}" ) )

    # Restore stderr

    exec 2>/dev/tty
}

complete -F _fgc_install install.sh
