#!/bin/bash
#
# Filename: release.sh
#
# Purpose:  Release last version to operational destination
#
# Author:   Krystian Wojtas
#
# Notes:    This script is written for bash at least 3.1.0
#
# Usage:
#           release.sh [options] <code>

helpmsg='
    Usage: release.sh [options] <code>

    The script creates a release for a given code (e.g. C002_31_IDProg). The script does the following:

        Updates and commits the history.txt with the tagcode info for the most recently tagged version.
        Tags the new commit in the local repo with the code version number.
        Push commits to origin.
        Copies the latest <code> to $release_access:$release_path/<code>/fieldbus/<version>/
        Copies the latest <code> to $release_access:$release_path/<code>/serial/<version>/
        Copies the history.txt to $release_access:$release_path/<code>/fieldbus/<version>/
        Copies the history.txt to $release_access:$release_path/<code>/serial/<version>/
        Sends an email to $notification_list with a release notice indicating who did the release, on which branch and tag, and for which code.

    Options:
    -h, --help This help
    -f force to release codes, even if latest released version is identical to the same being released
'

# The following codes are handled by the barcode manager, so we don't require the history file to be manually updated

history_file_ignorelist=(
    "C003_50_IDDB-C004_50_PTDB"
    "C005_50_SysDB-C006_50_CompDB-C007_50_DIMDB"
    "C018_60_IDDB-C019_60_PTDB"
    "C020_60_SysDB-C021_60_CompDB-C022_60_DIMDB"
    "C025_90_IDDB-C026_90_PTDB"
    "C027_90_SysDB-C028_90_CompDB-C029_90_DIMDB"
)

# Parse arguments, if needed show help message and exit

parse_args()
{
    # Parse options

    while getopts ":hf" opt; do
        case "$opt" in
            h)
                help='help'
                ;;
            f)
                force_release='force_release'
                ;;
            \?)
                echo >&2 "Error: Invalid option: -$OPTARG"
                exit 1
                ;;
        esac
    done

    # Shift already parsed positional parameters

    shift $((OPTIND-1))

    # Get the operands

    # Particular code to release is optionally passed as first operand

    code="$1"

    # Show help message if needed, then exit succesfully

    if [ -n "$help" ] ; then
        eval "echo \"$helpmsg\""
        exit 2
    fi
}

check_directory_tree()
{
    local -r all_local_codes=$(operate_on_local_codes "ls -d C*")
    local -r all_remote_codes=$(operate_on_released_codes "ls -d C*")

    # Find any differences between local and remote code directories

    local -r diff_codes=(`echo ${all_local_codes[@]} ${all_remote_codes[@]} | tr ' ' '\n' | sort | uniq -u `)

    # If the code directories are not synchronised then print a warning

    if [[ ${#diff_codes[@]} > 0 ]]; then
        echo >&2 "Warning: Your local code directory tree differs from the remote one - code menus in install and release scripts will be different."
        echo >&2 "Differences: ${diff_codes[@]}"
    fi
}

# Ensure that needed dirs are available on server and create them if not

prepare_dirs()
{
    echo 'Preparing dirs for release..'

    # Create fieldbus/ dir on the server if it does not exist

    if ! operate_on_released_codes "test -d '$code/fieldbus'" && ! operate_on_released_codes "mkdir -p '$code/fieldbus'"; then
        echo >&2 "Error: fieldbus dir does not exist and cannot create it on host '$release_access' path '$( operate_on_released_codes " \"\$PWD/$code/fieldbus\"" )'"
        exit 1
    fi

    if ! operate_on_released_codes "test -d '$code/serial'" && ! operate_on_released_codes "mkdir -p '$code/serial'"; then
        echo >&2 "Error: serial dir does not exist and cannot create it on host '$release_access' path '$( operate_on_released_codes " \"\$PWD/$code/serial\"" )'"
        exit 1
    fi

    # Stop if latest version is already deployed

    if operate_on_released_codes "test -d '$code/fieldbus/$version/'" || operate_on_released_codes "test -d '$code/serial/$version/'"; then
        echo >&2 "Error: Latest version $version of code $code is already released"
        exit 1
    fi
}

# Ensure that dependent repositories are clean, up-to-date and on the same operational branches

check_repo()
{
    echo 'Checking repo..'

    # Get current project branch name and commit head

    if ! branch="$( git rev-parse --abbrev-ref 'HEAD' )"; then
        echo >&2 'Error: Cannot get current branch name'
        exit 1
    fi

    if ! head="$( git rev-parse HEAD )"; then
        echo >&2 'Error: Cannot get current commit name'
        exit 1
    fi

    # Check if head is on any branch

    if [[ "$branch" = 'HEAD' ]]; then
        echo >&2 'Error: You are on no branch'
        echo >&2 "git rev-parse --abbrev-ref 'HEAD' yielded: $branch"
        exit 1
    fi

    # Get upstream for current branch

    upstream="$(git for-each-ref --format='%(upstream:short)' "refs/heads/$branch")"

    # Check if current branch is tracking anything

    if [ -z "$upstream" ]; then
        echo >&2 "Error: Current branch '$branch' is not tracking anything"
        exit 1
    fi

    # Check if current branch tracks the same on remote

    if [[ "$upstream" != *"$branch" ]]; then
        echo >&2 "Error: Branch '$branch' is not tracking the same on the remote '$upstream'"
        exit 1
    fi

    echo "Fetching origin.."

    # Fetch latest commits

    if ! git fetch origin "$branch"; then
        echo >&2 "Error: Cannot fetch changes from a default remote"
        exit 1
    fi

    # Check if current branch is up-to-date with the depot

    if ! [ -z "`git log ..@{upstream}`" ]; then
        echo >&2 "Error: Current branch is behind depot"
        exit 1
    fi

    # Check if current branch hasn't any unpushed commit

    if ! [ -z "`git log @{upstream}..`" ]; then
        echo >&2 "Error: There are some unpushed commits"
        exit 1
    fi

    # Check if files are not dirty

    dirty_files="$( git status --porcelain )"

    if [ -n "$dirty_files" ]; then
        echo >&2 -e "Project files are dirty\n$dirty_files"
        exit 1
    fi
}

# Check if the codes are already released

check_if_already_released()
{
    if [ -z "$force_release" ]; then

        local -r dest_latest_version_fieldbus=$(operate_on_released_codes "ls $code/fieldbus | grep -v history.txt | sort -u  -n | tail -n1")
        local -r dest_latest_version_serial=$(operate_on_released_codes "ls $code/serial | grep -v history.txt | sort -u  -n | tail -n1")

        local -r local_codes_fieldbus=$(operate_on_local_codes "ls $code/fieldbus/$version");
        local -r local_codes_serial=$(operate_on_local_codes "ls $code/serial/$version");

        echo "local version:             $version"
        echo "remote version (fieldbus): $dest_latest_version_fieldbus"
        echo "remote version (serial):   $dest_latest_version_serial"

        if [ -z "$local_codes_fieldbus" ] || [ -z "$local_codes_serial" ]; then
            echo "ERROR: No codes to release"
            exit 1
        fi

        if [ -z $dest_latest_version_fieldbus ] || [ -z $dest_latest_version_serial ]; then
            echo "Missing fieldbus or serial codes on remote, proceeding with release..."
            return 0
        fi

        local fieldbus_ok=0

        for code_file in $local_codes_fieldbus; do

            diff_codes operate_on_local_codes "$code/fieldbus/$version/$code_file" operate_on_released_codes "$code/fieldbus/$dest_latest_version_fieldbus/$code_file"

            if [ $? -eq 1 ]; then
                fieldbus_ok=1
            fi
        done

        for code_file in $local_codes_serial; do

            diff_codes operate_on_local_codes "$code/serial/$version/$code_file" operate_on_released_codes "$code/serial/$dest_latest_version_serial/$code_file"

            if [ $? -eq 1 ] && [ $fieldbus_ok -eq 1 ]; then
                echo "Local and remote codes are not equal, proceeding with release..."
                return 0
            fi
        done

        echo "INFO: Release not created, current version is identical to the latest version $dest_latest_version"
        echo "INFO: If you want to release the codes anyway, use the -f option"
        exit 0

    fi

    return 0
}

# Copy codes to the server

copy_codes()
{
    # Copy files

    echo 'Copying files..'

    # Transfer the most recent codes from local to shared dir on server side

    if ! operate_on_local_codes "tar -c '$code/fieldbus/$version/'" | operate_on_released_codes "tar -xm"; then
        echo >&2 "Error: Failed copying files to shared directory (fieldbus)"
        rollback
        exit 1
    fi

    if ! operate_on_local_codes "tar -c '$code/serial/$version/'" | operate_on_released_codes "tar -xm"; then
        echo >&2 "Error: Failed copying files to shared directory (serial)"
        rollback
        exit 1
    fi
}

# Update history with extra automatic lines taken from tagcode logs

update_history()
{
    echo 'Updating history..'

    # Ignore history for codes that are managed automatically

    for ignore_code in "${history_file_ignorelist[@]}"; do
        if [ "$code" = "$ignore_code" ]; then
            return 0
        fi
    done

    # Take the last lines of generated tagcode_log*.txt files to the beginning of hand-editable history.txt

    if ! log_files="$( operate_on_local_codes "ls '$code/tagcode_log'?*'.txt'" )"; then
        echo >&2 "Error: There isn't any log file '$(operate_on_local_codes "echo \"\$PWD/$code/tagcode_log\"?*'.txt'")'"
        rollback
        exit 1
    fi

    if ! lastlogs="$( operate_on_local_codes "echo '$log_files' | xargs -n 1 tail -1" )"; then
        echo >&2 "Error: Cannot tail '$(operate_on_local_codes "echo \"\$PWD/$code/tagcode_log\"?*'.txt' files")'"
        rollback
        exit 1
    fi

    # Check if history.txt file exists

    if ! operate_on_local_codes "test -f '$code/history.txt'"; then
        echo >&2 "Error: history.txt file does not exist in code path: '$(operate_on_local_codes "echo '$code/history.txt'")'"
        rollback
        exit 1
    fi

    # Check if hand-editable history.txt file has any description about new version

    if operate_on_local_codes "[[ \$(cat $code/history.txt) = Class* ]]"; then

        echo >&2 "Error: history.txt file should contain description of new deploying version"
        rollback
        exit 1
    fi

    # Update local history with lines on the beginning taken from tagcode logs

    if ! history="$lastlogs"$'\n'$'\n'"$(operate_on_local_codes "cat '$code/history.txt'" )"; then
        echo >&2 "Error: Cannot cat history '$(operate_on_local_codes "echo '\$PWD/$code/history.txt'")'"
        rollback
        exit 1
    fi

    if ! echo "$history" | operate_on_local_codes "cat - > '$code/history.txt'"; then
        echo >&2 "Error: Cannot write history to file '$(operate_on_local_codes "echo '\$PWD/$code/history.txt'")'"
        rollback
        exit 1
    fi

    # Update history on the server side also, but put it into code version dirs

    if ! echo "$history" | operate_on_released_codes "cat - > '$code/fieldbus/$version/history.txt'"; then
        echo >&2 "Error: Cannot write history to file '$(operate_on_released_codes "echo '\$PWD/$code/fieldbus/$version/history.txt'")' on host '$host'"
        rollback
        exit 1
    fi

    if ! echo "$history" | operate_on_released_codes "cat - > '$code/serial/$version/history.txt'"; then
        echo >&2 "Error: Cannot write history to file '$(operate_on_released_codes "echo '\$PWD/$code/serial/$version/history.txt'")' on host '$host'"
        rollback
        exit 1
    fi
}

# Make extra automatic commit about history, create a tag and push them

autocommit()
{
    # Ignore history for codes that are managed automatically

    for ignore_code in "${history_file_ignorelist[@]}"; do
        if [ "$code" = "$ignore_code" ]; then
            return 0
        fi
    done

    echo 'Autocommiting history...'

    # Compose commit message

    msg="$( echo "FGC code release. Update history.txt"$'\n'$'\n'"$lastlogs"$'\n'$'\n'"$codes_info_trimmed" )"

    # Commit history.txt

    if ! commit_output="`operate_on_local_codes "git commit -m '$msg' '$code/history.txt'" 2>&1`"; then
        echo >&2 "$commit_output"
        echo >&2 "Error: Failed to commit history log $( operate_on_local_codes "ls \"\$PWD/$code/history.txt\"" )"
        rollback
        exit 1
    fi

    # Build tagname

    tagname="$branch/$code/$version"

    # Tag this commit

    if ! tag_output="`git tag "$tagname" 2>&1`"; then
        echo >&2 "$tag_output"
        echo >&2 "Error: Cannot tag '$tagname'"
        rollback
        exit 1
    fi

    # Push changes

    if ! push_output="` ( git push origin "$branch" && git push origin "$tagname") 2>&1 `"; then
        echo >&2 "$push_output"
        echo >&2 "Error: Cannot push new automatic commit and tag"
        rollback
        exit 1
    fi
}

# Send emails to developers and notify about releasing the code

notify_releasing()
{
    # Notify developers
    ssh "${release_access#*@}" "mail -s '$code released (tag $tagname)' '$notification_list'" <<EOF
The code $code have been released by $LOGNAME with tag $tagname.

$codes_info

You can check the release notes at
https://git.cern.ch/web/ccs/fgc.git/blob/refs/heads/$branch:/sw/fgc/codes/$code/history.txt
EOF
}

# Rollback changes if something goes wrong

rollback()
{
    if ! operate_on_released_codes "rm -rf '$code/fieldbus/$version'"; then
        echo >&2 "Error: Cannot clean released dir '$code/fieldbus/$version'"
    fi

    if ! operate_on_released_codes "rm -rf '$code/serial/$version'"; then
        echo >&2 "Error: Cannot clean released dir '$code/serial/$version'"
    fi

    if ! git reset --hard "$head"; then
        echo >&2 'Error: Cannot reset hard to the last head'
    fi
}

# End of local functions
# Don't run script if it is being sourced

if [ "${BASH_SOURCE[0]}" = "${0}" ]; then

    # Save path to dir where the script is run from

    scripts_path="$(dirname "$0")"

    (
        # Change working directory in a subshell to one where scripts and codes are located

        cd "$scripts_path"

        # Import some common functions

        if ! source "./common.sh"; then
            echo >&2 "Error: Common functions could not be read: `pwd`/common.sh"
            exit 1
        fi

        # Turn on extended glob bash matching

        shopt -s extglob

        # Parse arguments

        parse_args "$@"

        # Compare local and remote code directory structure

        check_directory_tree

        # Choose codes from local filesystem

        use_local_codes

        # Choose code to release

        choose_code "$code" 'checklist'


        for code in "${codes[@]}"; do
            echo "Handling the code: $code"

            # Choose latest available version

            choose_version 'latest'

            # We only make a new release if the generated binaries are different from the latest installed version

            check_if_already_released

            # Collect codes info about releasing codes

            collect_codes_info

            # Build paths to nescessary dirs

            prepare_dirs

            # Ensure that dependent repositories are clean, up-to-date and on the same operational branches

            check_repo

            # Copy codes to the server

            copy_codes

            # Update history

            update_history

            # Copy files, then make extra automatic commit and tag about history and push them

            autocommit

            # Send emails to developers and notify about releasing the code
            # Right now it is disabled
            # notify_releasing

            # Script ends successful

            echo "Code $code was released in the version $version"

        done

        echo -e "\nSuccessful"
    )
fi

# EOF
