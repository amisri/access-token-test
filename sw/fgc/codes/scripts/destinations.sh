# Filename: destinations.sh
#
# Purpose:  Provides access to servers and destinations for installing codes
#
# Authors:  Stephen Page, Krystian Wojtas
#
# Notes:    This script is written for bash at least 3.1.0
#
# Usage:
#           Do not use it directly
#
#           This file is only for sourcing
#

# Destinations for installing depends on code's class

all_targets=( $(ls -1 $install_path) )

# declare array of targets by class
declare -A class_targets=()


function getTargetsForClass(){
    CLASS=$1

    # Check if we have already parsed the targets for this class
    if [[ -n ${class_targets[$CLASS]} ]]; then
        # Key exists in our dict of targets
	IFS=' '     # space is set as delimiter
	targ=()
	read -ra targ <<< "${class_targets[$CLASS]}"
        printf '%s\n' "${targ[@]}"
	unset IFS
	return
    fi

    old_pwd=$(pwd)

    case $CLASS in
        50)
            # get all fifties (51, 52, 53 ...)
            CLASS="5[0-9]"
            ;;
        60)
            # get all 60s
            CLASS="6[0-9]"
            ;;
        90)
            # get all 90s
            CLASS="9[0-9]"
            ;;
        31)
            # merge class 50 and 60 targets
            CLASS="[5-6][0-9]"
            ;;
    esac

    cd $install_path/..
    targ=( $(cat name | grep -o "^.*:.*:$CLASS" | grep -o "^[^:]*" | sort | uniq | while read -r line ; do grep "$line" group | awk -F':' '{ print $3 }' | grep -o "^[^/]*"; done | sort | uniq) )

    # SOME TARGET ON THE GROUP FILE MAY NOT EXIST IN THE $install_path
    # exclude targets that are not in all_targets by doing the intersection of the 2 arrays
    targ=( $(comm -12  <(printf '%s\n' "${targ[@]}") <(printf '%s\n' "${all_targets[@]}")) )

    # save the targets in the dictionary
    class_targets[$1]="${targ[@]}"

    # PRINT THE RESULT
    printf '%s\n' "${targ[@]}"
    unset IFS
    cd $old_pwd
}

# Instead of building the code groups in the beginning we build them on the fly, when we need them, to be faster

# dests_50=($(getTargetsForClass "5[0-9]"))

# dests_51=($(getTargetsForClass 51))

# dests_53=($(getTargetsForClass 53))

# dests_59=($(getTargetsForClass 59))

# dests_60=($(getTargetsForClass "6[0-9]"))

# dests_62=($(getTargetsForClass 62))

# dests_63=($(getTargetsForClass 63))

# dests_64=($(getTargetsForClass 64))

# dests_65=($(getTargetsForClass 65))

# dests_90=($(getTargetsForClass "9[0-9]"))

# 31 is id prog and it is common for both platforms

#dests_31=(
#    ${dests_50[@]}
#    ${dests_60[@]}
#)

# EOF
