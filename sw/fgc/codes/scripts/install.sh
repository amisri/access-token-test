#!/bin/bash
#
# Filename: install.sh
#
# Purpose:  Install fgc code files on gateways
#
# Author:   Krystian Wojtas
#
# Notes:    This script is written for bash at least 3.1.0
#

helpmsg='
    Usage: install.sh [options] [ <code> [ <destination> [ <version> ] ] ]

    This script install a given release. That is, the script copies a released code(s) (the ones under $release_access:$release_path) or local code(s) to the operational code directory/ies ($install_access:$install_path/<destination>).
    The script also sends an email to $notification_list with the version number, the name of the person executing the script, and a link to the history.txt in git web for op installs.

    Options:
    -h This help.
    -q Do not notify developers about a new version.
    -l Install codes from the local filesystem instead of released ones. Possible only for development destinations.
    -f Force to install codes, even if the version is already installed.
    -y Do not ask for confirmation (useful when executing from scripts).

    Arguments (if they are not set a menu appears):
    <code> Code id (e.g. C002_31_IDProg).
    <destination> One of the code groups or all of them if \"all\" keyword is provided.
    <version> This is the version id (where \"latest\" means the latest version).
'

# OUTPUT COLORS
red="\e[0;91m"
green="\e[0;92m"
yellow="\e[0;33m"
reset="\e[0m"


# Parse arguments, if needed show help message and exit

parse_args()
{
    # Parse options

    while getopts ":hqlyf" opt; do
        case "$opt" in
            h)
                show_help='show_help'
                ;;
            q)
                quiet='quiet'
                ;;
            l)
                dev_sending_case='dev_sending_case'
                ;;
            f)
                force_install='force_install'
                ;;
            y)
                dont_ask='dont_ask'
                ;;
            \?)
                echo >&2 "Error: Invalid option: -$OPTARG"
                exit 1
                ;;
        esac
    done


    # Shift already parsed positional parameters

    shift $((OPTIND-1))

    # Get the operands

    # Second parameter is code to sent if specified

    code="$1"

    # Optionally $target could be 'all', points one directly or be empty to ask user

    target="$2"

    # Optionally $version could be 'latest', points it directly or be empty to ask user

    version="$3"

    # Show help message if needed, then exit

    if [ -n "$show_help" ] ; then
        eval "echo \"$helpmsg\""
        exit 2
    fi
}

# After choosing particular code, it could be included all its settings and validated $target parameter if passed

choose_target()
{
    target="$1"

    # Load all possible destinations of the choosen classes
    # check if more than one code was selected
    dests_all=()
    for code in "${codes[@]}"; do
        class=${code#*_}
        class=${class/_*}
        dests_all+=( $(getTargetsForClass $class) )
    done

    # Remove duplicated destinations
    dests_all=( $( printf '%s\n' "${dests_all[@]}" | sort -u ) )

    # Exclude operational targets for development case
    dests_dev=( $( printf "%s\n" "${dests_all[@]}" | grep -vF "`printf "%s\n" "${operational_targets[@]}"`" ) )

    # Set $dests array to set of targets corresponding choosen sending case
    if [ -n "$dev_sending_case" ]; then
        dests=( "${dests_dev[@]}" )
    else
        dests=( "${dests_all[@]}" )
    fi


    # If not specified, then ask about target including possibility with all of them
    if [ -z "$target" ]; then
        checklist "Targets" "Choose on which targets to install $code:" 'all' "${dests[@]}"
    else
        choices=($target)
    fi

    # Resolve $targets array depending on parsing $target param

    case "${choices[0]}" in
        'all')
            # Choose all targets
            targets=( "${dests[@]}" )
            ;;
        ?*)
            # Target is specified directly, it must be validated first

            # Ensure that specified target host occurs in possible dests list

            if echo "${dests[@]}" | grep -iqv "$target"; then

                # Target does not exist in hardcoded arrays, die

                echo >&2 -e "${red}Error: The target $target is not valid for class $class. ${reset}"
                exit 1
            fi

            # Ask for confirmation if target is not Test_Reception and -y (don't ask) option is not set
            #
            #if [ $target != "Test_Reception" ] && [ -z "$dont_ask" ] ; then
            #
            #    echo "Confirm if your target is ok"
            #    echo "$target"

            #    echo
            #    read -p "Are you sure? [press 'y' followed by enter to confirm] " -r
            #    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
            #        echo >&2 "Cancelled by user"
            #        exit 1
            #    fi
            #fi

            targets=( "${choices[@]}" )
            ;;
    esac

    echo "Selected targets:"
    printf '%s\n' "${targets[@]}"
    echo

    # Check if every target is valid

    for target in ${targets[@]}; do
        if ! ssh "$install_access" "cd $install_path/$target"; then
            echo >&2 -e "${red}Error: Server $install_access on path $install_path has no target $target${reset}"
            exit 1
        fi
    done
}

# Checks if any code in the target has been changed

diff_codes_target()
{
    target="$1"

    for code_file in $($operate_on_choosen_codes "ls $code/fieldbus/$version/ | egrep '^C'"); do

        diff_codes operate_on_released_codes "$code/fieldbus/$version/$code_file" operate_on_installed_codes "$target/$code_file"

        if [ $? -eq 1 ]; then
            return 1
        fi
    done

    return 0
}

# Secure copy to destination targets particular version of choosen code

update_codes()
{

    for target in "${targets[@]}"; do

        if [ -z "$force_install" ]; then

            diff_codes_target "$target"

            if [ $? -eq 0 ]; then
                echo -e "${red}The selected release for $code is identical to the codes installed in $target. No need to update${reset}"
                echo -e "${red}If you want to install the codes anyway, use the -f option${reset}"
                echo
                continue
            fi
        fi

	# Check if the target is valid for this code
        class=${code#*_}
        class=${class/_*}
	if [[ "$(getTargetsForClass $class)" != *"$target"* ]]; then
            echo >&2 -e "${yellow}Info: $code not copied to $target because it is not a valid target for class $class ${reset}"
            echo
            codes_updated="warning"
            continue
        else
            echo "Installing version $version of $code in $target.."
            if ! "$operate_on_choosen_codes" "scp '$code/fieldbus/$version/C'* '$install_access:$install_path/$target/'"; then
                echo >&2 -e "${red}Error: Cannot secure copy files from '$code/fieldbus/$version/' to host '$install_access' into '$install_path/$target' dir${reset}"
                exit 1
            fi
            codes_updated="codes_updated"
        fi

    done
}

# Make a log about the date of installing choosen code

log()
{
    operate_on_released_codes "cat - >> '$code/install_log.txt'" <<EOF
`date`
New codes installed by $LOGNAME at: ${targets[@]}
$codes_info

EOF
}

log_installed()
{
    operate_on_installed_codes "cat - >> '../installed_codes/$code/install_log.txt'" <<EOF
`date +"%Y-%m-%d %H:%M:%S"`
New codes installed by $LOGNAME at: ${targets[@]}
$codes_info

EOF
}

# Send emails to developers and notify about sending the code

notify_send()
{
    # Look for tag name knowing the code and version

    tagname="$(git tag | grep "$code" | grep "$version")"
    branch="${tagname%%/*}"

    # Old codes version system does not have any tags in git, in this case point master

    [ -z "$branch" ] && branch='master'

    # Notify developers if quiet flag is not set

    if [ -z "$quiet" ]; then
        ssh "${release_access#*@}" "mail -s '$code installed on ${targets[@]}' '$notification_list'" <<EOF
The code $code has been installed by $LOGNAME in ${targets[@]}.

$codes_info

Tag: $tagname

You can check the release notes at
${git_repo_url}/${branch}/sw/fgc/codes/$code/history.txt
EOF
    fi
}

read_gateways()
{

    # Read codes on Development & Test Areas.
    #
    # The way gateways are mapped to codes deployment zones is not obvious, and anyway it's going to be changed in the future
    # Therefore, we just read the codes in development & test areas

    if [[ ${targets[@]} =~ (Development|Test) ]] ; then
        ssh $install_access "cat /user/pclhc/etc/fgcd/group"  | egrep "(Test Reception)" | awk -F: '{ system("rda-set -r location -p GW.READCODES -v \"\" -d "toupper($1))}'
    fi
}

commit()
{
    msg="fgc codes: Code $code with version $version installed at ${targets[@]}"
    operate_on_installed_codes "git add ../installed_codes && git commit -m '$msg'"
}

update_wiki_pages()
{
    make generate_and_deploy_html_for_code_release -C $(git rev-parse --show-toplevel)/sw/clients/python/wiki_pages
}

# End of local functions

# Don't run script if it is being sourced

if [ "${BASH_SOURCE[0]}" = "${0}" ]; then

    # Save path to dir where the script is run from

    scripts_path="$(dirname "$0")"

    (
        # Change working directory in a subshell to one where scripts and codes are located

        cd "$scripts_path"

        # Import some common functions

        if ! source "./common.sh"; then
            echo >&2 "Error: Common functions could not be read: `pwd`/common.sh"
            exit 1
        fi

        # Include all possible destinations

        if ! source "./destinations.sh"; then
            echo >&2 "Error: File with possible destinations could not be read: `pwd`/destinations.sh"
            exit 1
        fi


        # Parse arguments, set defaults, if needed show help message and exit

        parse_args "$@"

        # Choose the source of the codes. It could be taken from local filesystem or released on the server depends on sending case

        choose_codes_to_use

        # Choose code(s) to send

        choose_code "$code" 'checklist'

        # Choose target dir

        choose_target "$target"

        # If more than 1 code selected ask if ALL codes should be installed in its latest version
        if [ "${#codes[@]}" -gt "1" ]; then
            if (whiptail --title "Latest version" --yesno "Do you want to install the LATEST version for ALL the selected codes?" 8 78); then
                version='latest'
                all_latest=1
                echo "Latest version of the selected codes will be installed."
            else
                echo "Code versions to install selected individually."
            fi
        fi

        declare -i failures=0
	declare -i warnings=0
        for code in "${codes[@]}"; do

            # Choose one of the available versions

            choose_version "$version"

            # Collect codes info about installing codes

            collect_codes_info

            # Send files

            update_codes

            # if no codes have been sent, then we don't need to continue with the process of notifying/re-reading fecs, etc.
            if [[ "$codes_updated" == "codes_updated" ]]; then
                # Read codes on the gateways
                read_gateways

                # Make a log about the date of installing choosen code
                log
                log_installed

                # Commented because we STOPPED git tracking the install_log files. See EPCCCS-8950
                # commit

                # Send emails to developers and notify about sending the code
                notify_send
            elif [[ "$codes_updated" == "warning" ]]; then
		warnings+=1
            else
                failures+=1
            fi


            if [ -z "$all_latest" ]; then
                # empty the $version if user chose not to install latest for all codes
                version=""
            else
                # reassign 'latest' so it can be resolved to the version number
                version="latest"
            fi

            # empty $codes_updated to move on to next code
            codes_updated=""
        done

	if [ $warnings -gt 0 ]; then
            echo -e "${yellow}Finished with $warnings warnings ${reset}\n"
        fi

        if [ $failures -eq 0 ]; then
            # Script ends successfully
            echo -e "${green}Successful ${reset}\n"

            # Generate wiki-pages html tables
            update_wiki_pages
        else
            if [ $failures -gt 1 ]; then
                echo -e "${red}$failures CODES FAILED ${reset}\n"
            else
                echo -e "${red}FAILED${reset}\n"
            fi
        fi
    )
fi

# EOF
