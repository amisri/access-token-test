#!/bin/bash
#
# Filename: common.sh
#
# Purpose:  Common set of funtions and variables for another scripts
#
# Authors:  Stephen Page, Krystian Wojtas
#
# Notes:    This script is written for bash at least 3.1.0
#
# Usage:
#           Do not use it directly
#
#           This file is only for sourcing
#

# Accesses to servers

release_access=${release_access:-'poccdev@cs-ccr-teepc2.cern.ch'}
release_path=${release_path:-'/user/poccdev/projects/fgc/sw/fgc/codes'}

install_access=${install_access:-'pclhc@cs-ccr-teepc2.cern.ch'}

install_path=${install_path:-'/user/pclhc/etc/fgcd/code_groups'}
install_libraries_path=${install_libraries_path:-'/user/pclhc/lib/perl/FGC'}

# People on this list will be notified about instalation or releasing the codes

notification_list=${notification_list:-'converter-controls-software-updates@cern.ch'}

git_repo_url="https://gitlab.cern.ch/ccs/fgc/blob/"

# Draw menu with description and available options given by a parameters

menu()
{
    IFS=$'\n'

    # get the title and description of the window
    title=$1
    description=$2
    #description=""
    shift 2

    # transform list of items into a sequence of whiptail structured options
    # options structure: <ITEM_1> <DESCRIPTION_1> ...         
    options=()
    for item in $@ ;do
        options+=("$item" "");
    done

    # open menu windows
    choice=$(whiptail --title "$title" --menu "$description" 25 78 16 "${options[@]}" 3>&1 1>&2 2>&3)
    choices=$choice

    # Check if choice is valid
    if [ -z "$choice" ]; then
        echo >&2 "Error: Invalid choice"
        exit 1
    fi
}

checklist()
{
    IFS=$'\n'

    # get the title and description of the window
    title=$1
    description=$2
    shift 2

    instructions="Press SPACE to select and ENTER to proceed. Press TAB to change focus."

    # transform list of items into a sequence of whiptail structured options
    # options structure: <ITEM_1> <DESCRIPTION_1> <on/off> ...
    # we set off as default for all options
    options=()
    for item in $@ ;do
        options+=($item "" off);
    done

    # print window and save choice(s)
    choices=( $(whiptail --separate-output --title "$title" --checklist "$description\n$instructions" 20 78 12 "${options[@]}" 3>&1 1>&2 2>&3) )

    # Check if choice is valid
    if [ -z "$choices" ]; then
        echo >&2 "Error: Invalid choice"
        exit 1
    fi
}


# Install and release scripts operate on local codes

use_local_codes()
{
    host='localhost'

    operate_on_choosen_codes='operate_on_local_codes'
}

# Deploy operates on remotes codes

use_released_codes()
{
    host="$release_access"

    operate_on_choosen_codes='operate_on_released_codes'
}

# Helper functions

operate_on_local_codes()
{
    cmd="$1"

    bash -c "cd .. && $cmd"
}

operate_on_released_codes()
{
    cmd="$1"

    ssh -A "$release_access" "cd '$release_path' && $cmd"
}

operate_on_installed_codes()
{
    cmd="$1"

    ssh -A "$install_access" "cd '$install_path' && $cmd"
}

# Choose the source of the codes. It could be taken from local filesystem or released on the server depends on sending case which is parsed already

choose_codes_to_use()
{
    # Dev sending case implicates local source of the codes. Default is looking for already released codes

    if [ -n "$dev_sending_case" ]; then
        use_local_codes
    else
        use_released_codes
    fi
}

# Choose code to send

choose_code()
{

    # Use passed code if specified
    code="$1"

    # Check is prompt type was passed as arg, if not menu will be shown as default
    if [ -z "$2" ]; then
        prompt='menu'
    else
        prompt=$2
    fi

    # If code is not specified, ask interactively

    if [ -z "$code" ]; then

        # Collect all available codes

        all_codes=( `"$operate_on_choosen_codes" 'ls -d C*'` )

        # Check if there is any code inside

        if [ -z "$all_codes" ]; then
            echo >&2 "Error: In codes path there isn't any code to choose: '$(operate_on_local_codes 'pwd')'"
            exit 1
        fi

        # get calling script name without extension
        script_name=$(basename $0 | awk -F '.' '{ print $1 }')

        # Provide a menu        
        $prompt 'Codes' "Chose which codes to $script_name" 'all' "${all_codes[@]}"
        
        if [ "$choices" == "all" ]; then
            choices=( ${all_codes[@]} )
        fi

        # code="${choices[0]}"
        codes=( ${choices[@]} )
    else
       codes=($code)
    fi

    printf "Selected code(s):\n"
    printf '%s\n' "${codes[@]}"
    echo
}

# Parse version depends on passed argument

choose_version()
{
    version="$1"

    # Check if choosen code has any version available

    if [ -z "$($operate_on_choosen_codes "ls '$code/fieldbus'" )" ]; then
        echo >&2 "Choosen code has not any version available"
        exit 1
    fi

    # Show the menu if version is not specified

    if [ -z "$version" ]; then

        # Collect available versions

        if ! unixtimes=( $("$operate_on_choosen_codes" "cd '$code/fieldbus' && ls -dr */") ); then
            echo >&2 "Error: There isn't any available version: '$(operate_on_local_codes "ls '$code/fieldbus/'")'"
            exit 1
        fi

        # Make a list of possible options based upon unixtimes and corresponding human-readable ones

        for i in "${!unixtimes[@]}"; do
            verbose_versions[$i]="${unixtimes[$i]%/}   $( echo ${unixtimes[$i]}  | awk '{ print strftime("%c", $1) }' )"
        done

        # Invoke the menu and save choice

        menu 'Choose version: ' "Select which version to install for the code $code" 'latest' "${verbose_versions[@]}"
        version=`echo "$choice" | cut -d' ' -f 1`
    fi

    # Resolve version number if user wants the latest code

    if [ "$version" = 'latest' ]; then
        version="$("$operate_on_choosen_codes" "ls -t1 '$code/fieldbus' | head -n1")"
    fi

    # Check if there is any file inside

    if [ -z "$($operate_on_choosen_codes "ls '$code/fieldbus/$version'")" ]; then
        echo >&2 "Error: There isn't any file inside choosen version dir: '$(operate_on_local_codes "ls -d '$code/fieldbus/$version'")'"
        exit 1
    fi
}

# Collect codes info

collect_codes_info()
{
    # Build code info about all possible codes

    codes_info="$( "$operate_on_choosen_codes" "cd '$code/fieldbus/$version'; find C* -print0 | xargs --null ~pclhc/bin/python/code_info.sh" )"

    # Check if there is anything

    if [ -z "$codes_info" ]; then
        echo >&2 "Error: Cannot get any info about the codes"
        exit 1
    fi
}

diff_codes()
{
    origin_access="$1"
    origin_src="$2"

    dest_access="$3"
    dest_src="$4"

    # assigning initial value in case  $wc command does not return a number
    # which happens if file is not found
    declare -i origin_code_size=-1
    declare -i dest_code_size=-2

    origin_code_size=$($origin_access "wc --bytes $origin_src | tail -n1 | cut -d' ' -f1")
    dest_code_size=$($dest_access     "wc --bytes $dest_src | tail -n1 | cut -d' ' -f1")

    if [ "$origin_code_size" -ne "$dest_code_size" ]; then
        echo "codes are not the same size"
        return 1
    fi

    # Last 30 bytes are code info. This includes the date the codes were created.
    # We ignore these last bytes so that the same codes compiled at different times would still be considered to be equal
    bytes_to_compare=$(($origin_code_size-30))

    cmp -b --bytes=$bytes_to_compare <($origin_access "cat $origin_src") <($dest_access "cat $dest_src")

    if [ $? -ne 0 ]; then
        echo "codes differ"
        return 1
    fi

    return 0
}

# End of common functions

# EOF
