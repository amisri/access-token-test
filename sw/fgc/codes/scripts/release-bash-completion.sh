_release()
{
    # Prepare environment

    # Get rid of error messages

    exec 2>/dev/null

    local opts
    local scripts_path="$(dirname "${COMP_WORDS[0]}")"

    # Source all nescessary functions

    source "$scripts_path/common.sh"

    # Overwrite sourced menu function, this one will get rid of description of the menu in the first arg, but leaves the options

    menu() {
        shift
        printf '%s\n' "$@"
    }

    # Choose codes from the local filesystem

    use_local_codes

    # Make completion depends on current argument index

    case "$COMP_CWORD" in
        1)
            opts="$(choose_code)"
            ;;
    esac

    # Return for bash completion list filtered by begging letters of the current word

    COMPREPLY=( $( compgen -W "$opts" -- "${COMP_WORDS[$COMP_CWORD]}" ) )

    # Restore stderr

    exec 2>/dev/tty
}

complete -F _release release.sh
