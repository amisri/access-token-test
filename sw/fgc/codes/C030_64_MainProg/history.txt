Class:64 - MainProg:1679392875 - Tue Mar 21 11:01:15 2023 - CRC:8750

    - [EPCCCS-xxxx] Fix bug affecting initialization of REGFGC3.PARAMS
    - [EPCCCS-xxxx] Initialize REGFGC3.PARAMS version and DSP and slot and add REGFGC3.RAW RESET

Class:64 - MainProg:1675093011 - Mon Jan 30 16:36:51 2023 - CRC:DAB1

    - [EPCCCS-9439] liblog: Integrate nanosecond time resolution and add spy_v3 format
    - [EPCCCS-9525] libcctime: Integrate new time library

Class:64 - MainProg:1671198001 - Fri Dec 16 14:40:01 2022 - CRC:63AF

    - [EPCCCS-xxxx] Make REF.DIRECT properties NON_VOLATILE and CONFIG

Class:64 - MainProg:1669389870 - Fri Nov 25 16:24:30 2022 - CRC:D1C6

    - [EPCCCS-xxxx] Fix bug with REGFGC3.BAD.*

Class:64 - MainProg:1669123523 - Tue Nov 22 14:25:23 2022 - CRC:5523

    - [EPCCCS-xxxx] Increase command task timeout to account for FPGA binaries of 1.2 MB

Class:64 - MainProg:1668787208 - Fri Nov 18 17:00:08 2022 - CRC:BFEA

    - [EPCCCS-xxxx] Update crate definitions

Class:64 - MainProg:1668717479 - Thu Nov 17 21:37:59 2022 - CRC:6346

    - [EPCCCS-8820] Increase FGC_MAX_DEV_LEN from 23 to 31 characters

Class:64 - MainProg:1668605140 - Wed Nov 16 14:25:40 2022 - CRC:4EC1

    - [EPCCCS-xxxx] Add all possible values for FGC.NETWORK

Class:64 - MainProg:1666625224 - Mon Oct 24 17:27:04 2022 - CRC:2CEF

    - [EPCCCS-xxxx] Add FGC.NETWORK ETHER_101 and ETHER_102
    - [EPCCCS-9437] liblog signal names and units converted from pointers to strings to character arrays for FGC4
    - [EPCCCS-9437] Refactor menu_index to log_index in call logChangeSignalName

Class:64 - MainProg:1659455902 - Tue Aug  2 17:58:22 2022 - CRC:4BE4

    - [EPCCCS-9352] Remove CAL.VERF.ERR property limits

Class:64 - MainProg:1659452901 - Tue Aug  2 17:08:21 2022 - CRC:43A2

    - [EPCCCS-9136] Increase the DSP DPRAM memory size reserved for remote programming to account for the ARTIX-7
    - [EPCCCS-xxxx] Recover the Ethernet communication after a TXE error
    - [EPCCCS-xxxx] Add FGC.PLL.ETH_SYNC_CAL
    - [CCP   - 136] Always publish all the child properties even if one throws an error
    - [EPCCCS-xxxx] Consider the gateway lost only if 3 consecutive timing packets are not received
    - [EPCCCS-9132] Fix bug when discernaning if a DIM log is to be enabled

Class:64 - MainProg:1645194666 - Fri Feb 18 15:31:06 2022 - CRC:9DB8

    - [EPCCCS-xxxx] Fix bug added when eventsTask was introduced 

Class:64 - MainProg:1644247711 - Mon Feb  7 16:28:31 2022 - CRC:3960

    - [EPCCCS-9076] Fix bug with mapping between dim index and bus address
    - [EPCCCS-xxxx] Fix bug when no RegFGC3 boards are present

Class:64 - MainProg:1643130936 - Tue Jan 25 18:15:36 2022 - CRC:D099

    - [EPCCCS-xxxx] Move the init of regfgc3Prog from after DB to after NVS to recover mezzanine ID

Class:64 - MainProg:1643113561 - Tue Jan 25 13:26:01 2022 - CRC:DDAD

    - [EPCCCS-9100] Modify the behaviour when the RegFGC3 parameters API changes

Class:64 - MainProg:1642693353 - Thu Jan 20 16:42:33 2022 - CRC:902A

    - [EPCCCS-xxxx] Fix bug to correctly turn on the VS red led

Class:64 - MainProg:1642517493 - Tue Jan 18 15:51:33 2022 - CRC:4D02

    - [EPCCCS-xxxx] Timestamp DIAG.ANA, DIAG.DIG and DIAG.DATA properties with the DIM cycle time

Class:64 - MainProg:1640099379 - Tue Dec 21 16:09:39 2021 - CRC:EC21

    - [EPCCCS-xxxx] Increase stack size of MCU tasks and replace %f with %u for prinf-like functions
    - [EPCCCS-9076] Add support for DIM warnings
    - [EPCCCS-xxxx] Increase command execution timeout from 10 to 15s and MAX_VAL_LEN from 2890000 to 8410000 bytes
    - [EPCCCS-9065] Prevent turning on a converter during the association of ID/Barcode by the config manager

Class:64 - MainProg:1638885726 - Tue Dec  7 15:02:06 2021 - CRC:EECF

    - [EPCCCS-xxxx] Add FGC.DEBUG.WATCH.DSP and rename FGC.DEBUG.WATCH to FGC.DEBUG.WATCH.MCU

Class:64 - MainProg:1638462065 - Thu Dec  2 17:21:05 2021 - CRC:F302

    - [EPCCCS-xxxx] Add FGC.DEBUG.WATCH functionality
    - [EPCCCS-xxxx] Refactor and share the boot/main shared memory definition
    - [EPCCCS-xxxx] Modify the SPIVS log rate from 10KHz to 5KHz

Class:64 - MainProg:1636129048 - Fri Nov  5 17:17:28 2021 - CRC:C7C8

    - [EPCCCS-8957] Run the pars functoins on the DSP when the NVS has been initialized
    - [EPCCCS-xxxx] Fix bug with coupling between field names in REGFGC3.SLOT_INFO and the Program Manager

Class:64 - MainProg:1633676324 - Fri Oct  8 08:58:44 2021 - CRC:EA4E

    - [EPCCCS-8937] Make sure to switch to ProductionBoot when the Program Manager is not available
    - [EPCCCS-8901] Retrieve the sha value from RegFGC3 board programs

Class:64 - MainProg:1631802692 - Thu Sep 16 16:31:32 2021 - CRC:7EDA

    - [EPCCCS-8901] Remove SYNC_DB_CAL 60 seconds delay
    - [EPCCCS-8873] Drop NO_TEMP and BAD_TEMP errors and return 0C or 99C instead
    - [APS-9089] Set the acquisition stamp to conform with the expected contextual data
    - [EPCCCS-8837] Add support for ANA-104
    - [EPCCCS-8823] Add back the latched condition FGC_LAT_PSU_V_FAIL
    - [EPCCCS-xxxx] Correct I_ERATH log offset
    - [APS-8841]    Add property FIELDBUS.FGC_ETHER.TX.NUM_ST_FAILED
    - [EPCCCS-8656] Prevent DSP_FLT from being asserted during initialization
    - [EPCCCS-8650] Do not use constants to loop the regfgc3 board and variant sym lists
    - [EPCCCS-8597] Add properties REGFGC3.RAW.PARAM_S and REGFGC3.RAW.DSP_PARAM_S

Class:64 - MainProg:1612534920 - Fri Feb  5 15:22:00 2021 - CRC:20A1

    - [APS-8701]    Zero the acquisition stamp for settable properties
    - [EPCCCS-xxxx] Freeze transducer measurements during its ADC calibration period

Class:64 - MainProg:1611148230 - Wed Jan 20 14:10:30 2021 - CRC:8B32

    - [EPCCCS-xxxx] Simulate I_EARTH
    - [EPCCCS-6773] Refactor the FGC logger support

Class:64 - MainProg:1610645101 - Thu Jan 14 18:25:01 2021 - CRC:9CCA

    - [EPCCCS-8496] Replace REF.DIRECT.PF.VALUE with REF.DIRECT.PF.RATE and REF.DIRECT.PF.SIGN

Class:64 - MainProg:1608046763 - Tue Dec 15 16:39:23 2020 - CRC:31C7

    - [EPCCCS-8496] Add regulation mode Power Factor
    - [EPCCCS-8491] Increase the number of command packets to fit the longest property value

Class:64 - MainProg:1604409829 - Tue Nov  3 14:23:49 2020 - CRC:9346

    - [EPCCCS-XXXX] Fix bug when setting the number of elements for DSP properties
    - [EPCCCS-XXXX] Send reference and regulation mode to the DSP card after building the RegFGC3 cache
    - [EPCCCS-XXXX] Make REGFGC3.PROG.MANAGER NON CONFIG but NON VOLATILE
    - [EPCCCS-XXXX] Include symbol for REGFGC3.PROG.DEBUG.ACTION to reset SCIVS bus from main
    - [EPCCCS-XXXX] Fix transition to OF or FO without VS_READY being cleared

Class:64 - MainProg:1598000849 - Fri Aug 21 11:07:29 2020 - CRC:5DE9

    - [EPCCCS-8055] Automatic select the signal group from PowerSpy

Class:64 - MainProg:1597748191 - Tue Aug 18 12:56:31 2020 - CRC:7BF1

    - [EPCCCS-XXXX] Protect commands sent from the MCU to the DSP

Class:64 - MainProg:1593697892 - Thu Jul  2 15:51:32 2020 - CRC:512B

    - [EPCCCS-XXXX] Force a reset when all the boards have transitioned Production Boot
    - [EPCCCS-8177] Set calibration flag for ADC_D when calibrating the DACs
    - [EPCCCS-XXXX] Clear latched FGC_LAT_DIMS_EXP_FLT after switching to production boot

Class:64 - MainProg:1592816447 - Mon Jun 22 11:00:47 2020 - CRC:8BDA

    - [EPCCCS-XXXX] Dpuble the DSP stack (4 KB)

Class:64 - MainProg:1592570662 - Fri Jun 19 14:44:22 2020 - CRC:7704

    - [EPCCCS-XXXX] Increment the DSP RT watchdog before updating the profile to prevent a false DSP_FLT
    - [EPCCCS-XXXX] Suppress ADC processing if signal is NONE and not calibrating
    - [EPCCCS-XXXX] Suppress V_MEAS_OK in ADC status if signal is NONE
    - [EPCCCS-XXXX] Require ESC ESC to start or restart terminal editor mode

Class:64 - MainProg:1592299451 - Tue Jun 16 11:24:11 2020 - CRC:870C

    - [EPCCCS-XXXX] Update real-time DSP profiling. Add FGC.DEBUG.DSP.RT_MAX_US.
    - [EPCCCS-XXXX] Rebuild REGFGC3.PARAMS if REGFGC3.RAW used and property empty
    - [EPCCCS-8016] Init device.spare_id to zero. Make device.spare_id class 63 only

Class:64 - MainProg:1591092255 - Tue Jun  2 12:04:15 2020 - CRC:48DE

    - [EPCCCS-XXXX] Include forgotten commit

Class:64 - MainProg:1590478431 - Tue May 26 09:33:51 2020 - CRC:5122

    - [EPCCCS-XXXX] Fix Auto-calibration never running bug
    - [EPCCCS-8094] Reset expected vs detected dim counter (DIAG.DIMS_EXP_ERRS) after all boards are switched to PB
    - [EPCCCS-8093] Compute correctly number of parameters in property if only one block present
    - [EPCCCS-8092] Make possible send regfgc3 parameters after program manager is done

Class:64 - MainProg:1590756799 - Fri May 29 14:53:19 2020 - CRC:231C

    - [EPCCCS-8070] Migrate RegFgc3 blob to new version
    - [EPCCCS-8037] Build RegFgc3 parameters' cache when issuing S REGFGC3.SLOT_INFO

Class:64 - MainProg:1589464381 - Thu May 14 15:53:01 2020 - CRC:4313

    - [EPCCCS-6773] Refactor the FGC logger support to dicern between post-mortem and slow-abort events
    - [EPCCCS-8027] Fix bug related to SPY signals not corresponding with their values

Class:64 - MainProg:1588596991 - Mon May  4 14:56:31 2020 - CRC:DB1F

    - [EPCCCS-8022] Change signals SPIVS

Class:64 - MainProg:1588064802 - Tue Apr 28 11:06:42 2020 - CRC:FC5E

    - [EPCCCS-6773] Add support to SPY V2
    - [EPCCCS-7913] Take into account REGFGC3.PROG.MODE PROG_WARNING

Class:64 - MainProg:1586169676 - Mon Apr  6 12:41:16 2020 - CRC:0A92

    - [EPCCCS-7913] Send REGFGC3.PARAMS after program manager is done (new fix)

Class:64 - MainProg:1585738304 - Wed Apr  1 12:51:44 2020 - CRC:9A18

    - [EPCCCS-7913] Send REGFGC3.PARAMS after program manager is done
    - [EPCCCS-6773] Add support for the FGC logger

Class:64 - MainProg:1585142514 - Wed Mar 25 14:21:54 2020 - CRC:80EA

    - [EPCCCS-7913] Add config property to control program manager
    - [EPCCCS-7913] Wait for the config manager to raise program manager sync flag

Class:64 - MainProg:1583936410 - Wed Mar 11 15:20:10 2020 - CRC:49CE

    - [EPCCCS-7913] Disable program manager temporarily
    - [EPCCCS-xxxx] Initialize CAL properties with the correct number of elements

Class:64 - MainProg:1583824543 - Tue Mar 10 08:15:43 2020 - CRC:2D72

    - [EPCCCS-xxxx] Make sure all elements of non-shrinkable properties are set
    - [EPCCCS-5577] Include RegFgc3 reprogramming functionality
    - [EPCCCS-7735] Make the resolution of TIME_TILL_C0_US in microseconds
    - [EPCCCS-7539] Introduce new blob format for converters with more than 256 parameters
    - [EPCCCS-xxxx] SPIVS writes are transacional, no need to check the BUSY status
    - [EPCCCS-6453] Provide microsecond resolution for the timing events

Class:64 - MainProg:1579170590 - Thu Jan 16 11:29:50 2020 - CRC:67D0

    - [EPCCCS-7242] Restore values of properties mapped to parameters

Class:64 - MainProg:1576059464 - Wed Dec 11 11:17:44 2019 - CRC:B16C

    - [EPCCCS-7734] Associate the correct variable to the Q_FEEDER field

Class:64 - MainProg:1574430853 - Fri Nov 22 14:54:13 2019 - CRC:068D

    - [EPCCCS-xxxx] Make properties mapped into DSP card parameters not change REGFGC3.PARAMS, only HW
    - [EPCCCS-xxxx] Update the SPY interface
    - [EPCCCS-6840] Fix bug that caused run log entries to not be added in the main prog
    - [EPCCCS-7641] Map PLC commands to DIG_SUPB inputs
    - [EPCCCS-XXXX] Fix bug with DIM signal names not showing correctly in event log

Class:64 - MainProg:1568991045 - Fri Sep 20 16:50:45 2019 - CRC:795E

    - [EPCCCS-7546] Fix bug with the DIM analogue signal log menus

Class:64 - MainProg:1568911741 - Thu Sep 19 18:49:01 2019 - CRC:BB5E

    - [EPCCCS-7546] Fix bug with the DIM analogue signal log menus

Class:64 - MainProg:1567502729 - Tue Sep  3 11:25:29 2019 - CRC:3B7A

    - [EPCCCS-7504] Change mapping between properties and parameters (use block 2)
    - [EPCCCS-xxxx] Fix bug due to race conditions when sending MCU commands

Class:64 - MainProg:1566403027 - Wed Aug 21 17:57:07 2019 - CRC:F1D8

    - [EPCCCS-7485] Update the SPI-VS signals
    - [EPCCCS-7300] Increase to 7 the number of decimal digits for floating point values
    - [EPCCCS-7459] Fix DAC_FLT latched condition
    - [EPCCCS-7450] Do not block access to SPY logs when the OASIS properties are published

Class:64 - MainProg:1565084694 - Tue Aug  6 11:44:54 2019 - CRC:7FEB

    - [EPCCCS-xxxx] Decrease the code size to 320 KB
    - [EPCCCS-7324] Improve program manager FSM
    - [EPCCCS-xxxx] Increase the MCU code size by 128 Kb (640 Kb)
    - [EPCCCS-7233] Rename REF.FUNC.REG_MODE with REG.MODE
    - [EPCCCS-7363] Correctly set the internal ADC temperature thresholds
    - [EPCCCS-4715] Discern scalar and array properties using the type case
    - [EPCCCS-7313] Correctly update status conditions
    - [EPCCCS-7270] Fix DEVICE properties

Class:64 - MainProg:1556813179 - Thu May  2 18:06:19 2019 - CRC:1C56

    - [EPCCCS-7219] Fix issue with the event log action field

Class:64 - MainProg:1555419591 - Tue Apr 16 14:59:51 2019 - CRC:41D1

    - [EPCCCS-7227] Fix DEVICE.CLASS_ID returning 0

Class:64 - MainProg:1554302102 - Wed Apr  3 16:35:02 2019 - CRC:E762

    - [EPCCCS-5577] Switch boards to production boot during crate scan
    - [EPCCCS-7197] Format correctly output of REGFGC3.SLOT_INFO for terminal
    - [EPCCCS-7175] Fix runlog reading past the real size of input data

Class:64 - MainProg:1553792032 - Thu Mar 28 17:53:52 2019 - CRC:1A9B

    - [EPCCCS-xxxx] Fix limits for LIMITS.V.MAX

Class:64 - MainProg:1553791257 - Thu Mar 28 17:40:57 2019 - CRC:E06F

    - [EPCCCS-7180] Do not log DIM digital signals with no action label

Class:64 - MainProg:1553679929 - Wed Mar 27 10:45:29 2019 - CRC:FC3D

    - [EPCCCS-7177] Rename LIMITS.{A,V,Q}.POS with LIMITS.{A,V,Q}.MAX
    - [EPCCCS-7114] Change algorithm for setting DEVICE.NAME

Class:64 - MainProg:1552916190 - Mon Mar 18 14:36:30 2019 - CRC:67D6

    - [EPCCCS-xxxx] Add the decoding of the command pulses"

Class:64 - MainProg:1552644260 - Fri Mar 15 11:04:20 2019 - CRC:6578

    - [EPCCCS-7135] Fix bug causing the Main to crash
    - [EPCCCS-7096] Increment size of RegFGC3 params to 2000

Class:64 - MainProg:1552410209 - Tue Mar 12 18:03:29 2019 - CRC:CD98

    - [EPCCCS-7045] Fix bug with dallasTask not recognising external ids

Class:64 - MainProg:1549988853 - Tue Feb 12 17:27:33 2019 - CRC:F1D3

    - [EPCCCS-5878] Add support to set bitmask properties
    - [EPCCCS-6936] Add get option SYNCHED used to ensure tranactional synchronization with the DB
    - [EPCCCS-6863] Repurpose the ST_UNLATCHED condition ABORT_FLAG to LOG_PLEASE

Class:64 - MainProg:1544715620 - Thu Dec 13 16:40:20 2018 - CRC:A2FF

    - [EPCCCS-5968] Add published data

Class:64 - MainProg:1544437554 - Mon Dec 10 11:25:54 2018 - CRC:DFEF

    - [EPCCCS-5968] Define the real-time display
    - [EPCCCS-6893] Add transition from SP to FS"
    - [EPCCCS-6793] Do not shrink scalar properties

Class:64 - MainProg:1540978529 - Wed Oct 31 10:35:29 2018 - CRC:359E

    - [EPCCCS-6824] Use OPBLOCKED to decode the PLC commands

Class:64 - MainProg:1540898217 - Tue Oct 30 12:16:57 2018 - CRC:E0C9

    - [EPCCCS-6824] Use VS_EXTINTLOCK only to decode the PLC commands

Class:64 - MainProg:1539700341 - Tue Oct 16 16:32:21 2018 - CRC:63B3

    - [EPCCCS-5968] Rename LIMITS.*.NEG to LIMITS.*.MIN
    - [EPCCCS-5968] Map MEAS.ACQ_GROUP to a RegFGC3 parameter"

Class:64 - MainProg:1534514434 - Fri Aug 17 16:00:34 2018 - CRC:F46A

   - Second release of class FGC_64

Class:64 - MainProg:1534155394 - Mon Aug 13 12:16:34 2018 - CRC:7982

    - First release of class FGC_64
