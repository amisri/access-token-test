Class:60 - BootProg:1674817119 - Fri Jan 27 11:58:39 2023 - CRC:3E02

    - [EPCCCS-xxxx] Increase the size of C023_62 code to 384Kb

Class:60 - BootProg:1668717293 - Thu Nov 17 21:34:53 2022 - CRC:EF9C

    - [EPCCCS-8820] Increase FGC_MAX_DEV_LEN from 23 to 31 characters
    - [EPCCCS-xxxx] Add all possible values for FGC.NETWORK

Class:60 - BootProg:1666625024 - Mon Oct 24 17:23:44 2022 - CRC:6EE9

    - [EPCCCS-xxxx] Add FGC.NETWORK ETHER_101 and ETHER_102

Class:60 - BootProg:1666338015 - Fri Oct 21 09:40:15 2022 - CRC:0AE9

    - [EPCCCS-9468] Fix bug in a Test menu

Class:60 - BootProg:1659522081 - Wed Aug  3 12:21:21 2022 - CRC:2796

    - [EPCCCS-9405] Fix QSPI menu

Class:60 - BootProg:1638462044 - Thu Dec  2 17:20:44 2021 - CRC:6739

    - [EPCCCS-xxxx] Refactor and share the boot/main shared memory definition

Class:60 - BootProg:1628086900 - Wed Aug  4 16:21:40 2021 - CRC:53B9

    - [EPCCCS-8838] Same PLD code for all analogue interfaces
    - [EPCCCS-8837] Add support for ANA-104

Class:60 - BootProg:1622186699 - Fri May 28 09:24:59 2021 - CRC:1B96

    - [EPCCCS-8752] Fix formatting for data in menu 33106

Class:60 - BootProg:1604929559 - Mon Nov  9 14:45:59 2020 - CRC:021B

    - [EPCCCS-XXXX] Update the ADC error limits used by the Boot when testing the ADCs and DACs

Class:60 - BootProg:1600180178 - Tue Sep 15 16:29:38 2020 - CRC:CFB1

    - [EPCCCS-XXXX] Update QSPI Self Test

Class:60 - BootProg:1593760085 - Fri Jul  3 09:08:05 2020 - CRC:1F5A

    - [EPCCCS-XXXX] Fix for PLL Unstable message in the boot
    - [EPCCCS-XXXX] Add menu entry to set DIG SUP A data. Refactor DIG SUP self test.

Class:60 - BootProg:1587138262 - Fri Apr 17 17:44:22 2020 - CRC:753C

    - [EPCCCS-6932] Modify destination device fot UTC manual tests

Class:60 - BootProg:1584640044 - Thu Mar 19 18:47:24 2020 - CRC:32CD

    - [EPCCCS-xxxx] Remove all references to the FIR, no longer configurable

Class:60 - BootProg:1583516249 - Fri Mar  6 18:37:29 2020 - CRC:A4E0

    - [EPCCCS-5577] Remove set production boot pars task

Class:60 - BootProg:1582795763 - Thu Feb 27 10:29:23 2020 - CRC:6E96

    - [EPCCCS-7735] Make the resolution of TIME_TILL_C0_US in microseconds

Class:60 - BootProg:1565084271 - Tue Aug  6 11:37:51 2019 - CRC:5277

    - [EPCCCS-xxxx] Decrease the code size of class 62, 64 and 65 to 320 KB

Class:60 - BootProg:1564995149 - Mon Aug  5 10:52:29 2019 - CRC:3F0E

    - [EPCCCS-7445] Modify boot to accpet different MCU code sizes
    - [EPCCCS-xxxx] Increase the MCU code size by 128 Kb (640 Kb)

Class:60 - BootProg:1562916004 - Fri Jul 12 09:20:04 2019 - CRC:6201

    - [EPCCCS-7107] Refactor SPIVS manual and self tests

Class:60 - BootProg:1561623983 - Thu Jun 27 10:26:23 2019 - CRC:6BCC

    - [EPCCCS-7107] Improve Peek and Poke functions

Class:60 - BootProg:1561544354 - Wed Jun 26 12:19:14 2019 - CRC:DB53

    - [EPCCCS-7107] Increase MCU timeout on DSP acknowledge
    - [EPCCCS-7321] Terminal printing fixes for corrupted code

Class:60 - BootProg:1561538092 - Wed Jun 26 10:34:52 2019 - CRC:1E65

    - [EPCCCS-7321] Do not show the codes to be updated until this issue is fixed

Class:60 - BootProg:1561133976 - Fri Jun 21 18:19:36 2019 - CRC:0B5B

    - [EPCCCS-7107] Fix for PLL Unstable on DSP status

Class:60 - BootProg:1561122583 - Fri Jun 21 15:09:43 2019 - CRC:CD9A

    - [EPCCCS-7321] Show old DIMDB code as corrupted after size increase

Class:60 - BootProg:1560329628 - Wed Jun 12 10:53:48 2019 - CRC:A0FA

    - [EPCCCS-7321] Increase the size of DIMDB
    - [EPCCCS-7198] Add extra delay to allow the main PLL to stabilise

Class:60 - BootProg:1553863181 - Fri Mar 29 13:39:41 2019 - CRC:CB7B

    - [EPCCCS-7175] Fix runlog reading past the real size of input data
    - [EPCCCS-7157] Drop the FAST_BOOT functionality

Class:60 - BootProg:1551105423 - Mon Feb 25 15:37:03 2019 - CRC:66FF

    - [EPCCCS-5798] Fix PLL not being updated when there's no physical connection

Class:60 - BootProg:1550829905 - Fri Feb 22 11:05:05 2019 - CRC:C975

    - [EPCCCS-6991] Refactor handling of initialisation status
    - [EPCCCS-6932] New menu for Module UTC in Manual Tests

Class:60 - BootProg:1549620328 - Fri Feb  8 11:05:28 2019 - CRC:FCB7

    - [EPCCCS-5612] Fix FAILED code update status being stuck in standalone

Class:60 - BootProg:1534155398 - Mon Aug 13 12:16:38 2018 - CRC:AF5E

    - [EPCCCS-5968] Add code FGC_64

Class:60 - BootProg:1527256505 - Fri May 25 15:55:05 2018 - CRC:DF92

    - [EPCCCS-6421] Fix PLL not being initialised correctly

Class:60 - BootProg:1526284401 - Mon May 14 09:53:21 2018 - CRC:F516

    - [EPCCCS-6396] Always update NameDB before other codes

Class:60 - BootProg:1519998471 - Fri Mar  2 14:47:51 2018 - CRC:5162

    - [EPCCCS-6181] Quick fix for code CRC calculation taking too long

Class:60 - BootProg:1516364484 - Fri Jan 19 13:21:24 2018 - CRC:6A50

    - [EPCCCS-6005] Fix pulse generation test for dig sup

Class:60 - BootProg:1516281481 - Thu Jan 18 14:18:01 2018 - CRC:1456

    - [EPCCCS-6005] Improve dig sup pulse test

Class:60 - BootProg:1516266761 - Thu Jan 18 10:12:41 2018 - CRC:BB62

    - [EPCCCS-6004] Bring back DSP self-test on startup

Class:60 - BootProg:1516192951 - Wed Jan 17 13:42:31 2018 - CRC:6AD7

    - [EPCCCS-1550] Fix displaying of boot error labels

Class:60 - BootProg:1506352041 - Mon Sep 25 17:07:21 2017 - CRC:764E

    - [EPCCCS-5752] Fix sharing of code version between boot and main

Class:60 - BootProg:1501575055 - Tue Aug  1 10:10:55 2017 - CRC:5BD7

    - [EPCCCS-1550] Disable self tests on startup

Class:60 - BootProg:1500968951 - Tue Jul 25 09:49:11 2017 - CRC:1F0B

    - [EPCCCS-5607] Fix reprogramming mechanism

Class:60 - BootProg:1500382698 - Tue Jul 18 14:58:18 2017 - CRC:7B9B

    - Revert BootProg:1500381229 - testing new release.sh

Class:60 - BootProg:1500381229 - Tue Jul 18 14:33:49 2017 - CRC:D7AC

    - [EPCCCS-1550] Fix small bugs

Class:60 - BootProg:1500038885 - Fri Jul 14 15:28:05 2017 - CRC:30B1

    - [EPCCCS-1550] Disable DSP test on startup

Class:60 - BootProg:1500037260 - Fri Jul 14 15:01:00 2017 - CRC:4C6B

    - [EPCCCS-1550] Refactor and improve boot

Class:60 - BootProg:1496154728 - Tue May 30 16:32:08 2017 - CRC:7465

    - [EPCCCS-5137] Store PLD code CRC in memory instead of getting it from the GW
    - [EPCCCS-4957] Provide support to run DSP code from external memory
    - [EPCCCS-5233] Add test for DIG_SUP output

Class:60 - BootProg:1491573786 - Fri Apr  7 16:03:06 2017 - CRC:D53B

    - [EPCCCS-1550] Fix code updating with terminal

Class:60 - BootProg:1490697776 - Tue Mar 28 12:42:56 2017 - CRC:445C

    - [EPCCCS-1550] Last changes of this issue

Class:60 - BootProg:1490695291 - Tue Mar 28 12:01:31 2017 - CRC:0995

    - [EPCCCS-1550] Get device name, GW name and class ID from NameDB

Class:60 - BootProg:1489512006 - Tue Mar 14 18:20:06 2017 - CRC:C0F2

    - [EPCCCS-xxxx] FGC_63 associate DSP_THYRISTOR to class 63

Class:60 - BootProg:1487683033 - Tue Feb 21 14:17:13 2017 - CRC:8E87

    - [EPCCCS-4942] Add global reset after boot is flashed.

Class:60 - BootProg:1486468073 - Tue Feb  7 12:47:53 2017 - CRC:3ED3

    - [EPCCCS-4928] Add new PLD revision 3 for FGC3 3.1.3

Class:60 - BootProg:1484816628 - Thu Jan 19 10:03:48 2017 - CRC:B539

    - [EPCCCS-4843] Remove DSP test from self-tests

Class:60 - BootProg:1484563980 - Mon Jan 16 11:53:00 2017 - CRC:75BE

    - [EPCCCS-4828] Increment the FGC3 Main code size

Class:60 - BootProg:1481805072 - Thu Dec 15 13:31:12 2016 - CRC:E386

    - [EPCCCS-4777] Change expected ADC values to match new sampling rate
    - [EPCCCS-1550] Improve FGC3 boot management of codes

Class:60 - BootProg:1480587302 - Thu Dec  1 11:15:02 2016 - CRC:AC35

    - [EPCCCS-4071] Fix DACS and ADCS self tests in Boot

Class:60 - BootProg:1480576255 - Thu Dec  1 08:10:55 2016 - CRC:D96C

    - [EPCCCS-4707] Move DimDB to 128 KB memory block due to lack of space. Expand CompDB and SysDB.

Class:60 - BootProg:1479896111 - Wed Nov 23 11:15:11 2016 - CRC:FDD6

    - [EPCCCS-4687] Fixed to always go to Main after a reset, unless TEST_IN_BOOT specified

Class:60 - BootProg:1476106160 - Mon Oct 10 15:29:20 2016 - CRC:8640

    - [EPCCCS-4505] Fixed value for MACAO in crate_type/fgc3.xml for Bootloader

Class:60 - BootProg:1476103613 - Mon Oct 10 14:46:53 2016 - CRC:0086

    - [EPCCCS-4444] Fix FGC.NUM_RESETS showing wrong values after power cycle
    - [EPCCCS-4113] Fix code menu showing misleading resident code

Class:60 - BootProg:1465898803 - Tue Jun 14 12:06:43 2016 - CRC:09E7

    - [EPCCCS-3773] Adapt 60 and 30 codes to M16C62P

Class:60 - BootProg:1464363467 - Fri May 27 17:37:47 2016 - CRC:0F2F

    - [EPCCCS-3972] Increase the message refresh rate when programming via the serial port

Class:60 - BootProg:1463673941 - Thu May 19 18:05:41 2016 - CRC:7973

    - [EPCCCS-1304] Reprogramming a code using the serial interface does not work
    - [EPCCCS-2835] Redefine warnings, faults, latched and unlatched status

Class:60 - BootProg:1440573610 - Wed Aug 26 09:20:10 2015 - CRC:4DAA

    - Do not stay in Boot for the Modulator converters

Class:60 - BootProg:1437468603 - Tue Jul 21 10:50:03 2015 - CRC:35C0

    - [EPCCCS-2628] Report to runlog if a reset occurred due to a slow/fast watchdog

Class:60 - BootProg:1435912817 - Fri Jul  3 10:40:17 2015 - CRC:ACD8

    - [EPCCCS-2405] Review which conditions prevent the Boot from going to Main

Class:60 - BootProg:1429192642 - Thu Apr 16 15:57:22 2015 - CRC:D003

    - [EPCCCS-2143] Added Cobalt crate type

Class:60 - BootProg:1426247117 - Fri Mar 13 12:45:17 2015 - CRC:061D

    - [EPCCCS-2222] FGC3 stays in boot after FPGA reprogramming
    - [EPCCCS-1430] Do not boot if fgc_ether is not up, unless standalone dongle is plugged or ctrl+c is pressed
    - [EPCCS-2051] Keep FGC3 analogue interface heater active in the boot (Need upgrade of main as well)

Class:60 - BootProg:1422007543 - Fri Jan 23 11:05:43 2015 - CRC:EB21

    - [EPCCCS-2010] Fix Manual test of SciVS bus does not work when communicating with DSP

Class:60 - BootProg:1420735962 - Thu Jan  8 17:52:42 2015 - CRC:258D

    - [EPCCCS-1921] Make the Boot move to the Main for Mididscap and Maxidicap converters

Class:60 - BootProg:1416913859 - Tue Nov 25 12:10:59 2014 - CRC:597C

    - [EPCCCS-1406] Rename ANA102 into ANA103

Class:60 - BootProg:1408704636 - Fri Aug 22 12:50:36 2014 - CRC:CD5B

    - Boot now associates Maxidiscap, Modulator and HMinus with Class 62
    - [FGC-301] Fix reset after boot reprogramming

Class:60 - BootProg:1406899837 - Fri Aug  1 15:30:37 2014 - CRC:E6C2

    - [FGC-343] The Boot int the Midiscap should not automatically go to the Main
    - [FGC-324] Code version not updated immediately when run from command menu

Class:60 - BootProg:1406620016 - Tue Jul 29 09:46:56 2014 - CRC:D9C5

    - [FGCD-177] Boot for klystron modulator shows wrong analogue card settings

Class:60 - BootProg:1405938152 - Mon Jul 21 12:22:32 2014 - CRC:9965

    - [EPCCCS-39] Boot - Klystron now shows Ik and Uk

Class:60 - BootProg:1403859202 - Fri Jun 27 10:53:22 2014 - CRC:5FB2

    - [FGC-285] CANCUN does not jump automatically to main

Class:60 - BootProg:1402062614 - Fri Jun  6 15:50:14 2014 - CRC:BC05 - MCU_BUILD_VERSION:1402062614

    - MiDiDisCap the acquired current is taken at the trigger point

Class:60 - BootProg:1401983172 - Thu Jun  5 17:46:12 2014 - CRC:3EBC - MCU_BUILD_VERSION:1401983172

    - MiDiDisCap show again Iref

Class:60 - BootProg:1401956249 - Thu Jun  5 10:17:29 2014 - CRC:81CC - MCU_BUILD_VERSION:1401956249

    - MaxiDisCap NVRAM parameters relocated to unused space
    - increase timeout from 20ms to 25ms for M16C62 SwitchProg (reboot)

Class:60 - BootProg:1401460160 - Fri May 30 16:29:20 2014 - CRC:2E2D - MCU_BUILD_VERSION:1401460160

    - [FGC-276] Code for class 62 has the wrong code identifier

Class:60 - BootProg:1400742710 - Thu May 22 09:11:50 2014 - CRC:CD84 - MCU_BUILD_VERSION:1400742710

    - [FGC-7] Review LED behaviour

Class:60 - BootProg:1400081525 - Wed May 14 17:32:05 2014 - CRC:0CD8 - MCU_BUILD_VERSION:1400081525

    - [FGC-248] Erasing of the NVR is not done correctly in the BOOT

Class:60 - BootProg:1398770944 - Tue Apr 29 13:29:04 2014 - CRC:F6A1 - MCU_BUILD_VERSION:1398770944

    - If the Main restarts the device three times due to the DSP not running, the BOOT refuses to go to the Main.
    - [FGC-229] Boot moves to Main after a slow/fast watchdog is triggered
    - [FGC-36] Boot switches to main even if locked code
    - [FGC-239] Upgrade initialisation of SCIVS (reset is now released by software)

Class:60 - BootProg:1397492261 - Mon Apr 14 18:17:41 2014 - CRC:12F7 - MCU_BUILD_VERSION:1397492261

    - [FGC-212] The boot does not display the right version of PLD in the FGC3 line
    - [EPCCCS-69] Update for MaxiDiscap

Class:60 - BootProg:1395069270 - Mon Mar 17 16:14:30 2014 - CRC:49AF - MCU_BUILD_VERSION:1395069270

    - [FGC-157] Display actual analog interlock threshold on card

Class:60 - BootProg:1394468832 - Mon Mar 10 17:27:12 2014 - CRC:6FDB - MCU_BUILD_VERSION:1394468832

    - [FGC-167] Boot command "k2 Display Thresholds" reads actual registers on analog card
    - [EPCCCS-61] Add a new manual test function to access SCIVS RX/TX buffers

Class:60 - BootProg:1393931848 - Tue Mar  4 12:17:28 2014 - CRC:7923 - MCU_BUILD_VERSION:1393931848

    - [FGC-162] Serial communication does not work with address 127

Class:60 - BootProg:1393586191 - Fri Feb 28 12:16:31 2014 - CRC:A3E1 - MCU_BUILD_VERSION:1393586191

    - [FGC-149] Reset the FGC if the DSP is not running
    - [FGC-143] In the boot display, show the retrieved ID number. Do not zero it when it is outside the expected range.

Class:60 - BootProg:1392373384 - Fri Feb 14 11:23:04 2014 - CRC:4C1F - MCU_BUILD_VERSION:1392371872

    - [FGC-137] The IDProg, PTDB and IDDB versions are 16 bit values instead of 32 bit values

Class:60 - BootProg:1392287765 - Thu Feb 13 11:36:05 2014 - CRC:C0FB - MCU_BUILD_VERSION:1392287806

    - [FGC-123] Improve boot hardware characteristics on the display

Class:60 - BootProg:1390311154 - Tue Jan 21 14:32:34 2014 - CRC:F46D - MCU_BUILD_VERSION:1390309696

    - Fix dsp timeout at startup
    - Read current measurement on flat top for Power Converter menus

Class:60 - BootProg:1385720612 - Fri Nov 29 11:23:32 2013 - CRC:9FA7 - MCU_BUILD_VERSION:1385720547

    - [Bug fix] Fix reading of ADCs

Class:60 - BootProg:1385565759 - Wed Nov 27 16:22:39 2013 - CRC:0266 - MCU_BUILD_VERSION:1385565700

    - [New Feature] Scan of RegFGC3 cards work regardless the card version for diagnosis

Class:60 - BootProg:1385483181 - Tue Nov 26 17:26:21 2013 - CRC:7D57 - MCU_BUILD_VERSION:1385481473

    - [New Feature] Enlarge the reception buffer of the console to receive files up to 8kb
    - [Bug fix] Improve boot stability
    - [New Feature] Read state control status for MaxiDisCap
    - [New Feature] Various updates for "Drive power converter" commands

Class:60 - BootProg:1383310699 - Fri Nov  1 13:58:19 2013 - CRC:61CD - MCU_BUILD_VERSION:1383304754


    - [Bug fix] Enlarge the maximum file size sent to the console (from 256 to 8192 bytes)
    - [New Feature] Update boot functions for MaxiDisCap
    - [Bug Fix] Switch to main automatically even if no DIM

Tue Oct 22 15:03:59 2013 - BootProg:1382447036 - CRC:ECBD - MCU_BUILD_VERSION:1382446708


    - [New Feature] Include boot commands to drive power converters (branch L4)
    - [Bug fix] Code update compatible with legacy gateway

Fri Oct 11 12:15:33 2013 - BootProg:1381486530 - CRC:00F2 - MCU_BUILD_VERSION:1381484440

    - [Bug fix] Fix dsp boot issue after power cycle
    - [Bug fix] Fix timeout at start up without ethernet cable
    - [New Feature] Enable programming of FPGA 1400


Fri Sep 27 16:05:24 2013 - BootProg:1380290722 - CRC:6F44 - MCU_BUILD_VERSION:1380289688
    - First build for release on op/60/2013 based on master
