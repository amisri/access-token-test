#pragma once


// ****************************************************************
// ******* Bit operations *****************************************

// Return a value with all bytes in the 32 bit argument swapped.  
#define bs32(x)  \
    ((((x) & 0xff000000) >> 24) | \
     (((x) & 0x00ff0000) >>  8) | \
     (((x) & 0x0000ff00) <<  8) | \
     (((x) & 0x000000ff) << 24))

// Bit field 
#define bf(x)                  ( 1 << (x) )

// Bit operations on 32 bit registers
#define b32Set(reg, mask)      ( (*(volatile uint32_t *)(reg)) |=  (mask) )
#define b32Clear(reg, mask)    ( (*(volatile uint32_t *)(reg)) &= ~(mask) )

#define b32IsOne(reg, mask)    ( ((*(volatile uint32_t *)(reg)) & (mask)) == (mask) )

#define b32In(reg)             ( (*(volatile uint32_t *)(reg)) ) 
#define b32Out(reg, val)       ( (*(volatile uint32_t *)(reg)) = (uint32_t)(val) ) 


// ****************************************************************
// ******* GPIO registers defines *********************************

#define F_GPIO_1_BASE 0x2300000           // Base address 
#define F_GPIO_2_BASE 0x2310000           // Base address 
#define F_GPIO_3_BASE 0x2320000           // Base address 
#define F_GPIO_4_BASE 0x2330000           // Base address 

#define F_GPDIR 0x0                       // Offset: GPIO direction
#define F_GPODR 0x4                       // Offset: GPIO open drain
#define F_GPDAT 0x8                       // Offset: GPIO data


// ****************************************************************
// ******* GPIO pins defines **************************************

#define F_DEV_0   bs32(bf(31 - 18))       // MCU_DEV_GPIO0        GPIO 3.18
#define F_DEV_1   bs32(bf(31 - 17))       // MCU_DEV_GPIO1        GPIO 3.17
#define F_DEV_2   bs32(bf(31 - 16))       // MCU_DEV_GPIO2        GPIO 3.16
#define F_DEV_3   bs32(bf(31 - 15))       // MCU_DEV_GPIO3        GPIO 3.15
#define F_DEV_4   bs32(bf(31 - 19))       // MCU_DEV_GPIO3        GPIO 3.19
#define F_DEV_5   bs32(bf(31 - 20))       // MCU_DEV_GPIO3        GPIO 3.20

#define F_DDR_RST bs32(bf(31 - 12))       // DDR RESET            GPIO 3.12
#define F_DDR_SA0 bs32(bf(31 - 11))       // DDR SOCKET ADDR 0    GPIO 3.11
#define F_DDR_SA1 bs32(bf(31 - 10))       // DDR SOCKET ADDR 1    GPIO 3.10
#define F_DDR_SA2 bs32(bf(31 -  9))       // DDR SOCKET ADDR 2    GPIO 3.09


#define F_ASLEEP  bs32(bf(31 -  13))      // DDR SOCKET ADDR 2    GPIO 1.13


#define F_ETH_1   bs32(bf(31 -  10))      // ETH1_RESET_BUFF_n    GPIO 4.10
#define F_ETH_2   bs32(bf(31 -  11))      // ETH2_RESET_BUFF_n    GPIO 4.11
#define F_ETH_3   bs32(bf(31 -  12))      // ETH3_RESET_BUFF_n    GPIO 4.12
#define F_ETH_4   bs32(bf(31 -  13))      // ETH4_RESET_BUFF_n    GPIO 4.13


#define F_MDC     bs32(bf(31 -  0))       // EM1 MDC              GPIO 3.00
#define F_MDIO    bs32(bf(31 -  1))       // EM1 MDIO             GPIO 3.01


#define CONF_MEM_PROG_EN     bs32(bf(31 -  25))     //            GPIO 1.25
#define LED_BOOT_RED         bs32(bf(31 -  20))     //            GPIO 1.20



// ****************************************************************
// ******* Other registers ****************************************

#define SCFG_RCWPMUXCR0     0x157040C

#define F_SCFG_1  bs32(bf(31 -  19))       // SCFG_RCWPMUXCR0   IIC3_SCL
#define F_SCFG_2  bs32(bf(31 -  23))       // SCFG_RCWPMUXCR0   IIC3_SCL
#define F_SCFG_3  bs32(bf(31 -  27))       // SCFG_RCWPMUXCR0   IIC3_SCL
#define F_SCFG_4  bs32(bf(31 -  31))       // SCFG_RCWPMUXCR0   IIC3_SCL


// ****************************************************************
// ******* Functions **********************************************


inline void fgcDelay()
{
    //uint32_t amount = 300000000;

    for (volatile uint32_t i = 0; i < 20000000; ++i)
    {

    }
}

inline void fgcInitGpio()
{  

    puts("FGC: Starting GPIO.\n");

    // Set direction as OUTPUTS
    b32Set(F_GPIO_1_BASE + F_GPDIR, LED_BOOT_RED); 


    puts("FGC: Blinking RED led 1.\n");
    b32Set(F_GPIO_1_BASE + F_GPDAT, LED_BOOT_RED);
    fgcDelay();
    b32Clear(F_GPIO_1_BASE + F_GPDAT, LED_BOOT_RED);
    fgcDelay();
    

    puts("FGC: Blinking RED led 2.\n");
    b32Set(F_GPIO_1_BASE + F_GPDAT, LED_BOOT_RED);
    fgcDelay();
    b32Clear(F_GPIO_1_BASE + F_GPDAT, LED_BOOT_RED);
    fgcDelay();
    

    puts("FGC: Blinking RED led 3.\n");
    b32Set(F_GPIO_1_BASE + F_GPDAT, LED_BOOT_RED);
    fgcDelay();
    b32Clear(F_GPIO_1_BASE + F_GPDAT, LED_BOOT_RED);
    fgcDelay();
    

    puts("FGC: Setting CONF_MEM_PROG_EN as output with value of 0 .\n");
    b32Set(F_GPIO_1_BASE + F_GPDIR, CONF_MEM_PROG_EN);
    b32Clear(F_GPIO_1_BASE + F_GPDAT, CONF_MEM_PROG_EN);


    // Set direction as OUTPUTS
    /*b32Set(F_GPIO_3_BASE + F_GPDIR, F_DEV_0);
    b32Set(F_GPIO_3_BASE + F_GPDIR, F_DEV_1);
    b32Set(F_GPIO_3_BASE + F_GPDIR, F_DEV_2);
    b32Set(F_GPIO_3_BASE + F_GPDIR, F_DEV_3);
    b32Set(F_GPIO_3_BASE + F_GPDIR, F_DEV_4);
    b32Set(F_GPIO_3_BASE + F_GPDIR, F_DEV_5); */

    //b32Set(F_GPIO_3_BASE + F_GPDIR, F_DDR_RST); 
    // b32Set(F_GPIO_3_BASE + F_GPDIR, F_DDR_SA0); 
    // b32Set(F_GPIO_3_BASE + F_GPDIR, F_DDR_SA1); 
    // b32Set(F_GPIO_3_BASE + F_GPDIR, F_DDR_SA2); 
  
    //b32Set(F_GPIO_1_BASE + F_GPDIR, F_ASLEEP); 



    // Set default values of pins
   /* b32Clear(F_GPIO_3_BASE + F_GPDAT, F_DEV_0);
    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_DEV_1);
    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_DEV_2);
    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_DEV_3);
    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_DEV_4);
    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_DEV_5);*/
   
    //b32Clear(F_GPIO_3_BASE + F_GPDAT, F_DDR_RST);
      // b32Set(F_GPIO_3_BASE + F_GPDAT, F_DDR_SA0);
    // b32Clear(F_GPIO_3_BASE + F_GPDAT, F_DDR_SA1);
    // b32Clear(F_GPIO_3_BASE + F_GPDAT, F_DDR_SA2);

    //b32Set(F_GPIO_1_BASE + F_GPDAT, F_ASLEEP); 

/*
    printf("\nSCFG_RCWPMUXCR0              Value: %d \n", b32In(SCFG_RCWPMUXCR0));
    printf("F_GPIO_4_BASE + F_GPDIR      Value: %d \n", b32In(F_GPIO_4_BASE + F_GPDIR));
    printf("SCFG_RCWPMUXCR0 + F_GPDAT    Value: %d \n", b32In(F_GPIO_4_BASE + F_GPDAT));



    b32Set(SCFG_RCWPMUXCR0, F_SCFG_1);     
    b32Set(SCFG_RCWPMUXCR0, F_SCFG_2);     
    b32Set(SCFG_RCWPMUXCR0, F_SCFG_3);     
    b32Set(SCFG_RCWPMUXCR0, F_SCFG_4);     

    b32Set(F_GPIO_4_BASE + F_GPDIR, F_ETH_1); 
    b32Set(F_GPIO_4_BASE + F_GPDIR, F_ETH_2); 
    b32Set(F_GPIO_4_BASE + F_GPDIR, F_ETH_3); 
    b32Set(F_GPIO_4_BASE + F_GPDIR, F_ETH_4); 
  
    b32Set(F_GPIO_4_BASE + F_GPDAT, F_ETH_1); 
    b32Set(F_GPIO_4_BASE + F_GPDAT, F_ETH_2); 
    b32Set(F_GPIO_4_BASE + F_GPDAT, F_ETH_3); 
    b32Set(F_GPIO_4_BASE + F_GPDAT, F_ETH_4); 
*/



/*
    printf("SCFG_RCWPMUXCR0              Value: %d \n", b32In(SCFG_RCWPMUXCR0));
    printf("F_GPIO_4_BASE + F_GPDIR      Value: %d \n", b32In(F_GPIO_4_BASE + F_GPDIR));
    printf("SCFG_RCWPMUXCR0 + F_GPDAT    Value: %d \n", b32In(F_GPIO_4_BASE + F_GPDAT));
*/

    /*printf("Address: %d     Value: %d \n", (F_GPIO_3_BASE + F_GPDIR), b32In(F_GPIO_3_BASE + F_GPDIR) );
    printf("Address: %d     Value: %d \n", (F_GPIO_3_BASE + F_GPDAT), b32In(F_GPIO_3_BASE + F_GPDAT) );

    printf("Address: %d     Value: %d \n", (F_GPIO_1_BASE + F_GPDIR), b32In(F_GPIO_1_BASE + F_GPDIR) );
    printf("Address: %d     Value: %d \n", (F_GPIO_1_BASE + F_GPDAT), b32In(F_GPIO_1_BASE + F_GPDAT) );*/







/*
    puts("FGC: MDIO TEST 3.  \n");
 

    b32Set(F_GPIO_3_BASE + F_GPDIR, F_MDIO); 
    b32Set(F_GPIO_3_BASE + F_GPDIR, F_MDC); 
  


    b32Set(F_GPIO_3_BASE + F_GPDAT, F_MDIO); 
    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_MDC); 
    fgcDelay();

    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_MDIO); 
    b32Set(F_GPIO_3_BASE + F_GPDAT, F_MDC); 
    fgcDelay();


    b32Set(F_GPIO_3_BASE + F_GPDAT, F_MDIO); 
    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_MDC); 
    fgcDelay();


    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_MDIO); 
    b32Set(F_GPIO_3_BASE + F_GPDAT, F_MDC); 
    fgcDelay();
    fgcDelay();
    fgcDelay();

    b32Set(F_GPIO_3_BASE + F_GPDAT, F_MDIO); 
    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_MDC); 
    fgcDelay();

    b32Clear(F_GPIO_3_BASE + F_GPDAT, F_MDIO); 
    b32Set(F_GPIO_3_BASE + F_GPDAT, F_MDC); 
    fgcDelay();


    b32Clear(F_GPIO_3_BASE + F_GPDIR, F_MDIO); 
    b32Clear(F_GPIO_3_BASE + F_GPDIR, F_MDC); 
  
*/




    
    puts("FGC: GPIO configured.  \n");
 



}

/*
void fgcPinState(const uint32_t base, const uint32_t pin, const char state)
{
    if (state == 0)
    {
        b32Clear(base + F_GPDAT, pin);  
    }
    else
    {
        b32Set(base + F_GPDAT, pin);
    }   
}
*/
/*
void fgcDev0(const char state)
{
    fgcPinState(F_GPIO_3_BASE, F_DEV_0, state);
}


void fgcDdrReset(const char state)
{
    fgcPinState(F_GPIO_3_BASE, F_DDR_RST, state);
}

void fgcAsleep(const char state)
{
    fgcPinState(F_GPIO_1_BASE, F_ASLEEP, state);
}*/