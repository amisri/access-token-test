#!/bin/bash

read -r -d '' FGC_FILE_INITTAB << FGC_END_OF_FILE
# /etc/inittab
#
# Copyright (C) 2001 Erik Andersen <andersen@codepoet.org>
#
# Note: BusyBox init doesn't support runlevels.  The runlevels field is
# completely ignored by BusyBox init. If you want runlevels, use
# sysvinit.
#
# Format for each entry: <id>:<runlevels>:<action>:<process>
#
# id        == tty to run on, or empty for /dev/console
# runlevels == ignored
# action    == one of sysinit, respawn, askfirst, wait, and once
# process   == program to run

# Startup the system
::sysinit:/bin/mount -t proc proc /proc
::sysinit:/bin/mount -o remount,rw /
::sysinit:/bin/mkdir -p /dev/pts /dev/shm
::sysinit:/bin/mount -a
::sysinit:/sbin/swapon -a
null::sysinit:/bin/ln -sf /proc/self/fd /dev/fd
null::sysinit:/bin/ln -sf /proc/self/fd/0 /dev/stdin
null::sysinit:/bin/ln -sf /proc/self/fd/1 /dev/stdout
null::sysinit:/bin/ln -sf /proc/self/fd/2 /dev/stderr
::sysinit:/bin/hostname -F /etc/hostname

# Run any rc scripts
::sysinit:/etc/init.d/rcS

# Run FGC start-up
::sysinit:/bin/bash /bin/fgc_launcher.sh &

# Auto login as root
console::respawn:-/bin/login -f root

# Stuff to do for the 3-finger salute
#::ctrlaltdel:/sbin/reboot

# Stuff to do before rebooting
::shutdown:/etc/init.d/rcK
::shutdown:/sbin/swapoff -a
::shutdown:/bin/umount -a -r

FGC_END_OF_FILE




read -r -d '' FGC_FILE_PROFILE << FGC_END_OF_FILE
#Aliases:
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias  lsl="ls -lh"
alias   ll="ls -lh"
alias  lsa="ls -lha"
alias    c="clear"
alias   cl="clear; ls -lh"
alias  cdc="cd .."
alias    e="exit"

$Prompt:
export PS1="\e[0;32m[\h:\e[1;33m\w\e[0;32m]# \e[m"

FGC_END_OF_FILE





###########################
### Entry point
###########################

# Working directory - buildroot main
# Argument 1        - target path


# The target path is passed as the first argument
TARGET="$1"

# Inform about execution of this script
echo "FGC3.2: POST BUILD SCRIPT"


# Write out the inittab file
echo "FGC3.2: Generate inittab file."

echo "$FGC_FILE_INITTAB" > "${TARGET}/etc/inittab"

if [ $? -ne 0 ] ; then
	exit 1
fi


# Write out the profile file
echo "FGC3.2: Append variables to /etc/profile file."

echo "$FGC_FILE_PROFILE" >> "${TARGET}/etc/profile"

if [ $? -ne 0 ] ; then
	exit 1
fi


# Copy launcher into bin directory
echo "FGC3.2: Copy launcher into bin directory."
cp -r "output/fgc_launcher/." "${TARGET}/bin/"

if [ $? -ne 0 ] ; then
	exit 2
fi


# Copy FGC directory into root directory
echo "FGC3.2: Copy FGC software into root directory."
cp -r "output/fgc_sw/." "${TARGET}/root/"

if [ $? -ne 0 ] ; then
	exit 3
fi


# Copy kernel modules into root directory
echo "FGC3.2: Copy kernel modules into root directory."
cp -r "output/fgc_modules/." "${TARGET}/root/"

if [ $? -ne 0 ] ; then
	exit 4
fi

# The builder script will add here a line of code that will print
# the tag number and build date into the file "${TARGET}/root/tag.txt"
