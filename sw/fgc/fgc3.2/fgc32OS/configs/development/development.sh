#!/bin/bash

# This file can be placed inside a folder "init_extensions" on the DATA partition.
# It will then be executed by the Launcher.
# It configures DHCP, SSH etc. to make development easier.
#
# It is not supposed to be present on production images.

# Import printing utils
source /bin/fgc_utils.sh


fgcLine
fgcInfo "Development script executed"


# This has to be removed from here. Or rather the MAC address itself has to be in a separate file
fgcInfo "Change MAC addres of ethB to 00:04:9F:05:3E:4A"
ifconfig ethB hw ether 00:04:9F:05:3E:4A

# Enable SSH
fgcInfo "Enable SSH client (dropbear)"
/etc/init.d/S50dropbear start

# Enable DHCP
fgcInfo "Enable DHCP on ETH B interface"
dhclient -v ethB


