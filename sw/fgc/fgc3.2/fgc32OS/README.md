# FGC3.2 Operating System 


## 1. Directories

Permanent:
- **configs** - contains files used to configure FGC3.2 OS components (like u-boot, kernel, etc.). Those files are used to build and configure the whole OS. 
- **scripts** - Python 3 scripts used to build and configure OS.

Temporary:
- **buildroot** - when you fetch the Buildroot repository, it is placed here.
- **bin** - when you build images, they are placed here.



## 2. How to use

Simply type `make` from this directory. This will fetch and build everything.

To configure or perform other actions, type `make config`. The script will show you simple GUI where you can choose an action.

Using builder script requires that *ncurses* library and *Python 3* are installed.



## 3. Documentation

Run the builder script (`make config`) and choose option "Documentation" to open Wiki page with detailed description of the OS, how it works and how to build it.