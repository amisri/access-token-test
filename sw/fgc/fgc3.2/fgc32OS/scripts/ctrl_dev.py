#!/usr/bin/python3

"""
    FGC3.2 OS - Base class for controllers for the main script.
"""

import curses
from curses import *
from ctrl import Controller
import common_consts as consts
import utils_exec as exe
import common_paths as paths
from ctrl_images import *
from ctrl_buildroot import *


class DevCtrl(Controller):

    dev = "/dev/"

    def __init__(self):
        super().__init__("Development tools",
        [
            { "name"   : "Clear the SD card and create and format partitions",
              "help"   : "Clears the SD card with 0, creates partitions and format them.",
              "action" : self.dev_part
            },
            { "name"   : "Flash firmware to the SD card",
              "help"   : "Composes firmware image and flashes it to the SD card.",
              "action" : self.dev_firmware
            },
            { "name"   : "Flash kernel to the SD card",
              "help"   : "Composes ITB files and flashes it to the SD card.",
              "action" : self.dev_kernel
            },
            { "name"   : "Flash FGC software to the SD card",
              "help"   : "Copies FGC software directory to the SD card.",
              "action" : self.dev_fgc
            },
            { "name"   : "Rebuild u-boot, compose and flash the firmware, umount the SD card",
              "help"   : "",
              "action" : self.dev_task1
            },
            { "name"   : "Rebuild kernel, assembly ITB, flash the Kernel, rebuild u-boot, compose and flash the firmware, umount the SD card",
              "help"   : "",
              "action" : self.dev_task2
            },
            { "name"   : "Build buildroot, rebuild kernel, assembly ITB, flash the Kernel, rebuild u-boot, compose and flash the firmware, umount the SD card",
              "help"   : "",
              "action" : self.dev_task3
            },
            { "name"   : "Copy development extension into an SD card",
              "help"   : "",
              "action" : self.copy_dev
            },
        ])

    @staticmethod
    def ask_for_device(win):


        while True:
            # Clear screen
            win.stdscr.clear()
            win.pad0.clear()
            win.stdscr.refresh()

            # Print info and typed name of the device
            y = 0

            win.pad0.addstr(y, 0, "Type device path: ", color_pair(consts.color_bcg))
            y += 2
            win.pad0.addstr(y, 5, " {} ".format(DevCtrl.dev), color_pair(consts.color_bcg) | A_REVERSE)

            y += 3
            win.pad0.addstr(y, 0, "Press ENTER to proceed.", color_pair(consts.color_bcg) | A_BOLD)
            y += 1
            win.pad0.addstr(y, 0, "Press any other key (beside numbers and letters) to return.", color_pair(consts.color_bcg))

            y += 3
            win.pad0.addstr(y, 0, "List of bulk devices:", color_pair(consts.color_bcg))

            # Print device list (this may safely fail, if there is no 'lsblk' command)
            lsblk = exe.run_command(win, "lsblk | grep sd", True).splitlines()
            y += 1

            for line in lsblk:
                y += 1
                win.pad0.addstr(y, 5, line, color_pair(consts.color_bcg))



        # Draw pad
            win.drawPad(9, 1, 1)

            # Process user input
            c = win.stdscr.getch()

            if (   (ord('0') <= c <= ord('9')) or (ord('a') <= c <= ord('z')) or (ord('A') <= c <= ord('Z'))
                or  ord('/') == c):
                DevCtrl.dev += chr(c)

            elif c == curses.KEY_BACKSPACE:
                DevCtrl.dev = DevCtrl.dev[:-1]

            elif c == curses.KEY_ENTER or c == 10 or c == 13:
                return True

            else:
                return False


    def flash_kernel(self, win):
        # Unmount system (1) partition of the SD card (may safely fail if they are not mounted)
        exe.run_command(win, "sudo umount {}".format(self.dev + "1"), True)

        # Create temporary directory (to be used as a mounting point)
        temp_dir = paths.bin_dir + "/temp_dir"
        exe.run_command(win, "mkdir -p {}".format(temp_dir))

        try:
            # Mount first, system partition to the temporary directory
            exe.run_command(win, "sudo mount {} {}".format(self.dev + "1", temp_dir))

            try:
                # Copy the ITB file there
                exe.run_command(win, "sudo cp {} {}".format(paths.kernel_out_img.format("0"), temp_dir))

            finally:
                # Unmount the first partition
                exe.run_command_gui(win, "sudo umount {}".format(self.dev + "1"))

        finally:
            exe.run_command(win, "rm -r {}".format(temp_dir))


    def flash_fgc(self, win):
        # Unmount FGC (2) partition of the SD card (may safely fail if they are not mounted)
        exe.run_command(win, "sudo umount {}".format(self.dev + "2"), True)

        # Create temporary directory (to be used as a mounting point)
        temp_dir = paths.bin_dir + "/temp_dir"
        exe.run_command(win, "mkdir -p {}".format(temp_dir))

        try:
            # Mount second, FGC partition to the temporary directory
            exe.run_command(win, "sudo mount {} {}".format(self.dev + "2", temp_dir))
            try:
                # Copy the files there (may safely fail if paths.fgc_sw doesn't exist)
                exe.run_command(win, "sudo cp -r {} {}".format(paths.fgc_sw, temp_dir), True)
                exe.run_command(win, "sudo cp -r {} {}".format(paths.fgc_mod, temp_dir), True)

            finally:
                # Unmount the second partition
                exe.run_command_gui(win, "sudo umount {}".format(self.dev + "2"))

        finally:
            exe.run_command(win, "rm -r {}".format(temp_dir))



    def dev_part(self, win):
        # Ask user for the path to the device and confirmation
        if self.ask_for_device(win):
            # Unmount all partitions of the SD card (may safely fail if they are not mounted)
            exe.run_command(win, "sudo umount {}*".format(self.dev), True)

            # Create output dir (if not present)
            exe.run_command(win, "mkdir -p {}".format(paths.bin_dir))

            # Clear raw area with zeros
            exe.run_command_gui(win, "sudo dd of={} if=/dev/zero bs=512 count={} status=progress".format(self.dev, layout.mb_to_sectors(layout.part_raw_size) ))

            # Create partitions on that SD card
            ImageCtrl.make_part(win, self.dev)

            # Format partitions
            ImageCtrl.format_part(win, self.dev, "1", "2", "3")

            # Inform the user
            exe.inform_about_success(win, "formatting partitions")




    def dev_firmware(self, win):
        # Check that all required files exist
        if  exe.file_exists(win, paths.firmware_out_img.format("0"), "Assemble firmware image first."):
            # Ask user for the path to the device and confirmation
            if self.ask_for_device(win):
                # Copy firmware image at the right place
                exe.run_command_gui(win, "sudo dd of={} if={} bs=512 seek={} status=progress".format(self.dev, paths.firmware_out_img.format("0"), layout.offset_firmware))

                exe.inform_about_success(win, "flashing firmware")



    def dev_kernel(self, win):
        # Check that all required files exist
        if exe.file_exists(win, paths.kernel_out_img.format("0"), "Assemble ITB image first."):
            # Ask user for the path to the device and confirmation
            if self.ask_for_device(win):
                self.flash_kernel(win)
                exe.inform_about_success(win, "flashing Kernel")


    def dev_fgc(self, win):
        # Ask user for the path to the device and confirmation
        if self.ask_for_device(win):
            self.flash_fgc(win)
            exe.inform_about_success(win, "flashing FGC software")



    def dev_task1(self, win):
        # Ask user for the path to the device and confirmation
        if self.ask_for_device(win):
            # Copy RCW
            BuildrootCtrl.copyConfigs(win, "", False)

            # Rebuild u-boot
            exe.run_command_gui(win, "make -C {} uboot-reconfigure".format(paths.buildroot))

            # Assemble firmware
            ImageCtrl.as_firmware(win)

            # Copy firmware image at the right place
            exe.run_command_gui(win, "sudo dd of={} if={} bs=512 seek={} status=progress".format(self.dev, paths.firmware_out_img.format("0"), layout.offset_firmware))

            # Unmount all partitions of the SD card (may safely fail if they are not mounted)
            exe.run_command(win, "sudo umount {}*".format(self.dev), True)

            exe.inform_about_success(win, "rebuilding and flashing firmware")



    def dev_task2(self, win):
        # Ask user for the path to the device and confirmation
        if self.ask_for_device(win):
            # Copy RCW
            BuildrootCtrl.copyConfigs(win, "", False)

            # Rebuild u-boot
            exe.run_command_gui(win, "make -C {} uboot-reconfigure".format(paths.buildroot))

            # Assemble firmware
            ImageCtrl.as_firmware(win)

            # Rebuild Kernel
            exe.run_command_gui(win, "make -C {} linux-reconfigure".format(paths.buildroot))
            BuildrootCtrl.compressKernel(win)

            # Assemble ITB
            ImageCtrl.as_itb(win)

            # Copy ITB image to the SYSTEM partition
            self.flash_kernel(win)

            # Copy FGC software directory to FGC partition
            self.flash_fgc(win)

            # Copy firmware image at the right place
            exe.run_command_gui(win, "sudo dd of={} if={} bs=512 seek={} status=progress".format(self.dev, paths.firmware_out_img.format("0"), layout.offset_firmware))

            # Unmount all partitions of the SD card (may safely fail if they are not mounted)
            exe.run_command(win, "sudo umount {}*".format(self.dev), True)

            exe.inform_about_success(win, "rebuilding and flashing ITB and firmware")


    def dev_task3(self, win):
        # Ask user for the path to the device and confirmation
        if BuildrootCtrl.checkFgc(win) and self.ask_for_device(win):
            # Copy RCW and others
            BuildrootCtrl.copyConfigs(win, "")

            # Rebuild buildroot
            exe.run_command_gui(win, "make -C {} ".format(paths.buildroot))

            # Assemble firmware
            ImageCtrl.as_firmware(win)

            # Compress Kernel
            BuildrootCtrl.compressKernel(win)

            # Assemble ITB
            ImageCtrl.as_itb(win)

            # Copy ITB image to the SYSTEM partition
            self.flash_kernel(win)

            # Copy FGC software directory to FGC partition
            self.flash_fgc(win)

            # Copy firmware image at the right place
            exe.run_command_gui(win, "sudo dd of={} if={} bs=512 seek={} status=progress".format(self.dev, paths.firmware_out_img.format("0"), layout.offset_firmware))

            # Unmount all partitions of the SD card (may safely fail if they are not mounted)
            exe.run_command(win, "sudo umount {}*".format(self.dev), True)

            exe.inform_about_success(win, "rebuilding and flashing ITB and firmware")




    def copy_dev(self, win):
        # Ask user for the path to the device and confirmation
        if self.ask_for_device(win):
            # Unmount DATA (3) partition of the SD card (may safely fail if they are not mounted)
            exe.run_command(win, "sudo umount {}".format(self.dev + "3"), True)

            # Create temporary directory (to be used as a mounting point)
            temp_dir = paths.bin_dir + "/temp_dir"
            exe.run_command(win, "mkdir -p {}".format(temp_dir))

            try:
                # Mount third, DATA partition to the temporary directory
                exe.run_command(win, "sudo mount {} {}".format(self.dev + "3", temp_dir))

                try:
                    # Create a proper directory on the partition
                    exe.run_command(win, "sudo mkdir -p {}".format(temp_dir + "/init_extensions"))

                    # Copy the files there
                    exe.run_command(win, "sudo cp -r {} {}".format(paths.dev_ext, temp_dir + "/init_extensions"))

                finally:
                    # Unmount the second partition
                    exe.run_command_gui(win, "sudo umount {}".format(self.dev + "3"))

            finally:
                exe.run_command(win, "rm -r {}".format(temp_dir))

            exe.inform_about_success(win, "coping development extension")


