#!/usr/bin/python3

"""
    FGC3.2 OS - Base class for controllers for the main script.
"""

from ctrl import Controller
from curses import *
import utils_exec as exec
import common_paths as paths


class HelpCtrl(Controller):

    def __init__(self):
        super().__init__("Help",
        [
            { "name"   : "About",
              "help"   : "Displays information about the program.",
              "action" : self.act_about
            },
            { "name"   : "Running the script without user interface",
              "help"   : "List command-line commands to run the script without this GUI.",
              "action" : self.act_cmd_line_info
            },
            { "name"   : "Documentation",
              "help"   : "Opens Wiki documentation of this program and FGC3.2 OS in general.",
              "action" : self.act_docs
            }
        ])


    def act_about(self, win):
        win.stdscr.addstr(2, 2, "This script is used to configure and build Linux system for FGC 3.2.")

        win.stdscr.addstr(4, 2, "This is a user-friendly GUI version.")
        win.stdscr.addstr(5, 2, "See \"Running the script without user interface\" for command-line version.")

        win.stdscr.addstr(7, 2, "Select \"Documentation\" in the main menu to see how to use the script.")

        win.stdscr.addstr(10, 2, " Press any key to return. ", A_BOLD | A_REVERSE)
        win.stdscr.getch()


    def act_cmd_line_info(self, win):
        win.stdscr.addstr(2, 2, "You can use parts of this script from the command line, without this user interface.")

        win.stdscr.addstr(4, 2, "./builder --full       Fetch, build everything and assemble composite image")

        win.stdscr.addstr(7, 2, "You can also execute --full option when running make from the main directory.")

        win.stdscr.addstr(10, 2, " Press any key to return. ", A_BOLD | A_REVERSE)
        win.stdscr.getch()


    def act_docs(self, win):
        exec.open_by_os(paths.wiki_docs)
        super().info_and_wait(win, "Documentation wiki has been opened. Press any key to return.")




