#!/usr/bin/python3

"""
    FGC3.2 OS - Base class for controllers for the main script.
"""

import os
import curses
from ctrl import Controller
import common_consts as consts
import utils_exec as exe
import common_paths as paths


class FetchCtrl(Controller):

    def __init__(self):
        super().__init__("Fetch resources",
        [
            { "name" : "Fetch and build buildroot",
              "help" : "Removes buildroot directory, copies it again and builds it with default configs.",
              "action" : self.fetch
            },
            { "name" : "Build with default configuration",
              "help" : "Builds buildroot with default configuration (without FGC3.2 configs and overlay). This options should only be used when changing uboot/kernel versions. See docs.",
              "action": self.build_default
            },
            { "name" : "Prepare buildroot to be pushed upstream (clean)",
              "help" : "Cleans buildroot directory (make clean). Do this before copying buildroot directory to the place from which it is fetched. See docs.",
              "action": self.clean
            }
        ])

    @staticmethod
    def do_fetch(win):
        # First remove the buildroot directory (if it exists)
        if os.path.isdir(paths.buildroot):
            exe.run_command_gui(win, "rm -rf {}".format(paths.buildroot))

        # Create directory
        exe.run_command_gui(win, "mkdir -p {}".format(paths.buildroot))

        # Check if the path from where to download Buildroot exist
        if os.path.isdir(paths.buildroot_fetch):
            # If so, then simply copy it to a proper path
            exe.run_command_gui(win, "rsync -r --info=progress2 {}/* {}".format(paths.buildroot_fetch, paths.buildroot))
        else:
            # Otherwise, we can try to copy it using SCP and some server (defined in common_paths.py)
            exe.run_command_gui(win, "rsync -r --info=progress2 -e ssh {}:{}/* {}".format(paths.buildroot_fetch_server, paths.buildroot_fetch, paths.buildroot))

        # Copy Buildroot config (this on is required to build anything)
        exe.run_command(win, "cp {} {}".format(paths.overlay + paths.buildroot_config_target, paths.buildroot + paths.buildroot_config_target))

        # Then build the build root with default configurations. This is needed to extract uboot and kernel.
        # See docs for the in-depth explanation.
        exe.run_command_gui(win, "make -C {}".format(paths.buildroot))



    @staticmethod
    def ask_and_do_fetch(win):
        Controller.info(win, "The \"buildroot\" directory will be removed and then downloaded again. ", 3, curses.A_BOLD)
        Controller.info(win, "Press ENTER to continue.", 5)
        Controller.info(win, "Press any other key to cancel.", 6)
        c = win.stdscr.getch()

        if c != curses.KEY_ENTER and c != 10 and c != 13:
            return False

        FetchCtrl.do_fetch(win)
        return True


    @staticmethod
    def check_fetch(win, skip_asking=False):
        if not os.path.isdir(paths.buildroot):
            win.restore()
            if not skip_asking:
                FetchCtrl.info(win, "The \"buildroot\" directory does not exist at its path. ", 3, curses.A_BOLD)
                FetchCtrl.info(win, "That probably means it has not been fetched yet. ", 4, 0)
                FetchCtrl.info(win, "Press ENTER to fetch it now.", 6)
                FetchCtrl.info(win, "Press any other key to cancel.", 7)
                c = win.stdscr.getch()

                if c == curses.KEY_ENTER or c == 10 or c == 13:
                    FetchCtrl.do_fetch(win)        # If that fails, the whole script will be terminated.
                    return True
                else:
                    return False
            else:
                FetchCtrl.do_fetch(win)
                return True
        else:
            return True


    def fetch(self, win):
        if self.ask_and_do_fetch(win):
            exe.inform_about_success(win, "fetching external resources")


    def build_default(self, win):
        if FetchCtrl.check_fetch(win):
            exe.run_command_gui(win, "make -C {}".format(paths.buildroot))
            exe.inform_about_success(win, "building with default config")


    def clean(self, win):
        if FetchCtrl.check_fetch(win):
            exe.run_command_gui(win, "make -C {} clean".format(paths.buildroot))
            exe.inform_about_success(win, "cleaning buildroot")
