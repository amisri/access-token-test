#!/usr/bin/python3

"""
    FGC3.2 OS - Base class for controllers for the main script.
"""

from ctrl import Controller
from ctrl_fetch import *
import utils_exec as exe
import common_paths as paths


class ConfigCtrl(Controller):

    def __init__(self):
        super().__init__("Configure components",
        [
            { "name"   : "Configure RCW",
              "help"   : "Opens a RCW configurator. U-boot rebuilding is necessary after the change.",
              "action" : self.config_rcw
            },
            { "name"   : "Configure memory layout and partitions",
              "help"   : "Opens Python file that defines memory layout and partition sizes.",
              "action" : self.config_mem
            },
            { "name"   : "Configure Buildroot",
              "help"   : "Opens config menu for Buildroot.",
              "action" : self.config_buildroot
            },
            { "name"   : "Configure BusyBox",
              "help"   : "Opens config menu for BusyBOx.",
              "action" : self.config_busybox
            },
            { "name"   : "Configure U-boot",
              "help"   : "Opens config menu for U-boot.",
              "action" : self.config_uboot
            },
            { "name"   : "Configure kernel",
              "help"   : "Opens config menu for Kernel.",
              "action" : self.config_kernel
            },
        ])


    def config_rcw(self, win):
        exe.run_command_gui(win, "./rcw.py")


    def config_mem(self, win):
        if exe.file_exists(win, paths.mem_layout_file):
            exe.open_by_os(paths.mem_layout_file)
            super().info_and_wait(win, "The file has been opened for edit. When edited, press any key to reload.")
            exe.restart_app(win)


    @staticmethod
    def doConfig(win, config, component):
        if FetchCtrl.check_fetch(win):
            # Check that the config files are there (buildroot might be fetched, but if it was not build
            # then the kernel/uboot won't be extracted). See docs for in-depth explanation.
            if exe.file_exists(win, paths.buildroot + config, "Config file not found. Run \"Fetch and build buildroot\"."):
                # Copy the config file from the overlay to the buildroot directory
                exe.run_command(win, "cp {} {}".format(paths.overlay + config, paths.buildroot + config))

                # Run menu-config
                exe.run_command_gui(win, "make -C {} {}".format(paths.buildroot, component))

                # Copy the config file back from the buildroot directory to the overlay
                exe.run_command(win, "cp {} {}".format(paths.buildroot + config, paths.overlay + config))


    def config_buildroot(self, win):
        ConfigCtrl.doConfig(win, paths.buildroot_config_target, "menuconfig")


    def config_busybox(self, win):
        ConfigCtrl.doConfig(win, paths.busybox_config_target, "busybox-menuconfig")


    def config_uboot(self, win):
        ConfigCtrl.doConfig(win, paths.uboot_config_target, "uboot-menuconfig")


    def config_kernel(self, win):
        ConfigCtrl.doConfig(win, paths.kernel_config_target, "linux-menuconfig")

