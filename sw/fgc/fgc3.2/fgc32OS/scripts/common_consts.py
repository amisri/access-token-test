#!/usr/bin/python3

"""
    FGC3.2 OS - Definitions of common constants for different scripts (like Main, RCW...).
"""

from curses import *


color_bcg = 1                       # Background with normal text
color_bcgTitle = 2                  # Background with title text
color_bcgInv = 3                    # Inverse background
color_scroll = 4                    # Scrollbar background
color_scrollHandle = 5              # Scrollbar handle
color_bcgLigth = 6                  # Light background with normal text
color_valueSel = 7                  # Selected value
color_error = 8                     # Error / warning color
color_success = 9                   # Success information


def initCursesColor():
    init_pair(color_bcg, COLOR_WHITE, 24)
    init_pair(color_bcgTitle, COLOR_YELLOW, 24)
    init_pair(color_bcgInv, 24, COLOR_WHITE)
    init_pair(color_scroll, 0, 25)
    init_pair(color_scrollHandle, 0, COLOR_WHITE)
    init_pair(color_bcgLigth, COLOR_WHITE, 25)
    init_pair(color_valueSel , COLOR_BLACK, COLOR_YELLOW)
    init_pair(color_error, COLOR_WHITE, COLOR_RED)
    init_pair(color_success, 10, 24)




