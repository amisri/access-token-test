#!/usr/bin/python3

"""
    FGC3.2 OS - A set of functions facilitating executing other scripts, opening files etc.
"""

import os
import sys
import subprocess
import inspect
from curses import *
import common_consts as consts

class Col:
    INV  = '\033[7m'
    INFO = '\033[94m'
    OK   = '\033[92m'
    FAIL = '\033[1;91m'
    ERR  = '\033[0;31m'
    ENDC = '\033[0m'


def restart_app(win):
    """Restarts this script.
    """
    win.close()
    this_app = sys.executable
    os.execl(this_app, this_app, *sys.argv)


def file_exists(win, file, info = "Check paths or repository integrity."):
    if not os.path.exists(file):
        text = " The file \"{}\" does not exists. ".format(file)
        win.stdscr.addstr(3, (win.width() - len(text)) // 2, text, color_pair(consts.color_error) | A_BOLD)
        win.stdscr.addstr(4, (win.width() - len(info)) // 2, info, color_pair(consts.color_bcg))

        text = " Press any key to return "
        win.stdscr.addstr(6, (win.width() - len(text)) // 2, text, color_pair(consts.color_bcg) | A_BOLD | A_REVERSE)

        win.stdscr.getch()

        return False
    else:
        return True


def open_by_os(path_to_file):
    """Opens given file with a program default for the operating system.  
    """
    subprocess.call(["xdg-open", path_to_file])


def run_command(win, command, may_fall = False):
    """Runs the given command without GUI. Used for commands the user doesn't need to see.
    """
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = "   " + p.communicate()[0].decode(sys.stdout.encoding).replace("\n", "\n   ")
    error_code = p.returncode

    # This kind of internal commands shouldn't fail, but if so, terminate the script and inform the user.
    if not may_fall and error_code != 0:
        win.close()

        # Get info about the file, function and line of the caller
        info = inspect.getframeinfo(inspect.stack()[1][0])

        # Print the command
        print(Col.FAIL + command)

        # In case of error, write out stdout and stderr
        print(Col.ERR + output)

        print(Col.FAIL + "invoked from: {} : {} : {}".format(info.filename, info.function, info.lineno))
        print(Col.INV + "The above command has failed. Exit code {}.".format(error_code) + Col.ENDC)
        win.exit(error_code, "The builder script terminated because of the error. Check repository integrity, permissions, etc. and try again.")

    if error_code != 0:
        return ""
    else:
        return output


def run_command_gui(win, command):
    """Closes curses window, runs the given command and then returns the window. This function
       will return if the command was successful, otherwise the program terminates with an error.
    """
    win.close()

    print(Col.INFO + "\n---------------------------------------------------------------------------" + Col.ENDC)
    print(Col.INFO + Col.INV + "Executing command:" + Col.ENDC + Col.INFO + " " + command + Col.ENDC + "\n")
    error_code = subprocess.call(command, shell=True)

    if error_code == 0:
        print(Col.OK + Col.INV + "\nExit code OK (0)." + Col.ENDC + Col.OK + " " + command + Col.ENDC)
        win.restore()
    else:
        print(Col.FAIL + Col.INV + "\nError. Exit code {}.".format(error_code) + Col.ENDC + Col.FAIL + " " + command + Col.ENDC)
        win.exit(error_code, "The builder script terminated because of the error. Correct the error and try again.")


def inform_about_success(win, command):
    win.restore()
    win.stdscr.clear()

    text = "The command ({}) succeeded.".format(command)
    win.stdscr.addstr(3, (win.width() - len(text)) // 2, text, color_pair(consts.color_success))

    text = " Press any key to return "
    win.stdscr.addstr(5, (win.width() - len(text)) // 2, text, color_pair(consts.color_bcg) | A_BOLD | A_REVERSE)

    win.stdscr.getch()



