#!/usr/bin/python3

"""
    FGC3.2 OS - Base class for controllers for the main script.
"""

from curses import *
import common_consts as consts

class Controller:

    def __init__(self, name, opts):
        self.name = name
        self.opts = opts


    def action(self, win, index):
        self.opts[index].action(win)


    @staticmethod
    def info(win, text, line = 3, flags =A_BOLD | A_REVERSE):
        win.stdscr.addstr(line, (win.width() - len(text)) // 2, " {} ".format(text), color_pair(consts.color_bcg) | flags)


    @staticmethod
    def info_and_wait(win, text, line = 3):
        Controller.info(win, text, line)
        win.stdscr.getch()
