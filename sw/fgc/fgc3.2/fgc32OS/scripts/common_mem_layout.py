#!/usr/bin/python3

"""
    FGC3.2 OS - Definition of memory layout.
"""

# Size of the flash memory in the FGC3.2.
# Units: MB (1 MB = 1024 * 1024 bytes)
flash_size = 250


# Partitions' sizes definitions.
# Units: MB (1 MB = 1024 * 1024 bytes)
part_raw_size = 16              # Raw area at the beginning of the flash memory (don't change it - it's fixed).
part_sys_size = 150             # System partition (ITB file = DTB + Kernel + RFS).
part_fgc_size = 30              # FGC(D) partition.
                                # Data partition - will take all remaining space.


# Partitions' file system definitions.
# Units: hex type code (see table at the end)
part_sys_type  = "b"
part_fgc_type  = "83"
part_data_type = "83"


# Linux commands used to format each partition. {} represents path to the device (will be substituted later).
# Also the commands should: set partition label, enable verbose mode, don't ask user.
part_sys_cmd  = "sudo mkfs.vfat -n SYSTEM {}"
part_fgc_cmd  = "yes | sudo mkfs.ext4 -v -L FGC {}"
part_data_cmd = "yes | sudo mkfs.ext4 -v -L DATA {}"


# Firmware offset. It's the offset from the beginning of the flash memory.
# The beginning (4096 bytes) is reserved for the partition table.
# Units: number of sectors (1 sector = 512 bytes)
offset_firmware = 8


# Offsets used in the firmware image. The offsets are relative to the beginning of the firmware image.
# However, the firmware image is placed at the offset of 8 sectors. See Wiki for details.
# Units: number of sectors (1 sector = 512 bytes)
offset_uboot     = 8     - offset_firmware
offset_ppa       = 8192  - offset_firmware
offset_fman      = 18432 - offset_firmware
offset_qe        = 18944 - offset_firmware
offset_phy       = 19456 - offset_firmware
offset_dpaa2_fw  = 20480 - offset_firmware
offset_dpaa2_dpl = 26624 - offset_firmware
offset_dpaa2_dpc = 28672 - offset_firmware


# Helper functions
def mb_to_sectors(mb):
    return mb * 1024 * 1024 // 512





# Hex codes defining a partition type:
#
#   0  Vide                   1e  Hidden W95 FAT1           80  Old Minix                 bf  Solaris
#   1  FAT12                  24  NEC DOS                   81  Minix / old Lin           c1  DRDOS/sec
#   2  XENIX root             39  Plan 9                    82  Linux swap / So           c4  DRDOS/sec
#   3  XENIX usr              3c  PartitionMagic            83  Linux                     c6  DRDOS/sec
#   4  FAT16 <32M             40  Venix 80286               84  OS/2 hidden C:            c7  Syrinx
#   5  Extended               41  PPC PReP Boot             85  Linux extended            da  Non-FS data
#   6  FAT16                  42  SFS                       86  NTFS volume set           db  CP/M / CTOS
#   7  HPFS/NTFS              4d  QNX4.x                    87  NTFS volume set           de  Dell Utility
#   8  AIX                    4e  QNX4.x 2nd part           88  Linux plein tex           df  BootIt
#   9  AIX bootable           4f  QNX4.x 3rd part           8e  Linux LVM                 e1  DOS access
#   a  OS/2 Boot Manag        50  OnTrack DM                93  Amoeba                    e3  DOS R/O
#   b  W95 FAT32              51  OnTrack DM6 Aux           94  Amoeba BBT                e4  SpeedStor
#   c  W95 FAT32 (LBA)        52  CP/M                      9f  BSD/OS                    eb  BeOS fs
#   e  W95 FAT16 (LBA)        53  OnTrack DM6 Aux           a0  IBM Thinkpad hi           ee  GPT
#   f  W95 Etendu (LBA        54  OnTrackDM6                a5  FreeBSD                   ef  EFI
#  10  OPUS                   55  EZ-Drive                  a6  OpenBSD                   f0  Linux/PA-RISC
#  11  Hidden FAT12           56  Golden Bow                a7  NeXTSTEP                  f1  SpeedStor
#  12  Compaq diagnost        5c  Priam Edisk               a8  UFS Darwin                f4  SpeedStor
#  14  Hidden FAT16 <3        61  SpeedStor                 a9  NetBSD                    f2  DOS secondary
#  16  Hidden FAT16           63  GNU HURD or Sys           ab  Amorce Darwin             fb  VMware VMFS
#  17  Hidden HPFS/NTF        64  Novell Netware            b7  BSDI fs                   fc  VMware VMKCORE
#  18  AST SmartSleep         65  Novell Netware            b8  BSDI swap                 fd  Linux raid auto
#  1b  Hidden W95 FAT3        70  DiskSecure Mult           bb  Boot Wizard hid           fe  LANstep
#  1c  Hidden W95 FAT3        75  PC/IX                     be  Amorce Solaris            ff  BBT

