#!/usr/bin/python3

"""
    FGC3.2 OS - Base class for controllers for the main script.
"""

from ctrl import Controller
from datetime import datetime
from ctrl_fetch import *
import utils_exec as exe

class BuildrootCtrl(Controller):

    def __init__(self):
        super().__init__("Build Buildroot",
        [
            { "name"   : "Build everything",
              "help"   : "Executes make in the Buildroot directory.",
              "action" : self.b_all
            },
            { "name"   : "Rebuild U-boot",
              "help"   : "Rebuilds only U-boot.",
              "action" : self.b_uboot
            },
            { "name"   : "Rebuild Kernel",
              "help"   : "Rebuilds only Kernel.",
              "action" : self.b_kernel
            }
        ])


    @staticmethod
    def checkFgc(win, check_for_launcher=True):
        if check_for_launcher:
            if not (os.path.isdir(paths.fgc_launcher)):
                BuildrootCtrl.info_and_wait(win, "The path to the Launcher doesn't point to a directory. Fix repository or the path.")
                return False

        if not (os.path.isfile(paths.fgc_sw)):
            BuildrootCtrl.info_and_wait(win, "Path to the FGC software is invalid. Maybe build the FGC first?")
            return False

        if not (os.path.isdir(paths.fgc_mod)):
            BuildrootCtrl.info_and_wait(win, "Path to the kernel modules is invalid. Maybe build modules first?")
            return False

        return True



    @staticmethod
    def copyConfigs(win, tag_str, copy_fgc=True):
        # If the tagStr is empty, provide one.
        # The proper Tag is only set by "Build everything and assemble all images" option.
        if tag_str == "":
            tag_str = "Development build ({})".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S"))

        # First we define RCW header inside of the string
        rcw = "#PBL preamble and RCW header\n"
        rcw += "aa55aa55 01ee0100\n"

        # Then read the RCW file
        try:
            with open(paths.rcw_file, 'r') as file:
                rcw_data = file.read()

        except Exception:
            win.exit(1, "Exception occurred when trying to read RCW file at {}. Check repository integrity, correct the error and try again.".format(paths.rcw_file))

        # And append it to the rcw string
        rcw += rcw_data

        # Now write this to the file, overwriting RCW file in U-Boot
        try:
            file = open(paths.buildroot + paths.rcw_target, "w")
            file.write(rcw)

        except Exception:
            win.exit(1, "Exception occurred when trying to write out RCW file at {}. Check repository integrity, correct the error and try again.".format(paths.buildroot + paths.rcw_target))

        # Now we apply overlay, simply by copying it from the overlay directory to buildroot directory
        exe.run_command(win, "cp -r {}/* {}".format(paths.overlay, paths.buildroot))

        # Set correct execution permissions for the post-build script
        exe.run_command(win, "chmod +x {}".format(paths.buildroot + paths.post_build_script))

        # If copying the FGC software is enabled, then copy the launcher, the FGC and kernel modules
        if (copy_fgc):
            # First crate target directories
            exe.run_command(win, "mkdir -p {}".format(paths.buildroot+"/output/fgc_launcher"))
            exe.run_command(win, "mkdir -p {}".format(paths.buildroot+"/output/fgc_sw"))
            exe.run_command(win, "mkdir -p {}".format(paths.buildroot+"/output/fgc_modules"))

            # Then copy content of source directories into targets
            # Those target dirs will later be used by the post-build script that will put them in
            # a correct place inside the RFS structure.
            exe.run_command(win, "cp -r {}/* {}".format(paths.fgc_launcher, paths.buildroot+"/output/fgc_launcher"))
            exe.run_command(win, "cp    {}   {}".format(paths.fgc_sw, paths.buildroot+"/output/fgc_sw"))
            exe.run_command(win, "cp -r {}/* {}".format(paths.fgc_mod, paths.buildroot+"/output/fgc_modules"))

        # At the end, edit the copied files to include the tag information inside both firmware and Kernel
        # First for firmware, generate simple file containing a single C macro
        file = "#define FGC_RELEASE_TAG \"{}\"".format(tag_str)
        exe.run_command(win, "echo '{}' > {}".format(file, paths.buildroot + paths.firmware_tag_file))

        # For kernel, add line to fgc_post_build.sh script that in turn will create tag.txt inside the root
        # directory on the target Root File System
        line = "echo \"{}\" > \"{}\"".format(tag_str, "${TARGET}/root/tag.txt")
        exe.run_command(win, "echo '{}' >> {}".format(line, paths.buildroot + paths.post_build_script))




    @staticmethod
    def compressKernel(win):
        exe.run_command_gui(win, "gzip < {} > {}".format(paths.kernel_img, paths.compressed_kernel))


    @staticmethod
    def do_build(win, tag_str, skip_asking=False):
        if FetchCtrl.check_fetch(win, skip_asking) and BuildrootCtrl.checkFgc(win):
            if not (os.path.isfile(paths.buildroot + paths.buildroot_config_target) and
                    os.path.isfile(paths.buildroot + paths.uboot_config_target)     and
                    os.path.isfile(paths.buildroot + paths.kernel_config_target)):

                # Copy buildroot config (this one is required to build anything)
                exe.run_command(win, "cp {} {}".format(paths.overlay + paths.buildroot_config_target, paths.buildroot + paths.buildroot_config_target))

                # And then build buildroot (first time, to extract uboot/linux)
                exe.run_command_gui(win, "make -C {}".format(paths.buildroot))

            # Now copy our configuration
            BuildrootCtrl.copyConfigs(win, tag_str)

            # Build it (again)
            exe.run_command_gui(win, "make -C {}".format(paths.buildroot))

            # Prepare compressed kernel
            BuildrootCtrl.compressKernel(win)
            return True
        else:
            return False


    def b_all(self, win):
        if FetchCtrl.check_fetch(win) and BuildrootCtrl.checkFgc(win):
            if self.do_build(win, ""):
                exe.inform_about_success(win, "building buildroot")


    def b_uboot(self, win):
        if FetchCtrl.check_fetch(win):
            if not os.path.isfile(paths.uboot_config_target):
                exe.run_command_gui(win, "make -C {} uboot-reconfigure".format(paths.buildroot))

            BuildrootCtrl.copyConfigs(win, "", False)
            exe.run_command_gui(win, "make -C {} uboot-reconfigure".format(paths.buildroot))
            exe.inform_about_success(win, "rebuilding u-boot")


    def b_kernel(self, win):
        if FetchCtrl.check_fetch(win):
            if not os.path.isfile(paths.kernel_config_target):
                exe.run_command_gui(win, "make -C {} linux-reconfigure".format(paths.buildroot))

            BuildrootCtrl.copyConfigs(win, "", False)
            exe.run_command_gui(win, "make -C {} linux-reconfigure".format(paths.buildroot))
            BuildrootCtrl.compressKernel(win)
            exe.inform_about_success(win, "rebuilding kernel")



