#!/usr/bin/python3

"""
    FGC3.2 OS - Base class for controllers for the main script.
"""


import os
import time
from ctrl import Controller
from ctrl_fetch import *
from ctrl_buildroot import *
import utils_exec as exe
import common_paths as paths
import common_mem_layout as layout



class ImageCtrl(Controller):

    def __init__(self):
        super().__init__("Assemble images",
        [
            { "name"   : "Assemble firmware image",
              "help"   : "Assembles firmware image (RCW + PBI + binaries). This DOES NOT build required files, just pack them into image file. First you need to build buildroot.",
              "action" : self.i_firmware, "cmd": ""
            },
            { "name"   : "Assemble kernel + root file system image (ITB)",
              "help"   : "Assembles ITB image (Device Tree Blob + Kernel + Root File System) and copies Kernel headers. This DOES NOT build required files, just pack them into image file. First you need to build buildroot.",
              "action" : self.i_itb, "cmd": ""
            },
            { "name"   : "Assemble composite image",
              "help"   : "Assembles full-memory image with partitions, file system and other images at right places. This DOES NOT build required files, just pack them into image file. First you need to build buildroot and assemble firmware and ITB images.",
              "action" : self.i_image, "cmd": ""
            },
            { "name"   : "Build everything and assemble all images (USE THIS FOR RELEASE)",
              "help"   : "Runs full rebuild of Buildroot (with Kernel and Uboot), assembles required images (firmware, ITB) and creates the composite image. Adds proper release tags and build date - this option should be used to prepare release images.",
              "action" : self.i_full_image_no_fetch, "cmd": "",
            }
        ])

    @staticmethod
    def generate_tag():
        res = dict()
        res['tag'] = int(time.time())
        res['tagStr'] = "{} ({})".format(res['tag'], time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(res['tag'])))
        return res

    @staticmethod
    def mk_bindir(win):
        exe.run_command(win, "mkdir -p {}".format(paths.bin_dir))

    @staticmethod
    def as_firmware(win, tag="0"):
        # Check that all required files exist
        if  (   exe.file_exists(win, paths.uboot_pbl, "Try to build buildroot (or just u-boot) again.")
            and exe.file_exists(win, paths.ppa_bin)
            and exe.file_exists(win, paths.ppa_bin)):

            # Define name of the binary
            firmware_img = paths.firmware_out_img.format(tag)

            # Create output directory (may already exists)
            ImageCtrl.mk_bindir(win)

            # First prepare a zero-filled file with proper size (i.e. size of raw area - firmware offset, this offset is 8 sectors for partition table)
            exe.run_command(win, "dd of={} if=/dev/zero bs=512 count={}".format(firmware_img, layout.mb_to_sectors(layout.part_raw_size) - layout.offset_firmware ))

            # Then place the u-boot at the beginning (no offset, as the flash offset is added only for the composite image
            exe.run_command(win, "dd of={} if={} bs=512 seek={} conv=notrunc".format(firmware_img, paths.uboot_pbl, layout.offset_uboot))

            # Add binaries
            exe.run_command(win, "dd of={} if={} bs=512 seek={} conv=notrunc".format(firmware_img, paths.ppa_bin, layout.offset_ppa))
            exe.run_command(win, "dd of={} if={} bs=512 seek={} conv=notrunc".format(firmware_img, paths.fman_bin, layout.offset_fman))

            return True
        else:
            return False


    @staticmethod
    def as_itb(win, tag="0"):
        # Check that all required files exist
        if  (   exe.file_exists(win, paths.compressed_kernel, "Try to build buildroot (or just kernel) again.")
            and exe.file_exists(win, paths.rfs, "Try to build buildroot again.")
            and exe.file_exists(win, paths.dtb, "Try to build buildroot (or just kernel) again.")
            and exe.file_exists(win, paths.mkimage, "Try to build buildroot (or just u-boot) again.")
            and exe.file_exists(win, paths.its_file)):

            # Create output directory (may already exists)
            ImageCtrl.mk_bindir(win)

            # Read ITS file to a string
            try:
                with open(paths.its_file, 'r') as file:
                    its = file.read()
            except Exception:
                win.exit(1, "An exception was raised while trying to read ITS file ({}).".format(paths.its_file))

            # Replace path placeholders in the ITS file with the correct paths
            its = its.replace("PYTHON_KERNEL_PATH", os.path.abspath(paths.compressed_kernel))
            its = its.replace("PYTHON_RFS_PATH", os.path.abspath(paths.rfs))
            its = its.replace("PYTHON_DTB_PATH", os.path.abspath(paths.dtb))

            # Temporary files
            temp_mkimage = paths.bin_dir + "/mkimage"
            temp_its = paths.bin_dir + "/temp.its"

            # Temporarily copy mkimage file to the output dir
            exe.run_command(win, "cp {} {}".format(paths.mkimage, temp_mkimage))

            # Write out modified ITS string into a file
            try:
                with open(temp_its, "w") as its_file:
                    its_file.write(its)
            except Exception:
                win.exit(1, "An exception was raised while trying to write temporary ITS file ({}).".format(temp_its))

            # Now that we have everything, we can generate ITB file from the ITS using mkimage
            exe.run_command(win, "{} -f {} {}".format(temp_mkimage, temp_its, os.path.abspath(paths.kernel_out_img.format(tag))))

            # At the end remove temporary files
            exe.run_command(win, "rm {} {}".format(temp_mkimage, temp_its))

            return True
        else:
            return False


    @staticmethod
    def make_part(win, device):
        # Get sizes in number of sectors
        raw_size = layout.mb_to_sectors(layout.part_raw_size)
        sys_size = layout.mb_to_sectors(layout.part_sys_size)
        fgc_size  = layout.mb_to_sectors(layout.part_fgc_size)

        # Prepare sfdisk script file to create partitions.
        # See "man sfdisk" (or http://manpages.ubuntu.com/manpages/cosmic/man8/sfdisk.8.html).
        script  = "label: dos \nunit: sectors \n\n"
        script += "start= {}, size= {}, type={}\n".format(raw_size, sys_size, layout.part_sys_type)
        script += "start= {}, size= {}, type={}\n".format(raw_size + sys_size, fgc_size, layout.part_fgc_type)
        script += "start= {}, type={}\n".format(raw_size + sys_size + fgc_size, layout.part_data_type)

        # Write out the script into a file
        temp_script = paths.bin_dir + "/temp_sfdisk_script"

        try:
            with open(temp_script, "w") as script_file:
                script_file.write(script)
        except Exception:
            win.exit(1, "An exception was raised while trying to write temporary sfdisk script ({}).".format(temp_script))

        # Run sfdisk with the above script as input to make partitions
        exe.run_command_gui(win, "sudo sfdisk {} < {}".format(device, temp_script))

        # Remove the temporary file
        exe.run_command(win, "rm {}".format(temp_script))


    @staticmethod
    def format_part(win, device, p1, p2, p3):
        # Format system partition
        exe.run_command_gui(win, layout.part_sys_cmd.format(device + p1))

        # Format FGC partition
        exe.run_command_gui(win, layout.part_fgc_cmd.format(device + p2))

        # Format data partition
        exe.run_command_gui(win, layout.part_data_cmd.format(device + p3))




    @staticmethod
    def as_composite(win, tag="0"):
        # Check that all required files exist
        if  (   exe.file_exists(win, paths.firmware_out_img.format(tag), "Assemble firmware image first.")
            and exe.file_exists(win, paths.kernel_out_img.format(tag), "Assemble ITB image first.")
            and BuildrootCtrl.checkFgc(win, False)):

            # Create output directory (may already exists)
            ImageCtrl.mk_bindir(win)

            # Define name of the composite image
            composite_img = paths.composite_out_img.format(tag)

            # Create a zeroed file on disk with the size of the flash memory
            exe.run_command_gui(win, "dd of={} if=/dev/zero bs=512 count={} status=progress".format(composite_img, layout.mb_to_sectors(layout.flash_size ) ))

            # Create partitions inside of this file
            ImageCtrl.make_part(win, composite_img)

            # Copy firmware image at the right place
            exe.run_command_gui(win, "dd of={} if={} bs=512 seek={} conv=notrunc status=progress".format(composite_img, paths.firmware_out_img.format(tag), layout.offset_firmware))

            # Setup this file as a loop device. The losetup command will return assigned loop dev path.
            loop = exe.run_command(win, "sudo losetup --partscan --show --find {}".format(composite_img))
            loop = loop.replace("\n", "").strip()

            try:
                # Format partitions
                ImageCtrl.format_part(win, loop, "p1", "p2", "p3")

                # Create temporary directory (to be used as a mounting point)
                temp_dir = paths.bin_dir + "/temp_dir"
                exe.run_command(win, "mkdir -p {}".format(temp_dir))

                try:
                    # Mount first, system partition to the temporary directory
                    exe.run_command(win, "sudo mount {} {}".format(loop + "p1", temp_dir))

                    try:
                        # Copy the ITB file there
                        exe.run_command(win, "sudo cp {} {}".format(paths.kernel_out_img.format(tag), temp_dir))

                    finally:
                        # Unmount the first partition
                        exe.run_command(win, "sudo umount {}".format(loop + "p1"))

                    # Mount seconds, FGC partition to the temporary directory
                    exe.run_command(win, "sudo mount {} {}".format(loop + "p2", temp_dir))

                    try:
                        # Copy FGC program and kernel modules there
                        exe.run_command(win, "sudo cp -r {} {}".format(paths.fgc_sw, temp_dir))
                        exe.run_command(win, "sudo cp -r {} {}".format(paths.fgc_mod, temp_dir))
                        pass
                    finally:
                        # Unmount the second partition
                        exe.run_command(win, "sudo umount {}".format(loop + "p2"))

                finally:
                    # Remove the temporary directory
                    exe.run_command(win, "rm -r {}".format(temp_dir))

            finally:
                # Detach the loop device
                exe.run_command(win, "sudo losetup -d {}".format(loop))

            # As the last step, compress the resulting images (which will also delete uncompressed image)
            exe.run_command_gui(win, "gzip {}".format(composite_img))

            return True
        else:
            return False



    def i_firmware(self, win):
        if self.as_firmware(win):
            exe.inform_about_success(win, "assembling a firmware image")


    def i_itb(self, win):
        if self.as_itb(win):
            exe.inform_about_success(win, "assembling an ITB image")


    def i_image(self, win):
        if self.as_composite(win):
            exe.inform_about_success(win, "assembling a composite image")



    @staticmethod
    def do_full_image_base(win, tasks):
        # Execute tasks
        for task in tasks:
            if not task(win):
                return False

        return True

    @staticmethod
    def do_full_image_no_fetch(win, skip_asking=False):
        tag = ImageCtrl.generate_tag()

        return ImageCtrl.do_full_image_base(
            win,
            (
                lambda x: BuildrootCtrl.do_build(x, tag['tagStr'], skip_asking),
                lambda x: ImageCtrl.as_firmware(x, tag['tag']),
                lambda x: ImageCtrl.as_itb(x, tag['tag']),
                lambda x: ImageCtrl.as_composite(x, tag['tag'])
            )
        )

    def i_full_image_no_fetch(self, win):
        if ImageCtrl.do_full_image_no_fetch(win):
            exe.inform_about_success(win, "Building and assembling a composite image")
