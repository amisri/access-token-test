#!/usr/bin/python3

"""
    FGC3.2 OS - A class facilitating use of curses in other scripts.
"""

import curses
import common_consts as consts


class Window:
    def __init__(self, min_x, min_y):
        # Mark that the exit function was not used
        self._already_exit = False

        # Init curses
        self.stdscr = curses.initscr()
        curses.start_color()

        # Save minimal size
        self.min_x = min_x
        self.min_y = min_y

        # Create pad
        self.pad0 = curses.newpad(100, 1000)

        # Turn off the cursors
        curses.curs_set(0)

        # Initialize colors
        consts.initCursesColor()

        # Set background color of the window and the pad
        self.stdscr.bkgd(curses.color_pair(consts.color_bcg))
        self.pad0.bkgd(curses.color_pair(consts.color_bcg))


    # Check the minimal required size of the terminal
        if curses.COLS < min_x or curses.LINES < min_y:
            self.exit(1, "Error: Terminal size must be at least {} columns by {} lines.".format(curses.COLS, curses.LINES))


    def width(self):
        height, width = self.stdscr.getmaxyx()
        return width


    def height(self):
        height, width = self.stdscr.getmaxyx()
        return height


    def restore(self):
        self._already_exit = False
        self.stdscr.refresh()

        # Don't require enter, don't display keys and convert keys symbols
        curses.cbreak()
        curses.noecho()
        self.stdscr.keypad(True)


    def close(self):
        if not self._already_exit:
            # Restore normal parameters of the terminal
            curses.nocbreak()
            self.stdscr.keypad(False)
            curses.echo()
            curses.endwin()

            self._already_exit = True


    def checkMinSize(self):
        # Check that size of the terminal is as required
        if self.width() < self.min_x or self.height() < self.min_y:
            self.stdscr.clear()
            self.stdscr.refresh()

            self.pad0.clear()

            self.pad0.addstr(1, 1, "Size of the terminal is too small (Should be at least {}x{}). Increase it to resume.".format(self.min_x, self.min_y), curses.color_pair(consts.color_error))
            self.pad0.refresh(0, 0, 0, 0, self.height() - 1, self.width() - 1)

            self.stdscr.getch()
            return False
        return True


    def drawPad(self, offset_y, y, act_y):
        # Calculate stuff
        margin_x = 5
        margin_y = 6
        max_x = self.width()-margin_x-1
        max_y = self.height()-offset_y-1
        y -= 2

        scroll = act_y - max_y + margin_y
        if scroll < 0:
            scroll = 0

        # Draw scrollbar
        if act_y > 0 and y > 0:
            bar_x = max_x - margin_x - 1
            bar_y = max_y - margin_y + 2

            for bar in range(0, bar_y):
                self.pad0.addstr(scroll + bar, bar_x, " ", curses.color_pair(consts.color_scroll))

            self.pad0.addstr(scroll + (int)(((act_y-1) / y) * bar_y), bar_x, " ", curses.color_pair(consts.color_scrollHandle))

        # Display pad on the window
        self.pad0.refresh(scroll, 0, margin_x,margin_y, max_y, max_x)


    def drawHelp(self, offset_y, help_text, warning):
        title = "Additional info: "
        start_x = 5
        start_y = self.height() - offset_y
        height = 5

        help_count = 0

        self.stdscr.addstr(start_y, start_x, "{}".format(title), curses.color_pair(consts.color_bcgLigth) | curses.A_BOLD)
        init_x = start_x + len(title)

        for y in range(0, height):
            for x in range(init_x, self.width()-start_x):
                if help_count < len(help_text):
                    self.stdscr.addstr(start_y + y, x, "{}".format(help_text[help_count]), curses.color_pair(consts.color_bcgLigth))
                    help_count += 1
                else:
                    self.stdscr.addstr(start_y + y, x, " ", curses.color_pair(consts.color_bcgLigth))

            init_x = start_x

        # Draw warning if present
        if warning != "":
            self.stdscr.addstr(start_y + height - 1, start_x + (self.width()-start_x) // 2 - (len(warning) + 2)//2, " {} ".format(warning), curses.color_pair(consts.color_error))




    def exit(self, code, message):
        self.close()

        # Print the message and exit with a given code
        print(message)
        exit(code)


    def run(self, win_main):
        try:
            self.restore()

            # Run entry function
            win_main()

        except KeyboardInterrupt:
            # So when the user does ctrl+c no ugly Traceback is printed
            pass

        finally:
            self.close()