#!/usr/bin/python3

"""
    FGC3.2 OS - Main building script.
"""

import curses
import os
import re

from utils_window import Window
from curses import *
import argparse
import common_paths as paths
import common_consts as consts

# Import controllers
from ctrl_help import *
from ctrl_self_config import *
from ctrl_fetch import *
from ctrl_config import *
from ctrl_buildroot import *
from ctrl_images import *
from ctrl_dev import *


# Define minimum size of the terminal
min_width = 100
min_height = 40

# Create a new window with minimal required size of terminal
win = Window(min_width, min_height)

# Change working directory to the script directory
os.chdir(os.path.dirname(os.path.abspath(__file__)))


def drawHeader():
    win.stdscr.addstr(1, 2, "FGC 3.2 OS - Builder script ", color_pair(consts.color_bcgTitle) | curses.A_BOLD)
    win.stdscr.addstr(2, 2, "------------------------------------------", color_pair(consts.color_bcgTitle) | A_BOLD )


def winMain():
    # Add controllers to the list
    ctrls = [
        HelpCtrl(),
        SelfConfigCtrl(),
        FetchCtrl(),
        ConfigCtrl(),
        BuildrootCtrl(),
        ImageCtrl(),
        DevCtrl()
    ]

    act_ctrl = 0
    act_opt  = 0
    sel_ctrl = -1
    sel_opt  = -1


    # Check whether buildroot directory exists
    FetchCtrl.check_fetch(win)


    while True:
        if not win.checkMinSize():
            continue

        # Clear screen
        win.stdscr.clear()

        # Draw header
        drawHeader()
        win.drawHelp(7, ctrls[act_ctrl].opts[act_opt]["help"], "")
        win.stdscr.refresh()

        # Iterate over controllers to draw them
        y = 0
        act_y = 0
        win.pad0.clear()

        for ctrl_i, ctrl in enumerate(ctrls):
            # Draw title
            win.pad0.addstr(y, 0, ctrl.name, color_pair(consts.color_bcg) | A_BOLD)
            y += 1

            # Iterate over options in each controller to show them
            for opt_i, opt in enumerate(ctrl.opts):
                flag = 0
                flagNum = A_DIM

                if ctrl_i == act_ctrl and opt_i == act_opt:
                    flag = A_REVERSE

                if sel_ctrl > -1:
                    if ctrl_i != sel_ctrl:
                        flag |= A_DIM
                    else:
                        flagNum = 0

                if ctrl_i == act_ctrl and opt_i == act_opt:
                    act_y = y

                win.pad0.addstr(y, 3, "{}{}".format(ctrl_i + 1, opt_i + 1), color_pair(consts.color_bcg) | flagNum)
                win.pad0.addstr(y, 6, opt['name'], color_pair(consts.color_bcg) | flag)
                y += 1

            y += 1

        win.pad0.addstr(y + 2, 5, "Press q to exit the program.", color_pair(consts.color_bcg))

        # Display pad on the window
        win.drawPad(9, y, act_y)


        # Wait for user input
        c = win.stdscr.getch()

        # Handle number keys
        if ord('0') <= c <= ord('9'):
            if sel_ctrl < 0:
                sel_ctrl = c - ord('1')
                act_ctrl = sel_ctrl
                act_opt = 0
            else:
                sel_opt = c - ord('1')
                act_opt = sel_opt
        else:
            sel_ctrl = -1

        # Handle arrows UP-DOWN (navigate options), PAGE-UP-DOWN (navigate controllers)
        if c == curses.KEY_UP:
            act_opt -= 1

        elif c == curses.KEY_DOWN:
            act_opt += 1

        elif c == curses.KEY_PPAGE:
            act_opt -= 999

        elif c == curses.KEY_NPAGE:
            act_opt += 999

        # Handle enter key
        elif c == curses.KEY_ENTER or c == 10 or c == 13:
            sel_ctrl = act_ctrl
            sel_opt = act_opt
 
        # Handle exit
        elif c == ord('q'):
            win.exit(0, "The script has exited.")


        # Validate (and wrap) menu positions
        if act_opt < 0:
            act_ctrl -= 1
            act_opt = 999
        elif act_opt >= len(ctrls[act_ctrl].opts):
            act_ctrl += 1
            act_opt = 0

        if act_ctrl < 0:
            act_ctrl = len(ctrls)-1
        elif act_ctrl >= len(ctrls):
            act_ctrl = 0

        if act_opt > len(ctrls[act_ctrl].opts):
            act_opt = len(ctrls[act_ctrl].opts) - 1


        # If there is a selected option, execute if
        if sel_opt > -1:
            # Clear screen
            win.stdscr.clear()

            # Execute selected
            if sel_ctrl < len(ctrls):
                if sel_opt >=0 and sel_opt < len(ctrls[sel_ctrl].opts):
                    ctrls[sel_ctrl].opts[sel_opt]['action'](win)

            # Clear selected options
            sel_ctrl = -1
            sel_opt = -1

        # Refresh screen
        win.stdscr.refresh()



def cmdFull():
    ImageCtrl.do_full_image_no_fetch(win, True)
    win.exit(0, "The script has exited.")


# Entry point:
win.close()

parser = argparse.ArgumentParser()

parser.add_argument('-f', '--full', action='store_true', help="Fetch, build everything and assemble composite image")

args = parser.parse_args()

if args.full:
    cmdFull()
else:
    win.run(winMain)