#!/usr/bin/python3

"""
    FGC3.2 OS - Base class for controllers for the main script.
"""

from ctrl import Controller
import utils_exec as exe

class SelfConfigCtrl(Controller):

    def __init__(self):
        super().__init__("Configure this tool",
        [
            { "name"   : "Paths to internal configuration files and external resources",
              "help"   : "Opens file to edit paths to required components.",
              "action" : self.edit_paths
            }
        ])


    def edit_paths(self, win):
        exe.open_by_os("common_paths.py")
        super().info_and_wait(win, "The file has been opened for edit. When edited, press any key to reload.")
        exe.restart_app(win)








