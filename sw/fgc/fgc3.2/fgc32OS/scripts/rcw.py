#!/usr/bin/python3

"""
    FGC3.2 OS - Reset Configuration Word (RCW) configurator and compiler.

    This script provides a interactive menu to select RCW options
    and then to generate (compile) RCW in a binary form.
"""

##################################################################
### RCW options definitions. Based on QorIQ LS1046A Reference Manual, chapter 4.4.6.1.
import re

"""
    The following structure defines all RCW fields that can be set, along with the options.
    It is 3-level deep: groups, fields, options. Groups and fields are dictionaries, options are just
    arrays (it is assumed that elements in the array goes: [value, name, value, value, name (...)].
    Each value in the option is a string representing a binary value of bits. 'bit' in the field
    definition says where each field starts inside of the RCW. 
"""
rcw = [
    {
        "name" : "PLL CONFIGURATION",
        "bits"  : "0-127",
        "fields" :
        [
            {"name" : "System PLL Multiplier/Ratio", "help" : "This field selects the platform clock:SYSCLK ratio.",
             "bit" : 2, "isList" : True, "options" :
                 [
                     "00011", "3:1",
                     "00100", "4:1",
                     "00101", "5:1",
                     "00110", "6:1",
                     "00111", "7:1",
                     "01000", "8:1",
                     "01001", "9:1",
                     "01010", "10:1",
                     "01011", "11:1",
                     "01100", "12:1",
                     "01101", "13:1",
                     "01110", "14:1",
                     "01111", "15:1",
                     "10000", "16:1"
                 ]},

            {"name" : "Memory Controller Complex PLL Multiplier/Ratio", "help" : "This field configures the DDR PLL:SYSCLK Ratio. NOTE: All ratios may not be supported due to frequency restrictions. Refer to the chip data sheet for the supported frequencies.",
             "bit" : 10, "isList" : True, "options" :
                 [
                     "001000", "8:1",
                     "001001", "9:1",
                     "001010", "10:1",
                     "001011", "11:1",
                     "001100", "12:1",
                     "001101", "13:1",
                     "001110", "14:1",
                     "001111", "15:1",
                     "010000", "16:1",
                     "010001", "17:1",
                     "010010", "18:1",
                     "010011", "19:1",
                     "010100", "20:1",
                     "010101", "21:1",
                     "010110", "22:1",
                     "010111", "23:1",
                     "011000", "24:1"
                 ]},

            {"name" : "Cluster Group A PLL 1 Multiplier/Ratio", "help" : "See reference manual.",
             "bit" : 26, "isList" : True, "options" :
                 [
                     "000101", "5:1 Async",
                     "000110", "6:1 Async",
                     "000111", "7:1 Async",
                     "001000", "8:1 Async",
                     "001001", "9:1 Async",
                     "001010", "10:1 Async",
                     "001011", "11:1 Async",
                     "001100", "12:1 Async",
                     "001101", "13:1 Async",
                     "001110", "14:1 Async",
                     "001111", "15:1 Async",
                     "010000", "16:1 Async",
                     "010001", "17:1 Async",
                     "010010", "18:1 Async",
                     "010011", "19:1 Async",
                     "010100", "20:1 Async",
                     "010101", "21:1 Async",
                     "010110", "22:1 Async",
                     "010111", "23:1 Async",
                     "011000", "24:1 Async",
                     "011001", "25:1 Async",
                     "011010", "26:1 Async",
                     "011011", "27:1 Async",
                     "011100", "28:1 Async",
                     "011101", "29:1 Async",
                     "011110", "30:1 Async",
                     "011111", "31:1 Async",
                     "100000", "32:1 Async",
                     "100001", "33:1 Async",
                     "100010", "34:1 Async",
                     "100011", "35:1 Async",
                     "100100", "36:1 Async",
                     "100101", "37:1 Async",
                     "100110", "38:1 Async",
                     "100111", "39:1 Async",
                     "101000", "40:1 Async"
                 ]},

            {"name" : "Cluster Group A PLL 2 Multiplier/Ratio", "help" : "NOTE: All ratios may not be supported due to frequency restrictions. Refer to the chip data sheet for the supported frequencies.",
             "bit" : 34, "isList" : True, "options" :
                 [
                     "000101", "5:1 Async",
                     "000110", "6:1 Async",
                     "000111", "7:1 Async",
                     "001000", "8:1 Async",
                     "001001", "9:1 Async",
                     "001010", "10:1 Async",
                     "001011", "11:1 Async",
                     "001100", "12:1 Async",
                     "001101", "13:1 Async",
                     "001110", "14:1 Async",
                     "001111", "15:1 Async",
                     "010000", "16:1 Async",
                     "010001", "17:1 Async",
                     "010010", "18:1 Async",
                     "010011", "19:1 Async",
                     "010100", "20:1 Async",
                     "010101", "21:1 Async",
                     "010110", "22:1 Async",
                     "010111", "23:1 Async",
                     "011000", "24:1 Async",
                     "011001", "25:1 Async",
                     "011010", "26:1 Async",
                     "011011", "27:1 Async",
                     "011100", "28:1 Async",
                     "011101", "29:1 Async",
                     "011110", "30:1 Async",
                     "011111", "31:1 Async",
                     "100000", "32:1 Async",
                     "100001", "33:1 Async",
                     "100010", "34:1 Async",
                     "100011", "35:1 Async",
                     "100100", "36:1 Async",
                     "100101", "37:1 Async",
                     "100110", "38:1 Async",
                     "100111", "39:1 Async",
                     "101000", "40:1 Async"
                 ]},

            {"name" : "Cluster 1 PLL Select", "help" : "NOTE: All the four cores are in cluster 1 and run at the same frequency.",
             "bit" : 96, "isList" : True, "options" :
                 [
                     "0000", "CGA_PLL1 /1",
                     "0001", "CGA_PLL1 /2",
                     "0100", "CGA_PLL2 /1",
                     "0101", "CGA_PLL2 /2"
                 ]}
        ]
    },




    # ---------------------------------------------------------------------------------------------
    {
        "name" : "SerDes PLL AND PROTOCOL CONFIGURATION ",
        "bits"  : "128-183",
        "fields" :
            [
                {"name" : "SerDes Protocol Select - SerDes 1", "help" : "See SerDes lane assignments and multiplexing for a complete list of the options and the definitions of this encoded field.",
                 "bit" : 128, "isList" : False, "options" : 16
                 },

                {"name" : "SerDes Protocol Select - SerDes 2", "help" : "See SerDes lane assignments and multiplexing for a complete list of the options and the definitions of this encoded field.",
                 "bit" : 144, "isList" : False, "options" : 16},

                {"name" : "SerDes PLL reference clock select - SerDes 1", "help" : "See reference manual.",
                 "bit" : 160, "isList" : True, "options" :
                     [
                         "00", "PPL1: 100/125    MHz  |  PLL2: 100/125    MHz",
                         "01", "PPL1: 100/125    MHz  |  PLL2: 125/156.25 MHz",
                         "10", "PPL1: 125/156.25 MHz  |  PLL2: 100/125    MHz",
                         "11", "PPL1: 125/156.25 MHz  |  PLL2: 125/156.25 MHz"
                     ]},

                {"name" : "SerDes PLL reference clock select - SerDes 2", "help" : "See reference manual.",
                 "bit" : 162, "isList" : True, "options" :
                     [
                         "00", "PLL1: 100/125    MHz  |  PLL2: 100/125    MHz",
                         "01", "PLL1: 100/125    MHz  |  PLL2: 125/156.25 MHz",
                         "10", "PLL1: 125/156.25 MHz  |  PLL2: 100/125    MHz",
                         "11", "PLL1: 125/156.25 MHz  |  PLL2: 125/156.25 MHz"
                     ]},

                {"name" : "SerDes PLL Power Down - SerDes 1", "help" : "This field is used to power down the SerDes 1 PLLs. See reference manual.",
                 "bit" : 168, "isList" : True, "options" :
                     [
                         "00", "PLL1: Power on      |  PLL2: Power on",
                         "01", "PLL1: Power on      |  PLL2: Powered down",
                         "10", "PLL1: Powered down  |  PLL2: Power on",
                         "11", "PLL1: Powered down  |  PLL2: Powered down"
                     ]},

                {"name" : "SerDes PLL Power Down - SerDes 2", "help" : "This field is used to power down the SerDes 2 PLLs. See reference manual.",
                 "bit" : 170, "isList" : True, "options" :
                     [
                         "00", "PLL1: Power on      |  PLL2: Power on",
                         "01", "PLL1: Power on      |  PLL2: Powered down",
                         "10", "PLL1: Powered down  |  PLL2: Power on",
                         "11", "PLL1: Powered down  |  PLL2: Powered down"
                     ]},

                {"name" : "SerDes 1 frequency divider - PEX", "help" : "This field controls the frequncy of PCIExpress protocols on SerDes lanes that are operating 8/5/2.5 G. Lanes that supporting other frequencies and protocols are unaffected by this field.",
                 "bit" : 176, "isList" : True, "options" :
                     [
                         "00", "Can train up to a max rate of 8G",
                         "01", "Can train up to a max rate of 5G",
                         "10", "Can train up to a max rate of 2.5G"
                     ]},

                {"name" : "SerDes 2 frequency divider - PEX", "help" : "This field controls the frequncy of PCIExpress protocols on SerDes lanes that are operating 8/5/2.5 G. Lanes that supporting other frequencies and protocols are unaffected by this field.",
                 "bit" : 178, "isList" : True, "options" :
                     [
                         "00", "Can train up to a max rate of 8G",
                         "01", "Can train up to a max rate of 5G",
                         "10", "Can train up to a max rate of 2.5G"
                     ]},
            ]
    },




    # ---------------------------------------------------------------------------------------------
    {
        "name" : "MISC PLL-RELATED",
        "bits"  : "184-191",
        "fields" :
            [
                {"name" : "DDR reference clock selection", "help" : "",
                 "bit" : 186, "isList" : True, "options" :
                     [
                         "00", "The DDRCLK pin provides the reference clock to the DDR PLL",
                         "01", "DIFF_SYSCLK/DIFF_SYSCLK_B provides the reference clock to the DDR PLL"
                     ]},

                {"name" : "SerDes1 PLL2 reference clock selection", "help" : "Selects the reference clock for SerDes PLL2 (used for single reference clock selection for SerDes1).",
                 "bit" : 188, "isList" : True, "options" :
                     [
                         "0", "SD1_REF_CLK2/SD1_REF_CLK2_B. Separate reference clocks to both PLLs of SerDes1",
                         "1", "SD1_REF_CLK1/SD1_REF_CLK1_B. Single reference clock to both PLLs of SerDes1"
                     ]},

                {"name" : "SerDes2 PLL2 reference clock selection", "help" : "Selects the reference clock for SerDes PLL2 (used for single reference clock selection for SerDes2)",
                 "bit" : 189, "isList" : True, "options" :
                     [
                         "0", "SD2_REF_CLK2/SD1_REF_CLK2_B. Separate reference clocks to both PLLs of SerDes2",
                         "1", "SD2_REF_CLK1/SD1_REF_CLK1_B. Single reference clock to both PLLs of SerDes2"
                     ]},

                {"name" : "DDR PLL feedback path selection and multiplication enabler", "help" : "Only one value possible.",
                 "bit" : 190, "isList" : True, "options" :
                     [
                         "10", ""
                     ]},
            ]
    },




    # ---------------------------------------------------------------------------------------------
    {
        "name" : "BOOT CONFIGURATION",
        "bits"  : "192-223",
        "fields" :
            [
                {"name" : "Pre-Boot Initialization Source", "help" : "The Pre-Boot Loader fetches addr/data pairs from the selected interface for the purpose of Pre-Boot Initialization of CCSR and/or local memory space. RCW and pre-boot initialization data must be loaded from the same nonvolatile memory device.",
                 "bit" : 192, "isList" : True, "options" :
                     [
                         "0100", "QSPI",
                         "0110", "SD/MMC",
                         "1110", "IFC"
                     ]},

                {"name" : "Boot Holdoff", "help" : "",
                 "bit" : 201, "isList" : True, "options" :
                     [
                         "0", "All cores except core 0 in hold off",
                         "1", "All cores in hold off"
                     ]},

                {"name" : "Secure Boot Enable", "help" : "NOTE: Note that secure boot is enabled if either this RCW bit is set or the Intent to Secure fuse value is set. See chapter 'Secure Boot and Trust Architecture' for more information.",
                 "bit" : 202, "isList" : True, "options" :
                     [
                         "0", "Secure boot is not enabled",
                         "1", "Secure boot is enabled"
                     ]},

                {"name" : "IFC Mode", "help" : "When PBI_SRC is configured for IFC, this field selects the IFC mode for pre-boot initialization. Note that cfg_rcw_src have precedence over configuring the IFC, see the definition of the PBI_SRC field. Valid IFC_MODE encodings are a subset of the cfg_rcw_src encodings.",
                 "bit" : 203, "isList" : False, "options" : 9
                 }
            ]
    },




    # ---------------------------------------------------------------------------------------------
    {
        "name" : "CLOCKING CONFIGURATION",
        "bits"  : "224-255",
        "fields" :
            [
                {"name" : "Hardware Accelerator Block Cluster Group A Mux 1 Clock Select", "help" : "This controls the async clock frequency provided to FMAN module.",
                 "bit" : 224, "isList" : True, "options" :
                     [
                         "010", "Asynchronous mode: Cluster group A PLL 1 /2",
                         "011", "Asynchronous mode: Cluster group A PLL 1 /3",
                         "100", "Asynchronous mode: Cluster group A PLL 1 /4",
                         "101", "Platform Freq. The FMAN core clock will remain async",
                         "110", "Async mode: Cluster Group A PLL 2 /2 is clock",
                         "111", "Async mode: Cluster Group A PLL 2 /3 is clock"
                     ]},

                {"name" : "DDR Latency", "help" : "If DDR latency is not known at the time the RCW is loaded, it is acceptable to conservatively configure this field as 01. This field is used for optimizing the DDR interface. No functional issues result if this field does not match the latency of the actual DRAM's used.",
                 "bit" : 230, "isList" : True, "options" :
                     [
                         "00", "6-6-6 or 7-7-7 DRAMs",
                         "01", "8-8-8, 9-9-9, 10-10-10, 11-11-11, or higher latency DRAMs",
                         "11", "5-5-5 DRAMs"
                     ]},

                {"name" : "Cluster Group A PLL2 Speed Select", "help" : "",
                 "bit" : 245, "isList" : True, "options" :
                     [
                         "0", "High speed operation (from 1000.2 MHz to 1400 MHz)",
                         "1", "Low speed operation (from 800 MHz to 1000.1 MHz)"
                     ]}
            ]
    },



    # ---------------------------------------------------------------------------------------------
    {
        "name" : "MEMORY AND HIGH SPEED I/O CONFIGURATION",
        "bits"  : "256-287",
        "fields" :
            [
                {"name" : "Host/Agent PEX. Configures Host/Agent mode for all PCI Express Interfaces", "help" : "",
                 "bit" : 264, "isList" : True, "options" :
                     [
                         "000", "All host mode",
                         "001", "All agent mode",
                         "010", "PEX1 agent mode, rest in host mode",
                         "011", "PEX1 and PEX3 agent mode, rest in host mode",
                         "100", "PEX1 and PEX2 agent mode, rest in host mode",
                         "101", "PEX2 and PEX3 agent mode, rest in host mode",
                         "110", "PEX2 in agent mode, rest in host mode",
                         "111", "PEX3 in agent mode, rest in host mode"
                     ]}
            ]
    },




    # ---------------------------------------------------------------------------------------------
    {
        "name" : "GENERAL PURPOSE INFORMATION",
        "bits"  : "288-319",
        "fields" :
            [
                {"name" : "General purpose info 1", "help" : "General purpose information. This field has no effect on functional logic; it may be used by software.",
                 "bit" : 288, "isList" : False, "options" : 8
                 },

                {"name" : "General purpose info 2", "help" : "General purpose information. This field has no effect on functional logic; it may be used by software.",
                 "bit" : 299, "isList" : False, "options" : 21
                 }
            ]
    },




    # ---------------------------------------------------------------------------------------------
    {
        "name" : "GROUP A PIN CONFIGURATION",
        "bits"  : "352-383",
        "fields" :
            [
                {"name" : "Functionality of UART pins (together with UART_BASE field)", "help" : "For more details refer UART, GPIO, FTM, and LPUART signal multiplexing.",
                 "bit" : 354, "isList" : True, "options" :
                     [
                         "000", "See UART_BASE field definition",
                         "001", "{UART1_SOUT/GPIO1_15, LPUART1_SOUT, UART1_SIN/ GPIO1_17, LPUART1_SIN, LPUART2_SOUT, LPUART1_RTS_B, LPUART2_SIN, LPUART1_CTS_B}",
                         "010", "{UART1_SOUT/GPIO1_15, LPUART1_SOUT, UART1_SIN/ GPIO1_17, LPUART1_SIN, LPUART2_SOUT, LPUART4_SOUT, LPUART2_SIN, LPUART4_SIN}",
                         "011", "{UART1_SOUT/GPIO1_15, FTM4_CH0, UART1_SIN/GPIO1_17, FTM4_CH1, FTM4_CH2, FTM4_CH3, FTM4_CH4, FTM4_CH5}"
                     ]},

                {"name" : "Functionality of IRQ[3:5] pins (together with IRQ_BASE field)", "help" : "For more details refer PIC and GPIO1 signal multiplexing.",
                 "bit" : 357, "isList" : True, "options" :
                     [
                         "000", "See IRQ_BASE field definition",
                         "011", "{FTM3_CH7, FTM3_CH0, FTM3_CH1, FTM3_CH2, FTM3_CH3, FTM3_CH4, FTM3_CH5, FTM3_CH6, GPIO1_31}"
                     ]},

                {"name" : "Functionality of SPI pins (together with SPI_BASE field)", "help" : "For more details refer SPI, eSDHC, and GPIO2 signal multiplexing.",
                 "bit" : 360, "isList" : True, "options" :
                     [
                         "000", "See SPI_BASE field definition.",
                         "001", "{SDHC_CLK_SYNC_OUT, SDHC_CLK_SYNC_IN, Reserved, SDHC_VS, SPI_CS_B[1]/GPIO2_1/ SDHC_DAT5/SDHC_CMD_DIR, SPI_CS_B[2]/GPIO2_2/SDHC_DAT6/ SDHC_DAT0_DIR, SPI_CS_B[3]/ GPIO2_3/SDHC_DAT7/ SDHC_DAT123_DIR}",
                         "010", "{SPI_MOSI, SPI_MISO, Reserved, SPI_CS_B0/GPIO2_0/SDHC_DAT4, SPI_CS_B[1]/GPIO2_1/SDHC_DAT5/ SDHC_CMD_DIR, SPI_CS_B[2]/ GPIO2_2/SDHC_DAT6/ SDHC_DAT0_DIR, SPI_CS_B[3]/ GPIO2_3/SDHC_DAT7/ SDHC_DAT123_DIR}"
                     ]},

                {"name" : "Functionality of the SDHC pins (together with SDHC_BASE field)", "help" : "For more details refer UART, GPIO, FTM, and LPUART signal multiplexing.",
                 "bit" : 363, "isList" : True, "options" :
                     [
                         "000", "See SDHC_BASE field definition",
                         "001", "{LPUART3_SOUT, LPUART3_SIN, LPUART2_RTS_B, LPUART2_CTS_B, LPUART3_RTS_B, LPUART3_CTS_B}",
                         "010", "{LPUART3_SOUT, LPUART3_SIN, LPUART5_SOUT, LPUART5_SIN, LPUART6_SOUT, LPUART6_SIN}",
                         "011", "{FTM4_CH6, FTM4_CH7, FTM4_FAULT, FTM4_EXTCLK, FTM4_QD_PHA, FTM4_QD_PHB}"
                     ]},

                {"name" : "Functionality of the UART pins", "help" : "Note that UART_EXT field must be set to all 0's for UART_BASE to take effect.",
                 "bit" : 366, "isList" : True, "options" :
                     [
                         "000", "{GPIO1_15, GPIO1_17, GPIO1_19, GPIO1_21, GPIO1_16, GPIO1_18, GPIO1_20, GPIO1_22}",
                         "011", "{UART1_SOUT, UART1_SIN, GPIO1_19, GPIO1_21, GPIO1_16, GPIO1_18, GPIO1_20, GPIO1_22}",
                         "100", "{UART1_SOUT, UART1_SIN, UART1_RTS_B, UART1_CTS_B, GPIO1_16], GPIO1_18, GPIO1_20, GPIO1_22}",
                         "101", "{UART1_SOUT, UART1_SIN, GPIO1_19, GPIO1_21, UART2_SOUT, UART2_SIN, GPIO1_20, GPIO1_22}",
                         "110", "{UART1_SOUT, UART1_SIN, UART1_RTS_B, UART1_CTS_B, UART2_SOUT, UART2_SIN, UART2_RTS_B, UART2_CTS_B}",
                         "111", "{UART1_SOUT, UART1_SIN, UART3_SOUT, UART3_SIN, UART2_SOUT, UART2_SIN, UART4_SOUT, UART4_SIN}"
                     ]},

                {"name" : "Functionality of the ASLEEP pin.", "help" : "",
                 "bit" : 369, "isList" : True, "options" :
                     [
                         "0", "ASLEEP",
                         "1", "GPIO1_13"
                     ]},

                {"name" : "Functionality of the RTC pin.", "help" : "",
                 "bit" : 370, "isList" : True, "options" :
                     [
                         "0", "RTC",
                         "1", "GPIO1_14"
                     ]},

                {"name" : "Functionality of the SDHC pins.", "help" : "NOTE: If cfg_rcw_src selects SD/MMC as the RCW source, the SDHC pins are driven with SDHC functionality regardless of the setting of this field.",
                 "bit" : 371, "isList" : True, "options" :
                     [
                         "0", "{SDHC_CMD, SDHC_DAT[0:3], SDHC_CLK}",
                         "1", "GPIO2[4:9]"
                     ]},

                {"name" : "Functionality of the IRQ_OUT pin.", "help" : "",
                 "bit" : 372, "isList" : True, "options" :
                     [
                         "1", "Reserved"
                     ]},

                {"name" : "Functionality of the IRQ[3:11] pins.", "help" : "The corresponding GPIOs for these pins are GPIO1[23:31]. Each bit: 0 - IRQ, 1 - GPIO.",
                 "bit" : 373, "isList" : False, "options" : 9
                 },

                {"name" : "Functionality of the SPI_CS_B[0:3] pins.", "help" : "",
                 "bit" : 382, "isList" : True, "options" :
                     [
                         "00", "SPI_CS_B[0:3], SPI_MOSI, SPI_MISO, SPI_CLK.",
                         "01", "SDHC_DAT[4:7] for 8-bit MMC card support",
                         "10", "GPIO2[0:3]",
                         "11", "{Reserved, SDHC_CMD_DIR, SDHC_DAT0_DIR, SDHC_DAT123_DIR}"
                     ]}
            ]
    },




    # ---------------------------------------------------------------------------------------------
    {
        "name" : "GROUP B PIN CONFIGURATION",
        "bits"  : "384-415",
        "fields" :
            [
                {"name" : "Functionality of Group A of the IFC pins (together with IFC_GRP_A_BASE field)", "help" : "NOTE: If IFC_CS_B[5] is used, then IFC_RB_B[3] cannot be used and vice-versa.",
                 "bit" : 384, "isList" : True, "options" :
                     [
                         "000", "See IFC_GRP_A_BASE field definition",
                         "001", "QSPI_A_DATA[3]",
                         "010", "{FTM5_CH0, FTM5_CH1, FTM5_EXTCLK}",
                         "100", "{IFC_CS_B[4], IFC_CS_B[5], IFC_CS_B[6]}"
                     ]},

                {"name" : "Functionality of Group D of the IFC pins (together with IFC_GRP_D_BASE field)", "help" : "",
                 "bit" : 393, "isList" : True, "options" :
                     [
                         "000", "See IFC_GRP_D_BASE field definition",
                         "001", "{QSPI_B_DATA[0], QSPI_B_DATA[1], QSPI_B_DATA[2]}",
                         "010", "{FTM6_CH0, FTM6_CH1, FTM6_EXTCLK}"
                     ]},

                {"name" : "Functionality of Group E1 of the IFC pins (together with IFC_GRP_E1_BASE field)", "help" : "Note: This field has additional effect, as also used as IFC_GRP_E3_EXT. The primary functionality of GRP_ E3 is selected only when this field is 3’b000.",
                 "bit" : 396, "isList" : True, "options" :
                     [
                         "000", "See IFC_GRP_E1_BASE field definition",
                         "001", "{IFC_CS_B[1]/GPIO2_10, IFC_CS_B[2]/GPIO2_11, QSPI_B_DATA[3]}",
                         "010", "{FTM7_CH0, FTM7_CH1, FTM7_EXTCLK}",
                     ]},

                {"name" : "Group F of the IFC pins", "help" : "",
                 "bit" : 399, "isList" : True, "options" :
                     [
                         "000", "{IFC_A[16:24]/IFC_WP_B[1:3]}",
                         "001", "{QSPI_A_CS0, QSPI_A_CS1, QSPI_A_SCK, QSPI_B_CS0, QSPI_B_CS1, QSPI_B_SCK, QSPI_A_DATA[0], QSPI_A_DATA[1], QSPI_A_DATA[2]}"
                     ]},

                {"name" : "Functionality of Group E1 of the IFC pins (together with IFC_GRP_E1_EXT field)", "help" : "",
                 "bit" : 405, "isList" : True, "options" :
                     [
                         "0", "IFC_CS_B[1:3]",
                         "1", "{GPIO2[10:12]}"
                     ]},

                {"name" : "Functionality of Group D of the IFC pins (together with IFC_GRP_D_EXT field)", "help" : "",
                 "bit" : 407, "isList" : True, "options" :
                     [
                         "0", "{IFC_PAR[0:1]/IFC_PERR_B}",
                         "1", "{GPIO2[13:15]}"
                     ]},

                {"name" : "Functionality of Group A of the IFC pins", "help" : "Whenever IFC is selected for less than 28 bits (25-bits or 22-bits) using CFG_RCW_SRC, this field should be set such that IFC_AD is not selected. When IFC is selected as 25-b or 22-b addressing,RCW[IFC_GRP_A_BASE] should not be set to 00.",
                 "bit" : 412, "isList" : True, "options" :
                     [
                         "00", "{IFC_A[25:27]}",
                         "01", "{GPIO2[25:27]}",
                         "10", "{IFC_RB_B[2:3], Reserved}"
                     ]},

                {"name" : "Functionality of Group A of the IFC pins", "help" : "Whenever IFC is selected for 22-b functionality using CFG_RCW_SRC, this field should be chosen such that IFC_AD is not selected. And, When IFC is selected as 22-b addressing, RCW[IFC_A_22_24] should not be set to 0.",
                 "bit" : 415, "isList" : True, "options" :
                     [
                         "0", "{IFC_A[22:24]}",
                         "1", "{IFC_WP_B[1:3]}"
                     ]}
            ]
    },




    # ---------------------------------------------------------------------------------------------
    {
        "name" : "CHIP SPECIFIC CONFIGURATION",
        "bits"  : "416-512",
        "fields" :
            [
                {"name" : "Functionality assigned to the EC1 pins", "help" : "",
                 "bit" : 416, "isList" : True, "options" :
                     [
                         "000", "RGMII1",
                         "001", "GPIO3",
                         "101", "FTM1"
                     ]},

                {"name" : "Functionality assigned to the EC2 pins", "help" : "When configured for IEEE1588, the EC2 pins that are not available for IEEE1588 are configured for GPIO.",
                 "bit" : 419, "isList" : True, "options" :
                     [
                         "000", "RGMII2",
                         "001", "GPIO3",
                         "010", "IEEE 1588",
                         "101", "FTM2"
                     ]},

                {"name" : "Voltage of the LVDD IO domain", "help" : "",
                 "bit" : 422, "isList" : True, "options" :
                     [
                         "00", "1.8 V",
                         "01", "2.5 V",
                     ]},

                {"name" : "I2C ipg clock", "help" : "",
                 "bit" : 424, "isList" : True, "options" :
                     [
                         "0", "Platform clock/4 (should be platform/8 after internal division)",
                         "1", "Platform clock/2 (should be platform/4 after internal division)"
                     ]},

                {"name" : "Functionality of the EM1 MDC_MDIO pins", "help" : "",
                 "bit" : 425, "isList" : True, "options" :
                     [
                         "0", "MDC/MDIO (EM1)",
                         "1", "GPIO_3"
                     ]},

                {"name" : "Functionality of the EM2 MDC_MDIO pins", "help" : "",
                 "bit" : 426, "isList" : True, "options" :
                     [
                         "0", "MDC/MDIO (EM2)",
                         "1", "GPIO_4"
                     ]},

                {"name" : "EMI2 - Ethernet Management Interface MDIO pin multiplexing related operating modes", "help" : "Recommended setting for this bit is 1.",
                 "bit" : 427, "isList" : True, "options" :
                     [
                         "0", "MDIO configured in normal functional mode",
                         "1", "MDIO configured in Open-Drain mode"
                     ]},

                {"name" : "EMI2 - Ethernet Management Interface MDC pin multiplexing related operating modes", "help" : "Recommended setting for this bit is 1.",
                 "bit" : 428, "isList" : True, "options" :
                     [
                         "0", "MDIO configured in normal functional mode",
                         "1", "MDIO configured in Open-Drain mode"
                     ]},

                {"name" : "Functionality of the USB_DRVVBUS pin", "help" : "NOTE: Refer USB DRVVBUS Control Register (SCFG_USBDRVVBUS_SELCR) for USB_DRV_VBUS to USB Controller mapping.",
                 "bit" : 429, "isList" : True, "options" :
                     [
                         "0", "USB_DRVVBUS",
                         "1", "GPIO4[29]"
                     ]},

                {"name" : "Functionality of the USB_PWRFAULT signal", "help" : "NOTE: Refer USB PWRFAULTControl Register (SCFG_USBPWRFAULT_SELCR) for USB_PWRFAULT to USB Controller mapping.",
                 "bit" : 430, "isList" : True, "options" :
                     [
                         "0", "USB_PWRFAULT",
                         "1", "GPIO4[30]"
                     ]},

                {"name" : "Voltage of the TVDD IO domain", "help" : "",
                 "bit" : 433, "isList" : True, "options" :
                     [
                         "00", "1.2 V or 1.8 V",
                         "01", "2.5 V",
                         "11", "Auto-voltage selection enabled. This is used only for hard-coded RCW values."
                     ]},

                {"name" : "Voltage of the DVDD IO domain", "help" : "",
                 "bit" : 435, "isList" : True, "options" :
                     [
                         "00", "1.8 V",
                         "10", "3.3 V",
                         "11", "Auto-voltage selection enabled. This is used only for hard-coded RCW values."
                     ]},

                {"name" : "EMI 1-Ethernet Management Interface MDIO pin multiplexing related operating modes", "help" : "Recommended setting for this bit is 1.",
                 "bit" : 435, "isList" : True, "options" :
                     [
                         "0", "MDIO configured in normal functional mode",
                         "1", "MDIO configured in Open-Drain mode"
                     ]},

                {"name" : "Configures voltage of the EVDD IO domain", "help" : "",
                 "bit" : 439, "isList" : True, "options" :
                     [
                         "00", "1.8 V",
                         "10", "3.3 V",
                         "11", "Auto-voltage selection enabled. This is used only for hard-coded RCW values."
                     ]},

                {"name" : "This field selects the EMI 1-Ethernet Management Interface MDC pin multiplexing related operating modes", "help" : "Recommended setting for this bit is 1.",
                 "bit" : 444, "isList" : True, "options" :
                     [
                         "0", "MDC configured in normal functional mode",
                         "1", "MDC configured in Open-Drain mode"
                     ]},

                {"name" : "Selection between IIC2 base functionality and extension", "help" : "",
                 "bit" : 445, "isList" : True, "options" :
                     [
                         "000", "IIC2_SCL, IIC2_SDA",
                         "001", "SDHC_CD_B, SDHC_WP",
                         "010", "GPIO4[2], GPIO4[3]",
                         "011", "FTM3_QD_PHA, FTM3_QD_PHB"
                     ]},


                {"name" : "SYSCLK Frequency", "help" : "This field is used for proper hardware configuration of the Arm generic timer and by software to determine the frequency of SYSCLK. The value in this field is multiplied by 166.667 KHz. It is used to provide information to determine the frequency of operation of the Arm Generic Timer. The allowable range depends on the range of SYSCLK frequencies supported. The frequency for Arm generic timer is SYSCLK/4.",
                 "bit" : 472, "isList" : False, "options" : 10
                 },

                {"name" : "Hardware Accelerator Block Cluster Group A Mux 2 Clock Select", "help" : "This field allows for a platform accelerator block’s (or multiple blocks’) frequency to be maximized (using async clock) by leveraging Cluster Group B PLL 1 or 2 as the source clock. See reference manual.",
                 "bit" : 509, "isList" : True, "options" :
                     [
                         "001", "Async mode: Cluster Group A PLL_2 /1 is clock",
                         "010", "Async mode: Cluster Group A PLL_2 /2 is clock",
                         "011", "Async mode: Cluster Group A PLL_2 /3 is clock",
                         "110", "Async mode: Cluster Group A PLL_1 /2 is clock"
                     ]},

            ]
    },

]






##################################################################
### Code of the actual application

import curses
import os
import re


import common_paths as paths
import common_consts as consts
from utils_window import Window
from curses import *

# Define minimum size of the terminal
min_width = 100
min_height = 40

# Define buttons
buttons_regular = ["Select", "Main menu", "See RCW", "Save and exit", "Do not save and exit"]
buttons_in_edit = ["Select"]
buttons = buttons_regular

class Glob:
    # Warning about RCW file while reading it (empty by default)
    warning = ""

    # Set to true if the user changes anything in the RCW
    modified = False

    # Write to this variable to print it when exiting without saving
    debug = ""


# Create a new window with minimal required size of terminal
win = Window(min_width, min_height)


# Change working directory to the script directory
os.chdir(os.path.dirname(os.path.abspath(__file__)))


def prepareRcw():
    # Add "selected" and "modified" property to fields
    for g_index, group in enumerate(rcw):
        for f_index, field in enumerate(group["fields"]):
            if field["isList"]:
                rcw[g_index]["fields"][f_index]["selected"] = 0
            else:
                rcw[g_index]["fields"][f_index]["selected"] = list('0'*rcw[g_index]["fields"][f_index]["options"])

            rcw[g_index]["fields"][f_index]["modified"] = False


def rcwToBinary():
    # Create a list of characters that has 512 zeros
    bin_data = list("0"*512)

    # Iterate over each option and change bytes of 'bin' list
    for group in rcw:
        for field in group["fields"]:
            bitPos = field["bit"]
            data = []

            # Get the data
            if field["isList"]:
                data = list(field["options"][field["selected"] * 2])
            else:
                data = list(field["selected"])

            # Copy this data into 'bin' list at given bit position
            for index, bit in enumerate(data):
                bin_data[bitPos + index] = str(bit)

    return bin_data


def rcwBinToHex(bin):
    hex_bin = []

    # Iterate over the binary form
    for pos in range(0, 512, 8):

        # Get the next byte out of the binary form
        bin_byte = bin[pos:pos + 8]

        # Convert this byte into a number
        int_byte = int("".join(bin_byte), 2)

        # Convert this number into hex and add it to list
        str_byte = '{:02x}'.format(int_byte)

        # hex_bin.append(str_byte)
        hex_bin.append(str_byte)

    return hex_bin



def loadFromFile():
    info = " Working on a default (zeroed) RCW."

    # Check if the file exists
    if not os.path.isfile(paths.rcw_file):
        Glob.warning = "RCW file ({}) not found.{}".format(paths.rcw_file, info)
        return

    # Try to read a file
    try:
        with open(paths.rcw_file, 'r') as file:
            data = file.readlines()
    except Exception:
        Glob.warning = "Error while opening the file." + info
        return


    # Check if data has 5 lines
    if len(data) != 5:
        Glob.warning = "RCW format error: file is not 5 lines long." + info
        return

    # First line is a comment. Each of the next 4 lines should contain
    # 4 groups (divided by a space) of 8 characters, each [0-9][a-f]. Validate it:
    pattern = re.compile(r"^[0-9a-f]{8} [0-9a-f]{8} [0-9a-f]{8} [0-9a-f]{8}$")

    for line in range(1, 5):
        if not pattern.match(data[line]):
            Glob.warning = "RCW format error: line {} has incorrect format.{}".format(line + 1, info)
            return

    # Now that we know it's correct, let's parse it
    digits = list()

    # Copy HEX digits into list of digits
    for line in range(1, 5):
        for c in data[line]:
            if c != " " and c != "\n":
                digits.append(c)

    # Take two of those digits and create a binary number out of it, then put it into the 'bin'
    rcw_bin = list("0" * 512)
    rcw_bin_offset = 0

    for digit in range(0, len(digits), 2):
        if digit + 1 >= len(digits):
            break

        # Convert to binary
        hex_data = str(digits[digit]) + str(digits[digit + 1])
        bin_data = bin(int(hex_data, 16))[2:].zfill(8)

        # Copy into the list
        for index in range(0, 8):
            rcw_bin[rcw_bin_offset] = bin_data[index]
            rcw_bin_offset += 1


    # Iterate over rcw_bin and correctly fill "selected" property of the field in rcw structure
    for g_index, group in enumerate(rcw):
        for f_index, field in enumerate(group["fields"]):
            # First we need a slice of this binary data that represents this field
            if field["isList"]:
                # If the field is a list, length of this field is length of each/any option value:
                length = len(field["options"][0])
            else:
                # In case of not-list, options field contains number of bits
                length = field["options"]

            # Knowing the length of the filed, get the slice
            field_value = rcw_bin[field["bit"] : field["bit"]+length]

            # Fill the value of the 'selected' property
            if field["isList"]:
                # For list we first need to find an index
                for index in range(0, len(field["options"]), 2):
                    if field["options"][index] == "".join(field_value):
                        rcw[g_index]["fields"][f_index]["selected"] = index // 2
                        break
            else:
                # For non-list value is directly copied
                rcw[g_index]["fields"][f_index]["selected"] = field_value




def saveToFile():

    try:
        file = open(paths.rcw_file,"w")

        # Write warning / comment.
        file.write("# DO NOT EDIT MANUALLY - generated by the builder script.\n")

        # Convert rcw structure into hex format
        hex_bin = rcwBinToHex(rcwToBinary())

        # Write lines
        line = ""
        line_count = 0

        for byte_ind, byte in enumerate(hex_bin):
            line += byte

            if (byte_ind + 1) % 16 == 0:
                file.write(line)
                line = ""

                line_count += 1
                if line_count < 4:
                    file.write("\n")

            elif (byte_ind + 1) % 4 == 0:
                line += " "

        file.close()

    except Exception:
        return "There was an error when trying to save RCW to a file. Check RCW file path, permissions etc."

    # Return no error - meaning OK
    return ""


def drawWarning():
    margin = 5

    box_width = win.width() - (2 * margin)
    box_height = 12

    # Calculate box corners
    sx = win.width() // 2 - box_width // 2
    sy = win.height() // 2 - box_height // 2
    ex = win.width() // 2 + box_width // 2
    ey = win.height() // 2 + box_height // 2

    # Draw box
    for y in range(sy, ey):
        for x in range(sx, ex):
            ch = " "
            if y == sy or y == ey-1 or x == sx or x == ex-1:
                ch = "*"

            win.stdscr.addstr(y, x, "{}".format(ch), color_pair(consts.color_error))

    # Draw warning
    win.stdscr.addstr(win.height() // 2 - 2, win.width() // 2 - len(Glob.warning) // 2, "{}".format(Glob.warning), color_pair(consts.color_error))

    # Tell user to press any key
    comm = "Press any key to continue"
    win.stdscr.addstr(ey - 3, win.width() // 2 - len(comm) // 2, "{}".format(comm ), color_pair(consts.color_error))


def drawHeader():
    header = "FGC 3.2 OS - RCW configurator and compiler"

    win.stdscr.addstr(1, 2, "{}".format(header), color_pair(consts.color_bcgTitle) | curses.A_BOLD)
    win.stdscr.addstr(2, 2, "------------------------------------------", color_pair(consts.color_bcgTitle) | A_BOLD )

    if Glob.modified:
        win.stdscr.addstr(1, 2 + len(header), " (modified)", color_pair(consts.color_bcg) | A_DIM)



def drawFooter(act, is_in_edit):
    # Draw background
    bcg = " " * (win.width()-1)
    win.stdscr.addstr(win.height()-3, 0, bcg, color_pair(consts.color_bcgInv))
    win.stdscr.addstr(win.height()-2, 0, bcg, color_pair(consts.color_bcgInv))
    win.stdscr.addstr(win.height()-1, 0, bcg, color_pair(consts.color_bcgInv))

    # If we are in the edit mode, display only the first button (select)
    if is_in_edit:
        buttons = buttons_in_edit
    else:
        buttons = buttons_regular

    # Calculate buttons width
    buttons_width = 0
    for index, button in enumerate(buttons):
        buttons_width = buttons_width + len(button) + 9

    # Draw buttons
    x = win.width() // 2 - buttons_width // 2

    for index, button in enumerate(buttons):
        flags = curses.A_BOLD| color_pair(consts.color_bcgInv)

        if index == act:
            flags = flags | A_REVERSE

        win.stdscr.addstr(win.height()-2, x, "< {} >".format(button), flags)

        x = x + len(button) + 9



def drawMainMenu(act_group, act_field):
    # Clear the pad
    win.pad0.clear()

    y = 0
    act_y = 0

    # Draw on the pad
    for gIndex, group in enumerate(rcw):
        for x in range (0, win.width()-30):
            win.pad0.addstr(y, x, "-", color_pair(consts.color_bcg) | A_DIM)

        win.pad0.addstr(y, 0, "{} ".format(group["name"]), curses.A_BOLD)
        win.pad0.addstr(y, win.width()-30, " BITS {} ".format(group["bits"]) )
        y = y + 1

        # Print fields in the group
        for fIndex, field in enumerate(group["fields"]):

            flags = 0
            if gIndex == act_group and fIndex == act_field:
                flags = A_REVERSE
                act_y = y

            modified = ""
            if field["modified"]:
                modified = " (modified)"

            win.pad0.addstr(y, 2, " {} ".format(field["name"]), flags )
            win.pad0.addstr(y, 2 + 2 + len(field["name"]), "{}".format(modified), A_DIM)
            y = y + 1

        # Display empty line between groups
        y = y + 1

    win.drawPad(12, y, act_y)


def drawOptions(act_group, act_field, act_option, is_in_edit):
    # Clear the pad
    win.pad0.clear()

    win.pad0.addstr(0, 0, rcw[act_group]["fields"][act_field]["name"], curses.A_BOLD )

    y = 2
    act_y = 0

    # Draw on the pad
    field = rcw[act_group]["fields"][act_field]
    options = field["options"]

    if field["isList"]:
        for index in range(0, len(options), 2):
            if index + 1 >= len(options):
                break

            flags = color_pair(consts.color_bcg)

            if index//2 == rcw[act_group]["fields"][act_field]["selected"]:
                flags = color_pair(consts.color_valueSel)
                win.pad0.addstr(y, 0, "* ", flags)

            if index//2 == act_option:
                flags = color_pair(consts.color_bcg) | A_REVERSE
                act_y = y

            win.pad0.addstr(y, 2, " {}".format(options[index]), flags | A_BOLD)
            win.pad0.addstr(y, len(options[index]) + 3, " - {} ".format(options[index + 1]), flags)
            y = y + 1

    else:
        # Don't draw scrollbar for this
        act_y = -1

        # Set color
        flags = color_pair(consts.color_bcg)
        win.pad0.addstr(y, 2, "{}-bit numerical value: ".format(options), flags)
        y += 2

        # Draw the value
        for index, c in enumerate(field["selected"]):
            if index == act_option and is_in_edit:
                flags = color_pair(consts.color_valueSel)
            else:
                flags = color_pair(consts.color_bcg)

            win.pad0.addstr(y, 5 + index, "{}".format(c), flags)

        # Convert the value to dec and hex and display it
        flags = color_pair(consts.color_bcg)

        dec = int("".join(field["selected"]), 2)
        win.pad0.addstr(y, 5 + index + 10, "dec = {}  hex = {}".format(dec, hex(dec)), flags)

        y += 2
        if is_in_edit:
            win.pad0.addstr(y, 2, "Type 0 or 1. Use arrows to change cursor position. ", flags)
            y += 1
            win.pad0.addstr(y, 2, "Choose 'select' to finish editing. ", flags)
        else:
            win.pad0.addstr(y, 2, "Choose 'select' to edit the value.", flags)


    # Draw pad on the screen
    win.drawPad(12, y, act_y)



def drawRcw():
    # Clear the pad
    win.pad0.clear()
    win.pad0.addstr(0, 0, "RCW binary representation:", color_pair(consts.color_bcg) | curses.A_BOLD)

    # Get the RCW as binary
    bin = rcwToBinary()

    # Draw this binary to the pad
    start_x = 5
    start_y = 2
    bin_count = 0

    y = start_y
    x = start_x
    for bit_ind, bit in enumerate(bin):
        win.pad0.addstr(y, x, "{}".format(bin[bin_count]))
        bin_count += 1
        x += 1

        if bin_count % 64 == 0:
            x = start_x
            y += 1
        elif bin_count % 8 == 0:
            x+= 1


    # Get the RCW as HEX
    hex_bin = rcwBinToHex(bin)

    # Draw this binary to the pad
    start_y = y + 2

    win.pad0.addstr(start_y, 0, "RCW hex representation:", color_pair(consts.color_bcg) | curses.A_BOLD)
    start_y += 2

    y = start_y
    for byte_ind, byte in enumerate(hex_bin):
        win.pad0.addstr(y, x, "{}".format(byte))
        x += 2

        if (byte_ind + 1) % 16 == 0:
            x = start_x
            y += 1
        elif (byte_ind + 1) % 4 == 0:
            x+= 1

    win.drawPad(12, 0, 0)

    pass



def winMain():
    # Prepare RCW and load it from the file
    prepareRcw()
    loadFromFile()

    act_group = 0
    act_field = 0
    act_button = 0
    act_option = 0

    is_in_edit = False

    view = 0

    # Draw warning if there is one
    if Glob.warning != "":
        drawWarning()
        c = win.stdscr.getch()


    while True:
        if not win.checkMinSize():
            continue

        # Clear screen
        win.stdscr.clear()

        # Draw header and footer
        drawHeader()
        if view != 2:
            win.drawHelp(10, rcw[act_group]["fields"][act_field]["help"], Glob.warning)
        drawFooter(act_button, is_in_edit)

        win.stdscr.refresh()

        # Draw current view main menu
        if view == 0:
            drawMainMenu(act_group, act_field)
        elif view == 1:
            drawOptions(act_group, act_field, act_option, is_in_edit)
        elif view == 2:
            drawRcw()

        # Reset menu selection
        menu_button = -1

        # Wait for user input
        c = win.stdscr.getch()

        # Handle arrows UP-DOWN, PAGE-UP-DOWN
        if c == curses.KEY_UP:
            if view == 0:
                act_field -= 1
            elif view == 1:
                act_option -= 1

        elif c == curses.KEY_DOWN:
            if view == 0:
                act_field += 1
            elif view == 1:
                act_option += 1

        elif c == curses.KEY_PPAGE:
            if view == 0:
                act_field = -1
            elif view == 1:
                act_option -= 10

        elif c == curses.KEY_NPAGE:
            if view == 0:
                act_field = 999
            elif view == 1:
                act_option += 10

        # Handle HOME and END
        elif c == curses.KEY_HOME:
            if view == 0:
                act_group = 0
                act_field = 0
            elif view == 1:
                act_option = 0

        elif c == curses.KEY_END:
            if view == 0:
                act_group = 999
                act_field = 0
            elif view == 1:
                act_option = -1

        # Handle arrows LEFT-RIGHT
        elif c == curses.KEY_LEFT:
            if is_in_edit:
                act_option -= 1
            else:
                act_button -= 1

        elif c == curses.KEY_RIGHT:
            if is_in_edit:
                act_option += 1
            else:
                act_button += 1

        # Handle 0 and 1 keys
        elif c == ord('1') and is_in_edit:
            rcw[act_group]["fields"][act_field]["selected"][act_option] = '1'
            rcw[act_group]["fields"][act_field]["modified"] = True
            Glob.modified = True
            act_option += 1

        elif c == ord('0') and is_in_edit:
            rcw[act_group]["fields"][act_field]["selected"][act_option] = '0'
            rcw[act_group]["fields"][act_field]["modified"] = True
            Glob.modified = True
            act_option += 1

        # Handle enter
        elif c == curses.KEY_ENTER or c == 10 or c == 13:
            menu_button = act_button

        # Handle shortcuts
        elif c == ord('e') or c == ord('m') or c == KEY_BACKSPACE:
            menu_button = 1

        elif c == ord('s'):
            menu_button = 0
            act_button = 0

        elif c == ord('r'):
            menu_button = 2

        # Validate (and wrap) menu positions
        if act_group < 0:
            act_group = 0
        elif act_group >= len(rcw):
            act_group = len(rcw)-1

        if act_field < 0:
            act_group = act_group - 1
            if act_group < 0:
                act_group = len(rcw) - 1

            if c == curses.KEY_PPAGE:
                act_field = 0
            else:
                act_field = len(rcw[act_group]["fields"]) - 1

        elif act_field >= len(rcw[act_group]["fields"]):
            act_group = act_group + 1
            if act_group >= len(rcw):
                act_group = 0
            act_field = 0

        if act_button < 0:
            act_button = len(buttons) - 1
        elif act_button >= len(buttons):
            act_button = 0


        # Validate (and wrap) options position
        isList = rcw[act_group]["fields"][act_field]["isList"]

        if isList:
            max_opt = len(rcw[act_group]["fields"][act_field]["options"])//2
        else:
            max_opt = rcw[act_group]["fields"][act_field]["options"]

        if act_option < 0:
            act_option = max_opt - 1
        elif act_option >= max_opt:
            act_option = 0


        # Handle menu selection
        if menu_button == 0:                           # Select
            if view == 0:
                view = 1
                act_option = 0
            elif view == 1:
                if isList:
                    rcw[act_group]["fields"][act_field]["selected"] = act_option
                    rcw[act_group]["fields"][act_field]["modified"] = True
                    Glob.modified = True
                else:
                    is_in_edit = not is_in_edit

        elif menu_button == 1 and not is_in_edit:      # Main menu
            view = 0
            act_button = 0

        elif menu_button == 2 and not is_in_edit:      # See RCW
            view = 2
            act_button = 1

        elif menu_button == 3 and not is_in_edit:      # Save and exit
            save_error = saveToFile()

            if save_error == "":
                win.exit(0, "RCW saved to the file ({}).".format(paths.rcw_file))
            else:
                win.exit(1, "Error while saving the file: {}.".format(save_error))

        elif menu_button == 4 and not is_in_edit:      # Do not save and exit
            if Glob.debug != "":
                Glob.debug = "  Debug: \n\n" + Glob.debug
            win.exit(0, "RCW was not saved.{}".format(Glob.debug))

win.run(winMain)