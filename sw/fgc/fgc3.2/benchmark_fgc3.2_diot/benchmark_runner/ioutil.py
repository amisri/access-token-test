def read_until_end_or_timeout(inp):
    buf = b""
    while True:
        try:
            byte = inp.read(1)
        except TimeoutError:
            break

        if byte == b"":
            break

        buf += byte

    return buf


def readline_or_none(inp, break_on_prompt=None):
    buf = b""
    while True:
        if buf == break_on_prompt:
            break

        try:
            byte = inp.read(1)
        except TimeoutError:
            if not buf:
                return None

            break

        if byte == b"" or byte == b"\x0A":
            break

        buf += byte

    return buf

def as_lines(inp, break_on_prompt=None):
    while True:
        ln = readline_or_none(inp, break_on_prompt)
        if ln is not None:
            yield ln #.decode(errors="replace")
        else:
            break
