import getpass
import io
import logging
import time
from typing import Tuple

import fabric
import paramiko

from ioutil import as_lines

logger = logging.getLogger(__name__)

"""
>>> from fabric import Connection
>>> result = Connection('web1.example.com').run('uname -s', hide=True)
>>> msg = "Ran {0.command!r} on {0.connection.host}, got stdout:\n{0.stdout}"
>>> print(msg.format(result))
"""

class SshSession:
    def __init__(self, host, username):
        self.host = host
        self.username = username

        self._connection = None
        self._uart = None

    def close_uart(self):
        for ch in self._uart_channels:
            ch.close()

    def open_uart(self) -> Tuple[paramiko.channel.ChannelFile, paramiko.channel.ChannelStdinFile]:
        conn = self._get_connection()
        # out = io.BytesIO()
        # err = io.BytesIO()
        # conn.run("stty -F /dev/ttyUSB3 speed 115200 cs8 -cstopb -parenb")
        # self._uart = conn.run("screen /dev/ttyUSB3 115200",
        # self._uart = conn.run("cat /dev/ttyUSB3",
        #                       out_stream=open("TESTout.txt", "wb"),
        #                       err_stream=open("TESTerr.txt", "wb"),
        #                       asynchronous=True)
        # time.sleep(30)
        # out.seek(0)
        # err.seek(0)
        # print("S", out.read(30))
        # print("E", err.read())

        transport = self._client.get_transport()
        channel = transport.open_session()
        channel.get_pty()
        channel.settimeout(20)
        stdout = channel.makefile()
        # channel.exec_command(command)

        #stdin, stdout, stderr = \
        channel.exec_command("socat -d stdio /dev/ttyUSB3")

        channel2 = transport.open_session()
        channel2.get_pty()
        # channel2.settimeout(10)
        stdin = channel2.makefile_stdin("w")
        channel2.exec_command("cat >/dev/ttyUSB3")

        self._uart_channels = [channel, channel2]

        return stdout, stdin

    def reset_DIOT(self):
        if self._uart:
            raise NotImplementedError()

        client = self._get_connection()
        # stdin, stdout, stderr = client.exec_command('ls -l')
        # print((stdout.read(), stderr.read()))
        r = client.run("python3 power_cycle_diot.py")
        # print(r)
        assert r.ok

    def _get_connection(self) -> fabric.Connection:
        if self._connection:
            return self._connection

        password = getpass.getpass(f"Password for {self.username}@{self.host}: ")

        # open SSH session
        client = paramiko.client.SSHClient()
        client.load_system_host_keys()
        client.connect(self.host, username=self.username, password=password)
        self._client = client

        self._connection = fabric.Connection(self.host,
                                             user=self.username,
                                             connect_kwargs=dict(password=password))
        return self._connection
