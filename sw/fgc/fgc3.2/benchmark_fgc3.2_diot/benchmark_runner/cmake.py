import subprocess
from pathlib import Path


class CMake:
    def __init__(self, cmake_exe: Path):
        self.cmake_exe = cmake_exe

    def configure(self, source_dir: Path, build_dir: Path, options):
        subprocess.check_call([self.cmake_exe, "-S", source_dir, "-B", build_dir] + options)

    def build(self, build_dir: Path):
        subprocess.check_call([self.cmake_exe, "--build", build_dir])
