import logging

from ioutil import as_lines


logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)


class Uboot:
    def __init__(self, rc, wc, prompt):
        self.rc = rc
        self.wc = wc
        self.prompt = prompt

    def wait_for_prompt(self):
        stdout, stdin = self.rc, self.wc
        PROMPT = self.prompt

        for ln in as_lines(stdout, PROMPT):
            logger.debug("output: %s", ln)

            if ln == PROMPT:
                break
        else:
            raise TimeoutError("Didn't get expected prompt")

        # print("STDIN")
        # stdin.write(b"help\n")
        #
        # print("READ")
        #
        # for ln in as_lines(stdout, b"DIOT-sb> "):
        #     print("<", ln)
        #     if ln == PROMPT:
        #         break
        # else:
        #     raise TimeoutError("Didn't get expected prompt")

    def tftpboot(self, addr, host_ip, filename):
        self.wc.write(f"tftpboot 0x{addr:X} {host_ip}:{filename}\n".encode())
        logger.info("Sent tftpboot command, awaiting response")

        for ln in as_lines(self.rc, break_on_prompt=self.prompt):
            logger.debug("tftpboot reply: %s", ln)

            if ln.startswith(b"Bytes transferred = "):
                return
            elif ln.startswith(b"Retry count exceeded;"):
                raise Exception("Device failed to connect to TFTP server")
            elif ln.startswith(b"TFTP error:"):
                raise Exception(ln.decode(errors="ignore").strip())
            elif ln == self.prompt:
                break

            # print("RESP:", ln)

        raise Exception("Failed to parse command reply, or connection timed out")

    def tftpput(self, addr, length, host_ip, filename):
        self.wc.write(f"tftpput 0x{addr:X} 0x{length:X} {host_ip}:{filename}\n".encode())
        logger.info("Sent tftpput command, awaiting response")

        for ln in as_lines(self.rc, break_on_prompt=self.prompt):
            logger.debug("tftpput reply: %s", ln)

            if ln.startswith(b"Bytes transferred = "):
                return
            elif ln.startswith(b"Retry count exceeded;"):
                raise Exception("Device failed to connect to TFTP server")
            elif ln.startswith(b"TFTP error:"):
                raise Exception(ln.decode(errors="ignore").strip())
            elif ln == self.prompt:
                break

        raise Exception("Failed to parse command reply, or connection timed out")

    def send_command(self, cmd):
        self.wc.write(cmd.encode() + b"\n")

    def send_command_and_print_reply(self, cmd):
        self.wc.write(f"{cmd}\n".encode())
        logger.info("Sent tftpboot command, awaiting response")

        for ln in as_lines(self.rc, break_on_prompt=self.prompt):
            print(ln)
