import logging
from typing import Tuple, List

import numpy as np
import copy

from fortlogs_schemas import AnalogLog, AnalogLogComparison

logger = logging.getLogger(__name__)


def compare_log_objects(log: AnalogLogComparison,
                        ref_log: AnalogLogComparison,
                        threshold=1.0E-4,
                        only_signals: List[str] = []) -> Tuple[bool, AnalogLogComparison]:


    """
    Compares 2 synchronized log objects point by point

    @param log: fortlogs log object
    @param ref_log: reference fortlogs log object
    @param ref_log_id: reference log identifier in fortlogs
    @param test_name: test case
    @param threshold: error threshold
    @param only_signals: signal names to ignore
    @return: tuple (True/False, fortlogs log object with the difference between log and ref_log)
    """

    # check if ref_log exists
    if ref_log is None:
        error_msg = 'FAILED - Reference log not found'
        logger.error(error_msg)
        # _upload_log(log, test_name, error_msg, matched=False)
        return False, None

    # Pick which signals to compare
    select_signals = list()
    ref_select_signals = list()
    if len(only_signals):
        for signal in log.signals:
            for name in only_signals:
                if name in signal.name:
                    select_signals.append(signal)
        for signal in ref_log.signals:
            for name in only_signals:
                if name in signal.name:
                    ref_select_signals.append(signal)
    else:
        select_signals = log.signals
        ref_select_signals = ref_log.signals

    if len(select_signals) == 0 or len(ref_select_signals) == 0:
        error_msg = f'FAILED - Logs have no signals, {len(select_signals)} vs {len(ref_select_signals)}'
        logger.error(error_msg)
        # _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
        return False, None
    if len(select_signals[0].samples) != len(ref_select_signals[0].samples):
        error_msg = f'FAILED - Logs have different number of samples, {len(select_signals[0].samples)} vs {len(ref_select_signals[0].samples)}'
        logger.error(error_msg)
        # _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
        return False, None
    if log.period != ref_log.period:
        error_msg = 'FAILED - Logs have different period'
        logger.error(error_msg)
        # _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
        return False, None

    # Check if the logs contain the same signals
    if [signal.name for signal in select_signals] != [signal.name for signal in ref_select_signals]:
        error_msg = 'FAILED - Logs have different signals'
        logger.error(error_msg)
        logger.error("%s vs %s", [signal.name for signal in select_signals], [signal.name for signal in ref_select_signals])
        # _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
        return False, None

    # Check time differences
    # delta_time = log.timeOrigin - log.firstSampleTime
    # ref_delta_time = ref_log.timeOrigin - ref_log.firstSampleTime
    # diff_time = delta_time - ref_delta_time
    #
    # if(abs(diff_time) > time_tolerance_us * 1e-6):
    #     error_msg = f'FAILED - Time difference {diff_time} exceeds tolerance {time_tolerance_us}'
    #     logger.error(error_msg)
    #     _upload_log(log, test_name, error_msg, matched=False, ref_id=ref_log_id)
    #     return False, None

    # create a log with the differences
    diff_log = copy.deepcopy(log)

    failed_signals = []
    all_failed_samples = []

    # iterate signals to compare samples subtracting the numpy arrays
    for sig_index in range(len(select_signals)):
        curr_samples = np.array(select_signals[sig_index].samples, dtype=float)
        ref_samples = np.array(ref_select_signals[sig_index].samples, dtype=float)

        diff_signal = curr_samples - ref_samples

        max_ref = np.max(ref_samples)
        min_ref = np.min(ref_samples)

        # store diff_signal in the diff_log
        diff_log.signals[sig_index].samples = diff_signal.tolist()

        # Use the range to compute the signal tolerance
        delta = max_ref - min_ref
        if delta < threshold:
            sig_tolerance = threshold
        else:
            sig_tolerance = delta * threshold

        failed_samples = np.argwhere(abs(diff_signal) > sig_tolerance).tolist()
        # print("OK SAMPLES:  ", len(np.argwhere(abs(diff_signal) <= sig_tolerance).tolist()))
        # print("FAIL SAMPLES:", len(np.argwhere(abs(diff_signal) > sig_tolerance).tolist()))
        all_failed_samples += failed_samples

        # If there are failed samples store the signal in the failed_signals
        if len(failed_samples):
            failed_signals.append(select_signals[sig_index].name)

    # If there are failed signals, upload log to fortlogs with FAILED message and return False
    if len(failed_signals):
        # _upload_log(log, test_name,
        #             f"FAILED comparing signals {','.join(failed_signals)} at sample {min(all_failed_samples).pop()}; "
        #             f"threshold={threshold} only_signals={only_signals}",
        #             matched=False,
        #             ref_id=ref_log_id)
        return False, diff_log
    # Upload with SUCCESS and return True
    else:
        # _upload_log(log, test_name,
        #             f"threshold={threshold} only_signals={only_signals}",
        #             matched=True,
        #             ref_id=ref_log_id)
        return True, diff_log
