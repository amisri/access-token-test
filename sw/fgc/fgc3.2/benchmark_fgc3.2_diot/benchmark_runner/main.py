"""
open UART (also via ssh ...)

for entry in configuration_matrix:
    (detach UART if needed)
    reboot target
    (attach UART if needed)
    watch for boot messages
    (spam CR if needed -- FGC3.2)

    execute boot command
    capture output
    check for correct cookie in output
    parse output & save results


Terminology:
- a run
- a build / a configuration (many runs)
- a session (many benchmarks with various options)
"""

import base64
import datetime
import getpass
import hashlib
import itertools
import json
import logging
import shutil
import tempfile
import time
from pathlib import Path
from pprint import pprint

from cmake import CMake
from compare_log_objects import compare_log_objects
from ioutil import read_until_end_or_timeout, as_lines
from ssh_session import SshSession
from uboot import Uboot

from fortlogs_schemas import AnalogLog
import pyfgc_log

SSH_HOST = 'nucte25535'
SSH_USER = getpass.getuser()

CMAKE_PATH = Path(r"C:\Program Files\JetBrains\CLion 2021.3.4\bin\cmake\win\bin\cmake.exe")
SRC_DIR = Path(r"C:\fgc\sw\fgc\fgc3.2\cclibs_bare_metal")
OUTPUT_FILENAME = "cclibs_demo.bin"

TFTP_BASE_DIR = Path(r"C:\fgc\sw\fgc\fgc3.2\cclibs_bare_metal\build-diot")
TFTP_HOST_IP = "128.141.153.79"

PROGRAM_LOAD_ADDR = 0x4000_0000

CMAKE_OPTIONS = [
    "-DCMAKE_ASM_COMPILER=C:/gcc-arm-11.2-2022.02-mingw-w64-i686-aarch64-none-elf/bin/aarch64-none-elf-gcc.exe",
    "-DCMAKE_C_COMPILER=C:/gcc-arm-11.2-2022.02-mingw-w64-i686-aarch64-none-elf/bin/aarch64-none-elf-gcc.exe",
    "-DCMAKE_CXX_COMPILER=C:/gcc-arm-11.2-2022.02-mingw-w64-i686-aarch64-none-elf/bin/aarch64-none-elf-g++.exe",
    "-DCMAKE_MAKE_PROGRAM:FILEPATH=C:/Program Files/JetBrains/CLion 2021.3.4/bin/ninja/win/ninja.exe",
    "-G", "Ninja",

    "-DCMAKE_BUILD_TYPE=Release",

    "-DTARGET=diot",
]

BINARIES_PATH = Path("binaries")
LOGS_PATH = Path("logs")
RESULTS_PATH = Path("results")

ssh = SshSession(SSH_HOST, SSH_USER)

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(level=logging.DEBUG)

FPGA_ACCESS_VALUES = [0, 1]
LOGGING_ON_VALUES = [0, 1]
SCRIPT_VALUES = ["direct",
                 "idle",
                 ]

for dir in [BINARIES_PATH, LOGS_PATH, RESULTS_PATH]:
    dir.mkdir(parents=True, exist_ok=True)

# encode unix timestamp as 32-bit big-endian integer, then pass to b32hexencode
# this should preserve ordering
SESSION_ID = base64.b32hexencode(int(time.time()).to_bytes(length=4, byteorder="big", signed=False)).decode()
logger.info("session ID: %s", SESSION_ID)

all_configurations = list(itertools.product(FPGA_ACCESS_VALUES, LOGGING_ON_VALUES, SCRIPT_VALUES))
all_results = {}

for fpga_access, logging_on, script in all_configurations:
    # build_key = f"liblog={logging_on},script={script}"
    configuration_id = script + ("F" if fpga_access else "") + ("L" if logging_on else "")
    # the_uuid = RUN_ID + "_" + ("L" if logging_on else "") + "_" + script
    guid = SESSION_ID + "_" + configuration_id

    results = dict(uuid=str(guid))

    with tempfile.TemporaryDirectory() as dir:
        build_dir = Path(dir)

        # configure & build code
        cmake = CMake(CMAKE_PATH)
        cmake.configure(source_dir=SRC_DIR,
                        build_dir=build_dir,
                        options=CMAKE_OPTIONS + [
                            f"-DUSE_FPGA_ACCESS={fpga_access}",
                            f"-DUSE_LIBLOG={logging_on}",
                            f"-DUSE_SCRIPT={script}",
                        ])

        results["build_flags"] = (build_dir / "build_flags.txt").read_text().strip()

        build_timestamp_min = datetime.datetime.now()
        cmake.build(build_dir)
        build_timestamp_max = datetime.datetime.now()

        # copy to TFTP server dir
        shutil.copy(build_dir / OUTPUT_FILENAME, TFTP_BASE_DIR / f"{guid}.bin")
        # + back it up
        shutil.copy(build_dir / OUTPUT_FILENAME, BINARIES_PATH / f"{guid}.bin")

    # reset target
    logger.debug("reset_DIOT")
    ssh.reset_DIOT()

    logger.debug("open_uart")
    rc, wc = ssh.open_uart()

    uboot = Uboot(rc, wc, prompt=b"DIOT-sb> ")
    uboot.wait_for_prompt()
    uboot.tftpboot(PROGRAM_LOAD_ADDR, TFTP_HOST_IP, f"{guid}.bin")

    uboot.send_command(f"go 0x{PROGRAM_LOAD_ADDR:X}")

    # in the meantime, delete the temp binary
    try:
        (TFTP_BASE_DIR / f"{guid}.bin").unlink(missing_ok=True)
    except PermissionError:
        logger.exception("Failed to remove file. Oh well.")

    # save log, and also parse it
    log_unparsed = read_until_end_or_timeout(rc)
    log_lines = log_unparsed.decode(errors="replace").replace("\r", "").split("\n")
    # log_lines = [ln.decode(errors="replace").replace("\r", "") for ln in as_lines(rc, break_on_prompt="END")]

    ssh.close_uart()

    with open(LOGS_PATH / f"{guid}.log", "wb") as f:
        # f.write("\n".join(log_lines))
        f.write(log_unparsed)

    log_infos = {}

    for l in log_lines:
        if l.startswith("INFO "):
            print(configuration_id, l)
        elif l.startswith("LOG_INFO "):
            print(l)
            log_info = json.loads(l.removeprefix("LOG_INFO "))
            log_infos[log_info["name"]] = log_info
        elif l.startswith("RESULT "):
            keyword, key, value = l.split(" ")
            results[key] = value

    # print(results)

    all_results[configuration_id] = results

    for log_menu_name, log_info in log_infos.items():
        prefix = b"<<" + log_menu_name.encode() + b"<"
        start = log_unparsed.index(prefix)
        end = log_unparsed.index(b">>" + log_menu_name.encode() + b">")
        log_bin = log_unparsed[start + len(prefix):end].replace(b"\r\n", b"\n")
        # Path(f"{configuration_id}.bin").write_bytes(log_bin)

        md5 = hashlib.md5(log_bin).hexdigest()
        logger.debug("%s vs %s (exp); %d bytes vs %d (exp)", md5, log_info["md5sum"], len(log_bin), log_info["len"])
        assert md5 == log_info["md5sum"]

        # Convert binary log to PowerSpy format
        log_version = pyfgc_log.BufferVersion.V2.value
        decoded = pyfgc_log.decode(log_bin, log_version)
        encoded = pyfgc_log.encode(decoded, log_version)

        pydantic_log = AnalogLog.parse_raw(encoded)
        pydantic_log.to_csv(LOGS_PATH / f'{guid}_{log_info["name"]}')

        # compare log
        ref_log_path = Path("reference_logs") / script / f'{log_menu_name}.bin'
        try:
            decoded_ref = pyfgc_log.decode(ref_log_path.read_bytes(), log_version)
        except FileNotFoundError:
            continue

        encoded_ref = pyfgc_log.encode(decoded_ref, log_version)

        pydantic_log_ref = AnalogLog.parse_raw(encoded_ref)

        success, diff = compare_log_objects(pydantic_log,
                                            pydantic_log_ref,
                                            only_signals=['I_MEAS_REG_(A)'])

        if not success:
            raise Exception("LOG MISMATCH VS REFERENCE")


pprint(all_results)
with open(RESULTS_PATH / f"{SESSION_ID}.json", "wt") as f:
    json.dump(all_results, f, indent=2)
