#include "../common/misc.h"
#include "../common/cache.h"

void printRegs()
{
    printReg("Core Disable Register (COREDISR)             ", bs32(b32In(rCOREDISR)));
    printReg("Boot Release Register (BRR)                  ", bs32(b32In(rBRR)));
    uartSend("\n\r");
    printReg("Core Reset Status Register CRSTSR0           ", bs32(b32In(rCRSTSR0)));
    printReg("Core Reset Status Register CRSTSR1           ", bs32(b32In(rCRSTSR1)));
    printReg("Core Reset Status Register CRSTSR2           ", bs32(b32In(rCRSTSR2)));
    printReg("Core Reset Status Register CRSTSR3           ", bs32(b32In(rCRSTSR3)));
    uartSend("\n\r");
    printReg("Core Reset Vector Base 0.0                   ", bs32(b32In(rSCFG_RVBAR0_0)));
    printReg("Core Reset Vector Base 0.1                   ", bs32(b32In(rSCFG_RVBAR0_1)));
    printReg("Core Reset Vector Base 1.0                   ", bs32(b32In(rSCFG_RVBAR1_0)));
    printReg("Core Reset Vector Base 1.1                   ", bs32(b32In(rSCFG_RVBAR1_1)));
    printReg("Core Reset Vector Base 2.0                   ", bs32(b32In(rSCFG_RVBAR2_0)));
    printReg("Core Reset Vector Base 2.1                   ", bs32(b32In(rSCFG_RVBAR2_1)));
    printReg("Core Reset Vector Base 3.0                   ", bs32(b32In(rSCFG_RVBAR3_0)));
    printReg("Core Reset Vector Base 3.1                   ", bs32(b32In(rSCFG_RVBAR3_1)));
    uartSend("\n\r");
    printReg("Core Low Power Mode Control (SCFG_LPMCSR)    ", bs32(b32In(rSCFG_LPMCSR)));
    printReg("Core Boot Control Register (SCFG_COREBCR)    ", bs32(b32In(rSCFG_COREBCR)));
    printReg("CORE Soft Reset Enable (SCFG_CORESRENCR)     ", bs32(b32In(rSCFG_CORESRENCR)));
    printReg("CORE PM Control (SCFG_COREPMCR)              ", bs32(b32In(rSCFG_COREPMCR)));
    uartSend("\n\r");
    printReg("Thread Wait status Register (TWAITSR)        ", bs32(b32In(rTWAITSR)));
    printReg("Physical Core PH20 Status Register (PCPH20SR)", bs32(b32In(rPCPH20SR)));
    printReg("nFIQOUT Interrupt Register (nFIQOUTR)        ", bs32(b32In(rnFIQOUTR)));
    printReg("nIRQOUT interrupt mask register (nIRQOUTR)   ", bs32(b32In(rnIRQOUTR)));
}

void bootSecondary(const int pos)
{
    // This doesn't work, probably because it's lacking the d-cache flush

    #define SPIN_TABLE_ELEM_ENTRY_ADDR_IDX	0
    #define SPIN_TABLE_ELEM_STATUS_IDX	    1
    #define SPIN_TABLE_ELEM_LPID_IDX	    2
    #define SPIN_TABLE_ELEM_ARCH_COMP_IDX	3
    #define WORDS_PER_SPIN_TABLE_ENTRY	    8
    #define SPIN_TABLE_ELEM_SIZE		    64

    uint64_t  boot_addr = 0xF37FFC00UL;
    uint64_t* table     = (uint64_t*) 0xFBD2C5C0UL;

    table += pos * WORDS_PER_SPIN_TABLE_ENTRY;

    table[SPIN_TABLE_ELEM_ENTRY_ADDR_IDX] = boot_addr;
    __asm volatile("dmb sy" : : : "memory");
    __asm volatile("dsb sy" : : : "memory");

    __asm volatile ("ISB");

    sendSgi();
    sendSev();
}


void clearCache()
{
    tlb_invalidate_all();
    iciallu();
//    flush_dcache_all();
}

int main()
{
//    myCacheDisable();

//    printfInit();

    volatile uint8_t *a = (volatile uint8_t *)0xA0000000UL;

//    printf("Bare-metal BOOT started v2:myCacheDisable2 on EL%d\n\r", getCurrentEl());
    uartSend("Bare-metal BOOT started v2:  no using myCacheClear \n\r");


//    uartSend("Doing nothing \n\r");
    uartSend("Printing 0xA0000000 \n\r");
    while (1)
    {
        //myCacheClear(0xA0000000UL);

        uartSend("Mem =");
        uartSendChar(*a);
        uartSend("\n\r");
//        uartSendChar('.');

        delay();
    }
}