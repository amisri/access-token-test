#include "misc.h"

// **********************************************************

void delay()
{
    for (int i = 0; i < 18000000; ++i)
    {
    }
}

// **********************************************************

void shortDelay()
{
    for (int i = 0; i < 1000000; ++i)
    {
    }
}

// **********************************************************

uint8_t getCurrentEl()
{
    uint64_t regCurrentEL;
    __asm volatile ("mrs %0, CurrentEL" : "=r" (regCurrentEL));

    return (uint8_t)((regCurrentEL >> 2) & 0b11);
}

// **********************************************************

void printReg(const char* name, uint32_t reg)
{
    printf("%s = ", name);

    for (int i = 31; i >= 0; --i)
    {
        uartSendChar( (reg & bf(i)) == bf(i) ? '1' : '0');

        if (i % 8 == 0)
        {
            uartSendChar(' ');
        }
    }

    uartSend("\n\r");
}

// **********************************************************

void sendSgi()
{
    // GICD_BASE is 0x01410000

    __asm volatile ("mov x26, #0x0000");
    __asm volatile ("movk x26, #0x0141, lsl #16");

    __asm volatile ("mov w25, #0x8000");
    __asm volatile ("movk w25, #0x100, lsl #16");
    __asm volatile ("str w25, [x26, 0x0f00]");

    __asm volatile ("ISB");
}

// **********************************************************

void sendSev()
{
    __asm volatile ("sev");
}
