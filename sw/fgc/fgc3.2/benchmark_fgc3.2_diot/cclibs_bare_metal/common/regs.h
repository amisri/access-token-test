#pragma once

#include "bits.h"

// Core Disable Register
#define rCOREDISR            0x1EE0094
// Boot Release Register
#define rBRR                 0x1EE00E4

// Core Reset Status Register
#define rCRSTSR0             0x1EE0400
#define rCRSTSR1             0x1EE0404
#define rCRSTSR2             0x1EE0408
#define rCRSTSR3             0x1EE040C

// Core Reset Vector Base
#define rSCFG_RVBAR0_0       0x1570220
#define rSCFG_RVBAR0_1       0x1570224
#define rSCFG_RVBAR1_0       0x1570228
#define rSCFG_RVBAR1_1       0x157022C
#define rSCFG_RVBAR2_0       0x1570230
#define rSCFG_RVBAR2_1       0x1570234
#define rSCFG_RVBAR3_0       0x1570238
#define rSCFG_RVBAR3_1       0x157023C

// Core Low Power Mode Control Status Register
#define rSCFG_LPMCSR         0x1570240
//  Core Boot Control Register
#define rSCFG_COREBCR        0x1570680
// CORE PM Control Register
#define rSCFG_COREPMCR       0x157042C

// Core Soft Reset Enable Register
#define rSCFG_CORESRENCR     0x1570204
#define bCORESREN            nxpbf(0)

// Core soft reset Register
#define rSCFG_CORE0_SFT_RST  0x1570130
#define rSCFG_CORE1_SFT_RST  0x1570134
#define rSCFG_CORE2_SFT_RST  0x1570138
#define rSCFG_CORE3_SFT_RST  0x157013C

#define bSCFG_CORE_SFT_RST_RESET   nxpbf(0)


// Thread Wait status Register
#define rTWAITSR             0x1EE204C

// Physical Core PH20 Status Register
#define rPCPH20SR            0x1EE20D0

// nFIQOUT Interrupt Register
#define rnFIQOUTR            0x1EE215C

// nIRQOUT interrupt mask register
#define rnIRQOUTR            0x1EE216C