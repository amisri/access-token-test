#pragma once

#include "bits.h"

// ************************************************************
// UART registers
// ************************************************************

#ifdef _DIOT

#define UART_BASE 		0xFF000000
#define UART_TX_BUF		0xFF000030

#else

#define DUART_BASE 		0x21C0000		
#define UART_BASE 		0x500		

#define UDLB 			(DUART_BASE + UART_BASE + 0x0) 
#define URBR 			(DUART_BASE + UART_BASE + 0x0)
#define UTHR 			(DUART_BASE + UART_BASE + 0x0)
#define UDMB 			(DUART_BASE + UART_BASE + 0x1)
#define UIER 			(DUART_BASE + UART_BASE + 0x1)
#define UAFR 			(DUART_BASE + UART_BASE + 0x2)
#define UFCR 			(DUART_BASE + UART_BASE + 0x2)
#define UIIR 			(DUART_BASE + UART_BASE + 0x2)
#define ULCR 			(DUART_BASE + UART_BASE + 0x3)
#define UMCR 			(DUART_BASE + UART_BASE + 0x4)
#define ULSR 			(DUART_BASE + UART_BASE + 0x5)
#define UMSR 			(DUART_BASE + UART_BASE + 0x6)
#define USCR 			(DUART_BASE + UART_BASE + 0x7)
#define UDSR  			(DUART_BASE + UART_BASE + 0x10)


// ************************************************************
// UART bit fields
// ************************************************************

// Registry UIER - interrupt enable register
#define EMSI 			bf(3)
#define ERLSI 			bf(2)
#define ETHREI 			bf(1)
#define ERDAI 			bf(0)

// Registry UAFR - alternate function register
#define BO	 			bf(1)
#define CW  			bf(0)

// Registry UFCR - FIFO control register
#define RTL1 			bf(7)
#define RTL0 			bf(6)
#define EN64 			bf(5)
#define DMS 			bf(3)
#define TFR 			bf(2)
#define RFR				bf(1)
#define FEN 			bf(0)

// Registry UIIR - interrupt ID register
#define FE1 			bf(7)
#define FE0 			bf(6)
#define FE64 			bf(5)
#define IID3 			bf(3)
#define IID2 			bf(2)
#define IID1			bf(1)
#define IID0 			bf(0)

// Registry ULCR - line control register
#define DLAB 			bf(7)
#define SB	 			bf(6)
#define SP  			bf(5)
#define EPS				bf(4)
#define PEN 			bf(3)
#define NSTB 			bf(2)
#define WLS1			bf(1)
#define WLS0 			bf(0)

// Registry UMCR - modem control register
#define AFE  			bf(5)
#define LOOP			bf(4)
#define RTS				bf(1)

// Registry ULSR1 - line status register
#define RFE 			bf(7)
#define TEMT 			bf(6)
#define THRE  			bf(5)
#define BI				bf(4)
#define FE	 			bf(3)
#define PE	 			bf(2)
#define OE 				bf(1)
#define DR	 			bf(0)

// Registry UMSR - modem status register
#define CTS				bf(4)
#define DCTS 			bf(0)

// Registry UDSR - DMA status register
#define TXRDY			bf(1)
#define RXRDY 			bf(0)

#endif

// ************************************************************
// UART functions
// ************************************************************

void uartSendChar(byte c);

void uartSend(const char* str);

void uartSendN(const char* str, int len);

void uartSendHex(uint64_t value);

void uartWaitForSend();

void uartInit();
