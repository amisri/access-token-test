#include <stdio.h>

#include "bits/byteswap.h"

// SOME CODE BELOW COPIED FROM LINUX KERNEL HEADERS

typedef uint32_t u32;
typedef uint32_t __be32;

/*
 * The actual number of banks implemented depends on the IFC version
 *    - IFC version 1.0 implements 4 banks.
 *    - IFC version 1.1 onward implements 8 banks.
 */
#define FSL_IFC_BANK_COUNT 8

#define FSL_IFC_VERSION_MASK	0x0F0F0000
#define FSL_IFC_VERSION_1_0_0	0x01000000
#define FSL_IFC_VERSION_1_1_0	0x01010000
#define FSL_IFC_VERSION_2_0_0	0x02000000

/*
 * IFC controller GPCM Machine registers -- at +0x1800 apparently
 */
struct fsl_ifc_gpcm {
    __be32 gpcm_evter_stat;
    u32 res1[0x2];
    __be32 gpcm_evter_en;
    u32 res2[0x2];
    __be32 gpcm_evter_intr_en;
    u32 res3[0x2];
    __be32 gpcm_erattr0;
    __be32 gpcm_erattr1;
    __be32 gpcm_erattr2;
    __be32 gpcm_stat;
};

/*
 * IFC Controller Registers
 */
struct fsl_ifc_global {
    __be32 ifc_rev;
    u32 res1[0x2];
    struct {
        __be32 cspr_ext;
        __be32 cspr;
        u32 res2;
    } cspr_cs[FSL_IFC_BANK_COUNT];
    u32 res3[0xd];
    struct {
        __be32 amask;
        u32 res4[0x2];
    } amask_cs[FSL_IFC_BANK_COUNT];
    u32 res5[0xc];
    struct {
        __be32 csor;
        __be32 csor_ext;
        u32 res6;
    } csor_cs[FSL_IFC_BANK_COUNT];
    u32 res7[0xc];
    struct {
        __be32 ftim[4];
        u32 res8[0x8];
    } ftim_cs[FSL_IFC_BANK_COUNT];
    u32 res9[0x30];
    __be32 rb_stat;
    __be32 rb_map;
    __be32 wb_map;
    __be32 ifc_gcr;
    u32 res10[0x2];
    __be32 cm_evter_stat;
    u32 res11[0x2];
    __be32 cm_evter_en;
    u32 res12[0x2];
    __be32 cm_evter_intr_en;
    u32 res13[0x2];
    __be32 cm_erattr0;
    __be32 cm_erattr1;
    u32 res14[0x2];
    __be32 ifc_ccr;
    __be32 ifc_csr;
    __be32 ddr_ccr_low;
};

void ifc_init(void) {
    // start by configuring IFC. the specific register values are just dumped by halplay (we don't have its source anymore?)
    //  eventually we should reverse-engineer them to have some maintainable code

    struct fsl_ifc_global volatile* ifc =    (struct fsl_ifc_global*) 0x1530000;
    struct fsl_ifc_gpcm volatile* ifc_gpcm = (struct fsl_ifc_gpcm*) (0x1530000 + 0x1800);
    printf("IFC REV: %08Xh\r\n", __bswap_32(ifc->ifc_rev));

    const int bank = 2;   // hardcoded in hardware probably, or something.

    // CSPR_EXT at boot default
    ifc->cspr_cs[bank].cspr = __bswap_32(2142241029);        // CSPR
    // IFC_AMASK at boot default
    ifc->csor_cs[bank].csor = __bswap_32(2147614720);        // CSOR_GPCM
    // FTIM0..3_CS_GPCM at boot default
    // RB_STAT at boot default
    // GCR at boot default
    // CM_EVTER_STAT, CM_EVTER_EN at boot default
    ifc->cm_evter_intr_en = __bswap_32(2147483648);        // CM_EVTER_INTR_EN    = 2147483648
    // CM_ERATTR0, CM_ERATTR1 at boot default
//    ifc->ifc_ccr = __bswap_32(0x03008000);                  // CCR                 = 0x0300'8000 (boot default)
    ifc->ifc_ccr = __bswap_32(0x01008000);                  // CCR                 = 0x0100'8000 (2x faster)
    // GPCM_EVTER_STAT at boot default
    ifc_gpcm->gpcm_evter_en = __bswap_32(71303168);          //GPCM_EVTER_EN       = 71303168
    //GPCM_EVTER_INTR_EN, GPCM_ERATTR0, GPCM_ERATTR1, GPCM_ERATTR2, GPCM_STAT at boot defaults

//    printf("p_fpga_counter = %d\r\n", *p_fpga_counter);
//    my_sleep(20 * 125000000);
//    printf("p_fpga_counter = %d\r\n", *p_fpga_counter);
}
