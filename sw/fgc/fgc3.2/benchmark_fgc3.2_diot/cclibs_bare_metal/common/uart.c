#include "uart.h"

#ifdef _DIOT

void uartSendChar(byte c)
{
    // Write to UTHR to send data
    b32Out(UART_TX_BUF, c);
    for (int k = 0; k < 40000; ++k){}
}

// **********************************************************

void uartSend(const char* str)
{
    while (*str != 0)
    {
        b32Out(UART_TX_BUF, *str);
        str++;
        for (int k = 0; k < 40000; ++k){}
    }
}

// **********************************************************

void uartSendN(const char* str, int len)
{
    for (int i = 0; i < len; ++i)
    {
        b32Out(UART_TX_BUF, *str);
        str++;
        for (int k = 0; k < 40000; ++k){}
    }
}

// **********************************************************

void uartWaitForSend()
{
//    while (!bIsOne(ULSR, TEMT));
}

// **********************************************************

void uartInit()
{

}

#else

void uartSendChar(byte c)
{
    // Clear DLAB bit in the control register to make UTHR accessible
    // bClear(ULCR, DLAB);

    // Write to UTHR to send data
    uartWaitForSend();
    bOut(UTHR, c);
}

// **********************************************************

void uartSend(const char* str)
{
    while (*str != 0)
    {
        uartWaitForSend();
        bOut(UTHR, *str);
        str++;
    }
}

// **********************************************************

void uartSendN(const char* str, int len)
{
    for (int i = 0; i < len; ++i)
    {
        uartWaitForSend();
        bOut(UTHR, *str);
        str++;
    }
}

// **********************************************************

void uartWaitForSend()
{
    while (!bIsOne(ULSR, TEMT));
}

// **********************************************************

void uartInit()
{
    // DLAB - set DLAB bit in the control register to make UDLB accessible
    // WLS  - set word length to 8 bits
    bOut(ULCR, DLAB | WLS0 | WLS1);

    // Set divisor - 163
    bOut(UDLB, 163);
    bOut(UDMB, 0);

    // Clear DLAB
    bOut(ULCR, WLS0 | WLS1);
}

#endif

// **********************************************************

static char const hex_digits[] = "0123456789abcdef";

void uartSendHex(uint64_t value)
{
    // 64 bits -> 16 digits
    for (int i = 0; i < 16; i++) {
        uartSendChar(hex_digits[value >> 60]);
        value <<= 4;
    }
    uartSendChar('h');
}
