#pragma once
//
#include <stdint.h>
#include "byteswap.h"

typedef unsigned char byte;

// Bit field 
#define bf(x)				   ( 1 << (x) )
#define nxpbf(x)			   ( 1 << (31 - (x)) )

// Bit operations on 8 bit registers
#define bSet(reg, mask)        ( (*(volatile byte *)(reg)) |=  (mask) )
#define bClear(reg, mask)      ( (*(volatile byte *)(reg)) &= ~(mask) )

#define bIsOne(reg, mask)      ( ((*(volatile byte *)(reg)) & (mask)) == (mask) )

#define bIn(reg)               ( (*(volatile byte *)(reg)) ) 
#define bOut(reg, val)         ( (*(volatile byte *)(reg)) = (byte)(val) ) 

// Bit operations on 16 bit registers
#define b16Set(reg, mask)      ( (*(volatile uint16_t *)(reg)) |=  (mask) )
#define bvClear(reg, mask)     ( (*(volatile uint16_t *)(reg)) &= ~(mask) )

#define b16IsOne(reg, mask)    ( ((*(volatile uint16_t *)(reg)) & (mask)) == (mask) )

#define b16In(reg)             ( (*(volatile uint16_t *)(reg)) ) 
#define b16ut(reg, val)        ( (*(volatile uint16_t *)(reg)) = (uint16_t)(val) ) 


// Bit operations on 32 bit registers
#define b32Set(reg, mask)      ( (*(volatile uint32_t *)(reg)) |=  (mask) )
#define b32Clear(reg, mask)    ( (*(volatile uint32_t *)(reg)) &= ~(mask) )

#define b32IsOne(reg, mask)    ( ((*(volatile uint32_t *)(reg)) & (mask)) == (mask) )

#define b32In(reg)             ( (*(volatile uint32_t *)(reg)) ) 
#define b32Out(reg, val)       ( (*(volatile uint32_t *)(reg)) = (uint32_t)(val) ) 


// Bit operations on variables
#define bitSet(var, mask)      ( var |=  (mask) )
#define bitClear(var, mask)    ( var &= ~(mask) )
