#include "timer.h"

// **********************************************************

void timerStart()
{
	// Reset timer counter value
	b32Out(FTM1_CNT, 0);
	
	// Set clocking source to system clock
	uint32_t tmp = bs32(b32In(FTM1_SC));
	bitSet(tmp, CLKS0);
	b32Out(FTM1_SC, bs32(tmp));
}

// **********************************************************

void timerStop()
{
	// Disable clocking source
	uint32_t tmp = bs32(b32In(FTM1_SC));
	bitClear(tmp, CLKS0);
	bitClear(tmp, CLKS1);
	b32Out(FTM1_SC, bs32(tmp));
}

// **********************************************************

void timerInit()
{		
	// Disable clocking source
	timerStop();

	// Set MOD (final) value to max value of 16-bit timer
	b32Out(FTM1_MOD, bs32(0xFFFF));		

	// Set CNTIN (initial) value to 0
	b32Out(FTM1_CNTIN, 0);	
	
	// Set prescaler to 32  == Timer frequency is 32 768 Hz / 32 == 1024 Hz
	//						== Timer tick is 0.000976562 s
 	//						== Timer overflow is 64 s
	uint32_t SC = bs32(b32In(FTM1_SC));
	
	bitSet(SC, PS1);
	bitSet(SC, PS2);
	
	b32Out(FTM1_SC, bs32(SC));	
	
	// Reset timer counter value
	b32Out(FTM1_CNT, 0);
}

// **********************************************************

uint32_t timerRead()
{
	return (bs32(b32In(FTM1_CNT)));
}