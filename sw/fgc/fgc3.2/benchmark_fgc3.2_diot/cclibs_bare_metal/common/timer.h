#pragma once

#include "bits.h"

// ************************************************************
// Flex Timer registers
// ************************************************************

#define FTM1_SC        0x29D0000
#define FTM1_CNT       0x29D0004
#define FTM1_MOD       0x29D0008
#define FTM1_C0SC      0x29D000C
#define FTM1_C0V       0x29D0010
#define FTM1_C1SC      0x29D0014
#define FTM1_C1V       0x29D0018
#define FTM1_C2SC      0x29D001C
#define FTM1_C2V       0x29D0020
#define FTM1_C3SC      0x29D0024
#define FTM1_C3V       0x29D0028
#define FTM1_C4SC      0x29D002C
#define FTM1_C4V       0x29D0030
#define FTM1_C5SC      0x29D0034
#define FTM1_C5V       0x29D0038
#define FTM1_C6SC      0x29D003C
#define FTM1_C6V       0x29D0040
#define FTM1_C7SC      0x29D0044
#define FTM1_C7V       0x29D0048
#define FTM1_CNTIN     0x29D004C
#define FTM1_STATUS    0x29D0050
#define FTM1_MODE      0x29D0054
#define FTM1_SYNC      0x29D0058
#define FTM1_OUTINIT   0x29D005C
#define FTM1_OUTMASK   0x29D0060
#define FTM1_COMBINE   0x29D0064
#define FTM1_DEADTIME  0x29D0068
#define FTM1_EXTTRIG   0x29D006C
#define FTM1_POL       0x29D0070
#define FTM1_FMS       0x29D0074
#define FTM1_FILTER    0x29D0078
#define FTM1_FLTCTRL   0x29D007C
#define FTM1_QDCTRL    0x29D0080
#define FTM1_CONF      0x29D0084
#define FTM1_FLTPOL    0x29D0088
#define FTM1_SYNCONF   0x29D008C
#define FTM1_INVCTRL   0x29D0090
#define FTM1_SWOCTRL   0x29D0094
#define FTM1_PWMLOAD   0x29D0098



// ************************************************************
// Flex Timer bit fields
// ************************************************************

// Register SC - line control register
#define TOF 			bf(31 - 24)
#define TOIE			bf(31 - 25)
#define CPWMS  			bf(31 - 26)
#define CLKS1			bf(31 - 27)
#define CLKS0			bf(31 - 28)
#define PS2 			bf(31 - 29)
#define PS1				bf(31 - 30)
#define PS0	 			bf(31 - 31)


// ************************************************************
// Flex Timer functions
// ************************************************************

void timerStart();

void timerStop();

void timerInit();

uint32_t timerRead();