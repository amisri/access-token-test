#pragma once

#include <stdio.h>

#include "../common/uart.h"
#include "../common/regs.h"
#include "../common/retarget.h"

void delay();

void shortDelay();

uint8_t getCurrentEl();

void printReg(const char* name, uint32_t reg);

void sendSgi();

void sendSev();
