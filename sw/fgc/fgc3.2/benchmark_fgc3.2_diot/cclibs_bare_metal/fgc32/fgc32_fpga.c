#include "fgc32_fpga.h"

#include <stdio.h>

static uint32_t volatile* const p_fpga_counter_config = (uint32_t volatile*)(0x7fb00000 + 0x1b70);
static uint32_t volatile* const p_fpga_counter = (uint32_t volatile*)(0x7fb00000 + 0x1b7c);

__attribute__((unused)) static int const fpga_counter_freq = 125000000;
static int const fpga_counter_period_ns = 8;

void fpga_timer_init(void) {
    // Start counter
    *p_fpga_counter_config = 0x02;
}

void fpga_selftest(void) {
    fpga_timer_init();

    uint32_t reading1 = *p_fpga_counter;
    uint32_t reading2 = *p_fpga_counter;

    if (reading2 > reading1) {
        printf("FPGA init OK, counter is running\n");
    }
    else {
        printf("FPGA init / IFC config error -- CNT values %08Xh %08Xh\n", reading1, reading2);
    }
}

void my_usleep(int microseconds) {
    // TODO: handle case where ticks >= 2**31

    uint32_t ticks = microseconds * (1000 / fpga_counter_period_ns);
    uint32_t start_ticks = *p_fpga_counter;

    while ((uint32_t)(*p_fpga_counter - start_ticks) < ticks) {
        // do nothing
    }
}

uint64_t my_getnanosecs(void) {
    uint32_t val = *p_fpga_counter;

    static uint32_t last;
    static uint64_t corr = 0;

    if (val < last) {
//        printf("timer underflow! %08Xh -> %08Xh\n", last, val);
        corr += 0x100000000ull;
    }
    last = val;

    return (corr + val) * fpga_counter_period_ns;
}
