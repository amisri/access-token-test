#pragma once

#include <stdint.h>

void fpga_timer_init(void);
void fpga_selftest(void);

// NOTE: this overflows for ~30 seconds or more
// Best to only use for < 1 sec sleeps
void my_usleep(int microseconds);

uint64_t my_getnanosecs(void);
