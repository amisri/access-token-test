//! @file  inc_cclibs/sigStructs.h
//!
//! @brief Converter Control Signals library : Structures header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of libsig.
//!
//! libsig is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "libsig.h"

// Signals enums

enum SIG_cal_limits_enum
{
    SIG_CAL_LIMIT_ADC_A,
    SIG_CAL_LIMIT_ADC_B,
    SIG_CAL_LIMIT_ADC_C,
    SIG_CAL_LIMIT_ADC_D,
    SIG_CAL_LIMIT_DCCT_A,
    SIG_CAL_LIMIT_DCCT_B,
    SIG_CAL_LIMIT_V_PROBE,
    SIG_CAL_LIMIT_B_PROBE,
    SIG_CAL_LIMIT_I_PROBE,
    SIG_NUM_CAL_LIMITS,
};

enum SIG_temp_filters_enum
{
    SIG_TEMP_FILTER_INTERNAL,
    SIG_TEMP_FILTER_DCCT_A,
    SIG_TEMP_FILTER_DCCT_B,
    SIG_NUM_TEMP_FILTERS,
};

enum SIG_cal_refs_enum
{
    SIG_CAL_REF_INTERNAL,
    SIG_CAL_REF_EXTERNAL,
    SIG_NUM_CAL_REFS,
};

enum SIG_adcs_enum
{
    SIG_ADC_ADC_A,
    SIG_ADC_ADC_B,
    SIG_ADC_ADC_C,
    SIG_ADC_ADC_D,
    SIG_NUM_ADCS,
};

enum SIG_transducers_enum
{
    SIG_TRANSDUCER_DCCT_A,
    SIG_TRANSDUCER_DCCT_B,
    SIG_TRANSDUCER_V_PROBE,
    SIG_TRANSDUCER_B_PROBE,
    SIG_TRANSDUCER_I_PROBE,
    SIG_TRANSDUCER_V_AC,
    SIG_NUM_TRANSDUCERS,
};

enum SIG_selects_enum
{
    SIG_SELECT_I_MEAS,
    SIG_NUM_SELECTS,
};

// Signals structures

struct SIG_struct
{
    union SIG_cal_limits_union
    {
        struct SIG_cal_limit                     array[9];

        struct SIG_cal_limits
        {
            struct SIG_cal_limit                 adc_a;
            struct SIG_cal_limit                 adc_b;
            struct SIG_cal_limit                 adc_c;
            struct SIG_cal_limit                 adc_d;
            struct SIG_cal_limit                 dcct_a;
            struct SIG_cal_limit                 dcct_b;
            struct SIG_cal_limit                 v_probe;
            struct SIG_cal_limit                 b_probe;
            struct SIG_cal_limit                 i_probe;
        } named;
    } cal_limit;

    union SIG_temp_filters_union
    {
        struct SIG_temp_filter                   array[3];

        struct SIG_temp_filters
        {
            struct SIG_temp_filter               internal;
            struct SIG_temp_filter               dcct_a;
            struct SIG_temp_filter               dcct_b;
        } named;
    } temp_filter;

    union SIG_cal_refs_union
    {
        struct SIG_cal_ref                       array[2];

        struct SIG_cal_refs
        {
            struct SIG_cal_ref                   internal;
            struct SIG_cal_ref                   external;
        } named;
    } cal_ref;

    union SIG_adcs_union
    {
        struct SIG_adc                           array[4];

        struct SIG_adcs
        {
            struct SIG_adc                       adc_a;
            struct SIG_adc                       adc_b;
            struct SIG_adc                       adc_c;
            struct SIG_adc                       adc_d;
        } named;
    } adc;

    union SIG_transducers_union
    {
        struct SIG_transducer                    array[6];

        struct SIG_transducers
        {
            struct SIG_transducer                dcct_a;
            struct SIG_transducer                dcct_b;
            struct SIG_transducer                v_probe;
            struct SIG_transducer                b_probe;
            struct SIG_transducer                i_probe;
            struct SIG_transducer                v_ac;
        } named;
    } transducer;

    union SIG_selects_union
    {
        struct SIG_select                        array[1];

        struct SIG_selects
        {
            struct SIG_select                    i_meas;
        } named;
    } select;

    struct SIG_mgr                               mgr;
};

// EOF
