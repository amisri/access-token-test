//! @file  inc_cclibs/logStructs.h
//!
//! @brief Converter Control Logging library : Structures header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Include generated logMenus.h header file to have access to all the log specific constants

#include "logMenus.h"
#include "liblog/logStore.h"

// Declare LOG_buffers structure dedicated to the application - can be instantiated in slow RAM

struct LOG_buffers
{
    struct LOG_private priv   [LOG_NUM_LOGS];
    uint32_t           buffers[LOG_BUFFERS_LEN];

    // Log 1  : ACQ - acquisition log

    struct LOG_acq_structs
    {
        struct LOG_cycles          cycles   [1][LOG_ACQ_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_ACQ_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_ACQ_NUM_SIG_BUFS];
    } acq;

    // Log 2  : B_MEAS - field measurement log

    struct LOG_b_meas_structs
    {
        struct LOG_cycles          cycles   [1][LOG_B_MEAS_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_B_MEAS_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_B_MEAS_NUM_SIG_BUFS];
    } b_meas;

    // Log 3  : I_MEAS - current measurement log

    struct LOG_i_meas_structs
    {
        struct LOG_cycles          cycles   [1][LOG_I_MEAS_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_I_MEAS_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_I_MEAS_NUM_SIG_BUFS];
    } i_meas;

    // Log 4  : B_REG - field regulation log

    struct LOG_b_reg_structs
    {
        struct LOG_cycles          cycles   [1][LOG_B_REG_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_B_REG_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_B_REG_NUM_SIG_BUFS];
    } b_reg;

    // Log 5  : I_REG - current regulation log

    struct LOG_i_reg_structs
    {
        struct LOG_cycles          cycles   [1][LOG_I_REG_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_I_REG_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_I_REG_NUM_SIG_BUFS];
    } i_reg;

    // Log 6  : V_REF - voltage reference log

    struct LOG_v_ref_structs
    {
        struct LOG_cycles          cycles   [1][LOG_V_REF_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_V_REF_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_V_REF_NUM_SIG_BUFS];
    } v_ref;

    // Log 7  : V_REG - voltage regulation log

    struct LOG_v_reg_structs
    {
        struct LOG_cycles          cycles   [1][LOG_V_REG_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_V_REG_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_V_REG_NUM_SIG_BUFS];
    } v_reg;

    // Log 8  : I_1KHZ - 1 KHZ current log

    struct LOG_i_1khz_structs
    {
        struct LOG_cycles          cycles   [1][LOG_I_1KHZ_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_I_1KHZ_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_I_1KHZ_NUM_SIG_BUFS];
    } i_1khz;

    // Log 9  : I_EARTH - earth current log

    struct LOG_i_earth_structs
    {
        struct LOG_cycles          cycles   [1][LOG_I_EARTH_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_I_EARTH_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_I_EARTH_NUM_SIG_BUFS];
    } i_earth;

    // Log 10  : TEMP - temperature log

    struct LOG_temp_structs
    {
        struct LOG_cycles          cycles   [1][LOG_TEMP_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_TEMP_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_TEMP_NUM_SIG_BUFS];
    } temp;

    // Log 11  : FLAGS - digital flags log

    struct LOG_flags_structs
    {
        struct LOG_cycles          cycles   [1][LOG_FLAGS_NUM_CYCLES];
        struct LOG_priv_dig_signal signals  [1][LOG_FLAGS_NUM_SIGNALS];
    } flags;

    // Log 12  : BIS_FLAGS - BIS evaluation result

    struct LOG_bis_flags_structs
    {
        struct LOG_cycles          cycles   [1][LOG_BIS_FLAGS_NUM_CYCLES];
        struct LOG_priv_dig_signal signals  [1][LOG_BIS_FLAGS_NUM_SIGNALS];
    } bis_flags;

    // Log 13  : BIS_LIMITS - BIS limits

    struct LOG_bis_limits_structs
    {
        struct LOG_cycles          cycles   [1][LOG_BIS_LIMITS_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_BIS_LIMITS_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_BIS_LIMITS_NUM_SIG_BUFS];
    } bis_limits;

    // Log 14  : ILC_CYC - ILC cycle log

    struct LOG_ilc_cyc_structs
    {
        struct LOG_cycles          cycles   [1][LOG_ILC_CYC_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [1][LOG_ILC_CYC_NUM_SIGNALS];
        uint32_t                   selectors[1][LOG_ILC_CYC_NUM_SIG_BUFS];
    } ilc_cyc;

    // Log 15 - 30  : DIM - DIM analog signals log

    struct LOG_dim_structs
    {
        struct LOG_cycles          cycles   [16][LOG_DIM_NUM_CYCLES];
        struct LOG_priv_ana_signal signals  [16][LOG_DIM_NUM_SIGNALS];
        uint32_t                   selectors[16][LOG_DIM_NUM_SIG_BUFS];
    } dim;
};

// Declare LOG_structs structure dedicated to the application - should be in fast RAM

struct LOG_structs
{
    // Log structures array

    struct LOG_log  log[LOG_NUM_LOGS];

    // Start cycle structure

    struct LOG_start_cycle start_cycle;

    // Log 1  : ACQ - acquisition log

    struct LOG_rt_acq_structs
    {
        cc_float            * selector_ptrs[LOG_ACQ_NUM_SIG_BUFS];
    } acq[1];

    // Log 2  : B_MEAS - field measurement log

    struct LOG_rt_b_meas_structs
    {
        cc_float            * selector_ptrs[LOG_B_MEAS_NUM_SIG_BUFS];
    } b_meas[1];

    // Log 3  : I_MEAS - current measurement log

    struct LOG_rt_i_meas_structs
    {
        cc_float            * selector_ptrs[LOG_I_MEAS_NUM_SIG_BUFS];
    } i_meas[1];

    // Log 4  : B_REG - field regulation log

    struct LOG_rt_b_reg_structs
    {
        cc_float            * selector_ptrs[LOG_B_REG_NUM_SIG_BUFS];
    } b_reg[1];

    // Log 5  : I_REG - current regulation log

    struct LOG_rt_i_reg_structs
    {
        cc_float            * selector_ptrs[LOG_I_REG_NUM_SIG_BUFS];
    } i_reg[1];

    // Log 6  : V_REF - voltage reference log

    struct LOG_rt_v_ref_structs
    {
        cc_float            * selector_ptrs[LOG_V_REF_NUM_SIG_BUFS];
    } v_ref[1];

    // Log 7  : V_REG - voltage regulation log

    struct LOG_rt_v_reg_structs
    {
        cc_float            * selector_ptrs[LOG_V_REG_NUM_SIG_BUFS];
    } v_reg[1];

    // Log 8  : I_1KHZ - 1 KHZ current log

    struct LOG_rt_i_1khz_structs
    {
        cc_float            * selector_ptrs[LOG_I_1KHZ_NUM_SIG_BUFS];
    } i_1khz[1];

    // Log 9  : I_EARTH - earth current log

    struct LOG_rt_i_earth_structs
    {
        cc_float            * selector_ptrs[LOG_I_EARTH_NUM_SIG_BUFS];
    } i_earth[1];

    // Log 10  : TEMP - temperature log

    struct LOG_rt_temp_structs
    {
        cc_float            * selector_ptrs[LOG_TEMP_NUM_SIG_BUFS];
    } temp[1];

    // Log 11  : FLAGS - digital flags log

    struct LOG_rt_flags_structs
    {
        struct LOG_dig_signal signals[LOG_FLAGS_NUM_SIGNALS];
    } flags[1];

    // Log 12  : BIS_FLAGS - BIS evaluation result

    struct LOG_rt_bis_flags_structs
    {
        struct LOG_dig_signal signals[LOG_BIS_FLAGS_NUM_SIGNALS];
    } bis_flags[1];

    // Log 13  : BIS_LIMITS - BIS limits

    struct LOG_rt_bis_limits_structs
    {
        cc_float            * selector_ptrs[LOG_BIS_LIMITS_NUM_SIG_BUFS];
    } bis_limits[1];

    // Log 14  : ILC_CYC - ILC cycle log

    struct LOG_rt_ilc_cyc_structs
    {
        cc_float            * selector_ptrs[LOG_ILC_CYC_NUM_SIG_BUFS];
    } ilc_cyc[1];

    // Log 15 - 30  : DIM - DIM analog signals log

    struct LOG_rt_dim_structs
    {
        cc_float            * selector_ptrs[LOG_DIM_NUM_SIG_BUFS];
    } dim[16];
};

//! Cache selector pointers in case a selector has changed
//!
//! This should be called if a log menu multiplexer has been changed. It will cached the
//! pointers to the signal variables, which allows the storing of signals to be efficient.
//!
//! @param[in,out]  log_structs         Pointer to log_structs structure

static inline void logCacheSelectors(struct LOG_structs * log_structs)
{
    logStoreAnalogSelector(&log_structs->log[LOG_ACQ]);
    logStoreAnalogSelector(&log_structs->log[LOG_B_MEAS]);
    logStoreAnalogSelector(&log_structs->log[LOG_I_MEAS]);
    logStoreAnalogSelector(&log_structs->log[LOG_B_REG]);
    logStoreAnalogSelector(&log_structs->log[LOG_I_REG]);
    logStoreAnalogSelector(&log_structs->log[LOG_V_REF]);
    logStoreAnalogSelector(&log_structs->log[LOG_V_REG]);
}


//! Clear freezable postmortem logs
//!
//! This can be called in the background to reset all freezable postmortem logs before unfreezing.
//!
//! @param[in,out]  log_buffers         Pointer to log_buffers structure

static inline void logClearFreezablePostmortemLogs(struct LOG_buffers * const log_buffers)
{
    memset(&log_buffers->buffers[       0], 0,  1920000);   // Clear log ACQ
    memset(&log_buffers->buffers[  480000], 0, 11200000);   // Clear log B_MEAS
    memset(&log_buffers->buffers[ 3280000], 0, 11200000);   // Clear log I_MEAS
    memset(&log_buffers->buffers[ 6080000], 0,  5760000);   // Clear log B_REG
    memset(&log_buffers->buffers[ 7520000], 0,  9600000);   // Clear log I_REG
    memset(&log_buffers->buffers[ 9920000], 0, 11200000);   // Clear log V_REF
    memset(&log_buffers->buffers[12720000], 0,  4800000);   // Clear log V_REG
    memset(&log_buffers->buffers[14100000], 0,    32000);   // Clear log I_EARTH
    memset(&log_buffers->buffers[14168000], 0,  1600000);   // Clear log FLAGS
}


//! Change a signal name pointer
//!
//! The application can replace the automatically generated signal name string pointer
//! using this function. Note that the signal name string is not changed, only the pointer
//! to the name.
//!
//! @param[in,out]  log_buffers         Pointer to log_buffers structure
//! @param[in]      menu_index          Index of menu whose signal name pointer should be replaced
//! @param[in]      signal_index        Index of signal whose name pointer should be replaced
//! @param[in]      signal_name         Pointer to new signal name string

static inline void logChangeSignalName(struct LOG_buffers * const log_buffers,
                                       uint32_t             const menu_index,
                                       uint32_t             const signal_index,
                                       char         const * const signal_name)
{
    log_buffers->priv[menu_index].ad.analog.signals[signal_index].name = signal_name;
}


//! Change a signal units pointer
//!
//! The application can replace the automatically generated signal units string pointer
//! using this function. Note that the signal units string is not changed, only the pointer
//! to the units.
//!
//! @param[in,out]  log_buffers         Pointer to log_buffers structure
//! @param[in]      menu_index          Index of menu whose signal units pointer should be replaced
//! @param[in]      signal_index        Index of signal whose units pointer should be replaced
//! @param[in]      signal_units        Pointer to new signal units string

static inline void logChangeSignalUnits(struct LOG_buffers * const log_buffers,
                                        uint32_t             const menu_index,
                                        uint32_t             const signal_index,
                                        char         const * const signal_units)
{
    log_buffers->priv[menu_index].ad.analog.signals[signal_index].units = signal_units;
}

// EOF
