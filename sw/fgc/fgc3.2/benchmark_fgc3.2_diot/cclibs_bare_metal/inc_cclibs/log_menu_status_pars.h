//! @file  inc_cclibs/log_menu_status_pars.h
//!
//! @brief Converter Control Logging library : ccrt log menu pars header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

{ "ACQ"               , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_ACQ                 ]}, 1, PARS_RW },
{ "ACQ_MPX"           , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_ACQ_MPX             ]}, 1, PARS_RW },
{ "B_MEAS"            , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_B_MEAS              ]}, 1, PARS_RW },
{ "B_MEAS_MPX"        , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_B_MEAS_MPX          ]}, 1, PARS_RW },
{ "I_MEAS"            , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_I_MEAS              ]}, 1, PARS_RW },
{ "I_MEAS_MPX"        , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_I_MEAS_MPX          ]}, 1, PARS_RW },
{ "B_REG"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_B_REG               ]}, 1, PARS_RW },
{ "B_REG_MPX"         , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_B_REG_MPX           ]}, 1, PARS_RW },
{ "I_REG"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_I_REG               ]}, 1, PARS_RW },
{ "I_REG_MPX"         , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_I_REG_MPX           ]}, 1, PARS_RW },
{ "V_REF"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_V_REF               ]}, 1, PARS_RW },
{ "V_REF_MPX"         , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_V_REF_MPX           ]}, 1, PARS_RW },
{ "V_REG"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_V_REG               ]}, 1, PARS_RW },
{ "V_REG_MPX"         , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_V_REG_MPX           ]}, 1, PARS_RW },
{ "I_1KHZ"            , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_I_1KHZ              ]}, 1, PARS_RW },
{ "I_EARTH"           , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_I_EARTH             ]}, 1, PARS_RW },
{ "TEMP"              , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_TEMP                ]}, 1, PARS_RW },
{ "V_AC_HZ"           , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_V_AC_HZ             ]}, 1, PARS_RW },
{ "FLAGS"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_FLAGS               ]}, 1, PARS_RW },
{ "BIS_CH1_OK"        , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_CH1_OK          ]}, 1, PARS_RW },
{ "BIS_CH2_OK"        , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_CH2_OK          ]}, 1, PARS_RW },
{ "BIS_CH3_OK"        , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_CH3_OK          ]}, 1, PARS_RW },
{ "BIS_CH4_OK"        , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_CH4_OK          ]}, 1, PARS_RW },
{ "BIS_CH1_TESTS"     , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_CH1_TESTS       ]}, 1, PARS_RW },
{ "BIS_CH2_TESTS"     , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_CH2_TESTS       ]}, 1, PARS_RW },
{ "BIS_CH3_TESTS"     , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_CH3_TESTS       ]}, 1, PARS_RW },
{ "BIS_CH4_TESTS"     , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_CH4_TESTS       ]}, 1, PARS_RW },
{ "BIS_LIMITS_CH1"    , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_LIMITS_CH1      ]}, 1, PARS_RW },
{ "BIS_LIMITS_CH2"    , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_LIMITS_CH2      ]}, 1, PARS_RW },
{ "BIS_LIMITS_CH3"    , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_LIMITS_CH3      ]}, 1, PARS_RW },
{ "BIS_LIMITS_CH4"    , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_BIS_LIMITS_CH4      ]}, 1, PARS_RW },
{ "ILC_CYC"           , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_ILC_CYC             ]}, 1, PARS_RW },
{ "DIM"               , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM                 ]}, 1, PARS_RW },
{ "DIM2"              , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM2                ]}, 1, PARS_RW },
{ "DIM3"              , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM3                ]}, 1, PARS_RW },
{ "DIM4"              , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM4                ]}, 1, PARS_RW },
{ "DIM5"              , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM5                ]}, 1, PARS_RW },
{ "DIM6"              , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM6                ]}, 1, PARS_RW },
{ "DIM7"              , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM7                ]}, 1, PARS_RW },
{ "DIM8"              , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM8                ]}, 1, PARS_RW },
{ "DIM9"              , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM9                ]}, 1, PARS_RW },
{ "DIM10"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM10               ]}, 1, PARS_RW },
{ "DIM11"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM11               ]}, 1, PARS_RW },
{ "DIM12"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM12               ]}, 1, PARS_RW },
{ "DIM13"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM13               ]}, 1, PARS_RW },
{ "DIM14"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM14               ]}, 1, PARS_RW },
{ "DIM15"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM15               ]}, 1, PARS_RW },
{ "DIM16"             , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_DIM16               ]}, 1, PARS_RW },
{ "EVENTS"            , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_EVENTS              ]}, 1, PARS_RW },
{ "CYCLES"            , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_CYCLES              ]}, 1, PARS_RW },
{ "TIMING"            , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_TIMING              ]}, 1, PARS_RW },
{ "CONFIGURATION"     , PAR_BITMASK, 1, enum_log_menu_status, { .u = &log_menus.status_bit_mask[LOG_MENU_CONFIGURATION       ]}, 1, PARS_RW },

// EOF
