//! @file  inc_cclibs/log_mpx_enums.h
//!
//! @brief Converter Control Logging library : ccrt log mpx enums header file
//!
//! <h3> IMPORTANT - DO NOT EDIT - This file was generated automatically </h3>
//!
//! <h4> Contact </h4>
//!
//! cclibs-devs@cern.ch
//!
//! <h4> Copyright </h4>
//!
//! Copyright CERN 2017. This project is released under the GNU Lesser General
//! Public License version 3.
//!
//! <h4> License </h4>
//!
//! This file is part of liblog.
//!
//! liblog is free software: you can redistribute it and/or modify it under the
//! terms of the GNU Lesser General Public License as published by the Free
//! Software Foundation, either version 3 of the License, or (at your option)
//! any later version.
//!
//! This program is distributed in the hope that it will be useful, but WITHOUT
//! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//! FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//! for more details.
//!
//! You should have received a copy of the GNU Lesser General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Log names enum

CCPARS_LOG_EXT struct CC_pars_enum enum_logs[]
#ifdef GLOBALS
= {
    { LOG_ACQ                       , "ACQ"                },
    { LOG_B_MEAS                    , "B_MEAS"             },
    { LOG_I_MEAS                    , "I_MEAS"             },
    { LOG_B_REG                     , "B_REG"              },
    { LOG_I_REG                     , "I_REG"              },
    { LOG_V_REF                     , "V_REF"              },
    { LOG_V_REG                     , "V_REG"              },
    { LOG_I_1KHZ                    , "I_1KHZ"             },
    { LOG_I_EARTH                   , "I_EARTH"            },
    { LOG_TEMP                      , "TEMP"               },
    { LOG_FLAGS                     , "FLAGS"              },
    { LOG_BIS_FLAGS                 , "BIS_FLAGS"          },
    { LOG_BIS_LIMITS                , "BIS_LIMITS"         },
    { LOG_ILC_CYC                   , "ILC_CYC"            },
    { LOG_DIM                       , "DIM"                },
    { LOG_DIM2                      , "DIM2"               },
    { LOG_DIM3                      , "DIM3"               },
    { LOG_DIM4                      , "DIM4"               },
    { LOG_DIM5                      , "DIM5"               },
    { LOG_DIM6                      , "DIM6"               },
    { LOG_DIM7                      , "DIM7"               },
    { LOG_DIM8                      , "DIM8"               },
    { LOG_DIM9                      , "DIM9"               },
    { LOG_DIM10                     , "DIM10"              },
    { LOG_DIM11                     , "DIM11"              },
    { LOG_DIM12                     , "DIM12"              },
    { LOG_DIM13                     , "DIM13"              },
    { LOG_DIM14                     , "DIM14"              },
    { LOG_DIM15                     , "DIM15"              },
    { LOG_DIM16                     , "DIM16"              },
    { 0                             , NULL                 },
}
#endif
;

// Log ACQ enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_acq[]
#ifdef GLOBALS
= {
    { LOG_ACQ_V_ADC_A               , "V_ADC_A"            },
    { LOG_ACQ_V_ADC_B               , "V_ADC_B"            },
    { LOG_ACQ_V_ADC_C               , "V_ADC_C"            },
    { LOG_ACQ_V_ADC_D               , "V_ADC_D"            },
    { LOG_ACQ_I_DCCT_A              , "I_DCCT_A"           },
    { LOG_ACQ_I_DCCT_B              , "I_DCCT_B"           },
    { LOG_ACQ_I_DIFF                , "I_DIFF"             },
    { LOG_ACQ_DSP_RT_PROF_0         , "DSP_RT_PROF_0"      },
    { LOG_ACQ_DSP_RT_PROF_1         , "DSP_RT_PROF_1"      },
    { LOG_ACQ_DSP_RT_CPU            , "DSP_RT_CPU"         },
    { 0                             , NULL                 },
}
#endif
;

// Log ACQ enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_acq_mpx[]
#ifdef GLOBALS
= {
    { LOG_ACQ_V_ADC_A               , "V_ADC_A"            },
    { LOG_ACQ_V_ADC_B               , "V_ADC_B"            },
    { LOG_ACQ_V_ADC_C               , "V_ADC_C"            },
    { LOG_ACQ_V_ADC_D               , "V_ADC_D"            },
    { LOG_ACQ_I_DCCT_A              , "I_DCCT_A"           },
    { LOG_ACQ_I_DCCT_B              , "I_DCCT_B"           },
    { LOG_ACQ_I_DIFF                , "I_DIFF"             },
    { LOG_ACQ_DSP_RT_PROF_0         , "DSP_RT_PROF_0"      },
    { LOG_ACQ_DSP_RT_PROF_1         , "DSP_RT_PROF_1"      },
    { LOG_ACQ_DSP_RT_CPU            , "DSP_RT_CPU"         },
    { 0                             , NULL                 },
}
#endif
;

// Log B_MEAS enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_b_meas[]
#ifdef GLOBALS
= {
    { LOG_B_MEAS_B_MEAS             , "B_MEAS"             },
    { LOG_B_MEAS_B_MEAS_FLTR        , "B_MEAS_FLTR"        },
    { LOG_B_MEAS_B_MEAS_EXTR        , "B_MEAS_EXTR"        },
    { LOG_B_MEAS_B_REF_DELAYED      , "B_REF_DELAYED"      },
    { LOG_B_MEAS_B_ERR              , "B_ERR"              },
    { LOG_B_MEAS_B_MAX_ABS_ERR      , "B_MAX_ABS_ERR"      },
    { LOG_B_MEAS_B_MEAS_SIM         , "B_MEAS_SIM"         },
    { LOG_B_MEAS_I_MEAS             , "I_MEAS"             },
    { LOG_B_MEAS_V_REF              , "V_REF"              },
    { LOG_B_MEAS_V_MEAS             , "V_MEAS"             },
    { 0                             , NULL                 },
}
#endif
;

// Log B_MEAS enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_b_meas_mpx[]
#ifdef GLOBALS
= {
    { LOG_B_MEAS_B_MEAS             , "B_MEAS"             },
    { LOG_B_MEAS_B_MEAS_FLTR        , "B_MEAS_FLTR"        },
    { LOG_B_MEAS_B_MEAS_EXTR        , "B_MEAS_EXTR"        },
    { LOG_B_MEAS_B_REF_DELAYED      , "B_REF_DELAYED"      },
    { LOG_B_MEAS_B_ERR              , "B_ERR"              },
    { LOG_B_MEAS_B_MAX_ABS_ERR      , "B_MAX_ABS_ERR"      },
    { LOG_B_MEAS_B_MEAS_SIM         , "B_MEAS_SIM"         },
    { LOG_B_MEAS_I_MEAS             , "I_MEAS"             },
    { LOG_B_MEAS_V_REF              , "V_REF"              },
    { LOG_B_MEAS_V_MEAS             , "V_MEAS"             },
    { 0                             , NULL                 },
}
#endif
;

// Log I_MEAS enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_i_meas[]
#ifdef GLOBALS
= {
    { LOG_I_MEAS_I_MEAS             , "I_MEAS"             },
    { LOG_I_MEAS_I_MEAS_FLTR        , "I_MEAS_FLTR"        },
    { LOG_I_MEAS_I_MEAS_EXTR        , "I_MEAS_EXTR"        },
    { LOG_I_MEAS_I_REF_DELAYED      , "I_REF_DELAYED"      },
    { LOG_I_MEAS_I_ERR              , "I_ERR"              },
    { LOG_I_MEAS_I_MAX_ABS_ERR      , "I_MAX_ABS_ERR"      },
    { LOG_I_MEAS_I_MEAS_SIM         , "I_MEAS_SIM"         },
    { LOG_I_MEAS_I_RMS              , "I_RMS"              },
    { LOG_I_MEAS_I_RMS_LOAD         , "I_RMS_LOAD"         },
    { LOG_I_MEAS_V_REF              , "V_REF"              },
    { LOG_I_MEAS_V_MEAS             , "V_MEAS"             },
    { LOG_I_MEAS_I_CAPA_MEAS        , "I_CAPA_MEAS"        },
    { LOG_I_MEAS_B_MEAS             , "B_MEAS"             },
    { 0                             , NULL                 },
}
#endif
;

// Log I_MEAS enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_i_meas_mpx[]
#ifdef GLOBALS
= {
    { LOG_I_MEAS_I_MEAS             , "I_MEAS"             },
    { LOG_I_MEAS_I_MEAS_FLTR        , "I_MEAS_FLTR"        },
    { LOG_I_MEAS_I_MEAS_EXTR        , "I_MEAS_EXTR"        },
    { LOG_I_MEAS_I_REF_DELAYED      , "I_REF_DELAYED"      },
    { LOG_I_MEAS_I_ERR              , "I_ERR"              },
    { LOG_I_MEAS_I_MAX_ABS_ERR      , "I_MAX_ABS_ERR"      },
    { LOG_I_MEAS_I_MEAS_SIM         , "I_MEAS_SIM"         },
    { LOG_I_MEAS_I_RMS              , "I_RMS"              },
    { LOG_I_MEAS_I_RMS_LOAD         , "I_RMS_LOAD"         },
    { LOG_I_MEAS_V_REF              , "V_REF"              },
    { LOG_I_MEAS_V_MEAS             , "V_MEAS"             },
    { LOG_I_MEAS_I_CAPA_MEAS        , "I_CAPA_MEAS"        },
    { LOG_I_MEAS_B_MEAS             , "B_MEAS"             },
    { 0                             , NULL                 },
}
#endif
;

// Log B_REG enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_b_reg[]
#ifdef GLOBALS
= {
    { LOG_B_REG_B_MEAS_REG          , "B_MEAS_REG"         },
    { LOG_B_REG_B_MEAS_RATE         , "B_MEAS_RATE"        },
    { LOG_B_REG_B_REF_DIRECT        , "B_REF_DIRECT"       },
    { LOG_B_REG_B_REF_USER          , "B_REF_USER"         },
    { LOG_B_REG_B_REF_ADV           , "B_REF_ADV"          },
    { LOG_B_REG_B_REF_LIMITED       , "B_REF_LIMITED"      },
    { LOG_B_REG_B_REF_CLOSED        , "B_REF_CLOSED"       },
    { LOG_B_REG_B_REF_OPEN          , "B_REF_OPEN"         },
    { LOG_B_REG_B_REF_RATE          , "B_REF_RATE"         },
    { LOG_B_REG_B_REF_ILC           , "B_REF_ILC"          },
    { LOG_B_REG_B_ERR_ILC           , "B_ERR_ILC"          },
    { LOG_B_REG_B_ERR               , "B_ERR"              },
    { LOG_B_REG_B_TRACK_DELAY       , "B_TRACK_DELAY"      },
    { LOG_B_REG_B_RMS_ERR           , "B_RMS_ERR"          },
    { LOG_B_REG_V_REF               , "V_REF"              },
    { LOG_B_REG_V_MEAS              , "V_MEAS"             },
    { LOG_B_REG_FG_TIME             , "FG_TIME"            },
    { LOG_B_REG_FG_TIME_ADV         , "FG_TIME_ADV"        },
    { LOG_B_REG_REF_ADVANCE         , "REF_ADVANCE"        },
    { 0                             , NULL                 },
}
#endif
;

// Log B_REG enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_b_reg_mpx[]
#ifdef GLOBALS
= {
    { LOG_B_REG_B_MEAS_REG          , "B_MEAS_REG"         },
    { LOG_B_REG_B_MEAS_RATE         , "B_MEAS_RATE"        },
    { LOG_B_REG_B_REF_DIRECT        , "B_REF_DIRECT"       },
    { LOG_B_REG_B_REF_USER          , "B_REF_USER"         },
    { LOG_B_REG_B_REF_ADV           , "B_REF_ADV"          },
    { LOG_B_REG_B_REF_LIMITED       , "B_REF_LIMITED"      },
    { LOG_B_REG_B_REF_CLOSED        , "B_REF_CLOSED"       },
    { LOG_B_REG_B_REF_OPEN          , "B_REF_OPEN"         },
    { LOG_B_REG_B_REF_RATE          , "B_REF_RATE"         },
    { LOG_B_REG_B_REF_ILC           , "B_REF_ILC"          },
    { LOG_B_REG_B_ERR_ILC           , "B_ERR_ILC"          },
    { LOG_B_REG_B_ERR               , "B_ERR"              },
    { LOG_B_REG_B_TRACK_DELAY       , "B_TRACK_DELAY"      },
    { LOG_B_REG_B_RMS_ERR           , "B_RMS_ERR"          },
    { LOG_B_REG_V_REF               , "V_REF"              },
    { LOG_B_REG_V_MEAS              , "V_MEAS"             },
    { LOG_B_REG_FG_TIME             , "FG_TIME"            },
    { LOG_B_REG_FG_TIME_ADV         , "FG_TIME_ADV"        },
    { LOG_B_REG_REF_ADVANCE         , "REF_ADVANCE"        },
    { 0                             , NULL                 },
}
#endif
;

// Log I_REG enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_i_reg[]
#ifdef GLOBALS
= {
    { LOG_I_REG_I_MEAS_REG          , "I_MEAS_REG"         },
    { LOG_I_REG_I_MEAS_RATE         , "I_MEAS_RATE"        },
    { LOG_I_REG_I_REF_DIRECT        , "I_REF_DIRECT"       },
    { LOG_I_REG_I_REF_USER          , "I_REF_USER"         },
    { LOG_I_REG_I_REF_ADV           , "I_REF_ADV"          },
    { LOG_I_REG_I_REF_LIMITED       , "I_REF_LIMITED"      },
    { LOG_I_REG_I_REF_CLOSED        , "I_REF_CLOSED"       },
    { LOG_I_REG_I_REF_OPEN          , "I_REF_OPEN"         },
    { LOG_I_REG_I_REF_RATE          , "I_REF_RATE"         },
    { LOG_I_REG_I_REF_ILC           , "I_REF_ILC"          },
    { LOG_I_REG_I_ERR_ILC           , "I_ERR_ILC"          },
    { LOG_I_REG_I_ERR               , "I_ERR"              },
    { LOG_I_REG_I_TRACK_DELAY       , "I_TRACK_DELAY"      },
    { LOG_I_REG_I_RMS_ERR           , "I_RMS_ERR"          },
    { LOG_I_REG_SAT_FACTOR          , "SAT_FACTOR"         },
    { LOG_I_REG_I_MAG_SAT           , "I_MAG_SAT"          },
    { LOG_I_REG_MEAS_OHMS           , "MEAS_OHMS"          },
    { LOG_I_REG_MEAS_HENRYS         , "MEAS_HENRYS"        },
    { LOG_I_REG_HENRYS_SAT          , "HENRYS_SAT"         },
    { LOG_I_REG_POWER               , "POWER"              },
    { LOG_I_REG_V_REF               , "V_REF"              },
    { LOG_I_REG_V_MEAS              , "V_MEAS"             },
    { LOG_I_REG_FG_TIME             , "FG_TIME"            },
    { LOG_I_REG_FG_TIME_ADV         , "FG_TIME_ADV"        },
    { LOG_I_REG_REF_ADVANCE         , "REF_ADVANCE"        },
    { 0                             , NULL                 },
}
#endif
;

// Log I_REG enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_i_reg_mpx[]
#ifdef GLOBALS
= {
    { LOG_I_REG_I_MEAS_REG          , "I_MEAS_REG"         },
    { LOG_I_REG_I_MEAS_RATE         , "I_MEAS_RATE"        },
    { LOG_I_REG_I_REF_DIRECT        , "I_REF_DIRECT"       },
    { LOG_I_REG_I_REF_USER          , "I_REF_USER"         },
    { LOG_I_REG_I_REF_ADV           , "I_REF_ADV"          },
    { LOG_I_REG_I_REF_LIMITED       , "I_REF_LIMITED"      },
    { LOG_I_REG_I_REF_CLOSED        , "I_REF_CLOSED"       },
    { LOG_I_REG_I_REF_OPEN          , "I_REF_OPEN"         },
    { LOG_I_REG_I_REF_RATE          , "I_REF_RATE"         },
    { LOG_I_REG_I_REF_ILC           , "I_REF_ILC"          },
    { LOG_I_REG_I_ERR_ILC           , "I_ERR_ILC"          },
    { LOG_I_REG_I_ERR               , "I_ERR"              },
    { LOG_I_REG_I_TRACK_DELAY       , "I_TRACK_DELAY"      },
    { LOG_I_REG_I_RMS_ERR           , "I_RMS_ERR"          },
    { LOG_I_REG_SAT_FACTOR          , "SAT_FACTOR"         },
    { LOG_I_REG_I_MAG_SAT           , "I_MAG_SAT"          },
    { LOG_I_REG_MEAS_OHMS           , "MEAS_OHMS"          },
    { LOG_I_REG_MEAS_HENRYS         , "MEAS_HENRYS"        },
    { LOG_I_REG_HENRYS_SAT          , "HENRYS_SAT"         },
    { LOG_I_REG_POWER               , "POWER"              },
    { LOG_I_REG_V_REF               , "V_REF"              },
    { LOG_I_REG_V_MEAS              , "V_MEAS"             },
    { LOG_I_REG_FG_TIME             , "FG_TIME"            },
    { LOG_I_REG_FG_TIME_ADV         , "FG_TIME_ADV"        },
    { LOG_I_REG_REF_ADVANCE         , "REF_ADVANCE"        },
    { 0                             , NULL                 },
}
#endif
;

// Log V_REF enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_v_ref[]
#ifdef GLOBALS
= {
    { LOG_V_REF_V_MEAS              , "V_MEAS"             },
    { LOG_V_REF_V_REF_REG           , "V_REF_REG"          },
    { LOG_V_REF_V_REF_SAT           , "V_REF_SAT"          },
    { LOG_V_REF_V_REF_FF            , "V_REF_FF"           },
    { LOG_V_REF_V_REF_DECO          , "V_REF_DECO"         },
    { LOG_V_REF_V_REF               , "V_REF"              },
    { LOG_V_REF_V_REF_VS            , "V_REF_VS"           },
    { LOG_V_REF_V_REF_DAC           , "V_REF_DAC"          },
    { LOG_V_REF_V_REF_RATE          , "V_REF_RATE"         },
    { LOG_V_REF_V_RATE_RMS          , "V_RATE_RMS"         },
    { LOG_V_REF_V_FEEDFWD           , "V_FEEDFWD"          },
    { LOG_V_REF_V_FF                , "V_FF"               },
    { LOG_V_REF_V_FF_FLTR           , "V_FF_FLTR"          },
    { LOG_V_REF_V_HARMONICS         , "V_HARMONICS"        },
    { LOG_V_REF_V_ERR               , "V_ERR"              },
    { LOG_V_REF_V_MAX_ABS_ERR       , "V_MAX_ABS_ERR"      },
    { LOG_V_REF_V_MEAS_SIM          , "V_MEAS_SIM"         },
    { LOG_V_REF_FG_REF              , "FG_REF"             },
    { LOG_V_REF_RT_REF              , "RT_REF"             },
    { LOG_V_REF_B_MEAS              , "B_MEAS"             },
    { LOG_V_REF_I_MEAS              , "I_MEAS"             },
    { 0                             , NULL                 },
}
#endif
;

// Log V_REF enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_v_ref_mpx[]
#ifdef GLOBALS
= {
    { LOG_V_REF_V_MEAS              , "V_MEAS"             },
    { LOG_V_REF_V_REF_REG           , "V_REF_REG"          },
    { LOG_V_REF_V_REF_SAT           , "V_REF_SAT"          },
    { LOG_V_REF_V_REF_FF            , "V_REF_FF"           },
    { LOG_V_REF_V_REF_DECO          , "V_REF_DECO"         },
    { LOG_V_REF_V_REF               , "V_REF"              },
    { LOG_V_REF_V_REF_VS            , "V_REF_VS"           },
    { LOG_V_REF_V_REF_DAC           , "V_REF_DAC"          },
    { LOG_V_REF_V_REF_RATE          , "V_REF_RATE"         },
    { LOG_V_REF_V_RATE_RMS          , "V_RATE_RMS"         },
    { LOG_V_REF_V_FEEDFWD           , "V_FEEDFWD"          },
    { LOG_V_REF_V_FF                , "V_FF"               },
    { LOG_V_REF_V_FF_FLTR           , "V_FF_FLTR"          },
    { LOG_V_REF_V_HARMONICS         , "V_HARMONICS"        },
    { LOG_V_REF_V_ERR               , "V_ERR"              },
    { LOG_V_REF_V_MAX_ABS_ERR       , "V_MAX_ABS_ERR"      },
    { LOG_V_REF_V_MEAS_SIM          , "V_MEAS_SIM"         },
    { LOG_V_REF_FG_REF              , "FG_REF"             },
    { LOG_V_REF_RT_REF              , "RT_REF"             },
    { LOG_V_REF_B_MEAS              , "B_MEAS"             },
    { LOG_V_REF_I_MEAS              , "I_MEAS"             },
    { 0                             , NULL                 },
}
#endif
;

// Log V_REG enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_v_reg[]
#ifdef GLOBALS
= {
    { LOG_V_REG_V_MEAS              , "V_MEAS"             },
    { LOG_V_REG_V_MEAS_REG          , "V_MEAS_REG"         },
    { LOG_V_REG_V_REG_ERR           , "V_REG_ERR"          },
    { LOG_V_REG_V_INTEGRATOR        , "V_INTEGRATOR"       },
    { LOG_V_REG_D_REF               , "D_REF"              },
    { LOG_V_REG_D_MEAS              , "D_MEAS"             },
    { LOG_V_REG_F_REF_REG           , "F_REF_REG"          },
    { LOG_V_REG_F_REF_LIMITED       , "F_REF_LIMITED"      },
    { LOG_V_REG_F_REF               , "F_REF"              },
    { LOG_V_REG_I_CAPA_REG          , "I_CAPA_REG"         },
    { LOG_V_REG_I_CAPA              , "I_CAPA"             },
    { LOG_V_REG_I_CAPA_MEAS         , "I_CAPA_MEAS"        },
    { LOG_V_REG_I_CAPA_SIM          , "I_CAPA_SIM"         },
    { 0                             , NULL                 },
}
#endif
;

// Log V_REG enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_v_reg_mpx[]
#ifdef GLOBALS
= {
    { LOG_V_REG_V_MEAS              , "V_MEAS"             },
    { LOG_V_REG_V_MEAS_REG          , "V_MEAS_REG"         },
    { LOG_V_REG_V_REG_ERR           , "V_REG_ERR"          },
    { LOG_V_REG_V_INTEGRATOR        , "V_INTEGRATOR"       },
    { LOG_V_REG_D_REF               , "D_REF"              },
    { LOG_V_REG_D_MEAS              , "D_MEAS"             },
    { LOG_V_REG_F_REF_REG           , "F_REF_REG"          },
    { LOG_V_REG_F_REF_LIMITED       , "F_REF_LIMITED"      },
    { LOG_V_REG_F_REF               , "F_REF"              },
    { LOG_V_REG_I_CAPA_REG          , "I_CAPA_REG"         },
    { LOG_V_REG_I_CAPA              , "I_CAPA"             },
    { LOG_V_REG_I_CAPA_MEAS         , "I_CAPA_MEAS"        },
    { LOG_V_REG_I_CAPA_SIM          , "I_CAPA_SIM"         },
    { 0                             , NULL                 },
}
#endif
;

// Log I_1KHZ enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_i_1khz[]
#ifdef GLOBALS
= {
    { LOG_I_1KHZ_I_REF              , "I_REF"              },
    { LOG_I_1KHZ_I_MEAS             , "I_MEAS"             },
    { 0                             , NULL                 },
}
#endif
;

// Log I_EARTH enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_i_earth[]
#ifdef GLOBALS
= {
    { LOG_I_EARTH_I_EARTH           , "I_EARTH"            },
    { 0                             , NULL                 },
}
#endif
;

// Log TEMP enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_temp[]
#ifdef GLOBALS
= {
    { LOG_TEMP_T_FGC_IN             , "T_FGC_IN"           },
    { LOG_TEMP_T_FGC_OUT            , "T_FGC_OUT"          },
    { LOG_TEMP_T_INTERNAL           , "T_INTERNAL"         },
    { LOG_TEMP_T_DCCT_A             , "T_DCCT_A"           },
    { LOG_TEMP_T_DCCT_B             , "T_DCCT_B"           },
    { LOG_TEMP_V_AC_FREQUENCY       , "V_AC_FREQUENCY"     },
    { 0                             , NULL                 },
}
#endif
;

// Log TEMP enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_v_ac_hz[]
#ifdef GLOBALS
= {
    { LOG_TEMP_T_FGC_IN             , "T_FGC_IN"           },
    { LOG_TEMP_T_FGC_OUT            , "T_FGC_OUT"          },
    { LOG_TEMP_T_INTERNAL           , "T_INTERNAL"         },
    { LOG_TEMP_T_DCCT_A             , "T_DCCT_A"           },
    { LOG_TEMP_T_DCCT_B             , "T_DCCT_B"           },
    { LOG_TEMP_V_AC_FREQUENCY       , "V_AC_FREQUENCY"     },
    { 0                             , NULL                 },
}
#endif
;

// Log BIS_LIMITS enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_bis_limits_ch1[]
#ifdef GLOBALS
= {
    { LOG_BIS_LIMITS_CH1_MAX_REG_ERR, "CH1_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH1_MIN_V_MEAS , "CH1_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_V_MEAS , "CH1_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_V_RATE , "CH1_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH1_MIN_I_MEAS , "CH1_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_I_MEAS , "CH1_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_I_RATE , "CH1_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH2_MAX_REG_ERR, "CH2_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH2_MIN_V_MEAS , "CH2_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_V_MEAS , "CH2_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_V_RATE , "CH2_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH2_MIN_I_MEAS , "CH2_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_I_MEAS , "CH2_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_I_RATE , "CH2_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH3_MAX_REG_ERR, "CH3_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH3_MIN_V_MEAS , "CH3_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_V_MEAS , "CH3_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_V_RATE , "CH3_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH3_MIN_I_MEAS , "CH3_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_I_MEAS , "CH3_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_I_RATE , "CH3_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH4_MAX_REG_ERR, "CH4_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH4_MIN_V_MEAS , "CH4_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_V_MEAS , "CH4_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_V_RATE , "CH4_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH4_MIN_I_MEAS , "CH4_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_I_MEAS , "CH4_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_I_RATE , "CH4_MAX_I_RATE"     },
    { 0                             , NULL                 },
}
#endif
;

// Log BIS_LIMITS enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_bis_limits_ch2[]
#ifdef GLOBALS
= {
    { LOG_BIS_LIMITS_CH1_MAX_REG_ERR, "CH1_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH1_MIN_V_MEAS , "CH1_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_V_MEAS , "CH1_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_V_RATE , "CH1_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH1_MIN_I_MEAS , "CH1_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_I_MEAS , "CH1_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_I_RATE , "CH1_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH2_MAX_REG_ERR, "CH2_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH2_MIN_V_MEAS , "CH2_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_V_MEAS , "CH2_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_V_RATE , "CH2_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH2_MIN_I_MEAS , "CH2_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_I_MEAS , "CH2_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_I_RATE , "CH2_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH3_MAX_REG_ERR, "CH3_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH3_MIN_V_MEAS , "CH3_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_V_MEAS , "CH3_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_V_RATE , "CH3_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH3_MIN_I_MEAS , "CH3_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_I_MEAS , "CH3_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_I_RATE , "CH3_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH4_MAX_REG_ERR, "CH4_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH4_MIN_V_MEAS , "CH4_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_V_MEAS , "CH4_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_V_RATE , "CH4_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH4_MIN_I_MEAS , "CH4_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_I_MEAS , "CH4_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_I_RATE , "CH4_MAX_I_RATE"     },
    { 0                             , NULL                 },
}
#endif
;

// Log BIS_LIMITS enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_bis_limits_ch3[]
#ifdef GLOBALS
= {
    { LOG_BIS_LIMITS_CH1_MAX_REG_ERR, "CH1_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH1_MIN_V_MEAS , "CH1_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_V_MEAS , "CH1_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_V_RATE , "CH1_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH1_MIN_I_MEAS , "CH1_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_I_MEAS , "CH1_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_I_RATE , "CH1_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH2_MAX_REG_ERR, "CH2_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH2_MIN_V_MEAS , "CH2_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_V_MEAS , "CH2_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_V_RATE , "CH2_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH2_MIN_I_MEAS , "CH2_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_I_MEAS , "CH2_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_I_RATE , "CH2_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH3_MAX_REG_ERR, "CH3_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH3_MIN_V_MEAS , "CH3_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_V_MEAS , "CH3_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_V_RATE , "CH3_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH3_MIN_I_MEAS , "CH3_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_I_MEAS , "CH3_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_I_RATE , "CH3_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH4_MAX_REG_ERR, "CH4_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH4_MIN_V_MEAS , "CH4_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_V_MEAS , "CH4_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_V_RATE , "CH4_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH4_MIN_I_MEAS , "CH4_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_I_MEAS , "CH4_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_I_RATE , "CH4_MAX_I_RATE"     },
    { 0                             , NULL                 },
}
#endif
;

// Log BIS_LIMITS enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_bis_limits_ch4[]
#ifdef GLOBALS
= {
    { LOG_BIS_LIMITS_CH1_MAX_REG_ERR, "CH1_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH1_MIN_V_MEAS , "CH1_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_V_MEAS , "CH1_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_V_RATE , "CH1_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH1_MIN_I_MEAS , "CH1_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_I_MEAS , "CH1_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH1_MAX_I_RATE , "CH1_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH2_MAX_REG_ERR, "CH2_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH2_MIN_V_MEAS , "CH2_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_V_MEAS , "CH2_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_V_RATE , "CH2_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH2_MIN_I_MEAS , "CH2_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_I_MEAS , "CH2_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH2_MAX_I_RATE , "CH2_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH3_MAX_REG_ERR, "CH3_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH3_MIN_V_MEAS , "CH3_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_V_MEAS , "CH3_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_V_RATE , "CH3_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH3_MIN_I_MEAS , "CH3_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_I_MEAS , "CH3_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH3_MAX_I_RATE , "CH3_MAX_I_RATE"     },
    { LOG_BIS_LIMITS_CH4_MAX_REG_ERR, "CH4_MAX_REG_ERR"    },
    { LOG_BIS_LIMITS_CH4_MIN_V_MEAS , "CH4_MIN_V_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_V_MEAS , "CH4_MAX_V_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_V_RATE , "CH4_MAX_V_RATE"     },
    { LOG_BIS_LIMITS_CH4_MIN_I_MEAS , "CH4_MIN_I_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_I_MEAS , "CH4_MAX_I_MEAS"     },
    { LOG_BIS_LIMITS_CH4_MAX_I_RATE , "CH4_MAX_I_RATE"     },
    { 0                             , NULL                 },
}
#endif
;

// Log ILC_CYC enum

CCPARS_LOG_EXT struct CC_pars_enum enum_log_ilc_cyc[]
#ifdef GLOBALS
= {
    { LOG_ILC_CYC_RMS_ERR_ILC       , "RMS_ERR_ILC"        },
    { LOG_ILC_CYC_INIT_RMS_ERR_ILC  , "INIT_RMS_ERR_ILC"   },
    { LOG_ILC_CYC_RMS_V_RATE        , "RMS_V_RATE"         },
    { LOG_ILC_CYC_RMS_V_RATE_LIM    , "RMS_V_RATE_LIM"     },
    { 0                             , NULL                 },
}
#endif
;

// EOF
