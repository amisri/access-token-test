#include "../common/misc.h"
#include "../common/uart.h"
#include "../common/misc.h"
#include "../common/cache.h"
#include "../common/ifc.h"
#include "../common/fgc32_fpga.h"
#include "../common/timer.h"

#include <string.h>

void ccmain(void);
void benchmark_ifc_access(void);

void clearCache()
{
    tlb_invalidate_all();
    iciallu();      /* instruction cache invalidate all */
//    flush_dcache_all();
}

static uint64_t Get_VBAR_EL2()
{
    uint64_t vbar_el2;
    __asm__ ("mrs %0, vbar_el2" : "=r" (vbar_el2));

    return vbar_el2;
}

static uint64_t Get_VBAR_EL3()
{
    uint64_t vbar_el3;
    __asm__ ("mrs %0, vbar_el3" : "=r" (vbar_el3));

    return vbar_el3;
}

__attribute__((noreturn))
void exception_handler(void) {
    uint64_t esr, elr, sp;

    uartSend("\r\n\r\n\r\n==== CPU EXCEPTION ====\r\n\r\n");

    if (getCurrentEl() == 3) {
        __asm__ ("mrs %0, esr_el3" : "=r" (esr));
        __asm__ ("mrs %0, elr_el3" : "=r" (elr));

        uartSend("ESR_EL3=  "); uartSendHex(esr); uartSend("\r\n");
        uartSend("ELR_EL3=  "); uartSendHex(elr);
    }
    else {
        __asm__ ("mrs %0, esr_el2" : "=r" (esr));
        __asm__ ("mrs %0, elr_el2" : "=r" (elr));

        uartSend("ESR_EL2=  "); uartSendHex(esr); uartSend("\r\n");
        uartSend("ELR_EL2=  "); uartSendHex(elr);
    }
    uartSend(" (crash location)\r\n");

    // https://developer.arm.com/documentation/ddi0595/2021-06/AArch64-Registers/ESR-EL1--Exception-Syndrome-Register--EL1-
    int Exception_Class = ((esr >> 26) & 0b111111);
    if (Exception_Class == 0b100101) { uartSend("EC: Data Abort taken without a change in Exception level\r\n"); }
    else { uartSend("Exception_Class="); uartSendHex(Exception_Class); uartSend("\r\n"); }

    uartSend("\r\n");

    __asm__ ("mov %0, sp" : "=r" (sp));

    uartSend("SP="); uartSendHex(sp); uartSend("\r\n");

    int cnt = -100;

    for (int i = 0; i < 24; i++) {
        uartSend("Stack at "); 
        uartSendHex(sp + i * 8);
        uartSend(" = ");
        uartSendHex(*(uint64_t*)(sp + i * 8));

        // use magic word to sync up
        if (*(uint64_t*)(sp + i * 8) == 0x1234567890abcdefULL) {
            cnt = 0;
        }

        if (cnt == 0) { uartSend(" (cookie)"); }
        if (cnt == 1) { uartSend(" (saved LR)"); }
        if (cnt == 2) { uartSend(" (saved x3)"); }
        if (cnt == 3) { uartSend(" (saved x2)"); }
        if (cnt == 4) { uartSend(" (saved x1)"); }
        if (cnt == 5) { uartSend(" (saved x0)"); }
        uartSend("\r\n");

        cnt++;
    }

    uartSend("\r\ndeal with it.\r\n\r\n");

    for (;;) {}
}

extern char Crash_Handler;
extern char Crash_Handler_End;

static void install_exception_handler(void* destination) {
    // if this crashes while the MMU is disabled, it's due to unaligned access inside memcpy

    memcpy(destination, &Crash_Handler, &Crash_Handler_End - &Crash_Handler);
}

static void get_ttbr_tcr_mair(uint64_t* table_out, uint64_t* tcr_out, uint64_t* attr_out)
{
    uint64_t table, tcr, attr;

	__asm__ ("mrs %0, ttbr0_el3" : "=r" (table));
	__asm__ ("mrs %0, tcr_el3" : "=r" (tcr));
	__asm__ ("mrs %0, mair_el3" : "=r" (attr));

    *table_out = table;
    *tcr_out = tcr;
    *attr_out = attr;
}

static void set_ttbr_tcr_mair(int el, uint64_t table, uint64_t tcr, uint64_t attr)
{
	__asm__ volatile("dsb sy");
	if (el == 1) {
		__asm__ volatile("msr ttbr0_el1, %0" : : "r" (table) : "memory");
		__asm__ volatile("msr tcr_el1, %0" : : "r" (tcr) : "memory");
		__asm__ volatile("msr mair_el1, %0" : : "r" (attr) : "memory");
	} else if (el == 2) {
		__asm__ volatile("msr ttbr0_el2, %0" : : "r" (table) : "memory");
		__asm__ volatile("msr tcr_el2, %0" : : "r" (tcr) : "memory");
		__asm__ volatile("msr mair_el2, %0" : : "r" (attr) : "memory");
	} else if (el == 3) {
		__asm__ volatile("msr ttbr0_el3, %0" : : "r" (table) : "memory");
		__asm__ volatile("msr tcr_el3, %0" : : "r" (tcr) : "memory");
		__asm__ volatile("msr mair_el3, %0" : : "r" (attr) : "memory");
	} else {
		// hang();
	}
	__asm__ volatile("isb");
}

void Enable_MMU_EL2(void);

int main()
{
    uint64_t sctlr;

    // clearCache();

    uartSend("\n\n\r\n");

    uartSend("EL=      "); uartSendHex(getCurrentEl()); uartSend("\r\n");
    uartSend("VBAR_EL2="); uartSendHex(Get_VBAR_EL2()); uartSend("\r\n");
    if (getCurrentEl() >= 3) {
        uartSend("VBAR_EL3="); uartSendHex(Get_VBAR_EL3()); uartSend("\r\n");
    }

    if (getCurrentEl() == 3) {
        __asm__ volatile ("mrs %0, sctlr_el3" : "=r" (sctlr));

        uartSend("SCTLR_EL3="); uartSendHex(sctlr); uartSend("\r\n");

        uint64_t table, tcr, attr;
        get_ttbr_tcr_mair(&table, &tcr, &attr);
        uartSend("TTBR0_EL3="); uartSendHex(table); uartSend("\r\n");
        uartSend("TCR_EL3=  "); uartSendHex(tcr); uartSend("\r\n");
        uartSend("MAIR_EL3= "); uartSendHex(attr); uartSend("\r\n");
    }
    else {
        // Necessary to enable MMU, otherwise unaligned access always fails
        // (https://github.com/zephyrproject-rtos/sdk-ng/issues/167#issuecomment-568955981)
        uartSend("El==2 -> enable MMU & cache\r\n");
        // TODO: these must not be hard-coded, they will probably change when U-Boot is recompiled
        set_ttbr_tcr_mair(2, 0x00000000fbe00000ULL,
                             0x0000000080823518ULL,
                             0x000000ff440c0400ULL);
        Enable_MMU_EL2();

        __asm__ volatile ("mrs %0, sctlr_el2" : "=r" (sctlr));

        uartSend("SCTLR_EL2="); uartSendHex(sctlr); uartSend("\r\n");
    }

    // install a custom exception handler, since the default one for some reason does not work properly outside of core 0

    uartSend("customize handler\r\n");

    // position 4
    install_exception_handler((void*)(0x00000000fbd2c000ULL + 4 * 128));

    // flush dcache to propagate changes
    dcache_clean_by_va((void*)0x00000000fbd2c000ULL, 16 * 128);
    // flush icache
    clearCache();

    // trigger exception on purpose
    // uartSend("call silly address\r\n");
    // ((void(*)(void))(0x12345678))();

//    printfInit();

//    myCacheDisable();

    uartSend("Bare-metal MAIN started v3: no myCacheDisable2 \n\r");
    printf("3 Bare-metal MAIN started on EL%d, built on %s\r\n", getCurrentEl(), __TIMESTAMP__);

    printf("PLLPGSR = %08X\n", *(volatile uint32_t*) 0x1EE1C00);

    ifc_init();
    fpga_timer_init();
    fpga_selftest();

    // Test FTM timer
    timerInit();
    timerStart();
    int before1 = timerRead();
    int64_t before2 = my_getnanosecs();
    my_usleep(1000);
    int after1 = timerRead();
    int64_t after2 = my_getnanosecs();
    printf("timeRead: before=%d after=%d -> delta=%d\n", before1, after1, after1 - before1);
    printf("my_timer: %ld\n", after2 - before2);
    timerStop();
//    for (;;) {}

    ccmain();
//    benchmark_ifc_access();

    // volatile uint8_t *a = (volatile uint8_t *)0xA0000000UL;

    while (1)
    {
        // *a = 'a';        delay(); uartSendChar('1');
        // *a = 'b';        delay(); uartSendChar('2');
        // *a = 'c';        delay(); uartSendChar('3');
        // *a = 'd';        delay(); uartSendChar('4');
        // *a = 'e';        delay(); uartSendChar('5');
        // *a = 'f';        delay(); uartSendChar('6');
        // *a = 'g';        delay(); uartSendChar('7');



//        uartSendChar('H');
//        shortDelay();
//        uartSendChar('A');
//        shortDelay();
    }
}