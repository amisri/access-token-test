#pragma once

#include "liblog.h"
#include "libref.h"
#include "libreg.h"
#include "libsig.h"

#include "logMenus.h"
#include "logStructs.h"
#include "sigStructs.h"

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CC_NUM_SUB_SELS             1                   // Max number of sub devices
#define CC_MAX_CYC_SEL              0                  // Max cycle selector
#define CC_NUM_CYC_SELS             1                  // CC_MAX_CYC_SEL + 1

//! MODE parameters structure

struct CC_pars_mode
{
    enum REF_pc_state           pc;                  //!< MODE PC - desired PC STATE
};

struct CC_sim_vars
{
//    bool                        cal_dac;                        //!< Indicates that DAC calibration is running
//
//    enum SIG_cal_level          cal_dac_level;                  //!< Current DAC calibration level
//    const uint32_t              cal_dac_raw[SIG_NUM_CALS];      //!< raw_dac values for all three DAC calibration levels
//
    struct CC_sim_pc_state
    {
        cc_float                timer;                          //!< Converter on/off timer (s)
    } pcstate;
//
//    struct CC_sim_supercycle
//    {
//        cc_double               prev_iter_time_s;               //!< Iter time from previous iteration
//        cc_double               warning_time_s;                 //!< Next start function event warning time
//        struct CC_us_time       next_cycle_time;                //!< Next start of cycle time
//        struct CC_us_time       next_event_time;                //!< Next start function event time
//
//        struct CC_sim_sc_event  next;                           //!< Next cycle event
//        struct CC_sim_sc_event  active;                         //!< Active cycle event (set after warning time)
//        struct CC_sim_sc_event  running;                        //!< Running cycle event (set after event time)
//        pthread_mutex_t         event_mutex;                    //!< Mutex to protect the active event
//    } supercycle;
//
//    struct CC_sim_circuit
//    {
//        cc_float                i_dcct_a;                       //!< Simulated circuit current being measured by DCCT A
//        cc_float                i_dcct_b;                       //!< Simulated circuit current being measured by DCCT B
//        cc_float                v_probe;                        //!< Simulated voltage being measured by the V_PROBE
//        cc_float                b_probe;                        //!< Simulated magnet field being measured by the Hall Probe
//        cc_float                i_probe;                        //!< Simulated current measurement from output filter
//        cc_float                v_dac;                          //!< Simulated DAC voltage
//        cc_float                v_adc[ADC_SIGNAL_NUM_SOURCES];  //!< Simulated voltages available to the ADCs
//        int32_t                 raw_adc  [SIG_NUM_ADCS];        //!< Simulated raw ADC values
//        cc_float                raw_adc_f[SIG_NUM_ADCS];        //!< Simulated raw ADC values as floats
//    } circuit;
//
//    //! DCCT simulation structures
//    //!
//    //! DCCT tc and dtc temp coefficients are shared with libsig, while the errors are
//    //! independent, to allow the DCCT calibration to discover their values.
//
//
//    struct CC_sim_error         dcct_err[DCCT_N_DCCTS];         //!< Simulated DCCT electronics errors
//
//    //! ADC simulation structures
//    //!
//    //! ADC tc and dtc temp coefficients are shared with libsig, while the nominal gain and errors are
//    //! independent, to allow the ADC calibration to discover their values.
//
//    struct CC_sim_adc
//    {
//        uint32_t                nominal_gain;                   //!< Simulated ADC nominal gain
//        struct CC_sim_error     err;                            //!< Simulated ADC errors
//    } adc[SIG_NUM_ADCS];
//
//    //! DAC simulation structure
//    //!
//    //! For a 20 bit DAC with v_nominal = 11V the range (raw) is [-524288, 524287]
//    //! With v_offset = 0.1 and calibration raw values 367000 (~70% of the max value)
//    //! and measured voltages +-8V we have:
//    //! - positive gain = 367000 raw / (8V - 0.1V) = 46455.67 raw/V
//    //! - negative gain = 367000 raw / (8V + 0.1V) = 45308.64 raw/V
//
//    struct CC_sim_dac
//    {
//        cc_float const          v_offset;                       //!< DAC voltage for zero calibration
//        cc_float const          pos_raw_per_volt;               //!< Gain (raw/V) for positive voltages
//        cc_float const          neg_raw_per_volt;               //!< Gain (raw/V) for negative voltages
//        uint32_t const          resolution;                     //!< DAC resolution
//        cc_float const          v_nominal;                      //!< Nominal voltage
//    } dac;
//
//    struct REG_mgr_sim_vars     sim_vars;                       //!< Simulation variables
//
//    //! Decoupling simulation variables
//
//    struct REG_deco_shared volatile deco;                       //!< Shared deco structure
};

struct CC_pars_log
{
    cc_double                time_origin;         //!< Log time origin (zero for most recent data)
    uint32_t                 cyc_sel;             //!< Cycle selector to log automatically
    uint32_t                 sub_sampling_period; //!< Sub-sampling period in units of the log period (0 = automatic, 1-N = required sub-sampling period)
    struct LOG_read_timing   read_timing;         //!< Read request timing
//    enum CC_enabled_disabled prev_cycle;          //!< Enabled:Log previous cycle.  Disabled:use LOG CYC_SEL to set cycle to be logged.
//    enum CC_enabled_disabled overwrite;           //!< Enabled:each log is to a separate folder. Disabled:all logs written to top level log folder.
    enum CC_enabled_disabled csv_spy;             //!< Enables/disables creation of CSV Spy file on each writelog
    enum CC_enabled_disabled binary_spy;          //!< Enables/disables creation of binary Spy file on each writelog
//    enum CC_enabled_disabled pm_buf;              //!< Enables/disables PM_BUF generation when converter goes OFF
//    enum CC_enabled_disabled internal_tests;      //!< Enables/disables internal tests of log read control and header or PM_BUF
};


typedef void (*OpaqueFunc_t)(void);
// must return a new StateFunc pointer or NULL
typedef OpaqueFunc_t (*StateFunc_t)(void);

struct CCRT2C_script
{
    int iter_period_us;
    int event_log_period_iters;

    StateFunc_t (*init)(void);
};

extern struct LOG_buffers log_buffers;
extern struct REF_mgr ref_mgr;
extern struct REG_mgr reg_mgr;          // referred to by logs
extern struct REG_pars reg_pars;
extern struct SIG_struct sig_struct;

extern struct CC_pars_log ccpars_log;
extern enum CC_enabled_disabled ccpars_logmenu[LOG_NUM_MENUS];
extern struct CC_pars_mode ccpars_mode;
extern struct CC_sim_vars ccsim;

extern struct REF_ref_pars ccpars_ref[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS];

bool cctestTimeInStateElapsed(double time_sec);
uint32_t ccCmdsRun(void);
void ccRtArmRef(uint32_t sub_sel, uint32_t cyc_sel, uint32_t const num_pars, cc_float const * const par_values);

// defined in generated code
[[maybe_unused]] extern const struct CCRT2C_script script_direct;
[[maybe_unused]] extern const struct CCRT2C_script script_idle;

#ifdef __cplusplus
}
#endif
