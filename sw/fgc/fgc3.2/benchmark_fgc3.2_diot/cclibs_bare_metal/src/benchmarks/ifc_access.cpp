#include "../utility/benchmark.hpp"

extern "C" void benchmark_ifc_access_loop(void);

extern "C" void benchmark_ifc_access() {
    int const kNumIterations = 1'000'000;
//    auto* const p_fpga_counter_config = (uint32_t volatile*)(0x7fb00000 + 0x1b70);

    printf("benchmark_ifc_access\n\n");

    Benchmark perf;
    perf.start();

//    for (int i = 0; i < kNumIterations; i++) {
//        auto x = *p_fpga_counter_config;
//        *p_fpga_counter_config = x;
//    }

    benchmark_ifc_access_loop();


    perf.end();
//    perf.debug();
    perf.report(kNumIterations);
}
