//
// Created by mcejp on 19/05/2022.
//

#ifndef CCLIBS_DEMO_INC_LOG_HPP
#define CCLIBS_DEMO_INC_LOG_HPP

#define LOG_OUTPUT_BLK_LEN          374

static uint32_t      local_log_buffer[LOG_MAX_LOG_LEN + LOG_HEADER_LEN];

#ifndef _FGC32
static LOG_read_request log_read_request[LOG_NUM_LIBLOG_MENUS];
static LOG_read_control log_read_control[LOG_NUM_LIBLOG_MENUS];
#endif

void ccLogGetOutput(struct LOG_read_control * const read_control)
{
    uint32_t * blk_buf_ptr = local_log_buffer;
    uint32_t   values_returned = 1;

    while(logReadOutputFinished(read_control) == false && values_returned > 0)
    {
        values_returned = logOutput(read_control, log_read.buffers, blk_buf_ptr, LOG_OUTPUT_BLK_LEN);
        blk_buf_ptr    += values_returned;
    }

//    ccLogPrintTestTap(values_returned > 0,"logOutput","number of return values","is ZERO\n");
}

static void ccRunLogSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary, double iter_time_s)
{
    // Check if new cycle has started (iter time is >= start_cycle_time) - order of conditions matters!

//    if(ccrun.cycle_running == false              &&
//       ccsim.supercycle.active.event.cyc_sel > 0 &&
//       ccrun.iter_time_s >= ccsim.supercycle.active.event_time_s)
//    {
//        ccrun.cycle_running    = true;
//        ccrun.acq_disc_counter = -100;  // iterations till start of discontinuous logging
//
//        // Inform liblog about new cycle
//
//        logStoreStartContinuousRT(log_read.log, ccrun.iter_time_s, ccsim.supercycle.active.event.cyc_sel);
//    }

    // Log continuous and discontinuous logs

    ccRunLogContinuousSignals(iter_1s_boundary, iter_200ms_boundary, iter_time_s);
//    ccRunLogDiscontinuousSignals();
//    ccRunLogDimSignals();

    // Write PMD file if needed

//    if(logPostmortemLogsFroze(&log_mgr))
//    {
//        ccLogWakeThread(LOG_READ_GET_PM_BUF, NULL);
//    }
}

static void ccRunLogContinuousSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary, double iter_time_s)
{
    // log_debug_sig can be assigned to anything of interest to assist in debugging

//    log_debug_sig = (cc_float)ref_mgr.fsm.off.init_rst_timer;

    // Save these signals as floats for logging purposes
    // Scale function type to better match other debugging signals
    // Negate the values for improved readability when WAIT_CMDS signal is visible

//    ccrun.log.debug.reg_mode        =  (cc_float)regMgrVarGetValue(&reg_mgr,REG_MODE )          / 3.0 - 6.0;                      // 0 ... 3  ->  -6.0  ... -5.0
//    ccrun.log.debug.ref_state       =  (cc_float)refMgrVarGetValue(&ref_mgr,REF_STATE)          * 0.1;                            // 0 ... 14 ->   0.0  ...  1.4
//    ccrun.log.debug.cyc_sel         =  (cc_float)refMgrVarGetValue(&ref_mgr,EVENT_CYC_SEL)      * 0.1;                            // 0 ... 32 ->   0.0  ...  3.2
//    ccrun.log.debug.fg_type         = -(cc_float)refMgrVarGetValue(&ref_mgr,REF_FG_TYPE)        * 0.1;                            // 0 ... 13 ->   0.0  ... -1.3
//    ccrun.log.debug.fg_status       = -(cc_float)refMgrVarGetValue(&ref_mgr,REF_FG_STATUS)      / 4.0 - 2.0;                      // 0 ... 3  ->  -2.0  ... -2.75
//    ccrun.log.debug.ramp_mgr_status = -(cc_float)refMgrVarGetValue(&ref_mgr,REF_RAMP_MGR)       / 3.0 - 3.0;                      // 0 ... 2  ->  -3.0  ... -3.66
//    ccrun.log.debug.cycle_status    = -(cc_float)refMgrVarGetValue(&ref_mgr,REF_CYCLING_STATUS) / 3.0 - 4.0;                      // 0 ... 2  ->  -4.0  ... -4.66
//    ccrun.log.debug.dcct_user_sel   = -(cc_float)sigVarGetValue(&sig_struct, select, i_meas, SELECT_SELECTOR)        / 3.0 - 3.0; // 0 ... 2  ->  -3.0  ... -3.66
//    ccrun.log.debug.dcct_act_sel    = -(cc_float)sigVarGetValue(&sig_struct, select, i_meas, SELECT_ACTUAL_SELECTOR) / 3.0 - 4.0; // 0 ... 3  ->  -4.0  ... -5.00
//    ccrun.log.debug.iter_time_us    =  (cc_float)ccrun.iter_time.us * 1.0E-6;
//    ccrun.log.debug.iter_index_i    =  (cc_float)regMgrVarGetValue(&reg_mgr,IREG_ITER_INDEX);
//    ccrun.log.debug.iter_index_b    =  (cc_float)regMgrVarGetValue(&reg_mgr,BREG_ITER_INDEX);

    // Use mutex to log and reset the event type atomically

//    pthread_mutex_lock(&ccrun.log.event_type_mutex);

//    ccrun.log.debug.event_type = (cc_float)ccrun.log.event_type;
//    ccrun.log.event_type       = REF_EVENT_NONE;
//
//    pthread_mutex_unlock(&ccrun.log.event_type_mutex);

    struct LOG_log * const log = log_structs.log;

    // Store the following logs on every iteration

    logStoreContinuousRT(&log[LOG_ACQ],        iter_time_s);
//    logStoreContinuousRT(&log[LOG_DEBUG],      ccrun.iter_time_s);
    logStoreContinuousRT(&log[LOG_B_MEAS],     iter_time_s);
    logStoreContinuousRT(&log[LOG_I_MEAS],     iter_time_s);
    logStoreContinuousRT(&log[LOG_V_REF],      iter_time_s);
    logStoreContinuousRT(&log[LOG_V_REG],      iter_time_s);
//    logStoreContinuousRT(&log[LOG_FAULTS],     ccrun.iter_time_s);
//    logStoreContinuousRT(&log[LOG_WARNINGS],   ccrun.iter_time_s);
    logStoreContinuousRT(&log[LOG_FLAGS],      iter_time_s);
//    logStoreContinuousRT(&log[LOG_LOGRUNNING], ccrun.iter_time_s);

//    ccRunTestLogStateMachine(&log[LOG_ACQ]    );
//    ccRunTestLogStateMachine(&log[LOG_FLAGS]  );

    // Store ACQ_FLTR every 10th iteration

//    if((ccrun.iteration_counter % 10) == 0)
//    {
//        logStoreContinuousRT(&log[LOG_ACQ_FLTR], ccrun.iter_time_s);
//        ccRunTestLogStateMachine(&log[LOG_ACQ_FLTR]);
//    }

    // Store field regulation signals in log at field regulation rate

//    if(regMgrVarGetValue(&reg_mgr, BREG_ITER_INDEX) == 0)
//    {
//        // Log field regulation
//
//        logStoreContinuousRT(&log[LOG_B_REG], iter_time_s);
//
//        if(reg_mgr.reg_mode == REG_FIELD)
//        {
//            log_mgr.period_us[LOG_ILC_FLAGS] = log_mgr.period_us[LOG_B_REG];
//
//            logStoreContinuousRT(&log[LOG_ILC_FLAGS], iter_time_s);
//        }
//
//        ccRunTestLogStateMachine(&log[LOG_B_REG]);
//    }

    // Store current regulation signals in log at current regulation rate

    if(regMgrVarGetValue(&reg_mgr, IREG_ITER_INDEX) == 0)
    {
        // Log current regulation

        logStoreContinuousRT(&log[LOG_I_REG], iter_time_s);

        if(reg_mgr.reg_mode == REG_CURRENT)
        {
//            log_mgr.period_us[LOG_ILC_FLAGS] = log_mgr.period_us[LOG_I_REG];
//
//            logStoreContinuousRT(&log[LOG_ILC_FLAGS], iter_time_s);
        }

//        ccRunTestLogStateMachine(&log[LOG_I_REG]);
    }

    // Store ILC cyclic RMS signals when requested by refIlcStateToCycling

//    cc_double const ilc_log_time_stamp_s = refMgrVarGetValue(&ref_mgr, ILC_LOG_TIME_STAMP_S);
//
//    if(ilc_log_time_stamp_s != 0.0)
//    {
//        logStoreContinuousRT(&log[LOG_ILC_CYC], ilc_log_time_stamp_s);
//
//        refMgrVarGetValue(&ref_mgr, ILC_LOG_TIME_STAMP_S) = 0.0;
//    }

    // Store temperatures when the measurements get ready in the event log thread (~200ms intervals)
    // The time stamp should match the 200ms time stamp.

//    if(ccrun.log.temp_meas_ready)
//    {
//        logStoreContinuousRT(&log[LOG_TEMP], iter_200ms_timestamp_s);
//
//        ccRunTestLogStateMachine(&log[LOG_TEMP]);
//
//        ccrun.log.temp_meas_ready = false;
//
//        // Store the limits in the limits log at 5Hz
//
//        logStoreContinuousRT(&log[LOG_LIMITS], ccrun.iter_200ms_timestamp_s);
//
//        ccRunTestLogStateMachine(&log[LOG_LIMITS]);
//
//        // Store the V_AC frequency measurement at 5Hz
//
//        logStoreContinuousRT(&log[LOG_V_AC_HZ], iter_200ms_timestamp_s);
//
//        ccRunTestLogStateMachine(&log[LOG_V_AC_HZ]);
//    }

}


#ifndef __aarch64__
static void ccLogWriteSpyCsv(FILE                        * const f,
                             struct LOG_spy_header const * const header,
                             char                  const * const buf_name,
                             char                  const * const device_name)
{
    struct SPY_v2_buf_header const * const buf_header          = &header->buf_header;
    cc_double                        const first_sample_time_s = ccUsTimeToDouble((struct CC_us_time *)&buf_header->timestamp_s);
    cc_double                        const period              = (cc_double)buf_header->period_us * 1.0E-6;
    uint32_t                         const num_samples         = buf_header->num_samples;
    uint32_t                         const num_signals         = buf_header->num_signals;
    bool                             const log_is_digital      = (buf_header->meta & SPY_BUF_META_ANA_SIGNALS) == 0;

    // Print CSV header line

    fprintf(f, "source:ccrt device:%s", device_name);
    fprintf(f, " timeOrigin:%.6f",      ccUsTimeToDouble((struct CC_us_time *)&buf_header->time_origin_s));
    fprintf(f, " firstSampleTime:%.6f", first_sample_time_s);
    fprintf(f, " period:%.6f",          period);

    // Write buffer name and add a suffice for cyc_sel or log_dir_idx (if >1)

    fprintf(f, " name:%s", buf_name);

    if(ccpars_log.cyc_sel > 0)
    {
        fprintf(f, "(%u)", ccpars_log.cyc_sel);
    }
//    else if(ccfile.log_dir_idx > 1)
//    {
//        fprintf(f, "[%u]", ccfile.log_dir_idx);
//    }

    if(log_is_digital)
    {
        fputs(" type:digital",f);
    }
    else
    {
        fputs(" type:analog",f);
    }

    struct SPY_v2_sig_header const * const sig_header = header->sig_headers;

    for(uint32_t signal_index = 0; signal_index < num_signals; signal_index++)
    {
        // Print signal name

        fprintf(f, ",%s",  sig_header[signal_index].name);

        // Print meta data tags and time offset (if non-zero)

        if((sig_header[signal_index].meta & SPY_SIG_META_STEPS) != 0)
        {
            fputs(" STEP",f);
        }

        if(sig_header[signal_index].time_offset_us != 0)
        {
            fprintf(f, " %+.6f", (cc_double)sig_header[signal_index].time_offset_us * 1.0E-6);
        }
    }

    fputc('\n',f);

    // Print CSV signal records

    for(uint32_t sample_index = 0; sample_index < num_samples; sample_index++)
    {
        // Print time stamp

        fprintf(f, "%.6f", first_sample_time_s + (cc_double)sample_index * period);

        // Print signal values

        if(log_is_digital)
        {
            uint32_t const dig_mask = local_log_buffer[sample_index];

            for(uint32_t signal_index = 0; signal_index < num_signals; signal_index++)
            {
                fputc(',', f);
                fputc( dig_mask & (1 << sig_header[signal_index].dig_bit_index) ? '1' : '0', f);
            }
        }
        else
        {
            for(uint32_t signal_index = 0; signal_index < num_signals; signal_index++)
            {
                cc_float const * const float_buf = (cc_float *)&local_log_buffer[signal_index * num_samples + sample_index];

                fprintf(f, ",%.8g", *float_buf);
            }
        }

        fputc('\n',f);
    }
}


static void ccLogWriteSpyBin(FILE                          * const f,
                             struct LOG_spy_header   const * const header,
                             struct LOG_read_control const * const read_control)
{
    size_t written = fwrite(header, sizeof(uint8_t), read_control->header_size, f);

    if(written != read_control->header_size)
    {
        ccRtPrintError("Number of bytes written to log file (%zu) doesn't correspond to header size (%d)", written, read_control->header_size);
    }

    written = fwrite(local_log_buffer, sizeof(uint8_t), read_control->data_size, f);

    if(written != read_control->data_size)
    {
        ccRtPrintError("Number of bytes written to log file (%zu) doesn't correspond to data size (%d)", written, read_control->data_size);
    }
}


FILE * ccFileOpen(char const * const filename, char const * const mode)
{
    // Open temporary file for writing

    FILE *f = fopen(filename, mode);

    if(f == NULL)
    {
        ccRtPrintErrorAndExit("Opening file '%s' : %s (%d)", filename, strerror(errno), errno);
    }

    return f;
}
#define CC_PATH_LEN                 256

void ccFilePrintPath(char * const path, char const * fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    if(vsnprintf(path, CC_PATH_LEN, fmt, args) >= CC_PATH_LEN)
    {
        ccRtPrintErrorAndExit("Path too long: %s", path);
    }

    va_end(args);
}

#include <sys/stat.h>

static inline bool ccFileExists(char const * const filename)
{
    struct stat buffer;

    return stat(filename, &buffer) == 0;
}

void ccFileCloseAndReplace(FILE       * const temp_file,
                           char const * const temp_filename,
                           char const * const permanent_filename)
{
    if(temp_file != NULL && ccFileExists(temp_filename))
    {
        if(fclose(temp_file) != 0)
        {
            ccRtPrintErrorAndExit("Closing file '%s' : %s (%d)", temp_filename, strerror(errno), errno);
        }

        // Rename temporary file to replace permanent file

#ifdef _WIN32
        unlink(permanent_filename);     // necessary on Windows
#endif

        if(rename(temp_filename, permanent_filename) != 0)
        {
            ccRtPrintErrorAndExit("Failed to rename '%s' to '%s' : %s (%d)", temp_filename, permanent_filename, strerror(errno), errno);
        }

//        ccRtPrintInVerbose("Renamed '%s' to '%s'", temp_filename, permanent_filename);
    }
}


#define CC_TMP_LOG_FILE             "tmp_spy_buf"

static void ccLogWriteSpyFile(char const * const format, char const * const dir_name, uint32_t const menu_index)
{
    // Create new file with binary or ASCII type

    FILE * const f = ccFileOpen(CC_TMP_LOG_FILE, *format == 'b' ? "wb" : "w");

    fprintf(stderr, "Dumping SPY %s log for %s\n", format, log_menus.names[menu_index]);

    if(*format == 'b')
    {
        ccLogWriteSpyBin(f,&log_read.header.spy, &log_read_control[menu_index]);
    }
    else
    {
        ccLogWriteSpyCsv(f, &log_read.header.spy, log_menus.names[menu_index], dir_name);
    }

    char filename[CC_PATH_LEN];

    ccFilePrintPath(filename, "%s.%s",
            log_menus.names[menu_index],
            format);

    ccFileCloseAndReplace(f, CC_TMP_LOG_FILE, filename);
}
#endif

static size_t writeLogsToMemory(uintptr_t addr,
                              struct LOG_spy_header   const * const header,
                              struct LOG_read_control const * const read_control)
{
    memcpy((void*)(addr), header, read_control->header_size);
    memcpy((void*)(addr + read_control->header_size), local_log_buffer, read_control->data_size);
    return read_control->header_size + read_control->data_size;
}

#define __LITTLE_ENDIAN__
#include "picohash.h"

#ifndef _FGC32
void ccLogWriteSpy(void)
{
    bool  write_file = true;
//    char  dir_name[CC_PATH_LEN];

    for(int menu_index = 0; menu_index < LOG_NUM_LIBLOG_MENUS; menu_index++)
    {


        struct LOG_read_request * const read_request = &log_read_request[menu_index];
        struct LOG_read_control * const read_control = &log_read_control[menu_index];

//        fprintf(stderr, "%-15s -> %d\n", log_menus.names[menu_index], ccpars_logmenu[menu_index]);

        if(ccpars_logmenu[menu_index] == CC_ENABLED)
        {
            // Log menu is ENABLED - prepare and run a read request for the log menu

            ccDoubleToUsTime(ccpars_log.time_origin, &ccpars_log.read_timing.time_origin);

            // Set previous first sample time from previous read_control for this menu index, if previous acquisition was for SPY

            if(read_request->action == LOG_READ_GET_SPY)
            {
                ccpars_log.read_timing.prev_first_sample_time = read_control->first_sample_time;
            }
            else
            {
                ccpars_log.read_timing.prev_first_sample_time.s = 0;
            }

            // Prepare the read_request structure

            logPrepareReadMenuRequest(&log_menus,
                                      menu_index,
                                      LOG_READ_GET_SPY,
                                      0, //ccrun.log.cyc_sel,
                                      &ccpars_log.read_timing,
                                      ccpars_log.sub_sampling_period,
                                      read_request);

            // Copy read_request into log_structs. This simulates the transfer of the structure between CPUs on a multi-CPU platform.

            memcpy(&log_read.request, read_request, sizeof(struct LOG_read_request));

            // Process the read request to generate the read control and header

            logReadRequest(&log_read, read_control);

            logReadControlPrint(read_control);

            // Check the read_control status

            if(read_control->status == LOG_READ_CONTROL_SUCCESS)
            {
                // Read request successful

                ccLogGetOutput(read_control);

                // Set LOGSPY parameters from SPY header

//                ccLogSpySetParsFromHeader(&log_read.header.spy);

                // Test read_control

                if(write_file)
                {
                    // Rename DIM signals

//                    if(menu_index >= LOG_MENU_CODIM)
//                    {
//                        ccLogRenameDimSignals(menu_index);
//                    }

                    // Write CSV and/or binary spy files as required

#ifndef __aarch64__
                    if(ccpars_log.csv_spy == CC_ENABLED)
                    {
                        ccLogWriteSpyFile("csv", "", menu_index);
                    }

                    if(ccpars_log.binary_spy == CC_ENABLED)
                    {
                        ccLogWriteSpyFile("bin", "", menu_index);
                    }
#else
                    if (menu_index == LOG_MENU_ACQ_MPX || menu_index == LOG_MENU_I_REG) {
                        uintptr_t addr = 0x6000'0000;
                        size_t written = writeLogsToMemory(addr, &log_read.header.spy,
                                                           &log_read_control[menu_index]);
                        if (menu_index == LOG_MENU_ACQ_MPX)
                            printf(R"(LOG_INFO {"name": "ACQ_MPX", "addr": %lu, "len": %lu, "md5sum": ")", addr, written);
                        else
                            printf(R"(LOG_INFO {"name": "I_REG", "addr": %lu, "len": %lu, "md5sum": ")", addr, written);
                        __asm__ volatile("dsb sy");     // ensure data flushed to DDR

                        _picohash_md5_ctx_t ctx;
                        _picohash_md5_init(&ctx);
                        _picohash_md5_update(&ctx, (const void *) addr, written);
                        uint8_t digest[PICOHASH_MD5_DIGEST_LENGTH];
                        _picohash_md5_final(&ctx, &digest[0]);
                        for (auto ch : digest) {
                            printf("%02x", ch);
                        }
                        printf("\"}\n");

                        if (menu_index == LOG_MENU_ACQ_MPX)
                            printf("\n<<ACQ_MPX<");
                        else
                            printf("\n<<I_REG<");
                        fwrite((void*)addr, 1, written, stdout);
                        if (menu_index == LOG_MENU_ACQ_MPX)
                            printf(">>ACQ_MPX>\n");
                        else
                            printf(">>I_REG>\n");
                    }
#endif
                }
            }
            else
            {
                // Read request failed - reset LOGSPY parameters

//                memset(&ccpars_logspy, 0 , sizeof(ccpars_logspy));
                fprintf(stderr, "Read requested but FAILED: %s\n", log_menus.names[menu_index]);
            }
        }
        else
        {
            // Menu index is DISABLED - reset the read_request and read_control structures for this menu

            memset(read_request, 0, sizeof(struct LOG_read_request));
            memset(read_control, 0, sizeof(struct LOG_read_control));
        }
    }
}
#endif

#endif //CCLIBS_DEMO_INC_LOG_HPP
