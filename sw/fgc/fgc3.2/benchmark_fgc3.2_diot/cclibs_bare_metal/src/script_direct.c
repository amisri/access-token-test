// GENERATED AUTOMATICALLY FROM tests/direct/direct.cct
// DO NOT EDIT BY HAND

#include "cctest_runtime.h"

static StateFunc_t WAIT_5_0_None_None_None_None(void);
static StateFunc_t WAIT_4_0_None_None_None_None(void);
static StateFunc_t WAIT_0_8_None_None_None_None(void);
static StateFunc_t WAIT_1_0_None_None_None_None(void);

static StateFunc_t INIT(void)
{
    // scripts/config.cct:3
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_PERIOD_ITERS             ))[0] = 10;
    (regMgrParAppValue( &reg_pars, BREG_PERIOD_ITERS             ))[1] = 10;
    (regMgrParAppValue( &reg_pars, BREG_PERIOD_ITERS             ))[2] = 10;
    (regMgrParAppValue( &reg_pars, BREG_PERIOD_ITERS             ))[3] = 10;
    // scripts/config.cct:4
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXTERNAL_ALG             ))[0] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, BREG_EXTERNAL_ALG             ))[1] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, BREG_EXTERNAL_ALG             ))[2] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, BREG_EXTERNAL_ALG             ))[3] = CC_DISABLED;
    // scripts/config.cct:5
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_MEAS_SELECT          ))[0] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_INT_MEAS_SELECT          ))[1] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_INT_MEAS_SELECT          ))[2] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_INT_MEAS_SELECT          ))[3] = REG_MEAS_EXTRAPOLATED;
    // scripts/config.cct:6
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_PURE_DELAY_PERIODS   ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_PURE_DELAY_PERIODS   ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_PURE_DELAY_PERIODS   ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_PURE_DELAY_PERIODS   ))[3] = 0.000000E+00;
    // scripts/config.cct:7
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE1_HZ          ))[0] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE1_HZ          ))[1] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE1_HZ          ))[2] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE1_HZ          ))[3] = 1.000000E+01;
    // scripts/config.cct:8
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_HZ         ))[0] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_HZ         ))[1] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_HZ         ))[2] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_HZ         ))[3] = 1.000000E+01;
    // scripts/config.cct:9
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_Z          ))[0] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_Z          ))[1] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_Z          ))[2] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLES2_Z          ))[3] = 5.000000E-01;
    // scripts/config.cct:10
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE4_HZ          ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE4_HZ          ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE4_HZ          ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE4_HZ          ))[3] = 0.000000E+00;
    // scripts/config.cct:11
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE5_HZ          ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE5_HZ          ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE5_HZ          ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_INT_AUXPOLE5_HZ          ))[3] = 0.000000E+00;
    // scripts/config.cct:12
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_MEAS_SELECT          ))[0] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_EXT_MEAS_SELECT          ))[1] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_EXT_MEAS_SELECT          ))[2] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, BREG_EXT_MEAS_SELECT          ))[3] = REG_MEAS_EXTRAPOLATED;
    // scripts/config.cct:13
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_TRACK_DELAY_PERIODS  ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_TRACK_DELAY_PERIODS  ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_TRACK_DELAY_PERIODS  ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_TRACK_DELAY_PERIODS  ))[3] = 0.000000E+00;
    // scripts/config.cct:14
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_R                 ))[15] = 0.000000E+00;
    // scripts/config.cct:15
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_S                 ))[15] = 0.000000E+00;
    // scripts/config.cct:16
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, BREG_EXT_OP_T                 ))[15] = 0.000000E+00;
    // scripts/config.cct:17
    // (skipped write to &ccpars_cal.dcct_primary_turns)
    // scripts/config.cct:18
    // (skipped write to &ccpars_cal.num_samples)
    // scripts/config.cct:19
    *(sigVarGetPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_POSITION           )) = SIG_TRANSDUCER_POSITION_CONVERTER;
    // scripts/config.cct:20
    *(sigVarGetPointer(&sig_struct, transducer, dcct_a  , TRANSDUCER_NOMINAL_GAIN       )) = 1.000000E+01;
    // scripts/config.cct:21
    *(sigVarGetPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_POSITION           )) = SIG_TRANSDUCER_POSITION_LOAD;
    // scripts/config.cct:22
    *(sigVarGetPointer(&sig_struct, transducer, dcct_b  , TRANSDUCER_NOMINAL_GAIN       )) = 1.000000E+01;
    // scripts/config.cct:23
    *(sigVarGetPointer(&sig_struct, transducer, v_probe , TRANSDUCER_NOMINAL_GAIN       )) = 1.000000E+01;
    // scripts/config.cct:24
    *(sigVarGetPointer(&sig_struct, transducer, b_probe , TRANSDUCER_NOMINAL_GAIN       )) = 1.000000E+00;
    // scripts/config.cct:25
    *(sigVarGetPointer(&sig_struct, transducer, i_probe , TRANSDUCER_NOMINAL_GAIN       )) = 1.000000E+00;
    // scripts/config.cct:26
    *(regMgrParAppPointer(&reg_pars, DECO_PHASE)) = 0;
    // scripts/config.cct:27
    *(regMgrParAppPointer(&reg_pars, DECO_INDEX)) = 0;
    // scripts/config.cct:28
    _Static_assert(4 <= (REG_NUM_DECO), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, DECO_K    ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_K    ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_K    ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_K    ))[3] = 0.000000E+00;
    // scripts/config.cct:29
    _Static_assert(4 <= (REG_NUM_DECO), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, DECO_D    ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_D    ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_D    ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, DECO_D    ))[3] = 0.000000E+00;
    // scripts/config.cct:30
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_ACCELERATION  ))[0] = 1.000000E+02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_ACCELERATION  ))[1] = 1.010000E+02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_ACCELERATION  ))[2] = 1.020000E+02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_ACCELERATION  ))[3] = 1.030000E+02;
    // scripts/config.cct:31
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_DECELERATION  ))[0] = 7.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_DECELERATION  ))[1] = 7.100000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_DECELERATION  ))[2] = 7.200000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_DECELERATION  ))[3] = 7.300000E+01;
    // scripts/config.cct:32
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_LINEAR_RATE   ))[0] = 9.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_LINEAR_RATE   ))[1] = 9.100000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_LINEAR_RATE   ))[2] = 9.200000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_LINEAR_RATE   ))[3] = 9.300000E+01;
    // scripts/config.cct:33
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MAX  ))[0] = 1.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MAX  ))[1] = 1.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MAX  ))[2] = 1.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MAX  ))[3] = 1.000000E+01;
    // scripts/config.cct:34
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MIN  ))[0] = -1.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MIN  ))[1] = 0.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MIN  ))[2] = 0.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_PRE_FUNC_MIN  ))[3] = -1.000000E+01;
    // scripts/config.cct:35
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_MINRMS        ))[0] = 0.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_MINRMS        ))[1] = 0.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_MINRMS        ))[2] = 0.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_V_MINRMS        ))[3] = 0.000000E+00;
    // scripts/config.cct:36
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_ACCELERATION  ))[0] = 1.003000E+02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_ACCELERATION  ))[1] = 1.013000E+02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_ACCELERATION  ))[2] = 1.023000E+02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_ACCELERATION  ))[3] = 1.033000E+02;
    // scripts/config.cct:37
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_DECELERATION  ))[0] = 7.030000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_DECELERATION  ))[1] = 7.130000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_DECELERATION  ))[2] = 7.230000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_DECELERATION  ))[3] = 7.330000E+01;
    // scripts/config.cct:38
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_LINEAR_RATE   ))[0] = 9.030000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_LINEAR_RATE   ))[1] = 9.130000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_LINEAR_RATE   ))[2] = 9.230000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_LINEAR_RATE   ))[3] = 9.330000E+01;
    // scripts/config.cct:39
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MAX  ))[0] = 9.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MAX  ))[1] = 9.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MAX  ))[2] = 9.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MAX  ))[3] = 9.000000E+00;
    // scripts/config.cct:40
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MIN  ))[0] = -9.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MIN  ))[1] = 0.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MIN  ))[2] = 3.000000E-01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_PRE_FUNC_MIN  ))[3] = -9.000000E+00;
    // scripts/config.cct:41
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_MINRMS        ))[0] = 0.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_MINRMS        ))[1] = 1.000000E-01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_MINRMS        ))[2] = 1.000000E-01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_I_MINRMS        ))[3] = 0.000000E+00;
    // scripts/config.cct:42
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_ACCELERATION  ))[0] = 1.006000E+02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_ACCELERATION  ))[1] = 1.016000E+02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_ACCELERATION  ))[2] = 1.026000E+02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_ACCELERATION  ))[3] = 1.036000E+02;
    // scripts/config.cct:43
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_DECELERATION  ))[0] = 7.060000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_DECELERATION  ))[1] = 7.160000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_DECELERATION  ))[2] = 7.260000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_DECELERATION  ))[3] = 7.360000E+01;
    // scripts/config.cct:44
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_LINEAR_RATE   ))[0] = 9.060000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_LINEAR_RATE   ))[1] = 9.160000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_LINEAR_RATE   ))[2] = 9.260000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_LINEAR_RATE   ))[3] = 9.360000E+01;
    // scripts/config.cct:45
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MAX  ))[0] = 1.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MAX  ))[1] = 1.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MAX  ))[2] = 1.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MAX  ))[3] = 1.000000E+01;
    // scripts/config.cct:46
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MIN  ))[0] = -1.000000E+01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MIN  ))[1] = 0.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MIN  ))[2] = 3.000000E-01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_PRE_FUNC_MIN  ))[3] = -1.000000E+01;
    // scripts/config.cct:47
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_MINRMS        ))[0] = 0.000000E+00;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_MINRMS        ))[1] = 1.000000E-01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_MINRMS        ))[2] = 1.000000E-01;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_B_MINRMS        ))[3] = 0.000000E+00;
    // scripts/config.cct:48
    _Static_assert(2 <= (2), "List of values does not fit!");
    (refMgrParGetPointer(&ref_mgr,DEFAULT_PLATEAU_DURATION))[0] = 2.000000E-02;
    (refMgrParGetPointer(&ref_mgr,DEFAULT_PLATEAU_DURATION))[1] = 5.000000E-02;
    // scripts/config.cct:49
    *(refMgrParGetPointer(&ref_mgr,DIRECT_V_REF          )) = 0.000000E+00;
    // scripts/config.cct:50
    *(refMgrParGetPointer(&ref_mgr,DIRECT_I_REF          )) = 0.000000E+00;
    // scripts/config.cct:51
    *(refMgrParGetPointer(&ref_mgr,DIRECT_B_REF          )) = 0.000000E+00;
    // scripts/config.cct:52
    *(refMgrParGetPointer(&ref_mgr,DIRECT_DEGAUSS_AMP_PP )) = 0.2;
    // scripts/config.cct:53
    *(refMgrParGetPointer(&ref_mgr,DIRECT_DEGAUSS_PERIOD )) = 0.1;
    // scripts/config.cct:54
    // (skipped write to ccpars_global.super_cycle)
    // scripts/config.cct:55
    // (skipped write to &ccpars_global.basic_period_ms)
    // scripts/config.cct:56
    // (skipped write to &ccpars_global.event_advance_ms)
    // scripts/config.cct:57
    // (skipped write to &ccpars_global.float_tolerance)
    // scripts/config.cct:58
    // (skipped write to &ccpars_global.inc_float_tolerance)
    // scripts/config.cct:59
    *(refMgrParGetPointer(&ref_mgr, V_FEEDFWD_GAIN)) = 0.000000E+00;
    // scripts/config.cct:60
    *(refMgrParGetPointer(&ref_mgr, V_FEEDFWD_DELAY_PERIODS)) = 0.000000E+00;
    // scripts/config.cct:61
    *(refMgrParGetPointer(&ref_mgr,ILC_START_TIME    )) = 0.000000E+00;
    // scripts/config.cct:62
    *(refMgrParGetPointer(&ref_mgr,ILC_STOP_REF      )) = 0.000000E+00;
    // scripts/config.cct:63
    *(refMgrParGetPointer(&ref_mgr,ILC_L_ORDER       )) = 0;
    // scripts/config.cct:64
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_PERIOD_ITERS             ))[0] = 10;
    (regMgrParAppValue( &reg_pars, IREG_PERIOD_ITERS             ))[1] = 10;
    (regMgrParAppValue( &reg_pars, IREG_PERIOD_ITERS             ))[2] = 10;
    (regMgrParAppValue( &reg_pars, IREG_PERIOD_ITERS             ))[3] = 10;
    // scripts/config.cct:65
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXTERNAL_ALG             ))[0] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, IREG_EXTERNAL_ALG             ))[1] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, IREG_EXTERNAL_ALG             ))[2] = CC_DISABLED;
    (regMgrParAppValue( &reg_pars, IREG_EXTERNAL_ALG             ))[3] = CC_DISABLED;
    // scripts/config.cct:66
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_MEAS_SELECT          ))[0] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_INT_MEAS_SELECT          ))[1] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_INT_MEAS_SELECT          ))[2] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_INT_MEAS_SELECT          ))[3] = REG_MEAS_EXTRAPOLATED;
    // scripts/config.cct:67
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_PURE_DELAY_PERIODS   ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_PURE_DELAY_PERIODS   ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_PURE_DELAY_PERIODS   ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_PURE_DELAY_PERIODS   ))[3] = 0.000000E+00;
    // scripts/config.cct:68
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE1_HZ          ))[0] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE1_HZ          ))[1] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE1_HZ          ))[2] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE1_HZ          ))[3] = 1.000000E+01;
    // scripts/config.cct:69
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_HZ         ))[0] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_HZ         ))[1] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_HZ         ))[2] = 1.000000E+01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_HZ         ))[3] = 1.000000E+01;
    // scripts/config.cct:70
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_Z          ))[0] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_Z          ))[1] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_Z          ))[2] = 5.000000E-01;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLES2_Z          ))[3] = 5.000000E-01;
    // scripts/config.cct:71
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE4_HZ          ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE4_HZ          ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE4_HZ          ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE4_HZ          ))[3] = 0.000000E+00;
    // scripts/config.cct:72
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE5_HZ          ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE5_HZ          ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE5_HZ          ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_INT_AUXPOLE5_HZ          ))[3] = 0.000000E+00;
    // scripts/config.cct:73
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_MEAS_SELECT          ))[0] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_EXT_MEAS_SELECT          ))[1] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_EXT_MEAS_SELECT          ))[2] = REG_MEAS_EXTRAPOLATED;
    (regMgrParAppValue( &reg_pars, IREG_EXT_MEAS_SELECT          ))[3] = REG_MEAS_EXTRAPOLATED;
    // scripts/config.cct:74
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_TRACK_DELAY_PERIODS  ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_TRACK_DELAY_PERIODS  ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_TRACK_DELAY_PERIODS  ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_TRACK_DELAY_PERIODS  ))[3] = 0.000000E+00;
    // scripts/config.cct:75
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_R                 ))[15] = 0.000000E+00;
    // scripts/config.cct:76
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_S                 ))[15] = 0.000000E+00;
    // scripts/config.cct:77
    _Static_assert(16 <= (REG_NUM_RST_COEFFS), "List of values does not fit!");
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[0] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[1] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[2] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[3] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[4] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[5] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[6] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[7] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[8] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[9] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[10] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[11] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[12] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[13] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[14] = 0.000000E+00;
    (regMgrParAppValue( &reg_pars, IREG_EXT_OP_T                 ))[15] = 0.000000E+00;
    // scripts/config.cct:78
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_POS                               ))[0] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_POS                               ))[1] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_POS                               ))[2] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_POS                               ))[3] = 1.200000E+01;
    // scripts/config.cct:79
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_STANDBY                           ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_STANDBY                           ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_STANDBY                           ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_STANDBY                           ))[3] = 0.000000E+00;
    // scripts/config.cct:80
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_NEG                               ))[0] = -1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_NEG                               ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_NEG                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_NEG                               ))[3] = -1.200000E+01;
    // scripts/config.cct:81
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_RATE                              ))[0] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_RATE                              ))[1] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_RATE                              ))[2] = 1.200000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_RATE                              ))[3] = 1.200000E+01;
    // scripts/config.cct:82
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_CLOSELOOP                         ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_CLOSELOOP                         ))[1] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_CLOSELOOP                         ))[2] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_CLOSELOOP                         ))[3] = 0.000000E+00;
    // scripts/config.cct:83
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_LOW                               ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_LOW                               ))[1] = 1.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_LOW                               ))[2] = 1.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_LOW                               ))[3] = 0.000000E+00;
    // scripts/config.cct:84
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ZERO                              ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ZERO                              ))[1] = 1.000000E-02;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ZERO                              ))[2] = 1.000000E-02;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ZERO                              ))[3] = 0.000000E+00;
    // scripts/config.cct:85
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_WARNING                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_WARNING                       ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_WARNING                       ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_WARNING                       ))[3] = 0.000000E+00;
    // scripts/config.cct:86
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_FAULT                         ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_FAULT                         ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_FAULT                         ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_B_ERR_FAULT                         ))[3] = 0.000000E+00;
    // scripts/config.cct:87
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_POS                               ))[0] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_POS                               ))[1] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_POS                               ))[2] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_POS                               ))[3] = 1.000000E+01;
    // scripts/config.cct:88
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_STANDBY                           ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_STANDBY                           ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_STANDBY                           ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_STANDBY                           ))[3] = 0.000000E+00;
    // scripts/config.cct:89
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_NEG                               ))[0] = -1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_NEG                               ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_NEG                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_NEG                               ))[3] = -1.000000E+01;
    // scripts/config.cct:90
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RATE                              ))[0] = 2.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RATE                              ))[1] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RATE                              ))[2] = 1.000000E+01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RATE                              ))[3] = 1.000000E+01;
    // scripts/config.cct:91
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_CLOSELOOP                         ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_CLOSELOOP                         ))[1] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_CLOSELOOP                         ))[2] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_CLOSELOOP                         ))[3] = 0.000000E+00;
    // scripts/config.cct:92
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_LOW                               ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_LOW                               ))[1] = 1.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_LOW                               ))[2] = 1.000000E-01;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_LOW                               ))[3] = 0.000000E+00;
    // scripts/config.cct:93
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ZERO                              ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ZERO                              ))[1] = 1.000000E-02;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ZERO                              ))[2] = 1.000000E-02;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ZERO                              ))[3] = 0.000000E+00;
    // scripts/config.cct:94
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_WARNING                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_WARNING                       ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_WARNING                       ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_WARNING                       ))[3] = 0.000000E+00;
    // scripts/config.cct:95
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_FAULT                         ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_FAULT                         ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_FAULT                         ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_ERR_FAULT                         ))[3] = 0.000000E+00;
    // scripts/config.cct:96
    _Static_assert(2 <= (2), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_QUADRANTS41                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_QUADRANTS41                       ))[1] = 0.000000E+00;
    // scripts/config.cct:97
    *(regMgrParAppPointer(&reg_pars, LIMITS_I_RMS_TC                            )) = 0.000000E+00;
    // scripts/config.cct:98
    *(regMgrParAppPointer(&reg_pars, LIMITS_I_RMS_WARNING                       )) = 0.000000E+00;
    // scripts/config.cct:99
    *(regMgrParAppPointer(&reg_pars, LIMITS_I_RMS_FAULT                         )) = 0.000000E+00;
    // scripts/config.cct:100
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_TC                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_TC                       ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_TC                       ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_TC                       ))[3] = 0.000000E+00;
    // scripts/config.cct:101
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_WARNING                  ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_WARNING                  ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_WARNING                  ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_WARNING                  ))[3] = 0.000000E+00;
    // scripts/config.cct:102
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_FAULT                    ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_FAULT                    ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_FAULT                    ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_I_RMS_LOAD_FAULT                    ))[3] = 0.000000E+00;
    // scripts/config.cct:103
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_V_POS                               ))[0] = 1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_POS                               ))[1] = 1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_POS                               ))[2] = 1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_POS                               ))[3] = 1.000000E+02;
    // scripts/config.cct:104
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_V_NEG                               ))[0] = -1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_NEG                               ))[1] = -1.000000E+02;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_NEG                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_NEG                               ))[3] = -1.000000E+02;
    // scripts/config.cct:105
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_RATE                              )) = 1.000000E+03;
    // scripts/config.cct:106
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_RATE_RMS_TC                       )) = 0.000000E+00;
    // scripts/config.cct:107
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_RATE_RMS_FAULT                    )) = 0.000000E+00;
    // scripts/config.cct:108
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_ERR_WARNING                       )) = 0.000000E+00;
    // scripts/config.cct:109
    *(regMgrParAppPointer(&reg_pars, LIMITS_V_ERR_FAULT                         )) = 0.000000E+00;
    // scripts/config.cct:110
    _Static_assert(2 <= (2), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_V_QUADRANTS41                       ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_V_QUADRANTS41                       ))[1] = 0.000000E+00;
    // scripts/config.cct:111
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_P_POS                               ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_POS                               ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_POS                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_POS                               ))[3] = 0.000000E+00;
    // scripts/config.cct:112
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LIMITS_P_NEG                               ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_NEG                               ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_NEG                               ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LIMITS_P_NEG                               ))[3] = 0.000000E+00;
    // scripts/config.cct:113
    *(regMgrParAppPointer(&reg_pars, LOAD_SELECT        )) = 0;
    // scripts/config.cct:114
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_SER      ))[0] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_SER      ))[1] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_SER      ))[2] = 5.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_SER      ))[3] = 5.000000E-01;
    // scripts/config.cct:115
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_PAR      ))[0] = 1.000000E+09;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_PAR      ))[1] = 1.000000E+09;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_PAR      ))[2] = 1.000000E+09;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_PAR      ))[3] = 1.000000E+02;
    // scripts/config.cct:116
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_MAG      ))[0] = 6.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_MAG      ))[1] = 6.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_MAG      ))[2] = 6.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_OHMS_MAG      ))[3] = 6.000000E-01;
    // scripts/config.cct:117
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS        ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS        ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS        ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS        ))[3] = 1.000000E+00;
    // scripts/config.cct:118
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS_SAT    ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS_SAT    ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS_SAT    ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_HENRYS_SAT    ))[3] = 1.000000E+00;
    // scripts/config.cct:119
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_GAIN    ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_GAIN    ))[1] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_GAIN    ))[2] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_GAIN    ))[3] = 1.000000E+00;
    // scripts/config.cct:120
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_START   ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_START   ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_START   ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_START   ))[3] = 0.000000E+00;
    // scripts/config.cct:121
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_END     ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_END     ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_END     ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_I_SAT_END     ))[3] = 0.000000E+00;
    // scripts/config.cct:122
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_SAT_SMOOTHING ))[0] = 3.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_SAT_SMOOTHING ))[1] = 3.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_SAT_SMOOTHING ))[2] = 3.000000E-01;
    (regMgrParAppValue(  &reg_pars, LOAD_SAT_SMOOTHING ))[3] = 3.000000E-01;
    // scripts/config.cct:123
    _Static_assert(4 <= (REG_NUM_LOADS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, LOAD_GAUSS_PER_AMP ))[0] = 1.200000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_GAUSS_PER_AMP ))[1] = 1.200000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_GAUSS_PER_AMP ))[2] = 1.200000E+00;
    (regMgrParAppValue(  &reg_pars, LOAD_GAUSS_PER_AMP ))[3] = 1.200000E+00;
    // scripts/config.cct:124
    *(regMgrParAppPointer(&reg_pars, LOAD_SIM_TC_ERROR  )) = 0.000000E+00;
    // scripts/config.cct:125
    log_buffers.acq.selectors[0][LOG_MENU_ACQ_MPX_IDX] = LOG_ACQ_DSP_RT_CPU;
    // scripts/config.cct:126
    // (skipped write to &log_buffers.acq_fltr.selectors[0]     [LOG_MENU_ACQ_FLTR_MPX_IDX])
    // scripts/config.cct:127
    // (skipped write to &log_buffers.b_meas.selectors[0]       [LOG_MENU_B_MEAS_MPX_IDX])
    // scripts/config.cct:128
    // (skipped write to &log_buffers.i_meas.selectors[0]       [LOG_MENU_I_MEAS_MPX_IDX])
    // scripts/config.cct:129
    // (skipped write to &log_buffers.b_reg.selectors[0]        [LOG_MENU_B_REG_MPX_IDX])
    // scripts/config.cct:130
    // (skipped write to &log_buffers.i_reg.selectors[0]        [LOG_MENU_I_REG_MPX_IDX])
    // scripts/config.cct:131
    *(sigVarGetPointer(&sig_struct, select, i_meas, SELECT_SELECTOR               )) = SIG_AB;
    // scripts/config.cct:132
    *(regMgrParAppPointer(&reg_pars, MEAS_B_DELAY_ITERS                            )) = 1.300000E+00;
    // scripts/config.cct:133
    *(regMgrParAppPointer(&reg_pars, MEAS_I_DELAY_ITERS                            )) = 1.300000E+00;
    // scripts/config.cct:134
    *(regMgrParAppPointer(&reg_pars, MEAS_V_DELAY_ITERS                            )) = 1.300000E+00;
    // scripts/config.cct:135
    _Static_assert(2 <= (2), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, MEAS_B_FIR_LENGTHS                            ))[0] = 0;
    (regMgrParAppValue(  &reg_pars, MEAS_B_FIR_LENGTHS                            ))[1] = 0;
    // scripts/config.cct:136
    _Static_assert(2 <= (2), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, MEAS_I_FIR_LENGTHS                            ))[0] = 0;
    (regMgrParAppValue(  &reg_pars, MEAS_I_FIR_LENGTHS                            ))[1] = 0;
    // scripts/config.cct:137
    *(refMgrParGetPointer(&ref_mgr,MODE_REF_SUB_SEL     )) = CC_ENABLED;
    // scripts/config.cct:138
    *(refMgrParGetPointer(&ref_mgr,MODE_REF_CYC_SEL     )) = CC_ENABLED;
    // scripts/config.cct:139
    *(refMgrParGetPointer(&ref_mgr,MODE_REG_MODE_CYC    )) = REG_CURRENT;
    // scripts/config.cct:140
    *(refMgrParGetPointer(&ref_mgr,MODE_PRE_FUNC        )) = REF_PRE_FUNC_UPMINMAX;
    // scripts/config.cct:141
    *(refMgrParGetPointer(&ref_mgr,MODE_POST_FUNC       )) = REF_POST_FUNC_MINRMS;
    // scripts/config.cct:142
    *(refMgrParGetPointer(&ref_mgr,MODE_SIM_MEAS        )) = CC_ENABLED;
    // scripts/config.cct:143
    *(refMgrParGetPointer(&ref_mgr,MODE_RT_REF          )) = CC_DISABLED;
    // scripts/config.cct:144
    *(refMgrParGetPointer(&ref_mgr,RT_NUM_STEPS         )) = 0;
    // scripts/config.cct:145
    *(refMgrParGetPointer(&ref_mgr,MODE_ECONOMY         )) = CC_DISABLED;
    // scripts/config.cct:146
    *(refMgrParGetPointer(&ref_mgr,MODE_USE_ARM_NOW     )) = CC_DISABLED;
    // scripts/config.cct:147
    *(refMgrParGetPointer(&ref_mgr,MODE_TO_OFF_FAULTS   )) = 0x0;
    // scripts/config.cct:148
    *(refMgrParGetPointer(&ref_mgr,MODE_ILC             )) = CC_DISABLED;
    // scripts/config.cct:149
    *(regMgrParAppPointer(&reg_pars, PC_ACTUATION      )) = REG_VOLTAGE_REF;
    // scripts/config.cct:150
    *(regMgrParAppPointer(&reg_pars, PC_ACT_DELAY_ITERS)) = 1.000000E+00;
    // scripts/config.cct:151
    *(regMgrParAppPointer(&reg_pars, PC_NUM_VSOURCES   )) = 1;
    // scripts/config.cct:152
    *(regMgrParAppPointer(&reg_pars, PC_GAIN           )) = 1.000000E+01;
    // scripts/config.cct:153
    *(regMgrParAppPointer(&reg_pars, PC_OFFSET         )) = 0.000000E+00;
    // scripts/config.cct:154
    *(refMgrParGetPointer(&ref_mgr,  PC_DAC_CLAMP      )) = 0.000000E+00;
    // scripts/config.cct:155
    *(regMgrParAppPointer(&reg_pars, PC_FIRING_MODE    )) = REG_CASSEL;
    // scripts/config.cct:156
    *(regMgrParAppPointer(&reg_pars, PC_FIRING_DELAY   )) = 1.000000E-03;
    // scripts/config.cct:157
    _Static_assert(101 <= (REG_FIRING_LUT_LEN), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[0] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[3] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[4] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[5] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[6] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[7] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[8] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[9] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[10] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[11] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[12] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[13] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[14] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[15] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[16] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[17] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[18] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[19] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[20] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[21] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[22] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[23] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[24] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[25] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[26] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[27] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[28] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[29] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[30] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[31] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[32] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[33] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[34] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[35] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[36] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[37] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[38] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[39] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[40] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[41] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[42] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[43] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[44] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[45] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[46] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[47] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[48] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[49] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[50] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[51] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[52] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[53] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[54] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[55] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[56] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[57] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[58] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[59] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[60] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[61] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[62] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[63] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[64] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[65] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[66] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[67] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[68] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[69] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[70] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[71] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[72] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[73] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[74] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[75] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[76] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[77] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[78] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[79] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[80] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[81] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[82] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[83] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[84] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[85] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[86] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[87] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[88] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[89] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[90] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[91] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[92] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[93] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[94] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[95] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[96] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[97] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[98] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[99] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_FIRING_LUT     ))[100] = 0.000000E+00;
    // scripts/config.cct:158
    *(regMgrParAppPointer(&reg_pars, PC_HENRYS         )) = 1.000000E-03;
    // scripts/config.cct:159
    *(regMgrParAppPointer(&reg_pars, PC_OHMS           )) = 1.000000E-01;
    // scripts/config.cct:160
    *(regMgrParAppPointer(&reg_pars, PC_FARADS1        )) = 4.000000E-02;
    // scripts/config.cct:161
    *(regMgrParAppPointer(&reg_pars, PC_FARADS2        )) = 1.000000E-02;
    // scripts/config.cct:162
    *(regMgrParAppPointer(&reg_pars, PC_SIM_TC_ERROR   )) = 0.000000E+00;
    // scripts/config.cct:163
    *(regMgrParAppPointer(&reg_pars, PC_SIM_BANDWIDTH  )) = 2.000000E+02;
    // scripts/config.cct:164
    *(regMgrParAppPointer(&reg_pars, PC_SIM_Z          )) = 9.000000E-01;
    // scripts/config.cct:165
    *(regMgrParAppPointer(&reg_pars, PC_SIM_TAU_ZERO   )) = 0.000000E+00;
    // scripts/config.cct:166
    _Static_assert(4 <= (REG_NUM_PC_SIM_COEFFS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, PC_SIM_NUM        ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_SIM_NUM        ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_SIM_NUM        ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_SIM_NUM        ))[3] = 0.000000E+00;
    // scripts/config.cct:167
    _Static_assert(4 <= (REG_NUM_PC_SIM_COEFFS), "List of values does not fit!");
    (regMgrParAppValue(  &reg_pars, PC_SIM_DEN        ))[0] = 1.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_SIM_DEN        ))[1] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_SIM_DEN        ))[2] = 0.000000E+00;
    (regMgrParAppValue(  &reg_pars, PC_SIM_DEN        ))[3] = 0.000000E+00;
    // scripts/config.cct:168
    // (skipped write to polswitchParGetPointer(&polswitch_mgr,TIMEOUT))
    // scripts/config.cct:169
    // (skipped write to polswitchParGetPointer(&polswitch_mgr,AUTOMATIC    ))
    // scripts/config.cct:170
    // (skipped write to polswitchParGetPointer(&polswitch_mgr,MOVEMENT_TIME))
    // scripts/config.cct:171
    // (skipped write to &ccpars_rtref.sampling_iters)
    // scripts/config.cct:172
    // (skipped write to &ccpars_rtref.offset)
    // scripts/config.cct:173
    // (skipped write to &ccpars_rtref.amplitude)
    // scripts/config.cct:174
    // (skipped write to &ccpars_rtref.frequency)
    // scripts/config.cct:175
    // (skipped write to ccpars_sim.adc_signal)
    // scripts/config.cct:176
    // (skipped write to &ccpars_sim.vs_quantization)
    // scripts/config.cct:177
    // (skipped write to &ccpars_sim.tone_pp[TONE_VS])
    // scripts/config.cct:178
    // (skipped write to &ccpars_sim.meas_tone_period_iters)
    // scripts/config.cct:179
    // (skipped write to &ccpars_sim.tone_pp[TONE_DCCT_A])
    // scripts/config.cct:180
    // (skipped write to &ccpars_sim.tone_pp[TONE_DCCT_B])
    // scripts/config.cct:181
    // (skipped write to &ccpars_sim.tone_pp[TONE_B_PROBE])
    // scripts/config.cct:182
    // (skipped write to &ccpars_sim.tone_pp[TONE_I_PROBE])
    // scripts/config.cct:183
    // (skipped write to &ccpars_sim.noise_vs)
    // scripts/config.cct:184
    // (skipped write to &ccpars_sim.noise_dcct_a)
    // scripts/config.cct:185
    // (skipped write to &ccpars_sim.noise_dcct_b)
    // scripts/config.cct:186
    // (skipped write to &ccpars_sim.noise_v_probe)
    // scripts/config.cct:187
    // (skipped write to &ccpars_sim.noise_b_probe)
    // scripts/config.cct:188
    // (skipped write to &ccpars_sim.noise_i_probe)
    // scripts/config.cct:189
    // (skipped write to &ccpars_sim.noise_adcs)
    // scripts/config.cct:190
    *(regMgrParAppPointer(&reg_pars, VFILTER_V_MEAS_SOURCE)) = REG_MEASUREMENT;
    // scripts/config.cct:191
    *(regMgrParAppPointer(&reg_pars, VFILTER_I_CAPA_SOURCE)) = REG_MEASUREMENT;
    // scripts/config.cct:192
    *(regMgrParAppPointer(&reg_pars, VFILTER_FULL_I_CAPA  )) = CC_DISABLED;
    // scripts/config.cct:193
    *(regMgrParAppPointer(&reg_pars, VFILTER_EXT_CLBW     )) = 5.000000E+01;
    // scripts/config.cct:194
    *(regMgrParAppPointer(&reg_pars, VFILTER_EXT_Z        )) = 7.000000E-01;
    // scripts/config.cct:195
    *(regMgrParAppPointer(&reg_pars, VFILTER_EXT_K_U      )) = 0.000000E+00;
    // scripts/config.cct:196
    *(regMgrParAppPointer(&reg_pars, VFILTER_EXT_K_I      )) = 0.000000E+00;
    // scripts/config.cct:197
    *(regMgrParAppPointer(&reg_pars, VFILTER_EXT_K_D      )) = 0.000000E+00;
    // scripts/config.cct:198
    *(regMgrParAppPointer(&reg_pars, VREG_EXT_CLBW )) = 3.000000E+01;
    // scripts/config.cct:199
    *(regMgrParAppPointer(&reg_pars, VREG_EXT_Z    )) = 7.000000E-01;
    // scripts/config.cct:200
    *(regMgrParAppPointer(&reg_pars, VREG_EXT_K_INT)) = 0.000000E+00;
    // scripts/config.cct:201
    *(regMgrParAppPointer(&reg_pars, VREG_EXT_K_P  )) = 0.000000E+00;
    // scripts/config.cct:202
    *(regMgrParAppPointer(&reg_pars, VREG_EXT_K_FF )) = 1.000000E+00;
    // tests/direct/direct.cct:7
    *(&ccpars_log.read_timing.duration_ms) = 7000;
    // tests/direct/direct.cct:8
    *(&ccpars_logmenu[LOG_MENU_ACQ_MPX             ]) = CC_ENABLED;
    *(&ccpars_logmenu[LOG_MENU_I_REG               ]) = CC_ENABLED;
    // tests/direct/direct.cct:10
    *(refMgrParGetPointer(&ref_mgr,MODE_REG_MODE        )) = REG_CURRENT;
    // tests/direct/direct.cct:11
    *(refMgrParGetPointer(&ref_mgr,MODE_REF             )) = REF_DIRECT;
    // tests/direct/direct.cct:13
    *(&ccpars_mode.pc) = REF_PC_ON;

    // return new state
    return (StateFunc_t) &WAIT_5_0_None_None_None_None;
}

static StateFunc_t from_WAIT_5_0_None_None_None_None(void)
{
    // tests/direct/direct.cct:18
    *(refMgrParGetPointer(&ref_mgr,DIRECT_I_REF          )) = 5;

    // return new state
    return (StateFunc_t) &WAIT_4_0_None_None_None_None;
}

static StateFunc_t from_WAIT_4_0_None_None_None_None(void)
{
    // tests/direct/direct.cct:22
    *(refMgrParGetPointer(&ref_mgr,DIRECT_I_REF          )) = 0;

    // return new state
    return (StateFunc_t) &WAIT_0_8_None_None_None_None;
}

static StateFunc_t from_WAIT_0_8_None_None_None_None(void)
{
    // tests/direct/direct.cct:26
    *(refMgrParGetPointer(&ref_mgr,MODE_REF             )) = REF_STANDBY;

    // return new state
    return (StateFunc_t) &WAIT_1_0_None_None_None_None;
}

static StateFunc_t from_WAIT_1_0_None_None_None_None(void)
{
    // tests/direct/direct.cct:30
    // (skipped write to &ccpars_log.time_origin)
    // tests/direct/direct.cct:32
    // TODO: WRITELOGS

    // end of script
    return NULL;
}

// tests/direct/direct.cct:16
static StateFunc_t WAIT_5_0_None_None_None_None(void)
{
    if (cctestTimeInStateElapsed(5.0))
    {
        // call transition function and propagate returned new state
        return from_WAIT_5_0_None_None_None_None();
    }
    else
    {
        // stay in current state
        return (StateFunc_t) &WAIT_5_0_None_None_None_None;
    }
}

// tests/direct/direct.cct:20
static StateFunc_t WAIT_4_0_None_None_None_None(void)
{
    if (cctestTimeInStateElapsed(4.0))
    {
        // call transition function and propagate returned new state
        return from_WAIT_4_0_None_None_None_None();
    }
    else
    {
        // stay in current state
        return (StateFunc_t) &WAIT_4_0_None_None_None_None;
    }
}

// tests/direct/direct.cct:24
static StateFunc_t WAIT_0_8_None_None_None_None(void)
{
    if (cctestTimeInStateElapsed(0.8))
    {
        // call transition function and propagate returned new state
        return from_WAIT_0_8_None_None_None_None();
    }
    else
    {
        // stay in current state
        return (StateFunc_t) &WAIT_0_8_None_None_None_None;
    }
}

// tests/direct/direct.cct:28
static StateFunc_t WAIT_1_0_None_None_None_None(void)
{
    if (cctestTimeInStateElapsed(1.0))
    {
        // call transition function and propagate returned new state
        return from_WAIT_1_0_None_None_None_None();
    }
    else
    {
        // stay in current state
        return (StateFunc_t) &WAIT_1_0_None_None_None_None;
    }
}

const struct CCRT2C_script script_direct = {
    .iter_period_us         = 100,
    .event_log_period_iters = 50,

    .init                   = &INIT,
};
