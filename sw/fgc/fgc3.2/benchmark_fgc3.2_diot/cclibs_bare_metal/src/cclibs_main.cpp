#include "utility/benchmark.hpp"

#include "libintlk.h"
#include "liblog.h"
#include "libpolswitch.h"
#include "libref.h"
#include "libref/refParsLinkStructs.h"
#include "libreg.h"

#include <pthread.h>

#include <algorithm>
#include <array>
#include <numeric>
#include <stdarg.h>
#include <vector>

#if USE_FPGA_ACCESS
#ifdef _FGC32
#include <malloc.h>
extern "C" {
#include "fgc32_fpga.h"
}
void fpga_dummy_read() {
    my_getnanosecs();
    __asm__ volatile("dsb sy");
}
#endif
#else
void fpga_dummy_read() {}
#endif

#ifndef __aarch64__
#include <fstream>
#endif

static float dummy_float = 42.0f;
static float sig_cpu_usage_us = -66.0f;
static uint32_t dummy_bitfield = 0x55555555;

#include "logMenus.h"
#include "logStructs.h"
#include "sigStructs.h"

#include "cctest_runtime.h"

#include "logMenusInit.h"
#include "logStructsInit.h"
#include "sigStructsInit.h"



#define TABLE_LEN    5000

#define CC_FILTER_BUF_LEN           3000                // Max length of FIR and Extrapolation buffers
#define CC_ILC_MAX_SAMPLES          1000                // Max length of ILC reference buffer

REG_mgr reg_mgr;
REF_mgr ref_mgr;
static POLSWITCH_mgr polswitch_mgr;

REG_pars reg_pars;
SIG_struct sig_struct;

CC_pars_mode ccpars_mode;
CC_sim_vars ccsim;

struct REF_ref_pars ccpars_ref[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS];

struct REF_test_pars ccpars_test[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS] = { {
    /*[0][0] =*/ { .initial_ref   = { 0.0         },      // TEST INITIAL_REF
               .amplitude_pp  = { 1.0         },      // TEST AMPLITUDE_PP
               .num_periods   = { 1           },      // TEST NUM_PERIODS
               .period_iters  = { 0           },      // TEST PERIOD_ITERS
               .period        = { 0.8         },      // TEST PERIOD
               .window        = { CC_ENABLED  },      // TEST WINDOW
               .exp_decay     = { CC_DISABLED }, }    // TEST EXP_DECAY
} };

#ifdef _FGC32
#define secion_log_buffers __attribute__((section(".log_buffers")))
#else
#define secion_log_buffers
#endif

// liblog structures
static struct LOG_mgr log_mgr;
static struct LOG_structs log_structs;
secion_log_buffers struct LOG_buffers log_buffers;
static struct LOG_menus log_menus;

static LOG_read log_read;

static enum REF_fg_par_idx             static_fg_pars_idx[CC_NUM_SUB_SELS][CC_NUM_CYC_SELS][REF_FG_PAR_MAX_PER_GROUP]; // Null terminal array of libref fg parameter indexes of armed or restored parameters

struct CC_pars_log ccpars_log
        = {
                .time_origin          = 0.0,            // LOG TIME_ORIGIN
                .cyc_sel              = 0,              // LOG CYC_SEL
                .sub_sampling_period  = 0,              // LOG SUB_SAMPLING_PERIOD
//                .prev_cycle           = CC_DISABLED,    // LOG PREV_CYCLE
//                .overwrite            = CC_ENABLED,     // LOG OVERWRITE
                .csv_spy              = CC_ENABLED,     // LOG CSV_SPY
                .binary_spy           = CC_DISABLED,    // LOG BINARY_SPY
//                .pm_buf               = CC_DISABLED,    // LOG PM_BUF
//                .internal_tests       = CC_DISABLED,    // LOG INTERNAL_TESTS
        };

enum CC_enabled_disabled ccpars_logmenu[LOG_NUM_MENUS];

#ifndef _FGC32
static void ccLogWriteSpy(void);
#endif

static void ccRunLogContinuousSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary, double iter_time_s);
static void ccRunLogSignals(bool const iter_1s_boundary, bool const iter_200ms_boundary, double iter_time_s);
static void ccSimPcStateRT(void);

static SingleBenchmarkSummary ccBenchmarkSingle(CCRT2C_script const& script, bool save_log);

void ccRtPrintError(char const * const format, ...)
{
    va_list args;

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
}


static void ccRtPrintErrorAndExit(char const * const format, ...)
{
    va_list args;

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    exit(EXIT_FAILURE);
}

static struct Equipdev_ppm_transactional
{
    struct REF_ramp_pars        ramp;                   //!< REF.RAMP.*
    struct REF_pulse_pars       pulse;                  //!< REF.PULSE.*
    struct REF_plep_pars        plep;                   //!< REF.PLEP.*
    struct REF_pppl_pars        pppl;                   //!< REF.PPPL.*
    struct REF_cubexp_pars      cubexp;                 //!< REF.CUBEXP.*
    struct REF_trim_pars        trim;                   //!< REF.TRIM.*
    struct REF_prbs_pars        prbs;                   //!< REF.PRBS.*

    struct REF_table_pars
    {
        FG_point             function[TABLE_LEN];   //!< REF.TABLE.FUNC.VALUE
        uintptr_t            num_elements;              //!< Number of elements in REF.TABLE.FUNC.VALUE
    } table;
} transactional;

static void ccInitLibreg(int iter_period_us)
{
    // Set default parameter values

    auto err_num = regMgrInit(
            &reg_mgr,
            &reg_pars,
            nullptr,
            nullptr,
            nullptr,
            nullptr,
            nullptr,
            0,
            iter_period_us,
            CC_ENABLED,
            CC_ENABLED,
            CC_ENABLED);

    if(err_num != 0)
    {
        // Report error code

        ccRtPrintErrorAndExit("regMgrInit() reports error number %d", err_num);
    }

    // Prepare measurement FIR buffers

    int32_t * const i_meas_filter_buffer = (int32_t *) calloc(CC_FILTER_BUF_LEN, sizeof(int32_t));
    int32_t * const b_meas_filter_buffer = (int32_t *) calloc(CC_FILTER_BUF_LEN, sizeof(int32_t));

    if(i_meas_filter_buffer == NULL || b_meas_filter_buffer == NULL)
    {
        ccRtPrintErrorAndExit("Memory allocation failure during libreg initialization");
    }

    regMeasFilterInitBuffer(&reg_mgr.b.meas, i_meas_filter_buffer, CC_FILTER_BUF_LEN);
    regMeasFilterInitBuffer(&reg_mgr.i.meas, b_meas_filter_buffer, CC_FILTER_BUF_LEN);

    printf("Success regMgrInit\n");
}

static void ccInitLibref()
{
    // Needed, otherwise script_idle.cct fails with:
    // assertion "*(refMgrVarGetPointer(&ref_mgr, REF_STATE)) == (REF_STANDBY)" failed: file "../src/script_idle.c", line 965, function: from_WAIT_30_0_STATE_REF___STANDBY

    memset(&ref_mgr, 0, sizeof(ref_mgr));

    // Initialize libref parameter pointers to ccrt variables

     // ILC - Link to function parameter array lengths

    //  refMgrParSetValue(&ref_mgr, ILC_L_FUNC_NUM_ELS, ilc_pars[ILC_L_FUNC].num_elements);
    //  refMgrParSetValue(&ref_mgr, ILC_Q_FUNC_NUM_ELS, ilc_pars[ILC_Q_FUNC].num_elements);

     // TRANSACTION

    static struct REF_transaction_pars transaction;

     refMgrInitTransactionPars(&ref_mgr,
                               &transaction,
                               0,
                               sizeof(transaction));

     // CTRL - this shares ref_pars with REF FG_TYPE

    static struct REF_ctrl_pars ctrl;

     refMgrInitCtrlPars(&ref_mgr,
                        &ctrl,
                         0,
                         sizeof(ctrl));

     // REF - this shares ref_pars with REF PLAY and REF DYN_ECO_END_TIME

     refMgrInitRefPars(&ref_mgr,
                       &ccpars_ref[0][0],
                        0,
                        sizeof(struct REF_ref_pars));

    // RAMP

    refMgrInitRampPars(&ref_mgr,
                       &transactional.ramp,
                        0,
                        sizeof(transactional));

    // PULSE

    refMgrInitPulsePars(&ref_mgr,
                        &transactional.pulse,
                        0,
                        sizeof(transactional));

    // PLEP

    refMgrInitPlepPars(&ref_mgr,
                       &transactional.plep,
                        0,
                        sizeof(transactional));

    // ref_mgr.test_plep_initial_rate = &ccpars_global.test_plep_initial_rate;   // Test inital_rate parameter for PLEP function testing
    // ref_mgr.test_plep_final_rate   = &ccpars_global.test_plep_final_rate;     // Test final_rate parameter for PLEP function testing

    // PPPL

    refMgrInitPpplPars(&ref_mgr,
                       &transactional.pppl,
                        0,
                        sizeof(transactional));

    // PPPL num_els parameters are stored in a contiguous array 2D, hence the sub_sel_step and cyc_sel_step values need to be overridden

    // refMgrFgParInitPointer   (&ref_mgr, pppl_acceleration1_num_els, pppl_pars[PPPL_ACCELERATION1].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_acceleration1_num_els, sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_acceleration1_num_els, sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_acceleration2_num_els, pppl_pars[PPPL_ACCELERATION2].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_acceleration2_num_els, sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_acceleration2_num_els, sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_acceleration3_num_els, pppl_pars[PPPL_ACCELERATION3].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_acceleration3_num_els, sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_acceleration3_num_els, sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_rate2_num_els        , pppl_pars[PPPL_RATE2].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_rate2_num_els        , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_rate2_num_els        , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_rate4_num_els        , pppl_pars[PPPL_RATE4].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_rate4_num_els        , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_rate4_num_els        , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_ref4_num_els         , pppl_pars[PPPL_REF4].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_ref4_num_els         , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_ref4_num_els         , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, pppl_duration4_num_els    , pppl_pars[PPPL_DURATION4].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, pppl_duration4_num_els    , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, pppl_duration4_num_els    , sizeof(uintptr_t));

    // CUBEXP

    refMgrInitCubexpPars(&ref_mgr,
                         &transactional.cubexp,
                        0,
                        sizeof(transactional));

    // CUBEXP num_els parameters are stored in a contiguous array 2D, hence the sub_sel_step and cyc_sel_step values need to be overridden

    // refMgrFgParInitPointer   (&ref_mgr, cubexp_ref_num_els        , cubexp_pars[CUBEXP_REF].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, cubexp_ref_num_els        , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, cubexp_ref_num_els        , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, cubexp_rate_num_els       , cubexp_pars[CUBEXP_RATE].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, cubexp_rate_num_els       , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, cubexp_rate_num_els       , sizeof(uintptr_t));

    // refMgrFgParInitPointer   (&ref_mgr, cubexp_time_num_els       , cubexp_pars[CUBEXP_TIME].num_elements);
    // refMgrFgParInitSubSelStep(&ref_mgr, cubexp_time_num_els       , sizeof(uintptr_t) * CC_NUM_CYC_SELS);
    // refMgrFgParInitCycSelStep(&ref_mgr, cubexp_time_num_els       , sizeof(uintptr_t));

    // TABLE - Not a sub_sel function

    refMgrFgParInitPointer   (&ref_mgr, table_function            , transactional.table.function);
    // refMgrFgParInitCycSelStep(&ref_mgr, table_function            , table_pars[TABLE_FUNCTION].cyc_sel_step);

    refMgrFgParInitPointer   (&ref_mgr, table_function_num_els    , &transactional.table.num_elements);
    // refMgrFgParInitCycSelStep(&ref_mgr, table_function_num_els    , sizeof(uintptr_t));

    // TRIM

    refMgrInitTrimPars(&ref_mgr,
                       &transactional.trim,
                        0,
                        sizeof(transactional));

    // TEST

    refMgrInitTestPars(&ref_mgr,
                       &ccpars_test[0][0],
                        0,
                        sizeof(struct REF_test_pars));

    // PRBS

    refMgrInitPrbsPars(&ref_mgr,
                       &transactional.prbs,
                        0,
                        sizeof(transactional));

    // Initialize links from libref function generator parameter indexes to ccrt parameter structures

    // ccrun.fg_pars_link[REF_FG_PAR_REF_FG_TYPE          ] = &ref_pars[REF_PAR_FG_TYPE]         ;  // Function type.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_INITIAL_REF     ] = &ramp_pars[RAMP_INITIAL_REF]       ;  // Initial reference for a RAMP when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_FINAL_REF       ] = &ramp_pars[RAMP_FINAL_REF]         ;  // Final reference for a RAMP.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_ACCELERATION    ] = &ramp_pars[RAMP_ACCELERATION]      ;  // RAMP acceleration.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_DECELERATION    ] = &ramp_pars[RAMP_DECELERATION]      ;  // RAMP deceleration.
    // ccrun.fg_pars_link[REF_FG_PAR_RAMP_LINEAR_RATE     ] = &ramp_pars[RAMP_LINEAR_RATE]       ;  // Maximum linear rate during the RAMP.
    // ccrun.fg_pars_link[REF_FG_PAR_PULSE_REF            ] = &pulse_pars[PULSE_REF]             ;  // PULSE reference.
    // ccrun.fg_pars_link[REF_FG_PAR_PULSE_DURATION       ] = &pulse_pars[PULSE_DURATION]        ;  // PULSE duration.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_INITIAL_REF     ] = &plep_pars[PLEP_INITIAL_REF]       ;  // Initial reference for a PLEP when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_FINAL_REF       ] = &plep_pars[PLEP_FINAL_REF]         ;  // Final reference for a PLEP.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_ACCELERATION    ] = &plep_pars[PLEP_ACCELERATION]      ;  // PLEP acceleration.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_LINEAR_RATE     ] = &plep_pars[PLEP_LINEAR_RATE]       ;  // Maximum linear rate during the PLEP.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_EXP_TC          ] = &plep_pars[PLEP_EXP_TC]            ;  // PLEP exponential segment time constant.
    // ccrun.fg_pars_link[REF_FG_PAR_PLEP_EXP_FINAL       ] = &plep_pars[PLEP_EXP_FINAL]         ;  // PLEP exponential segment asymptote.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_INITIAL_REF     ] = &pppl_pars[PPPL_INITIAL_REF]       ;  // Initial reference for a PPPL when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION1   ] = &pppl_pars[PPPL_ACCELERATION1]     ;  // Array of initial PPPL accelerations.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION2   ] = &pppl_pars[PPPL_ACCELERATION2]     ;  // Array of second PPPL accelerations.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_ACCELERATION3   ] = &pppl_pars[PPPL_ACCELERATION3]     ;  // Array of third PPPL accelerations.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_RATE2           ] = &pppl_pars[PPPL_RATE2]             ;  // Array of rates at the start of the second PPPL accelerations.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_RATE4           ] = &pppl_pars[PPPL_RATE4]             ;  // Array of rates at the start of the linear PPPL segments.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_REF4            ] = &pppl_pars[PPPL_REF4]              ;  // Array of references at the start of the linear PPPL segments.
    // ccrun.fg_pars_link[REF_FG_PAR_PPPL_DURATION4       ] = &pppl_pars[PPPL_DURATION4]         ;  // Array of durations of the fourth linear linear PPPL segments.
    // ccrun.fg_pars_link[REF_FG_PAR_CUBEXP_REF           ] = &cubexp_pars[CUBEXP_REF]           ;  // Array of cubexp start/end references.
    // ccrun.fg_pars_link[REF_FG_PAR_CUBEXP_RATE          ] = &cubexp_pars[CUBEXP_RATE]          ;  // Array of cubexp start/end rates.
    // ccrun.fg_pars_link[REF_FG_PAR_CUBEXP_TIME          ] = &cubexp_pars[CUBEXP_TIME]          ;  // Array of cubexp start/end times.
    // ccrun.fg_pars_link[REF_FG_PAR_TABLE_FUNCTION       ] = &table_pars[TABLE_FUNCTION]        ;  // TABLE function points array.
    // ccrun.fg_pars_link[REF_FG_PAR_TRIM_INITIAL_REF     ] = &trim_pars[TRIM_INITIAL_REF]       ;  // Initial reference for a TRIM when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_TRIM_FINAL_REF       ] = &trim_pars[TRIM_FINAL_REF]         ;  // Final reference for a TRIM.
    // ccrun.fg_pars_link[REF_FG_PAR_TRIM_DURATION        ] = &trim_pars[TRIM_DURATION]          ;  // Duration for a TRIM. Set to zero to be limited only by the rate of change limit.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_INITIAL_REF     ] = &test_pars[TEST_INITIAL_REF]       ;  // Initial reference for a TEST when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_AMPLITUDE_PP    ] = &test_pars[TEST_AMPLITUDE_PP]      ;  // TEST function peak-peak amplitude.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_PERIOD          ] = &test_pars[TEST_PERIOD]            ;  // TEST function period.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_NUM_PERIODS     ] = &test_pars[TEST_NUM_PERIODS]       ;  // Number of TEST function periods to play.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_WINDOW          ] = &test_pars[TEST_WINDOW]            ;  // Control of TEST function half-period sine window.
    // ccrun.fg_pars_link[REF_FG_PAR_TEST_EXP_DECAY       ] = &test_pars[TEST_EXP_DECAY]         ;  // Control of TEST function exponential amplitude delay.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_INITIAL_REF     ] = &prbs_pars[PRBS_INITIAL_REF]       ;  // Initial reference for a PRBS when cycling. Ignored when arming in IDLE state.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_AMPLITUDE_PP    ] = &prbs_pars[PRBS_AMPLITUDE_PP]      ;  // PRBS peak-peak amplitude.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_PERIOD_ITERS    ] = &prbs_pars[PRBS_PERIOD_ITERS]      ;  // PRBS period in iterations.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_NUM_SEQUENCES   ] = &prbs_pars[PRBS_NUM_SEQUENCES]     ;  // Number of PRBS sequences to play.
    // ccrun.fg_pars_link[REF_FG_PAR_PRBS_K               ] = &prbs_pars[PRBS_K]                 ;  // PRBS K factor. Sequence length is 2^K - 1 periods.

    // Initialize memory for error, status and armed structures

    struct FG_error       * const fg_error   = (struct FG_error       *) calloc(CC_NUM_CYC_SELS * CC_NUM_SUB_SELS, sizeof(struct FG_error      ));
    struct REF_cyc_status * const cyc_status = (struct REF_cyc_status *) calloc(CC_NUM_CYC_SELS * CC_NUM_SUB_SELS, sizeof(struct REF_cyc_status));
    struct REF_armed      * const ref_armed  = (struct REF_armed      *) calloc(CC_NUM_CYC_SELS * CC_NUM_SUB_SELS, sizeof(struct REF_armed     ));

    // TABLE function does not support sub_sel in ccrt, so get memory only for CC_NUM_CYC_SELS - note that libref can support [sub_sel][cyc_sel] tables

    struct FG_point * const armed_table_function = (struct FG_point *) calloc(REF_ARMED_TABLE_FUNCTION_LEN(CC_NUM_CYC_SELS, 1, TABLE_LEN), sizeof(struct FG_point));

    // ILC cycle data store

    struct REF_ilc_cyc_data * const ilc_cyc_data_store = (struct REF_ilc_cyc_data *) calloc(REF_ILC_CYC_DATA_STORE_NUM_ELS(CC_ILC_MAX_SAMPLES,CC_MAX_CYC_SEL),sizeof(struct REF_ilc_cyc_data));

    // Running functions are triple buffered (two nexts and active) so allocated three more table buffers

    struct FG_point * const running_table_function = (struct FG_point *) calloc(REF_RUNNING_TABLE_FUNCTION_LEN(TABLE_LEN), sizeof(struct FG_point));

    if(fg_error               == NULL ||
       cyc_status             == NULL ||
       ref_armed              == NULL ||
       armed_table_function   == NULL ||
       running_table_function == NULL ||
       ilc_cyc_data_store     == NULL)
    {
        ccRtPrintErrorAndExit("Memory allocation failure during libref initialization");
    }

    // Initialize the ref manager

    int32_t err_num =  refMgrInit(&ref_mgr,
                                  &polswitch_mgr,
                                  &reg_mgr,
                                  nullptr,
                                  fg_error,
                                  cyc_status,
                                  ref_armed,
                                  armed_table_function,
                                  running_table_function,
                                  ilc_cyc_data_store,
                                  CC_NUM_CYC_SELS,
                                  TABLE_LEN,
                                  CC_ILC_MAX_SAMPLES,
                                  4);                           // ref_to_tc_limit

    if(err_num < 0)
    {
        // Report error code

        ccRtPrintErrorAndExit("refMgrInit() reports error number %d", err_num);
    }
    else if(err_num > 0)
    {
        // Report NULL parameter pointer

        ccRtPrintErrorAndExit("refMgrInit() reports FG parameter %d has a NULL pointer", err_num);
    }

    // Initialize fg_par_idx array

    enum REF_fg_par_idx * fg_pars_idx = &static_fg_pars_idx[0][0][0];
    uint32_t              len = sizeof(static_fg_pars_idx) / sizeof(enum REF_fg_par_idx);

    while(len-- > 0)
    {
        *(fg_pars_idx++) = REF_FG_PAR_NULL;
    }
}

void ccInitLogging(void)
{
    // Initialize the liblog structures with a pattern to check that logStructsInit() correctly resets them to zero

    memset(&log_mgr,     0x55, sizeof(log_mgr));
    memset(&log_structs, 0xAA, sizeof(log_structs));
    memset(&log_buffers, 0xAA, sizeof(log_buffers));

    // Initialize liblog structures (log_mgr, log_structs and log_read) for this application

    logStructsInit(&log_mgr, &log_structs, &log_buffers);     // logStructsInit() is generated automatically from def/log.csv

    // Initialize log_read structure

    memset(&log_read, 0x55, sizeof(log_read));

    logReadInit(&log_read, &log_structs, &log_buffers);                  // logReadInit() is generated automatically from def/log.csv

    // Initialize liblog menus (log_menus) for this application

    logMenusInit(&log_mgr, &log_menus, 0);     // logMenusInit() is generated automatically from def/log.csv

    // Change the name of two logs to test the logChangeMenuName()
    // which is generated automatically from def/log.csv

//    logChangeMenuName(&log_menus, LOG_MENU_CODIM, "CODIM1");
//    logChangeMenuName(&log_menus, LOG_MENU_DCDIM, "DCDIM1");

    // Change the name and/or units of signals in the TEMP log to test logPrepareSetSigNameAndUnitsRequest()

//    ccLogSetSigNamesAndUnits(LOG_TEMP, LOG_TEMP_EXT_3,       "INTERNAL_TEMP", "C");
//    ccLogSetSigNamesAndUnits(LOG_TEMP, LOG_TEMP_EXT_4,       "DCCT_A_TEMP",    "");
//    ccLogSetSigNamesAndUnits(LOG_TEMP, LOG_TEMP_DCCT_B_TEMP, "",              "C");

    // Set number of samples per cycle in the discontinuous DIM logs

//    ccrun.dim.dc_num_samples = log_structs.log[LOG_DCDIM].num_samples_per_sig / 2;
//    ccrun.dim.dc_counter     = ccrun.dim.dc_num_samples;

    // Adjust log_mgr periods which are defined in the log.ods file assuming an iter_period of 1000

//    for(uint32_t log_index = 0 ; log_index < LOG_NUM_LOGS ; log_index++)
//    {
//        log_mgr.period_us[log_index] = (log_mgr.period_us[log_index] * timer.iter_period_us) / CC_ITER_PERIOD_US;
//    }

    // Adjust polarity switch periods - they are defined by the event log period in the converter CLOCK file

//    log_mgr.period_us[LOG_POLSWITCH]   = timer.iter_period_us * timer.event_log_period_iters;
//    log_mgr.period_us[LOG_POLSWIFLAGS] = log_mgr.period_us[LOG_POLSWITCH];
}

static CC_us_time iter_time;
static CC_us_time state_start_time;

extern "C" bool cctestTimeInStateElapsed(double time_sec)
{
    return ccUsTimeToDouble(&iter_time) - ccUsTimeToDouble(&state_start_time) >= time_sec;
}

template <typename T>
double show_stats(std::vector<T> const& vec) {
    // Compute statistics
    // https://stackoverflow.com/a/12405793
    double sum = std::accumulate(std::begin(vec), std::end(vec), 0.0);
    double m =  sum / vec.size();

    double accum = 0.0;
    std::for_each (std::begin(vec), std::end(vec), [&](const double d) {
        accum += (d - m) * (d - m);
    });

    double stdev = sqrt(accum / (vec.size()-1));

    printf("Mean: %14.6f\t", m);
    printf("Ssd:  %14.6f\t", stdev);

    const auto [min, max] = std::minmax_element(std::begin(vec), std::end(vec));

    printf("Min:  %7g\t", (double) *min);
    printf("Max:  %7g\n", (double) *max);

    return m;
}

#define SCRIPT_int(s) script_##s
#define SCRIPT(s) SCRIPT_int(s)
#define SCRIPT_NAME_int(s) #s
#define SCRIPT_NAME(s) SCRIPT_NAME_int(s)

extern "C" void ccmain()
{
    printf("INFO built=%s %s use_liblog=%d script=%s\r\n", __DATE__, __TIME__, USE_LIBLOG, SCRIPT_NAME(USE_SCRIPT));

    auto& script = SCRIPT(USE_SCRIPT);

    std::array<SingleBenchmarkSummary, 100> summaries {};

    // do a few dry runs
    ccBenchmarkSingle(script, false);
    ccBenchmarkSingle(script, false);
    ccBenchmarkSingle(script, false);

    for (auto& summary : summaries) {
        summary = ccBenchmarkSingle(script, false);

//        printf("Time: %lu nsec for %d iterations\n", summary.total_ns, summary.num_iterations);
//        printf("   => %lu nsec per iteration\n", summary.total_ns / summary.num_iterations);
//
//        printf("   => %lu nsec min\n", summary.fastest_iter_ns);
//        double frequency_khz = 1'000'000.0 / (double) summary.slowest_iter_ns;
//        printf("   => %lu nsec max @ iter %d / %.2f sec\t=> iteration frequency: %.3f kHz\n",
//               summary.slowest_iter_ns,
//               summary.slowest_iter_num,
//               summary.slowest_iter_sim_time,
//               frequency_khz);
    }

    ccBenchmarkSingle(script, true);

    // Compute statistics

    int64_t total_ns = 0;           // 2^63 nanoseconds ~ 292 years... we will be fine.
    int num_iterations = 0;
    int64_t slowest_iter_ns = INT64_MIN;
    int slowest_iter_run = -1;
    int slowest_iter_num = -1;
    double slowest_iter_sim_time = 999;
    SingleIterProfile slowest_profile {};

    std::vector<int> slowest_iter_ns_by_run;
    std::vector<double> slowest_iter_freq_by_run;

    for (auto& summary : summaries) {
        total_ns += summary.total_ns;
        num_iterations += summary.num_iterations;

        if (slowest_iter_ns < summary.slowest_iter_ns) {
            slowest_iter_ns = summary.slowest_iter_ns;
            slowest_iter_run = &summary - &summaries[0];        // ouch
            slowest_iter_num = summary.slowest_iter_num;
            slowest_iter_sim_time = summary.slowest_iter_sim_time;
            slowest_profile = summary.slowest_profile;
        }

        slowest_iter_ns_by_run.push_back(summary.slowest_iter_ns);
        slowest_iter_freq_by_run.push_back(1'000'000.0 / (double) summary.slowest_iter_ns);

        printf("RUN;%ld;%ld;%d\n", summary.total_ns / summary.num_iterations, summary.slowest_iter_ns, summary.slowest_iter_num);
    }

    printf("\n\nExecuted %d runs.\n\n", (int) summaries.size());
    printf("Time: %.6f sec for %d iterations\n", (double)total_ns / 1.0e9, num_iterations);
    printf("   => %lu nsec per iteration\n", total_ns / num_iterations);

//    printf("   => %lu nsec min\n", summary.fastest_iter_ns);
    double frequency_khz = 1'000'000.0 / (double) slowest_iter_ns;
    printf("   => %lu nsec max @ iter %d:%d / %.2f sec\t=> iteration frequency: %.3f kHz\n",
           slowest_iter_ns,
           slowest_iter_run, slowest_iter_num,
           slowest_iter_sim_time,
           frequency_khz);
    printf("RESULT slowest_iter_ns %ld\n", slowest_iter_ns);

    printf("adc[%5d] time+sig[%5d] regMgrMeasX[%5d] refRtRegulationRT[%5d] refRtStateRT[%5d] intlk[%5d] ccRunLogSignals[%5d] dac[%5d]\n\n",
            slowest_profile.adc,
            slowest_profile.time_sig,
            slowest_profile.regMgrMeasX,
            slowest_profile.refRtRegulationRT,
            slowest_profile.refRtStateRT,
            slowest_profile.intlk,
            slowest_profile.ccRunLogSignals,
            slowest_profile.dac);
    printf("\nSTATS FOR slowest_iter_ns_by_run:\n");
    auto the_mean = show_stats(slowest_iter_ns_by_run);
    printf("RESULT mean_iter_ns %g\n", the_mean);

    printf("\nSTATS FOR slowest_iter_freq_by_run:\n");
    the_mean = show_stats(slowest_iter_freq_by_run);
    printf("RESULT mean_iter_freq %g\n", the_mean);

    printf("\nEND\n");
}

static SingleBenchmarkSummary ccBenchmarkSingle(CCRT2C_script const& script, bool save_log)
{
    ccInitLibreg(script.iter_period_us);

#if 0
    // Initialise constant parameter values (must be done after call to regMgrInit)

    regMgrParAppValue(&reg_pars, PC_ACTUATION)       = REG_VOLTAGE_REF;
    regMgrParAppValue(&reg_pars, PC_GAIN)            = 0.1f;
    regMgrParAppValue(&reg_pars, MEAS_V_DELAY_ITERS) = 0;

    // Set default value for I_SAT_GAIN for all loads

    for(uint32_t load_select = 0 ; load_select < REG_NUM_LOADS ; load_select++)
    {
        regMgrParAppValue(&reg_pars, LOAD_I_SAT_GAIN)[load_select] = 1.0;
    }

    // regMgrParAppValue(&reg_pars, LIMITS_POS)[0] = 5.0f;

    regMgrParAppValue(&reg_pars, LIMITS_V_POS)[0] = 5.0f;
    regMgrParAppValue(&reg_pars, LIMITS_V_NEG)[0] = -5.0f;
    regMgrParAppValue(&reg_pars, LIMITS_V_RATE)   = 20.0f;      // what units?

    // firing reg configuration
    regMgrParAppValue(&reg_pars, VFILTER_V_MEAS_SOURCE) = REG_MEASUREMENT;
    // regMgrParAppValue(&reg_pars, VREG_EXT_K_P) = 1.0f; DOESN'T WORK
    regMgrParGetValue(&reg_mgr, VREG_EXT_K_P) = 1.0f;

    // reg_mgr.par_groups_mask |= REG_PAR_GROUP_V_LIMITS_REF;
    reg_mgr.par_groups_mask |= REG_PAR_GROUP_VFILTER;           // force initialization of k_w
#endif
    // Link libreg to liblog to keep the current and field regulation log periods correct

    // ?? regMgrInitRegPeriodPointers(&reg_mgr, &log_mgr.period_us[LOG_B_REG], &log_mgr.period_us[LOG_I_REG]);

    // Set default parameter values

    // regMgrParAppValue(&reg_pars, HARMONICS_AC_PERIOD_ITERS) = ac_period_iters;

    regMgrParsCheck(&reg_mgr);
#if 0
    regMgrModeSetRT(&reg_mgr, REG_VOLTAGE, false);
#endif
    fprintf(stderr, "initialized reg_mgr; reg_mode: %d\n", reg_mgr.reg_mode);

    ccInitLibref();

    // Now that libref parameters have been initialized, we can initialize the signal structures

    sigStructsInit(&sig_struct, &polswitch_mgr);

    // Init liblog

    ccInitLogging();

    // Libintlk
    struct INTLK_mgr intlk_mgr;
    struct INTLK_pars intlk_pars;
    struct INTLK_latch_values intlk_latch_values;

    memset(&intlk_mgr, 0, sizeof(struct INTLK_mgr));
    memset(&intlk_pars, 0, sizeof(intlk_pars));
    memset(&intlk_latch_values, 0, sizeof(intlk_latch_values));

    // Initialize the libintlk library

    uint32_t interlock_sub_sel = 0;
    uint32_t interlock_cyc_sel = 0;

    uint32_t dummy_state_mask = 1;

    intlkInit(&intlk_mgr,
              &interlock_sub_sel,
              &interlock_cyc_sel,
              &dummy_state_mask,
              regMgrVarGetPointer(&reg_mgr, REG_ABS_ERR),
              regMgrVarGetPointer(&reg_mgr, MEAS_V_UNFILTERED),
              regMgrVarGetPointer(&reg_mgr, MEAS_I_UNFILTERED),
              regMgrVarGetValue(&reg_mgr, ITER_PERIOD),
              &intlk_pars,
              0,
              &intlk_latch_values,
              0,
              dummy_state_mask);

    intlkChanParGetValue(&intlk_pars, CHAN_CONTROL, 0) = INTLK_CHAN_CONDITIONAL;
    intlkChanParGetValue(&intlk_pars, CHAN_CONTROL, 1) = INTLK_CHAN_CONDITIONAL;
    intlkChanParGetValue(&intlk_pars, CHAN_CONTROL, 2) = INTLK_CHAN_CONDITIONAL;
    intlkChanParGetValue(&intlk_pars, CHAN_CONTROL, 3) = INTLK_CHAN_CONDITIONAL;

    intlkRT(&intlk_mgr, true, false);

    // Init state machine
    auto state = script.init();

    iter_time = {};
    state_start_time = iter_time;

//    refMgrParSetValue(&ref_mgr, MODE_REF,     REF_IDLE);
//    refMgrParSetValue(&ref_mgr, PC_STATE,     REF_PC_ON);
//    refMgrParSetValue(&ref_mgr, DIRECT_V_REF, 1.23f);

    // TODO sig.csv: assign stuck_limit_p -> NULL
    // TODO sig.csv: assign cal_num_samples_p -> NULL
    // TODO sig.csv: assign primary_turns_p -> NULL

    Benchmark perf;

    const bool verbose = false;
    const bool ascii_plot = false;

    [[maybe_unused]] bool worth_reporting = true;

#ifndef __aarch64__
    std::ofstream logf("log.csv");
    logf << "time,I_ref\n";
#endif

    SingleBenchmarkSummary summary {
        .fastest_iter_ns = INT64_MAX,
        .slowest_iter_ns = INT64_MIN,
        .total_ns = 0,
        .slowest_iter_num = -1,
    };

    sig_cpu_usage_us = 0;

    int i;

    for (i = 0; state != nullptr; i++) {

//        if (i == 8'000'000 / ITER_PERIOD_US) {
//            __debugbreak();
//        }

        /*
         * ccrt order of operations:
         *  - (sync real-time threads)
         *  - increase iter_time
         *  - (simulate super-cycle)
         *  - (simulate circuit)
         *  - (simulate ADC)
         *  - call sigMgrRT
         *  - Set measurements in libreg
         *  - Run CCLIBS real-time activity for this iteration
         *  - (Run the ILC state machine)
         *  - Store all relevant signals in logs
         *  - Simulate PC state
         *  - Wake up the event log thread at the specified rate
         */

        auto new_state = (StateFunc_t) state();

        if (i == 0 || new_state != state) {
            state_start_time = iter_time;
            state = new_state;
//            if (!ascii_plot) {
//                printf("NEW STATE\n");
//            }
            worth_reporting = true;

            // conservatively do these after every state update
            regMgrParsCheck(&reg_mgr);
            logCacheSelectors(&log_structs);
        }

        if (ascii_plot) {
            if (i % (100'000 / script.iter_period_us) == 0) {
                int offset = 80;
                float scale = 80.0f / 10.0f;

                auto val = offset + (int)floor(reg_mgr.i.ref_limited * scale);
                for (int j = 0; j < val; j++) {
                    printf(" ");
                }
                printf("@\n");
            }
        }
        else {
//            if (worth_reporting || i % (1'000'000 / ITER_PERIOD_US) == 0) {
//                printf("Iteration %6d\ttime [%5u.%06u]\tref.PC_STATE=%d\tref_mode=%u\treg-mode=%u\tref_state=%u\treg_mgr.i.ref_limited=%9f\n",
//                       i, iter_time.s, iter_time.us,
//                       refMgrParGetValue(&ref_mgr, PC_STATE),
//                       ref_mgr.iter_cache.ref_mode,
//                       ref_mgr.iter_cache.reg_mode,
//                       ref_mgr.ref_state,
//                       reg_mgr.i.ref_limited
//                );
//                worth_reporting = false;
//            }
        }

        // conservatively do this EVERY ITERATION
        // we had a problem where I LIMITS would not get set
//        regMgrParsCheck(&reg_mgr);

        if (verbose) {
            printf("\n");
        }

        SingleIterProfile prof;

        // BEGIN REAL-TIME SECTION
        perf.start();
        auto t_0 = get_monotonic_ns();

        // simulate ADC reads by re-reading timer register 4x
#if defined(_DIOT) && USE_FPGA_ACCESS
        __asm__ volatile("dsb sy");
        *(volatile uint32_t*)0xa000'0040;
        *(volatile uint32_t*)0xa000'0044;
        *(volatile uint32_t*)0xa000'0048;
        *(volatile uint32_t*)0xa000'004c;
//        *(volatile uint32_t*)0xa000'019c;
#else
        fpga_dummy_read();
        fpga_dummy_read();
        fpga_dummy_read();
        fpga_dummy_read();
#endif

        auto t_adc = get_monotonic_ns();

        bool iter_1s_boundary = false;
        iter_time.us += script.iter_period_us;

        if(iter_time.us >= 1'000'000)
        {
            iter_time.s++;
            iter_time.us -= 1'000'000;
            iter_1s_boundary = true;
        }

        // Feed measurements

//        reg_mgr.inputs.v_meas.meas.signal      = -0.0123f;  // or reg_mgr->sim_vars.v_meas;
//                                                            // Ignored in open-loop mode anyhow.
//
//        reg_mgr.inputs.v_meas.meas.is_valid    = true;

        // Start real-time iteration by registering new acquisitions
        // (regMgrMeasSetRT returns an "iteration index", but we don't care about it)

//        regMgrMeasSetRT(&reg_mgr,
//                        REG_OPERATIONAL_RST_PARS,       /* does this even apply for REG_VOLTAGE? */
//                        &iter_time,
//                        false,
//                        false);

        // This sets the actual Vref that we expect to see on the output

//        float ref = 1.0f + i * 0.01f;

        assert(200'000 >= script.iter_period_us);
        assert(200'000 % script.iter_period_us == 0);
        bool iter_200ms_boundary = (i == 200'000 / script.iter_period_us);

        sigMgrRT(&sig_struct.mgr, iter_200ms_boundary, &iter_time);

        auto t_time_sig = get_monotonic_ns();

        // Set measurements in libreg
        // TODO: why is this done 'by hand'? is this like that in classes as well?

        regMgrMeasSetBmeasRT   (&reg_mgr, sigVarGetValue(&sig_struct, transducer, b_probe , TRANSDUCER_MEAS_SIGNAL));
        regMgrMeasSetImeasRT   (&reg_mgr, sigVarGetValue(&sig_struct, select    , i_meas  , TRANSDUCER_MEAS_SIGNAL));
//        regMgrMeasSetImagSatRT (&reg_mgr, ccpars_sim.i_mag_sat);
        regMgrMeasSetIcapaRT   (&reg_mgr, sigVarGetValue(&sig_struct, transducer, i_probe , TRANSDUCER_MEAS_SIGNAL));
        regMgrMeasSetVmeasRT   (&reg_mgr, sigVarGetValue(&sig_struct, transducer, v_probe , TRANSDUCER_MEAS_SIGNAL));
        regMgrMeasSetVacRT     (&reg_mgr, sigVarGetValue(&sig_struct, transducer, v_ac    , TRANSDUCER_MEAS_SIGNAL));

        auto t_regMgrMeasSetX = get_monotonic_ns();

        // Run CCLIBS real-time activity for this iteration

        refRtRegulationRT(&ref_mgr, &iter_time);

        auto t_refRtRegulationRT = get_monotonic_ns();

        refRtStateRT(&ref_mgr);

        auto t_refRtStateRT = get_monotonic_ns();

        // Run libintlk

        intlkRT(&intlk_mgr, false, false);

        auto t_intlk = get_monotonic_ns();

        // Store all relevant signals in logs

//        if (i < kNumIterations) {
//            iter_log[i] = IterationLogEntry{.ref = ref_mgr.ref_now, .ref_actuation = reg_mgr.ref_actuation};
//        }

//        logStoreContinuousRT(&log_structs.log[LOG_ACQ], ccUsTimeToDouble(&iter_time));

#if USE_LIBLOG
        ccRunLogSignals(iter_1s_boundary, iter_200ms_boundary, ccUsTimeToDouble(&iter_time));
#endif

        auto t_ccRunLogSignals = get_monotonic_ns();

        // simulate DAC writes by re-reading timer register 2x
#if defined(_DIOT) && USE_FPGA_ACCESS
        *(volatile uint32_t*)0xa000'0140 = 0xAABBCCDD;
        *(volatile uint32_t*)0xa000'0144 = 0x11223344;
#else
        fpga_dummy_read();
        fpga_dummy_read();
#endif

        auto t_dac = get_monotonic_ns();

        perf.end();

        // END REAL-TIME SECTION

        prof.adc = t_adc - t_0;
        prof.time_sig = t_time_sig - t_adc;
        prof.regMgrMeasX = t_regMgrMeasSetX - t_time_sig;
        prof.refRtRegulationRT = t_refRtRegulationRT - t_regMgrMeasSetX;
        prof.refRtStateRT = t_refRtStateRT - t_refRtRegulationRT;
        prof.intlk = t_intlk - t_refRtStateRT;
        prof.ccRunLogSignals = t_ccRunLogSignals - t_intlk;
        prof.dac = t_dac - t_ccRunLogSignals;

#ifndef __aarch64__
        logf << ccUsTimeToDouble(&iter_time) << "," << reg_mgr.i.ref_limited << "\n";
#endif

        // Simulate PC state
        // (this is suffixed -RT, but will be done by hardware in practice)

        ccSimPcStateRT();

        if (verbose) {
//        printf("ref state: %d\n", refMgrParGetValue(&ref_mgr, ...));
            printf("PC state: %d\n", refMgrParGetValue(&ref_mgr, PC_STATE));
            printf("REF state: %d\n", ref_mgr.ref_state);
//        printf("actuation mode: %d (expect REG_FIRING_REF aka 2)\n", regMgrParGetValue(&reg_mgr, PC_ACTUATION));
            printf("ref_reg: %f\n", reg_mgr.v.ref_reg);
            printf("ref_rate: %f\n", reg_mgr.ref_rate);
            printf("v.ref: %f\n", reg_mgr.v.ref); // (rate-limited reference)
            printf("v_reg_err: %f\n", reg_mgr.v.reg_vars.v_reg_err);
            printf("ref_actuation: %f\n", reg_mgr.ref_actuation);
            printf("ref_dac: %f\n", ref_mgr.ref_dac);
        }
/*
        if (ref_mgr.ref_state == REF_IDLE) {
            // BEGIN configure and execute ramp function
            // state must be already IDLE. always have to ARM first, then RUN
            transactional.ramp.final_ref[0] = 1.98;

            REF_fg_par_idx thing[REF_FG_PAR_MAX_PER_GROUP];

//            refMgrFgParGetValueSubCyc(ref_mgr, sub_sel, cyc_sel, REF_FG_TYPE)

            transactional.ramp.acceleration[0] = 1.0f;
            transactional.ramp.linear_rate[0] = 1.0f;
            transactional.ramp.deceleration[0] = 1.0f;

            refMgrFgParSetValue(&ref_mgr, REF_FG_TYPE, FG_RAMP);

            //shoudl be OK to not pass pars...
            enum FG_errno fg_errno = refArm(&ref_mgr,
                                            0,                                // sub_sel
                                            0,
                                            CC_DISABLED,
                                            0,
                                            nullptr,
                                            thing);

            if (fg_errno != 0) {
                // Report error code

                ccRtPrintErrorAndExit("refArm() reports error number %d", fg_errno);
            }
            // END
        }

        if (ref_mgr.ref_state == REF_ARMED) {
//            struct CC_us_time run_time = {0};
            struct CC_us_time run_time = ref_mgr.iter_time;

            run_time.us += 1;

            if(run_time.us >= 1'000'000)
            {
                run_time.s++;
                run_time.us -= 1'000'000;
            }

            refMgrParSetValue(&ref_mgr, REF_RUN, run_time);

            refEventRun(&ref_mgr);
        }
*/

        // Perf stats
        auto ns = perf.get_ns();
        sig_cpu_usage_us = (float)ns * 0.001f;

        if (summary.fastest_iter_ns > ns) {
            summary.fastest_iter_ns = ns;
        }
        if (summary.slowest_iter_ns < ns && i > 0) {
            summary.slowest_iter_ns = ns;
            summary.slowest_iter_num = i;
            summary.slowest_iter_sim_time = ccUsTimeToDouble(&iter_time);
            summary.slowest_profile = prof;
        }
        summary.total_ns += ns;
    }

    // Print out log

    /*
    printf("\n\n");
    printf("time,ref,ref_actuation\n");

    for (int i = 0; i < kNumIterations; i++) {
        auto const& e = iter_log[i];
        printf("%2d,%f,%f\n", i, e.ref, e.ref_actuation);
    }
    */

//    perf.report(kNumIterations);

    summary.num_iterations = i;

#ifndef _FGC32
    if (save_log) {
        ccLogWriteSpy();
    }
#endif

    return summary;
}



static void ccSimPcStateRT(void)
{
    // Fault state if polarity switch fault is latched or When ref_state is OFF and a libref fault is active

    if(   polswitchVarGetValue(&polswitch_mgr,LATCHED_FAULTS) != 0
          || (   refMgrVarGetValue(&ref_mgr, REF_FAULTS_IN_OFF)  != 0
                 && refMgrVarGetValue(&ref_mgr, REF_STATE) == REF_OFF))
    {
        refMgrParSetValue(&ref_mgr, PC_STATE, REF_PC_FAULT);
    }
    else
    {
        // Delay switching on but not switching off

        if(ccpars_mode.pc != refMgrParGetValue(&ref_mgr,PC_STATE))
        {
            ccsim.pcstate.timer += regMgrVarGetValue(&reg_mgr, ITER_PERIOD);

            if(ccpars_mode.pc == REF_PC_OFF || ccsim.pcstate.timer > 1.5F)
            {
                refMgrParSetValue(&ref_mgr, PC_STATE, ccpars_mode.pc);
                ccsim.pcstate.timer = 0.0;
            }
        }
    }
}

extern "C"
void ccRtArmRef(uint32_t sub_sel, uint32_t cyc_sel, uint32_t const num_pars, cc_float const * const par_values)
{
    // Lock mutex before arming to avoid a concurrent call to refEventStartFunc() from the Super Cycle thread

#ifdef HAVE_RUNLOG
    if(ccRtStateIsInitialised() && refMgrParGetValue(&ref_mgr,MODE_USE_ARM_NOW) == CC_ENABLED)
    {
        ccRunLogEventStart(REF_EVENT_USE_ARM_NOW);
    }
#endif

    // Adjust sub_sel/cyc_sel according to MODE REF_SUB_SEL and MODE REF_CYC_SEL

    sub_sel = refMgrSubSel(&ref_mgr, sub_sel);
    cyc_sel = refMgrCycSel(&ref_mgr, cyc_sel);

    // Try to arm the reference

    auto fg_errno = refArm(&ref_mgr, sub_sel, cyc_sel, CC_DISABLED, num_pars, par_values, static_fg_pars_idx[sub_sel][cyc_sel]);

    // Check error response

    if(fg_errno != FG_OK)
    {
        struct FG_error * const fg_error = &ref_mgr.fg_error[cyc_sel + sub_sel * ref_mgr.num_cyc_sels];

        if(fg_errno == fg_error->fg_errno)
        {
            ccRtPrintErrorAndExit("Failed to arm XX for sub_sel %u cyc_sel %u: XX (%u) %g, %g, %g, %g",
//                           ccParsEnumValueToString(enum_fg_type, fg_error->fg_type),
                           sub_sel,
                           cyc_sel,
//                           ccParsEnumValueToString(enum_fgerror_errno,fg_error->fg_errno),
                           fg_error->index,
                           fg_error->data[0],
                           fg_error->data[1],
                           fg_error->data[2],
                           fg_error->data[3]);
        }
        else
        {
//            ccRtPrintError(ccParsEnumValueToString(enum_fgerror_errno,ccpars_global.fg_errno));
            ccRtPrintErrorAndExit("unexpected error %d in ccRtArmRef", (int) fg_errno);
        }
    }
}

extern "C"
uint32_t ccCmdsRun()
{
    cc_double iter_time_s;
//    cc_double offset_s;

    iter_time_s   = ccUsTimeToDouble(&iter_time);
//    offset_s = 0.0;

    // Calculate run time

    cc_double run_time_s;

    run_time_s = 0.0;

    // Bail out if the calculated run time is in the past

    cc_double delta_time = 0.0;

    if(run_time_s > 0.0)
    {
        cc_double delta_time = iter_time_s - run_time_s;

        if(delta_time > 0.0)
        {
            ccRtPrintErrorAndExit("Run time is already %.6f s in the past", delta_time);
        }
    }

    // Issue Run event

#ifdef HAVE_RUNLOG
    ccRunLogEventStart(REF_EVENT_RUN);
#endif

    struct CC_us_time run_time;

    ccDoubleToUsTime(run_time_s, &run_time);

    refMgrParSetValue(&ref_mgr, REF_RUN, run_time);

    if(refEventRun(&ref_mgr) == false)
    {
        ccRtPrintErrorAndExit("No armed reference function is ready to run in the current state");
    }

    run_time = refMgrParGetValue(&ref_mgr, REF_RUN);

    run_time_s = ccUsTimeToDouble(&run_time);

//    ccRtPrintInVerbose("%.6f run : event_time_s=%.6f margin=%.6f",
//                       ccThreadGetIterTimeS() - ccrun.start_time_s,
//                       run_time_s - ccrun.start_time_s,
//                       delta_time);

/// ?????
//    ccRtWaitForRefStateMachine();

    return EXIT_SUCCESS;
}


#include "inc_log.hpp"
