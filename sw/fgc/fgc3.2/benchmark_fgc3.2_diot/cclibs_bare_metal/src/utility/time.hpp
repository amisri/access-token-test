#pragma once

#include <stdint.h>

static uint64_t get_monotonic_ns();

#ifdef __aarch64__
#ifdef _FGC32
extern "C" {
#include "../../common/fgc32_fpga.h"
}
#endif

static uint64_t get_monotonic_ns() {
    uint64_t cntval;
    asm volatile("mrs %0, CNTPCT_EL0" : "=r" (cntval));

#ifdef _FGC32
    return cntval * 40UL;       // assuming CNTFRQ_EL0 == 25 MHz
#elif defined(_DIOT)
    return cntval * 20UL;       // assuming CNTFRQ_EL0 == 50 MHz
#else
#error
#endif
}
#else
#include <time.h>

static uint64_t get_monotonic_ns() {
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC, &tp);
    return tp.tv_sec * 1'000'000'000ULL + tp.tv_nsec;
}
#endif
