#pragma once

#include "time.hpp"

#include <stdio.h>

class Benchmark {
public:
    void start() {
        this->start_time_ns = get_monotonic_ns();
    }

    void end() {
        this->end_time_ns = get_monotonic_ns();
    }

    int64_t get_ns() const {
        return this->end_time_ns - this->start_time_ns;
    }

    void report(int num_iterations) const {
        printf("Time: %lu nsec for %d iterations\n", (end_time_ns - start_time_ns), num_iterations);
        printf("   => %lu nsec per iteration\n", (end_time_ns - start_time_ns) / num_iterations);

        double frequency_khz = (double) num_iterations / (double)(end_time_ns - start_time_ns) * 1'000'000.0;
        printf("   => iteration frequency: %.3f kHz\n", frequency_khz);
    }

    void debug() const {
        printf("start_time_ns = %ld\n", start_time_ns);
        printf("end_time_ns   = %ld\n", end_time_ns);
    }

private:
    uint64_t start_time_ns = 0;
    uint64_t end_time_ns = 0;
};

struct SingleIterProfile {
    int adc;
    int time_sig;
    int regMgrMeasX;
    int refRtRegulationRT;
    int refRtStateRT;
    int intlk;
    int ccRunLogSignals;
    int dac;
};


struct SingleBenchmarkSummary {
    int64_t fastest_iter_ns;
    int64_t slowest_iter_ns;
    int64_t total_ns;
    int slowest_iter_num;
    double slowest_iter_sim_time;
    int num_iterations;

    SingleIterProfile slowest_profile;
};
