# FGC3.2 platform

### Content
1. Directories
2. Developing


-----------------------------------------------------


## 1. Directories

- **app** - source code and makefile to build the main application for the FGC3.2 and its classes.
- **modules** - source code and makefile to build kernel modules for the FGC3.2.
- **launcher** - source code of the launcher / start-up scripts for the FGC3.2.
- **tools** - some auxiliary scripts used to facilitate development of the FGC3.2 code (e.g. copying the compiled binaries to the FGC3.2).



-----------------------------------------------------


## 2. Developing

### Building

To build everything (except OS), execute this command in the main directory:
```
make
```

To build components selectively:
```
make app
make modules
```

You can also build particular class, for example:
```
make 73
```

The `make app` command  build the default class (73). If you want to change default class assign its number to an environmental variable `FGC32_DEF_CLASS`.

Results of the `make` commands are placed in the *bin* directory.

> **Note:** To build the firmware and the Linux Operating System, invoke make in the *fgc32OS* directory. See wiki for details.



### Local building

To build the FGC3.2 repository locally (i.e. without connecting to the technical network) you have to create the following paths on your PC:

```
/user/poccdev/projects/lib/fgc32/kernel
/user/poccdev/projects/bin/gcc/aarch64/bin
```

You then have to copy content of those directories from the technical network (for example: `ssh cs-ccr-teepc2`). The top path contains kernel headers for building kernel modules. The second path contains GCC compiler for the *aarch64* architecture.



### Quickly reprogram the FGC when developing

When you are developing and you want to test your code on the FGC3.2 device you can simply copy the *bin* directory to the FGC3.2 and restart the application.

To facilitate this task you can use a command from the *tools* directory.

**Using the command**

Make sure that the command has *execute* permissions and type:
```
./tools/fgc
```

This will try to connect to the FGC3.2 device. For convenience, you may add an alias to your *~/.bashrc* file:
```
alias fgc="~/projects/fgc/sw/fgc/fgc3.2/tools/fgc"
```

**Selecting FGC3.2 device**

To select an FGC3.2 device you want to connect to, you have to pass its hostname as the first argument. You need to add a hyphen (-) as a prefix to the hostname. For example:
```
./tools/fgc -fgc32-dev1
```

Executing this command will open an SSH session to the FGC3.2 device which hostname is ```fgc32-dev1```.

You may set an environmental variable named ```FGC32_HOST```. If you set this variable, you don't have to add *-hostname* to every command. However, if you pass hostname to the command (*-hostname*) it will overwrite the hostname from the ```FGC32_HOST``` variable.  
For example:
```
FGC32_HOST="fgc32-dev1"
export FGC32_HOST
```

It is suggested to add this assignment to the *~/.bashrc* file for convenience.

**Copy the new binaries to the FGC3.2**

Assuming the alias and the environmental variables was set, type the following command to copy the whole content of the *bin* directory:
```
fgc cp
```

You can also copy the files selectively, using:
```
fgc cp app
fgc cp modules
fgc cp launcher
fgc cp service
```

You can copy several components at once. For example, to copy *app* and *launcher** type:
```
fgc cp app launcher
```

Invoking any *copy* command will cause to stop the application, copy the selected files and restart the application.

**SSH into FGC3.2 device**

To open SSH console to the selected device. simply type:
```
fgc
```
or the full version:
```
./tools/fgc -fgc32-dev1
```

**Passwordless SSH using keys**

For convenience, you may add your Linux user's key to trusted keys on the FGC3.2 device. That way you may SSH and copy files without entering the password.

First create public key on your PC (if you don't have one already). Do not enter the password (leave blank).
```
you@yourPC:~$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/a/.ssh/id_rsa):
Created directory '/home/a/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/a/.ssh/id_rsa.
Your public key has been saved in /home/a/.ssh/id_rsa.pub.
The key fingerprint is:
3e:4f:05:79:3a:9f:96:7c:3b:ad:e9:58:37:bc:37:e4 a@A
```

Next, copy your public key into FGC3.2 device   
```
you@yourPC:~$ cat .ssh/id_rsa.pub | ssh root@fgc32-dev1 'cat >> .ssh/authorized_keys'
root@fgc32-dev1 password:
```

From now on you will be able to log into FGC3.2 with SSH commands (and *fgc* script) without typing password.

### Control and debug FGC3.2 application

Once you are connected with the FGC3.2 device via SSH, you may use a command ```fgc``` (this one is executed directly on the FGC3.2; not to be confused with the ```fgc``` command described above) to control the application, debug it, execute get/set command etc.  
To see the full list of commands and usage, type:
```
fgc help
```
in the SSH session.
