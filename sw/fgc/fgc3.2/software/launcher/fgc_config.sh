#!/bin/bash

# Paths and definitions used by other scripts.

##################################
# Generic

# Name of the executable of the main app
FGC_APP_NAME="fgcd9"

# Name of the kernel ITB file
FGC_ITB_NAME="kernel.itb"


##################################
# Paths

# Path to a device with SYSTEM partition
FGC_SD_SYS="/dev/mmcblk0p1"

# Path to a device with FGC partition
FGC_SD_FGC="/dev/mmcblk0p2"

# Path to a device with DATA partition
FGC_SD_DATA="/dev/mmcblk0p3"

# SYSTEM partition mount point
FGC_MOUNT_SYS="/mnt/SYSTEM"

# FGC partition mount point
FGC_MOUNT_FGC="/mnt/FGC"

# DATA partition mount point
FGC_MOUNT_DATA="/mnt/DATA"

    # Extensions folder on the DATA partition
    FGC_EXTENSIONS_PATH="${FGC_MOUNT_DATA}/init_extensions"

    # Codes folder on the DATA partition
    FGC_CODES_PATH="${FGC_MOUNT_DATA}/codes"

# Path to the directory containing the fallback app and modules
FGC_FALLBACK="/root"

# Path to the console that corresponds to the UART terminal
FGC_UART_PATH="/dev/ttyS0"

# Path to the error file created by the launcher in case of errors
FGC_ERROR_LOG="/root/launcherErrorLog"


##################################
# Values

# Interrupts affinity mask (7 == 0111 - it will turn off interrupt handling for Core#3)
FGC_INTERRUPT_MASK=7

# Size of the FGC partition in MB
FGC_PART_FGC=30

# Size of the DATA partition in MB
FGC_PART_FGC=100

##################################
# GPIO

# GPIO kernel pin number that corresponds to the red LED physical pin
FGC_LED_PIN="493"

# Value to set to turn off the LED (the LED is active high)
FGC_LED_OFF="0"

# Value to set to turn on the LED (the LED is active high)
FGC_LED_ON="1"

#    How to obtain Kernel GPIO number from hardware pin name?
#        You have to go to /sys/class/gpio and you should see gpiochip* files. Do ls - l you will see a full patch.
#        For example, for "gpiochip480", the path is ../../devices/platform/soc/2300000.gpio/gpio/gpiochip480
#        As 2300000 is the hardware address of GPIO1 module, the number 480 corresponds to the first GPIO pin
#        in this port (i.e. GPIO1.0).
#        Now if we want, for example, GPIO1.13, we add 480+13 = 493. 493 is the pin number used in Kernel.


##################################
# Ethernets

# Address and hardware memory address for ethernet used for Gigabit communication and SSH
FGC_ETH_A_NAME="ethA"
FGC_ETH_A_ADDR="1ae6000"

# Address and hardware memory address for ethernet used for FGC_Ether
FGC_ETH_B_NAME="ethB"
FGC_ETH_B_ADDR="1ae4000"

# EOF
