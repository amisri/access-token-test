#!/bin/bash

# This script is executed by the init system (declared in inittab) when the system starts up.
# Its purpose is to configure the environment, do checks, clean up old logs, etc.

# Change directory to a script location
cd "$(dirname "$0")"

# Import
source /bin/fgc_config.sh
source /bin/fgc_utils.sh
source /bin/fgc_leds.sh
source /bin/fgc_ethernet.sh
source /bin/fgc_reprogram.sh


################################################
# Verify that the other launcher is not running already
    # Count how many processes are there that match name of the script
    launchers_count=$(pgrep $(basename "$0") | wc -l)

    # One entry is this script, second one is subshell when calculating launchers_count.
    # So if the number is greater than 2 it means there is already another launcher running.
    if [ "$launchers_count" -gt "2" ] ; then
        echo "Another launcher script is already running. Exiting..."
        exit 1
    fi


################################################
# Clear log file
    fgcLogClear

################################################
# Configure the red LED
    fgcLine
    fgcInfo "Blink LED"

    fgcLedsInit

    # Blink the LED
    fgcLedOn
    sleep 0.5
    fgcLedOff



################################################
# Check, fix and mount a partition
    function checkPartition() # (sdPath, mountPath, partName)
    {
        # Default return value is 0
        result=0

        # Copy arguments into semantic names
        sd_path="$1"
        mount_path="$2"
        part_name="$3"

        fgcInfo "Checking partition ${part_name} at ${sd_path}"

        # Check if the partition exists at all
        if [ -e "$sd_path" ] ; then
            # If it exist, then umount if (in case the launcher had already been run)
            umount "$sd_path" 2>/dev/null

            # Then check and auto-fix
            fsck -p -f "$sd_path"

            # If the check failed, then reformat the partition
            if [ "$?" -gt "1" ] ; then
                fgcError "fsck reported error $? for the ${part_name} partition. Formatting the parition"
                result=1

                yes | mkfs.ext4 -v -L "$part_name" "$sd_path"
            fi

            # Create a folder for the mount
            mkdir -p "$mount_path"

            # Mount the partition
            mount "$sd_path" "$mount_path"
            logOnError "Failed to mount the ${part_name} partition"
        else
            fgcError "The ${part_name} partition does not exist on the SD card"
            result=1
        fi

        return $result
    }




################################################
# Check and mount the FGC partition and use fallback in case of errors
    fgcLine
    checkPartition "$FGC_SD_FGC"  "$FGC_MOUNT_FGC"  "FGC"

    # Check if the checkPartition was successful and the main application exist
    if [ "$?" -eq "0" ] && [ -f "${FGC_MOUNT_FGC}/${FGC_APP_NAME}" ] ; then
        fgc_app_path="$FGC_MOUNT_FGC"
    else
        fgc_app_path="$FGC_FALLBACK"
        fgcWarn "Using fallback application"
    fi



################################################
# Check and mount the DATA partition
    checkPartition "$FGC_SD_DATA" "$FGC_MOUNT_DATA" "DATA"



################################################
# Set interrupt affinity to isolate the selected core
    fgcLine
    fgcInfo "Setting interrupt affinity"
    echo "$FGC_INTERRUPT_MASK" > /proc/irq/default_smp_affinity
    logOnError "Failed to set interrupt affinity"


################################################
# Configure, rename and enable Ethernet interfaces
    fgcLine
    fgcInfo "Rename and configure Ethernet interfaces"

    fgcEthernetInit

    # Disable SSH
    fgcInfo "Disabling SSH service (dropbear)"
    /etc/init.d/S50dropbear stop


################################################
# Reprogram FGC and modules in case there is anything to reprogram
    fgcLine
    fgcInfo "Checking if there is something to reprogram"
    fgcReprogram


################################################
# Load kernel modules
    fgcLine
    fgcInfo "Loading Kernel modules"

    if [ -d "${fgc_app_path}" ]; then
        modules=(`find "${fgc_app_path}"/ -maxdepth 1 -name "*.ko"`)

        for f in "${modules[@]}" ; do
            fgcInfo "Loading Kernel module ${f}"

            # Unload the module (and ignore error if it is not loaded)
            rmmod "${f}" 1>/dev/null 2>&1

            # Load the module
            insmod "${f}"
            logOnError "Failed to load kernel module ${f}"
        done
    fi



################################################
# Run all extensions from the extension folder (if present)
    fgcLine
    fgcInfo "Executing all extension scripts at ${FGC_EXTENSIONS_PATH}"

    if [ -d "${FGC_EXTENSIONS_PATH}" ]; then
        extensions=(`find "${FGC_EXTENSIONS_PATH}"/ -maxdepth 1 -name "*.sh"`)
        
        for f in "${extensions[@]}" ; do
            fgcInfo "Loading extension ${f}"
            bash "${f}" -H
        done
    fi


################################################
# Finally run the main FGC(D) application and keep it running

##########
# TODO don't exit here obviously
exit 0
##########

    quick_fail_count=0

    while : ; do
        # Save the start time
        start_time=$(date +%s)

        # Run the app if it exists
        if [ -f "${fgc_app_path}/${FGC_APP_NAME}" ] ; then
            fgcInfo "Starting the main FGCD application (at ${fgc_app_path}/${FGC_APP_NAME})"
            ${fgc_app_path}/${FGC_APP_NAME}
        else
            fgcError "The main FGCD application has not been found at ${fgc_app_path}/${FGC_APP_NAME}"
            fgcError "Resetting board in 10 seconds..."
            # TODO REQUEST BOARD RESET
            exit 1 # remove it
        fi

        # Get the number of second the app was running
        running_time=$(expr $start_time - $(date +%s))

        fgcInfo "The main FGCD application has stopped"

        # If the running time was less than 5 second, increase the QUICK_FAIL counter
        if [ "$running_time" -lt "5" ] ; then
            quick_fail_count=$(expr $quick_fail_count + 1)
        fi

        # If the counter is greater than 3, start reporting it and turn on the red LED
        if [ "$quick_fail_count" -gt 3 ] ; then
            # Log it
            fgcError "The main FGCD application crashed within 5s ${quick_fail_count} times"

            # Make sure the log file doesn't increase indefinitely
            tail -1000 "$FGC_ERROR_LOG" > "$FGC_ERROR_LOG"

            # Turn on the LED
            fgcLedOn
        fi
    done


# EOF
