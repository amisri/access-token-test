#!/bin/bash

# Bash utilities for FGC3.2 start-up scripts.

# Include paths and definitions
source /bin/fgc_config.sh

# Define colors

 cBLACK="\033[1;31m"
   cRED="\033[1;31m"
 cGREEN="\033[1;32m"
cYELLOW="\033[1;33m"
  cBLUE="\033[4;36m"
   cEND="\033[39m\033[0m"


# This function should be executed at the beginning of the script to clear the log file
function fgcLogClear #()
{
    rm ${FGC_ERROR_LOG} 1>/dev/null 2>&1
}

# Print to error log file and to the stdout
function fgcPrint #(stringToPrint)
{
    echo -e "$1" >> ${FGC_ERROR_LOG}
    #echo -e "$1" >  ${FGC_UART_PATH}
    echo -e "$1"
}

# Prints line used as a separator when printing
function fgcLine
{
    echo -e "${cEND}\n--------------------------------------------------"
}

# Print error
function fgcError #(errorString)
{
    # Print error message
    fgcPrint "${cRED}FGC: Error: $1 ${cEND}"
}

function logOnError #(errorMessage)
{
    # Check if the last command exited with an error
    if [ "$?" -ne "0" ] ; then
        fgcError "$1 (exit code $?)"
    fi
}

function fgcWarn #(warningString)
{
    fgcPrint "${cYELLOW}FGC: Warning: $1 ${cEND}"
}


function fgcInfo #(infoString)
{
    fgcPrint "${cBLUE}FGC: Info: $1 ${cEND}"
}


# EOF
