#!/bin/bash

# Configures GPIO port and pin to control red boot LED, use to indicate
# serious errors that make it impossible to start the main application.

# Change directory to a script location
cd "$(dirname "$0")"

# Import
source /bin/fgc_config.sh
source /bin/fgc_utils.sh

led_dir="/sys/class/gpio/gpio${FGC_LED_PIN}"

function fgcLedsInit()
{
    # Check if the LED directory doesn't exist - it that case, export it
    if [ ! -d "$led_dir" ]; then
        echo "${FGC_LED_PIN}" > "/sys/class/gpio/export"
        logOnError "Failed to export GPIO ${FGC_LED_PIN}"
    fi

    echo "out" > "${led_dir}/direction"
    logOnError "Failed to set GPIO ${FGC_LED_PIN} direction"

    echo "${FGC_LED_OFF}" > "${led_dir}/value"
    logOnError "Failed to set GPIO ${FGC_LED_PIN} value"
}


function fgcLedOff()
{
    echo "${FGC_LED_OFF}" > "${led_dir}/value"
    logOnError "Failed to turn off the LED (GPIO ${FGC_LED_PIN})"
}


function fgcLedOn()
{
    echo "${FGC_LED_ON}" > "${led_dir}/value"
    logOnError "Failed to turn on the LED (GPIO ${FGC_LED_PIN})"
}

# EOF

