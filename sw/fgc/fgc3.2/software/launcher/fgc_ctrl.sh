#!/bin/bash

# This script is used to control (start, stop, restart) the FGC main app
# during development, via console. It is not used in production.

# Change directory to a script location
cd "$(dirname "$0")"

# Import
source /bin/fgc_config.sh

# Get the PID of the running app (or empty value if the app is not running)
APP_PID=$(pgrep ${FGC_APP_NAME})

# Print help
function printHelp #()
{
    echo -e "Usage:\n"
    echo -e "   (no argument)  Starts the app if it's not running already.\n"
    echo -e "   s start        Starts the app if it's not running already.\n"
    echo -e "   k stop         Kills the app.\n"
    echo -e "   r restart      Kills the app and starts it again.\n"

    echo -e "   h help         Shows this help."

    exit 0
}


# Start the app
function fgcStart #()
{
    ${FGC_MOUNT_FGC}/${FGC_APP_NAME}
}


# Stop the app
function fgcStop #()
{
    if [ -z "$APP_PID" ] ; then
        echo "The FGC app is not running."
    else
        kill -9 ${APP_PID}
    fi
}


# Restart the app
function fgcRestart #()
{
    kill -9 ${APP_PID} 2>/dev/null
    ${FGC_MOUNT_FGC}/${FGC_APP_NAME}
}


########################
# Entry point

# Parse arguments
if   [ "$1" = "s" ] || [ "$1" = "start" ] || [ "$1" = "" ] ; then
    fgcStart
elif [ "$1" = "k" ] || [ "$1" = "stop" ] ; then
    fgcStop
elif [ "$1" = "r" ] || [ "$1" = "restart" ] ; then
    fgcRestart
else
    printHelp
fi


# EOF
