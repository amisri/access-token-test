#!/bin/bash

# Configures Ethernet ports and assign them correct names.

# Change directory to a script location
cd "$(dirname "$0")"

# Import
source /bin/fgc_config.sh
source /bin/fgc_utils.sh

function fgcEthernetRename() #(currentName, newName, readableName)
{
    currentName="$1"
    newName="$2"
    readableName="$3"

    fgcInfo "Renaming and enabling '${readableName}' (from ${currentName} to ${newName})"

    if [ -z "$currentName" ] ; then
        fgcError "Ethernet interface for the $readableName ($newName) has not been found"
    else
        ifconfig "$currentName" down
        logOnError "Failed to disable the $readableName ($newName) interface"

        ip link set "$currentName" name "$newName"
        logOnError "Failed to set name for the $readableName ($newName) interface"

        ifconfig "$newName" up
        logOnError "Failed to enable the $readableName ($newName) interface"
    fi
}

function fgcEthernetInit()
{
    # Those variables will hold current (default) names of ETH interfaces
    eth_A=""
    eth_B=""

    # Iterate over all interfaces and check their memory to know which one is which
    for interface in /sys/class/net/*; do
        # Get interface name
        if_name=$(basename $interface)

        # Check if the interface contains memory address for FGC and GIGA
        check_A=$(ifconfig "$if_name" | grep "$FGC_ETH_A_ADDR")
        check_B=$(ifconfig "$if_name" | grep "$FGC_ETH_B_ADDR")

        # If any of them is not empty, then the respective interface has been found
        if [ ! -z "$check_A" ] ;  then
            eth_A="$if_name"
        fi

        if [ ! -z "$check_B" ] ;  then
            eth_B="$if_name"
        fi
    done

    # Now verify that both interfaces have been found and if so rename and enable them
    fgcEthernetRename "$eth_A" "$FGC_ETH_A_NAME" "Eth Interface A"
    fgcEthernetRename "$eth_B" "$FGC_ETH_B_NAME" "Eth Interface B"
}


# EOF
