#!/bin/bash

# This script check the codes directory, check the checksum and copies binaries
# into proper places (i.e. does reprogramming).

# Change directory to a script location
cd "$(dirname "$0")"

# Import
source /bin/fgc_config.sh
source /bin/fgc_utils.sh


# This function check if the file exist and if so if its checksum is correct
function fgcCheckFile() #(fileToCheck) - return 0 on success
{
    fileToCheck="$1"

    if [ -f "$fileToCheck" ] ; then

        ### TODO check checksum
        # logOnError "Invalid checksum for $1"

        return 0
    else
        return 1
    fi
}

# Function that checks checksum and moves the file
function fgcCheckAndMove() #(source, target)
{
    source="$1"
    target="$2"

    # Check if the source file exists and is correct
    if fgcCheckFile $source ; then
        fgcInfo "Updating file at ${target}"

        # Move binary
        mv "$1source" "$target"
        logOnError "Failed to move $source to $target"
    fi
}

# Reprogram ITB file
function fgcCheckAndMoveItb() #()
{
    itb_src="${FGC_CODES_PATH}/${FGC_ITB_NAME}"
    itb_dest="${FGC_MOUNT_SYS}/${FGC_ITB_NAME}"

    # Check if the source ITB exists and is correct
    if fgcCheckFile "$itb_src" ; then
        fgcInfo "Updating the Kernel ITB"

        # Create a folder for the mount
        mkdir -p "$FGC_MOUNT_SYS"

        # Mount the partition
        if mount "$FGC_SD_SYS" "$FGC_MOUNT_SYS" ; then
            # Copy the ITB file with tmp extension
            mv "$itb_src" "${itb_dest}"

            # Umount partition
            umount "$FGC_SD_SYS" 2>/dev/null

            # Then check and auto-fix partition
            fsck -p -f "$FGC_SD_SYS"

            # In case of error in fsck, report it
            if [ "$?" -gt "1" ] ; then
                fgcError "fsck reported error $? for the SYSTEM partition."
            fi
        else
            fgcError "Failed to mount the SYSTEM partition."
        fi
    fi
}


function fgcReprogram()
{
    # First check that the codes directory exits (which implies that the DATA partition has been mounted)
    if [ -d "$FGC_CODES_PATH" ] ; then
        # The main app
        fgcCheckAndMove "${FGC_CODES_PATH}/${FGC_APP_NAME}" "${FGC_MOUNT_FGC}/${FGC_APP_NAME}"

        # Kernel modules
        if [ -d "${FGC_CODES_PATH}" ]; then
            modules=(`find "${FGC_CODES_PATH}"/ -maxdepth 1 -name "*.ko"`)

            for f in "${modules[@]}" ; do
                fgcCheckAndMove "$f" "$FGC_MOUNT_FGC"
            done
        fi

        # Kernel ITB file
        fgcCheckAndMoveItb
    fi
}


# EOF
