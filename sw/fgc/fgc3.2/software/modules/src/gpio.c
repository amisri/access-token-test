/* 
    FGC GPIO Kernel Module

    How to obtain Kernel GPIO number from hardware pin name?
        You have to go to /sys/class/gpio and you should see gpiochip* files. Do ls - l you will see a full patch.
        For example, for "gpiochip480", the path is ../../devices/platform/soc/2300000.gpio/gpio/gpiochip480
        As 2300000 is the hardware address of GPIO1 module. And the number 480 corresponds to the first GPIO pin
        in this port (i.e. GPIO1.0). 
        Now if we want, for example, GPIO1.13, we add 480+13 = 493. 493 is the pin number used in Kernel.
*/

#include <linux/init.h>                 // Macros used to mark up functions e.g. __init __exit
#include <linux/module.h>               // Core header for loading LKMs into the kernel
#include <linux/kernel.h>               // Contains types, macros, functions for the kernel
#include <linux/gpio.h>                 // Required for the GPIO functions
#include <linux/interrupt.h>            // Required for the IRQ code
#include <asm/io.h>


// ************ Module registration ******************************************************************

#define  DEVICE_NAME "fgc-GPIO"   
#define  CLASS_NAME  "fgc-gpio"       
 
MODULE_LICENSE("GPL");            
MODULE_AUTHOR("Dariusz Zielinski");    
MODULE_DESCRIPTION("FGC GPIO Kernel Module.");
MODULE_VERSION("1.0");            


// ***************************************************************************************************

static unsigned int gpioId = 493;       // GPIO1.13 (ASLEEP)
static unsigned int irqNumber;       

static irq_handler_t fgcGpioIsr(unsigned int irq, void *dev_id, struct pt_regs *regs);


// ***************************************************************************************************

int init_module(void) 
{
    if (gpio_request(gpioId, "sysfs") != 0)
    {
        printk(KERN_ERR "FGC-GPIO: Failed to get request GPIO %d.\n", gpioId);
        return 1;
    }  

    if (gpio_direction_input(gpioId) != 0)
    {
        printk(KERN_ERR "FGC-GPIO: Failed to set GPIO %d as input.\n", gpioId);
        return 2;
    }

    irqNumber = gpio_to_irq(gpioId);
    if (irqNumber < 0)
    {
        printk(KERN_ERR "FGC-GPIO: Failed to get IRQ number to the GPIO %d.\n", gpioId);
        return 3;
    }

    // Requests an interrupt line
    int ret = request_irq(irqNumber,              
                         (irq_handler_t) fgcGpioIsr,         // Function with interrupt handler
                         IRQF_TRIGGER_FALLING,               // Generate interrupt on HIGH to LOW transition
                         "fgc_gpio_handler",                 // Used in /proc/interrupts to identify the owner
                         NULL);                              // The *dev_id for shared interrupt lines, NULL is okay
    
    if (ret != 0)
    {
        printk(KERN_ERR "FGC-GPIO: Failed to request IRQ for the GPIO %d.\n", gpioId);
        return 4;
        
    }

    printk(KERN_INFO "FGC-GPIO: Registered GPIO interrupt handler on line %d.\n", irqNumber);

    // init_isr(0);

    return 0;
}
 

void cleanup_module(void) 
{
    free_irq(irqNumber, NULL);
    gpio_free(gpioId);

    printk(KERN_INFO "FGC-GPIO: Unloaded GPIO and IRQ\n");
}


static irq_handler_t fgcGpioIsr(unsigned int irq, void *dev_id, struct pt_regs *regs)
{
    printk(KERN_INFO "FGC-GPIO: Interrupt detected\n");
   
    return (irq_handler_t) IRQ_HANDLED; 
}
