// Empty module - for testing 
 

#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/io.h>


MODULE_LICENSE("GPL");            
MODULE_AUTHOR("Me");    
MODULE_DESCRIPTION("FGC IFC Kernel Module.");
MODULE_VERSION("1.0");    

#define IFC_BASE 0x1530000
#define IFC_BANK 2

#define IFC_REGION 0x7FB00000

#define IFC_REV  (IFC_BASE + 0)
#define CSPR_EXT (IFC_BASE + 0x000C + (IFC_BANK * 12))
#define CSPR     (IFC_BASE + 0x0010 + (IFC_BANK * 12))


void __iomem * vIFC_REV; 
void __iomem * vCSPR_EXT; 
void __iomem * vCSPR; 

void __iomem * vREG; 


int init_module(void) 
{
    printk(KERN_INFO "FGC-IFC: Hello 4\n");

	vIFC_REV  = ioremap_nocache(IFC_REV,  4);
	vCSPR_EXT = ioremap_nocache(CSPR_EXT, 4);
	vCSPR     = ioremap_nocache(CSPR,     4); 

    printk(KERN_INFO "FGC-IFC: IFC_REV  = %d \n", ioread32be(vIFC_REV));
    printk(KERN_INFO "FGC-IFC: CSPR_EXT = %d \n", ioread32be(vCSPR_EXT));
    printk(KERN_INFO "FGC-IFC: CSPR     = %d \n", ioread32be(vCSPR));


	vREG = ioremap_nocache(IFC_REGION, 4); 

    printk(KERN_INFO "FGC-IFC: 0x7FB00000 = %d \n", ioread32(vREG));

    return 0;
}

void cleanup_module(void) 
{
    printk(KERN_INFO "FGC-IFC: Goodbye\n");
}
