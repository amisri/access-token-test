//! @file   threads.cpp
//! @brief  Provides threads' priority and affinity configuration storage and methods.

// ---------- Includes

#include <pthread.h>

#include <threads.hpp>


using namespace fgc;


// ---------- Public static variables and external variables definitions

//! This namespace contains configurations data for threads.
//! Priority is from 1-49 for non-real time threads and >=50 from RT. Lower number means lower priority.
namespace fgc::threads
{   //                       Priority          Affinity
    ThreadType logger          (10,         Affinity::none);
    ThreadType ethernet        (40,         Affinity::none);
};



// ---------- Public methods and external functions definitions

ThreadType::ThreadType(int const priority, Affinity const affinity) noexcept :
        threadPriority(priority), threadAffinity(affinity)
{
}



void fgc::ThreadType::configure(std::thread& thread) const
{
    int error;

    // Set priority of the thread
    sched_param params{};
    params.sched_priority = threadPriority;

    error = pthread_setschedparam(thread.native_handle(), SCHED_FIFO, &params);

    if (error != 0)
    {
        // TODO Exception
    }


    // Set affinity of the thread
    if (threadAffinity != Affinity::none)
    {
        // Prepare CPU mask by setting selected core
        cpu_set_t cpu_set;
        CPU_ZERO(&cpu_set);
        CPU_SET(static_cast<size_t>(threadAffinity), &cpu_set);

        // Set affinity
        error = pthread_setaffinity_np(thread.native_handle(), sizeof(cpu_set), &cpu_set);

        // Check if that was successful
        if (error != 0)
        {
            // TODO Exception
        }
    }
}






















