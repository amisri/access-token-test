//! @file   clocks.cpp
//! @brief  Collection of classes used as a backend for timers


// ---------- Includes

#include <hardwareClock.hpp>


using namespace fgc;



// ---------- Internal classes, structures, unions and enumerations


/*


// ************************************************************
// Flex Timer functions
// ************************************************************

void timerStart()
{
    // Reset timer counter value
    b32Out(mmStatus + (FTM1_CNT & MAP_MASK), 0);

    // Set clocking source to system clock
    uint32_t tmp = bs32(b32In(mmStatus + (FTM1_SC & MAP_MASK)));

    bitSet(tmp, CLKS0);     // Selects System Clock == Platform Clock / 2 == 350 MHz
    //bitSet(tmp, CLKS1);   // Selects RTC == 32'768 Hz

    b32Out(mmStatus + (FTM1_SC & MAP_MASK), bs32(tmp));
}

void timerStop()
{
    // Disable clocking source
    uint32_t tmp = bs32(b32In(mmStatus + (FTM1_SC & MAP_MASK)));
    bitClear(tmp, CLKS0);
    bitClear(tmp, CLKS1);
    b32Out(mmStatus + (FTM1_SC & MAP_MASK), bs32(tmp));
}

void timerInit()
{
    // Disable clocking source
    timerStop();

    // Reset timer counter value
    b32Out(mmStatus + (FTM1_CNT & MAP_MASK), 0);

    // Set MOD (final) value to max value of 16-bit timer
    b32Out(mmStatus + (FTM1_MOD & MAP_MASK), bs32(0xFFFF));

    // Set CNTIN (initial) value to 0
    b32Out(mmStatus + (FTM1_CNTIN & MAP_MASK), 0);

    // Set prescaler to 32   == Timer frequency is 350'000'000 Hz / 32 == 10'937'500 Hz
    //                       == Timer tick is 0.000000091 s == 91 ns
    //                       == Timer overflow is 5.991862857 ms
    uint32_t SC = bs32(b32In(mmStatus + (FTM1_SC & MAP_MASK)));

    bitSet(SC, PS0);
    bitSet(SC, PS2);

    b32Out(mmStatus + (FTM1_SC & MAP_MASK), bs32(SC));

    // Reset timer counter value
    b32Out(mmStatus + (FTM1_CNT & MAP_MASK), 0);
}

uint32_t timerRead()
{
    return (bs32(b32In(mmStatus + (FTM1_CNT & MAP_MASK))));
}

char timerCheckTOF()
{
    // Read control register
    uint32_t tmp = bs32(b32In(mmStatus + (FTM1_SC & MAP_MASK)));

    // If Timer Overflow is set to 1, then clear it
    if ( (tmp & TOF) == TOF )
    {
        bitClear(tmp, TOF);
        b32Out(mmStatus + (FTM1_SC & MAP_MASK), bs32(tmp));

        return 1;
    }
    else
    {
        return 0;
    }
}


// Implement eval timer API

void evalTimerInit()
{


    timerInit();
    fprintf(stderr, "Timer initialized.");
}

void evalTimerStart()
{
    timerStart();
}

void evalTimerStop()
{
    timerStop();
}


float evalTimerRead()
{
    uint32_t timerValue = timerRead();

    // Timer tick is 91 ns == 0.091 us
    return (0.091f * timerValue);
}


uint32_t evalTimerRaw()
{
    return timerRead();
}

char evalTimerCheckInterrupt()
{
    return timerCheckTOF();
}
*/


// ---------- Public static variables and external variables definitions



// ---------- Private and protected static variables and internal variables definitions



// ---------- Public methods and external functions definitions



// ---------- Private and protected methods and internal functions definitions
