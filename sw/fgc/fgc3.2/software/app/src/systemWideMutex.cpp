//! @file  systemWideMutex.cpp
//! @brief Module ensures that only one instance of application is running at the time.
//!
//! It is a global, system-wide mutex used to ensure that only one instance (process) of the application is running at the same time.

// ---------- Includes

#include <cerrno>
#include <iostream>

#include <sys/file.h>

#include <systemWideMutex.hpp>


using namespace fgc;


// ---------- Public methods and external functions definitions

SystemWideMutex::SystemWideMutex() noexcept
{
    // Try to open a file or create it (if it doesn't exist).
    int pid_file = open("/var/tmp/fgc32.lock.pid", O_CREAT | O_RDWR, 0666);

    // And try to set a lock on the file. Request for an exclusive and non-blocking lock.
    int rc = flock(pid_file, LOCK_EX | LOCK_NB);

    // If 'rc' is different than 0 it's an error in acquiring the lock.
    if (rc != 0)
    {
        // Check if the locking failed due to the file being already lock (another instance of the app).
        if (EWOULDBLOCK == errno)
        {
            // Inform the user and terminate the app. Use stdout as there is no logger at this point.
            std::cout << "The application is already running" << std::endl;
            exit(1);
        }
    }
}

