//! @file   timing.cpp
//! @brief  Contains methods to get current time, time since program start etc.

// ---------- Includes

#include <iomanip>
#include <sstream>

#include <timing.hpp>


using namespace fgc;


// ---------- Public methods and external functions definitions

Timing::Timing()
{
    timeOnStart = std::chrono::steady_clock::now();
}



time_t Timing::getUtcTime() const noexcept
{
    return std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
}



void Timing::setUtcTime(time_t const newTime)
{
    // TODO set system time - noexcept?
}



std::string Timing::timeUtcStr() const noexcept
{
    char buffer[100];

    time_t timer  = getUtcTime();
    tm*    tmInfo = localtime(&timer);

    strftime(buffer, 100, "%Y-%m-%d %H:%M:%S", tmInfo);

    return std::string(buffer);
}



std::chrono::nanoseconds Timing::getTimeSinceStart() const noexcept
{
    return std::chrono::steady_clock::now() - timeOnStart;
}



std::string Timing::timeSinceStartStr() const noexcept
{
    return formatDuration(getTimeSinceStart(), TimingFormat::hours, TimingFormat::micro);
}



std::string Timing::formatDuration(std::chrono::nanoseconds const duration, TimingFormat from, TimingFormat to) noexcept
{
    // Declare const array with ratios.
    static constexpr double ratios[] = { 3600.0 * 1'000'000'000, 60.0 * 1'000'000'000, 1'000'000'000, 1'000'000, 1000, 1 };

    // Convert duration to nanoseconds.
    auto nano = static_cast<uint64_t>(duration.count());

    // Check that 'from' and 'to' are ok.
    if (from < TimingFormat::hours || from > TimingFormat::nano)
    {
        from = TimingFormat::hours;
    }

    if (to < TimingFormat::hours || to > TimingFormat::nano)
    {
        to = TimingFormat::nano;
    }

    std::stringstream ss;

    // Break-down nanoseconds into components.
    for (auto i = static_cast<uint8_t>(from); i <= static_cast<uint8_t>(to); ++i)
    {
        // Calculate next value.
        auto value = static_cast<uint64_t>(nano / ratios[i]);
             nano -= static_cast<uint64_t>(value * ratios[i]);

        // Print value.
        ss << std::setfill('0') << std::setw( i > static_cast<uint8_t>(TimingFormat::seconds) ? 3 : 2 ) << value;

        // Print delimiter.
        if (i < static_cast<uint8_t>(to))
        {
            ss << (i >= static_cast<uint8_t>(TimingFormat::seconds) ? '.' : ':');
        }
    }

    // Return prepared string.
    return ss.str();
}



