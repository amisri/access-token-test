//! @file   Initializer.cpp
//! @brief  Performs a task of initializing modules.

// ---------- Includes

#include <thread>
#include <chrono>
#include <signal.h>

#include <initializer.hpp>
#include <ethernet.hpp>
#include <logger.hpp>
#include <hardwareClock.hpp>


using namespace fgc;



// ---------- Public methods and external functions definitions

Initializer::Initializer() :
        file("/home/fgc/logs/pipes/out")
{
    // Ignore errors related to printing to broken pipes - this is used by the BufferedFile and the Logger classes.
    signal(SIGPIPE, SIG_IGN);

    // Connected the 'file' (BufferedFile) to the loggers
    /*TODO enable this devLog.setBufferedFile(&file);
    devInfo.setBufferedFile(&file);
    devError.setBufferedFile(&file);
    devTimer.setBufferedFile(&file);*/

    // Inform about the logger
    // TODO enable this devInfo << "Logger initialized";

    // Initialize hardware clocks
    HardwareClock2::init();

    // Initialize Ethernet port
    //ethPort = new EthernetPort("eth1", 128);
}


void Initializer::executeApplication() const noexcept
{
    // This function never exits, letting other threads to work.
    for (;;)
    {
        std::this_thread::sleep_for(std::chrono::hours(100));
    }
}










