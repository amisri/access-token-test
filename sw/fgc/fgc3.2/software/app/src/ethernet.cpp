//! @file   ethernet.cpp
//! @brief  Provides low level handling of the Ethernet (receiving and sending packets).


// ---------- Includes

#include <netinet/in.h>
#include <net/if.h>
#include <cstring>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <unistd.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <poll.h>
#include <cerrno>
#include <cstdlib>
#include <cstdint>

#include <ethernet.hpp>
#include <logger.hpp>

using namespace fgc;


// ---------- Internal classes, structures, unions and enumerations



// ---------- Public static variables and external variables definitions


// ---------- Private and protected static variables and internal variables definitions

constexpr uint16_t FGC_ETHER_TYPE = 0x88B5;


// ---------- Public methods and external functions definitions


EthernetPort::EthernetPort(std::string const portName, uint32_t const bufferSize)
{
    devInfo << "Ethernet initialization started";

    frameSize    = static_cast<uint32_t>(getpagesize());
    rxBuffer     = nullptr;
    socketHandle = -1;
    bufFrames    = bufferSize;
    bufSize      = bufFrames * frameSize;

    // Open a socket for raw packet access with no predefined (default) protocol
    if ((socketHandle = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0)
    {
        std::cout << "Error socketHandle = socket" << std::endl;  // TODO remove it
        // TODO exception
    }

    // ***************************************************

    // Get index of Ethernet interface (eth_name to corresponding number)

    int ethIndex = if_nametoindex(portName.c_str());

    if (ethIndex == 0)
    {
        std::cout << "Error if_nametoindex" << std::endl;  // TODO remove it
        // TODO exception
    }

    // ***************************************************

    // Get MAC address of Ethernet interface

    ifreq ifreq{};

    strncpy(ifreq.ifr_name, portName.c_str(), sizeof(ifreq.ifr_name) - 1);
    ifreq.ifr_name[sizeof(ifreq.ifr_name) - 1] = '\0';

    if (ioctl(socketHandle, SIOCGIFHWADDR, &ifreq))
    {
        std::cout << "Error ioctl(socketHandle" << std::endl;  // TODO remove it
        // TODO exception
    }

    // ***************************************************

    // Save Ethernet port (self) address

    memcpy(macAddress, ifreq.ifr_hwaddr.sa_data,  6);

    // ***************************************************

    // Binding Ethernet interface index to the socket using link-layer socket descriptor

    memset((void*)&socketAddress, 0, sizeof(socketAddress));
    socketAddress.sll_family   = AF_PACKET;
    socketAddress.sll_protocol = htons(FGC_ETHER_TYPE);
    socketAddress.sll_ifindex  = ethIndex;
    socketAddress.sll_hatype   = ARPHRD_ETHER;
    socketAddress.sll_pkttype  = PACKET_OTHERHOST;
    socketAddress.sll_halen    = ETH_ALEN;

    if (bind(socketHandle, reinterpret_cast<struct sockaddr*>(&socketAddress), sizeof(socketAddress)) < 0)
    {
        std::cout << "Error bind(socketHandle" << std::endl;  // TODO remove it
        // TODO exception
    }

    // ***************************************************

    // Above settings are sufficient for sending packets. Following settings are for receiving.
    // Configuring options used to memory map packet reception

    tpacket_req req{};
    req.tp_block_size = bufSize;     // Minimal size of contiguous block
    req.tp_block_nr   = 1;           // Number of blocks
    req.tp_frame_size = frameSize;   // Size of frame
    req.tp_frame_nr   = bufFrames;   // Total number of frames

    if (setsockopt(socketHandle, SOL_PACKET, PACKET_RX_RING, reinterpret_cast<void*>(&req), sizeof(req)))
    {
        std::cout << "Error setsockopt(socketHandle" << std::endl;  // TODO remove it
        // TODO exception
    }

    // ***************************************************

    // Memory map reception ring buffer and assign it to a pointer

    rxBuffer = reinterpret_cast<uint8_t*>(mmap(nullptr, bufSize, PROT_READ | PROT_WRITE, MAP_SHARED, socketHandle, 0));

    if (!rxBuffer)
    {
        std::cout << "Error rxBuffer = reinterpret_cast" << std::endl;  // TODO remove it
        // TODO exception
    }

    // ***************************************************


    // Finally, create reception thread and set RunThread as entry point.
    thread = std::thread(&EthernetPort::runThread, this);

    // And configure the thread.
    threads::ethernet.configure(thread);

    devInfo << "Ethernet initialization completed";
}


void EthernetPort::send(uint8_t const destinationMac[6], uint16_t etherType, uint8_t * const frame, uint32_t const size)
{
    // TODO check that size of the frame is at least longer than the Eth Header

    // Convert EtherType to network number.
    etherType = htons(etherType);

    // Copy data into the Ethernet header:
    // 0             6           12          14   -->   frameSize
    // |  DEST. MAC  |  SRC. MAC  | EtherType |   payload   |
    memcpy(frame,      destinationMac, 6);
    memcpy(frame + 6,  macAddress,     6);
    memcpy(frame + 12, &etherType,     2);


    // ***************************************************

    auto sentSize = static_cast<unsigned int>(sendto(socketHandle, frame, size, 0,
                    reinterpret_cast<struct sockaddr*>(&socketAddress), sizeof(socketAddress)));


    std::cout << "Packet sent: size " << sentSize << std::endl;  // TODO remove it

    // ***************************************************

    if (sentSize != size)
    {
        // TODO do something? Stats?
    }

    // ***************************************************

}


void EthernetPort::setReceiver(std::function<Handler> const handler)
{
    receiveHandler = handler;
}


void fgc::EthernetPort::setMacAddress(uint8_t const newAddress[6])
{
    memcpy(macAddress, newAddress, 6);

    // TODO Actually configure Eth port in Linux to change
}


void fgc::EthernetPort::runThread()
{
    // Header with meta-data at the beginning of each packet (not Ethernet header)
    struct tpacket_hdr* metaHeader;

    // Pointer to current head of reception ring buffer
    uint8_t* bufferHead = rxBuffer;

    // Pointer to the end of reception ring buffer
    const uint8_t* bufferTail = rxBuffer + bufSize - frameSize;

    // ***************************************************

    // Configure structure with options used by poll function

    static struct pollfd poll_opts;
    poll_opts.fd      = socketHandle;
    poll_opts.events  = POLLIN;
    poll_opts.revents = 0;

    // ***************************************************

    devInfo << "Starting Ethernet reception";

    // Infinite loop to capture and process packets
    for (;;)
    {
        // Get pointer to meta-data metaHeader in the first available packet in reception ring buffer
        metaHeader = reinterpret_cast<tpacket_hdr*>(bufferHead);

        // If packet is still processed by kernel then poll for change (will be in user space when it's ready)
        while ((metaHeader->tp_status & TP_STATUS_USER) == false)
        {
            if ((poll(&poll_opts, 1, -1) == -1) && (errno != EINTR))    // Poll without timeout
            {
                continue;
            }
        }

        if(metaHeader->tp_status & TP_STATUS_LOSING)
        {
            // TODO fgceth_vars.global_stats.losing_frames++;
        }


        // ***************************************************

        // When frame is in user space (== is ready to be read) then cast it to its structure

        uint8_t* frame = bufferHead + metaHeader->tp_mac;


        // TODO fgceth_vars.global_stats.load_received += metaHeader->tp_len;
        // TODO fgceth_vars.global_stats.eth.rx.all++;

        // ***************************************************
        devInfo << "Received!";
        std::cout << "Frame received " << std::endl;  // TODO remove it
        // Send the frame and its length to the receiver (if it's defined)
        if (receiveHandler)
        {
            receiveHandler(frame, metaHeader->tp_len);
        }

        // ***************************************************

        // After processing mark frame status as kernel so it can be used again by kernel

        metaHeader->tp_status = TP_STATUS_KERNEL;

        // Advance head to next element in reception ring buffer or reset it when the buffer is at the end

        bufferHead = ( (bufferHead == bufferTail) ? rxBuffer : (bufferHead + frameSize) );
    }

}



// ---------- Private and protected methods and internal functions definitions






















