//! @file   logger.cpp
//! @brief  Provides functionality to print logs to consoles and files.


// ---------- Includes

#include <threads.hpp>
#include <logger.hpp>


using namespace fgc;


// ---------- Public static variables and external variables definitions

Logger fgc::devLog(nullptr);
Logger fgc::devInfo(nullptr,  "\033[1;34mINFO  : ", "\033[0m\n");
Logger fgc::devError(nullptr, "\033[1;31mERROR : ", "\033[0m\n");
Logger fgc::devTimer(nullptr, "\033[1;33mTIMER : ", "\033[0m\n");


// ---------- Public methods and external functions definitions

BufferedFile::BufferedFile(std::string const filePath, bool const flushAtExit) : flush(flushAtExit)
{
    // Open the file in a read-only mode.
    // If the file doesn't exist, fileRead will be -1 and the filePath will be created as a regular file.
    // If the file exists and is a regular file, this open is not needed.
    // If the file exists and is a named pipe, this open will enable writing to the pipe (as the pipes have to
    // have reader and writer). Later the fileRead will be closed and the pipe will be broken, but writing to
    // a broken pipe is ignored (with signal(SIGPIPE, SIG_IGN);). That way as soon as some reader will connect to the pipe
    // it will be able to read from that pipe and the program doesn't have to wait (block) for it to happen.
    int fileRead = open(filePath.c_str(), O_RDONLY | O_NONBLOCK);

    // Open (or create) given file.
    file = open(filePath.c_str(), O_WRONLY | O_NONBLOCK | O_CREAT);

    // Check if the open was successful.
    if (file < 0)
    {
        std::cout << "Error " << filePath << std::endl;
        // TODO throw exception
    }

    // Close read-only file, as it's no longer needed. TODO what happens if fileRead < 0
    close(fileRead);

    // Create new thread and set RunThread as entry point.
    thread = std::thread(&BufferedFile::runThread, this);

    // Configure the thread.
    threads::logger.configure(thread);
}


BufferedFile::~BufferedFile()
{
    // Inform the thread to finish working.
    keepRunning = false;

    // Invalidate the buffer, releasing blocking functions.
    buffer.invalidate();

    // Join the thread.
    thread.join();

    // Close the file
    if (file > -1)
    {
        close(file);
    }
}


void BufferedFile::writeString(std::string const & str)
{
    buffer.push(str);
}


void BufferedFile::writeString(std::string&& str)
{
    buffer.push(str);
}




LoggerEntry::LoggerEntry(Logger* const loggerPtr)
    : logger(loggerPtr)
{
    // TODO add time stamp
    stream << logger->prefix;
}


LoggerEntry::LoggerEntry(LoggerEntry const & K)
    : logger(K.logger)
{
}


LoggerEntry::~LoggerEntry()
{
    if ((logger->file != nullptr) && (logger->enabled))
    {
        stream << logger->suffix;
        logger->file->writeString(stream.str());
    }
}


Logger::Logger(BufferedFile* const bufferedFile, std::string const logPrefix, std::string const logSuffix)
    : prefix(logPrefix), suffix(logSuffix), file(bufferedFile)
{
}


void Logger::setBufferedFile(BufferedFile* const bufferedFile)
{
    file = bufferedFile;
}


void fgc::Logger::setEnabled(bool const enableLogger)
{
    enabled = enableLogger;
}



// ---------- Private and protected methods and internal functions definitions

void BufferedFile::runThread()
{
    while (keepRunning)
    {
        // Block until the data is available and print it. popBlock may return empty string, but that's fine.
        std::string&& str = buffer.popBlock();

        // Write to the file
        write(file, str.c_str(), str.size());
    }

    // When the thread if asked to finish, check if flushAtExit was set and if so, print everything that is left in the buffer.
    while (flush && (buffer.isEmpty() == false))
    {
        std::string&& str = buffer.pop();

        // Write to the file
        write(file, str.c_str(), str.size());
    }
}


