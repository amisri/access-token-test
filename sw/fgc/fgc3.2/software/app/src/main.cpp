//! @file    main.cpp
//! @brief   Entry point of the FGC3.2 application.
//!
//! This file contains entry point of the FGC3.2 application, regardless of the specific class.

// ---------- Includes
#include <iostream>
#include <string>

#include <initializer.hpp>
#include <systemWideMutex.hpp>


#include <timing.hpp>
#include <timers.hpp>
#include <hardwareClock.hpp>


#include <ethernet.hpp>
#include <bits.hpp>
#include <mdio.hpp>
#include <phy.hpp>


using namespace fgc;


// ---------- Private and protected methods and internal functions definitions
 
//! This function prevents making a mistake of starting the application manually instead of via launcher.
//! It is a mistake because launcher performs some system-wide initialization tasks.
//! Launcher passes an argument "started_from_launcher" to the application.
//!
//! @param argc Number of arguments (passed from the entry point).
//! @param argv Array of arguments (passed from the entry point).

void checkIfStartedFromLauncher(int const& argc, char const * const argv[]) noexcept
{
    // First argument is the program's path. Check if there is at least the second one.
    if (argc >= 2)
    {
        // If there is a second argument, then check if it's the right one.
        if (std::string(argv[1]) == "started_from_launcher")
        {
            return;
        }
    }    
    
    // Otherwise it's an error. Stdout is used as there is no logger at this point.
    std::cout << "Not started from launcher." << std::endl;
    exit(1);
}



// ---------- Application's entry point

//! This function is an entry point of the application.
//! It performs necessary checks and invokes other modules' initialization.
//! 
//! @param argc Number of arguments.
//! @param argv Array of arguments.
//!
//! @return Application exit code.

int main(int argc, char *argv[])
{
    std::cout << "*** TEST APP ***" << std::endl;

    // Check if app was started from launcher.
    // TODO enable: checkIfStartedFromLauncher(argc, argv);

    // Try to acquire system-wide lock to make sure only one instance of the application is running.
    // Launcher checks for this as well, this is a cross-check. If the lock is blocked, the application will terminate.
    SystemWideMutex systemWideMutex;

    // Invoke the initialization of all modules.
    Initializer initializer;







/*
    Timer t1("t1");
    t1.start();
    std::this_thread::sleep_for(1s);
    t1.stop();
    t1.print();



    {
        ScopedTimer t2("t2");

        std::this_thread::sleep_for(1200ms);
    }




    ChainedTimer t3("t3");

    t3.start();

    std::this_thread::sleep_for(200ms);
    t3.addPoint();


    std::this_thread::sleep_for(300ms);
    t3.addPoint();


    std::this_thread::sleep_for(400ms);
    t3.addPoint();


    std::this_thread::sleep_for(500ms);
    t3.addPoint();


    std::this_thread::sleep_for(600ms);
    t3.addPoint();

    t3.print();







    Timer<HardwareClock2>th1("th1");

    HardwareClock2::reset();
    th1.start();
    std::this_thread::sleep_for(2ms);
    th1.stop();
    th1.print();


    ChainedTimer<HardwareClock2> th3("th3");

    HardwareClock2::reset();
    th3.start();

    std::this_thread::sleep_for(300us);    HardwareClock2::reset();
    th3.addPoint();


    std::this_thread::sleep_for(400us);    HardwareClock2::reset();
    th3.addPoint();


    std::this_thread::sleep_for(500us);    HardwareClock2::reset();
    th3.addPoint();


    std::this_thread::sleep_for(600us);    HardwareClock2::reset();
    th3.addPoint();


    std::this_thread::sleep_for(700us);    HardwareClock2::reset();
    th3.addPoint();

    th3.print();

*/


/*

    Mdio mdio("eth3", 2);



    //auto val = (mdio.read(0x0018) & ~bit::mask<uint16_t>(8, 11)) | (1 << 7);


    // Configure INTN/PWDNN pin as interrupt
    std::cout << " 0x001E = " << (int) mdio.read(0x001E) << std::endl;
    mdio.write(0x001E, mdio.read(0x001E) | (uint16_t)(1 << 7));
    std::cout << " 0x001E = " << (int) mdio.read(0x001E) << std::endl;


    // Enable interrupts
    std::cout << " 0x0012 = " << (int) mdio.read(0x0012) << std::endl;
    mdio.write(0x0012, (1 << 10));
    std::cout << " 0x0012 = " << (int) mdio.read(0x0012) << std::endl;
*/



    /*

    while (true)
    {
        mdio.read(0x0013);

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

     */




    /*
    std::cout << "*** TEST APP STARTED 2 ***" << std::endl;

    Phy phy("eth3", 2);


    std::cout << "LINK STATUS = " << (phy.linkStatus() ? "ON" : "OFF") << std::endl << std::endl;

    std::vector<PhyByte> pattern =
    {
        PhyByte(0xFF, true),
        PhyByte(0xFF, true),
        PhyByte(0xFF, true),
        PhyByte(0xFF, true),
        PhyByte(0xFF, true),
        PhyByte(0xFF, true),                // Destination MAC address

        PhyByte(0x00, true),
        PhyByte(0x1B, true),
        PhyByte(0x21, true),
        PhyByte(0x56, true),
        PhyByte(0x30, true),
        PhyByte(0x26, true),                // Sender MAC address

        PhyByte(0x88, true),
        PhyByte(0xB5, true)                 // EtherType
    };


    phy.configurePatternMatching(pattern);



    while (true)
    {
        std::cout << phy.readReceptionStatus() << std::endl;
        phy.clearInterrupts();

        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }*/




   // std::this_thread::sleep_for(std::chrono::seconds(1));


    std::cout << "*** TEST APP STARTED 2 ***" << std::endl;

    if (argc < 2)
    {
        std::cout << "No ETH name as the first parameter" << std::endl;
        exit(1);
    }

    std::cout << "Open socket for " << argv[1] << " interface" << std::endl;

    auto ethPort = new EthernetPort(argv[1], 128);

    uint8_t destinationMac[6] = {0x00, 0x1b, 0x21, 0x56, 0x30, 0x26};

    uint8_t frame[50] = {5,6,7,5,6,7,5,6,7,5,6,7,5,6,7,5,6,7,5,6,7,5,6,7,5,6,7,5,6,7,5,6,7,5,6,7};


    while (true)
    {
        std::cout << "Attempting to send packet***" << std::endl;

        ethPort->send(destinationMac, 0x88B6, frame, 50);

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }



/*
    uint32_t tmp = b32In(base);
    uint32_t tmpBs = bs32(b32In(base));

    std::cout << " IFC_REV    == " << tmp << std::endl;
    std::cout << " IFC_REV BS == " << tmpBs << std::endl;
*/






/*

    while (true)
    {
        uint16_t a = bit::read<uint16_t>(trololo + (0 & hw::mapMask));
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

        uint16_t b = bit::read<uint16_t>(trololo + (2 & hw::mapMask));
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

        uint16_t c = bit::read<uint16_t>(trololo + (4 & hw::mapMask));
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

        std::cout << (int)a << " "<< (int)b << " "<< (int)c << " " <<std::endl;


        std::this_thread::sleep_for(std::chrono::milliseconds(100));

    }*/



    std::cout << "*** TEST APP DONE  ***" << std::endl;


    // This function never exits.
    //initializer.executeApplication();

    return 0;
} 


















