//! @file   hardwareClock.hpp
//! @brief  A collection of classes used as a backend for timers


#pragma once

// ---------- Includes

#include <stdint.h>
#include <chrono>

#include <hardwareMemoryMap.hpp>
#include <bits.hpp>


namespace fgc
{
    // ---------- Classes and structures declarations


    //! This class provides low-level access to the hardware timers (FlexTimer modules) and satisfies
    //! C++ Clock requirements.
    //!
    //! @tparam baseAddress hardware address of the FlexTimer module.
    template<uint32_t baseAddress>
    class HardwareClock
    {
        public:
            typedef std::chrono::nanoseconds               duration;      //!< C++ Clock requirements: duration type of the clock.
            typedef duration::rep                          rep;           //!< C++ Clock requirements: type used by duration.
            typedef duration::period                       period;        //!< C++ Clock requirements: tick period of the clock in sec.
            typedef std::chrono::time_point<HardwareClock> time_point;    //!< C++ Clock requirements: type of a time point object.


            //! Initialize the hardware FlexTimer module. Prior to initialization, using other methods
            //! will not have any effect.
            static void init()
            {
                // Memory map hardware base address to the app's virtual memory (to access HW from user-space).
                base = hw::memoryMap(baseAddress);

                // Configure, initialize and start FlexTimer
                if (base != nullptr)
                {
                    // Disable clocking source
                    uint32_t tmp = bs32(b32In(base + (hw::ftm::SC & hw::mapMask)));
                    bitClear(tmp, hw::ftm::CLKS0);
                    bitClear(tmp, hw::ftm::CLKS1);
                    b32Out(base + (hw::ftm::SC & hw::mapMask), bs32(tmp));

                    // Reset timer counter value
                    b32Out(base + (hw::ftm::CNT & hw::mapMask), 0);

                    // Set MOD (final) value to max value of 16-bit timer
                    b32Out(base + (hw::ftm::MOD & hw::mapMask), bs32(0xFFFF));

                    // Set CNTIN (initial) value to 0
                    b32Out(base + (hw::ftm::CNTIN & hw::mapMask), 0);


                    // Set prescaler to 32   == Timer frequency is 350'000'000 Hz / 32 == 10'937'500 Hz
                    //                       == Timer tick is 0.000000091 s ~= 91.4 ns
                    //                       == Timer overflow is 5.991862857 ms
                    uint32_t SC = bs32(b32In(base + (hw::ftm::SC & hw::mapMask)));

                    bitSet(SC, hw::ftm::PS0);
                    bitSet(SC, hw::ftm::PS2);

                    b32Out(base + (hw::ftm::SC & hw::mapMask), bs32(SC));


                    // Set clocking source to system clock
                    tmp = bs32(b32In(base + (hw::ftm::SC & hw::mapMask)));

                    bitSet(tmp, hw::ftm::CLKS0);      // Selects System Clock == Platform Clock / 2 == 350 MHz
                    // bitSet(tmp, CLKS1);            // Selects RTC == 32'768 Hz

                    b32Out(base + (hw::ftm::SC & hw::mapMask), bs32(tmp));
                }
            }


            //! Clears the hardware FlexTimer's counter.
            static void reset() noexcept
            {
                if (base != nullptr)
                {
                    b32Out(base + (hw::ftm::CNT & hw::mapMask), 0);
                }
            }


            //! Returns current value of the counter. Fulfills a requirement of C++ Clock requirements
            //!
            //! @return time point representing current counter value.
            static time_point now() noexcept
            {
                if (base != nullptr)
                {
                    // Timer tick is 91.428571429 ns
                    return time_point(std::chrono::nanoseconds(static_cast<int>(
                               91.428571429 * (bs32(b32In(base + (hw::ftm::CNT & hw::mapMask))))
                           )));
                }
                else
                {
                    return time_point(std::chrono::nanoseconds(0));
                }
            }


            static constexpr bool is_steady = false;    //!< C++ Clock requirements: this clock is not steady

        private:
            static uint8_t* base;

    };

    template<uint32_t baseAddress>
    uint8_t* HardwareClock<baseAddress>::base = 0;


    typedef HardwareClock<hw::ftm::BASE2> HardwareClock2;       //!< HardwareClock instance using FlexTimer module 2
    typedef HardwareClock<hw::ftm::BASE3> HardwareClock3;       //!< HardwareClock instance using FlexTimer module 3
    typedef HardwareClock<hw::ftm::BASE4> HardwareClock4;       //!< HardwareClock instance using FlexTimer module 4
    typedef HardwareClock<hw::ftm::BASE5> HardwareClock5;       //!< HardwareClock instance using FlexTimer module 5
    typedef HardwareClock<hw::ftm::BASE6> HardwareClock6;       //!< HardwareClock instance using FlexTimer module 6
    typedef HardwareClock<hw::ftm::BASE7> HardwareClock7;       //!< HardwareClock instance using FlexTimer module 7
    typedef HardwareClock<hw::ftm::BASE8> HardwareClock8;       //!< HardwareClock instance using FlexTimer module 8
}
