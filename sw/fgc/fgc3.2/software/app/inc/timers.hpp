//! @file   timers.hpp
//! @brief  A collection of classes to measure time.

#pragma once

// ---------- Includes

#include <chrono>
#include <string>
#include <utility>
#include <vector>

#include <hardwareClock.hpp>
#include <logger.hpp>
#include <timing.hpp>


namespace fgc
{
    // ---------- Classes and structures declarations

    //! Basic timer class used to measure time since start() to stop() and print formatted time to the timer logger.
    //!
    //! @tparam T type of a clock used to get time. T should satisfy the C++ TrivialClock requirements.
    template <typename T = std::chrono::high_resolution_clock>
	class Timer
	{
		public:
            //! A default constructor that leaves name of the timer empty.
			Timer() = default;

			//! A constructor that sets name of the timer. Name is later printed along with the measured time.
			//!
			//! @param timerName name of the timer.
            explicit Timer(std::string const timerName) noexcept : name(std::move(timerName))
			{

			}

			//! Starts time measurement.
            //!
            //! @remarks Not thread-safe.
			void start()
			{
			    startPoint = T::now();
			    isStarted  = true;
			}


			//! Stops time measurement.
            //!
            //! @remarks Not thread-safe.
			void stop()
			{
			    if (isStarted)
			    {
                    stopPoint = T::now();
                    isStarted = false;
			    }
			}


			//! Returns measured duration as nanoseconds. If the timer has been started and stopped the duration will
			//! represent time between start() and stop(). If the timer has been started and not yet stopped the duration
			//! will represent time between start() and the current moment. If the timer has never been started, the duration
			//! will be equal to 0.
			//!
			//! @return Duration measured by the timer.
            //!
            //! @remarks Thread-safe.
			std::chrono::nanoseconds get()
			{
			    if (isStarted)
			    {
			        return T::now() - startPoint;
			    }
			    else
			    {
			        return stopPoint - startPoint;
			    }
			}


			//! Prints name of the timer and duration measured by the timer into the timer logger, displaying time from
			//! seconds to microseconds. The duration value is the one returned from the get() method.
            //!
            //! @remarks Thread-safe.
			void print()
			{
			    devTimer << name << " time = " << Timing::formatDuration(get(), TimingFormat::seconds, TimingFormat::micro);
			}


		protected:
			bool                   isStarted = false;           //!< Flag that marks whether the timer is started but not stopped.
			std::string            name;                        //!< Name of the timer. Empty by default.
			typename T::time_point startPoint;                  //!< Time point representing start() moment.
			typename T::time_point stopPoint;                   //!< Time point representing stop() moment.
	};



    //! Extension of the basic timer. This timer measures time its own instance lives, i.e. it does start()
    //! in constructor and stop() and print() in destructor.
    //!
    //! @tparam T type of a clock used to get time. T should satisfy the C++ TrivialClock requirements.
    template <typename T = std::chrono::high_resolution_clock>
    class ScopedTimer : public Timer<T>
    {
        public:
            //! Default constructor. Leaves name of the timer empty and starts the timer.
            ScopedTimer()
            {
                Timer<T>::start();
            }


            //! A constructor that sets name of the timer and starts it.
            //! Name is later printed along with the measured time.
            //!
            //! @param timerName name of the timer.
            //!
            //! @remarks Not thread-safe.
            explicit ScopedTimer(std::string timerName) : Timer<T>(timerName)
            {
                Timer<T>::start();
            }


            //! Destructor of the tmer. Stops it and print the measured time.
            //!
            //! @remarks Not thread-safe.
            ~ScopedTimer()
            {
                Timer<T>::stop();
                Timer<T>::print();
            }
    };


    //! This timer should be especially useful to profile a functions, as it supports multiple points of measurement
    //! from the single point of start.
    //! This timer doesn't have a stop() method. Instead, a user simply adds points and a print() method prints them.
    //!
    //! @tparam T type of a clock used to get time. T should satisfy the C++ TrivialClock requirements.
    template <typename T = std::chrono::high_resolution_clock>
    class ChainedTimer
    {
        public:
            //! A default constructor that leaves name of the timer empty.
            ChainedTimer() = default;


            //! A constructor that sets name of the timer. Name is later printed along with the measured time.
            //!
            //! @param timerName name of the timer.
            explicit ChainedTimer(std::string timerName) : name(timerName)
            {
            }

            //! Resets list of time points and starts the timer (i.e. adds the first point in time).
            //!
            //! @remarks Not thread-safe.
            void start()
            {
                points.clear();
                firstPoint = T::now();
            }


            //! Adds time point to the list of such. User may provide custom name for this point in name.
            //!
            //! @param pointName name of the added time point, defaults to empty string.
            //!
            //! @remarks Not thread-safe.
            void addPoint(std::string pointName = "")
            {
                points.push_back( std::make_pair(pointName, T::now()) );
            }


            //! Prints all added time points and their name to the timer logger.
            //! If only one point has been added nothing will be printed.
            //! Both absolute times (from the first point) and relative times (from the last point) are printed.
            //!
            //! @remarks Not thread-safe.
            void print()
            {
                auto  last = firstPoint;

                for (auto const& p : points)
                {
                    devTimer << name << " time point " << p.first << " = "
                             << Timing::formatDuration(p.second - firstPoint, TimingFormat::seconds, TimingFormat::micro) << " ("
                             << Timing::formatDuration(p.second - last,       TimingFormat::seconds, TimingFormat::micro) << ")";

                    last = p.second;
                }
            }

        private:
            std::string                                                 name;        //!< Name of the timer.
            typename T::time_point                                      firstPoint;  //!< First time point after starting the timer.
            std::vector<std::pair<std::string, typename T::time_point>> points;      //!< List of the time points and their names.
    };

}






