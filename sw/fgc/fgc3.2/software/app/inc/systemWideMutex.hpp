//! @file  systemWideMutex.hpp
//! @brief Module ensures that only one instance of application is running at the time.
//!
//! It is a global, system-wide mutex used to ensure that only one instance (process) of the application is running at the same time.

#pragma once

namespace fgc
{
    // ---------- Classes and structures declarations

    class SystemWideMutex
    {
        public:
            //! This is a constructor that tries to lock a file to ensure that only one instance of the application is running.
            //! If it fails to lock then it prints the error and terminates the app.
            SystemWideMutex() noexcept;
    };
}

