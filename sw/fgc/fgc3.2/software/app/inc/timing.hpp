//! @file   timing.hpp
//! @brief  Contains methods to get current time, time since program start etc.

#pragma once

// ---------- Includes

#include <chrono>
#include <stdint.h>
#include <string>


namespace fgc
{
    // ---------- Simple types and enumerations declarations

    //! Possible time-units to print in formatted way.
    enum class TimingFormat : uint8_t
    {
        hours   = 0,
        minutes = 1,
        seconds = 2,
        milli   = 3,
        micro   = 4,
        nano    = 5
    };


    // This is exceptional pollution of the namespace. It only adds time suffixes (like. 1s, 20ms, etc.).
    using namespace std::chrono_literals;


    // ---------- Classes and structures declarations

    //! This class provides methods to get system time and time since start (creation of an instance of the class) - both
    //! as variables and formatted strings. Additionally, it provides static methods to format duration as string.
	class Timing
	{
		public:
            //! Constructor of the class. Since this point the "time from start" is measured.
            Timing();

            //! Default copy constructor
            Timing(Timing const&) = default;

            //! Default copy assignment
            Timing& operator=(Timing const&) = default;

            //! Default move constructor
            Timing(Timing &&) = default;

            //! Default move assignment
            Timing& operator=(Timing &&) = default;

            //! Returns the system time (UTC). The time is synchronized by the gateway.
            //!
            //! @returns System time (UTC).
            //!
            //! @remarks Not thread-safe.
            time_t getUtcTime() const noexcept;

            //! Sets the system time. Used to synchronize time with gateway.
            //!
            //! @param newTime time to set (UTC).
            //!
            //! @remarks Not thread-safe.
            void setUtcTime(time_t newTime);

            //! Returns the system time (UTC) as a string in a following format: "%Y-%m-%d %H:%M:%S".
            //!
            //! @return A string representation of the system time (UTC).
            //!
            //! @remarks Not thread-safe.
            std::string timeUtcStr() const noexcept;

            //! Returns time in nanoseconds since the creation of an instance of this class.
            //!
            //! @return Time in nanoseconds since the creation of an instance of this class.
            //!
            //! @remarks Not thread-safe.
            std::chrono::nanoseconds getTimeSinceStart() const noexcept;

            //! Returns time since the creation of an instance of this class as a string in a following format: "hh:mm:ss.mmm.mmm".
            //!
            //! @return A string representation of time since the creation of an instance of this class.
            //!
            //! @remarks Not thread-safe.
            std::string timeSinceStartStr() const noexcept;

            //! Formats passed duration in nanoseconds as string. String format is defined by 'from' and 'to' variables,
            //! which define, respectively, the biggest time unit and the smallest one to print. For example, printing
            //! from minutes to milliseconds will produce a following format: "mm:ss.mmm". If 'from' is a smaller time unit
            //! than 'to' an empty string will be returned. If invalid 'from' or 'to' is passed, hours and nanoseconds will
            //! be used, respectively.
            //!
            //! @param duration duration in nanoseconds to format as a string.
            //! @param from the biggest time unit to print.
            //! @param to the smallest time unit to print.
            //! @return A string representation of a passed duration.
            //!
            //! @remarks Thread-safe.
            static std::string formatDuration(std::chrono::nanoseconds duration, TimingFormat from, TimingFormat to) noexcept;

		private:
            std::chrono::steady_clock::time_point timeOnStart;      //!< Time point representing creation of an instance of this class.
	};

}
