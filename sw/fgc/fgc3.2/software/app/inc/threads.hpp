//! @file   threads.cpp
//! @brief  Provides threads' priority and affinity configuration storage and methods.

#pragma once

// ---------- Includes

#include <thread>


namespace fgc
{
    // ---------- Simple types and enumerations declarations    

    //! Possible options of threads affinity to a processor core.
    enum class Affinity : size_t
    {
        core0 = 0,          //!< Affinity to core with index 0.
        core1 = 1,          //!< Affinity to core with index 1.
        core2 = 2,          //!< Affinity to core with index 2.
        core3 = 3,          //!< Affinity to core with index 3.

        none  = 99          //!< No affinity to a specific core. Default behavior.
    };


    // ---------- Classes and structures declarations

    //! A class containing thread configuration parameters (priority, affinity).
    class ThreadType
    {
        public:
            //! Constructs object holding thread configuration parameters given in the arguments.
            //!
            //! @param priority Priority of the thread, from 1-99 (where >=50 is real-time). Lower number means lower priority.
            //! @param affinity Thread affinity to a processor core.
            ThreadType(int priority, Affinity affinity) noexcept;

            //! Default copy constructor
            ThreadType(ThreadType const&) = default;

            //! Default copy assignment
            ThreadType& operator=(ThreadType const&) = default;

            //! Default move constructor
            ThreadType(ThreadType &&) = default;

            //! Default move assignment
            ThreadType& operator=(ThreadType &&) = default;

            //! Sets thread parameters (priority, affinity) using pthread methods based on native handler of std::thread.
            //!
            //! @param thread Reference to the thread object for which the configuration will be performed.
            void configure(std::thread& thread) const;

        private:
            const int      threadPriority;      //!< Priority for this type of threads.
            const Affinity threadAffinity;      //!< Affinity for this type of threads.
    };


    // ---------- External variables declarations

    //! Contains ThreadType objects. Each object represents type of a thread.
    namespace threads
    {
        extern ThreadType logger;               //!< Thread type used by Logger module.
        extern ThreadType ethernet;             //!< Thread type used by Ethernet module.
    };

}
