//! @file   logger.hpp
//! @brief  Provides functionality to print logs to consoles and files.


#pragma once

// ---------- Includes

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <thread>


#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <circularBuffer.hpp>


namespace fgc
{
    // ---------- Classes and structures declarations

    //! This class provides a thread-safe, buffered access to a file or named pipe for writing.
    //! One thread can add string to be printed in a non-blocking way. The class creates its own thread that
    //! is used to pop element from the buffer and write them to the file.
    class BufferedFile
    {
        public:
            //! Opens file pointed by 'filePath'. The file can be regular file or a named pipe.
            //! Named pipe has to exists prior to execution of this constructor. If the 'filePath' points to a nonexistent
            //! file, the file is created.
            //!
            //! @param filePath Path to the file that will be open or created.
            //! @param flushAtExit Setting this flag to true will flush the buffer when the object is destructed.
            explicit BufferedFile(std::string filePath, bool flushAtExit = false);

            //! Destructor - invalidates the buffer, joins the thread and closes open file.
            ~BufferedFile();

            //! Copy constructor - object not copyable.
            BufferedFile(BufferedFile const & K)            = delete;

            //! Move constructor - object not movable.
            BufferedFile(BufferedFile&& K)                  = delete;

            //! Copy assignment - object not copyable.
            BufferedFile& operator=(BufferedFile const & K) = delete;

            //! Move assignment - object not movable.
            BufferedFile& operator=(BufferedFile&& K)       = delete;

            //! Adds string to the buffer.
            //!
            //! @param str String to be added to the buffer.
            void writeString(std::string const & str);

            //! See `void writeString(const std::string& str)`.
            void writeString(std::string&& str);


        private:
            //! Entry point of the class's thread.
            void runThread();

            std::thread                 thread;                 //!< Thread object.
            bool                        keepRunning = true;     //!< Flag used to terminate thread.
            bool                        flush;                  //!< Flag to mark if the buffer should be flushed during destruction.
            int                         file        = -1;       //!< File descriptor.
            CircularBuffer<std::string> buffer;                 //!< Buffer object.
    };


    //! Forward declaration of the Logger class;
    class Logger;


    //! This a helper, temporary class used by the logger. It is created the logger as a temporary object.
    //! During its destruction the string stream is printed to the file pointed by the logger.
    //! The class enables atomic creation of the logger entry (the message won't be interleaved by other messages).
    class LoggerEntry
    {
        public:
            //! Construct the LoggerEntry class and prints the logger's prefix into the stream.
            //!
            //! @param loggerPtr pointer to the Logger instance that created LoggerEntry.
            LoggerEntry(Logger* loggerPtr);

            //! Copy constructor.
            LoggerEntry(LoggerEntry const& K);

            //! Destructor. Prints suffix and pushes the stream as a string into the logger's buffer.
            ~LoggerEntry();

            //! Template method that passed its argument into the string stream.
            //!
            //! @param obj Object passed into stream.
            //! @return Returns reference to the itself to enable chaining of <<.
            template <typename T>
            LoggerEntry& operator<<(T obj)
            {
                std::cout << obj << std::flush; // TODO remove this
                stream << obj;

                return *this;
            }

        private:
            Logger*           logger;       //!< Pointer to the parent Logger.
            std::stringstream stream;       //!< String stream used to keep printed message before it's pushed into buffer.
    };


    //! This class provides interface similar to standard output streams (like std::cout). It is used to log
    //! messages into files, terminals or named pipes.
    //! There are three global instances of this class: devLog, devInfo and devError.
    class Logger
    {
        //! Declaration of friendship with LoggerEntry so that the entry class can read prefix, suffix and file pointer of the logger.
        friend LoggerEntry;

        public:
            //! Constructs logger object with given prefix and suffix and pointer to the file.
            //!
            //! @param bufferedFile pointer to the BufferedFile object that handles buffering and printing to the file. Can be nullptr and set later.
            //! @param logPrefix a prefix that will be printed before the actual message.
            //! @param logSuffix a suffix that will be printed after the actual message.
            Logger(BufferedFile* bufferedFile = nullptr, std::string logPrefix = "", std::string logSuffix = "");

            //! Sets pointer to the BufferedFile object.
            //!
            //! @param bufferedFile pointer to the BufferedFile object that handles buffering and printing to the file.
            void setBufferedFile(BufferedFile* bufferedFile);

            //! Enables or disables printing by the logger.
            //!
            //! @param enableLogger if true the logger is enabled and prints to the file. Otherwise it ignores incoming messages.
            void setEnabled(bool enableLogger);

            //! This methods enables chaining of <<. It puts given argument into the stream of freshly created LoggerEntry,
            //! and then return a temporary LoggerEntry object. This object will be alive as long as there is a reference to it,
            //! so until the last << operator invoked on this LoggerEntry. After the destruction of LoggerEntry object, the message
            //! will be pushed into the BufferedFile object for printing.
            //!
            //! @param obj object to print (e.g. array of chars)
            //! @return Instance of LoggerEntry
            template <typename T>
            LoggerEntry operator<<(T obj)
            {
                LoggerEntry loggerEntry(this);

                loggerEntry << obj;

                return loggerEntry;
            }

        private:
            bool          enabled = true;       //!< Flag use to enable / dissable prinitng by this logger.
            std::string   prefix;               //!< Prefix of the message.
            std::string   suffix;               //!< Suffix of the message.
            BufferedFile* file    = nullptr;    //!< Pointer to the BufferedFile object.
    };


    // ---------- External variables declarations

    extern Logger devLog;                       //!< Logger to print any data (no specific).
    extern Logger devInfo;                      //!< Logger to print information.
    extern Logger devError;                     //!< Logger to print error.
    extern Logger devTimer;                     //!< Logger to print timer measured by timers.
}


















