//! @file   ethInterface.hpp
//! @brief  Describes interface to access Ethernet module.


#pragma once

// ---------- Includes

#include <stdint.h>
#include <functional>


namespace fgc
{
    // ---------- Classes and structures declarations

    struct EthInterface
    {
        using Handler = void(uint8_t * frame, uint32_t size);

        virtual ~EthInterface() = default;

        virtual void send(uint8_t const destinationMac[6], uint16_t etherType, uint8_t* frame, uint32_t size) = 0;
        virtual void setReceiver(std::function<Handler> handler) = 0;

        virtual void setMacAddress(uint8_t const newAddress[6]) = 0;
    };

}





















