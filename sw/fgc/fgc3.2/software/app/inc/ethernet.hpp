//! @file   ethernet.hpp
//! @brief  Provides low level handling of the Ethernet (receiving and sending packets).


#pragma once

// ---------- Includes

#include <string>

#include <linux/if_packet.h>

#include <threads.hpp>
#include <ethInterface.hpp>


namespace fgc
{
    // ---------- Classes and structures declarations

    class EthernetPort : public EthInterface
    {
        using Handler = EthInterface::Handler;

        public:
            EthernetPort(std::string portName, uint32_t bufferSize);

            void send(uint8_t const destinationMac[6], uint16_t etherType, uint8_t* frame, uint32_t size) override;
            void setReceiver(std::function<Handler> handler) override;

            void setMacAddress(uint8_t const newAddress[6]) override;

        private:
            void runThread();

            std::thread               thread;               //!< Thread object for Ethernet reception.
            std::function<Handler>    receiveHandler;       //!< Handler invoked on packet reception.

            uint32_t                  frameSize;            //!< Size of single frame in the reception ring buffer.
            uint32_t                  bufFrames;            //!< Size of reception ring buffer in frames.
            uint32_t                  bufSize;              //!< Size of reception ring buffer in bytes.
            uint8_t*                  rxBuffer;             //!< Ring buffer for reception.
            int                       socketHandle;         //!< Handle to opened Ethernet socket.
            struct sockaddr_ll        socketAddress;        //!< Link-layer socket descriptor.
            uint8_t                   macAddress[6];        //!< Source (self) MAC address.
    };

}





















