//! @file   Initializer.hpp
//! @brief  Performs a task of initializing modules.


#pragma once

// ---------- Includes
#include <logger.hpp>
#include <ethInterface.hpp>




namespace fgc
{
    // ---------- Classes and structures declarations

    //! Class used to initialize modules.
    class Initializer
    {
        public:
            //! Performs initialization of modules.
            Initializer();

            //! It creates endless loop and sleep inside it, letting other threads work.
            void executeApplication() const noexcept;
 
        // TODo enable this private:
            BufferedFile file;              //!< Used by standard loggers.
            EthInterface* ethPort;
    };
}


