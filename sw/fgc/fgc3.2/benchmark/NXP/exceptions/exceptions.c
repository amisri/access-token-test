/*
 ============================================================================
 Name        : exceptions.c
 Author      :
 Version     :
 Copyright   : Freescale Semiconductor, Inc.
 Description : Sample Project in C language
 ============================================================================
 */

#include <stdio.h>
#include "gic.h"

// user handler methods
void _handleISR();
void defaultHandler();
#if GIC_VER
void interruptHandler();
#endif

/****************************************************************************
 *  ARMv8 exception vectors are different from ARMv7
 *  There are 4 sets of vector arrays each addressing 4 exception types:
 *
 *                               Synchronous   IRQ/vIRQ   FIQ/vFIQ   SError/vSError
 *  -------------------------------------------------------------------------------
 *  Current EL with SP_EL0      |   0x000   |    0x080  |  0x100    |   0x180
 *  Current EL with SP_ELx,x>0  |   0x200   |    0x280  |  0x300    |   0x380
 *  Lower EL using AArch64      |   0x400   |    0x480  |  0x500    |   0x580
 *  Lower EL using AArch32      |   0x600   |    0x680  |  0x700    |   0x780
 *  -------------------------------------------------------------------------------
 *
 *  Each exception level(EL > 0) can have its specific vector handlers:
 *  VBAR_ELx - points to the exception base address and must be aligned at 0x800
 */

///////////////////////////////////////
// Current Exception level with SP_EL0:
///////////////////////////////////////
void currELSyncSP0(){ _handleISR(); }
void currELIRQSP0() { _handleISR(); }
void currELFIQSP0() { _handleISR(); }
void currELSErrSP0() { _handleISR(); }

////////////////////////////////////////////
// Current Exception level with SP_ELx, x>0:
////////////////////////////////////////////
void currELSyncSPx() { _handleISR(); }
void currELIRQSPx() { _handleISR(); }
void currELFIQSPx() { _handleISR(); }
void currELSErrSPx() { _handleISR(); }

////////////////////////////////
// AArch64 Lower Exception level:
////////////////////////////////
void lowELSyncA64() { _handleISR(); }
void lowELIRQA64() { _handleISR(); }
void lowELFIQA64() { _handleISR();}
void lowELSErrA64() { _handleISR(); }

////////////////////////////////
// AArch32 Lower Exception level:
////////////////////////////////
void lowELSyncA32() { _handleISR(); }
void lowELIRQA32() { _handleISR(); }
void lowELFIQA32() { _handleISR(); }
void lowELSErrA32() { _handleISR(); }


// user code for exception processing should go here
void _handleISR()
{
    // Interrupt handling

    // find out why we took an interrupt

    // clear the request and call the specific C interrupt handle

    unsigned int val = 0;
    asm volatile("mrs %0, isr_el1" : "=r" (val));
    if ((val & 0xC0) != 0)  // check if any IRQ or FIQ is pending
    {
#if GIC_VER
        interruptHandler();
#else
        defaultHandler();
#endif
    }
    else
    {
        defaultHandler();
    }
}

void defaultHandler()
{
    asm("b .");
}
