/*
 ============================================================================
 Name        : el_switches.h
 Author      :
 Version     :
 Copyright   : Freescale Semiconductor, Inc.
 Description : Sample Project in C language
 ============================================================================
 */


void switch_el3_to_el2();
void switch_el2_to_el1();
void switch_el1_to_el0();
void switch_el3_to_ns_el1();
void switch_el3_to_s_el1();
