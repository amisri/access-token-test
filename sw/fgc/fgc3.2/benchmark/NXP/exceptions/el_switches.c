/*
 ============================================================================
 Name        : el_switches.c
 Author      :
 Version     :
 Copyright   : Freescale Semiconductor, Inc.
 Description : Sample Project in C language
 ============================================================================
 */


void switch_el3_to_el2()
{
    asm("mov x0, #0x531"); /* Non-secure EL0/EL1 | HVC | 64bit EL2 */
    asm("msr scr_el3, x0");
    asm("msr cptr_el3, xzr"); /* Disable coprocessor traps to EL3 */
    asm("mov x0, #0x33ff");
    asm("msr cptr_el2, x0"); /* Disable coprocessor traps to EL2 */

    /* SCTLR_EL2 initialization */
    asm("mov x0, #0x0809\n"
        "movk x0, #0x30d0, lsl #16\n"
        "msr sctlr_el2, x0\n");

    asm("mrs x0, vbar_el3\n"
        "msr vbar_el2, x0\n"     /* Migrate VBAR */
        "mov x0, #0x3c9\n"
        "msr spsr_el3, x0\n"     /* EL2_SP2 | D | A | I | F */
        "msr elr_el3, x30\n"
        "eret");
}

void switch_el2_to_el1()
{
    asm("mrs x0, midr_el1\n"
        "mrs x1, mpidr_el1\n"
        "msr vpidr_el2, x0\n"
        "msr vmpidr_el2, x1");

    /* Disable coprocessor traps */
    asm("mov x0, #0x33ff\n"
        "msr cptr_el2, x0\n" /* Disable coprocessor traps to EL2 */
        "msr hstr_el2, xzr\n" /* Disable coprocessor traps to EL2 */
        "mov x0, #3 << 20\n"
        "msr cpacr_el1, x0\n" /* Enable FP/SIMD at EL1 */

    /* Initialize HCR_EL2 - TGE must be 0 here */
        "mov x0, #(1 << 31)\n" /* 64bit EL1 */
        "msr hcr_el2, x0\n");

    /* SCTLR_EL1 initialization */
    asm("mov x0, #0x0809\n"
        "movk x0, #0x30d0, lsl #16\n"
        "msr sctlr_el1, x0\n");

    asm("mrs x0, vbar_el2\n"
        "msr vbar_el1, x0\n"     /* Migrate VBAR */
        "mov x0, #0x3c5\n"
        "msr spsr_el2, x0\n"     /* EL2_SP2 | D | A | I | F */
        "msr elr_el2, x30\n"
        "eret");
}

void switch_el1_to_el0()
{
    asm("mov x0, #0x3c0\n"
        "msr spsr_el1, x0\n"     /* EL1_SP1 | D | A | I | F */
        "msr elr_el1, x30\n"
        "eret");
}

void switch_el3_to_ns_el1()
{
    asm("mov x0, #0x531"); /* Non-secure EL0/EL1 | HVC | 64bit EL1 */
    asm("msr scr_el3, x0");
    asm("msr cptr_el3, xzr"); /* Disable coprocessor traps to EL3 */

    /* Initialize HCR_EL2 - TGE must be 0 here */
    asm("mov x0, #(1 << 31)\n" /* 64bit EL1 */
        "msr hcr_el2, x0\n");

    /* Disable coprocessor traps */
    asm("mov x0, #3 << 20\n"
        "msr cpacr_el1, x0\n"); /* Enable FP/SIMD at EL1 */

    /* SCTLR_EL1 initialization */
    asm("mov x0, #0x0809\n"
        "movk x0, #0x30d0, lsl #16\n"
        "msr sctlr_el1, x0\n");

    asm("mrs x0, vbar_el3\n"
        "msr vbar_el2, x0\n"     /* Migrate VBAR */
        "msr vbar_el1, x0\n"     /* Migrate VBAR */
        "mov x0, #0x3c5\n"
        "msr spsr_el3, x0\n"     /* SPx | D | A | I | F */
        "msr elr_el3, x30\n"
        "eret");
}

void switch_el3_to_s_el1()
{
    asm("mov x0, #0x530"); /* Secure EL0/EL1 | HVC | 64bit EL1 */
    asm("msr scr_el3, x0");
    asm("msr cptr_el3, xzr"); /* Disable coprocessor traps to EL3 */

    /* Disable coprocessor traps */
    asm("mov x0, #3 << 20\n"
        "msr cpacr_el1, x0\n"); /* Enable FP/SIMD at EL1 */

    /* SCTLR_EL1 initialization */
    asm("mov x0, #0x0809\n"
        "movk x0, #0x30d0, lsl #16\n"
        "msr sctlr_el1, x0\n");

    asm("mrs x0, vbar_el3\n"
        "msr vbar_el1, x0\n"     /* Migrate VBAR */
        "mov x0, #0x3c5\n"
        "msr spsr_el3, x0\n"     /* SPx | D | A | I | F */
        "msr elr_el3, x30\n"
        "eret");
}
