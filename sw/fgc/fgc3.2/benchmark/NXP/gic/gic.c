/*
 * Copyright 2015 - 2016 Freescale Semiconductor, Inc.
 */

#include "gic.h"

#if GIC_VER
#include <stdio.h>
#include <stdatomic.h>

void testGIC(unsigned int coreIdx)
{
	if (coreIdx == 0)
	{
		printf("Test SGI interrupts.\n");
	}
	else
	{
		asm("wfe");		/* wait for master core to initialize */
	}

	int ret = configure_gic(coreIdx);

	if (coreIdx == 0)
	{
		if (ret)
		{
			printf("GICv%d not found on this processor - failed to configure GIC!\nTest aborted.\n", GIC_VER);
		}
		else
		{
			printf("Initializing GICv%d...\n", GIC_VER);
			printf("Sending SGI #0 from core #0 to all other cores...\n");
			sendall_sgi(0);		/* send INTID #0 (SGI) to all other cores */
		}
		asm("sev");		/* wake-up the other cores */
	}
}

/* need a lock to protect activeCore */
static atomic_flag mutex = ATOMIC_FLAG_INIT;

void _handlerPrintCallback(unsigned int coreIdx, unsigned int intID)
{
	do {} while (atomic_flag_test_and_set(&mutex));
	printf("Core#%d in IRQ handler ID #%d.\n", coreIdx, intID);
	atomic_flag_clear(&mutex);
}

#endif
