/*
 * Copyright 2015 - 2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 */

#ifndef GIC_H_
#define GIC_H_

/* Choose GIC version included in SoC 
*  supported: GICv2, GICv3, GICv4
*/
#define GIC_VER 0

/* GIC alignment (only valid for GIC_VER = 2)
 * For LS1046A, LS1026A, LS1043A (rev. 1.1), LS1023A (rev. 1.1) set this to 1.
 */
#define GIC_ADDRESS_ALIGNED 1

#if GIC_VER == 2
#include "gic_v2.h"
#elif GIC_VER >= 3
#include "gic_v3.h"
#endif

#if GIC_VER
void testGIC(unsigned int coreIdx);
void _handlerPrintCallback(unsigned int coreIdx, unsigned int intID);
#endif

#endif /* GIC_H_ */
