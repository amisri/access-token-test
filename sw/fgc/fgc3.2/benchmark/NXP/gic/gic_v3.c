/*
 * Copyright 2015 - 2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 */

#include "gic.h"

#if GIC_VER >= 3

extern unsigned int get_core_index();

/* Swap values to Little-Endian format (GIC registers) */
unsigned int _swap_to_LE(unsigned int val)
{
#ifdef __AARCH64EB__
	asm("rev %w0, %w0" : "=r" (val) : "r" (val));
#endif
	return val;
}

/* REFERENCES */
/* GICv3 Software Overview DAI 0492A */
/* ARM� Generic Interrupt Controller Architecture Specification GICv3 and GICv4. IHI 0069A */

/* Try to just get SGI and PPI working - no SPI supported.
 * Interrupts 0-15 are SGIs
 * Interrupts 16-31 are PPIs
 * Should only have to enable Redistributors (GICR_*) and CPU interfaces (ICC_*_ELn -
 * software must enable the system register interface before using these registers by
 * setting the SRE bit in the ICC_SRE_ELn registers)
 */

/* Configure the GIC Core interface */
int configure_gicc(unsigned int coreIdx)
{
	register unsigned long val = 0;

	/* Enable Non-secure system register access ICC_SRE_EL1 */
	asm volatile("mrs %0, S3_0_C12_C12_5" : "=r" (val));
	val |= ICC_SRE_EL1_SRE; /* Started with just enabling then added bypass for trial */
	asm volatile("msr S3_0_C12_C12_5, %0" : : "r" (val));
	/* Enable system register access ICC_SRE_EL2 */
	asm volatile("mrs %0, S3_4_C12_C9_5" : "=r" (val));
	val |= ICC_SRE_EL1_SRE | 0x7;  /* Started with just enabling then added bypass for trial */
	asm volatile("msr S3_4_C12_C9_5, %0" : : "r" (val));
	/* Enable system register access ICC_SRE_EL3 */
	asm volatile("mrs %0, S3_6_C12_C12_5" : "=r" (val));
	val |= ICC_SRE_EL1_SRE | 0x7;  /* Started with just enabling then added bypass for trial */
	asm volatile("msr S3_6_C12_C12_5, %0" : : "r" (val));
	/* Set priority mask ICC_PMR_EL1 and binary point registers ICC_BPRn_EL1 */
	asm volatile("msr S3_0_C4_C6_0, %0" : : "r" (0xf0)); /* ICC_PMR_EL1 interrupts w/priority >0xf0 will be signaled */
	asm volatile("msr S3_0_C12_C8_3, %0" : : "r" (0x3)); /* ICC_BPR0_EL1 */
	asm volatile("msr S3_0_C12_C12_3, %0" : : "r" (0x3)); /* ICC_BPR1_EL1 */
	/* Set EOI mode ICC_CTLR_EL1 and ICC_CTLR_EL3 */
	asm volatile("msr S3_0_C12_C12_4, %0" : : "r" (0x0)); /* ICC_CTLR_EL1 */
	asm volatile("msr S3_6_C12_C12_4, %0" : : "r" (0x0)); /* ICC_CTLR_EL3 */
	/* Enable signaling of each interrupt group ICC_IGRPEN1_EL1 ICC_IGRPEN0_EL1 or ICC_IGRPEN1_EL3 */
	/* set ICC_IGRPEN0_EL1 = S3_0_C12_C12_6 = 1 */
	asm volatile("msr S3_0_C12_C12_6, %0" : : "r" (1));
	isb();
	/* set ICC_IGRPEN1_EL1 = S3_0_C12_C12_7 = 1 */
	asm volatile("msr S3_0_C12_C12_7, %0" : : "r" (1));
	isb();
	/* set ICC_IGRPEN1_EL3 = S3_6_C12_C12_7 = 3*/
	asm volatile("msr S3_6_C12_C12_7, %0" : : "r" (3)); /* try setting both Secure Grp 1 and non-secure Grp 1 */
	isb();

	return 0;
}

/* Configure the GIC Redistributor interface */
int configure_gicr(unsigned int coreIdx)
{
	register unsigned int val = 0;
	volatile unsigned int * paddr = (unsigned int *) ((unsigned long) GICR_WAKER(coreIdx));

	val = _swap_to_LE(*paddr);
	val &= ~GICR_WAKER_ProcessorSleep;
	*paddr = _swap_to_LE(val);
	do {	/* Mark the core as being awake for the Redistributor */ 
		val = _swap_to_LE(*paddr);
		val &= GICR_WAKER_ChildrenAsleep;
	} while (val != 0);

	/* Enable_all_interrupts */
	for (int i=0; i<8; i++)
	{
		paddr = (unsigned int *) ((unsigned long) GICR_IPRIORITYRn(coreIdx) + i * 4);
		val = 0xa0a0a0a0;
		*paddr = _swap_to_LE(val);
	}

	/* Set all SGI and PPI interrupts in Secure Group 0 */
	*((unsigned int *) ((unsigned long) GICR_IGROUP0(coreIdx))) = _swap_to_LE(0x0);
	*((unsigned int *) ((unsigned long) GICR_IGRPMOD0(coreIdx))) = _swap_to_LE(0x0);

	/* Enable SGI and PPI (Private Peripheral Interrupts) */
	*((unsigned int *) ((unsigned long) GICR_ISENABLER0(coreIdx))) = _swap_to_LE(0xf);

	/* Core configuration - Interrupt masks PSTATE */
	asm ("msr DAIFclr, #0xf");

	return 0;
}

/* Configure GICv3 controller */
int configure_gic(unsigned int coreIdx)
{
	register unsigned long val = 0;

	/* test if we have a GICv3 */
	asm volatile("mrs %0, id_aa64pfr0_el1" : "=r" (val));
	if ((val & (1 << 24)) == 0)
	{
		return 1;
	}

	if (coreIdx == 0)
	{
		/* Enable the interrupt Groups and Affinity Routing (ARE) mode GICD_CTLR */
		val = GICD_CTLR_ARE_NS | GICD_CTLR_ARE_S | GICD_CTLR_ENABLE_G1S | GICD_CTLR_ENABLE_G1NS | GICD_CTLR_ENABLE_G0;
		*((unsigned int *) GICD_CTLR) = _swap_to_LE(val);
	}

	configure_gicc(coreIdx);
	configure_gicr(coreIdx);

	return 0;
}

void interruptHandler()
{
    unsigned int coreIdx = get_core_index(), iar = 0;
    asm volatile("mrs %0, S3_0_C12_C8_0" : "=r" (iar)); /* ICC_IAR0_EL1 */

    _handlerPrintCallback(coreIdx, iar);

    asm volatile("msr S3_0_C12_C8_1, %0" : : "r" (iar));  /* ICC_EOIR0_EL1 */
}

void sendall_sgi(unsigned int id)
{
	register unsigned long val = 0x10000000000;
	val |= ((id & 0xf) << 24);
	/* Generate SGI interrupt #id on all other cores	ICC_SGI0R_EL1 */
	asm volatile("msr S3_0_C12_C11_7, %0" : : "r" (val));
}
#endif
