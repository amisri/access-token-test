/*
 * Copyright 2015 - 2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 */

#ifndef GIC_V2_H_
#define GIC_V2_H_

/* ---------------- CHASSIS-BASED SOC ADDRESSES ---------------- */

/* Check this is the correct base address of the GICv2 module for your SoC */
/* Verified for LS1012A, LS1043A, LS1023A, LS1046A, LS1026A */
#define CONFIG_ARM_GIC_BASE_ADDRESS 0x1400000

/* ---------------- END OF CHASSIS-BASED ---------------- */

#if GIC_ADDRESS_ALIGNED == 0
	#define CONFIG_ARM_GICD_BASE_OFFSET 0x1000
	#define CONFIG_ARM_GICC_BASE_OFFSET 0x2000
#else
	#define CONFIG_ARM_GICD_BASE_OFFSET 0x10000
	#define CONFIG_ARM_GICC_BASE_OFFSET 0x2F000
#endif

#define CONFIG_ARM_GICD_BASE_ADDRESS (CONFIG_ARM_GIC_BASE_ADDRESS + CONFIG_ARM_GICD_BASE_OFFSET)
#define CONFIG_ARM_GICC_BASE_ADDRESS (CONFIG_ARM_GIC_BASE_ADDRESS + CONFIG_ARM_GICC_BASE_OFFSET)

#define GIC_CTLR_ENABLE_G1 (1U << 1) /* Enable Non-secure Group 1 interrupts */
#define GIC_CTLR_ENABLE_G0 (1U << 0) /* Enable Secure Group 0 interrupts */

/* GICD indicates a Distributor register */
#define GICD_CTLR (CONFIG_ARM_GICD_BASE_ADDRESS + 0x0000) /* Distributor Control Register */
#define GICD_TYPER (CONFIG_ARM_GICD_BASE_ADDRESS + 0x0004) /* Distributor Type Register */
#define GICD_IGROUPR0 (CONFIG_ARM_GICD_BASE_ADDRESS + 0x0080) /* Interrupt Group Registers */
#define GICD_ISENABLER0 (CONFIG_ARM_GICD_BASE_ADDRESS + 0x0100) /* Interrupt Set-Enable Registers */
#define GICD_ICENABLER0 (CONFIG_ARM_GICD_BASE_ADDRESS + 0x0180) /* Interrupt Clear-Enable Registers */
#define GICD_IPRIORITYR0 (CONFIG_ARM_GICD_BASE_ADDRESS + 0x0400) /* Interrupt Priority Registers */
#define GICD_ICFGR0 (CONFIG_ARM_GICD_BASE_ADDRESS + 0x0C00) /* Interrupt Configuration Registers */
#define GICD_SGIR (CONFIG_ARM_GICD_BASE_ADDRESS + 0x0F00) /* Software Generated Interrupt Register */
/* GICC indicates a CPU interface register */
#define GICC_CTLR (CONFIG_ARM_GICC_BASE_ADDRESS + 0x0000) /* CPU Interface Control Register */
#define GICC_PMR (CONFIG_ARM_GICC_BASE_ADDRESS + 0x0004) /* Priority Mask Register */
#define GICC_BPR (CONFIG_ARM_GICC_BASE_ADDRESS + 0x0008) /* Binary Point Register */
#define GICC_IAR (CONFIG_ARM_GICC_BASE_ADDRESS + 0x000C) /* Interrupt Acknowledge Register */
#define GICC_EOIR (CONFIG_ARM_GICC_BASE_ADDRESS + 0x0010) /* End of Interrupt Register */
#define GICC_CTLR_FIQEn (1U << 3) /* Enable signaling of Secure Group 0 interrupts as FIQ */
#define GICC_CTLR_CBPR (1U << 4) /* Enable common control of both groups with BPR */

void interruptHandler();
int configure_gic(unsigned int coreIdx);
void sendall_sgi(unsigned int id);

#endif /* GIC_V2_H_ */
