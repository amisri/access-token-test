/*
 * Copyright 2015 - 2016 Freescale Semiconductor, Inc.
 */

#ifndef GIC_V3_H_
#define GIC_V3_H_

/* ---------------- CHASSIS-BASED SOC ADDRESSES ---------------- */

/* Check this is the correct base address of the GICv3 module for your SoC */
/* Verified for LS1047A, LS1088A, LS2085A, LS2080A, LS2040A */
#define CONFIG_ARM_GIC_BASE_ADDRESS 0x6000000

/* ---------------- END OF CHASSIS-BASED ---------------- */

#define CONFIG_ARM_GIC_RDBASE_OFFSET 0x100000
#define CONFIG_ARM_GIC_RDBASE_CORE0 (CONFIG_ARM_GIC_BASE_ADDRESS + CONFIG_ARM_GIC_RDBASE_OFFSET)
#define CONFIG_ARM_GIC_SGIBASE_OFFSET 0x10000
#define CONFIG_ARM_GIC_RDBASE_CORE(x) (CONFIG_ARM_GIC_RDBASE_CORE0 + (CONFIG_ARM_GIC_SGIBASE_OFFSET * 2 * x))
/* GICD indicates a Distributor register */
/* GICR indicates a Re-distributor register */
/* GICR_I*R registers require CONFIG_ARM_GIC_SGIBASE_OFFSET */
#define GICD_CTLR (CONFIG_ARM_GIC_BASE_ADDRESS + 0x0000) /* Distributor Control Register */
#define GICD_TYPER (CONFIG_ARM_GIC_BASE_ADDRESS + 0x0004) /* Distributor Type Register */
#define GICD_CTLR_ARE_NS (1U << 5) /* Affinity Routing Enable - Non-Secure */
#define GICD_CTLR_ARE_S (1U << 4) /* Affinity Routing Enable - Secure */
#define GICD_CTLR_ENABLE_G1S (1U << 2)  /* Enable Secure Group 1 interrupts. */
#define GICD_CTLR_ENABLE_G1NS (1U << 1)  /* Enable Non-secure Group 1 interrupts */
#define GICD_CTLR_ENABLE_G0 (1U << 0) /* Enable Group 0 interrupts */
#define GICD_ISENABLERn (CONFIG_ARM_GIC_BASE_ADDRESS + 0x0100) /* Interrupt Set-Enable Registers */
#define GICD_ICENABLERn (CONFIG_ARM_GIC_BASE_ADDRESS + 0x0180) /* Interrupt Clear-Enable Registers */
#define GICD_IPRIORITYRn (CONFIG_ARM_GIC_BASE_ADDRESS + 0x0400) /* Interrupt Priority Registers */
#define GICD_ICFGR (CONFIG_ARM_GIC_BASE_ADDRESS + 0x0C00) /* Interrupt Configuration Registers */
#define GICD_IROUTERn (CONFIG_ARM_GIC_BASE_ADDRESS + 0x6100) /* Interrupt Routing Registers */
/* GICR indicates a Redistributor register */
#define GICR_WAKER(x) (CONFIG_ARM_GIC_RDBASE_CORE(x) + 0x0014)
/* GICR_WAKER defines whether the GIC-500 can be powered off if required. */
#define GICR_WAKER_ProcessorSleep (1U << 1) /* assert a WakeRequest if there is a pending interrupt */
#define GICR_WAKER_ChildrenAsleep (1U << 2) /* check for children active */ 

#define GICR_ISENABLER0(x) 	(CONFIG_ARM_GIC_RDBASE_CORE(x) + CONFIG_ARM_GIC_SGIBASE_OFFSET + 0x0100)
#define GICR_ICENABLER0(x) 	(CONFIG_ARM_GIC_RDBASE_CORE(x) + CONFIG_ARM_GIC_SGIBASE_OFFSET + 0x0180)
#define GICR_IPRIORITYRn(x) (CONFIG_ARM_GIC_RDBASE_CORE(x) + CONFIG_ARM_GIC_SGIBASE_OFFSET + 0x0400)
#define GICR_ICFGR(x) 		(CONFIG_ARM_GIC_RDBASE_CORE(x) + CONFIG_ARM_GIC_SGIBASE_OFFSET + 0x0C00)
#define GICR_IROUTERn(x) 	(CONFIG_ARM_GIC_RDBASE_CORE(x) + CONFIG_ARM_GIC_SGIBASE_OFFSET + 0x6100)
#define GICR_IGROUP0(x) 	(CONFIG_ARM_GIC_RDBASE_CORE(x) + CONFIG_ARM_GIC_SGIBASE_OFFSET + 0x0080)
#define GICR_IGRPMOD0(x) 	(CONFIG_ARM_GIC_RDBASE_CORE(x) + CONFIG_ARM_GIC_SGIBASE_OFFSET + 0x0D00)
/* ICC_SRE - Interrupt Controller System Register Enable */
#define ICC_SRE_EL1_SRE (1U << 0) /* System reg i/f (as opposed to memory mapped) enabled */
//#define ICC_IGRPEN1_EL1 S3_0_C12_C12_7 /* Group1 Interrupt Group Enable */
/* S3_<op1>_<Cn>_<Cm>_<op2>, IMPLEMENTATION DEFINED registers */

void interruptHandler();
int configure_gic(unsigned int coreIdx);
void sendall_sgi(unsigned int id);

#define isb() __asm__ __volatile__("isb" : : : "memory")
#define dsb() __asm__ __volatile__("dsb" : : : "memory")

#endif /* GIC_V3_H_ */
