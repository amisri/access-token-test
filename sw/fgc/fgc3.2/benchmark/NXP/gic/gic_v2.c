/*
 * Copyright 2015 - 2016 Freescale Semiconductor, Inc.
 */

#include "gic.h"

#if GIC_VER == 2

extern unsigned int get_core_index();

/* Swap values to Little-Endian format (GIC registers) */
unsigned int _swap_to_LE(unsigned int val)
{
#ifdef __AARCH64EB__
	asm("rev %w0, %w0" : "=r" (val) : "r" (val));
#endif
	return val;
}

/* REFERENCES */
/* ARM� Generic Interrupt Controller Architecture Specification GICv1 and GICv2. IHI 0048B */
/* CoreLink GIC-400 Generic Interrupt Controller TRM. DDI 0471B	*/

/* Enable SGI and PPI interrupts.
 * Interrupts 0-15 are SGIs
 * Interrupts 16-31 are PPIs
 * Should have to enable Distributor (GICR_*) and CPU interfaces (GCC_*)
 */

/* Configure the GIC Core interface */
int configure_gicc(unsigned int coreIdx)
{
	register unsigned long val = 0;

	/* Enable forwarding of pending interrupts (both groups) to CPU interfaces */
	val = GICC_CTLR_CBPR | GICC_CTLR_FIQEn | GIC_CTLR_ENABLE_G1 | GIC_CTLR_ENABLE_G0;
	*((unsigned int *) GICC_CTLR) = _swap_to_LE(val);

	/* Set priority mask and binary point registers */
	*((unsigned int *) GICC_PMR) = _swap_to_LE(0xf0); /* interrupts w/priority >0xf0 will be signaled */
	*((unsigned int *) GICC_BPR) = _swap_to_LE(0x3); /* set priority grouping schema to gggg.ssss */

	/* Core configuration - Interrupt masks PSTATE */
	asm ("msr DAIFclr, #0xf");

	return 0;
}

/* Configure the GIC Distributor interface */
int configure_gicd()
{
	register unsigned int val = 0;
	
	/* Enable forwarding of pending interrupts (both groups) to CPU interfaces */
	val = GIC_CTLR_ENABLE_G1 | GIC_CTLR_ENABLE_G0;
	*((unsigned int *) GICD_CTLR) = _swap_to_LE(val);

	/* Enable_all_interrupts */
	for (int i=0; i<2; i++)
	{
		*((unsigned int *) ((unsigned long) GICD_IPRIORITYR0 + i * 4)) = _swap_to_LE(0xa0a0a0a0);
	}
	for (int i=2; i<4; i++)
	{
		*((unsigned int *) ((unsigned long) GICD_IPRIORITYR0 + i * 4)) = _swap_to_LE(0x80808080);
	}

	/* Set SGIID[8-15] in Secure Group 0 and SGIID[0-7] in Non-secure Group 1 */
	*((unsigned int *) GICD_IGROUPR0) = _swap_to_LE(0xff);	// this doesn't seem to work

	/* Enable all SGI Interrupts - this is by default */
	*((unsigned int *) GICD_ISENABLER0) = _swap_to_LE(0xffff);

	printf("   configure_gicd    ");
	
	return 0;
}

/* Configure GICv2 controller */
int configure_gic(unsigned int coreIdx)
{
	register unsigned long val = 0;

	/* test if we have a GICv2 */
	asm volatile("mrs %0, id_aa64pfr0_el1" : "=r" (val));
	if ((val & (1 << 24)) != 0)
	{
		return 1;
	}

	if (coreIdx == 0)	/* Only one distributor per cluster(processor) */
	{
		configure_gicd();
	}

	configure_gicc(coreIdx);	

	return 0;
}

void interruptHandler()
{
    unsigned int coreIdx = get_core_index(), iar = 0;
    iar = *((unsigned int *) GICC_IAR);	/* acknowledge interrupt */

    _handlerPrintCallback(coreIdx, _swap_to_LE(iar));

    *((unsigned int *) GICC_EOIR) = iar; /* deactivate interrupt */
}

void sendall_sgi(unsigned int id)
{
	register unsigned long val = 0x1000000;	/* send to all except me */
	val |= (id & 0xf);
	
	/* triggering Non-secure Group 1 does not see to work
	 * the split in IGROUPR0 seems to be ignored
	if (id < 8)
	{
		val |= (1U < 15);
	}*/

	/* Generate SGI interrupt #id on all other cores */
	*((unsigned int *) GICD_SGIR) = _swap_to_LE(val);
}
#endif
