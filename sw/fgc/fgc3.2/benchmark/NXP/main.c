//--specs="${ProjDirPath}/lib/uart.specs"

#include <stdint.h>
#include "byteswap.h"

typedef unsigned char byte;


// ************************************************************
// Helper functions
// ************************************************************

// Bit field 
#define bf(x)				   ( 1 << (x) )

// Bit operations on 8 bit registers
#define bSet(reg, mask)        ( (*(volatile byte *)(reg)) |=  (mask) )
#define bClear(reg, mask)      ( (*(volatile byte *)(reg)) &= ~(mask) )

#define bIsOne(reg, mask)      ( ((*(volatile byte *)(reg)) & (mask)) == (mask) )

#define bIn(reg)               ( (*(volatile byte *)(reg)) ) 
#define bOut(reg, val)         ( (*(volatile byte *)(reg)) = (byte)(val) ) 

// Bit operations on 16 bit registers
#define b16Set(reg, mask)      ( (*(volatile uint16_t *)(reg)) |=  (mask) )
#define bvClear(reg, mask)    ( (*(volatile uint16_t *)(reg)) &= ~(mask) )

#define b16IsOne(reg, mask)    ( ((*(volatile uint16_t *)(reg)) & (mask)) == (mask) )

#define b16In(reg)             ( (*(volatile uint16_t *)(reg)) ) 
#define b16ut(reg, val)       ( (*(volatile uint16_t *)(reg)) = (uint16_t)(val) ) 


// Bit operations on 32 bit registers
#define b32Set(reg, mask)      ( (*(volatile uint32_t *)(reg)) |=  (mask) )
#define b32Clear(reg, mask)    ( (*(volatile uint32_t *)(reg)) &= ~(mask) )

#define b32IsOne(reg, mask)    ( ((*(volatile uint32_t *)(reg)) & (mask)) == (mask) )

#define b32In(reg)             ( (*(volatile uint32_t *)(reg)) ) 
#define b32Out(reg, val)       ( (*(volatile uint32_t *)(reg)) = (uint32_t)(val) ) 


// Bit operations on variables
#define bitSet(var, mask)      ( var |=  (mask) )
#define bitClear(var, mask)    ( var &= ~(mask) )



// ************************************************************
// UART registers
// ************************************************************

#define DUART_BASE 		0x21C0000		
#define UART_BASE 		0x500		

#define UDLB 			(DUART_BASE + UART_BASE + 0x0) 
#define URBR 			(DUART_BASE + UART_BASE + 0x0)
#define UTHR 			(DUART_BASE + UART_BASE + 0x0)
#define UDMB 			(DUART_BASE + UART_BASE + 0x1)
#define UIER 			(DUART_BASE + UART_BASE + 0x1)
#define UAFR 			(DUART_BASE + UART_BASE + 0x2)
#define UFCR 			(DUART_BASE + UART_BASE + 0x2)
#define UIIR 			(DUART_BASE + UART_BASE + 0x2)
#define ULCR 			(DUART_BASE + UART_BASE + 0x3)
#define UMCR 			(DUART_BASE + UART_BASE + 0x4)
#define ULSR 			(DUART_BASE + UART_BASE + 0x5)
#define UMSR 			(DUART_BASE + UART_BASE + 0x6)
#define USCR 			(DUART_BASE + UART_BASE + 0x7)
#define UDSR  			(DUART_BASE + UART_BASE + 0x10)



// ************************************************************
// UART bit fields
// ************************************************************

// Registry UIER - interrupt enable register
#define EMSI 			bf(3)
#define ERLSI 			bf(2)
#define ETHREI 			bf(1)
#define ERDAI 			bf(0)


// Registry UAFR - alternate function register
#define BO	 			bf(1)
#define CW  			bf(0)


// Registry UFCR - FIFO control register
#define RTL1 			bf(7)
#define RTL0 			bf(6)
#define EN64 			bf(5)
#define DMS 			bf(3)
#define TFR 			bf(2)
#define RFR				bf(1)
#define FEN 			bf(0)


// Registry UIIR - interrupt ID register
#define FE1 			bf(7)
#define FE0 			bf(6)
#define FE64 			bf(5)
#define IID3 			bf(3)
#define IID2 			bf(2)
#define IID1			bf(1)
#define IID0 			bf(0)


// Registry ULCR - line control register
#define DLAB 			bf(7)
#define SB	 			bf(6)
#define SP  			bf(5)
#define EPS				bf(4)
#define PEN 			bf(3)
#define NSTB 			bf(2)
#define WLS1			bf(1)
#define WLS0 			bf(0)


// Registry UMCR - modem control register
#define AFE  			bf(5)
#define LOOP			bf(4)
#define RTS				bf(1)


// Registry ULSR1 - line status register
#define RFE 			bf(7)
#define TEMT 			bf(6)
#define THRE  			bf(5)
#define BI				bf(4)
#define FE	 			bf(3)
#define PE	 			bf(2)
#define OE 				bf(1)
#define DR	 			bf(0)


// Registry UMSR - modem status register
#define CTS				bf(4)
#define DCTS 			bf(0)


// Registry UDSR - DMA status register
#define TXRDY			bf(1)
#define RXRDY 			bf(0)



// ************************************************************
// UART functions
// ************************************************************

void uartSendChar(byte c)
{
	// Clear DLAB bit in the control register to make UTHR accessible
	// bClear(ULCR, DLAB);
	
	// Write to UTHR to send data
	bOut(UTHR, c);
}

void uartWaitForSend()
{
	while (!bIsOne(ULSR, TEMT));
}

void uartInit()
{
	// DLAB - set DLAB bit in the control register to make UDLB accessible
	// WLS  - set word length to 8 bits
	bOut(ULCR, DLAB | WLS0 | WLS1);
	 
	// Set divisor - 163
	bOut(UDLB, 163);
	bOut(UDMB, 0);	
	
	// Clear DLAB
	bOut(ULCR, WLS0 | WLS1);
}

void waitSleep() 
{
	int counter = 0; 
	
	for (int i = 0; i < 99999999; ++i)
	{
		counter += i;
	} 
}



// ************************************************************
// Flex Timer registers
// ************************************************************

#define FTM1_SC        0x29D0000
#define FTM1_CNT       0x29D0004
#define FTM1_MOD       0x29D0008
#define FTM1_C0SC      0x29D000C
#define FTM1_C0V       0x29D0010
#define FTM1_C1SC      0x29D0014
#define FTM1_C1V       0x29D0018
#define FTM1_C2SC      0x29D001C
#define FTM1_C2V       0x29D0020
#define FTM1_C3SC      0x29D0024
#define FTM1_C3V       0x29D0028
#define FTM1_C4SC      0x29D002C
#define FTM1_C4V       0x29D0030
#define FTM1_C5SC      0x29D0034
#define FTM1_C5V       0x29D0038
#define FTM1_C6SC      0x29D003C
#define FTM1_C6V       0x29D0040
#define FTM1_C7SC      0x29D0044
#define FTM1_C7V       0x29D0048
#define FTM1_CNTIN     0x29D004C
#define FTM1_STATUS    0x29D0050
#define FTM1_MODE      0x29D0054
#define FTM1_SYNC      0x29D0058
#define FTM1_OUTINIT   0x29D005C
#define FTM1_OUTMASK   0x29D0060
#define FTM1_COMBINE   0x29D0064
#define FTM1_DEADTIME  0x29D0068
#define FTM1_EXTTRIG   0x29D006C
#define FTM1_POL       0x29D0070
#define FTM1_FMS       0x29D0074
#define FTM1_FILTER    0x29D0078
#define FTM1_FLTCTRL   0x29D007C
#define FTM1_QDCTRL    0x29D0080
#define FTM1_CONF      0x29D0084
#define FTM1_FLTPOL    0x29D0088
#define FTM1_SYNCONF   0x29D008C
#define FTM1_INVCTRL   0x29D0090
#define FTM1_SWOCTRL   0x29D0094
#define FTM1_PWMLOAD   0x29D0098



// ************************************************************
// Flex Timer bit fields
// ************************************************************

// Register SC - line control register
#define TOF 			bf(31 - 24)
#define TOIE			bf(31 - 25)
#define CPWMS  			bf(31 - 26)
#define CLKS1			bf(31 - 27)
#define CLKS0			bf(31 - 28)
#define PS2 			bf(31 - 29)
#define PS1				bf(31 - 30)
#define PS0	 			bf(31 - 31)


// ************************************************************
// Flex Timer functions
// ************************************************************

void timerStart()
{
	// Reset timer counter value
	b32Out(FTM1_CNT, 0);
	
	// Set clocking source to system clock
	uint32_t tmp = bs32(b32In(FTM1_SC));
	bitSet(tmp, CLKS1);
	b32Out(FTM1_SC, bs32(tmp));
}

void timerStop()
{
	// Disable clocking source
	uint32_t tmp = bs32(b32In(FTM1_SC));
	bitClear(tmp, CLKS0);
	bitClear(tmp, CLKS1);
	b32Out(FTM1_SC, bs32(tmp));	
}

void timerInit()
{		
	// Disable clocking source
	timerStop();

	// Set MOD (final) value to max value of 16-bit timer
	b32Out(FTM1_MOD, bs32(0xFFFF));		

	// Set CNTIN (initial) value to 0
	b32Out(FTM1_CNTIN, 0);	
	
	// Set prescaler to 32  == Timer frequency is 32 768 Hz / 32 == 1024 Hz
	//						== Timer tick is 0.000976562 s
 	//						== Timer overflow is 64 s
	uint32_t SC = bs32(b32In(FTM1_SC));
	
	bitSet(SC, PS1);
	bitSet(SC, PS2);
	
	b32Out(FTM1_SC, bs32(SC));	
	
	// Reset timer counter value
	b32Out(FTM1_CNT, 0);
}

uint32_t timerRead()
{
	return (bs32(b32In(FTM1_CNT)));
}



// ************************************************************
// Evaluation function implementation
// ************************************************************

// ******************************************************
// An address of a memory region used for performing memory tests

#define EVALP_MEM_TEST_ADDRESS 0x86400000


// ******************************************************
// Include the evaluation code
#define EVAL_NO_PRINTF_SYSCALLS
#define EVAL_NO_HARDWARE_TIMER

#include <eval/eval.h>
#include <eval/func.impl.h>
 

// ***********************************************
// UART functions
void evalWriteChar(const char c)
{
	uartSendChar(c);
	uartWaitForSend();
}


char evalReadChar()
{
    return 0;
}


char evalCheckChar()
{
	return 0;
}

// ***********************************************
// Sleeping functions

void evalMicroSleep(unsigned int micro)
{
	waitSleep();
}

void evalSleep(unsigned int seconds)
{
	waitSleep();
}


// ******************************************************
// Timer functions
 
void evalTimerStart()
{
	timerStart(); 
}

void evalTimerStop()
{
	timerStop();
} 

double evalTimerRead()
{
	uint32_t timerValue = timerRead();
	
	// 32768 Hz / 64 == 512 Hz. 1024 Hz == 0.001953125 s
	return (0.001953125 * timerValue);
}
 
  
 
// ************************************************************
// Main function
// ************************************************************
 
int main(void)
{ 	
	// Init Timer 
	timerInit();   
	     
    // Init evaluation 
    evalInit("NXP 9", "-O0 no -ftree-vectorize");
    	  
    for (;;)
    {
    	// Perform benchmarks
    	evalPerform(EVAL_ALL);

    	// Wait for user input and start again
    	evalWaitForUser();
    } 
	
	return 0;
}
