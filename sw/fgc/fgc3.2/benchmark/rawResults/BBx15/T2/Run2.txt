-----------------------------------
Program started on BeagleBoard x15.
Compiler: GCC 6.3.1 20170215 (release) [ARM/embedded-6-branch revision 245512] -O2 -ftree-vectorize. hard float.

Benchmark started: Simple iteration
   Number of iterations 1593835520
   Time taken = 0.0 seconds
Benchmark finished.

Benchmark started: PI calculation
   Number of iterations 99990000
   PI value   = 3.0
   Time taken = 12.8434087 seconds
Benchmark finished.

Benchmark started: Bubble sort
   Size of the array 35000
   Array 10: -34998 -34997 -34996 -34995 -34994 -34993 -34992 -34991 -34990 -34989 
   Time taken = 9.1037330 seconds
Benchmark finished.

Benchmark started: Quick sort
   Size of the array 35000
   Time taken = 3.121571 seconds
Benchmark finished.

Benchmark started: Fast Fourier transform
   Size of the array 1048576
   Time taken = 4.6839636 seconds
Benchmark finished.

Benchmark started: N-body problem
   Number of bodies 3000
   Number of iterations 20
   Time taken = 38.169439 seconds
Benchmark finished.

Benchmark started: SHA256
   Number of iterations 15000
   Time taken = 5.4661400 seconds
Benchmark finished.

Benchmark started: Simple memory test
   Number of memory bytes to test 524288000

   Read char                  = 1.3202391 seconds
   Read double                = 0.2092314 seconds
   Read char,   skipping   3  = 1.6235270 seconds
   Read char,   skipping   8  = 2.2847300 seconds
   Read char,   skipping  32  = 5.4523458 seconds
   Read char,   skipping  33  = 5.7383300 seconds
   Read char,   skipping 512  = 15.7168920 seconds
   Read double, skipping   3  = 0.5171885 seconds
   Read double, skipping   8  = 1.2477589 seconds
   Read double, skipping  32  = 1.9664883 seconds
   Read double, skipping  33  = 2.205662 seconds
   Read double, skipping 512  = 2.7386853 seconds

   Write char                 = 0.6994725 seconds
   Write double               = 0.1748681 seconds
   Write char,   skipping   3 = 1.521690 seconds
   Write char,   skipping   8 = 2.7428662 seconds
   Write char,   skipping  32 = 10.9493090 seconds
   Write char,   skipping  33 = 11.5305854 seconds
   Write char,   skipping 512 = 37.7529632 seconds
   Write double, skipping   3 = 1.280598 seconds
   Write double, skipping   8 = 3.1467414 seconds
   Write double, skipping  32 = 5.5454868 seconds
   Write double, skipping  33 = 3.3526464 seconds
   Write double, skipping 512 = 4.1621058 seconds

   Copy char                  = 1.680079 seconds
   Copy double                = 0.2853127 seconds
   Copy char,   skipping   3  = 2.190708 seconds
   Copy char,   skipping   8  = 4.7348677 seconds
   Copy char,   skipping  32  = 17.3308670 seconds
   Copy char,   skipping  33  = 17.8660306 seconds
   Copy char,   skipping 512  = 54.3768808 seconds
   Copy double, skipping   3  = 1.6482466 seconds
   Copy double, skipping   8  = 4.3174119 seconds
   Copy double, skipping  32  = 7.3032930 seconds
   Copy double, skipping  33  = 6.1761107 seconds
   Copy double, skipping 512  = 6.7299819 seconds
Benchmark finished.

Benchmark started: CoreMark
   Number of iterations 30000
   Time taken = 6.7255873 seconds
   Iterations/sec = 4460.5769582
Cannot validate operation for these seed values, please compare with results on a known platform.
   Time taken = 6.7255873 seconds
Benchmark finished.

Benchmark started: Linpack
   Size of the array    250
   Number of iterations 10
   Time taken = 6.1664060 seconds
Benchmark finished.

Final result:
Iterate, 0.0
picalc, 12.8434087
bubble, 9.1037330
quickSort, 3.121571
fft, 4.6839636
nbody, 38.169439
sha, 5.4661400
readChar0, 1.3202391
readDouble0, 0.2092314
readChar3, 1.6235270
readChar8, 2.2847300
readChar32, 5.4523458
readChar33, 5.7383300
readChar512, 15.7168920
readDouble3, 0.5171885
readDouble8, 1.2477589
readDouble32, 1.9664883
readDouble33, 2.205662
readDouble512, 2.7386853
writeChar0, 0.6994725
writeDouble0, 0.1748681
writeChar3, 1.521690
writeChar8, 2.7428662
writeChar32, 10.9493090
writeChar33, 11.5305854
writeChar512, 37.7529632
writeDouble3, 1.280598
writeDouble8, 3.1467414
writeDouble32, 5.5454868
writeDouble33, 3.3526464
writeDouble512, 4.1621058
copyChar0, 1.680079
copyDouble0, 0.2853127
copyChar3, 2.190708
copyChar8, 4.7348677
copyChar32, 17.3308670
copyChar33, 17.8660306
copyChar512, 54.3768808
copyDouble3, 1.6482466
copyDouble8, 4.3174119
copyDouble32, 7.3032930
copyDouble33, 6.1761107
copyDouble512, 6.7299819
coreMark, 6.7255873
linpack, 6.1664060
