-----------------------------------
Program started on Zynq.
Compiler: GCC 6.2.1 20161114 -O2 -ftree-vectorize.

Benchmark started: Simple iteration
   Number of iterations 1593835520
   Time taken = 0.80 seconds
Benchmark finished.

Benchmark started: PI calculation
   Number of iterations 99990000
   PI value   = 3.0
   Time taken = 10.4864959 seconds
Benchmark finished.

Benchmark started: Bubble sort
   Size of the array 35000
   Array 10: -34998.500000 -34997.500000 -34996.500000 -34995.500000 -34994.500000 -34993.500000 -34992.500000 -34991.500000 -34990.500000 -34989.500000 
   Time taken = 4.4029039 seconds
Benchmark finished.

Benchmark started: Quick sort
   Size of the array 35000
   Time taken = 4.6505519 seconds
Benchmark finished.

Benchmark started: Fast Fourier transform
   Size of the array 1048576
   Time taken = 5.3649279 seconds
Benchmark finished.

Benchmark started: N-body problem
   Number of bodies 3000
   Number of iterations 20
   Time taken = 28.9950799 seconds
Benchmark finished.

Benchmark started: SHA256
   Number of iterations 15000
   Time taken = 4.4788319 seconds
Benchmark finished.

Benchmark started: Simple memory test
   Number of memory bytes to test 524288000

   Read char                  = 0.8917600 seconds
   Read double                = 0.1967440 seconds
   Read char,   skipping   3  = 1.3522159 seconds
   Read char,   skipping   8  = 1.6541839 seconds
   Read char,   skipping  32  = 6.1691759 seconds
   Read char,   skipping  33  = 6.2961999 seconds
   Read char,   skipping 512  = 29.3269519 seconds
   Read double, skipping   3  = 0.5880799 seconds
   Read double, skipping   8  = 1.5759120 seconds
   Read double, skipping  32  = 1.9061040 seconds
   Read double, skipping  33  = 5.8540640 seconds
   Read double, skipping 512  = 8.1180799 seconds

   Write char                 = 0.8742480 seconds
   Write double               = 0.2187040 seconds
   Write char,   skipping   3 = 1.3378719 seconds
   Write char,   skipping   8 = 1.5809119 seconds
   Write char,   skipping  32 = 6.1246399 seconds
   Write char,   skipping  33 = 6.3202239 seconds
   Write char,   skipping 512 = 14.4969679 seconds
   Write double, skipping   3 = 0.5758799 seconds
   Write double, skipping   8 = 1.6022639 seconds
   Write double, skipping  32 = 1.5263359 seconds
   Write double, skipping  33 = 2.2667679 seconds
   Write double, skipping 512 = 2.2537279 seconds

   Copy char                  = 1.3286400 seconds
   Copy double                = 0.2835360 seconds
   Copy char,   skipping   3  = 2.406719 seconds
   Copy char,   skipping   8  = 4.4198800 seconds
   Copy char,   skipping  32  = 18.4777360 seconds
   Copy char,   skipping  33  = 18.9373439 seconds
   Copy char,   skipping 512  = 49.1283199 seconds
   Copy double, skipping   3  = 1.6239439 seconds
   Copy double, skipping   8  = 4.213839 seconds
   Copy double, skipping  32  = 4.2411120 seconds
   Copy double, skipping  33  = 7.7331040 seconds
   Copy double, skipping 512  = 10.7137039 seconds
Benchmark finished.

Benchmark started: CoreMark
   Number of iterations 30000
   Time taken = 7.6229920 seconds
   Iterations/sec = 3935.4626110
   Correct operation validated.
   Time taken = 7.6229920 seconds
Benchmark finished.

Benchmark started: Linpack
   Size of the array    250
   Number of iterations 10
   Time taken = 9.5125840 seconds
Benchmark finished.

Final result:
Iterate, 0.80
picalc, 10.4864959
bubble, 4.4029039
quickSort, 4.6505519
fft, 5.3649279
nbody, 28.9950799
sha, 4.4788319
readChar0, 0.8917600
readDouble0, 0.1967440
readChar3, 1.3522159
readChar8, 1.6541839
readChar32, 6.1691759
readChar33, 6.2961999
readChar512, 29.3269519
readDouble3, 0.5880799
readDouble8, 1.5759120
readDouble32, 1.9061040
readDouble33, 5.8540640
readDouble512, 8.1180799
writeChar0, 0.8742480
writeDouble0, 0.2187040
writeChar3, 1.3378719
writeChar8, 1.5809119
writeChar32, 6.1246399
writeChar33, 6.3202239
writeChar512, 14.4969679
writeDouble3, 0.5758799
writeDouble8, 1.6022639
writeDouble32, 1.5263359
writeDouble33, 2.2667679
writeDouble512, 2.2537279
copyChar0, 1.3286400
copyDouble0, 0.2835360
copyChar3, 2.406719
copyChar8, 4.4198800
copyChar32, 18.4777360
copyChar33, 18.9373439
copyChar512, 49.1283199
copyDouble3, 1.6239439
copyDouble8, 4.213839
copyDouble32, 4.2411120
copyDouble33, 7.7331040
copyDouble512, 10.7137039
coreMark, 7.6229920
linpack, 9.5125840