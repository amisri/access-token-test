# Code for FGC3.2 candidate processor's benchmark.

----------------------------------------------------------------------------------------

## 1. Bare-metal benchmarking framework

All the benchmarks are written in a generic way, that is platform-independent way,
so all the code can be easily reused for each tested hardware platform.

For the simplicity of use, the framework is provided as (almost-)header-only library,
so only the file `eval.h` and `func.impl.h` has to be included in
the main file for each platform.
The only exception is the CoreMark benchmarks which requires few C files to be compiled.

There are few functions or `#defines` that has to be defined for each platform
(like timing functions, printf functions etc.) in order to use benchmarks.
The aforementioned things are declarated in the `platformPort.h` file.

The framework provides simple API to run all the tests
and it presents the outcome to the user via UART terminal (printf functions)
in both human-readable form and CSV-like style.


----------------------------------------------------------------------------------------


## 2. The use of the framework

Every benchmark code and the framework code is located in the `eval` directory.

For each platform there is a separate folder (e.g. `Zynq`)
that contains implementation of platform-specific functions and the `main` function.

Those folders do not keep the whole projects (as they may be big and impractical)
but only the source file.
There is also a `readme.txt` file that provides additional information that may be
useful to recreate the project, if needed.


----------------------------------------------------------------------------------------        



## 3. Run conditions.

The following has to be taken into account when running the benchmarks:

- Optimization
    - none (O0)
    - more (O2)
- No debug data and options enabled
- Cache enable
    - data
    - instruction
- FPU
    - Neon (for 32 bit)
    - Auto-vectorization (-ftree-vectorize)


----------------------------------------------------------------------------------------        



## 4. The following benchmarks have been chosen:

1.  **EVAL_TIMERTEST**
        Test program just to test correctness of the timer measuring.
        It invokes platform’s sleeping functions and then measure the time of sleep
        to check if the output is the same.

2.  **EVAL_ITERATE**
        Very simple program that consists of a loop that
        increase value of an int variable many many times.

3.  **EVAL_PICALC**
        Program that calculates the value of PI for a given number of iterations.

4.  **EVAL_BUBBLE**
        This benchmark sorts an array of decreasing number in ascending order using bubble sort.

5.  **EVAL_QUICK**
        This benchmark sorts an array of decreasing number in ascending order using quick sort.

6.  **EVAL_FFT**
        Calculates Fast Fourier Transform algorithm.

7.  **EVAL_NBODY**
        This benchmark solves 3D N-body problem for a given number of bodies.

8.  **EVAL_SHA**
        Computes SHA hash many times.

9.  **EVAL_MEMTEST**
        This benchmark actually consists of many smaller ones
        that are use to benchmark memory performance. Sub-benchmarks are:

        - Reading char and reading double in order
        - Reading char and reading double skipping every 3, 8, 32, 33, 512 bytes.

        - Writing char and writing double in order
        - Writing char and writing double skipping every 3, 8, 32, 33, 512 bytes.

        - Copying char and copying double in order
        - Copying char and copying double skipping every 3, 8, 32, 33, 512 bytes.

10. **EVAL_COREMARK**
        This is a professional benchmark created by the EEMBC.
        It is now a widely used benchmark for measuring the performance of bare-metal systems
        and is it supposed to replace very old Dhrystone benchmark and its flaws.
        Its code was obtained from http://www.eembc.org/coremark/download.php
        and customized a little to fit into this framework.
        The benchmark normally gives results in its own units called CoreMarks.

        For reading and writing tests, a registry variable (register R12) is used
        to minimize the impact of additional memory access
        (so only actual reading and writing from the given memory is measured)

11. **EVAL_LINPACK**
        Linpack is a classic benchmark used to measure floating-point operations performance.
        Its code was obtained from http://www.roylongbottom.org.uk/index.htm
        and customized a little, similarly to CoreMark.
