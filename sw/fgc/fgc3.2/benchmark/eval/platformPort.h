#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Prototypes of functions used to interact with the platform.
 *  Function should be re-implemented for the particular platform.
 *
 ***************************************/

#include <eval/common.h>


// ******************************************************
// Used to print characters to a serial port

__weak void evalWriteChar(const char c);



// ******************************************************
// Used to read characters from a serial port

__weak char evalReadChar();



// ******************************************************
// Used to read character from a serial port in a non-blocking way

__weak char evalCheckChar();



// ******************************************************
// Sleep microseconds

__weak void evalMicroSleep(unsigned int micro);



// ******************************************************
// Sleep seconds

__weak void evalSleep(unsigned int micro);


// ******************************************************
// Timer functions - those are implemented with __weak.
// The default __weak implementation requires evalIoTimReset, evalIoTimSck and evalIoTimOut.
// Those functions can be overwritten if the separate, hardware timer is not used.

void   evalTimerStart();

void   evalTimerStop();

double evalTimerRead();



// ******************************************************
// Set timer reset line - required by the default __weak implementation of timer

__weak void evalIoTimReset(bool state);



// ******************************************************
// Set timer SCK (serial clock) line - required by the default __weak implementation of timer

__weak void evalIoTimSck(bool state);



// ******************************************************
// Read timer data out line - required by the default __weak implementation of timer

__weak bool evalIoTimOut();





// ******************************************************
// An address of a memory region used for performing memory tests
// Has to be defined before including this library

// For example: #define EVALP_MEM_TEST_ADDRESS 0x800000000





