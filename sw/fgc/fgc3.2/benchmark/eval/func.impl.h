#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Implementation of evaluation functions.
 *  To be included only once in main C file.
 *
 ***************************************/

#include <eval/io.h>
#include <eval/timer.h>
#include <eval/platformPort.h>

#include <errno.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/unistd.h>

/* The following may defined BEFORE including this file:
      EVAL_NO_PRINTF_SYSCALLS   - to provide different implementation for functions required by the printf
      EVAL_NO_HARDWARE_TIMER    - to provide different implementation for timer functions if not hardware timer is used
 */

// ******************************************************
// Functions used to make use of printf()

void evalPrint(const char* format, ...)
{
    va_list args;
    va_start(args, format);

    vfprintf(stdout, format, args);

    va_end(args);
}



void evalPrintln(const char* format, ...)
{
    va_list args;
    va_start(args, format);

    vfprintf(stdout, format, args);
    printf("\n\r");

    va_end(args);
}




// ******************************************************
// Should be invoked during initialization

void evalPrintSetup()
{
    // Turn off buffers, so I/O occurs immediately
    setvbuf(stdin,  NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

 
// ******************************************************
// Functions used to print time taken and log it

char* logBuffer;
int   logOffset;


// Printing double as some platforms have problems with this...
void evalLogDouble(double number, int digits, const char* logFormat, int param)
{
    // Get integer part
    int intPart = (int)number;

    // Print integer part and param if present
    if (param < 0)
    {
        logOffset += sprintf(logBuffer + logOffset, "%s, %d", logFormat, intPart);
    }
    else
    {
        logOffset += sprintf(logBuffer + logOffset, "%s%d, %d", logFormat, param, intPart);
    }

    // Remove integer part
    number -= intPart;

    int power = 1;
    for (int i = 0; i < digits; ++i)
        power *= 10;

    // Multiply by the number of digits to move factorial part to integer part
    number *= power;

    // Print factorial part
    logOffset += sprintf(logBuffer + logOffset, ".%d\n\r", (int)number);
}


// Printing double as some platforms have problems with this...
void evalPrintDouble(double number, int digits)
{
    // Get integer part
    int intPart = (int)number;

    // Print integer part
    printf("%d", intPart);

    // Remove integer part
    number -= intPart;

    int power = 1;
    for (int i = 0; i < digits; ++i)
        power *= 10;

    // Multiply by the number of digits to move factorial part to integer part
    number *= power;

    // Print factorial part
    printf(".%d", (int)number);
}


void evalPrintTime(const char* logFormat, double time)
{
    // Print to screen
    printf("   Time taken = " CRED);
    evalPrintDouble(time, 7);
    printf(CWHITE " seconds\n\r" CN);

    // Print to log
    evalLogDouble(time, 7, logFormat, -1);
    //logOffset += sprintf(logBuffer + logOffset, logFormat, time);
}

void evalLogTime(const char* logFormat, int param, double time)
{
    // Print to log
    //logOffset += sprintf(logBuffer + logOffset, logFormat, param, time);
    evalLogDouble(time, 7, logFormat, param);
}


void evalPrintLog()
{
    printf("%s", logBuffer);
}


// ******************************************************
// Used to write buffer by printf

int _write (int file, char *data, int len)
{
    for (int i = 0; i < len; ++i)
    {
        evalWriteChar(data[i]);
    }

    return len;
}



// ******************************************************
// Used to read into buffer by scanf

int _read(int file, char *data, int len)
{
    for (int i = 0; i < len; ++i)
    {
        data[i] = evalReadChar();
    }

    return len;
}



// ******************************************************
// Other I/O related functions

#ifndef EVAL_NO_PRINTF_SYSCALLS

int _lseek(int file, int ptr, int dir)
{
    return 0;
}



int _fstat(int file, struct stat *st)
{
    st->st_mode = S_IFCHR;

    return 0;
}



int _isatty(int file)
{
    return ((file == STDOUT_FILENO) || (file == STDIN_FILENO) || (file == STDERR_FILENO));
}



int _close(int file)
{
    return -1;
}



void _exit(int status)
{
    while (1);
}


caddr_t _sbrk(int incr)
{
    extern char end asm("end");
    static char *heap_end;
    char *prev_heap_end;

    if (heap_end == 0) {
    heap_end = &end;
    }

    prev_heap_end = heap_end;

    heap_end += incr;

    return (caddr_t)prev_heap_end;
}


#endif


#ifndef EVAL_NO_HARDWARE_TIMER


// ******************************************************
// Function to zero and start the timer

void evalTimerStart()
{
    // Reset SCK line to 0
    evalIoTimSck(0);

    // Set reset line to 0 which will put the timer into reset state
    evalIoTimReset(0);

    // Wait in reset state
    evalMicroSleep(5000);

    // Set reset line to 1 which will release the reset
    evalIoTimReset(1);

    // Wait for the timer to start up (100 ms)
    evalMicroSleep(100000);

    // Set the SCK line to 1 which will start the timer
    evalIoTimSck(1);
}



// ******************************************************
// Function to stop the timer

void evalTimerStop()
{
    // Set the SCK line to 0 which will stop the timer and output first bit
    evalIoTimSck(0);
}



// ******************************************************
// Function to read value from the timer

double evalTimerRead()
{
    unsigned long long timerValue        = 0;
    unsigned long long timerCounter      = 0;
    unsigned long long timerValuePower   = 1;
    unsigned long long timerCounterPower = 1;

    // Read the timer
    for (int i = 0; i < 32; ++i)
    {
        // Wait 5 ms (the hardware timer will output next bit in this time)
        evalMicroSleep(5000);

        if (i < 16)
        {
            // Read TimerOut line and calculate value
            timerValue      += timerValuePower * evalIoTimOut();
            timerValuePower *= 2;
        }
        else
        {
            // Read TimerOut line and calculate value
            timerCounter      += timerCounterPower * evalIoTimOut();
            timerCounterPower *= 2;
        }

        // Set SCK line to 1
        evalIoTimSck(1);

        // Wait 5 ms
        evalMicroSleep(5000);

        // Set SCK line to 1 which will cause hardware timer to output next bit
        evalIoTimSck(0);

        //while(!evalCheckChar());
    }

    // Set reset line to 0 which will hold the hardware timer in reset
    evalIoTimReset(0);

    // Calculate return value
    return (timerValue + (timerCounter * 256 * 256)) * 0.000008;
}

#endif
