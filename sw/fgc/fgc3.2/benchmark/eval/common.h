#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Common things to use across the evaluation code.
 *
 ***************************************/

#include <stdint.h>

// Make function a weak function (GCC)
#define __weak __attribute__((weak))


// Typedef for boolean
typedef unsigned char bool;


// Typedef for byte
typedef unsigned char byte;
