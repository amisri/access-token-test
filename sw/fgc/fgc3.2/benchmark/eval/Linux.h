#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Platform port for POSIX. It is intended to be include as the only file for the Linux.
 *
 ***************************************/

// Define that this is Linux (for conditional compilation in few places)
#define EVAL_IS_LINUX

// Do not implement printf functions in eval code (as it is provided by Linux)
#define EVAL_NO_PRINTF_SYSCALLS

// Do not use hardware timer (as it is provided by Linux)
#define EVAL_NO_HARDWARE_TIMER

// Disable register variables
#define EVAL_NO_CHAR_REGISTER
#define EVAL_NO_DOUBLE_REGISTER

// Include functions implementation
#include "func.impl.h"

// Include system files
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>


// ******************************************************

// Empty implementation of redundant functions

void evalWriteChar(const char c)
{

}

char evalReadChar()
{
    return 0;
}

char evalCheckChar()
{
    return 0;
}



// ******************************************************

// Implement timer functions (at they are common in Linux)

void evalMicroSleep(unsigned int micro)
{
    int milisec = micro * 1000; // length of time to sleep, in miliseconds

    struct timespec req = {0};
    req.tv_sec = 0;
    req.tv_nsec = milisec * 1000000L;
    nanosleep(&req, (struct timespec *)NULL);
}

void evalSleep(unsigned int sec)
{
    sleep(sec);
    /*struct timespec tim;

    tim.tv_sec  = 0;
    tim.tv_nsec = sec * 1000000000;

    nanosleep(&tim , NULL);*/
}

double GetTickCount(void)
{
    struct timeval tv;

    gettimeofday(&tv, NULL);

    return tv.tv_sec + (tv.tv_usec / 1000000.0);
}

volatile double timeStart;
volatile double timeStop;

void evalTimerStart()
{
    timeStart = GetTickCount();
}

void evalTimerStop()
{
    timeStop = GetTickCount();
}

double evalTimerRead()
{
    return timeStop - timeStart;
}


// ******************************************************

// And the main file
#include "eval.h"












