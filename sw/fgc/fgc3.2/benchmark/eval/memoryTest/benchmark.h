#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Simple iteration benchmark.
 *
 ***************************************/

#include <eval/evalParams.h>

// Memory
#ifndef EVAL_IS_LINUX
    char*   const charPtr     = (char*)   EVALP_MEM_TEST_ADDRESS;
    double* const doublePtr   = (double*) EVALP_MEM_TEST_ADDRESS;

    char*   const charPtrCp   = (char*)   (EVALP_MEM_TEST_ADDRESS + EVALP_MEM_COUNT + 1);
    double* const doublePtrCp = (double*) (EVALP_MEM_TEST_ADDRESS + EVALP_MEM_COUNT + 1);
#else
    char*   charPtr;
    double* doublePtr;

    char*   charPtrCp;
    double* doublePtrCp;
#endif

#ifndef EVAL_NO_CHAR_REGISTER
    register char charTest   asm ("r12");
#else
    double charTest;
#endif

#ifndef EVAL_NO_DOUBLE_REGISTER
    register double doubleTest asm ("r12");
#else
    double doubleTest;
#endif

void memSkippingReadChar(uint16_t skip)
{
    evalPrint("   Read char,   skipping %3d  = ", skip);
    evalTimerStart();

    for (int s = 0; s < skip; ++s)
    {
        for (int i = s; i < EVALP_MEM_COUNT; i += skip)
        {
            charTest = charPtr[i];
        }
    }

    evalTimerStop();

    double time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("readChar", skip, time);
}



void memSkippingReadDouble(uint16_t skip)
{
    evalPrint("   Read double, skipping %3d  = ", skip);
    evalTimerStart();

    for (int s = 0; s < skip; ++s)
    {
        for (int i = s; i < EVALP_MEM_COUNT / sizeof(double); i += skip)
        {
            doubleTest = doublePtr[i];
        }
    }

    evalTimerStop();
    double time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("readDouble", skip, time);
}



void memSkippingWriteChar(uint16_t skip)
{
    evalPrint("   Write char,   skipping %3d = ", skip);
    evalTimerStart();

    for (int s = 0; s < skip; ++s)
    {
        for (int i = s; i < EVALP_MEM_COUNT; i += skip)
        {
            charPtr[i] = i;
        }
    }

    evalTimerStop();
    double time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("writeChar", skip, time);
}



void memSkippingWriteDouble(uint16_t skip)
{
    evalPrint("   Write double, skipping %3d = ", skip);
    evalTimerStart();

    for (int s = 0; s < skip; ++s)
    {
        for (int i = s; i < EVALP_MEM_COUNT / sizeof(double); i += skip)
        {
            doublePtr[i] = i;
        }
    }

    evalTimerStop();
    double time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("writeDouble", skip, time);
}



void memSkippingCopyChar(uint16_t skip)
{
    evalPrint("   Copy char,   skipping %3d  = ", skip);
    evalTimerStart();

    for (int s = 0; s < skip; ++s)
    {
        for (int i = s; i < EVALP_MEM_COUNT; i += skip)
        {
            charPtrCp[i] = charPtr[i];
        }
    }

    evalTimerStop();
    double time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("copyChar", skip, time);
}



void memSkippingCopyDouble(uint16_t skip)
{
    evalPrint("   Copy double, skipping %3d  = ", skip);
    evalTimerStart();

    for (int s = 0; s < skip; ++s)
    {
        for (int i = s; i < EVALP_MEM_COUNT / sizeof(double); i += skip)
        {
            doublePtrCp[i] = doublePtr[i];
        }
    }

    evalTimerStop();
    double time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("copyDouble", skip, time);
}



void evalBenchmarkMemoryTest()
{
#ifdef EVAL_IS_LINUX
    charPtr     = (char*)   calloc (EVALP_MEM_COUNT, sizeof(char));;
    doublePtr   = (double*) charPtr;

    charPtrCp   = (char*)   calloc (EVALP_MEM_COUNT, sizeof(char));;
    doublePtrCp = (double*) charPtrCp;
#endif


    double time;

    evalPrintln("   Number of memory bytes to test %d\r\n", EVALP_MEM_COUNT);

    // ******************************************************
    // Memory accessing tests

    evalPrint("   Read char                  = ");
    evalTimerStart();

    for (int i = 0; i < EVALP_MEM_COUNT; ++i)
    {
        charTest = charPtr[i];
    }

    evalTimerStop();
    time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("readChar", 0, time);

    // ***************

    evalPrint("   Read double                = ");
    evalTimerStart();

    for (int i = 0; i < EVALP_MEM_COUNT / sizeof(double); ++i)
    {
        doubleTest = doublePtr[i];
    }

    evalTimerStop();
    time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("readDouble", 0, time);

    // ***************

    memSkippingReadChar(3);
    memSkippingReadChar(8);
    memSkippingReadChar(32);
    memSkippingReadChar(33);
    memSkippingReadChar(512);

    // ***************

    memSkippingReadDouble(3);
    memSkippingReadDouble(8);
    memSkippingReadDouble(32);
    memSkippingReadDouble(33);
    memSkippingReadDouble(512);



    // ******************************************************
    // Memory writing tests

    evalPrintln("");

    evalPrint("   Write char                 = ");
    evalTimerStart();

    for (int i = 0; i < EVALP_MEM_COUNT; ++i)
    {
        charPtr[i] = i;
    }

    evalTimerStop();
    time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("writeChar", 0, time);

    // ***************

    evalPrint("   Write double               = ");
    evalTimerStart();

    for (int i = 0; i < EVALP_MEM_COUNT / sizeof(double); ++i)
    {
        doublePtr[i] = i;
    }

    evalTimerStop();
    time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("writeDouble", 0, time);

    // ***************

    memSkippingWriteChar(3);
    memSkippingWriteChar(8);
    memSkippingWriteChar(32);
    memSkippingWriteChar(33);
    memSkippingWriteChar(512);

    // ***************

    memSkippingWriteDouble(3);
    memSkippingWriteDouble(8);
    memSkippingWriteDouble(32);
    memSkippingWriteDouble(33);
    memSkippingWriteDouble(512);



    // ******************************************************
    // Memory copy tests

    evalPrintln("");

    evalPrint("   Copy char                  = ");
    evalTimerStart();

    for (int i = 0; i < EVALP_MEM_COUNT; ++i)
    {
        charPtrCp[i] = charPtr[i];
    }

    evalTimerStop();
    time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("copyChar", 0, time);

    // ***************

    evalPrint("   Copy double                = ");
    evalTimerStart();

    for (int i = 0; i < EVALP_MEM_COUNT / sizeof(double); ++i)
    {
        doublePtrCp[i] = doublePtr[i];
    }

    evalTimerStop();
    time = evalTimerRead();
    evalPrintDouble(time, 7);
    evalPrintln(" seconds");
    evalLogTime("copyDouble", 0, time);

    // ***************

    memSkippingCopyChar(3);
    memSkippingCopyChar(8);
    memSkippingCopyChar(32);
    memSkippingCopyChar(33);
    memSkippingCopyChar(512);

    // ***************

    memSkippingCopyDouble(3);
    memSkippingCopyDouble(8);
    memSkippingCopyDouble(32);
    memSkippingCopyDouble(33);
    memSkippingCopyDouble(512);
}

