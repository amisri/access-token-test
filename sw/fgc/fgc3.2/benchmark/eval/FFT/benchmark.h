#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  FFT benchmark.
 *
 ***************************************/

#include <eval/evalParams.h>
#include <math.h>
#include <complex.h>

#define PI 3.14159265359
typedef double complex cplx;


void fftAux(cplx buf[], cplx out[], int n, int step)
{
    if (step < n)
    {
        fftAux(out, buf, n, step * 2);
        fftAux(out + step, buf + step, n, step * 2);

        for (int i = 0; i < n; i += 2 * step)
        {
            cplx t = cexp(-I * PI * i / n) * out[i + step];
            buf[i / 2]     = out[i] + t;
            buf[(i + n)/2] = out[i] - t;
        }
    }
}

#ifndef EVAL_IS_LINUX
void fft(cplx buf[], int n)
#else
void fft(cplx buf[], cplx out[], int n)
#endif
{
#ifndef EVAL_IS_LINUX
    cplx out[n];
#endif

    for (int i = 0; i < n; i++)
    {
        out[i] = buf[i];
    }

    fftAux(buf, out, n, 1);
}


void evalBenchmarkFft()
{
    evalPrintln("   Size of the array %d", EVALP_FFT_SIZE);

    // Prepare the array
#ifndef EVAL_IS_LINUX
    cplx array[EVALP_FFT_SIZE];
#else
    cplx *array = (cplx*) calloc (EVALP_FFT_SIZE, sizeof(cplx));
    cplx *out = (cplx*) calloc (EVALP_FFT_SIZE, sizeof(cplx));
#endif

    for (int i = 0; i < EVALP_FFT_SIZE; i++)
    {
        array[i] = (i + 0.5) * (i % 2 == 0 ? 1 : -1);
    }
    // Calculate FFT
    evalTimerStart();
#ifndef EVAL_IS_LINUX
    fft(array, EVALP_FFT_SIZE);
#else
    fft(array, out, EVALP_FFT_SIZE);
#endif
    evalTimerStop();
    evalPrintTime("fft", evalTimerRead());
}

