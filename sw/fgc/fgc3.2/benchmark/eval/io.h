#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  High level I/O functions and implementation of the functions
 *  needed to use printf and scanf from <stdio.h>.
 *
 ***************************************/

#include <stdarg.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/unistd.h>
#include <eval/platformPort.h>

// Colors defines
#define CN      "\e[0m"
#define CWHITE  "\e[1;37m"
#define CBLUE   "\e[1;34m"
#define CGREEN  "\e[1;32m"
#define CCYAN   "\e[1;36m"
#define CRED    "\e[1;31m"
#define CPURPLE "\e[1;35m"
#define CYELLOW "\e[1;33m"
#define CGRAY   "\e[0;37m"


// ******************************************************
// Functions used to make use of printf()

void evalPrint(const char* format, ...);

void evalPrintln(const char* format, ...);

// ******************************************************
// Should be invoked during initialization

void evalPrintSetup();

// ******************************************************
// Functions used to print to a buffer with final results

void evalPrintTime(const char* logFormat, double time);

void evalLogTime(const char* logFormat, int param, double time);

void evalPrintLog();

void evalPrintDouble(double number, int digits);




