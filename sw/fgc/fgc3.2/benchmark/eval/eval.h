#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Main evaluation file to include in "main.c" of the test programs.
 *
 ***************************************/

// Include system headers

// Include internal modules
#include <eval/io.h>
#include <eval/timer.h>
#include <eval/platformPort.h>

// Include benchmarks
#include <eval/timerTest/benchmark.h>
#include <eval/iterate/benchmark.h>
#include <eval/piCalc/benchmark.h>
#include <eval/bubbleSort/benchmark.h>
#include <eval/quickSort/benchmark.h>
#include <eval/FFT/benchmark.h>
#include <eval/NBody/benchmark.h>
#include <eval/SHA/benchmark.h>
#include <eval/memoryTest/benchmark.h>
#include <eval/coreMark/benchmark.h>
#include <eval/linpack/benchmark.h>


// ******************************************************

// Benchmark struct
typedef struct
{
    const int    id;
    const char*  name;
    void         (*func)();
} evalBenchmark;


// Listing of the benchmarks
enum evalBenchmarkId
{
    EVAL_TIMERTEST  = (1 << 0),
    EVAL_ITERATE    = (1 << 1),
    EVAL_PICALC     = (1 << 2),
    EVAL_BUBBLE     = (1 << 3),
    EVAL_QUICK      = (1 << 4),
    EVAL_FFT        = (1 << 5),
    EVAL_NBODY      = (1 << 6),
    EVAL_SHA        = (1 << 7),
    EVAL_MEMTEST    = (1 << 8),
    EVAL_COREMARK   = (1 << 9),
    EVAL_LINPACK    = (1 << 10),

    EVAL_ALL        = 0xFFFF
};


evalBenchmark evalBenchmarks[] =
{
    { EVAL_TIMERTEST, "Timer test",             evalBenchmarkTimerTest},
    { EVAL_ITERATE,   "Simple iteration",       evalBenchmarkIterate},
    { EVAL_PICALC,    "PI calculation",         evalBenchmarkPiCalc},
    { EVAL_BUBBLE,    "Bubble sort",            evalBenchmarkBubble},
    { EVAL_QUICK,     "Quick sort",             evalBenchmarkQuick},
    { EVAL_FFT,       "Fast Fourier transform", evalBenchmarkFft},
    { EVAL_NBODY,     "N-body problem",         evalBenchmarkNBody},
    { EVAL_SHA,       "SHA256",                 evalBenchmarkSha},
    { EVAL_MEMTEST,   "Simple memory test",     evalBenchmarkMemoryTest},
    { EVAL_COREMARK,  "CoreMark",               evalBenchmarkCoreMark},
    { EVAL_LINPACK,   "Linpack",                evalBenchmarkLinpack},

    { 0, "", NULL}
};

// ******************************************************

extern char* logBuffer;
extern int   logOffset;

// Initialize evaluation code
void evalInit(const char* cpu, const char* flags)
{
    // Init printing (std buffers)
    evalPrintSetup();

#ifndef EVAL_NO_HARDWARE_TIMER
    // Keep the hardware timer in reset state
    evalIoTimReset(0);
#endif

    // Print program start
    evalPrintln(CYELLOW "\r\n\n-----------------------------------");
    evalPrintln("Program started on %s.", cpu);
    evalPrintln("Compiler: GCC %s %s." CN, __VERSION__, flags);
}

// ******************************************************

// Perform single benchmark
void evalPerform(const int benchmarks)
{
    int i = 0;
    char logBufferLocal[10000];

    // Initialize log buffer variables
    logBuffer = logBufferLocal;
    logOffset = 0;

    // Iterate over selected benchmarks
    while (evalBenchmarks[i].func != NULL)
    {
        if (evalBenchmarks[i].id & benchmarks)
        {
            evalPrintln(CGREEN "\r\nBenchmark started: " CCYAN "%s", evalBenchmarks[i].name);
            evalPrint(CWHITE);

            evalBenchmarks[i].func();

            evalPrintln(CGREEN "Benchmark finished." CN);
        }

        i++;
    }

    evalPrintln("");
    evalPrintln(CRED "Final result:" CWHITE);

    evalPrintLog();

    evalPrintln(CN);
}


// Function that waits for the user input and prints dot every second
void evalWaitForUser()
{
    int milisec = 0;

    for (;;)
    {
        if (evalCheckChar() != 0)
        {
            evalPrintln("\r\n.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-");
            break;
        }

        if (milisec % 3000 == 0)
        {
        }

        evalMicroSleep(1000);
        milisec++;
    }
}







