#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  N-body problem benchmark.
 *
 ***************************************/

#include <eval/evalParams.h>
#include <math.h>
#include <stdlib.h>

#define SOFTENING 1e-9f

typedef struct
{
    double x, y, z, vx, vy, vz;
} NBodyBody;


void NBodyRandomize(double *data, int n)
{
    for (int i = 0; i < n; i++)
    {
        data[i] = 2.0f * (rand() / (double) RAND_MAX) - 1.0;
    }
}

typedef float real;

real f(real x, real n)
{
    return x*x - n;
}

real customSqrt(real n)
{
    real err = 0.00000000001f;
    real h = 0.01f;

    real x = 1.0f;
    real xant = 0.0f;

    do
    {
        xant = x;
        real df = (f(x+h, n) - f(x, n))/h;
        x = x - f(x, n)/df;
    }
    while (abs(x - xant) > err);

    return x;
}



void NBodyForce(NBodyBody *p, double dt, int n)
{
    for (int i = 0; i < n; i++)
    {
        double Fx = 0.0;
        double Fy = 0.0;
        double Fz = 0.0;

        for (int j = 0; j < n; j++)
        {
            double dx       = p[j].x - p[i].x;
            double dy       = p[j].y - p[i].y;
            double dz       = p[j].z - p[i].z;
            double distSqr  = dx*dx + dy*dy + dz*dz + SOFTENING;
            double invDist  = 1.0 / customSqrt(distSqr);
            double invDist3 = invDist * invDist * invDist;

            Fx += dx * invDist3;
            Fy += dy * invDist3;
            Fz += dz * invDist3;
        }

        p[i].vx += dt * Fx;
        p[i].vy += dt * Fy;
        p[i].vz += dt * Fz;
    }
}


void evalBenchmarkNBody()
{
    evalPrintln("   Number of bodies %d",     EVALP_NBODY_NUM);
    evalPrintln("   Number of iterations %d", EVALP_NBODY_ITER);

    // Prepare bodies
    double buf[EVALP_NBODY_NUM * 6];
    NBodyRandomize(buf, 6 * EVALP_NBODY_NUM);

    NBodyBody *p   = (NBodyBody*) buf;
    const float dt = 0.01;

    // Simulate N-body
    evalTimerStart();

    for (int iter = 1; iter <= EVALP_NBODY_ITER; iter++)
    {
        NBodyForce(p, dt, EVALP_NBODY_NUM);

        for (int i = 0 ; i < EVALP_NBODY_NUM; i++)
        {
            p[i].x += p[i].vx*dt;
            p[i].y += p[i].vy*dt;
            p[i].z += p[i].vz*dt;
        }
    }

    evalTimerStop();

    evalPrintTime("nbody", evalTimerRead());
}

