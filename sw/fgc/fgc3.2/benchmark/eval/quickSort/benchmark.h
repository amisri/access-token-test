#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Quick sort benchmark.
 *
 ***************************************/

#include <eval/evalParams.h>


void quickSwap(double* a, double* b)
{
    double t = *a;
    *a = *b;
    *b = t;
}

int quickPartition (double arr[], int low, int high)
{
    double pivot = arr[high];
    int i = (low - 1);

    for (int j = low; j <= high- 1; j++)
    {
        if (arr[j] <= pivot)
        {
            i++;
            quickSwap(&arr[i], &arr[j]);
        }
    }

    quickSwap(&arr[i + 1], &arr[high]);

    return (i + 1);
}

void quickSort(double arr[], int low, int high)
{
    if (low < high)
    {
        int pi = quickPartition(arr, low, high);

        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}


void evalBenchmarkQuick()
{
    evalPrintln("   Size of the array %d", EVALP_QUICK_SIZE);

    // Prepare the reversed array for sorting
    double array[EVALP_QUICK_SIZE];

    for (int i = 0; i < EVALP_QUICK_SIZE; i++)
    {
        array[i] = -i + 0.5;
    }

    // Sort the array
    evalTimerStart();

    quickSort(array, 0, EVALP_QUICK_SIZE-1);

    evalTimerStop();

    evalPrintTime("quickSort", evalTimerRead());
}

