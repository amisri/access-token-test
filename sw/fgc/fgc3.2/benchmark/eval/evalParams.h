#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Benchmarks parameters.
 *
 ***************************************/


// *******************************************************************
// ****** Simple iteration *******************************************

// Number of iterations
#define EVALP_ITER_ITER 256*256*256*95


// *******************************************************************
// ****** PI calculation *********************************************

// Number of iterations
#define EVALP_PI_ITER 99990000


// *******************************************************************
// ****** Bubble sort ************************************************

// Size of the array to sort
#define EVALP_BUBBLE_SIZE 35000


// *******************************************************************
// ****** Quick sort *************************************************

// Size of the array to sort
#define EVALP_QUICK_SIZE 35000


// *******************************************************************
// ****** FFT ********************************************************

// Size of the array (power of 2)
#define EVALP_FFT_SIZE 1048576


// *******************************************************************
// ****** N-Body problem *********************************************

// Number of bodies
#define EVALP_NBODY_NUM 3000

// Number of iterations
#define EVALP_NBODY_ITER 20


// *******************************************************************
// ****** SHA256 *****************************************************

// Number of SHA iterations
#define EVALP_SHA_ITER 15000


// *******************************************************************
// ****** CoreMark ***************************************************

// Number of iterations used in CoreMark benchmark
#define EVALP_COREMARK_ITER 30000


// *******************************************************************
// ****** Linpack ****************************************************

// Size of the array
#define EVALP_LINPACK_SIZE 250

// Number of iterations of Linpack benchmark
#define EVALP_LINPACK_ITER 10


// *******************************************************************
// ****** Simple memory tests ****************************************

// Number of memory bytes to test (access, write, copy)
#define EVALP_MEM_COUNT (500 * 1024 * 1024)










