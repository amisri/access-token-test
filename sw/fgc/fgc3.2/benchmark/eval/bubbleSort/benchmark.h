#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Bubble sort benchmark.
 *
 ***************************************/

#include <eval/evalParams.h>


void evalBenchmarkBubble()
{
    evalPrintln("   Size of the array %d", EVALP_BUBBLE_SIZE);

    // Prepare the reversed array for sorting
    double array[EVALP_BUBBLE_SIZE];

    for (int i = 0; i < EVALP_BUBBLE_SIZE; i++)
    {
        array[i] = -i + 0.5;
    }

    // Sort the array
    evalTimerStart();

    double swap;

    for (int i = 0; i < (EVALP_BUBBLE_SIZE - 1); i++)
    {
        for (int j = 0; j < EVALP_BUBBLE_SIZE - i - 1; j++)
        {
            if (array[j] > array[j+1])
            {
                swap       = array[j];
                array[j]   = array[j+1];
                array[j+1] = swap;
            }
        }
    }

    evalTimerStop();

    // Print 10 elements of array so the optimizer does't remove it
    evalPrint("   Array 10: ");

    for (int i = 0; i < 10; ++i)
    {
        evalPrint("%d ", (int)array[i] );
    }
    evalPrintln("");

    evalPrintTime("bubble", evalTimerRead());
}

