#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Hardware timer and sleeping functions test.
 *
 ***************************************/

void evalBenchmarkTimerTest()
{
    uint32_t seconds[] = { 1, 2, 3, 5, 10, 20, 60, 0 };
    uint32_t micros[]  = { 5000, 100000, 500000, 1000000, 2000000, 10000000, 60000000, 0 };
    int i = 0;

    // Test the sleep function
    while (seconds[i] != 0)
    {
        evalPrint("   evalSleep: %d seconds... ", seconds[i]);
        evalTimerStart();
        evalSleep(seconds[i]);
        evalTimerStop();

        evalPrint("timer  = ");
        evalPrintDouble(evalTimerRead(), 7);
        evalPrintln("");
        i++;
    }

    i = 0;
    evalPrintln("");

    // Test the micro sleep function
    while (micros[i] != 0)
    {
        evalPrint("   evalMicroSleep: %d microseconds... ", micros[i]);
        evalTimerStart();
        evalMicroSleep(micros[i]);
        evalTimerStop();
        evalPrint("timer  = ");
        evalPrintDouble(evalTimerRead(), 7);
        evalPrintln("");
        i++;
    }
}

