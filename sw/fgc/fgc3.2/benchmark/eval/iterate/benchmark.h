#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  Simple iteration benchmark.
 *
 ***************************************/

#include <eval/evalParams.h>

void evalBenchmarkIterate()
{
    unsigned long long i = 0;

    evalPrintln("   Number of iterations %d", EVALP_ITER_ITER);

    evalTimerStart();

    while (i < EVALP_ITER_ITER)
    {
        i++;
    }

    evalTimerStop();

    evalPrintTime("Iterate", evalTimerRead());
}

