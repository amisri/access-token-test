#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  PI calculation benchmark.
 *
 ***************************************/

#include <eval/evalParams.h>
#include <math.h>

// Iterative solution to calculate pow(x, n) using binary operators
int power(int x, unsigned n)
{
    int pow = 1;

    while (n)
    {
        if (n & 1)
            pow *= x;

        n = n >> 1;

        x = x * x;
    }

    return pow;
}

double piCalc1(int num)
{
    return (16/(num * power(5, num )));
}

double piCalc2(int num)
{
    return (4/(num * power(239, num )));
}

void evalBenchmarkPiCalc()
{
    evalPrintln("   Number of iterations %d", EVALP_PI_ITER);

    double temp1  = 0.0;
    double temp2  = 0.0;
    double answer = 0.0;
    char   flag   = 1;

    evalTimerStart();

    for(int i = 1; i <= EVALP_PI_ITER; i+= 2)
    {
       if(flag == 1)
       {
           temp1 += piCalc1(i);
           temp2 += piCalc2(i);
           flag = 0;
       }
       else
       {
           temp1 -= piCalc1(i);
           temp2 -= piCalc2(i);
           flag = 1;
       }
    }

    answer = temp1 - temp2;

    evalTimerStop();

    evalPrint("   PI value   = ");
    evalPrintDouble(answer, 5);
    evalPrintln("");

    evalPrintTime("picalc", evalTimerRead());
}

