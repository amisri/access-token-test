#include <sleep.h>
#include <stdarg.h>
#include <xuartps_hw.h>

#include "sleep.h"
#include "xparameters.h"
#include "xgpio.h"
#include "xil_cache.h"

// GPIO handler
XGpio pAXI0;

// Timer pins defines
#define PIN_VCC 0
#define PIN_RST 1
#define PIN_OUT 2
#define PIN_SCK 3

// ******************************************************
// An address of a memory region used for performing memory tests

#define EVALP_MEM_TEST_ADDRESS 0x800000000


// ******************************************************
// Include the evaluation code
#include <eval/eval.h>
#include <eval/func.impl.h>


// ***********************************************
// UART functions
void evalWriteChar(const char c)
{
    outbyte(c);
}


char evalReadChar()
{
    return inbyte();
}


char evalCheckChar()
{
	if (XUartPs_IsReceiveData(STDIN_BASEADDRESS))
	{
		return (u8)XUartPs_ReadReg(STDIN_BASEADDRESS, XUARTPS_FIFO_OFFSET);
	}
	else
	{
		return 0;
	}
}


// ***********************************************
// Sleeping functions
void evalMicroSleep(unsigned int micro)
{
    usleep(micro);
}


void evalSleep(unsigned int seconds)
{
    sleep(seconds);
}


// ***********************************************
// Timer functions
void evalIoTimReset(bool state)
{
    // Get current value
    int tmp = XGpio_DiscreteRead(&pAXI0, 2);

    // Calculate new value for the selected LED
    if (state)
    {
        tmp |= (1 << PIN_RST);
    }
    else
    {
        tmp &= ~(1 << PIN_RST);
    }

    // Set this value
    XGpio_DiscreteWrite(&pAXI0, 2, tmp);
}


void evalIoTimSck(bool state)
{
    // Get current value
    int tmp = XGpio_DiscreteRead(&pAXI0, 2);

    // Calculate new value for the selected LED
    if (state)
    {
        tmp |= (1 << PIN_SCK);
    }
    else
    {
        tmp &= ~(1 << PIN_SCK);
    }

    // Set this value
    XGpio_DiscreteWrite(&pAXI0, 2, tmp);
}


bool evalIoTimOut()
{
    return ((XGpio_DiscreteRead(&pAXI0, 2) & (1 << PIN_OUT)) == (1 << PIN_OUT));
}


// ***********************************************
// ***********************************************
// ***********************************************


int main()
{
    // Init GPIO (for communication with the timer)
    XGpio_Initialize(&pAXI0, XPAR_AXI_GPIO_0_DEVICE_ID);

    // Set data direction of leds pins
    XGpio_SetDataDirection(&pAXI0, 1, 0);
    XGpio_DiscreteWrite(&pAXI0, 1, 1);

    // Prepare dir variable with VCC, RST and SCK set as outputs
    u32 dir = 0xFFFFFFFF;
    dir &= ~(1 << PIN_VCC);
    dir &= ~(1 << PIN_RST);
    dir &= ~(1 << PIN_SCK);

    // Set data direction of timer pins
    XGpio_SetDataDirection(&pAXI0, 2, dir);

    // Set PIN_VCC as 1 to power the timer
    XGpio_DiscreteWrite(&pAXI0, 2, (1 << PIN_VCC));

    // Init evaluation
    evalInit("Zynq", "-O0");

    for (;;)
    {
    	// Perform benchmarks
    	evalPerform(EVAL_ALL & (~EVAL_TIMERTEST));

    	// Wait for user input and start again
    	evalWaitForUser();
    }

    return 0;
}
