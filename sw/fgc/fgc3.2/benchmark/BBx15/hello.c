#include <stdio.h>
#include <stdint.h>


// "rdimon.specs"

typedef unsigned char byte;


// ************************************************************
// Helper functions
// ************************************************************

// Bit field
#define bf(x)                  ( 1 << (x) )

// Bit operations on 8 bit registers
#define bSet(reg, mask)        ( (*(volatile byte *)(reg)) |=  (mask) )
#define bClear(reg, mask)      ( (*(volatile byte *)(reg)) &= ~(mask) )

#define bIsOne(reg, mask)      ( ((*(volatile byte *)(reg)) & (mask)) == (mask) )

#define bIn(reg)               ( (*(volatile byte *)(reg)) )
#define bOut(reg, val)         ( (*(volatile byte *)(reg)) = (byte)(val) )

// Bit operations on 16 bit registers
#define b16Set(reg, mask)      ( (*(volatile uint16_t *)(reg)) |=  (mask) )
#define bvClear(reg, mask)     ( (*(volatile uint16_t *)(reg)) &= ~(mask) )

#define b16IsOne(reg, mask)    ( ((*(volatile uint16_t *)(reg)) & (mask)) == (mask) )

#define b16In(reg)             ( (*(volatile uint16_t *)(reg)) )
#define b16ut(reg, val)        ( (*(volatile uint16_t *)(reg)) = (uint16_t)(val) )


// Bit operations on 32 bit registers
#define b32Set(reg, mask)      ( (*(volatile uint32_t *)(reg)) |=  (mask) )
#define b32Clear(reg, mask)    ( (*(volatile uint32_t *)(reg)) &= ~(mask) )

#define b32IsOne(reg, mask)    ( ((*(volatile uint32_t *)(reg)) & (mask)) == (mask) )

#define b32In(reg)             ( (*(volatile uint32_t *)(reg)) )
#define b32Out(reg, val)       ( (*(volatile uint32_t *)(reg)) = (uint32_t)(val) )


// Bit operations on variables
#define bitSet(var, mask)      ( var |=  (mask) )
#define bitClear(var, mask)    ( var &= ~(mask) )



// ************************************************************
// UART
// ************************************************************

#define UART_BASE           0x48020000

// Registers
#define U_THR               ( UART_BASE + 0x0 )
#define U_LSR               0x48020014

// Bits
#define U_TX_SR_E           bf(6)

// Functions
void uartSendChar(const char c)
{
    bOut(U_THR, c);
}

void uartWaitForSend()
{
    while (!bIsOne(U_LSR, U_TX_SR_E));
}



// ************************************************************
// Timer
// ************************************************************

// Timer register
#define T_CR    0x4AE04030

volatile uint32_t timerStartValue;
volatile uint32_t timerStopValue;

void timerStart()
{
    timerStartValue = b32In(T_CR);
}

void timerStop()
{
    timerStopValue = b32In(T_CR);
}

double timerRead()
{
    uint32_t diff = diff = timerStopValue - timerStartValue;

    // Convert to seconds (1/32_768 Hz) * difference
    return (0.000030518 * diff);
}



// ************************************************************
// Evaluation function implementation
// ************************************************************

// ******************************************************
/* An address of a memory region used for performing memory tests

   For MEMTEST change liner script to put everything to OCMC_RAM1 and load program
   from SD card into memory at 0x40300000. Also +1 was removed from MEMTEST benchmark. */
#define EVALP_MEM_TEST_ADDRESS 0x80000000




// ******************************************************
// Include the evaluation code
#define EVAL_NO_PRINTF_SYSCALLS
#define EVAL_NO_HARDWARE_TIMER
#define EVAL_NO_DOUBLE_REGISTER
#define EVAL_NO_CHAR_REGISTER
#define EVAL_NO_DOUBLE_REGISTER

#include <eval/eval.h>
#include <eval/func.impl.h>


// ***********************************************
// UART functions
void evalWriteChar(const char c)
{
    uartSendChar(c);
    uartWaitForSend();
}


char evalReadChar()
{
    return 0;
}


char evalCheckChar()
{
    return 0;
}

// ***********************************************
// Sleeping functions

void evalMicroSleep(unsigned int micro)
{

}

void evalSleep(unsigned int seconds)
{

}


// ******************************************************
// Timer functions

void evalTimerStart()
{
    timerStart();
}

void evalTimerStop()
{
    timerStop();
}

double evalTimerRead()
{
    return timerRead();
}



// ************************************************************
// Main
// ************************************************************

int main(void)
{
    // Enable FPU / NEON
    __asm__ (
            "MRC   p15, #0, r3, c1, c0, #2;"
            "ORR   r3, r3, #0x00F00000;"
            "MCR   p15, #0, r3, c1, c0, #2;"
            "MOV   r3, #0;"
            "MOV   r0,#0x40000000;"
            "FMXR  FPEXC,r0;"
            "MCR   p15, #0, r3, c7, c5, #4;"
    );

    // Enable cache
    __asm__ (
            "MRC   p15, #1, r3, c9, c0, #2;"
            "ORR   r3, r3, #0x200000;"
            "MCR   p15, #1, r3, c9, c0, #2;"
            "MOV   r3, #0;"

            "MRC   p15, #0, r3, c1, c0, #0;"
            "ORR   r3, r3, #0x1800;"
            "ORR   r3, r3, #0x0005;"
            "MCR   p15, #0, r3, c1, c0, #0;"
            "MOV   r3, #0;"
    );

    // ECC  200000
    //      001000000000000000000000

    // 1100000000100 - 0x1804 - I, C, branch
    // 1000000000100 - 0x1004 - I, C
    // 1000000000000 - 0x1000 - I
    // 1100000000000 - 0x1800 - I, branch

    // 1100000000001 - 0x1801 - I, branch, MMU
    // 1100000000101 - 0x1805 - I, C, branch, MMU

    evalWriteChar('A');
    evalWriteChar('0');

    // Init evaluation:
    // HARD FPU: m-floatabi: hard hard, -mfpu: neon-vfpv4
    //
    // evalInit("BeagleBoard x15", "-O0 no -ftree-vectorize. soft float");
    evalInit("BeagleBoard x15", "-O0 no -ftree-vectorize. hard float");
    // evalInit("BeagleBoard x15", "-O2 -ftree-vectorize. hard float");

    for (;;)
    {
        // Perform benchmarks

        // For O0
        evalPerform(EVAL_ALL & (~EVAL_TIMERTEST) & (~EVAL_MEMTEST));

        // For O2
        //evalPerform(EVAL_ALL & (~EVAL_TIMERTEST) & (~EVAL_MEMTEST));

        // For MEMTEST, see note above
        //evalPerform(EVAL_MEMTEST);

        // Wait for user input and start again
        evalWaitForUser();
    }

	return 0;
}









