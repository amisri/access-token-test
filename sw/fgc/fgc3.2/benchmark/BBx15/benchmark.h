#pragma once

/***************************************
 *
 *  FGC 3.2 CPU evaluation
 *
 *  CoreMark benchmark.
 *
 ***************************************/

#include <eval/evalParams.h>

void coreMarkPerform(void);

void evalBenchmarkCoreMark()
{
    evalPrintln("   Number of iterations %d", EVALP_COREMARK_ITER);

    coreMarkPerform();
}
