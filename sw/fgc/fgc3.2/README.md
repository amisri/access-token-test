# FGC3.2 platform  


#### Directories:

- **benchmark** - contains codes and results of benchmarks that were used to select the SoC and test it.
- **fgc32OS** - FGC3.2 Operating System. Contains building scripts and custom files to build, customize and deploy low-level firmware, Linux kernel and root file system.
- **software** - software specific for FGC3.2, like Hardware Abstraction Layer, kernel modules, launcher, etc.


See *README.md* files in each directory for more details.
