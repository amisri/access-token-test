#!/usr/bin/perl -w
#
# Name:     check_crc.pl
# Purpose:  Check the CRCs of the ID database
# Authors:  Marc Magrans de Abril, Krystian Wojtas

use Digest::CRC;
use File::Basename;
use File::Slurp;
use English qw( -no_match_vars );
use Carp;
use strict;
use warnings;

# Create reusable CRC-8-Dallas/Maxim

my $crc = Digest::CRC->new
(
    init   => 0x0000,
    poly   => 0x31,
    refin  => 1,
    refout => 1,
    xorout => 0x0000,
    width  => 8,
);

# Check one Dallas ID

sub check_crc
{
    my ($id_crc, $data) = @_;

    # Pack the data

    my $data_packed = reverse pack 'H*', $data;

    # Reset the CRC. Notice that this uses a private member of the class, so a new versio of Digest::CRC could break the code

    $crc->{_data} = undef;

    # Create the CRC-8-Dallas/Maxim. Notice that is different than the CRC-8-CCITT

    my $data_crc = $crc->add( $data_packed )->digest;

    # Die if crc sum is not correct

    if ( $id_crc != $data_crc )
    {
        die
            sprintf
                '%02X'.$data." has a wrong CRC-8-Dallas/Maxim (Expected 0x%02X but calculate 0x%02X)\n",
                $id_crc,
                $id_crc,
                $data_crc;
    }
}

# Validate input

sub validate
{
    my ( $barcode, $id ) = @_;

    if( not $barcode or not $id )
    {
        die "Error: empty barcode '".($barcode || '')."' or id '".($id || '')."' in input barcode_id.csv file\n";
    }

    if( $barcode !~ /^HC[A-Z_]{5}[\d_]{3}-[A-Z\d]{2}\d{6}$/)
    {
        die "Error: invalid barcode '$barcode'\nIt should be like      'HCRAALD001-CR000002'\n";
    }

    if( $id !~ /^[A-Z\d]{16}$/)
    {
        die "Error: invalid id      '$id'\nIt should be like      '3C00000B9AEF5801'\n";
    }
}

# Parse input file and validate crc

sub parse
{
    my ( $barcodes_ids ) = @_;

    for (@{$barcodes_ids})
    {
        # Ommit new line char on the end

        chomp;

        # Split line by comma char

        my ( $barcode, $id ) = split /,/sxm;

        # Validate input

        validate($barcode, $id);

        # CRC is the first byte of the string

        my $id_crc = hex( substr $id, 0, 2 );

        # The rest is a data

        my $data = substr $id, 2;

        # Validate provided crc

        check_crc($id_crc, $data);
    }

    return
}

# End of functions

# Run script

die "\nUsage: ", basename($PROGRAM_NAME), " <barcode_id file>\n\n" if ( @ARGV != 1 );

my ( $barcodes_ids_filename ) = @ARGV;

parse
    read_file
    (
        $barcodes_ids_filename,
        array_ref => 1
    );

# EOF
