#!/usr/bin/perl -w
#
# Name:     mkiddb.pl
# Purpose:  Construct FGC resident ID database
# Author:   Stephen Page

use FGC::Components;
use FGC::DB;
use FGC::AlimDB;
use File::Basename;
use File::Slurp;
use Array::Utils qw(unique array_diff);
use strict;

use Data::Dumper;

my @id_db_records;

# Check that this is a little-endian system

die "This program must be run on a little-endian system\n"
    if(unpack("h*", pack("s", 1)) =~ /01/);

# Takes some substring from passed barcode

# and returns it's definition from generated perl structure based on xml database

sub get_component_by_barcode($)
{
    my ( $barcode ) = @_;

    $fgc_components{substr $barcode, 0, 10}
}

# Return duplications in passed array reference

sub array_duplicates
{
    my ( $array ) = @_;

    my @unique = unique @$array;
    my @duplicates = array_diff( @$array, @unique );

    @duplicates;
}

# returns

sub filter_by_platform
{
    my ( $barcodes_ids, $platform ) = @_;

    my $barcodes_to_be_filtered = get_spare_barcodes_by_platform($platform);

    my $total = 0;
    my $interesting = 0;
    my $filtered = 0;

    my @wanted_barcodes_ids = grep
    {
        my($barcode, $id) = @$_;

        my $fgc_component = get_component_by_barcode($barcode);

        my $is_interesting = !$platform                                      # add if no platform has been specified by the user (script argument)
                             || !$fgc_component->{platform}                  # add if no platform is associated to this component (in components.xml)
                             || $fgc_component->{platform}=~ "$platform";    # add if the component's platform matches the user specified platform

        my $is_filtered = defined($barcodes_to_be_filtered->{$barcode});     # do not add if barcode is in list of barcodes to be filtered

        $total++;
        $interesting++ if($is_interesting);
        $filtered++ if($is_filtered);

        $_ if($is_interesting && !$is_filtered);
    }
    map
    {
        [$_->{CMA_CMP_ID},$_->{CMA_CHIP_ID}]
    }
    @$barcodes_ids;

    print("Total number of associations in DB: $total\n");
    print("Number of associatios for platform: $interesting\n");
    print("Filtered associations for platform: $filtered\n");
    print("Resulting associations for platform: ". (scalar @wanted_barcodes_ids) ."\n");

    return \@wanted_barcodes_ids;
}

# Validates if all provided data are ok

sub validate_input
{
    my ( $barcodes_ids, $platform ) = @_;

    # Collect all barcodes and ids emited by iterator

    my %barcode__ids;
    my @ids;

    for my $pair (@$barcodes_ids)
    {
        my ($barcode, $id) = @$pair;

        $barcode__ids{$barcode} ||= [];
        push @{ $barcode__ids{$barcode} }, $id;

        push @ids, $id;
    }

    # Check if all ids are unique

    if
    (
        my @duplicated_ids =
            unique
                array_duplicates
                    \@ids
    )
    {
        die
            "ERROR: IDs should always be unique, but there are provided duplications\n".
            join("\n", @duplicated_ids).
            "\n";
    }

    # Check whether the components are known

    if
    (
        my @fgc_components_not_defined =
            grep
                { not defined get_component_by_barcode($_) }
                keys
                    %barcode__ids
    )
    {
        die
            "ERROR: There are provided components which are not defined in components.xml file:\n".
            join("\n", @fgc_components_not_defined).
            "\n";
    }

    # Die if there are any barcodes with multiple ids when not allowed and print their code types

    if
    (
        my @code_types_of_barcodes_with_many_ids_when_not_allowed =

            # Format problematic barcodes

            map
                {
                    my $barcode = $_;
                    my $fgc_component = get_component_by_barcode($barcode);
                    my $label = $fgc_component->{label};

                    # Group together ids from the same barcode and add label

                    "$barcode '$label': @{$barcode__ids{$barcode}}";
                 }

                # Filter barcodes which has assigned many ids but it is not declared in components.xml

                # or number of them is greater than what is declared

                grep
                    {
                        my $barcode = $_;
                        my $fgc_component = get_component_by_barcode($barcode);
                        my $ids_number = @{ $barcode__ids{$barcode} };

                        $ids_number > 1
                        and
                        (
                            not defined $fgc_component->{multiple_ids}
                            or $ids_number > $fgc_component->{multiple_ids}
                        )
                    }

                    sort

                        # Iterate over barcodes

                        keys
                            %barcode__ids
    )
    {
        die
            "ERROR: There are some barcodes with assigned multiple ids, but 'multiple_ids' attribute in components.xml file is not set for them or it is set to less than number of discovered duplications.\n".
            join("\n", @code_types_of_barcodes_with_many_ids_when_not_allowed)."\n"
    }

    # Validate if provided barcodes exists in external database

    validate_barcodes_exist_in_db( [ keys %barcode__ids ] );
}

# Validate if provided barcodes exists in external database

sub validate_barcodes_exist_in_db
{
    my ($barcodes) = @_;

    # TODO really notify even if all these messages are going to be redirected in bash to a file ?
    # print "Checking consistency of barcodes with respect to data at pocontrols\@dbabco...\n";

    my $dbh = FGC::DB::connect(FGC_DB_USERNAME,FGC_DB_PASSWORD) or die "Unable to connect to pocontrols";

    my $query;
    if(!($query = $dbh->prepare("SELECT * FROM POCONTROLS.FGC_COMPONENTS WHERE CMP_MTF_ID=?")))
    {
        FGC::DB::disconnect($dbh);
        die "Failed to prepare database query\n";
    }

    my @wrong = ();
    foreach my $barcode (@$barcodes)
    {

        $query->execute($barcode);
        unless ($query->fetch())
        {
            push(@wrong,$barcode);
        }

    }
    $query->finish();

    FGC::DB::disconnect($dbh);

    if (@wrong)
    {
        FGC::DB::disconnect($dbh);
        die("The following barcodes are not in the pocontrols DB: @wrong");
    }
}

sub get_hcr_id_pairs()
{
    my $dbh = FGC::DB::connect(FGC_DB_USERNAME,FGC_DB_PASSWORD) or die "Unable to connect to pocontrols";

    my $query;
    if(!($query = $dbh->prepare("select CMA_CMP_ID, CMA_CHIP_ID from pocontrols.fgc_component_mapping")))
    {
        FGC::DB::disconnect($dbh);
        die "Failed to prepare database query\n";
    }
    $query->execute() or die "could not execute SQL query";

    my $results = $query->fetchall_arrayref({});

    FGC::DB::disconnect($dbh);

    return $results;
}

sub construct_part_type__ids
{
    my ( $barcodes_ids ) = @_;

    my %part_type__ids;

    for my $pair (@$barcodes_ids)
    {
        my ($barcode, $id) = @$pair;
        my $part_type = substr($barcode,  0, 13);

        # Create new array for mapping if not yet exist

        $part_type__ids{$part_type} ||= [];

        # Fill mapping of part type to all of it's ids

        push @{ $part_type__ids{$part_type} }, $id;
    }

    \%part_type__ids;
}



sub construct_id__serial
{
    my ( $barcodes_ids ) = @_;

    my %id__serial;

    for my $pair (@$barcodes_ids)
    {
        my ($barcode, $id) = @$pair;
        my $serial = substr($barcode, -6);

        # Fill mapping of id to serial

        $id__serial{$id} = $serial;
    }

    \%id__serial;
}

# Construct ranges

sub construct_ranges($$)
{
    my ($part_type__ids, $id__serial) = @_;

    my @ranges;
    my %id_range;

    # Add parts to ranges

    for my $part_type (sort keys %$part_type__ids)
    {
        # Extract barcodes and serial numbers

        my $range;
        my $previous_serial;
        for my $id (sort { $id__serial->{$a} <=> $id__serial->{$b} } @{ $part_type__ids->{$part_type} })
        {
            my $serial = $id__serial->{$id};

            # Check whether to create new type range

            if(!defined($previous_serial) || $serial > ($previous_serial + 500))
            {
                $range = {};

                push(@ranges, $range);

                $range->{min_serial} = $serial;
                $range->{parts}      = [];
                $range->{part_type}  = $part_type;
            }
            $previous_serial = $serial;

            $id_range{$id} = $range;

            $range->{max_serial} = $serial;
            push(@{$range->{parts}}, $serial);
        }
    }

    # Calculate pt_base and and pt_size for each range

    $ranges[0]->{pt_base} = 0;
    for(my $i = 0 ; $i < (@ranges - 1) ; $i++)
    {
        $ranges[$i]->{pt_size}        = ($ranges[$i]->{max_serial} - $ranges[$i]->{min_serial}) + 1;
        $ranges[$i + 1]->{pt_base}    = $ranges[$i]->{pt_base} + $ranges[$i]->{pt_size} + 1;
    }
    $ranges[-1]->{pt_size} = ($ranges[-1]->{max_serial} - $ranges[-1]->{min_serial}) + 1;

    ( \@ranges, \%id_range );
}

# Construct ID to part index

sub construct_id_to_part_idx($$)
{
    my ($id_range, $id__serial) = @_;

    my %id_types;

    # Process each part

    my %records;
    for my $id (keys(%$id__serial))
    {
        my $range       = $id_range->{$id} or print "UNDEF $id\n";
        my $serial      = $id__serial->{$id};
        my $part_idx    = $range->{pt_base} + ($serial - $range->{min_serial});

        $records{$id}->{id}         = $id;
        $records{$id}->{part_idx}   = $part_idx;
        $records{$id}->{id321}      = hex(substr($id,  8, 6));
        $records{$id}->{id1}        = hex(substr($id, 12, 2));
        $records{$id}->{id2}        = hex(substr($id, 10, 2));
        $records{$id}->{id3}        = hex(substr($id,  8, 2));
        $records{$id}->{id4}        = hex(substr($id,  6, 2));
        $records{$id}->{id_type}    = hex(substr($id, 14, 2));
    }

    # Output sorted records

    my $record_idx = 1;
    for my $record (sort { $a->{id_type} <=> $b->{id_type}  ||
                           $a->{id4} <=> $b->{id4}          ||
                           $a->{id3} <=> $b->{id3}          ||
                           $a->{id2} <=> $b->{id2}          ||
                           $a->{id1} <=> $b->{id1}
                         } (values(%records)))
    {
        $id_types{$record->{id_type}}->{value} = $record->{id_type};

        # First index save once in the first iteration, but last index increment with loop

        $id_types{$record->{id_type}}->{id4s}->{$record->{id4}}->{first_idx} ||= $record_idx;
        $id_types{$record->{id_type}}->{id4s}->{$record->{id4}}->{last_idx}    = $record_idx;

        $id_db_records[$record_idx] = pack("vCCC",
                                           $record->{part_idx},
                                           $record->{id1},
                                           $record->{id2},
                                           $record->{id3});
        # printf("[%05d] ID2PT PART_IDX=%05d ID1=%02X ID2=%02X ID3=%02X\n",
        #        $record_idx, $record->{part_idx}, $record->{id1}, $record->{id2}, $record->{id3});

        $record_idx++;
    }

    \%id_types;
}

# Construct directory 1

sub construct_dir_1($$)
{
    my ($ids, $id_types) = @_;

    my $first_idx   = 1 + $ids;
    my $last_idx    = $first_idx + scalar(keys(%$id_types)) - 1;

    $id_db_records[0] = pack("vvC", $first_idx, $last_idx, 0);
    print "[0] DIR 1 FIRST_IDX=$first_idx LAST_IDX=$last_idx TBL=0\n";
}

# Construct directory 2

sub construct_dir_2($$)
{
    my ($ids, $id_types) = @_;

    my $record_idx = 1 + $ids;
    for my $id_type (sort { $a->{value} <=> $b->{value} } (values(%$id_types)))
    {
        $id_db_records[$record_idx] = pack("vvC",
                                           $id_type->{first_idx},
                                           $id_type->{last_idx},
                                           $id_type->{value});
        printf("[%05d] DIR 2 FIRST_IDX=%05d LAST_IDX=%05d TYPE=%02X\n",
               $record_idx, $id_type->{first_idx}, $id_type->{last_idx}, $id_type->{value});
        $record_idx++;
    }
}

# Construct directory 3

sub construct_dir_3($$)
{
    my ($ids, $id_types) = @_;

    # Process each ID type

    my $record_idx = 1 + $ids + scalar(keys(%$id_types));
    for my $id_type (sort { $a->{value} <=> $b->{value} } (values(%$id_types)))
    {
        $id_type->{first_idx} = $record_idx;

        # Output record for each ID4 value

        for my $id4 (sort { $a <=> $b } (keys(%{$id_type->{id4s}})))
        {
            $id_db_records[$record_idx] = pack("vvC",
                                               $id_type->{id4s}->{$id4}->{first_idx},
                                               $id_type->{id4s}->{$id4}->{last_idx},
                                               $id4);
            printf("[%05d] DIR 3 FIRST_IDX=%05d LAST_IDX=%05d ID4=%02X\n",
                   $record_idx, $id_type->{id4s}->{$id4}->{first_idx}, $id_type->{id4s}->{$id4}->{last_idx}, $id4);

            $id_type->{last_idx} = $record_idx;
            $record_idx++;
        }
    }
}

# Construct part type database

sub construct_part_type_db($)
{
    my ($ranges) = @_;

    my @part_type_db_records = ();

    # Output ranges

    my $record_idx = 0;
    for my $range (@$ranges)
    {
        $part_type_db_records[$record_idx] = pack("vVa14",
                                                  $range->{pt_base},
                                                  $range->{min_serial},
                                                  $range->{part_type});
        printf("[%05d](pt) PTDB PT_BASE=%05d SERIAL_BASE=%06d BARCODE=%-13s\n",
               $record_idx, $range->{pt_base}, $range->{min_serial}, $range->{part_type});

        $record_idx++;
    }

    # Append "Unknown part" record

    $part_type_db_records[$record_idx] = pack("vVa14",
                                              $ranges->[-1]->{pt_base} + $ranges->[-1]->{pt_size} + 1,
                                              0,
                                              "Unknown_part");

    \@part_type_db_records;
}


sub get_hcr_id_pairs_patch()
{
    my $dbh = FGC::DB::connect(FGC_DB_USERNAME,FGC_DB_PASSWORD) or die "Unable to connect to pocontrols";

    # my $query = $dbh->prepare("select CMA_CMP_ID, CMA_CHIP_ID from pocontrols.fgc_component_mapping where CMA_CMP_ID not like 'HCRALMQ003%' or CMA_CMP_ID in (select component_mtf_id from pocontrols.FGC_SYSTEMS_COMPOSITION where component_mtf_id like 'HCRALMQ003%' and (SYSTEM_TYPE_ID != 30 or SYSTEM_TYPE_ID is NULL))")
    # my $query = $dbh->prepare("select CMA_CMP_ID, CMA_CHIP_ID from pocontrols.fgc_component_mapping where CMA_CMP_ID in (select component_mtf_id from pocontrols.FGC_SYSTEMS_COMPOSITION where (SYSTEM_TYPE_ID != 30 or SYSTEM_TYPE_ID is NULL))")
    # EPCCCS-9320: A HCRALMQ003 component was replaced in LHC. The above queries were not returning the mapping for this component and hence the component showed up as 'UNKNOWN' in FGCRun+. Using non-patched query as the other filters seem to be efficient enough not to reach file limit.
    my $query = $dbh->prepare("select CMA_CMP_ID, CMA_CHIP_ID from pocontrols.fgc_component_mapping")
    or die "Failed to prepare database query\n" .$dbh->errstr;

    $query->execute() or die "could not execute SQL query";

    my $results = $query->fetchall_arrayref({});

    FGC::DB::disconnect($dbh);

    return $results;
}

# EPCCCS-8559: FGC2 and FGC3 IDDB size limits reached. We need to filter out components that are currently not in use to reduce the number of Barcode-ID associations in the binaries.
sub get_spare_barcodes_by_platform($)
{
    my ($platform) = @_;

    # select spare parts based on platform
    my $query_string;
    if($platform eq 'fgc2')
    {
        $query_string = "
            SELECT BARCODE FROM ALIM.BARCODE_MANAGER_SPARE_CCS
            WHERE (EDMS_EQP_CODE = 'HCRFBEA___'
                OR EDMS_EQP_CODE = 'HCRFBBB___'
                OR EDMS_EQP_CODE = 'HCRFBHA___'
                OR EDMS_EQP_CODE = 'HCRFBDA___'
                OR EDMS_EQP_CODE = 'HCRFBCA___'
                OR EDMS_EQP_CODE = 'HCRFBAC___')
        ";
    }
    elsif($platform eq 'fgc3')
    {
        $query_string = "
            SELECT BARCODE FROM ALIM.BARCODE_MANAGER_SPARE_CCS
            WHERE (EDMS_EQP_CODE = 'HCRAMGC004'
                OR EDMS_EQP_CODE = 'HCRALSX004'
                OR EDMS_EQP_CODE = 'HCRAIRL003'
                OR EDMS_EQP_CODE = 'HCRAIRM003'
                OR EDMS_EQP_CODE = 'HCRAIRO003'
                OR EDMS_EQP_CODE = 'HCRAMEY003'
                OR EDMS_EQP_CODE = 'HCRAMES002'
                OR EDMS_EQP_CODE = 'HCRAKCT002'
                OR EDMS_EQP_CODE = 'HCRBACR001'
                OR EDMS_EQP_CODE = 'HCRBABV001'
                OR EDMS_EQP_CODE = 'HCRFAAB002'
                OR EDMS_EQP_CODE = 'HCRFAAB001'
                OR EDMS_EQP_CODE = 'HCRALCV005'
                OR EDMS_EQP_CODE = 'HCRAKZY005'
                OR EDMS_EQP_CODE = 'HCRAKZX005'
                OR EDMS_EQP_CODE = 'HCRBABT003'
                OR EDMS_EQP_CODE = 'HCRAMVU003'
                OR EDMS_EQP_CODE = 'HCRAJYH002'
                OR EDMS_EQP_CODE = 'HCRAJYH003'
                OR EDMS_EQP_CODE = 'HCRBAAZ001'
                OR EDMS_EQP_CODE = 'HCRBABP001'
                OR EDMS_EQP_CODE = 'HCRBACE002'
                OR EDMS_EQP_CODE = 'HCRBABS002'
                OR EDMS_EQP_CODE = 'HCRAJQU003'
                OR EDMS_EQP_CODE = 'HCRAJNO002'
                OR EDMS_EQP_CODE = 'HCRBACD002'
                OR EDMS_EQP_CODE = 'HCRAKZV002'
                OR EDMS_EQP_CODE = 'HCRALCV002'
                OR EDMS_EQP_CODE = 'HCRALCP002'
                OR EDMS_EQP_CODE = 'HCRALCN002'
                OR EDMS_EQP_CODE = 'HCRALCO002'
                OR EDMS_EQP_CODE = 'HCRALDD002'
                OR EDMS_EQP_CODE = 'HCRALWY002'
                OR EDMS_EQP_CODE = 'HCRAMCP002'
                OR EDMS_EQP_CODE = 'HCRAMKX001'
                OR EDMS_EQP_CODE = 'HCRAJXW003'
                OR EDMS_EQP_CODE = 'HCRAJJN002'
                OR EDMS_EQP_CODE = 'HCRAJNL002'
                OR EDMS_EQP_CODE = 'HCRAJOJ001'
                OR EDMS_EQP_CODE = 'HCRAJYM004'
                OR EDMS_EQP_CODE = 'HCRAKLN002')
        ";
    }
    else
    {
        return {};
    }

    # select barcodes of to be filtered spare / obsolete parts from db and store them in a lookup hash {barcode->1}
    my $dbh = FGC::AlimDB::connect(ALIM_DB_USERNAME, ALIM_DB_PASSWORD) or die "Unable to connect to alim db";
    my $query = $dbh->prepare($query_string) or die "Failed to prepare Alim database filter query\n" .$dbh->errstr;
    $query->execute() or die "could not execute SQL filter query on alim db";
    my %barcodes_to_be_filtered = map {@$_ => 1} @{$query->fetchall_arrayref()};
    FGC::DB::disconnect($dbh);

    return \%barcodes_to_be_filtered;
}

# End of functions


# Check that valid arguments have been supplied

die "\nUsage: ", basename($0), " <IDDB output file> <PTDB output file> [<platform>]\n\n" if(@ARGV < 2);
my ($iddb_path, $ptdb_path, $platform) = @ARGV;
$platform = lc $platform if(defined($platform));

# Read barcodes from DB
my $barcodes_ids;
# Patch for filter RadDIM from FGC2 dinamically
if ($platform eq "fgc2") {
    $barcodes_ids = get_hcr_id_pairs_patch();
}else{
    $barcodes_ids = get_hcr_id_pairs();
}

my $pairs = filter_by_platform ( $barcodes_ids, $platform );

# Validate provided data

validate_input( $pairs, $platform);

# Construct part type to ids mapping

my $part_type__ids = construct_part_type__ids ( $pairs );

# Construct id to serial mapping

my $id__serial = construct_id__serial( $pairs );

# Construct part type ranges

my ( $ranges, $id_range ) = construct_ranges($part_type__ids, $id__serial);

# Construct ID to part index

my $id_types = construct_id_to_part_idx($id_range, $id__serial);

# Count how many ids there are to provide it to directory constructors

my $ids = scalar keys %$id__serial;

# Construct directories

construct_dir_3($ids, $id_types);
construct_dir_2($ids, $id_types);
construct_dir_1($ids, $id_types);

# Write part type database

write_file
    $ptdb_path,
    { binmode => ':raw'},
    construct_part_type_db($ranges);

# Write ID database

write_file
    $iddb_path,
    { binmode => ':raw'},
    @id_db_records;

# EOF
