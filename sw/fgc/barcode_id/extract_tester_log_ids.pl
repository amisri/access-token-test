#!/usr/bin/perl -w
#
# Name:     extract_tester_log_ids.pl
# Purpose:  Extract barcodes and Dallas IDs from tester logfiles
# Author:   Stephen Page
#

use strict;
use File::Basename;

# Check that valid arguments have been supplied

die "\nUsage: ".basename($0)." <filename> [<filename 2> ...]\n\n" if(!@ARGV);

my %fgcs;

my %equipment;

my %id_barcode;
my %invalid_ids;

my $newline = "\015\012";

my %type_num_ids =  (
                        HCRFBMA => 2,
                        HCRFBNA => 2,
                        HCRFBPA => 2,
                        HCRFBSA => 2,
                    );

my %tester_ids =    (
                        "0113C7E80800003C" => 1,
                        "01232EE8080000F8" => 1,
                        "012F12450A00005B" => 1,
                        "0137C6E80800009B" => 1,
                        "0142BFE808000076" => 1,
                        "014A1D450A00001C" => 1,
                        "01A376D7090000C0" => 1,
                        "01A61D450A000048" => 1,
                        "01B471E808000036" => 1,
                        "01C748E808000080" => 1,
                        "01D122450A0000BE" => 1,
                        "01E254D7090000F9" => 1,
                        "01F725450A0000EB" => 1,
                        "900000029A7D4A28" => 1,
                        "CA0000029A706A28" => 1,
                        "CE0000029A799D28" => 1,
                        "E20000036C393028" => 1,
                    );

for my $filename (@ARGV)
{
    # Open input file

    open(FILE, "<$filename") or die "Unable to open file $filename: $!\n";

    # Read in entire input file

    my $file = join("", <FILE>);

    # Close input file

    close(FILE);

    # Remove start of server run indicator "^"

    $file =~ s/\^$newline//g;

    # Extract individual test entries from file

    my @test_entries = split(/$newline\*$newline/, $file);

    # Process each test entry

    for my $test_entry (@test_entries)
    {
        next if($test_entry =~ /^\s*$/); # Ignore end-of-file
        $test_entry .= $newline;

        my %test;

        $test_entry =~ /^\-(.*?)$newline(.*)/s or die "No match for test header line in file $filename\n";
        ($test{date}, $test{barcode}, $test{crc}) = split(",", $1);

        push(@{$equipment{$test{barcode}}->{tests}}, \%test);
        $equipment{$test{barcode}}->{barcode}   = $test{barcode};
        $equipment{$test{barcode}}->{type}      = substr($test{barcode}, 0, 7);

        my @log_entries = split("�", $2);
        shift(@log_entries);

        for my $log_entry (@log_entries)
        {
            my %log;
            push(@{$test{logs}}, \%log);

            $log_entry =~ /^(.*?)$newline(.*)/s;
            my $log_summary = $1;
            my $log_details = $2;
            my $delimiter   = $log_entry =~ /\`/ ? "\`" : ",";

            ($log{status}, $log{header}, $log{command_index})   = split($delimiter, $log_summary);
            (my $temp, $log{function}, $log{report})            = split($delimiter, $log_details);

            if($log{function} =~ /^IdCheck;/ && $log{status} eq "PASS")
            {
                # Check whether DUT is an FGC

                if($test{barcode} =~ /^HCRF[MN]/) # DUT is an FGC
                {
                    $fgcs{$test{barcode}}->{barcode} = $test{barcode};

                    for my $id (split(/$newline/, $log{report}))
                    {
                        $id =~ s/(.*?)\s.*/$1/; # Remove temperatures

                        # Skip tester IDs

                        next if(defined($tester_ids{$id}));

                        # Add ID to FGC

                        $fgcs{$test{barcode}}->{ids}->{$id} = 1;
                    }
                }
                else # DUT is not an FGC
                {
                    for my $id (split(/$newline/, $log{report}))
                    {
                        $id =~ s/(.*?)\s.*/$1/; # Remove temperatures

                        # Skip tester IDs

                        next if(defined($tester_ids{$id}));

                        # Check whether ID is associated with another barcode

                        if(defined($id_barcode{$id}) && $id_barcode{$id} ne $test{barcode}) # ID is associated with another barcode
                        {
                            # Add ID to invalid ID list

                            $invalid_ids{$id} = substr($test{barcode}, 0, 13)."000000";

                            warn "$id associated with multiple barcodes ($id_barcode{$id}, $test{barcode})\n";
                        }
                        else
                        {
                            $id_barcode{$id}                            = $test{barcode};
                            $equipment{$test{barcode}}->{ids}->{$id}    = 1;
                        }
                    }
                }
            }
        }
    }
}

# Check numbers of IDs

for my $component (values(%equipment))
{
    my $num_ids = keys(%{$component->{ids}});

    if(defined($type_num_ids{$component->{type}}))
    {
        if($num_ids > $type_num_ids{$component->{type}})
        {
            warn "Incorrect number of IDs ($num_ids where maximum is $type_num_ids{$component->{type}}) for $component->{barcode}\n";

            # Add all component IDs to invalid ID list

            for my $id (sort(keys(%{$component->{ids}})))
            {
                $invalid_ids{$id} = substr($component->{barcode}, 0, 13)."000000";
            }
            $component->{ids} = ();
        }
    }
    else # No number of IDs specified for type, default to 1
    {
        if($num_ids != 1)
        {
            warn "Incorrect number of IDs ($num_ids should be 1) for $component->{barcode}\n";

            # Add all component IDs to invalid ID list

            for my $id (sort(keys(%{$component->{ids}})))
            {
                $invalid_ids{$id} = substr($component->{barcode}, 0, 13)."000000";
            }
            $component->{ids} = ();
        }
    }
}

# Link IDs to FGCs

for my $component (sort { $a->{barcode} cmp $b->{barcode} } values(%equipment))
{
    for my $fgc (values(%fgcs))
    {
        for my $id (keys(%{$component->{ids}}))
        {
            if(defined($fgc->{ids}->{$id}))
            {
                push(@{$component->{fgcs}}, $fgc);

                $component->{fgc} = $fgc if(!defined($component->{fgc}));
            }
        }
    }

    if(defined($component->{fgcs}) && @{$component->{fgcs}} > 1)
    {
        print STDERR "$component->{barcode} present in multiple FGCs (";

        for my $fgc (sort { $a->{barcode} cmp $b->{barcode} } (@{$component->{fgcs}}))
        {
            print STDERR "$fgc->{barcode} ";
        }
        print STDERR ")\n";
    }
}

# Print barcode to ID mappings

for my $component (sort { $a->{barcode} cmp $b->{barcode} } values(%equipment))
{
    next if($component->{barcode} =~ /^HCRFM/); # Ignore FGC ccomponenttes

    # Extract IdCheck function results

    for my $id (sort(keys(%{$component->{ids}})))
    {
        print "$component->{barcode},$id\n";
    }
}

# Write FGC inventory

for my $fgc (sort { $a->{barcode} cmp $b->{barcode} } (values(%fgcs)))
{
    next if($fgc->{barcode} !~ /-JT/);

    print STDERR "FGC,$fgc->{barcode},";

    for my $id (keys(%{$fgc->{ids}}))
    {
        print STDERR "$id_barcode{$id} " if(defined($id_barcode{$id}) && $id_barcode{$id} =~ /^HCRFBA/);
    }
    print STDERR "\n";
}

# EOF
