#!/bin/bash

# Releases a new version of PTDB/IDDB if there have been changes on the database since the last release
#
# @author Miguel Hermo Serans

set -e # exits if any command fails 
set -x # prints commands as they are executed

script=$(readlink -f "$0")
script_path=$(dirname "$script")
fgc_repo_path=$(readlink -f $script_path/../../../)

# Include variables common to all the installation scripts

if ! source "${script_path}/../codes/scripts/common.sh"; then
    echo >&2 "Error: Common functions could not be read: ${script_path}/../codes/scripts/common.sh"
    exit 1
fi

# IDDB/PTDB variables
declare -A iddb_code_name=( ['fgc2']='C003_50_IDDB' ['fgc3']='C018_60_IDDB' )
declare -A ptdb_code_name=( ['fgc2']='C004_50_PTDB' ['fgc3']='C019_60_PTDB' )
declare -A iddb_code_id=( ['fgc2']='3' ['fgc3']='18' )
declare -A ptdb_code_id=( ['fgc2']='4' ['fgc3']='19' )
declare -A class_id=( ['fgc2']='50' ['fgc3']='60' )
declare -A codes_local_dir=( ['fgc2']="${fgc_repo_path}/sw/fgc/codes/C003_50_IDDB-C004_50_PTDB" ['fgc3']="${fgc_repo_path}/sw/fgc/codes/C018_60_IDDB-C019_60_PTDB" )
declare -A codes_release_dir=( ['fgc2']="${release_path}/C003_50_IDDB-C004_50_PTDB" ['fgc3']="${release_path}/C018_60_IDDB-C019_60_PTDB" )

barcode_manager_diff_url="https://fgc-barcode-manager.web.cern.ch/fgc-barcode-manager/hcr_id/history#"

# Functions

timestamp_to_date()
{
	timestamp=$1

}

send_email()
{
	notification_list=$1
	diff_urls=$2
	version=$3
	
	mail -s "IDDB/PTDB version ${version} automatically released" "${notification_list}" <<EOF
IDDB/PTDB version ${version} was automatically released.

You can see the changes at the following urls:
${diff_urls}
EOF
}

generate_codes()
{
	class_name=$1
	version=$2
	
	if [  $class_name != 'fgc2' ] && [ $class_name != 'fgc3' ]; then
		>&2 echo "Unknown class name: $class_name\n";
		exit 1;
	fi
	
	echo "$class_name: Generating IDDB and PTDB..."
	
	${script_path}/mkiddb.pl ${codes_local_dir[$class_name]}/iddb.bin ${codes_local_dir[$class_name]}/ptdb.bin $class_name > mkiddb.log
	
	echo "$class_name: Tagging codes..."
    
	${fgc_repo_path}/sw/utilities/perl/tagcode.pl ${codes_local_dir[$class_name]} IDDB $version 196608 ${class_id[$class_name]} ${iddb_code_id[$class_name]}  ${codes_local_dir[$class_name]}/iddb.bin 0
	${fgc_repo_path}/sw/utilities/perl/tagcode.pl ${codes_local_dir[$class_name]} PTDB $version   8192 ${class_id[$class_name]} ${ptdb_code_id[$class_name]}  ${codes_local_dir[$class_name]}/ptdb.bin 0
}

release_codes()
{
	class_name=$1
	version=$2
	
	if [  $class_name != 'fgc2' ] && [ $class_name != 'fgc3' ]; then
		>&2 echo "Unknown class name: $class_name\n";
		exit 1;
	fi

	echo "$class_name: Releasing codes..."
    
	ssh $release_access "mkdir -p ${codes_release_dir[$class_name]}/fieldbus/$version/"
	scp ${codes_local_dir[$class_name]}/fieldbus/$version/${iddb_code_name[$class_name]} $release_access:${codes_release_dir[$class_name]}/fieldbus/$version/${iddb_code_name[$class_name]}
	scp ${codes_local_dir[$class_name]}/fieldbus/$version/${ptdb_code_name[$class_name]} $release_access:${codes_release_dir[$class_name]}/fieldbus/$version/${ptdb_code_name[$class_name]}
}

# Obtain the modification date of the DB and the released codes

db_timestamp=`${script_path}/get_db_modification_timestamp.pl`
fgc2_timestamp=`ls ${release_path}/C003_50_IDDB-C004_50_PTDB/fieldbus | sort | tail -n 1`
fgc3_timestamp=`ls ${release_path}/C018_60_IDDB-C019_60_PTDB/fieldbus | sort | tail -n 1`

affected_versions=''
diff_urls=''

# Generate FGC2 codes if necessary

if [ $db_timestamp -gt $fgc2_timestamp ]; then
	echo "fgc2: IDDB/PTDB obsolete, releasing and installing new version $db_timestamp from database"
	generate_codes 'fgc2' $db_timestamp
	release_codes 'fgc2' $db_timestamp

	# Get current and previous versions date in ISO format (as used by the barcode manager web app)
	curr_version=`date -d @$db_timestamp     +"%Y-%m-%d%%20%H:%M:%S"`
	prev_version=`date -d @$fgc2_timestamp   +"%Y-%m-%d%%20%H:%M:%S"`

	diff_urls="    C003_50_IDDB-C004_50_PTDB : ${barcode_manager_diff_url}datetime_min=${prev_version}&datetime_max=${curr_version}";

else
	echo "fgc2: IDDB/PTDB up to date"
fi

# Generate FGC3 codes if necessary

if [ $db_timestamp -gt $fgc3_timestamp ]; then
	echo "fgc3: IDDB/PTDB obsolete, releasing and installing new version $db_timestamp from database"
	generate_codes 'fgc3' $db_timestamp	
	release_codes 'fgc3' $db_timestamp

	# Get current and previous versions date in ISO format (as used by the barcode manager web app)
	curr_version=`date -d @$db_timestamp   +"%Y-%m-%d%%20%H:%M:%S"`
	prev_version=`date -d @$fgc3_timestamp +"%Y-%m-%d%%20%H:%M:%S"`
	
	diff_urls+=$'\n'
	diff_urls+="    C018_60_IDDB-C019_60_PTDB : ${barcode_manager_diff_url}datetime_min=${prev_version}&datetime_max=${curr_version}";
else
	echo "fgc3: IDDB/PTDB up to date"
fi

# If there were any changes, re-read codes on gateways and notify by email

if [ -n "$diff_urls" ]; then
	send_email "$notification_list" "$diff_urls" $db_timestamp
fi
