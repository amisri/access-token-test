#!/usr/bin/perl -w
#
# Purpose:  Gets the timestamp of the last modification of barcode/id pairings on the database
# Author:   Miguel Hermo

use strict;
use Time::Local;

use FGC::DB;
use Getopt::Long;
use Data::Dumper;

my $username;
my $password;

GetOptions('db_user:s'=>\$username, 'db_pw:s'=>\$password);

# Ask user for name and pw if not provided in options
if(!defined($username) || !defined($password))
{
    # Prompt for username

    print "\nUsername: ";
    chomp($username = <STDIN>);

    # Prompt for password

    print "Password: ";
    system("stty -echo");
    chomp($password = <STDIN>);
    system("stty echo");
    print "\n\n";
}

my $dbh = FGC::DB::connect($username, $password) or die "Unable to connect to database: $!\n";
my $sql = "select to_char(max(create_time_utc), \'YYYY-MM-DD hh24:mi:ss\') DATETIME from abc.fgc_equipment_mapping_hist";
my $date = $dbh->selectrow_array($sql) or die('could not execute query');

my ($year,$mon,$mday,$hour,$min,$sec) = split(/[\s.:-]+/, $date);
my $time = timelocal($sec,$min,$hour,$mday,$mon-1,$year);

print "$time";

FGC::DB::disconnect($dbh);
