/*!
 *  @file      mem.h
 *  @defgroup  FGC3:MCU
 *  @brief     Memory manipulation functions
 */

#ifndef MEM_H   // header encapsulation
#define MEM_H

// Includes

#include <stdint.h>


/*!
 * This function compares n_words of data at s1 and s2.
 *
 * @return 0 if data matched, non-zero otherwise
 */
uint16_t MemCmpWords(const void * d1, const void * d2, uint16_t n_words);


/*!
 * Perform CRC-CCITT calculation on the specified zone of words.
 * @return resulting crc is the return
 */
uint16_t MemCrc16(const void * address, uint32_t n_words);


/*!
 * This function sets the specified number of words starting from the given 32-bit address.
 * On RX memcpy takes profit of sstr.w
 */
void MemSetWords(void * to, uint16_t value, uint32_t n_words);


/*!
 * This function sets the specified number of words starting from the given 32-bit address.
 * On RX memcpy takes profit of sstr.l
 */
void MemSetDWords(void * to, uint32_t value, uint32_t n_words);


#endif  // MEM_H end of header encapsulation

// EOF
