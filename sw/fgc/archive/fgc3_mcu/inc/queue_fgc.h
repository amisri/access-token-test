/*---------------------------------------------------------------------------------------------------------*\
  File:         queue_fgc.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
         There is already a queue.h file in
         /rx-elf/bin/newlib/include/sys/queue.h
\*---------------------------------------------------------------------------------------------------------*/

#ifndef QUEUE_FGC_H      // header encapsulation
#define QUEUE_FGC_H

#ifdef QUEUE_FGC_GLOBALS
#define QUEUE_FGC_VARS_EXT
#else
#define QUEUE_FGC_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

struct queue                                            // Circular queue indecies and status structure
{
    BOOLEAN             is_full;                        // Queue full flag
    INT16U              in_idx;                         // Input index  0->(len-1)
    INT16U              out_idx;                        // Output index 0->(len-1)
    INT16U              dec_mask;                       // Decrement mask (len-1) where len must be power of 2
    char        *       buf;                            // Start of circular buffer
};

//-----------------------------------------------------------------------------------------------------------

#endif  // QUEUE_FGC_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: queue_fgc.h
\*---------------------------------------------------------------------------------------------------------*/
