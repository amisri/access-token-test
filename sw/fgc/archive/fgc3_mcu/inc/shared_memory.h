/*---------------------------------------------------------------------------------------------------------*\
  File:     shared_memory.h

  Purpose:  runtime values shared between Boot and Main

\*---------------------------------------------------------------------------------------------------------*/

#ifndef SHARED_MEMORY_H // header encapsulation
#define SHARED_MEMORY_H

#ifdef SHARED_MEMORY_GLOBALS
#define SHARED_MEMORY_VARS_EXT
#else
#define SHARED_MEMORY_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>
#include <codes_holder.h>       // for SHORT_LIST_CODE_LAST
#include <definfo.h>

//-----------------------------------------------------------------------------------------------------------

enum Tmainprog_seq
{
    FGC_MP_STAY_IN_MAIN,   // 0, was 0x0000 - Starting (the Boot will clear the value before launching the main program)
    FGC_MP_STAY_IN_BOOT,   // 1, was 0xB007 - Stay in boot (S DEVICE.BOOT executed)
    FGC_MP_BOOT_RUNNING,   // 2, was 0x0010
    FGC_MP_MAIN_RUNNING,   // 3, was 0x0011 - Running (the main program will set this once the ID scan is complete)
    FGC_MP_TEST_IN_BOOT    // 4
};

// the following values comes from P9.PORT.BYTE
enum Tnetwork_type
{
    NETWORK_TYPE_FIP,       // 0
    NETWORK_TYPE_ETHERNET   // 1, lan9215
};

//-----------------------------------------------------------------------------------------------------------

struct TStackUse
{
    INT8U       SM_STKUSE_MST;     // SM_STKUSE_MST   Millisecond task stack usage
    INT8U       SM_STKUSE_FBS;     // SM_STKUSE_FBS   Fieldbus task stack usage
    INT8U       SM_STKUSE_STA;     // SM_STKUSE_STA   State task stack usage
    INT8U       SM_STKUSE_PUB;     // SM_STKUSE_PUB   Publication task stack usage
    INT8U       SM_STKUSE_FCM;     // SM_STKUSE_FCM   Fieldbus command task stack usage
    INT8U       SM_STKUSE_SCM;     // SM_STKUSE_SCM   Terminal command task stack usage
    INT8U       SM_STKUSE_DLS;     // SM_STKUSE_DLS   Dallas task stack usage
    INT8U       SM_STKUSE_RTD;     // SM_STKUSE_RTD   Real-time display task stack usage
    INT8U       SM_STKUSE_TRM;     // SM_STKUSE_TRM   Terminal communications task stack usage
    INT8U       SM_STKUSE_BGP;     // SM_STKUSE_BGP   Background processing task stack usage
};

union TUnionStackUse
{
    // ToDo we cannot include tsk.h here to use MCU_NB_OF_TASKS (circular dependency)
    // See the proper way to break the circular dependency (Do we need TID in shared memory?)
    INT8U               b[11];  // MCU_NB_OF_TASKS
    struct TStackUse    task;
};

struct TWatchPoint
{
    INT16U      SM_WATCH_CTRL;          // SM_WATCH_CTRL       Watchpoint control: 0=OFF 1=EQUALS 2=NOT EQUALS
    INT16U      SM_WATCH_ADDR;          // SM_WATCH_ADDR       Watchpoint address in page 0
    INT16U      SM_WATCH_VALUE;         // SM_WATCH_VALUE      Watchpoint value to check at address
    INT16U      SM_WATCH_HITCOUNT;      // SM_WATCH_HITCOUNT   Hit counter
    INT16U      SM_WATCH_TSKID;         // SM_WATCH_TSKID      Task ID
    INT16U      SM_WATCH_ISRMASK;       // SM_WATCH_ISRMASK    ISR Mask}
}
;

struct TSharedMemory
{
    INT16U      taskID;                     // SM_TSKID task identifier updated at context switch
    INT16U      isrMask;                    // SM_ISRMASK updated every time there is an interrupt

    //        0 - Power start - Full self test - update codes - run main program
    //        1 - Slow start - Partial self test - update codes - run main program
    //        2 - Fast start - Minimal self test - run main program
    //     0xFF - Uninitialised value for boot type
    // at codes.h defined BOOT_PWR 0, BOOT_SLOW 1, BOOT_FAST 2, BOOT_UNKNOWN 0xFF
    INT16U      boot_seq;                   // SM_BOOTTYPE

    // enum Tmainprog_seq:
    //        0 - Stay in MainProg
    //        1 - Stay in BootProg
    //        2 - BootPorg running
    //        3 - MainProg running
    //        4 - Stay in BootProg because there is a test running. Manually set by 625 boot menu option
    //   0xFACE - MainProg running, only with this value will the boot restart the main program in the event of a fast start
    INT16U      mainprog_seq;               // SM_MPRUN

    // SM_INTERFACETYPE = shared_mem.analog_card_model, used by defprops.h
    INT16U      analog_card_model;          // SM_INTERFACETYPE  analog card model


    //    The boot will set this variable to indicate the type of network being used.
    //        0 - WorldFIP
    //        1 - Ethernet
    //  enum Tnetwork_type  network_card_model;
    // in the parser is defined as INT16U so we are forced to use INT16U as enum will be INT8U
    // giving wrong values when reading as a property
    INT16U      network_card_model;         // SM_NETWORKTYPE network card model

    // code_holder_locked_count is not used by MainProg, only by BootProg ???
    INT16U      code_holder_locked_count;   // SM_FLASHLOCKS     Number of locked code_holders

    INT16U      pad0;                       // Padding bytes

    // ToDo: must go to non volatile instead of shared memory ?
    // It is used after a restart to know how many entries to clear to zero to maintain the time base within the log
    // The main program writes the time of the last entry in the THOUR temperature log into this register.
    // It is used after a restart to know how many entries to clear to zero to maintain the time base within the log.
    INT32U      temperatureLogHour_timestamp;   // SM_THOURTIME   Unixtime of last THOUR log entry

    // The main program writes the time of the last entry in the TDAY temperature log into this register.
    // It is used after a restart to know how many entries to clear to zero to maintain the time base within the log.
    INT32U      temperatureLogDay_timestamp;    // SM_TDAYTIME    Unixtime of last TDAY log entry

    // The main program writes the index of the last entry in the THOUR temperature log into this register.
    // It is used after a restart to know how many entries to clear to zero to maintain the time base within the log.
    INT16U      temperatureLogHour_index;       // SM_THOURIDX    Index of last THOUR log entry

    // The main program writes the index of the last entry in the TDAY temperature log into this register.
    // It is used after a restart to know how many entries to clear to zero to maintain the time base within the log.
    INT16U      temperatureLogDay_index;        // SM_TDAYIDX    Index of last TDAY log entry

    // The main program writes the index of the last entry in the event log into this register.
    // It is used after a restart so that the event log can continue from where it was.
    INT16U      logEvent_index;                 // SM_LOGEVTIDX  Index of last event log entry

    // The main program writes the index of the last entry in the cycle log into this register.
    // It is used after a restart so that the event log can continue from where it was.
    INT16U      logCycle_index;                 // Index of last entry in the cycle log

    // SM_ONTIME = shared_mem.power_on_utc, used by defprops.h
    INT32U      power_on_utc;           // SM_ONTIME    1st UTC Unix time received after boot

    // SM_STARTTIME = shared_mem.run_software_utc, used by defprops.h
    // SM_STARTTIME UTC Unix time found when reboot (not from reset boot), or when boot jumps to Main program
    INT32U      run_software_utc;

    // SM_PWRTIME = shared_mem.seconds_since_powerup, used by defprops.h
    // SM_PWRTIME        seconds counter always incremented, zeroed when shared memory is cleared (after a power reset)
    INT32U      seconds_since_powerup;

    // SM_RUNTIME = shared_mem.seconds_since_reboot, used by defprops.h
    // SM_RUNTIME        seconds counter always incremented, zeroed at boot start
    INT32U      seconds_since_reboot;

    /*
        used by defprops.h
        SM_CODES_PLD        PLD version
        SM_CODES_BOOTPROG   Boot program version
        SM_CODES_MAINPROG   Main programs version
        SM_CODES_IDPROG     ID program version
        SM_CODES_IDDB       ID database version
        SM_CODES_PTDB       Part-type database version
        SM_CODES_SYSDB      Systems database version
        SM_CODES_COMPDB     Components database version
        SM_CODES_DIMDB      DIM types database version
    */
    INT32U      codes_version[SHORT_LIST_CODE_LAST - 1];        // SM_CODES , NameDB is not included

    /*
        used by defprops.h
        SM_STKUSE_MST   Millisecond task stack usage
        SM_STKUSE_FBS   Fieldbus task stack usage
        SM_STKUSE_STA   State task stack usage
        SM_STKUSE_PUB   Publication task stack usage
        SM_STKUSE_FCM   Fieldbus command task stack usage
        SM_STKUSE_SCM   Terminal command task stack usage
        SM_STKUSE_DLS   Dallas task stack usage
        SM_STKUSE_RTD   Real-time display task stack usage
        SM_STKUSE_TRM   Terminal communications task stack usage
        SM_STKUSE_BGP   Background processing task stack usage
    */
    union TUnionStackUse    stack_usage;            // SM_STKUSE         Stack usage (percent)
    struct TWatchPoint      nn9;                    // SM_WATCH          NanOS task identifier
};

//-----------------------------------------------------------------------------------------------------------

/*! Memory area shared by boot and main, mapped to 0x00000560 by the linker */
SHARED_MEMORY_VARS_EXT volatile struct TSharedMemory  __attribute__((section("shared_area"))) shared_mem;

//-----------------------------------------------------------------------------------------------------------

#endif  // SHARED_MEMORY_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: shared_memory.h
\*---------------------------------------------------------------------------------------------------------*/
