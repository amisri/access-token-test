/*!
 *  @file     codes_holder.h
 *  @defgroup FGC3:MCU boot
 *  @brief    FGC Boot code container
 *
 *  Description:
 */

#ifndef CODES_HOLDER_H  // header encapsulation
#define CODES_HOLDER_H

#ifdef CODES_HOLDER_GLOBALS
#define CODES_HOLDER_VARS_EXT
#else
#define CODES_HOLDER_VARS_EXT extern
#endif

// Includes

#include <stdint.h>
#include <assert.h>

#include <fgc_codes_gen.h>      // Global FGC constants
#include <fgc_code.h>           // code communication structures, for struct fgc_dev_name
#include <fgc_consts_gen.h>         // Global FGC constants
#include <label.h>              // for struct sym_name
#include <m16c62_link.h>        // for C62_BLK3_ADDR, C62_BLK456_ADDR, C62_BLK1_ADDR
#include <macros.h>

// Constants

enum FGC_codes_state
{
    CODE_STATE_UNKNOWN,
    CODE_STATE_DEV_FAILED,
    CODE_STATE_CORRUPTED,
    CODE_STATE_NOT_AVL,
    CODE_STATE_VALID,
};

enum FGC_codes_adv_code_state
{
    ADV_CODE_STATE_NOT_IN_USE,
    ADV_CODE_STATE_OK,
    ADV_CODE_STATE_UNKNOWN_CODE,
    ADV_CODE_STATE_BAD_LEN,
};

enum FGC_codes_update_state
{
    UPDATE_STATE_UNKNOWN,
    UPDATE_STATE_UPDATE,
    UPDATE_STATE_OK,
    UPDATE_STATE_LOCKED,
    UPDATE_STATE_FAILURE,
};

// Internal structures, unions and enumerations

struct code_block
{
    uint8_t     class_id;
    uint8_t     code_id;
    uint16_t    block_idx;
    uint16_t    code[(FGC_CODE_BLK_SIZE / 2)];
};

/*! struct for information received via the fieldbus (in time_var) from the gateway about "codes" */
struct advertised_code_info
{
    uint8_t     code_id;            // Code ID
    uint8_t     class_id;           // FGC class ID
    uint16_t    len_blks;           // not used at FGC3 boot
    uint32_t    version;            // Version (unixtime)
    uint16_t    old_version;        // Version used for the old versioning system
    uint16_t    crc;
};

/*!
 * This is a small subset from enum fgc_code_ids (customised for the FGC3 boot)
 * warning!!! changes here affects the pre filled variables codes_holder_info[] (in this file) and
 * erase_dev[], program_dev[], write_dev[], check_dev[] (in codes_fgc.h)
 */
enum fgc_code_ids_short_list
{
    SHORT_LIST_CODE_PLD,    // Xilinx FPGA bitstream eeprom
    SHORT_LIST_CODE_BT,     // Boot for operation
    SHORT_LIST_CODE_MP_DSP, // Main programs
    SHORT_LIST_CODE_IDPROG, // M16C62 block 3    (IdProg)
    SHORT_LIST_CODE_IDDB,   // M16C62 blocks 4-6 (IDDB)
    SHORT_LIST_CODE_PTDB,   // M16C62 block 1    (PTDB)
    SHORT_LIST_CODE_SYSDB,  // Sys Data base
    SHORT_LIST_CODE_COMPDB, // Comp Data base
    SHORT_LIST_CODE_DIMDB,  // Dim Data base
    SHORT_LIST_CODE_NAMEDB, // Name Data base
    SHORT_LIST_CODE_LAST    // dummy to have the number of elements to use in the for/while loops
};

struct TCodesHolderInfo
{
    uint16_t                      len_blks;       // Flash size (blocks)
    uint32_t                      base_addr;      // Flash base address
    struct advertised_code_info   update;         // Update code info (from Advertised Code table)
    char                        * label;          // Flash label, max 7 chars, as used in MenuCodesShowInstalled
    uint16_t                      locked_f;       // Code locked flag - 2 bytes because FGC3 pointer is 4 bytes
    struct fgc_code_info          dev;            // Installed code info
    enum FGC_codes_update_state   update_state;
    enum FGC_codes_state          dev_state;      // State of installed code
    uint16_t                      calc_crc;       // not used at FGC3 boot
};

//-----------------------------------------------------------------------------------------------------------

CODES_HOLDER_VARS_EXT struct TCodesHolderInfo codes_holder_info[SHORT_LIST_CODE_LAST]
#ifdef CODES_HOLDER_GLOBALS
        =
{
    { FGC_CODE_60_PLD_7_ANA101_SIZE_BLKS, SRAM_TMPCODEBUF_32,     { FGC_CODE_60_PLD_7_ANA101, .version = 0xDEADC0DE}, "PLD    " },
    { FGC_CODE_60_BOOTPROG_SIZE_BLKS,     CODES_BOOTPROG_32,      { FGC_CODE_60_BOOTPROG,     .version = 0xDEADC0DE}, "BL27-24" },
    { FGC_CODE_61_MAINPROG_SIZE_BLKS,     CODES_MAINPROG_32,      { FGC_CODE_61_MAINPROG,     .version = 0xDEADC0DE}, "BL23-17" },
    { FGC_CODE_31_IDPROG_SIZE_BLKS,       C62_BLK3_ADDR,          { FGC_CODE_31_IDPROG,       .version = 0xDEADC0DE}, "C62_3  " },
    { FGC_CODE_60_IDDB_SIZE_BLKS,         C62_BLK456_ADDR,        { FGC_CODE_60_IDDB,         .version = 0xDEADC0DE}, "C62_456" },
    { FGC_CODE_60_PTDB_SIZE_BLKS,         C62_BLK1_ADDR,          { FGC_CODE_60_PTDB,         .version = 0xDEADC0DE}, "C62_1  " },
    { FGC_CODE_60_SYSDB_SIZE_BLKS,        CODES_SYSDB_SYSDB_32,   { FGC_CODE_60_SYSDB,        .version = 0xDEADC0DE}, "BL14   " },
    { FGC_CODE_60_COMPDB_SIZE_BLKS,       CODES_COMPDB_COMPDB_32, { FGC_CODE_60_COMPDB,       .version = 0xDEADC0DE}, "BL16   " },
    { FGC_CODE_60_DIMDB_SIZE_BLKS,        CODES_DIMDB_DIMDB_32,   { FGC_CODE_60_DIMDB,        .version = 0xDEADC0DE}, "BL18   " },
    { FGC_CODE_NAMEDB_SIZE_BLKS,          CODES_NAMEDB_32,        { FGC_CODE_NAMEDB,          .version = 0xDEADC0DE}, "BL01   " }
}
#endif
;

// External variable definitions

/*!
 * Code state symbol names.
 */
CODES_HOLDER_VARS_EXT const struct sym_name code_state[]
#ifdef CODES_HOLDER_GLOBALS
        =
{
    {  CODE_STATE_UNKNOWN,      "Unknown"       },
    {  CODE_STATE_DEV_FAILED,   "Dev failed"    },
    {  CODE_STATE_CORRUPTED,    "Corrupted"     },
    {  CODE_STATE_NOT_AVL,      "Not avail."    },
    {  CODE_STATE_VALID,        "Valid"         },
    { 0 } // the label part == 0  marks the end of the table
};

static_assert(ArrayLen(code_state) == CODE_STATE_VALID + 2, "Length of code_state array doesn't match corresponding enum")
#endif
;

/*!
 * Code update state symbol names.
 */
CODES_HOLDER_VARS_EXT const struct sym_name update_state[]
#ifdef CODES_HOLDER_GLOBALS
        =
{
    {  UPDATE_STATE_UNKNOWN,    "UNKNOWN"   },
    {  UPDATE_STATE_OK,         "OK"        },
    {  UPDATE_STATE_UPDATE,     "UPDATE"    },
    {  UPDATE_STATE_LOCKED,     "LOCKED"    },
    {  UPDATE_STATE_FAILURE,    "FAILURE"   },
    { 0 } // the label part == 0  marks the end of the table
};

static_assert(ArrayLen(update_state) == UPDATE_STATE_FAILURE + 2, "Length of update_state array doesn't match corresponding enum")
#endif
;

/*!
 * Code advertised state symbol names.
 */
CODES_HOLDER_VARS_EXT const struct sym_name adv_code_state[]
#ifdef CODES_HOLDER_GLOBALS
        =
{
    {  ADV_CODE_STATE_NOT_IN_USE,   "Not in use"        },
    {  ADV_CODE_STATE_OK,           "OK"                },
    {  ADV_CODE_STATE_UNKNOWN_CODE, "Unknown code ID"   },
    {  ADV_CODE_STATE_BAD_LEN,      "Bad code len"      },
    { 0 } // the label part == 0  marks the end of the table
};

static_assert(ArrayLen(adv_code_state) == ADV_CODE_STATE_BAD_LEN + 2, "Length of adv_code_state array doesn't match corresponding enum")
#endif
;
/*! Extracted from NameDB */
CODES_HOLDER_VARS_EXT struct fgc_dev_name   dev_name;
/*! Extracted from NameDB */
CODES_HOLDER_VARS_EXT struct fgc_dev_name   gw_name;


#endif  // CODES_HOLDER_H end of header encapsulation

//EOF
