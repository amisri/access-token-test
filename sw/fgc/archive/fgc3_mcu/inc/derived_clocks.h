/*!
 *  @file     derived_clocks.h
 *  @defgroup FGC3:MCU
 *  @brief    Sub clock structures
 *
 *  Description: maps the registers existing in the PLD to control several clock outputs
 *
 */


#ifndef DERIVED_CLOCKS_H        // header encapsulation
#define DERIVED_CLOCKS_H

#ifdef DERIVED_CLOCKS_GLOBALS
#define DERIVED_CLOCKS_VARS_EXT
#else
#define DERIVED_CLOCKS_VARS_EXT extern
#endif


// Includes

#include <memmap_mcu.h>
#include <sleep.h>


// External function declarations

/*!
 * start DSP NMI Tick
 */
static inline void StartDspClock(void)
{
    TICK_CTRL_P |= TICK_CTRL_DSP_MASK16; // enable DSP NMI Tick

    TICK_CTRL_P |= TICK_CTRL_SYNC_TO_C0_MASK16; // stop all derived clock until C0 is fired
    TIME_TILL_C0_MS_P = 1; // in the next 20ms frame, after 1ms C0 will be fired
    // ending the synchronisation and starting all the enabled derived clocks

    // wait until DSP tick is operational
    sleepUs(1100); // 1.1ms
}


/*!
 * Stop Dsp, Adc, Dac derived clock generation
 */
static inline void StopDerivedClocks(void)
{
    TICK_CTRL_P &= ~TICK_CTRL_DSP_MASK16;   // disable DSP NMI Tick
    TICK_CTRL_P &= ~TICK_CTRL_ADC_MASK16;   // disable ADC transfer clock
    TICK_CTRL_P &= ~TICK_CTRL_DAC_MASK16;   // disable DAC transfer clock
    TICK_CTRL_P &= ~TICK_CTRL_SPI_MASK16;   // disable SPI
}

/*!
 * Stop the generation of the DSP NMI interrupt (DSP tick) by the PLD
 */
static inline void StopDspClock(void)
{
    TICK_CTRL_P &= ~TICK_CTRL_DSP_MASK16; // disable DSP NMI Tick
}

/*!
 * ADC derived clock generation
 */
static inline void StartAdcClock(void)
{
    TICK_CTRL_P |= TICK_CTRL_ADC_MASK16; // enable ADC transfer clock
}

/*!
 * DAC derived clock generation
 */
static inline void StartDacClock(void)
{
    TICK_CTRL_P |= TICK_CTRL_DAC_MASK16; // enable DAC transfer clock
}

/*!
 * SPI derived clock generation
 */
static inline void StartSpiClock(void)
{
    TICK_CTRL_P |= TICK_CTRL_SPI_MASK16; // enable SPI transfer clock
}

#endif  // DERIVED_CLOCKS_H end of header encapsulation

//EOF

