/*!
 *  @file     master_clk_pll.h
 *  @defgroup FGC3:MCU
 *  @brief    RT Clock Phase Locked Loop Functions.
 *
 *  Description    :
 *  Integration of the PLL.
 *  There are 3 version
 *      2 based on Digital PLL - RTC Boot and Main
 *      1 based in Analogue PLL
 *
 */

#ifndef MASTER_CLK_PLL_H        // header encapsulation
#define MASTER_CLK_PLL_H

#ifdef MASTER_CLK_PLL_GLOBALS
#define MASTER_CLK_PLL_VARS_EXT
#else
#define MASTER_CLK_PLL_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <definfo.h>            // for FGC_CLASS_ID

// Constants

/*! 45ms (allow two missed packed before change of state) */
#define GATEWAY_ALIVE_TIMEOUT_MS            45

/*! PLD master clock : 50MHz */
#define PLD_MASTER_CLOCK_COUNT_FOR_1MS      PLD_MASTER_CLOCK_KHZ

// External function declarations

/*!
 * This function is called from main() to set up the PLL
 */
void MasterClockAnaloguePllInit(void);

/*!
 * Return true if the given read/write block at given slot is present in the crate
 * (Has been seen last time we scanned the crate).
 */
void MasterClockAnaloguePllIteration(void);

#if (FGC_CLASS_ID == 60)
/*!
 * This function is executed on ms 5 and this is important
 * because the local_ms calculation is based on this knowledge
 *
 * @retval true if PLL is stable, false if function has timed out.
 */
BOOLEAN Wait10secForStablePLL(void);
#endif

#endif  // MASTER_CLK_PLL_H end of header encapsulation

// EOF

