/*!
 *  @file      cycle_time.h
 *  @defgroup  FGC3:MCU
 *  @brief     Interface to the Time registers related to cycles.
 */

#ifndef FGC_TIME_H
#define FGC_TIME_H

#ifdef CYCLE_TIME_GLOBALS
#define CYCLE_TIME_VARS_EXT
#else
#define CYCLE_TIME_VARS_EXT extern
#endif

// Includes

#include <stdint.h>
#include <macros.h>
#include <memmap_mcu.h>

// External function declarations

static inline void    CycTimeSetTimeTillEvent(uint32_t value_ms);
static inline int32_t CycTimeGetTimeTillEvent(void);
static inline void    CycTimeSetTimeTillC0(INT16U value_ms);
static inline INT16U  CycTimeGetTimeTillC0(void);

static inline BOOLEAN CycTimeTstStatus(void);
static inline BOOLEAN CycTimeTstPulseArm(void);
static inline void    CycTimeSetPulseArm(void);
static inline void    CycTimeClrPulseArm(void);
static inline void    CycTimeSetPulseWidth(uint32_t value_us);
static inline void    CycTimeSetPulseEtim(uint32_t value_us);

// External function definitions

static inline void CycTimeSetTimeTillEvent(uint32_t value_ms)
{
    TIME_TILL_EVT_US_P = value_ms * 1000;
}

static inline int32_t CycTimeGetTimeTillEvent(void)
{
    return (TIME_TILL_EVT_US_P);
}

static inline void CycTimeSetTimeTillC0(INT16U value_ms)
{
    // Decrement 1 because the FPGA will latch the value on the
    // next millisecond boundary

    TIME_TILL_C0_MS_P = value_ms - 1;
}

static inline INT16U CycTimeGetTimeTillC0(void)
{
    return (TIME_TILL_C0_MS_P);
}

static inline BOOLEAN CycTimeTstStatus(void)
{
    return (Test(TIME_DSP_FLAG_ARM_P, TIME_DSP_FLAG_ARM_SET_MASK16));
}

static inline BOOLEAN CycTimeTstPulseArm(void)
{
    return Test(TIME_DSP_FLAG_ARM_P, TIME_DSP_FLAG_ARM_SET_MASK16);
}

static inline void CycTimeSetPulseArm(void)
{
    Set(TIME_DSP_FLAG_ARM_P, TIME_DSP_FLAG_ARM_SET_MASK16);
}

static inline void CycTimeClrPulseArm(void)
{
    Clr(TIME_DSP_FLAG_ARM_P, TIME_DSP_FLAG_ARM_SET_MASK16);
}

static inline void CycTimeSetPulseEtim(uint32_t value_us)
{
    // Sets the estimated time - with respect to the time till event register -
    // for the rising edge of the pulse generated for the DSP flag pulse.

    *TIME_DSP_FLAG_ETIM_US_A = value_us;
}

static inline void CycTimeSetPulseWidth(uint32_t value_us)
{
    // Sets the width in microseconds of the pulse generated for the
    // DSP flag pulse.

    *TIME_DSP_FLAG_WIDTH_US_A = value_us;
}

#endif

// EOF
