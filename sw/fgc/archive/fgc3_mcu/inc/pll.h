/*---------------------------------------------------------------------------------------------------------*\
 File:          pll.h

 Purpose:       FGC3 MCU Software - Analogue Phase-locked loop functions

 Author:        Quentin.King@cern.ch

 Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PLL_H      // header encapsulation
#define PLL_H

#ifdef PLL_GLOBALS
#define PLL_VARS_EXT
#else
#define PLL_VARS_EXT extern
#endif

// Include files

#include <cc_types.h>

// General constants

#define PLL_PHASE_ERR_WINDOW            50              // Phase err closed window (negative only) in 40ns units
#define PLL_MAX_DAC_PPM                 199.95          // Max DAC value in ppm of VCXO frequency change
#define PLL_ITER_PERIOD                 0.02            // Base PLL function iteration period (s)
#define PLL_ITER_PERIOD_TICKS           500000          // 20 milliseconds period in 25 MHz clock ticks (40ns)
#define PLL_INT_FLTR_DELAY_ITERS        1000             // Delay after entering locked before filtering integrator
#define PLL_INTEGRATOR_FLTR_TC          100             // Windowed err filter time constant in PLL periods
#define PLL_WINDOWED_ERR_FLTR_TC        5               // Windowed err filter time constant in PLL periods
#define PLL_ETH_SYNC_CAL_FILTER_TC      1000            // Ethernet phase error calibration TC in PLL periods
#define PLL_DAC_HALF_RANGE              0x2000          // DAC is 14-bit
#define PLL_DAC_OFFSET                  0x2000          // Midpoint for VCXO is at 1.65V
#define PLL_VCXO_PULL_PPM               200.0           // VCXO frequency pull range in ppm (approximate)
#define PLL_VCXO_FREQ                   25000000        // VCXO nominal frequency
#define PLL_MAX_CAPTURE_ITERS           20000           // Max PLL periods in CAPTURE to reach locked
#define PLL_DELAY_TO_LOCK               10              // PLL periods with error below limit to lock
#define PLL_LOCKED_ERR_LIMIT            400             // Phase error limit for locked in 40ns units
#define PLL_WAIT_IN_FAULT_ITERS         250             // Time to wait in PLL_FAILED state before trying to sync

// Phase references

#define PLL_FIP_PHASE_REF               486750          // Phase ref for FIP sync
#define PLL_EXT_PHASE_REF               499996          // Phase ref for external sync via opto-coupler
#define PLL_ETH18_PHASE_REF             550000          // Phase ref for ms 18 Ethernet sync
#define PLL_ETH19_PHASE_REF             525000          // Phase ref for ms 19 Ethernet sync

// PLL constants

#define PLL_PROP_GAIN                   0.05            // PLL proportional gain
#define PLL_PERIOD_ITERS                10              // PLL period in 20ms iterations
#define PLL_INT_TC                      3.0             // PLL integrator time constant (s)
#define PLL_INT_GAIN                    (PLL_PROP_GAIN*PLL_ITER_PERIOD*PLL_PERIOD_ITERS/PLL_INT_TC)

// Structure declarations

struct pll_vars_sync
{
    INT32U      sync_time;                              // Sync times - must be set before calling PllCalc()
    INT32S      phase_err;                              // Phase error - for diagnostics
};

struct pll_vars_err
{
    INT16U      sync_count;                             // Number of syncs received
    INT16U      prev_sync_count;                        // Previous number of syncs received for debugging
    FP32        phase_err_sum;                          // Sum of phase errors
    FP32        phase_err_average;                      // Average of phase errors during previous PLL period
};

struct pll_vars
{
    INT32U      irqs_count_all;                         // Used by the BOOT to report diagnostics.

    BOOLEAN     enabled;                                // Flag indicating if the PLL is enabled
    INT16U      fip_f;                                  // FIP interface in use flag
    INT16U      ext_sync_f;                             // External sync is active
    INT16U      state;                                  // PLL state machine
    INT32U      time_in_state_iters;                    // Time in current state in iterations
    INT16U      iter_count;                             // Iteration counter
    INT16U      dac_clipped_f;                          // DAC is being clipped
    INT16U      under_locked_limit_count;               // Phase error is less than locked limit counter
    INT16U      dacvcxo_int;                            // Integer part of DAC value
    INT16U      dacvcxo_frac;                           // Fractional part of DAC value

    INT16U      no_ext_sync_count;                      // Statistics counter for missing external syncs
    BOOLEAN     no_ext_sync;                            // Flag indicating if the external sync signal is expected

    FP32        init_integrator_ppm;                    // Initial PLL integrator (ppm)
    FP32        integrator_ppm;                         // PLL integrator (ppm)
    FP32        filtered_integrator_ppm;                // Filtered PLL integrator (ppm)
    FP32        dac_ppm;                                // DAC control (ppm)
    FP32        pi_err;                                 // PI algorithm: error
    FP32        eth_sync_cal;                           // Calibration to make Eth sync match external sync

    struct pll_vars_sync fip;                           // FIP sync time and error
    struct pll_vars_sync ext;                           // external sync time and error
    struct pll_vars_sync eth18;                         // Ethernet sync time and error
    struct pll_vars_sync eth19;                         // Ethernet sync time and error

    struct pll_vars_err  external;                      // External sync
    struct pll_vars_err  network;                       // Network sync (FIP or Ethernet)
    struct pll_vars_err  network_windowed;              // Network windowed sync (FIP or Ethernet)

    // legacy (for Main properties)
    INT16U      num_unlocks;                            // Number of times PLL unlocks
};

// Variable definitions

PLL_VARS_EXT struct pll_vars        pll;                // PLL global structure

// Function declarations

void    PllInit(INT16U fip_f, FP32 init_integrator_ppm, FP32 init_eth_sync_cal);
INT32U  PllCalc(INT32U int_sync_time);
void    PllDacVcxo(void);

#endif  // PLL_H end of header encapsulation
// EOF
