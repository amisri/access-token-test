/*!
 *  @file     ana_temp_regul.h
 *  @defgroup FGC3:MCU
 *  @brief    FGC3 MCU Analogue Interface Functions
 *
 *  WARNING: Common to BOOT and MAIN
 */

#ifndef ANA_TEMP_REGUL_H
#define ANA_TEMP_REGUL_H

#ifdef ANA_TEMP_REGUL_GLOBALS
#define ANA_TEMP_REGUL_VARS_EXT
#else
#define ANA_TEMP_REGUL_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>

// Global structures, unions and enumerations

// TODO Make this variable static after ADC.INTERNAL.VREF_TEMP.PID is no longer needed

typedef struct ana_temp_regul
{
    FP32                adc_gain;       // ADC value over temperature value
    INT32S              integrator;     // Integral register
    INT32S              prev_error;     // Previous error register
    INT16U              prop_gain;      // Proportional gain
    INT16U              int_gain;       // Integral     gain
    INT16U              diff_gain;      // Derivative   gain
    INT16U              proc_time;      // Processing time for the regul algorithm
    INT16U              adc_ref;        // Temperature reference (raw ADC units)
    INT16U              on_f;           // Regulation ON flag
} ana_temp_regul_t;

// Global variable definitions

ANA_TEMP_REGUL_VARS_EXT ana_temp_regul_t ana_temp_regul;

// Constants

/*!
 * Duration of heater self testing (boot menu), in seconds
 *
 */
#define  ANA_HEAT_TEST_TIME_S           30

// External function declarations

/*!
 * This function initialises the temperature regulation.
 *
 * On the MAIN, this function must be called AFTER NvsInit so that ADC.INTERNAL.VREF_TEMP.REF is set.
 */
void AnaTempInit(void);

/*!
 * This function starts the temperature regulation.
 *
 * @param ref_temp Target temperature in C.
 */
void AnaTempRegulStart(FP32 ref_temp);

/*!
 * This function stops the temperature regulation.
 *
 * To properly turn off the heater and disable regulation, call:
 * AnaTempRegulStart(0.0);
 */
void AnaTempRegulStop(void);

/*!
 * This function computes the analogue interface temperature regulation.
 *
 * Uses a PID controller running at 1Hz.
 */
void AnaTempRegul(void);

/*!
 * Reads the temperature value of the voltage reference on the analogue board.
 *
 * ADC values are 10 bit.
 * 1deg = 47mV
 * temp(deg) = ADC_val * 3.3 / 1023 / 0.047  (*0.068634)
 *
 * @retval The temperature value in oC.
 */
FP32 AnaTempGetTemp(void);

/*!
 * * Sets the PID gains.
 *
 * @param p_gain Proportional gain.
 * @param i_gain Integral gain.
 * @param d_gain Derivative gain.
 */
void AnaTempSetPIDGain(INT16U p_gain, INT16U i_gain, INT16U d_gain);

/*!
 * Returns True if the temperature regulation is on, False otherwise.
 *
 * @retval True if the temperature regulation is on, False otherwise.
 */
BOOLEAN AnaTempGetStatus(void);

/*!
 * Sets the heater power. Maximum power is 2W.
 *
 * @param mW power to set.
 */
void AnaTempSetPower(INT16U mW);

/*!
 * Returns the P Gain value.
 *
 * @retval The P Gain value.
 */
INT16U AnaTempGetPGain(void);

/*!
 * Returns the I Gain value.
 *
 * @retval The I Gain value.
 */
INT16U AnaTempGetIGain(void);

/*!
 * Returns the D Gain value.
 *
 * @retval The D Gain value.
 */
INT16U AnaTempGetDGain(void);

/*!
 * Returns the PID integrator value.
 *
 * @retval The PID integrator value.
 */
INT32S AnaTempGetIntegrator(void);

/*!
 * Returns the ADC raw value.
 *
 * @retval The ADC raw value.
 */
INT16U AnaTempGetAdcRef(void);

/*!
 * Returns the ADC gain.
 *
 * @retval The ADC gain.
 */
FP32 AnaTempGetAdcGain(void);

/*!
 * Returns the processing time.
 *
 * @retval The processing time.
 */
INT16U AnaTempGetProcessingTime(void);

#endif

// EOF
