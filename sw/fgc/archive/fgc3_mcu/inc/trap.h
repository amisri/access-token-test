/*---------------------------------------------------------------------------------------------------------*\
  File:         trap.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef TRAP_H  // header encapsulation
#define TRAP_H

#ifdef TRAP_GLOBALS
#define TRAP_VARS_EXT
#else
#define TRAP_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------
struct TTrapData
{
    INT16U              isrdummy_count;
    INT16U              isrdummy_fix_count;
    INT16U              isrdummy_empty_count;
};
//-----------------------------------------------------------------------------------------------------------

void    IsrDummy(void) __attribute__((interrupt));
void    IsrDummyFix(void) __attribute__((interrupt));
void    IsrDummyEmpty(void) __attribute__((interrupt));
void    IsrOverflow(void) __attribute__((interrupt));
void    IsrWatchdogTimer(void) __attribute__((interrupt));
void    IsrUndefinedInstruction(void) __attribute__((interrupt));
void    PanicFGC3(INT32U leds0, INT32U leds1);
void    IsrTrap(void);

//-----------------------------------------------------------------------------------------------------------

TRAP_VARS_EXT struct TTrapData  trap; // used by trap.c

//-----------------------------------------------------------------------------------------------------------

#endif  // TRAP_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: trap.h
\*---------------------------------------------------------------------------------------------------------*/
