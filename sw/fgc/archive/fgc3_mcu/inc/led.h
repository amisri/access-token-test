/*!
 *  @file     led.h
 *  @defgroup FGC3:MCU
 *  @brief    FGC3 MCU led Interface Functions
 *
 *  WARNING: Common to BOOT and MAIN
 */

#ifndef LED_H
#define LED_H

#ifdef LED_GLOBALS
#define LED_VARS_EXT
#else
#define LED_VARS_EXT extern
#endif

// Includes

#include <dev.h>
#include <memmap_mcu.h>

// External function declarations


#define ALL_RED (RGLEDS_NET_RED_MASK16 | RGLEDS_FGC_RED_MASK16 |RGLEDS_PSU_RED_MASK16 \
                 | RGLEDS_VS_RED_MASK16 |RGLEDS_DCCT_RED_MASK16 |RGLEDS_PIC_RED_MASK16)
#define ALL_GREEN (RGLEDS_NET_GREEN_MASK16 | RGLEDS_FGC_GREEN_MASK16 |RGLEDS_PSU_GREEN_MASK16 \
                   | RGLEDS_VS_GREEN_MASK16 |RGLEDS_DCCT_GREEN_MASK16 |RGLEDS_PIC_GREEN_MASK16)

/*!
 * This macro asks to set the given RED or GREEN led.
 *
 * @param led {FIP,FGC,PSU,VS,DCCT,PIC}_{RED,GREEN}
 */
#define LED_SET(led)            dev.leds |= RGLEDS_##led##_MASK16

/*!
 * This macro resets the given RED or GREEN led.
 *
 * @param led {FIP,FGC,PSU,VS,DCCT,PIC}_{RED,GREEN}
 */
#define LED_RST(led)            dev.leds &= ~ RGLEDS_##led##_MASK16

/*!
 * This macro toggles the given RED or GREEN led.
 *
 * @param led {FIP,FGC,PSU,VS,DCCT,PIC}_{RED,GREEN}
 */
#define LED_TOGGLE(led)         dev.leds ^= RGLEDS_##led##_MASK16

/*!
 * This macro turns all green/red leds off.
 *
 */
#define LED_RST_ALL()            dev.leds = 0x0000

/*!
 * This macro turns on all red leds.
 *
 */
#define LED_SET_ALL_RED()       dev.leds = ALL_RED

/*!
 * This macro will actually write the LEDs
 *
 */
#define LED_SET_PORT(leds)       RGLEDS_P = leds


/*!
 * This macro sets the given BLUE led.
 * Blue Leds are for boot only.
 *
 * @param led {FIP,FGC,PSU,VS,DCCT,PIC}_BLUE
 */
#define BLED_SET(led)           BLEDS_P |= BLEDS_##led##_MASK16

/*!
 * This macro resets the given BLUE led.
 * Blue Leds are for boot only.
 *
 * @param led {FIP,FGC,PSU,VS,DCCT,PIC}_BLUE
 */
#define BLED_RST(led)           BLEDS_P &= ~BLEDS_##led##_MASK16

/*!
 * This macro toggles the given BLUE led.
 * Blue Leds are for boot only.
 *
 * @param led {FIP,FGC,PSU,VS,DCCT,PIC}_BLUE
 */
#define BLED_TOGGLE(led)        BLEDS_P ^= BLEDS_##led##_MASK16

/*!
 * This macro set all leds off.
 *
 */
#define BLED_RST_ALL()          BLEDS_P = 0x0000


/*!
 * This function must be called regularly in order to generate a visible orange color for warnings,
 * and blink red lights for error.
 *
 */
static inline void LedManage(uint16_t ms)
{
    uint16_t blink_mask = 0xFFFF;

    // Toggle Red light at 2 Hz

    if (ms < 500)
    {
        blink_mask = 0xFFFF;
    }
    else
    {
        // Mask to turn off Red lights (if red set and green unset)
        blink_mask = ~((dev.leds & ALL_RED) & ~((dev.leds & ALL_GREEN) >> 1));
    }

    if ((ms & 3) == 3)            // 25% of time
    {
        // Apply mask to turn on/off Red Lights at 2Hz

        LED_SET_PORT(dev.leds & blink_mask);
    }
    else
    {
        // Apply mask to turn on/off Red Lights at 2Hz
        // Turn off red light 75% of time if we want orange (GREEN and RED set)

        LED_SET_PORT((dev.leds & ~((dev.leds & ALL_RED) & ((dev.leds & ALL_GREEN) >> 1))) & blink_mask);
    }

}

#endif

// EOF
