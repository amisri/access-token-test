/*!
 *  @file     hardware_setup.h
 *  @defgroup FGC3:MCU
 *  @brief    RX610 hardware related stuff
 *
 */


#ifndef HARDWARE_SETUP_H    // header encapsulation
#define HARDWARE_SETUP_H

// Includes

#include <cc_types.h>       // basic typedefs
#include <os_hardware.h>
#include <iodefines.h>
#if (FGC_CLASS_ID == 60)
#include <fieldbus_boot.h>
#endif


/*!
 *
 */
void HardwareSetup(void);


/*!
 *   This function will disable interrupt generation
 */
static inline void DisableNetworkInterrupt(void)
{
    //  __asm__ volatile ( "clrpsw I" ); // clear I flag in PSW

    //  IRQ2 - vector 66 - FIP_~MSG_IRQ - INT_Excep_IRQ2()
    ICU.IER08.BIT.IEN2 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR22.BIT.IPR_20 = 0x00;        // Interrupt Priority Register

    //  when  the FIP board this comes from exactly the same event as IRQ2, so we don't use it
    //  IRQ3 - vector 67 - FIP_~TIME_IRQ or ETHERNET 50Hz pulse - INT_Excep_IRQ3()
    //  ICU.IER08.BIT.IEN3 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR23.BIT.IPR_20 = 0x00;        // Interrupt Priority Register

#if (FGC_CLASS_ID != 60)
    // Disable end of DMA0 interrupt and cancel any DMA in progress
    ICU.IER18.BIT.IEN6 = 0;
    DMAC_COMMON.DMSCNT.BIT.DMST = 0;    // Stop DMAC
    DMAC0.DMCRB.BIT.DSCLR = 1;          // Internal Status clear
    DMAC1.DMCRB.BIT.DSCLR = 1;          // Internal Status clear
#endif
}


/*!
 *
 */
static inline void Disable1msTickInterrupt(void)
{
    //  IRQ4 - vector 68 - MCU_~TICK - INT_Excep_IRQ4()
    ICU.IER08.BIT.IEN4 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR24.BIT.IPR_20 = 0x00;        // Interrupt Priority Register


    //  TMR0 CMIA0 - Compare Match A Interrupt - vector 174 - INT_Excep_TMR0_CMIA0()
    //  TMR0 it is programmed to generate an interrupt every 1ms
    ICU.IER15.BIT.IEN6 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR68.BIT.IPR_20 = 0x00;        // Interrupt Priority Register

    // These 2 are never disabled

    // TMR3 OVI3 - Timer Overflow Interrupt - vector 185 - INT_Excep_TMR3_OVI3()
    // TMR3 will count [0..255] incremented by TMR2 count match (1us) and generating an interrupt when overflows
    //  ICU.IER17.BIT.IEN1 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR6B.BIT.IPR_20 = 0x00;        // Interrupt Priority Register

    // TPU0.TGRA input capture/compare match Interrupt - vector 104 - INT_Excep_TPU0_TGI0A()
    //  ICU.IER0D.BIT.IEN0 = 0;             // interrupt Request Enable Register
    //  ICU.IPR4C.BIT.IPR_20 = 0x00;        // Interrupt Priority Register
}


/*!
 *   This function will enable the 1ms tick interrupt generation from TIMER A1
 *
 *   Maskable interrupts are enabled and disabled by using
 *   a) the interrupt enable flag (I flag),
 *   b) interrupt priority level select bit
 *   c) processor interrupt priority level (IPL).
 */
static inline void Enable1msTickInterruptFromMcuTimer(void)
{
#if (FGC_CLASS_ID == 60)
    // TMR0 CMIA0 - Compare Match A Interrupt - vector 174 - INT_Excep_TMR0_CMIA0()
    // TMR0 it is programmed to generate an interrupt every 1ms
    ICU.IER15.BIT.IEN6 = 1;             // Interrupt Request Enable Register
    ICU.IPR68.BIT.IPR_20 = 0x03;        // Interrupt Priority Register
#endif

    // this 2 events are level 7 !!!, will not be disabled by OS_ENTER_CRITICAL()

    //  TMR3 OVI3 - Timer Overflow Interrupt - vector 185 - INT_Excep_TMR3_OVI3()
    //  TMR3 will count [0..255] incremented by TMR2 count match (1us) and generating an interrupt when overflows
    ICU.IER17.BIT.IEN1 = 1;             // Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20 = 0x07;        // Interrupt Priority Register

    //  TPU0.TGRA input capture/compare match Interrupt - vector 104 - INT_Excep_TPU0_TGI0A()
    ICU.IER0D.BIT.IEN0 = 1;             // interrupt Request Enable Register
    ICU.IPR4C.BIT.IPR_20 = 0x07;        // Interrupt Priority Register
}


/*!
 *   This function will enable the 1ms tick interrupt generation from the PLD.
 *
 *   Maskable interrupts are enabled and disabled by using
 *   a) the interrupt enable flag (I flag),
 *   b) interrupt priority level select bit
 *   c) processor interrupt priority level (IPL).
 */
static inline void Enable1msTickInterruptFromPLD(void)
{
    //  IRQ4 - vector 68 - MCU_~TICK - INT_Excep_IRQ4()
    ICU.IER08.BIT.IEN4 = 1;             // Interrupt Request Enable Register
    ICU.IPR24.BIT.IPR_20 = 0x03;        // Interrupt Priority Register

    // this 2 events are level 7 !!!, will not be disabled by OS_ENTER_CRITICAL()

    //  TMR3 OVI3 - Timer Overflow Interrupt - vector 185 - INT_Excep_TMR3_OVI3()
    //  TMR3 will count [0..255] incremented by TMR2 count match (1us) and generating an interrupt when overflows
    ICU.IER17.BIT.IEN1 = 1;             // Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20 = 0x07;        // Interrupt Priority Register

    //  TPU0.TGRA input capture/compare match Interrupt - vector 104 - INT_Excep_TPU0_TGI0A()
    ICU.IER0D.BIT.IEN0 = 1;             // interrupt Request Enable Register
    ICU.IPR4C.BIT.IPR_20 = 0x07;        // Interrupt Priority Register
}


/*!
 *
 */
static inline void EnableNetworkInterrupt(void)
{
#if (FGC_CLASS_ID == 60)
    FieldbusEnableInterrupts ();
#endif

    //  IRQ2 - vector 66 - FIP_~MSG_IRQ - INT_Excep_IRQ2()
    ICU.IER08.BIT.IEN2 = 1;             // Interrupt Request Enable Register
    ICU.IPR22.BIT.IPR_20 = 0x02;        // Interrupt Priority Register

    // when  the FIP board this comes from exactly the same event as IRQ2, so we don't use it
    //  IRQ3 - vector 67 - FIP_~TIME_IRQ or ETHERNET 50Hz pulse - INT_Excep_IRQ3()
    //  ICU.IER08.BIT.IEN3 = 1;             // Interrupt Request Enable Register
    //  ICU.IPR23.BIT.IPR_20 = 0x03;        // Interrupt Priority Register

#if (FGC_CLASS_ID != 60)
    // End of DMA0 interrupt
    DMAC_COMMON.DMEDET.BIT.DEDET0 = 1;  // Clear interrupt
    DMAC_COMMON.DMEDET.BIT.DEDET1 = 1;  // Clear interrupt
    ICU.IPR71.BIT.IPR_20 = 0x3;         // Set interrupt priority for DMA 1
    ICU.IER18.BIT.IEN7 = 1;             // Enable interrupt for DMA 1
    DMAC_COMMON.DMSCNT.BIT.DMST = 1;    // Start DMAC
#endif
}


/*!
 *
 */
static inline void EnableUARTsInterrupts(void)
{
    //-----------------------------------------------------------------------------------------------
    // UART 0

    // REMEBER: there is also the bits in the SCI structure that must be already being enabled
    //    SCI0.SCR.BIT.TIE = 0; // TXI interrupt request is disabled
    //    SCI0.SCR.BIT.RIE = 0; // RXI and ERI interrupt requests are disabled

    // SCI0_ERI0 - vector 214 - INT_Excep_SCI0_ERI0
    //  ICU.IER1A.BIT.IEN6 = 1; // Interrupt Request Enable Register

    // SCI0_RXI0 - vector 215 - INT_Excep_SCI0_RXI0
    //  ICU.IER1A.BIT.IEN7 = 1; // Interrupt Request Enable Register

    // SCI0_TXI0 - vector 216 - INT_Excep_SCI0_TXI0
    //  ICU.IER1B.BIT.IEN0 = 1; // Interrupt Request Enable Register

    // SCI0_TEI0 - vector 217 - INT_Excep_SCI0_TEI0
    //  ICU.IER1B.BIT.IEN1 = 1; // Interrupt Request Enable Register


    //  ICU.IPR80.BIT.IPR_20 = 0x01;    // Interrupt Priority Register
    //-----------------------------------------------------------------------------------------------
    // UART 1

    // REMEBER: there is also the bits in the SCI structure that must be already being enabled
    //    SCI1.SCR.BIT.TIE = 0; // TXI interrupt request is disabled
    //    SCI1.SCR.BIT.RIE = 0; // RXI and ERI interrupt requests are disabled

    // SCI1_ERI1 - vector 218 - INT_Excep_SCI1_ERI1()
    //  ICU.IER1B.BIT.IEN2 = 1; // Interrupt Request Enable Register

    // SCI1_RXI1 - vector 219 - INT_Excep_SCI1_RXI1()
    //  ICU.IER1B.BIT.IEN3 = 1; // Interrupt Request Enable Register

    // SCI1_TXI1 - vector - 220 - INT_Excep_SCI1_TXI1()
    //  ICU.IER1B.BIT.IEN4 = 1; // Interrupt Request Enable Register

    // SCI1_TEI1 - vector 221 - INT_Excep_SCI1_TEI1()
    //  ICU.IER1B.BIT.IEN5 = 1; // Interrupt Request Enable Register


    //  ICU.IPR81.BIT.IPR_20 = 0x01;    // Interrupt Priority Register
    //-----------------------------------------------------------------------------------------------
}

/*!
 * Software reset of the Rx610
 */
static inline void SoftwareReset(void)
{
    /*
     *     RSTCSR - Reset Control/Status Register  address 8802Bh value after reset 0x1F

    To write to this register, write data in WINB (Write window B register in 16 bits)

    b7 WOVF - Watchdog Timer Overflow Flag
    0: TCNT has not overflowed in watchdog timer mode
    1: TCNT has overflowed in watchdog timer mode

    b6 RSTE - Reset Enable
        0: The LSI is not reset internally when TCNT overflows in watchdog timer mode (TCNT and TCSR of the WDT are reset)
    1: The LSI is internally reset when TCNT overflows in watchdog timer mode



    TCSR - Timer Control/Status Register    address 88028h value after reset x0011000

    To write to this register, write data in WINB (Write window B register in 16 bits)

    b7 Reserved
        1: this bit is always read as an undefined value, the write value should always be 1

    b6 TMS - Timer Mode Select
    0: Interval timer mode, When TCNT overflows, an interval timer interrupt (WOVI) is requested
    1: Watchdog timer mode, When TCNT overflows, WDTOVF# is output (reset generated)

    b5 TME - Timer Enable
    0: TCNT stops counting and is initialised to 00h
    1: TCNT starts counting

    b4,b3 Reserved
        1: these bits are always read as 1, the write value should always be 1

    b2,b1,b0 CKS[2:0] - Clock Select
        000 PCLK/4



    WINA - Write Window A Register      address 88028h value after reset ???? 16 bits write only
    for writing TCNT (magic byte in HI is 0x5A) and TCSR (magic byte in HI is 0xA5)


    WINB - Write Window B Register      address 8802Ah value after reset ???? 16 bits write only
    for writing RSTCSR.RSTE (magic byte in HI is 0x5A) or RSTCSR.WOVF (magic byte in HI is 0xA5)
     */

    DISABLE_INTERRUPTS();

    WDT.WINB_RSTCSR.WINB = 0x5A5F;      // 0x5Axx writes in RSTCSR. RSTE = 1. Reset enable
    WDT.TCSR_WINA_TCNT.WINA = 0xA5F8;   // 0xA5xx writes in TCSR. TME = 1, TMS = 1

    // It will take 20.4us*255 ~ 5ms before the reset is generated

    // wait until the world blow in pieces
    while (1);
}

#endif  // HARDWARE_SETUP_H end of header encapsulation

// EOF

