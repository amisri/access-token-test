/*---------------------------------------------------------------------------------------------------------*\
  File:         extend_mask.h

  Contents:     Header file for extend_mask.c - An extended bit mask type for FGC3

  Notes:        An utility feature which purpose is to provide a bit mask data structure with up to 256 bits.
                For now it is only used by the publication task.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef EXTEND_MASK_H
#define EXTEND_MASK_H


#ifdef EXTEND_MASK_GLOBALS
#define EXTEND_MASK_VARS_EXT
#else
#define EXTEND_MASK_VARS_EXT extern
#endif

#include <cc_types.h>           // basic typedefs
#include <macros.h>             // For Test macro

//-----------------------------------------------------------------------------------------------------------

// struct extended_mask_gen:
//
// This is the generic mask structure that the functions defined in this module expect as an argument.
// There is no point in allocating this structure directly, since it only provides a mask of 16 bits.
//
// The user should allocate a similar structure, adapted to its needs, for instance:
//
//   struct mask_128bits                // A 128 bit mask. To be casted with struct extended_mask_gen in extend_mask.h
//   {
//       INT16U master;
//       INT16U mask_array[8];          // 8*16 = 128 bit array
//   };
//
//   struct mask_128bits   mask_128bits_inst;   // An instance of a 128 bit mask
//
//
// and cast &mask_128bits_inst with (struct extended_mask_gen*) before passing it as an argument to the
// functions of this module. Carrying on with the same example, the user would type:
//
//   ExtendMaskSetBit((struct extended_mask_gen *)&mask_128bits_inst, 42);      // To set   bit 42 in mask_128bits_inst
//   ExtendMaskClrBit((struct extended_mask_gen *)&mask_128bits_inst, 24);      // To clear bit 24 in mask_128bits_inst
//


#define ARR_ANY_SIZE (1)
struct extended_mask_gen
{
    INT16U  master;                         // Master mask where each bit represent one word in mask_array[]
    INT16U  mask_array[ARR_ANY_SIZE];       // Mask array. The intent here is to use a flexible array member,
    // but this is only supported in C99. The present syntax, with 1
    // as the size of the array, achieves the same and is fully
    // portable
};


//-----------------------------------------------------------------------------------------------------------

EXTEND_MASK_VARS_EXT const INT16U MASK16[16]
#ifdef EXTEND_MASK_GLOBALS
=
{
    0x0001, 0x0002, 0x0004, 0x0008,
    0x0010, 0x0020, 0x0040, 0x0080,
    0x0100, 0x0200, 0x0400, 0x0800,
    0x1000, 0x2000, 0x4000, 0x8000
}
#endif
;

EXTEND_MASK_VARS_EXT const INT16U IDX_MASK16[16]
#ifdef EXTEND_MASK_GLOBALS
=
{
    0xFFFF, 0xFFFE, 0xFFFC, 0xFFF8,
    0xFFF0, 0xFFE0, 0xFFC0, 0xFF80,
    0xFF00, 0xFE00, 0xFC00, 0xF800,
    0xF000, 0xE000, 0xC000, 0x8000
}
#endif
;


//-----------------------------------------------------------------------------------------------------------

// Test a bit in an extended mask and returns a BOOLEAN

#define ExtendMaskTestBit(extended_mask, idx) ( Test((extended_mask).mask_array[(idx)>>4], MASK16[(idx)&0xF]) )

//-----------------------------------------------------------------------------------------------------------

BOOLEAN ExtendMaskScan(struct extended_mask_gen * extend_mask_ptr, INT8U * idx_ptr);    // Max bit index: 255
void    ExtendMaskSetBit(struct extended_mask_gen * extend_mask_ptr, INT8U idx);        // Max bit index: 255
void    ExtendMaskClrBit(struct extended_mask_gen * extend_mask_ptr, INT8U idx);        // Max bit index: 255

//-----------------------------------------------------------------------------------------------------------

#endif  // EXTEND_MASK_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: extend_mask.h
\*---------------------------------------------------------------------------------------------------------*/

