/*---------------------------------------------------------------------------------------------------------*\
  File:     runlog_lib.h

  Purpose:  FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef RUNLOG_LIB_H        // header encapsulation
#define RUNLOG_LIB_H

#include <cc_types.h>
#include <fgc_runlog_entries.h>
#include <fgc_runlog.h>     // the item structure for the run log buffer
#include <memmap_mcu.h>
#include <stdint.h>         // basic types in other standard

#ifdef RUNLOG_LIB_GLOBALS
#define RUNLOG_LIB_VARS_EXT
#else
#define RUNLOG_LIB_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

struct TRunlogResetCounters
{
    INT16U      all;
    INT16U      power;
    INT16U      program;
    INT16U      manual;
    INT16U      fast_watchdog;
    INT16U      slow_watchdog;
    INT16U      dongle;
    INT16U      dsp;
};


struct TRunlog
{
    struct fgc_runlog               buffer[FGC_RUNLOG_N_ELS];
    INT16U                          index;
    struct TRunlogResetCounters     resets;
    INT16U                          magic;
};

//-----------------------------------------------------------------------------------------------------------

void    RunlogInit(void);
void    RunlogClrResets(void);
void    RunlogWrite(INT8U id, const void * data);
INT16U  RunlogRead(INT16U * index, struct fgc_runlog * runlog_entry);

/*---------------------------------------------------------------------------------------------------------*/
static INLINE void RunlogTimestamp(void)
/*---------------------------------------------------------------------------------------------------------*\
 Helper function to time-stamp logs.
\*---------------------------------------------------------------------------------------------------------*/
{
    RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));    // Unix time
    RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));          // Millisecond time
}
//-----------------------------------------------------------------------------------------------------------

#endif  // RUNLOG_LIB_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: runlog_lib.h
\*---------------------------------------------------------------------------------------------------------*/
