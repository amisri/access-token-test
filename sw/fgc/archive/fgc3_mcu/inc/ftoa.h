/*---------------------------------------------------------------------------------------------------------*\
  File:         ftoa.h

  Purpose:      float to ascii conversion

  History:

    Oct 2012 HL     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FTOA_H      // header encapsulation
#define FTOA_H

#ifdef FTOA_GLOBALS
#define FTOA_VARS_EXT
#else
#define FTOA_VARS_EXT extern
#endif

#include <cc_types.h>


#define LEN(array)      (sizeof(array)/sizeof(array[0]))
#define F_DIGITS        (LEN(pow10)-3)
#define FLOAT_MAX       1.0E+37
#define FLOAT_MIN       1.0E-37


#define ETYPE           0x1
#define FTYPE           0x2
#define GTYPE           (ETYPE | FTYPE)
#define BLANK           0x4
#define SIGNED          0x8
#define NEG             (SIGNED | BLANK)

#define UPPER           0x10
#define LEFT            0x20
#define ZEROPAD         0x40
#define ALT             0x80
//#define LONGVAL         0x100
//#define SHORTVAL        0x200
//#define PREC            0x400

void ftoa(char * buf, FP32 fval, INT16U prec, INT16U flags, INT16S width);

#endif  // FTOA_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ftoa.h
\*---------------------------------------------------------------------------------------------------------*/
