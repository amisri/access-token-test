/*!
 *  @file  pulse_permit.h
 *  @brief Provides the logic to block and unblock the converter based on the
 *  pulse permit signal, routed to the digital input INTLKSPARE.
 *
 *  This functionality is only required by the ComHV-PS chassis and the
 *  Modulator converter, used in the RF system. The pulse permit signal is
 *  managed by the RF system.
 */

#ifndef FGC_PULSE_PERMIT_H
#define FGC_PULSE_PERMIT_H

#include <cc_types.h>

/*!
 * @brief Returns True if the pulse permit (INTLKSPARE) is valid.
 *
 * This condition is only valid for the ComHV (RF systems) and the Modulator
 * converter.
 *
 * @retval True if the pulse permit is valid. False otherwise.
 */
BOOLEAN PulsePermitValid(void);

/*!
 * @brief Polls the pulse permit signal (INTLKSPARE digital input) to block
 * and unblock the converter.
 *
 *  This condition is only valid for the ComHV (RF systems) and the Modulator
 *  converter.
 */
void PulsePermitProcess(void);

#endif /* FGC_PULSE_PERMIT_H */

// EOF
