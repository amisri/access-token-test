/*!
 *  @file     ana_temp_regul.c
 *  @defgroup FGC3:MCU
 *  @brief    FGC3 MCU Analogue Interface Functions
 *
 *  WARNING: Common to BOOT and MAIN
 */

#define ANA_TEMP_REGUL_GLOBALS  // for ana_temp_regul global variable

// Includes

#include <ana_temp_regul.h>
#include <iodefines.h>
#include <memmap_mcu.h>
#include <shared_memory.h>
#if (FGC_CLASS_ID != 60)
#include <defprops.h>
#else
#include <math.h>
#endif

// Constants

#define ANA_TR_ADC_GAIN    14.57    // 0.047*1023/3.3
#define ANA_TR_PROP_GAIN   400
#define ANA_TR_INT_GAIN    7
#define ANA_TR_DIF_GAIN    20

// Macro to read the Vref raw temperature (RX610 on-chip 10-bit ADC)
#define ANA_VREF_RAW_TEMP()     (AD0.ADDRA & 0x3FF)

// External function definitions

void AnaTempInit(void)
{
    // Analogue interface temperature regulation parameters

    ana_temp_regul.prop_gain = ANA_TR_PROP_GAIN;
    ana_temp_regul.int_gain  = ANA_TR_INT_GAIN;
    ana_temp_regul.diff_gain = ANA_TR_DIF_GAIN;
    ana_temp_regul.adc_gain  = ANA_TR_ADC_GAIN;

    // DAC settings (heater command)

    DA.DADR0           = 0x000;      // set DAC to 0 (no heating)
    DA.DACR.BIT.DAOE0  = 0;          // disable DAC0 output
    DA.DADPR.BIT.DPSEL = 0;          // Format selector. Bits 0-9 are used in DADR0.

    // Start with no regulation by default. (On the Main Program the regulation will start
    // when the configuration property ADC.INTERNAL.VREF_TEMP.REF is initialised.)

    AnaTempRegulStop();

#if (FGC_CLASS_ID != 60)

    // If ADC.INTERNAL.VREF_TEMP.REF property is set, then start regulating (see SetAnaVrefTemp).

    if (PROP_ADC_INTERNAL_VREF_TEMP_REF.n_elements == 1)
    {
        AnaTempRegulStart(*((FP32 *)PROP_ADC_INTERNAL_VREF_TEMP_REF.value));
    }

#else

    // In boot we start regulating with value saved in NVRAM

    if (!isnan(NVRAM_TARGET_TEMP_P))
    {
        AnaTempRegulStart(NVRAM_TARGET_TEMP_P);
    }

#endif

}

void AnaTempRegulStart(FP32 ref_temp)
{
    if (ref_temp == 0.0)     // Turn off regulation & heater
    {
        AnaTempRegulStop();             // That will set ana_temp_regul.on_f  to FALSE;

        DA.DADR0          = 0x000;      // set DAC to 0 (no heating)
        DA.DACR.BIT.DAOE0 = 0;          // Disable DA0 output
    }
    else                        // Turn on regulation
    {
        ana_temp_regul.adc_ref = (INT16U)(ana_temp_regul.adc_gain * ref_temp);

        // If regulation was OFF, reset the PID algorithm and enable DAC0.

        if (ana_temp_regul.on_f == FALSE)
        {
            ana_temp_regul.on_f       = TRUE;

            ana_temp_regul.prev_error = 0;              // reset previous error
            ana_temp_regul.integrator = 0;              // reset integral value

            DA.DACR.BIT.DAOE0         = 1;              // enable DAC0 output
        }

        // Else if regulation was ON, we consider that the DAC was already active and we do not reset the PID parameters.
    }
}

void AnaTempRegulStop(void)
{
    ana_temp_regul.on_f        = FALSE;     // reset regulation flag

    ana_temp_regul.prev_error = 0;         // reset 1st computation flag
    ana_temp_regul.integrator = 0;         // reset integral value
}

void AnaTempRegul(void)
{
    INT32S  act;        // heater actuation (DAC units)
    INT32S  error;      // error (raw ADC units)
    INT32S  derror;     // differential of the error (raw ADC units)

    // Calculate error between reference and measurement in raw ADC units

    // Compute error with reference (calculated when set)

    error = (INT32S)ana_temp_regul.adc_ref - (INT32S)(ANA_VREF_RAW_TEMP());

    // Calculate the derivative of the error

    derror = error - ana_temp_regul.prev_error;

    ana_temp_regul.prev_error = error;

    // PID algorithm: act is calculated in fixed point with a multiplier of 256.

    act = ana_temp_regul.prop_gain * error                                 // P
          + ana_temp_regul.int_gain  * (ana_temp_regul.integrator + error)   // I
          + ana_temp_regul.diff_gain * derror;                               // D

    // Scale actuation in DAC units (10 bit)

    act /= 256;

    // Set DAC with protection against saturation

    if (act < 0)
    {
        DA.DADR0 = 0x000;                        // Heater is OFF
    }
    else if (act > 0x3FF)
    {
        DA.DADR0 = 0x3FF;                        // Heater at full power
    }
    else
    {
        DA.DADR0 = (INT16U)act;                  // Set DAC to control the heater
        ana_temp_regul.integrator += error;      // Integrate the error
    }
}

FP32 AnaTempGetTemp(void)
{
    FP32  temp = 666.6;

    if (ana_temp_regul.adc_gain != 0.0)
    {
        temp = (FP32)ANA_VREF_RAW_TEMP() / ana_temp_regul.adc_gain;
    }

    return (temp);
}

void AnaTempSetPower(INT16U mW)
{
    DA.DADR0 = (INT16U)(((INT32U) mW * 0x3FF) / 2000) & 0x3FF;
}

void AnaTempSetPIDGain(INT16U p_gain, INT16U i_gain, INT16U d_gain)
{
    ana_temp_regul.prop_gain = p_gain;
    ana_temp_regul.int_gain  = i_gain;
    ana_temp_regul.diff_gain = d_gain;
}

BOOLEAN AnaTempGetStatus(void)
{
    return ana_temp_regul.on_f;
}

INT16U AnaTempGetPGain(void)
{
    return ana_temp_regul.prop_gain;
}

INT16U AnaTempGetIGain(void)
{
    return ana_temp_regul.int_gain;
}

INT16U AnaTempGetDGain(void)
{
    return ana_temp_regul.diff_gain;
}

INT32S AnaTempGetIntegrator(void)
{
    return ana_temp_regul.integrator;
}

INT16U AnaTempGetAdcRef(void)
{
    return ana_temp_regul.adc_ref;
}

FP32 AnaTempGetAdcGain(void)
{
    return ana_temp_regul.adc_gain;
}

INT16U AnaTempGetProcessingTime(void)
{
    return ana_temp_regul.proc_time;
}

// EOF
