/*---------------------------------------------------------------------------------------------------------*\
 File:          vector_table.c

 Purpose:       RX610 hardware related stuff, Relocatable Interrupt vectors

 Author:        Daniel Calcoen

 History:
    02 nov 10   doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>           // basic typedefs

#include <trap.h>
#include <interrupt_handlers.h>
#include <os_hardware.h>        // for OSTskCtxSwitch()
#include <debug.h>


typedef void (*fPtr)(void);

#if defined(DEBUG_RUNNING_MAIN_WITHOUT_BOOT)

extern void PowerON_Reset(void);

/*---------------------------------------------------------------------------------------------------------*/
const void   *  HardwareVectors[] __attribute__((section(".fvectors")))  =
    /*---------------------------------------------------------------------------------------------------------*\
      hardware (fixed) vectors of Rx610

        128 bytes region
        vectors from 0 to 31

        will reside and run at 0xffffff80

    \*---------------------------------------------------------------------------------------------------------*/
{
    (fPtr)IsrDummyFix,                          // 0xffffff80  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffff84  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffff88  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffff8C  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffff90  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffff94  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffff98  Reserved

    // 0xFFFFFF9C  ROM code protection
    //        0x00000000 ROM protection activated (method 1)
    //        0x00000001 ROM protection activated (method 2)
    (fPtr)0xFFFFFFFF,   // ROM protection disabled

    // 0xFFFFFFA0  control code (ID protection)
    //             0x45 ID protection activated (authentication method 1)
    //             0x52 ID protection activated (authentication method 2 and 3)
    //             0xFF + all IDs 0xFF, special case for On-Chip Debugger

    // 0xFFFFFFA1  ID_1
    // ..........
    // 0xFFFFFFAF  ID_15
    (fPtr)0xFFFFFFFF,                           // 0xffffffA0
    (fPtr)0xFFFFFFFF,                           // 0xffffffA4
    (fPtr)0xFFFFFFFF,                           // 0xffffffA8
    (fPtr)0xFFFFFFFF,                           // 0xffffffAC

    (fPtr)IsrDummyFix,                          // 0xffffffB0  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffffB4  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffffB8  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffffBC  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffffC0  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffffC4  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffffC8  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffffCC  Reserved
    (fPtr)INT_Excep_SupervisorInst,             // 0xffffffd0  Privileged Instruction Exception(Supervisor Instruction)
    (fPtr)IsrDummyFix,                          // 0xffffffd4  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffffd8  Reserved
    (fPtr)INT_Excep_UndefinedInst,              // 0xffffffdc  Undefined Instruction Exception
    (fPtr)IsrDummyFix,                          // 0xffffffe0  Reserved
    (fPtr)INT_Excep_FloatingPoint,              // 0xffffffe4  Floating Point Exception
    (fPtr)IsrDummyFix,                          // 0xffffffe8  Reserved
    (fPtr)IsrDummyFix,                          // 0xffffffec  Reserved
    (fPtr)IsrDummyFix,                          // 0xfffffff0  Reserved
    (fPtr)IsrDummyFix,                          // 0xfffffff4  Reserved
    (fPtr)INT_NonMaskableInterrupt,             // 0xfffffff8  NMI
    (fPtr)PowerON_Reset                         // 0xfffffffc  RESET
};

#endif

/*---------------------------------------------------------------------------------------------------------*/
const fPtr RelocatableVectors[] __attribute__((section(".rvectors")))  =
    /*---------------------------------------------------------------------------------------------------------*\
      relocatable vectors of Rx610

        1024 bytes region
        vectors from 0 to 255

     search for "OS_TASK_SW_vector" through the project files to know where to replace if you change the interrupt
     of _OSTskCtxSwitch

    \*---------------------------------------------------------------------------------------------------------*/
{
    (fPtr)INT_Excep_BRK,                // 0x0000 int   0  int  0, hardware reserved,
    (fPtr)OSTskCtxSwitch,               // 0x0004 int   1  int  1, pure software, hardware reserved, RTOS context switch, called with the macro OS_TASK_SW()
    (fPtr)IsrDummy,                     // 0x0008 int   2  Reserved
    (fPtr)IsrDummy,                     // 0x000C int   3  Reserved
    (fPtr)IsrDummy,                     // 0x0010 int   4  Reserved
    (fPtr)IsrDummy,                     // 0x0014 int   5  Reserved
    (fPtr)IsrDummy,                     // 0x0018 int   6  Reserved
    (fPtr)IsrDummy,                     // 0x001C int   7  Reserved
    (fPtr)IsrDummy,                     // 0x0020 int   8  Reserved
    (fPtr)IsrDummy,                     // 0x0024 int   9  Reserved
    (fPtr)IsrDummy,                     // 0x0028 int  10  Reserved
    (fPtr)IsrDummy,                     // 0x002C int  11  Reserved
    (fPtr)IsrDummy,                     // 0x0030 int  12  Reserved
    (fPtr)IsrDummy,                     // 0x0034 int  13  Reserved
    (fPtr)IsrDummy,                     // 0x0038 int  14  Reserved
    (fPtr)IsrDummy,                     // 0x003C int  15  Reserved
    (fPtr)INT_Excep_BUSERR,             // 0x0040 int  16  BUSERR
    (fPtr)IsrDummy,                     // 0x0044 int  17  Reserved
    (fPtr)IsrDummy,                     // 0x0048 int  18  Reserved
    (fPtr)IsrDummy,                     // 0x004C int  19  Reserved
    (fPtr)IsrDummy,                     // 0x0050 int  20  Reserved
    (fPtr)INT_Excep_FCU_FCUERR,         // 0x0054 int  21  FCUERR
    (fPtr)IsrDummy,                     // 0x0058 int  22  Reserved
    (fPtr)INT_Excep_FCU_FRDYI,          // 0x005C int  23  FRDYI
    (fPtr)IsrDummy,                     // 0x0060 int  24  Reserved
    (fPtr)IsrDummy,                     // 0x0064 int  25  Reserved
    (fPtr)IsrDummy,                     // 0x0068 int  26  Reserved
    (fPtr)IsrDummy,                     // 0x006C int  27  Reserved
    (fPtr)INT_Excep_CMTU0_CMT0,         // 0x0070 int  28  CMTU0_CMT0
    (fPtr)INT_Excep_CMTU0_CMT1,         // 0x0074 int  29  CMTU0_CMT1
    (fPtr)INT_Excep_CMTU1_CMT2,         // 0x0078 int  30  CMTU1_CMT2
    (fPtr)INT_Excep_CMTU1_CMT3,         // 0x007C int  31  CMTU1_CMT3
    (fPtr)IsrDummy,                     // 0x0080 int  32  Reserved
    (fPtr)IsrDummy,                     // 0x0084 int  33  Reserved
    (fPtr)IsrDummy,                     // 0x0088 int  34  Reserved
    (fPtr)IsrDummy,                     // 0x008C int  35  Reserved
    (fPtr)IsrDummy,                     // 0x0090 int  36  Reserved
    (fPtr)IsrDummy,                     // 0x0094 int  37  Reserved
    (fPtr)IsrDummy,                     // 0x0098 int  38  Reserved
    (fPtr)IsrDummy,                     // 0x009C int  39  Reserved
    (fPtr)IsrDummy,                     // 0x00A0 int  40  Reserved
    (fPtr)IsrDummy,                     // 0x00A4 int  41  Reserved
    (fPtr)IsrDummy,                     // 0x00A8 int  42  Reserved
    (fPtr)IsrDummy,                     // 0x00AC int  43  Reserved
    (fPtr)IsrDummy,                     // 0x00B0 int  44  Reserved
    (fPtr)IsrDummy,                     // 0x00B4 int  45  Reserved
    (fPtr)IsrDummy,                     // 0x00B8 int  46  Reserved
    (fPtr)IsrDummy,                     // 0x00BC int  47  Reserved
    (fPtr)IsrDummy,                     // 0x00C0 int  48  Reserved
    (fPtr)IsrDummy,                     // 0x00C4 int  49  Reserved
    (fPtr)IsrDummy,                     // 0x00C8 int  50  Reserved
    (fPtr)IsrDummy,                     // 0x00CC int  51  Reserved
    (fPtr)IsrDummy,                     // 0x00D0 int  52  Reserved
    (fPtr)IsrDummy,                     // 0x00D4 int  53  Reserved
    (fPtr)IsrDummy,                     // 0x00D8 int  54  Reserved
    (fPtr)IsrDummy,                     // 0x00DC int  55  Reserved
    (fPtr)IsrDummy,                     // 0x00E0 int  56  Reserved
    (fPtr)IsrDummy,                     // 0x00E4 int  57  Reserved
    (fPtr)IsrDummy,                     // 0x00E8 int  58  Reserved
    (fPtr)IsrDummy,                     // 0x00EC int  59  Reserved
    (fPtr)IsrDummy,                     // 0x00F0 int  60  Reserved
    (fPtr)IsrDummy,                     // 0x00F4 int  61  Reserved
    (fPtr)IsrDummy,                     // 0x00F8 int  62  Reserved
    (fPtr)IsrDummy,                     // 0x00FC int  63  Reserved
    (fPtr)INT_Excep_IRQ0,               // 0x0100 int  64  IRQ0
    (fPtr)INT_Excep_IRQ1,               // 0x0104 int  65  IRQ1
    (fPtr)INT_Excep_IRQ2,               // 0x0108 int  66  IRQ2         IER08.IEN2      IPR22   ISELR066        FIP_~MSG_IRQ
    (fPtr)INT_Excep_IRQ3,               // 0x010C int  67  IRQ3         IER08.IEN3      IPR23   ISELR067        FIP_~TIME_IRQ or ETHERNET 50Hz pulse
    (fPtr)INT_Excep_IRQ4,               // 0x0110 int  68  IRQ4             IER08.IEN4  IPR24   ISELR068        MCU_~TICK
    (fPtr)INT_Excep_IRQ5,               // 0x0114 int  69  IRQ5
    (fPtr)INT_Excep_IRQ6,               // 0x0118 int  70  IRQ6
    (fPtr)INT_Excep_IRQ7,               // 0x011C int  71  IRQ7
    (fPtr)INT_Excep_IRQ8,               // 0x0120 int  72  IRQ8
    (fPtr)INT_Excep_IRQ9,               // 0x0124 int  73  IRQ9
    (fPtr)INT_Excep_IRQ10,              // 0x0128 int  74  IRQ10
    (fPtr)INT_Excep_IRQ11,              // 0x012C int  75  IRQ11
    (fPtr)INT_Excep_IRQ12,              // 0x0130 int  76  IRQ12
    (fPtr)INT_Excep_IRQ13,              // 0x0134 int  77  IRQ13
    (fPtr)INT_Excep_IRQ14,              // 0x0138 int  78  IRQ14
    (fPtr)INT_Excep_IRQ15,              // 0x013C int  79  IRQ15
    (fPtr)IsrDummy,                     // 0x0140 int  80  Reserved
    (fPtr)IsrDummy,                     // 0x0144 int  81  Reserved
    (fPtr)IsrDummy,                     // 0x0148 int  82  Reserved
    (fPtr)IsrDummy,                     // 0x014C int  83  Reserved
    (fPtr)IsrDummy,                     // 0x0150 int  84  Reserved
    (fPtr)IsrDummy,                     // 0x0154 int  85  Reserved
    (fPtr)IsrDummy,                     // 0x0158 int  86  Reserved
    (fPtr)IsrDummy,                     // 0x015C int  87  Reserved
    (fPtr)IsrDummy,                     // 0x0160 int  88  Reserved
    (fPtr)IsrDummy,                     // 0x0164 int  89  Reserved
    (fPtr)IsrDummy,                     // 0x0168 int  90  Reserved
    (fPtr)IsrDummy,                     // 0x016C int  91  Reserved
    (fPtr)IsrDummy,                     // 0x0170 int  92  Reserved
    (fPtr)IsrDummy,                     // 0x0174 int  93  Reserved
    (fPtr)IsrDummy,                     // 0x0178 int  94  Reserved
    (fPtr)IsrDummy,                     // 0x017C int  95  Reserved
    (fPtr)INT_Excep_WDT_WOVI,           // 0x0180 int  96  WDT_WOVI
    (fPtr)IsrDummy,                     // 0x0184 int  97  Reserved
    (fPtr)INT_Excep_AD0_ADI0,           // 0x0188 int  98  AD0_ADI0
    (fPtr)INT_Excep_AD1_ADI1,           // 0x018C int  99  AD1_ADI1
    (fPtr)INT_Excep_AD2_ADI2,           // 0x0190 int 100  AD2_ADI2
    (fPtr)INT_Excep_AD3_ADI3,           // 0x0194 int 101  AD3_ADI3
    (fPtr)IsrDummy,                     // 0x0198 int 102  Reserved
    (fPtr)IsrDummy,                     // 0x019C int 103  Reserved
    (fPtr)INT_Excep_TPU0_TGI0A,         // 0x01A0 int 104  TPU0_TGI0A   IER0D.IEN0      PR4C    ISELR104
    (fPtr)INT_Excep_TPU0_TGI0B,         // 0x01A4 int 105  TPU0_TGI0B
    (fPtr)INT_Excep_TPU0_TGI0C,         // 0x01A8 int 106  TPU0_TGI0C
    (fPtr)INT_Excep_TPU0_TGI0D,         // 0x01AC int 107  TPU0_TGI0D
    (fPtr)INT_Excep_TPU0_TCI0V,         // 0x01B0 int 108  TPU0_TCI0V
    (fPtr)IsrDummy,                     // 0x01B4 int 109  Reserved
    (fPtr)IsrDummy,                     // 0x01B8 int 110  Reserved
    (fPtr)INT_Excep_TPU1_TGI1A,         // 0x01BC int 111  TPU1_TGI1A
    (fPtr)INT_Excep_TPU1_TGI1B,         // 0x01C0 int 112  TPU1_TGI1B
    (fPtr)IsrDummy,                     // 0x01C4 int 113  Reserved
    (fPtr)IsrDummy,                     // 0x01C8 int 114  Reserved
    (fPtr)INT_Excep_TPU1_TCI1V,         // 0x01CC int 115  TPU1_TCI1V
    (fPtr)INT_Excep_TPU1_TCI1U,         // 0x01D0 int 116  TPU1_TCI1U
    (fPtr)INT_Excep_TPU2_TGI2A,         // 0x01D4 int 117  TPU2_TGI2A
    (fPtr)INT_Excep_TPU2_TGI2B,         // 0x01D8 int 118  TPU2_TGI2B
    (fPtr)IsrDummy,                     // 0x01DC int 119  Reserved
    (fPtr)INT_Excep_TPU2_TCI2V,         // 0x01E0 int 120  TPU2_TCI2V
    (fPtr)INT_Excep_TPU2_TCI2U,         // 0x01E4 int 121  TPU2_TCI2U
    (fPtr)INT_Excep_TPU3_TGI3A,         // 0x01E8 int 122  TPU3_TGI3A
    (fPtr)INT_Excep_TPU3_TGI3B,         // 0x01EC int 123  TPU3_TGI3B
    (fPtr)INT_Excep_TPU3_TGI3C,         // 0x01F0 int 124  TPU3_TGI3C
    (fPtr)INT_Excep_TPU3_TGI3D,         // 0x01F4 int 125  TPU3_TGI3D
    (fPtr)INT_Excep_TPU3_TCI3V,         // 0x01F8 int 126  TPU3_TCI3V
    (fPtr)INT_Excep_TPU4_TGI4A,         // 0x01FC int 127  TPU4_TGI4A
    (fPtr)INT_Excep_TPU4_TGI4B,         // 0x0200 int 128  TPU4_TGI4B
    (fPtr)IsrDummy,                     // 0x0204 int 129  Reserved
    (fPtr)IsrDummy,                     // 0x0208 int 130  Reserved
    (fPtr)INT_Excep_TPU4_TCI4V,         // 0x020C int 131  TPU4_TCI4V
    (fPtr)INT_Excep_TPU4_TCI4U,         // 0x0210 int 132  TPU4_TCI4U
    (fPtr)INT_Excep_TPU5_TGI5A,         // 0x0214 int 133  TPU5_TGI5A
    (fPtr)INT_Excep_TPU5_TGI5B,         // 0x0218 int 134  TPU5_TGI5B
    (fPtr)IsrDummy,                     // 0x021C int 135  Reserved
    (fPtr)INT_Excep_TPU5_TCI5V,         // 0x0220 int 136  TPU5_TCI5V
    (fPtr)INT_Excep_TPU5_TCI5U,         // 0x0224 int 137  TPU5_TCI5U
    (fPtr)INT_Excep_TPU6_TGI6A,         // 0x0228 int 138  TPU6_TGI6A
    (fPtr)INT_Excep_TPU6_TGI6B,         // 0x022C int 139  TPU6_TGI6B
    (fPtr)INT_Excep_TPU6_TGI6C,         // 0x0230 int 140  TPU6_TGI6C
    (fPtr)INT_Excep_TPU6_TGI6D,         // 0x0234 int 141  TPU6_TGI6D
    (fPtr)INT_Excep_TPU6_TCI6V,         // 0x0238 int 142  TPU6_TCI6V
    (fPtr)IsrDummy,                     // 0x023C int 143  Reserved
    (fPtr)IsrDummy,                     // 0x0240 int 144  Reserved
    (fPtr)INT_Excep_TPU7_TGI7A,         // 0x0244 int 145  TPU7_TGI7A
    (fPtr)INT_Excep_TPU7_TGI7B,         // 0x0248 int 146  TPU7_TGI7B
    (fPtr)IsrDummy,                     // 0x024C int 147  Reserved
    (fPtr)IsrDummy,                     // 0x0250 int 148  Reserved
    (fPtr)INT_Excep_TPU7_TCI7V,         // 0x0254 int 149  TPU7_TCI7V
    (fPtr)INT_Excep_TPU7_TCI7U,         // 0x0258 int 150  TPU7_TCI7U
    (fPtr)INT_Excep_TPU8_TGI8A,         // 0x025C int 151  TPU8_TGI8A
    (fPtr)INT_Excep_TPU8_TGI8B,         // 0x0260 int 152  TPU8_TGI8B
    (fPtr)IsrDummy,                     // 0x0264 int 153  Reserved
    (fPtr)INT_Excep_TPU8_TCI8V,         // 0x0268 int 154  TPU8_TCI8V
    (fPtr)INT_Excep_TPU8_TCI8U,         // 0x026C int 155  TPU8_TCI8U
    (fPtr)INT_Excep_TPU9_TGI9A,         // 0x0270 int 156  TPU9_TGI9A
    (fPtr)INT_Excep_TPU9_TGI9B,         // 0x0274 int 157  TPU9_TGI9B
    (fPtr)INT_Excep_TPU9_TGI9C,         // 0x0278 int 158  TPU9_TGI9C
    (fPtr)INT_Excep_TPU9_TGI9D,         // 0x027C int 159  TPU9_TGI9D
    (fPtr)INT_Excep_TPU9_TCI9V,         // 0x0280 int 160  TPU9_TCI9V
    (fPtr)INT_Excep_TPU10_TGI10A,       // 0x0284 int 161  TPU10_TGI10A
    (fPtr)INT_Excep_TPU10_TGI10B,       // 0x0288 int 162  TPU10_TGI10B
    (fPtr)IsrDummy,                     // 0x028C int 163  Reserved
    (fPtr)IsrDummy,                     // 0x0290 int 164  Reserved
    (fPtr)INT_Excep_TPU10_TCI10V,       // 0x0294 int 165  TPU10_TCI10V
    (fPtr)INT_Excep_TPU10_TCI10U,       // 0x0298 int 166  TPU10_TCI10U
    (fPtr)INT_Excep_TPU11_TGI11A,       // 0x029C int 167  TPU11_TGI11A
    (fPtr)INT_Excep_TPU11_TGI11B,       // 0x02A0 int 168  TPU11_TGI11B
    (fPtr)IsrDummy,                     // 0x02A4 int 169  Reserved
    (fPtr)INT_Excep_TPU11_TCI11V,       // 0x02A8 int 170  TPU11_TCI11V
    (fPtr)INT_Excep_TPU11_TCI11U,       // 0x02AC int 171  TPU11_TCI11U
    (fPtr)IsrDummy,                     // 0x02B0 int 172  Reserved
    (fPtr)IsrDummy,                     // 0x02B4 int 173  Reserved
    (fPtr)INT_Excep_TMR0_CMIA0,         // 0x02B8 int 174  TMR0_CMI0A   IER15.IEN6      IPR68   ISELR174
    (fPtr)INT_Excep_TMR0_CMIB0,         // 0x02BC int 175  TMR0_CMI0B   IER15.IEN7      IPR68   ISELR175
    (fPtr)INT_Excep_TMR0_OVI0,          // 0x02C0 int 176  TMR0_OV0I    IER16.IEN0      IPR68
    (fPtr)INT_Excep_TMR1_CMIA1,         // 0x02C4 int 177  TMR1_CMI1A   IER16.IEN1      IPR69   ISELR177
    (fPtr)INT_Excep_TMR1_CMIB1,         // 0x02C8 int 178  TMR1_CMI1B   IER16.IEN2      IPR69   ISELR178
    (fPtr)INT_Excep_TMR1_OVI1,          // 0x02CC int 179  TMR1_OV1I    IER16.IEN3      IPR69
    (fPtr)INT_Excep_TMR2_CMIA2,         // 0x02D0 int 180  TMR2_CMI2A   IER16.IEN4      IPR6A   ISELR180
    (fPtr)INT_Excep_TMR2_CMIB2,         // 0x02D4 int 181  TMR2_CMI2B   IER16.IEN5      IPR6A   ISELR181
    (fPtr)INT_Excep_TMR2_OVI2,          // 0x02D8 int 182  TMR2_OV2I    IER16.IEN6      IPR6A
    (fPtr)INT_Excep_TMR3_CMIA3,         // 0x02DC int 183  TMR3_CMI3A   IER16.IEN7      IPR6B   ISELR183
    (fPtr)INT_Excep_TMR3_CMIB3,         // 0x02E0 int 184  TMR3_CMI3B   IER17.IEN0      IPR6B   ISELR184
    (fPtr)INT_Excep_TMR3_OVI3,          // 0x02E4 int 185  TMR3_OV3I    IER17.IEN1      IPR6B
    (fPtr)IsrDummy,                     // 0x02E8 int 186  Reserved
    (fPtr)IsrDummy,                     // 0x02EC int 187  Reserved
    (fPtr)IsrDummy,                     // 0x02F0 int 188  Reserved
    (fPtr)IsrDummy,                     // 0x02F4 int 189  Reserved
    (fPtr)IsrDummy,                     // 0x02F8 int 190  Reserved
    (fPtr)IsrDummy,                     // 0x02FC int 191  Reserved
    (fPtr)IsrDummy,                     // 0x0300 int 192  Reserved
    (fPtr)IsrDummy,                     // 0x0304 int 193  Reserved
    (fPtr)IsrDummy,                     // 0x0308 int 194  Reserved
    (fPtr)IsrDummy,                     // 0x030C int 195  Reserved
    (fPtr)IsrDummy,                     // 0x0310 int 196  Reserved
    (fPtr)IsrDummy,                     // 0x0314 int 197  Reserved
    (fPtr)INT_Excep_DMAC_DMTEND0,       // 0x0318 int 198  DMAC_DMTEND0
    (fPtr)INT_Excep_DMAC_DMTEND1,       // 0x031C int 199  DMAC_DMTEND1
    (fPtr)INT_Excep_DMAC_DMTEND2,       // 0x0320 int 200  DMAC_DMTEND2
    (fPtr)INT_Excep_DMAC_DMTEND3,       // 0x0324 int 201  DMAC_DMTEND3
    (fPtr)IsrDummy,                     // 0x0328 int 202  Reserved
    (fPtr)IsrDummy,                     // 0x032C int 203  Reserved
    (fPtr)IsrDummy,                     // 0x0330 int 204  Reserved
    (fPtr)IsrDummy,                     // 0x0334 int 205  Reserved
    (fPtr)IsrDummy,                     // 0x0338 int 206  Reserved
    (fPtr)IsrDummy,                     // 0x033C int 207  Reserved
    (fPtr)IsrDummy,                     // 0x0340 int 208  Reserved
    (fPtr)IsrDummy,                     // 0x0344 int 209  Reserved
    (fPtr)IsrDummy,                     // 0x0348 int 210  Reserved
    (fPtr)IsrDummy,                     // 0x034C int 211  Reserved
    (fPtr)IsrDummy,                     // 0x0350 int 212  Reserved
    (fPtr)IsrDummy,                     // 0x0354 int 213  Reserved
    (fPtr)INT_Excep_SCI0_ERI0,          // 0x0358 int 214  SCI0_ERI0    IER1A.IEN6      IPR80
    (fPtr)INT_Excep_SCI0_RXI0,          // 0x035C int 215  SCI0_RXI0    IER1A.IEN7      IPR80   ISELR215
    (fPtr)INT_Excep_SCI0_TXI0,          // 0x0360 int 216  SCI0_TXI0    IER1B.IEN0      IPR80   ISELR216
    (fPtr)INT_Excep_SCI0_TEI0,          // 0x0364 int 217  SCI0_TEI0    IER1B.IEN1      IPR80
    (fPtr)INT_Excep_SCI1_ERI1,          // 0x0368 int 218  SCI1_ERI1    IER1B.IEN2      IPR81
    (fPtr)INT_Excep_SCI1_RXI1,          // 0x036C int 219  SCI1_RXI1    IER1B.IEN3      IPR81   ISELR219
    (fPtr)INT_Excep_SCI1_TXI1,          // 0x0370 int 220  SCI1_TXI1    IER1B.IEN4      IPR81   ISELR220
    (fPtr)INT_Excep_SCI1_TEI1,          // 0x0374 int 221  SCI1_TEI1    IER1B.IEN5      IPR81
    (fPtr)INT_Excep_SCI2_ERI2,          // 0x0378 int 223  SCI2_ERI2    IER1B.IEN6      IPR82
    (fPtr)INT_Excep_SCI2_RXI2,          // 0x037C int 223  SCI2_RXI2    IER1B.IEN7      IPR82   ISELR223
    (fPtr)INT_Excep_SCI2_TXI2,          // 0x0380 int 224  SCI2_TXI2    IER1C.IEN0      IPR82   ISELR224
    (fPtr)INT_Excep_SCI2_TEI2,          // 0x0384 int 225  SCI2_TEI2    IER1C.IEN1      IPR82
    (fPtr)INT_Excep_SCI3_ERI3,          // 0x0388 int 226  SCI3_ERI3    IER1C.IEN2      IPR83
    (fPtr)INT_Excep_SCI3_RXI3,          // 0x038C int 227  SCI3_RXI3    IER1C.IEN3      IPR83   ISELR227
    (fPtr)INT_Excep_SCI3_TXI3,          // 0x0390 int 228  SCI3_TXI3    IER1C.IEN4      IPR83   ISELR228
    (fPtr)INT_Excep_SCI3_TEI3,          // 0x0394 int 229  SCI3_TEI3    IER1C.IEN5      IPR83
    (fPtr)INT_Excep_SCI4_ERI4,          // 0x0398 int 230  SCI4_ERI4    IER1C.IEN6      IPR84
    (fPtr)INT_Excep_SCI4_RXI4,          // 0x039C int 231  SCI4_RXI4    IER1C.IEN7      IPR84   ISELR231
    (fPtr)INT_Excep_SCI4_TXI4,          // 0x03A0 int 232  SCI4_TXI4    IER1D.IEN0      IPR84   ISELR232
    (fPtr)INT_Excep_SCI4_TEI4,          // 0x03A4 int 233  SCI4_TEI4    IER1D.IEN1      IPR84
    (fPtr)INT_Excep_SCI5_ERI5,          // 0x03A8 int 234  SCI5_ERI5    IER1D.IEN2      IPR85
    (fPtr)INT_Excep_SCI5_RXI5,          // 0x03AC int 235  SCI5_RXI5    IER1D.IEN3      IPR85   ISELR235
    (fPtr)INT_Excep_SCI5_TXI5,          // 0x03B0 int 236  SCI5_TXI5    IER1D.IEN4      IPR85   ISELR236
    (fPtr)INT_Excep_SCI5_TEI5,          // 0x03B4 int 237  SCI5_TEI5    IER1D.IEN5      IPR85
    (fPtr)INT_Excep_SCI6_ERI6,          // 0x03B8 int 238  SCI6_ERI6    IER1D.IEN6      IPR86
    (fPtr)INT_Excep_SCI6_RXI6,          // 0x03BC int 239  SCI6_RXI6    IER1D.IEN7      IPR86   ISELR239
    (fPtr)INT_Excep_SCI6_TXI6,          // 0x03C0 int 240  SCI6_TXI6    IER1B.IEN0      IPR86   ISELR240
    (fPtr)INT_Excep_SCI6_TEI6,          // 0x03C4 int 241  SCI6_TEI6    IER1B.IEN1      IPR86
    (fPtr)IsrDummy,                     // 0x03C8 int 242  Reserved
    (fPtr)IsrDummy,                     // 0x03CC int 243  Reserved
    (fPtr)IsrDummy,                     // 0x03D0 int 244  Reserved
    (fPtr)IsrDummy,                     // 0x03D4 int 245  Reserved
    (fPtr)INT_Excep_RIIC0_EEI0,         // 0x03D8 int 246  RIIC0_EEI0
    (fPtr)INT_Excep_RIIC0_RXI0,         // 0x03DC int 247  RIIC0_RXI0
    (fPtr)INT_Excep_RIIC0_TXI0,         // 0x03E0 int 248  RIIC0_TXI0
    (fPtr)INT_Excep_RIIC0_TEI0,         // 0x03E4 int 249  RIIC0_TEI0
    (fPtr)INT_Excep_RIIC1_EEI1,         // 0x03E8 int 250  RIIC1_EEI1
    (fPtr)INT_Excep_RIIC1_RXI1,         // 0x03EC int 251  RIIC1_RXI1
    (fPtr)INT_Excep_RIIC1_TXI1,         // 0x03F0 int 252  RIIC1_TXI1
    (fPtr)INT_Excep_RIIC1_TEI1,         // 0x03F4 int 253  RIIC1_TEI1
    (fPtr)IsrDummy,                     // 0x03F8 int 254  Reserved
    (fPtr)IsrDummy,                     // 0x03FC int 255  Reserved
};
/*---------------------------------------------------------------------------------------------------------*\
  End of file: vector_table.c
\*---------------------------------------------------------------------------------------------------------*/
