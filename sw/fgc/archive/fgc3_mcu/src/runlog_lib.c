/*---------------------------------------------------------------------------------------------------------*\
  File:     runlog_lib.c

  Purpose:  FGC3 library - Run log functions

  Notes:    This file contains the functions needed to implement the run log used by the FGC3 Boot and
        Main Rx610 programs.  The run log is stored in the NV_RAM memory. The NV_RAM has no limited life.

        The run log record has a fixed length 6-byte structure as follows:

                +0      +1      +2      +3      +4      +5
            +-------+-------+-------+-------+-------+-------+
            |RL SEQ | RL ID |   RL DATA[0]  |   RL DATA[1]  |
            +-------+-------+-------+-------+-------+-------+

        The run log contains 128 elements arranged in a circular buffer.  The input pointer
        is stored in a word following the end of the buffer.  The run log will be exported
        via the WorldFIP to the gateway in the runlog byte in the status variable published
        by every FGC.  This can send 50 bytes/s, so the complete run log will be sent every
        16 seconds.

        The run log IDs are defined in def/src/platforms/fgc3/runlog.xml which the
        parser uses to generate inc/platforms/fgc3/runlog.h  This specifies a
        constant (enum) for each type of record, a label and the number of data bytes (0-4).

        The MRAM is normally read only - to write the MRAM, the ULKMRAM bit must
        be set in the CPU_RESET register.  The bit must be cleared afterward to relock the
        MRAM.

\*---------------------------------------------------------------------------------------------------------*/

#include <runlog_lib.h>

#include <iodefines.h>          // processor specific registers and constants
#include <mcu_dependent.h>      // for NVRAM_UNLOCK(), NVRAM_LOCK()
#include <os_hardware.h>        // for OS_TID_CODE, OS_ISR_MASK
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <shared_memory.h>      // for shared_mem global variable
#include <string.h>
#include <structs_bits_big.h>   // MOTOROLA (big endian) bits, bytes, words, dwords ...

/*---------------------------------------------------------------------------------------------------------*/
void RunlogInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called when the boot starts or by the boot Reset Run Log menu option.

  If the NV_RAM area is not valid or the index is not valid the runlog will be cleared
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR           cpu_sr;
    struct TRunlog   *  runlog_ptr;
    BOOLEAN             clear_all = FALSE;
    INT16U              resets_status;

    OS_ENTER_CRITICAL();    // Disable interrupts, except timers that are level 7

    NVRAM_UNLOCK();

    runlog_ptr = (struct TRunlog *) NVRAM_RL_BUF_32;

    if (runlog_ptr->magic != 0xBEEF)    // if the magic number is not initialised, full init
    {
        clear_all = TRUE;               // set flag to clear reset counters
    }

    // Check that index we will use is within run log buffer
    if ((runlog_ptr->index >= FGC_RUNLOG_N_ELS)         // if pointer is not valid
        || (clear_all == TRUE)
       )
    {
        // clear the run log buffer area in the NVRAM
        memset(&(runlog_ptr->buffer[0]) , 0x00, sizeof(struct fgc_runlog) * FGC_RUNLOG_N_ELS);

        runlog_ptr->index = 0x0000;
        runlog_ptr->magic = 0xBEEF;     // set magic number

        // first element after a clear
        runlog_ptr->buffer[0].id  = FGC_RL_RLRESET; // set RLRESET ID in first element
        //      runlog_ptr->buffer[0].seq = 0;
        //      runlog_ptr->buffer[0].data[] = 0;
    }

    // the resets counters are not clear
    OS_EXIT_CRITICAL();

    if (clear_all == FALSE)
    {
        runlog_ptr->resets.all++;           // Increment RESETS_ALL counter

        resets_status = CPU_RESET_SRC_P;

        if ((resets_status & CPU_RESET_SRC_POWER_MASK16) != 0)
        {
            runlog_ptr->resets.power++;
        }

        if ((resets_status & CPU_RESET_SRC_PROGRAM_MASK16) != 0)
        {
            runlog_ptr->resets.program++;
        }

        if ((resets_status & CPU_RESET_SRC_MANUAL_MASK16) != 0)
        {
            runlog_ptr->resets.manual++;
        }

        if ((resets_status & CPU_RESET_SRC_DONGLE_MASK16) != 0)
        {
            runlog_ptr->resets.dongle++;
        }

        if ((resets_status & CPU_RESET_SRC_FASTWD_MASK16) != 0)
        {
            runlog_ptr->resets.fast_watchdog++;
        }

        if ((resets_status & CPU_RESET_SRC_SLOWWD_MASK16) != 0)
        {
            runlog_ptr->resets.slow_watchdog++;
        }
    }
    else            // Full init
    {
        RunlogClrResets();  // zero reset counters
    }

    RunlogWrite(FGC_RL_RESETS, &(runlog_ptr->resets.all));
    NVRAM_LOCK();

    // Reset NanOS SM fields to prevent spurious Runlog entries in the event of a trap

    OS_TID_CODE = 0x10;
    OS_ISR_MASK = 0x00;
}
/*---------------------------------------------------------------------------------------------------------*/
void RunlogClrResets(void)
/*---------------------------------------------------------------------------------------------------------*\
  All the reset counters will be reset to zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct TRunlog     *    runlog_ptr;


    runlog_ptr = (struct TRunlog *) NVRAM_RL_BUF_32;

    NVRAM_UNLOCK();

    // Zero reset counters
    memset(&(runlog_ptr->resets), 0x00, sizeof(struct TRunlogResetCounters));

    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void RunlogWrite(INT8U id, const void * data)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write a record in the run log.  It adds the record to the NV_RAM run log zone.
  Interrupts are disabled during this function to ensure the run log stays coherent.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR               cpu_sr;
    INT16U                  saved_mode;
    struct TRunlog     *    runlog_ptr;
    const union TUnion32Bits * data_cast;
    INT8U                   sequence;


    runlog_ptr = (struct TRunlog *) NVRAM_RL_BUF_32;

    if (runlog_ptr->magic != 0xBEEF)        // if run log not ready
    {
        return;
    }

    data_cast = (union TUnion32Bits *) data;

    NVRAM_UNLOCK();

    OS_ENTER_CRITICAL();                // Disable interrupts, except timers that are level 7
    saved_mode = CPU_MODE_P;            // save mode

    // Get sequence number from last record and increment
    sequence = runlog_ptr->buffer[runlog_ptr->index].seq;
    sequence++;

    runlog_ptr->index++;

    if (runlog_ptr->index >= FGC_RUNLOG_N_ELS)  // if pointer exceeds buffer zone
    {
        runlog_ptr->index = 0;          // reset runlog_ptr->index to start of buffer
    }

    // add new item to the buffer
    runlog_ptr->buffer[runlog_ptr->index].seq = sequence;
    runlog_ptr->buffer[runlog_ptr->index].id  = id;

    runlog_ptr->buffer[runlog_ptr->index].data[0] = data_cast->word[0];
    runlog_ptr->buffer[runlog_ptr->index].data[1] = data_cast->word[1];

    NVRAM_LOCK();

    CPU_MODE_P = saved_mode;        // restore mode
    OS_EXIT_CRITICAL();             // allow interrupts
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U RunlogRead(INT16U * index, struct fgc_runlog * runlog_entry)
/*---------------------------------------------------------------------------------------------------------*\
  This function will read the specified record from the run log.
  If index is not valid, it will be initialised to the most recently entered record.
  The function returns the address of the previous record.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR           cpu_sr;
    struct TRunlog   *  runlog_ptr;
    INT16U              previous;


    runlog_ptr = (struct TRunlog *) NVRAM_RL_BUF_32;

    // Check that index we will use is within run log buffer
    if (*index >= FGC_RUNLOG_N_ELS)     // if pointer is not valid
    {
        *index = runlog_ptr->index; // initialise index to last stored record
    }

    // Read log
    OS_ENTER_CRITICAL();    // Disable interrupts, except timers that are level 7

    runlog_entry->seq     = runlog_ptr->buffer[*index].seq;
    runlog_entry->id      = runlog_ptr->buffer[*index].id;
    runlog_entry->data[0] = runlog_ptr->buffer[*index].data[0];
    runlog_entry->data[1] = runlog_ptr->buffer[*index].data[1];

    OS_EXIT_CRITICAL();


    // adjust index to get previous record
    previous = *index;
    previous--;

    if (previous == 0)          // if before start of buffer
    {
        previous = FGC_RUNLOG_N_ELS;  // Adjust past the end of buffer (not a valid runlog entry)
    }

    return (previous);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: runlog_lib.c
\*---------------------------------------------------------------------------------------------------------*/
