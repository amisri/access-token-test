/*!
 *  @file     master_clk_pll.h
 *  @defgroup FGC3:MCU
 *  @brief    RT Clock Phase Locked Loop Functions.
 *
 */

#define MASTER_CLK_PLL_GLOBALS      // define global variable

// Includes

#include <master_clk_pll.h>

#include <defconst.h>           // for FGC_PLL_NO_SYNC
#include <led.h>
#include <mcu_dependent.h>      // for NVRAM_UNLOCK(), NVRAM_LOCK()
#include <memmap_mcu.h>
#include <pll.h>                // for pll global variable, PllInit()
#include <shared_memory.h>      // for shared_mem global variable
#include <sleep.h>
#if (FGC_CLASS_ID == 60)        // Boot
#include <boot_dual_port_ram.h> // for dpram global variable
#include <dev.h>                // for dev global variable
#include <fieldbus_boot.h>      // for fieldbus global variable, stat_var
#include <pld.h>                // for pld_info global variable
#else                           // Main
#include <dpcom.h>
#include <fbs.h>                // for fbs global variable
#endif
#include <runlog_lib.h>

// Macros

// Macro arguments are expanded except if with # or ##, so we need 2 level of indirection
#define CLASS_FIELD_STRING(name)        c##name
#define CLASS_FIELD_STRING2(name)       CLASS_FIELD_STRING(name)
#define CLASS_FIELD                     CLASS_FIELD_STRING2(FGC_CLASS_ID)   // c##FGC_CLASS_ID

// Internal function declarations

#if (FGC_CLASS_ID != 60)
/*!
 *   Logs all the signals accessible from SPY
 */
static void MasterClockAnaloguePllSpy(void)
{
    dpcom.mcu.pll.dac                 = pll.dac_ppm;
    dpcom.mcu.pll.e18_error           = pll.eth18.phase_err;
    dpcom.mcu.pll.e19_error           = pll.eth19.phase_err;
    dpcom.mcu.pll.error               = pll.pi_err;
    dpcom.mcu.pll.eth_sync_cal        = pll.eth_sync_cal;
    dpcom.mcu.pll.ext_avg_error       = pll.external.phase_err_average;
    dpcom.mcu.pll.ext_error           = pll.ext.phase_err;
    dpcom.mcu.pll.fip_error           = pll.fip.phase_err;
    dpcom.mcu.pll.filtered_integrator = pll.filtered_integrator_ppm;
    dpcom.mcu.pll.integrator          = pll.integrator_ppm;
    dpcom.mcu.pll.net_avg_error       = pll.network.phase_err_average;
    dpcom.mcu.pll.state               = pll.state;
}
#endif

void MasterClockAnaloguePllInit(void)
{
    pll.enabled = TRUE;

#if (FGC_CLASS_ID == 60)
    // for boot
    stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll = FGC_PLL_NO_SYNC;
#else
    // for main
    fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll = FGC_PLL_NO_SYNC;
#endif

    PllInit(shared_mem.network_card_model == NETWORK_TYPE_FIP, NVRAM_PLL_INTEGRATOR_PPM_P,
            NVRAM_PLL_ETH_SYNC_CAL_P);
    PLL_DACVCXO_INT_P = pll.dacvcxo_int;
    PLL_DACVCXO_FRAC_P = pll.dacvcxo_frac;
}

void MasterClockAnaloguePllIteration(void)
{
    INT32U  int_sync_time;
    INT16U  previous_state;

    pll.ext.sync_time = PLL_EXTSYNCTIME_P;

    if (pll.enabled)
    {
        previous_state = pll.state;

        int_sync_time  = PllCalc(PLL_INTSYNCTIME_P);

        // Log new PLL state

        if (previous_state != pll.state)
        {
            RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));
            RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));
            RunlogWrite(FGC_RL_PLL_STATE, &pll.state);
        }

        // Set the LED NET if the external sync is not available, or there are no network packets

        if (!pll.ext_sync_f || (pll.iter_count && pll.network.sync_count == 0))
        {
            LED_SET(NET_RED);
        }
        else
        {
            // Turn NET_RED on. It will appear as orange (see doc, NET_GREEN is lighted by FPGA)

            LED_RST(NET_RED);
        }

        // don't write/lock it until the algorithm is stabilised
        // signalled by the rst mask

        if (((int_sync_time & PLL_INTSYNCTIME_RST_MASK32) == 0)
#if (FGC_CLASS_ID == 60)        // for boot
            || (dev.fieldbus_id == 127 || DevIsStandalone())   // force pll to run when in local
#else                           // for main
            || FbsIsStandalone()            // force pll to run when in local
#endif
           )
        {
            PLL_INTSYNCTIME_P = int_sync_time;
        }

        PLL_DACVCXO_INT_P  = pll.dacvcxo_int;
        PLL_DACVCXO_FRAC_P = pll.dacvcxo_frac;

#if (FGC_CLASS_ID != 60)
        MasterClockAnaloguePllSpy();
#endif

    }

    if (pll.state == FGC_PLL_LOCKED)
    {
        // update NVRAM
        // Note : We write NVRAM at PLL iteration frequency.
        // The technology of the MRAM used in FGC3 does not have a limited number of write cycles

        NVRAM_UNLOCK();

        NVRAM_PLL_INTEGRATOR_PPM_P  = pll.integrator_ppm;
        NVRAM_PLL_ETH_SYNC_CAL_P    = pll.eth_sync_cal;

        NVRAM_LOCK();
    }


    //-------------------------------------------------------------
#if (FGC_CLASS_ID == 60)
    // boot debugging with spy
    dpram->mcu.pll_debug_data[0] = pll.ext.phase_err;
    dpram->mcu.pll_debug_data[1] = pll.eth18.phase_err;
    dpram->mcu.pll_debug_data[2] = pll.eth19.phase_err;
    dpram->mcu.pll_debug_data[3] = (INT32U) pll.pi_err;
    dpram->mcu.pll_debug_data[4] = pll.network.sync_count;
    dpram->mcu.pll_debug_data[5] = pll.network_windowed.sync_count;
#endif
    //-------------------------------------------------------------
    // update status
#if (FGC_CLASS_ID == 60)        // for boot
    stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll = pll.state;
#else                           // for main
    fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll = pll.state;

    if (fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED)
    {
        if (fbs.time_v.ms_time != 0xFFFF)       // If valid time packet received...
        {
            // FGC3: On FIP, the timing message arrives on ms 0.
            //       On Eth, it arrives on ms 18 and ms 19.
            //       The code below is compatible with both.
            if (fbs.net_type == NETWORK_TYPE_ETHERNET)
            {

                if (fbs.time_v.ms_time > 960)
                {
                    // We merely skip these 2 cases, we don't need to always rewrite UTC.
                }
                else
                {
                    // general case without seconds overflow
                    UTC_SET_MS_P = fbs.time_v.ms_time + 40 - fbs.time_v.ms_time % 20;;
                    UTC_SET_UNIXTIME_P = fbs.time_v.unix_time;
                }
            }
            else if (fbs.net_type == NETWORK_TYPE_FIP)
            {
                UTC_SET_MS_P = fbs.time_v.ms_time + 20;
                UTC_SET_UNIXTIME_P = fbs.time_v.unix_time;

            }

            fbs.time_v.ms_time = 0xFFFF;                                // Cancel this time packet
        }
    }

#endif
}

#if (FGC_CLASS_ID == 60)
BOOLEAN Wait10secForStablePLL(void)
{
    INT16U      timeout_ms = 10000;

    // only worth if the PLD is programmed
    if (!pld_info.ok)
    {
        return (FALSE);
    }

    // wait while PLL_INTSYNCTIME_P is pristine during 1 second
    while (((PLL_INTSYNCTIME_P & PLL_INTSYNCTIME_RST_MASK32) == PLL_INTSYNCTIME_RST_MASK32)
           && (timeout_ms != 0)
          )
    {
        timeout_ms--;
        USLEEP(1000); // 1ms
    }

    return (timeout_ms != 0);
}
#endif

// EOF
