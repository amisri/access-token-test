/*---------------------------------------------------------------------------------------------------------*\
  File:         mcu_panic.c

  Purpose:      FGC3 MCU Software - Panic functions
\*---------------------------------------------------------------------------------------------------------*/

#define MCU_PANIC_GLOBALS

#include <mcu_panic.h>
#include <cc_types.h>
#include <mcu_dependent.h>      // for DISABLE_INTS, LEDS_INIT, LED0_TOGGLE, LED1_TOGGLE
#include <memmap_mcu.h>         // For RGLEDS_
#include <os.h>
#include <os_hardware.h>
#include <definfo.h>

#if (FGC_CLASS_ID != 60)
#include <core.h>               // for CoreDump()
#include <sta.h>
#include <runlog_lib.h>         // for RunlogWrite()
#include <fgc_runlog_entries.h> // for FGC_RL_*
#include <shared_memory.h>
#endif

/*---------------------------------------------------------------------------------------------------------*/
void McuPanic(enum mcu_panic_codes panic_code, INT32U leds0, INT32U leds1)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the MCU panic function. It does not return.
  This function disables interrupts and runs an infinite loop.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;
    INT32U      wait_count;

#if (FGC_CLASS_ID != 60)
    INT32U      crash_code;

    sta.mcu_panic.code = (INT32S)panic_code;
    RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));
    RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));
    RunlogWrite(FGC_RL_MCU_PANIC, &sta.mcu_panic.code);         // Panic code

    if (sta.mcu_panic.code == MCU_PANIC_SW_CRASH)
    {
        crash_code = sta.crash_code;
        RunlogWrite(FGC_RL_MCU_PANIC, &crash_code);             // Crash code
    }

    if (sta.mcu_panic.sp)
    {
        RunlogWrite(FGC_RL_SKSP, &sta.mcu_panic.sp);            // Stack Pointer (for exceptions)
        RunlogWrite(FGC_RL_PKPC, &sta.mcu_panic.pc);            // Program Counter before the exception
    }

    if (OS_TID_CODE < 16)
    {
        RunlogWrite(FGC_RL_TSK_ID, (void *) & (OS_TID_CODE));             // NanOS Task ID
    }

    if (OS_ISR_MASK != 0)
    {
        RunlogWrite(FGC_RL_ISR_ID, (void *) & (OS_ISR_MASK));             // NanOS ISR ID
    }

    CoreDump();                                                 // Core dump: See FGC.DEBUG.CORE_LEN_W/CORE_ADDR/CORE_DATA
#endif

    // --- Stop interrupts and halt TMS320C6727 and M16C62 ---

    DISABLE_INTERRUPTS();       // or OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running
    OS_ENTER_CRITICAL_7();      // Disable ALL interrupts
    CPU_RESET_CTRL_P |= (CPU_RESET_CTRL_C62OFF_MASK16 | CPU_RESET_CTRL_DSP_MASK16);

    // --- Flash front panel LEDs if possible and wait for a reset ---

    for (;;)                                    // Loop while waiting for watchdog
    {
        RGLEDS_P = leds0;                       // Set 1st pattern (long)
        wait_count = 0x00F00000;                // Cannot use USLEEP or MSLEEP because interrupts are disabled

        while (wait_count > 0)
        {
            wait_count--;
        }

        RGLEDS_P = leds1;                       // Set 2nd pattern (short)
        wait_count = 0x00100000;

        while (wait_count > 0)
        {
            wait_count--;
        }
    }

    // Never returns
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mcu_panic.c
\*---------------------------------------------------------------------------------------------------------*/

