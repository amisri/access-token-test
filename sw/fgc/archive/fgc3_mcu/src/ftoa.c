/*---------------------------------------------------------------------------------------------------------*\
  File:     ftoa.c

  Purpose:  float to ascii conversion.
            this function comes from Q.K. printf
            This function for performance improvement. ftoa+fputs is twice faster than fprintf
\*---------------------------------------------------------------------------------------------------------*/

#define SYSCALLS_GLOBALS        // for malloc_mngr[] global variable


#include <ftoa.h>               // for ftoa
#include <cc_types.h>
#include <math.h>

/*----------------------------------------------------------------------------*/
static float Ten(int e)
/*----------------------------------------------------------------------------*/
{
    static float fpow10[]  = {1e0, 1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e16, 1e24, 1e32};
    static float fpow_10[] = {1e0, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8, 1e-16, 1e-24, 1e-32};
    unsigned int        i;
    float       *       arr = fpow10;

    if (e < 0)
    {
        arr = fpow_10;
        e   = -e;
    }

    if (e <= 8)
    {
        return (arr[e]);
    }
    else
    {
        i = e >> 3;

        if (i > 4)
        {
            i = 4;
        }

        e  -= i << 3;

        return (arr[i + 7] * arr[e]);
    }
}


/*---------------------------------------------------------------------------------------------------------*/
void ftoa(char * buf, FP32 fval, INT16U prec, INT16U flags, INT16S width)
/*---------------------------------------------------------------------------------------------------------*\
    Dedicated "fast" float to ascii conversion.
    Formatting example: "%.7E"  -> prec=7, flags=UPPER|ETYPE, width=0 (not set)
                        "%13.5E" -> prec=5, flags=UPPER|ETYPE, width=13
    This code comes from printf written by Q.K.
    It is the duty of the caller to allocate the buffer buf with enough space.
\*---------------------------------------------------------------------------------------------------------*/
{
    static unsigned long pow10[] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 0};
    char dig;
    unsigned short len;
    int str_idx = 0;
    unsigned long val;
    int exp0, expon;
    unsigned long pwr;


    if (isnan(fval))
    {
        buf[str_idx++] = 'N';
        buf[str_idx++] = 'a';
        buf[str_idx++] = 'N';
        width = width - 3;

        if (width < 0)
        {
            width = 0;
        }
    }
    else
    {
        if (fval < 0.0)
        {
            fval = (-fval);
            flags |= NEG;
        }

        expon = 0;

        if (fval < FLOAT_MIN)       // Clip at min limit
        {
            fval = 0.0;
        }
        else if (fval > FLOAT_MAX)  // Clip at max limit
        {
            fval = FLOAT_MAX;
        }

        if (fval != 0.0)
        {
            (void)frexpf(fval, &exp0);

            expon = (exp0 * 3) / 10;    /*ln(2)/ln(10) = 0.30102999..*/
        }

        if ((flags & ETYPE) && (!(flags & FTYPE) || (expon < -4) || (expon >= (int) prec)))
        {
            /* base conversion */

            if (fval != 0.0)
            {
                fval *= Ten(-expon);

                if (fval != 0.0)
                {
                    while (fval >= 10.0)
                    {
                        fval *= 1e-1;
                        ++expon;
                    }

                    while (fval < 1.0)
                    {
                        fval *= 10.0;
                        --expon;
                    }
                }
            }

            /* x = fval * 10 ^ expon; 1 <= fval < 10 */

            if (prec && (flags & FTYPE)) /* for g type: prec = nof significant digits */
            {
                --prec;
            }

            if (prec < F_DIGITS)
            {
                fval += 0.5 * Ten(-(int)prec);
            }

            /* if rounding causes overflow */

            if (fval >= 10.0)
            {
                fval *= 1e-1;
                ++expon;
            }

            /*adjust precision for g type conversion (remove trailing zeros)*/

            if (prec && (flags & FTYPE) && !(flags & ALT))
            {
                if (prec > F_DIGITS)
                {
                    prec = F_DIGITS;
                }

                val = fval * Ten(prec);

                if (val)
                {
                    while (prec && !(val % 10))
                    {
                        val /= 10;
                        --prec;
                    }
                }
                else
                {
                    prec = 0;
                }
            }

            flags &= ~FTYPE;

            /* width of e type conversion */

            width -= (5 + prec); /*minimum: dE+dd*/

            if (prec || (flags & ALT)) /*decimal point*/
            {
                --width;
            }

            if (flags & NEG) /*sign or blank*/
            {
                --width;
            }

            val   = fval;
            fval -= (float) val;
            len   = 1;
        }
        else /* f type conversion */
        {
            /*float = fval * 10 ^ expon; fval is not restricted to 1 <= fval < 10 !!!*/

            /*adjust precision for g type conversion (trailing zeros)*/

            if (prec && (flags & ETYPE) && !(flags & ALT))
            {
                prec -= (expon + 1); /*prec >= expon*/

                if (expon < 0 || fval < 1.0) /*leading 0 is not significant*/
                {
                    ++prec;
                }

                if (prec > F_DIGITS)
                {
                    prec = F_DIGITS;
                }

                val = fval * Ten(prec) + 0.5;

                while (prec && !(val % 10))
                {
                    val /= 10;
                    --prec;
                }
            }

            flags &= ~ETYPE;

            if ((int)(expon - F_DIGITS) >= -(int)prec)
            {
                fval += 0.5 * Ten(expon - F_DIGITS);
            }
            else
            {
                fval += 0.5 * Ten(-(int)prec);
            }

            /* width of type f conversion */

            if (expon >= 0) /* (expon + 1) digits before dec pt */
            {
                if (expon > F_DIGITS)
                {
                    expon -= F_DIGITS;
                    val    = fval * Ten(-expon);
                    fval   = 0.0;
                }
                else
                {
                    val   = fval;
                    fval -= (float) val;
                    expon = 0;
                }
            }
            else
            {
                val   = 0;
                expon = 0;
            }

            {
                unsigned long pw;

                len = 1;

                while ((pw = pow10[len]) && val >= pw) { ++len; }
            }

            width -= (prec + len + expon);

            if (prec || (flags & ALT)) /*decimal point*/
            {
                --width;
            }

            if (flags & NEG) /*sign or blank*/
            {
                --width;
            }

        }

        if (width < 0)
        {
            width = 0;
        }

        /* pad left */

        if (!(flags & LEFT))
        {
            if (!(flags & ZEROPAD)) /* pad with spaces before sign */
            {
                while (width)
                {
                    buf[str_idx++] = ' ';
                    --width;
                }
            }
        }

        /* !(flags & LEFT) && !(flags & ZEROPAD) => (width == 0) */

        /* sign */

        if (flags & SIGNED)
        {
            if (flags & BLANK)
            {
                buf[str_idx++] = '-';
            }
            else
            {
                buf[str_idx++] = '+';
            }
        }
        else
        {
            if (flags & BLANK)
            {
                buf[str_idx++] = ' ';
            }
        }

        if (!(flags & LEFT)) /*pad with zeros after sign (if width still > 0)*/
        {
            while (width)
            {
                buf[str_idx++] = '0';
                --width;
            }
        }

        /* float conversion */

        /* print all digits before decimal point */

        do
        {
            --len;
            dig = '0';
            pwr = pow10[len];

            while (val >= pwr)
            {
                val -= pwr; ++dig;
            }

            buf[str_idx++] = dig;

        }
        while (len);

        if (flags & FTYPE) /* add zeros before decimal point */
        {
            while (expon > 0)
            {
                buf[str_idx++] = '0'; --expon;
            }
        }

        if (prec || (flags & ALT))
        {
            buf[str_idx++] = '.';

            if (prec > 0)
            {
                if (prec > F_DIGITS)
                {
                    len = F_DIGITS;
                }
                else
                {
                    len = prec;
                }

                prec -= len;

                if (flags & FTYPE)
                {
                    val = fval * Ten(len - expon); /*10^(-expon) --> 1 <= fval < 10 */
                }
                else
                {
                    val = fval * Ten(len);
                }

                /*out int val */

                do
                {
                    --len;
                    dig = '0';
                    pwr = pow10[len];

                    while (val >= pwr)
                    {
                        val -= pwr; ++dig;
                    }

                    buf[str_idx++] = dig;

                }
                while (len);

                while (prec)
                {
                    buf[str_idx++] = '0';
                    --prec;
                }
            } /* if prec > 0 */
        }

        if (flags & ETYPE) /* exponent */
        {
            if (flags & UPPER)
            {
                buf[str_idx++] = 'E';
            }
            else
            {
                buf[str_idx++] = 'e';
            }

            if (expon < 0)
            {
                expon = (-expon);
                buf[str_idx++] = '-';
            }
            else
            {
                buf[str_idx++] = '+';
            }

            buf[str_idx++] = expon / 10 + '0';
            buf[str_idx++] = expon % 10 + '0';
        }
    }

    /* pad right */

    while (width)
    {
        buf[str_idx++] = ' ';
        --width;
    }

    buf[str_idx] = '\0';
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: ftoa.c
\*---------------------------------------------------------------------------------------------------------*/

