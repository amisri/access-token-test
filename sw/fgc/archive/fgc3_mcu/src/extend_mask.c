/*---------------------------------------------------------------------------------------------------------*\
  File:         extend_mask.c

  Purpose:      FGC Software - An extended mask feature for FGC3

  Notes:        An utility feature which purpose is to provide a bit mask data structure with up to 256 bits.
                For now it is only used by the publication task.
\*---------------------------------------------------------------------------------------------------------*/


#define EXTEND_MASK_GLOBALS

#include <extend_mask.h>
#include <cc_types.h>
#include <memmap_mcu.h>         // for CPU_LSSB_P
#include <macros.h>             // for Set, Clr, Test macros
#include <mcu_dependent.h>      // for MCU_FF1()
#include <os_hardware.h>        // for OS_ENTER_CRITICAL/OS_EXIT_CRITICAL

/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN ExtendMaskScan(struct extended_mask_gen * extend_mask_ptr, INT8U * idx_ptr)
/*---------------------------------------------------------------------------------------------------------*\
  This function will scan a bit index to find the next set bit. At least one bit must be set somewhere
  or the function will return FALSE.

  Argument idx_ptr is used as input and output:
      - As an input it indicates from which index to start the search. If a set bit if found after that
        index, it is returned. Else the search is done starting from bit 0.
      - As an output this is the index of the first set bit that has been found, providing the function
        returned TRUE.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U              mask_idx = *idx_ptr >> 4;
    INT16U              bit_idx  = *idx_ptr & 0xF;

    // If next set bit is not in the same mask_array element
    if (!Test(extend_mask_ptr->mask_array[mask_idx], IDX_MASK16[bit_idx]))
    {
        mask_idx++;

        // And, if next set bit is not in the next mask_arrays either
        if (!Test(extend_mask_ptr->master, IDX_MASK16[mask_idx]))
        {
            mask_idx = 0;   // Then, start the search from bit 0 in lowest mask_array element
        }

        // Find first bit set. Answer in mask_idx.
        MCU_FF1(extend_mask_ptr->master & IDX_MASK16[mask_idx], mask_idx);
        bit_idx = 0;
    }

    if (mask_idx >= 0x10)               // CPU_LSSB_F returns 0x1F in case it is passed 0 as input (no bit set)
    {
        return FALSE;                           // Return FALSE: No set bit found
    }

    // Find first bit set. Answer in bit_idx.
    MCU_FF1(extend_mask_ptr->mask_array[mask_idx] & IDX_MASK16[bit_idx], bit_idx);

    if (bit_idx >= 0x10)                // CPU_LSSB_F returns 0x1F in case it is passed 0 as input (no bit set)
    {
        return FALSE;                           // Return FALSE: No set bit found
    }

    *idx_ptr = (mask_idx << 4) | bit_idx;       // Write the index of the found bit in *idx_ptr
    return TRUE;                                // Return TRUE: Found a set bit
}
/*--------------------------------------------------------------------------------------------------------*/
void ExtendMaskSetBit(struct extended_mask_gen * extend_mask_ptr, INT8U idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set the index bit in the index array and the corresponding bit in the
  master mask index
\*---------------------------------------------------------------------------------------------------------*/
{
    extend_mask_ptr->mask_array[idx >> 4] |= MASK16[idx & 0xF];         // Set bit in index array
    extend_mask_ptr->master               |= MASK16[idx >> 4];          // Set corresponding bit in top index
}
/*---------------------------------------------------------------------------------------------------------*/
void ExtendMaskClrBit(struct extended_mask_gen * extend_mask_ptr, INT8U idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will clear the index bit in the mask array and if required the corresponding bit in the
  master mask index
\*---------------------------------------------------------------------------------------------------------*/
{
    extend_mask_ptr->mask_array[idx >> 4] &= ~MASK16[idx & 0xF];        // Clear bit in mask array

    if (!extend_mask_ptr->mask_array[idx >> 4])                         // If word in index array is now zero
    {
        extend_mask_ptr->master &= ~MASK16[idx >> 4];                       // Clear corresponding bit in master mask
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: extend_mask.c
\*---------------------------------------------------------------------------------------------------------*/
