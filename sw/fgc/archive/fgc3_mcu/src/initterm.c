/*---------------------------------------------------------------------------------------------------------*\
  File:         initterm.c

  Purpose:      FGC Library - function to initialise an ANSI/VT100 terminal

  Author:       Quentin.King@cern.ch,

  Notes:        This file contains the function to reset an ANSI/VT100 terminal.
\*---------------------------------------------------------------------------------------------------------*/

#include <initterm.h>
#include <stdio.h>          // for fputs(), fputc()
#include <term.h>           // for TERM_RESET

/*---------------------------------------------------------------------------------------------------------*/
void InitTerm(FILE * file)
/*---------------------------------------------------------------------------------------------------------*\
  When using a USB-RS232 adapter, there appears to sometimes be a problem with lost characters after a
  reset of the terminal emulation program.  This is compensated by sending a large number of spaces which
  can be harmlessly ignored.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U ii;

    fputs(TERM_RESET, file);                       // Reset terminal

    for (ii = 0 ; ii < 32; ii++)                // Send spaces to allow terminal to reset
    {
        fputc(' ', file);
    }

    fputc('\r', file);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: initterm.c
\*---------------------------------------------------------------------------------------------------------*/

