/*!
 *  @file     hardware_setup.c
 *  @defgroup FGC3:MCU
 *  @brief    RX610 hardware related stuff
 *
 */

// Includes

#include <hardware_setup.h>
#include <debug.h>

// External function definitions

void HardwareSetup(void)
{
    /*
     * this is empty for the MainProg, already done by the BootProg,
     * unless DEBUG_RUNNING_MAIN_WITHOUT_BOOT is defined in debug.h
     */
#if defined(DEBUG_RUNNING_MAIN_WITHOUT_BOOT)

    // when power up it is in User boot mode
    // MD1 pin = 1
    // MD0 pin = 0
    // and the initial values of SYSCR0 - system control register 0 are
    //      syscr0.b0       ROME = 1                on chip ROM enabled
    //      syscr0.b1       EXBE = 0                external bus disabled


    // for On-chip ROM enabled extended mode
    // SYSCR0 - system control register 0 - 0008 0006h
    //      syscr0.b0       ROME = 1                on chip ROM enabled
    //      syscr0.b1       EXBE = 1                external bus enabled

    // When writing a value to the ROME or EXBE bit, write 5Ah to the KEY[7:0] bits simultaneously
    // If SYSCR0 is modified with a KEY[7:0] value other than 5Ah, the ROME and EXBE values remain unchanged

    __asm__ volatile
    (
        "mov.l   #00080006h, r1           \n\t"
        "mov     #5A03h, r2               \n\t"  // 0101 1010 0000 0011
        "mov.w   r2, [r1]                 \n\t"
        // setup PSW - Processor status word
        "mvtc    #00000000h, psw          \n\t"  // Ubit b17 clear (use ISP) & Ibit b16 clear (Interrupt disable)
    );
#endif

#if defined(DEBUG_RUNNING_MAIN_WITHOUT_BOOT) || (FGC_CLASS_ID == 60)

    /*---------------------------------------------------------------------------------------------------------*\
      set the internal peripherals of RX610
    \*---------------------------------------------------------------------------------------------------------*/
    //  Clock Description              Frequency
    //  ----------------------------------------
    //  Main Clock Frequency.............12.5MHz
    //  Internal Clock Frequency..........100MHz    Iphi
    //  Peripheral Clock Frequency.........50MHz    Pphi
    //  External Bus Clock Frequency.......25MHz    Bphi



    // System clock control register SCKCR 0202 0200h 0008 0020h 32

    // ICK[3:0] System Clock (ICLK) Select                  [b27,b26,b25,b24]
    // PSTOP1 BCLK Output Stop                              b23
    // BCK[3:0] External Bus Clock (BCLK) Select            [b19,b18,b17,b16]
    // PCK[3:0] Peripheral Module Clock (PCLK) Select       [b11,b10,b9,b8]

    // MCU clocks, SCKCR setting
    SYSTEM.SCKCR.LONG = 0x00020100;     // [00]x8 Iphi=100MHz, [02]x2 Bphi=25MHz, [01]x4 Pphi=50MHz

    //  SYSTEM.MSTPCRA.LONG =;      // after reset:67FFFFFFh        Module stop control register A  80010h 32
    SYSTEM.MSTPCRA.BIT.MSTPA5 = 0;      // Take 8-Bit Timer 1/0 (Unit 0) Module out of Module Stop Mode
    SYSTEM.MSTPCRA.BIT.MSTPA4 = 0;      // Take 8-Bit Timer 3/2 (Unit 1) Module out of Module Stop Mode
    SYSTEM.MSTPCRA.BIT.MSTPA13 = 0;     // Take 16-Bit Timer Pulse Unit (TPU) (Unit 0) out of Module Stop
    SYSTEM.MSTPCRA.BIT.MSTPA19 = 0;     // Take D/A Converter Module out of Module Stop Mode
    SYSTEM.MSTPCRA.BIT.MSTPA22 = 0;     // Take A/D Converter (Unit 1) Module out of Module Stop Mode
    SYSTEM.MSTPCRA.BIT.MSTPA23 = 0;     // Take A/D Converter (Unit 0) Module out of Module Stop Mode
    /*
        b31 ACSE All-Module Clock Stop Mode Enable
            0: All-module clock stop mode is disabled
            1: All-module clock stop mode is enabled
        b30,
        b29 Reserved These bits are always read as 1. The write value should always be 1
        b28 MSTPA28 DMA Controller Module Stop
    0: DMAC module stop state is cancelled
            1: Transition to the DMAC module stop state is made
        b27 MSTPA27 Data Transfer Controller Module Stop
    0: DTC module stop state is cancelled
            1: Transition to the DTC module stop state is made
        b26,
        b25,
        b24 Reserved These bits are always read as 1. The write value should always be 1
        b23 MSTPA23 A/D Converter (Unit 0) Module Stop
    0: AD0 module stop state is cancelled
            1: Transition to the AD0 module stop state is made
        b22 MSTPA22 A/D Converter (Unit 1) Module Stop
    0: AD1 module stop state is cancelled
            1: Transition to the AD1 module stop state is made
        b21 MSTPA21 A/D Converter (Unit 2) Module Stop
    0: AD2 module stop state is cancelled
            1: Transition to the AD2 module stop state is made
        b20 MSTPA20 A/D Converter (Unit 3) Module Stop
    0: AD3 module stop state is cancelled
            1: Transition to the AD3 module stop state is made
        b19 MSTPA19 D/A Converter Module Stop
    0: DA module stop state is cancelled
            1: Transition to the DA module stop state is made
        b18,
        b17,
        b16 Reserved These bits are always read as 1. The write value should always be 1
        b15 MSTPA15 Compare Match Timer 0 (Unit 0) Module Stop
    0: CMT unit 0 (CMT0, CMT1) module stop state is cancelled
            1: Transition to the CMT unit 0 (CMT0, CMT1) module stop state is made
        b14 MSTPA14 Compare Match Timer 1 (Unit 1) Module Stop
    0: CMT unit 1 (CMT2, CMT3) module stop state is cancelled
            1: Transition to the CMT unit 1 (CMT2, CMT3) module stop state is made
        b13 MSTPA13 16-Bit Timer Pulse Unit 0 (Unit 0) Module Stop
    0: TPU unit 0 (TPU0 to TPU5) module stop state is cancelled
            1: Transition to the TPU unit 0 (TPU0 to TPU5) module stop state is made
        b12 MSTPA12 16-Bit Timer Pulse Unit 1 (Unit 1) Module Stop
    0: TPU unit 1 (TPU6 to TPU11) module stop state is cancelled
            1: Transition to the TPU unit 1 (TPU6 to TPU11) module stop state is made
        b11 MSTPA11 Programmable Pulse Generator 0 (Unit 0) Module Stop
    0: PPG0 module stop state is cancelled
            1: Transition to the PPG0 module stop state is made
        b10 MSTPA10 Programmable Pulse Generator 1 (Unit 1) Module Stop
    0: PPG1 module stop state is cancelled
            1: Transition to the PPG1 module stop state is made
        b9,
        b8,
        b7,
        b6 Reserved These bits are always read as 1. The write value should always be 1
        b5 MSTPA5 8-Bit Timer 1/0 (Unit 0) Module Stop
    0: TMR1/TMR0 module stop state is cancelled
            1: Transition to the TMR1/TMR0 module stop state is made
        b4 MSTPA4 8-Bit Timer 3/2 (Unit 1) Module Stop
    0: TMR3/TMR2 module stop state is cancelled
            1: Transition to the TMR3/TMR2 module stop state is made
        b3,
        b2,
        b1,
        b0 Reserved These bits are always read as 1. The write value should always be 1
    */


    //  SYSTEM.MSTPCRB.LONG =;      // after reset:FFFFFFFFh        Module stop control register B  80014h 32
    // UART0 - Terminal - connected to the FTDI serial/USB with the connector in the front panel
    SYSTEM.MSTPCRB.BIT.MSTPB31 = 0;     // Take SCI0 out of Module Stop Mode
    // UART1 - routed to the PLD  (communication with SCOPE ?)
    SYSTEM.MSTPCRB.BIT.MSTPB30 = 0;     // Take SCI1 out of Module Stop Mode
    // UART2 - routed to the PLD  (communication with POWER CONVERTER ?)
    SYSTEM.MSTPCRB.BIT.MSTPB29 = 0;     // Take SCI2 out of Module Stop Mode
    // UART3 - routed to the PLD
    SYSTEM.MSTPCRB.BIT.MSTPB28 = 0;     // Take SCI3 out of Module Stop Mode
    // UART4 - not used
    // UART4 - communication with the Tester
    // in Boot mode
    // P0.5/RxD4 Input Used in boot mode to receive data via SCI4 (for host communications)
    // P0.4/TxD4 Output Used in boot mode to transmit data from SCI4 (for host communications)
    //  SYSTEM.MSTPCRB.BIT.MSTPB27 = 0;     // Take SCI4 out of Module Stop Mode
    // UART5 - not used
    //  SYSTEM.MSTPCRB.BIT.MSTPB26 = 0;     // Take SCI5 out of Module Stop Mode
    // UART6 - routed to the PLD - M16C62 development port (M16C62-UART1)
    //          when M16C62 is in NORMAL (not STAND_ALONE) its UART1 (M16C62 development uart) is directed to Rx610 UART6
    SYSTEM.MSTPCRB.BIT.MSTPB25 = 0;     // Take SCI6 out of Module Stop Mode

    SYSTEM.MSTPCRB.BIT.MSTPB23 = 0;     // Take CRC  out of Module Stop Mode
    /*
        b31 MSTPB31 Serial Communication Interface 0 Module Stop
    0: SCI0 module stop state is cancelled
            1: Transition to the SCI0 module stop state is made
        b30 MSTPB30 Serial Communication Interface 1 Module Stop
    0: SCI1 module stop state is cancelled
            1: Transition to the SCI1 module stop state is made
        b29 MSTPB29 Serial Communication Interface 2 Module Stop
    0: SCI2 module stop state is cancelled
            1: Transition to the SCI2 module stop state is made
        b28 MSTPB28 Serial Communication Interface 3 Module Stop
    0: SCI3 module stop state is cancelled
            1: Transition to the SCI3 module stop state is made
        b27 MSTPB27 Serial Communication Interface 4 Module Stop
    0: SCI4 module stop state is cancelled
            1: Transition to the SCI4 module stop state is made
        b26 MSTPB26 Serial Communication Interface 5 Module Stop
    0: SCI5 module stop state is cancelled
            1: Transition to the SCI5 module stop state is made
        b25 MSTPB25 Serial Communication Interface 6 Module Stop
    0: SCI6 module stop state is cancelled
            1: Transition to the SCI6 module stop state is made
        b24 Reserved These bits are always read as 1. The write value should always be 1
        b23 MSTPB23 CRC Calculator Module Stop
    0: CRC module stop state is cancelled
            1: Transition to the CRC module stop state is made
        b22 Reserved This bit is always read as 1. The write value should always be 1
        b21 MSTPB21 I2C Bus Interface 0 (Unit 0) Module Stop
    0: RIIC0 module stop state is cancelled
            1: Transition to the RIIC0 module stop state is made
        b20 MSTPB20 I2C Bus Interface 1 (Unit 1) Module Stop
    0: RIIC1 module stop state is cancelled
            1: Transition to the RIIC1 module stop state is made
        b19,
        b18,
        b17,
        b16,
        b15,
        b14,
        b13,
        b12,
        b11,
        b10,
        b9,
        b8,
        b7,
        b6,
        b5,
        b4,
        b3,
        b2,
        b0 Reserved These bits are always read as 1. The write value should always be 1
    */


    //  SYSTEM.MSTPCRC.LONG =;      // after reset:FFFF0000h        Module stop control register C  80018h 32
    /*
        b31,
        b30,
        b29,
        b28,
        b27,
        b26,
        b25,
        b24,
        b23,
        b22,
        b21,
        b20,
        b19,
        b18,
        b17,
        b16 Reserved These bits are always read as 1. The write value should always be 1

        b15,
        b14,
        b13,
        b12,
        b11,
        b10,
        b9,
        b8,
        b7,
        b6,
        b5,
        b4,
        b3,
        b2 Reserved These bits are always read as 0. The write value should always be 0
        b1 MSTPC1* RAM1 Module Stop Target module: RAM1 (0001 0000h to 0001 FFFFh)
            0: RAM1 run
            1: RAM1 stop
        b0 MSTPC0* RAM0 Module Stop Target module: RAM0 (0000 0000h to 0000 FFFFh)
            0: RAM0 run
            1: RAM0 stop
    */



    /*

    0008 3012h      BSC CS1 mode register                   CS1MOD          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 3014h      BSC CS1 wait control register 1         CS1WCNT1        32 32 1 to 2 BCLK       Value after Reset 0707 0707h
    0008 3018h      BSC CS1 wait control register 2         CS1WCNT2        32 32 1 to 2 BCLK       Value after Reset 0000 0007h
    0008 3022h      BSC CS2 mode register                   CS2MOD          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 3024h      BSC CS2 wait control register 1         CS2WCNT1        32 32 1 to 2 BCLK       Value after Reset 0707 0707h
    0008 3028h      BSC CS2 wait control register 2         CS2WCNT2        32 32 1 to 2 BCLK       Value after Reset 0000 0007h
    0008 3032h      BSC CS3 mode register                   CS3MOD          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 3034h      BSC CS3 wait control register 1         CS3WCNT1        32 32 1 to 2 BCLK       Value after Reset 0707 0707h
    0008 3038h      BSC CS3 wait control register 2         CS3WCNT2        32 32 1 to 2 BCLK       Value after Reset 0000 0007h
    0008 3042h      BSC CS4 mode register                   CS4MOD          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 3044h      BSC CS4 wait control register 1         CS4WCNT1        32 32 1 to 2 BCLK       Value after Reset 0707 0707h
    0008 3048h      BSC CS4 wait control register 2         CS4WCNT2        32 32 1 to 2 BCLK       Value after Reset 0000 0007h

    0008 3812h      BSC CS1 control register                CS1CNT          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 381Ah      BSC CS1 recovery cycle register         CS1REC          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 3822h      BSC CS2 control register                CS2CNT          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 382Ah      BSC CS2 recovery cycle register         CS2REC          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 3832h      BSC CS3 control register                CS3CNT          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 383Ah      BSC CS3 recovery cycle register         CS3REC          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 3842h      BSC CS4 control register                CS4CNT          16 16 1 to 2 BCLK       Value after Reset 0000h
    0008 384Ah      BSC CS4 recovery cycle register         CS4REC          16 16 1 to 2 BCLK       Value after Reset 0000h




    BSC.BERCLR.BIT.STSCLR

    BSC.BEREN.BIT.IGAEA
    BSC.BEREN.BIT.TDEN

    BSC.BERIE.BIT.CPEN



    */
    // 0008 C100h   Port Function Control Register0 (PFCR0)         Value after reset: 00h

    //  IOPORT.PFCR0.BIT.CS7E = 1;
    //  IOPORT.PFCR0.BIT.CS6E = 1;
    //  IOPORT.PFCR0.BIT.CS5E = 1;
    IOPORT.PFCR0.BIT.CS4E = 1;  // enable CS4
    IOPORT.PFCR0.BIT.CS3E = 1;  // enable CS3
    IOPORT.PFCR0.BIT.CS2E = 1;  // enable CS2
    IOPORT.PFCR0.BIT.CS1E = 1;  // enable CS1
    //  IOPORT.PFCR0.BIT.CS0E = 1;

    // 0008 C101h   Port Function Control Register 1 (PFCR1)        Value after reset: 00h

    IOPORT.PFCR1.BIT.CS4S_10 = 01;      // CS4#-B is output from P6.4
    //  IOPORT.PFCR1.BIT.CS5S_10 =
    //  IOPORT.PFCR1.BIT.CS6S_10 =
    //  IOPORT.PFCR1.BIT.CS7S_10 =

    // 0008 C102h   Port Function Control Register 2 (PFCR2)        Value after reset: 00h
    IOPORT.PFCR2.BIT.CS3S = 0;  // CS3#-A is output from P6.3
    IOPORT.PFCR2.BIT.CS2S = 0;  // CS2#-A is output from P6.2

    // 0008 C103h   Port Function Control Register 3 (PFCR3)        Value after reset: 00h
    IOPORT.PFCR3.BIT.A16E = 1;
    IOPORT.PFCR3.BIT.A17E = 1;
    IOPORT.PFCR3.BIT.A18E = 1;
    IOPORT.PFCR3.BIT.A19E = 1;
    IOPORT.PFCR3.BIT.A20E = 0;
    IOPORT.PFCR3.BIT.A21E = 0;
    IOPORT.PFCR3.BIT.A22E = 0;
    IOPORT.PFCR3.BIT.A23E = 0;

    // 0008 C104h   Port Function Control Register 4 (PFCR4)        Value after reset: 00h
    IOPORT.PFCR4.BIT.A8E = 1;
    IOPORT.PFCR4.BIT.A9E = 1;
    IOPORT.PFCR4.BIT.A10E = 1;
    IOPORT.PFCR4.BIT.A11E = 1;
    IOPORT.PFCR4.BIT.A12E = 1;
    IOPORT.PFCR4.BIT.A13E = 1;
    IOPORT.PFCR4.BIT.A14E = 1;
    IOPORT.PFCR4.BIT.A15E = 1;


    // 0008 C105h   Port Function Control Register 5 (PFCR5)        Value after reset: 00h
    //  IOPORT.PFCR5.BIT.TCLKS = 1;

    // 1: PE7 to PE0 are designated as D15 to D8 pins (function as part of the external data bus)
    IOPORT.PFCR5.BIT.DHE = 1;
    IOPORT.PFCR5.BIT.WR1BC1E = 1;       // 1: P51 is designated as the WR#1 or BC1# output pin

    /*--------------------------*\
    8C029h - P9 Data register

    P9.7 - MCU_FIP_~RESET       output
    P9.6 - FPGA_CCLK            output
    P9.5 - FPGA_DIN             output
    P9.4 - FPGA_M2              output
    P9.3 - NET_TYPE3            input
    P9.2 - NET_TYPE2            input
    P9.1 - NET_TYPE1            input
    P9.0 - NET_TYPE0            input
    \*--------------------------*/

    // ToDo: there is no pull-up capability in Port9
    //  pu23        =   0x01;       // P9.3/2/1/0 needs a pull-up so Pull-up P9.0 to P9.3

    P9.DDR.BIT.B7 = 1;  // P9.7 as output, MCU_FIP_~RESET
    P9.DR.BIT.B7  = 0;  // set to 0, FIP in reset

    P9.DR.BIT.B6 = 0;   // P9.6, FPGA_CCLK
    P9.DR.BIT.B5 = 0;   // P9.5, FPGA_DIN
    P9.DR.BIT.B4 = 0;   // [M2 M1 M0]=011 --> FPGA boot from internal SPI

    // leave them as input for safety
    //  P9.DDR.BIT.B6 = 1;  // P9.6 as output, FPGA_CCLK
    //  P9.DDR.BIT.B5 = 1;  // P9.5 as output, FPGA_DIN
    //  P9.DDR.BIT.B4 = 1;  // P9.4 as output, FPGA_M2

    P9.ICR.BIT.B3 = 1;  // enable the input buffer
    P9.DDR.BIT.B3 = 0;  // P9.3 as input, NET_TYPE3

    P9.ICR.BIT.B2 = 1;  // enable the input buffer
    P9.DDR.BIT.B2 = 0;  // P9.2 as input, NET_TYPE2

    P9.ICR.BIT.B1 = 1;  // enable the input buffer
    P9.DDR.BIT.B1 = 0;  // P9.1 as input, NET_TYPE1

    P9.ICR.BIT.B0 = 1;  // enable the input buffer
    P9.DDR.BIT.B0 = 0;  // P9.0 as input, NET_TYPE0


    // ============== CS1 NETWORK (FIP / LAN9215) ================================================================

    if ((P9.PORT.BYTE & 0x0F) == 0)    // FIP
    {
        // CS1 - P6.1 - MCU_~FIPCS
        // 0008 3802h - CS1 Control Register

        BSC.CS1CNT.BIT.EXENB = 1;       // Operation is enabled
        BSC.CS1CNT.BIT.BSIZE_10 = 2;    // An 8-bit bus space is selected
        BSC.CS1CNT.BIT.EMODE = 0;       // Endian of area is the same as the endian of operating mode

        // CS1 Recovery Cycle Register
        BSC.CS1REC.BIT.RRCV_30 = 0;     // no recovery cycles is inserted
        BSC.CS1REC.BIT.WRCV_30 = 0;     // no recovery cycles is inserted

        // CS1 Mode Register
        //BSC.CS1MOD.BIT.WRMOD = 1;     // Single write strobe mode
        BSC.CS1MOD.BIT.WRMOD = 0;       // Byte strobe mode
        BSC.CS1MOD.BIT.EWENB = 0;       // External wait is disabled
        BSC.CS1MOD.BIT.PRENB = 0;       // Page read access is disabled
        BSC.CS1MOD.BIT.PWENB = 0;       // Page write access is disabled
        BSC.CS1MOD.BIT.PRMOD = 0;       // Normal access compatible mode

        // CS1 Wait Control Register 1
        BSC.CS1WCNT1.BIT.CSPWWAIT_20 = 5;
        BSC.CS1WCNT1.BIT.CSPRWAIT_20 = 5;
        BSC.CS1WCNT1.BIT.CSWWAIT_40 = 5;
        BSC.CS1WCNT1.BIT.CSRWAIT_40 = 5;

        // CS1 Wait Control Register 2
        BSC.CS1WCNT2.BIT.CSROFF_20 = 1;
        BSC.CS1WCNT2.BIT.CSWOFF_20 = 1;
        BSC.CS1WCNT2.BIT.WDOFF_20 = 1;
        BSC.CS1WCNT2.BIT.RDON_20 = 1;
        BSC.CS1WCNT2.BIT.WRON_20 = 1;
        BSC.CS1WCNT2.BIT.WDON_20 = 0;
        BSC.CS1WCNT2.BIT.CSON_20 = 0;
    }
    else
    {
        if ((P9.PORT.BYTE & 0x0F) == 1)     // ETHERNET lan9215
        {
            // CS1 - P6.1 - MCU_~FIPCS
            // 0008 3802h - CS1 Control Register


            BSC.CS1CNT.BIT.EXENB = 1;   // Operation is enabled
            BSC.CS1CNT.BIT.BSIZE_10 = 0;        // A 16-bit bus space is selected
            BSC.CS1CNT.BIT.EMODE = 1;   // Endian of area is NOT the same as the endian of operating mode

            // CS1 Recovery Cycle Register
            BSC.CS1REC.BIT.RRCV_30 = 0;         // no recovery cycles is inserted
            BSC.CS1REC.BIT.WRCV_30 = 0;         // no recovery cycles is inserted

            // CS1 Mode Register
            BSC.CS1MOD.BIT.WRMOD = 1;   // Single write strobe mode
            //BSC.CS1MOD.BIT.WRMOD = 0; // Byte strobe mode
            BSC.CS1MOD.BIT.EWENB = 0;   // External wait is disabled
            BSC.CS1MOD.BIT.PRENB = 0;   // Page read access is disabled
            BSC.CS1MOD.BIT.PWENB = 0;   // Page write access is disabled
            BSC.CS1MOD.BIT.PRMOD = 0;   // Normal access compatible mode

            // CS1 Wait Control Register 1
            BSC.CS1WCNT1.BIT.CSPWWAIT_20 = 4;  //page access
            BSC.CS1WCNT1.BIT.CSPRWAIT_20 = 4;  //page access
            BSC.CS1WCNT1.BIT.CSWWAIT_40 = 3;
            BSC.CS1WCNT1.BIT.CSRWAIT_40 = 3;

            // CS1 Wait Control Register 2
            BSC.CS1WCNT2.BIT.CSROFF_20 = 1;
            BSC.CS1WCNT2.BIT.CSWOFF_20 = 0;
            BSC.CS1WCNT2.BIT.WDOFF_20 = 1;
            BSC.CS1WCNT2.BIT.RDON_20 = 1;
            BSC.CS1WCNT2.BIT.WRON_20 = 0;
            BSC.CS1WCNT2.BIT.WDON_20 = 0;
            BSC.CS1WCNT2.BIT.CSON_20 = 0;
        }
    }


    // ============== CS2 SRAM ===================================================================================

    // CS2 - P6.2 - MCU_~SRAMCS
    // CS2 Control Register
    BSC.CS2CNT.BIT.EXENB = 1;   // Operation is enabled
    BSC.CS2CNT.BIT.BSIZE_10 = 0;        // A 16-bit bus space is selected
    BSC.CS2CNT.BIT.EMODE = 0;   // Endian of area is the same as the endian of operating mode

    // CS2 Recovery Cycle Register
    BSC.CS2REC.BIT.RRCV_30 = 0;         // no recovery cycles is inserted
    BSC.CS2REC.BIT.WRCV_30 = 0;         // no recovery cycles is inserted

    // CS2 Mode Register
    BSC.CS2MOD.BIT.WRMOD = 1;   // Single write strobe mode
    BSC.CS2MOD.BIT.EWENB = 0;   // External wait is disabled
    BSC.CS2MOD.BIT.PRENB = 0;   // Page read access is disabled
    BSC.CS2MOD.BIT.PWENB = 0;   // Page write access is disabled
    BSC.CS2MOD.BIT.PRMOD = 0;   // Normal access compatible mode

    // CS2 Wait Control Register 1
    BSC.CS2WCNT1.BIT.CSPWWAIT_20 = 2;
    BSC.CS2WCNT1.BIT.CSPRWAIT_20 = 2;
    BSC.CS2WCNT1.BIT.CSWWAIT_40 = 2;
    BSC.CS2WCNT1.BIT.CSRWAIT_40 = 2;

    // CS2 Wait Control Register 2
    BSC.CS2WCNT2.BIT.CSROFF_20 = 0;
    BSC.CS2WCNT2.BIT.CSWOFF_20 = 1;
    BSC.CS2WCNT2.BIT.WDOFF_20 = 1;
    BSC.CS2WCNT2.BIT.RDON_20 = 1;
    BSC.CS2WCNT2.BIT.WRON_20 = 1;
    BSC.CS2WCNT2.BIT.WDON_20 = 0;
    BSC.CS2WCNT2.BIT.CSON_20 = 0;

    // ============== CS3 MRAM, NVRAM ============================================================================

    // CS3 - P6.3 - MCU_~MRAMCS
    // CS3 Control Register
    BSC.CS3CNT.BIT.EXENB = 1;   // Operation is enabled
    BSC.CS3CNT.BIT.BSIZE_10 = 0;        // A 16-bit bus space is selected
    BSC.CS3CNT.BIT.EMODE = 0;   // Endian of area is the same as the endian of operating mode

    // CS3 Recovery Cycle Register
    BSC.CS3REC.BIT.RRCV_30 = 0;         // no recovery cycles is inserted
    BSC.CS3REC.BIT.WRCV_30 = 0;         // no recovery cycles is inserted

    // CS3 Mode Register
    BSC.CS3MOD.BIT.WRMOD = 1;   // Single write strobe mode
    BSC.CS3MOD.BIT.EWENB = 0;   // External wait is disabled
    BSC.CS3MOD.BIT.PRENB = 0;   // Page read access is disabled
    BSC.CS3MOD.BIT.PWENB = 0;   // Page write access is disabled
    BSC.CS3MOD.BIT.PRMOD = 0;   // Normal access compatible mode

    // CS3 Wait Control Register 1
    BSC.CS3WCNT1.BIT.CSPWWAIT_20 = 2;
    BSC.CS3WCNT1.BIT.CSPRWAIT_20 = 2;
    BSC.CS3WCNT1.BIT.CSWWAIT_40 = 2;
    BSC.CS3WCNT1.BIT.CSRWAIT_40 = 2;

    // CS3 Wait Control Register 2
    BSC.CS3WCNT2.BIT.CSROFF_20 = 0;
    BSC.CS3WCNT2.BIT.CSWOFF_20 = 1;
    BSC.CS3WCNT2.BIT.WDOFF_20 = 1;
    BSC.CS3WCNT2.BIT.RDON_20 = 1;
    BSC.CS3WCNT2.BIT.WRON_20 = 1;
    BSC.CS3WCNT2.BIT.WDON_20 = 0;
    BSC.CS3WCNT2.BIT.CSON_20 = 0;

    // ============== CS4 PLD ====================================================================================

    // CS4 - P6.4 - MCU_~FPGACS
    // CS4 Control Register
    BSC.CS4CNT.BIT.EXENB = 1;   // Operation is enabled
    BSC.CS4CNT.BIT.BSIZE_10 = 0;        // A 16-bit bus space is selected
    BSC.CS4CNT.BIT.EMODE = 0;   // Endian of area is the same as the endian of operating mode

    // CS4 Recovery Cycle Register
    BSC.CS4REC.BIT.RRCV_30 = 0;         // no recovery cycles is inserted
    BSC.CS4REC.BIT.WRCV_30 = 0;         // no recovery cycles is inserted

    // CS4 Mode Register
    BSC.CS4MOD.BIT.WRMOD = 1;   // Single write strobe mode
    BSC.CS4MOD.BIT.EWENB = 0;   // External wait is disabled
    BSC.CS4MOD.BIT.PRENB = 0;   // Page read access is disabled
    BSC.CS4MOD.BIT.PWENB = 0;   // Page write access is disabled
    BSC.CS4MOD.BIT.PRMOD = 0;   // Normal access compatible mode

    // CS4 Wait Control Register 1
    BSC.CS4WCNT1.BIT.CSPWWAIT_20 = 3;
    BSC.CS4WCNT1.BIT.CSPRWAIT_20 = 3;
    BSC.CS4WCNT1.BIT.CSWWAIT_40 = 3;
    BSC.CS4WCNT1.BIT.CSRWAIT_40 = 3;

    // CS4 Wait Control Register 2
    BSC.CS4WCNT2.BIT.CSROFF_20 = 0;
    BSC.CS4WCNT2.BIT.CSWOFF_20 = 1;
    BSC.CS4WCNT2.BIT.WDOFF_20 = 1;
    BSC.CS4WCNT2.BIT.RDON_20 = 1;
    BSC.CS4WCNT2.BIT.WRON_20 = 1;
    BSC.CS4WCNT2.BIT.WDON_20 = 0;
    BSC.CS4WCNT2.BIT.CSON_20 = 0;

    // ============== Ports ======================================================================================

    /*--------------------------*\
        8C020h - P0 Data register

        P0.7 - non existant pin
        P0.6 - non existant pin
        P0.5 - MCU_DEV_TCK          (input RxD4 when in boot mode, to receive data via SCI4, host communications)
        P0.4 - MCU_DEV_TDI          (output TxD4 when in boot mode, to transmit data from SCI4, for host communications)
        P0.3 - MCU_DEV_TMS
        P0.2 - MCD_DEV_~TRST
        P0.1 - Rxd6 - MCU_C62_DEV_RXD1
        P0.0 - Txd6 - MCU_C62_DEV_TXD1
    \*--------------------------*/
    P0.ICR.BIT.B1 = 1;  // enable the input buffer
    P0.DDR.BIT.B1 = 0;  // P0.0 as input

    P0.DR.BIT.B0  = 1;  // set TXD6 (P0 data register), set the pin state before SCI6.SCR is configured
    P0.DDR.BIT.B0 = 1;  // TXD6, Set the idle state direction
    /*--------------------------*\
        8C021h - P1 Data register

        P1.7 - TXD3 - MCU_TXD4
        P1.6 - RXD3 - MCU_RXD4
        P1.5 - SCK3 - MCU_CLK4
        P1.4 - not connected
        P1.3 - TXD2 - MCU_TXD6
        P1.2 - RXD2 - MCU_RXD6
        P1.1 - SCK2 - MCU_CLK6
        P1.0 - MCU_JTAG_~READY      JTAG port to external board (voltage source, etc.)
    \*--------------------------*/
    P1.ICR.BIT.B0 = 1;  // enable the input buffer
    P1.DDR.BIT.B0 = 0;  // P1.0 as input
    // JTAG ready input (goes to zero when JTAG interface is powered, otherwise is 1 due to the internal pullup)

    // ToDo: there is no pull-up capability in Port1
    //  P1.0 needs an internal pull-up

    /*--------------------------*\
        8C022h - P2 Data register

        P2.7 - SCK1 - MCU_CLK5
        P2.6 - TXD1 - MCU_TXD5
        P2.5 - RXD1 - MCU_RXD5
        P2.4 - not connected
        P2.3 - not connected
        P2.2 - SCK0 - MCU_~TERM/SPY_~RDY   0=means FTDI usb chip is ready
        P2.1 - RXD0 - MCU_TERM_RXD0
        P2.0 - TXD0 - MCU_TERM_TXD0
    \*--------------------------*/

    //  needed by the SCI1 (uart1) configuration
    P2.ICR.BIT.B7 = 1;  // if SCK1 is input, Enable the input buffer
    P2.DDR.BIT.B7 = 0;  // if SCK1 is input, Ensure the pin is set to input

    P2.DR.BIT.B6  = 1;  // set TXD1 (P2 data register), set the pin state before SCI1.SCR is configured
    P2.DDR.BIT.B6 = 1;  // TXD1, Set the idle state direction

    P2.ICR.BIT.B5 = 1; // RXD1, the input buffer for the corresponding pin is enabled
    P2.DDR.BIT.B5 = 0; // RXD1, Ensure the pin is set to input

    P2.ICR.BIT.B2 = 1; // , the input buffer for the corresponding pin is enabled
    P2.DDR.BIT.B2 = 0; // , Ensure the pin is set to input

    P2.ICR.BIT.B1 = 1; // RXD0, the input buffer for the corresponding pin is enabled
    P2.DDR.BIT.B1 = 0; // RXD0, Ensure the pin is set to input

    P2.DR.BIT.B0  = 1;  // TXD0 (P2 data register), set the pin state before SCI0.SCR is configured
    P2.DDR.BIT.B0 = 1;  // TXD0, set the idle state direction

    /*--------------------------*\
        8C023h - P3 Data register

        P3.7 - MCU_JTAG_TCK ouput   JTAG port to external board (voltage source, etc.)
        P3.6 - MCU_JTAG_TMS output  JTAG port to external board (voltage source, etc.)
    // for MB100
    P3.5 - MCU_JTAG_TDO (output, it is "CONNECTED" to the external device TDI)   JTAG port to external board (voltage source, etc.)
    // for MB100-B
    P3.5 - MCU_JTAG_TDI (output, it is "CONNECTED" to the external device TDI)   JTAG port to external board (voltage source, etc.)
        P3.4 - ~IRQ4A - MCU_~TICK
        P3.3 - ~IRQ3A - FIP_~TIME_IRQ  (when  the FIP board this comes from exactly the same event as IRQ2)
        P3.2 - ~IRQ2A - FIP_~MSG_IRQ
        P3.1 - not connected
        P3.0 - not connected

        also needed

        PFCR9.ITS4 = 0; // P34 is designated as the IRQ4-A input pin.
        PFCR9.ITS3 = 0; // P33 is designated as the IRQ3-A input pin.
        PFCR9.ITS2 = 0; // P32 is designated as the IRQ2-A input pin.

        PFCR9, Port Function Control Register 9     Value after reset: 00h          address: 8C109h
                b7 ITS7 IRQ7 Pin Select
                    0: PE7 is designated as the IRQ7-A input pin.
                    1: P17 is designated as the IRQ7-B input pin.

                b6 ITS6 IRQ6 Pin Select
                    0: PE6 is designated as the IRQ6-A input pin.
                    1: P16 is designated as the IRQ6-B input pin.

                b5 ITS5 IRQ5 Pin Select
                    0: PE5 is designated as the IRQ5-A input pin.
                    1: P15 is designated as the IRQ5-B input pin.

                b4 ITS4 IRQ4 Pin Select
    ---             0: P34 is designated as the IRQ4-A input pin.
                    1: P14 is designated as the IRQ4-B input pin.

                b3 ITS3 IRQ3 Pin Select
    ---             0: P33 is designated as the IRQ3-A input pin.
                    1: P13 is designated as the IRQ3-B input pin.

                b2 ITS2 IRQ2 Pin Select
    ---             0: P32 is designated as the IRQ2-A input pin.
                    1: P12 is designated as the IRQ2-B input pin.

                b1 ITS1 IRQ1 Pin Select
                    0: P31 is designated as the IRQ1-A input pin.
                    1: P11 is designated as the IRQ1-B input pin.

                b0 ITS0 IRQ0 Pin Select
                    0: P30 is designated as the IRQ0-A input pin.
                    1: P10 is designated as the IRQ0-B input pin.

    IRQER2  IRQ detection enable register 2         00h     0008C302h 8
    IRQER3  IRQ detection enable register 3         00h     0008C303h 8
    IRQER4  IRQ detection enable register 4         00h     0008C304h 8
                b7,b6,b5,b4,b3,b2,b1        Reserved
    ---          0  0  0  0  0  0  0   should always be 0

                b0          IRQEN,IRQ Detection Enable
                 0 : Detection of the signal on the corresponding IRQn pin as an external interrupt source is disabled
    ---          1 : Detection of the signal on the corresponding IRQn pin as an external interrupt source is enabled

    IRQCR2  IRQ control register 2                  00h     0008C322h 8
    IRQCR3  IRQ control register 3                  00h     0008C323h 8
    IRQCR4  IRQ control register 4                  00h     0008C324h 8
                b7,b6,b5,b4 Reserved
                 0  0  0  0    should always be 0

                b3,b2       IRQMD[1:0] IRQ Detection Select
                 0  0 :  Low level
    ---          0  1 :  Falling edge
                 1  0 :  Rising edge
                 1  1 :  Rising and falling edges

                b1,b0       Reserved
                 0  0    should always be 0

    \*--------------------------*/

    ICU.IRQER2.BYTE = 0x01;     // FIP_~MSG_IRQ
    //  ICU.IRQER3.BYTE = 0x01;     // FIP_~TIME_IRQ, (when  the FIP board this comes from exactly the same event as IRQ2)
    ICU.IRQER4.BYTE = 0x01;     // MCU_~TICK, "hardware" 1ms Tick

    ICU.IRQCR2.BYTE = 0x04;     // Falling edge
    ICU.IRQCR3.BYTE = 0x04;     // Falling edge
    ICU.IRQCR4.BYTE = 0x04;     // Falling edge


    P3.DDR.BIT.B7 = 1;  // P3.7 as output, VS_JTAG TCK
    P3.DDR.BIT.B6 = 1;  // P3.6 as output, VS_JTAG TMS

    // for MB100
    //  P3.DDR.BIT.B5 = 1;  // P3.5 as output, VS_JTAG TDI
    //  P3.DR.BIT.B5  = 1;  // set to 1, inactive

    // for MB100-B
    P3.DDR.BIT.B5 = 1;  // P3.5 as output, VS_JTAG TDI
    P3.DR.BIT.B5  = 1;  // set to 1, inactive

    P3.ICR.BIT.B4 = 1;  // enable the input buffer
    P3.DDR.BIT.B4 = 0;  // P3.4 as input, MCU_~TICK

    P3.ICR.BIT.B3 = 1;  // enable the input buffer
    P3.DDR.BIT.B3 = 0;  // P3.3 as input, FIP_~TIME_IRQ

    P3.ICR.BIT.B2 = 1;  // enable the input buffer
    P3.DDR.BIT.B2 = 0;  // P3.2 as input, FIP_~MSG_IRQ

    /*--------------------------*\
        8C024h - P4 Data register

        P4.7 - ADC7 - MCU_~POWERCYCLE (output)
    P4.6 - ADC6 - MCU_TP1 - TEST POINT
    P4.5 - ADC5 - MCU_TP0 - TEST POINT
        P4.4 - ADC4 - for -15v measurement
        P4.3 - ADC3 - for +15v measurement
        P4.2 - ADC2 - for +5v measurement
        P4.1 - ADC1 - INLET_MEAS
        P4.0 - ADC0 - TEMP_MEAS

        To use the adc inputs
    P4.DDR.Bx bit for the analogue input should be set to 0 (input port)
        P4.ICR.Bx bit should be set to 0 (disabling the input buffer and fixing the input signal to the high level)

    \*--------------------------*/

    P4.DDR.BIT.B7 = 1;  // P4.7 as output, MCU_~POWERCYCLE
    P4.DR.BIT.B7  = 1;  // No powercycle by default

    P4.DDR.BIT.B6 = 1;  // P4.6 as output, TP0
    P4.DR.BIT.B6  = 0;  // set to 0

    P4.DDR.BIT.B5 = 1;  // P4.5 as output, TP1
    P4.DR.BIT.B5  = 0;  // set to 0

    // Ports 5 is used for Rx610 external bus (Data/Address/Control)
    /*--------------------------*\
        8C025h - P5 Data register

        P5.7 - MCU_~RDY
        P5.6 - not connected
        P5.5 - not connected
        P5.4 - not connected
        P5.3 - MCU_BCLK
        P5.2 - MCU_~RD
        P5.1 - MCU_~BHE
        P5.0 - MCU_~WR
    \*--------------------------*/

    //  P5.DDR.BIT.B7 = 1;  // set output direction, MCU_~RDY (goes to NET board)
    P5.DDR.BIT.B3 = 1;  // set output direction, MCU_BCLK

    // part of Ports 6 (the CS) is used for Rx610 external bus (Data/Address/Control)
    /*--------------------------*\
        8C026h - P6 Data register

        P6.7 - DAC1 - INLET
        P6.6 - DAC0 - HEATER_CTRL
        P6.5 - MCU_FWDTRIG
        P6.4 - CS4 - MCU_~FPGACS
        P6.3 - CS3 - MCU_~MRAMCS
        P6.2 - CS2 - MCU_~SRAMCS
        P6.1 - CS1 - MCU_~FIPCS
        P6.0 - CS0 - not connected
    \*--------------------------*/

    P6.DDR.BIT.B5 = 1;  // P6.5 as output, MCU_FWDTRIG, FastWatchdog (FWD)

    /*
    if P6.DR.BIT.B5 is 0 then when the network card
    receive a "reset SHORT pulse signal" (50) from the gateway it trigger
    the power cycle and if it is receive "reset LONG pulse signal" (100)
    it also trigger the power cycle.

    if P6.DR.BIT.B5 is 1 (longer integration time) then when the network card
    receive a "reset SHORT pulse signal" (50) from the gateway it is ignored.
    It waits for a "reset LONG pulse signal" (100) to trigger the power cycle.
    */

    P6.DR.BIT.B5  = 0;  // ACCEPT_BOTH_NET_PULSES(), for boot and crash

    /*--------------------------*\
        8C027h - P7 Data register

        P7.7 - not connected
    P7.6 - MCU_CMD_~ENABLE              Enable digital CMD output
        P7.5 - MRAM_PROTECT
        P7.4 - FGC out "0" to READ REVISON (output)
        P7.3 - FGC_TYPE2 (input)
        P7.2 - FGC_TYPE1 (input)
        P7.1 - FGC_TYPE0 (input)
        P7.0 - FGC out "1" to READ REVISON (output)
    \*--------------------------*/

    P7.DDR.BIT.B6 = 1;  // P7.6 as output, MCU_CMD_~ENABLE, DIG_CMD_ENA_not
    P7.DR.BIT.B6  = 1;  // set to 1, Disable CMD output, Enable digital CMD output

    // ToDo: there is no pull-up capability in Port7
    //  P7.5 needs an internal pull-up
    P7.DDR.BIT.B5 = 1;  // P7.5 as output, NVRAM protect

    P7.DDR.BIT.B4 = 1;  // P7.4 as output
    P7.DR.BIT.B4  = 0;  // set to 0

    P7.ICR.BIT.B3 = 1;  // enable the input buffer
    P7.DDR.BIT.B3 = 0;  // P7.3 as input, FGC_TYPE2

    P7.ICR.BIT.B2 = 1;  // enable the input buffer
    P7.DDR.BIT.B2 = 0;  // P7.2 as input, FGC_TYPE1

    P7.ICR.BIT.B1 = 1;  // enable the input buffer
    P7.DDR.BIT.B1 = 0;  // P7.1 as input, FGC_TYPE0

    P7.DDR.BIT.B0 = 1;  // P7.0 as output
    P7.DR.BIT.B0  = 1;  // set to 1

    /*--------------------------*\
        8C028h - P8 Data register

    P8.7 - non existent pin
        P8.6 - not connected
        P8.5 - not connected
    // for MB100
    P8.4 - MCU_JTAG_TDI        (input, it is "CONNECTED" to the external device TDO)   JTAG port to external board (voltage source, etc.)
    // for MB100-B
    P8.4 - MCU_JTAG_TDO        (input, it is "CONNECTED" to the external device TDO)   JTAG port to external board (voltage source, etc.)
        P8.3 - MCU_TRIG_SLOWDOG     output
        P8.2 - FPGA_DONE            input
        P8.1 - FPGA_~INIT           input
        P8.0 - FPGA_~PROG, FPGA_PROG_B      output
    \*--------------------------*/

    P8.ICR.BIT.B4 = 1;  // enable the input buffer
    // for MB100
    //  P8.DDR.BIT.B4 = 0;  // P8.4 as input, VS_JTAG TDI
    // for MB100-B
    P8.DDR.BIT.B4 = 0;  // P8.4 as input, VS_JTAG TDO

    P8.DDR.BIT.B3 = 1;  // P8.3 as output, MCU_TRIG_SLOWDOG, SlowWatchdog (SWD) trigger output to monostable
    P8.DR.BIT.B3  = 1;  // set to 1, SlowWatchdog (SWD) trigger = 1

    P8.ICR.BIT.B2 = 1;  // enable the input buffer
    P8.DDR.BIT.B2 = 0;  // P8.2 as input, FPGA_DONE

    P8.ICR.BIT.B1 = 1;  // enable the input buffer
    P8.DDR.BIT.B1 = 0;  // P8.1 as input, FPGA_~INIT, FPGA_INIT_B

    P8.DR.BIT.B0  = 1;  // prepare the output value, free FPGA_PROG_B
    //  P8.DDR.BIT.B0 = 1;  // P8.0 as output, FPGA_~PROG (to reset FPGA)
    /*--------------------------*\
        8C029h - P9 Data register

        done above because is need early to configure the data bus
    \*--------------------------*/
    // Ports A,B,C,D & E are used for Rx610 external bus (Data/Address/Control)
    /*--------------------------*\
        8C02Ah - PA Data register

        PA.7 - MCU_AD7
        PA.6 - MCU_AD6
        PA.5 - MCU_AD5
        PA.4 - MCU_AD4
        PA.3 - MCU_AD3
        PA.2 - MCU_AD2
        PA.1 - MCU_AD1
        PA.0 - MCU_AD0
    \*--------------------------*/
    PA.DDR.BIT.B7 = 1;  // P9.6 as output, MCU_AD7
    PA.DDR.BIT.B6 = 1;  // P9.6 as output, MCU_AD6
    PA.DDR.BIT.B5 = 1;  // P9.5 as output, MCU_AD5
    PA.DDR.BIT.B4 = 1;  // P9.4 as output, MCU_AD4
    PA.DDR.BIT.B3 = 1;  // P9.6 as output, MCU_AD3
    PA.DDR.BIT.B2 = 1;  // P9.6 as output, MCU_AD2
    PA.DDR.BIT.B1 = 1;  // P9.5 as output, MCU_AD1
    PA.DDR.BIT.B0 = 1;  // P9.4 as output, MCU_AD0
    /*--------------------------*\
        8C02Bh - PB Data register

        PB.7 - MCU_AD15
        PB.6 - MCU_AD14
        PB.5 - MCU_AD13
        PB.4 - MCU_AD12
        PB.3 - MCU_AD11
        PB.2 - MCU_AD10
        PB.1 - MCU_AD9
        PB.0 - MCU_AD8

        enable by PFCR4 Port Function Control Register 4
    // 0008 C104h   Port Function Control Register 4 (PFCR4)        Value after reset: 00h
    IOPORT.PFCR4.BIT.A8E = 1;


    \*--------------------------*/
    /*--------------------------*\
        8C02Ch - PC Data register

        PC.7 - TXD5 - not connected
        PC.6 - RXD5 - not connected
        PC.5 - SCK5 - not connected
        PC.4 - not connected
        PC.3 - MCU_AD19
        PC.2 - MCU_AD18
        PC.1 - MCU_AD17
        PC.0 - MCU_AD16

        enable by PFCR3 Port Function Control Register 3
    // 0008 C103h   Port Function Control Register 3 (PFCR3)        Value after reset: 00h
    \*--------------------------*/
    /*--------------------------*\
        8C02Dh - PD Data register

        PD.7 - MCU_DA7
        PD.6 - MCU_DA6
        PD.5 - MCU_DA5
        PD.4 - MCU_DA4
        PD.3 - MCU_DA3
        PD.2 - MCU_DA2
        PD.1 - MCU_DA1
        PD.0 - MCU_DA0
    \*--------------------------*/
    /*--------------------------*\
        8C02Eh - PE Data register

        PE.7 - MCU_DA15
        PE.6 - MCU_DA14
        PE.5 - MCU_DA13
        PE.4 - MCU_DA12
        PE.3 - MCU_DA11
        PE.2 - MCU_DA10
        PE.1 - MCU_DA9
        PE.0 - MCU_DA8
    \*--------------------------*/

    // ============== UART 0 ====================================================================================
    // UART0 - Terminal - connected to the FTDI serial/USB with the connector in the front panel

    //    P2.0  TxD0_OE     output (TPU.TIOCB3_OE=0 and SCI.TxD0_OE=1)
    //    P2.1  RxD0        input  (initial value after reset)
    //    P2.2  SCK0_OE     output (TPU.TIOCC3_OE=0 and TMR.TMO0_OE=0 and SCI.SCK0_OE=1)

    // this is done previously at P2 config
    //  P2.DR.BIT.B0 = 1;   // TXD0 (P2 data register), set the pin state before SCI0.SCR is configured
    //  P2.DDR.BIT.B0 = 1;  // TXD0, Set the idle state direction
    // Configure the input pin
    //  P2.ICR.BIT.B1 = 1; // the input buffer for the corresponding pin is enabled
    //  P2.DDR.BIT.B1 = 0; // RXD0, Ensure the pin is set to input

    // Configure the TXD pin
    //  TPU3.TIORH.BIT.IOB_30 = 0x00u;      // Disable the TIOCB3 output, Output compare disabled

    // SMR is 00 after reset
    //  SCI0.SMR.CM = 0 ; // asynchronous mode
    //  SCI0.SMR.CHR = 0; // 8 bits as the data length
    //  SCI0.SMR.PE = 0 ; // when transmitting: parity bit addition is not performed        when receiving: parity bit checking is not performed
    //  SCI0.SMR.PM = 0; // even parity
    //  SCI0.SMR.STOP = 0; // 1 stop bit
    //  SCI0.SMR.CKS10 = 00; // CKS[1:0] PCLK clock    (n = 0)

    //  SCI0.SCMR = 0xf2; // 0xf2 is the default, Smart card mode register
    //  SCI0.SEMR = 0x00;   // 0x00 is the default, Serial extended mode register

    SCI0.BRR = 161; // Bit rate 9600bps driven by PCLK which is 50MHz

    /*
        Note:
            both must be written at the same time because once one is written the register is protected
            SCI0.SCR.BYTE = (SCI0.SCR.BYTE | 0x30); // or b5b4  0011 0000

            can't be done this way
            SCI0.SCR.BIT.TE = 1; // Transmit Enable Bit, 1 = Transmit enabled
            SCI0.SCR.BIT.RE = 1; // Receive Enable Bit, 1 = Receive Enable

        // SMR is 0x after reset
        SCI0.SCR.BIT.TIE = 0;       // b7 - TXI interrupt request is disabled
        SCI0.SCR.BIT.RIE = 1;       // b6 - RXI and ERI interrupt requests are enabled
         we need RIE=1 even if we don't use interrupts to check the interrupt request bit status (used as char reception flag)
        SCI0.SCR.BIT.TE = 1;        // b5 - Transmit enabled
        SCI0.SCR.BIT.RE = 1;        // b4 - Receive enabled
        SCI0.SCR.BIT.TEIE = 0;      // b2 - TEI interrupt request is disabled
        SCI0.SCR.BIT.CKE_10 = 00;   // b1b0 - CKE[1:0] Clock Enable, on-chip baud rate generator, the SCKn pin functions as I/O port
    */

    // last thing to do because this blocks the registers
    SCI0.SCR.BYTE = 0x70; // 0111 0000

    // ============== UART 1 ====================================================================================
    // UART1 - routed to the PLD  (communication with SCOPE ?)

    //    P2.6  TxD1_OE     output (TPU.TIOCA5_OE=0 and TMR.TMO1_OE=0 and SCI.TxD1_OE=1)
    //    P2.5  RxD1        input  (initial value after reset)
    //    P2.7  SCK1_OE     output (TPU.TIOCB5_OE=0 and SCI.SCK0_OE=1)

    //  this is done previously at P2 config
    //  P2.DR.BIT.B6 = 1;   // TXD1 (P2 data register), set the pin state before SCI1.SCR is configured
    //  P2.DDR.BIT.B6 = 1;  // TXD1, Set the idle state direction
    // if SCK1 is input
    //  P2.ICR.BIT.B7 = 1;  // Enable the input buffer
    //  P2.DDR.BIT.B7 = 0;  // Ensure the pin is set to input
    // if SCK1 is output
    //  TPU5.TIOR.BIT.IOB = 0x0u;   // Disable the TIOCB5 output
    // Configure the input pin
    //  P2.ICR.BIT.B5 = 1; // RXD1, the input buffer for the corresponding pin is enabled
    //  P2.DDR.BIT.B5 = 0; // RXD1, Ensure the pin is set to input

    // Configure the TXD pin
    TPU5.TIOR.BIT.IOA_30 = 0x00u;       // Disable the TIOCA5 output, Output compare disabled
    //  TMR1.TCSR.OSA_10
    //  TMR1.TCSR.OSB_10
    TMR1.TCSR.BYTE = 0xf0u;     // Disable the TMO1 output

    // SMR is 0x after reset

    // SMR is 00 after reset
    //  SCI1.SMR.CM = 0 ; // asynchronous mode
    //  SCI1.SMR.CHR = 0; // 8 bits as the data length
    //  SCI1.SMR.PE = 0 ; // when transmitting: parity bit addition is not performed        when receiving: parity bit checking is not performed
    //  SCI1.SMR.PM = 0; // even parity
    //  SCI1.SMR.STOP = 0; // 1 stop bit
    //  SCI1.SMR.CKS10 = 00; // CKS[1:0] PCLK clock    (n = 0)

    //  SCI1.SCMR = 0xf2; // 0xf2 is the default, Smart card mode register
    //  SCI1.SEMR = 0x00;   // 0x00 is the default, Serial extended mode register

    SCI1.BRR = 161; // Bit rate 9600bps driven by PCLK which is 50MHz

    // this 2 enables the writing of the others
    SCI1.SCR.BIT.RE = 0; // Receive disabled
    SCI1.SCR.BIT.TE = 0; // Transmite disabled

    SCI1.SCR.BIT.TIE = 1; // TXI interrupt request is enabled
    SCI1.SCR.BIT.RIE = 1; // RXI and ERI interrupt requests are enabled
    SCI1.SCR.BIT.TEIE = 0; // TEI interrupt request is disabled

    // CKE[1:0] Clock Enable, on-chip baud rate generator, the SCKn pin functions as I/O port
    SCI1.SCR.BIT.CKE_10 = 00;
    // last thing to do because this blocks the registers
    //  SCI1.SCR.BYTE = 0x30; // 0011 0000

    // ============== UART 2 ====================================================================================
    // UART2 - routed to the PLD  (communication with POWER CONVERTER ?)
    // ============== UART 3 ====================================================================================
    // UART3 - routed to the PLD
    // ============== UART 4 ====================================================================================
    // UART4 - not used
    // in Boot mode
    // P0.5/RxD4 Input Used in boot mode to receive data via SCI4 (for host communications)
    // P0.4/TxD4 Output Used in boot mode to transmit data from SCI4 (for host communications)
    // ============== UART 5 ====================================================================================
    // UART5 - not used
    // ============== UART 6 ====================================================================================
    // UART6 - routed to the PLD - M16C62 development port (M16C62-UART1)
    //          when M16C62 is in NORMAL (not STAND_ALONE) its UART1 (M16C62 development uart) is directed to Rx610 UART6

    //    P0.0  TxD6_OE     output (SCI.TxD6_OE=1)  this is done with SCI6.SCR.TE=1
    //    P0.1  RxD6        input  (initial value after reset)
    //    P0.2  SCK6_OE     output (SCI.SCK6_OE=1, TMR.TMO2_OE=0)

    //  this is done previously at P2 config
    //  P0.DR.BIT.B0 = 1;   // TXD6 (P0 data register), set the pin state before SCI6.SCR is configured
    //  P0.DDR.BIT.B0 = 1;  // TXD6, Set the idle state direction
    // Configure the input pin
    //  P0.ICR.BIT.B1 = 1; // RXD6, the input buffer for the corresponding pin is enabled
    //  P0.DDR.BIT.B1 = 0; // RXD6, Ensure the pin is set to input

    // Configure the TXD pin


    // SMR is 00 after reset
    //  SCI6.SMR.CM = 0 ; // asynchronous mode
    //  SCI6.SMR.CHR = 0; // 8 bits as the data length
    //  SCI6.SMR.PE = 0 ; // when transmitting: parity bit addition is not performed        when receiving: parity bit checking is not performed
    //  SCI6.SMR.PM = 0; // even parity
    //  SCI6.SMR.STOP = 0; // 1 stop bit
    //  SCI6.SMR.CKS10 = 00; // CKS[1:0] PCLK clock    (n = 0)

    //  SCI6.SCMR = 0xf2; // 0xf2 is the default, Smart card mode register
    //  SCI6.SEMR = 0x00;   // 0x00 is the default, Serial extended mode register

    SCI6.BRR = 161; // Bit rate 9600bps driven by PCLK which is 50MHz

    /*
        Note:
            both must be written at the same time because once one is written the register is protected
            SCI6.SCR.BYTE = (SCI0.SCR.BYTE | 0x30); // or b5b4  0011 0000

            can't be done this way
    SCI6.SCR.BIT.TE = 1; // Transmit Enable Bit, 1 = Transmit enabled
            SCI6.SCR.BIT.RE = 1; // Receive Enable Bit, 1 = Receive Enable

        // SMR is 0x after reset
        SCI6.SCR.BIT.TIE = 0;       // b7 - TXI interrupt request is disabled
        SCI6.SCR.BIT.RIE = 1;       // b6 - RXI and ERI interrupt requests are enabled
         we need RIE=1 even if we don't use interrupts to check the interrupt request bit status (used as char reception flag)
    SCI6.SCR.BIT.TE = 1;    // b5 - Transmit enabled
        SCI6.SCR.BIT.RE = 1;        // b4 - Receive enabled
        SCI6.SCR.BIT.TEIE = 0;      // b2 - TEI interrupt request is disabled
        SCI6.SCR.BIT.CKE_10 = 00;   // b1b0 - CKE[1:0] Clock Enable, on-chip baud rate generator, the SCKn pin functions as I/O port
    */

    // last thing to do because this blocks the registers
    SCI6.SCR.BYTE = 0x40; // 0100 0000

    // ============== TMR0:TMR1 =================================================================================
    // TMR0(:TMR1) is used as a 16 bit counter incremented by PCLK 50MHz counting 50000 = 1ms

    // it is programmed to generate an interrupt every 1ms

    // TMR0(:TMR1) will count [0..49999] increment by PCLK (50MHz,20ns), the period is 1ms
    // and generates the interrupt CMIA0 - Compare Match A
    // 49999 = 0xC34F

    // Note:
    // this can be done with a TPUx (16 bit counter) in the same way

    // I couldn't found the way to cascade 2 TPUs (16 bit counters) so the reset of the low 16b
    // increments the high 16b counter

    // the MSLEEP() is not attached here as this interrupt can be inhibit, stopping the counter

    //-----------------------------------------------------------------------------------------------
    //  TMR0.TCNT.BYTE      =;       // after reset:00h     Timer counter                   88208h 8 or 16

    TMR0.TCORA.WORD     = 0xC34F; // after reset:FFh    Time constant register A        88204h 8 or 16
    //  TMR0.TCORB.BYTE     = 0x4F;  // after reset:FFh     Time constant register B        88206h 8 or 16

    TMR0.TCR.BYTE       = 0x48;  // after reset:00h     Timer control register          88200h 8
    /*
                b7          CMIEB Compare Match Interrupt Enable B
    ---          0 : Compare match B interrupt requests (CMIBm) are disabled
                 1 : Compare match B interrupt requests (CMIBm) are enabled

                b6          CMIEA Compare Match Interrupt Enable A
                 0 : Compare match A interrupt requests (CMIAm) are disabled
    ---          1 : Compare match A interrupt requests (CMIAm) are enabled

                b5          OVIE Timer Overflow Interrupt Enable
    ---          0 : Overflow interrupt requests (OVIm) are disabled
                 1 : Overflow interrupt requests (OVIm) are enabled

                b4,b3       CCLR[1:0] Counter Clear, ( Select edge or level by the TMRIS bit in TCCR )
                 0  0 : Clearing is disabled
    ---          0  1 : Cleared by compare match A
                 1  0 : Cleared by compare match B
                 1  1 : Cleared by the external reset input

                b2,b1,b0    Reserved
    ---          0  0  0    should always be 0
    */

    // by setting the bits TMR0.TCCR.CSS to 11, TMR0 and TMR1 are used as a 16bits counter
    // (when the bits in TMR1.TCCR.CSS are set to 11 TMR0 and TMR1 are in cascade)
    // count in TMR0.TCNT:TMR1.TCNT
    // setting taken from TMR0.TCR.CCLR (setting from TMR1.TCR.CCLR is ignored)
    // the clock source is selected at TMR1

    TMR0.TCCR.BYTE = 0x18;  // after reset:00h  Timer counter control register  8820Ah 8 or 16
    //  TMR0.TCCR.BYTE = 0x00u; // Stop the channel 0 clock input (by selecting external clock)
    /*
                b7          TMRIS Timer Reset Detection Condition Select (This bit is enabled when the TCR.CCLR[1:0] bits are 11b)
    ---          0 : Cleared at rising edge of the external reset
                 1 : Cleared when the external reset is high

                b6,b5       Reserved
    ---          0  0    should always be 0

                b4,b3       CSS[1:0] Clock Source Select
                 0  0 :  Uses external clock
                 0  1 :  Uses internal clock
                 1  0 :  Setting prohibited
    ---          1  1 :  use as 16bit timer,   Counts at TMR1.TCNT (TMR3.TCNT) overflow signal

                b2,b1,b0    CKS[2:0] Clock Select
                 0  0  0    Uses internal clock. Counts at PCLK.
                 0  0  1    Uses internal clock. Counts at PCLK/2.
                 0  1  0    Uses internal clock. Counts at PCLK/8.
                 0  1  1    Uses internal clock. Counts at PCLK/32.
                 1  0  0    Uses internal clock. Counts at PCLK/64.
                 1  0  1    Uses internal clock. Counts at PCLK/1024.
                 1  1  0    Uses internal clock. Counts at PCLK/8192.
                 1  1  1    Clock input prohibited
    */
    //  TMR0.TCSR.BYTE      = 0xE0;          // after reset:x0h     Timer control/status register   88202h 8
    /*
                b7,b6,b5    Reserved
    ---          1  1  1   should always be 1

                b4          ADTE A/D Trigger Enable
    ---          0 : A/D converter start requests by compare match A are disabled
                 1 : A/D converter start requests by compare match A are enabled

                b3,b2       OSB[1:0] Output Select B
    ---          0  0 : No change when compare match B occurs
                 0  1 : Low is output when compare match B occurs
                 1  0 : High is output when compare match B occurs
                 1  1 : Output is inverted when compare match B occurs (toggle output)

                b1,b0       OSA[1:0] Output Select A
    ---          0  0 : No change when compare match A occurs
                 0  1 : Low is output when compare match A occurs
                 1  0 : High is output when compare match A occurs
                 1  1 : Output is inverted when compare match A occurs (toggle output)
    */

    // by setting the bits TMR0.TCCR.CSS to 11, TMR0 and TMR1 are used as a 16bits counter
    // (when the bits in TMR1.TCCR.CSS are set to 11 TMR0 and TMR1 are in cascade)
    // count in TMR0.TCNT:TMR1.TCNT
    // setting taken from TMR0.TCR.CCLR (setting from TMR1.TCR.CCLR is ignored)
    // the clock source is selected at TMR1

    TMR1.TCCR.BYTE = 0x08; // count from PCLK.
    /*
                b7          TMRIS Timer Reset Detection Condition Select (This bit is enabled when the TCR.CCLR[1:0] bits are 11b)
    ---          0 : Cleared at rising edge of the external reset
                 1 : Cleared when the external reset is high

                b6,b5       Reserved
    ---          0  0    should always be 0

                b4,b3       CSS[1:0] Clock Source Select
                 0  0 :  Uses external clock
    ---          0  1 :  Uses internal clock
                 1  0 :  Setting prohibited
                 1  1 :  Counts at TMR1.TCNT (TMR3.TCNT) overflow signal

                b2,b1,b0    CKS[2:0] Clock Select
    ---          0  0  0    Uses internal clock. Counts at PCLK.
                 0  0  1    Uses internal clock. Counts at PCLK/2.
                 0  1  0    Uses internal clock. Counts at PCLK/8.
                 0  1  1    Uses internal clock. Counts at PCLK/32.
                 1  0  0    Uses internal clock. Counts at PCLK/64.
                 1  0  1    Uses internal clock. Counts at PCLK/1024.
                 1  1  0    Uses internal clock. Counts at PCLK/8192.
                 1  1  1    Clock input prohibited
    */
    // ============== TMR2:TMR3 =================================================================================
    // TMR2 and TMR3 are in cascade (8 bit counters)

    // TMR2 will count [0..49] increment by PCLK (50MHz,20ns), the period is 1us
    // the match count will increment TMR3
    // when TMR3 overflows it generates an interrupt (every 255us)

    // I couldn't found the way to cascade 2 TPUs (16 bit counters) so the reset of the low 16b
    // increments the high 16b counter

    // so with actual implementation with TMR2(:TMR3) I need a software free running counter incremented
    // in the 255us interrupt to allow USLEEP() to go beyond 255us

    //---------------------------------------------------------------------------------------
    //  TMR2.TCNT.BYTE      =;       // after reset:00h     Timer counter                   88208h 8 or 16
    TMR2.TCORA.BYTE     = 49;    // after reset:FFh     Time constant register A        88204h 8 or 16
    //  TMR2.TCORB.BYTE     =;       // after reset:FFh     Time constant register B        88206h 8 or 16
    TMR2.TCR.BYTE       = 0x08;  // after reset:00h     Timer control register          88200h 8
    /*
                b7          CMIEB Compare Match Interrupt Enable B
    ---          0 : Compare match B interrupt requests (CMIBm) are disabled
                 1 : Compare match B interrupt requests (CMIBm) are enabled

                b6          CMIEA Compare Match Interrupt Enable A
    ---          0 : Compare match A interrupt requests (CMIAm) are disabled
                 1 : Compare match A interrupt requests (CMIAm) are enabled

                b5          OVIE Timer Overflow Interrupt Enable
    ---          0 : Overflow interrupt requests (OVIm) are disabled
                 1 : Overflow interrupt requests (OVIm) are enabled

                b4,b3       CCLR[1:0] Counter Clear, ( Select edge or level by the TMRIS bit in TCCR )
                 0  0 : Clearing is disabled
    ---          0  1 : Cleared by compare match A
                 1  0 : Cleared by compare match B
                 1  1 : Cleared by the external reset input

                b2,b1,b0    Reserved
    ---          0  0  0    should always be 0
    */
    TMR2.TCCR.BYTE = 0x08;  // after reset:00h  Timer counter control register  8820Ah 8 or 16
    //  TMR2.TCCR.BYTE = 0x00u; // Stop the channel 0 clock input (by selecting external clock)
    /*
                b7          TMRIS Timer Reset Detection Condition Select (This bit is enabled when the TCR.CCLR[1:0] bits are 11b)
    ---          0 : Cleared at rising edge of the external reset
                 1 : Cleared when the external reset is high

                b6,b5       Reserved
    ---          0  0    should always be 0

                b4,b3       CSS[1:0] Clock Source Select
                 0  0 :  Uses external clock
    ---          0  1 :  Uses internal clock
                 1  0 :  Setting prohibited
                 1  1 :  Counts at TMR3.TCNT overflow signal

                b2,b1,b0    CKS[2:0] Clock Select
    ---          0  0  0    Uses internal clock. Counts at PCLK.
                 0  0  1    Uses internal clock. Counts at PCLK/2.
                 0  1  0    Uses internal clock. Counts at PCLK/8.
                 0  1  1    Uses internal clock. Counts at PCLK/32.
                 1  0  0    Uses internal clock. Counts at PCLK/64.
                 1  0  1    Uses internal clock. Counts at PCLK/1024.
                 1  1  0    Uses internal clock. Counts at PCLK/8192.
                 1  1  1    Clock input prohibited
    */
    //  TMR2.TCSR.BYTE      = 0xE0;          // after reset:x0h     Timer control/status register   88202h 8
    /*
                b7,b6,b5    Reserved
    ---          1  1  1   should always be 1

                b4          ADTE A/D Trigger Enable
    ---          0 : A/D converter start requests by compare match A are disabled
                 1 : A/D converter start requests by compare match A are enabled

                b3,b2       OSB[1:0] Output Select B
    ---          0  0 : No change when compare match B occurs
                 0  1 : Low is output when compare match B occurs
                 1  0 : High is output when compare match B occurs
                 1  1 : Output is inverted when compare match B occurs (toggle output)

                b1,b0       OSA[1:0] Output Select A
    ---          0  0 : No change when compare match A occurs
                 0  1 : Low is output when compare match A occurs
                 1  0 : High is output when compare match A occurs
                 1  1 : Output is inverted when compare match A occurs (toggle output)
    */
    //---------------------------------------------------------------------------------------

    // TMR3 will count [0..255] incremented by TMR2 count match (1us)
    // and generating INT_Excep_TMR3_OVI3 interrupt when overflows

    //---------------------------------------------------------------------------------------
    //  TMR3.TCNT.BYTE      =;       // after reset:00h     Timer counter                   88209h 8 or 16
    //  TMR3.TCORA.BYTE     =;       // after reset:FFh     Time constant register A        88205h 8 or 16
    //  TMR3.TCORB.BYTE     =;       // after reset:FFh     Time constant register B        88207h 8 or 16
    TMR3.TCR.BYTE       = 0x20;  // after reset:00h     Timer control register          88201h 8
    /*
                b7          CMIEB Compare Match Interrupt Enable B
    ---          0 : Compare match B interrupt requests (CMIBm) are disabled
                 1 : Compare match B interrupt requests (CMIBm) are enabled

                b6          CMIEA Compare Match Interrupt Enable A
    ---          0 : Compare match A interrupt requests (CMIAm) are disabled
                 1 : Compare match A interrupt requests (CMIAm) are enabled

                b5          OVIE Timer Overflow Interrupt Enable
                 0 : Overflow interrupt requests (OVIm) are disabled
    ---          1 : Overflow interrupt requests (OVIm) are enabled

                b4,b3       CCLR[1:0] Counter Clear, ( Select edge or level by the TMRIS bit in TCCR )
    ---          0  0 : Clearing is disabled
                 0  1 : Cleared by compare match A
                 1  0 : Cleared by compare match B
                 1  1 : Cleared by the external reset input

                b2,b1,b0    Reserved
    ---          0  0  0    should always be 0
    */
    TMR3.TCCR.BYTE      = 0x18;          // after reset:00h     Timer counter control register  8820Bh 8 or 16
    //  TMR3.TCCR.BYTE = 0x00u; // Stop the channel 1 clock input (by selecting external clock)
    /*
                b7          TMRIS Timer Reset Detection Condition Select (This bit is enabled when the TCR.CCLR[1:0] bits are 11b)
    ---          0 : Cleared at rising edge of the external reset
                 1 : Cleared when the external reset is high

                b6,b5       Reserved
    ---          0  0    should always be 0

                b4,b3       CSS[1:0] Clock Source Select
                 0  0 :  Uses external clock
                 0  1 :  Uses internal clock
                 1  0 :  Setting prohibited
    ---          1  1 :  Counts at TMR2.TCNT compare match A

                b2,b1,b0    CKS[2:0] Clock Select
    ---          0  0  0 : ignored when CSS[]=11
    */
    //  TMR3.TCSR.BYTE      = 0xE0;          // after reset:x0h     Timer control/status register   88203h 8
    /*
                b7,b6,b5    Reserved
    ---          1  1  1   should always be 1

                b4          Reserved
    ---          1 : should always be 1

                b3,b2       OSB[1:0] Output Select B
    ---          0  0 : No change when compare match B occurs
                 0  1 : Low is output when compare match B occurs
                 1  0 : High is output when compare match B occurs
                 1  1 : Output is inverted when compare match B occurs (toggle output)

                b1,b0       OSA[1:0] Output Select A
    ---          0  0 : No change when compare match A occurs
                 0  1 : Low is output when compare match A occurs
                 1  0 : High is output when compare match A occurs
                 1  1 : Output is inverted when compare match A occurs (toggle output)
    */

    // ============== TPU0:TPU1 =================================================================================
    // TPU0 is used as a 16 bit counter incremented by PCLK 50MHz counting 50000 = 1ms

    // it is programmed to generate an interrupt every 1ms

    // Note:
    // this is done with TMR0(:TMR1) in the same way

    // I couldn't found the way to cascade 2 TPUs (16 bit counters) so the reset of the low 16b
    // increments the high 16b counter

    // the MSLEEP() is attached to the interrupt generated by this counter
    // the interrupt level is set to 7, to not be stopped
    // there is a counter incremented by the 1ms interrupt in the ISR
    //-----------------------------------------------------------------------------------------------
    TPU0.TCR.BYTE = 0x20;    //  after reset:00h        Timer control register          88110h 8
    /*
                b7,b6,b5    CCLR[2:0] Counter Clear Source Select
                 0  0  0 TCNT counter clearing disabled
    ---          0  0  1 TCNT counter cleared by TGRA compare match/input capture
                 0  1  0 TCNT counter cleared by TGRB compare match/input capture
                 0  1  1 TCNT counter cleared by counter clearing for another channel performing synchronous clearing/synchronous operation
                 1  0  0 TCNT counter clearing disabled
                 1  0  1 TCNT counter cleared by TGRC compare match/input capture
                 1  1  0 TCNT counter cleared by TGRD compare match/input capture
                 1  1  1 TCNT counter cleared by counter clearing for another channel performing synchronous clearing/synchronous operation

                b4,b3       CKEG[1:0] Input Clock Edge Select
    ---          0  0 :  Counted at falling edge (for Internal Clock) or Counted at rising edge (for External clock)
                 0  1 :  Counted at rising edge (for Internal Clock) or Counted at falling edge (for External clock)
                 1  0 :  Counted at both edges
                 1  1 :  Counted at both edges

                b2,b1,b0    TPSC[2:0] Timer Prescaler Select
    ---          0  0  0    Uses internal clock. Counts at PCLK
                 0  0  1    Uses internal clock. Counts at PCLK/4
                 0  1  0    Uses internal clock. Counts at PCLK/16
                 0  1  1    Uses internal clock. Counts at PCLK/64
                 1  0  0    External clock: counts on TCLKA pin input
                 1  0  1    External clock: counts on TCLKB pin input
                 1  1  0    External clock: counts on TCLKC pin input
                 1  1  1    External clock: counts on TCLKD pin input
    */
    //  TPU0.TMDR.BYTE = 0x00;    //  after reset:00h       Timer mode register             88111h 8
    /*
                b7          ICSELD TGRD Input Capture
                 0 : Input capture input source is TIOCDn pin (n = 0, 3, 6, 9)
                 1 : Input capture input source is TIOCCn pin (n = 0, 3, 6, 9)

                b6          ICSELB TGRB Input Capture
                 0 : Input capture input source is TIOCBn pin (n = 0 to 11)
                 1 : Input capture input source is TIOCAn pin (n = 0 to 11)

                b5          BFB Buffer Operation B
    ---          0 : TPUn.TGRB operates normally operation (n = 0, 3, 6, 9)
                 1 : TPUn.TGRB and TPUn.TGRD used together for buffer operation (n = 0, 3, 6, 9)

                b4          BFB Buffer Operation A
    ---          0 : TPUn.TGRA operates normally operation (n = 0, 3, 6, 9)
                 1 : TPUn.TGRA and TPUn.TGRC used together for buffer operation (n = 0, 3, 6, 9)

                b3,b2,b1,b0 MD[3:0] Mode Select, b3 should always be 0
    ---          0  0  0  0: Normal operation
                 0  0  0  1: Setting prohibited
                 0  0  1  0: PWM mode 1
                 0  0  1  1: PWM mode 2
                 0  1  0  0: Phase counting mode 1
                 0  1  0  1: Phase counting mode 2
                 0  1  1  0: Phase counting mode 3
                 0  1  1  1: Phase counting mode 4
                Settings other than above are prohibited
    */

    //  TPU0.TIORH.BYTE = 0x00;    //  after reset:00h      Timer I/O control register H    88112h 8
    /*
                b7,b6,b5,b4 IOB[3:0] TGRB Control
    ---          0  0  0  0: Output compare register, Output disabled

                b3,b2,b1,b0 IOA[3:0] TGRA Control
    ---          0  0  0  0: Output compare register, Output disabled
    */
    //  TPU0.TIORL.BYTE = 0x00;    //  after reset:00h      Timer I/O control register L    88113h 8
    /*
                b7,b6,b5,b4 IOD[3:0] TGRD Control
    ---          0  0  0  0: Output compare register, Output disabled

                b3,b2,b1,b0 IOC[3:0] TGRC Control
    ---          0  0  0  0: Output compare register, Output disabled
    */
    //  TPU0.TSR   = 0x00;    //  after reset:xxh   Timer status register           88115h 8
    /*
                b7          TCFD Count Direction Flag
                 0 : TPUm.TCNT is counting down (m = 1, 2, 4, 5, 7, 8, 10, 11)
                 1 : TPUm.TCNT is counting up (m = 1, 2, 4, 5, 7, 8, 10, 11)
    */

    //  TPU0.TCNT  =       ;  //  after reset:0000h Timer counter                   88116h 16

    // TPU0.TCNT will count [0..49999] increment by PCLK (50MHz,20ns), the period is 1ms
    // and generates the interrupt TPU0 TGI0A - Compare Match TGRA
    // 49999 = 0xC34F

    TPU0.TGRA  = 0xC34F;  //  after reset:FFFFh Timer general register A        88118h 16

    //  TPU0.TGRB  =       ;  //  after reset:FFFFh Timer general register B        8811Ah 16
    //  TPU0.TGRC  =       ;  //  after reset:FFFFh Timer general register C        8811Ch 16
    //  TPU0.TGRD  =       ;  //  after reset:FFFFh Timer general register D        8811Eh 16

    //  TPU_UNIT0.TSYRA.BYTE = 0x00;    //  after reset:00h Timer Synchronous Register      88101h 8
    /*
                b7, b6      Reserved
                 0   0 : These bits are read as 0. The write value should always be 0

        b5          SYNC5 Timer Synchronisation 5
    ---          0 : TPU5.TCNT operates independently (presetting/clearing is unrelated to other channels)
                 1 : TPU5.TCNT performs synchronous operation (synchronous presetting/synchronous clearing is possible)

        b4          SYNC4 Timer Synchronisation 4
    ---          0 : TPU4.TCNT operates independently (presetting/clearing is unrelated to other channels)
                 1 : TPU4.TCNT performs synchronous operation (synchronous presetting/synchronous clearing is possible)

        b3          SYNC3 Timer Synchronisation 3
    ---          0 : TPU3.TCNT operates independently (presetting/clearing is unrelated to other channels)
                 1 : TPU3.TCNT performs synchronous operation (synchronous presetting/synchronous clearing is possible)

        b2          SYNC2 Timer Synchronisation 2
    ---          0 : TPU2.TCNT operates independently (presetting/clearing is unrelated to other channels)
                 1 : TPU2.TCNT performs synchronous operation (synchronous presetting/synchronous clearing is possible)

        b1          SYNC1 Timer Synchronisation 1
    ---          0 : TPU1.TCNT operates independently (presetting/clearing is unrelated to other channels)
                 1 : TPU1.TCNT performs synchronous operation (synchronous presetting/synchronous clearing is possible)

        b0          SYNC0 Timer Synchronisation 0
    ---          0 : TPU0.TCNT operates independently (presetting/clearing is unrelated to other channels)
                 1 : TPU0.TCNT performs synchronous operation (synchronous presetting/synchronous clearing is possible)
    */
    TPU0.TIER.BYTE = 0x41;    //  after reset:40h       Timer interrupt enable register 88114h 8
    /*
                b7          TTGE A/D Conversion Start Request Enable
    ---          0 : A/D conversion start request generation disabled
                 1 : A/D conversion start request generation enabled

                b6          CMIEA Compare Match Interrupt Enable A
                 1 : Reserved This bit is read as 1. The write value should always be 1

                b5          TCIEU Underflow Interrupt Enable
    ---          0 : Interrupt requests (TCImU) disabled (m = 1, 2, 4, 5, 7, 8, 10, 11)
                 1 : Interrupt requests (TCImU) enabled (m = 1, 2, 4, 5, 7, 8, 10, 11)

                b4          TCIEV Overflow Interrupt Enable (INT_Excep_TPU0_TCI0V, TPU0.TCNT overflow)
    ---          0 : Interrupt requests (TCImV) disabled (m = 0 to 11)
                 1 : Interrupt requests (TCImV) enabled (m = 0 to 11)

                b3          TGIED TGRD Interrupt Enable (INT_Excep_TPU0_TGI0D, TPU0.TGRD input capture/compare match)
    ---          0 : Interrupt requests (TGImD) disabled (m = 0, 3, 6, 9)
                 1 : Interrupt requests (TGImD) enabled (m = 0, 3, 6, 9)

                b2          TGIEC TGRC Interrupt Enable (INT_Excep_TPU0_TGI0C, TPU0.TGRC input capture/compare match)
    ---          0 : Interrupt requests (TGImC) disabled (m = 0, 3, 6, 9)
                 1 : Interrupt requests (TGImC) enabled (m = 0, 3, 6, 9)

                b1          TGIEB TGRB Interrupt Enable (INT_Excep_TPU0_TGI0B, TPU0.TGRB input capture/compare match)
    ---          0 : Interrupt requests (TGImB) disabled (m = 0 to 11)
                 1 : Interrupt requests (TGImB) enabled (m = 0 to 11)

                b0          TGIEA TGRA Interrupt Enable (INT_Excep_TPU0_TGI0A, TPU0.TGRA input capture/compare match)
                 0 : Interrupt requests (TGImA) disabled (m = 0 to 11)
    ---          1 : Interrupt requests (TGImA) enabled (m = 0 to 11)
    */
    //  TPU_UNIT0.TSTRA.BYTE = 0x40;    //  after reset:00h Timer Start Register    88100h 8
    TPU_UNIT0.TSTRA.BIT.CST0 = 1;    //  TPU0.TCNT performs count operation
    /*
                b7, b6      Reserved
                 0   0 : These bits are read as 0. The write value should always be 0

                b5          CST5 Counter Start 5
    ---          0 : TPU5.TCNT count operation is stopped
                 1 : TPU5.TCNT performs count operation

                b4          CST4 Counter Start 4
    ---          0 : TPU4.TCNT count operation is stopped
                 1 : TPU4.TCNT performs count operation

                b3          CST3 Counter Start 3
    ---          0 : TPU3.TCNT count operation is stopped
                 1 : TPU3.TCNT performs count operation

                b2          CST2 Counter Start 2
    ---          0 : TPU2.TCNT count operation is stopped
                 1 : TPU2.TCNT performs count operation

                b1          CST1 Counter Start 1
    ---          0 : TPU1.TCNT count operation is stopped
                 1 : TPU1.TCNT performs count operation

                b0          CST0 Counter Start 0
                 0 : TPU0.TCNT count operation is stopped
    ---          1 : TPU0.TCNT performs count operation
    */
    // ============== DAC 0 =====================================================================================
    //    DA.DADR0   dac 0 - Heater Control, analogue board heater
    //    DA.DADR1   dac 1 - Inlet Heater, external heater

    // ============== ADC 0 =====================================================================================
    /*
        Unit 0
        AD0.ADDRA  [pin 141, P4.0, AN0] analogue board temperature measurement
        AD0.ADDRB  [pin 139, P4.1, AN1] Inlet Temperature measurement (external)
        AD0.ADDRC  [pin 138, P4.2, AN2]  +5v power supply
        AD0.ADDRD  [pin 137, P4.3, AN3] +15v power supply

        Unit 1
        AD1.ADDRA  [pin 136, P4.4, AN4] -15v power supply
        AD1.ADDRB  [pin 135, P4.5, AN5] front panel TP0
        AD1.ADDRC  [pin 134, P4.6, AN6] front panel TP1

        To use the adc inputs
        P4.DDR.Bx bit for the analog input should be set to 0 (input port)
        P4.ICR.Bx bit should be set to 0 (disabling the input buffer and fixing the input signal to the high level)



        AD0.ADDRA   A/D data register A [AN0]               0000h   00088040h 16
        AD0.ADDRB   A/D data register B [AN1]               0000h   00088042h 16
        AD0.ADDRC   A/D data register C [AN2]               0000h   00088044h 16
        AD0.ADDRD   A/D data register D [AN3]               0000h   00088046h 16
        AD1.ADDRA   A/D data register A [AN4]               0000h   00088060h 16
        AD1.ADDRB   A/D data register B [AN4]               0000h   00088062h 16
        AD1.ADDRC   A/D data register C [AN6]               0000h   00088064h 16
        AD1.ADDRD   A/D data register D [AN7]               0000h   00088066h 16

        when
            ADx.ADDPR.DPSEL = 0, value is in bits [ b9..b0]
            ADx.ADDPR.DPSEL = 1, value is in bits [b15..b6]


        AD0.ADCSR   A/D control/status register     x0h     00088050h 8
        AD1.ADCSR   A/D control/status register     x0h     00088070h 8

                b7          undefined
                b6          ADIE    A/D Interrupt Enable
    ---          0 : ADI interrupt is disabled by completing A/D conversion
                 1 : ADI interrupt is enabled by completing A/D conversion
                b5          ADST    A/D Start
                 0 : Stops A/D conversion
    ---          1 : Starts A/D conversion
                b4          Reserved, should always be 0
                b3,b2,b1,b0 CH[3:0]
                    when
                        ADx.ADCR.MODE[1:0] = 00, single mode
                     0  0  0  0 : read AN0 (AN4)
                     0  0  0  1 : read AN1 (AN5)
                     0  0  1  0 : read AN2 (AN6)
                     0  0  1  1 : read AN3 (AN7)
                     other are prohibited
                        ADx.ADCR.MODE[1:0] = 1x, scan mode
                     0  0  0  0 : read AN0                  (AN4)
                     0  0  0  1 : read AN0,AN1              (AN4,AN5)
                     0  0  1  0 : read AN0,AN1,AN2          (AN4,AN5,AN6)
    ---              0  0  1  1 : read AN0,AN1,AN2,AN3      (AN4,AN5,AN6,AN7)
                     other are prohibited



        AD0.ADCR    A/D control register            00h     00088051h 8
        AD1.ADCR    A/D control register            00h     00088071h 8

                b7,b6,b5    TRGS[2:0] Trigger Select
    ---          0  0  0   Software trigger
                 0  0  1   Compare-match/input-capture A signals from TPU0 to TPU5
                 0  1  0   Compare-match A signal from TMR0
                 0  1  1   Trigger from ADTRG0# / Trigger from ADTRG1#
                 1  0  0   Compare-match/input-capture A signal from TPU0
                 1  0  1   Compare-match/input-capture A signals from TPU6 to TPU11
                 1  1  0   reserved
                 1  1  1   reserved

                b4          Reserved, should always be 0

                b3,b2       CKS[1:0] Clock Select
                 0  0 : PCLK/8
                 0  1 : PCLK/4
                 1  0 : PCLK/2
    ---          1  1 : PCLK

                b1,b0       MODE[1:0] Operation Mode Select
                 0  0 : Single mode
                 0  1 : reserved
    ---          1  0 : Continuous scan mode
                 1  1 : Single scan mode


        AD0.ADDPR   ADDRy format select register    00h     00088052h 8
        AD1.ADDPR   ADDRy format select register    00h     00088072h 8
                b7          DPSEL   ADDRy Format Select
    ---          0 : A/D data is aligned to the LSB end
                 1 : A/D data is aligned to the MSB end

                b6,b5,b4,b3,b2,b1,b0        Reserved
    ---          0  0  0  0  0  0  0    should always be 0


        AD0.ADSSTR  A/D sampling state register     19h     00088053h 8
        AD1.ADSSTR  A/D sampling state register     19h     00088073h 8


    */

    AD0.ADCR.BYTE = 0x0E;       // 0000 1110
    AD1.ADCR.BYTE = 0x0E;       // 0000 1110

    //  AD0.ADDPR.BYTE = 0x00;      // 0000 0000
    //  AD1.ADDPR.BYTE = 0x00;      // 0000 0000

    AD0.ADCSR.BYTE = 0x23;      // 0010 0011
    AD1.ADCSR.BYTE = 0x23;      // 0010 0011

    // ============== DMA 0 & 1 =================================================================================
    //    DMA 0 & 1 are used to transfer ethernet packet payload from external RAM buffers to lan chip
    //    (non DMA copy takes almost 100us for a full packet)
    // ============== DMA 0 & 1 =================================================================================

    // DMAC 0
    DMAC0.DMMOD.BIT.DMOD_20 = 0x0;      // Fixed transfer destination
    DMAC0.DMMOD.BIT.SMOD_20 = 0x1;      // Increment source
    DMAC0.DMMOD.BIT.SZSEL_20 = 0x2;     // 32 bits access
    DMAC0.DMMOD.BIT.OPSEL_30 = 0x0;     // Data count 1 (unused)

    DMAC0.DMCRA.BIT.DCTG_50 = 0x00;     // Software trigger
    DMAC0.DMCRA.BIT.DRLOD = 1;          // Reload destination address at end of DMA transfer
    DMAC0.DMCRA.BIT.SRLOD = 0;          // No source reload
    DMAC0.DMCRA.BIT.BRLOD = 0;          // No byte count reload
    DMAC0.DMCRA.BIT.DSEL_10 = 0x3;      // Nonstop transfer

    DMAC0.DMCRB.BIT.DSCLR = 1;          // Internal Status clear
    DMAC0.DMCRC.BIT.ECLR = 1;           //  ECLR enabled, DEN is cleared to 0 at end of transfer

    DMAC0.DMCDA = ETH_TX_DATA_FIFO_PORT_32; // Address destination is lan FIFO
    DMAC0.DMRDA = ETH_TX_DATA_FIFO_PORT_32; // Address destination reload address

    DMAC_COMMON.DMICNT.BIT.DINTM0 = 0;  // Disable interrupt at end of transfer

    // DMAC 1
    DMAC1.DMMOD.BIT.DMOD_20 = 0x0;      // Fixed transfer destination
    DMAC1.DMMOD.BIT.SMOD_20 = 0x1;      // Increment source
    DMAC1.DMMOD.BIT.SZSEL_20 = 0x2;     // 32 bits access
    DMAC1.DMMOD.BIT.OPSEL_30 = 0x0;     // Data count 1 (unused)

    DMAC1.DMCRA.BIT.DCTG_50 = 0x00;     // Software trigger
    DMAC1.DMCRA.BIT.DRLOD = 1;          // Reload destination address at end of DMA transfer
    DMAC1.DMCRA.BIT.SRLOD = 0;          // No source reload
    DMAC1.DMCRA.BIT.BRLOD = 0;          // No byte count reload
    DMAC1.DMCRA.BIT.DSEL_10 = 0x3;      // Nonstop transfer

    DMAC1.DMCRB.BIT.DSCLR = 1;          // Internal Status clear
    DMAC1.DMCRC.BIT.ECLR = 1;           // ECLR enabled, DEN is cleared to 0 at end of transfer

    DMAC1.DMCDA = ETH_TX_DATA_FIFO_PORT_32; // Address destination is lan FIFO
    DMAC1.DMRDA = ETH_TX_DATA_FIFO_PORT_32; // Address destination reload address

    DMAC_COMMON.DMICNT.BIT.DINTM1 = 1;  // Enable interrupt at end of transfer

    DMAC_COMMON.DMSCNT.BIT.DMST = 1;    // Start DMAC

#endif
}

// EOF

