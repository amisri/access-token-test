/*!
 *  @file  pulse_permit.c
 *  @brief Provides the logic to block and unblock the converter based on the
 *  pulse permit signal, routed to the digital input INTLKSPARE.
 *
 *  This functionality is only required by the ComHV-PS chassis and the
 *  Modulator converter, used in the RF system. The pulse permit signal is
 *  managed by the RF system.
 */

#include <pulse_permit.h>
#include <macros.h>
#include <state_manager.h>
#include <crate.h>
#include <pc_state.h>
#include <sta.h>
#include <trm.h>

// Types

/*!
 *  @brief Contains indexes of different commands in the commands array.
 */
typedef enum
{
    MST_INTLKSPARE_CMD_POWER_ON,   /*! Power on command index in the commands array. */
    MST_INTLKSPARE_BLOCK           /*! Blocking command index in the commands array. */
} mst_intlkspare_cmd_t;

// Static inline functions declarations

/*!
 *  @brief Sends command strings to the terminal message queue.
 *
 *  The function gets a pointer to a string literal and sends it, character by character,
 *  to the terminal message queue. It does not perform any validation of the given strings.
 *
 *  @param[in] command A pointer to the command string literal
 */
static INLINE void PulsePermitSendCommand(char const * const command);

// Internal variable definitions

/*!
 *  @brief Array of possible external commands
 *
 *  The array holds strings that can be send to the terminal queue in
 *  order to simulate external commands
 */
static char const * const intlkspare_ext_commands[] =
{
    "!S MODE.PC_SIMPLIFIED ON;",       // Power on the converter
    "!S MODE.PC_SIMPLIFIED BLOCKING;"  // Block the converter
};

// Internal function definitions

static INLINE void PulsePermitSendCommand(char const * const command)
{
    char const * c = command;

    while (*c)
    {
        // Post characters to the terminal queue.
        // Two casts suppress compiler warnings

        OSMsgPost(terminal.msgq, (void *)(intptr_t) *c++);
    }
}

BOOLEAN PulsePermitValid(void)
{
    BOOLEAN retval = TRUE;

    if ((crateGetType() == FGC_CRATE_TYPE_COMHV_PS      &&
         !Test(sta.inputs, DIG_IP1_INTLKSPARE_MASK16))  ||
        (crateGetType() == FGC_CRATE_TYPE_KLYSTRON_MOD  &&
         !Test(DIG_IPDIRECT_P, DIG_IPDIRECT_STATUS12_MASK16)))
    {
        retval = FALSE;
    }

    return (retval);
}

void PulsePermitProcess(void)
{
    static BOOLEAN pc_on_f = FALSE;

    // This functionality is only meaningful to the ComHV-PS chassis
    // and the kystron modulator converter.

    if (crateGetType() != FGC_CRATE_TYPE_COMHV_PS &&
        crateGetType() != FGC_CRATE_TYPE_KLYSTRON_MOD)
    {
        return;
    }

    // Only unblock/block if the desired state is above BLOCKING. Otherwise
    // the converter is already unblocked and should remained so.

    if (PulsePermitValid())
    {
        if (PcStateEqual(FGC_PC_BLOCKING) && pc_on_f)
        {
            pc_on_f = FALSE;

            PulsePermitSendCommand(intlkspare_ext_commands[MST_INTLKSPARE_CMD_POWER_ON]);
        }
    }
    else
    {
        if (PcStateAboveEqual(FGC_PC_BLOCKING)             &&
            !PcStateCmpEqual(sta.mode_pc, FGC_PC_BLOCKING) &&
            !pc_on_f)
        {
            // When the Pulse Permit (INTLK SPARE) is enabled again, the FGC can
            // automatically transition back to it MODE.PC_SIMPLIFIED ON, unless
            // the desired state was already BLOCKING.

            pc_on_f = TRUE;

            PulsePermitSendCommand(intlkspare_ext_commands[MST_INTLKSPARE_BLOCK]);
        }
    }

    // Prevent MODE.PC_SIMPLIFIED ON if transitioning to OFF.

    if (PcStateBelow(FGC_PC_BLOCKING))
    {
        pc_on_f = FALSE;
    }
}

// EOF
