/*---------------------------------------------------------------------------------------------------------*\
 File:          pll.c

 Purpose:       FGC3 MCU Software - Analogue Phase-locked loop functions

 Author:        Quentin.King@cern.ch

 Notes:         This file is included in the FGC3 boot, FGC3 main programs and the plltest project.
\*---------------------------------------------------------------------------------------------------------*/

#define PLL_GLOBALS             // define pll global variable

#include <math.h>               // For fabs()

#include "pll.h"
#include "defconst.h"           // For PLL state machine constants
#include "memmap_mcu.h"         // For PLL register masks

/*---------------------------------------------------------------------------------------------------------*/
static void PllCalcPhaseError(INT32U                phase_ref,
                              INT32U                int_sync_time,
                              struct pll_vars_sync * sync,
                              struct pll_vars_err * err)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by PllCalc() to calculate the phase errors for the sync signals and to
  accumulate the sum of these phase errors.
\*---------------------------------------------------------------------------------------------------------*/
{
    // If sync time is fresh

    if (!(sync->sync_time & PLL_EXTSYNCTIME_OLD_MASK32))
    {
        // Calculate phase error between the internal sync time and the external/network sync time
        // Note that phase_err is the signed result of the difference between two unsigned values

        sync->phase_err = ((int_sync_time - sync->sync_time) >> PLL_INTSYNCTIME_LATCH_SHIFT) - phase_ref;

        // Accumulate the sum of the phase errors unless they are large and not

        if (fabs(sync->phase_err) < (PLL_ITER_PERIOD_TICKS / 4.0))
        {
            err->sync_count++;
            err->phase_err_sum += sync->phase_err;
        }

        // In the Ethernet case if the time packet is not received, we do not retrieve the counter from PLL_NETIRQTIME_P.
        // This implies that in the ETH18 and ETH19 cases we do not know if it is old or not by the OLD flag.
        // So, we should set the OLD flag also by software

        sync->sync_time = sync->sync_time | PLL_EXTSYNCTIME_OLD_MASK32;
    }
    else
    {
        sync->phase_err = 0;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void PllCalcPhaseErrsAverages(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by PllCalc() to calculate the phase errors averages from the sums accumulated
  over one PLL period.
\*---------------------------------------------------------------------------------------------------------*/
{
    // If Ethernet interface in use then check the external syncs

    if (!pll.fip_f)
    {
        // If more than 50% of external syncs received then calculate average
        // external sync phase error from sum and count

        if (pll.external.sync_count > 0)
        {
            pll.external.phase_err_average = (FP32)pll.external.phase_err_sum / pll.external.sync_count;
        }

        // Keep sync count for diagnostics

        pll.external.prev_sync_count = pll.external.sync_count;
    }

    // Check network (FIP or Ethernet) syncs

    if (pll.network.sync_count > 0)
    {
        // Calculate average external sync phase error from sum and count

        pll.network.phase_err_average = (FP32)pll.network.phase_err_sum / pll.network.sync_count;

        // Keep sync counts for diagnostics

    }

    pll.network.prev_sync_count          = pll.network.sync_count;
}
/*---------------------------------------------------------------------------------------------------------*/
static void PllRun(FP32 phase_err)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by PllCapture() and PllLocked() to run the PI algorithm that calculates the
  integrator and DAC values needed to drive the VCXO to reduce the phase error.  Both the integrator
  and the DAC are given in units of ppm of frequency shift for the VCXO.
\*---------------------------------------------------------------------------------------------------------*/
{
    pll.pi_err = phase_err;

    // Calculate DAC using PI algorithm

    pll.dac_ppm = pll.integrator_ppm + (PLL_INT_GAIN + PLL_PROP_GAIN) * phase_err;

    // Clip DAC to working range

    if (pll.dac_ppm < -PLL_MAX_DAC_PPM)
    {
        pll.dac_ppm       = -PLL_MAX_DAC_PPM;
        pll.dac_clipped_f = 1;
    }
    else if (pll.dac_ppm > PLL_MAX_DAC_PPM)
    {
        pll.dac_ppm       = PLL_MAX_DAC_PPM;
        pll.dac_clipped_f = 1;
    }
    else // DAC not clipped
    {
        pll.dac_clipped_f = 0;

        if (pll.state == FGC_PLL_LOCKED &&
            pll.ext_sync_f == 0         &&
            pll.time_in_state_iters > PLL_INT_FLTR_DELAY_ITERS)
        {
            // When PLL locked and using (noisy) network sync, filter previous integrator value

            pll.filtered_integrator_ppm += (1.0 / PLL_INTEGRATOR_FLTR_TC) *
                                           (pll.integrator_ppm - pll.filtered_integrator_ppm);
        }
        else
        {
            // When not locked or using external sync, set filtered integrator to previous integrator value

            pll.filtered_integrator_ppm = pll.integrator_ppm;
        }

        // Integrate new phase error

        pll.integrator_ppm += PLL_INT_GAIN * phase_err;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U PllFailed(void)
/*---------------------------------------------------------------------------------------------------------*\
  STATE_PLL: PLL_FAILED

  It indicates that it was not possible for the PLL to reach the locked state within the time permitted or
  that the sync was completely wrong when locked.  This might indicate that the DAC or VCXO has a
  hardware fault or potentially that the gateway has a serious timing problem. The state machine will
  wait some time in this state before trying to synchronise again.
\*---------------------------------------------------------------------------------------------------------*/
{
    // If timeout for FAILED state has elapsed then switch to NO_SYNC

    if (pll.time_in_state_iters >= PLL_WAIT_IN_FAULT_ITERS)
    {
        return (FGC_PLL_NO_SYNC);
    }

    // Otherwise wait in FAILED state

    return (FGC_PLL_FAILED);
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U PllNoSync(void)
/*---------------------------------------------------------------------------------------------------------*\
  STATE_PLL: PLL_NO_SYNC
\*---------------------------------------------------------------------------------------------------------*/
{
    // If enough external or network IRQ sync pulses were received during the previous period

    if ((pll.external.sync_count == PLL_PERIOD_ITERS) || (pll.network.sync_count > PLL_PERIOD_ITERS / 2))
    {
        // Switch to CAPTURE state to run the PLL

        return (FGC_PLL_CAPTURE);
    }

    // Wait in NO SYNC state

    return (FGC_PLL_NO_SYNC);
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U PllCapture(void)
/*---------------------------------------------------------------------------------------------------------*\
  STATE_PLL: PLL_CAPTURE
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32        phase_err;

    // If no network syncs received or external sync rate was not exactly right during previous period

    if (pll.external.sync_count != PLL_PERIOD_ITERS && pll.network.sync_count == 0)
    {
        // Switch to NO_SYNC state and apply the initial integrator value to the DAC

        pll.dac_ppm = pll.integrator_ppm = pll.filtered_integrator_ppm;

        return (FGC_PLL_NO_SYNC);
    }

    // Select phase error to use - the external (opto) sync takes priority
    // if the external sync count is exactly correct

    if (pll.external.sync_count == PLL_PERIOD_ITERS)
    {
        phase_err      = pll.external.phase_err_average;
        pll.ext_sync_f = 1;
    }
    else // otherwise use the average network phase error
    {
        phase_err      = pll.network.phase_err_average - pll.eth_sync_cal;
        pll.ext_sync_f = 0;

        if (pll.no_ext_sync == FALSE)
        {
            pll.no_ext_sync_count++;
        }
    }

    // Run PI algorithm to calculate the DAC value

    PllRun(phase_err);

    // Check error against limit for locking

    if (fabs(phase_err) < PLL_LOCKED_ERR_LIMIT)
    {
        // If phase error below lock limit for the delay period then switch to LOCKED state

        if (++pll.under_locked_limit_count > PLL_DELAY_TO_LOCK)
        {
            return (FGC_PLL_LOCKED);
        }
    }
    else
    {
        pll.under_locked_limit_count = 0;
    }

    // Check time in CAPTURE against failure limit

    if (pll.time_in_state_iters > PLL_MAX_CAPTURE_ITERS)
    {
        // Failed to lock so switch to PLL_FAILED and reset DAC to initial value

        pll.dac_ppm = pll.integrator_ppm = pll.init_integrator_ppm;

        return (FGC_PLL_FAILED);
    }

    // Return FAST_SLEW state if DAC is clipped

    if (pll.dac_clipped_f)
    {
        return (FGC_PLL_FAST_SLEW);
    }

    // DAC is not clipped so return CAPTURE state

    return (FGC_PLL_CAPTURE);
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U PllLocked(void)
/*---------------------------------------------------------------------------------------------------------*\
  STATE_PLL: PLL_LOCKED
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      ext_sync_f;
    FP32        phase_err;

    // If no network syncs received and external sync rate was not exactly right during previous period

    if (pll.external.sync_count != PLL_PERIOD_ITERS && pll.network.sync_count == 0)
    {
        // Switch to NO_SYNC state and apply the filtered integrator value to the DAC for maximum precision

        pll.dac_ppm = pll.integrator_ppm = pll.filtered_integrator_ppm;

        return (FGC_PLL_NO_SYNC);
    }

    // Select phase error to use - the external (opto) sync takes priority if exactly the
    // right number were received.

    if (pll.external.sync_count == PLL_PERIOD_ITERS)
    {

        phase_err  = pll.external.phase_err_average;
        ext_sync_f = 1;

        // Use 1st-order filter on Ethernet average phase error to calculate a calibration
        // in case the external sync fails and it has to switch to the Ethernet sync

        pll.eth_sync_cal += (1.0 / PLL_ETH_SYNC_CAL_FILTER_TC) *
                            (pll.network.phase_err_average - pll.eth_sync_cal);
    }
    else
    {
        // ToDo : degraded mode without external sync, it should raise a warning.

        phase_err  = pll.network.phase_err_average - pll.eth_sync_cal;
        ext_sync_f = 0;
        
        if (pll.no_ext_sync == FALSE)
        {
            pll.no_ext_sync_count++;
        }
    }

    // Run PI algorithm to calculate the DAC value if the sync source hasn't just changed

    if (pll.ext_sync_f == ext_sync_f)
    {
        PllRun(phase_err);

        // Check if DAC is clipped when locked - indicating a serious timing issue in the FGC or gateway

        if (pll.dac_clipped_f)
        {
            // Apply filtered integrator to DAC for maximum precision

            pll.dac_ppm = pll.integrator_ppm = pll.filtered_integrator_ppm;

            return (FGC_PLL_FAILED);
        }
    }
    else // else sync source changed so skip this iteration and use integrator as the DAC value
    {
        pll.dac_ppm = pll.integrator_ppm;
    }

    pll.ext_sync_f = ext_sync_f;

    // Continue in LOCKED state
    return (FGC_PLL_LOCKED);
}
/*---------------------------------------------------------------------------------------------------------*/
void PllInit(INT16U fip_f, FP32 init_integrator_ppm, FP32 init_eth_sync_cal)
/*---------------------------------------------------------------------------------------------------------*\
  This must be called to initialise the PLL.  The integrator and eth_sync_cal values from the previous
  period of operation should be recovered from non-volatile memory and passed to this function.
\*---------------------------------------------------------------------------------------------------------*/
{
    pll.fip_f                   = fip_f;
    pll.eth_sync_cal            = init_eth_sync_cal;
    pll.init_integrator_ppm     = init_integrator_ppm;
    pll.integrator_ppm          = init_integrator_ppm;
    pll.filtered_integrator_ppm = init_integrator_ppm;
    pll.dac_ppm                 = init_integrator_ppm;
    pll.state                   = FGC_PLL_NO_SYNC;
    pll.time_in_state_iters     = 0;
    pll.fip.sync_time           = PLL_NETIRQTIME_OLD_MASK32;
    pll.eth18.sync_time         = PLL_NETIRQTIME_OLD_MASK32;
    pll.eth19.sync_time         = PLL_NETIRQTIME_OLD_MASK32;
    pll.ext.sync_time           = PLL_EXTSYNCTIME_OLD_MASK32;

    PllDacVcxo();               // Initialise the DAC integer and fractional parts
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U PllCalc(INT32U int_sync_time)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from PllFIP() and PllEth() to process the new sync time register values.  It
  returns the value that should be written to the INTSYNCTIME register.

  The external sync cannot be trusted to start the internal 50Hz clock because it is easy to plug the
  sync cable into an output of the gateway timing interface that generates 1 pulse per second. As a result,
  only network sync signals are trusted to start the clock.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      next_state;

    // If the internal 50Hz is still in reset state

    if (int_sync_time & PLL_INTSYNCTIME_RST_MASK32)
    {
        // Check if a valid sync was received and if so then set INTSYNCTIME to start
        // the internal 50Hz aligned with UTC.

        if (pll.fip_f)   // FIP interface
        {
            if (!(pll.fip.sync_time & PLL_NETIRQTIME_OLD_MASK32))
            {
                int_sync_time = pll.fip.sync_time + (PLL_FIP_PHASE_REF << PLL_INTSYNCTIME_LATCH_SHIFT);
            }
        }
        else            // Ethernet interface - Ethernet sync must be present to start
        {
            if (!(pll.eth18.sync_time & PLL_NETIRQTIME_OLD_MASK32))
            {
                int_sync_time = pll.eth18.sync_time + ((PLL_ETH18_PHASE_REF + (INT32U)pll.eth_sync_cal) <<
                                                       PLL_INTSYNCTIME_LATCH_SHIFT);
            }
            else if (!(pll.eth19.sync_time & PLL_NETIRQTIME_OLD_MASK32))
            {
                int_sync_time = pll.eth19.sync_time + ((PLL_ETH19_PHASE_REF + (INT32U)pll.eth_sync_cal) <<
                                                       PLL_INTSYNCTIME_LATCH_SHIFT);
            }
        }

        return (int_sync_time);
    }

    // Calculate and accumulate the sum of the phase errors on every iteration

    if (pll.fip_f)
    {
        PllCalcPhaseError(PLL_FIP_PHASE_REF,   int_sync_time, &pll.fip,   &pll.network);
    }
    else
    {
        PllCalcPhaseError(PLL_EXT_PHASE_REF,   int_sync_time, &pll.ext,   &pll.external);
        PllCalcPhaseError(PLL_ETH18_PHASE_REF, int_sync_time, &pll.eth18, &pll.network);
        PllCalcPhaseError(PLL_ETH19_PHASE_REF, int_sync_time, &pll.eth19, &pll.network);
    }

    // On every PLL period (the period depends on the PLL state)

    if (++pll.iter_count >= PLL_PERIOD_ITERS)
    {
        pll.iter_count = 0;

        // Calculate phase error averages from the phase error sums

        PllCalcPhaseErrsAverages();

        // Run PLL function according to PLL state

        switch (pll.state)
        {
            default:                next_state = PllFailed();    break;

            case FGC_PLL_NO_SYNC:   next_state = PllNoSync();    break;

            case FGC_PLL_FAST_SLEW:
            case FGC_PLL_CAPTURE:   next_state = PllCapture();    break;

            case FGC_PLL_LOCKED:    next_state = PllLocked();    break;
        }

        // Reset phase error sums and counters ready for the next PLL period

        pll.external.sync_count            = 0;
        pll.external.phase_err_sum         = 0;
		pll.network.sync_count             = 0;
		pll.network.phase_err_sum          = 0;

        // Keep track of the time in the current state and set the PLL period for the next period

        if (next_state != pll.state)
        {
            pll.state = next_state;
            pll.time_in_state_iters = 0;

        }
        else
        {
            pll.time_in_state_iters += PLL_PERIOD_ITERS;
        }

       	PllDacVcxo();
    }

    return (int_sync_time);
}
/*---------------------------------------------------------------------------------------------------------*/
void PllDacVcxo(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will translate the pll.dac_ppm value into pll.dacvcxo_int and pll.dacvcxo_frac.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 dacvcxo;
    FP32 dacvcxo_int;

    dacvcxo = pll.dac_ppm * PLL_DAC_HALF_RANGE / PLL_VCXO_PULL_PPM + PLL_DAC_OFFSET;

    pll.dacvcxo_frac = (INT16U)(65535.999 * modff(dacvcxo, &dacvcxo_int));
    pll.dacvcxo_int  = (INT16U)dacvcxo_int;
}
// EOF

