/*---------------------------------------------------------------------------------------------------------*\
  File:         macros.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MACROS_H        // header encapsulation
#define MACROS_H

#include <cc_types.h>

//-----------------------------------------------------------------------------------------------------------
// Bit manipulation macros: Set, Clr, Test, TestAll
//

// Set(v,b): in v, set all bits identified by bitmask b

#define Set(v,b)                ( (v) |=  (b) )         // Atomic action on the FGC2

// Clr(v,b): in v, clear all bits identified by bitmask b

#define Clr(v,b)                ( (v) &= ~(b) )

// Implementation notes regarding the Test() macro:
//
//  (Please refer also to the macros.h header file on the MCU side)
//
//  Same as on the MCU, the implementation of the Test macro on FGC2 and FGC3 differs.
//  On the C32 all the integer types are 32 bits, therefore a boolean is also on
//  32 bits and there is no reason to do a conversion to an actual boolean (0 or 1).
//  On the C67 however, the BOOLEAN type can be defined to be less than 32 bits and
//  then the same issue as on the MCU arises. Without the double not operator !!,
//  what would the following piece of code do?
//
//      BOOLEAN set_f;                      // Let us say that BOOLEAN is a INT8U
//      set_f = Test(value, 0x0100);        // set_f = 0 or 1 ?

// Test(v,b): Test if at least one bit in bitmask b is set in v. Returns a BOOLEAN on FGC3, not FGC2.

#define Test(v,b)           ( !!( (v) & (b) ) )

// TestAll(v,b): Test if all bits in bitmask b are set in v. Returns a BOOLEAN.

#define TestAll(v,b)          ( ( (v) & (b) ) == (b) )

//-----------------------------------------------------------------------------------------------------------

#endif  // MACROS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: macros.h
\*---------------------------------------------------------------------------------------------------------*/
