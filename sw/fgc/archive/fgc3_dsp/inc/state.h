/*!
 *  @file      state.h
 *  @defgroup  FGC3:DSP
 *  @brief     DSP operation state declaration
 */

#ifndef FGC_STATE_H
#define FGC_STATE_H

// Includes

#include <cc_types.h>

// Platform/class specific functions

/*!
 *  This function is called when the PC state has changed.
 */
void StatePc(INT32U new_state_op);

// External function declarations

/*!
 *  This function is called from IsrPeriod() if the operational state has changed.
 */
void  StateOp(INT32U new_state_op);

#endif

// EOF
