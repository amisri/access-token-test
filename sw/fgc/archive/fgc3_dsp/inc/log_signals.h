/*!
 *  @file      log_signals.h
 *  @defgroup  FGC3:DSP
 *  @brief     File providing functionality for logging signals
 */

#ifndef FGC_LOG_SIGNALS_H
#define FGC_LOG_SIGNALS_H

#ifdef LOG_SIGNALS_GLOBALS
#define LOG_SIGNALS_VARS_EXT
#else
#define LOG_SIGNALS_VARS_EXT extern
#endif

#ifdef LOG_SIGNALS_GLOBALS_FAR
#define LOG_SIGNALS_VARS_EXT_FAR
#else
#define LOG_SIGNALS_VARS_EXT_FAR extern
#endif

// Includes

#include <cc_types.h>
#include <defconst.h>
#include <mcu_dsp_common.h>

// Constants

#define LOG_SIGNALS_LOG_MAX_SIGNALS   (2)

// External structures, unions and enumerations

typedef struct log_signal_capture
{
    INT32U  freeze_f;               //!< Capture log freeze flag
    INT32U  n_samples_recorded;     //!< Number of samples recorded
} log_signal_capture_t;

//
/*!
 * Buffer index colouring, to distinguish between one pass on the circular
 * buffer and the next.
 */
enum log_idx_color
{
    LOG_ODD   = 1,   //!< 1st, 3rd, 5th, etc. pass on the circular buffer
    LOG_EVEN         //!< 2nd, 4th, 6th, etc. pass on the circular buffer
};

/*!
 * Index in the circular buffer, along with its colouring
 */
typedef struct log_signal_index
{
    INT32U index;           //!< Index value
    enum log_idx_color
    color; //!< Binary state to distinguish between one pass on the circular buffer and the next
} log_signal_index_t;

/*!
 * Generic log control structure
 */
typedef struct log_signals_ctrl
{
    struct log_signal_index  write_idx;       //!< Circular buffers write index
    struct log_signal_index  last_start_idx;  //!< Index of the last start index (usually, C0)
    INT32U  max_index;                     //!< Circular buffers max index (= max size)
    INT16U  nb_signals;
    FP32  * signal_src_ptr[LOG_SIGNALS_LOG_MAX_SIGNALS];
    FP32  * log_buf_ptr   [LOG_SIGNALS_LOG_MAX_SIGNALS];
} log_signals_ctrl_t;

typedef struct log_signal_interval_ns
{
    INT32U interval_ns;
} log_signal_interval_ns_t;

/*!
 * Oasis related structure
 */
typedef struct log_signal_oasis
{
    struct log_oasis_interval_ns
    {
        INT32U cycle_value;  //!< = interval_ns of the current cycle
        INT32U prop_value;   //!< Properties LOG.OASIS.(I_REF|I_MEAS).INTERVAL_NS
        //!< = interval_ns of previously logged cycles
    } interval_ns;

    struct log_oasis_subsampling
    {
        INT32U cycle_value;  //!< subsampling value of the current cycle
        INT32U prop_value;   //!< Property LOG.OASIS.SUBSAMPLE
        INT32U counter;      //!< counter is in range 0..(cycle_value-1)
    } subsampling;

    INT32U cycle_tag[FGC_MAX_USER_PLUS_1]; //!< Cycle stamp for the logged segment

    /*!
     * The background task (BGP) must copy those segments in the shared
     * memory for the MCU to read them. This is the purpose of the function
     * BgpLogOasisNewSegment()
     */
    struct log_oasis_segments
    {
        struct abs_time_us     timestamp[FGC_MAX_USER_PLUS_1];  //!< Log data segment timestamp
        struct log_signal_index   start_idx[FGC_MAX_USER_PLUS_1];  //!< Log data segment start index
        INT32U
        size[FGC_MAX_USER_PLUS_1];  //!< Log data segment size, and, LOG.OASIS.I_REF/I_MEAS.DATA property size
        //!< One reason why this array was introduced is because the dpcls copy
        //!< of the size array cannot be used directly as the property size.

    } local; //!< The log.oasis segments data that the DSP ISR will update.
} log_signal_oasis_t;

// External variable definitions

LOG_SIGNALS_VARS_EXT log_signal_oasis_t log_oasis;
LOG_SIGNALS_VARS_EXT log_signal_capture_t log_capture;

// Platform/class specific functions

void LogSignalClassCaptureLog(uint16_t n_sample);

// External function declarations

/*!
 * This function is called at the start of every cycle to manage the capture log.
 */
void LogSignalCaptureStart(void);

/*!
 * This function is called every ms to manage the capture log.
 */
void LogSignalCaptureLog(void);

/*!
 * This function is a generic way of logging signals. It is used for the
 * LOG.OASIS buffers but could as well be used for other logs.
 */
void LogSignalsLog(log_signals_ctrl_t * cyc_log_ctrl);

/*!
 * This function is called on every DSP iteration to log the OASIS signals.
 * With property LOG.OASIS.SUBSAMPLE the user is able to sub-sample the log
 */
void LogSignalsOasisLog(log_signals_ctrl_t * cyc_log_ctrl);

/*!
 * This function is intended to be called at the beginning of every user cycle.
 *
 * It will set the start_idx in the LOG.OASIS buffer, for the specified user.
 */
void LogSignalsOasisStartCycle(log_signals_ctrl_t * cyc_log_ctrl);

/*!
 * This function is intended to be called at the end of every user cycle.
 *
 * It will calculate the size of the LOG.OASIS buffer for the specified user.
 * It will also notify the Background task that a new log segment is available.
 * The BGP task will in turn copy the log information in shared memory and notify
 * the MCU.
 */
void LogSignalsOasisEndCycle(INT16U user, log_signals_ctrl_t * cyc_log_ctrl,
                             struct abs_time_us * cycle_timestamp_ptr);

/*!
 * This function is intended to be called when entering the cycling state.
 * It will reset the LOG.OASIS internal variables.
 */
void LogSignalOasisReset(log_signals_ctrl_t * cyc_log_ctrl);

#endif

// EOF
