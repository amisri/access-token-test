/*---------------------------------------------------------------------------------------------------------*\
  File:         hw_tms320c6727.h

  Contents:     constants for FCC3 DSP
                   Texas   TMS320c6727b DSP
                                   family: Texas TMS 320 C 6000
                                   code  : TMS 320 C 6727B GDH 300

  History:

\*---------------------------------------------------------------------------------------------------------*/
#ifndef HW_TMS320C6727_H        // header encapsulation
#define HW_TMS320C6727_H

#include <cc_types.h>

//-----------------------------------------------------------------------------------------------------------
// Start address of peripherals
#define  INTERNAL_ROM_PAGE_0                 0X00000000 // Start: Internal ROM Page 0 (256K Bytes) End: 0x0003FFFF Byte and Word
#define  INTERNAL_ROM PAGE_1                 0x00040000 // Start: Internal ROM Page 1 (128K Bytes) End: 0x0005FFFF Byte and Word
#define  INTERNAL_RAM_PAGE_0                 0x10000000 // Start: Internal RAM Page 0 (256K Bytes) End: 0x1003FFFF Byte and Word
#define  MEMORY_N_CACHE_CONTROL_REG_START    0x20000000 // Start: Memory and Cache Control Registers End: 0x2000001F Word Only
#define  EMULATION_CONTROL_REG_START         0x30000000 // Start: Emulation Control Registers (Do Not Access) End: 0x3FFFFFFF Word Only
#define  DEVICE_CONFIG_REG_START             0x40000000 // Start: Device Configuration Registers End: 0x40000083 Word Only
#define  PLL_REG_START                       0x41000000 // Start: PLL Control Registers End: 0x4100015F Word Only
#define  RTI_REG_START                       0x42000000 // Start: Real-time Interrupt (RTI) Control Registers End: 0x420000A3 Word Only
#define  UHPI_REG_START                      0x43000000 // Start: Universal Host-Port Interface (UHPI) Registers End: 0x43000043 Word Only
#define  McASP0_REG_START                    0x44000000 // Start: McASP0 Control Registers End: 0x440002BF Word Only
#define  McASP1_REG_START                    0x45000000 // Start: McASP1 Control Registers End: 0x450002BF Word Only
#define  McASP2_REG_START                    0x46000000 // Start: McASP2 Control Registers End: 0x460002BF Word Only
#define  SPI0_REG_START                      0x47000000 // Start: SPI0 Control Registers End: 0x4700007F Word Only
#define  SPI1_REG_START                      0x48000000 // Start: SPI1 Control Registers End: 0x4800007F Word Only
#define  I2C0_REG_START                      0x49000000 // Start: I2C0 Control Registers End: 0x4900007F Word Only
#define  I2C1_REG_START                      0x4A000000 // Start: I2C1 Control Registers End: 0x4A00007F Word Only
#define  McASP0_DMA_REG_START                0x54000000 // Start: McASP0 DMA Port (any address in this range) End: 0x54FFFFFF Word Only
#define  McASP1_DMA_REG_START                0x55000000 // Start: McASP1 DMA Port (any address in this range) End: 0x55FFFFFF Word Only
#define  McASP2_DMA_REG_START                0x56000000 // Start: McASP2 DMA Port (any address in this range) End: 0x56FFFFFF Word Only
#define  dMAX_REG_START                      0x60000000 // Start: dMAX Control Registers End: 0x6000008F Word Only
#define  MAX0_EVENT_ENTRY                    0x61008000 // Start: MAX0 (HiMAX) Event Entry Table End: 0x6100807F Byte and Word
#define  MAX0_TRANSFER_ENTRY                 0x610080A0 // Start: MAX0 (HiMAX) Transfer Entry Table End: 0x610081FF Byte and Word
#define  MAX1_EVENT_ENTRY                    0x62008000 // Start: MAX1 (LoMAX) Event Entry Table End: 0x6200807F Byte and Word
#define  MAX1_TRANSFER_ENTRY                 0x620080A0 // Start: MAX1 (LoMAX) Transfer Entry Table End: 0x620081FF Byte and Word
#define  SDRAM_SPACE_START                   0x80000000 // Start: External SDRAM space on EMIF End: 0x8FFFFFFF Byte and Word
#define  FLASH_SPACE_START                   0x90000000 // Start: External Asynchronous / Flash space on EMIF End: 0x9FFFFFFF Byte and Word
#define  EMIF_REG_START                      0xF0000000 // Start: EMIF Control Registers End: 0xF00000BF Word Only(


// C6727 peripheral definitions
#define MCASP0_PFUNC  (volatile INT32U *)(McASP0_REG_START + 0x10)
#define MCASP0_PDIR   (volatile INT32U *)(McASP0_REG_START + 0x14)
#define MCASP0_PDOUT  (volatile INT32U *)(McASP0_REG_START + 0x18)
#define MCASP0_PDIN   (volatile INT32U *)(McASP0_REG_START + 0x1c)
#define MCASP0_PDSET  (volatile INT32U *)(McASP0_REG_START + 0x1c)
#define MCASP0_PDCLR  (volatile INT32U *)(McASP0_REG_START + 0x20)
#define MCASP1_PFUNC  (volatile INT32U *)(McASP1_REG_START + 0x10)
#define MCASP1_PDIR   (volatile INT32U *)(McASP1_REG_START + 0x14)
#define MCASP1_PDOUT  (volatile INT32U *)(McASP1_REG_START + 0x18)
#define MCASP1_PDIN   (volatile INT32U *)(McASP1_REG_START + 0x1c)
#define MCASP1_PDSET  (volatile INT32U *)(McASP1_REG_START + 0x1c)
#define MCASP1_PDCLR  (volatile INT32U *)(McASP1_REG_START + 0x20)
#define MCASP2_PFUNC  (volatile INT32U *)(McASP2_REG_START + 0x10)
#define MCASP2_PDIR   (volatile INT32U *)(McASP2_REG_START + 0x14)
#define MCASP2_PDOUT  (volatile INT32U *)(McASP2_REG_START + 0x18)
#define MCASP2_PDIN   (volatile INT32U *)(McASP2_REG_START + 0x1c)
#define MCASP2_PDSET  (volatile INT32U *)(McASP2_REG_START + 0x1c)
#define MCASP2_PDCLR  (volatile INT32U *)(McASP2_REG_START + 0x20)

// C6727 peripheral definitions
#define PLL_BASE_ADDR   PLL_REG_START
#define PLL_PID         ( PLL_BASE_ADDR + 0x000 )   // identification
#define PLL_CSR         ( PLL_BASE_ADDR + 0x100 )   // control/status
#define PLL_M           ( PLL_BASE_ADDR + 0x110 )   // multiplier 1...25
#define PLL_DIV0        ( PLL_BASE_ADDR + 0x114 )   //
#define PLL_DIV1        ( PLL_BASE_ADDR + 0x118 )   //
#define PLL_DIV2        ( PLL_BASE_ADDR + 0x11C )   //
#define PLL_DIV3        ( PLL_BASE_ADDR + 0x120 )   //
#define PLL_CMD         ( PLL_BASE_ADDR + 0x138 )   // controller command
#define PLL_STAT        ( PLL_BASE_ADDR + 0x13c )   // controller status
#define ALN_CTL         ( PLL_BASE_ADDR + 0x140 )   // controller clock align control
#define CKEN            ( PLL_BASE_ADDR + 0x148 )   // clock enable control
#define CKSTAT          ( PLL_BASE_ADDR + 0x14c )   // clock status
#define SYSTAT          ( PLL_BASE_ADDR + 0x150 )   // SYSCLK status

// C6727 peripheral definitions
#define CSR_PLLEN        0x00000001
#define CSR_PLLPWRDN     0x00000010
#define CSR_OSCPWRDN     0x00000004
#define CSR_PLLRST       0x00000008
#define CSR_PLLPWRDN     0x00000010
#define CSR_PLLSTABLE    0x00000040
#define DIV_ENABLE       0x00008000
#define CMD_GOSET        0x00000001

#define EMIF_AWCCR       (EMIF_REG_START + 0x04)           // async wait cycle config
#define EMIF_SDCR        (EMIF_REG_START + 0x08)           // SDRAM config reg
#define EMIF_SDRCR       (EMIF_REG_START + 0x0c)           // SDRAM refresh control
#define EMIF_A1CR        (EMIF_REG_START + 0x10)           // asynchronous 1 config
#define EMIF_SDTIMR      (EMIF_REG_START + 0x20)           // SDRAM timing register
#define EMIF_SDSRETR     (EMIF_REG_START + 0x3c)           // SDRAM self refresh exit timing register
#define EMIF_EIRR        (EMIF_REG_START + 0x40)           // interrupt raw
#define EMIF_EIMR        (EMIF_REG_START + 0x44)           // interrupt mask reg
#define EMIF_EIMSR       (EMIF_REG_START + 0x48)           // interrupt mask set
#define EMIF_EIMCR       (EMIF_REG_START + 0x4c)           // interrupt mask clear reg


#define  UHPI        (volatile INT32U *)(UHPI_REG_START+0x08)   // Configuration Register
#define  CFGHPIAMSB  (volatile INT32U *)(UHPI_REG_START+0x0C)   // Most Significant Byte of UHPI Address
#define  CFGHPIAUMB  (volatile INT32U *)(UHPI_REG_START+0x10)   // Upper Middle Byte of UHPI Address
// UHPI Internal Registers
#define  PID         (volatile INT32U *)(UHPI_REG_START+0x00)   // Peripheral ID Register
#define  PWREMU      (volatile INT32U *)(UHPI_REG_START+0x04)   // Power and Emulation Management Register
#define  GPIOINT     (volatile INT32U *)(UHPI_REG_START+0x08)   // General Purpose I/O Interrupt Control Register
#define  GPIOEN      (volatile INT32U *)(UHPI_REG_START+0x0C)   // General Purpose I/O Enable Register
#define  GPIODIR1    (volatile INT32U *)(UHPI_REG_START+0x10)   // General Purpose I/O Direction Register 1
#define  GPIODAT1    (volatile INT32U *)(UHPI_REG_START+0x14)   // General Purpose I/O Data Register 1
#define  GPIODIR2    (volatile INT32U *)(UHPI_REG_START+0x18)   // General Purpose I/O Direction Register 2
#define  GPIODAT2    (volatile INT32U *)(UHPI_REG_START+0x1C)   // General Purpose I/O Data Register 2
#define  GPIODIR3    (volatile INT32U *)(UHPI_REG_START+0x20)   // General Purpose I/O Direction Register 3
#define  GPIODAT3    (volatile INT32U *)(UHPI_REG_START+0x24)   // General Purpose I/O Data Register 3
#define  HPIC        (volatile INT32U *)(UHPI_REG_START+0x30)   // Control Register
#define  HPIAW       (volatile INT32U *)(UHPI_REG_START+0x34)   // Write Address Register
#define  HPIAR       (volatile INT32U *)(UHPI_REG_START+0x38)   // Read Address Register

#define   UHPI_BYTEAD (1<<4)     // 0 R/W UHPI Host Address Type
// 0 = Host Address is a word address
// 1 = Host Address is a byte address
#define   UHPI_FULL (1<<3)       // 0 R/W UHPI Multiplexing Mode (when NMUX = 0)
// 0 = Half-Word (16-bit data) Multiplexed Host Address and Data Mode
// 1 = Fullword (32-bit data) Multiplexed Host Address and Data Mode
#define   UHPI_NMUX (1<<2)       // 0 R/W UHPI Non-Multiplexed Mode Enable
// 0 = Multiplexed Host Address and Data Mode
// 1 = Non-Multiplexed Host Address and Data Mode (utilizes optional
//     UHPI_HA[15:0] pins). Host data bus is 32 bits in Non-Multiplexed mode.
#define   UHPI_PAGEM (1<<1)      // 0 R/W UHPI Page Mode Enable (Only for Multiplexed Host Address and Data Mode).
// 0 = Full 32-bit DSP address specified through host port.
// 1 = Only lower 16 bits of DSP address are specified through host port. Upper
// 16 bits are restricted to the page selected by CFGHPIAMSB and
// CFGHPIAUMB registers.
#define   UHPI_ENA (1<<0)        // 0 R/W UHPI Enable
// 0 = UHPI is disabled
// 1 = UHPI is enabled. Set this bit to '1' only after configuring the other bits in this
// register.
//
#define   UHPI_HPIAMSB (1<<0)    // R/W UHPI most significant byte of DSP address to access in non-multiplexed host
// address/data mode and in multiplexed host address and data mode when
// CFGHPI.PAGEM = 1. Sets bits [31:24] of the DSP internal address as
// accessed through UHPI.

// RTI registers

#define  RTIGCTRL               RTI_REG_START           // Global Control Register. Starts / stops the counters.
#define  RTICAPCTRL             (RTI_REG_START + 0x08)  // Capture Control. Controls the capture source for the counters.
#define  RTICOMPCTRL            (RTI_REG_START + 0x0C)  // Compare Control. Controls the source for the compare registers.
#define  RTIFRC0                (RTI_REG_START + 0x10)  // Free-Running Counter 0. Current value of free running counter 0.
#define  RTIUC0                 (RTI_REG_START + 0x14)  // Up-Counter 0. Current value of prescale counter 0.
#define  RTICPUC0               (RTI_REG_START + 0x18)  // Compare Up-Counter 0. Compare value compared with prescale counter 0.
#define  RTICAFRC0              (RTI_REG_START + 0x20)  // Capture Free-Running Counter 0. Current value of free running counter 0 on external event.
#define  RTICAUC0               (RTI_REG_START + 0x24)  // Capture Up-Counter 0. Current value of prescale counter 0 on external event.
#define  RTIFRC1                (RTI_REG_START + 0x30)  // Free-Running Counter 1. Current value of free running counter 1.
#define  RTIUC1                 (RTI_REG_START + 0x34)  // Up-Counter 1. Current value of prescale counter 1.
#define  RTICPUC1               (RTI_REG_START + 0x38)  // Compare Up-Counter 1. Compare value compared with prescale counter 1.
#define  RTICAFRC1              (RTI_REG_START + 0x40)  // Capture Free-Running Counter 1. Current value of free running counter 1 on external event.
#define  RTICAUC1               (RTI_REG_START + 0x44)  // Capture Up-Counter 1. Current value of prescale counter 1 on external event.
#define  RTICOMP0               (RTI_REG_START + 0x50)  // Compare 0. Compare value to be compared with the counters.
#define  RTIUDCP0               (RTI_REG_START + 0x54)  // Update Compare 0. Value to be added to the compare register 0 value on compare match.
#define  RTICOMP1               (RTI_REG_START + 0x58)  // Compare 1. Compare value to be compared with the counters.
#define  RTIUDCP1               (RTI_REG_START + 0x5C)  // Update Compare 1. Value to be added to the compare register 1 value on compare match.
#define  RTICOMP2               (RTI_REG_START + 0x60)  // Compare 2. Compare value to be compared with the counters.
#define  RTIUDCP2               (RTI_REG_START + 0x64)  // Update Compare 2. Value to be added to the compare register 2 value on compare match.
#define  RTICOMP3               (RTI_REG_START + 0x68)  // Compare 3. Compare value to be compared with the counters.
#define  RTIUDCP3               (RTI_REG_START + 0x6C)  // Update Compare 3. Value to be added to the compare register 3 value on compare match.
#define  RTISETINT              (RTI_REG_START + 0x80)  // Set Interrupt Enable. Sets interrupt enable bits int RTIINTCTRL without having to do aread-modify-write operation.
#define  RTICLEARINT            (RTI_REG_START + 0x84)  // Clear Interrupt Enable. Clears interrupt enable bits int RTIINTCTRL without having to do a read-modify-write operation.
#define  RTIINTFLAG             (RTI_REG_START + 0x88)  // Interrupt Flags. Interrupt pending bits.
#define  RTIDWDCTRL             (RTI_REG_START + 0x90)  // Digital Watchdog Control. Enables the Digital Watchdog.
#define  RTIDWDPRLD             (RTI_REG_START + 0x94)  // Digital Watchdog Preload. Sets the experation time of the Digital Watchdog.
#define  RTIWDSTATUS            (RTI_REG_START + 0x98)  // Watchdog Status. Reflects the status of Analog and Digital Watchdog.
#define  RTIWDKEY               (RTI_REG_START + 0x9C)  // Watchdog Key. Correct written key values discharge the external capacitor.
#define  RTIDWDCNTR             (RTI_REG_START + 0xA0)  // Digital Watchdog Down-Counter

// Cache registers (In addition to the CSR core register)

#define L1PSAR *((volatile INT32U *)(MEMORY_N_CACHE_CONTROL_REG_START))
#define L1PICR *((volatile INT32U *)(MEMORY_N_CACHE_CONTROL_REG_START + 0x04))

#define L1P_INVALIDATE  (0x80000000u)

// CSR[7:5] controls the L1 Program Cache

#define CACHE_ENABLE    (0x00000040u)
#define CACHE_FREEZE    (0x00000060u)
#define CACHE_BYPASS    (0x00000080u)
#define CACHE_CTRL_MASK (0x000000E0u)
//-----------------------------------------------------------------------------------------------------------
#endif  // HW_TMS320C6727_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: hw_tms320c6727.h
\*---------------------------------------------------------------------------------------------------------*/
