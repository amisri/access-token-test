/*---------------------------------------------------------------------------------------------------------*\
  File:         property.h

  Purpose:      Property structures of the DSP

\*---------------------------------------------------------------------------------------------------------*/

#ifndef PROPERTY_H
#define PROPERTY_H

#include <cc_types.h>

struct prop
{
    INT32U              float_f;            // Data requires conversion from IEEE to TI FP format on FGC2 DSP.
    INT32U              flags;              // Property flags
    INT32U              el_size;            // Number of long words per element
    INT32U              n_elements;         // Number of elements of data in the property
    INT32U              max_elements;       // Max number of elements of data in the property
    INT32U              pars_idx;           // Parameter function index (1-32)
    void        *       value;              // Data buffer
};

extern struct prop prop[];

#endif // End of PROPERTY_H encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: property.h
\*---------------------------------------------------------------------------------------------------------*/
