/*---------------------------------------------------------------------------------------------------------*\
  File:        props.h

  Purpose:    FGC DSP global property variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PROPS_H   // Header encapsulation
#define PROPS_H

#ifdef PROPS_GLOBALS
#define PROPS_VARS_EXT
#else
#define PROPS_VARS_EXT extern
#endif

#include <cc_types.h>
#include <ref.h>        // will include libfg.h
#include <libreg.h>
#include <dpcom.h>
#include <defconst.h>   // for FGC_N_ADCS
#include <definfo.h>    // for FGC_CLASS_ID
#include <ppm.h>        // Required for defprops_dsp_fgc.h
#include <diag.h>
#include <histogram.h>

// DSP Property variables

struct property
{
    struct                                                      // ---------------------------------
    {
        INT32S          filter_mpx[FGC_N_ADCS];                 // ADC.FILTER.MPX
        struct adc_analog_bus
        {
            INT32S          signal;                             // Read/write property ADC.ANALOG_BUS.SIGNAL
            INT32U          swin;                               // Read-only  property ADC.ANALOG_BUS.SWIN
            INT32U          swbus;                              // Read-only  property ADC.ANALOG_BUS.SWBUS
        } analog_bus;
        INT32U        internal_signals[FGC_N_ADCS];             // ADC.INTERNAL.SIGNALS
        FP32          tau_temp;                                 // ADC.ADC16.TAU_TEMP
        FP32          temperature;                              // ADC.ADC16.TEMPERATURE
    } adc;
    struct                                                      // ---------------------------------
    {
        struct                                  // # = A or B
        {
            FP32        headerr;                // CAL.#.DCCT.HEADERR
            FP32        tc[3];                  // CAL.#.DCCT.TC
            FP32        dtc[3];                 // CAL.#.DCCT.DTC
        } dcct[2];
        struct
        {
            struct                              // # = A, B, C or D
            {
                INT32S  gain;                   // CAL.#.ADC.INTERNAL.GAIN
                FP32    tc[3];                  // CAL.#.ADC.INTERNAL.TC
                FP32    dtc[3];                 // CAL.#.ADC.INTERNAL.DTC
                INT32S  vraw[FGC_CAL_VRAW_LEN]; // CAL.#.ADC.INTERNAL.VRAW
            } internal[FGC_N_ADCS];
            struct                              // # = A or B
            {
                INT32S  gain;                   // CAL.#.ADC.EXTERNAL..GAIN
            } external[2];
        } adc;
        struct
        {
            FP32    err[6];                     // CAL.VREF.ERR
            FP32    tc[3];                      // CAL.VREF.TC
        } vref;

        FP32    ext_ref_err;                    // CAL.EXT_REF_ERR
        FP32    dac[FGC_N_DACS][3];             // CAL.#.DAC  (# = A, B)
    } cal;
    struct                                                      // ---------------------------------
    {
        INT32U          primary_turns;                          // DCCT.PRIMARY_TURNS
        FP32            tau_temp;                               // DCCT.TAU_TEMP
        FP32            gain[2];                                // DCCT.#.GAIN
        FP32            temperature[2];                         // DCCT.#.TEMPERATURE
        INT32U          position[2];                            // DCCT.#.POSITION
    } dcct;
    struct                                                      // ---------------------------------
    {
        struct
        {
            INT32U int_dsp_div;                                 // FGC.ITER_PERIOD.INT_DSP_DIV
        } iter_period;
        struct
        {
            struct
            {
                FP32  *  fpval;                                 // FGC.DEBUG.SPY.FPVAL
                INT32S * intval;                                // FGC.DEBUG.SPY.INTVAL
            } spy;

            histogram_t histogram_debug;                        // FGC.DEBUG.HISTOGRAM_DSP
        } debug;
        struct
        {
            INT32U    len_basic_per[FGC_MAX_USER_PLUS_1];       // FGC.CYC.LEN_BASIC_PER
        } event;
    } fgc;
    struct                                                      // ---------------------------------
    {
        INT32U  rt_bad_values;                                  // FIELDBUS.RT_BAD_VALUES
    } fieldbus;
    struct                                                      // ---------------------------------
    {
        struct
        {
            FP32        err_warning;                              // LIMITS.B.ERR_WARNING
            FP32        err_fault;                              // LIMITS.B.ERR_FAULT
        } b;
        struct
        {
            FP32        access;                                 // LIMITS.I.ACCESS
            FP32        earth;                                  // LIMITS.I.EARTH
            FP32        hardware;                               // LIMITS.I.HARDWARE
            FP32        meas_diff;                              // LIMITS.I.MEAS_DIFF
            FP32        closeloop       [FGC_N_LOADS];          // LIMITS.I.CLOSELOOP
            FP32        pos             [FGC_N_LOADS];          // LIMITS.I.POS
            FP32        min             [FGC_N_LOADS];          // LIMITS.I.MIN
            FP32        neg             [FGC_N_LOADS];          // LIMITS.I.NEG
            FP32        rate            [FGC_N_LOADS];          // LIMITS.I.RATE
            FP32        acceleration    [FGC_N_LOADS];          // LIMITS.I.ACCELERATION
            FP32        err_warning     [FGC_N_LOADS];          // LIMITS.I.ERR_WARNING
            FP32        err_fault       [FGC_N_LOADS];          // LIMITS.I.ERR_FAULT
            FP32        quadrants41     [2];                    // LIMITS.I.QUADRANTS41
            FP32        zero;                                   // LIMITS.I.ZERO
            FP32        low;                                    // LIMITS.I.LOW
            FP32        rms_warning;                            // LIMITS.I.RMS_WARNING
            FP32        rms_fault;                              // LIMITS.I.RMS_FAULT
            FP32        rms_tc;                                 // LIMITS.I.RMS_TC
        } i;
        struct
        {
            FP32        pos             [FGC_N_LOADS];          // LIMITS.V.POS
            FP32        neg             [FGC_N_LOADS];          // LIMITS.V.NEG
            FP32        min             [FGC_N_LOADS];          // LIMITS.V.MIN
            FP32        err_warning;                            // LIMITS.V.ERR_WARNING
            FP32        err_fault;                              // LIMITS.V.ERR_FAULT
            FP32        quadrants41     [2];                    // LIMITS.V.QUADRANTS41
        } v;
        struct
        {
            struct fg_limits    b;                              // LIMITS.OP.B.POS/RATE & LIMITS.B.POS/RATE
            struct fg_limits    i;                              // LIMITS.OP.I.POS/MIN/NEG/RATE/ACCELERATION
            FP32                i_available;                    // LIMITS.OP.I.AVAILABLE
            struct fg_limits    v;                              // LIMITS.OP.V.POS/NEG/RATE & LIMITS.V.RATE
        } op;
    } limits;
    struct                                                      // ---------------------------------
    {
        FP32            ohms_ser        [FGC_N_LOADS];          // LOAD.OHMS_SER
        FP32            ohms_par        [FGC_N_LOADS];          // LOAD.OHMS_PAR
        FP32            ohms_mag        [FGC_N_LOADS];          // LOAD.OHMS_MAG
        FP32            henrys          [FGC_N_LOADS];          // LOAD.HENRYS
        FP32            henrys_sat      [FGC_N_LOADS];          // LOAD.HENRYS_SAT
        FP32            i_sat_start     [FGC_N_LOADS];          // LOAD.I_SAT_START
        FP32            i_sat_end       [FGC_N_LOADS];          // LOAD.I_SAT_END
        FP32            gauss_per_amp   [FGC_N_LOADS];          // LOAD.GAUSS_PER_AMP
        INT32U          select;                                 // LOAD.SELECT
    } load;
    struct                                                      // ---------------------------------
    {
        struct
        {
            INT32U     status          [FGC_MAX_USER_PLUS_1];   // LOG.CYC.STATUS
            FP32       max_abs_err     [FGC_MAX_USER_PLUS_1];   // LOG.CYC.MAX_ABS_ERR
        } cyc;
    } log;
    struct                                                      // ---------------------------------
    {
        INT32U          sim;                                    // MEAS.SIM
        FP32            i_sim;                                  // MEAS.I_SIM
        FP32            i_rms;                                  // MEAS.I_RMS
        struct
        {
            FP32        u_leads[2];                             // MEAS.MAX.U_LEADS
        } max;
        FP32            acq_delay[FGC_MAX_USER_PLUS_1];         // MEAS.ACQ_DELAY
        FP32            acq_value[FGC_MAX_USER_PLUS_1];         // MEAS.ACQ_VALUE_AVG
        FP32            acq_value_avg_accum;
        INT32U          acq_value_avg_cnt;
#if FGC_CLASS_ID == 62        
        FP32            acq_pulses[FGC_MAX_USER_PLUS_1 * FGC_N_ACQ_PULSES];
#endif        
    } meas;
    struct                                                      // ---------------------------------
    {
        INT32U          rt;                                     // MODE.RT
    } mode;
    struct                                                      // ---------------------------------
    {
        FP32                ccv;                                // REF.CCV.VALUE
        FP32                vcv;                                // REF.VCV.VALUE
        INT32U              on_failure;                         // REF.ON_FAILURE
        INT32S              dac[FGC_N_DACS];                    // REF.DAC
        struct abs_time_us  run;                                // REF.RUN
        struct abs_time_us  abort;                              // REF.ABORT
        FP32                run_delay;                          // REF.RUN_DELAY
        struct
        {
            struct
            {
                FP32   data[FGC_N_CYC_CHK_DATA];               // REF.CYC.FAULT.DATA
            } fault;
        } cyc;

        struct
        {
            struct
            {
                FP32    acceleration;                           // REF.DEFAULTS.B.ACCELERATION
            } b;
        } defaults;
        struct
        {
            struct
            {
                FP32   starting        [FGC_N_LOADS];           // REF.RATE.I.STARTING
                FP32   to_standby      [FGC_N_LOADS];           // REF.RATE.I.TO_STANDBY
                FP32   stopping        [FGC_N_LOADS];           // REF.RATE.I.STOPPING
            } i;
        } rate;
        struct
        {
            FP32        vref;                                   // REF.START.VREF
            FP32        time;                                   // REF.START.TIME
            FP32        duration;                               // REF.START.DURATION
        } start;
        struct fg_plep_config           plep;                   // REF.PLEP.*
        struct fg_trim_config           trim;                   // REF.TRIM.FINAL/PERIOD
        struct fg_test_config           test;                   // REF.TEST.*
        struct fg_openloop_config       openloop;               // REF.OPENLOOP.FINAL
    } ref;
    struct                                                      // ---------------------------------
    {
        struct
        {
            INT32U                period_div;                   // REG.B.PERIOD_DIV
            FP32                  period;                       // REG.B.PERIOD     (read-only)
            struct reg_rst        rst;                          // REG.B.R/S/T ; REG.B.TRACK_DELAY
        } b;
        struct
        {
            INT32U                period_div[FGC_N_LOADS];      // REG.I.PERIOD_DIV
            FP32                  period    [FGC_N_LOADS];      // REG.I.PERIOD     (read-only)
            FP32                  clbw      [FGC_N_LOADS];      // REG.I.CLBW
            FP32                  clbw2     [FGC_N_LOADS];      // REG.I.CLBW2
            FP32                  z         [FGC_N_LOADS];      // REG.I.Z
            FP32                  pure_delay[FGC_N_LOADS];      // REG.I.PURE_DELAY
            struct reg_rst        manual_rst;                   // REG.I.MANUAL.R/S/T ; REG.I.MANUAL.TRACK_DELAY
            struct reg_rst_pars   op;                           // REG.I.OP.STATUS/R/S/T    (read-only)
        } i;
        struct
        {
            INT32U                period_div;                   // REG.V.PERIOD_DIV
            struct reg_rst_pars   rst_pars;                     // REG.V.PERIOD (read-only) ; REG.V.TRACK_DELAY
        } v;
        struct
        {
            INT32U                reset;                        // REG.TEST.RESET
            INT32U                user;                         // REG.TEST.USER
            INT32U                ref_user;                     // REG.TEST.REF_USER
            struct
            {
                INT32U                period_div;               // REG.TEST.B.PERIOD_DIV
                FP32                  period;                   // REG.TEST.B.PERIOD
                struct reg_rst        rst;                      // REG.TEST.B.R/S/T ; REG.TEST.B.TRACK_DELAY
            } b;
            struct
            {
                INT32U                period_div;               // REG.TEST.I.PERIOD_DIV
                FP32                  period;                   // REG.TEST.I.PERIOD
                struct reg_rst        rst;                      // REG.TEST.I.R/S/T ; REG.TEST.I.TRACK_DELAY
            } i;
        } test;
    } reg;

    struct                                                      // ---------------------------------
    {
        INT32U          mpx[FGC_N_SPY_CHANS];                   // SPY.MPX
    } spy;
    struct                                                      // ---------------------------------
    {
        struct
        {
            INT32S     test_int32s[FGC_DSP_TEST_PROP_LEN];      // TEST.DSP.INT32S
            FP32       test_float [FGC_DSP_TEST_PROP_LEN];      // TEST.DSP.FLOAT
        } dsp;
    } test;
    struct                                                      // ---------------------------------
    {
        FP32            offset;                                 // VS.OFFSET
        FP32            gain;                                   // VS.GAIN
        struct reg_sim_vs_pars sim;                             // VS.SIM.NUM/DEN
        FP32            ctrl_delay;                             // VS.SIM.CTRL_DELAY
        FP32            dac_clamp_pct;                          // VS.DAC_CLAMP_PCT (range -100 to +100)
        struct
        {
            FP32        gain;                                   // VS.I_LIMIT.GAIN
            FP32        ref;                                    // VS.I_LIMIT.REF
        } i_limit;
    } vs;
    struct                                                      // ---------------------------------
    {
        struct
        {
            FP32        gain;                                   // V_PROBE.CAPA.GAIN
            INT32U      position;                               // V_PROBE.CAPA.POSITION
        } capa;
        struct
        {
            FP32        gain;                                   // V_PROBE.LOAD.GAIN
            INT32U      position;                               // V_PROBE.LOAD.POSITION
        } load;
    } v_probe;
};

PROPS_VARS_EXT  struct property property;

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation PROPS_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp\inc\props.h
\*---------------------------------------------------------------------------------------------------------*/
