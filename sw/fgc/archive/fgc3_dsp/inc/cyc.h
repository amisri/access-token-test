/*---------------------------------------------------------------------------------------------------------*\
  File:         cyc.h

  Purpose:      FGC2 Class 51/53 DSP Cycling state variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CYC_H   // Header encapsulation
#define CYC_H

#ifdef CYC_GLOBALS
#define CYC_VARS_EXT
#else
#define CYC_VARS_EXT extern
#endif

// Specific macros for data buffer that are to be mapped in external RAM

#ifdef CYC_GLOBALS_FAR
#define CYC_VARS_EXT_FAR
#else
#define CYC_VARS_EXT_FAR extern
#endif

/*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>
#include <dpcom.h>
#include <defconst.h>   // for FGC_MAX_USER_PLUS_1
#include <mcu_dsp_common.h>
#include <dsp_time.h>   // For struct fp_time
#include <ref.h>
#include <meas.h>

/*---------------------------------------------------------------------------------------------------------*/
#define TRIGG_LOG       TRUE
#define DONT_TRIGG_LOG  FALSE

// Default list

#if (FGC_CLASS_ID == 61)
#define STRETCHED_CYC_WARNINGS (  FGC_WRN_TIMING_EVT     \
                                  | FGC_WRN_CYCLE_START   \
                                  | FGC_WRN_I_MEAS        \
                                  | FGC_WRN_REG_ERROR )
#elif (FGC_CLASS_ID == 62)
#define STRETCHED_CYC_WARNINGS (  FGC_WRN_TIMING_EVT     \
                                  | FGC_WRN_I_MEAS        \
                                  | FGC_WRN_REG_ERROR )
#endif


#define MAX_CONSECUTIVE_WRN     5                       // Maximum number of consecutive warnings without a
// clean cycle. Reaching that number causes a fault,
// FGC_CYC_FLT_CYC_WARN_LIM, and should end cycling.

// Exceptions to the consecutive warnings fault mechanism. Those warnings are not counted.

#define CONSECUTIVE_WRN_IGNORE ( FGC_CYC_WRN_SC_EVT_EARLY | FGC_CYC_WRN_REF_NOT_ARMED | FGC_CYC_WRN_REF_DISABLED)

// [FGC3] Enumeration used for variable cyc.next_cycle_state.
// This enumeration represents the states that the DSP must go through in sequence to prepare for a new
// cycle. The user event is received 200ms prior to the start of a new cycle. On C0 (start of cycle), there
// is a cycle check to assert that cyc.next_cycle_state is NEXT_CYCLE_READY.

enum next_cycle
{
    NEXT_CYCLE_WAIT_USER_EVENT,                 // Wait for the user event
    NEXT_CYCLE_PREPARE_REF,                     // Prepare the reference function    for the next cycle
    NEXT_CYCLE_PREPARE_REG,                     // Prepare the regulation parameters for the next cycle
    NEXT_CYCLE_PREPARE_REG_REQUEST,             // Reg request sent
    NEXT_CYCLE_READY                            // Next cycle is ready
};

// [FGC2] Enumeration for the state of the copy of the reference function from the ppm->armed structure to the
// ppm->cycling. That copy will occur during ms 0 and ms 1 at the beginning of a new cycle, if the armed reference
// has changed for the cycle user.

/*---------------------------------------------------------------------------------------------------------*/

// A note on cyc.timing_user, cyc.user, cyc.ref_user. On a PPM device usually all three are equal, but
// they can differ in some situations:
//
// cyc.timing_user = the user received from the external timing (e.g. PS timing), in NORMAL     mode
//                 = the simulated user received from the MCU,                    in SIMULATION mode
//
// cyc.user        = cyc.timing_user, if DEVICE.PPM == ENABLED
//
// cyc.ref_user    = REG.TEST.REF_USER, if cyc.user == REG.TEST.USER && REG.TEST.REF_USER != 0
//                 = cyc.user,          otherwise
//

struct cyc
{
    struct abs_time_us  start_time;                     // Time of start of this cycle
    struct abs_time_us  start_time_next;                // Time of start of the next cycle
    struct fp_time      time;                           // Time since start of cycle (s)
    INT32U              timing_user;                    // Cycle user index from timing
    INT32U              user;                           // Cycle user index (always zero if PPM disabled)
    INT32U              ref_user;                       // Cycle user index for reference function
    INT32U              prev_user;                      // Previous cycle user index (always zero if PPM disabled)
    INT32U              next_user;                      // Next cycle user index (always zero if PPM disabled)
    INT32U              next_ref_user;                  // Next cycle user index for reference function
    INT32U              ref_func_type;                  // Function type for the cycle
    INT32U              user_evt_f;                     // Next cycle mux event received
    INT32U              ssc_evt_f;                      // Start super-cycle event received this cycle
    enum next_cycle     next_cycle_state;               // State of the next cycle (Only used on FGC3)
    BOOLEAN             trigger_bgp_prepare_ref_f;      // Trigger the BGP task to prepare the reference function
    BOOLEAN             start_cycle_f;                  // True during one iteration at the start of the cycle
    INT32U              biv_check_f;                    // BIV checks enabled flag
    FP32                biv_check_time;                 // Time for BIV checks (s)
    INT32U              stretched_warnings;             // Check warnings (stretches to >= 1 supercycle)
#if defined(__TMS320C32__)
    // ToDo check this, was unsigned int but used as INT16U, this is why I put the #else
    INT32U              dysfunction_code;               // Cycle warning or fault
    INT32U              dysfunction_is_a_fault;         // warning (0) or fault (1)
    INT32U              consecutive_wrn_cnt;            // Count the number of consecutive warnings
#else
    INT16U              dysfunction_code;               // Cycle warning or fault
    INT16U              dysfunction_is_a_fault;         // warning (0) or fault (1)
    INT16U              consecutive_wrn_cnt;            // Count the number of consecutive warnings
#endif
};

void            CycEnter(void);
void            CycExit(void);
void            CycCancelNextUser(void);
void            CycStartSuperCycle(void);
void            CycStartCycle(void);
BOOLEAN         CycPrepareRefFunction(void);
INT32U          CycEnableRefFunction(void);
void            CycSendMCUCheckResults(void);
void            CycResetCheckWarning(INT32U user);
void            CycResetCheckFault(void);
void            CycSaveCheckWarning(INT32U trig_log_capture_f, INT32U check, INT32U warning, FP32 data0,
                                    FP32 data1, FP32 data2, FP32 data3);
void            CycSaveCheckFault(INT32U check, INT32U warning, FP32 data0, FP32 data1, FP32 data2,
                                  FP32 data3);
void            CycLogAbsMaxErr(void);


/*---------------------------------------------------------------------------------------------------------*/

CYC_VARS_EXT struct cyc         cyc;

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation CYC_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cyc.h
\*---------------------------------------------------------------------------------------------------------*/
