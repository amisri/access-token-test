/*---------------------------------------------------------------------------------------------------------*\
  File:         refto.h

  Purpose:      FGC DSP Ref change to new functions
\*---------------------------------------------------------------------------------------------------------*/

#ifndef REFTO_H   // Header encapsulation
#define REFTO_H

#ifdef REFTO_GLOBALS
#define REFTO_VARS_EXT
#else
#define REFTO_VARS_EXT extern
#endif

#include <cc_types.h>
#include <ppm.h>

struct refto
{
    struct
    {
        BOOLEAN   standby;                      // Going to standby  state flag
        BOOLEAN   stopping;                     // Going to stopping state flag
        BOOLEAN   aborting;                     // Going to aborting state flag
        BOOLEAN   end;                          // Going to end      state flag (used in cycling operation only)
    } flags;
};

REFTO_VARS_EXT struct refto refto;

void            RefToStandbyToOpenloop(void);
void            RefToEnd(void);
void            RefToAborting(void);
void            RefToStandby(void);
void            RefToStopping(void);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation REFTO_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: refto.h
\*---------------------------------------------------------------------------------------------------------*/
