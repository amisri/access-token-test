/*---------------------------------------------------------------------------------------------------------*\
  File:         rtcom.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef RTCOM_H // header encapsulation
#define RTCOM_H

#ifdef RTCOM_GLOBALS
#define RTCOM_VARS_EXT
#else
#define RTCOM_VARS_EXT extern
#endif

#include <cc_types.h>
#include <dpcom.h>                              // For struct abs_time_us, struct abs_time_ms

//-----------------------------------------------------------------------------------------------------------

// Periodic interrupt RT processing variables

struct  rtcom
{
    //  INT32U            class_id;                       // Class ID
    INT32U            par_group_set_mask;             // Parameter group set mask
    INT32U            ppm_start;                      // Start of PPM property structure
    INT32U            ppm_end;                        // End of PPM property structure
    INT32U            ppm_length;                     // Length of PPM property structure

    // Time and watchdog

    struct abs_time_ms  time_now_ms;                  // Time now in ms
    struct abs_time_us  time_now_us;                  // Time now in us

    INT32U            led_counter;                    // Millisecond counter for FP LED
    INT32U            ms10;                           //   time_now_ms.ms_time%10
    INT32U            ms20;                           //   time_now_ms.ms_time%20
    INT32U            ms200;                          //   time_now_ms.ms_time%200

    BOOLEAN           start_of_ms_f;                  // Flag for the start of a new millisecond
    BOOLEAN           start_of_ms10_f;                // start_of_ms_f && (time_now_ms.ms_time%10  == 0)
    BOOLEAN           start_of_ms20_f;                // start_of_ms_f && (time_now_ms.ms_time%20  == 0)
    BOOLEAN           start_of_ms200_f;               // start_of_ms_f && (time_now_ms.ms_time%200 == 0)
    BOOLEAN           start_of_second_f;              // start_of_ms_f && (time_now_ms.ms_time     == 0)
    INT32U            cpu_usage;                      // DSP RT cpu usage
    INT32U            start_freerun_us;               // free running us counter value at start of ISR
    INT32U            last_start_freerun_us;          // free running us counter value at start of last ISR
    INT32U            dsp_utc_us;                     // last UTC us of previous ISR
    INT32U            last_dsp_utc_us;                // last UTC us of previous ISR

    // States and modes, faults and warnings

    INT32U            state_op;                       // OP state machine
    INT32U            faults;                         // Fault flags
    INT32U            warnings;                       // Warning flags
    INT32U            st_unlatched;                   // Unlatched status flags
    INT32U            st_latched;                     // Latched status flags
    INT32U            interfacetype;                  // Slot 5 interface type
};

//-----------------------------------------------------------------------------------------------------------

RTCOM_VARS_EXT struct rtcom    rtcom;

//-----------------------------------------------------------------------------------------------------------

#endif  // RTCOM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rtcom.h
\*---------------------------------------------------------------------------------------------------------*/
