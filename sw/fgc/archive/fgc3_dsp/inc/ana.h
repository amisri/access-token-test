/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp/fgc3/inc/ana.h

  Purpose:      FGC3 Analogue card driver
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ANA_H   // Header encapsulation
#define ANA_H

#ifdef ANA_GLOBALS
#define ANA_VARS_EXT
#else
#define ANA_VARS_EXT extern
#endif

#include <stdlib.h>
#include <cc_types.h>
#include <defconst.h>

// ANA constants and enums

// Analogue card ADC mask selectors

#define ANA_MASK_NO_ADC         (0)
#define ANA_MASK_ADC1           (ANA_SWBUS_IA_MASK8)                                            // 0x0001
#define ANA_MASK_ADC2           (ANA_SWBUS_IB_MASK8)                                            // 0x0002
#define ANA_MASK_ADC3           (ANA_SWBUS_VS_MASK8)                                            // 0x0004
#define ANA_MASK_ADC4           (ANA_SWBUS_AUX_MASK8)                                           // 0x0008
#define ANA_MASK_ALL_ADCS       (ANA_MASK_ADC1|ANA_MASK_ADC2|ANA_MASK_ADC3|ANA_MASK_ADC4)       // 0x000F
#define ANA_IDX_ADC1            (0)
#define ANA_IDX_ADC2            (1)
#define ANA_IDX_ADC3            (2)
#define ANA_IDX_ADC4            (3)

#define ANA_IDX_TO_MASK(idx) (1 << idx)

// Analogue card DAC selectors

#define ANA_IDX_DAC1      (0)
#define ANA_IDX_DAC2      (1)

// Analogue bus input selector
// Note1: On FGC3 the analogue bus is always connected to the COM port on the front panel, and the later can
//        always be used to spy whatever signal is connected to the analogue bus. Yet there can be use cases
//        were we need the COM port to be used to measure an input signal. The enum value ANABUS_INPUT_COM
//        was added for that purpose. And it is also the reason why ANABUS_INPUT_NONE is a different enum.
// Note2: Symbol list "INTERNAL_ADC_MPX" MUST be aligned with the enum below.

typedef enum analogue_bus_input
{
    ANABUS_INPUT_NONE,                      // The nominal case: no signal goes to the analogue bus
    ANABUS_INPUT_COM,                       // Use the COM port (on the front panel) as an input
    ANABUS_INPUT_CH_A,                      // Input channel A
    ANABUS_INPUT_CH_B,                      // Input channel B
    ANABUS_INPUT_CH_C,                      // Input channel C
    ANABUS_INPUT_CH_D,                      // Input channel D
    ANABUS_INPUT_MPX_RANGE_START,
    ANABUS_INPUT_CH1_MINUS = ANABUS_INPUT_MPX_RANGE_START,
    ANABUS_INPUT_CH2_MINUS,
    ANABUS_INPUT_DAC2,
    ANABUS_INPUT_DAC1,
    ANABUS_INPUT_UNUSED,                    // In fact this is a redundant GNDSENSE
    ANABUS_INPUT_CAL_ZERO,                  //   0V, a.k.a. GNDSENSE
    ANABUS_INPUT_CAL_POSREF,                // +10V
    ANABUS_INPUT_CAL_NEGREF,                // -10V
    ANABUS_INPUT_MAX_VALUE                  // Not a valid input, used for range check
} ANALOGUE_BUS_INPUT;

// Default value for the multiplexer selector if the multiplexor is disabled,
// in that case we want to set the multiplexer selector equal to CAL_ZERO (i.e. GNDSENSE).

#define ANA_MPX_SEL_DEFAULT (ANABUS_INPUT_CAL_ZERO - ANABUS_INPUT_MPX_RANGE_START)

// ANA variables

struct ana
{
    INT32U              type;                           // Analogue interface type (ANA-101, ANA-102...)

    struct saved_mpx                                    // Structure used in AnaSaveMpx()/AnaRestoreMpx()
    {
        BOOLEAN             in_use_f;                   // Flag indicating if a MPX context is saved
        INT8U               swin;
        INT8U               swbus;
        INT8U               mpx;
        BOOLEAN             mpx_enable;
        INT16U              modmpx[FGC_N_ADCS];
    } saved;
};

ANA_VARS_EXT struct ana ana;

// ANA function declarations

void    AnaInit(void);
BOOLEAN AnaConfigSetMpx(INT8U swin, INT8U swbus, BOOLEAN mpx_enable, INT8U mpx);
void    AnaConfigAnalogueBus(ANALOGUE_BUS_INPUT input, INT8U adcs_mask);
void    AnaProcessMpxRequest(void);
void    AnaUpdateAdcProperties(void);
void    AnaSetModMpx(void);
void    AnaSaveMpx(void);
void    AnaRestoreMpx(void);
void    AnaDacSet(INT32U dac_idx, INT32S dac_raw);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation ANA_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp/fgc3/inc/ana.h
\*---------------------------------------------------------------------------------------------------------*/
