/*!
 *  @file      isr.h
 *  @defgroup  FGC3:DSP
 *  @brief     FGC3 DSP interrupt declaration
 */

#ifndef FGC_ISR_H
#define FGC_ISR_H

#ifdef ISR_GLOBALS
#define ISR_VARS_EXT
#else
#define ISR_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <reg.h>
#include <sim.h>
#include <ref.h>
#include <cal.h>
#include <mcu_dsp_common.h>

// A parameter switch is when the value of the two pointers *isr_switch_ptrs.active and *isr_switch_ptrs.next are swapped

struct isr_switch_ptrs
{
    void ** active;
    void ** next;
};

// Parameter switch flags

enum
{
    ISR_SWITCH_ENUM_LOAD = 0,
    ISR_SWITCH_ENUM_SIM,
    ISR_SWITCH_ENUM_REF,
    ISR_SWITCH_ENUM_REG,
    ISR_SWITCH_ENUM_CAL,
    ISR_SWITCH_MAX_VALUE
};

// Corresponding bitmasks

#define ISR_SWITCH_LOAD         (1 << ISR_SWITCH_ENUM_LOAD)
#define ISR_SWITCH_SIM          (1 << ISR_SWITCH_ENUM_SIM)
#define ISR_SWITCH_REF          (1 << ISR_SWITCH_ENUM_REF)
#define ISR_SWITCH_REG          (1 << ISR_SWITCH_ENUM_REG)
#define ISR_SWITCH_CAL          (1 << ISR_SWITCH_ENUM_CAL)

// Corresponding pair of parameter sets

ISR_VARS_EXT const struct isr_switch_ptrs isr_switch_ptrs[32]
#ifdef ISR_GLOBALS
        =
{
    /*  0 - ISR_SWITCH_ENUM_LOAD */     { (void **) & load.active, (void **) & load.next              },
    /*  1 - ISR_SWITCH_ENUM_SIM  */     { (void **) & sim.active, (void **) & sim.next               },
    /*  2 - ISR_SWITCH_ENUM_REF  */     { (void **) & ref.active, (void **) & ref.next               },
    /*  3 - ISR_SWITCH_ENUM_REG  */     { (void **) & reg.active, (void **) & reg.next               },
    /*  4 - ISR_SWITCH_ENUM_CAL  */     { (void **) & cal.active, (void **) & cal.next               }
}
#endif
;

// Mask of the switch requests for the ISR task.
//  - Once a switch is active, then the BGP must not modify the corresponding "next" structure
//    (e.g. if ISR_SWITCH_REG mask is set, BGP must not edit reg.next)
//  - The ISR shall never use (read or write) the "next" structures, only the "active" ones.
//  - Those flags are automatically cleared after the switch has been performed by the ISR.

ISR_VARS_EXT INT32U isr_switch_request_flags
#ifdef ISR_GLOBALS
    = 0
#endif
      ;

// Mask used in cycling operation to indicated that a parameter switch request (in isr_switch_request_flags)
// is only to happen at the beginning of the next cycle (C0).
// Those flags are automatically cleared after the switch has been performed by the ISR.

ISR_VARS_EXT INT32U isr_switch_cycling_flags
#ifdef ISR_GLOBALS
    = 0
#endif
      ;

enum isr_switch_type
{
    ISR_SWITCH_REGULAR_ISR,
    ISR_SWITCH_START_OF_CYCLE
};

// Platform/class specific functions

void IsrClassInit(void);
void IsrClassMain(void);

// External function declarations

/*!
 * This function is the Interrupt Service Routine for the CLK_DSP external
 * interrupt request.  This will be triggered at the start of each DSP
 * regulation period.
 */
#pragma NMI_INTERRUPT (IsrPeriod);
void IsrPeriod(void);

/*!
 * This function is used as the target for all unexpected interrupts.
 */
#pragma INTERRUPT (IsrTrap);
void IsrTrap(void);

/*!
 * This function is used by Class 51/53/61 for the main part of the real-time
 * ISR processing.
 */
void IsrMain(void);

/*!
 * This function converts the DAC voltage parameter into a raw value.
 *
 * If there is an on-going calibration of the DAC, the DAC value is not modified.
 */
void IsrDacSet(INT32U dac_idx, FP32 v_dac);

/*!
 * This function processes the direct DAC reference from the MCU to add a scanning
 * functionality to allow DAC testing.
 *
 * The normal DAC range is effectively +/-524287, but this function support special
 * processing if the user supplies a value >1000000 or less than -1000000. In this
 * case, the excess is used as an increment/decrement value.
 */
void IsrDacDirect(INT32U dac_idx, INT32S ref_dac);

/*!
 * This function handles the logging of the FGC signals (SPY, LOG.CAPTURE, LOG.OASIS).
 *
 * This function is mainly used with POPS: At the start of the first plateau of a PPPL reference,
 * corresponding to the injection, a fake marker is inserted on the reference (+1A for a current
 * reference, +1G for a field reference), lasting 1ms.
 */
void IsrLogging(void);

/*!
 * This function is called to treat a request from the BGP task to switch ISR parameter
 * structures, namely the regulation, load, or simulation variables.
 *
 * For example if:
 *
 *    load.active = &load_param[0];
 *    load.next   = &load_param[1];
 *
 * and, ISR_SWITCH_LOAD is set in isr_switch_request_flags, then after calling this function, providing
 * the switch is permitted by the input argument isr_switch_type, we will get:
 *
 *    load.active = &load_param[1];
 *    load.next   = &load_param[0];
 *
 * Meaning the values of the load.active and load.next pointers were swapped.
 *
 * A reminder on the usage of the "active" and "next" data structures:
 *
 *    - The "next" data structure (for example, load.next) is work-in-progress of the BGP task. The ISR
 *      shall NEVER read or write load.next or any of the data structures pointed by "next".
 *
 *    - The BGP task shall NEVER write a "next" structure if the corresponding switch bit is set in
 *      isr_switch_request_flags. For example, the BGP task should not modify the structure pointed by
 *      load.next if Test(isr_switch_request_flags, ISR_SWITCH_LOAD) is true. In that case, the BGP must
 *      wait for the ISR to do the switching. Once the bit gets cleared, the BGP task can edit load.next.
  */
void IsrSwitchParameters(enum isr_switch_type isr_switch_type);

#endif

// EOF
