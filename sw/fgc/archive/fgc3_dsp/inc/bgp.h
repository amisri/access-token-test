/*!
 *  @file      bgp.h
 *  @defgroup  FGC3:DSP
 *  @brief     FGC DSP Software - Background Processing Functions
 *
 *  The DSP code does not run under an operating system. There is just the
 *  background processing level, and interrupt level. The background processing
 *  is used for operations that may take longer than about 400us. These
 *  background functions are used to process reference function arm/disarm
 *  requests from the MCU.
 */

#ifndef FGC_BGP_H
#define FGC_BGP_H

#ifdef BGP_GLOBALS
#define BGP_VARS_EXT
#else
#define BGP_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <libfg.h>

// External structures, unions and enumerations

typedef struct bgp
{
    struct bgp_trigger
    {
        BOOLEAN         calibration_refresh;
        BOOLEAN         calibration_process;
        BOOLEAN         one_hertz_processing;
        BOOLEAN         process_200ms_filter_and_report;
        BOOLEAN         process_1s_filter_and_report;
        INT32S          log_oasis_latest_user;                  // Write -1 to disable the trigger
    } trigger;
} bgp_t;

// External variable definitions

BGP_VARS_EXT bgp_t bgp;
BGP_VARS_EXT BOOLEAN bgp_no_pending_action;

// Platform/class specific functions

typedef enum fg_error(*bgp_check_Iref_func)(struct fg_limits * limits,
                                            uint32_t invert_limits,
                                            float ref, float rate,
                                            float acceleration);

/*!
 * This function returns a pointer to the BgpCheckIRef function if
 * implemented or NULL otherwise.
 */
bgp_check_Iref_func BgpGetCheckIref(void);

/*!
 * This function is called if the MCU has set the ref arm flag to request that
 * a new reference function be armed (or disarmed if NONE).
 *
 * This is done in the background process because it can take many milliseconds
 * and would over-run the IsrMst().
 */
void BgpRefChange(INT32U user);

/*!
 * This function is called if the user cancels a reference with
 * S REF.FUNC.TYPE(user) NONE or if the MCU or DSP needs to cancel a
 * reference for other reasons.
 */
INT32U BgpInitNone(INT32U reg_mode, INT32U user);

// External function declarations

/*!
 * This is the Background Processing Loop.
 */
void BgpLoop(void);

/*!
 * It processes the 200ms filter on the ADC measurements.
 */
void BgpFilter200ms(void);

/*!
 * It processes the 1s filter on the ADC measurements
 */
void BgpFilter1s(void);

/*!
 * Translates an error to be safe in case enum fg_errno is modified and the
 * numbers change.
 */
INT32U BgpTranslateFgError(enum fg_error fg_error);

#endif

// EOF

