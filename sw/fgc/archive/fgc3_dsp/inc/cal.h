/*---------------------------------------------------------------------------------------------------------*\
  File:         fgc3\inc\cal.h

  Purpose:      FGC3 Class 61 DSP calibration variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CAL_H   // Header encapsulation
#define CAL_H

#ifdef CAL_GLOBALS
#define CAL_VARS_EXT
#else
#define CAL_VARS_EXT extern
#endif

#include <stdlib.h>
#include <cc_types.h>
#include <dpcls.h>
#include <libcal.h>
#include <main.h>
#include <defconst.h>

// ----- Calibration constants and enums -----

#define CAL_NUM_SAMPLES_PER_AVE_STEP       250          // Average step = 40 samples * 250 = 10,000 = 1 s
#define CAL_SAMPLES_BEFORE_SIGNAL_ACQ      50000        // Num of samples to wait after the calibration signal = 5 s
// is set before starting the samples acquisition.

#define CAL_ADC_FOR_DAC_MEAS               ANA_IDX_ADC4 // Internal ADC used for DAC calibration. DACs can be
// calibrated one at a time and ADC4 has no operational
// use on FGC3 for the moment.

enum cal_process_st                             // Calibration process steps (or state)
{
    CAL_REQUEST_ARMED,                          // Calibration process is ready to start
    CAL_REQUEST_PREPARE,                        // Prepare HW for calibration
    CAL_REQUEST_WAIT,                           // Wait to acquire samples
    CAL_REQUEST_ACQUIRE_SAMPLES,                // Acquire samples
    CAL_REQUEST_SAMPLES_READY,                  // Samples are ready
    CAL_REQUEST_TERMINATE,                      // HW is restored to its initial state
    CAL_REQUEST_HIDE_END_PROBLEM,               // ToDo: this need to be fixed and removed
    CAL_REQUEST_DONE                            // Calibration process has completed (redundant with cal.ongoing_f)
};

// ----- Calibration variables -----


struct cal_lim                                          // Calibration error limits
{
    FP32                mid;                            // Middle of acceptable range (ppm)
    FP32                wrn;                            // Warning threshold is mid +/- wrn
    FP32                flt;                            // Fault threshold is   mid +/- flt
};

struct cal_request                                  // A generic structure for calibration requests
{
    BOOLEAN             from_mcu_f;                 // Is the request coming from the MCU?
    enum cal_action     action;                     // Calibration action
    enum cal_idx        idx;                        // Signal type (OFFSET/POS/NEG), only for calibration
    // of external ADC or DCCT.
    INT32U              ave_steps;                  // Averaging period in 200-sample steps
    INT8U               chan_mask;                  // Channels to calibrate
    INT8U               adc_mask;                   // ADCs used for the calibration (except for DAC
    // calibration, this is equal to request.chan_mask)
    enum cal_process_st state;                      // Request state machine

};

// The calibration factors are the ADC/DAC factors used by the ISR to calibrate the measurements
// We use an active/next scheme where the calibrations factors are duplicated in memory so that
// the ISR can use one set of factors while the BGP task is calculating new factors. The ISR will
// switch between the "active" and "next" parameter sets whenever necessary.

struct cal_factors
{
    struct cal_adc          adc_factors[FGC_N_ADCS];    // ADC  calibration at ambient temperature
    struct cal_dcct         dcct_factors[2];            // DCCT calibration at ambient temperature
    struct cal_dac          dac_factors[FGC_N_DACS];    // DAC  calibration (no temperature compensation)
    struct cal_v_meas       v_meas_gain[2];             // Vmeas gain (for ADC3 and possibly ADC4)
};

CAL_VARS_EXT struct cal_factors cal_factors[2];                     // active and next calibration factors

struct cal
{
    INT32U              imeas_zero_f;                   // Imeas zeroed request from MCU
    INT32U              step;                           // 200ms step number for calibration
    INT32S              acc_raw_a;                      // Raw 200ms value accumulator for chan A
    INT32S              acc_raw_b;                      // Raw 200ms value accumulator for chan B
    FP32                t_adc_C;                        // Filtered ADC16 temperature         (units: C)
    FP32                t_dcct_C[2];                    // Filtered DCCT electronics temperature (units: C)
    DP_IEEE_FP32        days;                           // Date stamp for calibration (days since 1-1-1970)
    DP_IEEE_FP32        seconds;                        // Time stamp for calibration (secs since midnight)
    BOOLEAN             ongoing_dac_cal[FGC_N_DACS];    // Flag indicating an ongoing calibration of the DAC

    // --- Current calibration coefficients ---

    BOOLEAN                     is_internal_adc[FGC_N_ADCS];    // Flag indicating if the ADC is internal/external
    const INT32S        *       adc_nominal_gain       [FGC_N_ADCS];
    const struct cal_event   *  normalized_adc_factors [FGC_N_ADCS];    // ADC  calibration factors at T0
    const struct cal_event   *  normalized_dcct_factors[2];             // DCCT calibration factors at T0

    struct cal_factors     *    active;                         // Pointer to the active calibration factors
    struct cal_factors     *    next;                           // Pointer to the next set of calibration factors

    // --- Calibration coefficients update ---

    BOOLEAN                     ongoing_f;                      // If TRUE, indicates a calibration is on-going
    // using the ADCs cal.request.adc_mask

    struct cal_request          request;
    struct cal_average_v_raw    average_v_raw[FGC_N_ADCS];      // Structures used to compute the average
    // Vraw during calibration
};

CAL_VARS_EXT struct cal cal;

// Dummy cal_v_meas structure used for ADC_INPUT_DIRECT type of measurement. The voltage is measured directly
// at the input of the ADC (Vadc), i.e. this is a special case of voltage measurement with a gain equal to 1.

CAL_VARS_EXT const struct cal_v_meas  CAL_DIRECT_V_MEAS
#ifdef CAL_GLOBALS
        =
{
    1.0,                        // gain = 1
    1.0                         // inv_gain
}
#endif
;

// Dummy cal_event structure used for the FILTER test patterns. That structure represents an ideal ADC.

CAL_VARS_EXT const struct cal_event CAL_PERFECT_ADC
#ifdef CAL_GLOBALS
        =
{
    0.0,                      // offset_ppm
    0.0,                      // gain_err_pos_ppm
    0.0,                      // gain_err_neg_ppm
    CAL_TEMP_T0,              // temp_c (temperature when calibrated)
    0.0,                      // date_days (calibrated on January 1, 1970. Perfect since then)
    0.0                       // time_s
}
#endif
;

// TODO FGC3 DSP. The ADC gain below is in fact CAL_DEF_ADC16_GAIN, which for now is only visible by the MCU
// Gain = Vadc/Vnominal, Vnominal = 10V
// This is a theoretical gain of a perfect ADC, plus the filter function

CAL_VARS_EXT const INT32S CAL_PERFECT_ADC_NOMINAL_GAIN
#ifdef CAL_GLOBALS
    = CAL_DEF_INTERNAL_ADC_GAIN
#endif
      ;

// ----- Function declarations -----

void            CalInit(void);
void            CalCalc();
void            CalProcess(void);
void            CalCalcAdc(INT32U c, FP32 * err, INT32S gain, FP32 * tc, FP32 * dtc,
                           struct cal_lim cal_lim[3]);
BOOLEAN         CalNewRequest(BOOLEAN from_mcu_f, enum cal_action action, enum cal_idx idx,
                              INT32U ave_steps, INT32U chan_mask);
void            CalAcqSamples(void);
enum cal_idx    CalTranslateCpuIdx(INT32U idx);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation CAL_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: fgc3\inc\cal.h
\*---------------------------------------------------------------------------------------------------------*/
