/*---------------------------------------------------------------------------------------------------------*\
  File:         fgc3\inc\adc.h
  Purpose:      FGC3 ADC support
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ADC_H   // Header encapsulation
#define ADC_H

#ifdef ADC_GLOBALS
#define ADC_VARS_EXT
#else
#define ADC_VARS_EXT extern
#endif

#include <cc_types.h>
#include <libcal.h>
#include <defconst.h>   // for FGC_N_ADCS
#include <main.h>       // for MAX_ITERS_PER_MS
#include <ana.h>

//
// ADC constants
//

#define FLT_CLIP                36000000        // Filter clip level
#define FLT_MAX_INT             9000000         // 100% value for internal ADC
#define FLT_MAX_EXT             30000000        // 100% value for external ADC

#define FILTER_20MS_MAX_LEN             ( 20*DSP_MAX_ITERS_PER_MS)      // 20ms filter: max size of the circular buffer
#define VRAW_SUM_HIST_LEN               (100*DSP_MAX_ITERS_PER_MS)      // 20ms filter: length of the Vraw sum history

// Mask of the channel status flags that can trigger a reset of the ADC filters

#define ADC_RESET_STATUS_MASK (FGC_ST_ADC_SIGNAL_STUCK)

#define ADC_MIN_DSP_RESET_DELAY_S       120             // Minimum delay between two DSP-triggered ADC resets (s)

// ADC input selector: that enumeration is used to specify what an ADC is measuring:
//  - The voltage directly at its input (ADC_INPUT_DIRECT). This is the case during calibration for example.
//  - An external voltage (ADC_INPUT_V_MEAS_LOAD). In that case a measurement gain (Vmeas/Vadc) is applied.
//  - The current in the magnet, thanks to a DCCT (ADC_INPUT_I_DCCT_A, ADC_INPUT_I_DCCT_B). In that case, DCCT gain and calibration
//    coefficients are also part of the measurement chain.

enum adc_input_type
{
    ADC_INPUT_NONE,         // No input
    ADC_INPUT_DIRECT,       // Direct input:                    Vadc -> Vraw
    ADC_INPUT_V_MEAS_LOAD,  // V load measurement:     Vmeas -> Vadc -> Vraw
    ADC_INPUT_V_MEAS_CAPA,  // V capa measurement:     Vmeas -> Vadc -> Vraw
    ADC_INPUT_I_DCCT_A,     // I DCCT:        Idcct -> Vdcct -> Vadc -> Vraw
    ADC_INPUT_I_DCCT_B,     // I DCCT:        Idcct -> Vdcct -> Vadc -> Vraw
};

struct adc_input_type_and_index
{
    enum adc_input_type input_type;
    INT32U              input_idx;
};

// The following array is used to map an input signal (based on enum analogue_bus_input) to a couple input_type/input_idx.
// It is intended for the INTERNAL_ADC multiplexing.

// Array index is of type enum analogue_bus_input.
ADC_VARS_EXT const struct adc_input_type_and_index adc_input_type_and_index[]
#ifdef ADC_GLOBALS
            =
                //          input_type, input_idx
{
    { ADC_INPUT_DIRECT,         0 },                    //  0 - ANABUS_INPUT_NONE
    { ADC_INPUT_DIRECT,         0 },                    //  1 - ANABUS_INPUT_COM
    { ADC_INPUT_I_DCCT_A,       0 },                    //  2 - ANABUS_INPUT_CH_A              <--->    DCCT_A
    { ADC_INPUT_I_DCCT_B,       1 },                    //  3 - ANABUS_INPUT_CH_B              <--->    DCCT_B
    { ADC_INPUT_V_MEAS_LOAD,    0 },                    //  4 - ANABUS_INPUT_CH_C              <--->    VS LOAD
    { ADC_INPUT_V_MEAS_CAPA,    1 },                    //  5 - ANABUS_INPUT_CH_D              <--->    VS CAPA
    { ADC_INPUT_DIRECT,         0 },                    //  6 - ANABUS_INPUT_CH1_MINUS
    { ADC_INPUT_DIRECT,         0 },                    //  7 - ANABUS_INPUT_CH2_MINUS
    { ADC_INPUT_DIRECT,         0 },                    //  8 - ANABUS_INPUT_DAC2
    { ADC_INPUT_DIRECT,         0 },                    //  9 - ANABUS_INPUT_DAC1
    { ADC_INPUT_DIRECT,         0 },                    // 10 - ANABUS_INPUT_UNUSED
    { ADC_INPUT_DIRECT,         0 },                    // 11 - ANABUS_INPUT_CAL_ZERO
    { ADC_INPUT_DIRECT,         0 },                    // 12 - ANABUS_INPUT_CAL_POSREF
    { ADC_INPUT_DIRECT,         0 },                    // 13 - ANABUS_INPUT_CAL_NEGREF
}
#endif
;


//
// ADC variables
//

struct adc
{
    INT32U              stuck_limit;                    // Limit for number of samples with no change
    INT32U              i_dcct_match_f;                 // I_dcct match flag
    INT32U              last_dsp_reset;                 // Unix time of the last ADC filter reset executed
    // after being triggered by the DSP
    INT32U              reset_hist_idx;                 // index used by property.adc.reset_hist_flags[]
    INT32U              reset_req_copy;                 // Local copy of the dpcls->dsp.adc.reset_req

    INT32U              filter_mpx[FGC_N_ADCS];         // ANA_MODMPX
    INT32U              signal_mpx[FGC_N_ADCS];         // ANA_MPX

    struct filter_20ms
    {
        INT32U              nb_of_samples;  // Number of samples so that the filter duration is close to 20ms
        INT32U              buffer_idx;                     // Index in the Vraw circular buffer
        INT32U              history_idx;                    // Index in adc_chan.filter_20ms.vraw_sum_history[]
        INT32U              history_offset;                 // Index offset is usually equal to REG.I.PERIOD_DIV
        FP32                compensation_delay; // Compensation delay of the filter, in number of iterations
        FP32                time_ratio;                     // = compensation_delay / history_offset
    }                   filter_20ms;                    // 20 ms filter
};

ADC_VARS_EXT struct adc adc;

// ADC latest calibrated measurements

union adc_cal_meas
{
    struct cal_current current;
    struct cal_voltage voltage;
};

// Structure used to hold the working variables of a basic mean filter

struct filter_mean_vars
{
    INT32U             nb_samples;
    INT64S             raw_sum;
    struct cal_voltage min;                 // Min voltage values for peak-peak calculation
    struct cal_voltage max;                 // Max voltage values for peak-peak calculation
};

// Structures used to hold the filtered measurements (on 1ms, 20ms, 200ms and 1s windows)

struct filter_mean_cal_meas
{
    union adc_cal_meas filtered;            // Filtered, calibrated measurements
    union adc_cal_meas min;                 // Min measurement values in the filter window
    union adc_cal_meas max;                 // Max measurement values in the filter window
    union adc_cal_meas peak_to_peak;        // Peak to peak    values
    INT32S             err_downcounter;     // Equal to zero if this measurement is based on MEAS_OK samples only
};

struct filtered_cal_meas
{
    union adc_cal_meas filtered;            // Filtered, calibrated measurements
    INT32S             err_downcounter;     // Equal to zero if this measurement is based on MEAS_OK samples only
};

// ADC channels

struct adc_chan
{
    INT64S                      adc_direct;         // Low word of raw ADC value ToDo remove and clean spy
    INT32S                      adc_raw;                        // Raw ADC value per acquisition period
    enum adc_input_type         input_type;                     // Type of input of the ADC (Idcct, Vmeas, DIRECT)
    INT32U                      input_idx;                      // Index in cal_dcct or cal_v_meas arrays
    INT32U                      stuck_counter;                  // Number of samples with no change
    INT32U                      st_check;                       // Signal tests status (jump, stuck, noise)
    INT32U                      last_st_check;                          // Previous signal tests status
    INT32U                      st_meas;                        // ADC  status bits
    INT32U                      st_dcct;                        // DCCT status bits
    INT32U                      i_meas_bad_counter;             // Bad Imeas millisecond down counter
    union adc_cal_meas          meas;                           // Calibrated measurement per acquisition period
    union adc_cal_meas          previous_meas;      // Calibrated measurement on the previous iteration

    // Filters results

    struct filter_mean_cal_meas ms;                 //   1 ms measurements (@ 1 kHz, without prediction)
    struct filtered_cal_meas
            ms20;                  //  20 ms measurements (@ the iteration period, with prediction)
    struct filter_mean_cal_meas ms200;              // 200 ms measurements (@ 5 Hz,  without prediction)
    struct filter_mean_cal_meas s;                  //   1 s  measurements (@ l Hz,  without prediction)

    // Filters working variables

    struct filter_mean_vars     filter_1ms_vars;
    struct filter_20ms_chan
    {
        INT32S                      vraw_buffer[FILTER_20MS_MAX_LEN];       // 20-sample sliding window filter buffer
        INT64S                      vraw_sum;                               // Sum of the last 20 Vraw values
        INT64S vraw_sum_history[VRAW_SUM_HIST_LEN];    // 20-sample filter's history circular buffer
    }                           filter_20ms;
    struct filter_mean_vars     filter_200ms_vars;
    struct filter_mean_vars     filter_1s_vars;

    BOOLEAN i_meas_ok_200ms;                // True if all I_MEAS_OK since the last 200ms boundary
    BOOLEAN i_meas_ok_1s;                   // True if all I_MEAS_OK since the last 1s boundary
};

ADC_VARS_EXT struct adc_chan adc_chan[FGC_N_ADCS];              // Four ADC channels (IA, IB, Vs and Aux)
ADC_VARS_EXT struct adc_chan * adc_current_a;                   // IA
ADC_VARS_EXT struct adc_chan * adc_current_b;                   // IB
ADC_VARS_EXT struct adc_chan * adc_v_meas_load;                 // V load
ADC_VARS_EXT struct adc_chan * adc_v_meas_capa;                 // V capa
ADC_VARS_EXT struct adc_chan * adc_aux;                         // Aux
ADC_VARS_EXT float * adc_chan_v_adc[FGC_N_ADCS];                // v_adc for SPY

struct adc_save                 // Save the ADC configuration (used during calibration)
{
    enum adc_input_type         input_type[FGC_N_ADCS];         // Type of input of the ADC (Idcct, Vmeas, DIRECT)
    INT32U                      input_idx [FGC_N_ADCS];         // Index in cal_dcct or cal_v_meas arrays
};

ADC_VARS_EXT struct adc_save adc_save;

//
// Function declarations
//

void            AdcResetSignalsPointers(void);
void            AdcSignalToChannel(INT32U signal_type, INT32U signal_nr);
void            AdcSignalsToChannels(void);
void            AdcInit(void);
void            AdcCalibratedMeas(INT32U          adc_idx,
                                  INT32S          adc_raw,
                                  BOOLEAN         sim_flag,
                                  union adc_cal_meas * output_measurement);
void            AdcRead(void);
void            AdcFilter(void);
void            AdcMpx(void);
void            AdcGatePars(void);
void            AdcSaveInputs(void);
void            AdcRestoreInputs(void);
void            AdcMeanFilterProcess(INT32U adc_idx, struct filter_mean_vars * filter_mean_vars,
                                     struct filter_mean_cal_meas * out_filter_mean_cal_meas);
void            AdcUpdateInputSignal(INT32U adc_idx, ANALOGUE_BUS_INPUT input_signal, INT32U filter_mpx);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation ADC_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: fgc3\inc\adc.h
\*---------------------------------------------------------------------------------------------------------*/
