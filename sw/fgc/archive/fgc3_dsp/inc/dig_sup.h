/*!
 *  @file      dig_sup.h
 *  @brief     Interface to the supplemental digital input/output channels.
 */

#ifndef FGC_DIG_SUP_H
#define FGC_DIG_SUP_H

#ifdef DIG_SUP_GLOBALS
#define DIG_SUP_VARS_EXT
#else
#define DIG_VARS_EXT extern
#endif


// Includes

#include <macros.h>
#include <memmap.h>


// External structures, unions and enumerations

/*!
 * Digital supplementary output and input channels.
 */
typedef enum dig_sup_channel
{
    DIG_SUP_A_CHANNEL0 = DIG_SUP_INP_A0_MASK16,
    DIG_SUP_A_CHANNEL1 = DIG_SUP_INP_A1_MASK16,
    DIG_SUP_A_CHANNEL2 = DIG_SUP_INP_A2_MASK16,
    DIG_SUP_A_CHANNEL3 = DIG_SUP_INP_A3_MASK16,
    DIG_SUP_A_CHANNEL4 = DIG_SUP_INP_A4_MASK16,
    DIG_SUP_B_CHANNEL0 = DIG_SUP_INP_B0_MASK16,
    DIG_SUP_B_CHANNEL1 = DIG_SUP_INP_B1_MASK16,
    DIG_SUP_B_CHANNEL2 = DIG_SUP_INP_B2_MASK16,
    DIG_SUP_B_CHANNEL3 = DIG_SUP_INP_B3_MASK16,
    DIG_SUP_B_CHANNEL4 = DIG_SUP_INP_B4_MASK16
} dig_sup_channel_e;

/*!
 * Digital supplementary output and input channels.
 */
typedef enum dig_sup_stauts
{
    DIG_SUP_LOW,
    DIG_SUP_HIGH,
} dig_sup_stauts_e;



// External function declarations

/*!
 *  Return the status of the supplemental digital channel.
 *
 *  @param channel The supplemental digital channel to access.
 *  @retval DIG_SUP_LOW  The channel is low.
 *  @retval DIG_SUP_HIGH The channel is high.
 */
static inline dig_sup_stauts_e digSupGetStatus(dig_sup_channel_e channel);



// External function definitions

static inline dig_sup_stauts_e digSupGetStatus(dig_sup_channel_e channel)
{
    return (Test(DIG_SUP_P, channel) ? DIG_SUP_HIGH : DIG_SUP_LOW);
}

#endif  // FGC_DIG_SUP_H end of header encapsulation

// EOF
