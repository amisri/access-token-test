/*---------------------------------------------------------------------------------------------------------*\
  File:         lib.h

  Purpose:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef LIB_H   // Header encapsulation
#define LIB_H

#ifdef LIB_GLOBALS
#define LIB_VARS_EXT
#else
#define LIB_VARS_EXT extern
#endif

/*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>
#include <defconst.h>   // for FGC_MAX_DIM_BUS_ADDR
#include <string.h>     // for size_t

/*---------------------------------------------------------------------------------------------------------*/

struct TDspDebug
{
    INT32U        buf[FGC_LOG_DIAG_DBUG_LEN];           // DIAG.DEBUGLOG
    INT32U        idx;                                  // DIAG.DEBUGIDX
    INT32U        samples_to_acq;                       // Debug samples to acquire
    INT32U        stop_log;                             // Debug log stopping flag
};

/*---------------------------------------------------------------------------------------------------------*/

void            InitDsp(void);
void            InitLib(INT32U n_props, INT32U max_user, INT32U ppm_start, INT32U ppm_length);
void            DebugMemTransfer(void);
BOOLEAN         BgpPropComms(void);
void            interruptible_memcpy(void * _s1, const void * _s2, size_t _n);
/*---------------------------------------------------------------------------------------------------------*/

LIB_VARS_EXT struct TDspDebug           dsp_debug;

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation LIB_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: lib.h
\*---------------------------------------------------------------------------------------------------------*/
