/*---------------------------------------------------------------------------------------------------------*\
  File:         pars.h

  Purpose:      FGC DSP Parsing functions declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PARS_H   // Header encapsulation
#define PARS_H

#ifdef PARS_GLOBALS
#define PARS_VARS_EXT
#else
#define PARS_VARS_EXT extern
#endif


#include <cc_types.h>
#include <dpcom.h>      // for MAX_CMD_PKT_LEN, MAX_CMD_TOKEN_LEN
#include <mcu_dsp_common.h>


#define REGFGC3_DEBUG_BLOCKS 10
// --------------------------------------------------------------------------------------------------------

// Functions that are not declared in defprops_dsp_fgc.h

void ParsLoadSim(void);
void ParsGroupSet(void);
void ParsLoadSelect(void);
void ParsMeasSim(void);
void ParsAbortTime(void);
void ParsRunTime(void);

// --------------------------------------------------------------------------------------------------------

// Command Packet parse buffers

struct cmd_pkt_parse_buf
{
    INT32U n_carryover_chars;                       // Number of characters from last token of prev pkt
    char buf[MAX_CMD_PKT_LEN + MAX_CMD_TOKEN_LEN];  // Parse buffer with extra space for last token of previous packet
};


typedef struct regfgc3_prog_debug
{
	uint32_t  block_start;
	uint32_t  block_end;
	uint32_t  block_length;
	uint32_t  block_pattern;	
} regfgc3_prog_debug_t;
// --------------------------------------------------------------------------------------------------------

PARS_VARS_EXT struct cmd_pkt_parse_buf  fcm_pars_buf;
PARS_VARS_EXT struct cmd_pkt_parse_buf  tcm_pars_buf;
PARS_VARS_EXT regfgc3_prog_debug_t regfgc3_debug[REGFGC3_DEBUG_BLOCKS]; 

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation PARS_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars.h
\*---------------------------------------------------------------------------------------------------------*/
