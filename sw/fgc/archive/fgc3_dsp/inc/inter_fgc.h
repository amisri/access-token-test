/*!
 *  @file      inter_fgc.h
 *  @defgroup  FGC3:DSP
 *  @brief     Inter FGC communication.
 */

#ifndef INTER_FGC_H
#define INTER_FGC_H

#ifdef INTER_FGC_GLOBALS
#define INTER_FGC_VARS_EXT
#else
#define INTER_FGC_VARS_EXT extern
#endif

// External functions declarations

/*!
 * Retrieves and prepares inter FGC measurements
 */
void InterFgcSignals(void);

#endif

// EOF
