/*---------------------------------------------------------------------------------------------------------*\
  File:         bgcom.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef BGCOM_H // header encapsulation
#define BGCOM_H

#ifdef BGCOM_GLOBALS
#define BGCOM_VARS_EXT
#else
#define BGCOM_VARS_EXT extern
#endif

#include <cc_types.h>

//-----------------------------------------------------------------------------------------------------------

// Common background processing variables

struct  bgcom
{
    INT32U            run_f;                          // Set by IsrAdc() at 4 kHz
    INT32U            led_counter;                    // Front panel LED toggle counter
    INT32U            idle_counter;                   // Idle up counter
    FP32              db_float;                       // DEBUG.MEM.DSP FP32 value
    INT32U            debug_idx;                      // FGC.DEBUG.MEM.DSP index
    INT32S            i;                              // Local var in bgcom to spare stack space
};

BGCOM_VARS_EXT struct bgcom    bgcom;

//-----------------------------------------------------------------------------------------------------------

#endif  // BGCOM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: bgcom.h
\*---------------------------------------------------------------------------------------------------------*/
