/*---------------------------------------------------------------------------------------------------------*\
  File:         reg.h

  Purpose:      FGC DSP regulation, load, limits and simulation support
\*---------------------------------------------------------------------------------------------------------*/

#ifndef REG_H   // Header encapsulation
#define REG_H

#ifdef REG_GLOBALS
#define REG_VARS_EXT
#else
#define REG_VARS_EXT extern
#endif

#include <cc_types.h>           // basic typedefs
#include <libreg.h>
#include <mcu_dsp_common.h>
#include <dsp_time.h>
#include <definfo.h>

// Regulation constants

#define REG_V_ERR_HIST_LEN                300           // Vref      history max length (used for error calculation)
#define REG_BI_ERR_HIST_LEN               100           // Iref/Bref history max length (used for error calculation)

#define REG_HI_SPEED_MAX_PERIOD_MS        10            // Max regulation period (in ms) that is considered "High-speed"
// Use the testing macro below:
#define TEST_REG_HI_SPEED(period)        (DSP_ROUND_TIME_MS(period) <= REG_HI_SPEED_MAX_PERIOD_MS)

// Reference history buffers

REG_VARS_EXT FP32 reg_v_ref_history_buffer [REG_V_ERR_HIST_LEN];
REG_VARS_EXT FP32 reg_bi_ref_history_buffer[REG_BI_ERR_HIST_LEN];

// Regulation loops

REG_VARS_EXT struct reg_rst_vars    reg_vars;           // ref, meas, act history arrays
REG_VARS_EXT struct reg_err         reg_v_err;          // Active V regulation error variables
REG_VARS_EXT struct reg_err         reg_bi_err;         // Active B/I regulation error variables

// Load  parameters for control, simulation and measurement

// Current and voltage measurement and reference limits

REG_VARS_EXT struct reg_lim_meas    lim_i_meas_a;       // Current measurement limits channel A
REG_VARS_EXT struct reg_lim_meas    lim_i_meas_b;       // Current measurement limits channel B
REG_VARS_EXT struct reg_lim_ref     lim_i_ref;          // Current reference limits
REG_VARS_EXT struct reg_lim_ref     lim_v_ref;          // Voltage reference limits
REG_VARS_EXT struct reg_lim_ref     lim_v_ref_fg;       // Voltage reference limits for libfg calls

struct limits                                           // Additional limits (not supported by libreg)
{
    FP32                            i_avl_max;          // Max available current = min(I_POS,I_HARDWARE)
    FP32                            i_avl_warn;         // Warning level for available current
    FP32                            i_start;            // Didt loop threshold for 1/2Q convertor startup
    FP32                            i_closeloop;        // Current loop threshold for 1/2Q convertor startup
};

REG_VARS_EXT struct limits          limits;

// Overall regulation structure

struct reg_param                                        // Additional regulation parameters and variables
{
    INT32U state;                   // Regulation state: B, I or V
    INT32U previous_state;          // Regulation state before the last change request: B, I or V
    struct reg_rst_pars rst_v_pars; // Openloop reg_rst_pars structure used in OFF state, after
    // a fault, or with the END reference in cycling
    struct reg_rst_pars
            rst_pars;   // Generic  reg_rst_pars structure used for B, I or V regulation in the DSP ISR
};

struct load_param
{
    INT32S                  select;                     // Load index (corresponding to property LOAD.SELECT)
    struct reg_load_pars    pars;                       // Load pars structure used by libreg
};

REG_VARS_EXT struct reg_param reg_param[2];     // Active and next regulation structures
// The DSP ISR will switch from one to the other when necessary

REG_VARS_EXT struct load_param load_param[2];   // Active and next loads used for regulation (and measurement)
// The DSP ISR will switch from one to the other when necessary

struct reg
{
    INT32U                  test_user_f;                // Test regulation parameters in use
    FP32                    max_abs_err_enable_time;    // Time to enable max_abs_err calculation
    INT32U hi_speed_f;          // Hi-speed regulation flag is set if regulation period <= 10 iteration periods
    INT32U period_counter;      // Period counter for hi_speed_f operation (in units of iteration periods)
    struct reg_param    *   active;                     // Active regulation parameters
    struct reg_param    *   next;                       // Prepare regulation parameters for next switch
    INT32U                  iteration_period_ns;        // DSP iteration period in ns

    struct reg_state_req
    {
        BOOLEAN next_cycle_f;   // Flag indicating that the regulation must change on the next C0 (and not immediately)
        BOOLEAN inform_mcu_f;
        INT32U  reg_state;
        FP32    v_ref;          // Only considered if the context switch is immediate.
        BOOLEAN bgp_process_f;  // BGP process for this REG request is pending
        BOOLEAN isr_process_f;  // ISR process for this REG request is pending
        BOOLEAN locked;         // If TRUE, no other reg request can be passed.
    }                       request;                    // A request to change the regulation mode
};
REG_VARS_EXT struct reg         reg;

struct load
{
    struct load_param   *   active;                     // Pointer to the active load
    struct load_param   *   next;                       // Prepare next load
    struct load_param   *   last_parsed;                // To be used only in parsing functions (pars.c)
};
REG_VARS_EXT struct load        load;

// Function declarations

void            RegStartCycle(void);
void            RegStatePrepare(void);
void            RegStateSwitch(void);
BOOLEAN         RegStateRequest(BOOLEAN next_cycle_f, BOOLEAN inform_mcu_f, INT32U reg_state, FP32 v_ref);
void            RegStateV(INT32U inform_mcu_f, FP32 v_ref);
void            RegPeriod(INT32U * period_vp_ptr, FP32 * period_ptr);
void            RegVarsInit(FP32 v_ref, FP32 reg_period, FP32 track_delay);
FP32            RegVrefFilter(FP32 v_ref);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation REG_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: reg.h
\*---------------------------------------------------------------------------------------------------------*/
