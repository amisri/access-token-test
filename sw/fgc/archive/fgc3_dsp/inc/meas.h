/*---------------------------------------------------------------------------------------------------------*\
  File:         meas.h

  Purpose:      FGC DSP measurement function and variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MEAS_H   // Header encapsulation
#define MEAS_H

#ifdef MEAS_GLOBALS
#define MEAS_VARS_EXT
#else
#define MEAS_VARS_EXT extern
#endif

#include <cc_types.h>
#include <definfo.h>    // for FGC_CLASS_ID

/*---------------------------------------------------------------------------------------------------------*/

#define MEAS_HIST_MASK                  3                       // Mask for B and I meas history buffer index

// TODO duplicated definitions
#define VMEAS1_CAL                      5.117e-3                // Into +/-10V
#define VMEAS10_CAL                     5.117e-4                // Into +/-10V
#define PLL_TICKS_TO_SECONDS            25000000  // How many ticks a s represents.

/*---------------------------------------------------------------------------------------------------------*/

struct meas_hist
{
    FP32                meas;                           // Measurement now based on history
    FP32                rate;                           // Rate of change based on history
    FP32                estimated;                      // Estimated measurement on the next regulation period
    FP32                buf[MEAS_HIST_MASK + 1];        // Measurement history buffer
    INT32U              idx;                            // Index of most recent sample in history
};

struct meas
{
    FP32                b;                              // ms measured field used for feedback (G) (class 53)
    FP32                i;                              // ms measured current used for feedback
    FP32                v;                              // Voltage measurement on the load in volts
    FP32                v_capa;                         // Voltage measurement on the capacitor in volts

    struct meas_hist  * hist;               // Pointer to b_hist or i_hist according to reg.active->state
    struct meas_hist    b_hist;             // Field measurement history
    struct meas_hist i_hist;                // Current measurement history (one sample every iteration period)
    struct meas_hist i_hist_reg;            // Current measurement history (one sample every regulation period)

    FP32                i_earth_pcnt;                   // I_EARTH_PCNT (% of trip level)
    FP32                i_earth;                        // I_EARTH (A)
    FP32                i_diff_1s;                      // 1s difference between measurements
    FP32                u_lead_pos;                     // U_LEAD_POS accumulator
    FP32                u_lead_neg;                     // U_LEAD_NEG accumulator
    FP32                u_leads[2];                     // U_LEADS values
};

#define REG_LIM_RMS_HYSTERESIS      0.9             //!< Zero limit hysteresis factor (also used for low limit)

/*!
 * RMS limits
 */
struct REG_lim_rms
{
    float        rms2_fault;                     //!< Squared RMS fault threshold
    float        rms2_warning;                   //!< Squared RMS warning threshold
    float        rms2_warning_hysteresis;        //!< Squared RMS warning threshold with hysteresis
    float        meas2_filter;                   //!< Filtered square of the measurement
    float        meas2_filter_factor;            //!< First-order filter factor for square of measurement

    struct
    {
        BOOLEAN        fault;                          //!< Set if filtered square of the measurement exceeds reg_lim_rms::rms2_fault
        BOOLEAN        warning;                        //!< Set if filtered square of the measurement exceeds reg_lim_rms::rms2_warning
    } flags;                                        //!< RMS limits flags
};

/*---------------------------------------------------------------------------------------------------------*/

/*!
 * Initialize measurement limits structure RMS squared trip and warning thresholds.
 *
 * This is a non-Real-Time function: do not call from the real-time thread or interrupt
 *
 * @param[out]    lim_rms          RMS limits object to initialize
 * @param[in]     rms_warning      RMS warning threshold
 * @param[in]     rms_fault        RMS fault threshold
 * @param[in]     rms_tc           Used to determine whether reg_lim_meas::rms2_fault, reg_lim_meas::rms2_warning
 *                                 and reg_lim_meas::rms2_warning_hysteresis should be set, and to calculate the
 *                                 value for reg_lim_meas::meas2_filter_factor.
 * @param[in]     iter_period      Iteration period, used to calculate the value for reg_lim_meas::meas2_filter_factor.
 */
void regLimRmsInit(struct REG_lim_rms *lim_rms, float rms_warning, float rms_fault, float rms_tc, float iter_period);

/*!
 * Check filtered squared measurement against limits.
 *
 * This is a Real-Time function.
 *
 * @param[in,out] lim_rms          Pointer to RMS current limits structure
 * @param[in]     meas             Measurement value
 */
void regLimMeasRmsRT(struct REG_lim_rms *lim_rms, float meas);

void            MeasHistory(struct meas_hist * hist, FP32 meas);
void            MeasHistoryRegPeriod(struct meas_hist * hist, FP32 meas);
void            MeasSelect(void);
FP32            MeasSpyMpx(INT32U);
void            MeasSpySend(void);
void            MeasDiag(void);
/*---------------------------------------------------------------------------------------------------------*/

MEAS_VARS_EXT struct REG_lim_rms lim_i_rms;
MEAS_VARS_EXT struct meas meas;

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation MEAS_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: meas.h
\*---------------------------------------------------------------------------------------------------------*/
