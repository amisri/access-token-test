/*!
 *  @file     unipolar_switch.h
 *  @defgroup FGC3:DSP
 *  @brief    Provides functions to modify the sign of the measurement or
 *  based on the state of the polarity switch.
 *
 *  A unipolar converter with an external polarity switch implements a bipolar
 *  regulation. The sign of the voltage, current and reference values must be
 *  flipped when necessary depending on the state of the polarity switch.
 */

#ifndef FGC_UNIPOLAR_SWITCH_H
#define FGC_UNIPOLAR_SWITCH_H

#ifdef UNIPOLAR_SWITCH_GLOBALS
#define UNIPOLAR_SWITCH_VARS_EXT
#else
#define UNIPOLAR_SWITCH_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <defconst.h>
#include <dpcls.h>
#include <rtcom.h>
#include <props.h>
#include <libfg.h>

// External function declarations

/*!
 * Flips the sign of the voltage measurement across the load if necessary.
 *
 * The voltage measurement is always positive. This function flips the sign of
 * the measurement if the polarity switch is negative.
 *
 * @param value Voltage value
 */
static INLINE void UniSwitchVMeasLoad(float * const value);

/*!
 * Flips the sign of the voltage measurement across the capacitor if necessary.
 *
 * The voltage measurement is always positive. This function flips the sign of
 * the measurement if the polarity switch is negative.
 *
 * @param value Voltage value
 */
static INLINE void UniSwitchVMeasCapa(float * const value);

/*!
 * Flips the sign of the current measurement if necessary.
 *
 * The current measurement sign depends on the state of the polarity switch
 * and the position of the DCCT, i. e., before or after the switch.
 *
 * @param value Current value
 */
static INLINE void UniSwitchIMeas(uint8_t dcct_indx, float * const value);

/*!
 * Flips the sign of the voltage reference if necessary.
 *
 * The voltage reference sent to the converter must always be positive.
 * Regulation is however bipolar so the sign might need to be flipped.
 *
 * @param value Voltage reference.
 */
static INLINE void UniSwitchVRef(float * const value);

/*!
 * Returns the polarity for the limits.
 *
 * @return FG_LIMITS_POL_NEGATIVE if unipolar with negative polarity,
 * FG_LIMITS_POL_NORMAL otherwise.
 */
static INLINE enum fg_limits_polarity UniSwitchLimitsEnum(void);

/*!
 * Returns whether the limits must be inverted.
 *
 * @return 1 if limits must be inverted. 0 otherwise.
 */
static INLINE uint32_t UniSwitchLimitsInt(void);

/*!
 * Flips the sign of a float value if the converter is unipolar with an
 * external polarity switch and the state of which is negative.
 *
 * @param value Float to change.
 */
static INLINE void UniSwitchFloat(float * const value);

// External function definitions

#if (FGC_CLASS_ID == 61)

static INLINE void UniSwitchVMeasLoad(float * const value)
{
    if (dpcls->mcu.vs.polarity.bipolar == FGC_CTRL_ENABLED            &&
        dpcls->mcu.vs.polarity.state == FGC_POL_NEGATIVE              &&
        property.v_probe.load.position == FGC_TRANSDUCER_POSITION_CONVERTER &&
        rtcom.state_op != FGC_OP_CALIBRATING)
    {
        *value = -(*value);
    }
}

static INLINE void UniSwitchVMeasCapa(float * const value)
{
    if (dpcls->mcu.vs.polarity.bipolar == FGC_CTRL_ENABLED            &&
        dpcls->mcu.vs.polarity.state == FGC_POL_NEGATIVE              &&
        property.v_probe.capa.position == FGC_TRANSDUCER_POSITION_CONVERTER &&
        rtcom.state_op != FGC_OP_CALIBRATING)
    {
        *value = -(*value);
    }
}

static INLINE void UniSwitchIMeas(uint8_t dcct_indx, float * const value)
{
    if (dpcls->mcu.vs.polarity.bipolar == FGC_CTRL_ENABLED  &&
        dpcls->mcu.vs.polarity.state == FGC_POL_NEGATIVE    &&
        property.dcct.position[dcct_indx] == FGC_TRANSDUCER_POSITION_CONVERTER)
    {
        *value = -(*value);
    }
}

static INLINE enum fg_limits_polarity UniSwitchLimitsEnum(void)
{
    return ((dpcls->mcu.vs.polarity.bipolar == FGC_CTRL_ENABLED &&
             dpcls->mcu.vs.polarity.state == FGC_POL_NEGATIVE) ?
            FG_LIMITS_POL_NEGATIVE : FG_LIMITS_POL_NORMAL);
}

static INLINE uint32_t UniSwitchLimitsInt(void)
{
    /* This function is called when the switch polarity related properties are
     * modified. If switching the polarity is slow, which is the case for the
     * COMHV-PS for example, POLARITY.MODE might be NEGATIVE whilst
     * POLARITY.STATE is still POSITIVE. That is why the condition below checks
     * for the POLARITY.MODE instead of the POLARITY.STATE. This is however not
     * the case if POLARITY.MODE has never been defined, that is, it is UNSET.
     * In that case rely on POLARITY.STATE.
     */

    uint32_t polarity = ((dpcls->mcu.vs.polarity.mode != FGC_POL_MODE_NOT_SET) ?
                         dpcls->mcu.vs.polarity.mode :
                         dpcls->mcu.vs.polarity.state);

    return ((dpcls->mcu.vs.polarity.bipolar == FGC_CTRL_ENABLED &&
             polarity == FGC_POL_NEGATIVE) ? 1 : 0);
}

static INLINE void UniSwitchFloat(float * const value)
{
    if (dpcls->mcu.vs.polarity.bipolar == FGC_CTRL_ENABLED  &&
        dpcls->mcu.vs.polarity.state == FGC_POL_NEGATIVE)
    {
        *value = -(*value);
    }
}

static INLINE void UniSwitchVRef(float * const value)
{
    if (dpcls->mcu.vs.polarity.bipolar == FGC_CTRL_ENABLED  &&
        dpcls->mcu.vs.polarity.state == FGC_POL_NEGATIVE)
    {
        *value = -(*value);
    }
}

#elif (FGC_CLASS_ID == 62)

static INLINE void UniSwitchVMeasLoad(float * const value)
{
    ;
}

static INLINE void UniSwitchVMeasCapa(float * const value)
{
    ;
}

static INLINE void UniSwitchIMeas(uint8_t dcct_indx, float * const value)
{
    if (dpcls->mcu.vs.polarity.state == FGC_POL_NEGATIVE &&
        property.dcct.position[dcct_indx] == FGC_TRANSDUCER_POSITION_CONVERTER)
    {
        *value = -(*value);
    }
}

static INLINE enum fg_limits_polarity UniSwitchLimitsEnum(void)
{
    return FG_LIMITS_POL_NORMAL;
}

static INLINE uint32_t UniSwitchLimitsInt(void)
{
    return (0);
}

static INLINE void UniSwitchFloat(float * const value)
{
    ;
}

static INLINE void UniSwitchVRef(float * const value)
{
    if (dpcls->mcu.vs.polarity.state == FGC_POL_NEGATIVE)
    {
        *value = -(*value);
    }
}

#endif // (FGC_CLASS_ID == 61)

#endif

// EOF
