/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp_panic.c

  Purpose:      FGC DSP Software - DSP panic code for FGC3
\*---------------------------------------------------------------------------------------------------------*/

#define DSP_PANIC_GLOBALS

#include <dsp_panic.h>
#include <cc_types.h>
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <dsp_dependent.h>      // for DISABLE_INTS
#include <rtcom.h>              // for rtcom global variable
#include <mcu_dsp_common.h>

volatile INT32U dsp_panic_dummy;

/*---------------------------------------------------------------------------------------------------------*/
void DspPanic(enum dsp_panic_codes panic_code)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the DSP panic function. It does not return.
  This function disables interrupts and runs an infinite loop.
\*---------------------------------------------------------------------------------------------------------*/
{
    // TODO DspPanic: get the calling SP

    DISABLE_INTS;

    RUNCODE(RUNCODE_DSP_PANIC);

    dpcls->dsp.panic.panic_code = panic_code;

    for (;;)
    {
        dsp_panic_dummy = 0;
    }

    // Never returns
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_panic.c
\*---------------------------------------------------------------------------------------------------------*/

