/*---------------------------------------------------------------------------------------------------------*\
 File:      patch_dMax.c

   Patch to dMax code to fix spurious interrupts caused by failure to clear R0.

\*---------------------------------------------------------------------------------------------------------*/


#include <patch_dMax.h>

/* External Reference to dMax Patch Code */

extern const far int SarPdspFirmware0[];
extern const far int SarPdspFirmware1[];


/*---------------------------------------------------------------------------------------------------------*/
void Fix_dMax()
/*---------------------------------------------------------------------------------------------------------*\
 iniPdsp
 Initialises dMax data structures that manage resource allocation
\*---------------------------------------------------------------------------------------------------------*/
{
    unsigned int    *   dmaxDataRam;
    unsigned int    *   dmaxProgRam;
    unsigned int        dmaxCodeSize;
    unsigned int        i;

    // Initialise Dmax 0
    dmaxDataRam  = HDEVICE_DMAX0_DATARAM_ADDRESS;
    dmaxProgRam  = HDEVICE_DMAX0_PROGRAM_ADDRESS;
    dmaxCodeSize = SarPdspFirmware0[1];

    // Make sure dMax is in Reset
    *HDEVICE_GPIODIR0_REGISTER = 0x0;
    asm("   nop 2");

    *HDEVICE_GPIOGCR0_REGISTER = 0x0;

    *HDEVICE_CFGBRIDGE_REGISTER |= 1;
    asm("   nop 2");

    *HDEVICE_CFGBRIDGE_REGISTER &= 0xFFFFFFFE;


    for (i = 9; i <= (dmaxCodeSize); ++i)
    {
        *dmaxProgRam++ = SarPdspFirmware0[i];
    }

    for (i = 0; i < 1024; ++i)
    {
        *dmaxDataRam++ = 0x00000000;
    }

    // Initialise Dmax1
    dmaxDataRam  = HDEVICE_DMAX1_DATARAM_ADDRESS;
    dmaxProgRam  = HDEVICE_DMAX1_PROGRAM_ADDRESS;
    dmaxCodeSize = SarPdspFirmware1[1];

    for (i = 9; i <= (dmaxCodeSize); ++i)
    {
        *dmaxProgRam++ = SarPdspFirmware1[i];
    }

    for (i = 0; i < 1024; ++i)
    {
        *dmaxDataRam++ = 0x00000000;
    }


    *HDEVICE_CFGBRIDGE_REGISTER |= 1;
    asm("   nop 2");

    *HDEVICE_CFGBRIDGE_REGISTER &= 0xFFFFFFFE;

    // Release dMax from Reset
    *HDEVICE_GPIOGCR0_REGISTER = 0x1;
    asm("   nop 2");

    *HDEVICE_GPIODIR0_REGISTER = 0x3;
}
/*---------------------------------------------------------------------------------------------------------*/
