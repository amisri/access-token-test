/*---------------------------------------------------------------------------------------------------------*\
  File:         sim.c

  Purpose:      FGC DSP Software - VS and Load simulation
\*---------------------------------------------------------------------------------------------------------*/

#define SIM_GLOBALS

#include <sim.h>
#include <cc_types.h>
#include <dsp_panic.h>
#include <props.h>      // for property global variable
#include <dpcls.h>      // for dpcls global variable
#include <reg.h>        // reg, for load global variables
#include <definfo.h>    // for FGC_CLASS_ID
#include <main.h>       // for DSP_ITER_PERIOD

// Static global variables

static FP32 vmeas_history_buffer[V_MEAS_HIST_LEN];             // Voltage measurement delay history buffer
static FP32 imeas_history_buffer[I_MEAS_HIST_LEN];             // Current measurement delay history buffer
static FP32 bmeas_history_buffer[B_MEAS_HIST_LEN];             // Field measurement delay history buffer

/*---------------------------------------------------------------------------------------------------------*/
void SimNextRegState(FP32 vref)
/*---------------------------------------------------------------------------------------------------------*\
  This function simulates the response of the voltage source (VS) and the load to the voltage reference (Vref)

  Input:  Vref from the last regulation iteration

  Output: Vmeas/Imeas/Bmeas to be used during the next regulation iteration (if in simulation mode).
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 vload = 0.0;                  // Voltage applied to the load

    // Simulate the voltage source response - note the delay in the vref is included in the vmeas and imeas delays

    vload = regSimVs(&property.vs.sim, &sim.active->vs, vref);

    // Simulate the load response

    regSimLoad(&load.active->pars, &sim.active->load, vload);

    // Voltage measurement delay (includes voltage control delay)

    regDelayCalc(&sim.active->vmeas_delay, vload, &sim.active->v);

    // Current measurement delay (includes voltage control delay)

    regDelayCalc(&sim.active->imeas_delay, sim.active->load.current, &sim.active->i);

    // Field measurement (assumes no measurement delay, just the voltage control delay)

    regDelayCalc(&sim.active->bmeas_delay, sim.active->load.field, &sim.active->b);
}
/*---------------------------------------------------------------------------------------------------------*/
void SimInitDelays(struct sim_param * sim_param, FP32 vref_init, FP32 vmeas_init, FP32 imeas_init,
                   FP32 bmeas_init)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the structures required to simulate the following delays:

      1 - Voltage measurement delay
      2 - Current measurement delay
      3 - Field measurement delay

  Note: The simulated voltage control delay is moved to be included in the simulated
        voltage/current/field measurement delays
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32    delay_in_iterations;       // Measurement delay in iterations

    // -----------------------------------------------------
    // Voltage measurement delay
    // -----------------------------------------------------

    // NB: The minus 1 is because the delayed measurement will only be used on the next iteration

    delay_in_iterations = property.vs.ctrl_delay / DSP_ITER_PERIOD + VMEAS_DELAY_ITERS - 1.0;

    // Check that the delay does not exceed the buffer size

    if ((INT32S)delay_in_iterations > V_MEAS_HIST_LEN)
    {
        DspPanic(DSP_PANIC_BAD_PARAMETER);
    }

    regDelayInitPars(&sim_param->vmeas_delay,
                     &vmeas_history_buffer[0],
                     delay_in_iterations,
                     load.active->pars.sim.vs_undersampled_flag);

    regDelayInitVars(&sim_param->vmeas_delay, vmeas_init);

    // -----------------------------------------------------
    // Current measurement delay
    // -----------------------------------------------------

    // Is in fact the measurement delay plus the voltage control delay.
    // The measurement delay is 0 in low speed due to latency compensation.
    // The minus 1 is because the delayed measurement will only be used on the next iteration.

    delay_in_iterations = property.vs.ctrl_delay / DSP_ITER_PERIOD + (reg.hi_speed_f ? IMEAS_DELAY_ITERS : 0.0) -
                          1.0;

    // Check that the delay does not exceed the buffer size

    if ((INT32S)delay_in_iterations > I_MEAS_HIST_LEN)
    {
        DspPanic(DSP_PANIC_BAD_PARAMETER);
    }

    regDelayInitPars(&sim_param->imeas_delay,
                     &imeas_history_buffer[0],
                     delay_in_iterations,
                     load.active->pars.sim.vs_undersampled_flag && load.active->pars.sim.load_undersampled_flag);

    regDelayInitVars(&sim_param->imeas_delay, imeas_init);

    // -----------------------------------------------------
    // Field measurement delay
    // -----------------------------------------------------

    // Is in fact the measurement delay plus the voltage control delay.
    // Assume no delay on the Field measurement, this is in fact only the voltage control delay.
    // The minus 1 is because the delayed measurement will only be used on the next iteration.

    delay_in_iterations = property.vs.ctrl_delay / DSP_ITER_PERIOD - 1.0;

    // Check that the delay does not exceed the buffer size.

    if ((INT32S)delay_in_iterations > B_MEAS_HIST_LEN)
    {
        DspPanic(DSP_PANIC_BAD_PARAMETER);
    }

    regDelayInitPars(&sim_param->bmeas_delay,
                     &bmeas_history_buffer[0],
                     delay_in_iterations,
                     load.active->pars.sim.vs_undersampled_flag && load.active->pars.sim.load_undersampled_flag);

    regDelayInitVars(&sim_param->bmeas_delay, bmeas_init);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sim.c
\*---------------------------------------------------------------------------------------------------------*/

