/*---------------------------------------------------------------------------------------------------------*\
  File:         hardware_setup.c

  Purpose:      TMS320C6727 hardware setup

  Author:       daniel.calcoen@cern.ch

  Notes:

\*---------------------------------------------------------------------------------------------------------*/

#include <hardware_setup.h>
#include <cc_types.h>
#include <hw_tms320c6727.h>
#include <c6x.h>

/*---------------------------------------------------------------------------------------------------------*/
void Init_SPI(void)
/*---------------------------------------------------------------------------------------------------------*\
  Initialise SPI1 serial port.
\*---------------------------------------------------------------------------------------------------------*/
{
    *(volatile INT32U *)(SPI1_REG_START + 0)    = 1;            // Release sw reset
    *(volatile INT32U *)(SPI1_REG_START + 4)    = 0x3;          // Master mode
    *(volatile INT32U *)(SPI1_REG_START + 0x14) = 0xE00;        // SPIPC0 (SIMO,SOMI,CLK enabled)
    *(volatile INT32U *)(SPI1_REG_START + 0x50) = 0xFF10;       // SPIFMT0 (fmt0 selected by default)
    // MSB first, slowest BR
    *(volatile INT32U *)(SPI1_REG_START + 4)   |= 0x01000000;   // Enable SPI1

}
/*---------------------------------------------------------------------------------------------------------*/
void Init_Timers(void)
/*---------------------------------------------------------------------------------------------------------*\
  Initialise DSP timers.
  RTIFRC0 counts in us
\*---------------------------------------------------------------------------------------------------------*/
{
    *(INT32U *)RTICPUC0 = 0x95;         // US - Set prescale value at 150 (SYSCLK2 runs at 150MHz)
    *(INT32U *)RTICPUC1 = 0x249EF;      // MS - Set prescale value at 150000 (SYSCLK2 runs at 150MHz)
    *(INT32U *)RTIGCTRL = 0x03;         // start counters 0 and 1
}
/*---------------------------------------------------------------------------------------------------------*/
void Hw_cache_enable()
/*---------------------------------------------------------------------------------------------------------*\
  This function Invalidates and Enables Cache
\*---------------------------------------------------------------------------------------------------------*/
{
    L1PSAR = 0x00000000;                 // Invalidate start address

    L1PICR  = L1PICR | L1P_INVALIDATE;   // Invalidate all lines of cache by setting L1P bit

    while (L1PICR != 0x00000000u);       // Make Sure Cache Invalidate is complete

    CSR = (CSR & (~CACHE_CTRL_MASK)) | CACHE_ENABLE; // Enable the Cache
}
/*---------------------------------------------------------------------------------------------------------*/
void Hw_cache_bypass()
/*---------------------------------------------------------------------------------------------------------*\
  This function Invalidates and by passes Cache forcing all fetches from memory contents.
\*---------------------------------------------------------------------------------------------------------*/
{
    L1PSAR = 0x00000000;                // Invalidate start address

    L1PICR = L1PICR | L1P_INVALIDATE;   // Invalidate all lines of cache by setting L1P bit

    while (L1PICR != 0x00000000u);               // Make Sure Cache Invalidate is Complete

    CSR = (CSR & (~CACHE_CTRL_MASK)) | CACHE_BYPASS; // Bypass the Cache
}
/*---------------------------------------------------------------------------------------------------------*/
void Hw_init(void)
/*---------------------------------------------------------------------------------------------------------*\
  (is here only for debug purpose)
  Initialise the DSP as the asm BootLoader does

 With ClkIn 25MHz, D0=0, D1=1, D2=3, D3=5, M=12
            PllOut=300MHz, SysClk1=150Mhz, SysClk2=75MHz, SysClk3=50MHz
 the time for the test points were :
  TP1 TP0
    0   1 Start
    0   1 ---------        +6us
          after PLLDIV1
          go alignment command
          quit pll reset state
    1   1 ---------        +9us
          wait for lock
    0   1 ---------        +6us
          EMIF config
    0   0 ---------        +434ns
          set mem in 32bits
    1   1 ---------        +100ns
          start loading
    0   0 ---------        +290ms
          jump to Prog

 With ClkIn 25MHz, D0=0, D1=0, D2=1, D3=2, M=12
            PllOut=300MHz, SysClk1=300Mhz, SysClk2=150MHz, SysClk3=100MHz
 the time for the test points were :
  TP1 TP0
    0   1 Start
    0   1 ---------        +6us
          after PLLDIV1
          go alignment command
          quit pll reset state
    1   1 ---------        +5us
          wait for lock
    0   1 ---------        +3us
          EMIF config
    0   0 ---------        +177ns
          set mem in 32bits
    1   1 ---------        +31ns
          start loading
    0   0 ---------        +290ms
          jump to Prog
\*---------------------------------------------------------------------------------------------------------*/
{
    /*
    EMIF_SDCR : SDRAM Configuration Register : 0xF0000008

    EMIF_SDCR+3 = HiHi, bits b31..b24
                  1000 0000
    b31 = EMIF_SDCR.SR - Self-Refresh mode bit
        This bit controls entering and exiting of the Self-Refresh mode described in Section 2.4.7
        The field should be written using a byte-write to the upper byte of SDCR to avoid
        triggering the SDRAM initialization sequence

        0 exit the Self-Refresh mode
        1 enter the Self-Refresh mode
    */

    *(volatile INT8U *)(EMIF_SDCR + 3) = 0x80;        // enter self-refresh mode

    //-----------------------------------------------------------------------------------------------------------
    /*
    GPIOEN : General Purpose I/O Enable Register : 0x4300000C after reset 0x00000000

    b31
    ..
    b17 - reserved, always read as 0, a value written to this field has no effect
    b16
    ..
    b13 - unsupported on th C672x, always write a 0 to this location

    b12 GPIO_EN12
        0 UHPI_HA[15:8] pins function as UHPI pins
        1 UHPI_HA[15:8] pins function as GPIO pins

    b11 GPIO_EN11
        0 UHPI_HA[7:0] pins function as UHPI pins
        1 UHPI_HA[7:0] pins function as GPIO pins

    b10 GPIO_EN10
        0 UHPI_HD[31:24] pins function as UHPI pins
        1 UHPI_HD[31:24] pins function as GPIO pins

    b9 GPIO_EN9
        0 UHPI_HD[23:16] pins function as UHPI pins
        1 UHPI_HD[23:16] pins function as GPIO pins

    b8 GPIO_EN8
        0 UHPI_HD[15:8] pins function as UHPI pins
        1 UHPI_HD[15:8] pins function as GPIO pins

    b7 GPIO_EN7
        0 UHPI_HD[7:0] pins function as UHPI pins
        1 UHPI_HD[7:0] pins function as GPIO pins

    b6 GPIO_EN6
        0 UHPI_HINT pin functions as a UHPI pin
        1 UHPI_HINT pin functions as a GPIO pin

    b5 GPIO_EN5
        0 UHPI_HRDY pin functions as a UHPI pin
        1 UHPI_HRDY pin functions as a GPIO pin

    b4 GPIO_EN4, UHPI_HHWIL pin is not supported as GPIO on C672x DSP devices through this bit. Write '0' to this register bit

    b3 GPIO_EN3
        0 UHPI_HBE[3:0] pins function as UHPI pins
        1 UHPI_HBE[3:0] pins function as GPIO pins

    b2 GPIO_EN2
        0 UHPI_HAS pin functions as a UHPI pin
        1 UHPI_HAS pin functions as a GPIO pin

    b1 GPIO_EN1
        0 UHPI_HCNTL[1:0] pins function as UHPI pins
        1 UHPI_HCNTL[1:0] pins function as GPIO pins

    b0 GPIO_EN0
        0 UHPI_HCS, UHPI_HDS[2:1], and UHPI_HR/Wfunction as UHPI pins
        1 UHPI_HCS, UHPI_HDS[2:1], and UHPI_HR/W function as GPIO pins

    */
    *(volatile INT32U *)GPIOEN   = 0x00000080;        // b7=1, UHPI_HD[7:0] pins function as GPIO pins

    *(volatile INT32U *)GPIODAT1 = 0x0001; // TP0 = 1, TP1 = 0
    /*
    GPIODIR1 : General Purpose I/O Data Direction Register 1 : 0x43000010 after reset 0x00000000

    b31
    ..
    b0
        0 UHPI_HD[bit] pin as input
        1 UHPI_HA[bit] pin as output

        only takes effect when the pins are enabled as GPIO through the GPIOEN register
    */
    *(volatile INT32U *)GPIODIR1 = 0x0007;        // bits b2,b1,b0 as outputs

    //-----------------------------------------------------------------------------------
    // Universal Host Port Interface - Configure GPIO pins
    // UHPI_H[0] = TP0
    // UHPI_H[1] = TP1
    // UHPI_H[2] = 32bits access to PLD DPRAM
    //-----------------------------------------------------------------------------------
    *(volatile INT32U *)GPIODAT1 = 0x0001; // TP0 = 1, TP1 = 0

    //-----------------------------------------------------------------------------------------------------------
    /*
    Init the PLL registers to have the full DSP CPU and EMIF speed
        DSP CPU = 300MHz
        EMIF = 100MHz
        The external clock is 25MHz


    following the steps described in spru879a.pdf 3.1.1

     1  PLLCSR.PLLEN = 0 (bypass mode) (not required when the device is coming out of reset)
     2  wait 4 cycles of the slowest of PLLOUT or reference clock source (CLKIN or OSCIN) (not required when the device is coming out of reset)
     3  PLLCSR.PLLRST = 1 (PLL is reset) (not required when the device is coming out of reset)
     4  program PLLDIV0
     5  program PLLM
     6  program PLLDIV1, apply the GO operation to change the divider to new ratio
     7  program PLLDIV2, apply the GO operation to change the divider to new ratio
     8  program PLLDIV3, apply the GO operation to change the divider to new ratio
     9  wait for PLL to properly reset
    10  PLLCSR.PLLRST = 0 (to bring PLL out of reset)
    11  wait for PLL to lock
    12  PLLCSR.PLLEN = 1 (enable PLL mode)
    */

    /*
    PLL_CSR : PLL Control/Status Register : 0x41000100 after reset 0x00000058

    b31
    ..
    b7 - reserved, always read as 0, always write a 0 to this location

    b6 - STABLE, Oscillator input stable bit
        Indicates if the OSCIN/CLKIN input has stabilized
        The STABLE bit is set to 1 after the reset controller counts 4096 OSCIN or CLKIN input
        clock cycles after the RESET signal is asserted high
        It is assumed that the OSCIN/CLKIN input is stabilized after this number of cycles

        0 OSCIN/CLKIN input is not yet stable. Oscillator counter is not finished counting
        1 Oscillator counter is done counting and OSCIN/CLKIN input is assumed to be stable

    b5 - reserved, always read as 0, always write a 0 to this location

    b4 - PLLPWRDN, PLL power-down mode select bit
        0 PLL is operational
        1 PLL is placed in power-down state

    b3 - PLLRST, PLL reset bit
        0 PLL reset is released
        1 PLL reset is asserted

    b2 - OSCPWRDN, Oscillator power-down mode command bit
        0 No effect, write of 0 clears bit to 0
        1 Oscillator power-down command
        Write of 1 causes oscillator power-down command
        Once set, OSCPWRDN remains set but further writes of 1 can reinitiate the oscillator
        power-down command

    b1 - reserved, always read as 0, always write a 0 to this location

    b0 - PLLEN, PLL enable bit
        0 Bypass mode, divider D0 and PLL are bypassed
          SYSCLK1/SYSCLK2/SYSCLK3 are divided down directly from input reference clock
        1 PLL mode, divider D0 and PLL are not bypassed
          PLL output path is enabled, SYSCLK1/SYSCLK2/SYSCLK3 are divided down from PLL output
    */



    // When PLLEN is off DSP is running with CLKIN clock source, currently 25MHz or 40ns clk rate.
    *(volatile INT32U *)PLL_CSR  &= ~0x00000001; // clear the enable bit (Bypass mode)

    *(volatile INT32U *)PLL_CSR  &= ~0x00000010; // clear power-down, PLL is operational

    *(volatile INT32U *)PLL_CSR  |= 0x00000008; // activate PLL reset, PLL takes 125ns to reset

    /*
    PLL_DIV0 : PLL Controller Divider Register 0 : 0x41000114 after reset 0x00008000

    b31
    ..
    b16 - reserved, always read as 0, a value written to this field has no effect

    b15 - D0EN, divider D0 enable bit
        0 divider 0 is disabled, no clock output
        1 divider 0 is enabled

    b14
    ..
    b5 - reserved, always read as 0, a value written to this field has no effect

    b4,b3,b2,b1,b0 - RATIO
         [1F..00] divide frequency by (value+1)

    when PLLEN = 0 (bypass mode) D0 and PLL are bypassed and the input reference clock is directly
    input to dividers D1, D2, and D3

    when PLLEN = 1 (PLL mode) the input reference clock is supplied to divider D0 then divided down by
    the value in the PLL divider ratio bits
    The output from divider D0 is input to the PLL
    The PLL multiplies the clock by the value in the PLL multiplier bits (PLLM)
    The output from the PLL (PLLOUT) is input to dividers D1, D2, and D3
    */

    // PLLOUT = CLKIN/(DIV0.RATIO+1) * PLLM
    // 300MHz = 25MHz/1 * 12

    *(volatile INT32U *)PLL_DIV0 = 0x00008000; // divider D0 enable with ratio 0 (so divides by 1)

    /*
    PLL_M : PLL Multiplier Control Register : 0x41000110 after reset 0x0000000D

    b31
    ..
    b5 - reserved, always read as 0, a value written to this field has no effect

    b4,b3,b2,b1,b0 - PLLM
         [1F..1A] reserved
         [19..04] divide frequency by (value+1)
         [03..00] reserved
    */
    *(volatile INT32U *)PLL_M = 12;

    /*
    Program in reverse order
    DSP requires that peripheral clocks be less then 1/2 the CPU clock at all times
    Order not critical, since changes need GO to come into effect
    */

    /*
    EMIF
    PLL_DIV3 : PLL Controller Divider Register 3 : 0x41000120 after reset 0x00008002

    b31
    ..
    b16 - reserved, always read as 0, a value written to this field has no effect

    b15 - D0EN, divider D0 enable bit
        0 divider 0 is disabled, no clock output
        1 divider 0 is enabled

    b14
    ..
    b5 - reserved, always read as 0, a value written to this field has no effect

    b4,b3,b2,b1,b0 - RATIO
         [1F..00] divide frequency by (value+1).

    when PLLEN = 0 (bypass mode) D0 and PLL are bypassed and the input reference clock is directly
    input to dividers D1, D2, and D3

    when PLLEN = 1 (PLL mode) the input reference clock is supplied to divider D0 then divided down by
    the value in the PLL divider ratio bits
    The output from divider D0 is input to the PLL
    The PLL multiplies the clock by the value in the PLL multiplier bits (PLLM)
    The output from the PLL (PLLOUT) is input to dividers D1, D2, and D3

    The output clock of divider D3 has 50% duty cycle and is SYSCLK3
    */

    *(volatile INT32U *)PLL_DIV3 = 0x00008002;// divider D3 enable with ratio 2 (so divides by 3)

    /*
    Periph & dMAX
    PLL_DIV2 : PLL Controller Divider Register 2 : 0x4100011C after reset 0x00008001

    b31
    ..
    b16 - reserved, always read as 0, a value written to this field has no effect

    b15 - D0EN, divider D0 enable bit
        0 divider 0 is disabled, no clock output
        1 divider 0 is enabled

    b14
    ..
    b5 - reserved, always read as 0, a value written to this field has no effect

    b4,b3,b2,b1,b0 - RATIO
         [1F..00] divide frequency by (value+1).

    when PLLEN = 0 (bypass mode) D0 and PLL are bypassed and the input reference clock is directly
    input to dividers D1, D2, and D3

    when PLLEN = 1 (PLL mode) the input reference clock is supplied to divider D0 then divided down by
    the value in the PLL divider ratio bits
    The output from divider D0 is input to the PLL
    The PLL multiplies the clock by the value in the PLL multiplier bits (PLLM)
    The output from the PLL (PLLOUT) is input to dividers D1, D2, and D3

    The output clock of divider D2 has 50% duty cycle and is SYSCLK2
    */

    *(volatile INT32U *)PLL_DIV2 = 0x00008001;// divider D2 enable with ratio 1 (so divides by 2)

    /*
    CPU & memory
    PLL_DIV1 : PLL Controller Divider Register 1 : 0x41000118 after reset 0x00008000

    b31
    ..
    b16 - reserved, always read as 0, a value written to this field has no effect

    b15 - D0EN, divider D0 enable bit
        0 divider 0 is disabled, no clock output
        1 divider 0 is enabled

    b14
    ..
    b5 - reserved, always read as 0, a value written to this field has no effect

    b4,b3,b2,b1,b0 - RATIO
         [1F..00] divide frequency by (value+1).

    when PLLEN = 0 (bypass mode) D0 and PLL are bypassed and the input reference clock is directly
    input to dividers D1, D2, and D3

    when PLLEN = 1 (PLL mode) the input reference clock is supplied to divider D0 then divided down by
    the value in the PLL divider ratio bits
    The output from divider D0 is input to the PLL
    The PLL multiplies the clock by the value in the PLL multiplier bits (PLLM)
    The output from the PLL (PLLOUT) is input to dividers D1, D2, and D3

    The output clock of divider D1 has 50% duty cycle and is SYSCLK1
    */

    *(volatile INT32U *)PLL_DIV1 = 0x00008000;// divider D1 enable with ratio 0 (so divides by 1)

    /*
    PLL_CMD : PLL Controller Command Register : 0x41000138 after reset 0x00000000

    b31
    ..
    b1 - reserved, always read as 0, a value written to this field has no effect
    b0 - GOSET, GO operation command for SYSCLK rate change and phase alignment
        0 no effect
        1 Initiates GO operation, once set, GOSET remains set but further writes of 1 can reinitiate the GO
    */

    //-----------------------------------------------------------------------------------
    *(volatile INT32U *)GPIODAT1 = 0x0002; // TP0 = 0, TP1 = 1
    //-----------------------------------------------------------------------------------
    /*
    All SYSCLKs must be aligned; therefore, the ALNn bits in ALNCTL should always be set to 1
    before a GO operation

    ALN_CTL : PLL Controller Clock Align Control Register : 0x41000140 after reset 0x00000000

    b31
    ..
    b3 - reserved, always read as 0, a value written to this field has no effect

    b2 - ALN3, SYSCLK3 alignment
        0 Do not use this setting
          Do not change SYSCLK3 divide ratio nor align SYSCLK3 to other SYSCLKs during GO operation
          SYSCLK3 is left free-running when the GOSET bit in PLLCMD is set to 1
        1 Use this setting
          Align SYSCLK3 to other SYSCLKs selected in ALNCTL when the GOSET bit in PLLCMD is set to 1
          Change SYSCLK3 rate to the ratio programmed in the RATIO bit in PLLDIV3.

    b1 - ALN2, SYSCLK2 alignment
        0 Do not use this setting
          Do not change SYSCLK2 divide ratio nor align SYSCLK2 to other SYSCLKs during GO operation
          SYSCLK2 is left free-running when the GOSET bit in PLLCMD is set to 1
        1 Use this setting
          Align SYSCLK2 to other SYSCLKs selected in ALNCTL when the GOSET bit in PLLCMD is set to 1
          Change SYSCLK2 rate to the ratio programmed in the RATIO bit in PLLDIV2

    b0 - ALN1, SYSCLK1 alignment
        0 Do not use this setting
          Do not change SYSCLK1 divide ratio nor align SYSCLK1 to other SYSCLKs during GO operation
          SYSCLK1 is left free-running when the GOSET bit in PLLCMD is set to 1
        1 Use this setting
          Align SYSCLK1 to other SYSCLKs selected in ALNCTL when the GOSET bit in PLLCMD is set to 1
          Change SYSCLK1 rate to the ratio programmed in the RATIO bit in PLLDIV1

    */

    *(volatile INT32U *)ALN_CTL  = 0x00000007; // all SYSCLK3, SYSCLK2, SYSCLK1 will be aligned

    *(volatile INT32U *)PLL_CMD  = 0x00000001; // cmd GoSet

    /*
    PLL_STAT : PLL Controller Status Register : 0x41000138 after reset 0x00000000

    b31
    ..
    b1 - reserved, always read as 0, a value written to this field has no effect
    b0 - GOSTAT, GO operation status.
        0 GO operation is not in progress (SYSCLK divide ratios are not being changed)
        1 GO operation is in progress (SYSCLK divide ratios are being changed and SYSCLKs are being aligned)
    */

    // Read the GOSTAT bit in PLLSTAT to make sure the bit goes back to 0 to indicate that the GO operation has completed
    while (*(volatile INT32U *)PLL_STAT  == 0x00000001)
    {}

    *(volatile INT32U *)PLL_CSR &= ~0x00000008; // clear b3 PLLRST to bring PLL out of reset

    //-----------------------------------------------------------------------------------
    *(volatile INT32U *)GPIODAT1 = 0x0003; // TP0 = 1, TP1 = 1
    //-----------------------------------------------------------------------------------

    //  wait for PLL to lock
    /*
    b6 - STABLE, Oscillator input stable bit
        Indicates if the OSCIN/CLKIN input has stabilized
        The STABLE bit is set to 1 after the reset controller counts 4096 OSCIN or CLKIN input
        clock cycles after the RESET signal is asserted high
        It is assumed that the OSCIN/CLKIN input is stabilized after this number of cycles

        0 OSCIN/CLKIN input is not yet stable. Oscillator counter is not finished counting
        1 Oscillator counter is done counting and OSCIN/CLKIN input is assumed to be stable
    */
    while ((*(volatile INT32U *)PLL_CSR & 0x00000040) != 0x00000040)
    {}

    *(volatile INT32U *)PLL_CSR |= 0x00000001; // set PLLCSR.PLLEN = 1 to enable PLL mode
    // Now running PLL a 300MHz
    // SysClk1 : 300MHz        CPU and Memory (Max 300MHz)
    // SysClk2 : 150MHz        Peripheral and dMax
    // SysClk3 : 100MHz        EMIF (Max 133MHz when CPU 266MHz)

    // with 100 MHz SDRAM.


    //-----------------------------------------------------------------------------------
    *(volatile INT32U *)GPIODAT1 = 0x0001; // TP0 = 1, TP1 = 0
    //-----------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------
    // EMIF setup
    *(REG8U *)(EMIF_SDCR + 3) = 0x00;       // Exit self-refresh mode after setting PLL (see DSP Doc 2.4.5)

    *(volatile INT32U *)EMIF_SDTIMR   = 0x31114610;
    *(volatile INT32U *)EMIF_SDSRETR  = 0x00000006;
    *(volatile INT32U *)EMIF_SDRCR    = 0x0000061a;
    *(volatile INT32U *)EMIF_SDCR     = 0x00000721;    // 32Bit, CAS=2, 4 banks, 9 columns
    *(volatile INT32U *)EMIF_A1CR     = 0x086225be;    // WE strobe mode, 32bit

    //-----------------------------------------------------------------------------------------------------------
    // Universal Host Port Interface - Configure GPIO pins
    // UHPI_H[0] = TP0
    // UHPI_H[1] = TP1
    // UHPI_H[2] = 32bits access to PLD DPRAM
    //-----------------------------------------------------------------------------------------------------------
    // Tell PLD that the DSP EMIF databus size is 32b
    /*
    GPIODAT1 : General Purpose I/O Data Register 1 : 0x43000014 after reset 0x00000000

    b31
    ..
    b0
       when GPIODIR1 as input read is the input value (write has no effect)
       when GPIODIR1 as ouptut write is the input value, read is the readback of the output value
    */
    *(volatile INT32U *)GPIODAT1 = 0x0004;        // (b2=1 -> 32bits access to PLD DPRAM)
    //-----------------------------------------------------------------------------------
    // Configure UHPI GPIOs
    //  *(volatile INT32U *)GPIOINT  = 0;
    //  *(volatile INT32U *)GPIOEN   = 0x00001FEF;
    //  *(volatile INT32U *)GPIODIR2 = 0;
    //  *(volatile INT32U *)GPIODAT2 = 0;
    //  *(volatile INT32U *)GPIODIR3 = 0;
    //  *(volatile INT32U *)GPIODAT3 = 0;
    //-----------------------------------------------------------------------------------

    // Enable L1 Program Cache

    Hw_cache_enable();
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: hardware_setup.c
\*---------------------------------------------------------------------------------------------------------*/
