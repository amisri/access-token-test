/*---------------------------------------------------------------------------------------------------------*\
  File:     cyc.c

  Purpose:  FGC DSP Software - contains cycling related functions
\*---------------------------------------------------------------------------------------------------------*/

#define CYC_GLOBALS
#define CYC_GLOBALS_FAR

#include <cyc.h>
#include <cc_types.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <dsp_dependent.h>      // for ToIEEE(), DISABLE_TICKS/ENABLE_TICKS
#include <string.h>             // for memset()
#include <rtcom.h>              // for rtcom global variable
#include <main.h>               // for state_pc global variable
#include <meas.h>               // for meas global variable
#include <macros.h>             // for Set(), Clr(), Test()
#include <debug.h>              // for db global variable
#include <ref.h>                // for PPM_ALL_USERS
#include <reg.h>                // for reg global variable
#include <refto.h>              // for RefToEnd()
#include <ppm.h>                // for ppm[]
#include <isr.h>
#include <mcu_dsp_common.h>
#include <bgp.h>
#include <report.h>
#include <pc_state.h>
#include <log_signals.h>
#include <log_signals_class.h>
#include "libfg/zero.h"

/*---------------------------------------------------------------------------------------------------------*/

static void CycSendMCUTimingLog(INT32U empty_cycle_f);

/*---------------------------------------------------------------------------------------------------------*/
void CycEnter(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when entering TO_CYCLING state
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U    user;

    // Reset property REF.CYC.WARNING

    for (user = 0 ; user <= FGC_MAX_USER ; user++)
    {
        CycResetCheckWarning(user);
    }

    // Reset and notification of property REF.CYC.FAULT

    CycResetCheckFault();

    // Reset LOG.CYC related properties

    memset(&(property.log.cyc.status), 0, sizeof(property.log.cyc.status));
    memset(&(property.log.cyc.max_abs_err), FP32ZERO, sizeof(property.log.cyc.max_abs_err));

    // clear on STATUS.WARNING the bit FGC_WRN_CYCLE_START, FGC_WRN_TIMING_EVT, ...
    rtcom.warnings &= ~STRETCHED_CYC_WARNINGS;

    cyc.dysfunction_code       = 0;
    cyc.dysfunction_is_a_fault = FALSE;

    cyc.consecutive_wrn_cnt = 0;

#if (FGC_CLASS_ID == 61)
    // Switch to END reference with initial openloop voltage equal to 0 Volts

    DISABLE_TICKS;
    RefResetValue(&ref.v, 0.0);
    refto.flags.end = TRUE;             // Will call RefToEnd() on the next regulation ISR.
    ENABLE_TICKS;

#endif

    // Re-arm references for all cycling users, copy the armed reference to the cycling one, if necessary

    for (user = 1; user <= FGC_MAX_USER; user++)
    {
        ppm[user].cycling.func_type = dpcls->dsp.ref.func.type[user];
    }

    // Reset cycle preparation state and wait for the next user event

    CycCancelNextUser();

    // Reset LOG.OASIS property

    LogSignalOasisReset(&log_oasis_ctrl);
}
/*---------------------------------------------------------------------------------------------------------*/
void CycExit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when exiting CYCLING state
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U    user;

    ref.cycling_f                   = FALSE;                // Prevent the ISR from starting another cycle

    CycCancelNextUser();                                    // Cancel next cycle user

    cyc.ref_user                    = 0;
    cyc.ref_func_type               = FGC_REF_NONE;
    reg.max_abs_err_enable_time     = 0.0;

    // Transfer stretched warnings to warnings and then clear stretched warning

    rtcom.warnings |= cyc.stretched_warnings;
    cyc.stretched_warnings = 0;

    // Disarm cycling reference functions

    for (user = 0; user <= FGC_MAX_USER; user++)
    {
        ppm[user].cycling.func_type = FGC_REF_NONE;
    }

    dpcls->dsp.ref.stc_func_type = 0;
    dpcls->dsp.ref.max           = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void CycCancelNextUser(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will cancel the next programmed user and restore the DSP in a state where it is waiting
  for another user event.

  This function can be called from ISR or BGP context.
\*---------------------------------------------------------------------------------------------------------*/
{
    dpcom->mcu.evt.next_user_delay_ms = 0;           // Order matters
    cyc.user_evt_f                    = FALSE;

    // Clear ISR parameter switch flags (if set) related to the next cycle

    DISABLE_TICKS;

    if (Test(isr_switch_request_flags, ISR_SWITCH_REF) &&
        Test(isr_switch_cycling_flags, ISR_SWITCH_REF))
    {
        Clr(isr_switch_request_flags, ISR_SWITCH_REF);
        Clr(isr_switch_cycling_flags, ISR_SWITCH_REF);
    }

    if (Test(isr_switch_request_flags, ISR_SWITCH_REG) &&
        Test(isr_switch_cycling_flags, ISR_SWITCH_REG))
    {
        Clr(isr_switch_request_flags, ISR_SWITCH_REG);
        Clr(isr_switch_cycling_flags, ISR_SWITCH_REG);
    }

    ENABLE_TICKS;

    cyc.next_cycle_state = NEXT_CYCLE_WAIT_USER_EVENT;          // Reset next cycle preparation state machine
}
/*---------------------------------------------------------------------------------------------------------*/
void CycStartSuperCycle(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a start super-cycle event is received -- this is normally 20ms after the
  start of the super-cycle.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Stretch cycle related warnings to be at least one super-cycle to avoid toggling when CYCLING

    if (ref.cycling_f)
    {
        cyc.stretched_warnings = rtcom.warnings & STRETCHED_CYC_WARNINGS;
        Clr(rtcom.warnings, STRETCHED_CYC_WARNINGS);
    }
    else
    {
        // even when not cycling
        // clear general warnings (like FGC_WRN_CYCLE_START) that are not
        // that are not clear individually (like FGC_WRN_I_RMS_LIM)
        Clr(rtcom.warnings, STRETCHED_CYC_WARNINGS);
    }

    // Assert TIMING_EVT warning if supercycle is simulated

    if (dpcom->mcu.evt.sim_supercycle_f)
    {
        Set(rtcom.warnings, FGC_WRN_TIMING_EVT);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_TIMING_EVT);
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void CycStartCycle(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to start a new cycle on C0 (millisecond 0 of the cycle).
\*---------------------------------------------------------------------------------------------------------*/
{
    // Start of cycle time in secs and usecs

    dpcom->dsp.cyc.prev_start_time = cyc.start_time;

    cyc.start_time = rtcom.time_now_us;

    // Get timing_user and cycle duration for that user

    cyc.timing_user = dpcom->mcu.evt.next_user;

    // Get cyc.user based on DEVICE.PPM, REG.TEST.USER

    reg.test_user_f = (cyc.timing_user && cyc.timing_user == property.reg.test.user);

    cyc.prev_user   = cyc.user;

    cyc.user        = cyc.next_user;

    // Once in CYCLING state set the cycling flag to start first cycle

    if (state_pc == FGC_PC_CYCLING)
    {
        ref.cycling_f = TRUE;
    }

    // If cycling is active

    if (ref.cycling_f)
    {
#if (FGC_CLASS_ID == 61)
        INT32U reg_state;

        // Enable function for cyc.ref_user and set regulation state

        reg_state = CycEnableRefFunction();

        // With asynchronous DSP, the regulation parameters for the new cycle are prepared in advance
        // and the switch to the set of parameters is already done at this stage.

        // Only in the case where an error is detected at the start of the cycle do we need to
        // switch to V regulation

        if ((reg_state == FGC_REG_V)
            && (reg.active->state != FGC_REG_V)
           )
        {
            RegStateV(TRUE, ref.v.value);
        }

#endif

        dpcls->dsp.ref.stc_func_type = (cyc.ref_func_type ?        ppm[cyc.ref_user].cycling.stc_func_type   : 0);
        dpcls->dsp.ref.max           = (cyc.ref_func_type ? ToIEEE(ppm[cyc.ref_user].cycling.meta.range.max) : 0);


        // Arm BIV checks to happen at time REF.START.TIME + REF.START.DURATION

        cyc.biv_check_f     = TRUE;
        cyc.biv_check_time  = property.ref.start.time + property.ref.start.duration;

        ref.reg_bypass_mask = DEFAULT_REF_REG_BYPASS;

        // Enable regulation max_abs_err logging from start of first plateau

        reg.max_abs_err_enable_time = ppm[cyc.ref_user].cycling.first_plateau.time;
    }

    // LOG.TIMING : starting a new cycle

    CycSendMCUTimingLog(FALSE);

    // Reset cycling events from MCU

    cyc.ssc_evt_f    = FALSE;
    cyc.user_evt_f   = FALSE;
    dpcom->mcu.evt.ssc_f              = FALSE;
    dpcom->mcu.evt.next_user_delay_ms = 0;

    // Start capture log

    LogSignalCaptureStart();

    // LOG.OASIS

    LogSignalsOasisStartCycle(&log_oasis_ctrl);
}
/*---------------------------------------------------------------------------------------------------------*/
void CycSendMCUCheckResults(void)
/*---------------------------------------------------------------------------------------------------------*\
  Send data to MCU for the timing log.
  This function is supposed to be called at the end of every cycle.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Update warning and fault flag for the past cycle

    dpcom->dsp.cyc.dysfunction_code       = cyc.dysfunction_code;
    dpcom->dsp.cyc.dysfunction_is_a_fault = cyc.dysfunction_is_a_fault;

    // Reset warning/fault for last cycle. If clean run, also reset REF.CYC.WARNING, and cyc.consecutive_wrn_cnt

    if (cyc.dysfunction_code == 0)
    {
        CycResetCheckWarning(cyc.user);
        cyc.consecutive_wrn_cnt = 0;
    }

    cyc.dysfunction_code = 0;
    cyc.dysfunction_is_a_fault = FALSE;
}
/*---------------------------------------------------------------------------------------------------------*/
static void CycSendMCUTimingLog(INT32U empty_cycle_f)
/*---------------------------------------------------------------------------------------------------------*\
  Send data to MCU for the timing log
\*---------------------------------------------------------------------------------------------------------*/
{
    // Timing data for the new cycle

    dpcom->dsp.cyc.start_time = cyc.start_time;

    if (!empty_cycle_f)
    {
        dpcom->dsp.cyc.user_timing = cyc.timing_user;
        dpcom->dsp.cyc.prev_user   = dpcom->dsp.cyc.user;
        dpcom->dsp.cyc.user        = cyc.user;
    }

    // Trigger MCU

    dpcom->dsp.cyc.start_cycle_f = TRUE;
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CycPrepareRefFunction(void)
/*---------------------------------------------------------------------------------------------------------*\
  [FGC3]
  This function is called as soon as the user event for the next cycle is received.
  It prepares the reference function for the next cycle and triggers a reference switch to be performed by the
  ISR at the beginning of that cycle (i.e., on C0).

  This function is called in BGP (background task) context.

  This function returns FALSE if it cannot be executed immediately due to a pending REF parameter switch
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U       timing_user;
#if (FGC_CLASS_ID == 61)
    struct ppm * ppm_user;
    INT32U       ref_func_type;
#endif

    if (Test(isr_switch_request_flags, ISR_SWITCH_REF))   // Early return if there is a parameter switch pending
    {
        return FALSE;
    }

    timing_user   =  dpcom->mcu.evt.next_user;
    cyc.next_user = timing_user;

    if (cyc.timing_user
        && (cyc.timing_user == property.reg.test.user))     // If test user
    {
        cyc.next_ref_user = property.reg.test.ref_user;
    }
    else
    {
        cyc.next_ref_user = (dpcom->mcu.device_ppm ? cyc.next_user : 0);
    }

#if (FGC_CLASS_ID == 61)

    ppm_user = ppm + cyc.next_ref_user;

    ref_func_type = ppm_user->cycling.func_type;

    // If reference is armed/cycling, and if REF.FUNC.PLAY(user) == ENABLED

    if (ref_func_type == FGC_REF_NONE)
    {
        ref.next->func_type = FGC_REF_ZERO;
        ref.next->reg_mode  = FGC_REG_I;
        fgZeroArm(NULL);
    }
    else if (dpcls->mcu.ref.func.play[cyc.next_ref_user] == FGC_CTRL_ENABLED)
    {
        // Copy function details from ppm_user->cycling to ref.next (the reference data for the next cycle user)

        *ref.next = ppm_user->cycling;          // Long memcpy

        // In the case of table, the pointers to the table data must be updated

        switch (ref_func_type)
        {
            case FGC_REF_TABLE:
                ref.next->pars.table.function = &ref.next->table_data.function[0];
                break;

            default:
                break;
        }
    }
    else
    {
        // Set the function type to NONE since it has been disabled. Regulation (V,I,B)
        // still needs to be done though so the parameters for this user are used.

        *ref.next = ppm_user->cycling;
        ref.next->func_type = FGC_REF_ZERO;
        ref.next->reg_mode  = FGC_REG_I;
        fgZeroArm(NULL);
    }

#endif

    // The reference for the next cycle is now ready
    // Request the ISR to switch the reference context on the next C0 interrupt.

    DISABLE_TICKS;
    Set(isr_switch_cycling_flags, ISR_SWITCH_REF);                      // Order of the two statements matters
    Set(isr_switch_request_flags, ISR_SWITCH_REF);
    ENABLE_TICKS;

    return TRUE;
}

INT32U CycEnableRefFunction(void)
/*---------------------------------------------------------------------------------------------------------*\
  [FGC2, FGC3]
  This function is called on the first iteration of every cycle to enable a reference function based on the
  reference user for the new cycle.  It returns the regulation state for the cycle.

  This function has a very different behaviour on a synchronous DSP (the case of FGC2) and on an
  asynchronous one (the case of FGC3). On FGC3, the function reference is already copied and ready to use
  in ref.active structure. The ISR simply needs to switch to it.

  This function is called in ISR context.
\*---------------------------------------------------------------------------------------------------------*/

{
    /*
     * The function reference is already copied and ready to use in ref.active
     * structure. The ISR simply needs to switch to it. This function is called
     * in ISR context.
     */

    INT32U       reg_state;

    // Set user for ref function according to reg test flag and REF.TEST.REF_USER property

    cyc.ref_user      = cyc.next_ref_user;
    reg_state         = ref.active->reg_mode;
    cyc.ref_func_type = ref.active->func_type;

#if (FGC_CLASS_ID == 61)

    struct ppm * ppm_user;

    ppm_user = ppm + cyc.ref_user;
    dpcom->dsp.cyc.reg_mode = ppm_user->cycling.reg_mode;

    if ( (ref.active->func_type == FGC_REF_NONE) ||
         (ref.active->func_type == FGC_REF_ZERO) 
       )
    {
        // Cycle check - REF_NOT_ARMED / REF_DISABLED
        // NB: The REF_DISABLED cycle warning does not trigger an FGC warning.

        CycSaveCheckWarning(DONT_TRIGG_LOG,
                            (dpcls->mcu.ref.func.play[cyc.ref_user] == FGC_CTRL_DISABLED ? FGC_CYC_WRN_REF_DISABLED :
                             FGC_CYC_WRN_REF_NOT_ARMED),
                            (dpcls->mcu.ref.func.play[cyc.ref_user] == FGC_CTRL_DISABLED ? 0                        :
                             FGC_WRN_CYCLE_START),
                            (FP32)cyc.timing_user,                              // REF.CYC.WARNING.DATA[0]
                            (FP32)cyc.user,                                     // REF.CYC.WARNING.DATA[1]
                            (FP32)cyc.ref_user,                                 // REF.CYC.WARNING.DATA[2]
                            (FP32)dpcls->mcu.ref.func.type[cyc.ref_user]);      // REF.CYC.WARNING.DATA[3]

        cyc.ref_func_type = (cyc.ref_func_type == FGC_REF_NONE) ? FGC_REF_NONE : FGC_REF_ZERO;
        ref.active->min_basic_periods = 0;      // Don't risk a REF_TOO_LONG warning
    }

    if (cyc.ref_func_type == FGC_REF_NONE &&
        reg_state == FGC_REG_V)
    {
        RefResetValue(&ref.fg, 0.0);
        RefResetValue(&ref.v,  0.0);
    }

#endif
    // Enabled generation of the reference

    ref.func_type = cyc.ref_func_type;

    // Return the REG state for this cycle

    return (reg_state);
}

/*---------------------------------------------------------------------------------------------------------*/
void CycResetCheckWarning(INT32U user)
/*---------------------------------------------------------------------------------------------------------*\
  This function will reset the cycling check results for the supplied user, and notify the property.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (ppm[user].cycling.warning.chk != FGC_CYC_WRN_NONE)
    {
        ppm[user].cycling.warning.chk = FGC_CYC_WRN_NONE;

        // Notification of REF.CYC.WARNING

        dpcls->dsp.notify.ref_cyc_warning = user;
    }

    memset(&(ppm[user].cycling.warning.data), FP32ZERO, sizeof(ppm[user].cycling.warning.data));
}
/*---------------------------------------------------------------------------------------------------------*/
void CycResetCheckFault(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will reset the cycling fault, and notify the property.
\*---------------------------------------------------------------------------------------------------------*/
{
    dpcls->dsp.cyc.fault.user = 0;
    dpcls->dsp.cyc.fault.chk  = FGC_CYC_FLT_NONE;

    memset(&(property.ref.cyc.fault.data), FP32ZERO, sizeof(property.ref.cyc.fault.data));
    // Notification of REF.CYC.FAULT

    dpcls->dsp.notify.ref_cyc_fault = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void CycSaveCheckWarning(INT32U trig_log_capture_f,
                         INT32U check_wrn,
                         INT32U warning,
                         FP32 data0,
                         FP32 data1,
                         FP32 data2,
                         FP32 data3)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called if a cycling check fails.  It records the check and the check data and possibly
  capture a log.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct ppm * ppm_user = ppm + cyc.user;

    rtcom.warnings |= warning;

    dpcom->dsp.warnings = rtcom.warnings | cyc.stretched_warnings;

    // ToDo I guess is worth the following info even when not cycling
    // as a warning is already reported
    if (ref.cycling_f)
    {
        ppm_user->cycling.warning.chk     = check_wrn;
        ppm_user->cycling.warning.data[0] = data0;
        ppm_user->cycling.warning.data[1] = data1;
        ppm_user->cycling.warning.data[2] = data2;
        ppm_user->cycling.warning.data[3] = data3;

        // Notification of property REF.CYC.WARNING(cyc.user)

        dpcls->dsp.notify.ref_cyc_warning = cyc.user;

        if (trig_log_capture_f
            && (dpcls->dsp.log.capture.state == FGC_LOG_RUNNING)
            && !dpcls->dsp.log.capture.cyc_chk_time.unix_time
           )
        {
            log_capture.freeze_f                    = TRUE;              // will freeze the log at the end of cycle
            dpcls->dsp.log.capture.cyc_chk_time = cyc.start_time;
            dpcls->dsp.log.capture.user         = cyc.user;
        }

        // For LOG.TIMING

        cyc.dysfunction_code       = check_wrn;
        cyc.dysfunction_is_a_fault = FALSE;            // is a Warning

        // Count consecutive warnings. A clean cycle (without any warning) shall reset that counter.

        if ((check_wrn != FGC_CYC_WRN_REF_NOT_ARMED) &&
            (check_wrn != FGC_CYC_WRN_REF_DISABLED))
        {
            cyc.consecutive_wrn_cnt++;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CycSaveCheckFault(INT32U check_flt, INT32U warning, FP32 data0, FP32 data1, FP32 data2, FP32 data3)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called if cycling fails and we return to ON_STANDBY state.
  It records the check and the check data of the fault.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 61)
    rtcom.warnings |= warning;
    dpcom->dsp.warnings = rtcom.warnings | cyc.stretched_warnings;

    // ToDo I guess is worth the following info even when not cycling
    // as a warning is already reported
    if (ref.cycling_f
        && !dpcls->dsp.cyc.to_standby_f
       )
    {
        dpcls->dsp.cyc.fault.user       = cyc.user;
        dpcls->dsp.cyc.fault.chk        = check_flt;
        property.ref.cyc.fault.data[0]  = data0;
        property.ref.cyc.fault.data[1]  = data1;
        property.ref.cyc.fault.data[2]  = data2;
        property.ref.cyc.fault.data[3]  = data3;

        // Notification of property REF.CYC.FAULT

        dpcls->dsp.notify.ref_cyc_fault = 0;

        // Trigger the STANDBY flag

        dpcls->dsp.cyc.to_standby_f     = TRUE;

        RegStateV(TRUE, 0.0);

        // For LOG.TIMING

        cyc.dysfunction_code       = check_flt;
        cyc.dysfunction_is_a_fault = TRUE;        // is a Fault

        // Report the fault to the MCU

        CycSendMCUCheckResults();
        CycSendMCUTimingLog(TRUE);   // dummy log entry
    }

#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void CycLogAbsMaxErr(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a cycling function finishes to record and check the max absolute error
  recorded during the cycle.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 61)

    if (reg.active->state != FGC_REG_V && TestRefCheckLimits())
    {
        property.log.cyc.max_abs_err[cyc.user] = reg_bi_err.max_abs_err;

        if (reg_bi_err.fault_limit > 0.0 &&
            reg_bi_err.fault_limit < reg_bi_err.max_abs_err)
        {
            property.log.cyc.status[cyc.user] = FGC_CYC_STAT_FAULT;
        }
        else if (reg_bi_err.warning_limit > 0.0 &&
                 reg_bi_err.warning_limit < reg_bi_err.max_abs_err)
        {
            property.log.cyc.status[cyc.user] = FGC_CYC_STAT_WARNING;
        }
        else
        {
            property.log.cyc.status[cyc.user] = FGC_CYC_STAT_OK;
        }
    }

    regErrInitVars(&reg_bi_err);
#endif
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cyc.c
\*---------------------------------------------------------------------------------------------------------*/

