/*---------------------------------------------------------------------------------------------------------*\
  File:     reg.c

  Purpose:  FGC DSP Software - contains RT regulation functions

  Notes:    All real-time processing runs at interrupt level under IsrPeriod().
\*---------------------------------------------------------------------------------------------------------*/

#include <reg.h>
#include <cc_types.h>
#include <string.h>             // for memset
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <rtcom.h>              // for rtcom global variable
#include <meas.h>               // for meas global variable
#include <adc.h>                // for adc, adc_chan global variables
#include <sim.h>                // for sim global variable
#include <pars.h>               // for ParsRegV()
#include <macros.h>             // for Test()
#include <dsp_time.h>           // for DSP_ROUND_TIME_MS()
#include <isr.h>
#include <bgp.h>
#include <bgp.h>
#include <mcu_dsp_common.h>
#include <dsp_dependent.h>      // for DISABLE_TICKS/ENABLE_TICKS
#include <main.h>

/*---------------------------------------------------------------------------------------------------------*/
void RegStatePrepare()
/*---------------------------------------------------------------------------------------------------------*\
  [FGC3]
  In RUNNING operation, this function is called to change the regulation state

  In CYCLING operation, this function is called for each cycle to set the regulation state for the next user.
  It can also be called during a cycle, for example if an error is detected.

  This function MUST be called in BGP (background task) context.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Early return if a switch of the REG parameters is already pending

    if (Test(isr_switch_request_flags, ISR_SWITCH_REG))
    {
        return;
    }

    // Regulation mode

    reg.next->state = reg.request.reg_state;    // reg.next->previous_state will be set in RegStateSwitch()

    // V regulation parameters (whatever the regulation state is)

    reg.next->rst_v_pars = property.reg.v.rst_pars;

    switch (reg.request.reg_state)
    {
        case FGC_REG_V:                     // Voltage regulation

            // Copy the V regulation parameters into the generic rst_pars.

            reg.next->rst_pars = reg.next->rst_v_pars;

            break;

        case FGC_REG_I:                     // Current regulation

            if (!reg.test_user_f)                       // Operational parameters
            {
                reg.next->rst_pars = property.reg.i.op;
            }
            else                                        // Test parameters
            {
                regRstInit(&reg.next->rst_pars,
                           property.reg.v.rst_pars.period,
                           property.reg.test.i.period_div,
                           &load.active->pars,
                           0.0, 0.0, 0.0, 0.0,
                           &property.reg.test.i.rst);
            }

            break;

#if (FGC_REG_B_SUPPORT == TRUE)

        case FGC_REG_B:                     // Field regulation

            if (!reg.test_user_f)                       // Operational parameters
            {
                regRstInit(&reg.next->rst_pars,
                           property.reg.v.rst_pars.period,
                           property.reg.b.period_div,
                           &load.active->pars,
                           0.0, 0.0, 0.0, 0.0,
                           &property.reg.b.rst);
            }
            else                                        // Test parameters
            {
                regRstInit(&reg.next->rst_pars,
                           property.reg.v.rst_pars.period,
                           property.reg.test.b.period_div,
                           &load.active->pars,
                           0.0, 0.0, 0.0, 0.0,
                           &property.reg.test.b.rst);
            }

            break;
#endif
    }

    // Trigger a switch between reg.active and reg.next, to happen immediately (if next_cycle_f == FALSE),
    // or at the beginning of the next cycle (if next_cycle_f == TRUE)

    DISABLE_TICKS;

    if (reg.request.next_cycle_f)
    {
        // This set must be before the set of isr_switch_request_flags
        Set(isr_switch_cycling_flags, ISR_SWITCH_REG);
    }

    Set(isr_switch_request_flags, ISR_SWITCH_REG);
    ENABLE_TICKS;

    // BGP process for the requested REG parameter changed has completed.

    // But since the ISR_SWITCH_REG flag is set in isr_switch_request_flags, it implies that
    // reg.request.isr_process_f = TRUE, and so that the REG request remains locked until the
    // ISR actually does the parameter switch.

    reg.request.bgp_process_f = FALSE;                  // BGP process for the REG request is done
}
/*---------------------------------------------------------------------------------------------------------*/
void RegStateSwitch()
/*---------------------------------------------------------------------------------------------------------*\
  [FGC3]
  This function is called right after a REG parameter switch occur in the ISR. It will finalise the transition
  to the new regulation state.

  On asynchronous DSP, a transition to a new regulation state should be first prepared in the BGP task,
  to the exception of V regulation, where function RegStateV can be called directly in ISR context.

  This function runs in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 v_ref;

    // Since this function is called in ISR context and right after the parameter switch,
    // the 'next' parameter structure is in fact the previous 'active' one.

    reg.active->previous_state = reg.next->state;       // reg.next actually is 'reg.prev' in this case

    // Starting Vref:
    //  - reg.request.v_ref if this is not a cycling reg request
    //  - the present ref.v, otherwise.

    if (reg.request.next_cycle_f)       // If CYCLING reg request
    {
        v_ref = ref.v.value;
    }
    else
    {
        v_ref = reg.request.v_ref;
    }

    // According to the new regulation state

    switch (reg.active->state)
    {
        case FGC_REG_V:                     // Voltage regulation

            ref.advance          = 0.001;           // 1 ms
            meas.hist            = &meas.i_hist;
            ref.now              = &ref.i;          // SPY signal REF = 0
            RefResetValue(&ref.fg,            v_ref);
            RefResetValue(&ref.fg_plus_rt,    v_ref);
            RefResetValue(&ref.i,             0.0);
            RefResetValue(&ref.i_clipped,     0.0);
            RefResetValue(&ref.b,             0.0);

            break;

        case FGC_REG_I:                     // Current regulation

            // Initialise I regulation errors

            regErrInitDelay(&reg_bi_err,
                            &reg_bi_ref_history_buffer[0],
                            reg.active->rst_pars.rst.track_delay / reg.active->rst_pars.period,
                            load.active->pars.sim.load_undersampled_flag);

            regErrInitLimits(&reg_bi_err,
                             property.limits.i.err_warning[property.load.select],
                             property.limits.i.err_fault  [property.load.select]);

            RefResetValue(&ref.b, 0.0);

            meas.hist = &meas.i_hist;

            // The following function will recalculate the RST history for a bumpless regulation transition

            RegVarsInit(v_ref, reg.active->rst_pars.period, reg.active->rst_pars.rst.track_delay);

            if (reg.active->previous_state != FGC_REG_I)
            {
                // ref.i will be equal to 0.0 because we come from a different regulation state.
                // Starting from zero, the I reference might need several regulation periods to reach ref.fg.value (because
                // LIMITS.I.RATE would apply), and that can cause a bump on the current.

                RefResetValue(&ref.fg,        reg_vars.ref[0]);
                RefResetValue(&ref.i,         reg_vars.ref[0]);
                RefResetValue(&ref.i_clipped, reg_vars.ref[0]);
            }

            // High-speed current measurement has a delay of 1.3 times the DSP iteration period due to analogue and dig filters
            // Low-speed  current measurement uses a filter with latency compensation, so measurement delay is zero.

            ref.advance = reg.active->rst_pars.rst.track_delay -
                          (TEST_REG_HI_SPEED(reg.active->rst_pars.period) ? IMEAS_DELAY_ITERS * DSP_ITER_PERIOD : 0.0);

            // Ref.now

            ref.now = &ref.i;

            break;

#if (FGC_REG_B_SUPPORT == TRUE)

        case FGC_REG_B:                     // Field regulation

            // Initialise B regulation errors

            regErrInitDelay(&reg_bi_err,
                            &reg_bi_ref_history_buffer[0],
                            reg.active->rst_pars.rst.track_delay / reg.active->rst_pars.period,
                            load.active->pars.sim.load_undersampled_flag);

            regErrInitLimits(&reg_bi_err,
                             property.limits.b.err_warning,
                             property.limits.b.err_fault);

            RefResetValue(&ref.i,         0.0);
            RefResetValue(&ref.i_clipped, 0.0);

            meas.hist = &meas.b_hist;

            // The following function will recalculate the RST history for a bumpless regulation transition

            RegVarsInit(v_ref, reg.active->rst_pars.period, reg.active->rst_pars.rst.track_delay);

            if (reg.active->previous_state != FGC_REG_B)
            {
                // ref.b will be equal to 0.0 because we come from a different regulation state.
                // Starting from zero, the B reference might need several regulation periods to reach ref.fg.value (because
                // LIMITS.B.RATE would apply), and that can cause a bump on the field.

                RefResetValue(&ref.fg, reg_vars.ref[0]);
                RefResetValue(&ref.b,  reg_vars.ref[0]);
            }

            // B-train has no measurement delay so ref advance is simply the regulation tracking delay

            ref.advance = reg.active->rst_pars.rst.track_delay;

            // Ref.now

            ref.now = &ref.b;

            break;
#endif
    }

    // Initialise voltage regulation parameters

    ref.v.value = ref.v_openloop = v_ref;

    // Call to regErrInitFilterPeriod. Regarding the second parameter (filter_period_iters):
    //  - In V regulation, rst_pars = rst_v_pars therefore filter_period_iters = 1.
    //  - In B/I regulation, filter_period_iters is equal to the number of iteration periods
    //    in a regulation period. The V measurement will be averaged on a regulation period.

    regErrInitFilterPeriod(&reg_v_err, reg.active->rst_pars.period_iters);

    regErrInitDelay(&reg_v_err,
                    &reg_v_ref_history_buffer[0],
                    reg.active->rst_v_pars.rst.track_delay / reg.active->rst_v_pars.period,
                    load.active->pars.sim.vs_undersampled_flag);

    regErrInitLimits(&reg_v_err,
                     property.limits.v.err_warning,
                     property.limits.v.err_fault);


    regErrInitVars(&reg_v_err);

    // If hi_speed_f is changing, reset the measurement delay queues

    if (reg.hi_speed_f != TEST_REG_HI_SPEED(reg.active->rst_pars.period))
    {
        reg.hi_speed_f = TEST_REG_HI_SPEED(reg.active->rst_pars.period);

        SimInitDelays(sim.active, ref.v.value, sim.active->v, sim.active->i, sim.active->b);
    }

    // Set the 20ms sliding window filter factor (provides latency compensation)
    // The history offset should correspond to one regulation period
    // The compensation delay, expressed in units of iteration periods, is the sum of two sub-delays:
    //  - The delay introduced by the 20ms average, equal to (N-1)/2 iterations, where N = adc.filter_20ms.nb_of_samples
    //  - The measurement delay, sum of the FIR digital filter delay and the analogue anti-aliasing delay on the
    //    analogue card.

    adc.filter_20ms.history_offset     = (reg.hi_speed_f ? 10 : reg.active->rst_pars.period_iters);
    adc.filter_20ms.compensation_delay = (FP32)(adc.filter_20ms.nb_of_samples - 1) / 2.0 + ADC_MEAS_DELAY_ITERS;
    adc.filter_20ms.time_ratio         =  adc.filter_20ms.compensation_delay / (FP32)
                                          adc.filter_20ms.history_offset;

    // Reinitialise period counter

    if (reg.hi_speed_f)
    {
        // Hi-speed regulation (regulation period <= 10 ms)
        // Reset the period_counter so that we are now at the beginning of a regulation period. This ensure that
        // the recalculated RST history (function RegVarsInit) is perfectly aligned with the new regulation iterations

        dpcls->dsp.reg.run_f = FALSE;
        reg.period_counter   = 0;
    }
    else
    {
        // Lo-speed regulation (regulation period > 10 ms)
        // Calculate reg.period_counter so that the regulation iterations are aligned on a 12s boundary. Note that
        // by doing so the recalculated RST history (function RegVarsInit) might have a slight offset in time,
        // causing a small bump in the regulation.

        reg.period_counter   = ((rtcom.time_now_ms.unix_time % 12) * 1000 + rtcom.time_now_ms.ms_time) %
                               reg.active->rst_pars.period_iters;
    }

    // Calculate reference advance in milliseconds for Start/Abort events for when in ARMED/RUNNING

    ref.advance_us = DSP_ROUND_TIME_US(ref.advance);

    // Clear warnings

    Clr(rtcom.warnings, FGC_WRN_REF_LIM | FGC_WRN_REF_RATE_LIM);

    // Pass new regulation state to MCU if required

    if (reg.request.inform_mcu_f)
    {
        dpcls->dsp.reg.state.int32u = reg.active->state;
    }

    // Gate ADC filters reset

    AdcGatePars();
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN RegStateRequest(BOOLEAN next_cycle_f, BOOLEAN inform_mcu_f, INT32U reg_state, FP32 v_ref)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the MCU requests a change of the regulation state, or at the start of each
  cycle (in CYCLING) to set the regulation state for the next user.

  This function can be called in ISR or BGP context. (This is why the access to reg.request.locked is
  protected against interrupts.)

  next_cycle_f will indicate if the regulation state change is to occur on the start of the next cycle
  (if next_cycle_f == TRUE), or immediately (if next_cycle_f == FALSE).
\*---------------------------------------------------------------------------------------------------------*/
{
    DISABLE_TICKS;                                                               // ENTER CRITICAL SECTION

    // Check if we can unlock the REG request (if locked)
    // This is possible only if the BGP and the ISR processing for the last reg request have completed.

    if (reg.request.locked     &&                           // If REG request is locked
        reg.request.bgp_process_f == FALSE)                  // If BGP process has completed
    {
        reg.request.isr_process_f = Test(isr_switch_request_flags, ISR_SWITCH_REG);

        if (reg.request.isr_process_f == FALSE)             // And no REG parameter switch pending in the ISR
        {
            reg.request.locked = FALSE;                         // Unlock the request
        }
    }

    if (reg.request.locked == FALSE)
    {
        reg.request.locked         = TRUE;
        reg.request.bgp_process_f  = TRUE;
        reg.request.isr_process_f  = TRUE;
        ENABLE_TICKS;                                                            // EXIT CRITICAL SECTION

        reg.request.next_cycle_f   = next_cycle_f;
        reg.request.inform_mcu_f   = inform_mcu_f;
        reg.request.reg_state      = reg_state;
        reg.request.v_ref          = v_ref;

        return TRUE;
    }
    else
    {
        ENABLE_TICKS;                                                            // EXIT CRITICAL SECTION
        return FALSE;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RegStateV(INT32U inform_mcu_f, FP32 v_ref)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to switch rapidly to openloop regulation, in case a regulation error is detected,
  in the END reference function in cycling mode of operation, or simply moving to the OFF state.

  This function supposes that reg.active->rst_v_pars is already set to the correct V regulation parameters.
  Therefore it should be called after an initial call to RegStateRequest(B/I/V) that will initialise that
  structure.

  This function is preferably called in ISR context.
  It can also be called in BGP (background task) context, thanks to the critical section inside the function.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Execute in critical section in case this function is called from the BGP task
    DISABLE_TICKS;

    // Copy the WHOLE openloop regulation structure into the active regulation structure

    reg.active->rst_pars    = reg.active->rst_v_pars;

    ref.advance             = 0.001;                            // ref.advance becomes 1 ms
    meas.hist               = &meas.i_hist;
    ref.now                 = &ref.i;                           // SPY signal REF = 0

    RefResetValue(&ref.v,             v_ref);
    RefResetValue(&ref.fg,            v_ref);
    RefResetValue(&ref.fg_plus_rt,    v_ref);
    RefResetValue(&ref.i,             0.0);
    RefResetValue(&ref.i_clipped,     0.0);
    RefResetValue(&ref.b,             0.0);

    // Initialise voltage regulation parameters

    ref.v_openloop = v_ref;

    // Calculate reference advance in milliseconds for Start/Abort events for when in ARMED/RUNNING

    ref.advance_us = DSP_ROUND_TIME_US(ref.advance);

    // Clear warnings

    Clr(rtcom.warnings, FGC_WRN_REF_LIM | FGC_WRN_REF_RATE_LIM);

    // Disarm reference

    BgpInitNone(FGC_REG_V, 0);                  // First argument is ignored
    ref.func_type = FGC_REF_NONE;

    // Remember previous and new regulation state and pass to MCU if required

    reg.active->previous_state = reg.active->state;
    reg.active->state          = FGC_REG_V;

    // End of critical section
    ENABLE_TICKS;

    if (inform_mcu_f)
    {
        dpcls->dsp.reg.state.int32u = FGC_REG_V;
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void RegPeriod(INT32U * period_vp_ptr, FP32 * period_ptr)
/*---------------------------------------------------------------------------------------------------------*\
  This function converts a regulation period in units of the V regulation periods into a period in seconds,
  with some constraints on the allowed periods.
  - period_vp_ptr: INPUT/OUTPUT. This is the requested period in units of the V regulation period.
                                 The pointed value may be edited to comply with the above-mentioned constraints
  - period_ptr:    OUTPUT.       This is the pointer to the period, in seconds.
\*---------------------------------------------------------------------------------------------------------*/
{
    *period_ptr = (*period_vp_ptr) * property.reg.v.rst_pars.period;
}
/*---------------------------------------------------------------------------------------------------------*/
void RegVarsInit(FP32 v_ref, FP32 reg_period, FP32 track_delay)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the RST history arrays to allow the bumpless closing of the current or field
  loops, in high-speed regulation. To do this it uses the last four measurements to determine the rate of
  change. The voltage reference (supplied) should be constant (or close to constant) during this period.

  Note: We might still see a bump in low-speed regulation if changing the regulation period. The reason for
  that is this function works well in low-speed because we reset reg.period_counter on the transition to the
  new regulation parameters. But one does not do so in low-speed because we have the additional constraint
  of having the regulation periods aligned on 12s boundaries.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U    idx;
    FP32      ref_offset;

    // Prepare RST history to have bumpless loop closing

    ref_offset = meas.hist->rate * track_delay;

    for (idx = 0; idx < FGC_N_RST_COEFFS; idx++)
    {
        reg_vars.act [idx] = v_ref;
        reg_vars.meas[idx] = meas.hist->meas - meas.hist->rate * (FP32)idx * reg_period;
        reg_vars.ref [idx] = reg_vars.meas[idx] + ref_offset;
    }

    regErrInitVars(&reg_bi_err);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: reg.c
\*---------------------------------------------------------------------------------------------------------*/

