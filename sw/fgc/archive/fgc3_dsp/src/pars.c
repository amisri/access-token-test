/*---------------------------------------------------------------------------------------------------------*\
  File:         pars.c

  Purpose:      FGC2 C32 Software - Parameter group set functions.

  Author:       Quentin.King@cern.ch

  Notes:        These functions are called at the end of the millisecond ISR provided it is not a
                10ms millisecond boundary when lots of parameters are sent to the MCU.
\*---------------------------------------------------------------------------------------------------------*/

#define PARS_GLOBALS
#define DEFPROPS                // Force property table definition

#include <cc_types.h>

#include <pars.h>
#include <cc_types.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <dsp_dependent.h>      // for ToIEEE()
#include <rtcom.h>              // for rtcom global variable
#include <mcu_dsp_common.h>     // for IsFiniteFloat
#include <dsp_panic.h>
#include <adc.h>
#include <isr.h>
#include <bgp.h>
#include <macros.h>             // for Test()
#include <unipolar_switch.h>
#include <string.h>             // String library, memset()
#include <cal.h>                // for cal global variable
#include <log_signals_class.h>
#include <lib.h>
#include <defprops_dsp_fgc.h>   // for pars_func[idx]()

extern void (*pars_func[])(void);

static struct pars
{
    // delay_flag: Flag used to delay the parsing of a property until the next ISR.
    // This is intended to be used whenever a property parsing function needs to perform a parameter switch
    // but there is already a switch pending on that same parameter set. The BGP task then needs to wait for
    // the next ISR (where the switch will occur)

    BOOLEAN delay_flag;

    // switch_mask: incremental mask of the "next" parameter sets (e.g. ISR_SWITCH_REF) that the parsing
    // functions are modifying. After successful parsing, this mask is used to request a parameter switch to
    // the ISR. The use of a unique mask ensures that all parameter sets will switch on the same ISR.

    INT32U  parameter_switch_mask;

} pars;

// This macro is intended to be used at the start of the parsing functions that modify one or several
// parameter sets (e.g. ISR_SWITCH_REF). It will test whether a parameter switch is already pending and
// if so, do early return.

#define PARS_EARLY_RETURN(switch_mask)                                      \
    if(Test(isr_switch_request_flags, switch_mask))                \
    {                                                              \
        pars.delay_flag = TRUE;                                    \
        return;                                                    \
    }

/*---------------------------------------------------------------------------------------------------------*/
void ParsGroupSet(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at the end of the millisecond ISR (on FGC2) if the par_group_set_mask is non-zero, which
  indicates that at least one parameter group has had a property set. It does not necessarily mean that
  the property has changed value. The function identifies the related property group from the bit set in
  the mask and calls the appropriate function. Only one function is ever executed per millisecond to reduce
  the risk of overrun. If a function is run, the associated bit is cleared in the change mask.

  On FGC3, this function is called in the BGP task.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U      idx;
    INT32U      mask;

    pars.delay_flag            = FALSE;
    pars.parameter_switch_mask = 0;

    // Search for LSB which is set
    for (idx = 0, mask = 1; !(rtcom.par_group_set_mask & mask); mask <<= 1, idx++)
    {
    };

    pars_func[idx]();                                                    // Run parameter group function

    if (pars.delay_flag == FALSE)
    {
        // Request the ISR to switch to the new parameter sets (the ones pointed by "next", e.g. ref.next)

        DISABLE_TICKS;
        Set(isr_switch_request_flags, pars.parameter_switch_mask);              // Atomic set
        ENABLE_TICKS;

        Clr(rtcom.par_group_set_mask, mask);                                    // Clear parsing request
    }
}
#if (FGC_CLASS_ID == 61)
/*---------------------------------------------------------------------------------------------------------*/
void ParsMeasSim(void)
/*---------------------------------------------------------------------------------------------------------*\
  Simulate measurement parameter has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U enabled;

    enabled = (property.meas.sim && rtcom.state_op == FGC_OP_SIMULATION);

    // With asynchronous DSP the parsing functions are executed in BGP task context and in particular
    // this function requires a critical section

    DISABLE_TICKS;

    if (enabled && !sim.active->enabled)                                // If sim now enabled
    {
        sim.active->load.current    = 0.0;                                // Reset simulated current
        sim.active->load.integrator = 0.0;                                // Reset simulated current
    }

    sim.active->enabled = enabled;

    ENABLE_TICKS;
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsMeasISim(void)
/*---------------------------------------------------------------------------------------------------------*\
  Force the simulation integrator to a user specified value (called on a S MEAS.I_SIM command)
\*---------------------------------------------------------------------------------------------------------*/
{
    // Atomic memory write: no need for a critical section

    sim.active->load.integrator = property.meas.i_sim;              // Force simulated current
}
#endif
/*---------------------------------------------------------------------------------------------------------*/
void ParsCalFactors(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a calibration property that impacts the calibration factors is modified
\*---------------------------------------------------------------------------------------------------------*/
{
    // On FGC2 the calibration update is done in the ISR whereas on FGC3 it is taken care of by the BGP task.

    bgp.trigger.calibration_refresh = TRUE;
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsCalDac(void)
/*---------------------------------------------------------------------------------------------------------*\
  DAC calibration parameters have changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U dac_idx;

    PARS_EARLY_RETURN(ISR_SWITCH_CAL);                  // Early return if parameter switch already pending

    // Trigger the parameter switch once the parsing has completed
    Set(pars.parameter_switch_mask, ISR_SWITCH_CAL);

    // Copy the whole cal_factors structure even though we only need to copy the ADC part from the currently
    // active calibration parameters (the DAC part will be overwritten by calDacInit)

    *cal.next = *cal.active;

    for (dac_idx = 0; dac_idx < FGC_N_DACS; dac_idx++)
    {
        calDacInit(&property.cal.dac[dac_idx][0], &cal.next->dac_factors[dac_idx], DAC_RESOLUTION, DAC_CAL_REF);
    }

    ParsLimits();                                       // Recalculate limits based on new DAC calibration
}
#if (FGC_CLASS_ID == 61)
/*---------------------------------------------------------------------------------------------------------*/
void ParsRegI(void)
/*---------------------------------------------------------------------------------------------------------*\
  Current regulation parameters have changed. Recalculate the RST coefficients for the current regulation.
\*---------------------------------------------------------------------------------------------------------*/
{
    // [FGC3] DO NOT DO A "Set(pars.parameter_switch_mask, ISR_SWITCH_REG)" IN THIS CASE!!
    //        Indeed at the end of this function, the next regulation parameters (pointed by reg.next) a not
    //        ready. The setting of those parameters and the request for a switch, if necessary/possible, is
    //        entirely handled by the RegStatePrepare() function.

    // Update the regulation properties

    regRstInit(&property.reg.i.op,
               property.reg.v.rst_pars.period,
               property.reg.i.period_div    [property.load.select],
               &load.last_parsed->pars,
               property.reg.i.clbw          [property.load.select],
               property.reg.i.clbw2         [property.load.select],
               property.reg.i.z             [property.load.select],
               property.reg.i.pure_delay    [property.load.select],
               &property.reg.i.manual_rst);

    // If currently in I regulation, need to update the regulation parameter as soon as possible

    if (reg.active->state == FGC_REG_I)
    {
        RegStateRequest(FALSE, FALSE, FGC_REG_I, ref.v.value);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsRegV(void)
/*---------------------------------------------------------------------------------------------------------*\
  Voltage regulation parameters have changed (REG.V.PERIOD_DIV or REG.V.TRACK_DELAY).
  Recalculate the voltage regulation error parameters.
\*---------------------------------------------------------------------------------------------------------*/
{
    property.limits.op.v.err_warning  = property.limits.v.err_warning;
    property.limits.op.v.err_fault    = property.limits.v.err_fault;

    regErrInitLimits(&reg_v_err,
                     property.limits.v.err_warning,
                     property.limits.v.err_fault);
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsRegTestReset(void)
/*---------------------------------------------------------------------------------------------------------*\
  REG.TEST.RESET has been set. Initialise all the REF.TEST properties to be copies of the operational
  properties.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Reset REG.TEST.USER and REF_USER

    property.reg.test.user            = 0;
    property.reg.test.ref_user        = 0;

    // Copy current RST coefficients + track delay

    property.reg.test.b.rst           = property.reg.b.rst;
    property.reg.test.i.rst           = property.reg.i.op.rst;         // Copy the REG.I.OP coefficients

    // Copy period requests and call parsPeriod() to compute property.reg.test.i.period
    // and property.reg.test.b.period

    property.reg.test.b.period_div = property.reg.b.period_div;
    property.reg.test.i.period_div = property.reg.i.period_div[property.load.select];
    ParsPeriods();
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLoad(void)
/*---------------------------------------------------------------------------------------------------------*\
  Load parameters have changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    PARS_EARLY_RETURN(ISR_SWITCH_LOAD);                 // Early return if parameter switch already pending

    // Trigger the parameter switch once the parsing has completed
    Set(pars.parameter_switch_mask, ISR_SWITCH_LOAD);

    // Copy the saturation part from the current (active) load

    *load.next = *load.active;

    // Load.select

    load.next->select = property.load.select;

    // Will initialise the load_param structure, except for the saturation part (load.next->pars.sat)

    regLoadInit(&load.next->pars,
                property.load.ohms_ser     [property.load.select],
                property.load.ohms_par     [property.load.select],
                property.load.ohms_mag     [property.load.select],
                property.load.henrys       [property.load.select],
                property.load.gauss_per_amp[property.load.select]);

    // load.last_parsed is used by the other parsing functions in this file to know the latest load settings,
    // in particular if the switch request has just been done.

    load.last_parsed = load.next;               // Pointers

    ParsLoadSim();
    ParsLimits();
    ParsRegI();
    ParsLoadSaturation();
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLoadSim(void)
/*---------------------------------------------------------------------------------------------------------*\
  Reinitialise VS + load simulation.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32        vref;
    FP32        vload;

    PARS_EARLY_RETURN(ISR_SWITCH_SIM);                  // Early return if parameter switch already pending

    // Trigger the parameter switch once the parsing has completed
    Set(pars.parameter_switch_mask, ISR_SWITCH_SIM);

    // Atomic read of the active simulated values

    DISABLE_TICKS;

    vref              = ref.v.value;
    sim.next->enabled = sim.active->enabled;
    sim.next->b       = sim.active->b;
    sim.next->i       = sim.active->i;
    sim.next->v       = sim.active->v;

    ENABLE_TICKS;

    // Reset initial B/I/V measurements in case they are not finite floats (either an infinity, or a NAN)

    if (!IsFiniteFloat(sim.next->b))
    {
        sim.next->b = 0.0;
    }

    if (!IsFiniteFloat(sim.next->i))
    {
        sim.next->i = 0.0;
    }

    if (!IsFiniteFloat(sim.next->v))
    {
        sim.next->v = 0.0;
    }

    // Reset VS simulation

    regSimVsInitGain(&property.vs.sim, &load.last_parsed->pars);

    // Reset Load simulation

    regSimLoadInit(&load.last_parsed->pars, &sim.next->load, property.reg.v.rst_pars.period);

    // According to the type of regulation, initialise the voltage or current in the load simulation
    // (sim.next->v/i/b have been copied from sim.active earlier in this function)

    if (reg.active->state == FGC_REG_V)
    {
        // vload = sim.next->v in this case

        vload = regSimLoadSetVoltage(&load.last_parsed->pars, &sim.next->load, sim.next->v);
    }
    else
    {
        // regSimLoadSetCurrent returns the voltage that corresponds to the V_LOAD

        vload = regSimLoadSetCurrent(&load.last_parsed->pars, &sim.next->load, sim.next->i);
    }

    regSimVsInitHistory(&property.vs.sim, &sim.next->vs, vload);


    // Reset the VS controller and measurements delays
    // (sim.next->v/i/b have been copied from sim.active earlier in this function)

    SimInitDelays(sim.next, vref, sim.next->v, sim.next->i, sim.next->b);
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLoadSaturation(void)
/*---------------------------------------------------------------------------------------------------------*\
  Load saturation parameters have changed.

  Note: implementation can be simplified if there is only one pars function ParsLoad doing the load + sat parts.
\*---------------------------------------------------------------------------------------------------------*/
{
    PARS_EARLY_RETURN(ISR_SWITCH_LOAD);                 // Early return if parameter switch already pending

    // Trigger the parameter switch once the parsing has completed
    Set(pars.parameter_switch_mask, ISR_SWITCH_LOAD);

    if (load.last_parsed != load.next)
    {
        // Copy the whole load_param structure in case this function is not called right after ParsLoad

        *load.next = *load.last_parsed;
    }

    regLoadInitSat(&load.next->pars,
                   property.load.henrys_sat [load.next->select],
                   property.load.i_sat_start[load.next->select],
                   property.load.i_sat_end  [load.next->select]);

    load.last_parsed = load.next;
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLoadSelect(void)
/*---------------------------------------------------------------------------------------------------------*\
  LOAD.SELECT has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Update load only if LOAD.SELECT has changed

    if (load.active->select != property.load.select)
    {
        PARS_EARLY_RETURN(ISR_SWITCH_LOAD);                 // Early return if parameter switch already pending

        // Trigger the parameter switch once the parsing has completed
        Set(pars.parameter_switch_mask, ISR_SWITCH_LOAD);

        ParsLoad();

        dpcls->dsp.load_select = property.load.select;
    }
}
#else

void ParsLoadSelect(void)
{

}
void ParsLoadSim(void)
{

}
void ParsMeasISim(void)
{

}

void ParsMeasSim(void)
{

}

#endif
/*---------------------------------------------------------------------------------------------------------*/
void ParsLimits(void)
/*---------------------------------------------------------------------------------------------------------*\
  Limit parameters have changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t invert_limits = UniSwitchLimitsInt();
    float    i_low         = property.limits.i.low;

    FP32 v_pos_dac, v_neg_dac;

    property.limits.op.b.err_warning  = property.limits.b.err_warning;
    property.limits.op.b.err_fault    = property.limits.b.err_fault;
    property.limits.op.i.pos          = property.limits.i.pos           [property.load.select];
    property.limits.op.i.min          = property.limits.i.min           [property.load.select];
    property.limits.op.i.neg          = property.limits.i.neg           [property.load.select];
    property.limits.op.i.rate         = property.limits.i.rate          [property.load.select];
    property.limits.op.i.err_warning  = property.limits.i.err_warning   [property.load.select];
    property.limits.op.i.err_fault    = property.limits.i.err_fault     [property.load.select];
    property.limits.op.i.acceleration = property.limits.i.acceleration  [property.load.select];
    property.limits.op.v.pos          = property.limits.v.pos           [property.load.select];
    property.limits.op.v.neg          = property.limits.v.neg           [property.load.select];

    regLimRefInit(&lim_i_ref, property.limits.op.i.pos, property.limits.op.i.neg,
                  property.limits.op.i.rate, invert_limits);

    regLimRmsInit(&lim_i_rms, property.limits.i.rms_warning, property.limits.i.rms_fault, property.limits.i.rms_tc, DSP_ITER_PERIOD);

    if (lim_i_ref.flags.unipolar)                       // If 1 or 2 quadrants
    {
        limits.i_closeloop = property.limits.i.closeloop[property.load.select];
    }
    else                                                // If 4 quadrants
    {
        limits.i_closeloop = 0.0;       // LIMITS.I.CLOSELOOP is not used with 4-quadrant VS
    }

    // If LIMITS.I.LOW is set to 0.0 force it to 1% of LIMITS.I.POS[0] (limit for nominal load)

    if(property.limits.i.low <= 0.0)
    {
        i_low = 0.01 * property.limits.i.pos[0];
    }

    regLimMeasInit(&lim_i_meas_a,
                   property.limits.op.i.pos,
                   property.limits.op.i.neg,
                   i_low,
                   property.limits.i.zero,
                   invert_limits);

    regLimMeasInit(&lim_i_meas_b,
                   property.limits.op.i.pos,
                   property.limits.op.i.neg,
                   i_low,
                   property.limits.i.zero,
                   invert_limits);

    regLimVrefInit(&lim_v_ref,
                   property.limits.op.v.pos, property.limits.op.v.neg, property.limits.op.v.rate,
                   property.limits.i.quadrants41, property.limits.v.quadrants41, invert_limits);

    lim_v_ref_fg = lim_v_ref;

    limits.i_start   = property.limits.op.i.min * FGC_I_START_FACTOR;

    limits.i_avl_max = (property.limits.op.i.pos < property.limits.i.hardware ?
                        property.limits.op.i.pos : property.limits.i.hardware);

    property.limits.op.i_available = limits.i_avl_max;

    dpcls->dsp.unipolar_f = lim_i_ref.flags.unipolar;   // Send unipolar current flag to MCU

    if (lim_v_ref.flags.unipolar)                       // If 1 quadrant
    {
        // Adjust default Tc by 15% to reduce trips
        property.ref.plep.exp_tc        = load.last_parsed->pars.tc * 1.15;
        property.ref.plep.exp_final     = limits.i_closeloop;

        dpcls->dsp.ref.exp_tc    = ToIEEE(load.last_parsed->pars.tc * 1.15); // Define default PLEP decay parameters
        dpcls->dsp.ref.exp_final = ToIEEE(limits.i_closeloop);
    }
    else                                                // else 2 or 4 quadrants
    {
        dpcls->dsp.ref.exp_tc    = 0;                   // Clear default PLEP decay parameters
        dpcls->dsp.ref.exp_final = 0;

        property.ref.plep.exp_tc    = 0.0;
        property.ref.plep.exp_final = 0.0;
    }

    // Limit Vref to range that DAC can produce

    v_pos_dac = cal.active->dac_factors[0].max_v_dac * property.vs.gain + property.vs.offset;
    v_neg_dac = cal.active->dac_factors[0].min_v_dac * property.vs.gain + property.vs.offset;

    if (property.limits.op.v.pos > v_pos_dac)
    {
        property.limits.op.v.pos = v_pos_dac;
    }

    if (property.limits.op.v.neg < v_neg_dac)
    {
        property.limits.op.v.neg = v_neg_dac;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsPeriods(void)
/*---------------------------------------------------------------------------------------------------------*\
  Periods parameters have changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;
    INT32U filter_20ms_length;
#if (FGC_CLASS_ID == 61)
    FP32   prev_v_reg_period = property.reg.v.rst_pars.period;
#endif

    property.reg.v.rst_pars.status       = REG_OK;

    // On FGC3, for now, ensure that REG.V.PERIOD_DIV == 1
    // A different value is not supported by the current DSP implementation and the assumption is made that
    // DSP_ITER_PERIOD (the DSP iteration, or interrupt, period) is equal to the V regulation period.

    if (property.reg.v.period_div != 1)
    {
        property.reg.v.period_div = 1;
        //DspPanic(DSP_PANIC_REG_V_PERIOD_DIV);
    }

    reg.iteration_period_ns              = property.reg.v.period_div *
                                           property.fgc.iter_period.int_dsp_div *
                                           FGC_MAIN_CLOCK_PERIOD_NS;            // Main clock period = 1 us on FGC3

    dpcls->dsp.log.capture.sampling_period_us = reg.iteration_period_ns /
                                                1000; // LOG.CAPTURE sampling period (us)


#if (FGC_CLASS_ID == 61)
    property.reg.v.rst_pars.period       = reg.iteration_period_ns * 1.0E-09;   // Iteration period in seconds
    property.reg.v.rst_pars.period_iters = 1;   // V regulation period is equal to the iteration period

    // NB: REG.V.PERIOD (the read-back property) points to &property.reg.v.rst_pars.period

    if (fabs(property.reg.v.rst_pars.period - prev_v_reg_period) > 5.0E-07)
    {
        // V regulation period has changed

        property.reg.v.rst_pars.freq = 1.0 / property.reg.v.rst_pars.period;

        ParsRegV();
        ParsLoadSim();
    }

#endif
    // Based on the iteration period of the DSP, calculate the length of the 20ms filter applied to the ADC channels.

    filter_20ms_length = (INT32U)(0.5 + 0.020 / DSP_ITER_PERIOD);

    if (filter_20ms_length > FILTER_20MS_MAX_LEN)
    {
        DspPanic(DSP_PANIC_FILTER_20MS_LEN);
    }

    if (adc.filter_20ms.nb_of_samples != filter_20ms_length)            // If the 20ms filter length has changed
    {
        adc.filter_20ms.nb_of_samples = filter_20ms_length;

        // Reset the circular buffers: in case the new number of samples is smaller than the previous one, this
        // prevents the FGC from keeping old vraw values indefinitely in the buffers.

        adc.filter_20ms.buffer_idx = 0;

        for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
        {
            memset((void *)&adc_chan[adc_idx].filter_20ms.vraw_buffer[0], 0, sizeof(adc_chan[0].filter_20ms.vraw_buffer));
        }
    }

#if (FGC_CLASS_ID == 61)

    INT32U idx;

    // I/B/testI/TestB regulation periods

    RegPeriod(&property.reg.b.period_div, &property.reg.b.period);

    for (idx = 0; idx < FGC_N_LOADS; idx++)
    {
        RegPeriod(&property.reg.i.period_div[idx], &property.reg.i.period[idx]);
    }

    RegPeriod(&property.reg.test.b.period_div, &property.reg.test.b.period);
    RegPeriod(&property.reg.test.i.period_div, &property.reg.test.i.period);

    ParsRegI();

#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsRunTime(void)
/*---------------------------------------------------------------------------------------------------------*\
  REF.RUN time property has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    ref.run = property.ref.run;                                 // Transfer from property

    // Subtract ref.advance_us

    if (ref.run.unix_time)              // If run time != 0
    {
        AbsTimeUsAddLongDelay(&ref.run, -1, 1000000 - ref.advance_us);          // I.e. ref.run -= ref.advance_us;
    }

    // Add 1s if ref.run is already in the past

    if (AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.run))                // If run time is already in the past
    {
        ref.run = rtcom.time_now_us;                                            // Set run time to time.now + 1s
        AbsTimeUsAddLongDelay(&ref.run, 1, 0);

        property.ref.run = ref.run;                 // Transfer the new value to the property
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsAbortTime(void)
/*---------------------------------------------------------------------------------------------------------*\
  REF.ABORT time property has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 61)
    ref.abort = property.ref.abort;     // Transfer from property

    // Subtract ref.advance_us

    if (ref.abort.unix_time)            // If abort time != 0
    {
        AbsTimeUsAddLongDelay(&ref.abort, -1, 1000000 - ref.advance_us);        // I.e. ref.abort -= ref.advance_us;
    }

    // Add 1s if ref.abort is already in the past

    if (AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.abort))      // If abort time is already in the past
    {
        ref.abort = rtcom.time_now_us;                              // Set abort time to time.now + 1s
        AbsTimeUsAddLongDelay(&ref.abort, 1, 0);

        property.ref.abort = ref.abort;                             // Transfer the new value to the property
    }

#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsTest(void)
/*---------------------------------------------------------------------------------------------------------*\
  Property TEST.DSP.INT32U or TEST.DSP.FLOAT has been set.  This function is available to support software
  debugging.
\*---------------------------------------------------------------------------------------------------------*/
{
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsVsSim(void)
/*---------------------------------------------------------------------------------------------------------*\
  VS model parameters have changed so reinitialise load simulation entirely.
\*---------------------------------------------------------------------------------------------------------*/
{
    ParsLoadSim();
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars.c
\*---------------------------------------------------------------------------------------------------------*/

