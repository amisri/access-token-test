
; originally applySystemPatch.asm

; Apply Antara ROM Code patches via pinit

; the list of pointer at pinit is read at boot time to do the initialisations

    .ref _Fix_dMax

    .sect .pinit

    .long _Fix_dMax     ; address of the routine Fix_dMax() in patch_dMax.c


    .end
