/*---------------------------------------------------------------------------------------------------------*\
  File:         meas.c

  Purpose:      FGC DSP Software - contains measurement processing functions

  Notes:        All functions there run at interrupt level under IsrPeriod().
\*---------------------------------------------------------------------------------------------------------*/

#define MEAS_GLOBALS

#include <meas.h>
#include <cc_types.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable

#if defined(__TMS320C32__)
#include <serprt30.h>       // for SERIAL_PORT_ADDR()
#endif
#include <dsp_dependent.h>      // for ToIEEE(), WAITSPY

#include <rtcom.h>              // for rtcom global variable
#include <reg.h>                // for reg, load global variables
#include <main.h>               // for dcct_select, state_pc
#include <adc.h>                // for adc, adc_chan global variables
#include <cyc.h>                // for cyc global variable
#include <cal.h>                // for cal global variable
#include <sim.h>                // for sim global variable
#include <macros.h>             // for Test()
#include <lib.h>                // for
#include <diag.h>               // for diag global variable
#include <definfo.h>            // for FGC_CLASS_ID
#include <defconst.h>           // for FGC_PC states
#include <macros.h>             // for Test() macro
#include <sim.h>                // for sim global variable, needed by defprops_dsp_fgc.h
#include <defprops_dsp_fgc.h>   // for PROP_ADC_FILTER_UNEXP_RESETS_HIST_CHAN
#include <math.h>               // for fabs()
#include <structs_bits_little.h>
#include <pc_state.h>



void regLimRmsInit(struct REG_lim_rms *lim_rms, float rms_warning, float rms_fault, float rms_tc, float iter_period)
{
    if(rms_tc > 0.0)
    {
        lim_rms->meas2_filter_factor     = iter_period / rms_tc;
        lim_rms->rms2_fault              = rms_fault * rms_fault;
        lim_rms->rms2_warning            = rms_warning * rms_warning;
        lim_rms->rms2_warning_hysteresis = lim_rms->rms2_warning * REG_LIM_RMS_HYSTERESIS * REG_LIM_RMS_HYSTERESIS;
    }
    else
    {
        lim_rms->meas2_filter_factor = 0.0;
    }

    lim_rms->flags.warning = FALSE;
    lim_rms->flags.fault   = FALSE;
}



void regLimMeasRmsRT(struct REG_lim_rms *lim_rms, float meas)
{
    if(lim_rms->meas2_filter_factor > 0.0)
    {
        // Use first order filter on measurement squared

        lim_rms->meas2_filter += (meas * meas - lim_rms->meas2_filter) * lim_rms->meas2_filter_factor;

        // Apply trip limit if defined

        if(lim_rms->rms2_fault > 0.0 && lim_rms->meas2_filter > lim_rms->rms2_fault)
        {
            lim_rms->flags.fault = TRUE;
        }
        else
        {
            lim_rms->flags.fault = FALSE;
        }

        // Apply warning limit if defined (with hysteresis)

        if(lim_rms->rms2_warning > 0.0)
        {
            if(lim_rms->meas2_filter > lim_rms->rms2_warning)
            {
                lim_rms->flags.warning = TRUE;
            }
            else if(lim_rms->meas2_filter < lim_rms->rms2_warning_hysteresis)
            {
                lim_rms->flags.warning = FALSE;
            }
        }
    }
}



/*---------------------------------------------------------------------------------------------------------*/
void MeasHistory(struct meas_hist * hist, FP32 meas)
/*---------------------------------------------------------------------------------------------------------*\
  This function calculates the measurement value and rate of change, based on four samples, using
  a least-squares regression (see Z:\projects\fgc\sw\fgc2\c32\fgc2_DspProg\doc\least_squares.xlsx).
  It is used to allow the regulation loop to be closed even if there is not enough of a measurement
  history to fill the RST history buffer.  This is the case for the field measurement with the POPS
  converter.
  To be called on the iteration period.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32   *  buf = hist->buf;
    INT32U    idx = hist->idx = ((hist->idx + 1) & MEAS_HIST_MASK);

    buf[idx] = meas;

    // Least-squares linear regression through four points

    hist->rate = property.reg.v.rst_pars.freq * 2.0 / 20.0 *
                 (3.0 * (buf[ idx                      ] - buf[(idx - 3) & MEAS_HIST_MASK]) +
                  (buf[(idx - 1) & MEAS_HIST_MASK] - buf[(idx - 2) & MEAS_HIST_MASK]));

    // Estimate value now (extrapolate 1.5 periods from middle of 4-point average)

    hist->meas = 0.25 * (buf[0] + buf[1] + buf[2] + buf[3]) + 1.5 * property.reg.v.rst_pars.period * hist->rate;

    // Estimate value after one regulation period (B/I/V)

    hist->estimated = hist->meas + reg.active->rst_pars.period * hist->rate;
}
/*---------------------------------------------------------------------------------------------------------*/
void MeasHistoryRegPeriod(struct meas_hist * hist, FP32 meas)
/*---------------------------------------------------------------------------------------------------------*\
  This function calculates the rate of change of the measurement, based on four samples, using
  a least-squares regression (see Z:\projects\fgc\sw\fgc2\c32\fgc2_DspProg\doc\least_squares.xlsx).
  To be called on the regulation period.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32   *  buf = hist->buf;
    INT32U    idx = hist->idx = ((hist->idx + 1) & MEAS_HIST_MASK);

    buf[idx] = meas;

    // Least-squares linear regression through four points

    hist->rate = reg.active->rst_pars.freq * 2.0 / 20.0 *
                 (3.0 * (buf[ idx                      ] - buf[(idx - 3) & MEAS_HIST_MASK]) +
                  (buf[(idx - 1) & MEAS_HIST_MASK] - buf[(idx - 2) & MEAS_HIST_MASK]));
}

/*---------------------------------------------------------------------------------------------------------*/
void MeasSelect(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function analyses the ADC signal statuses and determines which signal to use for feedback.
  It also controls the measurement fault flag and finally it decide whether or not to request an ADC reset.
  The function is not called during ADC16/DAC calibration.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U c;
    FP32   meas_value_a = 0;
    FP32   meas_value_b = 0;
    INT8U  dccts_used   = 0;

    // Return immediately if I_meas blocked at zero
    if (cal.imeas_zero_f)
    {
        return;
    }

    // Signal status

    for (c = 0; c < FGC_N_ADCS; c++)                                             // For each channel (A & B)
    {
        switch (adc_chan[c].input_type)
        {
            case ADC_INPUT_I_DCCT_A: // DCCT_A
            case ADC_INPUT_I_DCCT_B: // DCCT_B

                adc_chan[c].st_meas |= (adc_chan[c].st_check | adc_chan[c].last_st_check);

                if (rtcom.start_of_ms200_f)                                     // Every 200ms check SIGNAL jump
                {
                    adc_chan[c].last_st_check = adc_chan[c].st_check;

                    if ((dcct_select != FGC_DCCT_SELECT_AB)                     // If channels now agree
                        || adc.i_dcct_match_f
                       )
                    {
                        adc_chan[c].st_check = 0;                               // Clear signal check status
                    }
                }

                // Check that the channel measurement is all right (flag FGC_ADC_STATUS_V_MEAS_OK)
                // In the case the ADC filters are in reset, keep that flag set.
                // The current measurement will remain constant during the reset

                if (((adc_chan[c].st_meas & FGC_ADC_STATUS_V_MEAS_OK) != FGC_ADC_STATUS_V_MEAS_OK)
                    || (adc_chan[c].st_dcct & (FGC_DCCT_FAULT | FGC_DCCT_CAL_FLT))
                   )
                {
                    // Clear Imeas OK flag

                    Clr(adc_chan[c].st_meas, FGC_ADC_STATUS_V_MEAS_OK);

                    // Account for the bad measurement in the filtered measurements
                    adc_chan[c].ms20.err_downcounter       = adc.filter_20ms.nb_of_samples +
                                                             adc.filter_20ms.history_offset;  // Decremented every iteration
                    adc_chan[c].i_meas_ok_200ms            = FALSE;
                    adc_chan[c].i_meas_ok_1s               = FALSE;

                    // Block use of this channel for a given number of ms
                    adc_chan[c].i_meas_bad_counter = FGC_IMEAS_BAD_TIMER_MS;
                }
                else                                                    // else I_MEAS is now OK
                {
                    if (adc_chan[c].i_meas_bad_counter)                 // Down count to zero the bad counter
                    {
                        adc_chan[c].i_meas_bad_counter--;
                    }
                }

                if (adc_chan[c].i_meas_bad_counter)
                {
                    // If the present channel was marked as bad Imeas less than FGC_IMEAS_BAD_TIMER_MS ms
                    // in the past, do not use it to compute Imeas.

                    Clr(adc_chan[c].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
                }

                break;
        }
    }

    // Select validated current measurement

    if (dcct_select == FGC_DCCT_SELECT_AB ||
        dcct_select == FGC_DCCT_SELECT_A)
    {
        if ((adc_current_a != NULL) &&
            (adc_current_a->st_meas & FGC_ADC_STATUS_V_MEAS_OK))
        {
            Set(adc_current_a->st_meas, FGC_ADC_STATUS_IN_USE);

            meas_value_a = (reg.hi_speed_f ? adc_current_a->meas.current.i_dcct :
                            adc_current_a->ms20.filtered.current.i_dcct);

            dccts_used |= 1;
        }
    }

    if (dcct_select == FGC_DCCT_SELECT_AB ||
        dcct_select == FGC_DCCT_SELECT_B)
    {
        if ((adc_current_b != NULL) &&
            (adc_current_b->st_meas & FGC_ADC_STATUS_V_MEAS_OK))
        {
            Set(adc_current_b->st_meas, FGC_ADC_STATUS_IN_USE);

            meas_value_b = (reg.hi_speed_f ? adc_current_b->meas.current.i_dcct :
                            adc_current_b->ms20.filtered.current.i_dcct);

            dccts_used |= 2;
        }
    }

    meas.i = (meas_value_a + meas_value_b);

    // Average if both DCCTs are used

    if (dccts_used == 3)
    {
        meas.i /= 2;
    }

    if (dccts_used == 0 && !sim.active->enabled)
    {
        Set(rtcom.faults, FGC_FLT_I_MEAS);
    }

    // Check current on Ia and Ib against trip, low and zero limits
    // If one of the DCCTs is not used, its default measurement is zero.
    // Assume this value is always within the limits.

    // during calibration we can go outside limits without a fault

    if (cal.ongoing_f == FALSE && dpcls->mcu.cal.active == FGC_CTRL_DISABLED)
    {
        regLimMeas(&lim_i_meas_a, meas_value_a);
        regLimMeas(&lim_i_meas_b, meas_value_b);

        if (lim_i_meas_a.flags.trip || lim_i_meas_b.flags.trip)
        {
#if (FGC_CLASS_ID == 61)
            Set(rtcom.faults, FGC_FLT_LIMITS);
            RegStateV(TRUE, 0.0);
#endif
        }
    }

    // Calc available voltage range from converter

    regLimVrefCalc(&lim_v_ref, meas.i);

#if (FGC_CLASS_ID == 61)

    // Accumulate Imeas squared if cycling enabled

    if (dpcom->mcu.device_cyc == FGC_CTRL_ENABLED)
    {
        // Calculate I_RMS and compare it against the warning and fault limits

        regLimMeasRmsRT(&lim_i_rms, meas.i);

        if (lim_i_rms.flags.warning)
        {
            Set(rtcom.warnings, FGC_WRN_I_RMS_LIM);
        }
        else
        {
            Clr(rtcom.warnings, FGC_WRN_I_RMS_LIM);
        }

        if (lim_i_rms.flags.fault)
        {
            Set(rtcom.faults, FGC_FLT_I_RMS_LIM);
        }

        // Each 20ms update MEAS.I_RMS property

        if(!rtcom.ms20)
        {
            property.meas.i_rms = sqrt(lim_i_rms.meas2_filter);
        }
    }

#endif

    // Estimate field based on current

    meas.b = regLoadCurrentToField(&load.active->pars, meas.i);
}

//===========================================================================================
#if FGC_CLASS_ID == 61 || FGC_CLASS_ID == 62

#define SPY_MAX_WAIT_BUF 7

static INT32U spy_buf_idx = 0;
static FP32   spy_wait_buf[SPY_MAX_WAIT_BUF];
static INT32U spy_wait_buf_idx = 0;

/*---------------------------------------------------------------------------------------------------------*/
static void MeasSpyOut(FP32 * out)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends one value to the SPY interface. It implements a wait buffer to store data in case
  the USB is not ready.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U wait_idx = 0;

    if (spy_buf_idx == 0 && !(SPY_CTRL_P & SPY_CTRL_STATUS_BUF_RDY_MASK32))
    {
        if (spy_wait_buf_idx < SPY_MAX_WAIT_BUF)
        {
            spy_wait_buf[ spy_wait_buf_idx++ ] = *out;
        }
    }
    else if ((SPY_CTRL_P & SPY_CTRL_STATUS_BUF_RDY_MASK32))
    {
        while (wait_idx < spy_wait_buf_idx)
        {
            // Assumption: spy_buf_idx starts at 0, so no need for a boundary check in this while loop

            SPY_BUFFER_A[spy_buf_idx++] = spy_wait_buf[wait_idx++];
        }

        spy_wait_buf_idx = 0;

        SPY_BUFFER_A[spy_buf_idx] = *out;

        if (++spy_buf_idx >= (SPY_BUFFER_B / 4))
        {
            spy_buf_idx = 0;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MeasSpySend(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends values to the SPY interface.  There are six channels, all of which can be selected
  from a range of choices by the user.
  Values are only sent if someone is listening on the port, that is if the command field in the control
  register is non-zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U              idx;
    DP_IEEE_FP32        v;
    INT32U              spy_synch = SPY_SYNCH;

    if (!(SPY_CTRL_P & SPY_CTRL_STATUS_USB_RDY_MASK32) ||     // The USB interface is ready.
        ((SPY_CTRL_P & SPY_CTRL_CMD_MASK32) == 0))            // An external reset was triggered.
    {
        spy_buf_idx      = 0;
        spy_wait_buf_idx = 0;
        return;
    }

    MeasSpyOut((FP32 *)&spy_synch);  // Write synch word (0xFFFFFFFF)

    // Write six words to SPY (total: 7 words)

    for (idx = 0; idx < FGC_N_SPY_CHANS; idx++)                        // Write data words
    {
        v = ToIEEE(MeasSpyMpx(property.spy.mpx[idx]));          // Get data word for this channel
        MeasSpyOut((FP32 *)&v);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MeasDiag(void)
/*---------------------------------------------------------------------------------------------------------*\
  This will process the special diagnostic signals on ms 19 of 20:

    - VSource max current based on number of sub converters that are active
    - I_EARTH - value used directly at 50 Hz
    - U_LEADS - values accumulated at 50 Hz for 1Hz average (in Report1s)
    - SD-360 diagnostic ADC channel scalings
\*---------------------------------------------------------------------------------------------------------*/
{
    // ToDo function MeasDiag
}
#endif
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rt.c
\*---------------------------------------------------------------------------------------------------------*/

