/*!
 *  @file      bgp.c
 *  @defgroup  FGC3:DSP
 *  @brief     FGC DSP Software - Background Processing Functions
 *
 *  The DSP code does not run under an operating system. There is just the
 *  background processing level, and interrupt level. The background processing
 *  is used for operations that may take longer than about 400us. These
 *  background functions are used to process reference function arm/disarm
 *  requests from the MCU.
 */
#define RTCOM_GLOBALS           // define rtcom global variable
#define BGP_GLOBALS

#include <adc.h>
#include <bgp.h>
#include <cc_types.h>
#include <cyc.h>
#include <dpcls.h>
#include <dpcom.h>
#include <dsp_dependent.h>
#include <dsp_panic.h>
#include <fgc_errs.h>
#include <isr.h>
#include <lib.h>
#include <macros.h>
#include <main.h>
#include <mcu_dsp_common.h>
#include <props.h>
#include <report.h>
#include <rtcom.h>
#include <state_class.h>
#include <state.h>
#include <stdint.h>
#include <log_signals.h>
#include <unipolar_switch.h>

// Internal functions declaration

static void BgpProcess(void);
static void BgpLogOasisNewSegment(INT32U user);

// Internal function definitions

static void BgpProcess(void)
{
    // CYCLING Operation: prepare the next cycle

    // The BGP task will go through the following states in sequence to prepare for a new cycle.
    // There is an overrun check done in TimeCycling()

    switch (cyc.next_cycle_state)
    {
        case NEXT_CYCLE_WAIT_USER_EVENT:
            // Nothing to do
            // (Transition to PREPARE_REF or READY state will occur in function TimeCyclingEvents)

            break;

        case NEXT_CYCLE_PREPARE_REF:

            if (CycPrepareRefFunction() == TRUE)
            {
                cyc.next_cycle_state = NEXT_CYCLE_PREPARE_REG;
            }

            break;

        case NEXT_CYCLE_PREPARE_REG:

#if (FGC_CLASS_ID == 61)

            // Request for a REG state change at the beginning of the next cycle
            // NB: We can read ref.next->reg_mode because the 'next' parameter structure was set during previous
            //     state PREPARE_REF and the switch has not happened yet

            if (RegStateRequest(TRUE, TRUE, ref.next->reg_mode, ref.v.value))
            {
                Set(isr_switch_cycling_flags, ISR_SWITCH_REG);      // The regulation transition will occur on C0
                cyc.next_cycle_state = NEXT_CYCLE_PREPARE_REG_REQUEST;
            }

#elif (FGC_CLASS_ID == 62)
            cyc.next_cycle_state = NEXT_CYCLE_PREPARE_REG_REQUEST;
#endif
            break;

        case NEXT_CYCLE_PREPARE_REG_REQUEST:

#if (FGC_CLASS_ID == 61)

            // Check that the REF and REG parameters set are ready for switch, with the cycling flag on. This
            // indicates the next cycle is now ready to play.

            if (TestAll(isr_switch_request_flags, ISR_SWITCH_REF | ISR_SWITCH_REG) &&
                TestAll(isr_switch_cycling_flags, ISR_SWITCH_REF | ISR_SWITCH_REG))
            {
                cyc.next_cycle_state = NEXT_CYCLE_READY;
            }

#elif (FGC_CLASS_ID == 62)
            cyc.next_cycle_state = NEXT_CYCLE_READY;
#endif

            break;

        case NEXT_CYCLE_READY:
            // Nothing to do, the state will be reset to WAIT_USER_EVENT on C0, or in case we leave the CYCLING state

            break;
    }

    // Processing requests triggered by the ISR
#if (FGC_CLASS_ID == 61)

    if (reg.request.bgp_process_f)              // Regulation state change request (BGP process needed)
    {
        RegStatePrepare();                              // Prepare the regulation context switch based on the
        // data in reg.request structure
    }

#endif

    if (bgp.trigger.calibration_process)        // Calibration process
    {
        CalProcess();

        bgp.trigger.calibration_process     = FALSE;
    }

    if (bgp.trigger.one_hertz_processing)               // Background processing done every second
    {
        TemperatureCompensation();                      // Temperature compensation for internal ADCs and DCCTs
        CalCalc();                                      // Calculate updated calibration factors based on temperature

        bgp.trigger.one_hertz_processing    = FALSE;
    }

    if (bgp.trigger.process_200ms_filter_and_report)
    {
        BgpFilter200ms();       // 200ms filter
#if (FGC_CLASS_ID == 61)
        Report200ms();          // 200ms report to the MCU
#endif

        bgp.trigger.process_200ms_filter_and_report = FALSE;
    }

    if (bgp.trigger.process_1s_filter_and_report)
    {
        BgpFilter1s();          // 1s filter
#if (FGC_CLASS_ID == 61)
        Report1s();             // 1s report to the MCU
#endif
        bgp.trigger.process_1s_filter_and_report = FALSE;
    }

    if (bgp.trigger.log_oasis_latest_user >= 0)         // New LOG.OASIS segment
    {
        BgpLogOasisNewSegment((INT32U)bgp.trigger.log_oasis_latest_user);

        bgp.trigger.log_oasis_latest_user = -1;
    }

    // Other processing requests

    if (bgp.trigger.calibration_refresh)                // Calibration properties have changed
    {
        CalCalc();                                              // Calculate updated calibration factors

        bgp.trigger.calibration_refresh = FALSE;
    }

    // MCU requests

    if (dpcls->mcu.ana.req_f) // Check if there is a multiplexing change request for the Analogue card.
    {
        AnaProcessMpxRequest();
    }

    if (dpcls->mcu.cal.req_f)
    {
        if (CalNewRequest(TRUE,
                          (enum cal_action)dpcls->mcu.cal.action,
                          CalTranslateCpuIdx(dpcls->mcu.cal.idx),
                          dpcls->mcu.cal.ave_steps,
                          dpcls->mcu.cal.chan_mask))
        {
            dpcls->mcu.cal.req_f = FALSE;   // Acknowledge MCU: request is copied to the DSP

            //                  and the calibration is on-going
            if (cal.ongoing_f)
            {
                CalProcess();                   // Start calibration processing right away
            }
        }
    }

    if (state_pc != dpcls->mcu.state_pc)        // STATE.PC has changed
    {
        StatePc(dpcls->mcu.state_pc);
    }

    if (rtcom.state_op != dpcom->mcu.state_op)  // STATE.OP has changed
    {
        StateOp(dpcom->mcu.state_op);
    }

    if (dpcls->mcu.adc_mpx_f)                   // ADC MPX or SD filters have changed
    {
        AdcMpx();
    }

    // If BGP has no pending action

    if (bgp_no_pending_action)
    {
        DebugMemTransfer();             // For property FGC.DEBUG.MEM.DSP

        // TODO wait for next ISR?
    }
}

static void BgpLogOasisNewSegment(INT32U latest_user)
{
    /*
     * This function is called each time a new segment (usually, a user cycle)
     * is saved in the LOG.OASIS buffer.
     *
     * This function has two roles:
     *    1. Copy the local segment data in the shared memory, and let the MCU
     *       know about it (for publication).
     *    2. Look for old log segments (older than the total duration of the
     *       LOG.OASIS circular buffer) and wipe them out.
     * The writing in shared memory must use a protection mechanism so that the
     * MCU always read a coherent triplet timestamp + start_idx + size.
     *
     * The implementation of this function makes two (reasonable) assumptions:
     *    - That the DSP interrupt does not fiddle with another segment (in
     *      log_oasis.local structure) soon after it triggers the execution of
     *      this function. Since the period at which new log segments are saved
     *      should be much higher then the response time of the BGP task
     *      (typically, one basic period i.e. 1.2 s), it is assumed that it will
     *      never be the case.
     *    - That this function is called at least once during the duration of the
     *      log circular buffer. Else the algorithm for the clean-up of old segments
     *      will not work as expected (some old segments might stay).
     */

    INT32U               user;
    BOOLEAN              recent_segment_f;
    struct log_signal_index start_idx = log_oasis.local.start_idx[latest_user];

    // Copy the latest user start_idx/size info in shared memory

    sm_lock_write_take(&dpcls->dsp.log.oasis.lock);                 // Atomic write in shared memory

    dpcls->dsp.log.oasis.start_idx[latest_user] = start_idx.index;
    dpcls->dsp.log.oasis.size     [latest_user] = log_oasis.local.size     [latest_user];
    dpcls->dsp.log.oasis.timestamp[latest_user] = log_oasis.local.timestamp[latest_user];

    sm_lock_write_release(&dpcls->dsp.log.oasis.lock);              // End of atomic write

    // Update the value of property LOG.OASIS.(I_REF|IMEAS).INTERVAL_NS

    log_oasis.interval_ns.prop_value = log_oasis.interval_ns.cycle_value;

    // Update the cycle_tag property LOG.OASIS.(I_REF|I_MEAS).CYCLE_TAG

    log_oasis.cycle_tag[latest_user] = dpcom->mcu.evt.cycle_tag.int32u;

    // Notify MCU for publication of the LOG.OASIS properties

    dpcls->dsp.notify.log_oasis_user = latest_user;

    // Look for old segments (older than the total duration of the LOG.OASIS circular buffer) and wipe them out.

    for (user = 0; user <= FGC_MAX_USER; user++)
    {
        // If a segment is defined, and not the latest

        if (log_oasis.local.timestamp[user].unix_time != 0 && user != latest_user)
        {
            // Boolean indicating if the segment is a recent one (i.e. more recent than the duration of the log circular buffer)

            recent_segment_f = (log_oasis.local.start_idx[user].color == start_idx.color ?
                                log_oasis.local.start_idx[user].index <= start_idx.index :
                                log_oasis.local.start_idx[user].index >  start_idx.index
                               );

            // If it is not the case, delete the log segment

            if (!recent_segment_f)
            {
                // Reset the log segment in DSP local memory
                // With the assumption (see above) that the DSP interrupt is not modifying one of the segments soon after
                // notifying one to the background processing task.

                log_oasis.local.timestamp[user].unix_time = 0;
                log_oasis.local.timestamp[user].us_time   = 0;
                log_oasis.local.size     [user]           = 0;
                log_oasis.local.start_idx[user].color     = LOG_ODD;
                log_oasis.local.start_idx[user].index     = 0;

                // Reset its copy in shared memory


                sm_lock_write_take(&dpcls->dsp.log.oasis.lock);                 // Atomic write in shared memory

                dpcls->dsp.log.oasis.timestamp[user].unix_time = 0;
                dpcls->dsp.log.oasis.timestamp[user].us_time   = 0;
                dpcls->dsp.log.oasis.size     [user]           = 0;
                dpcls->dsp.log.oasis.start_idx[user]           = 0;

                sm_lock_write_release(&dpcls->dsp.log.oasis.lock);
            }
        }
    }
}

// External function definitions

void BgpLoop(void)
{
    for (;;)
    {
        dpcom->dsp.mcu_counter_dsp_alive.int32u = 0; // reset the counter, incremented at MCU MstTsk()

        // Check for change of reference from MCU

        if (dpcls->mcu.ref.arm_f)
        {
            BgpRefChange(dpcls->mcu.ref.user);
            continue;
        }

        // Check for property communications and parameter parsing

        bgp_no_pending_action = BgpPropComms();

        // Platform specific background processing

        BgpProcess();
    }
}

void BgpFilter200ms(void)
{
    INT32U              adc_idx;
    INT32U              adc_mask;
    INT32U              dcct_idx;

    // Reseting values of current
    dpcls->dsp.adc.amps_200ms[0] = ToIEEE(0.0);
    dpcls->dsp.adc.amps_200ms[1] = ToIEEE(0.0);

    // Reseting values of voltage
    dpcls->dsp.adc.vmeas_200ms[0] = ToIEEE(0.0);
    dpcls->dsp.adc.vmeas_200ms[1] = ToIEEE(0.0);

    for (adc_idx = 0, adc_mask = 1; adc_idx < FGC_N_ADCS; adc_idx++, adc_mask <<= 1)      // For each channel
    {
        AdcMeanFilterProcess(adc_idx, &adc_chan[adc_idx].filter_200ms_vars, &adc_chan[adc_idx].ms200);

        adc_chan[adc_idx].ms200.err_downcounter = !(adc_chan[adc_idx].i_meas_ok_200ms);

        dpcls->dsp.adc.raw_200ms  [adc_idx] =        adc_chan[adc_idx].ms200.filtered.current.v_raw;
        dpcls->dsp.adc.volts_200ms[adc_idx] = ToIEEE(adc_chan[adc_idx].ms200.filtered.current.v_adc);

        // Setting values of current and voltage
        switch (adc_chan[adc_idx].input_type)
        {
            case ADC_INPUT_I_DCCT_A: // DCCT_A
            case ADC_INPUT_I_DCCT_B: // DCCT_B

                dcct_idx = adc_chan[adc_idx].input_idx;
                dpcls->dsp.adc.amps_200ms[dcct_idx] = ToIEEE(adc_chan[adc_idx].ms200.filtered.current.i_dcct);
                break;

            case ADC_INPUT_V_MEAS_LOAD:
                dpcls->dsp.adc.vmeas_200ms[0] = ToIEEE(adc_chan[adc_idx].ms200.filtered.voltage.v_meas);
                break;

            case ADC_INPUT_DIRECT:
                dpcls->dsp.adc.vmeas_200ms[1] = ToIEEE(adc_chan[adc_idx].ms200.filtered.voltage.v_meas);
                break;
        }

        adc_chan[adc_idx].i_meas_ok_200ms  = TRUE;
    }
}

void BgpFilter1s(void)
{
    INT32U              adc_idx;
    INT32U              adc_mask;
    INT32U              dcct_idx;

    for (adc_idx = 0, adc_mask = 1; adc_idx < FGC_N_ADCS; adc_idx++, adc_mask <<= 1)// For each channel
    {
        AdcMeanFilterProcess(adc_idx, &adc_chan[adc_idx].filter_1s_vars, &adc_chan[adc_idx].s);

        adc_chan[adc_idx].s.err_downcounter = !(adc_chan[adc_idx].i_meas_ok_1s);

        dpcls->dsp.adc.raw_1s     [adc_idx] =        adc_chan[adc_idx].s.filtered.current.v_raw;
        dpcls->dsp.adc.volts_1s   [adc_idx] = ToIEEE(adc_chan[adc_idx].s.filtered.current.v_adc);
        dpcls->dsp.adc.pp_volts_1s[adc_idx] = ToIEEE(adc_chan[adc_idx].s.peak_to_peak.current.v_adc);

        switch (adc_chan[adc_idx].input_type)
        {
            case ADC_INPUT_I_DCCT_A: // DCCT_A
            case ADC_INPUT_I_DCCT_B: // DCCT_B

                dcct_idx = adc_chan[adc_idx].input_idx;
                dpcls->dsp.adc.amps_1s[dcct_idx] = ToIEEE(adc_chan[adc_idx].s.filtered.current.i_dcct);
                break;
        }

        // can be a current or a voltage
        dpcls->dsp.adc.cal_meas_1s[adc_idx] = ToIEEE(adc_chan[adc_idx].s.filtered.voltage.v_meas);

        adc_chan[adc_idx].i_meas_ok_1s = TRUE;
    }   // End for each channel


    // Calculate Idiff on 1s boundaries if cal is not active

    meas.i_diff_1s = 0.0;  // Default value

    if (adc_current_a != NULL && adc_current_b != NULL)
    {
        if (Test(adc_current_a->st_meas, FGC_ADC_STATUS_IN_USE) &&
            Test(adc_current_b->st_meas, FGC_ADC_STATUS_IN_USE) &&
            !cal.ongoing_f                                &&
            !adc_current_a->s.err_downcounter             &&
            !adc_current_b->s.err_downcounter)
        {
            meas.i_diff_1s = adc_current_b->s.filtered.current.i_dcct - adc_current_a->s.filtered.current.i_dcct;
        }
    }
}

INT32U BgpTranslateFgError(enum fg_error fg_error)
{
    switch (fg_error)
    {
        case FG_OK:                         return (FGC_OK_NO_RSP);

        case FG_BAD_ARRAY_LEN:              return (FGC_BAD_ARRAY_LEN);

        case FG_BAD_PARAMETER:              return (FGC_BAD_PARAMETER);

        case FG_INVALID_TIME:               return (FGC_INVALID_TIME);

        case FG_OUT_OF_ACCELERATION_LIMITS: return (FGC_OUT_OF_ACCELERATION_LIMITS);

        case FG_OUT_OF_LIMITS:              return (FGC_OUT_OF_LIMITS);

        case FG_OUT_OF_RATE_LIMITS:         return (FGC_OUT_OF_RATE_LIMITS);

        case FG_OUT_OF_VOLTAGE_LIMITS:      return (FGC_OUT_OF_VOLTAGE_LIMITS);
    }

    return (FGC_UNKNOWN_ERROR_CODE);
}

// EOF
