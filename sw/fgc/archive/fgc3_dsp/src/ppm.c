/*---------------------------------------------------------------------------------------------------------*\
  File:         ppm.c

  Purpose:      FGC DSP Software - contains PPM global variable

  Notes:        This structure being quite large on FGC3, it is defined in a separate file to
                allow for an easy re-mapping
\*---------------------------------------------------------------------------------------------------------*/

#define PPM_GLOBALS

#include <ref.h>        // Must be included before ppm.h to avoid a compiler warning.
#include <ppm.h>        // In fact already included by ref.h

/*---------------------------------------------------------------------------------------------------------*\
  End of file: ppm.c
\*---------------------------------------------------------------------------------------------------------*/

