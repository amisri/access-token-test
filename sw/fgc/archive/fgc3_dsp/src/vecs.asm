;------------------------------------------------------------------------------------------------------------
; File:         vecs.asm
;
; Contents:     This file defines the interrupt and reset vectors for the TMS320C6727 program
;------------------------------------------------------------------------------------------------------------

    .sect   ".vector"

    .ref     _c_int00           ; entry point
    .ref    _IsrPeriod          ; external interrupt NMI (ms tick) service routine
    .ref    _IsrTrap            ; external interrupt NMI (ms tick) service routine

    .global _vector_start

; Interrupt Service Table (IST)

; The IST is a table of fetch packets that contain code for servicing the interrupts.
; The IST consists of 16 consecutive fetch packets of eight instructions.
; Because each fetch packet contains eight 32-bit instruction words (or 32 bytes), each
; address in the table is incremented by 32 bytes (20h) from the one adjacent to it.

_vector_start:

;000h   RESET ISFP
    b        _c_int00               ; reset vector
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;020h   NMI ISFP
    b       _IsrPeriod              ; NMI interrupt is used for the DSP Periodic Interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;040h   Reserved
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;060h   Reserved
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;080h   INT4 ISFP
    b       _IsrTrap                ; DSP Periodic Interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;0A0h   INT5 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;0C0h   INT6 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;0E0h   INT7 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;100h   INT8 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;120h   INT9 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;140h   INT10 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;160h   INT11 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;180h   INT12 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;1A0h   INT13 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;1C0h   INT14 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop
;1E0h   INT15 ISFP
    b       _IsrTrap                ; Unexpected interrupt
    nop 5
    nop
    nop
    nop
    nop
    nop
    nop

    .end
;------------------------------------------------------------------------------------------------------------
; End of file: vecs.asm
;------------------------------------------------------------------------------------------------------------

