/*---------------------------------------------------------------------------------------------------------*\
  File:         main.c

  Purpose:      FGC DSP Software - contains main()

  Author:       Quentin.King@cern.ch

  Notes:        The C32 is normally under the control of the MCU.  The C32 program is copied into SRAM
                by the MCU main program.

  Debugging:    The debug union can be used to see DSP variables via the property DEBUG.MEM.DSP.  By
                default, this property points to the debug union (it can be set to any address), so
                up to 32 floats or ints can be visualised using code like this:

                    db.buf.f[0] = tx;                                   // ##### DEBUG #####
                    db.buf.i[1] = exp_f;                                // ##### DEBUG #####
                    db.buf.f[db.idx++ & (DB_DSP_MEM_LEN-1)] = fp_val;   // ##### DEBUG #####
                    db.buf.i[db.idx++ & (DB_DSP_MEM_LEN-1)] = val;      // ##### DEBUG #####

                    db.buf.i and db.idx are initialised to zero.
\*---------------------------------------------------------------------------------------------------------*/

#define DPRAM_CLASS_GLOBALS     // define dpcls global variable
#define DPCOM_GLOBALS           // define dpcom global variable
#define DEBUG_GLOBALS           // define db global variable
#define LIB_GLOBALS             // define
#define PC_STATE_GLOBALS
#define CRATE_GLOBALS           // define crate global variable
#define MAIN_GLOBALS
#define PROPS_GLOBALS
#define REF_GLOBALS
#define REFTO_GLOBALS
#define REG_GLOBALS

#include <main.h>               // for state_pc global variable
#include <version.h>            // for version structure from version_def.txt
#include <cc_types.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <reg.h>                // for reg, load_select global variables
#include <meas.h>               // for meas global variable
#include <stdlib.h>             // for NULL
#include <debug.h>              // for db global variable
#include <lib.h>                // for InitDsp()
#include <dsp_dependent.h>      // for
#include <string.h>             // for memset()
#include <adc.h>                // for adc, adc_chan global variables
#include <cyc.h>                // for cyc global variable
#include <cal.h>                // for cal global variable
#include <sim.h>                // for sim global variable
#include <defprops_dsp_fgc.h>   // for N_DSP_PROPS
#include <bgp.h>                // for BgpCheckIref()
#include <definfo.h>            // for FGC_CLASS_ID
#include <ppm.h>                // for ppm[]
#include <refto.h>
#include <mcu_dsp_common.h>
#include <isr.h>
#include <pc_state.h>
#include <spivs.h>
#include <crate.h>
#include <libcal.h>
#include <c6x.h>
#include <ana.h>
#include <log_signals.h>

extern far int vector_start;          // Vector table address as defined by the linker

/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*\
  This is the main function for the FGC DSP Program.  The DSP is controlled by the MCU, and will be started
  only after the MCU has prepared the Dual Port RAM.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U      adc_idx;
    INT32U   *  dpram32_p;
    INT32U      ii;

    // Initialise DSP resources (Strobes, timers, ports)
    InitDsp(); // it checks for Hw_init() in case of FGC3

    dpcom = (volatile struct dpcom *)  DPRAM_DPCOM_32;  // Pointer to common DP ram structure
    dpram32_p = (INT32U *)(DPRAM_DPCOM_32 + 16384 - 4);  // DPRAM_START_32

    adc_idx = dpcom->mcu.interfacetype; // temporary store this value that was already set

    // clear the Dual Port RAM, 16 K bytes = 4K_32bit_words
    // the last one written is the handshake acknowledge expected by the MCU
    for (ii = (0x1000 - 1); ii != 0 ; ii--)
    {
        *dpram32_p-- = 0;
    }

    dpcom->mcu.interfacetype = adc_idx; // recover temporary stored

    // Initialise global structures with either integer 0 or floating point 0.0.
    // Where required, individual fields are initialised in InitGlobals()

    // Initialise global structures to integer 0x00000000 (floating point 1.0)

    memset(&(ana),             0, sizeof(ana));
    memset(&(adc),             0, sizeof(adc));
    memset(&(adc_chan),        0, sizeof(adc_chan));
    memset(&(cyc),             0, sizeof(cyc));
    memset(&(plep),            0, sizeof(plep));
    memset(&(ppm),             0, sizeof(ppm));
    memset(&(property),        0, sizeof(property));
    memset(&(ref_fg),          0, sizeof(ref_fg));
    memset(&(reg),             0, sizeof(reg));
    memset(&(load),            0, sizeof(load));
    memset(&(reg_bi_err),      0, sizeof(reg_bi_err));
    memset(&(reg_v_err),       0, sizeof(reg_v_err));

    // done below with FP32ZERO
    //  memset( &(cal),             0, sizeof(cal) );

    memset(&(rt_ref_data),     0, sizeof(rt_ref_data));
    memset(&(sim),             0, sizeof(sim));
    memset(&(dsp_debug),       0, sizeof(dsp_debug));


    // Initialise global structures to floating point 0.0 (integer 0x80000000)

    memset(&(cal),                     FP32ZERO, sizeof(cal));      // Refined in InitGlobals()
    memset(&(cal_factors),             FP32ZERO, sizeof(cal_factors));
    memset(&(load_param),              FP32ZERO, sizeof(load_param));
    memset(&(lim_i_meas_a),            FP32ZERO, sizeof(lim_i_meas_a));
    memset(&(lim_i_meas_b),            FP32ZERO, sizeof(lim_i_meas_b));
    memset(&(lim_i_ref),               FP32ZERO, sizeof(lim_i_ref));
    memset(&(lim_i_rms),               FP32ZERO, sizeof(lim_i_rms));
    memset(&(lim_v_ref),               FP32ZERO, sizeof(lim_v_ref));
    memset(&(lim_v_ref_fg),            FP32ZERO, sizeof(lim_v_ref_fg));
    memset(&(limits),                  FP32ZERO, sizeof(limits));
    memset(&(meas),                    FP32ZERO, sizeof(meas));
    memset(&(ref),                     FP32ZERO, sizeof(ref));
    memset(&(reg_param),               FP32ZERO, sizeof(reg_param));
    memset(&(reg_vars),                FP32ZERO, sizeof(reg_vars));
    memset(&(property.reg.v.rst_pars), FP32ZERO, sizeof(property.reg.v.rst_pars));
    memset(&(property.reg.i.op),       FP32ZERO, sizeof(property.reg.i.op));
    memset(&(sim_param),               FP32ZERO, sizeof(sim_param));

    // InitLib will initialise all property variables according to their type (int/FP32)

    // here the Dual Port is clear
    InitLib(N_DSP_PROPS, FGC_MAX_USER, (INT32U)&ppm, sizeof(struct ppm));

    // Initialise the DSP ISR switching structures
    //
    // On FGC3, where the MCU and DSP are asynchronous, the DSP relies on duplicate configurations
    // in memory to improve the performance of the DSP periodic interrupt. The DSP will switch the
    // "active" configuration pointer with the "next" configuration pointer whenever necessary.
    //
    // On FGC3, that design is not implemented, yet the same syntax is shared with FGC3. Therefore
    // there will be an "active" and a "next" configuration pointers, even though they are equal
    // and pointing to a unique configuration in memory.

    reg.active  = &reg_param[0];
    reg.next    = &reg_param[1];

    load.active = &load_param[0];
    load.next   = &load_param[1];

    sim.active  = &sim_param[0];
    sim.next    = &sim_param[1];

    ref.active  = &ppm_cycling[0];
    ref.next    = &ppm_cycling[1];

    cal.active  = &cal_factors[0];
    cal.next    = &cal_factors[1];

    // Manually initialise the rest of the global variables

    InitGlobals();

    // Miscellaneous initialisations

    // Set a non-zero values for the ADC gains before the properties are set, to
    // avoid division by zero in the calibration initialisation

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
    {
        property.cal.adc.internal[adc_idx].gain = 10.0;    // Temporary, non-zero value
    }

    property.cal.adc.external[0].gain = 10.0;    // Temporary, non-zero value
    property.cal.adc.external[0].gain = 10.0;    // Temporary, non-zero value
    property.vs.gain = 1.0;                      // Temporary, non-zero value
    property.v_probe.capa.gain = 1.0;            // Temporary, non-zero value
    property.v_probe.load.gain = 1.0;            // Temporary, non-zero value
    property.dcct.primary_turns = 1;
    property.dcct.gain[0] = 1.0;
    property.dcct.gain[1] = 1.0;

    property.vs.i_limit.gain = 1;
    property.vs.i_limit.ref  = 0;

    CalInit();
    AdcInit();
    crateInit();
    IsrClassInit();
    spivsInit();

    TemperatureCompensationInit();

#if (FGC_CLASS_ID == 61)
    // Start with REG.STATE = V. Since we are not regulating yet, this call is mainly to ensure the proper
    // initialisation of the regulation structure (reg.active) with the default V regulation parameters.

    RegStateRequest(FALSE, TRUE, FGC_REG_V, 0.0);
#endif

    // Call to regErrInitFilterPeriod for I/B regulation.
    // Regarding the second parameter (filter_period_iters), it is always equal to 1
    // since the function regErrCalc is called only on the regulation period.
    // Therefore this init function needs only be called once.

    regErrInitFilterPeriod(&reg_bi_err, 1);

    CycExit();


    // Clear and enable interrupts

    ISTP = (unsigned int)&vector_start; // Relocate interrupt table
    ICR = 0xFFF0;                       // Clear all non-maskable interrupts
    IER = 0x0002;                       // Enable all interrupts, including NMI (to do once)

    ACKNOWLEDGE_NMI;
    ENABLE_INTS;
    ENABLE_TICKS;

    dpcom->dsp.started = DSP_STARTED;   // Inform MCU that DSP software is running

    // Start Background Processing

    BgpLoop();                          // Background processing loop for ever
}
/*---------------------------------------------------------------------------------------------------------*/
void InitGlobals(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises global variables
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U dac_idx;

    // Get DSP build number

    dpcom->dsp.version = version.unixtime;

    // Property.limits.op contains structures that are only partially mapped to properties so some fields
    // will not be initialised by InitLib.  Here we initialise all fields including the pointers to the
    // user callback function to check additional limits when arming a reference function.

    memset(&(property.limits.op), FP32ZERO, sizeof(property.limits.op));

    property.limits.op.b.user_check_limits = NULL;
    property.limits.op.i.user_check_limits = BgpGetCheckIref();
    property.limits.op.v.user_check_limits = NULL;

    // Set ADC/DCCT temperatures to default values

    property.adc.temperature     = CAL_T0;
    property.dcct.temperature[0] = CAL_T0;
    property.dcct.temperature[1] = CAL_T0;

    // Initialise global variables

    state_pc                  = 0;
    refto.flags.standby       = FALSE;
    refto.flags.stopping      = FALSE;
    refto.flags.aborting      = FALSE;
    dcct_select               = 0;

    // cal - to avoid division by zero in CalCalc()

    property.dcct.tau_temp    = 80.0;
    property.adc.tau_temp     = 80.0;

    // Arm capture log

    dpcls->dsp.log.capture.state                  = FGC_LOG_ARMED;
    dpcls->dsp.log.capture.user                   = -1;
    dpcls->dsp.log.capture.cyc_chk_time.unix_time = 0;
    dpcls->dsp.log.capture.cyc_chk_time.us_time   = 0;
    dpcls->dsp.log.capture.sampling_period_us     = 100;

    // cyc

    cyc.start_time_next.unix_time   = TIME_NEVER;

    // log.oasis

    sm_lock_write_reset(&dpcls->dsp.log.oasis.lock);

    log_oasis.subsampling.prop_value  = 1;
    log_oasis.subsampling.cycle_value = 1;

    // ref

    ref.now                   = &ref.i;
    ref.run.unix_time         = TIME_NEVER;
    ref.abort.unix_time       = TIME_NEVER;
    ref.time.s                = 0.0;
    ref.time.period_counter   = 0;
    ref.reg_bypass_mask       = DEFAULT_REF_REG_BYPASS;

    for (dac_idx = 0; dac_idx < FGC_N_DACS; dac_idx++)
    {
        ref.dac_raw[dac_idx]      = 0;
    }

    // Voltage source (VS)

    property.vs.dac_clamp_pct = 0.0;

    // property.reg.v.rst_pars (initial values for all classes.)
    // On FGC2, they do not change. On FGC3, they can be modified in ParsPeriod()

    property.reg.v.rst_pars.status       = REG_OK;
    property.reg.v.rst_pars.period_iters = 1;                   // 1 Vreg period  = 1 iteration period
    property.reg.v.rst_pars.period       = 0.001;               // 1 Vreg period  = 1 ms
    property.reg.v.rst_pars.freq         = 1000.0;              // Vreg frequency = 1 kHz

    reg.iteration_period_ns              = 1000000;             // DSP iteration period = 1ms

    // BGP/ISR triggers

    bgp.trigger.calibration_refresh             = FALSE;
    bgp.trigger.calibration_process             = FALSE;
    bgp.trigger.one_hertz_processing            = FALSE;
    bgp.trigger.process_200ms_filter_and_report = FALSE;
    bgp.trigger.process_1s_filter_and_report    = FALSE;
    bgp.trigger.log_oasis_latest_user           = -1;

    // ADC 20ms filter

    adc.filter_20ms.nb_of_samples = 20;

    property.fgc.iter_period.int_dsp_div = 100;         // DSP iteration period is 10 kHz
    // Default V regulation period = 1 kHz (On FGC2)

    property.reg.v.period_div = 1;

    // simulation

    sim.active->enabled = sim.next->enabled = 0;

    SimInitDelays(sim.active, 0.0, 0.0, 0.0, 0.0);
    SimInitDelays(sim.next,   0.0, 0.0, 0.0, 0.0);

    // Reg errors

    reg_v_err.delay.delay_int = 0;
    reg_v_err.filter_period_iters = 1;
    reg_v_err.counter_limit = 0;
    reg_v_err.warning_limit = 0.0;
    reg_v_err.fault_limit = 0.0;

    regErrInitVars(&reg_v_err);

    reg_bi_err.delay.delay_int = 0;
    reg_bi_err.filter_period_iters = 1;
    reg_bi_err.counter_limit = 0;
    reg_bi_err.warning_limit = 0.0;
    reg_bi_err.fault_limit = 0.0;

    regErrInitVars(&reg_bi_err);

    // REG change request

    reg.request.locked        = FALSE;
    reg.request.bgp_process_f = FALSE;

    // Measurement history

    meas.i_hist.idx     = 0;
    meas.i_hist_reg.idx = 0;
    meas.b_hist.idx     = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void TemperatureCompensationInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialise the temperature compensation for the ADCs and DCCTs
\*---------------------------------------------------------------------------------------------------------*/
{
    calTempFilterInit(&temperature_filter_adcs,    1.0, property.adc.tau_temp);
    calTempFilterInit(&temperature_filter_dcct[0], 1.0, property.dcct.tau_temp);
    calTempFilterInit(&temperature_filter_dcct[1], 1.0, property.dcct.tau_temp);
}
/*---------------------------------------------------------------------------------------------------------*/
void TemperatureCompensation(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function, to be called every second, updates the temperature of the ADCs and DCCTs thanks to the
  temperature compensation mechanism available as part of the calibration library.
\*---------------------------------------------------------------------------------------------------------*/
{
    property.adc.temperature     = calTempFilter(&temperature_filter_adcs,
                                                 CAL_TEMP * (float)dpcls->mcu.cal.t_adc);
    property.dcct.temperature[0] = calTempFilter(&temperature_filter_dcct[0],
                                                 CAL_TEMP * (float)dpcls->mcu.cal.t_dcct[0]);
    property.dcct.temperature[1] = calTempFilter(&temperature_filter_dcct[1],
                                                 CAL_TEMP * (float)dpcls->mcu.cal.t_dcct[1]);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/
