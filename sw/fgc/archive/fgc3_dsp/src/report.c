/*---------------------------------------------------------------------------------------------------------*\
  File:     report.c

  Purpose:  FGC DSP Software - contains functions that report information to the MCU

  Author:   Quentin.King@cern.ch

  Notes:    All real-time processing runs at interrupt level under IsrPeriod().
\*---------------------------------------------------------------------------------------------------------*/

#include <report.h>
#include <cc_types.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <dsp_dependent.h>      // for ToIEEE()
#include <rtcom.h>              // for rtcom global variable
#include <main.h>               // for state_pc, dcct_select global variables
#include <meas.h>               // for meas global variable
#include <reg.h>                // for reg, load global variables
#include <adc.h>                // for adc_chan global variables
#include <cyc.h>                // for cyc global variable
#include <macros.h>             // for Test()
#include <definfo.h>            // for FGC_CLASS_ID
#include <sim.h>                // for sim global variable
#include <math.h>               // for fabs()
#include <ppm.h>                // for ppm[]
#include <defconst.h>           // for FGC_N_ADCS
#include <unipolar_switch.h>

/*---------------------------------------------------------------------------------------------------------*/
void Report(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU
\*---------------------------------------------------------------------------------------------------------*/
{
    // Transfer every iteration

    dpcls->dsp.ref.func_type = ref.func_type;
    dpcls->dsp.meas.ia  = (adc_current_a != NULL ? ToIEEE(adc_current_a->meas.current.i_dcct) : 0.0);
    dpcls->dsp.meas.ib  = (adc_current_b != NULL ? ToIEEE(adc_current_b->meas.current.i_dcct) : 0.0);
    dpcls->dsp.ref.now  = ToIEEE(ref.now->value);
    dpcls->dsp.ref.i    = ToIEEE(ref.i_clipped.value);
    dpcls->dsp.ref.i_rst = ToIEEE(ref.i.value);
    dpcls->dsp.ref.v    = ToIEEE(ref.v.value);
    dpcls->dsp.meas.b   = ToIEEE(meas.b);
    dpcls->dsp.meas.i   = ToIEEE(meas.i);
    dpcls->dsp.meas.v   = ToIEEE(meas.v);
    dpcls->dsp.meas.v_capa = ToIEEE(meas.v_capa);
    dpcls->dsp.reg.err  = ToIEEE(reg_bi_err.err);

    // Transfer values to MCU on 20ms boundaries

    if (rtcom.start_of_ms20_f)
    {
        Report20ms();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void Report10ms(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU on 10ms boundaries
\*---------------------------------------------------------------------------------------------------------*/
{
    dpcls->dsp.ref.v       = ToIEEE(ref.v.value);           // Voltage reference
    dpcls->dsp.ref.i       = ToIEEE(ref.i_clipped.value);   // Current reference (clipped reference)
    dpcls->dsp.ref.i_rst   = ToIEEE(ref.i.value);           // Current reference (the one stored in RST history)
    dpcls->dsp.meas.i      = ToIEEE(meas.i);                // Current measurement
    dpcls->dsp.meas.v      = ToIEEE(meas.v);                // Voltage source output
    dpcls->dsp.meas.i_rate = ToIEEE(meas.i_hist.rate);      // Current rate measurement
}
/*---------------------------------------------------------------------------------------------------------*/
void Report20ms(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU on 20ms boundaries
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32S     i_err_ma;

    // MCU warnings

    rtcom.warnings |= dpcom->mcu.warnings;

    // Measurement warning
    if (adc_current_a != NULL && adc_current_b != NULL)
    {
        if ((rtcom.st_unlatched & FGC_UNL_I_MEAS_DIFF)
            || !(adc_current_a->st_meas & adc_current_b->st_meas  & FGC_ADC_STATUS_V_MEAS_OK)
            || ((adc_current_a->st_meas | adc_current_b->st_meas) & FGC_ADC_STATUS_CAL_FAILED)
           )
        {
            Set(rtcom.warnings, FGC_WRN_I_MEAS);
        }
        else
        {
            Clr(rtcom.warnings, FGC_WRN_I_MEAS);
        }
    }

    // VS gain warning
    if (!lim_i_ref.flags.unipolar
        || (meas.i > limits.i_closeloop)
       )
    {
        if (reg_v_err.flags.warning)
        {
            Set(rtcom.warnings, FGC_WRN_V_ERROR);
        }
        else
        {
            Clr(rtcom.warnings, FGC_WRN_V_ERROR);
        }
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_V_ERROR);
    }

    // REG_ERROR warning (suppressed if REG_ERROR fault present)
    if (reg.active->state != FGC_REG_V &&
        (reg.active->rst_pars.status != REG_OK ||
         (!reg.test_user_f && reg_bi_err.flags.warning && !reg_bi_err.flags.fault)))
    {
        Set(rtcom.warnings, FGC_WRN_REG_ERROR);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_REG_ERROR);
    }

    // REG_ERROR fault
    if (reg.active->state != FGC_REG_V
        && !ref.cycling_f
        && reg_bi_err.flags.fault)
    {
        Set(rtcom.faults, FGC_FLT_REG_ERROR);
    }

    // ELSE... DO NOT Clr(rtcom.faults,FGC_FLT_REG_ERROR);

    // Latch MCU warnings
    
    rtcom.warnings |= dpcom->mcu.warnings;
    dpcom->mcu.warnings = 0;

    // Send status data to MCU

    dpcom->dsp.faults     = rtcom.faults;
    dpcom->dsp.st_latched = rtcom.st_latched;
    dpcom->dsp.warnings   = rtcom.warnings | cyc.stretched_warnings;

    if (adc_current_a != NULL) {dpcls->dsp.meas.st_dcct[0] = adc_current_a->st_dcct;}

    if (adc_current_b != NULL) {dpcls->dsp.meas.st_dcct[1] = adc_current_b->st_dcct;}

    dpcls->dsp.meas.st_meas[0] = adc_chan[0].st_meas;
    dpcls->dsp.meas.st_meas[1] = adc_chan[1].st_meas;
    dpcls->dsp.meas.st_meas[2] = adc_chan[2].st_meas;
    dpcls->dsp.meas.st_meas[3] = adc_chan[3].st_meas;

    rtcom.faults     = 0;
    rtcom.st_latched = 0;

    // Channels A & B (Current measurement channels IA and IB)
    if (dpcls->mcu.meas.status_reset_f)                                 // If "S MEAS.STATUS RESET"
    {
        dpcls->mcu.meas.status_reset_f = FALSE;

        adc_chan[0].st_meas = 0;                                                // Clear all status bits
        adc_chan[1].st_meas = 0;                                                // Clear all status bits
        adc_chan[2].st_meas = 0;                                                // Clear all status bits
        adc_chan[3].st_meas = 0;                                                // Clear all status bits
    }

    adc_chan[0].st_meas &= FGC_ADC_STATUS_CAL_FAILED;  // Clear all but CAL_FAILED and DEAD_FILTER
    Set(adc_chan[0].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
    Set(adc_chan[0].st_meas, FGC_ADC_STATUS_V_MEAS_OK);

    adc_chan[1].st_meas &= FGC_ADC_STATUS_CAL_FAILED;  // Clear all but CAL_FAILED and DEAD_FILTER
    Set(adc_chan[1].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
    Set(adc_chan[1].st_meas, FGC_ADC_STATUS_V_MEAS_OK);

    adc_chan[2].st_meas &= FGC_ADC_STATUS_CAL_FAILED;  // Clear all but CAL_FAILED and DEAD_FILTER
    Set(adc_chan[2].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
    Set(adc_chan[2].st_meas, FGC_ADC_STATUS_V_MEAS_OK);

    adc_chan[3].st_meas &= FGC_ADC_STATUS_CAL_FAILED;  // Clear all but CAL_FAILED and DEAD_FILTER
    Set(adc_chan[3].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
    Set(adc_chan[3].st_meas, FGC_ADC_STATUS_V_MEAS_OK);

    // Send Izero, Ilow and Iaccess flags
    dpcls->dsp.meas.i_zero_f   = (lim_i_meas_a.flags.zero & lim_i_meas_b.flags.zero);
    dpcls->dsp.meas.i_low_f    = (lim_i_meas_a.flags.low  & lim_i_meas_b.flags.low);
    dpcls->dsp.meas.i_access_f = (fabs(meas.i) > property.limits.i.access);

    //  Send current regulation error to MCU in mA clipped to around +/-33 A (since the published value is
    //  a signed INT16)
    if (reg.active->state == FGC_REG_I)
    {
        i_err_ma = (INT32S)(1000.0 * reg_bi_err.err);

        // Clip the integer value to a signed 16 bit integer (type of the published I_ERR_MA).
        // NB: We do not use the defines SHRT_MIN/SHRT_MAX from limits.h here because
        //     the short integer is 32 bits on C32.

        i_err_ma = MAX(-32768,   i_err_ma);
        i_err_ma = MIN(i_err_ma, 32767);
    }
    else
    {
        i_err_ma = 0;
    }

    dpcls->dsp.reg.i_err_ma = i_err_ma;

    // TODO here, risk of a race condition with function ParsMeasISim (S MEAS.I_SIM value)

    property.meas.i_sim = sim.active->i;
}

/*---------------------------------------------------------------------------------------------------------*/
void Report200ms(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU on 200ms boundaries
\*---------------------------------------------------------------------------------------------------------*/
{
    BOOLEAN i_min_f;
    static BOOLEAN  prev_i_min_f;

    if (lim_i_ref.flags.unipolar)
    {
        FP32 meas_i_unipolar = meas.i;

        UniSwitchFloat(&meas_i_unipolar);

        i_min_f = meas_i_unipolar > (property.limits.op.i.min * (1.0 - REG_LIM_CLIP));
    }
    else
    {
        i_min_f = (meas.i > lim_i_ref.min_clip);
    }

    dpcls->dsp.meas.i_min_f  = (prev_i_min_f && i_min_f);   // Set i_min_f once I_MIN reached for 200ms
    prev_i_min_f = i_min_f;

    // Report value for properties REF.RUNNING and REF.REMAINING

    dpcls->dsp.ref.time_running   = ToIEEE(ref.time_running.s);
    dpcls->dsp.ref.time_remaining = ToIEEE(ref.time_remaining.s);
}

void Report1s(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU on 1 second boundaries
\*---------------------------------------------------------------------------------------------------------*/
{
    // Don't check Idiff when stopping as rapid changes in current can make Idiff incorrect

    if (state_pc != FGC_PC_STOPPING && state_pc != FGC_PC_FLT_STOPPING && state_pc != FGC_PC_FLT_OFF)
    {
        if (adc_current_a != NULL && adc_current_b != NULL)
        {
            // Idiff warning with 20% hysteresis

            if (Test(rtcom.st_unlatched, FGC_UNL_I_MEAS_DIFF))
            {
                if (dcct_select != FGC_DCCT_SELECT_AB ||
                    !(adc_current_a->st_meas & adc_current_b->st_meas & FGC_ADC_STATUS_V_MEAS_OK) ||
                    fabs(meas.i_diff_1s) < (0.8 * property.limits.i.meas_diff))
                {
                    Clr(rtcom.st_unlatched, FGC_UNL_I_MEAS_DIFF);           // Clear unlatched status
                }
            }
            else
            {
                if (dcct_select == FGC_DCCT_SELECT_AB &&
                    (adc_current_a->st_meas & adc_current_b->st_meas & FGC_ADC_STATUS_V_MEAS_OK) &&
                    fabs(meas.i_diff_1s) > property.limits.i.meas_diff)
                {
                    Set(rtcom.st_unlatched, FGC_UNL_I_MEAS_DIFF);           // Set unlatched status
                }
            }


            // Idiff fault at 10x warning level
            if (!sim.active->enabled
                && ((dcct_select == FGC_DCCT_SELECT_AB)
                    && (adc_current_a->st_meas & adc_current_b->st_meas & FGC_ADC_STATUS_V_MEAS_OK)
                    && (fabs(meas.i_diff_1s) > (10 * property.limits.i.meas_diff)))
               )
            {
                Set(rtcom.faults, FGC_FLT_I_MEAS);
            }
        }
    }

    // Clipped Idiff signal at 1 Hz. Clipping is to account for the fact the the published I_diff value
    // is a signed 16-bit integer.
    // NB: We do not use the defines SHRT_MIN/SHRT_MAX from limits.h here because
    //     the short integer is 32 bits on C32.

    dpcls->dsp.meas.i_diff_ma = (INT32S)(meas.i_diff_1s * 1000.0);
    dpcls->dsp.meas.i_diff_ma = MAX(-32768,   dpcls->dsp.meas.i_diff_ma);
    dpcls->dsp.meas.i_diff_ma = MIN(dpcls->dsp.meas.i_diff_ma, 32767);
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: report.c
\*---------------------------------------------------------------------------------------------------------*/

