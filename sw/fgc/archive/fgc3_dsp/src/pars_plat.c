/*---------------------------------------------------------------------------------------------------------*\
  File:         pars_plat.c

  Purpose:      FGC3 DSP Software - Parameter group set functions specific to the platform.

  Notes:        These functions are a complement to the functions in pars.c
\*---------------------------------------------------------------------------------------------------------*/

#include <adc.h>
#include <ana.h>
#include <cal.h>
#include <props.h>

/*---------------------------------------------------------------------------------------------------------*/
void ParsAdcSignals(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when ADC.INTERNAL.SIGNALS is modified
\*---------------------------------------------------------------------------------------------------------*/
{
    AdcSignalsToChannels();
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsAdcMpx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when ADC.FILTER_MPX is modified
\*---------------------------------------------------------------------------------------------------------*/
{
    AnaSetModMpx();
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsAnalogBus(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when ADC.ANALOG_BUS.SIGNAL is modified.

  Particular values:
    "NC" (=ANABUS_INPUT_NONE): floating analogue bus
    "COM" (=ANABUS_INPUT_COM): COM port on the front panel used as an input

  Note that if the user is trying to set the property equal to "COM" when the SWBUS register on the analogue
  card is zero, the read-back of the property will be "NC".
\*---------------------------------------------------------------------------------------------------------*/
{
    ANALOGUE_BUS_INPUT anabus_input = (ANALOGUE_BUS_INPUT)property.adc.analog_bus.signal;

    if (anabus_input < ANABUS_INPUT_MAX_VALUE)
    {
        // Use the current SWBUS value as the ADC mask. (Yes, it works.)

        AnaConfigAnalogueBus(anabus_input, property.adc.analog_bus.swbus);
    }
    else
    {
        // If not in the valid range, restore the property value based on the HW state

        AnaUpdateAdcProperties();
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars_plat.c
\*---------------------------------------------------------------------------------------------------------*/

