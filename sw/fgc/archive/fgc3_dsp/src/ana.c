/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp/fgc3/inc/ana.c

  Purpose:      FGC DSP Software - Analogue card driver for FGC3

  Notes:        This driver provides support for the ANA_101 card only.
\*---------------------------------------------------------------------------------------------------------*/

#define ANA_GLOBALS

#include <ana.h>
#include <cc_types.h>
#include <defconst.h>
#include <memmap_dsp.h>
#include <dsp_panic.h>
#include <dsp_dependent.h>
#include <adc.h>
#include <props.h>
#include <main.h>
#include <cal.h>

/*---------------------------------------------------------------------------------------------------------*/
void AnaInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the Analogue card to the nominal signal routing: all 4 inputs CH1 to CH4 are
  connected with ADC1 to ADC4 respectively. The analogue bus (or crossbar) is floating and the MPX is disabled
\*---------------------------------------------------------------------------------------------------------*/
{
    // Determine Analogue card Type

    ana.type = DSP_MID_ANATYPE_P;

    switch (DSP_MID_ANATYPE_P)
    {
        case FGC_INTERFACE_ANA_101:    // ANA_101
            break;

        case FGC_INTERFACE_ANA_103:    // ANA_103
            break;

        default:
            ana.type = FGC_INTERFACE_NO_CARD;
            DspPanic(DSP_PANIC_HW_ANATYPE);
            break;
    }

    // Configure the analogue card to its nominal state, meaning:
    //
    //   CH1 (IA)  ------->  ADC1 (IA)
    //   CH2 (IB)  ------->  ADC2 (IB)
    //   CH3 (VS)  ------->  ADC3 (VS)
    //   CH4 (AUX) ------->  ADC4 (AUX)

    AnaConfigAnalogueBus(ANABUS_INPUT_NONE, ANA_MASK_NO_ADC);

    // MODMPX initial state (this is mostly to set the cal.normalized_adc_factors
    // and adc_nominal_gain pointers to safe values, since the properties are not set yet)

    AnaSetModMpx();
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaConfigAnalogueBus(ANALOGUE_BUS_INPUT input, INT8U adcs_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function will connect zero or one input signal with any subset of the ADCs, via the analogue bus
  (crossbar). The function parameters are the following:

   - input:     the input to connect to the analogue bus. Note the special selector ANABUS_INPUT_NONE can be
                used to connect no input to the analogue bus (this is the nominal configuration).
   - adcs_mask: mask of the ADCs to connect to the analogue bus. Note that the value ANA_MASK_NO_ADC
                can be used to indicate no ADC should be connected to the analogue bus.

  Those two parameters alone are sufficient to express the full set of the multiplexing solutions offered by
  the analogue card. Examples are provided below in this comment section to help you choose the parameters
  for a variety of multiplexing options.

  Regarding the ADCs NOT listed in adcs_mask, they shall by default be connected to their respective channel
  inputs, not going through the analogue bus. Note that if the crossbar input is one of the 4 input channels,
  that channel is also connected to its respective ADC via a direct link (by design of the analogue card).

  EXAMPLE 0: perhaps the most simple usage of this function is to call it as follows:

      AnaConfigAnalogueBus(ANABUS_INPUT_NONE, ANA_MASK_NO_ADC);

    to configure the analogue card to its nominal state. Every ADC is connected to its default channel input.
    The analogue bus is not connected to any input, as meant by the first argument "ANABUS_INPUT_NONE"
    (technically the analogue bus is physically connected to the COM connector on the front panel, but the
    signal is floating.)


      CH1 (IA)  ------->  ADC1 (IA)
      CH2 (IB)  ------->  ADC2 (IB)
      CH3 (VS)  ------->  ADC3 (VS)
      CH4 (AUX) ------->  ADC4 (AUX)


  EXAMPLE 1: the following call to AnaConfigAnalogueBus

      AnaConfigAnalogueBus(ANABUS_INPUT_CH_B, (ANA_MASK_ADC2|ANA_MASK_ADC4));

    will set the following connections. This is meant to look at a channel with 2 ADCS.


      CH1 (IA)  ------->  ADC1 (IA)
      CH2 (IB)  ----+-->  ADC2 (IB)   (CH2 linked to ADC2 via direct link)
                    |
      CH3 (VS)  ------->  ADC3 (VS)
                    |
      CH4 (AUX) -x  \-->  ADC4 (AUX)  (CH4 disconnected, CH2 linked to ADC4 via crossbar)

                    x    MPX_ENA = 0  (the multiplexer is disconnected)
                    |
                   0V    MPX_SEL = 5  (connected to GNDSENSE (CALZERO) by default)

    NB: in the input parameters, ANA_MASK_ADC2 is not required in the mask but make the code
        more readable.
    NB: as mentioned above, after this function call, ADC1 and ADC3 (which are not listed in the 2nd
        argument of the function) are mapped to their default channel (IA and VS, respectively).


  EXAMPLE 2: the following call will typically be used during calibration

       AnaConfigAnalogueBus(ANABUS_INPUT_CAL_POSREF, ANA_MASK_ADC2);


      CH1 (IA)  ------->  ADC1 (IA)
      CH2 (IB)  -x  /-->  ADC2 (IB)    (CH2 disconnected, ADC2 connected to +10V)
                    |
      CH3 (VS)  ------->  ADC3 (VS)
      CH4 (AUX) ------->  ADC4 (AUX)
                    |
                    +    MPX_ENA = 1   (the multiplexer is connected to the crossbar)
                    |
                  +10V   MPX_SEL = 6   (CAL_POSREF)


  EXAMPLE 3: The following calls allow spying the channel 1 (IA) on the COM port. Notice that those
             two function calls are equivalent and will produce the same hardware multiplexing.

       AnaConfigAnalogueBus(ANABUS_INPUT_CH_A, ANA_MASK_ADC1);
                                  -OR-
       AnaConfigAnalogueBus(ANABUS_INPUT_CH_A, ANA_MASK_NO_ADC);


                   COM  (Front panel COM port used as an OUTPUT, to spy IA)
                    |
      CH1 (IA)  ----+-->  ADC1 (IA)
      CH2 (IB)  ------->  ADC2 (IB)
      CH3 (VS)  ------->  ADC3 (VS)
      CH4 (AUX) ------->  ADC4 (AUX)


  EXAMPLE 4: The following call will connect ADC1 to the COM port and disconnect channel 1. To
             the contrary of example 3 above, in this case the COM port is used as an INPUT.

       AnaConfigAnalogueBus(ANABUS_INPUT_COM, ANA_MASK_ADC1);


                   COM  (Front panel COM port used as an INPUT, measured on ADC1)
                    |
      CH1 (IA)  -x  \-->  ADC1 (IA)
      CH2 (IB)  ------->  ADC2 (IB)
      CH3 (VS)  ------->  ADC3 (VS)
      CH4 (AUX) ------->  ADC4 (AUX)

\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U swin, swbus, input_mask;

    if (input == ANABUS_INPUT_NONE)                                     // Analogue bus is not used
    {
        // Ignore adcs_mask

        AnaConfigSetMpx(0x0F, 0x00, FALSE, ANA_MPX_SEL_DEFAULT);        // SWIN, SWBUS, MPX_ENA, MPX_SEL
    }
    else if (input == ANABUS_INPUT_COM)                                 // COM port used as an input
    {
        swbus      =  adcs_mask                     & (ANA_FGC3_CTRL_SWBUS_MASK8);
        swin       = ~adcs_mask                     & (ANA_FGC3_CTRL_SWIN_MASK8);

        if (!AnaConfigSetMpx(swin, swbus, FALSE, ANA_MPX_SEL_DEFAULT))
        {
            DspPanic(DSP_PANIC_HW_ANALOG_CARD);
        }
    }
    else if (input < ANABUS_INPUT_MPX_RANGE_START)                      // Input is CH1, CH2, CH3 or CH4
    {
        //if( input >= ANABUS_INPUT_CH_D)          // Map alternative auxiliary inputs to the physical AUX channel
        //{
        //    input = ANABUS_INPUT_CH_D;
        //}

        input_mask = (1 << (input - ANABUS_INPUT_CH_A)) & ANA_FGC3_CTRL_SWIN_MASK8;
        swbus      = (adcs_mask | input_mask)      & ANA_FGC3_CTRL_SWBUS_MASK8;
        swin       = (~adcs_mask | input_mask)      & ANA_FGC3_CTRL_SWIN_MASK8;

        if (!AnaConfigSetMpx(swin, swbus, FALSE, ANA_MPX_SEL_DEFAULT))
        {
            DspPanic(DSP_PANIC_HW_ANALOG_CARD);
        }
    }
    else if (input < ANABUS_INPUT_MAX_VALUE)                            // One of the multiplexer inputs
    {
        swbus      =  adcs_mask                     & ANA_FGC3_CTRL_SWBUS_MASK8;
        swin       = ~adcs_mask                     & ANA_FGC3_CTRL_SWIN_MASK8;

        if (!AnaConfigSetMpx(swin, swbus, TRUE, (input - ANABUS_INPUT_MPX_RANGE_START)))
        {
            DspPanic(DSP_PANIC_HW_ANALOG_CARD);
        }
    }
    else                                                                // Invalid input value
    {
        DspPanic(DSP_PANIC_BAD_PARAMETER);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN AnaConfigSetMpx(INT8U swin, INT8U swbus, BOOLEAN mpx_enable, INT8U mpx)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to set the routing of the input analogue signals (input channels, multiplexer)
  to the 4 ADCs. The parameters of this function describe the requested configuration, and they are directly
  mapped to the low-level registers of the analogue card:

   - swin:  mask indicating which of the four input channels are connected (1) or disconnected (0)
   - swbus: mask indicating which of the four ADCs are connected (1) or disconnected (0) to the analogue bus
   - mpx_enable: flag indicating whether the multiplexer is connected to the analogue bus or not
   - mpx:   multiplexer selector

  This function has the essential role of ensuring no two inputs are connected to the analogue bus (or crossbar)
  at the same time. RULE: At any given time, the analogue bus takes zero or one input signal and direct it to
  a subset of ADCs.

  If the parameters passed to this function break the above rule, then the HW configuration is not modified
  and the function return FALSE to indicate the configuration could not be set on HW.

  On the contrary, if the parameters correspond to a valid configuration the function will drive the HW to
  set that configuration. That requires several steps of changing the HW registers one by one in a specific
  order, to ensure that the transition states of the analogue bus all comply with the above rule. The function
  finally returns TRUE when the job is done.

  Finally, this function will not disturb channels that are directly connected to their respective ADCs (not
  going through the analogue bus), providing they are so in the current configuration and in the requested one.
  This is to allow for example to change the configuration of the fourth ADC (AUX channel) without disturbing
  the operation on IA and IB channels.

  Outside of this function there should be NO writing to registers ANA_SWIN, ANA_SWBUS and ANA_MPX!

\*---------------------------------------------------------------------------------------------------------*/
{

    if (mpx_enable == FALSE)                    // Multiplexer to be disconnected
    {
        // The rule is then to check that no more than one input channel (CH1 to CH4) is connected to the
        // analogue bus. To be connected to the analogue bus, an input channel must have its SWIN bit and
        // its SWBUS bit set. That leads to the test condition (SWIN & SWBUS) equals to 0 or a power of two.

        if ((swin & swbus) != 0 &&
            (swin & swbus) != 1 &&
            (swin & swbus) != 2 &&
            (swin & swbus) != 4 &&
            (swin & swbus) != 8)
        {
            return FALSE;
        }
        else
        {

            // Order of the following register writes is important

            DISABLE_TICKS;
            ANA_FGC3_CTRL_MPX_P   = (mpx & ~ANA_FGC3_CTRL_MPX_ENA_MASK8);       // Multiplexer is now disabled
            ANA_FGC3_CTRL_SWBUS_P = 0;
            ANA_FGC3_CTRL_SWIN_P  = swin;
            ANA_FGC3_CTRL_SWBUS_P = swbus;
            ENABLE_TICKS;

            AnaUpdateAdcProperties();

            return TRUE;
        }
    }
    else                                        // Multiplexer to be connected
    {
        // The rule is that none of the input channels (CH1 to CH4) must be connected at the same time as
        // the multiplexer to the analogue bus. Hence, (SWIN & SWBUS) must be zero.

        if ((swin & swbus) != 0)
        {
            return FALSE;
        }
        else
        {
            // Order of the following register writes is important

            DISABLE_TICKS;
            ANA_FGC3_CTRL_SWBUS_P = 0;
            ANA_FGC3_CTRL_SWIN_P  = swin;

            // Multiplexer is now enabled
            ANA_FGC3_CTRL_MPX_P   = ANA_FGC3_CTRL_MPX_ENA_MASK8 | (mpx & ANA_FGC3_CTRL_MPX_SEL_MASK8);
            ANA_FGC3_CTRL_SWBUS_P = swbus;
            ENABLE_TICKS;

            AnaUpdateAdcProperties();

            return TRUE;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaProcessMpxRequest(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function handles a SET of the property ADC.INTERNAL.MPX
\*---------------------------------------------------------------------------------------------------------*/
{
    ANALOGUE_BUS_INPUT bus_input;
    INT8U              adc_mask;

    if (ana.saved.in_use_f == TRUE)
    {
        return;         // If inside a AnaSaveMpx/AnaRestoreMpx section (e.g. while calibrating),
        // return immediately in order to delay the request
    }

    // Copy request

    bus_input = (ANALOGUE_BUS_INPUT)dpcls->mcu.ana.anabus_input;
    adc_mask  = (INT8U)dpcls->mcu.ana.adc_mask;

    // Process request

    AnaConfigAnalogueBus(bus_input, adc_mask);

    // Acknowledge MCU request

    dpcls->mcu.ana.req_f = FALSE;
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaUpdateAdcProperties(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function updates the following properties:
    ADC.INTERNAL.MPX       (via function AdcUpdateInputSignal)
    ADC.ANALOG_BUS.SIGNAL
    ADC.ANALOG_BUS.SWIN
    ADC.ANALOG_BUS.SWBUS
  according to the current state of the hardware.
\*---------------------------------------------------------------------------------------------------------*/
{
    ANALOGUE_BUS_INPUT bus_input, adc_mpx;
    INT32U ch_idx, adc_idx;
    INT8U  swin, swbus, mpx, modmpx[4], mask;
    INT32U adc_source;

    // Read HW registers once

    swin  = ANA_FGC3_CTRL_SWIN_P;
    swbus = ANA_FGC3_CTRL_SWBUS_P;
    mpx   = ANA_FGC3_CTRL_MPX_P;


    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
    {
        if (ana.type == FGC_INTERFACE_ANA_101)
        {
            // set modmpx (adc.filter.mpx) according to adc_source in control register
            // and test pattern (if test pattern selected)

            adc_source = (ANA_FGC3_ADC_CTRL_A[adc_idx] & ANA_FGC3_ADC_CTRL_A_SOURCE_MASK32) >>
                         ANA_FGC3_ADC_CTRL_A_SOURCE_SHIFT;

            if (adc_source == 0x1)
            {
                modmpx[adc_idx] = FGC_ADC_FILTER_MPX_INTERNAL_ADC;
            }
            else if (adc_source == 0x4)
            {
                modmpx[adc_idx] = FGC_ADC_FILTER_MPX_EXTERNAL_ADC;
            }
            else if (adc_source == 0x0)
            {
                switch (ANA_FGC3_TEST_PATTERN_P)
                {
                    case 0x00:
                        modmpx[adc_idx] = FGC_ADC_FILTER_MPX_0_PERCENT;
                        break;

                    case 0x01:
                        modmpx[adc_idx] = FGC_ADC_FILTER_MPX_12_PERCENT;
                        break;

                    case 0x55:
                        modmpx[adc_idx] = FGC_ADC_FILTER_MPX_50_PERCENT_F;
                        break;

                    case 0x0F:
                        modmpx[adc_idx] = FGC_ADC_FILTER_MPX_50_PERCENT_S;
                        break;

                    case 0xFE:
                        modmpx[adc_idx] = FGC_ADC_FILTER_MPX_87_PERCENT;
                        break;

                    case 0xFF:
                        modmpx[adc_idx] = FGC_ADC_FILTER_MPX_100_PERCENT;
                        break;
                }
            }
        }
        else
        {
            // ANA102, 103, etc: modmpx (adc.filter.mpx) can be internal ADC only

            modmpx[adc_idx] = FGC_ADC_FILTER_MPX_INTERNAL_ADC;
        }
    }

    // Update property values

    property.adc.analog_bus.swin  = swin;
    property.adc.analog_bus.swbus = swbus;

    // Identify the analogue bus input

    bus_input = ANABUS_INPUT_NONE;      // Default value (Analogue bus is not connected)

    if (mpx & ANA_FGC3_CTRL_MPX_ENA_MASK8)        // If multiplexer enabled
    {
        bus_input = (ANALOGUE_BUS_INPUT)((mpx & ANA_FGC3_CTRL_MPX_SEL_MASK8) + ANABUS_INPUT_MPX_RANGE_START);
    }
    else                                // Else if disabled
    {
        for (ch_idx = 0, mask = 1; ch_idx < FGC_N_ADCS; ch_idx++, mask <<= 1)   // Loop on all channels
        {
            if (swin & swbus & mask)
            {
                switch (ch_idx)
                {
                    case 0:
                        bus_input = ANABUS_INPUT_CH_A;  // DCCT_A
                        break;

                    case 1:
                        bus_input = ANABUS_INPUT_CH_B;  // DCCT_B
                        break;

                    case 2:
                        bus_input = ANABUS_INPUT_CH_C;  // Vmeas
                        break;

                    case 3:
                        bus_input = ANABUS_INPUT_CH_D; // Direct input
                        break;
                }

                break;
            }
        }
    }

    if (bus_input == ANABUS_INPUT_NONE && swbus)        // If still the default value but SWBUS != 0
    {
        bus_input = ANABUS_INPUT_COM;                   // Then it means the analogue bus is used as
        // an input (the COM port on the front panel)
    }

    // Update property ADC.ANALOG_BUS.SIGNAL

    property.adc.analog_bus.signal = (INT32S)bus_input;

    // For each ADC identify its input signal

    for (adc_idx = 0, mask = 1; adc_idx < FGC_N_ADCS; adc_idx++, mask <<= 1)
    {
        adc_mpx = ANABUS_INPUT_NONE;                                        // NC (not connected)

        if (modmpx[adc_idx] == FGC_ADC_FILTER_MPX_INTERNAL_ADC)
        {
            if (swin & mask)
            {
                switch (adc_idx)
                {
                    case 0:
                        adc_mpx = ANABUS_INPUT_CH_A;  // DCCT_A
                        break;

                    case 1:
                        adc_mpx = ANABUS_INPUT_CH_B;  // DCCT_B
                        break;

                    case 2:
                        adc_mpx = ANABUS_INPUT_CH_C;  // Vmeas
                        break;

                    case 3:
                        adc_mpx = ANABUS_INPUT_CH_D; // Direct input
                        break;
                }
            }
            else if (swbus & mask)
            {
                adc_mpx = bus_input;                        // COM, IA, IB, VS, AUX, CH1IN_GND, CH2IN_GND,
                // DAC1, DAC2, CALZERO, CALPOS or CALNEG
            }
        }

        AdcUpdateInputSignal(adc_idx, adc_mpx, modmpx[adc_idx]);        // Will update ADC.INTERNAL.MPX
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaSetModMpx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function sets the filter multiplexer for all channels according to the property ADC.FILTER.MPX
  for ANA101 only.
  This function also selects the relevant properties for the calibration of the ADC output:
      - Internal ADC: CAL.A/B/C/D.ADC.INTERNAL.GAIN/ERR/TC/DTC
      - External ADC: CAL.A/B.ADC.EXTERNAL.GAIN/ERR
      - Test pattern: No calibration. Ideal ADC gain.

  For more information regarding the design of ADC multiplexing on FGC3 please refer to the following
  doc: "Z:\projects\fgc\doc\development\fgc3\ADC Multiplexing with ANA101 Card.pdf" (in the FGC superproject)
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;

    if (ana.type != FGC_INTERFACE_ANA_101)      // No support for analogue cards other than ANA-101
    {
        // For other analogue boards, only internal ADCs can be selected

        for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
        {
            cal.is_internal_adc[adc_idx]        = TRUE;
            cal.adc_nominal_gain[adc_idx]       = &property.cal.adc.internal[adc_idx].gain;
            cal.normalized_adc_factors[adc_idx] = (struct cal_event *)dpcls->mcu.cal.adc.internal[adc_idx].err;

            property.adc.filter_mpx[adc_idx] = FGC_ADC_FILTER_MPX_INTERNAL_ADC;
        }

        return;
    }

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)  // Loop on all ADCs
    {

        switch (property.adc.filter_mpx[adc_idx])
        {
            case FGC_ADC_FILTER_MPX_INTERNAL_ADC:

                cal.is_internal_adc[adc_idx]        = TRUE;
                cal.adc_nominal_gain[adc_idx]       = &property.cal.adc.internal[adc_idx].gain;
                cal.normalized_adc_factors[adc_idx] = (struct cal_event *)dpcls->mcu.cal.adc.internal[adc_idx].err;

                ANA_FGC3_ADC_CTRL_A[adc_idx] = (ANA_FGC3_ADC_CTRL_A[adc_idx] & ~ANA_FGC3_ADC_CTRL_A_SOURCE_MASK32)
                                               | (0x1 <<  ANA_FGC3_ADC_CTRL_A_SOURCE_SHIFT);

                break;

            case FGC_ADC_FILTER_MPX_EXTERNAL_ADC:

                if (adc_idx >= 2)               // Only adc_idx = 0 and 1 can be used with an external ADC.
                {
                    DspPanic(DSP_PANIC_INDEX_OUT_OF_RANGE);
                }
                else
                {
                    cal.is_internal_adc[adc_idx]        = FALSE;
                    cal.adc_nominal_gain[adc_idx]       = &property.cal.adc.external[adc_idx].gain;
                    cal.normalized_adc_factors[adc_idx] = (struct cal_event *)dpcls->mcu.cal.adc.external[adc_idx].err;
                }

                ANA_FGC3_ADC_CTRL_A[adc_idx] = (ANA_FGC3_ADC_CTRL_A[adc_idx] & ~ANA_FGC3_ADC_CTRL_A_SOURCE_MASK32)
                                               | (0x4 <<  ANA_FGC3_ADC_CTRL_A_SOURCE_SHIFT); // DIG_SUP_B[0] todo
                break;

            default:            // Any of the test patterns

                cal.is_internal_adc[adc_idx]        = FALSE;
                cal.normalized_adc_factors[adc_idx] = &CAL_PERFECT_ADC;
                cal.adc_nominal_gain[adc_idx]       = &CAL_PERFECT_ADC_NOMINAL_GAIN;

                ANA_FGC3_ADC_CTRL_A[adc_idx] = (ANA_FGC3_ADC_CTRL_A[adc_idx] & ~ANA_FGC3_ADC_CTRL_A_SOURCE_MASK32)
                                               | (0x0 <<  ANA_FGC3_ADC_CTRL_A_SOURCE_SHIFT);
                break;
        }

        switch (property.adc.filter_mpx[adc_idx])
        {
            case FGC_ADC_FILTER_MPX_0_PERCENT:
                ANA_FGC3_TEST_PATTERN_P = 0x00;
                break;

            case FGC_ADC_FILTER_MPX_12_PERCENT:
                ANA_FGC3_TEST_PATTERN_P = 0x01;
                break;

            case FGC_ADC_FILTER_MPX_50_PERCENT_F:
                ANA_FGC3_TEST_PATTERN_P = 0x55;
                break;

            case FGC_ADC_FILTER_MPX_50_PERCENT_S:
                ANA_FGC3_TEST_PATTERN_P = 0x0F;
                break;

            case FGC_ADC_FILTER_MPX_87_PERCENT:
                ANA_FGC3_TEST_PATTERN_P = 0xFE;
                break;

            case FGC_ADC_FILTER_MPX_100_PERCENT:
                ANA_FGC3_TEST_PATTERN_P = 0xFF;
                break;
        }
    }

    // Update properties ADC.INTERNAL.MPX, ADC.ANALOG_BUS.SIGNAL, ADC.ANALOG_BUS.SWIN, ADC.ANALOG_BUS.SWBUS

    AnaUpdateAdcProperties();

    // Update the calibration factors based on the new configuration

    CalCalc();
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaSaveMpx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function save the current state of the analogue card's multiplexer registers (MODMPX, SWIN, SWBUS,
  MPX). The saved multiplexing state can then be restored using function AnaRestoreMpx(). Those two
  functions are intended to be used during calibration.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;

    if (!ana.saved.in_use_f)
    {
        ana.saved.in_use_f     = TRUE;
        ana.saved.swin         = ANA_FGC3_CTRL_SWIN_P;
        ana.saved.swbus        = ANA_FGC3_CTRL_SWBUS_P;
        ana.saved.mpx          = ANA_FGC3_CTRL_MPX_P;
        ana.saved.mpx_enable   = ((ana.saved.mpx & ANA_FGC3_CTRL_MPX_ENA_MASK8) != 0);
        ana.saved.mpx         &= ANA_FGC3_CTRL_MPX_SEL_MASK8;

        for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)    // Loop on all ADCs
        {
            // ToDo why is this commented out?? we will not restore external ADCs! I do not see any good reason
            //            ana.saved.modmpx[adc_idx] = ANA_MODMPX_A[adc_idx];
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaRestoreMpx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function restores the analogue card's multiplexer registers saved with AnaSaveMpx()
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;

    if (ana.saved.in_use_f)
    {
        if (!AnaConfigSetMpx(ana.saved.swin, ana.saved.swbus, ana.saved.mpx_enable, ana.saved.mpx))
        {
            DspPanic(DSP_PANIC_HW_ANALOG_CARD);     // If we are unable to restore the previous HW settings,
            // that indicates a serious HW problem.
        }

        for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)    // Loop on all ADCs
        {
            property.adc.filter_mpx[adc_idx] = ana.saved.modmpx[adc_idx];
        }

        AnaSetModMpx();

        ana.saved.in_use_f = FALSE;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaDacSet(INT32U dac_idx, INT32S dac_raw)
/*---------------------------------------------------------------------------------------------------------*\
  This function set the DAC raw value.

 the DAC is unipolar (0 to 2.5v) but later the 0v is shifted to the middle of the range
 (there is a bit manipulation inside the PLD) so we end with

 7FFF    max positive (+12.5v)
 6665   +10v                  .... +50A
 0000    +0v
 FFFF    -0v
 9999   -10v                  .... -50A
 8000    max negative (-12.5v)

\*---------------------------------------------------------------------------------------------------------*/
{
    // Clip the raw DAC value

    dac_raw = MAX(dac_raw, cal.active->dac_factors[dac_idx].min_dac_raw);
    dac_raw = MIN(dac_raw, cal.active->dac_factors[dac_idx].max_dac_raw);

    // Write the DAC value on the HW

    ANA_FGC3_DAC_A[dac_idx] = (dac_raw << 12) & 0xFFFFF000;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp/fgc3/inc/ana.c
\*---------------------------------------------------------------------------------------------------------*/
