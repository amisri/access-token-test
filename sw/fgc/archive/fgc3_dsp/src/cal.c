/*---------------------------------------------------------------------------------------------------------*\
  File:         cal.c

  Purpose:      FGC3 Software - contains analogue calibration functions
\*---------------------------------------------------------------------------------------------------------*/

#define CAL_GLOBALS

#include <cal.h>
#include <ana.h>
#include <adc.h>
#include <libcal.h>
#include <main.h>
#include <props.h>
#include <pars.h>
#include <rtcom.h>
#include <isr.h>
#include <defprops_dsp_fgc.h>   // For ParsCalDac()
#include <dsp_dependent.h>


// ----- ADC calibration limits -----

static const struct cal_limits  adc_cal_limits[4] =
            //         OFFSET          GAIN POS/NEG
            //         WARN FAULT      WARN FAULT
            //  MID    +/-     +/-     +/-     +/-
{
    {    0.0,    0.0,    0.0,    0.0,    0.0 },     // 0 : Not Used
    { -280.0, 2000.0, 3000.0,  500.0, 2000.0 },   // 1 : ANA-101
    {    0.0, 2000.0, 3000.0,  500.0, 2000.0 },   // 2 : ANA-102
    {    0.0, 1000.0, 2000.0,  500.0, 2000.0 },   // 3 : ANA-103
};

// The VREF errors are always normalized to 40 C
#define VREF_CAL_ERR_TEMP   40.0f

// ----- DCCT calibration limits -----

static const struct cal_limits  dcct_cal_limits =
            //         OFFSET          GAIN POS/NEG
            //         WARN FAULT      WARN FAULT
            //  MID    +/-     +/-     +/-     +/-

{    0.0, 5000.0, 10000.0,  5000.0,  10000.0 };

// Static variables used only during the calibration process

static enum cal_idx       cal_signal;     // The signal being sampled (OFFSET/POS/NEG)
static INT32U             dac_idx;        // The DAC being calibrated
static FP32               cal_vref_errors[3]; // VREF gains with compensation for temperature target.

// Static functions called by CalProcess()

static void CalProcessInitHW(void);
static void CalProcessPrepareSignal(void);
static void CalProcessSamplesReady(void);

/*---------------------------------------------------------------------------------------------------------*/
void CalInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function set initial values for the calibration global variables
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U dac_idx;

    for (dac_idx = 0; dac_idx < FGC_N_DACS; dac_idx++)
    {
        // Calculate DAC raw value range (Note: will be copied in cal.next in ParsCalDac)

        cal.active->dac_factors[dac_idx].min_dac_raw = -(1 << (DAC_RESOLUTION - 1));
        cal.active->dac_factors[dac_idx].max_dac_raw = (1 << (DAC_RESOLUTION - 1)) - 1;

        // No ongoing DAC calibration

        cal.ongoing_dac_cal[dac_idx] = FALSE;
    }

    cal_vref_errors[0] = 0;                         // Not used
    cal_vref_errors[1] = property.cal.vref.err[1];  // Error for VREF+
    cal_vref_errors[2] = property.cal.vref.err[2];  // Error for VREF-
}
/*---------------------------------------------------------------------------------------------------------*/
void CalCalc()
/*---------------------------------------------------------------------------------------------------------*\
  [FGC3] This function calculates the analogue input calibrations based on the multiplexer settings at the
  time. It is called from IsrPeriod() at 1Hz, after the temperature of the internal ADCs and DCCTs has been
  updated (temperature compensation). It is also called if the Analogue card multiplexers changed position
  (in function AdcMpx() and AnaSetModMpx()), and finally in the ParsCalFactors() function whenever one of
  the following properties has changed:

    CAL.#.ADC.INTERNAL.GAIN/TC/DTC
    CAL.#.ADC.EXTERNAL.GAIN/TC/DTC
    CAL.#.DCCT.HEADERR/TC/DTC
    DCCT.#.GAIN
    DCCT.PRIMARY_TURN

  On FGC3, this function MUST ONLY be called in BGP context.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx, dcct_idx;

    if (Test(isr_switch_request_flags, ISR_SWITCH_CAL))     // If switch already pending on the CAL parameters
    {
        return;                                                 // Early return.
        // CalCalc() is called periodically at 1Hz anyway.
    }

    // --- Recalculate calibration factors ---

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)    // Loop on all ADCs
    {
        if (cal.is_internal_adc[adc_idx])       // If internal ADC
        {
            calAdcFactors(*cal.adc_nominal_gain[adc_idx],
                          cal.normalized_adc_factors[adc_idx],
                          property.adc.temperature,
                          property.cal.adc.internal[adc_idx].tc,
                          property.cal.adc.internal[adc_idx].dtc,
                          &adc_cal_limits[ana.type],
                          &cal.next->adc_factors[adc_idx]);

            // Note: the calibration flags (cal.next->adc_factors[].flags) are checked in AdcFilter

        }
        else                                    // External ADC or test pattern
        {
            // No temperature compensation and no limits check on the calibration coefficients

            calAdcFactors(*cal.adc_nominal_gain[adc_idx],
                          cal.normalized_adc_factors[adc_idx],
                          CAL_T0,
                          NULL,
                          NULL,
                          NULL,
                          &cal.next->adc_factors[adc_idx]);
        }
    }

    // --- Recalculate DCCT factors ---

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)    // Loop on both DCCTs
    {
        switch (adc_chan[adc_idx].input_type)
        {
            case ADC_INPUT_I_DCCT_A: // DCCT_A
            case ADC_INPUT_I_DCCT_B: // DCCT_B

                dcct_idx = adc_chan[adc_idx].input_idx;

                calDcctFactors(property.dcct.gain[dcct_idx],
                               property.dcct.primary_turns,
                               property.cal.dcct[dcct_idx].headerr,
                               cal.normalized_dcct_factors[dcct_idx],
                               property.dcct.temperature[dcct_idx],
                               property.cal.dcct[dcct_idx].tc,
                               property.cal.dcct[dcct_idx].dtc,
                               &dcct_cal_limits,
                               &cal.next->dcct_factors[dcct_idx]);
                break;
        }

        // Note: the calibration flags (cal.next->dcct_factors[].flags) are checked in AdcFilter
    }

    // --- Recalculate Voltage divider factors ---

    // Voltage divider 0: V load

    calVoltageDividerFactors(property.v_probe.load.gain,  // Nominal Vmeas gain (Vmeas/Vadc)
                             0.0,                         // Vmeas gain error in ppm of Vmeas/Vadc
                             &cal.next->v_meas_gain[0]);  // Returned calibration for any temp

    // Voltage divider 1: V capa

    calVoltageDividerFactors(property.v_probe.capa.gain,  // Nominal Vmeas gain (Vmeas/Vadc)
                             0.0,                         // Vmeas gain error in ppm of Vmeas/Vadc
                             &cal.next->v_meas_gain[1]);  // Returned calibration for any temp

    // Copy the DAC calibration

    cal.next->dac_factors[0] = cal.active->dac_factors[0];
    cal.next->dac_factors[1] = cal.active->dac_factors[1];

    // Trigger the switch between cal.active and cal.next on the next ISR

    DISABLE_TICKS;
    Set(isr_switch_request_flags, ISR_SWITCH_CAL);
    ENABLE_TICKS;
}
/*---------------------------------------------------------------------------------------------------------*/
void CalProcess(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function processes a calibration request (coming from the MCU or the DSP itself). A request states
  that one or several channels of a given type (internal or external ADC, DAC, DCCT) must be calibrated.
  All channels will be processed in parallel (e.g. calibrate all internal ADCs in parallel). A calibration
  typically requires a number of steps that are abstracted thanks to the state machine pictured below.

  A simple case is when only one reference signal is measured (OFFSET, POS or NEG) that will involve going
  from state CAL_REQUEST_ARMED to CAL_REQUEST_DONE in one sequence. Examples of that are external
  ADC and DCCT calibration requests.

  Another case will involve acquiring several reference signals (typically, an offset reference, a
  positive reference and a negative one) automatically. Internal ADC calibration is an example of such case.
  Then the CalProcess state machine will loop on the sequence of states going from CAL_REQUEST_PREPARE to
  CAL_REQUEST_SAMPLES_READY to measure each signal and calculate the calibration coefficients in the end.

  Finally, it is also possible to calibrate the two internal DACs (in FGC3) in one request. The CalProcess
  function will calibrate one DAC after another and for each DAC, measure the three reference voltages
  (OFFSET/POS/NEG) in sequence. The internal ADC used for DAC calibration is hard-coded, identified by
  constant CAL_ADC_FOR_DAC_MEAS. On FGC3, this is usually ADC4 (AUX channel) that is dedicated to that.

  CalProcess state machine:

         CAL_REQUEST_ARMED
          |
          v
    /--- CAL_REQUEST_PREPARE <-------------\ Loop on signal type (OFFSET/POS/NEG)
    |     |                                | and loop on DAC id for DAC calibration.
    |     v                                |
    |    CAL_REQUEST_WAIT                  |
    |     |                                |
    |     v                                |
    |    CAL_REQUEST_ACQUIRE_SAMPLES(*)    |    (*) Transition to CAL_REQUEST_SAMPLES_READY done in the ISR (in function CalAcqSamples)
    |     |                                |
    |     v                                |
    |    CAL_REQUEST_SAMPLES_READY --------/
    |     |
    |     v
    \--> CAL_REQUEST_TERMINATE
          |
          v
         CAL_REQUEST_HIDE_END_PROBLEM (State added to mask out-of-limits measurements right after the ADC multiplexing is restored)
          |
          v
         CAL_REQUEST_DONE (nothing performed while in that state: it marks the end of the calibration process)


    This function is called in BGP context.
\*---------------------------------------------------------------------------------------------------------*/
{
    static INT32U settling_time_counter;

    switch (cal.request.state)
    {
            /*-----------------------------------------------*\
             * Calibration state: CAL_REQUEST_ARMED
            \*-----------------------------------------------*/
        case CAL_REQUEST_ARMED:

            // Save the current state of the analogue card's multiplexer registers
            // as well as the ADC inputs' type and index

            AnaSaveMpx();
            AdcSaveInputs();

            // Get the HW ready for calibration, set static variable cal_signal (OFFSET/POS/NEG)

            CalProcessInitHW();

            // Go to state PREPARE

            cal.request.state = CAL_REQUEST_PREPARE;

            break;

            /*-----------------------------------------------*\
             * Calibration state: CAL_REQUEST_PREPARE
            \*-----------------------------------------------*/
        case CAL_REQUEST_PREPARE:

            // Prepare the samples acquisition for the signal input defined by cal_signal (OFFSET/POS/NEG)

            CalProcessPrepareSignal();      // State transition to CAL_REQUEST_WAIT or CAL_REQUEST_TERMINATE

            settling_time_counter = 0;
            break;

            /*-----------------------------------------------*\
             * Calibration state: CAL_REQUEST_WAIT
            \*-----------------------------------------------*/
        case CAL_REQUEST_WAIT:


            if (cal.request.action == CAL_REQ_INTERNAL_ADC) { settling_time_counter++; }
            else { settling_time_counter += 10; }

            if (settling_time_counter >= CAL_SAMPLES_BEFORE_SIGNAL_ACQ)
            {
                cal.request.state = CAL_REQUEST_ACQUIRE_SAMPLES;
            }

            break;

            // The state transition from CAL_REQUEST_ACQUIRE_SAMPLES to
            // CAL_REQUEST_SAMPLES_READY takes place in function CalAcqSamples()

            /*-----------------------------------------------*\
             * Calibration state: CAL_REQUEST_SAMPLES_READY
            \*-----------------------------------------------*/
        case CAL_REQUEST_SAMPLES_READY:

            CalProcessSamplesReady();       // State transition to CAL_REQUEST_PREPARE or CAL_REQUEST_TERMINATE

            break;

            /*-----------------------------------------------*\
             * Calibration state: CAL_REQUEST_TERMINATE
            \*-----------------------------------------------*/
        case CAL_REQUEST_TERMINATE:

            // Restore the previous state of the analogue card's multiplexer registers and ADC input types

            AdcRestoreInputs();
            AnaRestoreMpx();

            // Calibration process is now over. Acknowledge the MCU and reset the cal.ongoing_f flag

            // dpcls->dsp.cal_complete_f = TRUE;
            // cal.ongoing_f             = FALSE;
            cal.request.state         = CAL_REQUEST_HIDE_END_PROBLEM;

            // ToDo: this need to be removed when CAL_REQUEST_HIDE_END_PROBLEM is fixed
            settling_time_counter = 0;

            break;

            /*-----------------------------------------------*\
             * Calibration state: CAL_REQUEST_HIDE_END_PROBLEM
             *
             *   State added to mask out-of-limits measurements
             *   right after the ADC multiplexing is restored.
             *   This was causing a MEAS fault of the FGC after
             *   every calibration.
             *
            \*-----------------------------------------------*/
        case CAL_REQUEST_HIDE_END_PROBLEM:

            // Wait for 300 DSP interrupts (30 ms if called @1KHz)

            if (++settling_time_counter >= 300)
            {
                dpcls->dsp.cal_complete_f = TRUE;
                cal.ongoing_f             = FALSE;
                cal.request.state         = CAL_REQUEST_DONE;
            }

            break;

            /*-----------------------------------------------*\
             * Other states
            \*-----------------------------------------------*/
        case CAL_REQUEST_ACQUIRE_SAMPLES:
        case CAL_REQUEST_DONE:
        default:

            /* Do nothing */
            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void CalProcessInitHW(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by CalProcess() in state CAL_REQUEST_ARMED.

        CAL_REQUEST_ARMED
         |
         v
        ...

  This function is called by CalProcess, therefore is executed in BGP context.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;
    INT8U  adc_mask;

    // Set MODMPX for calibration

    for (adc_idx = 0, adc_mask = 1; adc_idx < FGC_N_ADCS; adc_idx++, adc_mask <<= 1)
    {
        if (adc_mask & cal.request.adc_mask)    // Modify only the ADCs that are needed for the calibration
        {
            if (cal.request.action == CAL_REQ_EXTERNAL_ADC)
            {
                property.adc.filter_mpx[adc_idx] = FGC_ADC_FILTER_MPX_EXTERNAL_ADC;
            }
            else
            {
                property.adc.filter_mpx[adc_idx] = FGC_ADC_FILTER_MPX_INTERNAL_ADC;
            }
        }
    }

    AnaSetModMpx();

    // Select the signal type

    if ((cal.request.action == CAL_REQ_INTERNAL_ADC)
        || (cal.request.action == CAL_REQ_DAC)                   // Auto calibration
       )
    {
        // For the internal ADCs and the internal DACs, the calibration is automated and
        // the FGC will automatically switch to the three calibrations signals (OFFSET/POS/NEG)

        // Start with the OFFSET signal (0V)

        cal_signal = CAL_OFFSET_V;
    }
    else                                                                                 // Manual calibration
    {
        // For external ADC or DCCT, the calibration is performed one signal at a time because it requires
        // a manual operation between each signal.
        // The signal type is given by cal.request.idx

        cal_signal = cal.request.idx;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void CalProcessPrepareSignal(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by CalProcess() in state CAL_REQUEST_PREPARE. It sets the calibration signal
  (if internal to the FGC) and routes it to the channel(s) being calibrated to prepare samples acquisition.

        ...
         |
         v
    /-- CAL_REQUEST_PREPARE <-------------\ Loop on signal type (OFFSET/POS/NEG)
    |    |                                | and loop on DAC id for DAC calibration.
    |    v                                |
   ...  ...                              ...


  This function is called by CalProcess, therefore is executed in BGP context.
\*---------------------------------------------------------------------------------------------------------*/
{
    static const INT32S             dac_ref     [CAL_NUM_ERRS] = { 0,
                                                                   DAC_CAL_REF,
                                                                   -DAC_CAL_REF
                                                                 };

    static const ANALOGUE_BUS_INPUT anabus_input[CAL_NUM_ERRS] = { ANABUS_INPUT_CAL_ZERO,
                                                                   ANABUS_INPUT_CAL_POSREF,
                                                                   ANABUS_INPUT_CAL_NEGREF
                                                                 };

    INT32U adc_idx;
    INT8U  adc_mask;


    // Reset the v_raw average structures

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
    {
        calAverageVraw(&cal.average_v_raw[adc_idx],
                       CAL_NUM_SAMPLES_PER_AVE_STEP * cal.request.ave_steps,
                       0);
    }

    // Prepare the signal given by 'cal_signal'

    if (cal_signal < CAL_NUM_ERRS)      // Range check
    {
        switch (cal.request.action)
        {
                /*------------------------------------*\
                  Internal ADC calibration
                \*------------------------------------*/
            case CAL_REQ_INTERNAL_ADC:

                AnaConfigAnalogueBus(anabus_input[cal_signal], cal.request.adc_mask);

                for (adc_idx = 0, adc_mask = 1; adc_idx < FGC_N_ADCS; adc_idx++, adc_mask <<= 1)
                {
                    if (adc_mask & cal.request.adc_mask)
                    {
                        adc_chan[adc_idx].input_type = ADC_INPUT_DIRECT;
                    }
                }

                break;

                /*------------------------------------*\
                  External ADC calibration
                \*------------------------------------*/
            case CAL_REQ_EXTERNAL_ADC:

                /* No specific HW settings for external ADC calibration */

                break;

                /*------------------------------------*\
                  DCCT calibration
                \*------------------------------------*/
            case CAL_REQ_DCCT:

                AnaConfigAnalogueBus(ANABUS_INPUT_NONE, ANA_MASK_NO_ADC);
                AdcSignalsToChannels();

                break;

                /*------------------------------------*\
                  DAC calibration
                \*------------------------------------*/
            case CAL_REQ_DAC:

                if (cal.request.chan_mask & 1)          // Calibrate DAC1
                {
                    AnaConfigAnalogueBus(ANABUS_INPUT_DAC1, cal.request.adc_mask);      // CAL_ADC_FOR_DAC_MEAS
                    dac_idx = 0;        // static variable
                }
                else                                    // Calibrate DAC2
                {
                    AnaConfigAnalogueBus(ANABUS_INPUT_DAC2, cal.request.adc_mask);      // CAL_ADC_FOR_DAC_MEAS
                    dac_idx = 1;        // static variable
                }

                cal.ongoing_dac_cal[dac_idx] = TRUE;    // Flag preventing the DSP from setting the Vref value on the DAC

                adc_chan[CAL_ADC_FOR_DAC_MEAS].input_type = ADC_INPUT_DIRECT;   // Direct voltage measurement

                property.ref.dac[dac_idx] = dac_ref[cal_signal];
                AnaDacSet(dac_idx, property.ref.dac[dac_idx]);                  // Set DAC voltage
                break;
        }

        cal.request.state     = CAL_REQUEST_WAIT;
    }
    else
    {
        cal.request.state     = CAL_REQUEST_TERMINATE;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void CalProcessSamplesReady(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by CalProcess() in state CAL_REQUEST_TERMINATE.

  ...   ...
   |     |
   |     v
   \--> CAL_REQUEST_TERMINATE
         |
         v
        ...

  This function is called by CalProcess, therefore is executed in BGP context.
\*---------------------------------------------------------------------------------------------------------*/
{

    INT32U adc_idx, dcct_idx;
    INT8U  adc_mask, dcct_mask;
    FP32   ext_ref_err[CAL_NUM_ERRS];   // Reference error (in ppm) used for the calibration external ADC.
    // Filled in based on property CAL.EXT_REF_ERR.

    union  adc_cal_meas adc_meas;       // used for DAC calibration

    // Gather the average Vraw for all ADCs

    if (cal_signal < CAL_NUM_ERRS)      // Range check
    {
        for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
        {
            property.cal.adc.internal[adc_idx].vraw[cal_signal] = cal.average_v_raw[adc_idx].v_raw_ave;
        }
    }

    // Default next state (can change to CAL_REQUEST_PREPARE below)

    cal.request.state = CAL_REQUEST_TERMINATE;

    // Take actions

    switch (cal.request.action)
    {
            /*------------------------------------*\
              Internal ADC calibration
            \*------------------------------------*/
        case CAL_REQ_INTERNAL_ADC:

            cal_signal++;

            if (cal_signal >= CAL_NUM_ERRS)
            {
                // Compensate for variations of VREF temperature reference as follows:
                // ErrorVrefGain+(T) = ErrorVRefGain+ + DeltaVRefGain+(T)
                // DeltaVRefGain+(T) = (ADC.INTERNAL.VREF_TEMP.REF - Tcal) * CAL.VREF.TC
                cal_vref_errors[1] = property.cal.vref.err[1] + (dpcls->mcu.adc.vref_temp.ref - property.cal.vref.err[3]) *
                                     property.cal.vref.tc[1];
                cal_vref_errors[2] = property.cal.vref.err[2] + (dpcls->mcu.adc.vref_temp.ref - property.cal.vref.err[3]) *
                                     property.cal.vref.tc[2];

                // Got the three acquisition samples. Compute the new calibration coefficients

                for (adc_idx = 0, adc_mask = 1; adc_idx < FGC_N_ADCS; adc_idx++, adc_mask <<= 1)
                {
                    if (adc_mask & cal.request.adc_mask)
                    {
                        calAdcErrors(&property.cal.adc.internal[adc_idx].vraw[0],
                                     property.cal.adc.internal[adc_idx].gain,
                                     property.adc.temperature,
                                     property.cal.adc.internal[adc_idx].tc,
                                     property.cal.adc.internal[adc_idx].dtc,
                                     cal_vref_errors,
                                     property.cal.vref.tc,
                                     (struct cal_event *)dpcls->mcu.cal.adc.internal[adc_idx].err);

                        calEventStamp((struct cal_event *)dpcls->mcu.cal.adc.internal[adc_idx].err,
                                      rtcom.time_now_us.unix_time,
                                      property.adc.temperature);
                    }
                }
            }
            else
            {
                // Continue calibration with signals CAL_ERR_POS and CAL_ERR_NEG.

                cal.request.state = CAL_REQUEST_PREPARE;
            }

            break;

            /*------------------------------------*\
              External ADC calibration
            \*------------------------------------*/
        case CAL_REQ_EXTERNAL_ADC:

            // Error (ppm) on the external reference

            ext_ref_err[CAL_OFFSET_V]     = 0.0;
            ext_ref_err[CAL_GAIN_ERR_POS] = property.cal.ext_ref_err;
            ext_ref_err[CAL_GAIN_ERR_NEG] = property.cal.ext_ref_err;

            // Calculate the new calibration factors for the requested external ADCS

            for (adc_idx = 0, adc_mask = 1; adc_idx < 2; adc_idx++, adc_mask <<= 1)
            {
                if (adc_mask & cal.request.chan_mask)
                {
                    // It is the responsibility of the operator to carry out the three calibration
                    // steps (OFFSET,POS,NEG) in the right order (i.e. starting with OFFSET)

                    calAdcError(cal_signal,
                                property.cal.adc.internal[adc_idx].vraw[cal_signal],
                                property.cal.adc.external[adc_idx].gain,
                                CAL_T0,
                                NULL,
                                NULL,
                                ext_ref_err,
                                NULL,
                                (struct cal_event *)dpcls->mcu.cal.adc.external[adc_idx].err);

                    calEventStamp((struct cal_event *)dpcls->mcu.cal.adc.external[adc_idx].err,
                                  rtcom.time_now_us.unix_time,
                                  CAL_T0);
                }
            }

            break;

            /*------------------------------------*\
              DCCT calibration
            \*------------------------------------*/
        case CAL_REQ_DCCT:

            for (adc_idx = 0, dcct_mask = 1; adc_idx < FGC_N_ADCS; adc_idx++, dcct_mask <<= 1)
            {
                switch (adc_chan[adc_idx].input_type)
                {
                    case ADC_INPUT_I_DCCT_A: // DCCT_A
                    case ADC_INPUT_I_DCCT_B: // DCCT_B

                        dcct_idx = adc_chan[adc_idx].input_idx;

                        if (dcct_mask & cal.request.chan_mask)
                        {
                            // It is the responsibility of the operator to carry out the three calibration
                            // steps (OFFSET,POS,NEG) in the right order (i.e. starting with OFFSET)

                            calDcctError(cal_signal,
                                         property.cal.adc.internal[adc_idx].vraw[cal_signal],
                                         &cal.active->adc_factors[adc_idx],
                                         property.dcct.temperature[dcct_idx],
                                         property.cal.dcct[dcct_idx].tc,
                                         property.cal.dcct[dcct_idx].dtc,
                                         property.cal.dcct[dcct_idx].headerr,
                                         (struct cal_event *)dpcls->mcu.cal.dcct[dcct_idx].err);

                            calEventStamp((struct cal_event *)dpcls->mcu.cal.dcct[dcct_idx].err,
                                          rtcom.time_now_us.unix_time,
                                          property.dcct.temperature[dcct_idx]);
                        }

                        break;
                }
            }

            break;

            /*------------------------------------*\
              DAC calibration
            \*------------------------------------*/
        case CAL_REQ_DAC:

            // Get the calibrated voltage corresponding to the average Vraw value on the ADC used for the measurement

            AdcCalibratedMeas(CAL_ADC_FOR_DAC_MEAS,
                              property.cal.adc.internal[CAL_ADC_FOR_DAC_MEAS].vraw[cal_signal],
                              FALSE,
                              &adc_meas);

            property.cal.dac[dac_idx][cal_signal] = adc_meas.voltage.v_meas;    // The measured voltage is copied in
            // property CAL.#.DAC (# = A or B)

            cal_signal++;      // Continue the calibration with DAC voltages DAC_CAL_REF and -DAC_CAL_REF.

            if (cal_signal >= CAL_NUM_ERRS)
            {
                // Got the three acquisition samples. Compute the new calibration coefficients

                ParsCalDac();

                // Force the switching of the calibration parameters.
                DISABLE_TICKS;
                Set(isr_switch_request_flags, ISR_SWITCH_CAL);
                ENABLE_TICKS;

                // Calibrate the other DAC if necessary, else terminate the calibration process

                if (dac_idx == ANA_IDX_DAC1 && (cal.request.chan_mask & 2) != 0)
                {
                    cal.request.chan_mask &= 2;                 // chan_mask is set for DAC2
                    cal_signal             = CAL_OFFSET_V;
                    cal.request.state      = CAL_REQUEST_PREPARE;
                }

                cal.ongoing_dac_cal[dac_idx] = FALSE;    // Clear the flag for DAC on-going calibration

                // Zero the DAC output

                ref.dac_raw[dac_idx] = calDacSet(&cal.active->dac_factors[dac_idx], 0.0);
                AnaDacSet(dac_idx, ref.dac_raw[dac_idx]);
            }
            else
            {
                cal.request.state      = CAL_REQUEST_PREPARE;
            }

            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalNewRequest(BOOLEAN from_mcu_f, enum cal_action action, enum cal_idx idx, INT32U ave_steps,
                      INT32U chan_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to register a new calibration request coming from the MCU or the DSP itself.
  It returns TRUE to acknowledge the request is being processed, and FALSE otherwise. In the latter case,
  the caller should try passing the request some other time.

  This function is called in BGP context.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (cal.ongoing_f)
    {
        return FALSE;
    }
    else
    {
        // If chan_mask == 0, there is nothing to calibrate.
        // The request is pointless so do not take it into account
        if (chan_mask != 0)
        {
            cal.ongoing_f = TRUE;

            cal.request.from_mcu_f = from_mcu_f;
            cal.request.action     = action;
            cal.request.idx        = idx;           // Calibration idx (OFFSET/POS/NEG) is only relevant for
            // external ADC and DCCT calibration.
            cal.request.ave_steps  = ave_steps;
            cal.request.chan_mask  = chan_mask;     // Channel(s) to calibrate (can be ADCs, DACs or DCCTs)

            if (cal.request.action == CAL_REQ_DAC)  // If DAC calibration
            {
                // Set the adc_mask corresponding to the ADC being used to measure the DAC voltage (usually ADC4)

                cal.request.adc_mask   = ANA_IDX_TO_MASK(CAL_ADC_FOR_DAC_MEAS);
            }
            else                                    // Internal/external ADC calibration, DCCT calibration.
            {
                cal.request.adc_mask   = cal.request.chan_mask;
            }

            cal.request.state = CAL_REQUEST_ARMED;
        }

        return TRUE; // calibration acknowledged
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CalAcqSamples(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called during calibration state CAL_REQUEST_ACQUIRE_SAMPLES

  This function is called in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U   adc_mask;
    INT32U  adc_idx;

    for (adc_idx = 0, adc_mask = 1; adc_idx < FGC_N_ADCS; adc_idx++, adc_mask <<= 1)
    {
        if (adc_mask & cal.request.adc_mask)
        {
            if (!calAverageVraw(&cal.average_v_raw[adc_idx], 0, adc_chan[adc_idx].meas.voltage.v_raw))
            {
                cal.request.state = CAL_REQUEST_SAMPLES_READY;           // If last sample
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
enum cal_idx CalTranslateCpuIdx(INT32U idx)
/*---------------------------------------------------------------------------------------------------------*\
  This helper function translates the MCU signal IDX to the CAL library 'enum cal_idx'
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (idx)
    {
        case CAL_IDX_OFFSET: return (CAL_OFFSET_V);

        case CAL_IDX_POS:    return (CAL_GAIN_ERR_POS);

        case CAL_IDX_NEG:    return (CAL_GAIN_ERR_NEG);
    }

    return (CAL_NUM_ERRS);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cal.c
\*---------------------------------------------------------------------------------------------------------*/

