/*---------------------------------------------------------------------------------------------------------*\
  File:         ref.c

  Purpose:      FGC DSP Software - contains Ref functions

  Notes:        Ref functions return 0 if the reference function has not finished and 1 when time exceeds
                the end of the function.
\*---------------------------------------------------------------------------------------------------------*/

#include <ref.h>
#include <cc_types.h>
#include <mcu_dsp_common.h>
#include <macros.h>             // for Test()
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <rtcom.h>              // for rtcom global variable
#include <reg.h>                // for reg, load global variables
#include <main.h>               // for state_pc global variable
#include <meas.h>               // for meas global variable
#include <refto.h>              // for RefToEnd()
#include <cyc.h>                // for cyc global variable, CycLogAbsMaxErr()
#include <bgp.h>                // for BgpInitNone()
#include <dsp_dependent.h>      // for FromIEEE()
#include <deftypes.h>           // for MIN/MAX
#include <math.h>               // for fabs()
#include <ppm.h>                // for ppm[]
#include <dpcls.h>
#include <definfo.h>
#include <pc_state.h>


// Reference function pointer array

INT32U(*ref_func[])(void) =     // Ref gen functions
{
    RefNone,                    // 0  FGC_REF_NONE
    RefStarting,                // 1  FGC_REF_STARTING
    RefStopping,                // 2  FGC_REF_STOPPING
    RefPlep,                    // 3  FGC_REF_TO_STANDBY
    RefNone,                    // 4  FGC_REF_ARMED
    RefPlep,                    // 5  FGC_REF_ABORTING
    RefNone,                    // 6  FGC_REF_NOW
    RefTest,                    // 7  FGC_REF_STEPS
    RefTest,                    // 8  FGC_REF_SQUARE
    RefTest,                    // 9  FGC_REF_SINE
    RefTest,                    // 10 FGC_REF_COSINE
    RefNone,                    // 11 FGC_REF_PLP    (Used in Class 59 and not in Class 51/53)
    RefPlep,                    // 12 FGC_REF_PLEP
    RefPppl,                    // 13 FGC_REF_PPPL   (Used in class 53 and not in class 51)
    RefTrim,                    // 14 FGC_REF_LTRIM
    RefTrim,                    // 15 FGC_REF_CTRIM
    RefTable,                   // 16 FGC_REF_TABLE
    RefOpenloop,                // 17 FGC_REF_OPENLOOP
    RefZero,                    // 18 FGC_REF_ZERO
    RefEnd,                     // 19 FGC_REF_END
    RefNone,                    // 20 FGC_REF_IDLE
    RefPlep,                    // 21 FGC_REF_PLEP_NO_CLIP
};

// Internal functions declaration

static inline BOOLEAN IsNaN(DP_IEEE_FP32 value);

// Internal function definitions

/*---------------------------------------------------------------------------------------------------------*/
static inline BOOLEAN IsNaN(DP_IEEE_FP32 value)
/*---------------------------------------------------------------------------------------------------------*\
  This function return TRUE if the value passed is not a number (NaN).
\*---------------------------------------------------------------------------------------------------------*/
{

    return isnan(value);
}

/*---------------------------------------------------------------------------------------------------------*/
void Ref(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function calculates the reference (current or voltage depending upon the mode).
  This function is called on each regulation iteration whether it is I, B or V reg mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Copy the reference values for previous regulation iteration in the corresponding ref_container->prev_value
    // Do it for ALL instances of struct ref_container.

    ref.i.prev_value          = ref.i.value;
    ref.i_clipped.prev_value  = ref.i_clipped.value;
    ref.b.prev_value          = ref.b.value;
    ref.v.prev_value          = ref.v.value;
    ref.fg.prev_value         = ref.fg.value;
    ref.fg_plus_rt.prev_value = ref.fg_plus_rt.value;

    // Get the real-time value from the gateway (only important if MODE.RT is ENABLED)

    RefRtCtrl();

    // If to_standby flag set due to state change then switch reference go to standby

    if (refto.flags.standby)
    {
        RefToStandby();
    }

    // Else if to_aborting flag is set

    else if (refto.flags.aborting)
    {
        RefToAborting();
    }

    else if (refto.flags.end)
    {
        RefToEnd();
    }

    // Calculate predefined reference according to reference type

    if (ref_func[ref.func_type]())
    {
        // Function has finished

        if (ref.cycling_f)
        {
            CycLogAbsMaxErr();
//            RefToEnd();
        }
        else
        {
            BgpInitNone(FGC_REG_V, 0);          // First argument is ignored
            ref.func_type = FGC_REF_NONE;
        }
    }

    // If to_stopping flag is set and no reference is playing (this must always follow a TO_STANDBY reference)

    if (refto.flags.stopping &&
        ref.func_type == FGC_REF_NONE)
    {
        RefToStopping();
    }

#if (FGC_CLASS_ID == 61)

    // Compute the reference value following the algorithm below:
    // if STATE_PC = DIRECT
    //    REF = CCV + (MODE.RT ? RT : 0)
    // else
    //    REF = FG + (MODE.RT ? RT : 0)
    // endif

    if (state_pc == FGC_PC_DIRECT)
    {
        ref.fg_plus_rt.value = (reg.active->state == FGC_REG_I? property.ref.ccv : property.ref.vcv);

        // Check if the regulation mode has changed

        if (reg.active->state != dpcls->mcu.ref.func.reg_mode[0])
        {
            RegStateRequest(FALSE, TRUE, dpcls->mcu.ref.func.reg_mode[0], ref.fg_plus_rt.value);
        }
    }
    else
    {
        ref.fg_plus_rt.value = ref.fg.value;
    }

    if (PcStateAbove(FGC_PC_ON_STANDBY) &&
        dpcls->mcu.mode_rt           &&
        !IsNaN(ref.rt))
    {
        ref.fg_plus_rt.value += ref.rt;

        // In case of 1 quadrant, libreg limits are 0 to I_POS, libfg limits are I_MIN to I_POS
        // We want limits to be from I_MIN to I_POS, however, here we bypass libfg
        // So we check here for limits. Note ccv is only for FGC_REG_I
        // call to fgCheckRef here?
        // We temporary (till direct mode state) use FGC_WRN_ILIM_EXPECTED (unused) to warn when limit is clipped
        // ToDo This should be reviewed once direct state is added.
        if (lim_i_ref.flags.unipolar == TRUE)
        {
            if (ref.fg_plus_rt.value < property.limits.op.i.min)
            {
                ref.fg_plus_rt.value = property.limits.op.i.min;    // Clip to i.min
                Set(rtcom.warnings, FGC_WRN_ILIM_EXPECTED);         // instead of FGC_WRN_REF_LIM
            }
            else
            {
                Clr(rtcom.warnings, FGC_WRN_ILIM_EXPECTED);         // instead of FGC_WRN_REF_LIM
            }
        }

    }

#endif
    RefRefreshValue(&ref.fg);
    RefRefreshValue(&ref.fg_plus_rt);
}
/*---------------------------------------------------------------------------------------------------------*/
void RefRtCtrl(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function processes the real-time control data from the gateway according to the MODE.RT property.
  For periods < 20ms then the RT data (received every 20ms) is interpolated.
  In states ON_STANDBY and below, the RT value is ignored and REF.RT is set to zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    DP_IEEE_FP32        rtdata;                                 // Real-time control value from gateway (IEEE FP)
    FP32                rtvalue;                                // Real-time control value from gateway (TI FP)

    // If real-time data not being used then set it to zero.

    if (!dpcls->mcu.mode_rt || PcStateBelowEqual(FGC_PC_ON_STANDBY))
    {
        rt_ref_data.value_gw = 0xFFFFFFFF;
        rt_ref_data.value    = 0.0;
        rt_ref_data.final    = 0.0;
        ref.rt               = 0.0;
        return;
    }

    // If new real-time control data received then check it

    if (dpcls->mcu.ref.rt_data_f)
    {
        dpcls->mcu.ref.rt_data_f = FALSE;
        rtdata = dpcls->mcu.ref.rt_data;

        if (!IsNaN(rtdata)  &&                          // If valid value received (!NaN)
            rtdata != rt_ref_data.value_gw)             // and it has changed
        {
            rt_ref_data.value_gw = rtdata;              // Remember the new value
            rtvalue = FromIEEE(rtdata);                 // Convert RT data to TI FP format

            if (rtvalue < -property.limits.op.i.pos ||  // If RT value is crazy
                rtvalue >  property.limits.op.i.pos)
            {
                property.fieldbus.rt_bad_values++;      // Increment bad val counter
            }
            else                                        // else value is good - prepare interpolation
            {
                rt_ref_data.time_s  = 0.0;
                rt_ref_data.value = rt_ref_data.final;
                rt_ref_data.final = rtvalue;
                rt_ref_data.step  = rt_ref_data.final - rt_ref_data.value;
            }
        }
    }

    // Interpolate if iteration period < 20ms

    if (reg.active->rst_pars.period < INTERPOLATION_TIME)
    {
        rt_ref_data.time_s += reg.active->rst_pars.period;

        if (rt_ref_data.time_s < INTERPOLATION_TIME)
        {
            ref.rt = rt_ref_data.value + rt_ref_data.step * rt_ref_data.time_s * (1.0 / INTERPOLATION_TIME);
        }
        else
        {
            ref.rt = rt_ref_data.final;

            rt_ref_data.time_s = INTERPOLATION_TIME;
        }
    }
    else
    {
        ref.rt = rt_ref_data.final;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RefFirstPlateau(void)
/*---------------------------------------------------------------------------------------------------------*\
  First plateau is used for PPPL for B and I regulation modes, and TABLE in B regulation mode.
  It starts the cycle in openloop, applying a voltage according to the REF.START properties.  It then
  starts regulating field or current and ramps with a Linear-Parabola function (pLeP) to the first
  plateau level.
  To transition between openloop to closeloop at time REF.START.TIME + REF.START.DURATION, this function
  uses cyc.time timing reference, not ref.time, so that the BIV checks (function cycBIVChecks) are done
  precisely at the time of the transition (cycBIVChecks also relies on cyc.time).
\*---------------------------------------------------------------------------------------------------------*/
{
    // Before REF.START.TIME the open loop voltage reference is zero

    if (cyc.time.s < property.ref.start.time)
    {
        EnableRegBypass();
        ref.fg.value = ref.v_openloop = 0.0;
    }

    // Until REF.START.TIME + REF.START.DURATION apply voltage

    else if (cyc.time.s < (property.ref.start.time + property.ref.start.duration))
    {
        ref.v_openloop = property.ref.start.vref;
    }
    else // Then after REF.START.TIME + REF.START.DURATION close loop and regulate to first plateau
    {
        // If regulation bypass is still enabled then close loop (which disables bypass)

        if (TestRegBypass())
        {

            // TODO This is possibly a worst case scenario for the ISR, due to the call to FgPlepCalc
            //      Need to investigate the performance cost.

            struct fg_plep_config     config;
            struct fg_first_plateau * first_plateau = &ppm[cyc.ref_user].cycling.first_plateau;

            // Prepare RST history to have bumpless loop closing

            RegVarsInit(ref.v.value, reg.active->rst_pars.period, reg.active->rst_pars.rst.track_delay);

            // Set the reference to the last value in the RST rebuilt history

            RefResetValue(&ref.fg, reg_vars.ref[0]);

            // Prepare PLEP function to first plateau

            config.final_rate   = 0.0;
            config.final        = first_plateau->ref;
            config.linear_rate  = MIN(fabs(meas.hist->rate), property.limits.op.i.rate);

#if (FGC_REG_B_SUPPORT == TRUE)
            config.acceleration = (reg.active->state == FGC_REG_B ?
                                   property.ref.defaults.b.acceleration :
                                   FromIEEE(dpcls->mcu.ref.defaults.i.acceleration[load.active->select]));
#else
            config.acceleration =  FromIEEE(dpcls->mcu.ref.defaults.i.acceleration[load.active->select]);
#endif

            config.exp_tc       = 0.0;

            fgPlepCalc(&config, &plep, (FP32)ref.time.s, reg_vars.ref[0], config.linear_rate, NULL);

            // Cycle Check - START_TIME : First plateau arrival time

            if (plep.time[4] > first_plateau->time)
            {
                CycSaveCheckWarning(TRIGG_LOG, FGC_CYC_WRN_START_TIME, FGC_WRN_CYCLE_START,
                                    first_plateau->time,                             // REF.CYC.FAILED.DATA[0]
                                    plep.time[4],                                    // REF.CYC.FAILED.DATA[1]
                                    meas.hist->meas,                                 // REF.CYC.FAILED.DATA[2]
                                    meas.hist->rate);                                // REF.CYC.FAILED.DATA[3]
            }

            ref.fg.value =  reg_vars.ref[0];

            DisableRegBypass();
        }
        else // Use PLEP to ramp to first plateau
        {
            fgPlepGen(&plep, (const double *)&ref.time.s, &ref.fg.value);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefNone(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING,RUNNING] This function is called when ref.type is FGC_REF_NONE or FGC_REF_ARMED.  In this state,
  the predefined reference is held constant.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefZero(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING] This function is used for a ZERO cycle.  It can only be armed for V and I regulation modes.
  For V it holds the Vref at zero for the whole cycle while for I the function CTRIMs to the first plateau.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(lim_i_ref.flags.unipolar && property.limits.op.i.min != 0)
    {
        ref.fg.value = property.limits.op.i.min;

        return(0);
    }

    // Return zero

    return (fgZeroGen(&ref.fg.value));

}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefPppl(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING] This function derives the reference if the PPPL function is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (ref.time.s < ref.active->pars.pppl.delay)
    {
        RefFirstPlateau();
        return (0);
    }
    else
    {
        return (!fgPpplGen(&ref.active->pars.pppl, (const double *)&ref.time.s, &ref.fg.value));
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefTable(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING,RUNNING] This function derives the reference if the TABLE function is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Start with FIRST_PLATEAU if B regulation is active

#if (FGC_REG_B_SUPPORT == TRUE)
    if (reg.active->state == FGC_REG_B &&
        ref.time.s < ref.active->table_data.function[1].time)
    {
        RefFirstPlateau();
        return (0);
    }

#endif

    return (!fgTableGen(&ref.active->pars.table, (const double *)&ref.func_time, &ref.fg.value));
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefPlep(void)
/*---------------------------------------------------------------------------------------------------------*\
  [RUNNING] This function derives the reference if the PLEP function is selected - this is used for the
  ramp to STANDBY or when the user selects the PLEP reference function.  Note that plep contains the
  working PLEP parameters while ref_fg.plep contain the user PLEP parameters.  The working parameters
  can be the user parameters or can be calculated following a SLOW_ABORT or command to ramp to ON_STANDBY,
  or the end of an OPENLOOP function.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (!fgPlepGen(&plep, (const double *)&ref.time.s, &ref.fg.value));
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefTrim(void)
/*---------------------------------------------------------------------------------------------------------*\
  [RUNNING] This function derives the reference if the LTRIM or CTRIM function is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (!fgTrimGen(&ref_fg.trim, (const double *)&ref.time.s, &ref.fg.value));
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefTest(void)
/*---------------------------------------------------------------------------------------------------------*\
  [RUNNING] This function derives the reference if a Test function (STEPS, SQUARE, SINE or COSINE) is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (!fgTestGen(&ref_fg.test, (const double *)&ref.time.s, &ref.fg.value));
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefOpenloop(void)
/*---------------------------------------------------------------------------------------------------------*\
  [RUNNING] This function derives the reference if the OPENLOOP function is active.  It is not included in
  the fg library.  When the current loop is closed again, the reference runs PLEP.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct fg_plep_config config;

    // Pre trim coast - hold Vref fixed

    if (ref.time.s <= ref_fg.openloop.run_delay)
    {
        ref.v_openloop = ref.v_fltr;
    }

    // Ramping

    else
    {
        // Ramp voltage smoothly to OPENLOOP Vref level (provides smooth acceleration)

        ref.v_openloop += ref_fg.openloop.d_v_ref;

        if ((ref_fg.openloop.pos_ramp_flag && ref.v_openloop > ref_fg.openloop.v_ref)
            || (!ref_fg.openloop.pos_ramp_flag && ref.v_openloop < ref_fg.openloop.v_ref))
        {
            ref.v_openloop = ref_fg.openloop.v_ref;
        }

        // If close loop threshold passed - prepare to end function with a PLEP

        if ((ref_fg.openloop.pos_ramp_flag && meas.hist->meas > ref_fg.openloop.i_closeloop)
            || (!ref_fg.openloop.pos_ramp_flag && meas.hist->meas < ref_fg.openloop.i_closeloop))
        {
            // Prepare PLEP configuration.
            // Important: The linear rate CAN exceed LIMITS.OP.I.RATE in some cases and therefore the reference PLEP_NO_CLIP
            //            is used instead of a regular PLEP (PLEP_NO_CLIP is the same function except clipping is not
            //            applied to it in the regulation algorithm.)

            config.final        = ref_fg.openloop.final;
            config.linear_rate  = fabs(ref.fg.rate);
            config.exp_tc       = 0.0;
            config.acceleration = 0.5 * config.linear_rate * config.linear_rate / fabs(config.final - ref.fg.estimated);

            // Calculate working PLEP parameters

            // NB: the alternative that is to call fgPlepCalc with ref.time as the function delay is not advised
            //     here since the duration of an openloop function can be several hours and there might be a
            //     precision issue on the PLEP timings.
            //     Instead we choose to reset the time (ref.time.s = 0.0) and to set the PLEP delay equal to 0.0.

            fgPlepCalc(&config, &plep, 0.0, ref.fg.estimated, ref.fg.rate, &ppm[0].armed.meta);

            ref.time.s              = 0.0;
            ref.time.period_counter = 0;

            // Prepare to run PLEP on next iteration

            ref.time_remaining.s = plep.time[4] - ref.time.s;
            ref.time_remaining.period_counter = DSP_CEIL_TIME_VP(ref.time_remaining.s);

            // Switch to PLEP_NO_CLIP function type, that shall close the loop on the current iteration and therefore
            // one needs to set a value for ref.fg.value

            ref.func_type        = FGC_REF_PLEP_NO_CLIP;
            ref.fg.value         = ref.fg.estimated;

            // Update meta data and RTD

            ppm[0].armed.meta.duration    = ref.time_remaining.s;

            ppm[0].armed.meta.range.start =        ref.fg.value;
            dpcls->dsp.ref.start          = ToIEEE(ref.fg.value);
            ppm[0].armed.meta.range.end   =        config.final;
            dpcls->dsp.ref.end            = ToIEEE(config.final);

            ppm[0].armed.meta.range.min   = MIN(ref.fg.value, config.final);
            ppm[0].armed.meta.range.max   = MAX(ref.fg.value, config.final);
        }
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefEnd(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING] This function returns the current to zero as quickly as possible using open-loop (Class 53 only)
\*---------------------------------------------------------------------------------------------------------*/
{
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefStarting(void)
/*---------------------------------------------------------------------------------------------------------*\
  [TO_STANDBY] This function sets the reference to the estimated values during the openloop startup period.
  This is a minimum of 10 regulation periods to ensure that the RST history is correctly tracking the open
  loop behaviour.  After this, it checks the end-of-startup threshold, which is always true for bipolar
  converters and is the I_CLOSELOOP for unipolar converters.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct meas_hist * i_meas_hist_ptr;         // Pointer to the measurement history used to estimate Irate.

    if (reg.hi_speed_f)                         // On high-speed converters (regulation period <= 10 ms)
    {
        // Irate measured based on Imeas history over the last 4 DSP iterations
        i_meas_hist_ptr = &meas.i_hist;
    }
    else
    {
        // Irate measured based on Imeas history over the last 4 regulation periods
        i_meas_hist_ptr = &meas.i_hist_reg;
    }

    // Check for end of start

    if (ref.time_running.s >= ref_fg.starting.delay)    // Once RST history buffer is ready
    {
        if (ref.time_remaining.s == 0.0)                // If timeout
        {
            Set(rtcom.faults, FGC_FLT_REG_ERROR);
            // Report FB fault to MCU
        }

        // Detect when to close the loop

        else if (!lim_i_ref.flags.unipolar ||           // Else, if bipolar circuit or
                 fabs(meas.i) > limits.i_closeloop)     // I > Icloseloop.
        {
            refto.flags.standby = TRUE;                 // Closeloop and PLEP to I_MIN
        }
        else
        {
            // STARTING openloop function, divided into two parts:
            //  1 - A very slow ramp that lasts for around 6 s and that raises the current up to limits.i_start.
            //      limits.i_start is equal to 1% of LIMITS.I.MIN
            //  2 - A dI/dt close loop with a fixed ramp equal to REF.RATE.I.STARTING until the current reaches
            //      LIMITS.I.CLOSELOOP, at which point the FGC will switch to normal I regulation.

            if (ref_fg.starting.openloop_f)                     // If openloop start mode
            {
                ref.v_openloop += (reg.active->rst_pars.period * ref_fg.starting.v_rate);    // Ramp Vref with constant dVdt

                if (INDUCTIVE_LOAD &&                           // If inductive load (Tc > 1s)
                    fabs(meas.i) > limits.i_start)              // and current above start threshold
                {
                    ref_fg.starting.openloop_f = FALSE;         // Close loop on dI/dt
                }
            }
            else                                                // else closed loop start mode (on dI/dt)
            {
                FP32 i_rate = ref_fg.starting.i_rate;

                if (!reg_v_err.flags.warning &&                 // If VS gain is good and
                    !(RESISTIVE_ZERO_IS_SIGNIFICANT))            // parallel resistance pole is not significant
                {
                    // Close loop on dI/dt

                    ref.v_openloop += reg.active->rst_pars.period *
                                      (i_rate * load.active->pars.ohms +
                                       load.active->pars.henrys * (i_rate - i_meas_hist_ptr->rate));
                }
                else                                            // else set Vref using openloop on Imeas
                {
                    ref.v_openloop = meas.i * load.active->pars.ohms + load.active->pars.henrys * i_rate;
                }
            }
        }
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT32U RefStopping(void)
/*---------------------------------------------------------------------------------------------------------*\
  [STOPPING] This function derives the reference if the STOPPING function is active.  It is only used by
  2-Quadrant converters to ramp down smoothly in openloop to 1% of I_MIN.  It calculates the Vref value by a
  proportional feedback control on dI/dt.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32               vref_clip_neg;
    struct meas_hist * i_meas_hist_ptr;         // Pointer to the measurement history used to estimate Irate.

    if (reg.hi_speed_f)                         // On high-speed converters (regulation period <= 10 ms)
    {
        // Irate measured based on Imeas history over the last 4 DSP iterations
        i_meas_hist_ptr = &meas.i_hist;
    }
    else
    {
        // Irate measured based on Imeas history over the last 4 regulation periods
        i_meas_hist_ptr = &meas.i_hist_reg;
    }

    // Close loop on dI/dt. The target dI/dt (i_rate_min) is reached progressively starting from i_rate_init.

    // Ramp down the dI/dt reference
    ref_fg.stopping.i_rate += ref_fg.stopping.i_rate_step;
    // Clip reference to target level
    ref_fg.stopping.i_rate  = MAX(ref_fg.stopping.i_rate, ref_fg.stopping.i_rate_min);

    ref.v_openloop += reg.active->rst_pars.period *
                      (ref_fg.stopping.i_rate * load.active->pars.ohms +
                       load.active->pars.henrys * (ref_fg.stopping.i_rate - i_meas_hist_ptr->rate));


    if (property.limits.op.i.min != 0.0)
    {
        // Clip Vref so that 1% of I_MIN gives 2% of V_NEG. This provides a smooth stop.

        vref_clip_neg = 2.0 * property.limits.op.v.neg * meas.hist->estimated / property.limits.op.i.min;

        if (ref.v_openloop < vref_clip_neg)             // Clip voltage
        {
            ref.v_openloop = vref_clip_neg;
        }
    }

    if (meas.hist->estimated > limits.i_start)          // If current is still greater than start threshold
    {
        return (0);
    }

    return (1);
}
/*---------------------------------------------------------------------------------------------------------*/
void RefRefreshValue(struct ref_container * ref_ptr)
/*---------------------------------------------------------------------------------------------------------*\
  Utility function on struct ref_container.

  Calling that function acknowledge the new value of the reference ref_ptr->value and compute the rate of
  change and the estimated reference on the next reg iteration.

  Caution: The update of ref_ptr->prev_value must be done once at the start of every regulation iteration.
           It was decided to do it for all instantiations of the ref_container in the Ref() function, instead
           of doing it in the present function. Doing it in RefRefreshValue() would have the drawback that
           if RefRefreshValue() is called twice during the same regulation iteration, the computed rate
           would be zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 diff = ref_ptr->value - ref_ptr->prev_value;

    ref_ptr->estimated = ref_ptr->value + diff;
    ref_ptr->rate      = diff * reg.active->rst_pars.freq;

    // ref_ptr->prev_value is updated in Ref(). Do not edit it in this function, see comment above.
}
/*---------------------------------------------------------------------------------------------------------*/
void RefResetValue(struct ref_container * ref_ptr, FP32 reset_value)
/*---------------------------------------------------------------------------------------------------------*\
  Utility function on struct ref_container.

  Reset the structure, assuming the reference was constant equal to reset_value until now.
\*---------------------------------------------------------------------------------------------------------*/
{
    ref_ptr->estimated = ref_ptr->value = ref_ptr->prev_value = reset_value;
    ref_ptr->rate = 0.0;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ref.c
\*---------------------------------------------------------------------------------------------------------*/

