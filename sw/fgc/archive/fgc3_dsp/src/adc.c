/*---------------------------------------------------------------------------------------------------------*\
  File:     adc.c

  Purpose:  FGC3 DSP Software - ADC functions

  Notes:    All real-time processing runs at interrupt level under IsrPeriod().
\*---------------------------------------------------------------------------------------------------------*/

#define ADC_GLOBALS

#include <adc.h>
#include <ana.h>
#include <cal.h>
#include <sim.h>
#include <libcal.h>
#include <cc_types.h>
#include <defconst.h>
#include <deftypes.h>
#include <memmap_dsp.h>
#include <rtcom.h>
#include <dsp_dependent.h>
#include <props.h>
#include <meas.h>
#include <reg.h>
#include <dpcls.h>
#include <dsp_time.h>
#include <macros.h>
#include <limits.h>
#include <report.h>
#include <bgp.h>
#include <sleep.h>
#include <unipolar_switch.h>

inline void AdcMeanFilterNewSample(const union adc_cal_meas * new_sample,
                                   struct filter_mean_vars * out_filter_mean_vars);
inline void AdcMeanFilterAccumulate(const struct filter_mean_vars * filter_mean_vars,
                                    struct filter_mean_vars * out_accumulator);

/*---------------------------------------------------------------------------------------------------------*/
void AdcResetSignalsPointers(void)
{
    // Reseting pointers
    adc_current_a = NULL;
    adc_current_b = NULL;
    adc_v_meas_load = NULL;
    adc_v_meas_capa = NULL;
    adc_aux = NULL;
}

/*---------------------------------------------------------------------------------------------------------*/

void AdcSignalToChannel(INT32U signal_type, INT32U signal_nr)
{
    if (signal_type == (INT32U)FGC_ADC_SIGNALS_I_DCCT_A)
    {
        adc_chan[signal_nr].input_type = ADC_INPUT_I_DCCT_A;
        adc_chan[signal_nr].input_idx  = 0;
        adc_current_a = &adc_chan[signal_nr];
        adc_chan_v_adc[signal_nr] = &adc_chan[signal_nr].meas.current.v_adc;
    }
    else if (signal_type == (INT32U)FGC_ADC_SIGNALS_I_DCCT_B)
    {
        adc_chan[signal_nr].input_type = ADC_INPUT_I_DCCT_B;
        adc_chan[signal_nr].input_idx  = 1;
        adc_current_b = &adc_chan[signal_nr];
        adc_chan_v_adc[signal_nr] = &adc_chan[signal_nr].meas.current.v_adc;
    }
    else if (signal_type == (INT32U)FGC_ADC_SIGNALS_V_LOAD)
    {
        adc_chan[signal_nr].input_type = ADC_INPUT_V_MEAS_LOAD;
        adc_chan[signal_nr].input_idx  = 0;
        adc_v_meas_load = &adc_chan[signal_nr];
        adc_chan_v_adc[signal_nr] = &adc_chan[signal_nr].meas.voltage.v_adc;
    }
    else if (signal_type == (INT32U)FGC_ADC_SIGNALS_V_CAPA)
    {
        adc_chan[signal_nr].input_type = ADC_INPUT_V_MEAS_CAPA;
        adc_chan[signal_nr].input_idx  = 1;
        adc_v_meas_capa = &adc_chan[signal_nr];
        adc_chan_v_adc[signal_nr] = &adc_chan[signal_nr].meas.voltage.v_adc;
    }
    else if (signal_type == (INT32U)FGC_ADC_SIGNALS_AUX)
    {
        adc_chan[signal_nr].input_type = ADC_INPUT_DIRECT;
        adc_chan[signal_nr].input_idx  = 0;
        adc_aux = &adc_chan[signal_nr];
        adc_chan_v_adc[signal_nr] = &adc_chan[signal_nr].meas.voltage.v_adc;
    }
    else
    {
        adc_chan[signal_nr].input_type = ADC_INPUT_NONE; // Direct input
        adc_chan[signal_nr].input_idx  = 0;
        adc_chan_v_adc[signal_nr] = &adc_chan[signal_nr].meas.voltage.v_adc;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void AdcSignalsToChannels(void)
{
    INT32U signal_nr;
    AdcResetSignalsPointers();

    for (signal_nr = 0; signal_nr < FGC_N_ADCS; signal_nr++)
    {
        AdcSignalToChannel((INT32U)property.adc.internal_signals[signal_nr], signal_nr);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises access to the ADCs. It calls the AnaInit() function to set the Analogue card in
  its default configuration meaning the Analogue bus (COM input/output) is floating and each ADC is
  connected to its respective input.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;

    // This is a workaround for the problem with synchronization of config properties from the DB
    // for more info see FGC-236
    // setting ADC.INTERNAL.SIGNALS to default channels
    property.adc.internal_signals[0] = FGC_ADC_SIGNALS_I_DCCT_A;
    property.adc.internal_signals[1] = FGC_ADC_SIGNALS_I_DCCT_B;
    property.adc.internal_signals[2] = FGC_ADC_SIGNALS_V_LOAD;
    property.adc.internal_signals[3] = FGC_ADC_SIGNALS_AUX;

    AdcSignalsToChannels();             // Assigning signals to channels
    AnaInit();                          // Initialise the Analogue card. Set the default configuration

    switch (ana.type)
    {
        case FGC_INTERFACE_ANA_101:

            adc.stuck_limit = 100;          // 100 equal samples in a row to raise the SIGNAL_STUCK flag

            for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
            {
                // ToDo To be reviewed, just hardcoded to limit range to 22 bits
                ANA_FGC3_ADC_SHIFT_BITS_A[adc_idx] = CAL_DEF_INTERNAL_ADC_SHIFT;
            }

            break;

        case FGC_INTERFACE_ANA_103:

            adc.stuck_limit = 100;          // 100 equal samples in a row to raise the SIGNAL_STUCK flag
            break;
    }

    // Pointers to the DCCT factors cal_event structure (will not be modified)

    cal.normalized_dcct_factors[0] = (struct cal_event *)dpcls->mcu.cal.dcct[0].err;    // DCCT_A
    cal.normalized_dcct_factors[1] = (struct cal_event *)dpcls->mcu.cal.dcct[1].err;    // DCCT_B

    // Initialise history buffer for 20-sample filter

    adc.filter_20ms.history_idx = (VRAW_SUM_HIST_LEN - 1);

    // This delay is done in order to properly update analog bus properties below
    sleepUs(500);

    // Update properties related to analog bus
    AnaUpdateAdcProperties();
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcCalibratedMeas(INT32U adc_idx, INT32S adc_raw, BOOLEAN sim_flag,
                       union adc_cal_meas * output_measurement)
/*---------------------------------------------------------------------------------------------------------*\
  If the sim_flag is not set, this function translates for a given ADC channel (adc_idx) the ADC raw
  output (Vraw) to a voltage or current measurement according to the ADC type of input.

        sim_flag == FALSE:    Vraw ---> Idcct or Vmeas or Vadc

  If the sim_flag is set, this function does the opposite operation (simulated current or voltage gives Vraw)

        sim_flag == TRUE:     sim.v or sim.i  --->  Vraw

  This function is called in ISR context and BGP context
\*---------------------------------------------------------------------------------------------------------*/
{
    const struct cal_v_meas * v_div_factors;
    const struct cal_dcct  *  dcct_factors;

    v_div_factors = &cal.active->v_meas_gain [adc_chan[adc_idx].input_idx];
    dcct_factors  = &cal.active->dcct_factors[adc_chan[adc_idx].input_idx];

    switch (adc_chan[adc_idx].input_type)
    {
        case ADC_INPUT_NONE:
        case ADC_INPUT_DIRECT:

            v_div_factors = &CAL_DIRECT_V_MEAS;                 // Voltage gain = 1

            // Fall through

        case ADC_INPUT_V_MEAS_CAPA:

            calVoltage(v_div_factors,                           // Voltage divider calibration factors
                       &cal.active->adc_factors[adc_idx],       // ADC calibration factors
                       adc_raw,                                 // ADC raw value
                       sim.active->v,                           // Simulated divider voltage (used if sim_flag is TRUE)
                       sim_flag,                                // Simulation flag
                       & (output_measurement->voltage));        // Returned voltage values

            UniSwitchVMeasCapa(&output_measurement->voltage.v_meas);

            break;

        case ADC_INPUT_V_MEAS_LOAD:

            calVoltage(v_div_factors,                           // Voltage divider calibration factors
                       &cal.active->adc_factors[adc_idx],       // ADC calibration factors
                       adc_raw,                                 // ADC raw value
                       sim.active->v,                           // Simulated divider voltage (used if sim_flag is TRUE)
                       sim_flag,                                // Simulation flag
                       & (output_measurement->voltage));        // Returned voltage values

            UniSwitchVMeasLoad(&output_measurement->voltage.v_meas);

            break;

        case ADC_INPUT_I_DCCT_A:
        case ADC_INPUT_I_DCCT_B:

            calCurrent(dcct_factors,                            // DCCT calibration factors
                       &cal.active->adc_factors[adc_idx],       // ADC calibration factors
                       adc_raw,                                 // ADC raw value
                       sim.active->i,                           // Simulated DCCT current (used if sim_flag is TRUE)
                       sim_flag,                                // Simulation flag
                       & (output_measurement->current));        // Returned current/voltage values

            UniSwitchIMeas(adc_idx, &output_measurement->current.i_dcct);

            break;
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void AdcRead(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets the ADC data from the Analogue interface at the start of each regulation period.
  This function also transforms the raw ADC output to a V or I measurement.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;

    if (dpcls->mcu.adc_block_f)
    {
        return;
    }

    // Read ADC data and calibrate the measurement

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
    {
        // Read raw data  from the ADC
        // Note: TMS320C6x compiler preserves the sign when doing right shifts

        if (!sim.active->enabled)
        {
            adc_chan[adc_idx].adc_raw = ANA_FGC3_ADC_SAMPLE_A[adc_idx];
        }
        else
        {
            adc_chan[adc_idx].adc_direct = 0;
        }

        // Calibrate the data

        if (rtcom.state_op == FGC_OP_UNCONFIGURED)  // Hide acquisition if UNCONFIGURED (after startup, no cal yet)
        {
            adc_chan[adc_idx].meas.voltage.v_raw  = adc_chan[adc_idx].adc_raw;
            adc_chan[adc_idx].meas.voltage.v_adc  = 0.0;
            adc_chan[adc_idx].meas.voltage.v_meas = 0.0;
        }
        else
        {
            AdcCalibratedMeas(adc_idx, adc_chan[adc_idx].adc_raw, sim.active->enabled, &adc_chan[adc_idx].meas);

            if (sim.active->enabled)
            {
                adc_chan[adc_idx].adc_raw = adc_chan[adc_idx].meas.current.v_raw;       // Copy the back-calculated Vraw
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcFilter(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function processes the ADC signals. It runs four different filters on the raw data:

                Duration    Type
       ------------------------------------------------------------
        1       1ms         average, min/max, peak-peak
        2       20ms        sliding average with phase compensation
        3       200ms       average, min/max, peak-peak
        4       1s          average, min/max, peak-peak

  The 20ms filter is designed to filter out the 50Hz harmonic from the measurement, in low-speed regulation.

  This function also applies a check for unexpected signal jumps and for the signal becoming stuck.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U              c;
    INT32U              mask;
    INT32S              v_raw;
    INT32S              raw;
    INT32U
    old_vraw_sum_idx;   // Index of the oldest value in adc_chan[].filter_20ms.vraw_sum_history[]
    INT64S              old_vraw_sum;
    union adc_cal_meas  previous_meas;


    dcct_select      = dpcls->mcu.dcct_select;
    cal.imeas_zero_f = dpcls->mcu.cal.imeas_zero;       // Set for ADC/DAC calibration

    // Control cal active down counter

    if (cal.imeas_zero_f                                // If current measurement is blocked
        && (rtcom.state_op != FGC_OP_CALIBRATING)      // and calibration has finished
       )
    {
        dpcls->mcu.cal.imeas_zero--;                    // Decrement block down counter
    }

    // Adjust the 20ms filter indexes OUTSIDE of the "for each channel" loop

    if (++adc.filter_20ms.buffer_idx >= adc.filter_20ms.nb_of_samples)
    {
        adc.filter_20ms.buffer_idx = 0;
    }

    if (!(adc.filter_20ms.history_idx--))                          // Index for the 100 element history of the 20 sample filtered values
    {
        adc.filter_20ms.history_idx = (VRAW_SUM_HIST_LEN - 1);
    }

    old_vraw_sum_idx = (adc.filter_20ms.history_idx + adc.filter_20ms.history_offset);

    if (old_vraw_sum_idx >= VRAW_SUM_HIST_LEN)
    {
        old_vraw_sum_idx -= VRAW_SUM_HIST_LEN;
    }

    // Loop on all channels

    if (adc_current_a != NULL && dpcls->mcu.dcct_flt & 0x01)
    {
        Set(adc_current_a->st_dcct, FGC_DCCT_FAULT); 
    }
    else
    {
        Clr(adc_current_a->st_dcct, FGC_DCCT_FAULT); 
    }

    if (adc_current_b != NULL && dpcls->mcu.dcct_flt & 0x02)
    {
        Set(adc_current_b->st_dcct, FGC_DCCT_FAULT);
    }
    else
    {
        Clr(adc_current_b->st_dcct, FGC_DCCT_FAULT);
    }
    
    for (c = 0, mask = 1; c < FGC_N_ADCS; c++, mask <<= 1)      // For each channel
    {
        v_raw                     = adc_chan[c].meas.voltage.v_raw;
        previous_meas             = adc_chan[c].previous_meas;
        adc_chan[c].previous_meas = adc_chan[c].meas;                   // Save raw measurement

        // Signal status

        if (cal.active->adc_factors[c].flags.fault)                     // If ADC calibration fault is present
        {
            Clr(adc_chan[c].st_meas, FGC_ADC_STATUS_V_MEAS_OK);               // Mark channel voltage as invalid
        }

        if (cal.active->adc_factors[c].flags.warning)
        {
            Set(adc_chan[c].st_meas, FGC_ADC_STATUS_CAL_FAILED);
        }
        else
        {
            Clr(adc_chan[c].st_meas, FGC_ADC_STATUS_CAL_FAILED);
        }

        if (adc_chan[c].input_type == ADC_INPUT_I_DCCT_A || adc_chan[c].input_type == ADC_INPUT_I_DCCT_B)
        {
            if (cal.active->dcct_factors[adc_chan[c].input_idx].flags.fault)
            {
                Set(adc_chan[c].st_dcct, FGC_DCCT_CAL_FLT);
            }
            else
            {
                Clr(adc_chan[c].st_dcct, FGC_DCCT_CAL_FLT);
            }
        }
        else
        {
            Clr(adc_chan[c].st_dcct, FGC_DCCT_CAL_FLT);
        }

        // SIGNAL_STUCK detection

        if (!sim.active->enabled                                        // If not simulating,
            && Test(adc_chan[c].st_meas, FGC_ADC_STATUS_V_MEAS_OK)           // and no voltage error,
            && (v_raw == previous_meas.voltage.v_raw)                  // and if the signal is stuck
           )
        {
            if (!Test(adc_chan[c].st_meas, FGC_ADC_STATUS_SIGNAL_STUCK))      // If channel not yet stuck
            {
                adc_chan[c].stuck_counter++;

                if (adc_chan[c].stuck_counter > adc.stuck_limit)        // If no change for too long
                {
                    Set(adc_chan[c].st_meas, FGC_ADC_STATUS_SIGNAL_STUCK);    // Channel is stuck
                    Clr(adc_chan[c].st_meas, FGC_ADC_STATUS_V_MEAS_OK);       // Channel voltage is invalid
                }
            }
        }
        else
        {
            if (Test(adc_chan[c].st_meas, FGC_ADC_STATUS_SIGNAL_STUCK))      // If channel was stuck
            {
                adc_chan[c].stuck_counter--;

                if (!(adc_chan[c].stuck_counter))                      // If count is zero
                {
                    Clr(adc_chan[c].st_meas, FGC_ADC_STATUS_SIGNAL_STUCK);   // Channel not stuck
                }
            }
            else                                                       // else channel not stuck
            {
                adc_chan[c].stuck_counter = 0;                         // Reset counter
            }
        }

        // ----------------------------------------------------------------------
        // Filter 1 - 1ms filter
        // ----------------------------------------------------------------------

        // NB: As long as the DSP period is 1ms, that filter gives identical measurement to adc_chan[c].meas

        AdcMeanFilterNewSample(&adc_chan[c].meas, &adc_chan[c].filter_1ms_vars);

        if (rtcom.start_of_ms_f)
        {
            // This is for the 200ms filter
            AdcMeanFilterAccumulate(&adc_chan[c].filter_1ms_vars, &adc_chan[c].filter_200ms_vars);
            AdcMeanFilterProcess(c, &adc_chan[c].filter_1ms_vars, &adc_chan[c].ms);
        }

        // ----------------------------------------------------------------------
        // Filter 2 - 20ms sliding window filter with latency compensation,
        //            aimed at damping the 50Hz harmonic in low-speed reg
        // ----------------------------------------------------------------------

        adc_chan[c].filter_20ms.vraw_sum += (v_raw - adc_chan[c].filter_20ms.vraw_buffer[adc.filter_20ms.buffer_idx]);
        adc_chan[c].filter_20ms.vraw_buffer[adc.filter_20ms.buffer_idx] =  v_raw;

        old_vraw_sum = adc_chan[c].filter_20ms.vraw_sum_history[old_vraw_sum_idx];
        adc_chan[c].filter_20ms.vraw_sum_history[adc.filter_20ms.history_idx] = adc_chan[c].filter_20ms.vraw_sum;

        // Filtered raw value. Add 0.5 to the floating-point part of the equation and nb_of_samples/2 to the total
        // to round the result to the nearest integer.

        raw = (adc_chan[c].filter_20ms.vraw_sum +
               (INT64S)(adc.filter_20ms.time_ratio * (FP32)(adc_chan[c].filter_20ms.vraw_sum - old_vraw_sum) + 0.5) +
               adc.filter_20ms.nb_of_samples / 2
              ) / adc.filter_20ms.nb_of_samples;

        // Clip the raw value

        raw = MAX(-FLT_CLIP, raw);
        raw = MIN(raw, FLT_CLIP);

        // Call AdcCalibratedMeas with sim_flag = FALSE because the 'raw' ADC value is already
        // the result of a simulation if sim.enabled is set

        AdcCalibratedMeas(c, raw, FALSE, &adc_chan[c].ms20.filtered);

        // Decrement the Imeas error downcounter

        if (adc_chan[c].ms20.err_downcounter)
        {
            adc_chan[c].ms20.err_downcounter--;
        }
    }   // End for each channel

    // Ignore simultaneous signal jumps (on IA and IB.)

    if (adc_current_a != NULL && adc_current_b != NULL)
    {
        adc.i_dcct_match_f = (fabs(adc_current_b->meas.current.i_dcct - adc_current_a->meas.current.i_dcct) <
                              property.limits.i.meas_diff);
    }

    // ----------------------------------------------------------------------
    // Filter 3 - 200ms filter
    // ----------------------------------------------------------------------

    if (rtcom.start_of_ms200_f)                             // If 200 ms boundary
    {
        for (c = 0, mask = 1; c < FGC_N_ADCS; c++, mask <<= 1)  // For each channel
        {
            // This is for the 1s filter
            AdcMeanFilterAccumulate(&adc_chan[c].filter_200ms_vars, &adc_chan[c].filter_1s_vars);
        }

        // Trigger the 200ms filter processing in the BGP task
        bgp.trigger.process_200ms_filter_and_report = TRUE;
    }

    // ----------------------------------------------------------------------
    // Filter 4 - 1 second filter
    // ----------------------------------------------------------------------

    if (rtcom.start_of_second_f)                            // If 1 second boundary
    {
        bgp.trigger.process_1s_filter_and_report =
            TRUE;                // Trigger the 1s filter processing in the BGP task
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcMpx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called if the ADC multiplexer or SD filters are changed by the MCU.  It will extract
  the correct shift for the ADC values (used only for SD filters values, not for the SAR interface).  The
  function AdcRead uses this shift value.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;

    if (ana.type == FGC_INTERFACE_ANA_101)
    {
        for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
        {
            ANA_FGC3_ADC_SHIFT_BITS_A[adc_idx] = dpcls->mcu.sd_shift[adc_idx];
        }
    }

    CalCalc();                      // Recalculate calibrations immediately

    dpcls->mcu.adc_mpx_f = 0;           // Clear request
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcGatePars(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will calculate DSP's reset_gate_f for the ADC filters. If FALSE, that gate will prevent
  all ADC resets, except if manually triggered with S ADC RESET or S ADC.FILTER RESET.

  This is always true on FGC3, not the same happens in FGC2
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 61)

    switch (reg.active->state)
    {
        case FGC_REG_I:
        case FGC_REG_B:
        case FGC_REG_V:

            dpcls->dsp.adc.reset_gate_f = TRUE;
            break;
    }

#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcSaveInputs(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function ...
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)    // Loop on all ADCs
    {
        adc_save.input_type[adc_idx] = adc_chan[adc_idx].input_type;
        adc_save.input_idx [adc_idx] = adc_chan[adc_idx].input_idx;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcRestoreInputs(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function ...
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U adc_idx;

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)    // Loop on all ADCs
    {
        adc_chan[adc_idx].input_type = adc_save.input_type[adc_idx];
        adc_chan[adc_idx].input_idx  = adc_save.input_idx [adc_idx];
    }
}
/*---------------------------------------------------------------------------------------------------------*/
inline void AdcMeanFilterNewSample(const union adc_cal_meas * new_sample,
                                   struct filter_mean_vars * out_filter_mean_vars)
/*---------------------------------------------------------------------------------------------------------*\
  This function stores a new sample for a basic mean filter
\*---------------------------------------------------------------------------------------------------------*/
{
    out_filter_mean_vars->raw_sum += new_sample->voltage.v_raw;

    // Min/Max measurements.
    // This is independent from the type of input (I, B or V), therefore we can use new_sample->voltage
    // in place of new_sample->current or new_sample->field.

    if (new_sample->voltage.v_raw < out_filter_mean_vars->min.v_raw)
    {
        out_filter_mean_vars->min = new_sample->voltage;
    }

    if (new_sample->voltage.v_raw > out_filter_mean_vars->max.v_raw)
    {
        out_filter_mean_vars->max = new_sample->voltage;
    }

    // Increment number of samples

    out_filter_mean_vars->nb_samples++;
}
/*---------------------------------------------------------------------------------------------------------*/
inline void AdcMeanFilterAccumulate(const struct filter_mean_vars * filter_mean_vars,
                                    struct filter_mean_vars * out_accumulator)
/*---------------------------------------------------------------------------------------------------------*\
  This function accumulates the data stored in on struct filter_mean_vars (input filter_mean_vars) into
  another structure of the same type (output out_accumulator).

  When computing several filters whose lengths constitute a monotonic series, in which each length is a
  multiple of previous one (in the case of the FGC: 1s = 5 * 200ms = 200 * 1ms,) it is more efficient to
  store the individual samples in the shortest filter only, and, at the time when the program computes the
  result of that filter, accumulate all its data into the next longer filter. And so on for the next filters.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (filter_mean_vars->min.v_raw < out_accumulator->min.v_raw)
    {
        out_accumulator->min = filter_mean_vars->min;
    }

    if (filter_mean_vars->max.v_raw > out_accumulator->max.v_raw)
    {
        out_accumulator->max = filter_mean_vars->max;
    }

    out_accumulator->raw_sum    += filter_mean_vars->raw_sum;
    out_accumulator->nb_samples += filter_mean_vars->nb_samples;
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcMeanFilterProcess(INT32U adc_idx, struct filter_mean_vars * filter_mean_vars,
                          struct filter_mean_cal_meas * out_filter_mean_cal_meas)
/*---------------------------------------------------------------------------------------------------------*\
  This function computes the result of a basic mean filter based on the data accumulated in the input
  filter_mean_vars. This function can be called in ISR or BGP context
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U              raw;
    struct cal_voltage * pp;             // Peak to peak values

    DISABLE_TICKS;

    //
    // Populate filter_mean_cal_meas
    //

    raw = filter_mean_vars->raw_sum / filter_mean_vars->nb_samples;

    // Min/max values

    out_filter_mean_cal_meas->min.voltage = filter_mean_vars->min;
    out_filter_mean_cal_meas->max.voltage = filter_mean_vars->max;

    // Peak to peak values

    pp = &out_filter_mean_cal_meas->peak_to_peak.voltage;
    pp->v_raw  = filter_mean_vars->max.v_raw  - filter_mean_vars->min.v_raw;
    pp->v_adc  = filter_mean_vars->max.v_adc  - filter_mean_vars->min.v_adc;
    pp->v_meas = filter_mean_vars->max.v_meas - filter_mean_vars->min.v_meas;

    //
    // Reset filter_mean_vars
    //

    filter_mean_vars->min.v_raw  = INT_MAX;
    filter_mean_vars->max.v_raw  = INT_MIN;
    filter_mean_vars->min.v_adc  = 0;
    filter_mean_vars->max.v_adc  = 0;
    filter_mean_vars->min.v_meas = 0;
    filter_mean_vars->max.v_meas = 0;
    filter_mean_vars->nb_samples = 0;
    filter_mean_vars->raw_sum    = 0;

    ENABLE_TICKS;

    // Call AdcCalibratedMeas with sim_flag = FALSE because the 'raw' ADC value is already the result
    // of a simulation if sim.enabled is TRUE. This does not need to be in a critical section.

    AdcCalibratedMeas(adc_idx, raw, FALSE, &(out_filter_mean_cal_meas->filtered));
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcUpdateInputSignal(INT32U adc_idx, ANALOGUE_BUS_INPUT input_signal, INT32U filter_mpx)
/*---------------------------------------------------------------------------------------------------------*\
  This function must be called after the multiplexing changed on the Analogue card. It will set the type of
  input for one of the ADC channels.

  With the information of the input type and index (adc_chan[c].input_type/input_idx) set by this function,
  the FGC is able to use the right calibration factors depending on the input signal. For example, the DCCT
  calibration for current measurement channels, the voltage calibration for a VS, or no calibration factors
  for a direct input.

  This function will also update the value of property ADC.INTERNAL.MPX.

  For more information regarding the design of ADC multiplexing on FGC3 please refer to the following
  doc: "Z:\projects\fgc\doc\development\fgc3\ADC Multiplexing with ANA101 Card.pdf" (in the FGC superproject)
\*---------------------------------------------------------------------------------------------------------*/
{
    if (filter_mpx == FGC_ADC_FILTER_MPX_INTERNAL_ADC)                          // If INTERNAL ADC
    {
        dpcls->dsp.ana.mpx[adc_idx] = (INT32S)input_signal;                     // Update property ADC.INTERNAL.MPX
    }
    else if (filter_mpx == FGC_ADC_FILTER_MPX_EXTERNAL_ADC && adc_idx <= 1)     // If EXTERNAL ADC channel A or B
    {
        dpcls->dsp.ana.mpx[adc_idx] = ANABUS_INPUT_NONE;    // ADC.INTERNAL.MPX = NC (Not connected)
    }
    else                                                                        // In any other case
    {
        dpcls->dsp.ana.mpx[adc_idx] = ANABUS_INPUT_NONE;    // ADC.INTERNAL.MPX = NC (Not connected)
    }

    // Do not call CalCalc() immediately. That function is called every second by the background processing
    // task, and it will take into account the new values of adc_chan[adc_idx].input_type and input_idx.
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: adc.c
\*---------------------------------------------------------------------------------------------------------*/
