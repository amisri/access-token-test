/*!
 *  @file      state.c
 *  @defgroup  FGC3:DSP
 *  @brief     DSP operation state declaration.
 *  These are only called if STATE.PC has changed
 */

#include <state.h>
#include <cc_types.h>
#include <defconst.h>
#include <rtcom.h>
#include <pars.h>

// External function definitions

void StateOp(INT32U new_state_op)
{
    //  Class 53 doesn't include LOAD.SELECT so force setting of property.load.select once FGC is configured

    if (rtcom.state_op == FGC_OP_UNCONFIGURED)
    {
        ParsLoadSelect();
    }

    // Save new OP state and and process MEAS.SIM which depends on OP state

    rtcom.state_op = new_state_op;

    ParsMeasSim();
}

// EOF

