/*---------------------------------------------------------------------------------------------------------*\
  File:         time.c

  Purpose:      FGC2 Class 51 Software - Time and events handling
\*---------------------------------------------------------------------------------------------------------*/

// time.c function declarations
// TODO Rename time.c and create corresponding header file. time.h already exists as part of the compiler header files and dsp_time.h is already taken.


#include <bgp.h>
#include <cc_types.h>
#include <cyc.h>        // for cyc global variable
#include <defprops_dsp_fgc.h>
#include <dpcls.h>      // for dpcls global variable
#include <dpcom.h>      // for dpcom global variable
#include <dsp_dependent.h> // for test points TP0/TP1
#include <dsp_panic.h>
#include <dsp_time.h>
#include <isr.h>
#include <macros.h>     // for Test()
#include <main.h>       // for state_pc global variable
#include <mcu_dsp_common.h>
#include <pars.h>
#include <props.h>      // for property global variable
#include <ref.h>        // for TestRefAbortable()
#include <refto.h>      // for RefToAborting()
#include <reg.h>        // for reg, load global variables
#include <rtcom.h>      // for rtcom global variable
#include <log_signals.h>
#include <log_signals_class.h>
#include <time_fgc.h>
#include <memmap_dsp.h>
#include <dig_sup.h>


/*---------------------------------------------------------------------------------------------------------*/
static void TimeCalcEvt(struct abs_time_us * abs_time, INT32U delay_ms)
/*---------------------------------------------------------------------------------------------------------*\
  This function produces an absolute time structure that correspond to the present Fieldbus time, plus
  a delay expressed in milliseconds
\*---------------------------------------------------------------------------------------------------------*/
{
    AbsTimeCopyFromMsToUs((const struct abs_time_ms *)&dpcom->mcu.time.fbs, abs_time);

    AbsTimeUsAddDelay(abs_time, 1000 * delay_ms);
}
/*---------------------------------------------------------------------------------------------------------*/
void TimeIteration(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at the start of every iteration to manage the iteration time
\*---------------------------------------------------------------------------------------------------------*/
{
    // Get time now and 10/20/200ms time from MCU (FGC3) or UTC register (FGC3)

    INT32U ms_time = TimeGetUtcTimeMs();
    static INT32U dsp_isr_cnt;
    static const INT32U dsp_isr_freq = 10000;

    dsp_isr_cnt++;

    if (rtcom.time_now_ms.ms_time != ms_time)                           // If start of a new millisecond
    {
        rtcom.time_now_ms.unix_time = TimeGetUtcTime();
        rtcom.time_now_ms.ms_time = ms_time;
        AbsTimeCopyFromMsToUs(&rtcom.time_now_ms, &rtcom.time_now_us);
        rtcom.ms10         = rtcom.time_now_ms.ms_time % 10;
        rtcom.ms20         = rtcom.time_now_ms.ms_time % 20;
        rtcom.ms200        = rtcom.time_now_ms.ms_time % 200;

        rtcom.start_of_ms_f = TRUE;
    }
    else
    {
        rtcom.start_of_ms_f = FALSE;
    }

    // Start of 10/20/200/1000ms flags. To be used ONLY in ISR context.

    rtcom.start_of_ms10_f   = rtcom.start_of_ms_f && (rtcom.ms10  == 0);
    rtcom.start_of_ms20_f   = rtcom.start_of_ms_f && (rtcom.ms20  == 0);
    rtcom.start_of_ms200_f  = rtcom.start_of_ms_f && (rtcom.ms200 == 0);
    rtcom.start_of_second_f = rtcom.start_of_ms_f && (rtcom.time_now_ms.ms_time == 0);

    if (rtcom.start_of_second_f)
    {

        // Trigger 1 Hz processing on the background processing task. Detect overrun of that processing.

        if (bgp.trigger.one_hertz_processing == TRUE)           // If overrun of the BGP task
        {
            DspPanic(DSP_PANIC_BGP_1HZ_OVERRUN);
        }

        bgp.trigger.one_hertz_processing = TRUE;

        // Monitor RT ISR

        if (dpcom->mcu.pll.state == FGC_PLL_LOCKED &&
            dpcom->dsp.run_code  == 0              &&
            dsp_isr_cnt          != dsp_isr_freq)
        {
            dpcom->dsp.run_code = RUNCODE_DSP_ISR_ERROR;
            dpcom->dsp.debug[0] = rtcom.dsp_utc_us;
            dpcom->dsp.debug[1] = rtcom.last_dsp_utc_us;
            dpcom->dsp.debug[2] = TimeGetUs();
            dpcom->dsp.debug[3] = rtcom.start_freerun_us;
            dpcom->dsp.debug[4] = rtcom.last_start_freerun_us;
            dpcom->dsp.debug[5] = dsp_isr_cnt;

        }

        dsp_isr_cnt = 0;
    }

    if (((1000000 + rtcom.dsp_utc_us) - rtcom.last_dsp_utc_us) % 1000000 != 100)
    {
        dpcom->dsp.isr_overrun++;

        if (dpcom->dsp.run_code == 0)
        {
            dpcom->dsp.run_code = RUNCODE_DSP_ISR_ERROR2;
            dpcom->dsp.debug[0] = rtcom.dsp_utc_us;
            dpcom->dsp.debug[1] = rtcom.last_dsp_utc_us;
            dpcom->dsp.debug[2] = TimeGetUs();
            dpcom->dsp.debug[3] = rtcom.start_freerun_us;
            dpcom->dsp.debug[4] = rtcom.last_start_freerun_us;
            dpcom->dsp.debug[5] = dsp_isr_cnt;
        }
    }

    if (dpcom->mcu.runcode_handshake == 1)
    {
        dpcom->dsp.run_code = 0;
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void TimeRunningEvents(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the start/abort timing event delays from the MCU to see if a Start or Abort timing
  event has been received.  It sets the relevant property value and processes the time.
  This function is called on every iteration period in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    // If Start event is active then update REF.RUN time

    if (dpcom->mcu.evt.start_event_delay != 0)
    {
        if (property.ref.run.unix_time == 0)
        {
            TimeCalcEvt(&property.ref.run, dpcom->mcu.evt.start_event_delay);

            ParsRunTime();           
        }

        if (state_pc == FGC_PC_IDLE)
        {
            dpcom->mcu.evt.start_event_delay = 0;
        }
    }

    // If Abort event is active then update REF.ABORT time

    if (dpcom->mcu.evt.abort_event_delay)
    {
        TimeCalcEvt(&property.ref.abort, dpcom->mcu.evt.abort_event_delay);        
        ParsAbortTime();

        dpcom->mcu.evt.abort_event_delay = 0;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TimeRunning(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks if a reference function should start running or should be aborted.
  This function is called on every iteration period in ISR context, in both cycling and non-cycling modes.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 61)

    switch (ref.func_type)
    {
        case FGC_REF_NONE:          // Function is disarmed

            ref.time_running.s                = 0.0;
            ref.time_running.period_counter   = 0;
            ref.time_remaining.s              = 0.0;
            ref.time_remaining.period_counter = 0;

            if (state_pc == FGC_PC_IDLE)
            {
                if (dpcls->mcu.ref.func.reg_mode[0] != FGC_REG_V)   // I or B
                {
                    if (reg.active->state != dpcls->mcu.ref.func.reg_mode[0])
                    {
                        RegStateRequest(FALSE, TRUE, dpcls->mcu.ref.func.reg_mode[0], ref.v.value);
                    }
                }
                else                                                // V
                {
                    if (reg.active->state != FGC_REG_V)
                    {
                        RegStateV(TRUE, ref.v_fltr);
                    }
                }
            }

            break;

        case FGC_REF_ARMED:         // Function is armed

            //
            // REF.RUN
            //
            // If RUN time has arrived then start running function

            if (AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.run))
            {
                ref.time.s              = 0.0;
                ref.time.period_counter = 0;
                ref.func_type           = dpcls->dsp.ref.func.type[0];
                property.ref.run        = rtcom.time_now_us;
                ref.reg_bypass_mask     = DEFAULT_REF_REG_BYPASS;
                dpcls->dsp.ref.start_f  = TRUE;
            }

            //
            // REF.ABORT
            //
            // else if abort time has arrived disarm reference

            else if (AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.abort))
            {
                ref.func_type = FGC_REF_NONE;
            }

            break;

        default:                    // Function is running

            // If NOT in CYCLING or TO_CYCLING state

            if (state_pc != FGC_PC_TO_CYCLING && state_pc != FGC_PC_CYCLING)
            {
                // Update reference time and time running and remaining (for MCU)
                // To improve the accuracy on floating point values for the ref time, this function increments
                // a counter (ref.time.period_counter) and multiplies it by the V regulation period.
                // The same concept is used in cycling with cyc.time: see comments in function TimeCycling().

                ref.time.period_counter++;

#ifdef __TMS320C32__
                ref.time.s = (FP32)ref.time.period_counter * property.reg.v.rst_pars.period;
#else
                ref.time.s = (FP64)ref.time.period_counter * property.reg.v.rst_pars.period;
                ref.func_time = ref.time.s;
#endif

                // The same idea as for ref.time is used for ref.time_running and ref.time_remaining

                ref.time_running.period_counter++;

#ifdef __TMS320C32__
                ref.time_running.s = (FP32)ref.time_running.period_counter * property.reg.v.rst_pars.period;
#else
                ref.time_running.s = (FP64)ref.time_running.period_counter * property.reg.v.rst_pars.period;
#endif

                if (ref.time_remaining.period_counter)
                {
                    ref.time_remaining.period_counter--;
#ifdef __TMS320C32__
                    ref.time_remaining.s = (FP32)ref.time_remaining.period_counter * property.reg.v.rst_pars.period;
#else
                    ref.time_remaining.s = (FP64)ref.time_remaining.period_counter * property.reg.v.rst_pars.period;
#endif
                }
                else
                {
                    ref.time_remaining.s = 0.0;
                }
            }

            break;
    }

    //
    // REF.ABORT
    //
    // If function is abortable, abort time has arrived, and we are not in cycling then start abort

    if (AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.abort))
    {
        if (TestRefAbortable() && state_pc != FGC_PC_TO_CYCLING && state_pc != FGC_PC_CYCLING)
        {
            // RefToAborting() will reset the mirror value ref.abort, but
            refto.flags.aborting = TRUE;
            // not the property REF.ABORT
        }
        else
        {
            // Cancel the REF.ABORT because the present reference cannot be aborted.
            // The REF.ABORT property will read zero, indicating that no abort took place.

            property.ref.abort.unix_time  = 0;                  // Reset property REF.ABORT and its mirror value ref.abort
            property.ref.abort.us_time    = 0;
            ref.abort.unix_time           = TIME_NEVER;
        }
    }

#endif

    // In low-speed regulation (regulation period > 10 ms), align regulation on 12s UTC boundaries

    if (reg.hi_speed_f == FALSE)
    {
        if (rtcom.start_of_second_f && rtcom.time_now_ms.unix_time % 12 == 0)   // If 12s boundary
        {
            // Then this iteration is a regulation one
            reg.period_counter = reg.active->rst_pars.period_iters - 1;
        }
    }

    // Set the run_f flag which defines if this iteration is a regulation one or not

    reg.period_counter++;

    // period_iters NOT being a constant, the "greater than" comparison is required.
    if (reg.period_counter >= reg.active->rst_pars.period_iters)
    {
        reg.period_counter   = 0;               // Reset period_counter
        dpcls->dsp.reg.run_f = TRUE;
    }
    else
    {
        dpcls->dsp.reg.run_f = FALSE;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TimeCyclingEvents(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to process cycling related timing events.
  This function is called on every iteration period in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Next cycle user event received

    if (!cyc.user_evt_f && dpcom->mcu.evt.next_user_delay_ms)
    {
        cyc.user_evt_f = TRUE;
        TimeCalcEvt(&cyc.start_time_next, dpcom->mcu.evt.next_user_delay_ms);
    }

    if (cyc.user_evt_f
        && (cyc.next_cycle_state == NEXT_CYCLE_WAIT_USER_EVENT)
       )
    {
        if ((state_pc == FGC_PC_TO_CYCLING) || (state_pc == FGC_PC_CYCLING))
        {
            // Trigger the BGP task to prepare the next reference function

            cyc.next_cycle_state = NEXT_CYCLE_PREPARE_REF;
        }
        else
        {
            // we go to READY to avoid warning and other processing while not cycling, like when OFF

            cyc.next_cycle_state = NEXT_CYCLE_READY;

            // Yet we make sure that the DSP knows the current user (cyc.timing, cyc.user will be updated)

            cyc.next_user = dpcom->mcu.evt.next_user;
        }
    }

    // Start super-cycle event received (on ms 20 of first cycle in the super-cycle)

    if (!cyc.ssc_evt_f && dpcom->mcu.evt.ssc_f)
    {
        cyc.ssc_evt_f = TRUE;

        CycStartSuperCycle();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TimeCycling(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called on every iteration period in ISR context, to manage the cycle time.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 61)
    static BOOLEAN timing_signal_detected = FALSE;
#endif

    INT32U    start_new_cycle_f = FALSE;

    cyc.time.period_counter += 1;      // Count the number of V regulation periods

    // Compute the current cycle time using a multiplication. This is more accurate than the alternative
    // method using a summation (cyc.time.s += property.reg.v.rst_pars.period;). Some empirical data for future reference:
    //
    // cyc.time.period_counter = 3599   per     (End of a 3.6 s cycle; 1 per = 1 ms)
    // cyc.time.s              = 3.599000 s     (Using a multiplication)
    // cyc.time.s              = 3.598800 s     (Using a summation)
    // delta                   = 0.000200 s

#ifdef __TMS320C32__
    cyc.time.s        = (FP32)cyc.time.period_counter * property.reg.v.rst_pars.period;
#else
    cyc.time.s        = (FP64)cyc.time.period_counter * property.reg.v.rst_pars.period;
#endif

    // Check for start of cycle

    if (AbsTimeUs_IsGreaterThan(rtcom.time_now_us, cyc.start_time_next))
    {
        cyc.start_time_next.unix_time = TIME_NEVER;             // Don't come back here twice during the same cycle

        // Trigger next cycle

        start_new_cycle_f = TRUE;
    }

    cyc.start_cycle_f = false;

    if (start_new_cycle_f)
    {
#if (FGC_CLASS_ID == 61)
        timing_signal_detected = FALSE;
#endif
        cyc.start_cycle_f = true;

        ref.func_time = 0.0;

        // Properly end the current cycle

        cyc.time.s              = 0.0;
        cyc.time.period_counter = 0;

        LogSignalsOasisEndCycle(cyc.user, &log_oasis_ctrl, &cyc.start_time);

        // Perform parameters switches for the new cycle

        if (cyc.next_cycle_state == NEXT_CYCLE_READY)
        {
            IsrSwitchParameters(ISR_SWITCH_START_OF_CYCLE);         // Switch REF and REG parameter sets
            cyc.next_cycle_state = NEXT_CYCLE_WAIT_USER_EVENT;      // Reset next_cycle_state for next cycle
        }

#if (FGC_CLASS_ID == 61)
        else
        {
            // Cycle check - BGP_OVERRUN : BGP processing for the new cycle has not completed before C0

            CycSaveCheckWarning(DONT_TRIGG_LOG, FGC_CYC_WRN_BGP_OVERRUN, FGC_WRN_CYCLE_START,
                                (FP32)cyc.next_cycle_state,     // REF.CYC.WARNING.DATA[0]
                                (FP32)isr_switch_request_flags, // REF.CYC.WARNING.DATA[1]
                                (FP32)isr_switch_cycling_flags, // REF.CYC.WARNING.DATA[2]
                                0.0);                           // REF.CYC.WARNING.DATA[3]

            CycCancelNextUser();                                    // Clean whatever was already programmed for the next user


            RegStateV(FALSE, 0.0);
            ref.active->func_type = FGC_REF_NONE;
            ref.active->reg_mode  = FGC_REG_V;

        }

        // Send warning or fault to the MCU for the LOG.TIMING property

        CycSendMCUCheckResults();

        // Cycle Check - CYC_WARN_LIM : Too many consecutive warnings

        if (cyc.consecutive_wrn_cnt >= MAX_CONSECUTIVE_WRN)
        {
            CycSaveCheckFault(FGC_CYC_FLT_CYC_WARN_LIM, FGC_WRN_CYCLE_START,
                              (FP32)MAX_CONSECUTIVE_WRN,                         // REF.CYC.FAILED.DATA[0]
                              (FP32)cyc.consecutive_wrn_cnt,                     // REF.CYC.FAILED.DATA[1]
                              0.0,                                              // REF.CYC.FAILED.DATA[2]
                              0.0);                                             // REF.CYC.FAILED.DATA[3]

            // Schedule a cycle for user 0 with length 2 BP (this will be ref NONE with reg state = V)

            cyc.next_user = dpcom->mcu.evt.next_user = 0;
        }

#endif
        // Start the new cycle

        CycStartCycle();
    }
#if (FGC_CLASS_ID == 61)
    // When cycling, drive the reference time from the cycle time

    if (ref.cycling_f)
    {
        ref.time.s = cyc.time.s + ref.advance;

        if (   dpcls->mcu.ref.event_cyc >= FGC_EVENT_CYC_SUP_B0
            && dpcls->mcu.ref.event_cyc <= FGC_EVENT_CYC_SUP_B4)
        {
            // Function is synchronous to an external timing signal
            // connected to a supplemental digital input on bank B

            if (timing_signal_detected)
            {
                ref.func_time += property.reg.v.rst_pars.period;
            }
            else
            {
                dig_sup_channel_e channel = (dig_sup_channel_e)(DIG_SUP_B_CHANNEL0 << (dpcls->mcu.ref.event_cyc - FGC_EVENT_CYC_SUP_B0));

                if (digSupGetStatus(channel) == DIG_SUP_HIGH)
                {
                    ref.func_time = ref.advance;

                    timing_signal_detected = TRUE;
                }
            }
        }
        else
        {
            // Function is synchronous to the start of cycle C0

            ref.func_time = ref.time.s;
        }
    }
#endif
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: time.c
\*---------------------------------------------------------------------------------------------------------*/
