/*!
 *  @file      log_signals.c
 *  @brief     File providing functionality for logging signals
 */

#define LOG_SIGNALS_GLOBALS
#define LOG_SIGNALS_GLOBALS_FAR

// Includes

#include <log_signals.h>
#include <log_signals_class.h>
#include <dpcls.h>
#include <rtcom.h>
#include <ref.h>
#include <reg.h>
#include <cyc.h>
#include <bgp.h>

// External function definitions

void LogSignalCaptureStart(void)
{
    // Freeze log if triggered

    if (dpcls->dsp.log.capture.state == FGC_LOG_RUNNING &&
        log_capture.freeze_f)
    {
        dpcls->dsp.log.capture.state     = FGC_LOG_FROZEN;
        dpcls->dsp.log.capture.n_samples = log_capture.n_samples_recorded;

        // Notify properties LOG.CAPTURE.SIGNALS.*

        dpcls->dsp.notify.log_capture_user = dpcls->dsp.log.capture.user;
    }

    // If ARMED or RUNNING...

    if (dpcls->dsp.log.capture.state != FGC_LOG_FROZEN)
    {
        // Prepare to start capturing spy data

        log_capture.freeze_f                 = FALSE;
        log_capture.n_samples_recorded       = 0;
        dpcls->dsp.log.capture.n_samples = 0;
        dpcls->dsp.log.capture.state     = FGC_LOG_RUNNING;
        dpcls->dsp.log.capture.reg_state = reg.active->state;

        // Inform MCU of time of first sample

        dpcls->dsp.log.capture.first_sample_time = cyc.start_time;
    }
}

void LogSignalCaptureLog(void)
{
    switch (dpcls->dsp.log.capture.state)
    {
        case FGC_LOG_ARMED:
        {
            dpcls->dsp.log.capture.user = -1;

            // State switch from ARMED to RUNNING is done at the beginning of a
            // cycle in CycCaptureLogStart

            break;
        }

        case FGC_LOG_RUNNING:
        {
            // Check for request to capture log for a specified USER (or next
            // cycle if capture.user == 0)

            if (dpcls->dsp.log.capture.user != -1)
            {
                if (!log_capture.freeze_f                             &&
                    (dpcls->dsp.log.capture.user == PPM_ALL_USERS ||
                     dpcls->dsp.log.capture.user == cyc.user))
                {
                    log_capture.freeze_f = TRUE;
                }
            }
            else
            {
                // In any case, if the capture.user is set to -1, reset log_capture.freeze_f

                log_capture.freeze_f = FALSE;
            }

            // Maximum log size is around 6 basic periods

            if (log_capture.n_samples_recorded < FGC_LOG_CAPTURE_LEN)
            {
                // Record sample

                LogSignalClassCaptureLog(log_capture.n_samples_recorded++);

                // Time of last sample

                dpcls->dsp.log.capture.last_sample_time = rtcom.time_now_us;
            }

            break;
        }

        case FGC_LOG_FROZEN:
        {
            // If capture.user is set to -1, switch STATE from FROZEN to ARMED

            if (dpcls->dsp.log.capture.user == -1)
            {
                dpcls->dsp.log.capture.state = FGC_LOG_ARMED;
            }

            // TIMEOUT. If frozen, we can still initiate a new log if the two following conditions are met:
            //   (1) The reason for the freeze is not a cycle check warning (and so, cyc_chk_time == 0)
            //   (2) The capture log timeout has expired.

            if (!dpcls->dsp.log.capture.cyc_chk_time.unix_time &&
                (rtcom.time_now_us.unix_time - dpcls->dsp.log.capture.first_sample_time.unix_time) > LOG_CAPTURE_TIMEOUT)
            {
                dpcls->dsp.log.capture.state = FGC_LOG_ARMED;
                dpcls->dsp.log.capture.user  = -1;
            }

            break;
        }
    }
}

void LogSignalsLog(log_signals_ctrl_t * log_signals_ctrl)
{
    INT16U sig_idx;

    // Copy each signal in the corresponding log buffer

    for (sig_idx = 0; sig_idx < log_signals_ctrl->nb_signals; sig_idx++)
    {
        log_signals_ctrl->log_buf_ptr[sig_idx][log_signals_ctrl->write_idx.index] =
            *log_signals_ctrl->signal_src_ptr[sig_idx];
    }

    // Increment the write index of a circular buffer

    (log_signals_ctrl->write_idx.index)++;

    if (log_signals_ctrl->write_idx.index >= log_signals_ctrl->max_index)
    {
        // Rollover of the index, change the index color

        log_signals_ctrl->write_idx.index = 0;

        // Swap between LOG_ODD and LOG_EVEN
        log_signals_ctrl->write_idx.color = (log_signals_ctrl->write_idx.color == LOG_ODD ? LOG_EVEN : LOG_ODD);
    }
}

void LogSignalsOasisLog(log_signals_ctrl_t * log_signals_ctrl)
{
    if (log_oasis.subsampling.cycle_value <= 1 ||
        log_oasis.subsampling.counter >= log_oasis.subsampling.cycle_value - 1)
    {
        LogSignalsLog(&log_oasis_ctrl);
        log_oasis.subsampling.counter = 0;
    }
    else
    {
        log_oasis.subsampling.counter++;
    }
}

void LogSignalsOasisStartCycle(log_signals_ctrl_t * log_signals_ctrl)

{
    // Memorise the index of C0 (struct copy)

    log_signals_ctrl->last_start_idx = log_signals_ctrl->write_idx;

    // Copy subsampling period from property LOG.OASIS.SUBSAMPLE

    log_oasis.subsampling.cycle_value = log_oasis.subsampling.prop_value;

    // Initialise the subsampling counter so that the first iteration of the cycle is logged.

    log_oasis.subsampling.counter = log_oasis.subsampling.cycle_value - 1;

    // LOG.OASIS interval_ns for this cycle
#if (FGC_CLASS_ID == 62)
    log_oasis.interval_ns.cycle_value = log_oasis.subsampling.cycle_value * 100000;
#else
    log_oasis.interval_ns.cycle_value = log_oasis.subsampling.cycle_value * reg.iteration_period_ns;
#endif
}

void LogSignalsOasisEndCycle(INT16U user, log_signals_ctrl_t * log_signals_ctrl,
                             struct abs_time_us * cycle_timestamp_ptr)
{
    log_signal_index_t start_idx = log_signals_ctrl->last_start_idx;
    log_signal_index_t end_idx   = log_signals_ctrl->write_idx;

    // If rollover of the circular buffer

    if (start_idx.color != end_idx.color)
    {
        end_idx.index += log_signals_ctrl->max_index;
    }


#if (FGC_CLASS_ID == 62)

    // This ugly, ugly, ugly hack fixes the problem with the 1ms offset of data. 
    // It will disapear when refactoring FGC_62 and using liblog

    start_idx.index += 10;
    end_idx.index   += 10;

    if (start_idx.index > log_signals_ctrl->max_index) { start_idx.index -= log_signals_ctrl->max_index; }
    if (end_idx.index   > log_signals_ctrl->max_index) { end_idx.index   -= log_signals_ctrl->max_index; }

#endif

    int32_t size = end_idx.index - start_idx.index;

    if (size < 0) 
    {
        size += log_signals_ctrl->max_index;
    }

    log_oasis.local.start_idx[user] = start_idx;
    log_oasis.local.timestamp[user] = *cycle_timestamp_ptr;
    log_oasis.local.size     [user] = size;
    
    // Notify the BGP task of the new log buffer.

    bgp.trigger.log_oasis_latest_user = user;
}

void LogSignalOasisReset(log_signals_ctrl_t * log_signals_ctrl)
{
    INT32U user;

    for (user = 0 ; user <= FGC_MAX_USER ; user++)
    {
        // Reset the log segment in DSP local memory

        log_oasis.local.timestamp[user].unix_time = 0;
        log_oasis.local.timestamp[user].us_time   = 0;
        log_oasis.local.size     [user]           = 0;
        log_oasis.local.start_idx[user].color     = LOG_ODD;
        log_oasis.local.start_idx[user].index     = 0;

        // Reset its copy in shared memory

        sm_lock_write_take(&dpcls->dsp.log.oasis.lock);

        dpcls->dsp.log.oasis.timestamp[user].unix_time = 0;
        dpcls->dsp.log.oasis.timestamp[user].us_time   = 0;
        dpcls->dsp.log.oasis.size     [user]           = 0;
        dpcls->dsp.log.oasis.start_idx[user]           = 0;

        sm_lock_write_release(&dpcls->dsp.log.oasis.lock);
    }
}

// EOF
