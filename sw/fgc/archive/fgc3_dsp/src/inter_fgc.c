/*!
 *  @file      inter_fgc.c
 *  @defgroup  FGC3:DSP
 *  @brief     Inter FGC communication.
 */

#define INTER_FGC_GLOBALS

// Includes

#include <inter_fgc.h>
#include <dpcls.h>
#include <adc.h>
#include <meas.h>

// External function definitions

void InterFgcSignals(void)
{
    //if (dpcls->mcu.inter_fgc.role == FGC_ROLE_REMOTE_MEAS)
    //{
    //    if (adc_current_a != NULL)
    //    {
    //        dpcls->mcu.inter_fgc.fgc_ether_meas.internal[0].meas         = adc_current_a->meas.current.i_dcct;
    //        dpcls->mcu.inter_fgc.fgc_ether_meas.internal[0].filter_20_ms = adc_current_a->ms20.filtered.current.i_dcct;
    //    }
    //
    //    if (adc_current_b != NULL)
    //    {
    //        dpcls->mcu.inter_fgc.fgc_ether_meas.internal[1].meas         = adc_current_b->meas.current.i_dcct;
    //        dpcls->mcu.inter_fgc.fgc_ether_meas.internal[1].filter_20_ms = adc_current_b->ms20.filtered.current.i_dcct;
    //    }
    //}
    //else if (dpcls->mcu.inter_fgc.use_meas != 0)
    //{
    //    if (adc_current_a != NULL)
    //    {
    //        adc_current_a->meas.current.i_dcct          = dpcls->mcu.inter_fgc.fgc_ether_meas.internal[0].meas;
    //        adc_current_a->ms20.filtered.current.i_dcct = dpcls->mcu.inter_fgc.fgc_ether_meas.internal[0].filter_20_ms;
    //    }
    //
    //    if (adc_current_b != NULL)
    //    {
    //        adc_current_b->meas.current.i_dcct          = dpcls->mcu.inter_fgc.fgc_ether_meas.internal[1].meas;
    //        adc_current_b->ms20.filtered.current.i_dcct = dpcls->mcu.inter_fgc.fgc_ether_meas.internal[1].filter_20_ms;
    //    }
    //}
}

// EOF
