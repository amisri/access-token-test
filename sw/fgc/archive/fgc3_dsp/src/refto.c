/*---------------------------------------------------------------------------------------------------------*\
  File:         refto.c

  Purpose:      FGC DSP Software - change the reference to a new function

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#include <refto.h>
#include <cc_types.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dsp_dependent.h>      // for ToIEEE(), FromIEEE()
#include <rtcom.h>              // for rtcom global variable
#include <reg.h>                // for reg, load, load_select global variables
#include <defprops_dsp_fgc.h>   // for PROP_REF_TFA_FREQUENCIES
#include <main.h>               // for state_pc global variable
#include <meas.h>               // for meas global variable
#include <cyc.h>                // for cyc global variable
#include <ref.h>                // for TestRefAbortable()
#include <macros.h>             // for Test()
#include <bgp.h>                // for BgpInitNone()
#include <ppm.h>                // for ppm[]
#include <mcu_dsp_common.h>     // for TWO_PI
#include <deftypes.h>           // for MIN/MAX
#include <unipolar_switch.h>

/*---------------------------------------------------------------------------------------------------------*/
void RefToStandbyToOpenloop(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is only called from IsrRegStateI() during a TO_STANDBY reference on a unipolar converter,
  in the event that Vref < 0.  It will switch the function from PLEP to OPENLOOP.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32        didt_final;             // Estimated dI/dT (A/s) at final current (I_MIN)

    // Factor 2 for MQM cases
    didt_final = 2.0 * property.limits.op.i.min * load.active->pars.ohms * load.active->pars.inv_henrys;

    if ((ref.fg.value - property.limits.op.i.min) > didt_final) // If change is sufficient (stop in ~2s)
    {
        ref.func_type                 = FGC_REF_OPENLOOP;
        ref.v_openloop                = 0.0;
        ref_fg.openloop.v_ref         = property.limits.op.v.neg;
        ref_fg.openloop.i_closeloop   = property.limits.op.i.min + didt_final;
        ref_fg.openloop.final         = property.limits.op.i.min;
        ref_fg.openloop.d_v_ref       = 0.0;
        ref_fg.openloop.run_delay     = 0.0;
        ref_fg.openloop.pos_ramp_flag = FALSE;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RefToEnd(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to switch to the End reference function.
   - On Class 53 (POPS): This will ramp down the current as fast as possible in open-loop.
   - On Class 61 (PSB):  This will regulate the reference B, I, V to Bmin, Imin, 0V respectively.

  Do not call this function in BGP context. Instead use the flag refto.flags.end to trigger it on the next
  regulation interrupt.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Clear all refto.flags

    refto.flags.standby       = FALSE;
    refto.flags.stopping      = FALSE;
    refto.flags.aborting      = FALSE;
    refto.flags.end           = FALSE;


#if (FGC_CLASS_ID == 61 || FGC_CLASS_ID == 62)
    // Class 61 (PSB):  regulate the reference B, I, V to Bmin, Imin, 0V respectively.

    switch (reg.active->state)
    {
        case FGC_REG_V:
            ref.fg.value = 0.0;
            break;

        case FGC_REG_I:
            ref.fg.value = property.limits.op.i.min;
            break;

        case FGC_REG_B:
            ref.fg.value = load.active->pars.gauss_per_amp * property.limits.op.i.min;
            break;
    }

    ref.func_type = FGC_REF_NONE;
#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void RefToAborting(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initiates the abort sequence.  It is called from IsrPeriod() if a reference function
  is running. For test functions this function stops the reference immediately, while for normal functions
  it switches to a parabolic roll off of pre-defined duration.
  If the standard roll off will exceed the limit, then the roll off is shortened.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32                ref_limit_min;
    FP32                ref_limit_max;
    FP32                ref_value_delta;
    FP32                ref_delta;                    // Reference delta induced by the aborting function
    FP32                start_rate, start_rate_clipped;
    struct fg_plep_config config;

    // Clear all refto.flags

    refto.flags.standby       = FALSE;
    refto.flags.stopping      = FALSE;
    refto.flags.aborting      = FALSE;
    refto.flags.end           = FALSE;

    // Clear the mirror value of property REF.ABORT to avoid repeating the ABORT.
    // The property REF.ABORT itself does not change, so that the user can still read the time of the last abort.

    ref.abort.unix_time       = TIME_NEVER;

    if (reg.active->state == FGC_REG_V  ||              // If V regulation
        ref.func_type == FGC_REF_STEPS  ||               // or test function is running
        ref.func_type == FGC_REF_SQUARE ||
        ref.func_type == FGC_REF_SINE   ||
        ref.func_type == FGC_REF_COSINE)
    {
        ref.func_type = FGC_REF_NONE;                   // stop immediately
    }
    else                                                // else
    {
        ref.fg.value      = ref.fg.estimated;
        start_rate        = ref.fg.rate;

        if (start_rate > 0.0)                           // If ref is rising
        {
            start_rate_clipped = MIN(start_rate, property.limits.op.i.rate);
            ref_limit_max      = lim_i_ref.max_clip;
            ref_delta          = 0.5 * start_rate_clipped * start_rate_clipped / property.limits.op.i.acceleration;

            if ((ref.fg.value + ref_delta) <= ref_limit_max)
            {
                // Calculate PLEP

                config.final_rate   = 0.0;
                config.final        = ref.fg.value + ref_delta;                                // Final current
                config.linear_rate  = start_rate_clipped;                                // Max linear rate
                config.exp_tc       = 0.0;                                               // Exp time constant (disabled)
                config.acceleration = property.limits.op.i.acceleration;                 // Acceleration/deceleration

                fgPlepCalc(&config, &plep, 0.0, ref.fg.value, start_rate, NULL);

                ref.func_type                     = FGC_REF_ABORTING;           // Run the ABORTING function
                ref.time.s                        = 0.0;                        // Reset ref time
                ref.time.period_counter           = 0;
                ref.time_running.s                = 0.0;
                ref.time_running.period_counter   = 0;
                ref.time_remaining.s              = plep.time[4] - ref.time.s;
                ref.time_remaining.period_counter = DSP_CEIL_TIME_VP(ref.time_remaining.s);
            }
            else
            {
                ref.func_type = FGC_REF_NONE;           // Stop immediately
            }
        }
        else if (start_rate < 0.0)                      // else ref is falling
        {
            start_rate_clipped = MAX(start_rate, -property.limits.op.i.rate);

            ref_limit_min = (lim_i_ref.flags.unipolar ? property.limits.op.i.min : property.limits.op.i.neg);
            ref_delta     = 0.5 * start_rate_clipped * start_rate_clipped / property.limits.op.i.acceleration;
            ref_value_delta = (ref.fg.value - ref_delta);

            UniSwitchFloat(&ref_value_delta);

            if (ref_value_delta >= ref_limit_min)
            {
                // Calculate PLEP

                config.final_rate   = 0.0;
                config.final        = ref.fg.value - ref_delta;                                // Final current
                config.linear_rate  = -start_rate_clipped;                               // Max linear rate (absolute value)
                config.exp_tc       = 0.0;                                               // Exp time constant (disabled)
                config.acceleration = property.limits.op.i.acceleration;                 // Acceleration/deceleration

                fgPlepCalc(&config, &plep, 0.0, ref.fg.value, start_rate, NULL);

                ref.func_type                     = FGC_REF_ABORTING;           // Run the ABORTING function
                ref.time.s                        = 0.0;                        // Reset ref time
                ref.time.period_counter           = 0;
                ref.time_running.s                = 0.0;
                ref.time_running.period_counter   = 0;
                ref.time_remaining.s              = plep.time[4] - ref.time.s;
                ref.time_remaining.period_counter = DSP_CEIL_TIME_VP(ref.time_remaining.s);
            }
            else
            {
                ref.func_type = FGC_REF_NONE;                                   // Stop immediately
            }
        }
        else
        {
            ref.func_type = FGC_REF_NONE;                                   // Stop immediately
        }
    }

    if (ref.func_type == FGC_REF_NONE)
    {
        ref.time_remaining.s              = 0.0;                            // Adjust time remaining
        ref.time_remaining.period_counter = 0;
        config.final                      = ref.fg.value;
    }

    // Update the RTD and meta data

    ppm[0].armed.meta.duration    = ref.time_remaining.s;
    ppm[0].armed.meta.range.start =        ref.fg.value;
    dpcls->dsp.ref.start          = ToIEEE(ref.fg.value);
    ppm[0].armed.meta.range.end   =        config.final;
    dpcls->dsp.ref.end            = ToIEEE(config.final);
    ppm[0].armed.meta.range.min   = MIN(ref.fg.value, config.final);
    ppm[0].armed.meta.range.max   = MAX(ref.fg.value, config.final);
}
/*---------------------------------------------------------------------------------------------------------*/
void RefToStandby(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is only called in IREF mode during startup or if a SLOW_ABORT or S PC SB have occurred. It
  calculates the PLEP parameters to take the reference to the standby level and immediately starts the PLEP.

  Important note: During a ramp down of the current on a 1Q converter, if the reference voltage needs to be
                  negative, then the regulation algorithm will automatically switch to an OPENLOOP reference
                  to end the ramp.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32  start_acc;
    FP32  start_rate;
    struct fg_plep_config config;

    // Clear all refto.flags, except the stopping one since a TO_STANDBY ref can be followed by a STOPPING reference

    refto.flags.standby       = FALSE;
    refto.flags.aborting      = FALSE;
    refto.flags.end           = FALSE;

    // Estimated reference value on the next regulation iteration

    ref.fg.value = ref.fg.estimated;

    // Prepare PLEP parameters

    // Caution: If the FGC is configured in such a way that REF.RATE.I.TO_STANDBY[load] > LIMITS.I.RATE[load], then
    //          the I reference will be clipped by the regulation function

    config.final_rate   = 0.0;
    config.final        = property.limits.op.i.min;                             // Final current
    config.linear_rate  = property.ref.rate.i.to_standby[load.active->select];  // Max linear rate
    config.exp_tc       = 0.0;                                  // Exp time constant (disabled)
    config.acceleration = FromIEEE(
                              dpcls->mcu.ref.defaults.i.acceleration[load.active->select]);    // Acceleration/deceleration
    start_rate          = ref.fg.rate;                          // Set initial rate to the reference rate

    UniSwitchFloat(&config.final);

    // Special unipolar converter behaviour

    if (lim_i_ref.flags.unipolar)
    {
        if (ref.func_type == FGC_REF_STARTING)                          // If starting
        {
            if (fabs(ref.fg.value) < property.limits.op.i.min)                // If below I_MIN
            {
                config.linear_rate = ref_fg.starting.i_rate;

                if (fabs(start_rate) > 0.0)                                   // If current is climbing
                {
                    start_acc = 0.5 * start_rate * start_rate / (config.final - ref.fg.value);

                    if (fabs(start_acc) > config.acceleration)                // Avoid overshoot
                    {
                        config.acceleration = start_acc;
                    }
                }
            }
            else                                        // else already above I_MIN
            {
                if (start_rate > 0.0)                   // If current is climbing
                {
                    start_rate = 0.0;                   // Halt climb
                }
            }
        }
    }

    // Calculate PLEP parameters

    fgPlepCalc(&config, &plep, 0.0, ref.fg.value, start_rate, NULL);

    // Prepare to run PLEP immediately

    ref.func_type                     = FGC_REF_TO_STANDBY;
    ref.time.s                        = 0.0;                        // Reset ref time
    ref.time.period_counter           = 0;
    ref.time_running.s                = 0.0;
    ref.time_running.period_counter   = 0;
    ref.time_remaining.s              = plep.time[4] - ref.time.s;
    ref.time_remaining.period_counter = DSP_CEIL_TIME_VP(ref.time_remaining.s);

    // Update the RTD and meta data

    ppm[0].armed.meta.duration    = ref.time_remaining.s;
    ppm[0].armed.meta.range.start =        ref.fg.value;
    dpcls->dsp.ref.start          = ToIEEE(ref.fg.value);
    ppm[0].armed.meta.range.end   =        property.limits.op.i.min;
    dpcls->dsp.ref.end            = ToIEEE(property.limits.op.i.min);
    ppm[0].armed.meta.range.min   = MIN(ref.fg.value, property.limits.op.i.min);
    ppm[0].armed.meta.range.max   = MAX(ref.fg.value, property.limits.op.i.min);
}
/*---------------------------------------------------------------------------------------------------------*/
void RefToStopping(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is only called in IREF mode for a 2Q converter if the user does a S PC OF from ON_STANDBY or
  TO_STANDBY, or if a SLOW_ABORT has reached ON_STANDBY. It prepares the openloop parameters for an openloop
  smooth stopping using the STOPPING reference function.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Clear all refto.flags

    refto.flags.standby       = FALSE;
    refto.flags.stopping      = FALSE;
    refto.flags.aborting      = FALSE;
    refto.flags.end           = FALSE;

    // Prepare the STOPPING reference function

    ref.v_openloop                    = ref.v.value;                    // Initialise Vref to current value
    ref.func_type                     = FGC_REF_STOPPING;
    ref.time.s                        = 0.0;                            // Reset ref time
    ref.time.period_counter           = 0;
    ref.time_running.s                = 0.0;
    ref.time_running.period_counter   = 0;
    ref.time_remaining.s              = 0.0;
    ref.time_remaining.period_counter = 0;

    // Keep the measured initial Irate in memory for debugging
    ref_fg.stopping.i_rate_init_meas  = meas.hist->rate;

    // Clip the measured value to stay in a Irate domain limited by property LIMITS.I.RATE

    ref_fg.stopping.i_rate_init       = ref_fg.stopping.i_rate_init_meas;
    ref_fg.stopping.i_rate_init       = MIN(ref_fg.stopping.i_rate_init, property.limits.op.i.rate);
    ref_fg.stopping.i_rate_init       = MAX(-property.limits.op.i.rate, ref_fg.stopping.i_rate_init);

    ref_fg.stopping.i_rate            = ref_fg.stopping.i_rate_init;        // Initialise Irate to current value
    ref_fg.stopping.i_rate_min =
        -property.ref.rate.i.stopping[load.active->select]; // Target Irate for the STOPPING function
    ref_fg.stopping.i_rate_step       = -property.limits.op.i.acceleration * reg.active->rst_pars.period;

    ppm[0].armed.meta.range.max       = ppm[0].armed.meta.range.start = ref.i.value;
    ppm[0].armed.meta.range.min       = ppm[0].armed.meta.range.end   = limits.i_start;
    dpcls->dsp.ref.start              = ToIEEE(ref.i.value);
    dpcls->dsp.ref.end                = ToIEEE(limits.i_start);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: refto.c
\*---------------------------------------------------------------------------------------------------------*/

