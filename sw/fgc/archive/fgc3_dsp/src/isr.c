/*!
 *  @file      isr.c
 *  @defgroup  FGC3:DSP
 *  @brief     FGC3 DSP interrupt declaration
 */

#define ISR_GLOBALS
#define BGCOM_GLOBALS

// Includes

#include <isr.h>
#include <adc.h>
#include <ana.h>
#include <bgcom.h>
#include <bgp.h>
#include <crate.h>
#include <cyc.h>
#include <defprops_dsp_fgc.h>
#include <deftypes.h>
#include <dpcls.h>
#include <dpcom.h>
#include <dsp_dependent.h>
#include <dsp_panic.h>
#include <inter_fgc.h>
#include <log_signals_class.h>
#include <log_signals.h>
#include <meas.h>
#include <props.h>
#include <report.h>
#include <rtcom.h>
#include <spivs.h>
#include <time_fgc.h>

#if FGC_CLASS_ID == 61
#include <ana.h>
#endif

// Constants

// Isr duration histogram. Times are expressed in us.

#define HIST_RANGE_MIN  (48)            // Must be a multiple of 4.
#define HIST_RANGE_MAX  (144)           // Same.

// Internal variable definitions

static INT32U free_running_hist[(HIST_RANGE_MAX >> 2) - (HIST_RANGE_MIN >> 2)];
static INT32U free_running_max = 0;

// External function definitions

void IsrPeriod(void)
{
    INT32U  free_running_idx;

    // Acknowledge interrupt

    ACKNOWLEDGE_NMI;

    SET_TP0;                    // Set DSP Test Point 0

    rtcom.start_freerun_us = TimeGetUs();
    rtcom.dsp_utc_us = TimeGetUtcTimeUs();

    // RTI counter 0 is used to calculate rtcom.cpu_usage (The ISR duration in us)

    *(INT32U *)RTIGCTRL &= 0xFFFFFFFE;  // Disable RTI counter 0
    *(INT32U *)RTIFRC0   = 0;           // Reset   RTI counter 0
    *(INT32U *)RTIGCTRL |= 0x00000001;  // Start   RTI counter 0

    dpcom->dsp.cpu_usage     = rtcom.cpu_usage;                 // Return CPU usage for previous millisecond
    bgcom.run_f              = TRUE;                            // Signal to background proc to run after ISR

    dpcom->dsp.ms_irq_alive_counter.int32u++;                   // Increment watchdog with MCU

    // Main Real-Time Processing function (class dependent)

    if (DSP_INITIALISED)
    {
        IsrMain();
    }

    // Trigger BGP calibration processing, if calibration is active

    if (cal.ongoing_f)
    {
        bgp.trigger.calibration_process = TRUE;
    }

    // Process one cal/state/parameter request (avoiding 10ms boundaries)

    if (!rtcom.start_of_ms10_f)
    {
        /*
                if ( rtcom.time_now_us.unix_time != 0 )         // If unix_time is known
                {
                    InIrqProcessQSPIbusDataSlice();
                }
        */
        // Toggle LED 0 on development front panel (if connected) at about 0.5Hz

        rtcom.led_counter++;

        if (!(rtcom.led_counter & 0x00FF))
        {
            LED0_TOGGLE;
        }
    }

    // Calculate time taken by ISR on this iteration

    rtcom.cpu_usage = (*(INT32U *)RTIFRC0);        // Elapsed time (us)

    free_running_max = MAX(free_running_max, rtcom.cpu_usage);

    free_running_idx = rtcom.cpu_usage >> 2;       // 4 us steps

    free_running_idx = MAX(free_running_idx, (HIST_RANGE_MIN >> 2));
    free_running_idx = MIN(free_running_idx, (HIST_RANGE_MAX >> 2) - 1);

    free_running_hist[free_running_idx - (HIST_RANGE_MIN >> 2)]++;

    dpcom->dsp.cpu_usage_ext_counter = TimeGetUsDiffNow(rtcom.start_freerun_us);

    rtcom.last_start_freerun_us = rtcom.start_freerun_us;
    rtcom.last_dsp_utc_us = rtcom.dsp_utc_us;

    CLR_TP0;                    // Clear DSP Test Point 0
}

void IsrMain(void)
{
    // Process time and timing events

    TimeIteration();

    // Switch the ISR parameter structures that need to be

    IsrSwitchParameters(ISR_SWITCH_REGULAR_ISR);

    if (dpcom->mcu.device_cyc == FGC_CTRL_ENABLED)
    {
        TimeRunning();
        TimeCyclingEvents();
        TimeCycling();
    }
    else
    {
        TimeRunningEvents();
        TimeRunning();
    }

    // Get and process signals from PAL/PBL and ADCs

    AdcRead();
    AdcFilter();

    InterFgcSignals();

    MeasSelect();

    // Calibration samples

    if (cal.ongoing_f && cal.request.state == CAL_REQUEST_ACQUIRE_SAMPLES)
    {
        CalAcqSamples();
    }

    if (rtcom.ms20 == 19)                               // Every ms 19 of 20
    {
        MeasDiag();                                 // Process Diag signals
    }

    // On FGC3, meas.v does not come from the DIAG signal, but from the Analogue card, channel C.

    if (sim.active->enabled)
    {
        meas.v = sim.active->v;
    }
    else
    {
        meas.v = (adc_v_meas_load != NULL ? adc_v_meas_load->meas.voltage.v_meas : 0.0);
        meas.v_capa = (adc_v_meas_capa != NULL ? adc_v_meas_capa->meas.voltage.v_meas : 0.0);
    }

    if (rtcom.state_op == FGC_OP_TEST)
    {
        IsrDacDirect(0, property.ref.dac[0]);        // Set DAC value from REF.DAC property
        IsrDacDirect(1, property.ref.dac[1]);        // Set DAC value from REF.DAC property
        spivsTest();                                 // Test the SPIVS bus
    }

    // Class specific functionality

    if (!cal.ongoing_f)
    {
        IsrClassMain();
    }

    // Logging

    IsrLogging();

    // Write to SPY interface unless STATE.PC == ARMED (to allow synchronisation)

    if (ref.func_type != FGC_REF_ARMED && rtcom.start_of_ms_f)
    {
        MeasSpySend();
    }

    // Simulate Vmeas, Imeas and Bmeas for next iteration

    SimNextRegState(ref.v.value);
}

void IsrDacSet(INT32U dac_idx, FP32 v_dac)
{
    ref.dac_raw[dac_idx] = calDacSet(&cal.active->dac_factors[dac_idx], v_dac);

    if (!cal.ongoing_dac_cal[dac_idx])
    {
        AnaDacSet(dac_idx, ref.dac_raw[dac_idx]);
        property.ref.dac[dac_idx] = ref.dac_raw[dac_idx];
    }
}

void IsrDacDirect(INT32U dac_idx, INT32S ref_dac)
{
    if (ref_dac >= 1000000)
    {
        ref.dac_raw[dac_idx] += (ref_dac - 1000000);

        if (ref.dac_raw[dac_idx] > cal.active->dac_factors[dac_idx].max_dac_raw)
        {
            ref.dac_raw[dac_idx] = cal.active->dac_factors[dac_idx].min_dac_raw;
        }
    }
    else if (ref_dac <= -1000000)
    {
        ref.dac_raw[dac_idx] += (ref_dac + 1000000);

        if (ref.dac_raw[dac_idx] < cal.active->dac_factors[dac_idx].min_dac_raw)
        {
            ref.dac_raw[dac_idx] = cal.active->dac_factors[dac_idx].max_dac_raw;
        }
    }
    else
    {
        ref.dac_raw[dac_idx] = ref_dac;
    }

    AnaDacSet(dac_idx, ref.dac_raw[dac_idx]);
}

void IsrLogging()
{
    FP32 restore_ref_now_val;

    restore_ref_now_val = ref.now->value;

    // First plateau +1G or +1A start marker in ref.now. The original ref.now value is restored at the end of this function

    if (ref.func_type == FGC_REF_PPPL &&
        DSP_ROUND_TIME_MS(cyc.time.s) == DSP_ROUND_TIME_MS(ppm[cyc.ref_user].cycling.first_plateau.time))
    {
        ref.now->value += 1.0;
    }

    // Capture log
    LogSignalCaptureLog();

    // LOG.OASIS log
    LogSignalsOasisLog(&log_oasis_ctrl);

    ref.now->value = restore_ref_now_val;
}

void IsrSwitchParameters(enum isr_switch_type isr_switch_type)
{
    INT32U idx, mask;
    void * temp_ptr;

    for (idx = 0, mask = 1; idx < ISR_SWITCH_MAX_VALUE; idx++, mask <<= 1)
    {
        // If switch requested And cycling check is OK
        if (Test(isr_switch_request_flags, mask) && (!Test(isr_switch_cycling_flags, mask) ||
                                                     isr_switch_type == ISR_SWITCH_START_OF_CYCLE))
        {
            if (isr_switch_ptrs[idx].active == NULL || isr_switch_ptrs[idx].next == NULL)
            {
                DspPanic(DSP_PANIC_ISR_SWITCH);
            }

            // Switch active and next pointers

            temp_ptr                       = *(isr_switch_ptrs[idx].next);
            *(isr_switch_ptrs[idx].next)   = *(isr_switch_ptrs[idx].active);
            *(isr_switch_ptrs[idx].active) = temp_ptr;

            // Additional processing specific to the mask value

            switch (mask)
            {
#if (FGC_CLASS_ID == 61)

                case ISR_SWITCH_REG:  RegStateSwitch();                         break;
#endif

                default:                                                        break;
            }

            // Clear flags

            Clr(isr_switch_request_flags, mask);
            Clr(isr_switch_cycling_flags, mask);
        }
    }
}

void IsrTrap(void)
{
    DspPanic(DSP_PANIC_ISR_TRAP);
}

// EOF
