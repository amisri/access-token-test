/*---------------------------------------------------------------------------------------------------------*\
  File:     lib.c

  Purpose:  FGC DSP library functions - common to all FGC2 classes

  Notes:    Memory Maps for the different classes:

        +---------------------------------------------------------------+
        |           CLASS  51: PC_LHC           |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 00000 | 04000 | 08000 | 0C000 | 10000 | 14000 | 18000 | 1C000 |
        |       |       |               |
        |    DSP Prog   |       |               |
        |     (96KB)    |   Variables   |               |
        |       +-------+    (158KB)    |        DIM LOGS       |
        |   |       +-------|        (256KB)        |
        |   |       | 0FE00 |               |
        |   |       | Stack |               |
        |   |       | (2KB) |               |
        +-------+-------+-------+-------+-------+-------+-------+-------+

        +---------------------------------------------------------------+
        |           CLASS  53: POPS                 |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 00000 | 04000 | 08000 | 0C000 | 10000 | 14000 | 18000 | 1C000 |
        |       |       |               |
        |    DSP Prog   |       |               |
        |     (96KB)    |       |               |
        |       +-------+   Variables   |       CAPTURE LOG     |
        |   |        (160KB)    |        (256KB)        |
        |   |           |               |
        |   |           |               |
        |   |           |               |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 87FE00|
        | Stack |
        | (2KB) |
        +-------+

        +---------------------------------------------------------------+
        |           CLASS  59: RF_FGEN          |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 00000 | 04000 | 08000 | 0C000 | 10000 | 14000 | 18000 | 1C000 |
        |   |   |           |           |
        |   |   |           |           |
        |   |   |           |           |
        |  DSP  | Vars  | Function Time Vectors | Function Value Vectors|
        |  Prog | (64KB)|    (192KB)    |    (192KB)    |
        | (64KB)|   |           |           |
        |   |   |           |           |
        |   |   |           |           |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 87FE00|
        | Stack |
        | (2KB) |
        +-------+

        The 2KB stack for classes 53 and 59 is in internal SRAM at 0x87FE00 as there is not a
                radiation problem for these systems and internal SRAM is faster than external SRAM.
                This is essential for the RF_FGEN class which is at the limit of processing resources.
\*---------------------------------------------------------------------------------------------------------*/

#include <lib.h>
#include <cc_types.h>
#include <string.h>         // ANSI string library
#include <stdlib.h>         // ANSI stdlib library
#include <libfg.h>          // Function generation library
#include <sleep.h>
#include <memmap_dsp.h>     // DSP memory map constants
#include <defconst.h>       // Global FGC DSP constants
#include <mcu_dsp_common.h>
#include <pars.h>
#include <dpcls.h>
#include <property.h>
#include <defprops_dsp_fgc.h>

// needed by fgc_parser_consts.h
struct hash_entry               // Required to satisfy parser_consts.h
{
    INT32U    dummy;
};

#include <fgc_parser_consts.h>  // Global parser constants - contains property flags

#include <dpcom.h>              // Dual port ram structures
#include <bgcom.h>              // for bgcom global variable
#include <debug.h>              // for db global variable
#include <diag.h>               // for
#include <dsp_dependent.h>      // for DISABLE_TICKS, LEDS_INIT, LED0_TOGGLE, LED1_TOGGLE
#include <rtcom.h>              // for rtcom global variable
#include <macros.h>             // for Test()
#include <propcomms.h>          // for PropComms()
#include <hw_tms320c6727.h>     // for EMIF_SDCR
#include <hardware_setup.h>     // for Hw_Init(), Init_Timers(), Init_SPI()
#include <mcu_dsp_common.h>     // for DB_DSP_MEM_LEN

/*---------------------------------------------------------------------------------------------------------*/
void InitDsp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at the start of main.c to set up of the DSP internal registers.
\*---------------------------------------------------------------------------------------------------------*/
{

    // arriving here the first PLD_DPRAM 32b word must be 0x00000002.
    // It corresponding to a DSP status = MAIN RUNNING set by the Boot Loader

    // this is only when we reload with the debugger not passing via the boot
    // so we set the values again

    // I check the content of this register to test if the bootloader has been executed
    // so if this value changes in the boot must be reflected in the DSP program
    // We write 0x720 but read back 0x620, see DOC
    // This is broken, by default we read 0x00000620!
    if (*(volatile INT32U *) EMIF_SDCR != 0x00000621)
    {
        Hw_init();              // This is the same done by the BootLoader (1st 1Kb loaded at boot time)
    }

    Init_Timers();
    Init_SPI();

#if 0
    // Set up Memory Strobes

    BUS_ADDR->strb0_gcontrol  =  EXTERNAL_RDY | DATA_32 | MEMW_32 | STRB_SW_XTRA;   // DSP RAM
    BUS_ADDR->strb1_gcontrol  =  EXTERNAL_RDY | DATA_32 | MEMW_32;          // MCU RAM
    BUS_ADDR->iostrb_gcontrol =  EXTERNAL_RDY;                      // MCU interrupts

    // Set up Timer 0 to time RT processing

    TIMER_ADDR(0)->period       = 0xFFFFFFFF;
    TIMER_ADDR(0)->gcontrol_bit.clksrc  = INTERNAL;

    // Set up serial port to communicate with USB spy interface

    SERIAL_PORT_ADDR(0)->gcontrol =
        FSXOUT   |  // FSXOUT is an output pin
        XCLKSRCE |  // XCLKSRCE is internal transmit clock
        CLKXP    |  // CLKXP(Polarity) is active low (data on neg edge)
        XLEN_32  |  // 32 bits transmitted
        XVAREN;     // Frame synch FSX active when all data transmitted

    SERIAL_PORT_ADDR(0)->s_x_control =      // Serial output port direction control
        CLKXFUNC |  // CLKX0 is serial output port pin
        DXFUNC   |  // DX0 is serial output port pin
        FSXFUNC ;   // FX0 is serial output port pin

    SERIAL_PORT_ADDR(0)->s_rxt_period  = 0x00;  // 8 Mhz clock (4us/long word: this is the maximum possible)

    SERIAL_PORT_ADDR(0)->s_rxt_control =
        XGO      |  // Timer enabled
        XCLKSRC  |  // Internal clock
        XHLD_    |      // Counter enabled
        XCP_;       // Clock mode chosen

    SERIAL_PORT_ADDR(0)->gcontrol |= XRESET;    // Transmit reset - start serial port
#endif
    // Set up control of port that drives the LEDs on the development front panel

    LEDS_INIT;                  // I/O XF0/1 are both outputs to drive front panel LEDs
}
/*---------------------------------------------------------------------------------------------------------*/
void InitLib(INT32U n_props, INT32U max_user, INT32U ppm_start, INT32U ppm_length)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main.c to set up the library variables and to wait for the MCU to start.
  Note that it is the responsibility of the main function to clear the property structures to integer
  zero before calling this function.
  The property structures must contain the property lengths when they
  are indirect (which is always true for PPM properties).
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U    i;
    INT32U    prop_idx;
    INT32U    prop_size;                              // Property length for each user index
    INT32U    user;
    INT32U    value_addr;
    INT32U    n_users;
    INT32U    ppm_step;
    INT32U  *  mem_addr;

    // Initialise common structures

    memset(&rtcom, 0, sizeof(rtcom));               // Clear common RT variables
    memset(&bgcom, 0, sizeof(bgcom));               // Clear common BG variables
    memset(&db,    0, sizeof(db));                  // Initialise debug structure

    rtcom.ppm_start  = ppm_start;                    // Save PPM structure range and length
    rtcom.ppm_length = ppm_length;
    rtcom.ppm_end    = ppm_start + ppm_length;

    // Set floating point properties to TI floating point zero

    for (prop_idx = 0 ; prop_idx < n_props ; prop_idx++)
    {
        if (prop[prop_idx].float_f)
        {
            value_addr = (INT32U)prop[prop_idx].value;
            prop_size  = prop[prop_idx].max_elements * prop[prop_idx].el_size;

            if (!Test(prop[prop_idx].flags, PF_PPM))    // If not PPM property
            {
                n_users = 1;                                         // Slot 0 only
            }
            else                        // else PPM property
            {
                n_users = max_user + 1;                              // Add one for slot 0

                if (value_addr <  rtcom.ppm_start ||                 // if not in PPM zone
                    value_addr >= rtcom.ppm_end)
                {
                    ppm_step = prop_size;                               // PPM array is contiguous
                }
                else                                                // else in PPM zone
                {
                    ppm_step = ppm_length;                              // PPM array is in struct ppm array
                }
            }

            for (user = 0 ; user < n_users ; user++)                 // Initialise property values to FP32 zero
            {
                mem_addr = (INT32U *)(value_addr + ppm_step * user);

                if (value_addr >= DPRAM_DPCOM_32)                    // If property is in DPRAM
                {
                    for (i = 0 ; i < prop_size ;)                    // Avoid memset as it blocks
                    {
                        // access by the MCU to
                        mem_addr[i] = FP32ZERO;                      // its memory
                    }
                }
                else
                {
                    memset(mem_addr, FP32ZERO, prop_size);           // Use memset for DSP memory
                }                                                    // as it is quicker
            }
        }
    }

    // Initialise other variables
    rtcom.interfacetype = dpcom->mcu.interfacetype;     // Get slot5 interface type

    dpcom->mcu.debug_mem_dsp_addr = (INT32U)&db;        // Default debug address is the debug struct

    // Wait for MCU to permit DSP to start

    //-----------------------------------------------------------------------------------
    //*(volatile INT32U *)GPIODAT1 = 0x0003; // TP0 = 1, TP1 = 1
    //-----------------------------------------------------------------------------------

    // ToDo: How to avoid this while when in standalone
    //  if ( !mst.dsp_is_standalone )               // If DSP is not standalone
    {
        //      while commented for stand alone debugging
        while (dpcom->mcu.started != DSP_STARTED)                   // Wait for MCU to start
        {
            ;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DebugMemTransfer(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called repetitively from Bgp() to transfer the debug zone of 32 words to the MCU where it
  is mapped onto the property FGC.DEBUG.MEM.DSP.  The user can define the address of the 32 words to
  copy using the same property.
\*---------------------------------------------------------------------------------------------------------*/
{
    bgcom.debug_idx++;

    if (bgcom.debug_idx >= DB_DSP_MEM_LEN)          // Increment DB index
    {
        bgcom.debug_idx = 0;
    }                           // Get value as FP32 and INT32S

    dpcom->dsp.debug_mem_dsp_i[bgcom.debug_idx] =
        ((INT32U *)dpcom->mcu.debug_mem_dsp_addr)[bgcom.debug_idx];

    bgcom.db_float = ((FP32 *)dpcom->mcu.debug_mem_dsp_addr)[bgcom.debug_idx];

    if (bgcom.db_float > 1.0E+30)       // Clip FP32 at +/- 1E+30 to protect MCU printf
    {
        bgcom.db_float = 1.0E+30;
    }
    else
    {
        if (bgcom.db_float < -1.0E+30)
        {
            bgcom.db_float = -1.0E+30;
        }
    }

    dpcom->dsp.debug_mem_dsp_f[bgcom.debug_idx] = bgcom.db_float; // Translate to IEEE FP32
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN BgpPropComms(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the background processing loop.  It manages property communications with the
  MCU and parameter parsing.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Check for DSP property set/get requests from MCU

    if (dpcom->pcm_prop.action)                         // Publication task action
    {
        PropComms(&dpcom->pcm_prop);
        return FALSE;
    }

    if (dpcom->fcm_prop.action)                         // Fieldbus command task action
    {
        PropComms(&dpcom->fcm_prop);
        return FALSE;
    }

    if (dpcom->tcm_prop.action)                         // Serial command task action
    {
        PropComms(&dpcom->tcm_prop);
        return FALSE;
    }

    // If the polarity switch has changed, parse the limits again.
    if (dpcls->mcu.vs.polarity.changed)
    {
        DISABLE_TICKS;
        UPDATE_PARS(DSP_PARS_Limits);
        ENABLE_TICKS;
        dpcls->mcu.vs.polarity.changed = FALSE;
    }

    if (rtcom.par_group_set_mask)
    {
        ParsGroupSet();
        return FALSE;
    }

    return TRUE;
}
/*---------------------------------------------------------------------------------------------------------*/
void interruptible_memcpy(void * s1, const void * s2, size_t n)
/*---------------------------------------------------------------------------------------------------------*\
    Standard library memcpy is not interruptible on C67x. This can cause problems, with longer than expected
    critical sections in the Background (BGP) task. (The bug was first seen in PropComms function.)
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U * s1_w, *s2_w;
    INT8U * s1_b, *s2_b;

    // If target, source and size are all multiple of 4, do a 32-bit word copy.
    if (!((INT32U)s1 & 3) && !((INT32U)s2 & 3) && !(n & 3))
    {
        s1_w = (INT32U *)s1;
        s2_w = (INT32U *)s2;
        n /= 4;

        while (n--)
        {
            *s1_w++ = *s2_w++;          // Word copy
        }
    }
    // Else, do a byte-copy
    else
    {
        s1_b = (INT8U *)s1;
        s2_b = (INT8U *)s2;

        while (n--)
        {
            *s1_b++ = *s2_b++;          // Byte copy
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: lib.c
\*---------------------------------------------------------------------------------------------------------*/
