/*!
 *  @file  comhv_ps_class_test.h
 *  @brief Provides unit tests for VSNOCABLE input polling function
 */

#ifndef COMHV_PS_CLASS_TEST_H
#define COMHV_PS_CLASS_TEST_H

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>

#define uintptr_t cmocka_uintptr_t
#include <cmocka.h>
#undef  uintptr_t

#define uintptr_t fgc_uintptr_t
#include <comhv_ps_class.c>
#undef  uintptr_t
#define uintptr_t fgc_uintptr_t

BOOLEAN VsnocableInput(void);

#endif // COMHV_PS_CLASS_TEST_H
