#include <string.h>

#include <comhv_ps_class_test.h>

// Constants

/*! VSNOCABLE high pulse duration (in milliseconds) which encodes state 1 - NOEXT = 0, RST = 0, OFF = 0. */
#define MST_VSNOCABLE_NORST_ON  5
/*! VSNOCABLE high pulse duration (in milliseconds) which encodes state 2 - NOEXT = 0, RST = 0, OFF = 1. */
#define MST_VSNOCABLE_NORST_OFF 10
/*! VSNOCABLE high pulse duration (in milliseconds) which encodes state 3 - NOEXT = 0, RST = 1, OFF = 0. */
#define MST_VSNOCABLE_RST_ON    15
/*! VSNOCABLE high pulse duration (in milliseconds) which encodes state 4 - NOEXT = 0, RST = 1, OFF = 1. */
#define MST_VSNOCABLE_RST_OFF   20
/*! VSNOCABLE high pulse duration (in milliseconds) which encodes state 5 - NOEXT = 1, RST = X, OFF = X. */
#define MST_VSNOCABLE_NOEXT     25

#define INITIAL_LOW_STATE_TIME (2  * MST_VSNOCABLE_PWM_PERIOD - 0.6 * MST_VSNOCABLE_PWM_PERIOD)

#define FIRST_CMD  INITIAL_LOW_STATE_TIME + 1 * MST_VSNOCABLE_PWM_PERIOD
#define SECOND_CMD INITIAL_LOW_STATE_TIME + 2 * MST_VSNOCABLE_PWM_PERIOD
#define THIRD_CMD  INITIAL_LOW_STATE_TIME + 3 * MST_VSNOCABLE_PWM_PERIOD
#define FOURTH_CMD INITIAL_LOW_STATE_TIME + 4 * MST_VSNOCABLE_PWM_PERIOD

#define RECOVERY_FIRST_CMD      INITIAL_LOW_STATE_TIME  + 1 * MST_VSNOCABLE_PWM_PERIOD
#define RECOVERY_BUS_ERROR_TIME RECOVERY_FIRST_CMD      + 5.5 * MST_VSNOCABLE_PWM_PERIOD
#define RECOVERY_SECOND_CMD     RECOVERY_BUS_ERROR_TIME + MST_VSNOCABLE_PWM_PERIOD
#define RECOVERY_THIRD_CMD      RECOVERY_SECOND_CMD     + 1 * MST_VSNOCABLE_PWM_PERIOD

// Internal variables

BOOLEAN is_setup;

/*!
 * @brief Simulation time
 */
unsigned t = 0;

// Internal function declarations

static void PrepareExpectedBufferCommand(char const * const command);

/*!
 *  @brief Sets the previous state saved in vs.ext_control
 *
 *  @param[in] state The state number to which vs.ext_control should be set
 */
inline static void SetState(mst_vsnocable_state_t state);

/*!
 *  @brief Checks if the saved state equals the given one
 *
 *  @param[in] state The state number to which vs.ext_control should be equal
 */
inline static void CheckState(mst_vsnocable_state_t state);

/*
 * @brief Prepares the VSNOCABLE input for state resetting function
 *
 * Order of commands:
 *  -# Inital low state
 *  -# State 1: NOEXT = 0, RST = 0, OFF = 0 (1 period)
 *  -# State 1: NOEXT = 0, RST = 0, OFF = 0 (1 period)
 *  -# State 1: NOEXT = 0, RST = 0, OFF = 0 (1 period)
 */
static void ResetPollingFunctionStatePrepareInput(void);

/*!
 * @brief Prepares the VSNOCABLE input for test
 *
 * Order of commands:
 *  -# Inital low state
 *  -# State 4: NOEXT = 0, RST = 1, OFF = 1 (1 period)
 */
static void SimpleStateChangePrepareInput(void);

/*!
 * @brief Prepares the VSNOCABLE input for test
 *
 * Order of commands:
 *  -# Inital low state
 *  -# State 1: NOEXT = 0, RST = 0, OFF = 0 (1 period)
 *  -# Low state - bus error (5.5 periods)
 *  -# State 1: NOEXT = 0, RST = 0, OFF = 0 (1 period)
 *  -# State 2: NOEXT = 0, RST = 0, OFF = 1 (1 period)
 */
static void LowStateRecoveryPrepareInput(void);

/*!
 * @brief Prepares the VSNOCABLE input for test
 *
 * Order of commands:
 *  -# Inital low state
 *  -# State 1: NOEXT = 0, RST = 0, OFF = 0 (1 period)
 *  -# High state - bus error (5.5 periods)
 *  -# State 1: NOEXT = 0, RST = 0, OFF = 0 (1 period)
 *  -# State 2: NOEXT = 0, RST = 0, OFF = 1 (1 period)
 */
static void HighStateRecoveryPrepareInput(void);

/*!
 * @brief Prepares the VSNOCABLE input for test
 *
 * Order of commands:
 *  -# Initial low state
 *  -# State 5: NOEXT = 1, RST = x, OFF = x (1 period)
 *  -# State 1: NOEXT = 0, RST = 0, OFF = 0 (1 period)
 *  -# State 5: NOEXT = 1, RST = x, OFF = x (1 period)
 *  -# State 4: NOEXT = 0, RST = 0, OFF = 1 (1 period)
 */
static void NoextRecoveryPrepareInput(void);

/*!
 * @brief Prepares the VSNOCABLE input for test
 *
 * Order of commands:
 *  -# Initial low state
 *  -# State 5: NOEXT = 1, RST = x, OFF = x (1 period)
 *  -# State 5: NOEXT = 1, RST = x, OFF = x (1 period)
 */
static void NoextHoldPrepareInput(void);

/*!
 *  @brief Tests if the signal is decoded correctly
 */
static void DecodePwmSignalStateTest(void **state);

/*!
 * @brief Triggers a valid command which resets all error flags
 */
static void ResetTriggerWarningsAndFlags(void **state);

/*!
 * @brief No command should be triggered when in unknown state
 */
static void KeepUnknownStateTest(void **state);

/*!
 * No command should be triggered when changing to unknown state
 */
static void ChangeStateToUnknownTest(void **state);

/*!
 *  @brief Tests if the command triggering function works correctly
 */
static void KeepStateTest(void **state);

/*!
 *  @brief Tests if the command triggering function works correctly
 */
static void ChangeStateTest(void **state);

/*!
 * @brief Resets the polling function state before a test
 */
static void ResetPollingFunctionState(void **state);

/*!
 * @brief Tests if simple state changes work correctly
 */
static void SimpleStateChangeTest(void **state);

/*!
 * @brief Tests if the function recovers properly from low-state bus error
 */
static void LowStateRecoveryTest(void **state);

/*!
 * @brief Tests if the function recovers properly from high-state bus error
 */
static void HighStateRecoveryTest(void **state);

/*!
 * @brief Tests if the function recovers properly from external control disabled (NOEXT = 1)
 *
 * On the falling edge of NOEXT signal a reset should be triggered, followed by an ON/OFF command,
 * based on the current state.
 */
static void NoextRecoveryTest(void **state);

/*!
 * No commands should be triggered when NOEXT is high.
 */
static void NoextHoldTest(void **state);

// Internal function definitions



static void PrepareExpectedBufferCommand(char const * const command)
{
    int i;

    for (i = 0; i < strlen(command); i++)
    {
        expect_value(OSMsgPost, c, command[i]);
    }
}



inline static void SetState(mst_vsnocable_state_t state)
{
    // Set the state, but save all warning/error flags
    vs.ext_control = (vs.ext_control & ~MST_VSNOCABLE_MASK_SIGNALS) | state;
}



inline static void CheckState(mst_vsnocable_state_t state)
{
    assert_true((vs.ext_control & MST_VSNOCABLE_MASK_SIGNALS) == state);
}



static void ResetPollingFunctionStatePrepareInput(void)
{
    for(t = 0; t < INITIAL_LOW_STATE_TIME; t++) will_return(VsnocableInput, FALSE);
    for(     ; t < FIRST_CMD;              t++) will_return(VsnocableInput, !!(t < INITIAL_LOW_STATE_TIME + MST_VSNOCABLE_NORST_ON));
    for(     ; t < SECOND_CMD;             t++) will_return(VsnocableInput, !!(t < FIRST_CMD + MST_VSNOCABLE_NORST_ON));
    for(     ; t < THIRD_CMD;              t++) will_return(VsnocableInput, !!(t < SECOND_CMD + MST_VSNOCABLE_NORST_ON));
    will_return(VsnocableInput, FALSE);
}



static void SimpleStateChangePrepareInput(void)
{
    for(t = 0; t < INITIAL_LOW_STATE_TIME; t++) will_return(VsnocableInput, FALSE);
    for(     ; t < FIRST_CMD;              t++) will_return(VsnocableInput, !!(t < INITIAL_LOW_STATE_TIME + MST_VSNOCABLE_RST_OFF));
    will_return(VsnocableInput, FALSE);
}



static void LowStateRecoveryPrepareInput(void)
{
    for(t = 0; t < INITIAL_LOW_STATE_TIME;  t++) will_return(VsnocableInput, FALSE);
    for(     ; t < RECOVERY_FIRST_CMD;      t++) will_return(VsnocableInput, !!(t < INITIAL_LOW_STATE_TIME + MST_VSNOCABLE_NORST_ON));
    for(     ; t < RECOVERY_BUS_ERROR_TIME; t++) will_return(VsnocableInput, FALSE);
    for(     ; t < RECOVERY_SECOND_CMD;     t++) will_return(VsnocableInput, !!(t < RECOVERY_FIRST_CMD + MST_VSNOCABLE_NORST_ON));
    for(     ; t < RECOVERY_THIRD_CMD;      t++) will_return(VsnocableInput, !!(t < RECOVERY_SECOND_CMD + MST_VSNOCABLE_NORST_OFF));
    will_return(VsnocableInput, FALSE);
}



static void HighStateRecoveryPrepareInput(void)
{
    for(t = 0; t < INITIAL_LOW_STATE_TIME;  t++) will_return(VsnocableInput, FALSE);
    for(     ; t < RECOVERY_FIRST_CMD;      t++) will_return(VsnocableInput, !!(t < INITIAL_LOW_STATE_TIME + MST_VSNOCABLE_NORST_ON));
    for(     ; t < RECOVERY_BUS_ERROR_TIME; t++) will_return(VsnocableInput, TRUE);
    for(     ; t < RECOVERY_SECOND_CMD;     t++) will_return(VsnocableInput, !!(t < RECOVERY_FIRST_CMD + MST_VSNOCABLE_NORST_ON));
    for(     ; t < RECOVERY_THIRD_CMD;      t++) will_return(VsnocableInput, !!(t < RECOVERY_SECOND_CMD + MST_VSNOCABLE_NORST_OFF));
    will_return(VsnocableInput, FALSE);
}



static void NoextRecoveryPrepareInput(void)
{
    for(t = 0; t < INITIAL_LOW_STATE_TIME; t++) will_return(VsnocableInput, FALSE);
    for(     ; t < FIRST_CMD;              t++) will_return(VsnocableInput, !!(t < INITIAL_LOW_STATE_TIME + MST_VSNOCABLE_NOEXT));
    for(     ; t < SECOND_CMD;             t++) will_return(VsnocableInput, !!(t < FIRST_CMD + MST_VSNOCABLE_NORST_OFF));
    for(     ; t < THIRD_CMD;              t++) will_return(VsnocableInput, !!(t < SECOND_CMD + MST_VSNOCABLE_NOEXT));
    for(     ; t < FOURTH_CMD;             t++) will_return(VsnocableInput, !!(t < THIRD_CMD + MST_VSNOCABLE_RST_ON));
    will_return(VsnocableInput, FALSE);
}



static void NoextHoldPrepareInput(void)
{
    for(t = 0; t < INITIAL_LOW_STATE_TIME; t++) will_return(VsnocableInput, FALSE);
    for(     ; t < FIRST_CMD;              t++) will_return(VsnocableInput, !!(t < INITIAL_LOW_STATE_TIME + MST_VSNOCABLE_NOEXT));
    for(     ; t < SECOND_CMD;             t++) will_return(VsnocableInput, !!(t < FIRST_CMD + MST_VSNOCABLE_NOEXT));
    will_return(VsnocableInput, FALSE);
}



static void DecodePwmSignalStateTest(void **state)
{
    for (t = 0; t <= MST_VSNOCABLE_PWM_PERIOD; t++)
    {
        if (t >= (MST_VSNOCABLE_NORST_ON - 1) &&
            t <= (MST_VSNOCABLE_NORST_ON + 1))
        {
            assert_int_equal(DecodePwmSignalState(t), MST_VSNOCABLE_STATE_NORST_ON);
        }
        else if (t >= (MST_VSNOCABLE_NORST_OFF - 1) &&
                 t <= (MST_VSNOCABLE_NORST_OFF + 1))
        {
            assert_int_equal(DecodePwmSignalState(t), MST_VSNOCABLE_STATE_NORST_OFF);
        }
        else if (t >= (MST_VSNOCABLE_RST_ON - 1) &&
                 t <= (MST_VSNOCABLE_RST_ON + 1))
        {
            assert_int_equal(DecodePwmSignalState(t), MST_VSNOCABLE_STATE_RST_ON);
        }
        else if (t >= (MST_VSNOCABLE_RST_OFF - 1) &&
                 t <= (MST_VSNOCABLE_RST_OFF + 1))
        {
            assert_int_equal(DecodePwmSignalState(t), MST_VSNOCABLE_STATE_RST_OFF);
        }
        else if (t >= (MST_VSNOCABLE_NOEXT - 1) &&
                 t <= (MST_VSNOCABLE_NOEXT + 1))
        {
            assert_int_equal(DecodePwmSignalState(t), MST_VSNOCABLE_STATE_NOEXT);
        }
        else
        {
            assert_int_equal(DecodePwmSignalState(t), MST_VSNOCABLE_STATE_UNKNOWN);
        }
    }
}



static void ResetTriggerWarningsAndFlags(void **state)
{
    // Reset the warning and error flags
    TriggerStateCommands(MST_VSNOCABLE_STATE_NOEXT);
}



static void KeepStateTest(void **state)
{
    // State 1 -> State 1

    // Set previous state
    SetState(MST_VSNOCABLE_STATE_NORST_ON);

    // Trigger commands based on state change
    TriggerStateCommands(MST_VSNOCABLE_STATE_NORST_ON);

    // Check if the state was saved
    CheckState(MST_VSNOCABLE_STATE_NORST_ON);

    // Check if no warnings were triggered

    assert_false(vs.ext_control & FGC_EXT_CONTROL_UNKNOWN_CMD);
    assert_false(ST_LATCHED & FGC_LAT_VS_COMMS);
}



static void ChangeStateTest(void **state)
{
    // State 5 -> State 1

    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_RESET]);
    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_POWER_ON]);
    SetState(MST_VSNOCABLE_STATE_NOEXT);
    TriggerStateCommands(MST_VSNOCABLE_STATE_NORST_ON);
    CheckState(MST_VSNOCABLE_STATE_NORST_ON);
    assert_false(vs.ext_control & FGC_EXT_CONTROL_UNKNOWN_CMD);
    assert_false(ST_LATCHED & FGC_LAT_VS_COMMS);

    // State 4 -> State 3

    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_POWER_ON]);
    SetState(MST_VSNOCABLE_STATE_RST_OFF);
    TriggerStateCommands(MST_VSNOCABLE_STATE_RST_ON);
    CheckState(MST_VSNOCABLE_STATE_RST_ON);
    assert_false(vs.ext_control & FGC_EXT_CONTROL_UNKNOWN_CMD);
    assert_false(ST_LATCHED & FGC_LAT_VS_COMMS);
}



static void ChangeStateToUnknownTest(void **state)
{
    expect_value(RunlogWrite, error_number, MST_VSNOCABLE_ERROR_UNKNOWN_CMD);

    SetState(MST_VSNOCABLE_STATE_NOEXT);
    TriggerStateCommands(MST_VSNOCABLE_STATE_UNKNOWN);

    // Check if the state has not changed
    CheckState(MST_VSNOCABLE_STATE_NOEXT);

    // Check if warning flags has been set
    assert_true(vs.ext_control & FGC_EXT_CONTROL_UNKNOWN_CMD);
    assert_true(ST_LATCHED & FGC_LAT_VS_COMMS);
}



static void KeepUnknownStateTest(void **state)
{
    // Now check if the runlog will not be flooded with error messages in case of an unknown state

    // There should be only one call to the RunlogWrite function
    expect_value(RunlogWrite, error_number, MST_VSNOCABLE_ERROR_UNKNOWN_CMD);

    // Set a valid state to start with
    SetState(MST_VSNOCABLE_STATE_RST_ON);

    unsigned i = 0;

    // Call the command triggering function 5 times

    for (i = 0; i < 5; i++)
    {
        TriggerStateCommands(MST_VSNOCABLE_STATE_UNKNOWN);
        assert_true(vs.ext_control & FGC_EXT_CONTROL_UNKNOWN_CMD);
        assert_true(ST_LATCHED & FGC_LAT_VS_COMMS);
    }
}



static void ResetPollingFunctionState(void **state)
{
    is_setup = TRUE;

    ResetPollingFunctionStatePrepareInput();

    // Run the simulation and reset the function
    for (t = 0; t <= THIRD_CMD; t++)
    {
        ComHvPollVsNoCable();
    }

    vs.ext_control = 0;

    is_setup = FALSE;
}



static void SimpleStateChangeTest(void **state)
{
    SimpleStateChangePrepareInput();

    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_RESET]);
    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_POWER_OFF]);

    for (t = 0; t <= FIRST_CMD; t++)
    {
        ComHvPollVsNoCable();
    }
}



static void LowStateRecoveryTest(void **state)
{
    LowStateRecoveryPrepareInput();

    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_POWER_OFF]);

    expect_value(RunlogWrite, error_number, MST_VSNOCABLE_ERROR_BUS_FAULT);

    for (t = 0; t <= RECOVERY_THIRD_CMD; t++)
    {
        ComHvPollVsNoCable();

        if (t == RECOVERY_SECOND_CMD)
        {
            assert_true(vs.ext_control & FGC_EXT_CONTROL_STUCK_LOW);
        }
    }
}



static void HighStateRecoveryTest(void **state)
{
    HighStateRecoveryPrepareInput();

    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_POWER_OFF]);

    expect_value(RunlogWrite, error_number, MST_VSNOCABLE_ERROR_BUS_FAULT);

    for (t = 0; t <= RECOVERY_THIRD_CMD; t++)
    {
        ComHvPollVsNoCable();

        if (t == RECOVERY_SECOND_CMD)
        {
            assert_true(vs.ext_control & FGC_EXT_CONTROL_STUCK_HIGH);
        }
    }
}



static void NoextRecoveryTest(void **state)
{
    NoextRecoveryPrepareInput();

    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_RESET]);
    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_POWER_OFF]);
    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_RESET]);
    PrepareExpectedBufferCommand(vsnocable_ext_commands[MST_VSNOCABLE_CMD_POWER_ON]);

    for (t = 0; t <= FOURTH_CMD; t++)
    {
        ComHvPollVsNoCable();
    }
}



static void NoextHoldTest(void **state)
{
    NoextHoldPrepareInput();

    for (t = 0; t <= SECOND_CMD; t++)
    {
        ComHvPollVsNoCable();
    }
}

// External function definitions

// These functions are mocks

void RunlogWrite(INT8U id, void * data)
{
    uint16_t error_number = *((uint16_t *)data);

    check_expected(error_number);
}



BOOLEAN VsnocableInput(void)
{
    return (BOOLEAN) mock();
}



void OSMsgPost(struct os_msg * msg, void * message)
{
    if (!is_setup)
    {
        char c = (char) (intptr_t) message;

        check_expected(c);
    }
}



int main(int argc, char *argv[])
{
    const UnitTest tests[] =
    {
            unit_test(DecodePwmSignalStateTest),
            unit_test_setup(KeepStateTest,            ResetTriggerWarningsAndFlags),
            unit_test_setup(ChangeStateTest,          ResetTriggerWarningsAndFlags),
            unit_test_setup(ChangeStateToUnknownTest, ResetTriggerWarningsAndFlags),
            unit_test_setup(KeepUnknownStateTest,     ResetTriggerWarningsAndFlags),
            unit_test_setup(SimpleStateChangeTest,    ResetPollingFunctionState),
            unit_test_setup(LowStateRecoveryTest,     ResetPollingFunctionState),
            unit_test_setup(HighStateRecoveryTest,    ResetPollingFunctionState),
            unit_test_setup(NoextRecoveryTest,        ResetPollingFunctionState),
            unit_test_setup(NoextHoldTest,            ResetPollingFunctionState),
    };

    return run_tests(tests);
}
