#!/usr/bin/python
#
# Copyright (C) 2012 CERN.
#
# DESCRIPTION:
#
# CONTACT: main author, Pierre DEJOUE.
#

import sys, os

# Somewhat inelegant way to import the struct_map.py module
scripts_rel_path = '../../../utils/scripts'
sys.path.append(os.path.join(*scripts_rel_path.split('/')))
from struct_map import *
sys.path.pop()

# Keep intermediate files (with the mapping of prop_buf)?
keep_intermediate_files = False

# Class used to parse prop_buf
class struct_map_prop_buf(struct_map):

    def __init__(self):
        self.input_source_file = "./obj/main.i"
        self.input_info_file   = "dpcom_map.info.csv"

        self.output_file_flag  = keep_intermediate_files
        self.output_file       = "prop_buf.map"

        self.struct_name       = "prop_buf"

        self.start_address_mcu = 0x00
        self.start_address_dsp = 0x00

        self.dsp_c32_flag      = False                                          # The C32 DSP addresses 32 bits of data.
                                                                                # The addr_offset therefore needs to be divided by four.


# Class used to parse dpcom
class struct_map_dpcom(struct_map):

    def __init__(self):
        self.input_source_file = "./obj/main.i"
        self.input_info_file   = "dpcom_map.info.csv"

        self.output_file_flag  = True
        self.output_file       = "dpcom.map"

        self.struct_name       = "dpcom"

        self.start_address_mcu = 0x04000000
        self.start_address_dsp = 0x90000000

        self.dsp_c32_flag      = False                                          # The C32 DSP addresses 32 bits of data.
                                                                                # The addr_offset therefore needs to be divided by four.

if __name__ == "__main__":

    mapping1 = struct_map_prop_buf()                                            # Parse "struct prop_buf"
    mapping1.parse()

    mapping2 = struct_map_dpcom()                                               # Parse "struct dpcom"
    mapping2.parse()
