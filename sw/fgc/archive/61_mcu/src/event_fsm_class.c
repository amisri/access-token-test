/*!
 *  @file      event_fsm_class.c
 *  @defgroup  FGC:MCU:61
 *  @brief     No functionality defined in this file. Class 61 currently does not use the
 *             event-driver state machine.
 */

#define EVENT_FSM_CLASS_GLOBALS

// Includes

#include <event_fsm_class.h>
#include <stdbool.h>


// External function definitions

void EventFsmInit(void)
{
    ;
}

bool WAtoPR(void)
{
    return (false);
}

bool PRtoSE(void)
{
    return (false);
}

bool SEtoRE(void)
{
    return (false);
}

bool REtoWA(void)
{
    return (false);
}

bool XXtoFA(void)
{
    return (false);
}

bool FAtoWA(void)
{
    return (false);
}

void EventStateWA(bool first_f)
{
    ;
}

void EventStatePR(bool first_f)
{
    ;
}

void EventStateSE(bool first_f)
{
    ;
}

void EventStateRE(bool first_f)
{
    ;
}

void EventStateFA(bool first_f)
{
    ;
}

// EOF
