/*!
 *  @file      set_class.c
 *  @defgroup  FGC:MCU:61
 *  @brief     Class specific Set command functions.
 */

#include <set_class.h>
#include <cc_types.h>
#include <cmd.h>
#include <macros.h>
#include <property.h>
#include <defprops.h>
#include <dev.h>
#include <dpcls.h>
#include <dpcom.h>
#include <fgc_errs.h>
#include <prop.h>
#include <pars.h>
#include <pub.h>
#include <ref_class.h>

// External function definitions

INT16U SetDefaults(struct cmd * c, struct prop * p)
{
    /*
     * SetDefaults provides support for S REF.DEFAULTS[] action. It will
     * assert the default values in the REF properties.
     */

    INT16U      prop_idx;
    INT16U      ls;
    union
    {
        FP32    f;
        INT32U  i;
    } value[7];                         // Size of props[] array below

    static struct prop * props[] =
    {
        &PROP_REF_PLEP_ACCELERATION,    //  0
        &PROP_REF_PLEP_LINEAR_RATE,     //  1
        &PROP_REF_PLEP_EXP_TC,          //  2
        &PROP_REF_PLEP_EXP_FINAL,       //  3
        &PROP_REF_TRIM_DURATION,        //  4
        &PROP_REF_RUN_DELAY,            //  5
        NULL,                           //  6
    };

    ls = (INT16U)dpcls.dsp.load_select;

    if (NON_PPM_REG_MODE == FGC_REG_V)                          // If openloop
    {
        value[0].f = dpcls.mcu.ref.defaults.v.acceleration;             // PROP_REF_PLEP_ACCELERATION
        value[1].f = dpcls.mcu.ref.defaults.v.linear_rate;              // PROP_REF_PLEP_LINEAR_RATE
        value[2].f = 0.0;                                               // PROP_REF_PLEP_EXP_TC
        value[3].f = 0.0;                                               // PROP_REF_PLEP_EXP_FINAL
    }
    else                                                        // else closed-loop (on B or I)
    {
        value[0].f = dpcls.mcu.ref.defaults.i.acceleration[ls];         // PROP_REF_PLEP_ACCELERATION
        value[1].f = dpcls.mcu.ref.defaults.i.linear_rate [ls];         // PROP_REF_PLEP_LINEAR_RATE
        value[2].f = dpcls.dsp.ref.exp_tc;                              // PROP_REF_PLEP_EXP_TC
        value[3].f = dpcls.dsp.ref.exp_final;                           // PROP_REF_PLEP_EXP_FINAL
    }

    value[4].f = 0.0;                                                   // PROP_REF_TRIM_DURATION
    value[5].f = 0.0;                                                   // PROP_REF_RUN_DELAY

    /* For each property that must be reset */

    for (prop_idx = 0; (p = props[prop_idx]); prop_idx++)
    {
        PropSet(c, p, &value[prop_idx], 1);
    }

    return (0);
}

INT16U SetDevicePPM(struct cmd * c, struct prop * p)
{
    // Used with DEVICE.PPM property.

    INT16U      errnum;

    // If DEVICE.PPM has changed

    if (!(errnum = ParsSet(c, p)))
    {
        dev.max_user = (dpcom.mcu.device_ppm ? FGC_MAX_USER : 0);

        // If DEVICE.PPM is now ENABLED then disarm user 0 if armed

        if (dpcom.mcu.device_ppm && dpcls.mcu.ref.func.type[0] != FGC_REF_NONE)
        {
            errnum = RefArm(c, NON_PPM_USER, FGC_REF_NONE, STC_NONE);
        }
    }

    return (errnum);
}

INT16U SetRef(struct cmd * c, struct prop * p)
{
    /*
     * SetRef provides support for S REF FUNC_TYPE,args,... and
     * S REF.FUNC.TYPE(user) FUNC_TYPE
     */

    INT16U              errnum;
    INT16U              user;                  // User index
    INT16U              func_type;             // Function type constant
    INT16U              stc_func_type;         // Function type symbol index
    const struct sym_lst * sym_const;

    // Scan first argument for settable function type symbol

    errnum = ParsScanSymList(c, "Y,", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    user          = c->mux_idx;
    func_type     = sym_const->value;
    stc_func_type = sym_const->sym_idx;

    /*--- Process request according to user (PPM/NON-PPM) and property (REF or REF.FUNC.TYPE) ---*/

    if (!user)                          // If NON-PPM (user == 0)
    {
        dpcls.mcu.ref.run_now_f = 0;            // Clear run now flag

        if (stc_func_type == STC_NONE)          // If NONE supplied (disarm NON-PPM function)
        {
            if (STATE_PC == FGC_PC_ARMED)
            {
//            if (STATE_PC == FGC_PC_ARMED &&     // If function is ARMED and
//                !eventsStartRcvd())             // start event not yet received
//            {
                return (0);
            }

            return (FGC_BAD_STATE);                     // Reject request
        }

        if (p->sym_idx == STP_REF)                      // if S REF FUNC_TYPE,args...
        {
            switch (stc_func_type)                      // Switch according to FUNC_TYPE to process args
            {
                case STC_NOW:
                    dpcls.mcu.ref.run_now_f = 1;    // Replace NOW with PLEP run now
                    func_type = FGC_REF_PLEP;
                    stc_func_type = STC_PLEP;
                    // Fall through

                case STC_PLEP:
                    errnum = RefPlep(c);
                    break;

                case STC_OPENLOOP:
                    errnum = RefOpenloop(c);
                    break;

                case STC_LTRIM:
                case STC_CTRIM:
                    errnum = RefTrim(c);
                    break;

                case STC_TABLE:
                    errnum = 0;
                    break;

                case STC_STEPS:
                case STC_SQUARE:
                case STC_SINE:
                case STC_COSINE:
                    errnum = RefTest(c);
                    break;

                default:
                    errnum = FGC_BAD_PARAMETER;
                    break;

            }

            if (errnum)                                 // If error returned
            {
                return (errnum);                        // Report the error
            }
        }
        else                                            // else S REF.FUNC.TYPE FUNC_TYPE
        {
            switch (stc_func_type)                      // Switch according to FUNC_TYPE
            {
                case STC_NOW:                               // If NOW

                    dpcls.mcu.ref.run_now_f = 1;            // Replace NOW with PLEP run now
                    func_type     = FGC_REF_PLEP;
                    stc_func_type = STC_PLEP;
                    break;

                case STC_OPENLOOP:                          // If OPENLOOP

                    if (NON_PPM_REG_MODE == FGC_REG_V)      // Check that reg mode is closed-loop
                    {
                        return (FGC_BAD_STATE);
                    }

                    break;
            }
        }
    }
    else                                        // else PPM (user > 0)
    {
        if (p->sym_idx == STP_REF)              // If setting REF then user must be zero
        {
            return (FGC_BAD_CYCLE_SELECTOR);              // Report BAD USER
        }

        if (stc_func_type == STC_NONE)
        {
            stc_func_type = STC_ZERO;
        }
    }


    return (RefArm(c, user, func_type, stc_func_type));
}

INT16U SetRefControlValue(struct cmd * c, struct prop * p)
{

    FP32 *        value_ptr;
    FP32          value;
    struct prop * limit_pos_p;
    struct prop * limit_min_p;
    struct prop * limit_neg_p;
    FP32          limits_pos;
    FP32          limits_neg;
    FP32          limits_min;
    INT16U        errnum;


    errnum  = ParsValueGet(c, p, (void **) &value_ptr);

    if (errnum != 0)
    {
        return errnum;
    }

    value = *value_ptr;


    // Get the limits from the DSP

    if (p == &PROP_REF_CCV_VALUE)
    {
        limit_pos_p = &PROP_LIMITS_OP_I_POS;
        limit_min_p = &PROP_LIMITS_OP_I_MIN;
        limit_neg_p = &PROP_LIMITS_OP_I_NEG;
    }
    else
    {
        limit_pos_p = &PROP_LIMITS_V_POS;
        limit_min_p = &PROP_LIMITS_V_NEG;
        limit_neg_p = &PROP_LIMITS_V_NEG;
    }


    // Used for REF.CCV and REF.VCV

    errnum |= PropValueGet(c, limit_pos_p, NULL, 0, (uint8_t **)&value_ptr);
    limits_pos = *value_ptr;

    errnum |= PropValueGet(c, limit_min_p, NULL, 0, (uint8_t **)&value_ptr);
    limits_min = *value_ptr;

    errnum |= PropValueGet(c, limit_neg_p, NULL, 0, (uint8_t **)&value_ptr);
    limits_neg = *value_ptr;


    if (errnum != 0)
    {
        return errnum;
    }

    if (limits_neg < 0.0)
    {
        limits_min = limits_neg;
    }

    if (value != 0.0 && (value < limits_min || value > limits_pos))
    {
        return(FGC_OUT_OF_LIMITS);
    }


    PropSet(c, p, (void*)&value, 1);

    PubProperty(p, NON_PPM_USER, TRUE);

    return (FGC_OK_NO_RSP);
}



INT16U SetTableFunc(struct cmd * c, struct prop * p)
{
    INT16U      errnum;

    errnum = ParsSet(c, p);

    if (errnum == 0)
    {
        // Attempt to arm the table function

        errnum = RefArm(c, c->mux_idx, FGC_REF_TABLE, STC_TABLE);
    }

    // If setting the function or arming it failed, recover the values from NVS

    if (errnum != 0)
    {
        NvsRecallValuesForOneProperty(c, p, c->mux_idx);
    }

    return (errnum);
}

// EOF
