/*!
 *  @file      mst_class.c
 *  @defgroup  FGC:MCU:61
 *  @brief     Class related millisecond actions.
 */

#define MST_CLASS_GLOBALS

#include <string.h>

#include <mst_class.h>
#include <shared_memory.h>
#include <dpcls.h>
#include <dpcom.h>
#include <macros.h>
#include <class.h>
#include <fbs_class.h>
#include <sta.h>
#include <dev.h>
#include <dls.h>
#include <mst.h>
#include <log.h>
#include <log_class.h>
#include <comhv_ps_class.h>


// Constants

#define  MST_LOG_IEARTH_PHASE         5
#define  MST_LOG_IREG_PHASE           6
#define  MST_LOG_ILEADS_PHASE         8


// Class specific function definitions

void MstClassProcess(void)
{
    // The ComHV-PS crate type encodes commands sent by a PLC into the VSNOCABLE input
    // The Thyristor crate type for the SVC application only encodes commands sent 
    // by a PLC into the STATUS11 input

    if (   crateGetType() == FGC_CRATE_TYPE_COMHV_PS
        || crateGetType() == FGC_CRATE_TYPE_RF25KV)
    {
        ComHvPollVsNoCable(Test(DIG_IP1_P, DIG_IP1_VSNOCABLE_MASK16));
    }
    else if (strncmp("RPAFU", dev.type, FGC_SYSDB_SYS_LEN) == 0)
    {
        ComHvPollVsNoCable(Test(DIG_IPDIRECT_P, DIG_IPDIRECT_STATUS11_MASK16));
    }
}

void MstClassPublishData(void)
{
    // Millisecond 1 of 10: Read analogue values.

    if ((mst.ms_mod_10 == 1) &&
        (shared_mem.mainprog_seq == FGC_MP_MAIN_RUNNING) &&
        (STATE_OP != FGC_OP_UNCONFIGURED))
    {
        I_REF = dpcls.dsp.ref.i;
        V_REF = dpcls.dsp.ref.v;
        I_MEAS = dpcls.dsp.meas.i;
    }

    //  Millisecond 1 of 20: Read DSP status and run Gateway PC_PERMIT timeout

    if (mst.ms_mod_20 == 1)
    {
        FAULTS      |= (INT16U) dpcom.dsp.faults;               // Latch faults from DSP
        ST_LATCHED  |= (INT16U) dpcom.dsp.st_latched;           // Latch status from DSP

        WARNINGS     = (WARNINGS & ~ALL_DSP_WARNINGS_MASK) |    // Mix in DSP warnings
                       (INT16U) dpcom.dsp.warnings;
        ST_UNLATCHED = (ST_UNLATCHED & ~DSP_UNLATCHED) |        // Mix in DSP unlatched status
                       (INT16U) dpcom.dsp.st_unlatched;

        ST_MEAS_A    = (ST_MEAS_A & ~DSP_MEAS) |                // Mix in DSP meas A flags
                       (INT8U) dpcls.dsp.meas.st_meas[0];       
        ST_MEAS_B    = (ST_MEAS_B & ~DSP_MEAS) |                // Mix in DSP meas B flags
                       (INT8U) dpcls.dsp.meas.st_meas[1];
        ST_MEAS_C    = (ST_MEAS_C & ~DSP_MEAS) |                // Mix in DSP meas C flags
                       (INT8U) dpcls.dsp.meas.st_meas[2];
        ST_MEAS_D    = (ST_MEAS_D & ~DSP_MEAS) |                // Mix in DSP meas D flags
                       (INT8U) dpcls.dsp.meas.st_meas[3];

        ST_DCCT_A    = (ST_DCCT_A & ~DSP_DCCT_MASK) |           // Mix in DSP dcct A flags
                       (INT8U) dpcls.dsp.meas.st_dcct[0];
        ST_DCCT_B    = (ST_DCCT_B & ~DSP_DCCT_MASK) |           // Mix in DSP dcct B flags
                       (INT8U) dpcls.dsp.meas.st_dcct[1];

        V_MEAS       = dpcls.dsp.meas.v;            // Vmeas is only measured at 50Hz
        I_ERR_MA     = (INT16S) dpcls.dsp.reg.i_err_ma;
        I_DIFF_MA    = (INT16S) dpcls.dsp.meas.i_diff_ma;
        I_EARTH_PCNT = (INT8S) meas_i_earth.pcnt;

        // Suppress the I_MEAS warning if I_MEAS fault is present.

        if (Test(FAULTS, FGC_FLT_I_MEAS))
        {
            Clr(WARNINGS, FGC_WRN_I_MEAS);
        }
    }
}

void MstClassLog(void)
{
    // Log IAB each millisecond

    if (log_iab.samples_to_acq)
    {
        LogIab();
    }

    // Millisecond 5 of 20: Log Iearth signal

    if (mst.ms_mod_20 == MST_LOG_IEARTH_PHASE)
    {
        if (log_iearth.samples_to_acq)
        {
            LogIearth();
        }
    }

    // Millisecond 6 of 10: Log Ireg signals

    if (mst.ms_mod_10 == MST_LOG_IREG_PHASE)
    {
        if (log_ireg.samples_to_acq)
        {
            LogIreg();
        }
    }

    // Check and log start of PPM cycles

    if ((DEVICE_CYC == FGC_CTRL_ENABLED) &&
        (INT16U)dpcom.dsp.cyc.start_cycle_f)
    {
        // Trigger publication of property LOG.CYC by STA task

        sta.cyc_start_new_cycle_f = TRUE;

        LogTiming();

        dpcom.dsp.cyc.start_cycle_f = FALSE;
    }

    // Millisecond 0 of 1000: Check logging and ADC periodic reset

    if (TimeGetUtcTimeMs() == 0)
    {
        if (dev.log_pm_state == FGC_LOG_STOPPING &&
            FGC_LAST_LOG.samples_to_acq == 0)
        {
            dev.log_pm_state = FGC_LOG_FROZEN;

            Clr(fbs.u.fieldbus_stat.ack, FGC_SELF_PM_REQ);
        }
    }

    // Millisecond 9 of 1000: Log temperatures

    if (TimeGetUtcTimeMs() == 9)
    {
        mst.s_mod_600 = (INT16U)(TimeGetUtcTime() % 600L);
        mst.s_mod_10  = mst.s_mod_600 % 10;

        if (!mst.s_mod_10 && temp.thour_f)
        {
            LogThour();

            if (temp.tday_f)
            {
                LogTday();
            }

            temp.thour_f = FALSE;
        }
    }
}

// EOF
