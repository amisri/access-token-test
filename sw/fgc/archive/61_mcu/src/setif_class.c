/*!
 *  @file      setif_class.c
 *  @defgroup  FGC:MCU:61
 *  @brief     Class specific Setif command functions.
 */

#include <setif_class.h>
#include <cmd.h>
#include <fbs_class.h>
#include <mst.h>
#include <log_class.h>
#include <fgc_errs.h>

// External function definitions

INT16U SetifCalOk(struct cmd * c)
{
    // Set If Condition: Is Auto-Calibration of ADCs allowed?

    if ( (STATE_OP != FGC_OP_CALIBRATING)
      && ((STATE_OP == FGC_OP_UNCONFIGURED)
       || ((STATE_OP == FGC_OP_NORMAL)
        && ((STATE_PC == FGC_PC_IDLE)
         || (!(!log_ireg.run_f && log_ireg.samples_to_acq)
            )
          )
         )
        )
       )
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}

// EOF
