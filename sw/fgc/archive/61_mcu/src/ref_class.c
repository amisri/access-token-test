/*!
 *  @file     ref_class.c
 *  @defgroup FGC3:MCU:61
 *  @brief    Reference-related functions specific to Class 61
 */

#define REF_CLASS_GLOBALS

// Includes

#include <ref_class.h>
#include <ref.h>
#include <defconst.h>
#include <defprops.h>
#include <fgc_errs.h>
#include <property.h>
#include <dpcls.h>
#include <pars.h>

// Internal function declarations

/*!
 * This function will get and check the required number of reference
 * property parameters.
 *
 * @param c Command structure.
 * @param p Pointer to the property.
 * @param pars Buffer to store the parameter values.
 * @param n_pars Number of parameters to retrieve.
 *
 * @retval FGC_OK_NO_RSP if it succeeds.
 */
static INT16U RefGetPars(struct cmd * c, struct prop ** p,
                         FP32 * pars, INT16U n_pars);

/*!
 * This function sets the properties identified in the array p with the values
 * in the array pars
 *
 * @param c Command structure.
 * @param p Pointer to the property.
 * @param pars Buffer with the parameter values.
 * @param n_pars Number of parameters to set.
 *
 * @retval FGC_OK_NO_RSP if it succeeds.
 */
static  void RefSetPars(struct cmd * c, struct prop ** p,
                        FP32 * pars, INT16U n_pars);

// Internal function definitions

static INT16U RefGetPars(struct cmd * c, struct prop ** p,
                         FP32 * pars, INT16U n_pars)
{
    INT16U  i;
    INT16U  errnum;
    float * newval;
    float   value;
    struct prop * pr;

    for (i = 0; i < n_pars && !c->last_par_f; p++, i++, pars++)
    {
        errnum = ParsScanStd(c, &PROP_REF_PLEP_FINAL, PKT_FLOAT, (void **)&newval);     // User PLEP_FINAL as it

        // has the widest limits
        if (errnum != FGC_NO_SYMBOL)
        {
            if (errnum)
            {
                if (i)                  // If not first parameter
                {
                    c->device_par_err_idx = i;          // Set err_idx to parameter idx
                }

                return (errnum);
            }

            value = *newval;
            pr    = *p;

            if (value < ((struct float_limits *)pr->range)->min ||
                value > ((struct float_limits *)pr->range)->max)
            {
                c->device_par_err_idx = i;
                return (FGC_OUT_OF_LIMITS);             // Report OUT OF LIMITS
            }

            *pars = value;
        }
    }

    if (!c->last_par_f)             // If more parameters remain
    {
        c->device_par_err_idx = i;
        return (FGC_BAD_PARAMETER);         // Report BAD PARAMETER
    }

    return (FGC_OK_NO_RSP);
}

static void RefSetPars(struct cmd * c, struct prop ** p,
                       FP32 * pars, INT16U n_pars)
{
    while (n_pars--)
    {
        PropSet(c, *(p++), pars++, 1);
    }
}

// External function definitions

INT16U RefPlep(struct cmd * c)
{
    INT16U  errnum;
    INT16U  load_select;

    static struct prop * p[] =
    {
        &PROP_REF_PLEP_FINAL,
        &PROP_REF_PLEP_ACCELERATION,
        &PROP_REF_PLEP_LINEAR_RATE,
        &PROP_REF_PLEP_EXP_TC,
        &PROP_REF_PLEP_EXP_FINAL,
    };

    // Prepare default values

    if (NON_PPM_REG_MODE == FGC_REG_V)                          // If open loop
    {       
        ref.pars.f[0] = V_REF;                                  // FINAL
        ref.pars.f[1] = dpcls.mcu.ref.defaults.v.acceleration;  // ACCELERATION
        ref.pars.f[2] = dpcls.mcu.ref.defaults.v.linear_rate;   // LINEAR_RATE
        ref.pars.f[3] = 0.0;                                    // EXP_TC
        ref.pars.f[4] = 0.0;                                    // EXP_FINAL
    }
    else                                                        // else, close loop
    {
        load_select = (INT16U)dpcls.dsp.load_select;

        ref.pars.f[0] = I_REF;                                              // FINAL
        ref.pars.f[1] = dpcls.mcu.ref.defaults.i.acceleration[load_select]; // ACCELERATION
        ref.pars.f[2] = dpcls.mcu.ref.defaults.i.linear_rate [load_select]; // LINEAR_RATE
        ref.pars.f[3] = dpcls.dsp.ref.exp_tc;                               // EXP_TC
        ref.pars.f[4] = dpcls.dsp.ref.exp_final;                            // EXP_FINAL
    }

    // Get up to five parameters (final,acceleration,linear_rate,exp_tc,exp_final)

    errnum = RefGetPars(c, (struct prop **) &p, ref.pars.f, 5);

    if (errnum)
    {
        return (errnum);
    }

    // Set properties

    RefSetPars(c, (struct prop **) &p, ref.pars.f, 5);

    return (FGC_OK_NO_RSP);
}

INT16U RefOpenloop(struct cmd * c)
{
    INT16U  errnum;

    static struct prop * p[] =
    {
        &PROP_REF_OPENLOOP_FINAL,
    };

    // Check that REF.FUNC.REG_MODE[0] is not V (closed-loop)
    // and RT is NOT enabled

    if (NON_PPM_REG_MODE == FGC_REG_V || (INT16U)dpcls.mcu.mode_rt)
    {
        return (FGC_BAD_STATE);
    }

    //Prepare default values

    ref.pars.f[0] = I_REF;

    // Get up to two parameters (final,period)

    errnum = RefGetPars(c, (struct prop **) &p, ref.pars.f, 1);

    if (errnum)
    {
        return (errnum);
    }

    // Set properties

    RefSetPars(c, (struct prop **) &p, ref.pars.f, 1);

    return (FGC_OK_NO_RSP);
}

INT16U RefTrim(struct cmd * c)
{
    INT16U  errnum;

    static struct prop * p[] =
    {
        &PROP_REF_TRIM_FINAL,
        &PROP_REF_TRIM_DURATION,
    };

    // Prepare default values

    ref.pars.f[0] = (NON_PPM_REG_MODE == FGC_REG_V ? V_REF : I_REF);
    ref.pars.f[1] = 0.0;

    // Get up to two parameters (final,period)

    errnum = RefGetPars(c, (struct prop **) &p, ref.pars.f, 2);

    if (errnum)
    {
        return (errnum);
    }

    // Set properties

    RefSetPars(c, (struct prop **) &p, ref.pars.f, 2);

    return (FGC_OK_NO_RSP);
}

INT16U RefTest(struct cmd * c)
{
    INT16U  errnum;

    static struct prop * p[] =
    {
        &PROP_REF_TEST_AMPLITUDE,
        &PROP_REF_TEST_NUM_CYCLES,
        &PROP_REF_TEST_PERIOD
    };

    /*--- Prepare default values ---*/

    ref.pars.f[0] = 1.0;
    ref.pars.f[1] = 1.0;
    ref.pars.f[2] = 1.0;

    // Get up to three parameters (amp,n_cyc,period)

    errnum = RefGetPars(c, (struct prop **) &p, ref.pars.f, 3);

    if (errnum)
    {
        return (errnum);
    }

    // Set properties

    RefSetPars(c, (struct prop **) &p, ref.pars.f, 3);

    return (FGC_OK_NO_RSP);
}

// EOF
