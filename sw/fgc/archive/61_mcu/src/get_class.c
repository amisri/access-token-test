/*!
 *  @file      get_class.c
 *  @defgroup  FGC:MCU:61
 *  @brief     Class specific get functions
 */

#define GET_CLASS_GLOBALS

// Includes

#include <get_class.h>
#include <class.h>
#include <defprops.h>
#include <DIMsDataProcess.h>
#include <fgc_errs.h>
#include <fgc_parser_consts.h>
#include <log_class.h>
#include <log.h>
#include <macros.h>
#include <mcu_dependent.h>
#include <mem.h>
#include <start.h>
#include <string.h>

// External function definitions

uint16_t GetLogPm(struct cmd * c, struct prop * p)
{
    if (c->n_arr_spec || c->step > 1)       // If array indices are specified
    {
        return (FGC_BAD_ARRAY_IDX);         // Report error
    }

    if (c == &tcm || !Test(c->getopts, GET_OPT_BIN)) // If command is not via the fieldbus or BIN not specified
    {
        return (0);                 // Return nothing
    }

    if (FGC_LAST_LOG.samples_to_acq)    // If logging in progress
    {
        return (FGC_BAD_STATE);             // Report BAD STATE
    }

    CmdStartBin(c, p, FGC_PM_BUF_LEN_61);          // Write binary data header

    LogGetPmEvtBuf(c);                  // Send Event  log  ( 49096     bytes)
    LogGetPmSigBuf(&log_iab,    c);     // Send IAB    log  ( 65536 + 8 bytes)
    LogGetPmSigBuf(&log_ireg,   c);     // Send IREG   log  ( 61440 + 8 bytes)
    LogGetPmSigBuf(&log_iearth, c);     // Send IEARTH log  (  4096 + 8 bytes)
    LogGetPmSigBuf(&log_thour,  c);     // Send THOUR  log  (  2880 + 8 bytes)

    return (0);
}

uint16_t GetLogCaptureSpy(struct cmd * c, struct prop * p)
{
    /*
     *   The capture log buffer (LOG.CAPTURE.BUF) contains up to 7200 records
     *   of 9 floats. These signals are:
     *       0   0x0001      REF      (I_REF or B_REF)
     *       1   0x0002      V_REF
     *       2   0x0004      B_MEAS
     *       3   0x0008      I_MEAS
     *       4   0x0010      V_MEAS
     *       5   0x0020      ERR
     *       6   0x0040      Signal from LOG.SPY.MPX[0]
     *       7   0x0080      Signal from LOG.SPY.MPX[1]
     *       8   0x0100      Signal from LOG.SPY.MPX[2]
     */

    uint16_t      i;
    uint16_t      s;
    uint16_t      reg_state;
    prop_size_t   sample_idx;
    uint16_t      n_sigs;
    uint16_t      n_samples;
    uint16_t      mask;
    uint16_t      sigs_mask;
    uint16_t      errnum;
    char     *    label;
    char     *    ch_ptr;

    union                                       // Union to reduce impact on stack
    {
        struct fgc_log_header      log;
        struct fgc_log_sig_header  sig;
    } header;

    static struct capture_log
    {
        char    *   label;
        char    *   units;
        uint16_t    info;
    }  signals[3][9] =
    {
        {
            { 0,                "",       0 },                          //  V reg mode
            { "V_REF",          "V",      FGC_LOG_SIG_INFO_STEPS },
            { "B_MEAS",         "G",      0 },
            { "I_MEAS",         "A",      0 },
            { "V_MEAS",         "V",      0 },
            { 0,                "",       0 },
            { 0,                "",       0 },
            { 0,                "",       0 },
            { 0,                "",       0 },
        },
        {
            { "I_REF",          "A",      FGC_LOG_SIG_INFO_STEPS },     //  I reg mode
            { "V_REF",          "V",      FGC_LOG_SIG_INFO_STEPS },
            { "B_MEAS",         "G",      0 },
            { "I_MEAS",         "A",      0 },
            { "V_MEAS",         "V",      0 },
            { "I_ERR",          "A",      FGC_LOG_SIG_INFO_STEPS },
            { 0,                "",       0 },
            { 0,                "",       0 },
            { 0,                "",       0 },
        },
        {
            { "B_REF",          "G",      FGC_LOG_SIG_INFO_STEPS },     // B reg mode
            { "V_REF",          "V",      FGC_LOG_SIG_INFO_STEPS },
            { "B_MEAS",         "G",      0 },
            { "I_MEAS",         "A",      0 },
            { "V_MEAS",         "V",      0 },
            { "B_ERR",          "G",      FGC_LOG_SIG_INFO_STEPS },
            { 0,                "",       0 },
            { 0,                "",       0 },
            { 0,                "",       0 },
        }
    };

    // Check that Get command and options are valid for a Spy log

    n_samples = dpcls.dsp.log.capture.n_samples;

    // Array indices cannot be specified

    if (c->n_arr_spec || c->step > 1)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    // BIN must be set

    if (!Test(c->getopts, GET_OPT_BIN))
    {
        return (FGC_BAD_GET_OPT);
    }

    // Make sure data is available

    if (!n_samples)
    {
        return (0);
    }

    // Make sure the user is correct

    if (c->mux_idx != 0 &&
        c->mux_idx != dpcls.dsp.log.capture.user)
    {
        return (FGC_BAD_CYCLE_SELECTOR);
    }

    // Select signals according to property and reg mode

    reg_state = dpcls.dsp.log.capture.reg_state;

    sigs_mask = 0x0000;

    switch (p->sym_idx)
    {
        case STP_ALL:

            switch (reg_state)
            {
                case FGC_REG_B: sigs_mask = 0x01FF; break; // B_REF|V_REF|B_MEAS|I_MEAS|V_MEAS|B_ERR|MPX[0]|MPX[1]|MPX[2]

                case FGC_REG_I: sigs_mask = 0x01FB; break; // I_REF|V_REF|      |I_MEAS|V_MEAS|I_ERR|MPX[0]|MPX[1]|MPX[2]

                case FGC_REG_V: sigs_mask = 0x01DE; break; //      |V_REF|B_MEAS|I_MEAS|V_MEAS|     |MPX[0]|MPX[1]|MPX[2]
            }

            break;

        case STP_OP:

            switch (reg_state)
            {
                case FGC_REG_B: sigs_mask = 0x003F; break; // B_REF|V_REF|B_MEAS|I_MEAS|V_MEAS|B_ERR

                case FGC_REG_I: sigs_mask = 0x003B; break; // I_REF|V_REF|      |I_MEAS|V_MEAS|I_ERR

                case FGC_REG_V: sigs_mask = 0x001E; break; //      |V_REF|B_MEAS|I_MEAS|V_MEAS|
            }

            break;

        case STP_REG:

            switch (reg_state)
            {
                case FGC_REG_B: sigs_mask = 0x0025; break; // B_REF|     |B_MEAS|      |      |B_ERR

                case FGC_REG_I: sigs_mask = 0x0029; break; // I_REF|     |      |I_MEAS|      |I_ERR

                case FGC_REG_V: return (FGC_BAD_STATE);
            }

            break;
    }

    // Count the number of signals specified in sigs_mask

    for (n_sigs = 0, mask = sigs_mask; mask; n_sigs++)
    {
        mask &= mask - 1;                               // Clear least significant bit set
    }

    // Generate log header

    header.log.version   = FGC_LOG_VERSION;
    header.log.info      = 0;
    header.log.n_signals = n_sigs;
    header.log.n_samples = n_samples;
    header.log.period_us = dpcls.dsp.log.capture.sampling_period_us;
    header.log.unix_time = dpcls.dsp.log.capture.last_sample_time.unix_time;
    header.log.us_time   = dpcls.dsp.log.capture.last_sample_time.us_time;

    CmdStartBin(c, p, (uint32_t)(sizeof(header.log) + sizeof(header.sig) * n_sigs) +
                (uint32_t)(sizeof(float)) * (uint32_t)(n_sigs * header.log.n_samples));

    FbsOutBuf((uint8_t *) &header.log, sizeof(header.log), c);  // Write log header

    // Generate signal headers

    header.sig.type           = FGC_LOG_TYPE_FLOAT;
    header.sig.us_time_offset = 0;
    header.sig.gain           = 1.0;
    header.sig.offset         = 0.0;

    for (i = 0, mask = 1; i < FGC_LOG_CAPTURE_N_SIGS; i++, mask <<= 1)
    {
        if (Test(sigs_mask, mask))                               // if signal is included
        {
            header.sig.info = signals[reg_state][i].info;       // Set signal info (Step interpolation)

            label = signals[reg_state][i].label;

            if (!label)
            {
                label = CmdPrintSymLst((struct sym_lst *) &SYM_LST_SPY, (uint16_t)dpcls.mcu.log_capture_mpx[i - 6]);
            }

            strcpy(header.sig.label, label);                      // Set label and units
            strcpy(header.sig.units, signals[reg_state][i].units);
            header.sig.label_len = strlen(header.sig.label);
            header.sig.units_len = strlen(header.sig.units);

            FbsOutBuf((uint8_t *) &header.sig, sizeof(header.sig), c);               // Write signal header
        }
    }

    // Write data

    for (s = sample_idx = 0; s < n_samples; s++)
    {
        for (i = 0, mask = 1; i < FGC_LOG_CAPTURE_N_SIGS; i++, mask <<= 1)
        {
            if (Test(sigs_mask, mask))                      // if signal is included
            {
                errnum = PropValueGet(c, &PROP_LOG_CAPTURE_BUF, NULL, sample_idx + i, (uint8_t **)&ch_ptr);

                if (errnum != 0)
                {
                    return (errnum);
                }

                FbsOutLong(ch_ptr, c);
            }
        }

        sample_idx += FGC_LOG_CAPTURE_N_SIGS;
    }

    // Arm log capture to allow new Spy acquisitions, and reset LOG.CAPTURE.CYC_CHK_TIME

    dpcls.dsp.log.capture.state                  = FGC_LOG_ARMED;
    dpcls.dsp.log.capture.cyc_chk_time.unix_time = 0;
    dpcls.dsp.log.capture.cyc_chk_time.us_time   = 0;

    return (0);
}

uint16_t GetLogTiming(struct cmd * c, struct prop * p)
{
    const struct sym_lst * dysfunction_symbol_list;
    prop_size_t            n;
    uint16_t               out_idx;
    uint16_t               errnum;
    int8_t                 user;
    BOOLEAN                is_start_super_cycle;       
    char                 * dysfunction_label;


    if (DEVICE_CYC == FGC_CTRL_DISABLED)
    {
        return (0);
    }

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    out_idx = (timing_log.in_idx + 1 + c->from) % FGC_LOG_TIMING_LEN;

    while (n--)             // For all elements in the array range
    {
        errnum = CmdPrintIdx(c, c->from);      // Print index if required

        c->from++;

        if (errnum)
        {
            return (errnum);
        }

        user = timing_log.user[out_idx];

        // If log record is complete

        if (user)           
        {
            // A negative user indicates the start of the super-cycle

            if (user < 0)
            {
                is_start_super_cycle = TRUE;
                user = -user;
            }
            else
            {
                is_start_super_cycle = FALSE;
            }

            if (timing_log.dysfunction_code[out_idx])
            {
                dysfunction_symbol_list = (timing_log.dysfunction_is_a_fault[out_idx] ? SYM_LST_CYC_FLT : SYM_LST_CYC_WRN);
                dysfunction_label = CmdPrintSymLst(dysfunction_symbol_list, timing_log.dysfunction_code[out_idx]);
            }
            else
            {
                dysfunction_label = "";
            }

            fprintf(c->f, "%10lu.%06lu %2u %3s %c %-8s %s",
                    timing_log.time[out_idx].unix_time,
                    timing_log.time[out_idx].us_time,
                    user,
                    is_start_super_cycle ? "SSC" : "",
                    timing_log.reg_mode_flag[out_idx],
                    SYM_TAB_CONST[timing_log.stc_func_type[out_idx]].key.c,
                    dysfunction_label);

        }

        out_idx = (out_idx + 1) % FGC_LOG_TIMING_LEN;
    }

    return (0);
}



INT16U GetSpivs(struct cmd * c, struct prop * p)
{
    // Displays SPIVS debug information

    static const char * fmt_opts[] = { " 0x%08X  ", "%12.5E "};

    char        const * fmt;
    prop_size_t         n;
    uint16_t            errnum;
    char                delim;

       
    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    // Set delimiter
   
    delim   = (c->token_delim == ' ' ? '\n' : ',');
    fmt     = (Test(c->getopts, GET_OPT_HEX) ? fmt_opts[0] : fmt_opts[1]);

    fprintf(c->f, "Sent: %-10d  Faults: %-6d  Busy: %1d%c",
            (int)dpcom.spivs.num_sent,
            (int)dpcom.spivs.num_faults,
            (int)dpcom.spivs.num_busy,
            delim);

    fprintf(c->f, "                 Tx                                       Rx                    Error%c", delim);

    uint8_t i;
    uint8_t j;

    for (i = 0; i < SPIVS_DBG_LENGTH; ++i)
    {
        for (j = 0; j < 3; ++j)
        {
            fprintf(c->f, fmt, (unsigned int)dpcom.spivs.dbg_tx[i][j]);
        }

        fprintf(c->f, "| ");

        for (j = 0; j < 3; ++j)
        {
            fprintf(c->f, fmt, (unsigned int)dpcom.spivs.dbg_rx[i][j]);
        }

        fprintf(c->f, "   %1d%c", (int)dpcom.spivs.dbg_st[i], delim);
    }

    // Zero all the counters and mark the index for the DSP to resume
    // the logging of SPIVS diagnostics

    if (Test(c->getopts, GET_OPT_ZERO) == true)
    {
        dpcom.spivs.num_sent   = 0;
        dpcom.spivs.num_faults = 0;
        dpcom.spivs.num_busy   = 0;

        dpcom.spivs.dbg_idx = 0xFFFFFFFF;

    }

    return (0);
}

// EOF
