/*!
 *  @file      init_class.c
 *  @defgroup  FGC3:MCU:61
 *  @brief     Class specific initialization.
 */

#define INIT_CLASS_GLOBALS

// Include

#include <init_class.h>
#include <dev.h>
#include <dpcom.h>
#include <prop.h>
#include <macros.h>
#include <ana_temp_regul.h>
#include <regfgc3_pars.h>

// Class specific function definitions

void InitClassPreInts(void)
{
    // Read SD filter function meta data

    AdcFiltersReadFunctions();
}

void InitClassPostInts(void)
{
    NvsInit(&init_prop[0]);

    // Initialise the default non-PPM reg mode. This is the FIRST COPY of
    // the REG.MODE_INIT property to the non-PPM reg mode. (The second copy
    // will happen after synchronising with the Database.)

    dpcls.mcu.ref.func.reg_mode[0] = ref.reg_mode_init;

    dpcls.mcu.vs.polarity.mode = FGC_POL_MODE_NOT_SET;
    dpcls.mcu.vs.polarity.changed = FALSE;

    // Analogue Card temperature regulation
    // Make sure to call this function AFTER NvsInit
    // because it needs the config value CAL.VREF.TEMPERATURE.TARGET

    AnaTempInit();

    // Other initialisations

    dev.max_user = (dpcom.mcu.device_ppm ? FGC_MAX_USER : 0);

    if (DEVICE_CYC == FGC_CTRL_ENABLED)
    {
        rtd.ctrl = FGC_RTD_CYC;
    }

    // For standalone mode there is no post sync mechanism, we retrieve RegFGC3 parameters here
    if (StaIsStandalone() || Test(dev.omode_mask, FGC_LHC_SECTORS_NO_CONFIG_MGR) == true)
    {
        regFgc3ParsRetrieveParams();
    }

}

void InitClassPostSync(struct cmd * c)
{
}

// EOF
