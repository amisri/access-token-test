/*!
 *  @file      events_class.c
 *  @brief     File with the class-specific processing of events.
 */

#define EVENTS_CLASS_GLOBALS



// Includes

#include <stdint.h>

#include <events_class.h>
#include <events.h>
#include <fbs_class.h>
#include <log_cycle.h>
#include <cycle_time.h>
#include <dpcls.h>
// @TODO remove when EPCCCS-4425 is implemented
#include <crate.h>
#include <dig.h>




// Platform/class specific function declarations

void eventsClassProcessStart(struct fgc_event const * event)
{
    uint8_t event_group = event->payload;

    // Only accept this event if a function has been armed

    if (   STATE_PC == FGC_PC_ARMED
        && (    event_group == 0 
             || event_group == EVENT_GROUP))
    {
        uint32_t delay = event->delay_ms;

        dpcom.mcu.evt.start_event_delay = delay;

        LogCycleInsert("Event start", LOG_VALUE_TYPE_INT32U, (void *)&delay, LOG_ACTION_SET);            
    }
}



void eventsClassProcessAbort(struct fgc_event const * event)
{
     uint8_t event_group = event->payload;

    // Only accept this event if a function has been armed

    if (   STATE_PC == FGC_PC_RUNNING
        && (    event_group == 0 
             || event_group == EVENT_GROUP))
    {
        uint32_t delay = event->delay_ms;

        dpcom.mcu.evt.abort_event_delay = delay;

        Set(ST_UNLATCHED, FGC_UNL_ABORT_EVENT);

        LogCycleInsert("Event abort", LOG_VALUE_TYPE_INT32U, (void *)&delay, LOG_ACTION_SET);
    }
}



void eventsClassProcessInjection(struct fgc_event const * event)
{
    ; // Do nothing
}



void eventsClassProcessExtraction(struct fgc_event const * event)
{
    ; // Do nothing
}



void eventsClassProcessCycleStart(struct fgc_event const * event)
{
    // Register the timing event if REF.EVENT_CYC has been configured with this type

    if (dpcls.mcu.ref.event_cyc == FGC_EVENT_CYC_START_CYCLE)
    {
        CycTimeSetTimeTillEvent(event->delay_ms);
    }

    // @TODO The condition below is temporal until EPCCCS-4425 is implemented and
    //       only applies to the BSW converters in LN4.
    //       Explanation for this code is in EPCCCS-4420: 2 us pulse at C0 on SUP_A3

    if (crateGetType() == FGC_CRATE_TYPE_DSP_IGBT)
    {
        DigSupClrArm(DIG_SUP_A_CHANNEL3);
        DigSupSetPulseEtim(DIG_SUP_A_CHANNEL3, 0);
        DigSupSetPulseWidth(DIG_SUP_A_CHANNEL3, 20);
        DigSupSetArm(DIG_SUP_A_CHANNEL3);
    }
}


// EOF
