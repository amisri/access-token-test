/*!
 *  @file     log_class.h
 *  @defgroup FGC:MCU:61
 *  @brief    Logging related functions for class 61.
 */

#define LOG_CLASS_GLOBALS

// Includes

#include <log_class.h>
#include <dpcls.h>
#include <get_class.h>
#include <mem.h>
#include <string.h>

// External function definitions

void LogIab(void)
{
    /*
     * The unfiltered DCCT current measurements are copied from dpram into
     * meas.ia and meas.ib by IsrMst(). This function is called at the
     * beginning of each millisecond from the MstClass() function.
     */

    memcpy(LogNextAnaSample(&log_iab), &(meas.ia), 2 * sizeof(FP32));

    if (log_iab.run_f == 0)
    {
        log_iab.samples_to_acq--;
    }

    log_iab.last_sample_time = mst.prev_time;
}

void LogIreg(void)
{
    /*
     * The IREG signals are copied into the FIP status variable buffer on
     * millisecond 1 of 20 by MstTsk() which calls this function on
     * millisecond 6 of 20 to record the values in the IREG log buffer.
     * Note that the log must start at the beginning of the page because
     * of the XGDY instruction!
     */

    float ireg_data[4];

    ireg_data[0] = dpcls.dsp.ref.i_rst;
    ireg_data[1] = I_MEAS;
    ireg_data[2] = V_REF;
    ireg_data[3] = V_MEAS;

    // Copy Iref, Imeas, Vref, Vmeas in the log buffer

    memcpy(LogNextAnaSample(&log_ireg), &(ireg_data), 4 * sizeof(FP32));

    if (log_ireg.run_f == 0)
    {
        log_ireg.samples_to_acq--;
    }

    // Time adjusted to start of 10 ms

    log_ireg.last_sample_time = mst.time10ms;
}

void LogTiming(void)
{
    static INT8S reg_mode_flag[] = "VIB ";

    if (STATE_PC == FGC_PC_CYCLING ||
        STATE_PC == FGC_PC_OFF     ||
        STATE_PC == FGC_PC_FLT_OFF)
    {
        // Warning or Fault that occurred during the previous cycle

        timing_log.dysfunction_code[timing_log.in_idx]       = dpcom.dsp.cyc.dysfunction_code;
        timing_log.dysfunction_is_a_fault[timing_log.in_idx] = dpcom.dsp.cyc.dysfunction_is_a_fault;

        // Increment log index

        timing_log.in_idx++;

        if (timing_log.in_idx >= FGC_LOG_TIMING_LEN)
        {
            timing_log.in_idx = 0;
        }

        // Transfer user last to timing_log last to validate the timing_log record

        timing_log.time         [timing_log.in_idx].unix_time = dpcom.dsp.cyc.start_time.unix_time;
        timing_log.time         [timing_log.in_idx].us_time   = dpcom.dsp.cyc.start_time.us_time;
        timing_log.stc_func_type[timing_log.in_idx]           = REF_STC_ARMED_FUNC_TYPE;
        timing_log.reg_mode_flag[timing_log.in_idx]           = reg_mode_flag[dpcls.dsp.reg.state.int32u];
        timing_log.dysfunction_code[timing_log.in_idx]        = FGC_CYC_WRN_NONE;
        timing_log.dysfunction_is_a_fault[timing_log.in_idx]  = FALSE;       

        // Keep as last line. Note that user==0 means empty record

        timing_log.user         [timing_log.in_idx]           = dpcom.dsp.cyc.user_timing;
    }
}
// EOF
