/*!
 *  @file      get_class.h
 *  @defgroup  FGC:MCU:61
 *  @brief     Class specific get functions
 */

#ifndef GET_CLASS_H
#define GET_CLASS_H

#ifdef GET_CLASS_GLOBALS
#define GET_CLASS_VARS_EXT
#else
#define GET_CLASS_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <cmd.h>
#include <property.h>

// Constants

#define FGC_PM_BUF_LEN  FGC_PM_BUF_LEN_61
#define SYM_LST_SPY     sym_lst_61_spy

// External structures, unions and enumerations

typedef struct meas_vars
{
    FP32    ia;          // Unfiltered 1kHz measurement of DCCT A
    FP32    ib;          // Unfiltered 1kHz measurement of DCCT B
} meas_vars_t;

// External variable definitions

GET_CLASS_VARS_EXT meas_vars_t meas;  // IA/B FIFO variables

/*!
 * Displays analogue measurement properties - FIFO action with binary.
 *
 * This get function only returns data for commands via the WorldFIP with the BIN
 * get option set.
 */
INT16U GetMeas(struct cmd * c, struct prop * p);

#endif

// EOF
