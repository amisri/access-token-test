/*!
 *  @file      setif_class.h
 *  @defgroup  FGC:MCU:61
 *  @brief     Class specific Setif command functions.
 */

#ifndef SETIF_CLASS_H
#define SETIF_CLASS_H

#ifdef SETIF_CLASS_GLOBALS
#define SETIF_CLASS_VARS_EXT
#else
#define SETIF_CLASS_VARS_EXT extern
#endif

#endif

// EOF
