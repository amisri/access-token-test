/*!
 *  @file      set_class.h
 *  @defgroup  FGC:MCU:61
 *  @brief     Class specific Set command functions.
 */

#ifndef SET_CLASS_H
#define SET_CLASS_H

#ifdef SET_CLASS_GLOBALS
#define SET_CLASS_VARS_EXT
#else
#define SET_CLASS_VARS_EXT extern
#endif

#endif

// EOF
