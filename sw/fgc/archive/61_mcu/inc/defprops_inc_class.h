/*!
 *  @file      defprops_inc_class.h
 *  @defgroup  FGC:MCU:61
 *  @brief     Adds all the class-specific headers needed by defprops.h
 */

#ifndef DEFPROP_CLASS_ALL_H
#define DEFPROP_CLASS_ALL_H

#include <get_class.h>
#include <ref_class.h>

#endif

// EOF
