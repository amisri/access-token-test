/*!
 *  @file      init_class.h
 *  @defgroup  FGC3:MCU:61
 *  @brief     Class specific initialization.
 */

#ifndef INIT_CLASS_H
#define INIT_CLASS_H

#ifdef INIT_CLASS_GLOBALS
#define INIT_CLASS_VARS_EXT
#else
#define INIT_CLASS_VARS_EXT extern
#endif

#define DEFPROPS_INC_ALL

// Includes

#include <cc_types.h>
#include <defconst.h>
#include <defprops.h>
#include <dpcls.h>
#include <property.h>

// Constants

#define SYM_LST_REF sym_lst_61_ref

// External structures, unions and enumerations

struct init_dynflag_timestamp_select
{
    struct prop        *        prop;                   // Pointer to property
    enum timestamp_select       timestamp_select;       // dynamic flags
} init_dynflag_timestamp_select_t;

// External variable definitions

INIT_CLASS_VARS_EXT char devtype[]
#ifdef INIT_CLASS_GLOBALS
=
{
    "RFNA"
}
#endif
;

INIT_CLASS_VARS_EXT INT32S  conf_l[]
#ifdef INIT_CLASS_GLOBALS
=
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ CAL_DEF_INTERNAL_ADC_GAIN,
    /* 3 */ CAL_DEF_EXTERNAL_ADC_GAIN,
    /* 4 */ 0
}
#endif
;

INIT_CLASS_VARS_EXT INT16U  conf_s[]
#ifdef INIT_CLASS_GLOBALS
=
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ FGC_OP_NORMAL
}
#endif
;

INIT_CLASS_VARS_EXT INT8U  conf_b[]
#ifdef INIT_CLASS_GLOBALS
=
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ 0,
}
#endif
;

INIT_CLASS_VARS_EXT FP32    conf_f[]
#ifdef INIT_CLASS_GLOBALS
=
{
    0.0, 0.0, 0.0, 8.0, -8.0, 10.0, 0.1
}
#endif
;

INIT_CLASS_VARS_EXT FP32    conf_vs_sim[FGC_N_VS_SIM_COEFFS]
#ifdef INIT_CLASS_GLOBALS
=
{
    1.0, 0.0, 0.0, 0.0
}
#endif
;

INIT_CLASS_VARS_EXT FP32    conf_vs_delay
#ifdef INIT_CLASS_GLOBALS
    = 0.001
#endif
      ;

INIT_CLASS_VARS_EXT INT32U  log_oasis_sub
#ifdef INIT_CLASS_GLOBALS
    = 1
#endif
      ;

// CAUTION: If the property is NON-VOLATILE, its default size (numels in the
//          XML definition) must be zero for it to be initialised based on the
//          data in init_prop

INIT_CLASS_VARS_EXT struct init_prop init_prop[]
#ifdef INIT_CLASS_GLOBALS
        =
{
    // Non-volatile, non-config properties

    {   &PROP_MODE_OP,                          1,                      &conf_s[2]      },
    {   &PROP_CAL_A_ADC_INTERNAL_ERR,           3,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_INTERNAL_ERR,           3,                      &conf_f[0]      },
    {   &PROP_CAL_C_ADC_INTERNAL_ERR,           3,                      &conf_f[0]      },
    {   &PROP_CAL_D_ADC_INTERNAL_ERR,           3,                      &conf_f[0]      },
    {   &PROP_MEAS_SIM,                         1,                      &conf_l[1]      },    
    {   &PROP_ADC_INTERNAL_LAST_CAL_TIME,       1,                      &conf_l[4]      },
    {   &PROP_FGC_NAME,                         sizeof(devtype),        &devtype[0]     },
    {   &PROP_DEVICE_TYPE,                      sizeof(devtype),        &devtype[0]     },
    {   &PROP_VS_ACTIVE_FILTER,                 1,                      &conf_s[1]      },
    {   &PROP_MODE_RT,                          1,                      &conf_l[0]      },
    {   &PROP_DEVICE_CYC,                       1,                      &conf_l[0]      },
    {   &PROP_DEVICE_PPM,                       1,                      &conf_s[0]      },

    {   &PROP_CAL_A_DCCT_HEADERR,               1,                      &conf_f[0]      },
    {   &PROP_CAL_A_DCCT_ERR,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_A_DCCT_TC,                    3,                      &conf_f[0]      },
    {   &PROP_CAL_A_DCCT_DTC,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_A_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_A_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_A_ADC_INTERNAL_DTC,           3,                      &conf_f[0]      },
    {   &PROP_CAL_A_ADC_EXTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_A_ADC_EXTERNAL_ERR,           3,                      &conf_f[0]      },

    {   &PROP_CAL_B_DCCT_HEADERR,               1,                      &conf_f[0]      },
    {   &PROP_CAL_B_DCCT_ERR,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_B_DCCT_TC,                    3,                      &conf_f[0]      },
    {   &PROP_CAL_B_DCCT_DTC,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_B_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_INTERNAL_DTC,           3,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_EXTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_B_ADC_EXTERNAL_ERR,           3,                      &conf_f[0]      },

    {   &PROP_CAL_C_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_C_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_C_ADC_INTERNAL_DTC,           3,                      &conf_f[0]      },

    {   &PROP_CAL_D_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_D_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_D_ADC_INTERNAL_DTC,           3,                      &conf_f[0]      },

    {   &PROP_CAL_VREF_ERR,                     3,                      &conf_f[0]      },
    {   &PROP_CAL_VREF_TC,                      3,                      &conf_f[0]      },

    {   &PROP_VS_SIM_NUM,                       FGC_N_VS_SIM_COEFFS,    &conf_vs_sim[0] },
    {   &PROP_VS_SIM_DEN,                       FGC_N_VS_SIM_COEFFS,    &conf_vs_sim[0] },
    {   &PROP_VS_SIM_CTRL_DELAY,                1,                      &conf_vs_delay  },
    {   &PROP_VS_I_LIMIT_GAIN,                  1,                      &conf_f[0]      },

    {   &PROP_LOG_OASIS_SUBSAMPLE,              1,                      &log_oasis_sub  },
    {   &PROP_BARCODE_FGC_CASSETTE,             0,                      NULL            },
    {   &PROP_BARCODE_A_ADC22_CASSETTE,         0,                      NULL            },
    {   &PROP_BARCODE_B_ADC22_CASSETTE,         0,                      NULL            },
    {   &PROP_BARCODE_A_DCCT_HEAD,              0,                      NULL            },
    {   &PROP_BARCODE_A_DCCT_ELEC,              0,                      NULL            },
    {   &PROP_BARCODE_B_DCCT_HEAD,              0,                      NULL            },
    {   &PROP_BARCODE_B_DCCT_ELEC,              0,                      NULL            },

    // Normal properties

    {   &PROP_CAL_A_DAC,                        3,                      &conf_f[2]      },
    {   &PROP_CAL_B_DAC,                        3,                      &conf_f[2]      },
    {   &PROP_ADC_INTERNAL_TAU_TEMP,            1,                      &conf_f[6]      },
    {   &PROP_VS_SIM_INTLKS,                    1,                      &conf_s[1]      },

    {   NULL    }       // End of list
}
#endif
;

// The DF_TIMESTAMP_SELECT dynamic flag is set for all the properties in the
// following list and their children. The same list is also used in function
// CmdPrintTimestamp to read the timestamp selector.

INIT_CLASS_VARS_EXT struct init_dynflag_timestamp_select init_dynflag_timestamp_select[]
#ifdef INIT_CLASS_GLOBALS
        =
{
    {   &PROP_LOG_CYC,                          TIMESTAMP_PREVIOUS_CYCLE        },
    {   &PROP_REF_CYC_FAULT,                    TIMESTAMP_CURRENT_CYCLE         },
    {   &PROP_REF_CYC_WARNING,                  TIMESTAMP_CURRENT_CYCLE         },
    {   &PROP_LOG_OASIS,                        TIMESTAMP_LOG_OASIS             },
    {   &PROP_LOG_CAPTURE_SIGNALS,              TIMESTAMP_LOG_CAPTURE           },

    {   NULL    }       // End of list
}
#endif
;

#endif

// EOF
