/*!
 *  @file     ref_class.h
 *  @defgroup FGC3:MCU:61
 *  @brief    Reference-related functions specific to Class 61
 */

#ifndef FGC_REF_CLASS_H
#define FGC_REF_CLASS_H

#ifdef REF_CLASS_GLOBALS
#define REF_CLASS_VARS_EXT
#else
#define REF_CLASS_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <cmd.h>


REF_CLASS_VARS_EXT char ref_units_ccv
#ifdef REF_CLASS_GLOBALS
 = 'A'
#endif
 ;


REF_CLASS_VARS_EXT char  ref_units_vcv
#ifdef REF_CLASS_GLOBALS
    = 'V'
#endif
;

// External function declarations

/*!
 * This function is called when the user enters "s ref plep,..." to process the
 * plep reference setup.
 *
 * @param c Command structure.
 *
 * @retval FGC_OK_NO_RSP if no error occurred.
 */
INT16U RefPlep(struct cmd * c);

/*!
 * This function is called when the user enters "s ref openloop,..." to process
 * a trim reference function setup.
 *
 * Parameters are:
 *   s ref openloop, final
 *
 * Defaults are: final reference now
 *
 * @param c Command structure.
 *
 * @retval FGC_OK_NO_RSP if no error occurred.
 */
INT16U RefOpenloop(struct cmd * c);

/*!
 * This function is called when the user enters "s ref ltrim|ctrim,..." to
 * process a trim reference function setup.
 *
 * Parameters are:
 *   s ref func, final, period
 *
 * Defaults are: final  reference now period 0 (as fast as possible)
 *
 * @param c Command structure.
 *
 * @retval FGC_OK_NO_RSP if no error occurred.
 */
INT16U RefTrim(struct cmd * c);

/*!
 * This function is called when the user enters
 * "s ref steps|square|sine|cosine,..." to process the test reference function
 * setup.
 *
 * Parameters are:
 *   s ref func, amplitude, num_cycles, period
 *
 * Defaults are:     amplitude   1.0
 *                   num_cycles  1.0
 *                   period      1.0
 *
 * @param c Command structure.
 *
 * @retval FGC_OK_NO_RSP if no error occurred.
 */
INT16U RefTest(struct cmd * c);

#endif

// EOF
