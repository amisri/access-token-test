/*!
 *  @file      fbs_class.h
 *  @defgroup  FGC:MCU:61
 *  @brief     Handlers to published data variables.
 */


#ifndef FBS_CLASS_H
#define FBS_CLASS_H

#ifdef FBS_CLASS_GLOBALS
#define FBS_CLASS_VARS_EXT
#else
#define FBS_CLASS_VARS_EXT extern
#endif

// Includes

#include <dpcls.h>
#include <fbs.h>

// External structures, unions and enumerations

#define FAULTS          fbs.u.fgc_stat.class_data.c61.st_faults
#define WARNINGS        fbs.u.fgc_stat.class_data.c61.st_warnings
#define ST_LATCHED      fbs.u.fgc_stat.class_data.c61.st_latched
#define ST_UNLATCHED    fbs.u.fgc_stat.class_data.c61.st_unlatched
#define STATE_PLL       fbs.u.fgc_stat.class_data.c61.state_pll
#define STATE_OP        fbs.u.fgc_stat.class_data.c61.state_op
#define STATE_VS        fbs.u.fgc_stat.class_data.c61.state_vs
#define STATE_PC        fbs.u.fgc_stat.class_data.c61.state_pc
#define ST_MEAS_A       fbs.u.fgc_stat.class_data.c61.st_adc_a
#define ST_MEAS_B       fbs.u.fgc_stat.class_data.c61.st_adc_b
#define ST_MEAS_C       fbs.u.fgc_stat.class_data.c61.st_adc_c
#define ST_MEAS_D       fbs.u.fgc_stat.class_data.c61.st_adc_d
#define ST_DCCT_A       fbs.u.fgc_stat.class_data.c61.st_dcct_a
#define ST_DCCT_B       fbs.u.fgc_stat.class_data.c61.st_dcct_b
#define I_ERR_MA        fbs.u.fgc_stat.class_data.c61.i_err_ma
#define I_DIFF_MA       fbs.u.fgc_stat.class_data.c61.i_diff_ma
#define EVENT_GROUP     fbs.u.fgc_stat.class_data.c61.event_group
#define I_EARTH_PCNT    fbs.u.fgc_stat.class_data.c61.i_earth_pcnt
#define REF             fbs.u.fgc_stat.class_data.c61.i_ref
#define I_REF           fbs.u.fgc_stat.class_data.c61.i_ref
#define V_REF           fbs.u.fgc_stat.class_data.c61.v_ref
#define I_MEAS          fbs.u.fgc_stat.class_data.c61.i_meas
#define V_MEAS          fbs.u.fgc_stat.class_data.c61.v_meas

#define REG_STATE               dpcls.dsp.reg.state.byte[3]
#define B_MEAS                  dpcls.dsp.meas.b

#endif

// EOF
