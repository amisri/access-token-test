/*!
 *  @file      events_class.c
 *  @brief     File with the class-specific processing of events.
 */

#ifndef FGC_EVENTS_CLASS_H
#define FGC_EVENTS_CLASS_H

#ifdef EVENTS_CLASS_GLOBALS
#define EVENTS_CLASS_VARS_EXT
#else
#define EVENTS_CLASS_VARS_EXT extern
#endif


#endif  // FGC_EVENTS_CLASS_H end of header encapsulation

// EOF
