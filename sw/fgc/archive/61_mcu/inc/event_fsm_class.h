/*!
 *  @file      event_fsm_class.h
 *  @defgroup  FGC:MCU:61
 *  @brief     No functionality declared in this header.
 */

#ifndef FGC_EVENT_FSM_CLASS_H
#define FGC_EVENT_FSM_CLASS_H

#ifdef EVENT_FSM_CLASS_GLOBALS
#define EVENT_FSM_CLASS_VARS_EXT
#else
#define EVENT_FSM_CLASS_VARS_EXT extern
#endif

#endif

// EOF
