/*!
 *  @file     log_class.h
 *  @defgroup FGC:MCU:61
 *  @brief    Logging related functions for class 61.
 */

#ifndef LOG_CLASS_H
#define LOG_CLASS_H

#ifdef LOG_CLASS_GLOBALS
#define LOG_CLASS_VARS_EXT
#else
#define LOG_CLASS_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <defprops.h>
#include <fgc_log.h>
#include <log_event.h>
#include <log.h>
#include <memmap_mcu.h>


#define FGC_LAST_LOG log_ireg


// External function declarations

/*!
 * This function stores one sample in the IAB log.
 */
void LogIab(void);

/*!
 * This function stores one sample in the ILEADS log.
 */
void LogIleads(void);

/*!
 * This function stores one sample in the IREG log.
 */
void LogIreg(void);

/*!
 * This function is filling the timing log at the beginning of each cycle
 * provided DEVICE_CYC is ENABLED and start_cycle_f is set
 */
void LogTiming(void);

// External variable definitions

// These properties are logged in logEvent whenever they change.
// At the same time they are also published if needed (no need to
// explicitly call PubProperty).

LOG_CLASS_VARS_EXT struct log_evt_prop log_evt_prop[]
#ifdef LOG_CLASS_GLOBALS
            =     // Property pointer                Old value       +-Property Name----------+
{
    {   &PROP_STATE_PC,                 0xFFFF,         "STATE.PC"                      },
    {   &PROP_STATE_OP,                 0xFFFF,         "STATE.OP"                      },
    {   &PROP_CONFIG_STATE,             0xFFFF,         "CONFIG.STATE"                  },
    {   &PROP_CONFIG_MODE,              0xFFFF,         "CONFIG.MODE"                   },
    {   &PROP_FGC_PLL_STATE,            0xFFFF,         "FGC_PLL.STATE"                 },
    {   &PROP_VS_STATE,                 0xFFFF,         "VS.STATE"                      },
    {   &PROP_LOG_PM_TRIG,              0x0000,         "LOG.PM.TRIG"                   },
    {   &PROP_DIG_STATUS,               0x0000,         "DIG.STATUS"                    },
    {   &PROP_DIG_IP_DIRECT,            0x0000,         "DIG.IP_DIRECT"                 },
    {   &PROP_STATUS_FAULTS,            0x0000,         "STATUS.FAULTS"                 },
    {   &PROP_STATUS_WARNINGS,          0x0000,         "STATUS.WARNINGS"               },
    {   &PROP_STATUS_ST_LATCHED,        0x0000,         "STATUS.ST_LATCHED"             },
    {   &PROP_STATUS_ST_UNLATCHED,      0x0000,         "STATUS.ST_UNLATCHED"           },
    {   &PROP_ADC_STATUS_A,             0x0000,         "ADC.STATUS.A"                  },
    {   &PROP_ADC_STATUS_B,             0x0000,         "ADC.STATUS.B"                  },
    {   &PROP_ADC_STATUS_C,             0x0000,         "ADC.STATUS.C"                  },
    {   &PROP_ADC_STATUS_D,             0x0000,         "ADC.STATUS.D"                  },
    {   &PROP_DCCT_A_STATUS,            0x0000,         "DCCT.A.STATUS"                 },
    {   &PROP_DCCT_B_STATUS,            0x0000,         "DCCT.B.STATUS"                 },
    {   &PROP_DIG_COMMANDS,             0x0000,         "DIG.COMMANDS"                  },
    {   &PROP_REF_EVENT_GROUP,          0x0000,         "REF.EVENT_GROUP"               },
    {   &PROP_FGC_SECTOR_ACCESS,        0x0000,         "FGC.SECTOR_ACCESS"             },
    {   NULL    },
}
#endif
;

// FGC_SELF_PM_REQ must be cleared before the 10 seconds of gateway timeout
// 8192 samples at 1ms : 8.192 seconds
// post trigger is at 8192 / 2 = 4096 samples (4.096 seconds)

LOG_CLASS_VARS_EXT struct log_ana_vars log_iab
#ifdef LOG_CLASS_GLOBALS
        =
{
    SRAM_LOG_IAB_32,                            // Buffer base address
    SRAM_LOG_IAB_W,                             // Buffer size in words
    STP_IAB,                                    // Sym_idx for log type
    FGC_LOG_IAB_LEN,                            // Number of samples in the log
    FGC_LOG_IAB_LEN / 2,                        // Number of post trig samples
    2,                                          // Number of signals per sample
    8,                                          // Number of bytes per sample
    -2681,                                      // Wait time (ms) (fudge factor - no real delay)
    1,                                          // Period in milliseconds (1kHz)
    { 0, 0 },                                   // Signal info
    { "I_A:A", "I_B:A" },                       // Signal labels and units
}
#endif
;

// FGC_SELF_PM_REQ must be cleared before the 10 seconds of gateway timeout
// 3840 samples at 10ms : 38.4 seconds
// post trigger is at 3840 / 6 = 640 samples (6.40 seconds)

LOG_CLASS_VARS_EXT struct log_ana_vars log_ireg
#ifdef LOG_CLASS_GLOBALS
        =
{
    SRAM_LOG_IREG_32,                           // Buffer base address
    SRAM_LOG_IREG_W,                            // Buffer size in words
    STP_IREG,                                   // Sym_idx for log type
    FGC_LOG_IREG_LEN,                           // Number of samples in the log
    FGC_LOG_IREG_LEN / 6,                       // Number of post trig samples
    4,                                          // Number of signals per sample
    16,                                         // Number of bytes per sample
    0,                                          // Wait time (ms)
    10,                                         // Period in milliseconds (100Hz)
    {
        FGC_LOG_SIG_INFO_STEPS, 0,                // Signal info - reference signal have step interpolation
        FGC_LOG_SIG_INFO_STEPS, 0
    },
    {
        "I_REF_RST:A", "I_MEAS:A",                    // Signal labels and units
        "V_REF:V",     "V_MEAS:V"
    },
}
#endif
;

#endif

// EOF

