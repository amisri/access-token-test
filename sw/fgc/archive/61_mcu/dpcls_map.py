#!/usr/bin/python
#
# Copyright (C) 2012 CERN.
#
# DESCRIPTION:
#
# CONTACT: main author, Pierre DEJOUE.
#

import sys, os

# Somewhat inelegant way to import the struct_map.py module
scripts_rel_path = '../../../utils/scripts'
sys.path.append(os.path.join(*scripts_rel_path.split('/')))
from struct_map import *
sys.path.pop()

# Class used to parse dpcls
class struct_map_dpcls(struct_map):

    def __init__(self):
        self.input_source_file = "./obj/main.i"
        self.input_info_file   = "dpcls_map.info.csv"

        self.output_file_flag  = True
        self.output_file       = "dpcls.map"

        self.struct_name       = "dpcls"

        self.start_address_mcu = 0x04002000
        self.start_address_dsp = 0x90002000

        self.dsp_c32_flag      = False                                          # The C32 DSP addresses 32 bits of data.
                                                                                # The addr_offset therefore needs to be divided by four.

if __name__ == "__main__":

    mapping = struct_map_dpcls()
    mapping.parse()
