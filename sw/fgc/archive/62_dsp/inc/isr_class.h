/*!
 *  @file      isr_class.h
 *  @defgroup  FGC:DSP:62
 *  @brief     Class 62 specific declarations for ISR.
 */

#ifndef FGC_ISR_CLASS_H
#define FGC_ISR_CLASS_H

#ifdef ISR_GLOBALS
#define ISR_CLASS_VARS_EXT
#else
#define ISR_CLASS_VARS_EXT extern
#endif

// Includes

#endif

// EOF
