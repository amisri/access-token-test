/*!
 *  @file      isr_class.c
 *  @defgroup  FGC:DSP:62
 *  @brief     Sends the reference to the fast pulse converter once the
 *             new event has been received.
 */

#include <stdio.h>
#include <string.h>

#include <isr_class.h>
#include <isr.h>
#include <dpcls.h>
#include <rtcom.h>
#include <props.h>
#include <crate.h>
#include <spivs.h>
#include <adc.h>
#include <cyc.h>
#include <meas.h>
#include <pc_state.h>


// Constants

static uint8_t const MAX_NUM_SPIVS_FAILS  =     2;


// Internal structures, unions and enumerations

typedef void (*ref_func_ptr)(float);

typedef struct isr_class_local
{
    ref_func_ptr  send_ref_func;
    float        *meas;
} isr_class_local_t;


// @TODO these should not be here

static float     acq_pulses[FGC_N_ACQ_PULSES] = { 0.0 };
static uint32_t  acq_pulses_idx = 0;



// Internal variable definition

static isr_class_local_t local =
{
    .send_ref_func = NULL,
};


// Internal function declarations

static void IsrSendRefSiramatrix(float pulse_ref);
static void IsrSendDspCard      (float pulse_ref);
static void IsrSendRefMididiscap(float pulse_ref);
static void IsrSendRefModulator (float pulse_ref);
static void IsrReport(void);


// Internal function definitions

static void IsrSendRefSiramatrix(float pulse_ref)
{
    /*
     * The value sent is a 32 bits signed value. The range is as follows:
     *
     * 7FFF FFFF   + Nominal current     REF.PULSE.NOMINAL
     *
     * 0000 0000                           0.0
     * FFFF FFFF                          -0.....
     *
     * 8000 0000   - Nominal current    -REF.PULSE.NOMINAL
     */

    static uint32_t prev_ref_sent = 0;
    static int8_t   num_fails     = 0;

    uint32_t ref_read;
    uint32_t ref_value_i;

    // Convert the floating point value to a 32 bit signed integer

    ref_value_i = (uint32_t)(((float) INT32_MAX / dpcls->mcu.limits.pulse.nominal) * pulse_ref + 0.5);

    spivsSend(&ref_value_i, 1);

    spivsWait(1);

    spivsRead(&ref_read, 1);

    // An error in any of the functions above will be picked up by the
    // condition below.

    // Verify the transmission over the SPIVS bus. The Siramatrix might round
    // down the value sent, which will result in a failure.

    if (spivsValidate((uint32_t const *)&prev_ref_sent, &ref_read, 1) == SPIVS_INVALID)
    {
        num_fails++;

        if (num_fails > MAX_NUM_SPIVS_FAILS)
        {
            Set(rtcom.st_latched, FGC_LAT_SPIVS_FLT);
        }
    }
    else
    {
        num_fails = 0;
    }

    prev_ref_sent = ref_value_i;
}



static void IsrSendDspCard(float pulse_ref)
{
    /*
     * The values sent to the DSP card are the gain and the duration
     */

    static float   tx_values[2][2];
    static uint8_t indx      =  0;
    static int8_t  num_fails = -1;

    // Gain, duration

    tx_values[indx][0] = pulse_ref;
    tx_values[indx][1] = dpcls->mcu.ref.pulse.duration[dpcls->mcu.ref.pulse.user];

    spivsSend((uint32_t *)tx_values[indx], 2);

    uint32_t rx_values[2];

    spivsRead(rx_values, 2);

    indx ^= 1;

    if (   dpcom->mcu.state_op == FGC_OP_NORMAL
        && spivsValidate((uint32_t const *)tx_values[indx], rx_values, 2) == SPIVS_INVALID)
    {
        // The fails must be consecutive

        if (++num_fails > MAX_NUM_SPIVS_FAILS)
        {
            Set(rtcom.st_latched, FGC_LAT_SPIVS_FLT);
        }
    }
    else
    {
        num_fails = 0;
    }    
}



static void IsrSendRefModulator(float pulse_ref)
{
    // Apply nominal calibration gain from VS. The modulators are special in
    // that the reference value sent must be positive, event when the user
    // reference is negative.

    pulse_ref = -(pulse_ref - property.vs.offset) / property.vs.gain;

    IsrDacSet(0, pulse_ref);
}



static void IsrSendRefMididiscap(float pulse_ref)
{
    // The reference value is in Amps. The gain applied must be that of the DCCT_A.
    // The reference is always positive.

    pulse_ref = fabs(pulse_ref / property.dcct.gain[FGC_DCCT_SELECT_A]);

    IsrDacSet(1, pulse_ref);
}



static void IsrReport(void)
{
    // Report signal values

    dpcls->dsp.meas.i = meas.i;
    dpcls->dsp.meas.v = meas.v;
    dpcls->dsp.meas.v_capa = meas.v_capa;

    // Report every 20 ms

    if (rtcom.start_of_ms20_f)
    {
        rtcom.warnings |= dpcom->mcu.warnings;

        // Send status data to MCU

        dpcom->mcu.warnings = 0;

        dpcom->dsp.faults     = rtcom.faults;
        dpcom->dsp.st_latched = rtcom.st_latched;
        dpcom->dsp.warnings   = rtcom.warnings | cyc.stretched_warnings;

        rtcom.faults     = 0;
        rtcom.st_latched = 0;

        if (adc_current_a != NULL)
        {
            dpcls->dsp.meas.st_dcct[0] = adc_current_a->st_dcct;
        }

        dpcls->dsp.meas.st_meas[0] = adc_chan[0].st_meas;
        dpcls->dsp.meas.st_meas[1] = adc_chan[1].st_meas;
        dpcls->dsp.meas.st_meas[2] = adc_chan[2].st_meas;
        dpcls->dsp.meas.st_meas[3] = adc_chan[3].st_meas;

        // If "S MEAS.STATUS RESET"

        if (dpcls->mcu.meas.status_reset_f)
        {
            dpcls->mcu.meas.status_reset_f = FALSE;

            adc_chan[0].st_meas = 0;
            adc_chan[1].st_meas = 0;
            adc_chan[2].st_meas = 0;
            adc_chan[3].st_meas = 0;
        }
        else
        {
            // Clear all but CAL_FAILED

            adc_chan[0].st_meas &= FGC_ADC_STATUS_CAL_FAILED;
            Set(adc_chan[0].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
            Set(adc_chan[0].st_meas, FGC_ADC_STATUS_V_MEAS_OK);

            adc_chan[1].st_meas &= FGC_ADC_STATUS_CAL_FAILED;
            Set(adc_chan[1].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
            Set(adc_chan[1].st_meas, FGC_ADC_STATUS_V_MEAS_OK);

            adc_chan[2].st_meas &= FGC_ADC_STATUS_CAL_FAILED;
            Set(adc_chan[2].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
            Set(adc_chan[2].st_meas, FGC_ADC_STATUS_V_MEAS_OK);

            adc_chan[3].st_meas &= FGC_ADC_STATUS_CAL_FAILED;
            Set(adc_chan[3].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
            Set(adc_chan[3].st_meas, FGC_ADC_STATUS_V_MEAS_OK);
        }


        // Update Izero and Ilow flags

        dpcls->dsp.meas.i_zero_f = true;
        dpcls->dsp.meas.i_low_f  = true;
    }
}



// Class specific function definitions

void IsrClassInit(void)
{
    switch (crateGetType())
    {
        case FGC_CRATE_TYPE_MIDIDISCAP:
            local.send_ref_func = &IsrSendRefMididiscap;
            local.meas = &meas.i;
            break;

        case FGC_CRATE_TYPE_MAXIDISCAP:
            local.send_ref_func = &IsrSendRefSiramatrix;
            local.meas    = &meas.i;
            break;

        case FGC_CRATE_TYPE_MEGADISCAP:
            local.send_ref_func = &IsrSendRefSiramatrix;
            local.meas = &meas.i;
            break;

        case FGC_CRATE_TYPE_DSP_IGBT:
            local.send_ref_func = &IsrSendDspCard;
            local.meas = &meas.i;
            break;            

        case FGC_CRATE_TYPE_HMINUS_DISCAP:
            local.send_ref_func = &IsrSendRefSiramatrix;
            local.meas = &meas.v;
            break;

        case FGC_CRATE_TYPE_KLYSTRON_MOD:
            local.send_ref_func = &IsrSendRefModulator;
            local.meas = &meas.v;
            break;

        case FGC_CRATE_TYPE_DEVELOPMENT: // Fall through
        case FGC_CRATE_TYPE_RECEPTION:
            // For development and reception test send the value via
            // SPIVS and us the current as the measurement

            local.send_ref_func = &IsrSendRefSiramatrix;
            local.meas = &meas.i;
            break;

        default:
            break;
    }

    // Take the current measurement value and not the filtered one. For further
    // details look into meas.c::MeasSelect()

    reg.hi_speed_f = TRUE;
}



void IsrClassMain(void)
{
    static BOOLEAN prev_b2_state = FALSE;

    if (dpcls->mcu.ref.pulse.ref_avail_f)
    {
        local.send_ref_func(dpcls->mcu.ref.pulse.ref);

        dpcls->mcu.ref.pulse.ref_avail_f = FALSE;

        ref.func_type = FGC_REF_NONE;
    }

    /*
     * For now the measurement is acquired at the event time, triggered by the
     * rising edge of B2. When ANA103 is available, this must be done at the
     * end of the pulse to traverse the fast acquisition measurements and pick
     * the right one for the measurement and calculate the absolute error.
     */

    if (!prev_b2_state &&
        Test(DIG_SUP_P, DIG_SUP_INP_B2_MASK16))
    {
        property.meas.acq_value_avg_accum += *local.meas;
        property.meas.acq_value_avg_cnt++;

        if (acq_pulses_idx < FGC_N_ACQ_PULSES)
        {
            acq_pulses[acq_pulses_idx] = *local.meas;
            acq_pulses_idx++;
        }       

        dpcls->dsp.meas.pulse.meas_i       = meas.i;
        dpcls->dsp.meas.pulse.meas_v       = meas.v;
        dpcls->dsp.meas.pulse.meas_avail_f = TRUE;
    }

    prev_b2_state = Test(DIG_SUP_P, DIG_SUP_INP_B2_MASK16);

    if (cyc.start_cycle_f == true)
    {
        property.meas.acq_value[cyc.prev_user] = property.meas.acq_value_avg_accum / property.meas.acq_value_avg_cnt;
        property.meas.acq_value_avg_accum = 0.0;
        property.meas.acq_value_avg_cnt = 0;

        acq_pulses_idx = 0;

        // Transfer the pulses for the cucle user to the property

        memcpy(&property.meas.acq_pulses[cyc.prev_user * FGC_N_ACQ_PULSES], acq_pulses, sizeof(acq_pulses));

        // Zero the pulses for the next cycle

        memset(acq_pulses, 0, sizeof(acq_pulses));


        dpcls->dsp.log.pulse_acq_user = cyc.prev_user;
    }

    IsrReport();
}

// EOF
