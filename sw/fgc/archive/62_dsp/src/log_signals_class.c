/*!
 *  @file      log_signals_class.c
 *  @defgroup  FGC3:DSP:62
 *  @brief     File providing functionality for logging signals specific
 *             to Class 62.
 */

#define LOG_SIGNALS_CLASS_GLOBALS
#define LOG_SIGNALS_CLASS_GLOBALS_FAR

// Includes

#include <log_signals_class.h>
#include <meas.h>
#include <memmap_dsp.h>
#include <dpcls.h>


// External function definitions

void LogSignalClassCaptureLog(uint16_t n_sample)
{
    log_signal_capture_buf_t * buf = &log_capture_buffer[n_sample];

    buf->ref     = dpcls->mcu.ref.pulse.ref;
    buf->i_meas  = meas.i;
    buf->v_meas  = meas.v;
    buf->v_capa  = meas.v_capa;
    buf->pulse_b0  = Test(DIG_SUP_P, DIG_SUP_INP_B0_MASK16);
    buf->pulse_b1  = Test(DIG_SUP_P, DIG_SUP_INP_B1_MASK16);
    buf->pulse_b3  = Test(DIG_SUP_P, DIG_SUP_INP_B3_MASK16);
    buf->pulse_acq = Test(DIG_SUP_P, DIG_SUP_INP_B2_MASK16);
}

// EOF
