/*!
 *  @file      bgp_class.c
 *  @defgroup  FGC:DSP:62
 *  @brief     Background processing functions specific to class 61.
 */

#define BGP_CLASS_GLOBALS

// Includes

#include <bgp_class.h>
#include <cc_types.h>
#include <fgc_errs.h>
#include <bgp.h>


void BgpRefChange(INT32U user)
{

}

// External function definitions

INT32U BgpInitNone(INT32U reg_mode, INT32U user)
{
    return (FGC_OK_NO_RSP);
}

// Platform/class specific function definition

bgp_check_Iref_func BgpGetCheckIref(void)
{
    return NULL;
}

// EOF
