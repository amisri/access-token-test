/*!
 *  @file      state_class.c
 *  @defgroup  FGC3:DSP:62
 *  @brief     DSP PC state declaration
 */

// Includes

#include <state.h>
#include <cc_types.h>
#include <defconst.h>
#include <cyc.h>
#include <main.h>

// Internal function declarations

// Internal function definitions

static void StateTC(void)
{
    CycEnter();
}

// Platform/class specific function definition

void StatePc(INT32U new_state_pc)
{
    static void (*state_func[])(void) =         // Ref gen functions
    {
        0,                          //  0    FLT_OFF
        0,                          //  1    OFF
        0,                          //  2    FLT_STOPPING
        0,                          //  3    STOPPING
        0,                          //  4    STARTING
        0,                          //  5    SLOW_ABORT
        0,                          //  6    TO_STANDBY
        0,                          //  7    ON_STANDBY
        0,                          //  8    IDLE
        StateTC,                    //  9    TO_CYCLING
        0,                          // 10    ARMED
        0,                          // 11    RUNNING
        0,                          // 12    ABORTING
        0,                          // 13    CYCLING
        0,                          // 14    POL_SWITCHING
        0,                          // 15    BLOCKING
        0,                          // 16    ECONOMY
        0,                          // 17    DIRECT
    };

    // If leaving CYCLING then run CycExit()

    if (state_pc == FGC_PC_CYCLING)
    {
        CycExit();
    }

    // Run state function for new PC state, if defined

    if (state_func[new_state_pc])
    {
        state_func[new_state_pc]();
    }

    state_pc = new_state_pc;
}

// EOF
