/*------------------------------------------------------------------------------*\
 File:		62\link.cmd

 Purpose:	Class 62 DSP Program Linker file for FGC3 Hardware

\*------------------------------------------------------------------------------*/

-e _c_int00  /* for the command line : --entry_point _c_int00 inside rts6700.lib */
-heap  400h  /* for the command line : --heap_size 0x400 */
-stack 400h  /* for the command line : --stack_size 0x400 */

/* include libraries */
/* -l rts6700.lib */

/****************************************************************************/
/*  Specify the Memory Configuration                                        */
/****************************************************************************/

MEMORY
{
/* 384Kb internal ROM */

    IROM_BOOT     (RX)   : origin = 00000000h,   length = 00020000h
    IROM_DSPLIB   (RX)   : origin = 00020000h,   length = 0000C000h
    IROM_FASTRTS  (RX)   : origin = 0002C000h,   length = 00004000h
    IROM_BIOS     (RX)   : origin = 00030000h,   length = 00030000h

/* 256Kb internal RAM (no cache in use) */

    IRAM_BOOT     (RWXI) : origin = 10000000h,   length = 000003FCh /* Boot program */
    IRAM_START    (RWXI) : origin = 100003FCh,   length = 00000004h /* Adress of the entry point function for the Boot or Main */
    IRAM_VEC      (RWXI) : origin = 10000400h,   length = 00000200h
    IRAM          (RWXI) : origin = 10000600h,   length = 0003fa00h

/* IMPORTANT: do not change the origin address in IRAM_START (100003FC). This value is used  */
/*            in mot2bin.pl to discern if the binary is the dsp boot loder or the Main/Boot. */

/* 64Mb external SDRAM, linked with CE0 */
/* 64 MB SDRAM is divided into three sections: */
/* 4  MB: code that is executed from this memory due to lack of space in MCU */
/* 4  MB: temporarily store the RegFGC3 binaries before sending them to the RegFGC3 cards */
/* 59 MB: LOG signals from the liblog library */

    SDRAM_CODE    (RWXI) : origin = 80000000h,   length = 00100000h
    SDRAM_REGFGC3 (RWXI) : origin = 80100000h,   length = 00400000h
    SDRAM_LOG     (RWXI) : origin = 80500000h,   length = 03B00000h

/* 16Kb external DPRAM, linked with CE1 */
    PLD_DPRAM     (RWXI) : origin = 90000000h,   length = 00004000h

/*  Expansion     (RWXI) : origin = 90100000h,   length = 00008000h /* external expansion range, 32kB */
    ExtCE2        (RWXI) : origin = 90200000h,   length = 00008000h /* Expansion CE2-space */
    ExtCE3        (RWXI) : origin = 90280000h,   length = 00008000h /* Expansion CE3-space */
    ExtCE4        (RWXI) : origin = 90300000h,   length = 00008000h /* Expansion CE4-space */

/* from the linker example */
   EXT2:   origin = 02000000h,   length = 01000000h 
}


/****************************************************************************/
/*  Specify the Output Sections                                             */
/****************************************************************************/


SECTIONS
{
    /* The bootloader needs the entry point of the program */
    /* This is done in start.asm which defines myStartSection */
    /* with this information */

    .dspStartSection   > IRAM_START
    {
       *(.dspStartSection)
       code_destination = .;
    }
    
    .vector		: load > IRAM_VEC
    {            	
	   *(.vector)
    }
    .text		: load > IRAM
    {
    	*(.text)
    }

    /*-----------------------------------------------------------------------*/
    /* In EABI (--abi=eabi) mode, the compiler generates the following       */
    /* sections that are accessed using the near DP addressing (DP[15-bit]): */
    /*   .neardata - initialized read-write data                             */
    /*   .rodata   - initialized read-only  data                             */
    /*   .bss      - uninitialized data                                      */
    /* These sections should be co-located so that they are placed within    */
    /* DP range.                                                             */
    /*                                                                       */
    /* In COFF (--abi=coffabi) mode, the compiler only generates .bss section*/
    /*-----------------------------------------------------------------------*/
    .ext_log            : load > SDRAM_LOG
    {
        */ppm.obj(.far)               // For ppm[]                (~512kB)
		*/log_signals_class.obj(.far) // For log_capture_buffer[] (~256kB)
    }
    
    GROUP (NEARDP)
    {
       .neardata   /* ELF only */
       .rodata     /* ELF only */
       .bss        /* COFF & ELF */
       {
           	*(.bss)
       }
       .int_far
       {
                *(.far)
       }
    } > IRAM

    .cinit		: load > IRAM
    .pinit		: load > IRAM
    .cio		: load > IRAM
    .const		: load > IRAM
    .switch		: load > IRAM
    .sysmem		: load > IRAM
    .tables		: load > IRAM
    .stack		: load > IRAM (HIGH)
    .ppdata		: load > EXT2 
}
