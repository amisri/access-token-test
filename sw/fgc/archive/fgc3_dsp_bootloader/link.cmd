-e _bootEntryPoint /* for the command line : --entry_point _c_int00 inside rts6700.lib */
-heap  400h        /* for the command line : --heap_size 0x400 */
-stack 400h        /* for the command line : --stack_size 0x400 */

/****************************************************************************/
/*  Specify the Memory Configuration                                        */
/****************************************************************************/

MEMORY
{
/* 384Kb internal ROM */

    IROM_BOOT     (RX)   : origin = 00000000h,   length = 00020000h
    IROM_DSPLIB   (RX)   : origin = 00020000h,   length = 0000C000h
    IROM_FASTRTS  (RX)   : origin = 0002C000h,   length = 00004000h
    IROM_BIOS     (RX)   : origin = 00030000h,   length = 00030000h

/* 256Kb internal RAM (no cache in use) */

/* original definition
    IRAM_BOOT     (RWXI) : origin = 10000000h,   length = 00001000h
    IRAM_BIOS     (RWXI) : origin = 10001000h,   length = 00000B00h
    IRAM_RESERVED (RWXI) : origin = 10001B00h,   length = 00000100h /* Reserved only if using ROMed applications
    IRAM_STACK    (RWXI) : origin = 10001C00h,   length = 00000800h
    IRAM          (RWXI) : origin = 10002400h,   length = 0003DC00h
*/
/* FGC 3 definition */
    IRAM_BOOT     (RWXI) : origin = 10000000h,   length = 00000400h /* 1kByte - used by flash boot mode*/
    IRAM_VEC      (RWXI) : origin = 10000400h,   length = 00000200h
    IRAM          (RWXI) : origin = 10000600h,   length = 0003fa00h

/* 16Mb external SDRAM, linked with CE0 */
    SDRAM         (RWXI) : origin = 80000000h,   length = 01000000h

/* 16Kb external DPRAM, linked with CE1 */
    PLD_DPRAM     (RWXI)   : origin = 90000000h,   length = 00004000h

/*  Expansion     (RWXI) : origin = 90100000h,   length = 00008000h /* external expansion range, 32kB */
    ExtCE2        (RWXI) : origin = 90200000h,   length = 00008000h /* Expansion CE2-space */
    ExtCE3        (RWXI) : origin = 90280000h,   length = 00008000h /* Expansion CE3-space */
    ExtCE4        (RWXI) : origin = 90300000h,   length = 00008000h /* Expansion CE4-space */

/* from the linker example */
   EXT2:   origin = 02000000h,   length = 01000000h 
}


/****************************************************************************/
/*  Specify the Output Sections                                             */
/****************************************************************************/


SECTIONS
{
    .myBootSection	: load > IRAM_BOOT
    {
    	*(.myBootSection)
    	code_size = .;		/* to get the size as a global variable */
    }

    /*-----------------------------------------------------------------------*/
    /* In EABI (--abi=eabi) mode, the compiler generates the following       */
    /* sections that are accessed using the near DP addressing (DP[15-bit]): */
    /*   .neardata - initialized read-write data                             */
    /*   .rodata   - initialized read-only  data                             */
    /*   .bss      - uninitialized data                                      */
    /* These sections should be co-located so that they are placed within    */
    /* DP range.                                                             */
    /*                                                                       */
    /* In COFF (--abi=coffabi) mode, the compiler only generates .bss section*/
    /*-----------------------------------------------------------------------*/
    GROUP (NEARDP)
    {
       .neardata   /* ELF only */
       .rodata     /* ELF only */
    } > IRAM

    .cinit      : load > IRAM
    .pinit      : load > IRAM
    .cio        : load > IRAM
    .const      : load > IRAM
    .switch     : load > IRAM
    .sysmem     : load > IRAM
    .far        : load > IRAM
    .tables     : load > IRAM
    .stack		: load > IRAM (HIGH)
    .fardata	: load > EXT2   /* ELF only */
    .ppdata		: load > EXT2 
}
