/*!
 *  @file     bootLoader.c
 *  @brief    TMS320C6727B hardware setup
 *
 *    Texas   DSP:    TMS320c6727b
 *            Family: Texas TMS 320 C 6000.
 *            Code:   TMS 320 C 6727B GDH 300
 *
 *    This is the hardware initialisation done by the boot loader
 *
 *    WARNING: this code must not be greater than 1Kb!!
 *
 *    DSP sequence:
 *        1) Startup with internal boot.
 *        2) From the config pins at startup (#HCS=1,SPI0SOMI=0,SPI0SIMO=1,SPI0CLK=0)
 *           (parallel flash mode) it knows that must look for data at 9000 0000.
 *        3) Read the magic transfer byte (at 9000 0000) to know the parallel size (16 bits in our case)
 *        4) Copy 1Kb from 9000 0000 to 1000 0000
 *        5) Jump to 1000 0004  Detailed description
 */

// Includes

#include <stdint.h>
#include <stdbool.h>

#include <cc_types.h>

#include "fgc3/inc/common.h"



// Constants

// from file hw_tms320c6727.h
// TMS320C6727 peripheral definitions

#define PLL_PID         ( 0x41000000 + 0x000 )  // identification
#define PLL_CSR         ( 0x41000000 + 0x100 )  // control/status
#define PLL_M           ( 0x41000000 + 0x110 )  // multiplier 1...25
#define PLL_DIV0        ( 0x41000000 + 0x114 )
#define PLL_DIV1        ( 0x41000000 + 0x118 )
#define PLL_DIV2        ( 0x41000000 + 0x11C )
#define PLL_DIV3        ( 0x41000000 + 0x120 )
#define PLL_CMD         ( 0x41000000 + 0x138 )  // controller command
#define PLL_STAT        ( 0x41000000 + 0x13c )  // controller status
#define ALN_CTL         ( 0x41000000 + 0x140 )  // controller clock align control
#define CKEN            ( 0x41000000 + 0x148 )  // clock enable control
#define CKSTAT          ( 0x41000000 + 0x14c )  // clock status
#define SYSTAT          ( 0x41000000 + 0x150 )  // SYSCLK status

#define EMIF_AWCCR      ( 0xF0000000 + 0x04 )   // async wait cycle config
#define EMIF_SDCR       ( 0xF0000000 + 0x08 )   // SDRAM config reg
#define EMIF_SDRCR      ( 0xF0000000 + 0x0c )   // SDRAM refresh control
#define EMIF_A1CR       ( 0xF0000000 + 0x10 )   // asynchronous 1 config
#define EMIF_SDTIMR     ( 0xF0000000 + 0x20 )   // SDRAM timing register
#define EMIF_SDSRETR    ( 0xF0000000 + 0x3c )   // SDRAM self refresh exit timing register
#define EMIF_EIRR       ( 0xF0000000 + 0x40 )   // interrupt raw
#define EMIF_EIMR       ( 0xF0000000 + 0x44 )   // interrupt mask reg
#define EMIF_EIMSR      ( 0xF0000000 + 0x48 )   // interrupt mask set
#define EMIF_EIMCR      ( 0xF0000000 + 0x4c )   // interrupt mask clear reg

#define GPIOEN          ( 0x43000000 + 0x0C )   // General Purpose I/O Enable Register
#define GPIODIR1        ( 0x43000000 + 0x10 )   // General Purpose I/O Direction Register 1
#define GPIODIR2        ( 0x43000000 + 0x18 )   // General Purpose I/O Direction Register 2
#define GPIODAT1        ( 0x43000000 + 0x14 )   // General Purpose I/O Data Register 1

#define PLD_DPRAM_START             0x90000000   // start address of PLD_DPRAM
#define DPRAM_DEBUG                 0x90000000   // debug address

// after the boot was read, the first 4 bytes are reserved for the handshake
#define PLD_DPRAM_TRANSFER_BUFFER   0x90000004
#define PLD_DPRAM_STOP              0x90004000   // first address after PLD_DPRAM (PLD_DPRAM size = 4Kb lw)

// values for the parallel flash signature
// only 1 byte is needed but as program counter will start at +4, they decide to repeat 4 times the byte value
#define TRANSFER_MAGIC_8            0x00000000
#define TRANSFER_MAGIC_16           0x01010101
#define TRANSFER_REQUEST            0x02020202
#define TRANSFER_DONE               0x03030303



// Internal function declarations

void dspBootLoader(void) __attribute__((noreturn, section(".myBootSection")));



// Internal function definitions

void dspBootLoader(void)
{
    /*
     * The first byte defines the type of access:
     *        0 (TRANSFER_MAGIC_8)    8 bit parallel flash
     *        1 (TRANSFER_MAGIC_16)   16 bit parallel flash
     *        2 (TRANSFER_REQUEST)    reserved
     *        3 (TRANSFER_DONE)       reserved
     *
     *  Only 1 byte is needed but as the program counter starts at +4, they decide
     *  to repeat 4 times the byte value. The magic number to tell the IROM-bootloader 
     *  that the Flash width is 16 bit wide is therefore 0x01010101.
     */

    // The pragma below silences a warning due to the assembly line below
    _Pragma("diag_suppress=1119")
    asm("   .word 0x01010101");

    asm("   .global _bootEntryPoint");
    asm("_bootEntryPoint:");

   /*
     * PLL initialization. From spru879a.pdf: TMS320C672x DSP Software-Programmable
     *                                        Phase-Locked Loop (PLL) Controller
     *   Section 3.1.1
     *    This section shows the initialization sequence, if the system intends to use divider D0 and PLL. This
     *    section also shows when you should program the multipliers and dividers, if needed.
     *    1. In PLLCSR, write PLLEN = 0 (bypass mode).
     *    2. Wait 4 cycles of the slowest of PLLOUT or reference clock source (CLKIN or OSCIN).
     *    3. In PLLCSR, write PLLRST = 1 (PLL is reset).
     *    4. If necessary, program PLLDIV0 and PLLM.
     *    5. If necessary, program PLLDIV1-n. Note that you must apply the GO operation to change these dividers
     *    to new ratios. See Section 3.2.1.
     *    6. Wait for PLL to properly reset. See device-specific data manual for PLL reset time.
     *    7. In PLLCSR, write PLLRST = 0 to bring PLL out of reset.
     *    8. Wait for PLL to lock. See device-specific data manual for PLL lock time.
     *    9. In PLLCSR, write PLLEN = 1 to enable PLL mode.
     *    Steps 1, 2, and 3 are required when PLLEN and PLLRST bits are not already 0 and 1, respectively. These
     *    steps are not required when the device is coming out of reset (by default, PLLEN and PLLRST bits are 0
     *    and 1, respectively).
     *
     *  Section 3.2.1.1
     *    The PLL controller clock align control register (ALNCTL) determines which SYSCLKs must be aligned.
     *    Before a GO operation, you must program ALNCTL according to the device-specific data manual
     *    requirement so that the appropriate clocks are aligned during the GO operation. All SYSCLKs must be
     *    aligned; therefore, the ALNn bits in ALNCTL should always be set to 1 before a GO operation.
     *    A GO operation is initiated by setting the GOSET bit in PLLCMD to 1. During a GO operation:
     *    Any SYSCLKn with the corresponding ALNn bit in ALNCTL set to 1 is paused at the low edge. Then
     *    the PLL controller restarts all these SYSCLKs simultaneously, aligned at the rising edge. When the
     *    SYSCLKs are restarted, SYSCLKn toggles at the rate programmed in the RATIO field in PLLDIVn.
     *    Any SYSCLKn with the corresponding ALNn bit in ALNCTL cleared to 0 remains free-running during a
     *    GO operation. SYSCLKn is not modified to the new RATIO rate in PLLDIVn. SYSCLKn is not aligned
     *    to other SYSCLKs.
     *    The GOSTAT bit in PLLSTAT is set to 1 throughout the duration of a GO operation.
     *
     *  The steps to do are:
     *
     *    1  PLLCSR.PLLEN = 0 (bypass mode) (not required when the device is coming out of reset)
     *    2  wait 4 cycles of the slowest of PLLOUT or reference clock source (CLKIN or OSCIN) (not required when the device is coming out of reset)
     *    3  PLLCSR.PLLRST = 1 (PLL is reset) (not required when the device is coming out of reset)
     *    4  program PLLDIV0
     *    5  program PLLM
     *    6  program PLLDIV1, apply the GO operation to change the divider to new ratio
     *    7  program PLLDIV2, apply the GO operation to change the divider to new ratio
     *    8  program PLLDIV3, apply the GO operation to change the divider to new ratio
     *    9  wait for PLL to properly reset
     *   10  PLLCSR.PLLRST = 0 (to bring PLL out of reset)
     *   11  wait for PLL to lock
     *   12  PLLCSR.PLLEN = 1 (enable PLL mode)
     *
     *    61 MainProg
     *    With ClkIn 25MHz, D0=0, D1=1, D2=3, D3=5, M=12
     *    PllOut=300MHz, SysClk1=150Mhz, SysClk2=75MHz, SysClk3=50MHz
     *
     *    60 BootProg
     *    With ClkIn 25MHz, D0=0, D1=0, D2=1, D3=2, M=12
     *    PllOut=300MHz, SysClk1=300Mhz, SysClk2=150MHz, SysClk3=100MHz
     *
     *    Init the PLL registers to have the full DSP CPU and EMIF speed
     *      DSP CPU = 300MHz
     *      EMIF = 100MHz
     *      The external clock is 25MHz
     */

    *(REG8U *)(EMIF_SDCR + 3) = 0x80;

    while ((*(REG32U *)PLL_CSR & 0x00000040) != 0x00000040)
    {
        ;
    }

    // When PLLEN is off DSP is running with CLKIN clock source, currently 25MHz or 40ns clk rate.

    // Clear the enable bit (Bypass mode)
    *(REG32U *)PLL_CSR  &= ~0x00000001;

    // Clear power-down, PLL is operational
    *(REG32U *)PLL_CSR  &= ~0x00000010;

    // Activate PLL reset, PLL takes 125ns to reset
    *(REG32U *)PLL_CSR  |= 0x00000008;

    // PLLOUT = CLKIN/(DIV0.RATIO+1) * PLLM
    // 300MHz = 25MHz / 1 * 12

    // Divider D0 enable with ratio 0 (so divides by 1)
    *(REG32U *)PLL_DIV0 = 0x00008000;

    *(REG32U *)PLL_M = 12;

    // Program in reverse order. DSP requires that peripheral clocks be less then 1/2
    // the CPU clock at all times. Order not critical, since changes need the GO to
    // come into effect.

    // Divider D3 enable with ratio 2 (so divides by 3)
    *(REG32U *)PLL_DIV3 = 0x00008002;

    // Divider D2 enable with ratio 1 (so divides by 2)
    *(REG32U *)PLL_DIV2 = 0x00008001;

    // Divider D1 enable with ratio 0 (so divides by 1)
    *(REG32U *)PLL_DIV1 = 0x00008000;

    // Align SYSCLK3, SYSCLK2 and SYSCLK1
    *(REG32U *)ALN_CTL  = 0x00000007;

    // Command GoSet
    *(REG32U *)PLL_CMD  = 0x00000001;

    // Make sure the GO operation has completed.
    while (*(REG32U *)PLL_STAT  == 0x00000001)
    {
        ;
    }

    // Bring PLL out of reset
    *(REG32U *)PLL_CSR &= ~0x00000008;

    // Wait for the PLL to lock. Minimum time: 187.5 us.
    {
        register uint32_t i = 0;

        // The while loop takes ~340 us. > 187.5 us.
        while (i++ < 500)
        {
            ;
        }
    }

    // Enable PLL mode
    *(REG32U *)PLL_CSR |= 0x00000001;

    // Now running PLL at 300MHz
    // SysClk1 : 300MHz     CPU and Memory (Max 300MHz)
    // SysClk2 : 150MHz     Peripheral and dMax
    // SysClk3 : 100MHz     EMIF (Max 133MHz when CPU 266MHz)
    // with 100 MHz SDRAM.


    // GPIOEN : General Purpose I/O Enable Register : 0x4300000C after reset 0x00000000
    // Universal Host Port Interface - Configure GPIO pins
    // UHPI_H[0] = TP0
    // UHPI_H[1] = TP1
    // UHPI_H[2] = 32bits access to PLD DPRAM

    *(REG32U *)GPIOEN   = 0x00000080;      // b7 = 1, UHPI_HD[7:0] pins function as GPIO pins
    *(REG32U *)GPIODIR1 = 0x0007;          // Bits b2,b1,b0 as outputs

    // EMIF setup
    // Note : Main.c looks for EMIF_SDCR != 0x00000621 to know if the boot loader already
    //        run or the Main.c is run from the debugger. So if the value is changed here
    //        don't forget to update in Main.c
    //        Read back as 621  instead of 721 because Bit 8 is always read as 0.

    *(REG8U *)(EMIF_SDCR + 3) = 0x00;       // Exit self-refresh mode

    *(REG32U *)EMIF_SDTIMR    = 0x31114610;
    *(REG32U *)EMIF_SDSRETR   = 0x00000006;
    *(REG32U *)EMIF_SDRCR     = 0x0000061a;
    *(REG32U *)EMIF_SDCR      = 0x00000721; // 32Bit, CAS=2, 4 banks, 9 columns
    *(REG32U *)EMIF_A1CR      = 0x086225be; // WE strobe mode, 32bit


    // Tell PLD that the DSP EMIF databus size is 32b -> 32bits access to PLD DPRAM
    *(REG32U *)GPIODAT1 = 0x0004;

   // Hardware initialisation finished

    // No stack is available. Use registers to allocate local variables.
    // Unfortunately, "register" is the only respected by the compiler for -O0 no optimisation.

    volatile uint32_t * dpram_p;
    register uint32_t   counter;
    register uint32_t * destination_ptr;
    register uint32_t   block_size;

    void (* jump_address)(void);

    // The 1st DPRAM handshake clear is to acknowledge the Rx610 that TMS320c6727 has booted
    // and is waiting for the program.

    while (true)
    {
        dpram_p = (uint32_t *) PLD_DPRAM_START;

        // handshake = 0 = DSP_HANDSHAKE_DSP_ACK for the Rx610

        *dpram_p = DSP_LOADER_DSP_ACK;

        // Wait until Rx610 has acknowledge the handshake

        do
        {
            // Sleep ~ 1ms

            counter = 0x000493E0;

            while (counter-- > 0) { ; }
        }
        while (*dpram_p != DSP_LOADER_MCU_REQUEST);

        if (counter == 0)
        {
            // TP0 = 1, TP1 = 0, 16 bits

            *(REG32U *)GPIODAT1 = 0x0001;

            // Jump to 0x00000000

            asm("           zero  B1");
            asm("           b    .S2 B1");
            asm("           nop   5");
        }
        else
        {
            // Reserve the first word for the handshake (DPRAM_HANDSHAKE_A)

            dpram_p++;

            // Retrieve the address where data must be copied to

            destination_ptr = (uint32_t *) *dpram_p;

            // @TODOD: verify the address is within a valid range

            dpram_p++;

            block_size = *dpram_p;

            // Get the size in 32 bit words

            block_size = block_size >> 2;

            // @TODO: verify that the size is valid

            dpram_p++;

            // After transferring the DSP image, the MCU sends the entry point. This
            // is signaled by setting the block size to 0.

            if (block_size == 0)
            {
                // Signal the MCU that the data has been retrieved

                dpram_p = (uint32_t *) PLD_DPRAM_START;

                *dpram_p = DSP_LOADER_DSP_ACK;

                // TP0 and TP1 to 0

                *(REG32U *)GPIODAT1 = 0x0004;

                // Jump to the BOOT/MAIN program               

                jump_address = (void ( *)(void)) destination_ptr;

                (*jump_address)();
            }
            else
            {
                // Copy one block of data

                do
                {
                    *destination_ptr++ = *dpram_p++;
                }
                while (--block_size > 0);
            }
        }
    }
}

// EOF
