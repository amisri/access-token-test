/*!
 *  @file      log_signals_class.h
 *  @defgroup  FGC:DSP:61
 *  @brief     Class-specific functionality to handle signal logging.
 */

#ifndef FGC_LOG_SIGNALS_CLASS_H
#define FGC_LOG_SIGNALS_CLASS_H

#ifdef LOG_SIGNALS_CLASS_GLOBALS
#define LOG_SIGNALS_CLASS_VARS_EXT
#else
#define LOG_SIGNALS_CLASS_VARS_EXT extern
#endif

// Specific macros for data buffer that are to be mapped in external RAM

#ifdef LOG_SIGNALS_CLASS_GLOBALS_FAR
#define LOG_SIGNALS_CLASS_VARS_EXT_FAR
#else
#define LOG_SIGNALS_CLASS_VARS_EXT_FAR extern
#endif

// Includes

#include <defconst.h>
#include <log_signals.h>
#include <meas.h>
#include <ref.h>

// External structures, unions and enumerations

typedef struct log_signal_capture_buf
{
    FP32  ref;
    FP32  v_ref;
    FP32  b_meas;
    FP32  i_meas;
    FP32  v_meas;
    FP32  err;
    FP32  mpx0;
    FP32  mpx1;
    FP32  mpx2;
} log_signal_capture_buf_t;

LOG_SIGNALS_CLASS_VARS_EXT_FAR log_signal_capture_buf_t log_capture_buffer[FGC_LOG_CAPTURE_LEN];

// Log buffer for property LOG.OASIS

LOG_SIGNALS_CLASS_VARS_EXT_FAR struct log_oasis_buf
{
    FP32 i_meas[FGC_LOG_LEN];  // LOG.OASIS.I_MEAS.DATA
    FP32 i_ref [FGC_LOG_LEN];  // LOG.OASIS.I_REF.DATA
} log_oasis_buf;

LOG_SIGNALS_CLASS_VARS_EXT log_signals_ctrl_t log_oasis_ctrl
#ifdef LOG_SIGNALS_CLASS_GLOBALS
=
{
    { 0, LOG_ODD },                     // write_idx
    { 0, LOG_ODD },                     // last_start_idx
    FGC_LOG_LEN,                        // max_idx
    2,                                  // nb_signals
    {
        &ref.i.value,
        &meas.i,
    },                                  // signal_src_ptr[]
    {
        &log_oasis_buf.i_ref[0],
        &log_oasis_buf.i_meas[0],
    }                                   // log_buf_ptr[]
}
#endif
;

#endif

// EOF
