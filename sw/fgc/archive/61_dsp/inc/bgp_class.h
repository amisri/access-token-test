/*!
 *  @file      bgp_class.h
 *  @defgroup  FGC:DSP:61
 *  @brief     Background processing functions specific to class 61.
 */

#ifndef FGC_BGP_CLASS_H
#define FGC_BGP_CLASS_H

#ifdef BGP_CLASS_GLOBALS
#define BGP_CLASS_VARS_EXT
#else
#define BGP_CLASS_VARS_EXT extern
#endif

#endif

// EOF
