/*!
 *  @file      state_class.h
 *  @defgroup  FGC3:DSP:61
 *  @brief     DSP PC state declaration
 */

#ifndef FGC_STATE_CLASS_H
#define FGC_STATE_CLASS_H

void StatePc(INT32U new_state_pc);


#endif

// EOF
