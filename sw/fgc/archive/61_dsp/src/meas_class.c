/*!
 *  @file      meas_class.c
 *  @defgroup  FGC:DSP:61
 *  @brief     Class specific measurement functions.
 */

#define MEAS_CLASS_GLOBALS

// Includes

#include <meas.h>
#include <stdint.h>
#include <defconst.h>
#include <ref.h>
#include <reg.h>
#include <sim.h>
#include <rtcom.h>
#include <cyc.h>
#include <props.h>
#include <adc.h>


// Class sepcific function definitions

FP32 MeasSpyMpx(uint32_t idx)
{
    switch (idx)
    {
            // Common

        case FGC_SPY_REF:           return (ref.now->value);

        case FGC_SPY_REFRATE:       return (ref.now->rate);

        case FGC_SPY_ERR:           return (reg_bi_err.err);

        case FGC_SPY_BMEAS:         return (meas.b);

        case FGC_SPY_BRATE:         return (meas.b_hist.rate);

        case FGC_SPY_BSIM:          return (sim.active->b);

        case FGC_SPY_IMEAS:         return (meas.i);

        case FGC_SPY_IRATE:         return (meas.i_hist.rate);

        case FGC_SPY_IRATEREG:      return (meas.i_hist_reg.rate);

        case FGC_SPY_ISIM:          return (sim.active->i);

        case FGC_SPY_MEAS:          return (meas.hist->meas);

        case FGC_SPY_MEASRATE:      return (meas.hist->rate);

        case FGC_SPY_MEASESTIMATED: return (meas.hist->estimated);

        case FGC_SPY_VREF:          return (ref.v.value);

        case FGC_SPY_VREFRATE:      return (ref.v.rate);

        case FGC_SPY_VMEAS:         return (meas.v);

        case FGC_SPY_VCAPA:         return (meas.v_capa);

        case FGC_SPY_VSIM:          return (sim.active->v);

        case FGC_SPY_VERR:          return (ref.v_err_avg);

        case FGC_SPY_IDIFF:         return (meas.i_diff_1s);

        case FGC_SPY_TADC:          return (property.adc.temperature);

        case FGC_SPY_TDCCTA:        return (property.dcct.temperature[0]);

        case FGC_SPY_TDCCTB:        return (property.dcct.temperature[1]);

        case FGC_SPY_CYCTIME:       return ((FP32)cyc.time.s);

        case FGC_SPY_REFTIME:       return ((FP32)ref.time.s);

        case FGC_SPY_DSPISR:        return ((FP32)rtcom.cpu_usage);

        case FGC_SPY_FPVAL:         return (*property.fgc.debug.spy.fpval);

        case FGC_SPY_INTVAL:        return ((FP32)(*property.fgc.debug.spy.intval));

        case FGC_SPY_FG:            return (ref.fg.value);

        case FGC_SPY_FGRATE:        return (ref.fg.rate);

        case FGC_SPY_FGPLUSRT:      return (ref.fg_plus_rt.value);

        case FGC_SPY_RT:            return (ref.rt);

        case FGC_SPY_PLL_DAC:        return (dpcom->mcu.pll.dac);

        case FGC_SPY_PLL_E18_E:      return (((FP32)(dpcom->mcu.pll.e18_error)) / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_E19_E:      return (((FP32)(dpcom->mcu.pll.e19_error)) / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_E:          return (dpcom->mcu.pll.error / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_ETH_SYNC:   return (dpcom->mcu.pll.eth_sync_cal / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_EXT_A_E:    return (dpcom->mcu.pll.ext_avg_error / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_EXT_E:      return (((FP32)(dpcom->mcu.pll.ext_error)) / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_FIP_E:      return (((FP32)(dpcom->mcu.pll.fip_error)) / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_FILT_INTE:  return (dpcom->mcu.pll.filtered_integrator / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_INTE:       return (dpcom->mcu.pll.integrator / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_NET_A_E:    return (dpcom->mcu.pll.net_avg_error / PLL_TICKS_TO_SECONDS);

        case FGC_SPY_PLL_STATE:      return ((FP32)(dpcom->mcu.pll.state));

        case FGC_SPY_DIRECTA:       return ((FP32)adc_chan[0].adc_direct);

        case FGC_SPY_DIRECTB:       return ((FP32)adc_chan[1].adc_direct);

        case FGC_SPY_RAWA:          return ((FP32)adc_chan[0].adc_raw);

        case FGC_SPY_RAWB:          return ((FP32)adc_chan[1].adc_raw);

        case FGC_SPY_DIRECTC:       return ((FP32)adc_chan[2].adc_direct);

        case FGC_SPY_DIRECTD:       return ((FP32)adc_chan[3].adc_direct);

        case FGC_SPY_RAWC:          return ((FP32)adc_chan[2].adc_raw);

        case FGC_SPY_RAWD:          return ((FP32)adc_chan[3].adc_raw);

        case FGC_SPY_VA:            return (*adc_chan_v_adc[0]);

        case FGC_SPY_VB:            return (*adc_chan_v_adc[1]);

        case FGC_SPY_VC:            return (*adc_chan_v_adc[2]);

        case FGC_SPY_VD:            return (*adc_chan_v_adc[3]);

        case FGC_SPY_IA:            return (adc_current_a != NULL ? adc_current_a->meas.current.i_dcct : 0.0);

        case FGC_SPY_IB:            return (adc_current_b != NULL ? adc_current_b->meas.current.i_dcct : 0.0);

        case FGC_SPY_VS:            return (adc_v_meas_load != NULL ? adc_v_meas_load->meas.voltage.v_meas : 0.0);

        case FGC_SPY_AUX:           return (adc_aux != NULL ? adc_aux->meas.voltage.v_meas : 0.0);

        case FGC_SPY_I_RMS:         return (property.meas.i_rms);
    }

    return (-1.23456789);       // Unknown channel
}

// EOF

