/*!
 *  @file      state_class.c
 *  @defgroup  FGC3:DSP:61
 *  @brief     DSP PC state declaration
 */

// Includes

#include <state.h>
#include <cc_types.h>
#include <props.h>
#include <dpcls.h>
#include <rtcom.h>
#include <reg.h>
#include <main.h>
#include <meas.h>
#include <refto.h>
#include <macros.h>
#include <bgp.h>
#include <cyc.h>
#include <pars.h>
#include <definfo.h>
#include <refto.h>
#include <dsp_dependent.h>
#include <defprops_dsp_fgc.h>
#include <mcu_dsp_common.h>
#include <pc_state.h>
#include <unipolar_switch.h>

// Internal function declarations

static void StateFS_FO_OF(void);
static void StateSP(void);
static void StateST(void);
static void StateSA(void);
static void StateTS(void);
static void StateSB_IL(void);
static void StateAB(void);
static void StateTC(void);
static void StateBK(void);

// Internal function definitions

void StateFS_FO_OF(void)
{
    RegStateV(TRUE, 0.0);

    // Reset property REF.ABORT if the abort time is in the future,
    // but keep a value in the past as it indicates the time of the last abort.

    if (AbsTimeUs_IsGreaterThan(property.ref.abort, rtcom.time_now_us))
    {
        property.ref.abort.unix_time  = 0;
        property.ref.abort.us_time    = 0;
    }

    ref.abort.unix_time           = TIME_NEVER;         // Reset the mirror value ref.abort
}

void StateSP(void)
{
    if (REF_STOPPING_REQUIRED &&                        // If a stopping reference required and
        (state_pc == FGC_PC_ON_STANDBY ||                 // PC state is ON_STANDBY or
         state_pc == FGC_PC_TO_STANDBY))                  // TO_STANDBY
    {
        refto.flags.stopping = TRUE;                    // Run STOPPING reference function
    }
    else                                                //else
    {
        StateFS_FO_OF();                                        // Jump to OF
    }
}

void StateST(void)
{
    RegStateV(TRUE, 0);
    Clr(rtcom.warnings, STRETCHED_CYC_WARNINGS);
}

void StateSA(void)
{
    if (reg.active->state == FGC_REG_V)         // Open-loop
    {
        if (PcStateBelow(FGC_PC_TO_STANDBY) ||               // If previous PC state is < TO_STANDBY or
            (lim_i_ref.flags.unipolar &&                    // 1/2-Quadrant converter and
             meas.i <= property.limits.op.i.min))           // current below I_MIN
        {
            RegStateV(TRUE, 0.0);                             // Reset Vref
        }
    }
    else                                        // Closed loop
    {
        // Set refto.flags.standby, refto.flags.stopping or both. E.g. on a 2Q converter, if in IDLE state, both flags
        // are set, which means that the FGC will run a TO_STANDBY reference followed by a STOPPING reference

        if (PcStateAbove(FGC_PC_ON_STANDBY))               // If previous PC state is > ON_STANDBY
        {
            refto.flags.standby = TRUE;                                // Ref() will call RefToStandby()
        }

        if (REF_STOPPING_REQUIRED &&                    // If a stopping reference required and
            PcStateAboveEqual(FGC_PC_TO_STANDBY))               // PC state is >= TO_STANDBY
        {
            refto.flags.stopping = TRUE;                               // Ref() will call RefToStopping()
        }
    }
}

void StateTS(void)
{
    struct fp_time time_remaining;
    FP32   v_ref_sb;
    INT32U reg_state = dpcls->mcu.ref.func.reg_mode[0];

    if (reg_state != FGC_REG_V)                 // If current loop is to be enabled
    {
        if (PcStateBelow(FGC_PC_SLOW_ABORT))       // If starting
        {
            RegStateRequest(FALSE, TRUE, reg_state, 0.0);

            if (lim_i_ref.flags.unipolar)       // if unipolar circuit
            {
                ref_fg.starting.openloop_f = TRUE;
                ref_fg.starting.delay      = FGC_N_RST_COEFFS * reg.active->rst_pars.period;
                ref_fg.starting.i_rate     = property.ref.rate.i.starting[property.load.select];

                time_remaining.s = (20.0 + ref_fg.starting.delay + 2.0 * limits.i_closeloop / ref_fg.starting.i_rate);
                time_remaining.period_counter = DSP_CEIL_TIME_VP(time_remaining.s);

                if (RESISTIVE_LOAD)                             // If fast load (Tc <= 1s)
                {
                    ref_fg.starting.v_rate = ref_fg.starting.i_rate * load.active->pars.ohms;
                }
                else                                            // else Tc > 1s
                {
                    // Coefficients are chosen to take ~6s to ramp to I_START

                    ref_fg.starting.v_rate = limits.i_start * (0.05 * load.active->pars.henrys + 0.2 * load.active->pars.ohms);
                }

                UniSwitchFloat(&ref_fg.starting.v_rate);
                UniSwitchFloat(&ref_fg.starting.i_rate);
            }
            else                                                // else bipolar circuit
            {
                ref_fg.starting.v_rate = 0.0;                   // No voltage ramp
                ref_fg.starting.delay = 7.0;                    // Start delay of 7s

                time_remaining.s = ref_fg.starting.delay + 0.5;
                time_remaining.period_counter = DSP_CEIL_TIME_VP(time_remaining.s);

            }

            // The three assignments below must be done atomically since
            // Time::TimeRunning() might otherwise zero ref.time_remaining.s if
            // ref.funcy_type = FGC_REF_NONE. Hence the use of the temporarily
            // time_remaining variable.

            DISABLE_TICKS;

            ref.time_remaining.s = time_remaining.s;
            ref.time_remaining.period_counter = time_remaining.period_counter;
            ref.func_type = FGC_REF_STARTING;

            ENABLE_TICKS;

            // Update RTD info, prepare meta data

            dpcls->dsp.ref.func_type      = FGC_REF_STARTING;
            dpcls->dsp.ref.stc_func_type  = dpcls->mcu.ref.stc_func_type;       // Should be equal to STC_STARTING
            // (that define is only known by the MCU)
            ppm[0].armed.meta.duration    = ref.time_remaining.s;
            ppm[0].armed.meta.range.start = 0.0;
            dpcls->dsp.ref.start          = ToIEEE(0.0);
            ppm[0].armed.meta.range.end   = limits.i_closeloop;
            dpcls->dsp.ref.end            = ToIEEE(limits.i_closeloop);
            ppm[0].armed.meta.range.min   = ppm[0].armed.meta.range.start;
            ppm[0].armed.meta.range.max   = ppm[0].armed.meta.range.end;

        }
        else if (PcStateAbove(FGC_PC_ON_STANDBY))   // else (not starting) and if > ON_SB now
        {
            refto.flags.standby = TRUE;         // Flag call RefToStandby
        }
    }
    else                                        // else current loop disabled
    {
        v_ref_sb = property.limits.op.i.min * load.active->pars.ohms;

        RegStateRequest(FALSE, TRUE, FGC_REG_V, v_ref_sb);
    }
}

void StateSB_IL(void)
{
    BgpInitNone(FGC_REG_V, 0);                  // Cancel reference. First argument is ignored
    ref.func_type = FGC_REF_NONE;
}

void StateAB(void)
{
    /*
     * State AB on the MCU indicates that the user requested the abort via a "S PC IL" command.
     * In this case, property REF.ABORT must be set by the FGC to show the actual time.
     */

    if (ref.func_type != FGC_REF_ABORTING)      // If not already aborting
    {
        // Set REF.ABORT to time.now + 100 ms (keep a margin because ParsAbortTime will subtract the ref advance time).

        property.ref.abort = rtcom.time_now_us;

        AbsTimeUsAddDelay(&property.ref.abort, 100000);          // +100ms

        // Parse the modified REF.ABORT property as if the user did a "S REF.ABORT" command.

        ParsAbortTime();
    }
}

void StateTC(void)
{
    CycEnter();
}

void StateBK(void)
{
    RegStateV(TRUE, 0);
}

// Platform/class specific function definition

void StatePc(INT32U new_state_pc)
{
    static void (*state_func[])(void) =         // Ref gen functions
    {
        StateFS_FO_OF,                          //  0    FLT_OFF
        StateFS_FO_OF,                          //  1    OFF
        StateFS_FO_OF,                          //  2    FLT_STOPPING
        StateSP,                                //  3    STOPPING
        StateST,                                //  4    STARTING
        StateSA,                                //  5    SLOW_ABORT
        StateTS,                                //  6    TO_STANDBY
        StateSB_IL,                             //  7    ON_STANDBY
        StateSB_IL,                             //  8    IDLE
        StateTC,                                //  9    TO_CYCLING
        0,                                      // 10    ARMED
        0,                                      // 11    RUNNING
        StateAB,                                // 12    ABORTING
        0,                                      // 13    CYCLING
        0,                                      // 14    POL_SWITCHING
        StateBK,                                // 15    BLOCKING
        0,                                      // 16    ECONOMY
        0,                                      // 17    DIRECT
    };

    if (dpcls->mcu.mode_rt &&                   // If RT control is enabled, and
        new_state_pc <= FGC_PC_ON_STANDBY &&     // new PC state is ON_STANDBY or LOWER, and
        PcStateAbove(FGC_PC_ON_STANDBY))       // PC state was ABOVE ON_STANDBY
    {
        // Copy ref.fg_plus_rt to ref.fg, to avoid a bump in the reference.
        // The whole reference structure is copied, this is on purpose. The real-time reference ref.rt will
        // jump to 0.0 on the next regulation iteration after the transition to ON_STANDBY state (or lower),
        // so the new reference ref.fg must exactly match the last ref.fg_plus_rt (including rate of change,
        // estimated value etc.)

        ref.fg = ref.fg_plus_rt;
    }

    // If leaving CYCLING then run CycExit()

    if (state_pc == FGC_PC_CYCLING)
    {
        CycExit();                              // Exit from CYCLING
    }

    // Run state function for new PC state, if defined

    if (state_func[new_state_pc])
    {
        state_func[new_state_pc]();
    }

    state_pc = new_state_pc;
}

// EOF
