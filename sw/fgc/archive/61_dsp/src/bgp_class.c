/*!
 *  @file      bgp_class.c
 *  @defgroup  FGC:DSP:61
 *  @brief     Background processing functions specific to class 61.
 */

#define BGP_CLASS_GLOBALS

// Includes

#include <bgp_class.h>
#include <bgp.h>
#include <cc_types.h>
#include <stdint.h>
#include <libfg.h>
#include <libfg/trim.h>
#include <libfg/test.h>
#include <props.h>
#include <dpcls.h>
#include <macros.h>
#include <dsp_dependent.h>
#include <dpcom.h>
#include <fgc_errs.h>
#include <rtcom.h>
#include <reg.h>
#include <ref.h>
#include <definfo.h>
#include <defprops_dsp_fgc.h>
#include <main.h>
#include <ppm.h>
#include <lib.h>
#include <isr.h>
#include <dsp_panic.h>
#include <mcu_dsp_common.h>
#include <math.h>
#include <cyc.h>
#include <mathf.h>
#include <unipolar_switch.h>
#include <property.h>

// Internal functions declaration

static INT32U BgpCopyRefRunning(void);
static void BgpCopyRefCycling(INT32U user);
static enum fg_error BgpCheckIref(struct fg_limits * limits,
                                  uint32_t invert_limits,
                                  float ref, float rate,
                                  float acceleration);
static INT32U BgpInitZero(INT32U reg_mode,
                          INT32U user,
                          enum fg_limits_polarity limits_polarity);
static INT32U BgpInitPppl(INT32U reg_mode,
                          INT32U user,
                          enum fg_limits_polarity limits_polarity);
static INT32U BgpInitTable(INT32U reg_mode,
                           INT32U user,
                           INT32U func_state,
                           enum fg_limits_polarity limits_polarity);
static INT32U BgpInitPlep(INT32U reg_mode,
                          enum fg_limits_polarity limits_polarity);
static INT32U BgpInitTrim(INT32U reg_mode,
                          enum fg_trim_type trim_type,
                          enum fg_limits_polarity limits_polarity);
static INT32U BgpInitTestFunc(INT32U reg_mode,
                              enum fg_test_type test_type,
                              enum fg_limits_polarity limits_polarity);
static INT32U BgpInitOpenloop(INT32U reg_mode);

// Internal variable definitions

static struct fg_limits * vib_limits[] = { &property.limits.op.v, &property.limits.op.i, &property.limits.op.b };

// Internal function definitions

static INT32U BgpCopyRefRunning(void)
{
    /*
     * This function is called to set a new RUNNING reference, for the specific
     * function types that can be set in both cycling and non-cycling modes
     * (e.g. a TABLE function).
     *
     * Contrary to the CYCLING mode, there is no need to copy the armed reference
     * ntirely (for example, a full copy of a TABLE's data points is not required).
     * Yet for compatibility with the CYCLING mode, the "pars" structure (e.g.
     * fg_table_pars in the case of a TABLE function) must be copied inside a
     * container of type "struct ppm_cycling". That structure will be:
     *    - In the case of a synchronous DSP   (FGC2): ppm[0].cycling
     *    - In the case of an asynchronous DSP (FGC3): *ref.next
     */

    INT32U                  ref_func_type;
    struct ppm_cycling   *  ppm_cycling_ptr;

    // Early return if there is a parameter switch pending

    if (Test(isr_switch_request_flags, ISR_SWITCH_REF))
    {
        return FGC_BAD_STATE;
    }

    ppm_cycling_ptr = ref.next;

    ref_func_type = dpcls->dsp.ref.func.type[0];

    if (ref_func_type != FGC_REF_NONE)
    {
        ppm_cycling_ptr->func_type         = ref_func_type;
        ppm_cycling_ptr->reg_mode          = ppm[0].armed.reg_mode;
        ppm_cycling_ptr->stc_func_type     = ppm[0].armed.stc_func_type;

        switch (ref_func_type)
        {
            case FGC_REF_TABLE:

                ppm_cycling_ptr->pars.table        = ppm[0].armed.pars.table;

                // NB: ppm[0].armed.table_data is NOT copied, therefore the table.ref and table.time
                // pointers will refer to it directly.

                ppm_cycling_ptr->pars.table.function = &ppm[0].armed.table_data.function[0];

                break;
        }
    }

    // Request the ISR to switch the reference context

    DISABLE_TICKS;
    Set(isr_switch_request_flags, ISR_SWITCH_REF);
    ENABLE_TICKS;

    return FGC_OK_NO_RSP;
}

static void BgpCopyRefCycling(INT32U user)
{
    /*
     * This function is called to set the new CYCLING reference for the user
     * passed as argument. The cycling reference is copied from the armed
     * reference.
     */

    INT32U       ref_func_type;
    struct ppm * ppm_user = ppm + user;

    ref_func_type = dpcls->dsp.ref.func.type[user];

    ppm_user->cycling.func_type         = ref_func_type;
    ppm_user->cycling.reg_mode          = ppm_user->armed.reg_mode;
    ppm_user->cycling.stc_func_type     = ppm_user->armed.stc_func_type;
    ppm_user->cycling.meta              = ppm_user->armed.meta;
    ppm_user->cycling.min_basic_periods = ppm_user->armed.min_basic_periods;
    ppm_user->cycling.first_plateau     = ppm_user->armed.first_plateau;

    switch (ref_func_type)
    {
        case FGC_REF_ZERO:

            ppm_user->cycling.pars.zero         = ppm_user->armed.pars.zero;
            break;

        case FGC_REF_PPPL:

            ppm_user->cycling.pars.pppl         = ppm_user->armed.pars.pppl;
            break;

        case FGC_REF_TABLE:

            ppm_user->cycling.pars.table        = ppm_user->armed.pars.table;
            ppm_user->cycling.table_data        = ppm_user->armed.table_data;

            // Need to adjust the pointers in ppm_user->cycling.pars structure

            ppm_user->cycling.pars.table.function = &ppm_user->cycling.table_data.function[0];

            break;

        case FGC_REF_NONE:
        default:
            // Nothing to copy
            break;
    }
}

static enum fg_error BgpCheckIref(struct fg_limits * limits,
                                  uint32_t invert_limits,
                                  float ref, float rate,
                                  float acceleration)
{
    /*
     * This function is a callback for property.limits.op.i and will be called
     * when libfg is checking a function of current. The library will already
     * check the reference against pos/neg/rate and acceleration limits. This
     * function uses the load model to check if the converter can supply
     * sufficient voltage to drive the function.
     */

    float v_ref;
    float i_ref;

    i_ref = ref;
    v_ref = i_ref * load.active->pars.ohms +
            rate  * load.active->pars.henrys * regLoadCalcSatFactor(&load.active->pars, i_ref);

    // Calculate the voltage limits for the current i_ref, taking into account invert_limits

    regLimRefSetInvertLimits(&lim_v_ref_fg, invert_limits);

    regLimVrefCalc(&lim_v_ref_fg, i_ref);

    if (invert_limits == 0)
    {
        // Check estimated voltage required against estimated voltage available

        if (v_ref < lim_v_ref_fg.min_clip || v_ref > lim_v_ref_fg.max_clip)
        {
            return (FG_OUT_OF_VOLTAGE_LIMITS);
        }
    }
    else
    {
        // Check estimated voltage required against estimated voltage available using inverted limits

        if (v_ref < -lim_v_ref_fg.max_clip || v_ref > -lim_v_ref_fg.min_clip)
        {
            return (FG_OUT_OF_VOLTAGE_LIMITS);
        }
    }

    return (FG_OK);
}

static INT32U BgpInitZero(INT32U reg_mode,
                          INT32U user,
                          enum fg_limits_polarity limits_polarity)
{
    /*
     * [CYCLING] This function is called to prepare a ZERO cycle. The length
     * of a zero cycle is defined by the start plateau timing parameters,
     * even though the reference
     */

    INT32U                 errnum;
    struct fg_trim_config  config;
    struct ppm      *      ppm_user = ppm + user;

    // ZERO reference is only valid in V and I regulation modes (not B)

#if (FGC_REG_B_SUPPORT == TRUE)

    if (reg_mode == FGC_REG_B)
    {
        return (FGC_BAD_REG_MODE);
    }

#endif

    // For current (I) regulation mode the ZERO uses a CTRIM to ramp to the first plateau settings

    if (reg_mode == FGC_REG_I)
    {
        config.type     = FG_TRIM_CUBIC;
        config.duration = ppm_user->armed.first_plateau.time - property.ref.start.time;
        config.final    = ppm_user->armed.first_plateau.ref;

        if ((errnum = BgpTranslateFgError(
                          fgTrimArm(&property.limits.op.i,
                                    limits_polarity,
                                    &config,
                                    property.ref.start.time,
                                    0.0,
                                    &ppm_user->armed.pars.zero,
                                    &ppm_user->armed.meta))))
        {
            return (errnum);
        }

        ppm_user->armed.meta.duration = ppm_user->armed.first_plateau.time + ppm_user->armed.first_plateau.duration;
    }

    return (FGC_OK_NO_RSP);
}



static INT32U BgpInitPppl(INT32U reg_mode,
                          INT32U user,
                          enum fg_limits_polarity limits_polarity)
{
    /*
     * [CYCLING] This function is called to prepare the PPPL parameters.
     */

    struct ppm * ppm_user = ppm + user;

    // PPPL reference is not valid in V regulation mode

    if (reg_mode == FGC_REG_V)
    {
        return (FGC_BAD_REG_MODE);
    }

    return (BgpTranslateFgError(
                fgPpplArm(vib_limits[reg_mode],
                          limits_polarity,
                          &ppm_user->config.pppl,
                          ppm_user->armed.first_plateau.time + ppm_user->armed.first_plateau.duration,
                          ppm_user->armed.first_plateau.ref,
                          &ppm_user->armed.pars.pppl,
                          &ppm_user->armed.meta)));
}

static INT32U BgpInitTable(INT32U reg_mode,
                           INT32U user,
                           INT32U func_state,
                           enum fg_limits_polarity limits_polarity)
{
    /*
     * [CYCLING,RUNNING]  This function is called to prepare the Table for
     * either CYCLING or RUNNING operation. In CYCLING if the reg mode is B
     * then the cycle will start with the FIRST_PLATEAU and the first table
     * segment must end exactly at the end of the first plateau. In CYCLING
     * if the reg mode is I then FIRST_PLATEAU is not used.
     */

    INT32U       errnum;
    FP32         run_delay;
    FP32         min_time_step;                 // Minimal step between two table points
    struct ppm * ppm_user = ppm + user;

    // Link TABLE config structure with table data arrays

    ppm_user->config.table.function = &ppm_user->armed.table_data.function[0];

    // Prepare to arm for either RUNNING or CYCLING

    if (func_state == FGC_PC_RUNNING)
    {
        // Check for an initial reference mismatch of more than 10mA or 10mV

        if (fabs(ref.fg.value - ppm[0].armed.table_data.function[0].ref) > 0.01)
        {
            return (FGC_REF_MISMATCH);
        }

        run_delay = property.ref.run_delay;
    }
    else // CYCLING
    {
        // If B regulation check that first table segment is compatible with FIRST_PLATEAU

#if (FGC_REG_B_SUPPORT == TRUE)
        if (reg_mode == FGC_REG_B)
        {
            // Time of second point must exactly match time of end of the FIRST PLATEAU

            if (ppm_user->armed.table_data.function[1].time != (ppm_user->armed.first_plateau.time +
                                                                ppm_user->armed.first_plateau.duration))
            {
                ppm_user->armed.meta.error.index = 1;
                return (FGC_INVALID_TIME);
            }

            // First table segment must be flat and with 10mG of the FIRST_PLATEAU level

            if (ppm_user->armed.table_data.function[0].ref != ppm_user->armed.table_data.function[1].ref ||
                fabs(ppm_user->armed.table_data.function[0].ref - ppm_user->armed.first_plateau.ref) > 0.01)
            {
                ppm_user->armed.meta.error.index = 1;
                return (FGC_REF_MISMATCH);
            }
        }

#endif

        run_delay = 0.0;
    }

    min_time_step = REF_TABLE_MIN_TIME_STEP;

    // Arm TABLE function

    if (dpcls->mcu.ref.func_type == FGC_REF_TABLE) // ToDo check
    {
        errnum = BgpTranslateFgError(
                     fgTableArm(vib_limits[reg_mode],
                                limits_polarity,
                                &ppm_user->config.table,
                                run_delay,
                                min_time_step,
                                &ppm_user->armed.pars.table,
                                &ppm_user->armed.meta));
    }

    return (errnum);
}

static INT32U BgpInitPlep(INT32U reg_mode,
                          enum fg_limits_polarity limits_polarity)
{
    /*
     * [RUNNING] This function is called to prepare the PLEP parameters when
     * a PLEP has been specified by the user. Since the PLEP function is used
     * for slow abort, the working parameters are in plep and interrupts must
     * be inhibited before copying the user parameters over the working
     * parameters.
     */

    INT32U errnum;

    errnum = BgpTranslateFgError(
                 fgPlepArm(vib_limits[reg_mode],
                           limits_polarity,
                           &property.ref.plep,
                           property.ref.run_delay,
                           ref.fg.value,
                           &ref_fg.plep,
                           &ppm[0].armed.meta));

    // Copy user PLEP parameters to working PLEP parameters protected against interrupts

    DISABLE_TICKS;

    if (!errnum && state_pc == FGC_PC_IDLE)
    {
        plep = ref_fg.plep;
    }

    ENABLE_TICKS;

    return (errnum);
}

static INT32U BgpInitTrim(INT32U reg_mode,
                          enum fg_trim_type trim_type,
                          enum fg_limits_polarity limits_polarity)
{
    /*
     * [RUNNING] This function is called to prepare the Trim Function parameters.
     */
    property.ref.trim.type = trim_type;

    return (BgpTranslateFgError(
                fgTrimArm(vib_limits[reg_mode],
                          limits_polarity,
                          &property.ref.trim,
                          property.ref.run_delay,
                          ref.fg.value,
                          &ref_fg.trim,
                          &ppm[0].armed.meta)));
}

static INT32U BgpInitTestFunc(INT32U reg_mode,
                              enum fg_test_type test_type,
                              enum fg_limits_polarity limits_polarity)
{
    /*
     * [RUNNING] This function is called to prepare the Test Function
     * parameters.
     */

    property.ref.test.type = test_type;

    return (BgpTranslateFgError(
                fgTestArm(vib_limits[reg_mode],
                          limits_polarity,
                          &property.ref.test,
                          property.ref.run_delay,
                          ref.fg.value,
                          &ref_fg.test,
                          &ppm[0].armed.meta)));
}

static INT32U BgpInitOpenloop(INT32U reg_mode)
{
    /*
     * [RUNNING] This function is called to prepare the Openloop Function
     * parameters. This is not included in libfg. The input parameters are:
     *  ref.fg.value                    Raw Ref value now (A)
     *  property.ref.run_delay_tb       Time before start of test function
     *  property.ref.openloop.final     Final ref (A)
     */

    FP32        dref;                   // Trim range (A)
    FP32        i_acceleration;         // Initial and final acceleration
    FP32        didt_start;             // Max dI/dT (A/s) at initial current
    FP32        didt_final;             // Max dI/dT (A/s) at final current
    FP32        min_dref;               // Minimum change in current
    FP32        duration_s;             // Trim period (s)
    FP32        dopenloop;              // Open loop zone required for acceleration (A)
    FP32        dcloseloop;             // Close loop zone required for deceleration (A)

    // OPENLOOP reference is not valid in V regulation mode

    if (reg_mode == FGC_REG_V)
    {
        return (FGC_BAD_REG_MODE);
    }

    // Check final current is within limits

    if ((property.ref.openloop.final > lim_i_ref.max_clip) ||
        (!lim_i_ref.flags.unipolar && (property.ref.openloop.final < lim_i_ref.min_clip)) ||
        (lim_i_ref.flags.unipolar && (property.ref.openloop.final < property.limits.op.i.min)))
    {
        return (FGC_OUT_OF_LIMITS);
    }

    // Get parameters from properties

    ref_fg.openloop.final     = property.ref.openloop.final;
    ref_fg.openloop.run_delay = property.ref.run_delay;

    // Check direction and stopping distance

    dref = ref_fg.openloop.final - ref.fg.value;
    ref_fg.openloop.pos_ramp_flag = (dref >= 0.0);
    ref_fg.openloop.v_ref = (ref_fg.openloop.pos_ramp_flag ? property.limits.op.v.pos :
                             property.limits.op.v.neg);

    i_acceleration     = FromIEEE(dpcls->mcu.ref.defaults.i.acceleration[property.load.select]);
    didt_start = fabs((ref_fg.openloop.v_ref - ref.fg.value * load.active->pars.ohms) *
                      load.active->pars.inv_henrys / regLoadCalcSatFactor(&load.active->pars, ref.fg.value));
    didt_final = fabs((ref_fg.openloop.v_ref - ref_fg.openloop.final * load.active->pars.ohms) *
                      load.active->pars.inv_henrys / regLoadCalcSatFactor(&load.active->pars, ref_fg.openloop.final));

    dopenloop  = 0.5 * didt_start * didt_start / i_acceleration;
    dcloseloop = 0.5 * didt_final * didt_final / i_acceleration;

    min_dref = dopenloop + dcloseloop + 10 * reg.active->rst_pars.period * didt_start;

    if (fabs(dref) < min_dref)          // If change less than minimum
    {
        return (FGC_CHANGE_TOO_SMALL);  // Reject the change
    }

    ref_fg.openloop.d_v_ref = reg.active->rst_pars.period * i_acceleration * load.active->pars.henrys
                              * // Set initial acceleration
                              regLoadCalcSatFactor(&load.active->pars, ref.fg.value) *
                              (ref_fg.openloop.pos_ramp_flag ? 1.0 : -1.0);

    ref.v_openloop = ref.v_fltr;

    // Use half of dcloseloop because MQM circuits
    ref_fg.openloop.i_closeloop = ref_fg.openloop.final +       // are slower than expected by FGC
                                  0.5 * (ref_fg.openloop.pos_ramp_flag ? -dcloseloop : dcloseloop);

    // Prepare meta data. Update RTD info.

    duration_s = 2.0 * fabs(dref) / (didt_start + didt_final);

    ppm[0].armed.meta.duration    = ref_fg.openloop.run_delay + duration_s;
    ppm[0].armed.meta.range.start = ref.fg.value;
    ppm[0].armed.meta.range.end   = ref_fg.openloop.i_closeloop;
    ppm[0].armed.meta.range.min   = MIN(ref.fg.value, ref_fg.openloop.i_closeloop);
    ppm[0].armed.meta.range.max   = MAX(ref.fg.value, ref_fg.openloop.i_closeloop);

    return (FGC_OK_NO_RSP);
}

// External function definitions

void BgpRefChange(INT32U user)
{
    INT32U    min_basic_periods;
    INT32U    func_type  = dpcls->mcu.ref.func_type;
    INT32U    func_state = dpcls->mcu.ref.func_state;                   // func_state: CYCLING or RUNNING
    INT32U    reg_mode   = dpcls->mcu.ref.func.reg_mode[user];
    enum      fg_limits_polarity limits_polarity;

    if (!Test((func_state == FGC_PC_RUNNING ? REF_ARM_FOR_RUNNING : REF_ARM_FOR_CYCLING), (1 << func_type)))
    {
        dpcom->dsp.errnum = FGC_BAD_PARAMETER;
    }
    else
    {
        limits_polarity = UniSwitchLimitsEnum();

        switch (func_type)
        {
            case FGC_REF_NONE:

                dpcom->dsp.errnum = BgpInitNone(reg_mode, user);
                break;

            case FGC_REF_ZERO:

                dpcom->dsp.errnum = BgpInitZero(reg_mode, user, limits_polarity);
                break;

            case FGC_REF_PPPL:

                dpcom->dsp.errnum = BgpInitPppl(reg_mode, user, limits_polarity);
                break;

            case FGC_REF_PLEP:

                dpcom->dsp.errnum = BgpInitPlep(reg_mode, limits_polarity);
                break;

            case FGC_REF_TABLE:

                dpcom->dsp.errnum = BgpInitTable(reg_mode, user, func_state, limits_polarity);
                break;

            case FGC_REF_LTRIM:

                dpcom->dsp.errnum = BgpInitTrim(reg_mode, FG_TRIM_LINEAR, limits_polarity);
                break;

            case FGC_REF_CTRIM:

                dpcom->dsp.errnum = BgpInitTrim(reg_mode, FG_TRIM_CUBIC, limits_polarity);
                break;

            case FGC_REF_SQUARE:

                dpcom->dsp.errnum = BgpInitTestFunc(reg_mode, FG_TEST_SQUARE, limits_polarity);
                break;

            case FGC_REF_STEPS:

                dpcom->dsp.errnum = BgpInitTestFunc(reg_mode, FG_TEST_STEPS, limits_polarity);
                break;

            case FGC_REF_SINE:

                dpcom->dsp.errnum = BgpInitTestFunc(reg_mode, FG_TEST_SINE, limits_polarity);
                break;

            case FGC_REF_COSINE:

                dpcom->dsp.errnum = BgpInitTestFunc(reg_mode, FG_TEST_COSINE, limits_polarity);
                break;

            case FGC_REF_OPENLOOP:

                dpcom->dsp.errnum = BgpInitOpenloop(reg_mode);
                break;
        }
    }

    // Complete arming for RUNNING or CYCLING states if no error reported

    if (dpcom->dsp.errnum == FGC_OK_NO_RSP)
    {
        dpcls->dsp.ref.func.type[user] = func_type;
        ppm[user].armed.stc_func_type  = dpcls->mcu.ref.stc_func_type;
        ppm[user].armed.reg_mode       = dpcls->mcu.ref.func.reg_mode[user];

        if (func_state == FGC_PC_RUNNING)
        {
            // Important info: The ref.run time must be cleared BEFORE the ref.func_type is set to FGC_REF_ARMED, else
            //                 there is a time window where the interrupt could possibly start running the armed function

            ref.run.unix_time = TIME_NEVER;         // Reset property REF.RUN and its mirror value ref.run
            ref.run.us_time                   = 0;
            property.ref.run.unix_time        = 0;
            property.ref.run.us_time          = 0;

            // Reset property REF.ABORT if the abort time is in the future,
            // but keep a value in the past as it indicates the time of the last abort.

            if (AbsTimeUs_IsGreaterThan(property.ref.abort, rtcom.time_now_us))
            {
                property.ref.abort.unix_time  = 0;
                property.ref.abort.us_time    = 0;
            }

            ref.abort.unix_time               = TIME_NEVER;             // Reset the mirror value ref.abort

            ref.func_type                     = (func_type == FGC_REF_NONE ? FGC_REF_NONE : FGC_REF_ARMED);
            ref.time_remaining.s              = ppm[0].armed.meta.duration;
            ref.time_remaining.period_counter = DSP_CEIL_TIME_VP(ref.time_remaining.s);

            dpcls->dsp.ref.stc_func_type      = dpcls->mcu.ref.stc_func_type;
            dpcls->dsp.ref.start              = ToIEEE(ppm[0].armed.meta.range.start);
            dpcls->dsp.ref.end                = ToIEEE(ppm[0].armed.meta.range.end);

            // If command is "S REF NOW,..." then set run time to 1 second in the future

            if (dpcls->mcu.ref.run_now_f)
            {
                dpcls->mcu.ref.run_now_f = FALSE;       // Reset the flag

                ref.run = rtcom.time_now_us;            // Copy time.now
                AbsTimeUsAddLongDelay(&ref.run, 1, 0);  // + 1s

                property.ref.run  = ref.run;            // Transfer value to the property REF.RUN
            }

            // For function types that can be armed in both RUNNING and CYCLING (such as TABLE)
            // some extra processing is needed to arm the reference

            if (Test(REF_ARM_FOR_CYCLING, (1 << func_type)))
            {
                // Copy the armed reference parameters in a cycling "pars" structure

                dpcom->dsp.errnum = BgpCopyRefRunning();
            }
        }
        else // func_state == CYCLING
        {
            // Calculate and check minimum basic periods for the function

            // NB: On FGC3 it is important to force the calculation to 32-bit floating-point, by casting all operands and relying
            //     the standard library function ceilf, because the meta.duration is stored as a 32-bit float in memory. If part
            //     of the calculus is performed with extra precision there is a risk that a 1.2s reference (for example) is wrongly
            //     considered as requiring 2 basic periods. This is because 1.2 cannot be represented exactly as a floating-point.

            min_basic_periods = (INT32U)ceilf(ppm[user].armed.meta.duration / (FP32)FGC_BASIC_PERIOD_S);
            ppm[user].armed.min_basic_periods = min_basic_periods;

            if (property.fgc.event.len_basic_per[user] &&
                min_basic_periods > property.fgc.event.len_basic_per[user])
            {
                dpcom->dsp.errnum = FGC_BAD_CYCLE_LEN;
            }

            if (dpcom->dsp.errnum == FGC_OK_NO_RSP)
            {
                // If still no error, copy (or trigger the copy) of the armed reference to the cycling one.

                BgpCopyRefCycling(user);
            }

        }
    }

    // Report to MCU that the job is done
    // If an error was reported (via dpcom->dsp.errnum) then the DSP reference remains as it is.

    dpcls->mcu.ref.arm_f     = FALSE;
    dpcom->dsp.bg_complete_f = TRUE;
}

INT32U BgpInitNone(INT32U reg_mode, INT32U user)
{
    struct ppm * ppm_user = ppm + user;

    dpcls->dsp.ref.func.type[user]    = FGC_REF_NONE;
    ppm_user->armed.min_basic_periods = 0;
    fgResetMeta(&ppm_user->armed.meta, NULL, 0.0);

    return (FGC_OK_NO_RSP);
}

// Platform/class specific function definition

bgp_check_Iref_func BgpGetCheckIref(void)
{
    return BgpCheckIref;
}

// EOF
