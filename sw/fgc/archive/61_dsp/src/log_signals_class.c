/*!
 *  @file      log_signals_class.c
 *  @defgroup  FGC3:DSP:61
 *  @brief     File providing functionality for logging signals specific
 *             to Class 61
 */

#define LOG_SIGNALS_CLASS_GLOBALS
#define LOG_SIGNALS_CLASS_GLOBALS_FAR

// Includes

#include <log_signals_class.h>
#include <dpcls.h>
#include <ref.h>
#include <meas.h>
#include <reg.h>

// External function definitions

void LogSignalClassCaptureLog(uint16_t n_sample)
{
    log_signal_capture_buf_t * buf = &log_capture_buffer[n_sample];

    buf->ref    = ref.now->value;
    buf->v_ref  = ref.v.value;
    buf->b_meas = meas.b;
    buf->i_meas = meas.i;
    buf->v_meas = meas.v;
    buf->err    = reg_bi_err.err;
    buf->mpx0   = MeasSpyMpx(dpcls->mcu.log_capture_mpx[0]);
    buf->mpx1   = MeasSpyMpx(dpcls->mcu.log_capture_mpx[1]);
    buf->mpx2   = MeasSpyMpx(dpcls->mcu.log_capture_mpx[2]);
}

// EOF
