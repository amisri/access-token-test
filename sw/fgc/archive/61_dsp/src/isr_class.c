/*!
 *  @file      isr_class.c
 *  @defgroup  FGC:DSP:61
 *  @brief     Provides the definition of IsrRegProcess(), which for class 61 implements
 *             the actual regulation based on the the regulation type (I,V,B).
 */

#include <isr_class.h>
#include <isr.h>
#include <structs_bits_little.h>
#include <ana.h>
#include <cyc.h>
#include <reg.h>
#include <ref.h>
#include <refto.h>
#include <meas.h>
#include <dpcls.h>
#include <rtcom.h>
#include <props.h>
#include <main.h>
#include <crate.h>
#include <spivs.h>
#include <pc_state.h>
#include <unipolar_switch.h>
#include <adc.h>
#include <report.h>
#include <dig_sup.h>


// Constants

/*!
 * Number of consecutive SPIVS failures that trigger a warning.
 */
static uint8_t const MAX_NUM_SPIVS_FAILS = 3;

// Internal structures, unions and enumerations

typedef void (*ref_func_ptr)(float);

static ref_func_ptr IsrSendRef = NULL;

// Internal function declarations

/*!
 * This function is called when in field regulation state.
 */
static void IsrRegStateB(void);

/*!
 * This function is called when in current regulation state.
 */
static void IsrRegStateI(void);

/*!
 * This function is called when in voltage regulation state.
 */
static void IsrRegStateV(void);

/*!
 * Sends the reference value to the DAC 0.
 */
static void IsrSendRefDAQ(float ref);

/*!
 * Sends the reference and measurement to the digitally controlled converters
 * using the SPIVS bus.
 */
static void IsrSendRefSpivs(float ref);

/*!
 * Updates the acquisition measurement if needed.
 */
static void IsrUpdateAcquisition(void);

// Internal function definitions

void IsrRegStateI(void)
{
    FP32 v_ref;         // Vref, not accounting for load saturation
    FP32 v_ref_sat;     // Vref,     accounting for load saturation

    // Calculate current reference

    Ref();

    // Regulate current

    if (!TestRegBypass())
    {
        // ------------------------------------------------------------------------------------------------
        // Regulate current based on reference
        // ------------------------------------------------------------------------------------------------

        // Clip the reference. ref.i_clipped:  clipped reference;
        //                     ref.fg_plus_rt: unclipped reference
        //
        // Special case: the PLEP_NO_CLIP function for which no rate clipping is applied

        if (ref.func_type != FGC_REF_PLEP_NO_CLIP)
        {
            ref.i_clipped.value = regLimRef(&lim_i_ref, reg.active->rst_pars.period, ref.fg_plus_rt.value,
                                            ref.i_clipped.value);
        }
        else
        {
            // No rate clipping
            ref.i_clipped.value = regLimRef(&lim_i_ref, reg.active->rst_pars.period, ref.fg_plus_rt.value,
                                            ref.fg_plus_rt.value);
        }

        RefRefreshValue(&ref.i_clipped);        // Compute rate of change.

        // Calculate Vref

        v_ref = regRstCalcAct(&reg.active->rst_pars, &reg_vars, ref.i_clipped.value, meas.i);

        v_ref_sat = regLoadVrefSat(&load.active->pars, meas.i, v_ref);

        ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, v_ref_sat, ref.v.value);

        if (lim_v_ref.flags.clip || lim_v_ref.flags.rate)
        {
            v_ref = regLoadInverseVrefSat(&load.active->pars, meas.i, ref.v.value);

            ref.i.value = regRstCalcRef(&reg.active->rst_pars, &reg_vars, v_ref, meas.i);

            lim_i_ref.flags.rate = TRUE;
        }
        else
        {
            ref.i.value = ref.i_clipped.value;
        }

        RefRefreshValue(&ref.i);                // Compute rate of change.
        RefRefreshValue(&ref.v);                // Compute rate of change.

        // Calculate the error between the measurement and the clipped reference

        regErrCalc(&reg_bi_err, (ref.time.s >= reg.max_abs_err_enable_time), ref.i_clipped.value, meas.i);

        // Unipolar descent to standby, check for Vref hitting zero

        if (lim_v_ref.flags.unipolar  &&                // If 1-Q converter and
            ref.func_type == FGC_REF_TO_STANDBY &&       // ramping to standby and
            ref.v.value <= 0.0)                          // Vref has fallen below zero
        {
            RefToStandbyToOpenloop();                           // Switch to OPENLOOP to finish ramp down
        }

        // Cycle Check - REG_ERR : Regulation fault

        if (ref.cycling_f && reg_bi_err.flags.fault)
        {
            CycSaveCheckWarning(TRIGG_LOG, FGC_CYC_WRN_REG_ERR, FGC_WRN_REG_ERROR, 0.0, 0.0, 0.0, 0.0);
            CycLogAbsMaxErr();
            refto.flags.end = TRUE;             // Will call RefToEnd() on the next regulation period
        }

        // Manage REF_LIM and REF_RATE_LIM warnings (clipped reference)

        if (lim_i_ref.flags.clip && state_pc != FGC_PC_CYCLING)
        {
            Set(rtcom.warnings, FGC_WRN_REF_LIM);
        }
        else
        {
            Clr(rtcom.warnings, FGC_WRN_REF_LIM);
        }

        if (lim_i_ref.flags.rate && state_pc != FGC_PC_CYCLING)
        {
            Set(rtcom.warnings, FGC_WRN_REF_RATE_LIM);
        }
        else
        {
            Clr(rtcom.warnings, FGC_WRN_REF_RATE_LIM);
        }
    }
    else
    {
        // ------------------------------------------------------------------------------------------------
        // Use openloop voltage reference and calculate current reference
        // ------------------------------------------------------------------------------------------------

        v_ref_sat = regLoadVrefSat(&load.active->pars, meas.i, ref.v_openloop);

        ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, v_ref_sat, ref.v.value);

        v_ref = regLoadInverseVrefSat(&load.active->pars, meas.i, ref.v.value);

        // Calculate the equivalent current reference: ref.v --> ref.i = ref.i_clipped = ref.fg_plus_rt --> ref.fg
        // The RST history gets updated with the calculated reference.

        ref.i.value          = regRstCalcRef(&reg.active->rst_pars, &reg_vars, v_ref, meas.i);
        ref.fg_plus_rt.value = ref.i_clipped.value = ref.i.value;
        ref.fg.value         = ref.fg_plus_rt.value - ref.rt;

        RefRefreshValue(&ref.fg);               // Compute rate of change.
        RefRefreshValue(&ref.fg_plus_rt);       // Compute rate of change.
        RefRefreshValue(&ref.i_clipped);        // Compute rate of change.
        RefRefreshValue(&ref.i);                // Compute rate of change.
        RefRefreshValue(&ref.v);                // Compute rate of change.

        regErrCalc(&reg_bi_err, FALSE, ref.i_clipped.value, meas.i);
    }
}

void IsrRegStateV(void)
{
    // Calculate voltage reference

    Ref();

    // Apply limits to voltage reference

    ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, ref.fg_plus_rt.value, ref.v.value);

    RefRefreshValue(&ref.v);            // Compute Vref rate

    // Manage REF_LIM and REF_RATE_LIM warnings

    if (lim_v_ref.flags.clip && state_pc != FGC_PC_CYCLING)
    {
        Set(rtcom.warnings, FGC_WRN_REF_LIM);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_REF_LIM);
    }

    if (lim_v_ref.flags.rate && state_pc != FGC_PC_CYCLING)
    {
        Set(rtcom.warnings, FGC_WRN_REF_RATE_LIM);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_REF_RATE_LIM);
    }

    // In simulation mode, the simulated voltage (meas.v) is copied from the RTS history. Therefore even in V regulation,
    // one needs to keep the RST history updated.

    reg_vars.act [0] = ref.v.value;
    reg_vars.meas[0] = meas.i;
    reg_vars.ref [0] = meas.i;
}

static void IsrRegStateB(void)
{
    FP32 v_ref;

    // Calculate field reference

    Ref();

    // Regulate field

    if (!TestRegBypass())
    {
        // ------------------------------------------------------------------------------------------------
        // Regulate field using field reference
        // ------------------------------------------------------------------------------------------------

        // Clip the reference. ref.b: clipped reference; ref.fg_plus_rt: unclipped reference

        ref.b.value = ref.fg_plus_rt.value;   // No field reference limits required since no real-time control

        // Calculate the error between the measurement and the clipped reference

        regErrCalc(&reg_bi_err, (ref.time.s >= reg.max_abs_err_enable_time), ref.b.value, meas.b);

        // Calculate Vref

        v_ref = regRstCalcAct(&reg.active->rst_pars, &reg_vars, ref.b.value, meas.b);

        ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, v_ref, ref.v.value);

        if (lim_v_ref.flags.clip || lim_v_ref.flags.rate)
        {
            // Back calculate the field reference value corresponding to the clipped reference voltage

            ref.b.value = regRstCalcRef(&reg.active->rst_pars, &reg_vars, ref.v.value, meas.b);
        }

        RefRefreshValue(&ref.b);                // Compute rate of change.
        RefRefreshValue(&ref.v);                // Compute rate of change.

        // Cycle Check - REG_ERR : Regulation fault

        if (reg_bi_err.flags.fault)
        {
            CycSaveCheckWarning(TRIGG_LOG, FGC_CYC_WRN_REG_ERR, FGC_WRN_REG_ERROR, 0.0, 0.0, 0.0, 0.0);
            CycLogAbsMaxErr();
            refto.flags.end = TRUE;     // Will call RefToEnd() on the next regulation period
        }
    }
    else
    {
        // ------------------------------------------------------------------------------------------------
        // Use openloop voltage reference
        // ------------------------------------------------------------------------------------------------

        ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, ref.v_openloop, ref.v.value);

        RefRefreshValue(&ref.v);                // Compute rate of change.

        ref.fg.value = ref.fg_plus_rt.value = ref.b.value = 0.0;

        // rates of change will all be zero

        //RefRefreshValue(&ref.fg_plus_rt);       // Compute rate of change.
        //RefRefreshValue(&ref.fg);               // Compute rate of change.
        //RefRefreshValue(&ref.b);                // Compute rate of change.

        // B error = 0.0

        regErrCalc(&reg_bi_err, FALSE, 0.0, 0.0);
    }
}

static void IsrSendRefDAQ(float ref)
{
    ref = (ref - property.vs.offset) / property.vs.gain;

    UniSwitchVRef(&ref);

    IsrDacSet(0, ref);
}

static void IsrSendRefSpivs(float ref)
{
    /*
     * This function first retrieves the values in the SPIVS rx buffer and then
     * the refernce, measurement, etc are sent. This avoids having to wait for
     * the transmission to complete in order to read the SPIVS rx contents.
     *
     * The caveat is that the words sent and the words received are offset
     * by two transmissions: one due to reading before writing and the second
     * due to the DSP card sending the value it receives in the following
     * transmission.
     *
     * Thus a buffer is needed to store the value sent two transmissions
     * before and be able to check for errors.
     *
     * Because of the above the first two transmissions result in a mismatch
     * of the value sent and received. Hence num_fails = - 2.
     *
     * The parameters sent over the SPIVS are the reference, current measurement
     * and the inductance. The latter is used by some converters (e.g. SIRIUS)
     * to implement in the DSP card power balance between the capacitors and
     * the load. As a result, for different converters with the same crate type
     * DSP_IGBT (APOLO, COMET, SATURN, SIRIUS, etc) the DSP card only needs two
     * or three parameters and thus only sends back two or three words. For this
     * reason the SPIVS transmission validation is only done on the first two
     * words sent instead of all of them.
     *
     * The reference sent is the actual V value, without applying VS.GAIN.
     */

    static float tx_values[2][3];
    static uint8_t indx = 0;
    static int8_t  num_fails = -2;

    uint32_t rx_values[2];

    spivsRead(rx_values, 2);

    if (   spivsValidate((uint32_t const *)tx_values[indx], rx_values, 2) == SPIVS_INVALID
        && rtcom.state_op == FGC_OP_NORMAL)
    {
        // The fails must be consecutive

        if (++num_fails > MAX_NUM_SPIVS_FAILS)
        {
            Set(rtcom.st_latched, FGC_LAT_SPIVS_FLT);
            Set(rtcom.faults, FGC_FLT_FGC_HW);
        }
    }
    else
    {
        num_fails = 0;
    }

    tx_values[indx][0] = (ref - property.vs.offset);
    tx_values[indx][1] = meas.i;
    tx_values[indx][2] = load.active->pars.henrys_sat;

    spivsSend((uint32_t *)tx_values[indx], 3);

    indx ^= 1;
}

static void IsrUpdateAcquisition(void)
{
    static BOOLEAN only_once = TRUE;

    if (((FP32)cyc.time.s * 1000) >= property.meas.acq_delay[cyc.user])
    {
        if (only_once)
        {
            switch (reg.active->state)
            {
                case FGC_REG_I:
                    property.meas.acq_value[cyc.user] = meas.i;
                    break;

                case FGC_REG_V:
                    property.meas.acq_value[cyc.user] = meas.v;
                    break;

                case FGC_REG_B:
                    property.meas.acq_value[cyc.user] = meas.b;
                    break;

                default:
                    property.meas.acq_value[cyc.user] = 0.0;
                    break;
            }

            dpcls->dsp.notify.acq_meas_ready = cyc.user;

            only_once = FALSE;
        }
    }
    else
    {
        only_once = TRUE;
    }
}

// Class specific function definitions

void IsrClassInit(void)
{
    switch (crateGetType())
    {
        case FGC_CRATE_TYPE_DSP_IGBT:
        // Fall through
        
        case FGC_CRATE_TYPE_RF25KV:
            IsrSendRef = &IsrSendRefSpivs;
            break;

        // The remaining converters use the DAC

        default:
            IsrSendRef = &IsrSendRefDAQ;
            break;
    }

    // Take the current measurement value and not the filtered one. For further
    // details look into meas.c::MeasSelect()

    reg.hi_speed_f = TRUE;
}

void IsrClassMain(void)
{
    // Estimate rate of change of field, current and voltage based on last four samples

    MeasHistory(&meas.i_hist, meas.i);
    MeasHistory(&meas.b_hist, meas.b);

    // Estimate the rate of change of the current on the last four regulation periods

    if (dpcls->dsp.reg.run_f)
    {
        MeasHistoryRegPeriod(&meas.i_hist_reg, meas.i_hist.meas);
    }

    // Perform start of cycle Field-Current-Voltage checks when cycling

    // CycBIVchecks() not implemented. Is this needed?
    //    if(property.device.cyc == FGC_CTRL_ENABLED)
    //    {
    //        CycBIVchecks();
    //    }


    // If not in TEST mode then process according to regulation state

    if (rtcom.state_op != FGC_OP_TEST)
    {
        BOOLEAN run_regulation_f = TRUE;

        // Do not run the regulation if the voltage source is blocked (except when
        // transitioning to ON_STANBY).

        if (  dpcls->mcu.vs.blockable >= FGC_VS_BLOCK_SUP_B0
           && PcStateAbove(FGC_PC_ON_STANDBY))
        {
            dig_sup_channel_e channel = (dig_sup_channel_e)(DIG_SUP_B_CHANNEL0 << (dpcls->mcu.vs.blockable - FGC_VS_BLOCK_SUP_B0));

            if (digSupGetStatus(channel) == DIG_SUP_LOW)
            {
                run_regulation_f = FALSE;
            }
        }

        if (run_regulation_f)
        {
            switch (reg.active->state)
            {
                case FGC_REG_B:

                    if (dpcls->dsp.reg.run_f)
                    {
                        IsrRegStateB();
                    }

                    break;


                case FGC_REG_I:

                    if (dpcls->dsp.reg.run_f)
                    {
                        IsrRegStateI();
                    }

                    break;

                case FGC_REG_V:

                    IsrRegStateV();
                    break;
            }
        }
        else
        {
            ref.v.value = 0;
        }

        // Calculate V error (This is an average over a regulation period)

        ref.v_err_avg = regErrCalc(&reg_v_err, TRUE, ref.v.value, meas.v);

        if (PcStateAboveEqual(FGC_PC_OFF) == TRUE)
        {
            IsrSendRef(ref.v.value);
        }

        // Allow the COMHV or reception crates to manually set the value of DAC-B

        if ((crateGetType() == FGC_CRATE_TYPE_COMHV_PS) || (crateGetType() == FGC_CRATE_TYPE_RECEPTION))
        {
            if (PcStateAboveEqual(FGC_PC_TO_STANDBY))
            {
                if (property.vs.i_limit.gain == 0)
                {
                    property.vs.i_limit.gain = 1.0;
                }

                IsrDacSet(1, (property.vs.i_limit.ref / property.vs.i_limit.gain));
            }
            else
            {
                IsrDacSet(1, 0.0);
            }
        }
    }

    // Cycle Check - VMEAS_VREF : Voltage regulation fault

    if (ref.cycling_f && reg_v_err.flags.fault)
    {
        CycSaveCheckFault(FGC_CYC_FLT_VMEAS_VREF, FGC_WRN_V_ERROR, 0.0, 0.0, 0.0, 0.0);
    }

    // Shift RST history and get the filtered voltage reference (= average of the actuation)

    if (dpcls->dsp.reg.run_f)
    {
        ref.v_fltr = regRstHistory(&reg_vars);
    }

    // Report data to the MCU on the start of each ms

    if (rtcom.start_of_ms_f)
    {
        Report();
    }

    // Update acquisition measurement if PPM

    if (cyc.user != 0)
    {
        IsrUpdateAcquisition();
    }
}

// EOF
