/*!
 *  @file      init_class.h
 *  @defgroup  FGC3:MCU:61
 *  @brief     Class specific initialization.
 */

#ifndef INIT_CLASS_H
#define INIT_CLASS_H

#ifdef INIT_CLASS_GLOBALS
#define INIT_CLASS_VARS_EXT
#else
#define INIT_CLASS_VARS_EXT extern
#endif

#define DEFPROPS_INC_ALL

// Includes

#include <cc_types.h>
#include <defprops.h>

// Constants

#define SYM_LST_REF sym_lst_62_ref

// External structures, unions and enumerations

struct init_dynflag_timestamp_select
{
    struct prop        *        prop;                   // Pointer to property
    enum timestamp_select       timestamp_select;       // dynamic flags
} init_dynflag_timestamp_select_t;

// External variable definitions

INIT_CLASS_VARS_EXT char devtype[]
#ifdef INIT_CLASS_GLOBALS
=
{
    "RFNA"
}
#endif
;

INIT_CLASS_VARS_EXT INT32S  conf_l[]
#ifdef INIT_CLASS_GLOBALS
=
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ CAL_DEF_INTERNAL_ADC_GAIN,
    /* 3 */ CAL_DEF_EXTERNAL_ADC_GAIN,
    /* 4 */ 0
}
#endif
;

INIT_CLASS_VARS_EXT INT16U  conf_s[]
#ifdef INIT_CLASS_GLOBALS
=
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ FGC_OP_NORMAL
}
#endif
;

INIT_CLASS_VARS_EXT INT8U  conf_b[]
#ifdef INIT_CLASS_GLOBALS
=
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ 0,
}
#endif
;

INIT_CLASS_VARS_EXT FP32    conf_f[]
#ifdef INIT_CLASS_GLOBALS
=
{
    0.0, 0.0, 0.0, 8.0, -8.0, 10.0, 0.1
}
#endif
;

INIT_CLASS_VARS_EXT FP32    conf_vs_sim[FGC_N_VS_SIM_COEFFS]
#ifdef INIT_CLASS_GLOBALS
=
{
    1.0, 0.0, 0.0, 0.0
}
#endif
;

INIT_CLASS_VARS_EXT FP32    conf_vs_delay
#ifdef INIT_CLASS_GLOBALS
    = 0.001
#endif
      ;

INIT_CLASS_VARS_EXT INT32U  log_oasis_sub
#ifdef INIT_CLASS_GLOBALS
    = 1
#endif
      ;

INIT_CLASS_VARS_EXT INT16U  inter_fgc_sig
#ifdef INIT_CLASS_GLOBALS
= 0
#endif
;

INIT_CLASS_VARS_EXT INT16U  inter_fgc_isr
#ifdef INIT_CLASS_GLOBALS
= 400
#endif
;

INIT_CLASS_VARS_EXT char inter_fgc_none[]
#ifdef INIT_CLASS_GLOBALS
= "NONE"
#endif
;

// CAUTION: If the property is NON-VOLATILE, its default size (numels in the
//          XML definition) must be zero for it to be initialised based on the
//          data in init_prop

INIT_CLASS_VARS_EXT struct init_prop init_prop[]
#ifdef INIT_CLASS_GLOBALS
        =
{
    // Non-volatile, non-config properties

    {   &PROP_MODE_OP,                          1,                      &conf_s[2]      },
    {   &PROP_CAL_A_ADC_INTERNAL_ERR,           3,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_INTERNAL_ERR,           3,                      &conf_f[0]      },
    {   &PROP_CAL_C_ADC_INTERNAL_ERR,           3,                      &conf_f[0]      },
    {   &PROP_CAL_D_ADC_INTERNAL_ERR,           3,                      &conf_f[0]      },
    {   &PROP_ADC_INTERNAL_LAST_CAL_TIME,       1,                      &conf_l[4]      },
    {   &PROP_FGC_NAME,                         3,                      &devtype[0]     },
    {   &PROP_DEVICE_TYPE,                      3,                      &devtype[0]     },

    // Global config properties
    {   &PROP_DEVICE_CYC,                       1,                      &conf_l[0]      },
    {   &PROP_DEVICE_PPM,                       1,                      &conf_s[0]      },

    {   &PROP_CAL_A_DCCT_HEADERR,               1,                      &conf_f[0]      },
    {   &PROP_CAL_A_DCCT_ERR,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_A_DCCT_TC,                    3,                      &conf_f[0]      },
    {   &PROP_CAL_A_DCCT_DTC,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_A_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_A_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_A_ADC_INTERNAL_DTC,           3,                      &conf_f[0]      },

    {   &PROP_CAL_B_DCCT_HEADERR,               1,                      &conf_f[0]      },
    {   &PROP_CAL_B_DCCT_ERR,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_B_DCCT_TC,                    3,                      &conf_f[0]      },
    {   &PROP_CAL_B_DCCT_DTC,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_B_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_INTERNAL_DTC,           3,                      &conf_f[0]      },

    {   &PROP_CAL_C_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_C_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_C_ADC_INTERNAL_DTC,           3,                      &conf_f[0]      },

    {   &PROP_CAL_D_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_D_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_D_ADC_INTERNAL_DTC,           3,                      &conf_f[0]      },

    {   &PROP_CAL_VREF_ERR,                     3,                      &conf_f[0]      },
    {   &PROP_CAL_VREF_TC,                      3,                      &conf_f[0]      },

    {   &PROP_LOG_OASIS_SUBSAMPLE,              1,                      &log_oasis_sub  },

    {   &PROP_BARCODE_FGC_CASSETTE,             0,                      NULL            },
    {   &PROP_BARCODE_A_DCCT_HEAD,              0,                      NULL            },
    {   &PROP_BARCODE_A_DCCT_ELEC,              0,                      NULL            },

    // Normal properties

    {   &PROP_CAL_A_DAC,                        3,                      &conf_f[2]      },
    {   &PROP_CAL_B_DAC,                        3,                      &conf_f[2]      },
    {   &PROP_ADC_INTERNAL_TAU_TEMP,            1,                      &conf_f[6]      },
    {   &PROP_VS_SIM_INTLKS,                    1,                      &conf_s[1]      },

    {   &PROP_INTER_FGC_SLAVES,                 1,                      &inter_fgc_none[0] },
    {   &PROP_INTER_FGC_MASTERS,                1,                      &inter_fgc_none[0] },
    {   &PROP_INTER_FGC_CONSUMERS,              1,                      &inter_fgc_none[0] },
    {   &PROP_INTER_FGC_SIG_SOURCES,            1,                      &inter_fgc_none[0] },

    {   &PROP_INTER_FGC_PRODUCED_SIGS,          1,                      &inter_fgc_sig  },

    {   &PROP_INTER_FGC_BROADCAST,              1,                      &conf_s[0]      },
    {   &PROP_INTER_FGC_ISR_OFFSET_US,          1,                      &inter_fgc_isr  },
    {   &PROP_INTER_FGC_TOPOLOGY,               1,                      &inter_fgc_sig  },

    {   NULL    }       // End of list
}
#endif
;

// The DF_TIMESTAMP_SELECT dynamic flag is set for all the properties in the
// following list and their children. The same list is also used in function
// CmdPrintTimestamp to read the timestamp selector.

INIT_CLASS_VARS_EXT struct init_dynflag_timestamp_select init_dynflag_timestamp_select[]
#ifdef INIT_CLASS_GLOBALS
        =
{
    {   &PROP_REF_CYC_FAULT,                    TIMESTAMP_CURRENT_CYCLE         },
    {   &PROP_REF_CYC_WARNING,                  TIMESTAMP_CURRENT_CYCLE         },
    {   &PROP_LOG_OASIS,                        TIMESTAMP_LOG_OASIS             },
    {   &PROP_LOG_CAPTURE_SIGNALS,              TIMESTAMP_LOG_CAPTURE           },
    {   &PROP_MEAS_ACQ_VALUE,                   TIMESTAMP_PREVIOUS_CYCLE        },
    {   &PROP_MEAS_ACQ_PULSES,                  TIMESTAMP_PREVIOUS_CYCLE        },

    {   NULL    }       // End of list
}
#endif
;

#endif

// EOF
