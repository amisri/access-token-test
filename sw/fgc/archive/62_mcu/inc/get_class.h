/*!
 *  @file      get_class.h
 *  @defgroup  FGC:MCU:62
 *  @brief     Class specific get functions
 */

#ifndef GET_CLASS_H
#define GET_CLASS_H

#ifdef GET_CLASS_GLOBALS
#define GET_CLASS_VARS_EXT
#else
#define GET_CLASS_VARS_EXT extern
#endif

// Constants

#define FGC_PM_BUF_LEN  FGC_PM_BUF_LEN_62
#define SYM_LST_SPY     sym_lst_62_spy

#endif

// EOF
