/*!
 *  @file      class.c
 *  @defgroup  FGC:MCU:62
 *  @brief     This file contains all class 62 related definitions.
 *  The CLASS_ID constant is not defined when header files are parsed because
 *  defs.h must be the last header file. So all class related definitions are
 *  collected in this file for class 62.
 */

#ifndef CLASS_H      // header encapsulation
#define CLASS_H

#ifdef CLASS_GLOBALS
#define CLASS_VARS_EXT
#else
#define CLASS_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>

// Constants

// MST constants

#define DEV_STA_TSK_PHASE   2      // Millisecond to run StaTsk()



// Warnings/unlatched/latched status masks

#define FGC_FAULTS      (   FGC_FLT_FGC_HW          \
                            | FGC_FLT_REG_ERROR       \
                            | FGC_FLT_POL_SWITCH      \
                            | FGC_FLT_FGC_STATE       \
                            | FGC_FLT_LIMITS )

#define FGC_WARNINGS    (   FGC_WRN_FGC_HW          \
                            | FGC_WRN_I_MEAS          \
                            | FGC_WRN_REG_ERROR       \
                            | FGC_WRN_TEMPERATURE     \
                            | FGC_WRN_CONFIG )

// All DSP warning bits MUST be here to be cleared

#define ALL_DSP_WARNINGS_MASK (   FGC_WRN_I_MEAS      \
                                  | FGC_WRN_TIMING_EVT )

#define DSP_UNLATCHED   (   FGC_UNL_I_MEAS_DIFF )

#define DSP_MEAS        (     FGC_ADC_STATUS_V_MEAS_OK      \
                            | FGC_ADC_STATUS_IN_USE         \
                            | FGC_ADC_STATUS_SIGNAL_STUCK   \
                            | FGC_ADC_STATUS_CAL_FAILED )

#define DSP_DCCT_MASK   (   FGC_DCCT_CAL_FLT       \
                            | FGC_DCCT_FAULT )

#define ST_LATCHED_RESET (    FGC_LAT_PSU_V_FAIL      \
                            | FGC_LAT_DIM_SYNC_FLT    \
                            | FGC_LAT_DIMS_EXP_FLT    \
                            | FGC_LAT_DSP_FLT         \
                            | FGC_LAT_DIM_TRIG_FLT    \
                            | FGC_LAT_DAC_FLT         \
                            | FGC_LAT_DALLAS_FLT      \
                            | FGC_LAT_SPIVS_FLT )

#define SYM_LST_CYC_FLT &sym_lst_62_cyc_flt[0]
#define SYM_LST_CYC_WRN &sym_lst_62_cyc_wrn[0]

/*
     reg=00000 cy=1 then shl (<<) number
     or
     reg=00001 then shl (<<) (number-1)

     1 - > 0x0001,
     2 - > 0x0002,
     3 - > 0x0004,
     4 - > 0x0008,
     5 - > 0x0010,
     6 - > 0x0020,
     7 - > 0x0040,
     8 - > 0x0080,
     9 - > 0x0100,
    10 - > 0x0200,
    11 - > 0x0400,
    12 - > 0x0800,
    13 - > 0x1000,
    14 - > 0x2000,
    15 - > 0x4000,
    16 - > 0x8000,

 */

// External variable definitions

CLASS_VARS_EXT INT16U   const masks[16]
#ifdef CLASS_GLOBALS
=
{
    0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080,
    0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000, 0x4000, 0x8000,
}
#endif
;

#endif

// EOF
