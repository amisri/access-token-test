/*!
 *  @file     log_class.h
 *  @defgroup FGC:MCU:61
 *  @brief    Logging related functions for class 61.
 */

#ifndef LOG_CLASS_H
#define LOG_CLASS_H

#ifdef LOG_CLASS_GLOBALS
#define LOG_CLASS_VARS_EXT
#else
#define LOG_CLASS_VARS_EXT extern
#endif

// Includes

#include <defprops.h>
#include <log_event.h>


#define FGC_LAST_LOG log_iearth


// External function declarations

/*!
 * This function stores the pulse measurements.
 */
void LogPulseMeas(void);

// External variable definitions

// These properties are logged in logEvent whenever they change.
// At the same time they are also published if needed (no need to
// explicitly call PubProperty).

LOG_CLASS_VARS_EXT struct log_evt_prop log_evt_prop[]
#ifdef LOG_CLASS_GLOBALS
            =     // Property pointer                Old value       +-Property Name----------+
{
    {   &PROP_STATE_PC,                 0xFFFF,         "STATE.PC"                      },
    {   &PROP_STATE_OP,                 0xFFFF,         "STATE.OP"                      },
    {   &PROP_CONFIG_STATE,             0xFFFF,         "CONFIG.STATE"                  },
    {   &PROP_CONFIG_MODE,              0xFFFF,         "CONFIG.MODE"                   },
    {   &PROP_FGC_PLL_STATE,            0xFFFF,         "FGC_PLL.STATE"                 },
    {   &PROP_VS_STATE,                 0xFFFF,         "VS.STATE"                      },
    {   &PROP_LOG_PM_TRIG,              0x0000,         "LOG.PM.TRIG"                   },
    {   &PROP_DIG_STATUS,               0x0000,         "DIG.STATUS"                    },
    {   &PROP_DIG_IP_DIRECT,            0x0000,         "DIG.IP_DIRECT"                 },
    {   &PROP_STATUS_FAULTS,            0x0000,         "STATUS.FAULTS"                 },
    {   &PROP_STATUS_WARNINGS,          0x0000,         "STATUS.WARNINGS"               },
    {   &PROP_STATUS_ST_LATCHED,        0x0000,         "STATUS.ST_LATCHED"             },
    {   &PROP_STATUS_ST_UNLATCHED,      0x0000,         "STATUS.ST_UNLATCHED"           },
    {   &PROP_ADC_STATUS_A,             0x0000,         "ADC.STATUS.A"                  },
    {   &PROP_ADC_STATUS_B,             0x0000,         "ADC.STATUS.B"                  },
    {   &PROP_ADC_STATUS_C,             0x0000,         "ADC.STATUS.C"                  },
    {   &PROP_ADC_STATUS_D,             0x0000,         "ADC.STATUS.D"                  },
    {   &PROP_DCCT_A_STATUS,            0x0000,         "DCCT.A.STATUS"                 },
    {   &PROP_DIG_COMMANDS,             0x0000,         "DIG.COMMANDS"                  },
    {   &PROP_FGC_SECTOR_ACCESS,        0x0000,         "FGC.SECTOR_ACCESS"             },
    {   NULL    },
}
#endif
;

// FGC_SELF_PM_REQ must be cleared before the 10 seconds of gateway timeout
// 3840 samples at 1ms : 3.84 seconds
// post trigger is at 3840 / 6 = 640 samples (0.640 seconds)

LOG_CLASS_VARS_EXT struct log_ana_vars log_pulse
#ifdef LOG_CLASS_GLOBALS
        =
{
    SRAM_LOG_IREG_32,                           // Buffer base address
    SRAM_LOG_IREG_W,                            // Buffer size in words
    STP_PULSE,                                  // Sym_idx for log type
    FGC_LOG_PULSE_LEN,                          // Number of samples in the log
    FGC_LOG_PULSE_LEN / 6,                      // Number of post trig samples
    4,                                          // Number of signals per sample
    16,                                         // Number of bytes per sample
    0,                                          // Wait time (ms)
    1,                                          // Period in milliseconds (1 KHz)
    { 0, 0, 0, 0 },                             // Signal info
    {
        "REF:A",    "I_MEAS:A",                    // Signal labels and units
        "V_MEAS:V", "V_CAPA:V"
    },
}
#endif
;

#endif

// EOF
