/*!
 *  @file      event_fsm_class.h
 *  @defgroup  FGC:MCU:62
 *  @brief     File providing the transition and state functions specific to class 62 for the
 *             event-driven state machine.
 */

#ifndef FGC_EVENT_FSM_CLASS_H
#define FGC_EVENT_FSM_CLASS_H

#ifdef EVENT_FSM_CLASS_GLOBALS
#define EVENT_FSM_CLASS_VARS_EXT
#else
#define EVENT_FSM_CLASS_VARS_EXT extern
#endif


// Includes

#include <stdint.h>

#include <mcu_dsp_common.h>



// External functions declaration

/*!
 * Next event to process
 */
void eventsFsmClassProcessEvent(struct abs_time_ms const * start, uint8_t user);


#endif

// EOF
