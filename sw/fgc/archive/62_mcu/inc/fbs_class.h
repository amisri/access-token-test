/*!
 *  @file      fbs_class.h
 *  @defgroup  FGC:MCU:62
 *  @brief     Handlers to published data variables.
 */


#ifndef FBS_CLASS_H
#define FBS_CLASS_H

#ifdef FBS_CLASS_GLOBALS
#define FBS_CLASS_VARS_EXT
#else
#define FBS_CLASS_VARS_EXT extern
#endif

// Includes

#include <dpcls.h>
#include <fbs.h>

// External structures, unions and enumerations

#define FAULTS          fbs.u.fgc_stat.class_data.c62.st_faults
#define WARNINGS        fbs.u.fgc_stat.class_data.c62.st_warnings
#define ST_LATCHED      fbs.u.fgc_stat.class_data.c62.st_latched
#define ST_UNLATCHED    fbs.u.fgc_stat.class_data.c62.st_unlatched
#define STATE_PLL       fbs.u.fgc_stat.class_data.c62.state_pll
#define STATE_OP        fbs.u.fgc_stat.class_data.c62.state_op
#define STATE_VS        fbs.u.fgc_stat.class_data.c62.state_vs
#define STATE_PC        fbs.u.fgc_stat.class_data.c62.state_pc
#define ST_MEAS_A       fbs.u.fgc_stat.class_data.c62.st_adc_a
#define ST_MEAS_B       fbs.u.fgc_stat.class_data.c62.st_adc_b
#define ST_MEAS_C       fbs.u.fgc_stat.class_data.c62.st_adc_c
#define ST_MEAS_D       fbs.u.fgc_stat.class_data.c62.st_adc_d
#define ST_DCCT_A       fbs.u.fgc_stat.class_data.c62.st_dcct_a
#define I_ERR_MA        fbs.u.fgc_stat.class_data.c62.i_err_ma
#define I_EARTH_PCNT    fbs.u.fgc_stat.class_data.c62.i_earth_pcnt
#define REF             fbs.u.fgc_stat.class_data.c62.i_ref
#define I_MEAS          fbs.u.fgc_stat.class_data.c62.i_meas
#define V_MEAS          fbs.u.fgc_stat.class_data.c62.v_meas
#define V_CAPA          fbs.u.fgc_stat.class_data.c62.v_capa

// Use the DCCT_A status for DCCT_B. Once a more generic signal/channel
// structure is implemented, this should be cleaned up.

#define ST_DCCT_B       fbs.u.fgc_stat.class_data.c62.st_dcct_a

#endif

// EOF

