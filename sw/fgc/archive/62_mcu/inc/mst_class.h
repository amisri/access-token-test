/*!
 *  @file      mst_class.h
 *  @defgroup  FGC:MCU:62
 *  @brief     Class related millisecond actions.
 */

#ifndef MST_CLASS_H
#define MST_CLASS_H

#ifdef MST_CLASS_GLOBALS
#define MST_CLASS_VARS_EXT
#else
#define MST_CLASS_VARS_EXT extern
#endif

#endif

// EOF
