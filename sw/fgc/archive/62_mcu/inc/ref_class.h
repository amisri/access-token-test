/*!
 *  @file     ref_class.h
 *  @defgroup FGC3:MCU:62
 *  @brief    Reference-related functions specific to Class 61
 */

#ifndef FGC_REF_CLASS_H
#define FGC_REF_CLASS_H

#ifdef REF_CLASS_GLOBALS
#define REF_CLASS_VARS_EXT
#else
#define REF_CLASS_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <cmd.h>

// External function declarations

/*!
 * This function is called when the user enters "s ref pulse,..." to process the
 * pulse reference setup.
 *
 * @param c Command structure.
 *
 * @retval FGC_OK_NO_RSP if no error occurred.
 */
INT16U RefPulse(struct cmd * c);

#endif

// EOF
