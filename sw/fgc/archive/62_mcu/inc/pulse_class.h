/*!
 *  @file      pulse_class.h
 *  @defgroup  FGC:MCU:62
 *  @brief     Provides the functionality to generate and read-back a pulse
 *             using the supplemental digital I/O.
 */

#ifndef FGC_PULSE_H
#define FGC_PULSE_H

#ifdef PULSE_GLOBALS
#define PULSE_VARS_EXT
#else
#define PULSE_VARS_EXT extern
#endif


// Includes

#include <stdint.h>
#include <dig.h>
#include <defconst.h>


// External structures, unions and enumerations

/*!
 * This structure contains the pulse limits.
 */
typedef struct pulse_limits
{
    float  pos;                          //!< LIMITS.PULSE.POS
    float  min;                          //!< LIMITS.PULSE.MIN
    float  neg;                          //!< LIMITS.PULSE.NEG
    float  error_warning;                //!< LIMITS.PULSE.ERR_WARNING
    float  error_fault;                  //!< LIMITS.PULSE.ERR_FAULT
} pulse_limits_t;


/*!
 * This structure contains the pulse status.
 */
typedef struct pulse_log
{
    int32_t status     [FGC_MAX_USER_PLUS_1];   //!< LOG.PULSE.STATUS
    float   max_abs_err[FGC_MAX_USER_PLUS_1];   //!< LOG.PULSE.MAX_ABS_ERR
} pulse_log_t;


/*!
 * This structure contains the reported pulse values.
 */
typedef struct pulse_report
{
    float   ref;                         //!< Pulse reference
    float   meas_i;                      //!< Current pulse measurement
    float   meas_v;                      //!< Voltage pulse measurement
    float   error;                       //!< Error between the reference and measurement.
    int32_t error_mav;                   //!< Error between the reference and measurement in mA or mV.
} pulse_report_t;


// External variable definitions

PULSE_VARS_EXT pulse_limits_t pulse_limits;
PULSE_VARS_EXT pulse_log_t    pulse_log;
PULSE_VARS_EXT pulse_report_t pulse_report;
PULSE_VARS_EXT char           pulse_units;


// External function declarations

/*
 * Initializes the pulse
 */
void PulseInit(void);

/*!
 * Sets the pulse reference value
 *
 * @param  user                   Cycle user
 * @param  value                  Pulse value
 * @return FGC_OK_NO_RSP          The reference is correct.
 * @return FGC_OUT_OF_LIMITS      The reference is out of limits.
 * @return FGC_BAD_CYCLE_SELECTOR The cycle selector is not valid.
 */
uint16_t PulseSetRef(uint16_t user, float value);

/*!
 * Sets the local reference value
 *
 * @param  value             Local reference value
 * @return FGC_OK_NO_RSP     The reference is correct.
 * @return FGC_OUT_OF_LIMITS The reference is out of limits.
 */
uint16_t PulseSetRefLocal(float value);


#endif

// EOF
