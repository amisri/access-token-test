/*!
 *  @file      defprops_inc_class.h
 *  @brief     Adds all the class-specific headers needed by defprops.h
 */

#ifndef DEFPROP_CLASS_ALL_H
#define DEFPROP_CLASS_ALL_H

#include <pulse_class.h>
#include <timing_pulse_class.h>

#endif

// EOF
