/*!
 *  @file      init_class.c
 *  @defgroup  FGC3:MCU:62
 *  @brief     Class specific initialization.
 */

#define INIT_CLASS_GLOBALS
#define REF_GLOBALS

// Include

#include <init_class.h>
#include <dev.h>
#include <dpcom.h>
#include <dpcls.h>
#include <macros.h>
#include <ana_temp_regul.h>
#include <ref.h>
#include <regfgc3_pars.h>
#include <inter_fgc.h>

// Class specific function definitions

void InitClassPreInts(void)
{
    // Read SD filter function meta data

    AdcFiltersReadFunctions();
}

void InitClassPostInts(void)
{
    NvsInit(&init_prop[0]);

    dpcls.mcu.vs.polarity.changed = FALSE;

    // Analogue Card temperature regulation
    // Make sure to call this function AFTER NvsInit
    // because it needs the config value CAL.VREF.TEMPERATURE.TARGET

    AnaTempInit();
    interFgcPopulateStatus();

    // Other initialisations

    dev.max_user = (dpcom.mcu.device_ppm ? FGC_MAX_USER : 0);

    dpcls.dsp.log.pulse_acq_user = -1;

    if (DEVICE_CYC == FGC_CTRL_ENABLED)
    {
        rtd.ctrl = FGC_RTD_CYC;
    }

    // For standalone mode there is no post sync mechanism, we retrieve RegFGC3 parameters here

    if (StaIsStandalone() || Test(dev.omode_mask, FGC_LHC_SECTORS_NO_CONFIG_MGR) == true)
    {
        regFgc3ParsRetrieveParams();
    }

    PulseInit();
}

void InitClassPostSync(struct cmd * c)
{

}

// EOF
