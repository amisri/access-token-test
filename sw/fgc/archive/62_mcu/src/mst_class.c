/*!
 *  @file      mst_class.c
 *  @defgroup  FGC:MCU:62
 *  @brief     Class related millisecond actions.
 */

#define MST_CLASS_GLOBALS

#include <mst_class.h>

#include <class.h>
#include <dev.h>
#include <dls.h>
#include <dpcls.h>
#include <dpcom.h>
#include <fbs_class.h>
#include <log_class.h>
#include <log.h>
#include <macros.h>
#include <mst.h>
#include <shared_memory.h>
#include <sta.h>
#include <pub.h>
#include <pulse_class.h>
#include <events_class.h>


// Constants

#define  MST_LOG_IEARTH_PHASE         5

// Class specific function definitions

void MstClassProcess(void)
{
    eventsClassProcess();
}

void MstClassPublishData(void)
{
    // Millisecond 1 of 10: Read analogue values.

    if ((mst.ms_mod_10 == 1) &&
        (shared_mem.mainprog_seq == FGC_MP_MAIN_RUNNING) &&
        (STATE_OP != FGC_OP_UNCONFIGURED))
    {
        REF      = pulse_report.ref;
        I_MEAS   = pulse_report.meas_i;
        V_MEAS   = pulse_report.meas_v;
        V_CAPA   = dpcls.dsp.meas.v_capa;
        I_ERR_MA = (int16_t)pulse_report.error_mav;
    }

    //  Millisecond 1 of 20: Read DSP status and run Gateway PC_PERMIT timeout

    if (mst.ms_mod_20 == 1)
    {
        FAULTS      |= (INT16U) dpcom.dsp.faults;               // Latch faults from DSP
        ST_LATCHED  |= (INT16U) dpcom.dsp.st_latched;           // Latch status from DSP

        WARNINGS     = (WARNINGS & ~ALL_DSP_WARNINGS_MASK) |    // Mix in DSP warnings
                       (INT16U) dpcom.dsp.warnings;
        ST_UNLATCHED = (ST_UNLATCHED & ~DSP_UNLATCHED) |        // Mix in DSP unlatched status
                       (INT16U) dpcom.dsp.st_unlatched;

        ST_MEAS_A    = (ST_MEAS_A & ~DSP_MEAS) |                // Mix in DSP meas A flags
                       (INT8U) dpcls.dsp.meas.st_meas[0];
        ST_MEAS_B    = (ST_MEAS_B & ~DSP_MEAS) |                // Mix in DSP meas B flags
                       (INT8U) dpcls.dsp.meas.st_meas[1];
        ST_MEAS_C    = (ST_MEAS_C & ~DSP_MEAS) |                // Mix in DSP meas C flags
                       (INT8U) dpcls.dsp.meas.st_meas[2];
        ST_MEAS_D    = (ST_MEAS_D & ~DSP_MEAS) |                // Mix in DSP meas D flags
                       (INT8U) dpcls.dsp.meas.st_meas[3];

        ST_DCCT_A    = (ST_DCCT_A & ~DSP_DCCT_MASK) |           // Mix in DSP dcct A flags
                       (INT8U) dpcls.dsp.meas.st_dcct[0];

        I_EARTH_PCNT = (INT8S) meas_i_earth.pcnt;
    }
}

void MstClassLog(void)
{
    // Millisecond 5 of 20: Log Iearth signal

    if (mst.ms_mod_20 == MST_LOG_IEARTH_PHASE)
    {
        if (log_iearth.samples_to_acq)
        {
            LogIearth();
        }
    }

    // Every millisecond

    if (log_pulse.samples_to_acq)
    {
        LogPulseMeas();
    }

    // Check if the pulse acquisitions need published

    if (dpcls.dsp.log.pulse_acq_user != -1)
    {
        PubProperty(&PROP_MEAS_ACQ_VALUE,  dpcls.dsp.log.pulse_acq_user, false);
        PubProperty(&PROP_MEAS_ACQ_PULSES, dpcls.dsp.log.pulse_acq_user, false);

        dpcls.dsp.log.pulse_acq_user = -1;
    }

    // Check and log start of PPM cycles

    if ((DEVICE_CYC == FGC_CTRL_ENABLED) &&
        (INT16U)dpcom.dsp.cyc.start_cycle_f)
    {
        // Trigger publication of property LOG.CYC by STA task

        sta.cyc_start_new_cycle_f = TRUE;

        dpcom.dsp.cyc.start_cycle_f = FALSE;
    }

    // Millisecond 0 of 1000: Check logging and ADC periodic reset

    if (TimeGetUtcTimeMs() == 0)
    {
        if (dev.log_pm_state == FGC_LOG_STOPPING &&
            FGC_LAST_LOG.samples_to_acq == 0)
        {
            dev.log_pm_state = FGC_LOG_FROZEN;

            Clr(fbs.u.fieldbus_stat.ack, FGC_SELF_PM_REQ);
        }
    }

    // Millisecond 9 of 1000: Log temperatures

    if (TimeGetUtcTimeMs() == 9)
    {
        mst.s_mod_600 = (INT16U)(TimeGetUtcTime() % 600L);
        mst.s_mod_10  = mst.s_mod_600 % 10;

        if (!mst.s_mod_10 && temp.thour_f)
        {
            LogThour();

            if (temp.tday_f)
            {
                LogTday();
            }

            temp.thour_f = FALSE;
        }
    }
}

// EOF
