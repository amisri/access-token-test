/*!
 *  @file      set_class.c
 *  @defgroup  FGC:MCU:62
 *  @brief     Class specific Set command functions.
 */

#include <set_class.h>
#include <dev.h>
#include <fgc_errs.h>
#include <dpcom.h>
#include <pars.h>


// External function definitions

uint16_t SetDevicePPM(struct cmd * c, struct prop * p)
{
    uint16_t  errnum;

    // If DEVICE.PPM has changed

    if (!(errnum = ParsSet(c, p)))
    {
        if (p == &PROP_DEVICE_PPM)
        {
            dev.max_user = (dpcom.mcu.device_ppm == FGC_CTRL_ENABLED ? FGC_MAX_USER : 0);
        }

        // PROP_DEVICE_MULTI_PPM
        
        else 
        {
            dev.max_sub_devs = (dpcom.mcu.device_multi_ppm == FGC_CTRL_ENABLED ? FGC_MAX_SUB_DEVS : 0);
        }
    }

    return (errnum);
}



uint16_t SetDefaults(struct cmd * c, struct prop * p)
{
    return (0);
}



uint16_t SetRef(struct cmd * c, struct prop * p)
{
    // Remove function when removing property REF.FUNC.TYPE

    return (0);
}



uint16_t SetRefPulse(struct cmd * c, struct prop * p)
{
    float      * value;
    uint16_t     errnum;

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum == FGC_OK_NO_RSP)
    {
        switch (p->sym_idx)
        {
            case STP_VALUE:     errnum = PulseSetRef(c->mux_idx, *value); break;
            case STP_REF_LOCAL: errnum = PulseSetRefLocal(*value);        break;
            default:            errnum = FGC_BAD_PARAMETER;               break;
        }
    }

    return (errnum);
}


// EOF
