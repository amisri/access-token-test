/*!
 *  @file      get_class.c
 *  @defgroup  FGC:MCU:62
 *  @brief     Class specific get functions
 */

#define GET_CLASS_GLOBALS

// Includes

#include <get_class.h>
#include <defprops.h>
#include <DIMsDataProcess.h>
#include <fgc_errs.h>
#include <fgc_log.h>
#include <fgc_parser_consts.h>
#include <log.h>
#include <macros.h>
#include <mcu_dependent.h>
#include <start.h>
#include <string.h>

// External function definitions

uint16_t GetLogPm(struct cmd * c, struct prop * p)
{
    // If array indices are specified

    if (c->n_arr_spec || c->step > 1)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    // If command is not via the FIP or BIN not specified

    if (c == &tcm || !Test(c->getopts, GET_OPT_BIN))
    {
        return (0);
    }

    // If logging in progress report BAD STATE. This should never occur.

    if (FGC_LAST_LOG.samples_to_acq)
    {
        return (FGC_BAD_STATE);
    }

    CmdStartBin(c, p, FGC_PM_BUF_LEN_62);

    LogGetPmEvtBuf(c);                  // Send Event  log  ( 49096     bytes)
    LogGetPmSigBuf(&log_pulse,  c);     // Send PULSE  log  ( 61440 + 8 bytes)
    LogGetPmSigBuf(&log_iearth, c);     // Send IEARTH log  (  4096 + 8 bytes)
    LogGetPmSigBuf(&log_thour,  c);     // Send THOUR  log  (  2880 + 8 bytes)

    return (0);
}

uint16_t GetLogCaptureSpy(struct cmd * c, struct prop * p)
{
    /*
     *   The capture log buffer (LOG.CAPTURE.BUF) contains up to 7200 records
     *   of 9 floats. These signals are:
     *       0   0x0001      REF
     *       1   0x0002      I_MEAS
     *       2   0x0004      V_MEAS
     *       3   0x0008      V_CAPA
     *       4   0x0010      Pulse B0
     *       5   0x0020      Pulse B1
     *       6   0x0040      Pulse B3
     *       7   0x0080      Pulse Acquisition
     */

    uint16_t    s;
    uint16_t    i;
    uint16_t    n_samples;
    uint16_t    errnum;
    prop_size_t sample_idx;
    char    *   ch_ptr;

    union
    {
        struct fgc_log_header      log;
        struct fgc_log_sig_header  sig;
    } header;

    static struct capture_log
    {
        char    *   label;
        char    *   units;
        uint16_t    info;
    }  signals[] =
    {
        { "REF",         "A",      0 },
        { "I_MEAS",      "A",      0 },
        { "V_MEAS",      "V",      0 },
        { "V_CAPA",      "V",      0 },
        { "B0",          "",       0 },
        { "B1",          "",       0 },
        { "B3",          "",       0 },
        { "ACQ",         "",       0 },
    };

    // Check that Get command and options are valid for a Spy log

    n_samples = dpcls.dsp.log.capture.n_samples;

    // Array indices cannot be specified

    if (c->n_arr_spec || c->step > 1)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    // BIN must be set

    if (!Test(c->getopts, GET_OPT_BIN))
    {
        return (FGC_BAD_GET_OPT);
    }

    // Make sure data is available

    if (!n_samples)
    {
        return (0);
    }

    // Make sure the user is correct

    if (c->mux_idx != 0 &&
        c->mux_idx != dpcls.dsp.log.capture.user)
    {
        return (FGC_BAD_CYCLE_SELECTOR);
    }

    // Generate log header.

    header.log.version   = FGC_LOG_VERSION;
    header.log.info      = 0;
    header.log.n_signals = FGC_LOG_CAPTURE_N_SIGS;
    header.log.n_samples = n_samples;
    header.log.period_us = dpcls.dsp.log.capture.sampling_period_us;
    header.log.unix_time = dpcls.dsp.log.capture.last_sample_time.unix_time;
    header.log.us_time   = dpcls.dsp.log.capture.last_sample_time.us_time;

    CmdStartBin(c, p, (uint32_t)(sizeof(header.log) + sizeof(header.sig) * FGC_LOG_CAPTURE_N_SIGS) +
                (uint32_t)(sizeof(float)) * (uint32_t)(FGC_LOG_CAPTURE_N_SIGS * header.log.n_samples));

    FbsOutBuf((uint8_t *) &header.log, sizeof(header.log), c);  // Write log header

    // Generate signal headers

    header.sig.type           = FGC_LOG_TYPE_FLOAT;
    header.sig.us_time_offset = 0;
    header.sig.gain           = 1.0;
    header.sig.offset         = 0.0;

    for (i = 0; i < FGC_LOG_CAPTURE_N_SIGS; i++)
    {
        header.sig.info = signals[i].info;

        strcpy(header.sig.label, signals[i].label);
        strcpy(header.sig.units, signals[i].units);

        header.sig.label_len = strlen(header.sig.label);
        header.sig.units_len = strlen(header.sig.units);

        // Write signal header

        FbsOutBuf((uint8_t *) &header.sig, sizeof(header.sig), c);
    }

    // Write data

    for (s = sample_idx = 0; s < n_samples; s++)
    {
        for (i = 0; i < FGC_LOG_CAPTURE_N_SIGS; i++)
        {
            errnum = PropValueGet(c, &PROP_LOG_CAPTURE_BUF, NULL, sample_idx + i, (uint8_t **)&ch_ptr);

            if (errnum != 0)
            {
                return (errnum);
            }

            FbsOutLong(ch_ptr, c);
        }

        sample_idx += FGC_LOG_CAPTURE_N_SIGS;
    }

    // Arm log capture to allow new Spy acquisitions, and reset LOG.CAPTURE.CYC_CHK_TIME

    dpcls.dsp.log.capture.state                  = FGC_LOG_ARMED;
    dpcls.dsp.log.capture.cyc_chk_time.unix_time = 0;
    dpcls.dsp.log.capture.cyc_chk_time.us_time   = 0;

    return (0);
}


INT16U GetSpivs(struct cmd * c, struct prop * p)
{
    // Displays SPIVS debug information

    static const char * fmt_opts[] = { " 0x%08X  ", "%12.5E "};

    char        const * fmt;
    prop_size_t         n;
    uint16_t            errnum;
    char                delim;

       
    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    // Set delimiter
   
    delim   = (c->token_delim == ' ' ? '\n' : ',');
    fmt     = (Test(c->getopts, GET_OPT_HEX) ? fmt_opts[0] : fmt_opts[1]);

    fprintf(c->f, "Sent: %-10d  Faults: %-6d  Busy: %1d%c",
            (int)dpcom.spivs.num_sent,
            (int)dpcom.spivs.num_faults,
            (int)dpcom.spivs.num_busy,
            delim);

    fprintf(c->f, "      Tx             Rx       Error%c", delim);

    uint8_t i;
    uint8_t j;

    for (i = 0; i < SPIVS_DBG_LENGTH; ++i)
    {
        for (j = 0; j < 2; ++j)
        {
            fprintf(c->f, fmt, (unsigned int)dpcom.spivs.dbg_tx[i][j]);
        }

        fprintf(c->f, "| ");

        for (j = 0; j < 2; ++j)
        {
            fprintf(c->f, fmt, (unsigned int)dpcom.spivs.dbg_rx[i][j]);
        }

        fprintf(c->f, "   %1d%c", (int)dpcom.spivs.dbg_st[i], delim);
    }

    // Zero all the counters and mark the index for the DSP to resume
    // the logging of SPIVS diagnostics

    if (Test(c->getopts, GET_OPT_ZERO) == true)
    {
        dpcom.spivs.num_sent   = 0;
        dpcom.spivs.num_faults = 0;
        dpcom.spivs.num_busy   = 0;

        dpcom.spivs.dbg_idx = 0xFFFFFFFF;

    }

    return (0);
}



uint16_t GetHistogram(struct cmd * c, struct prop * p)
{
    return 0;
}


// EOF

