/*!
 *  @file     log_class.h
 *  @defgroup FGC:MCU:61
 *  @brief    Logging related functions for class 61.
 *
 *  LogInitEvtProp() makes links between bits and enums for a property
 *  with a symlist in the structure log_evt_prop.  This means that the
 *  symbol constant values for any property logged must NEVER be outside
 *  the range 0-15.
 */

#define LOG_CLASS_GLOBALS

// Includes

#include <log_class.h>
#include <string.h>

// External function definitions

void LogPulseMeas(void)
{
    /*
     * The PULSE signals are copied into the FIP status variable buffer on
     * millisecond 1 of 20 by MstTsk() which calls this function on
     * millisecond 6 of 20 to record the values in the PULSE log buffer.
     */

    float meas_data[4];

    meas_data[0] = REF;
    meas_data[1] = dpcls.dsp.meas.i;
    meas_data[2] = dpcls.dsp.meas.v;
    meas_data[3] = dpcls.dsp.meas.v_capa;

    memcpy(LogNextAnaSample(&log_pulse), &meas_data, 4 * sizeof(FP32));

    if (log_pulse.run_f == 0)
    {
        log_pulse.samples_to_acq--;
    }

    // Time adjusted to start of 10 ms

    log_pulse.last_sample_time = mst.time10ms;
}

// EOF
