/*!
 *  @file      events_class.c
 *  @brief     File with the class-specific processing of events.
 */

#define EVENTS_CLASS_GLOBALS


// Includes

#include <stdint.h>

#include <events_class.h>
#include <event_fsm_class.h>
#include <events_sim.h>
#include <mcu_dsp_common.h>
#include <mst.h>
#include <time_fgc.h>
#include <log_cycle.h>
#include <cycle_time.h>
#include <defconst.h>
#include <dpcls.h>



// Constants 

#define EVENTS_QUEUE_SIZE   2



// Internal structures, unions and enumerations


/*!
 * Event information
 */
struct event
{
    struct abs_time_ms    start;                       //!< Event start time
    uint8_t               payload;                     //!< Event payload
};

/*!
 * Event queue
 */
struct event_queue
{
    struct event          data[EVENTS_QUEUE_SIZE];     //!< Array of events
    uint8_t               head;                        //!< Queue head
    uint8_t               tail;                        //!< Queue tail
    uint8_t               size;                        //!< Queue size
};

/*!
 * Internal global variables for the module events_class
 */
struct events_class
{
    struct event_queue    queue;                      //!< Event queue
};



// Internal variable definition

struct events_class events_class;



// Internal functions declaration

/*
 * Adds a new event into the queue
 */
static void eventsClassQueueEvent(struct fgc_event const * time_event);

/*
 * Retrieves the last queued event and handles it
 */
static void eventsClassDequeueEvent(void);



// External function definitions

void eventsClassProcess(void)
{
    // Check if the next event can be processed

    if (eventsClassIsEventActive() == false)
    {
        eventsClassDequeueEvent();
    }
}



bool eventsClassIsEventActive(void)
{
    return (CycTimeGetTimeTillEvent() >= (int32_t)0);
}



// Platform/class specific function declarations

void eventsClassProcessStart(struct fgc_event const * event)
{
    ; // Do nothing
}



void eventsClassProcessAbort(struct fgc_event const * event)
{
    ; // Do nothing
}



void eventsClassProcessInjection(struct fgc_event const * event)
{
    if (dpcls.mcu.ref.event_cyc == FGC_EVENT_CYC_INJECTION)
    {
        eventsClassQueueEvent(event);
    }
}



void eventsClassProcessExtraction(struct fgc_event const * event)
{
    if (dpcls.mcu.ref.event_cyc == FGC_EVENT_CYC_EJECTION)
    {
        eventsClassQueueEvent(event);
    }
}



void eventsClassProcessCycleStart(struct fgc_event const * event)
{
    ; // Do nothing
}



// Internal function definitions

static void eventsClassQueueEvent(struct fgc_event const * time_event)
{
    // Check the queue is not already full 

    if (events_class.queue.size >= EVENTS_QUEUE_SIZE)
    {
        return;
    }
       
    struct event event = { .start.unix_time = TimeGetUtcTime(),
                           .start.ms_time   = TimeGetUtcTimeMs(),
                           .payload         = time_event->payload,
                         };
 
    // Compensate for the timing event delay

    uint32_t delay = time_event->delay_ms;

    /*
     * The event is sent at millisecond 18/19, whilst the processing is done at millisecond 12 
     * of the next fieldbus cycle. The parameter time_till_event_ms refers to the beginning of 
     * the fieldbus cycle when the event is sent by the gateway. Therefore there is an offset 
     * of 20 + 12 = 32 ms.
     *     ^                       ^                     ^
     *  _ _|_______________|___|___|___________|_________|_____ _ _ _ _ ___*|*___ _ _
     *     0              18  19 20/0 ms.      12         20/0 ms.
     *                  Event sent        Event processed                 Event
     *     <--------------------------- time_till_event_ms ----------------->
     *     <------------- offset -------------->
     *
     * Simulated events are already compensated.
     */

    if (EventsSimIsEnabled() == false)
    {
        delay -= (mst.ms_mod_20 + 20);
    }

    AbsTimeMsAddDelay(&event.start, delay);    

    // Push the event into the queue. 

    events_class.queue.data[events_class.queue.head] = event;

    events_class.queue.size++;
    events_class.queue.head++;

    if (events_class.queue.head >= EVENTS_QUEUE_SIZE)
    {
        events_class.queue.head = 0;
    }   

    // Log the event

    if (LogCycleAllEnabled() == true)
    {
        char * const log_val  = LogCycleGetValBuf();

        snprintf(log_val, FGC_LOG_EVT_VAL_LEN,
                 "%-19s (%2u) - %4u ms",
                 fgc_event_names[time_event->type],
                 (unsigned int)time_event->payload,
                 (unsigned int)delay);

        LogCycleInsert("WARNING EVENT", LOG_VALUE_TYPE_STRING,
                       log_val, LOG_ACTION_RCV);
    }
}



static void eventsClassDequeueEvent(void)
{
    if (events_class.queue.size == 0 )
    {
        return;
    }

    struct event       * event =  &events_class.queue.data[events_class.queue.tail];    
    struct abs_time_ms   now   = {  .unix_time = TimeGetUtcTime(),
                                    .ms_time   = TimeGetUtcTimeMs()
                                 };
    uint32_t             delay = AbsTimeMsDiff(&now, &event->start);

    // Set up the TimeTillEvent register

    CycTimeSetTimeTillEvent(delay);

    // Remove the event

    events_class.queue.size--;
    events_class.queue.tail++;

    if (events_class.queue.tail >= EVENTS_QUEUE_SIZE)
    {
        events_class.queue.tail = 0;
    }

    // Pass the events the Event FSM to prepare the timing pulses

    eventsFsmClassProcessEvent(&event->start, event->payload);
}


// EOF
