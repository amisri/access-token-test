/*!
 *  @file      event_fsm_class.c
 *  @defgroup  FGC:MCU
 *  @brief     File providing the transition and state functions specific to class 61 for the
 *             event-driven state machine.
 */

/*
 *         MIDIDSCAP
 * DAC_0:  Not used
 * DAC_1:  Reference (A)
 *
 * ADC_A:  Load voltage (1000V)
 * ADC_B:  Load current (50A)
 * ADC_C:  Capacitor voltage (1000V)
 * ADC_D:  Not used
 *
 * DIG_A0: CMD_INVERTER_RUN
 * DIG_A1: CMD_POLARITY_RUN
 * DIG_A2: Not used
 * DIG_A3: Not used
 * DIG_A4: Not used
 *
 * DIG_B0: CMD_INVERTER_RUN
 * DIG_B1: CMD_POLARITY_RUN
 * DIG_B2: CMD_LINEAR_RUN
 * DIG_B3: CMD_EARTH_FAULT_DISABLE
 * DIG_B4: External Trigger (LOCAL-SYNC)
 */

#define EVENT_FSM_CLASS_GLOBALS

// Includes
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include <event_fsm_class.h>
#include <event_fsm.h>
#include <events_class.h>
#include <defconst.h>
#include <defprops.h>
#include <fgc_consts_gen.h>
#include <mcu_dsp_common.h>
#include <macros.h>
#include <dpcls.h>
#include <dev.h>
#include <log_cycle.h>
#include <fbs_class.h>
#include <pub.h>
#include <pulse_class.h>
#include <timing_pulse_class.h>
#include <events.h>
#include <crate.h>
#include <start.h>
#include <events.h>
#include <cycle_time.h>
#include <pc_state.h>
#include <state_manager.h>

 

 // Constants
 
#define INVALID_USER   0xFF



// Internal structures, unions and enumerations

typedef struct event_fsm_local
{
    char const   signals_logic_name[4][3];
    char const * signals_names[4];

    struct
    {
        struct abs_time_ms start;
        struct abs_time_ms next_start;
        uint8_t            enabled;
        uint8_t            user;
    } pulse_info;

    struct
    {
        int32_t timeout;
        uint8_t state;
    } watchdog;

    float  * meas;

    uint32_t warning_mask_reg;
    uint32_t warning_mask_pol;
    bool     ssc_f;
    bool     stretch_warning_reg_f;
    bool     stretch_warning_pol_f;

} event_fsm_local_t;


// Internal variable definition

static event_fsm_local_t local =
{
    .signals_logic_name     = { "B0", "B1", "B2", "B3" },
    .signals_names          = { "CHARGE-ENABLED", "START",  "ACQ", "BOUNCER" },
    .warning_mask_reg       = 0,
    .warning_mask_pol       = 0,
    .stretch_warning_reg_f  = true,
    .stretch_warning_pol_f  = true,
};


// Internal functions declaration

/*!
 * Adjusts the edge of a timing signal.
 */
static void EventFsmEdgeTimeAdjustment(struct abs_time_us * time,
                                       int32_t offset_us);

/*!
 * Logs the rising and falling edge of a timing signal.
 */
static void EventFsmLogTimingSignal(uint8_t indx, int32_t r_edge, int32_t f_edge);

/*!
 * Checks if the timeout for the state has been reached. Return true if so.
 */
static bool EventFsmWatchodCheck(void);

/*!
 * Logs the precise time when the timing pulse in the B channels are detected.
 */
static void EventFsmLogTiming(void);

/*!
 * Outputs a report in the event log of the current pulse.
 */
static void EventFsmAddEventLog(void);

/*!
 * Translates the pulse status into a string.
 */
static char const * EventFsmStatusStr(uint8_t status);

/*!
 * Checks if any conditions that would trigger a REG_ERROR warning or fault
 * have been triggered.
 *
 * The warning is set for an entire super-cycle to prevent the condition from
 * toggling on a cycle-basis.
 */
static void EventFsmCheckErrors(void);


// Internal function definitions

static void EventFsmEdgeTimeAdjustment(struct abs_time_us * time,
                                       int32_t offset_us)
{
    time->unix_time = local.pulse_info.start.unix_time;
    time->us_time   = local.pulse_info.start.ms_time * 1000;

    AbsTimeUsAddOffset(time, -offset_us);
}



static void EventFsmLogTimingSignal(uint8_t indx, int32_t r_edge, int32_t f_edge)
{
    if (LogCycleAllEnabled())
    {
        struct abs_time_us time;

        EventFsmEdgeTimeAdjustment(&time, r_edge);
        LogCycleInsertUs(local.signals_logic_name[indx],
                         &time,
                         LOG_VALUE_TYPE_STRING,
                         (void *)local.signals_names[indx],
                         LOG_ACTION_RISE);

        EventFsmEdgeTimeAdjustment(&time, f_edge);
        LogCycleInsertUs(local.signals_logic_name[indx],
                         &time,
                         LOG_VALUE_TYPE_STRING,
                         (void *)local.signals_names[indx],
                         LOG_ACTION_FALL);
    }
}



static bool EventFsmWatchodCheck(void)
{
    bool retval = false;

    if (event_fsm.time_ms > local.watchdog.timeout)
    {
        local.watchdog.state = event_fsm.state;
        local.watchdog.timeout = -1;
        retval = true;
    }

    return (retval);
}



static void EventFsmLogTiming(void)
{
    timing_pulse_status_t status;
    int32_t r_edge;
    int32_t f_edge;
    uint8_t i;

    for (i = 0; i < PULSE_NUM; ++i)
    {
        status = TimingPulseReadPulse((timing_pulse_channel_t)i, &r_edge, &f_edge);

        if (status == PULSE_TIME_OK)
        {
            EventFsmLogTimingSignal(i, r_edge, f_edge);
        }
    }
}



static void EventFsmAddEventLog(void)
{
    char * const   log_val  = LogCycleGetValBuf();
    char * const   log_prop = LogCycleGetPropBuf();
    uint8_t        user     = dpcls.mcu.ref.pulse.user;

    snprintf(log_prop, FGC_LOG_EVT_PROP_LEN,
             "ACQUISITION (%2u)   %3s",
             user,
             (local.ssc_f ? "SSC" : ""));

    snprintf(log_val, FGC_LOG_EVT_VAL_LEN,
             "%-10s %11.3f %11.3f",
             (local.pulse_info.enabled ? EventFsmStatusStr(pulse_log.status[user]) : SYM_TAB_CONST[STC_DISABLED].key.c),             
             (double)dpcls.mcu.ref.pulse.ref,
             (double)*local.meas);

    LogCycleInsertMs(log_prop, &local.pulse_info.start, LOG_VALUE_TYPE_STRING,
                     log_val, LOG_ACTION_NOW);
}



static char const * EventFsmStatusStr(uint8_t status)
{
    char const * name;

    switch (status)
    {
        case FGC_PULSE_STATUS_OK:         name = SYM_TAB_CONST[STC_OK].key.c;         break;

        case FGC_PULSE_STATUS_WARNING:    name = SYM_TAB_CONST[STC_WARNING].key.c;    break;

        case FGC_PULSE_STATUS_FAULT:      name = SYM_TAB_CONST[STC_FAULT].key.c;      break;

        case FGC_PULSE_STATUS_NOT_READY:  name = SYM_TAB_CONST[STC_NOT_READY].key.c;  break;

        case FGC_PULSE_STATUS_TIMEOUT:    name = SYM_TAB_CONST[STC_TIMEOUT].key.c;    break;

        case FGC_PULSE_STATUS_POL_SWITCH: name = SYM_TAB_CONST[STC_POL_SWITCH].key.c; break;

        default:                          name = SYM_TAB_CONST[STC_INVALID].key.c;    break;
    }

    return name;
}



static void EventFsmCheckErrors(void)
{
    uint32_t user_mask = 1 << dpcls.mcu.ref.pulse.user;
    uint8_t  user      = dpcls.mcu.ref.pulse.user;

    Clr(local.warning_mask_reg, user_mask);
    Clr(local.warning_mask_pol, user_mask);

    if (pulse_log.status[user] == FGC_PULSE_STATUS_FAULT)
    {
        Set(FAULTS, FGC_FLT_REG_ERROR);
    }
    else if (pulse_log.status[user] == FGC_PULSE_STATUS_POL_SWITCH)
    {
        Set(local.warning_mask_pol, user_mask);

        local.stretch_warning_pol_f = true;

        Set(WARNINGS, FGC_WRN_POL_SWITCH);
    }
    else if (pulse_log.status[user] != FGC_PULSE_STATUS_OK)
    {
        // STATUS_NOT_READY is not signalled until this condition is repeated a
        // number of consecutive pulses, specified by the property VS.VSRDY_TIMEOUT

        if (pulse_log.status[user] != FGC_PULSE_STATUS_NOT_READY ||
            vs.vs_not_rdy_count    >= vs.vs_ready_timeout)
        {
            Set(local.warning_mask_reg, user_mask);

            local.stretch_warning_reg_f = true;

            Set(WARNINGS, FGC_WRN_REG_ERROR);
        }
    }

    // Latch the warning for an entire super-cycle.

    if (local.ssc_f)
    {
        if (local.stretch_warning_reg_f)
        {
            local.stretch_warning_reg_f = false;
        }
        else
        {
            if (local.warning_mask_reg == 0)
            {
                Clr(WARNINGS, FGC_WRN_REG_ERROR);
            }
        }

        if (local.stretch_warning_pol_f)
        {
            local.stretch_warning_pol_f = false;
        }
        else
        {
            if (local.warning_mask_pol == 0)
            {
                Clr(WARNINGS, FGC_WRN_POL_SWITCH);
            }
        }

        // Zero warning_mask in case a user is removed in the next SSC.

        local.warning_mask_reg = 0;
        local.warning_mask_pol = 0;
    }
}



// External function definitions

void EventFsmInit(void)
{
    ; // Do nothing for the moment

    switch (crateGetType())
    {
        case FGC_CRATE_TYPE_MIDIDISCAP:
            local.meas = (float *)&pulse_report.meas_i;
            break;

        case FGC_CRATE_TYPE_MEGADISCAP:
            local.meas = (float *)&pulse_report.meas_i;
            break;

        case FGC_CRATE_TYPE_MAXIDISCAP:
            local.meas = (float *)&pulse_report.meas_i;
            break;

        case FGC_CRATE_TYPE_DSP_IGBT:
            local.meas = (float *)&pulse_report.meas_i;
            break;

        case FGC_CRATE_TYPE_HMINUS_DISCAP:
            local.meas = (float *)&pulse_report.meas_v;
            break;

        case FGC_CRATE_TYPE_KLYSTRON_MOD:
            local.meas = (float *)&pulse_report.meas_v;

            // The Modulator requires the pulse timing in A1 and A3 to be inverted.

            TimingPulseSetInvert(PULSE_START);
            TimingPulseSetInvert(PULSE_BOUNCER);

            break;

        case FGC_CRATE_TYPE_DEVELOPMENT: // Fall through
        case FGC_CRATE_TYPE_RECEPTION:
            // For development and reception test use the current as the measurement
            local.meas = (float *)&pulse_report.meas_i;
            break;

        default:
            Crash(0xBADF);
            break;
    }

    dpcls.mcu.ref.pulse.ref_avail_f   = false;
    dpcls.dsp.meas.pulse.meas_avail_f = false;

    local.pulse_info.user = INVALID_USER;
}



void eventsFsmClassProcessEvent(struct abs_time_ms const * start, uint8_t user)
{
    local.pulse_info.next_start = *start;
    local.pulse_info.user  = user;
}



bool WAtoPR(void)
{
    /* Condition: An event has been identified. */

    return (eventsClassIsEventActive());
}



bool PRtoSE(void)
{
    /* Condition: the reference has been sent by the DSP. */

    return (true);
}



bool SEtoRE(void)
{
    /* Condition: the pulse has been generated or it is disabled. */

    return (dpcls.dsp.meas.pulse.meas_avail_f);
}



bool REtoWA(void)
{
    /* Condition: once the event service reports the event is completed. */

    return (true);
}



bool XXtoFA(void)
{
    /* Condition: Watch-dog. */

    return (EventFsmWatchodCheck());
}



bool FAtoWA(void)
{
    /* Condition: TBD. */

    return (true);
}



void EventStateWA(bool first_f)
{
    EventFsmLogTiming();

    dpcls.dsp.meas.pulse.meas_avail_f = false;
}




void EventStatePR(bool first_f)
{
    if (first_f)
    {
        float   ref  = 0.0;

        local.pulse_info.start = local.pulse_info.next_start;

        local.ssc_f = dpcom.mcu.evt.ssc_f;

        // Determine the source of the reference and latch the value

        if (PcStateAboveEqual(FGC_PC_TO_CYCLING))
        {
            // If REF.PULSE.REF_LOCAL is non-zero overwrite REF.PULSE.REF.VALUE

            if (fabs(dpcls.mcu.ref.pulse.ref_local) > 0.0)
            {
                ref  = dpcls.mcu.ref.pulse.ref_local;

                Set(WARNINGS, FGC_WRN_REF_LOCAL);
            }
            else
            {
                // If the device is not ppm get the reference from slot 0

                uint8_t ref_user = (dpcom.mcu.device_ppm == FGC_CTRL_ENABLED ? local.pulse_info.user : 0);

                if (dpcls.mcu.ref.func.play[ref_user] == FGC_CTRL_ENABLED)
                {
                    ref = dpcls.mcu.ref.pulse.ref_armed[ref_user];
                }

                Clr(WARNINGS, FGC_WRN_REF_LOCAL);
            }
        }
        else if (PcStateBelow(FGC_PC_BLOCKING))
        {
            dpcls.mcu.ref.pulse.ref_local = 0.0;

            Clr(WARNINGS, FGC_WRN_REF_LOCAL);
        }
        else
        {
            // Between BLOCKING and TO_CYCLING the reference must be zero
            // even if REF.PULSE.REF_LOCAL != 0

            ref = 0.0;
        }

        // Set the user to zero if DEVICE.PPM is DISABLED

        dpcls.mcu.ref.pulse.user = local.pulse_info.user;
        dpcls.mcu.ref.pulse.ref  = ref;

        // The pulse is enabled when the reference is non-zero (in cycling).

        local.pulse_info.enabled = (fabs(ref) > 0.0f);
        local.pulse_info.user    = INVALID_USER;
    }
}



void EventStateSE(bool first_f)
{
    if (first_f)
    {
        struct abs_time_ms now = { TimeGetUtcTime(), TimeGetUtcTimeMs() } ;

        // Always send the acquisition timing pulse even if it is disabled.
        // This allows to always get the current and voltage measurement.

        TimingPulseSendPulse(PULSE_ACQ);

        if (local.pulse_info.enabled)
        {
            // Set the timing pulses

            TimingPulseSendPulse(PULSE_CHARGE);
            TimingPulseSendPulse(PULSE_START);
            TimingPulseSendPulse(PULSE_BOUNCER);

            // Function type reported is PULSE

            dpcls.dsp.ref.stc_func_type = STC_PULSE;

            // Instruct the DSP to send the reference

            dpcls.mcu.ref.pulse.ref_avail_f = true;

            // Change the polarity switch if need be

            if (dpcls.mcu.vs.polarity.timeout != 0)
            {
                if (dpcls.mcu.ref.pulse.ref > 0.0 &&
                    dpcls.mcu.vs.polarity.state == FGC_POL_NEGATIVE)
                {
                    dpcls.mcu.vs.polarity.mode = FGC_POL_MODE_POSITIVE;
                    Set(sta.cmd, DDOP_CMD_POL_POS);
                }
                else if (dpcls.mcu.ref.pulse.ref < 0.0 &&
                         dpcls.mcu.vs.polarity.state == FGC_POL_POSITIVE)
                {
                    dpcls.mcu.vs.polarity.mode = FGC_POL_MODE_NEGATIVE;
                    Set(sta.cmd, DDOP_CMD_POL_NEG);
                }
                else
                {
                    ; // Nothing
                }
            }
        }

        // Set the watch-dog: The DSP should return the measurement at the event time.
        // Add 10 ms. extra for the DSP to process the measurement.

        if (AbsTimeLessThanMs(&local.pulse_info.start, &now))
        {
            // Force the state machine to transition to Fault

            local.watchdog.timeout = 0;
        }
        else
        {
            local.watchdog.timeout  = AbsTimeMsDiff(&now, &local.pulse_info.start);
            local.watchdog.timeout += 10;
        }
    }

    EventFsmLogTiming();
}



void EventStateRE(bool first_f)
{
    if (first_f)
    {    
        uint8_t user = dpcls.mcu.ref.pulse.user;

        pulse_report.ref       = dpcls.mcu.ref.pulse.ref;
        pulse_report.meas_i    = dpcls.dsp.meas.pulse.meas_i;
        pulse_report.meas_v    = dpcls.dsp.meas.pulse.meas_v;
        pulse_report.error     = pulse_report.ref - *local.meas;
        pulse_report.error_mav = (int32_t)(1000.0 * (pulse_report.error));
        pulse_report.error_mav = MAX(INT16_MIN, pulse_report.error_mav);
        pulse_report.error_mav = MIN(pulse_report.error_mav, INT16_MAX);

        // Update the pulse status

        pulse_log.max_abs_err[user] = fabs(pulse_report.error);

        /*
         * Update the  pulse status based on:
         *
         * OK:         the pulse was correctly generated or the state is below TO_CYCLING
         * WARNING:    abs(ref - meas) > Warning threshold
         * FAULT:      abs(ref - meas) > Fault threshold
         * NOT_READY:  the VS_READY input is not active during a consecutive number of cycles
         * POL_SWITCH: the polarity switch is not in the expected position
         *
         * VS_READY signals the status of the power converter. It might happen
         * that the voltage source is not ready to generate the pulse with the
         * specified reference because the capacitors are not yet charged to a
         * nominal voltage.
         */

        // Increment the counts of pulses where VS_READY was not asserted

        if (vs.vs_ready_timeout != 0 && !Test(sta.inputs, DIG_IP1_VSREADY_MASK16))
        {
            vs.vs_not_rdy_count++;
        }
        else
        {
            vs.vs_not_rdy_count = 0;
        }

        if (PcStateBelow(FGC_PC_TO_CYCLING))
        {
            pulse_log.status[user] = FGC_PULSE_STATUS_OK;
        }
        else if (pulse_limits.error_fault   != 0.0 &&
                 pulse_log.max_abs_err[user] > pulse_limits.error_fault)
        {
            pulse_log.status[user] = FGC_PULSE_STATUS_FAULT;
        }
        else if (dpcls.mcu.vs.polarity.timeout != 0 &&
                 dpcls.mcu.vs.polarity.mode    != dpcls.mcu.vs.polarity.state)
        {
            pulse_log.status[user] = FGC_PULSE_STATUS_POL_SWITCH;
        }
        else if (vs.vs_not_rdy_count > 0)
        {
            pulse_log.status[user] = FGC_PULSE_STATUS_NOT_READY;
        }
        else if (pulse_limits.error_warning != 0.0 &&
                 pulse_log.max_abs_err[user] > pulse_limits.error_warning)
        {
            pulse_log.status[user] = FGC_PULSE_STATUS_WARNING;
        }
        else
        {
            pulse_log.status[user] = FGC_PULSE_STATUS_OK;
        }

        dpcls.dsp.ref.stc_func_type = STC_NULL;

        EventFsmCheckErrors();

        EventFsmAddEventLog();

        PubProperty(&PROP_LOG_PULSE_STATUS,      user, false);
        PubProperty(&PROP_LOG_PULSE_MAX_ABS_ERR, user, false);
    }

    EventFsmLogTiming();
}



void EventStateFA(bool first_f)
{
    uint8_t user = dpcls.mcu.ref.pulse.user;

    // Fault triggered by a watch-dog timeout

    if (local.watchdog.timeout == -1)
    {
        // If the polarity switch timed out resulting in an inconsistent
        // sate, cancel the request.

        if (dpcls.mcu.vs.polarity.mode  == FGC_POL_MODE_POSITIVE  &&
            dpcls.mcu.vs.polarity.state == FGC_POL_NEGATIVE)
        {
            dpcls.mcu.vs.polarity.mode = FGC_POL_NEGATIVE;
            vs.req_polarity = 0;
        }
        else if (dpcls.mcu.vs.polarity.mode  == FGC_POL_MODE_NEGATIVE  &&
                 dpcls.mcu.vs.polarity.state == FGC_POL_POSITIVE)
        {
            dpcls.mcu.vs.polarity.mode = FGC_POL_POSITIVE;
            vs.req_polarity = 0;
        }
        else
        {
            ; // Do nothing
        }

        pulse_log.status[user] = (  PcStateBelow(FGC_PC_TO_CYCLING)
                                  ? FGC_PULSE_STATUS_OK             
                                  : FGC_PULSE_STATUS_TIMEOUT);

        EventFsmCheckErrors();

        // Assume the acquisition was not taken. Default to zero.

        pulse_report.ref       = dpcls.mcu.ref.pulse.ref;
        pulse_report.meas_i    = 0.0;
        pulse_report.meas_v    = 0.0;
        pulse_report.error     = 0.0;
        pulse_report.error_mav = 0.0;

        EventFsmAddEventLog();

        LogCycleInsert("Event state timeout",
                       LOG_VALUE_TYPE_STRING,
                       (void *)EventFsmStateStr(local.watchdog.state),
                       LOG_ACTION_SET);
    }

    pulse_log.max_abs_err[user] = 0.0;

    PubProperty(&PROP_LOG_PULSE_STATUS,      user, false);
    PubProperty(&PROP_LOG_PULSE_MAX_ABS_ERR, user, false);
}


// EOF
