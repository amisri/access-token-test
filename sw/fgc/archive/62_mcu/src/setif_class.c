/*!
 *  @file      setif_class.c
 *  @defgroup  FGC:MCU:62
 *  @brief     Class specific Setif command functions.
 */

#include <setif_class.h>
#include <defprops.h>
#include <cmd.h>
#include <fbs_class.h>
#include <mst.h>
#include <fgc_errs.h>

// External function definitions

INT16U SetifCalOk(struct cmd * c)
{
    // Set If Condition: Is Auto-Calibration of ADCs allowed?

    if ((STATE_OP != FGC_OP_CALIBRATING)
        && ((STATE_OP == FGC_OP_UNCONFIGURED)
            || ((STATE_OP == FGC_OP_NORMAL)
                && ((STATE_PC == FGC_PC_IDLE)
                    || (!SetifPcOff(c))
                   )
               )
           )
       )
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}

// EOF
