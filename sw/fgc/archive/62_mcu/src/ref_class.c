/*!
 *  @file     ref_class.c
 *  @defgroup FGC3:MCU:62
 *  @brief    Reference-related functions specific to Class 61
 */

#define REF_CLASS_GLOBALS

// Includes

#include <ref_class.h>
#include <ref.h>
#include <defprops.h>
#include <dpcls.h>
#include <fgc_errs.h>

// External function definitions

INT16U RefPulse(struct cmd * c)
{
    return (FGC_OK_NO_RSP);
}

// EOF
