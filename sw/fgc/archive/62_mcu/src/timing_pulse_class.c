/*!
 *  @file      timing_pulse_class.c
 *  @defgroup  FGC:MCU:62
 *  @brief     Provides the functionality to generate and read-back a pulse
 *             using the supplemental digital I/O.
 */

#define TIMING_PULSE_CLASS_GLOBALS

// Includes

#include <timing_pulse_class.h>
#include <cycle_time.h>
#include <dig.h>


// Internal structures, unions and enumerations

/*!
 * This structure includes the rising and falling edge of a timing pulse.
 */
typedef struct pulse_edges
{
    int32_t  rise;       //!< Delay wrt the event of the rising edge of the pulse.
    int32_t  fall;       //!< Delay wrt the event of the falling edge of the pulse.
} pulse_edges_t;


// Internal function declarations

static void PulseSend(timing_pulse_channel_t channel, pulse_edges_t const * const pulse);


// Internal function definitions

static void PulseSend(timing_pulse_channel_t channel, pulse_edges_t const * const pulse)
{
    /*
     * Only channels A0, A1, A2, A3 can be used. The pulse is triggered
     * 'time_till_event' usecs. before the event is due and has a duration of
     * 'fall - rise' usecs.
     *
     * Note that if the ARM bit corresponding to the channel is already set, this
     * action will be silently ignored by the FPGA.
     *
     * Negative delays correspond to times prior to the event. The FPGA is
     * configured however to have positive values corresponding to times
     * prior to the event. Hence the sign is flipped.
     */

    // Always clear the ARM bit so that the PULSE can be re-configured.

    DigSupClrArm(channel);
    DigSupSetPulseEtim(channel, -pulse->rise);
    DigSupSetPulseWidth(channel, pulse->fall - pulse->rise);
    DigSupSetArm(channel);
}



// External function definitions

void TimingPulseSendPulse(timing_pulse_channel_t channel)
{
    pulse_edges_t pulse;

    switch (channel)
    {
        case DIG_SUP_A_CHANNEL0:
            pulse.rise = timing_pulse.vs_pulses[0];
            pulse.fall = timing_pulse.vs_pulses[1];
            break;

        case DIG_SUP_A_CHANNEL1:
            pulse.rise = timing_pulse.vs_pulses[2];
            pulse.fall = timing_pulse.vs_pulses[3];
            break;

        case DIG_SUP_A_CHANNEL2:
            /*
             * Channel A2 is always associated to the acquisition time. The
             * acquisition time is always at the moment of the event and the
             * duration is hard-coded to some sensible value: 500 us.
             */

            pulse.rise = 0;
            pulse.fall = 500;
            break;

        case DIG_SUP_A_CHANNEL3:
            pulse.rise = timing_pulse.vs_pulses[4];
            pulse.fall = timing_pulse.vs_pulses[5];
            break;

        default:
            pulse.rise = 0;
            pulse.fall = 0;
            break;
    }

    PulseSend(channel, &pulse);
}



void TimingPulseSendFastAcquisition()
{
    /*
     * Sends a soft pulse that sets a flag in the FPGA that can be polled
     * by the DSP to run the fast acquisition.
     */

    if (CycTimeTstPulseArm())
    {
        CycTimeClrPulseArm();
    }

    CycTimeSetPulseEtim(-timing_pulse.fast_acq[0]);
    CycTimeSetPulseWidth(timing_pulse.fast_acq[1] - timing_pulse.fast_acq[0]);
    CycTimeSetPulseArm();
}



timing_pulse_status_t TimingPulseReadPulse(timing_pulse_channel_t channel,
                                           int32_t * const raising_us,
                                           int32_t * const falling_us)
{
    /*
     * Only channels B0, B1, B2, B3 can be used. The time in microseconds is in
     * relation to the 'time_till_event' register. If the raising or falling edge
     * have not been detected, the function returns PULSE_TIME_NOT_READY. Otherwise
     * it returns PULSE_TIME_OK and the raising_us and falling_us are appropriately
     * set. For B0 and B1, this function makes sure that the pulses generated in
     * A0 and A1 match with those detected in B0 and B1, respectively.
     *
     */

    timing_pulse_status_t retval = PULSE_TIME_NOT_READY;

    if (DigSupTstEdges(channel))
    {
        *raising_us = DigSupGetRiseEtim(channel);
        *falling_us = DigSupGetFallEtim(channel);

        retval = PULSE_TIME_OK;

        /*
         * Matching A0-B0 and A1-B1 is currently not used. TBD if it might be needed in
         * the feature.
         *
         * if (channel == PULSE_CHARGE ||
         *     channel == PULSE_START)
         * {
         *     // The values in timing_pulse.vs_pulses have the correct delay sign,
         *     // whilst those returned by DigSupGetRiseEtim() have the opposite sign.
         *
         *     int32_t a_rising_us  = -timing_pulse.vs_pulses[channel * 2];
         *     int32_t a_falling_us = -timing_pulse.vs_pulses[channel * 2 + 1];
         *
         *     if (*raising_us != a_rising_us ||
         *         *falling_us != a_falling_us)
         *     {
         *         retval = PULSE_TIME_WRONG;
         *     }
         * }
         */
    }

    return (retval);
}



timing_pulse_status_t TimingPulseStatusFastAcquisition(void)
{
    return (CycTimeTstStatus() ? PULSE_TIME_OK : PULSE_TIME_NOT_READY);
}



void TimingPulseSetInvert(timing_pulse_channel_t channel)
{
    DigSupInvertChannelA(channel);
    DigSupInvertChannelB(channel);
}


// EOF
