/*!
 *  @file      pulse_class.c
 *  @defgroup  FGC:MCU:62
 *  @brief     Provides the functionality to generate and read-back a pulse
 *             using the supplemental digital I/O.
 */

#define PULSE_GLOBALS

// Includes

#include <pulse_class.h>
#include <dpcls.h>
#include <fgc_errs.h>
#include <defprops.h>
#include <nvs.h>
#include <dev.h>
#include <ref.h>
#include <crate.h>
#include <pub.h>
 


// External function definitions

void PulseInit(void)
{
    switch (crateGetType())
    {
        case FGC_CRATE_TYPE_HMINUS_DISCAP:  // Fall through
        case FGC_CRATE_TYPE_KLYSTRON_MOD:   pulse_units = 'V';   break;
        default:                            pulse_units = 'A';   break;
    }
}



uint16_t PulseSetRef(uint16_t user, float value)
{
    if (user == NON_PPM_USER && dpcom.mcu.device_ppm == FGC_CTRL_ENABLED)
    {
        return (FGC_BAD_CYCLE_SELECTOR);
    }

    if (value >= 0.0)
    {
        if (   value > pulse_limits.pos
            || value < pulse_limits.min)
        {
            return (FGC_OUT_OF_LIMITS);
        }
    }
    else
    {
        if (   value < pulse_limits.neg
            || value > -pulse_limits.min)
        {
            return (FGC_OUT_OF_LIMITS);
        }
    }

    dpcls.mcu.ref.pulse.ref_armed[user] = value;

    nvsStoreBlockForOneProperty(NULL, &PROP_REF_PULSE_REF_VALUE, (uint8_t *)&dpcls.mcu.ref.pulse.ref_armed[user],
                                0, user, 1, TRUE);

    PubProperty(&PROP_REF_PULSE_REF_VALUE, user, true);

    return (FGC_OK_NO_RSP);
}



uint16_t PulseSetRefLocal(float value)
{
    if (value > 0.0)
    {
        if (   value > pulse_limits.pos
            || value < pulse_limits.min)
        {
            return (FGC_OUT_OF_LIMITS);
        }
    }
    else if (value < 0.0)
    {
        if (   value < pulse_limits.neg
            || value > -pulse_limits.min)
        {
            return (FGC_OUT_OF_LIMITS);
        }
    }
    else
    {
        // 0.0 is used to reset REF.PULSE.REF_LOCAL
    }

    dpcls.mcu.ref.pulse.ref_local = value;

    return (FGC_OK_NO_RSP);
}

// EOF
