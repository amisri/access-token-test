/*---------------------------------------------------------------------------------------------------------*\
  File:     fip.c

  Purpose:  FGC MCU Software - uFIP Interface Functions.

  Author:   Quentin.King@cern.ch

  Notes:    This file contains the functions to support the uFIP interface.

        The uFIP MSGSA register has a bit (EMP_MST) to indicate if the msg TX buffer is
        empty or occupied.  When message transmission is disabled, this claims the buffer
        is occupied, so a flag is needed to override EMP_MST when messaging has been enabled
        for the first time (when the PLL locks).  This flag is fip.start_tx.
\*---------------------------------------------------------------------------------------------------------*/

#define FIP_GLOBALS         // define fip global variable

#include <fip.h>

#include <iodefines.h>      // specific processor registers and constants
#include <string.h>

#include <class.h>          // Include class dependent definitions
#include <cmd.h>            // for fcm global variable
#include <definfo.h>        // for FGC_CLASS_ID
#include <dpcls.h>
#include <dpcom.h>          // for dpcom global variable
#include <fbs.h>            // for struct fbs_stream, fbs global variable
#include <fgc_errs.h>       // for FGC_EXECUTING#include <dpcls.h>
#include <fgc_fip.h>        // for FGC_FIELDBUS_MAX_CMD_LEN. Order dependent!! Before fbs.h
#include <led.h>
#include <macros.h>         // for Test(), Set()
#include <memmap_mcu.h>     // for CPU_RESET_
#include <pll.h>            // for pll global variable
#include <shared_memory.h>  // for FGC_MP_MAIN_RUNNING
#include <sleep.h>
#include <sta.h>            // for sta global variable
#include <tsk.h>
#if defined(__HC16__) || defined(__RX610__)
#include <structs_bits_big.h>
#endif


// Local structures

struct fip_cmd_msg                                      // FIP command message
{
    struct fgc_fip_addr         address;
    struct fgc_fieldbus_cmd     msg;
};

struct fip_rsp_msg                                      // FIP response message
{
    struct fgc_fip_addr         address;
    struct fgc_fieldbus_rsp     msg;
};

#define FIP_STAT_V              ((struct fgc_fieldbus_stat *)FIP_DATMPS_32)
#define FIP_CMD_MSG             ((struct fip_cmd_msg *)FIP_RXDMSG_32)
#define FIP_RSP_MSG             ((struct fip_rsp_msg *)FIP_TXDMSG_32)
#define FIP_MSG_HDR             ((struct fgc_fieldbus_cmd_header *)FIP_MSGHEAD_32)

/*---------------------------------------------------------------------------------------------------------*/
void FipInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called MstTsk() if the FIP connector is present:

      o Prepare some of the fip global variables
      o Initialise and start the uFIP
\*---------------------------------------------------------------------------------------------------------*/
{
    fip.rt_ref_offset       = 4 + 4 * ((fbs.id - 1) % 15); // RD Data offset for this DC
    fip.rt_ref_vidgl        = 2 + (fbs.id > 15);        // Prepare for RT data variable
    fip.runlog_idx_offset        = (INT32U)&fbs.time_v.runlog_idx - (INT32U)&fbs.time_v;

    FbsInit();

    // Store REGIS bank register values

    FIP_CONFA_P = 0x05;     // No Msg ACK, 0x4: Msg snd disabled, 0x1: Msg rcv disabled
    FIP_CONFB_P = 0x80;     // 0x80: Var 7 rcv enabled, others disabled
    FIP_CONFC_P = 0x0A;     // RP time 4us, Field-drive, 0x8: WorldFIP frame, 0x2: 2.5MB/s
    FIP_CONFD_P = 0x0F;     // 0xE: Silence time 100us, 0x1: Var 7 uses global ID
    FIP_CONFE_P = 0x84;     // 0x80: Int on Var 7 rcv, 0x4: Int on msg rcv
    FIP_MPSPR_P = 0x00;     // Var 7 promptness 50ms, Var 6 refreshment 250ms
    FIP_VIDGL_P = 0x00;     // Var 7 global ID (low byte)
    FIP_VIDGH_P = 0x30;     // Var 7 global ID (high byte)

    // Store REGI2 bank register values

    FIP_MSSEG_P = 0x00;     // Msg segment number
    FIP_ARCNL_P = 0x01;     // UART config (low byte):  40MHz clock, 2.5MHz bus
    FIP_ARCNH_P = 0x00;     // UART config (high byte): 40MHz clock, 2.5MHz bus

    // Set up COMBPS area to define variable lengths and buffers

    FIP_COBMPS_A[6] = 0x71; // Var 6 (stat): Length = 7 blocks (56 bytes), first block = 1
    FIP_COBMPS_A[7] = 0x88; // Var 7 (time): Length = 8 blocks (64 bytes), first block = 8

    // Set up Response message address block

    FIP_RSP_MSG->address.dest_addr_h = 0x00;    // GW address is 0x0000
    FIP_RSP_MSG->address.dest_addr_l = 0x00;
    FIP_RSP_MSG->address.dest_seg    = 0x00;    // Segment 0
    FIP_RSP_MSG->address.src_addr_h  = 0x00;    // DC address (0x0001-0x001E)
    FIP_RSP_MSG->address.src_addr_l  = fbs.id;
    FIP_RSP_MSG->address.src_seg     = 0x00;    // Segment 0

    // Start interface

    FIP_CONFD_P |= 0x80;                // Start uFIP interface
}
/*---------------------------------------------------------------------------------------------------------*/
void FipWatch(void)
/*---------------------------------------------------------------------------------------------------------*\
  Watchdog for FIP interface.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (!fbs.pll_locked_f)
    {
        fip.watchdog++;

        if (fip.watchdog >= FBS_WATCHDOG_PERIOD)
        {
            fip.watchdog = 0;

            if (FipIdCheck())           // If FIP connector plugged in
            {
                // this will enable Network interrupts
                FipInit();              // Start FIP interface
            }
        }
    }
    else
    {
        fip.watchdog = 0;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN FipIdCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main() and MstTsk() to test if uFIP interface is connected (it sets
  fbs.id > 0 if it is).  The uFIP will initially be reset in InitMCU() and the address read.
  The address must be read the same twice before it is accepted.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  last_stadr;

    // Releases FIP/ETHERNET hw reset, reset line must have an internal pull-up
    P9.DR.BIT.B7 = 1; // FIP is free

    FIP_RESET_P = 0;                            // Reset uFIP (takes 1us)

    sleepUs(2);

    last_stadr = fip.stadr;                     // Take copy of last reading of FIP address

    fip.stadr = FIP_STADR_P;                    // Get FIP address

    if (fip.stadr != last_stadr ||              // If FIP address has changed or
        fip.stadr < 1 || fip.stadr > 30)         // not in valid range (1-30)
    {

        // FIP reset is direct from RX610, set FIP in reset
        P9.DR.BIT.B7 = 0;       // FIP in reset

        FbsSetStandalone();
        return (FALSE);
    }

    if (sta.config_state == FGC_CFG_STATE_STANDALONE)           // If FGC ran without FIP cable
    {
        FbsLeaveStandalone();
    }

    LED_SET(NET_RED);                   // Set RED FIP LED
    fbs.id = fip.stadr;                 // Store FIP ID
    return (TRUE);                      // Report that FIP is connected
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN FipSendStatVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will send the status variable data to the uFIP.  It returns 1 on success and 0 on error.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (Test(FIP_MPSSA_P, 0x08))                        // If Var 6 is NOT accessable...
    {
        return (FALSE);                                 // Report failure
    }

    FIP_CONFD_P = (6 << 4) | (FIP_CONFD_P & 0x8f);      // Identify Var 6 access to uFIP

    // Write digital data to Var 6 in uFIP

    memcpy((void *) FIP_STATVAR_32, &fbs.u.fieldbus_stat, FIP_STATVAR_B);

    FIP_COBMPS_A [6] = 0x71;                            // Re-assert Var 6 size/position
    Set(FIP_CONFB_P, 0x08);
    // Re-enable Var 6 transmission

    return (TRUE);
}
/*---------------------------------------------------------------------------------------------------------*/
void FipSendMsg(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by FbsWriteMsg() to send the pending message to the uFIP
\*---------------------------------------------------------------------------------------------------------*/
{
    FIP_CONFA_P = 0x04;                                         // Disable transmit channel

    FIP_RSP_MSG->msg.header.flags = fbs.pend_header;            // Set header with pkt description
    FIP_RSP_MSG->msg.header.n_term_chs = fbs.n_term_chs;        // Set number of remote term chars

    // Copy msg packet to uFIP

    memcpy(&FIP_RSP_MSG->msg.rsp_pkt, fbs.rsp_pkt, fbs.rsp_msg_len);

    FIP_MSCOT_P = fbs.rsp_msg_len + 8;                          // Write msg length into microfip
    FIP_CONFA_P = 0x08;                                         // Enable transmit channel
}
/*---------------------------------------------------------------------------------------------------------*/
void FipCheckMsgTx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will check if a message has not been transmitted within the max timeout period.  It must
  be executed periodically, but NOT during the message window.
\*---------------------------------------------------------------------------------------------------------*/
{
    /*--- Check for message transmission timeout ---*/

    // If pkt being sent, and uFIP TX buf is still full
    if (fbs.send_pkt && !Test(FipMsgsa(), 0x02))
    {

        if (fip.send_msg_timer > FIP_SEND_MSG_TIMEOUT)          // If TX timeout has expired
        {
            FIP_CONFA_P = 0x04;                 // Disable transmit channel
            fip.start_tx = TRUE;                // Flag that EMP_MST will say FULL

            if (fcm.stat == FGC_EXECUTING &&                     // If command is executing and
                fbs.send_pkt == FBS_CMD_PKT)                 // cmd rsp pkt is waiting to be sent
            {
                FbsCancelCmd(FGC_RSP_TX_FAILED);                // Cancel command response
            }

            fbs.send_pkt = 0;                                   // Clear send packet flags
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT8U FipMsgsa(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reads and returns the microfip message status register.  It checks the register value for:

     - Message receiver errors: Tx frame, Manchester error, FCS error, bit num error

     - Messages ignored: Message arrived while buffer still contained previous msg.

     - Buffer overflow: Message >128 bytes in length was received.

  It increments the statistics variables as appropriate.  The returned value should be checked before
  sending a message (bit 1 = 1 if tx buf is empty) and when receiving a message (bit 0 = 1 if rx buf
  contains a msg).

  This function is called from IsrFbs() as well as from FbsTsk functions.  Because all the stats counters
  are 16 bit, incrementing them is an atomic operation and no protection is needed against interrupts.  If
  counters become 32 bit (on the MCU), then the function must include masking of interrupts.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U msgsa = FIP_MSGSA_P;          // Read status register from uFIP

    return (msgsa);
}

/*---------------------------------------------------------------------------------------------------------*/
void FipISR(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function manages the interrupt from the network
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U               fip_msg;
    INT8U               fip_irq;
    INT16U              payload;
    INT16U              runlog_idx;
    INT16U              data;
    union TUnion32Bits  tmp_fp32;

    fip_irq = FIP_IRQSA_P;                                      // read FIP IRQ reg to ack (clear) the irq

    if ((fip_irq & 0x80) != 0)                                  // If Var 7 is accessible...
    {
        if ((FIP_MPSSA_P & 0x80) != 0)                          // If Var 7 is accessible...
        {
            // Clear CONFB bit 7 to lock Var 7
            FIP_CONFB_P &= 0x7F;                                // Disable FIP access to Var 7

            // in the Boot is done like this
            //          FIP_CONFD_P = (7 << 4) | (FIP_CONFD_P & 0x8F);      // Identify Var 7 access to uFip
            FIP_CONFD_P |= 0x70;                                // Identify Var 7 access to uFip

            // Check type of variable received
            if (FIP_VIDGL_P != 0)                               // Test FIP variable address (low byte) (0=Time 4=Code)
            {
                // Reset Variable 7 index to receive time variable next
                FIP_VIDGL_P = 0x00;                             // Reactivate Time variable reception (ID=0x3000)

                data = *((INT8U *)FIP_RTDATA0_32);              // Get RD var header byte

                if (data != fip.rd_header)                      // Check against last header
                {

                    fip.rd_header = data;                       // Save new header byte
                    fip.rd_rcvd_f = data;                       // Set RD rcvd flag (D cannot be zero)
                    tmp_fp32.part.hi = *((INT16U *)(FIP_RTDATA0_32 + fip.rt_ref_offset));           // Store in dpram
                    tmp_fp32.part.lo = *((INT16U *)(FIP_RTDATA2_32 + fip.rt_ref_offset));           // Store in dpram
                    dpcls.mcu.ref.rt_data = tmp_fp32.fp32;

                    dpcls.mcu.ref.rt_data_f = TRUE;        // Flag to DSP that new ref data is waiting
                    fbs.u.fieldbus_stat.ack |= FGC_ACK_RD_PKT;  // Set RD rcvd flag in status variable
                }
            }
            else // Time variable
            {
                // we validate our capture origin so we can fill the variable
                pll.fip.sync_time = PLL_NETIRQTIME_P;

                runlog_idx = *((INT16U *)(FIP_TIMEVAR_32 + fip.runlog_idx_offset));

                if (runlog_idx == fbs.last_runlog_idx)          // if fbs.time_v.runlog_idx == fbs.last_runlog_idx
                {
                    {
                        fbs.last_runlog_idx = runlog_idx;       // fbs.last_runlog_idx = fbs.time_v.runlog_idx

                        FIP_VIDGL_P = fip.rt_ref_vidgl;         // Change var 7 global index to receive RT data variable

                        memcpy(& (fbs.time_v), (void *) FIP_TIMEVAR_32, FIP_TIMEVAR_B);

                        fbs.time_rcvd_f = 1;                    // Set time rcvd flag (D cannot be zero)
                        fbs.events_rcvd_f = 1;                  // Set events rcvd flag (D cannot be zero)
                        fbs.u.fieldbus_stat.ack |= FGC_ACK_TIME;// Acknowledge time_var reception

                        // dpcom.mcu.time.fbs.unix_time = fbs.time_v.unix_time
                        dpcom.mcu.time.fbs.unix_time = fbs.time_v.unix_time;

                        // dpcom.mcu.time.fbs.ms_time = fbs.time_v.ms_time
                        dpcom.mcu.time.fbs.ms_time   = (INT32U) fbs.time_v.ms_time;
                    }
                }
                else
                {
                    fbs.last_runlog_idx = runlog_idx;           // fbs.last_runlog_idx = fbs.time_v.runlog_idx

                    FIP_VIDGL_P = fip.rt_ref_vidgl;             // Change var 7 global index to receive RT data variable

                    memcpy(& (fbs.time_v), (void *) FIP_TIMEVAR_32, FIP_TIMEVAR_B);

                    fbs.time_rcvd_f = 1;                        // Set time rcvd flag (D cannot be zero)
                    fbs.events_rcvd_f = 1;                      // Set events rcvd flag (D cannot be zero)
                    fbs.u.fieldbus_stat.ack |= FGC_ACK_TIME;    // Acknowledge time_var reception

                    dpcom.mcu.time.fbs.unix_time = fbs.time_v.unix_time;
                    dpcom.mcu.time.fbs.ms_time   = (INT32U) fbs.time_v.ms_time;
                }
            }

            FIP_CONFB_P |= 0x80;                               // Set bit 7 to unlock Var 7
        }
    }
    else
    {
        // Message received - pass to FbsTsk
        fip_msg = FipMsgsa();                                   // Call FipMsgsa() to test msg status (returned in B)

        if ((fip_msg & 0x01) != 0)      // Test bit 0 (rdy_msg = 1 if valid msg is waiting in buffer)
        {
            payload = FIP_MSCOR_P - 8;                          // Sub 8 from Message length to get payload length (1-120)

            if (payload <= 0)                                   // if message is too short (no payload)
            {
                FIP_CONFA_P = 0x01;                             // Disable message reception...
                FIP_CONFA_P = 0x03;                             // Enable message reception...
            }
            else
            {
                fbs.pkt_len = payload;                          // Save payload length for FbsTsk() to use

                // Extract header word (flags:user) from FIP msg
                fbs.rcvd_header = *FIP_MSG_HDR;                 // Save header word for FbsTsk()

                OSTskResume(TSK_FBS);
            }
        }
        else
        {
            // no valid message is waiting
            FIP_CONFA_P = 0x01;                                 // Disable message reception...
            FIP_CONFA_P = 0x03;                                 // Enable message reception...
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void FipCpyCmd(void * to, uint16_t length)
/*---------------------------------------------------------------------------------------------------------*\
  Callback to copy command packet
\*---------------------------------------------------------------------------------------------------------*/
{
    // Copy command packet into packet buffer

    memcpy(to, &(FIP_CMD_MSG->msg.cmd_pkt), length);

    FIP_CONFA_P = 0x01;                         // Disable message reception
    FIP_CONFA_P = 0x03;                         // Enable message reception
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fip.c
\*---------------------------------------------------------------------------------------------------------*/
