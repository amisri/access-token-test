/*---------------------------------------------------------------------------------------------------------*\
  File:         bgp.c

  Purpose:      FGC MCU Software - Background processing Task

  Author:       Quentin.King@cern.ch

  Notes:        The background task takes the role of an idle task. It never blocks and is used to do
                background monitoring of the FGC.
\*---------------------------------------------------------------------------------------------------------*/

#include <bgp.h>

#include <adc.h>
#include <class.h>           // Include class dependent definitions
#include <regfgc3_pars.h>
#include <shared_memory.h>   // for shared_mem global variable
#include <task_trace.h>
#include <tsk.h>             // for tsk_stk global variable

/*---------------------------------------------------------------------------------------------------------*/
void BgpTsk(void * unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the Background processing Task.  It is the lowest priority task and it never blocks, so
  it plays the role of an Idle task. It is responsible for background monitoring of the FGC:

        - MCU registers  (Not yet implemented)
        - Task stack consumption
\*---------------------------------------------------------------------------------------------------------*/
{
    for (;;)
    {
        TskTraceReset();

        // Copy the current state of the ADC Multiplexing from shared memory to the property
        // That code must run in a lower priority task than FCM and SCM, or a critical section must be
        // added to function SetInternalAdcMpx(). The choice that was made was to call it in the background
        // processing task.

        AdcCpyMpx();

        TskTraceInc();

        // Check Stack Usage

        
        BgpCheckStack(TSK_MST, tsk_stk.mst, STK_SIZE_MST);    // 0. MstTsk - Millisecond task
        
        BgpCheckStack(TSK_FBS, tsk_stk.fbs, STK_SIZE_FBS);    // 1. FbsTsk - Fieldbus task
    
        BgpCheckStack(TSK_STA, tsk_stk.sta, STK_SIZE_STA);    // 2. StaTsk - State machine task
 
        BgpCheckStack(TSK_SCIVS, tsk_stk.regfgc3, STK_SIZE_SCIVS);    // 3. ScivsTsk - SCIVS bus task

        BgpCheckStack(TSK_PUB, tsk_stk.pub, STK_SIZE_PUB);    // 4. PubTsk - Publication task

        BgpCheckStack(TSK_FCM, tsk_stk.fcm, STK_SIZE_FCM);    // 5. FcmTsk - Fieldbus commands task
       
        BgpCheckStack(TSK_TCM, tsk_stk.tcm, STK_SIZE_TCM);    // 6. TcmTsk - Terminal commands task
       
        BgpCheckStack(TSK_DLS, tsk_stk.dls, STK_SIZE_DLS);    // 7. DlsTsk - Dallas bus task
     
        BgpCheckStack(TSK_RTD, tsk_stk.rtd, STK_SIZE_RTD);    // 8. RtdTsk - Real-time display task
    
        BgpCheckStack(TSK_TRM, tsk_stk.trm, STK_SIZE_TRM);    // 9. TrmTsk - Terminal communications task
   
        BgpCheckStack(TSK_BGP, tsk_stk.bgp, STK_SIZE_BGP);    // 10. BgpTsk - Background processing task
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void BgpCheckStack(INT16U tsk_id, OS_STK * stk, INT16U stk_size)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the consumption of the stack.
  The results are stored in shared memory registers
  which are recorded in the runlog if the system crashes.
  They can also be seen through the property FGC.DEBUG.STACKUSE

  100 : all the stack already used (big probability of stack overflow)
    0 : all stack available
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      n;
    INT16U      empty_value;

    empty_value = 0xCAF0 + tsk_id;

    for (n = 0; n < stk_size && *(stk++) == empty_value; n++)
    {
    }

    shared_mem.stack_usage.b[tsk_id] = 100 - ((n * 100) / stk_size);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: bgp.c
\*---------------------------------------------------------------------------------------------------------*/
