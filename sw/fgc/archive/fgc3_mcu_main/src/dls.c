/*!
 *  @file     dls.c
 *  @defgroup FGC3:MCU main
 *  @brief    Dallas bus functions
 *
 * Notes:   This file contains the function for the Dallas Task and all the functions related to
 *          reading the IDs and temperatures from the Dallas devices controlled by the C62 microcontroller.
 */

#define DLS_GLOBALS             // define dls, id, temp, barcode, barcode_n_els global variables
#define DEFPROPS_INC_ALL        // defprops.h

// Includes

#include <dls.h>
#include <stdlib.h>             // for abs()
#include <string.h>             // for strncmp(), strlen()
#include <dev.h>                // for dev global variable
#include <dpcls.h>              // for dpcls global variable, CAL_T0
#include <fbs_class.h>          // for WARNINGS, ST_LATCHED
#include <fbs.h>                // for fbs global variable
#include <fgc/fgc_db.h>         // for access to CompDB
#include <log.h>                // for LogTStart()
#include <m16c62_link.h>        // for C62_SWITCH_PROG
#include <macros.h>             // for Test(), Set(), Clr()
#include <memmap_mcu.h>         // for CPU_RESET_CTRL_C62OFF_MASK16, SM_UXTIME_P
#include <microlan_1wire.h>     // for struct TBranchSummary, struct TMicroLAN_element_summary
#include <mst.h>                // for mst global variable
#include <os.h>                 // for OSTskSuspend()
#include <prop.h>               // for PropSetNumEls()
#include <shared_memory.h>      // for FGC_MP_MAIN_RUNNING
#include <sta.h>                // for sta global variable
#include <task_trace.h>
#include <time_fgc.h>

// Constants

// Dallas error codes

#define DLS_OK              0
#define DLS_OK_WAIT         1       // Use for slow commands, wait for slow command to be completed
#define DLS_C62_POWER_OFF   1
#define DLS_NO_TEMP_SENSOR  2
#define DLS_ERROR           3


/*!
 *   [10,20,30,40]
 * This function is called at 10Hz if the M16C62 is running.
 * It executes according to a state machine that aims to move the M16C62 from boot to main program
 * to running the temperature scans for the  different branches.
 *
 * @param m16c62_ctrl_reg
 * @return 0 if the process is still in progress and the M16C62 should remain powered.
 * It returns 1 when the operation is complete (successfully or otherwise) and the M16C62 should be
 * powered off.
 */
static INT16U DlsRun(INT16U m16c62_ctrl_reg);

/*!
 * [6x,7x]
 * This function is called to start the conversion of temperatures from sensors on the specified
 * logical path.
 * A failure will be considered to be a global failure and operation will stop.
 */
static INT16U DlsConvTemp(INT16U logical_path);

/*!
 * [8x,9x,10x,11x,12x,13x]
 * This function is called once a READ_TEMPS_BRANCH command has been launched in the M16C62.
 * It checks for the command to finish and then tries to read out the new temperature values
 * for the temp sensors on the branch.
 */
static INT16U DlsReadTemp(struct prop * p, INT16U m16c62_ctrl_reg);

/*!
 * This function is called once all the temperatures have been read to transfer the ADC and DCCT temperatures
 * (if valid) to the DSP.
 *
 * temp.fgc.in and temp.fgc.out are linked with the corresponding temperature sensor
 * via the assignment in components.xml with
 *     temp_prop = "TEMP.FGC.IN"
 * and
 *     temp_prop = "TEMP.FGC.OUT"
 * (this information is extracted from CompDB by the MCU program)
 */
static void DlsLogTemp(void);

/*!
 * This function will check if the measured temperature is valid and if so, it will set the DSP temp if
 * a pointer is supplied.
 * It returns TRUE if valid and FALSE if not valid and sets the temp_fail_f if
 * the report flag is TRUE.
 */
static BOOLEAN ValidateTemperatureAndCopyForDSP(BOOLEAN report_invalid, INT16U temp_to_check,
                                                volatile INT32U * dsp_temp);

/*!
 * [140,15x,16x,17x,18x]
 * This will read the IDs for all branches and will look for specific components
 * * fill the table id.to_summary_sorted_by_codebar[] sorting the elements in elements summary
 *
 * @return 0 if complete, 1 if need to wait, other for errors
 */
static INT16U DlsReadIDs(INT16U m16c62_ctrl_reg);

/*!
 * This will scan the barcodes of the devices for the specified branch looking for those that are not
 * DB lookup errors (which begin with a *).  Valid bar codes are linked to the BARCODE.BUS property and
 * also if the component type was found for the device and if the type has a barcode property specified,
 * then the barcode is also copied to this property.
 *
 */
static void DlsBarcodeBus(struct prop * p, INT16U logical_path);

/*!
 * This will scan the devices on each branch, looking for those for which a component has been identified
 * and for which the component has a temperature property defined.
 * For these devices, the link is made to the temperature property.
 *
 */
static void DlsPropTemp(INT16U logical_path);

/*!
 * This function is used to replace the "?" character in the temperature and barcode property names with
 * "A" or "B" according to the branch idx (0,1 -> 'A'  2,3 -> 'B')
 *
 */
static void DlsSetPropertyChan(INT16U logical_path, char * barcode_buf);

/*!
 * Send a M16C62 ISR command with one argument byte
 */
static INT16U DlsSendIsrCmd(INT8U cmd, INT8U arg);

/*!
 * Sends a byte to the M16C62 and waits up to C62_MAX_ISR_US for an immediate response
 *
 */
static INT16U DlsSendByte(INT8U tx, INT8U * rx);

/*!
 * Use DlsSendByte to read the response buffer from the M16C62
 *
 */
static INT16U DlsReadRsp(INT8U * buf, INT16U * max_bytes);

/*!
 * This function will prepare the response property when an error is detected.  The function also sets
 * the MPRUN registers to indicate that the dallas scan has completed, even though an error may have
 * stopped the scan early.
 *
 */
static void DlsRspError(struct prop * p, INT16U errloc, INT16U errnum, INT16U rsplen);

/*!
 * This recursive function will scan all the (CHAR) barcode properties and will reset the lengths of all that
 * were not set by a Dallas ID.  This is done before requesting a SYNC_FGC by the FGC manager so that old
 * barcodes don't remain to confuse the users.  Properties that were set by a Dallas ID have the
 * DF_SET_BY_DALLAS_ID dynamic property flag set.
 *
 */
static void DlsResetBarcodes(struct prop * p);

/*!
 *  This function gets the external IDs and sends them to the C62 to retrieve their 
 *  associated barcodes. 
 */
static INT8U DlsGetBarcodesForExternalIds(void);

/*!
 *
 */
static void DlsAddDeviceToBranch(INT8U branch_number, struct TMicroLAN_element_summary dev);

/*!
 *
 */
static void DlsSortDevicesByBarcode(void);

/*!
 *
 */
static void DlsIdentifyAndCountDevices(void);

/*!
 *
 */
static void DlsCheckGroups(void);

/*!
 *
 */
static void DlsLinkPropertiesAndTemperatures(void);

/*!
 *
 */
static INT16U DlsSendExternalId(union TMicroLAN_unique_serial_code id);

/*!
 *
 */
static INT16U DlsQueryIddbForBarcode(void);

/*!
 *
 */
static void DlsPrepareDeviceStructure(struct TMicroLAN_element_summary * device,
                                      union TMicroLAN_unique_serial_code id, struct barcode barcode, INT8U logical_path);

/*!
 *
 */
static INT16U DlsGetBarcodeForExternalId(struct barcode * this);


// External function definitions

void DlsTsk(void * unused)
{
    INT16U m16c62_ctrl_reg;

    // Initialise ADC and DCCT temperatures used by the DSP calibration system

    dpcls.mcu.cal.t_adc = CAL_T0 * ID_TEMP_GAIN_PER_C;
    dpcls.mcu.cal.t_dcct[0] = dpcls.mcu.cal.t_dcct[1] = dpcls.mcu.cal.t_adc;

    // Dallas Task main loop

    for (;;)
    {
        TskTraceReset();

        // Wait for MstTsk() to wake DslTsk at 10 Hz

        OSTskSuspend();

        TskTraceInc();

        m16c62_ctrl_reg = C62_P;

        if (m16c62_ctrl_reg & C62_POWER_MASK16)         // If M16C62 power is ON
        {
            if (DlsRun(m16c62_ctrl_reg) == DLS_C62_POWER_OFF)            // If M16C62 must be switched off
            {
                CPU_RESET_P |= CPU_RESET_CTRL_C62OFF_MASK16;    // Switch off M16C62

                TskTraceInc();

                if (dls.temp_fail_f)                // If Temperature failure detected
                {
                    WARNINGS |= FGC_WRN_TEMPERATURE;        // Set TEMPERATURE warning
                }
                else
                {
                    WARNINGS &= ~FGC_WRN_TEMPERATURE;       // Clear TEMPERATURE warning
                }
            }
        }
        else
        {
            if (!mst.s_mod_10                                   // If on the 10s boundary
                && (temp.unix_time != TimeGetUtcTime())         // for the first time
               )
            {
                temp.unix_time = TimeGetUtcTime();              // Record time stamp
                temp.tday_f = !mst.s_mod_600;                   // Make TDAY logging flag (10 minute period)

                if (!dls.inhibit_f)                     // If not inhibited
                {
                    C62_P = C62_SWITCH_PROG;                // Tell M16C62 boot to run in slave mode
                    CPU_RESET_P &= ~CPU_RESET_CTRL_C62OFF_MASK16;       // Turn on M16C62
                    dls.state = DLS_START;              // Prepare state machine
                    dls.temp_fail_f = FALSE;            // Clear temperature failed flag
                }
                else                            // else skip this acquisition
                {
                    DlsLogTemp();               // Log previous point again
                    dls.inhibit_f = FGC_CTRL_DISABLED;      // Remove inhibit
                }
            }
        }

        TskTraceInc();
    }
}

char * DlsIdString(const union TMicroLAN_unique_serial_code * devid, char * id_string)
{
    sprintf(id_string, "%02X%02X%02X%02X%02X%02X%02X%02X", (INT16U) devid->b[7], (INT16U) devid->b[6],
            (INT16U) devid->b[5], (INT16U) devid->b[4], (INT16U) devid->b[3], (INT16U) devid->b[2],
            (INT16U) devid->b[1], (INT16U) devid->b[0]);

    return (id_string);
}

void DlsSetId(union TMicroLAN_unique_serial_code ext_id)
{

    /*
     * External IDs are saved in a buffer, to be managed asynchronously by the dls task
     * Fail if buffer is full
     */

    if (id.external_id_num > DLS_EXTERNAL_ID_MAX)
    {
        DlsRspError(&PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_200), 1, 0);
        return;
    }

    if (dls.active_f)
    {
        DlsRspError(&PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_200), 2, 0);
        return;
    }

    id.external_ids[id.external_id_num] = ext_id;
    id.external_id_num++;

}

// Internal function definitions

INT16U DlsRun(INT16U m16c62_ctrl_reg)
{
    /*
     * The state machine relies on the fact that in case of error,
     * we return DLS_C62_POWER_OFF and the caller will reset the c62 (thus, restart the state machine).
     */

    INT16U errnum;
    INT8U rx;

    switch (dls.state)
    {
        case DLS_START:

            if (((m16c62_ctrl_reg ^ C62_SWITCH_PROG) & 0x00FF) != 0xFF)     // If M16C62 boot did not respond
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_10, M16C62_NO_RSP, 0);
                return (DLS_C62_POWER_OFF);
            }

            errnum = DlsSendByte(C62_SWITCH_PROG, &rx);

            if (errnum != DLS_OK)          // If request to switch to main prog fails
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_20, errnum, 0);
                return (DLS_C62_POWER_OFF);                     // Power off M16C62
            }

            dls.state = DLS_AT_MAINPROG;
            break;

        case DLS_AT_MAINPROG:

            if (m16c62_ctrl_reg & C62_CMDBUSY_MASK16)           // If main M16C62 program not started
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_30, M16C62_NO_RSP, 0);
                return (DLS_C62_POWER_OFF);                             // Power off M16C62
            }

            if (((m16c62_ctrl_reg ^ C62_SWITCH_PROG) & 0x00FF) != 0xFF) // If M16C62 boot did not respond
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_39, M16C62_NO_RSP, 0);
                return (DLS_C62_POWER_OFF);                                                 // Power off M16C62
            }

            // Read IDs only if IDs not read yet (once at start-up only)

            if (!dls.active_f)
            {
                errnum = DlsSendByte(C62_READ_IDS, &rx);

                if (errnum != DLS_OK)
                {
                    DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_40, errnum, 0);
                    return (DLS_C62_POWER_OFF);                         // Power off M16C62
                }

                dls.state = DLS_READID;                     // Next state is READID
                dls.timeout_cnt = C62_READID_TIMEOUT * 10;  // Set timeout in 100ms units
            }
            else
            {
                // Next state is CONVTEMPA

                dls.state = DLS_CONVTEMPA;
            }

            break;

        case DLS_READID:

            // Once at start-up

            errnum = DlsReadIDs(m16c62_ctrl_reg);// Get IDs and barcodes

            switch (errnum)
            {

                case DLS_OK:            // all ok
                    dls.state = DLS_READEXTID;
                    break;

                case DLS_OK_WAIT:           // waiting, wait another 100ms
                    break;                  // Keep M16C62 running

                default:                    // Other errors we cannot recover
                    return (DLS_C62_POWER_OFF);     // Power off M16C62
            }

            break;

        case DLS_READEXTID:

            // Once at start-up

            errnum = DLS_OK;

            if (id.external_id_num != 0)
            {
                errnum = DlsGetBarcodesForExternalIds();
            }

            switch (errnum)
            {
                case DLS_OK_WAIT:           // waiting, wait another 100ms
                    return DLS_OK;          // Keep M16C62 running

                case DLS_OK:
                    // Sort on barcode for each branch

                    DlsSortDevicesByBarcode();

                    // Identify and count the number of components found

                    DlsIdentifyAndCountDevices();

                    // Check found-per-group count against expected-per-group for the given system

                    DlsCheckGroups();

                    DlsLinkPropertiesAndTemperatures();

                    // Tell boot that main program has completed it's start up
                    shared_mem.mainprog_seq = FGC_MP_MAIN_RUNNING;
                    dls.state++;
            }

            return DLS_C62_POWER_OFF;

        case DLS_CONVTEMPA:

            errnum = DlsConvTemp(ID_LOGICAL_PATH_FOR_MEAS_A);

            switch (errnum)
            {
                case DLS_NO_TEMP_SENSOR: // If no sensors
                    dls.state = DLS_CONVTEMPB;
                    break;

                case DLS_OK:     // all ok
                    dls.state = DLS_READTEMPA;
                    return (DLS_OK);// Keep M16C62 running

                default:                    // Other errors we cannot recover
                    return (DLS_C62_POWER_OFF);     // Power off M16C62
            }

            break;

        case DLS_READTEMPA:

            errnum = DlsReadTemp(&PROP_FGC_DLS_MEAS_A, m16c62_ctrl_reg);

            switch (errnum)
            {
                case DLS_OK:            // all ok
                    dls.state++;        // Advance to next CONVTEMPx

                    // no break; on purpose
                case DLS_OK_WAIT:   // waiting, wait another 100ms
                    return (DLS_OK);// Keep M16C62 running

                default:                    // Other errors we cannot recover
                    return (DLS_C62_POWER_OFF);     // Power off M16C62
            }

            break;

        case DLS_CONVTEMPB:

            errnum = DlsConvTemp(ID_LOGICAL_PATH_FOR_MEAS_B);

            switch (errnum)
            {
                case DLS_NO_TEMP_SENSOR: // If no sensors
                    dls.state = DLS_CONVTEMPL;
                    break;

                case DLS_OK:     // all ok
                    dls.state = DLS_READTEMPB;
                    return (DLS_OK);// Keep M16C62 running

                default:                    // Other errors we cannot recover
                    return (DLS_C62_POWER_OFF);     // Power off M16C62
            }

            break;

        case DLS_READTEMPB:

            errnum = DlsReadTemp(&PROP_FGC_DLS_MEAS_B, m16c62_ctrl_reg);

            switch (errnum)
            {
                case DLS_OK:        // all ok
                    dls.state++;    // Advance to next CONVTEMPx

                    // no break; on purpose
                case DLS_OK_WAIT:   // waiting, wait another 100ms
                    return (DLS_OK);// Keep M16C62 running

                default:                    // Other errors we cannot recover
                    return (DLS_C62_POWER_OFF);     // Power off M16C62
            }

            break;

        case DLS_CONVTEMPL:

            errnum = DlsConvTemp(ID_LOGICAL_PATH_FOR_LOCAL);

            switch (errnum)
            {
                case DLS_NO_TEMP_SENSOR: // If no sensors
                    dls.state = DLS_LOGTEMPS;
                    break;

                case DLS_OK:// all ok
                    dls.state = DLS_READTEMPL;
                    return (DLS_OK);// Keep M16C62 running

                default:                    // Other errors we cannot recover
                    return (DLS_C62_POWER_OFF);     // Power off M16C62
            }

            break;

        case DLS_READTEMPL:

            errnum = DlsReadTemp(&PROP_FGC_DLS_LOCAL, m16c62_ctrl_reg);

            switch (errnum)
            {
                case DLS_OK:                     // all ok
                    dls.state++;// Advance to next CONVTEMPx

                    // no break; on purpose
                case DLS_OK_WAIT:// waiting, wait another 100ms
                    return (DLS_OK);// Keep M16C62 running

                default:                    // Other errors we cannot recover
                    return (DLS_C62_POWER_OFF);     // Power off M16C62
            }

            break;

        case DLS_LOGTEMPS:

            DlsLogTemp();
            return (DLS_C62_POWER_OFF); // Switch off M16C62
    }

    return (DLS_OK);             // Keep M16C62 running
}

static INT16U DlsConvTemp(INT16U logical_path)
{
    INT8U rx;
    INT16U errnum;

    // Check if there are temp sensors to be read

    if (!id.logical_paths_info[logical_path].ds18b20s)
    {
        return (DLS_NO_TEMP_SENSOR); // no temperature sensors
    }

    // Request M16C62 to scan branch for temperatures

    dls.logical_path = logical_path;

    errnum = DlsSendIsrCmd(C62_SET_BRANCH, logical_path); // Set logical_path for temp conversion

    if (errnum != 0)
    {
        DlsRspError(&PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_60 + logical_path), errnum, 0);
        return (3); // C62_SET_BRANCH fail
    }

    errnum = DlsSendByte(C62_READ_TEMPS_BRANCH, &rx);   // Request scan for temperatures on this branch

    if (errnum != 0)
    {
        DlsRspError(&PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_70 + logical_path), errnum, 0);
        return (1); // C62_READ_TEMPS_BRANCH fail
    }

    dls.timeout_cnt = C62_READTEMP_TIMEOUT * 10;    // Set timeout in 100ms units
    return (DLS_OK); // ok
}

INT16U DlsReadTemp(struct prop * p, INT16U m16c62_ctrl_reg)
{
    INT16U errnum;
    BOOLEAN err_f;
    INT16U in_elements_summary;
    INT16U n_temps;
    INT16U n_temps_read;
    INT16U rsp_len;
    INT8U data[M16C62_MAX_EXPECTED_RESPONSE];
    struct TMicroLAN_element_summary * dev;
    struct prop ** prop_temp;

    // Check if READ_TEMPS_BRANCH has completed

    if ((m16c62_ctrl_reg & C62_CMDBUSY_MASK16) != 0)        // If C62_READ_TEMPS_BRANCH command NOT completed
    {
        dls.timeout_cnt--;

        if (dls.timeout_cnt == 0)               // if timeout
        {
            DlsRspError(p, (DLS_ERROR_LOCATION_80 + dls.logical_path), DLS_BG_CMD_TIMEOUT, 0);
            return (2);     // timeout expired
        }

        return (DLS_OK_WAIT);     // waiting, wait another 100ms
    }

    // Check response to READ_TEMPS_BRANCH BG command

    rsp_len = M16C62_MAX_EXPECTED_RESPONSE - 1;

    errnum = DlsReadRsp((INT8U *) &data, (INT16U *) &rsp_len);

    if (errnum != 0)
    {
        DlsRspError(p, (DLS_ERROR_LOCATION_90 + dls.logical_path), errnum, 0);
        return (3);     // read response fail
    }

    if (rsp_len == 0)                       // If no rsp received
    {
        DlsRspError(p, (DLS_ERROR_LOCATION_100 + dls.logical_path), M16C62_NO_MORE_DATA, 0);
        return (4); // response empty
    }

    err_f = data[0];

    if (err_f != 0)                             // If READ_TEMPS_BRANCH failed
    {
        memcpy(((INT8U *)(p->value)) + 1, &data, rsp_len);

        // Report failure for branch
        DlsRspError(p, (DLS_ERROR_LOCATION_110 + dls.logical_path), data[0], (rsp_len - 1));
        data[2] = ID_BAD_TEMP;                      // Prepare "BAD TEMP" value
        data[3] = 0;
    }

    dev = id.element_info_ptr[dls.logical_path];
    n_temps = id.logical_paths_info[dls.logical_path].ds18b20s;
    prop_temp = &id.prop_temp[id.start_in_elements_summary[dls.logical_path]];

    // Scan the list of devices for the temp sensors

    for (in_elements_summary = n_temps_read = 0; n_temps_read < n_temps;
         in_elements_summary++, dev++, prop_temp++)
    {
        // If device is a temp sensor
        if (dev->unique_serial_code.p.family == MICROLAN_DEVICE_DS18B20_THERMOMETER)
        {
            if (!err_f)                                           // If no error for branch
            {
                errnum = DlsSendIsrCmd(C62_SET_DEV, in_elements_summary);                  // Set device index

                if (errnum != 0)    // Set device index
                {
                    DlsRspError(p, (DLS_ERROR_LOCATION_120 + dls.logical_path), errnum, 0);
                    return (5); // C62_SET_DEV fail
                }

                rsp_len = sizeof(data);

                errnum = DlsReadRsp((INT8U *) &data, (INT16U *) &rsp_len);       // Get temperature for device

                if (errnum != 0)
                {
                    DlsRspError(p, (DLS_ERROR_LOCATION_130 + dls.logical_path), errnum, 0);
                    return (3);     // read response fail
                }
            }

            dev->temp_C = data[2];               // Recover temperature from response
            dev->temp_C_16 = data[3];               // or BAD TEMP if error occurred

            if (*prop_temp)
            {
                *((INT16U *)((*prop_temp)->value)) = dev->temp_C * 16 + dev->temp_C_16;
            }

            n_temps_read++;
        }
    }

    return (DLS_OK);         // ok
}

void DlsLogTemp(void)
{

    // Calculate FGC delta (outlet - inlet)

    if (ValidateTemperatureAndCopyForDSP(REPORT_INVALID_TEMPERATURE, temp.fgc.in, &dpcls.mcu.cal.t_adc)
        && ValidateTemperatureAndCopyForDSP(REPORT_INVALID_TEMPERATURE, temp.fgc.out, NULL)
#if (FGC_CLASS_ID == 61 || FGC_CLASS_ID == 62)
        // ToDo: re-think, i.e. Acapulco has a negative flow
        // may be to check against a threshold for the delta
        //       && ( temp.fgc.out > temp.fgc.in )
       )
    {
        temp.fgc.delta = abs(temp.fgc.out - temp.fgc.in); // because negative airflow are not handled for the moment
#else
        && (temp.fgc.out > temp.fgc.in)
       )
    {
        temp.fgc.delta = temp.fgc.out - temp.fgc.in;
#endif
    }
    else
    {
        temp.fgc.delta = ID_BAD_TEMP * ID_TEMP_GAIN_PER_C;      // Bad data: 99.0C
        dls.temp_fail_f = TRUE;
    }

    // Transfer DCCT temperatures to DSP - use other channel if measurement fails, or ADC

    // for the moment we only have ACAPULCOs, RPCAB
    // and it haven't DCCT with temperature sensors
    dpcls.mcu.cal.t_dcct[0] = dpcls.mcu.cal.t_dcct[1] = dpcls.mcu.cal.t_adc; // Use FGC temp for DCCTs

    // Collect temperatures (16-bit) ready to be logged

    temp.log.dcct_a = temp.dcct[0].elec;
    temp.log.dcct_b = temp.dcct[1].elec;

    temp.log.fgc_in = temp.fgc.in;
    temp.log.fgc_out = temp.fgc.out;

    temp.thour_f = TRUE;
}

static BOOLEAN ValidateTemperatureAndCopyForDSP(BOOLEAN report_invalid, INT16U temp_to_check,
                                                volatile INT32U * dsp_temp)
{
    if (temp_to_check && (temp_to_check < (ID_BAD_TEMP * ID_TEMP_GAIN_PER_C)))
    {
        // If DSP temperature pointer supplied

        if (dsp_temp != NULL)
        {
            *dsp_temp = (INT32U) temp_to_check;     // Also set this variable to the temperature
        }

        return (TRUE);
    }

    if (report_invalid)
    {
        dls.temp_fail_f = TRUE;                 // Report temperature failure
    }

    return (FALSE);
}

static INT16U DlsReadIDs(INT16U m16c62_ctrl_reg)
{
    INT16U errnum;
    INT16U rsp_len;
    INT16U in_elements_summary;
    INT16U logical_path;
    struct TMicroLAN_element_summary * element_info_ptr;

    // Check if READ_IDS has completed

    if (m16c62_ctrl_reg & C62_CMDBUSY_MASK16)       // If C62_READ_IDS command NOT completed
    {
        dls.timeout_cnt--;

        if (!dls.timeout_cnt)                       // if timeout
        {
            DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_140, DLS_BG_CMD_TIMEOUT, 0);
            return (2); // timeout expired
        }

        return (DLS_OK_WAIT);     // wait another 100ms
    }

    // Read out ID device information for all branches

    element_info_ptr = (struct TMicroLAN_element_summary *) &id.devs;

    for (logical_path = 0; logical_path < FGC_ID_N_BRANCHES; logical_path++)
    {
        errnum = DlsSendIsrCmd(C62_SET_BRANCH, logical_path);

        if (errnum != 0)
        {
            DlsRspError(&PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_150 + logical_path), errnum, 0);
            return (3); // C62_SET_BRANCH fail
        }

        rsp_len = sizeof(struct TBranchSummary);

        errnum = DlsReadRsp((INT8U *) &id.logical_paths_info[logical_path], &rsp_len);

        if (errnum != 0)
        {
            DlsRspError(&PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_160 + logical_path), errnum, 0);
            return (4); // read response fail
        }

        id.element_info_ptr[logical_path] = element_info_ptr;
        id.start_in_elements_summary[logical_path] = id.n_devs;

        for (in_elements_summary = 0; in_elements_summary < id.logical_paths_info[logical_path].total_devices;
             in_elements_summary++)
        {
            errnum = DlsSendIsrCmd(C62_SET_DEV, in_elements_summary);

            if (errnum != 0)
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_170 + logical_path), errnum, 0);
                return (5);     // C62_SET_DEV fail
            }

            rsp_len = sizeof(struct TMicroLAN_element_summary);

            errnum = DlsReadRsp((INT8U *) element_info_ptr, &rsp_len);

            if (errnum != 0)
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_180 + logical_path), errnum, 0);
                return (4); // read response fail
            }

            element_info_ptr++;
            id.n_devs++;
        }
    }

    dls.active_f = id.n_devs;       // Mark Dallas system as active if at least one ID device found

    return (DLS_OK); // ok
}

void DlsBarcodeBus(struct prop * p, INT16U logical_path)
{
    INT16U ii;
    INT16U n_els;
    INT16U idx_in_summary;
    fgc_db_t comp_idx;
    char propertyname[FGC_COMPDB_PROP_LEN];
    INT16U total_devices_on_logical_path;
    INT8U ** bc;
    struct TMicroLAN_element_summary * element_info_ptr;

    bc = (INT8U **) p->value;

    idx_in_summary = id.start_in_elements_summary[logical_path];

    element_info_ptr = id.element_info_ptr[logical_path];

    total_devices_on_logical_path = id.logical_paths_info[logical_path].total_devices;

    p->dynflags |= DF_SET_BY_DALLAS_ID;

    // For all devices on the specified branch
    for (ii = 0; ii < total_devices_on_logical_path; ii++, element_info_ptr++, idx_in_summary++)
    {
        if (element_info_ptr->barcode.c[0] <= 'Z')          // If barcode is valid (starts upper case)
        {
            n_els = PropGetNumEls(NULL, p);

            if (n_els < p->max_elements)
            {
                bc[n_els] = (INT8U *)&element_info_ptr->barcode.c;      // Add barcode to bus property
                PropSetNumEls(NULL, p, n_els + 1);
            }

            comp_idx = dev.sys.comp_idx[idx_in_summary];

            if (comp_idx)                       // If component was identified
            {
                // Get barcode property name

                memcpy(&propertyname, CompDbBarcodeProperty(comp_idx), FGC_COMPDB_PROP_LEN);

                if (*propertyname)
                {
                    DlsSetPropertyChan(logical_path, propertyname); // Replace '?' with A or B

                    OSSemPend(pcm.sem);

                    if (!PropFind(propertyname))
                    {
                        memcpy(pcm.prop->value, &(element_info_ptr->barcode.c), FGC_ID_BARCODE_LEN);
                        PropSetNumEls(NULL, pcm.prop, strlen(pcm.prop->value));

                        pcm.prop->dynflags |= DF_SET_BY_DALLAS_ID;
                    }

                    OSSemPost(pcm.sem);
                }
            }
        }
    }
}

void DlsPropTemp(INT16U logical_path)
{
    INT16U ii;
    INT16U idx_in_summary;
    fgc_db_t comp_idx;
    char propertyname[FGC_COMPDB_PROP_LEN];
    INT16U total_devices_on_logical_path;

    idx_in_summary = id.start_in_elements_summary[logical_path];
    total_devices_on_logical_path = id.logical_paths_info[logical_path].total_devices;

    for (ii = 0; ii < total_devices_on_logical_path; ii++, idx_in_summary++)
    {
        comp_idx = dev.sys.comp_idx[idx_in_summary];

        if (comp_idx)                                       // If component was identified
        {
            // Get temperature property name

            memcpy(&propertyname, CompDbTempProperty(comp_idx), FGC_COMPDB_PROP_LEN);

            if (*propertyname)
            {
                DlsSetPropertyChan(logical_path, propertyname); // Replace '?' with A or B

                OSSemPend(pcm.sem);

                if (!PropFind(propertyname))            // If property found
                {
                    id.prop_temp[idx_in_summary] = pcm.prop;        // Link to device
                }

                OSSemPost(pcm.sem);
            }
        }
    }
}

void DlsSetPropertyChan(INT16U logical_path, char * barcode_buf)
{
    if (logical_path < ID_LOGICAL_PATH_FOR_LOCAL)
    {
        while (*barcode_buf && (*barcode_buf != '?'))
        {
            barcode_buf++;
        }

        if (*barcode_buf)
        {
            *barcode_buf = 'A' + logical_path / 2;
        }
    }
}

INT16U DlsSendIsrCmd(INT8U cmd, INT8U arg)
{
    INT16U errnum;
    INT8U rx;

    errnum = DlsSendByte(cmd, &rx);

    if (errnum != 0)
    {
        return (errnum);
    }

    return (DlsSendByte(arg, &rx));
}

INT16U DlsSendByte(INT8U tx, INT8U * rx)
{
    OS_CPU_SR cpu_sr;
    uint16_t reg;
    uint32_t t0;

    OS_ENTER_CRITICAL()
    ;

    C62_P = (uint16_t) tx;            // Send byte
    t0 = TimeGetUs();

    OS_EXIT_CRITICAL()
    ;

    do
    {
        reg = C62_P;
    }
    while (!(reg & (C62_DATARDY_MASK16 | C62_CMDERR_MASK16)) && ((TimeGetUsDiffNow(t0)) < C62_MAX_ISR_US));

    reg = C62_P;

    *rx = (reg & 0xFF);                 // Return data byte

    if (reg & C62_DATARDY_MASK16)       // If data ready
    {
        return (DLS_OK);
    }

    if (reg & C62_CMDERR_MASK16)        // If command error
    {
        return (DLS_ISR_CMD_ERR);
    }

    return (DLS_ISR_CMD_TIMEOUT);       // Timeout
}

INT16U DlsReadRsp(INT8U * buf, INT16U * max_bytes)
{
    INT16U errnum;
    INT16U n_bytes;

    for (n_bytes = 0; n_bytes < *max_bytes; n_bytes++, buf++)
    {
        errnum = DlsSendByte(C62_GET_RESPONSE, buf);

        if ((errnum == DLS_ISR_CMD_ERR) && (*buf == M16C62_NO_MORE_DATA))
        {
            *max_bytes = n_bytes;
            return (DLS_OK);
        }

        if (errnum)
        {
            *max_bytes = n_bytes;
            return (errnum);
        }
    }

    return (DLS_OK);
}

static void DlsRspError(struct prop * p, INT16U errloc, INT16U errnum, INT16U rsp_len)
{
    static INT32U last_time_start = 0;
    static INT32U last_error_count = 0;

    INT8U * rsp = (INT8U *) p->value;

    rsp[0] = errloc;                // Store error location and errnum in property
    rsp[1] = errnum;

    last_error_count++;

    // Set the latched condition only if the rate of dallas errors is
    // larger than 5 per hour.

    if (last_error_count > 5)
    {
        if ((mst.time.unix_time - last_time_start) < 3600)
        {
            Set(ST_LATCHED, FGC_LAT_DALLAS_FLT);
        }

        last_time_start = mst.time.unix_time;
        last_error_count = 0;
    }

    dls.num_errors++;                           // Increment error counter

    dls.error_time = TimeGetUtcTime();        // Time of last error

    PropSetNumEls(NULL, p, rsp_len + 2);    // Set property length (Header bytes + C62 response)

    shared_mem.mainprog_seq = FGC_MP_MAIN_RUNNING;
}

void DlsResetBarcodes(struct prop * p)
{
    INT16U ii;
    INT16U n_els;
    struct prop * child_prop;

    if (p->type == PT_PARENT)                       // If property is a parent
    {
        child_prop = p->value;                      // Get address of array of lower level properties
        n_els = PropGetNumEls(NULL, p);             // Get number of low level properties

        for (ii = 0; ii < n_els; ii++, child_prop++)  // For each child property
        {
            DlsResetBarcodes(child_prop);           // Call function recursively for the child
        }
    }
    else                                            // else property is NOT a parent
    {
        if ((p->type == PT_CHAR)                         // If property is CHAR type (i.e. contains a barcode)
            && !Test(p->dynflags, DF_SET_BY_DALLAS_ID)      // and property was set NOT by a Dallas ID
           )
        {
            PropSetNumEls(NULL, p, 0);              // Clear property length
        }
    }
}

static INT16U DlsSendExternalId(union TMicroLAN_unique_serial_code external_id)
{
    INT8U rx;       // M16C62 response byte
    INT8U i;        // Loop variable
    INT16U errnum;  // M16C62 return values

    // Send a fast command which prepares a buffer for the external ID
    errnum = DlsSendByte(C62_SET_EXTERNAL_ID, &rx);

    // If sending an external ID failed
    // rx value should be equal to size of the ID buffer on the M16C62 side
    if (errnum != 0 || rx != 8)
    {
        DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_190, errnum, 0);
        return (DLS_ERROR);
    }

    // Send the ID byte-by-byte
    for (i = 0; i < MICROLAN_UNIQUE_SERIAL_CODE_LEN; i++)
    {
        errnum = DlsSendByte(external_id.b[i], &rx);

        if (errnum != 0 || rx != 7 - i)
        {
            DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_190, errnum, 0);
            return (DLS_ERROR);
        }
    }

    return DLS_OK;
}

static INT16U DlsQueryIddbForBarcode(void)
{
    INT8U rx;       // M16C62 response byte
    INT16U errnum;  // M16C62 return values

    // Send a slow command which queries IDDB for barcode matching the external ID sent earlier
    errnum = DlsSendByte(C62_GET_BARCODE_FOR_EXT_ID, &rx);

    if (errnum != 0)
    {
        DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_190, errnum, 0);
        return (DLS_ERROR);
    }

    return DLS_OK;
}

static INT16U DlsGetBarcodeForExternalId(struct barcode * this)
{
    INT16U errnum;
    INT16U max_bytes = FGC_ELEMENT_BARCODE_LEN;

    if (C62_P & C62_CMDBUSY_MASK16)       // If C62_READ_IDS command NOT completed
    {
        dls.timeout_cnt--;

        if (!dls.timeout_cnt)                       // if timeout
        {
            DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_190, DLS_BG_CMD_TIMEOUT, 0);
            return (DLS_ERROR);         // timeout expired
        }

        return (DLS_OK_WAIT);           // wait another 100ms
    }

    errnum = DlsReadRsp((uint8_t *) this->c, &max_bytes);

    if (errnum != 0)
    {
        DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_190, errnum, 0);
        return (DLS_ERROR);
    }

    return DLS_OK;
}

static void DlsPrepareDeviceStructure(struct TMicroLAN_element_summary * device,
                                      union TMicroLAN_unique_serial_code this_id, struct barcode this_barcode, INT8U logical_path)
{

    device->unique_serial_code = this_id;
    device->barcode = this_barcode;
    device->logical_path = logical_path;
    device->temp_C = 0;
    device->temp_C_16 = 0;
}


static INT8U DlsGetBarcodesForExternalIds(void)
{
    /*
     * For this function implementation, see issue EPCCCS-6009
     */
    static uint8_t id_idx = 0;
    static bool executed_already = false;

    INT16U errnum;
    struct barcode temp_barcode = {{0}};    
    struct TMicroLAN_element_summary device;
       
    if (id_idx >= id.external_id_num)
    {
        return DLS_OK;
    }

    if (executed_already == false)
    {
        if (DlsSendExternalId(id.external_ids[id_idx]) != DLS_OK)
        {
            return (DLS_ERROR);
        }

        if (DlsQueryIddbForBarcode() != DLS_OK)
        {
            return (DLS_ERROR);
        }

        executed_already = true; 
    }

    errnum = DlsGetBarcodeForExternalId(&temp_barcode);

    if (errnum != DLS_OK)
    {
        return errnum;
    }

    DlsPrepareDeviceStructure(&device, id.external_ids[id_idx], temp_barcode, ID_LOGICAL_PATH_FOR_CRATE);

    DlsAddDeviceToBranch(ID_LOGICAL_PATH_FOR_CRATE, device);

    id_idx++;
    executed_already = false; 

    if (id_idx == id.external_id_num)
    {
        return DLS_OK;
    }
    else
    {
        return DLS_OK_WAIT;
    }
    
}

void DlsAddDeviceToBranch(INT8U branch_number, struct TMicroLAN_element_summary dev)
{
    INT8U i;

    // Shift all devices in id.devs table after point of insertion.

    if (branch_number >= (FGC_ID_N_BRANCHES - 1))
    {
        // Do not shift if we add to the last branch
    }
    else
    {
        for (i = (id.n_devs - 1); i > (id.start_in_elements_summary[branch_number + 1]); i--)
        {
            id.devs[i] = id.devs[i - 1];
        }

        for (i = (branch_number + 1); i < FGC_ID_N_BRANCHES; i++)
        {
            id.start_in_elements_summary[i]++;
        }
    }

    // Insert dev

    id.devs[id.start_in_elements_summary[branch_number] + id.logical_paths_info[branch_number].total_devices] =
        dev;
    id.logical_paths_info[branch_number].total_devices++;
    id.n_devs++;

    dls.active_f = id.n_devs;       // Mark Dallas system as active if at least one ID device found
}

static void DlsSortDevicesByBarcode(void)
{
    INT16U logical_path;
    INT16U in_elements_summary;
    INT16U c;
    INT16U ii;
    INT16U in_sorted;
    struct TMicroLAN_element_summary * element_info_ptr;

    for (logical_path = 0; logical_path < FGC_ID_N_BRANCHES; logical_path++)
    {
        element_info_ptr = id.element_info_ptr[logical_path];

        in_elements_summary = id.start_in_elements_summary[logical_path];

        for (c = 0; c < id.logical_paths_info[logical_path].total_devices;
             c++, element_info_ptr++, in_elements_summary++)
        {
            for (ii = c, in_sorted = in_elements_summary;
                 (ii > 0)
                 && strcmp(&element_info_ptr->barcode.c[0],
                           &id.devs[id.to_summary_sorted_by_codebar[in_sorted - 1]].barcode.c[0]) < 0;
                 ii--, in_sorted--)
            {
                id.to_summary_sorted_by_codebar[in_sorted] = id.to_summary_sorted_by_codebar[in_sorted - 1];
            }

            id.to_summary_sorted_by_codebar[in_sorted] = in_elements_summary;
        }
    }
}


static void DlsIdentifyAndCountDevices(void)
{
    struct TMicroLAN_element_summary * element_info_ptr;
    char * codebar_ptr;
    INT16U in_elements_summary;
    fgc_db_t comp_idx;
    fgc_db_t grp_idx;
    INT16S mismatch;
    char comp_type[FGC_COMPDB_COMP_LEN + 2];
    const fgc_db_t n_comps = CompDbLength();

    element_info_ptr = (struct TMicroLAN_element_summary *) &id.devs;

    // For every 1-wire device found
    for (in_elements_summary = 0; in_elements_summary < id.n_devs; in_elements_summary++, element_info_ptr++)
    {
        codebar_ptr = (char *) &element_info_ptr->barcode.c;

        // we consider barcode valid if starts upper case
        if (*codebar_ptr <= 'Z')
        {
            comp_idx = 1;               // Start from component 1 (0=Unknown)

            do                          //  Scan list of comps in TYPEDB to find a match
            {
                memcpy(&comp_type, CompDbType(comp_idx), FGC_COMPDB_COMP_LEN + 2);
            }
            while (((mismatch = strncmp(codebar_ptr, comp_type, FGC_COMPDB_COMP_LEN)) > 0)
                   && (++comp_idx < n_comps));

            if (mismatch)
            {
                comp_idx = 0;           // system 0 (unknown)
            }
        }
        else                            // else barcode is not valid
        {
            comp_idx = 0;               // system 0 (unknown)
        }

        dev.sys.comp_idx[in_elements_summary] = comp_idx;

        grp_idx = CompDbGroupIndex(comp_idx,dev.sys.sys_idx);

        if (grp_idx < FGC_N_COMP_GROUPS)
        {
            dev.sys.detected[grp_idx]++;

            if (!grp_idx)
            {
                dev.sys.n_unknown_grp[comp_idx]++;
            }
        }
    }
}

static void DlsCheckGroups(void)
{
    BOOLEAN id_flt_f;
    fgc_db_t grp_idx;

    id_flt_f = FALSE;
    PROP_BARCODE_MISMATCHED.n_elements = dev.sys.detected[0];

    for (grp_idx = 1; grp_idx < FGC_N_COMP_GROUPS; grp_idx++)
    {
        if (dev.sys.detected[grp_idx] != SysDbRequiredGroups(dev.sys.sys_idx,grp_idx))      // If any group is mismatched
        {
            if (dev.sys.detected[grp_idx] < SysDbRequiredGroups(dev.sys.sys_idx,grp_idx))   // If reqd comps are missing
            {
                id_flt_f = TRUE;
            }

            PROP_BARCODE_MISMATCHED.n_elements++;
        }
        else
        {
            if (SysDbRequiredGroups(dev.sys.sys_idx,grp_idx))
            {
                PROP_BARCODE_MATCHED.n_elements++;
            }
        }
    }

    if (id_flt_f)                   // If any required components are missing
    {
        // TODO ID_FLT should eventually be a fault for operational devices

        ST_LATCHED |= FGC_LAT_ID_FLT;       // Set ID_FLT in latched status
    }
    else
    {
        ST_LATCHED &= ~FGC_LAT_ID_FLT;      // Clear ID_FLT in latched status
    }
}

static void DlsLinkPropertiesAndTemperatures(void)
{
    // Link BARCODE.BUS properties to devices and store barcodes in BARCODE properties

    DlsBarcodeBus(&PROP_BARCODE_BUS_V, ID_LOGICAL_PATH_FOR_VS_A);
    DlsBarcodeBus(&PROP_BARCODE_BUS_V, ID_LOGICAL_PATH_FOR_VS_B);
    DlsBarcodeBus(&PROP_BARCODE_BUS_A, ID_LOGICAL_PATH_FOR_MEAS_A);
    DlsBarcodeBus(&PROP_BARCODE_BUS_B, ID_LOGICAL_PATH_FOR_MEAS_B);
    DlsBarcodeBus(&PROP_BARCODE_BUS_C, ID_LOGICAL_PATH_FOR_CRATE);
    DlsBarcodeBus(&PROP_BARCODE_BUS_L, ID_LOGICAL_PATH_FOR_LOCAL);

    // Link devices to temperatures properties

    DlsPropTemp(ID_LOGICAL_PATH_FOR_VS_A);
    DlsPropTemp(ID_LOGICAL_PATH_FOR_VS_B);
    DlsPropTemp(ID_LOGICAL_PATH_FOR_MEAS_A);
    DlsPropTemp(ID_LOGICAL_PATH_FOR_MEAS_B);
    DlsPropTemp(ID_LOGICAL_PATH_FOR_LOCAL);

    // Start temperature logging

    LogTStart(&log_thour);
    LogTStart(&log_tday);

    // Set CONFIG.MODE/CONFIG.STATE according to whether SYNC_FGC will be attempted

    if (dls.active_f// If at least one component found and
        && (sta.config_state != FGC_CFG_STATE_STANDALONE)// not running STANDALONE
        && (sta.config_mode != FGC_CFG_MODE_SYNC_FAILED)// and SYNC_FAILED not set and
        && (dev.name[0] != '*')// NameDB is valid
       )
    {
        DlsResetBarcodes(&PROP_BARCODE);            // Reset barcodes not set by Dallas IDs

        if (fbs.id != 0)
        {
            sta.config_mode = FGC_CFG_MODE_SYNC_FGC;    // Initialise CONFIG.MODE to SYNC_FGC
        }
    }
    else                                            // else S ID RESET
    {
        if (sta.config_state != FGC_CFG_STATE_STANDALONE)
        {
            sta.config_state = FGC_CFG_STATE_UNSYNCED;  // Set config state to UNSYNCED
        }
    }
}

// EOF

