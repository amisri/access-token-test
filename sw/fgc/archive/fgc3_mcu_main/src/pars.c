/*!
 *  @file      pars.c
 *  @defgroup  FGC3:MCU:Main
 *  @brief     Parameter parsing Functions.
 */

#define DEFPROPS_INC_ALL    // defprops.h

// Include

#include <math.h>


#include <pars.h>
#include <string.h>
#include <class.h>
#include <definfo.h>
#include <defprops.h>
#include <dev.h>
#include <fgc_errs.h>
#include <log.h>
#include <macros.h>
#include <mst.h>
#include <os.h>
#include <pars_service.h>
#include <prop.h>
#include <dpcls.h>

#if (FGC_CLASS_ID == 62)
#include <name_db.h>
#endif

#include <math.h>

#ifdef DEBUG
#define DBLEN       20
static INT16U       dbidx;
static struct
{
    INT16U  pars_idx;
    INT16U  n_pars;
    INT16U  out_idx;
    INT16U  n_chars;
    INT16U  pkt_buf_idx;
    INT16U  n_carryover_chars;
    INT16U  last_par_f;
    INT16U  header;
    INT16U  end_cmd_f;
} db[DBLEN];
#endif

// Static function declaration


/*!
 * This function is used to convert an AbsTime parameter from the command packet.  An AbsTime is formated
 * as UNIX_TIME.US or as a double precision value formatted as "%.17e".  This must be parsed as two integers
 * as there is no double precision support.
 * .
 * @param c command packet
 * @param value value of conversion (if success), TIME.NOW if no token is supplied or the value is zero.
 * @return errnum:
 *       0                       AbsTime successfuly converted in *value
 *       FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
 *       FGC_NO_SYMBOL           Token buffer empty
 *       FGC_SYNTAX_ERROR        Token too long or contains a space before a valid delimiter
 *       FGC_BAD_INTEGER         Token contains non-valid characters
 *       FGC_INVALID_TIME        Microseconds > 999999
 */
static INT16U ParsScanAbsTime(struct cmd * c, struct abs_time_us ** value);


/*!
 * This function is used to convert a Char parameter from the command packet.  Char parameters can be
 * comma separated if required, or can simply be concatenated as a string.  If the scan succeeds, the
 * function returns zero and the character will be returned in *value.  The calling function will know
 * that the returned value is the last when the c->last_par_f is set.
 *
 * @param c command packet
 * @param value character after conversion
 *
 * @return errnum:
 *       0                       Character successfully converted in *value
 *       FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
 *       FGC_NO_SYMBOL           Token buffer empty
 *       FGC_NO_DELIMITER        End of buffer found before a valid delimiter
 *       FGC_SYNTAX_ERROR        Token buffer full or space found before a valid delimiter
 */
static INT16U ParsScanChar(struct cmd * c, char ** value);

/*!
 * This function is used to convert unsigned Point types from the command packet. A Point type 
 * is specified as "ref|time". If the conversion succeeds, the value will be returned in *value. 
 *
 * @param c command packet
 * @param p property
 * @param value
 * @return errnum:
 *       0                       Number successfuly converted in *value
 *       FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
 *       FGC_BAD_INTEGER         Token contains spaces or non-valid characters
 *       FGC_NO_DELIMITER        End of buffer found before a valid delimiter
 *       FGC_SYNTAX_ERROR        Token buffer full or contains a space before a valid delimiter
 *       FGC_NO_SYMBOL           Token buffer empty
 */
#if (FGC_CLASS_ID != 62)
static INT16U ParsScanPoint(struct cmd * c, struct prop * p, struct point ** value);
#endif
/*!
 * See the documentation for function ParsScanSymList(). This function has the same functionality,
 * except it adds one more possible error status: FGC_GENERIC_NONE_SYMBOL.
 * That additional error is used for the purpose of the generic "NONE" symbol available by default on all BIT_MASK
 * properties. That symbol allows to reset the bit mask. E.g. S TEST.BIT_MASK NONE
 */
static INT16U ParsScanSymListAndNone(struct cmd * c, const char * delim, const struct sym_lst * sym_lst,
                                     const struct sym_lst ** sym_const);


/*!
 * This function is called from ParsValueGet() if a symlist value is required.  If it is a bit mask symlist
 * then multiple symbols can be entered separated by | characters.
 *
 * @param c command packet
 * @param p property
 * @param value parsed value
 * @return
 */
static INT16U ParsValueGetSymList(struct cmd * c, struct prop * p, void ** value);


/*!
 * Converter char [0-9],[A-F] into integer value
 * @param ch character
 * @return integer value, 0x10 if not a valid char
 */
static inline INT16U ParsScanHexDigit(char ch);


// External function definition

INT16U ParsScanInteger(struct cmd * c, const char * delim, INT32U * value)
{
    char        ch;                     // Get first character from command buffer
    INT16U      errnum;                 // Return status
    INT16U      n_ch;                   // Number of characters in token
    char    *   buf;                    // Character pointer into token buffer
    INT16U      digit;                  // Working digit value
    INT32U      val;                    // Unsigned working value

    if (delim)
    {
        errnum = ParsScanToken(c, delim);       // Get next token

        if (errnum != 0)
        {
            return (errnum);
        }
    }

    c->last_par_f = c->end_cmd_f;       // Set last_par_f to end_cmd_f

    n_ch = c->n_token_chars;

    if (!n_ch)                          // If token contains no characters
    {
        return (FGC_NO_SYMBOL);                 // Report NO_SYMBOL
    }

    buf  = c->token;                    // Set up character pointer into token buffer
    ch   = *(buf++);                    // Extract first character
    val  = 0;                           // Clear resulting unsigned value

    while (ch == '0')                   // While character is a zero
    {
        ch = *(buf++);                          // Extract next character
        n_ch--;                                 // keep track of chars remaining
    }

    if (ch == 'X')                      // If hex value specified
    {
        if (n_ch <= 1)                          // If no characters follow the 'X'
        {
            return (FGC_BAD_INTEGER);                   // Report BAD INTEGER
        }

        do
        {
            ch = *(buf++);                              // Extract next character
            n_ch--;                                     // keep track of chars remaining
        }
        while (ch == '0');                      // While character is a zero

        if (n_ch > 8)                           // If token still has more than 8 characters
        {
            return (FGC_BAD_INTEGER);                   // Report BAD INTEGER
        }

        while (ch)                              // Loop until token buffer is empty;
        {
            digit = ParsScanHexDigit(ch);

            if (digit == 0x10)                  // If character is not a valid hex digit
            {
                return (FGC_BAD_INTEGER);                       // Report BAD INTEGER
            }

            val = val * 16 + digit;                     // Accumulate the new digit
            ch  = *(buf++);                             // Get next character from token buffer
        }
    }
    else                                // else for Decimal number
    {
        if (n_ch > 10 ||                        // If integer has more than 10 more characters
            (n_ch == 10 && ch > '3'))             // or if it has exactly 10, but first digit is > '3'
        {
            return (FGC_BAD_INTEGER);                   // Report BAD INTEGER
        }

        while (ch)                      // Loop until token buffer is empty;
        {
            if (ch < '0' || ch > '9')           // If character isn't a valid decimal digit
            {
                return (FGC_BAD_INTEGER);               // Report BAD INTEGER
            }

            digit = ch - '0';                   // Calculate value of new digit
            val   = val * 10 + digit;           // Accumulate the new digit
            ch    = *(buf++);                   // Get next character from token buffer
        }
    }

    *value = val;                       // Return value
    return (0);
}


INT16U ParsScanStd(struct cmd * c, struct prop * p, INT16U par_type, void ** value)
{
    INT16U              errnum;
    INT32U       *      val;
    struct pars_buf  *  pars_buf; 
    uint16_t            pars_idx_token;   


    pars_buf = c->pars_buf;

    if (c->pars_idx >= pars_buf->n_pars)                // If results buffer is empty
    {
        do
        {
            if (pars_buf->out_idx >= pars_buf->pkt[c->pkt_buf_idx].n_chars) // If command packet is empty
            {
                if (c->end_cmd_f ||                                     // If command already empty
                    Test(pars_buf->pkt[c->pkt_buf_idx].header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT))
                {
                    c->last_par_f = TRUE;                                       // Report NO_SYMBOL
                    return (FGC_NO_SYMBOL);
                }

                errnum = CmdNextCmdPkt(c);

                if (errnum)                             // If next packet restarts the cmd
                {
                    return (errnum);                                            // report error
                }
            }

            if (c->evt_log_state != EVT_LOG_VALUE_FULL)         // If log record isn't full
            {
                LogEvtSaveValue(c);                                     // Record value in record
            }

            pars_buf->pkt_buf_idx    = c->pkt_buf_idx;                 // Request DSP to parse the packet
            pars_buf->limits.integer = *(struct int_limits *)p->range; // Prepare limits (int or uint or float)
            pars_buf->type           = par_type;                       // This triggers PropParsePkt() in DSP (on FGC2)

            if (c->f == fcm.f)
            {
                ParsPkt(pars_buf, &fcm_pars_buf);
            }
            else
            {
                ParsPkt(pars_buf, &tcm_pars_buf);
            }

            if (pars_buf->errnum)                               // If DSP reported parsing error
            {
                c->device_par_err_idx = pars_buf->par_err_idx;          // Offset error index
                return (pars_buf->errnum);                              // Return the error
            }
        }
        while (!pars_buf->n_pars);

        c->pars_idx = 0;                                        // Reset parameter index

        if (Test(pars_buf->pkt[c->pkt_buf_idx].header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT)) // If last pkt
        {
            c->end_cmd_f = TRUE;                                                // Set end of command flag
        }
    }

    // Save current pars index for later, to retrieve whether a particular token was empty or not
    pars_idx_token = c->pars_idx;

    val = &pars_buf->results.intu[c->pars_idx++];       // Get address of next parameter in results buffer

    if (c->pars_idx >= pars_buf->n_pars)                // If that was the last parameter of the packet
    {
        c->last_par_f = c->end_cmd_f;                           // Set last_par_f to end_cmd_f
    }
    

    /*
        if(dbidx < DBLEN)
        {
            db[dbidx].pars_idx    = c->pars_idx;
            db[dbidx].n_pars      = pars_buf->n_pars;
            db[dbidx].out_idx     = pars_buf->out_idx;
            db[dbidx].n_chars     = pars_buf->pkt[c->pkt_buf_idx].n_chars;
            db[dbidx].pkt_buf_idx = c->pkt_buf_idx;
            db[dbidx].n_carryover_chars = pars_buf->pkt_buf_idx;
            db[dbidx].last_par_f  = c->last_par_f;
            db[dbidx].header      = pars_buf->pkt[c->pkt_buf_idx].header;
            db[dbidx].end_cmd_f   = c->end_cmd_f;
            dbidx++;
        }
    */
   
    if ( (par_type == PKT_FLOAT) && pars_buf->empty_tokens[pars_idx_token] == true )
    {
        *val = PAR_NO_FLOAT;
        return FGC_NO_SYMBOL;
    }

    if ( (par_type != PKT_FLOAT) && pars_buf->empty_tokens[pars_idx_token] == true )
    {
        return FGC_NO_SYMBOL;
    }
    

    // Set pointer to the value according to type size
    *value = (void *)((INT32U)(val + 1) - prop_type_size[p->type]);
    return (0);
}


INT16U ParsScanToken(struct cmd * c, const char * delim)
{
    INT16U              errnum;         // Error number
    char                ch;             // Working character from command buffer
    char        *       buf;            // Pointer into token buffer
    INT16U              n_ch;           // Number of characters transfered
    char                ldelim[8];      // Local copy of far string delimiter

    errnum = CmdNextCh(c, &ch);

    if (errnum != 0)    // If next character aborts the command
    {
        return (errnum);                        // Return error
    }

    if (ch == ' ')                      // If leading space - skip and take another character
    {
        errnum = CmdNextCh(c, &ch);

        if (errnum != 0)                // If next character aborts the command
        {
            return (errnum);                            // Return error
        }
    }

    ldelim[2] = '\a';                   // Pre pad local delimiter with "impossible" \a character
    ldelim[3] = '\a';
    ldelim[4] = '\a';
    ldelim[5] = '\a';

    strcpy(ldelim, delim);
    ldelim[strlen(ldelim)] = '\a';  // Replace nul from string copy with \a

    buf  = c->token;                    // Set up character pointer into destination token buffer
    n_ch = 0;

    while (n_ch < MAX_CMD_TOKEN_LEN)    // While token buffer isn't full
    {
        LogEvtSaveCh(c, ch);            // Log character

        if ((!ch && ldelim[0] == 'Y') || // If character is nul and nul delimiter is allowed , or
            ch == ldelim[1] ||           // character matches any of the other five possible
            ch == ldelim[2] ||           // delimiter characters
            ch == ldelim[3] ||
            ch == ldelim[4] ||
            ch == ldelim[5])
        {
            *buf             = '\0';            // Nul terminate token
            c->token_delim   = ch;              // Report the delimiter character
            c->n_token_chars = n_ch;            // Report token length
            c->token_idx     = 0;               // Reset token index to start of token buffer
            return (0);                         // Return success
        }

        if (!ch)                                // Buffer empty unexpectedly
        {
            return (FGC_NO_DELIMITER);                  // Report NO DELIMITER
        }

        if (ch == ' ')                          // Character is a space (and space is not a valid delimiter)
        {
            break;                                      // Break to return SYNTAX_ERROR
        }

        *(buf++) = ch;                          // Put character into token buffer
        n_ch++;                                 // Increment token length counter

        errnum = CmdNextCh(c, &ch);

        if (errnum != 0)                // If next character aborts the command
        {
            return (errnum);                            // Return error
        }
    }

    return (FGC_SET_BUF_FULL);                  // Buffer overrun - report SYNTAX ERROR
}


INT16U ParsScanSymList(struct cmd * c, const char * delim, const struct sym_lst * sym_lst,
                       const struct sym_lst ** sym_const)
{
    INT16U      errnum;                 // Return status

    errnum = ParsScanSymListAndNone(c, delim, sym_lst, sym_const);

    if (errnum == FGC_GENERIC_NONE_SYMBOL)      // This error message should stay internal to the FGC, so
        // replace it by a more generic "BAD PARAMETER" message.
    {
        errnum = FGC_BAD_PARAMETER;
    }

    return errnum;
}


static INT16U ParsScanSymListAndNone(struct cmd * c, const char * delim, const struct sym_lst * sym_lst,
                                     const struct sym_lst ** sym_const)
{
    INT16U      errnum;             // Return status
    INT16U      sl_idx;             // Symbol index in symbol list (FGC_NOT_SETTABLE bit may be set)
    INT16U      sym_idx;            // Identified symbol index
    BOOLEAN not_settable_f;         // Flag indicating that the symbol was found, but cannot be (FGC_NOT_SETTABLE)

    errnum = PropIdentifySymbol(c, delim, ST_CONST, &sym_idx);  // Identify Constant Symbol

    c->last_par_f = c->end_cmd_f;       // Set last_par_f to end_cmd_f

    if (errnum != 0)                            // If Symbol not identified
    {
        return (errnum);                        // Report error status
    }

    not_settable_f = FALSE;

    // Scan supplied sym_lst for a match
    while ((!not_settable_f) && (sl_idx = sym_lst->sym_idx) && (sl_idx != sym_idx))
    {
        // Non-settable symbols will NEVER
        // match because the FGC_NOT_SETTABLE
        // bit is set in sym_idx in the sym_lst
        not_settable_f = (sl_idx == (sym_idx | FGC_NOT_SETTABLE));

        sym_lst++;
    }

    // If the symbol was found but ORed with mask FGC_NOT_SETTABLE, then return the "symbol is not settable" error.

    if (not_settable_f == TRUE)
    {
        return (FGC_SYMBOL_IS_NOT_SETTABLE);
    }

    // Else, if no match found (sl_idx == NULL)

    else if (!sl_idx)
    {
        // Generic NONE symbol. The NONE symbol is recognised by default for all bit mask properties. Note that if
        // STC_NONE or (STC_NONE|FGC_NOT_SETTABLE) is present in the sym_lst, this is not the generic NONE symbol but
        // a symbol explicitly defined in the property symbol list.

        if (sym_idx == STC_NONE)
        {
            return (FGC_GENERIC_NONE_SYMBOL);   // If the property is a bit mask, that error gets caught by the FGC SW.
        }

        // In any other case, report BAD PARAMETER

        else
        {
            return (FGC_BAD_PARAMETER);
        }
    }

    *sym_const = sym_lst;               // Pass pointer to sym_lst record to calling function
    return (0);                         // Return success
}


INT16U ParsSet(struct cmd * c, struct prop * p)
{
    INT16U      errnum;
    prop_size_t el_idx;
    INT32U      el_size;
    prop_size_t n_els_old;
    INT32U      max_pars;
    BOOLEAN     set_f;
    INT8U   *   cur_val;                        // Pointer to current element value
    void    *   new_val;                        // Pointer to new element value

    /*--- Prepare array range ---*/

    el_idx = c->from;

    // Get first property block into cache and n_elements

    errnum = PropValueGet(c, p, NULL, el_idx, &cur_val);

    if (errnum)
    {
        return (errnum);
    }

    n_els_old = c->n_elements;

    max_pars  = c->to - el_idx + 1;             // Calculate number of elements in array range
    el_size   = prop_type_size[p->type];        // Get element size for this property
    set_f     = FALSE;                          // Clear parameter set flag

    if (p->type == PT_CHAR &&                   // If CHAR type and
        el_idx > n_els_old)                      // array start is beyond the end of the existing data
    {
        return (FGC_BAD_ARRAY_IDX);                     // Report bad array index
    }

    // Parse command packets

    while (!c->last_par_f && c->n_pars < max_pars)
    {
        errnum  = PropValueGet(c, p, &set_f, el_idx, &cur_val); // Get existing value from prop (as default)

        if (errnum)
        {
            return (errnum);
        }

        errnum  = ParsValueGet(c, p, &new_val);                 // Get new value from command packet

        switch (errnum)
        {
            case 0:

                if (memcmp(cur_val, new_val, el_size))
                {
                    memcpy(cur_val, new_val, el_size);

                    c->changed_f = TRUE;
                    set_f        = TRUE;
                }

                c->n_pars++;
                el_idx++;
                break;

            case FGC_NO_SYMBOL:             // TOKEN is empty (i.e. blank between two commas or after last comma)

                if (p->type == PT_CHAR)             // If CHAR type
                {
                    if (c->n_pars)                          // If at least one character was provided
                    {
                        return (FGC_NO_SYMBOL);                     // Report NO_SYMBOL
                    }
                }

                if (el_idx || !c->last_par_f)       // If not first element or not last parameter
                {
                    c->n_pars++;                            // Don't change existing value for this element
                    el_idx++;
                }

                break;

            default:

                c->device_par_err_idx += c->n_pars;
                return (errnum);
        }
    }

    /*--- Check number of parameters set and adjust property length ---*/

    if (!c->last_par_f && c->n_pars >= max_pars)
    {
        c->device_par_err_idx += c->n_pars;

        return (FGC_BAD_PARAMETER);
    }

    if (el_idx < n_els_old                            // If length has reduced and
        && (Test(p->flags, PF_DONT_SHRINK)))            // (property cannot shrink or
            //|| (c->n_pars >= max_pars)))              //  complete range supplied)
    {        
        el_idx = n_els_old;                           // Keep original data length
    }

    if (el_idx != n_els_old)
    {
        c->changed_f = TRUE;
    }   

    return (PropBlkSet(c, el_idx, set_f, FGC_FIELDBUS_FLAGS_LAST_PKT)); // Write back zeroed value(s)

}

#if (FGC_CLASS_ID == 62)
static bool IsMacAddress(const char * const str)
{
    unsigned i;
    for(i = 0; i < strlen(str); i++)
    {
        if(str[i] == '-')
        {
            return true;
        }
    }

    return false;
}



static uint16_t ValidateMacAddress(const char * const str)
{
    const size_t length = strlen(str);
          size_t i;

    if(length != 17)
    {
        return FGC_BAD_ADDRESS;
    }

    for(i = 0; i < length; i++)
    {
        if((i + 1) % 3 == 0)
        {
            if(str[i] != '-')
            {
                return FGC_BAD_ADDRESS;
            }
        }
        else if((str[i] < '0' || str[i] > '9') &&
                (str[i] < 'A' || str[i] > 'F') &&
                (str[i] < 'a' || str[i] > 'f'))
        {
            return FGC_BAD_ADDRESS;
        }
    }

    return 0;
}



static uint16_t ParsScanString(struct cmd * c, char ** value, const char * const delimiters)
{
    INT16U      errnum;

    if (c->token_idx >= c->n_token_chars)
    {
        errnum = ParsScanToken(c, delimiters);

        if(errnum != 0)
        {
            return errnum;
        }
    }

    c->token_idx = c->n_token_chars;

    // Return address of the character in the token buffer

    *value = &c->token[0];

    // Set last_par_f to end_cmd_f

    c->last_par_f = c->end_cmd_f;

    // If new token is empty, report no symbol

    if(c->n_token_chars == 0)
    {
        return FGC_NO_SYMBOL;
    }

    return (0);
}
#endif



uint16_t ParsScanDevName(struct cmd * c, struct prop * p, char ** value)
{
#if (FGC_CLASS_ID == 62)
    // Use the pars_buf results area for the value

    char *   result = (char *)&c->pars_buf->results;

    uint16_t errnum;
    char *   token;
    char *   head;

    *value = result;
    head   = result;

    // If there's a symlist linked to the property, we expect
    // the string to be in <symlist_entry>@<device_name> format.
    // Otherwise, the string should contain only a device name.

    if(Test(p->flags, PF_SYM_LST))
    {
        const struct sym_lst * sym_const;

        // Look for symlist entry followed by '@'

        errnum = ParsScanSymList(c, "Y\\@,", (struct sym_lst *)p->range, &sym_const);

        c->token_idx = c->n_token_chars;
        token        = c->token;

        // Mask 'buffer full' error coming from the tokeniser

        if(errnum == FGC_SET_BUF_FULL)
        {
            return FGC_UNKNOWN_SYM;
        }

        if(errnum != 0)
        {
            return errnum;
        }

        if(c->token_delim == '@')
        {
            if(strlen(token) > ST_MAX_SYM_LEN)
            {
                return FGC_UNKNOWN_SYM;
            }

            strcpy(head, token);

            head   += c->n_token_chars;
            *head++ = '@';
        }
        else
        {
            if(memcmp(token, "NONE", 4) == 0)
            {
                strcpy(head, token);

                return 0;
            };
        }
    }

    errnum = ParsScanString(c, &token, "Y,");

    // Mask 'no symbol' error coming from the tokeniser

    if(errnum == FGC_NO_SYMBOL && Test(p->flags, PF_SYM_LST))
    {
        return FGC_UNKNOWN_DEV;
    }

    // Mask 'buffer full' error coming from the tokeniser

    if(errnum == FGC_SET_BUF_FULL)
    {
        return FGC_DEV_NAME_TOO_LONG;
    }

    if(errnum != 0)
    {
        return errnum;
    }

    if(strlen(token) > FGC_MAX_DEV_LEN)
    {
        return FGC_DEV_NAME_TOO_LONG;
    }

    strcpy(head, token);

    if(memcmp(token, "NONE", 4) == 0)
    {
        return 0;
    }

    if(IsMacAddress(token))
    {
        errnum = ValidateMacAddress(token);

        if(errnum != 0)
        {
            return errnum;
        }

        return 0;
    }
    else if(nameDbGetIndex(token) >= 0)
    {
        return 0;
    }
#endif
    return FGC_UNKNOWN_DEV;
}




INT16U ParsValueGet(struct cmd * c, struct prop * p, void ** value)
{
    INT16U              par_type;

    if(p->type == PT_DEV_NAME)
    {
        return ParsScanDevName(c, p, (char **)value);
    }

    switch (p->get_func_idx)
    {
        case GET_ABSTIME:                           // AbsTime

            return (ParsScanAbsTime(c, (struct abs_time_us **)value));

        case GET_CHAR:                              // Char

            return (ParsScanChar(c, (char **)value));
#if (FGC_CLASS_ID != 62)
        case GET_POINT:
            return (ParsScanPoint(c, p, (struct point **)value));
#endif
        case GET_FLOAT:                             // Float
        case GET_RAWDSPPARAMETERFLOAT:

            par_type = PKT_FLOAT;
            break;

        default:                                    // Integer

            if (Test(p->flags, PF_SYM_LST))                 // If symlist
            {
                return (ParsValueGetSymList(c, p, value));          // Return symlist value
            }

            par_type = (p->type == PT_INT32U ? PKT_INTU : PKT_INT);

            break;
    }

    return (ParsScanStd(c, p, par_type, value)); // Standard type (Integer, Float, RelTime)
}


// Static function definition

static INT16U ParsScanAbsTime(struct cmd * c, struct abs_time_us ** value)
{
    INT16U              i;
    INT16U              errnum;
    INT32U              val;
    INT16U              expon;
    INT16U              n_chars;
    char        *       token;                          // Pointer to character in token
    char        *       c1;
    char        *       c2;
    struct abs_time_us * result;

    result = (struct abs_time_us *)&c->pars_buf->results;       // Use the pars_buf results area for the value
    *value = result;                                    // Return the address of the value

    errnum = ParsScanToken(c, "Y,");

    if (errnum)                 // Get next token, delimiters: ',' and '\0'
    {
        return (errnum);
    }

    c->last_par_f = c->end_cmd_f;                       // Set last_par_f to end_cmd_f

    n_chars = c->n_token_chars;

    if (!n_chars)                       // If no token
    {
        *result = mst.time;                                     // Set result to time now
        return (0);                                             // Return time now
    }

    token = (char *) &c->token;

    if (token[0] < '0' || token[0] > '9')               // If first character isn't a digit
    {
        return (FGC_INVALID_TIME);                              // Report bad integer
    }

    OSSemPend(pcm.sem);                                 // Reserve access to the PCM structure

    if (n_chars > 6 && token[1] == '.' && token[n_chars - 4] == 'E')
    {
        if (token[n_chars - 3] != '+')
        {
            OSSemPost(pcm.sem);
            return (FGC_INVALID_TIME);
        }

        token[n_chars - 4] = '\0';
        pcm.end_cmd_f = FALSE;
        pcm.prop_buf  = (struct prop_buf *)&token[n_chars - 2];

        if (ParsScanInteger(&pcm, "Y", &val) || val > 9 || ((INT16U)val + 6) > n_chars) // delim = '\0'
        {
            OSSemPost(pcm.sem);
            return (FGC_INVALID_TIME);
        }

        expon = (INT16U)val;

        c1 = &token[2];
        i  = n_chars - 6;

        while (i--)                             // Check all the digits are valid
        {
            if (*c1 < '0' || *c1 > '9')
            {
                OSSemPost(pcm.sem);
                return (FGC_INVALID_TIME);              // Report bad time if any digit is invalid
            }

            c1++;
        }

        c1 = &token[1];
        c2 = &token[2];
        i  = expon;

        while (i--)                             // Shift digits to move decimal point
        {
            *(c1++) = *(c2++);
        }

        *c1 = '.';                              // Add new decimal point

        if ((n_chars - expon - 6) > 6)          // If more than 6 digits after decimal
        {
            token[expon + 8] = '\0';                    // Nul terminate after sixth digit
        }
    }


    pcm.end_cmd_f = FALSE;
    pcm.prop_buf  = (struct prop_buf *)&token[0];

    errnum = ParsScanInteger(&pcm, "Y.", &val);

    if (errnum != 0)    // Delimiters: '.'  '\0'
    {
        OSSemPost(pcm.sem);
        return (errnum);
    }

    result->unix_time = val;                            // Prepare time now (to the second) as default
    result->us_time   = 0;

    if (pcm.token_delim == '.')
    {
        switch ((errnum = ParsScanInteger(&pcm, "Y", &val)))    // Delimiters: '\0'
        {
            case 0:

                if (val > 999999)                   // If too may characters found
                {
                    OSSemPost(pcm.sem);
                    return (FGC_INVALID_TIME);
                }

                if (val < 10)
                {
                    result->us_time = val * 100000;
                }
                else if (val < 100)
                {
                    result->us_time = val * 10000;
                }
                else if (val < 1000)
                {
                    result->us_time = val * 1000;
                }
                else if (val < 10000)
                {
                    result->us_time = val * 100;
                }
                else if (val < 100000)
                {
                    result->us_time = val * 10;
                }
                else
                {
                    result->us_time = val;
                }

                break;

            case FGC_NO_SYMBOL:                             // If no ms specified then return seconds only

                break;

            default:                                        // Report errors

                OSSemPost(pcm.sem);
                return (errnum);
        }
    }

    if (!result->unix_time)                             // If time is zero seconds (any milliseconds)
    {
        *result = mst.time;                                     // Set result to time now
    }

    OSSemPost(pcm.sem);
    return (0);
}


static INT16U ParsScanChar(struct cmd * c, char ** value)
{
    INT16U      errnum;

    if (c->token_idx >= c->n_token_chars)       // If token buffer is empty
    {
        errnum = ParsScanToken(c, "Y,");

        if (errnum)             // Delimiters: ',' '\0'
        {
            return (errnum);                                    // Return error
        }
    }

    *value = &c->token[c->token_idx++];         // Return address of the character in the token buffer

    if (**value == ':')                         // If colon
    {
        return (FGC_SYNTAX_ERROR);                      // Report error
    }

    if (c->token_idx >= c->n_token_chars)       // If token now empty
    {
        c->last_par_f = c->end_cmd_f;                   // Set last_par_f to end_cmd_f
    }

    if (!c->n_token_chars)                      // If new token is empty
    {
        return (FGC_NO_SYMBOL);                         // Report NO_SYMBOL
    }

    return (0);
}


#if (FGC_CLASS_ID != 62)
static INT16U ParsScanPoint(struct cmd * c, struct prop * p, struct point ** value)
{
    struct     point  * result;
    char              * token;
    uint16_t            errnum;
    char                inv_par;

    // Use the pars_buf results area for the value

    result = (struct point *)&c->pars_buf->results;

    // Return the address of the value

    *value = result;

    errnum = ParsScanToken(c, "Y,");

    if (errnum)
    {
        return (errnum);
    }

    // Set last_par_f to end_cmd_f

    c->last_par_f = c->end_cmd_f;

    token = (char *) &c->token;

    // Parse the reference and time values

    if (sscanf(token, "%f|%f%c", &result->time, &result->ref, &inv_par) != 2)
    {
        return (FGC_BAD_POINT);
    }

    union pars_limits * range = (union pars_limits *)p->range;

    if (   result->time < 0
        || result->time > 4.0E+5
        || isnan(result->time) == true)
    {
        return (FGC_INVALID_TIME);
    }
    
    if (   result->ref  < range->fp.min
        || result->ref  > range->fp.max
        || isnan(result->ref) == true)
    {
        return (FGC_OUT_OF_LIMITS);
    }

    return (0);
}
#endif


static INT16U ParsValueGetSymList(struct cmd * c, struct prop * p, void ** value)
{
    BOOLEAN             bitmask_f;
    INT16U              errnum;
    INT16U       *      results;
    const struct sym_lst * sym_const;

    bitmask_f  = Test(p->flags, PF_BIT_MASK);
    results    = (INT16U *)&c->pars_buf->results;       // Use the pars_buf results area for the value
    results[0] = 0;
    results[1] = 0;
    *value     = (INT8U *)&c->pars_buf->results + (4 - prop_type_size[p->type]);

    do
    {
        errnum = ParsScanSymListAndNone(c, (bitmask_f ? "Y|," : "Y,"), p->range, &sym_const);

        // Generic "NONE" symbol for bit masks.
        // If the property is a bit mask, and the symbol NONE is found in the command but that symbol is not explicitly
        // mentioned in the symbol list for that property, then the default interpretation is to reset the bit mask.

        if (bitmask_f && errnum == FGC_GENERIC_NONE_SYMBOL)
        {
            results[1] = 0;     // Reset the property value
            errnum = 0;         // Ignore the error
        }
        else if (errnum == FGC_GENERIC_NONE_SYMBOL)
        {
            errnum = FGC_BAD_PARAMETER;         // Replace internal error GENERIC NONE SYMBOL by user error BAD PARAMETER
        }
        else if (!errnum)
        {
            results[1] |= sym_const->value;
        }
    }
    while (!errnum && bitmask_f && c->token_delim == '|');

    return (errnum);
}


static inline INT16U ParsScanHexDigit(char ch)
{
    return (ch >= '0' && ch <= '9' ? ch - '0' : (ch >= 'A' && ch <= 'F' ? ch - ('A' - 10) : 0x10));
}

// EOF

