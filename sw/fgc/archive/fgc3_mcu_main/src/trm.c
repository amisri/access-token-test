/*---------------------------------------------------------------------------------------------------------*\
  File:     trm.c

  Purpose:  FGC Software - TerminalInterface Functions.

  Notes:    This file contains the functions for communications via the MCU's terminal interface
        (normally used for a ANSI/VT100 type terminal) or via a remote terminal connection over
        the fieldbus.  It includes the Trm task function TrmTsk().

        The number of terminal output buffers is defined by TRM_N_BUFS.  The circular queue for passing
        pointers to the terminal output buffers has a length of TRM_N_BUFS+1 so that it can never fill
        up.  This avoids the need for code to detect if the queue is full.

        The terminal can be in one of three modes (terminal.mode): Editor, Direct or Diagnostic.
        When in Editor mode, a edit state machine (terminal_state.edit_state) controls the interpretation
        of cursor and function keys from the ANSI/VT100 keyboard.
\*---------------------------------------------------------------------------------------------------------*/

#define TRM_GLOBALS             // define terminal global variable
#define FGC_GLOBALS             // Force global variable definition

#include <trm.h>

#include <iodefines.h>
#include <stdio.h>
#include <string.h>

#include <class.h>              // Include class dependent definitions
#include <cmd.h>
#include <dev.h>                // for dev global variable
#include <diag.h>               // for diag global variable
#include <fbs_class.h>          // for ST_LATCHED
#include <fbs.h>                // for fbs global variable
#include <fgc_errs.h>           // for FGC_PKT_BUF_NOT_AVL, fgc_errmsg[]
#include <init_class.h>         // for InitClassPreInts(), InitClassPostInts()
#include <init.h>               // for EnableMcuSerialInterface_Terminal(), EnableMcuTimersAndInterrupts(), InitSystem(), InitSpyMpx(), InitPropParentLinks()
#include <initterm.h>           // for InitTerm()
#include <log.h>                // for LogInit()
#include <macros.h>             // for Test(), Set(), Clr()
#include <pars_service.h>
#include <fgc/fgc_db.h>
#include <queue_fgc.h>          // struct queue
#include <rtd.h>                // for rtd global variable
#include <serial_stream.h>      // for SerialStreamInternal_WriteChar(), SerialStreamInternal_FlushBuffer()
#include <shared_memory.h>      // for shared_mem global variable
#include <sleep.h>
#include <start.h>              // for StartRunningDsp(), WaitForDspStarted()
#include <state_manager.h>      // for StaInit()
#include <task_trace.h>
#include <term.h>               // for TERM_INIT
#include <tsk.h>
#include <memmap_mcu.h>

// Static function prototypes (this file only)

static  INT16U  SciEdit0(char);
static  INT16U  SciEdit1(char);
static  INT16U  SciEdit2(char);
static  INT16U  SciEdit3(char);
static  INT16U  SciEdit4(char);
static  void    SciTerm(INT16U);
static  void    SciDirect(char);
static  void    SciInsertChar(char);
static  void    SciNewline(void);
static  void    SciCursorLeft(void);
static  void    SciCursorRight(void);
static  void    SciStartOfLine(void);
static  void    SciEndOfLine(void);
static  void    SciDeleteLine(void);
static  void    SciDeleteLeft(void);
static  void    SciDeleteRight(void);
static  void    SciShiftRemains(void);
static  void    SciRepeatLine(void);
static  void    SciPreviousLine(void);
static  void    SciNextLine(void);
static  void    SciRecallLine(INT16U);
static  void    SciAddCmd(char *);
static  void    SciDefaultPrefix(char *);
static  void    SciRingBell(void);
static  INT16U  SciAddCh(char ch);
static  void    SciSendPkt(void);

/*---------------------------------------------------------------------------------------------------------*/
void TrmTsk(void * unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the terminal input task.  It gets characters received from the serial input or from the
  remote terminal channel.  The communication may either come from a ANSI/VT100 terminal, or a client application.
  Editor mode is provided to support human interaction via a terminal, while Direct mode and Diagnostic mode
  are provided for client applications.

  This function never exits, so "local" variables are static to save stack space.
\*---------------------------------------------------------------------------------------------------------*/
{
    static char     ch;
    static INT16U(*sci_task_edit_func[])(char) =        // SCI edit state functions
    {
        SciEdit0,                   // State 0 : Normal characters
        SciEdit1,                   // State 1 : ESC pressed
        SciEdit2,                   // State 2 : Cursor keys or all function keys
        SciEdit3,                   // State 3 : All function keys except PF1-PF4
        SciEdit4,                   // State 4 : PF1-PF4 keys
    };

    // initialise FILE structures for 'tcm' stream
    tcm.f = fopen("tcm", "wb");

    if (setvbuf(tcm.f, NULL, _IONBF, 0) != 0)  // not buffered, buffer size 0
    {
        // 0   = ok
        // EOF = error
    }

    //===================================================================================================
    // FGC2 set the hardware again, even if was already done by the Boot program because under debugging
    // they can start running the Main on SRAM by-passing the Boot

    // on OSTskStart() all the task are started and suspended
    // MstTsk -> FbsTsk -> StaTsk -> PubTsk -> CmdTsk(fcm) -> CmdTsk(tcm) -> DlsTsk -> RtdTsk -> TrmTsk
    // so in this point we put the interrupts and peripherals to work

    EnableMcuSerialInterface_Terminal();        // Prepare QSM interface and tcm.file structure
    StaInit();                                  // Initialise state machines
    LogInit();                                  // Prepare logging (leaves Page23=BB23)
    InitClassPreInts();                         // Init class related structures (pre-interrupts)

    StartRunningDsp();                  // Start DSP
    WaitForDspStarted();                // Wait for DSP software to start

    // InitClassPostInts calls NvsInit which will set DSP properties, in that case the MCU tick interrupt is needed
    EnableMcuTimersAndInterrupts();     // Especially the 1 millisecond tick
    sleepUs(2000);                       // Wait for 2ms to all interrupts to start

    InitClassPostInts();        // Init class related structures (post-interrupts)

    InitSystem();               // Initialise system related structures

    // Note: Setting the SPY.MPX property will complete the initialisations on the DSP side and
    //       the DSP_INTIALISED flag will be set

    InitSpyMpx(&tcm);           // Initialise Spy channel multiplexer

    DiagDimHashInit();          // Initialise the Hash sctruc with the Dim of the current system
    DiagDimDbInit();            // Initialise Dim Database

    InitPropertyTree();         // Initialise the property tree (parent link, PPM flag, etc.)
    //===================================================================================================

    terminal_state.pkt_buf_idx = 1;     // Initialise packet buffer index
    SciTerm(TRM_DIRECT_MODE);           // Start SCI in direct mode

    // Task loop

    for (;;)                    // Main loop - process each character received
    {
        TskTraceReset();

        ch = (char)(uintptr_t) OSMsgPend(terminal.msgq);         // Wait for character from IsrMst or MstTsk or FbsTsk

        terminal_state.edit_mode_timer_min = 0; // Reset editor mode timeout timer

        TskTraceInc();

        switch (ch)                     // Check character for mode change
        {
            case 0x19:                  // Ctrl-Y : Diag Mode

                if (terminal.mode != TRM_DIAG_MODE)  // If not in diag mode
                {
                    SciTerm(TRM_DIAG_MODE);     // Switch to diag mode
                }

                continue;

            case '!':               // '!'    : Direct Mode
            case 0x1A:              // Ctrl-Z : Direct Mode

                if (terminal.mode != TRM_DIRECT_MODE) // If not in direct mode
                {
                    SciTerm(TRM_DIRECT_MODE);   // Switch to direct mode
                }

                break;

            case 0x1B:              // ESC : Editor Mode

                if (terminal.mode != TRM_EDITOR_MODE) // If not in editor mode
                {
                    rtd.enabled = TRUE;
                    SciTerm(TRM_EDITOR_MODE);   // Switch to editor mode
                    continue;
                }

                break;
        }

        TskTraceInc();

        // Process new character according to mode (if Diag mode, ignore character)

        switch (terminal.mode)
        {
            case TRM_DIRECT_MODE:

                SciDirect(ch);
                break;

            case TRM_EDITOR_MODE:

                terminal_state.edit_state = sci_task_edit_func[terminal_state.edit_state](ch);
                break;
        }

        TskTraceInc();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciTerm(INT16U sci_mode)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set the SCI mode according to the parameter.  The mode can be Editor, Diagnostic or
  Direct.
\*---------------------------------------------------------------------------------------------------------*/
{
    terminal.mode = TRM_DIRECT_MODE;     // Switch to Direct mode to stop RTD/Diag output
    SciDirect('!');         // Cancel any command in progress
    terminal_state.cmd_state = 0;       // Reset command state

    switch (sci_mode)
    {
        case TRM_DIRECT_MODE:           // Direct mode

            tcm.errnum = 0;             // Clear SCI command receiving state
            break;

        case TRM_DIAG_MODE:             // Diag mode

            diag.term_byte_idx    = DIAG_TERM_LEN + 1;   // Suppress diag transmission for 1st cycle
            diag.term_buf_counter = FGC_DIAG_PERIOD;     // Suppress diag data accumulation for 1st cycle
            break;

        case TRM_EDITOR_MODE:           // Editor mode

            InitTerm(tcm.f);                // Reset terminal
            fputs(TERM_INIT, tcm.f);            // CLS, enable line wrap, ring bell

            if (rtd.enabled)
            {
                RtdInit(tcm.f);                     // Initialise real-time display lines
            }

            fprintf(tcm.f,                  // Write heading
                    "PLD VERSION: %lu (%04x)\n\r"    // PLD version
                    "CODE (BOOT): %lu\n\r"           // Boot code version
                    "CODE (MP):   %lu\n\r"           // Main programs version
                    "NAME:        %s\n\r"            // Device name
                    "SYSTEM:      %s\n\r",           // System label
                    shared_mem.codes_version[SHORT_LIST_CODE_PLD],
                    MID_PLDVER_P,
                    shared_mem.codes_version[SHORT_LIST_CODE_BT],
                    shared_mem.codes_version[SHORT_LIST_CODE_MP_DSP],
                    (char *)dev.name,
                    SysDbLabel(dev.sys.sys_idx));


            if (!FbsIsStandalone())
            {
                fprintf(tcm.f,                  // Write gateway name and
                        "FBS_ID:      %s:%u\n\r",   // Fieldbus address
                        (char *)fbs.host.name,
                        fbs.id);
            }

            fputs(";\v", tcm.f);

            terminal_state.line_idx  = 0;               // Reset line buffer cursor index
            terminal_state.line_end  = 0;               // Reset line buffer end index
            terminal_state.edit_state = 0;              // Reset edit state
            break;
    }

    terminal.mode = sci_mode;            // Set SCI mode
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciDirect(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when in Direct or Editor mode to prepare command packets for parsing by the TcmTsk.
  A command must have the syntax: !S PROPERTY VALUES\n or !G PROPERTY GETOPTIONS\n.  All characters before
  the ! are ignored.  If ! is repeated, all characters in the current packet are discarded.  Commands may
  also be terminated with ; or \r.  The $, { and } characters are ignored because they are delimiter
  characters.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Process new command character

    switch (ch)
    {
        case '{':                   // Server protocol delimiters
        case '}':
        case '$':

            return;                     // Ignore

        case ' ':                   // Space

            terminal_state.cmd_space_f = TRUE;          // Set space flag
            return;

        case '!':                   // New command

            terminal_state.pkt_header_flags = FGC_FIELDBUS_FLAGS_GET_CMD; // Set packet header to get command by default

            if (terminal_state.cmd_state == TRM_CMD_RECEIVING)  // If command being received
            {
                terminal_state.cmd_state = TRM_CMD_START;           // Change state to CMD START
                tcm.stat      = FGC_CMD_RESTARTED;          // Report new command started
                break;
            }
            else                        // else command not being received
            {
                terminal_state.cmd_state = TRM_CMD_START;           // Change state to CMD START
                return;                     // Silently start new command
            }

        case '\n':                  // newline
        case '\r':                  // carriage return

            ch = ';';                   // Replace with execute character ';' and fall through

        default:                    // All other characters

            if (ch < 0x20)                  // Ignore all control characters
            {
                return;
            }

            if (ch >= 'a' && ch <= 'z')         // Convert lower case to UPPER CASE
            {
                Clr(ch, 0x20);
            }

            switch (terminal_state.cmd_state)               // Switch on direct reception state machine
            {
                default:                    // Not receiving command

                    return;                     // Ignore character

                case TRM_CMD_START:             // ! received - wait for S or G

                    switch (ch)                     // Switch on character
                    {
                        case 'S':                       // 'S' : Set comamnd

                            terminal_state.pkt_header_flags = FGC_FIELDBUS_FLAGS_SET_CMD;    // Prepare pkt header and fall through

                        case 'G':                       // 'G' : Get comamnd

                            terminal_state.cmd_state    = TRM_CMD_DEFINED;          // Advance state
                            terminal_state.cmd_space_f  = FALSE;                // Clear space flag
                            return;

                        default:                        // All other characters

                            tcm.stat = FGC_UNKNOWN_CMD;             // Report unknown command
                            break;
                    }

                    break;

                case TRM_CMD_DEFINED:               // !S or !G received - next char must be a space

                    if (!terminal_state.cmd_space_f)            // If a space was not received
                    {
                        tcm.stat = FGC_UNKNOWN_CMD;             // Report unknown command
                        break;
                    }

                    if ((ch < 'A' || ch > 'Z') &&           // If first char of property is not valid
                        (ch < '0' || ch > '9'))
                    {
                        tcm.stat = FGC_UNKNOWN_SYM;             // Report unknown symbol
                        break;
                    }

                    terminal_state.pkt_in_idx   = 0;                // Reset packet input index
                    terminal_state.cmd_space_f  = FALSE;                // Clear space flag
                    terminal_state.cmd_state = TRM_CMD_RECEIVING;       // Advance state machine
                    Set(terminal_state.pkt_header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT); // Set first packet bit in header

                    fputc((terminal.mode == TRM_DIRECT_MODE ? '$' : '\n') , tcm.f);  // Write cmd header

                case TRM_CMD_RECEIVING:             // Command being received

                    if (ch == ';')                  // End of command
                    {
                        if (terminal_state.pkt_in_idx)                  // If at least one char in pkt
                        {
                            tcm.abort_f = FALSE;                         // Clear SCI Abort flag
                            Set(terminal_state.pkt_header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT);   // Set last pkt bit in header

                            SciSendPkt();                       // Send last packet to TcmTsk

                            while (terminal_state.pkt->header_flags)            // Wait for cmd to finish
                            {
                                OSTskSuspend();
                            }

                            tcm.stat = tcm.errnum;                  // Get errnum from TcmTsk
                        }
                        else                            // else
                        {
                            tcm.stat = FGC_NO_SYMBOL;                   // report NO_SYMBOL error
                        }

                        break;                          // break to report error and end cmd
                    }

                    if (terminal_state.cmd_space_f)             // If space flag is set
                    {
                        tcm.stat = SciAddCh(' ');                          // Add space to packet

                        if (tcm.stat)
                        {
                            break;                          // Report error
                        }

                        terminal_state.cmd_space_f = FALSE;             // Clear space flag
                    }

                    tcm.stat = SciAddCh(ch);                       // Add character to packet

                    if (tcm.stat)
                    {
                        break;                          // Report error
                    }

                    return;
            }

            break;
    }

    /* Report errors if reported */

    if (tcm.stat)                   // If error occured
    {
        fprintf(tcm.f,
                (terminal.mode == TRM_DIRECT_MODE ? "$%u %s\n!\v" : "\n\a!%u %s\n;\v"),
                tcm.stat, fgc_errmsg[tcm.stat]);
        tcm.errnum = 0;
    }
    else                        // else no error
    {
        fputs("\n;\v", tcm.f);                  // Add trailing '\n;'
    }

    terminal_state.cmd_state = 0;               // Reset direct mode state machine
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U SciEdit0(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 0. In this state, the character is analysed
  directly.  If the ESC code (0x1B) is received, the state changes to 1, otherwise the character is
  processed and the state remains 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)                     //-[CHARACTER]--ACTION-----------------------------
    {
        case 0x1B:  return (1);      break;     // [ESC]    Start escape sequence processing

        case 0x24:                              // [$]      Ignore (server protocol delimiter)

        case 0x7B:                              // [{]      Ignore (server protocol delimiter)

        case 0x7D:              break;          // [}]      Ignore (server protocol delimiter)

        case 0x7F:                              // [Delete] Delete left

        case 0x08:  SciDeleteLeft();    break;  // [Backspace]  Delete left

        case 0x04:  SciDeleteRight();   break;  // [CTRL-D] Delete right

        case 0x21:                              // [!]      Delete line

        case 0x15:  SciDeleteLine();    break;  // [CTRL-U] Delete line

        case 0x01:  SciStartOfLine();   break;  // [CTRL-A] Move to start of line

        case 0x05:  SciEndOfLine();     break;  // [CTRL-E] Move to end of line

        case 0x12:  SciRepeatLine();    break;  // [CTRL-R] Repeat line

        case 0x0C: Clr(ST_LATCHED, ST_LATCHED_RESET); break; // [CTRL-L] Reset FGC.ST_LATCHED

        case 0x14:  rtd.enabled = FALSE;        // [CTRL-T] Terminal only (no RTD)
            SciTerm(TRM_EDITOR_MODE); break;

        case 0x3B:                              // [;]      Semicolon - run command

        case 0x0D:  SciNewline();       break;  // [Return] Carriage return - run command

        case 0x0A:              break;          // [LF]     Linefeed - ignore

        default:    SciInsertChar(ch);  break;  // [others] Insert character
    }

    return (0);                 // Continue with edit state 0
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U SciEdit1(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 1. The previous character was [ESC].
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)                 // Switch according to next code
    {
        case 0x1B:              // [ESC]    Restart editor
            rtd.enabled = TRUE;
            SciTerm(TRM_EDITOR_MODE);
            return (0);

        case 0x5B:              // [Cursor/Fxx] Change state to analyse
            return (2);

        case 0x4F:              // [PF1-4]  Change state to ignore
            return (4);
    }

    return (0);                 // All other characters - return to edit state 0
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U SciEdit2(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 2. The key was either a cursor key or a function
  key.  Cursors keys have the code sequence "ESC[A" to "ESC[D", while function keys have the code
  sequence "ESC[???~" where ??? is a variable number of alternative codes.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)                 // Switch according to next code
    {
        case 0x41:  SciPreviousLine();  return (0); // [Up]     Previous history line

        case 0x42:  SciNextLine();      return (0); // [Down]   Next history line

        case 0x43:  SciCursorRight();   return (0); // [Right]  Move cursor right

        case 0x44:  SciCursorLeft();    return (0); // [Left]   Move cursor left
    }

    return (3);                 // Function key - change to edit state 3
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U SciEdit3(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 3.  Orignial Key was a Function with sequence
  terminated by 0x7E (~).  However, the function will also accept a new [ESC] to allow an escape route
  in case of corrupted reception.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)                 // Switch according to next code
    {
        case 0x1B:              return (1); // [ESC]    Escape to edit state 1

        case 0x7E:              return (0); // [~]      Sequence complete - return to state 0
    }

    return (3);                 // No change to edit state
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U SciEdit4(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 4.  The original key was PF1-4 and one more
  character must be ignored.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (0);                 // Return to edit state 0
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciInsertChar(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit0() if a standard character has been received.  It will try to enter
  the character into the current line under the current cursor position.  The rest of the line will be
  moved to the right to make space for the new character.  If the line is full, the bell will ring.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  i;

    if (ch >= 0x20)         // If character is printable
    {
        if (terminal_state.line_end == TRM_LINE_SIZE)   // If line buffer is full...
        {
            SciRingBell();              // Ring the bell
        }
        else                    // else
        {
            for (i = terminal_state.line_end++; i > terminal_state.line_idx; i--) // For the rest of the line (backwards)
            {
                terminal_state.linebuf[i] = terminal_state.linebuf[i - 1];      // Shift characters one column in buffer
            }

            terminal_state.linebuf[terminal_state.line_idx] = ch;   // Save new character in line buffer

            for (i = terminal_state.line_idx++; i < terminal_state.line_end; i++) // For the rest of the line (forwards)
            {
                SerialStreamInternal_WriteChar(terminal_state.linebuf[i], tcm.f);   // Output chars to screen
            }

            i = terminal_state.line_end - terminal_state.line_idx;

            if (i)      // If cursor is now offset from true position
            {
                fprintf(tcm.f, "\33[%uD", i);       // Move cursor back
            }

            SerialStreamInternal_FlushBuffer(tcm.f);            // Flush buffer
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciNewline(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit0() if Enter or Return or ; have been received.  It skips leading
  white space and enters the line into the command buffer and the line history.  It then executes the
  command (blocking).
\*---------------------------------------------------------------------------------------------------------*/
{
    char  * cp;

    terminal_state.linebuf[terminal_state.line_end] = '\0'; // Nul terminate line buffer

    for (cp = terminal_state.linebuf; *cp == ' '; cp++); // Skip leading white space

    if (*cp)              // If line is not empty
    {
        if (terminal_state.line_end != terminal_state.line_len[(terminal_state.cur_line - 1) & TRM_LINEBUFS_MASK] ||
            // If new line is different
            memcmp(terminal_state.linebuf, &terminal_state.linebufs[(terminal_state.cur_line - 1) & TRM_LINEBUFS_MASK],
                   terminal_state.line_end))
        {
            // Save new line in history buffer

            terminal_state.line_len[terminal_state.cur_line] = terminal_state.line_end;
            memcpy(terminal_state.linebufs[terminal_state.cur_line], terminal_state.linebuf, terminal_state.line_end);

            terminal_state.cur_line = (terminal_state.cur_line + 1) & TRM_LINEBUFS_MASK;
        }

        SciDefaultPrefix(cp);           // Check for default commands
        SciAddCmd(cp);              // Add command line to packet
        SciDirect(';');             // Execute command (blocks until execution completed)
    }
    else                // else (empty line)
    {
        fputs("\n;\v", tcm.f);          // Repeat prompt
    }

    terminal_state.line_idx = terminal_state.line_end = 0;  // Reset cursor and end of line indexes
    terminal_state.recall_line = terminal_state.cur_line;   // Reset recall line index
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciCursorLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit2() if the Cursor left key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (!terminal_state.line_idx)               // If cursor is already at the start of the line
    {
        SciRingBell();                  // Ring the bell
    }
    else
    {
        fputs("\b\v", tcm.f);               // Move cursor left one character
        terminal_state.line_idx--;              // Decrement cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciCursorRight(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit2() if the Cursor right key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (terminal_state.line_idx == terminal_state.line_end) // If cursor is already at the end of the line
    {
        SciRingBell();                  // Ring the bell
    }
    else
    {
        fputs("\33[C\v", tcm.f);            // Move cursor right one character
        terminal_state.line_idx++;              // Increment cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciStartOfLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit0() if the Ctrl-A key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (terminal_state.line_idx)            // If cursor is not already at the start of the line
    {
        fprintf(tcm.f, "\33[%uD\v", terminal_state.line_idx); // Move cursor the required number of columns left
        terminal_state.line_idx = 0;                // Reset cursor position index
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciEndOfLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit0() if the Ctrl-E key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  i;

    i = terminal_state.line_end - terminal_state.line_idx;

    if (i)      // If cursor is not already at the end of the line
    {
        fprintf(tcm.f, "\33[%uC\v", i);         // Move cursor the required number of columns right
        terminal_state.line_idx = terminal_state.line_end;      // Set cursor position index to end of line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciDeleteLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit0() if the Ctrl-U key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    fputs("\r\33[K;\v", tcm.f);         // Clear line

    terminal_state.line_idx = terminal_state.line_end = 0;  // Reset cursor and line end indexes
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciDeleteLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit0() if the backspace or delete keys have been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (!terminal_state.line_idx)               // If cursor is already at the start of the line
    {
        SciRingBell();                  // Ring the bell
    }
    else
    {
        SerialStreamInternal_WriteChar('\b', tcm.f);            // Cursor left
        terminal_state.line_idx--;              // Adjust cursor position index
        SciShiftRemains();              // Shift remains of line left
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciDeleteRight(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit0() if the Ctrl-D key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (terminal_state.line_idx == terminal_state.line_end) // If cursor is already at the end of the line
    {
        SciRingBell();                  // Ring the bell
    }
    else
    {
        SciShiftRemains();              // Shift remains of line left
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciShiftRemains(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciDeleteLeft() and SciDeleteRight() to shift the remains of the line
  one character to the left.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  i;

    terminal_state.line_end--;              // Adjust end of line index

    for (i = terminal_state.line_idx; i < terminal_state.line_end; i++) // For the remainder of the line
    {
        // Shift character in buffer and display it
        SerialStreamInternal_WriteChar((terminal_state.linebuf[i] = terminal_state.linebuf[i + 1]), tcm.f);
    }

    SerialStreamInternal_WriteChar(' ', tcm.f);             // Clear last character

    fprintf(tcm.f, "\33[%uD\v",         // Move cursor the required number of columns left
            (1 + terminal_state.line_end - terminal_state.line_idx));
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciRepeatLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit0() if Ctrl-R is entered.  It recovers the previous line from the line
  history and enters it.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  recall_line;

    recall_line = (terminal_state.cur_line - 1) & TRM_LINEBUFS_MASK;    // Calc index of previous line in history

    if (!terminal_state.line_len[recall_line])              // If previous line history is blank
    {
        return;                             // ignore CTRL-R completely
    }

    terminal_state.cur_line = recall_line;                  // Set current line to avoid double entry
    SciRecallLine(recall_line);                 // Recall line
    SciNewline();                       // Submit line to command buffer
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciPreviousLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit2() if cursor up is pressed.  It will recover the previous line from
  the line history buffers.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  recall_line;

    if (terminal_state.recall_line == terminal_state.cur_line)              // If editing current line
    {
        terminal_state.line_len[terminal_state.cur_line] = terminal_state.line_end;
        // save the current line in the history
        memcpy(&terminal_state.linebufs[terminal_state.cur_line], terminal_state.linebuf, terminal_state.line_end);
    }

    // Adjust recall line index to previous line
    recall_line = (terminal_state.recall_line - 1) & TRM_LINEBUFS_MASK;

    if (recall_line == terminal_state.cur_line ||               // If at the end of the history
        !terminal_state.line_len[recall_line])               // or the history is empty
    {
        SciRingBell();                          // Ring the bell
    }
    else                            // else history line is available so
    {
        SciRecallLine(recall_line);                 // recall history line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciNextLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciEdit2() if cursor down is pressed.  It will recover the next line from
  the line history buffers.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  recall_line;

    if (terminal_state.recall_line == terminal_state.cur_line)              // If editing current line
    {
        SciRingBell();                          // Ring the bell
    }
    else                            // else editing a history line
    {
        recall_line = (terminal_state.recall_line + 1) & TRM_LINEBUFS_MASK; // Adjust recall line index

        SciRecallLine(recall_line);                 // Recall history line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciRecallLine(INT16U recall_line)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciRepeatLine(), SciPreviousLine() or SciNextLine() when a history line
  needs to be recalled.
\*---------------------------------------------------------------------------------------------------------*/
{
    terminal_state.recall_line = recall_line;                   // Save recall line idx
    terminal_state.line_end    = terminal_state.line_len[recall_line];          // Recover line length

    // Recover line from history
    memcpy(terminal_state.linebuf, &terminal_state.linebufs[recall_line], terminal_state.line_end);

    terminal_state.linebuf[terminal_state.line_end] = '\0';             // Nul terminate the line

    fputs("\r\33[K;", tcm.f);                       // Clear line

    fputs(terminal_state.linebuf, tcm.f);                       // Write recovered line
    SerialStreamInternal_FlushBuffer(tcm.f);                            // Flush buffer

    terminal_state.line_idx = terminal_state.line_end;                  // Set cursor position to end of line
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciAddCmd(char  * cp)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciNewline() and SciDefaultPrefix() to add a text string to the command
  packet buffer.
\*---------------------------------------------------------------------------------------------------------*/
{
    char    ch;

    while ((ch = *(cp++)))              // loop while characters remain
    {
        SciDirect(ch);          // Submit character to command packet
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciDefaultPrefix(char * cp)
/*---------------------------------------------------------------------------------------------------------*\
  This function helps to make the user interface easier to use when the editor is running.  It provides
  the following support:

    1.  Default "s ref now" command - if the command buffer starts with a number, the function
    adds the prefix "S REF NOW," in the command buffer.

    2.  Default get command - if the command doesn't start "S " or "s " or "G " or "g ", the
    function adds the prefix "G " in the command buffer.

\*---------------------------------------------------------------------------------------------------------*/
{
    char    ch1;
    char    ch2;

    ch1 = cp[0];                    // Get first two characters of command
    ch2 = cp[1];

    // Default S REF NOW command

    if (ch1 == '+' || ch1 == '-' || ch1 == '.' ||   // If command starts with a number
        (ch1 >= '0' && ch1 <= '9'))
    {
        SciAddCmd("!S REF NOW,");               // Default to S REF NOW command
        return;
    }

    // Default G command

    if ((ch1 != 'S' && ch1 != 's' && ch1 != 'G' && ch1 != 'g')          // If char 1 is not S or s or G or g, or
        || (ch2 != 0   && ch2 != ' ')                      // char 2 is not nul or a space
       )
    {
        SciAddCmd("!G ");                   // Default to get command
        return;
    }

    SciDirect('!');                 // Start command
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciRingBell(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will ring the bell.
\*---------------------------------------------------------------------------------------------------------*/
{
    SerialStreamInternal_WriteChar('\a', tcm.f);            // Ring the bell
    SerialStreamInternal_FlushBuffer(tcm.f);                    // Flush buffer
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U SciAddCh(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciDirect() when a character must be added to the current packet.  If the
  character results in the packet overflowing, the packet will be submitted to the TcmTsk.  The character
  is the first of the packet it checks that the packet buffer is available.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (tcm.errnum)                                     // If Scm has reported a parsing error
    {
        return (tcm.errnum);                                    // Report the error to stop the command
    }

    if (!terminal_state.pkt_in_idx)                             // If starting a new packet
    {
    CheckPktBuf:

        terminal_state.pkt = &tcm_pars.pkt[terminal_state.pkt_buf_idx];   // Get address of new packet

        if (terminal_state.pkt->header_flags)                           // If packet buffer is not free
        {
            return (FGC_PKT_BUF_NOT_AVL);                               // report BUF NOT AVL error
        }
    }
    else if (terminal_state.pkt_in_idx >= MAX_CMD_PKT_LEN)      // else if packet is full
    {
        if (terminal_state.pkt_header_flags == FGC_FIELDBUS_FLAGS_GET_CMD) // If get command
        {
            return (FGC_CMD_BUF_FULL);                                  // Report error (1 pkt only for get)
        }

        SciSendPkt();                                           // Send the packet to TcmTsk

        goto CheckPktBuf;                                       // Jump back to check if new packet is free
    }

    terminal_state.pkt->buf[terminal_state.pkt_in_idx++] = ch;          // Save character in buffer
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
static void SciSendPkt(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SciDirect()and SciAddCh() when a packet is ready to be sent to the
  TcmTsk.  A semaphore is used to inform the TcmTsk that the packet is waiting.
\*---------------------------------------------------------------------------------------------------------*/
{
    terminal_state.pkt->header_flags = terminal_state.pkt_header_flags;     // Prepare new packet header
    terminal_state.pkt->n_chars      = terminal_state.pkt_in_idx;

    OSSemPost(tcm.sem);                     // Trigger command task to process new packet

    Clr(terminal_state.pkt_header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT); // Clear first pkt bit in header
    terminal_state.pkt_buf_idx ^= 1;                    // Flip to other buffer for next packet
    terminal_state.pkt_in_idx   = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void TrmMsOut(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the the Millisecond Task to manage output buffers destined for the SCI.

  Note that terminal_state.bufq can never fill up because it is one element longer than the number of buffers it
  needs to address.

  The diag_sci structure starts with a header field of six sync bytes.

  Note that term_byte_idx is initialised to 1 and counts to the length of the SCI diag data.
  diag.term_byte_idx is set to zero when diag transmission starts to signal that it
  is the first cycle and transmission is to be suppressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    static INT16U   idx;
    static INT16U   len;
    static char  *  buf;
    char            ch;

    // Diagnostic Output - to SCI only (not to Remote terminal)

    if (terminal.mode == TRM_DIAG_MODE)          // If diag transmission is active
    {
        if (!terminal.char_waits_for_tx                  // If output buffer is empty and
            && (diag.term_byte_idx < DIAG_TERM_LEN)      // diag struct has not been completely sent
           )
        {
            terminal.snd_ch = *(diag.term_buf_p++);       // Pass next diag byte to IsrMst for transmission
            terminal.char_waits_for_tx = TRUE;       // Set flag to say that byte is waiting
            diag.term_byte_idx++;            // Increment index
        }

        return;
    }

    // Pass output character to IsrMst and FIP remote terminal buffer (if in use)

    if (!terminal.char_waits_for_tx && !fbs.termq.is_full)   // If output character buffer(s) are empty...
    {
        if (!buf)   // If no output buffer is being processed...
        {
            // if a new output buffer is waiting in Q...
            if (terminal_state.bufq.in_idx != terminal_state.bufq.out_idx)
            {
                idx = 0;                        // Reset buffer index
                buf = terminal_state.bufq_adr[terminal_state.bufq.out_idx];         // Get buffer address
                len = terminal_state.bufq_len[terminal_state.bufq.out_idx];     // and length
                terminal_state.bufq.out_idx++;

                if (terminal_state.bufq.out_idx > TRM_N_BUFS)           // Adjust buf Q out idx
                {
                    terminal_state.bufq.out_idx = 0;
                }
            }
        }

        if (buf)        // If an output buffer is being processed...
        {
            if (len)        // and still contains unsent characters...
            {
                terminal.snd_ch = ch = buf[idx++];   // put next character in SCI send buffer for IsrMst()
                terminal.char_waits_for_tx = TRUE;   // Set flag to say that byte is waiting

                if (fbs.gw_online_f)            // If FIP/ETH link is working
                {
                    FbsOutTermCh(ch);           // send character to FIP task as well
                }

                len--;                          // Decrement length of string remaining
            }

            if (!len)                           // If buffer is now empty...
            {
                OSMemPost(terminal.mbuf, buf);       // Return buffer to partition
                buf = 0;                // Clear buffer pointer
            }
        }
    }

    // Editor mode timeout (1 hour)

    if (terminal.mode == TRM_EDITOR_MODE)    // If line editor is enabled
    {
        terminal_state.edit_mode_timer_ms++;

        if (terminal_state.edit_mode_timer_ms >= 60000)     // If millisecond timer has reached 1 minute
        {
            terminal_state.edit_mode_timer_min++;

            if (terminal_state.edit_mode_timer_min >= TRM_EDITOR_MODE_TIMEOUT_MIN)  // If minute timer has reach timeout
            {
                SciTerm(TRM_DIRECT_MODE);                     // Switch to Direct mode
            }

            terminal_state.edit_mode_timer_ms = 0;                      // Reset millisecond timer
        }
    }
}

// EOF
