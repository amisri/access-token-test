/*!
 *  @file      get.c
 *  @defgroup  FGC3:MCU
 *  @brief     This file contains the function for the Get Task and all the
 *  property Get functions.
 */

#define GET_GLOBALS

// Includes

#include <get.h>

#include <class.h>          // Include class dependent definitions
#include <core.h>           // for CoreTableGet(), struct fgc_corectrl & fgc_coreseg
#include <defprops.h>       // Include property related definitions
#include <dev.h>            // for dev global variable,
#include <diag.h>           // for diag global variable
#include <DIMsDataProcess.h>// for DimGetDimLog()
#include <dls.h>            // for dls, id  global variable
#include <dpcls.h>          // for dpcls global variable
#include <dpcom.h>          // for dpcom global variable, struct abs_time_us
#include <fbs.h>            // for fbs global variable, FbsOutLong(), FbsOutBuf()
#include <fgc/fgc_db.h>     // for access SysDB and CompsDB
#include <fgc_errs.h>       // for FGC_BAD_ARRAY_IDX
#include <fgc_log.h>        // for struct fgc_log_header, struct fgc_log_sig_header
#include <ftoa.h>           // for ftoa()
#include <get_class.h>
#include <log_cycle.h>
#include <log_event.h>      // for struct log_evt_rec
#include <log.h>            // for LOG_MAX_SIGS, struct log_ana_vars
#include <m16c62_link.h>    // for MICROLAN_UNIQUE_SERIAL_CODE_LEN
#include <macros.h>         // for Test(), Set(), Clr(), DEVICE_CYC
#include <mcu_dependent.h>  // for ENTER_BB(), EXIT_BB(), ENTER_SR(), EXIT_SR
#include <mcu_dsp_common.h>
#include <prop.h>           // for PropValueGet()
#include <pub.h>
#include <scivs.h>
#include <spivs.h>
#include <regfgc3_pars.h>
#include <regfgc3_task.h>
#include <shared_memory.h>  // for shared_mem global variable
#include <start.h>          // for Crash()
#include <stdio.h>          // for fputc(), fprintf(), fputs()
#include <string.h>
#include <task_trace.h>
#if (FGC_CLASS_ID != 61)
#include <name_db.h>
#include <inter_fgc.h>
#endif

// Internal function declarations

/*!
 * Helper function for GetComponent(). This will display the type and
 * description for one particular type of component. It will also add
 * the prefix for the number found and required before the first element
 * of the list.
 */
static INT16U GetComp(struct cmd * c, INT16U * list_idx, INT16U n_found, INT16U n_req, fgc_db_t comp_idx);

// External function definitions


INT16U GetAbsTime(struct cmd * c, struct prop * p)
{
    prop_size_t          n;          // Number of element in array range
    INT16U               errnum;         // Error number
    const char     *     fmt;
    struct abs_time_us * t;
    static const char  * decfmt[]   = { "%10u.%06u",      "%u.%06u"   };
    static const char  * hexfmt[]   = { " 0x%08X.%05X",   "0x%X.%05X" };


    errnum = CmdPrepareGet(c, p, 2, &n);

    if (errnum)
    {
        return (errnum);
    }

    errnum = PropValueGet(c, p, NULL, 0, (INT8U **)&t); // Get first abstime element to use as timestamp

    if (errnum)
    {
        return (errnum);
    }

    fmt = (Test(c->getopts, GET_OPT_HEX) ? hexfmt[(c->token_delim == ',')] :
           decfmt[(c->token_delim == ',')]);

    while (n--)                 // For all elements in the array range
    {
        errnum = PropValueGet(c, p, NULL, c->from, (INT8U **)&t); // Get property (from DSP is necessary)

        if (errnum)
        {
            return (errnum);
        }

        errnum = CmdPrintIdx(c, c->from);            // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        fprintf(c->f, fmt, t->unix_time, t->us_time);

        c->from += c->step;
    }

    return (0);
}


INT16U GetChar(struct cmd * c, struct prop * p)
{
    char            ch = 1;                 // Character to display (initialise to non-zero for while())
    char      *     ch_ptr;
    char            delim_save;             // Saved token delimiter
    INT16U          hex_f;                  // HEX get option flag
    prop_size_t     n;                      // Number of array elements to display
    INT16U          errnum = FGC_OK_NO_RSP; // Error number

    hex_f = Test(c->getopts, GET_OPT_HEX);

    errnum = CmdPrepareGet(c, p, (hex_f ? 8 : 64), &n);

    if (errnum)
    {
        return (errnum);
    }

    delim_save = c->token_delim;            // Save delimiter so that it can be restored at the end

    if (!hex_f && c->token_delim == ' ')    // If displaying ASCII and not comma delimiter
    {
        c->token_delim = '\0';              // Suppress space delimiter
    }

    // For all characters in array range

    while (n-- &&
           (errnum = PropValueGet(c, p, NULL, c->from, (INT8U **)&ch_ptr)) == 0 &&
           (ch = *ch_ptr) != 0)
    {
        errnum = CmdPrintIdx(c, c->from);   // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        if (hex_f)                          // If hex flag is set
        {
            fprintf(c->f, "0x%02X", ch);    // Display character as hex
        }
        else
        {
            fputc(ch, c->f);                // Display character as ASCII
        }

        c->from += c->step;
    }

    c->token_delim = delim_save;            // Restore delimiter in case called from parent or config.set

    return (errnum);
}


INT16U GetComponent(struct cmd * c, struct prop * p)
{
    prop_size_t n;              // Number of array elements to display
    INT16U      errnum;         // Error number
    fgc_db_t      grp_idx;        // Group index
    fgc_db_t      comp_idx = 0;   // Comp index
    INT16U      list_idx;       // List element index
    BOOLEAN     unknown_f;      // Scan for unknown components
    const fgc_db_t    n_comps = CompDbLength(); // Number of components

    if (!dls.active_f)              // If ID scan has not completed successfully
    {
        return (FGC_ID_NOT_READY);          // Report that ID is not ready
    }

    if (c->n_arr_spec)              // If array is specified
    {
        return (FGC_BAD_ARRAY_IDX);         // report BAD ARRAY INDEX
    }

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    Clr(c->getopts, GET_OPT_IDX);       // Turn of printing of element indexes
    unknown_f = FALSE;                  // Start with known groups
    grp_idx = 0;                        // Reset group index

    while (n--)                         // For all elements
    {
        grp_idx++;                      // Consider next group
        list_idx = 0;

        if (!unknown_f)                 // If showing Matched/mismatched groups
        {
            switch (p->sym_idx)
            {
                case STP_MATCHED:

                    while (grp_idx < FGC_N_COMP_GROUPS &&       // Scan to find next matching group
                           (!SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) ||
                            SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) != dev.sys.detected[grp_idx]))
                    {
                        grp_idx++;
                    }

                    break;

                case STP_MISMATCHED:

                    while (grp_idx < FGC_N_COMP_GROUPS &&       // Scan to find next mismatching group
                           (!SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) ||
                            SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) == dev.sys.detected[grp_idx]))
                    {
                        grp_idx++;
                    }

                    break;
            }

            if (grp_idx >= FGC_N_COMP_GROUPS)           // If all groups checked
            {
                comp_idx = 0;                           // Switch to unknown comps
                list_idx = 0;
                unknown_f = TRUE;
            }
            else
            {
                for (comp_idx = 1; comp_idx < n_comps; comp_idx++)
                {
                    if (CompDbGroupIndex(comp_idx,dev.sys.sys_idx) == grp_idx)
                    {
                        errnum = GetComp(c, &list_idx, dev.sys.detected[grp_idx], SysDbRequiredGroups(dev.sys.sys_idx,grp_idx), comp_idx);

                        if (errnum)
                        {
                            return (errnum);
                        }
                    }
                }
            }
        }

        if (unknown_f)                  // If showing unknown components
        {
            while (comp_idx < n_comps && !dev.sys.n_unknown_grp[comp_idx])
            {
                comp_idx++;
            }

            if (comp_idx >= n_comps)         // If end of components reached
            {
                n = 0;                          // Abort early
            }
            else
            {
                errnum = GetComp(c, &list_idx, dev.sys.n_unknown_grp[comp_idx], 0, comp_idx);
                comp_idx++;

                if (errnum != 0)
                {
                    return (errnum);
                }
            }
        }
    }

    return (0);
}


INT16U GetConfigChanged(struct cmd * c, struct prop * p)
{
    // Displays the modified config property names and data.

    prop_size_t n;              // Number of array elements to display
    INT16U      errnum;         // Error number

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    c->si_idx = c->n_prop_name_lvls = 0;               // Force complete name to be written

    p->n_elements = p->max_elements;

    return (CmdParentGet(c, p, PARENT_GET_CONFIG_CHANGED));
}


INT16U GetConfigSet(struct cmd * c, struct prop * p)
{
    // Displays the set config property names and data.

    prop_size_t n;              // Number of array elements to display
    INT16U      errnum;         // Error number

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    c->si_idx = c->n_prop_name_lvls = 0;               // Force complete name to be written

    p->n_elements = p->max_elements;

    return (CmdParentGet(c, p, PARENT_GET_CONFIG_SET));
}


INT16U GetConfigUnset(struct cmd * c, struct prop * p)
{
    // Displays the unset config property names.

    prop_size_t n;              // Number of array elements to display
    INT16U      errnum;         // Error number

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    c->si_idx = c->n_prop_name_lvls = 0;               // Force complete name to be written

    p->n_elements = p->max_elements;

    return (CmdParentGet(c, p, PARENT_GET_CONFIG_UNSET));
}


INT16U GetCore(struct cmd * c, struct prop * p)
{
    // Displays the core data properties. Only supplies results with
    // the BIN get option.

    INT16U              n_bytes;
    INT16U              n_words;
    prop_size_t         idx;
    prop_size_t         n_segs;
    INT8U       *       core_data;
    INT8U       *       table_data;
    INT16U              errnum;
    struct fgc_corectrl corectrl;

    if (c->step > 1)                            // If array step is not 1
    {
        return (FGC_BAD_ARRAY_IDX);                     // Report bad array index
    }

    if (!Test(c->getopts, GET_OPT_BIN))     // If BIN get option not specified
    {
        return (FGC_BAD_GET_OPT);           // Report bad get option!
    }

    n_words    = 0;

    errnum = CmdPrepareGet(c, p, 1, &n_segs);

    if (errnum)
    {
        return (errnum);
    }

    /*--- Get Stack dump ---*/

    if (p->sym_idx == STP_STACK)
    {
        n_bytes = NVRAM_CORESTACK_W * 2;        // Length of stack dump in bytes

        CmdStartBin(c, p, (INT32U)n_bytes);

        core_data = (INT8U *)NVRAM_CORESTACK_32;

        while (n_bytes--)               // Write data
        {
            c->store_cb(*(core_data++));
        }

        return (0);
    }

    /*--- Get Core dump ---*/

    CoreTableGet(&corectrl);            // Retrieve table from FRAM

    for (idx = c->from; idx <= c->to; idx++)
    {
        n_words += (corectrl.seg[idx].n_words + (sizeof(struct fgc_coreseg)) / 2);
    }

    CmdStartBin(c, p, (INT32U)n_words * 2);

    /*--- Write data segments from FRAM ---*/

    for (idx = c->from; idx <= c->to; idx++)    // For each segment
    {
        n_bytes = corectrl.seg[idx].n_words * 2;
        core_data = (INT8U *)corectrl.addr_in_core[idx];

        while (n_bytes--)                       // Write data
        {
            c->store_cb(*(core_data++));
        }
    }

    /*--- Write one table entry per segment ---*/

    n_bytes = n_segs * sizeof(struct fgc_coreseg);
    table_data = (INT8U *)&corectrl.seg[c->from];

    while (n_bytes--)
    {
        c->store_cb(*(table_data++));
    }

    return (0);
}


INT16U GetDiagChan(struct cmd * c, struct prop * p)
{
    // Displays the DIAGCHAN properties and the addressed diagnostic data!

    prop_size_t     n;              // Number of array elements to display
    INT8U     *     chan;           // Pointer to channel element
    INT16U          data;           // data for channel
    INT16U          errnum;         // Error number
    INT16U          data_chan;      // Channel within diag.data[] array
    fgc_db_t          logical_dim;
    fgc_db_t          board_number;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    while (n--)   // For all elements
    {
        errnum = CmdPrintIdx(c, c->from);            // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        chan      = &((INT8U *)p->value)[c->from];  // Get address of channel
        data_chan = *(chan);                        // Get current channel (list_offset is not used)

        fprintf(c->f, "0x%02X ", data_chan);        // Show raw channel number in hex always

        if (data_chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL))   // If DIM register
        {
            //                      & 0x001F
            logical_dim = data_chan & (FGC_MAX_DIM_BUS_ADDR - 1); // Translate to logical address

            board_number = SysDbDimBusAddress(dev.sys.sys_idx, logical_dim);

            if (board_number < FGC_MAX_DIM_BUS_ADDR)
            {
                data_chan = board_number + (data_chan & 0x00E0);
            }
        }

        // ToDo: can data_chan coded as [register:branch:board] [(0..7)(soft registers):(0..1):(0..31)

        // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)

        data = diag.data.w[data_chan];

        if (Test(c->getopts, GET_OPT_HEX))      // If hex get option
        {
            fprintf(c->f, "0x%04X", data);      // Display address and data in hex
        }
        else
        {
            if (data_chan < DIAG_MAX_ANALOG)
            {
                data &= 0xFFF;
                fprintf(c->f, "%4.0f", ((FP32)data * DIAG_ANALOG_CAL)); // Display as millivolts
            }
            else
            {
                if (data_chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL))   // if data is digital
                {
                    data &= 0xFFF;
                    fprintf(c->f, "0x%03X", data);  // Display address and data in hex
                }
                else
                {
                    fprintf(c->f, "%u", data);      // Display software channel in decimal
                }
            }
        }

        // Next element

        c->from += c->step;
    }

    return (0);
}


INT16U GetElapsedTime(struct cmd * c, struct prop * p)
{
    return (GetInteger(c, p));
}


INT16U GetErrIdx(struct cmd * c, struct prop * p)
{
    /*
     * Both fieldbus and terminal commands have their own error index
     * variables. This function will link to the relevant variable and
     * then use GetInteger() to report the value.
     */

    p->value = &c->device_par_err_idx;      // Link property value to error index for the command

    return (GetInteger(c, p));          // Display the error index
}


INT16U GetFloat(struct cmd * c, struct prop * p)
{
    /*
     * Measurement properties must be available in full resolution (8 digits)
     * while configuration properties should be limited to 6 digits to prevent
     * 5.0 appearing as 4.9999999, for example. The NON_VOLATILE flag is used
     * to control this choice of format when the NEAT get option is not active.
     * The function supports the ZERO get option.
     */

    BOOLEAN         bin_f;          // Binary get option active
    BOOLEAN         ze_f;           // ZERO get option active
    BOOLEAN         hex_f;          // HEX get option active
    BOOLEAN         set_f;          // Value has been set (to zero by ZERO get opt)
    prop_size_t     n;              // Number of element in array range
    INT16U          format_idx = 0; // Format index for DSP
    FP32      *     value_p;        // Pointer to float value
    FP32            value;          // Float value
    const char   *  fmt;            // Pointer to format string
    INT16U          errnum;         // Error number

    static const char   *  decfmt[]   = { "", "%.5E", "%.7E", "%13.5E" };  // Max length is 12 chars
    static const char   *  hexfmt[]   = { "   0x%08X", "0x%X" };           // CmdPrintfDsp in DSP lib
    static const INT16U prec[] = {0, 5, 7, 5};
    static const INT16U flags[] = {0, UPPER | ETYPE, UPPER | ETYPE, UPPER | ETYPE};
    static const INT16S width[] = {0, 0, 0, 13};
    char                float_str[15];

    bin_f = Test(c->getopts, GET_OPT_BIN);              // Prepare binary flag

    hex_f = Test(c->getopts, GET_OPT_HEX);              // Prepare hex flag

    ze_f  = Test(p->flags, PF_GET_ZERO) &&              // Prepare zero flag
            Test(c->getopts, GET_OPT_ZERO);

    if (hex_f)                                  // If HEX
    {
        fmt = hexfmt[c->token_delim == ','];
    }
    else                            // else DECIMAL
    {
        if (c->token_delim == ' ')                  // If NEAT
        {
            format_idx = 3;
        }
        else                                // else not NEAT
        {
            format_idx = 1 + !Test(p->flags, PF_CONFIG);
        }

        fmt = decfmt[format_idx];
    }

    set_f = FALSE;
    errnum = CmdPrepareGet(c, p, 4, &n);

    if (errnum)
    {
        return (errnum);
    }

    if (n && bin_f)
    {
        CmdStartBin(c, p, (INT32U)(sizeof(float)) * (INT32U)n);
    }

    while (n--)   // For all elements
    {
        errnum = PropValueGet(c, p, &set_f, c->from, (INT8U **)&value_p);

        if (errnum != 0)   // Get element value from property
        {
            return (errnum);
        }

        value = *value_p;

        if (ze_f)
        {
            *value_p = 0.0;
        }

        set_f = ze_f;

        if (bin_f)
        {
            FbsOutLong((char *) &value, c);
        }
        else
        {
            errnum = CmdPrintIdx(c, c->from);                // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            if (hex_f)
            {
                // Display as 0xXXXXXXXX
                fprintf(c->f, fmt, *((INT32U *)value_p));
            }
            else
            {
                ftoa(float_str, value, prec[format_idx], flags[format_idx], width[format_idx]);
                fputs(float_str, c->f);
            }

        }

        c->from += c->step;
    }

    if (set_f)
    {
        PropBlkSet(c, c->n_elements, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT); // Write back zeroed value(s)
    }

    return (0);
}


INT16U GetId(struct cmd * c, struct prop * p)
{
    /*
     * ToDo split into 2 different functions and eliminate runtime type checking
     *
     * The max_elements field of the property structure has been used to
     * indicate the Dallas branch associated with the property:
     *  ID.VS.A     0
     *  ID.MEAS.A   1
     *  ID.VS.B     2
     *  ID.MEAS.B   3
     *  ID.LOCAL    4
     *  ID.CRATE    5
     *  ID.SUMMARY  6
     */

    prop_size_t         n;
    INT16U              errnum;         // Error number
    BOOLEAN             neat_f;         // Neat get option flag
    INT16U              logical_path;
    INT16U              id_dev_idx;
    INT16U       *      sorted_idx;
    const char     *    fmt;            // Pointer to format string
    struct TMicroLAN_element_summary  * id_dev;
    char                comp_label[FGC_SYSDB_SYS_LBL_LEN + 2];
    char                id_string[ 2 * MICROLAN_UNIQUE_SERIAL_CODE_LEN + 2 ];
    static const char * slfmt[] = { "%s%3u.%02u %-19s ", "%s:%02u.%02u:%s=" };


    if (p->sym_idx == STP_SUMMARY)      // If getting ID - report scan summary (ID.SUMMARY)
    {
        errnum = CmdPrepareGet(c, p, 1, &n);

        if (errnum)
        {
            return (errnum);
        }

        while (n--)             // For all elements
        {
            errnum = CmdPrintIdx(c, c->from);                        // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            fprintf(c->f, "%u|cplrs %u devs %2u {idonly %2u idtemp %2u other %2u} max_reads %u",
                    (unsigned int) c->from,
                    id.logical_paths_info[c->from].ds2409s,
                    id.logical_paths_info[c->from].total_devices,
                    id.logical_paths_info[c->from].ds2401s,
                    id.logical_paths_info[c->from].ds18b20s,
                    id.logical_paths_info[c->from].unknow_devices,
                    id.logical_paths_info[c->from].max_reads);

            // Next element

            c->from += c->step;
        }
    }
    else                // Get results data
    {
        Set(c->getopts, GET_OPT_IDX);               // Set IDX get option
        neat_f        = (c->token_delim == ' ');        // Prepare neat flag
        logical_path  = p->max_elements;            // Max Elements in XML is branch number
        p->n_elements = id.logical_paths_info[logical_path].total_devices;
        fmt           = slfmt[!neat_f];             // Prepare format for symbol string

        errnum = CmdPrepareGet(c, p, 1, &n);

        if (errnum)
        {
            return (errnum);
        }

        while (n--)             // For all elements
        {
            errnum = CmdPrintIdx(c, c->from);

            if (errnum)             // Print index if required
            {
                return (errnum);
            }

            sorted_idx = &id.to_summary_sorted_by_codebar[id.start_in_elements_summary[logical_path] + c->from];
            id_dev_idx = *(sorted_idx);
            id_dev     = &id.devs[id_dev_idx];

            fprintf(c->f, fmt,
                    (char *)DlsIdString(&id_dev->unique_serial_code, id_string),
                    id_dev->temp_C,
                    ID_TEMP_FRAC_C(id_dev->temp_C_16),
                    (char *)id_dev->barcode.c);

            memcpy(&comp_label, CompDbLabel(dev.sys.comp_idx[id_dev_idx]), sizeof comp_label);

            if (neat_f && comp_label[30])       // There is space for up to 30 characters
            {
                // on the terminal window (NEAT mode)
                comp_label[27] = '.';
                comp_label[28] = '.';
                comp_label[29] = '.';
                comp_label[30] = '\0';
            }

            fputs(comp_label, c->f);

            // Next element

            c->from += c->step;
        }
    }

    return (0);
}


INT16U GetInteger(struct cmd * c, struct prop * p)
{
    BOOLEAN             bin_f;          // Binary get option active
    BOOLEAN             sl_f;           // Property uses symlist flag
    BOOLEAN             bm_f;           // Property uses bit mask (symbol list)
    BOOLEAN             ze_f;           // ZERO get option active
    BOOLEAN             set_f;          // Value has been set (to zero by ZERO get opt)
    prop_size_t         n;              // Number of array elements to display
    INT16U              type;           // Integer type index (0-5)
    INT16U              size;           // Integer size index (0-2)
    INT8S               v8   = 0;       // 8-bit value
    INT16S              v16  = 0;       // 16-bit value
    INT16U              u16  = 0;       // 16-bit (unsigned) value
    INT32S              v32  = 0;       // 32-bit value
    INT16U              errnum;         // Error number
    struct sym_lst   *  sym_lst;        // Symlist address
    const char     *    fmt;            // Pointer to format string
    union
    {
        INT8S     *     p8;
        INT16S     *    p16;
        INT32S     *    p32;
    } value;

    static const char  *  slfmt[]     = { "%13s", "%s" };
    static const char  *  hexfmt[]        = { "0x%02X", "0x%04X", "0x%08X" };
    static const char  *  hexneatfmt[]    = { "         0x%02X", "       0x%04X", "   0x%08X" };
    static const char  *  decfmt[]        = { "%u", "%d", "%u", "%d", "%u", "%d" };
    static const char  *  decneatfmt[]    = { "%13u", "%13d", "%13u", "%13d", "%13u", "%13d" };
    static const INT16U   size_bytes[]    = { 1, 2, 4 };



    sl_f   = Test(p->flags,  PF_SYM_LST) &&         // Prepare symlist flag
             !Test(c->getopts, GET_OPT_HEX);

    ze_f   = Test(p->flags,  PF_GET_ZERO) &&            // Prepare zero flag
             Test(c->getopts, GET_OPT_ZERO);

    bm_f   = Test(p->flags,  PF_BIT_MASK) && sl_f;      // Prepare bit mask flag

    bin_f  = Test(c->getopts, GET_OPT_BIN) && !sl_f;    // Prepare binary flag

    errnum = CmdPrepareGet(c, p, (bm_f ? 1 : 4), &n);   // Handle standard print options

    if (errnum)
    {
        return (errnum);
    }

    if (!n)                                     // If no data
    {
        return (FGC_OK_NO_RSP);                         // Return immediately with OK status
    }

    switch (p->type)            // Prepare integer type index (0-5)
    {
        case PT_INT8U:  type = 0;   break;

        case PT_INT8S:  type = 1;   break;

        case PT_INT16U: type = 2;   break;

        case PT_INT16S: type = 3;   break;

        case PT_INT32U: type = 4;   break;

        case PT_INT32S: type = 5;   break;

        default:
            return (FGC_BAD_PARAMETER);
            break;
    }

    size = type / 2;                // Prepare integer size index (0-2)

    if (sl_f)                   // If property uses symbol list
    {
        sym_lst = (struct sym_lst *)p->range;       // Get address of symbol list
        fmt     = slfmt[(c->token_delim == ',')];   // Prepare format for symbol string
        ze_f    = 0;                    // Cancel zero flag (should never be set)
    }
    else                    // else property doesn't use a sym list
    {
        if (Test(c->getopts, GET_OPT_HEX))      // Prepare format to use for integer data
        {
            fmt = ((c->token_delim == ' ') ? hexneatfmt[size] : hexfmt[size]);
        }
        else
        {
            fmt = ((c->token_delim == ' ') ? decneatfmt[type] : decfmt[type]);
        }
    }

    set_f = FALSE;

    if (bin_f)
    {
        CmdStartBin(c, p, (INT32U)size_bytes[size] * (INT32U)n);
    }

    while (n--)             // For all elements in the array range
    {
        errnum = PropValueGet(c, p, &set_f, c->from, (INT8U **) &value.p8);

        if (errnum  != 0)
        {
            return (errnum);
        }

        switch (size)               // Get data and zero if required
        {
            case 0:                 // Size 0: 8-bit

                v8 = *value.p8;

                if (ze_f) { *value.p8 = 0; }

                if (bin_f)
                {
                    c->store_cb(v8);
                }

                break;

            case 1:                 // Size 1: 16-bit

                v16 = *value.p16;

                if (ze_f) { *value.p16 = 0; }

                if (bin_f)
                {
                    FbsOutShort((INT8U *) &v16, c);
                }

                break;

            case 2:                 // Size 2: 32-bit

                v32 = *value.p32;

                if (ze_f) { *value.p32 = 0; }

                if (bin_f)
                {
                    FbsOutLong((char *) &v32, c);
                }

                break;
        }

        set_f = ze_f;

        if (!bin_f)
        {
            errnum = CmdPrintIdx(c, c->from);              // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            if (sl_f)               // If property uses a sym list
            {
                u16 = (INT16U)(size ? (size == 1 ? v16 : v32) : v8);

                if (bm_f)                   // If property is a bit mask
                {
                    CmdPrintBitMask(c->f, sym_lst, u16);    // Display all symbols which are active
                }
                else                    // else numeric symlist
                {
                    fprintf(c->f, fmt, CmdPrintSymLst(sym_lst, u16)); // Display value as a symbol
                }
            }
            else                    // else property is simply integer data
            {
                switch (size)               // Display according to data size
                {
                    case 0:
                        v16 = (type ? v8 : (INT8U)v8);

                        // Fall through

                    case 1:
                        if (type == 2)          // meaning, PT_INT16U
                        {
                            u16 = (INT16U)v16;
                            fprintf(c->f, fmt, u16);
                        }
                        else
                        {
                            fprintf(c->f, fmt, v16);
                        }

                        break;

                    case 2:
                        fprintf(c->f, fmt, v32);
                        break;
                }
            }
        }

        c->from += c->step;
    }

    if (set_f)
    {
        PropBlkSet(c, c->n_elements, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT); // Write back zeroed value(s)
    }

    return (0);
}



INT16U GetPoint(struct cmd * c, struct prop * p)
{
    struct point *   value_p;
    struct point     value;
    BOOLEAN          bin_f;
    BOOLEAN          hex_f;
    prop_size_t      n;
    INT16U           errnum;

    bin_f = Test(c->getopts, GET_OPT_BIN);
    hex_f = Test(c->getopts, GET_OPT_HEX);

    errnum = CmdPrepareGet(c, p, 2, &n);

    if (errnum)
    {
        return (errnum);
    }

    if (n != 0 && bin_f == true)
    {
        CmdStartBin(c, p, (INT32U)(sizeof(struct point)) * (INT32U)n);
    }

    // For all elements

    while (n--)
    {
        // Get element value from property

        errnum = PropValueGet(c, p, NULL, c->from, (INT8U **)&value_p);

        if (errnum != 0)
        {
            return (errnum);
        }

        value = *value_p;

        if (errnum)
        {
            return (errnum);
        }

        if (bin_f)
        {
            FbsOutBuf((INT8U *) &value, sizeof(struct point), c);
        }
        else
        {
            // Print index if required

            errnum = CmdPrintIdx(c, c->from);

            if (errnum)
            {
                return (errnum);
            }

            if (hex_f)
            {
                // Display point time and reference as 0xTTTTTTTT|0xRRRRRRRR

                fprintf(c->f, "0x%08lX|0x%08lX", (uint32_t)value.time, (uint32_t)value.ref);
            }
            else
            {
                // Display point as n.nnnn|n.nnnnnnnE+/-nn

                fprintf(c->f, "%.4f|%.7E", (double)value.time, (double)value.ref);
            }
        }

        c->from += c->step;
    }

    return (0);    
}


INT16U GetLogEvt(struct cmd * c, struct prop * p)
{
    /*
     * For the terminal only 20 of the maximum 23 property name characters
     * will be displayed.
     */

    OS_CPU_SR           cpu_sr;

    static char * FAR evtfmt[] = { "%lu%06lu:%s:%s:%s", "%08lX.%06lu:%-20s:%-35s:%s" };

    uint16_t           offset;
    int16_t            idx;
    struct log_evt_rec evt_rec;
    char * FAR         fmt;
    uint16_t           errnum;
    prop_size_t        n;
    bool               neat_f;

    if(c->step > 1)                            // If array step is not 1
    {
        return(FGC_BAD_ARRAY_IDX);                     // Report bad array index
    }

    errnum = CmdPrepareGet(c, p, 1, &n);    // Handle standard print options

    if (errnum)
    {
        return (errnum);
    }

    idx    = log_evt.index - c->from;
    neat_f = (c->token_delim == ' ');
    fmt    = evtfmt[neat_f];            // Prepare format string

    Clr(c->getopts, GET_OPT_IDX);       // Suppress display of element index

    if(Test(c->getopts, GET_OPT_BIN))
    {
        // Always dump all event log records

        if(c->n_arr_spec != 0 || c->step > 1)
        {
            return(FGC_BAD_ARRAY_IDX);
        }

        CmdStartBin(c, p, FGC_LOG_EVT_LEN * (sizeof(struct log_evt_rec) + sizeof(struct log_evt_rec)));

        // Normal log

        for(idx = 0; idx < FGC_LOG_EVT_LEN; idx++)
        {
            offset = idx * sizeof(struct log_evt_rec);

            OS_ENTER_CRITICAL();
            memcpy(&evt_rec, (void *) SRAM_LOG_EVT_32 + offset, sizeof(struct log_evt_rec));
            OS_EXIT_CRITICAL();

            // Always put a null as the last action character. This is for compatibility with libevtlog,
            // where the last character is a record status and should be set to null or space

            evt_rec.action[FGC_LOG_EVT_ACT_LEN - 1] = '\0';

            FbsOutBuf((uint8_t *)&evt_rec, sizeof(struct log_evt_rec), c);
        }

        // Cyclic log

        for(idx = 0; idx < FGC_LOG_EVT_LEN; idx++)
        {
            offset = idx * sizeof(struct log_evt_rec);

            OS_ENTER_CRITICAL();
            memcpy(&evt_rec, (char *) SRAM_LOG_CYC_EVT_32 + offset, sizeof(struct log_evt_rec));
            OS_EXIT_CRITICAL();

            // Always put a null as the last action character. This is for compatibility with libevtlog,
            // where the last character is a record status and should be set to null or space

            evt_rec.action[FGC_LOG_EVT_ACT_LEN - 1] = '\0';

            FbsOutBuf((uint8_t *)&evt_rec, sizeof(struct log_evt_rec), c);
        }
    }
    else
    {
        // Normal log

        while (n--)
        {
            if (idx < 0)
            {
                idx += FGC_LOG_EVT_LEN;
            }

            // Use intermediate variable to avoid compiler bug

            offset = sizeof(struct log_evt_rec) * idx;

            OS_ENTER_CRITICAL();
            memcpy(&evt_rec, (void *) SRAM_LOG_EVT_32 + offset, sizeof(struct log_evt_rec));
            OS_EXIT_CRITICAL();

            // Stop if end of buffer reached

            if (!evt_rec.timestamp.unix_time)
            {
                break;
            }

            if (neat_f)                 // If neat (terminal)
            {
                evt_rec.prop[20] = '\0';                // Clip at 20 chars for prop name
            }

            errnum = CmdPrintIdx(c, c->from++);          // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            // Substitute all commas in the property name with single quotes
            // and all colons with pipes. Commas and colons are special characters
            // used by the communication protocol.

            uint16_t i;
            char     ch;

            for(i = 0; i < FGC_LOG_EVT_PROP_LEN; i++)
            {
                ch = evt_rec.prop[i];

                if(ch == '\0')
                {
                    break;
                }
                else if(ch == ',')
                {
                    evt_rec.prop[i] = '\047';
                }
                else if(ch == ':')
                {
                    evt_rec.prop[i] = '|';
                }
            }

            // Substitute all commas in the property value with single quotes
            // Comma is a character reserved by the communication protocol

            for(i = 0; i < FGC_LOG_EVT_VAL_LEN; i++)
            {
                ch = evt_rec.value[i];

                if(ch == '\0')
                {
                    break;
                }
                else if(ch == ',')
                {
                    evt_rec.value[i] = '\047';
                }
            }

            // Always put a null as the last action character. This is for compatibility with libevtlog,
            // where the last character is a record status and should be set to null or space

            evt_rec.action[FGC_LOG_EVT_ACT_LEN - 1] = '\0';

            fprintf(c->f, fmt, evt_rec.timestamp.unix_time,
                    evt_rec.timestamp.us_time,
                    (char *)evt_rec.prop,
                    (char *)evt_rec.value,
                    (char *)evt_rec.action);
            idx--;            
        }

        // Cyclic log

        n = FGC_CYCLE_LOG_LEN;

        // Assume that getting the index is an atomic operation. Otherwise it would
        // need to be protected against race conditions.
        // cycle_log.index points at the last log inserted

        idx = LogCycleGetIdx();

        while (n--)
        {
            offset = sizeof(struct log_evt_rec) * idx;

            // Do we really need to protect the code below?

            OS_ENTER_CRITICAL();
            memcpy(&evt_rec, (char *) SRAM_LOG_CYC_EVT_32 + offset, sizeof(struct log_evt_rec));
            OS_EXIT_CRITICAL();

            // Check if the end of the buffer has been reached.
            if (!evt_rec.timestamp.unix_time)
            {
                break;
            }

            // If sending to the terminal, clip at 20 chars for prop name.
            if (neat_f)
            {
                evt_rec.prop[20] = '\0';
            }

            errnum = CmdPrintIdx(c, c->from++);

            if (errnum != 0)
            {
                break;
            }

            evt_rec.prop[FGC_LOG_EVT_PROP_LEN - 1] = '\0';
            evt_rec.value[FGC_LOG_EVT_VAL_LEN - 1] = '\0';
            evt_rec.action[FGC_LOG_EVT_ACT_LEN - 1] = '\0';

            fprintf(c->f, fmt,
                    evt_rec.timestamp.unix_time,
                    (evt_rec.timestamp.us_time),
                    (char *)evt_rec.prop,
                    (char *)evt_rec.value,
                    (char *)evt_rec.action);

            if (--idx < 0)
            {
                idx += FGC_CYCLE_LOG_LEN;
            }
        } 
    }    

    return (errnum);
}


INT16U GetLogPmSpy(struct cmd * c, struct prop * p)
{
    /*
     *   Displays: Post mortem LOG properties in binary in fgc_log.h format
     *   for Spy. This get function only returns data for commands via the
     *   WorldFIP with the BIN get option set.
     *
     *  Interfaces such as FGCspy can read from up to 10 FGCs on a FIP segment
     *  simultaneously. To achieve synchronisation, the fbs.log_sync_time
     *  variable is used. This is set to the current unix time in
     *  FbsProcessTimeVar() if the FGC_SYNC_LOG bit of the flags word of the
     *  FIP time variable makes a transition from 0 to 1. The value of
     *  fbs.log_sync_time is cleared to zero when this bit makes a transition
     *  from 1 to 0. The bit is controlled by the gateway property FGC.SYNC_LOGS.
     *  Setting the property will set the bit.
     *
     *  The synchronisation of the logs depends on the log being read:
     *
     *  Log   Fill time    Min Read time  KB   Twait   Comment
     *  -----------------------------------------------------------------------------------------
     *  IAB        8.192s      10.925s   64.0  2.681s  This is a fudge factor - no real wait
     *  IEARTH    20.480s       0.685s    4.0  2.500s
     *  ILEADS  1024.000s       2.050s   12.0  2.500s
     *  IREG      38.400s      10.242s   60.0  0.000s
     *  THOUR   3600.000s       0.482s    2.8  2.200s
     *  TDAY   91200.000s       0.203s    1.2  2.200s
     *
     *  The temperature logs (THOUR, TDAY) are treated slightly differently to the others because
     *  they fill so slowly (10s or 600s periods).  The synchronisation delay is completed before
     *  taking the last sample time/index.  For the others, a more complicated calculation is
     *  needed to determine which will be the first/last samples read out so as to avoid crossing
     *  the filling line.  This is then followed by the synchronisation delay for IEARTH and ILEADS
     *  but not for the others.  IAB is the most demanding buffer because it fills faster than it
     *  is read out, and for this the same calculation works by setting the wait time to a
     *  particular value (2681) but without this actually resulting in a sync delay of 2.6s.  This
     *  is signalled by making the wait time negative (-2681).
     */

    OS_CPU_SR           cpu_sr;
    INT16U              s;
    INT16U              n_sigs;
    INT16U              n_samples;
    INT16U              sample_size;
    INT16U              sample_idx;
    INT16U              sig_type;
    INT16S              twait_ms;
    INT32U              d_unix_time;
    INT16U              dTsync_ms;
    INT16U              dTlast_ms;
    INT16U              dIdx;
    INT8U       *       bin_data;
    char        *       addr;
    INT16U              last_sample_idx;
    struct abs_time_us  last_sample_time;
    INT16U              last_sample_ms_time;
    INT16U              errnum;
    prop_size_t         n;
    char                ch;
    const char     *    from;
    char        *       to;
    struct log_ana_vars    *    log;
    struct abs_time_ms          sync_time;
    FP32                        values[LOG_MAX_SIGS];   // Buffer must be size of largest log sample
    union                   // Union to reduce impact on stack
    {
        struct fgc_log_header       log;
        struct fgc_log_sig_header   sig;
    } header;

    /*--- Check that Get options and array specifiers are valid for a DIM log ---*/

    if (c->n_arr_spec || c->step > 1)       // If array indecies are specified or step > 1
    {
        return (FGC_BAD_ARRAY_IDX);         // Report error
    }

    if (!Test(c->getopts, GET_OPT_BIN))     // If BIN get option not specified
    {
        return (FGC_BAD_GET_OPT);           // Report bad get option
    }

    errnum = CmdPrepareGet(c, p, 1, &n);    // Handle standard print options

    if (errnum)
    {
        return (errnum);
    }

    /*--- Calculate synchronisation according to type of log ---*/

    log = (struct log_ana_vars *)p->value;  // Get address of log structure for this property
    n_sigs      = log->n_signals;       // Get local copy of number of signals
    n_samples   = log->n_samples;       // Get length of buffer in samples
    sample_size = log->sample_size;     // Get local copy of sample size in bytes

    if (log->type == STP_THOUR ||       // THOUR log    (2880B 0.1 Hz data) or
        log->type == STP_TDAY)           // TDAY  log    (1152B 10 min data)
    {
        CmdWaitUntil(((fbs.log_sync_time ?              // Wait now for 2.2 seconds to be sure
                       fbs.log_sync_time :              // not to collide with acquisition of
                       TimeGetUtcTime()) + 2), 200);         // temperatures (on 10s boundaries)

        last_sample_idx          = log->last_sample_idx;        // Get most recent entry index
        last_sample_time         = log->last_sample_time;       // and time stamp
        last_sample_time.us_time = 0;
        last_sample_ms_time      = 0;
        sync_time.unix_time      = 0;                   // No extra sync delay
        sig_type = FGC_LOG_TYPE_INT16U;                 // Temps are 16-bit unsigned shorts
    }
    else                    // All other logs
    {
        sig_type = FGC_LOG_TYPE_FLOAT;                  // All other logs are floats
        sync_time.unix_time = 0;                    // No sync delay by default

        OS_ENTER_CRITICAL();                        // Protect against interrupts
        last_sample_idx       = log->last_sample_idx;           // Get most recent entry index
        last_sample_time      = log->last_sample_time;                  // and time stamp
        OS_EXIT_CRITICAL();
        last_sample_ms_time   = log->last_sample_time.us_time / 1000;

        if (log->samples_to_acq)            // If log running
        {
            sync_time.unix_time = fbs.log_sync_time;

            if (!sync_time.unix_time)                   // If sync time is not active
            {
                sync_time.unix_time = last_sample_time.unix_time;   // Set sync time to start
                dTsync_ms = last_sample_ms_time;            // of current second
            }
            else                        // else sync is active
            {
                d_unix_time = last_sample_time.unix_time - sync_time.unix_time; // This must be done in 2 steps to
                dTsync_ms   = (INT16U)d_unix_time * 1000           //  ensure 32-bit subtraction
                              + last_sample_ms_time;               // !!! Do not merge these lines !!!
            }

            twait_ms = log->wait_time_ms;

            if (twait_ms  > 0)                                  // If wait time is positive
            {
                sync_time.unix_time += twait_ms / 1000;     // Prepare to wait for the specified time
                sync_time.ms_time    = twait_ms % 1000;
            }
            else                    // else time is zero or negative
            {
                twait_ms = -log->wait_time_ms;          // Abs value should be used in calculation
                sync_time.unix_time = 0;            // but not real delay required
            }

            dIdx      = (((INT16U)log->bufsize_w) / 6 + twait_ms - dTsync_ms) / log->period_ms;
            dTlast_ms = dIdx * log->period_ms;

            last_sample_idx       += dIdx;          // Adjust index to point to last sample
            last_sample_time.unix_time += dTlast_ms / 1000; // Adjust time to be of last sample
            last_sample_ms_time   += dTlast_ms % 1000;

            if (last_sample_ms_time > 999)
            {
                last_sample_ms_time -= 1000;
                last_sample_time.unix_time++;
            }
        }
    }

    sample_idx = last_sample_idx;   // Prepare sample index (will be pre-incremented to first sample)

    /*--- Generate log header ---*/

    header.log.version   = FGC_LOG_VERSION;
    header.log.info      = (log->samples_to_acq ? 0 : FGC_LOG_BUF_INFO_STOPPED);
    header.log.n_signals = n_sigs;
    header.log.n_samples = n_samples;
    header.log.period_us = 1000L * (INT32U)log->period_ms;
    header.log.unix_time = last_sample_time.unix_time;
    header.log.us_time   = (INT32U)last_sample_ms_time * 1000L;

    CmdStartBin(c, p, (INT32U)(sizeof(header.log)) +        // Write Binary data header (including length)
                (INT32U)(sizeof(header.sig) * n_sigs) + log->bufsize_w * 2);

    FbsOutBuf((INT8U *) &header.log, sizeof(header.log), c);        // Write log header

    /*--- Generate signal headers ---*/

    header.sig.type           = sig_type;
    header.sig.us_time_offset = 0;
    header.sig.gain           = (sig_type == FGC_LOG_TYPE_INT16U ? (1.0 / ID_TEMP_GAIN_PER_C) : 1.0);
    header.sig.offset         = 0.0;

    for (s = 0; s < n_sigs; s++)
    {
        header.sig.info = log->sig_info[s];     // Set signal info (Step interpolation for REF sigs)

        from = log->sig_label_units[s];         // Signal label and units are concatenated with a
        to   = header.sig.label;            // colon (:) separator, e.g. "T_FGC_IN:C"
        header.sig.label_len = 0;           // Prepare to count label length

        while ((ch = *(from++)) != ':')         // Copy signal label up to colon (no null termination)
        {
            *(to++) = ch;
            header.sig.label_len++;
        }

        to = header.sig.units;
        header.sig.units_len = 0;           // Prepare to count label length

        while ((ch = *(from++)))            // Copy signal label up to null (no null termination)
        {
            *(to++) = ch;
            header.sig.units_len++;
        }

        FbsOutBuf((INT8U *) &header.sig, sizeof(header.sig), c);    // Write signal header
    }

    /*--- Wait for synchronisation if required ---*/

    if (sync_time.unix_time)                // If sync delay required
    {
        CmdWaitUntil(sync_time.unix_time, sync_time.ms_time);           // Sleep required delay time
    }

    /*--- Write data ---*/

    while (n_samples--)                 // Write each sample in binary to FIP stream
    {
        sample_idx = (sample_idx + 1) % log->n_samples;     // Pre-increment sample index
        addr       = (char *) log->baseaddr + sample_size * sample_idx;

        memcpy(&values, addr, sample_size); // Copy one sample from log to the stack

        s = sample_size;
        bin_data = (INT8U *)&values;

        while (s--)                     // Write bytes from the stack to the stream
        {
            c->store_cb(*(bin_data++));
        }
    }

    return (0);
}


INT16U GetMemDsp(struct cmd * c, struct prop * p)
{
    prop_size_t n;                  // Number of array elements to display
    INT32U  addr;                   // Address in DSP memory
    INT32U  value_int;              // Integer read from memory
    FP32    value_float;                // Float read from memory
    INT32U   *  addr_int_p;             // Address to memory as int
    FP32  * addr_fp_p;              // Address to memory as float
    INT16U      errnum;

    if (c->n_arr_spec)                  // If array indexes given
    {
        return (FGC_BAD_ARRAY_IDX);             // Report BAD_ARRAY_IDX
    }

    addr = dpcom.mcu.debug_mem_dsp_addr;        // Address in DSP memory
    addr_int_p = (INT32U *) &dpcom.dsp.debug_mem_dsp_i;     // Address of data in DPCOM zone
    addr_fp_p  = (FP32 *)   &dpcom.dsp.debug_mem_dsp_f;     // Address of data in DPCOM zone

    c->to = DB_DSP_MEM_LEN;             // Set number elements

    p->n_elements = c->to;              // Set n_elements to fool CmdPrepareGet()

    errnum = CmdPrepareGet(c, p, 2, &n);        // Prepare to report 2 32-bit words per line

    if (errnum)
    {
        return (errnum);
    }

    p->n_elements = p->max_elements;            // Restore n_elements

    while (n--)             // For all elements to show
    {
        if (c->abort_f)             // If command was aborted
        {
            return (FGC_ABORTED);           // Report ABORTED
        }

        value_float = *(addr_fp_p++);           // Get float from DPCOM
        value_int   = *(addr_int_p++);          // Get integer from DPCOM

        if ((c->token_delim == ' ') &&      // If not comma delimiter and
            !(c->line % c->linelen))      // it's time for a new line
        {
            if (Test(c->getopts, GET_OPT_IDX))      // If Index required
            {
                fprintf(c->f, "\n0x%05lX:", addr);      // Write address
            }
            else if (c->line)               // else if not the first element
            {
                fputc('\n', c->f);              // Write newline
            }
        }
        else if (c->line)           // else if delimiter required
        {
            fputc(c->token_delim, c->f);        // Write delimiter character (space or comma)
        }

        c->line++;

        fprintf(c->f, "%13.5E:0x%08lX", (double) value_float, value_int); // Write int and float values in ASCII

        addr += 4;
    }

    return (0);
}


INT16U GetMemMcu(struct cmd * c, struct prop * p)
{
    prop_size_t n;                  // Number of array elements to display
    INT16U  value;                  // Word read from memory
    INT32U  addr;                   // Address to memory
    INT32U  max_n_words;                // Max num of words (to end of 20 bit address space)
    INT16U      errnum;

    if (c->n_arr_spec || c->step > 1)           // If array indexes given or step > 1
    {
        return (FGC_BAD_ARRAY_IDX);             // Report BAD_ARRAY_IDX
    }

    if (!debug.mem[1])                  // If n_elements is zero
    {
        debug.mem[1] = 128;                 // Set 128 words by default
    }

    addr = debug.mem[0] & (0xFFFFFFFE);         // Round address to word boundary
    max_n_words = (0x100000 - addr) / 2;        // Max number of words to end of 20-bit address space

    if (debug.mem[1] < max_n_words)         // If user supplied number of words is less than max
    {
        max_n_words = debug.mem[1];             // Use user supplied number of words
    }

    if (Test(c->getopts, GET_OPT_BIN))          // if BIN get option flag is set
    {
        errnum = CmdPrepareGet(c, p, 8, &n);            // Prepare to report 8 words per line

        if (errnum)
        {
            return (errnum);
        }

        n = (max_n_words < 0x8000 ? max_n_words : 0x8000);  // Clip number of words to 32K (64KB)

        CmdStartBin(c, p, (INT32U)n * sizeof(INT16U));      // Start binary transmission
    }
    else
    {
        c->to = (max_n_words < 512 ? max_n_words : 512);    // Clip number of words to 512 (1KB)

        p->n_elements = c->to;                  // Set n_elements to fool CmdPrepareGet()

        errnum = CmdPrepareGet(c, p, 8, &n);            // Prepare to report 8 words per line

        if (errnum)
        {
            return (errnum);
        }

        p->n_elements = p->max_elements;            // Restore n_elements
    }

    while (n--)             // For all words to show
    {
        value = *(INT16U *) addr;

        if (Test(c->getopts, GET_OPT_BIN))  // If BIN get option
        {
            FbsOutShort((INT8U *)&value, c);        // Write word in binary
        }
        else                    // else (ASCII)
        {
            if (c->abort_f)             // If command was aborted
            {
                return (FGC_ABORTED);               // Report ABORTED
            }

            if ((c->token_delim == ' ') &&      // If not comma delimiter and
                !(c->line % c->linelen))          // it's time for a new line
            {
                if (Test(c->getopts, GET_OPT_IDX))          // If Index required
                {
                    fprintf(c->f, "\n0x%05lX:", addr);          // Write address
                }
                else    // else if not the first element
                {
                    if (c->line)
                    {
                        fputc('\n', c->f);                  // Write newline
                    }
                }
            }
            else // else if delimiter required
            {
                if (c->line)
                {
                    fputc(c->token_delim, c->f);        // Write delimiter character (space or comma)
                }
            }

            c->line++;

            fprintf(c->f, "0x%04X", value);     // Write word in ASCII using HEX
        }

        addr += 2;              // Advance address to next word
    }

    return (0);
}


INT16U GetParent(struct cmd * c, struct prop * p)
{
    return (CmdParentGet(c, p, PARENT_GET_NOT_HIDDEN));
}


void GetPropInfo(struct cmd * c, struct prop * p)
{
    INT16U              dsp_status = FGC_OK_NO_RSP;

    struct TInternalInfoFmt
    {
        char      *     label;
        char      *     valfmt;
    };


    const char               *              fmt;
    static const char           *           lblfmt[] = { "%-16s", "%s" };
    static const struct TInternalInfoFmt    info[] =
    {
        { "struct prop *:",     "0x%08X\n"      },      // 0
        { "sym_idx:",       "%u %s\n"   },              // 1
        { "flags:",     "0x%04X\n"  },                  // 2
        { "type:",      "%u %s\n"   },                  // 3
        { "setif_func_idx:",    "%u 0x%05lX\n"  },      // 4
        { "set_func_idx:",  "%u 0x%05lX\n"  },          // 5
        { "get_func_idx:",  "%u 0x%05lX\n"  },          // 6
        { "n_elements:",    "%u"        },              // 7
        { "max_elements:",  "%u\n"      },              // 8
        { "range:",     "0x%04X\n"  },                  // 9
        { "value:",     "0x%04X\n"  },                  // 10
        { "dsp_idx:",       "%u\n"      },              // 11
        { "nvs_idx:",       "0x%02X\n"  },              // 12
        { "sub_idx:",       "%u\n"          },          // 13
        { "dynflags:",      "0x%02X\n"  },              // 14
        { "parent *:",          "0x%08X\n"      },      // 15
    };


    if (Test(p->flags, PF_DSP_FGC))
    {
        dsp_status = PropValueGet(c, p, NULL, 0, NULL);// Get n_element if DSP property
    }

    CmdPrintTimestamp(c, p);                         // Produce timestamp

    fmt = lblfmt[c->token_delim == ','];

    fprintf(c->f, fmt, info[0].label);                  // Property pointer
    fprintf(c->f, info[0].valfmt, p);

    fprintf(c->f, fmt, info[15].label);                 // Parent pointer
    fprintf(c->f, info[15].valfmt, p->parent);

    fprintf(c->f, fmt, info[1].label);                  // Symbol index (and symbol)
    fprintf(c->f, info[2].valfmt, p->sym_idx, SYM_TAB_PROP[p->sym_idx].key.c);

    fprintf(c->f, fmt, info[2].label);                  // Flags
    fprintf(c->f, info[2].valfmt, p->flags);

    fprintf(c->f, fmt, info[14].label);                 // Dynamic flags
    fprintf(c->f, info[14].valfmt, p->dynflags);

    fprintf(c->f, fmt, info[3].label);                  // Property type
    fprintf(c->f, info[3].valfmt, p->type, prop_type_name[p->type]);

    fprintf(c->f, fmt, info[4].label);                  // Setif function
    fprintf(c->f, info[4].valfmt, p->setif_func_idx, (INT32U) SETIF_FUNC[p->setif_func_idx]);

    fprintf(c->f, fmt, info[5].label);                  // Set function
    fprintf(c->f, info[5].valfmt, p->set_func_idx, (INT32U) SET_FUNC[p->set_func_idx]);

    fprintf(c->f, fmt, info[6].label);                  // Get function
    fprintf(c->f, info[6].valfmt, p->get_func_idx, (INT32U) GET_FUNC[p->get_func_idx]);

    fprintf(c->f, fmt, info[7].label);                  // Number of elements

    if (dsp_status == FGC_DSP_NOT_AVL)
    {
        fprintf(c->f, "%s", fgc_errmsg[dsp_status]);
    }
    else
    {
        fprintf(c->f, info[7].valfmt, PropGetNumEls(c, p));

        if (Test(p->flags, PF_INDIRECT_N_ELS))              // Indirect number of elements
        {
            fprintf(c->f, " (0x%08X)", (unsigned int)p->n_elements);
        }
    }

    fputc('\n', c->f);

    fprintf(c->f, fmt, info[8].label);                  // Max elements
    fprintf(c->f, info[8].valfmt, p->max_elements);

    fprintf(c->f, fmt, info[9].label);                  // Range
    fprintf(c->f, info[9].valfmt, p->range);

    fprintf(c->f, fmt, info[10].label);                 // Value pointer
    fprintf(c->f, info[10].valfmt, p->value);

    fprintf(c->f, fmt, info[11].label);                 // DSP property index
    fprintf(c->f, info[11].valfmt, p->dsp_idx);

    fprintf(c->f, fmt, info[12].label);                 // NVS index
    fprintf(c->f, info[12].valfmt, p->nvs_idx);

    fprintf(c->f, fmt, info[13].label);                 // Subscription index
    fprintf(c->f, info[13].valfmt, p->sub_idx);
}


INT16U GetPropSize(struct cmd * c, struct prop * p)

{
    INT16U              i;
    prop_size_t         size[3];
    const char     *    fmt;
    static const char  *  decfmt[] = { "%13u", "%u" };
    static const char  *  hexfmt[] = { "       0x%08X", "0x%08X" };
    INT16U              errnum;

    if (Test(p->flags, PF_DSP_FGC))
    {
        errnum = PropValueGet(c, p, NULL, 0, NULL);     // Get n_element if DSP property

        if (errnum)
        {
            return (errnum);
        }
    }

    CmdPrintTimestamp(c, p);                            // Produce timestamp

    size[0] = (prop_size_t)prop_type_size[p->type]; // Get element size in bytes
    size[1] = PropGetNumEls(c, p);          // Get number of elements in property
    size[2] = p->max_elements;              // Get max number of elements in property

    fmt = (Test(c->getopts, GET_OPT_HEX) ? hexfmt[(c->token_delim == ',')] :    // Prepare format to respect
           decfmt[(c->token_delim == ',')]);     // Hex and Neat options

    if (Test(c->getopts, GET_OPT_LBL))          // If LBL option defined
    {
        CmdPrintLabel(c, p);            // Display property label
    }

    for (i = 0; i < 3; i++)             // For each element of size[]
    {
        if (i)                      // If not first element
        {
            fputc(c->token_delim, c->f);        // then write delimiter
        }

        fprintf(c->f, fmt, size[i]);            // Display size parameter
    }

    fputc('\n', c->f);                  // write newline

    return (FGC_OK_NO_RSP);
}



INT16U GetRawParameter(struct cmd * c, struct prop * p)
{
    BOOLEAN             bin_f;          // Binary get option active
    prop_size_t         n;              // Number of array elements to display
    INT16U              size;           // Integer size index (0-2)
    INT16U              type;           // Integer type index (0-5)
    INT32S              v32  = 0;       // 32-bit value
    INT16U              errnum;         // Error number
    enum scivs_err_code    scivs_err;      // Error number
    const char     *    fmt;            // Pointer to format string
    uint8_t slot, block, param; 
    enum scivs_card_device comp;

    static const char  *  hexfmt[]        = { "0x%02X", "0x%04X", "0x%08X" };
    static const char  *  hexneatfmt[]    = { "         0x%02X", "       0x%04X", "   0x%08X" };
    static const char  *  decfmt[]        = { "%u", "%d", "%u", "%d", "%u", "%d" };
    static const char  *  decneatfmt[]    = { "%13u", "%13d", "%13u", "%13d", "%13u", "%13d" };
    static INT16U       size_bytes[]    = { 1, 2, 4 };

    bin_f  = Test(c->getopts, GET_OPT_BIN);             // Prepare binary flag

    errnum = CmdPrepareGet(c, p, 4, &n);   // Handle standard print options

    if (errnum)
    {
        return (errnum);
    }

    if (!n)                                     // If no data
    {
        return (FGC_OK_NO_RSP);                         // Return immediately with OK status
    }

    size = 4 / 2;                // Prepare integer size index (0-2)

    switch (p->type)            // Prepare integer type index (0-5)
    {
        case PT_INT8U:  type = 0;   break;

        case PT_INT8S:  type = 1;   break;

        case PT_INT16U: type = 2;   break;

        case PT_INT16S: type = 3;   break;

        case PT_INT32U: type = 4;   break;

        case PT_INT32S: type = 5;   break;

        default:
            return (FGC_BAD_PARAMETER);
            break;
    }

    if (Test(c->getopts, GET_OPT_HEX))      // Prepare format to use for integer data
    {
        fmt = ((c->token_delim == ' ') ? hexneatfmt[size] : hexfmt[size]);
    }
    else
    {
        fmt = ((c->token_delim == ' ') ? decneatfmt[type] : decfmt[type]);
    }

    if (bin_f)
    {
        CmdStartBin(c, p, (INT32U)size_bytes[size] * (INT32U)n);
    }

    slot  = regfgc3_pars_handler.raw.slot;
    block = regfgc3_pars_handler.raw.block + (regfgc3_pars_handler.raw.access == FGC_ACCESS_RIGHTS_RO ? SCIVS_N_WRITE_BLOCKS : 0);

    param = c->from;
    comp = (p->sym_idx == STP_PARAM) ? SCIVS_MF : SCIVS_DEVICE_2;

    scivs_err = regFgc3ParsGetBlock(slot, comp, block);

    if (scivs_err != SCIVS_OK)
    {
        return (scivsConvertToFgcErr(scivs_err));
    }

    while (n--)             // For all elements in the array range
    {

        param = c->from;

        scivs_err = regFgc3ParsGetRawParameter(slot, comp, block, param, &v32);

        if ((scivs_err != SCIVS_OK) && (scivs_err != SCIVS_ERR_WRONG_PARAM_IDX))
        {
            return (scivsConvertToFgcErr(scivs_err));
        }

        if (bin_f)
        {
            FbsOutLong((char *) &v32, c);
        }

        if (!bin_f)
        {
            errnum = CmdPrintIdx(c, c->from);              // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            if (scivs_err == SCIVS_OK)
            {
                fprintf(c->f, fmt, v32);
            }
        }

        c->from += c->step;
    }

    return (0);
}



INT16U GetRawDspParameterFloat(struct cmd * c, struct prop * p)
{

    INT16U          format_idx = 0; // Format index for DSP
    static const char * decfmt[]   = { "", "%.5E", "%.7E", "%13.5E" };  // Max length is 12 chars
    static const char * hexfmt[]   = { "   0x%08X", "0x%X" };           // CmdPrintfDsp in DSP lib
    static const INT16U prec[] = {0, 5, 7, 5};
    static const INT16U flags[] = {0, UPPER | ETYPE, UPPER | ETYPE, UPPER | ETYPE};
    static const INT16S width[] = {0, 0, 0, 13};
    char                float_str[15];
    BOOLEAN             bin_f;          // Binary get option active
    BOOLEAN             hex_f;          // HEX get option active
    prop_size_t         n;              // Number of array elements to display
    FP32                value;          // Float value
    INT16U              errnum;         // Error number
    enum scivs_err_code    scivs_err;      // Error number
    const char     *    fmt;            // Pointer to format string
    uint8_t slot, block, param; 
    enum scivs_card_device comp;

    bin_f  = Test(c->getopts, GET_OPT_BIN);             // Prepare binary flag

    hex_f = Test(c->getopts, GET_OPT_HEX);              // Prepare hex flag

    if (hex_f)                                  // If HEX
    {
        fmt = hexfmt[c->token_delim == ','];
    }
    else                            // else DECIMAL
    {
        if (c->token_delim == ' ')                  // If NEAT
        {
            format_idx = 3;
        }
        else                                // else not NEAT
        {
            format_idx = 1 + !Test(p->flags, PF_NON_VOLATILE);
        }

        fmt = decfmt[format_idx];
    }

    errnum = CmdPrepareGet(c, p, 4, &n);   // Handle standard print options

    if (errnum)
    {
        return (errnum);
    }

    if (n && bin_f)
    {
        CmdStartBin(c, p, (INT32U)(sizeof(float)) * (INT32U)n);
    }

    slot  = regfgc3_pars_handler.raw.slot;
    block = regfgc3_pars_handler.raw.block + (regfgc3_pars_handler.raw.access == FGC_ACCESS_RIGHTS_RO ? SCIVS_N_WRITE_BLOCKS : 0);

    param = c->from;
    comp = (p->sym_idx == STP_PARAM) ? SCIVS_MF : SCIVS_DEVICE_2;

    scivs_err = regFgc3ParsGetBlock(slot, comp, block);

    if (scivs_err != SCIVS_OK)
    {
        return (scivsConvertToFgcErr(scivs_err));
    }

    while (n--)             // For all elements in the array range
    {
        param = c->from;

        scivs_err = regFgc3ParsGetRawParameter(slot, comp, block, param, (int32_t *) &value);

        if ((scivs_err != SCIVS_OK) && (scivs_err != SCIVS_ERR_WRONG_PARAM_IDX))
        {
            return (scivsConvertToFgcErr(scivs_err));
        }

        if (bin_f)
        {
            FbsOutLong((char *) &value, c);
        }
        else
        {
            errnum = CmdPrintIdx(c, c->from);              // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            if (hex_f)
            {
                // Display as 0xXXXXXXXX

                void * value_p = &value;
                fprintf(c->f, fmt, *((INT32U *)value_p));
            }
            else
            {
                ftoa(float_str, value, prec[format_idx], flags[format_idx], width[format_idx]);
                fputs(float_str, c->f);
            }
        }

        c->from += c->step;
    }

    return (0);
}


INT16U GetRegFgc3SlotInfo(struct cmd * c, struct prop * p)
{
    prop_size_t n;
    uint16_t errnum;
    char delim;
   
    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    delim = (c->token_delim == ' ' ? '\n' : ',');
   
    regFgc3ProgSlotPrint(c->f, delim);

    return (0);
}


INT16U GetCache(struct cmd * c, struct prop * p)
{
    // Displays all parameters on all boards on REGFGC3 crate.

    char delim;
    prop_size_t n;
    uint16_t errnum;
   
    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    // Set delimiter
    delim = (c->token_delim == ' ' ? '\n' : ',');   
   
    regFgc3ParsPrint(c->f, delim, REGFGC3_PARS_PRINT_CACHE);
    
    return (0);
}


INT16U GetString(struct cmd * c, struct prop * p)
{
    // This supports scalar near strings and arrays of far strings.

    prop_size_t     n;
    INT16U      errnum;         // Error number
    INT16U      mux_offset;
    char * str;
    char ** strs;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    mux_offset = p->max_elements * c->mux_idx;

    if (p->max_elements == 1 && p->type != PT_DEV_NAME)
    {
        str  = ((char *)p->value) + mux_offset;   // Add address of scalar string
        strs = &str;                // Point array at this scalar
    }
    else                    // else array property
    {
        strs = ((char **)p->value) + (c->from + mux_offset);
    }

    while (n--)                 // For all elements in the array range
    {
        errnum = CmdPrintIdx(c, c->from);          // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        if (*strs)                  // If not a null pointer
        {
            fputs(*(strs), c->f);           // Display string
        }

        strs += c->step;
        c->from += c->step;
    }

    return (0);
}


INT16U GetSubTable(struct cmd * c, struct prop * p)
{
    // This debug property displays the subscription table.

    char        delim;
    prop_size_t     n;
    INT16U      errnum;         // Error number
    struct sub_table  * sub_table;
    INT8U cnt = 0;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    delim = (c->token_delim == ' ' ? '\n' : ',');                        // Set delimiter

    fprintf(c->f, "n_subs=%u n_direct_pubs=%u n_get_pubs=%u%c",         // Report top level sub variables
            sub.n_subs, sub.n_direct_pubs, sub.n_get_pubs, delim);

    fprintf(c->f, "free_sub_idx=%u new_sub_idx=%u pub_idx=%u flags=%x%c",
            sub.free_sub_idx, sub.new_sub_idx, sub.pub_idx, sub.flags, delim);

    fprintf(c->f, "pending_get.sub_idx=%u pending_get.user=%u%c",
            sub.pending_get.sub_idx, sub.pending_get.user, delim);

    fprintf(c->f, "nti=0x%04X 0x%04X 0x%04X %c",
            sub.notify_msk.master, sub.notify_msk.mask_array[0],
            sub.notify_msk.mask_array[1], delim);

    fprintf(c->f, "sub_acked_msk nti=0x%04X 0x%04X 0x%04X %c",
            sub.sub_acked_msk.master, sub.sub_acked_msk.mask_array[0],
            sub.sub_acked_msk.mask_array[1], delim);

    sub_table = &sub.table[c->from];

    while (n--)                 // For all elements in the array range
    {
        errnum = CmdPrintIdx(c, c->from);            // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        fprintf(c->f, "%2u idx=%2u prop*=0x%08lX notify_cnt=%u nti=0x%04X nix0=0x%04X %.6s",
                cnt++,
                sub_table->idx,
                (INT32U) sub_table->prop,
                sub_table->notify_count,
                sub_table->notify_msk.master,
                sub_table->notify_msk.mask_array[0],
                SYM_TAB_PROP[sub_table->prop->sym_idx].key.c);

        sub_table += c->step;
        c->from   += c->step;
    }

    return (0);
}


INT16U GetTemp(struct cmd * c, struct prop * p)
{
    prop_size_t         n;
    INT16U              hex_f;          // Hex get option flag
    INT16U              errnum;
    INT16U              temp;                   // Temperature
    INT16U       *      temp_ptr;
    const char     *    fmt;
    static const char * hexfmt[] = { "       0x%04X", "0x%04X" };
    static const char * decfmt[] = { "%10u.%02u", "%u.%02u" };

    errnum = CmdPrepareGet(c, p, 4, &n);

    if (errnum)
    {
        return (errnum);
    }

    hex_f =  Test(c->getopts, GET_OPT_HEX);
    fmt   = (hex_f ? hexfmt[(c->token_delim == ',')] : decfmt[(c->token_delim == ',')]);

    while (n--)                 // For all elements in the array range
    {
        errnum = PropValueGet(c, p, NULL, c->from, (INT8U **)&temp_ptr);

        if (errnum)
        {
            return (errnum);
        }

        temp = *temp_ptr;

        errnum = CmdPrintIdx(c, c->from);          // Print index if required
        c->from++;

        if (errnum)
        {
            return (errnum);
        }

        if (hex_f)
        {
            fprintf(c->f, fmt, temp);                             // Display temperature
        }
        else
        {
            if (c->si_idx == c->n_prop_name_lvls)       // If temperature got individually (not via parent)
            {
                if (!temp)                  // If no temperature
                {
                    return (FGC_NO_TEMP);
                }
                else
                {
                    if (temp == (ID_BAD_TEMP * ID_TEMP_GAIN_PER_C))
                    {
                        return (FGC_BAD_TEMP);
                    }
                }
            }
            else                // else if getting parent
            {
                if (temp == (ID_BAD_TEMP * ID_TEMP_GAIN_PER_C))
                {
                    temp = 0;
                }
            }

            fprintf(c->f, fmt, (temp >> 4), ID_TEMP_FRAC_C(temp));  // Display temperature
        }
    }

    return (0);
}


INT16U GetLog(struct cmd * c, struct prop * p)
{
    /*
     * The index and size used to access those PPM properties are set by the
     * DSP in structure dpcls.dsp.log.oasis. The log buffer on the DSP sid
     *  is a circular buffer, common to all users. This function must cope
     *  wit and in particular rearrange the c->from and c->to indexes
     *
     *   With start_idx = dpcls.dsp.log.oasis.start_idx[user] an
     *   size = dpcls.dsp.log.oasis.size[user], we hav:
     *
     *     MCU SIDE                               DSP SID
     *     ========                               =======
     *     LOG.OASIS.I_REF.DATA(user)[0] ~=o log_oasis_buf.i_ref[start_idx
     *     LOG.OASIS.I_REF.DATA(user)    ~=  log_oasis_buf.i_ref[start_idx; start_idx+size[
     */

    BOOLEAN             bin_f;                  // Binary get option active
    BOOLEAN             set_f = FALSE;          // Read-only property
    prop_size_t         n;                      // Number of element in array range
    INT32U              log_size;
    INT32U              log_start_idx;
    INT16U              format_idx;             // Format index for DSP
    FP32        *       value_p;                // Pointer to float value
    FP32                value;                  // Float value
    INT16U              errnum;                 // Error number
    BOOLEAN             valid_read_f;
    SM_LOCK             lock_val;
    char                float_str[15];

    // Equivalent printf formats are { "","%.5E","%.7E","%13.5E" }
    static const INT16U prec[] = {0, 5, 7, 5};
    static const INT16U flags[] = {0, UPPER | ETYPE, UPPER | ETYPE, UPPER | ETYPE};
    static const INT16S width[] = {0, 0, 0, 13};

    bin_f = Test(c->getopts, GET_OPT_BIN);              // Prepare binary flag

    if (c->token_delim == ' ')                                 // If NEAT
    {
        format_idx = 3;
    }
    else                                                    // else not NEAT
    {
        format_idx = 1 + !Test(p->flags, PF_NON_VOLATILE);
    }

    // Start read protection (data written asynchronously by the DSP in shared memory)

    lock_val = sm_lock_read_poll(&dpcls.dsp.log.oasis.lock);

    do
    {

        // Read LOG timestamp (and append it to the response)

        CmdPrintTimestamp(c, p);

        // Copy log start_idx & size for the specified user

        log_size      = dpcls.dsp.log.oasis.size[c->mux_idx];           // Read LOG segment size
        log_start_idx = dpcls.dsp.log.oasis.start_idx[c->mux_idx];      // Read LOG segment start location

        valid_read_f = sm_lock_read_check(&dpcls.dsp.log.oasis.lock, lock_val);
    }
    while (!valid_read_f);                                      //  End read protection

    errnum = CmdPrepareGet(c, p, 4, &n);                        // Ignore the n returned by CmdPrepareGet()

    if (errnum)
    {
        return (errnum);
    }

    // force user 0 because the log buffer is a fake PPM property. There is one buffer common to all users.

    c->mux_idx = 0;

    // max number of elements

    n = log_size / c->step;

    // Rearrange c->from and c->to indexes:
    //  - Keep indexes in a range lower than the oasis buffer size

    if (c->to < c->from || c->from > log_size)
    {
        n = c->from = c->to = 0;
    }
    else
    {
        if (c->to < c->from + log_size)
        {
            n = (c->to - c->from + c->step) / c->step;
        }
    }

    //  - Shift them by start_idx

    c->from += log_start_idx;

    if (c->from >= p->max_elements)                // LOG.OASIS buffer is a circular buffer
    {
        c->from -= p->max_elements;
    }

    c->to += log_start_idx;

    if (c->to >= p->max_elements)                  // LOG.OASIS buffer is a circular buffer
    {
        c->to -= p->max_elements;
    }

    if (n && bin_f)
    {
        CmdStartBin(c, p, (INT32U)(sizeof(float)) * (INT32U)n);
    }

    while (n--)   // For all elements
    {

        errnum = PropValueGet(c, p, &set_f, c->from, (INT8U **)&value_p);

        if (errnum != 0)   // Get element value from property
        {
            return (errnum);
        }

        value = *value_p;

        if (bin_f)
        {
            FbsOutLong((char *) &value, c);
        }
        else
        {
            errnum = CmdPrintIdx(c, c->from - log_start_idx);                // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            ftoa(float_str, value, prec[format_idx], flags[format_idx], width[format_idx]);
            fputs(float_str, c->f);

        }

        c->from += c->step;

        if (c->from >= p->max_elements)                // LOG.OASIS buffer is a circular buffer
        {
            c->from -= p->max_elements;
        }
    }

    return (0);
}


INT16U GetLogCaptureSig(struct cmd * c, struct prop * p)
{
    /*
     * The PPM log buffer (LOG.CAPTURE.BUF) contains up to 7200 records of
     * 9 floats.  These signals are:
     *     0   0x0001      REF
     *     1   0x0002      V_REF
     *     2   0x0004      B_MEAS
     *     3   0x0008      I_MEAS
     *     4   0x0010      V_MEAS
     *     5   0x0020      ERR
     *     6   0x0040      Signal from LOG.SPY.MPX[0]
     *     7   0x0080      Signal from LOG.SPY.MPX[1]
     *     8   0x0100      Signal from LOG.SPY.MPX[2]
     *
     * Properties are provided to give an application access to the first six
     * only. The value of the property is the offset to the signal (0-5).
     */

    prop_size_t n;
    INT16U      errnum;
    prop_size_t sample_idx;
    INT16U      sample_step;
    FP32    *   f_p;                    // Pointer to float value
    const char * fmt = NULL;            // Pointer to format string

    static const char  * decfmt[]   = { "%.7E", "%13.5E" };
    static const char  * hexfmt[]   = { "   0x%08lX", "0x%lX" };


    errnum = CmdPrepareGet(c, p, 4, &n);

    if (errnum)
    {
        return (errnum);
    }

    // Check that Get command and options are valid for a capture log signal

    if (Test(c->getopts, GET_OPT_BIN))          // If BIN get option
    {
        if (!dpcls.dsp.log.capture.n_samples)           // If no log data available
        {
            return (0);                                         // Return nothing
        }

        n = dpcls.dsp.log.capture.n_samples;            // Get number of samples and

        // value is defined as a pointer so for RX601 is INT32U
        // but here used as INT16U
        // ToDo : use an union for this shared space

        sample_idx  = (prop_size_t)p->value;                 // offset to required signal (from property value)

        CmdStartBin(c, p, (INT32U)(sizeof(float) * n));
    }
    else                                        // else ASCII
    {
        if (Test(c->getopts, GET_OPT_HEX))              // If HEX
        {
            fmt = hexfmt[c->token_delim == ','];
        }
        else                                            // else DECIMAL
        {
            fmt = decfmt[c->token_delim == ' '];
        }

        // value is defined as a pointer so for RX601 is INT32U
        // but here used as INT16U
        // ToDo : use an union for this shared space

        sample_idx = c->from * FGC_LOG_CAPTURE_N_SIGS + (prop_size_t)p->value;
    }

    sample_step = FGC_LOG_CAPTURE_N_SIGS * c->step;

    while (n--)                                 // For all elements in the array range
    {
        errnum = PropValueGet(c, &PROP_LOG_CAPTURE_BUF, NULL, sample_idx, (INT8U **)&f_p);

        if (errnum)
        {
            return (errnum);
        }

        if (Test(c->getopts, GET_OPT_BIN))              // If BIN get option
        {
            FbsOutLong((char *) f_p, c);                // Send in binary
        }
        else                                            // else not binary
        {
            errnum = CmdPrintIdx(c, c->from);           // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            fprintf(c->f, fmt, *f_p);                   // Display as x.xxxxxxEyy or 0xXXXXXXXX

            c->from += c->step;
        }

        sample_idx += sample_step;
    }

    return (0);
}


INT16U GetSemInfo(struct cmd * c, struct prop * p)
{
    OS_SEM * current_sem = fcm.sem;
    INT8U  i;

    for (i = 0; i < FGC_NUM_SEMAPHORES; ++current_sem, ++i)
    {
        task_trace.sem_info[i] = (current_sem->pending << 16) + current_sem->counter;\
    }

    return (GetInteger(c, p));
}


uint16_t GetDim(struct cmd * c, struct prop * p)
{
    uint16_t  errnum = FGC_OK_NO_RSP;

    switch (c->sym_idxs[1])
    {
        case STP_DIM:
            errnum = DimGetDimLog(c, p); // G LOG.DIM.{NAME}
            break;

        case STP_DIM_NAMES:
            errnum = DimGetDimNames(c, p);  // G DIAG.DIM_NAMES
            break;

        case STP_ANA_LBLS:
            errnum = DimGetAnaLbls(c, p);   // G DIAG.ANA_LBLS.{NAME}
            break;

        case STP_DIG_LBLS:
            errnum = DimGetDigLbls(c, p, 0); // G DIAG.DIG_LBLS.{NAME}
            break;

        case STP_FAULTS:
            errnum = DimGetFaults(c, p);    // G DIAG.FAULTS
            break;

        case STP_ANA:
            errnum = DimGetAna(c, p);       // G DIAG.ANA.{NAME}
            break;

        case STP_DIG:
            errnum = DimGetDig(c, p);       // G DIAG.DIG.{NAME}
            break;

        default:
            Crash(0xBAD7);
    }

    return (errnum);
}

// Internal function definitions

static INT16U GetComp(struct cmd * c, INT16U * list_idx, INT16U n_found, INT16U n_req, fgc_db_t comp_idx)
{
    INT16U  errnum;                 // Error number
    BOOLEAN neat_f;                 // Neat get option flag
    char    comp_label[FGC_COMPDB_COMP_LBL_LEN + 2];    // Buffer to receive the component description

    neat_f = (c->token_delim == ' ');

    // Display found/req before first element

    if (! *list_idx)                            // If first element in list
    {
        errnum = CmdPrintIdx(c, c->from++);

        if (errnum)                                 // Print index if required
        {
            return (errnum);
        }

        fprintf(c->f, "%02u/%02u", n_found, n_req);
    }
    else
    {
        if (neat_f)
        {
            fputs("\n     ", c->f);         // Add spaces to line up colunns
        }
    }

    fputc(((*list_idx)++ ? '\\' : '|'), c->f);      // Add list element delimiter

    fprintf(c->f, (neat_f ? "%10s = " : "%s="), CompDbType(comp_idx));

    memcpy(&comp_label, CompDbLabel(comp_idx), sizeof(comp_label));

    if (neat_f && comp_label[61])
    {
        comp_label[58] = '.';
        comp_label[59] = '.';
        comp_label[60] = '.';
        comp_label[61] = '\0';
    }

    fputs(comp_label, c->f);

    return (0);
}


#if (FGC_CLASS_ID != 61)
static bool isFgcMac(const struct mac * mac)
{
    static const uint8_t fgc_mac_prefix[5] = { 0x02, 0x00, 0x00, 0x00, 0x00 };

    return (memcmp(mac->octets, fgc_mac_prefix, 5) == 0 && mac->octets[5] < 65);
}



static inline void printMacAsString(FILE * f, const struct mac * mac)
{
    fprintf(f, "%02X-%02X-%02X-%02X-%02X-%02X",
            mac->octets[0],
            mac->octets[1],
            mac->octets[2],
            mac->octets[3],
            mac->octets[4],
            mac->octets[5]);
}



INT16U GetMac(struct cmd * c, struct prop * p)
{
    prop_size_t n;
    uint16_t    errnum;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if(errnum != 0)
    {
        return errnum;
    }

    const bool zero_f = (Test(p->flags, PF_GET_ZERO) && Test(c->getopts, GET_OPT_ZERO));

    struct mac * mac = p->value;
    uint16_t     i   = c->from;

    while(n--)
    {
        // Print index if required

        errnum = CmdPrintIdx(c, i);

        if(errnum != 0)
        {
            return errnum;
        }

        // Check if MAC address belongs to an FGC

        if(isFgcMac(mac))
        {
            uint16_t     name_db_index = mac->octets[5];
            const char * name          = nameDbGetName(name_db_index);

            // If there's a name in NameDb that corresponds to the FGC dongle number,
            // then print it instead of MAC address. Otherwise, print the MAC directly.

            if(name != NULL)
            {
                fputs(name, c->f);
            }
            else
            {
                printMacAsString(c->f, mac);
            }
        }
        else
        {
            // Non-FGC devices aren't in NameDb, so print the MAC directly

            printMacAsString(c->f, mac);
        }

        // Clear the value if zero flag present

        if(zero_f)
        {
            memset(mac, 0, NUM_MAC_OCTETS);
        }

        // Advance to the next element

        mac += c->step;
        i++;
    }

    if(zero_f)
    {
        p->n_elements = 0;
    }

    return 0;
}
#endif

// EOF
