/*---------------------------------------------------------------------------------------------------------*\
  File:         cal.c

  Purpose:      Automatic ADC/DAC calibration Functions

  Author:       Quentin.King@cern.ch

  Notes:        This file contains functions which support the automatic calibration of the
                ADCs, DCCTs and DAC.
\*---------------------------------------------------------------------------------------------------------*/

#define CAL_GLOBALS       // define cal global variable

#include <cal.h>
#include <cmd.h>
#include <nvs.h>
#include <fbs_class.h>          // for STATE_OP, ST_MEAS_A, ST_MEAS_B
#include <defprops.h>
#include <dev.h>                // for dev global variable
#include <prop.h>               // for PropSetNumEls()
#include <mst.h>                // for mst global variable
#include <dpcom.h>              // for dpcom global variable
#include <sta.h>                // for sta global variable
#include <dpcls.h>              // for dpcls global variable
#include <dls.h>                // for temp  global variable
#include <macros.h>             // for Set(), Clr()
#include <fbs.h>                // for fbs global variable
#include <memmap_mcu.h>         // for


//-----------------------------------------------------------------------------------------------------------
// Static functions and global variables
//

static BOOLEAN CalSendRequestToDsp(void);
static void    CalSetMeasMask(void);
static void    CalClearMeasMask(void);

// The next calibration request to be sent to the DSP
// CAUTION: Right now we assume there is only one active cal request at a time (it basically means that the
// end user can only do one S CAL command at a time, and wait for the command to complete before sending a
// new one.)

static struct cal_req cal_request_to_dsp;

// Pointer to the calibration sequence being executed (NULL pointer means no calibration sequence on-going)

static const struct cal_seq * current_cal_sequence;


/*---------------------------------------------------------------------------------------------------------*/
void CalInitSequence(const struct cal_seq * cal_sequence, INT32U chan_mask, INT32S signal)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to initiate a new calibration.
  If the input cal_sequence is not NULL, and STATE_OP is not FGC_OP_CALIBRATING, then this function will:

      - Set the calibration sequence pointer to the one given in input (cal_sequence)
      - Set the channel mask = chan_mask
      - Set the signal id (CAL_IDX_OFFSET/CALPOS/CALNEG) = signal. Note that this parameter is not relevant
        for all types of calibration. In that case, leave this parameter set to zero.

      - Switch to state FGC_OP_CALIBRATING

\*---------------------------------------------------------------------------------------------------------*/
{
    if (cal_sequence != NULL     &&
        STATE_OP     != FGC_OP_CALIBRATING)
    {
        current_cal_sequence         = cal_sequence;
        cal_request_to_dsp.chan_mask = chan_mask;
        cal_request_to_dsp.idx       = signal;

        STATE_OP = FGC_OP_CALIBRATING;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CalRunSequence(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the Sta Task when the OP state is CALIBRATING and the millisecond time
  is 102, 302, 502, 702, or 902. It is responsible for directing the DSP in running the auto calibration
  sequence for the ADCs, DAC and DCCTs.
\*---------------------------------------------------------------------------------------------------------*/
{
    BOOLEAN next_sequence_flag;

    if (current_cal_sequence != NULL)
    {
        if (current_cal_sequence->func != NULL)
        {
            next_sequence_flag = current_cal_sequence->func(current_cal_sequence->par1,
                                                            current_cal_sequence->par2);    // Run the sequence function

            if (next_sequence_flag)
            {
                current_cal_sequence++;             // Go to the next sequence structure (struct cal_seq) in the list
                // The last sequence structure is marked by a NULL function pointer
            }
        }

        if (current_cal_sequence->func == NULL)
        {
            current_cal_sequence = NULL;            // Over
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalWaitDsp(INT16U unused1, INT16U unused2)
/*---------------------------------------------------------------------------------------------------------*\
  Wait until DSP completes calibration
\*---------------------------------------------------------------------------------------------------------*/
{
    if ((INT16U) dpcls.dsp.cal_complete_f)              // If calibration complete in DSP
    {
        dpcls.mcu.cal.req_f      = FALSE;
        dpcls.dsp.cal_complete_f = FALSE;
        return (TRUE);                                  // Advance to next sequence element
    }

    return (FALSE);                                     // Remain at this sequence element
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalInternalAdcs(INT16U ave_steps, INT16U unused)
/*---------------------------------------------------------------------------------------------------------*\
  Request DSP to calibrate the internal ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Calibration request to the DSP:
    // cal_request_to_dsp.chan_mask & idx are set in CalInitSequence()

    cal_request_to_dsp.action    = CAL_REQ_INTERNAL_ADC;
    cal_request_to_dsp.ave_steps = ave_steps;

    return (CalSendRequestToDsp());             // Attempt to send the request to the DSP
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalInternalAdcsMask(INT16U ave_steps, INT16U adc_mask)
/*---------------------------------------------------------------------------------------------------------*\
  Request DSP to calibrate the internal ADCs specified by the function parameter adc_mask
\*---------------------------------------------------------------------------------------------------------*/
{
    // Calibration request to the DSP:
    // cal_request_to_dsp.idx is set in CalInitSequence()

    cal_request_to_dsp.action    = CAL_REQ_INTERNAL_ADC;
    cal_request_to_dsp.ave_steps = ave_steps;
    cal_request_to_dsp.chan_mask = adc_mask;

    return (CalSendRequestToDsp());             // Attempt to send the request to the DSP
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalExternalAdcs(INT16U ave_steps, INT16U unused)
/*---------------------------------------------------------------------------------------------------------*\
  Request DSP to calibrate the internal ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Calibration request to the DSP:
    // cal_request_to_dsp.chan_mask & idx are set in CalInitSequence()

    cal_request_to_dsp.action    = CAL_REQ_EXTERNAL_ADC;
    cal_request_to_dsp.ave_steps = ave_steps;

    return (CalSendRequestToDsp());             // Attempt to send the request to the DSP
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalDccts(INT16U ave_steps, INT16U unused)
/*---------------------------------------------------------------------------------------------------------*\
  Request DSP to calibrate the internal ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Calibration request to the DSP:
    // cal_request_to_dsp.chan_mask & idx are set in CalInitSequence()

    cal_request_to_dsp.action    = CAL_REQ_DCCT;
    cal_request_to_dsp.ave_steps = ave_steps;

    return (CalSendRequestToDsp());             // Attempt to send the request to the DSP
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalDacs(INT16U ave_steps, INT16U unused)
/*---------------------------------------------------------------------------------------------------------*\
  Request DSP to calibrate the internal ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Calibration request to the DSP:
    // cal_request_to_dsp.chan_mask & idx are set in CalInitSequence()

    cal_request_to_dsp.action    = CAL_REQ_DAC;
    cal_request_to_dsp.ave_steps = ave_steps;

    return (CalSendRequestToDsp());             // Attempt to send the request to the DSP
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalNvsSaveInternalAdcs(INT16U unused1, INT16U unused2)
/*---------------------------------------------------------------------------------------------------------*\
  Save internal ADCs errors in non-volatile memory, according to cal_request_to_dsp.chan_mask
\*---------------------------------------------------------------------------------------------------------*/
{
    if (cal_request_to_dsp.chan_mask & 0x01)
    {
        dpcls.mcu.cal.adc.internal[0].err[3] = ID_TEMP_FLOAT(temp.log.fgc_in);
        PropSetNumEls(NULL, &PROP_CAL_A_ADC_INTERNAL_ERR, 6);
        nvsStoreBlockForOneProperty(NULL, &PROP_CAL_A_ADC_INTERNAL_ERR, 
                                    (INT8U *)&dpcls.mcu.cal.adc.internal[0].err[0],
                                    0, 0, 6, TRUE);
    }

    if (cal_request_to_dsp.chan_mask & 0x02)
    {
        dpcls.mcu.cal.adc.internal[1].err[3] = ID_TEMP_FLOAT(temp.log.fgc_in);
        PropSetNumEls(NULL, &PROP_CAL_B_ADC_INTERNAL_ERR, 6);
        nvsStoreBlockForOneProperty(NULL, &PROP_CAL_B_ADC_INTERNAL_ERR,
                                    (INT8U *)&dpcls.mcu.cal.adc.internal[1].err[0], 
                                    0, 0, 6, TRUE);
    }

    if (cal_request_to_dsp.chan_mask & 0x04)
    {
        dpcls.mcu.cal.adc.internal[2].err[3] = ID_TEMP_FLOAT(temp.log.fgc_in);
        PropSetNumEls(NULL, &PROP_CAL_C_ADC_INTERNAL_ERR, 6);
        nvsStoreBlockForOneProperty(NULL, &PROP_CAL_C_ADC_INTERNAL_ERR,
                                    (INT8U *)&dpcls.mcu.cal.adc.internal[2].err[0],
                                    0, 0, 6, TRUE);
    }

    if (cal_request_to_dsp.chan_mask & 0x08)
    {
        dpcls.mcu.cal.adc.internal[3].err[3] = ID_TEMP_FLOAT(temp.log.fgc_in);
        PropSetNumEls(NULL, &PROP_CAL_D_ADC_INTERNAL_ERR, 6);
        nvsStoreBlockForOneProperty(NULL, &PROP_CAL_D_ADC_INTERNAL_ERR,
                                    (INT8U *)&dpcls.mcu.cal.adc.internal[3].err[0],
                                    0, 0, 6, TRUE);
    }

    if (cal_request_to_dsp.chan_mask == 0x0F)   // If all ADCs were calibrated
    {
        CalLastCalTime(0, 0);
    }

    return (TRUE);
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalNvsSaveExternalAdcs(INT16U unused1, INT16U unused2)
/*---------------------------------------------------------------------------------------------------------*\
  End auto calibration of one error for one ADC22
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 61)

    if (cal_request_to_dsp.chan_mask & 0x01)
    {
        dpcls.mcu.cal.adc.external[0].err[3] = ID_TEMP_FLOAT(temp.ext_adc[0].mod);
        PropSetNumEls(NULL, &PROP_CAL_A_ADC_EXTERNAL_ERR, 6);
        nvsStoreBlockForOneProperty(NULL,
                                    (struct prop *) &PROP_CAL_A_ADC_EXTERNAL_ERR,
                                    (INT8U *) &dpcls.mcu.cal.adc.external[0].err,
                                    0,
                                    0,
                                    6,
                                    TRUE);
    }

    if (cal_request_to_dsp.chan_mask & 0x02)
    {
        dpcls.mcu.cal.adc.external[1].err[3] = ID_TEMP_FLOAT(temp.ext_adc[1].mod);
        PropSetNumEls(NULL, &PROP_CAL_B_ADC_EXTERNAL_ERR, 6);
        nvsStoreBlockForOneProperty(NULL,
                                    &PROP_CAL_B_ADC_EXTERNAL_ERR,
                                    (INT8U *) &dpcls.mcu.cal.adc.external[1].err,
                                    0,
                                    0,
                                    6,
                                    TRUE);
    }

#endif
    return (TRUE);
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalNvsSaveDccts(INT16U unused1, INT16U unused2)
/*---------------------------------------------------------------------------------------------------------*\
  End auto calibration of one error for one DCCT
\*---------------------------------------------------------------------------------------------------------*/
{
    if (cal_request_to_dsp.chan_mask & 0x01)
    {
        dpcls.mcu.cal.dcct[0].err[3] = ID_TEMP_FLOAT(temp.dcct[0].elec);
        PropSetNumEls(NULL, &PROP_CAL_A_DCCT_ERR, 6);
        nvsStoreBlockForOneProperty(NULL,
                                    &PROP_CAL_A_DCCT_ERR,
                                    (INT8U *) &dpcls.mcu.cal.dcct[0].err,
                                    0,
                                    0,
                                    6,
                                    TRUE);
    }

    if (cal_request_to_dsp.chan_mask & 0x02)
    {
        dpcls.mcu.cal.dcct[1].err[3] = ID_TEMP_FLOAT(temp.dcct[1].elec);
        PropSetNumEls(NULL, &PROP_CAL_B_DCCT_ERR, 6);
        nvsStoreBlockForOneProperty(NULL,
                                    &PROP_CAL_B_DCCT_ERR,
                                    (INT8U *) &dpcls.mcu.cal.dcct[1].err,
                                    0,
                                    0,
                                    6,
                                    TRUE);
    }

    return (TRUE);
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalNvsSaveDacs(INT16U unused1, INT16U unused2)
/*---------------------------------------------------------------------------------------------------------*\
  End auto calibration of one error for one DCCT
\*---------------------------------------------------------------------------------------------------------*/
{
    if (cal_request_to_dsp.chan_mask & 0x01)    // DAC1
    {
        //nvsStoreBlockForOneProperty(NULL, &PROP_CAL_A_DAC, 0, 3, TRUE, NULL);
    }

    if (cal_request_to_dsp.chan_mask & 0x02)    // DAC2
    {
        //nvsStoreBlockForOneProperty(NULL, &PROP_CAL_B_DAC, 0, 3, TRUE, NULL);
    }

    return (TRUE);
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalLastCalTime(INT16U unused1, INT16U unused2)
/*---------------------------------------------------------------------------------------------------------*\
  Save the last calibration time of the internal ADCs (supposedly, calibration of ALL internal ADCS):

     ADC.ADC16.LAST_CAL_TIME
\*---------------------------------------------------------------------------------------------------------*/
{
    cal.last_cal_time_unix = TimeGetUtcTime();
    nvsStoreBlockForOneProperty(NULL,
                                &PROP_ADC_INTERNAL_LAST_CAL_TIME,
                                (INT8U *) &cal.last_cal_time_unix,
                                0,
                                0,
                                1,
                                TRUE);

    return (TRUE);
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN CalEnd(INT16U sync_db_f, INT16U unused2)
/*---------------------------------------------------------------------------------------------------------*\
  End auto calibration: this will run after a pause of 200ms to allow the multiplexers to
  be reset to the standard values in case the system is unconfigured. By clearing the FGC_OP_CALIBRATING state
  in STATE_OP, this function terminates the calibration sequence.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (sync_db_f &&                                    // If SYNC_DB is required and
        sta.config_state != FGC_CFG_STATE_STANDALONE &&  // CONFIG.MODE can be modified, and
        dev.name[0]      != '*'                      &&  // the device name is valid, and
        sta.config_mode  == FGC_CFG_MODE_SYNC_NONE)      // no synchronisation is in progress or failed
    {
        sta.sync_db_cal_delay_s = 60;                           // Trigger sync_db_cal in 1 minute
    }


    if (!nvs.n_unset_config_props)          // If all properties are configured
    {
        STATE_OP = sta.mode_op;                     // set operational state to operational mode
    }
    else                                    // else some properties are unconfigured
    {
        STATE_OP = FGC_OP_UNCONFIGURED;             // stay in UNCONFIGURED state
    }

    CalClearMeasMask();

    return (TRUE);
}
/*---------------------------------------------------------------------------------------------------------*/
static BOOLEAN CalSendRequestToDsp()
/*---------------------------------------------------------------------------------------------------------*\
  Send the calibration request to the DSP if it is ready to read
\*---------------------------------------------------------------------------------------------------------*/
{
    if (dpcls.mcu.cal.req_f)
    {
        // DSP already has a calibration request to read

        return (FALSE);         // Remain in the same calibration step
    }
    else
    {
        // DSP is ready: copy the calibration request in shared memory and go to next calibration step

        dpcls.mcu.cal.action    = (INT32S)cal_request_to_dsp.action;
        dpcls.mcu.cal.chan_mask =         cal_request_to_dsp.chan_mask;
        dpcls.mcu.cal.ave_steps =         cal_request_to_dsp.ave_steps;
        dpcls.mcu.cal.idx       =         cal_request_to_dsp.idx;

        dpcls.mcu.cal.req_f     = TRUE;

        if (cal_request_to_dsp.action != CAL_REQ_DAC)
        {
            CalSetMeasMask();
        }

        return (TRUE);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void CalSetMeasMask()
/*---------------------------------------------------------------------------------------------------------*\
  Send the calibration request to the DSP if it is ready to read
\*---------------------------------------------------------------------------------------------------------*/
{
    if (dpcls.mcu.cal.chan_mask & 0x01)
    {
        Set(ST_MEAS_A, FGC_ADC_STATUS_CAL_ACTIVE);
    }

    if (dpcls.mcu.cal.chan_mask & 0x02)
    {
        Set(ST_MEAS_B, FGC_ADC_STATUS_CAL_ACTIVE);
    }

    if (dpcls.mcu.cal.chan_mask & 0x04)
    {
        Set(ST_MEAS_C, FGC_ADC_STATUS_CAL_ACTIVE);
    }

    if (dpcls.mcu.cal.chan_mask & 0x08)
    {
        Set(ST_MEAS_D, FGC_ADC_STATUS_CAL_ACTIVE);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void CalClearMeasMask()
/*---------------------------------------------------------------------------------------------------------*\
  Send the calibration request to the DSP if it is ready to read
\*---------------------------------------------------------------------------------------------------------*/
{
    Clr(ST_MEAS_A, FGC_ADC_STATUS_CAL_ACTIVE);
    Clr(ST_MEAS_B, FGC_ADC_STATUS_CAL_ACTIVE);
    Clr(ST_MEAS_C, FGC_ADC_STATUS_CAL_ACTIVE);
    Clr(ST_MEAS_D, FGC_ADC_STATUS_CAL_ACTIVE);
}

// EOF
