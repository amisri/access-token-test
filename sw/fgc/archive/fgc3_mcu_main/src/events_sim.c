/*---------------------------------------------------------------------------------------------------------*\
 File        : events_sim.c
 Description : Reproduces a super cycle by simulating events either based on a property or hear-beat.
 Scope       : FGC:MCU
 NB          : The simulation logic assumes that on powering on or reseting of the FGC, the connection
               to the gateway is functioning correctly ???.
\*---------------------------------------------------------------------------------------------------------*/

#define EVENTS_SIM_GLOBALS

// Includes

#include <macros.h>
#include <defprops.h>
#include <fgc_fieldbus.h>
#include <dpcom.h>

// Constants

// The constants below must be integer multiple of 20, the period at which EventsSimulate is called.

#define NEXT_SSC_DELAY_MS         1200   // Delay for the start of super cycle.
#define NEXT_CYCLE_DELAY_MS       1180   // Delay for the next cycle (C0).
#define NEXT_EXTRACTION_DELAY_MS   900   // Delay for the extraction event.
#define NEXT_INJECTION_DELAY_MS    180   // Delay for the injection event.
#define NEXT_START_CYCLE_DELAY_MS  600   // Delay for the start cycle event.

// The constants below define the times in milliseconds with respect to C0. A value
// of 1180 corresponds to millisecond 20 within the last basic period of a cycle.

#define TIME_EVENT_SET_C0             0   // Beginning of a cycle (send SSC event if needed).
#define TIME_EVENT_CLR_C0          1180   // Time to clear events related to C0 (SSC event).
#define TIME_EVENT_SET_NEXT_CYCLE  1180   // Time to send the next cycle information: user, dst, bps.
#define TIME_EVENT_CLR_NEXT_CYCLE  1160   // Time to clear the next cycle information: user, dst, bps.
#define TIME_EVENT_SET_START_CYCLE  600   // Time to send the start cycle event.
#define TIME_EVENT_CLR_START_CYCLE  580   // Time to clear the start cycle event
#define TIME_EVENT_SET_INJECTION    520   // Time to send the injection event.
#define TIME_EVENT_CLR_INJECTION    500   // Time to clear the injection event.
#define TIME_EVENT_SET_EXTRACTION   100   // Time to send the extraction event.
#define TIME_EVENT_CLR_EXTRACTION    80   // Time to clear the extraction event.

// Internal structures, unions and enumerations

static struct                  // Internal global variables
{
    INT16U      cyc_idx;                        // Index in simulated super-cycle
    INT16U      cyc_time;                       // Down counter within a cycle
    INT16U      n_cycles;                       // Number of cycles in the simulated super-cycle
    INT8U       user;                           // User for the current simulated cycle.
    INT8U       length;                         // Length for the current simulated cycle.
    INT8U       prev_length;                    // Previous user length
    BOOLEAN     enabled;                        // Simulated super-cycle enabled flag
} vars;

// Internal variable definitions

static struct fgc_event simevents[FGC_NUM_EVENT_SLOTS] =
{
    {0, 0, NEXT_SSC_DELAY_MS},
    {0, 0, NEXT_CYCLE_DELAY_MS},
    {0, 0, NEXT_CYCLE_DELAY_MS},
    {0, 0, NEXT_START_CYCLE_DELAY_MS},
    {0, 0, NEXT_INJECTION_DELAY_MS},
    {0, 0, NEXT_EXTRACTION_DELAY_MS}
};

// External function definitions

/*---------------------------------------------------------------------------------------------------------*/
void EventsSimEnable(void)
/*---------------------------------------------------------------------------------------------------------*\
  Disable simulation.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (!vars.enabled)
    {
        dpcom.mcu.evt.sim_supercycle_f = TRUE;
        vars.enabled      = TRUE;
        vars.cyc_idx     = 0;
        vars.cyc_time    = 0;
        vars.prev_length = 1;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void EventsSimDisable(void)
/*---------------------------------------------------------------------------------------------------------*\
  Disable simulation.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (vars.enabled)
    {
        vars.enabled = FALSE;
        dpcom.mcu.evt.sim_supercycle_f = FALSE;
    }
}
//*---------------------------------------------------------------------------------------------------------*/
BOOLEAN EventsSimIsEnabled(void)
/*---------------------------------------------------------------------------------------------------------*\
  Returns TRUE if the events are being simulated.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (vars.enabled);
}
/*---------------------------------------------------------------------------------------------------------*/
struct fgc_event * EventsSimProcess(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function simulates a super-cycle in the event that the Gateway is not accessible or it does not
  receive timing events or in the case the FGC.EVENT.SIM property has been set.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U cyc_time = vars.cyc_time;

    if (cyc_time == TIME_EVENT_SET_C0)
    {
        // The simulated events might have been modified. Refresh it.

        vars.n_cycles = PROP_FGC_CYC_SIM.n_elements;

        // If the number of simulated events has decreased, reset the cycle index.
        // s fgc.cyc.sim 101,202,2403,404
        // and during the cycle 3:
        // s fgc.cyc.sim 101
        // vars.cyc_idx might be pointing outside the new array of simulated events in vars.cycles

        if (vars.cyc_idx >= vars.n_cycles)
        {
            vars.cyc_idx = 0;

            simevents[0].type = FGC_EVT_SSC;
        }

        // Prepare parameters for the next cycle

        vars.user = simsc.cycles[vars.cyc_idx] / 100;
        vars.length = simsc.cycles[vars.cyc_idx] % 100;

        if (vars.length <= 0)
        {
            vars.length = 1;
        }

        // Time for the current cycle.

        vars.cyc_time = vars.prev_length * FGC_BASIC_PERIOD_MS;

        vars.prev_length = vars.length;

        ++vars.cyc_idx;
    }

    if (cyc_time == TIME_EVENT_CLR_C0)
    {
        simevents[0].type = FGC_EVT_NONE;
    }

    if (cyc_time == TIME_EVENT_SET_NEXT_CYCLE)
    {
        simevents[1].type    = FGC_EVT_NEXT_CYC_USER;
        simevents[1].payload = vars.user;

        simevents[2].type    = FGC_EVT_NEXT_CYC_LEN;
        simevents[2].payload = vars.length;
    }

    if (cyc_time == TIME_EVENT_CLR_NEXT_CYCLE)
    {
        simevents[1].type = FGC_EVT_NONE;
        simevents[2].type = FGC_EVT_NONE;
    }

    if (cyc_time == TIME_EVENT_SET_START_CYCLE)
    {
        simevents[3].type = FGC_EVT_CYCLE_START;
        simevents[3].payload = vars.user;
    }

    if (cyc_time == TIME_EVENT_CLR_START_CYCLE)
    {
        simevents[3].type = FGC_EVT_NONE;
        simevents[3].payload = 0;
    }

    if (cyc_time == TIME_EVENT_SET_INJECTION)
    {
        simevents[4].type    = FGC_EVT_INJECTION;
        simevents[4].payload = vars.user;
    }

    if (cyc_time == TIME_EVENT_CLR_INJECTION)
    {
        simevents[4].type    = FGC_EVT_NONE;
        simevents[4].payload = 0;
    }

    if (cyc_time == TIME_EVENT_SET_EXTRACTION)
    {
        simevents[5].type    = FGC_EVT_EXTRACTION;
        simevents[5].payload = vars.user;
    }

    if (cyc_time == TIME_EVENT_CLR_EXTRACTION)
    {
        simevents[5].type    = FGC_EVT_NONE;
        simevents[5].payload = 0;
    }

    vars.cyc_time -= 20;

    return simevents;
}

// EOF
