/*---------------------------------------------------------------------------------------------------------*\
  File:         fgc_gateway.c

  Purpose:      FGC boot - Ethernet ethernet Interface Functions.

                Aug 2011 hl created
\*---------------------------------------------------------------------------------------------------------*/
// Defines
#define INLINE inline

// Includes
#include <fgc_gateway.h>
#include <iodefines.h>
#include <string.h>

#include <dpcls.h>
#include <ether_comm.h>
#include <fbs.h>
#include <lan92xx.h>
#include <pars_service.h>
#include <pll.h>
#include <time_fgc.h>
#include <tsk.h>

// Internal Functions Declaration
static INLINE uint8_t FgcGatewaySendStatus(uint32_t packet_tag);
static INLINE uint8_t FgcGatewaySendStatusPayload(uint32_t packet_tag);
static INLINE uint8_t FgcGatewaySendHeader(uint32_t packet_tag);
static INLINE uint8_t FgcGatewayAddPadding(uint32_t packet_tag);

static INLINE void    FgcGatewayReceivePacketTypeTime(void);
static INLINE void    FgcGatewayReceivePacketTypeCmd(void);


static INLINE void    FgcGatewayReadTime(void);
static INLINE void    FgcGatewayReadCmd(void);

static INLINE void    FgcGatewayInitVariables(void);
static INLINE void    FgcGatewayInitHistograms(void);
static INLINE void    FgcGatewayInitFunctions(void);
static INLINE void    FgcGatewaySetHeader(void);
static INLINE void    FgcGatewayResetRemoteTerminalBuffer(void);

// Internal Enumerations
enum fgc_gateway_payload_received
{
    FGC_GATEWAY_PAYLOAD_TIME,
    FGC_GATEWAY_PAYLOAD_COMMAND,
    FGC_GATEWAY_PAYLOAD_NUMBER
};

// Internal Types
typedef union fgc_gateway_payload
{
    fgc_ether_cmd_payload_t     cmd;
    fgc_ether_pub_payload_t     pub;
    fgc_ether_rsp_payload_t     rsp;
    fgc_ether_status_payload_t  status;
} fgc_gateway_payload_t;

typedef struct fgc_gateway_packet
{
    fgc_ether_header_t      header;
    fgc_gateway_payload_t   payload;
} fgc_gateway_packet_t;

typedef struct fgc_gateway_packets
{
    fgc_gateway_packet_t rx;
    fgc_gateway_packet_t tx;
} fgc_gateway_packets_t;

typedef struct fgc_gateway_functions
{
    void (*receive_packet[FGC_ETHER_PAYLOAD_NUM_TYPES])(void);
} fgc_gateway_functions_t;

typedef struct local_fgc_gateway
{
    fgc_gateway_packets_t packets;
    uint16_t payload_to_index[FGC_ETHER_PAYLOAD_NUM_TYPES];
    fgc_gateway_functions_t functions;
} local_fgc_gateway_t;

// Internal Variables Declaration
local_fgc_gateway_t local_fgc_gateway;

// External Functions Definitions
void    FgcGatewayInit(void)
{
    FgcGatewayInitVariables();
    FgcGatewayInitFunctions();
    FgcGatewaySetHeader();
    FgcGatewayInitHistograms();
}

BOOLEAN FgcGatewaySendStatVar(void)
{
    uint8_t err = 0;
    uint32_t packet_tag;
    uint32_t mst_task_start_us = TimeGetUtcTimeUs();

    if (!fbs.gw_online_f)
    {
        return (FALSE);
    }

    local_fgc_gateway.packets.tx.header.fgc.payload_type = FGC_ETHER_PAYLOAD_STATUS;
    packet_tag = local_fgc_gateway.packets.tx.header.fgc.payload_type << 16;

    err = FgcGatewaySendStatus(packet_tag);

    if (err)
    {
        ethernet.stats.frame_count.sent.fail++;
    }

    HistogramAdd(&communication_histograms.tx.gw, HistogramGetRowIndexFromTime(mst_task_start_us),
                 TimeGetAbsUs());
    return (err == 0);
}

void    FgcGatewaySendMsg(void)
{

    INT8U       err;
    INT16U      segment1_size, segment2_size, segment3_size;
    INT16U      packet_size;
    INT32U      packet_tag;
    uint32_t    mst_task_start_us;

    mst_task_start_us = TimeGetUtcTimeUs();

    // Prepare ETH header
    if (fbs.pend_pkt == FBS_CMD_PKT)
    {
        local_fgc_gateway.packets.tx.header.fgc.payload_type = FGC_ETHER_PAYLOAD_RSP;
    }
    else if (fbs.pend_pkt == FBS_PUB_PKT)
    {
        local_fgc_gateway.packets.tx.header.fgc.payload_type = FGC_ETHER_PAYLOAD_PUB;
    }

    packet_tag = local_fgc_gateway.packets.tx.header.fgc.payload_type << 16;

    // Prepare RSP header

    // Set header with pkt description
    local_fgc_gateway.packets.tx.payload.rsp.data.header.flags = fbs.pend_header;
    // term chars are sent in status
    local_fgc_gateway.packets.tx.payload.rsp.data.header.n_term_chs = 0;
    local_fgc_gateway.packets.tx.payload.rsp.length = fbs.rsp_msg_len + sizeof(struct fgc_fieldbus_rsp_header);

    // NB:      A middle segment cannot be less than 4bytes, thus the length cannot be sent alone to the Tx FIFO.
    // NB:      Automatic padding if size less than minimum payload size.

    segment1_size   = sizeof(local_fgc_gateway.packets.tx.header);
    segment2_size   = sizeof(local_fgc_gateway.packets.tx.payload.rsp.length)
                      + sizeof(local_fgc_gateway.packets.tx.payload.rsp.data.header);
    segment3_size   = fbs.rsp_msg_len * sizeof(uint8_t);
    packet_size     = segment1_size + segment2_size + segment3_size;


    err = Lan92xxSendPacketOpt(&local_fgc_gateway.packets.tx.header, segment1_size, packet_size,
                               TX_CMDA_FIRST_SEG, packet_tag);
    err |= Lan92xxSendPacketOpt(&local_fgc_gateway.packets.tx.payload.rsp, segment2_size, packet_size, 0,
                                packet_tag);
    err |= Lan92xxSendPacketOpt(NULL, segment3_size, packet_size,
                                TX_CMDA_LAST_SEG | (ethernet.rsp_dma.offset << 16),
                                packet_tag);


    if (err)
    {
        ethernet.stats.frame_count.sent.fail++;
    }
    else
    {
        if (ethernet.rsp_dma.size_bytes2 != 0)
        {
            DMAC0.DMCSA = ethernet.rsp_dma.source2;
            DMAC0.DMCBC = ethernet.rsp_dma.size_dwords2 * 4;
            DMAC0.DMCRE.BIT.DEN = 1;                    // Enable DMA
            DMAC0.DMCRD.BIT.DREQ = 1;                   // DMA Request (SW trigger)
        }

        DMAC1.DMCSA = ethernet.rsp_dma.source;
        DMAC1.DMCBC = ethernet.rsp_dma.size_dwords * 4;
        DMAC1.DMCRE.BIT.DEN = 1;                        // Enable DMA
        DMAC1.DMCRD.BIT.DREQ = 1;                       // DMA Request (SW trigger)
    }

    HistogramAdd(&communication_histograms.tx.gw, HistogramGetRowIndexFromTime(mst_task_start_us),
                 TimeGetAbsUs());
}

void    FgcGatewayDmac1End(void)
{
    DMAC_COMMON.DMEDET.BIT.DEDET1 = 1;                          // Clear interrupt

    fbs.pend_stream->q.out_idx = fbs.pend_stream->out_idx;      // Advance circular buffer pointer
    fbs.pend_stream->q.is_full = FALSE;                         // Mark queue as not full

    if (fbs.pend_stream->q_not_full->pending)                   // If tasks are pending on sem
    {
        OSSemPost(fbs.pend_stream->q_not_full);                 // Post semaphore
    }
}

void    FgcGatewayReceivePacket(void)
{
    uint16_t    payload_index = local_fgc_gateway.payload_to_index[ethernet.rx_header.fgc.payload_type];
    local_fgc_gateway.functions.receive_packet[payload_index]();
}

// Internal Functions definitions
static INLINE void    FgcGatewayInitVariables(void)
{
    local_fgc_gateway.payload_to_index[FGC_ETHER_PAYLOAD_TIME]  = FGC_GATEWAY_PAYLOAD_TIME;
    local_fgc_gateway.payload_to_index[FGC_ETHER_PAYLOAD_CMD]   = FGC_GATEWAY_PAYLOAD_COMMAND;
    FgcGatewayResetRemoteTerminalBuffer();
}

static INLINE void    FgcGatewayInitFunctions(void)
{
    local_fgc_gateway.functions.receive_packet[FGC_GATEWAY_PAYLOAD_TIME]    = &FgcGatewayReceivePacketTypeTime;
    local_fgc_gateway.functions.receive_packet[FGC_GATEWAY_PAYLOAD_COMMAND] = &FgcGatewayReceivePacketTypeCmd;
}

static INLINE void    FgcGatewayInitHistograms(void)
{
    uint16_t min = 0;
    uint16_t width = 40;

    HistogramInit(&communication_histograms.rx.gw, min, width);
    HistogramInit(&communication_histograms.tx.gw, min, width);
}

static INLINE void    FgcGatewaySetHeader(void)
{
    local_fgc_gateway.packets.tx.header = ethernet.tx_header;
}

static INLINE void    FgcGatewayResetRemoteTerminalBuffer(void)
{
    fgc_gateway.rterm[0] = '\0';
}

static INLINE void    FgcGatewayReceivePacketTypeTime(void)
{
    if (!fbs.gw_online_f)   // if Gateway never seen online, or connection lost, recover gateway address
    {
        memcpy(&local_fgc_gateway.packets.tx.header.ethernet.dest_addr, &ethernet.rx_header.ethernet.src_addr,
               FGC_ETHER_ADDR_SIZE);
    }

    FgcGatewayReadTime();
}

static INLINE void    FgcGatewayReceivePacketTypeCmd(void)
{
    FgcGatewayReadCmd();
}

static INLINE void    FgcGatewayReadTime(void)
{
    uint32_t mst_task_start_us = TimeGetUtcTimeUs();

    if (fbs.count_time_rcvd < 5)
    {
        fbs.count_time_rcvd++;
    }

    // Process Time
    // ToDo to be refactorized

    fbs.last_runlog_idx = fbs.time_v.runlog_idx;

    Lan92xxReadFifo(&fbs.time_v, sizeof(fbs.time_v), &ethernet.current_rx_fifo_byte_length);


    fbs.time_rcvd_f = TRUE;                             // Set time rcvd flag (D cannot be zero)
    fbs.events_rcvd_f = TRUE;                           // Set events rcvd flag (D cannot be zero)

    fbs.u.fieldbus_stat.ack |= FGC_ACK_TIME;            // Acknowledge time_var reception

    // On Ethernet, fbs.time_v.ms_time must be rounded to a 20 ms boundary

    dpcom.mcu.time.fbs.unix_time = fbs.time_v.unix_time;
    dpcom.mcu.time.fbs.ms_time = (INT32U)(fbs.time_v.ms_time - fbs.time_v.ms_time % 20);

    // we need to validate that this irq comes from MSG18 or MSG19
    if ((fbs.time_v.ms_time % 20) == 18)
    {
        pll.eth18.sync_time = PLL_NETIRQTIME_P;
    }
    else if ((fbs.time_v.ms_time % 20) == 19)
    {
        pll.eth19.sync_time = PLL_NETIRQTIME_P;
    }
    else
    {
        ethernet.stats.frame_count.rcvd.bad.time++;
    }

    // Process RT data

    Lan92xxReadFifo(&fgc_gateway.rt_ref, sizeof(fgc_gateway.rt_ref), &ethernet.current_rx_fifo_byte_length);

    dpcls.mcu.ref.rt_data = fgc_gateway.rt_ref[fbs.id];
    dpcls.mcu.ref.rt_data_f = TRUE;             // Flag to DSP that new ref data is waiting

    fbs.u.fieldbus_stat.ack |= FGC_ACK_RD_PKT;  // Set RD rcvd flag in status variable

    HistogramAdd(&communication_histograms.rx.gw, HistogramGetRowIndexFromTime(mst_task_start_us),
                 TimeGetAbsUs());
}

static INLINE void    FgcGatewayReadCmd(void)
{
    uint32_t mst_task_start_us = TimeGetUtcTimeUs();
    Lan92xxReadFifo(&fbs.pkt_len, sizeof(fbs.pkt_len), &ethernet.current_rx_fifo_byte_length);
    fbs.pkt_len -= sizeof(struct fgc_fieldbus_cmd_header);

    Lan92xxReadFifo(&fbs.rcvd_header, sizeof(fbs.rcvd_header), &ethernet.current_rx_fifo_byte_length);
    Lan92xxReadFifo(fcm_pars.pkt[fbs.pkt_buf_idx].buf, fbs.pkt_len, &ethernet.current_rx_fifo_byte_length);

    HistogramAdd(&communication_histograms.rx.gw, HistogramGetRowIndexFromTime(mst_task_start_us),
                 TimeGetAbsUs());
    OSTskResume(TSK_FBS);
}


static INLINE uint8_t FgcGatewaySendStatus(uint32_t packet_tag)
{
    uint8_t err = 0;

    err = FgcGatewaySendHeader(packet_tag);
    err |= FgcGatewaySendStatusPayload(packet_tag);
    // Note: We fill the padding of the struct (member reserved) with data beyond fgc_gateway.rterm, but no write access is performed
    err |= FgcGatewayAddPadding(packet_tag);

    FgcGatewayResetRemoteTerminalBuffer();
    return err;
}

static INLINE uint8_t FgcGatewaySendHeader(uint32_t packet_tag)
{
    return Lan92xxSendPacketOpt(&local_fgc_gateway.packets.tx.header        ,
                                sizeof(local_fgc_gateway.packets.tx.header) ,
                                sizeof(struct fgc_ether_status)             ,
                                TX_CMDA_FIRST_SEG                   ,
                                packet_tag);
}

static INLINE uint8_t FgcGatewaySendStatusPayload(uint32_t packet_tag)
{
    return Lan92xxSendPacketOpt(&fbs.u, sizeof(union fgc_stat_var), sizeof(struct fgc_ether_status), 0,
                                packet_tag);
}

static INLINE uint8_t FgcGatewayAddPadding(uint32_t packet_tag)
{
    return Lan92xxSendPacketOpt(fgc_gateway.rterm,
                                sizeof(fgc_gateway.rterm) + sizeof(((struct fgc_ether_status *) 0)->payload.reserved),
                                sizeof(struct fgc_ether_status), TX_CMDA_LAST_SEG, packet_tag);
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: ethernet.c
\*---------------------------------------------------------------------------------------------------------*/
