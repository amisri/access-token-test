/*---------------------------------------------------------------------------------------------------------*\
  File:         hash.c

  Purpose:      FGC MCU Software - Hash Functions.

  Author:       Quentin.King@cern.ch

  Notes:        This file contains the functions to set up and search hash tables of FGC symbols
                There are three seperate hash tables: Property symbols, constant symbols and
                get option symbols
\*---------------------------------------------------------------------------------------------------------*/

#include <hash.h>               // for HASH_VALUE and struct hash_entry needed by defprops.h
#include <class.h>              // Include class dependent definitions
#include <cmd.h>
#include <defprops.h>           // for PROP_HASH_SIZE, CONST_HASH_SIZE
#include <fgc_parser_consts.h>  // for GETOPT_HASH_SIZE
#include <property.h>           // defprops.h needs DEFPROPS_FUNC_SET(), and for ST_MAX_SYM_LEN
#include <start.h>
#include <string.h>             // for strcmp(), strlen()


// Hash table arrays

static struct hash_entry   *   prop_hash_table  [PROP_HASH_SIZE];
static struct hash_entry   *   const_hash_table [CONST_HASH_SIZE];
static struct hash_entry   *   getopt_hash_table[GETOPT_HASH_SIZE];

static char dims_symbols [FGC_MAX_DIMS][ST_MAX_SYM_LEN];

// Constants

#define KEY_LEN_W             ((ST_MAX_SYM_LEN+1)/2)

/*---------------------------------------------------------------------------------------------------------*/
static INT16U HashSymbol(INT16U size, const char * key)
/*---------------------------------------------------------------------------------------------------------*\
  This function will generate a hash index from the symbol.  This is the result of adding all the words
  of the symbol (with rollover) and then taking the modulus with the size of the hash table.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U              hash_sum;
    INT16U              ii;
    const INT16U    *   key_ptr = (INT16U *) key;

    hash_sum = 0;

    for (ii = 0; ii < KEY_LEN_W; ii++)
    {
        hash_sum += *(key_ptr++);
    }

    return (hash_sum % size);
}
/*---------------------------------------------------------------------------------------------------------*/
static void HashInsert(struct hash_entry ** table, struct hash_entry * entry, INT16U size)
/*---------------------------------------------------------------------------------------------------------*\
  This function will add the entry to the hash table.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U              index;
    struct hash_entry * node;
    char                key[ST_MAX_SYM_LEN + 1];

    // Clear key array

    memset(key, 0, sizeof(key));

    // Transfer far symbol to near key and check the length does not exceed the space on the stack

    strcpy(key, entry->key.c);

    if (strlen(key) > ST_MAX_SYM_LEN)
    {
        Crash(0xBADD);
    }

    // Get root node in hash table for this key

    index = HashSymbol(size, key);
    node  = table[index];

    // Add hash entry into hash table

    if (!node)
    {
        table[index] = entry;           // Add first hash entry for this node index
    }
    else
    {
        while (node->next)              // node index in use so add entry to end of the chain
        {
            node = node->next;
        }

        node->next = entry;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static INT16U HashFind(struct hash_entry * const * table, INT16U size, const char * sym)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search the hash table for an entry with the near symbol sym.  It returns zero if no
  match found, or the symbol index if successful.  The symbol can be assumed to be null terminated and no
  longer than ST_MAX_SYM_LEN characters in length, but not nul padded.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U                      index;
    const struct hash_entry  *  node;
    char                        key[ST_MAX_SYM_LEN + 1];

    memset(key, 0, sizeof(key));

    strcpy(key, sym);

    // Look up hash table node for this symbol and return zero if node is empty

    node = table[ HashSymbol(size, key) ];

    if (!node)
    {
        return (0);
    }

    // Search chain of nodes for a match between the symbol and the hash table entries

    while (node != 0)
    {
        index = node->sym_idx;

        if (strcmp(node->key.c, sym) == 0)
        {
            return (index);      // match - return the node index
        }

        node = node->next;
    }

    index = 0;                                          // No match - return zero

    return (index);
}

INT8U HashInitDIM(const char * key, INT16U n_dims)
{
    if(HashFindProp(key)!=0)
    {
        return 0;
    }

    strncpy(dims_symbols[n_dims], key, ST_MAX_SYM_LEN);
    SYM_TAB_PROP[N_PROP_SYMBOLS + n_dims].key.c = dims_symbols[n_dims];
    
    HashInsert(prop_hash_table, &SYM_TAB_PROP[N_PROP_SYMBOLS + n_dims], PROP_HASH_SIZE);
    return 1;      
}

/*---------------------------------------------------------------------------------------------------------*/
void HashInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will initialise the hash tables for the three types of FGC symbols: property names,
  constants and get options.  The symbols are defined in defprops.h - the first symbol in each table is
  not used so that a zero symbol index can indicate no symbol match.  The length of the tables is defined
  by the parser to be the next prime number up from the number of symbols in the table.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      ii;

    for (ii = 1; ii < N_PROP_SYMBOLS; ii++)
    {
        HashInsert(prop_hash_table, &SYM_TAB_PROP[ii], PROP_HASH_SIZE);
    }

    for (ii = 1; ii < N_CONST_SYMBOLS; ii++)
    {
        HashInsert(const_hash_table, &SYM_TAB_CONST[ii], CONST_HASH_SIZE);
    }

    for (ii = 1; ii < N_GETOPT_SYMBOLS; ii++)
    {
        HashInsert(getopt_hash_table, &sym_tab_getopt[ii], GETOPT_HASH_SIZE);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U HashFindProp(const char * prop_symbol)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search the property hash table for the property symbol
\*---------------------------------------------------------------------------------------------------------*/
{
    return (HashFind(prop_hash_table, PROP_HASH_SIZE, prop_symbol));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U HashFindConst(const char * const_symbol)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search the constant hash table for the constant symbol
\*---------------------------------------------------------------------------------------------------------*/
{
    return (HashFind(const_hash_table, CONST_HASH_SIZE, const_symbol));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U HashFindGetopt(const char * getopt_symbol)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search the get options hash table for the get option symbol
\*---------------------------------------------------------------------------------------------------------*/
{
    return (HashFind(getopt_hash_table, GETOPT_HASH_SIZE, getopt_symbol));
}
// EOF
