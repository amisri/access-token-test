/*!
 * @file      nvs.c
 * @defgroup  FGC:MCU:FGC3
 * @brief     Non-Volatile Storage Management.
 *
 * This file contains the functions which support the non-volatile storage and recovery
 * of Property data to/from the NVRAM memory.
 *
 * NVRAM is accessible by WORD only - BYTE access will result in a bus error!
 *
 * Some non-volatile properties are part of the configuration, meaning they are stored
 * in the central property database and will be set by the FGC configuration manager.
 */

#define NVS_GLOBALS

// Includes

#include <nvs.h>

#include <core.h>
#include <defprops.h>
#include <dev.h>
#include <fbs_class.h>
#include <fbs.h>
#include <fgc_errs.h>
#include <hash.h>
#include <iodefines.h>
#include <macros.h>
#include <mcu_dependent.h>
#include <mcu_dsp_common.h>
#include <mem.h>
#include <memmap_mcu.h>
#include <prop.h>
#include <runlog_lib.h>
#include <sta.h>
#include <start.h>
#include <string.h>

// Constants

#define NVS_RECORDS          ((struct nvs_record *)NVSIDX_REC_32)
#define NVS_DATA_BUF_SIZE(p) ((p->max_elements * prop_type_size[p->type] + sizeof(uint16_t) + 1) & ~1)

// Internal function declarations

static void NvsResetNvsIdx(uint16_t lvl, struct prop * p);
static void NvsCountUnsetConfig(uint16_t lvl, struct prop * p);
static void NvsInitCore(void);
static void NvsRecallValuesForAllProperties(BOOLEAN new_sw_f);
static void NvsStoreValuesForAllProperties(uint16_t lvl, struct prop * p);
static void NvsFormat(void);
static void NvsFormatRecord(uint16_t lvl, struct prop * p);
static void NvsClearCfgChanged(uint16_t lvl, struct prop * p);
static void NvsSanityCheck(struct prop * p);

// Import labels defined by the linker to know possible range for property structures in RAM

extern const char ram_dst_of_data; // Start address for variables
extern const char data_end;        // End address for variables

// Internal function definition

static void NvsClearCfgChanged(uint16_t lvl, struct prop * p)
{
    /* Function of type prop_map_func used in NvsResetChanged() */

    Clr(p->dynflags, DF_CFG_CHANGED);
}

static void NvsResetNvsIdx(uint16_t lvl, struct prop * p)
{
    /* This function is called by PropMap() via NvsInit() to set the NVS record
     * index to the default value INVALID_NVS_IDX.
     */

    p->nvs_idx = INVALID_NVS_IDX;
}

static void NvsCountUnsetConfig(uint16_t lvl, struct prop * p)
{
    if (Test(p->flags, PF_CONFIG) && !Test(p->dynflags, DF_NVS_SET))
    {
        nvs.n_unset_config_props++;
    }
}

static void NvsInitCore(void)
{
    /* This function is called by NvsInit() to initialise the core control
     * table properties in RAM from the table in the NVRAM.  If the table has
     * never been initialised, then it will be set to a default table.
     */

    uint16_t  i;
    struct fgc_corectrl corectrl;

    CoreTableGet(&corectrl);

    // Transfer to CORE properties

    for (i = 0; i < FGC_N_CORE_SEGS; i++)
    {
        core.addr[i]    = corectrl.seg[i].addr;
        core.n_words[i] = corectrl.seg[i].n_words;
    }
}

static void NvsRecallValuesForAllProperties(BOOLEAN new_sw_f)
{
    /*
     * It recover the data for all properties in the NVS. If new_sw_f is FALSE
     * then it can use the stored pointer to find the property, otherwise it
     * must look up the property by name and check to see if it is still
     * non-volatile.
     */

    uint16_t nvs_idx;
    uint32_t prop_addr;
    uint32_t data_addr;

    // Recover number of NVS properties as recorded in the NVS index header
    // and check it is valid

    nvs.n_props = NVSIDX_NUMPROPS_P;

    if (nvs.n_props >= (NVSIDX_W / NVSIDX_REC_W))
    {
        nvs.n_corrupted_indexes++;

        // Return immediately to reformat NVS

        return;
    }

    // Loop for all NVS index records

    for (nvs_idx = 0 ; nvs_idx < nvs.n_props ; nvs_idx++)
    {
        // Check if NVS record is inside valid memory range

        // Data address in NVSDATA zone (must be word aligned)

        data_addr = NVS_RECORDS[nvs_idx].data;

        if ((data_addr <  NVSDATA_32)              ||
            (data_addr > (NVSDATA_32 + NVSDATA_B)) ||
            (data_addr & 1))
        {
            nvs.n_corrupted_indexes++;
            continue;
        }

        // Find property

        if (new_sw_f)
        {
            // PropFind uses struct cmd pcm, but since this is the init,
            // one does not need to take pcm's semaphore.

            if (PropFind((char *)NVS_RECORDS[nvs_idx].name) != 0  ||
                !Test(pcm.prop->flags, PF_NON_VOLATILE)          ||
                NVS_RECORDS[nvs_idx].bufsize != NVS_DATA_BUF_SIZE(pcm.prop))
            {
                continue;
            }

            tcm.cached_prop = pcm.prop;
        }
        else
        {
            prop_addr       = NVS_RECORDS[nvs_idx].prop;
            tcm.cached_prop = (struct prop *) prop_addr;

            // If property pointer is corrupted.
            // Warning, this depends on how the variables are located in the
            // RAM memory the properties are pre filled variables so they go
            // to the .data (not .bss) we test that the properties are in the
            // RAM area defined by the linker

            if (prop_addr < (uint32_t) &ram_dst_of_data          ||
                prop_addr >= (uint32_t) &data_end                ||
                !Test(tcm.cached_prop->flags, PF_NON_VOLATILE) ||
                NVS_RECORDS[nvs_idx].bufsize != NVS_DATA_BUF_SIZE(tcm.cached_prop))
            {
                nvs.n_corrupted_indexes++;
                continue;
            }
        }

        // Link property to NVS record (this will be updated if NVS is
        // reformatted due to a corrupted index). This is used in
        // NvsRecallValuesForOneProperty()

        tcm.cached_prop->nvs_idx = nvs_idx;

        // Extract property data from NVS

        NvsRecallValuesForOneProperty(&tcm, tcm.cached_prop, NVS_ALL_USERS);

        // If this is an old software version, there will be no call to
        // NvsStoreValuesForAllProperties() to copy back the property
        // values in NVS, and therefore this property is now correctly
        // set in both RAM and NVS.

        NvsPropDynflags(tcm.cached_prop);
    }
}

static void NvsFormat(void)
{
    /*
     * This function is called by NvsInit() if it finds that the software
     * version has changed or the NVS area needs to be reformatted. It scans
     * the property tree and for NVS properties it creates a record and saves
     * the property data in the record.
     */

    nvs.data_addr = NVSDATA_32;

    NVRAM_UNLOCK();

    MemSetWords((void *) NVSIDX_32,  0x0000, NVSIDX_W);
    MemSetWords((void *) NVSDATA_32, 0x0000, NVSDATA_W);

    // Format and fill NVS records for all NVS properties

    PropMap(NvsFormatRecord, PROP_MAP_ALL);

    // Update software version and magic number

    NVSIDX_NUMPROPS_P = nvs.idx;
    NVSIDX_SWVER_P    = version.unixtime;
    NVSIDX_MAGIC_P    = NVS_MAGIC;
    NVRAM_NVS_COMPATIBILITY_FP = NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION;
    NVRAM_LOCK();
}

static void NvsFormatRecord(uint16_t lvl, struct prop * p)
{
    /*
     * This function is called from the recursive function NvsScanNvsProps()
     * when it finds a non-volatile property. It will create a new storage
     * space in NVRAM for that property and copy its contents into it.
     */

    uint16_t      idx;
    uint16_t      name_length;
    uint16_t      n_bufs;
    uint16_t      bufsize;
    char        namebuf[NVSIDX_REC_NAME_B + 1];
    char    *   namebuf_p = namebuf;

    // Stack the symbol index of each part of the property name

    nvs.sym_idxs[lvl] = p->sym_idx;

    // If it is non volatile return immediately

    if (p->type != PT_PARENT && Test(p->flags, PF_NON_VOLATILE))
    {

        // If no space left in NVS index

        if (nvs.idx >= (NVSIDX_W / NVSIDX_REC_W))
        {
            Crash(0xBAD1);
        }

        // Link property with its new NVS record

        p->nvs_idx = nvs.idx;

        // Format NVS index record
        bufsize = NVS_DATA_BUF_SIZE(p);
        NVS_RECORDS[nvs.idx].prop    = (uint32_t) p;
        NVS_RECORDS[nvs.idx].bufsize = bufsize;
        NVS_RECORDS[nvs.idx].data    = nvs.data_addr;

        // Assemble and copy property name into NVS index record

        // Clear the name buffer

        memset(namebuf, 0, NVSIDX_REC_NAME_B + 1);

        // Generate name string from symbols

        for (idx = 0, name_length = 0; idx <= lvl; idx++)
        {
            namebuf_p += snprintf(namebuf_p, NVSIDX_REC_NAME_B + 1 - name_length, "%s.",
                                  SYM_TAB_PROP[nvs.sym_idxs[idx]].key.c);

            name_length = namebuf_p - namebuf;

            if ((name_length) > (NVSIDX_REC_NAME_B + 1))
            {
                Crash(0xBADE);
            }
        }

        // Drop trailing '.'

        *(--namebuf_p) = '\0';

        memcpy(&(NVS_RECORDS[nvs.idx].name), namebuf, NVSIDX_REC_NAME_B);

        // Set every NVS data buffer to have the number of elements equal to NVS_UNSET

        n_bufs = (Test(p->flags, PF_PPM) ? FGC_MAX_USER_PLUS_1 : 1);

        for (idx = 0; idx < n_bufs ; idx++, nvs.data_addr += bufsize)
        {
            *((uint16_t *)nvs.data_addr) = (uint16_t)NVS_UNSET;
        }

        // If NVS data area has been overrun or overlap with the NVS index (next in memory map) then crash

        if (nvs.data_addr > (NVSDATA_32 + NVSDATA_B) || nvs.data_addr > NVSIDX_32)
        {
            Crash(0xBAD8);
        }

        nvs.idx++;
    }
}

static void NvsStoreValuesForAllProperties(uint16_t lvl, struct prop * p)
{
    /*
     * This function is called by PropMap() in NvsInit(). Remember that since
     * PropMap() is a recursive function, the deeper the property tree, the
     * more stack space is required.
     */

    if (Test(p->flags, PF_NON_VOLATILE))
    {
        NvsStoreValuesForOneProperty(&tcm, p, NVS_ALL_USERS);
    }
}

void NvsPropDynflags(struct prop * p)
{
    /*
     * This function is used to update the property dynflags after editing the
     * data stored in NVRAM. It will check the data stored in NVS and set the
     * DF_NVS_SET flag only if the number of elements is different from NVS_UNSET
     * for one of the mux_idx.
     */

    uint16_t  mux_idx;
    uint16_t  n_users;
    uint32_t  bufsize;
    uint32_t  data_addr;
    BOOLEAN just_set_f = FALSE;

    // If property is still unset

    if (!Test(p->dynflags, DF_NVS_SET))
    {
        // Number of PPM buffers
        n_users   = (Test(p->flags, PF_PPM) ? FGC_MAX_USER_PLUS_1 : 1);
        data_addr = NVS_RECORDS[p->nvs_idx].data;
        bufsize   = NVS_RECORDS[p->nvs_idx].bufsize;

        for (mux_idx  = 0;
             mux_idx < n_users;
             mux_idx++, data_addr += bufsize)
        {
            // If number of elements for this user (stored in first word of data buffer) is not UNSET
            // and if it's not 0 (so the property was set at least once)

            if ((*((uint16_t *)data_addr) != NVS_UNSET) &&
                (*((uint16_t *)data_addr) != 0))
            {
                Set(p->dynflags, DF_NVS_SET);
                just_set_f = TRUE;
                break;
            }
        }
    }

    // For config properties, keep track of the number unset

    if (Test(p->flags, PF_CONFIG))
    {
        // If property has been set for the first time

        if (just_set_f)
        {
            if (--nvs.n_unset_config_props == 0)
            {
                STATE_OP = sta.mode_op;

                Clr(sta.faults, FGC_FLT_FGC_STATE);
                Clr(FAULTS, FGC_FLT_FGC_STATE);
            }
        }

        Set(p->dynflags, DF_CFG_CHANGED);
        StaSetConfigState(FGC_CFG_STATE_UNSYNCED);
    }
}

static void NvsSanityCheck(struct prop * p)
{
    /*
     * Check that the property is non-volatile and bound to a NVS record.
     */

    if (!Test(p->flags, PF_NON_VOLATILE))
    {
        Crash(0xBADA);
    }

    if (p->nvs_idx == INVALID_NVS_IDX)
    {
        Crash(0xBADB);
    }
}

// External function definitions

void NvsInit(struct init_prop * property_init_info)
{
    /*
     * This function is called during startup, just after interrupts are
     * enabled so that the DSP will run and allow DSP properties to be
     * configured. It is responsible for the recovery/formatting of
     * non-volatile properties using data from the NVS area of the NVRAM
     * memory. This can take three forms:
     *
     *  1. NVS never used - Erase & format
     *  2. SW Version changed - Recover data where possible, then erase
     *     and re-format
     *  3. SW Version not changed - Recover data.
     *
     *  This function sets the operational state (STATE_OP) to UNCONFIGURED or
     *  NORMAL according to whether there are unset configuration properties
     *  after the NVS property initialisation.
     */

    BOOLEAN new_sw_f;

    NvsInitCore();

    sta.mode_op = FGC_OP_NORMAL;

    // Set the nvs_idx for all properties to INVALID_NVS_IDX

    PropMap(NvsResetNvsIdx, PROP_MAP_ALL);

    // If NVS never formatted (or is to be reformatted) or config
    // is too out of date

    if ((NVSIDX_MAGIC_P != NVS_MAGIC) ||
        (NVRAM_NVS_COMPATIBILITY_FP != NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION))
    {
        NvsFormat();
    }
    else
    {
        new_sw_f = (NVSIDX_SWVER_P != version.unixtime);

        NvsRecallValuesForAllProperties(new_sw_f);

        if (new_sw_f || nvs.n_corrupted_indexes)
        {
            NvsFormat();
            PropMap(NvsStoreValuesForAllProperties, PROP_MAP_ONLY_CHILDREN);
        }
    }

    nvs.n_unset_config_props = 0;
    PropMap(NvsCountUnsetConfig, PROP_MAP_ONLY_CHILDREN);

    if (nvs.n_unset_config_props)
    {
        Set(sta.faults, FGC_FLT_FGC_STATE);
    }
    else
    {
        STATE_OP = sta.mode_op;
    }

    // If MODE.OP is TEST, then reset it to value in property_init_info (NORMAL)

    if (sta.mode_op == FGC_OP_TEST)
    {
        Clr(PROP_MODE_OP.dynflags, DF_NVS_SET);
    }

    // Initialise properties

    while (property_init_info->prop)
    {
        if (!Test(property_init_info->prop->dynflags, DF_NVS_SET))
        {
            PropSet(&tcm, property_init_info->prop, property_init_info->data, property_init_info->n_elements);
        }

        property_init_info++;
    }
}

uint16_t NvsStoreValuesForOneProperty(struct cmd  * c,
                                      struct prop * p,
                                      uint16_t      user)
{
    uint16_t      user_from;
    uint16_t      user_to;
    uint32_t      el_size;
    uint32_t      bufsize;
    uint32_t      runlog_error;
    size_t      n_bytes;
    prop_size_t n_els;         // Number of elements in case the property has not been set
    prop_size_t n_els_old;     // Previous number of elements
    uint32_t      data_addr;
    uint32_t      block_addr;
    uint16_t      errnum = FGC_OK_NO_RSP;
    uint16_t      n_blocks;      // Number of property transfer blocks
    uint16_t      remaining_w;   // Number of words remaining to copy

    // Get NVS record info

    NvsSanityCheck(p);

    if (user == NVS_ALL_USERS)
    {
        user_from = 0;
        user_to = (Test(p->flags, PF_PPM) ? FGC_MAX_USER : 0);
    }
    else
    {
        user_from = user_to = user;
    }

    // Data address in NVSDATA zone

    el_size   = prop_type_size[p->type];
    bufsize   = NVS_RECORDS[p->nvs_idx].bufsize;
    data_addr = NVS_RECORDS[p->nvs_idx].data + user_from * bufsize;

    // This is required to call the PropBlkGet() function

    c->cached_prop = p;

    NVRAM_UNLOCK();

    for (c->mux_idx  = user_from;
         c->mux_idx <= user_to;
         c->mux_idx++, data_addr += bufsize)
    {
        c->prop_blk_idx = 0;
        errnum          = PropBlkGet(c, &n_els);

        if (errnum != FGC_OK_NO_RSP)
        {
            runlog_error = 0x01000000 + (p->nvs_idx << 16) + (c->mux_idx << 8) + errnum;
            RunlogWrite(FGC_RL_NVS_ERR, &runlog_error);
        }
        else
        {
            if (n_els > 0)
            {
                remaining_w = (n_els * el_size + 1) / 2;
                n_blocks    = (remaining_w + PROP_BLK_SIZE_W - 1) / PROP_BLK_SIZE_W;

                // Skip the word containing number of elements

                block_addr  = data_addr + sizeof(uint16_t);

                while (n_blocks--)
                {
                    // If not first block

                    if (c->prop_blk_idx)
                    {
                        errnum = PropBlkGet(c, NULL);

                        if (errnum != FGC_OK_NO_RSP)
                        {
                            runlog_error = 0x02000000 + (p->nvs_idx << 16) + (c->mux_idx << 8) + errnum;
                            RunlogWrite(FGC_RL_NVS_ERR, &runlog_error);
                        }
                    }

                    memcpy((void *) block_addr, c->prop_buf->blk.intu,
                           (n_blocks ? PROP_BLK_SIZE_W : remaining_w) * sizeof(uint16_t));

                    block_addr  += PROP_BLK_SIZE;
                    remaining_w -= PROP_BLK_SIZE_W;
                    c->prop_blk_idx++;
                }
            }

            if (errnum == FGC_OK_NO_RSP)
            {
                // Cleaning up the buffer if the length has shrunk

                n_els_old  = *((uint16_t *)data_addr);

                if (n_els_old != NVS_UNSET && n_els < n_els_old)
                {
                    n_bytes = (n_els_old - n_els) * el_size;
                    memset((void *)(data_addr + sizeof(uint16_t) + n_els * el_size), 0, n_bytes);
                }

                // Copy number of elements in first word of NVRAM data
                // (= NVS_UNSET if the property has not been set)

                *((uint16_t *)data_addr) = (uint16_t)n_els;

            }
        }
    }

    // Restore c->mux_idx to a value inside the input range

    c->mux_idx  = user_from;

    NVRAM_LOCK();

    // Mark property as set in NVS (if relevant) and additional process
    // for config properties

    NvsPropDynflags(p);

    return errnum;
}


void nvsStoreBlockForOneProperty(struct cmd         * c,
                                 struct prop        * p,
                                 uint8_t            * data,
                                 uint16_t     const   sub_sel,
                                 uint16_t     const   cyc_sel,
                                 prop_size_t  const   n_elements,
                                 bool         const   last_blk_f)
{
    /*
     * On FGC2 and FGC3 the maximum n_elements for a property saved in NVS
     * is 65534 = 0xFFFE, despite the fact that on FGC3 the number of elements
     * of a property is a uint32_t. Moreover, n_elements = 0xFFFF is a reserved
     * keyword (= NVS_UNSET).
     */

    prop_size_t n_els_old;      // Number of elements of property data
    uint16_t n_bytes;           // Number of bytes to copy
    uint16_t block_idx;         // Property transfer block index
    uint16_t el_size;
    char * data_addr;
    char * block_addr;

    NvsSanityCheck(p);

    data_addr = (char *) NVS_RECORDS[p->nvs_idx].data;

    if (cyc_sel)
    {
        // Select data buffer for cyc_sel. The multiplication must be on 32 bits
        // because the offset can exceed 64kB for some properties (on FGC3)

        data_addr  += (uint32_t)cyc_sel * (uint32_t)(NVS_RECORDS[p->nvs_idx].bufsize);
    }

    // Get local copy of el_size

    el_size   = prop_type_size[p->type];

    // Use prop block index if supplied

    if (c != NULL)
    {
        block_idx = c->prop_blk_idx;
    }
    else
    {
        block_idx = 0;
    }

    n_els_old = *((uint16_t *)data_addr);

    block_addr = data_addr + sizeof(uint16_t) + (uint32_t) block_idx * PROP_BLK_SIZE;

    n_bytes = el_size * n_elements - block_idx * PROP_BLK_SIZE;

    if (n_bytes > PROP_BLK_SIZE)
    {
        n_bytes = PROP_BLK_SIZE;
    }
    else
    {
        // If data has an odd number of bytes pad with zero to word boundary.

        if (n_bytes & 1)
        {
            data[n_bytes++] = 0;
        }
    }

    // Early RETURN if data has not changed

    if (n_elements == n_els_old && !MemCmpWords(block_addr, data, n_bytes / 2))
    {
        return;
    }

    // If property was never sets et old number of elements to zero
    if (n_els_old == NVS_UNSET)
    {
        n_els_old = 0;
    }

    NVRAM_UNLOCK();

    memcpy(block_addr, data, n_bytes);

    if (last_blk_f || n_elements > n_els_old)
    {
        *((uint16_t *)data_addr) = (uint16_t)n_elements;
    }

    // If last block and data has shrunk

    if (last_blk_f && n_elements < n_els_old)
    {
        n_bytes = (n_els_old - n_elements) * el_size;
        memset((void *)(data_addr + sizeof(uint16_t) + n_elements * el_size), 0, n_bytes);
    }

    NVRAM_LOCK();

    NvsPropDynflags(p);
}

void NvsRecallValuesForOneProperty(struct cmd  * c,
                                   struct prop * p,
                                   uint16_t      user)
{
    uint16_t      nvs_idx;
    uint16_t      user_from;
    uint16_t      user_to;
    prop_size_t n_els;
    uint32_t      data_addr;
    uint32_t      block_addr;
    uint32_t      bufsize;

    // Get NVS record

    NvsSanityCheck(p);

    nvs_idx = p->nvs_idx;

    if (user == NVS_ALL_USERS)
    {
        user_from = 0;
        user_to   = (Test(p->flags, PF_PPM) ? FGC_MAX_USER : 0);
    }
    else
    {
        user_from = user_to = user;
    }

    // Prepare data address for the first user

    bufsize   = NVS_RECORDS[nvs_idx].bufsize;
    data_addr = NVS_RECORDS[nvs_idx].data + user_from * bufsize;

    // Extract property data from NVS

    // This is required to call the PropBlkSet() function

    c->cached_prop = p;

    // This is the only function where we set a property without writing it
    // in the NVRAM

    c->write_nvs_f = FALSE;

    for (c->mux_idx  = user_from;
         c->mux_idx <= user_to ;
         c->mux_idx++, data_addr += bufsize)
    {
        n_els = *((uint16_t *)data_addr);

        if (n_els == NVS_UNSET)
        {
            n_els = 0;
        }
        else if (n_els > p->max_elements)
        {
            nvs.n_corrupted_indexes++;
        }
        else
        {
            // If property has not been set for that particular mux_idx

            // Skip the n_els word at the start of the buffer and set
            // the property

            block_addr = data_addr + sizeof(uint16_t);
            PropSetFar(c, p, (void *) block_addr, n_els);
        }
    }

    // Restore c->mux_idx to a value inside the input range

    c->mux_idx     = user_from;

    // Restore the default behaviour of the PropBlkSet() function

    c->write_nvs_f = TRUE;
}

void NvsResetChanged(void)
{
    PropMap(NvsClearCfgChanged, PROP_MAP_ONLY_CHILDREN);
}

// EOF
