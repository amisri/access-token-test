/*---------------------------------------------------------------------------------------------------------*\
  File:         main.c

  Purpose:      FGC MCU Software - contains main()


FOR FGC3
   main chips

   Renesas Rx6108 processor
                   family: Rx600
                   code  : R5F56108VNFP
                   25MHz

   Texas   TMS320c6727b DSP
                   family: Texas TMS 320 C 6000
                   code  : TMS 320 C 6727B GDH 300

   Xilinx Spartan 3AN FPGA
                   family: Spartan
                   code  : XC3S 700AN - 4FG484C

ADC
ADS1274IPAPT

DAC
MAX5541CSA
16 bits

\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL        // defprops.h
#define FGC_PARSER_CONSTS       // Force parser global variable definitions
#define DEF_VERSION             // define def_version global variable
#define DEFPROPS                // define property global variables
#define DEV_GLOBALS             // to instantiate dev global variable
#define PC_STATE_GLOBALS

#define MAIN_GLOBALS            // define main_misc global variable
#define FGC_COMPDB_GLOBALS      // define compdb global variable
#define FGC_SYSDB_GLOBALS       // define sysdb global variable
#define FGC_DIMDB_GLOBALS       // define dimdb global variable
#define OS_GLOBALS              // define os global variable
#define CRATE_GLOBALS           // define crate global variable
#define LOG_EVENT_GLOBALS       // define log_evt global variable

#define SHARED_MEMORY_GLOBALS   // define shared_mem global variable
#define DPRAM_CLASS_GLOBALS     // define dpcls global variable
#define DPCOM_GLOBALS           // define dpcom global variable
#define FGC_TASK_TRACE_GLOBALS

#include <main.h>
#include <debug.h>              // for DEBUG_RUNNING_MAIN_WITHOUT_BOOT
#include <ana_temp_regul.h>
#include <cmd.h>
#include <shared_memory.h>      // for shared_mem global variable, FGC_STAY_IN_BOOT
#include <start.h>              // for Crash()
#include <init.h>               // for InitMCU(), InitOS()
#include <os.h>                 // for OSTskStart()
#include <dev.h>                // for dev global variable
#include <read_PSU_adc.h>       // for psu global variable
#include <DIMsDataProcess.h>    // for InitQspiBus(), diagcom, dim_collection global variables
#include <pll.h>                // for pll global variable needed by defprops.h
#include <master_clk_pll.h>     // for MasterClockAnaloguePllInit() or PllInit()
#include <dig.h>                // for InitDig();
#include <spivs.h>
#include <crate.h>
#include <defprops.h>           // for STP_RESET, STP_BOOT, STP_CRASH
#include <iodefines.h>          // specific processor registers and constants
#include <mcu_dependent.h>      // for NVRAM_LOCK(), ACCEPT_ONLY_NET_LONG_PULSE()
#include <syscalls.h>           // for InitMalloc()
#include <hardware_setup.h>     // for Enable1msTickInterruptFromMcuTimer(), Enable1msTickInterruptFromPLD()
#include <fbs.h>
#include <ethernet.h>           // for EthInit()
#include <dsp_loader.h>
#include <derived_clocks.h>     // for clocks structures, TICK_ADC_PHASE, TICK_DAC_PHASE, StartDspClock(), StartAdcClock(), StartDacClock()
#include <core.h>
#include <mst.h>
#include <dpcom.h>              // for dpcom global variable
#include <pc_state.h>
#include <task_trace.h>
#include <regfgc3_prog_fsm.h>


#if (FGC_CLASS_ID == 61)

#include <dsp_61.h>
#define  DSP_CODE        dsp_61_code
#define  DSP_CODE_SIZE   DSP_61_CODE_SIZE

#elif (FGC_CLASS_ID == 62)

#include <dsp_62.h>
#define  DSP_CODE        dsp_62_code
#define  DSP_CODE_SIZE   DSP_62_CODE_SIZE

#endif

/*---------------------------------------------------------------------------------------------------------*/
int main(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by startup() and is the function that does all the initialisation duties.  It
  then starts the operating system to launch multi-tasking.
\*---------------------------------------------------------------------------------------------------------*/
{
    /*
        if P6.DR.BIT.B5 is 1 (longer integration time) then when the network card
        receive a "reset SHORT pulse signal" (50) from the gateway it is ignored.
        It waits for a "reset LONG pulse signal" (100) to trigger the power cycle.
    */
    ACCEPT_ONLY_NET_LONG_PULSE();

    // Reset peripherals

    P7.DR.BIT.B6 = 1;   // disable digital CMD output

    FbsSelectBus();

    if (fbs.net_type == NETWORK_TYPE_FIP)
    {
        // FIP/ETHERNET reset is direct from Rx610, set FIP/ETHERNET in reset
        P9.DR.BIT.B7 = 0;
    }

    // Clear the stay in boot flag

    shared_mem.mainprog_seq = FGC_MP_STAY_IN_MAIN;

    //------------------------------------------------------------------
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_C62OFF_MASK16;   // Turn M16C62 off
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_DSP_MASK16;      // Reset TMS320C6727
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_ANALOG_MASK16;   // Reset Analog interface

    //  StopDerivedClocks();

    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_DIGITAL_MASK16;  // Reset Digital interface
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_QSM_MASK16;      // Reset diagnostics interface
    //------------------------------------------------------------------
    // release reset signal
    // ToDo : look for a proper place to do this
    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_DIGITAL_MASK16; // Reset Digital interface
    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_QSM_MASK16;     // Reset diagnostics interface
    //------------------------------------------------------------------

    main_misc.n_nvs_unlock_request = 1;
    NVRAM_LOCK();

    // Set temp regul heater to 0

    //  DIG_SUP_A_CTRL_P = 0;   // SUP_A as input & output disabled
    //  DIG_SUP_B_CTRL_P = 0;   // SUP_B as input & output disabled

    InitMCU();

    InitMalloc();       // NewLib uses malloc to allocate the reentrant structure (1 per thread)

    // Todo call Enable1msTickInterruptFromPLD()
    Enable1msTickInterruptFromMcuTimer();       // by default so we can start using USLEEP() and MSLEEP()
    // we can't use Enable1msTickInterruptFromPLD() because there is a OSTskResume(TSK_MST) in it

    ENABLE_INTERRUPTS();

    //------------------------------------------------------------------
    // check if DSP is in standalone mode
    mst.dsp_is_standalone = ((CPU_MODEL_P & CPU_MODEL_DSPSTAL_MASK8) != 0);
    mst.reset = CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_DSP_MASK16; // Clear DSP reset


#if defined(DEBUG_RUNNING_MAIN_WITHOUT_BOOT)
    // Set analog_card_model (normally done by the boot program)

    shared_mem.analog_card_model = MID_ANATYPE_P;

    // the ADC transfer clock is derived from DSP NMI Tick
    // so DSP NMI Tick must be correctly programmed for ADC clock to work
    TICK_DSP_ADC_DELAY_US_P = 20; // 20us from NMI generation to DAC transfer
    // the DAC transfer clock is derived from DSP NMI Tick
    // so DSP NMI Tick must be correctly programmed for DAC clock to work
    TICK_DSP_DAC_DELAY_US_P = 20; // 20us from NMI generation to DAC transfer

#endif

    dpcom.mcu.interfacetype = shared_mem.analog_card_model;// Transfer interface type to DSP

    // load DSP boot and run it

    dev.dsp_load_error = dspLoadImage(DSP_CODE, DSP_CODE_SIZE);

    if (dev.dsp_load_error != 0)
    {
        // Do something
    }

    // Leave the DSP frequency as set by the boot program (i.e. 10 kHz)
    TICK_DSP_PERIOD_US_P = 100; // 100us, 10Khz rate for DSP NMI generation

    TICK_DSP_SPI_DELAY_US_P = 15; // 15 us after the DSP tick

    StartAdcClock();
    StartDacClock();
    StartDspClock();

    // ToDo: try to encapsulate this call and share with boot
    // INTERNAL ADCs & DACs

    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_ANALOG_MASK16;  // release analogue reset signal
    mst.reset   &= ~CPU_RESET_CTRL_ANALOG_MASK16;       // If DSP in standalone

    //------------------------------------------------------------------
    InitQspiBus();
    HashInit();
    DigInit();

    //------------------------------------------------------------------
    // Releases FIP/ETHERNET hw reset, reset line must have an internal pull-up
    P9.DR.BIT.B7 = 1;

    if (fbs.net_type == NETWORK_TYPE_FIP)
    {
        FipIdCheck();
    }
    else if (fbs.net_type == NETWORK_TYPE_ETHERNET)
    {
        EthInit();
    }

    MasterClockAnaloguePllInit();

    crateInit();

    regFgc3ProgFsmInit();

    //------------------------------------------------------------------
    InitOS();

    OSTskStart(); // Never returns

    return (0);   // Appease the compiler
}
/*---------------------------------------------------------------------------------------------------------*/
void stop(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from MstTsk() if the dev.ctrl_counter reaches zero.  This will be set by the
  SetDevice property to trigger a delayed stop action to allow the command communication to complete.
  Note that the PWRCYC option is activated in MstTsk() via the LEDS register which must be activated for
  many milliseconds, during which time the fast watchdog trigger must be maintained, otherwise a crash
  response occurs.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (dev.ctrl_sym_idx)
    {
        case STP_PWRCYC:

            POWER_CYCLE();
            break;

        case STP_RESET:         // Global Reset

            Reset();
            break;

        case STP_BOOT:          // Global Reset and stay in boot

            shared_mem.mainprog_seq = FGC_MP_STAY_IN_BOOT;
            Reset();
            break;

        case STP_CRASH:         // Force a MCU crash

            Crash(0xDEAD);
            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/
