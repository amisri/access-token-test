/*---------------------------------------------------------------------------------------------------------*\
  File:     start.c

  Purpose:

\*---------------------------------------------------------------------------------------------------------*/

#include <start.h>

#include <cc_types.h>           // basic typedefs
#include <cmd.h>                // Command header
#include <fbs_class.h>          // for ST_LATCHED
#include <dev.h>                // for dev global variable
#include <mst.h>                // for mst global variable
#include <dpcom.h>              // for dpcom global variable
#include <sta.h>                // for sta global variable
#include <memmap_mcu.h>         // for CPU_RESET_CTRL_DSP_MASK16, RGLEDS_
#include <fbs.h>                // for fbs global variable
#include <mem.h>                // for MemCpyWords()
#include <macros.h>             // for Set()
#include <mcu_dependent.h>      // for NVRAM_UNLOCK(), NVRAM_LOCK()
#include <mst.h>                // for mst global variable
#include <stdlib.h>             // for exit()
#include <sta.h>                // for sta.crash_code
#include <mcu_panic.h>          // for McuPanic()
#include <mcu_dsp_common.h>     // for DSP_STARTED
#include <defprops.h>
#include <runlog_lib.h>
#include <sleep.h>

/*---------------------------------------------------------------------------------------------------------*/
void Reset(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function performs the following task:

    - Write the MP command into the SM register
    - Mask Interrupts
    - Activate a global reset
    - Wait to be reset
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR  cpu_sr;

    OS_ENTER_CRITICAL_7();              // Disable ALL interrupts

    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_GLOBAL_MASK16;   // Request a global reset

    // Wait for reset to take effect

    exit(0);       // this will hang in a jump to itself
}
/*---------------------------------------------------------------------------------------------------------*/
void Crash(INT16U reg_e)
/*---------------------------------------------------------------------------------------------------------*\
  On FGC3 a Crash is redirected to McuPanic
\*---------------------------------------------------------------------------------------------------------*/
{
    sta.crash_code = reg_e;

    McuPanic(MCU_PANIC_SW_CRASH,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16  | RGLEDS_PIC_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16 | RGLEDS_DCCT_RED_MASK16);

}
/*---------------------------------------------------------------------------------------------------------*/
void StartRunningDsp()
/*---------------------------------------------------------------------------------------------------------*\
  This function will signal the DSP to run
\*---------------------------------------------------------------------------------------------------------*/
{
    dpcom.mcu.started = DSP_STARTED;                        // signal the DSP to run
}
/*---------------------------------------------------------------------------------------------------------*/
void WaitForDspStarted(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will wait for the DSP signalling its start.
  Problems are reported through the DSP version string in the DPRAM.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct TRunlog * runlog_ptr = (struct TRunlog *) NVRAM_RL_BUF_32;
    INT32U timeout = 100000;

    while (dpcom.dsp.started != DSP_STARTED && (timeout != 0))      // Wait for DSP to start
    {
        sleepUs(10);
        timeout--;
    }

    // If DSP is not standalone and DSP didn't start

    if ((!mst.dsp_is_standalone) && (dpcom.dsp.started != DSP_STARTED))
    {
        if (runlog_ptr->resets.dsp < 3)
        {
            INT16U rl_value;

            NVRAM_UNLOCK();
            runlog_ptr->resets.dsp++;
            NVRAM_LOCK();

            rl_value = 0x8000 + runlog_ptr->resets.dsp;
            RunlogWrite(FGC_RL_MAIN_REFUSED, (void *) &rl_value);

            // If the DSP is not running, reset the FGC. This is a temporarily hack to
            // fix the recurrent problem of the FGC not starting. The cause is still
            // unknown but could be due to the PLL not locking.
            Reset();
        }
        else
        {
            Set(ST_LATCHED, FGC_LAT_DSP_FLT);            // Report that DSP didn't start
            Set(sta.faults, FGC_FLT_FGC_HW);             // and set a FGC_HW fault

            NVRAM_UNLOCK();
            runlog_ptr->resets.dsp = 0;
            NVRAM_LOCK();
        }
    }
    else                        // else DSP is running
    {
        dev.dsp_version = dpcom.dsp.version;            // Take copy of DSP program version
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: start.c
\*---------------------------------------------------------------------------------------------------------*/
