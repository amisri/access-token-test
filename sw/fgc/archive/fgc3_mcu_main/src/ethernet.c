/*!
 *  @file     ethernet.h
 *  @defgroup FGC3
 *  @brief      Ethernet ethernet Interface Functions.
 *
 */

#define ETHER_COMM_GLOBALS
#define INLINE inline


// Includes
#include <ethernet.h>

#include <fbs.h>
#include <fgc_errs.h>
#include <fgc_gateway.h>
#include <hardware_setup.h>
#include <inter_fgc.h>
#include <lan92xx.h>
#include <led.h>
#include <os.h>
#include <sleep.h>
#include <sta.h>
#include <tsk.h>

// Defines
// ToDo get rid of these macros

/*----------------------------------- EthInit -------------------------------------------------------------*/
#define SUSPEND_INTERRUPT                           ( ETH_IRQ_CFG_P = 0                             )
#define SOFTWARE_RESET                              ( ETH_HW_CFG_P |= 0x00000001                    )
// note : look for 0x0001000 or 0x00000001 if we don't know endianness
#define DEVICE_NOT_READY                            ( (ETH_PMT_CTRL_P & 0x00010001) == 0            )

#define RIGHT_ENDIANNESS                            ( ETH_BYTE_TEST_P == 0x87654321                 )
#define OTHER_ENDIANNESS                            ( ETH_BYTE_TEST_P == 0x43218765                 )
#define SWITCH_ENDIANNESS                           ( ETH_WORD_SWAP_P = 0xFFFFFFFF                  )

#define WRONG_LAN_CHIP                              ( (ETH_ID_REV_P & 0xFFFF0000) != 0x92210000     )

#define ID_FROM_MAC_ADDR                            ( *((uint8_t *) ETH_MAC_ADDR_32) & 0xFF         )
#define WROND_ID(id)                                ( (id  < 1) || (id  > 64)                       )

#define ENABLE_FULL_DUPLEX                          ( ETH_MAC_CSR_DATA_P  = 0x0010000C              )
#define WRITE_MAC_CR                                ( ETH_MAC_CSR_CMD_P   = 0x80000001              )
#define ENABLE_TX                                   ( ETH_TX_CFG_P        |= 0x00000002             )

#define RESET_DROP_FRAME_COUNTER                    ( dummy_read = ETH_RX_DROP_P                    )

#define PACKET_AVAILABLE                            ( ETH_RX_FIFO_INF_P & 0x00FF0000                )
#define POP_PACKET_STATUS                           ( dummy_read = ETH_RX_STATUS_PORT_P             )

/*-------------------------------------EthISR--------------------------------------------------------------*/
#define INTERRUPT_ACTIVE                            ( ETH_IRQ_CFG_P & 0x00001000                    )
// Interrupt status is set even if not enabled. Thus, we test it is active AND enabled.
#define INTERRUPT_RX_ACTIVE                         ( ETH_INT_STS_P & ETH_RSFL_INT & ETH_INT_EN_P   )
#define INTERRUPT_TX_ACTIVE                         ( ETH_INT_STS_P & ETH_TSFL_INT & ETH_INT_EN_P   )

#define PACKET_AVAILABLE                            ( ETH_RX_FIFO_INF_P & 0x00FF0000                )
#define STATUS_AVAILABLE                            ( ETH_TX_FIFO_INF_P & 0x00FF0000                )

#define RX_STATUS_OK(rx_status)                     ( (rx_status & 0x000090DA) == 0                 )
#define TX_STATUS_OK(tx_status)                     ( (tx_status & 0x00008000) == 0                 )


// Enumerations

enum communication_devices
{
    ETHERNET_COMMUNICATION_FGC,
    ETHERNET_COMMUNICATION_GATEWAY,
    ETHERNET_COMMUNICATION_DEVICE_NUMBER
};


// Types

typedef struct local_vars_eth
{
    // To prepare fgc_ether_rsp_payload (length + data header) we don't want to allocate a full packet in memory
    INT8U eth_rsp_buf[sizeof(struct fgc_ether_rsp_payload) - sizeof(((struct fgc_fieldbus_rsp *)0)->rsp_pkt)];
    struct fgc_ether_rsp_payload   *  eth_rsp;

    void (*specific_receive_packet_functions    [ETHERNET_COMMUNICATION_DEVICE_NUMBER])(void);

    uint16_t payload_to_device  [FGC_ETHER_PAYLOAD_NUM_TYPES];
} local_vars_eth_t;


// Local Variables Declaration
local_vars_eth_t     local_eth;

// Static Inline functions declarations
static INLINE void EthAcknowledgeRxErrors(void);
static INLINE void EthAcknowledgeRxDroppedFrames(void);
static INLINE void EthReceivePacket(void);
static INLINE void EthCheckSentPacketStatus(void);
static INLINE void EthInitLinkingArray(void);
static INLINE void EthInitFunctionArrays(void);
static INLINE void EthHwInitEnableTxRx(void);
static INLINE void EthHwInitDeviceConfig(void);
static INLINE void EthHwInitClearRxFIFO(void);
static INLINE void EthHwInitInterrupt(void);
static INLINE void EthSetHeader(uint16_t id);

// External functions Definitions
void EthInit(void)
{
    SUSPEND_INTERRUPT;

    FbsInit();

    local_eth.eth_rsp = (struct fgc_ether_rsp_payload *) local_eth.eth_rsp_buf;

    SOFTWARE_RESET;
    EthInitLinkingArray();
    EthInitFunctionArrays();
    EthHwInit(100);                             // Wait for the Eth chip to respond with 100ms timeout

    // Go to standalone if no ethernet cable plugged at startup
    // ToDo Need to be review. This should be a situation where operation is not allowed
    // Standalone is only allowed if we plug special dongle

    if (!Lan92xxIsLinkUp())
    {
        FbsSetStandalone();
    }

    FgcGatewayInit();
    interFgcInit();
}

uint16_t EthHwInit(uint16_t timeout_ms)
{
    uint16_t id;

    // Wait for device to be ready, with given timeout

    while (DEVICE_NOT_READY && (timeout_ms != 0))
    {
        sleepMs(1);
        timeout_ms--;
    }

    if (DEVICE_NOT_READY)
    {
        // time out, the chip is not responding
        return 1;
    }

    // test if LAN9215 reg is accessible and set endianness ---

    if (RIGHT_ENDIANNESS)
    {
        // Ok Do nothing
    }
    else
    {
        if (OTHER_ENDIANNESS)
        {
            // Ok swap bytes
            SWITCH_ENDIANNESS;
        }
        else
        {
            // register issue
            return 2;
        }
    }

    // check if ID CODE is 9221 then overwrite bus access times
    if (WRONG_LAN_CHIP) // lan9221
    {
        // Unexpected chip
        return 3;
    }

#ifdef DEBUG_RUNNING_MAIN_WITHOUT_BOOT
    else
    {
        // Overwrite access timing
        // CS1 Wait Control Register 1
        BSC.CS1WCNT1.BIT.CSPWWAIT_20 = 1;//page access
        BSC.CS1WCNT1.BIT.CSPRWAIT_20 = 1;//page access
        BSC.CS1WCNT1.BIT.CSWWAIT_40 = 1;
        BSC.CS1WCNT1.BIT.CSRWAIT_40 = 1;

        // CS1 Wait Control Register 2
        BSC.CS1WCNT2.BIT.CSROFF_20 = 1;
        BSC.CS1WCNT2.BIT.CSWOFF_20 = 0;
        BSC.CS1WCNT2.BIT.WDOFF_20 = 1;
        BSC.CS1WCNT2.BIT.RDON_20 = 1;
        BSC.CS1WCNT2.BIT.WRON_20 = 0;
        BSC.CS1WCNT2.BIT.WDON_20 = 0;
        BSC.CS1WCNT2.BIT.CSON_20 = 0;
    }

#endif

    // Read fieldbus ID in external EEPROM, set mac address accordingly

    id = ID_FROM_MAC_ADDR;
    EthSetHeader(id);

    if (WROND_ID(id))
    {
        FbsSetStandalone();
        return 4;
    }

    fbs.id = id;
    Lan92xxSetMac(ethernet.tx_header.ethernet.src_addr);

    EthHwInitDeviceConfig();
    EthHwInitEnableTxRx();
    EthHwInitClearRxFIFO();
    EthHwInitInterrupt();

    ICU.IRQCR4.BYTE = 0x0C;     // Trigger IRQ4 on both falling edge and rising edge

    return 0;
}

void EthWatch(void)
{
    static BOOLEAN soft_reset_in_progress;

    if (fbs.net_type == NETWORK_TYPE_ETHERNET)
    {
        if (fbs.count_time_rcvd == 5)
        {
            fbs.count_time_rcvd++;
            FbsReconnect();
            FbsInit();
            FbsLeaveStandalone();
            fbs.id = *((uint8_t *) ETH_MAC_ADDR_32) & 0xFF;
            fbs.gw_online_f = TRUE;
        }
        else if (!fbs.time_rcvd_f && (fbs.count_time_rcvd != 0))        // If we have just lost GW
        {
            fbs.count_time_rcvd = 0;
            fbs.gw_online_f = FALSE;
            FbsDisconnect(FGC_PLL_NOT_LOCKED);
        }
    }

    //   In case of TX error or FIFO overrun we have to reset the chip and configure it again.
    //   This is not done within interrupt because EthHwInit() takes a while
    //   Rx error are tackle within ethernet interrupt

    if ((fbs.id != 0) && (((ETH_INT_STS_P & ETH_TXE_INT) != 0) || ((ETH_INT_STS_P & ETH_TDFO_INT) != 0)))
    {
        DisableNetworkInterrupt();
        fbs.id = FBS_STANDALONE_ID;
        ETH_HW_CFG_P |= 0x00000001; // Software reset
        soft_reset_in_progress = TRUE;
        ethernet.stats.soft_reset++;
        FbsDisconnect(FGC_RSP_TX_FAILED); // Cancel command response
    }

    if (soft_reset_in_progress && (FbsIsStandalone()))
    {
        // Try to initialize Eth interface (without timeout, not blocking). If succeed then fbs.id is set
        EthHwInit(0);

        if (!FbsIsStandalone())
        {
            soft_reset_in_progress = FALSE;
            ETH_INT_STS_P = ETH_INT_EN_P;       // Reset active interrupts
            EnableNetworkInterrupt();
        }
    }
}

void EthISR(void)
{
    while (INTERRUPT_ACTIVE)
    {
        if (INTERRUPT_RX_ACTIVE)
        {
            while (PACKET_AVAILABLE)
            {
                EthReceivePacket();
            }

            Lan92xxAckInterrupt(ETH_RSFL_INT);
        }

        if (INTERRUPT_TX_ACTIVE)
        {
            while (STATUS_AVAILABLE)
            {
                EthCheckSentPacketStatus();
            }

            Lan92xxAckInterrupt(ETH_TSFL_INT);
        }

        EthAcknowledgeRxErrors();
        EthAcknowledgeRxDroppedFrames();
    }
}

void EthCpyCmd(void * to, uint16_t length)
{
}

/*---------------------------------------------------------------------------------------------------------*/
// Declarations of Inline functions
/*--------------------------------------Functions Called from EthISR---------------------------------------*/
static INLINE void      EthReceivePacket(void)
/*---------------------------------------------------------------------------------------------------------*\
  Function called from the ISR.
  This function is called for each packet available in the FIFO. It treats the different error cases
  and calls the appropriate function depending on the payload type.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t                    rx_payload_type;
    uint16_t                    rx_device_type;
    uint32_t                    rx_fifo_status;

    ethernet.stats.frame_count.rcvd.all++;

    //read RX status from the port
    rx_fifo_status = ETH_RX_STATUS_PORT_P;

    if (RX_STATUS_OK(rx_fifo_status))
    {
        ethernet.rx_is_broadcast = Test(rx_fifo_status, 0x00002000);
        ethernet.rx_time_us      = TimeGetAbsUs();

        ethernet.current_rx_fifo_byte_length = (rx_fifo_status & 0x3FFF0000) >> 16;
        Lan92xxReadFifo(&ethernet.rx_header, sizeof(ethernet.rx_header), &ethernet.current_rx_fifo_byte_length);

        if (ethernet.rx_header.ethernet.ether_type == FGC_ETHER_TYPE)
        {
            rx_payload_type = (enum fgc_ether_payload_type) ethernet.rx_header.fgc.payload_type;

            if (rx_payload_type < FGC_ETHER_PAYLOAD_NUM_TYPES)
            {
                ethernet.stats.frame_count.rcvd.payload_types[rx_payload_type]++;

                rx_device_type = local_eth.payload_to_device[rx_payload_type];
                local_eth.specific_receive_packet_functions[rx_device_type]();
            }
            else
            {
                ethernet.stats.frame_count.rcvd.bad.payload_type++;
                ethernet.stats.frame_count.rcvd.bad.all++;
            }
        }
        else
        {
            ethernet.stats.frame_count.rcvd.bad.ether_type++;
            ethernet.stats.frame_count.rcvd.bad.all++;
        }
    }
    else
    {
        ethernet.stats.frame_count.rcvd.bad.crc++;
        ethernet.stats.frame_count.rcvd.bad.all++;
    }

    // All data in the FIFO must be read
    // Discard end of packet if there is still data.
    // Note: CRC is discarded
    if (ethernet.current_rx_fifo_byte_length != 0)
    {
        lan92xxDiscardPacket(ethernet.current_rx_fifo_byte_length);
    }
}

static INLINE void      EthCheckSentPacketStatus(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called for each packet available in the FIFO. It treats the different error cases
  and calls the appropriate function depending on the payload type.
  Function called from the ISR.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      tx_fifo_status;
    uint32_t      tx_packet_type;

    //read TX status
    tx_fifo_status = ETH_TX_STATUS_PORT_P;
    tx_packet_type = (tx_fifo_status & 0xFFFF0000) >> 16;

    // Note: no error does not ensure the packet is received by the GW
    // Indeed, even if there is no cable plugged, status is ok

    if (TX_STATUS_OK(tx_fifo_status) && (tx_packet_type < FGC_ETHER_PAYLOAD_NUM_TYPES))
    {
        ethernet.stats.frame_count.sent.all++;
        ethernet.stats.frame_count.sent.payload_types[tx_packet_type]++;
    }
    else
    {
        ethernet.stats.frame_count.sent.fail++;
    }
}

static INLINE void      EthAcknowledgeRxErrors(void)
/*---------------------------------------------------------------------------------------------------------*\
  Function called from the ISR.
\*---------------------------------------------------------------------------------------------------------*/
{
    if ((ETH_INT_STS_P & ETH_INT_EN_P & ETH_RXE_INT) != 0)    // RX Error
    {
        ethernet.stats.frame_count.rcvd.bad.all++;
        Lan92xxRxDump();
        Lan92xxAckInterrupt(ETH_RXE_INT);
    }
}

static INLINE void      EthAcknowledgeRxDroppedFrames(void)
/*---------------------------------------------------------------------------------------------------------*\
  Function called from the ISR.
\*---------------------------------------------------------------------------------------------------------*/
{
    if ((ETH_INT_STS_P & ETH_INT_EN_P & ETH_RXDF_INT) != 0)   //RX drop frame
    {
        uint16_t    number_of_dropped_frames = ETH_RX_DROP_P;   // Reset drop frame counter
        ethernet.stats.frame_count.rcvd.bad.dropped += number_of_dropped_frames;
        ethernet.stats.frame_count.rcvd.bad.all += number_of_dropped_frames;
        Lan92xxRxDump();
        Lan92xxAckInterrupt(ETH_RXDF_INT);
    }
}

static INLINE void      EthInitLinkingArray(void)
{
    local_eth.payload_to_device[FGC_ETHER_PAYLOAD_INTER_FGC]  = ETHERNET_COMMUNICATION_FGC;

    local_eth.payload_to_device[FGC_ETHER_PAYLOAD_TIME]   = ETHERNET_COMMUNICATION_GATEWAY;
    local_eth.payload_to_device[FGC_ETHER_PAYLOAD_STATUS] = ETHERNET_COMMUNICATION_GATEWAY;
    local_eth.payload_to_device[FGC_ETHER_PAYLOAD_CMD]    = ETHERNET_COMMUNICATION_GATEWAY;
    local_eth.payload_to_device[FGC_ETHER_PAYLOAD_RSP]    = ETHERNET_COMMUNICATION_GATEWAY;
    local_eth.payload_to_device[FGC_ETHER_PAYLOAD_PUB]    = ETHERNET_COMMUNICATION_GATEWAY;
}

static INLINE void      EthInitFunctionArrays(void)
{
    local_eth.specific_receive_packet_functions[ETHERNET_COMMUNICATION_GATEWAY] = &FgcGatewayReceivePacket;
    local_eth.specific_receive_packet_functions[ETHERNET_COMMUNICATION_FGC]     = &interFgcReceivePacket;
}

static INLINE void      EthSetHeader(uint16_t id)
{
    // mac address is as follow: 02:00:00:00:00:XX where XX is the fieldbus id.
    ethernet.tx_header.ethernet.src_addr[0] = 0x02;
    ethernet.tx_header.ethernet.src_addr[1] = 0x00;
    ethernet.tx_header.ethernet.src_addr[2] = 0x00;
    ethernet.tx_header.ethernet.src_addr[3] = 0x00;
    ethernet.tx_header.ethernet.src_addr[4] = 0x00;
    ethernet.tx_header.ethernet.src_addr[5] = id;
    ethernet.tx_header.ethernet.ether_type = FGC_ETHER_TYPE;
}

static INLINE void  EthHwInitEnableTxRx(void)
{
    // Configure MAC_CR : enable RX and TX
    ENABLE_FULL_DUPLEX;
    WRITE_MAC_CR;
    ENABLE_TX;
}

static INLINE void  EthHwInitDeviceConfig(void)
{
    ETH_GPIO_CFG_P  =   0x30000000;     // activate leds 1 and 2
    // ETH_HW_CFG_P:    Byte swap for FIFO port only  (w/o FIFO_SEL)
    //                  Set "Must Be One" bit (even though does not seem to affect operation)
    ETH_HW_CFG_P   |=   0x20000000 | 0x00100000;
}

static INLINE void  EthHwInitClearRxFIFO(void)
{
    uint32_t dummy_read;
    (void) dummy_read; // Null statement, prevent gcc from warning "set but not used"

    RESET_DROP_FRAME_COUNTER;

    while (PACKET_AVAILABLE)
    {
        POP_PACKET_STATUS;
        lan92xxDiscardPacket(0);
    }
}

static INLINE void  EthHwInitInterrupt(void)
{
    uint32_t      int_en_mask = 0;

    int_en_mask     =   ETH_RSFL_INT | ETH_TSFL_INT;     // RX and TX status received
    int_en_mask    |=  ETH_RXE_INT | ETH_TXE_INT;        // Rx error
    //    int_en_mask    |=  ETH_RSFF_INT | ETH_TSFF_INT | ETH_RXSTOP_INT; // Rx/Tx stop, fifo full
    int_en_mask    |=  ETH_TXSO_INT;    // Tx status overflow
    int_en_mask    |=  ETH_RXDF_INT;    // Rx dropped frame
    ETH_INT_STS_P   =   int_en_mask;     // Clear all interrupt status
    ETH_INT_EN_P    =   int_en_mask;
    ETH_IRQ_CFG_P  |=  0x01000100;     // enable interrupt line, minimal interrupt de-assertion time
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: ethernet.c
\*---------------------------------------------------------------------------------------------------------*/
