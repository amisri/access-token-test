/*!
 *  @file      mst.c
 *  @defgroup  FGC3:MCU
 *  @brief     Millisecond actions
 *
               Activity alignment:

                Class 61:

                0       FipRcvTimeVar   DiagTermNext      DiagSciStart/200ms
                1       Get DSP data    PrepareStatVar    WriteStatVar
                2       StaTsk
                3       FbsProcessTimeVar     RtdTsk/100ms
                4       PLL
                5       LogIearth
                6       LogIreg
                7       StaTsk
                8       LogIleads/1000ms
                9       LogThour/10s
                10      TskTiming/100ms DlsTsk/100ms
                11      RegFGC3Tsk
                12      StaTsk
                13      PubTsk
                14
                15
                16      LogIreg
                17      StaTsk
                18
                19      PrepareFipMsg
 */

#define MST_GLOBALS

#include <adc.h>
#include <ana_temp_regul.h>
#include <class.h>
#include <diag.h>
#include <DIMsDataProcess.h>
#include <events.h>
#include <fbs_class.h>
#include <fbs.h>
#include <fgc_runlog_entries.h>
#include <fip.h>
#include <led.h>
#include <macros.h>
#include <main.h>
#include <master_clk_pll.h>
#include <memmap_mcu.h>
#include <mst_class.h>
#include <mst.h>
#include <nvs.h>
#include <pulse_permit.h>
#include <read_PSU_adc.h>
#include <rtd.h>
#include <runlog_lib.h>
#include <sleep.h>
#include <sta.h>
#include <task_trace.h>
#include <trm.h>
#include <tsk.h>
#include <defprops.h>
#include <pub.h>
#include <regfgc3_task.h>
#include <dpcls.h>


// Constants

// IMPORTANT: changing MST_PROCESS_EVENTS might have side-effects. Read
// the comments in mcu/fgc3/61/inc/events_class.h::EventsInjection()

#define  MST_10_MS_TIME                0
#define  MST_SCI_DIAG_STA_VAR          0
#define  MST_FBS_STATUS_VAR_TIME       1
#define  MST_PUB_RESUME_TIME_VAR       3
#define  MST_RT_DISPLAY_MANUAL_PM      3
#define  MST_PLL_SYNC                  4
#define  MST_LOG_IREG_PHASE            6
#define  MST_CHECK_FIELDBUS            7
#define  MST_LOG_ILEADS_PHASE          8
#define  MST_PLL_ITERATION            10
#define  MST_REGFGC3_PROG_ITERATION   11

#define  MST_CLEAR_EVENTS_PROCESSED   15
#define  MST_PREPARE_MSG              19

#define  MST_ETHER_RSP_FLUSH_MS         FGC_ETHER_RSP_FLUSH_MS
#define  MST_ETHER_PUB_FLUSH_MS         FGC_ETHER_PUB_FLUSH_MS
#define  MST_PENDING_ADC_RESET        1
#define  MST_REFRESH_ANALOG_MEASURS   1
#define  MST_PROCESS_EVENTS          12


// Internal function declarations

static void MstUpdateTiming(void);
static void MstUpdateFieldbus(void);
static void MstUpdateGwPermit(void);
static void MstUpdateDiag(void);
static void MstUpdateLedStatus(void);
static void MstUpdateCpuUsage(void);
static void MstUpdateDspStatus(void);
static void MstUpdateMsDuration(void);


// Internal function definitions

static void MstUpdateDiag(void)
{
    static INT32U last_period_start = 0;
    static INT32U dim_sync_reset_prev = 0;
    static INT32U dim_mismatched_prev = 0;
    static INT8U  dim_sync_counter = 0;

    // Every second: Check for DIAG faults and run set command counter

    if (TimeGetUtcTimeMs() == 0)
    {
        if ((mst.time.unix_time - last_period_start) >= 60)
        {
            // Set the latched condition FGC_LAT_DIMS_EXP_FLT only if during
            // periods of 1 minute the rate of faults is larger than ten (10).

            if ((qspi_misc.expected_detected_mismatches - dim_mismatched_prev) >= DIAG_EXPEC_LATCHED_FAULT)
            {
                Set(ST_LATCHED, FGC_LAT_DIMS_EXP_FLT);
            }

            // Set the latched condition FGC_LAT_DIM_SYNC_FLT if during three
            // consecutive minutes the number of errors have increased.

            if ((diag.sync_resets - dim_sync_reset_prev) > 0)
            {
                if (++dim_sync_counter == 3)
                {
                    Set(ST_LATCHED, FGC_LAT_DIM_SYNC_FLT);
                    dim_sync_counter = 0;
                }
            }
            else
            {
                dim_sync_counter = 0;
            }

            last_period_start = mst.time.unix_time;
            dim_sync_reset_prev = diag.sync_resets;
            dim_mismatched_prev = qspi_misc.expected_detected_mismatches;
        }


        // Command counter

        if (mst.set_cmd_counter)
        {
            mst.set_cmd_counter--;
        }
    }
}

static void MstUpdateTiming(void)
{
    // This function is called at 10Hz to work out the task/ISR/MstTsk
    // timing - the results are put in the diag data structure.

    INT16U ii;

    // Transfer Task and ISR timing

    for (ii = 0; ii < (TSK_BGP + 1); ii++)
    {
        diag.data.r.software[TASK_RUN_TIME][ii] = os.tsk[ii].task_run_time;
        diag.data.r.software[ISR_RUN_TIME][ii]  = os.tsk[ii].isr_run_time;

        os.tsk[ii].task_run_time = 0;
        os.tsk[ii].isr_run_time  = 0;
    }

    // Transfer MstTsk timing

    for (ii = 0; ii < 20; ii++)
    {
        // Copy Max MstTsk time in us  for previous 100ms

        diag.data.r.ms_duration[ii] = mst.max_duration[ii];
        mst.max_duration[ii] = 0;
    }
}

static void MstUpdateFieldbus(void)
{
    OS_CPU_SR cpu_sr;

    if (mst.ms_mod_20 == MST_SCI_DIAG_STA_VAR)
    {
        if (terminal.mode == TRM_DIAG_MODE)
        {
            DiagTermNext();
        }

        FbsPrepareStatVar();
    }

    // Millisecond 1 of 20: Trigger FIP task to write status variable into uFIP

    if (mst.ms_mod_20 == MST_FBS_STATUS_VAR_TIME && !FbsIsStandalone())
    {
        if (fbs.net_type == NETWORK_TYPE_FIP)
        {
            // If PLL is locked, messaging has just been enabled and
            // uFIP transmit buffer is empty (EMP_MST=1)

            if (fbs.pll_locked_f && (fip.start_tx || (FipMsgsa() & 0x02) != 0))
            {
                FbsWriteMsg();
            }
        }

        FbsWriteStatVar();
    }

    // Millisecond 3 of 20: Resume PubTsk and if new time var received then process time var

    if (mst.ms_mod_20 == MST_PUB_RESUME_TIME_VAR)
    {
        if (fbs.net_type == NETWORK_TYPE_FIP)
        {
            OS_ENTER_CRITICAL();
            OSTskResumeLP(TSK_PUB);
            OS_EXIT_CRITICAL();
        }

        if (fbs.time_rcvd_f)
        {
            FbsProcessTimeVar();
        }
    }

    // Millisecond 7 of 20: Check fieldbus

    if (mst.ms_mod_20 == MST_CHECK_FIELDBUS)
    {
        if (!FbsIsStandalone())
        {
            FbsCheck();
        }
    }

    // Millisecond 19 of 20: Trigger FIP task to prepare response message

    if (mst.ms_mod_20 == MST_PREPARE_MSG)
    {
        if ((fbs.net_type == NETWORK_TYPE_FIP) && (!FbsIsStandalone()))
        {
            FbsPrepareMsg();

            if (!fbs.time_rcvd_f)
            {
                // Prepare repeat Var 7 detection

                fbs.last_runlog_idx = 0x0000;
            }
            else
            {
                // Prepare missed time var detection

                fbs.time_rcvd_f = FALSE;
            }
        }

        FbsWatch();
    }

    // Every Millisecond : Send Rsp/Pub packets
    // Millisecond 15: Clear the time received/time processed flags on the Fieldbus

    if (fbs.net_type == NETWORK_TYPE_ETHERNET)
    {
        // Send only full ethernet packet or flush PUB/RSP stream once every 20ms

        if (fbs.gw_online_f                                                 &&
            ((FbsStreamGetSize(&fbs.cmd) > sizeof(struct fgc_fieldbus_rsp)) ||
             (FbsStreamGetSize(&fbs.pub) > sizeof(struct fgc_fieldbus_rsp)) ||
             (mst.ms_mod_20 == MST_ETHER_PUB_FLUSH_MS) ||
             (mst.ms_mod_20 == MST_ETHER_RSP_FLUSH_MS)))
        {

            FbsPrepareMsg();
            FbsWriteMsg();
        }

        if (mst.ms_mod_20 == MST_CLEAR_EVENTS_PROCESSED)
        {
            // For Ethernet, time is received at ms 18/19. For ETH/FIP,
            // EventsProces is called on ms 12, so reset flags at ms 15.

            fbs.time_processed_f = FALSE;
            fbs.time_rcvd_f = FALSE;
        }
    }
}

static void MstUpdateGwPermit(void)
{
    // Sector Access Flag: The flag used by the C32 dpcls.mcu.sector_access is
    // linked to the settable property FGC.SECTOR_ACCESS. If this is disabled
    // by the user when the real sector access flag received from the gateway
    // is set (fbs.sector_access), then the C32 flag must be re-asserted.

    if (fbs.sector_access_gw)
    {
        fbs.sector_access = FGC_CTRL_ENABLED;
    }
}

static void MstUpdateLedStatus(void)
{
    // Millisecond  11: updated the LED warning and fault status.
    // Millisecond 811: reset the LEDs for them to blink if warning or fault.

    if (TimeGetUtcTimeMs() == 11)
    {
        if (Test(FAULTS, FGC_FAULTS) || Test(WARNINGS, FGC_WARNINGS))
        {
            LED_SET(FGC_RED);
        }

        if (!Test(FAULTS, FGC_FAULTS))
        {
            LED_SET(FGC_GREEN);
        }
    }
    else if (TimeGetUtcTimeMs() == 811)
    {
        LED_RST(FGC_RED);
        LED_RST(FGC_GREEN);
    }
}

static void MstUpdateCpuUsage(void)
{
    static uint32_t       next_run_log_enry[]   = { 0, 0 };
    static uint32_t const run_log_inhibit_s     = 60 * 60;  // 1 hour

    static INT16U cpu32_usage_ms;               // CPU usage in % (max since start of second)
    static INT16U cpu32_usage_ext_ms;           // CPU usage in % (max since start of second)

    INT32U cpu32;

    // Update maximum DSP usage (%)

    cpu32 = (INT16U) dpcom.dsp.cpu_usage;

    uint32_t utc_now = mst.time.unix_time;

    if (   cpu32 > 100
        && utc_now > next_run_log_enry[0])
    {
        RunlogTimestamp();
        cpu32 =  cpu32 + 0x10000;
        RunlogWrite(FGC_RL_DSP_RT, (void *)&cpu32);

        next_run_log_enry[0] = utc_now + run_log_inhibit_s;
    }

    if (cpu32 > cpu32_usage_ms)
    {
        cpu32_usage_ms = cpu32;
    }

    cpu32 = (INT16U) dpcom.dsp.cpu_usage_ext_counter;

    if (   cpu32 > 100
        && utc_now > next_run_log_enry[1])
    {
        RunlogTimestamp();
        cpu32 =  cpu32 + 0x20000;
        RunlogWrite(FGC_RL_DSP_RT, (void *)&cpu32);

        next_run_log_enry[1] = utc_now + run_log_inhibit_s;
    }

    if (cpu32 > cpu32_usage_ext_ms)
    {
        cpu32_usage_ext_ms = cpu32;
    }

    // Save maximum DSP usage and MCU usage on millisecond 0

    if (!TimeGetUtcTimeMs())
    {
        mst.cpu_usage.dsp = cpu32_usage_ms;
        mst.cpu_usage.dsp_ext_counter = cpu32_usage_ext_ms;
        cpu32_usage_ms = 0;
        cpu32_usage_ext_ms = 0;
        mst.cpu_usage.mcu = (50000 - diag.data.r.software[TASK_RUN_TIME][TSK_BGP]) / 500;
    }
}

static void MstUpdateDspStatus(void)
{
    static uint32_t       next_run_log_enry[]   = { 0, 0, 0, 0 };
    static uint32_t const run_log_inhibit_s     = 60 * 60;  // 1 hour

    static INT16U const cpu_mst_freq = 1000;
    static INT16U const expected_dsp_irqs_count_low  = 10000;
    static INT16U const expected_dsp_irqs_count_high = 10000;
    static uint32_t prev_dsp_ms_irq_alive_counter;      // Last DSP alive 1ms IRQ counter.
    static BOOLEAN dsp_bg_is_alive;                     // DSP BG task running
    static uint32_t cpu_isr_cnt;                        // Count CPU isr tick every second
    static uint32_t last_rt_run_log_1 = 0;
    static uint32_t last_rt_run_log_2 = 0;

    uint32_t count_dsp_irqs_1s;                         // count the number of DSP interrupts over 1 second.

    uint32_t utc_now = mst.time.unix_time;

    cpu_isr_cnt++;

    // The DSP background task is responsible for zeroing the counter. If it
    // does not do so within 1 second, set the corresponding fault.

    if (dpcom.dsp.mcu_counter_dsp_alive.part.lo++ > cpu_mst_freq)
    {
        if (   dsp_bg_is_alive
            && utc_now > next_run_log_enry[0])
        {
            dsp_bg_is_alive = FALSE;

            Set(ST_LATCHED, FGC_LAT_DSP_FLT);
            Set(FAULTS, FGC_FLT_FGC_HW);

            RunlogTimestamp();
            RunlogWrite(FGC_RL_DSP_BG, NULL);           // DSP BG task failed

            next_run_log_enry[0] = utc_now + run_log_inhibit_s;
        }
    }
    else
    {
        dsp_bg_is_alive = TRUE;
    }

    // Every second evaluate the DSP frequency by counting the number of interrupts.

    if (mst.ms == 0)
    {
        count_dsp_irqs_1s = mst.dsp_irq_alive_counter -  prev_dsp_ms_irq_alive_counter;
        mst.dsp_freq_khz = (FP32)count_dsp_irqs_1s / 1000.0;

        if (fbs.pll_locked_f)
        {
            if (count_dsp_irqs_1s < expected_dsp_irqs_count_low ||
                count_dsp_irqs_1s > expected_dsp_irqs_count_high)
            {
                if (   (TimeGetUtcTime() - last_rt_run_log_1) > 86400L
                    && utc_now > next_run_log_enry[1])
                {                
                    RunlogTimestamp();
                    RunlogWrite(FGC_RL_DSP_RT, (void *)&dpcom.dsp.run_code);
                    RunlogWrite(FGC_RL_DSP_RT, (void *)&count_dsp_irqs_1s);
                    RunlogWrite(FGC_RL_DSP_RT, (void *)&dpcom.dsp.debug[0]);

                    last_rt_run_log_1 = TimeGetUtcTime();

                    next_run_log_enry[1] = utc_now + run_log_inhibit_s;
                }

                if ((dpcom.dsp.run_code & 0x0000FFFF) == RUNCODE_DSP_PANIC)
                {
                    RunlogTimestamp();
                    RunlogWrite(FGC_RL_DSP_PANIC, (void *)&dpcls.dsp.panic.panic_code);
                }                
            }

            if (   cpu_isr_cnt != cpu_mst_freq
                && utc_now > next_run_log_enry[2])
            {
                RunlogTimestamp();
                RunlogWrite(FGC_RL_MCU_RT, (void *)&cpu_isr_cnt);

                next_run_log_enry[2] = utc_now + run_log_inhibit_s;
            }
        }

        prev_dsp_ms_irq_alive_counter = mst.dsp_irq_alive_counter;

        cpu_isr_cnt = 0;
    }

    if (mst.ms_mod_100 == 2)
    {
        if ((dpcom.mcu.runcode_handshake == 0) && (dpcom.dsp.run_code & 0x0000FFFF))
        {
            if (   (TimeGetUtcTime() - last_rt_run_log_2) > 86400L
                && utc_now > next_run_log_enry[3])
            {
                RunlogTimestamp();
                RunlogWrite(FGC_RL_DSP_RT, (void *)&dpcom.dsp.run_code);
                RunlogWrite(FGC_RL_DSP_RT, (void *)&dpcom.dsp.debug[0]);
                RunlogWrite(FGC_RL_DSP_RT, (void *)&dpcom.dsp.debug[1]);
                RunlogWrite(FGC_RL_DSP_RT, (void *)&dpcom.dsp.debug[2]);
                RunlogWrite(FGC_RL_DSP_RT, (void *)&dpcom.dsp.debug[3]);
                RunlogWrite(FGC_RL_DSP_RT, (void *)&dpcom.dsp.debug[4]);
                RunlogWrite(FGC_RL_DSP_RT, (void *)&dpcom.dsp.debug[5]);

                next_run_log_enry[3] = utc_now + run_log_inhibit_s;
                
                dpcom.mcu.runcode_handshake = 1;

                last_rt_run_log_2 = TimeGetUtcTime();
            }
        }
        else
        {
            dpcom.mcu.runcode_handshake = 0;
        }
    }
}

static void MstUpdateMsDuration()
{
    mst.duration = (INT16U)(UTC_US_P - mst.isr_start_us);

    if (mst.duration > mst.max_duration[mst.ms_mod_20])
    {
        mst.max_duration[mst.ms_mod_20] = mst.duration;
    }

    // Pass MstTsk time to DSP for SPY

    dpcom.mcu.mst_time = mst.duration;
}


// External function definitions

void MstTsk(void * unused)
{
    OS_CPU_SR cpu_sr;

    // Main Loop - synchronised to ms interrupts

    for (;;)
    {
        TskTraceReset();

        OSTskSuspend();

        TskTraceInc();

        // Read the DSP counters, to check their status later on in this function.

        mst.dsp_irq_alive_counter = dpcom.dsp.ms_irq_alive_counter.int32u;

        mst.prev_time = mst.time;
        mst.time.unix_time = TimeGetUtcTime();
        mst.time.us_time   = TimeGetUtcTimeUs();
        mst.ms         = TimeGetUtcTimeMs();
        mst.ms_mod_5   = mst.ms % 5;
        mst.ms_mod_10  = mst.ms % 10;       // Modulo  10 counter for  10 ms period activity
        mst.ms_mod_20  = mst.ms % 20;       // Modulo  20 counter for  20 ms period activity
        mst.ms_mod_100 = mst.ms % 100;      // Modulo 100 counter for 100 ms period activity
        mst.ms_mod_200 = mst.ms % 200;      // Modulo 200 counter for 200 ms period activity

        // Save time of start of 20ms for logging

        if (mst.ms_mod_10 == MST_10_MS_TIME)
        {
            mst.time10ms = mst.time;
        }

        LedManage(mst.ms);

        TskTraceInc();

        QSPIbusStateMachine();

        TskTraceInc();

        MstClassProcess();

        // Some converters use the spare interlock digital input as a pulse
        // permit signal to block and unblock the converter.

        PulsePermitProcess();

        TskTraceInc();

        MstClassPublishData();

        TskTraceInc();

        // Tell STA task to publish the state properties at 2 Hz

        if (TimeGetUtcTimeMs() == 0 || TimeGetUtcTimeMs() == 500)
        {
            sta.tick_2hz_f = TRUE;
        }

        MstUpdateGwPermit();

        TskTraceInc();

        if (mst.ms_mod_20 == 2)
        {
            Read_PSU_voltage();
        }

        TskTraceInc();

        MstClassLog();

        TskTraceInc();

        if (mst.ms_mod_20 == MST_PROCESS_EVENTS)
        {
            eventsProcess();
        }

        TskTraceInc();

        if (mst.ms_mod_200 == MST_PENDING_ADC_RESET &&
            adc.pending_reset_req_f)
        {
            AdcFiltersResetRequest(FALSE);
        }

        TskTraceInc();

        if (mst.ms_mod_200 == MST_REFRESH_ANALOG_MEASURS)
        {
            dpcls.mcu.adc.vref_temp.meas = AnaTempGetTemp();

            // Publish AMPS_200MS

            PubProperty(&PROP_ADC_AMPS_200MS, NON_PPM_USER, FALSE);
        }

        MstUpdateFieldbus();

        TskTraceInc();

        if (mst.ms_mod_20 == MST_PLL_ITERATION)
        {
            MasterClockAnaloguePllIteration();
        }

        TskTraceInc();

        // Milliseconds 2 of 5: Trigger state task (STA task) and check for device control actions

        if (mst.ms_mod_5 == DEV_STA_TSK_PHASE)
        {
            // If DEVICE.RESET/BOOT/CRASH command pending

            if (dev.ctrl_counter)
            {
                dev.ctrl_counter--;

                if (!dev.ctrl_counter)
                {
                    stop();
                }
            }

            sta.watchdog++;

            // If StaTsk watchdog failed to complete within 20ms

            if (sta.watchdog > 4)
            {
                RunlogTimestamp();
                RunlogWrite(FGC_RL_STATSK, &sta.watchdog);

                sta.watchdog = 0;
            }

            // Set PM trig property from FIP ack byte

            dev.log_pm_trig = fbs.u.fieldbus_stat.ack & (FGC_EXT_PM_REQ | FGC_SELF_PM_REQ);

            // Wake up STA task

            OSTskResume(TSK_STA);
        }

        TskTraceInc();

        // Millisecond 3 of 100: Real-time Display and manual post mortem flags

        if (mst.ms_mod_100 == MST_RT_DISPLAY_MANUAL_PM)
        {
            if ((terminal.mode == TRM_EDITOR_MODE) && rtd.enabled)
            {
                OSTskResume(TSK_RTD);
            }
        }

        // Every millisecond: Drive terminal interface

        TrmMsOut();

        TskTraceInc();

        // Every millisecond: Update time for DSP (ready for next millisecond interrupt)

        if (TimeGetUtcTimeMs() >= 999)
        {
            shared_mem.seconds_since_powerup++;
            shared_mem.seconds_since_reboot++;
        }

        MstUpdateDiag();

        TskTraceInc();

        // Every second - Internal Analogue Card temperature regulation

        if (TimeGetUtcTimeMs() == 12 && AnaTempGetStatus())
        {
            AnaTempRegul();
        }

        TskTraceInc();

        MstUpdateLedStatus();

        TskTraceInc();

        // Every 100ms - Task timings, wake up Dallas task and check config state/mode

        if (mst.ms_mod_100 == 10)
        {
            MstUpdateTiming();

            OSTskResume(TSK_DLS);

            if (sta.config_mode == FGC_CFG_MODE_SYNC_FAILED)
            {
                StaSetConfigState(FGC_CFG_STATE_UNSYNCED);
            }

            if (sta.config_state == FGC_CFG_STATE_UNSYNCED || nvs.n_corrupted_indexes)
            {
                Set(WARNINGS, FGC_WRN_CONFIG);
            }
            else
            {
                Clr(WARNINGS, FGC_WRN_CONFIG);
            }
        }

        TskTraceInc();

        MstUpdateCpuUsage();

        TskTraceInc();

        if (!mst.dsp_is_standalone)
        {
            MstUpdateDspStatus();
        }

        TskTraceInc();

        // Every millisecond: Resume FcmTsk, TcmTsk and TrmTsk if suspended

        OS_ENTER_CRITICAL();
        OSTskResumeLP(TSK_FCM);
        OSTskResumeLP(TSK_TCM);
        OSTskResumeLP(TSK_TRM);

        TskTraceInc();

        if (fbs.net_type == NETWORK_TYPE_ETHERNET)
        {
            OSTskResumeLP(TSK_PUB);
        }

        OS_EXIT_CRITICAL();

        TskTraceInc();

        MstUpdateMsDuration();

        TskTraceInc();

        interFgcStartMs();
    }
}
