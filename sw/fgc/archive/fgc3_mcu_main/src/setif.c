/*---------------------------------------------------------------------------------------------------------*\
 File:          setif.c

 Purpose:       FGC MCU Software - Set Command Condition Functions
\*---------------------------------------------------------------------------------------------------------*/

#define SETIF_GLOBALS
#define DEFPROPS_INC_ALL    // defprops.h

#include <cc_types.h>
#include <setif.h>
#include <cal.h>            // for REF_ARMED_FUNC_TYPE
#include <cmd.h>            // for tcm global variable and for struct cmd
#include <crate.h>          // for crate global variable
#include <defprops.h>
#include <pars.h>
#include <dev.h>            // for dev global variable
#include <dpcls.h>          // for dpcls global variable
#include <fbs.h>            // for fbs global variable
#include <fbs_class.h>      // for STATE_OP
#include <fgc_errs.h>       // for FGC_BAD_STATE
#include <macros.h>         // for Test()
#include <mst.h>            // for mst global variable
#include <sta.h>            // for sta global variable
#include <state_manager.h>      // for vs global variable
#include <defconst.h>       // for FGC_INTERFACE_*
#include <set.h>            // for SetRefLockInternal()
#include <shared_memory.h>  // for FGC_MP_MAIN_RUNNING
#include <pc_state.h>

/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifFbsCmd(struct cmd * c)
/*---------------------------------------------------------------------------------------p------------------*\
  Set If Condition:     Is command from the FIELDBUS (FIP or ETH)?
                        This is mainly to ensure that the command is passed via a tool with secured access,
                        such as FGCRun+.
                        Note: Commands typed on the remote console are considered non-Fieldbus since in
                        that case the Fieldbus is only a transport pipe for the SCM task.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (c != &tcm)
    {
        return (0);
    }

    return (FGC_USE_FGCRUN_PLUS);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifConfigModeOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Can the user set CONFIG.MODE?
\*---------------------------------------------------------------------------------------------------------*/
{
  
    if (   STATE_OP != FGC_OP_CALIBRATING
        && sta.config_state != FGC_CFG_STATE_STANDALONE
        && sta.config_mode == FGC_CFG_MODE_SYNC_NONE
        )
    {
        return (0);
    }

    return (FGC_BAD_STATE);
    

}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifOpNormalSim(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is operational state NORMAL or SIMULATION and the Dallas ID scan
                        or the configuration is not in progress?
\*---------------------------------------------------------------------------------------------------------*/
{
    if ((STATE_OP == FGC_OP_NORMAL ||
         STATE_OP == FGC_OP_SIMULATION)                  &&
        shared_mem.mainprog_seq == FGC_MP_MAIN_RUNNING  && // Dallas ID scan in progress
        sta.config_mode != FGC_CFG_MODE_SYNC_FGC    // configuration in progress
       )
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifStandalone(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is FGC running standalone (no FIP connector plugged in)?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (sta.config_state == FGC_CFG_STATE_STANDALONE)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifOpTestingSim(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is operational state TEST or SIMULATION?
\*---------------------------------------------------------------------------------------------------------*/
{
    if ((STATE_OP == FGC_OP_TEST) || (STATE_OP == FGC_OP_SIMULATION))
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifRefDac(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is operational state TEST
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_OP == FGC_OP_TEST)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifRefOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: PPM reference is always accepted, whilst a non-PPM reference (0) is only
                    accepted when the state is FGC_PC_IDLE.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U errnum = FGC_OK_NO_RSP;

    if (c->mux_idx == 0 && STATE_PC != FGC_PC_IDLE)
    {
        errnum = FGC_BAD_STATE;
    }

    return (errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifPcCycling(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is PC State ARMED?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_PC == FGC_PC_CYCLING)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}

/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifPcArmed(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is PC State ARMED?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_PC == FGC_PC_ARMED)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifPcNotCycling(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is STATE.PC not CYCLING?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_PC != FGC_PC_CYCLING)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifCalActiveOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Can CAL.ACTIVE be set
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_OP == FGC_OP_NORMAL &&
        sta.config_mode == FGC_CFG_MODE_SYNC_NONE &&
        !SetifPcOff(c) &&
        (INT16U)dpcls.dsp.meas.i_zero_f)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifMpxOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is operational state TEST and adc interface is present?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (shared_mem.analog_card_model
        && (STATE_OP == FGC_OP_TEST))
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifModeOpOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Can the user set MODE.OP?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (PcStateBelowEqual(FGC_PC_STOPPING) &&
         STATE_OP != FGC_OP_CALIBRATING &&
        (crate.type == FGC_CRATE_TYPE_RECEPTION ||
         !(STATE_OP == FGC_OP_NORMAL 
#if (FGC_CLASS_ID == 61)
          && !Test(ST_UNLATCHED, FGC_UNL_LOW_CURRENT)
#endif          
          )))
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifOpNotCal(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is operational state not CALIBRATING
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_OP != FGC_OP_CALIBRATING)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifSwitchOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: VS is OFF and all working DCCTs are report zero current.
  If ComHV-PS (backplane #13), the polarity can be switched in IDLE, the other converters on in OFF
\*---------------------------------------------------------------------------------------------------------*/
{
    BOOLEAN state_ok;
    BOOLEAN polarity_ok;

    state_ok = PcStateBelowEqual((crate.type != FGC_CRATE_TYPE_COMHV_PS ? FGC_PC_BLOCKING : FGC_PC_IDLE));

    polarity_ok = (dpcls.mcu.vs.polarity.state == FGC_POL_POSITIVE ||
                   dpcls.mcu.vs.polarity.state == FGC_POL_NEGATIVE);

    if (STATE_OP == FGC_OP_NORMAL &&
        state_ok                  &&
        polarity_ok               &&
        dpcls.dsp.meas.i_zero_f)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifResetOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is the power converter OFF or STOPPING
\*---------------------------------------------------------------------------------------------------------*/
{
    if (PcStateBelowEqual(FGC_PC_ON_STANDBY) ||
        STATE_VS == FGC_VS_INVALID)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifPcOff(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is Power Converter OFF?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_PC == FGC_PC_FLT_OFF ||
        STATE_PC == FGC_PC_OFF)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifPcOffIdle(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is Power Converter OFF or IDLE?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (!SetifPcOff(c) ||
        STATE_PC == FGC_PC_IDLE)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetifRefUnlock(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    /*
     * Set If Condition: Is REF function lock DISABLED?
     *   NB: Keep the Bad State error for non-cycling or non-PPM operations, in
     *   the case the reference is already ARMED. In this case, the procedure
     *   to edit the reference is first to disarm it (with a S REF.FUNC.TYPE NONE)
     *   then to edit it and arm it again.
     */

    INT16U errnum = FGC_OK_NO_RSP;

    // If user 0 and ARMED or RUNNING state, return "bad state" error

    if (c->mux_idx == 0 && (PcStateEqual(FGC_PC_ARMED) || PcStateEqual(FGC_PC_RUNNING)))
    {
        errnum = FGC_BAD_STATE;
    }

    return (errnum);
}



INT16U SetifSlaveOk(struct cmd * c)
{
#if (FGC_CLASS_ID == 63)
    if(interFgcIsMaster() == false && pcStateBelowEqual(FGC_PC_OFF))
#else
    if(interFgcIsMaster() == false && PcStateBelowEqual(FGC_PC_OFF))
#endif
    {
        return 0;
    }

    return FGC_BAD_STATE;
}



INT16U SetifMasterOk(struct cmd * c)
{
#if (FGC_CLASS_ID == 63)
    if(interFgcIsSlave() == false && pcStateBelowEqual(FGC_PC_OFF))
#else
    if(interFgcIsSlave() == false && PcStateBelowEqual(FGC_PC_OFF))
#endif
    {
        return 0;
    }

    return FGC_BAD_STATE;
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: setif.c
\*---------------------------------------------------------------------------------------------------------*/

