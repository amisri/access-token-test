/*!
 *  @file     fbs.h
 *  @defgroup FGC3:MCU main
 *  @brief    Fieldbus communications Functions.
 *
 */

#define FBS_GLOBALS                 // define fbs global variable

// Includes

#include <fbs.h>
#include <string.h>

#include <dev.h>
#include <diag.h>
#include <ethernet.h>
#include <extend_mask.h>
#include <fbs_class.h>
#include <fgc_errs.h>
#include <fgc_gateway.h>
#include <fip.h>
#include <led.h>
#include <macros.h>
#include <mst.h>
#include <pars_service.h>
#include <pll.h>
#include <pub.h>
#include <regfgc3_prog.h>
#include <runlog_lib.h>
#include <sta.h>
#include <task_trace.h>
#include <trm.h>
#include <defprops.h>
#include <fgc/fgc_db.h>

// Macro arguments are expanded except if with # or ##, so we need 2 level of indirection
#define CLASS_FIELD_STRING(name)        c##name
#define CLASS_FIELD_STRING2(name)       CLASS_FIELD_STRING(name)
#define CLASS_FIELD                     CLASS_FIELD_STRING2(FGC_CLASS_ID)   // c##FGC_CLASS_ID

/*!
 * This function checks that the command is not stuck in FGC_EXECUTING
 */
static void FbsCmdCheck(void);

/*!
 * This function will read and process a command packet message.  The IsrFbs() has already established
 * that a valid message is waiting in the buffer.  IsrFbs() prepares fbs.pkt_len and fbs.rcvd_header with
 * the length and header word.  Get commands are limited by this function to one packet.  Set commands can be
 * any length and will be executed on the fly.  During the reception of packets the status will be
 * FGC_RECEIVING until the last packet is received when the status will change to FGC_EXECUTING.  The gateway
 * will not attempt to send a new command when the status is executing, but it may do so if the status is
 * receiving.
 */
static void FbsReadCmdMsg(void);

/*!
 * This function will copy from either the Eth/Fip cmd output queue or the remote terminal output queue into
 * the linear response buffer.  It is called from FbsPrepareMsg().
 *
 * @param
 */
static void FbsCpyFromQueue(struct queue * q, INT16U * out_idx, INT16U * rsp_idx, INT16U max_rsp_idx);

/*!
 * This function is called from FbsPrepareMsg to prepare the response packet by taking data from the
 * specified stream (pub or cmd).
 *
 * @param stream
 * @param pkt_type
 */
static void FbsQData(struct fbs_stream * stream, INT8U pkt_type);

static struct
{
    INT16U              ext_pm_req_count;               // Tx counter for EXT_PM_REQ
    BOOLEAN             cmd_ack_f;                      // Command has been acknowledged
    INT8U               send_header;                    // Send packet header
    INT8U               last_ack;                       // Last sent ack byte for PM REQ run log trigger
    INT16U              diag_list_idx[FGC_DIAG_N_LISTS];// Diagnostic channel list index
    char                termq_buf    [FBS_TERMQ_SIZE];  // Remote terminal output buffer
} local_fbs;

void FbsSelectBus(void)
{

    fbs.net_type = shared_mem.network_card_model;

    // by default we fill with FIP bus
    // because if fbs.net_type is unknown
    // we NEED an ISR or the system will crash
    FbsISR = &FipISR;
    FbsCpyCmd = &FipCpyCmd;
    FbsSendStatVar = &FipSendStatVar;
    FbsSendMsg = &FipSendMsg;
    FbsWatch = &FipWatch;

    // fbs.cmd_rsp_len = sizeof(((struct fgc_fieldbus_rsp *)0)->rsp_pkt)
    fbs.cmd_rsp_len = FGC_FIP_MAX_RSP_LEN - sizeof(struct fgc_fieldbus_rsp_header);

    if (fbs.net_type == NETWORK_TYPE_ETHERNET)
    {
        FbsISR = &EthISR;
        FbsCpyCmd = &EthCpyCmd;
        FbsSendStatVar = &FgcGatewaySendStatVar;
        FbsSendMsg = &FgcGatewaySendMsg;
        FbsWatch = &EthWatch;

        // fbs.cmd_rsp_len = sizeof(((struct fgc_fieldbus_rsp *)0)->rsp_pkt)
        fbs.cmd_rsp_len = FGC_ETHER_MAX_RSP_LEN - sizeof(struct fgc_fieldbus_rsp_header);

    }
}

void FbsTsk(void * unused)
{
    /*
     *   This function never exits, so the "local" variables are static which saves stack space.
     */

    // initialise FILE structures for 'fcm' stream
    fcm.f = fopen("fcm", "wb");

    if (setvbuf(fcm.f, NULL, _IONBF, 0) != 0)       // not buffered, buffer size 0
    {
        // 0   = ok
        // EOF = error
    }

    fcm.store_cb         = FbsPutcStoreFcm;         // Use FIP/ETH character output callback function

    if (fbs.net_type == NETWORK_TYPE_FIP)
    {
        fip.watchdog     = FBS_WATCHDOG_PERIOD - 2; // Init FIP watchdog so interface will be initialised
    }

    fbs.pkt_buf_idx      = 1;                       // Fill pkt 1 buffer first
    fbs.cmd.q.buf        = cmd_q_buf;               // Link cmd response stream queue to buffer
    fbs.pub.q.buf        = pub_q_buf;               // Link publication stream queue to buffer
    fbs.termq.buf        = local_fbs.termq_buf;           // Link rterm queue to buffer
    fbs.cmd.q.dec_mask   = (FBS_OUTQ_SIZE - 1);     // Set dec mask
    fbs.pub.q.dec_mask   = (FBS_OUTQ_SIZE - 1);     // Set dec mask
    fbs.termq.dec_mask   = (FBS_TERMQ_SIZE - 1);    // Set dec mask
    fbs.cmd.header_flags = FGC_FIELDBUS_FLAGS_CMD_PKT;  // Initialise fcm response header flags

    for (;;)                    // Main loop - process each message received
    {
        TskTraceReset();

        OSTskSuspend();         // wait to be resumed by IsrFbs

        TskTraceInc();

        FbsReadCmdMsg();        // read command message
    }
}

void FbsCheck(void)
{
    if (fbs.net_type == NETWORK_TYPE_FIP)
    {
        FipCheckMsgTx();                // Check msg TX for timeout

        if (!fip.rd_rcvd_f)             // If real-time data msg was NOT received...
        {
        }
        else                            // else Var 7 WAS received...
        {
            fip.rd_rcvd_f = FALSE;      // Prepare missed ref data msg detection
        }
    }

    if (fbs.pll_locked_f &&             // If was PLL locked and
        fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll != FGC_PLL_LOCKED)     // has now unlocked
    {
        fbs.pll_locked_f = FALSE;       // Reset PLL locked flag

        if (fbs.net_type == NETWORK_TYPE_FIP)
        {
            FIP_CONFA_P    = 0x05;      // Message rx/tx disabled
            fbs.gw_online_f = FALSE;
        }

        pll.num_unlocks++;              // Increment unlocks counter
        FbsDisconnect(FGC_PLL_NOT_LOCKED);
    }

    // Check for post mortem request bit changes

    if ((fbs.u.fieldbus_stat.ack ^ local_fbs.last_ack) & (FGC_SELF_PM_REQ | FGC_EXT_PM_REQ))
    {
        local_fbs.last_ack = fbs.u.fieldbus_stat.ack;

        if (Test(fbs.u.fieldbus_stat.ack, FGC_SELF_PM_REQ))
        {
            RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));
            RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));
            RunlogWrite(FGC_RL_PM_REQ, &fbs.u.fieldbus_stat.ack);
        }
    }

    // Reset EXT_PM_REQ after 500ms

    if (Test(fbs.u.fieldbus_stat.ack, FGC_EXT_PM_REQ))
    {
        local_fbs.ext_pm_req_count++;

        if (local_fbs.ext_pm_req_count >= 25)         // If EXT_PM_REQ active for 25*20ms = 500ms
        {
            Clr(fbs.u.fieldbus_stat.ack, FGC_EXT_PM_REQ);   // Clear Ext PM request
        }
    }
    else
    {
        local_fbs.ext_pm_req_count = 0;
    }

    FbsCmdCheck();
}

void FbsDisconnect(INT8U sta)
{
    if (fcm.stat == FGC_EXECUTING)      // If FIP command executing
    {
        FbsCancelCmd(sta);              // Cancel command response
    }

    PubCancelPub();                     // Cancel all subscriptions
    fbs.send_pkt       = 0;             // Discard packet being sent
    fbs.termq.is_full  = FALSE;         // Flush rterm queue
    fbs.termq.out_idx  = fbs.termq.in_idx;
    //    LED_RST(NET_GREEN);
}

void FbsProcessTimeVar(void)
{
    if (!fbs.log_sync_time)                             // If log sync not active
    {
        if (Test(fbs.time_v.flags, FGC_FLAGS_SYNC_LOG)) // if sync pulse arrives
        {
            fbs.log_sync_time = dpcom.mcu.time.fbs.unix_time;       // Set log sync to current time
        }
    }
    else                                                // else log sync is active
    {
        if (!Test(fbs.time_v.flags, FGC_FLAGS_SYNC_LOG)) // if sync pulse ends
        {
            fbs.log_sync_time = 0;                      // Clear log sync time
        }
    }

    if (fbs.sector_access_gw &&                         // If SECTOR ACCESS flag has just been disabled
        !Test(fbs.time_v.flags, FGC_FLAGS_SECTOR_ACCESS))
    {
        fbs.sector_access = FGC_CTRL_DISABLED;          // Reset sector access flag
    }

    fbs.sector_access_gw = Test(fbs.time_v.flags, FGC_FLAGS_SECTOR_ACCESS);
    dev.pm_enabled_f     = Test(fbs.time_v.flags, FGC_FLAGS_PM_ENABLED);

    if (fbs.time_v.fgc_id == fbs.id)                // If rterm char is for this FGC
    {
        if (fbs.time_v.fgc_char == 0x03)            // If char is CTRL-C
        {
            tcm.abort_f = TRUE;                     // Abort terminal cmd
            terminal.xoff_timer = 0;                     // Reset XOFF timer
            OSMsgFlush(terminal.msgq);                   // Clear kbd buffer
        }
        else                                        // else normal character
        {
            OSMsgPost(terminal.msgq, (void *)(uintptr_t)(fbs.time_v.fgc_char));          // send character to TrmTsk
        }
    }

    fbs.last_runlog_idx = fbs.time_v.runlog_idx;    // Remember new runlog index
}

void FbsPrepareStatVar(void)
{
    INT16U      i;
    INT16U      data_chan;          // Channel within diag.data[] array
    INT16U      reported_chan;      // Channel reported to user in diag_chan[]
    fgc_db_t      logical_dim;
    fgc_db_t      board_number;
    char    *   in_circular_buffer;
    char    *   out_linear_buffer;
    INT8U       n_chs;

    // Sync please bit in unlatched status

    if (fbs.gw_online_f
        && Test(sta.config_mode, (FGC_CFG_MODE_SYNC_DB | FGC_CFG_MODE_SYNC_FGC | FGC_CFG_MODE_SYNC_DB_CAL)))
    {
        Set(ST_UNLATCHED, FGC_UNL_SYNC_PLEASE);
    }
    else
    {
        Clr(ST_UNLATCHED, FGC_UNL_SYNC_PLEASE);
    }

    // Diagnostic mpx & data

    for (i = 0; i < FGC_DIAG_N_LISTS; i++)              // For each diag list
    {
        fbs.u.fieldbus_stat.diag_chan[i] = FGC_DIAG_UNUSED_CHAN;    // Set reported channel to UNUSED by default

        if (diag.list_len[i])                           // If list is in use
        {
            if (local_fbs.diag_list_idx[i] >= diag.list_len[i]) // If beyond end of list
            {
                local_fbs.diag_list_idx[i] = 0;               // Reset index to start of list
            }

            reported_chan = diag.list[i][local_fbs.diag_list_idx[i]++];       // Set reported channel from diag list

            if (reported_chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL))  // If DIM channel
            {
                data_chan = reported_chan + diag.list_offset[i];        // Include list offset

                logical_dim = data_chan & (FGC_MAX_DIM_BUS_ADDR - 1);   // Get DIM index

                board_number = SysDbDimBusAddress(dev.sys.sys_idx, logical_dim);

                if (board_number < FGC_MAX_DIM_BUS_ADDR)
                {
                    fbs.u.fieldbus_stat.diag_chan[i] = reported_chan;
                    // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)
                    fbs.u.fieldbus_stat.diag_data[i] = diag.data.w[board_number + (data_chan & 0xE0)];
                }
            }
            else if (reported_chan < FGC_DIAG_UNUSED_CHAN)      // else if valid non-DIM channel
            {
                fbs.u.fieldbus_stat.diag_chan[i] = reported_chan;           // Report channel
                // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)
                fbs.u.fieldbus_stat.diag_data[i] = diag.data.w[reported_chan];
            }
        }
    }

    /* Transfer remote terminal characters to ethernet.rterm for ETH only*/

    if (fbs.net_type == NETWORK_TYPE_ETHERNET)
    {
        // copy max 21 char from fbs.termq[fbs.termq.out_idx] into fbs.u.fieldbus_stat.payload.rterm
        if (fbs.termq.is_full                           // term queue is full or
            || (fbs.termq.in_idx != fbs.termq.out_idx)      // at least not empty
           )
        {

            // TODO could be factorized with FbsCpyFromQueue(), need one more argument for destination
            // FbsCpyFromQueue(&fbs.termq, &fbs.termq.out_idx, &fbs.n_term_chs, ethernet.rterm, 21); // use ethernet.rterm instead of &fbs.rsp_pkt[*rsp_idx]

            n_chs = (fbs.termq.in_idx - fbs.termq.out_idx) & fbs.termq.dec_mask; // Number of characters in queue (0=FULL)

            if (!n_chs || n_chs > (FGC_ETHER_MAX_RTERM_CHARS - 1))
            {
                n_chs = FGC_ETHER_MAX_RTERM_CHARS - 1;
            }

            // copy bytes from the circular buffer to the linear output buffer
            out_linear_buffer = fgc_gateway.rterm;              // Address of rterm
            in_circular_buffer = fbs.termq.buf;         // Address of queue buffer

            while (n_chs != 0)
            {
                *(out_linear_buffer++) = in_circular_buffer[fbs.termq.out_idx++];
                fbs.termq.out_idx &= fbs.termq.dec_mask;    // increment out_idx modulo dec_mask (which is a power of 2)

                n_chs--;
            }

            *(out_linear_buffer) = '\0'; // NULL terminated
        }
    }
}

void FbsWriteStatVar(void)
{

    /*--- Export the requested runlog data byte ---*/

    // ToDo: check this difference
    fbs.u.fieldbus_stat.runlog = NVRAM_RL_BUF_A[fbs.time_v.runlog_idx];
    local_fbs.cmd_ack_f = TRUE;


    /*--- Prepare header part of status variable ---*/

    if (fbs.pll_locked_f && fbs.gw_online_f)            // ack byte
    {
        // The gateway needs to receive status w/o this flag and then with this flag to properly reset communication.
        // Thus we check for fbs.gw_online_f which is not true for the first packets sent.
        Set(fbs.u.fieldbus_stat.ack, FGC_ACK_PLL);
    }

    fbs.u.fieldbus_stat.cmd_stat = fcm.stat;            // command status

    /*--- Write status variable to uFIP/ETH ---*/

    if (FbsSendStatVar())                             // If send to uFIP/ETH succeeds
    {
        fbs.u.fieldbus_stat.ack = (((fbs.u.fieldbus_stat.ack + 1) & FGC_ACK_STATUS)
                                   | (fbs.u.fieldbus_stat.ack & (FGC_ACK_CMD_TOG | FGC_SELF_PM_REQ | FGC_EXT_PM_REQ)));

        if (!(fbs.u.fieldbus_stat.ack & FGC_ACK_STATUS))// If status bits are 0
        {
            fbs.u.fieldbus_stat.ack++;                  // Advance to 1
        }
    }
    else                                                // else send to uFIP/ETH failed
    {
        fbs.u.fieldbus_stat.ack &= (FGC_SELF_PM_REQ | FGC_EXT_PM_REQ |  // Keep STATUS, CMD_TOG
                                    FGC_ACK_STATUS | FGC_ACK_CMD_TOG);                      // and PM triggers

    }
}

void FbsPrepareMsg(void)
{
    if (fbs.gw_online_f == true)
    {
        // If sync has just locked when gateway is just online.

        if (!fbs.pll_locked_f && fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED)
        {
            fbs.pll_locked_f = TRUE;    // Latch Sync Locked state

            // Clear the degraded mode warning

            Clr(WARNINGS, FGC_WRN_EXT_SYNC_LOST);

            FbsReconnect();
        }

        // Prepare Msg to be sent

        if (fcm.stat == FGC_EXECUTING)                  // If a command is executing
        {
            if (fbs.cmd.rsp_len > FGC_MAX_VAL_LEN)      // If response has overrun the buffer
            {
                fcm.errnum = FGC_RSP_BUF_FULL;          // Set error number
            }

            if (fcm.errnum)                             // If an error has occurred and data is being sent
            {
                FbsCancelCmd(fcm.errnum);               // Cancel command response
            }
        }

        /* Transfer remote terminal characters to response string for FIP only*/

        if (fbs.net_type == NETWORK_TYPE_FIP)
        {
            if (fbs.n_term_chs < FGC_FIELDBUS_MAX_RTERM_CHARS  &&    // if less than max rterm chars already in msg and
                (fbs.termq.is_full     ||                        // term queue is full or
                 fbs.termq.in_idx != fbs.termq.out_idx))         // at least not empty
            {
                FbsCpyFromQueue(&fbs.termq, &fbs.termq.out_idx, &fbs.n_term_chs, FGC_FIELDBUS_MAX_RTERM_CHARS);
                fbs.rsp_msg_len = fbs.n_term_chs;               // Set response length to rem term len
                fbs.pub.out_idx = fbs.pub.q.out_idx;            // Cancel previous pub data
                fbs.cmd.out_idx = fbs.cmd.q.out_idx;            // Cancel previous cmd rsp data
                fbs.pend_stream = NULL;                         // Clear pend stream pointer
                fbs.pend_pkt    = FBS_RTERM_PKT;                // Mark pending packet as RTERM only
                fbs.pend_header = FGC_FIELDBUS_FLAGS_CMD_PKT |  // Mark Remote terminal response as pending
                                  FGC_FIELDBUS_FLAGS_SEQ_MASK;
            }

            /* Create a pub or cmd packet - pub takes priority over cmd data */

            if (fbs.pub.q.is_full ||                                // If publication queue is full or
                fbs.pub.out_idx != fbs.pub.q.in_idx)                // at least not empty
            {
                if (fbs.pend_pkt == FBS_CMD_PKT)                    // If pending pkt contained cmd data
                {
                    fbs.cmd.out_idx = fbs.cmd.q.out_idx;            // Cancel this cmd data
                    fbs.rsp_msg_len = fbs.n_term_chs;               // Set rsp length to rterm length
                }

                FbsQData(&fbs.pub, FBS_PUB_PKT);                    // Add pub data to packet
            }
            else if ((fbs.cmd.q.is_full ||                          // else if get cmd rsp queue is full or
                      fbs.cmd.out_idx != fbs.cmd.q.in_idx) &&       // at least not empty, and
                     (fbs.pend_pkt != FBS_PUB_PKT)                  // pending packet is not a pub packet
                     &&   local_fbs.cmd_ack_f                             // And command has been acknowledge.
                    )
            {
                FbsQData(&fbs.cmd, FBS_CMD_PKT);                    // Add pub data to packet
            }

        }
        else if (fbs.net_type == NETWORK_TYPE_ETHERNET)
        {

            /* Create a pub or cmd packet - pub takes priority over cmd data, except for last ms */

            if (mst.ms_mod_20 == FGC_ETHER_RSP_FLUSH_MS)        // Flush cmd packet, cmd takes priority
            {
                if ((fbs.cmd.q.is_full ||                       // if get cmd rsp queue is full or
                     fbs.cmd.out_idx != fbs.cmd.q.in_idx) &&    // at least not empty, and
                    (fbs.pend_pkt != FBS_PUB_PKT)               // pending packet is not a pub packet
                    && local_fbs.cmd_ack_f                            // And command has been acknowledge.
                   )
                {
                    FbsQData(&fbs.cmd, FBS_CMD_PKT);                // Add cmd data to packet
                }
                else if (fbs.pub.q.is_full ||                       // If publication queue is full or
                         fbs.pub.out_idx != fbs.pub.q.in_idx)        // at least not empty
                {
                    FbsQData(&fbs.pub, FBS_PUB_PKT);                // Add pub data to packet
                }
            }
            else                                                    // For other packets, pub takes priority
            {
                if (fbs.pub.q.is_full ||                            // If publication queue is full or
                    fbs.pub.out_idx != fbs.pub.q.in_idx)        // at least not empty
                {
                    FbsQData(&fbs.pub, FBS_PUB_PKT);                // Add pub data to packet
                }
                else if ((fbs.cmd.q.is_full ||                      // else if get cmd rsp queue is full or
                          fbs.cmd.out_idx != fbs.cmd.q.in_idx) &&     // at least not empty, and
                         (fbs.pend_pkt != FBS_PUB_PKT)               // pending packet is not a pub packet
                         && local_fbs.cmd_ack_f                            // And command has been acknowledge.
                        )
                {
                    FbsQData(&fbs.cmd, FBS_CMD_PKT);                // Add cmd data to packet
                }
            }
        }
    }
}

void FbsWriteMsg(void)
{
    INT8U       next_seq;                           // Next sequence number
    next_seq = fbs.pend_header + 1;                 // Inc sequence number for stream

    /*--- Message Sent ---*/

    if (fbs.send_pkt)                               // If message was being sent
    {

        if (Test(local_fbs.send_header, FGC_FIELDBUS_FLAGS_LAST_PKT)  // If pkt sent was a last packet, and
            && (fbs.send_pkt == FBS_CMD_PKT))               // pkt contained a command response
        {
            if (fcm.stat == FGC_EXECUTING)          // If command executing
            {
                fcm.stat = FGC_OK_RSP;              // Report exec completed OK
            }

            // The pub task can now send the first update for the subscription
            // just acknowledged.

            if (sub.new_sub_idx != 0xFF)
            {
                extendMaskSetBit((struct extended_mask_gen *) &sub.sub_acked_msk, sub.new_sub_idx);
                sub.new_sub_idx = 0xFF;
            }
        }

        fbs.send_pkt = 0;                           // Clear send rsp flags
    }

    /*--- Send Pending Message ---*/

    if (fbs.pend_pkt)                               // If new packet is pending
    {

        FbsSendMsg();                               // Send pending msg to uFIP/ETH

        local_fbs.send_header    = fbs.pend_header;       // Save packet header
        fbs.send_pkt       = fbs.pend_pkt;          // Mark packet as "being sent"
        fbs.pend_pkt       = 0;                     // Clear pending packet buffer
        fbs.n_term_chs     = 0;
        fbs.rsp_msg_len    = 0;
        fip.send_msg_timer = 0;                     // Reset send timer
        fip.start_tx       = FALSE;                 // Clear the start transmission flag

        if (fbs.pend_stream)                        // If pkt contains cmd or pub data
        {
            next_seq = fbs.pend_header + 1;         // Inc sequence number for stream

            Clr(fbs.pend_stream->header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT | FGC_FIELDBUS_FLAGS_SEQ_MASK);

            fbs.pend_stream->header_flags += (next_seq & FGC_FIELDBUS_FLAGS_SEQ_MASK);

            if (fbs.net_type == NETWORK_TYPE_FIP)
            {
                fbs.pend_stream->q.out_idx = fbs.pend_stream->out_idx;  // Advance circular buffer pointer
                fbs.pend_stream->q.is_full = FALSE;                     // Mark queue as not full

                if (fbs.pend_stream->q_not_full->pending)               // If tasks are pending on sem
                {
                    OSSemPost(fbs.pend_stream->q_not_full);             // Post semaphore
                }
            }

            if (fbs.send_pkt == FBS_PUB_PKT &&                          // If sending a last pub packet
                Test(local_fbs.send_header, FGC_FIELDBUS_FLAGS_LAST_PKT))
            {
                OSSemPost(pub.last_pkt_send);                           // Send semaphore
            }
        }
    }
}

void FbsCancelCmd(INT8U stat)
{
    fcm.stat          = stat;                               // Report status to GW
    fcm.abort_f       = TRUE;                               // Abort fip command and inhibit output to cmd stream
    fbs.cmd.q.out_idx = fbs.cmd.q.in_idx;                   // Reset fifo pointers
    fbs.cmd.out_idx   = fbs.cmd.q.in_idx;                   // Reset local queue out pointer
    fbs.cmd.q.is_full = FALSE;                              // Mark queue as not full

    if (fbs.cmd.q_not_full->pending)                        // If tasks are pending on sem
    {
        OSSemPost(fbs.cmd.q_not_full);                      // Post semaphore
    }

    if (stat != FGC_PLL_NOT_LOCKED &&                       // If PLL not unlocked
        fbs.pend_pkt == FBS_CMD_PKT)                          // pending contains command response
    {
        if (fbs.n_term_chs)                                 // If pkt also contains RTERM chars
        {
            fbs.rsp_msg_len = fbs.n_term_chs;               // Remove get cmd rsp from msg
            fbs.pend_stream = NULL;                         // Clear pend stream pointer
            fbs.pend_pkt    = FBS_RTERM_PKT;                // Mark pending packet as RTERM only
            fbs.pend_header = FGC_FIELDBUS_FLAGS_CMD_PKT |  // Mark Remote terminal response as pending
                              FGC_FIELDBUS_FLAGS_SEQ_MASK;
        }
        else                                                // else pkt has no RTERM chars
        {
            fbs.pend_pkt    = 0;                            // Cancel pending msg completely
            fbs.rsp_msg_len = 0;
            fbs.n_term_chs  = 0;
        }
    }
}

void FbsPutcFcm(char ch, FILE * f)
{

    if (ch == '\n')                     // If character is newline (linefeed)
    {
        // Set newline flag
        f->_flags2 |= NL;               // is the only flag that can be handled externally
        return;
    }

    // If newline flag is set
    if ((f->_flags2 & NL) != 0)
    {
        FbsPutcStoreFcm('\n');          // Write a single newline to buffer
        // Clear newline flag
        f->_flags2 &= ~NL;              // is the only flag that can be handled externally
    }

    FbsPutcStoreFcm(ch);                // Write character to buffer;
}

void FbsPutcPub(char ch, FILE * f)
{
    if (ch == '\n')                     // If character is newline (linefeed)
    {
        // Set newline flag
        f->_flags2 |= NL;               // is the only flag that can be handled externally
        return;
    }

    // If newline flag is set
    if ((f->_flags2 & NL) != 0)
    {
        FbsPutcStorePub('\n');          // Write a single newline to buffer
        // Clear newline flag
        f->_flags2 &= ~NL;              // is the only flag that can be handled externally
    }

    FbsPutcStorePub(ch);                // Write character to buffer;
}

void FbsPutcStoreFcm(char ch)
{
    OS_CPU_SR  cpu_sr;

    OS_ENTER_CRITICAL();                                // Protect against interrupts

    if (!fcm.abort_f)                                   // If command has not been aborted
    {
        if (fbs.cmd.q.is_full)                          // If buffer is full
        {
            OSSemPend(fbs.cmd.q_not_full);              // Wait on semaphore
        }

        fbs.cmd.q.buf[fbs.cmd.q.in_idx] = ch;           // Store character in queue

        fbs.cmd.q.in_idx = (fbs.cmd.q.in_idx + 1) & fbs.cmd.q.dec_mask; // Adjust in index

        if (fbs.cmd.q.in_idx == fbs.cmd.q.out_idx)      // If buffer now full
        {
            fbs.cmd.q.is_full = TRUE;                   // set buf full flag
        }

        fbs.cmd.rsp_len++;                              // Increment total response length
    }

    OS_EXIT_CRITICAL();
}

void FbsPutcStorePub(char ch)
{
    OS_CPU_SR cpu_sr;

    OS_ENTER_CRITICAL();                                // Protect against interrupts

    if (!pcm.abort_f)                                   // If publication has not been aborted
    {
        if (fbs.pub.q.is_full)                          // If buffer is full
        {
            OSSemPend(fbs.pub.q_not_full);              // Wait on semaphore
        }

        fbs.pub.q.buf[fbs.pub.q.in_idx] = ch;           // Store character in queue

        fbs.pub.q.in_idx = (fbs.pub.q.in_idx + 1) & fbs.pub.q.dec_mask; // Adjust in index

        if (fbs.pub.q.in_idx == fbs.pub.q.out_idx)      // If buffer now full
        {
            fbs.pub.q.is_full = TRUE;                   // set buf full flag
        }

        fbs.pub.rsp_len++;                              // Increment total response length
    }

    OS_EXIT_CRITICAL();
}

void FbsOutShort(const INT8U * ch, struct cmd * c)
{
    c->store_cb(ch[0]);
    c->store_cb(ch[1]);
}

void FbsOutLong(const char * ch, struct cmd * c)
{
    c->store_cb(ch[0]);
    c->store_cb(ch[1]);
    c->store_cb(ch[2]);
    c->store_cb(ch[3]);
}

void FbsOutBuf(const INT8U * ch, INT16U n_bytes, struct cmd * c)
{
    while (n_bytes--)
    {
        c->store_cb(*(ch++));
    }
}

void FbsOutTermCh(char ch)
{
    OS_CPU_SR  cpu_sr;

    local_fbs.termq_buf[fbs.termq.in_idx] = ch;       // Store character in queue

    OS_ENTER_CRITICAL();                        // Protect against interrupts

    fbs.termq.in_idx = (fbs.termq.in_idx + 1) & fbs.termq.dec_mask; // Increment input index

    if (fbs.termq.in_idx == fbs.termq.out_idx)  // Test for buffer now full
    {
        fbs.termq.is_full = TRUE;
    }

    OS_EXIT_CRITICAL();
}

INT16U  FbsStreamGetSize(const struct fbs_stream * s)
{
    if (s->q.is_full)
    {
        return FBS_OUTQ_SIZE;
    }
    else if (s->out_idx <= s->q.in_idx)
    {
        return (s->q.in_idx - s->out_idx);
    }
    else // s->out_idx > s->q.in_idx
    {
        return (FBS_OUTQ_SIZE - s->out_idx + s->q.in_idx);
    }
}

// Internal function definitions

static void FbsReadCmdMsg(void)
{
    INT16U              cmd_type;
    struct cmd_pkt   *  pkt = &fcm_pars.pkt[fbs.pkt_buf_idx];           // Get address of new packet buffer;

    TskTraceInc();

    if (pkt->header_flags)                              // If the new packet buffer is not yet available
    {
        fcm.stat = FGC_PKT_BUF_NOT_AVL;                 // report BUF NOT AVL error
        fbs.u.fieldbus_stat.ack ^= FGC_ACK_CMD_TOG;     // Acknowledge receipt of command packet
        return;
    }

    TskTraceInc();

    // If this command pkt starts a new command...
    if (Test(fbs.rcvd_header.flags, FGC_FIELDBUS_FLAGS_FIRST_PKT))
    {
        if (fcm.stat == FGC_EXECUTING)                  // If command is still executing
        {
            return;                                     // Ignore new packet
        }

        fcm.errnum = FGC_OK_NO_RSP;

        TskTraceInc();

        cmd_type = fbs.rcvd_header.flags & FGC_FIELDBUS_FLAGS_CMD_TYPE_MASK;    // Extract cmd type from header

        if (cmd_type == FGC_FIELDBUS_FLAGS_GET_CMD   ||         // If new pkt contains a command that will return
            cmd_type == FGC_FIELDBUS_FLAGS_SUB_CMD   ||      // data through the get command stream
            cmd_type == FGC_FIELDBUS_FLAGS_UNSUB_CMD ||
            cmd_type == FGC_FIELDBUS_FLAGS_GET_SUB_CMD)
        {
            if (!Test(fbs.rcvd_header.flags, FGC_FIELDBUS_FLAGS_LAST_PKT))  // If the cmd is longer than one packet
            {
                fcm.stat = FGC_CMD_BUF_FULL;                    // report CMD BUF FULL error
                fbs.u.fieldbus_stat.ack ^= FGC_ACK_CMD_TOG;                 // Acknowledge receipt of command packet
                return;                             // Ignore packet
            }

            TskTraceInc();

            Set(fbs.cmd.header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT); // Set first rsp packet flag
            Clr(fbs.cmd.header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT);  // Clear last rsp packet flag

            fcm.f->_flags2 &= ~NL;                  // is the only flag that can be handled externally
            fcm.abort_f = FALSE;                    // Clear abort flag for new command
        }

        fbs.cmd.rsp_len = 0;
        fcm.stat    = FGC_RECEIVING;                // report state change
    }
    else                                            // else packet isn't the first for command...
    {
        if (fcm.stat != FGC_RECEIVING)              // If a new packet is not expected
        {
            return;                                 // Ignore packet
        }
    }

    TskTraceInc();

    if (Test(fbs.rcvd_header.flags, FGC_FIELDBUS_FLAGS_LAST_PKT)) // If this packet is the last for the command
    {
        fcm.stat = FGC_EXECUTING;                        // Indicate that command now executing
        local_fbs.cmd_ack_f = FALSE;
    }

    fbs.pkt_buf_idx   ^= 1;                     // Flip to other buffer for next packet
    fbs.u.fieldbus_stat.ack ^= FGC_ACK_CMD_TOG; // Acknowledge receipt of command packet
    pkt->n_chars       = fbs.pkt_len;           // Pass packet length and
    pkt->header_flags  = fbs.rcvd_header.flags; // msg header flags and
    pkt->header_user   = fbs.rcvd_header.user;  // user to command task

    TskTraceInc();

    FbsCpyCmd(pkt->buf, fbs.pkt_len);           // callback for FIP/ethernet

    TskTraceInc();

    OSSemPost(fcm.sem);                         // Trigger command task to process new packet

    TskTraceInc();
}

static void FbsCmdCheck(void)
{
    // @TODO is this check needed?

    //    static INT16U stat = FGC_OK_NO_RSP;
    //    static INT16U prop_indx_prev = 0;
    //    static struct abs_time_us prev = { 0, 0 };
    //
    //    if (fcm.stat != FGC_EXECUTING || fcm.prop->sym_idx != prop_indx_prev)
    //    {
    //        prev = mst.time;
    //        prop_indx_prev = fcm.prop->sym_idx;
    //    }
    //
    //    if ((AbsTimeUsDiff(&prev, &mst.time) > 5000000))
    //    {
    //        FbsCancelCmd(FGC_ABORTED);
    //    }
}

void FbsReconnect(void)
{
    if (fbs.net_type == NETWORK_TYPE_FIP)
    {
        FipMsgsa();                         // Read status register from uFIP to clear it
        FIP_CONFA_P          = 0x07;        // Message reception enabled and transmission disabled
        fip.start_tx         = TRUE;        // Flag that messaging has just been enabled
        fbs.gw_online_f      = TRUE;
    }

    fbs.pub.header_flags = 1;           // (Re-)start pub message sequence number at 1
    fbs.pend_pkt         = 0;           // Clear pend flags
    fbs.rsp_msg_len      = 0;
    fbs.n_term_chs       = 0;
    pcm.abort_f          = FALSE;       // Enable subscription publication

    //    LED_SET(NET_GREEN);
}

void FbsQData(struct fbs_stream * stream, INT8U pkt_type)
{
    if (fbs.rsp_msg_len < fbs.cmd_rsp_len)                              // If space remains in packet
    {
        fbs.pend_stream = stream;                                       // Save stream pointer for packet
        fbs.pend_pkt = pkt_type;                                        // Save pkt type

        FbsCpyFromQueue(&stream->q, &stream->out_idx, &fbs.rsp_msg_len, // Fill packet from stream buffer
                        fbs.cmd_rsp_len);

        if (stream->out_idx == stream->q.in_idx)                        // If output queue now empty
        {
            fbs.pend_header = stream->header_flags;                     // Save pkt header

            if (Test(fbs.pend_header, FGC_FIELDBUS_FLAGS_LAST_PKT))     // If last pkt
            {
                fbs.pend_pkt = pkt_type;                                // Save pkt type
            }
        }
        else
        {
            fbs.pend_header = stream->header_flags & ~FGC_FIELDBUS_FLAGS_LAST_PKT;  // Save pkt header without
        }                                                                           // last pkt flag bit
    }
}

void FbsCpyFromQueue(struct queue * q, INT16U * out_idx, INT16U * rsp_idx, INT16U max_rsp_idx)
{
    char    *   in_circular_buffer;
    char    *   out_linear_buffer = &fbs.rsp_pkt[*rsp_idx];

    INT16U      n_chs;
    INT16U      max_chs;
    INT16U      dec_mask;
    INT16U      nchs_till_end;

    max_chs  = max_rsp_idx - *rsp_idx;                  // Max number of characters which may be transferred
    dec_mask = q->dec_mask;                             // Local copy of dec mask for the queue
    n_chs    = (q->in_idx - *out_idx) & dec_mask;       // Number of characters in queue (0=FULL)

    if (!n_chs ||  n_chs > max_chs)                     // If more characters than space
    {
        n_chs = max_chs;                                // Clip to space available
    }

    // copy bytes from the circular buffer to the linear output buffer

    in_circular_buffer = &q->buf[*out_idx];             // Address of queue buffer
    nchs_till_end = (dec_mask + 1 - *out_idx);          // Number of chars till end of circular buffer

    if (fbs.net_type == NETWORK_TYPE_FIP)
    {
        if (n_chs > nchs_till_end)
        {
            // Data wrapped in circular buffer wraps, copy is done in 2 memcpy

            memcpy(out_linear_buffer, in_circular_buffer, nchs_till_end);
            memcpy(out_linear_buffer + nchs_till_end, q->buf, (n_chs - nchs_till_end));
        }
        else
        {
            // Data not wrapped in circular buffer, copy is done in 1 memcpy

            memcpy(out_linear_buffer, in_circular_buffer, n_chs);
        }
    }
    else if (fbs.net_type == NETWORK_TYPE_ETHERNET)
    {
        // With fgc_ether we use DMA, address must be double word aligned.
        // In case we change source address to keep alignment,
        // first extra bytes are then discarded by LAN chip (thanks to offset).
        // Second DMA run (required if data loops from end to start of circular buffer)
        // will start at beginning of buffer which is 32 bit aligned (no offset needed).

        if (n_chs > nchs_till_end)
        {
            // Circular buffer wraps up, copy will be done in 2 DMA run
            ethernet.rsp_dma.source2         = (INT32U) in_circular_buffer & 0xFFFFFFFC;
            ethernet.rsp_dma.offset          = (INT32U) in_circular_buffer & 3;
            ethernet.rsp_dma.size_bytes2     = nchs_till_end;
            ethernet.rsp_dma.size_dwords2    = (((nchs_till_end + ethernet.rsp_dma.offset) / 4)
                                                + (((nchs_till_end + ethernet.rsp_dma.offset) & 0x0003) ? 1 : 0));

            ethernet.rsp_dma.source          = (INT32U) q->buf;
            ethernet.rsp_dma.size_bytes      = n_chs - nchs_till_end;
            ethernet.rsp_dma.size_dwords     = ((n_chs - nchs_till_end) / 4) + (((n_chs - nchs_till_end) & 0x0003) ? 1 :
                                               0);
        }
        else
        {
            // Circular buffer does not come back to beginning, we need just one DMA
            ethernet.rsp_dma.source           = (INT32U) in_circular_buffer & 0xFFFFFFFC;
            ethernet.rsp_dma.offset           = (INT32U) in_circular_buffer & 3;
            ethernet.rsp_dma.size_bytes       = n_chs;
            ethernet.rsp_dma.size_dwords      = (((n_chs + ethernet.rsp_dma.offset) / 4)
                                                 + (((n_chs + ethernet.rsp_dma.offset) & 0x0003) ? 1 : 0));

            ethernet.rsp_dma.size_bytes2 = 0;
        }
    }

    *out_idx  = (*out_idx + n_chs) & dec_mask;          // Update queue out index
    *rsp_idx += n_chs;                                  // Update index in response buffer
}

void FbsSetStandalone(void)
{
    fbs.id = FBS_STANDALONE_ID;
    LED_RST(NET_RED);               // Clear RED FIP LED
    sta.config_state            = FGC_CFG_STATE_STANDALONE;    // Change config state to STANDALONE
    sta.config_mode             = FGC_CFG_MODE_SYNC_NONE;      // Reset CONFIG.MODE
}

void FbsLeaveStandalone(void)
{
    // Request SYNC_FGC if the FGC was in standalone SYNC DB to configure the properties

    if (sta.config_state == FGC_CFG_STATE_STANDALONE  &&
        shared_mem.mainprog_seq == FGC_MP_MAIN_RUNNING)
    {
    	sta.config_state            = FGC_CFG_STATE_UNSYNCED;
        sta.config_mode             = FGC_CFG_MODE_SYNC_FGC;
    }
}

void FbsInit()
{
    fbs.gw_online_f = FALSE;

    fbs.last_runlog_idx          = 0x0000;                      // Prepare repeat Var 7 detection
    fbs.time_v.ms_time           = 0xFFFF;                      // Cancel initial time packet
    fbs.u.fieldbus_stat.ack     &= ~FGC_ACK_STATUS;             // Reset stat var sequence to 0
    fbs.u.fieldbus_stat.class_id = FGC_CLASS_ID;                // Report FGC class ID to gateway
}

// EOF
