/*---------------------------------------------------------------------------------------------------------*\
  File:         pars_service.c

  Purpose:      FGC3 MCU parsing function

  Note:         On FGC2, the function ParsPkt was on the DSP (with a slightly different name, ParsePkt)
\*---------------------------------------------------------------------------------------------------------*/

#define PARS_SERVICE_GLOBALS

#include <pars_service.h>
#include <errno.h>      // ANSI errors library
#include <stdio.h>      // ANSI stdlib library
#include <stdlib.h>     // ANSI stdlib library
#include <string.h>     // ANSI string library
#include <defconst.h>   // Global FGC DSP constants
#include <definfo.h>    // for FGC_CLASS_ID
#include <deftypes.h>   // for int_limits / float_limits
#include <fgc_errs.h>   // Include global FGC error constants
#include <macros.h>     // for Set(), Clr(), Test()


/*---------------------------------------------------------------------------------------------------------*/
void ParsPkt(struct pars_buf * pars, struct cmd_pkt_parse_buf * buf)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the MCU wants help to parse a command packet containing Integers,
  Floats or RelTime values.  A token can be split across packets so the function must find the
  last token delimiter character (,) in the packet and keep all characters following this in the start
  of the buffer for the next time the function is called.

  The function supports three parameter types:

  1. Integers

     All integers are parsed as signed longs, and integer limits are the same.  This means that even
     INT32U properties are limited to the range 0 to (2^31)-1.

  3. Floats

     Floating point values have a different format in the DSP than in the MCU.  So after parsing the value
     to FP32, it must be converted to IEEE format before being returned to the MCU.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U              i;                      // Loop variable
    INT32U              out_of_limits_f;        // Out of limits flag
    INT32U              base;                   // Integer base (10=decimal 16=hex)
    INT32U              type;                   // Parameter type: PKT_FLOAT|PKT_INT|PKT_RELTIME|PKT_INTU
    INT32U              n_pars;                 // Number of parameters converted
    INT32U              n_pkt_chars;            // Number of characters in command pkt
    INT32U              n_ex_chars;             // Number of excess characters in command pkt
    INT32U              out_idx;                // Out index for new pkt
    INT32S              intval  = 0;            // Integer value
    INT32U              intuval = 0;            // Unsigned integer value
    FP32                fpval   = 0.0;          // Floating point value
    char        *       pkt_buf;                // Pointer to start of uncompressed pkt buffer
    char        *       dppkt_buf;              // Pointer within incoming pkt buffer in DPRAM
    struct cmd_pkt   *  cmd_pkt;                // Pointer to command pkt structure (selected from FP32 buf)

    // Prepare local variables from new packet data in DPRAM

    n_pars      = 0;
    n_ex_chars  = 0;
    type        = pars->type;
    cmd_pkt     = &pars->pkt[pars->pkt_buf_idx];
    dppkt_buf   = &cmd_pkt->buf[0];
    n_pkt_chars = cmd_pkt->n_chars;                     // Number of new characters in pkt buffer

    // Check for first packet

    if (Test(cmd_pkt->header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT)) // If pkt is first for command
    {
        buf->n_carryover_chars = 0;                                   // Cancel any left over characters
    }

    // Uncompress new packet characters into byte array

    pkt_buf = &buf->buf[buf->n_carryover_chars];        // Initialise pkt_buf pointer to after carryover chars

    memcpy(pkt_buf, dppkt_buf, n_pkt_chars);

    pkt_buf = &buf->buf[0];                             // Init pointer to start pkt buffer
    out_idx = pars->out_idx;                            // Index to start of first new token
    pars->out_idx = n_pkt_chars;                        // Set out_idx to end of packet to indicate it's empty
    n_pkt_chars += buf->n_carryover_chars;              // Total number of characters in pkt buffer

    // Check for incomplete token at the end of the packet

    if (!Test(cmd_pkt->header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT))
    {
        while (n_pkt_chars > out_idx && pkt_buf[--n_pkt_chars] != ',')  // Scan backwards for ','
        {
            if (++n_ex_chars >= MAX_CMD_TOKEN_LEN)                          // If token exceeds max length
            {
                pars->errnum       = FGC_SYNTAX_ERROR;                          // Report syntax error
                pars->par_err_idx = 0;                                          // Unknown par index
                pars->type        = 0;                                          // Report action complete
                return;
            }
        }

        if (pkt_buf[n_pkt_chars] != ',')                                // If first token
        {
            n_pkt_chars--;
            goto end;                                                           // Jump to end
        }
    }

    pkt_buf[n_pkt_chars] = 0;                           // Null terminate previous token

    // Try to parse each token in turn

    pkt_buf = &buf->buf[out_idx];                       // Set pkt buf pointer to start of first token

    pars->errnum = 0;                                   // Clear errnum

    for (;;)
    {

        if (*pkt_buf == ' ')                    // If leading space
        {
            pkt_buf++;                                  // Skip space
        }

        if (*pkt_buf == '\0')                   // If end of pkt
        {
            n_pars++;                                   // Return NO_INT/NO_FLOAT value
            break;                                      // End processing of pkt
        }

        if (*pkt_buf == ',')                    // If no token before comma
        {
            pars->empty_tokens[n_pars] = true;
            n_pars++;                                   // Return NO_INT/NO_FLOAT value
            pkt_buf++;                                  // Skip comma
        }
        else                                    // else token present
        {
            errno = 0;                                          // Reset errno before the call to strtol/strtoul/strtod
            pars->empty_tokens[n_pars] = false; 

            if (type == PKT_INT || type == PKT_INTU)            // If INT parameter (signed or unsigned)
            {
                if (pkt_buf[0] == '0' &&                        // If prefix is "0x" or "0X"
                    (pkt_buf[1] == 'x' || pkt_buf[1] == 'X'))
                {
                    base = 16;                                          // Prepare for hex value
                    pkt_buf += 2;
                }
                else                                            // else
                {
                    base = 10;                                          // Prepare for decimal value
                }

                if (type == PKT_INT)
                {
                    intval = strtol(pkt_buf, &pkt_buf, base);           // Parse token as signed integer
                    out_of_limits_f = (intval < pars->limits.integer.min ||
                                       intval > pars->limits.integer.max);
                }
                else
                {
                    // The strtoul() implementation in the gcc RX610 library converts a negative
                    // value into its unsigned representation, thus conforming to the standard.
                    // An explicit check for '-' must be added.

                    out_of_limits_f = (*pkt_buf == '-');

                    if (!out_of_limits_f)
                    {
                        intuval = strtoul(pkt_buf, &pkt_buf, base);         // Parse token as unsigned integer
                        out_of_limits_f = (intuval < pars->limits.unsigned_integer.min ||
                                           intuval > pars->limits.unsigned_integer.max);
                    }
                }
            }
            else if (type == PKT_FLOAT)                         // else FLOAT or RELTIME parameter
            {
                fpval = strtod(pkt_buf, &pkt_buf);                      // Convert token to FP32
                out_of_limits_f = (fpval < pars->limits.fp.min       || fpval > pars->limits.fp.max);
            }

            // Check errno == ERANGE is a portable way to detect "out of range" errors on INT32U and INT32S (like for
            // example setting a INT32U to -1).

            if (errno == ERANGE || out_of_limits_f)             // If parameter was out of limits
            {
                pars->par_err_idx = n_pars;                             // Report parameter index for error
                pars->errnum      = FGC_OUT_OF_LIMITS;                  // Report error
                pars->type        = 0;                                  // Cancel request to indicate end of action
                return;
            }

            if (*pkt_buf == ' ')                                // If trailing space
            {
                pkt_buf++;                                              // Skip space
            }

            if (errno ||                                        // If not a valid delimiter or other type of lib error
                (*pkt_buf && *pkt_buf != ','))
            {
                pars->par_err_idx = n_pars;                             // Report parameter index for error
                pars->errnum      = (type == PKT_FLOAT ? FGC_BAD_FLOAT : FGC_BAD_INTEGER);
                pars->type        = 0;                                  // Cancel request to indicate end of action
                return; 
            }

            switch (type)
            {
                case PKT_INT:

                    pars->results.ints[n_pars++] = intval;
                    break;

                case PKT_INTU:

                    pars->results.intu[n_pars++] = intuval;
                    break;

                case PKT_FLOAT:

                    pars->results.fp[n_pars++] = fpval;
                    break;
            }

            if (!*pkt_buf)                                      // Delimiter is nul
            {
                break;                                                  // End processing of this packet
            }
            else                                                // else delimiter is a comma
            {
                pkt_buf++;                                              // Skip over comma
            }
        }
    }

    // Transfer extra character to start of pkt buffer for next time

end:

    pkt_buf = &buf->buf[0];                             // Reinit pointer to start of pkt buffer

    for (i = 0; i < n_ex_chars; i++)                    // For extra characters
    {
        pkt_buf[i] = pkt_buf[++n_pkt_chars];                    // Transfer extra characters to start of buffer
    }

    errno = 0;

    // Return results to MCU

    pars->pkt_buf_idx      = n_ex_chars;                // Remember number of extra character for next time
    buf->n_carryover_chars = n_ex_chars;                // Remember number of extra character for next time
    pars->n_pars           = n_pars;                    // Return number of parameters found
    pars->type             = 0;                         // Cancel request to indicate that action is completed

    return;
}


/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars_service.c
\*---------------------------------------------------------------------------------------------------------*/
