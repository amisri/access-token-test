/*---------------------------------------------------------------------------------------------------------*\
  File:         serial_stream.c

  Purpose:      FGC Software - Serial Communication Callback Functions.

\*---------------------------------------------------------------------------------------------------------*/

#include <serial_stream.h>
#include <cc_types.h>           // basic typedefs
#include <stdio.h>              // for FILE
#include <os.h>                 // for OS_MSG, OS_MEM types
#include <trm.h>                // for terminal_state and sci global variables
#include <fbs.h>                // only in FGC3, for NL constant
#include <cmd.h>                // for fcm, tcm, pcm global variables
#include <rtd.h>                // for rtd global variable

/*---------------------------------------------------------------------------------------------------------*/
void TermPutcChar(char ch, FILE * f)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SerialStreamInternal_WriteChar() to put the character ch into the buffer for the specified
  stream f.  If the buffer is full it will be flushed automatically.
\*---------------------------------------------------------------------------------------------------------*/
{
    char    **   stream_buffer_ptr;
    INT16U   *   stream_buffer_offset_ptr;

    if (f == tcm.f)
    {
        stream_buffer_ptr =        &(tcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(tcm.serial_stream_buffer_offset);
    }
    else if (f == rtd.f)
    {
        stream_buffer_ptr =        &(rtd.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(rtd.serial_stream_buffer_offset);
    }
    else if (f == pcm.f)
    {
        stream_buffer_ptr =        &(pcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(pcm.serial_stream_buffer_offset);
    }
    else if (f == fcm.f)
    {
        stream_buffer_ptr =        &(fcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(fcm.serial_stream_buffer_offset);
    }
    else
    {
        return;
    }



    if (*stream_buffer_ptr == 0)                                // If no buffer is in use with this stream
    {
        *stream_buffer_ptr = (char *) OSMemPend(terminal.mbuf);      // Pend of a free buffer from partition
        *stream_buffer_offset_ptr = 0;                          // Reset buffer index
    }

    (*stream_buffer_ptr)[(*stream_buffer_offset_ptr)++] = ch;   // Save character in current buffer

    if (*stream_buffer_offset_ptr >= TRM_BUF_SIZE)              // If buffer is now full...
    {
        SerialStreamInternal_FlushBuffer(f);                   // then flush buffer to SCI
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void SerialStreamInternal_WriteChar(char ch, FILE * f)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the callback for the SCI output streams (TrmTsk() commands or RtdTsk()).  It supports
  the following special codes:

        \v              Flush buffer immediately
        \n              Insert one newline even if multiple \n are seen and if editor is enabled
                        then follow by \r to produce a carriage return on the ANSI/VT100 screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U   *   line_offset_ptr;

    if (f == tcm.f)
    {
        line_offset_ptr = &(tcm.line_offset);
    }
    else if (f == rtd.f)
    {
        line_offset_ptr = &(rtd.line_offset);
    }
    else if (f == pcm.f)
    {
        line_offset_ptr = &(pcm.line_offset);
    }
    else if (f == fcm.f)
    {
        line_offset_ptr = &(fcm.line_offset);
    }
    else
    {
        return;
    }

    if (ch == '\n')                                     // If character is newline (linefeed)
    {
        f->_flags2 |= NL;                                 // Set newline flag
        *line_offset_ptr = 0;
        return;
    }

    if ((f->_flags2 & NL) != 0)                         // If newline flag is set
    {
        TermPutcChar('\n', f);                           // Write a single newline to buffer
        f->_flags2 &= ~NL;                              // Clear newline flag

        if (terminal.mode == TRM_EDITOR_MODE)                // If the line editor is enabled
        {
            TermPutcChar('\r', f);                       // Write a single CR to buffer
        }
    }

    if (ch == '\v')                                     // If character is vertical tab
    {
        SerialStreamInternal_FlushBuffer(f);            // then flush buffer to SCI immediately
        return;
    }

    TermPutcChar(ch, f);                                 // Store character in buffer
    *line_offset_ptr += 1;
}
/*---------------------------------------------------------------------------------------------------------*/
void SerialStreamInternal_FlushBuffer(FILE * f)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to flush the stream buffer so that it is queued for transmission to the SCI by
  the TrmMsOut() function.  This function can be called with a NULL FILE pointer.
  Note that terminal_state.bufq can
  never fill up because it is one element longer than the number of buffers it needs to address.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;
    char   **   stream_buffer_ptr;
    INT16U   *  stream_buffer_offset_ptr;

    if (f == tcm.f)
    {
        stream_buffer_ptr =        &(tcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(tcm.serial_stream_buffer_offset);
    }
    else if (f == rtd.f)
    {
        stream_buffer_ptr =        &(rtd.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(rtd.serial_stream_buffer_offset);
    }
    else if (f == pcm.f)
    {
        stream_buffer_ptr =        &(pcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(pcm.serial_stream_buffer_offset);
    }
    else if (f == fcm.f)
    {
        stream_buffer_ptr =        &(fcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(fcm.serial_stream_buffer_offset);
    }
    else
    {
        return;
    }


    if (*stream_buffer_ptr != 0)                                     // If buffer is defined
    {
        OS_ENTER_CRITICAL();

        // Queue point to SCI buffer and length of contents
        terminal_state.bufq_adr[terminal_state.bufq.in_idx] = *stream_buffer_ptr;
        terminal_state.bufq_len[terminal_state.bufq.in_idx] = *stream_buffer_offset_ptr;

        if (++terminal_state.bufq.in_idx > TRM_N_BUFS)          // If input index has advanced beyond end of queue
        {
            terminal_state.bufq.in_idx = 0;                     // reset queue input index
        }

        OS_EXIT_CRITICAL();

        *stream_buffer_ptr = 0;                                 // Clear local copy of buffer pointer
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: serial_stream.c
\*---------------------------------------------------------------------------------------------------------*/
