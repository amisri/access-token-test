/*!
 *  @file      state_manager.c
 *  @defgroup  FGC3:MCU
 *  @brief     This file contains the functions to support the Power Converter
 *  State Machine
 */

#define STATE_MANAGER_GLOBALS
#define STA_GLOBALS

// Includes

#include <string.h>

#include <state_manager.h>
#include <cmd.h>
#include <class.h>              // for DEV_STA_TSK_PHASE
#include <fbs_class.h>          // for FAULTS, WARNINGS, ST_LATCHED, ST_UNLATCHED, STATE_OP, STATE_VS, STATE_PC, ST_DCCT_A, ST_DCCT_B
#include <ref.h>                // for EVENT_GROUP, NON_PPM_USER
#include <defprops.h>
#include <definfo.h>            // for FGC_CLASS_ID
#include <defconst.h>           // For FGC_LOG_RUNNING
#include <dev.h>                // for dev global variable
#include <fbs.h>                // for fbs global variable
#include <mst.h>                // for mst global variable
#include <dpcom.h>              // for dpcom global variable
#include <macros.h>             // for Test(), Set(), Clr()
#include <sta.h>                // for sta global variable
#include <diag.h>               // for diag global variable
#include <os.h>                 // for OSTskSuspend()
#include <dpcls.h>              // for dpcls global variable
#include <cal.h>                // for cal global variable
#include <fgc_errs.h>           // for FGC_BAD_STATE
#include <log.h>                // for LogStartAll()
#include <log_class.h>          // for timing_log global variable, LogEvtTiming()
#include <rtd.h>                // for rtd global variable
#include <fgc_pc_fsm.h>         // for pc_states[], pc_transitions[]
#include <mcu_dependent.h>      // for ENTER_BB(), EXIT_BB()
#include <adc.h>                // for AdcInit(), AdcFiltersSetIndex()
#include <memmap_mcu.h>         // for DIG_OP_P
#include <shared_memory.h>      // for shared_mem global variable, FGC_MP_MAIN_RUNNING
#include <iodefines.h>          // specific processor registers and constants
#include <mcu_dsp_common.h>     // For struct abs_time_us/ms
#include <pub.h>                // for PubProperty()
#include <debug.h>              // for MEDAUSTRON
#include <property.h>           // for struct prop
#include <pc_state.h>
#include <led.h>
#include <crate.h>
#include <task_trace.h>
#include <pll.h>



#define RESET_COMMAND_DURATION 4 + 1 // Reset sequence is 20ms, number of iterations + 1, the iterations are spaced 5ms



// Internal functions declaration

static void StaOpState(void);
static void StaDdips(void);
static void StaVsState(void);
static void StaAdcControl(void);
static void StaCheck(void);
static void StaLeds(void);
static void StaPcState(void);
static void StaCalControl(void);
static void StaCmdControl(void);
static void StaPubCyclingProp(void);
static void Sta2HzPublication(void);

// Internal function definitions

static void StaOpState(void)
{
    /*
     * This function is called to control the operational state.
     */

    if (STATE_OP != FGC_OP_CALIBRATING)   // If not auto calibrating
    {
        STATE_OP = sta.mode_op;             // Set the state to equal the mode
    }

    if (STATE_OP != (INT16U)dpcom.mcu.state_op) // If state has changed
    {
        if (STATE_OP == FGC_OP_SIMULATION)          // If simulating voltage source
        {
            if ((INT16U)dpcom.mcu.state_op != FGC_OP_CALIBRATING) // If not calibrating before
            {
                vs.sim_intlks = TRUE;                   // Enable interlock simulation
                sta.cmd_req = 0;                    // Clear command request register
                sta.inputs  = DIG_IP1_PCPERMIT_MASK16;                  // Simulated PC_PERMIT
                WARNINGS    = 0;                    // Reset warnings
                FAULTS      = 0;                    // Reset latched faults
                Clr(sta.faults, FGC_FLT_FGC_STATE);         // Clear FGC_STATE fault

                if (vs.fw_diode)                    // If FW_DIODE is present
                {
                    vs.fw_diode = FGC_VDI_NO_FAULT;             // Clear the FW fault state
                }

                if (vs.fabort_unsafe)                   // If FABORT_UNSAFE is present
                {
                    vs.fabort_unsafe = FGC_VDI_NO_FAULT;            // Clear the FAU fault state
                }

                if (dpcls.mcu.vs.polarity.timeout)                // If polarity switch expected
                {
                    Set(sta.inputs, DIG_IP1_POLSWIPOS_MASK16);          // Simulate in positive pos
                }
            }

            WARNINGS |= FGC_WRN_SIMULATION;                 // Set SIMULATION warning
        }
        else                        // else not simulating
        {
            Clr(WARNINGS, FGC_WRN_SIMULATION);              // Clear SIMULATION warning

            if ((STATE_OP == FGC_OP_NORMAL)             // If state is NORMAL or
                || (STATE_OP == FGC_OP_CALIBRATING)        // CALIBRATING
               )
            {
                sta.faults &= ~FGC_FLT_FGC_STATE;       // Clear FGC_STATE fault
            }
            else
            {
                sta.faults |= FGC_FLT_FGC_STATE;
            }
        }

        dpcom.mcu.state_op = STATE_OP;          // Copy state to DPRAM
    }

    if ((STATE_OP == FGC_OP_NORMAL)
        && !((INT16U) dpcls.dsp.load_select)
        && !((INT16U) dpcls.mcu.cal.active)
       )
    {
        ST_UNLATCHED |= FGC_UNL_NOMINAL_LOAD;
    }
    else
    {
        ST_UNLATCHED &= ~FGC_UNL_NOMINAL_LOAD;
    }
}

static void StaDdips(void)
{
    /*
     * This function will read and/or simulate the direct I/O registers. There
     * are two properties that control the way the simulation works and three
     * different modes:

             MODE.OP      VS.SIM_INTLKS       VSRUN/VSREADY        All other
           sta.mode_op    vs.sim_intlks         VSPOWERON            signals
         --------------------------------------------------------------------------------------
         1: !SIMULATION  not significant         REAL              REAL
         2:  SIMULATION     DISABLED           SIMULATED           REAL
         3:  SIMULATION     ENABLED            SIMULATED         SIMULATED
         --------------------------------------------------------------------------------------

          Input/Cmd      Mode 1      Mode 2       Mode 3
           Signal        NORMAL      !SIM_INTLKS  SIM ALL
         -----------------------------------------------------------------
         DIG watchdog    TRIGGERED   TRIGGERED   NON TRIGGERED
         -----------------------------------------------------------------
         VSNOCABLE       REAL        REAL        0
         TIMEOUT         REAL        REAL        0
         VSRUN           REAL        SIM         SIM
         PWRFAILURE      REAL        REAL        SIM
         PCPERMIT        REAL        REAL        1
         PCDISCHRQ       REAL        REAL        0
         FASTABORT       REAL        REAL        0
         POLSWINEG       REAL        REAL        SIM
         POLSWIPOS       REAL        REAL        SIM
         DCCTAFLT        REAL        REAL        0
         DCCTBFLT        REAL        REAL        0
         VSEXTINTLK      REAL        REAL        0
         VSFAULT         REAL        REAL        0
         VSREADY         REAL        SIM         SIM
         VSPOWERON       REAL        SIM         SIM
         -----------------------------------------------------------------
         FGCOKCMD        REAL        REAL        SIM
         INTLKOUTCMD     REAL        REAL        0
         AFRUNCMD        REAL        SIM         SIM

         POLTONEGCMD     REAL        REAL        0
         POLTOPOSCMD     REAL        REAL        0
         VSRESETCMD      REAL        REAL        0
         VSRUNCMD        REAL        REAL        SIM
     */

    INT32U polarity_state;

    if (sta.mode_op != FGC_OP_SIMULATION || !vs.sim_intlks)
    {
        // Mode 1: NORMAL OPERATION

        if (sta.mode_op != FGC_OP_SIMULATION)
        {
            sta.cmd_req = DIG_OP_P;             // Read command register

            sta.inputs = 0;                     // Clear all inputs

            // Read inputs except those use to encode PLC commands:
            // COMHV-PS: VSNOCABLE
            // SVC: Status11
            // @TODO remove this when FGC_64 for the SVC is developed
            
            uint16_t ip_mask        = 0;
            uint16_t ip_direct_mask = 0;

            if (crateGetType() == FGC_CRATE_TYPE_COMHV_PS)
            {
                ip_mask        = DIG_IP1_VSNOCABLE_MASK16;
                ip_direct_mask = DIG_IPDIRECT_VSNOCABLE_MASK16;  
            }
            else if (strncmp("RPAFU", dev.type, FGC_SYSDB_SYS_LEN) == 0)
            {
                ip_direct_mask = DIG_IPDIRECT_STATUS11_MASK16;
            }

            sta.inputs    = DIG_IP1_P      & (~ip_mask);
            sta.ip_direct = DIG_IPDIRECT_P & (~ip_direct_mask);

            //  Patch to make prototype 2 work on delta converter
            if (crateGetType() == FGC_CRATE_TYPE_COMRCIAL_PC1)
            {
                // Inhibit subsequent signals, not properly mapped on interface board
                // Always OFF

                sta.inputs = sta.inputs & (~DIG_IP1_VSNOCABLE_MASK16);

                // Simulate VS_READY after VSPOWERON, this signal does not exist on this type of converter

                if (STATE_PC == FGC_PC_STARTING)
                {
                    if ((sta.inputs & DIG_IP1_VSPOWERON_MASK16) && (sta.time_ms > 200L))    // 200ms after starting
                    {
                        Set(sta.inputs, DIG_IP1_VSREADY_MASK16);
                    }
                    else
                    {
                        Clr(sta.inputs, DIG_IP1_VSREADY_MASK16);
                    }
                }

                if (!(sta.inputs & DIG_IP1_VSRUN_MASK16))       // If VSRUN command not present
                {
                    Clr(sta.inputs, (DIG_IP1_VSREADY_MASK16));  // Reset VSREADY
                }

                // Simulate OPBLOCKED, this signal does not exist on this type of converter

                if (DIG_OP_P & DIG_OP_SET_UNBLOCKCMD_MASK16)
                {
                    Clr(sta.inputs, DIG_IP1_OPBLOCKED_MASK16);
                }
                else
                {
                    Set(sta.inputs, DIG_IP1_OPBLOCKED_MASK16);
                }
            }
        }
        else
        {
            // Mode 2: SIMULATE VS SIGNALS ONLY

            sta.cmd_req &= (DIG_OP_SET_VSRUNCMD_MASK16 |                        // Clear commands except VSRUN
                            DIG_OP_SET_AFRUNCMD_MASK16);                        // and AFRUN

            Set(sta.cmd_req, (DIG_OP_P & ~(DIG_OP_SET_VSRUNCMD_MASK16 |         // Readback commands except VSRUN
                                           DIG_OP_SET_AFRUNCMD_MASK16)));        // and AFRUN

            // Preserve simulated VS signals
            sta.inputs &= DIG_IP1_PCPERMIT_MASK16 | DIG_IP1_VSRUN_MASK16 | DIG_IP1_VSREADY_MASK16
                          | DIG_IP1_VSPOWERON_MASK16;

            // Read inputs except sim VS signals
            Set(sta.inputs, (DIG_IP1_P & ~(DIG_IP1_PCPERMIT_MASK16
                                           | DIG_IP1_VSRUN_MASK16 | DIG_IP1_VSREADY_MASK16 | DIG_IP1_VSPOWERON_MASK16)));

            if (Test(sta.inputs, (DIG_IP1_PWRFAILURE_MASK16 |   // If any faults occured or
                                  DIG_IP1_FASTABORT_MASK16)))     // FAST ABORT received
            {
                Clr(sta.cmd_req, (DIG_OP_SET_VSRUNCMD_MASK16 |      // Simulate Dig interface by
                                  DIG_OP_SET_AFRUNCMD_MASK16));      // reseting VSRUN and AFRUN
            }
        }  // Mode 1 & 2: REAL INTERLOCKS
    }
    else
    {
        // Mode 3: SIMULATE ALL SIGNALS

        sta.inputs &= (DIG_IP1_PCPERMIT_MASK16 | DIG_IP1_VSRUN_MASK16 | DIG_IP1_VSREADY_MASK16 |
                       DIG_IP1_VSPOWERON_MASK16 | DIG_IP1_POLSWINEG_MASK16 | DIG_IP1_POLSWINEG_MASK16);

        if (FAULTS)
        {
            Set(sta.inputs, DIG_IP1_PWRFAILURE_MASK16);
        }

        // Simulate simplified polarity switch with instant response.
        // Default to positive polarity.

        if ((INT16U)dpcls.mcu.vs.polarity.timeout)
        {
            if (vs.req_polarity == FGC_POL_NEGATIVE)
            {
                Set(sta.inputs, DIG_IP1_POLSWINEG_MASK16);
            }
            else if (vs.req_polarity == FGC_POL_POSITIVE ||
                     (!Test(sta.inputs, DIG_IP1_POLSWINEG_MASK16) &&
                      !Test(sta.inputs, DIG_IP1_POLSWIPOS_MASK16)))
            {
                Set(sta.inputs, DIG_IP1_POLSWIPOS_MASK16);
            }

        }
    }

    // Modes 2 & 3: SIMULATE ANY SIGNALS

    if (sta.mode_op == FGC_OP_SIMULATION)
    {
        /*--- Simulate response to VS_RUN request ---*/

        if (sta.cmd_req & DIG_OP_SET_VSRUNCMD_MASK16)       // If VS_RUN command requested
        {
            if (!(sta.inputs & DIG_IP1_VSRUN_MASK16))                       // If readback of this command missing
            {
                Set(sta.inputs, (DIG_IP1_VSRUN_MASK16 | DIG_IP1_VSPOWERON_MASK16)); // Set VS_RUN and POWER_ON
            }
        }
        else                            // else VS_RUN command not requested
        {
            if (sta.inputs & DIG_IP1_VSRUN_MASK16)
            {
                //          DIG_IP1_VSREADY_MASK16
                //            0: VS is not ready
                //            1: VS has finished its initialisation

                Clr(sta.inputs, (DIG_IP1_VSREADY_MASK16 | DIG_IP1_VSRUN_MASK16)); // Reset VSREADY and VS_RUN
            }
        }

        // Simulate starting (3s) and stopping (5s) of voltage source
#if (FGC_CLASS_ID == 61)

        if (STATE_PC == FGC_PC_STARTING && sta.time_ms > 3000L)
        {
            sta.inputs |= DIG_IP1_VSREADY_MASK16;
        }

#elif (FGC_CLASS_ID == 62)

        if (STATE_PC == FGC_PC_TO_CYCLING && sta.time_ms > 1000L)
        {
            sta.inputs |= DIG_IP1_VSREADY_MASK16;
        }

#endif

        // Simulate OPBLOCKED

        if (DIG_OP_P & DIG_OP_SET_UNBLOCKCMD_MASK16)
        {
            Clr(sta.inputs, DIG_IP1_OPBLOCKED_MASK16);
        }
        else
        {
            Set(sta.inputs, DIG_IP1_OPBLOCKED_MASK16);
        }

        if (((sta.inputs & (DIG_IP1_VSRUN_MASK16 | DIG_IP1_VSPOWERON_MASK16)) == DIG_IP1_VSPOWERON_MASK16)
            && (sta.time_ms > 5000L)
           )
        {
            Clr(sta.inputs, DIG_IP1_VSPOWERON_MASK16);
        }

        if (STATE_PC == FGC_PC_TO_CYCLING && sta.time_ms > 1000L)
        {
            sta.inputs |= DIG_IP1_VSREADY_MASK16;
        }
    }

    // Determine polarity changer state from input signals

    polarity_state = dpcls.mcu.vs.polarity.state;

    Clr(ST_UNLATCHED, (FGC_UNL_POL_SWI_POS | FGC_UNL_POL_SWI_NEG));

    if ((INT16U)dpcls.mcu.vs.polarity.timeout)
    {
        dpcls.mcu.vs.polarity.state = 0;

        if (sta.inputs & DIG_IP1_POLSWIPOS_MASK16)
        {
            dpcls.mcu.vs.polarity.state = FGC_POL_POSITIVE;
            Set(ST_UNLATCHED, FGC_UNL_POL_SWI_POS);
        }

        if (sta.inputs & DIG_IP1_POLSWINEG_MASK16)
        {
            dpcls.mcu.vs.polarity.state = FGC_POL_NEGATIVE;
            Set(ST_UNLATCHED, FGC_UNL_POL_SWI_NEG);
        }

        if ((sta.inputs & DIG_IP1_POLSWIPOS_MASK16)
            && (sta.inputs & DIG_IP1_POLSWINEG_MASK16)
           )
        {
            dpcls.mcu.vs.polarity.state = FGC_POL_SWITCH_FAULT;
        }
        else
        {
            if (!(sta.inputs & DIG_IP1_POLSWIPOS_MASK16)
                && !(sta.inputs & DIG_IP1_POLSWINEG_MASK16)
               )
            {
                dpcls.mcu.vs.polarity.state = FGC_POL_SWITCH_MOVING;
            }
        }
    }
    else
    {
        dpcls.mcu.vs.polarity.state = FGC_POL_NO_SWITCH;
    }

    if (dpcls.mcu.vs.polarity.state != polarity_state)
    {
        PubProperty(&PROP_SWITCH_POLARITY_STATE, NON_PPM_USER, FALSE);
    }
}

static void StaLeds(void)
{
    /*
     * This function will set some of the LEDs.
     */

    // PSU LED

    // A RED led is for DCCT_PSU warning but the latter is not defined for class 61

    if (Test(WARNINGS, FGC_WRN_FGC_PSU))
    {
        // ORANGE
        LED_SET(PSU_RED);
        LED_SET(PSU_GREEN);
    }
    else
    {
        LED_RST(PSU_RED);
        LED_SET(PSU_GREEN);
    }

    // VS LED

    if (sta.inputs & DIG_IP1_VSPOWERON_MASK16)
    {
        LED_SET(VS_GREEN);
    }
    else
    {
        LED_RST(VS_GREEN);
    }

    if (Test(sta.inputs, (DIG_IP1_VSFAULT_MASK16 | DIG_IP1_VSEXTINTLK_MASK16)) || Test(FAULTS, FGC_FLT_VS_STATE))
    {
        LED_SET(VS_RED);
    }
    else
    {
        LED_RST(VS_RED);
    }

    // DCCT LED

    LED_RST(DCCT_GREEN);
    LED_RST(DCCT_RED);

    uint8_t dcct_flt_mask =   (Test(ST_DCCT_A, FGC_DCCT_FAULT) << 0)
                            + (Test(ST_DCCT_B, FGC_DCCT_FAULT) << 1);
    uint8_t dcct_flt      = dcct_flt_mask & (dpcls.mcu.dcct_select + 1);

    if (dcct_flt == 0)
    {
        LED_SET(DCCT_GREEN);
    }
    else if (dcct_flt == 3)
    {
        LED_SET(DCCT_RED);
    }
    else
    {
        LED_SET(DCCT_RED);

        if (dpcls.mcu.dcct_select == FGC_DCCT_SELECT_AB)
        {
            LED_SET(DCCT_GREEN);
        }
    }

    // PIC LED

    if ((FAULTS & FGC_FLT_FAST_ABORT)
        || (ST_UNLATCHED & (FGC_UNL_PWR_FAILURE | FGC_UNL_PWR_FAILURE))
       )
    {
        LED_SET(PIC_RED);
    }
    else
    {
        LED_RST(PIC_RED);
    }

    if (ST_UNLATCHED & FGC_UNL_PC_PERMIT)
    {
        LED_SET(PIC_GREEN);
    }
    else
    {
        LED_RST(PIC_GREEN);
    }
}

static void StaVsState(void)
{
    /*
     * This function is called to determine the voltage source state from five
     * direct digital inputs using a lookup table.
     */

    INT16U      idx;
    INT16U      state_vs;

    static INT8U vs_state[] =
    {
        //  IDX  OPBLOCKED:VSRUN:VSPOWERON:VSREADY:FASTABORT:VSFAULT|VSEXTINTLK
        FGC_VS_OFF,         //   0       0       0       0       0        0        0
        FGC_VS_FLT_OFF,     //   1       0       0       0       0        0        1
        FGC_VS_FASTPA_OFF,  //   2       0       0       0       0        1        0
        FGC_VS_FLT_OFF,     //   3       0       0       0       0        1        1
        FGC_VS_INVALID,     //   4       0       0       0       1        0        0
        FGC_VS_INVALID,     //   5       0       0       0       1        0        1
        FGC_VS_INVALID,     //   6       0       0       0       1        1        0
        FGC_VS_INVALID,     //   7       0       0       0       1        1        1
        FGC_VS_STOPPING,    //   8       0       0       1       0        0        0
        FGC_VS_FAST_STOP,   //   9       0       0       1       0        0        1
        FGC_VS_FAST_STOP,   //  10       0       0       1       0        1        0
        FGC_VS_FAST_STOP,   //  11       0       0       1       0        1        1
        FGC_VS_INVALID,     //  12       0       0       1       1        0        0
        FGC_VS_INVALID,     //  13       0       0       1       1        0        1
        FGC_VS_INVALID,     //  14       0       0       1       1        1        0
        FGC_VS_INVALID,     //  15       0       0       1       1        1        1
        FGC_VS_STARTING,    //  16       0       1       0       0        0        0
        FGC_VS_FLT_OFF,     //  17       0       1       0       0        0        1
        FGC_VS_FASTPA_OFF,  //  18       0       1       0       0        1        0
        FGC_VS_FLT_OFF,     //  19       0       1       0       0        1        1
        FGC_VS_INVALID,     //  20       0       1       0       1        0        0
        FGC_VS_INVALID,     //  21       0       1       0       1        0        1
        FGC_VS_INVALID,     //  22       0       1       0       1        1        0
        FGC_VS_INVALID,     //  23       0       1       0       1        1        1
        FGC_VS_STARTING,    //  24       0       1       1       0        0        0
        FGC_VS_FAST_STOP,   //  25       0       1       1       0        0        1
        FGC_VS_FAST_STOP,   //  26       0       1       1       0        1        0
        FGC_VS_FAST_STOP,   //  27       0       1       1       0        1        1
        FGC_VS_READY,       //  28       0       1       1       1        0        0
        FGC_VS_INVALID,     //  29       0       1       1       1        0        1
        FGC_VS_INVALID,     //  30       0       1       1       1        1        0
        FGC_VS_INVALID,     //  31       0       1       1       1        1        1
        FGC_VS_OFF,         //  32       1       0       0       0        0        0
        FGC_VS_FLT_OFF,     //  33       1       0       0       0        0        1
        FGC_VS_FASTPA_OFF,  //  34       1       0       0       0        1        0
        FGC_VS_FLT_OFF,     //  35       1       0       0       0        1        1
        FGC_VS_INVALID,     //  36       1       0       0       1        0        0
        FGC_VS_INVALID,     //  37       1       0       0       1        0        1
        FGC_VS_INVALID,     //  38       1       0       0       1        1        0
        FGC_VS_INVALID,     //  39       1       0       0       1        1        1
        FGC_VS_STOPPING,    //  40       1       0       1       0        0        0
        FGC_VS_FAST_STOP,   //  41       1       0       1       0        0        1
        FGC_VS_FAST_STOP,   //  42       1       0       1       0        1        0
        FGC_VS_FAST_STOP,   //  43       1       0       1       0        1        1
        FGC_VS_INVALID,     //  44       1       0       1       1        0        0
        FGC_VS_INVALID,     //  45       1       0       1       1        0        1
        FGC_VS_INVALID,     //  46       1       0       1       1        1        0
        FGC_VS_INVALID,     //  47       1       0       1       1        1        1
        FGC_VS_STARTING,    //  48       1       1       0       0        0        0
        FGC_VS_FLT_OFF,     //  49       1       1       0       0        0        1
        FGC_VS_FASTPA_OFF,  //  50       1       1       0       0        1        0
        FGC_VS_FLT_OFF,     //  51       1       1       0       0        1        1
        FGC_VS_INVALID,     //  52       1       1       0       1        0        0
        FGC_VS_INVALID,     //  53       1       1       0       1        0        1
        FGC_VS_INVALID,     //  54       1       1       0       1        1        0
        FGC_VS_INVALID,     //  55       1       1       0       1        1        1
        FGC_VS_BLOCKED,     //  56       1       1       1       0        0        0
        FGC_VS_FAST_STOP,   //  57       1       1       1       0        0        1
        FGC_VS_FAST_STOP,   //  58       1       1       1       0        1        0
        FGC_VS_FAST_STOP,   //  59       1       1       1       0        1        1
        FGC_VS_BLOCKED,     //  60       1       1       1       1        0        0
        FGC_VS_INVALID,     //  61       1       1       1       1        0        1
        FGC_VS_INVALID,     //  62       1       1       1       1        1        0
        FGC_VS_INVALID      //  63       1       1       1       1        1        1
    };

    // Create state index from the five direct digital inputs

    idx = 0;

    if (Test(sta.inputs, (DIG_IP1_VSFAULT_MASK16 | DIG_IP1_VSEXTINTLK_MASK16)))
    {
        Set(idx, 0x01);
    }

    if (Test(sta.inputs, DIG_IP1_FASTABORT_MASK16))
    {
        Set(idx, 0x02);
    }

    //    DIG_IP1_VSREADY_MASK16
    //      0: VS is not ready
    //      1: VS has finished its initialisation

    if (Test(sta.inputs, DIG_IP1_VSREADY_MASK16))
    {
        Set(idx, 0x04);
    }

    if (Test(sta.inputs, DIG_IP1_VSPOWERON_MASK16))
    {
        Set(idx, 0x08);
    }

    if (Test(sta.inputs, DIG_IP1_VSRUN_MASK16))
    {
        Set(idx, 0x10);
    }

    if (dpcls.mcu.vs.blockable != FGC_VS_BLOCK_DISABLED &&
        Test(sta.inputs, DIG_IP1_OPBLOCKED_MASK16))
    {
        Set(idx, 0x20);
    }

    // Check for unexpected loss of voltage source

    if (vs_state[idx] == FGC_VS_INVALID)
    {
        state_vs = FGC_VS_INVALID;
    }
    else
    {
        state_vs = vs_state[idx];
    }

#if (FGC_CLASS_ID == 61)

    if (state_vs == FGC_VS_STARTING &&   // If VSRUN active but VSREADY and/or VSPOWERON are not, and
        STATE_PC != FGC_PC_STARTING)     // converter is NOT starting
    {
        state_vs = FGC_VS_INVALID;      // Mark state as invalid
    }

#endif

    STATE_VS = state_vs;
}

static void StaAdcControl(void)
{
    /*
     * This function will control the ADC analogue and digital multiplexors
     * (if card is present).
     */
}

static void StaCheck(void)
{
    /*
     * This function is called to check the operational health of the system.
     */

    // Faults

    if (sta.inputs & DIG_IP1_VSFAULT_MASK16)
    {
        Set(sta.faults, FGC_FLT_VS_FAULT);
    }
    else
    {
        Clr(sta.faults, FGC_FLT_VS_FAULT);
    }

    if (sta.inputs & DIG_IP1_VSEXTINTLK_MASK16)
    {
        Set(sta.faults, FGC_FLT_VS_EXTINTLOCK);
    }
    else
    {
        Clr(sta.faults, FGC_FLT_VS_EXTINTLOCK);
    }

    if (sta.inputs & DIG_IP1_FASTABORT_MASK16)
    {
        Set(sta.faults, FGC_FLT_FAST_ABORT);    // 1: VS has received the PC_FAST_ABORT signal from PIC
    }
    else
    {
        Clr(sta.faults, FGC_FLT_FAST_ABORT);    // 0: PC_FAST_ABORT signal (from PIC) was not received by the VS
    }

    if (!Test(sta.inputs, DIG_IP1_VSRUN_MASK16))
    {
        if (Test(sta.inputs, DIG_IP1_PCPERMIT_MASK16))
        {
            Clr(sta.faults, FGC_FLT_NO_PC_PERMIT);
        }
        else
        {
            Set(sta.faults, FGC_FLT_NO_PC_PERMIT);
        }
    }

    // VS State invalid fault test

    if (STATE_VS == FGC_VS_INVALID)
    {
        vs.vsstate_counter++;

        if (!Test(FAULTS, FGC_FLT_VS_STATE)
            && (((sta.inputs & DIG_IP1_VSRUN_MASK16) && vs.vsstate_counter > 1)
                || (vs.vsstate_counter >= 100)                      // Timeout in 5ms units
               )
           )
        {
            Set(FAULTS, FGC_FLT_VS_STATE);
        }
    }
    else
    {
        vs.vsstate_counter = 0;
    }

    // Polarity switch warning and fault

    if (dpcls.mcu.vs.polarity.timeout)                        // If polarity switch is present
    {
#if (FGC_CLASS_ID == 61)
        if (dpcls.mcu.vs.polarity.state == FGC_POL_SWITCH_FAULT)        // If switch fault (POS and NEG at same time)
        {
            Set(sta.pol_switch_fault, POL_SWITCH_FAULT_STATE);
        }
#endif
        if (    crateGetType() == FGC_CRATE_TYPE_COBALT
             && Test(DIG_IPDIRECT_P, DIG_IPDIRECT_STATUS12_MASK16) == true)
        {
            Set(sta.pol_switch_fault, POL_SWITCH_FAULT_INPUT);
        }

        if (sta.sec_f                                           &&
            vs.polarity_counter < dpcls.mcu.vs.polarity.timeout &&
            ++vs.polarity_counter == dpcls.mcu.vs.polarity.timeout)
        {
            // Initialize SWITCH.POLARITY.MODE with SWITCH.POLARITY.STATE

            if (dpcls.mcu.vs.polarity.mode == FGC_POL_MODE_NOT_SET)
            {
                if (dpcls.mcu.vs.polarity.state == FGC_POL_POSITIVE)
                {
                    dpcls.mcu.vs.polarity.mode = FGC_POL_MODE_POSITIVE;
                }
                else if (dpcls.mcu.vs.polarity.state == FGC_POL_NEGATIVE)
                {
                    dpcls.mcu.vs.polarity.mode = FGC_POL_MODE_NEGATIVE;
                }

                if (dpcls.mcu.vs.polarity.mode != FGC_POL_MODE_NOT_SET)
                {
                    PubProperty(&PROP_SWITCH_POLARITY_MODE, NON_PPM_USER, FALSE);
                }
            }
#if (FGC_CLASS_ID == 61)
            if (dpcls.mcu.vs.polarity.state != dpcls.mcu.vs.polarity.mode)
            {
                // Converters connected to a ComHV-PS chassis behave specially. When turned off
                // they will always move to negative polarity, even if they were in positive.
                // To prevent a fault, the counter is only incremented when the state is in
                // BLOCKING or above.

                if (crateGetType() != FGC_CRATE_TYPE_COMHV_PS ||
                    PcStateAboveEqual(FGC_PC_BLOCKING))
                {
                    Set(sta.pol_switch_fault, POL_SWITCH_FAULT_TIMEOUT);
                }
            }
#endif
        }
    }

    if (   dpcls.mcu.vs.polarity.timeout != 0
        && sta.pol_switch_fault != POL_SWITCH_FAULT_NONE)
    {
        Set(sta.faults, FGC_FLT_POL_SWITCH);
    }

    // Warnings

    if (Test(ST_LATCHED, (  FGC_LAT_DIM_TRIG_FLT
#if (FGC_CLASS_ID == 61)
                          | FGC_LAT_VS_COMMS
#endif
                          | FGC_LAT_DSP_FLT
                          | FGC_LAT_ID_FLT
                          | FGC_LAT_DIMS_EXP_FLT
                          | FGC_LAT_SPIVS_FLT
                          | FGC_LAT_SCIVS_EXP_FLT )))
    {
        Set(WARNINGS, FGC_WRN_FGC_HW);
    }
    else
    {
        Clr(WARNINGS, FGC_WRN_FGC_HW);
    }

    if ( Test(ST_LATCHED, FGC_LAT_PSU_V_FAIL) )
    {
        Set(WARNINGS, FGC_WRN_FGC_PSU);
    }
    else
    {
        Clr(WARNINGS, FGC_WRN_FGC_PSU);
    }

    // Check the warning FGC_WRN_EXT_SYNC_LOST

    if (pll.ext_sync_f == false && pll.no_ext_sync == false)
    {
        Set(WARNINGS, FGC_WRN_EXT_SYNC_LOST);
    }
    else
    {
        Clr(WARNINGS, FGC_WRN_EXT_SYNC_LOST);
    }

    // Unlatched status

    if (sta.inputs & DIG_IP1_VSPOWERON_MASK16)
    {
        Set(ST_UNLATCHED, FGC_UNL_VS_POWER_ON);
    }
    else
    {
        Clr(ST_UNLATCHED, FGC_UNL_VS_POWER_ON);
    }

    // Unlatched status

    if (sta.inputs & DIG_IP1_PWRFAILURE_MASK16)
    {
        Set(ST_UNLATCHED, FGC_UNL_PWR_FAILURE);
    }
    else
    {
        Clr(ST_UNLATCHED, FGC_UNL_PWR_FAILURE);
    }

    if (!(sta.inputs & DIG_IP1_PCPERMIT_MASK16))
    {
        Clr(ST_UNLATCHED, FGC_UNL_PC_PERMIT);
    }
    else
    {
        Set(ST_UNLATCHED, FGC_UNL_PC_PERMIT);
    }

    // The VSNOCABLE input is used for external commands passing when working with ComHV-PS converter
    // In this case we should not check if the input state has changed

    if (
        ((crateGetType() != FGC_CRATE_TYPE_COMHV_PS) && (crateGetType() != FGC_CRATE_TYPE_RF25KV)) 
        && (sta.inputs & DIG_IP1_VSNOCABLE_MASK16)
        )
    {
        Set(ST_UNLATCHED, FGC_UNL_NO_VS_CABLE);
    }
    else
    {
        Clr(ST_UNLATCHED, FGC_UNL_NO_VS_CABLE);
    }

#if (FGC_CLASS_ID == 61)
    if ((INT16U)dpcls.dsp.meas.i_low_f &&
        STATE_PC != FGC_PC_CYCLING)   // LOW_CURRENT flag not relevant during cycling
    {
        Set(ST_UNLATCHED, FGC_UNL_LOW_CURRENT);
    }
    else
    {
        Clr(ST_UNLATCHED, FGC_UNL_LOW_CURRENT);
    }
#endif


    if ((INT16U)dpcls.mcu.mode_rt == FGC_CTRL_ENABLED)
    {
        Set(ST_UNLATCHED, FGC_UNL_REF_RT_ACTIVE);
    }
    else
    {
        Clr(ST_UNLATCHED, FGC_UNL_REF_RT_ACTIVE);
    }

    // DCCT status for DSP

#if (FGC_CLASS_ID == 61)

    if ((sta.inputs & DIG_IP1_DCCTAFLT_MASK16)        &&
        ((dpcls.mcu.dcct_select == FGC_DCCT_SELECT_A) ||
         (dpcls.mcu.dcct_select == FGC_DCCT_SELECT_AB)))
    {
        Set(sta.dcct_flts, 0x01);
    }

    if ((sta.inputs & DIG_IP1_DCCTBFLT_MASK16)        &&
        ((dpcls.mcu.dcct_select == FGC_DCCT_SELECT_B) ||
         (dpcls.mcu.dcct_select == FGC_DCCT_SELECT_AB)))
    {
        Set(sta.dcct_flts, 0x02);
    }

#elif (FGC_CLASS_ID == 62)

    if (sta.inputs & DIG_IP1_DCCTAFLT_MASK16)
    {
        Set(sta.dcct_flts, 0x01);
    }

#endif

    dpcls.mcu.dcct_flt = (INT32U)sta.dcct_flts;

    // Latch new faults

    FAULTS |= sta.faults;
}

static void StaPcState(void)
{
    PcFsmProcessState();
}

static void StaCalControl(void)
{
    /*
     * This function is called to run the auto calibration of ADCs, DCCTs and
     * DAC. The function CalRunSequence() is called when sta.cal_f is set,
     * which is only every 200ms.
     *
     * The function also checks if an automatic calibration of the internal
     * ADCs is required.
     */

    if (sta.sec_f)
    {
        if (cal.inhibit_cal)            // Down count inhibit_cal time at 1Hz
        {
            cal.inhibit_cal--;
        }

        if (sta.sync_db_cal_delay_s)        // Down count sync_db time at 1Hz
        {
            if (!--sta.sync_db_cal_delay_s && !FbsIsStandalone())
            {
                sta.config_mode = FGC_CFG_MODE_SYNC_DB_CAL;     // Request SYNC_DB_CAL by manager
            }
        }
    }

    if (STATE_OP == FGC_OP_CALIBRATING)     // If auto calibrating
    {
        if (((INT16U)sta.timestamp_ms.ms_time % 200) == (100 + DEV_STA_TSK_PHASE))
        {
            CalRunSequence();
        }
    }

    // Auto calibration every day (as defined per CAL_NO_AUTO_TIME)

    else if (sta.sec_f                   && // If start of new second and
             !mst.set_cmd_counter             &&  // no set command run in the past 5 seconds and
             sta.config_mode == FGC_CFG_MODE_SYNC_NONE   &&  // no sync request is outstanding and
             !cal.inhibit_cal             &&  // calibration is NOT inhibited at the moment and
             dev.set_lock->counter           &&  // set command not running (might be S CAL) and
             !SetifPcOff(NULL)               &&  // converter is OFF and
             !SetifCalOk(NULL)               &&  // states are okay for calibration and
             sta.time_ms > CAL_IN_STATE_DELAY        &&  // time in state is more than minimum
             (FbsIsStandalone() ||                     // not FIP/ETH connected or
              (fbs.gw_online_f                       // GW present
#if (FGC_CLASS_ID == 61)
              && Test(ST_UNLATCHED, FGC_UNL_LOW_CURRENT) // and LOW CURRENT and time
#endif              
              )) && 
             (TimeGetUtcTime() - cal.last_cal_time_unix) > CAL_NO_AUTO_TIME) // to calibrate
    {
        CalInitSequence(&cal_seq_int_adcs[0],           // Calibration sequence for the ADCs (all in parallel)
                        CAL_INTERNAL_ADCS,              // Calibrate all ADCs
                        0);                             // Last parameter (signal) is not relevant so set it to zero.

        CalRunSequence();               // run the first step of the calibration sequence
    }
}

static void StaCmdControl(void)
{
    /*
     * This function will drive or simulate the direct command outputs.
     */

    INT16U set;
    INT16U rst;
    static INT16U reset_counter;
    static INT16U polarity_counter;
#if (FGC_CLASS_ID == 62)
    if(interFgcIsMaster())
    {
        interFgcPrepareControlWord(reset_counter != 0);
        interFgcMonitorSlaveFault (reset_counter != 0);
    }
    else if(interFgcIsSlave())
    {
        interFgcProcessControlWord();
    }
#endif

    set = rst = 0;                  // Clear command masks

    // VS_RUN control

    if (!Test(sta.cmd_req, DIG_OP_SET_VSRUNCMD_MASK16) && // if VS_RUN is not active and
        Test(sta.cmd, DDOP_CMD_ON))         // ON requested
    {
        Set(set, DIG_OP_SET_VSRUNCMD_MASK16);           // Set VS_RUN
        sta.cal_active = FGC_CTRL_DISABLED;         // Disable CAL.ACTIVE property
    }

    if ((Test(sta.cmd, DDOP_CMD_OFF) || FAULTS) &&  // OFF requested or faults have been detected and
        Test(sta.cmd_req, DIG_OP_SET_VSRUNCMD_MASK16 |  // If VS_RUN is active or
             DIG_OP_SET_AFRUNCMD_MASK16))    // AFRUN is active
    {
        Set(rst, (DIG_OP_SET_VSRUNCMD_MASK16 |          // Clear VS_RUN
                  DIG_OP_SET_AFRUNCMD_MASK16));          // and AFRUN commands
    }

    // AFRUN control (used to turn on Active Filter if present)

    if (vs.active_filter == FGC_CTRL_ENABLED &&     // If active filter is enabled
        PcStateAboveEqual(FGC_PC_IDLE) &&           // state.pc is IDLE or higher, and
        sta.time_ms > 1000 &&                // time in state is > 1s, and
        !Test(sta.cmd_req, DIG_OP_SET_AFRUNCMD_MASK16))  // active filter command is OFF
    {
        Set(set, DIG_OP_SET_AFRUNCMD_MASK16);           // Set AFRUN to enabled active filter
    }
    else if ((vs.active_filter == FGC_CTRL_DISABLED ||  // else if (active filter is disabled, or
              PcStateBelow(FGC_PC_IDLE)) &&                    // state.pc is less than IDLE), and
             Test(sta.cmd_req, DIG_OP_SET_AFRUNCMD_MASK16))  // active filter command is ON
    {
        Set(rst, DIG_OP_SET_AFRUNCMD_MASK16);                   // Reset AFRUN to disable active filter
    }

    // Polarity switch control

    if (polarity_counter == 0)                              // If Polarity command not in progress
    {
        // Converters connected to a ComHV-PS chassis behave specially. When turned off
        // they will always move to negative polarity, even if they were in positive.
        // When turned back on, the polarity must be moved to positive again.

        if (crateGetType() == FGC_CRATE_TYPE_COMHV_PS)
        {
            static BOOLEAN send_cmd_once = TRUE;

            if (dpcls.mcu.vs.polarity.mode == FGC_POL_MODE_POSITIVE  &&
                dpcls.mcu.vs.polarity.state == FGC_POL_NEGATIVE      &&
                PcStateEqual(FGC_PC_BLOCKING))
            {
                if (send_cmd_once)
                {
                    Set(sta.cmd, DDOP_CMD_POL_POS);
                    send_cmd_once = FALSE;
                }
            }
            else
            {
                send_cmd_once = TRUE;
            }
        }

        if (Test(sta.cmd, DDOP_CMD_POL_POS))                    // If POL POS requested
        {
            polarity_counter = 10;                       // Start polarity command sequence (10ms)

            Set(set, DIG_OP_SET_POLTOPOSCMD_MASK16);        // Set external POL POS line
            vs.req_polarity = FGC_POL_POSITIVE;
            vs.polarity_counter = 0;
        }
        else if (Test(sta.cmd, DDOP_CMD_POL_NEG))                   // If POL NEG requested
        {
            polarity_counter = 10;                       // Start polarity command sequence (10ms)

            Set(set, DIG_OP_SET_POLTONEGCMD_MASK16);        // Set external POL NEG line
            vs.req_polarity = FGC_POL_NEGATIVE;
            vs.polarity_counter = 0;
        }
    }
    else                                            //else polarity command is in progress
    {
        polarity_counter--;

        if (polarity_counter == 0)                              // After 50ms
        {
            Set(rst, (DIG_OP_SET_POLTOPOSCMD_MASK16 | DIG_OP_SET_POLTONEGCMD_MASK16));  // Clear polarity request
        }
    }

    // FGCOKCMD control

    // If non-interlock faults or VS faultsare latched

    if ( (FAULTS & ~(FGC_FLT_NO_PC_PERMIT | FGC_FLT_FAST_ABORT) ))
    {
        if (Test(sta.cmd_req, DIG_OP_SET_FGCOKCMD_MASK16))             // if FGCOKCMD command is not active
        {
            rst |= DIG_OP_SET_FGCOKCMD_MASK16;                 // Set FGCOKCMD command
            DIG_INTLK_OUT_P = DIG_INTLK_OUT_SET_PWRFAILURE_MASK16;
        }
    }
    else                                        // else there are no latched faults
    {
        if (!Test(sta.cmd_req, DIG_OP_SET_FGCOKCMD_MASK16))                // if FGCOKCMD command is active
        {
            set |= DIG_OP_SET_FGCOKCMD_MASK16;                 // Set FGCOKCMD command
            DIG_INTLK_OUT_P = DIG_INTLK_OUT_RST_PWRFAILURE_MASK16;
        }
    }

    // Deactivate VS output stage blocking (pin C19, OUTPUT_BLOCK)

    if ((sta.cmd & DDOP_CMD_UNBLOCK) != 0)
    {
        set |= DIG_OP_SET_UNBLOCKCMD_MASK16;
    }

    // Activate VS output stage blocking (pin C19, OUTPUT_BLOCK)

    if ((sta.cmd & DDOP_CMD_BLOCKING) != 0)
    {
        rst |= DIG_OP_SET_UNBLOCKCMD_MASK16;
    }

    //----------------------------------------------------------
    // pulse generation of RESET signal

    // If reset not in progress

    if (reset_counter == 0)
    {
        // If reset requested

        if (sta.cmd & DDOP_CMD_RESET)
        {
            reset_counter = RESET_COMMAND_DURATION;

            // In case of master, wait additional time before reset command is issued
            // to give time to the slaves to do the reset

            if(interFgcIsMaster())
            {
                reset_counter += RESET_COMMAND_DURATION + 1;
            }
        }
    }

    if (reset_counter != 0)
    {
        if (reset_counter == RESET_COMMAND_DURATION)
        {
            // Set external reset line

            set |= DIG_OP_SET_VSRESETCMD_MASK16;
        }
        else if(reset_counter < RESET_COMMAND_DURATION)
        {
            FAULTS        = sta.faults | (INT16U) dpcom.dsp.faults;   // Clear faults latch
            sta.dcct_flts = 0;                        // Clear DCCT faults latch
#if (FGC_CLASS_ID == 62)
            interFgcResetRuntimeFaults();
#endif
        }

        reset_counter--;

        if (reset_counter == 0)
        {
            rst |= DIG_OP_SET_VSRESETCMD_MASK16;        // Clear external reset line

            // Clear polarity switch fault

            if (dpcls.mcu.vs.polarity.state == vs.req_polarity)
            {
                vs.polarity_counter = dpcls.mcu.vs.polarity.timeout;

                Clr(sta.pol_switch_fault, POL_SWITCH_FAULT_TIMEOUT);
            }

            if (dpcls.mcu.vs.polarity.state != FGC_POL_SWITCH_FAULT)
            {
                Clr(sta.pol_switch_fault, POL_SWITCH_FAULT_STATE);
            }

            if (    crateGetType() == FGC_CRATE_TYPE_COBALT
                 && Test(DIG_IPDIRECT_P, DIG_IPDIRECT_STATUS12_MASK16) == false)
            {
                Clr(sta.pol_switch_fault, POL_SWITCH_FAULT_INPUT);
            }
        }
    }

    if (   dpcls.mcu.vs.polarity.timeout == 0
        || sta.pol_switch_fault == POL_SWITCH_FAULT_NONE)
    {
        Clr(FAULTS, FGC_FLT_POL_SWITCH);
        Clr(sta.faults, FGC_FLT_POL_SWITCH);
    }



    //----------------------------------------------------------

    // Send or simulate command to digital interface

    if (sta.mode_op == FGC_OP_SIMULATION)       // If operating mode is SIMULATION
    {
        sta.cmd_req = (sta.cmd_req & ~rst) | set;   // Simulate command

        Clr(set, (DIG_OP_SET_VSRUNCMD_MASK16 |      // Block VSRUN and
                  DIG_OP_SET_AFRUNCMD_MASK16));      // AFRUN from being generated
    }

    //----------------------------------------------------------
    // output the signals
    if (set ^ rst)              // If any command bits need to be changed
    {
        DIG_OP_P = (rst << 8) | set;        // Write reset/set mask to command register
    }

    //----------------------------------------------------------

    sta.cmd = 0;            // Clear command variable
}

static void StaPubCyclingProp()
{
    /*
     * This function will check whether some of the cycling properties need
     * to be published
     */

    INT16U user;

#if (FGC_CLASS_ID == 61)

    if (sta.cyc_start_new_cycle_f == TRUE)               // Property LOG.CYC
    {
        PubProperty(&PROP_LOG_CYC_STATUS,      dpcom.dsp.cyc.prev_user, FALSE);
        PubProperty(&PROP_LOG_CYC_MAX_ABS_ERR, dpcom.dsp.cyc.prev_user, FALSE);

        sta.cyc_start_new_cycle_f = FALSE;
    }


    if ((INT16S)dpcls.dsp.notify.ref_cyc_fault >= 0)
    {
        PubProperty(&PROP_REF_CYC_FAULT_USER, NON_PPM_USER, FALSE);
        PubProperty(&PROP_REF_CYC_FAULT_CHK,  NON_PPM_USER, FALSE);
        PubProperty(&PROP_REF_CYC_FAULT_DATA, NON_PPM_USER, FALSE);

        dpcls.dsp.notify.ref_cyc_fault = -1;
    }

    if ((INT16S)dpcls.dsp.notify.ref_cyc_warning >= 0 &&
        (INT16S)dpcls.dsp.notify.ref_cyc_warning <= FGC_MAX_USER)
    {
        PubProperty(&PROP_REF_CYC_WARNING_DATA, dpcls.dsp.notify.ref_cyc_warning, FALSE);
        PubProperty(&PROP_REF_CYC_WARNING_CHK,  dpcls.dsp.notify.ref_cyc_warning, FALSE);

        dpcls.dsp.notify.ref_cyc_warning = -1;
    }

    if (dpcls.dsp.notify.acq_meas_ready != -1)
    {
        PubProperty(&PROP_MEAS_ACQ_VALUE, dpcls.dsp.notify.acq_meas_ready, FALSE);

        dpcls.dsp.notify.acq_meas_ready = -1;
    }

#endif

    if ((INT16S)dpcls.dsp.notify.log_oasis_user >= 0)
    {
        user = dpcls.dsp.notify.log_oasis_user;

        PubProperty(&PROP_LOG_OASIS_I_REF_DATA,         user, FALSE);
        PubProperty(&PROP_LOG_OASIS_I_REF_INTERVAL_NS,  user, FALSE);
        PubProperty(&PROP_LOG_OASIS_I_MEAS_DATA,        user, FALSE);
        PubProperty(&PROP_LOG_OASIS_I_MEAS_INTERVAL_NS, user, FALSE);

        dpcls.dsp.notify.log_oasis_user = -1;
    }
}

static void Sta2HzPublication()
{
    /*
     * This function will publish the FGC state properties at a frequency of
     * 2Hz. This function is complementary to the publication mechanism in
     * place for the properties tracked in the log event (LOG.EVT) for which
     * the list of properties is the array log_evt_prop[]. The properties
     * published by this function must be non PPM ones.
     */
#if (FGC_CLASS_ID == 61)
    static struct prop * publish_list[] =       // List of properties to publish @ 2Hz
    {
        &PROP_REF_I,
        &PROP_REF_V,
        &PROP_MEAS_I,
        &PROP_MEAS_V,
        &PROP_MEAS_I_EARTH,
        &PROP_MEAS_I_EARTH_PCNT,
        &PROP_MEAS_I_DIFF_MA,
        &PROP_MEAS_I_ERR_MA,
        NULL
    };
#elif (FGC_CLASS_ID == 62)
    static struct prop * publish_list[] =       // List of properties to publish @ 2Hz
    {
        &PROP_REF_PULSE_REF,
        &PROP_MEAS_I,
        &PROP_MEAS_V,
        &PROP_MEAS_V_CAPA,
        &PROP_MEAS_I_EARTH,
        &PROP_MEAS_I_EARTH_PCNT,
        &PROP_MEAS_I_ERR_MA,
        NULL
    };
#endif

    static struct prop ** p = &publish_list[0]; // Pointer to one of the properties in publish_list
    // (or NULL if end of list)

    if (sta.tick_2hz_f)
    {
        sta.tick_2hz_f = FALSE;
        p = &publish_list[0];           // point to the beginning of the publish list
    }

    if (*p != NULL)
    {
        PubProperty(*p, NON_PPM_USER, FALSE);

        p++;                            // Jump to the next property pointer in publish_list[]
    }
}

// External function definitions

void StaInit(void)
{
    AdcInit();

    rtd.ctrl           = FGC_RTD_OFF;
    STATE_OP           = FGC_OP_UNCONFIGURED;
    STATE_PC           = FGC_PC_OFF;         // Start PC state in OFF
    dpcls.mcu.state_pc = STATE_PC;           // Transfer state to DSP

    sta.mode_pc            = FGC_PC_OFF;
    sta.mode_pc_simplified = FGC_PC_OFF;

    sta.copy_log_capture_state = FGC_LOG_RUNNING;
    sta.copy_log_capture_user  = -1;
    sta.copy_log_capture_cyc_chk_time.unix_time = 0;
    sta.copy_log_capture_cyc_chk_time.us_time   = 0;

    sta.pol_switch_fault = POL_SWITCH_FAULT_NONE;

    PubProperty(&PROP_MODE_PC, NON_PPM_USER, FALSE);
    PubProperty(&PROP_MODE_PC_SIMPLIFIED, NON_PPM_USER, FALSE);
}

void StaTsk(void * unused)
{
    cal.inhibit_cal  = 30;      // Suppress auto-calibration for the first 30s

    // ToDo: look for the proper place to put this
    P7.DR.BIT.B6 = 0;           // Enable digital CMD output

    for (;;)            // Main loop @ 200Hz
    {
        TskTraceReset();

        OSTskSuspend();             // Wait for OSTskResume() from MstTsk();

        TskTraceInc();

        sta.sec_f     = (TimeGetUtcTimeMs() == DEV_STA_TSK_PHASE);

        sta.time_ms  += 5L;                                     // Increment state time by 5ms
        sta.timestamp_ms.unix_time = TimeGetUtcTime();          // Calculate iteration timestamp for logging
        sta.timestamp_ms.ms_time   = TimeGetUtcTimeMs();

        AbsTimeCopyFromMsToUs(&sta.timestamp_ms, &sta.timestamp_us);

        TskTraceInc();

        if (STATE_OP != FGC_OP_UNCONFIGURED)            // If configured
        {
            StaOpState();

            TskTraceInc();

            StaDdips();

            TskTraceInc();

            StaVsState();

            TskTraceInc();

            StaCalControl();

            TskTraceInc();

            StaAdcControl();

            TskTraceInc();

            StaCheck();                 // Run health checks

            TskTraceInc();

            StaLeds();                  // Set front panel LEDs

            TskTraceInc();

            StaPcState();               // Control Power Converter state

            TskTraceInc();

            DiagCheckConverter();       // Check sub converter & FW diode status (at 20ms)

            TskTraceInc();

            StaCmdControl();            // Control direct command outputs

            TskTraceInc();

            Sta2HzPublication();        // Publish state properties at 2Hz

            TskTraceInc();

            // If standalone or UTC time received

            if (FbsIsStandalone() || sta.timestamp_ms.unix_time)
            {
                LogEvtProp();               // Log property changes in event log

#if (FGC_CLASS_ID == 61)

                if (STATE_PC != FGC_PC_CYCLING && timing_log.out_idx < FGC_LOG_TIMING_LEN)
                {
                    LogEvtTiming();             // Log cycle datas in event log
                }

#endif

                if (diag.n_dims)            // If system contains DIMs
                {
                    LogEvtDim();                // Log DIM statuses in the event log
                }
            }

            TskTraceInc();

            // Publication of properties

            if (sta.sec_f)                                      // If start of the second
            {
                PubProperty(&PROP_TIME_NOW, NON_PPM_USER, FALSE);  // Publish TIME.NOW

                TskTraceInc();
            }

            TskTraceInc();

            if (DEVICE_CYC == FGC_CTRL_ENABLED)
            {
                StaPubCyclingProp();
            }

            TskTraceInc();
        }
        else                    // else if FGC is unconfigured
        {
            STATE_PC = FGC_PC_FLT_OFF;          // Force FLT_OFF state
            StaCheck();                         // Run health checks
            StaLeds();                          // Set front panel LEDs
        }

        TskTraceInc();

        sta.watchdog = 0;       // Flag that StaTsk has completed it's iteration
    }
}

// EOF
