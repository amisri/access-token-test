/*!
 *  @file      adc.h
 *  @defgroup  FGC3:MCU
 *  @brief     ADC functions
 */

#define ADC_GLOBALS

// Includes

#include <adc.h>
#include <hash.h>               // for HASH_VALUE and struct hash_entry needed by defprops.h
#include <defprops.h>           // Include property related definitions
#include <dpcls.h>
#include <macros.h>
#include <defconst.h>           // for FGC_INTERFACE_*
#include <fir_fgc3_6x.h>        // fir_coef[] generated from MATLAB

// External function definitions

void AdcInit(void)
{
    adc.fir_err = AdcUpdateFIR();
}

void AdcFiltersResetRequest(BOOLEAN user_request_f)
{
    // Clear request flags from MCU and DSP to acknowledge that the reset request is being treated

    adc.pending_reset_req_f = FALSE;                    // 200ms flag

    if (dpcls.dsp.adc.reset_req == ADC_DSP_RESET_REQ)
    {
        dpcls.dsp.adc.reset_req = ADC_DSP_RESET_WAIT;   // DSP reset request in WAIT state
    }
}

void AdcFiltersReset(void)
{
    adc.trigger_reset_f = FALSE;
    /*
        if(shared_mem.analog_card_model == FGC_INTERFACE_SD_350 ||
           shared_mem.analog_card_model == FGC_INTERFACE_SD_351)
        {
            Set(ANA_CTRL_P,ANA_CTRL_FLTRESET_MASK16);       // Reset filter FPGAs
            Clr(ANA_CTRL_P,ANA_CTRL_FLTRESET_MASK16);       // Reprogramming filters and run in normal operation

            adc.reset_counter_ms = 0;                               // Start a timeout counter

            adc.in_reset_f              = TRUE;
            adc.last_reset_unix_time    = mst.time.unix_time;
            dpcls.mcu.adc.filter_state  = FGC_ADC_STATUS_RESETTING;
            dpcls.dsp.adc.reset_req     = ADC_DSP_RESET_ACK;        // Acknowledge the reset if asked by the DSP
        }
    */
}

void AdcFiltersResetCheck(void)
{
    /*
     * This is a three stage process:
     *    1. Wait for FPGA reprogramming to complete (usually 28ms);
     *    2. Set the filter index for each channel to enable data
     *       acquisition (immediate);
     *    3. Wait for the first valid data to come out of the filter,
     *    i.e. 2ms which is the filter length.
     */

    adc.reset_counter_ms++;
}

void AdcFiltersReadFunctions(void)
{
    /*
     * The SD Filter flash contains five functions (0-4):
     *     0 - Operational filter for 500kHz ADCs (internal or external)
     *     1 - Test filter for 500kHz ADCs (internal or external)
     *     2 - Operational filter for 1MHz ADCs (external CERN 22-bit ADC)
     *     3 - Test filter for 1MHz ADCs (external CERN 22-bit ADC)
     *     4 - Operational filter for external ADS1281 ADC
     */

    ;
}

void AdcFiltersSetIndex(INT16U filter_idx_a, INT16U filter_idx_b)
{
    ;
}

void AdcCpyMpx()
{
    INT32U adc_idx;

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
    {
        adc.internal_adc_mpx[adc_idx] = (INT16U)dpcls.dsp.ana.mpx[adc_idx];
    }
}

INT16U AdcUpdateFIR(void)
{
    INT16U  ret_err = 0;
    INT16U  fir_n_order;

    for (fir_n_order = 0; fir_n_order < FIR_ORDER; fir_n_order++)
    {
        // ToDo take real bitrate, not implemented yet (set to 0 -> 8MHz in boot)
        FIR_A_Z_A[fir_n_order] = fir_factor[fir_n_order] * TICK_DSP_PERIOD_US_P * adc_bitrate_mhz_map[0];
        FIR_B_Z_A[fir_n_order] = fir_factor[fir_n_order] * TICK_DSP_PERIOD_US_P * adc_bitrate_mhz_map[0];
        FIR_C_Z_A[fir_n_order] = fir_factor[fir_n_order] * TICK_DSP_PERIOD_US_P * adc_bitrate_mhz_map[0];
        FIR_D_Z_A[fir_n_order] = fir_factor[fir_n_order] * TICK_DSP_PERIOD_US_P * adc_bitrate_mhz_map[0];
    }

    // Filter stage length should not exceed 16000
    if (FIR_A_SUM_Z_P > FIR_MAX_LENGTH)
    {
        ret_err = 1;
    }

    if (FIR_B_SUM_Z_P > FIR_MAX_LENGTH)
    {
        ret_err = 2;
    }

    if (FIR_C_SUM_Z_P > FIR_MAX_LENGTH)
    {
        ret_err = 3;
    }

    if (FIR_D_SUM_Z_P > FIR_MAX_LENGTH)
    {
        ret_err = 4;
    }

    return (ret_err);
}

// EOFF
