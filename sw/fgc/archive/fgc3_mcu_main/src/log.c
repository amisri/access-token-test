/*---------------------------------------------------------------------------------------------------------*\
  File:     log.c

  Purpose:  FGC MCU Software - Logging related functions

  Note:     LogInitEvtProp() makes links between bits and enums for a property with a symlist
        in the structure log_evt_prop.  This means that the symbol constant values for any
        property logged must NEVER be outside the range 0-15.  If this happens then the
        system will crash itself at start up with E=0xBAD0.  This will be visible in the run log.
\*---------------------------------------------------------------------------------------------------------*/

#define LOG_GLOBALS         // define log_thour, log_tday global variables
#define CLASS_GLOBALS       // define masks[] global variable

#include <log.h>

#include <class.h>
#include <definfo.h>
#include <dev.h>
#include <diag.h>
#include <DIMsDataProcess.h>
#include <dls.h>
#include <dpcom.h>
#include <fbs_class.h>
#include <fbs.h>
#include <fgc_consts_gen.h>
#include <fgc_consts_gen.h>
#include <log_class.h>
#include <log_cycle.h>
#include <macros.h>
#include <math.h>
#include <mcu_dependent.h>
#include <mem.h>
#include <memmap_mcu.h>
#include <property.h>
#include <pub.h>
#include <qspi_bus.h>
#include <ref.h>
#include <shared_memory.h>
#include <sta.h>
#include <start.h>
#include <stdio.h>
#include <string.h>
#include <fgc/fgc_db.h>


static void LogEvtCheck(void);
static void LogInitEvtProp(void);
static void LogStart(struct log_ana_vars * log_ana);
static void LogEvtPropStore(const char  * prop_name, const char  * symbol, INT16U set_f, INT16U bitmask_f);
static void LogEvtDimInput(BOOLEAN input_zero_f, BOOLEAN fault_input_f, BOOLEAN dimdb_lbl_is_null);

/*---------------------------------------------------------------------------------------------------------*/
void LogInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main() to set up the logs - this is before the OS has started.
\*---------------------------------------------------------------------------------------------------------*/
{
    log_evt.dim_log_rec.prop[0]  = 'D';             // Prepare DIM log entry header
    log_evt.dim_log_rec.prop[1]  = 'I';
    log_evt.dim_log_rec.prop[2]  = 'M';
    log_evt.dim_log_rec.prop[3]  = '.';

    log_evt.sample_size = sizeof(struct log_evt_rec);

    // Restore temperature log times and indexes from before reset
    log_thour.last_sample_idx = shared_mem.temperatureLogHour_index;
    log_tday.last_sample_idx  = shared_mem.temperatureLogDay_index;

    log_thour.last_sample_time.unix_time = shared_mem.temperatureLogHour_timestamp;
    log_tday.last_sample_time.unix_time  = shared_mem.temperatureLogDay_timestamp;

    LogCycleInit();

    LogEvtCheck();                      // Check event log and clear if corrupted

    LogInitEvtProp();                       // Init event log table

#if (FGC_CLASS_ID == 61)
    LogStart(&log_iab);                     // Clear and start IAB log
    LogStart(&log_ireg);                    // Clear and restart IREG log
#elif (FGC_CLASS_ID == 62)
    LogStart(&log_pulse);                    // Clear and restart PULSE log
#endif

    LogStart(&log_iearth);                  // Clear and start IEARTH log
    LogTStart(&log_thour);                  // Clear and start THOUR log

    dev.log_pm_state = FGC_LOG_RUNNING;                 // Set PM log state to RUNNING
}
/*---------------------------------------------------------------------------------------------------------*/
void LogGetPmSigBuf(struct log_ana_vars * log, struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This is a helper function for GetLogPm(), which is class specific.  It will export an operational log
  in binary format followed by its timestamp to the FIP command stream.  The log will always be frozen
  before this is called.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      n;
    INT16U      s;
    INT16U      sample_size;
    INT16U      idx;
    INT8U   *   bin_data;
    char    *   addr;
    INT8U       values[64];     // Buffer must be size of largest log sample

    n   = log->n_samples;           // Get length of buffer in samples
    idx = log->last_sample_idx;         // Get most recent entry index
    sample_size = log->sample_size;     // Get local copy of sample size in bytes

    while (n--)                 // Write each sample in binary to FIP stream
    {
        idx    = (idx + 1) % log->n_samples;        // Pre-increment sample index
        addr   = (char *)(log->baseaddr + sample_size * idx);

        memcpy(&values, addr, sample_size); // Extract one sample

        s = sample_size;
        bin_data = (INT8U *) &values;

        while (s--)
        {
            c->store_cb(*(bin_data++));
        }
    }

    FbsOutLong((char *) &log->last_sample_time.unix_time, c);       // as unix time and
    FbsOutLong((char *) &log->last_sample_time.us_time, c);         // microseconds
}
/*---------------------------------------------------------------------------------------------------------*/
void LogGetPmEvtBuf(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This is a helper function for GetLogPm(), which is class specific.  It will export the event log
  in binary format.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR           cpu_sr;
    INT16U              n;
    INT16U              n_bytes;
    INT16U              idx;
    INT8S       *       bin_data;
    struct log_evt_rec  evt_rec;

    n = FGC_LOG_EVT_LEN;            // Get number of data elements in array
    idx = log_evt.index;            // Get most recent entry index

    while (n--)
    {
        idx++;

        if (idx >= FGC_LOG_EVT_LEN)         // Export oldest first
        {
            idx = 0;
        }

        OS_ENTER_CRITICAL();

        memcpy(&evt_rec, (char *) SRAM_LOG_EVT_32 + log_evt.sample_size * idx, sizeof(struct log_evt_rec));

        OS_EXIT_CRITICAL();

        n_bytes  = log_evt.sample_size;
        bin_data = (INT8S *)&evt_rec;

        while (n_bytes--)
        {
            c->store_cb(*bin_data);
            bin_data++;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks to see if the contents of the event log are corrupted.  If so, it clears the log.
  To pass the check, the saved event log index must be in range (0-699) and every record's unix time stamp
  must be zero, or within one month of the restart time.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U                      i;
    INT32U                      unix_time;
    INT32U                      low_limit;
    INT32U                      high_limit;
    struct log_evt_rec     *    evt_rec_fp;

    log_evt.index = shared_mem.logEvent_index;             // Restore event log index from before reset

    if (log_evt.index < FGC_LOG_EVT_LEN)
    {
        evt_rec_fp = (struct log_evt_rec *) SRAM_LOG_EVT_32;
        high_limit = TimeGetUtcTime();
        low_limit  = high_limit - 2500000;          // Last month only

        for (i = 0; i < FGC_LOG_EVT_LEN; ++i, ++evt_rec_fp)
        {
            unix_time = evt_rec_fp->timestamp.unix_time;

            if (unix_time && ((unix_time < low_limit) || (unix_time > high_limit)))
            {
                log_evt.index = FGC_LOG_EVT_LEN;        // Flag event log as corrupted
                break;
            }
        }
    }

    if (log_evt.index >= FGC_LOG_EVT_LEN)       // If log was found to be corrupted
    {
        MemSetWords((void *) SRAM_LOG_EVT_32, 0x0000, SRAM_LOG_EVT_W);    // Clear Event log
        log_evt.index = shared_mem.logEvent_index = 0;      // Reset log index
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogInitEvtProp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from LogInit() to initialise the event log table that is partially defined at
  the start of the logXX.c file where XX is the class ID.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U                  bit_idx;
    struct sym_lst     *    sym_lst;
    struct log_evt_prop  *  lp;         // Pointer to entry in event log property table

    // For each entry in the event log property table
    for (lp = (struct log_evt_prop *) &log_evt_prop; lp->prop ; lp++)
    {
        if (lp->prop->flags & PF_SYM_LST)       // If property has a symbol list
        {
            sym_lst = (struct sym_lst *)lp->prop->range;    // Get start of symlist

            if (!(lp->prop->flags & PF_BIT_MASK))       // If property is not a bit mask symlist
            {
                while (sym_lst->sym_idx)                // For each symbol in the list
                {
                    if (sym_lst->value > FGC_MAX_SYMLIST_VALUE) // If const data out of range
                    {
                        Crash(0xBAD0);                          // Signal the failure
                    }

                    lp->symbols[sym_lst->value] =
                        SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c; // Link to symbol
                    sym_lst++;
                }
            }
            else                    // else property is a bit mask symlist
            {
                for (bit_idx = 0; bit_idx < 16; bit_idx++)      // For each bit 0 to 15
                {
                    lp->symbols[bit_idx] = bitmask_default_sym[bit_idx];// Set a default symbol name
                }

                while (sym_lst->sym_idx)            // For each symbol in list
                {
                    MCU_FF1(sym_lst->value, bit_idx);                   // Find first bit that is set. Answer stored in bit_idx.

                    lp->symbols[bit_idx] = SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c; // Link to symbol

                    sym_lst++;
                }
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogStartAll(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to restart the all data logging.  It will clear the FGC_UNL_POST_MORTEM bit.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 61)
    LogStart(&log_iab);                 // Clear and restart IAB log
    LogStart(&log_ireg);                // Clear and restart IREG log
#elif (FGC_CLASS_ID == 62)
    LogStart(&log_pulse);               // Clear and restart PULSE log
#endif
    LogStart(&log_iearth);              // Clear and restart IEARTH log
    LogTStart(&log_thour);              // Restart THOUR log
    Clr(ST_UNLATCHED, FGC_UNL_POST_MORTEM);     // Clear post mortem flag
    Clr(fbs.u.fieldbus_stat.ack, FGC_SELF_PM_REQ);  // Reset self-trig PM dump request

    dev.log_pm_state = FGC_LOG_RUNNING;         // Set PM log state to RUNNING
}
/*---------------------------------------------------------------------------------------------------------*/
void LogStart(struct log_ana_vars * log_ana)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to restart logging for the specified buffer
\*---------------------------------------------------------------------------------------------------------*/
{
    if (!log_ana->run_f)                // If not already running
    {
        MemSetWords((void *) log_ana->baseaddr, 0x0000, log_ana->bufsize_w);  // Clear log buffer
        log_ana->samples_to_acq = log_ana->post_trig;       // Set number of post trig samples
        log_ana->run_f = TRUE;                  // Start logging
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogTStart(struct log_ana_vars * log_ana)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to restart the temperature logging for the specified buffer.  This attempts to
  preserve as much of the existing data as possible.  To do this it uses the time of the last sample saved
  in the SM area.  It will clear to zero every entry that was missed.  The temperature buffers are in
  page 0 so the mode register doesn't need to be manipulated.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U  duration_ms;                // Log duration in milliseconds
    INT32U  t_missing_ms;               // Missed time in milliseconds
    INT16U  n_missing;              // Missed samples
    INT16U  offset;
    INT16U  sample_size_w;              // Sample size in words

    if (!log_ana->run_f)                // If not already running
    {
        t_missing_ms = (TimeGetUtcTime() - log_ana->last_sample_time.unix_time) * 1000;
        duration_ms  = log_ana->period_ms * log_ana->n_samples;

        if (t_missing_ms > duration_ms)             // If no valid data is left in the buffer
        {
            MemSetWords((char *) log_ana->baseaddr, 0x0000, log_ana->bufsize_w);  // Clear log buffer
            log_ana->last_sample_idx = 0;               // Reset index
        }
        else
        {
            n_missing     = t_missing_ms / log_ana->period_ms;
            sample_size_w = log_ana->sample_size / 2;

            while (n_missing--)
            {
                log_ana->last_sample_idx = (log_ana->last_sample_idx + 1) % log_ana->n_samples;
                offset = log_ana->last_sample_idx * log_ana->sample_size;

                MemSetWords((char *) log_ana->baseaddr + offset, 0x0000, sample_size_w);
            }
        }

        log_ana->samples_to_acq = log_ana->post_trig;       // Set number of post trig samples
        log_ana->run_f = TRUE;                  // Start logging
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtSaveValue(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This function stores set command value characters in the event log record for the command.  It can only
  go as far as the end of the current packet.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U              out_idx;
    INT16U              n_chars;
    char        *       buf;
    struct pars_buf  *  pars_buf;


    pars_buf = c->pars_buf;
    out_idx  = pars_buf->out_idx;
    n_chars  = pars_buf->pkt[c->pkt_buf_idx].n_chars;
    buf      = &pars_buf->pkt[c->pkt_buf_idx].buf[out_idx];

    while (c->evt_log_state != EVT_LOG_VALUE_FULL && out_idx++ < n_chars)
    {
        LogEvtSaveCh(c, *(buf++));
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtSaveCh(struct cmd * c, char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function stores characters from the command buffer in the event log record for the command.
  Characters are either a property name or a property value.  The function uses a state machine to know
  which is currently being accummulated in the event record for the command.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct log_evt_rec * evt_rec  = &c->evt_rec;

    switch (c->evt_log_state)
    {
        case EVT_LOG_RESET_PROP:

            c->evt_log_idx    = 0;
            c->evt_log_state  = EVT_LOG_PROP;
            evt_rec->value[0] = '\0';

            // Fall through !!!!
            // no break here !!!!
        case EVT_LOG_PROP:

            if (ch && ch != ' ')
            {
                evt_rec->prop[  c->evt_log_idx] = ch;
                evt_rec->prop[++c->evt_log_idx] = '\0';

                if (c->evt_log_idx >= (FGC_LOG_EVT_PROP_LEN - 1))
                {
                    c->evt_log_state = EVT_LOG_PROP_FULL;
                }
            }

            break;

        case EVT_LOG_RESET_VALUE:

            c->evt_log_idx   = 0;
            c->evt_log_state = EVT_LOG_VALUE;

            // Fall through !!!!
            // no break here !!!!

        case EVT_LOG_VALUE:

            if (!ch || ch == ';')
            {
                c->evt_log_state = EVT_LOG_VALUE_FULL;
            }
            else if (ch != ' ')
            {
                evt_rec->value[c->evt_log_idx++] = ch;
                evt_rec->value[c->evt_log_idx] = '\0';

                if (c->evt_log_idx >= (FGC_LOG_EVT_VAL_LEN - 1))
                {
                    c->evt_log_state = EVT_LOG_VALUE_FULL;
                }
            }

            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtCmd(struct cmd * c, INT16U errnum)
/*---------------------------------------------------------------------------------------------------------*\
  This function completes the event record for a set command by writing the action and copying the
  record into the event buffer atomically by suspending interrupts.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR                   cpu_sr;
    struct log_evt_rec     *    evt_rec  = &c->evt_rec;

    // Prepare command error number in the action field

    sprintf(evt_rec->action, (c == &tcm ? "TRM.%03u" : "NET.%03u"), errnum);

    // Write event record to event log atomically

    OS_ENTER_CRITICAL();

    log_evt.index++;

    if (log_evt.index >= FGC_LOG_EVT_LEN)
    {
        log_evt.index = 0;
    }

    OS_EXIT_CRITICAL();

    memcpy((char *)SRAM_LOG_EVT_32 + log_evt.sample_size * log_evt.index, evt_rec, sizeof(struct log_evt_rec));

    shared_mem.logEvent_index = log_evt.index;

}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtProp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the property value for changes and records entries in the log if they are detected.
  For bit mask properties, every bit change uses one entry in the log. For numeric values, the value will
  always be written in hex to log for speed reasons.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U              new_value = 0;      // New property value
    INT16U              old_value;          // Old property value
    INT16U              chg_mask;           // Change mask
    INT16U              bit_idx;
    INT16U              set_f;
    struct prop      *      p;              // Pointer to property
    struct log_evt_prop    *    lp;             // Pointer to entry in event log property table
    static char         hexvalue[ST_MAX_SYM_LEN + 1] = { '0', 'x' };

    for (lp = log_evt_prop; (p = lp->prop) ; lp++)
    {
        /*--- Check if property value has changed ---*/

        switch (p->type)
        {
            case PT_INT8U:
                new_value = *((INT8U *)p->value);
                break;

            case PT_INT16U:
                new_value = *((INT16U *)p->value);
                break;

            case PT_INT32U:
                new_value = *((INT32U *)p->value);  // Take low word only
                break;
        }

        // This is a hack
        // The VSNOCABLE input for sending external commands when working with ComHV-PS converter
        // The input changes very fast, so the event log is overflowing with its value change messages
        // We always set the property bit corresponding to the VSNOCABLE input to 0 - this will supress the messages

        if (   crateGetType() == FGC_CRATE_TYPE_COMHV_PS
            || crateGetType() == FGC_CRATE_TYPE_RF25KV)
        {
            if (p == &PROP_DIG_STATUS)
            {
                new_value = new_value & ~FGC_DIG_STAT_VS_NO_CABLE;
            }

            if (p == &PROP_DIG_IP_DIRECT)
            {
                new_value = new_value & ~FGC_DIG_IPDIRECT_VSNOCABLE;
            }
        }

        old_value = lp->old_value;

        if (new_value == old_value)     // if value has not changed
        {
            continue;                       // continue to next property
        }

        /*--- Prepare log entry ---*/

        lp->old_value = new_value;          // Remember new property value


        PubProperty(p, NON_PPM_USER, FALSE);   // Publish change

        if (p->flags & PF_SYM_LST)      // If property has a symbol list
        {
            if (p->flags & PF_BIT_MASK)     // If property is a bit mask
            {
                chg_mask = new_value ^ old_value;   // Calculate change mask

                while (chg_mask)            // While bits remain in the change mask
                {
                    MCU_FF1(chg_mask, bit_idx);         // Find first bit that is set. Answer is stroed in bit_idx

                    set_f = masks[bit_idx] & new_value;

                    chg_mask &= ~masks[bit_idx];
                    LogEvtPropStore(lp->name, lp->symbols[bit_idx], set_f, TRUE);
                }
            }
            else                // else property is NOT a bit mask
            {
                LogEvtPropStore(lp->name, lp->symbols[new_value], TRUE, FALSE);
            }
        }
        else                // else numeric property
        {
            // the first 2 chars "0x" are already in hexvalue
            sprintf(hexvalue + 2, "%04X", new_value);     // Convert 16-bit integer value into HEX ASCII string
            LogEvtPropStore(lp->name, (char *) &hexvalue, TRUE, FALSE); // Log new hex value
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtPropStore(const char * far_prop_name, const char * far_sym, INT16U set_f, INT16U bitmask_f)
/*---------------------------------------------------------------------------------------------------------*\
  This function writes a property log entry in the event log.  It is called from LogEvtProp() which takes
  responsibility for the ENTER/EXIT SRAM so this function doesn't need to touch the MODEL register.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct log_evt_rec     *    evt_rec;


    log_evt.index++;

    if (log_evt.index >= FGC_LOG_EVT_LEN)               // No interrupt protection required as
    {
        // called from StaTsk() which is highest priority
        log_evt.index = 0;
    }

    evt_rec = (struct log_evt_rec *)(SRAM_LOG_EVT_32 + (INT32U)log_evt.sample_size * log_evt.index);

    evt_rec->timestamp = sta.timestamp_us;

    // Write property name (24 bytes fixed length),
    strncpy(evt_rec->prop, far_prop_name, FGC_LOG_EVT_PROP_LEN);        // (to,from,len)

    // Write symbol (14 bytes fixed length)
    strncpy(evt_rec->value, far_sym, 14); // FGC_LOG_EVT_VAL_LEN

    // Write action (SET or CLR)
    if (set_f == FALSE)
    {
        if (bitmask_f != FALSE)
        {
            strcpy(evt_rec->action, "CLR_BIT");
        }
        else
        {
            strcpy(evt_rec->action, "CLR");
        }
    }
    else
    {
        if (bitmask_f != FALSE)
        {
            strcpy(evt_rec->action, "SET_BIT");
        }
        else
        {
            strcpy(evt_rec->action, "SET");
        }
    }

    shared_mem.logEvent_index = log_evt.index;

}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtDim(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at 200Hz from StaTsk() to log the triggered DIM digital inputs.
\*---------------------------------------------------------------------------------------------------------*/
{
    fgc_db_t                        dim_type_idx;
    fgc_db_t                        dim_props_idx;          // 0 - (n_dims-1)
    fgc_db_t                        board_number;
    BOOLEAN                         input_zero_f;
    BOOLEAN                         input_zero_old_f;
    BOOLEAN                         fault_input_f;
    BOOLEAN                         end_f;      // Log entry made so end this iteration flag

    dim_props_idx = log_evt.dim_props_idx;

    do
    {
        if (log_evt.dim_input_mask)     // If logging a DIM
        {
            do
            {
                dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,log_evt.logical_dim);
                if (DimDbIsDigitalInput(dim_type_idx, (log_evt.dim_bank_idx+4), log_evt.dim_input_idx)) // If channel is in use
                {

                    // fault_input_f:    Is the data bit indicating a fault?
                    // input_zero_f:     Is the new data bit zero?
                    // input_zero_old_f: Is the old data bit zero?

                    fault_input_f     =  Test(log_evt.dim_input_mask, log_evt.fault_mask  [log_evt.dim_bank_idx]);
                    input_zero_f      = !Test(log_evt.dim_input_mask, log_evt.dim_new_data[log_evt.dim_bank_idx]);
                    input_zero_old_f  = !Test(log_evt.dim_input_mask,
                                              log_evt.dim_old_data[log_evt.dim_bank_idx][log_evt.logical_dim]);

                    // In TRIG  state: Log all non zero values, and faults, if they are set
                    // In WATCH state: Log any fault that has just appeared

                    if ((log_evt.dim_state == DIM_EVT_TRIG  && (!fault_input_f || !input_zero_f)) ||
                        (log_evt.dim_state == DIM_EVT_WATCH && fault_input_f && !input_zero_f && input_zero_old_f))
                    {
                        LogEvtDimInput(input_zero_f, fault_input_f,true);
                        end_f = TRUE;
                    }
                }

                log_evt.dim_input_mask <<= 1;

                log_evt.dim_input_idx++;

                if (log_evt.dim_input_idx >= FGC_N_DIM_DIG_INPUTS)
                {
                    log_evt.dim_bank_idx++;

                    if (log_evt.dim_bank_idx < FGC_N_DIM_DIG_BANKS)
                    {
                        log_evt.dim_input_idx  = 0;
                        log_evt.dim_input_mask = 1;
                    }
                    else
                    {
                        log_evt.dim_input_mask = 0;

                        if (log_evt.dim_state == DIM_EVT_TRIG)
                        {
                            dim_collection.evt_log_state[log_evt.logical_dim] = DIM_EVT_TRIGGED;
                        }
                        else
                        {
                            log_evt.dim_old_data[0][log_evt.logical_dim] = log_evt.dim_new_data[0];
                            log_evt.dim_old_data[1][log_evt.logical_dim] = log_evt.dim_new_data[1];
                        }
                    }
                }

                if (end_f)
                {
                    log_evt.dim_props_idx = dim_props_idx;
                    return;
                }
            }
            while (log_evt.dim_input_mask);
        }

        // Check for new DIMs to log only at the start of each 20ms period

        if (((INT16U)sta.timestamp_ms.ms_time % 20) != DEV_STA_TSK_PHASE)
        {
            return;
        }

        // Analyse next DIM

        dim_props_idx++;

        if (dim_props_idx >= diag.n_dims)
        {
            dim_props_idx = 0;
        }

        log_evt.logical_dim = ((struct TAccessToDimInfo *) diag.dim_props[dim_props_idx].value)->logical_dim;

        board_number = SysDbDimBusAddress(dev.sys.sys_idx, log_evt.logical_dim);

        log_evt.dim_state = dim_collection.evt_log_state[log_evt.logical_dim];

        // Following a TRIG state and until dim_state is changed by the DSP
        if (log_evt.dim_state == DIM_EVT_TRIGGED)
        {
            continue;
        }

        if (log_evt.dim_state == DIM_EVT_NONE)
        {
            if (board_number < FGC_MAX_DIM_BUS_ADDR)
            {
                diag.data.r.latched_0[BRANCH_A][board_number] = 0;
                diag.data.r.latched_1[BRANCH_A][board_number] = 0;
            }

            continue;
        }

        dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,log_evt.logical_dim);
        log_evt.fault_mask   [0] = DimDbDigitalFaults(dim_type_idx,4);
        log_evt.fault_mask   [1] = DimDbDigitalFaults(dim_type_idx,5);

        log_evt.dim_new_data [0] = diag.data.r.digital_0[BRANCH_A][board_number];
        log_evt.dim_new_data [1] = diag.data.r.digital_1[BRANCH_A][board_number];

        if (log_evt.dim_state == DIM_EVT_WATCH &&       // if DIM_EVT_WATCH state
            !((log_evt.dim_new_data[0] ^ log_evt.dim_old_data[0][log_evt.logical_dim]) & log_evt.fault_mask[0]) &&
            !((log_evt.dim_new_data[1] ^ log_evt.dim_old_data[1][log_evt.logical_dim]) & log_evt.fault_mask[1]))
        {
            continue;                           // Nothing changed so continue
        }

        log_evt.dim_input_mask = 1;     // Set mask to process DIM
        log_evt.dim_input_idx  = 0;     // Start with first input of bank 0
        log_evt.dim_bank_idx   = 0;

        memcpy(&log_evt.dim_log_rec.prop[4], SYM_TAB_PROP[diag.dim_props[dim_props_idx].sym_idx].key.c,
               ST_MAX_SYM_LEN + 1);

        if (log_evt.dim_state == DIM_EVT_TRIG)  // if DIM_EVT_TRIG state
        {

            diag.data.r.latched_0[BRANCH_A][board_number] = log_evt.dim_old_data[0][log_evt.logical_dim] =
                                                                log_evt.dim_new_data[0];
            diag.data.r.latched_1[BRANCH_A][board_number] = log_evt.dim_old_data[1][log_evt.logical_dim] =
                                                                log_evt.dim_new_data[1];

            log_evt.dim_log_rec.timestamp.unix_time = dim_collection.trig_s[log_evt.logical_dim];
            log_evt.dim_log_rec.timestamp.us_time   = dim_collection.trig_us[log_evt.logical_dim];
            LogEvtDimInput(0, 0, false);
        }
        else                        // else DIM_EVT_WATCH state
        {
            log_evt.dim_log_rec.timestamp = sta.timestamp_us;
        }
    }
    while (log_evt.dim_input_mask || dim_props_idx != log_evt.dim_props_idx);   // until all DIMS checked
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtDimInput(BOOLEAN input_zero_f, BOOLEAN fault_input_f, BOOLEAN dimdb_lbl_is_null)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called LogEvtDim() to record the state of a DIM digital input in the event log.
\*---------------------------------------------------------------------------------------------------------*/
{
    fgc_db_t dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,log_evt.logical_dim);
    if (!dimdb_lbl_is_null)              // If pointer to label is NULL
    {
        log_evt.dim_log_rec.value[0] = 'T';     // Report DIM trigger time always
        log_evt.dim_log_rec.value[1] = 'R';
        log_evt.dim_log_rec.value[2] = 'I';
        log_evt.dim_log_rec.value[3] = 'G';
        log_evt.dim_log_rec.value[4] = 'G';
        log_evt.dim_log_rec.value[5] = 'E';
        log_evt.dim_log_rec.value[6] = 'R';
        log_evt.dim_log_rec.value[7] = '\0';

        log_evt.dim_log_rec.action[0] = 'S';
        log_evt.dim_log_rec.action[1] = 'E';
        log_evt.dim_log_rec.action[2] = 'T';
        log_evt.dim_log_rec.action[3] = '\0';
    }
    else                    // else use label
    {
        if (fault_input_f)
        {
            log_evt.dim_log_rec.value[0] = 'F';
            log_evt.dim_log_rec.value[1] = 'L';
            log_evt.dim_log_rec.value[2] = 'T';
            log_evt.dim_log_rec.value[3] = '|';
        }
        else
        {
            log_evt.dim_log_rec.value[0] = 'S';
            log_evt.dim_log_rec.value[1] = 'T';
            log_evt.dim_log_rec.value[2] = 'A';
            log_evt.dim_log_rec.value[3] = '|';
        }

        memcpy(&log_evt.dim_log_rec.value[4], DimDbDigitalLabel(dim_type_idx, (log_evt.dim_bank_idx+4), log_evt.dim_input_idx),
               FGC_LOG_EVT_VAL_LEN - 4);

        memcpy(&log_evt.dim_log_rec.action,
                (input_zero_f ?  
                    DimDbDigitalLabelZero(dim_type_idx, (log_evt.dim_bank_idx+4), log_evt.dim_input_idx) : 
                    DimDbDigitalLabelOne(dim_type_idx, (log_evt.dim_bank_idx+4), log_evt.dim_input_idx)) ,
                FGC_LOG_EVT_ACT_LEN);
    }

    log_evt.index++;

    if (log_evt.index >= FGC_LOG_EVT_LEN)       // No interrupt protection required as
    {
        // called from StaTsk() which is highest priority
        log_evt.index = 0;
    }

    memcpy((char *) SRAM_LOG_EVT_32 + log_evt.sample_size * log_evt.index, &log_evt.dim_log_rec,
           sizeof(struct log_evt_rec));

    shared_mem.logEvent_index = log_evt.index;
}
/*---------------------------------------------------------------------------------------------------------*/
void * LogNextAnaSample(struct log_ana_vars * log_ana)
/*---------------------------------------------------------------------------------------------------------*\
  This function will advance the sample index for the analogue log and return the address of the next
  record to be written.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (++log_ana->last_sample_idx >= log_ana->n_samples)
    {
        log_ana->last_sample_idx = 0;
    }

    return ((void *)(log_ana->baseaddr + (INT32U)log_ana->last_sample_idx * log_ana->sample_size));
}
/*---------------------------------------------------------------------------------------------------------*/
void LogThour(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will log the FGC and DCCT temperatures in the THOUR log. It is called from MstTsk() and
  records one sample per 10 s.  Note that the THOUR log is in page 1 so ENTER/EXIT_SR is NOT required.
\*---------------------------------------------------------------------------------------------------------*/
{
    memcpy(LogNextAnaSample(&log_thour), & (temp.log), sizeof(struct temperatures_to_logbook));
    shared_mem.temperatureLogHour_index = log_thour.last_sample_idx;                 // Save new index in SM area
    shared_mem.temperatureLogHour_timestamp = log_thour.last_sample_time.unix_time = temp.unix_time;
}
/*---------------------------------------------------------------------------------------------------------*/
void LogTday(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will log the FGC and DCCT temperatures in the TDAY log.  It is called from MstTsk() and
  records one sample per 10 min.  Note that the TDAY log is in page 1 so ENTER/EXIT_SR is NOT required.
\*---------------------------------------------------------------------------------------------------------*/
{
    memcpy(LogNextAnaSample(&log_tday), & (temp.log), sizeof(struct temperatures_to_logbook));
    shared_mem.temperatureLogDay_index = log_tday.last_sample_idx;                   // Save new index in SM area
    shared_mem.temperatureLogDay_timestamp = log_tday.last_sample_time.unix_time =
                                                 temp.unix_time;   // Save time in SM area
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtTiming(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at 200Hz from StaTsk() to check if the timing log needs to be written to the
  event log.  This will be done whenever the system leaves the CYCLING state.  It will write a maximum of
  one entry to the event log per call.

  Log record:

    Unix time   Start of cycle time
    us_time     Start of cycle time
    prop[24]    "USER=NN LENGTH=LL SSC=Y/N"
    value[36]   "REG_MODE=x FUNC_TYPE=xxxxxxxxxxxxx"
    action[8]   "START"
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  idx;
    INT16U  s;
    INT8S   user;
    INT8S   ssc_f;
    char  * log_evt_addr;

    // Log cycle time stamps from circular buffer

    idx = timing_log.out_idx++;

    user = timing_log.user[idx];

    if (user)                              // If log record is complete
    {
        if (user < 0)                      // If length_bp is negative
        {
            ssc_f = 'Y';                        // It is a flag for start-supercycle
            user  = -user;
        }
        else
        {
            ssc_f = 'N';
        }      

        timing_log.log_rec.timestamp = timing_log.time[idx];
        timing_log.log_rec.prop[5]   = '0' + (user / 10);
        timing_log.log_rec.prop[6]   = '0' + (user % 10);
        timing_log.log_rec.prop[12]  = ssc_f;
        timing_log.log_rec.value[9]  = timing_log.reg_mode_flag[idx];

        strcpy(&timing_log.log_rec.value[21], SYM_TAB_CONST[timing_log.stc_func_type[idx]].key.c);

        log_evt.index++;

        if (log_evt.index >= FGC_LOG_EVT_LEN)
        {
            log_evt.index = 0;
        }

        s = sizeof(struct log_evt_rec) * log_evt.index; // Use intermediate var to avoid compiler bug
        log_evt_addr = (char *) SRAM_LOG_EVT_32 + s;

        memcpy(log_evt_addr, &timing_log.log_rec, sizeof(struct log_evt_rec));

        shared_mem.logEvent_index = log_evt.index;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogIearth(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function stores one sample in the IEARTH log.
  The Iearth signal comes from an analogue DIM channel. The function is
  called at the end of millisecond 5 of 20 from the MstClass() function.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 i_earth;
    FP32 i_earth_pcnt;
    INT8S indx;

    // Are there 0, 1 or 4 channels?

    i_earth = 0.0;

    for (indx = 0; indx < LOG_I_EARTH_SIGS; ++indx)
    {
        i_earth = (meas_i_earth.channel[indx] != -1 ?
                   ((FP32 *)&dim_collection.analogue_in_physical)[meas_i_earth.channel[indx]] :
                   0.0);

        if (meas_i_earth.max < fabs(i_earth))
        {
            meas_i_earth.max = fabs(i_earth);
        }

        memcpy(LogNextAnaSample(&log_iearth), &(i_earth), sizeof(FP32));

        // If logging is not active, decrement samples remaining counter

        if (log_iearth.run_f == 0)
        {
            log_iearth.samples_to_acq--;
        }
    }

    // Time adjusted to start of 10 ms

    log_iearth.last_sample_time = mst.time10ms;

    // Use the last I earth value for the property's value

    meas_i_earth.value = i_earth;

    // i_earth percent

    i_earth_pcnt = 100.0 * i_earth / meas_i_earth.limit;

    // Clip integer value to -128%/+127% (8 bits)

    if (i_earth_pcnt > 127.0)
    {
        meas_i_earth.pcnt = 127;
    }
    else if (i_earth_pcnt < -127.0)
    {
        meas_i_earth.pcnt = -128;
    }
    else
    {
        meas_i_earth.pcnt = (INT32S)i_earth_pcnt;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: log.c
\*---------------------------------------------------------------------------------------------------------*/
