/*---------------------------------------------------------------------------------------------------------*\
  File:         prop.c

  Purpose:      FGC MCU Software - Property Functions.

  Author:       Quentin.King@cern.ch

  Notes:        This file contains the functions to manipulate the properties and symbols
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#define PROP_GLOBALS

#include <prop.h>
#include <string.h>
#include <class.h>          // Include class dependent definitions
#include <dev.h>            // for dev global variable
#include <fbs.h>            // for FGC_FIELDBUS_MAX_CMD_LEN, FGC_FIELDBUS_FLAGS_SUB_CMD, FGC_FIELDBUS_FLAGS_SET_CMD
#include <fgc_errs.h>       // for FGC_BAD_ARRAY_IDX
#include <log.h>            // for LogEvtSaveCh()
#include <macros.h>         // for Test(), TestAll(), Set(), DEVICE_PPM, DEVICE_CYC
#include <mcu_dsp_common.h>
#include <mcu_panic.h>
#include <mst.h>            // for mst global variable
#include <nvs.h>
#include <os.h>             // for OSTskSuspend()
#include <pars.h>           // for ParsScanToken(), ParsScanInteger()
#include <runlog_lib.h>
#include <start.h>          // for Crash()
#include <time_fgc.h>
#include <trm.h>

#include <dpcls.h>

static void PropMapRecurse(INT16U lvl, struct prop * p, prop_map_func func,
                           enum prop_map_specifier specifier);

/*---------------------------------------------------------------------------------------------------------*/
INT16U PropFind(const char * propname)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to find a property from its name.  It uses the pcm command structure in a
  special way and is separate from the tcm and fcm structures so that it can be used by the Dallas task
  in parallel with FIP and serial command processing.  It is also used during startup to to find non-volatile
  storage properties.

  Input parameters:

        INT8S *propname         Pointer to string containing the name of the property name

  Output parameters (if successful):

        pcm.prop                Pointer to identified property
        pcm.n_arr_spec          Number of array specifiers (0,1,2)
        pcm.from                Array start index
        pcm.to                  Array end index specifier
        pcm.sym_idxs            Array of symbol indexes for parent properties leading to identified property
        pcm.si_idx              Number of entries in c->sym_idxs
        pcm.n_prop_name_lvls    Number of entries in c->sym_idxs (this is number of levels in property name)

  Return value:

        0                       Successful match
        FGC_NO_DELIMITER        End of buffer found before a valid delimiter
        FGC_SYNTAX_ERROR        Token buffer full or contains a space before a valid delimiter
        FGC_NO_SYMBOL           No symbol found
        FGC_UNKNOWN_SYM         Token didn't match any known property symbol
        FGC_BAD_ARRAY_IDX       The array specifier has a bad syntax or values
\*---------------------------------------------------------------------------------------------------------*/
{
    pcm.end_cmd_f = FALSE;

    // BE CAREFUL: Here pcm.prop_buf is used as a char pointer (to the property name) so that the parsing
    // functions can be used thanks to pcm's cmd structure.

    pcm.prop_buf  = (struct prop_buf *)propname;        // pcm.prop_buf is later used as a char* in CmdNextCh

    return (PropIdentifyProperty(&pcm));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PropIdentifyProperty(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to identify a property from it's symbolic name, read from the command buffer.

  Input parameters:

        struct cmd *c           Pointer to command structure (fcm or tcm).

  Output parameters (if successful):

        c->prop                 Pointer to identified property
        c->mux_idx              Multiplexor index (set to user idx or sub_dev_idx)
        c->n_arr_spec           Number of array specifiers (0,1,2)
        c->from                 Array start index
        c->to                   Array end index specifier
        c->step                 Array step specifier
        c->sym_idxs             Array of symbol indexes for parent properties leading to identified property
        c->si_idx               Number of entries in c->sym_idxs
        c->n_prop_name_lvls     Number of entries in c->sym_idxs (this is number of levels in property name)
        c->header               Command header
        c->cmd_type             Command type from low nibble of command header

  Return value:

        0                       Successful match
        FGC_NO_DELIMITER        End of buffer found before a valid delimiter
        FGC_SYNTAX_ERROR        Token buffer full or contains a space before a valid delimiter
        FGC_NO_SYMBOL           No symbol found
        FGC_INVALID_SUBDEV      Sub-device number out of valid range
        FGC_UNKNOWN_SYM         Token didn't match any known property symbol
        FGC_BAD_ARRAY_IDX       The array specifier has a bad syntax or values
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U              errnum;         // Status return from ST_MAX_SYM_LEN() or PropIdentifyArray()
    INT16U              sym_idx;        // Symbol index returned from ST_MAX_SYM_LEN()
    prop_size_t         n_els;          // Number of property elements
    INT32U              sub_dev_idx;    // Sub-device index
    struct prop    *    p;              // Pointer to property
    struct prop    *    sub_p;          // Pointer to sub property

    // Process sub-device index if present

    errnum = ParsScanToken(c, "Y.:([ ");

    if (errnum != 0)                                            // If token is bad
    {
        return (errnum);                                        // Report status
    }

    if (c->token_delim == ':')                                  // If sub_dev index supplied
    {
        errnum = ParsScanInteger(c, 0, &sub_dev_idx);           // Parse index

        if (errnum != 0)
        {
            return (errnum);
        }

        if (sub_dev_idx == 0 || sub_dev_idx > FGC_MAX_SUB_DEVS)      // Check limits
        {
            return (FGC_INVALID_SUBDEV);
        }

        errnum = ParsScanToken(c, "Y.([ ");             // If next token is bad

        if (errnum != 0)                                // If next token is bad
        {
            return (errnum);                            // Report status
        }
    }
    else
    {
        sub_dev_idx = 0;                                // No sub-dev index supplied
    }

    // Process property name

    errnum = PropIdentifySymbol(c, 0, ST_TLPROP, &sym_idx); // Try to identify top level property

    if (errnum != 0)
    {
        return (errnum);                                // Report failure if search failed
    }

    p = (struct prop *) SYM_TAB_PROP[sym_idx].value;    // Get pointer to top level property

    c->sym_idxs[0] = sym_idx;                           // Store symbol for top level property
    c->si_idx = 1;

    while ((p->type == PT_PARENT)                       // while property contains further properties and
           && (c->token_delim == '.'))                 // delimiter is .
    {
        errnum = PropIdentifySymbol(c, "Y.([ ", ST_LLPROP, &sym_idx); // Try to identify low level prop sym

        if (errnum != 0)
        {
            return (errnum);                    // Report failure if search failed
        }

        sub_p = p->value;                       // Get pointer to sub property array
        n_els = PropGetNumEls(c, p);            // and number of sub properties

        if (n_els)                              // If there are children
        {
            while (sym_idx != sub_p->sym_idx && // While symbol doesn't match sub property
                   --n_els)                    // and not all sub properties have been checked
            {
                sub_p++;                                // Try next sub property
            }
        }

        if (!n_els)                                     // If no match was found
        {
            return (FGC_UNKNOWN_SYM);                   // Report UNKNOWN SYMBOL
        }

        c->sym_idxs[c->si_idx++] = sym_idx;             // Store sub property symbol
        p = sub_p;                                      // Move down to sub property
    }

    if (c->token_delim == '.')                          // If delimiter was still '.'
    {
        return (FGC_UNKNOWN_SYM);                       // Report UNKNOWN SYMBOL
    }
    
    c->prop = p;                        // Save property address in command structure    
    c->n_prop_name_lvls = c->si_idx;    // Remember number of levels in property name

    memset(&c->sym_idxs[c->si_idx], 0, FGC_MAX_PROP_DEPTH - c->si_idx); // Clear rest of symbols stack

    if (!c->pars_buf)                                   // If pcm (used for PropFind())
    {
        return (0);                                     // Return immediately
    }

    // Continue for Serial or FIP commands

    c->header = c->pars_buf->pkt[c->pkt_buf_idx].header_flags;
    c->cmd_type = c->header & FGC_FIELDBUS_FLAGS_CMD_TYPE_MASK;

    // Set the last property if it as leaf so that the correct label can be appended in cmd.c::CmdPrintLabel()

    c->last_prop = (p->type != PT_PARENT ? p  : NULL); 

    // Check sub-device compatibility

    if (Test(p->flags, PF_SUB_DEV))             // If property is a sub-device property
    {
        if (!sub_dev_idx)                       // If sub_dev index not given
        {
            return (FGC_SUBDEV_ONLY);           // Report error
        }

        c->mux_idx = sub_dev_idx;               // Use sub_dev index as mux_idx
    }
    else                                        // else property is a device property
    {
        if (sub_dev_idx)                        // If sub_dev index given
        {
            return (FGC_INVALID_SUBDEV);        // Report error
        }
    }

    // Process user index and array indexes if present

    errnum = PropIdentifyUser(c, c->pars_buf->pkt[c->pkt_buf_idx].header_user);

    if (errnum != 0)
    {
        return (errnum);
    }

    return (PropIdentifyArray(c));              // Analyse array specifier if supplied
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PropIdentifySymbol(struct cmd * c, const char * delim, INT16U sym_type, INT16U * sym)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to identify the symbol in the command token buffer.  A symbol can either be a
  constant, a top level property name, a low level property name, or a get option.

  Input parameters:

        struct cmd *c           Pointer to command structure containing token to identify.
        INT8S *delim            If not NULL then this will be used as the delimiter string
        INT16U sym_type         Symbol type selection: ST_CONST, ST_GETOPT, ST_TLPROP OR ST_LLPROP.

  Output parameters:

        INT16U *sym             *sym will be set to the index of the symbol if a match is made.

  Return value:

        0                       Successful match - symbol index in *sym
        FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
        FGC_NO_DELIMITER        End of buffer found before a valid delimiter
        FGC_SYNTAX_ERROR        Token buffer full or space found before a valid delimiter
        FGC_NO_SYMBOL           Token buffer empty
        FGC_UNKNOWN_SYM         Token didn't match any symbol with given type
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U errnum;               // Return status
    INT16U n_ch;                 // Number of chars in the token
    INT16U sym_idx = 0;          // Symbol index (zero means 'symbol not found')

    // Warning!
    // in short circuit evaluation if delim == 0 then ParsScanToken() is not evaluated
    // but if complete evaluation compiler switch is used then ParsScanToken() wiil be evaluated always
    if (delim && (errnum = ParsScanToken(c, delim))) // If delimiter supplied and token is bad
    {
        return (errnum);                        // Report status
    }

    n_ch = c->n_token_chars;

    if (!n_ch)                                  // If token buffer is empty
    {
        return (FGC_NO_SYMBOL);                 // Report NO_SYMBOL
    }

    if (n_ch > ST_MAX_SYM_LEN)                  // If token is too long
    {
        return (FGC_UNKNOWN_SYM);               // Report UNKNOWN SYM
    }

    switch (sym_type)
    {
        case ST_TLPROP:

            sym_idx = HashFindProp(c->token);

            if (sym_idx && !SYM_TAB_PROP[sym_idx].value)        // If symbol is a low level property
            {
                sym_idx = 0;                                    // Clear symbol index to indicate not found
            }

            break;

        case ST_LLPROP:

            sym_idx = HashFindProp(c->token);
            break;

        case ST_CONST:

            sym_idx = HashFindConst(c->token);
            break;

        case ST_GETOPT:

            sym_idx = HashFindGetopt(c->token);
            break;
    }

    if (!sym_idx)                               // If no match made...
    {
        return (FGC_UNKNOWN_SYM);               // Report unknown symbol
    }

    *sym = sym_idx;                             // Pass symbol index to calling function

    return (0);                                 // Return OK status
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PropIdentifyUser(struct cmd * c, INT16U user_from_header)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to identify the ASCII user index specifier in the command if it is present.  This
  has the syntax "(USER)".  The gateway will supply a user from CMW via the user byte in the packet header
  and if this is non-zero, it will always take priority over the ASCII user.

  If the ASCII user specifier is empty "()" and the property is NOT a parent then the PPM_ALL_USERS
  value will be returned forcing a get of all users for that property.

  A non-zero user is only acceptable for this command/property if a complex condition is met:

      dpcom.mcu.device_ppm (DEVICE.PPM property) is ENABLED and
            SUB command or
            SET command and PPM property, or
            GET or GET_SUB command and (PARENT or PPM property)

  If the condition is not met then the user (in c->mux_idx) is forced to zero.

  Input parameters:

        struct cmd *c           Pointer to command structure.
        c->mux_idx              sub_dev index
        user_from_header        user from command packet header (take priority over user inside command)

  Output parameters:

        c->mux_idx              User index (sub_dev index must be zero)

  Return value:

        0                       Successful identification of user index or no index supplied
        FGC_BAD_CYCLE_SELECTOR  The cycle index specified has bad syntax or is not allowed
        FGC_NOT_IMPL            A sub_dev index has been supplied - sub_dev properties cannot be PPM
        FGC_SYNTAX_ERROR        Invalid character after user index
\*---------------------------------------------------------------------------------------------------------*/
{
    char ch;                            // first character after array specifier
    INT32U value = 0;                   // Long integer value for ScanInteger
    struct prop * p = c->prop;          // Local copy of pointer property

    if (c->token_delim == '(')          // If ASCII user index specifier is included
    {
        if (c->mux_idx)                 // If sub device index already supplied
        {
            return (FGC_NOT_IMPL);      // report not implemented
        }

        switch (ParsScanInteger(c, "N)", &value))               // scan (delim = ')') for an integer value
        {
            case FGC_NO_SYMBOL:                                 // No value

                if ((c->cmd_type == FGC_FIELDBUS_FLAGS_GET_CMD) // if command is GET and
                    && (p->type != PT_PARENT)               // not a parent and
                    && Test(p->flags, PF_PPM))              // PPM property
                {
                    if (   DEVICE_PPM == FGC_CTRL_ENABLED
                        || p->set_func_idx == SET_NONE)
                    {
                        value = PPM_ALL_USERS;                      // Signal get all users
                    }                    
                }

                break;

            case 0:                                     // Valid integer

                break;                                  // Use value

            default:                                    // Invalid integer

                return (FGC_BAD_CYCLE_SELECTOR);                  // Report BAD USER
        }

        CmdNextCh(c, &ch);                              // Take next char from cmd pkt
        LogEvtSaveCh(c, ch);                            // Log character

        if (ch && ch != ' ' && ch != '[')               // If not zero, space or [
        {
            return (FGC_SYNTAX_ERROR);                  // Report SYNTAX_ERROR
        }

        c->token_delim = ch;
    }

    // If user supplied in command packet header (for SUB_GET), then use it unless sub_dev index supplied

    if (user_from_header)
    {
        if (c->mux_idx && c->mux_idx != user_from_header) // The user ID can be redundant but the user in the packet
            // header should match the user in the cmd.
        {
            return (FGC_BAD_CYCLE_SELECTOR);
        }

        value = user_from_header;
    }

    // Check that value is within valid range

    if (   (DEVICE_PPM == FGC_CTRL_ENABLED  && value > FGC_MAX_USER && value != PPM_ALL_USERS)
        || (DEVICE_PPM == FGC_CTRL_DISABLED && value > 0            && p->set_func_idx != SET_NONE))
    {
        return (FGC_BAD_CYCLE_SELECTOR);
    }

    // Condition to decide if a non-zero User can be accepted for this command/property

    if ((DEVICE_PPM == FGC_CTRL_ENABLED  || p->set_func_idx == SET_NONE)
        && ((c->cmd_type == FGC_FIELDBUS_FLAGS_SUB_CMD)
            || (c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD && Test(p->flags, PF_PPM))
            || ((c->cmd_type == FGC_FIELDBUS_FLAGS_GET_CMD || c->cmd_type == FGC_FIELDBUS_FLAGS_GET_SUB_CMD)
                && (p->type == PT_PARENT || Test(p->flags, PF_PPM))
               )
           ))
    {
        c->mux_idx = (INT16U)value;
    }

    return (0);                                            // Report success
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PropIdentifyArray(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to identify the array range and step specifiers.

  Input parameters:

        struct cmd *c           Pointer to command structure.

  Output parameters:

        c->n_arr_spec           Number of array specifiers (0,1,2)
        c->from                 Array start index
        c->to                   Array end index specifier
        c->step                 Array step specifier (for Set, step must be 1)

  Return value:

        0                       Successful identification of array range
        FGC_BAD_ARRAY_IDX       The array specified has bad syntax or it's a parent property and a set cmd.

  Notes:

        The following array specifier syntaxes are allowed:

            Cmd Buf             from            to              n_arr_spec
            no specifier        0               max_els-1       0
                [x]             x               x               2
                [x,]            x               max_els-1       1
                [x,x]           x               x               2
                [x,y]           x               y               2     (x != y)

        In all cases x and y must be positive.
\*---------------------------------------------------------------------------------------------------------*/
{
    char ch;                            // first character after array specifier
    INT32U value;                       // Unsigned long value (from ScanInteger)
    struct prop * p = c->prop;          // Local copy of pointer property

    // Set default values

    c->n_arr_spec = 0;                  // Report no specifiers
    c->from = 0;                        // Set default array range
    c->to = p->max_elements - 1;
    c->step = 1;                        // Set default step to 1

    // FROM

    if (c->token_delim != '[')          // If no array specifiers
    {
        c->n_token_chars = 0;           // Cancel token
        return (0);
    }

    if ((p->type == PT_PARENT)                          // If property is a PARENT
        && (c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD))     // and it is a set command
    {
        return (FGC_BAD_ARRAY_IDX);                     // report BAD ARRAY INDEX
    }

    switch (ParsScanInteger(c, "N,]", &value))          // Switch on result of scan for 1st integer
    {
        case FGC_NO_SYMBOL:                             // Token buffer is empty

            break;

        case 0:                                         // Valid integer

            if (Test(value, ~PROP_ARRAY_IDX_MASK) ||    // If idx is out of possible range, or
                (p->type != PT_PARENT &&            // (Not a parent, and
                 value >= p->max_elements)) //  longer than max elements)
            {
                return (FGC_BAD_ARRAY_IDX);             // Report BAD ARRAY INDEX
            }

            c->from = value;                   // Save new from value
            c->to = c->from;                            // Pass index to calling function
            c->n_arr_spec = 2;
            break;

        default:                                        // Invalid number

            return (FGC_BAD_ARRAY_IDX);                 // Report BAD ARRAY INDEX
    }

    // TO

    if (c->token_delim == ',')                          // If a second index is expected
    {
        switch (ParsScanInteger(c, "N,]", &value))      // Switch on result of scan for 2nd integer
        {
            case FGC_NO_SYMBOL:                         // Token buffer is empty

                c->n_arr_spec = 1;
                c->to = p->max_elements - 1;            // to = max_els-1
                break;

            case 0:                                     // Valid integer

                if (Test(value, ~PROP_ARRAY_IDX_MASK) || // If idx is out of possible range, or
                    (p->type != PT_PARENT &&        // (property is not a PARENT and
                     value >= p->max_elements) || //  'to' is beyond the end of array) or
                     value < c->from)       // 'to' index is less than from index
                {
                    return (FGC_BAD_ARRAY_IDX);         // Report BAD ARRAY INDEX
                }

                c->to = value;                 // Save new to value
                c->n_arr_spec = 2;
                break;

            default:                                    // Invalid number

                return (FGC_BAD_ARRAY_IDX);             // Report BAD ARRAY INDEX
        }
    }

    // STEP

    if (c->token_delim == ',')                          // If a step is expected
    {
        switch (ParsScanInteger(c, "N]", &value))       // Switch on result of scan for 3rd integer
        {
            case FGC_NO_SYMBOL:                         // Token buffer is empty

                break;                                  // Keep default value of 1

            case 0:                                     // Valid integer

                if (Test(value, ~PROP_ARRAY_IDX_MASK) || !value // if step is out of possible range, or
                    || (( value > 1)        // step > 1, and
                        && ((c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD) // (Set command, or
                            || (p->type == PT_PARENT))      //  property is a PARENT)
                       ))
                {
                    return (FGC_BAD_ARRAY_IDX);         // Report BAD ARRAY INDEX
                }

                // Clip step to max_elements

                c->step = (value < p->max_elements ? value : p->max_elements);

                break;

            default:                                    // Invalid number

                return (FGC_BAD_ARRAY_IDX);             // Report BAD ARRAY INDEX
        }
    }

    CmdNextCh(c, &ch); // Take next char from cmd pkt (it is safe to ignore errnum)

    if (ch && ch != ' ')                                // If not zero or a space
    {
        LogEvtSaveCh(c, ch);                            // Log character
        return (FGC_SYNTAX_ERROR);                      // Report SYNTAX_ERROR
    }

    c->n_token_chars = 0;                               // Cancel token
    return (0);                                         // Return success
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PropIdentifyGetOptions(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  This function will try to identify the get options which follow a property name.  Options must be
  separated by spaces.

  Input parameters:

        struct cmd *c           Pointer to command structure
        struct prop *p          Pointer to property structure

  Output parameters:

          c->getopts            Get option flags
        c->token_delim          Array delimiter

  Return value:

        0                       Successful identification of get options
        FGC_BAD_GET_OPT         Invalid get option encountered
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      sym;
    INT16U      errnum;
    INT16U      flag;
    INT16U      flags;

    errnum = 0;                                 // Clear status
    flags = 0;                                  // Reset option flags

    while (!errnum)                             // While status is good
    {
        errnum = PropIdentifySymbol(c, "Y ", ST_GETOPT, &sym); // Identify Symbol (delim '\0' or ' ')

        switch (errnum)                                 // Switch on status
        {
            case 0:                                     // Get option identified

                flag = sym_tab_getopt[sym].value;       // Get flag constant from symbol table

                if (Test(flags, flag))                  // If option already specified
                {
                    return (FGC_BAD_GET_OPT);           // Report BAD GET OPTION
                }

                Set(flags, flag);                       // Set option flag
                break;

            case FGC_NO_SYMBOL:                         // No token found

                break;

            default:                                    // All other errors

                return (FGC_BAD_GET_OPT);               // Report BAD GET OPT
        }
    }

    if (TestAll(flags, GET_OPT_IDX | GET_OPT_NOIDX) ||  // If index options contradict (both are set)
        (Test(flags, GET_OPT_BIN) &&                // Or, if invalid BIN option
         (c == &tcm || p->type == PT_PARENT)))
    {
        return (FGC_BAD_GET_OPT);                       // Report BAD GET OPTION
    }

    // Set token delimiter: Space forces NEAT for terminal command, Comma is standard for FIP cmds or direct

    c->token_delim = (c == &tcm && terminal.mode == TRM_EDITOR_MODE ? ' ' : ',');

    c->getopts = flags;                                 // Return get option flags in command structure

    return (0);                                         // Return OK status
}
/*---------------------------------------------------------------------------------------------------------*/
prop_size_t PropGetNumEls(struct cmd * c, struct prop const * p)
/*---------------------------------------------------------------------------------------------------------*\
  This function will return the number of elements contained in the property.  This may either be directly
  in p->n_elements, or in *p->n_elements, according to p->flags & PF_INDIRECT_N_ELS.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (Test(p->flags, PF_DSP_FGC))             // If DSP property
    {       

        if (c)                                  // If command structure provided
        {
            return (c->n_elements);             // Return cached length
        }

        return (0);                             // Unknown so return 0
    }

    if (Test(p->flags, PF_INDIRECT_N_ELS))      // MCU Property : if indirect
    {
        if (c)
        {
            return (((prop_size_t *) p->n_elements)[c->mux_idx]);
        }

        return (*((prop_size_t *) p->n_elements));
    }

    return ((prop_size_t) p->n_elements);
}
/*---------------------------------------------------------------------------------------------------------*/
void PropSetNumEls(struct cmd * c, struct prop * p, prop_size_t n_elements)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set the number of elements contained in the property. NB: p->n_elements may either be
  the number of elements, or a pointer to it, according to (p->flags & PF_INDIRECT_N_ELS).
\*---------------------------------------------------------------------------------------------------------*/
{
    if (Test(p->flags, PF_DSP_FGC))             // If attempt to set n_elements for a DSP property
    {
        Crash(0xBAD3);                          // Crash to generate a run log dump
    }

    if (Test(p->flags, PF_INDIRECT_N_ELS))
    {
        if (c)
        {
            ((prop_size_t *)p->n_elements)[c->mux_idx] = n_elements;
        }
        else
        {
            *((prop_size_t *)p->n_elements)            = n_elements;
        }
    }
    else
    {
        p->n_elements = (uintptr_t)n_elements;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void PropSet(struct cmd * c, struct prop * p, const void * value, prop_size_t n_elements)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to directly set a property value.  The value may NOT be greater than PROP_BLK_SIZE \
  bytes long.
\*---------------------------------------------------------------------------------------------------------*/
{
    if ((prop_type_size[p->type] * n_elements) > PROP_BLK_SIZE)
    {
        Crash(0xBAD2);
    }

    PropBufWait(c);                             // Wait for property block buffer to be free

    c->cached_prop  = p;                        // Prepare command structure variables
    c->prop_blk_idx = 0;

    // Copy data to block buffer, from supplied value
    memcpy(&c->prop_buf->blk, value, prop_type_size[p->type] * n_elements);

    PropBlkSet(c, n_elements, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT);
}
/*---------------------------------------------------------------------------------------------------------*/
void PropSetFar(struct cmd * c, struct prop * p, const void * value, prop_size_t n_elements)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to directly set a property value.  The value may be greater than PROP_BLK_SIZE bytes long.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      n_blocks;                       // Number of property transfer blocks
    INT16U      remaining_w;                    // Number of words remaining to copy
    INT16U      errnum = FGC_OK_NO_RSP;

    c->cached_prop  = p;                        // Prepare command structure variables
    c->prop_blk_idx = 0;
    c->n_elements   = 0;                        // TODO to be reviewed

    if (n_elements == 0)
    {
        remaining_w = 0;
        n_blocks    = 1;
    }
    else
    {
        remaining_w = (n_elements * prop_type_size[p->type] + 1) / 2;
        n_blocks    = (remaining_w + PROP_BLK_SIZE_W - 1) / PROP_BLK_SIZE_W;
    }

    while (n_blocks--)
    {
        if (Test(p->flags, PF_DSP_FGC))
        {
            errnum = PropBufWait(c);

            if (errnum != FGC_OK_NO_RSP)
            {
                return;
            }
        }

        // Copy data to property buffer

        memcpy(c->prop_buf->blk.intu, value, (n_blocks ? PROP_BLK_SIZE_W : remaining_w) * sizeof(uint16_t));

        errnum = PropBlkSet(c, n_elements, TRUE, (n_blocks ? 0 : FGC_FIELDBUS_FLAGS_LAST_PKT));

        if (errnum)
        {
            RunlogWrite(FGC_RL_UXTIME, (void *) &UTC_MCU_UNIXTIME_P);   // Unix time
            RunlogWrite(FGC_RL_MSTIME, (void *) &UTC_MCU_MS_P);         // Millisecond time
            RunlogWrite(FGC_RL_DSP_STATUS, &errnum);
            return;
        }

        value  = (INT16U *) value + PROP_BLK_SIZE_W;
        remaining_w -= PROP_BLK_SIZE_W;
        c->prop_blk_idx++;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PropValueGet(struct cmd * c, struct prop * p, BOOLEAN * set_f, prop_size_t el_idx, INT8U ** data_ptr)
/*---------------------------------------------------------------------------------------------------------*\
  This function will recover the specified element of the specified property.  If the value block
  containing the element (which may not span blocks) has not already been recovered, the function uses
  PropBlkGet() to read the PROP_BLK_SIZE byte block.  If the *set_f is set, then before recovering a new block,
  the existing block will be written back to the property value buffer and *set_f will be cleared.
  The function writes a pointer to the required element data in argument data_ptr and sets:

        c->cached_prop          Currently cached property
        c->prop_blk_idx         Currently cached block index
        c->mux_idx              Multiplexor index
        c->n_elements           Set when caching a block to number of elements in the property
        *set_f                  Cleared if current block is saved before reading new block

  The function returns a status (FGC_OK_NO_RSP if no error)
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U      start_byte;
    INT16U      blk_idx;
    INT16U      errnum;

    start_byte = (INT32U) el_idx * (INT32U) prop_type_size[p->type]; // Calculate start byte for element in value
    blk_idx = start_byte / PROP_BLK_SIZE;    // Calculate block index containing the element

    if (c->cached_prop != p || c->prop_blk_idx != blk_idx)      // If required block is NOT already in buffer
    {
        if (set_f && *set_f)                                    // If pointer to set flag provided and set
        {
            errnum = PropBlkSet(c, 0, TRUE, 0);                 // Set current block, 0=!last blk

            if (errnum)
            {
                return (errnum);
            }

            *set_f = FALSE;                                     // Clear set flag
        }


        c->cached_prop = p;                                     // Get new block
        c->prop_blk_idx = blk_idx;
        
        errnum = PropBlkGet(c, &(c->n_elements));

        if (errnum)
        {
            return (errnum);
        }
    }

    if (data_ptr != NULL)
    {
        *data_ptr = (INT8U *) &c->prop_buf->blk + (start_byte & (PROP_BLK_SIZE -
                                                                 1)); // Return pointer to required element
    }

    return (FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PropBlkGet(struct cmd * c, prop_size_t * n_elems_ptr)
/*---------------------------------------------------------------------------------------------------------*\
  This function will get a block of property data into the property block buffer linked to the command
  structure.  The property may be in the MCU or DSP.

  Inputs:       c->cached_prop          Pointer to property
                c->prop_blk_idx         Block being got
                c->cached_prop->flags   Property flags
                c->mux_idx              Multiplexor index

                n_elems_ptr             Pointer for the returned number of elements

  Returns:      An error status.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U              n_bytes;
    INT32U              value_addr;
    INT32U              offset;                         // byte offset to start of block in the property
    struct prop    *    p = c->cached_prop;
    struct prop_buf  *  pbuf = c->prop_buf;
    INT8U       *       value;
    INT16U              dsp_status;

    PropBufWait(c);                                     // Wait for property block buffer to be free

    if (Test(p->flags, PF_DSP_FGC))                     // If FGC DSP property
    {
        pbuf->dsp_prop_idx = p->dsp_idx;                // Set DSP property index
        pbuf->blk_idx      = c->prop_blk_idx;           // Set block index of required block
        pbuf->mux_idx      = c->mux_idx;                // Set multiplexor index
        pbuf->action = FGC_FIELDBUS_FLAGS_GET_CMD;      // Set GET action to trigger DSP PropComms()
        dsp_status         = PropBufWait(c);            // Wait for DSP to respond

        if (dsp_status != FGC_OK_NO_RSP)
        {
            return (dsp_status);
        }

        if (pbuf->dsp_rsp != PROP_DSP_RSP_OK)
        {
            return (FGC_DSP_PROPERTY_ISSUE);
        }
    }
    else                                                // else MCU property
    {
        n_bytes    = prop_type_size[p->type] * p->max_elements;         // Size of property (for one mux_idx)
        offset     = c->prop_blk_idx * PROP_BLK_SIZE;                   // Byte offset to start of block

        value_addr = (INT32U)p->value + c->mux_idx * n_bytes;           // Address for data for mux_idx
        value      = (INT8U *) value_addr + offset;                     // Start of block
        n_bytes   -= offset;                                            // Number of bytes to end of property

        if (n_bytes > PROP_BLK_SIZE)                                    // Clip to size of one block
        {
            n_bytes = PROP_BLK_SIZE;
        }

        if(p->type == PT_DEV_NAME)
        {
            memcpy(&pbuf->blk, *(char **)value_addr, n_bytes);               // Copy data to block buffer
        }
        else
        {
            memcpy(&pbuf->blk, value, n_bytes);               // Copy data to block buffer
        }

        if (n_bytes & 1)                                                // If odd number of bytes
        {
            ((INT8U *) &pbuf->blk)[n_bytes] = 0;                        // Pad zero to word boundary
        }

        pbuf->n_elements = PropGetNumEls(c, p);
    }

    if (n_elems_ptr != NULL)
    {
        *n_elems_ptr = (prop_size_t) pbuf->n_elements;                  // Return number of elements in the property
    }

    return (FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PropBlkSet(struct cmd * c, prop_size_t n_elements, BOOLEAN set_f, INT16U last_blk_f)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set a block of property data into the property value.  The property may be in the
  MCU or DSP.  If the calling function sets the last_blk_f, the property length will be checked to see
  if the property has shrunk.  If so, trailing values will be cleared to zero.  If the property has grown,
  the length will be set.

  Inputs:       c->cached_prop          Pointer to property
                c->prop_blk_idx         Block being set
                c->mux_idx              Multiplexor index
                c->cached_prop->flags   Property flags
                last_blk_f              0 or FGC_FIELDBUS_FLAGS_LAST_PKT

  Returns: status (FGC_OK_NO_RSP if no error was detected)
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U              el_size;
    INT16S              n_bytes;                // Crash if n_bytes turns out to be negative
    INT32U              offset;
    prop_size_t         n_els_old;
    INT32U              value_addr;
    INT8U       *       value_p;
    struct prop    *    p         = c->cached_prop;
    struct prop_buf  *  pbuf      = c->prop_buf;

    el_size = prop_type_size[p->type];

    if (el_size == 0)           // Would cause a division by zero later on
    {
        return (FGC_NULL_ADDRESS);
    }

    if ( last_blk_f == 0)
    {
        // NB: Integer multiplication must be done before the division

        n_elements = ((c->prop_blk_idx + 1) * (prop_size_t) PROP_BLK_SIZE) / el_size;
    }

    if (Test(p->flags, PF_DSP_FGC))                             // If FGC DSP property
    {
        if (set_f || last_blk_f)                                // If set required, or last_block
        {
            pbuf->dsp_prop_idx = p->dsp_idx;                    // Set DSP property index
            pbuf->blk_idx = c->prop_blk_idx;                    // Set block index of block to be set
            pbuf->mux_idx = c->mux_idx;                         // Set multiplexor index
            pbuf->n_elements = n_elements;                      // Set new n_els in property

            if (set_f)                                          // If set flag
            {
                pbuf->action = FGC_FIELDBUS_FLAGS_SET_CMD | last_blk_f; // Trig DSP to update contents and length
            }
            else                                                // else
            {
                pbuf->action = last_blk_f;                      // Trig DSP to update length only
            }

            PropBufWait(c);
        }
    }
    else                                                        // else MCU property
    {
        offset = c->prop_blk_idx * PROP_BLK_SIZE;               // Byte offset to start of block

        value_addr = (INT32U) p->value + c->mux_idx * p->max_elements * el_size;

        value_p = (INT8U *) value_addr + offset;                // Start of block in buffer

        if (set_f)                                              // If set required
        {
            n_bytes = el_size * n_elements - offset;            // Num of bytes from start of block

            if (n_bytes < 0)
            {
                Crash(0xBAD4);
            }

            if (n_bytes > PROP_BLK_SIZE)                        // Clip to size of one block
            {
                n_bytes = PROP_BLK_SIZE;
            }

            if(p->type == PT_DEV_NAME)
            {
                memcpy(*(char **)value_addr, &pbuf->blk, n_bytes); // Copy data from block buffer
            }
            else
            {
                memcpy(value_p, &pbuf->blk, n_bytes); // Copy data from block buffer
            }
        }

        n_els_old = PropGetNumEls(c, p);

        // If last block and data has shrunk

        if (last_blk_f && n_elements < n_els_old)
        {
            // Clear to zero all trailing elements in property

            if(p->type == PT_DEV_NAME)
            {
                memset(*(((char **)p->value) + n_elements), 0, (n_els_old - n_elements) * el_size);
            }
            else
            {
                memset((void *)(value_addr + n_elements * el_size), 0, (n_els_old - n_elements) * el_size);
            }
        }

        if (last_blk_f ||                                       // If last block, or
            n_elements > n_els_old)                         // property has grown
        {
            PropSetNumEls(c, p, n_elements);                    // Set property length
        }
    }

    // If write_nvs_f == TRUE, and property is non-volatile and it's not a reference function property

    if (c->write_nvs_f && Test(p->flags, PF_NON_VOLATILE) && !Test(p->flags, PF_DONT_STORE_ON_SET))
    {
        // Save property data in NVS

        nvsStoreBlockForOneProperty(c, p, (INT8U *) &pbuf->blk, 0, c->mux_idx, n_elements, last_blk_f);
    }

    return (FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PropBufWait(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This function will wait for the property data buffer to become available.  The buffer may be in use by
  the DSP to set/get a DSP property.  If the buffer is busy, the function will suspend the task having
  first set a flag to request that the MstTsk() resume the task on the next millisecond.

  Input:   command structure struct cmd

  Returns: DSP status, FGC_DSP_NOT_AVL if DSP is OFF, FGC_OK_NO_RSP otherwise
           That status should not be considered if the property is not a DSP property
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32U start;
    INT16U retval;

    // Wait until the buffer is no longer in use or
    // the DSP is not responding (5 second time-out)

    start = TimeGetUs();

    while (!mst.dsp_is_standalone &&
           c->prop_buf->action    &&
           (TimeGetUsDiffNow(start)) <= 5000000U)
    {
        // Resumed by MstTsk() on next millisecond

        OSTskSuspend();
    }

    if (c->prop_buf->action == 0)
    {
        retval = FGC_OK_NO_RSP;
    }
    else
    {
        // Only notify if the DSP is not in stand-alone

        if (!mst.dsp_is_standalone)
        {
            prop_debug.access_timeouts++;
            RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));
            RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));
            RunlogWrite(FGC_RL_PROP_TIMEOUT, &c->prop->dsp_idx);
        }

        // Reset the buffer for further use.

        c->prop_buf->action = 0;

        retval = FGC_DSP_NOT_AVL;
    }

    return (retval);
}
/*---------------------------------------------------------------------------------------------------------*/
void PropMap(prop_map_func func, enum prop_map_specifier specifier)
/*---------------------------------------------------------------------------------------------------------*\
  This function will apply function 'func' on all FGC properties which comply with the argument 'specifier'
  (specifier = all properties, only parent properties or only child properties).
  In the case the map applies to parent and child properties, the map function is called on the parent
  BEFORE calling its children.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U idx;

    for (idx = 0; idx < FGC_N_TLPS; idx++)                 // For each higher level PARENT property
    {
        PropMapRecurse(0, &((struct prop *) &PROPS)[idx], func, specifier);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static void PropMapRecurse(INT16U lvl, struct prop * p, prop_map_func func, enum prop_map_specifier specifier)
/*---------------------------------------------------------------------------------------------------------*\
  This function is RECURSIVE and called by PropMap() only.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U idx;

    if (p->type == PT_PARENT)                           // If PARENT property
    {
        if (specifier == PROP_MAP_ALL || specifier == PROP_MAP_ONLY_PARENTS)
        {
            func(lvl, p);
        }

        if (p->value != &PROPS)     // Avoid a loopback to upper level on property such as CONFIG.SET
        {
            for (idx = 0; idx < p->max_elements; idx++) // For each CHILD property
            {
                PropMapRecurse(lvl + 1, &((struct prop *) p->value)[idx], func, specifier);
            }
        }
    }
    else                                                // If CHILD property
    {
        if (specifier == PROP_MAP_ALL || specifier == PROP_MAP_ONLY_CHILDREN)
        {
            func(lvl, p);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: prop.c
\*---------------------------------------------------------------------------------------------------------*/
