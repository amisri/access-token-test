/*!
 *  @file      pub.h
 *  @defgroup  FGC3:MCU
 *  @brief     Unused sub table entries are chained in a free list
 */

#define PUB_GLOBALS             // define sub global variable
#define DEFPROPS_INC_ALL        // defprops.h

// Includes

#include <pub.h>

#include <class.h>
#include <cmd.h>
#include <definfo.h>
#include <defprops.h>
#include <dev.h>
#include <dpcom.h>
#include <extend_mask.h>
#include <fbs.h>
#include <fgc_errs.h>
#include <hash.h>
#include <macros.h>
#include <mcu_panic.h>
#include <mem.h>
#include <memmap_mcu.h>
#include <os.h>
#include <ref.h>
#include <string.h>
#include <task_trace.h>
#include <tsk.h>

// Constants

#define SUB_INIT         0     // Signal to PubInitSubs() to init only
#define SUB_REINIT       1     // Signal to PubInitSubs() to re-init

// Internal function declarations

/*!
 * This function will initialise or re-initialise the sub structure
 */
static void PubInitSubs(INT16U reinit_f);

/*!
 * This function will finish sending a publication response to the
 * gateway and will wait for the last packet to be written to the
 * fieldbus interface.  This does not mean that it has actually been
 * sent to the gateway.
 */
static void PubWaitForLastPktSend(void);

/*!
 * This function is called from PubTsk() to publish the subscription
 * specified by sub_idx and user
 */
static void PubSendSub(INT16U sub_idx, INT16U user, BOOLEAN new_sub_f, BOOLEAN set_f);

/*!
 * Publishes a subscription for a given user.
 */
static void PubUser(INT16U sub_idx, INT16U user, BOOLEAN set_f);

// External function definitions

void PubTsk(void * unused)
{
    // This function never exits, so the "local" variables are
    // static which saves stack space.

    static INT16U  user;
    static BOOLEAN set_f;
    static BOOLEAN new_sub_f;
    static INT8U   pub_indx;
    static struct prop const * p;
    static struct sub_table * sub_table;

    // Initialise FILE structures for 'pcm' stream

    pcm.f = fopen("pcm", "wb");

    if (setvbuf(pcm.f, NULL, _IONBF, 0) != 0)  // not buffered, buffer size 0
    {
        // 0   = ok
        // EOF = error
    }

    pcm.store_cb      = FbsPutcStorePub;        // Use PUB character store callback function
    pcm.write_nvs_f   = TRUE;

    sub.new_sub_idx = 0xFF;

    PubInitSubs(SUB_INIT);                      // Initialise free subs list only

    for (;;)                                    // Main publication task loop
    {
        TskTraceInc();

        OSSemPost(pub.lock);                    // Unlock sub structure

        TskTraceReset();

        OSTskSuspend();                         // Resumed by MstTsk() every 1 ms

        TskTraceInc();

        OSSemPend(pub.lock);                    // Lock sub structure

        TskTraceInc();

        /*--- Cancel all Subs if PLL unlocked ---*/

        if (!fbs.gw_online_f)    // If PLL unlocked or GW not online (eth only)
        {
            // TODO check its usefulness when pll is implemented (can we have gw offline AND pll still locked?)
            if (sub.n_subs)                              // If subscriptions are still present
            {
                PubInitSubs(SUB_REINIT);                // Re-initialise sub structure
            }

            sub.flags = 0;                              // Clear sub flags
            continue;                                   // Skip until PLL re-locked
        }

        TskTraceInc();

        // Publish subscriptions that have already been acknowledged
        // to the Gateway.

        while (ExtendMaskScan((struct extended_mask_gen *) &sub.sub_acked_msk, &pub_indx))
        {
            ExtendMaskClrBit((struct extended_mask_gen *) &sub.sub_acked_msk, pub_indx);

            PubSubscription(pub_indx);
        }

        TskTraceInc();

        // Publish subscriptions

        while (!pcm.abort_f        &&                       // If pub stream not aborted, and
               (sub.n_direct_pubs  ||                       // Direct publication requests are outstanding, or
                (sub.n_get_pubs    &&                       // Get publication requests are outstanding and
                 !Test(sub.flags, SUB_FLAGS_GET_PENDING)))) // no get request to GW is pending
        {
            // Identify next subscription to process

            if (!ExtendMaskScan((struct extended_mask_gen *)&sub.notify_msk, &sub.pub_idx))
            {
                McuPanic(MCU_PANIC_PUB_TSK_SUB_NOTIFY,
                         RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16  | RGLEDS_PIC_RED_MASK16,
                         RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16 | RGLEDS_DCCT_RED_MASK16);
            }

            sub_table = &sub.table[sub.pub_idx];                // Local variables for speed
            p = sub_table->prop;
            new_sub_f = ExtendMaskTestBit(sub.new_sub_msk, sub.pub_idx);

            if (p == NULL)                                         // If p is indeed NULL, avoid an exception later on in the code
            {
                McuPanic(MCU_PANIC_PUB_TSK_NULL_PROP_PTR,
                         RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16  | RGLEDS_PIC_RED_MASK16,
                         RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16 | RGLEDS_DCCT_RED_MASK16);
            }

            if (!Test(p->dynflags, DF_PUB_USING_GET) ||         // If this sub does NOT require a GET by GW, or
                !Test(sub.flags, SUB_FLAGS_GET_PENDING))        // a get is not already pending
            {
                if (Test(p->flags, PF_PPM))                              // If PPM property
                {
                    // Identify next user to publish and clear corresponding bit in the notify mask of the subscription

                    if (!ExtendMaskScan((struct extended_mask_gen *)&sub_table->notify_msk, &sub_table->idx))
                    {
                        McuPanic(MCU_PANIC_PUB_TSK_SUB_TABLE_NOTIFY,
                                 RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16  | RGLEDS_PIC_RED_MASK16,
                                 RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16 | RGLEDS_DCCT_RED_MASK16);
                    }

                    user = sub_table->idx;                                                      // User to publish

                    ExtendMaskClrBit((struct extended_mask_gen *)&sub_table->notify_msk, user); // Clear notification bit

                    // Is the publication the result of a set cmd?
                    set_f = ExtendMaskTestBit(sub_table->set_f_msk, user);
                    ExtendMaskClrBit((struct extended_mask_gen *)&sub_table->set_f_msk, user);  // Clear set_f bit

                    if (!sub_table->notify_msk.master)                        // If no more users to publish
                    {
                        sub_table->idx = 0;                                         // Reset user index
                        sub_table->notify_count = 0;                                // Reset notify count

                        // Reset notify bit in the subscription

                        ExtendMaskClrBit((struct extended_mask_gen *)&sub.notify_msk,  sub.pub_idx);
                        ExtendMaskClrBit((struct extended_mask_gen *)&sub.new_sub_msk, sub.pub_idx);
                    }
                    else                                                    // else
                    {
                        sub_table->notify_count--;                              // Decrement notify count

                        if (++sub_table->idx > dev.max_user)               // Advance to next user
                        {
                            sub_table->idx = 0;
                        }
                    }
                }
                else                                                    // else non-PPM property
                {
                    user = NON_PPM_USER;

                    sub_table->notify_count = 0;                            // Reset notify count

                    // Clear notify bit for user zero and that subscription

                    // Is the publication the result of a set cmd?
                    set_f = ExtendMaskTestBit(sub_table->set_f_msk, NON_PPM_USER);
                    ExtendMaskClrBit((struct extended_mask_gen *)&sub_table->set_f_msk, NON_PPM_USER);  // Clear set_f bit

                    ExtendMaskClrBit((struct extended_mask_gen *)&sub.notify_msk,        sub.pub_idx);
                    ExtendMaskClrBit((struct extended_mask_gen *)&sub.new_sub_msk,       sub.pub_idx);
                    ExtendMaskClrBit((struct extended_mask_gen *)&sub_table->notify_msk, NON_PPM_USER);
                }

                PubSendSub(sub.pub_idx, user, new_sub_f, set_f);        // Send subscription
            }

            sub.pub_idx++;

            if (sub.pub_idx >= FGC_MAX_SUBS)                   // Advance to next subscription
            {
                sub.pub_idx = 0;
            }
        }

        TskTraceInc();
    }
}

INT16U PubAddSub(struct prop * p, INT16U * sub_idx_ptr)
{
    INT16U sub_idx;                                 // The subscription index

    OSSemPend(pub.lock);                            // Wait for access to sub table

    if (Test(p->dynflags, DF_SUBSCRIBED))           // If property is already subscribed
    {
        OSSemPost(pub.lock);                        // Release lock on sub table
        return FGC_PROTO_ERROR;                     // Report protocol error
    }

    if (sub.n_subs >= FGC_MAX_SUBS)                 // If subscription table is full
    {
        OSSemPost(pub.lock);                        // Release lock on sub table
        return FGC_BUSY;                            // Report FGC busy
    }

    sub.n_subs++;

    sub_idx = sub.free_sub_idx;                     // Take next free entry in the sub table
    sub.free_sub_idx = sub.table[sub_idx].idx;
    sub.table[sub_idx].idx = 0;                     // Reset user pub index for the new sub
    sub.new_sub_idx = sub_idx;
    p->sub_idx = sub_idx;                           // Link property to subscription
    sub.table[sub_idx].prop = p;                    // Link subscription to property
    Set(p->dynflags, DF_SUBSCRIBED);

    OSSemPost(pub.lock);                            // Release lock on sub table

    *sub_idx_ptr = sub_idx;

    return FGC_OK_NO_RSP;
}

INT16U PubRemoveSub(struct prop * p, INT16U * sub_idx_ptr)
{
    INT16U              sub_idx;
    struct sub_table  * sub_table;

    OSSemPend(pub.lock);                                        // Wait for access to sub table

    if (!Test(fcm.prop->dynflags, DF_SUBSCRIBED))            // If property has no subscription
    {
        OSSemPost(pub.lock);                                // Release lock on sub table
        return (FGC_PROTO_ERROR);                           // Report Protocol Error
    }

    sub_idx   = p->sub_idx;                                 // Get sub_idx for property
    sub_table = &sub.table[sub_idx];

    sub.n_subs--;                                           // Dec total subs counter

    if (Test(sub_table->prop->dynflags, DF_PUB_USING_GET))   // If PUB using GET from GW
    {
        sub.n_get_pubs -= sub_table->notify_count;          // Dec global get pending count
    }
    else                                                    // else if direct PUB to GW
    {
        sub.n_direct_pubs -= sub_table->notify_count;       // Dec global direct pending count
    }


    if (Test(sub.flags, SUB_FLAGS_GET_PENDING) &&            // If this subscription is pending a GET from the GW
        sub.pending_get.sub_idx == sub_idx)
    {
        Clr(sub.flags, SUB_FLAGS_GET_PENDING);               // Then clear the GET pending flag
    }

    Clr(sub_table->prop->dynflags, DF_SUBSCRIBED);          // Cancel sub flag in property

    // Clear the notification bit in the subscription mask

    ExtendMaskClrBit((struct extended_mask_gen *)&sub.notify_msk, sub_idx);

    memset(sub_table, 0, sizeof(struct sub_table));                 // Erase subscription table

    sub_table->idx   = sub.free_sub_idx;                            // Return table record to free list
    sub.free_sub_idx = sub_idx;

    OSSemPost(pub.lock);                                                // Release lock on sub table

    *sub_idx_ptr = sub_idx;

    return FGC_OK_NO_RSP;
}

void PubCancelPub(void)
{
    pcm.abort_f       = TRUE;                   // Inhibit output to pcm stream
    sub.flags         = 0;                      // Clear sub flags
    fbs.pub.q.out_idx = fbs.pub.q.in_idx;       // Reset fifo pointers
    fbs.pub.out_idx   = fbs.pub.q.in_idx;       // Reset local queue out pointer
    fbs.pub.q.is_full = FALSE;                  // Mark queue as not full

    if (fbs.pub.q_not_full->pending)             // If PubTsk pending on queue full semaphore
    {
        OSSemPost(fbs.pub.q_not_full);          // Post semaphore to wake task
    }

    if (pub.last_pkt_send->pending)              // If PubTsk pending on last packet send semaphore
    {
        OSSemPost(pub.last_pkt_send);           // Post semaphore to wake task
    }

    OSTskResume(TSK_PUB);                       // Resume PubTsk if suspended
}

void PubProperty(struct prop * p, INT16U user, BOOLEAN set_f)
{
    OSSemPend(pub.lock);                                // Lock sub structure

    do                                                  // Loop for all levels up the property tree
    {
        if (Test(p->dynflags, DF_SUBSCRIBED))
        {
            PubUser(p->sub_idx, user, set_f);
        }
    }
    while (!Test(p->flags, PF_HIDE) && (p = p->parent)); // Until property hidden or has no parent

    OSSemPost(pub.lock);                                // Unlock sub structure
}

void PubSubscription(INT16U sub_idx)
{
    if (sub.table[sub_idx].prop)                         // If sub is in use
    {
        ExtendMaskSetBit((struct extended_mask_gen *)&sub.new_sub_msk, sub_idx);

        if (!Test(sub.table[sub_idx].prop->flags, PF_DONT_PUB_ON_SUB))
        {
            INT16U user;
            INT16U max_user;

            if (   Test(sub.table[sub_idx].prop->flags, PF_PPM)
                && (   sub.table[sub_idx].prop->set_func_idx == SET_NONE
                    || DEVICE_PPM == FGC_CTRL_ENABLED))
            {
                max_user = FGC_MAX_USER;
            }
            else
            {
                max_user = 0;
            }

            for (user = 0 ; user <= max_user ; user++)          // Publish all users for this subscription
            {
                PubUser(sub_idx, user, FALSE);
            }
        }
    }
}

// Internal function definitions

static void PubInitSubs(INT16U reinit_f)
{
    INT16U              idx;

    if (reinit_f == SUB_REINIT)                          // If re-initalising
    {
        for (idx = 0 ; idx < FGC_MAX_SUBS ; idx++)            // Cancel subscribed flags in properties
        {
            if (sub.table[idx].prop)                                 // If sub is in use
            {
                Clr(sub.table[idx].prop->dynflags, DF_SUBSCRIBED);      // Clear SUBSCRIBED flag in property
            }
        }

        MemSetWords(&sub, 0x0000, sizeof(sub) / 2);       // Clear complete sub structure
    }

    for (idx = 0 ; idx < FGC_MAX_SUBS ; idx++)        // Link all sub table entries to free list
    {
        sub.table[idx].idx = idx + 1;
    }
}

static void PubWaitForLastPktSend(void)
{
    OS_CPU_SR  cpu_sr;

    OS_ENTER_CRITICAL();                                        // Protect against interrupts

    if (!pcm.abort_f)
    {
        Set(fbs.pub.header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT); // Set last packet flag
        FbsPutcStorePub(';');                                   // Finish response with ';'

        OSSemPend(pub.last_pkt_send);                           // Wait till last packet is in the interface
    }

    OS_EXIT_CRITICAL();
}

static void PubSendSub(INT16U sub_idx, INT16U user, BOOLEAN new_sub_f, BOOLEAN set_f)
{
    INT16U             i;
    INT16U             sym_idxs[FGC_MAX_PROP_DEPTH];
    struct prop    *   p;
    struct sub_table * sub_table = &sub.table[sub_idx];
    INT8U              flags = 0;

    fbs.pub.rsp_len = 0;                                        // Reset pub response length

    Clr(fbs.pub.header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT |     // Clear last rsp packet flag
        FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK); // and rsp type bits

    if (new_sub_f)
    {
        flags |= FGC_FIELDBUS_PUB_TRIG_NEW_SUB;                 // The publication was triggered by the start of a subscription
    }
    else if (set_f)
    {
        flags |= FGC_FIELDBUS_PUB_TRIG_SET;                     // The publication was triggered by a set command.

        // Note that the 'set' and the 'new subscription' flags cannot be active simultaneously.
        // The latter will preempt the former.
    }

    if (Test(sub_table->prop->dynflags, DF_PUB_USING_GET))       // If PUB using GET from gateway
    {
        sub.n_get_pubs--;                                           // Dec count of get pubs pending

        Set(sub.flags, SUB_FLAGS_GET_PENDING);                      // Note that GET is pending
        sub.pending_get.sub_idx = sub_idx;                          // Note which sub/user is pending
        sub.pending_get.user    = user;

        OSSemPost(pub.lock);                                        // Unlock sub structure

        Set(fbs.pub.header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT |    // Set first rsp packet flag
            FGC_FIELDBUS_FLAGS_PUB_GET_PKT);   // and PUB_GET packet type

        FbsPutcStorePub(sub_idx);                                   // Request get for sub_idx
        FbsPutcStorePub(flags);                                     // 'set' and 'new subscription' flags
        FbsPutcStorePub(user);                                      // and user index
    }
    else                                                            // else PUB directly using pub stream
    {

        sub.n_direct_pubs--;                                        // Dec count of direct pubs pending

        struct prop * prop_tmp = sub_table->prop;
        prop_size_t to_tmp = sub_table->prop->max_elements - 1;

        OSSemPost(pub.lock);                                            // Unlock sub structure

        OSSemPend(pcm.sem);                                         // Reserve pcm structure

        pcm.prop_buf    = (struct prop_buf *) &dpcom.pcm_prop;      // Prepare pcm for get function
        pcm.prop        = prop_tmp;
        pcm.timestamp_f = FALSE;
        pcm.mux_idx     = user;                                     // Publish for specified user
        pcm.cached_prop = NULL;
        pcm.f->_flags2 &= ~NL;   // Clear newline flag, is the only flag that can be handled externally
        pcm.n_arr_spec  = 0;
        pcm.from        = 0;
        pcm.to          = to_tmp;
        pcm.step        = 1;
        pcm.getopts     = GET_OPT_TYPE | GET_OPT_LBL | GET_OPT_BIN;
        pcm.token_delim = ',';

        // Prepare property symbol stack so that the property names are correctly reported

        pcm.si_idx = 0;
        p = pcm.prop;

        do
        {
            sym_idxs[pcm.si_idx++] = p->sym_idx;
            p = p->parent;
        }
        while (p);

        pcm.n_prop_name_lvls = pcm.si_idx;

        for (i = 0 ; i < pcm.n_prop_name_lvls ; i++)
        {
            pcm.sym_idxs[i] = sym_idxs[--pcm.si_idx];
        }

        pcm.si_idx = pcm.n_prop_name_lvls;

        // Create publication header and call get function

        Set(fbs.pub.header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT |        // Set first rsp packet flag
            FGC_FIELDBUS_FLAGS_PUB_DIRECT_PKT);    // and PUB_DIRECT packet type

        FbsPutcStorePub(sub_idx);                                       // Add prefix giving sub index
        FbsPutcStorePub(flags);                                         // 'set' and 'new subscription' flags
        FbsPutcStorePub(user);                                          // user index

        // Set the last property if it as leaf so that the correct label can be appended in cmd.c::CmdPrintLabel()

        pcm.last_prop = (pcm.prop->type != PT_PARENT ? pcm.prop : NULL);

        // Call the get function for the property

        GET_FUNC[pcm.prop->get_func_idx](&pcm, pcm.prop);               // Call Get function

        OSSemPost(pcm.sem);                                             // Release pcm structure
    }

    PubWaitForLastPktSend();                            // Wait for last packet to be sending

    OSSemPend(pub.lock);                                // Lock sub structure
}

static void PubUser(INT16U sub_idx, INT16U user, BOOLEAN set_f)
{
    /*  This function is called from PubProperty and PubSubscription to mark
     *  a particular user of a particular subscription as needing publication.
     *  The calling function must have the sub semaphore before calling. It
     *  only notifies the sub/user if:
     *    - It is not already notified
     *    - It is not a new subscription pending the initial sub_idx being sent
     *      to the Gateway.
     *    - It is not already pending a get from the gateway
     */

    struct sub_table * sub_table = &sub.table[sub_idx];

    if (!ExtendMaskTestBit(sub_table->notify_msk, user)  &&
        !ExtendMaskTestBit(sub.sub_acked_msk, sub_idx) &&
        !(Test(sub.flags, SUB_FLAGS_GET_PENDING)         &&
          sub_idx == sub.pending_get.sub_idx             &&
          user == sub.pending_get.user))
    {
        // Notify this user, and this subscription

        ExtendMaskSetBit((struct extended_mask_gen *)&sub_table->notify_msk, user);
        ExtendMaskSetBit((struct extended_mask_gen *)&sub.notify_msk,        sub_idx);

        if (set_f)
        {
            ExtendMaskSetBit((struct extended_mask_gen *)&sub_table->set_f_msk, user);
        }
        else
        {
            ExtendMaskClrBit((struct extended_mask_gen *)&sub_table->set_f_msk, user);
        }

        sub_table->notify_count++;                                  // Inc notify count for subscription

        if (Test(sub_table->prop->dynflags, DF_PUB_USING_GET))       // If PUB using GET from GW
        {
            sub.n_get_pubs++;                                           // Inc global get pub count
        }
        else                                                        // else direct PUB to GW
        {
            sub.n_direct_pubs++;                                        // Inc global direct pub count
        }
    }
}

// EOF
