/* 
 *  @file      events.c
 *  @brief     Functionality to process timing events received from the gateway or 
 *             super-cycle simulation.
 */

#define EVENTS_GLOBALS
#define FGC_EVENT_GLOBALS


// Includes

#include <stdint.h>

#include <events.h>
#include <fgc_event.h>
#include <time_fgc.h>
#include <cycle_time.h>
#include <defprops.h>
#include <events_sim.h>
#include <fbs.h>
#include <log_cycle.h>
#include <log.h>
#include <mst.h>



// Internal structures, unions and enumerations

typedef void (*events_funcs_t)(struct fgc_event const *);



// Internal functions declaration

/*!
 * Function handling the event FGC_EVT_PM
 */
static void eventsProcessPostMortem(struct fgc_event const * event);

/*!
 * Function handling the event FGC_EVT_START
 */
static void eventsProcessStart(struct fgc_event const * event);

/*!
 * Function handling the event FGC_EVT_ABORT
 */
static void eventsProcessAbort(struct fgc_event const * event);

/*!
 * Function handling the event FGC_EVT_SSC
 */
static void eventsProcessSSC(struct fgc_event const * event);

/*!
 * Function handling the event FGC_EVT_CYCLE_TAG_HIGH
 */
static void eventsProcessCycleTagHigh(struct fgc_event const * event);

/*!
 * Function handling the event FGC_EVT_CYCLE_TAG_LOW
 */
static void eventsProcessCycleTagLow(struct fgc_event const * event);

/*!
 * Function handling the event FGC_EVT_INJECTION
 */
static void  eventsProcessInjection(struct fgc_event const * event);

/*!
 * Function handling the event FGC_EVT_EXTRACTION
 */
static void  eventsProcessExtraction(struct fgc_event const * event);

/*!
 * Function handling the event FGC_EVT_CYCLE_WARNING
 */
static void eventsProcessCycleStart(struct fgc_event const * event);

/*!
 * Returns an array with either real or simulated events.
 */
static struct fgc_event const * eventsSource(void);



// External function definitions

void eventsProcess(void)
{
    static events_funcs_t events_funcs[] =
    {
         NULL,                             // FGC_EVT_NONE,
         eventsProcessPostMortem,          // FGC_EVT_PM,
         eventsProcessStart,               // FGC_EVT_START,
         eventsProcessAbort,               // FGC_EVT_ABORT,
         NULL,                             // FGC_EVT_NEXT_CYC_USER,
         NULL,                             // FGC_EVT_NEXT_CYC_LEN,
         eventsProcessSSC,                 // FGC_EVT_SSC,
         NULL,                             // FGC_EVT_NEXT_DEST,
         eventsProcessCycleTagHigh,        // FGC_EVT_CYCLE_TAG_HIGH,
         eventsProcessCycleTagLow,         // FGC_EVT_CYCLE_TAG_LOW,
         eventsProcessInjection,           // FGC_EVT_INJECTION,
         eventsProcessExtraction,          // FGC_EVT_EXTRACTION,
         eventsProcessCycleStart,          // FGC_EVT_CYCLE_START,
    };

    static struct abs_time_ms lastEventTime[FGC_NUM_EVENT_TYPES];

    if (shared_mem.mainprog_seq != FGC_MP_MAIN_RUNNING)
    {
        return;
    }

    // Retrieve the event from either the gateway or the super-cycle simulation.

    struct fgc_event const * event = eventsSource();
    uint16_t                 type;
    uint16_t                 i;
    struct abs_time_ms       now = {  .unix_time = TimeGetUtcTime(),
                                      .ms_time   = TimeGetUtcTimeMs()
                                   };

    for (i = 0; i < FGC_NUM_EVENT_SLOTS; i++, event++)
    {
        type = event->type;

        // The gateways sends the same event in 5 consecutive fieldbus cycles, that is
        // 5 x 20 ms = 100 ms. Omit events of the same type that that are within that 
        // time window.

        if (   type < FGC_NUM_EVENT_TYPES
            && events_funcs[type] != NULL
            && (uint32_t)AbsTimeMsDiff(&lastEventTime[type], &now) >= (uint32_t)100)
        {
            lastEventTime[type] = now;

            events_funcs[type](event);
        }
    }
}



void eventsDelayCorrection(uint32_t * delay)
{
    /*
     *  In order to  calculate the delay till event one has to take into
     *  account the following:
     *
     *  When using Ethernet, the event is sent at millisecond 18/19, whilst the
     *  processing is done at millisecond 12 of the next fieldbus cycle. The
     *  parameter time_till_event_ms refers to the beginning of the fieldbus cycle
     *  when the event is sent by the gateway. Therefore there is an offset of
     *  20 + 12 = 32 ms.
     *
     *     ^                       ^                     ^
     *  _ _|_______________|___|___|___________|_________|_____ _ _ _ _ ___*|*___ _ _
     *     0              18  19 20/0 ms.      12         20/0 ms.
     *                  Event sent        Event processed                 Event
     *     <--------------------------- time_till_event_ms ----------------->
     *     <------------- offset -------------->
     *
     */

    // Do not compensate if the events are being simulated since the
    // event times are already correct.

    if (EventsSimIsEnabled() == false)
    {
        *delay -= (mst.ms_mod_20 + 20);
    }
}



// Internal function definitions

static void eventsProcessPostMortem(struct fgc_event const * event)
{
    Set(fbs.u.fieldbus_stat.ack, FGC_EXT_PM_REQ);
}



static void eventsProcessStart(struct fgc_event const * event)
{
    eventsClassProcessStart(event);
}



static void eventsProcessAbort(struct fgc_event const * event)
{
    eventsClassProcessAbort(event);
}



static void eventsProcessSSC(struct fgc_event const * event)
{
    if (dpcom.mcu.evt.ssc_f == false)
    {
        dpcom.mcu.evt.ssc_f = true;

        // -length_bp -> SSC

        timing_log.user[timing_log.in_idx] = -timing_log.user[timing_log.in_idx];
    }
}



static void eventsProcessCycleTagHigh(struct fgc_event const * event)
{
    dpcom.mcu.evt.cycle_tag.byte[2] = event->payload;
}



static void eventsProcessCycleTagLow(struct fgc_event const * event)
{
    dpcom.mcu.evt.cycle_tag.byte[3] = event->payload;
}



static void  eventsProcessInjection(struct fgc_event const * event)
{
    eventsClassProcessInjection(event);
}



static void  eventsProcessExtraction(struct fgc_event const * event)
{
    eventsClassProcessExtraction(event);
}



static void eventsProcessCycleStart(struct fgc_event const * event)
{
    // Only process the start cycle event if the previous cycle has completed

    if (CycTimeGetTimeTillC0() > 0)
    {
        return;
    }

    // Inform the DSP of the next cycle data

    dpcom.mcu.evt.next_user          = event->payload;
    dpcom.mcu.evt.next_user_delay_ms = event->delay_ms;

    // Compensate the timing event delay

    uint32_t delay = event->delay_ms;

    eventsDelayCorrection(&delay);

    // Set up the TimeTillC0 register to start counting down as soon as possible

    CycTimeSetTimeTillC0(delay);

    eventsClassProcessCycleStart(event);
}



static struct fgc_event const * eventsSource(void)
{
    /*
     * This function checks whether simulated or real events are to be used based on
     * the following priority:
     * 1) If the property PROP_FGC_EVENT_SIM has been set, use its values even if that
     *    means overwriting the Gateway timing events.
     * 2) Timing events from the Gateway.
     */

    // Switch in or out of simulation on cycle boundaries.

    if (CycTimeGetTimeTillC0() == 0)
    {
        if (PROP_FGC_CYC_SIM.n_elements != 0)
        {
            EventsSimEnable();
        }
        else if (fbs.time_rcvd_f)
        {
            EventsSimDisable();
        }
    }

    return (EventsSimIsEnabled() ? EventsSimProcess() : fbs.time_v.event);
}


// EOF
