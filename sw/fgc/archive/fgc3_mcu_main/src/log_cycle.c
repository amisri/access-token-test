/*!
 *  @file     log_cycle.c
 *  @defgroup FGC3:MCU
 *  @brief    Functionality for the log for cycling-related information.
 */

#define LOG_CYCLE_GLOBALS

// Includes

#include <log_cycle.h>
#include <string.h>
#include <fbs_class.h>
#include <log_event.h>
#include <mem.h>
#include <memmap_mcu.h>
#include <os_hardware.h>
#include <pc_state.h>
#include <sta.h>
#include <time_fgc.h>
#include <defprops.h>
#include <fgc_errs.h>



// Types

typedef struct log_evt_rec log_t;

// Internal function declarations

static log_t    *   CycleLogGetNextEntry(void);
static INLINE void  CycleLogCopyTimeStamp(log_t * const log,
                                          struct abs_time_us const * const timestamp);
static INLINE void  CycleLogCopyName(log_t * const log, char const * const name);
static INLINE void  CycleLogCopyAction(log_t * const log, char const * const action);
static INLINE void  CycleLogCopyValue(log_t * const log,
                                      log_cycle_value_type_t type,
                                      void const * const value);

// Internal variable definitions

static struct                                   // Internal variables
{
    INT16U    index;                            // Index of the last log inserted.
} cycle_log =
{
    .index = 0
};

// Cycle log actions field. String length: 23 + \0.

static char const * const log_action[] =
{
    "CHANGED",
    "SET",
    "CLEAR",
    "RISE",
    "FALL",
    "RCV",
    "NOW"
};

// Internal function definitions

static log_t * CycleLogGetNextEntry(void)
{
    OS_CPU_SR  cpu_sr;
    INT16U     index;

    // Protect the selection of a slot in the circular buffer since this function might be
    // invoked from different threads of execution.

    OS_ENTER_CRITICAL();

    if (++cycle_log.index >= FGC_CYCLE_LOG_LEN)
    {
        cycle_log.index = 0;
    }

    index = cycle_log.index;
    shared_mem.logCycle_index = cycle_log.index;

    OS_EXIT_CRITICAL();

    return ((log_t *)(SRAM_LOG_CYC_EVT_32 + sizeof(log_t) * index));
}

static INLINE void CycleLogCopyTimeStamp(log_t * const log, struct abs_time_us const * const timestamp)
{
    log->timestamp = *timestamp;
}

static INLINE void CycleLogCopyName(log_t * const log, char const * const name)
{
    snprintf(log->prop, FGC_LOG_EVT_PROP_LEN, ">%s", name);
}

static INLINE void CycleLogCopyAction(log_t * const log, char const * const action)
{
    strncpy(log->action, action, FGC_LOG_EVT_ACT_LEN);
}

static INLINE void CycleLogCopyValue(log_t * const log,
                                     log_cycle_value_type_t type,
                                     void const * const value)
{
    switch (type)
    {
        case LOG_VALUE_TYPE_INT32S:
            sprintf(log->value, "%ld", *(INT32S *)value);
            break;

        case LOG_VALUE_TYPE_INT32U:
            sprintf(log->value, "%lu", *(INT32U *)value);
            break;

        case LOG_VALUE_TYPE_HEX:
            sprintf(log->value, "%08X", *(unsigned int *)value);
            break;

        case LOG_VALUE_TYPE_FLOAT:
            sprintf(log->value, "%f", (double)(*(float *)(value)));
            break;

        case LOG_VALUE_TYPE_CHAR:
            log->value[0] = *(char *)(value);
            log->value[1] = '\0';
            break;

        case LOG_VALUE_TYPE_STRING:
            strncpy(log->value, (char const *)(value), FGC_LOG_EVT_VAL_LEN);
            log->value[FGC_LOG_EVT_VAL_LEN - 1] = '\0';
            break;

        default:
            break;
    }
}

// External function definitions

void LogCycleInit(void)
{
    INT16U  i;
    INT32U  unix_time;
    INT32U  low_limit;
    INT32U  high_limit;
    log_t * evt_rec_fp;

    cycle_log.index = shared_mem.logCycle_index;

    if (cycle_log.index < FGC_CYCLE_LOG_LEN)
    {
        evt_rec_fp = (log_t *)SRAM_LOG_CYC_EVT_32;
        high_limit = TimeGetUtcTime();
        low_limit  = high_limit - 2500000;                          // Last month only

        for (i = 0; i < FGC_CYCLE_LOG_LEN; ++i, ++evt_rec_fp)
        {
            unix_time = evt_rec_fp->timestamp.unix_time;

            if (unix_time != 0 && ((unix_time < low_limit) || (unix_time > high_limit)))
            {
                cycle_log.index = FGC_CYCLE_LOG_LEN;                // Flag event log as corrupted
                break;
            }
        }
    }

    if (cycle_log.index >= FGC_CYCLE_LOG_LEN)                      // If log was found to be corrupted
    {
        MemSetWords((void *) SRAM_LOG_CYC_EVT_32, 0x0000, SRAM_LOG_CYC_EVT_W);    // Clear Event log
        cycle_log.index = shared_mem.logCycle_index = 0;            // Reset log index
    }
}
void LogCycleInsertUs(char const * const name,
                      struct abs_time_us const * const timestamp,
                      log_cycle_value_type_t type,
                      void const * const value,
                      log_cycle_action_t action)
{
    log_t * log;

    log = CycleLogGetNextEntry();
    CycleLogCopyTimeStamp(log, timestamp);
    CycleLogCopyName(log, name);
    CycleLogCopyAction(log, log_action[action]);
    CycleLogCopyValue(log, type, value);
}

void LogCycleInsertMs(char const * const name,
                      struct abs_time_ms const * const timestamp,
                      log_cycle_value_type_t type,
                      void const * const value,
                      log_cycle_action_t action)
{
    struct abs_time_us timestamp_us;

    AbsTimeCopyFromMsToUs(timestamp, &timestamp_us);
    LogCycleInsertUs(name, &timestamp_us, type, value, action);
}

void LogCycleInsert(char const * const name,
                    log_cycle_value_type_t type,
                    void const * const value,
                    log_cycle_action_t action)
{
    struct abs_time_us now;
    now.unix_time = TimeGetUtcTime();
    now.us_time   = TimeGetUtcTimeUs();

    LogCycleInsertUs(name, &now, type, value, action);
}

char * const LogCycleGetValBuf(void)
{
    static char log_buffer[FGC_LOG_EVT_VAL_LEN];

    return log_buffer;
}

char * const LogCycleGetPropBuf(void)
{
    static char log_buffer[FGC_LOG_EVT_PROP_LEN];

    return log_buffer;
}


INT16U LogCycleGetIdx(void)
{
    return (cycle_log.index);
}

// EOF
