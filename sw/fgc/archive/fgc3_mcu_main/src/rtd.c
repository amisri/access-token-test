/*!
 *  @file      rtd.c
 *  @defgroup  FGC3:MCU
 *  @brief     Global option line display Layout (DIAG, PSU)
 *                         1         2         3         4         5         6         7         8
 *               +---------+---------+---------+---------+---------+---------+---------+---------++
 *    DIAG   20  |aa:nnnn aa:xnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn |
 *    PSU    20  |+5: n.nn    +15: nn.nn  -15:-nn.nn                                              |
 *               +---------+---------+---------+---------+---------+---------+---------+---------++
 */

#define RTD_GLOBALS

// Includes

#include <rtd.h>

#include <cal.h>
#include <class.h>
#include <cmd.h>
#include <defprops.h>
#include <diag.h>
#include <dls.h>
#include <fgc_pc_fsm.h>
#include <math.h>
#include <mst.h>
#include <os.h>
#include <read_PSU_adc.h>
#include <serial_stream.h>
#include <string.h>
#include <task_trace.h>

// Internal function declarations

/*!
 * This function will display a 16 bit signed value on the screen.
 */
static BOOLEAN RtdInt16s(INT16S * source, INT16S * last, const char * fmt, const char * pos);

/*!
 * This function will display a 32 unsigned integer value on the screen.
 */
// Not used for now but maybe in the future?
// static BOOLEAN RtdInt32u(INT32U * source, INT32U * last, char * fmt, char * pos);

/*!
 * This function will display a measured temperature value on the screen.
 */
static BOOLEAN RtdTemp(INT16U temperature, INT16U * previous_temperature_ptr, const char * pos);

/*!
 * This function will display a float value on the screen. The function checks
 * if the string value of the float has changed before displaying it.
 */
static BOOLEAN RtdFloat(FP32 * source, FP32 * last, char * last_s, const char * fmt, const char * pos);

/*!
 * This function will display the bit status flags on the screen, either as a
 * reverse video character, or as the choice between two different characters.
 */
static BOOLEAN RtdFlags(void);

/*!
 * This function will display the measured FGC outlet temperature value on
 * the screen.
 */
static BOOLEAN RtdTout(void);

/*!
 * This function will display the measured FGC inlet temperature value on
 * the screen.
 */
static BOOLEAN RtdTin(void);

/*!
 * This function will display the CPU usage on the screen.
 */
static BOOLEAN RtdCpuUsage(void);

/*!
 * This function will display the CPU32 usage on the screen.
 */
static BOOLEAN RtdCpu32Usage(void);

/*!
 * This function will control the display on line 20.
 */
static BOOLEAN RtdOptionLine(void);

/*!
 * This function will clear the option line and will reset the option
 * line variables
 */
static void RtdResetOptionLine(void);

/*!
 * This function will display the diagnostic data on the option line.
 */
static BOOLEAN RtdDiagList(void);

/*!
 * This function will display the extra information about the measured
 * load on the option line
 */
static BOOLEAN RtdPsu(void);

/*!
 * This function will display the device states in abbreviated form on
 * the screen.
 */
static BOOLEAN RtdStates(void);

/*!
 * This function will display the current reference value on the screen.
 */
static BOOLEAN RtdIref(void);

/*!
 * This function will display the current measurement value on the screen.
 */
static BOOLEAN RtdImeas(void);

/*!
 * This function will display the rate of change of Iref value on the screen.
 */
static BOOLEAN RtdImeasRate(void);

#if (FGC_CLASS_ID == 61)
/*!
 * This function will display the voltage reference value on the screen.
 */
static BOOLEAN RtdVref(void);
#endif

/*!
 * This function will display the voltage measurment value on the screen.
 */
static BOOLEAN RtdVmeas(void);

/*!
 * This function will display the voltage measurment value across the capacitor
 * on the screen.
 */
static BOOLEAN RtdVcapa(void);

/*!
 * This function will display the feedback error value on the screen.
 */
static BOOLEAN RtdIerr(void);

/*!
 * This function will display the ADC A voltage value (Vadc) on the screen.
 */
static BOOLEAN RtdVa(void);

/*!
 * This function will display the ADC B voltage value (Vadc) on the screen.
 */
static BOOLEAN RtdVb(void);

/*!
 * This function will display the ADC C voltage value (Vadc) on the screen.
 */
static BOOLEAN RtdVc(void);

/*!
 * This function will display the ADC D voltage value (Vadc) on the screen.
 */
static BOOLEAN RtdVd(void);

/*!
 * This function will display the feedback error value on the screen.
 */
static BOOLEAN RtdIdiff(void);

/*!
 * This function will display the reference function type
 */
static BOOLEAN RtdRef(void);

/*!
 * This function will display the extra information about the ADC 1 & 2 signals
 * on the option line.
 */
static BOOLEAN RtdAdcs(void);

/*!
 * This function will display the extra information about the ADC 1 & 4 signals
 * on the option line.
 */
static BOOLEAN RtdAdc14(void);

/*!
 * This function will display the extra information about the ADC 2 & 4 signals
 * on the option line.
 */
static BOOLEAN RtdAdc24(void);

/*!
 * This function will display the extra information about the ADC 3 & 4 signals
 * on the option line.
 */
static BOOLEAN RtdAdc34(void);

/*!
 * This function will display the extra information about the current cycle on
 * the option line.
 */
static BOOLEAN RtdCyc(void);

// Internal variable definitions

BOOLEAN(*rtd_func[])(void) =
{
    RtdStates,                                  // Display states
    RtdFlags,                                   // Flag characters
    RtdIref,                                    // Display current reference value
    RtdImeas,                                   // Display current measurement value
    RtdImeasRate,                               // Display rate of change of the Imeas value
    RtdIdiff,                                   // Display current difference value
#if (FGC_CLASS_ID == 61)
    RtdVref,                                    // Display voltage reference value
#endif
    RtdVmeas,                                   // Display voltage measurement value
    RtdVcapa,                                   // Display voltage measurement value across the capacitor
    RtdIerr,                                    // Display feedback error term value
    RtdVa,                                      // Display ADC A voltage value (Vadc)
    RtdVb,                                      // Display ADC B voltage value (Vadc)
    RtdVc,                                      // Display ADC C voltage value (Vadc)
    RtdVd,                                      // Display ADC D voltage value (Vadc)
    RtdTout,                                    // Display outlet temperature
    RtdTin,                                     // Display inlet temperature
    RtdRef,                                     // Display reference function type and range
    RtdCpuUsage,                                // CPU usage
    RtdCpu32Usage,                              // CPU32 usage
    RtdOptionLine,                              // Option line
    RtdOptionLine,                              // Option line
    0
};

struct rtd_flags rtd_flags[] =
{
    { &FAULTS,              FGC_FLT_FGC_HW,                 { '#', 'H'}, "22;5"  },         // 1
    { &FAULTS,              FGC_FLT_I_MEAS,                 { '#', 'M'}, "22;6"  },         // 2
    { &FAULTS,              FGC_FLT_REG_ERROR,              { '#', 'F'}, "22;7" },          // 3
    { &FAULTS,              FGC_FLT_VS_RUN_TO,              { '#', 'T'}, "22;8"  },         // 4
    { &FAULTS,              FGC_FLT_VS_FAULT,               { '#', 'V'}, "22;9"  },         // 5
    { &FAULTS,              FGC_FLT_VS_EXTINTLOCK,          { '#', 'E'}, "22;10" },         // 6
    { &FAULTS,              FGC_FLT_LIMITS,                 { '#', 'L'}, "22;11" },         // 7

    { &WARNINGS,            FGC_WRN_FGC_HW,                 { '#', 'H'}, "22;13" },         // 8
    { &WARNINGS,            FGC_WRN_I_MEAS,                 { '#', 'M'}, "22;14" },         // 9
    { &WARNINGS,            FGC_WRN_REG_ERROR,              { '#', 'F'}, "22;15" },         // 10
#if (FGC_CLASS_ID == 61)
    { &WARNINGS,            FGC_WRN_SUBCONVTR_FLT,          { '#', 'S'}, "22;16" },         // 11
#endif
    { &WARNINGS,            FGC_WRN_FGC_PSU,                { '#', 'P'}, "22;17" },         // 12
#if (FGC_CLASS_ID == 61)
    { &WARNINGS,            FGC_WRN_V_ERROR,                { '#', 'G'}, "22;18" },         // 13
#endif
    { &WARNINGS,            FGC_WRN_CONFIG,                 { '#', 'C'}, "22;19" },         // 14

    { &ST_UNLATCHED,        FGC_UNL_POL_SWI_POS,            { '#', 'P'}, "23;10" },         // 15
    { &ST_UNLATCHED,        FGC_UNL_POL_SWI_NEG,            { '#', 'N'}, "23;11" },         // 16

    // trick to access via INT16U the DCCT that is INT8U
    { (INT16U *)(&ST_DCCT_A - 1), FGC_DCCT_FAULT,           { '#', 'A'}, "23;12" },         // 17
    { (INT16U *)(&ST_DCCT_B - 1), FGC_DCCT_FAULT,           { '#', 'B'}, "23;13" },         // 18

    { &ST_UNLATCHED,        FGC_UNL_POST_MORTEM,            { '#', 'M'}, "23;14" },         // 19

    { &sta.config_state,    FGC_CFG_STATE_SYNCHRONISED,     { '~', 'U'}, "23;16" },         // 20
    { &sta.config_mode,     FGC_CFG_MODE_SYNC_FGC,          { '#', 'F'}, "23;17" },         // 21
    { &sta.config_mode,     FGC_CFG_MODE_SYNC_DB,           { '#', 'D'}, "23;18" },         // 22
    { &sta.config_mode,     FGC_CFG_MODE_SYNC_DB_CAL,       { '#', 'C'}, "23;19" },         // 23
    { &sta.config_mode,     FGC_CFG_MODE_SYNC_FAILED,       { '#', 'F'}, "23;20" },         // 24

    { (INT16U *)(&ST_MEAS_A - 1), FGC_ADC_STATUS_IN_USE,    { '#', 'a'}, "21;57" },         // 25
    { (INT16U *)(&ST_MEAS_B - 1), FGC_ADC_STATUS_IN_USE,    { '#', 'b'}, "22;57" },         // 26
    { (INT16U *)(&ST_MEAS_A - 1), FGC_ADC_STATUS_V_MEAS_OK, { ' ', '*'}, "21;69" },         // 27
    { (INT16U *)(&ST_MEAS_B - 1), FGC_ADC_STATUS_V_MEAS_OK, { ' ', '*'}, "22;69" },         // 28
    { (INT16U *)(&ST_MEAS_C - 1), FGC_ADC_STATUS_V_MEAS_OK, { ' ', '*'}, "23;69" },         // 29
    { (INT16U *)(&ST_MEAS_D - 1), FGC_ADC_STATUS_V_MEAS_OK, { ' ', '*'}, "24;69" },         // 30

#if (FGC_CLASS_ID == 61)
    // trick to access via INT16U the reg_mode that is INT32U
    {(INT16U *)(&dpcom.dsp.cyc.reg_mode) + 1, FGC_REG_I,    { '#', 'I'}, "21;25" },      // 31
    {(INT16U *)(&dpcom.dsp.cyc.reg_mode) + 1, FGC_REG_I,    { '~', 'V'}, "21;41" },      // 32

    { &WARNINGS,            FGC_WRN_ILIM_EXPECTED,          { '#', 'I'}, "22;20" },         // 33
    { &WARNINGS,            FGC_WRN_REF_LIM,                { '#', 'L'}, "22;21" },         // 34
    { &WARNINGS,            FGC_WRN_REF_RATE_LIM,           { '#', 'R'}, "22;22" },         // 35
#endif
    { &ST_UNLATCHED,        FGC_UNL_PC_PERMIT,              { '#', 'P'}, "23;5" },          // 36
    { &FAULTS,              FGC_FLT_FAST_ABORT,             { '#', 'F'}, "23;6"  },         // 37
    { &ST_UNLATCHED,        FGC_UNL_PC_DISCH_RQ,            { '#', 'D'}, "23;7"  },         // 38
    { &ST_UNLATCHED,        FGC_UNL_PWR_FAILURE,            { '#', 'P'}, "23;8"  },         // 39
    { NULL },                                       // # = Reverse flag if 1, ~ = Reverse flag if 0
};

// Internal function definitions

static BOOLEAN RtdInt16s(INT16S * source, INT16S * last, const char * fmt, const char * pos)
{
    INT16S  value;

    value = *source;            // Take local copy of value

    if (value != *last)         // If value has changed since last display
    {
        *last = value;                  // Take "last" copy

        fprintf(rtd.f, RTD_START_POS, pos); // Move cursor
        fprintf(rtd.f, fmt, value);         // Display float value
        fputs(RTD_END, rtd.f);              // Restore cursor position

        return (TRUE);
    }

    return (FALSE);
}

// Not used for now but maybe in the futur
//static BOOLEAN RtdInt32u(INT32U * source, INT32U * last, char * fmt, char * pos)
//{
//    INT32U  value;
//
//    value = *source;            // Take local copy of value
//
//    if (value != *last)         // If value has changed since last display
//    {
//        *last = value;                  // Take "last" copy
//
//        fprintf(rtd.f, RTD_START_POS, pos);     // Move cursor
//        fprintf(rtd.f, fmt, value);         // Display float value
//        fputs(RTD_END, rtd.f);              // Restore cursor position
//
//        return (TRUE);
//    }
//
//    return (FALSE);
//}

static BOOLEAN RtdTemp(INT16U temperature, INT16U * previous_temperature_ptr, const char * pos)
{
    if (temperature != *previous_temperature_ptr)
    {
        *previous_temperature_ptr = temperature;

        fprintf(rtd.f, RTD_START_POS, pos);
        fprintf(rtd.f, "%2u.%02u", (temperature >> 4), ID_TEMP_FRAC_C(temperature));
        fputs(RTD_END, rtd.f);

        return (TRUE);
    }

    return (FALSE);
}

static BOOLEAN RtdFloat(FP32 * source, FP32 * last, char * last_s, const char * fmt, const char * pos)
{
    OS_CPU_SR       cpu_sr;
    FP32            value;
    static char     str[20];        // String buffer. This buffer is sufficient to print floating-point values
    // in range [-1.0E10; +1.0E10] with 6 decimal places.

    OS_ENTER_CRITICAL();
    value = *source;            // Take local copy of value
    OS_EXIT_CRITICAL();


    if (fabs(value) > 1.0E+10)      // Clip max value to avoid crashing due to overruns of the string buffer
    {
        value = (value > 0.0 ? 1.0E+10 : -1.0E-10);
    }

    if (value != *last)         // If value has changed since last display
    {
        sprintf(str, fmt, value);       // Write float value to temporary buffer
        str[10] = '\0';             // Clip to 10 characters

        if (strcmp(last_s, str) == 0)   // == 0 means match
        {
            return (FALSE);                             // Return false
        }
        else
        {
            strcpy(last_s, str);
        }

        *last = value;                  // Take "last" copy of numeric value

        fprintf(rtd.f, RTD_START_POS, pos);     // Move cursor
        fputs((char *)str, rtd.f);           // Write string
        fputs(RTD_END, rtd.f);              // Restore cursor position

        return (TRUE);
    }

    return (FALSE);
}

static BOOLEAN RtdFlags(void)
{
    /*
     * If the previous call detected a flag that had toggled then that flag
     * will not be check on this call. If no flags changed on the previous
     * call then all flags will be check on this call. This behaviour prevents
     * the function from blocking the RTD in the event that a flag is toggling
     * constantly since the function will make sure that it is recalled
     * immediately if a flag is found to have toggled. This is used to ensure
     * that all flags are written quickly when the RTD is enabled.
     */

    BOOLEAN     flag_off;
    char    *   flags;
    INT16U      var;
    INT16U      flag_end_idx;

    do                      // Loop for all flags
    {
        flag_end_idx = rtd.flag_idx;

        if (!(rtd_flags[++rtd.flag_idx].var))       // Adjust flag index to check next flag
        {
            rtd.flag_idx = 0;
        }

        var = *rtd_flags[rtd.flag_idx].var;     // Get flag variable

        flag_off = !(var & rtd_flags[rtd.flag_idx].mask); // Extract bit(s) identified by mask

        if (flag_off != rtd.last.flag_off[rtd.flag_idx]) // If flag has changed
        {
            rtd.last.flag_off[rtd.flag_idx] = flag_off;             // Save new flag
            fprintf(rtd.f, RTD_START_POS, rtd_flags[rtd.flag_idx].pos);     // Move cursor
            flags = (char *) &rtd_flags[rtd.flag_idx].flag;

            switch (flags[0])               // Act on flag control character
            {
                case '#':                       // Reverse video if flag is 1

                    if (!flag_off)                      // If flag is active
                    {
                        fputs(TERM_REVERSE, rtd.f);                 // Enable reverse video
                    }

                    SerialStreamInternal_WriteChar(flags[1], rtd.f);            // Write flag character
                    break;

                case '~':                       // Reverse video if flag is 0

                    if (flag_off)                       // If flag is inactive
                    {
                        fputs(TERM_REVERSE, rtd.f);                 // Enable reverse video
                    }

                    SerialStreamInternal_WriteChar(flags[1], rtd.f);            // Write flag character
                    break;

                default:                        // Normal video with two flag characters

                    SerialStreamInternal_WriteChar(flags[flag_off], rtd.f);             // Write flag character
                    break;
            }

            fputs(RTD_END, rtd.f);                  // Restore cursor position
            rtd.flag_end_idx = flag_end_idx;                // Set end index for next iteration
            rtd.idx--;                          // Return to flags next time
            return (TRUE);                      // Report that field has been written
        }

    }
    while (rtd.flag_idx != rtd.flag_end_idx);           // until all flags checked

    return (FALSE);                 // Report that field has not been written
}

static BOOLEAN RtdTout(void)
{
    return (RtdTemp(temp.fgc.out, &rtd.last.tout, RTD_TOUT_POS));
}

static BOOLEAN RtdTin(void)
{
    return (RtdTemp(temp.fgc.in, &rtd.last.tin, RTD_TIN_POS));
}

static BOOLEAN RtdCpuUsage(void)
{
    INT16S usage = mst.cpu_usage.mcu;

    if (usage > 99)         // Clip at 99%!
    {
        usage = 99;
    }

    return (RtdInt16s(&usage, &rtd.last.cpu_usage, "%02d", RTD_CPUUSE_POS));
}

static BOOLEAN RtdCpu32Usage(void)
{
    INT16S usage = mst.cpu_usage.dsp;

    if (usage > 99)         // Clip at 99%!
    {
        usage = 99;
    }

    return (RtdInt16s(&usage, &rtd.last.cpu32_usage, "%02d", RTD_CPU32USE_POS));
}

static BOOLEAN RtdOptionLine(void)
{
    /*
     * This function will control the display on line 20 of:   -----    Class     ------
     *                                                             51      53      59     61
     * 0. OFF    = Nothing                                         Y       Y       Y      Y
     * 1. DIAG   = Diagnostic data                                 Y       Y       Y      Y
     * 2. PROT   = Measured current lead voltages and Iearth       Y       N       N      N
     * 3. LOAD   = Measured load                                           (deleted)
     * 4. ADCS   = ADC1 & ADC2 data                                Y       Y       N      Y
     * 5. PSU    = Measured FGC PSU voltages                       Y       Y       Y      Y
     * 6. CYC    = Cycling & PPM data                              N       Y       N      Y
     * 7. ADC14  = ADC1 & ADC4 data                                N       N       N      Y
     * 8. ADC24  = ADC2 & ADC4 data                                N       N       N      Y
     * 9. ADC34  = ADC3 & ADC4 data                                N       N       N      Y
     *
     * The choice is controlled by rtd.ctrl
     */

    if (rtd.ctrl != rtd.last_ctrl)      // If option line control has changed
    {
        rtd.last_ctrl = rtd.ctrl;           // Save option line control
        RtdResetOptionLine();               // Reset option line
        return (TRUE);                  // Report that a field has been written
    }

    switch (rtd.ctrl)
    {
        case FGC_RTD_DIAG:  return (RtdDiagList());

        case FGC_RTD_PSU:   return (RtdPsu());

        case FGC_RTD_ADCS:  return (RtdAdcs());

        case FGC_RTD_CYC:   return (RtdCyc());

        case FGC_RTD_ADCS_AD:   return (RtdAdc14());

        case FGC_RTD_ADCS_BD:   return (RtdAdc24());

        case FGC_RTD_ADCS_CD:   return (RtdAdc34());
    }

    return (FALSE);
}

static void RtdResetOptionLine(void)
{
    fprintf(rtd.f, RTD_START_POS, RTD_OPTLINE_POS);     // Move cursor to option line
    fputs(TERM_CLR_LINE, rtd.f);                // Erase complete line
    fputs(RTD_END, rtd.f);                  // Restore cursor position

    memset(&rtd.optline,    0xFF, sizeof(rtd.optline)); // Reset all option line variables

    rtd.optline.diag_list_len = diag.list_rtd_len;      // Remember number of diag data fields
    rtd.optline.idx = 0;                    // Zero option line index
}

static BOOLEAN RtdDiagList(void)
{
    INT16U  chan;
    INT16U  data;
    char    pos[6];

    if (diag.list_rtd_len != rtd.optline.diag_list_len)     // If the diag chan list length has changed
    {
        RtdResetOptionLine();                       // Reset option line
        return (TRUE);
    }

    if (!diag.list_rtd_len)                 // If diag chan list is empty
    {
        return (FALSE);                         // Report no field written
    }

    rtd.optline.idx++;

    if (rtd.optline.idx >= diag.list_rtd_len)           // Pre increment index to next field
    {
        rtd.optline.idx = 0;
    }

    chan = diag.list_rtd[rtd.optline.idx];

    if (chan != rtd.optline.diag_chan[rtd.optline.idx])         // If channel address address has changed
    {
        rtd.optline.diag_chan[rtd.optline.idx] = chan;      // Remember new channel
    }
    else
    {
        // else if data has not changed
        // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)
        if (diag.data.w[chan] == rtd.optline.diag_data[rtd.optline.idx])
        {
            return (FALSE);                         // Report that no field was written
        }
    }

    // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)
    data = diag.data.w[chan];                                     // Get new data
    rtd.optline.diag_data[rtd.optline.idx] = data;      // Save new data

    sprintf(pos, "%u;%u", (RTD_OPTLINE_VPOS), (RTD_DIAGDATA_HPOS + RTD_DIAGDATA_HWIDTH * rtd.optline.idx));

    fprintf(rtd.f, RTD_START_POS, (char *)pos);      // Move cursor

    if (chan < DIAG_MAX_ANALOG)                 // If data is analogue
    {
        data &= 0xFFF;
        fprintf(rtd.f, "%02X:%-4.0f", chan,             // Display address in hex and
                ((FP32)data * DIAG_ANALOG_CAL));           // data in millivolts
    }
    else
    {
        if (chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL)) // else if data is digital
        {
            data &= 0xFFF;
            fprintf(rtd.f, "%02X:x%03X", chan, data);           // Display address and data in hex
        }
        else                            // else data is software value
        {
            fprintf(rtd.f, "%02X:%04X", chan, data);            // Display address and data in hex
        }
    }

    fputs(RTD_END, rtd.f);                  // Restore cursor position

    return (TRUE);
}

static BOOLEAN RtdPsu(void)
{
    INT16U              psu_idx   = rtd.optline.idx;
    char                pos[6];
    static char    *    psu_fmt[] = { "+5:%5.2f", "+15:%6.2f", "-15:%6.2f" };
    FP32                psu_value;

    do
    {
        rtd.optline.idx++;

        if (rtd.optline.idx > 2)
        {
            rtd.optline.idx = 0;
        }

        sprintf(pos, "%u;%u", (RTD_OPTLINE_VPOS), (RTD_PSU_HPOS + RTD_PSU_HWIDTH * rtd.optline.idx));

        // FGC3
        switch (rtd.optline.idx)
        {
            case 0: psu_value = psu.v5;
                break;

            case 1: psu_value = psu.vp15;
                break;

            case 2: psu_value = psu.vm15;
                break;
        }

        // for Rx610 the warning came from the volatile attribute in dpcom variable
        if (RtdFloat(&psu_value ,
                     &(rtd.optline.psu[rtd.optline.idx]) ,
                     &rtd.optline.float_s[rtd.optline.idx][0],
                     psu_fmt[rtd.optline.idx],
                     pos)
           )
        {
            return (TRUE);
        }
    }
    while (psu_idx != rtd.optline.idx);

    return (FALSE);
}

static BOOLEAN RtdStates(void)
{
    char const   *  p;
    static char     pll_str[] = "LKCPFSNSFL";               // FGC.PLL.STATE
    static char     op_str[]  = "UCNLSMCLTTBTPRSO";         // STATE.OP
    static char     vs_str[]  = "IVFOPAOFFSSPSTRDBK";       // VS.STATE

    if (STATE_PLL != rtd.last.state_pll    ||           // If a state has changed
        STATE_OP  != rtd.last.state_op     ||
        STATE_VS  != rtd.last.state_vs ||
        STATE_PC  != rtd.last.state_pc)
    {
        rtd.last.state_pll     = STATE_PLL;   // Remember states
        rtd.last.state_op      = STATE_OP;
        rtd.last.state_vs  = STATE_VS;
        rtd.last.state_pc  = STATE_PC;
        fprintf(rtd.f, RTD_START_POS, RTD_STATES_POS);      // Move cursor

        p = pll_str + 2 * rtd.last.state_pll;           // Display PLL state
        SerialStreamInternal_WriteChar(*(p++), rtd.f);
        SerialStreamInternal_WriteChar(*p, rtd.f);

        SerialStreamInternal_WriteChar('.', rtd.f);
        p = op_str + 2 * rtd.last.state_op;         // Display operational state
        SerialStreamInternal_WriteChar(*(p++), rtd.f);
        SerialStreamInternal_WriteChar(*p, rtd.f);

        if (rtd.last.state_op != FGC_OP_UNCONFIGURED)       // If operational state is not UNCONFIGURED
        {
            SerialStreamInternal_WriteChar('.', rtd.f);
            p = vs_str + 2 * rtd.last.state_vs;         // Display voltage source state
            SerialStreamInternal_WriteChar(*(p++), rtd.f);
            SerialStreamInternal_WriteChar(*p, rtd.f);

            SerialStreamInternal_WriteChar('.', rtd.f);

            p = pc_str + 2 * rtd.last.state_pc;         // Display power converter state
            SerialStreamInternal_WriteChar(*(p++), rtd.f);
            SerialStreamInternal_WriteChar(*p, rtd.f);
        }

        fputs(RTD_END, rtd.f);                  // Restore cursor position

        return (TRUE);
    }

    return (FALSE);
}


static BOOLEAN RtdIref(void)
{
#if (FGC_CLASS_ID == 61)
    return (RtdFloat((FP32 *) &I_REF, &rtd.last.iref, (char *) &rtd.last.iref_s, "%9.2f", RTD_IREF_POS));
#elif (FGC_CLASS_ID == 62)
    return (RtdFloat((FP32 *) &REF, &rtd.last.iref, (char *) &rtd.last.iref_s, "%9.2f", RTD_IREF_POS));
#endif
}

static BOOLEAN RtdImeas(void)
{
    return (RtdFloat(&I_MEAS,
                     &rtd.last.imeas,
                     (char *) &rtd.last.imeas_s,
                     "%9.2f",
                     RTD_IMEAS_POS)
           );
}

#if (FGC_CLASS_ID == 61)
static BOOLEAN RtdVref(void)
{
    return (RtdFloat(&V_REF, &rtd.last.vref, (char *) &rtd.last.vref_s, "%9.2f", RTD_VREF_POS));
}
#endif

static BOOLEAN RtdVmeas(void)
{
    return (RtdFloat(&V_MEAS,
                     &rtd.last.vmeas,
                     (char *) &rtd.last.vmeas_s,
                     "%9.2f",
                     RTD_VMEAS_POS)
           );
}

static BOOLEAN RtdVcapa(void)
{
    return (RtdFloat((FP32 *) &dpcls.dsp.meas.v_capa,
                     &rtd.last.vcapa,
                     (char *) &rtd.last.vcapa_s,
                     "%9.2f",
                     RTD_VCAPA_POS)
           );
}

static BOOLEAN RtdVa(void)
{
    return (RtdFloat((FP32 *) &dpcls.dsp.adc.volts_200ms[0],
                     &rtd.last.vadc[0],
                     (char *) &rtd.last.vadc_s[0],
                     "%10.6f",
                     RTD_VA_POS)
           );
}

static BOOLEAN RtdVb(void)
{
    return (RtdFloat((FP32 *) &dpcls.dsp.adc.volts_200ms[1],
                     &rtd.last.vadc[1],
                     (char *) &rtd.last.vadc_s[1],
                     "%10.6f",
                     RTD_VB_POS)
           );
}

static BOOLEAN RtdVc(void)
{
    return (RtdFloat((FP32 *) &dpcls.dsp.adc.volts_200ms[2],
                     &rtd.last.vadc[2],
                     (char *) &rtd.last.vadc_s[2],
                     "%10.6f",
                     RTD_VC_POS)
           );
}

static BOOLEAN RtdVd(void)
{
    return (RtdFloat((FP32 *) &dpcls.dsp.adc.volts_200ms[3],
                     &rtd.last.vadc[3],
                     (char *) &rtd.last.vadc_s[3],
                     "%10.6f",
                     RTD_VD_POS)
           );
}

static BOOLEAN RtdImeasRate(void)
{
    FP32 irate = dpcls.dsp.meas.i_rate;

    // Clip I_RATE to +/- 999.99 to avoid overflow on terminal real-time display

    irate = MAX(-999.99, irate);
    irate = MIN(irate, 999.99);

    return (RtdFloat(&irate,
                     &rtd.last.i_rate,
                     (char *) &rtd.last.i_rate_s,
                     "%9.2f",
                     RTD_IRATE_POS)
           );
}

static BOOLEAN RtdIerr(void)
{
    return (RtdInt16s(&I_ERR_MA,
                      &rtd.last.ierr,
                      "%6d",
                      RTD_IERR_POS)
           );
}

static BOOLEAN RtdIdiff(void)
{
    INT16S  i_diff_ma = dpcls.dsp.meas.i_diff_ma;


    return (RtdInt16s(&i_diff_ma,
                      &rtd.last.idiff,
                      "%6d",
                      RTD_IDIFF_POS)
           );
}

static BOOLEAN RtdRef(void)

{
    INT16U      stc_ref;
    BOOLEAN     changed_f = FALSE;

    stc_ref = REF_STC_ARMED_FUNC_TYPE;

    // Reference function type (Cycling and non-Cycling)

    if (stc_ref != rtd.last.ref_stc_type)
    {
        rtd.last.ref_stc_type = stc_ref;

        if (!stc_ref)
        {
            stc_ref = STC_NONE;
        }

        fprintf(rtd.f, RTD_START_POS, RTD_REFTYPE_POS);         // Move cursor
        fprintf(rtd.f, "%-10s", SYM_TAB_CONST[rtd.last.ref_stc_type].key.c); // Display symbol for type
        fputs(RTD_END, rtd.f);                      // Restore cursor position

        changed_f = TRUE;
    }

    return (changed_f);
}

static BOOLEAN RtdAdcxy(INT16U adc_left, INT16U adc_right)
{
    OS_CPU_SR   cpu_sr;
    INT16U      adc_pos, adc_idx;
    INT16U      adc_field;
    INT16U      hpos;
    char        pos[6];
    char        value[12];
    INT32S      raw;
    FP32        f;

    hpos = 0;

    rtd.optline.idx = (rtd.optline.idx + 1) % 8;    // Adjust index

    adc_pos   = rtd.optline.idx / 4;            // Position:    0=left, 1=right
    adc_idx   = (adc_pos ? adc_right : adc_left);       // ADC idx: 0=ADC1 ... 3=ADC4
    adc_field = rtd.optline.idx % 4;            // Field: 0=Raw, 1=Vadc, 2=PP noise, 3=Measured current/voltage

    switch (adc_field)      // Check field for specified channel
    {
        case 0:             // Raw ADC value field

            OS_ENTER_CRITICAL();
            raw = dpcls.dsp.adc.raw_1s[adc_idx];    // Take local copy of raw ADC value
            OS_EXIT_CRITICAL();

            if (raw != rtd.optline.adc_raw[adc_pos])        // If raw value has changed
            {
                hpos = RTD_ADCS_HPOS_RAW;
                rtd.optline.adc_raw[adc_pos] = raw;
                sprintf(value, "%9ld", raw);
            }

            break;

        case 1:             // Volts ADC value field

            OS_ENTER_CRITICAL();
            f = dpcls.dsp.adc.volts_1s[adc_idx];    // Take local copy of ADC volts value
            OS_EXIT_CRITICAL();

            if (f != rtd.optline.adc_volts[adc_pos])        // If volts value has changed
            {
                hpos = RTD_ADCS_HPOS_VOLTS;
                rtd.optline.adc_volts[adc_pos] = f;
                sprintf(value, "%10.6f", (double)f);
            }

            break;

        case 2:             // Peak-peak noise ADC value field

            OS_ENTER_CRITICAL();
            f = dpcls.dsp.adc.pp_volts_1s[adc_idx]; // Take local copy of ADC pp noise value
            OS_EXIT_CRITICAL();

            if (f != rtd.optline.adc_pp[adc_pos])   // If PP noise value has changed
            {
                hpos = RTD_ADCS_HPOS_PP;
                rtd.optline.adc_pp[adc_pos] = f;
                sprintf(value, "%.1E", (double)f);
            }

            break;

        case 3:             // Amps ADC value field

            OS_ENTER_CRITICAL();
            f = dpcls.dsp.adc.cal_meas_1s[adc_idx]; // Take local copy of the ADC calibrated measurement
            OS_EXIT_CRITICAL();

            if (f != rtd.optline.adc_meas[adc_pos]) // If value has changed
            {
                hpos = RTD_ADCS_HPOS_AMPS;
                rtd.optline.adc_meas[adc_pos] = f;
                sprintf(value, "%10.3f", (double)f);
            }

            break;
    }

    if (hpos)               // If field has changed
    {
        sprintf(pos, "%u;%u",               // Prepare cursor position
                (RTD_OPTLINE_VPOS),
                (hpos + RTD_ADCS_HWIDTH * adc_pos));
        fprintf(rtd.f, RTD_START_POS, (char *)pos);     // Move cursor
        fputs(value, rtd.f);                // Display value field
        fputs(RTD_END, rtd.f);                  // Restore cursor position
        return (TRUE);
    }

    return (FALSE);             // Report that field has not been written
}

static BOOLEAN RtdAdcs(void)
{
    return (RtdAdcxy(0, 1));
}

static BOOLEAN RtdAdc14(void)
{
    return (RtdAdcxy(0, 3));
}

static BOOLEAN RtdAdc24(void)
{
    return (RtdAdcxy(1, 3));
}

static BOOLEAN RtdAdc34(void)
{
    return (RtdAdcxy(2, 3));
}

static BOOLEAN RtdCyc(void)
{
    OS_CPU_SR       cpu_sr;
    INT16S          user;
    FP32            max_ref;
    INT16U          reg_mode;
    static char     ref_units[] = "VAG ";            // volts, amps, gauss, none


    OS_ENTER_CRITICAL();

    user     = dpcom.dsp.cyc.user;
    max_ref  = dpcls.dsp.ref.max;
    reg_mode = dpcls.dsp.reg.state.part.lo;

    OS_EXIT_CRITICAL();

    if (user != rtd.optline.user)
    {
        fprintf(rtd.f, RTD_START_POS "USR:%3d MX:%6.0f%c" RTD_END,
                RTD_CYC, user, (double)max_ref, ref_units[reg_mode]);

        rtd.optline.user = user;
        return (TRUE);
    }

    return (FALSE);
}

// External function definitions

void RtdInit(FILE * f)
{
    /*
     * It uses the following vt100 control codes (ESC='\33'):
     *    ESC [y;xH     Move cursor to (x,y)
     *    ESC [H        Move cursor to top left of screen
     *    ESC [1;19r    Set scroll window to lines 1 to 19
     */

    fputs("\33[21;1H", f);          // Move to RTD display area

    //    0         10        20        30        40        50        60        70        80
    //    |---------+---------+---------+---------+---------+---------+---------+---------+|

#if (FGC_CLASS_ID == 61)
    fputs("States: __.__.__.__     Iref:______.__  Vref:______.__ Va:___.______+ Tout:__.__\n\r", f);
#elif (FGC_CLASS_ID == 62)
    fputs("States: __.__.__.__      Ref:______.__                 Va:___.______+ Tout:__.__\n\r", f);
#endif
    fputs("F/W:_______/_________  Imeas:______.__ Vmeas:______.__ Vb:___.______+ Tin: __.__\n\r", f);
    fputs("I/S:  __/_____-_____   Irate:______.__  Ierr:______ "
          TERM_BOLD "mA"
          TERM_NORMAL " Vc:___.______\n\r", f);
    fputs("Fun: ______            Idiff:______ "
          TERM_BOLD "mA"
          TERM_NORMAL " Vcapa:______.__ Vd:___.______  Cpu:__:__%", f);

    fputs("\33[H\33[1;19r\v", f);       // Set up scroll window to protect RTD lines

    // Initialise RTD variables

    memset(&rtd.last,       0xFF, sizeof(rtd.last));       // Reset all last value variables

    rtd.idx = 0;                    // Reset field counter
    rtd.last.ierr  = 0x7FFF;    // Reset the INT16S values to +32767
    rtd.last.idiff = 0x7FFF;

    if (rtd.ctrl != FGC_RTD_OFF)    // If option line is active
    {
        rtd.last_ctrl = FGC_RTD_OFF;    // Reset last choice value to trigger reset
    }
}

void RtdTsk(void * unused)
{
    /*
     * It makes use of the following vt100 control codes (ESC = '\33'):
     *   ESC 7       Save cursor position and attributes
     *   ESC [y;xH   Move cursor to (x,y)
     *   ESC 8       Restore saved cursor position and attributes
     */

    // This function never exits so "local" variables are static
    // to save stack space.

    static INT16U   idx;
    static BOOLEAN  done_f;

    // Initialise FILE structures for 'rtd' stream

    rtd.f = fopen("rtd", "wb");

    if (setvbuf(rtd.f, NULL, _IONBF, 0) != 0)  // not buffered, buffer size 0
    {
        // 0   = ok
        // EOF = error
    }

    rtd.enabled = TRUE;             // RTD Display is enabled by default

    for (;;)                        // Main loop @ RTD_PERIOD_MS
    {
        TskTraceReset();

        OSTskSuspend();             // Wait for OSTskResume() from MstTsk();

        TskTraceInc();

        idx = rtd.idx;              // Take local copy of rtd index

        do
        {
            done_f = rtd_func[rtd.idx++]();     // Call RTD function for the current index value

            if (!rtd_func[rtd.idx])             // If index reaches the end
            {
                rtd.idx = 0;                    // Wrap back to start
            }
        }
        while (!done_f && rtd.idx != idx);  // while nothing displayed and all elements not checked

        TskTraceInc();
    }
}

// EOF
