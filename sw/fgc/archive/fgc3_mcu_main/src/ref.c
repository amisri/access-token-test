/*!
 *  @file     ref.c
 *  @defgroup FGC3:MCU
 *  @brief    Reference-related functions
 */

#define REF_GLOBALS

// Includes

#include <ref.h>
#include <defprops.h>
#include <fgc_errs.h>
#include <macros.h>
#include <pub.h>
#include <class.h>
#include <fbs_class.h>
#include <pars.h>
#include <set.h>
#include <mcu_dependent.h>

// Static function definitions



/*!
 * The function returns an array of pointers to properties, which fully characterize given function type.
 * Note that reg mode is never included in the list.
 *
 * @param[in] type Function type for which we want to get the properties.
 *
 * @return Array of pointers to properties, which fully characterize given function type.
 */
static struct prop ** RefGetDspProps(uint32_t type)
{
#if FGC_CLASS_ID == 61

    static struct prop *plep[] =
    {
         &PROP_REF_PLEP_FINAL,
         &PROP_REF_PLEP_ACCELERATION,
         &PROP_REF_PLEP_LINEAR_RATE,
         &PROP_REF_PLEP_EXP_TC,
         &PROP_REF_PLEP_EXP_FINAL,
         NULL
    };

    static struct prop *pppl[] =
    {
         &PROP_REF_FIRST_PLATEAU_DURATION,
         &PROP_REF_FIRST_PLATEAU_REF,
         &PROP_REF_FIRST_PLATEAU_TIME,
         &PROP_REF_PPPL_ACCELERATION1,
         &PROP_REF_PPPL_ACCELERATION2,
         &PROP_REF_PPPL_ACCELERATION3,
         &PROP_REF_PPPL_RATE2,
         &PROP_REF_PPPL_RATE4,
         &PROP_REF_PPPL_REF4,
         &PROP_REF_PPPL_DURATION4,
         NULL
    };

    static struct prop *trim[] =
    {
         &PROP_REF_TRIM_FINAL,
         &PROP_REF_TRIM_DURATION,
         NULL
    };

    static struct prop *test[] =
    {
        &PROP_REF_TEST_AMPLITUDE,
        &PROP_REF_TEST_NUM_CYCLES,
        &PROP_REF_TEST_PERIOD,
        &PROP_REF_TEST_WINDOW,
        NULL
    };

    static struct prop *table[] =
    {
        &PROP_REF_FIRST_PLATEAU_DURATION,
        &PROP_REF_FIRST_PLATEAU_REF,
        &PROP_REF_FIRST_PLATEAU_TIME,
        &PROP_REF_TABLE_FUNCTION,
        NULL
    };

    static struct prop *zero[] =
    {
        &PROP_REF_FIRST_PLATEAU_DURATION,
        &PROP_REF_FIRST_PLATEAU_REF,
        &PROP_REF_FIRST_PLATEAU_TIME,
        NULL
    };

    static struct prop *openloop[] =
    {
        &PROP_REF_OPENLOOP_FINAL,
        NULL
    };

#elif FGC_CLASS_ID == 62

    static struct *pulse[] =
    {
        &PROP_REF_PULSE_REF,
        NULL
    };

#endif

    switch (type)
    {
#if FGC_CLASS_ID == 61

        case FGC_REF_TABLE:    return table;    break;
        case FGC_REF_PLEP:     return plep;     break;
        case FGC_REF_PPPL:     return pppl;     break;
        case FGC_REF_LTRIM:    return trim;     break;
        case FGC_REF_CTRIM:    return trim;     break;
        case FGC_REF_SINE:     return test;     break;
        case FGC_REF_COSINE:   return test;     break;
        case FGC_REF_STEPS:    return test;     break;
        case FGC_REF_SQUARE:   return test;     break;
        case FGC_REF_ZERO:     return zero;     break;
        case FGC_REF_OPENLOOP: return openloop; break;

#elif FGC_CLASS_ID == 62

        case FGC_REF_PULSE:    return pulse;    break;

#endif
        default:               return NULL;     break;
    }
}



/*!
 * This function saves all non-volatile REF properties associated with the given func type in the NVS
 *
 * @param[in] c         Command structure
 * @param[in] user      User number
 * @param[in] func_type Function type in form of FGC constant FGC_REF_*
 */
static void RefStoreArmedFuncInNvs(struct cmd * c, uint16_t user, uint32_t func_type)
{
    // Get an array of all properties associated with the givent func type

    struct prop **ref_props = RefGetDspProps(func_type);

    if (ref_props != NULL)
    {
        while (*ref_props != NULL)
        {
            // If a property is non-volatile then save it in the NVS

            if (Test((*ref_props)->flags, PF_NON_VOLATILE))
            {
                NVRAM_UNLOCK();
                NvsStoreValuesForOneProperty(c, *ref_props, user);
                NVRAM_LOCK();
            }

            ref_props++;
        }

#if FGC_CLASS_ID == 61

        // Always save the new reg mode in the NVS

        NVRAM_UNLOCK();
        NvsStoreValuesForOneProperty(c, &PROP_REF_FUNC_REG_MODE, user);
        NVRAM_LOCK();

#endif
    }
}



// External function definitions



INT16U RefArm(struct cmd * c, INT16U user,
              INT32U func_type, INT16U stc_func_type)
{
    OS_CPU_SR cpu_sr;
    INT16U    dsp_status;

    // Wait for property block buffer to be free

    dsp_status = PropBufWait(c);

    if (dsp_status)
    {
        return (dsp_status);
    }

    // Determine target state for the armed function (RUNNING or CYCLING)
    // and reject invalid states

    if (Test(masks[STATE_PC], RUNNING_FUNC_STATE_MASK))
    {
        if (user == NON_PPM_USER && (STATE_PC == FGC_PC_IDLE  || (STATE_PC == FGC_PC_ARMED &&
                                                                  func_type == FGC_REF_NONE)))
        {
            dpcls.mcu.ref.func_state = FGC_PC_RUNNING;
        }
        else
        {
            return (FGC_BAD_STATE);
        }
    }
    else // CYCLING
    {
        // Does the class support CYCLING?

        if (!FGC_MAX_USER)
        {
            return (FGC_BAD_STATE);
        }

        if (user == NON_PPM_USER && DEVICE_PPM && func_type != FGC_REF_NONE)
        {
            return (FGC_BAD_STATE);
        }

        dpcls.mcu.ref.func_state = FGC_PC_CYCLING;
    }

    // Pass Arm/Disarm request to DSP

    dpcls.mcu.ref.user          = user;
    dpcls.mcu.ref.func_type     = func_type;
    dpcls.mcu.ref.stc_func_type  = stc_func_type;
    OS_ENTER_CRITICAL();

    dpcom.dsp.bg_complete_f = FALSE;
    dpcls.mcu.ref.arm_f     = TRUE;

    // Sleep till MstTsk() resumes on next ms

    while (!(INT16U)dpcom.dsp.bg_complete_f)
    {
        OSTskSuspend();
    }

    OS_EXIT_CRITICAL();

    // Record armed func type in property and set non-volatile
    // memory (NONE always for RUNNING)

    if (dpcls.mcu.ref.func.type[user] != dpcls.dsp.ref.func.type[user])
    {
        dpcls.mcu.ref.func.type[user] = dpcls.dsp.ref.func.type[user];
        c->changed_f = TRUE;
    }

    func_type = (dpcls.mcu.ref.func_state == FGC_PC_RUNNING ?
                 FGC_REF_NONE :
                 dpcls.mcu.ref.func.type[user]);

    nvsStoreBlockForOneProperty(NULL, &PROP_REF_FUNC_TYPE, (INT8U *)&func_type, 
                                0, user, 1, TRUE);

    if (dpcls.mcu.ref.func.type[user] != FGC_REF_NONE)
    {
        // Pub property REF.INFO

        PubProperty(&PROP_REF_INFO, user, FALSE);
    }

    // If the DSP response is positive, then acknowledge all properties pending
    // a write in the NVS for that user.
    // There is a design choice here, that is if one does "S REF.FUNC.TYPE(user) NONE",
    // then any change in the reference parameters will be saved in NVRAM. This
    // was done so that the end user has a mean of forcing values into the REF
    // properties. For example, the DSP will refuse to arm an empty table, so
    // the only way to clear:
    //     REF.TABLE.REF(user) is to do:
    //        S REF.TABLE.REF(user)       # Set an empty array
    //        S REF.FUNC.TYPE(user) NONE  # Clear the REF.TABLE.REF array in NVRAM too.

    // If DSP OK to arm the function

    if (!dpcom.dsp.errnum)
    {
        RefStoreArmedFuncInNvs(c, user, dpcls.mcu.ref.func.type[user]);
    }

    return (dpcom.dsp.errnum);
}

// EOF
