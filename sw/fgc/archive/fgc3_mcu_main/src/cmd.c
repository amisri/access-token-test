/*!
 *  @file     cmd.h
 *  @defgroup FGC3:MCU main
 *  @brief    Command processing functions
 *
 */

#define CMD_GLOBALS             // define fcm, tcm, pcm global variables
#define DEFPROPS_INC_ALL        // defprops.h

// Includes

#include <cmd.h>

#include <class.h>              // Include class dependent definitions
#include <dev.h>                // for dev global variable
#include <dpcls.h>              // for dpcls global variable
#include <fbs.h>                // for fbs global variable, FbsOutLong()
#include <fgc_errs.h>           // for FGC error codes
#include <get.h>                // for GetPropSize()
#include <hash.h>               // for HASH_VALUE and struct hash_entry needed by defprops.h
#include <init_class.h>
#include <log.h>                // for EVT_LOG_RESET_PROP, LogEvtSaveValue(), LogEvtCmd()
#include <macros.h>             // for Test(), Set(), Clr()
#include <memmap_mcu.h>         // for SM_UXTIME_P
#include <mst.h>                // for mst global variable
#include <prop.h>               // for PropValueGet()
#include <pub.h>                // for PubProperty(), sub global variable
#include <shared_memory.h>      // for shared_mem global variable
#include <sta.h>                // for sta global variable
#include <task_trace.h>
#include <string.h>

// Internal function declarations

/*!
 * This function is called from CmdTsk() if a set command has been received.  It will run the appropriate
 * set function if permitted.  Set commands are logged in the event log.
 * The function PropIdentifyUser() will only allow a non-zero p->mux_idx if the property is either
 * SUB_DEV or PPM.
 */
static INT16U CmdSet(struct cmd * c);

/*!
 * This function is called from CmdTsk() when a get command must be executed.
 */
static INT16U CmdGet(struct cmd * c);

/*!
 * This function is called from CmdTsk() when a get for subscription command must be executed. It must
 * clear the pending_get flag before the get.
 */
static INT16U CmdGetSub(void);

/*!
 * This function is called from CmdTsk() when a subscription must be started - sub commands
 * can only come from the fieldbus, never from the serial terminal.  If the subscription is successfully
 * set up, or is already present, it will return the sub index (index into sub table) to the gateway,
 * and will notify the PubTsk to publish the property for every user (if PPM).
 */
static INT16U CmdSub(void);

/*!
 * This function is called from CmdTsk() when a subscription must be cancelled.  This can only come from
 * the Fieldbus, never from the serial terminal.  It should never normally occur without the subscription
 * being present and if it isn't a PROTO ERROR is reported
 */
static INT16U CmdUnSub(void);

/*!
 * This function will display the range for the property in ().  The range can either be integer limits,
 * float limits or a symbol list.=
 */
static void CmdPrintRange(struct cmd * c, const struct prop * p);

/*!
 * This will print idx according to the command token delimiter (' '=Neat, ','=Direct) and the HEX get
 * option flag.
 */
static void CmdPrintIdxFormatted(struct cmd * c, INT16U idx);

/*!
 * Helper function for CmdGetChangedProps()
 */
static INT16U CmdGetChangedPropsHelper(const struct prop * p);

// External function definitions

void CmdTsk(void * init_par)
{
    OS_CPU_SR  cpu_sr;
    struct cmd * c = init_par;

    for (;;)                // Main loop - process each command
    {
        c->mux_idx       = 0;
        c->n_elements    = 0;
        c->cached_prop   = NULL;
        c->new_cmd_f     = TRUE;
        c->end_cmd_f     = FALSE;
        c->write_nvs_f   = TRUE;

        c->evt_log_state = EVT_LOG_RESET_PROP;

        TskTraceReset();

        c->errnum = PropIdentifyProperty(c);

        TskTraceInc();

        if (c->prop != &PROP_TEST_CMD_LAST_RCV)
        {
            strncpy(cmd_last_p.name, SYM_TAB_PROP[c->prop->sym_idx].key.c, MAX_CMD_TOKEN_LEN);
        }

        //        if (c->prop == &PROP_TEST_CMD_BLOCK)
        //        {
        //            MSLEEP(1000*60);
        //        }

        TskTraceInc();

        if (!(c->errnum))   // If property identified successfully
        {
            switch (c->cmd_type)                            // Switch on command type
            {
                case FGC_FIELDBUS_FLAGS_SET_CMD:                    // SET - From fieldbus or serial

                    c->errnum = CmdSet(c);
                    break;

                case FGC_FIELDBUS_FLAGS_SET_BIN_CMD:                // SET BIN - Not implemented

                    c->errnum = FGC_NOT_IMPL;
                    break;

                case FGC_FIELDBUS_FLAGS_SUB_CMD:                    // SUBSCRIBE - From fieldbus only

                    c->errnum = CmdSub();
                    break;

                case FGC_FIELDBUS_FLAGS_UNSUB_CMD:                  // UNSUBSCRIBE - From fieldbus only

                    c->errnum = CmdUnSub();
                    break;

                case FGC_FIELDBUS_FLAGS_GET_SUB_CMD:                // GET for SUB - From fieldbus only

                    c->errnum = CmdGetSub();
                    break;

                case FGC_FIELDBUS_FLAGS_GET_CMD:                    // GET - From fieldbus or serial

                    c->errnum = CmdGet(c);
                    break;
            }
        }

        TskTraceInc();

        if (c->errnum != FGC_CMD_RESTARTED)         // Unless CMD_RESTARTED error reported
        {
            c->pars_buf->pkt[c->pkt_buf_idx].n_chars = 0;   // Cancel packet
        }

        dev.cmdrcvd++;

        if (c == &fcm)                              // If fieldbus command
        {
            if (fcm.errnum == FGC_OK_NO_RSP)        // If no error
            {
                if (fbs.cmd.rsp_len == 0 && fcm.stat != FGC_RECEIVING)    // If no response or still receiving
                {
                    fcm.stat = FGC_OK_NO_RSP;                       // Set stat to end cmd
                }
                else                                // else response was generated
                {
                    OS_ENTER_CRITICAL();            // Protect against interrupts
                    Set(fbs.cmd.header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT); // Set last rsp packet flag
                    fcm.store_cb(';');              // Force last pkt (see notes above)
                    OS_EXIT_CRITICAL();
                }
            }
        }

        TskTraceInc();
    }
}

INT16U CmdNextCh(struct cmd * c, char * ch_p)
{
    char                ch;
    INT16U              idx;
    INT16U              errnum;
    struct pars_buf  *  pars_buf;
    struct cmd_pkt   *  cmd_pkt;
    char        *       ch_ptr;

    // Process pcm structure

    pars_buf = c->pars_buf;                         // local copy

    // In the case of PCM, the prop_buf pointer is hijacked to hold a char pointer to the property name.

    if (c == &pcm)                                  // If PCM command
    {
        ch_ptr      = (char *)c->prop_buf;
        ch          = *ch_ptr++;
        c->prop_buf = (struct prop_buf *)ch_ptr;

        if (ch >= 'a' && ch <= 'z')                 // Convert lower case to UPPER CASE
        {
            Clr(ch, 0x20);
        }

        c->end_cmd_f = !(*ch_p = ch);               // Set end_cmd_f if nul
        return (0);
    }

    // Process FCM and SCM commands

    if (pars_buf == NULL)
    {
        return (FGC_NULL_ADDRESS);
    }

    do
    {
        cmd_pkt = &pars_buf->pkt[c->pkt_buf_idx];   // Get address of command packet
        idx     = pars_buf->out_idx++;              // Get index of new character in packet

        if (idx < cmd_pkt->n_chars)                 // If packet is NOT empty
        {
            ch = cmd_pkt->buf[idx];                 // Get character

            if (ch >= 'a' && ch <= 'z')             // Convert lower case to UPPER CASE
            {
                Clr(ch, 0x20);
            }

            c->new_cmd_f = FALSE;                   // Clear new-cmd-pkt-expected flag
            *ch_p = ch;                             // Return character to calling function
            return (0);                             // return good status
        }

        if (!c->new_cmd_f &&                        // If not starting a new command and
            Test(cmd_pkt->header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT)) // last packet of command
        {
            c->end_cmd_f = TRUE;                    // Set end_cmd_f
            *ch_p = 0;                              // return nul character
            return (0);
        }
    }
    while (!(errnum = CmdNextCmdPkt(c)));           // while next command packet is expected

    return (errnum);                                // Report unexpected first packet
}

INT16U CmdNextCmdPkt(struct cmd * c)
{
    struct pars_buf  *  pars_buf;
    struct cmd_pkt   *  cmd_pkt;

    pars_buf = c->pars_buf;

    /*--- Loop waiting for packet(s) ---*/

    do                                              // Loop waiting for packets
    {
        pars_buf->pkt[c->pkt_buf_idx].header_flags = 0;     // Cancel old packet

        if (c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD)      // If executing a set command
        {
            OSSemPost(dev.set_lock);                // Post set lock semaphore
        }

        OSSemPend(c->sem);                          // Wait on task semaphore for next packet

        if (c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD) // If executing a set command
        {
            OSSemPend(dev.set_lock);                // Pend on set lock semaphore
        }

        c->pkt_buf_idx   ^= 1;                      // Flip to other packet buffer
        pars_buf->out_idx = 0;                      // Reset out_idx to beginning of packet

        cmd_pkt = &pars_buf->pkt[c->pkt_buf_idx];   // Get address of new cmd packet
    }
    while (c->new_cmd_f &&                          // While new command starting and
           !Test(cmd_pkt->header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT)); // packet does not start a command

    /*--- Check new packet ---*/

    if (!c->new_cmd_f &&                            // If command already running and
        Test(cmd_pkt->header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT)) // packet starts a command
    {
        return (FGC_CMD_RESTARTED);                 // Report that cmd has been aborted
    }

    return (FGC_OK_NO_RSP);                         // Return good status
}



INT16U CmdGetChangedProps(void)
{
	INT16U tmp  = 0;
    struct prop * p = &PROP_CONFIG_CHANGED;

    tmp =  p->n_elements;

    p->n_elements = p->max_elements;

    tmp = CmdGetChangedPropsHelper(p);

    return tmp;
}



INT16U CmdParentGet(struct cmd * c, const struct prop * p, INT16U filter)
{
    INT16U              errnum;
    INT16U              idx;
    INT16U              mux_idx;
    prop_size_t         n_els;
    prop_size_t         from;
    prop_size_t         to;
    INT16U              getopts;
    struct prop    *    child_prop;
    DEFPROPS_FUNC_GET((*get_func));                // function pointer declaration

    if (c->abort_f)                                 // If command was aborted (CTRL-C via keyboard or FIP unlock)
    {
        return (FGC_ABORTED);                       // Report ABORTED
    }

    CmdPrintTimestamp(c, p);                        // Produce timestamp (required for Fieldbus commands)

    if (Test(p->flags,   PF_SYM_LST)   &&   // If Parent property with an associated symlist (e.g. CAL, REF)
        Test(c->getopts, GET_OPT_RANGE))             // and RANGE option is present
    {
        CmdPrintRange(c, p);                        // Then, print the associated symlist

        return (FGC_OK_NO_RSP);                     // And return
    }

    from       = c->from;                           // Save array range and flags
    to         = c->to;
    getopts    = c->getopts;
    mux_idx    = c->mux_idx;
    child_prop = p->value;                          // Get address of array of lower level properties

    Set(getopts, GET_OPT_LBL);                      // Enable option labels by default

    n_els = PropGetNumEls(c, p);

    for (idx = 0 ; idx < n_els ; idx++, child_prop++) // For each child property
    {
        if (!child_prop->get_func_idx || child_prop->value == &PROPS)
        {
            continue;
        }

        if (filter == PARENT_GET_NOT_HIDDEN)
        {
            if (Test(child_prop->flags, PF_HIDE))
            {
                continue;
            }
        }
        else if (child_prop->type != PT_PARENT &&
                 (!Test(child_prop->flags, PF_CONFIG) ||
                  (filter == PARENT_GET_CONFIG_CHANGED && !Test(child_prop->dynflags, DF_CFG_CHANGED)) ||
                  (filter == PARENT_GET_CONFIG_SET     && !Test(child_prop->dynflags, DF_NVS_SET)) ||
                  (filter == PARENT_GET_CONFIG_UNSET   &&  Test(child_prop->dynflags, DF_NVS_SET))
                 )
                )
        {
            continue;
        }

        c->from    = from;                      // Restore array range and flags
        c->to      = to;
        c->getopts = getopts;
        c->prop    = child_prop;
        c->sym_idxs[c->si_idx++] = child_prop->sym_idx;

        c->mux_idx = (child_prop->type == PT_PARENT ||
                      Test(child_prop->flags, PF_PPM | PF_SUB_DEV) ? mux_idx : 0);

        get_func = GET_FUNC[child_prop->get_func_idx];

        if (get_func == GetParent)
        {
            errnum = CmdParentGet(c, child_prop, filter);
        }
        else
        {
            errnum = get_func(c, child_prop);   // Call child's get function
        }

        c->si_idx--;                            // Pop prop name symbols stack index

        if (errnum)                             // If function returns an error
        {
            return (errnum);                    // Report error to calling function
        }

        if (get_func != GetParent)              // If not a parent property
        {
            fputc('\n', c->f);                  // write newline
        }
    }

    return (0);
}

INT16U CmdPrepareGet(struct cmd * const c, struct prop * const p, INT16U linelen, prop_size_t * n_els_ptr)
{
    prop_size_t     n_els;
    INT16U          type_idx;
    INT16U          errnum;

    c->line    = 0;                             // Reset line counter
    c->linelen = linelen;                       // And set elements per line

    // Prepare array range

    errnum = PropValueGet(c, p, NULL, c->from, NULL);   // Get first element to retrieve n_element in case

    // property iS on the DSP. This will cache the first block.
    if (errnum)
    {
        if (n_els_ptr != NULL)
        {
            *n_els_ptr = 0;                     // In case of errnum, the number of elements is forced to 0
        }

        return (errnum);
    }

    n_els = c->n_elements;

    if (n_els)                                  // Get number of data elements in array
    {
        if (c->n_arr_spec < 2 || c->to >= n_els) // if to index not specified, or to beyond n_elemens
        {
            c->to = n_els - 1;                  // Clip 'to' to end of contents
        }

        if (c->from > c->to)                    // If start is after end
        {
            n_els = c->from = c->to = 0;        // Reset from, to & n_els to zero
        }
        else
        {
            n_els = (c->to - c->from + c->step) / c->step;  // Calc number of elements in range
        }                                       // incorporating the array step
    }

    /*--- Prepare IDX flags ---*/

    if ((c->token_delim != ',')         &&      // if delimiter is NOT a comma, and
        !Test(c->getopts, GET_OPT_NOIDX) &&      // NOIDX is NOT specified, and
        (Test(c->getopts, GET_OPT_IDX)   ||      // IDX is specifed, or
         (n_els > linelen)))                      // the number of elements exceeds line length
    {
        Set(c->getopts, GET_OPT_IDX);           // set IDX flag in opt structure
    }
    else
    {
        Clr(c->getopts, GET_OPT_IDX);           // reset IDX flag in opt structure
    }

    /*--- Add timestamp to response on first call ---*/

    CmdPrintTimestamp(c, p);

    /*--- Display label and/or type and/or range as required ---*/

    if (Test(c->getopts, GET_OPT_LBL))          // If LBL option defined
    {
        CmdPrintLabel(c, p);                    // Display property label
    }

    if (Test(c->getopts, GET_OPT_TYPE))         // If TYPE option defined
    {
        if (Test(p->flags, PF_SYM_LST))         // If symlist
        {
            if (p->max_elements > 1)            // If array
            {
                type_idx = PT_STRING;           // Specify STRINGS
            }
            else
            {
                type_idx = PT_CHAR;             // Specify CHAR
            }
        }
        else
        {
            type_idx = p->type;
        }

        fprintf(c->f, "%s:", prop_type_name[type_idx]); // Display property type name
    }

    if (Test(c->getopts, GET_OPT_RANGE))        // If RANGE get option specified
    {
        CmdPrintRange(c, p);                    // Print range if relevant to this property
    }

    /*--- Write newline if required ---*/

    if (Test(c->getopts, GET_OPT_IDX) ||
        (Test(c->getopts, GET_OPT_LBL) && (n_els > linelen) && c->token_delim == ' '))
    {
        fputc('\n', c->f);
    }

    if (n_els_ptr != NULL)
    {
        *n_els_ptr = n_els;
    }

    return (FGC_OK_NO_RSP);
}

void CmdStartBin(struct cmd * c, const struct prop * p, INT32U n_bytes)
{
    CmdPrintTimestamp(c, p);                    // Produce timestamp
    c->store_cb(0xFF);                          // Write Binary data header byte (0xFF)
    FbsOutLong((char *) &n_bytes, c);           // Write payload length in bytes
}

void CmdPrintTimestamp(struct cmd * c, const struct prop * p)
{
    OS_CPU_SR                              cpu_sr;
    struct abs_time_us                     timestamp;                            // Local copy of the timestamp
    volatile struct abs_time_us      *     timestamp_ptr;
    enum timestamp_select timestamp_select = TIMESTAMP_NOW;     // The default timestamp (time now)
    BOOLEAN                                timestamp_found  = FALSE;
    const struct prop           *          parent_p;
    struct init_dynflag_timestamp_select * property_timestamp_info;

    // If not from serial terminal and timestamp not already produced

    if ((c != &tcm) && (!c->timestamp_f))
    {
        if (!Test(p->dynflags, DF_TIMESTAMP_SELECT))
        {
            // The most common case: the property uses the default timestamp (time now).

            timestamp_ptr = &mst.time;
        }
        else
        {
            // If the property has the DF_TIMESTAMP_SELECT flag then look for the property and its parents in the
            // init_dynflag_timestamp_select list, in order to know which timestamp to use.

            parent_p = p;

            while (!timestamp_found && parent_p)
            {
                property_timestamp_info = &init_dynflag_timestamp_select[0];

                while (property_timestamp_info->prop)
                {
                    if (property_timestamp_info->prop == parent_p)
                    {
                        timestamp_select = property_timestamp_info->timestamp_select;
                        timestamp_found  = TRUE;
                        break;
                    }

                    property_timestamp_info++;
                }

                parent_p = parent_p->parent;
            }

            // Switch on the timestamp selector

            switch (timestamp_select)
            {
                case TIMESTAMP_CURRENT_CYCLE:
                    timestamp_ptr = &dpcom.dsp.cyc.start_time;
                    break;

                case TIMESTAMP_PREVIOUS_CYCLE:
                    timestamp_ptr = &dpcom.dsp.cyc.prev_start_time;
                    break;

                case TIMESTAMP_LOG_CAPTURE:
                    timestamp_ptr = &dpcls.dsp.log.capture.first_sample_time;
                    break;

                case TIMESTAMP_LOG_OASIS:
                    timestamp_ptr = &dpcls.dsp.log.oasis.timestamp[c->mux_idx];
                    break;

                case TIMESTAMP_NOW:
                default:
                    timestamp_ptr = &mst.time;
                    break;
            }
        }

        // Copy of timestamp in a critical section

        OS_ENTER_CRITICAL();
        timestamp = *timestamp_ptr;
        OS_EXIT_CRITICAL();

        FbsOutLong((char *) &timestamp.unix_time, c);
        FbsOutLong((char *) &timestamp.us_time,   c);

        c->timestamp_f = TRUE;
    }
}

void CmdPrintLabel(struct cmd * c, const struct prop * p)
{
    /*
     * ToDo p not used here!?
     */

    INT16S      n;                      // n: Length of the last symbol name. Relevant on FGC2 only
    INT16U      line_offset_parent = 0; // Line offset after the last parent symbol name
    INT16U      line_offset_child  = 0; // Line offset after the child symbol name
    INT16U      idx;
    INT16U      si_idx;

    si_idx = c->si_idx;                 // Number of symbols in the name of the child property
    idx    = c->n_prop_name_lvls;       // Number of levels in the name of the parent property (if present)

    // ToDo: vulnerability
    // if (idx = c->n_prop_name_lvls)  is bigger than (si_idx = c->si_idx) then later
    // n can be used uninitialised !!! can we assure that always c->n_prop_name_lvls <= c->si_idx


    if (idx == si_idx)                  // If just getting this child property
    {
        idx = 0;                        // Show complete name, otherwise only show symbols below
    }

    // If a leaf property, do not return the property name but "value" as the label.
    // This is required by CMW and was previously hacked in the fgcd (EPCCCS-4855)

    if (   c->last_prop       != NULL   
        && c->last_prop->type != PT_PARENT)
    {
        line_offset_child = fputs("value:", c->f);
    }
    else
    {
        n = -1;

        while (idx < c->si_idx)
        {
            line_offset_parent  = c->line_offset;
            line_offset_parent += (n + 1);

            // On M68HC16 (FGC2), n holds the length of the last symbol name being written
            n = fputs(SYM_TAB_PROP[c->sym_idxs[idx++]].key.c, c->f);
            fputc((idx == si_idx ? ':' : '.'), c->f);

            line_offset_child  = c->line_offset;
        }

    }

    if (c->token_delim == ' ')                          // If neat formating required
    {
        // Compute the number of spaces to add to the current printout for neat formatting

        n  = line_offset_parent + ST_MAX_SYM_LEN;               // Min column number
        n += ST_NEAT_FORMAT_TAB_SZ - n % ST_NEAT_FORMAT_TAB_SZ; // Add a delta for tabulation
        n -= line_offset_child;                                 // Number of spaces to add

        while (n--)                                             // For each space required
        {
            fputc(' ', c->f);                                   // write a space to output stream
        }
    }
}

INT16U CmdPrintIdx(struct cmd * c, prop_size_t idx)
{
    if (c->abort_f)                             // If command was aborted (CTRL-C via keyboard or FIP unlock)
    {
        return (FGC_ABORTED);                   // Report ABORTED
    }

    if ((c->token_delim == ' ') &&              // If not comma delimiter and
        !(c->line % c->linelen))                 // it's time for a new line
    {
        if (Test(c->getopts, GET_OPT_IDX))      // If Index required
        {
            fputc('\n', c->f);                  // write newline
            CmdPrintIdxFormatted(c, idx);       // Write IDX with neat/direct/hex/dec
        }
        else if (c->line)                       // else if not the first element
        {
            fputc('\n', c->f);                  // Write newline
        }
    }
    else if (c->line && c->token_delim)         // else if delimiter required
    {
        fputc(c->token_delim, c->f);            // Write delimiter character
    }

    c->line++;

    return (0);
}

char * CmdPrintSymLst(const struct sym_lst * sym_lst, INT16U value)
{
    while (sym_lst->sym_idx)                    // For each symbol in list
    {
        if (value == sym_lst->value)            // If value matches symbol constant
        {
            return (SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c); // Return pointer to symbol string
        }

        sym_lst++;
    }

    return ("!invalid.");                       // No match so report invalid
}

void CmdPrintBitMask(FILE * f, const struct sym_lst * sym_lst, INT16U value)
{
    INT16U  mask;
    BOOLEAN space_f = FALSE;

    while (sym_lst->sym_idx)                    // For each symbol in list
    {
        mask = sym_lst->value;                  // Get mask constant for symbol

        if ((value & mask) == mask)             // If bits in value match bits in mask
        {
            if (space_f)
            {
                fputc(' ', f);                  // Prefix space if not first symbol
            }
            else
            {
                space_f = TRUE;
            }

            fputs(SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c, f); // Write symbol
        }

        sym_lst++;
    }
}

void CmdWaitUntil(INT32U unix_time, INT16U ms_time)
{
    while ((TimeGetUtcTime() < unix_time)
           || ((TimeGetUtcTime() == unix_time) && (TimeGetUtcTimeMs() < ms_time))
          )
    {
        OSTskSuspend();  // Resumed by MstTsk on next millisecond
    }
}

// Internal function definitions

static void CmdPrintIdxFormatted(struct cmd * c, INT16U idx)
{
    //        ---- TERM ----      --- DIRECT ---
    //        DEC        HEX      DEC        HEX
    static const char * int4fmtstr[] = { "%4u: ", "0x%04X: ", "%u:", "0x%04X:" };
    INT16U              i               = 0;


    if (Test(c->getopts, GET_OPT_HEX))
    {
        i = 1;
    }

    if (c->token_delim == ',')
    {
        i += 2;
    }

    fprintf(c->f, int4fmtstr[i], idx);          // Write idx using format
}

static void CmdPrintRange(struct cmd * c, const struct prop * p)
{
    const struct sym_lst   *  sym_lst;
    BOOLEAN     space_f;
    INT8U       delim;

    delim = (c->token_delim == ',' ? ' ' : '\n');

    if (Test(p->flags, PF_SYM_LST))             // If property uses a symlist
    {
        space_f = FALSE;                        // Suppress leading space
        sym_lst = (struct sym_lst *)p->range;   // Get pointer to symbol list

        fputc('(', c->f);                       // Write opening bracket (

        while (sym_lst->sym_idx)                // For each symbol index (until end of list)
        {
            if (space_f)
            {
                fputc(' ', c->f);               // Write leading space if not first time
            }
            else
            {
                space_f = TRUE;
            }

            fputs(SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c, c->f); // Write symbol

            sym_lst++;
        }

        fputc(')', c->f);
        fputc(delim, c->f);                     // Write closing delimiter
    }
    else if (Test(p->flags, PF_INT_LIMITS))     // If Integer limits in use
    {
        if (p->type == PT_INT32U && !Test(c->getopts, GET_OPT_HEX))
        {
            fprintf(c->f,
                    "(%lu %lu)%c",
                    ((struct intu_limits *)p->range)->min,
                    ((struct intu_limits *)p->range)->max,
                    delim);
        }
        else
        {
            fprintf(c->f,
                    (Test(c->getopts, GET_OPT_HEX) ? "(0x%08lX 0x%08lX)%c" : "(%ld %ld)%c"),
                    ((struct int_limits *)p->range)->min,
                    ((struct int_limits *)p->range)->max,
                    delim);
        }
    }
    else if (Test(p->flags, PF_FLOAT_LIMITS))   // If float limits in use
    {
        fprintf(c->f,
                "(%.5E %.5E)%c",                // Float limits as x.xxxxxEyy
                (double)((struct float_limits *)p->range)->min,
                (double)((struct float_limits *)p->range)->max,
                delim);
    }
}

static INT16U CmdUnSub(void)
{
    INT16U      sub_idx;
    INT16U      errnum = FGC_OK_NO_RSP;

    errnum = PubRemoveSub(fcm.prop, &sub_idx);      // Remove sub for this property

    if (!errnum)
    {
        fcm.store_cb(sub_idx);                      // Return sub index byte to the GW
    }

    return errnum;
}

static INT16U CmdSet(struct cmd * c)
{
    OS_CPU_SR       cpu_sr;
    INT16U          errnum          = 0;
    INT16U          ms_time;
    struct prop  *  p               = c->prop;

    OS_ENTER_CRITICAL();            // Take event log timestamp atomically
    c->evt_rec.timestamp.unix_time = TimeGetUtcTime();
    ms_time = TimeGetUtcTimeMs();

    OS_EXIT_CRITICAL();

    c->evt_rec.timestamp.us_time   = (INT32U)ms_time * 1000L;   // Convert milliseconds to microseconds

    if (!p->set_func_idx)                   // If Set Function is not defined for this property
    {
        errnum = FGC_SET_NOT_PERM;          // Report that set is not permitted
    }
    else if (p->setif_func_idx)             // If SetIf function is defined
    {
        errnum = SETIF_FUNC[p->setif_func_idx](c);  // Call SetIf function
    }

    if (!errnum &&                          // if no error yet, and
        Test(p->flags, PF_CONFIG) &&         // property is part of the global configuration, and
        sta.config_mode == FGC_CFG_MODE_SYNC_DB) // config mode is SYNC_DB
    {
        errnum = FGC_SYNC_IN_PROGRESS;      // Report SYNC IN PROGRESS
    }

    c->device_par_err_idx = 0;              // Reset parsing error index
    c->evt_log_state = EVT_LOG_RESET_VALUE; // Prepare to log set value

    if (!errnum)                            // If no error so far
    {
        c->n_pars           = 0;            // Prepare to parse parameters
        c->pars_buf->n_pars = 0;
        c->last_par_f       = FALSE;
        c->changed_f        = FALSE;

        OSSemPend(dev.set_lock);            // Pend on set lock semaphore
        errnum = SET_FUNC[p->set_func_idx](c, p);   // Call SET function
        OSSemPost(dev.set_lock);            // Post set lock semaphore

        if (errnum == 0 && Test(p->flags, PF_SUB_DEV) == false)
        {
            PubProperty(p, c->mux_idx, TRUE);     // Notify for publication
        }
    }
    else
    {
        LogEvtSaveValue(c);                 // Log value string (up to end of current packet only)
    }

    LogEvtCmd(c, errnum);                   // Log set command

    mst.set_cmd_counter = 5;                // Inhibit autocalibration for 5s

    return (errnum);
}

static INT16U CmdGet(struct cmd * c)
{
    INT16U          errnum = FGC_OK_NO_RSP;
    prop_size_t     from;
    prop_size_t     to;
    INT16U          getopts;

    struct prop * p = c->prop;

    if (!p->get_func_idx)                   // if Get Function is not defined for this property
    {
        return (FGC_GET_NOT_PERM);          // Report GET NOT PERMITTED
    }

    errnum = PropIdentifyGetOptions(c, p);

    if (errnum)                             // if get options are not valid
    {
        return (errnum);                    // Report error
    }

    c->timestamp_f = FALSE;                 // Reset timestamp flag so that it will be produced once

    if (Test(c->getopts, GET_OPT_INFO))     // If INFO get option set
    {
        GetPropInfo(c, p);                  // Run PropInfo function on property
    }
    else if (Test(c->getopts, GET_OPT_SIZE)) // else if SIZE get option set
    {
        errnum = GetPropSize(c, p);         // Run PropSize function on property
    }
    else
    {
        if (c->mux_idx == PPM_ALL_USERS)    // If get for all users (only possible if !PARENT)
        {
            uint8_t max_user;
            
            Clr(c->getopts, GET_OPT_LBL);   // Suppress labels

            CmdPrintTimestamp(c, p);        // Produce timestamp

            from    = c->from;              // Save array range and flags
            to      = c->to;
            getopts = c->getopts;

            // If the property is read only, get all users irrespective of DEVICE.PPM

            max_user = (p->set_func_idx == SET_NONE ? FGC_MAX_USER : dev.max_user);            

            for (c->mux_idx = 0 ; c->mux_idx <= max_user ; c->mux_idx++)
            {
                CmdPrintIdxFormatted(c, c->mux_idx);        // Write mux_idx with neat/direct/hex/dec

                c->cached_prop = NULL;      // Clear cached property pointer
                c->from    = from;          // Restore array range and flags
                c->to      = to;
                c->getopts = getopts;

                errnum = GET_FUNC[p->get_func_idx](c, p);

                if (errnum)                                     // If get function returns an error
                {
                    return (errnum);        // Report the error
                }

                fputc('\n', c->f);          // write newline
            }
        }
        else                                // else get for one user only
        {
            return (GET_FUNC[p->get_func_idx](c, p)); // Call Get function and return errnum
        }
    }

    return (errnum);
}

static INT16U CmdGetSub(void)
{
    if (!Test(fcm.prop->dynflags, DF_SUBSCRIBED)) // If property has no subscription
    {
        return (FGC_PROTO_ERROR);               // Report PROTO ERROR
    }

    OSSemPend(pub.lock);                        // Wait for access to sub table

    Clr(sub.flags, SUB_FLAGS_GET_PENDING);      // Clear Get pending flag

    OSSemPost(pub.lock);                        // Release lock on sub table

    return (CmdGet(&fcm));                      // Execute GET command
}

static INT16U CmdSub(void)
{
    INT16U         errnum = FGC_OK_NO_RSP;
    INT16U         sub_idx;

    // Check for possible errors with sub command

    if (!fcm.prop->get_func_idx ||                      // if Get Function is not defined for this prop, or
        fcm.prop->value == &PROPS)                      // property loops back to top level (e.g. CONFIG.SET)
    {
        return (FGC_SUB_NOT_PERM);                      // Report SUB NOT PERMITTED
    }

    if (Test(fcm.prop->flags, PF_SUB_DEV))              // If SUB_DEV property
    {
        return (FGC_NOT_IMPL);                          // Report Not Implemented
    }

    if (fcm.mux_idx)                                    // Always act as is user zero is provided (i.e. subscribe for all users)
    {
        fcm.mux_idx = 0;
    }

    errnum = PropIdentifyGetOptions(&fcm, fcm.prop);

    if (errnum)                                         // if get options are not valid
    {
        return (errnum);                                // Report error
    }

    if (Test(fcm.prop->dynflags, DF_SUBSCRIBED))        // If property is already subscribed
    {
        sub_idx = fcm.prop->sub_idx;

        OSSemPend(pub.lock);                            // Lock sub structure

        // Remember which subscription needs to be published once the acknowledgement
        // has been sent to the Gateway.

        sub.new_sub_idx = sub_idx;

        OSSemPost(pub.lock);                            // Unlock sub structure
    }
    else                                                // else new subscription is required
    {
        errnum = PubAddSub(fcm.prop, &sub_idx);
    }

    if (!errnum)
    {
        fcm.store_cb(sub_idx);                          // Return sub index byte to the GW
    }

    return errnum;
}



static INT16U CmdGetChangedPropsHelper(const struct prop * p)
{
    struct prop    *child_prop;
    prop_size_t     n_els;
    INT16U          idx;
    INT16U          changed = 0;

    child_prop = p->value;
    n_els      = PropGetNumEls(NULL, p);

    for (idx = 0 ; idx < n_els ; idx++, child_prop++)
    {
        if (child_prop->value == &PROPS)
        {
            continue;
        }

        if (child_prop->type == PT_PARENT)
        {
            changed += CmdGetChangedPropsHelper(child_prop);
        }
        else
        {
            if (Test(child_prop->flags, PF_CONFIG) &&
                Test(child_prop->dynflags, DF_CFG_CHANGED))
            {
                changed++;
            }
        }
    }

    return (changed);
}


// EOF

