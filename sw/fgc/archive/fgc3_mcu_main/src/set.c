/*---------------------------------------------------------------------------------------------------------*\
  File:     set.c

  Purpose:  FGC MCU Software - Set Command Functions
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#include <set.h>

#include <adc.h>
#include <ana_temp_regul.h>
#include <cal.h>
#include <class.h>
#include <dev.h>
#include <diag.h>
#include <DIMsDataProcess.h>
#include <dls.h>
#include <dpcls.h>
#include <fbs_class.h>
#include <fbs.h>
#include <fgc_errs.h>
#include <fgc_pc_fsm.h>
#include <init.h>
#include <log.h>
#include <math.h>
#include <mcu_dependent.h>
#include <memmap_mcu.h>
#include <pars.h>
#include <prop.h>
#include <pub.h>
#include <ref.h>
#include <regfgc3_task.h>
#include <regfgc3_prog.h>
#include <regfgc3_prog_fsm.h>
#include <regfgc3_pars.h>
#include <runlog_lib.h>
#include <regfgc3_pars.h>
#include <set_class.h>
#include <sta.h>
#include <string.h>

/*---------------------------------------------------------------------------------------------------------*/
INT16U SetAbsTime(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    DOUBLE
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetChar(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    CHAR
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetConfig(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetConfig allows the Configuration to be:

    UNSET       Magic number in non-volatile memory is erased so that complete config will be
            wiped on next reset
    SET     Magic number in non-volatile memory is reinstated, so configuration will be
            preserved
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (p->sym_idx)
    {
        case STP_UNSET: // Erase configuration

            NVRAM_UNLOCK();
            NVSIDX_MAGIC_P = ~NVS_MAGIC;
            NVRAM_LOCK();
            break;

        case STP_SET:   // Preserve configuration

            NVRAM_UNLOCK();
            NVSIDX_MAGIC_P = NVS_MAGIC;
            NVRAM_LOCK();
            break;
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetCore(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetCore is used to check and save the Core control table
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U                  i;
    INT32U                  total_length_words = 0;
    INT32U                  addr_in_core       = NVRAM_COREDATA_32;
    struct fgc_corectrl     corectrl;


    for (i = 0; i < FGC_N_CORE_SEGS; i++)           // Calculate total length of segments
    {
        total_length_words += core.n_words[i];
    }

    if (total_length_words > NVRAM_COREDATA_W)      // If length is too long
    {
        memset(&core.n_words, 0, sizeof(core.n_words)); // Clear all lengths
        return (FGC_BAD_PARAMETER);                 // Report error
    }

    for (i = 0; i < FGC_N_CORE_SEGS; i++)           // Create core control table
    {
        corectrl.seg[i].addr     = core.addr[i];
        corectrl.seg[i].n_words  = core.n_words[i];
        corectrl.seg[i].pad      = 0xFFFF;
        corectrl.addr_in_core[i] = addr_in_core;
        addr_in_core += (2 * core.n_words[i]);
    }

    CoreTableSet(&corectrl);                        // Write table to FRAM and clear core data

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetFloat(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    FLOAT
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetInteger(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    INT8U, INT8S, INT16U, INT16S, INT32U, INT32S including symlist properties
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U PeriodIters(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    INT32U
\*---------------------------------------------------------------------------------------------------------*/
{
    return (SetInteger(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetRawParameter(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    INT32U
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      errnum;
    prop_size_t el_idx;
    INT16U      max_pars;
    uint16_t    block_idx;
    void    *   new_val;                        // Pointer to new element value
    int32_t     data[SCIVS_N_PARAMS_PER_BLOCK];
    enum scivs_card_device component; 

    /*--- Prepare array range ---*/

    el_idx = c->from;

    max_pars  = c->to - el_idx + 1;             // Calculate number of elements in array range

    component = (p->sym_idx == STP_PARAM) ? SCIVS_MF : SCIVS_DEVICE_2; 
    block_idx = regfgc3_pars_handler.raw.block + (regfgc3_pars_handler.raw.access == FGC_ACCESS_RIGHTS_RO ? SCIVS_N_WRITE_BLOCKS : 0);

    errnum = regFgc3ParsGetBlockFromCache(regfgc3_pars_handler.raw.slot, component, block_idx, data);

    if (errnum != SCIVS_OK)
    {
        return scivsConvertToFgcErr(errnum);
    }
    
    while (!c->last_par_f && c->n_pars < max_pars)
    {
        errnum  = ParsValueGet(c, p, &new_val);                 // Get new value from command packet

        switch (errnum)
        {
            case 0:

                data[el_idx] = *((int32_t *) new_val);
                c->changed_f = TRUE;

                c->n_pars++;
                el_idx++;
                break;

            case FGC_NO_SYMBOL:             // TOKEN is empty (i.e. blank between two commas or after last comma)

                if (p->type == PT_CHAR)             // If CHAR type
                {
                    if (c->n_pars)                          // If at least one character was provided
                    {
                        return (FGC_NO_SYMBOL);                     // Report NO_SYMBOL
                    }
                }

                if (el_idx || !c->last_par_f)       // If not first element or not last parameter
                {
                    c->n_pars++;                            // Don't change existing value for this element
                    el_idx++;
                }

                break;

            default:

                c->device_par_err_idx += c->n_pars;
                return (errnum);
        }
    }
    
    /*--- Check number of parameters set and adjust property length ---*/

    if (!c->last_par_f && c->n_pars >= max_pars)
    {
        c->device_par_err_idx += c->n_pars;
        return (FGC_BAD_PARAMETER);
    }

    // Contrary to properties, perform transaction even if data has not changed. This behaviour is more predictable
    // and can be useful during the development cycle of RegFGC3 boards

    return regFgc3ParsSetBlockHwAndProp(regfgc3_pars_handler.raw.slot, component, block_idx, data);
}


INT16U SetRegFgc3CrateCacheScan(struct cmd * c, struct prop * p)
{
    
    regFgc3TaskSetErrand(REGFGC3_TASK_ERRAND_SCAN);
    
    return FGC_OK_NO_RSP;
}



INT16U SetRegFgc3ProgMode(struct cmd * c, struct prop * p)
{
    
    INT16U errnum;
    const struct sym_lst * sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum != FGC_OK_NO_RSP)
    {
        return (errnum);
    }

    return regFgc3ProgFsmSetMode(sym_const->value);
}



INT16U SetRegFgc3ProgStatus(struct cmd * c, struct prop * p)
{
    
    INT16U errnum;
    const struct sym_lst * sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum != FGC_OK_NO_RSP)
    {
        return (errnum);
    }

    return regFgc3ProgSetStatus(sym_const->value);
}



INT16U SetRegFgc3Parameters(struct cmd * c, struct prop * p)
{
    uint16_t err_code; 
    if ((err_code = SetInteger(c, p)) != FGC_OK_NO_RSP)
    {
        return err_code; 
    }
    
    return regFgc3ParsRetrieveParams();
}

/* 
 * EPCCCS-4496: risk of race condition by setting the config mode before checking whether the action is 
 * allowed or not, and rolling it back later on. See EPCCCS-4496.
 */
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetConfigMode(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Used to synchronize either the FGC or the DB
\*---------------------------------------------------------------------------------------------------------*/
{    
    
    
    INT16U config_mode = sta.config_mode;
    INT16U errnum;

    errnum = SetInteger(c, p);    

    if (errnum != 0)
    {
        return (errnum);
    }

    if (   STATE_PC != FGC_PC_OFF 
        && STATE_PC != FGC_PC_FLT_OFF
        && sta.config_mode != FGC_CFG_MODE_SYNC_DB)
    {
        sta.config_mode = config_mode;
        return (FGC_BAD_STATE);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetReset(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetReset is used to allow a S PROP RESET for
  S CONFIG.STATE RESET
  S DIAG RESET
  S DLS RESET
  S ID RESET
  S LOG RESET
  S DEVICE.RESETS RESET
  S SPY RESET
  S MEAS.STATUS RESET
  S FGC.ST_LATCHED RESET
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U                  errnum;
    const struct sym_lst  * sym_const;
    static struct sym_lst   sym_lst_reset[] = { { STC_RESET, 0 }, { 0, 0 } };

    errnum = ParsScanSymList(c, "Y", sym_lst_reset, &sym_const);   // delimiter: '\0'

    if (errnum)
    {
        return (errnum);
    }

    // RESET is the only choice in the symlist

    switch (p->sym_idx)
    {
        case STP_ADC:                               // ADC
            AdcFiltersResetRequest(TRUE);

            break;


        case STP_PROG:
            errnum = regFgc3ProgSetStatus(FGC_REGFGC3_PROG_STATE_SYNCHRONIZED);
            if (errnum != FGC_OK_NO_RSP)
            {
                return errnum; 
            }

            break;

        case STP_STATE:             // CONFIG.STATE
            StaSetConfigState(FGC_CFG_STATE_SYNCHRONISED);
            NvsResetChanged();                          // Clear the DF_CFG_CHANGED flag on all properties
            SetDefaults(c, NULL);                       // Assert default REF values

            if (sta.config_mode == FGC_CFG_MODE_SYNC_FGC)
            {
                InitPostSync(c);
            }

            sta.config_mode  = FGC_CFG_MODE_SYNC_NONE;

            break;

        case STP_DIAG:              // DIAG RESET

            diag.req_QSPI_reset = 1;                    // Request immediate diag reset

            // ToDo: I guess that as the reset is demanded from the property
            // cleaning also the number of reset counter is a nice effect
            // can this line stay?
            diag.sync_resets = 0;                       // DIAG.SYNC_RESETS
            break;

        case STP_DLS:               // DLS RESET

            dls.num_errors = 0;                         // Reset Dallas errors counter
            memset(&dls.rsp, 0, sizeof(dls.rsp));  // Reset response buffers
            PropSetNumEls(NULL, &PROP_FGC_DLS_GLOBAL, 0);   // Reset response buffer lengths for properties
            PropSetNumEls(NULL, &PROP_FGC_DLS_LOCAL,  0);
            PropSetNumEls(NULL, &PROP_FGC_DLS_MEAS_A, 0);
            PropSetNumEls(NULL, &PROP_FGC_DLS_MEAS_B, 0);
            Clr(ST_LATCHED, FGC_LAT_DALLAS_FLT);        // Clear DALLAS_FLT in latched status
            break;

        case STP_FLASH_LOCKS:                   // CODE.FLASH_LOCKS

            NVRAM_UNLOCK();
            NVRAM_MAGIC_FP = 0;                 // Clear NVRAM flash locks magic number
            NVRAM_LOCK();
            break;

        case STP_FW_DIODE:              // VS.FW_DIODE

            // ToDo: no FGC_FLT_FW_DIODE in FGC3
            break;

        case STP_FABORT_UNSAFE:
            vs.fabort_unsafe = FGC_VDI_NO_FAULT;

            break;

        case STP_LOG:               // LOG

            if (!Test(sta.cmd_req, DIG_OP_SET_VSRUNCMD_MASK16)) // If converter is not running
            {
                LogStartAll();
            }
            else
            {
                return (FGC_BAD_STATE);
            }

            break;

        case STP_NUM_RESETS:            // DEVICE.NUM_RESETS

            // ToDo: change this to be compatible
            RunlogClrResets();

            break;

        case STP_SPY:               // SPY
            InitSpyMpx(c);
            break;

        case STP_ST_LATCHED:            // STATUS.ST_LATCHED

            Clr(ST_LATCHED, ST_LATCHED_RESET);
            break;

        case STP_STATUS:                            // MEAS.STATUS

            dpcls.mcu.meas.status_reset_f = TRUE;
            break;

        case STP_VS:                // VS

            return (SetResetFaults(c, p));      // S VS RESET has the same effect as S STATUS.RESET_FAULTS
            break;

        default:
            return FGC_BAD_PARAMETER;           // In case one defined this function as the set function
            // for a property and forgot to add a switch case for it
            break;

    }

    return (FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetRterm(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    NULL (CLIENT.RTERM)  This is a dummy function because the GW will intercept the set
        request so that it can apply an RBAC rule.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (FGC_SET_NOT_PERM);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetRtermLock(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    NULL (CLIENT.RTERMLOCK)  This is a dummy function because the GW will intercept the set
        request so that it can apply an RBAC rule.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (FGC_SET_NOT_PERM);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetTerminate(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetTerminate is used by the S DEVICE.RESET/BOOT/PWRCYC/CRASH properties.
\*---------------------------------------------------------------------------------------------------------*/
{
    dev.ctrl_sym_idx = p->sym_idx;
    dev.ctrl_counter = 40;          // 40 x 5ms = 200ms
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetResetFaults(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  This function will:
    - Clear the latched faults in STATUS.ST_LATCHED
    - Send the reset signal to the VS to clear the external faults
  It is called by doing a S VS RESET or a S STATUS.RESET_FAULTS.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (vs.fw_diode == FGC_VDI_FAULT)          // If a FW DIODE fault was received
    {
        return (FGC_FW_DIODE_FAULT);                // Block the reset request
    }

    if (vs.fabort_unsafe == FGC_VDI_FAULT)     // If a FW DIODE fault was received
    {
        return (FGC_FABORT_UNSAFE);                 // Block the reset request
    }

    Set(sta.cmd, DDOP_CMD_RESET);

    qspi_misc.relaunch_dim_logging = TRUE;

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetPC(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Set PC command
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U errnum;
    const struct sym_lst * sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    return PcFsmSetState(c, sym_const->value);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetPCSimplified(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Set PC Simplified command
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U errnum;
    const struct sym_lst * sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    errnum = PcFsmSetSimplifiedState(c, sym_const->value);

    return errnum;
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetAnaVrefTemp(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:        FLOAT

  Property:     CAL.VREF.TEMPERATURE.TARGET
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U errnum;

    errnum = ParsSet(c, p);

    if (!errnum)
    {
        AnaTempRegulStart(*((FP32 *)p->value));
        NVRAM_UNLOCK();
        NVRAM_TARGET_TEMP_P = *((FP32 *)p->value);
        NVRAM_LOCK();

    }

    return (errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetCal(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:        FLOAT

  This a special set function because it must be able to set manually the calibration factors for the
  ADCs, DCCTs and DAC as well launch auto-calibrations.

  S CAL ADCS                    Automatic calibration of all errors of all internal ADCs (channels A, B, C, D)
  S CAL ADC_A                   Automatic calibration of all errors of internal ADC1 (= channel A)
  S CAL ADC_B                   Automatic calibration of all errors of internal ADC2 (= channel B)
  S CAL ADC_C                   Automatic calibration of all errors of internal ADC3 (= channel C)
  S CAL ADC_D                   Automatic calibration of all errors of internal ADC4 (= channel D)
  S CAL EXT_ADCS, {p}           Automatic calibration of an error for both external ADCs (p=CALZERO,CALPOS,CALNEG)
  S CAL EXT_ADC_A,{p}           Automatic calibration of external ADC A error (p=CALZERO,CALPOS,CALNEG)
  S CAL EXT_ADC_B,{p}           Automatic calibration of external ADC B error (p=CALZERO,CALPOS,CALNEG)
  S CAL DCCTS, {p}              Automatic calibration of an error for both DCCTs (p=CALZERO,CALPOS,CALNEG)
  S CAL DCCT_A,{p}              Automatic calibration of DCCT A error (p=CALZERO,CALPOS,CALNEG)
  S CAL DCCT_B,{p}              Automatic calibration of DCCT B error (p=CALZERO,CALPOS,CALNEG)
  S CAL DACS                    Automatic calibration of of both DACs
  S CAL DAC_A                   Automatic calibration of DAC1 (done automatically when converter starts)
  S CAL DAC_B                   Automatic calibration of DAC2

  Auto calibrations are requested to the DSP via the following variables:

  dpcls.mcu.cal.action          CAL_REQ_BOTH_ADC16S|CAL_REQ_ADC16|CAL_REQ_ADC22|CAL_REQ_DCCT|CAL_REQ_DAC
  dpcls.mcu.cal.chan_mask       FGC_DCCT_SELECT_A or FGC_DCCT_SELECT_B or FGC_DCCT_SELECT_AB
  dpcls.mcu.cal.idx             0=Offset, 1=GainPos, 2=GainNeg

  The function uses two symbol lists - only one can be defined per property in the XML (sym_lst_cal_type)
  while the second (sym_lst_cal_ref) must be defined statically in the function.

  For all the cases were {p} is defined as CALZERO, CALPOS or CALNEG the function will check that the
  measured voltage is within +/-CAL_VREF_LIMIT of the nominal value (0V,+10V,-10V) and will return
  REF_MISMATCH if not.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U              errnum;
    INT16U              cal_type;
    const struct sym_lst * sym_const;
    INT32S              cal_signal_idx;
    INT32U              cal_chan_mask;
    struct cal_seq const * cal_sequence = NULL;

    static const FP32   vref[3] = { 0.0, 10.0, -10.0 };

    static const struct sym_lst sym_lst_cal_ref[] =
    {
        {STC_CALZERO, CAL_IDX_OFFSET},
        {STC_CALPOS,  CAL_IDX_POS},
        {STC_CALNEG,  CAL_IDX_NEG},
        {0}
    };

    errnum = ParsScanSymList(c, "Y,", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    cal_type = sym_const->value;

    // Set cal_signal_idx

    switch (cal_type)
    {
        case FGC_CAL_TYPE_ADCS:
        case FGC_CAL_TYPE_ADC_A:
        case FGC_CAL_TYPE_ADC_B:
        case FGC_CAL_TYPE_ADC_C:
        case FGC_CAL_TYPE_ADC_D:
        case FGC_CAL_TYPE_DACS:
        case FGC_CAL_TYPE_DAC_A:
        case FGC_CAL_TYPE_DAC_B:

            if (c->token_delim == ',')
            {
                return (FGC_BAD_PARAMETER);
            }

            cal_signal_idx = 0;          // Not used for this type of calibration
            break;

        case FGC_CAL_TYPE_EXT_ADCS:
        case FGC_CAL_TYPE_EXT_ADC_A:
        case FGC_CAL_TYPE_EXT_ADC_B:
        case FGC_CAL_TYPE_DCCTS:
        case FGC_CAL_TYPE_DCCT_A:
        case FGC_CAL_TYPE_DCCT_B:

            errnum = ParsScanSymList(c, "Y", (struct sym_lst *) &sym_lst_cal_ref, &sym_const);

            if (errnum)
            {
                return (errnum);
            }

            cal_signal_idx = sym_const->value;  // = CAL_IDX_OFFSET/POS/NEG
            break;

        default:

            return (FGC_BAD_PARAMETER);
            break;
    }

    cal.prop = p;

    // Set cal_chan_mask
    // and the cal_sequence pointer

    cal_chan_mask = 0;

    switch (cal_type)
    {
            // ------------------------------------------------------------------------
            // Internal ADCs calibration
            // ------------------------------------------------------------------------

        case FGC_CAL_TYPE_ADCS:
            if (STATE_PC == FGC_PC_IDLE)
            {
                return (FGC_BAD_STATE);
            }

            // Fall through

        case FGC_CAL_TYPE_ADC_A:

            if (STATE_PC == FGC_PC_IDLE &&
                (dpcls.mcu.dcct_select == FGC_DCCT_SELECT_AB ||      // If both channel in use or
                 !(ST_MEAS_A & ST_MEAS_B & FGC_ADC_STATUS_V_MEAS_OK) ||  // both channels are not ok or
                 dpcls.mcu.dcct_select == FGC_DCCT_SELECT_A))        // measurement is in use
            {
                return (FGC_BAD_STATE);                                 // report BAD STATE
            }

            // Fall through

        case FGC_CAL_TYPE_ADC_B:

            if (STATE_PC == FGC_PC_IDLE &&
                (dpcls.mcu.dcct_select == FGC_DCCT_SELECT_AB ||      // If both channel in use or
                 !(ST_MEAS_A & ST_MEAS_B & FGC_ADC_STATUS_V_MEAS_OK) ||  // both channels are not ok or
                 dpcls.mcu.dcct_select == FGC_DCCT_SELECT_B))        // measurement is in use
            {
                return (FGC_BAD_STATE);                                 // report BAD STATE
            }

            // Fall through

        case FGC_CAL_TYPE_ADC_C:
        case FGC_CAL_TYPE_ADC_D:

            if (cal_type == FGC_CAL_TYPE_ADCS || cal_type == FGC_CAL_TYPE_ADC_A)           // Internal ADC1 (channel A)
            {
                cal_chan_mask |= 0x01;
            }

            if (cal_type == FGC_CAL_TYPE_ADCS || cal_type == FGC_CAL_TYPE_ADC_B)           // Internal ADC2 (channel B)
            {
                cal_chan_mask |= 0x02;
            }

            if (cal_type == FGC_CAL_TYPE_ADCS || cal_type == FGC_CAL_TYPE_ADC_C)           // Internal ADC3 (channel C)
            {
                cal_chan_mask |= 0x04;
            }

            if (cal_type == FGC_CAL_TYPE_ADCS || cal_type == FGC_CAL_TYPE_ADC_D)           // Internal ADC4 (channel D)
            {
                cal_chan_mask |= 0x08;
            }

            cal_sequence = &cal_seq_int_adcs[0];
            break;

            // ------------------------------------------------------------------------
            // DACs calibration
            // ------------------------------------------------------------------------

        case FGC_CAL_TYPE_DACS:
        case FGC_CAL_TYPE_DAC_A:

            if (STATE_PC == FGC_PC_IDLE)
            {
                return (FGC_BAD_STATE);
            }

            // Fall through

        case FGC_CAL_TYPE_DAC_B:

            if (cal_type == FGC_CAL_TYPE_DACS || cal_type == FGC_CAL_TYPE_DAC_A)           // DAC1 (channel A)
            {
                cal_chan_mask |= 0x01;
            }

            if (cal_type == FGC_CAL_TYPE_DACS || cal_type == FGC_CAL_TYPE_DAC_B)           // DAC2 (channel B)
            {
                cal_chan_mask |= 0x02;
            }

            cal_sequence = &cal_seq_dacs[0];
            break;

            // ------------------------------------------------------------------------
            // External ADCs calibration
            // ------------------------------------------------------------------------

        case FGC_CAL_TYPE_EXT_ADCS:
        case FGC_CAL_TYPE_EXT_ADC_A:
        case FGC_CAL_TYPE_EXT_ADC_B:

            if (STATE_PC == FGC_PC_IDLE)
            {
                return (FGC_BAD_STATE);
            }

            if (cal_type == FGC_CAL_TYPE_EXT_ADCS || cal_type == FGC_CAL_TYPE_EXT_ADC_A)         // External ADC Channel A
            {
                cal_chan_mask |= 0x01;
            }

            if (cal_type == FGC_CAL_TYPE_EXT_ADCS || cal_type == FGC_CAL_TYPE_EXT_ADC_B)         // External ADC Channel B
            {
                cal_chan_mask |= 0x02;
            }

            cal_sequence = &cal_seq_ext_adcs[0];

            // Check the voltage reference is present

            if (((dpcls.mcu.cal.chan_mask & 0x01) &&
                 fabs(dpcls.dsp.adc.volts_200ms[0] - vref[cal_signal_idx]) > CAL_VREF_LIMIT) ||
                ((dpcls.mcu.cal.chan_mask & 0x02) &&
                 fabs(dpcls.dsp.adc.volts_200ms[1] - vref[cal_signal_idx]) > CAL_VREF_LIMIT))
            {
                return (FGC_REF_MISMATCH);
            }

            break;

            // ------------------------------------------------------------------------
            // DCCTs calibration
            // ------------------------------------------------------------------------

        case FGC_CAL_TYPE_DCCTS:
        case FGC_CAL_TYPE_DCCT_A:
        case FGC_CAL_TYPE_DCCT_B:

            if (STATE_PC == FGC_PC_IDLE)
            {
                return (FGC_BAD_STATE);
            }

            if (cal_type == FGC_CAL_TYPE_DCCTS || cal_type == FGC_CAL_TYPE_DCCT_A)           // DCCT channel A
            {
                cal_chan_mask |= 0x01;
            }

            if (cal_type == FGC_CAL_TYPE_DCCTS || cal_type == FGC_CAL_TYPE_DCCT_B)           // DCCT channel B
            {
                cal_chan_mask |= 0x02;
            }

            cal_sequence = &cal_seq_dccts[0];

            // Check the voltage reference is present

            if (((dpcls.mcu.cal.chan_mask & 0x01) &&
                 fabs(dpcls.dsp.adc.volts_200ms[0] - vref[cal_signal_idx]) > CAL_VREF_LIMIT) ||
                ((dpcls.mcu.cal.chan_mask & 0x02) &&
                 fabs(dpcls.dsp.adc.volts_200ms[1] - vref[cal_signal_idx]) > CAL_VREF_LIMIT))
            {
                return (FGC_REF_MISMATCH);
            }

            break;

        default:

            return (FGC_BAD_PARAMETER);
            break;
    }

    CalInitSequence(cal_sequence, cal_chan_mask, cal_signal_idx);   // Initiate Calibration sequence
    // and switch to FGC_OP_CALIBRATING

    CalRunSequence();                                               // Run the first step of the calibration sequence

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetDirectCommand(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Set direct command register:  0xRRSS     SS=Set mask, RR=Reset mask
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U      errnum;
    INT32U      value;

    errnum = ParsScanInteger(c, "Y", &value);

    if (errnum)
    {
        return (errnum);
    }

    if (value > 0xFFFF)
    {
        return (FGC_OUT_OF_LIMITS);
    }

    DIG_OP_P = (INT16U)value;           // Write word direct (high byte is reset, low byte is set)

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetInternalAdcMpx(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:        INTERNAL_ADC_MPX
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U errnum = 0;
    INT32U adc_idx, adc_mask;

    // Setif conditions (embedded in the set function)
    //
    // Access to the AUX channel is less constrained than to the other 3. To modify its mpx, use a command
    // of the form: S ADC.INTERNAL.MPX[3] CALZERO

    if (c->from == c->to && c->from == 3)
    {
        errnum = SetifOpNotCal(c);      // Setif condition to modify the multiplexing of channel AUX only
    }
    else
    {
        errnum = SetifMpxOk(c);         // Setif condition to modify the multiplexing of other channels
    }

    if (errnum)
    {
        return errnum;
    }

    // Parsing of the user mpx request
    //
    // Note: the property array (adc.internal_adc_mpx[]) is used to process the user request and to translate it,
    //       if possible, in a DSP request. In any case the property array is updated regularly by the lower
    //       priority BGP task to reflect the real state of the Analogue HW. Therefore the modification done by
    //       this function is temporary and the user should not notice it.

    errnum = ParsSet(c, p);     // Will update adc.internal_adc_mpx[]

    if (errnum)
    {
        return errnum;
    }

    // Check that the DSP is ready to take an MPX request

    if (dpcls.mcu.ana.req_f)
    {
        return (FGC_BUSY);
    }

    // Prepare the multiplexing request to the DSP
    //
    // The MPX request coming from the user is an MPX request per ADC channel. The goal here is to summarise
    // that request into a MPX state for the analogue bus and a mask of the ADC that must connect to that bus.
    // It is likely that the user request does not translate into a valid {anabus_input + adc_mask} DSP request,
    // in which case the user is warned with "invalid mpx request" message.

    dpcls.mcu.ana.anabus_input = FGC_INTERNAL_ADC_MPX_NC;       // Request field 1: anabus_input
    dpcls.mcu.ana.adc_mask     = 0;                             // Request field 2: adc_mask

    for (adc_idx = 0, adc_mask = 1; adc_idx < FGC_N_ADCS; adc_idx++, adc_mask <<= 1)
    {
        if (adc.internal_adc_mpx[adc_idx] != FGC_INTERNAL_ADC_MPX_CH_A + adc_idx) // If link through crossbar
        {
            if (dpcls.mcu.ana.anabus_input == FGC_INTERNAL_ADC_MPX_NC)
            {
                dpcls.mcu.ana.anabus_input = adc.internal_adc_mpx[adc_idx];
            }
            else if (dpcls.mcu.ana.anabus_input != adc.internal_adc_mpx[adc_idx])
            {
                return (FGC_INVALID_MPX_REQUEST);     // Requested configuration is invalid (more than one input on the crossbar)
            }

            dpcls.mcu.ana.adc_mask |= adc_mask;
        }
    }

    // Send the request to the DSP

    dpcls.mcu.ana.req_f = TRUE;

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetPolaritySwitch(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetPolarity allows the FGC change the polarity switch position.
\*---------------------------------------------------------------------------------------------------------*/
{
    const struct sym_lst * sym_const;
    INT32U           polarity_mode;
    INT16U           errnum;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    polarity_mode = dpcls.mcu.vs.polarity.mode;

    switch (sym_const->sym_idx)
    {
        case STC_POSITIVE:                  // Request positive polarity
            dpcls.mcu.vs.polarity.mode = FGC_POL_MODE_POSITIVE;
            Set(sta.cmd, DDOP_CMD_POL_POS);
            break;

        case STC_NEGATIVE:                  // Request negative polarity
            dpcls.mcu.vs.polarity.mode = FGC_POL_MODE_NEGATIVE;
            Set(sta.cmd, DDOP_CMD_POL_NEG);
            break;

        default:                            // Should never happen
            return (FGC_BAD_PARAMETER);
    }

    if (polarity_mode != dpcls.mcu.vs.polarity.mode)
    {
        dpcls.mcu.vs.polarity.changed = TRUE;

        PubProperty(p, NON_PPM_USER, TRUE);
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U SetLogCaptureUser(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Set LOG.CAPTURE.USER
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  errnum;
    INT32S * new_val;
    INT16S  user;

    // If LOG.CAPTURE.CYC_CHK_TIME is not zero, meaning that the capture log is already used for a cycle
    // check warning, do not set LOG.CAPTURE.USER

    if (dpcls.dsp.log.capture.cyc_chk_time.unix_time)
    {
        return (FGC_LOG_WAITING);
    }

    // Parse user value

    errnum  = ParsValueGet(c, p, (void **) &new_val);                      // Get new value from command packet

    if (!c->last_par_f)
    {
        return (FGC_BAD_PARAMETER);
    }

    if (errnum == 0)
    {
        user = *new_val;

        // LOG.CAPTURE.USER can be modified from -1 to a positive value (i.e. ask to capture a log for the
        // specified user), or from a positive value to -1 (i.e. cancel current capture log). All other
        // transitions are forbidden.

        if (user == dpcls.dsp.log.capture.user)
        {
            errnum = FGC_OK_NO_RSP;
        }
        else if ((user == -1  && dpcls.dsp.log.capture.user >= 0) ||
                 (user >= 0   && dpcls.dsp.log.capture.user == -1))
        {
            c->changed_f = TRUE;
            dpcls.dsp.log.capture.user = user;
        }
        else
        {
            errnum = FGC_BAD_STATE;
        }
    }

    return (errnum);
}


uint16_t SetDevName(struct cmd * c, struct prop * p)
{
#if (FGC_CLASS_ID == 63)
    static bool init = false;

    // TODO Move this!

    if(!init)
    {
        dev.test_strings[0] = &dev.test_data[0];
        dev.test_strings[1] = &dev.test_data[44];
        dev.test_strings[2] = &dev.test_data[88];
        dev.test_strings[3] = &dev.test_data[132];

        init = true;
    }
#endif

    uint16_t errnum = ParsSet(c, p);

    if(errnum == 0)
    {
        interFgcPopulateStatus();
    }

    return errnum;
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: set.c
\*---------------------------------------------------------------------------------------------------------*/
