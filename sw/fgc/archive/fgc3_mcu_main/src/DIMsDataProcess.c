/*!
 *  @file     DIMsDataProcess.h
 *
 * Purpose:      FGC3 MCU Software - Process data received via QSPI bus
 *
 */

#define DIMSDATAPROCESS_GLOBALS
#define DEFPROPS_INC_ALL    // defprops.h


#include <DIMsDataProcess.h>

#include <class.h>          // Include class dependent definitions
#include <definfo.h>        // for FGC_CLASS_ID
#include <defprops.h>
#include <dev.h>            // for dev global variable
#include <diag.h>           // for diag and qspi_misc global variable
#include <fbs.h>            // for fbs global variable, FbsOutBuf()
#include <fgc_errs.h>       // for FGC_DSP_NOT_AVL, FGC_BAD_ARRAY_IDX, FGC_BAD_GET_OPT
#include <fgc_log.h>        // for struct fgc_log_header, struct fgc_log_sig_header
#include <macros.h>
#include <mcu_dsp_common.h> // for AbsTimeUs_IsEqual()
#include <memmap_mcu.h>
#include <mst.h>            // for mst global variable
#include <mst.h>            // for mst global variable
#include <os.h>             // for OSTskSuspend()
#include <prop.h>           // for PropValueGet()
#include <sleep.h>
#include <stdio.h>          // for fprintf()
#include <string.h>         // for memset()
#include <fgc/fgc_db.h>
#if defined(__RX610__)
#include <structs_bits_big.h>
#endif


// Constants


// Internal structures, unions and enumerations

// we can make up to 3 log request
struct TDimLogRequests
{
    //    INT8U    number_of_requests;                    // (1-3) number of logical DIMs to log
    INT8U    logical_dim_to_log_for_req[REQUESTS_FOR_DIM_LOG];
    INT16U   entry_for_req[REQUESTS_FOR_DIM_LOG];   // list of sample index
};

struct dims_data_process_local_t
{
    BOOLEAN       trigger_fired;                        //!< DIM trigger detected flag
    INT32U    reset_req_packed;                         //!< DIM board reset request bit packed in INT32U
    struct TDimLogRequests log;                         //!< we can make up to 3 log request

    INT32U      flat_qspi_board_is_present_packed[3]; //!< DIM present bit packed in INT32U
    //!< 0 is [1] & [2], DIMS present in the two consecutive cycles
    //!< 1 is DIMS present in actual cycle
    //!< 2 is DIMS present in previous cycle

    INT32U      flat_qspi_board_is_valid_packed;    //!< DIM board is valid bit packed in INT32U
    INT32U      flat_qspi_not_triggered_packed;     //!< not triggered bit packed in INT32U

    INT32U      scantime_us[QSPI_BRANCHES];     //!< Scan start times relative to start of second
    INT32U      prev_scantime_us[QSPI_BRANCHES];// !<Previous scan times

    INT32U      unix_time;                      //!< Unix time for current cycle Private
    INT32U      prev_unix_time;                 //!< Unix time for previous cycle Private
} local;

// Common diagnostic (DIM) data processing
struct TDimLogData
{
    INT16U    r[4];
};

// 256Kb space buffer allocated

// to handle FGC_MAX_DIMS (20) slots of FGC_LOG_DIM_LEN (1600) elements
// each element has 8 bytes data

// FGC_MAX_DIMS * FGC_LOG_DIM_LEN * sizeof(TDimLogData)
//     20       *    1600         *        8              = 256000
struct TDimLogEntry
{
    struct TDimLogEntryStatus   status;
    struct TDimLogData          data[FGC_LOG_DIM_LEN];
};

struct TDimLogEntry __attribute__((section("dim_log_space"))) dim_log[FGC_MAX_DIMS];

// Internal function declarations

/*!
 *
 */
static void LookForTriggerEvent(void);

/*!
 *
 */
static void ProcessReceivedQSPIdata(INT16U ms_mod_20);

/*!
 *
 */
static void PutDIMsamplesInLog(INT32U logical_dim, struct abs_time_us * sample_time, INT32U * samples);

/*!
 *
 */
static void ScaleDimAnalogueData(void);

/*!
 *
 */
static void ValidatePresentDimBoards(INT32U scan_step);

/*!
 *   Extracts the analogue signal label from the DimDB code and copies to lbl_buf (max len defined in buf_len).
 *   If the property sym_idx is for SUB1-SUB8 or SUB1B-SUB8B then the sub converter index will be derived and
 *   substituted for any # characters found in the name.  The function returns the length of the label not
 *   including the null termination.
 */
static INT16U DimAnaLabel(char * lbl_buf, INT16U buf_len, const char * dim_ana_lbl, INT16U sym_idx);

// External function definition

void InitQspiBus(void)
{
    INT16U      b;
    INT16U      ii;

    // initial reset of all DIMS

    // when QSPI_CLK goes HIGH the DIM starts checking the line, if QSPI_CLK remains HIGH at least 15.7us
    // this is recognised as a RESET cmd by the DIM card
    // (for branches A & B simultaneously)
    QSM_CTRL_P |= (QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16);

    sleepUs(20);

    // reset done, we release the line to let time for the LOAD cmd
    QSM_CTRL_P &= ~(QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16);
    // when QSPI_CLK goes low the DIM starts checking the line, if QSPI_CLK remains low at least 15.7us
    // this is recognised as a LOAD cmd, so the next DIM register gets ready to by transmitted with the next
    // 256 clk burst

    for (ii = 0; ii < 16; ii++)
    {
        // Can never be returned by a DIM (DIM register ID:3 doesn't exist)
        ((volatile INT16U *) QSM_TRA_32)[ii] = 0xBAB0 + ii;
        ((volatile INT16U *) QSM_TRB_32)[ii] = 0xBAB0 + ii;
    }

    // Prepare diag header with sync bytes (6 x 0xFE)

    for (b = 0; b < 2; b++)                             // For double buffer
    {
        diag.term_buf[b].data.data_status = 0x0F;        // Set status in the header to all OK
        diag.term_buf[b].data.class_id = FGC_CLASS_ID;   // Set class ID in the header

        for (ii = 0; ii < FGC_DIAG_N_SYNC_BYTES; ii++)  // Prepare each sync byte
        {
            diag.term_buf[b].sync[ii] = FGC_DIAG_SYNC;
        }
    }

    memset(&(qspi_misc), 0, sizeof(qspi_misc));

    memset(&(dim_collection.gains),   0.0, sizeof(dim_collection.gains));
    memset(&(dim_collection.offsets), 0.0, sizeof(dim_collection.offsets));
    memset(&(dim_collection.trig_us), 0.0, sizeof(dim_collection.trig_us));

    // flat_qspi_board_number gives the flat qspi addr for the corresponding item in the dim_collection[]
    // address values
    // 0x00..0x1F are real physical DIM boards
    // 0x80, b7=1 means a composite virtual DIM
    // 0xff means empty
    memset(dim_collection.flat_qspi_board_number, 0xFF, FGC_MAX_DIMS);

    // not triggered bit from flat qspi addr packed in INT32U
    // by default non was triggered
    local.flat_qspi_not_triggered_packed = 0xFFFFFFFF;

    // Clear DIM logs buffer
    memset(dim_log,  0, sizeof(dim_log));
}


void QSPIbusStateMachine(void)
{
    /*
     * The reset process for the diag uses 3 states because there needs to be a delay between the end
     * of the reset pulse (clock line high) and the start of the scan.
     *
     * reset process
     * a) DIAG_RESET = clock high
     * b) DIAG_START = clock low
     * c) DIAG_SCAN  = trigger
     * d) DIAG_RUN   = readout + next trigger
     *
     * for next DIM register only
     *    DIAG_RUN = readout + next trigger
     *
     */

    // Act according to 20ms time

    switch (mst.ms_mod_20)
    {
        case 0:
        {
            // At least 15.7us must passed with QSPI_CLK low after the last read (256 clk burst) or RESET for the LOAD cmd
            // to be recognised, so this acquire with this new 256 clk burst the next DIM register

            local.prev_scantime_us[BRANCH_A] = local.scantime_us[BRANCH_A];

            // Set READ A bit to fire the state machine which generates the 256 QSPI_CLK to shift
            // the active DIM register in the reception buffer

            QSM_CTRL_P |= QSM_CTRL_READA_MASK16;

            local.scantime_us[BRANCH_A] = UTC_US_P;

            // used for the trigger
            local.prev_unix_time = local.unix_time;
            local.unix_time      = mst.time.unix_time;

            break;
        }

        case 10:
        {
            // At least 15.7us must passed with QSPI_CLK low after the last read (256 clk burst) for the LOAD cmd
            // to be recognised, so this acquire with this new 256 clk burst the next DIM register

            local.prev_scantime_us[BRANCH_B] = local.scantime_us[BRANCH_B];

            // Set READ B bit to fire the state machine which generates the 256 QSPI_CLK to shift
            // the active DIM register in the reception buffer

            QSM_CTRL_P |= QSM_CTRL_READB_MASK16;

            local.scantime_us[BRANCH_B] = UTC_US_P;

            break;
        }

        case 1:   // Dig0        DIM register ID:0  x 16 DIM boards
        case 2:   // Dig1        DIM register ID:1  x 16 DIM boards
        case 3:   // Ana0        DIM register ID:4  x 16 DIM boards
        case 4:   // Ana1        DIM register ID:5  x 16 DIM boards
        case 5:   // Ana2        DIM register ID:6  x 16 DIM boards
        case 6:   // Ana3        DIM register ID:7  x 16 DIM boards
        {
            ProcessReceivedQSPIdata(mst.ms_mod_20);

            // Start next scan

            QSM_CTRL_P |= QSM_CTRL_READA_MASK16;

            break;
        }

        case 7: //  TrigCounter DIM register ID:2  x16 DIM boards
            ProcessReceivedQSPIdata(mst.ms_mod_20);
            break;

        case 11:  // Dig0        DIM register ID:0  x 16 DIM boards
        case 12:  // Dig1        DIM register ID:1  x 16 DIM boards
        case 13:  // Ana0        DIM register ID:4  x 16 DIM boards
        case 14:  // Ana1        DIM register ID:5  x 16 DIM boards
        case 15:  // Ana2        DIM register ID:6  x 16 DIM boards
        case 16:  // Ana3        DIM register ID:7  x 16 DIM boards
        {
            ProcessReceivedQSPIdata(mst.ms_mod_20);

            // Start next scan

            QSM_CTRL_P |= QSM_CTRL_READB_MASK16;

            break;
        }

        case 17: // TrigCounter DIM register ID:2  x16 DIM boards
            ProcessReceivedQSPIdata(mst.ms_mod_20);

            // branch A triggerCounter registers b15 collection
            diag.data.r.software[ISR_RUN_TIME][BRANCH_A_LATCHED_TRIGS]   = diag.triggers_from_ms[ 7];
            // branch B triggerCounter registers b15 collection
            diag.data.r.software[ISR_RUN_TIME][BRANCH_B_LATCHED_TRIGS]   = diag.triggers_from_ms[17];
            // branch A dig0 registers b15 collection
            diag.data.r.software[ISR_RUN_TIME][BRANCH_A_UNLATCHED_TRIGS] = diag.triggers_from_ms[ 1];
            // branch B dig0 registers b15 collection
            diag.data.r.software[ISR_RUN_TIME][BRANCH_B_UNLATCHED_TRIGS] = diag.triggers_from_ms[11];

            LookForTriggerEvent();

            break;

        case 8:
        case 9:
            break;

        case 18:

            // we fire the reset in ms 18 for FGC3 instead of ms 19 like in FGC2 because ...
            // if we fire in ms 19, we have the QSPI_CLK in HIGH during ms 19 and going to LOW in ms 0
            // and in the same ms 0 the first burst read is fire but too close from the start of QSPI_CLK in LOW
            // (less than 15.7us) so this little LOW time is not recognised as LOAD by the DIMS
            // starting reset in ms 18 let going to LOW in ms 19 and remain in LOW until ms 0 fires the read

            if (local.trigger_fired)
            {
                local.trigger_fired = FALSE;
                diag.req_QSPI_reset = 100;                      // Set 2s (100 x 20ms) timeout for DIAG reset
            }

            if (local.reset_req_packed)                // If requests a DIAG reset
            {
                local.reset_req_packed = 0x00000000;   // Clear reset request bits

                diag.req_QSPI_reset = 1;                        // Set immediate DIAG reset request
                diag.sync_resets++;                             // DIAG.SYNC_RESETS, increment diag reset request counter
            }

            if (diag.req_QSPI_reset != 0)                       // If any DIAG reset request is pending
            {
                diag.req_QSPI_reset--;

                if (diag.req_QSPI_reset == 0)                   // If reset request timer expired
                {
                    // 1st RESET state
                    // Set QSPI_CLK high, start of RESET signal
                    // when QSPI_CLK goes HIGH the DIM starts checking the line, if QSPI_CLK remains HIGH at least 15.7us
                    // this is recognised as a RESET cmd by the DIM card
                    // (for branches A & B simultaneously)
                    QSM_CTRL_P |= (QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16);
                    // The 2nd step is done in millisecond 19

                    // when QSPI_CLK goes HIGH the DIM starts checking the line, if QSPI_CLK remains HIGH at least 15.7us
                    // this is recognised as a RESET cmd by the DIM card
                }
            }

            break;

        case 19:
            if ((QSM_CTRL_P & (QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16)) != 0)
            {
                // 2nd RESET state (the 1st step was fired in millisecond 18)
                // Set QSPI_CLK low, generates the fall edge and the low level for the needed RESET signal
                // (for bus A & B simultaneously)

                QSM_CTRL_P &= ~(QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16);
            }

            ScaleDimAnalogueData();             // Process and log raw values
            ValidatePresentDimBoards(1);
            break;
    }
}

INT16U DimGetDimLog(struct cmd * c, struct prop * p)
{
    /*
     * There is no ASCII access to these properties.
     * The function needs the support of the DSP to acquire the data from the TMS320C32s memory.
     * The DSP will service one GetLog request per millisecond and always returns 8 samples
     * for between 1 and 3 DIMs.
     */


    fgc_db_t    dim_type_idx; 
    INT8U       sub_sym_idx; 
    fgc_db_t    e;
    prop_size_t n;
    INT16U      s;
    BOOLEAN     stopped_f;
    INT16U      n_sigs;
    fgc_db_t      chan_idx[12];
    INT16U      chan_offset_us[12];
    INT16U   *  buf_data_p;
    INT16U      twait;
    INT16U      dTsync;
    INT16U      dTlast;
    INT16U      dIdx;
    INT32U      d_unix_time;
    INT32U      sync_time = fbs.log_sync_time;;
    INT32U      unix_time;
    INT16U      ms_time;
    INT16U      errnum;
    
    //char      aux[FGC_LOG_SIG_UNITS_LEN];

    struct prop *   dim_info_ptr;

    struct TDimLogEntryStatus       log_status1; // Log status before readout
    struct TDimLogEntryStatus       log_status2; // Log status after readout

    union                                               // Union to reduce impact on stack
    {
        struct fgc_log_header      log;
        struct fgc_log_sig_header  sig;
    } header;

    // Check that Get command and options are valid for a DIM log

    if (c->to != c->from || c->n_arr_spec != 2)         // If array indecies are more than one
    {
        return (FGC_BAD_ARRAY_IDX);                     // Report error
    }

    if (!(c->getopts & GET_OPT_BIN))                    // If BIN get option not specified
    {
        return (FGC_BAD_GET_OPT);                       // Report error
    }

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    // Identify active analogue channels from the DIMs

    dim_type_idx = SysDbDimDbType(dev.sys.sys_idx, c->to);
    local.log.logical_dim_to_log_for_req[0] = c->to;

    //--------------------------------------------------------------------
    // from DSP code
    memcpy(&(log_status1), &(dim_log[ local.log.logical_dim_to_log_for_req[0] ].status), sizeof(struct TDimLogEntryStatus));

    // Take copy of status for DIAG.LOGSTAT debug property
    memset(dbg_dim_log_status, 0, sizeof(struct TDimLogEntryStatus));

    //  dbg_dim_log_status = log_status1;
    memcpy(& (dbg_dim_log_status[0]), & (log_status1), sizeof(struct TDimLogEntryStatus)); // protected from interrupts

    //--------------------------------------------------------------------

    stopped_f = !(log_status1.samples_to_acq);       // Get state of first DIM log

    // Get DIM link structure
    dim_info_ptr = &diag.dim_props[local.log.logical_dim_to_log_for_req[0]];
    sub_sym_idx = (INT8U)(dim_info_ptr ? dim_info_ptr->sym_idx : 0);     // Get DIM name sym index

    n_sigs = 0;

    for( e = 0; e < FGC_N_DIM_ANA_CHANS ; e++)          // For each analog channel (0-3)
    {
        if(DimDbIsAnalogChannel(dim_type_idx,e))
        {
            chan_idx    [n_sigs] = e;                                   // Remember index to data

            // The chan_offset_us must be done on two lines to not provoke a compiler bug
            // ToDo: is for HC16 compiler ??
            // Calc sig time offset
            chan_offset_us[n_sigs]  = (SysDbDimBusAddress(dev.sys.sys_idx,local.log.logical_dim_to_log_for_req[0]) & 0x10 ? 11680 : 1680);
            chan_offset_us[n_sigs] += (1000 * e);

            n_sigs++;                                                   // Count number of active sigs
        }
    }

    //  sample_size = sizeof(INT16U) * n_sigs;

    // Calculate synchronisation

    unix_time = log_status1.time.unix_time;
    ms_time   = (INT16U)(log_status1.time.us_time / 1000);

    if (!stopped_f)                                 // If logging active
    {
        if (!sync_time)                             // If sync time is not active
        {
            sync_time = unix_time;                  // Set sync time to start of current second
            dTsync    = ms_time;
        }
        else                                        // else sync is active
        {
            d_unix_time = unix_time - sync_time;                // Must be done to ensure 32-bit sub
            // ToDo: bug in which compiler??? TMS320C32 ?
            dTsync = (INT16U)d_unix_time * 1000 + ms_time;      // !!!! Compiler bug:do not merge these lines
        }

        twait = 2500 - (261*n_sigs); // ajust for FGC3

        //dIdx   = ((n_sigs * FGC_LOG_DIM_LEN) / 6 /* 6 bytes/ms over FIP fieldbus */
        //          + twait - dTsync) / FGC_FIELDBUS_CYCLE_PERIOD_MS;
        dIdx   = ((n_sigs * FGC_LOG_DIM_LEN) / 6 + twait - dTsync) / FGC_FIELDBUS_CYCLE_PERIOD_MS;
        dTlast = dIdx * FGC_FIELDBUS_CYCLE_FREQ;
        
        log_status1.idx = (INT16U)log_status1.idx + dIdx;

        unix_time += dTlast / 1000;                 // Adjust time to be of last sample
        ms_time   += dTlast % 1000;

        if (ms_time > 999)
        { 
            ms_time -= 1000;
            unix_time++;
        } 
    }

    local.log.entry_for_req[0] = (log_status1.idx +1) % FGC_LOG_DIM_LEN;

    // Generate log header

    header.log.version   = FGC_LOG_VERSION;
    header.log.info      = (stopped_f ? FGC_LOG_BUF_INFO_STOPPED : 0);
    header.log.n_signals = n_sigs;
    header.log.n_samples = FGC_LOG_DIM_LEN;
    header.log.period_us = 20000L;                      // 20000us = 20 ms
    header.log.unix_time = unix_time;
    header.log.us_time   = (INT32U)ms_time * 1000L;

    CmdStartBin(c, p, sizeof(header.log) +                      // Write binary data header
                      sizeof(header.sig) * n_sigs +
                      sizeof(INT16U)     * n_sigs * FGC_LOG_DIM_LEN);

    FbsOutBuf((INT8U *) &header.log, sizeof(header.log), c);    // Write log header

    // Generate signal headers

    header.sig.type = FGC_LOG_TYPE_INT16U;
    header.sig.info = 0;

    for (s = 0; s < n_sigs; s++)
    {
        header.sig.us_time_offset = (INT32S)chan_offset_us[s];
        header.sig.gain           = DimDbAnalogGain(dim_type_idx,chan_idx[s]);
        header.sig.offset         = DimDbAnalogOffset(dim_type_idx,chan_idx[s]);
        header.sig.label_len      = DimAnaLabel(header.sig.label,
                                                FGC_LOG_SIG_LABEL_LEN,
                                                DimDbAnalogLabel(dim_type_idx,chan_idx[s]),
                                                sub_sym_idx);
        header.sig.units_len       = 0;


        //MemCpyStrFar(aux,DimDbAnalogUnits(dim_type_idx,chan_idx[s]));
        strncpy(header.sig.units, DimDbAnalogUnits(dim_type_idx,chan_idx[s]), FGC_LOG_SIG_UNITS_LEN);

        FbsOutBuf((INT8U *) &header.sig, sizeof(header.sig), c);        // Write signal header
    }

    // Wait for synchronisation

    if (!stopped_f)                                        // If logging active
    {
        CmdWaitUntil(sync_time + 2, 500);                         // Wait till 2.5 s after sync time
    }

    // Get and write data in blocks of 8 samples

    for (n = 0; n < FGC_LOG_DIM_LEN; n += 8)
    {
        //      OSTskSuspend();                                 // Resumed by MstTsk on next millisecond

        //-------------------------------------------------------------
        // this was in the DSP
        // ToDo: this needs to be integrated
        INT32U                      sample_idx;
        struct TDimLogData     *    destination_ptr;

        // storing
        destination_ptr  = (struct TDimLogData *)&dpcom.fcm_prop.blk.intu;      // Address of results buf in DPRAM
        sample_idx = local.log.entry_for_req[0];              // restore previously saved sample index

        for (e = 0; e < 8; e++)                              // For 8 samples
        {

            *destination_ptr = dim_log[ local.log.logical_dim_to_log_for_req[0] ].data[sample_idx];
            destination_ptr++;

            sample_idx++;

            if (sample_idx >= FGC_LOG_DIM_LEN)
            {
                sample_idx = 0;
            }

            local.log.entry_for_req[0] = sample_idx;              // Save new sample index

        }

        // retrieving
        buf_data_p = (INT16U *)&dpcom.fcm_prop.blk.intu; // Get pointer to start of 8 samples in DPRAM

        for (e = 0; e < 8; e++)                              // For each of 8 samples
        {
            for (s = 0; s < n_sigs; s++)                             // For each signal
            {
                FbsOutShort((INT8U *)&buf_data_p[chan_idx[s]], c);         // Write the signal to the FIP
            }

            buf_data_p += (FGC_N_DIM_ANA_CHANS * 1);       // Advance to next sample
        }
    }


    //-------------------------------------------------------------------------
    // from DSP code
    memcpy(& (log_status2), & (dim_log[ local.log.logical_dim_to_log_for_req[0] ].status), sizeof(struct TDimLogEntryStatus));


    // Take copy of status for DIAG.LOGSTAT debug property

    memset(dbg_dim_log_status, 0, sizeof(struct TDimLogEntryStatus));

    //  dbg_dim_log_status = log_status2;

        // protected from interrupts
    memcpy(& (dbg_dim_log_status[0]), & (log_status2), sizeof(struct TDimLogEntryStatus));

    //-------------------------------------------------------------------------
    // Check if logging state has changed
    if (!(log_status1.samples_to_acq) != !(log_status2.samples_to_acq))
    {
        return (FGC_LOG_NOT_SYNCED);
    }

    return (0);
}

INT16U DimGetDimNames(struct cmd * c, struct prop * p)
{
    prop_size_t                 n;
    INT16U                      errnum;                 // Error number
    struct TAccessToDimInfo  *  dim_info_ptr;
    struct prop        *        dim_props;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    while (n--)    // For all elements
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)                             // Print index if required
        {
            return (errnum);
        }

        dim_props = &diag.dim_props[c->from]; // [logical dim]
        dim_info_ptr = (struct TAccessToDimInfo *)dim_props->value;

        fprintf(c->f,   "%s:%u:0x%02X",
                SysDbDimName(dev.sys.sys_idx, dim_info_ptr->logical_dim),
                dim_info_ptr->logical_dim,
                dim_info_ptr->flat_qspi_board_number);

        c->from += c->step;     // Next element
    }

    return (0);
}


INT16U DimGetAnaLbls(struct cmd * c, struct prop * p)
{
    prop_size_t                 n;
    INT16U                      offset;
    INT16U                      errnum;                         // Error number
    char                        ana_lbl[FGC_LOG_SIG_LABEL_LEN + 1];

    if ((c->n_arr_spec == 2)
        && (c->to >= FGC_N_DIM_ANA_CHANS)
       )
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    p->n_elements = FGC_N_DIM_ANA_CHANS;        // Set property size to number of analog channels

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    p->n_elements = p->max_elements;

        
    offset = FGC_MAX_DIM_BUS_ADDR * c->from;

    const fgc_db_t dim_type_idx =  SysDbDimDbType(dev.sys.sys_idx, ((struct TAccessToDimInfo *)p->value)->logical_dim);

    while (n--)                                 // For all elements
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)             // Print index if required
        {
            return (errnum);
        }

        DimAnaLabel(ana_lbl, (FGC_LOG_SIG_LABEL_LEN + 1), DimDbAnalogLabel(dim_type_idx,c->from), p->sym_idx);

        fprintf(c->f, "0x%02X:%s:%s", offset, (char *)ana_lbl,  DimDbAnalogUnits(dim_type_idx,c->from));

        offset += (c->step * FGC_MAX_DIM_BUS_ADDR);
        c->from += c->step;
    }

    return (0);
}


INT16U DimGetDigLbls(struct cmd * c, struct prop * p, INT16U bank)
{
    prop_size_t                 n;
    INT16U                      errnum;                 // Error number

    if ((c->n_arr_spec == 2)
        && (c->to >= FGC_N_DIM_DIG_INPUTS)
       )
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    p->n_elements = FGC_N_DIM_DIG_INPUTS;       // Set property size to number of digital channels

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    p->n_elements = p->max_elements;

    const fgc_db_t dim_type_idx =  SysDbDimDbType(dev.sys.sys_idx, ((struct TAccessToDimInfo *)p->value)->logical_dim);
   
    while (n--)                                 // For all elements
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)             // Print index if required
        {
            return (errnum);
        }

        fprintf(c->f, "0x%04X:%c:%s:%s:%s",
                (1 << c->from),
                (DimDbDigitalIsFault(dim_type_idx,bank+4,c->from) ? 'T' : 'S'), // "TRG" "STA"
                DimDbDigitalLabel(dim_type_idx,bank+4,c->from),
                DimDbDigitalLabelOne(dim_type_idx,bank+4,c->from),
                DimDbDigitalLabelZero(dim_type_idx,bank+4,c->from));

        c->from += c->step;
    }

    return (0);
}


INT16U DimGetAna(struct cmd * c, struct prop * p)
{
    prop_size_t                 n;                      // Number of element in array range
    FP32                        ana_val;
    FP32            *           ana_val_ptr;            // Analogue value pointer
    INT16U                      errnum;                 // Error number
    char                        ana_lbl[FGC_LOG_SIG_LABEL_LEN + 1];
    struct TAccessToDimInfo  *  dim_info_ptr;


    if (c->n_arr_spec == 2 && c->to >= FGC_MAX_DIMS)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    c->getopts |= GET_OPT_NOIDX;                        // Suppress automatic indexes
    dim_info_ptr = (struct TAccessToDimInfo *)p->value; // Get DIM info structure for this DIM
    const fgc_db_t dim_type_idx = SysDbDimDbType(dev.sys.sys_idx, dim_info_ptr->logical_dim);

    p->n_elements = FGC_N_DIM_ANA_CHANS;        // Set number of channels

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    p->n_elements = p->max_elements;

    while (n--)                                 // For all elements in the array range
    {

        if (DimDbIsAnalogChannel(dim_type_idx,c->from))  // If input is in use
        {
            errnum = PropValueGet(c, &PROP_DIAG_ANASIGS, NULL,
                                  dim_info_ptr->logical_dim + FGC_MAX_DIMS * c->from, (INT8U **)&ana_val_ptr);

            if (errnum)
            {
                return (errnum);
            }

            ana_val = *ana_val_ptr;

            DimAnaLabel(ana_lbl, (FGC_LOG_SIG_LABEL_LEN + 1), DimDbAnalogLabel(dim_type_idx, c->from), p->sym_idx);

            errnum = CmdPrintIdx(c, c->from);

            if (errnum)
            {
                return (errnum);
            }

            fprintf(c->f, "%u:%-15s:%10.3E:%s", (unsigned int) c->from, (char *)ana_lbl, (double) ana_val, DimDbAnalogUnits(dim_type_idx,c->from));
        }

        c->from += c->step;
    }

    return (0);
}


INT16U DimGetDig(struct cmd * c, struct prop * p)
{
    prop_size_t                         n;
    INT16U                              bit_state;
    INT16U                              errnum;
    fgc_db_t                              flat_board_number;
    fgc_db_t                               bank;               // 0 refers to digital_0 and 1 refers to digital_1
    fgc_db_t                              bit_in_bank;
    INT16U                              bit_mask;
    //INT16U                              faults_mask;
    BOOLEAN                             trigger_is_latched;
    BOOLEAN                             unlatched_trig;
    static char            *            str_TRG_STA[] = { "FLT", "STA" };
    struct TAccessToDimInfo      *      dim_info_ptr;

    c->getopts |= GET_OPT_NOIDX;                              // Suppress indexes

    dim_info_ptr = (struct TAccessToDimInfo *)p->value;
    const fgc_db_t dim_type_idx = SysDbDimDbType(dev.sys.sys_idx, dim_info_ptr->logical_dim);
    flat_board_number = SysDbDimBusAddress(dev.sys.sys_idx, dim_info_ptr->logical_dim);

    if (flat_board_number & 0x80) // If DIM is a composite (ANA only)
    {
        return (0);          // nothing to do
    }

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    while (n--) // For all elements
    {
        // pass from [0..23] to [bank][0..11]
        // digital signals 0..11 comes from digital_0 and signals 12..23 comes from digital_1

        // select digital_0 or digital_1 based on the signal number [0..23]
        bank = (c->from / FGC_N_DIM_DIG_INPUTS);

        // this refers to which of the 12 digital signals (bits) in the bank we are talking about
        bit_in_bank    = (c->from % FGC_N_DIM_DIG_INPUTS);

        if (!DimDbIsDigitalInput(dim_type_idx, bank+4, bit_in_bank))     // If input is not used
        {
            c->from += c->step; // Skip to next element
            continue;
        }

        bit_mask  = 0x0001 << bit_in_bank;
        bit_state = diag.data.a.digital[bank][flat_board_number] & bit_mask;

        trigger_is_latched = (diag.data.a.latched[bank][flat_board_number] & bit_mask) != 0;
        unlatched_trig = (diag.data.f.analog_0[flat_board_number] & 0x8000) != 0;

        errnum = CmdPrintIdx(c, c->from);                       // Field delimiter

        if (errnum)
        {
            return (errnum);
        }

        fprintf(c->f, "%2u:%s|%-31s:%-7s:%2s:%2s",
                (unsigned int) c->from,
                str_TRG_STA[!(DimDbDigitalIsFault(dim_type_idx, bank+4, bit_in_bank))],
                DimDbDigitalLabel(dim_type_idx, bank+4, bit_in_bank),
                (bit_state      ?	DimDbDigitalLabelOne(dim_type_idx, bank+4, bit_in_bank) :
                					DimDbDigitalLabelZero(dim_type_idx, bank+4, bit_in_bank)),
                DimDbDigitalIsFault(dim_type_idx, bank+4, bit_in_bank)?(unlatched_trig ? "UT" : ""):"",
                DimDbDigitalIsFault(dim_type_idx, bank+4, bit_in_bank)?(trigger_is_latched ? "LT" : ""):"");

        // Go to next element

        c->from += c->step;
    }

    return (0);
}


INT16U DimGetFaults(struct cmd * c, struct prop * p)
{
    prop_size_t                 n;
    INT16U                      idx;
    INT16U                      errnum;
    fgc_db_t                    flat_board_number;
    fgc_db_t                    bank; // 0 refers to digital_0 and 1 refers to digital_1
    fgc_db_t                    dim_type_idx;
    fgc_db_t                    bit_in_bank;
    INT16U                      bit_mask;
    BOOLEAN                     trigger_is_latched;
    BOOLEAN                     unlatched_trig;
    struct TAccessToDimInfo  *  dim_info_ptr;
    struct prop        *        dim_props;

    if (c->step > 1)                            // If array step is not 1
    {
        return (FGC_BAD_ARRAY_IDX);                     // Report bad array index
    }

    idx = 0;            // for CmdPrintIdx

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    dim_props = &diag.dim_props[c->from]; // [logical dim]

    while (n--)                                  // For all elements
    {
        dim_info_ptr = (struct TAccessToDimInfo *) dim_props->value;
        dim_type_idx = SysDbDimDbType(dev.sys.sys_idx, dim_info_ptr->logical_dim);
        flat_board_number = SysDbDimBusAddress(dev.sys.sys_idx, dim_info_ptr->logical_dim);

        if (flat_board_number & 0x80) // If DIM is a composite (ANA only)
        {
            continue;                 // Skip this DIM
        }

        unlatched_trig = (diag.data.f.analog_0[flat_board_number] & 0x8000) != 0;

        // 0 refers to digital_0 and 1 refers to digital_1
        // digital signals 0..11 comes from digital_0 and signals 12..23 comes from digital_1
        for (bank = 0; bank < FGC_N_DIM_DIG_BANKS; bank++)
        {
            bit_mask = 0x0001;

            for (bit_in_bank = 0; bit_in_bank < FGC_N_DIM_DIG_INPUTS; bit_in_bank++, bit_mask <<= 1)
            {

                if (!DimDbIsDigitalInput(dim_type_idx, bank+4, bit_in_bank)    // If input is not used, or
                    || !(DimDbDigitalIsFault(dim_type_idx, bank+4, bit_in_bank))
                    || !(diag.data.a.digital[bank][flat_board_number] & bit_mask)   // input is not active
                   )
                {
                    continue;    // Skip to next
                }

                trigger_is_latched = (diag.data.a.latched[bank][flat_board_number] & bit_mask) != 0;

                errnum = CmdPrintIdx(c, idx++);           // Field delimiter

                if (errnum)
                {
                    return (errnum);
                }

                fprintf(c->f, "%-6s:%-31s:%-7s:%2s:%2s",
                        SYM_TAB_PROP[dim_props->sym_idx].key.c,
                        DimDbDigitalLabel(dim_type_idx, bank+4, bit_in_bank),
                        DimDbDigitalLabelOne(dim_type_idx, bank+4, bit_in_bank),
                        (unlatched_trig ? "UT" : ""),
                        (trigger_is_latched ? "LT" : ""));
            }
        }

        dim_props++;
    }

    return (0);
}


// Internal function definitions

INT16U DimAnaLabel(char * lbl_buf, INT16U buf_len, const char * dimdb_ana_lbl, INT16U sym_idx)
{
    char        ch;
    INT16U      n_ch;

    if (!dimdb_ana_lbl)                          // If no label present
    {
        *lbl_buf = '\0';                                // Return empty string
        return (0);
    }

    n_ch  = 0;

    do                                          // Copy string until buffer full
    {
        ch = *dimdb_ana_lbl++;

        *(lbl_buf++) = ch;

        if (ch)
        {
            n_ch++;
        }
    }
    while (ch && n_ch < buf_len);

    return (n_ch);
}


void LookForTriggerEvent(void)
{
    /*
     *   Analyse the trigger data from the DIMs
     * and identify the trigger time in the event that a trigger input has been activated.
     *
     * Inputs to the processing are:
     *
     *      local.flat_qspi_board_is_valid_packed
     *      local.flat_qspi_not_triggered_packed
     *      qspi_misc.unix_time                                            Unix time for current cycle
     *      qspi_misc.scantime_us                                          us scan times from current cycle
     *      qspi_misc.prev_scantime_us                                     us scan times from previous cycle
     *
     * And the outputs are:
     *
     *      dim_collection.trig_s[]                                 DIM trigger times (Unixtime/us time)
     *      dim_collection.trig_us[]
     *      dim_collection.evt_log_state[]                          DIM event log state (NONE,WATCH,TRIG)
     *
     *
     * The MCU will pack the the unlatched and latched bits on ms 17 of 20 so this function is run
     * on ms 1 of 20 of the following cycle.
     *
     * Setting the dim_collection.evt_log_state[] to DIM_EVT_TRIG is the trigger for
     * the MCU to write the active fault(s) and all status bits for the triggered DIM to the event log.
     * One second later it will reset the bus to clear the latch and allow additional faults to be logged.
     * This is signalled by the DIM_EVT_WATCH state.
     */

    INT32U              logical_dim;
    INT32U              qspi_branch;
    INT32U              flat_board_number;
    INT32U              bit_in_packed;
    INT32U              dim_board_has_latched_trig;
    INT32U              dim_board_has_UNlatched_trig;
    INT32U              at_least_one_UNlatched_trig;
    INT32U              trig_time_us;                   // Trigger time since start of second in us
    INT32U              trig_time_s;                    // Trigger time in seconds (unix_time)
    union TUnion32Bits  tmp;

    at_least_one_UNlatched_trig = FALSE;

    // scan the DIM list
    for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        flat_board_number = dim_collection.flat_qspi_board_number[logical_dim];

        if (flat_board_number < FGC_MAX_DIM_BUS_ADDR)      // If it is a physical DIM board
        {
            // bit position corresponding to this board, when packed in INT32U
            bit_in_packed = 0x00000001 << flat_board_number;

            // process only valid boards
            if (bit_in_packed & local.flat_qspi_board_is_valid_packed)
            {
                tmp.part.lo = diag.data.r.software[ISR_RUN_TIME][BRANCH_A_UNLATCHED_TRIGS];
                tmp.part.hi = diag.data.r.software[ISR_RUN_TIME][BRANCH_B_UNLATCHED_TRIGS];

                dim_board_has_UNlatched_trig = bit_in_packed & tmp.int32u;

                tmp.part.lo = diag.data.r.software[ISR_RUN_TIME][BRANCH_A_LATCHED_TRIGS];
                tmp.part.hi = diag.data.r.software[ISR_RUN_TIME][BRANCH_B_LATCHED_TRIGS];

                dim_board_has_latched_trig = bit_in_packed & tmp.int32u;

                if (dim_board_has_UNlatched_trig)
                {
                    at_least_one_UNlatched_trig = TRUE;
                }

                if (dim_board_has_latched_trig)
                {
                    // check if it is the 1st detection
                    if (bit_in_packed & local.flat_qspi_not_triggered_packed)
                    {
                        // get branch 0=A,1=B
                        // flat [0..15] belongs to A, [16..31] belongs to B
                        qspi_branch = flat_board_number / (FGC_MAX_DIM_BUS_ADDR / 2);

                        // clear bit to mark this DIM board as triggered
                        local.flat_qspi_not_triggered_packed &= ~bit_in_packed;

                        // The register trigCounter of DIM board has its 8us clock count

                        trig_time_us = 8 * (diag.data.f.trigCounter[flat_board_number] & 0x0FFF);

                        if (dim_board_has_UNlatched_trig)
                        {
                            trig_time_us += local.prev_scantime_us[qspi_branch];

                            if (trig_time_us < 1000000)   // < 1 second
                            {
                                trig_time_s = local.prev_unix_time;
                            }
                            else
                            {
                                trig_time_s   = local.unix_time;
                                trig_time_us -= 1000000;
                            }
                        }
                        else
                        {
                            trig_time_us += local.scantime_us[qspi_branch];
                            trig_time_s   = local.unix_time;
                        }

                        dim_collection.trig_s[logical_dim] = trig_time_s;
                        dim_collection.trig_us[logical_dim] = trig_time_us;

                        dim_collection.evt_log_state[logical_dim] = DIM_EVT_TRIG;
                        local.trigger_fired = TRUE;
                    }
                }
                else
                {
                    dim_collection.evt_log_state[logical_dim] = (dim_board_has_UNlatched_trig ? DIM_EVT_WATCH : DIM_EVT_NONE);
                    // set bit to mark DIM as not triggered
                    local.flat_qspi_not_triggered_packed |= bit_in_packed;
                }
            }
            else
            {
                dim_collection.evt_log_state[logical_dim] = DIM_EVT_NONE;
                // set bit to mark DIM as not triggered
                local.flat_qspi_not_triggered_packed |= bit_in_packed;
            }

        } // If it is a physical DIM board

    } // end for

    dim_collection.unlatched_trigger_fired = at_least_one_UNlatched_trig;
}


void ProcessReceivedQSPIdata(INT16U ms_mod_20)
{
    struct THandyData
    {
        INT8U       branch; // 0= branch A, 1= branch B, 2=skip
        INT16U   *  place_ptr;
    };

    /*
            the 7 DIM registers arrive in this order
                0 - Dig0        DIM register ID:0
                1 - Dig1        DIM register ID:1
                2 - Ana0        DIM register ID:4
                3 - Ana1        DIM register ID:5
                4 - Ana2        DIM register ID:6
                5 - Ana3        DIM register ID:7
                6 - TrigCounter DIM register ID:2
                                 there is no ID:3
    */

    static const struct THandyData ms_to_store_place[20] =               // ms     branch A
    {
        { 2,        NULL                                    },//  0
        { BRANCH_A, &(diag.data.r.digital_0[BRANCH_A][0])   },//  1       Dig0        DIM register ID:0  x16 DIM boards
        { BRANCH_A, &(diag.data.r.digital_1[BRANCH_A][0])   },//  2       Dig1        DIM register ID:1  x16 DIM boards
        { BRANCH_A, &(diag.data.r.analog_0[BRANCH_A][0])    },//  3       Ana0        DIM register ID:4  x16 DIM boards
        { BRANCH_A, &(diag.data.r.analog_1[BRANCH_A][0])    },//  4       Ana1        DIM register ID:5  x16 DIM boards
        { BRANCH_A, &(diag.data.r.analog_2[BRANCH_A][0])    },//  5       Ana2        DIM register ID:6  x16 DIM boards
        { BRANCH_A, &(diag.data.r.analog_3[BRANCH_A][0])    },//  6       Ana3        DIM register ID:7  x16 DIM boards
        { BRANCH_A, &(diag.data.r.trigCounter[BRANCH_A][0]) },//  7       TrigCounter DIM register ID:2  x16 DIM boards
        { 2,        NULL                                    },//  8
        { 2,        NULL                                    },//  9  branch B
        { 2,        NULL                                    },// 10
        { BRANCH_B, &(diag.data.r.digital_0[BRANCH_B][0])   },// 11       Dig0        DIM register ID:0  x16 DIM boards
        { BRANCH_B, &(diag.data.r.digital_1[BRANCH_B][0])   },// 12       Dig1        DIM register ID:1  x16 DIM boards
        { BRANCH_B, &(diag.data.r.analog_0[BRANCH_B][0])    },// 13       Ana0        DIM register ID:4  x16 DIM boards
        { BRANCH_B, &(diag.data.r.analog_1[BRANCH_B][0])    },// 14       Ana1        DIM register ID:5  x16 DIM boards
        { BRANCH_B, &(diag.data.r.analog_2[BRANCH_B][0])    },// 15       Ana2        DIM register ID:6  x16 DIM boards
        { BRANCH_B, &(diag.data.r.analog_3[BRANCH_B][0])    },// 16       Ana3        DIM register ID:7  x16 DIM boards
        { BRANCH_B, &(diag.data.r.trigCounter[BRANCH_B][0]) },// 17       TrigCounter DIM register ID:2  x16 DIM boards
        { 2,        NULL                                    },// 18
        { 2,        NULL                                    } // 19
    };

    struct THandyData       idx;
    INT16U                  ii;
    INT16U                  triggers;

    idx = ms_to_store_place[ms_mod_20];

    // copy the received data from the 16 DIM boards to the buffer
    if (idx.branch == BRANCH_A)
    {
        memcpy(idx.place_ptr, (void *) QSM_RRA_32, 16 * sizeof(uint16_t));
    }
    else
    {
        if (idx.branch == BRANCH_B)
        {
            memcpy(idx.place_ptr, (void *) QSM_RRB_32, 16 * sizeof(uint16_t));
        }
        else
        {
            // just to be safe and not to copy when place_ptr is NULL
        }
    }

    // also collects the trigger bit (b15) from every register

    // prepare the triggers in a single word to be returned
    // insert trigger bit from left to right
    // triggers b15=DIM15...b1=DIM1, b0=DIM0

    triggers = 0;

    for (ii = 0; ii < 16; ii++)
    {
        triggers = triggers >> 1;

        triggers |= (idx.place_ptr[ii] & 0x8000);
    }

    diag.triggers_from_ms[ms_mod_20] = triggers;
}


void PutDIMsamplesInLog(INT32U logical_dim, struct abs_time_us * sample_time, INT32U * samples)
{
    /*
     * This function is called from ScaleDimAnalogueData() to store a DIM sample in a log buffer.
     * If samples is NULL then a zero sample is stored as a marker.
     * The function only really stores data for class 51 as the other classes do not have DIM logs.
     */

    INT32U      xx;

    xx = dim_log[logical_dim].status.idx + 1;

    if (xx >= FGC_LOG_DIM_LEN)                  // beyond allocated space ?
    {
        xx = 0;
    }

    if (samples)   // samples = 0, is used as mark
    {
        dim_log[logical_dim].data[xx].r[0] = samples[0];
        dim_log[logical_dim].data[xx].r[1] = samples[1];
        dim_log[logical_dim].data[xx].r[2] = samples[2];
        dim_log[logical_dim].data[xx].r[3] = samples[3];
    }
    else
    {
        dim_log[logical_dim].data[xx].r[0] = 0;           // Store a MIN value sample
        dim_log[logical_dim].data[xx].r[1] = 0;
        dim_log[logical_dim].data[xx].r[2] = 0;
        dim_log[logical_dim].data[xx].r[3] = 0;
    }

    dim_log[logical_dim].status.idx  = xx;
    dim_log[logical_dim].status.time = *sample_time;

    if (dim_log[logical_dim].status.dont_log)
    {
        dim_log[logical_dim].status.samples_to_acq--;         // Decrement samples to acquire
    }
}


void ScaleDimAnalogueData(void)
{
    /*
     * This function is called from QSPIbusStateMachine() on ms 19.
     * It converts the raw analogue diagnostic values to calibrated floating point values in
     * the DIAG.ANASIGS property and logs the raw signals when the logging for a DIM is active.
     *
     * A composite DIM is a virtual DIM that combines the same analogue register [analogue_0..analogue_3]
     * from up to four physical DIMs
     *
     * For composite DIMs the flat_qspi_board_number defines
     *     b7     : composite
     *     b6,b5  : analogue register to use [analogue_0..analogue_3]
     *     b4..b0 : first DIM in list to use (the follow consecutive DIMS in the list will be automatically used)
     *
     *
     *        7     Bus Address     0
     *       +--+--+--+--+--+--+--+--+
     *       |CF|a1|a0|d4|d3|d2|d1|d0|
     *       +--+--+--+--+--+--+--+--+
     *
     *       CF    = Composite flag (0x80)
     *       a1:a0 = Analogue input (0-3)
     *       d4:d0 = DIM channel for first DIM to combine (0-18)
     *
     */

    INT32U              logical_dim;
    INT32U              composite_list_idx;
    INT32U              flat_board_number;
    INT32U              ana_reg;                        // Analogue Channel index (0-3)
    INT32U              composite_ana_reg;
    INT32U              bit_in_packed;
    INT32U              latched_triggers_packed;
    BOOLEAN             freeze_all_dim_logs;
    BOOLEAN             relaunch_dim_logging;
    INT32U              analogue_value[FGC_N_DIM_ANA_CHANS];// up to 4 values to compose
    struct abs_time_us  sample_time;                    // Sample time (for logging) rounded to 20ms
    union TUnion32Bits  tmp;
    struct abs_time_ms  time_now_ms;                    // Time now in UXTIME and MS

    tmp.part.lo = diag.data.r.software[ISR_RUN_TIME][BRANCH_A_LATCHED_TRIGS];
    tmp.part.hi = diag.data.r.software[ISR_RUN_TIME][BRANCH_B_LATCHED_TRIGS];

    // Latched triggered for DIMs that are present and valid
    latched_triggers_packed = tmp.int32u & local.flat_qspi_board_is_valid_packed;

    freeze_all_dim_logs  = qspi_misc.freeze_all_dim_logs;
    relaunch_dim_logging = qspi_misc.relaunch_dim_logging;

    time_now_ms.unix_time = TimeGetUtcTime();
    time_now_ms.ms_time = TimeGetUtcTimeMs();
    AbsTimeCopyFromMsToUs(&time_now_ms, &sample_time);

    // ToDo check this (as it was imported form DSP)
    //  sample_time.us_time -= 19000;// This function is called on ms 19/20 so round down to start of 20ms

    // process the DIMs in the list
    for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        flat_board_number = dim_collection.flat_qspi_board_number[logical_dim];

        if (flat_board_number == 0xFF)     // If DIM is unused
        {
            continue;
        }

        if (flat_board_number & 0x80)              // if DIM is a composite
        {
            bit_in_packed = 0; // no bit in the packed bits to use, since composite DIM is not physical

            composite_list_idx = (flat_board_number & 0x1F);               // list number of first DIM to combine

            composite_ana_reg = (flat_board_number >> 5) & 0x03;   // analogue register to use

            // the 4 analogue register of this composite DIM are taken from the same
            // composite_ana_reg of 4 consecutive (in the list) DIM boards
            for (ana_reg = 0; ana_reg < FGC_N_DIM_ANA_CHANS; ana_reg++)
            {
                flat_board_number = dim_collection.flat_qspi_board_number[composite_list_idx];

                composite_list_idx++;

                if (flat_board_number < FGC_MAX_DIM_BUS_ADDR)
                {
                    analogue_value[ana_reg] = diag.data.a.analog[composite_ana_reg][flat_board_number] & 0x0FFF;

                    dim_collection.analogue_in_physical[ana_reg][logical_dim] =
                        dim_collection.offsets[ana_reg][logical_dim] +
                        dim_collection.gains[ana_reg][logical_dim] * (FP32)(analogue_value[ana_reg]);
                }
            }

            if (dim_log[logical_dim].status.samples_to_acq)           // If logging active
            {
                PutDIMsamplesInLog(logical_dim, &sample_time, &analogue_value[0]);                   // Log sample
            }

            if (!dim_log[logical_dim].status.dont_log)              // If logging is active
            {
                if (freeze_all_dim_logs ||
                    (latched_triggers_packed & (0x00000001 <<
                                                dim_collection.flat_qspi_board_number[FGC_COMP_DIM_TRIG_IDX]))   // If DIM VS latched
                   )
                {
                    dim_log[logical_dim].status.dont_log = TRUE; // Stop log for this composite DIM
                }
            }
            else                                                // else logging is not active
            {
                if (relaunch_dim_logging)
                {
                    dim_log[logical_dim].status.dont_log = FALSE;
                    PutDIMsamplesInLog(logical_dim, &sample_time, 0);  // Log NULL sample to mark the start of logging
                }
            }
        }
        else // not composite DIM
        {
            if (flat_board_number < FGC_MAX_DIM_BUS_ADDR)
            {
                // bit position corresponding to this board, when packed in INT32U
                bit_in_packed = 0x00000001 << flat_board_number;

                if (bit_in_packed & local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES])
                {
                    // log and process the 4 analogue registers of this dim
                    for (ana_reg = 0; ana_reg < FGC_N_DIM_ANA_CHANS; ana_reg++)
                    {
                        analogue_value[ana_reg] = diag.data.a.analog[ana_reg][flat_board_number] & 0x0FFF;

                        dim_collection.analogue_in_physical[ana_reg][logical_dim] =
                            dim_collection.offsets[ana_reg][logical_dim] +
                            dim_collection.gains[ana_reg][logical_dim] * (FP32)(analogue_value[ana_reg]);
                    }

                    if (dim_log[logical_dim].status.samples_to_acq)         // If logging active
                    {
                        PutDIMsamplesInLog(logical_dim, &sample_time, &analogue_value[0]);
                    }
                }

                if (!dim_log[logical_dim].status.dont_log)                  // If logging is active
                {
                    if (freeze_all_dim_logs
                        || (latched_triggers_packed & bit_in_packed)
                       )
                    {
                        dim_log[logical_dim].status.dont_log = TRUE;        // Stop logging for this DIM
                    }
                }
                else                                                        // else logging is stopped
                {
                    if (relaunch_dim_logging
                        && (local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] & bit_in_packed)
                       )
                    {
                        dim_log[logical_dim].status.dont_log = FALSE;       // Start logging for this DIM
                        PutDIMsamplesInLog(logical_dim, &sample_time, 0);   // Log NULL sample to mark
                    }                                                       // the start of logging
                }
            }
        }

        if (!dim_log[logical_dim].status.dont_log)                  // If logging is active
        {
            dim_log[logical_dim].status.samples_to_acq = FGC_LOG_DIM_LEN / 2; // Set 50% post trig samples
        }
    }

    /*
        if ( relaunch_dim_logging )
        {
            dsp_debug.stop_log = FALSE;
            dsp_debug.samples_to_acq = DIAG_DEBUG_TO_ACQ;
        }
    */
    qspi_misc.freeze_all_dim_logs  = FALSE;
    qspi_misc.relaunch_dim_logging = FALSE;
}


void ValidatePresentDimBoards(INT32U scan_step)
{
    /*
     * This function is called from QSPIbusStateMachine() on millisecond 19.
     * It must check to see how many DIMs are present on each bus and check for DIM sync faults.
     * If the number of DIMs detected is not the number expected, it sets DIAG_FLT in the ST_LATCHED status.
     *
     * The QSPI bus has 2 branches [A,B] with up to 16 DIM boards connected to each branch,
     * each DIM card has 7 registers (of 16 bits)
     *
     */

    INT8U       flat_board_number;
    INT32U      bit_in_packed;
    INT32U      present_boards_packed[QSPI_BRANCHES];
    INT8U       ana_regs_id[4];         // ID of DIM registers [analog_0..3 + trigCounter]
    INT8U       boards_in_qspi[QSPI_BRANCHES];

    /*
        the 7 registers are received in this order
            0 - Dig0        DIM register ID:0       NEXT ID -> 1
            1 - Dig1        DIM register ID:1                  4
            2 - Ana0        DIM register ID:4                  5
            3 - Ana1        DIM register ID:5                  6
            4 - Ana2        DIM register ID:6                  7
            5 - Ana3        DIM register ID:7                  2
            6 - TrigCounter DIM register ID:2                  0
                             there is no ID:3                  x

       sort by ID weight
            0 - Dig0        DIM register ID:0       NEXT ID -> 1
            1 - Dig1        DIM register ID:1                  4
            6 - TrigCounter DIM register ID:2                  0
                             there is no ID:3                  x use 8 as invalid, 3 can't be used as fails with the test pattern BABx
            2 - Ana0        DIM register ID:4                  5
            3 - Ana1        DIM register ID:5                  6
            4 - Ana2        DIM register ID:6                  7
            5 - Ana3        DIM register ID:7                  2
     */
    static INT8U NEXT_ID[] = { 1, 4, 0, 8, 5, 6, 7, 2 };


    // Move DIM present history
    local.flat_qspi_board_is_present_packed[DIMS_IN_PREVIOUS_CYCLE] =
        local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE];

    local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE] = 0x00000000;
    local.flat_qspi_board_is_valid_packed = 0x00000000;

    flat_board_number = 0;

    while (flat_board_number < FGC_MAX_DIM_BUS_ADDR)
    {
        // Extract register ID number from top nibble

        ana_regs_id[0] = (diag.data.f.analog_0[flat_board_number] >> 12) & 0x00000007;
        ana_regs_id[1] = (diag.data.f.analog_1[flat_board_number] >> 12) & 0x00000007;
        ana_regs_id[2] = (diag.data.f.analog_2[flat_board_number] >> 12) & 0x00000007;
        ana_regs_id[3] = (diag.data.f.analog_3[flat_board_number] >> 12) & 0x00000007;

        // If the analogue registers IDs are ok we assume that a DIM board is detected
        if ((ana_regs_id[1] == NEXT_ID[ana_regs_id[0]])
            && (ana_regs_id[2] == NEXT_ID[ana_regs_id[1]])
            && (ana_regs_id[3] == NEXT_ID[ana_regs_id[2]])
           )
        {
            // bit position corresponding to this board, when packed in INT32U
            bit_in_packed = 0x00000001 << flat_board_number;

            // Mark DIM as present on this cycle
            local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE] |= bit_in_packed;

            // If was present in previous cycle
            if (local.flat_qspi_board_is_present_packed[DIMS_IN_PREVIOUS_CYCLE] & bit_in_packed)
            {
                // if ID of trigCounter is also correctly 2
                if ((diag.data.f.trigCounter[flat_board_number] & 0x7000) == 0x2000)
                {
                    local.flat_qspi_board_is_valid_packed |= bit_in_packed;
                }
                else // channel is not correct
                {
                    local.reset_req_packed |= bit_in_packed; // Request a DIAG reset
                    qspi_misc.flat_qspi_non_valid_counts[flat_board_number]++;
                }
            }
        }

        flat_board_number += scan_step;
    }

    if (scan_step == 1)         // not class 59
    {
        // a DIM must be seen on two cycles to be present
        local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] =
            local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE]
            & local.flat_qspi_board_is_present_packed[DIMS_IN_PREVIOUS_CYCLE];

        present_boards_packed[BRANCH_B] = (local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] >> 16);
        present_boards_packed[BRANCH_A] = (local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES]) &
                                          0x0000FFFF;

        boards_in_qspi[BRANCH_A] = boards_in_qspi[BRANCH_B] = 0;

        while (present_boards_packed[BRANCH_B] | present_boards_packed[BRANCH_A])   // While bits still left to count
        {
            boards_in_qspi[BRANCH_A] += (present_boards_packed[BRANCH_A] & 0x00000001);
            boards_in_qspi[BRANCH_B] += (present_boards_packed[BRANCH_B] & 0x00000001);

            present_boards_packed[BRANCH_A] >>= 1; // next board
            present_boards_packed[BRANCH_B] >>= 1; // next board
        }

        qspi_misc.detected[BRANCH_A] = boards_in_qspi[BRANCH_A];
        qspi_misc.detected[BRANCH_B] = boards_in_qspi[BRANCH_B];

        // Check for number of DIMs detected not equal to number expected

        if (qspi_misc.expected_retrieved                                 &&
            (qspi_misc.detected[BRANCH_A] != qspi_misc.expected[BRANCH_A] ||
             qspi_misc.detected[BRANCH_B] != qspi_misc.expected[BRANCH_B]))
        {
            qspi_misc.expected_detected_mismatches++; // this one can overflow
        }

        dim_collection.dims_are_valid = (qspi_misc.detected[BRANCH_A] == qspi_misc.expected[BRANCH_A])
                                        && (qspi_misc.detected[BRANCH_B] == qspi_misc.expected[BRANCH_B])
                                        && (local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] ==
                                            local.flat_qspi_board_is_valid_packed);
    }

    /*
        if ( dsp_debug.samples_to_acq )
        {
            // First DIM on bus A

            // register analog_0 of DIM board, flat[ 1] = A[1]
            // register analog_1 of DIM board, flat[ 1] = A[1]
            SaveInDspDebugBuffer( (  diag.data.f.analog_0[1] << 16) | diag.data.f.analog_1[1]);

            // register analog_2 of DIM board, flat[ 1] = A[1]
            // register analog_3 of DIM board, flat[ 1] = A[1]
            SaveInDspDebugBuffer( (  diag.data.f.analog_2[1] << 16) | diag.data.f.analog_3[1]);

            // register trigCounter of DIM board, flat[ 1] = A[1]
            SaveInDspDebugBuffer( (  diag.data.f.trigCounter[1] << 16) | qspi_misc.mismatches_level);

            SaveInDspDebugBuffer(rtcom.time_now_ms.unix_time);
            SaveInDspDebugBuffer(rtcom.time_now_ms.ms_time | (boards_in_qspi[BRANCH_A] << 16));

            SaveInDspDebugBuffer(dim_collection.reset_req_packed);
            SaveInDspDebugBuffer(local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE]);
            SaveInDspDebugBuffer(dpcom.dsp.st_latched);
        }
    */
}

// EOF
