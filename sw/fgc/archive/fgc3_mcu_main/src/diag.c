/*---------------------------------------------------------------------------------------------------------*\
 File:      diag.c

 Purpose:   FGC MCU Software - QSPI related functions

 Author:    Quentin.King@cern.ch

 Notes:     The DIPS board has an embedded DIM that is the first on both branch A and branch B, so it is
        scanned at 100Hz.
        The analogue channels are different for the two scans (there are 8 channels in all)
        and are used to measure the voltage source output voltage (Vmeas) twice (x1 and x10)
        and the +5V and +/-15V PSU voltages for the FGC and the FGC's analogue interface.
        These are checked by the DSP against threshold values.
        If the qspi bus is reset, these channels get out of sync for 2 20ms cycles, so the DSP must suppress
        PSU monitoring for 2 cycles.
        This is signalled by the MCU using dpcom.mcu.diag.reset.
        This should be set to the number of cycles that need to be blocked (2).
        It is decremented by the DSP every 20ms cycle.
\*---------------------------------------------------------------------------------------------------------*/

#define DIAG_GLOBALS            // define diag global variable
#define DEFPROPS_INC_ALL        // defprops.h

#include <diag.h>

#include <class.h>
#include <cmd.h>                // for tcm global variable
#include <definfo.h>            // for FGC_CLASS_ID
#include <defprops.h>           // for PROP_LOG_MENU_NAMES, PROP_LOG_MENU_PROPS
#include <dev.h>                // for dev global variable
#include <DIMsDataProcess.h>
#include <dpcom.h>              // for dpcom global variable
#include <fbs_class.h>          // for ST_DCCT_A, ST_DCCT_B
#include <fbs.h>                // for fbs global variable
#include <fgc/fgc_db.h>         // for access to SysDB
#include <log.h>
#include <macros.h>             // for Test(), Set()
#include <mcu_dependent.h>      // for SMALL_USLEEP()
#include <mem.h>
#include <memmap_mcu.h>         // for SM_UXTIME_P
#include <mst.h>                // for mst global variable
#include <prop.h>               // for PropSetNumEls()
#include <shared_memory.h>      // for shared_mem global variable
#include <string.h>
#include <trm.h>

// Macro arguments are expanded except if with # or ##, so we need 2 level of indirection
#define CLASS_FIELD_STRING(name)        c##name
#define CLASS_FIELD_STRING2(name)       CLASS_FIELD_STRING(name)
#define CLASS_FIELD                     CLASS_FIELD_STRING2(FGC_CLASS_ID)   // c##FGC_CLASS_ID
#define LOG_DIM_NUM_LOGS                16

static    log_dim_name_t    buf[LOG_DIM_NUM_LOGS];

/*---------------------------------------------------------------------------------------------------------*/
void DiagDimHashInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function add the DIM to the properties HASH structure for posterios usage
\*---------------------------------------------------------------------------------------------------------*/
{
    int n_dims = 0;
    char sym[ST_MAX_SYM_LEN + 1];

    for (int ii =  0; ii < FGC_MAX_DIMS; ii++)
    {
        if (SysDbDimEnabled(dev.sys.sys_idx,ii))     // If this DIM index is occupied (0xFF=unused)
        {
            strncpy(sym, SysDbDimName(dev.sys.sys_idx,ii), ST_MAX_SYM_LEN); // Transfer symbol to near memory
            if (HashInitDIM(sym, n_dims)) n_dims++;
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void DiagDimDbInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare the DIM database for use with the DIAG properties
\*---------------------------------------------------------------------------------------------------------*/
{
    fgc_db_t                      dim_idx;    
    fgc_db_t                      ii;                             // Loop index
    fgc_db_t                      n_log_menus;                    // Number of log menus
    fgc_db_t                      board_number;
    INT32U                      dims_expected[QSPI_BRANCHES];   // Number of real dims expected per branch
    INT32U           *          val_scm_ptr;
    char                        sym[ST_MAX_SYM_LEN + 1];
    struct prop        *        dim_props;
    struct TAccessToDimInfo  *  logical_dim_info_ptr;

    // If databases are not the same version or system is unknown then return immediately

    if (!dev.dbs_ok_f || !dev.sys.sys_idx)
    {
        DiagSetPropNumEls(0);               // Reset the dynamic DIAG property lengths
        return;
    }

    // Prepare pointers to codes in flash memory

    // note : warning while debugging, if SYSDB is not in the ROM this will crash horrendously
    // overwriting the stack in the following for() statement
   
    
    n_log_menus = SysDbLogMenuLength(dev.sys.sys_idx);
    val_scm_ptr = &tcm.prop_buf->blk.intu[0];

    // Set up LOG menu names/properties lists for LOG.MENU properties

    for (ii = 0; ii < n_log_menus; ii++)
    {
        diag.menu_names[ii] = SysDbLogMenuName(dev.sys.sys_idx,ii);
        diag.menu_props[ii] = SysDbLogMenuProperty(dev.sys.sys_idx,ii);
    }
    // Initialize DIM logs
    
    for (dim_idx = 0; dim_idx < LOG_DIM_NUM_LOGS; ++dim_idx)
    {
        // Check if the logical DIM is used
            
        fgc_db_t      dim_type_idx = SysDbDimDbType(dev.sys.sys_idx, dim_idx);
        uint8_t       dim_ana_active  = 0;
        fgc_db_t      ana_idx;

        sprintf(buf[dim_idx], "LOG.DIM[%d]", dim_idx);

        // Make sure there is at least one analogue signal

        for (ana_idx = 0; ana_idx < FGC_N_DIM_ANA_CHANS; ++ana_idx)
        {
            if (dim_type_idx && DimDbIsAnalogChannel(dim_type_idx, ana_idx))
            {
                dim_ana_active = 1;
            }
        }

        // Set the DIM name and prop if the DIM exists and has analogue signals
        if (dim_ana_active != 0)
        {
            diag.menu_names[n_log_menus] = SysDbDimName(dev.sys.sys_idx, dim_idx);
            diag.menu_props[n_log_menus] = (char *)&buf[dim_idx];
            n_log_menus++;
        }

    }

    PropSetNumEls(NULL, &PROP_LOG_MENU_NAMES, n_log_menus); // Set property length
    PropSetNumEls(NULL, &PROP_LOG_MENU_PROPS, n_log_menus); // Set property length

    // Set up DIM property structures

    dims_expected[BRANCH_A] = dims_expected[BRANCH_B] = 0;          // Clear DIM expected counters
    dim_props = &diag.dim_props[0];
    logical_dim_info_ptr = &diag.logical_dim_info[0];


    PropBufWait(&tcm);                      // Wait for buffer to be free


    for (ii = diag.n_dims = 0; ii < FGC_MAX_DIMS; ii++)
    {
        logical_dim_info_ptr->logical_dim = SysDbDimLogicalAddress(dev.sys.sys_idx,ii);

        if (SysDbDimEnabled(dev.sys.sys_idx,ii))     // If this DIM index is occupied (0xFF=unused)
        {
            diag.n_dims++;

            *(val_scm_ptr) = board_number = logical_dim_info_ptr->flat_qspi_board_number = SysDbDimBusAddress(dev.sys.sys_idx,ii);

            val_scm_ptr++;

            strcpy(sym, SysDbDimName(dev.sys.sys_idx,ii)); // Transfer symbol to near memory

            dim_props->sym_idx      = HashFindProp(sym);        // Look up symbol in property hash
            dim_props->type         = PT_STRING;
            dim_props->get_func_idx = PROP_DIAG_DIM_NAMES.get_func_idx;
            dim_props->n_elements   = dim_props->max_elements = FGC_N_DIM_DIG_BANKS * FGC_N_DIM_DIG_INPUTS;
            dim_props->value        = logical_dim_info_ptr;

            if (board_number < FGC_MAX_DIM_BUS_ADDR)
            {
                // 0..15 belongs to branch A=0, 16..31 belongs to branch B=1
                dims_expected[board_number / (FGC_MAX_DIM_BUS_ADDR / 2)]++; // Increment DIMs expected bus branch
            }

            dim_props++;
        }
        else                    // else DIM index is unused
        {
            *(val_scm_ptr) = FGC_DIAG_UNUSED_CHAN;

            val_scm_ptr++;
        }

        logical_dim_info_ptr++;
    }

    tcm.cached_prop  = &PROP_DIAG_BUS_ADDRESSES;        // Write SCM buffer to DIAG.BUS_ADDRESSES
    tcm.prop_blk_idx = 0;                   // Write buffer to first property block
    tcm.n_elements   = 0;

    PropBlkSet(&tcm, FGC_MAX_DIMS, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT);// Write SCM buffer
    PropSet(&tcm, &PROP_DIAG_DIMS_EXPECTED, &dims_expected, 2); // Write DIMS_EXPECTED to DSP property

    DiagSetPropNumEls(diag.n_dims);             // Set the property lengths

    DiagSetOffsetGain(&PROP_DIAG_GAINS,  STP_GAINS);
    DiagSetOffsetGain(&PROP_DIAG_OFFSETS, STP_OFFSETS);

    DiagInitClass();                        // Class specific diag initialisations

    qspi_misc.expected_retrieved = TRUE;
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagSetPropNumEls(INT16U n_dims)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set the length of the parent properties which use the dynamic list of children that
  corresponds to the list of DIMS for the device type.
\*---------------------------------------------------------------------------------------------------------*/
{
    PropSetNumEls(NULL, &PROP_DIAG_ANA,        n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_ANA_LBLS,   n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_DIG,        n_dims);   
    PropSetNumEls(NULL, &PROP_DIAG_DIG_LBLS,   n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_DIM_NAMES,  n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_FAULTS,     n_dims);   
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagSetOffsetGain(struct prop * p, INT16U prop_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare the DIAG.GAIN and DIAG.OFFSET properties in the DSP (FGC2) or MCU (FGC3) memory.
  We prepare the property from dimdb into a buffer before calling PropSet()
  We use the terminal_state struct as a temporary buffer, this function being called before the terminal
  can be started.
\*---------------------------------------------------------------------------------------------------------*/
{
    typedef FP32 diag_offset_gain_array[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];
    fgc_db_t                      logical_dim;
    fgc_db_t                      chan;
    diag_offset_gain_array * buf;

    tcm.cached_prop = p;                // Link SCM structure to DSP property

#ifdef __GNUC__
    // C11 standard, supported by GCC > 4.6
    _Static_assert(sizeof(FP32) * FGC_N_DIM_ANA_CHANS * FGC_MAX_DIMS < sizeof(terminal_state),
                   "Buffer too small");
#endif
    buf = (diag_offset_gain_array *) &terminal_state;
    PropBufWait(&tcm);                 // Wait for buffer to be free

    for (chan = 0; chan < FGC_N_DIM_ANA_CHANS; chan++)
    {
        for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
        {
            fgc_db_t dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,logical_dim);
            if (SysDbDimEnabled(dev.sys.sys_idx,logical_dim)) // If DIM channel is in use        
            {  
                if (prop_idx == STP_GAINS)                      // If DIAG.GAIN property
                {
                    (*buf)[chan][logical_dim] = DimDbAnalogGain(dim_type_idx,chan);
                }
                else if (prop_idx == STP_OFFSETS)               // else DIAG.OFFSET property
                {
                    (*buf)[chan][logical_dim] = DimDbAnalogOffset(dim_type_idx,chan);
                }
            }
        }
    }

    PropSetFar(&tcm, p, (void *)*buf, p->max_elements);
    MemSetWords(*buf, 0x0000, sizeof(FP32) * FGC_N_DIM_ANA_CHANS * FGC_MAX_DIMS);     // Clear buffer

}
/*---------------------------------------------------------------------------------------------------------*/
INT32S DiagFindAnaChan(const char * chan_name)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search for an analogue channel with the name given in chan_name.
  It will stop at the first match and will return the channel index based on the ANASIGS array.
  If no match is found it will return -1.
\*---------------------------------------------------------------------------------------------------------*/

{
    fgc_db_t                      logical_dim;
    fgc_db_t                      ana_reg;                                // 0-3
    char                        chan_name_near[ST_MAX_SYM_LEN + 1];


    strcpy(chan_name_near, chan_name);                            // Get near copy of channel name

    for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        fgc_db_t dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,logical_dim);
        if (SysDbDimEnabled(dev.sys.sys_idx,logical_dim)) // If DIM channel is in use
        {
            for (ana_reg = 0; ana_reg < FGC_N_DIM_ANA_CHANS; ana_reg++) // For each analog channel
            {
                if (DimDbIsAnalogChannel(dim_type_idx,ana_reg)
                    && !strcmp(DimDbAnalogLabel(dim_type_idx,ana_reg), chan_name_near)) // name matches
                {
                    return (ana_reg * FGC_MAX_DIMS + logical_dim);      // Return ANASIGS index
                }
            }
        }
    }

    return (-1);                // No match
}
/*---------------------------------------------------------------------------------------------------------*/
BOOLEAN DiagFindDigChan(INT16U logical_dim, const char * chan_name,
//BOOLEAN DiagFindDigChan(const struct TAccessToDimInfo * logical_dim_info_ptr, const char * chan_name,
                        INT16U * data_idx, INT16U * diag_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search for a digital channel with the name given in chan_name in the DIM specified
  by logical_dim_info_ptr.  It will stop at the first match and will return TRUE and the channel index in the diag.data[]
  array and the bit mask for the channel.  If no match is found it will return FALSE.
\*---------------------------------------------------------------------------------------------------------*/
{
    fgc_db_t                      mask;
    fgc_db_t                      bank_idx;       // 0 - 1
    fgc_db_t                      ana_reg;        // 0 - 3
    const fgc_db_t dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,logical_dim);

    for (bank_idx = 0; bank_idx < FGC_N_DIM_DIG_BANKS; bank_idx++)
    {
        mask = 1;
        
        for (ana_reg = 0; ana_reg < FGC_N_DIM_DIG_INPUTS; ana_reg++)
        {
            if (DimDbIsDigitalChannel(dim_type_idx,(bank_idx+4)) &&
                !strcmp(DimDbDigitalLabel(dim_type_idx,(bank_idx+4),ana_reg), chan_name))
            {
                *diag_mask = mask;

                // DIAG_MAX_ANALOG is 0x80
                // 16 DIM boards is 0x20
                // I guess this is to point to
                //  diag.data.r.digital_0[flat_qspi_board_number]
                // or
                //  diag.data.r.digital_1[flat_qspi_board_number]
                *data_idx = SysDbDimBusAddress(dev.sys.sys_idx,logical_dim) + 0x80 + (0x20 * bank_idx);

                return (TRUE);
            }
            mask <<= 1;
        }
    }

    return (FALSE);
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagSciStart(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare each 200ms period of diagnostic output via the SCI RS232 port.  This function
  is called from DiagTermNext() on ms 1 of every 200ms period.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct diag_term  *  sci_diag;



    if (diag.term_byte_idx > DIAG_TERM_LEN)   // If starting
    {
        diag.term_byte_idx = DIAG_TERM_LEN;       // Block transmission for first cycle
    }
    else
    {
        if (diag.term_byte_idx == DIAG_TERM_LEN)      // else running
        {
            sci_diag                 = &diag.term_buf[diag.term_buf_idx];   // Get diag buffer address
            sci_diag->data.time_sec  = TimeGetUtcTime();
            sci_diag->data.time_usec = TimeGetUtcTimeUs();                  // us time (0,200000,400000,...)

            memcpy(&sci_diag->data.class_data.reserved[0], &fbs.u.fgc_stat.class_data.CLASS_FIELD,
                   sizeof(sci_diag->data.class_data.reserved));

            diag.term_buf_p    = (INT8U *)sci_diag;
            diag.term_byte_idx = 0;
        }
    }

    diag.term_buf_idx     = 1 - diag.term_buf_idx;        // Prepare to acquire new data in
    sci_diag             = &diag.term_buf[diag.term_buf_idx];
    diag.term_buf_chan    = (INT8U *)  & (sci_diag->data.chan);
    diag.term_buf_data    = (INT16U *) & (sci_diag->data.data);
    diag.term_buf_counter = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagTermNext(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will transfer the next set of diagnostic data values.  This is called on ms 1 of every
  20 ms while the diag mode is active.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  ii;

    if (diag.term_buf_counter++ < FGC_DIAG_PERIOD)       // Protect against overrun
    {
        for (ii = 0; ii < FGC_DIAG_N_LISTS; ii++)
        {
            *(diag.term_buf_chan++) = fbs.u.fieldbus_stat.diag_chan[ii];
            *(diag.term_buf_data++) = fbs.u.fieldbus_stat.diag_data[ii];
        }
    }

    if (mst.ms_mod_200 == 1)                    // Every 200ms, start diag period
    {
        DiagSciStart();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagInitClass(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises aspects of the diagnostic system that are related to the Class 61 version.
  Some power converters are made from subconverters which can trip off without stopping the whole converter.
  The state of the subconverters is available via the diagnostic system and this function will find the
  relevant channels and save them so that they can be checked during operation.
\*---------------------------------------------------------------------------------------------------------*/
{
    static char vrw_name[] = "VS_REDUNDANCY_WARNING";
    static char fwd_name[] = "FREE WHEEL DIODE";
    static char fau_name[] = "FAST ABORT UNSAFE";

    // Find analogue channels

    // The I earth analogue channels come in two flavours:
    // One single channel (COABLAT and ACAPULCO): I_EARTH
    // Four channels (RegFGC3): I_EARTH_1MS, I_EARTH_1MS, I_EARTH_11MS and I_EARTH_16MS

    meas_i_earth.channel[0] = DiagFindAnaChan("I_EARTH");

    if (meas_i_earth.channel[0] != -1)
    {
        // Use the same value in log.c LogIearth(). Logging is done at 200 Hz
        // whilst sampling in this case is 50 Hz (20 ms) since there is only
        // one channel. This means that the same value will be logged four
        // time.

        meas_i_earth.channel[1] = meas_i_earth.channel[0];
        meas_i_earth.channel[2] = meas_i_earth.channel[1];
        meas_i_earth.channel[3] = meas_i_earth.channel[2];
    }
    else
    {
        // Assume the four channels below exist. If not, channel[] will contain
        // -1, which will default in a I earth value of 0.0.

        meas_i_earth.channel[0] = DiagFindAnaChan("I_EARTH_1MS");
        meas_i_earth.channel[1] = DiagFindAnaChan("I_EARTH_6MS");
        meas_i_earth.channel[2] = DiagFindAnaChan("I_EARTH_11MS");
        meas_i_earth.channel[3] = DiagFindAnaChan("I_EARTH_16MS");
    }

    dpcls.mcu.vs.u_lead_pos_chan = DiagFindAnaChan("U_LEAD_POS");
    dpcls.mcu.vs.u_lead_neg_chan = DiagFindAnaChan("U_LEAD_NEG");

    uint16_t  logical_dim;

    // Find digital channels for sub-converters, free-wheel diode and fast abort unsafe
    
    vs.fabort_unsafe  = FGC_VDI_NOT_PRESENT;
    vs.fw_diode       = FGC_VDI_NOT_PRESENT;

    for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        if (DiagFindDigChan(logical_dim, fau_name, &vs.fabort_unsafe_dig.channel, &vs.fabort_unsafe_dig.mask))
        {
            vs.fabort_unsafe = FGC_VDI_NO_FAULT;
        }

        if (DiagFindDigChan(logical_dim, fwd_name, &vs.fw_diode_dig.channel, &vs.fw_diode_dig.mask) == true)
        {
            vs.fw_diode = FGC_VDI_NO_FAULT;
        }

        DiagFindDigChan(logical_dim, vrw_name, &vs.vs_redundancy_warning.channel, &vs.vs_redundancy_warning.mask);        
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagCheckConverter(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from StaTsk() every 5ms.  Provided the converter is not being simulated then
  every 4th iteration it will check the number of active subconverters based on the diag data channels
  and masks identified by DiagInitClass().  It provides the number to the DSP so that the actual maximum
  current can be calculated.

  It also checks to see if the VS DIM is reporting a FREE WHEEL DIODE fault and if so it sets the
  VS.FW_DIODE property and asserts a FW_DIODE fault.  Since this can trip the converter and the signal
  comes from the DIM, a filter is included to avoid spurious DIAG data from stopping the converter. This
  requires a VS_FAULT to be present and the FW_DIODE fault bit in the DIM data to be active for 200ms.

  It also checks to see if a DIM is reporting an unlatched trigger while the converter is not reporting
  a fault.  If this condition persists, the DIAG_TRIG_FLT flag will be set in the latched status, which
  will generate an FGC_HW warning.
\*---------------------------------------------------------------------------------------------------------*/
{
    static INT16U fw_diode_counter;                     // Filter counter for FW_DIODE fault
    static INT16U fabort_unsafe_counter;                // Filter counter for FABORT_UNSAFE fault
    static INT16U dim_trig_counter;                     // Filter counter for DIM_TRIG_FLT latched status

    if ((sta.mode_op != FGC_OP_SIMULATION)              // If not simulating the converter and
        && (((INT16U) sta.timestamp_ms.ms_time % 20) == (15 + DEV_STA_TSK_PHASE))      // time is end of 20ms cycle
       )
    {
#if (FGC_CLASS_ID == 61)

        // Check for voltage source redundancy warning

        if (   vs.vs_redundancy_warning.mask != 0
            && dim_collection.dims_are_valid 
            && sta.mode_op != FGC_OP_SIMULATION)
        {
            if (Test(diag.data.w[vs.vs_redundancy_warning.channel], vs.vs_redundancy_warning.mask))
            {
                WARNINGS |= FGC_WRN_SUBCONVTR_FLT;
            }
            else
            {
                WARNINGS &= ~FGC_WRN_SUBCONVTR_FLT;
            }
        }
#endif

        // Check for a Free Wheel Diode fault - with 200ms filter

        // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)

        if (vs.fw_diode_dig.mask
            && (STATE_OP == FGC_OP_NORMAL)
            && !Test(sta.cmd_req, DIG_OP_SET_VSRESETCMD_MASK16)
            && Test(FAULTS, FGC_FLT_VS_FAULT)
            && Test(diag.data.w[vs.fw_diode_dig.channel], vs.fw_diode_dig.mask))
        {
            fw_diode_counter++;

            if (fw_diode_counter > 10)                            // If fault present for > 200ms
            {
                vs.fw_diode = FGC_VDI_FAULT;
            }
        }
        else
        {
            fw_diode_counter = 0;
        }

        // Check for a Fast Abort fault - with 100ms filter

        // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)

        if (vs.fabort_unsafe_dig.mask
            && (STATE_OP == FGC_OP_NORMAL)
            && !Test(sta.cmd_req, DIG_OP_SET_VSRESETCMD_MASK16)
            && Test(FAULTS, FGC_FLT_VS_FAULT)
            && Test(diag.data.w[vs.fabort_unsafe_dig.channel], vs.fabort_unsafe_dig.mask))
        {
            fabort_unsafe_counter++;

            if (fabort_unsafe_counter > 5)                     // If fault present for > 100ms
            {
                vs.fabort_unsafe = FGC_VDI_FAULT;
            }
        }
        else
        {
            fabort_unsafe_counter = 0;
        }

        if (dim_collection.unlatched_trigger_fired      // If a DIM has an unlatched trigger and
            && Test(sta.inputs, DIG_IP1_VSRUN_MASK16)   // VSRUN is active and
            && !Test(FAULTS, FGC_FLT_VS_FAULT)          // no VS_FAULT is present and
            && !Test(FAULTS, FGC_FLT_VS_EXTINTLOCK)     // no VS_EXTINTLOCK is present and
            && !Test(FAULTS, FGC_FLT_FAST_ABORT)        // no FAST_ABORT is present and
            && !Test(ST_LATCHED, FGC_LAT_DIM_TRIG_FLT)) // the DIM_TRIG_FLT is not yet set
        {
            dim_trig_counter++;

            if (dim_trig_counter > 5)                   // If fault present for > 100ms
            {
                ST_LATCHED |= FGC_LAT_DIM_TRIG_FLT;     // Set DIM_TRIG_FLT in latched status
            }
        }
        else
        {
            dim_trig_counter = 0;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: diag.c
\*---------------------------------------------------------------------------------------------------------*/
