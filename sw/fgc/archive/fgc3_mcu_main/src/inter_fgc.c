/*!
 *  @file      inter_fgc.c
 *  @defgroup  FGC3:MCU
 *  @brief     Inter FGC communication.
 */

#define INTER_FGC_GLOBALS

// Includes

#include <inter_fgc.h>
#include <string.h>

#include <ether_comm.h>
#include <fbs_class.h>
#include <lan92xx.h>
#include <macros.h>
#include <mst.h>
#include <pc_state.h>
#include <runlog_lib.h>
#include <sta.h>
#include <time_fgc.h>

// Constants

#define     DESTINATION_PROPERTY_IS_BROADCAST       ( inter_fgc.communications.dest_addr > FGC_ETHER_MAX_FGCS )

#define     MISSING_COMMUNICATION                   ( local_inter_fgc.reception_check.missing.low || local_inter_fgc.reception_check.missing.high )

#define     PREVIOUSLY_MISSING_COMMUNICATION_LOW    ( local_inter_fgc.reception_check.previously_missing.low  & local_inter_fgc.reception_check.missing.low   )
#define     PREVIOUSLY_MISSING_COMMUNICATION_HIGH   ( local_inter_fgc.reception_check.previously_missing.high & local_inter_fgc.reception_check.missing.high  )
#define     PREVIOUSLY_MISSING_COMMUNICATION        ( PREVIOUSLY_MISSING_COMMUNICATION_LOW || PREVIOUSLY_MISSING_COMMUNICATION_HIGH                                             )

#define     US_TIME_INTER_FGC_ISR                   400

#define     FGC_NUMBER_OF_ROLES                     4//(FGC_ROLE_REMOTE_MEAS + 1)

// Internal structures, unions and enumerations
enum inter_fgc_sources_expected
{
    INTER_FGC_UNIQUE_EXPECTED_SOURCE,
    INTER_FGC_MULTIPLE_EXPECTED_SOURCES,
    INTER_FGC_SOURCE_NUMBER_NUMBER
};



typedef struct long_mask
{
    uint32_t        low;
    uint32_t        high;
} long_mask_t;



typedef struct reception_check
{
    long_mask_t     expected;
    long_mask_t     tracked;
    long_mask_t     missing;
    long_mask_t     previously_missing;
} reception_check_t;



typedef struct inter_fgc_packets
{
    inter_fgc_packet_t  tx;
    inter_fgc_packet_t  rx;
} inter_fgc_packets_t;



typedef struct local_inter_fgc_master
{
    uint8_t id_to_index[FGC_ETHER_MAX_FGCS + 1];
} local_inter_fgc_master_t;



typedef struct local_inter_fgc_slave
{
    uint8_t missing;
    uint8_t previous_missing;
} local_inter_fgc_slave_t;



typedef struct local_inter_fgc
{
    local_inter_fgc_master_t master;
    reception_check_t        reception_check;
    inter_fgc_packets_t      packets;
    uint16_t                 expected_source_plurality;
    local_inter_fgc_slave_t  slave;
} local_inter_fgc_t;



// Internal function declarations
/*------------------------------------------------- Init --------------------------------------------------*/
static void InterFgcInitVariables(void);
static void InterFgcInitHeader(fgc_ether_header_t * const header, const uint8_t dest_addr, const uint8_t role);

static void InterFgcInitProfiles(void);
static void InterFgcSetHeader(fgc_ether_header_t * const header, const uint8_t dest_addr, const uint8_t role);

/*-------------------------------------------------- TX ---------------------------------------------------*/
static void InterFgcPreparePacket(fgc_ether_metadata_t * const metadata, inter_fgc_payload_t * const payload, const uint8_t role);
static void InterFgcSendPacket(inter_fgc_packet_t * const packet);

static void InterFgcPreparePayload(inter_fgc_payload_t * const payload, const uint8_t role);

/*-------------------------------------------------- RX ---------------------------------------------------*/
static void InterFgcReadPacket(inter_fgc_packet_t * const packet);
static void InterFgcProcessPacket(inter_fgc_packet_t * const packet, const uint8_t source_role, const enum inter_fgc_sources_expected source_plurality);

static void InterFgcProcessPayload(const inter_fgc_payload_t * const payload, const enum fgc_ether_payload_type payload_type);
static void InterFgcProcessMetadata(const enum inter_fgc_sources_expected source_plurality);

static uint8_t InterFgcPacketExpected(void);

/*------------------------------------------------ Refresh ------------------------------------------------*/
static void InterFgcRefreshVariables(const uint8_t role);
static void InterFgcRefreshExpectedSourcePlurality(void);

static void InterFgcRefreshDualPortVariables(void);

static INLINE void InterFgcRefreshIdToIndexArray(void);
static INLINE void InterFgcInitIdToIndexArray(void);
static INLINE void InterFgcSetIdToIndexArray(void);

//static void InterFgcSetProfileRowOfId(void);

/*----------------------------------------- Reception Check Process----------------------------------------*/
static INLINE void InterFgcAddIdToMask(long_mask_t * const long_mask, const uint16_t id);

//static void InterFgcCheckReception(const enum inter_fgc_sources_expected source_plurality);

//static void InterFgcRecordMissingCommunication(void);
//static void InterFgcSetExpectedCommunicationMask(void);
//static INLINE void InterFgcCalculateMissingCommunicationMask(void);
//static INLINE void InterFgcMaskReset(long_mask_t * const mask);
static void InterFgcCountSlaves(void);

// Internal variable definitions

local_inter_fgc_t priv;

// Internal function definitions



static uint8_t InterFgcRoleToPayloadType(const uint8_t role)
{
    static const uint8_t role_to_payload_type[FGC_NUMBER_OF_ROLES] =
    {
            0,                                   // FGC_ROLE_INDEPENDANT
            FGC_ETHER_PAYLOAD_INTER_FGC,     // FGC_ROLE_MASTER
            FGC_ETHER_PAYLOAD_INTER_FGC,     // FGC_ROLE_SLAVE
            FGC_ETHER_PAYLOAD_INTER_FGC,    // FGC_ROLE_REMOTE_MEAS
    };

    return((role >= FGC_NUMBER_OF_ROLES) ? 0 : role_to_payload_type[role]);
}



static uint8_t InterFgcPayloadTypeToRole(const uint8_t payload_type)
{
    static const uint8_t payload_type_to_role[FGC_ETHER_PAYLOAD_NUM_TYPES] =
    {
            0,                       // FGC_ETHER_PAYLOAD_TIME
            0,                       // FGC_ETHER_PAYLOAD_STATUS
            0,                       // FGC_ETHER_PAYLOAD_CMD
            0,                       // FGC_ETHER_PAYLOAD_RSP
            0,                       // FGC_ETHER_PAYLOAD_PUB
            //FGC_ROLE_MASTER,         // FGC_ETHER_PAYLOAD_INTER_FGC_CMD
            //FGC_ROLE_SLAVE,          // FGC_ETHER_PAYLOAD_INTER_FGC_RSP
            //FGC_ROLE_REMOTE_MEAS,    // FGC_ETHER_PAYLOAD_INTER_FGC_MEAS
    };

    return((payload_type >= FGC_ETHER_PAYLOAD_NUM_TYPES) ? 0 : payload_type_to_role[payload_type]);
}



static void InterFgcInitVariables(void)
{
    TICK_MCU_COMM_DELAY_US_P = US_TIME_INTER_FGC_ISR;
    inter_fgc.communications.dest_addr = 0x00;

    InterFgcInitProfiles();
    InterFgcRefreshExpectedSourcePlurality();
}



static void InterFgcInitProfiles(void)
{
    const uint16_t min   = US_TIME_INTER_FGC_ISR;
    const uint16_t width = ((1000 - min) / (FGC_HISTOGRAM_BINS * 5) + 1) * 5;

    HistogramInit(&communication_histograms.rx.fgc,        min,  width);
    HistogramInit(&communication_histograms.tx.fgc,        min,  width);
    HistogramInit(&communication_histograms.rx.fgc_id,     min,  width);
    HistogramInit(&communication_histograms.rx.latency,    0,    width);
    HistogramInit(&communication_histograms.rx.latency_id, 0,    width);
}



static void InterFgcInitHeader(fgc_ether_header_t * const header, const uint8_t dest_addr, const uint8_t role)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called every 200ms, to check if the user has changed role properties.
  The function will set the header accordingly to the role properties of the FGC.
\*---------------------------------------------------------------------------------------------------------*/
{
    *header = ethernet.tx_header;

    InterFgcSetHeader(header, dest_addr, role);
}



static void InterFgcSetHeader(      fgc_ether_header_t * const header,
                              const uint8_t                    dest_addr,
                              const uint8_t                    role)
{
    // Set destination

    if(DESTINATION_PROPERTY_IS_BROADCAST)
    {
        memset(&header->ethernet.dest_addr, 0xFF, FGC_ETHER_ADDR_SIZE);
    }
    else
    {
        header->ethernet.dest_addr[0] = 0x02;
        memset(&header->ethernet.dest_addr[1], 0x00, FGC_ETHER_ADDR_SIZE - 2);
        header->ethernet.dest_addr[5] = dest_addr;
    }

    // Set payload type

    header->fgc.payload_type = InterFgcRoleToPayloadType(role);
}



static void InterFgcPreparePacket(      fgc_ether_metadata_t * const metadata,
                                        inter_fgc_payload_t  * const payload,
                                  const uint8_t                      role)
{
    // Prepare metadata

    //metadata->class_id    = fbs.u.fieldbus_stat.class_id;
    //metadata->data_status = fbs.u.fgc_stat.data_status;

    // Prepare payload

    InterFgcPreparePayload(payload, role);
}



static void InterFgcSendPacket(inter_fgc_packet_t * const packet)
{
    const uint32_t packet_tag  = packet->header.fgc.payload_type << 16;
    const uint16_t packet_size = sizeof(*packet);
    const uint32_t tx_time_us  = UTC_US_P;

    //packet->metadata.us_time_stamp = tx_time_us;

    uint8_t err = Lan92xxSendPacketOpt(packet, packet_size, packet_size, TX_CMDA_FIRST_SEG | TX_CMDA_LAST_SEG, packet_tag);

    if(err != 0)
    {
        ethernet.stats.frame_count.sent.fail++;
    }

    HistogramAdd(&communication_histograms.tx.fgc, HistogramGetRowIndexFromTime(tx_time_us), TimeGetAbsUs());
}



static void InterFgcPreparePayload(inter_fgc_payload_t * const payload, const uint8_t role)
{
    switch(role)
    {
//        case FGC_ROLE_MASTER:
//
//            if(FAULTS)
//            {
//                payload->master.mode_pc = FGC_PC_OFF;
//            }
//            else
//            {
//                payload->master.mode_pc = sta.mode_pc;
//            }
//
//            break;
//
//        case FGC_ROLE_SLAVE:
//
//            payload->slave.faults = FAULTS;
//
//            break;
//
//        case FGC_ROLE_REMOTE_MEAS:
//
//            payload->meas = dpcls.mcu.inter_fgc.fgc_ether_meas;
//
//            break;
//
//        case FGC_ROLE_INDEPENDANT:
        default:

            break;
    }
}



static void InterFgcReadPacket(inter_fgc_packet_t * const packet)
{
    Lan92xxReadFifo(&packet->metadata, sizeof(packet->metadata), &ethernet.current_rx_fifo_byte_length);
    Lan92xxReadFifo(&packet->payload,  sizeof(packet->payload),  &ethernet.current_rx_fifo_byte_length);
}



static void InterFgcProcessPacket(      inter_fgc_packet_t * const      packet,
                                  const uint8_t                         source_role,
                                  const enum inter_fgc_sources_expected source_plurality)
{
    // Process payload

    InterFgcProcessPayload(&packet->payload, InterFgcRoleToPayloadType(source_role));

    // Process metadata

    const uint32_t rx_time_us = 0;//packet->metadata.us_time_stamp;

    HistogramAdd(&communication_histograms.rx.latency, HistogramGetRowIndexFromTime(rx_time_us), TimeGetUsDiffNow(rx_time_us));
    HistogramAdd(&communication_histograms.rx.fgc,     HistogramGetRowIndexFromTime(rx_time_us), TimeGetAbsUs());

    InterFgcProcessMetadata(source_plurality);
}



static void InterFgcProcessMetadata(const enum inter_fgc_sources_expected source_plurality)
{
    switch(source_plurality)
    {
        case INTER_FGC_UNIQUE_EXPECTED_SOURCE:

            priv.slave.missing = 0;

            break;

        case INTER_FGC_MULTIPLE_EXPECTED_SOURCES:
        {
            const uint32_t rx_time_us  = 0;
            const int16_t  slave_index = priv.master.id_to_index[ ethernet.rx_header.ethernet.src_addr[5] ];

            RunlogWrite(18, &slave_index);
            InterFgcAddIdToMask(&priv.reception_check.tracked, ethernet.rx_header.ethernet.src_addr[5]);

            HistogramAdd(&communication_histograms.rx.latency_id, slave_index + 1, TimeGetUsDiffNow(rx_time_us));
            HistogramAdd(&communication_histograms.rx.fgc_id,     slave_index + 1, TimeGetAbsUs());
        }
            break;

        default:

            break;
    }
}



static void InterFgcProcessPayload(const inter_fgc_payload_t * const payload, const enum fgc_ether_payload_type payload_type)
{
    switch(payload_type)
    {
//        case FGC_ETHER_PAYLOAD_INTER_FGC_CMD:
//
//            sta.mode_pc = payload->master.mode_pc;
//
//            break;
//
//        case FGC_ETHER_PAYLOAD_INTER_FGC_RSP:
//
//            if (payload->slave.faults)
//            {
//                Set(FAULTS , FGC_FLT_SLAVE);
//            }
//
//            break;

        case FGC_ETHER_PAYLOAD_INTER_FGC:

            // TODO Make sure that discarding volatile from fgc_ether_meas is safe

  //          memcpy((void *)&dpcls.mcu.inter_fgc.fgc_ether_meas, &payload->meas, sizeof(payload->meas));

//            dpcls.mcu.inter_fgc.use_meas = 0x12345678;

            break;

        default:

            break;
    }
}



static uint8_t InterFgcPacketExpected(void)
{
    switch(ethernet.rx_header.fgc.payload_type)
    {
        //case FGC_ETHER_PAYLOAD_INTER_FGC_CMD:
        //
        //    return((inter_fgc.role == FGC_ROLE_SLAVE) &&
        //           (ethernet.rx_header.ethernet.src_addr[5] == inter_fgc.communications.src_addr.master_id));
        //
        //case FGC_ETHER_PAYLOAD_INTER_FGC_RSP:
        //
        //    return(inter_fgc.role == FGC_ROLE_MASTER &&
        //           (local_inter_fgc.master.id_to_index[ethernet.rx_header.ethernet.src_addr[5]] != 0xFF ||
        //            ethernet.rx_header.ethernet.dest_addr[5] == 0xFF));

        case FGC_ETHER_PAYLOAD_INTER_FGC:

            return(inter_fgc.communications.src_addr.remote_meas_id == ethernet.rx_header.ethernet.src_addr[5]);

        default:

            return(0);
    }
}



static void InterFgcRefreshVariables(const uint8_t role)
{
    switch(role)
    {
//        case FGC_ROLE_MASTER:
//
//            InterFgcCheckReception(local_inter_fgc.expected_source_plurality);
//
//            if(!mst.ms_mod_200)
//            {
//                InterFgcSetExpectedCommunicationMask();
//                InterFgcRefreshIdToIndexArray();
//                InterFgcSetProfileRowOfId();
//            }
//
//            break;
//
//        case FGC_ROLE_SLAVE:
//
//            InterFgcCheckReception(local_inter_fgc.expected_source_plurality);
//            break;
//
//        case FGC_ROLE_REMOTE_MEAS:
//
//            InterFgcCheckReception(local_inter_fgc.expected_source_plurality);
//            break;
//
//        case FGC_ROLE_INDEPENDANT:
        default:

            break;
    }
}



static void InterFgcRefreshExpectedSourcePlurality(void)
{
    //const uint8_t role = inter_fgc.role;

//    if (role == FGC_ROLE_SLAVE || role == FGC_ROLE_REMOTE_MEAS ||
//        (role == FGC_ROLE_MASTER &&
//         inter_fgc.master.converter_number == (!inter_fgc.master.no_converter) + 1))
//    {
//        local_inter_fgc.expected_source_plurality = INTER_FGC_UNIQUE_EXPECTED_SOURCE;
//    }
//    else
//    {
//        local_inter_fgc.expected_source_plurality = INTER_FGC_MULTIPLE_EXPECTED_SOURCES;
//    }
}



static void InterFgcRefreshDualPortVariables(void)
{
//    dpcls.mcu.inter_fgc.enable       = inter_fgc.enable;
//    dpcls.mcu.inter_fgc.role         = inter_fgc.role;
//    dpcls.mcu.inter_fgc.no_converter = inter_fgc.master.no_converter;

    InterFgcCountSlaves();
    //dpcls.mcu.inter_fgc.converter_number    = inter_fgc.master.converter_number;

    if(!inter_fgc.communications.src_addr.remote_meas_id)
    {
        //dpcls.mcu.inter_fgc.use_meas = 0x00000000;
    }
}



static INLINE void InterFgcRefreshIdToIndexArray(void)
{
    InterFgcInitIdToIndexArray();
    InterFgcSetIdToIndexArray();
}



static INLINE void InterFgcSetIdToIndexArray(void)
{
    uint16_t slave_idx;

    for (slave_idx = 0; slave_idx < FGC_MAX_SLAVES; slave_idx++)
    {
        priv.master.id_to_index[inter_fgc.communications.src_addr.slave_id[slave_idx] ] = slave_idx;
    }
}



static INLINE void InterFgcInitIdToIndexArray(void)
{
    uint16_t slave_id;

    for (slave_id = 0; slave_id < FGC_ETHER_MAX_FGCS; slave_id++)
    {
        priv.master.id_to_index[slave_id] = 0xFF;
    }
}



//static void InterFgcSetProfileRowOfId(void)
//{
//    const uint8_t * const slaves = inter_fgc.communications.src_addr.slave_id;
//
//    uint16_t slave_idx;
//
//    for (slave_idx = 0; slave_idx < FGC_MAX_SLAVES; slave_idx++)
//    {
//        communication_histograms.rx.fgc_id.row[0].bins[slave_idx]     = slaves[slave_idx];
//        communication_histograms.rx.latency_id.row[0].bins[slave_idx] = slaves[slave_idx];
//    }
//}



static void InterFgcCountSlaves(void)
{
    inter_fgc.master.converter_number = 0;
    uint16_t slave_idx;

    for (slave_idx = 0; slave_idx < FGC_MAX_SLAVES; slave_idx++)
    {
        if (inter_fgc.communications.src_addr.slave_id[slave_idx])
        {
            inter_fgc.master.converter_number++;
        }
    }

    if (inter_fgc.master.no_converter == 0)
    {
        inter_fgc.master.converter_number++;
    }
}



static INLINE void InterFgcAddIdToMask(long_mask_t * const long_mask, const uint16_t id)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called during each reception of a packet from another FGC3.
  It sets the bit corresponding to the source of the packet in the mask of the communication within the millisecond.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(id <= 32)
    {
        long_mask->low  |= 1 << (id - 1);
    }
    else
    {
        long_mask->high |= 1 << (id - 1);
    }
}



/*!
 * This function is called during every start of the millisecond task. It checks the mask of the expected
 * communication from FGC3s against the mask of the actual communication from FGC3s.
 *
 * ToDo: check if understandable:
 * In the case where there is a difference, a counter of missed packet from the source will be incremented according
 * to the bit in which there is a difference.
 *
 * The expected communication should always be greater or equal to the actual communication.
 * The communication mask within the millisecond is then reset for the next millisecond.
 */
//static void InterFgcCheckReception(const enum inter_fgc_sources_expected source_plurality)
//{
//    switch(source_plurality)
//    {
//        case INTER_FGC_UNIQUE_EXPECTED_SOURCE:
//        {
//            local_inter_fgc_slave_t * const slave = &local_inter_fgc.slave;
//
//            if (slave->missing)
//            {
//                if (slave->previous_missing)
//                {
//                    //            sta.mode_pc = FGC_PC_SLOW_ABORT;
//                }
//                else
//                {
//                    slave->previous_missing = 1;
//                }
//            }
//            else
//            {
//                slave->previous_missing = 0;
//            }
//
//            slave->missing = 1;
//        }
//
//            break;
//
//        case INTER_FGC_MULTIPLE_EXPECTED_SOURCES:
//
//            InterFgcCalculateMissingCommunicationMask();
//
//            if (MISSING_COMMUNICATION)
//            {
//                if (PREVIOUSLY_MISSING_COMMUNICATION)
//                {
//                    //            Set(FAULTS , FGC_FLT_SLAVE);
//                    //            sta.mode_pc = FGC_PC_SLOW_ABORT;
//                }
//                else
//                {
//                    InterFgcRecordMissingCommunication();
//                }
//            }
//
//            break;
//
//        default:
//            break;
//    }
//}



//static void InterFgcRecordMissingCommunication(void)
//{
//    uint32_t * const missing_comm = ethernet.stats.frame_count.rcvd.inter_fgc.missing_communication;
//
//    const uint32_t mask_low  = local_inter_fgc.reception_check.missing.low;
//    const uint32_t mask_high = local_inter_fgc.reception_check.missing.high;
//
//    uint16_t id;         // Id (modulo 32) of the FGC3 for which we are comparing actual and expected communication.
//    uint32_t id_mask;    // Mask with one bit set to the bit corresponding to the Id being checked.
//
//    for (id = 1, id_mask = 1; id_mask; id_mask <<= 1, id++)
//    {
//        if (id_mask & mask_low)
//        {
//            missing_comm[id - 1]++;
//        }
//
//        if (id_mask & mask_high)
//        {
//            missing_comm[id - 1 + 32]++;
//        }
//    }
//}
//
//
//
//static void InterFgcSetExpectedCommunicationMask(void)
//{
//    uint16_t idx;
//
//    InterFgcMaskReset(&local_inter_fgc.reception_check.expected);
//
//    for (idx = 0; idx < FGC_MAX_SLAVES; idx++)
//    {
//        if (inter_fgc.communications.src_addr.slave_id[idx] <= FGC_ETHER_MAX_FGCS)
//        {
//            InterFgcAddIdToMask(&local_inter_fgc.reception_check.expected,
//                                inter_fgc.communications.src_addr.slave_id[idx]);
//        }
//    }
//}



//static INLINE void InterFgcCalculateMissingCommunicationMask(void)
//{
//    reception_check_t * const reception_check = &local_inter_fgc.reception_check;
//
//    reception_check->missing.low  = reception_check->expected.low  ^ reception_check->tracked.low;
//    reception_check->missing.high = reception_check->expected.high ^ reception_check->tracked.high;
//}
//
//
//
//static INLINE void InterFgcMaskReset(long_mask_t * const mask)
//{
//    mask->low   = 0;
//    mask->high  = 0;
//}



// External function definitions

void interFgcInit(void)
{
    InterFgcInitVariables();
    InterFgcInitHeader(&priv.packets.tx.header, inter_fgc.communications.dest_addr, inter_fgc.role);
}



void interFgcStartMs(void)
{
    if(!mst.ms_mod_200)
    {
        InterFgcSetHeader(&priv.packets.tx.header, inter_fgc.communications.dest_addr, inter_fgc.role);
        InterFgcRefreshDualPortVariables();
        InterFgcRefreshExpectedSourcePlurality();
    }

    InterFgcRefreshVariables(inter_fgc.role);
}



void interFgcSendPackets(void)
{
    inter_fgc_packet_t * const packet = &priv.packets.tx;

    InterFgcPreparePacket(&packet->metadata, &packet->payload, inter_fgc.role);

    if(packet->header.fgc.payload_type == FGC_ETHER_PAYLOAD_INTER_FGC)
       //packet->header.fgc.payload_type <= FGC_ETHER_PAYLOAD_INTER_FGC_MEAS)
    {
        InterFgcSendPacket(packet);
    }
    else
    {
        ethernet.stats.frame_count.sent.fail++;
    }
}



void interFgcReceivePacket(void)
{
    if(InterFgcPacketExpected())
    {
        InterFgcReadPacket   (&priv.packets.rx);
        InterFgcProcessPacket(&priv.packets.rx,
                              InterFgcPayloadTypeToRole(ethernet.rx_header.fgc.payload_type),
                              priv.expected_source_plurality);

//        ethernet.stats.frame_count.rcvd.inter_fgc.used++;
    }
    else
    {
  //      ethernet.stats.frame_count.rcvd.inter_fgc.unused++;
    }
}



void interFgcPopulateStatus(void)
{

}

// EOF
