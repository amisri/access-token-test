/*---------------------------------------------------------------------------------------------------------*\
 File:      interrupt_handlers.c

 Purpose:   RX610 ISRs - Interrupt Service Routines, for MainProg

\*---------------------------------------------------------------------------------------------------------*/

#include <interrupt_handlers.h>

#include <iodefines.h>

#include <cc_types.h>
#include <defconst.h>           // for FGC_INTERFACE_
#include <dpcls.h>              // for dpcls global variable
#include <ethernet.h>
#include <fip.h>
#include <fbs.h>
#include <fgc_gateway.h>
#include <inter_fgc.h>
#include <log.h>
#include <macros.h>
#include <main.h>               // for main_misc global variable
#include <mcu_dependent.h>      // for REFRESH_SLOWWATCHDOG()
#include <mcu_panic.h>
#include <memmap_mcu.h>         // For RGLEDS_
#include <mst.h>                // for mst global variable
#include <os_hardware.h>        // for OS_INT_EXIT(), OS_INT_ENT()
#include <os.h>                 // for os global variable
#include <pll.h>                // for pll global variable
#include <runlog_lib.h>
#include <sleep.h>
#include <sta.h>
#include <trm.h>                // for terminal global variable
#include <tsk.h>                // for TSK_MST

#if (FGC_CLASS_ID == 61)
#include <get_class.h>
#endif

/*---------------------------------------------------------------------------------------------------------*/
void ProcessMsTimeSlice(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    BOOLEAN     skip_xoff;
    uint8_t     new_char;

    mst.dsp_reg_run_f = (INT16U) dpcls.dsp.reg.run_f;

#if (FGC_CLASS_ID == 61)

    meas.ia = dpcls.dsp.meas.ia;
    meas.ib = dpcls.dsp.meas.ib;

    // Prepare to reset FIFO

    if (dpcls.dsp.ref.start_f != 0)           // Branch if ref start flag is NOT set
    {
        dpcls.dsp.ref.start_f = 0;            // Clear ref start flag
    }

#endif
    // =====================================================================================================

    mst.mode = CPU_MODE_P;                                 // Read back MODE register

    // =====================================================================================================

    // do Terminal hardware

    skip_xoff = FALSE;

    // there is no "end of reception flag" in the uart registers
    // we can scan the SCI0 RXI interrupt flag or use directly the interrupt

    // SCI0_RXI0 - vector 215 - INT_Excep_SCI0_RXI0()

    if (SCI0.SSR.BIT.ORER == 1) // if overrun error, clear error flag
    {
        SCI0.SSR.BIT.ORER = 0;
    }

    if (SCI0.SSR.BIT.FER == 1) // if frame error, clear error flag
    {
        SCI0.SSR.BIT.FER = 0;
    }

    if (SCI0.SSR.BIT.PER == 1)  // if parity error, clear error flag
    {
        SCI0.SSR.BIT.PER = 0;
    }

    // Receive char
    //  ICU.IR215.BIT.IR = 0;                               // Clear the interrupt request flag
    if (ICU.IR215.BIT.IR == 1)                          // interrupt generated? (equivalent to char received)
    {
        ICU.IR215.BIT.IR = 0;                           // Clear the interrupt request flag
        new_char = SCI0.RDR;                            // Read character from input data register


        if ((new_char & 0x80) != 0x80)                  // Ignore if bit 7 is set (B=128->255)
        {
            if (new_char == 19)                         // Check if character is XOFF (19) (halt SCI output)
            {
                // XOFF received: Set XOFF timer
                terminal.xoff_timer = 60000;                 // to 60s and fall through...
                skip_xoff = TRUE;
            }
            else
            {
                if (new_char == 17)                     // Check if character is XON (17) (resume SCI output)
                {
                    terminal.xoff_timer = 0;                 // XON received: clear XOFF timer
                }
                else
                {
                    if (new_char == 3)                  // Check if character is CTRL-C (3) (abort SCI command)
                    {
                        tcm.abort_f = TRUE;             // Abort SCI command
                        terminal.xoff_timer = 0;             // clear XOFF timer
                        // Flush the SCI message queue...
                        OSMsgFlush(terminal.msgq);           // of any waiting characters
                    }
                    else
                    {
                        OSMsgPost(terminal.msgq, (void *)(uintptr_t) new_char);
                    }
                }

            }
        }
    }

    if (skip_xoff == FALSE)
    {
        if (terminal.char_waits_for_tx == TRUE)
        {
            if (terminal.xoff_timer != 0)                    // Check if XOFF is active...
            {
                terminal.xoff_timer--;                       // decrement XOFF timer by 1 ms
            }
            else
            {
                // Chars to transmit

                // TDRE Transmit Data Register Empty
                //   0: Transmit register is not empty.
                //   1: Transmit register is empty, data has been transfered to shift register.

                if (SCI0.SSR.BIT.TDRE == 1)             // No data in transmit buffer
                {
                    //                  if ( terminal.tx.n_ch != 0 )             // there are chars to send
                    {
                        SCI0.TDR = terminal.snd_ch;          // write char in transmit buffer
                        terminal.char_waits_for_tx = FALSE;      // Clear flag to say the character has been sent
                    }
                }
            }
        }
    }

    OSTskResume(TSK_MST);
}
/*=========================================================================================================*/
// FIXED VECTORS
/*=========================================================================================================*/
/*---------------------------------------------------------------------------------------------------------*/
//void PowerON_Reset(void)
/*---------------------------------------------------------------------------------------------------------*\
 RESET - Fixed vector
 declared in reset_program.S
\*---------------------------------------------------------------------------------------------------------*/
//{
//   __asm__ volatile ( "rte" );    // return from interrupt, if __attribute__ ((naked)) is used
//}
/*---------------------------------------------------------------------------------------------------------*/
/*=========================================================================================================*/
// RELOCATABLE VECTORS
/*=========================================================================================================*/
/*---------------------------------------------------------------------------------------------------------*/
// void OSTskCtxSwitch(void)
/*---------------------------------------------------------------------------------------------------------*\
 vector  1 pure software, hardware reserved OSTskCtxSwitch()
 implemented in os_tsk.c
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    __asm__ volatile ( "rte" );     // return from interrupt, if __attribute__ ((naked)) is used
}
*/
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ2(void) // naked, we have OS_INT_ENT
/*---------------------------------------------------------------------------------------------------------*\
    network packet reception irq

    IRQ2 - vector 66 - FIP_~MSG_IRQ (in case of FIP board this an IRQ3 - vector 67 - FIP_~TIME_IRQ are generated simultaneously)
                       or Ethernet MSG IRQ

    ICU.IER08.BIT.IEN2      Interrupt Request Enable Register
    ICU.IPR22.BIT.IPR_20    Interrupt Priority Register
    ICU.IR066.BIT.IR        Interrupt request register 066 (status)
    ISELR066

  when FIP board

  The source of this interrupt is the MicroFIP interface IRQ.
  Only the Var 7 reception has an interrupt enabled, so there can be no other reason for the interrupt.
  Var 7 is used twice per WorldFIP cycle
    - first for the time variable (0x3000)
    and then
    - for the code variable (0x3004).
  When the time variable is received it first reads the time variable and then writes the status variable.
  It then runs the phase locked loop to maintain the phase between the millisecond interrupts and the
  FIP interrupts.
  If code reception from the FIP is enabled, the FipReadTimeVar function will set the Var 7 global
  variable identifier to the 0x3004 to receive the code variable
  which will return the identifier to 0x3000 for the time variable on the next cycle.


  when ETHERNET board

  The source of this interrupt is packet reception in the LAN chip

\*---------------------------------------------------------------------------------------------------------*/
{
    OS_INT_ENT(entryFieldbusISR);

    pll.irqs_count_all++;
    FbsISR();
#ifdef FGC_DEBUG_OS_HISTORY
    os_history.tsk_switch[os_history.index].last_isr = 2;
#endif

    OS_INT_EXIT(); // Exit from interrupt through scheduler (Never returns)

    //  __asm__ volatile ( "rte" ); // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ3(void)
/*---------------------------------------------------------------------------------------------------------*\
    network time synchronisation irq

    IRQ3 - vector 67 - FIP_~TIME_IRQ (in case of FIP board this an IRQ2 - vector 66 - FIP_~MSG_IRQ are generated simultaneously)
                       or synchronisation pulse irq (when Ethernet board)

    ICU.IER08.BIT.IEN3          Interrupt Request Enable Register
    ICU.IPR23.BIT.IPR_20        Interrupt Priority Register
    ICU.IR067.BIT.IR            Interrupt request register 067 (status)
    ISELR067

  when FIP board

  The source of this interrupt is the MicroFIP interface IRQ.
  Only the Var 7 reception has an interrupt enabled, so there can be no other reason for the interrupt.
  Var 7 is used twice per WorldFIP cycle
    - first for the time variable (0x3000)
    and then
    - for the code variable (0x3004).
  When the time variable is received it first reads the time variable and then writes the status variable.
  It then runs the phase locked loop to maintain the phase between the millisecond interrupts and the
  FIP interrupts.
  If code reception from the FIP is enabled, the FipReadTimeVar function will set the Var 7 global
  variable identifier to the 0x3004 to receive the code variable
  which will return the identifier to 0x3000 for the time variable on the next cycle.


  when ETHERNET board

  The source of this interrupt is the 50Hz pulse from the timing system in the gateway

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ4(void) // naked, we have OS_INT_ENT
/*---------------------------------------------------------------------------------------------------------*\
    IRQ4 - vector 68 - MCU_~TICK - "hardware" 1ms Tick

    ICU.IER08.BIT.IEN4      Interrupt Request Enable Register
    ICU.IPR24.BIT.IPR_20    Interrupt Priority Register
    ICU.IR068.BIT.IR        Interrupt request register 068 (status)
    ISELR068
    PFCR9

    1ms Tick used to schedule the program
    this hardware input comes from the Xilinx Spartan 3AN FPGA (XC3S 700AN - 4FG484C),
    it is the output of the digital PLL implemented in it

    (this interrupt must be similar to INT_Excep_TMR0_CMIA0() - vector 174, that is the backup of this
     interrupt, 1ms generated internally by TMR0)
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_INT_ENT(entryIRQ4);

    //    ICU.IR068.BIT.IR = 0;


    //    if (fip.stats.msg_rcvd & 0x01)
    if (! P3.PORT.BIT.B4)
    {
        mst.isr_start_us = UTC_US_P;
        ProcessMsTimeSlice();
        REFRESH_SLOWWATCHDOG();

    }
    else
    {
        interFgcSendPackets();
    }

#ifdef FGC_DEBUG_OS_HISTORY
    os_history.tsk_switch[os_history.index].last_isr = 4;
#endif

    OS_INT_EXIT();              // Exit from interrupt through scheduler (Never returns)

    //  __asm__ volatile ( "rte" );     // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR0_CMIA0(void) // not naked
/*---------------------------------------------------------------------------------------------------------*\
    CMIA0 - Compare Match A Interrupt - vector 174 - "software" 1ms Tick

    ICU.IER15.BIT.IEN6      Interrupt Request Enable Register
    ICU.IPR68.BIT.IPR_20    Interrupt Priority Register
    ICU.IR174.BIT.IR        Interrupt request register 174 (status)
    ISELR174

    TMR0 is used as a 16 bit counter incremented by PCLK 50MHz counting 50000 = 1ms

    it is programmed to generate an interrupt every 1ms

    (backup) internally generated 1ms Tick used to schedule the program
    (so this one must be similar to INT_Excep_IRQ4() - vector 68 - MCU_~TICK)

    It is used when the Xilinx Spartan 3AN FPGA (XC3S 700AN - 4FG484C) is unprogrammed and no tick is present.

\*---------------------------------------------------------------------------------------------------------*/
{
    REFRESH_SLOWWATCHDOG(); // trigger slowWatchdog SWD if enabled
    ProcessMsTimeSlice();

    //  __asm__ volatile ( "rte" ); // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TGI0A(void)  // not naked
/*---------------------------------------------------------------------------------------------------------*\
    TPU0 TGI0A - vector 104

    ICU.IER0D.BIT.IEN0      Interrupt Request Enable Register
    ICU.IPR4C.BIT.IPR_20    Interrupt Priority Register
    ICU.IR104.BIT.IR        Interrupt request register 104 (status)
    ISELR104

 fired by TPU0.TGRA input capture/compare match
\*---------------------------------------------------------------------------------------------------------*/
{
    // ToDo Unused in main, should be disabled

}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR3_OVI3(void) // not naked
/*---------------------------------------------------------------------------------------------------------*\
    OVI3 - Timer 3 Overflow Interrupt - vector 185

    TMR3.TCNT                   overflow INT enable
    ICU.IER17.BIT.IEN1          Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20        Interrupt Priority Register
    ICU.IR185.BIT.IR            Interrupt request register 185 (status)

 TMR3 will count [0..255] incremented by TMR2 count match (1us)
 and generating an interrupt when overflows

 TMR2 will count [0..49] increment by PCLK (50MHz,20ns), the period is 1us

 we count the TMR3 overflows to enhance USLEEP()

 This interrupt is set to level 7, and never blocked

\*---------------------------------------------------------------------------------------------------------*/
{
    // ToDo Unused in main, should be disabled

}
/*=========================================================================================================*/
// Not used interrupts
/*=========================================================================================================*/
/*=========================================================================================================*/
// FIXED VECTORS
/*=========================================================================================================*/
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SupervisorInst(void)
/*---------------------------------------------------------------------------------------------------------*\
 Exception(Supervisor Instruction) - Fixed vector
\*---------------------------------------------------------------------------------------------------------*/
{
    GET_ISP(sta.mcu_panic.sp);

    sta.mcu_panic.pc = *(sta.mcu_panic.sp + 1);         // PC stored in the stack at the time of the interrupt

    McuPanic(MCU_PANIC_EXCEP_SUPERVISOR_INSTR,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_UndefinedInst(void)
/*---------------------------------------------------------------------------------------------------------*\
 Exception(Undefined Instruction) - Fixed vector
\*---------------------------------------------------------------------------------------------------------*/
{
    GET_ISP(sta.mcu_panic.sp);

    sta.mcu_panic.pc = *(sta.mcu_panic.sp + 1);         // PC stored in the stack at the time of the interrupt

    McuPanic(MCU_PANIC_EXCEP_INVALID_INSTR,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_FloatingPoint(void)
/*---------------------------------------------------------------------------------------------------------*\
 Exception(Floating Point) - Fixed vector
\*---------------------------------------------------------------------------------------------------------*/
{
    GET_ISP(sta.mcu_panic.sp);

    sta.mcu_panic.pc = *(sta.mcu_panic.sp + 1);         // PC stored in the stack at the time of the interrupt

    McuPanic(MCU_PANIC_EXCEP_FLOATING_POINT,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_NonMaskableInterrupt(void)
/*---------------------------------------------------------------------------------------------------------*\
 NMI  - Fixed vector
\*---------------------------------------------------------------------------------------------------------*/
{
    GET_ISP(sta.mcu_panic.sp);

    sta.mcu_panic.pc = *(sta.mcu_panic.sp + 1);         // PC stored in the stack at the time of the interrupt

    McuPanic(MCU_PANIC_EXCEP_NMI,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}
/*---------------------------------------------------------------------------------------------------------*/
/*=========================================================================================================*/
// RELOCATABLE VECTORS
/*=========================================================================================================*/
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_BRK(void)
/*---------------------------------------------------------------------------------------------------------*\
 vector  0 - BRK
 This vector is triggered when the core executes the BRK (break) instruction. BRK's opcode being 00,
 it is very likely that this interrupt handler be called after a faulty jump in memory (if the core
 starts executing junk code)
\*---------------------------------------------------------------------------------------------------------*/
{
    GET_ISP(sta.mcu_panic.sp);

    sta.mcu_panic.pc = *(sta.mcu_panic.sp + 1);         // PC stored in the stack at the time of the interrupt

    McuPanic(MCU_PANIC_EXCEP_BRK,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_BUSERR(void)
/*---------------------------------------------------------------------------------------------------------*\
 BUSERR
\*---------------------------------------------------------------------------------------------------------*/
{
    GET_ISP(sta.mcu_panic.sp);

    sta.mcu_panic.pc = *(sta.mcu_panic.sp + 1);         // PC stored in the stack at the time of the interrupt

    McuPanic(MCU_PANIC_EXCEP_BUS_ERROR,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_FCU_FCUERR(void)
/*---------------------------------------------------------------------------------------------------------*\
  vector 21 - FCU_FCUERR
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_FCU_FRDYI(void)
/*---------------------------------------------------------------------------------------------------------*\
 FCU_FRDYI
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_CMTU0_CMT0(void)
/*---------------------------------------------------------------------------------------------------------*\
 CMTU0_CMT0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_CMTU0_CMT1(void)
/*---------------------------------------------------------------------------------------------------------*\
 CMTU0_CMT1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_CMTU1_CMT2(void)
/*---------------------------------------------------------------------------------------------------------*\
 CMTU1_CMT2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_CMTU1_CMT3(void)
/*---------------------------------------------------------------------------------------------------------*\
 CMTU1_CMT3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ0(void)
/*---------------------------------------------------------------------------------------------------------*\
 0x0100 int  64  IRQ0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ1(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ5(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ5
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ6(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ6
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ7(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ7
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ8(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ8
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ9(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ9
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ10(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ10
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ11(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ11
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ12(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ12
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ13(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ13
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ14(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ14
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ15(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ15
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_WDT_WOVI(void)
/*---------------------------------------------------------------------------------------------------------*\
 WDT_WOVI
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_AD0_ADI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 AD0_ADI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_AD1_ADI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 AD1_ADI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_AD2_ADI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 AD2_ADI2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_AD3_ADI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 AD3_ADI3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TGI0B(void)
/*---------------------------------------------------------------------------------------------------------*\
 fired by TPU0.TGRB input capture/compare match
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TGI0C(void)
/*---------------------------------------------------------------------------------------------------------*\
 fired by TPU0.TGRC input capture/compare match
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TGI0D(void)
/*---------------------------------------------------------------------------------------------------------*\
 fired by TPU0.TGRD input capture/compare match
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TCI0V(void)
/*---------------------------------------------------------------------------------------------------------*\
 fired by TPU0.TCNT overflow
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU1_TGI1A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU1_TGI1A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU1_TGI1B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU1_TGI1B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU1_TCI1V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU1_TCI1V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU1_TCI1U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU1_TCI1U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU2_TGI2A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU2_TGI2A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU2_TGI2B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU2_TGI2B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU2_TCI2V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU2_TCI2V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU2_TCI2U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU2_TCI2U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TGI3A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TGI3A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TGI3B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TGI3B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TGI3C(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TGI3C
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TGI3D(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TGI3D
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TCI3V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TCI3V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU4_TGI4A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU4_TGI4A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU4_TGI4B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU4_TGI4B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU4_TCI4V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU4_TCI4V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU4_TCI4U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU4_TCI4U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU5_TGI5A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU5_TGI5A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU5_TGI5B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU5_TGI5B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU5_TCI5V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU5_TCI5V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU5_TCI5U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU5_TCI5U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TGI6A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TGI6A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TGI6B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TGI6B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TGI6C(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TGI6C
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TGI6D(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TGI6D
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TCI6V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TCI6V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU7_TGI7A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU7_TGI7A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU7_TGI7B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU7_TGI7B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU7_TCI7V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU7_TCI7V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU7_TCI7U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU7_TCI7U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU8_TGI8A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU8_TGI8A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU8_TGI8B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU8_TGI8B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU8_TCI8V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU8_TCI8V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU8_TCI8U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU8_TCI8U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TGI9A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TGI9A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TGI9B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TGI9B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TGI9C(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TGI9C
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TGI9D(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TGI9D
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TCI9V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TCI9V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU10_TGI10A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU10_TGI10A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU10_TGI10B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU10_TGI10B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU10_TCI10V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU10_TCI10V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU10_TCI10U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU10_TCI10U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU11_TGI11A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU11_TGI11A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU11_TGI11B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU11_TGI11B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU11_TCI11V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU11_TCI11V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU11_TCI11U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU11_TCI11U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR0_CMIB0(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIB0 - Compare Match B Interrupt - vector 175

    ICU.IER15.IEN7
    ICU.IPR68.BIT.IPR_20        Interrupt Priority Register
    ICU.IR175.BIT.IR            Interrupt request register 175 (status)
    ISELR175
\*---------------------------------------------------------------------------------------------------------*/
{
    // ICU.IR175.BIT.IR = 0;        // Clear the interrupt request flag
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR0_OVI0(void)
/*---------------------------------------------------------------------------------------------------------*\
    OVI0 - Timer 0 Overflow Interrupt - vector 176

    TMR0.TCNT                   overflow INT enable
    ICU.IER16.BIT.IEN0          Interrupt Request Enable Register
    ICU.IPR68.BIT.IPR_20        Interrupt Priority Register
    ICU.IR176.BIT.IR            Interrupt request register 176 (status)
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR176.BIT.IR = 0;        // Clear the interrupt request flag
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR1_CMIA1(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIA1 - Compare Match A Interrupt - vector 177

    ICU.IER16.BIT.IEN1          Interrupt Request Enable Register
    ICU.IPR69.BIT.IPR_20        Interrupt Priority Register
    ICU.IR177.BIT.IR            Interrupt request register 177 (status)
    ISELR177
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR1_CMIB1(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIB1 - Compare Match B Interrupt - vector 178

    ICU.IER16.BIT.IEN2          Interrupt Request Enable Register
    ICU.IPR69.BIT.IPR_20        Interrupt Priority Register
    ICU.IR178.BIT.IR            Interrupt request register 178 (status)
    ISELR178
\*---------------------------------------------------------------------------------------------------------*/
{
    // ICU.IR178.BIT.IR = 0;        // Clear the interrupt request flag
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR1_OVI1(void)
/*---------------------------------------------------------------------------------------------------------*\
    OVI1 - Timer 1 Overflow Interrupt - vector 179

    TMR1.TCNT                   overflow INT enable
    ICU.IER16.BIT.IEN3          Interrupt Request Enable Register
    ICU.IPR69.BIT.IPR_20        Interrupt Priority Register
    ICU.IR179.BIT.IR            Interrupt request register 179 (status) 8870B3h 8
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR179.BIT.IR = 0;        // Clear the interrupt request flag
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR2_CMIA2(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIA2 - Compare Match A Interrupt - vector 180

    ICU.IER16.BIT.IEN4          Interrupt Request Enable Register
    ICU.IPR6A.BIT.IPR_20        Interrupt Priority Register
    ICU.IR180.BIT.IR            Interrupt request register 180 (status)
    ISELR180
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR2_CMIB2(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIB2 - Compare Match B Interrupt - vector 181

    ICU.IER16.BIT.IEN5          Interrupt Request Enable Register
    ICU.IPR6A.BIT.IPR_20        Interrupt Priority Register
    ICU.IR181.BIT.IR            Interrupt request register 181 (status)
    ISELR181
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR2_OVI2(void)
/*---------------------------------------------------------------------------------------------------------*\
    OVI2 - Timer 2 Overflow Interrupt - vector 182

    TMR2.TCNT                   overflow INT enable
    ICU.IER16.BIT.IEN6          Interrupt Request Enable Register
    ICU.IPR6A.BIT.IPR_20        Interrupt Priority Register
    ICU.IR182.BIT.IR            Interrupt request register 182 (status)
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR3_CMIA3(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIA3 - Compare Match A Interrupt - vector 183

    ICU.IER16.BIT.IEN7          Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20        Interrupt Priority Register
    ICU.IR183.BIT.IR            Interrupt request register 183 (status)
    ISELR183
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR3_CMIB3(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIB3 - Compare Match B Interrupt - vector 184

    ICU.IER17.BIT.IEN0          Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20        Interrupt Priority Register
    ICU.IR184.BIT.IR            Interrupt request register 184 (status)
    ISELR184
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_DMAC_DMTEND0(void)
/*---------------------------------------------------------------------------------------------------------*\
 DMAC_DMTEND0
 DMA0 is used to send data from circular buffer in external ram to LAN chip
 At end of completion we restart DMA if needed (circular goes back to zero, DMA is split in 2 steps),
 and we update the queue output.
     ICU.IER18.BIT.IEN6          Interrupt Request Enable Register
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_DMAC_DMTEND1(void)
/*---------------------------------------------------------------------------------------------------------*\
 DMAC_DMTEND1
 DMA1 is used to send data from circular buffer in external ram to LAN chip
 At end of completion we update the queue output index.
     ICU.IER18.BIT.IEN6          Interrupt Request Enable Register
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_INT_ENT(entryDMTEND1);

#ifdef FGC_DEBUG_OS_HISTORY
    os_history.tsk_switch[os_history.index].last_isr = 10; // Arbitrary constant for debugging
#endif

    FgcGatewayDmac1End();

    OS_INT_EXIT();
    //    __asm__ volatile ( "rte" );     // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_DMAC_DMTEND2(void)
/*---------------------------------------------------------------------------------------------------------*\
 DMAC_DMTEND2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_DMAC_DMTEND3(void)
/*---------------------------------------------------------------------------------------------------------*\
 DMAC_DMTEND3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI0_ERI0(void)
/*---------------------------------------------------------------------------------------------------------*\
  - vector 214
   ICU.IER1A.BIT.IEN6           Interrupt Request Enable Register
   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
 UART0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI0_RXI0(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 215
   ICU.IER1A.IEN7
   ICU.IPR80.BIT.IPR_20 Interrupt Priority Register
 ISELR215
 UART0
\*---------------------------------------------------------------------------------------------------------*/
{
    //  SCI0.RDR is the received character
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI0_TXI0(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 216
   ICU.IER1B.BIT.IEN0           Interrupt Request Enable Register
   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
 ISELR216
 UART0
\*---------------------------------------------------------------------------------------------------------*/
{
    //  SCI0.TDR = char to transmit;
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI0_TEI0(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 217
   ICU.IER1B.BIT.IEN1           Interrupt Request Enable Register
   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
 UART0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI1_ERI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI1_ERI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI1_RXI1(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 219
    ICU.IER1B.IEN3              Interrupt Request Enable Register
    ICU.IPR81.BIT.IPR_20        Interrupt Priority Register
    ICU.IR220.BIT.IR            Interrupt request register 219 (status)
 ISELR219
 UART1
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR219.BIT.IR = 0;        // Clear the interrupt request flag

    //  SCI1.RDR is the received character
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI1_TXI1(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 220
    ICU.IER1B.BIT.IEN4          Interrupt Request Enable Register
    ICU.IPR81.BIT.IPR_20        Interrupt Priority Register
    ICU.IR220.BIT.IR            Interrupt request register 220 (status)

 ISELR220
 UART1
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR220.BIT.IR = 0;        // Clear the interrupt request flag

    //  SCI0.TDR = char to transmit;
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI1_TEI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI1_TEI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI2_ERI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI2_ERI2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI2_RXI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI2_RXI2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI2_TXI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI2_TXI2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI2_TEI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI2_TEI2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI3_ERI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI3_ERI3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI3_RXI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI3_RXI3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI3_TXI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI3_TXI3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI3_TEI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI3_TEI3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI4_ERI4(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI4_ERI4
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI4_RXI4(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI4_RXI4
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI4_TXI4(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI4_TXI4
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI4_TEI4(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI4_TEI4
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI5_ERI5(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI5_ERI5
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI5_RXI5(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI5_RXI5
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI5_TXI5(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI5_TXI5
- vector 236

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI5_TEI5(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI5_TEI5
- vector 237

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI6_ERI6(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 238 (level)
    ICU.IER1D.BIT.IEN6          Interrupt Request Enable Register
    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
    ICU.IR238.BIT.IR            Interrupt request register 238 (status)

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI6_RXI6(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 239 (edge)
    ICU.IER1D.BIT.IEN7          Interrupt Request Enable Register
    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
    ICU.IR239.BIT.IR            Interrupt request register 239 (status)

 ISELR239
 UART6
\*---------------------------------------------------------------------------------------------------------*/
{
    //  ICU.IR239.BIT.IR = 0;       // Clear the interrupt request flag
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI6_TXI6(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 240 (edge)
    ICU.IER1E.BIT.IEN0          Interrupt Request Enable Register
    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
    ICU.IR240.BIT.IR            Interrupt request register 240 (status)

 ISELR240
 UART6
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI6_TEI6(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 241 (level)
    ICU.IER1E.BIT.IEN1          Interrupt Request Enable Register
    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
    ICU.IR241.BIT.IR            Interrupt request register 241 (status)

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC0_EEI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC0_EEI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC0_RXI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC0_RXI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC0_TXI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC0_TXI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC0_TEI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC0_TEI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC1_EEI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC1_EEI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC1_RXI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC1_RXI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC1_TXI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC1_TXI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC1_TEI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC1_TEI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: interrupt_handlers.c
\*---------------------------------------------------------------------------------------------------------*/
