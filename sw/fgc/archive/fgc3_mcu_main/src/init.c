/*!
 *  @file      init.c
 *  @defgroup  FGC:MCU
 *  @brief     Initialisation functions that are not linked to other files.
 */

#define DEFPROPS_INC_ALL    // defprops.h
#define TSK_GLOBALS         // define tsk_stk global variable
#define INIT_GLOBALS

// Includes

#include <init.h>
#include <init_class.h>

#include <iodefines.h>      // specific processor registers and constants
#include <string.h>         // for strlen(), strncpy()

#include <bgp.h>            // for BgpTsk()
#include <cal.h>            // for cal global variable
#include <class.h>
#include <defconst.h>       // defconst.h is needed by fgc_fip.h because it includes fgc_code.h that needs FGC_MAX_DEVS_PER_GW
#include <definfo.h>        // for FGC_CLASS_ID
#include <defprops.h>       // Include property related definitions
#include <dev.h>            // for dev global variable
#include <dls.h>            // for DlsTsk()
#include <dpcom.h>          // for dpcom global variable
#include <fbs.h>            // for fbs global variable, FbsTsk()
#include <fgc_code.h>       // code communication structures, for struct fgc_dev_name, struct fgc_code_info
#include <fgc/fgc_db.h>     // for access to SysDB, CompDB and DIMDB
#include <fgc_errs.h>       // for FGC_OK_NO_RSP
#include <fgc_runlog_entries.h> // Include run log definition, for FGC_RL_
#include <fip.h>            // for fip global variable
#include <hardware_setup.h> // for Enable1msTickInterruptFromPLD()
#include <hash.h>           // for HashInit()
#include <macros.h>         // for Test(), Set(), Clr()
#include <memmap_mcu.h>
#include <mst.h>            // for mst global variable, MstTsk()
#include <nvs.h>
#include <pars_service.h>
#include <prop.h>           // for PropSet() and PropMap()
#include <pub.h>
#include <regfgc3.h>
#include <rtd.h>            // for RtdTsk()
#include <runlog_lib.h>     // for RunlogWrite(), RunlogInit()
#include <regfgc3_pars.h>
#include <regfgc3_task.h>
#include <shared_memory.h>  // for shared_mem global variable, FGC_MP_MAIN_RUNNING
#include <sta.h>            // for sta global variable
#include <start.h>          // for StartRunningDsp(), WaitForDspStarted()
#include <state_manager.h>      // for StaTsk()
#include <trm.h>            // for terminal task
#include <tsk.h>
#include <version.h>        // for version
#include <pll.h>



// Constants

/*!
 * @brief Used for specifying type initialisation method
 */
enum InitTypeFrom
{
    INIT_TYPE_FROM_NAME,
    INIT_TYPE_FROM_PROPERTY,
    INIT_TYPE_FROM_UNKNOWN_SYSTEM_RECORD,
};

// Internal functions declarations

/*!
 * @brief Compares DB versions and initialises some of the DB-related structures
 */
static void InitDbStructures(void);

/*!
 * @brief Initialises device name based on mode (stand-alone/on-line)
 *
 * @param[out] dev_name A structure to which the function will save the name
 */
static void InitDeviceName(struct fgc_dev_name * dev_name);

/*!
 * @brief Queries SysDB for device type
 *
 * @return True if type found in DB, false otherwise
 */
static BOOLEAN InitFindTypeInSysDb(void);

/*!
 * @brief Initialises device type using suggested method
 *
 * This function can initialise the type using three methods: extracting type from name, reading type from property and
 * setting type to unknown. In the last case the function will modify the structure passed as an argument.
 *
 * @param[in] from Method used for type initialisation
 * @param[in] dev_name Name structure initialised earlier
 */
static void InitType(enum InitTypeFrom from, const struct fgc_dev_name * dev_name);

/*!
 * @brief Sets device name to 'Unknown'
 *
 * @param[in] dev_name Name structure initialised earlier
 */
static void InitSetDeviceNameToUnknown(struct fgc_dev_name * dev_name);

/*!
 * @brief Initialises system-related structures based on name/type
 *
 * @param[in] dev_name Name structure initialised earlier
 */
static void InitSystemStructures(const struct fgc_dev_name * dev_name);

static void InitPropParentLinksSet(INT16U lvl, struct prop * p);
static INT32U InitPropPPMFlagAndPubSize(struct prop * p);



// Internal function definitions


#define SYSDB               ( (struct fgc_sysdb * const) CODES_SYSDB_SYSDB_32 )
#define COMPDB              ( (struct fgc_compdb * const) CODES_COMPDB_COMPDB_32 )
#define DIMDB               ( (void *) CODES_DIMDB_DIMDB_32 )    // DIMDB now points to the beginning of the DimDb, not to the DimDb types

static void InitDbStructures(void)
{
    SysDbInit(SYSDB);
    CompDbInit(COMPDB);
    DimDbInit(DIMDB);
    
    dev.dbs_ok_f = (SysDbVersion() == CompDbVersion() &&
                    SysDbVersion() == DimDbVersion());
}



static void InitDeviceName(struct fgc_dev_name * dev_name)
{
    // In stand-alone mode get device name from the property
    // When FIP is connected get name from NameDB

    if (FbsIsStandalone())
    {
        strncpy(dev_name->name, "STANDALONE", FGC_MAX_DEV_LEN);
        dev_name->name[FGC_MAX_DEV_LEN] = '\0';

        // Clear Last Calibration time if not connected to the Gateway
        cal.last_cal_time_unix = 0;
    }
    else
    {
        struct fgc_dev_name * dev_name_fp;

        // Get access to NameDB
        dev_name_fp    = ((struct fgc_name_db *) CODES_NAMEDB_32)->dev;

        // Extract gateway name record
        fbs.host       = dev_name_fp[0];

        *dev_name      = dev_name_fp[fbs.id];

        // Take omode_mask from NameDB record
        
        dev.omode_mask = dev_name->omode_mask;

        pll.no_ext_sync = Test(dev.omode_mask, FGC_LHC_SECTORS_NO_EXT_SYNC);
    }
}


static BOOLEAN InitFindTypeInSysDb(void)
{
    int16_t         cmp;                    // For system type comparisons
    const fgc_db_t  n_sys = SysDbLength();  // Num of systems

    // Search for type in SysDB
    // Start from 1, because record 0 is an unknown system

    dev.sys.sys_idx = 1;

    do
    {
        cmp = strcmp(SysDbType(dev.sys.sys_idx), dev.sys.sys_type);
    }
    while ((cmp != 0) && (++dev.sys.sys_idx < n_sys));

    return (cmp == 0);
}



static void InitType(enum InitTypeFrom from, const struct fgc_dev_name * dev_name)
{
    if (from == INIT_TYPE_FROM_NAME)
    {
        // Extract type from device name

        char * c;

        strncpy(dev.sys.sys_type, dev_name->name, FGC_SYSDB_SYS_LEN);
        dev.sys.sys_type[FGC_SYSDB_SYS_LEN] = '\0';

        // Replace first '.' with null

        for (c = dev.sys.sys_type; (*c) && (*c != '.'); c++);

        *c = '\0';
    }
    else if (from == INIT_TYPE_FROM_PROPERTY)
    {
        strncpy(dev.sys.sys_type, dev.type, FGC_SYSDB_SYS_LEN);
        dev.sys.sys_type[FGC_SYSDB_SYS_LEN] = '\0';
    }
    else
    {
        // System type is unknown

        dev.sys.sys_idx = 0;

        // Device type: 'UKNWN'

        strncpy(dev.sys.sys_type, SysDbType(0), FGC_SYSDB_SYS_LEN);
        dev.sys.sys_type[FGC_SYSDB_SYS_LEN] = '\0';
    }
}



static void InitSetDeviceNameToUnknown(struct fgc_dev_name * dev_name)
{
    // Device name: 'Unknown' -> 7 characters

    strncpy(dev_name->name, SysDbLabel(0), 7);
    dev_name->name[7] = '\0';
}



static void InitSystemStructures(const struct fgc_dev_name * dev_name)
{
    PropSet(&tcm, &PROP_FGC_NAME, dev_name->name, strlen(dev_name->name));

    dev.type_nels = strlen(dev.sys.sys_type);
    PropSet(&tcm, &PROP_DEVICE_TYPE, dev.sys.sys_type, dev.type_nels);
}



static void InitPropParentLinksSet(INT16U lvl, struct prop * p)
{
    /* This function to link each property to its parent and propagate the
     * DF_TIMESTAMP_SELECT flag to children
     */

    if (lvl > 0)
    {
        p->parent = init.parent_prop[lvl - 1];

        if (Test(p->parent->dynflags, DF_TIMESTAMP_SELECT))
        {
            Set(p->dynflags, DF_TIMESTAMP_SELECT);
        }
    }
    else
    {
        p->parent = NULL;
    }

    if (p->type == PT_PARENT)
    {
        init.parent_prop[lvl] = p;
    }
}



static INT32U InitPropPPMFlagAndPubSize(struct prop * p)
{
    /* This function is called for PARENT and child properties. It is RECURSIVE
     *  and scans the property tree to:
     *   - Set the PPM flag for all properties that have a PPM child property.
     *   - Count the potential size of data produced by the property using a
     *     get function. This is used to decide if the publication can be direct
     *     or must be via a GET request to the gateway.
     */

    INT16U      idx;
    INT32U      max_size = 0;

    if (p->type == PT_PARENT)                       // If PARENT property
    {
        if (p->value != &PROPS)                     // If property doesn't loop back to top level
        {
            for (idx = 0 ; idx < p->max_elements ; idx++) // For each child property
            {
                max_size += InitPropPPMFlagAndPubSize((struct prop *)p->value + idx);  // Call recursively
            }
        }
    }
    else                                            // else CHILD property
    {
        if (p->get_func_idx)                        // If get function present
        {
            max_size = p->max_elements * prop_type_size[p->type];        // Calc max size in bytes
        }
    }

    if ((max_size > SUB_USE_GET_SIZE_LIMIT)           // If max size is above threshold
        && (fbs.net_type == NETWORK_TYPE_FIP)
       )
    {
        Set(p->dynflags, DF_PUB_USING_GET);         // Set PUB USING GET flag
    }

    if (Test(p->flags, PF_HIDE))                    // If property is HIDDEN
    {
        max_size = 0;                               // Report size 0 to parent
    }

    if (p->parent &&                                // If property has a parent, and
        Test(p->flags, PF_PPM) &&                    // property is PPM and
        max_size)                                    // property is visible to a get of the parent
    {
        Set(p->parent->flags, PF_PPM);              // Set PPM flag for the parent
    }

    return (max_size);
}



// External function definitions



void InitMCU(void)
{
    INT16U      ii;
    INT16U      tmp;


    //  ToDo: check this, as it is different from FGC2
    tmp = CPU_RESET_CTRL_DIGITAL_MASK16 | ~P9.PORT.BIT.B7;

    mst.reset = mst.reset & ~tmp;

    // difference between FGC2 and FGC3
    //  CPU_RESET_P &= ~CPU_RESET_CTRL_NETWORK_MASK16;              // Remove hardware reset of uFIP
    // Releases FIP/ETHERNET hw reset
    if (fbs.net_type == NETWORK_TYPE_FIP)
    {
        P9.DR.BIT.B7 = 1;   // FIP is running
        FIP_RESET_P = 0;
    }

    dev.main_prog_version = version.unixtime;

    RunlogWrite(FGC_RL_UXTIME, (void *) &UTC_MCU_UNIXTIME_P);   // Unix time
    RunlogWrite(FGC_RL_MSTIME, (void *) &UTC_MCU_MS_P);   // Millisecond time
    RunlogWrite(FGC_RL_MP_START, &dev.main_prog_version);

    // Initial read of FIP connector address

    dev.namedb_crc   = CODES_NAMEDB_INFO_CRC_FP;// Read NameDB CRC from flash

    if (fbs.net_type == NETWORK_TYPE_FIP)
    {
        fip.stadr = FIP_STADR_P;        // Get FIP address
    }

    // Initialise the test property data

    for (ii = 0 ; ii < DEV_TEST_DATA_SZ; ii++)
    {
        dev.test_data[ 1 + ii * 2 ] = ii;             // Big endian !
    }
}



void InitOS(void)
{
    // Create tasks

    // if this grows the size of shared_mem->stack_usage[10] need to grow accordingly
    OSTskInit(MCU_NB_OF_TASKS);         // Initialise task control blocks

    OSTskCreate(MstTsk,        NULL,         tsk_stk.mst,       STK_SIZE_MST);   	//  0. MstTsk - Millisecond task
    OSTskCreate(FbsTsk,        NULL,         tsk_stk.fbs,       STK_SIZE_FBS);   	//  1. FbsTsk - Fieldbus task
    OSTskCreate(StaTsk,        NULL,         tsk_stk.sta,       STK_SIZE_STA);   	//  2. StaTsk - State machine task
    OSTskCreate(RegFgc3Tsk,    (void *)&rcm, tsk_stk.regfgc3,   STK_SIZE_REGFGC3);//  3. RegFgc3Tsk - RegFgc3 task
    // OSTskCreate(RegFgc3Tsk,    NULL,         tsk_stk.regfgc3,   STK_SIZE_REGFGC3);//  3. RegFgc3Tsk - RegFgc3 task
    OSTskCreate(PubTsk,        NULL,         tsk_stk.pub,       STK_SIZE_PUB);   	//  4. PubTsk - Publication task
    OSTskCreate(CmdTsk,        (void *)&fcm, tsk_stk.fcm,       STK_SIZE_FCM);  	//  5. FcmTsk - Fieldbus commands task
    OSTskCreate(CmdTsk,        (void *)&tcm, tsk_stk.tcm,       STK_SIZE_TCM);  	//  6. TcmTsk - Terminal commands task
    OSTskCreate(DlsTsk,        NULL,         tsk_stk.dls,       STK_SIZE_DLS);   	//  7. DlsTsk - Dallas bus task
    OSTskCreate(RtdTsk,        NULL,         tsk_stk.rtd,       STK_SIZE_RTD);   	//  8. RtdTsk - Real-time display task
    OSTskCreate(TrmTsk,        NULL,         tsk_stk.trm,       STK_SIZE_TRM);   	//  9. TrmTsk - Terminal communication task
    OSTskCreate(BgpTsk,        NULL,         tsk_stk.bgp,       STK_SIZE_BGP);   	// 10. BgpTsk - Background processing task

    // Create Semaphores

    // Initialise semaphore control blocks - THIS MUST MATCH THE NUMBER OF SEMAPHORES CREATED BELOW
    OSSemInit(FGC_NUM_SEMAPHORES);

    fcm.sem                     = OSSemCreate(0);           // 0. FbsTsk -> FcmTsk (next cmd pkt waiting)
    tcm.sem                     = OSSemCreate(0);           // 1. TrmTsk -> TcmTsk (next cmd pkt waiting)
    pcm.sem                     = OSSemCreate(1);           // 2. Pub cmd struct reservation semaphore
    fbs.cmd.q_not_full          = OSSemCreate(0);           // 3. Fieldbus cmd.q flow control semaphore
    fbs.pub.q_not_full          = OSSemCreate(0);           // 4. Fieldbus pub.q flow control semaphore
    dev.set_lock                = OSSemCreate(1);           // 5. Set command lock semaphore
    pub.lock                    = OSSemCreate(0);           // 6. Sub structure lock semaphore
    pub.last_pkt_send           = OSSemCreate(0);           // 7. Last pub pkt now being sent
    regfgc3_pars_handler.sem_cache_mutex = OSSemCreate(1);  // 8. Mutex to access RegFGC3 params cache
    regfgc3_task_sem_go         = OSSemCreate(0);           // 9. Awake RegFGC3 task
    regfgc3_task_todo_sem       = OSSemCreate(1);           // 10. Mutex to access the RegFGC3 subtasks  


    uint8_t num_sem = regfgc3_task_todo_sem - fcm.sem + 1; 

    if ( FGC_NUM_SEMAPHORES != num_sem )
    {
        Crash(num_sem);
    }
    
    // Create Message queues

    OSMsgInit(FGC_NUM_MSG_QUEUES);                               // Initialise message queue control blocks

    terminal.msgq = OSMsgCreate(terminal.msgq_buf, TRM_MSGQ_SIZE);    // 0. IsrMst & FbsTsk -> TrmTsk (kbd chars)

    // Create Memory partition

    OSMemInit(FGC_NUM_MEM_PARTITIONS);                               // Initialise memory partition control blocks

    // 0. Mem buffers for terminal output characters
    terminal.mbuf = OSMemCreate(terminal.mem_buf, TRM_N_BUFS, TRM_BUF_SIZE);

    // Initialise command structure links before OS starts

    fcm.prop_buf = (struct prop_buf *) &dpcom.fcm_prop;     // Link fcm property buffer structure
    tcm.prop_buf = (struct prop_buf *) &dpcom.tcm_prop;     // Link scm property buffer structure
    // rcm.prop_buf = (struct prop_buf *) &dpcom.rcm_prop;     // Link scm property buffer structure

    fcm.pars_buf = (struct pars_buf *) &fcm_pars;       // Link fcm command pkt structure
    tcm.pars_buf = (struct pars_buf *) &tcm_pars;       // Link scm command pkt structure
}



void EnableMcuTimersAndInterrupts(void)
{
    Disable1msTickInterrupt();
    Enable1msTickInterruptFromPLD(); // we can take 1ms Tick from PLD now

    // The network interrupt is enabled later by FipInit() in MsTsk()
    // Todo comment does not match code. Obsolete?
    DisableNetworkInterrupt();
    EnableNetworkInterrupt();
}



void EnableMcuSerialInterface_Terminal(void)
{
    // ============== UART 0 ====================================================================================
    // UART0 - Terminal - connected to the FTDI serial/USB with the connector in the front panel

    // already done in boot

    // ============== UART 1 ====================================================================================
    // UART1 - routed to the PLD  (communication with SCOPE ?)
    // ============== UART 2 ====================================================================================
    // UART2 - routed to the PLD  (communication with POWER CONVERTER ?)
    // ============== UART 3 ====================================================================================
    // UART3 - routed to the PLD
    // ============== UART 4 ====================================================================================
    // UART4 - not used
    // in Boot mode
    // P0.5/RxD4 Input Used in boot mode to receive data via SCI4 (for host communications)
    // P0.4/TxD4 Output Used in boot mode to transmit data from SCI4 (for host communications)
    // ============== UART 5 ====================================================================================
    // UART5 - not used
    // ============== UART 6 ====================================================================================
    // UART6 - routed to the PLD - M16C62 development port (M16C62-UART1)
    //          when M16C62 is in NORMAL (not STAND_ALONE) its UART1 (M16C62 development uart) is directed to Rx610 UART6
}



void InitSystem(void)
{
    BOOLEAN             type_in_db; // Flag for SysDB queries
    struct fgc_dev_name dev_name;   // Used for device name initialisation
    enum InitTypeFrom   init_from;  // Type initialisation method

    InitDbStructures();

    InitDeviceName(&dev_name);

    // Extract type from device name and check if it can be found in SysDB
    // If not then get type from DEVICE.TYPE property and check again
    // If still couldn't find the system then set the type and the name to unknown

    init_from = INIT_TYPE_FROM_NAME;

    do
    {
        InitType(init_from, &dev_name);
        type_in_db = InitFindTypeInSysDb();
    }
    while ((!type_in_db) && (++init_from < INIT_TYPE_FROM_UNKNOWN_SYSTEM_RECORD));

    if (!type_in_db)
    {
        InitType(init_from, &dev_name);
        InitSetDeviceNameToUnknown(&dev_name);
    }

    InitSystemStructures(&dev_name);
}



void InitSpyMpx(struct cmd * c)
{
#if (FGC_CLASS_ID == 61)
    static INT32U init_spy_mpx[FGC_N_SPY_CHANS] = {  FGC_SPY_REF,  FGC_SPY_IMEAS,
                                                     FGC_SPY_VREF, FGC_SPY_VMEAS,
                                                     FGC_SPY_IA,   FGC_SPY_IB
                                                  };
#elif (FGC_CLASS_ID == 62)
    static INT32U init_spy_mpx[FGC_N_SPY_CHANS] = {  FGC_SPY_REF,        FGC_SPY_IMEAS,
                                                     FGC_SPY_VMEAS,      FGC_SPY_DIG_SUP_B0,
                                                     FGC_SPY_DIG_SUP_B1, FGC_SPY_DIG_SUP_B2
                                                  };
#endif

    PropSet(c, &PROP_SPY_MPX, init_spy_mpx, FGC_N_SPY_CHANS);
}



void InitPropertyTree(void)
{
    INT16U        idx;

    struct init_dynflag_timestamp_select * property_init_info = &init_dynflag_timestamp_select[0];

    while (property_init_info->prop)                            // Initialise the timestamp dynamic flag
    {
        Set(property_init_info->prop->dynflags, DF_TIMESTAMP_SELECT);
        property_init_info++;
    }

    PropMap(InitPropParentLinksSet, PROP_MAP_ALL);              // Link each property to its parent and propagate
    // the DF_TIMESTAMP_SELECT flag to children

    // The rest of the procedure to initialise the properties is too complex to be handled with a PropMap,
    // so one needs to manually recurse on all properties.

    for (idx = 0; idx < FGC_N_TLPS; idx++)
    {
        InitPropPPMFlagAndPubSize(&((struct prop *)&PROPS)[idx]);
    }
}



void InitPostSync(struct cmd * c)
{
#if (FGC_CLASS_ID == 61)
    const struct sym_lst * sym_lst;
    INT16U              user;
    INT16U              max_users;
    INT16U              func_type;
    INT16U              stc_func_type;

    // After synchronisation with the config DB, this is the SECOND COPY of the
    // REG.MODE_INIT property to the non-PPM reg mode. (The first copy happened
    // after NVS init.)
    // Do this only for cycling devices.

    dpcls.mcu.ref.func.reg_mode[0] = ref.reg_mode_init;

    if (STATE_OP != FGC_OP_UNCONFIGURED &&
        DEVICE_CYC == FGC_CTRL_ENABLED)
    {
        // If the device PPM is disabled, only re-arm user 0. Otherwise, re-arm all PPM users.

        if (dpcom.mcu.device_ppm == FGC_CTRL_ENABLED)
        {
            user = 1;
            max_users = dev.max_user;
        }
        else
        {
            user = 0;
            max_users = 0;
        }

        // For all active users attempt to re-arm reference functions

        for (; user <= max_users; ++user)
        {
            func_type = dpcls.mcu.ref.func.type[user];

            if (func_type != dpcls.dsp.ref.func.type[user])
            {
                // Search ref sym_lst to find sym_idx (stc_xxx) for this func_type
                // Default value if not found in symbolic list

                stc_func_type = 0;

                for (sym_lst = SYM_LST_REF; sym_lst->sym_idx ; sym_lst++)
                {
                    if (func_type == sym_lst->value)
                    {
                        // Drop the FGC_NOT_SETTABLE flag

                        stc_func_type = sym_lst->sym_idx & ~FGC_NOT_SETTABLE;
                        break;
                    }
                }

                RefArm(c, user, func_type, stc_func_type);
            }
        }
    }

#endif

    InitClassPostSync(c);

    regFgc3ParsRetrieveParams();
}
