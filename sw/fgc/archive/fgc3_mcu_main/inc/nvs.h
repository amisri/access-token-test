/*!
 * @file      nvs.h
 * @defgroup  FGC:MCU:FGC3
 * @brief     Non-Volatile Storage Management.
 *
 * This file contains the functions which support the non-volatile storage and recovery
 * of Property data to/from the NVRAM memory.
 *
 * NVRAM is accessible by WORD only - BYTE access will result in a bus error!
 *
 * Some non-volatile properties are part of the configuration, meaning they are stored
 * in the central property database and will be set by the FGC configuration manager.
 */

#ifndef FGC_NVS_H
#define FGC_NVS_H

#ifdef NVS_GLOBALS
#define NVS_VARS_EXT
#else
#define NVS_VARS_EXT extern
#endif

// Includes

#include <stdint.h>
#include <property.h>
#include <cmd.h>
#include <defconst.h>

// Constants

#define NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION  1399280666

// ToDo: use NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION as MVS_MAGIC instead of this constant one

/*! Magic number in NVS to say it is formatted. */
#define NVS_MAGIC               0x1566
/*! NVS property unset (this is per user for PPM prop). */
#define NVS_UNSET               ((prop_size_t)0xFFFFu)
/*! Used to store/recall values for all users. */
#define NVS_ALL_USERS           0xFF
/*! Invalid NVS index (p->nvs_idx). It means the property is not associated with a NVS record. */
#define INVALID_NVS_IDX         0xFF

// External structures, unions and enumerations

// Mapping of a NVS record in Non-volatile memory (NVSIDX_REC_32)
// Notice the differences between FGC2 and FGC3 platforms:
//  - On FGC2, the property address is 16 bit long (size of a near pointer) and total structure size is: 0x28 = 40 bytes
//  - On FGC3, the property address is 32 bit long (size of any pointer)    and total structure size is: 0x2C = 44 bytes (+2 padding bytes at the end)

/*! Structure of the property data in NVRAM (for one user, is PPM property) */
struct TDataSet
{
    uint16_t number_of_elements; /*!< If this is 0xFFFF means NVS_UNSET */
    uint32_t element[];          /*!< This must be word aligned for FGC2 */
};

/*! Structure of the NVS record */
struct nvs_record
{
    uint32_t prop;                     /*!< Non-volatile property structure address (cast to struct prop *) */
    uint32_t data;                     /*!< Non-volatile property data address */
    uint16_t bufsize;                  /*!< Non-volatile property data size in bytes, for one user (if PPM) */
    uint8_t  name[NVSIDX_REC_NAME_B];  /*!< Non-volatile property name */
};

/*! Mapped to DEBUG.NVS[4] property */
struct nvs_vars
{
    uint16_t              idx;                           /*!< Record index variable */
    uint16_t              n_props;                       /*!< Number of NVS properties in FRAM */
    uint16_t              n_unset_config_props;          /*!< Number of config properties unset */
    uint16_t              n_corrupted_indexes;           /*!< Number of corrupted index records */
    uint32_t              data_addr;                     /*!< Next free zone for data */
    uint16_t              sym_idxs[FGC_MAX_PROP_DEPTH];  /*!<Symbols for nested property */
};

// External variable definitions

NVS_VARS_EXT struct nvs_vars nvs;

// External function declarations

/*!
 * This function is responsible for the recovery/formatting of non-volatile
 * properties using data from the NVS area of the NVRAM memory.
 */
void NvsInit(struct init_prop * property_init_info);

/*!
 * Recurses through all the properties clearing the DF_CFG_CHANGED flag.
 */
void NvsResetChanged(void);

/*!
 * This function will recover from NVRAM the data for the specified property
 * and range of users.
 *
 * This function requires a NVS record identified by p->nvs_idx already exists
 * for that property.
 *
 * @param c Command structure.
 * @param p Property to recover.
 * @param user Range of users to recover.
 */
void NvsRecallValuesForOneProperty(struct cmd  * c,
                                   struct prop * p,
                                   uint16_t      user);

/*!
 * This function will save in NVRAM the data of the specified property for a
 * given range of users.
 *
 * This function requires a NVS record identified by p->nvs_idx already exists
 * for that property. If not NULL, end_data_address pointer will be used to
 * return the address of the end of the NVRAM segment used to write the
 * property.
 *
 * The caller function is responsible for unlocking the NVRAM thanks to a
 * NVRAM_UNLOCK()/NVRAM_LOCK() section.
 *
 * @param c Command structure.
 * @param p Property to store.
 * @param user Range of users to store.
 *
 * @retval An error status. FGC_OK_NO_RSP if no error occurred.
 */
uint16_t NvsStoreValuesForOneProperty(struct cmd * c, struct prop * p, uint16_t user);

/*!
 * This function is called to save a property's data and number of elements in
 * the NVRAM.
 *
 * The data comes from the property block buffer for the command task (Fcm or Scm)
 * or directly from a property value. Note that the data size must be no greater
 * than a property block (= PROP_BLK_SIZE).
 *
 * Note, on the FGC2, FRAM writes must be by word and be word aligned!
 *
 * @param c Command structure.
 * @param p Property to store.
 * @param mux_idx
 * @param n_elements Number of elements to store.
 * @param last_blk_f True if this is the last block to store.
 * @param data Buffer with the block data.
 */
void nvsStoreBlockForOneProperty(struct cmd         * c,
                                 struct prop        * p,
                                 uint8_t            * data,
                                 uint16_t     const   sub_sel,
                                 uint16_t     const   cyc_sel,
                                 prop_size_t  const   n_elements,
                                 bool         const   last_blk_f);


void NvsPropDynflags(struct prop * p);


#endif
