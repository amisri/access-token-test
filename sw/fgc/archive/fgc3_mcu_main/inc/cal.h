/*---------------------------------------------------------------------------------------------------------*\
  File:         cal.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FGC_CAL_H
#define FGC_CAL_H

#ifdef CAL_GLOBALS
#define CAL_VARS_EXT
#else
#define CAL_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <property.h>   // for struct prop
#include <dpcls.h>      // for enum cal_action
#include <stddef.h>     // for NULL

//-----------------------------------------------------------------------------------------------------------

/*----- CAL constants -----*/

#define CAL_NO_AUTO_TIME        86400L          // No auto ADC calibration time (s)
#define CAL_IN_STATE_DELAY      600000L         // No auto ADC calibration before a minimum amount of time
//                             spent in the same state (ms)
#define CAL_SUM16_PERIOD        40              // ADC16 calibration period (x200 samples)
#define CAL_SUM22_PERIOD        50              // ADC22 calibration period (x200 samples)
#define CAL_DCCT_PERIOD         50              // DCCT  calibration period (x200 samples)
#define CAL_DAC_PERIOD          1               // Internal DAC calibration period (x200 samples)
#define CAL_VREF_LIMIT          0.2             // Max error in reference measurement to allow calibration.

#define REF_FUNC_TYPE           (INT16U)dpcls.dsp.ref.func_type         // Ref func now
#define REF_ARMED_FUNC_TYPE     (INT16U)dpcls.dsp.ref.func.type         // Armed funcs array for [user]
#define REF_STC_ARMED_FUNC_TYPE (INT16U)dpcls.dsp.ref.stc_func_type     // Ref func sym_idx now for RTD
#define NON_PPM_REG_MODE        (INT16U)dpcls.mcu.ref.func.reg_mode[0]  // Reg mode for user 0

#define CAL_INTERNAL_ADCS       0x0F                    // All internal ADCs
#define CAL_DAC1                0x01                    // Mask for DAC1 channel
#define CAL_DAC2                0x02                    // Mask for DAC2 channel
#define CAL_DACS                0x03                    // Mask for DAC1 & DAC2 channels

//-----------------------------------------------------------------------------------------------------------

struct cal_req
{
    enum cal_action  action;                    // To be copied to dpcls.mcu.cal.action (casted to INT32S)
    INT32U           chan_mask;                 // To be copied to dpcls.mcu.cal.chan_mask
    INT32U           ave_steps;                 // To be copied to dpcls.mcu.cal.ave_steps
    INT32S           idx;                       // To be copied to dpcls.mcu.cal.idx
};

struct cal_seq
{
    BOOLEAN(*func)(INT16U, INT16U);                     // Sequence function
    INT16U              par1;                           // Sequence parameter
    INT16U              par2;                           // Sequence parameter
};

/*----- Auto Calibration Variables -----*/

struct cal_vars
{
    struct prop    *    prop;                           // Calibration property
    INT16U              counter;                        // Calibration delay counter
    INT32U              inhibit_cal;                    // Seconds down counter for inhibiting auto-cal
    INT32U              last_cal_time_unix;             // Last calibration time for internal ADC (Unix time)
    FP32                dac[3];                         // Workspace for calibrating DAC
};

//-----------------------------------------------------------------------------------------------------------

// Tamper with the usual header layout: need to put the function declarations before the definition of
// calibration sequence arrays

void            CalInitSequence(const struct cal_seq * cal_sequence, INT32U chan_mask, INT32S signal);
void            CalRunSequence(void);
BOOLEAN         CalInternalAdcs(INT16U, INT16U);
BOOLEAN         CalInternalAdcsMask(INT16U, INT16U);
BOOLEAN         CalExternalAdcs(INT16U, INT16U);
BOOLEAN         CalDccts(INT16U, INT16U);
BOOLEAN         CalDacs(INT16U, INT16U);
BOOLEAN         CalWaitDsp(INT16U, INT16U);
BOOLEAN         CalNvsSaveInternalAdcs(INT16U, INT16U);
BOOLEAN         CalNvsSaveExternalAdcs(INT16U, INT16U);
BOOLEAN         CalNvsSaveDccts(INT16U, INT16U);
BOOLEAN         CalNvsSaveDacs(INT16U, INT16U);
BOOLEAN         CalLastCalTime(INT16U, INT16U);
BOOLEAN         CalEnd(INT16U, INT16U);

// Hardcoded calibration sequences are defined below.
// They are all defined as arrays of struct cal_seq, in which the last sequence structure has a NULL function
// pointer, to mark the end of the sequence

// Sequence to calibrate the internal ADCS (in parallel)

CAL_VARS_EXT const struct cal_seq cal_seq_int_adcs[]
#ifdef CAL_GLOBALS
        =
{
    { CalInternalAdcs,          CAL_SUM16_PERIOD,   0               },      // Calibrate internal ADCs selected
    { CalWaitDsp,               0,                  0               },      // by dpcls.mcu.cal.chan_mask
    { CalNvsSaveInternalAdcs,   0,                  0               },      // Save ADCs errors in non-volatile memory
    { CalEnd,                   TRUE,               0               },
    { NULL,                     0,                  0               },
}
#endif
;

// Sequence to calibrate all internal ADCS one after the other.

CAL_VARS_EXT const struct cal_seq cal_seq_int_adcs_one_by_one[]
#ifdef CAL_GLOBALS
        =
{
    { CalInternalAdcsMask,      CAL_SUM16_PERIOD,   0x01            },      // Calibrate ADC1
    { CalWaitDsp,               0,                  0               },
    { CalNvsSaveInternalAdcs,   0,                  0               },      // Save ADCs errors in non-volatile memory

    { CalInternalAdcsMask,      CAL_SUM16_PERIOD,   0x02            },      // Calibrate ADC2
    { CalWaitDsp,               0,                  0               },
    { CalNvsSaveInternalAdcs,   0,                  0               },      // Save ADCs errors in non-volatile memory

    { CalInternalAdcsMask,      CAL_SUM16_PERIOD,   0x04            },      // Calibrate ADC3
    { CalWaitDsp,               0,                  0               },
    { CalNvsSaveInternalAdcs,   0,                  0               },      // Save ADCs errors in non-volatile memory

    { CalInternalAdcsMask,      CAL_SUM16_PERIOD,   0x08            },      // Calibrate ADC4
    { CalWaitDsp,               0,                  0               },
    { CalNvsSaveInternalAdcs,   0,                  0               },      // Save ADCs errors in non-volatile memory

    { CalLastCalTime,           0,                  0               },      // Force the update of ADC.ADC16.LAST_CAL_TIME

    { CalEnd,                   TRUE,               0               },
    { NULL,                     0,                  0               },
}
#endif
;

// Sequence to calibrate the external ADCS

CAL_VARS_EXT const struct cal_seq cal_seq_ext_adcs[]
#ifdef CAL_GLOBALS
        =
{
    { CalExternalAdcs,          CAL_SUM22_PERIOD,   0               },      // Calibrate external ADCs selected
    { CalWaitDsp,               0,                  0               },      // by dpcls.mcu.cal.chan_mask
    { CalNvsSaveExternalAdcs,   0,                  0               },      // Save ADCs errors in non-volatile memory
    { CalEnd,                   TRUE,               0               },
    { NULL,                     0,                  0               },
}
#endif
;

// Sequence to calibrate the DCCTs

CAL_VARS_EXT const struct cal_seq cal_seq_dccts[]
#ifdef CAL_GLOBALS
        =
{
    { CalDccts,                 CAL_DCCT_PERIOD,    0               },      // Calibrate DCCTs selected
    { CalWaitDsp,               0,                  0               },      // by dpcls.mcu.cal.chan_mask
    { CalNvsSaveDccts,          0,                  0               },      // Save ADCs errors in non-volatile memory
    { CalEnd,                   TRUE,               0               },
    { NULL,                     0,                  0               },
}
#endif
;

// Sequence to calibrate the DACs

CAL_VARS_EXT const struct cal_seq cal_seq_dacs[]
#ifdef CAL_GLOBALS
        =
{
    { CalDacs,                  CAL_DAC_PERIOD,     0               },      // Calibrate DACs selected
    { CalWaitDsp,               0,                  0               },      // by dpcls.mcu.cal.chan_mask
    { CalNvsSaveDacs,           0,                  0               },      // Save ADCs errors in non-volatile memory
    { CalEnd,                   FALSE,              0               },
    { NULL,                     0,                  0               },
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

// used by defprops.h
CAL_VARS_EXT struct cal_vars        cal;                    // Auto calibration structure

//-----------------------------------------------------------------------------------------------------------

#endif  // CAL_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: CAL.h
\*---------------------------------------------------------------------------------------------------------*/
