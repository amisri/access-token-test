/*!
 *  @file     log_cycle.h
 *  @defgroup FGC3:MCU
 *  @brief    Functionality for the log for cycling-related information.
 */

#ifndef FGC_LOG_CYCLE_H
#define FGC_LOG_CYCLE_H

#ifdef LOG_CYCLE_GLOBALS
#define LOG_CYCLE_VARS_EXT
#else
#define LOG_CYCLE_VARS_EXT extern
#endif


// Includes

#include <stdio.h>
#include <stdbool.h>
#include <cmd.h>
#include <defconst.h>
#include <mcu_dsp_common.h>

// External structures, unions and enumerations

/*!
 * Types of events handled by the cycle log.
 */
typedef enum log_cycle_value_type
{
    LOG_VALUE_TYPE_INT32S,
    LOG_VALUE_TYPE_INT32U,
    LOG_VALUE_TYPE_HEX,
    LOG_VALUE_TYPE_FLOAT,
    LOG_VALUE_TYPE_CHAR,
    LOG_VALUE_TYPE_STRING
} log_cycle_value_type_t;

/*!
 * Actions associated to log types.
 */
typedef enum log_cycle_action
{
    LOG_ACTION_CHANGED,
    LOG_ACTION_SET,
    LOG_ACTION_CLEAR,
    LOG_ACTION_RISE,
    LOG_ACTION_FALL,
    LOG_ACTION_RCV,
    LOG_ACTION_NOW,
} log_cycle_action_t;

/*!
 * Event log debug level (ENABLE or DISABLE)
 */
typedef struct log_cycle_level
{
    uint8_t  log_all;   //!< If True, all event log info is logged.
} log_cycle_level_t;

// External variable definitions

LOG_CYCLE_VARS_EXT log_cycle_level_t log_cyc_level;

// External function declarations

/*!
 * This function checks to see if the contents of the event log are corrupted.
 *
 * If so, it clears the log. To pass the check, the saved event log index must
 * be in range (0-699) and every record's time stamp must be zero, or within one
 * month of the restart time.
 */
void   LogCycleInit(void);

/*!
 * Inserts a new entry in the cycle event log. The time-stamp is in microseconds.
 *
 * @param name Log name.
 * @param type Log type
 * @param action Log action
 */
void LogCycleInsert(char const * const name,
                    log_cycle_value_type_t type,
                    void const * const value,
                    log_cycle_action_t action);

/*!
 * Inserts a new entry in the cycle event log. The time-stamp is in microseconds.
 *
 * @param name Log name.
 * @param timestamp Log time-stamp
 * @param type Log type
 * @param action Log action
 */
void LogCycleInsertUs(char const * const name,
                      struct abs_time_us const * const timestamp,
                      log_cycle_value_type_t type,
                      void const * const value,
                      log_cycle_action_t action);

/*!
 * Inserts a new entry in the cycle event log. The time-stamp is in milliseconds.
 *
 * @param name Log name.
 * @param timestamp Log times-tamp
 * @param type Log type
 * @param action Log action
 */
void LogCycleInsertMs(char const * const name,
                      struct abs_time_ms const * const timestamp,
                      log_cycle_value_type_t type,
                      void const * const value,
                      log_cycle_action_t action);

/*!
 * Provides a shared buffer that can be used to form a formatted string
 * for the contents of the value field.
 *
 * This is NOT a thread-safe mechanism so care must be taken.
 *
 * @retval Buffer
 */
char * const LogCycleGetValBuf(void);

/*!
 * Provides a shared buffer that can be used to form a formatted string
 * for the contents of the property field.
 *
 * This is NOT a thread-safe mechanism so care must be taken.
 *
 * @retval Buffer
 */
char * const LogCycleGetPropBuf(void);

/*!
 * Returns the log index
 */
INT16U LogCycleGetIdx(void);

/*!
 * Returns True if logging all cycling events is enabled
 */
static inline bool LogCycleAllEnabled(void);

// External function definitions

static inline bool LogCycleAllEnabled(void)
{
    return (log_cyc_level.log_all == FGC_CTRL_ENABLED);
}

#endif

// EOF
