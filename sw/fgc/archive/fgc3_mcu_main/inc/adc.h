/*!
 *  @file      adc.h
 *  @defgroup  FGC3:MCU
 *  @brief     ADC functions
 */

#ifndef FGC_ADC_H
#define FGC_ADC_H

#ifdef ADC_GLOBALS
#define ADC_VARS_EXT
#else
#define ADC_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <defconst.h>

// Constants

#define SD_FLTR_LEN_W           0x4000      // 0x2000 samples max per filter function
#define FIR_MAX_LENGTH          16000       // ToDo share with boot
#define FIR_ORDER               4           // ToDo share with boot

// External structures, unions and enumerations

typedef struct adc                          // Structure used for driving the SD-350/351 analog cards
{
    BOOLEAN in_reset_f;                     // ADC filters in reset
    BOOLEAN trigger_reset_f;                // Reset the ADC filters after next regulation period
    BOOLEAN pending_reset_req_f;            // Flag for pending reset request (checked every 200ms)
    INT16U  reset_counter_ms;               // 1kHz counter started when resetting the ADC filters
    INT16U  filter_idx_a;                   // Last filter index set for channel A (range: 0-4)
    INT16U  filter_idx_b;                   // Last filter index set for channel B (range: 0-4)
    INT32U  last_reset_unix_time;           // Property ADC.FILTER.LAST_RESET
    INT16U  sd_shift[FGC_N_SD_FLTRS];       // ADC SD filter shifts
    INT16U  sd_halftaps[FGC_N_SD_FLTRS];    // ADC SD filter half taps
    INT16U  internal_adc_mpx[FGC_N_ADCS];   // Property ADC.ADC16.MPX
    INT16U  fir_err;                        // Status returned by the last FIR update)
} adc_t;

// External variable definitions

ADC_VARS_EXT adc_t adc;

// fir factors ToDo share this with boot
ADC_VARS_EXT const FP32 fir_factor[FIR_ORDER]
#ifdef ADC_GLOBALS
    = { 1.000,
        0.736,
        0.206,
        0.058
      }
#endif
      ;

// Link between bitrate register value and bitrate value in mHz
ADC_VARS_EXT const FP32 adc_bitrate_mhz_map[16]
#ifdef ADC_GLOBALS
    = { 8.000,
        0.675,
        0.100,
        0.500,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
      }
#endif
      ;

// External function declarations

/*!
 * ADC initialisation
 */
void   AdcInit(void);

/*!
 * Treat any request to reset the ADC filters. The argument user_request_f is
 * set in case the reset is requested by the end user with one of the following
 * commands: S ADC RESET, S ADC.FILTER RESET or S PC OF.
 */
void   AdcFiltersResetRequest(BOOLEAN user_request_f);

/*!
 * Trigger a reprogramming of the FPGA used for filters on SD-350 and SD-351
 * cards. On other analog cards the reset trigger is cleared which means the
 * reset request is simply ignored.
 */
void   AdcFiltersReset(void);

/*!
 * Check that the filter reset triggered by AdcFilterReset has finished.
 */
void   AdcFiltersResetCheck(void);

/*!
 * Read SD filter functions meta data.
 */
void   AdcFiltersReadFunctions(void);

/*!
 * Set the ADC filter index for channels A/B. Index range goes from 0 to 4
 * for SD-350/SD-351 cards: see comments for function AdcFiltersReadFunctions.
 */
void   AdcFiltersSetIndex(INT16U filter_idx_a, INT16U filter_idx_b);

/*!
 * Copy the current state of the ADC Multiplexing from shared memory to the
 * property variable.
 */
void   AdcCpyMpx(void);

/*!
 * This function writes and checks the FIR filter.
 *
 * This is needed to use the Sigma-Delta ADC in the analog interface board.
 * It must be updated every time dsp tick or adc bitrate changes.
 */
INT16U AdcUpdateFIR(void);

#endif

// EOF
