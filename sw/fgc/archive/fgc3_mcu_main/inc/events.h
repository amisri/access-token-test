/*!
 *  @file      events.h
 *  @brief     Functionality to process timing events received from the gateway or 
 *             super-cycle simulation.
 */

#ifndef FGC_EVENTS_H
#define FGC_EVENTS_H

#ifdef EVENTS_GLOBALS
#define EVENTS_VARS_EXT
#else
#define EVENTS_VARS_EXT extern
#endif


// Includes

#include <fgc_event.h>



// External structures, unions and enumerations



// Platform/class specific function declarations

/*
 * Class specific handling of the event FGC_EVT_START
 */
void eventsClassProcessStart(struct fgc_event const * event);

/*!
 * Class specific handling of the  FGC_EVT_ABORT
 */
void eventsClassProcessAbort(struct fgc_event const * event);

/*!
 * Class specific handling of the  FGC_EVT_INJECTION
 */
void eventsClassProcessInjection(struct fgc_event const * event);

/*!
 * Class specific handling of the  FGC_EVT_EXTRACTION
 */
void eventsClassProcessExtraction(struct fgc_event const * event);

/*
 * Class specific handling of the event FGC_EVT_CYCLE_WARNING
 */
void eventsClassProcessCycleStart(struct fgc_event const * event);



// External function declarations

/*!
 * Processes timing events.
 */
void eventsProcess(void);


#endif  // FGC_EVENTS_H end of header encapsulation

// EOF
