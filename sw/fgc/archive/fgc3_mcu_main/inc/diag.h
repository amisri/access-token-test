/*---------------------------------------------------------------------------------------------------------*\
 File:          diag.h

 Purpose:       FGC MCU Software - QSPI related functions

 Author:        Quentin.King@cern.ch

 Notes:         The DIPS board has an embedded DIM that is the first on both branch A and branch B,
                so it is scanned at 100Hz.
                The analogue channels are different for the two scans (there are 8 channels in all)
                and are used to measure the voltage source output voltage (Vmeas) twice (x1 and x10)
                and the +5V and +/-15V PSU voltages for the FGC and the FGC's analogue interface.
                These are checked by the DSP against threshold values.
                If the diagnostic buses are reset, these channels get out of sync for 2 20ms cycles,
                so the DSP must suppress PSU monitoring for 2 cycles.
                This is signalled by the MCU using dpcom.mcu.diag.reset.
                This should be set to the number of cycles that need to be blocked (2).
                It is decremented by the DSP every 20ms cycle.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DIAG_H      // header encapsulation
#define DIAG_H

#ifdef DIAG_GLOBALS
#define DIAG_VARS_EXT
#else
#define DIAG_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <defconst.h>           // for FGC_MAX_DIM_BUS_ADDR, FGC_DIAG_RTD_LIST_LEN, FGC_DIAG_LIST_LEN, FGC_MAX_LOG_MENUS, FGC_MAX_DIMS, FGC_N_DIM_DIG_BANKS
#include <fgc_consts_gen.h>     // for FGC_DIAG_N_SYNC_BYTES
#include <fgc_stat.h>           // for struct fgc_diag
#include <property.h>           // for struct prop and typedef prop_size_t
#include <qspi_bus.h>           // for QSPI_BRANCHES

//-----------------------------------------------------------------------------------------------------------

#define DIAG_EXPEC_LATCHED_FAULT        10              // Threshold to set a DIM related latched fault.
#define DIAG_MAX_ANALOG                 128             // Max number of analogue channels
#define DIAG_MAX_DIGITAL                64              // Max number of digital channels
#define DIAG_MAX_SOFTWARE               64              // Max number of software channels
#define DIAG_UL_DIG_DATA_0              0x080           // Bank 0 unlatched digital data channels
#define DIAG_UL_DIG_DATA_1              0x0A0           // Bank 1 unlatched digital data channels
#define DIAG_TASK_TIMES                 0x0C0           // Channels for task timing
#define DIAG_ISR_TIMES                  0x0D0           // Channels for ISR timing
#define DIAG_MST_TIMES                  0x0E0           // Channels for MstTsk timing
#define DIAG_LT_DIG_DATA_0              0x120           // Bank 0 latched digital data channels
#define DIAG_LT_DIG_DATA_1              0x140           // Bank 1 latched digital data channels
#define DIAG_ANALOG_CAL                 1.220703        // Millivolts/raw for analogue channels
#define DIAG_TRIGGER                    0x8000          // Trigger scan bit in QSM_SPCR1
#define DIAG_DCCT_STAT_CHAN             0x80            // DCCT status channel
#define DIAG_PSU_STAT_CHAN              0xA0            // PSU status channel
#define DIAG_UNLATCHED_TRIGS            0xDC            // Unlatched trigger channels
#define DIAG_LATCHED_TRIGS              0xDE            // Latched trigger channels
#define DIAG_TERM_LEN                   sizeof(struct diag_term)

//-----------------------------------------------------------------------------------------------------------

struct TAccessToDimInfo                                    // DIM link structures (logical_dim order)
{
    //struct prop          *          dim_prop;       // Property for this DIM
    INT8U                           logical_dim;    // DIM index (0-19)
    INT8U                           flat_qspi_board_number; // 0..31
};

struct diag_term                                        // Double diag buffer for terminal diag mode
{
    INT8U               sync[FGC_DIAG_N_SYNC_BYTES];    // Sync bytes for terminal diag client
    struct fgc_diag     data;                           // Diag data structure
};


// for software register array index
#define TASK_RUN_TIME   0
#define ISR_RUN_TIME    1

#define BRANCH_A_UNLATCHED_TRIGS     12 // branch A dig0 registers b15 collection
#define BRANCH_B_UNLATCHED_TRIGS     13 // branch B dig0 registers b15 collection
#define BRANCH_A_LATCHED_TRIGS       14 // branch A triggerCounter registers b15 collection
#define BRANCH_B_LATCHED_TRIGS       15 // branch B triggerCounter registers b15 collection

// this can't be modified without adjusting at labels DIAG_FLAT
struct TDimDataFromHwAndMore_1
{
    INT16U  analog_0[QSPI_BRANCHES][16];        // Ana0  DIM register ID:4  x16 DIM boards per branch A or B
    INT16U  analog_1[QSPI_BRANCHES][16];        // Ana1  DIM register ID:5  x16 DIM boards per branch A or B
    INT16U  analog_2[QSPI_BRANCHES][16];        // Ana2  DIM register ID:6  x16 DIM boards per branch A or B
    INT16U  analog_3[QSPI_BRANCHES][16];        // Ana3  DIM register ID:7  x16 DIM boards per branch A or B
    INT16U  digital_0[QSPI_BRANCHES][16];       // Dig0  DIM register ID:0  x16 DIM boards per branch A or B
    INT16U  digital_1[QSPI_BRANCHES][16];       // Dig1  DIM register ID:1  x16 DIM boards per branch A or B
    INT16U  software[2][16];                    // [0=task_run_time][0..9 tasks] , [10..15]?
    // [1=isr_run_time] [0..9 tasks] , [10,11]? [12=BRANCH_A_UNLATCHED_TRIGS][13=BRANCH_B_UNLATCHED_TRIGS][14=BRANCH_A_LATCHED_TRIGS][15=BRANCH_B_LATCHED_TRIGS]
    INT16U  ms_duration[32];    // the MST duration for each ms, in microseconds. [20..31] not used.
    INT16U  trigCounter[QSPI_BRANCHES][16];     // TrigCounter DIM register ID:2  x16 DIM boards per branch A or B
    INT16U  latched_0[QSPI_BRANCHES][16];       // Latched digital
    INT16U  latched_1[QSPI_BRANCHES][16];       // Latched digital
};

struct TDimDataFromHwAndMore_2
{
    INT16U              analog_0[FGC_MAX_DIM_BUS_ADDR];
    INT16U              analog_1[FGC_MAX_DIM_BUS_ADDR];
    INT16U              analog_2[FGC_MAX_DIM_BUS_ADDR];
    INT16U              analog_3[FGC_MAX_DIM_BUS_ADDR];
    INT16U              digital_0[FGC_MAX_DIM_BUS_ADDR];
    INT16U              digital_1[FGC_MAX_DIM_BUS_ADDR];
    INT16U              software[2][16];
    INT16U              ms_duration[32];
    INT16U              trigCounter[FGC_MAX_DIM_BUS_ADDR];
    INT16U              latched_0[FGC_MAX_DIM_BUS_ADDR];
    INT16U              latched_1[FGC_MAX_DIM_BUS_ADDR];
};

struct TDimDataFromHwAndMore_3
{
    INT16U              analog[4][FGC_MAX_DIM_BUS_ADDR];    // Ana0, Ana1, Ana2, Ana3
    INT16U              digital[FGC_N_DIM_DIG_BANKS][FGC_MAX_DIM_BUS_ADDR];   // Dig0, Dig1
    INT16U              software[2][16];
    INT16U              ms_duration[32];
    INT16U              trigCounter[FGC_MAX_DIM_BUS_ADDR];
    INT16U              latched[FGC_N_DIM_DIG_BANKS][FGC_MAX_DIM_BUS_ADDR];   // latched 0, 1
};

struct TDimDataFromHwAndMore_4
{
    INT16U              analog[DIAG_MAX_ANALOG];          // [0x000-0x07F]  128 Analogue registers
    INT16U              digital[DIAG_MAX_DIGITAL];        // [0x080-0x0BF]   64 Unlatched digital registers
    INT16U              software[DIAG_MAX_SOFTWARE];      // [0x0C0-0x0FF]   64 Software (non-DIM) registers
    INT16U              trigCounter[FGC_MAX_DIM_BUS_ADDR];// [0x100-0x11F]   32 DIM trigger timers
    INT16U              latched[DIAG_MAX_DIGITAL];        // [0x120-0x15F]   64 Latched digital registers
};

union TDimDataFromHwAndMore
{
    INT16U w[DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL + DIAG_MAX_SOFTWARE + FGC_MAX_DIM_BUS_ADDR + DIAG_MAX_DIGITAL];
    struct TDimDataFromHwAndMore_1      r;      // registers [A,B][0..15]
    struct TDimDataFromHwAndMore_2      f;      // flat registers [0..31]
    struct TDimDataFromHwAndMore_3      a;      // for analogue process flat registers [0..31]
    struct TDimDataFromHwAndMore_4      c;      // groups
} /* __attribute__ ((__transparent_union__)) */ ;


// ToDo: decouple branches A and B to handle the branches independently
// and not to stop branch A if B is faulty or viceversa

struct diag_vars
{
    union TDimDataFromHwAndMore data;
    INT16U              req_QSPI_reset;                 // reset request down counter
    INT32U              sync_resets;                    // DIAG.SYNC_RESETS
    INT16U              fault_resets_counter;           // Reset counter to detect DIAG_FLT
    INT16U              qspi_branch;                    // branch being accessed 0=A, 1=B
    INT16U       *      copy_to;                        // Pointer into destination buffer for data
    INT16U              triggers_from_ms[20];           // Trigger bits collected in each millisecond
    struct diag_term    term_buf[2];                    // Double diag buffer for terminal diag mode
    INT16U              term_byte_idx;                   // Data byte index (0 to sizeof(struct diag_buf))
    INT16U              term_buf_idx;                    // Double buffer index (0 or 1)
    INT8U       *       term_buf_p;                      // Pointer to next byte to send
    INT8U       *       term_buf_chan;                   // Pointer to next chan address
    INT16U       *      term_buf_data;                   // Pointer to next data address
    INT16U              term_buf_counter;                // Counter of number of samples entered in the buffer
    prop_size_t list_rtd_len;                   // RTD diagnostic channel list length, and size of prop DIAG.RTD
    prop_size_t list_len[FGC_DIAG_N_LISTS];     // Diag list property lengths (DIAG.LIST0/LIST1/LIST2/LIST3)
    INT8U               list_offset[FGC_DIAG_N_LISTS];  // Diag list channel offsets
    INT8U               list_rtd[FGC_DIAG_RTD_LIST_LEN];        // RTD diagnostic channel list
    INT8U               list[FGC_DIAG_N_LISTS][FGC_DIAG_LIST_LEN];      // Diagnostic channel lists
    INT16U              n_dims;                         // Number DIMs
    const char  *       menu_names[FGC_MAX_LOG_MENUS];  // LOG.MENU.NAMES[] pointers to name strings
    const char  *       menu_props[FGC_MAX_LOG_MENUS];  // LOG.MENU.PROPS[] pointers to name strings
    struct prop         dim_props[FGC_MAX_DIMS];        // prop_idx order for [logical dim]
    struct TAccessToDimInfo    logical_dim_info[FGC_MAX_DIMS];      // This array may have holes
};

typedef char log_dim_name_t[FGC_DIMDB_MAX_DIM_NAME_LEN + 1]; 

//-----------------------------------------------------------------------------------------------------------

void    DiagDimHashInit(void);
void    DiagDimDbInit(void);
void    DiagSetPropNumEls(INT16U n_dims);
void    DiagSetOffsetGain(struct prop * p, INT16U prop_idx);
INT32S  DiagFindAnaChan(const char * chan_name);
BOOLEAN DiagFindDigChan(INT16U logical_dim, const char * chan_name, INT16U * data_idx, INT16U * diag_mask);
void    DiagSciStart(void);
void    DiagTermNext(void);

void    DiagInitClass(void);
void    DiagCheckConverter(void);
void    DiagSD360(void);

//-----------------------------------------------------------------------------------------------------------

DIAG_VARS_EXT struct diag_vars  diag;                   // Diag variables structure

//-----------------------------------------------------------------------------------------------------------

#endif  // DIAG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: diag.h
\*---------------------------------------------------------------------------------------------------------*/
