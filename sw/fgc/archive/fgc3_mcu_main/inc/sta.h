/*!
 *  @file      sta.h
 *  @defgroup  FGC3:MCU
 *  @brief     Power converter state functionality
 */

#ifndef PC_STATE_H
#define PC_STATE_H

#ifdef PC_STATE_GLOBALS
#define PC_STATE_VARS_EXT
#else
#define PC_STATE_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <definfo.h>
#include <mcu_dsp_common.h>
#include <defconst.h>

typedef enum 
{
    POL_SWITCH_FAULT_NONE    = 0x00,
    POL_SWITCH_FAULT_STATE   = 0x01,
    POL_SWITCH_FAULT_INPUT   = 0x02,
    POL_SWITCH_FAULT_TIMEOUT = 0x04
} pol_switch_fault_t;

// External structures, unions and enumerations

typedef struct pc_state_vars
{
    struct abs_time_us  timestamp_us;                   // Iteration timestamp (for logging)
    struct abs_time_ms  timestamp_ms;                   // Iteration timestamp (with millisecond precision)
    BOOLEAN             sec_f;                          // Start of new second flag
    BOOLEAN             tick_2hz_f;                     // Flag set by the MST task at 2Hz
    INT16U              cal_active;                     // CAl.ACTIVE property
    INT16U              mode_op;                        // Operational mode
    INT16U              config_mode;                    // Configuration mode
    INT16U              config_state;                   // Configuration state
    INT16U              watchdog;                       // Watchdog value zero for MstTsk
    INT32U              time_ms;                        // Time since entering the state (ms)
    INT16U              faults;                         // Active faults mask (unlatched)
    pol_switch_fault_t  pol_switch_fault;           // Possible polarity switch faults
    INT16U              num_faults[16];                 // Number of faults
    BOOLEAN cyc_start_new_cycle_f;                      // Flag used to warn the PC_STATE task about the starting of a new cycle
    INT16U              copy_log_capture_state;         // Copy of the last value of LOG.CAPTURE.STATE
    INT16S              copy_log_capture_user;          // Copy of the last value of LOG.CAPTURE.USER
    struct abs_time_us  copy_log_capture_cyc_chk_time;  // Copy of the last value of LOG.CAPTURE.CYC_CHK_TIME
    INT16U cal_type;                       // Calibration type (CAL_REQ_ADC16, CAL_REQ_ADC16, CAL_REQ_DAC)
    INT16U              cal_chan;                       // Calibration channel (0=A, 1=B)
    INT16U              cal_seq_idx;                    // Calibration sequence index
    INT16U              cal_counter;                    // Calibration down counter
    INT16U              sync_db_cal_delay_s;            // SYNC_DB_CAL delay in seconds
    INT16U              dcct_flts;                      // DCCT faults latch
    INT16U              inputs;                         // Direct digital inputs
    INT16U              ip_direct;                      // Raw digital inputs
    INT16U              cmd;                            // Command request (ON/OFF/RESET/POLPOS/POLNEG)
    INT16U              cmd_req;                        // Requested command mask (read back from interface)
    INT16U              mode_pc;                        // MODE.PC property
    INT16U              mode_pc_simplified;             // MODE.PC_SIMPLIFIED property
    INT16U              state_pc_simplified;            // STATE.PC_SIMPLIFIED property
    INT16U              state_pc_on;                    // STATE.PC_ON property
    BOOLEAN force_slow_abort_f;     // Condition that makes the transition to SLOW_ABORT effective.
    BOOLEAN force_to_standby_f;     // Condition that makes the transition to TO_STANDBY effective.
    INT16U crash_code;              // SW crash codes (Crash(0xBADx) inherited from FGC2) will be redirected to a McuPanic on FGC3

    struct mcu_panic
    {
        INT32S   code;                                  // Panic ID. See McuPanic function (FGC3 only for now)
        INT32U * sp;                                    // Stack pointer (for exceptions)
        INT32U   pc;                                    // Program Counter before the exception.
    }                   mcu_panic;

} pc_state_vars_t;

// External variable definitions

PC_STATE_VARS_EXT pc_state_vars_t  sta;                    // State variables structure

// Inline function definition

/*!
 * Set config_state.
 * It will remain standalone if already in standalone mode.
 * Standalone must be set/reset by fieldbus only, according to fieldbus state
 */
static inline void StaSetConfigState(uint32_t state)
{
    if (sta.config_state != FGC_CFG_STATE_STANDALONE)
    {
        sta.config_state = state;
    }
}

/*!
 * Set config_state.
 * It will remain standalone if already in standalone mode.
 * Standalone must be set/reset by fieldbus only, according to fieldbus state
 */
static inline BOOLEAN StaIsStandalone(void)
{
    return (sta.config_state == FGC_CFG_STATE_STANDALONE);

}
#endif

// EOF
