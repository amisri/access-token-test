/*!
 *  @file      init.h
 *  @defgroup  FGC:MCU
 *  @brief     Initialisation functions that are not linked to other files.
 */

#ifndef INIT_H      // header encapsulation
#define INIT_H

#ifdef INIT_GLOBALS
#define INIT_VARS_EXT
#else
#define INIT_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <cmd.h>
#include <property.h>

// External structures, unions and enumerations

typedef struct init
{
    struct prop * parent_prop[FGC_MAX_PROP_DEPTH];
} init_t;

// External variable definitions

INIT_VARS_EXT init_t init;

// Class specific function declarations

/*!
 * Class specific initialisation before interrupts are started or the NVS
 * properties have been set or the DSP is started.
 */
void InitClassPreInts(void);

/*!
 * Class specific initialisation after interrupts are started and the NVS
 * properties have been set.
 */
void InitClassPostInts(void);

/*!
 * Class specific initialisation after the FGC has been synchronised
 * with the DB.
 *
 * @param c Command received.
 */
void InitClassPostSync(struct cmd * c);

// External function declarations

/*!
 * This function initialises the operation of the MCU.
 */
void InitMCU(void);

/*!
 * This function initialises the operation of the RTOS (NanOs).
 */
void InitOS(void);

/*!
 * This function initialises the operation of the M68HC16
 * General Purpose Timer (GPT).
 */
void EnableMcuTimersAndInterrupts(void);

/*!
 * This function initialises the operation of the serial communications
 * interface.
 */
void EnableMcuSerialInterface_Terminal(void);

/*!
 * @brief Initialises the system related data structures based on the device name/type
 *
 * Depending on the mode the function gets the device name from NameDB (FIP-driven) or from the property (stand-alone mode).
 * After that it extracts the type from the name and queries SysDB for it. If it cannot find the type then it tries
 * the same with the type property. This second check is done to be consistent with FGC2 (RPH_ type is not in the database
 * - users will set the correct type, like RPHA, RPHB etc., manually). If the second query also fails then the type is set
 * to unknown. At the end system structures are initialised.
 */
void InitSystem(void);

/*!
 * This function initialises the Spy multiplexer channel property SPY.MPX both
 * on startup and on the command S SPY RESET.
 *
 * @param c Command received.
 */
void InitSpyMpx(struct cmd * c);

/*!
 * This function is called from TrmTsk() on startup to set the parent pointers
 * in the property structures. The parent links are needed for subscriptions.
 */
void InitPropertyTree(void);

/*!
 * Initialisation after the FGC has been synchronised with the DB.
 *
 * @param c Command received.
 */
void InitPostSync(struct cmd * c);

#endif

// EOF
