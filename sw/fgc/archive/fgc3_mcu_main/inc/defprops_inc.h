/*!
 *  @file      defprops_inc.h
 *  @defgroup  FGC:MCU
 *  @brief     Adds all the headers needed by defprops.h
 */

#ifndef DEFPROP_ALL_H      // header encapsulation
#define DEFPROP_ALL_H

#include <hash.h>
#include <property.h>
#include <nvs.h>
#include <fbs.h>
#include <fbs_class.h>
#include <fip.h>
#include <ethernet.h>
#include <ether_comm.h>
#include <inter_fgc.h>
#include <dls.h>
#include <memmap_mcu.h>
#include <rtd.h>
#include <state_manager.h>
#include <dpcls.h>
#include <sta.h>
#include <cal.h>
#include <master_clk_pll.h>
#include <diag.h>
#include <ref.h>
#include <events_sim.h>
#include <events.h>
#include <mst.h>
#include <get.h>
#include <core.h>
#include <crate.h>
#include <log.h>
#include <log_cycle.h>
#include <log_class.h>
#include <syscalls.h>
#include <version.h>
#include <regfgc3.h>
#include <regfgc3_pars.h>
#include <regfgc3_prog.h>
#include <regfgc3_prog_fsm.h>
#include <defprops_inc_class.h>
#include <adc.h>
#include <prop.h>
#include <task_trace.h>

#endif

// EOF
