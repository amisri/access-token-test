/*---------------------------------------------------------------------------------------------------------*\
  File:         debug.h

  Purpose:      FGC3

  History:

    8 feb 2011  doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DEBUG_H // header encapsulation
#define DEBUG_H

#if (!defined(__RX610__) && !defined(__M16C62__) && !defined(__HC16__)) \
    || defined(__RX610__) || defined(__M16C62__) || defined(__HC16__)
#endif

//-----------------------------------------------------------------------------------------------------------

// this avoid the test of stand alone, so we can use the M16C62 for debugging in stand alone as if
// it is in its normal state
// #define      DEBUG_M16C62_STANDALONE

// this do all the hardware initialisation like done by the boot
// to run the Main directly without passing throw the Boot
// #define      DEBUG_RUNNING_MAIN_WITHOUT_BOOT

// for Philippe Fraboulet at Medaustron
// they need that DIG_IP1_FASTABORT_MASK16/DIG_IPDIRECT_FAST_ABORT generates PWRFAILURE
// #define      MEDAUSTRON

//-----------------------------------------------------------------------------------------------------------

#endif  // DEBUG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: debug.h
\*---------------------------------------------------------------------------------------------------------*/
