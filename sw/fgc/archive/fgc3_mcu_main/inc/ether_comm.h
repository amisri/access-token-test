/*---------------------------------------------------------------------------------------------------------*\
  File:     ether_comm.h

  Purpose:  FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef ETHER_COMM_H
#define ETHER_COMM_H

#ifdef ETHER_COMM_GLOBALS
#define ETHER_COMM_VARS_EXT
#else
#define ETHER_COMM_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>           // For basic typedefs
#include <fgc_ether.h>          // For FGC_FIELDBUS_
#include <inter_fgc.h>
#include <histogram.h>          // For profiling functions


/* FGC Gateway Types */

typedef struct fgc_gateway
{
    char                    rterm[FGC_ETHER_MAX_RTERM_CHARS];              // Remote terminal data
    float                   rt_ref[64];                                     // RT data
} fgc_gateway_t;

/* Ethernet Types */
struct dma_setting
{
    uint32_t      source;         // Source address
    uint16_t      size_bytes;     // Size in bytes (payload)
    uint16_t      size_dwords;     // Size in dword to be written
    uint16_t      offset;         // offset in bytes

    uint32_t      source2;        // Reload source address
    uint16_t      size_bytes2;    // Size in bytes (payload)
    uint16_t      size_dwords2;    // Size in dword to be written
};

typedef struct frames_bad
{
    uint32_t            all;
    uint32_t            address;
    uint32_t            crc;
    uint32_t            dropped;
    uint32_t            ether_type;
    uint32_t            payload_type;
    uint32_t            size;
    uint32_t            time;
}  frames_bad_t;

typedef struct frames_rcvd
{
    uint32_t            all;
    uint32_t            payload_types[FGC_ETHER_PAYLOAD_NUM_TYPES];
    frames_bad_t        bad;
} frames_rcvd_t;

typedef struct frames_sent
{
    uint32_t            all;
    uint32_t            payload_types[FGC_ETHER_PAYLOAD_NUM_TYPES];
    uint32_t            fail;
} frames_sent_t;

typedef struct frame_count
{
    frames_rcvd_t   rcvd;
    frames_sent_t   sent;
} frame_count_t;

typedef struct ethernet_stats
{
    frame_count_t       frame_count;
    uint32_t            soft_reset;                     // ETH stats: # of soft reset performed
} ethernet_stats_t;

typedef struct ethernet
{
    uint16_t    current_rx_fifo_byte_length;    // track number of bytes read from rx fifo

    ethernet_stats_t        stats;          // ETH stats structure
    struct dma_setting      rsp_dma;        // Dma settings to send response packets directly from circular buffer

    fgc_ether_header_t      rx_header;
    bool                    rx_is_broadcast;
    uint32_t                rx_time_us;
    fgc_ether_header_t      tx_header;
} ethernet_t;

typedef struct communication_histograms_rx
{
    histogram_t fgc;
    histogram_t gw;
    histogram_t fgc_id;
    histogram_t latency;
    histogram_t latency_id;
} communication_histograms_rx_t;

typedef struct communication_histograms_tx
{
    histogram_t gw;
    histogram_t fgc;
} communication_histograms_tx_t;

typedef struct communication_histograms
{
    communication_histograms_rx_t     rx;
    communication_histograms_tx_t     tx;
} communication_histograms_t;

// External Variables Declarations
ETHER_COMM_VARS_EXT fgc_gateway_t  fgc_gateway;
ETHER_COMM_VARS_EXT ethernet_t     ethernet;
ETHER_COMM_VARS_EXT communication_histograms_t __attribute__((section("SRAM_ram"))) communication_histograms;

#endif

//EOF
