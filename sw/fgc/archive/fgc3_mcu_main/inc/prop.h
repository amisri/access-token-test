/*---------------------------------------------------------------------------------------------------------*\
  File:         prop.h

  Purpose:      FGC MCU Software - Property Functions.

  Author:       Quentin.King@cern.ch

  Notes:        This file contains the functions to manipulate the properties and symbols
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PROP_H      // header encapsulation
#define PROP_H

#ifdef PROP_GLOBALS
#define PROP_VARS_EXT
#else
#define PROP_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <cmd.h>        // for struct cmd
#include <defprops.h>
#include <property.h>   // for struct prop, and typedef prop_size_t
//-----------------------------------------------------------------------------------------------------------

// Valid array index: the following mask is used to test if a property element index is valid or not
// On FGC3 the max property size and therefore the max element index is 4294967295.

#define PROP_ARRAY_IDX_MASK    (0xFFFFFFFF)

//-----------------------------------------------------------------------------------------------------------

// PropMap() function arguments

enum prop_map_specifier
{
    PROP_MAP_ALL,
    PROP_MAP_ONLY_PARENTS,
    PROP_MAP_ONLY_CHILDREN
};

typedef void (*prop_map_func)(INT16U lvl, struct prop * p);

//-----------------------------------------------------------------------------------------------------------

typedef struct prop_debug
{
    INT16U access_timeouts;   // FGC.DEBUG.DSP_PROP.TIMEOUT
} prop_debug_t;

PROP_VARS_EXT prop_debug_t prop_debug;

//-----------------------------------------------------------------------------------------------------------

INT16U      PropFind(const char    *    propname);
INT16U      PropIdentifyProperty(struct cmd * c);
INT16U      PropIdentifySymbol(struct cmd * c, const char * delim, INT16U sym_type, INT16U * sym);
INT16U      PropIdentifyArray(struct cmd * c);
INT16U      PropIdentifyUser(struct cmd * c, INT16U user_from_header);
INT16U      PropIdentifyGetOptions(struct cmd * c, struct prop * p);
prop_size_t PropGetNumEls(struct cmd * c, struct prop const * p);

void PropSetNumEls(struct cmd * c, struct prop * p, prop_size_t n_elements);

void PropSet(struct cmd * c, struct prop * p, const void * value, prop_size_t n_elements);

void PropSetFar(struct cmd * c, struct prop * p, const void * value, prop_size_t n_elements);

INT16U PropValueGet(struct cmd * c, struct prop * p, BOOLEAN * set_f, prop_size_t el_idx, INT8U ** data_ptr);

INT16U PropBlkGet(struct cmd * c, prop_size_t * n_elems_ptr);

INT16U PropBlkSet(struct cmd * c, prop_size_t n_elements, BOOLEAN set_f, INT16U last_blk_f);

INT16U PropBufWait(struct cmd * c);

void PropMap(prop_map_func func, enum prop_map_specifier specifier);

//-----------------------------------------------------------------------------------------------------------

#endif  // PROP_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: prop.h
\*---------------------------------------------------------------------------------------------------------*/
