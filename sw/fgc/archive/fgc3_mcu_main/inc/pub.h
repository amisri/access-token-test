/*!
 *  @file      pub.h
 *  @defgroup  FGC3:MCU
 *  @brief     Header file for pub.c - Publication of subscriptions
 *             The publication thread uses the subscription table to know which properties to publish.
 *             The sub structures and constants are defined here.
 */

#ifndef PUB_H
#define PUB_H

#ifdef PUB_GLOBALS
#define PUB_VARS_EXT
#else
#define PUB_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <os.h>
#include <property.h>
#include <defconst.h>

// Constants

#define SUB_FLAGS_GET_PENDING           0x01            // Get request to gateway is pending

#define SUB_USE_GET_SIZE_LIMIT          100             // Threshold based on max_elements (bytes)

#define SUB_INDEX_LEN    ((FGC_MAX_SUBS+15)/16)         // Number of 16 bit index masks
#define SUB_USER_LEN     (FGC_MAX_USER/16+1)            // Number of 16 bit index masks

// External structures, unions and enumerations

struct pub_sema
{
    OS_SEM           *          lock;                   // Semaphore lock for sub structure
    OS_SEM           *          last_pkt_send;          // Semaphore signal that last packet is now being sent
};

struct sub_idx_subs                                     // Index for subscriptions (for publish and cancel)
{
    INT16U              master;                         // Top index mask for the mask array
    INT16U              mask_array[SUB_INDEX_LEN];      // masks array - must follow 'master'
};

struct sub_idx_users                                    // Index for users for one sub (for publish and get pending)
{
    INT16U              master;                         // Top index mask for the mask array
    INT16U              mask_array[SUB_USER_LEN];       // masks array - must follow 'master'
};

struct st_pending_get
{
    INT8U               sub_idx;                        // Pending get sub_idx
    INT8U               user;                           // Pending get user

};

struct sub_table                                        // Subscription table
{
    struct prop        *        prop;           // Pointer to subscribed property (0=Not in use)
    INT8U                       idx;            // Next free idx when not in use, user idx when in use
    INT8U                       notify_count;   // Notification counter (num of users pending)
    struct sub_idx_users notify_msk;        // Mask for publication notifications for all users
    struct sub_idx_users set_f_msk;         // Mask indicating if the publication comes from a set command
};

struct sub_vars
{
    INT16U                      n_subs;                 // Number of subscriptions
    INT16U                      n_direct_pubs;          // Number of pending direct publications
    INT16U                      n_get_pubs;             // Number of pending get publications
    INT8U                       free_sub_idx;           // Free list for sub table entries
    INT8U                       new_sub_idx;            // New sub pending send to GW
    INT8U                       resub_indx;
    INT8U                       pub_idx;                // Current publication subscription index
    INT8U                       flags;                  // Subscription flags
    struct st_pending_get pending_get;
    struct sub_idx_subs notify_msk;     // Mask of the subscriptions with publish notification
    struct sub_idx_subs sub_acked_msk;  // Mask of the subscriptions that have been acknowledgement
    // to the Gateway and need first update publication.
    struct sub_idx_subs new_sub_msk;    // Mask of the subscriptions for which FGC_FIELDBUS_PUB_TRIG_NEW_SUB
    // must be set in the publication responses
    struct sub_table table[FGC_MAX_SUBS];   // Subscription tables
};

// External function declarations

/*!
 * Publish task function.
 *
 * This function is used to publish subscribed properties. Publication is
 * driven by the subscription table in sub.table[]. The MstTsk() will
 * resume this task every 20 milliseconds.  The task either blocks on the
 * output stream semaphore when the stream buffer is full, or it suspends
 * itself if it finds nothing to publish.
 *
 * @param unused
 */
void PubTsk(void * unused);

/*!
 * This function blocks the publication stream (using pcm.abort_f) and request
 * PubTsk to cancel all subscriptions.
 */
void PubCancelPub(void);

/*!
 * This function allocates a new subscription table and associates it with
 * the property passed as argument. It returns the subscription index in the
 * variable pointed by sub_idx_ptr.
 *
 * @param p property to add.
 * @param sub_idx_ptr the subscription index.
 *
 * @retval FGC_OK_NO_RSP if it succeeded, FGC_PROTO_ERROR or FGC_BUSY otherwise.
 */
INT16U PubAddSub(struct prop * p, INT16U * sub_idx_ptr);

/*!
 * This function is called from CmdUnSub() to cancel a subscription. It returns
 * the subscription index in the variable pointed by sub_idx_ptr.
 *
 * @param p property to remove.
 * @param sub_idx_ptr the subscription index.
 *
 * @retval FGC_OK_NO_RSP if it succeeded, FGC_PROTO_ERROR otherwise.
 */
INT16U PubRemoveSub(struct prop * p, INT16U * sub_idx_ptr);

/*!
 * This function is called if the prop(user) value has changed. It will notify
 * PubTsk to publish any subscriptions that include this property/user,
 * including parents.
 *
 * @param p property to publish.
 * @param user user that has changed it value in the property.
 * @set_f TRUE if the publication is the result of a set command.
 */
void PubProperty(struct prop * p, INT16U user, BOOLEAN set_f);

/*!
 * This function is called for a new subscription once the sub_idx has been
 * sent to the gateway in a command response packet, or if an existing
 * subscription is re-subscribed. It sets the publish flags for all users
 * for the property, as well as the start new subscription flag.
 *
 * @param sub_idx index in the table with the subscription
 *
 */
void PubSubscription(INT16U sub_idx);

// External variable definitions

PUB_VARS_EXT struct sub_vars sub;   // Subscription variables structure
PUB_VARS_EXT struct pub_sema pub;   // Publication semaphore

#endif

// EOF
