/*---------------------------------------------------------------------------------------------------------*\
  File:         dev.h

  Purpose:

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DEV_H   // header encapsulation
#define DEV_H

#ifdef DEV_GLOBALS
#define DEV_VARS_EXT
#else
#define DEV_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <fgc_consts_gen.h>     // Global FGC constants
#include <microlan_1wire.h>     // for MICROLAN_NETWORK_ALL_ELEMENTS_MAX
#include <definfo.h>            // for FGC_CLASS_ID, FGC_PLATFORM_ID, FGC_PLATFORM_NAME, FGC_CLASS_NAME
#include <os.h>                 // for OS_SEM type
#include <dsp_loader.h>
#include <property.h>           // for prop_size_t
#include <fgc/fgc_db.h>           // for prop_size_t

//-----------------------------------------------------------------------------------------------------------

#define DEV_NAME_SIZE           32

// for shared_mem.boot_seq (SM_BOOTTYPE_P)
#define BOOT_PWR                0               // Power start - do full self test and update codes
#define BOOT_SLOW               1               // Slow start  - do partial test and update codes
#define BOOT_FAST               2               // Fast start  - do minimal test and run main program
#define BOOT_UNKNOWN            0xFF            // Uninitialised value for boot type

#define DEV_TEST_DATA_SZ        32              // in 16-bit words


//-----------------------------------------------------------------------------------------------------------
struct sys_info
{
    //Sys Info
    fgc_db_t            sys_idx;                        // System index in SYSDB
    char                sys_type[FGC_SYSDB_SYS_LEN + 1];
    INT8U               detected[FGC_N_COMP_GROUPS];    // Number of detected Dallas devices in each group
    fgc_db_t            comp_idx[MICROLAN_NETWORK_ALL_ELEMENTS_MAX];  // Component index for id.devs[] (0 = unknown)
    INT8U               n_unknown_grp[FGC_COMPDB_N_COMPS];            // Number of unknown group members for each comp type
};

struct dev_vars
{
    INT8U               platform_id;                    // Platform ID     |
    INT8U               class_id;                       // Class ID        | These four elements are
    INT8S               platform_nm[8];                 // Platform name   | initialised in main.c
    INT8S               class_nm[16];                   // Class name      |
    INT32U              main_prog_version;              // Main program version
    INT16U              max_user;                       // Set according to DEVICE.PPM and class
    INT16U              omode_mask;                     // omode_mask (from NameDB)
    INT16U              ctrl_counter;                   // Device Reset/Boot/PwrCyc/Crash down counter (5ms)
    INT16U              ctrl_sym_idx;                   // Device Reset/Boot/PwrCyc/Crash prop sym_idx
    INT16U              run_number;                     // Run number
    INT32U              cmdrcvd;                        // Commands received counter (DEVICE.CMDRCVD)
    INT16U              namedb_crc;                     // Resident NameDB CRC
    enum dsp_error      dsp_load_error;
    INT32U              dsp_version;                    // DSP program version
    INT16U              leds;                           // Front panel LED values (see MstTsk())
    BOOLEAN             dbs_ok_f;                       // SysDB, CompDB and DimDB match versions
    BOOLEAN             pm_enabled_f;                   // Post morten enabled flag (from FIP time var)
    INT16U              log_pm_state;                   // PM Log state
    INT16U              log_pm_test;                    // Manual post mortem trigger property
    INT16U              log_pm_trig;                    // Readback of post mortem trigger bits in ACK byte
    INT16U              pwr_cyc_req;                    // Activate power cycle request (must be
    OS_SEM       *      set_lock;                       // Set command lock semaphore
    prop_size_t         name_nels;                      // Device name length
    prop_size_t         type_nels;                      // Length of dev.type
    FP32                psu_fgc[3];                     // FGC PSU voltages: +5V, +15V, -15V
    FP32                psu_ana[3];                     // Ana interface PSU voltages: +5V, +15V, -15V
    char                test_data[2 * DEV_TEST_DATA_SZ ]; // Buffer for TEST.XXXX properties
    char                name[DEV_NAME_SIZE + 1];        // System (power converter) name
    char                type[6];                        // Device type (system type property DEVICE.TYPE)
    struct sys_info     sys;
};

//-----------------------------------------------------------------------------------------------------------

DEV_VARS_EXT struct dev_vars    dev
#ifdef DEV_GLOBALS
        =
{
    FGC_PLATFORM_ID,
    FGC_CLASS_ID,
    FGC_PLATFORM_NAME,
    FGC_CLASS_NAME,
}
#endif
;
//-----------------------------------------------------------------------------------------------------------

#endif  // DEV_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dev.h
\*---------------------------------------------------------------------------------------------------------*/
