/*---------------------------------------------------------------------------------------------------------*\
  File:         hash.h

  Contents:     Header file for hash.c - Hashing functions

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef HASH_H
#define HASH_H

/*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>
#include <definfo.h>

/*---------------------------------------------------------------------------------------------------------*/

typedef uintptr_t HASH_VALUE;                   // needed by defprops.h

// Hash table Structure Declaration

struct hash_entry
{
    const INT16U        sym_idx;                // Symbol index
    const HASH_VALUE    value;                  // Pointer to top level property OR getopt mask
    union hash_key
    {
        char      *     c;                      // Symbol string (nul padded to 14 characters)
    } key;
    struct hash_entry * next;                   // Link to next hash entry
};

/*---------------------------------------------------------------------------------------------------------*/

void    HashInit(void);
INT8U   HashInitDIM(const char * key, INT16U n_dims);
INT16U  HashFindProp(const char * prop_symbol);
INT16U  HashFindConst(const char * const_symbol);
INT16U  HashFindGetopt(const char * getopt_symbol);

//-----------------------------------------------------------------------------------------------------------

#endif  // HASH_H end of header encapsulation

// EOF
