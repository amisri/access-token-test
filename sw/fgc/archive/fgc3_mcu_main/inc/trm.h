/*---------------------------------------------------------------------------------------------------------*\
  File:         trm.h

  Contents:     Header file for trm.c - Terminal Communications Interface

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef TRM_H
#define TRM_H


#ifdef TRM_GLOBALS
#define TRM_VARS_EXT
#else
#define TRM_VARS_EXT extern
#endif

#include <cc_types.h>           // basic typedefs
#include <os.h>                 // for OS_MSG, OS_MEM types
#include <queue_fgc.h>          // struct queue

//-----------------------------------------------------------------------------------------------------------

// Serial Communication Interface Global Constants

#define TRM_N_BUFS                      6
#define TRM_BUF_SIZE                    40
#define TRM_MSGQ_SIZE                   64
#define TRM_DIRECT_MODE                 0
#define TRM_EDITOR_MODE                 1
#define TRM_DIAG_MODE                   2


#define TRM_N_LINEBUFS                        16                      // Must be a power of 2 !!!
#define TRM_LINEBUFS_MASK                     (TRM_N_LINEBUFS-1)      // Mask for circular history buffer
#define TRM_LINE_SIZE                         78
#define TRM_EDITOR_MODE_TIMEOUT_MIN           60
#define TRM_CMD_START                          1
#define TRM_CMD_DEFINED                        2
#define TRM_CMD_RECEIVING                      3


//-----------------------------------------------------------------------------------------------------------

// Serial Communication Interface Task Variables

struct terminal_vars
{
    OS_MSG       *      msgq;                           // Input characters message Q
    OS_MEM       *      mbuf;                           // Output buffers memory partition (in terminal.mem_buf)
    INT16U              mode;                           // Terminal mode
    INT16U              xoff_timer;                     // XOFF timer (ms)

    INT8U               mem_buf[TRM_N_BUFS * TRM_BUF_SIZE ];    // terminal output buffer space
    void        *       msgq_buf[TRM_MSGQ_SIZE];         // Trm task message queue buffer

    BOOLEAN             char_waits_for_tx;              // Character is waiting to be sent flag
    INT8U               snd_ch;                         // Single char output buffer to IsrMst
};


struct terminal_static
{
    INT16U              edit_mode_timer_ms;             // Edit mode timer (ms)
    INT16U              edit_mode_timer_min;            // Edit mode timer (minutes)
    INT16U              edit_state;                     // Terminal editor state machine
    INT16U              line_idx;                       // Line editor buffer index
    INT16U              line_end;                       // Line editor end of buffer contents index
    INT16U              cur_line;                       // Current line in linebufs[] history
    INT16U              recall_line;                    // Recall line in linebufs[] history
    INT16U              line_len[TRM_N_LINEBUFS];       // History line lengths
    INT16U              cmd_state;                      // Direct reception state machine
    INT16U              pkt_buf_idx;                    // Packet buffer index (0 or 1)
    INT16U              pkt_in_idx;                     // Packet input index (0-120)
    INT16U              pkt_header_flags;               // Command packet header
    struct queue        bufq;                           // TRM buffer Q indices and status
    struct cmd_pkt   *  pkt;                            // Pointer to current cmd packet
    char        *       bufq_adr[TRM_N_BUFS + 1];       // TRM buffer address Q
    INT16U              bufq_len[TRM_N_BUFS + 1];       // TRM buffer length Q
    char                linebuf[TRM_LINE_SIZE + 2];     // TRM edit line buffer
    char                linebufs[TRM_N_LINEBUFS][TRM_LINE_SIZE + 2]; // TRM history line buffers
    BOOLEAN             cmd_space_f;                    // SciAddCmdCh() space flag
};

//-----------------------------------------------------------------------------------------------------------

void    TrmTsk(void *);
void    TrmMsOut(void);

//-----------------------------------------------------------------------------------------------------------

TRM_VARS_EXT struct terminal_vars    terminal;               // Terminal variables structure
TRM_VARS_EXT struct terminal_static  terminal_state;

//-----------------------------------------------------------------------------------------------------------

#endif  // TRM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: trm.h
\*---------------------------------------------------------------------------------------------------------*/

