/*!
 *  @file      rtd.c
 *  @defgroup  FGC3:MCU
 *  @brief     Global option line display Layout (DIAG, PSU)
 *                         1         2         3         4         5         6         7         8
 *               +---------+---------+---------+---------+---------+---------+---------+---------++
 *    DIAG   20  |aa:nnnn aa:xnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn |
 *    PSU    20  |+5: n.nn    +15: nn.nn  -15:-nn.nn                                              |
 *               +---------+---------+---------+---------+---------+---------+---------+---------++
 */

#ifndef RTD_H
#define RTD_H

#ifdef RTD_GLOBALS
#define RTD_VARS_EXT
#else
#define RTD_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <definfo.h>
#include <fbs_class.h>
#include <sta.h>
#include <stdio.h>
#include <term.h>

// Constants

/*! Save then set cursor position */
#define RTD_START_POS           "\33" "7\33[%sH" TERM_BOLD
/*! Restore cursor position */
#define RTD_END                 "\33" "8\v"
#define RTD_OPTLINE_VPOS        20

#define RTD_OPTLINE_POS         "20;1"
#define RTD_DIAGDATA_HPOS       1
#define RTD_DIAGDATA_HWIDTH     8
#define RTD_PSU_HPOS            1
#define RTD_PSU_HWIDTH          12

// RTD Field position constants. Format: "line;column"

#define RTD_TOUT_POS            "21;76"
#define RTD_TIN_POS             "22;76"
#define RTD_CPUUSE_POS          "24;75"
#define RTD_CPU32USE_POS        "24;78"
#define RTD_STATES_POS          "21;9"
#define RTD_IREF_POS            "21;30"
#define RTD_IMEAS_POS           "22;30"
#define RTD_IRATE_POS           "23;30"
#define RTD_VREF_POS            "21;46"
#define RTD_VMEAS_POS           "22;46"
#define RTD_IERR_POS            "23;46"
#define RTD_VCAPA_POS           "24;46"
#define RTD_VA_POS              "21;59"
#define RTD_IA_POS              "21;59"
#define RTD_VB_POS              "22;59"
#define RTD_IB_POS              "22;59"
#define RTD_VC_POS              "23;59"
#define RTD_VS_POS              "23;59"
#define RTD_VD_POS              "24;59"
#define RTD_AUX_POS             "24;59"
#define RTD_IDIFF_POS           "24;30"
#define RTD_TRUN_POS            "24;60"
#define RTD_TREM_POS            "24;75"
#define RTD_REFTYPE_POS         "24;6"
#define RTD_REF_START_POS       "24;30"
#define RTD_REF_END_POS         "24;43"
#define RTD_LOAD_R_POS          "20;1"
#define RTD_LOAD_L_POS          "20;12"
#define RTD_PROT_IEARTH         "20;1"
#define RTD_PROT_POS_POS        "20;23"
#define RTD_PROT_NEG_POS        "20;35"
#define RTD_CYC                 "20;1"

#define RTD_ADCS_HWIDTH         41
#define RTD_ADCS_HPOS_RAW       1
#define RTD_ADCS_HPOS_VOLTS     11
#define RTD_ADCS_HPOS_PP        22
#define RTD_ADCS_HPOS_AMPS      30

// External structures, unions and enumerations

struct rtd_flags                                        // RTD flag display structure
{
    INT16U       *      var;                            // Pointer to flag variable
    INT16U              mask;                           // Flag bit mask
    INT8S               flag[2];                        // flag[0]: #=inv on 1 ~=inv on 0 else use flags
    char        *       pos;                            // Flag position "yy;xx"
};

struct rtd_vars
{
    INT16U              enabled;                        // Global enable flag
    INT16U              ctrl;                           // Option line control
    INT16U              last_ctrl;                      // Old value of option line control
    FILE        *       f;                              // Pointer to RTD output stream
    char        *       serial_stream_buffer_ptr;
    INT16U              serial_stream_buffer_offset;
    INT16U              line_offset;
    INT16U              idx;                            // Field index
    INT16U              flag_idx;                       // Flag array index
    INT16U              flag_end_idx;                   // Flag index loop end

    struct rtd_vars_last                                // Last displayed values:
    {
        BOOLEAN         flag_off[40];                   // Flag is not set
        INT16U          tout;                           // Outlet temperature
        INT16U          tin;                            // Inlet temperature
        INT16S          cpu_usage;                      // Last displayed CPU usage
        INT16S          cpu32_usage;                    // Last displayed CPU32 usage
        INT16U          state_pll;                      // Last displayed PLL State
        INT16U          state_op;                       // Operational state
        FP32            iref;                           // Iref
        FP32            imeas;                          // Imeas
        FP32            vref;                           // Vref
        FP32            vmeas;                          // Vmeas
        FP32            vcapa;                          // Vcapa
        FP32            i_rate;                         // Irate
        FP32            vadc[FGC_N_ADCS];               // Measured voltages (Vadc)
        FP32            i_meas[2];                      // Ia, Ib
        FP32            v_meas[2];                      // Vs, Aux
        char            iref_s[10];                     // Iref string
        char            imeas_s[10];                    // Imeas string
        char            vref_s[10];                     // Vref string
        char            vmeas_s[10];                    // Vmeas string
        char            vcapa_s[10];                    // Vcapa string
        char            i_rate_s[10];                   // Irate string
        char            vadc_s[FGC_N_ADCS][12];         // Measured voltages string (4x Vadc)
        char            i_meas_s[2][12];                // Measured currents string (Ia, Ib)
        char            v_meas_s[2][12];                // Measured voltages string (Vs, Aux)
        INT16U          state_vs;                       // Voltage source state
        INT16U          state_pc;                       // Power converter state
        INT16U          state_lk;                       // PAL links state
        INT16U          ref_stc_type;                   // Ref type symbol index
        FP32            ref_end;                        // ref end
        INT16S          ierr;                           // Current loop error (mA)
        INT16S          idiff;                          // Measurement difference (mA)
        INT32U          trun;                           // Ref run time
        INT32U          trem;                           // Ref remain time
    } last;

    struct optline
    {
        INT16U          idx;                            // Option line index
        INT16U          diag_list_len;                  // Last value of rtd diagnostic channel list len
        INT16U          diag_chan[10];                  // Last displayed diag channel
        INT16U          diag_data[10];                  // Last displayed diag data
        FP32            psu[3];                         // +5V,+15V,-15V PSU floats
        FP32            i_earth;                        // IEARTH as a float
        char            float_s[3][10];                 // Option line float values as strings
        INT32S          adc_raw[2];                     // Last displayed ADC raw value
        FP32            adc_volts[2];                   // Last displayed ADC volts value
        FP32            adc_pp[2];                      // Last displayed ADC pp noise value
        FP32            adc_meas[2];                    // Last displayed ADC amps value
        FP32            ohms;                           // Measured load resistance
        FP32            henrys;                         // Measured load inductance
        FP32            u_leads[2];                     // Measured U_LEAD_POS/NEG
        char            ohms_s[10];                     // Measured load resistance string
        char            henrys_s[10];                   // Measured load inductance string
        INT16S          user;                           // Last displayed user

    } optline;
};

// External function declarations

/*!
 * This function is the Real-time display task.
 *
 * The real-time display is active only if the line editor is enabled.
 * The Rtd Task is triggered every 100 ms by the millisecond task. It
 * works through the different fields in turn, looking for a field with
 * changed data. If one is found, it refreshes the display for this value.
 * Only one field is updated per cycle, so a maximum of 10 fields may be
 * updated per second.
 */
void RtdTsk(void *);

/*!
 * This function prepares the screen for the 4 line real-time display.
 */
void RtdInit(FILE *);

// External variable definitions

RTD_VARS_EXT struct rtd_vars __attribute__((section("SRAM_ram"))) rtd;

#endif

// EOF
