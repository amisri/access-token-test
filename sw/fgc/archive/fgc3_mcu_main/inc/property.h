/*---------------------------------------------------------------------------------------------------------*\
  File:         property.h

  Contents:     property structures of the MCU
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PROPERTY_H
#define PROPERTY_H

#ifdef PROPERTY_GLOBALS
#define PROPERTY_VARS_EXT
#else
#define PROPERTY_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

// Symbol Table related constants

#define ST_TLPROP                       0               // Symbol is a top level property label
#define ST_LLPROP                       1               // Symbol is a low level property label
#define ST_CONST                        2               // Symbol is an integer constant
#define ST_GETOPT                       3               // Symbol is an get option
#define ST_MAX_SYM_LEN                  20              // Maximum symbol length
#define ST_NEAT_FORMAT_TAB_SZ           4               // Tab size used for neat formatting in the console

#define DEFPROPS_FUNC_SET(func)   INT16U func (struct cmd *, struct prop *)
#define DEFPROPS_FUNC_GET(func)   INT16U func (struct cmd *, struct prop *)
#define DEFPROPS_FUNC_SETIF(func) INT16U func (struct cmd *)

// Dynamic property flags

#define DF_TIMESTAMP_SELECT             0x04            // Property uses a timestamp different from time now.
#define DF_NVS_SET                      0x08            // Non-volatile property has been set
#define DF_CFG_CHANGED                  0x10            // Non-volatile config property has been changed
#define DF_SET_BY_DALLAS_ID             0x20            // Property set by Dallas ID (barcode)
#define DF_SUBSCRIBED                   0x40            // Property is subscribed
#define DF_PUB_USING_GET                0x80            // Property is so big that publish should use GET

// Timestamp selector used if DF_TIMESTAMP_SELECT dynamic flag is set for the property.

enum timestamp_select
{
    TIMESTAMP_NOW,                                      // Time now (default)
    TIMESTAMP_CURRENT_CYCLE,                            // Time of start of current cycle
    TIMESTAMP_PREVIOUS_CYCLE,                           // Time of start of previous cycle
    TIMESTAMP_LOG_CAPTURE,                              // Time of start of capture log
    TIMESTAMP_LOG_OASIS,                                // Time of start of LOG.OASIS buffer
};

//-----------------------------------------------------------------------------------------------------------

// Property maximum size (i.e. max number of elements)
//
// The type to be used for the property size and index is prop_size_t. It is defined as follows:
//  - INT16U on FGC2, allowing a maximum of 65535 elements per property (and per user, for PPM ones);
//  - INT32U on FGC3, allowing a maximum of 4294967295 elements per property (and per user, for PPM ones).
//
// Note the distinction between prop_size_t and uintptr_t: the former is always a size expressed in number of
// elements, whereas the latter (used as the type of prop->n_elements) is either a size OR a pointer to a size.

typedef INT32U    prop_size_t;
//-----------------------------------------------------------------------------------------------------------

// needed for constants generated in z:\projects\fgc\sw\inc\classes\xx\defprops.h
struct sym_lst
{
    INT16U      sym_idx;                                // FGC_NOT_SETABLE bit (0x8000) set if not settable
    INT16U      value;                                  // Symbol list constant value
};

struct init_prop
{
    struct prop    *    prop;                           // Pointer to property
    prop_size_t         n_elements;                     // Number of elements
    void        *       data;                           // Pointer to initialisation data
};

// better name struct property_kernel ?
struct prop
{
    INT16U          sym_idx;            // Index in sym_tab[] for property's symbol
    INT16U          flags;              // Bit flags PF_XXXX
    INT8U           type;               // Type from enum prop_type_e
    const INT8U     setif_func_idx;     // Index from enum setif_func_e
    INT8U     set_func_idx;             // Index from enum set_func_e
    INT8U           get_func_idx;       // Index from enum get_func_e
    uintptr_t       n_elements;         // Property size in nb of elements, or pointer to the prop size
    prop_size_t     max_elements;       // Max property size in nb of elements
    const void   *  range;              // Pointer to sym_lst or limits
    void      *     value;              // Pointer to property value
    const INT8U     dsp_idx;            // Type from enum dsp_xxx_prop_e
    INT8U           nvs_idx;            // Non-volatile storage index
    INT8U           sub_idx;            // Index into subscription table
    INT8U           dynflags;           // Dynamic flags
    struct prop  *  parent;             // Pointer to parent property structure
};

//-----------------------------------------------------------------------------------------------------------

#endif  // PROPERTY_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: property.h
\*---------------------------------------------------------------------------------------------------------*/
