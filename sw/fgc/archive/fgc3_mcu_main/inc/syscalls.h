/*---------------------------------------------------------------------------------------------------------*\
  File:         syscalls.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef SYSCALLS_H      // header encapsulation
#define SYSCALLS_H

#ifdef SYSCALLS_GLOBALS
#define SYSCALLS_VARS_EXT
#else
#define SYSCALLS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <sys/reent.h>          // for struct _reent
#include <tsk.h>                // for MCU_NB_OF_TASKS

//-----------------------------------------------------------------------------------------------------------
// if this values are changed the linker.txt file needs to be updated

enum
{
    NEWLIB_MALLOC_POOL1 = 0,
    NEWLIB_MALLOC_POOL2,
    NEWLIB_MALLOC_POOL3,
    NEWLIB_MALLOC_POOL4,
    NEWLIB_MALLOC_NB_OF_POOLS
};

#define NEWLIB_MALLOC_POOL1_BLOCK_SIZE          32
#define NEWLIB_MALLOC_POOL1_BLOCK_COUNT         10

#define NEWLIB_MALLOC_POOL2_BLOCK_SIZE          64
#define NEWLIB_MALLOC_POOL2_BLOCK_COUNT         10

#define NEWLIB_MALLOC_POOL3_BLOCK_SIZE          128
#define NEWLIB_MALLOC_POOL3_BLOCK_COUNT         30

#define NEWLIB_MALLOC_POOL4_BLOCK_SIZE          500
#define NEWLIB_MALLOC_POOL4_BLOCK_COUNT         4


//-----------------------------------------------------------------------------------------------------------

struct TMallocManagerCtrl
{
    INT16U  buffer_size  [NEWLIB_MALLOC_NB_OF_POOLS];                   // FGC.DEBUG.MEM_POOLS.BUF_SIZE
    INT16U  nb_of_buffers[NEWLIB_MALLOC_NB_OF_POOLS];                   // FGC.DEBUG.MEM_POOLS.TOTAL_NB
    INT16U  free_idx     [NEWLIB_MALLOC_NB_OF_POOLS];                   // FGC.DEBUG.MEM_POOLS.ALLOCATED
};

struct TMallocManagerData
{
    struct _pool1
    {
        INT8U   buffer[NEWLIB_MALLOC_POOL1_BLOCK_SIZE];
    } pool1[NEWLIB_MALLOC_POOL1_BLOCK_COUNT];

    struct _pool2
    {
        INT8U   buffer[NEWLIB_MALLOC_POOL2_BLOCK_SIZE];
    } pool2[NEWLIB_MALLOC_POOL2_BLOCK_COUNT];

    struct _pool3
    {
        INT8U   buffer[NEWLIB_MALLOC_POOL3_BLOCK_SIZE];
    } pool3[NEWLIB_MALLOC_POOL3_BLOCK_COUNT];

    struct _pool4
    {
        INT8U   buffer[NEWLIB_MALLOC_POOL4_BLOCK_SIZE];
    } pool4[NEWLIB_MALLOC_POOL4_BLOCK_COUNT];
};

struct TMallocManagerDbg        // NB: All sub-structures below are aligned on 4 bytes for easier reading in a memory dump
{
    struct _panic_dbg
    {
        INT32U      requested_size;
        INT32U      task_id;
    } panic_data;

    struct _pool_dbg
    {
        INT8U       free;
        INT8U       task_id;
        INT16U      requested_size;
    }                   pool1[NEWLIB_MALLOC_POOL1_BLOCK_COUNT];

    struct _pool_dbg    pool2[NEWLIB_MALLOC_POOL2_BLOCK_COUNT];

    struct _pool_dbg    pool3[NEWLIB_MALLOC_POOL3_BLOCK_COUNT];

    struct _pool_dbg    pool4[NEWLIB_MALLOC_POOL4_BLOCK_COUNT];
};



//-----------------------------------------------------------------------------------------------------------
void    InitMalloc(void);
//-----------------------------------------------------------------------------------------------------------

// Reentrant structures for tasks that will need malloc/calloc.

SYSCALLS_VARS_EXT struct _reent newlib_reent_fbs;       // FBS
SYSCALLS_VARS_EXT struct _reent newlib_reent_sta;       // STA
SYSCALLS_VARS_EXT struct _reent newlib_reent_scivs;     // SCIVS
SYSCALLS_VARS_EXT struct _reent newlib_reent_pub;       // PUB
SYSCALLS_VARS_EXT struct _reent newlib_reent_fcm;       // FCM
SYSCALLS_VARS_EXT struct _reent newlib_reent_tcm;       // TCM
SYSCALLS_VARS_EXT struct _reent newlib_reent_rtd;       // RTD

// Reentrant structures pointer for all tasks

SYSCALLS_VARS_EXT struct _reent * newlib_reent_ptr[MCU_NB_OF_TASKS];

// Tasks pointing to the following reent structure (which is the default newlib one) are not allowed to call malloc/calloc.

SYSCALLS_VARS_EXT struct _reent * newlib_no_malloc_reent_ptr;

// Malloc/calloc buffer pools

SYSCALLS_VARS_EXT struct TMallocManagerCtrl   malloc_mngr_ctrl;
SYSCALLS_VARS_EXT struct TMallocManagerData   malloc_mngr_data;
SYSCALLS_VARS_EXT struct TMallocManagerDbg    malloc_mngr_dbg;

//-----------------------------------------------------------------------------------------------------------

#endif  // SYSCALLS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: syscalls.h
\*---------------------------------------------------------------------------------------------------------*/
