/*---------------------------------------------------------------------------------------------------------*\
  File:         log.h

  Purpose:      FGC MCU Software - Logging related functions

  Note:         LogInitEvtProp() makes links between bits and enums for a property with a symlist
                in the structure log_evt_prop.  This means that the symbol constant values for any
                property logged must NEVER be outside the range 0-15.  If this happens then the
                system will crash itself at start up with E=0xBAD0.  This will be visible in the run log.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef LOG_H      // header encapsulation
#define LOG_H

#ifdef LOG_GLOBALS
#define LOG_VARS_EXT
#else
#define LOG_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------
#define DEFPROPS_INC_ALL    // defprops.h

#include <cc_types.h>
#include <cmd.h>
#include <defprops.h>
#include <log_event.h>
#include <mcu_dsp_common.h>

//-----------------------------------------------------------------------------------------------------------

#define LOG_MAX_SIGS                    4        // Max signals in any operational log (IREG)
#define LOG_I_EARTH_SIGS                4        // There are up to four analogue channels measuring I_EARTH

// State machine for logging commands in the event log

#define EVT_LOG_RESET_PROP              0
#define EVT_LOG_PROP                    1
#define EVT_LOG_PROP_FULL               2
#define EVT_LOG_RESET_VALUE             3
#define EVT_LOG_VALUE                   4
#define EVT_LOG_VALUE_FULL              5


/*---------------------------------------------------------------------------------------------------------*/

// Analog signals log variables

struct log_ana_vars                                     // These fields are initialised in log.c
{
    INT32U              baseaddr;                       // ptr Buffer base address
    INT32U              bufsize_w;                      // ptr Buffer size in words
    INT16U              type;                           // ptr Sym_idx for log type
    INT16U              n_samples;                      // ptr Number of samples in the log
    INT16U              post_trig;                      // ptr Number of post trig samples
    INT16U              n_signals;                      // ptr Number of signals per sample
    INT16U              sample_size;                    // ptr Number of bytes per sample
    INT16S              wait_time_ms;                   // ptr Number of bytes per sample
    INT32U              period_ms;                      // ptr Period in milliseconds
    INT8U               sig_info[LOG_MAX_SIGS];         // Log info byte per signal
    char        *       sig_label_units[LOG_MAX_SIGS];  // Signal labels:units
    INT16U              samples_to_acq;                 // Number of samples still to acquire
    INT16U              last_sample_idx;                // Index of last sample
    struct abs_time_us  last_sample_time;               // Time of last sample
    BOOLEAN             run_f;                          // Logging active flag (cleared on trip)
};

//-----------------------------------------------------------------------------------------------------------

void LogInit(void);
void LogEvtCmd(struct cmd * c, INT16U errnum);
void LogEvtDim(void);
void LogEvtProp(void);
void LogEvtSaveCh(struct cmd * c, char ch);
void LogEvtSaveValue(struct cmd * c);
void LogEvtTiming(void);
void LogGetPmEvtBuf(struct cmd * c);
void LogGetPmSigBuf(struct log_ana_vars * log, struct cmd * c);
void LogIearth(void);
void * LogNextAnaSample(struct log_ana_vars * log_ana);
void LogStartAll(void);
void LogTday(void);
void LogThour(void);
void LogTStart(struct log_ana_vars * log_ana);

/*---------------------------------------------------------------------------------------------------------*/

struct timing_log_vars                                  // Initialised in log.c
{
    struct log_evt_rec  log_rec;                                // Event log record workspace
    INT16U              out_idx;                                // Buffer out index
    INT16U              in_idx;                                 // Circular buffer input index
    struct abs_time_us  time         [FGC_LOG_TIMING_LEN];      // Start of cycles time
    char                user         [FGC_LOG_TIMING_LEN];      // Cycle user (negative for start of super cycle)
    INT16U              stc_func_type[FGC_LOG_TIMING_LEN];      // Cycle function type symbol index
    char                reg_mode_flag[FGC_LOG_TIMING_LEN];      // Cycle regulation mode flag=[B|I|V]
    INT16U              dysfunction_code[FGC_LOG_TIMING_LEN];       // warning or fault number
    INT8U               dysfunction_is_a_fault[FGC_LOG_TIMING_LEN]; // warning (0) or fault (1)
};

typedef struct meas_i_earth_t
{
    FP32             value;              // MEAS.I_EARTH [20ms]  Measured earth current
    INT32S           pcnt;               //  [20ms] Measured earth current (% of trip limit)
    FP32             max;                // MEAS.MAX.I_EARTH
    FP32             limit;              // LIMITS.I.EARTH
    INT32S           channel[LOG_I_EARTH_SIGS];  // DIM channels containing the I_EARTH_##MS values.
} meas_i_earth_t;

typedef struct meas_units
{
    INT8U        i_unit;    // MEAS.UNIT.I
    INT8U        v_unit;    // MEAS.UNIT.V
} meas_units_t;

// Define/Initialise timing log variable

LOG_VARS_EXT struct timing_log_vars       timing_log
#ifdef LOG_GLOBALS
        =
{
    { {0, 0}, "USER=-- SSC=-", "REG_MODE=- FUNC_TYPE=-------------", "START" }, // timing_log.log_rec
    FGC_LOG_TIMING_LEN,                                                                // timing_log.out_idx
}
#endif
;

// Analogue log structure definitions

LOG_VARS_EXT struct log_ana_vars     log_iearth             // IEARTH log variables structure
#ifdef LOG_GLOBALS
        =
{
    SRAM_LOG_IEARTH_32,                         // Buffer base address
    SRAM_LOG_IEARTH_W,                          // Buffer size in words
    STP_IEARTH,                                 // Sym_idx for log type
    FGC_LOG_IEARTH_LEN,                         // Number of samples in the log
    FGC_LOG_IEARTH_LEN / 2,                     // Number of post trig samples
    1,                                          // Number of signals per sample
    4,                                          // Number of bytes per sample
    2500,                                       // Wait time (ms)
    5,                                          // Period in milliseconds (200Hz)
    { 0 },                                      // Signal info
    { "I_EARTH:A" },                            // Signal labels and units
}
#endif
;

LOG_VARS_EXT struct log_ana_vars     log_thour              // THOUR log variables structure
#ifdef LOG_GLOBALS
        =
{
    SRAM_LOG_THOUR_32,                          // Buffer base address
    SRAM_LOG_THOUR_W,                           // Buffer size in words
    STP_THOUR,                                  // Sym_idx for log type
    FGC_LOG_THOUR_LEN,                          // Number of samples in the log
    1,                                          // Number of post trig samples
    4,                                          // Number of signals per sample
    8,                                          // Number of bytes per sample
    0,                                          // Wait time (ms)
    10000,                                      // Period in milliseconds (10s)
    { 0, 0, 0, 0 },                             // Signal info
    {
        "T_FGC_IN:C", "T_FGC_OUT:C",              // Signal labels and units
        "T_DCCT_A:C", "T_DCCT_B:C"
    },
}
#endif
;

LOG_VARS_EXT struct log_ana_vars     log_tday               // TDAY log variables structure
#ifdef LOG_GLOBALS
        =
{
    SRAM_LOG_TDAY_32,                           // Buffer base address
    SRAM_LOG_TDAY_W,                            // Buffer size in words
    STP_TDAY,                                   // Sym_idx for log type
    FGC_LOG_TDAY_LEN,                           // Number of samples in the log
    1,                                          // Number of post trig samples
    4,                                          // Number of signals per sample
    8,                                          // Number of bytes per sample
    0,                                          // Wait time (ms)
    600000,                                     // Period in milliseconds (10 minutes)
    { 0, 0, 0, 0 },                             // Signal info
    {
        "T_FGC_IN:C", "T_FGC_OUT:C",              // Signal labels and units
        "T_DCCT_A:C", "T_DCCT_B:C"
    },
}
#endif
;

LOG_VARS_EXT const char * bitmask_default_sym[16]
#ifdef LOG_GLOBALS
=
{
    "BIT_00",
    "BIT_01",
    "BIT_02",
    "BIT_03",
    "BIT_04",
    "BIT_05",
    "BIT_06",
    "BIT_07",
    "BIT_08",
    "BIT_09",
    "BIT_10",
    "BIT_11",
    "BIT_12",
    "BIT_13",
    "BIT_14",
    "BIT_15"
}
#endif
;

LOG_VARS_EXT meas_i_earth_t  meas_i_earth;
LOG_VARS_EXT meas_units_t    meas_units;

//-----------------------------------------------------------------------------------------------------------

#endif  // LOG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: log.h
\*---------------------------------------------------------------------------------------------------------*/
