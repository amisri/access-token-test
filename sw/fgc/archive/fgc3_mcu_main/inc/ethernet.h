/*!
 *  @file     ethernet.h
 *  @defgroup FGC3
 *  @brief      Ethernet eth Interface Functions.
 *
 */

#ifndef ETHERNET_H
#define ETHERNET_H

#include <stdint.h>
#include <ether_comm.h>

// External functions declarations

/*!
 *  This function is called from main() and CheckStatus() to prepare the ethernet LAN9215 interface.
 */
void EthInit(void);

/*!
 *  This function check the LAN chip is in a consistent state. Recover if not
 */
void EthWatch(void);

/*!
 * This function is called from main() and CheckStatus() to prepare the ethernet LAN9215 interface.
 */
uint16_t EthHwInit(uint16_t timeout_ms);

/*!
 * This function will manage the interrupt from the lan9221 chip
 * All the processing of received packets takes place in the interrupt
 * Note: IRQ are sensitive to falling edge, therefore we need to clear ALL the lan92xx interrupts
 * so that the interrupt line goes inactive and IRQ is rearmed
 * If a new interrupt occurs during the routine, it MUST be processed and cleared as well.
 * With the de-assertion interval, we are sure no interrupt can arrive before leaving this routine.
 */
void EthISR(void);

/*!
 * This function copy command from local var to given place
 * Note: everything must be read in EthReadCmd (in the ISR), because the lan92xx Rx FIFO is flushed
 * Thus we access directly the destination buffer fcm_pars.pkt in EthReadCmd (other solution is to use a buffer)
 * Won't work if EthCpyCmd is used with an other destination pointer than fcm_pars.pkt
 */
void EthCpyCmd(void * to, uint16_t length);

#endif

// EOF

