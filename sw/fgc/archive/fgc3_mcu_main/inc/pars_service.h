/*---------------------------------------------------------------------------------------------------------*\
  File:         pars_service.h
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PARS_SERVICE_H   // Header encapsulation
#define PARS_SERVICE_H

#ifdef PARS_SERVICE_GLOBALS
#define PARS_SERVICE_VARS_EXT
#else
#define PARS_SERVICE_VARS_EXT extern
#endif

#include <cc_types.h>
#include <dpcom.h>      // for struct pars_buf
#include <pars.h>       // for struct cmd_pkt_parse_buf


// Command Packet parse buffers

struct cmd_pkt_parse_buf
{
    INT32U n_carryover_chars;                           // Number of characters from last token of prev pkt
    char   buf[MAX_CMD_PKT_LEN + MAX_CMD_TOKEN_LEN];    // Parse buffer with extra space for last token of previous packet
};

PARS_SERVICE_VARS_EXT struct cmd_pkt_parse_buf  fcm_pars_buf;
PARS_SERVICE_VARS_EXT struct cmd_pkt_parse_buf  tcm_pars_buf;

PARS_SERVICE_VARS_EXT struct pars_buf           fcm_pars;                       // FcmTsk parameters
PARS_SERVICE_VARS_EXT struct pars_buf           tcm_pars;                       // TcmTsk parameters

/*---------------------------------------------------------------------------------------------------------*/

void ParsPkt(struct pars_buf * pars, struct cmd_pkt_parse_buf * buf);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation PARS_SERVICE_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars_service.h
\*---------------------------------------------------------------------------------------------------------*/
