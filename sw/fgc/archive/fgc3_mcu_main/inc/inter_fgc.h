/*!
 *  @file      inter_fgc.h
 *  @defgroup  FGC3:MCU
 *  @brief     Inter FGC communication.
 */

#ifndef INTER_FGC_H
#define INTER_FGC_H

#ifdef INTER_FGC_GLOBALS
#define INTER_FGC_VARS_EXT
#else
#define INTER_FGC_VARS_EXT extern
#endif

// Includes

#include <stdint.h>
#include <cc_types.h>
#include <fgc_ether.h>
#include <mcu_dsp_common.h>
#include <defconst.h>

enum master_transitions
{
    MASTER_FS_TO_FO,
    MASTER_SP_TO_OF,
    MASTER_FO_TO_OF,
    MASTER_OF_TO_ST,
    MASTER_ST_TO_BK,
    MASTER_FS_TO_SP,
    MASTER_BK_TO_TS,
};

// External structures, unions and enumerations

typedef struct src_addr
{
    uint8_t     slave_id[FGC_MAX_SLAVES];
    uint8_t     remote_meas_id;
    uint8_t     master_id;
} src_addr_t;

typedef struct master
{
    uint32_t  converter_number;
    uint32_t  no_converter;
} master_t;

typedef struct communications
{
    uint8_t             dest_addr;
    src_addr_t          src_addr;
} communications_t;

typedef struct inter_fgc
{
    uint8_t             enable;
    uint8_t             role;
    master_t            master;
    communications_t    communications;
    uint16_t            slave_pc_state;
} inter_fgc_t;

typedef struct frames_inter_fgc
{
    uint32_t            used;
    uint32_t            unused;
    uint32_t            missing_communication[FGC_ETHER_MAX_FGCS];
} frames_inter_fgc_t;

typedef struct fgc_ether_master_payload
{
    uint32_t    mode_pc;
    FP32        ref;
    uint32_t    use_ref;

} fgc_ether_master_payload_t;

typedef struct fgc_ether_slave_payload
{
    uint32_t    faults;
    FP32        v_meas;
    uint32_t    inputs;
} fgc_ether_slave_payload_t;

typedef union inter_fgc_payload
{
    fgc_ether_master_payload_t  master;
    fgc_ether_slave_payload_t   slave;
    fgc_ether_meas_payload_t    meas;
} inter_fgc_payload_t;

typedef struct inter_fgc_packet
{
    fgc_ether_header_t          header;
    fgc_ether_metadata_t        metadata;
    inter_fgc_payload_t         payload;
} inter_fgc_packet_t;

// External functions declarations

/*!
 * Initialises inter-FGC communication
 *
 * Its purpose is to initialize the variables and the packet Header responsible for the communication
 * with other FGCs
 */
void interFgcInit(void);

/*!
 * Sends FGC packets
 *
 * This function is called by INT_Excep_IRQ4, when triggered by a rising edge and the FGC isn't independant.
 * It sends a packet to other FGC3s depending on its role.
 */
void interFgcSendPackets(void);

/*!
 * Maintains inter-FGC variables
 *
 * This function is called by the millisecond tick task.
 * It refreshes inter_fgc variables depending on the role of the FGC.
 * In the case of Master or Slave FGC3s, it checks the good reception of expected packets
 */
void interFgcStartMs(void);

/*!
 * Processes inter-FGC packets
 *
 * This function is called by the EthISR when the received packet as been determined as coming from another FGC.
 * It then calls functions which whill read the packet depending on the payload type.
 */
void interFgcReceivePacket(void);

void interFgcPopulateStatus(void);

/*!
 * Dummy function for class 61
 */
static inline bool interFgcIsMaster(void)
{
    return false;
}

/*!
 * Dummy function for class 61
 */
static inline bool interFgcIsSlave(void)
{
    return false;
}

/*!
 * Dummy function for class 61
 */
static inline bool interFgcMasterTransition(enum master_transitions transition)
{
    return true;
}

// External Variables Declarations

INTER_FGC_VARS_EXT inter_fgc_t inter_fgc;

#endif

// EOF
