/*!
 *  @file      get.h
 *  @defgroup  FGC3:MCU
 *  @brief     This file contains the function for the Get Task and all the
 *  property Get functions.
 */

#ifndef GET_H
#define GET_H

#ifdef GET_GLOBALS
#define GET_VARS_EXT
#else
#define GET_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <cmd.h>
#include <property.h>

// External structures, unions and enumerations

typedef struct debug_vars
{
    INT32U mem[2];      // DEBUG.MEM control: address and number of words
} debug_vars_t;

// External variable definitions

GET_VARS_EXT  debug_vars_t  debug;     // Debug variables structure

// Platform/class specific functions

/*!
 * Displays post mortem log property.
 *
 * This function is rather special and is only used by the gateway to get the
 * LOG.PM in binary.
 */
INT16U GetLogPm(struct cmd * c, struct prop * p);

/*!
 * Displays the DIM logs properties
 */
INT16U  GetLogPm(struct cmd * c, struct prop * p);

// External function declarations

/*!
 * Displays the FIFO data for ADC.
 */
INT16U GetFifo(struct cmd * c, struct prop * p);

/*!
 * Displays the timing log property
 */
INT16U GetLogTiming(struct cmd * c, struct prop * p);

/*!
 * Displays the diagnostic info about a specified property.
 *
 * This is a special Get function because it is activated by the get option
 * INFO rather than a property itself. It is intended to provide diagnostic
 * information about the property.
 */
void GetPropInfo(struct cmd * c, struct prop *);

/*!
 * Displays the size info about specified property (el_size, num_els, max_els).
 *
 * This is a special Get function because it is activated by the get option
 * SIZE rather than a property itself. It is intended to provide diagnostic
 * information about the property.
 */
INT16U GetPropSize(struct cmd * c, struct prop *);

/*!
 * Get function used for the following read-only properties:
 *    - LOG.OASIS.I_REF.DATA
 *    - LOG.OASIS.I_MEAS.DATA
 */
INT16U GetLog(struct cmd * c, struct prop * p);

/*!
 * Displays the capture signal log property.
 */
INT16U GetLogCaptureSig(struct cmd * c, struct prop * p);

/*!
 * Displays the capture log spy property.
 */
INT16U GetLogCaptureSpy(struct cmd * c, struct prop * p);

#endif

// EOF

