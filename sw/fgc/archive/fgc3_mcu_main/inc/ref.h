/*!
 *  @file     ref.h
 *  @defgroup FGC3:MCU
 *  @brief    Reference-related functions
 */

#ifndef FGC_REF_H
#define FGC_REF_H

#ifdef REF_GLOBALS
#define REF_VARS_EXT
#else
#define REF_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <defconst.h>
#include <property.h>
#include <cmd.h>

// Constants

#define NON_PPM_USER            (0)

// List of states which permit arming a NON-CYCLING function
// - is also -
// List of states which do not permit arming a CYCLING function.

#define RUNNING_FUNC_STATE_MASK ((1 << FGC_PC_IDLE)         | \
                                 (1 << FGC_PC_ARMED)        | \
                                 (1 << FGC_PC_RUNNING)      | \
                                 (1 << FGC_PC_ABORTING)     | \
                                 (1 << FGC_PC_SLOW_ABORT))

// External structures, unions and enumerations

struct ref_vars
{
    INT16U      reg_mode_init;                 // REG.MODE_INIT property
    INT16U      event_group;                   // Used in 53 for compatibility with 51

    union
    {
        FP32    f[5];
        INT32U  i[5];
    } pars;
};

// External variable definitions

REF_VARS_EXT struct ref_vars ref;

// External function declarations

/*!
 * This function is called to arm or disarm a reference function in the DSP.
 *
 * The rules about when a given user can be armed/disarmed are rather complex
 * and are summarised here: doc\ref_arming_rules.xlsx
 *
 * @param c Command structure.
 * @param user User number.
 * @param func_type Function type to arm.
 * @param stc_func_type Symbol index for function type to arm.
 *
 * @retval FGC_OK_NO_RSP if it succeeds.
 */
INT16U RefArm(struct cmd * c, INT16U user,
              INT32U func_type, INT16U stc_func_type);

#endif

// EOF
