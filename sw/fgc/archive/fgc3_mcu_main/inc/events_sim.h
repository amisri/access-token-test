/*---------------------------------------------------------------------------------------------------------*\
 File        : events_sim.h
 Description : Reproduces a super cycle by simulating events either based on a property or hear-beat.
 Scope       : FGC:MCU
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FGC_EVENTS_SIM_H_
#define FGC_EVENTS_SIM_H_

#ifdef EVENTS_SIM_GLOBALS
#define EVENTS_SIM_VARS_EXT
#else
#define EVENTS_SIM_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <defconst.h>
#include <deftypes.h>

// External structures, unions and enumerations

struct SimSC                            // PPM Simulated Super-cycle structure
{
    INT16U  cycles[FGC_SIMSC_LEN];      // Property TIME.SIMSUPERCYCLE
};

// External function declarations

struct fgc_event * EventsSimProcess(void);
void        EventsSimEnable(void);
void        EventsSimDisable(void);
BOOLEAN     EventsSimIsEnabled(void);

// External variable definitions

EVENTS_SIM_VARS_EXT struct SimSC simsc;

#endif

// EOF
