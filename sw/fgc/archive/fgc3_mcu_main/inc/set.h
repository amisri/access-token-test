/*---------------------------------------------------------------------------------------------------------*\
  File:         set.h

  Purpose:      FGC MCU Software - Set Command Functions
\*---------------------------------------------------------------------------------------------------------*/

#ifndef SET_H      // header encapsulation
#define SET_H

#ifdef SET_GLOBALS
#define SET_VARS_EXT
#else
#define SET_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <cmd.h>

//-----------------------------------------------------------------------------------------------------------

// Note: the prototype of these functions are defined automatically by the parser
// in defprops.h

/*
INT16U SetAbsTime(struct cmd *c, struct prop *p);
INT16U SetChar(struct cmd *c, struct prop *p);
INT16U SetConfig(struct cmd *c, struct prop *p);
INT16U SetCore(struct cmd *c, struct prop *p);
INT16U SetFloat(struct cmd *c, struct prop *p);
INT16U SetInteger(struct cmd *c, struct prop *p);

#if FGC_CLASS_ID == 59
INT16U SetRelTime(struct cmd *c, struct prop *p);
#endif

INT16U SetReset(struct cmd *c, struct prop *p);
INT16U SetRterm(struct cmd *c, struct prop *p);
INT16U SetRtermLock(struct cmd *c, struct prop *p);
INT16U SetTerminate(struct cmd *c, struct prop *p);

#if FGC_CLASS_ID != 59
INT16U SetResetFaults(struct cmd *c, struct prop *p);
#endif

#if (FGC_CLASS_ID == 51) || (FGC_CLASS_ID == 53)    // Class 61: Look for set_class.c file
INT16U SetCal(struct cmd *c, struct prop *p);
INT16U SetDefaults(struct cmd *c, struct prop *p);
INT16U SetDevicePPM(struct cmd *c, struct prop *p);
INT16U SetDirectCommand(struct cmd *c, struct prop *p);
INT16U SetFifo(struct cmd *c, struct prop *p);
INT16U SetInternalAdcMpx(struct cmd *c, struct prop *p);
INT16U SetPC(struct cmd *c, struct prop *p);
INT16U SetRef(struct cmd *c, struct prop *p);
#endif
*/

//-----------------------------------------------------------------------------------------------------------


#endif  // SET_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: set.h
\*---------------------------------------------------------------------------------------------------------*/


