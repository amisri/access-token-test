/*!
 *  @file      state_manager.h
 *  @defgroup  FGC3:MCU
 *  @brief     This file contains the functions to support the Power Converter
 *  State Machine
 */

#ifndef STATE_MANAGER_H
#define STATE_MANAGER_H

#ifdef STATE_MANAGER_GLOBALS
#define STATE_MANAGER_VARS_EXT
#else
#define STATE_MANAGER_VARS_EXT extern
#endif

//Includes

#include <cc_types.h>

#include <stdint.h>


// Constants

#define DDOP_CMD_ON             0x01
#define DDOP_CMD_OFF            0x02
#define DDOP_CMD_RESET          0x04
#define DDOP_CMD_POL_POS        0x08
#define DDOP_CMD_POL_NEG        0x10
#define DDOP_CMD_BLOCKING       0x20
#define DDOP_CMD_UNBLOCK        0x40
#define DDOP_CMD_FGCFLT         0x80

// External structures, unions and enumerations

struct dim_channel
{
    uint16_t          channel;
    uint16_t          mask;    
};

struct vs_vars
{
    INT16U              sim_intlks;                     // Property VS.SIM_INTLKS value
    INT16U              active_filter;                  // Property VS.ACTIVE_FILTER value
    INT16U              ext_control;                    //
    INT32U              vsrun_timeout_ms;               // Voltage source startup timeout (ms)
    FP32                vsrun_timeout;                  // Voltage source startup timeout (0.1ms)
    INT16U              req_polarity;                   // Requested polarity switch state
    INT16U              polarity_counter;               // Polarity switch check counter (s)
    INT16U              vsstate_counter;                // VS invalid state test counter
    INT16U              vs_not_rdy_count;               // Number of consecuite cycles with VS_READY not active
    INT32U              vs_ready_timeout;               // VS_READY not active timeout (ms) VS.VS_READY_TIMEOUT

    struct dim_channel  vs_redundancy_warning;          // Voltage source redundancy warning

    struct dim_channel  fabort_unsafe_dig;              // Fast abort unsafe DIM digital signal
    INT16U              fabort_unsafe;                  // 0=FWD Not Present  1=No FWD Fault  2=FWD Fault

    struct dim_channel  fw_diode_dig;                   // Fast abort unsafe DIM digital signal
    INT16U              fw_diode;                       // 0=FWD Not Present  1=No FWD Fault  2=FWD Fault
    INT16U              present;
};

// External variable definitions

STATE_MANAGER_VARS_EXT struct vs_vars vs;                             // Reference variables structure

// External function declarations

/*!
 * This function is the PC State manager task.
 *
 * It is triggered by OSTskResume() in MstTsk() on milliseconds 2, 7, 12
 * and 17, provided the FGC is configured.
 */
void StaTsk(void *);

/*!
 * This function initialises the interface and states related to the power
 * converter.
 */
void StaInit(void);

#endif

// EOF
