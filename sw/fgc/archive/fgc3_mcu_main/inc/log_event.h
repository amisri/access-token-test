/*---------------------------------------------------------------------------------------------------------*\
  File:         log_event.h

  Contents:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef LOG_EVENT_H
#define LOG_EVENT_H

#ifdef LOG_EVENT_GLOBALS
#define LOG_EVENT_VARS_EXT
#else
#define LOG_EVENT_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <fgc_consts_gen.h>
#include <fgc_consts_gen.h>         // for FGC_LOG_EVT_PROP_LEN, FGC_LOG_EVT_ACT_LEN
#include <defconst.h>           // for FGC_MAX_DIMS
#include <property.h>           // for struct prop
#include <mcu_dsp_common.h>     // for struct abs_time_us
#include <fgc/fgc_db.h>         // for struct fgc_db_t

//-----------------------------------------------------------------------------------------------------------

struct log_evt_prop
{
    struct prop    *    prop;                           // Pointer to property
    INT16U              old_value;                      // Old property value
    char        *       name;                           // Pointer to property name in full
    char const     *    symbols[FGC_MAX_SYMLIST_VALUE + 1]; // Table of symbols
};

struct log_evt_rec
{
    struct abs_time_us  timestamp;                      // Timestamp (Unix time + microsecond)
    char                prop[FGC_LOG_EVT_PROP_LEN];     // Property name field
    char                value[FGC_LOG_EVT_VAL_LEN];     // Value field
    char                action[FGC_LOG_EVT_ACT_LEN];    // Action field
};

struct log_evt_vars
{
    INT16U              index;                          // Index of last sample
    INT16U              sample_size;                    // Number of bytes per sample
    fgc_db_t            dim_props_idx;                  // DIM index (0 -> (diag.n_dims-1))
    fgc_db_t            logical_dim;
    INT16U              dim_state;                      // DIM event log state
    fgc_db_t            dim_bank_idx;                   // DIM bank (0-1)
    fgc_db_t            dim_input_idx;                  // DIM input index (0-11)
    fgc_db_t            dim_input_mask;                 // DIM input mask (0x001 - 0x800)
    fgc_db_t            fault_mask[2];                  // DIM bank fault mask
    fgc_db_t            dim_new_data[2];                // New DIM data for 2 banks
    fgc_db_t            dim_old_data[2][FGC_MAX_DIMS];  // Old data for 2 banks for all DIMs
    struct fgc_dimdb_bank   *   dimdb_bank_fp[2];       // Pointer to DimDB bank info
    struct log_evt_rec          dim_log_rec;            // DIM log record
};

//-----------------------------------------------------------------------------------------------------------

// Event log variables structure
LOG_EVENT_VARS_EXT struct log_evt_vars  __attribute__((section("SRAM_ram"))) log_evt;

//-----------------------------------------------------------------------------------------------------------

#endif  // LOG_EVENT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: log_event.h
\*---------------------------------------------------------------------------------------------------------*/
