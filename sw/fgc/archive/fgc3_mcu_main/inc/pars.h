/*!
 *  @file      pars.h
 *  @defgroup  FGC3:MCU:Main
 *  @brief     Parameter parsing Functions.
 */

#ifndef PARS_H
#define PARS_H

#ifdef PARS_GLOBALS
#define PARS_VARS_EXT
#else
#define PARS_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <stdint.h>
#include <property.h>
#include <cmd.h>
#include <dpcom.h>

// External functions

/*!
 * This function is used to convert unsigned 32-bit integers from the command packet.  The delim string
 * will be used in a call to ParsScanToken() to extract the next token from the packet.  If the conversion
 * succeeds, the value will be returned in *value.  For speed reasons, the maximum decimal value that can be
 * converted is 3999999999 even though this is less than the real maximum that can be stored in an INT32U.
 * Hex values can be specified by a leading "0X" or just "X".
 * @param c command packet
 * @param delim delimiter
 * @param value
 * @return errnum:
 *       0                       Number successfuly converted in *value
 *       FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
 *       FGC_BAD_INTEGER         Token contains spaces or non-valid characters
 *       FGC_NO_DELIMITER        End of buffer found before a valid delimiter
 *       FGC_SYNTAX_ERROR        Token buffer full or contains a space before a valid delimiter
 *       FGC_NO_SYMBOL           Token buffer empty
 */
INT16U  ParsScanInteger(struct cmd * c, const char * delim, INT32U * value);


/*!
 * This function parses the standard types: Integers, Floats and RelTimes.
 * It returns a pointer to the result value in 'value'.
 *
 * @param c command packet
 * @param p pointer to property
 * @param par_type
 * @param value
 * @return errnum:
 */
INT16U  ParsScanStd(struct cmd * c, struct prop * p, INT16U par_type, void ** value);


/*!
 *   This function extracts the next token from the command buffer into the command token buffer.
 *
 * A leading space will be ignored.  The token is terminated by any of up to six delimiter characters
 * in the string pointed to by delim.  The nul delimiter has special handling:
 *
 *       delim[0] = 'Y'          Nul delimiter is allowed
 *       delim[0] = 'N'          Nul delimiter is not allowed
 *
 * delim[1] to delim[5] can supply up to five more delimiter characters.
 *
 * For example:
 *
 *       delim                   Delimiter characters
 *       "Y"                     '\0'
 *       "Y "                    '\0' or ' '
 *       "N "                    ' '
 *       "Y.:([ "                '\0' or '.' or ':' or '(' or '[' or ' '
 *
 * The token length must be less than (MAX_CMD_TOKEN_LEN-1) and it may not contain spaces.
 *
 * The token will be nul terminated in the token buffer.  The length of the token is returned in the
 * command structure, along with the delimiter character.
 *
 * @param c         Command structure containing a command buffer and token buffer
 *                  c->token[]              Nul terminated token string
 *                  c->token_delim          Delimiter character found
 *                  c->n_token_chars        Token length
 * @param delim     Delimiter character specifier
 *
 * @return  0                       Token successfully extracted from command buffer into token buffer
 *          FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
 *          FGC_NO_DELIMITER        End of buffer found before a valid delimiter
 *          FGC_SYNTAX_ERROR        Token buffer full or space found before a valid delimiter
 */
INT16U  ParsScanToken(struct cmd * c, const char * delim);


/*!
 * This function extracts a token and sees if it is a settable symbol from the specified symbol list.
 *
 * @param c         Command structure containing the token buffer
 * @param delim     This will be used as the delimiter string for ParsScanToken()
 * @param sym_lst   Pointer to symbol list
 * @param sym_const If no symbol is given then *sym_const will be zero.  If a symbol matches one in the symbol list,
 *  then a pointer to the symbol list record is returned - this contains the sym_idx and constant value.
 *
 * Note:                            The sym_idx in the sym_lst will have FGC_NOT_SETTABLE (0x8000) set if
 *                                  the symbol may not be set.
 *
 * @return 0                               Successful match returned in *sym
 *       FGC_NO_SYMBOL                   No token found
 *       FGC_CMD_RESTARTED               Unexpected first-command packet received (new command starting)
 *       FGC_NO_DELIMITER                End of buffer found before a valid delimiter
 *       FGC_SYNTAX_ERROR                Token buffer full or contains a space before a valid delimiter
 *       FGC_UNKNOWN_SYM                 Token didn't match any symbol with given type
 *       FGC_BAD_PARAMETER               Constant symbol identified but not in symlist
 *       FGC_SYMBOL_IS_NOT_SETTABLE      Symbol is identified and present in the symlist, but is not settable
 */
INT16U  ParsScanSymList(struct cmd * c, const char * delim, const struct sym_lst * sym_lst,
                        const struct sym_lst ** sym_const);


/*!
 * This function is the standard method for setting a property from ASCII values in the command buffer.
 * The following types are supported: AbsTime, RelTime, Float, All integers including properties
 * with symlists.
 *
 */
INT16U  ParsSet(struct cmd * c, struct prop * p);


/*!
 * This function will return a pointer to the next parameter parsed from the command packet.  For integers,
 * floats and reltimes, the MCU gets help from the DSP to pars all the parameters in the packet.  For
 * chars, symlists and abstime, it does the conversion itself.  If the current packet is empty, the function
 * will sleep on the semaphore from the fieldbus or terminal task until a new packet is available.
 *
 * @param c
 * @param p
 * @param pointer to the value converted
 * @return 0                       Valid value is waiting at *value
 *       FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
 *       FGC_NO_SYMBOL           Token buffer empty (c->cmd_end_f set if no more tokens in command)
 *       FGC_SYNTAX_ERROR        Token too long or contains a space before a valid delimiter
 *       FGC_BAD_INTEGER         Token contains non-valid integer characters
 *       FGC_BAD_FLOAT           Token contains non-valid floating point characters
 *       FGC_INVALID_TIME        AbsTime Milliseconds > 999
 *       FGC_UNKNOWN_SYM         Token didn't match any symbol with given type
 *       FGC_BAD_PARAMETER       Constant symbol identified but not in symlist
 *       FGC_OUT_OF_LIMITS       Value is out of min/max limits for the property
 *
 */
INT16U  ParsValueGet(struct cmd * c, struct prop * p, void ** value);

#endif  // PARS_H end of header encapsulation

//EOF

