/*---------------------------------------------------------------------------------------------------------*\
  File:         bgp.h

  Purpose:      FGC MCU Software - Background processing Task

  Author:       Quentin.King@cern.ch

  Notes:        The background task takes the role of an idle task.  It never blocks and is used to do
                background surveillance of the FGC.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef BGP_H      // header encapsulation
#define BGP_H

#ifdef BGP_GLOBALS
#define BGP_VARS_EXT
#else
#define BGP_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <os.h>                 // for OS_STK

//-----------------------------------------------------------------------------------------------------------

void    BgpTsk(void * unused);
void    BgpCheckStack(INT16U tsk_id, OS_STK * stk, INT16U stk_size);

// Platform/class specific functions

/*!
 * Background Processing Loop. It calls BgpPlatform() for platform specific processing.
 */
void BgpLoop(void);
void BgpPlatform(void);
void BgpFilter200ms(void);
void BgpFilter1s(void);
void BgpLogOasisNewSegment(INT32U user);

//-----------------------------------------------------------------------------------------------------------

#endif  // BGP_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: bgp.h
\*---------------------------------------------------------------------------------------------------------*/
