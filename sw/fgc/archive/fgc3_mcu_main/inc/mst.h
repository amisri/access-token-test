/*!
 *  @file      mst.h
 *  @defgroup  FGC3:MCU
 *  @brief     Millisecond actions
 */

#ifndef MST_H      // header encapsulation
#define MST_H

#ifdef MST_GLOBALS
#define MST_VARS_EXT
#else
#define MST_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <mcu_dsp_common.h>

// External structures, unions and enumerations

struct mst_vars
{
    INT32U              isr_start_us;                   // Starting time of the ISR in us
    INT16U              reset;                          // Copy of reset register (to force 16-bit access only)
    INT16U              mode;                           // Readback of MODE register
    INT16U              set_cmd_counter;                // Set command downcounter (to inhibit auto cal)
    INT16U              ms;                             // mstime
    INT16U              ms_mod_5;                       // mstime % 5
    INT16U              ms_mod_10;                      // mstime % 10
    INT16U              ms_mod_20;                      // mstime % 20
    INT16U              ms_mod_100;                     // mstime % 100
    INT16U              ms_mod_200;                     // mstime % 200
    INT16U              s_mod_10;                       // unix_time % 10
    INT16U              s_mod_600;                      // unix_time % 600
    BOOLEAN             dsp_is_standalone;              // DSP is in standalone mode
    uint32_t            dsp_irq_alive_counter;          // Current DSP alive 1 ms IRQ counter.
    FP32   dsp_freq_khz;                   // Property FGC.DSP_FREQ_KHZ: DSP frequency evaluated every second
    INT16U dsp_reg_run_f;                  // DSP regulation state on the previous iteration (running or not)
    struct                                              // CPU usage in % (calculated at 1Hz)
    {
        INT16U          mcu;                            // MCU CPU usage in % (calculated at 1Hz)
        INT16U          dsp;                            // DSP CPU usage in % (calculated at 1Hz)
        INT16U          dsp_ext_counter;                // DSP CPU usage in % (calculated at 1Hz)
    } cpu_usage;
    INT16U duration;                       //     MstTsk execution duration (FGC2: in 0.5us ticks; FGC3: in us)
    INT16U max_duration[20];               // Max MstTsk execution duration (FGC2: in 0.5us ticks; FGC3: in us)
    struct abs_time_us  time;                           // Timestamp of current millisecond
    struct abs_time_us  prev_time;                      // Timestamp of previous millisecond
    struct abs_time_us  time10ms;                       // Timestamp of last start of 10ms period
};

// External variable definitions

MST_VARS_EXT struct mst_vars    mst;                    // Millisecond task global structure

// Class specific function declarations

void MstClassProcess(void);
void MstClassPublishData(void);
void MstClassLog(void);

// External function declarations

/*!
 * This function is the Millisecond Task.
 *
 * During initialisation, it auto-suspends until resumed by the Idle Task. Only
 * then does it enable the GPT to start normal synchronous operation. The main
 * loop is controlled by IsrMst via an OSTskResume() call.
 */
void MstTsk(void *);

#endif

// EOF
