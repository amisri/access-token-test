/*!
 *  @file     task_trace.h
 *  @defgroup FGC3:MCU
 *  @brief    Used for tracing the various MCU tasks
 */

#ifndef FGC_TASK_TRACE_H
#define FGC_TASK_TRACE_H

#ifdef FGC_TASK_TRACE_GLOBALS
#define FGC_TASK_TRACE_VARS_EXT
#else
#define FGC_TASK_TRACE_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <tsk.h>

// External structures, unions and enumerations

typedef struct tsk_trace
{
    uint32_t sem_info[FGC_NUM_SEMAPHORES]; // FGC.DEBUG.SEM_INFO
    uint8_t trace[MCU_NB_OF_TASKS];            // FGC.DEBUG.TASK.TRACE
    uint8_t iter_cnt[MCU_NB_OF_TASKS];         // FGC.DEBUG.TASK.ITERS
} task_trace_t;

// External variable definitions

FGC_TASK_TRACE_VARS_EXT task_trace_t __attribute__((section("SRAM_ram")))  task_trace;

// External function declarations

static INLINE void TskTraceInc(void);
static INLINE void TskTraceReset(void);

// External function definitions

static INLINE void TskTraceInc(void)
{
    task_trace.trace[os.curr_tid]++;
}

static INLINE void TskTraceReset(void)
{
    task_trace.trace[os.curr_tid] = 0;
    task_trace.iter_cnt[os.curr_tid]++;
}

#endif

// EOF
