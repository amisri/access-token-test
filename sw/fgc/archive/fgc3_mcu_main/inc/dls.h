/*!
 *  @file     dls.h
 *  @defgroup FGC3:MCU main
 *  @brief    Dallas bus functions
 *
 * Notes:   This file contains the function for the Dallas Task and all the functions related to
 *          reading the IDs and temperatures from the Dallas devices controlled by the C62 microcontroller.
 */

#ifndef DLS_H      // header encapsulation
#define DLS_H

#ifdef DLS_GLOBALS
#define DLS_VARS_EXT
#else
#define DLS_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>           // basic typedefs
#include <stdint.h>
#include <defconst.h>           // for FGC_MAX_DIMS, FGC_ID_N_BRANCHES, FGC_MAX_T_PROBES
#include <fgc_consts_gen.h>     // for FGC_C62_ERR_RSP_LEN
#include <microlan_1wire.h>     // for MICROLAN_NETWORK_ALL_ELEMENTS_MAX, struct TBranchSummary, struct TMicroLAN_element_summary
#include <property.h>           // for prop_size_t

// Constants

/*!  Buffer to hold slow (background) command error responses */
#define M16C62_MAX_EXPECTED_RESPONSE     12

/*! Maximum number of external IDs that can be sent to C62 */
#define DLS_EXTERNAL_ID_MAX               6

// Dallas bus constants

#define DLS_ISR_CMD_ERR                 101
#define DLS_ISR_CMD_TIMEOUT             102
#define DLS_BG_CMD_TIMEOUT              103


#define DLS_ERROR_LOCATION_10   10
#define DLS_ERROR_LOCATION_20   20
#define DLS_ERROR_LOCATION_30   30
#define DLS_ERROR_LOCATION_39   39
#define DLS_ERROR_LOCATION_40   40
#define DLS_ERROR_LOCATION_60   60      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_70   70      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_80   80      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_90   90      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_100  100      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_110  110      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_120  120      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_130  130      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_140  140
#define DLS_ERROR_LOCATION_150  150      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_160  160      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_170  170      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_180  180      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_190  190
#define DLS_ERROR_LOCATION_200  200


#define REPORT_INVALID_TEMPERATURE      TRUE


// External structures, unions and enumerations

/*! dls.state machine constants. Use enum to ensure that states are sequential */
enum
{
    DLS_START,
    DLS_AT_MAINPROG,
    DLS_READID,
    DLS_READEXTID,
    DLS_CONVTEMPA,
    DLS_READTEMPA,
    DLS_CONVTEMPB,
    DLS_READTEMPB,
    DLS_CONVTEMPL,
    DLS_READTEMPL,
    DLS_LOGTEMPS,
};

/*! Dallas ID variables */
struct id_vars
{
    INT16U n_devs;                                              // Number of devices in devs[] array
    INT16U start_in_elements_summary[FGC_ID_N_BRANCHES];        // Index of 1st device per branch in dev array
    INT16U to_summary_sorted_by_codebar[MICROLAN_NETWORK_ALL_ELEMENTS_MAX]; // Sorted array to Dallas device information
    struct TBranchSummary logical_paths_info[FGC_ID_N_BRANCHES];
    struct TMicroLAN_element_summary * element_info_ptr[FGC_ID_N_BRANCHES];
    struct TMicroLAN_element_summary devs[MICROLAN_NETWORK_ALL_ELEMENTS_MAX]; // Dallas device information array
    struct prop * prop_temp[MICROLAN_NETWORK_ALL_ELEMENTS_MAX]; // Link to temperature properties
    union TMicroLAN_unique_serial_code external_ids[DLS_EXTERNAL_ID_MAX];
    uint8_t external_id_num;

};

/*! */
struct Trsp
{
    INT8U               global[FGC_C62_ERR_RSP_LEN];    //!< global response (for all branches)
    INT8U               local [FGC_C62_ERR_RSP_LEN];    //!< local branch response (logical_path=4)
    INT8U               meas_a[FGC_C62_ERR_RSP_LEN];    //!< measurement A branch response (logical_path=1)
    INT8U               meas_b[FGC_C62_ERR_RSP_LEN];    //!< measurement B branch response (logical_path=3)
};

/*! */
struct dls_vars
{
    BOOLEAN             active_f;                       //!< Dallas buses read flag
    BOOLEAN             temp_fail_f;                    //!< Dallas temperature read failed
    INT16U              inhibit_f;                      //!< Dallas temperature read inhibit (DLS.INHIBIT)
    INT16U              state;                          //!< Dallas communications state
    INT16U              timeout_cnt;                    //!< Command timeout down counter
    INT16U              logical_path;                   //!< Branch idx of branch been scanned for temps
    INT32U              num_errors;                     //!< Error counter
    INT32U              error_time;                     //!< Abstime of last error
    struct Trsp         rsp;
};


// Temperature measurement variables

// this temperatures are 16 bits raw value form the sensor
// and are converter to FP32 by GetTemp() that make a conversion when access via a property

/*! */
struct TTemperatureValues
{
    // for the client level and must therefore be size 4
    INT32U              unix_time;                      //!< Acquisition unixtime
    BOOLEAN             thour_f;                        //!< Acquisition should be logged in THOUR
    BOOLEAN             tday_f;                         //!< Acquisition should be logged in THOUR

    struct temperatures_to_logbook                      //!< Temperature log structure
    {
        INT16U          fgc_in;                         //!< FGC inlet temperature
        INT16U          fgc_out;                        //!< FGC outlet temperature
        INT16U          dcct_a;                         //!< DCCT A electronics temperature (if present)
        INT16U          dcct_b;                         //!< DCCT B electronics temperature (if present)
    } log;

    // temp.fgc.in and temp.fgc.out are linked with the corresponding temperature sensor
    // via the assignment in components.xml with
    //  temp_prop = "TEMP.FGC.IN"
    // and
    //  temp_prop = "TEMP.FGC.OUT"
    // (this information is extracted from CompDB by the MCU program)

    struct
    {
        INT16U          in;                             //!< FGC inlet temperature
        INT16U          out;                            //!< FGC outlet temperature
        INT16U          delta;                          //!< Difference between FGC outlet and inlet temps
    } fgc;
    BOOLEAN             dcct_temp_available[2];         //!< DCCT temperature measurements expected flags

    struct
    {
        INT16U          mod;                            //!< External ADC modulator temperature
        INT16U          psu;                            //!< External ADC PSU temperature
    } ext_adc[2];

    struct
    {
        INT16U          head;                           //!< DCCT head temperature
        INT16U          elec;                           //!< DCCT electronics temperature
    } dcct[2];
};

/*! Structure for barcode properties */
struct barcode_vars
{

    struct
    {
        INT8S     *     L         [FGC_MAX_L_BARCODES]; //!< Local barcodes
        INT8S     *     V         [FGC_MAX_V_BARCODES]; //!< Voltage source barcodes
        INT8S     *     A         [FGC_MAX_A_BARCODES]; //!< Measurement channel A barcodes
        INT8S     *     B         [FGC_MAX_B_BARCODES]; //!< Measurement channel B barcodes
        INT8S     *     C         [FGC_MAX_C_BARCODES]; //!< Crate barcodes
    }                   bus;

    struct
    {
        INT8U           cassette  [FGC_ID_BARCODE_LEN]; //!< FGC cassette barcode
        INT8U           ana       [FGC_ID_BARCODE_LEN]; //!< FGC Analogue Interface board barcode
    }                   fgc;
    struct
    {
        INT8U           head      [FGC_ID_BARCODE_LEN]; //!< DCCT head barcode
        INT8U           elec      [FGC_ID_BARCODE_LEN]; //!< DCCT electronics barcode
    }                   dcct[2];
    struct
    {
        INT8U           cassette  [FGC_ID_BARCODE_LEN]; //!< ADC22 cassette barcode
        INT8U           mod       [FGC_ID_BARCODE_LEN]; //!< ADC22 Delta-Sigma modulator barcode
        INT8U           psu       [FGC_ID_BARCODE_LEN]; //!< ADC22 PSU barcode
    }                   adc22[2];
};

/*! */
struct barcode_n_els
{

    struct
    {
        prop_size_t     L;                              //!< Local barcodes
        prop_size_t     V;                              //!< Voltage source barcodes
        prop_size_t     A;                              //!< Measurement channel A barcodes
        prop_size_t     B;                              //!< Measurement channel B barcodes
        prop_size_t     C;                              //!< Crate barcodes
    } bus;

    struct
    {
        prop_size_t     cassette;                       //!< FGC cassette barcode
        prop_size_t     ana;                            //!< FGC Analogue Interface board barcode
    } fgc;
    struct
    {
        prop_size_t     head;                           //!< DCCT head barcode
        prop_size_t     elec;                           //!< DCCT electronics barcode
    } dcct[2];

    struct
    {
        prop_size_t     cassette;                       //!< ADC22 cassette barcode
        prop_size_t     mod;                            //!< ADC22 Delta-Sigma modulator barcode
        prop_size_t     psu;                            //!< ADC22 PSU barcode
    } adc22[2];
};


/*!
 * This function is the Dallas task.  It is woken by the MstTsk() at the start of every 100ms period in
 * order to read the temperatures from the M16C62.  A temperature reading is make once per 10s, but the process
 * takes several seconds and the task polls at 10Hz to know when the new measurements are ready to be
 * read out from the M16C62.  It will log the results at 0.1Hz in the THOUR log and once per 10 minutes in the
 * TDAY log.
 *
 * When the task first runs (or after a S ID RESET command), the task will also read out the Dallas IDs of
 * all the connected devices and will analyse them to see if all the components that are expected for the
 * type of power converter identified by the device name are in fact present.
 */
void DlsTsk(void *);

/*!
 * This function will translate the supplied device 1-wire ID into an ASCII string so that it can be
 * printed with the most significant byte (CRC) first.
 *
 * @param devid ID
 * @param id_string Formatted string
 * @return
 */
char * DlsIdString(const union TMicroLAN_unique_serial_code * devid, char * id_string);


/*!
 * This function will inject a dallas ID into the ID management.
 * This allow to read a remote ID (basically via SCIVS bus).
 * Note: this is probably a bad design, routing 1 wire would make the system simpler and more consistent...
 * This function must be called at start up, before the initial reading of IDs by the dallas task
 *
 * @param devid ID to be send to dallas ID management
 */
void DlsSetId(union TMicroLAN_unique_serial_code ext_id);


// Global variables

/*! Dallas task variables structure */
DLS_VARS_EXT struct dls_vars __attribute__((section("SRAM_ram"))) dls;

/*! Dallas ID variables structure */
DLS_VARS_EXT struct id_vars __attribute__((section("SRAM_ram"))) id;

/*! Dallas Temperature variables structure */
DLS_VARS_EXT struct TTemperatureValues __attribute__((section("SRAM_ram"))) temp;

/*! Barcode variables structure */
DLS_VARS_EXT struct barcode_vars __attribute__((section("SRAM_ram"))) barcode;

/*! Barcode property n_els structure */
DLS_VARS_EXT struct barcode_n_els __attribute__((section("SRAM_ram"))) barcode_n_els;

#endif  // DLS_H end of header encapsulation

// EOF
