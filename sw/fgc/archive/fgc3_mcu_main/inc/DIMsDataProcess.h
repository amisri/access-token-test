/*!
 *  @file     DIMsDataProcess.h
 *
 *
 */

#ifndef DIMSDATAPROCESS_H       // header encapsulation
#define DIMSDATAPROCESS_H

#ifdef DIMSDATAPROCESS_GLOBALS
#define DIMSDATAPROCESS_VARS_EXT
#else
#define DIMSDATAPROCESS_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>           // basic typedefs
#include <cmd.h>                // for struct cmd
#include <defconst.h>           // for FGC_MAX_DIMS, FGC_N_DIM_ANA_CHANS, FGC_MAX_DIM_BUS_ADDR
#include <dpcom.h>              // for struct abs_time_us
#include <property.h>           // for struct prop
#include <qspi_bus.h>           // for QSPI_BRANCHES

// Constants

#define DEBUG_LOG_DIPS  1                       //! 0=Log first DIM on branch A, 1=Log DIPS board
#define DIPS_MASK       0x00010001              //! DIPS boards mask (0 and 16)

//! array index for flat_qspi_board_is_present_packed[]
#define DIMS_IN_BOTH_CYCLES     0
#define DIMS_IN_ACTUAL_CYCLE    1
#define DIMS_IN_PREVIOUS_CYCLE  2

/*!
 * @brief For list (0..19) process
 */
struct TDimCollectionInfo
{
    /*!
     * @brief DIAG.BUS_ADDRESSES
     *
     * here the flat qspi bus board number can be
     * 0x00..0x1F are real physical DIM boards (0x00..0x0F on branch A, 0x10..0x1F on branch B)
     * 0x80   b7=1 means a virtual DIM (composite)
     * 0xFF   means not used (empty)
     */
    INT32U        flat_qspi_board_number[FGC_MAX_DIMS];

    /*!
     * analogue register converted to physical values
     * DIAG.ANASIGS
     */
    FP32          analogue_in_physical[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];

    BOOLEAN       unlatched_trigger_fired;
    BOOLEAN       dims_are_valid;

    INT32U        trig_us[FGC_MAX_DIMS];                          //!< DIAG.TRIGUS
    INT32U        trig_s[FGC_MAX_DIMS];

    INT32U        evt_log_state[FGC_MAX_DIMS];                    //!< Event log state for DIMs

    FP32          gains  [FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];     //!< DIAG.GAIN
    FP32          offsets[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];     //!< DIAG.OFFSET
};


struct TQspiMiscellaneousInfo
{
    INT32U      expected[QSPI_BRANCHES];        //!< DIAG.DIMS_EXPECTED
    INT32U      detected[QSPI_BRANCHES];        //!< DIAG.DIMS_DETECTED
    INT32U      expected_detected_mismatches;   //!< DIAG.DIMS_EXP_ERRS
    BOOLEAN     expected_retrieved;             //!< TRUE when the expected DIMs have been retrieved.

    INT32U      flat_qspi_non_valid_counts[FGC_MAX_DIM_BUS_ADDR];   //!< DIAG.DIM_SYNC_FLTS

    BOOLEAN     freeze_all_dim_logs;
    BOOLEAN     relaunch_dim_logging;           //!< restart DIM logging
};

struct TDimLogEntryStatus
{
    INT16U              idx;                    //!< last sample index
    struct abs_time_us  time;                   //!< Time of last sample in the buffer
    BOOLEAN             dont_log;               //!< Logging stopping flag
    INT16U              samples_to_acq;         //!< Samples still to acquire
};

// External function declarations

/*!
 *
 */
void InitQspiBus(void);

/*!
 * This function will be called from the 1 millisecond task at the start of every millisecond.
 * It must quickly read any data waiting from a previous scan, and trigger the next scan if required.
 */
void QSPIbusStateMachine(void);

/*!
 *    Get DIM log properties.
 *    These can only be acquired via the fieldbus with the BIN get option.
 *
 *    g log.evt
 */
INT16U DimGetDimLog(struct cmd * c, struct prop * p);

/*!
 *  Displays:     DIM log properties
 */
INT16U DimGetDimNames(struct cmd * c, struct prop * p);

/*!
 *  Displays:     DIM log properties
 */
INT16U DimGetAnaLbls(struct cmd * c, struct prop * p);

/*!
 *  Displays:     DIM log properties
 */
INT16U DimGetDigLbls(struct cmd * c, struct prop * p, INT16U bank);

/*!
 *  Displays:     DIM analogue channel properties
 */
INT16U DimGetAna(struct cmd * c, struct prop * p);

/*!
 *  Displays:     DIM digital channel properties
 */
INT16U DimGetDig(struct cmd * c, struct prop * p);

/*!
 *  Displays:     DIM digital channels in Fault
 */
INT16U DimGetFaults(struct cmd * c, struct prop * p);


DIMSDATAPROCESS_VARS_EXT struct TQspiMiscellaneousInfo __attribute__((section("SRAM_ram"))) qspi_misc;
DIMSDATAPROCESS_VARS_EXT struct TDimCollectionInfo __attribute__((section("SRAM_ram"))) dim_collection;
DIMSDATAPROCESS_VARS_EXT struct TDimLogEntryStatus __attribute__((section("SRAM_ram")))
dbg_dim_log_status[REQUESTS_FOR_DIM_LOG];

#endif  // DIMSDATAPROCESS_H end of header encapsulation

// EOF
