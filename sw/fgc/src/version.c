/*!
 *  @file      version.c
 *  @brief     FGC Software version - this will define the version struct that is automatically
 *             generated in version_def.txt by version_info.sh
 */

// Includes

#include "version.h"

#include <version_def.txt>


// EOF
