/*!
 *  @file     pc_state.h
 *  @defgroup FGC3
 *  @brief    Provides functions to discern the current state of the
 *  pc state machine.
 */

#ifndef FGC_PC_STATE_H
#define FGC_PC_STATE_H

#ifdef PC_STATE_GLOBALS
#define PC_STATE_VARS_EXT
#else
#define PC_STATE_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>
#include <defconst.h>

// Constants

#if defined(__RX610__) || defined(__HC16__)
#include <fbs_class.h>
#define CURRENT_PC_STATE  STATE_PC
#else
#include <main.h>
#define CURRENT_PC_STATE  state_pc
#endif

// External function declarations

/*!
 * Returns true if the current state is equal to the the state value in
 * the argument.
 *
 * @param state State to compare against.
 *
 * @retval True if the current state is equal to the state. False otherwise.
 */
static INLINE BOOLEAN PcStateEqual(INT32U state);

/*!
 * Returns true if the current state is above the state value in the argument.
 *
 * @param state State to compare against.
 *
 * @retval True if the current state is above state. False otherwise.
 */
static INLINE BOOLEAN PcStateAbove(INT32U state);

/*!
 * Returns true if the current state is above or equal to the state
 * value in the argument.
 *
 * @param state State to compare against.
 *
 * @retval True if the current state is above or equal to state. False otherwise.
 */
static INLINE BOOLEAN PcStateAboveEqual(INT32U state);

/*!
 * Returns true if the current state is below the state value in the argument.
 *
 * @param state State to compare against.
 *
 * @retval True if the current state is below state. False otherwise.
 */
static INLINE BOOLEAN PcStateBelow(INT32U state);

/*!
 * Returns true if the current state is below or equal to the state value in
 * the argument.
 *
 * @param state State to compare against.
 *
 * @retval True if the current state is below or equal to state. False otherwise.
 */
static INLINE BOOLEAN PcStateBelowEqual(INT32U state);

/*!
 * Returns true if the state1 is below to state2.
 *
 * @param state1 State to compare against.
 * @param state2 State to compare against.
 *
 * @retval True if state1 is below to state2. False otherwise.
 */
static INLINE BOOLEAN PcStateCmpBelow(INT32U state1, INT32U state2);

/*!
 * Returns true if the state1 is below or equal to state2.
 *
 * @param state1 State to compare against.
 * @param state2 State to compare against.
 *
 * @retval True if state1 is below or equal to state2. False otherwise.
 */
static INLINE BOOLEAN PcStateCmpBelowEqual(INT32U state1, INT32U state2);

/*!
 * Returns true if the state1 is above to state2.
 *
 * @param state1 State to compare against.
 * @param state2 State to compare against.
 *
 * @retval True if state1 is above to state2. False otherwise.
 */
static INLINE BOOLEAN PcStateCmpAbove(INT32U state1, INT32U state2);

/*!
 * Returns true if the state1 is equal to state2.
 *
 * @param state1 State to compare against.
 * @param state2 State to compare against.
 *
 * @retval True if state1 is equal to state2. False otherwise.
 */
static INLINE BOOLEAN PcStateCmpEqual(INT32U state1, INT32U state2);

/*!
 * Returns true if the state1 is above or equal to state2.
 *
 * @param state1 State to compare against.
 * @param state2 State to compare against.
 *
 * @retval True if state1 is above or equal to state2. False otherwise.
 */
static INLINE BOOLEAN PcStateCmpAboveEqual(INT32U state1, INT32U state2);

/*!
 * Returns true if the current state is associated to the converter being off.
 *
 * @retval True if the current state is associated to the converter being off.
 * False otherwise.
 */
static INLINE BOOLEAN PcStateOff(void);

/*!
 * Returns true if the current state is associated to the converter being on.
 *
 * @retval True if the current state is associated to the converter being on.
 * False otherwise.
 */
static INLINE BOOLEAN PcStateOn(void);

// External variable definitions

PC_STATE_VARS_EXT INT8U state_logical_order[]
#ifdef PC_STATE_GLOBALS
=
{
    0,   // FGC_PC_FLT_OFF
    1,   // FGC_PC_OFF
    2,   // FGC_PC_FLT_STOPPING
    3,   // FGC_PC_STOPPING
    4,   // FGC_PC_STARTING
    6,   // FGC_PC_SLOW_ABORT
    7,   // FGC_PC_TO_STANDBY
    8,   // FGC_PC_ON_STANDBY
    9,   // FGC_PC_IDLE
    10,  // FGC_PC_TO_CYCLING
    11,  // FGC_PC_ARMED
    12,  // FGC_PC_RUNNING
    13,  // FGC_PC_ABORTING
    14,  // FGC_PC_CYCLING
    15,  // FGC_PC_POL_SWITCHING
    5,   // FGC_PC_BLOCKING
    16,  // FGC_PC_ECONOMY
    17   // FGC_PC_DIRECT
}
#endif
;

PC_STATE_VARS_EXT const INT32U state_off
#ifdef PC_STATE_GLOBALS
=
{
    ((1 << FGC_PC_FLT_OFF)      |
    (1 << FGC_PC_OFF)           |
    (1 << FGC_PC_FLT_STOPPING)  |
    (1 << FGC_PC_STOPPING)      |
    (1 << FGC_PC_STARTING)      |
    (1 << FGC_PC_POL_SWITCHING) |
    (1 << FGC_PC_BLOCKING))
}
#endif
;

// External function definitions

static INLINE BOOLEAN PcStateEqual(INT32U state)
{
    return (state_logical_order[CURRENT_PC_STATE] == state_logical_order[state]);
}

static INLINE BOOLEAN PcStateAbove(INT32U state)
{
    return (state_logical_order[CURRENT_PC_STATE] > state_logical_order[state]);
}

static INLINE BOOLEAN PcStateAboveEqual(INT32U state)
{
    return (state_logical_order[CURRENT_PC_STATE] >= state_logical_order[state]);
}

static INLINE BOOLEAN PcStateBelow(INT32U state)
{
    return (state_logical_order[CURRENT_PC_STATE] < state_logical_order[state]);
}

static INLINE BOOLEAN PcStateBelowEqual(INT32U state)
{
    return (state_logical_order[CURRENT_PC_STATE] <= state_logical_order[state]);
}

static INLINE BOOLEAN PcStateOff(void)
{
    return (0 != ((1 << CURRENT_PC_STATE) & state_off));
}

static INLINE BOOLEAN PcStateOn(void)
{
    return (!PcStateOff());
}

static INLINE BOOLEAN PcStateCmpBelow(INT32U state1, INT32U state2)
{
    return (state_logical_order[state1] < state_logical_order[state2]);
}

static INLINE BOOLEAN PcStateCmpBelowEqual(INT32U state1, INT32U state2)
{
    return (state_logical_order[state1] <= state_logical_order[state2]);
}

static INLINE BOOLEAN PcStateCmpAbove(INT32U state1, INT32U state2)
{
    return (state_logical_order[state1] > state_logical_order[state2]);
}

static INLINE BOOLEAN PcStateCmpAboveEqual(INT32U state1, INT32U state2)
{
    return (state_logical_order[state1] >= state_logical_order[state2]);
}

static INLINE BOOLEAN PcStateCmpEqual(INT32U state1, INT32U state2)
{
    return (state_logical_order[state1] == state_logical_order[state2]);
}

#endif

// EOF
