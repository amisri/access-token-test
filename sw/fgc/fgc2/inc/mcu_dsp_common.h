/*---------------------------------------------------------------------------------------------------------*\
  File:         mcu_dsp_common.h

  Purpose:      This file defines the structures shared between the MCU program and DSP program
                that are common to all software classes, except dpcom definitions


                ********* If this file is changed - remember to recompile the DSP library *********

\*---------------------------------------------------------------------------------------------------------*/

#ifndef MCU_DSP_COMMON_H      // header encapsulation
#define MCU_DSP_COMMON_H

#ifdef MCU_DSP_COMMON_GLOBALS
#define MCU_DSP_COMMON_VARS_EXT
#else
#define MCU_DSP_COMMON_VARS_EXT extern
#endif

#include <cc_types.h>
#include <stdbool.h>
#include <definfo.h>            // for FGC_CLASS_ID, FGC_PLATFORM_ID
#include <float.h>              // For FLT_MAX

// /!\ Order dependent: FGC_FIELDBUS_RSP_PKT_LEN is from first included (ether if defined, fip otherwise)

#include <fgc_fip.h>

//-----------------------------------------------------------------------------------------------------------

// Shared Constants

#define DSP_STARTED             1324354657              // Magic number for dpcls->dsp.started

#define PI                      3.141592653             // PI
#define TWO_PI                  6.283185307             // Two PI
#define TIME_NEVER              0xFFFFFFFF              // End of time

#define PACKET_TYPE_NONE        0
#define PKT_FLOAT               1                       // Command packet types
#define PKT_INT                 2
#define PKT_RELTIME             3                       // RELTIME is only used by Class 59
#define PKT_INTU                4
#define PKT_POINT               5

#define PAR_NO_INT              0x80000000
#define PAR_NO_FLOAT            0xFFFFFFFF              // No float token (NaN)

// Master clock

#define PLD_MASTER_CLOCK_KHZ        (2000L)                 // PLD master clock :  2 MHz

// Property block

#define PROP_BLK_SIZE           256                     // Property block size in bytes (must be a power of 2)
#define PROP_BLK_SIZE_W         (PROP_BLK_SIZE/2)       // Property block size in words
#define PROP_BLK_SIZE_LW        (PROP_BLK_SIZE/4)       // Property block size in long words

#define PROP_DSP_RSP_OK         0                       // DSP response to a property GET/SET: OK
#define PROP_DSP_RSP_ERR_NELEM  1                       // DSP response to a property GET/SET: NULL pointer to n_elements
#define PROP_DSP_RSP_ERR_BUF    2                       // DSP response to a property GET/SET: NULL buffer pointer

#define PFLOAT_LEN              16                      // Print float buffer size (bytes)

#define DIM_EVT_NONE            0                       // DIM event log state: nothing to do
#define DIM_EVT_TRIG            1                       // DIM event log state: log all
#define DIM_EVT_TRIGGED         2                       // DIM event log state: nothing to do (! Set by the MCU)
#define DIM_EVT_WATCH           3                       // DIM event log state: watch for new faults

#define PLL_QTICS_PER_MS        (PLD_MASTER_CLOCK_KHZ << 16)    // Required by MCU and DSP

#define DIAG_DEBUG_TO_ACQ       64                      // Post trig debug samples (64 = 8 * 8 words)

#define DB_DSP_MEM_LEN          32                      // Long words

// Command packet

#define MAX_CMD_PKT_LEN         FGC_FIP_RSP_PKT_LEN     // Command packet buffer length in bytes (for MCU)
#define MAX_CMD_PKT_LEN_LW      (FGC_FIP_RSP_PKT_LEN/4) // Command packet buffer length in long words (for DSP)
#define MAX_CMD_PKT_PARS        (FGC_FIP_RSP_PKT_LEN/2) // Max parameters that can be in one command packet
#define MAX_CMD_TOKEN_LEN       32                      // Max command token length in characters

// DSP Panic

#define RUNCODE_DSP_PANIC       0xDEAD              // Runcode use to identify that a DSP panic occurred.
#define RUNCODE_DSP_ISR_ERROR   0x0001              // Runcode use to identify a DSP ISR overrun.
#define RUNCODE_DSP_ISR_ERROR2  0x0002              // Runcode use to identify a DSP ISR overrun.
#define RUNCODE_DSP_ISR_ERROR3  0x0003              // Runcode use to identify a DSP ISR overrun.

// Float formats support by the DSP print float

enum FGC_float_fmt
{
    FLOAT_FMT_NONE,
    FLOAT_FMT_REGULAR,
    FLOAT_FMT_NEAT,
    FLOAT_FMT_TABLE_TIME,
    FLOAT_FMT_COUNT
};

//-----------------------------------------------------------------------------------------------------------
// Macros to assert that a floating-point value is neither one of the two infinities, or a NAN.
//

// 32-bit floating-points (float)

#ifdef FLT_MAX
#define IsFiniteFloat(x)  ( (x) >= -FLT_MAX && (x) <= FLT_MAX )
#endif

// 64-bit floating-points (double)

#ifdef DBL_MAX
#define IsFiniteDouble(x) ( (x) >= -DBL_MAX && (x) <= DBL_MAX )
#endif

//-----------------------------------------------------------------------------------------------------------
// Absolute time structures and basic operations
//

// Absolute time with microsecond precision

struct abs_time_us                                      // Absolute time structure (microsecond resolution)
{
    INT32U              unix_time;                      // Seconds since 1970
    INT32U              us_time;                        // Microseconds within the second
};

// Is TIME_A greater than, strictly greater than or equal to TIME_B?

#define AbsTimeUs_IsGreaterThan(time_a, time_b)         ((time_a.unix_time >  time_b.unix_time) || ((time_a.unix_time == time_b.unix_time) && (time_a.us_time >= time_b.us_time)))
#define AbsTimeUs_IsStrictlyGreaterThan(time_a, time_b) ((time_a.unix_time >  time_b.unix_time) || ((time_a.unix_time == time_b.unix_time) && (time_a.us_time >  time_b.us_time)))
#define AbsTimeUs_IsEqual(time_a, time_b)               ((time_a.unix_time == time_b.unix_time) && (time_a.us_time == time_b.us_time))

// Is TIME_A lower than, strictly lower than TIME_B?

#define AbsTimeUs_IsLowerThan(time_a, time_b)            AbsTimeUs_IsGreaterThan(time_b, time_a)
#define AbsTimeUs_IsStrictlyLowerThan(time_a, time_b)    AbsTimeUs_IsStrictlyGreaterThan(time_b, time_a)


// Absolute time with millisecond precision

struct abs_time_ms                                      // Absolute time structure (millisecond resolution)
{
    INT32U              unix_time;                      // Seconds since 1970
    INT32U              ms_time;                        // Milliseconds within the second
};


// Is TIME_A greater than, strictly greater than or equal to TIME_B?

#define AbsTimeMs_IsGreaterThan(time_a, time_b)         ((time_a.unix_time >  time_b.unix_time) || ((time_a.unix_time == time_b.unix_time) && (time_a.ms_time >= time_b.ms_time)))
#define AbsTimeMs_IsStrictlyGreaterThan(time_a, time_b) ((time_a.unix_time >  time_b.unix_time) || ((time_a.unix_time == time_b.unix_time) && (time_a.ms_time >  time_b.ms_time)))
#define AbsTimeMs_IsEqual(time_a, time_b)               ((time_a.unix_time == time_b.unix_time) && (time_a.ms_time == time_b.ms_time))

// Is TIME_A lower than, strictly lower than TIME_B?

#define AbsTimeMs_IsLowerThan(time_a, time_b)            AbsTimeMs_IsGreaterThan(time_b, time_a)
#define AbsTimeMs_IsStrictlyLowerThan(time_a, time_b)    AbsTimeMs_IsStrictlyGreaterThan(time_b, time_a)

// Difference between two absolute times

INT32S AbsTimeMsDiff(struct abs_time_ms const * const abs_time1,
                     struct abs_time_ms const * const abs_time2);
INT32S AbsTimeUsDiff(struct abs_time_us const * const abs_time1,
                     struct abs_time_us const * const abs_time2);

// Add a delay to an absolute time

void AbsTimeUsAddDelay(struct abs_time_us * abs_time, INT32U delay_us);
void AbsTimeMsAddDelay(struct abs_time_ms * abs_time, INT32U delay_ms);
void AbsTimeUsAddOffset(struct abs_time_us * abs_time, INT32S offset);
void AbsTimeMsAddOffset(struct abs_time_ms * abs_time, INT32S offset);

void AbsTimeUsAddLongDelay(struct abs_time_us * abs_time, INT32S delay_second, INT32U delay_us);
void AbsTimeMsAddLongDelay(struct abs_time_ms * abs_time, INT32S delay_second, INT32U delay_ms);

// Convert from absolute time in us to absolute time in ms and the opposite

void AbsTimeCopyFromMsToUs(const struct abs_time_ms * abs_time, struct abs_time_us * converted_abs_time);
void AbsTimeCopyFromUsToMs(const struct abs_time_us * abs_time, struct abs_time_ms * converted_abs_time);

// Compare absolute times

bool AbsTimeLessThanMs(struct abs_time_ms const * time1, struct abs_time_ms const * time2);
bool AbsTimeMoreThanMs(struct abs_time_ms const * time1, struct abs_time_ms const * time2);
bool AbsTimeMoreThanEqualMs(struct abs_time_ms const * time1, struct abs_time_ms const * time2);

//-----------------------------------------------------------------------------------------------------------

#endif  // MCU_DSP_COMMON_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: mcu_dsp_common.h
\*---------------------------------------------------------------------------------------------------------*/
