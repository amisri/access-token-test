/*---------------------------------------------------------------------------------------------------------*\
  File:         dpcom.h

  Purpose:      This file defines the structures shared between the MCU program and DSP program
                for dual port communication.

  MCU Notes:    The top 16KB of SR0 are writable by the DSP.
                Only 10KB of the DPRAM zone are used (0xD800 - 0xEFFF) with the DPCOM area taking
                4KB from 0xD800 - 0xE7FF.

  DSP Notes:    The whole of SR0 is addressed using STROBE 1, so the 10KB appear at 0x903600

                FGC2 only: Floating point values in the dpram are in IEEE32 format and must be converted
                by the DSP to/from TI's floating point format.

  FGC3 Notes:

                The dual port ram is implemented inside the xilinx
                and can be accessed by the DSP tms320c6727 and the MCU Rx610 (or the M32C87)


                DPRAM zone used by the M32C87 and the TMS320C6727 at the same time
                DPRAM zone used by the RX610 and the TMS320C6727 at the same time

                WARNING!!! check the padding if you change something here

                the compiler for the TMS320C6727 pads the structure to be multiple of the
                biggest type inside, in this case INT32U (but it can access in 8, 16 or 32 bits)


                the structures needs to be packed to fit the access of registers in the DPRAM (dual port RAM)
                implemented in the PLD
                the PLD allows only 16 bits access for its normal registers


                For the Rx610 the PLD implements a conversion between big endian / little endian
                but ONLY HANDLES CORRECTLY 32 BITS DATA !!!! (addresses starting on 32bits boundary)

                so all fields need to be 32bits long


                ********* If this file is changed - remember to recompile the DSP library *********

\*---------------------------------------------------------------------------------------------------------*/

#ifndef DPCOM_H      // header encapsulation
#define DPCOM_H

#ifdef DPCOM_GLOBALS
    #define DPCOM_VARS_EXT
#else
    #define DPCOM_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <deftypes.h>           // for int_limits, float_limits
#include <defconst.h>           // for FGC_MAX_DIMS
#include <definfo.h>            // for FGC_CLASS_ID
#include <mcu_dsp_common.h>
#include <qspi_bus.h>           // for QSPI_BRANCHES
#include <assert.h>


#ifdef __TMS320C32__            // FGC2 DSP
    #include <structs_bits_little.h>
#endif
#ifdef __HC16__                 // FGC2 MCU
    #include <structs_bits_big.h>
#endif

//-----------------------------------------------------------------------------------------------------------

#define REQUESTS_FOR_DIM_LOG         3
#define ASSERT_FILENAME_BUF_SIZE_MCU 16
#define ASSERT_FILENAME_BUF_SIZE_DSP (ASSERT_FILENAME_BUF_SIZE_MCU / 4)

static_assert(ASSERT_FILENAME_BUF_SIZE_MCU % 4 == 0);

//-----------------------------------------------------------------------------------------------------------

/*!
 * Table function point struct contains a pair of time and reference values
 */

struct point
{
    DP_IEEE_FP32        time;
    DP_IEEE_FP32        ref;
};

// General structure declarations

struct cmd_pkt
{
#if defined(__HC16__)
    INT16U              pad0;
    INT16U              header_flags;                   // Fieldbus packet header flags
    INT16U              pad1;
    INT16U              header_user;                    // Fieldbus packet header user
    INT16U              pad2;
    INT16U              n_chars;
    char                buf[MAX_CMD_PKT_LEN];

//    __TMS320C32__
#else                                                   // Command packet as seen by DSP
    INT32U              header_flags;                   // Fieldbus packet header flags
    INT32U              header_user;                    // Fieldbus packet header user
    INT32U              n_chars;
    INT32U              buf[MAX_CMD_PKT_LEN_LW];
#endif
};

struct pars_buf
{
#if defined(__HC16__)
    INT16U              pad0;
    INT16U              type;                           // Parameter type: PKT_FLOAT|PKT_INT|PKT_RELTIME|PKT_INTU
    INT16U              pad1;
    INT16U              pkt_buf_idx;                    // 0 or 1 to select pkt[] structure
    INT16U              pad2;
    INT16U              out_idx;                        // Start of first parameter
    INT16U              pad3;
    INT16U              errnum;                         // Error number after parsing
    INT16U              pad4;
    INT16U              par_err_idx;                    // Parameter number that caused the error
    INT16U              pad5;
    INT16U              n_pars;                         // Number of parameters in results
//    __TMS320C32__
#else                                                   // Parse buffer as seen by DSP
    INT32U              type;                           // Parameter type: PKT_FLOAT|PKT_INT|PKT_RELTIME|PKT_INTU
    INT32U              pkt_buf_idx;                    // 0 or 1 to select pkt[] structure
    INT32U              out_idx;                        // Start of first parameter
    INT32U              errnum;                         // Error number after parsing
    INT32U              par_err_idx;                    // Parameter number that caused the error
    INT32U              n_pars;                         // Number of parameters in results
#endif
    union pars_limits                                   // Min/Max limits union
    {
        struct int_limits       integer;                // Integer limits
        struct intu_limits      unsigned_integer;       // Unsigned integer limits
        struct float_limits     fp;                     // Float limits
    } limits;

    union                                               // Parsing results union
    {
        INT32U          intu [MAX_CMD_PKT_PARS];        // Unsigned integer results
        INT32S          ints [MAX_CMD_PKT_PARS];        // Signed integer results
        DP_IEEE_FP32    fp   [MAX_CMD_PKT_PARS];        // Floating point results
        struct point    point[MAX_CMD_PKT_PARS / 2];    // Point results
    } results;

    struct cmd_pkt      pkt[2];                         // Double buffer for command packets
};

struct prop_buf
{
#if defined(__HC16__)
    INT16U              pad0;
    INT16U              action;                         // Action - uses fieldbus cmd pkt header bits
    INT16U              pad1;
    INT16U              dsp_prop_idx;                   // Index in DSP property table for property
    INT16U              pad2;
    INT16U              mux_idx;                        // Multiplexor index (0=not used, 1-N=slot idx)
    INT16U              pad3;
    INT16U              blk_idx;                        // Index of block
    INT16U              pad4;
    INT16U              n_elements;                     // Number of elements (16 bits on FGC3)
    INT16U              pad5;
    INT16U              dsp_rsp;                        // DSP response
//    __TMS320C32__
#else                                                   // Parse buffer as seen by DSP
    INT32U              action;                         // Action - uses fieldbus cmd pkt header bits
    INT32U              dsp_prop_idx;                   // Index in DSP property table for property
    INT32U              mux_idx;                        // Multiplexor index (0=not used, 1-N=slot idx)
    INT32U              blk_idx;                        // Index of block
    INT32U              n_elements;                     // Number of elements
    INT32U              dsp_rsp;                        // DSP response
#endif
    union                                               // Property data block
    {
        INT32U          intu[PROP_BLK_SIZE_LW];         // Unsigned integer results
        INT32S          ints[PROP_BLK_SIZE_LW];         // Signed integer results
        DP_IEEE_FP32    fp  [PROP_BLK_SIZE_LW];         // Floating point results
    } blk;
};

struct pfloat_buf
{
#if defined(__HC16__)
    INT16U              pad;
    INT16U              format_idx;                     // Format code (1-3)
    FP32                value;                          // Float to be printed to ASCII
    INT8S               result[PFLOAT_LEN];             // Results buffer
//    __TMS320C32__
#else                                                   // Parse buffer as seen by DSP
    INT32U              format_idx;                     // Action - uses fieldbus cmd pkt header bits
    DP_IEEE_FP32        value;                          // Float in IEEE format
    INT32U              result[PFLOAT_LEN/4];           // Results buffer
#endif
};

    // we can make up to 3 log request
    struct TDimLogRequests
    {
        INT32U   number_of_requests;                    // (1-3) number of logical DIMs to log
        INT32U   logical_dim_to_log_for_req[REQUESTS_FOR_DIM_LOG];
        INT32U   entry_for_req[REQUESTS_FOR_DIM_LOG];   // list of sample index
    };

// Command Dual Port Ram Structure

struct dpcom
{                                                       // ***** Cmd packet parsing *****
    struct pars_buf     fcm_pars;                       // FcmTsk parameters
    struct pars_buf     tcm_pars;                       // TcmTsk parameters
                                                        // ***** DSP property communications *****

    struct prop_buf     pcm_prop;                       // PubTsk property communications
    struct prop_buf     fcm_prop;                       // FcmTsk property communications
    struct prop_buf     tcm_prop;                       // TcmTsk property communications

                                                        // ***** Print Float Support *****
    struct pfloat_buf   pcm_pfloat;                     // PubTsk print float buffer
    struct pfloat_buf   fcm_pfloat;                     // FcmTsk print float buffer
    struct pfloat_buf   tcm_pfloat;                     // TcmTsk print float buffer

    struct dpcom_mcu                                    // *********** MCU -> DSP ***********
    {
        INT32S           started;                       // Software started magic number (DSP_STARTED)
        INT32U           state_op;                      // Operational state
        INT32U           device_ppm;                    // DEVICE.PPM property (ENABLED/DISABLED)
        INT32U           interfacetype;                 // Slot5 interface type
        INT32U           mst_time;                      // [1 ms] Time needed for MstTsk in 0.5us units
        INT32U           debug_mem_dsp_addr;            // Start of address for debug memory zone
        INT32U           runcode_handshake;             // Handshake for runcode reporting

        struct dpcom_mcu_diag
        {
            INT32U       scantime_tcnt[QSPI_BRANCHES];  // Scan trigger times relative to start of ms (0.5us)
            INT32U       freeze_all_dim_logs;
            INT32U       relaunch_dim_logging;          // restart DIM logging
    #if defined(__HC16__)
            INT16U       qspi[QSPI_BRANCHES][16];       // data received from QSPI in the actual ms
    #else // __TMS320C32__
            INT32U       qspi[QSPI_BRANCHES][8];        // NB: in DSP qspi[A,B][0..15] is handled as [A,B][0..7] because the DSP fake INT16U
    #endif
            INT32U       flat_qspi_UNlatched_trig_bits; // Unlatched DIM triggers bit packed in INT32U
            INT32U       flat_qspi_latched_trig_bits;   // Latched DIM triggers bit packed in INT32U
            struct TDimLogRequests log;                 // we can make up to 3 log request
        } diag;

        struct dpcom_mcu_time
        {
            INT32U       ms_mod_10;                     // (time_now.ms_time%10)
            INT32U       ms_mod_20;                     // (time_now.ms_time%20)
            INT32U       ms_mod_200;                    // (time_now.ms_time%200)

            struct abs_time_ms now_ms;                  // Absolute time now in ms
            struct abs_time_ms fbs;                     // Absolute time in last fieldbus time var (rounded to 20ms boundary)
        } time;

        struct dpcom_mcu_evt
        {
            INT32U              start_event_delay;            // Non-PPM Start event delay (ms)
            INT32U              abort_event_delay;            // Non-PPM Abort event delay (ms)
            INT32U              next_cycle_start_delay_ms;    // Next cycle start delay (ms) coming from event
            INT32U              next_cycle_user;              // Next cycle user coming from event
            INT32U              ssc_f;                        // Start super-cycle received
            INT32U              sim_supercycle_f;             // Simulated super-cycle flag
            INT32U              cycle_tag;                    // Cycle tag
        } evt;

        struct dpcom_mcu_pll
        {
            INT32S      period;                         // PLL period
            INT32S      integrator;                     // PLL integrator
            INT32S      error;                          // PLL error
        } pll;
    } mcu;

    struct dpcom_dsp                                    // *********** DSP -> MCU ***********
    {
        INT32U                  assert_line;
#if defined(__HC16__)
        INT8S                   assert_file[ASSERT_FILENAME_BUF_SIZE_MCU];
#else // __TMS320C32__
        INT32U                  assert_file[ASSERT_FILENAME_BUF_SIZE_DSP];
#endif
        INT32S                  started;                // Software started magic number (DSP_STARTED)
        INT32U                  version;                // Version number
        union TUnion32Bits      ms_irq_alive_counter;   // [1ms]   incremented at IsrPeriod
        union TUnion32Bits      mcu_counter_dsp_alive;  // [4Hz]   reset at dsp main bg loop, incremented at MCU MstTsk()
        INT32U                  cpu_usage;              // [1ms]   DSP RT processing usage in %
        INT32U                  run_code;               // [isr]   Isr run codes
        INT32U                  bg_complete_f;          // Background processing completed flag
        INT32U                  errnum;                 // Background processing error number
        INT32U                  faults;                 // [20ms]  Faults
        INT32U                  warnings;               // [20ms]  Warnings
        INT32U                  st_unlatched;           // [20ms]  Unlatched status
        INT32U                  st_latched;             // [20ms]  Latched status
#if (FGC_CLASS_ID == 59)
        INT32U                  tb_freq_hz;             //         Timebase value in Hz
#endif

        // Cycle variables

        struct dpcom_dsp_cyc
        {
            INT32U              start_cycle_f;          // [1ms]   Start of cycle flag
            struct abs_time_us  start_time;             // [sc]    Time of start of cycle
            struct abs_time_us  prev_start_time;        // [sc]    Time of start of previous cycle
            INT32U              user_timing;            // [sc]    User of this cycle from timing
            INT32U              user;                   // [sc]    User of this cycle
            INT32U              prev_user;              // [sc]    User of the previous cycle
            INT32U              dysfunction_code;       // Warning or fault that occurred during the cycle
            INT32U              dysfunction_is_a_fault; // warning (0) or fault (1)
        } cyc;

        // PSU voltages

        struct dpcom_psu
        {
            DP_IEEE_FP32        fgc[3];                         // [1Hz] FGC PSU voltages (+5,+15,-15)
            DP_IEEE_FP32        ana[3];                         // [1Hz] Ana interface PSU voltages (+5,+15,-15)
        } psu;

        // Diagnostic processing

        struct dpcom_diag
        {
            INT32S      dim_trig_f;                     // DIM trigger detected flag
            INT32S      reset_req_packed;               // DIM board reset request bit packed in INT32U
            INT32S      log_lock_counter;               // Logging lock counter
            INT32S      log_lock_f;                     // Logging lock flag
            INT32U      dips_are_valid;                 // DIPS boards are valid
            INT32U      dims_are_valid;                 // DIM boards are valid flag
            INT32U      global_unlatched_trig_f;        // Global unlatched trigger flag

            struct dpcom_diag_log
            {
                INT32U                  samples_to_acq; // Samples still to acquire
                INT32U                  idx;            // Index of last sample in the buffer
                struct abs_time_us      time;           // Time of last sample in the buffer
            } log[FGC_MAX_DIMS];                        // FGC_MAX_DIMS slots are allocated for logging

            INT32U              dim_evt_log_state[FGC_MAX_DIMS];// Event log state for DIMs
            struct abs_time_us  dim_trig_time[FGC_MAX_DIMS];    // Time of DIM trigger
        } diag;

        // Debug zone for DEBUG.MEM.DSP

        INT32U          debug_mem_dsp_i[DB_DSP_MEM_LEN];        // For DEBUG.MEM.DSP (int value)
        DP_IEEE_FP32    debug_mem_dsp_f[DB_DSP_MEM_LEN];        // For DEBUG.MEM.DSP (float value)
    } dsp;
};

//-----------------------------------------------------------------------------------------------------------

#if defined(__TMS320C32__)
    DPCOM_VARS_EXT volatile struct dpcom *      dpcom;
#endif

#if defined(__HC16__)
    #pragma DATA_SEG DPCOM
    DPCOM_VARS_EXT volatile struct dpcom        dpcom;
    #pragma DATA_SEG DEFAULT
#endif

//-----------------------------------------------------------------------------------------------------------

#endif  // DPCOM_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: dpcom.h
\*---------------------------------------------------------------------------------------------------------*/
