/*---------------------------------------------------------------------------------------------------------*\
  File:         qspi_bus.h


\*---------------------------------------------------------------------------------------------------------*/

#ifndef QSPI_BUS_H      // header encapsulation
#define QSPI_BUS_H

#ifdef QSPI_BUS_GLOBALS
#define QSPI_BUS_VARS_EXT
#else
#define QSPI_BUS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

#define BRANCH_A        0
#define BRANCH_B        1
#define QSPI_BRANCHES   2       // for array size

/*
    physically we have 2 branches A and B with capability of 16 boards (DIMs) each
    qspi branch A boards [0..15]
    qspi branch B boards [0..15]

    for simplicity we flatten the bus as flat_qspi_board_number[0..31]

    corresponding
    [00..15] for qspi branch A boards [0..15]
    [16..31] for qspi branch B boards [0..15]

    when we store the flat qspi bus board number in the logical dim list it can be
    0x00..0x1F are real physical DIM boards (0x00..0x0F on branch A, 0x10..0x1F on branch B)
    0x80   b7=1 means a virtual DIM (composite)
    0xFF   means not used (empty)



    in simple cases the information is "packed" as bits in an INT32U where each
    bits corresponds to the board with the same flatten order [0..31]

    in the FGC2 flat_qspi_board_number[0] and [16] are the same board (DIPS)

    later we made a list where only FGC_MAX_DIMS (0..19) boards including virtual ones
    are available for process


    Each DIM card has 7 registers (of 16 bits)

        the register info is composed by : tiii dddd dddd dddd
            t            : trigger
            iii          : ID
            dddddddddddd : data

        the 7 registers are received in this order

            0 - Dig0        DIM register ID:0
            1 - Dig1        DIM register ID:1
            2 - Ana0        DIM register ID:4
            3 - Ana1        DIM register ID:5
            4 - Ana2        DIM register ID:6
            5 - Ana3        DIM register ID:7
            6 - TrigCounter DIM register ID:2
                             there is no ID:3


    The scan/reception of the DIM register are spread along the 20ms cycle

    ms 00 resets the qspi bus to start the reception

    ms 01 , qspi[1][0..15] filled with branch A DIM boards register ID 0, digital_0
    ms 02 , qspi[0][0..15] filled with branch A DIM boards register ID 1, digital_1
    ms 03 , qspi[1][0..15] filled with branch A DIM boards register ID 4, analog_0
    ms 04 , qspi[0][0..15] filled with branch A DIM boards register ID 5, analog_1
    ms 05 , qspi[1][0..15] filled with branch A DIM boards register ID 6, analog_2
    ms 06 , qspi[0][0..15] filled with branch A DIM boards register ID 7, analog_3
    ms 07 , qspi[1][0..15] filled with branch A DIM boards register ID 2, trigCounter

    ms 11 , qspi[1][0..15] filled with branch B DIM boards register ID 0, digital_0
    ms 12 , qspi[0][0..15] filled with branch B DIM boards register ID 1, digital_1
    ms 13 , qspi[1][0..15] filled with branch B DIM boards register ID 4, analog_0
    ms 14 , qspi[0][0..15] filled with branch B DIM boards register ID 5, analog_1
    ms 15 , qspi[1][0..15] filled with branch B DIM boards register ID 6, analog_2
    ms 16 , qspi[0][0..15] filled with branch B DIM boards register ID 7, analog_3
    ms 17 , qspi[1][0..15] filled with branch B DIM boards register ID 2, trigCounter
*/




#if defined(__RX610__) || defined(__HC16__)     // big endian
struct TDIM_register_parts
{
    BIT_FIELD t    :  1; // b15
    BIT_FIELD id   :  3; // b14, b13, b12
    BIT_FIELD data : 12; // b11..b0
};
#endif

#if defined(__TMS320C6727__) || defined(__TMS320C32__) // little endian
struct TDIM_register_parts
{
    BIT_FIELD data  : 12; // b0..b11
    BIT_FIELD id    :  3; // b12, b13, b14
    BIT_FIELD t     :  1; // b15
};
#endif

union TDIM_register
{
    INT16U                          reg;
    struct TDIM_register_parts      part;
};

//-----------------------------------------------------------------------------------------------------------

#endif  // QSPI_BUS_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: qspi_bus.h
\*---------------------------------------------------------------------------------------------------------*/
