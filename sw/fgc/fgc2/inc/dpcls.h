/*---------------------------------------------------------------------------------------------------------*\
  File:         dpcls.h

  Purpose:

  History:

        The dual port ram is implemented inside the Xilinx
        and can be accessed by the DSP tms320c6727 and the MCU Rx610 (or the M32C87)


        DPRAM zone used by the M32C87 and the TMS320C6727 at the same time
        DPRAM zone used by the RX610 and the TMS320C6727 at the same time

        WARNING!!! check the padding if you change something here

        the compiler for the TMS320C6727 pads the structure to be multiple of the
        biggest type inside, in this case INT32U (but it can access in 8, 16 or 32 bits)


        the structures needs to be packed to fit the access of registers in the DPRAM (dual port RAM)
        implemented in the PLD
        the PLD allows only 16 bits access for its normal registers


        For the Rx610 the PLD implements a conversion between big endian / little endian
        but ONLY HANDLES CORRECTLY 32 BITS DATA !!!! (addresses starting on 32bits boundary)

        so all fields need to be 32bits long
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DPRAM_CLASS_H   // header encapsulation
#define DPRAM_CLASS_H

#ifdef DPRAM_CLASS_GLOBALS
    #define DPRAM_CLASS_VARS_EXT
#else
    #define DPRAM_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>
#include <defconst.h>       // for FGC_N_ADCS, FGC_N_PAL_CHANS, FGC_N_FGC2PBL_WORDS
#include <mcu_dsp_common.h> // for struct abs_time_us
#include <definfo.h>        // for FGC_CLASS_ID

#ifdef __TMS320C32__
    #include <structs_bits_little.h>
#endif
#ifdef __HC16__
    #include <memmap_mcu.h>
    #include <structs_bits_big.h>
#endif

#include <dpcom.h>

//-----------------------------------------------------------------------------------------------------------

// Shared Constants

#define CAL_TEMP                0.0625          // (= 1/16)Temperature calibration (C/raw)
#define CAL_T0                  23.0            // T0 calibration temperature
#define CAL_T1                  28.0            // T1 calibration temperature
#define CAL_T2                  33.0            // T2 calibration temperature

// ADCs default gains

#define CAL_DEF_INTERNAL_ADC_GAIN   19800000    // Default 16-bit ADC gain   (raw ADC units per 10V)
#define CAL_DEF_EXTERNAL_ADC_GAIN   19800000    // Default 22-bit ADC gain   (raw ADC units per 10V)

// Calibration actions

#define CAL_BOTH_ADC16S_N_MEAS  5               // Number of measurements for auto cal of ADC16s
#define CAL_REQ_BOTH_ADC16S     0               // Calibration request: Both ADC16s all errors
#define CAL_REQ_ADC16           1               // Calibration request: One ADC16 error
#define CAL_REQ_ADC22           2               // Calibration request: ADC22 error
#define CAL_REQ_DCCT            3               // Calibration request: DCCT  error
#define CAL_REQ_DAC             4               // Calibration request: Complete DAC

#define LOG_CAPTURE_TIMEOUT     60              // Log capture frozen state timeout (s)

#define PAL_RESET_DELAY_MS      5000            // Delay (ms) after reset trigger before reset

#if (FGC_CLASS_ID == 51) || (FGC_CLASS_ID == 53)
    #define ADC_RESET_MAX_MS        57              // Maximum duration of an ADC filters reset (ms)
                                                    // Must be around twice the typical duration (30 ms)

    // Min regulation period that can support a reset of the ADC filters while regulating the current
    // That duration is calculated as follow:
    //    1                         (delay between latest regulation and start of filter reset)
    //  + ADC_RESET_MAX_MS          (max duration of a filter reset)
    //  + 2                         (filter length)
    //  + 20                        (20ms sliding window used to compute meas.i in slow regulation mode)

    #define ADC_REG_MIN_PERIOD_MS   (1 + ADC_RESET_MAX_MS + 2 + 20)
#endif

enum adc_dsp_reset_req                                  // Enum type for dpcls.dsp.adc.reset_req
{
        ADC_DSP_RESET_ACK = 0,                          // Leave equal to zero
        ADC_DSP_RESET_REQ,
        ADC_DSP_RESET_WAIT
};

//-----------------------------------------------------------------------------------------------------------
struct dpcls_fifo_head
{
    INT32U      in_idx;                 // Next sample to write
    INT32U      out_idx;                // Next sample to read
};

//-----------------------------------------------------------------------------------------------------------
#if (FGC_CLASS_ID == 59)
struct dpcls
{
    struct dbg_t
    {
        INT32U    u[20];                        // TEST.DEBUG.INT32U
        FP32      f[20];                        // TEST.DEBUG.FLOAT
    } dbg;

    struct dpcls_mcu                                    // *********** MCU -> DSP ***********
    {
        INT32U          rfc_reset;                      // [1ms]   RFC being reset down counter
        INT32U          rfc_status [FGC_N_FGEN_CHANS];  // [1ms]   RFC status registers
        DP_IEEE_FP32    wval       [FGC_N_FGEN_CHANS];  // [20ms]  WorldFIP RT values
        DP_IEEE_FP32    wval_plus_one;                  //         +1 float is needed after wval[] because
                                                        //         MemCpyFip() copies blocks of 8 bytes and
        struct dph_ref                                  //         transfer starts at [1] so must end at [16])
        {
            INT32U      change_f;                               // Reference changed flag
            INT32U      chan_idx;                               // Channel index for reference change
            INT32U      type             [FGC_N_FGEN_CHANS];    // FGFUNC.TYPE[]
            INT32U      type_old         [FGC_N_FGEN_CHANS];    // Old value of FGFUNC.TYPE[]
            INT32U      start_event_delay[FGC_N_FGEN_CHANS];    // Start event delay (ms)
            INT32U      abort_event_delay[FGC_N_FGEN_CHANS];    // Abort event delay (ms)
        } ref;
    } mcu;

    struct dpcls_dsp                                    // *********** DSP -> MCU ***********
    {
        INT32U          trig_str;                       // [On change] ASCII hex value of trigger
        INT32U          trigger;                        // [1ms]  Trigger for next millisecond
        INT32U          n_trig_chans;                   // [1ms]  Number of active channels to be triggered
        INT32U          idle;                           // [20ms] Idle state mask
        INT32U          armed;                          // [20ms] Armed state mask
        INT32U          running;                        // [20ms] Running state mask
        INT32U          aborting;                       // [20ms] Aborting state mask
        INT32U          not_ack;                        // [20ms] Not ack state mask
        INT32U          rt_ctrl;                        // [20ms] Real-time control mask
        INT32U          ack;                            // [20ms] Acknowledge received mask
        INT32U          nack;                           // [20ms] Not Acknowledge received mask
        INT32U          start_evt;                      // [20ms] Start event mask
        INT32U          abort_evt;                      // [20ms] Abort event mask
        INT32U          lim_rt_pos;                     // [20ms] RT positive limit mask
        INT32U          lim_rt_neg;                     // [20ms] RT negative limit mask
        INT32U          lim_chan_max;                   // [20ms] Chan max limit mask
        INT32U          lim_chan_min;                   // [20ms] Chan min limit mask
        INT32U          lim_rate_pos;                   // [20ms] Idle state mask
        INT32U          lim_rate_neg;                   // [20ms] Idle state mask
        INT32U          loop_f;                         // [20ms] Loop broken flag
        INT32U          crc_err_10s_f;                  // [20ms] CRC error flag        (stretched to 10s)
        INT32U          lim_any_10s;                    // [20ms] Any limit active mask (stretched to 10s)
        INT32U          not_ack_10s;                    // [20ms] Not Acknowledged mask (stretched to 10s)

        struct dpc_fgfunc
        {
            INT32U              run_period   [FGC_N_FGEN_CHANS];//         Function run periods
            DP_IEEE_FP32        cval         [FGC_N_FGEN_CHANS];// [1ms]   Floating point channel values
            INT32S              ival         [FGC_N_FGEN_CHANS];// [1ms]   Integer channel values
        } fgfunc;

        struct dpc_fgstate
        {
            INT32U      func       [FGC_N_FGEN_CHANS];  // FGSTATE.FUNC[]
        } fgstate;

        struct dpcls_dsp_panic
        {
            INT32U      panic_code;                     // DSP panic code
        } panic;
    } dsp;
};
#else
struct dpcls
{
    struct dbg_t
    {
        INT32U       u[20];                        // TEST.DEBUG.INT32U
        DP_IEEE_FP32 f[20];                        // TEST.DEBUG.FLOAT
    } dbg;

    struct dpcls_mcu                            // *********** MCU -> DSP ***********
    {
        INT32U state_pc;                        // Power Converter state
        INT32U mode_rt;                         // MODE.RT (Real-time data mode)
        INT32U dcct_select;                     // DCCT.SELECT (A, B or AB)
        INT32U adc_block_f;                     // ADC block flag (during multiplexor changes)
        INT32U adc_mpx_f;                       // ADC multiplexer changed flag
        INT32S adc_filter_mpx[FGC_N_ADCS];      // ADC filter A/B digital input selector
        INT32U internal_adc_mpx[FGC_N_ADCS];    // ADC16A/B analogue input selector
        INT32U sd_shift[FGC_N_ADCS];            // ADC SD Filter shift values for A/B
        INT32U adc_mpx_flt;                     // ADC mpx in faulty position flags (A:0x1 B:0x2)
        INT32U dcct_flt;                        // DCCT fault flags (A:0x1 B:0x2)
        INT32U log_capture_mpx[FGC_LOG_CAPTURE_MPX_LEN]; // LOG.CAPTURE.SPY.MPX property

        struct dpcls_mcu_adc
        {
            INT32U  filter_state;                       // Property ADC.FILTER.STATE

            struct dpcls_mcu_adc_chan
            {
                INT32S  raw[3];                         // Three samples x 16bit (SAR) or one sample of SD
            } chan[FGC_N_ADCS];

            INT32S      buserr_f[FGC_N_ADCS];           // A bus error occurred while reading the ADC samples
        } adc;

        struct dpcls_mcu_cal
        {
            INT32U      active;                 // CAL.ACTIVE property
            INT32U      req_f;                  // Set by MCU, cleared by DSP
            INT32S      action;                 // Cast to 'enum cal_action' on FGC3
            INT32U      chan_mask;              // Mask of the input channels (2 on the FGC2, 4 on the FGC3)
            INT32U      ave_steps;              // Averaging period in 200-sample steps (max 50)
            INT32U      idx;                    // CAL_IDX_OFFSET/CAL_IDX_POS/CAL_IDX_NEG, or measurement number (0-4)
                                                // on FGC2 for the calibration of both ADC16s

            INT32U      imeas_zero;             // Block IMEAS at zero request down counter
            INT32U      t_adc;                  // ADC16 temperature            (units: 1/16 C)
            INT32U      t_dcct[2];              // DCCT electronics temperature (units: 1/16 C)

            // The arrays 'FP32 err[6]' below should be casted to (struct cal_event)

            struct                                      // # = A or B
            {
                DP_IEEE_FP32    err[6];                 // CAL.#.DCCT.ERR (Err0,Err+,Err-,Temp,Date,Time)
            } dcct[2];
            struct
            {
                struct                                  // # = A, B, C or D
                {
                    DP_IEEE_FP32    err[6];             // CAL.#.ADC.INTERNAL.ERR (Err0,Err+,Err-,Temp,Date,Time)
                } internal[FGC_N_ADCS];
                struct                                  // # = A or B
                {
                    DP_IEEE_FP32    err[6];             // CAL.#.ADC.EXTERNAL.ERR (Err0,Err+,Err-,Temp,Date,Time)
                } external[2];
            } adc;
        } cal;

        struct dpcls_mcu_meas
        {
            INT32U              status_reset_f;         // User asked for a S MEAS.STATUS RESET
        } meas;

        struct dpcls_mcu_ref
        {
            INT32U              arm_f;                  // Arm reference function flag
            INT32U              user;                   // User to arm
            INT32U              func_state;             // Target state for armed function: CYCLING or RUNNING
            INT32U              func_type;              // Function type to arm
            INT32U              stc_func_type;          // Symbol index for function type to arm
            INT32U              run_now_f;              // Run immediately flag
            INT32U              rt_data_f;              // Real-time data is waiting
            DP_IEEE_FP32        rt_data;                // Real-time data from WFIP

            struct dpcls_mcu_ref_defaults
            {
                struct dpcls_mcu_ref_defaults_i
                {
                    DP_IEEE_FP32        acceleration[FGC_N_LOADS];      // REF.DEFAULTS.I.ACCELERATION
                    DP_IEEE_FP32        linear_rate [FGC_N_LOADS];      // REF.DEFAULTS.I.LINEAR_RATE
                } i;

                struct dpcls_mcu_ref_defaults_v
                {
                    DP_IEEE_FP32        acceleration;                   // REF.DEFAULTS.V.ACCELERATION
                    DP_IEEE_FP32        linear_rate;                    // REF.DEFAULTS.V.LINEAR_RATE
                } v;
            } defaults;

            struct dpcls_mcu_ref_func
            {
                INT32U  type     [FGC_MAX_USER_PLUS_1]; // REF.FUNC.TYPE property
                INT32U  reg_mode [FGC_MAX_USER_PLUS_1]; // REF.FUNC.REG_MODE property
            } func;
        } ref;

        struct dpcls_mcu_vs
        {
            INT32U      n_sub_convs;                    // Number of active sub converters
            INT32U      amps_per_sub_conv;              // Amps per sub converter
            INT32S      i_earth_chan;                   // I_EARTH channel index
            INT32S      u_lead_pos_chan;                // U_LEAD_POS channel index
            INT32S      u_lead_neg_chan;                // U_LEAD_NEG channel index

            struct dpcls_mcu_vs_polarity
            {
                INT32U  bipolar;                        // Property SWITCH.POLARITY.BIPOLAR
                INT32U  state;                          // Polarity switch state SWITCH.POLARITY.STATE
                INT32U  mode;                           // Polarity switch state SWITCH.POLARITY.MODE
                INT32U  timeout;                        // Polarity switch timeout (s) SWITCH.POLARITY.TIMEOUT
                INT32U  changed;                        // Polarity switch has been changed.
            } polarity;
        } vs;

#if (FGC_CLASS_ID == 53)
        struct dpcls_mcu_pal                            // POPS Acqisition Link
        {
            INT32U dig_commands;                        // [5ms]   Digital commands (from StaTsk())
            INT32S pam_raw[FGC_N_PAL_CHANS];            // [1ms]   Raw PAM data for PAL.CHAN.RAW

            struct dpcls_mcu_pal_lk1
            {
                INT32U  plc_pbl_pam_stat;               // [1ms]   PBL Loopback, PBL and PAM status
                INT32S  bmeas_raw;                      // [1ms]   Raw B measurement from B-train receiver
                INT32S  vout_dxp1;                      // [1ms]   Vout from DXP1 (Output filter 1)
                INT32S  ucap_dsp1;                      // [1ms]   Ucap from DSP1 (Container 1)
                INT32S  ucap_dsp3;                      // [1ms]   Ucap from DSP3 (Container 3)
                INT32S  ucap_dsp5;                      // [1ms]   Ucap from DSP5 (Container 5)
                INT32S  bmeas_raw_test;                 // [1ms]   Raw B measurement from second B-train receiver - NOT USED
            }lk1;
            struct dpcls_mcu_pal_lk2
            {
                INT32U  clc_pal_pops_stat;              // [1ms]   CTL Loopback, PAL and POPS status
                INT32U  pal_pbl_prop;                   // [1ms]   Requested property data (variable type)
                INT32S  vout_dxp2;                      // [1ms]   Vout from DXP2 (Output filter 2)
                INT32S  ucap_dsp2;                      // [1ms]   Ucap from DSP2 (Container 2)
                INT32S  ucap_dsp4;                      // [1ms]   Ucap from DSP4 (Container 4)
                INT32S  ucap_dsp6;                      // [1ms]   Ucap from DSP6 (Container 6)
            }lk2;
        }pal;
#endif
    } mcu;

    struct dpcls_dsp                            // *********** DSP -> MCU ***********
    {
        INT32U  load_select;                    // Load select
        INT32U  unipolar_f;                     // Unipolar current converter (1Q or 2Q)
        INT32U  cal_complete_f;                 // Calibration complete.  Set by C32, cleared by MCU

        struct dpcls_dsp_adc
        {
            INT32U       num_no_frame [FGC_N_ADCS];      // [1ms]   Number of no frame filter statuses
            INT32U       num_frame_err[FGC_N_ADCS];      // [1ms]   Number of frame err filter statuses
            INT32S       raw_200ms    [FGC_N_ADCS];      // [200ms] Average calibrated (Vraw)
            DP_IEEE_FP32 volts_200ms  [FGC_N_ADCS];      // [200ms] Average calibrated in Volts (Vadc)
            DP_IEEE_FP32 amps_200ms   [2];               // [200ms] Average calibrated in Amps  (DCCT: IA, IB)
            INT32S       raw_1s       [FGC_N_ADCS];      // [1s]    Average calibrated (Vraw)
            DP_IEEE_FP32 volts_1s     [FGC_N_ADCS];      // [1s]    Average calibrated in Volts (Vadc)
            DP_IEEE_FP32 amps_1s      [2];               // [1s]    Average calibrated in Amps  (I DCCT)
            DP_IEEE_FP32 pp_volts_1s  [FGC_N_ADCS];      // [1s]    Peak-peak range for volts over 1s
            INT32U       reset_gate_f;                   // Gate for ADC filters reset depending on reg state
            INT32U       reset_req;                      // ADC filter reset DSP request (REQ/WAIT/ACQ)
        } adc;

        struct dpcls_dsp_cyc
        {
            INT32U              to_standby_f;                   // To standby flag (quit CYCLING state)
            struct dpcls_cyc_fault
            {
                INT32U           user;                          // Property REF.CYC.FAULT.USER
                INT32U           chk;                           // Property REF.CYC.FAULT.CHK
                DP_IEEE_FP32     data[FGC_N_CYC_CHK_DATA];      // Property REF.CYC.FAULT.DATA
            } fault;
        } cyc;

        struct dpcls_dsp_log
        {
            struct dpcls_dsp_log_capture
            {
                INT32U                  state;                  // LOG.CAPTURE.STATE (ARMED, RUNNING, or FGC_LOG_FROZEN)
                INT32S                  user;                   // LOG.CAPTURE.USER
                INT32U                  n_samples;              // Number of capture log samples in buffer
                INT32U                  reg_state;              // Regulation state; Set at start of logging
                struct abs_time_us      first_sample_time;      // Time of first sample
                struct abs_time_us      last_sample_time;       // Time of last sample
                struct abs_time_us      cyc_chk_time;           // LOG.CAPTURE.CYC_CHK_TIME
                INT32U                  sampling_period_us;     // Sampling period (us)
            } capture;
        } log;

        struct dpcls_dsp_fifo
        {
            struct dpcls_fifo_head      adc;            // ADC fifo

            struct abs_time_us  adc_time [FGC_ADC_FIFO_LEN];    // ADC: Sample time
            INT32S              adc_raw_a[FGC_ADC_FIFO_LEN];    // ADC: Raw A (200ms)
            INT32S              adc_raw_b[FGC_ADC_FIFO_LEN];    // ADC: Raw B (200ms)
            INT32U              adc_pp_a [FGC_ADC_FIFO_LEN];    // ADC: P-P A (200ms)
            INT32U              adc_pp_b [FGC_ADC_FIFO_LEN];    // ADC: P-P B (200ms)
        } fifo;

        struct dpcls_dsp_meas
        {
            INT32U          st_meas[FGC_N_ADCS];        // [20ms]  ADC status
            INT32U          st_dcct[2];                 // [20ms]  DCCT status
            DP_IEEE_FP32    v;                          // [20ms]  Measured voltage (from diag)
            DP_IEEE_FP32    v_capa;                     // [20ms]  Measured voltage across the capacitor
            DP_IEEE_FP32    i;                          // [1ms]   Measured current for feedback
            DP_IEEE_FP32    b;                          // [1ms]   Measured field (Class 53)
            DP_IEEE_FP32    ia;                         // [1ms]   Measured current from DCCT A (unfiltered)
            DP_IEEE_FP32    ib;                         // [1ms]   Measured current from DCCT B (unfiltered)
            INT32S          i_diff_ma;                  // [20ms]  Measurement difference
            DP_IEEE_FP32    i_earth;                    // [20ms]  Measured earth current
            INT32S          i_earth_cpcnt;              // [20ms]  Measured earth current (centi-percent of trip limit)
            INT32U          i_access_f;                 // [20ms]  Iaccess flag (|Imeas| > Iaccess)
            INT32U          i_min_f;                    // [20ms]  Imin flag    (Imeas > Ineg)
            INT32U          i_low_f;                    // [20ms]  Ilow flag   (|Imeas| < 0.1 * Ipos)
            INT32U          i_zero_f;                   // [20ms]  Izero flag  (|Imeas| < 0.0001 * Ipos)
            DP_IEEE_FP32    i_rate;                     // [1ms]   Measured current rate for feedback

            struct dpcls_dsp_meas_ileads
            {
                DP_IEEE_FP32 i;                         // [1s]    Measured current (20ms sampled at 1Hz)
                DP_IEEE_FP32 v;                         // [1s]    Measured voltage (20ms sampled at 1Hz)
                DP_IEEE_FP32 u_leads[2];                // [1s]    U_LEAD_POS, U_LEAD_NEG (Volts 1s averages)
            } ileads;
        } meas;

        struct dpcls_dsp_notify                         // Reset to -1, set to user (0..MAX_USER)
        {
            INT32S      log_oasis_user;                 // Pub LOG.OASIS
            INT32S      log_capture_user;               // Pub LOG.CAPTURE.SIGNALS
            INT32S      ref_cyc_warning;                // Pub REF.CYC.WARNING(user)
            INT32S      ref_cyc_fault;                  // Pub REF.CYC.FAULT
        } notify;

        struct dpcls_dsp_panic
        {
            INT32U      panic_code;                     // DSP panic code
        } panic;

        struct dpcls_dsp_ref
        {
            INT32U       start_f;        // [1ms]   ref start flag (set at the transition from ARMED to RUNNING)
            INT32U       func_type;      //         Actual ref function type (CYC & non-CYC)
            INT32U       stc_func_type;  //         Func type symbol index for RTD
            DP_IEEE_FP32 max;            //         ref function max value
            DP_IEEE_FP32 start;          //         ref function intial value
            DP_IEEE_FP32 end;            //         ref function final value
            DP_IEEE_FP32 now;            // [1ms]   ref now (G or A)
            DP_IEEE_FP32 i;              // [1ms]   ref gen value (A)
            DP_IEEE_FP32 i_rst;          //         ref stored in the RST history (A)
            DP_IEEE_FP32 v;              // [1ms]   ref gen value (V)
            INT32S       dac_raw;        // [1ms]   Raw dac value
            DP_IEEE_FP32 time_running;   // [1ms]   Time spent running the reference (s)
            DP_IEEE_FP32 time_remaining; // [1ms]   Time remaining to run the reference (s)
            DP_IEEE_FP32 exp_tc;         //         default PLEP exp_tc    (load_tc2)
            DP_IEEE_FP32 exp_final;      //         default PLEP exp_final (I_CLOSELOOP)

            struct dpcls_dsp_ref_func
            {
                INT32U  type     [FGC_MAX_USER_PLUS_1]; // Armed ref function type
            } func;
        } ref;

        struct dpcls_dsp_reg
        {
            union TUnion32Bits  state;                   // REG.STATE (set at start of each cycle)
            INT32U              run_f;                   // True on iterations when regulation runs
            DP_IEEE_FP32        err;                     // [1ms]   B/I loop error (G or A)
            INT32S              i_err_ma;                // [20ms]  I loop error (mA)
        } reg;

#if (FGC_CLASS_ID == 53)
        struct dpcls_dsp_pal                            // POPS Acquisition Link
        {
            INT32U       reset_req;                     // PAL reset request downcounter
            INT32U       state;                         // PAL comms state
            DP_IEEE_FP32 pam_volts[FGC_N_PAL_CHANS];    // Calibrated PAM data for PAL.CHAN.VOLTS
            DP_IEEE_FP32 fgc2pbl[FGC_N_FGC2PBL_WORDS];  // Debug access to FGC-to-PBL data

            struct dpcls_dsp_pal_cal
            {
                DP_IEEE_FP32 offset  [FGC_N_PAL_CHANS]; // Property PAL.CHAN.CAL.OFFSET
                DP_IEEE_FP32 gain_pos[FGC_N_PAL_CHANS]; // Property PAL.CHAN.CAL.GAIN_POS
                DP_IEEE_FP32 gain_neg[FGC_N_PAL_CHANS]; // Property PAL.CHAN.CAL.GAIN_NEG
            } cal;
        } pal;
#endif
    } dsp;
};
#endif

//-----------------------------------------------------------------------------------------------------------

// Class related Dual Port Ram (in DPCLS data segment)
#ifdef __HC16__                                                 // MCU FGC2
#pragma DATA_SEG DPCLS

DPRAM_CLASS_VARS_EXT volatile struct dpcls       dpcls;  // Class related Dual Port RAM Structure

#pragma DATA_SEG DEFAULT
#endif

#if defined(__TMS320C32__)          // DSP FGC2
#include <memmap_dsp.h>

// Note: 'dpcls' is defined here as a const pointer. However, it cannot be used to declare a DSP property
//       value or size, that will be rejected by the compiler.

DPRAM_CLASS_VARS_EXT volatile struct dpcls * const dpcls
#ifdef DPRAM_CLASS_GLOBALS
= (struct dpcls *)DPRAM_DPCLS_32
#endif
;
#endif

//-----------------------------------------------------------------------------------------------------------

#endif  // DPRAM_CLASS_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: dpcls.h
\*---------------------------------------------------------------------------------------------------------*/
