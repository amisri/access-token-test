/*---------------------------------------------------------------------------------------------------------*\
  File:     boot_dual_port_ram.h

  Purpose:  FGC3

  History:

    The dual port ram is implemented inside the xilinx
    and can be accessed by the DSP TMS320c6727b and the MCU Rx610

    The TI compiler for the TMS320c6727b pads the structure to be multiple of the biggest type inside,
    in this case INT32U (but it can access in 8, 16 or 32 bits) or bigger (Compiler guide 8.2.1.6).
    To ensure dpram is aligned on both sides (DSP and MCU), we compile on dsp side with -gcc option.
    Thus we can use aligned(4) attribute on both side. If a struct on dsp side is actually aligned on
    bigger amount (i.e. 8 aligned if the struct or nested struct contains a 64 bit type), this will produce
    a warning: "warning #1236-D: a reduction in alignment without the "packed" attribute is ignored".
    Packed attribute is not supported on c6727. Thus we _must_ increased the aligned size to get rid of this warning,
    and add padding if necessary. This way we know we follow the same alignment on both mcu and dsp.

    the PLD allows only 16 bits access for its normal registers


    For the Rx610 the PLD implements a conversion between big endian / little endian
    but ONLY HANDLES CORRECTLY 32 BITS DATA !!!! (addresses starting on 32bits boundary)

    so all fields need to be 32bits long
\*---------------------------------------------------------------------------------------------------------*/

#ifndef BOOT_DUAL_PORT_RAM_H    // header encapsulation
#define BOOT_DUAL_PORT_RAM_H

#ifdef BOOT_DUAL_PORT_RAM_GLOBALS
#define BOOT_DUAL_PORT_RAM_VARS_EXT
#else
#define BOOT_DUAL_PORT_RAM_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------
#include <cc_types.h>


#ifdef __HC16__
#include <structs_bits_big.h>
#include <memmap_mcu.h>         // for DPRAM_BT_C32STATUS_16
#endif
#ifdef __TMS320C6727__
#include <structs_bits_little.h>
#endif
#ifdef __RX610__
#include <structs_bits_big.h>
#include <memmap_mcu.h>         // for DPRAM_START_32
#endif

#include <defconst.h>
#include <definfo.h>
#include <version.h>                // for struct version

//-----------------------------------------------------------------------------------------------------------

#define  DPRAM_ARG_MAX      10

#if defined (__RX610__)
#define PACKED_ATTRIBUTE_IF_SUPPORTED   packed,
#elif defined (__TMS320C6727__)
#define PACKED_ATTRIBUTE_IF_SUPPORTED
#endif

//-----------------------------------------------------------------------------------------------------------
// DSP zone in the Dual Port RAM
//(even the ADC came from the DSP they have their own zone)

struct TDspSlowCmdRsp_GET_INFO  // DSP_CMD_SLOW_GET_INFO
{
    INT32U  time_consumed_in_msTickIsr;             // [us]
    INT32U  time_consumed_in_interpolation;         // [us]
    INT32U  time_consumed_in_msTickIsr_max;         // [us]
    INT32U  time_consumed_in_interpolation_max;     // [us]
    INT32U  msTick;     // [us] between the last 2 ISRs
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;
struct TMcuSlowCmd_spivs
{
    union TUnion32Bits  control;    // Control SPIVS
    union TUnion32Bits  num_lwords; // Number of words for transaction
    INT32U              buf[7];     // Buffer used for rx and tx data
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;

union TDspSlowCmdRsp
{
    INT32U                          dw[DPRAM_ARG_MAX];
    char                            ch[4 * DPRAM_ARG_MAX];    // DSP_CMD_SLOW_GET_VERSION
    INT32U                          data;       // DSP_CMD_SLOW_RETURN_NEG_DATA
    struct TDspSlowCmdRsp_GET_INFO  get_info;   // DSP_CMD_SLOW_GET_INFO
    struct TMcuSlowCmd_spivs        spivs;
    union TUnion32Bits              data32;
    union TUnion64Bits              data64;
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(8)))
#endif
;

union TDspFastCmdRsp
{
    INT32U              data;   // DSP_CMD_FAST_RETURN_NEG_DATA
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;
//-----------------------------------------------------------------------------------------------------------
struct TDualPortRam_dsp_zone    // DPRAM zone writable by DSP TMS320c6727
{
    volatile union TDspSlowCmdRsp   slowRsp;
    volatile union TDspFastCmdRsp   fastRsp;
    volatile INT32U                 status;
    volatile INT32U                 version;
    volatile INT32U                 sdram_test_n;
    volatile INT32U                 irq_counter;
    volatile FP32                   regulated_current;
    volatile FP32                   regulated_voltage;
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(8)))
#endif
;
//-----------------------------------------------------------------------------------------------------------
struct TMcuSlowCmdArg_ADC_PATH  // DSP_CMD_SLOW_ADC_PATH
{
    INT32U  external_signal_access;     // ANA_SWIN
    INT32U  adc_selector_bus_or_signal; // SW_BUS
    INT32U  internal_signal_bus_access; // MPX
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;
struct TMcuSlowCmdArg_ADC_CTRL  // DSP_CMD_SLOW_ADC_CTRL
{
    INT32U  source;     // Select source for ADC
    INT32U  encoding;   // Select encoding for ADC
    INT32U  bitrate;    // Select bitrate for ADC
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;

union TMcuSlowCmdArg
{
    INT32U                                  dw[DPRAM_ARG_MAX];
    INT32U                                  data;
    INT32U                                  test_pattern;               // DSP_CMD_SLOW_TEST_PATTERN
    FP32                                    regulation_reference;
    struct TMcuSlowCmdArg_ADC_PATH          adc_path;                   // DSP_CMD_SLOW_ADC_PATH
    struct TMcuSlowCmdArg_ADC_CTRL          adc_ctrl[FGC_N_ADCS];       // DSP_CMD_SLOW_ADC_CTRL
    struct TMcuSlowCmd_spivs                spivs;
    // I only needs 24 for the moment but due to the PLD implementation of the big / little endian conversion (based on 32bits words) I made it multiple of 32bits words
    char                                    str[32];
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;

union TMcuFastCmdArg
{
    INT32U                  data;
    INT32S                  int32s;
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;
//-----------------------------------------------------------------------------------------------------------
struct TDualPortRam_mcu_zone        // DPRAM zone writable by M32C87 / RX610
{
    volatile INT32U                 fastCmd;        // Set by MCU, reset to 0 by DSP when finished
    volatile INT32U                 slowCmd;        // Set by MCU, reset to 0 by DSP when finished
    volatile INT32U                 mode;
    volatile union TMcuFastCmdArg   fastArg;
    volatile union TMcuSlowCmdArg   slowArg;


#if (FGC_CLASS_ID == 60)
    INT32U      pll_debug_data[6];
#endif

}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;
//-----------------------------------------------------------------------------------------------------------
// ADC area

struct TDualPortRam_AnalogueData
{
    volatile INT32U             n_samples;      // number of samples to be taken
    volatile INT32U             is_first_sample;// 1st sample flag
    volatile INT32U             time_consumed_us;// Elapsed time for reading out data in us
    volatile INT32U             pad0;           // Keep 8 alignment
    volatile INT32S             max[4];         // Max values for each channel
    volatile INT32S             min[4];         // Min values for each channel
    volatile INT32S             value[4];       // Average value for each channel
    volatile union TUnion64Bits sum[4];         // Sum values for each channel
    volatile INT32U             fast_adc_sample[FGC_N_ADCS][100];
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(8)))
#endif
;

//-----------------------------------------------------------------------------------------------------------
struct TDualPortRam_ExchangeZone
{
    struct TDualPortRam_dsp_zone        dsp;
    struct TDualPortRam_mcu_zone        mcu;
    struct TDualPortRam_AnalogueData    adc;
    // ToDo: convert this to an union, to have access to the full 16Kb
    //  INT32U                              int32u[0x1000]; // 16 K bytes = 4K_32bit_words
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(8)))
#endif
;
//-----------------------------------------------------------------------------------------------------------
BOOT_DUAL_PORT_RAM_VARS_EXT struct TDualPortRam_ExchangeZone    *   dpram
#ifdef BOOT_DUAL_PORT_RAM_GLOBALS

#ifdef __HC16__
        // ToDo: which one is the correct DPRAM start for FGC2 BOOT ?
        // #define DPRAM_16                            0xD800                        // DPRAM zones
        // #define DPRAM_BT_A                          ((REG32U *)0x0000E800)        // BOOT DPRAM zones
        // #define DPRAM_BT_C32STATUS_16               0xE800                        // C32 status
    = (struct TDualPortRam_ExchangeZone *) DPRAM_BT_C32STATUS_16
#endif

#ifdef __RX610__
      = (struct TDualPortRam_ExchangeZone *) DPRAM_START_32
#endif

#ifdef __TMS320C6727__
#endif

#ifdef __TMS320C32__
        // DPRAM is ((struct dpram*)0x903A00)  --> 0xE800 in M68HC16 page
#endif

#endif
        ;
//-----------------------------------------------------------------------------------------------------------

#endif  // BOOT_DUAL_PORT_RAM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: boot_dual_port_ram.h
\*---------------------------------------------------------------------------------------------------------*/
