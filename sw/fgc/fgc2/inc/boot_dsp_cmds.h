/*---------------------------------------------------------------------------------------------------------*\
  File:     boot_dsp_cmds.h

  Contents: constants for FCG shared by CPU and DSP

\*---------------------------------------------------------------------------------------------------------*/

#ifndef BOOT_DSP_CMDS_H // header encapsulation
#define BOOT_DSP_CMDS_H

#ifdef BOOT_DSP_CMDS_GLOBALS
#define BOOT_DSP_CMDS_VARS_EXT
#else
#define BOOT_DSP_CMDS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

enum dsp_command
{
    DSP_CMD_NOTHING,
    DSP_CMD_SLOW_TST_SDRAM,
    DSP_CMD_SLOW_TST_PLD_DPRAM,
    DSP_CMD_SLOW_ADC_PATH,
    DSP_CMD_SLOW_ADC_CTRL,
    DSP_CMD_SLOW_TEST_PATTERN,
    DSP_CMD_SLOW_SET_DAC1,
    DSP_CMD_SLOW_SET_DAC2,
    DSP_CMD_SLOW_RETURN_NEG_DATA,
    DSP_CMD_SLOW_GET_INFO,
    DSP_CMD_SLOW_GET_SPY_INFO,
    DSP_CMD_SLOW_SET_REFERENCE,
    DSP_CMD_SLOW_START_STOP_REGULATION,
    DSP_CMD_SLOW_SELECT_ALGORITHM,
    DSP_CMD_SLOW_StrToFP32,
    DSP_CMD_SLOW_StrToFP64,
    DSP_CMD_FAST_RETURN_NEG_DATA,
    DSP_CMD_FAST_STRESS_TST_PLD_DPRAM,
    DSP_CMD_FAST_CLEAR_INFO,
    DSP_CMD_SLOW_SET_SPY_OWNER,
    DSP_CMD_SLOW_SPY_TEST,
    DSP_CMD_SLOW_SPY_ONE_SHOT,
    DSP_CMD_SLOW_SEND_VALUE_VIA_SPIVS,
    DSP_CMD_SLOW_GET_SPIVS_RETURN_VALUE,
    DSP_CMD_SLOW_GET_VERSION_UNIXTIME_A,
    DSP_CMD_SLOW_GET_VERSION_HOSTNAME,
    DSP_CMD_SLOW_GET_VERSION_ARCHITECTURE,
    DSP_CMD_SLOW_GET_VERSION_CC_VERSION,
    DSP_CMD_SLOW_GET_VERSION_USER_GROUP,
    DSP_CMD_SLOW_GET_VERSION_DIRECTORY,
    DSP_CMD_SLOW_FORCE_ADC_TYPE,
    DSP_CMD_FAST_SIGNAL_MEASURE,
    DSP_CMD_LAST        // dummy to have the number of elements to use in the for/while loops
};

// DSP STATUS

#define  DSP_STS_NOTHING                0x0000
#define  DSP_STS_ON                     0x0001
#define  DSP_STS_DPRAM_BUSY             0x0002
#define  DSP_STS_SDRAM_FAULT            0x0004
#define  DSP_STS_PLDDPRAM_FAULT         0x0008
#define  DSP_STS_BAD_IRQ                0x0010
#define  DSP_STS_CMD_UNKNOWN            0x0020
//                                      0x0040
#define  DSP_STS_SPY_OVERUN             0x0080
//                                      0x0100
//                                      0x0200
//                                      0x0400
//                                      0x0800

//-----------------------------------------------------------------------------------------------------------

#endif  // BOOT_DSP_CMDS_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of boot_dsp_cmds.h
\*---------------------------------------------------------------------------------------------------------*/
