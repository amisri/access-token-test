/*---------------------------------------------------------------------------------------------------------*\
  File:         deftypes.h

  Purpose:      Type declarations for property definitions
\*---------------------------------------------------------------------------------------------------------*/
#ifndef DEFTYPES_H
#define DEFTYPES_H

#include <cc_types.h>
#include <definfo.h>

// Global constants

#define ABSTIME_SIZE  8  // Size of ABSTIME property element on FGC2 (is it sizeof(struct timeval) on FGCD)

// Type to use for string constants

#ifdef __HC16__
typedef char * FAR def_const_string;
#else
typedef const char * def_const_string;
#endif

// Put definitions in ROM
// Not possible on FGC2 (why?)

#define CONST

// Type to use with in defprops.h for struct prop flags to truncate flags to the size of the field
// 16-bit for FGCs and 32-bit for FGCD

typedef INT16U          def_props_flags;

// MIN/MAX macros
// Caution: do not use pre/post-increment operators with those macros, that can lead to strange results
// (E.g., never write z = MAX(x++,y))

#define MIN(a,b)        (((a) < (b)) ? (a) : (b))
#define MAX(a,b)        (((a) > (b)) ? (a) : (b))


// DSP loader constants

// /*! MCU handshake request value */
#define  DSP_LOADER_MCU_REQUEST      0xAAAAAAAA
// /*! DSP handshake acknowdlge value */
#define  DSP_LOADER_DSP_ACK          0x55555555

// Integer limits structures

struct int_limits                                       // Integer limits structure
{
    INT32S              min;                            // Min integer limit
    INT32S              max;                            // Max integer limit
};

struct intu_limits                                      // Unsigned integer limits structure
{
    INT32U              min;                            // Min unsigned integer limit
    INT32U              max;                            // Max unsigned integer limit
};

// Floating point limits structure

struct float_limits                                     // Floating point limits structure
{
    DP_IEEE_FP32        min;                            // Min float limit
    DP_IEEE_FP32        max;                            // Max float limit
};


#endif

// EOF

