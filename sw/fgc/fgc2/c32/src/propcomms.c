/*---------------------------------------------------------------------------------------------------------*\
  File:         propcomms.c

  Purpose:      FGC DSP library property communications functions - common to all FGC classes

  Notes:        PPM Properties

                Properties with the PPM flag set are assumed to to an array of properties which will be
                selected using the mux index.  This is used for sub-device properties (1-16) in class 59
                and for PPM properties (0-MAX_USER) in classes 51 and 53.

                PPM Properties

                For Classes 51 and 53, some PPM properties are part of the ppm structure array.  The
                address of this array and the length of the struct are given to this library so that the
                PropComms() function can know if a property is included and if so, it uses the mux index
                to access the relevant element of the array.  Note that all PPM properties must have the
                PPM flag set and have the INDIRECT_NELS flag set if the length of the property can be
                different for each user.

                INDIRECT_NELS flag

                If this flag is set for a property, the n_elements field defines a pointer to the
                number of elements in the property.  If the property is PPM but not PPM, then it points
                to an array, while for PPM properties it points to a scalar value in the ppm structure
                array.
\*---------------------------------------------------------------------------------------------------------*/

#include <stdbool.h>

#include <propcomms.h>
#include <string.h>             // ANSI string library, memset()
#include <defconst.h>           // Global FGC DSP constants

struct hash_entry               // Required to satisfy fgc_parser_consts.h
{
    uint32_t    dummy;
};

#include <fgc_parser_consts.h>  // Global parser constants - contains property flags
#include <fgc_errs.h>           // Include global FGC error constants
#include <dpcom.h>              // Dual port ram structures
#include <memmap_dsp.h>
#include <dsp_dependent.h>      // for DISABLE_INTS
#include <dpcom.h>              // for struct prop_buf
#include <rtcom.h>              // for rtcom global variable
#include <macros.h>             // for Test()
#include <lib.h>
#include <property.h>

/*---------------------------------------------------------------------------------------------------------*/
void PropComms(volatile struct prop_buf *pbuf)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the MCU FcmTsk(), PubTsk() or ScmTsk() wants to Set or Get a block of
  property data.

  [TMS320C32 only] If the property contains FP32 data, each value must be converted between TI and IEEE floating point
  formats. For non FP32 values, the data can simply be copied.

  If the last block of data is set and the property has shrunk, the function must clear all the excess
  elements to zero.

  This function is called in BGP context.

  CAUTION: BEFORE MODIFYING THIS CODE. Consider that this function was written in such a way that it is
           easily portable, for example between DSPs TMS320C32 and TMS320C6727.
           The function uses standard C types such as
           size_t, char*, etc. to ensure pointer manipulation, memcpy, memset will work correctly on both
           DSPs despite a very different memory layout.
           TMS320C32 uses 32-bit addressing and therefore a char is 4 bytes on that platform,
           whereas TM320C6727 uses byte addressing.
           All sizes returned by sizeof on the TMS320C32 are in 32-bit words unit.
           On the TMS320C6727, size are in bytes.

           THE PARTS OF THE FUNCTION UNDER __TMS320C32__ CONDITIONALS are only to handle TI's specific
           floating point format on the C32 DSP. The function handles IEEE floats by default.

\*---------------------------------------------------------------------------------------------------------*/
{
    const bool get_f      = Test(pbuf->action, FGC_FIELDBUS_FLAGS_GET_CMD);
    const bool set_f      = Test(pbuf->action, FGC_FIELDBUS_FLAGS_SET_CMD);
    const bool last_pkt_f = Test(pbuf->action, FGC_FIELDBUS_FLAGS_LAST_PKT);

    struct prop * property_meta = &prop[pbuf->dsp_prop_idx];

    const size_t elem_size  = PROPERTY_ELEM_SIZEOF(property_meta->el_size);
    const size_t prop_size  = property_meta->max_elements * elem_size;    // Size of property data for one user index
    const size_t blk_offset = pbuf->blk_idx * sizeof(pbuf->blk);          // Block start index in property value

    size_t                copy_size;            // size to copy
    uint32_t                *nelem_addr;          // Pointer to the number of elements
    char                  *buf_addr;            // Pointer to start of the value buffer
    void                  *dsp_prop_ptr;        // Pointer to FP32 property value
    void                  *dpram_ptr;           // Pointer to the shared memory
    DP_IEEE_FP32          *dpvalue;             // Pointer to float value in DPRAM
    FP32                  *fpvalue;             // Pointer to float value in DSP own memory
    uint32_t                fp_idx;               // Loop variable

    pbuf->dsp_rsp = PROP_DSP_RSP_OK;

    // Sanity check on the DSP property

    if(Test(property_meta->flags,PF_INDIRECT_N_ELS) &&           // If indirect number of elements
       property_meta->n_elements == NULL)                        // and n_elements (in this case, a pointer) is NULL
    {
        pbuf->dsp_rsp = PROP_DSP_RSP_ERR_NELEM;         // Indicate error to the MCU
        pbuf->action  = 0;                              // Cancel request to indicate the action is completed
        return;
    }

    if(property_meta->value == NULL)                             // If the data pointer is NULL
    {
        pbuf->dsp_rsp = PROP_DSP_RSP_ERR_BUF;           // Indicate error to the MCU
        pbuf->action  = 0;                              // Cancel request to indicate the action is completed
        return;
    }

    // Process the MCU request

    if(Test(property_meta->flags, PF_SUB_DEV))
    {
        // Nb of elements are consecutive, as well as data buffers

        nelem_addr = (uint32_t*)(property_meta->n_elements) + pbuf->mux_idx;
        buf_addr   = (char*)property_meta->value + pbuf->mux_idx * prop_size;
    }
    else if(pbuf->mux_idx != 0 && Test(property_meta->flags, PF_PPM))
    {
        // If property value is not in PPM range

        if((uint32_t)property_meta->value <  rtcom.ppm_start ||
           (uint32_t)property_meta->value >= rtcom.ppm_end)
        {
            // Nb of elements are consecutive, as well as data buffers

            nelem_addr = (uint32_t*)(property_meta->n_elements) + pbuf->mux_idx;
            buf_addr   = (char*)property_meta->value + pbuf->mux_idx * prop_size;
        }
        else // Property is in the PPM range
        {
            // Nb of elements are separated by sizeof(ppm), as well as data buffers

            nelem_addr = (uint32_t*)(property_meta->n_elements + pbuf->mux_idx * rtcom.ppm_length);
            buf_addr   = (char*)property_meta->value + pbuf->mux_idx * rtcom.ppm_length;
        }
    }
    else // Not PPM/SUBDEV or mux_idx is zero
    {
        // Assume n_elements is a pointer

        nelem_addr = (uint32_t*)property_meta->n_elements;
        buf_addr   = (char*)property_meta->value;
    }

    if(!Test(property_meta->flags,PF_INDIRECT_N_ELS))            // If not an indirect number of elements
    {
        nelem_addr = (uint32_t*)&property_meta->n_elements;        // n_elements is a scalar. Make nelem_addr point to it.
    }

    // Calculate the size of the block to be copied

    dpram_ptr     = (void*)&pbuf->blk;
    dsp_prop_ptr  = (void*)(buf_addr + blk_offset);

    if(property_meta->float_f)
    {
        dpvalue  = (DP_IEEE_FP32*)pbuf->blk.fp;
        fpvalue  = (FP32*)dsp_prop_ptr;
    }

    // Transfer data according to action

    if(get_f)                                           // If GET action
    {
        const uint32_t n_els = MIN(*nelem_addr, property_meta->max_elements);

        copy_size = n_els * elem_size - blk_offset;                 // Number of words from start of block to end of property
        copy_size = MIN(copy_size, sizeof(pbuf->blk));      // Clip to size of one block

        if(!property_meta->float_f)                              // If not FP32, use memcpy
        {
            memcpy(dpram_ptr, dsp_prop_ptr, copy_size);
        }
        else                                            // else FP32
        {
            for(fp_idx = 0; fp_idx < copy_size; fp_idx++)               // Transfer each value...
            {
                *(dpvalue++) = ToIEEE(*(fpvalue++));    // converting to IEEE format
            }
        }

        pbuf->n_elements = n_els;                   // Return number of property elements to MCU
    }
    else                                                // else not GET action
    {
        // Get number of element of the DSP property

        const uint32_t n_els_old = MIN(*nelem_addr,      property_meta->max_elements);
              uint32_t n_els_new = MIN(pbuf->n_elements, property_meta->max_elements);

        if(set_f)                                       // If SET words need to be transferred
        {
            copy_size = n_els_new * elem_size - blk_offset;                 // Number of words from start of block to end of property
            copy_size = MIN(copy_size, sizeof(pbuf->blk));      // Clip to size of one block

            if(!property_meta->float_f)                          // If not FP32, use memcpy
            {
                memcpy(dsp_prop_ptr, dpram_ptr, copy_size);
            }
            else
            {
                for(fp_idx = 0; fp_idx < copy_size; fp_idx++)           // Transfer each value...
                {
                    *(fpvalue++) = FromIEEE(*(dpvalue++));                      // converting from IEEE format
                }
            }
        }

        if(last_pkt_f)                                  // If setting last block for property
        {
            if(n_els_new < n_els_old)                   // If data has shrunk
            {
                if(Test(property_meta->flags,PF_DONT_SHRINK))    // If DONT_SHRINK flag set
                {
                    n_els_new = n_els_old;              // Don't change the length
                }
                else                                    // else DONT_SHRINK is not set
                {
                    memset((void*)(buf_addr + n_els_new * elem_size),   // Clear excess values
                           (property_meta->float_f ? FP32ZERO : 0),              // Note: on C32, 0.0 is not integer zero
                           (n_els_old - n_els_new) * elem_size);
                }
            }
        }
        else                                            // else not last block
        {
            if(n_els_new <= n_els_old)                  // If length has shrunk
            {
                n_els_new = n_els_old;                  // Don't change the length until last block
            }
        }

        if(n_els_new != n_els_old)                      // if number of elements has changed
        {
            *nelem_addr = n_els_new;                    // Save n_elements (indirect)
        }

        if(last_pkt_f && property_meta->pars_idx)                // If property linked to a parameter group
        {
            DISABLE_INTS;
            UPDATE_PARS(property_meta->pars_idx);                // Set change flag for pars group
            ENABLE_INTS;
        }
    }

    pbuf->action = 0;                           // Cancel request to indicate the action is completed
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: propcomms.c
\*---------------------------------------------------------------------------------------------------------*/
