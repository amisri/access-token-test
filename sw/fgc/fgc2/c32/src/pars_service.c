/*---------------------------------------------------------------------------------------------------------*\
  File:         pars_service.c

  Purpose:      FGC DSP library parameter parsing functions - common to all FGC2 classes

  Notes:        The FGC2 MCU (HC16) is very slow at floating point maths (5 KFlops!) while
                the DSP is able to do about 10 MFlops.  The MCU gets the help from the DSP
                using these functions.  It can parse ASCII data into binary and also convert
                binary floating point values into ASCII.
\*---------------------------------------------------------------------------------------------------------*/

#include <pars_service.h>

#include <string.h>     // ANSI string library
#include <stdlib.h>     // ANSI stdlib library
#include <stdio.h>      // ANSI stdlib library
#include <errno.h>      // ANSI errors library
#include <defconst.h>   // Global FGC DSP constants
#include <fgc_errs.h>   // Include global FGC error constants
#include <dpcom.h>      // Dual port ram structures
#include <definfo.h>    // for FGC_CLASS_ID
#include <macros.h>     // for Set(), Clr(), Test()
#include <lib.h>        // for FromMCUString()
#include <dsp_dependent.h>      // for ToIEEE(), FromIEEE()
#include <dpcom.h>      // for struct pars_buf
#include <pars.h>       // for struct cmd_pkt_parse_buf
#include <deftypes.h>   // for int_limits / float_limits
#include <main.h>
#include <mcu_dsp_common.h>

struct fp_limits                                       // Floating point limits structure
{
    FP32               min;                            // Min FP32 limit
    FP32               max;                            // Max FP32 limit
};

/*---------------------------------------------------------------------------------------------------------*/
void ParsePkt(volatile struct pars_buf *pars, struct cmd_pkt_parse_buf *buf)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the MCU wants help to parse a command packet containing Integers,
  Floats or RelTime values.  A token can be split across packets so the function must find the
  last token delimiter character (,) in the packet and keep all characters following this in the start
  of the buffer for the next time the function is called.

  The function supports three parameter types:

  1. Integers

     All integers are parsed as signed longs, and integer limits are the same.  This means that even
     uint32_t properties are limited to the range 0 to (2^31)-1.

  2. RelTime

     Relative time is supplied by the user as a positive floating point value in seconds.  No RelTime value
     may exceed 400000.  The function parses the value as a FP32, but internally, RelTime values are stored
     as uint32_t longs in FG timebase units.

  3. Floats

     Floating point values have a different format in the DSP than in the MCU.  So after parsing the value
     to FP32, it must be converted to IEEE format before being returned to the MCU.

  4. Points

     Point values contain two floats separated by a pipe character.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t              i;                      // Loop variable
    uint32_t              out_of_limits_f;        // Out of limits flag
    uint32_t              base;                   // Integer base (10=decimal 16=hex)
    uint32_t              type;                   // Parameter type: PKT_FLOAT|PKT_INT|PKT_RELTIME|PKT_INTU
    uint32_t              n_pars;                 // Number of parameters converted
    uint32_t              n_pkt_chars;            // Number of characters in command pkt
    uint32_t              n_ex_chars;             // Number of excess characters in command pkt
    uint32_t              out_idx;                // Out index for new pkt
    int32_t              intval  = 0;            // Integer value
    uint32_t              intuval = 0;            // Unsigned integer value
    FP32                fptime  = 0.0;          // Floating point time value for POINT types
    FP32                fpval   = 0.0;          // Floating point value
    struct fp_limits    fp_limits;              // Floating point limits
    char *              pkt_buf;                // Pointer to start of uncompressed pkt buffer
    volatile uint32_t *   dppkt_buf;              // Pointer within incoming pkt buffer in DPRAM
    volatile struct cmd_pkt * cmd_pkt;          // Pointer to command pkt structure (selected from FP32 buf)

    // Prepare local variables from new packet data in DPRAM

    n_pars      = 0;
    n_ex_chars  = 0;
    type        = pars->type;
    cmd_pkt     = &pars->pkt[pars->pkt_buf_idx];
    dppkt_buf   = &cmd_pkt->buf[0];
    n_pkt_chars = cmd_pkt->n_chars;                     // Number of new characters in pkt buffer

    // Check for first packet

    if(Test(cmd_pkt->header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT)) // If pkt is first for command
    {
        buf->n_carryover_chars = 0;                                   // Cancel any left over characters
    }

    // Uncompress new packet characters into byte array

    pkt_buf = &buf->buf[buf->n_carryover_chars];        // Initialise pkt_buf pointer to after carryover chars

    FromMCUString(pkt_buf, dppkt_buf, (n_pkt_chars + 3) / 4);

    pkt_buf = &buf->buf[0];                             // Init pointer to start pkt buffer
    out_idx = pars->out_idx;                            // Index to start of first new token
    pars->out_idx = n_pkt_chars;                        // Set out_idx to end of packet to indicate it's empty
    n_pkt_chars += buf->n_carryover_chars;              // Total number of characters in pkt buffer

    // Check for incomplete token at the end of the packet

    if(!Test(cmd_pkt->header_flags,FGC_FIELDBUS_FLAGS_LAST_PKT))
    {
        while(n_pkt_chars > out_idx && pkt_buf[--n_pkt_chars] != ',')   // Scan backwards for ','
        {
            if(++n_ex_chars >= MAX_CMD_TOKEN_LEN)                           // If token exceeds max length
            {
                pars->errnum       = FGC_SYNTAX_ERROR;                          // Report syntax error
                pars->par_err_idx = 0;                                          // Unknown par index
                pars->type        = 0;                                          // Report action complete
                return;
            }
        }

        if(pkt_buf[n_pkt_chars] != ',')                                 // If first token
        {
            n_pkt_chars--;
            goto end;                                                           // Jump to end
        }
    }

    pkt_buf[n_pkt_chars] = 0;                           // Null terminate previous token

    if(type == PKT_FLOAT || type == PKT_POINT
#if FGC_CLASS_ID == 59
       || type == PKT_RELTIME
#endif
      )
    {
        fp_limits.min = FromIEEE(pars->limits.fp.min);          // Conv FP limits from IEEE->TI format
        fp_limits.max = FromIEEE(pars->limits.fp.max);
    }

    // Try to parse each token in turn

    pkt_buf = &buf->buf[out_idx];                       // Set pkt buf pointer to start of first token

    for(;;)
    {
        // Prepare the result with a signal for NO_FLOAT or NO_INT in case the token is empty

        switch(type)
        {
            case PKT_FLOAT:

                pars->results.intu[n_pars] = PAR_NO_FLOAT;

                break;

            case PKT_POINT:

                // Points use two floats per element and signal NO_FLOAT using the first element when not present

                pars->results.intu[n_pars*2] = PAR_NO_FLOAT;

                break;

            default:    // INT, INTU and RELTIME

                pars->results.intu[n_pars] = PAR_NO_INT;
                break;
        }

        // Parse the next token

        if(*pkt_buf == ' ')                     // If leading space
        {
            pkt_buf++;                                  // Skip space
        }

        if(*pkt_buf == '\0')                    // If end of pkt
        {
            n_pars++;                                   // Return NO_INT/NO_FLOAT value
            break;                                      // End processing of pkt
        }

        if(*pkt_buf == ',')                     // If no token before comma
        {
            n_pars++;                                   // Return NO_INT/NO_FLOAT value
            pkt_buf++;                                  // Skip comma
        }
        else                                    // else token present
        {
            errno = 0;                                          // Reset errno before the call to strtol/strtoul/strtod

            if(type == PKT_INT || type == PKT_INTU)             // If INT parameter (signed or unsigned)
            {
                pars->errnum = FGC_BAD_INTEGER;                 // Prepare to return BAD_INTEGER if the parsing fails

                if(pkt_buf[0] == '0' &&                         // If prefix is "0x" or "0X"
                  (pkt_buf[1] == 'x' || pkt_buf[1] == 'X'))
                {
                    base = 16;                                          // Prepare for hex value
                    pkt_buf += 2;
                }
                else                                            // else
                {
                    base = 10;                                          // Prepare for decimal value
                }

                if(type == PKT_INT)
                {
                    intval = strtol(pkt_buf,&pkt_buf,base);             // Parse token as signed integer
                    out_of_limits_f = (intval < pars->limits.integer.min ||
                                       intval > pars->limits.integer.max);
                }
                else
                {
                    // The strtoul() implementation in the TI TMS320C32 library does not take
                    // into account negative numbers, as specified in the standard. pkt_buf
                    // is not modified and errno is not set to ERANGE. An explicit check for
                    // '-'  must be added.

                    out_of_limits_f = (*pkt_buf == '-');

                    if (!out_of_limits_f)
                    {
                        intuval = strtoul(pkt_buf, &pkt_buf, base);         // Parse token as unsigned integer
                        out_of_limits_f = (intuval < pars->limits.unsigned_integer.min ||
                                           intuval > pars->limits.unsigned_integer.max);
                    }
                }
            }
            else if(   type == PKT_FLOAT                        // else FLOAT
                    || type == PKT_RELTIME)                     // or RELTIME parameter (Class 59 only)
            {
                pars->errnum = FGC_BAD_FLOAT;                 // Prepare to return BAD_FLOAT if the parsing fails

                fpval = strtod(pkt_buf, &pkt_buf);                      // Convert token to FP32
                out_of_limits_f = (fpval < fp_limits.min || fpval > fp_limits.max);
            }
            else    // type must be PKT_POINT
            {
                char * tail_ptr;

                pars->errnum = FGC_BAD_POINT;                           // Prepare to return BAD_FLOAT if the parsing fails

                fptime = strtod(pkt_buf, &tail_ptr);                     // Convert first float in token to FP32 (point time)

                if(tail_ptr == pkt_buf)
                {
                    goto exit_with_error;
                }

                pkt_buf = tail_ptr;

                out_of_limits_f = (fptime < 0.0 || fptime > 4.0E5);

                if(out_of_limits_f)
                {
                    pars->errnum = FGC_OUT_OF_LIMITS;
                    goto exit_with_error;
                }

                // Check that pipe delimiter is the next character

                if(*pkt_buf == '|')
                {
                    pkt_buf++;

                    // Convert second float in token to FP32 (point ref)

                    fpval = strtod(pkt_buf, &tail_ptr);

                    if(tail_ptr == pkt_buf)
                    {
                        goto exit_with_error;
                    }

                    pkt_buf = tail_ptr;

                    out_of_limits_f |= (fpval < fp_limits.min || fpval > fp_limits.max);
                }
                else
                {
                    goto exit_with_error;
                }
            }

            // Check errno == ERANGE is a portable way to detect "out of range" errors on uint32_t and int32_t (like for
            // example setting a uint32_t to -1).

            if(errno == ERANGE || out_of_limits_f)              // If parameter was out of limits
            {
                pars->par_err_idx = n_pars;                             // Report parameter index for error
                pars->errnum      = FGC_OUT_OF_LIMITS;                  // Report error
                pars->type        = 0;                                  // Cancel request to indicate end of action
                return;
            }

            if(*pkt_buf == ' ')                                 // If trailing space
            {
                pkt_buf++;                                              // Skip space
            }

            if( errno ||                                        // If not a valid delimiter or other type of lib error
               (*pkt_buf && *pkt_buf != ','))
            {
                exit_with_error:

                pars->par_err_idx = n_pars;                             // Report parameter index for error
                pars->type        = 0;                                  // Cancel request to indicate end of action
                return;
            }

            switch(type)
            {
            case PKT_INT:

                pars->results.ints[n_pars++] = intval;
                break;

            case PKT_INTU:

                pars->results.intu[n_pars++] = intuval;
                break;

#if FGC_CLASS_ID == 59
            case PKT_RELTIME:

                pars->results.intu[n_pars++] =  FG_CONV_S_TO_TB(fpval);
                break;
#endif

            case PKT_FLOAT:

                pars->results.fp[n_pars++] = ToIEEE(fpval);
                break;

            case PKT_POINT:

                pars->results.point[n_pars].time = ToIEEE(fptime);
                pars->results.point[n_pars].ref  = ToIEEE(fpval);
                n_pars++;
                break;
            }

            if(!*pkt_buf)                                       // Delimiter is nul
            {
                break;                                                  // End processing of this packet
            }
            else                                                // else delimiter is a comma
            {
                pkt_buf++;                                              // Skip over comma
            }
        }
    }

    // Transfer extra character to start of pkt buffer for next time

    end:

    pkt_buf = &buf->buf[0];                             // Reinit pointer to start of pkt buffer

    for(i=0;i < n_ex_chars;i++)                         // For extra characters
    {
        pkt_buf[i] = pkt_buf[++n_pkt_chars];                    // Transfer extra characters to start of buffer
    }

    errno = 0;

    // Return results to MCU

    pars->pkt_buf_idx      = n_ex_chars;                // Remember number of extra character for next time
    buf->n_carryover_chars = n_ex_chars;                // Remember number of extra character for next time
    pars->n_pars           = n_pars;                    // Return number of parameters found
    pars->errnum           = 0;                         // Reset the errnum
    pars->type             = 0;                         // Cancel request to indicate that action is completed
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars_service.c
\*---------------------------------------------------------------------------------------------------------*/
