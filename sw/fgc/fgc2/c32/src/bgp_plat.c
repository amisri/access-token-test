/*---------------------------------------------------------------------------------------------------------*\
  File:         bgp_plat.c

  Purpose:      FGC2 DSP Software - Background Processing Functions, platform specifics.
\*---------------------------------------------------------------------------------------------------------*/

#define BGP_PLAT_GLOBALS

#include <bgp_plat.h>
#include <bgp.h>
#include <dsp_dependent.h>
#include <bgcom.h>
#include <dpcom.h>
#include <dpcls.h>
#include <lib.h>                // For DebugMemTransfer()

#if FGC_CLASS_ID == 53
    #include <pal.h>
#endif

/*---------------------------------------------------------------------------------------------------------*/
void BgpPlatform(void)
/*---------------------------------------------------------------------------------------------------------*\
  This is the DSP BackGround Processing part that is specific to FGC2.
\*---------------------------------------------------------------------------------------------------------*/
{

#if FGC_CLASS_ID == 53
    // Check for request to reset communications with the PAL interface

    if(dpcls->dsp.pal.reset_req == 2)
    {
        PalReset();
    }
#endif

    // Flash front panel LED to indicate that IsrMst interrupt is running
    // The DSP will stay in that IDLE loop until the next periodic ISR.

    while(bgp_no_pending_action && !bgcom.run_f)        // Wait for IsrMst() to set flag
    {
        bgcom.i = TICTAC;                                       // Read TICTAC to trigger memory refresh
        bgcom.idle_counter++;                                   // Counter for Spy to trace BG CPU usage

        if(!bgcom.led_counter)                                  // If downcounter reaches zero
        {
            LED1_TOGGLE;                                        // Toggle LED1
            bgcom.led_counter     = 0x20000;                    // Reset counter
            dpcom->dsp.mcu_counter_dsp_alive.int32u = 0;        // signal MCU that we are alive
        }
        else
        {
            --bgcom.led_counter;
            if ( !(bgcom.led_counter & ~0x7F) )                 // Once per 128 iterations
            {
                DebugMemTransfer();                             // Transfer debug memory zone to MCU
            }
        }
    }

    bgcom.run_f = false;
    dpcom->dsp.mcu_counter_dsp_alive.int32u = 0;                // signal MCU that we are alive
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: bgp_plat.c
\*---------------------------------------------------------------------------------------------------------*/

