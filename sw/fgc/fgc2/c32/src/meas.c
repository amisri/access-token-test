/*---------------------------------------------------------------------------------------------------------*\
  File:         meas.c

  Purpose:      FGC DSP Software - contains measurement processing functions

  Notes:        All functions there run at interrupt level under IsrPeriod().
\*---------------------------------------------------------------------------------------------------------*/

#define MEAS_GLOBALS

#include <meas.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <serprt30.h>           // for SERIAL_PORT_ADDR()
#include <dsp_dependent.h>      // for ToIEEE(), WAITSPY
#include <rtcom.h>              // for rtcom global variable
#include <reg.h>                // for reg, load global variables
#include <main.h>               // for dcct_select, state_pc
#include <adc.h>                // for adc, adc_chan global variables
#include <cyc.h>                // for cyc global variable
#include <cal.h>                // for cal global variable
#include <sim.h>                // for sim global variable
#include <macros.h>             // for Test()
#include <lib.h>                // for
#include <diag.h>               // for diag global variable
#include <definfo.h>            // for FGC_CLASS_ID
#include <defconst.h>           // for FGC_PC states
#include <macros.h>             // for Test() macro
#include <sim.h>                // for sim global variable, needed by defprops_dsp_fgc.h
#include <property.h>
#include <defprops_dsp_fgc.h>   // for PROP_ADC_FILTER_UNEXP_RESETS_HIST_CHAN

#if FGC_CLASS_ID == 53
    #include <pal.h>            // for pal global variable
#endif

#include <math.h>               // for fabs()

#include <structs_bits_little.h>

/*---------------------------------------------------------------------------------------------------------*/
void MeasHistory(struct meas_hist *hist, FP32 meas)
/*---------------------------------------------------------------------------------------------------------*\
  This function calculates the measurement value and rate of change, based on four samples, using
  a least-squares regression (see Z:\projects\fgc\sw\fgc2\c32\fgc2_DspProg\doc\least_squares.xlsx).
  It is used to allow the regulation loop to be closed even if there is not enough of a measurement
  history to fill the RST history buffer.  This is the case for the field measurement with the POPS
  converter.
  To be called on the iteration period.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32    * buf = hist->buf;
    uint32_t    idx = hist->idx = ((hist->idx + 1) & MEAS_HIST_MASK);

    buf[idx] = meas;

    // Least-squares linear regression through four points

    hist->rate = property.reg.v.rst_pars.freq * 2.0 / 20.0 *
                 (3.0 * (buf[ idx                      ] - buf[(idx - 3) & MEAS_HIST_MASK]) +
                        (buf[(idx - 1) & MEAS_HIST_MASK] - buf[(idx - 2) & MEAS_HIST_MASK]));

    // Estimate value now (extrapolate 1.5 periods from middle of 4-point average)

    hist->meas = 0.25 * (buf[0] + buf[1] + buf[2] + buf[3]) + 1.5 * property.reg.v.rst_pars.period * hist->rate;

    // Estimate value after one regulation period (B/I/V)

    hist->estimated = hist->meas + reg.active->rst_pars.period * hist->rate;
}
/*---------------------------------------------------------------------------------------------------------*/
void MeasHistoryRegPeriod(struct meas_hist *hist, FP32 meas)
/*---------------------------------------------------------------------------------------------------------*\
  This function calculates the rate of change of the measurement, based on four samples, using
  a least-squares regression (see Z:\projects\fgc\sw\fgc2\c32\fgc2_DspProg\doc\least_squares.xlsx).
  To be called on the regulation period.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32    * buf = hist->buf;
    uint32_t    idx = hist->idx = ((hist->idx + 1) & MEAS_HIST_MASK);

    buf[idx] = meas;

    // Least-squares linear regression through four points

    hist->rate = reg.active->rst_pars.freq * 2.0 / 20.0 *
                 (3.0 * (buf[ idx                      ] - buf[(idx - 3) & MEAS_HIST_MASK]) +
                        (buf[(idx - 1) & MEAS_HIST_MASK] - buf[(idx - 2) & MEAS_HIST_MASK]));
}
/*---------------------------------------------------------------------------------------------------------*/
void MeasSelect(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function analyses the ADC signal statuses and determines which signal to use for feedback.
  It also controls the measurement fault flag and finally it decide whether or not to request an ADC reset.
  The function is not called during ADC16/DAC calibration.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      c;

    // ADC filter reset test

    static uint32_t      adc_ready_delay_ms;              // Count down before allowing ADC filters reset.
    bool            adc_reset_check_f = false;       // Flag that indicates the test for the ADC filters reset is allowed
    bool            filter_enabled;                  // 20 ms filter enabled flag

    filter_enabled = !reg.hi_speed_f || (dpcls->mcu.cal.active != 0);

    if ( dpcls->mcu.adc.filter_state == FGC_FILTER_STATE_READY )
    {
        if ( adc_ready_delay_ms == 0 )                  // If ADC filter has been in READY state for at least 50 ms
        {
            adc_reset_check_f = true;                   // An ADC filter reset is permitted
        }
        else
        {
            adc_ready_delay_ms--;
        }
    }
    else
    {
        adc_ready_delay_ms = 50;
    }

    // Return immediately if I_meas blocked at zero

    if ( cal.imeas_zero_f )
    {
        return;
    }

    // Signal status

    for ( c = 0; c < 2; c++ )                                           // For each channel (A & B)
    {
        adc_chan[c].st_meas |= (adc_chan[c].st_check | adc_chan[c].last_st_check);

        if ( rtcom.start_of_ms200_f )                                   // Every 200ms check SIGNAL jump
        {
            adc_chan[c].last_st_check = adc_chan[c].st_check;

            if (   ( dcct_select != FGC_DCCT_SELECT_AB )                // If channels now agree
                 || adc.i_dcct_match_f
               )
            {
                adc_chan[c].st_check = 0;                               // Clear signal check status
            }
        }

        if (    ( adc_reset_check_f == true )                                   // If ADC filters reset permitted,
             &&  !Test(adc_chan[c].st_meas, FGC_MEAS_DEAD_FILTER)               // and not a DEAD_FILTER_channel,
             && ( (dcct_select == c) || (dcct_select == FGC_DCCT_SELECT_AB) )   // and the channel is selected,
             && ( adc.reset_req_copy == ADC_DSP_RESET_ACK )                     // and no on-going DSP reset request,
             && ( (adc_chan[c].st_meas & property.adc.filter_reset_mask & ADC_RESET_STATUS_MASK) != 0)  // and reset condition is true
           )
        {
            // Decide to reset the ADC filters or to flag the channel as DEAD_FILTER

            if (    ( adc.last_dsp_reset != 0 )                                  // If previous ADC reset is too close
                 && ((rtcom.time_now_us.unix_time - adc.last_dsp_reset) < ADC_MIN_DSP_RESET_DELAY_S)
               )
            {
                // Flag the channel as DEAD_FILTER, if PC is not in OFF, FLT_OFF, STOPPING, FLT_STOPPING state.
                // NB: We want to avoid a DEAD_FILTER when there is a technical intervention on a power converter.
                //     The DEAD_FILTER flag can be always cleared with a S MEAS.STATUS RESET, but it is still an
                //     annoyance, and it is best avoided.
                if(state_pc != FGC_PC_STOPPING     &&
                   state_pc != FGC_PC_FLT_STOPPING &&
                   state_pc != FGC_PC_FLT_OFF      &&
                   state_pc != FGC_PC_OFF)
                {
                    Set(adc_chan[c].st_meas, FGC_MEAS_DEAD_FILTER);
                }
                // Else, do nothing (in particular, do not reset the ADC)
            }
            else
            {
                // Issue a request for an ADC reset to the MCU, and keep a local copy of that request
                // TODO What to do in this case on FGC3?
                dpcls->dsp.adc.reset_req = ADC_DSP_RESET_REQ;
                adc.reset_req_copy       = ADC_DSP_RESET_REQ;

                // Fill in the reset history

                property.adc.reset_hist_chan [adc.reset_hist_idx] = c;
                property.adc.reset_hist_flags[adc.reset_hist_idx] = (adc_chan[c].st_meas & ADC_RESET_STATUS_MASK);

                adc.reset_hist_idx++;
                if ( PROP_ADC_FILTER_UNEXP_RESETS_HIST_CHAN.n_elements < adc.reset_hist_idx )
                {
                    PROP_ADC_FILTER_UNEXP_RESETS_HIST_CHAN.n_elements  = adc.reset_hist_idx; // Tamper with prop
                    PROP_ADC_FILTER_UNEXP_RESETS_HIST_FLAGS.n_elements = adc.reset_hist_idx; // structure (ugly)
                }

                if ( adc.reset_hist_idx == FGC_ADC_RESET_HIST_LEN )
                {
                    adc.reset_hist_idx = 0;
                }

                // Note: property.adc.num_unexpected_resets is incremented in AdcRead,
                //       at the time the reset is actually starting.
            }
        }

        // Check that the channel measurement is all right (flag FGC_MEAS_I_MEAS_OK)
        // In the case the ADC filters are in reset, keep that flag set.
        // The current measurement will remain constant during the reset

        if ( ( (adc_chan[c].st_meas & (   FGC_MEAS_DEAD_FILTER
                                        | FGC_MEAS_MPX_INVALID
                                        | FGC_MEAS_SIGNAL_JUMP
                                        | FGC_MEAS_V_MEAS_OK  )) != FGC_MEAS_V_MEAS_OK )
             || ( adc_chan[c].st_dcct & (FGC_DCCT_FAULT | FGC_DCCT_CAL_FLT) )
           )
        {
            // Clear Imeas OK flag

            Clr(adc_chan[c].st_meas, FGC_MEAS_I_MEAS_OK);

            // Account for the bad measurement in the filtered measurements

            adc_chan[c].ms20.err_downcounter       = 20 + adc.filter_20ms.history_offset;  // Decremented every 1ms
            adc_chan[c].i_meas_ok_200ms            = false;
            adc_chan[c].i_meas_ok_1s               = false;

            // Block use of this channel for a given number of ms

            adc_chan[c].i_meas_bad_counter = FGC_IMEAS_BAD_TIMER_MS;
        }
        else                                                    // else I_MEAS is now OK
        {
            if ( adc_chan[c].i_meas_bad_counter )               // Down count to zero the bad counter
            {
                adc_chan[c].i_meas_bad_counter--;
            }
        }

        if ( adc_chan[c].i_meas_bad_counter )
        {
            // If the present channel was marked as bad Imeas less than FGC_IMEAS_BAD_TIMER_MS ms
            // in the past, do not use it to compute Imeas.

            Clr(adc_chan[c].st_meas, FGC_MEAS_I_MEAS_OK);
        }
    }

    // Select validated current measurement

    c = dcct_select;                            // A: c = 0   B: c = 1

    switch(dcct_select)
    {
    case FGC_DCCT_SELECT_AB:                    // A and B selected

        if ( adc_chan[0].st_meas & adc_chan[1].st_meas & FGC_MEAS_I_MEAS_OK )      // If both channels are ok
        {
            Set(adc_chan[0].st_meas, FGC_MEAS_IN_USE);                          // Set IN_USE flags
            Set(adc_chan[1].st_meas, FGC_MEAS_IN_USE);

            meas.i = 0.5 * (filter_enabled ?                    // Meas is average of A & B
                           (adc_chan[0].ms20.cal.i_dcct + adc_chan[1].ms20.cal.i_dcct):
                           (adc_chan[0].meas.i_dcct     + adc_chan[1].meas.i_dcct));
            break;
        }

        c = ((adc_chan[0].st_meas & FGC_MEAS_I_MEAS_OK) ? 0 : 1);       // Use channel A if it is ok, otherwise try B

        // Fall through

    default:                                    // IA or IB selected

        meas.i = (filter_enabled ? adc_chan[c].ms20.cal.i_dcct : adc_chan[c].meas.i_dcct);

        if ( adc_chan[c].st_meas & FGC_MEAS_I_MEAS_OK )
        {
            Set(adc_chan[c].st_meas, FGC_MEAS_IN_USE);
        }
        else
        {
            Set(rtcom.faults, FGC_FLT_I_MEAS);
        }

        break;
    }

    // Check current against trip, low and zero limits

    regLimMeas(&lim_i_meas, meas.i);

    // Prevent tripping on limits when CAL.ACTIVE is enabled
    // Use case: Calibration of main converters in the LHC. Any fault
    // triggers free wheeling thyristors. They inject some current
    // into the circuit, which adds to the calibration current
    // and increases calibration error.

    if(lim_i_meas.flags.trip && dpcls->mcu.cal.active == FGC_CTRL_DISABLED)
    {
        Set(rtcom.faults, FGC_FLT_LIMITS);
        RegStateV(true, 0.0);
    }

    // Calc available voltage range from converter

    regLimVrefCalc( &lim_v_ref, meas.i );

    // Accumulate Imeas squared if cycling enabled

#if FGC_CLASS_ID == 53
    i_rms.sc_i_meas_sqr += (meas.i * meas.i);
#endif

    // Estimate field based on current

    meas.b = regLoadCurrentToField(&load.active->pars, meas.i);
}
/*---------------------------------------------------------------------------------------------------------*/
FP32 MeasSpyMpx(uint32_t idx)
/*---------------------------------------------------------------------------------------------------------*\
  This returns the relevant signal based on the choice in spy_mpx.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(idx)
    {

    // Common (class 51/53/61) SPY signals

        case FGC_SPY_REF:           return(ref.now->value);
        case FGC_SPY_REFRATE:       return(ref.now->rate);
        case FGC_SPY_ERR:           return(reg_bi_err.err);
        case FGC_SPY_BMEAS:         return(meas.b);
        case FGC_SPY_BRATE:         return(meas.b_hist.rate);
        case FGC_SPY_BSIM:          return(sim.active->b);
        case FGC_SPY_IMEAS:         return(meas.i);
        case FGC_SPY_IRATE:         return(meas.i_hist.rate);
        case FGC_SPY_IRATEREG:      return(meas.i_hist_reg.rate);
        case FGC_SPY_ISIM:          return(sim.active->i);
        case FGC_SPY_MEAS:          return(meas.hist->meas);
        case FGC_SPY_MEASRATE:      return(meas.hist->rate);
        case FGC_SPY_MEASESTIMATED: return(meas.hist->estimated);
        case FGC_SPY_VREF:          return(ref.v.value);
        case FGC_SPY_VREFRATE:      return(ref.v.rate);
        case FGC_SPY_VMEAS:         return(meas.v);
        case FGC_SPY_VSIM:          return(sim.active->v);
        case FGC_SPY_VERR:          return(ref.v_err_avg);
        case FGC_SPY_IDIFF:         return(meas.i_diff_1s);
        case FGC_SPY_TADC:          return(property.adc.temperature);
        case FGC_SPY_TDCCTA:        return(property.dcct.temperature[0]);
        case FGC_SPY_TDCCTB:        return(property.dcct.temperature[1]);
        case FGC_SPY_CYCTIME:       return((FP32)cyc.time.s);
        case FGC_SPY_REFTIME:       return((FP32)ref.time.s);
        case FGC_SPY_PLLERR:        return((FP32)(dpcom->mcu.pll.error));
        case FGC_SPY_PLLPERIOD:     return((FP32)(dpcom->mcu.pll.period     - PLL_QTICS_PER_MS));
        case FGC_SPY_PLLINT:        return((FP32)(dpcom->mcu.pll.integrator - PLL_QTICS_PER_MS));
        case FGC_SPY_DSPISR:        return((FP32)rtcom.cpu_usage);
        case FGC_SPY_FPVAL:         return(*property.fgc.debug.spy.fpval);
        case FGC_SPY_INTVAL:        return((FP32)(*property.fgc.debug.spy.intval));
        case FGC_SPY_FG:            return(ref.fg.value);
        case FGC_SPY_FGRATE:        return(ref.fg.rate);
        case FGC_SPY_FGPLUSRT:      return(ref.fg_plus_rt.value);
        case FGC_SPY_RT:            return(ref.rt);

    // Class 53 SPY signals

#if FGC_CLASS_ID == 53
        case FGC_SPY_VMEAS_POPS:    return(pal.v_pops);
        case FGC_SPY_VOUT_DXP1:     return(pal.v_pam[0]);
        case FGC_SPY_VOUT_DXP2:     return(pal.v_pam[1]);
        case FGC_SPY_UCAP_DSP1:     return(pal.v_pam[2]);
        case FGC_SPY_UCAP_DSP2:     return(pal.v_pam[3]);
        case FGC_SPY_UCAP_DSP3:     return(pal.v_pam[4]);
        case FGC_SPY_UCAP_DSP4:     return(pal.v_pam[5]);
        case FGC_SPY_UCAP_DSP5:     return(pal.v_pam[6]);
        case FGC_SPY_UCAP_DSP6:     return(pal.v_pam[7]);
        case FGC_SPY_RAW_DXP1:      return((FP32)dpcls->mcu.pal.pam_raw[0]);
        case FGC_SPY_RAW_DXP2:      return((FP32)dpcls->mcu.pal.pam_raw[1]);
        case FGC_SPY_RAW_DSP1:      return((FP32)dpcls->mcu.pal.pam_raw[2]);
        case FGC_SPY_RAW_DSP2:      return((FP32)dpcls->mcu.pal.pam_raw[3]);
        case FGC_SPY_RAW_DSP3:      return((FP32)dpcls->mcu.pal.pam_raw[4]);
        case FGC_SPY_RAW_DSP4:      return((FP32)dpcls->mcu.pal.pam_raw[5]);
        case FGC_SPY_RAW_DSP5:      return((FP32)dpcls->mcu.pal.pam_raw[6]);
        case FGC_SPY_RAW_DSP6:      return((FP32)dpcls->mcu.pal.pam_raw[7]);
        case FGC_SPY_P80_CYC_DATA:  return(FromIEEE(dpcls->dsp.pal.fgc2pbl[3]));
#endif

    // FGC2 and FGC3 specific SPY signals

        case FGC_SPY_DIRECTA:       return((FP32)adc_chan[0].adc_direct);
        case FGC_SPY_DIRECTB:       return((FP32)adc_chan[1].adc_direct);
        case FGC_SPY_RAWA:          return((FP32)adc_chan[0].adc_raw);
        case FGC_SPY_RAWB:          return((FP32)adc_chan[1].adc_raw);
        case FGC_SPY_VA:            return(adc_chan[0].meas.v_adc);
        case FGC_SPY_VB:            return(adc_chan[1].meas.v_adc);
        case FGC_SPY_IA:            return(adc_chan[0].meas.i_dcct);
        case FGC_SPY_IB:            return(adc_chan[1].meas.i_dcct);
    }

    return(-1.23456789);        // Unknown channel
}
//===========================================================================================
#if (FGC_CLASS_ID == 51) || (FGC_CLASS_ID == 59)
/*---------------------------------------------------------------------------------------------------------*/
void MeasSpySend(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends values to the SPY interface.  There are six channels, all of which can be selected
  from a range of choices by the user.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t              idx;
    DP_IEEE_FP32        v;
    uint32_t              spy_synch = SPY_SYNCH;

    // Write six words to SPY (29us)

    WAITSPY;
    SPY(spy_synch);                                     // Write synch word (0xFFFFFFFF)

    for(idx=0;idx < FGC_N_SPY_CHANS;idx++)                      // Write data words
    {
        v = ToIEEE(MeasSpyMpx(property.spy.mpx[idx]));          // Get data word for this channel
        WAITSPY;                                                // Wait for previous word to be sent
        SPY(v);                                                 // Send word to SPY
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MeasDiag(void)
/*---------------------------------------------------------------------------------------------------------*\
  This will process the special diagnostic signals on ms 19 of 20:

    - VSource max current based on number of sub converters that are active
    - I_EARTH - value used directly at 50 Hz
    - U_LEADS - values accumulated at 50 Hz for 1Hz average (in Report1s)
    - Vmeas   - this is received on two channels (x1 and x10)
    - SD-360 diagnostic ADC channel scalings
\*---------------------------------------------------------------------------------------------------------*/
{
    int32_t      ana_reg;                    // [0..3] analogue_0 to analogue_3 register
    uint32_t      amps_per_sub_conv;
    FP32        i_avl;
    FP32        vmeas1;
    FP32        vmeas10;
    FP32        weight;

    // Max converter current

    if((amps_per_sub_conv = dpcls->mcu.vs.amps_per_sub_conv))   // If converter contains subconverters
    {
        i_avl = (FP32)amps_per_sub_conv * (FP32)dpcls->mcu.vs.n_sub_convs;
        property.limits.op.i_available = (i_avl < limits.i_avl_max ? i_avl : limits.i_avl_max);
    }

    // I_EARTH

    if (sim.active->enabled != FGC_OP_SIMULATION)
    {
        ana_reg = dpcls->mcu.vs.i_earth_chan;

        if ( ana_reg >= 0 )
        {
            meas.i_earth = ((FP32*)&dim_list.analogue_in_physical)[ana_reg];
        }
        else
        {
            meas.i_earth = 0.0;
        }
    }
    else
    {
        meas.i_earth = meas.i * property.limits.i.earth / property.limits.op.i.pos;
    }

    meas.i_earth_pcnt = 100.0 * meas.i_earth / property.limits.i.earth;

    // U_LEADS

    ana_reg = dpcls->mcu.vs.u_lead_pos_chan;
    if ( ana_reg >= 0 )
    {
        meas.u_lead_pos += ((FP32*)&dim_list.analogue_in_physical)[ana_reg];
    }

    ana_reg = dpcls->mcu.vs.u_lead_neg_chan;
    if ( ana_reg >= 0 )
    {
        meas.u_lead_neg += ((FP32*)&dim_list.analogue_in_physical)[ana_reg];
    }

    // V_MEAS

    if ( (qspi_misc.flat_qspi_board_is_valid_packed & DIPS_MASK) != DIPS_MASK) // If DIPS boards not valid
    {
        return;                                                         // Skip Vmeas calculation
    }

    // Calculate Vmeas from x1 and x10 DIPS signals removing offset measured when converter is off

    if ( sim.active->enabled )
    {
        meas.v = reg_vars.act[1];                               // Simulate vmeas
    }
    else
    {
        vmeas1  = (FP32)(0x800 - (int32_t)(qspi_misc.analog_regs_from_all_dim_boards[0][0x10] & 0xFFF))  * VMEAS1_CAL;  // register analog_0 of DIM board, flat[16] = B[0]
        vmeas10 = (FP32)(0x800 - (int32_t)(qspi_misc.analog_regs_from_all_dim_boards[1][0x00] & 0xFFF)) * VMEAS10_CAL;  // register analog_1 of DIM board, flat[ 0] = A[0]
        weight  = 10.0 * fabs(vmeas1) - 9.0;
        weight = (weight < 0.0 ? 0.0 : (weight > 1.0) ? 1.0 : weight);
        meas.v = property.vs.gain * (vmeas1 * weight + vmeas10 * (1.0 - weight));
    }
}
#endif
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rt.c
\*---------------------------------------------------------------------------------------------------------*/
