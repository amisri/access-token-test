/*---------------------------------------------------------------------------------------------------------*\
  File:     cyc.c

  Purpose:  FGC DSP Software - contains cycling related functions
\*---------------------------------------------------------------------------------------------------------*/

#define CYC_GLOBALS             // define cyc global variable

#include <cyc.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <dsp_dependent.h>      // for ToIEEE(), DISABLE_INTS/ENABLE_INTS
#include <string.h>             // for memset()
#include <rtcom.h>              // for rtcom global variable
#include <main.h>               // for state_pc global variable
#include <meas.h>               // for meas global variable
#include <macros.h>             // for Set(), Clr(), Test()
#include <debug.h>              // for db global variable
#include <ref.h>                // for PPM_ALL_USERS
#include <reg.h>                // for reg global variable
#include <refto.h>              // for RefToEnd()
#include <ppm.h>                // for ppm[]
#include <isr.h>
#include <mcu_dsp_common.h>
#include <bgp_plat.h>
#include <defconst.h>

/*---------------------------------------------------------------------------------------------------------*/

static void CycSendMCUTimingLog(uint32_t empty_cycle_f);

/*---------------------------------------------------------------------------------------------------------*/
void CycEnter(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when entering TO_CYCLING state
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t    user;

    // Reset property REF.CYC.WARNING

    for (user = 0 ; user <= FGC_MAX_USER ; user++)
    {
        CycResetCheckWarning(user);
    }

    // Reset and notification of property REF.CYC.FAULT

    CycResetCheckFault();

    // Reset LOG.CYC related properties

    memset(&(property.log.cyc.status), 0, sizeof(property.log.cyc.status));
    memset(&(property.log.cyc.max_abs_err), FP32ZERO, sizeof(property.log.cyc.max_abs_err));

    // clear on STATUS.WARNING the bit FGC_WRN_CYCLE_START, FGC_WRN_TIMING_EVT, ...
    rtcom.warnings &= ~STRETCHED_CYC_WARNINGS;

    cyc.dysfunction_code       = 0;
    cyc.dysfunction_is_a_fault = false;

    cyc.consecutive_wrn_cnt = 0;

    // Switch to END reference with initial openloop voltage equal to 0 Volts

    DISABLE_INTS;
    RefResetValue(&ref.v, 0.0);
    refto.flags.end = true;             // Will call RefToEnd() on the next regulation ISR.
    ENABLE_INTS;

    // Re-arm references for all cycling users, copy the armed reference to the cycling one, if necessary

    for (user = 1; user <= FGC_MAX_USER; user++)
    {
        ppm[user].cycling.func_type = dpcls->dsp.ref.func.type[user];

        if (dpcls->dsp.ref.func.type[user] != FGC_REF_NONE)
        {
            cyc.ref_copy_state[cyc.ref_user] = REF_CYCLING_COPY_FROM_ARMED;
        }
    }

    // Reset cycle preparation state and wait for the next user event

    CycCancelNextUser();
}
/*---------------------------------------------------------------------------------------------------------*/
void CycExit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when exiting CYCLING state
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t    user;

    ref.cycling_f                   = false;                // Prevent the ISR from starting another cycle

    CycCancelNextUser();                                    // Cancel next cycle user

    cyc.ref_user                    = 0;
    cyc.ref_func_type               = FGC_REF_NONE;
    reg.max_abs_err_enable_time     = 0.0;

    // Transfer stretched warnings to warnings and then clear stretched warning

    rtcom.warnings |= cyc.stretched_warnings;
    cyc.stretched_warnings = 0;

    // Disarm cycling reference functions

    for (user = 0; user <= FGC_MAX_USER; user++)
    {
        ppm[user].cycling.func_type = FGC_REF_NONE;

        cyc.ref_copy_state[cyc.ref_user] = REF_CYCLING_OK;
    }

    dpcls->dsp.ref.stc_func_type = 0;
    dpcls->dsp.ref.max           = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void CycCancelNextUser(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will cancel the next programmed user and restore the DSP in a state where it is waiting
  for another user event.

  This function can be called from ISR or BGP context.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Clear cycle start event

    dpcom->mcu.evt.next_cycle_start_delay_ms = 0;
    cyc.start_cycle_evt_f                    = false;

    // Reset next cycle preparation state machine

    cyc.next_cycle_state = NEXT_CYCLE_WAIT_USER_EVENT;
}
/*---------------------------------------------------------------------------------------------------------*/
void CycStartSuperCycle(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a start super-cycle event is received -- this is normally 20ms after the
  start of the super-cycle.
\*---------------------------------------------------------------------------------------------------------*/
{
    // I_RMS calculation for previous super-cycle - set or clear warning based on LIMIT.I.RMS

    property.meas.i_rms = sqrt(i_rms.sc_i_meas_sqr / (i_rms.sc_length_s / property.reg.v.rst_pars.period));

    i_rms.sc_i_meas_sqr = 0.0;
    i_rms.sc_length_s   = 0.0;

#if (FGC_CLASS_ID != 51)

    if ((property.limits.i.rms != 0.0)
        && (property.meas.i_rms > property.limits.i.rms)
       )
    {
        Set(rtcom.warnings, FGC_WRN_I_RMS_LIM);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_I_RMS_LIM);
    }

    // Stretch cycle related warnings to be at least one super-cycle to avoid toggling when CYCLING

    if (ref.cycling_f)
    {
        cyc.stretched_warnings = rtcom.warnings & STRETCHED_CYC_WARNINGS;
        Clr(rtcom.warnings, STRETCHED_CYC_WARNINGS);
    }
    else
    {
        // even when not cycling
        // clear general warnings (like FGC_WRN_CYCLE_START) that are not
        // that are not clear individually (like FGC_WRN_I_RMS_LIM)
        Clr(rtcom.warnings, STRETCHED_CYC_WARNINGS);
    }

    // Assert TIMING_EVT warning if supercycle is simulated

    if (dpcom->mcu.evt.sim_supercycle_f)
    {
        Set(rtcom.warnings, FGC_WRN_TIMING_EVT);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_TIMING_EVT);
    }

#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void CycStartCycle(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to start a new cycle on C0 (millisecond 0 of the cycle).
\*---------------------------------------------------------------------------------------------------------*/
{
    // Start of cycle time in secs and usecs

    dpcom->dsp.cyc.prev_start_time = cyc.start_time;

    cyc.start_time = rtcom.time_now_us;

    // Get timing_user

    cyc.timing_user = dpcom->mcu.evt.next_cycle_user;

    // Get cyc.user based on DEVICE.PPM, REG.TEST.USER

    reg.test_user_f = (cyc.timing_user && cyc.timing_user == property.reg.test.user);

    cyc.user        = (dpcom->mcu.device_ppm ? cyc.timing_user : 0);

    // Once in CYCLING state set the cycling flag to start first cycle

    if(state_pc == FGC_PC_CYCLING)
    {
        ref.cycling_f = true;
    }

    // If cycling is active

    if(ref.cycling_f)
    {
        // Enable function for cyc.ref_user and set regulation state

        const uint32_t reg_state = CycEnableRefFunction();

        // Synchronous DSP (i.e. FGC2). Change the regulation state

        RegState(true, reg_state, ref.v.value);

        dpcls->dsp.ref.stc_func_type = (cyc.ref_func_type ?        ppm[cyc.ref_user].cycling.stc_func_type   : 0);
        dpcls->dsp.ref.max           = (cyc.ref_func_type ? ToIEEE(ppm[cyc.ref_user].cycling.meta.range.max) : 0);

        // Arm BIV checks to happen at time REF.START.TIME + REF.START.DURATION

        cyc.biv_check_f     = true;
        cyc.biv_check_time  = ppm[cyc.ref_user].cycling.ref_start.time + ppm[cyc.ref_user].cycling.ref_start.duration;

        ref.reg_bypass_mask = DEFAULT_REF_REG_BYPASS;

        // Enable regulation max_abs_err logging from start of first plateau

        reg.max_abs_err_enable_time = ppm[cyc.ref_user].cycling.first_plateau.time;
    }

    // LOG.TIMING : starting a new cycle

    CycSendMCUTimingLog(false);

    // Reset cycling events from MCU

    cyc.ssc_evt_f         = false;
    cyc.start_cycle_evt_f = false;

    dpcom->mcu.evt.ssc_f  = false;

    dpcom->mcu.evt.next_cycle_user     = 0;
    dpcom->mcu.evt.next_cycle_start_delay_ms = 0;

    // Start capture log

    CycCaptureLogStart();
}
/*---------------------------------------------------------------------------------------------------------*/
void CycSendMCUCheckResults(void)
/*---------------------------------------------------------------------------------------------------------*\
  Send data to MCU for the timing log.
  This function is supposed to be called at the end of every cycle.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Update warning and fault flag for the past cycle

    dpcom->dsp.cyc.dysfunction_code       = cyc.dysfunction_code;
    dpcom->dsp.cyc.dysfunction_is_a_fault = cyc.dysfunction_is_a_fault;

    // Reset warning/fault for last cycle. If clean run, also reset REF.CYC.WARNING, and cyc.consecutive_wrn_cnt

    if (cyc.dysfunction_code == 0)
    {
        CycResetCheckWarning(cyc.user);
        cyc.consecutive_wrn_cnt = 0;
    }

    cyc.dysfunction_code = 0;
    cyc.dysfunction_is_a_fault = false;
}
/*---------------------------------------------------------------------------------------------------------*/
static void CycSendMCUTimingLog(uint32_t empty_cycle_f)
/*---------------------------------------------------------------------------------------------------------*\
  Send data to MCU for the timing log
\*---------------------------------------------------------------------------------------------------------*/
{
    // Timing data for the new cycle

    dpcom->dsp.cyc.start_time = cyc.start_time;

    if (!empty_cycle_f)
    {
        dpcom->dsp.cyc.user_timing = cyc.timing_user;
        dpcom->dsp.cyc.prev_user   = dpcom->dsp.cyc.user;
        dpcom->dsp.cyc.user        = cyc.user;
    }
    else
    {
        // Empty cycle (useful when reporting a FAULT)

        dpcom->dsp.cyc.user_timing = 0;
    }

    // Trigger MCU

    dpcom->dsp.cyc.start_cycle_f = true;
}
/*---------------------------------------------------------------------------------------------------------*/
void CycCopyRefFunction(void)
/*---------------------------------------------------------------------------------------------------------*\
  [FGC2]
  This function is called on the second iteration of a new cycle to take a copy of the reference data if the
  reference function is still armed.  Note that the main details about the reference function are copied
  in the first iteration (on millisecond 0) by CycEnableRefFunction() but the copy of the function data
  can take some time so it is spread between two iterations to reduce the risk of an overrun.

  This function is called in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t          ref_func_type;
    struct ppm   *  ppm_user = ppm + cyc.ref_user;

    // If reference function must be copied from armed to cycling

    if (cyc.ref_copy_state[cyc.ref_user] == REF_CYCLING_COPY_ONGOING)
    {
        // Copy function parameters and data from armed to cycling

        switch (cyc.ref_func_type)
        {
            case FGC_REF_ZERO:

                ppm_user->cycling.pars.zero = ppm_user->armed.pars.zero;
                break;

            case FGC_REF_PPPL:

                ppm_user->cycling.pars.pppl = ppm_user->armed.pars.pppl;
                break;

            case FGC_REF_TABLE:

                ppm_user->cycling.pars.table = ppm_user->armed.pars.table;
                ppm_user->cycling.table_data = ppm_user->armed.table_data;

                // Need to adjust the pointers in ppm_user->cycling.pars structure

                ppm_user->cycling.pars.table.function = &ppm_user->cycling.table_data.function[0];
                break;

            case FGC_REF_NONE:
            default:
                // Nothing to copy
                break;
        }
    }

    // Finish copying the reference

    cyc.ref_copy_state[cyc.ref_user] = REF_CYCLING_OK;

    // Enabled generation of the reference, and ref.active now points to ppm_user->cycling

    ref.func_type = cyc.ref_func_type;
    ref.active = ref.next = &ppm_user->cycling;
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t CycEnableRefFunction(void)
/*---------------------------------------------------------------------------------------------------------*\
  [FGC2, FGC3]
  This function is called on the first iteration of every cycle to enable a reference function based on the
  reference user for the new cycle.  It returns the regulation state for the cycle.

  This function has a very different behaviour on a synchronous DSP (the case of FGC2) and on an
  asynchronous one (the case of FGC3). On FGC3, the function reference is already copied and ready to use
  in ref.active structure. The ISR simply needs to switch to it.

  This function is called in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t       reg_state;
    struct ppm * ppm_user;

    // Set user for ref function according to reg test flag and REF.TEST.REF_USER property

    cyc.ref_user = (reg.test_user_f && property.reg.test.ref_user ? property.reg.test.ref_user : cyc.user);

    ppm_user = ppm + cyc.ref_user;

    // If start-cycle event received from timing

    if (cyc.timing_user)
    {
        if (cyc.ref_copy_state[cyc.ref_user] == REF_CYCLING_COPY_FROM_ARMED)
        {
            // Copy function details from armed to cycling
            // Note the function parameters are copied by CycCopyRefFunction() on the second iteration of the
            // cycle to avoid overrunning the first iteration.

            ppm_user->cycling.func_type         = cyc.ref_func_type = dpcls->dsp.ref.func.type[cyc.ref_user];
            ppm_user->cycling.reg_mode          = reg_state         = ppm_user->armed.reg_mode;
            ppm_user->cycling.stc_func_type                         = ppm_user->armed.stc_func_type;
            ppm_user->cycling.meta                                  = ppm_user->armed.meta;
            ppm_user->cycling.min_basic_periods                     = ppm_user->armed.min_basic_periods;
            ppm_user->cycling.first_plateau                         = ppm_user->armed.first_plateau;

            // This is only a partial copy. The rest is done on the next iteration in function CycCopyRefFunction.

            cyc.ref_copy_state[cyc.ref_user] = REF_CYCLING_COPY_ONGOING;
        }
        else
        {
            cyc.ref_func_type = ppm_user->cycling.func_type;
            reg_state         = ppm_user->cycling.reg_mode;
        }

        // Cycle Check - REF_NOT_ARMED : Reference is not armed for this user

        if (cyc.ref_func_type == FGC_REF_NONE)
        {
#if (FGC_CLASS_ID != 51)
            CycSaveCheckWarning(DONT_TRIGG_LOG, FGC_CYC_WRN_REF_NOT_ARMED, FGC_WRN_CYCLE_START,
                                (float)cyc.timing_user,
                                (float)cyc.user,
                                (float)cyc.ref_user,
                                0.0);
#endif

            reg_state = FGC_REG_V;
            RefResetValue(&ref.fg, 0.0);
            RefResetValue(&ref.v,  0.0);

            ppm_user->cycling.min_basic_periods = 0;
        }
    }
    else        // cyc.timing_cyc == 0 - no timing event received
    {
        cyc.ref_func_type = FGC_REF_NONE;
        reg_state         = FGC_REG_V;
        RefResetValue(&ref.fg, 0.0);
        RefResetValue(&ref.v,  0.0);
    }

    // Ensure that no reference is generated on ms 0 because parameters may not be ready

    ref.func_type = FGC_REF_NONE;

    return (reg_state);
}
/*---------------------------------------------------------------------------------------------------------*/
void CycResetCheckWarning(uint32_t user)
/*---------------------------------------------------------------------------------------------------------*\
  This function will reset the cycling check results for the supplied user, and notify the property.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (ppm[user].cycling.warning.chk != FGC_CYC_WRN_NONE)
    {
        ppm[user].cycling.warning.chk = FGC_CYC_WRN_NONE;

        // Notification of REF.CYC.WARNING

        dpcls->dsp.notify.ref_cyc_warning = user;
    }

    memset(&(ppm[user].cycling.warning.data), FP32ZERO, sizeof(ppm[user].cycling.warning.data));
}
/*---------------------------------------------------------------------------------------------------------*/
void CycResetCheckFault(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will reset the cycling fault, and notify the property.
\*---------------------------------------------------------------------------------------------------------*/
{
    dpcls->dsp.cyc.fault.user = 0;
    dpcls->dsp.cyc.fault.chk  = FGC_CYC_FLT_NONE;

    memset(&(property.ref.cyc.fault.data), FP32ZERO, sizeof(property.ref.cyc.fault.data));
    // Notification of REF.CYC.FAULT

    dpcls->dsp.notify.ref_cyc_fault = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void CycSaveCheckWarning(uint32_t trig_log_capture_f,
                         uint32_t check_wrn,
                         uint32_t warning,
                         FP32 data0,
                         FP32 data1,
                         FP32 data2,
                         FP32 data3)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called if a cycling check fails.  It records the check and the check data and possibly
  capture a log.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct ppm * ppm_user = ppm + cyc.user;

    rtcom.warnings |= warning;

    dpcom->dsp.warnings = rtcom.warnings | cyc.stretched_warnings;

    // ToDo I guess is worth the following info even when not cycling
    // as a warning is already reported
    if (ref.cycling_f)
    {
        ppm_user->cycling.warning.chk     = check_wrn;
        ppm_user->cycling.warning.data[0] = data0;
        ppm_user->cycling.warning.data[1] = data1;
        ppm_user->cycling.warning.data[2] = data2;
        ppm_user->cycling.warning.data[3] = data3;

        // Notification of property REF.CYC.WARNING(cyc.user)

        dpcls->dsp.notify.ref_cyc_warning = cyc.user;

        if (trig_log_capture_f
            && (dpcls->dsp.log.capture.state == FGC_LOG_RUNNING)
            && !dpcls->dsp.log.capture.cyc_chk_time.unix_time
           )
        {
            capture.freeze_f                    = true;              // will freeze the log at the end of cycle
            dpcls->dsp.log.capture.cyc_chk_time = cyc.start_time;
            dpcls->dsp.log.capture.user         = cyc.user;
        }

        // For LOG.TIMING

        cyc.dysfunction_code       = check_wrn;
        cyc.dysfunction_is_a_fault = false;            // is a Warning

        // Count consecutive warnings. A clean cycle (without any warning) shall reset that counter.
        if (( check_wrn != FGC_CYC_WRN_REF_NOT_ARMED ) &&
            ( check_wrn != FGC_CYC_WRN_REF_DISABLED  ))
        {
            cyc.consecutive_wrn_cnt++;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CycSaveCheckFault(uint32_t check_flt, uint32_t warning, FP32 data0, FP32 data1, FP32 data2, FP32 data3)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called if cycling fails and we return to ON_STANDBY state.
  It records the check and the check data of the fault.
\*---------------------------------------------------------------------------------------------------------*/
{
    rtcom.warnings |= warning;
    dpcom->dsp.warnings = rtcom.warnings | cyc.stretched_warnings;

    // ToDo I guess is worth the following info even when not cycling
    // as a warning is already reported
    if (ref.cycling_f
        && !dpcls->dsp.cyc.to_standby_f
       )
    {
        dpcls->dsp.cyc.fault.user       = cyc.user;
        dpcls->dsp.cyc.fault.chk        = check_flt;
        property.ref.cyc.fault.data[0]  = data0;
        property.ref.cyc.fault.data[1]  = data1;
        property.ref.cyc.fault.data[2]  = data2;
        property.ref.cyc.fault.data[3]  = data3;

        // Notification of property REF.CYC.FAULT

        dpcls->dsp.notify.ref_cyc_fault = 0;

        // Trigger the STANDBY flag

        dpcls->dsp.cyc.to_standby_f     = true;

        RegStateV(true, 0.0);

        // For LOG.TIMING

        cyc.dysfunction_code       = check_flt;
        cyc.dysfunction_is_a_fault = true;        // is a Fault

        // Report the fault to the MCU

        CycSendMCUCheckResults();
        CycSendMCUTimingLog(true);   // dummy log entry
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CycCaptureLogStart(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at the start of every cycle to manage the capture log.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Freeze log if triggered

    if ((dpcls->dsp.log.capture.state == FGC_LOG_RUNNING)
        && capture.freeze_f
       )
    {
        dpcls->dsp.log.capture.state     = FGC_LOG_FROZEN;
        dpcls->dsp.log.capture.n_samples = capture.n_samples_recorded;

        // Notify properties LOG.CAPTURE.SIGNALS.*

        dpcls->dsp.notify.log_capture_user = dpcls->dsp.log.capture.user;
    }

    // If ARMED or RUNNING...

    if (dpcls->dsp.log.capture.state != FGC_LOG_FROZEN)
    {
        // Prepare to start capturing spy data

        capture.freeze_f                 = false;
        capture.n_samples_recorded       = 0;
        dpcls->dsp.log.capture.n_samples = 0;
        dpcls->dsp.log.capture.state     = FGC_LOG_RUNNING;
        dpcls->dsp.log.capture.reg_state = reg.active->state;

        // Inform MCU of time of first sample

        dpcls->dsp.log.capture.first_sample_time = cyc.start_time;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CycCaptureLog(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called every ms to manage the capture log.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct capture_buf * buf;

    switch (dpcls->dsp.log.capture.state)
    {

            // --------------------------------------------------------------------------------------------
        case FGC_LOG_ARMED:
            // --------------------------------------------------------------------------------------------

            // If STATE is ARMED, set user to -1.

            dpcls->dsp.log.capture.user = -1;

            // NOTE. State switch from ARMED to RUNNING is done at the beginning of a cycle in CycCaptureLogStart

            break;

            // --------------------------------------------------------------------------------------------
        case FGC_LOG_RUNNING:

            // --------------------------------------------------------------------------------------------

            // Check for request to capture log for a specified USER (or next cycle if capture.user == 0)

            if (dpcls->dsp.log.capture.user != -1)
            {
                if (!capture.freeze_f &&
                    (dpcls->dsp.log.capture.user == PPM_ALL_USERS || dpcls->dsp.log.capture.user == cyc.user))
                {
                    capture.freeze_f = true;
                }
            }
            else
            {
                // In any case, if the capture.user is set to -1, reset capture.freeze_f

                capture.freeze_f = false;
            }

            if (capture.n_samples_recorded < FGC_LOG_CAPTURE_LEN)   // Maximum log size is around 6 basic periods
            {
#if (FGC_CLASS_ID == 53)
                buf = &((struct capture_buf *)LOG_CAPTURE_BASEADDR)[capture.n_samples_recorded];
#else
                buf = &log_capture_buffer[capture.n_samples_recorded];
#endif
                capture.n_samples_recorded++;
                // Record sample

                buf->ref    = ref.now->value;
                buf->v_ref  = ref.v.value;
                buf->b_meas = meas.b;
                buf->i_meas = meas.i;
                buf->v_meas = meas.v;
                buf->err    = reg_bi_err.err;
                buf->mpx0   = MeasSpyMpx(dpcls->mcu.log_capture_mpx[0]);
                buf->mpx1   = MeasSpyMpx(dpcls->mcu.log_capture_mpx[1]);
                buf->mpx2   = MeasSpyMpx(dpcls->mcu.log_capture_mpx[2]);

                // Time of last sample

                dpcls->dsp.log.capture.last_sample_time = rtcom.time_now_us;
            }

            break;

            // --------------------------------------------------------------------------------------------
        case FGC_LOG_FROZEN:

            // --------------------------------------------------------------------------------------------

            // If capture.user is set to -1, switch STATE from FROZEN to ARMED

            if (dpcls->dsp.log.capture.user == -1)
            {
                dpcls->dsp.log.capture.state = FGC_LOG_ARMED;
            }

            // TIMEOUT. If frozen, we can still initiate a new log if the two following conditions are met:
            //   (1) The reason for the freeze is not a cycle check warning (and so, cyc_chk_time == 0)
            //   (2) The capture log timeout has expired.

            if (!dpcls->dsp.log.capture.cyc_chk_time.unix_time &&
                (rtcom.time_now_us.unix_time - dpcls->dsp.log.capture.first_sample_time.unix_time) > LOG_CAPTURE_TIMEOUT)
            {
                dpcls->dsp.log.capture.state = FGC_LOG_ARMED;
                dpcls->dsp.log.capture.user  = -1;
            }

            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CycLogAbsMaxErr(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a cycling function finishes to record and check the max absolute error
  recorded during the cycle.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (reg.active->state != FGC_REG_V && TestRefCheckLimits())
    {
        property.log.cyc.max_abs_err[cyc.user] = reg_bi_err.max_abs_err;

        if (reg_bi_err.fault_limit > 0.0 &&
            reg_bi_err.fault_limit < reg_bi_err.max_abs_err)
        {
            property.log.cyc.status[cyc.user] = FGC_CYC_STATUS_FAULT;
        }
        else if (reg_bi_err.warning_limit > 0.0 &&
                 reg_bi_err.warning_limit < reg_bi_err.max_abs_err)
        {
            property.log.cyc.status[cyc.user] = FGC_CYC_STATUS_WARNING;
        }
        else
        {
            property.log.cyc.status[cyc.user] = FGC_CYC_STATUS_OK;
        }
    }

    regErrInitVars(&reg_bi_err);
}
/*---------------------------------------------------------------------------------------------------------*/
void CycLogSignals(struct cyc_log_ctrl * cyc_log_ctrl)
/*---------------------------------------------------------------------------------------------------------*\
  This function is a generic way of logging signals. It is used for the LOG.OASIS buffers but could as well
  be used for other logs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t sig_idx;

    // Copy each signal in the corresponding log buffer

    for (sig_idx = 0; sig_idx < cyc_log_ctrl->nb_signals; sig_idx++)
    {
        cyc_log_ctrl->log_buf_ptr[sig_idx][cyc_log_ctrl->write_idx.index] = *cyc_log_ctrl->signal_src_ptr[sig_idx];
    }

    // Increment the write index of a circular buffer

    (cyc_log_ctrl->write_idx.index)++;

    if (cyc_log_ctrl->write_idx.index >= cyc_log_ctrl->max_index)
    {
        // Rollover of the index, change the index color

        cyc_log_ctrl->write_idx.index = 0;

        // Swap between LOG_ODD and LOG_EVEN
        cyc_log_ctrl->write_idx.color = (cyc_log_ctrl->write_idx.color == LOG_ODD ? LOG_EVEN : LOG_ODD);
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cyc.c
\*---------------------------------------------------------------------------------------------------------*/
