*---------------------------------------------------------------------------------------------------------
* File:         vecs.asm
*
* Purpose:      FGC2 DSP interrupt vectors
*---------------------------------------------------------------------------------------------------------

        .sect   "vecs"                  ; reset and interrupt vectors

        .ref    _c_int00                ; compiler defined C initialization routine
        .ref    _IsrMst                 ; external interrupt 2 (ms tick) service routine
        .ref    _IsrTrap                ; Unexpected interrupt service routine

reset:  .word   _c_int00                ; Reset
int0:   .word   _IsrTrap                ; Unexpected IRQ 0
int1:   .word   _IsrTrap                ; Unexpected IRQ 1
int2:   .word   _IsrMst                 ; IRQ 2: _IsrMst millisecond service routine
int3:   .word   _IsrTrap                ; Unexpected IRQ 3
xint0:  .word   _IsrTrap        ; Serial port 0 tx buffer empty
rint0:  .word   _IsrTrap        ; Serial port 0 rx buffer empty
xint1:  .word   _IsrTrap        ; Serial port 1 tx buffer empty
rint1:  .word   _IsrTrap        ; Serial port 1 rx buffer empty
tint0:  .word   _IsrTrap        ; Timer 0 interrupt
tint1:  .word   _IsrTrap        ; Timer 1 interrupt
dint:   .word   _IsrTrap        ; DMA interrupt

*---------------------------------------------------------------------------------------------------------
*       End of file: vecs.asm
*---------------------------------------------------------------------------------------------------------

