/*---------------------------------------------------------------------------------------------------------*\
  File:         ref.c

  Purpose:      FGC DSP Software - contains Ref functions

  Notes:        Ref functions return 0 if the reference function has not finished and 1 when time exceeds
                the end of the function.
\*---------------------------------------------------------------------------------------------------------*/

#include <ref.h>
#include <mcu_dsp_common.h>
#include <macros.h>             // for Test()
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <rtcom.h>              // for rtcom global variable
#include <reg.h>                // for reg, load global variables
#include <main.h>               // for state_pc global variable
#include <meas.h>               // for meas global variable
#include <refto.h>              // for RefToEnd()
#include <cyc.h>                // for cyc global variable, CycLogAbsMaxErr()
#include <bgp.h>                // for BgpInitNone()
#include <dsp_dependent.h>      // for FromIEEE()
#include <deftypes.h>           // for MIN/MAX
#include <math.h>               // for fabs()
#include <ppm.h>                // for ppm[]
#include <dpcls.h>
#include <definfo.h>

// Reference function pointer array

uint32_t(*ref_func[])(void) =     // Ref gen functions
{
    RefNone,                    // 0  FGC_REF_NONE
    RefStarting,                // 1  FGC_REF_STARTING
    RefStopping,                // 2  FGC_REF_STOPPING
    RefPlep,                    // 3  FGC_REF_TO_STANDBY
    RefNone,                    // 4  FGC_REF_ARMED
    RefPlep,                    // 5  FGC_REF_ABORTING
    RefNone,                    // 6  FGC_REF_NOW
    RefTest,                    // 7  FGC_REF_STEPS
    RefTest,                    // 8  FGC_REF_SQUARE
    RefTest,                    // 9  FGC_REF_SINE
    RefTest,                    // 10 FGC_REF_COSINE
    RefNone,                    // 11 FGC_REF_PLP    (Used in Class 59 and not in Class 51/53)
    RefPlep,                    // 12 FGC_REF_PLEP
    RefPppl,                    // 13 FGC_REF_PPPL   (Used in class 53 and not in class 51)
    RefTrim,                    // 14 FGC_REF_LTRIM
    RefTrim,                    // 15 FGC_REF_CTRIM
    RefTable,                   // 16 FGC_REF_TABLE
    RefOpenloop,                // 17 FGC_REF_RAMP
    RefZero,                    // 18 FGC_REF_ZERO
    RefEnd,                     // 19 FGC_REF_END
    RefNone,                    // 20 FGC_REF_IDLE
    RefPlep,                    // 21 FGC_REF_PLEP_NO_CLIP
};
/*---------------------------------------------------------------------------------------------------------*/
void Ref(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function calculates the reference (current or voltage depending upon the mode).
  This function is called on each regulation iteration whether it is I, B or V reg mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Copy the reference values for previous regulation iteration in the corresponding ref_container->prev_value
    // Do it for ALL instances of struct ref_container.

    ref.i.prev_value          = ref.i.value;
    ref.i_clipped.prev_value  = ref.i_clipped.value;
    ref.b.prev_value          = ref.b.value;
    ref.v.prev_value          = ref.v.value;
    ref.fg.prev_value         = ref.fg.value;
    ref.fg_plus_rt.prev_value = ref.fg_plus_rt.value;

    // Get the real-time value from the gateway (only important if MODE.RT is ENABLED)

    RefRtCtrl();

    // If to_standby flag set due to state change then switch reference go to standby

    if (refto.flags.standby)
    {
        RefToStandby();
    }

    // Else if to_aborting flag is set

    else if (refto.flags.aborting)
    {
        RefToAborting();
    }

    else if (refto.flags.end)
    {
        RefToEnd();
    }

    // Calculate predefined reference according to reference type

    // TODO Add proper function status

    if (ref_func[ref.func_type]())
    {
        // Function has finished

        if (ref.cycling_f)
        {
            CycLogAbsMaxErr();
            RefToEnd();                // Directly call RefToEnd function not to wait the next regulation period
        }
        else
        {
            BgpInitNone(FGC_REG_V, 0);          // First argument is ignored
            ref.func_type = FGC_REF_NONE;
        }
    }

    // If to_stopping flag is set and no reference is playing (this must always follow a TO_STANDBY reference)

    if (refto.flags.stopping &&
        ref.func_type == FGC_REF_NONE)
    {
        RefToStopping();
    }

    // Compute ref.fg + ref.rt

    ref.fg_plus_rt.value = ref.fg.value;

    if (dpcls->mcu.mode_rt && state_pc > FGC_PC_ON_STANDBY)
    {
        ref.fg_plus_rt.value += ref.rt;
    }

    // Calculate rates of change

    RefRefreshValue(&ref.fg);
    RefRefreshValue(&ref.fg_plus_rt);
}
/*---------------------------------------------------------------------------------------------------------*/
void RefRtCtrl(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function processes the real-time control data from the gateway according to the MODE.RT property.
  For periods < 20ms then the RT data (received every 20ms) is interpolated.
  In states ON_STANDBY and below, the RT value is ignored and REF.RT is set to zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    DP_IEEE_FP32        rtdata;                                 // Real-time control value from gateway (IEEE FP)
    FP32                rtvalue;                                // Real-time control value from gateway (TI FP)

    // If real-time data not being used then set it to zero.

    if (!dpcls->mcu.mode_rt || state_pc <= FGC_PC_ON_STANDBY)
    {
        rt_ref_data.value_gw = 0xFFFFFFFF;
        rt_ref_data.value    = 0.0;
        rt_ref_data.final    = 0.0;
        ref.rt               = 0.0;
        return;
    }

    // If new real-time control data received then check it

    if (dpcls->mcu.ref.rt_data_f)
    {
        dpcls->mcu.ref.rt_data_f = false;
        rtdata = dpcls->mcu.ref.rt_data;

        if((rtdata & NAN_MASK) != NAN_MASK &&          // If valid value received (!NaN)
           (rtdata != rt_ref_data.value_gw))            // and it has changed
        {
            rt_ref_data.value_gw = rtdata;              // Remember the new value
            rtvalue = FromIEEE(rtdata);                 // Convert RT data to TI FP format

            if (rtvalue < -property.limits.op.i.pos ||  // If RT value is crazy
                rtvalue >  property.limits.op.i.pos)
            {
                property.fieldbus.rt_bad_values++;      // Increment bad val counter
            }
            else                                        // else value is good - prepare interpolation
            {
                rt_ref_data.time_s  = 0.0;
                rt_ref_data.value = rt_ref_data.final;
                rt_ref_data.final = rtvalue;
                rt_ref_data.step  = rt_ref_data.final - rt_ref_data.value;
            }
        }
    }

    // Interpolate if iteration period < 20ms

    if (reg.active->rst_pars.period < INTERPOLATION_TIME)
    {
        rt_ref_data.time_s += reg.active->rst_pars.period;

        if (rt_ref_data.time_s < INTERPOLATION_TIME)
        {
            ref.rt = rt_ref_data.value + rt_ref_data.step * rt_ref_data.time_s * (1.0 / INTERPOLATION_TIME);
        }
        else
        {
            ref.rt = rt_ref_data.final;

            rt_ref_data.time_s = INTERPOLATION_TIME;
        }
    }
    else
    {
        ref.rt = rt_ref_data.final;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RefFirstPlateau(void)
/*---------------------------------------------------------------------------------------------------------*\
  First plateau is used for PPPL for B and I regulation modes, and TABLE in B regulation mode.
  It starts the cycle in openloop, applying a voltage according to the REF.START properties.  It then
  starts regulating field or current and ramps with a Linear-Parabola function (pLeP) to the first
  plateau level.
  To transition between openloop to closeloop at time REF.START.TIME + REF.START.DURATION, this function
  uses cyc.time timing reference, not ref.time, so that the BIV checks (function cycBIVChecks) are done
  precisely at the time of the transition (cycBIVChecks also relies on cyc.time).
\*---------------------------------------------------------------------------------------------------------*/
{
    // Before REF.START.TIME the open loop voltage reference is zero

    FP32 ref_start_time = ppm[cyc.ref_user].cycling.ref_start.time;

    if (cyc.time.s < ref_start_time)
    {
        EnableRegBypass();
        ref.fg.value = ref.v_openloop = 0.0;
    }

    // Until REF.START.TIME + REF.START.DURATION apply voltage

    else if (cyc.time.s < (ref_start_time + ppm[cyc.ref_user].cycling.ref_start.duration))
    {
        ref.v_openloop = ppm[cyc.ref_user].cycling.ref_start.vref;
    }
    else // Then after REF.START.TIME + REF.START.DURATION close loop and regulate to first plateau
    {
        // If regulation bypass is still enabled then close loop (which disables bypass)

        if (TestRegBypass())
        {

            // TODO This is possibly a worst case scenario for the ISR, due to the call to FgPlepCalc
            //      Need to investigate the performance cost.

            struct fg_plep_config     config;
            struct fg_first_plateau * first_plateau = &ppm[cyc.ref_user].cycling.first_plateau;

            // Prepare RST history to have bumpless loop closing

            RegVarsInit(ref.v.value, reg.active->rst_pars.period, reg.active->rst_pars.rst.track_delay);

            // Set the reference to the last value in the RST rebuilt history

            RefResetValue(&ref.fg, reg_vars.ref[0]);

            // Prepare PLEP function to first plateau

            config.final      = first_plateau->ref;
            config.final_rate = 0.0;

#if (FGC_CLASS_ID == 53)
            config.acceleration = (reg.active->state == FGC_REG_B ?
                                   property.ref.defaults.b.acceleration :
                                   FromIEEE(dpcls->mcu.ref.defaults.i.acceleration[load.active->select]));

            config.linear_rate  = (reg.active->state == FGC_REG_B ?
                                   MIN(fabs(meas.hist->rate), property.limits.op.b.rate) :
                                   MIN(fabs(meas.hist->rate), property.limits.op.i.rate));

#else
            config.acceleration =  FromIEEE(dpcls->mcu.ref.defaults.i.acceleration[load.active->select]);
            config.linear_rate  =  MIN(fabs(meas.hist->rate), property.limits.op.i.rate);
#endif

            config.exp_tc       = 0.0;

            fgPlepCalc(&config, &plep, (FP32)ref.time.s, reg_vars.ref[0], config.linear_rate, NULL);

#if (FGC_CLASS_ID != 51)
            // Cycle Check - START_TIME : First plateau arrival time

            if (plep.time[4] > first_plateau->time)
            {
                CycSaveCheckWarning(TRIGG_LOG, FGC_CYC_WRN_START_TIME, FGC_WRN_CYCLE_START,
                                    first_plateau->time,                             // REF.CYC.FAILED.DATA[0]
                                    plep.time[4],                                    // REF.CYC.FAILED.DATA[1]
                                    meas.hist->meas,                                 // REF.CYC.FAILED.DATA[2]
                                    meas.hist->rate);                                // REF.CYC.FAILED.DATA[3]
            }
#endif

            ref.fg.value =  reg_vars.ref[0];

            DisableRegBypass();
        }
        else // Use PLEP to ramp to first plateau
        {
            fgPlepGen(&plep, (const double *)&ref.time.s, &ref.fg.value);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefNone(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING,RUNNING] This function is called when ref.type is FGC_REF_NONE or FGC_REF_ARMED.  In this state,
  the predefined reference is held constant.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefZero(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING] This function is used for a ZERO cycle.  It can only be armed for V and I regulation modes.
  For V it holds the Vref at zero for the whole cycle while for I the function CTRIMs to the first plateau.
\*---------------------------------------------------------------------------------------------------------*/
{
    // If voltage regulation then end immediately

    if (reg.active->state == FGC_REG_V)
    {
        return (1);
    }

    // If current regulation the use a trim function to ramp to the first plateau

    fgTrimGen(&ref.active->pars.zero, (const double *)&ref.time.s, &ref.fg.value);

    return (ref.time.s >= ref.active->meta.duration);
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefPppl(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING] This function derives the reference if the PPPL function is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (ref.time.s < ref.active->pars.pppl.delay)
    {
        RefFirstPlateau();
        return (0);
    }
    else
    {
        return (!fgPpplGen(&ref.active->pars.pppl, (const double *)&ref.time.s, &ref.fg.value));
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefTable(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING,RUNNING] This function derives the reference if the TABLE function is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Start with FIRST_PLATEAU if B regulation is active

#if (FGC_CLASS_ID == 53)
    if (reg.active->state == FGC_REG_B &&
        ref.time.s < ref.active->table_data.function[1].time)
    {
        RefFirstPlateau();
        return (0);
    }

#endif

    return (!fgTableGen(&ref.active->pars.table, (const double *)&ref.time.s, &ref.fg.value));
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefPlep(void)
/*---------------------------------------------------------------------------------------------------------*\
  [RUNNING] This function derives the reference if the PLEP function is selected - this is used for the
  ramp to STANDBY or when the user selects the PLEP reference function.  Note that plep contains the
  working PLEP parameters while ref_fg.plep contain the user PLEP parameters.  The working parameters
  can be the user parameters or can be calculated following a SLOW_ABORT or command to ramp to ON_STANDBY,
  or the end of an OPENLOOP function.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (!fgPlepGen(&plep, (const double *)&ref.time.s, &ref.fg.value));
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefTrim(void)
/*---------------------------------------------------------------------------------------------------------*\
  [RUNNING] This function derives the reference if the LTRIM or CTRIM function is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (!fgTrimGen(&ref_fg.trim, (const double *)&ref.time.s, &ref.fg.value));
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefTest(void)
/*---------------------------------------------------------------------------------------------------------*\
  [RUNNING] This function derives the reference if a Test function (STEPS, SQUARE, SINE or COSINE) is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (!fgTestGen(&ref_fg.test, (const double *)&ref.time.s, &ref.fg.value));
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefOpenloop(void)
/*---------------------------------------------------------------------------------------------------------*\
  [RUNNING] This function derives the reference if the OPENLOOP function is active.  It is not included in
  the fg library.  When the current loop is closed again, the reference runs PLEP.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct fg_plep_config config;

    // Pre trim coast - hold Vref fixed

    if (ref.time.s <= ref_fg.openloop.run_delay)
    {
        ref.v_openloop = ref.v_fltr;
    }

    // Ramping

    else
    {
        // Ramp voltage smoothly to OPENLOOP Vref level (provides smooth acceleration)

        ref.v_openloop += ref_fg.openloop.d_v_ref;

        if ((ref_fg.openloop.pos_ramp_flag && ref.v_openloop > ref_fg.openloop.v_ref)
            || (!ref_fg.openloop.pos_ramp_flag && ref.v_openloop < ref_fg.openloop.v_ref))
        {
            ref.v_openloop = ref_fg.openloop.v_ref;
        }

        // If close loop threshold passed - prepare to end function with a PLEP

        if ((ref_fg.openloop.pos_ramp_flag && meas.hist->meas > ref_fg.openloop.i_closeloop)
            || (!ref_fg.openloop.pos_ramp_flag && meas.hist->meas < ref_fg.openloop.i_closeloop))
        {
            // Prepare PLEP configuration.
            // Important: The linear rate CAN exceed LIMITS.OP.I.RATE in some cases and therefore the reference PLEP_NO_CLIP
            //            is used instead of a regular PLEP (PLEP_NO_CLIP is the same function except clipping is not
            //            applied to it in the regulation algorithm.)

            config.final        = ref_fg.openloop.final;
            config.linear_rate  = fabs(ref.fg.rate);
            config.final_rate   = 0.0;
            config.exp_tc       = 0.0;
            config.acceleration = 0.5 * config.linear_rate * config.linear_rate / fabs(config.final - ref.fg.estimated);

            // Calculate working PLEP parameters

            // NB: the alternative that is to call fgPlepCalc with ref.time as the function delay is not advised
            //     here since the duration of an openloop function can be several hours and there might be a
            //     precision issue on the PLEP timings.
            //     Instead we choose to reset the time (ref.time.s = 0.0) and to set the PLEP delay equal to 0.0.

            fgPlepCalc(&config, &plep, 0.0, ref.fg.estimated, ref.fg.rate, &ppm[0].armed.meta);

            ref.time.s              = 0.0;
            ref.time.period_counter = 0;

            // Prepare to run PLEP on next iteration

            ref.time_remaining.s = plep.time[4] - ref.time.s;
            ref.time_remaining.period_counter = DSP_CEIL_TIME_VP(ref.time_remaining.s);

            // Switch to PLEP_NO_CLIP function type, that shall close the loop on the current iteration and therefore
            // one needs to set a value for ref.fg.value

            ref.func_type        = FGC_REF_PLEP_NO_CLIP;
            ref.fg.value         = ref.fg.estimated;

            // Update meta data and RTD

            ppm[0].armed.meta.duration    = ref.time_remaining.s;

            ppm[0].armed.meta.range.start =        ref.fg.value;
            dpcls->dsp.ref.start          = ToIEEE(ref.fg.value);
            ppm[0].armed.meta.range.end   =        config.final;
            dpcls->dsp.ref.end            = ToIEEE(config.final);

            ppm[0].armed.meta.range.min   = MIN(ref.fg.value, config.final);
            ppm[0].armed.meta.range.max   = MAX(ref.fg.value, config.final);
        }
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefEnd(void)
/*---------------------------------------------------------------------------------------------------------*\
  [CYCLING] This function returns the current to zero as quickly as possible using open-loop (Class 53 only)
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 53)
    FP32        i_meas;
    FP32        vref_clip;

    // Clear I and B references since this always running in V regulation

    RefResetValue(&ref.i,         0.0);
    RefResetValue(&ref.i_clipped, 0.0);
    RefResetValue(&ref.b,         0.0);

    // Normalise voltage to be a positive ramp down

    i_meas = ref_fg.end.direction * meas.i_hist.meas;

    // Set voltage reference to maximum and then clip as current approaches zero

    ref.fg.value = ref_fg.end.v_ref;                                            // Always negative
    vref_clip    = ref_fg.end.clip_factor * (i_meas + lim_i_meas.zero);         // Add offset to drive current
    // through zero
    // If current crosses zero then END function is finished

    if (i_meas <= 0.0)
    {
        ref_fg.end.direction = 0.0;
        ref.func_type = FGC_REF_NONE;
    }

    // Clip the voltage reference to respond to the measured current

    if (ref.fg.value < vref_clip)
    {
        ref.fg.value = vref_clip;
    }

    ref.fg.value *= ref_fg.end.direction;

    // Return reference in ref.fg.value and ref.v_openloop to cover all regulation states

    ref.v_openloop = ref.fg.value;
#endif
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefStarting(void)
/*---------------------------------------------------------------------------------------------------------*\
  [TO_STANDBY] This function sets the reference to the estimated values during the openloop startup period.
  This is a minimum of 10 regulation periods to ensure that the RST history is correctly tracking the open
  loop behaviour.  After this, it checks the end-of-startup threshold, which is always true for bipolar
  converters and is the I_CLOSELOOP for unipolar converters.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct meas_hist * i_meas_hist_ptr;         // Pointer to the measurement history used to estimate Irate.

    // On high-speed converters (regulation period <= 10 ms)

    if(reg.hi_speed_f)
    {
        // Irate measured based on Imeas history over the last 4 DSP iterations

        i_meas_hist_ptr = &meas.i_hist;
    }
    else
    {
        // Irate measured based on Imeas history over the last 4 regulation periods

        i_meas_hist_ptr = &meas.i_hist_reg;
    }

    // Check for end of start
    // Once RST history buffer is ready

    if(ref.time_running.s >= ref_fg.starting.delay)
    {
        // Check for timeout

        if(ref.time_remaining.s == 0.0)
        {
            Set(rtcom.faults, FGC_FLT_REG_ERROR);

            return 0;
        }

        // In case of bipolar converters, we go to standby immediately

        if(!lim_i_ref.flags.unipolar)
        {
            refto.flags.standby = true;

            return 0;
        }

        // Detect when to close the loop

        if(fabs(meas.i) > limits.i_closeloop)
        {
            // Closeloop and PLEP to I_MIN

            refto.flags.standby = true;
        }

        // STARTING openloop function, divided into two parts:
        //  1 - A very slow ramp that lasts for around 6 s and that raises the current up to limits.i_start.
        //      limits.i_start is equal to 1% of LIMITS.I.MIN
        //  2 - A dI/dt close loop with a fixed ramp equal to REF.RATE.I.STARTING until the current reaches
        //      LIMITS.I.CLOSELOOP, at which point the FGC will switch to normal I regulation.

        if(ref_fg.starting.openloop_f)
        {
            // Ramp Vref with constant dVdt

            ref.v_openloop += (reg.active->rst_pars.period * ref_fg.starting.v_rate);

            // If inductive load (Tc > 1s) and current above start threshold, close loop on dI/dt

            if(INDUCTIVE_LOAD && fabs(meas.i) > limits.i_start)
            {
                ref_fg.starting.openloop_f = false;
            }
        }
        else
        {
            // else closed loop start mode (on dI/dt)

            FP32 i_rate = ref_fg.starting.i_rate;

            // If VS gain is good and parallel resistance pole is not significant

            if(!reg_v_err.flags.warning && !RESISTIVE_ZERO_IS_SIGNIFICANT)
            {
                // Close loop on dI/dt

                ref.v_openloop += reg.active->rst_pars.period *
                                  (i_rate * load.active->pars.ohms +
                                  load.active->pars.henrys * (i_rate - i_meas_hist_ptr->rate));
            }
            else
            {
                // Set Vref using openloop on Imeas

                ref.v_openloop = meas.i * load.active->pars.ohms + load.active->pars.henrys * i_rate;
            }
        }
    }

    return 0;
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t RefStopping(void)
/*---------------------------------------------------------------------------------------------------------*\
  [STOPPING] This function derives the reference if the STOPPING function is active.  It is only used by
  2-Quadrant converters to ramp down smoothly in openloop to 1% of I_MIN.  It calculates the Vref value by a
  proportional feedback control on dI/dt.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32               vref_clip_neg;
    struct meas_hist * i_meas_hist_ptr;         // Pointer to the measurement history used to estimate Irate.

    if (reg.hi_speed_f)                         // On high-speed converters (regulation period <= 10 ms)
    {
        // Irate measured based on Imeas history over the last 4 DSP iterations
        i_meas_hist_ptr = &meas.i_hist;
    }
    else
    {
        // Irate measured based on Imeas history over the last 4 regulation periods
        i_meas_hist_ptr = &meas.i_hist_reg;
    }

    // Close loop on dI/dt. The target dI/dt (i_rate_min) is reached progressively starting from i_rate_init.

    // Ramp down the dI/dt reference
    ref_fg.stopping.i_rate += ref_fg.stopping.i_rate_step;
    // Clip reference to target level
    ref_fg.stopping.i_rate  = MAX(ref_fg.stopping.i_rate, ref_fg.stopping.i_rate_min);

    ref.v_openloop += reg.active->rst_pars.period *
                      (ref_fg.stopping.i_rate * load.active->pars.ohms +
                       load.active->pars.henrys * (ref_fg.stopping.i_rate - i_meas_hist_ptr->rate));


    if (property.limits.op.i.min != 0.0)
    {
        // Clip Vref so that 1% of I_MIN gives 2% of V_NEG. This provides a smooth stop.

        vref_clip_neg = 2.0 * property.limits.op.v.neg * meas.hist->estimated / property.limits.op.i.min;

        if (ref.v_openloop < vref_clip_neg)             // Clip voltage
        {
            ref.v_openloop = vref_clip_neg;
        }
    }

    if (meas.hist->estimated > limits.i_start)          // If current is still greater than start threshold
    {
        return (0);
    }

    return (1);
}
/*---------------------------------------------------------------------------------------------------------*/
void RefRefreshValue(struct ref_container * ref_ptr)
/*---------------------------------------------------------------------------------------------------------*\
  Utility function on struct ref_container.

  Calling that function acknowledge the new value of the reference ref_ptr->value and compute the rate of
  change and the estimated reference on the next reg iteration.

  Caution: The update of ref_ptr->prev_value must be done once at the start of every regulation iteration.
           It was decided to do it for all instantiations of the ref_container in the Ref() function, instead
           of doing it in the present function. Doing it in RefRefreshValue() would have the drawback that
           if RefRefreshValue() is called twice during the same regulation iteration, the computed rate
           would be zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 diff = ref_ptr->value - ref_ptr->prev_value;

    ref_ptr->estimated = ref_ptr->value + diff;
    ref_ptr->rate      = diff * reg.active->rst_pars.freq;

    // ref_ptr->prev_value is updated in Ref(). Do not edit it in this function, see comment above.
}
/*---------------------------------------------------------------------------------------------------------*/
void RefResetValue(struct ref_container * ref_ptr, FP32 reset_value)
/*---------------------------------------------------------------------------------------------------------*\
  Utility function on struct ref_container.

  Reset the structure, assuming the reference was constant equal to reset_value until now.
\*---------------------------------------------------------------------------------------------------------*/
{
    ref_ptr->estimated = ref_ptr->value = ref_ptr->prev_value = reset_value;
    ref_ptr->rate = 0.0;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ref.c
\*---------------------------------------------------------------------------------------------------------*/
