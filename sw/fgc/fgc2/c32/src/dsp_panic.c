/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp_panic.c

  Purpose:      FGC DSP Software - DSP panic code for FGC2
\*---------------------------------------------------------------------------------------------------------*/

#define DSP_PANIC_GLOBALS

#include <dsp_panic.h>
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <dsp_dependent.h>      // for DISABLE_INTS, LEDS_INIT, LED0_TOGGLE, LED1_TOGGLE
#include <rtcom.h>              // for rtcom global variable

/*---------------------------------------------------------------------------------------------------------*/
void DspPanic(enum dsp_panic_codes panic_code)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the DSP panic function. It does not return.
  This function disables interrupts and runs an infinite loop.
\*---------------------------------------------------------------------------------------------------------*/
{
    // TODO DspPanic: get the calling SP

    DISABLE_INTS;
    RUNCODE(0xDEAD);

    dpcls->dsp.panic.panic_code = panic_code;

    // Blinking DSP LEDs (visible on FGC development crate only)

    for(;;)
    {
        LEDS_INIT;

        while(dpcom->mcu.time.now_ms.ms_time != 375 && dpcom->mcu.time.now_ms.ms_time != 875);

        LED0_TOGGLE;
        LED1_TOGGLE;

        while(dpcom->mcu.time.now_ms.ms_time != 0   && dpcom->mcu.time.now_ms.ms_time != 500);
    }

    // Never returns
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_panic.c
\*---------------------------------------------------------------------------------------------------------*/

