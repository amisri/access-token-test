/*---------------------------------------------------------------------------------------------------------*\
  File:         time.c

  Purpose:      FGC2 Class 51 Software - Time and events handling
\*---------------------------------------------------------------------------------------------------------*/

// time.c function declarations
// TODO Rename time.c and create corresponding header file. time.h already exists as part of the compiler header files and dsp_time.h is already taken.

#include <stdbool.h>

#include <dsp_time.h>
#include <props.h>      // for property global variable
#include <dpcls.h>      // for dpcls global variable
#include <dpcom.h>      // for dpcom global variable
#include <rtcom.h>      // for rtcom global variable
#include <main.h>       // for state_pc global variable
#include <reg.h>        // for reg, load global variables
#include <pars.h>       // for ParsRunTime()
#include <cyc.h>        // for cyc global variable
#include <ref.h>        // for TestRefAbortable()
#include <macros.h>     // for Test()
#include <refto.h>      // for RefToAborting()
#include <isr.h>
#include <mcu_dsp_common.h>
#include <dsp_dependent.h> // for test points TP0/TP1
#include <bgp_plat.h>
#include <defprops_dsp_fgc.h>
#include <dsp_panic.h>

/*---------------------------------------------------------------------------------------------------------*/
static void TimeCalcEvt(struct abs_time_us * abs_time, uint32_t delay_ms)
/*---------------------------------------------------------------------------------------------------------*\
  This function produces an absolute time structure that correspond to the present Fieldbus time, plus
  a delay expressed in milliseconds
\*---------------------------------------------------------------------------------------------------------*/
{
    AbsTimeCopyFromMsToUs((const struct abs_time_ms *)&dpcom->mcu.time.fbs, abs_time);

    AbsTimeUsAddDelay(abs_time, 1000 * delay_ms);
}
/*---------------------------------------------------------------------------------------------------------*/
void TimeIteration(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at the start of every iteration to manage the iteration time
\*---------------------------------------------------------------------------------------------------------*/
{
    // Get time now and 10/20/200ms time from MCU (FGC3) or UTC register (FGC3)

    if (rtcom.time_now_ms.ms_time != dpcom->mcu.time.now_ms.ms_time)    // If start of a new millisecond
    {
        rtcom.time_now_ms  = dpcom->mcu.time.now_ms;
        AbsTimeCopyFromMsToUs(&rtcom.time_now_ms, &rtcom.time_now_us);

        rtcom.ms10         = dpcom->mcu.time.ms_mod_10;
        rtcom.ms20         = dpcom->mcu.time.ms_mod_20;
        rtcom.ms200        = dpcom->mcu.time.ms_mod_200;

        rtcom.start_of_ms_f = true;
    }
    else
    {
        rtcom.start_of_ms_f = false;
    }

    // Start of 10/20/200/1000ms flags. To be used ONLY in ISR context.

    rtcom.start_of_ms10_f   = rtcom.start_of_ms_f && (rtcom.ms10  == 0);
    rtcom.start_of_ms20_f   = rtcom.start_of_ms_f && (rtcom.ms20  == 0);
    rtcom.start_of_ms200_f  = rtcom.start_of_ms_f && (rtcom.ms200 == 0);
    rtcom.start_of_second_f = rtcom.start_of_ms_f && (rtcom.time_now_ms.ms_time == 0);
}
/*---------------------------------------------------------------------------------------------------------*/
void TimeRunningEvents(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the start/abort timing event delays from the MCU to see if a Start or Abort timing
  event has been received.  It sets the relevant property value and processes the time.
  This function is called on every iteration period in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    // If Start event is active then update REF.RUN time

    if (dpcom->mcu.evt.start_event_delay)
    {
        TimeCalcEvt(&property.ref.run, dpcom->mcu.evt.start_event_delay);

        dpcom->mcu.evt.start_event_delay = 0;
        ParsRunTime();
    }

    // If Abort event is active then update REF.ABORT time

    if (dpcom->mcu.evt.abort_event_delay)
    {
        TimeCalcEvt(&property.ref.abort, dpcom->mcu.evt.abort_event_delay);
        dpcom->mcu.evt.abort_event_delay = 0;
        ParsAbortTime();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TimeRunning(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks if a reference function should start running or should be aborted.
  This function is called on every iteration period in ISR context, in both cycling and non-cycling modes.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ref.func_type)
    {
        case FGC_REF_NONE:          // Function is disarmed

            ref.time_running.s                = 0.0;
            ref.time_running.period_counter   = 0;
            ref.time_remaining.s              = 0.0;
            ref.time_remaining.period_counter = 0;

            if (state_pc == FGC_PC_IDLE)
            {
                if (dpcls->mcu.ref.func.reg_mode[0] != FGC_REG_V)   // I or B
                {
                    if (reg.active->state != dpcls->mcu.ref.func.reg_mode[0])
                    {
                        RegStateRequest(false, true, dpcls->mcu.ref.func.reg_mode[0], ref.v.value);
                    }
                }
                else                                                // V
                {
                    if (reg.active->state != FGC_REG_V)
                    {
                        RegStateV(true, ref.v_fltr);
                    }
                }
            }

            break;

        case FGC_REF_ARMED:         // Function is armed

            //
            // REF.RUN
            //
            // If RUN time has arrived then start running function

            if (AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.run))
            {
                ref.time.s              = 0.0;
                ref.time.period_counter = 0;
                ref.func_type           = dpcls->dsp.ref.func.type[0];
                property.ref.run        = rtcom.time_now_us;
                ref.reg_bypass_mask     = DEFAULT_REF_REG_BYPASS;
                dpcls->dsp.ref.start_f  = true;
            }

            //
            // REF.ABORT
            //
            // else if abort time has arrived disarm reference

            else if (AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.abort))
            {
                ref.func_type = FGC_REF_NONE;
            }

            break;

        default:                    // Function is running

            // If NOT in CYCLING or TO_CYCLING state

            if (state_pc != FGC_PC_TO_CYCLING && state_pc != FGC_PC_CYCLING)
            {
                // Update reference time and time running and remaining (for MCU)
                // To improve the accuracy on floating point values for the ref time, this function increments
                // a counter (ref.time.period_counter) and multiplies it by the V regulation period.
                // The same concept is used in cycling with cyc.time: see comments in function TimeCycling().

                ref.time.period_counter++;
                ref.time.s = (FP32)ref.time.period_counter * property.reg.v.rst_pars.period;

                // The same idea as for ref.time is used for ref.time_running and ref.time_remaining

                ref.time_running.period_counter++;
                ref.time_running.s = (FP32)ref.time_running.period_counter * property.reg.v.rst_pars.period;

                if (ref.time_remaining.period_counter)
                {
                    ref.time_remaining.period_counter--;
                    ref.time_remaining.s = (FP32)ref.time_remaining.period_counter * property.reg.v.rst_pars.period;
                }
                else
                {
                    ref.time_remaining.s = 0.0;
                }
            }

            break;
    }

    //
    // REF.ABORT
    //
    // If function is abortable, abort time has arrived, and we are not in cycling then start abort

    if (AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.abort))
    {
        if (TestRefAbortable() && state_pc != FGC_PC_TO_CYCLING && state_pc != FGC_PC_CYCLING)
        {
            // RefToAborting() will reset the mirror value ref.abort, but
            refto.flags.aborting = true;
            // not the property REF.ABORT
        }
        else
        {
            // Cancel the REF.ABORT because the present reference cannot be aborted.
            // The REF.ABORT property will read zero, indicating that no abort took place.

            property.ref.abort.unix_time  = 0;                  // Reset property REF.ABORT and its mirror value ref.abort
            property.ref.abort.us_time    = 0;
            ref.abort.unix_time           = TIME_NEVER;
        }
    }

    // In low-speed regulation (regulation period > 10 ms), align regulation on 12s UTC boundaries

    if (reg.hi_speed_f == false)
    {
        if (rtcom.start_of_second_f && rtcom.time_now_ms.unix_time % 12 == 0)   // If 12s boundary
        {
            // Then this iteration is a regulation one
            reg.period_counter = reg.active->rst_pars.period_iters - 1;
        }
    }

    // Set the run_f flag which defines if this iteration is a regulation one or not

    reg.period_counter++;

    // period_iters NOT being a constant, the "greater than" comparison is required.
    if (reg.period_counter >= reg.active->rst_pars.period_iters)
    {
        reg.period_counter   = 0;               // Reset period_counter
        dpcls->dsp.reg.run_f = true;
    }
    else
    {
        dpcls->dsp.reg.run_f = false;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TimeCyclingEvents(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to process cycling related timing events.
  This function is called on every iteration period in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Next cycle start event received

    if(!cyc.start_cycle_evt_f && dpcom->mcu.evt.next_cycle_start_delay_ms != 0)
    {
        cyc.start_cycle_evt_f = true;

        // Calculate event time

        TimeCalcEvt(&cyc.start_time_next, dpcom->mcu.evt.next_cycle_start_delay_ms);
    }

    // Start super-cycle event received (on ms 20 of first cycle in the super-cycle)

    if(!cyc.ssc_evt_f && dpcom->mcu.evt.ssc_f)
    {
        cyc.ssc_evt_f = true;

        CycStartSuperCycle();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TimeCycling(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called on every iteration period in ISR context, to manage the cycle time.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID != 51)

    bool start_new_cycle_f = false;

    // Compute the current cycle time using a multiplication. This is more accurate than the alternative
    // method using a summation (cyc.time.s += property.reg.v.rst_pars.period;). Some empirical data for future reference:
    //
    // cyc.time.period_counter = 3599   per     (End of a 3.6 s cycle; 1 per = 1 ms)
    // cyc.time.s              = 3.599000 s     (Using a multiplication)
    // cyc.time.s              = 3.598800 s     (Using a summation)
    // delta                   = 0.000200 s

    cyc.time.period_counter++;
    cyc.time.s = (float)cyc.time.period_counter * property.reg.v.rst_pars.period;

    // Check for start of cycle

    if(AbsTimeUs_IsGreaterThan(rtcom.time_now_us, cyc.start_time_next))
    {
        // Don't come back here twice during the same cycle

        cyc.start_time_next.unix_time = TIME_NEVER;

        // Cycle Check - SC_REF_OVERRUN : current reference function is too long and has not yet completed.

        if(ref.cycling_f && ref.func_type != FGC_REF_NONE && ref.time_remaining.s > FGC_ACQ_TIME_IN_SECONDS)
        {
            CycSaveCheckWarning(TRIGG_LOG, FGC_CYC_WRN_REF_OVERRUN, FGC_WRN_TIMING_EVT,
                                (float)cyc.ref_user,
                                (float)cyc.timing_user,
                                ref.time_remaining.s,
                                0.0);

            CycCancelNextUser();
        }
        else
        {
            start_new_cycle_f = true;
        }
    }

    if(start_new_cycle_f)
    {
        // Accumulate super-cycle length for RMS calculation

        i_rms.sc_length_s += cyc.time.s;

        // Zero cycle time

        cyc.time.s              = 0.0;
        cyc.time.period_counter = 0;

        // Send warning or fault to the MCU for the LOG.TIMING property

        CycSendMCUCheckResults();

        // Cycle Check - CYC_WARN_LIM : Too many consecutive warnings

        if(cyc.consecutive_wrn_cnt >= MAX_CONSECUTIVE_WRN)
        {
            CycSaveCheckFault(FGC_CYC_FLT_CYC_WARN_LIM, FGC_WRN_CYCLE_START,
                              (float)MAX_CONSECUTIVE_WRN,
                              (float)cyc.consecutive_wrn_cnt,
                              0.0,
                              0.0);

            // Schedule a cycle for user 0 (this will be ref NONE with reg state = V)

            dpcom->mcu.evt.next_cycle_user = 0;
            cyc.next_user                  = 0;
        }

        CycStartCycle();
    }
    else if(ref.cycling_f                &&
            cyc.time.period_counter == 1 &&
            cyc.ref_func_type != FGC_REF_NONE)
    {
        // On FGC2, part of the copy of the reference is done on C0 + 1 ms

        CycCopyRefFunction();
    }

    // When cycling, drive the reference time from the cycle time

    if(ref.cycling_f)
    {
        ref.time.s = cyc.time.s + ref.advance;

        // ref.time.period_counter is not used in cycling. Should always be 0.

        ref.time.period_counter = 0;
    }

#endif
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: time.c
\*---------------------------------------------------------------------------------------------------------*/
