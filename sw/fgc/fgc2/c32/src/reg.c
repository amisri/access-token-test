/*---------------------------------------------------------------------------------------------------------*\
  File:     reg.c

  Purpose:  FGC DSP Software - contains RT regulation functions

  Notes:    All real-time processing runs at interrupt level under IsrPeriod().
\*---------------------------------------------------------------------------------------------------------*/

#include <reg.h>
#include <string.h>             // for memset
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <rtcom.h>              // for rtcom global variable
#include <meas.h>               // for meas global variable
#include <adc.h>                // for adc, adc_chan global variables
#include <sim.h>                // for sim global variable
#include <pars.h>               // for ParsRegV()
#include <macros.h>             // for Test()
#include <dsp_time.h>           // for DSP_ROUND_TIME_MS()
#include <isr.h>
#include <bgp.h>
#include <bgp_plat.h>
#include <mcu_dsp_common.h>
#include <dsp_dependent.h>      // for DISABLE_INTS/ENABLE_INTS
#include <main.h>


/*---------------------------------------------------------------------------------------------------------*/
void RegState(uint32_t inform_mcu_f, uint32_t reg_state, FP32 v_ref)
/*---------------------------------------------------------------------------------------------------------*\
  [FGC2]
  This function is called at the start of each cycle when CYCLING to set the regulation state
  for that cycle.

  This function runs in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Remember the previous regulation state

    reg.active->previous_state = reg.active->state;

    // On synchronous DSP (i.e. FGC2) the ISR must copy the regulation parameters from the property values.

    // Copy the V regulation parameters

    reg.active->rst_v_pars = property.reg.v.rst_pars;

    switch(reg_state)
    {
    case FGC_REG_V:                     // Voltage regulation

        reg.active->rst_pars                 = reg.active->rst_v_pars;
        reg.active->rst_pars.rst.track_delay = reg.active->rst_v_pars.rst.track_delay;

        ref.advance          = 0.001;           // 1 ms
        meas.hist            = &meas.i_hist;
        ref.now              = &ref.i;          // SPY signal REF = 0
        RefResetValue(&ref.fg,            v_ref);
        RefResetValue(&ref.fg_plus_rt,    v_ref);
        RefResetValue(&ref.i,             0.0);
        RefResetValue(&ref.i_clipped,     0.0);
        RefResetValue(&ref.b,             0.0);

        break;

    case FGC_REG_I:                     // Current regulation

        if(!reg.test_user_f)                        // Operational parameters
        {
            reg.active->rst_pars    = property.reg.i.op;
        }
        else                                        // Test parameters
        {
            regRstInit(&reg.active->rst_pars,
                       property.reg.v.rst_pars.period,
                       property.reg.test.i.period_div,
                       &load.active->pars,
                       0.0,0.0,0.0,0.0,
                       &property.reg.test.i.rst);
        }

        // Initialise I regulation errors

        regErrInitDelay( &reg_bi_err,
                         &reg_bi_ref_history_buffer[0],
                         reg.active->rst_pars.rst.track_delay / reg.active->rst_pars.period,
                         load.active->pars.sim.load_undersampled_flag);

        regErrInitLimits(&reg_bi_err,
                         property.limits.i.err_warning[property.load.select],
                         property.limits.i.err_fault  [property.load.select]);

        RefResetValue(&ref.b, 0.0);

        meas.hist = &meas.i_hist;

        // The following function will recalculate the RST history for a bumpless regulation transition
        // Note: Even if the regulation mode is not modified, we reconstruct the RST history in case for example
        // The regulation period has changed.

        RegVarsInit(v_ref, reg.active->rst_pars.period, reg.active->rst_pars.rst.track_delay);

        if(reg.active->previous_state != FGC_REG_I)
        {
            // ref.i will be equal to 0.0 because we come from a different regulation state.
            // Starting from zero, the I reference might need several regulation periods to reach ref.fg.value (because
            // LIMITS.I.RATE would apply), and that can cause a bump on the current.

            RefResetValue(&ref.fg,        reg_vars.ref[0]);
            RefResetValue(&ref.i,         reg_vars.ref[0]);
            RefResetValue(&ref.i_clipped, reg_vars.ref[0]);
        }

        // High-speed current measurement has a delay of 1.3 times the DSP iteration period due to analogue and dig filters
        // Low-speed  current measurement uses a filter with latency compensation, so measurement delay is zero.

        ref.advance = reg.active->rst_pars.rst.track_delay -
                     (TEST_REG_HI_SPEED(reg.active->rst_pars.period) ? IMEAS_DELAY_ITERS * DSP_ITER_PERIOD : 0.0);

        // Ref.now

        ref.now = &ref.i;

        break;

#if (FGC_CLASS_ID == 53)
    case FGC_REG_B:                     // Field regulation

        if(!reg.test_user_f)                             // Operational parameters
        {
            regRstInit(&reg.active->rst_pars,
                       property.reg.v.rst_pars.period,
                       property.reg.b.period_div[property.load.select],
                       &load.active->pars,
                       0.0,0.0,0.0,0.0,
                       &property.reg.b.manual_rst);
        }
        else                                        // Test parameters
        {
            regRstInit(&reg.active->rst_pars,
                       property.reg.v.rst_pars.period,
                       property.reg.test.b.period_div,
                       &load.active->pars,
                       0.0,0.0,0.0,0.0,
                       &property.reg.test.b.rst);

        }

        // Initialise B regulation errors

        regErrInitDelay( &reg_bi_err,
                         &reg_bi_ref_history_buffer[0],
                         reg.active->rst_pars.rst.track_delay / reg.active->rst_pars.period,
                         load.active->pars.sim.load_undersampled_flag);

        regErrInitLimits(&reg_bi_err,
                         property.limits.b.err_warning,
                         property.limits.b.err_fault);

        RefResetValue(&ref.i,         0.0);
        RefResetValue(&ref.i_clipped, 0.0);

        meas.hist = &meas.b_hist;

        // The following function will recalculate the RST history for a bumpless regulation transition
        // Note: Even if the regulation mode is not modified, we reconstruct the RST history in case for example
        // The regulation period has changed.

        RegVarsInit(v_ref, reg.active->rst_pars.period, reg.active->rst_pars.rst.track_delay);

        if(reg.active->previous_state != FGC_REG_B)
        {
            // ref.b will be equal to 0.0 because we come from a different regulation state.
            // Starting from zero, the B reference might need several regulation periods to reach ref.fg.value (because
            // LIMITS.B.RATE would apply), and that can cause a bump on the field.

            RefResetValue(&ref.fg, reg_vars.ref[0]);
            RefResetValue(&ref.b,  reg_vars.ref[0]);
        }

        // B-train has no measurement delay so ref advance is simply the regulation tracking delay

        ref.advance = reg.active->rst_pars.rst.track_delay;

        // Ref.now

        ref.now = &ref.b;

        break;
#endif
    }

    // Initialise voltage regulation parameters

    ref.v.value = ref.v_openloop = v_ref;

    // V regulation error (Reset of B/I errors is done in RegVarsInit, called above)

    // Call to regErrInitFilterPeriod. Regarding the second parameter (filter_period_iters):
    //  - In V regulation, rst_pars = rst_v_pars therefore filter_period_iters = 1.
    //  - In B/I regulation, filter_period_iters is equal to the number of iteration periods
    //    in a regulation period. The V measurement will be averaged on a regulation period.

    regErrInitFilterPeriod(&reg_v_err, reg.active->rst_pars.period_iters);

    regErrInitDelay( &reg_v_err,
                     &reg_v_ref_history_buffer[0],
                     reg.active->rst_v_pars.rst.track_delay / reg.active->rst_v_pars.period,
                     load.active->pars.sim.vs_undersampled_flag);

    regErrInitLimits(&reg_v_err,
                     property.limits.v.err_warning,
                     property.limits.v.err_fault);

    // Set 20ms sliding window filter factor (provides latency compensation)

    if(reg.hi_speed_f != TEST_REG_HI_SPEED(reg.active->rst_pars.period))        // If hi_speed_f is changing, reset the measurement delay queues
    {
        reg.hi_speed_f = TEST_REG_HI_SPEED(reg.active->rst_pars.period);

        SimInitDelays(sim.active, ref.v.value, sim.active->v, sim.active->i, sim.active->b);
    }

    // Set the 20-sample sliding window filter factor (provides latency compensation)
    // The history offset should correspond to one regulation period
    // The compensation delay, expressed in units of iteration periods, is the sum of two sub-delays:
    //  - The delay introduced by the 20ms average, equal to (N-1)/2 iterations, where N = nb of samples = 20
    //  - The measurement delay, sum of the FIR digital filter delay and the analogue anti-aliasing delay on the
    //    analogue card.

    adc.filter_20ms.history_offset     = (reg.hi_speed_f ? 10 : reg.active->rst_pars.period_iters);
    adc.filter_20ms.time_ratio         = (9.5 + IMEAS_DELAY_ITERS) / (FP32)adc.filter_20ms.history_offset;

    // Reinitialise period counter

    if(reg.hi_speed_f)
    {
        // Hi-speed regulation (regulation period <= 10 ms)
        // Reset the period_counter so that we are now at the beginning of a regulation period. This ensure that
        // the recalculated RST history (function RegVarsInit) is perfectly aligned with the new regulation iterations

        dpcls->dsp.reg.run_f = false;
        reg.period_counter   = 0;
    }
    else
    {
        // Lo-speed regulation (regulation period > 10 ms)
        // Calculate reg.period_counter so that the regulation iterations are aligned on a 12s boundary. Note that
        // by doing so the recalculated RST history (function RegVarsInit) might have a slight offset in time,
        // causing a small bump in the regulation.

        reg.period_counter   = ((rtcom.time_now_ms.unix_time % 12) * 1000 + rtcom.time_now_ms.ms_time) % reg.active->rst_pars.period_iters;
    }

    // Calculate reference advance in milliseconds for Start/Abort events for when in ARMED/RUNNING

    ref.advance_us = DSP_ROUND_TIME_US(ref.advance);

    // Disarm the reference if reg state is being modified (for example from V to I or from I to V...)

    if(reg.active->previous_state != reg_state)
    {
        ref.func_type = FGC_REF_NONE;
    }

    // Clear warnings

    Clr(rtcom.warnings, FGC_WRN_REF_LIM | FGC_WRN_REF_RATE_LIM);

    // Set the new regulation state and pass it to MCU if required

    reg.active->state = reg_state;

    if ( inform_mcu_f )
    {
        dpcls->dsp.reg.state.int32u = reg_state;
    }

    // Gate ADC filters reset

    AdcGatePars();
}
/*---------------------------------------------------------------------------------------------------------*/
bool RegStateRequest(bool next_cycle_f, bool inform_mcu_f, uint32_t reg_state, FP32 v_ref)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the MCU requests a change of the regulation state, or at the start of each
  cycle (in CYCLING) to set the regulation state for the next user.

  This function can be called in ISR or BGP context. (This is why the access to reg.request.locked is
  protected against interrupts.)

  next_cycle_f will indicate if the regulation state change is to occur on the start of the next cycle
  (if next_cycle_f == true), or immediately (if next_cycle_f == false).
\*---------------------------------------------------------------------------------------------------------*/
{
    RegState(inform_mcu_f, reg_state, v_ref);
}
/*---------------------------------------------------------------------------------------------------------*/
void RegStateV(uint32_t inform_mcu_f, FP32 v_ref)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to switch rapidly to openloop regulation, in case a regulation error is detected,
  in the END reference function in cycling mode of operation, or simply moving to the OFF state.

  This function supposes that reg.active->rst_v_pars is already set to the correct V regulation parameters.
  Therefore it should be called after an initial call to RegStateRequest(B/I/V) that will initialise that
  structure.

  This function is preferably called in ISR context.
  It can also be called in BGP (background task) context, thanks to the critical section inside the function.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Execute in critical section in case this function is called from the BGP task
    DISABLE_INTS;

    // Copy the WHOLE openloop regulation structure into the active regulation structure

    reg.active->rst_pars    = reg.active->rst_v_pars;

    ref.advance             = 0.001;                            // ref.advance becomes 1 ms
    meas.hist               = &meas.i_hist;
    ref.now                 = &ref.i;                           // SPY signal REF = 0

    RefResetValue(&ref.v,             v_ref);
    RefResetValue(&ref.fg,            v_ref);
    RefResetValue(&ref.fg_plus_rt,    v_ref);
    RefResetValue(&ref.i,             0.0);
    RefResetValue(&ref.i_clipped,     0.0);
    RefResetValue(&ref.b,             0.0);

    // Initialise voltage regulation parameters

    ref.v_openloop = v_ref;

    // Calculate reference advance in milliseconds for Start/Abort events for when in ARMED/RUNNING

    ref.advance_us = DSP_ROUND_TIME_US(ref.advance);

    // Clear warnings

    Clr(rtcom.warnings, FGC_WRN_REF_LIM | FGC_WRN_REF_RATE_LIM);

    // Disarm reference

    BgpInitNone(FGC_REG_V, 0);                  // First argument is ignored
    ref.func_type = FGC_REF_NONE;

    // Remember previous and new regulation state and pass to MCU if required

    reg.active->previous_state = reg.active->state;
    reg.active->state          = FGC_REG_V;

    // End of critical section
    ENABLE_INTS;

    if(inform_mcu_f)
    {
        dpcls->dsp.reg.state.int32u = FGC_REG_V;
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void RegPeriod(uint32_t * period_vp_ptr, FP32 * period_ptr)
/*---------------------------------------------------------------------------------------------------------*\
  This function converts a regulation period in units of the V regulation periods into a period in seconds,
  with some constraints on the allowed periods.
  - period_vp_ptr: INPUT/OUTPUT. This is the requested period in units of the V regulation period.
                                 The pointed value may be edited to comply with the above-mentioned constraints
  - period_ptr:    OUTPUT.       This is the pointer to the period, in seconds.
\*---------------------------------------------------------------------------------------------------------*/
{
    // On asynchronous DSP: property.reg.v.rst_pars.period = 1 ms

    uint32_t    idx;
    FP32      period_s;

    // Permitted RST period must be between 10ms and 100ms and divide into 12 seconds exactly

    static FP32 rst_vp_periods[] = { 10, 20, 30, 40, 50, 60, 80, 100, 0 };

    // Round regulation period down to an allowed value: 1 ms or one period in rst_periods_s

    if(*period_vp_ptr >= rst_vp_periods[0])                 // If period is >= 10 ms
    {
        for(idx=1 ; rst_vp_periods[idx] != 0 && *period_vp_ptr >= rst_vp_periods[idx]; idx++);

        *period_vp_ptr = rst_vp_periods[idx-1];
    }
    else if(*period_vp_ptr == 0)                    // else clip period at 1ms
    {
        *period_vp_ptr = 1;
    }

    *period_ptr = (*period_vp_ptr) * property.reg.v.rst_pars.period;
}
/*---------------------------------------------------------------------------------------------------------*/
void RegVarsInit(FP32 v_ref, FP32 reg_period, FP32 track_delay)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the RST history arrays to allow the bumpless closing of the current or field
  loops, in high-speed regulation. To do this it uses the last four measurements to determine the rate of
  change. The voltage reference (supplied) should be constant (or close to constant) during this period.

  Note: We might still see a bump in low-speed regulation if changing the regulation period. The reason for
  that is this function works well in low-speed because we reset reg.period_counter on the transition to the
  new regulation parameters. But one does not do so in low-speed because we have the additional constraint
  of having the regulation periods aligned on 12s boundaries.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t    idx;
    FP32      ref_offset;

    // Prepare RST history to have bumpless loop closing

    ref_offset = meas.hist->rate * track_delay;

    for(idx = 0; idx < FGC_N_RST_COEFFS; idx++)
    {
        reg_vars.act [idx] = v_ref;
        reg_vars.meas[idx] = meas.hist->meas - meas.hist->rate * (FP32)idx * reg_period;
        reg_vars.ref [idx] = reg_vars.meas[idx] + ref_offset;
    }

    regErrInitVars(&reg_bi_err);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: reg.c
\*---------------------------------------------------------------------------------------------------------*/

