/*---------------------------------------------------------------------------------------------------------*\
  File:     adc.c

  Purpose:  FGC DSP Software - ADC functions

  Author:   Quentin.King@cern.ch

  Notes:    All real-time processing runs at interrupt level under IsrMst().
\*---------------------------------------------------------------------------------------------------------*/

#define ADC_GLOBALS

#include <adc.h>
#include <mcu_dsp_common.h>
#include <cal.h>
#include <sim.h>                // for sim global variable
#include <libcal.h>             // for calVoltage(), calCurrent()
#include <deftypes.h>           // For MIN/MAX
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dsp_dependent.h>      // for ToIEEE()
#include <rtcom.h>              // for rtcom global variable
#include <defconst.h>           // for FGC_N_ADCS
#include <main.h>               // for dcct_select global variable
#include <macros.h>             // for Set(), Clr(), Test()
#include <reg.h>                // for reg, load global variables
#include <meas.h>               // for meas global variable
#include <dsp_time.h>           // for DSP_ROUND_TIME_MS()
#include <math.h>               // for fabs()
#include <limits.h>

#define SAR_FACTOR      256

// ToDo : try to clean this, here is DSP so no MCU memory map
#include <memmap_mcu.h>         // for ANA_SD_NL_A_R_LOW_NOFRAME_MASK16



void AdcInit(void)
{
    // Determine ADC type for use with calibration limits

    switch(rtcom.interfacetype)
    {
        case FGC_INTERFACE_SD_350:
        case FGC_INTERFACE_SD_351:

            adc.type        = ADC_INTERFACE_SD_350;
            adc.stuck_limit = 100;
            break;

        // With an unknown interface default to SAR_400
        // TODO Add proper error handling for unknown interfaces?

        case FGC_INTERFACE_SAR_400:
        default:

            adc.type        = ADC_INTERFACE_SAR_400;
            adc.stuck_limit = 100;
            break;
    }

    // Initialise history buffer for 20 ms filter

    adc.filter_20ms.history_idx = (VRAW_SUM_HIST_LEN - 1);
}



/*---------------------------------------------------------------------------------------------------------*/
void AdcRead(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets the ADC data from the DPRAM where it is put at the start of each millisecond by the
  MCU. For the SAR interface, there are three 16-bit samples to average.  For the SD interface there is
  just one 48-bit value which is left justified into two long words.  The bottom 2 bits of the 48 are the
  status bits.  The scaling shift is defined by RtAdcMpx with an implicit shift of 14 bits because this
  function starts with the top two words only.  This discards the 14 data bits in the low word (2 lsbs are
  for status). This function also checks the ADC filters state, and the bus error flags for both channels.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t       flt_stat;


    // Return immediately if ADCs blocked or signals being simulated

    if(sim.active->enabled || dpcls->mcu.adc_block_f)
    {
        return;
    }

    // Read ADC data from MCU dpram

    if(rtcom.interfacetype == FGC_INTERFACE_SAR_400)    // If SAR interface, average the 3 16-bit samples
    {
        adc_chan[0].adc_raw = (dpcls->mcu.adc.chan[0].raw[0] +
                      dpcls->mcu.adc.chan[0].raw[1] +
                      dpcls->mcu.adc.chan[0].raw[2] - 0x18000) * SAR_FACTOR;

        adc_chan[1].adc_raw = (dpcls->mcu.adc.chan[1].raw[0] +
                      dpcls->mcu.adc.chan[1].raw[1] +
                      dpcls->mcu.adc.chan[1].raw[2] - 0x18000) * SAR_FACTOR;
    }
    else                    // else SD interface
    {
        // CHANNEL A

        if(!dpcls->mcu.adc.buserr_f[0])
        {
            flt_stat = (dpcls->mcu.adc.chan[0].raw[1] & 0x00030000) >> 16;  // Extract 2 status bits
        }
        else
        {
            flt_stat = 0;
            Set(adc_chan[0].st_meas, FGC_MEAS_ADC_BUSERR);
        }

        adc_chan[0].adc_direct = (dpcls->mcu.adc.chan[0].raw[0] << 14) |
                                  ((uint32_t)dpcls->mcu.adc.chan[0].raw[1] >> 18);

        if(!flt_stat && !dpcls->mcu.adc.buserr_f[0])    // If chan A status is valid
        {
            adc_chan[0].adc_raw = dpcls->mcu.adc.chan[0].raw[0] << (14 - adc_chan[0].shift) |
                                   (uint32_t)dpcls->mcu.adc.chan[0].raw[1] >> (18 + adc_chan[0].shift);
        }
        else                        // else status is invalid
        {
            Clr(adc_chan[0].st_meas, FGC_MEAS_V_MEAS_OK);
            adc_chan[0].adc_raw = 0;

            if(flt_stat & ANA_SD_NL_A_R_LOW_NOFRAME_MASK16) // If chan A status is No Frame
            {
                dpcls->dsp.adc.num_no_frame[0]++;
                Set(adc_chan[0].st_meas, FGC_MEAS_FLT_NO_FRAME);
            }

            if(flt_stat & ANA_SD_NL_A_R_LOW_BADFRAME_MASK16)    // If chan A status is bad frame
            {
                dpcls->dsp.adc.num_frame_err[0]++;
                Set(adc_chan[0].st_meas, FGC_MEAS_FLT_FRAME_ERR);
            }
        }

        // CHANNEL B

        if(!dpcls->mcu.adc.buserr_f[1])
        {
            flt_stat = (dpcls->mcu.adc.chan[1].raw[1] & 0x00030000) >> 16;  // Extract 2 status bits
        }
        else
        {
            flt_stat = 0;
            Set(adc_chan[1].st_meas, FGC_MEAS_ADC_BUSERR);
        }

        adc_chan[1].adc_direct = (dpcls->mcu.adc.chan[1].raw[0] << 14) |
                                  ((uint32_t)dpcls->mcu.adc.chan[1].raw[1] >> 18);

        if(!flt_stat && !dpcls->mcu.adc.buserr_f[1])                    // If chan A status is valid
        {
            adc_chan[1].adc_raw = dpcls->mcu.adc.chan[1].raw[0] << (14 - adc_chan[1].shift) |
                   (uint32_t)dpcls->mcu.adc.chan[1].raw[1] >> (18 + adc_chan[1].shift);
        }
        else                        // else status is invalid
        {
            Clr(adc_chan[1].st_meas, FGC_MEAS_V_MEAS_OK);
            adc_chan[1].adc_raw = 0;

            if(flt_stat & ANA_SD_NL_B_R_LOW_NOFRAME_MASK16) // If chan A status is No Frame
            {
                dpcls->dsp.adc.num_no_frame[1]++;
                Set(adc_chan[1].st_meas, FGC_MEAS_FLT_NO_FRAME);
            }

            if(flt_stat & ANA_SD_NL_B_R_LOW_BADFRAME_MASK16)    // If chan A status is bad frame
            {
                dpcls->dsp.adc.num_frame_err[1]++;
                Set(adc_chan[1].st_meas, FGC_MEAS_FLT_FRAME_ERR);
            }
        }
    }

    // Check ADC reset completion

    if(adc.reset_req_copy == ADC_DSP_RESET_REQ && dpcls->dsp.adc.reset_req == ADC_DSP_RESET_ACK)
    {
        // Reset asked by the DSP has completed!

        adc.reset_req_copy = dpcls->dsp.adc.reset_req;
        adc.last_dsp_reset = rtcom.time_now_us.unix_time;
        property.adc.num_unexpected_resets++;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcFilter(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function processes the ADC signals.  It runs four different filters on the raw data:

       Flt  Type
       ------------------------------------------------------------
    1   1ms average
    2   20ms sliding average with phase compensation
    3   200ms accumulations (inc p-p v_raw)
    4   1s accumulations (inc p-p v_adc)

  The function applies a check for unexpected signal jumps and for the signal becoming stuck.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      c;
    uint32_t      mask;
    uint32_t      flt_idx;
    int32_t      adc_raw;
    int32_t      raw;
    int32_t      vraw_sum_20ms;          // Local storage variable for adc_chan[c].filter_20ms.vraw_sum. To help C32 compiler.
    int32_t      old_vraw_sum_idx;       // Index of the oldest value in adc_chan[].filter_20ms.vraw_sum_history[]
    int32_t      old_vraw_sum;
    FP32        last_i_dcct;
    int32_t      last_adc_raw;

    static bool first_f = true;                      // First time running this function?


    dcct_select      = dpcls->mcu.dcct_select;
    cal.imeas_zero_f = dpcls->mcu.cal.imeas_zero;   // Set for ADC16/DAC calibration

    // Control cal active down counter

    if ( dpcls->mcu.cal.req_f )                 // If any sort of calibration request is active
    {
        cal.ongoing = 1500;                 // Mark calibration as active for 1.5s
    }
    else                                                // cal was active with last 1.5s
    {
        if ( cal.ongoing )
        {
            cal.ongoing--;
        }
    }

    if (     cal.imeas_zero_f                   // If current measurement is blocked
         && (rtcom.state_op != FGC_OP_CALIBRATING)  // and calibration has finished
       )
    {
        dpcls->mcu.cal.imeas_zero--;                // Decrement block down counter
    }

    // Adjust filter indexes

    flt_idx = rtcom.ms20;                   // Index in 20 ms sliding window filter buffer

    if ( !(adc.filter_20ms.history_idx--) )                 // Index for 100 ms history of 20 ms filtered values
    {
        adc.filter_20ms.history_idx = (VRAW_SUM_HIST_LEN - 1);
    }

    old_vraw_sum_idx = (adc.filter_20ms.history_idx + adc.filter_20ms.history_offset);

    if ( old_vraw_sum_idx >= VRAW_SUM_HIST_LEN )
    {
        old_vraw_sum_idx -= VRAW_SUM_HIST_LEN;
    }

   // Loop for each channel (A and B)

    for ( c = 0, mask = 1; c < 2; c++, mask <<= 1 )     // For each channel (A & B)
    {
        adc_raw                  = adc_chan[c].adc_raw;
        last_adc_raw             = adc_chan[c].last_adc_raw;
        adc_chan[c].last_adc_raw = adc_raw;         // Save raw measurement

        // Signal status

        if ( dpcls->mcu.adc_mpx_flt & mask )            // If MPX fault is present
        {
            Set(adc_chan[c].st_meas, FGC_MEAS_MPX_INVALID); // Mark MPX as invalid
        }

        if ( dpcls->mcu.dcct_flt & mask )           // If DCCT status is bad
        {
            Set(adc_chan[c].st_dcct, FGC_DCCT_FAULT);       // Set DCCT fault status
        }
        else
        {
            Clr(adc_chan[c].st_dcct, FGC_DCCT_FAULT);       // Clear DCCT fault status
        }

        if ( cal.active->adc_factors[c].flags.fault )           // If ADC calibration fault is present
        {
            Clr(adc_chan[c].st_meas, FGC_MEAS_V_MEAS_OK);       // Mark channel voltage as invalid
        }

        if ( cal.active->adc_factors[c].flags.warning )
        {
            Set(adc_chan[c].st_meas, FGC_MEAS_CAL_FAILED);
        }
        else
        {
            Clr(adc_chan[c].st_meas, FGC_MEAS_CAL_FAILED);
        }

        if ( cal.active->dcct_factors[c].flags.fault )
        {
            Set(adc_chan[c].st_dcct, FGC_DCCT_CAL_FLT);
        }
        else
        {
            Clr(adc_chan[c].st_dcct, FGC_DCCT_CAL_FLT);
        }

        if (    !sim.active->enabled                                    // If not simulating,
             && Test(adc_chan[c].st_meas, FGC_MEAS_V_MEAS_OK)           // and no voltage error,
             && (dpcls->mcu.adc.filter_state != FGC_FILTER_STATE_READY) // no on-going reset of the ADC
             && (adc_raw == last_adc_raw)                               // and if the signal is stuck
           )
        {
            if ( !Test(adc_chan[c].st_meas, FGC_MEAS_SIGNAL_STUCK) )    // If channel not yet stuck
            {
                adc_chan[c].stuck_counter++;
                if ( adc_chan[c].stuck_counter > adc.stuck_limit )  // If no change for too long
                {
                    Set(adc_chan[c].st_meas, FGC_MEAS_SIGNAL_STUCK);    // Channel is stuck
                    Clr(adc_chan[c].st_meas, FGC_MEAS_V_MEAS_OK);   // Channel voltage is invalid
                }
            }
        }
        else
        {
            if ( Test(adc_chan[c].st_meas, FGC_MEAS_SIGNAL_STUCK) ) // If channel was stuck
            {
                adc_chan[c].stuck_counter--;
                if ( !(adc_chan[c].stuck_counter) )         // If count is zero
                {
                    Clr(adc_chan[c].st_meas, FGC_MEAS_SIGNAL_STUCK);    // Channel not stuck
                }
            }
            else                            // else channel not stuck
            {
                adc_chan[c].stuck_counter = 0;              // Reset counter
            }
        }


        // Filter 1 - 1ms sample direct from ADC register

        last_i_dcct = adc_chan[c].meas.i_dcct;        // Keep the last measurement in memory for SIGNAL_JUMP check

        CalMeas(adc_raw, c, sim.active->enabled, &adc_chan[c].meas);

        if ( adc_chan[c].meas.v_raw > adc_chan[c].max.v_raw )         // Record maximum raw voltage
        {
            adc_chan[c].max.v_raw = adc_chan[c].meas.v_raw;
        }

        if ( adc_chan[c].meas.v_raw < adc_chan[c].min.v_raw )         // Record minimum raw voltage
        {
            adc_chan[c].min.v_raw = adc_chan[c].meas.v_raw;
        }

        if ( adc_chan[c].meas.v_adc > adc_chan[c].max.v_adc )         // Record maximum ADC voltage
        {
            adc_chan[c].max.v_adc = adc_chan[c].meas.v_adc;
        }

        if ( adc_chan[c].meas.v_adc < adc_chan[c].min.v_adc )         // Record minimum ADC voltage
        {
            adc_chan[c].min.v_adc = adc_chan[c].meas.v_adc;
        }


        // Filter 2 - 20 ms sliding window filter with latency compensation

        vraw_sum_20ms = (adc_chan[c].filter_20ms.vraw_sum += (adc_raw - adc_chan[c].filter_20ms.vraw_buffer[flt_idx]));
        adc_chan[c].filter_20ms.vraw_buffer[flt_idx] = adc_raw;

        old_vraw_sum = adc_chan[c].filter_20ms.vraw_sum_history[old_vraw_sum_idx];
        adc_chan[c].filter_20ms.vraw_sum_history[adc.filter_20ms.history_idx] = vraw_sum_20ms;

        raw = ( vraw_sum_20ms +
                (int32_t)(adc.filter_20ms.time_ratio * (FP32)(vraw_sum_20ms - old_vraw_sum)) + 40 )
              / 20;

        raw = MAX(-FLT_CLIP, raw);
        raw = MIN( FLT_CLIP, raw);

        CalMeas(raw, c, sim.active->enabled, &adc_chan[c].ms20.cal);

        // Decrement the Imeas error downcounter

        if(adc_chan[c].ms20.err_downcounter)
        {
            adc_chan[c].ms20.err_downcounter--;
        }

        // Filter 3 - 200ms average

        // Using last_adc_raw instead of adc_raw because I_MEAS_OK flag is undefined for adc_raw at this point

        adc_chan[c].sum_raw_200ms += (FP32)(last_adc_raw - adc_chan[c].offset_raw_200ms);

        if(rtcom.start_of_ms200_f)                      // If 200 ms boundary, process average
        {
            raw = adc_chan[c].offset_raw_200ms + (int32_t)(0.005 * adc_chan[c].sum_raw_200ms + 0.499);

            CalMeas(raw, c, sim.active->enabled, &adc_chan[c].ms200.cal);

            adc_chan[c].ms200.err_downcounter = !(adc_chan[c].i_meas_ok_200ms);

            dpcls->dsp.adc.raw_200ms  [c] = adc_chan[c].ms200.cal.v_raw;
            dpcls->dsp.adc.volts_200ms[c] = ToIEEE(adc_chan[c].ms200.cal.v_adc);
            dpcls->dsp.adc.amps_200ms [c] = ToIEEE(adc_chan[c].ms200.cal.i_dcct);

            adc_chan[c].sum_raw_200ms    = 0;
            adc_chan[c].offset_raw_200ms = last_adc_raw;
            adc_chan[c].raw_pp_200ms     = (adc_chan[c].max.v_raw - adc_chan[c].min.v_raw);
            adc_chan[c].max.v_raw        = INT_MIN;     // Reset max value
            adc_chan[c].min.v_raw        = INT_MAX;     // Reset min value

            adc_chan[c].i_meas_ok_200ms  = true;
        }



        // Filter 4 - 1s average

        // Using last_adc_raw instead of adc_raw since I_MEAS_OK flag is undefined for adc_raw at this point

        adc_chan[c].sum_raw_1s += (FP32)(last_adc_raw - adc_chan[c].offset_raw_1s);

        if(rtcom.start_of_second_f)             // If 1s boundary, process average
        {
            raw = adc_chan[c].offset_raw_1s + (int32_t)(0.001 * adc_chan[c].sum_raw_1s + 0.499);

            CalMeas(raw, c, sim.active->enabled, &adc_chan[c].s.cal);

            adc_chan[c].s.err_downcounter = !(adc_chan[c].i_meas_ok_1s);

            dpcls->dsp.adc.raw_1s     [c] = adc_chan[c].s.cal.v_raw;
            dpcls->dsp.adc.volts_1s   [c] = ToIEEE(adc_chan[c].s.cal.v_adc);
            dpcls->dsp.adc.amps_1s    [c] = ToIEEE(adc_chan[c].s.cal.i_dcct);
            dpcls->dsp.adc.pp_volts_1s[c] = ToIEEE(adc_chan[c].max.v_adc - adc_chan[c].min.v_adc);

            adc_chan[c].sum_raw_1s     = 0;
            adc_chan[c].offset_raw_1s  = last_adc_raw;
            adc_chan[c].max.v_adc      = -1.0E10;     // Reset max value
            adc_chan[c].min.v_adc      =  1.0E10;     // Reset min value

            adc_chan[c].i_meas_ok_1s   = true;
        }


        // Signal jump check

        if (    !first_f
             && !cal.ongoing
                 && ( property.limits.i.err_fault[load.active->select] > 0.0 )
                 && ( dcct_select == FGC_DCCT_SELECT_AB )
             && (( adc_chan[c].st_meas & (FGC_MEAS_MPX_INVALID | FGC_MEAS_V_MEAS_OK)) == FGC_MEAS_V_MEAS_OK )
             && ( fabs(adc_chan[c].meas.i_dcct - last_i_dcct) > property.limits.i.err_fault[load.active->select] )
           )
        {
            Set(adc_chan[c].st_check, FGC_MEAS_SIGNAL_JUMP);
        }
    }

    // Ignore simultaneous signal jumps

    adc.i_dcct_match_f = (fabs(adc_chan[0].meas.i_dcct - adc_chan[1].meas.i_dcct) < property.limits.i.meas_diff);

    if (    adc.i_dcct_match_f                                              // If i_dccts match or
         || !( (adc_chan[0].st_check ^ adc_chan[1].st_check) & FGC_MEAS_SIGNAL_JUMP )   // signals jump together
       )
    {
        Clr(adc_chan[0].st_check, FGC_MEAS_SIGNAL_JUMP);                // Clear flags
        Clr(adc_chan[1].st_check, FGC_MEAS_SIGNAL_JUMP);
    }

    // Calculate Idiff on 1s boundaries if cal is not active

    if (rtcom.start_of_second_f)
    {
        if (   cal.ongoing
            || adc_chan[0].s.err_downcounter
            || adc_chan[1].s.err_downcounter
           )
        {
            meas.i_diff_1s = 0;
        }
        else
        {
            // Channel B - Channel A

            meas.i_diff_1s = adc_chan[1].s.cal.i_dcct - adc_chan[0].s.cal.i_dcct;
        }
    }

    first_f = false;            // Flag indicating if this is the first time this function is run.
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcMpx(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called if the ADC multiplexer or SD filters are changed by the MCU.  It will extract
  the correct shift for the ADC values (used only for SD filters values, not for the SAR interface).  The
  function AdcRead uses this shift value.
\*---------------------------------------------------------------------------------------------------------*/
{
    adc_chan[0].shift = dpcls->mcu.sd_shift[0];
    adc_chan[1].shift = dpcls->mcu.sd_shift[1];

    CalCalc();                                  // Recalculate calibrations immediately

    dpcls->mcu.adc_mpx_f = 0;                   // Clear request
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcFifo(void)
/*---------------------------------------------------------------------------------------------------------*\
  This records a sample in the ADC fifo. It is called every 200ms.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  in_idx;

    in_idx = dpcls->dsp.fifo.adc.in_idx;

    dpcls->dsp.fifo.adc_time [in_idx] = rtcom.time_now_us;
    dpcls->dsp.fifo.adc_raw_a[in_idx] = dpcls->dsp.adc.raw_200ms[0];
    dpcls->dsp.fifo.adc_raw_b[in_idx] = dpcls->dsp.adc.raw_200ms[1];
    dpcls->dsp.fifo.adc_pp_a [in_idx] = adc_chan[0].raw_pp_200ms;
    dpcls->dsp.fifo.adc_pp_b [in_idx] = adc_chan[1].raw_pp_200ms;

    dpcls->dsp.fifo.adc.in_idx = (in_idx + 1) & (FGC_ADC_FIFO_LEN - 1);
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcGatePars(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will calculate DSP's reset_gate_f for the ADC filters. If false, that gate will prevent
  all ADC resets, except if manually triggered with S ADC RESET or S ADC.FILTER RESET.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(reg.next->state)
    {
        case FGC_REG_I:

#if (FGC_CLASS_ID == 53)
        case FGC_REG_B:
#endif
            dpcls->dsp.adc.reset_gate_f = (DSP_ROUND_TIME_MS(reg.next->rst_pars.period) < ADC_REG_MIN_PERIOD_MS ? false : true);
            break;

        case FGC_REG_V:

            dpcls->dsp.adc.reset_gate_f = true;
            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: adc.c
\*---------------------------------------------------------------------------------------------------------*/
