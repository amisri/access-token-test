*---------------------------------------------------------------------------------------------------------
* File:     conv.asm
*
* Purpose:  This file includes the assembler conversion functions: FromIEEE() and ToIEEE()
*
* Notes:    Taken from C32 documentation.  These functions are required because the C32 uses a
*       different floating point format to the HC16.  The HC16 uses the standard format
*       defined by the IEEE 748
*---------------------------------------------------------------------------------------------------------

        .global _FromIEEE
        .global _ToIEEE

        .data
table   .word   0FF800000h
        .word   0FF000000h
        .word   07F000000h
        .word   080000000h
        .word   081000000h
t_adr   .word   table
        .text

*---------------------------------------------------------------------------------------------------------
*   double FromIEEE(unsigned int ieee32_float)
*
*   double = float = 32 bits
*
*   with our standard types
*
*   FP32 FromIEEE(DP_IEEE_FP32 ieee32_float)
*---------------------------------------------------------------------------------------------------------
* Register AR2 contains ieee32_float

_FromIEEE:
        LDI     @t_adr,AR1      ;Point to start of mask table
        AND3    AR2,*AR1,R1     ;replace fraction with zero
        BND     neg1            ;Test sign & branch if Negative (delayed)
        ADDI    AR2,R1          ;Shift sign & exponent, inserting 0
        LDIZ    *+AR1(1),R1     ;If all 0, generate C32 0
        SUBI    *+AR1(2),R1     ;Unbias exponent
        PUSH    R1
        POPF    R0              ;Load this as floating point number
        RETS

neg1    PUSH    R1
        POPF    R0              ;Load this as floating point number
        NEGF    R0,R0           ;Negate if original sign is negative
        RETS                    ;Return TI float in R0

*---------------------------------------------------------------------------------------------------------
*   unsigned int ToIEEE(double ti_double)
*
*   double = float = 32 bits
*
*   with our standard types
*
*   DP_IEEE_FP32 ToIEEE(FP32 ti_float)
*---------------------------------------------------------------------------------------------------------
* Register R2 contains ti_double

_ToIEEE:
        LDI     @t_adr,AR1      ;Point to start of mask table
        LDF     R2,R2           ;Determine the sign of the number
        LDFZ    *+AR1(4),R2     ;If 0, load appropriate number
        BND     neg2            ;Test sign & branch if negative (delayed)
        ABSF    R2              ;Take the absolute value of the number
        LSH     1,R2            ;Eliminate the sign bit in R2
        PUSHF   R2
        POP     R0              ;Place number in lower 32 bits of R0
        ADDI    *+AR1(2),R0     ;Add exponent bias (127)
        LSH     -1,R0           ;Add the positive sign
        RETS

neg2    POP     R0              ;Place number in lower 32 bits of R0
        ADDI    *+AR1(2),R0     ;Add exponent bias (127)
        LSH     -1,R0           ;Make place for the sign
        ADDI    *+AR1(3),R0     ;Add the negative sign
        RETS                    ;Return IEEE value in R0

*---------------------------------------------------------------------------------------------------------
*   End of file: conv.asm
*---------------------------------------------------------------------------------------------------------

