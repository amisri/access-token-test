/*!
 * @file  read_psu_adc.c
 */

#define READ_PSU_ADC_GLOBALS

#include <read_PSU_adc.h>
#include <memmap_mcu.h>
#include <definfo.h>
#include <rtcom.h>
#include <dpcom.h>
#include <diag.h>
#if (FGC_CLASS_ID == 51)
#include <props.h>
#endif // (FGC_CLASS_ID == 51)
#include <dsp_dependent.h>

#if (FGC_CLASS_ID == 59)
// Missing from defconsts.h on class 59

#define FGC_LAT_ANA_FLT          128
#endif // (FGC_CLASS_ID == 59)



#if (FGC_CLASS_ID == 51)
/*!
 * Utility function called to log a PSU voltage value
 *
 * The value submitted must be in IEEE format.
 */
static void DiagPsuVoltagesLog(bool log_f, struct diag_psulog * psulog, DP_IEEE_FP32 value)
{
    if(log_f)
    {
        psulog->log_ieee[psulog->idx] = value;

        psulog->idx++;

        if(psulog->idx >= FGC_PSU_LOG_LEN)
        {
            psulog->idx = 0;
        }
    }
}
#endif // (FGC_CLASS_ID == 51)



void VerifyPsuVoltages(void)
{
    FP32         r15p;
    FP32         r15n;
    DP_IEEE_FP32 psu_ieee[3];        // PSU voltages (+5V, +15V, -15V) converted to IEEE format
    bool      is_out_of_limits = false;

    // FGC Trivolt PSU voltages

    // +5V PSU
    // register analog_2 of DIM board, flat[ 0] = A[0]

    psu.v5 = 2.4414E-3 * (FP32)(qspi_misc.analog_regs_from_all_dim_boards[2][0x00] & 0xFFF);

    // +15V PSU
    // register analog_0 of DIM board, flat[ 0] = A[0]

    r15p = (FP32)(qspi_misc.analog_regs_from_all_dim_boards[0][0x00] & 0xFFF);
    psu.vp15 = 7.3120E-3 * r15p;

    // -15V PSU
    // register analog_1 of DIM board, flat[16] = B[0]

    r15n = (FP32)(qspi_misc.analog_regs_from_all_dim_boards[1][0x10] & 0xFFF);
    psu.vm15 = 2.9282E-3 * r15n - 1.02280E-2 * r15p;

#if (FGC_CLASS_ID == 51)
    if((psu.v5 < FGC_PSU_MIN_5V) ||
       (psu.v5 > FGC_PSU_MAX_5V))
    {
        property.fgc.debug.psu_5v.out_of_limits++;
        is_out_of_limits = true;
    }

    if((psu.vp15 < FGC_PSU_MIN_15V) ||
       (psu.vp15 > FGC_PSU_MAX_15V))
    {
        property.fgc.debug.psu_pos_15v.out_of_limits++;
        is_out_of_limits = true;
    }

    if((psu.vm15 < -FGC_PSU_MAX_15V) ||
       (psu.vm15 > -FGC_PSU_MIN_15V))
    {
        property.fgc.debug.psu_neg_15v.out_of_limits++;
        is_out_of_limits = true;
    }
#else // (FGC_CLASS_ID != 51)
    if((psu.v5   <   FGC_PSU_MIN_5V)   ||
       (psu.v5   >   FGC_PSU_MAX_5V)   ||
       (psu.vp15 <   FGC_PSU_MIN_15V)  ||
       (psu.vp15 >   FGC_PSU_MAX_15V)  ||
       (psu.vm15 < (-FGC_PSU_MAX_15V)) ||
       (psu.vm15 > (-FGC_PSU_MIN_15V)))
    {
        is_out_of_limits = true;
    }
#endif // (FGC_CLASS_ID == 51)

    if(is_out_of_limits)
    {
        // If 50 consecutive OUT_OF_LIMITS errors

        psu.fails_counter++;

        if(psu.fails_counter > 50)
        {
            rtcom.st_latched |= FGC_LAT_PSU_V_FAIL;

#if (FGC_CLASS_ID == 51)
            // Freeze debug circular buffers

            property.fgc.debug.psu_5v.log_state      = FGC_LOG_FROZEN;
            property.fgc.debug.psu_pos_15v.log_state = FGC_LOG_FROZEN;
            property.fgc.debug.psu_neg_15v.log_state = FGC_LOG_FROZEN;
#endif // (FGC_CLASS_ID == 51)
        }
    }
    else
    {
        psu.fails_counter = 0;
    }

    // Convert to IEEE format

    psu_ieee[0] = ToIEEE(psu.v5);
    psu_ieee[1] = ToIEEE(psu.vp15);
    psu_ieee[2] = ToIEEE(psu.vm15);

#if (FGC_CLASS_ID == 51)
    // Log values in properties FGC.DEBUG.PSU_5V.LOG, FGC.DEBUG.PSU_NEG_15V.LOG, FGC.DEBUG.PSU_POS_15V.LOG

    DiagPsuVoltagesLog((property.fgc.debug.psu_5v.log_state      != FGC_LOG_FROZEN),
                       &property.fgc.debug.psu_5v.log,
                        psu_ieee[0]);
    DiagPsuVoltagesLog((property.fgc.debug.psu_pos_15v.log_state != FGC_LOG_FROZEN),
                       &property.fgc.debug.psu_pos_15v.log,
                        psu_ieee[1]);
    DiagPsuVoltagesLog((property.fgc.debug.psu_neg_15v.log_state != FGC_LOG_FROZEN),
                       &property.fgc.debug.psu_neg_15v.log,
                        psu_ieee[2]);
#endif // (FGC_CLASS_ID == 51)

    // Report values to the MCU at 5Hz

    if (rtcom.ms200 == 2)
    {
        dpcom->dsp.psu.fgc[0] = psu_ieee[0];
        dpcom->dsp.psu.fgc[1] = psu_ieee[1];
        dpcom->dsp.psu.fgc[2] = psu_ieee[2];
    }

    // FGC Analogue interface PSU voltages - if present in the FGC
    // If Slot 5 contains an analogue interface

    if(rtcom.interfacetype != FGC_INTERFACE_RFC_500)
    {
        // register analog_3 of DIM board, flat[ 0] = A[0]

        psu.v5 = 2.5659E-3 * (FP32)(qspi_misc.analog_regs_from_all_dim_boards[3][0x00] & 0xFFF);

        // register analog_2 of DIM board, flat[16] = B[0]

        r15p = (FP32)(qspi_misc.analog_regs_from_all_dim_boards[2][0x10] & 0xFFF);
        psu.vp15 = 7.5860E-3 * r15p + 2.4952E-5 * r15n;

        // register analog_3 of DIM board, flat[16] = B[0]

        r15n = (FP32)(qspi_misc.analog_regs_from_all_dim_boards[3][0x10] & 0xFFF);
        psu.vm15 = 2.9532E-3 * r15n - 1.0377E-2 * r15p;

        if((psu.v5   <   FGC_PSU_MIN_5V)   ||
           (psu.v5   >   FGC_PSU_MAX_5V)   ||
           (psu.vp15 <   FGC_PSU_MIN_15V)  ||
           (psu.vp15 >   FGC_PSU_MAX_15V)  ||
           (psu.vm15 < (-FGC_PSU_MAX_15V)) ||
           (psu.vm15 > (-FGC_PSU_MIN_15V)))
        {
            psu.fails_counter_ana_interfaz++;

            if(psu.fails_counter_ana_interfaz > 50)
            {
                rtcom.st_latched |= FGC_LAT_ANA_FLT;
            }
        }
        else
        {
            psu.fails_counter_ana_interfaz = 0;
        }

        // Report values to the MCU at 5Hz

        if(rtcom.ms200 == 2)
        {
            dpcom->dsp.psu.ana[0] = ToIEEE(psu.v5);
            dpcom->dsp.psu.ana[1] = ToIEEE(psu.vp15);
            dpcom->dsp.psu.ana[2] = ToIEEE(psu.vm15);
        }
    }
}

// EOF
