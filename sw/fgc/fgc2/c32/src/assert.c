/*!
 * @file  assert.c
 */

#include <string.h>

#include <assert.h>
#include <dpcom.h>
#include <lib.h>
#include <macros.h>
#include <dsp_panic.h>



void assertFunc(const char * filename, const int line)
{
    const size_t max_string_length = ASSERT_FILENAME_BUF_SIZE_MCU - 1;

    char tmp_filename[ASSERT_FILENAME_BUF_SIZE_MCU];

    // Copy at most 15 characters of the filename and terminate the string with null

    strncpy(tmp_filename, filename, max_string_length)[max_string_length] = '\0';

    ToMCUString(dpcom->dsp.assert_file, tmp_filename, max_string_length);

    dpcom->dsp.assert_line = line;

    DspPanic(DSP_PANIC_ASSERT);
}

// EOF
