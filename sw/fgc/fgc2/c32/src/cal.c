/*---------------------------------------------------------------------------------------------------------*\
  File:         cal.c

  Purpose:      FGC C32 Software - contains analogue calibration functions

  Author:       Quentin.King@cern.ch
\*---------------------------------------------------------------------------------------------------------*/

#define CAL_GLOBALS

#include <cal.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dsp_dependent.h>      // for ToIEEE(), FromIEEE()
#include <rtcom.h>              // for rtcom global variable
#include <stdlib.h>             // for NULL
#include <adc.h>                // for FLT_MAX_EXT, struct filtered_cal_current
#include <macros.h>             // for Set(), Clr(), Test()
#include <string.h>             // for memset()
#include <pars.h>               // for ParsCalDac()
#include <reg.h>                // for reg global variables
#include <sim.h>                // for sim global variable
#include <math.h>               // for fabs()
#include <main.h>               // for DAC_RESOLUTION
#include <defprops_dsp_fgc.h>   // for ParsCalDac()

/*!
 * DAC calibration limits
 */
static const FP32 dac_high[CAL_NUM_ERRS] = {  0.1, 8.9, -7.5 };

/*!
 * DAC calibration limits
 */
static const FP32 dac_low [CAL_NUM_ERRS] = { -0.1, 7.5, -8.9 };

/*!
 * ADC calibration limits
 */
static const struct cal_limits adc_cal_limits[4] =
        //         OFFSET          GAIN POS/NEG
        //         WARN FAULT      WARN FAULT
        //  MID    +/-     +/-     +/-     +/-
{
        {   20.0,  100.0,  200.0,  100.0,  200.0 },     // 0 : SD22
        {    0.0, 1000.0, 2000.0,  500.0, 1000.0 },     // 1 : SAR_400
        {  280.0,  500.0, 1000.0,  500.0, 1000.0 },     // 2 : SD_350/351
};

/*!
 * DCCT calibration limits
 */
static const struct cal_limits dcct_cal_limits =
        //         OFFSET          GAIN POS/NEG
        //         WARN FAULT      WARN FAULT
        //  MID    +/-     +/-     +/-     +/-

        {    0.0, 5000.0, 10000.0, 5000.0, 10000.0 };

/*---------------------------------------------------------------------------------------------------------*/
void CalInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function set initial values for the calibration global variables
\*---------------------------------------------------------------------------------------------------------*/
{
    // Calculate DAC raw value range

    cal.active->dac_factors.min_dac_raw = -(1 << (DAC_RESOLUTION-1));
    cal.active->dac_factors.max_dac_raw =  (1 << (DAC_RESOLUTION-1)) - 1;

    // No ongoing DAC calibration

    cal.ongoing_dac_cal = false;
}
/*---------------------------------------------------------------------------------------------------------*/
void CalMeas(int32_t v_raw_int, uint32_t channel_idx, bool sim_flag, struct cal_current *meas)
/*---------------------------------------------------------------------------------------------------------*\
  If the sim_flag is not set, this function uses the real-time ADC/DCCT calibration factors calculated
  by CalCalc to translate the ADC raw input (Vraw) into a calibrated current measurement (Idcct):

        sim_flag == false:    Vraw -> Vadc -> Vdcct -> Idcct

  If the sim_flag is set, this function does the reverse operation (simulated current or voltage gives Vraw)

        sim_flag == true:     Isim -> Vdcct -> Vadc -> Vraw

\*---------------------------------------------------------------------------------------------------------*/
{
    if(!rtcom.time_now_us.unix_time)    // First second of operation (no cal yet)
    {
        meas->v_raw  = v_raw_int;
        meas->v_adc  = 0.0;
        meas->i_dcct = 0.0;
    }
    else
    {
        // Translate  Vraw -> Vadc -> Vdcct -> Idcct (in normal mode)
        //        or  Isim -> Vdcct -> Vadc -> Vraw  (in simulation mode)

        calCurrent(&cal.active->dcct_factors[channel_idx],  // DCCT calibration factors
                   &cal.active->adc_factors[channel_idx],   // ADC calibration factors
                   v_raw_int,                               // ADC raw value
                   sim.active->i,                           // Simulated DCCT current (used if sim_flag is true)
                   sim_flag,                                // Simulation flag
                   meas);                                   // Returned current values

        // Check the DCCT cal status

        if(sim_flag == false &&
           Test(adc_chan[channel_idx].st_dcct, FGC_DCCT_CAL_FLT))
        {
            meas->i_dcct = 0.0;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CalCalc()
/*---------------------------------------------------------------------------------------------------------*\
  [FGC2] This function is called from IsrMst() at 1Hz. It is also called from RtAdcMpx() if the multiplexers
  have changed position.
  It recalculates the analogue input calibrations based on the multiplexer settings at the time.

  On FGC2, this function is always called in ISR context.
\*---------------------------------------------------------------------------------------------------------*/
{
    int32_t                  gain;
    uint32_t                  channel_idx;        // Channel (A or B)
    int32_t                  adc_filter_mpx;     // Copy of ADC.FILTER_MPX[channel_idx]
    uint32_t                  mpx_idx;            // Channel multiplexing (A or B)
    uint32_t                  dcct_mpx;           // DCCT connected to the channel: a value different
                                                // from FGC_INTERNAL_ADC_MPX_DCCTA/B means no DCCT

    // Calibration event structure with no error used to calculate the ADC calibration factors for
    // digital channels and the DCCT calibration factors for channels not connected to a DCCT

    const struct cal_event  cal_event_no_err = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

    // Loop on channels A and B

    for(channel_idx = 0; channel_idx < 2; channel_idx++)
    {
        // --- Recalculate the ADC calibration factors ---

        adc_filter_mpx = dpcls->mcu.adc_filter_mpx[channel_idx];        // Switch on adc_mpx = ADC.FILTER_MPX[channel_idx]

        switch(adc_filter_mpx)
        {
        case FGC_ADC_FILTER_MPX_ADC16A:                // Internal ADCs
        case FGC_ADC_FILTER_MPX_ADC16B:

            // Usually, mpx_idx == channel_idx, but channels inversions are possible.
            // For example if channel A is connected to ADC16B then channel_idx = 0 and mpx_idx = 1

            mpx_idx = ((adc_filter_mpx == FGC_ADC_FILTER_MPX_ADC16A) ? 0 : 1);

            // Copy err from shared memory

            cal.normalized_adc16_factors[mpx_idx].offset_ppm       = FromIEEE( dpcls->mcu.cal.adc.internal[mpx_idx].err[0] );
            cal.normalized_adc16_factors[mpx_idx].gain_err_pos_ppm = FromIEEE( dpcls->mcu.cal.adc.internal[mpx_idx].err[1] );
            cal.normalized_adc16_factors[mpx_idx].gain_err_neg_ppm = FromIEEE( dpcls->mcu.cal.adc.internal[mpx_idx].err[2] );

            // Note: the calibration flags (cal.active->adc_factors[].flags) are checked in AdcFilter

            calAdcFactors(property.cal.adc.internal[mpx_idx].gain,
                          &cal.normalized_adc16_factors[mpx_idx],
                          property.adc.temperature,
                          property.cal.adc.internal[mpx_idx].tc,
                          property.cal.adc.internal[mpx_idx].dtc,
                          &adc_cal_limits[adc.type],
                          &cal.active->adc_factors[channel_idx]);

            // DCCT selector: it depends on the internal ADC multiplexing (ADC.ADC16.MPX).
            //                it is ignored if != FGC_INTERNAL_ADC_MPX_DCCTA/B

            dcct_mpx = dpcls->mcu.internal_adc_mpx[mpx_idx];

            break;

        case FGC_ADC_FILTER_MPX_ADC22A:                // External ADCs
        case FGC_ADC_FILTER_MPX_ADC22B:

            // Usually, mpx_idx == channel_idx, but channel inversions are possible.
            // For example if channel A is connected to ADC22B then channel_idx = 0 and mpx_idx = 1

            mpx_idx = ((adc_filter_mpx == FGC_ADC_FILTER_MPX_ADC22A) ? 0 : 1);

            // Copy err from shared memory

            cal.normalized_adc22_factors[mpx_idx].offset_ppm       = FromIEEE( dpcls->mcu.cal.adc.external[mpx_idx].err[0] );
            cal.normalized_adc22_factors[mpx_idx].gain_err_pos_ppm = FromIEEE( dpcls->mcu.cal.adc.external[mpx_idx].err[1] );
            cal.normalized_adc22_factors[mpx_idx].gain_err_neg_ppm = FromIEEE( dpcls->mcu.cal.adc.external[mpx_idx].err[2] );

            // No temperature compensation and no limits check on the calibration coefficients

            calAdcFactors(property.cal.adc.external[mpx_idx].gain,
                          &cal.normalized_adc22_factors[mpx_idx],
                          CAL_T0,
                          NULL,
                          NULL,
                          &adc_cal_limits[ADC_INTERFACE_SD_22],
                          &cal.active->adc_factors[channel_idx]);

            // DCCT selector: ADC22A is always connected to DCCTA, ADC22B is always connected to DCCTB

            dcct_mpx = ((adc_filter_mpx == FGC_ADC_FILTER_MPX_ADC22A) ? FGC_INTERNAL_ADC_MPX_DCCTA : FGC_INTERNAL_ADC_MPX_DCCTB);

            break;

        default:                                // All digital calibrations

            // gain = FLT_MAX_INT (100% value for internal ADC) if adc_mpx is equal to:
            //
            //    FGC_ADC_FILTER_MPX_TST_FRAME_ERR, FGC_ADC_FILTER_MPX_CAL_0, FGC_ADC_FILTER_MPX_CAL_12,
            //    FGC_ADC_FILTER_MPX_CAL_50,  FGC_ADC_FILTER_MPX_CAL_87, FGC_ADC_FILTER_MPX_CAL_100
            //
            // gain = FLT_MAX_EXT (100% value for external ADC) if adc_mpx is equal to:
            //
            //    FGC_ADC_FILTER_MPX_TST_NO_FRAME, FGC_ADC_FILTER_MPX_CAL_0_1MHZ, FGC_ADC_FILTER_MPX_CAL_12_1MHZ,
            //    FGC_ADC_FILTER_MPX_CAL_50_1MHZ, FGC_ADC_FILTER_MPX_CAL_100_1MHZ, FGC_ADC_FILTER_MPX_CAL_87_1MHZ

            gain = (adc_filter_mpx <= FGC_ADC_FILTER_MPX_CAL_100 ? FLT_MAX_INT : FLT_MAX_EXT);

            calAdcFactors(gain,
                          &cal_event_no_err,
                          CAL_T0,
                          NULL,
                          NULL,
                          NULL,
                          &cal.active->adc_factors[channel_idx]);

            // DCCT selector is not relevant in this case, so take any value != from FGC_INTERNAL_ADC_MPX_DCCTA/B

            dcct_mpx = FGC_INTERNAL_ADC_MPX_AUX;

            break;
        }

        // --- Recalculate the DCCT factors ---

        // Note: the calibration flags (cal.next->dcct_factors[].flags) are checked in AdcFilter

        switch(dcct_mpx)                                // Switch on DCCT value
        {
        case FGC_INTERNAL_ADC_MPX_DCCTA:                        // DCCT A

            // Copy err from shared memory

            cal.normalized_dcct_factors[0].offset_ppm       = FromIEEE( dpcls->mcu.cal.dcct[0].err[0] );
            cal.normalized_dcct_factors[0].gain_err_pos_ppm = FromIEEE( dpcls->mcu.cal.dcct[0].err[1] );
            cal.normalized_dcct_factors[0].gain_err_neg_ppm = FromIEEE( dpcls->mcu.cal.dcct[0].err[2] );

            // Update DCCT calibration

            calDcctFactors(property.dcct.gain[0],
                           property.dcct.primary_turns,
                           (dpcls->mcu.cal.active ? 0.0 : property.cal.dcct[0].headerr),
                           &cal.normalized_dcct_factors[0],
                           property.dcct.temperature[0],
                           property.cal.dcct[0].tc,
                           property.cal.dcct[0].dtc,
                           &dcct_cal_limits,
                           &cal.active->dcct_factors[channel_idx]);

            break;

        case FGC_INTERNAL_ADC_MPX_DCCTB:                        // DCCT B

            // Copy err from shared memory

            cal.normalized_dcct_factors[1].offset_ppm       = FromIEEE( dpcls->mcu.cal.dcct[1].err[0] );
            cal.normalized_dcct_factors[1].gain_err_pos_ppm = FromIEEE( dpcls->mcu.cal.dcct[1].err[1] );
            cal.normalized_dcct_factors[1].gain_err_neg_ppm = FromIEEE( dpcls->mcu.cal.dcct[1].err[2] );

            // Update DCCT calibration

            calDcctFactors(property.dcct.gain[1],
                           property.dcct.primary_turns,
                           (dpcls->mcu.cal.active ? 0.0 : property.cal.dcct[1].headerr),
                           &cal.normalized_dcct_factors[1],
                           property.dcct.temperature[1],
                           property.cal.dcct[1].tc,
                           property.cal.dcct[1].dtc,
                           &dcct_cal_limits,
                           &cal.active->dcct_factors[channel_idx]);

            break;

        default:                                        // AUX A/B, DAC, VREF CAL or DIG CAL

            // Set up a virtual DCCT with gain 1

            calDcctFactors(1.0,                 // Gain
                           1,                   // Primary_turns
                           0.0,                 // Head_err_ppm
                           &cal_event_no_err,
                           CAL_T0,
                           NULL,
                           NULL,
                           NULL,
                           &cal.active->dcct_factors[channel_idx]);

            break;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
bool CalAuto(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from IsrMst() on millisecond 2 of every 200ms period if an auto-calibration
  request is active (req_f && !cal_complete_f).  The request is defined by the MCU in the DPRAM via the
  following  variables:

    dpcls->mcu.cal.req_f                Set by MCU start calibration
    dpcls->mcu.cal.action               CAL_REQ_BOTH_ADC16S|CAL_REG_ADC16|CAL_REQ_ADC22|CAL_REQ_DCCT|CAL_REQ_DAC
    dpcls->mcu.cal.chan_mask            Channel mask: 1=IA 2=IB 3=IA & IB
    dpcls->mcu.cal.ave_steps            Number of 200ms averaging steps for the measurement (max 50)
    dpcls->mcu.cal.idx                  0=Offset, 1=GainPos, 2=GainNeg or ADC16 measurement number (0-4)
    dpcls->mcu.cal.t_adc                ADC16 temperatures (raw units)
    dpcls->mcu.cal.t_dcct[2]            DCCT electronics temperatures (raw units)

  The results are posted by the C32:

    dpcls->dsp.cal_complete_f           Set by C32, cleared by MCU
    dpcls->mcu.cal.dcct[2].err[6]       DCCT errors (ppm), date and time (temp added by MCU)
    dpcls->mcu.cal.adc.internal[2].err[6]      ADC16 errors (ppm), date and time (temp added by MCU)
    dpcls->mcu.cal.adc.external[2].err[6]      ADC22 errors (ppm), date and time (temp added by MCU)
    property.cal.dac[0][3]              DAC calibrations (volts)

  The sequence is as follows:

        1. The MCU prepares the action, adc_type, adc_chan and idx (as required) and then sets req_f
        2. The C32 starts processing
        3. When complete the C32 clears req_f and sets cal_complete_f
        4. The MCU clears the action and the cal_complete_f

  The trigger condition for the C32 to start processing is (req_f && !cal_complete_f).
  The trigger condition for the MCU to conclude the processing is (cal_complete_f).

  This function returns true if the on-going calibration process has completed, false otherwise.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t idx = dpcls->mcu.cal.idx;
    bool last_sample_f;
    bool internal_adc_f;

    internal_adc_f = (dpcls->mcu.cal.action == CAL_REQ_BOTH_ADC16S || dpcls->mcu.cal.action == CAL_REQ_ADC16);

    // --- DAC calibration ---

    if(dpcls->mcu.cal.action == CAL_REQ_DAC)
    {
        return(CalAutoDac());
    }

    // --- ADC/DCCT calibration ---

    if(!cal.step_200ms++)       // If starting the calibration
    {
        // Reset the v_raw average structures

        calAverageVraw(&cal.average_v_raw[0], dpcls->mcu.cal.ave_steps, 0);
        calAverageVraw(&cal.average_v_raw[1], dpcls->mcu.cal.ave_steps, 0);

        if(internal_adc_f && idx == 0)                               // If internal ADC calibration and first measurement (OFFSET)
        {
            memset(&property.cal.adc.internal[0].vraw[0],0,CAL_BOTH_ADC16S_N_MEAS);     // Clear vraw averages but don't
            memset(&property.cal.adc.internal[1].vraw[0],0,CAL_BOTH_ADC16S_N_MEAS);     // touch ADC16 compensation values
        }

        return false;
    }

    // Store the 200ms raw average for both channels and check for last 200ms period
    // NB: Since the sample sizes of both channels are equal, the calls to calAverageVraw
    //     will return 0 simultaneously at the end of the sampling.

    last_sample_f  = !calAverageVraw(&cal.average_v_raw[0], 0, adc_chan[0].ms200.cal.v_raw);
    last_sample_f |= !calAverageVraw(&cal.average_v_raw[1], 0, adc_chan[1].ms200.cal.v_raw);

    if(last_sample_f)
    {
        if(internal_adc_f)                               // If internal ADC calibration
        {
            property.cal.adc.internal[0].vraw[idx] = cal.average_v_raw[0].v_raw_ave;
            property.cal.adc.internal[1].vraw[idx] = cal.average_v_raw[1].v_raw_ave;
        }
        else                                             // If external ADC or DCCT calibration
        {
            cal.vraw[0][idx] = cal.average_v_raw[0].v_raw_ave;
            cal.vraw[1][idx] = cal.average_v_raw[1].v_raw_ave;
        }

        switch(dpcls->mcu.cal.action)
        {
        case CAL_REQ_BOTH_ADC16S:

            if(idx >= (CAL_BOTH_ADC16S_N_MEAS-1))
            {
                CalAutoBothAdc16s();
            }
            break;

        case CAL_REQ_ADC16:

            CalAutoInternalAdc((enum cal_idx)idx);
            break;

        case CAL_REQ_ADC22:

            CalAutoExternalAdc((enum cal_idx)idx);
            break;

        case CAL_REQ_DCCT:

            CalAutoDcct((enum cal_idx)idx);
            break;
        }

        // Calibration process completed

        return true;
    }

    return false;
}
/*---------------------------------------------------------------------------------------------------------*/
void CalAutoBothAdc16s(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when calibrating the all errors for both internal 16-bit ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      adc_idx;
    int32_t      vraw[FGC_N_ADCS][CAL_NUM_ERRS];

    // --- Calculate calibration compensations ---

    // NB: The measured and calculated Vraw values are accessible with properties CAL.A/B.VRAW
    //
    // Reminder:
    //                                                       Channel A       Channel B
    //
    // property.cal.adc.internal[channel_idx].vraw[0]:        CALZERO         CALZERO
    // property.cal.adc.internal[channel_idx].vraw[1]:        CALPOS          CALNEG
    // property.cal.adc.internal[channel_idx].vraw[2]:        CALPOS          CALPOS
    // property.cal.adc.internal[channel_idx].vraw[3]:        CALNEG          CALPOS
    // property.cal.adc.internal[channel_idx].vraw[4]:        CALNEG          CALNEG
    // property.cal.adc.internal[channel_idx].vraw[5]:  Calculated delta between the other channel's POS/NEG and POS/POS
    // property.cal.adc.internal[channel_idx].vraw[6]:  Calculated delta between the other channel's NEG/POS and NEG/NEG

    property.cal.adc.internal[0].vraw[5] = property.cal.adc.internal[1].vraw[3] - property.cal.adc.internal[1].vraw[2];        // Chan A pos
    property.cal.adc.internal[0].vraw[6] = property.cal.adc.internal[1].vraw[1] - property.cal.adc.internal[1].vraw[4];        // Chan A neg
    property.cal.adc.internal[1].vraw[5] = property.cal.adc.internal[0].vraw[1] - property.cal.adc.internal[0].vraw[2];        // Chan B pos
    property.cal.adc.internal[1].vraw[6] = property.cal.adc.internal[0].vraw[3] - property.cal.adc.internal[0].vraw[4];        // Chan B neg

    // --- Calculate compensated raw values ---

    vraw[0][CAL_OFFSET_V]     = property.cal.adc.internal[0].vraw[0];
    vraw[0][CAL_GAIN_ERR_POS] = property.cal.adc.internal[0].vraw[1] + property.cal.adc.internal[0].vraw[5];
    vraw[0][CAL_GAIN_ERR_NEG] = property.cal.adc.internal[0].vraw[3] + property.cal.adc.internal[0].vraw[6];

    vraw[1][CAL_OFFSET_V]     = property.cal.adc.internal[1].vraw[0];
    vraw[1][CAL_GAIN_ERR_POS] = property.cal.adc.internal[1].vraw[3] + property.cal.adc.internal[1].vraw[5];
    vraw[1][CAL_GAIN_ERR_NEG] = property.cal.adc.internal[1].vraw[1] + property.cal.adc.internal[1].vraw[6];

    // --- Calculate ADC calibration errors normalised to T0 (23C) for both channels

    for(adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
    {
        calAdcErrors(&vraw[adc_idx][0],
                     property.cal.adc.internal[adc_idx].gain,
                     property.adc.temperature,
                     property.cal.adc.internal[adc_idx].tc,
                     property.cal.adc.internal[adc_idx].dtc,
                     property.cal.vref.err,
                     &cal.normalized_adc16_factors[adc_idx]);

        calEventStamp(&cal.normalized_adc16_factors[adc_idx],
                      rtcom.time_now_us.unix_time,
                      property.adc.temperature);
    }

    // --- Convert the calibration factors for MCU property CAL.#.ADC16.ERR ---

    for(adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)     // Loop on all ADCs
    {
        dpcls->mcu.cal.adc.internal[adc_idx].err[0] = ToIEEE( cal.normalized_adc16_factors[adc_idx].offset_ppm );
        dpcls->mcu.cal.adc.internal[adc_idx].err[1] = ToIEEE( cal.normalized_adc16_factors[adc_idx].gain_err_pos_ppm );
        dpcls->mcu.cal.adc.internal[adc_idx].err[2] = ToIEEE( cal.normalized_adc16_factors[adc_idx].gain_err_neg_ppm );
     // dpcls->mcu.cal.adc.internal[adc_idx].err[3] (the temperature) is set by the MCU
        dpcls->mcu.cal.adc.internal[adc_idx].err[4] = ToIEEE( cal.normalized_adc16_factors[adc_idx].date_days );
        dpcls->mcu.cal.adc.internal[adc_idx].err[5] = ToIEEE( cal.normalized_adc16_factors[adc_idx].time_s );
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CalAutoInternalAdc(enum cal_idx cal_signal)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when calibrating one error on a single ADC16 channel (A or B).
  For the auto-calibration of both ADC16 channels, see function CalAutoBothAdc16s().
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      adc_idx;
    FP32        err;
    int32_t      vraw;

    // Get the channel index

    adc_idx = (dpcls->mcu.cal.chan_mask == 0x01 ? 0 : 1);       // Channel A: 0; Channel B: 1

    // Calculate the compensated raw value, based on a previous calibration of both ADC16s, if available.

    vraw = property.cal.adc.internal[adc_idx].vraw[cal_signal];

    switch(cal_signal)
    {
    case CAL_GAIN_ERR_POS:

        vraw += property.cal.adc.internal[adc_idx].vraw[5];
        break;

    case CAL_GAIN_ERR_NEG:

        vraw += property.cal.adc.internal[adc_idx].vraw[6];
        break;

    case CAL_OFFSET_V:
    default:

        // No correction
        break;
    }

    // --- Calculate ADC calibration errors normalised to T0 (23C) for both channels ---

    // It is the responsibility of the operator to carry out the three calibration
    // steps (OFFSET, POS, NEG) in the right order (i.e. starting with OFFSET)

    calAdcError(cal_signal,
                vraw,
                property.cal.adc.internal[adc_idx].gain,
                property.adc.temperature,
                property.cal.adc.internal[adc_idx].tc,
                property.cal.adc.internal[adc_idx].dtc,
                property.cal.vref.err,
                &cal.normalized_adc16_factors[adc_idx]);

    calEventStamp(&cal.normalized_adc16_factors[adc_idx],
                  rtcom.time_now_us.unix_time,
                  property.adc.temperature);

    // --- Convert the calibration factors for MCU property CAL.#.ADC16.ERR ---

    dpcls->mcu.cal.adc.internal[adc_idx].err[cal_signal] = ToIEEE( ((FP32*)&cal.normalized_adc16_factors[adc_idx])[cal_signal] );
 // dpcls->mcu.cal.adc.internal[adc_idx].err[3] (the temperature) is set by the MCU
    dpcls->mcu.cal.adc.internal[adc_idx].err[4]          = ToIEEE(          cal.normalized_adc16_factors[adc_idx].date_days );
    dpcls->mcu.cal.adc.internal[adc_idx].err[5]          = ToIEEE(          cal.normalized_adc16_factors[adc_idx].time_s );
}
/*---------------------------------------------------------------------------------------------------------*/
void CalAutoExternalAdc(enum cal_idx cal_signal)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when calibrating the external 22-bit ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      adc_idx;
    uint32_t      start_idx;
    uint32_t      end_idx;
    FP32        ext_ref_err[CAL_NUM_ERRS];

    // Reference error (in ppm), derived from property CAL.EXT_REF_ERR.

    ext_ref_err[CAL_OFFSET_V]     = 0.0;
    ext_ref_err[CAL_GAIN_ERR_POS] = property.cal.ext_ref_err;
    ext_ref_err[CAL_GAIN_ERR_NEG] = property.cal.ext_ref_err;

    // Start, end indexes

    start_idx  = ((dpcls->mcu.cal.chan_mask & 0x01) ? 0 : 1);
    end_idx    = ((dpcls->mcu.cal.chan_mask & 0x02) ? 1 : 0);

    for(adc_idx = start_idx; adc_idx <= end_idx; adc_idx++)
    {
        // It is the responsibility of the operator to carry out the three calibration
        // steps (OFFSET, POS, NEG) in the right order (i.e. starting with OFFSET)

        calAdcError(cal_signal,
                    cal.vraw[adc_idx][cal_signal],
                    property.cal.adc.external[adc_idx].gain,
                    CAL_T0,
                    NULL,
                    NULL,
                    &ext_ref_err[0],
                    &cal.normalized_adc22_factors[adc_idx]);

        calEventStamp(&cal.normalized_adc22_factors[adc_idx],
                      rtcom.time_now_us.unix_time,
                      CAL_T0);

        // --- Convert the calibration factors for MCU property CAL.#.ADC22.ERR ---

        dpcls->mcu.cal.adc.external[adc_idx].err[cal_signal] = ToIEEE( ((FP32*)&cal.normalized_adc22_factors[adc_idx])[cal_signal] );
     // dpcls->mcu.cal.adc.external[adc_idx].err[3] (the temperature) is set by the MCU
        dpcls->mcu.cal.adc.external[adc_idx].err[4]          = ToIEEE(          cal.normalized_adc22_factors[adc_idx].date_days );
        dpcls->mcu.cal.adc.external[adc_idx].err[5]          = ToIEEE(          cal.normalized_adc22_factors[adc_idx].time_s );
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CalAutoDcct(enum cal_idx cal_signal)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when calibrating the DCCTs.

  Note: cal.adc.invgain[c] = 10 / adc_gain
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      channel_idx;
    uint32_t      start_idx;
    uint32_t      end_idx;

    start_idx  = ((dpcls->mcu.cal.chan_mask & 0x01) ? 0 : 1);
    end_idx    = ((dpcls->mcu.cal.chan_mask & 0x02) ? 1 : 0);

    for(channel_idx = start_idx; channel_idx <= end_idx; channel_idx++)
    {
        // It is the responsibility of the operator to carry out the three calibration
        // steps (OFFSET,POS,NEG) in the right order (i.e. starting with OFFSET)

        calDcctError(cal_signal,
                     cal.vraw[channel_idx][cal_signal],
                     &cal.active->adc_factors[channel_idx],
                     property.dcct.temperature[channel_idx],
                     property.cal.dcct[channel_idx].tc,
                     property.cal.dcct[channel_idx].dtc,
                     property.cal.dcct[channel_idx].headerr,
                     &cal.normalized_dcct_factors[channel_idx]);

        calEventStamp(&cal.normalized_dcct_factors[channel_idx],
                      rtcom.time_now_us.unix_time,
                      property.dcct.temperature[channel_idx]);

        dpcls->mcu.cal.dcct[channel_idx].err[cal_signal] = ToIEEE( ((FP32*)&cal.normalized_dcct_factors[channel_idx])[cal_signal] );
     // dpcls->mcu.cal.dcct[channel_idx].err[3] (the temperature) is set by the MCU
        dpcls->mcu.cal.dcct[channel_idx].err[4]          = ToIEEE(          cal.normalized_dcct_factors[channel_idx].date_days );
        dpcls->mcu.cal.dcct[channel_idx].err[5]          = ToIEEE(          cal.normalized_dcct_factors[channel_idx].time_s );
    }
}
/*---------------------------------------------------------------------------------------------------------*/
bool CalAutoDac(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when calibrating the DAC.
  This function returns true if the DAC calibration has completed, false if it is still on-going.

  The timeline of the DAC calibration process can be found below, in steps of 200ms. Note that for each of
  the three voltage measurements, the first 200ms period is ignored, and only the second one is used for
  calibration. Therefore the total DAC calibration process amounts to 1.2s.

  200ms period #0:      Set DAC reference to 0
  200ms period #1:      Pause
  200ms period #2:      Read ADC 200ms filtered measurement;
                        Set DAC reference to positive calibration reference (+DAC_CAL_REF)
  200ms period #3:      Pause
  200ms period #4:      Read ADC 200ms filtered measurement;
                        Set DAC reference to negative calibration reference (-CAL_DAC_REF)
  200ms period #5:      Pause
  200ms period #6:      Read ADC 200ms filtered measurement;
                        Set DAC reference to 0;
                        Compute the new calibration coefficients

\*---------------------------------------------------------------------------------------------------------*/
{
    const int32_t    dac_ref[4] = { 0, DAC_CAL_REF, -DAC_CAL_REF, 0 };
    uint32_t          signal_idx;

    signal_idx = cal.step_200ms / 2;            // signal_idx = 0, 1, 2, 3

    // Set the DAC voltage for the next two 200ms periods

    property.ref.dac = dac_ref[signal_idx];
    DAC_SET(property.ref.dac);

    // State machine based on 200ms steps

    switch(cal.step_200ms++)
    {
    case 0:

        cal.ongoing_dac_cal = true;          // Start of DAC calibration
        break;

    case 1:
    case 3:
    case 5:
    default:

        // Do nothing
        break;

    case 2:

        // Store calibration measurement for the zero reference

        property.cal.dac[CAL_OFFSET_V]     = 0.5 * (adc_chan[0].ms200.cal.v_adc + adc_chan[1].ms200.cal.v_adc);
        break;

    case 4:

        // Store calibration measurement for the positive reference

        property.cal.dac[CAL_GAIN_ERR_POS] = 0.5 * (adc_chan[0].ms200.cal.v_adc + adc_chan[1].ms200.cal.v_adc);
        break;

    case 6:

        // Store calibration measurement for the negative reference

        property.cal.dac[CAL_GAIN_ERR_NEG] = 0.5 * (adc_chan[0].ms200.cal.v_adc + adc_chan[1].ms200.cal.v_adc);

        // Check calibration

        for(signal_idx=0; signal_idx < 3; signal_idx++)
        {
            if(property.cal.dac[signal_idx] > dac_high[signal_idx] ||
               property.cal.dac[signal_idx] < dac_low [signal_idx])
            {
                Set(rtcom.st_latched, FGC_LAT_DAC_FLT);     // Report HW fault if calibration is bad
                Set(rtcom.faults,     FGC_FLT_FGC_HW);
                break;
            }
        }

        // Got the three acquisition samples. Compute the new calibration coefficients

        ParsCalDac();

        // DAC calibration completed

        cal.ongoing_dac_cal = false;
        break;
    }

    return (!cal.ongoing_dac_cal);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cal.c
\*---------------------------------------------------------------------------------------------------------*/

