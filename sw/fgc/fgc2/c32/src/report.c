/*---------------------------------------------------------------------------------------------------------*\
  File:     report.c

  Purpose:  FGC DSP Software - contains functions that report information to the MCU

  Author:   Quentin.King@cern.ch

  Notes:    All real-time processing runs at interrupt level under IsrPeriod().
\*---------------------------------------------------------------------------------------------------------*/

#include <report.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <dsp_dependent.h>      // for ToIEEE()
#include <rtcom.h>              // for rtcom global variable
#include <main.h>               // for state_pc, dcct_select global variables
#include <meas.h>               // for meas global variable
#include <reg.h>                // for reg, load global variables
#include <adc.h>                // for adc_chan global variables
#include <cyc.h>                // for cyc global variable
#include <macros.h>             // for Test()
#include <definfo.h>            // for FGC_CLASS_ID
#include <sim.h>                // for sim global variable
#include <math.h>               // for fabs()
#include <ppm.h>                // for ppm[]
#include <defconst.h>           // for FGC_N_ADCS

/*---------------------------------------------------------------------------------------------------------*/
void Report(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU
\*---------------------------------------------------------------------------------------------------------*/
{
    // Transfer every iteration

    dpcls->dsp.ref.func_type = ref.func_type;

#if FGC_CLASS_ID == 51
    dpcls->dsp.meas.ia  = ToIEEE(adc_chan[0].meas.i_dcct);
    dpcls->dsp.meas.ib  = ToIEEE(adc_chan[1].meas.i_dcct);
#else
    dpcls->dsp.ref.now  = ToIEEE(ref.now->value);
    dpcls->dsp.ref.i    = ToIEEE(ref.i_clipped.value);
    dpcls->dsp.ref.i_rst= ToIEEE(ref.i.value);
    dpcls->dsp.ref.v    = ToIEEE(ref.v.value);
    dpcls->dsp.meas.b   = ToIEEE(meas.b);
    dpcls->dsp.meas.i   = ToIEEE(meas.i);
    dpcls->dsp.meas.v   = ToIEEE(meas.v);
    dpcls->dsp.reg.err  = ToIEEE(reg_bi_err.err);
#endif

    // Transfer values to MCU on 10ms, 20ms, 200ms and 1s boundaries

#if FGC_CLASS_ID == 51
    if(rtcom.start_of_ms10_f)
    {
        Report10ms();           // Report10ms should be before Report1s() because of Imeas/Vmeas
    }
#endif

    if(rtcom.start_of_second_f)
    {
        Report1s();         // Report1s is preferably called before Report20ms because of Idiff status
    }

    if(rtcom.start_of_ms20_f)
    {
        Report20ms();
    }

    if(rtcom.start_of_ms200_f)
    {
        Report200ms();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void Report10ms(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU on 10ms boundaries
\*---------------------------------------------------------------------------------------------------------*/
{
    dpcls->dsp.ref.v       = ToIEEE(ref.v.value);       // Voltage reference
    dpcls->dsp.ref.i       = ToIEEE(ref.i_clipped.value);   // Current reference (clipped reference)
    dpcls->dsp.ref.i_rst   = ToIEEE(ref.i.value);               // Current reference (the one stored in RST history)
    dpcls->dsp.meas.i      = ToIEEE(meas.i);                    // Current measurement
    dpcls->dsp.meas.v      = ToIEEE(meas.v);                // Voltage source output
    dpcls->dsp.meas.i_rate = ToIEEE(meas.i_hist.rate);          // Current rate measurement
}
/*---------------------------------------------------------------------------------------------------------*/
void Report20ms(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU on 20ms boundaries
\*---------------------------------------------------------------------------------------------------------*/
{
    int32_t     i_err_ma;

#if FGC_CLASS_ID == 51
    dpcls->dsp.meas.b   = ToIEEE(meas.b);
#endif

#if FGC_CLASS_ID == 51
    // I_LIM_EXPECTED warning

    if((reg.active->state == FGC_REG_I &&                   // if IREF mode and
        ppm[0].armed.meta.range.max > property.limits.op.i_available) ||// final ref value is > avl current, or
        property.limits.op.i.pos > property.limits.i.hardware)          // I_POS is > I_HARDWARE
    {
        Set(rtcom.warnings, FGC_WRN_ILIM_EXPECTED);              // Set I_LIM_EXP warning
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_ILIM_EXPECTED);              // Clear I_LIM_EXP warning
    }
#endif

    // Measurement warning

    if (    (rtcom.st_unlatched & FGC_UNL_I_MEAS_DIFF)
         || !(adc_chan[0].st_meas & adc_chan[1].st_meas  & FGC_MEAS_I_MEAS_OK)
         || ( (adc_chan[0].st_meas | adc_chan[1].st_meas) & FGC_MEAS_CAL_FAILED)
       )
    {
        Set(rtcom.warnings, FGC_WRN_I_MEAS);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_I_MEAS);
    }

    // VS gain warning

    if (    !lim_i_ref.flags.unipolar
         || (meas.i > limits.i_closeloop)
       )
    {
        if ( reg_v_err.flags.warning )
        {
            Set(rtcom.warnings, FGC_WRN_V_ERROR);
        }
        else
        {
            Clr(rtcom.warnings, FGC_WRN_V_ERROR);
        }
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_V_ERROR);
    }

    // REG_ERROR warning (suppressed if REG_ERROR fault present)

    if(reg.active->state != FGC_REG_V &&
       (reg.active->rst_pars.status != REG_OK ||
        (!reg.test_user_f && reg_bi_err.flags.warning && !reg_bi_err.flags.fault)))
    {
        Set(rtcom.warnings, FGC_WRN_REG_ERROR);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_REG_ERROR);
    }

    // REG_ERROR fault

    if (    reg.active->state != FGC_REG_V
         && !ref.cycling_f
         && reg_bi_err.flags.fault)
    {
        Set(rtcom.faults,FGC_FLT_REG_ERROR);
    }
    // ELSE... DO NOT Clr(rtcom.faults,FGC_FLT_REG_ERROR);

    // Send status data to MCU

    dpcom->dsp.faults   = rtcom.faults;
#if FGC_CLASS_ID == 51
    dpcom->dsp.warnings = rtcom.warnings;
#else
    dpcom->dsp.warnings = rtcom.warnings | cyc.stretched_warnings;
#endif

    dpcom->dsp.st_unlatched = rtcom.st_unlatched;
    dpcom->dsp.st_latched   = rtcom.st_latched;

    dpcls->dsp.meas.st_dcct[0] = adc_chan[0].st_dcct;
    dpcls->dsp.meas.st_dcct[1] = adc_chan[1].st_dcct;
    dpcls->dsp.meas.st_meas[0] = adc_chan[0].st_meas;
    dpcls->dsp.meas.st_meas[1] = adc_chan[1].st_meas;

    rtcom.faults     = 0;
    rtcom.st_latched = 0;

    // Channels A & B (Current measurement channels IA and IB)

    if(dpcls->mcu.meas.status_reset_f)                                  // If "S MEAS.STATUS RESET"
    {
        dpcls->mcu.meas.status_reset_f = false;

        adc_chan[0].st_meas = 0;                                                // Clear all status bits
        adc_chan[1].st_meas = 0;                                                // Clear all status bits
    }

    adc_chan[0].st_meas &= FGC_MEAS_CAL_FAILED | FGC_MEAS_DEAD_FILTER;  // Clear all but CAL_FAILED and DEAD_FILTER
    Set(adc_chan[0].st_meas, FGC_MEAS_V_MEAS_OK);
    Set(adc_chan[0].st_meas, FGC_MEAS_I_MEAS_OK);

    adc_chan[1].st_meas &= FGC_MEAS_CAL_FAILED | FGC_MEAS_DEAD_FILTER;  // Clear all but CAL_FAILED and DEAD_FILTER
    Set(adc_chan[1].st_meas, FGC_MEAS_V_MEAS_OK);
    Set(adc_chan[1].st_meas, FGC_MEAS_I_MEAS_OK);

#if (FGC_CLASS_ID == 51)
    // Send Izero, Ilow and Iaccess flags

    dpcls->dsp.meas.i_zero_f   = lim_i_meas.flags.zero;
    dpcls->dsp.meas.i_low_f    = lim_i_meas.flags.low;
    dpcls->dsp.meas.i_access_f = (fabs(meas.i) > property.limits.i.access);
#endif

#if (FGC_CLASS_ID == 51)
    // Send i_earth measurement to MCU as FP32 (A) and integer (% of trip level)

    if(property.meas.max.i_earth < fabs(meas.i_earth))  // Remember max absolute value of I_EARTH
    {
        property.meas.max.i_earth = fabs(meas.i_earth);
    }

    dpcls->dsp.meas.i_earth = ToIEEE(meas.i_earth);     // Send as FP32 to MCU

    if(meas.i_earth_pcnt >= 327.665)                      // Clip integer value to -327.68%/+327.67% (16 bits)
    {
        dpcls->dsp.meas.i_earth_cpcnt = 32767;                             // MAX VALUE FOR SIGNED SHORT
    }
    else if(meas.i_earth_pcnt <= -327.675)
    {
        dpcls->dsp.meas.i_earth_cpcnt = -32768;                            // MIN VALUE FOR SIGNED SHORT
    }
    else
    {
        // Truncate floating-point to nearest signed integer

        dpcls->dsp.meas.i_earth_cpcnt = meas.i_earth_pcnt >= 0.0
                                      ? (int32_t)(100.0 * meas.i_earth_pcnt + 0.5)
                                      : (int32_t)(100.0 * meas.i_earth_pcnt - 0.5);
    }
#endif

    //  Send current regulation error to MCU in mA clipped to around +/-33 A (since the published value is
    //  a signed INT16)

    if(reg.active->state == FGC_REG_I)
    {
        i_err_ma = (int32_t)(1000.0 * reg_bi_err.err);

        // Clip the integer value to a signed 16 bit integer (type of the published I_ERR_MA).
        // NB: We do not use the defines SHRT_MIN/SHRT_MAX from limits.h here because
        //     the short integer is 32 bits on C32.

        i_err_ma = MAX(-32768,   i_err_ma);
        i_err_ma = MIN(          i_err_ma, 32767);
    }
    else
    {
        i_err_ma = 0;
    }

    dpcls->dsp.reg.i_err_ma = i_err_ma;

    // TODO here, risk of a race condition with function ParsMeasISim (S MEAS.I_SIM value)

    property.meas.i_sim = sim.active->i;
}
/*---------------------------------------------------------------------------------------------------------*/
void Report200ms(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU on 200ms boundaries
\*---------------------------------------------------------------------------------------------------------*/
{
#if FGC_CLASS_ID == 51
           bool i_min_f;
    static bool  prev_i_min_f;

    if(lim_i_ref.flags.unipolar)
    {
        i_min_f = ( meas.i > (property.limits.op.i.min * (1.0 - REG_LIM_CLIP)) );
    }
    else
    {
        i_min_f = ( meas.i > lim_i_ref.min_clip );
    }

    dpcls->dsp.meas.i_min_f  = (prev_i_min_f && i_min_f);   // Set i_min_f once I_MIN reached for 200ms
    prev_i_min_f = i_min_f;
#endif

    // Report value for properties REF.RUNNING and REF.REMAINING

    dpcls->dsp.ref.time_running   = ToIEEE(ref.time_running.s);
    dpcls->dsp.ref.time_remaining = ToIEEE(ref.time_remaining.s);

    // Enter new ADC values into ADC FIFO

    AdcFifo();
}
/*---------------------------------------------------------------------------------------------------------*/
void Report1s(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU on 1 second boundaries
\*---------------------------------------------------------------------------------------------------------*/
{
#if FGC_CLASS_ID == 51
    uint32_t  c;

    // ILEADS signals at 1 Hz - Check warning threshold with 80% hysteresis

    dpcls->dsp.meas.ileads.i = dpcls->dsp.meas.i;   // Prepared by Report10ms()
    dpcls->dsp.meas.ileads.v = dpcls->dsp.meas.v;

    if(!sim.active->enabled)            // If not simulating
    {
        meas.u_leads[0] = 0.02 * meas.u_lead_pos;       // Take average of 50 samples
        meas.u_leads[1] = 0.02 * meas.u_lead_neg;
    }
    else                // else simulate based on 0.13 mOhms and 0.15 mOhms
    {
        meas.u_leads[0] = 1.3E-4 * meas.i;
        meas.u_leads[1] = 1.5E-4 * meas.i;
    }

    if(Test(rtcom.warnings, FGC_WRN_CURRENT_LEAD))
    {
        if(fabs(meas.u_leads[0]) < ULEAD_WARN_CLR_LIMIT &&
           fabs(meas.u_leads[1]) < ULEAD_WARN_CLR_LIMIT)
        {
            Clr(rtcom.warnings, FGC_WRN_CURRENT_LEAD);              // Clear warning
        }
    }
    else
    {
        if(fabs(meas.u_leads[0]) >= ULEAD_WARN_SET_LIMIT ||
           fabs(meas.u_leads[1]) >= ULEAD_WARN_SET_LIMIT)
        {
            Set(rtcom.warnings, FGC_WRN_CURRENT_LEAD);              // Set warning
        }
    }

    for( c = 0; c < 2; c++ )          // Transfer to microcontroller and remember max abs values
    {
        dpcls->dsp.meas.ileads.u_leads[c] = ToIEEE(meas.u_leads[c]);

        if(fabs(meas.u_leads[c]) > property.meas.max.u_leads[c])
        {
            property.meas.max.u_leads[c] = fabs(meas.u_leads[c]);
        }
    }

    meas.u_lead_pos = meas.u_lead_neg = 0.0;
#endif
    // Don't check Idiff when stopping as rapid changes in current can make Idiff incorrect

    if(state_pc != FGC_PC_STOPPING && state_pc != FGC_PC_FLT_STOPPING && state_pc != FGC_PC_FLT_OFF)
    {
        // Idiff warning with 20% hysteresis

        if(Test(rtcom.st_unlatched,FGC_UNL_I_MEAS_DIFF))
        {
            if(dcct_select != FGC_DCCT_SELECT_AB ||
              !(adc_chan[0].st_meas & adc_chan[1].st_meas & FGC_MEAS_I_MEAS_OK) ||
               fabs(meas.i_diff_1s) < (0.8 * property.limits.i.meas_diff))
            {
                Clr(rtcom.st_unlatched, FGC_UNL_I_MEAS_DIFF);           // Clear unlatched status
            }
        }
        else
        {
            if(dcct_select == FGC_DCCT_SELECT_AB &&
              (adc_chan[0].st_meas & adc_chan[1].st_meas & FGC_MEAS_I_MEAS_OK) &&
               property.limits.i.meas_diff > 0.0 && fabs(meas.i_diff_1s) > property.limits.i.meas_diff)
            {
                Set(rtcom.st_unlatched, FGC_UNL_I_MEAS_DIFF);           // Set unlatched status
            }
        }

        // Idiff fault at 10x warning level

        if (     ( dcct_select == FGC_DCCT_SELECT_AB )
              && ( adc_chan[0].st_meas & adc_chan[1].st_meas & FGC_MEAS_I_MEAS_OK )
              && ( property.limits.i.meas_diff > 0.0)
              && ( fabs(meas.i_diff_1s) > (10 * property.limits.i.meas_diff) )
           )
        {
            Set(rtcom.faults, FGC_FLT_I_MEAS);
        }
    }

    // Clipped Idiff signal at 1 Hz. Clipping is to account for the fact the the published I_diff value
    // is a signed 16-bit integer.
    // NB: We do not use the defines SHRT_MIN/SHRT_MAX from limits.h here because
    //     the short integer is 32 bits on C32.

    dpcls->dsp.meas.i_diff_ma = (int32_t)(meas.i_diff_1s * 1000.0);
    dpcls->dsp.meas.i_diff_ma = MAX(-32768,   dpcls->dsp.meas.i_diff_ma);
    dpcls->dsp.meas.i_diff_ma = MIN(          dpcls->dsp.meas.i_diff_ma, 32767);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: report.c
\*---------------------------------------------------------------------------------------------------------*/
