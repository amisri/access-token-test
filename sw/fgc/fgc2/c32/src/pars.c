/*---------------------------------------------------------------------------------------------------------*\
  File:         pars.c

  Purpose:      FGC2 C32 Software - Parameter group set functions.

  Author:       Quentin.King@cern.ch

  Notes:        These functions are called at the end of the millisecond ISR provided it is not a
                10ms millisecond boundary when lots of parameters are sent to the MCU.
\*---------------------------------------------------------------------------------------------------------*/

#define PARS_GLOBALS
#define DEFPROPS                // Force property table definition

#include <pars.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <dsp_dependent.h>      // for ToIEEE()
#include <rtcom.h>              // for rtcom global variable
#include <reg.h>                // for load global variables
#include <macros.h>             // for Test()
#include <isr.h>
#include <lib.h>                // for
#include <sim.h>                // for sim global variable, needed by defprops_dsp_fgc.h
#include <cyc.h>                // for LOG_CAPTURE_BASEADDR/log_capture_buffer, needed by defprops_dsp_fgc.h
#if FGC_CLASS_ID == 53
    #include <pal.h>            // for pal global variable, needed by defprops_dsp_fgc.h
    #include <cyc_class.h>
#endif
#include <string.h>             // String library, memset()
#include <property.h>
#include <defprops_dsp_fgc.h>   // for pars_func[idx]()
#include <cal.h>                // for cal global variable
#include <main.h>               // for DAC_RESOLUTION, DSP_ITER_PERIOD
#include <definfo.h>            // for FGC_CLASS_ID
#include <mcu_dsp_common.h>     // for IsFiniteFloat
#include <dsp_panic.h>
#include <adc.h>
#include <isr_plat.h>
#include <bgp_plat.h>

extern void (*pars_func[])(void);

/*---------------------------------------------------------------------------------------------------------*/
void ParsGroupSet(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at the end of the millisecond ISR (on FGC2) if the par_group_set_mask is non-zero, which
  indicates that at least one parameter group has had a property set. It does not necessarily mean that
  the property has changed value. The function identifies the related property group from the bit set in
  the mask and calls the appropriate function. Only one function is ever executed per millisecond to reduce
  the risk of overrun. If a function is run, the associated bit is cleared in the change mask.

  On FGC3, this function is called in the BGP task.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      idx;
    uint32_t      mask;

    // Search for LSB which is set
    for( idx = 0, mask = 1; !(rtcom.par_group_set_mask & mask); mask <<= 1, idx++ )
    {
    };

    pars_func[idx]();                                                    // Run parameter group function

    Clr(rtcom.par_group_set_mask, mask);                                    // Clear parsing request
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsMeasSim(void)
/*---------------------------------------------------------------------------------------------------------*\
  Simulate measurement parameter has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t enabled;

    enabled = (property.meas.sim && rtcom.state_op == FGC_OP_SIMULATION);

    if(enabled && !sim.active->enabled)                                 // If sim now enabled
    {
        sim.active->load.current    = 0.0;                                // Reset simulated current
        sim.active->load.integrator = 0.0;                                // Reset simulated current
    }

    sim.active->enabled = enabled;
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsMeasISim(void)
/*---------------------------------------------------------------------------------------------------------*\
  Force the simulation integrator to a user specified value (called on a S MEAS.I_SIM command)
\*---------------------------------------------------------------------------------------------------------*/
{
    // Atomic memory write: no need for a critical section

    sim.active->load.integrator = property.meas.i_sim;              // Force simulated current
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsCalFactors(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a calibration property that impacts the calibration factors is modified
\*---------------------------------------------------------------------------------------------------------*/
{
    // On FGC2 the calibration update is done in the ISR whereas on FGC3 it is taken care of by the BGP task.

    isr.trigger.calibration_refresh = true;
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsCalDac(void)
/*---------------------------------------------------------------------------------------------------------*\
  DAC calibration parameters have changed.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 51)
    calDacInit(&property.cal.dac[0], &cal.next->dac_factors, DAC_RESOLUTION, DAC_CAL_REF);

    ParsLimits();                                       // Recalculate limits based on new DAC calibration
#endif // #if (FGC_CLASS_ID == 51)
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsRegI(void)
/*---------------------------------------------------------------------------------------------------------*\
  Current regulation parameters have changed. Recalculate the RST coefficients for the current regulation.
\*---------------------------------------------------------------------------------------------------------*/
{
    // [FGC3] DO NOT DO A "Set(pars.parameter_switch_mask, ISR_SWITCH_REG)" IN THIS CASE!!
    //        Indeed at the end of this function, the next regulation parameters (pointed by reg.next) a not
    //        ready. The setting of those parameters and the request for a switch, if necessary/possible, is
    //        entirely handled by the RegStatePrepare() function.

    // Update the regulation properties

    regRstInit(&property.reg.i.op,
               property.reg.v.rst_pars.period,
               property.reg.i.period_div    [property.load.select],
               &load.last_parsed->pars,
               property.reg.i.clbw          [property.load.select],
               property.reg.i.clbw2         [property.load.select],
               property.reg.i.z             [property.load.select],
               property.reg.i.pure_delay    [property.load.select],
               &property.reg.i.manual_rst);

    // If currently in I regulation, need to update the regulation parameter as soon as possible

    if(reg.active->state == FGC_REG_I)
    {
        RegStateRequest(false, false, FGC_REG_I, ref.v.value);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsRegV(void)
/*---------------------------------------------------------------------------------------------------------*\
  Voltage regulation parameters have changed (REG.V.PERIOD_DIV or REG.V.TRACK_DELAY).
  Recalculate the voltage regulation error parameters.
\*---------------------------------------------------------------------------------------------------------*/
{
    regErrInitLimits(&reg_v_err,
                     property.limits.v.err_warning,
                     property.limits.v.err_fault);
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsRegTestReset(void)
/*---------------------------------------------------------------------------------------------------------*\
  REG.TEST.RESET has been set. Initialise all the REF.TEST properties to be copies of the operational
  properties.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Reset REG.TEST.USER and REF_USER

    property.reg.test.user            = 0;
    property.reg.test.ref_user        = 0;

    // Copy current RST coefficients + track delay

    property.reg.test.b.rst           = property.reg.b.manual_rst;
    property.reg.test.i.rst           = property.reg.i.op.rst;         // Copy the REG.I.OP coefficients

    // Copy period requests and call parsPeriod() to compute property.reg.test.i.period
    // and property.reg.test.b.period

    property.reg.test.b.period_div = property.reg.b.period_div[property.load.select];
    property.reg.test.i.period_div = property.reg.i.period_div[property.load.select];
    ParsPeriods();
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLoad(void)
/*---------------------------------------------------------------------------------------------------------*\
  Load parameters have changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Load.select

    load.next->select = property.load.select;

    // Will initialise the load_param structure, except for the saturation part (load.next->pars.sat)

    regLoadInit(&load.next->pars,
                property.load.ohms_ser     [property.load.select],
                property.load.ohms_par     [property.load.select],
                property.load.ohms_mag     [property.load.select],
                property.load.henrys       [property.load.select],
                property.load.gauss_per_amp[property.load.select]);

    // load.last_parsed is used by the other parsing functions in this file to know the latest load settings,
    // in particular if the switch request has just been done.

    load.last_parsed = load.next;               // Pointers

    ParsLoadSim();
    ParsLimits();
    ParsRegI();
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLoadSim(void)
/*---------------------------------------------------------------------------------------------------------*\
  Reinitialise VS + load simulation.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32        vref;
    FP32        vload;

    vref              = ref.v.value;

    // Reset initial B/I/V measurements in case they are not finite floats (either an infinity, or a NAN)

    if(!IsFiniteFloat(sim.next->b))
    {
        sim.next->b = 0.0;
    }

    if(!IsFiniteFloat(sim.next->i))
    {
        sim.next->i = 0.0;
    }

    if(!IsFiniteFloat(sim.next->v))
    {
        sim.next->v = 0.0;
    }

    // Reset VS simulation

    regSimVsInitGain(&property.vs.sim, &load.last_parsed->pars);

    // Reset Load simulation

    regSimLoadInit(&load.last_parsed->pars, &sim.next->load, property.reg.v.rst_pars.period);

    // According to the type of regulation, initialise the voltage or current in the load simulation
    // (sim.next->v/i/b have been copied from sim.active earlier in this function)

    if(reg.active->state == FGC_REG_V)
    {
        // vload = sim.next->v in this case

        vload = regSimLoadSetVoltage(&load.last_parsed->pars, &sim.next->load, sim.next->v);
    }
    else
    {
        // regSimLoadSetCurrent returns the voltage that corresponds to the V_LOAD

        vload = regSimLoadSetCurrent(&load.last_parsed->pars, &sim.next->load, sim.next->i);
    }

    regSimVsInitHistory(&property.vs.sim, &sim.next->vs, vload);


    // Reset the VS controller and measurements delays
    // (sim.next->v/i/b have been copied from sim.active earlier in this function)

    SimInitDelays(sim.next, vref, sim.next->v, sim.next->i, sim.next->b);
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLoadSaturation(void)
/*---------------------------------------------------------------------------------------------------------*\
  Load saturation parameters have changed.

  Note: implementation can be simplified if there is only one pars function ParsLoad doing the load + sat parts.
\*---------------------------------------------------------------------------------------------------------*/
{
    regLoadInitSat(&load.next->pars,
                   property.load.henrys_sat [load.next->select],
                   property.load.i_sat_start[load.next->select],
                   property.load.i_sat_end  [load.next->select]);

    load.last_parsed = load.next;
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLoadSelect(void)
/*---------------------------------------------------------------------------------------------------------*\
  LOAD.SELECT has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Update load only if LOAD.SELECT has changed

    if(load.active->select != property.load.select)
    {
        ParsLoad();                                         // Order of those two function calls matter
        ParsLoadSaturation();

        dpcls->dsp.load_select = property.load.select;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLimits(void)
/*---------------------------------------------------------------------------------------------------------*\
  Limit parameters have changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t invert_limits = 0;
    float    i_low         = property.limits.i.low;

#if (FGC_CLASS_ID == 51)
    FP32 v_pos_dac, v_neg_dac;
#endif

    property.limits.op.i.pos          = property.limits.i.pos           [property.load.select];
    property.limits.op.i.min          = property.limits.i.min           [property.load.select];
    property.limits.op.i.neg          = property.limits.i.neg           [property.load.select];
    property.limits.op.i.rate         = property.limits.i.rate          [property.load.select];
    property.limits.op.i.acceleration = property.limits.i.acceleration  [property.load.select];
    property.limits.op.v.pos          = property.limits.v.pos           [property.load.select];
    property.limits.op.v.neg          = property.limits.v.neg           [property.load.select];

    regLimRefInit(&lim_i_ref, property.limits.op.i.pos, property.limits.op.i.neg,
                  property.limits.op.i.rate, invert_limits);

    if(lim_i_ref.flags.unipolar)                        // If 1 or 2 quadrants
    {
        limits.i_closeloop = property.limits.i.closeloop[property.load.select];
    }
    else                                                // If 4 quadrants
    {
        limits.i_closeloop = 0.0;       // LIMITS.I.CLOSELOOP is not used with 4-quadrant VS
    }

    // If LIMITS.I.LOW is set to 0.0 force it to 1% of LIMITS.I.POS[0] (limit for nominal load)

    if(property.limits.i.low <= 0.0)
    {
        i_low = 0.01 * property.limits.i.pos[0];
    }

    regLimMeasInit(&lim_i_meas,
                   property.limits.op.i.pos,
                   property.limits.op.i.neg,
                   i_low,
                   property.limits.i.zero,
                   invert_limits);

    regLimVrefInit(&lim_v_ref,
                    property.limits.op.v.pos, property.limits.op.v.neg, property.limits.op.v.rate,
                    property.limits.i.quadrants41, property.limits.v.quadrants41, invert_limits);

    lim_v_ref_fg = lim_v_ref;

    limits.i_start   = property.limits.op.i.min * FGC_I_START_FACTOR;

    limits.i_avl_max = (property.limits.op.i.pos < property.limits.i.hardware ?
                        property.limits.op.i.pos : property.limits.i.hardware);

    property.limits.op.i_available = limits.i_avl_max;

    dpcls->dsp.unipolar_f = lim_i_ref.flags.unipolar;   // Send unipolar current flag to MCU

    if(lim_v_ref.flags.unipolar)                        // If 1 quadrant
    {
        property.ref.plep.exp_tc        = load.last_parsed->pars.tc * 1.15;  // Adjust default Tc by 15% to reduce trips
        property.ref.plep.exp_final     = limits.i_closeloop;

        dpcls->dsp.ref.exp_tc    = ToIEEE(load.last_parsed->pars.tc * 1.15); // Define default PLEP decay parameters
        dpcls->dsp.ref.exp_final = ToIEEE(limits.i_closeloop);
    }
    else                                                // else 2 or 4 quadrants
    {
        dpcls->dsp.ref.exp_tc    = 0;                   // Clear default PLEP decay parameters
        dpcls->dsp.ref.exp_final = 0;

        property.ref.plep.exp_tc    = 0.0;
        property.ref.plep.exp_final = 0.0;
    }

#if (FGC_CLASS_ID == 51)
    // Limit Vref to range that DAC can produce

    v_pos_dac = cal.active->dac_factors.max_v_dac * property.vs.gain + property.vs.offset;
    v_neg_dac = cal.active->dac_factors.min_v_dac * property.vs.gain + property.vs.offset;

    if(property.limits.op.v.pos > v_pos_dac)
    {
        property.limits.op.v.pos = v_pos_dac;
    }

    if(property.limits.op.v.neg < v_neg_dac)
    {
        property.limits.op.v.neg = v_neg_dac;
    }
#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsPeriods(void)
/*---------------------------------------------------------------------------------------------------------*\
  Periods parameters have changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t idx, adc_idx;
    uint32_t filter_20ms_length;
    FP32   prev_v_reg_period = property.reg.v.rst_pars.period;

    property.reg.v.rst_pars.status       = REG_OK;

    property.reg.v.rst_pars.period_iters = 1;                                   // V regulation period is equal to the iteration period
    property.reg.v.rst_pars.period       = 0.001;                               // REG.V.PERIOD is always 1ms on classes 51 and 53
    property.reg.v.rst_pars.freq         = 1000.0;                              // REG.V frequency is always 1 kHz on FGC2.

    reg.iteration_period_ns              = 1000000;                             // DSP iteration period = 1ms

    // I/B/testI/TestB regulation periods


    for(idx = 0; idx < FGC_N_LOADS; idx++)
    {
         RegPeriod(&property.reg.b.period_div[idx], &property.reg.b.period[idx]);
         RegPeriod(&property.reg.i.period_div[idx], &property.reg.i.period[idx]);
    }

    RegPeriod(&property.reg.test.b.period_div, &property.reg.test.b.period);
    RegPeriod(&property.reg.test.i.period_div, &property.reg.test.i.period);

    ParsRegI();
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsRunTime(void)
/*---------------------------------------------------------------------------------------------------------*\
  REF.RUN time property has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    ref.run = property.ref.run;                                 // Transfer from property

    // Subtract ref.advance_us

    if(ref.run.unix_time)               // If run time != 0
    {
        AbsTimeUsAddLongDelay(&ref.run, -1, 1000000 - ref.advance_us);          // I.e. ref.run -= ref.advance_us;
    }

    // Add 1s if ref.run is already in the past

    if(AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.run))                 // If run time is already in the past
    {
        ref.run = rtcom.time_now_us;                                            // Set run time to time.now + 1s
        AbsTimeUsAddLongDelay(&ref.run, 1, 0);

        property.ref.run = ref.run;                                             // Transfer the new value to the property
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsAbortTime(void)
/*---------------------------------------------------------------------------------------------------------*\
  REF.ABORT time property has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    ref.abort = property.ref.abort;     // Transfer from property

    // Subtract ref.advance_us

    if(ref.abort.unix_time)             // If abort time != 0
    {
        AbsTimeUsAddLongDelay(&ref.abort, -1, 1000000 - ref.advance_us);        // I.e. ref.abort -= ref.advance_us;
    }

    // Add 1s if ref.abort is already in the past

    if(AbsTimeUs_IsGreaterThan(rtcom.time_now_us, ref.abort))                   // If abort time is already in the past
    {
        ref.abort = rtcom.time_now_us;                                          // Set abort time to time.now + 1s
        AbsTimeUsAddLongDelay(&ref.abort, 1, 0);

        property.ref.abort = ref.abort;                                         // Transfer the new value to the property
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsTest(void)
/*---------------------------------------------------------------------------------------------------------*\
  Property TEST.DSP.uint32_t or TEST.DSP.FLOAT has been set.  This function is available to support software
  debugging.
\*---------------------------------------------------------------------------------------------------------*/
{
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsVsSim(void)
/*---------------------------------------------------------------------------------------------------------*\
  VS model parameters have changed so reinitialise load simulation entirely.
\*---------------------------------------------------------------------------------------------------------*/
{
    ParsLoadSim();
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars.c
\*---------------------------------------------------------------------------------------------------------*/
