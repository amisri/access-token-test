/*!
 * @file diag.c
 * @brief FGC DSP Software - Diag data processing functions.
 *
 * DIM logs are supported for class 51 only.
 */

#define DIAG_GLOBALS

#include <diag.h>
#include <definfo.h>            // For FGC_CLASS_ID
#include <defconst.h>           // Global FGC DSP constants
#include <fgc_consts_gen.h>         // Global FGC constants
#include <fgc_errs.h>           // Include global FGC error constants
#include <dpcom.h>              // for dpcom global variable
#include <rtcom.h>              // for rtcom global variable
#include <lib.h>                // for
#include <macros.h>             // for Test()
#include <dsp_dependent.h>      // for FP32ZERO
#include <defconst.h>
#include <dpcls.h>
#if (FGC_CLASS_ID == 51)
#include <props.h>              // for property global variable
#endif
#include <read_PSU_adc.h>       // for psu global variable

// Internal function declarations

/*!
 * This function supports the DimGetDimLog() function in the MCU by providing access to the logged DIM data.
 *
 * The MCU specifies up to 3 request to obtain samples from a logical_dim using:
 *
 *   dpcom->mcu.diag.log.number_of_requests          can be 0..3,    This is the trigger to process the request
 *   dpcom->mcu.diag.log.logical_dim_to_log_for_req          can be [0..19]
 *
 * It also specifies the sample index for each of the request via :
 *
 *   dpcom->mcu.diag.log.entry_for_req                   can be [0..1599]
 *
 * The function will work for any class but only returns valid data for Class 51 as the other classes do not
 * support DIM logs.
 *
 * This function will acknowledge the completion of the request by resetting diag.log.number_of_requests to zero.
 * It will return 8 consecutive samples for the specified DIMS.
 * It will increment log.sample_idx (and roll over to zero when the end is reached).
 * The resulting data will be returned in dpcom->fcm_prop.blk.intu[64].
 */
static void ProcessLogRequest_8samples  (void);

/*!
 * This function is called from ScaleDimAnalogueData() to store a DIM sample in a log buffer.
 * If samples is NULL then a zero sample is stored as a marker.
 * The function only really stores data for class 51 as the other classes do not have DIM logs.
 */
static void PutDIMsamplesInLog          (uint32_t logical_dim, struct abs_time_us *sample_time, uint32_t *samples);

/*!
 * This function is called from InIrqProcessQSPIbusDataSlice() on ms 19 or 20.
 * It converts the raw analogue diagnostic values to calibrated floating point values
 * in the DIAG.ANASIGS property and logs the raw signals when the logging for a DIM is active.
 *
 * A composite DIM is a virtual DIM that combines the same analogue register [analogue_0..analogue_3]
 * from up to four physical DIMs
 *
 * For composite DIMs the flat_qspi_board_number defines
 *     b7     : composite
 *     b6,b5  : analogue register to use [analogue_0..analogue_3]
 *     b4..b0 : first DIM in list to use (the follow consecutive DIMS in the list will be automatically used)
 *
 *
 *    7     Bus Address     0
 *   +--+--+--+--+--+--+--+--+
 *   |CF|a1|a0|d4|d3|d2|d1|d0|
 *   +--+--+--+--+--+--+--+--+
 *
 *   CF    = Composite flag (0x80)
 *   a1:a0 = Analog input   (0-3)
 *   d4:d0 = DIM channel for first DIM to combine (0-18)
 */
static void ScaleDimAnalogueData        (void);

/*!
 * This function will log debug data in the property DIAG.DIM_SYNC_DBUG
 */
static void SaveInDspDebugBuffer        (uint32_t value);

/*!
 * Analyse the trigger data from the DIMs
 * and identify the trigger time in the event that a trigger input has been activated.
 *
 *   dpcom->mcu.diag.flat_qspi_UNlatched_trig_bits
 *   dpcom->mcu.diag.flat_qspi_latched_trig_bits
 *
 *   qspi_misc.analog_regs_from_all_dim_boards[FGC_N_DIM_ANA_CHANS+1][FGC_MAX_DIM_BUS_ADDR]
 *   qspi_misc.flat_qspi_board_is_valid_packed
 *   qspi_misc.flat_qspi_not_triggered_packed
 *   qspi_misc.unix_time                     Unix time for current cycle
 *   qspi_misc.scantime_us                           us scan times from current cycle
 *   qspi_misc.prev_scantime_us                  us scan times from previous cycle
 *
 *  And the outputs are:
 *
 *   dpcom->dsp.diag.dim_trig_time    [FGC_MAX_DIMS]         DIM trigger times (Unixtime/us time)
 *   dpcom->dsp.diag.dim_evt_log_state[FGC_MAX_DIMS]         DIM event log state (NONE,WATCH,TRIG)
 *
 *  The MCU will pack the the unlatched and latched bits on ms 17 of 20 so this function is run
 *  on ms 1 of 20 of the following cycle.
 *
 *  Setting the dim_evt_log_state to DIM_EVT_TRIG is the trigger for
 *  the MCU to write the active fault(s) and all status bits for the triggered DIM to the event log.
 *  One second later it will reset the bus to clear the latch and allow additional faults to be logged.
 *  This is signalled by the DIM_EVT_WATCH state.
 */
static void LookForTriggerEvent         (void);

#if (FGC_CLASS_ID == 51)

/*!
 * Utility function called to reset a PSU voltage log
 */
static void DiagPsuVoltagesLogReset(struct diag_psulog * psulog);

#endif



// Internal function definitions



#if (FGC_CLASS_ID == 51)

void DiagPsuVoltagesLogReset(struct diag_psulog * psulog)
{
    uint32_t idx;

    for(idx = 0; idx < FGC_PSU_LOG_LEN; idx++)
    {
        psulog->log_ieee[idx] = FP32ZERO;
    }

    psulog->idx = 0;
}

#endif



void ProcessLogRequest_8samples(void)
{
    uint32_t                  req;
    uint32_t                  ss;
    uint32_t                  number_of_requests;
    uint32_t                  entry;
    uint32_t *                    entry_ptr;
    struct TDimLogData *        destination_ptr;


    number_of_requests = dpcom->mcu.diag.log.number_of_requests;    // requests can be [0..3]

    if ( number_of_requests )
    {
        destination_ptr = (struct TDimLogData *)&dpcom->fcm_prop.blk.intu;     // Address of results buf in DPRAM
        entry_ptr = (uint32_t *)dpcom->mcu.diag.log.entry_for_req;

        for( ss = 0; ss < 8; ss++)              // For 8 samples
        {
            for( req = 0; req < number_of_requests; req++)
            {
                entry = entry_ptr[req];     // get where we were

#if FGC_CLASS_ID == 51
                *destination_ptr = dim_log->log_space[ dpcom->mcu.diag.log.logical_dim_to_log_for_req[req] ][entry];
                destination_ptr++;
#else
                // no DIM logs, class 53,59
                destination_ptr->log[0] = 0;
                destination_ptr->log[1] = 0;
                destination_ptr++;
#endif

                entry++;
                if ( entry >= FGC_LOG_DIM_LEN )
                {
                    entry = 0;
                }

                entry_ptr[req] = entry;             // remember
            }
        }

        dpcom->mcu.diag.log.number_of_requests = 0;         // Acknowledge completion of request
    }
}



void PutDIMsamplesInLog(uint32_t logical_dim, struct abs_time_us *sample_time, uint32_t *samples)
{
    uint32_t  xx;

    xx = dpcom->dsp.diag.log[logical_dim].idx + 1;

    if ( xx >= FGC_LOG_DIM_LEN )        // beyond allocated space ?
    {
        xx = 0;
    }

#if FGC_CLASS_ID == 51  // Only store data for class 51
    if ( samples ) // samples = 0, is used as mark
    {
        dim_log->log_space[logical_dim][xx].log[0] = (samples[0] << 16) | samples[1]; // Compact 4 samples into
        dim_log->log_space[logical_dim][xx].log[1] = (samples[2] << 16) | samples[3]; // into two long words
    }
    else
    {
        dim_log->log_space[logical_dim][xx].log[0] = 0;
        dim_log->log_space[logical_dim][xx].log[1] = 0;
    }
#endif

    dpcom->dsp.diag.log[logical_dim].idx  = xx;
    dpcom->dsp.diag.log[logical_dim].time = *sample_time;

    if ( dim_list.dont_log[logical_dim] )
    {
        dpcom->dsp.diag.log[logical_dim].samples_to_acq--;      // Decrement samples to acquire
    }
}



void ScaleDimAnalogueData(void)
{
    uint32_t              logical_dim;
    uint32_t              composite_list_idx;
    uint32_t              board_number;
    uint32_t              ana_reg;                        // analogue register index (0-3)
    uint32_t              composite_ana_reg;
    uint32_t              bit_in_packed;
    uint32_t              latched_triggers_packed;
    uint32_t              freeze_all_dim_logs;
    uint32_t              relaunch_dim_logging;
    uint32_t              analogue_value[FGC_N_DIM_ANA_CHANS];// DIM sample (4 channels)
    struct abs_time_us  sample_time;            // Sample time (for logging) rounded to 20ms


    dpcom->dsp.diag.log_lock_f = -1;    // Set log lock to inform MCU that logging is about to happen
    dpcom->dsp.diag.log_lock_counter++; // Increment log lock counter

    // Latched triggered for DIMs that are present and valid
    latched_triggers_packed = dpcom->mcu.diag.flat_qspi_latched_trig_bits & qspi_misc.flat_qspi_board_is_valid_packed;

    freeze_all_dim_logs  = dpcom->mcu.diag.freeze_all_dim_logs;
    relaunch_dim_logging = dpcom->mcu.diag.relaunch_dim_logging;
    sample_time          = rtcom.time_now_us;

    sample_time.us_time -= 19000;       // This function is called on ms 19/20 so round down to start of 20ms

    // process the DIMs in the list
    for ( logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++ )
    {
        board_number = dim_list.flat_qspi_board_number[logical_dim];

        if ( board_number == 0xFF )         // If DIM is unused
        {
            continue;
        }

        if ( board_number & 0x80 )          // if DIM is a composite
        {
            bit_in_packed = 0; // no bit in the packed bits to use, since composite DIM is not physical
            composite_list_idx = (board_number & 0x1F);     // DIM index of first DIM to combine
            composite_ana_reg = (board_number >> 5) & 0x03; // Ana chan to combine from 4 consecutive DIMs

            // the 4 analogue register of this composite DIM are taken from the same
            // composite_ana_reg of 4 consecutive (in the list) DIM boards
            for ( ana_reg = 0; ana_reg < FGC_N_DIM_ANA_CHANS; ana_reg++ )
            {
                board_number = dim_list.flat_qspi_board_number[composite_list_idx];
                composite_list_idx++;

                if ( board_number < FGC_MAX_DIM_BUS_ADDR )
                {
                    // register analog_0..3 of DIM board, flat[x]
                    analogue_value[ana_reg] = qspi_misc.analog_regs_from_all_dim_boards[composite_ana_reg][board_number] & 0x0FFF;

                    dim_list.analogue_in_physical[ana_reg][logical_dim] =
                    dim_list.offsets[ana_reg][logical_dim] +
                    dim_list.gains[ana_reg][logical_dim] * (FP32)(analogue_value[ana_reg]);
                }
            }

            if ( dpcom->dsp.diag.log[logical_dim].samples_to_acq )      // If logging active
            {
                PutDIMsamplesInLog(logical_dim, &sample_time, &analogue_value[0]);          // Log sample
            }

            if( !dim_list.dont_log[logical_dim] )       // If logging is active
            {
                if (    freeze_all_dim_logs
                    || (latched_triggers_packed & (0x00000001 << dim_list.flat_qspi_board_number[FGC_COMP_DIM_TRIG_IDX]))) // If DIM VS latched
                {
                    dim_list.dont_log[logical_dim] = true;          // Stop log for this comp. DIM
                }
            }
            else                        // else logging is not active
            {
                if ( relaunch_dim_logging )
                {
                    dim_list.dont_log[logical_dim] = false;
                    PutDIMsamplesInLog(logical_dim, &sample_time, 0);   // Log NULL sample to mark the start of logging
                }
            }
        }
        else // not composite DIM
        {
            if ( board_number < FGC_MAX_DIM_BUS_ADDR )
            {
                // bit position corresponding to this board, when packed in uint32_t
                bit_in_packed = 0x00000001U << board_number;

                if ( bit_in_packed & qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] )
                {
                    // log and process the 4 analogue registers of this dim
                    for ( ana_reg = 0; ana_reg < FGC_N_DIM_ANA_CHANS; ana_reg++)    // Convert to FP values
                    {
                        // register analog_0..3 of DIM board, flat[x]
                        analogue_value[ana_reg] = qspi_misc.analog_regs_from_all_dim_boards[ana_reg][board_number] & 0x0FFF;

                        dim_list.analogue_in_physical[ana_reg][logical_dim] =
                                dim_list.offsets[ana_reg][logical_dim] +
                                dim_list.gains[ana_reg][logical_dim] * (FP32)(analogue_value[ana_reg]);
                    }

                    if ( dpcom->dsp.diag.log[logical_dim].samples_to_acq )      // If logging active
                    {
                        PutDIMsamplesInLog(logical_dim, &sample_time, &analogue_value[0]);
                    }
                }

                if ( !dim_list.dont_log[logical_dim] )      // If logging is active
                {
                    if (     freeze_all_dim_logs
                         || (latched_triggers_packed & bit_in_packed))
                    {
                        dim_list.dont_log[logical_dim] = true;      // Stop logging for this DIM
                    }
                }
                else                        // else logging is stopped
                {
                    if (    relaunch_dim_logging
                         && (qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] & bit_in_packed)
                       )
                    {
                        dim_list.dont_log[logical_dim] = false;         // Start logging for this DIM
                        PutDIMsamplesInLog(logical_dim, &sample_time, 0);       // Log NULL sample to mark
                    }                                   // the start of logging
                }
            }
        }

        if ( !dim_list.dont_log[logical_dim] )      // If logging is active
        {
            dpcom->dsp.diag.log[logical_dim].samples_to_acq = FGC_LOG_DIM_LEN / 2;  // Set 50% post trig samples
        }
    }

    if ( relaunch_dim_logging )
    {
        dsp_debug.stop_log = false;
        dsp_debug.samples_to_acq = DIAG_DEBUG_TO_ACQ;

#if (FGC_CLASS_ID == 51)
    // Reset FGC.DEBUG.PSU_5V, FGC.DEBUG.PSU_POS_15V, FGC.DEBUG.PSU_NEG_15V properties

        property.fgc.debug.psu_5v.out_of_limits      = 0;               // Reset error counters
        property.fgc.debug.psu_pos_15v.out_of_limits = 0;
        property.fgc.debug.psu_neg_15v.out_of_limits = 0;

        property.fgc.debug.psu_5v.log_state      = FGC_LOG_RUNNING;     // Unfreeze debug circular buffers
        property.fgc.debug.psu_pos_15v.log_state = FGC_LOG_RUNNING;
        property.fgc.debug.psu_neg_15v.log_state = FGC_LOG_RUNNING;

        DiagPsuVoltagesLogReset(&property.fgc.debug.psu_5v.log);        // Reset log buffers
        DiagPsuVoltagesLogReset(&property.fgc.debug.psu_pos_15v.log);
        DiagPsuVoltagesLogReset(&property.fgc.debug.psu_neg_15v.log);

        psu.fails_counter = 0;    // Counter of consecutive errors (if we do not reset it, the PSU logs can stay at 0.0
        psu.fails_counter_ana_interfaz = 0;
#endif

    }

    dpcom->mcu.diag.freeze_all_dim_logs  = false;
    dpcom->mcu.diag.relaunch_dim_logging = false;
    dpcom->dsp.diag.log_lock_f           = false;       // Clear log lock
}



void SaveInDspDebugBuffer(uint32_t value)
{
    dsp_debug.buf[dsp_debug.idx] = value;

    dsp_debug.idx++;
    if ( dsp_debug.idx >= FGC_LOG_DIAG_DBUG_LEN )
    {
        dsp_debug.idx = 0;
    }

    if ( dsp_debug.stop_log )
    {
        dsp_debug.samples_to_acq--;
    }
}



void LookForTriggerEvent(void)
{
    uint32_t  logical_dim;
    uint32_t  qspi_branch;            // 0=A, 1=B
    uint32_t  board_number;
    uint32_t  bit_in_packed;
    uint32_t  dim_board_has_latched_trig;
    uint32_t  dim_board_has_UNlatched_trig;
    uint32_t  at_least_one_UNlatched_trig;
    uint32_t  trig_time_us;           // Trigger time since start of second in us
    uint32_t  trig_time_s;            // Trigger time in seconds (unix_time)

    at_least_one_UNlatched_trig = false;

    for ( logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++ )
    {
        board_number = dim_list.flat_qspi_board_number[logical_dim];

        if ( board_number < FGC_MAX_DIM_BUS_ADDR )  // If DIM address is real
        {
            // bit position corresponding to this board, when packed in uint32_t
            bit_in_packed = 0x00000001U << board_number;

            // process only valid boards
            if ( bit_in_packed & qspi_misc.flat_qspi_board_is_valid_packed )
            {
                dim_board_has_UNlatched_trig = bit_in_packed & dpcom->mcu.diag.flat_qspi_UNlatched_trig_bits;
                dim_board_has_latched_trig   = bit_in_packed & dpcom->mcu.diag.flat_qspi_latched_trig_bits;

                if ( dim_board_has_UNlatched_trig )
                {
                    at_least_one_UNlatched_trig = true;
                }

                if ( dim_board_has_latched_trig )
                {
                    // check if it is the 1st detection
                    if ( bit_in_packed & qspi_misc.flat_qspi_not_triggered_packed )
                    {
                        // get branch 0=A,1=B
                        // flat [0..15] belongs to A, [16..31] belongs to B
                        qspi_branch = board_number / ( FGC_MAX_DIM_BUS_ADDR / 2 );

                        // clear bit to mark this DIM board as triggered
                        qspi_misc.flat_qspi_not_triggered_packed &= ~bit_in_packed;

                        // the register trigCounter of DIM board is incremented every 8us
                        trig_time_us = 3 + 8 * (qspi_misc.analog_regs_from_all_dim_boards[4][board_number] & 0xFFF);

                        if ( dim_board_has_UNlatched_trig )
                        {
                            trig_time_us += qspi_misc.prev_scantime_us[qspi_branch];

                            if ( trig_time_us < 1000000 )
                            {
                                trig_time_s = qspi_misc.prev_unix_time;
                            }
                            else
                            {
                                trig_time_s   = qspi_misc.unix_time;
                                trig_time_us -= 1000000;
                            }
                        }
                        else
                        {
                            trig_time_us += qspi_misc.scantime_us[qspi_branch];
                            trig_time_s   = qspi_misc.unix_time;
                        }

                        dpcom->dsp.diag.dim_trig_time[logical_dim].unix_time = trig_time_s;

                        dim_list.trig_us[logical_dim] = trig_time_us;
                        dpcom->dsp.diag.dim_trig_time[logical_dim].us_time = trig_time_us;

                        dpcom->dsp.diag.dim_evt_log_state[logical_dim] = DIM_EVT_TRIG;
                        dpcom->dsp.diag.dim_trig_f = true;
                    }
                }
                else
                {
                    dpcom->dsp.diag.dim_evt_log_state[logical_dim] = (dim_board_has_UNlatched_trig ? DIM_EVT_WATCH : DIM_EVT_NONE);
                    // set bit to mark DIM as not triggered
                    Set(qspi_misc.flat_qspi_not_triggered_packed, bit_in_packed);
                }
            }
            else
            {
                dpcom->dsp.diag.dim_evt_log_state[logical_dim] = DIM_EVT_NONE;
                // set bit to mark DIM as not triggered
                Set(qspi_misc.flat_qspi_not_triggered_packed, bit_in_packed);
            }
        }
    }

    dpcom->dsp.diag.global_unlatched_trig_f = at_least_one_UNlatched_trig;
}



// External function definitions



void ValidatePresentDimBoards(uint32_t scan_step)
{
    uint32_t  board_number;
    uint32_t  bit_in_packed;
    uint32_t  present_boards_packed[QSPI_BRANCHES];
    uint32_t  ana_regs_id[4];         // ID of DIM registers [analog_0..3 + trigCounter]
    uint32_t  boards_in_qspi[QSPI_BRANCHES];

/*
    the 7 registers are received in this order
        0 - Dig0        DIM register ID:0       NEXT -> 1
        1 - Dig1        DIM register ID:1               4
        2 - Ana0        DIM register ID:4               5
        3 - Ana1        DIM register ID:5               6
        4 - Ana2        DIM register ID:6               7
        5 - Ana3        DIM register ID:7               2
        6 - TrigCounter DIM register ID:2               0
                         there is no ID:3               x

   sort by ID weight
        0 - Dig0        DIM register ID:0       NEXT -> 1
        1 - Dig1        DIM register ID:1               4
        6 - TrigCounter DIM register ID:2               0
                         there is no ID:3               x use 8 as invalid, 3 can't be used as fails with the test pattern BABx
        2 - Ana0        DIM register ID:4               5
        3 - Ana1        DIM register ID:5               6
        4 - Ana2        DIM register ID:6               7
        5 - Ana3        DIM register ID:7               2
 */
    static uint32_t NEXT_ID[] = { 1, 4, 0, 8, 5, 6, 7, 2 };


    qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_PREVIOUS_CYCLE] = qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE];    // Move DIM present history
    qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE] = 0x00000000;                 // Reset DIM present mask
    qspi_misc.flat_qspi_board_is_valid_packed = 0x00000000;

    board_number = 0;

    while ( board_number < FGC_MAX_DIM_BUS_ADDR )
    {
        // Extract register ID number from top nibble

        ana_regs_id[0] = (qspi_misc.analog_regs_from_all_dim_boards[0][board_number] >> 12) & 0x00000007; // register analog_0 of DIM board
        ana_regs_id[1] = (qspi_misc.analog_regs_from_all_dim_boards[1][board_number] >> 12) & 0x00000007; // register analog_1 of DIM board
        ana_regs_id[2] = (qspi_misc.analog_regs_from_all_dim_boards[2][board_number] >> 12) & 0x00000007; // register analog_2 of DIM board
        ana_regs_id[3] = (qspi_misc.analog_regs_from_all_dim_boards[3][board_number] >> 12) & 0x00000007; // register analog_3 of DIM board

        // If the analog registers IDs are ok we assume that a DIM board is detected
        if (    ( ana_regs_id[1] == NEXT_ID[ana_regs_id[0]] )
             && ( ana_regs_id[2] == NEXT_ID[ana_regs_id[1]] )
             && ( ana_regs_id[3] == NEXT_ID[ana_regs_id[2]] )
           )
        {
            // bit position corresponding to this board, when packed in uint32_t
            bit_in_packed = 0x00000001U << board_number;

            // Mark DIM as present on this cycle
            qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE] |= bit_in_packed;

            // If was present in previous cycle
            if ( qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_PREVIOUS_CYCLE] & bit_in_packed )
            {
                // if ID of trigCounter is also correctly 2
                if ( ( qspi_misc.analog_regs_from_all_dim_boards[4][board_number] & 0x7000) == 0x2000 )
                {
                    qspi_misc.flat_qspi_board_is_valid_packed |= bit_in_packed;
                }
                else // else channel is not correct
                {
                    dpcom->dsp.diag.reset_req_packed |= bit_in_packed; // Request a DIAG reset
                    qspi_misc.flat_qspi_non_valid_counts[board_number]++;
                }
            }
        }

        board_number += scan_step;
    }



    dpcom->dsp.diag.dips_are_valid = (qspi_misc.flat_qspi_board_is_valid_packed & DIPS_MASK) == DIPS_MASK;



    // Count number of DIMs detected (excluding DIPS)

    if ( scan_step == 1 )       // not class 59
    {
        // a DIM must be seen on two cycles to be present
        qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] =   qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE]
                                                     & qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_PREVIOUS_CYCLE];

        // exclude DIPS boards, A[0] and B[0]
        present_boards_packed[BRANCH_B] = (qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] >> 17);     // DIMs on branch B (17-31)
        present_boards_packed[BRANCH_A] = (qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] >>  1) & 0x00007FFF;    // DIMs on branch A ( 1-15)

        boards_in_qspi[BRANCH_A] = boards_in_qspi[BRANCH_B] = 0;

        while ( present_boards_packed[BRANCH_B] | present_boards_packed[BRANCH_A] )         // While bits still left to count
        {
            boards_in_qspi[BRANCH_A] += (present_boards_packed[BRANCH_A] & 0x00000001);
            boards_in_qspi[BRANCH_B] += (present_boards_packed[BRANCH_B] & 0x00000001);

            present_boards_packed[BRANCH_A] >>= 1; // next board
            present_boards_packed[BRANCH_B] >>= 1; // next board
        }

        qspi_misc.detected[BRANCH_A] = boards_in_qspi[BRANCH_A];        // Store number of DIMs in property
        qspi_misc.detected[BRANCH_B] = boards_in_qspi[BRANCH_B];

        // Check for number of DIMs detected not equal to number expected

        if (    (qspi_misc.detected[BRANCH_A] != qspi_misc.expected[BRANCH_A])
             || (qspi_misc.detected[BRANCH_B] != qspi_misc.expected[BRANCH_B])
           )
        {
            qspi_misc.expected_detected_mismatches++;   // this one can overflow
            qspi_misc.mismatches_level++;

            if ( qspi_misc.mismatches_level > MISMATCHES_FAULT_THRESHOLD )
            {
                rtcom.st_latched |= FGC_LAT_DIMS_EXP_FLT;
            }
        }
        else
        {
            qspi_misc.mismatches_level = 0;
        }

        dpcom->dsp.diag.dims_are_valid = (    (qspi_misc.detected[BRANCH_A] == qspi_misc.expected[BRANCH_A])
                                           && (qspi_misc.detected[BRANCH_B] == qspi_misc.expected[BRANCH_B])
                                           && (qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] == qspi_misc.flat_qspi_board_is_valid_packed) );
    }

    // Debug logging

#if DEBUG_LOG_DIPS == 1
    if ( dpcom->dsp.diag.reset_req_packed & DIPS_MASK ) // If reset request due to DIPS board
    {
        dsp_debug.stop_log = true;          // Freeze debug logging
    }
#else
    if(dpcom->dsp.diag.reset_req_packed & 0x00000002)   // If reset request due to first DIM on bus A
    {
        dsp_debug.stop_log = true;          // Freeze debug logging
    }
#endif

    if ( dsp_debug.samples_to_acq )         // If debug logging active
    {
#if DEBUG_LOG_DIPS == 1
        // DIPS board

        // register analog_0 of DIM board, flat[ 0] = A[0]
        // register analog_0 of DIM board, flat[16] = B[0]
        SaveInDspDebugBuffer( (  qspi_misc.analog_regs_from_all_dim_boards[0][0] << 16)
                               | qspi_misc.analog_regs_from_all_dim_boards[0][16]);

        // register analog_1 of DIM board, flat[ 0] = A[0]
        // register analog_1 of DIM board, flat[16] = B[0]
        SaveInDspDebugBuffer( (  qspi_misc.analog_regs_from_all_dim_boards[1][0] << 16)
                               | qspi_misc.analog_regs_from_all_dim_boards[1][16]);

        // register analog_2 of DIM board, flat[ 0] = A[0]
        // register analog_2 of DIM board, flat[16] = B[0]
        SaveInDspDebugBuffer( (  qspi_misc.analog_regs_from_all_dim_boards[2][0] << 16)
                               | qspi_misc.analog_regs_from_all_dim_boards[2][16]);

        // register analog_3 of DIM board, flat[ 0] = A[0]
        // register analog_3 of DIM board, flat[16] = B[0]
        SaveInDspDebugBuffer( (  qspi_misc.analog_regs_from_all_dim_boards[3][0] << 16)
                               | qspi_misc.analog_regs_from_all_dim_boards[3][16]);

        // register trigCounter of DIM board, flat[ 0] = A[0]
        SaveInDspDebugBuffer( (  qspi_misc.analog_regs_from_all_dim_boards[4][0] << 16)
                               | qspi_misc.analog_regs_from_all_dim_boards[4][16]);
#else
    // First DIM on bus A

        // register analog_0 of DIM board, flat[ 1] = A[1]
        // register analog_1 of DIM board, flat[ 1] = A[1]
        SaveInDspDebugBuffer( (  qspi_misc.analog_regs_from_all_dim_boards[0][1] << 16)
                               | qspi_misc.analog_regs_from_all_dim_boards[1][1]);

        // register analog_2 of DIM board, flat[ 1] = A[1]
        // register analog_3 of DIM board, flat[ 1] = A[1]
        SaveInDspDebugBuffer( (  qspi_misc.analog_regs_from_all_dim_boards[2][1] << 16)
                               | qspi_misc.analog_regs_from_all_dim_boards[3][1]);

        // register trigCounter of DIM board, flat[ 1] = A[1]
        SaveInDspDebugBuffer( (  qspi_misc.analog_regs_from_all_dim_boards[4][1] << 16)
                               | qspi_misc.mismatches_level);
        SaveInDspDebugBuffer(rtcom.time_now_ms.unix_time);
        SaveInDspDebugBuffer(rtcom.time_now_ms.ms_time | (boards_in_qspi[BRANCH_A] << 16));
#endif
        SaveInDspDebugBuffer(dpcom->dsp.diag.reset_req_packed);
        SaveInDspDebugBuffer(qspi_misc.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE]);
        SaveInDspDebugBuffer(rtcom.st_latched);
    }
}



void InIrqProcessQSPIbusDataSlice(void)
{
    uint32_t  xx;
    uint32_t  us_time;
    uint32_t  qspi_twice_data;
    uint32_t *analog_regs_p;
    uint32_t *source_p;         // Point to MCU buffer containing QSPI raw data
                              // In DSP qspi[A,B][0..15] is handled as [A,B][0..7] because the DSP fake uint16_t

    // Act according to current position in 20ms cycle

    switch( rtcom.ms20 )
    {
    case 1:

        LookForTriggerEvent();
        break;

    case 2:

        if ( dpcom->dsp.diag.dips_are_valid )
        {
            VerifyPsuVoltages();
        }

        ProcessLogRequest_8samples();
        break;

    case 4:  // Store Ana0          from 16 DIM boards on branch A (collected by the MCU on ms 3)
    case 5:  // Store Ana1          from 16 DIM boards on branch A (collected by the MCU on ms 4)
    case 6:  // Store Ana2          from 16 DIM boards on branch A (collected by the MCU on ms 5)
    case 7:  // Store Ana3          from 16 DIM boards on branch A (collected by the MCU on ms 6)
    case 8:  // Store TrigCounter   from 16 DIM boards on branch A (collected by the MCU on ms 7)

    case 14: // Store Ana0          from 16 DIM boards on branch B (collected by the MCU on ms 13)
    case 15: // Store Ana1          from 16 DIM boards on branch B (collected by the MCU on ms 14)
    case 16: // Store Ana2          from 16 DIM boards on branch B (collected by the MCU on ms 15)
    case 17: // Store Ana3          from 16 DIM boards on branch B (collected by the MCU on ms 16)
    case 18: // store TrigCounter   from 16 DIM boards on branch B (collected by the MCU on ms 17)

        // This is in flat_qspi array format, branch A starts at 0, branch B starts at 16

        analog_regs_p = &qspi_misc.analog_regs_from_all_dim_boards[rtcom.ms10 - 4][16 * (rtcom.ms20 / 10) ];

        // In DSP qspi[A,B][0..15] is handled as [A,B][0..7] because the DSP fake uint16_t
        // Select correct buffer A or B
        // source_p will be pointing to qspi[branch][register according to ms]

        source_p = (uint32_t*)&dpcom->mcu.diag.qspi[1 - (rtcom.ms10 & 1)];

        // we do 8 uint32_t same as 16 uint16_t, so 16 DIM boards belonging to this actual branch (A or B)

        for( xx = 0; xx < 8; xx++ )
        {
            // Data comes packed as register of two DIM boards (uint16_t) in 1 uint32_t

            qspi_twice_data = *(source_p++);

            // store DIM register in high word
            *(analog_regs_p++) = (qspi_twice_data >> 16);
            // store DIM register in low word
            *(analog_regs_p++) = (qspi_twice_data & 0xFFFF);
        }

        // After last transfer (ms 18)

        if ( rtcom.ms20 == 18 )
        {
            // Check for DIMs present and valid

            ValidatePresentDimBoards(1);
        }
        else
        {
            ProcessLogRequest_8samples();
        }

        break;

    case 11:

        // Time of 20ms cycle within second

        us_time = rtcom.time_now_us.us_time - 11000;

        // Save prev unix time
        qspi_misc.prev_unix_time = qspi_misc.unix_time;
        // Unix time for this cycle
        qspi_misc.unix_time      = rtcom.time_now_us.unix_time;

        // Save prev scan times

        qspi_misc.prev_scantime_us[BRANCH_A] = qspi_misc.scantime_us[BRANCH_A];
        qspi_misc.prev_scantime_us[BRANCH_B] = qspi_misc.scantime_us[BRANCH_B];

        qspi_misc.scantime_us[BRANCH_A] = dpcom->mcu.diag.scantime_tcnt[BRANCH_A] / 2 + us_time;
        qspi_misc.scantime_us[BRANCH_B] = dpcom->mcu.diag.scantime_tcnt[BRANCH_B] / 2 + us_time + 10000;

        ProcessLogRequest_8samples();
        break;

    case 19:

        // Process and log raw values

        ScaleDimAnalogueData();
        break;

    case 3:
    case 9:
    case 12:
    case 13:

        ProcessLogRequest_8samples();
        break;

    default:
        break;
    }
}



// EOF
