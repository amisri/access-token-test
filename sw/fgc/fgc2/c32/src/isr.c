/*---------------------------------------------------------------------------------------------------------*\
  File:         isr.c

  Purpose:      FGC Software - contains Isr functions

  Author:       Quentin.King@cern.ch

\*---------------------------------------------------------------------------------------------------------*/

#define ISR_GLOBALS

#include <isr.h>
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <rtcom.h>              // for rtcom global variable
#include <ref.h>
#include <reg.h>                // for reg, load global variables
#include <meas.h>               // for meas global variable
#include <refto.h>              // for RefToEnd()
#include <main.h>               // for TimeIteration()
#include <adc.h>                // for AdcRead(), AdcFilter(), adc_chan[]
#include <cyc_class.h>          // for CycBIVchecks()
#include <report.h>             // for Report()
#include <cyc.h>                // for CycCaptureLog()
#include <sim.h>                // for SimNextRegState()
#include <macros.h>             // for Set(), Clr(), Test()
#include <cal.h>                // for cal global variable
#include <dsp_panic.h>          // for DspPanic()
#include <definfo.h>            // for FGC_CLASS_ID
#include <mcu_dsp_common.h>
#include <dsp_dependent.h>      // For DAC_SET()
#include <pars.h>
#include <dsp_panic.h>
#include <ppm.h>                // for ppm[]

#if FGC_CLASS_ID == 53
    #include <pal.h>            // for PalLinkReceive(), PalBVsignals()
#endif

/*---------------------------------------------------------------------------------------------------------*/
void IsrMain(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used by Class 51/53/61 for the main part of the real-time ISR processing
\*---------------------------------------------------------------------------------------------------------*/
{
    // Process time and timing events

    TimeIteration();

#if FGC_CLASS_ID == 53
    TimeRunning();
    TimeCyclingEvents();
    TimeCycling();
#else
    TimeRunningEvents();
    TimeRunning();
#endif

    // Get and process signals from PAL/PBL and ADCs

    AdcRead();
    AdcFilter();
    MeasSelect();


#if FGC_CLASS_ID == 53
    PalLinkReceive();
    PalBVsignals();
#else
    if(rtcom.ms20 == 19)                                // Every ms 19 of 20
    {
            MeasDiag();                                 // Process Diag signals
    }
#endif

    // Estimate rate of change of field, current and voltage based on last four samples

    MeasHistory(&meas.i_hist, meas.i);
    MeasHistory(&meas.b_hist, meas.b);

    // Estimate the rate of change of the current on the last four regulation periods

    if(dpcls->dsp.reg.run_f)
    {
        MeasHistoryRegPeriod(&meas.i_hist_reg, meas.i_hist.meas);
    }

#if FGC_CLASS_ID == 53
    // Perform start of cycle Field-Current-Voltage checks when cycling

    CycBIVchecks();
#endif

    if(rtcom.state_op == FGC_OP_TEST)
    {
        IsrDacDirect(property.ref.dac);                   // Set DAC value from REF.DAC property
    }
    else
    {
        // If not in TEST mode then process according to regulation state

        switch(reg.active->state)
        {
#if (FGC_CLASS_ID == 53)
        case FGC_REG_B:

            if(dpcls->dsp.reg.run_f)
            {
                IsrRegStateB();
            }
            break;
#endif

        case FGC_REG_I:

            if(dpcls->dsp.reg.run_f)
            {
                IsrRegStateI();
            }
            break;

        case FGC_REG_V:

            IsrRegStateV();
            break;
        }

        // Calculate V error (This is an average over a regulation period)

        ref.v_err_avg = regErrCalc(&reg_v_err, true, ref.v.value, meas.v);

        // Send V_REF to the voltage source
        {
            FP32 ref_val = ref.v.value;

            IsrDacSet((ref_val - property.vs.offset) / property.vs.gain);
        }

    }

    // Report data to the MCU on the start of each ms

    if(rtcom.start_of_ms_f)
    {
        Report();
    }

    // Logging

    IsrLogging();

    // Cycle Check - VMEAS_VREF : Voltage regulation fault

    if(ref.cycling_f && reg_v_err.flags.fault)
    {
        CycSaveCheckFault(FGC_CYC_FLT_VMEAS_VREF, FGC_WRN_V_ERROR, 0.0, 0.0, 0.0, 0.0);
    }

    // Shift RST history and get the filtered voltage reference (= average of the actuation)

    if(dpcls->dsp.reg.run_f)
    {
        ref.v_fltr = regRstHistory(&reg_vars);
    }

#if FGC_CLASS_ID == 53
    // PAL reset request down counter from N to 2 (it is checked by main())

    if(dpcls->dsp.pal.reset_req > 2)
    {
        dpcls->dsp.pal.reset_req--;
    }
#else
    // Write to SPY interface unless STATE.PC == ARMED (to allow synchronisation)

    if(ref.func_type != FGC_REF_ARMED && rtcom.start_of_ms_f)
    {
        MeasSpySend();
    }
#endif

    // Simulate Vmeas, Imeas and Bmeas for next iteration

    SimNextRegState(ref.v.value);
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrRegStateV(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when in voltage regulation state.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Calculate voltage reference

    Ref();

    // Apply limits to voltage reference

    ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, ref.fg_plus_rt.value, ref.v.value);

    RefRefreshValue(&ref.v);            // Compute Vref rate

    // Manage REF_LIM and REF_RATE_LIM warnings

    if(lim_v_ref.flags.clip && state_pc != FGC_PC_CYCLING)
    {
        Set(rtcom.warnings, FGC_WRN_REF_LIM);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_REF_LIM);
    }

    if(lim_v_ref.flags.rate && state_pc != FGC_PC_CYCLING)
    {
        Set(rtcom.warnings, FGC_WRN_REF_RATE_LIM);
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_REF_RATE_LIM);
    }

    // In simulation mode, the simulated voltage (meas.v) is copied from the RTS history. Therefore even in V regulation,
    // one needs to keep the RST history updated.

    reg_vars.act [0] = ref.v.value;
    reg_vars.meas[0] = meas.i;
    reg_vars.ref [0] = meas.i;
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrRegStateI(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when in current regulation state.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 v_ref;         // Vref, not accounting for load saturation
    FP32 v_ref_sat;     // Vref,     accounting for load saturation

    // Calculate current reference

    Ref();

    // Regulate current

    if(!TestRegBypass())
    {
        // ------------------------------------------------------------------------------------------------
        // Regulate current based on reference
        // ------------------------------------------------------------------------------------------------

        // Clip the reference. ref.i_clipped:  clipped reference;
        //                     ref.fg_plus_rt: unclipped reference
        //
        // Special case: the PLEP_NO_CLIP function for which no rate clipping is applied

        if(ref.func_type != FGC_REF_PLEP_NO_CLIP)
        {
            ref.i_clipped.value = regLimRef(&lim_i_ref, reg.active->rst_pars.period, ref.fg_plus_rt.value, ref.i_clipped.value);
        }
        else
        {
            ref.i_clipped.value = regLimRef(&lim_i_ref, reg.active->rst_pars.period, ref.fg_plus_rt.value, ref.fg_plus_rt.value);     // No rate clipping
        }

        RefRefreshValue(&ref.i_clipped);        // Compute rate of change.

        // Calculate Vref

        v_ref = regRstCalcAct(&reg.active->rst_pars, &reg_vars, ref.i_clipped.value, meas.i);

        v_ref_sat = regLoadVrefSat(&load.active->pars, meas.i, v_ref);

        ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, v_ref_sat, ref.v.value);

        if(lim_v_ref.flags.clip || lim_v_ref.flags.rate)
        {
            v_ref = regLoadInverseVrefSat(&load.active->pars, meas.i, ref.v.value);

            ref.i.value = regRstCalcRef(&reg.active->rst_pars, &reg_vars, v_ref, meas.i);

            lim_i_ref.flags.rate = true;
        }
        else
        {
            ref.i.value = ref.i_clipped.value;
        }

        RefRefreshValue(&ref.i);                // Compute rate of change.
        RefRefreshValue(&ref.v);                // Compute rate of change.

        // Calculate the error between the measurement and the clipped reference

        regErrCalc(&reg_bi_err, (ref.time.s >= reg.max_abs_err_enable_time), ref.i_clipped.value, meas.i);

#if (FGC_CLASS_ID == 51)
        // Unipolar descent to standby, check for Vref hitting zero

        if(lim_v_ref.flags.unipolar  &&                 // If 1-Q converter and
           ref.func_type == FGC_REF_TO_STANDBY &&       // ramping to standby and
           ref.v.value <= 0.0)                          // Vref has fallen below zero
        {
            RefToStandbyToOpenloop();                           // Switch to OPENLOOP to finish ramp down
        }
#endif

        // Cycle Check - REG_ERR : Regulation fault

        if(ref.cycling_f && reg_bi_err.flags.fault)
        {
            CycSaveCheckWarning(TRIGG_LOG, FGC_CYC_WRN_REG_ERR, FGC_WRN_REG_ERROR, 0.0, 0.0, 0.0, 0.0);
            CycLogAbsMaxErr();
            refto.flags.end = true;             // Will call RefToEnd() on the next regulation period
        }

        // Manage REF_LIM and REF_RATE_LIM warnings (clipped reference)

        if(lim_i_ref.flags.clip && state_pc != FGC_PC_CYCLING)
        {
            Set(rtcom.warnings, FGC_WRN_REF_LIM);
        }
        else
        {
            Clr(rtcom.warnings, FGC_WRN_REF_LIM);
        }

        if(lim_i_ref.flags.rate && state_pc != FGC_PC_CYCLING)
        {
            Set(rtcom.warnings, FGC_WRN_REF_RATE_LIM);
        }
        else
        {
            Clr(rtcom.warnings, FGC_WRN_REF_RATE_LIM);
        }
    }
    else
    {
        // ------------------------------------------------------------------------------------------------
        // Use openloop voltage reference and calculate current reference
        // ------------------------------------------------------------------------------------------------

        v_ref_sat = regLoadVrefSat(&load.active->pars, meas.i, ref.v_openloop);

        ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, v_ref_sat, ref.v.value);

        v_ref = regLoadInverseVrefSat(&load.active->pars, meas.i, ref.v.value);

        // Calculate the equivalent current reference: ref.v --> ref.i = ref.i_clipped = ref.fg_plus_rt --> ref.fg
        // The RST history gets updated with the calculated reference.

        ref.i.value          = regRstCalcRef(&reg.active->rst_pars, &reg_vars, v_ref, meas.i);
        ref.fg_plus_rt.value = ref.i_clipped.value = ref.i.value;
        ref.fg.value         = ref.fg_plus_rt.value - ref.rt;

        RefRefreshValue(&ref.fg);               // Compute rate of change.
        RefRefreshValue(&ref.fg_plus_rt);       // Compute rate of change.
        RefRefreshValue(&ref.i_clipped);        // Compute rate of change.
        RefRefreshValue(&ref.i);                // Compute rate of change.
        RefRefreshValue(&ref.v);                // Compute rate of change.

        regErrCalc(&reg_bi_err, false, ref.i_clipped.value, meas.i);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrRegStateB(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when in field regulation state.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 v_ref;

    // Calculate field reference

    Ref();

    // Regulate field

    if(!TestRegBypass())
    {
        // ------------------------------------------------------------------------------------------------
        // Regulate field using field reference
        // ------------------------------------------------------------------------------------------------

        // Clip the reference. ref.b: clipped reference; ref.fg_plus_rt: unclipped reference

        ref.b.value = ref.fg_plus_rt.value;   // No field reference limits required since no real-time control

        // Calculate the error between the measurement and the clipped reference

        regErrCalc(&reg_bi_err, (ref.time.s >= reg.max_abs_err_enable_time), ref.b.value, meas.b);

        // Calculate Vref

        v_ref = regRstCalcAct(&reg.active->rst_pars, &reg_vars, ref.b.value, meas.b);

        ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, v_ref, ref.v.value);

        if(lim_v_ref.flags.clip || lim_v_ref.flags.rate)
        {
            // Back calculate the field reference value corresponding to the clipped reference voltage

            ref.b.value = regRstCalcRef(&reg.active->rst_pars, &reg_vars, ref.v.value, meas.b);
        }

        RefRefreshValue(&ref.b);                // Compute rate of change.
        RefRefreshValue(&ref.v);                // Compute rate of change.

        // Cycle Check - REG_ERR : Regulation fault

        if(reg_bi_err.flags.fault)
        {
            CycSaveCheckWarning(TRIGG_LOG, FGC_CYC_WRN_REG_ERR, FGC_WRN_REG_ERROR, 0.0, 0.0, 0.0, 0.0);
            CycLogAbsMaxErr();
            refto.flags.end = true;     // Will call RefToEnd() on the next regulation period
        }
    }
    else
    {
        // ------------------------------------------------------------------------------------------------
        // Use openloop voltage reference
        // ------------------------------------------------------------------------------------------------

        ref.v.value = regLimRef(&lim_v_ref, reg.active->rst_pars.period, ref.v_openloop, ref.v.value);

        RefRefreshValue(&ref.v);                // Compute rate of change.

        ref.fg.value = ref.fg_plus_rt.value = ref.b.value = 0.0;

        // rates of change will all be zero

        //RefRefreshValue(&ref.fg_plus_rt);       // Compute rate of change.
        //RefRefreshValue(&ref.fg);               // Compute rate of change.
        //RefRefreshValue(&ref.b);                // Compute rate of change.

        // B error = 0.0

        regErrCalc(&reg_bi_err, false, 0.0, 0.0);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrDacSet(FP32 v_dac)
/*---------------------------------------------------------------------------------------------------------*\
  This function converts the DAC voltage parameter into a raw value to be send to the DAC by the MCU
  at the start of the next millisecond.
  If there is an on-going calibration of the DAC, the DAC value is not modified.
\*---------------------------------------------------------------------------------------------------------*/
{
    ref.dac_raw = calDacSet(&cal.active->dac_factors, v_dac);

#if FGC_CLASS_ID == 51
    // Set a DAC clamp value while in STARTING state.
    // For 1Q and 2Q thyristor converters on class 51 only (see documentation for property VS.DAC_CLAMP)

    if(state_pc == FGC_PC_STARTING &&               // If STATE.PC == STARTING and
       lim_i_ref.flags.unipolar  &&                 // 1Q or 2Q converter and
       rtcom.state_op == FGC_OP_NORMAL &&           // OP state is normal and
       meas.i < limits.i_start)                     // current is less than min for VLOOP
    {
        ref.dac_raw = calDacSet(&cal.active->dac_factors, property.vs.dac_clamp);
    }
#endif

    if(!cal.ongoing_dac_cal)
    {
        DAC_SET(ref.dac_raw);

        property.ref.dac = ref.dac_raw;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrDacDirect(int32_t ref_dac)
/*---------------------------------------------------------------------------------------------------------*\
  This function processes the direct DAC reference from the MCU to add a scanning functionality to allow
  DAC testing.  The normal DAC range is effectively +/-524287, but this function support special
  processing if the user supplies a value >1000000 or less than -1000000.  In this case, the excess is
  used as an increment/decrement value.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (ref_dac >= 1000000)
    {
        ref.dac_raw += (ref_dac - 1000000);

        if (ref.dac_raw > cal.active->dac_factors.max_dac_raw)
        {
            ref.dac_raw = cal.active->dac_factors.min_dac_raw;
        }
    }
    else if (ref_dac <= -1000000)
    {
        ref.dac_raw += (ref_dac + 1000000);

        if (ref.dac_raw < cal.active->dac_factors.min_dac_raw)
        {
            ref.dac_raw = cal.active->dac_factors.max_dac_raw;
        }
    }
    else
    {
        ref.dac_raw = ref_dac;
    }

    DAC_SET(ref.dac_raw);
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrLogging()
/*---------------------------------------------------------------------------------------------------------*\
  This function handles the logging of the FGC signals (SPY, LOG.CAPTURE, LOG.OASIS).

  The following is mainly used on POPS: At the start of the first plateau of a PPPL reference, corresponding
  to the injection, a fake marker is inserted on the reference (+1A for a current reference, +1G for a field
  reference), lasting 1ms.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 restore_ref_now_val;

    restore_ref_now_val = ref.now->value;

    // First plateau +1G or +1A start marker in ref.now. The original ref.now value is restored at the end of this function

    if(ref.func_type == FGC_REF_PPPL &&
       DSP_ROUND_TIME_MS(cyc.time.s) == DSP_ROUND_TIME_MS(ppm[cyc.ref_user].cycling.first_plateau.time))
    {
        ref.now->value += 1.0;
    }

#if FGC_CLASS_ID == 53
    PalLinkSend();
    CycCaptureLog();
#endif

    ref.now->value = restore_ref_now_val;
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrTrap(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used as the target for all unexpected interrupts
\*---------------------------------------------------------------------------------------------------------*/
{
    DspPanic(DSP_PANIC_ISR_TRAP);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.c
\*---------------------------------------------------------------------------------------------------------*/
