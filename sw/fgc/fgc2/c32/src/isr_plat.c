/*---------------------------------------------------------------------------------------------------------*\
  File:     isr_plat.c

  Purpose:  FGC2 Software - contains IsrMst function

  Author:   Quentin.King@cern.ch

  Notes:    The FGC uses only one of the C32's four external interrupt inputs: int2.

                This code was used to drive the FGC DAC with any signal - for POPS it is
                very unlikely to be used so it has been removed.

                  IsrDacSet((20.0 * MeasSpyMpx(property.spy.dac.mpx)
                          - (property.spy.dac.pos10v + property.spy.dac.neg10v))
                          / (property.spy.dac.pos10v - property.spy.dac.neg10v));

\*---------------------------------------------------------------------------------------------------------*/

#define BGCOM_GLOBALS           // define bgcom global variable
#define ISR_PLAT_GLOBALS        // define isr global variable

#include <isr_plat.h>
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <rtcom.h>              // for rtcom global variable
#include <main.h>               // for state_pc global variable
#include <bgcom.h>              // for bgcom global variable


#include <lib.h>                // for
#include <sim.h>                // for sim global variable, needed by defprops_dsp_fgc.h
#include <property.h>
#include <defprops_dsp_fgc.h>   // for PROP_SPY_MPX

#include <isr.h>                // for IsrMain()
#include <dsp_dependent.h>      // for LED0_TOGGLE
#include <cal.h>                // for CalCalc(), CalAuto()
#include <state.h>              // for StatePc(), StateOp()
#include <adc.h>                // for AdcMpx()
#include <pars.h>               // for ParsGroupSet()
#include <diag.h>               // for InIrqProcessQSPIbusDataSlice()

#include <timer30.h>            // for TIMER_ADDR()

// ToDo : try to clean this, here is DSP so no MCU memory map
#include <memmap_mcu.h>         // for CPU_MODE_C32IRQ_2_MASK16

/*---------------------------------------------------------------------------------------------------------*/
void IsrMst(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the Interrupt Service Routine for the C32 int2 external interrupt request.  This will be
  triggered at the start of each millisecond (as defined by the MCU GPT OC2 clock).  It is responsible for
  all the millisecond level real-time processing of the FGC.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Acknowledge interrupt

    TIMER_ADDR(0)->gcontrol |= (HLD_|GO);                   // Restart 8MHz timer from zero.

    dpcom->dsp.cpu_usage     = rtcom.cpu_usage;             // Return CPU usage for previous millisecond
    TICTAC                   = CPU_MODE_C32IRQ_2_MASK16;    // Acknowledge MCU interrupt
    bgcom.run_f              = true;                        // Signal to background proc to run after ISR

    dpcom->dsp.ms_irq_alive_counter.int32u++;

    // Main Real-Time Processing function (class dependent)

    if(DSP_INITIALISED)
    {
        IsrMain();
    }

    // Process one cal/state/parameter request (avoiding 10ms boundaries)

    if ( !rtcom.start_of_ms10_f )
    {
        if(rtcom.time_now_ms.ms_time == 1)
        {
            TemperatureCompensation();                      // Temperature compensation for internal ADCs and DCCTs
            isr.trigger.calibration_refresh = true;         // Trigger an update of the calibration factors, based on the new temperature
        }
        else if(rtcom.ms200 == 2 && dpcls->mcu.cal.req_f && !dpcls->dsp.cal_complete_f)
        {
            if(true == CalAuto())
            {
                // Calibration process has completed

                dpcls->mcu.cal.req_f      = false;
                dpcls->dsp.cal_complete_f = true;
                cal.step_200ms            = 0;              // Reset step counter for the next calibration process
            }
        }
        else if(state_pc != dpcls->mcu.state_pc)
        {
            StatePc(dpcls->mcu.state_pc);
        }
        else if(dpcls->mcu.adc_mpx_f)
        {
            AdcMpx();
        }
        else if(rtcom.state_op != dpcom->mcu.state_op)
        {
            StateOp(dpcom->mcu.state_op);
        }
        else if(rtcom.par_group_set_mask)
        {
            ParsGroupSet();
        }
        else if(isr.trigger.calibration_refresh)            // Calibration properties have changed
        {
            CalCalc();                                      // Calculate updated calibration factors

            isr.trigger.calibration_refresh = false;
        }

        if ( rtcom.time_now_us.unix_time != 0 )             // If unix_time is known
        {
            InIrqProcessQSPIbusDataSlice();
        }

        // Toggle LED 0 on development frontpanel (if connected) at about 0.5Hz

        rtcom.led_counter++;
        if ( !(rtcom.led_counter & 0x00FF) )
        {
            LED0_TOGGLE;
        }
    }

    // Calculate time taken by ISR on this iteration

    rtcom.cpu_usage = (TIMER_ADDR(0)->counter + 40) / 80;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr_plat.c
\*---------------------------------------------------------------------------------------------------------*/
