/*---------------------------------------------------------------------------------------------------------*\
  File:         state.c

  Purpose:      FGC C32 Software - PC state change functions

  Notes:        These are only called if STATE.PC has changed
\*---------------------------------------------------------------------------------------------------------*/

#include <state.h>
#include <props.h>      // for property global variable
#include <dpcls.h>      // for dpcls global variable
#include <rtcom.h>      // for rtcom global variable
#include <reg.h>        // for reg, load, load global variables
#include <main.h>       // for state_pc global variable
#include <meas.h>       // for meas global variable
#include <refto.h>      // for RefToStopping()
#include <macros.h>     // for Test()
#include <bgp.h>        // for BgpInitNone()
#include <cyc.h>        // for STRETCHED_CYC_WARNINGS
#include <pars.h>       // for ParsMeasSim()
#include <definfo.h>    // for FGC_CLASS_ID
#include <refto.h>
#include <dsp_dependent.h>
#include <defprops_dsp_fgc.h>
#include <mcu_dsp_common.h>

extern  void    ParsLoadSelect  (void);         // Required for Class 53

/*---------------------------------------------------------------------------------------------------------*/
void StateFS_FO_OF(void)                // FLT_STOPPING, FLT_OFF, OFF
/*---------------------------------------------------------------------------------------------------------*/
{
    RegStateV(true, 0.0);

    // Reset property REF.ABORT if the abort time is in the future,
    // but keep a value in the past as it indicates the time of the last abort.

    if(AbsTimeUs_IsGreaterThan(property.ref.abort, rtcom.time_now_us))
    {
        property.ref.abort.unix_time  = 0;
        property.ref.abort.us_time    = 0;
    }
    ref.abort.unix_time           = TIME_NEVER;         // Reset the mirror value ref.abort
}
/*---------------------------------------------------------------------------------------------------------*/
void StateSP(void)                      // STOPPING
/*---------------------------------------------------------------------------------------------------------*/
{
    if(REF_STOPPING_REQUIRED &&                         // If a stopping reference required and
      (state_pc == FGC_PC_ON_STANDBY ||                 // PC state is ON_STANDBY or
       state_pc == FGC_PC_TO_STANDBY))                  // TO_STANDBY
    {
        refto.flags.stopping = true;                    // Run STOPPING reference function
    }
    else                                                //else
    {
        StateFS_FO_OF();                                        // Jump to OF
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateST(void)                      // STARTING
/*---------------------------------------------------------------------------------------------------------*/
{
    RegStateV(true, 0);
    Clr(rtcom.warnings, STRETCHED_CYC_WARNINGS);
}
/*---------------------------------------------------------------------------------------------------------*/
void StateSA(void)                      // SLOW_ABORT
/*---------------------------------------------------------------------------------------------------------*/
{
    if(reg.active->state == FGC_REG_V)          // Open-loop
    {
        if(state_pc < FGC_PC_TO_STANDBY ||                 // If previous PC state is < TO_STANDBY or
           (lim_i_ref.flags.unipolar &&                    // 1/2-Quadrant converter and
            meas.i <= property.limits.op.i.min))           // current below I_MIN
        {
            RegStateV(true, 0.0);                             // Reset Vref
        }
    }
    else                                        // Closed loop
    {
        // Set refto.flags.standby, refto.flags.stopping or both. E.g. on a 2Q converter, if in IDLE state, both flags
        // are set, which means that the FGC will run a TO_STANDBY reference followed by a STOPPING reference

        if(state_pc > FGC_PC_ON_STANDBY)                // If previous PC state is > ON_STANDBY
        {
            refto.flags.standby = true;                                // Ref() will call RefToStandby()
        }

        if(REF_STOPPING_REQUIRED &&                     // If a stopping reference required and
           state_pc >= FGC_PC_TO_STANDBY)               // PC state is >= TO_STANDBY
        {
            refto.flags.stopping = true;                               // Ref() will call RefToStopping()
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateTS(void)                      // TO_STANDBY
/*---------------------------------------------------------------------------------------------------------*/
{
    FP32        v_ref_sb;

    uint32_t      reg_state = dpcls->mcu.ref.func.reg_mode[0];

    if(reg_state != FGC_REG_V)                  // If current loop is to be enabled
    {
        if(state_pc < FGC_PC_SLOW_ABORT)        // If starting
        {
            RegStateRequest(false, true, reg_state, 0.0);

            if(lim_i_ref.flags.unipolar)        // if unipolar circuit
            {
                ref_fg.starting.openloop_f = true;
                ref_fg.starting.delay      = FGC_N_RST_COEFFS * reg.active->rst_pars.period;
                ref_fg.starting.i_rate     = property.ref.rate.i.starting[property.load.select];

                ref.time_remaining.s              = (20.0 + ref_fg.starting.delay + 2.0 * limits.i_closeloop / ref_fg.starting.i_rate);
                ref.time_remaining.period_counter = DSP_CEIL_TIME_VP(ref.time_remaining.s);

                if(RESISTIVE_LOAD)                              // If fast load (Tc <= 1s)
                {
                    ref_fg.starting.v_rate = ref_fg.starting.i_rate * load.active->pars.ohms;
                }
                else                                            // else Tc > 1s
                {
                    // Coefficients are chosen to take ~6s to ramp to I_START

                    ref_fg.starting.v_rate = limits.i_start * (0.05 * load.active->pars.henrys + 0.2 * load.active->pars.ohms);
                }
            }
            else                                                // else bipolar circuit
            {
                ref_fg.starting.v_rate= 0.0;                    // No voltage ramp
                ref_fg.starting.delay = 7.0;                    // Start delay of 7s

                ref.time_remaining.s              = ref_fg.starting.delay + 0.5;
                ref.time_remaining.period_counter = DSP_CEIL_TIME_VP(ref.time_remaining.s);
            }

            ref.func_type                 = FGC_REF_STARTING;   // Set ref type to START

            // Update RTD info, prepare meta data

            dpcls->dsp.ref.func_type      = FGC_REF_STARTING;
            dpcls->dsp.ref.stc_func_type  = dpcls->mcu.ref.stc_func_type;       // Should be equal to STC_STARTING
                                                                                // (that define is only known by the MCU)

            ppm[0].armed.meta.duration    = ref.time_remaining.s;
            ppm[0].armed.meta.range.start = 0.0;
            dpcls->dsp.ref.start          = ToIEEE(0.0);
            ppm[0].armed.meta.range.end   = limits.i_closeloop;
            dpcls->dsp.ref.end            = ToIEEE(limits.i_closeloop);
            ppm[0].armed.meta.range.min   = ppm[0].armed.meta.range.start;
            ppm[0].armed.meta.range.max   = ppm[0].armed.meta.range.end;

        }
        else if(state_pc > FGC_PC_ON_STANDBY)    // else (not starting) and if > ON_SB now
        {
            refto.flags.standby = true;         // Flag call RefToStandby
        }
    }
    else                                        // else current loop disabled
    {
#if FGC_CLASS_ID == 51
        v_ref_sb = (refto.flags.standby ? 0.0 : ref.v_fltr);
#else
        v_ref_sb = property.limits.op.i.min * load.active->pars.ohms;
#endif

        RegStateRequest(false, true, FGC_REG_V, v_ref_sb);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateSB_IL(void)                   // ON_STANDBY, IDLE
/*---------------------------------------------------------------------------------------------------------*/
{
    BgpInitNone(FGC_REG_V, 0);                  // Cancel reference. First argument is ignored
    ref.func_type = FGC_REF_NONE;
}
/*---------------------------------------------------------------------------------------------------------*/
void StateAB(void)                      // ABORTING
/*---------------------------------------------------------------------------------------------------------*\
  State AB on the MCU indicates that the user requested the abort via a "S PC IL" command.
  In this case, property REF.ABORT must be set by the FGC to show the actual time.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(ref.func_type != FGC_REF_ABORTING)       // If not already aborting
    {
        // Set REF.ABORT to time.now + 100 ms (keep a margin because ParsAbortTime will subtract the ref advance time).

        property.ref.abort = rtcom.time_now_us;

        AbsTimeUsAddDelay(&property.ref.abort, 100000);          // +100ms

        // Parse the modified REF.ABORT property as if the user did a "S REF.ABORT" command.

        ParsAbortTime();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateTC(void)                      // TO_CYCLING
/*---------------------------------------------------------------------------------------------------------*/
{
    CycEnter();
}
/*---------------------------------------------------------------------------------------------------------*/
void StatePc(uint32_t new_state_pc)
/*---------------------------------------------------------------------------------------------------------*\
  PC state parameter has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    static void (*state_func[])(void) =         // Ref gen functions
    {
        StateFS_FO_OF,                          // 0    FLT_OFF
        StateFS_FO_OF,                          // 1    OFF
        StateFS_FO_OF,                          // 2    FLT_STOPPING
        StateSP,                                // 3    STOPPING
        StateST,                                // 4    STARTING
        StateSA,                                // 5    SLOW_ABORT
        StateTS,                                // 6    TO_STANDBY
        StateSB_IL,                             // 7    ON_STANDBY
        StateSB_IL,                             // 8    IDLE
        StateTC,                                // 9    TO_CYCLING
        0,                                      // 10    ARMED
        0,                                      // 11    RUNNING
        StateAB,                                // 12    ABORTING
        0,                                      // 13    CYCLING
    };

    if(dpcls->mcu.mode_rt &&                    // If RT control is enabled, and
       new_state_pc <= FGC_PC_ON_STANDBY &&     // new PC state is ON_STANDBY or LOWER, and
           state_pc >  FGC_PC_ON_STANDBY)       // PC state was ABOVE ON_STANDBY
    {
        // Copy ref.fg_plus_rt to ref.fg, to avoid a bump in the reference.
        // The whole reference structure is copied, this is on purpose. The real-time reference ref.rt will
        // jump to 0.0 on the next regulation iteration after the transition to ON_STANDBY state (or lower),
        // so the new reference ref.fg must exactly match the last ref.fg_plus_rt (including rate of change,
        // estimated value etc.)

        ref.fg = ref.fg_plus_rt;
    }

    // If leaving CYCLING then run CycExit()

    if (state_pc == FGC_PC_CYCLING)
    {
        CycExit();                              // Exit from CYCLING
    }

    // Run state function for new PC state, if defined

    if (state_func[new_state_pc])
    {
        state_func[new_state_pc]();
    }

    state_pc = new_state_pc;
}
/*---------------------------------------------------------------------------------------------------------*/
void StateOp(uint32_t new_state_op)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from IsrPeriod() if the operational state has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    //  Class 53 doesn't include LOAD.SELECT so force setting of property.load.select once FGC is configured

    if (rtcom.state_op == FGC_OP_UNCONFIGURED)
    {
        ParsLoadSelect();
    }

    // Save new OP state and and process MEAS.SIM which depends on OP state

    rtcom.state_op = new_state_op;

    ParsMeasSim();
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: state.c
\*---------------------------------------------------------------------------------------------------------*/
