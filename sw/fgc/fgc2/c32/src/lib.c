/*---------------------------------------------------------------------------------------------------------*\
  File:     lib.c

  Purpose:  FGC DSP library functions - common to all FGC2 classes

  Notes:    Memory Maps for the different classes:

        +---------------------------------------------------------------+
        |           CLASS  51: PC_LHC           |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 00000 | 04000 | 08000 | 0C000 | 10000 | 14000 | 18000 | 1C000 |
        |       |       |               |
        |    DSP Prog   |       |               |
        |     (96KB)    |   Variables   |               |
        |       +-------+    (158KB)    |        DIM LOGS       |
        |   |       +-------|        (256KB)        |
        |   |       | 0FE00 |               |
        |   |       | Stack |               |
        |   |       | (2KB) |               |
        +-------+-------+-------+-------+-------+-------+-------+-------+

        +---------------------------------------------------------------+
        |           CLASS  53: POPS                 |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 00000 | 04000 | 08000 | 0C000 | 10000 | 14000 | 18000 | 1C000 |
        |       |       |               |
        |    DSP Prog   |       |               |
        |     (96KB)    |       |               |
        |       +-------+   Variables   |       CAPTURE LOG     |
        |   |        (160KB)    |        (256KB)        |
        |   |           |               |
        |   |           |               |
        |   |           |               |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 87FE00|
        | Stack |
        | (2KB) |
        +-------+

        +---------------------------------------------------------------+
        |           CLASS  59: RF_FGEN          |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 00000 | 04000 | 08000 | 0C000 | 10000 | 14000 | 18000 | 1C000 |
        |   |   |           |           |
        |   |   |           |           |
        |   |   |           |           |
        |  DSP  | Vars  | Function Time Vectors | Function Value Vectors|
        |  Prog | (64KB)|    (192KB)    |    (192KB)    |
        | (64KB)|   |           |           |
        |   |   |           |           |
        |   |   |           |           |
        +-------+-------+-------+-------+-------+-------+-------+-------+
        | 87FE00|
        | Stack |
        | (2KB) |
        +-------+

        The 2KB stack for classes 53 and 59 is in internal SRAM at 0x87FE00 as there is not a
                radiation problem for these systems and internal SRAM is faster than external SRAM.
                This is essential for the RF_FGEN class which is at the limit of processing resources.
\*---------------------------------------------------------------------------------------------------------*/

#include <lib.h>
#include <string.h>             // ANSI string library
#include <stdlib.h>             // ANSI stdlib library

#include <memmap_dsp.h>         // DSP memory map constants
#include <defconst.h>           // Global FGC DSP constants
#include <mcu_dsp_common.h>

// needed by fgc_parser_consts.h
struct hash_entry               // Required to satisfy parser_consts.h
{
    uint32_t    dummy;
};

#include <fgc_parser_consts.h>  // Global parser constants - contains property flags

#include <dpcom.h>              // Dual port ram structures
#include <bgcom.h>              // for bgcom global variable
#include <debug.h>              // for db global variable
#include <diag.h>               // for struct TDimLogSpace
#include <dsp_dependent.h>      // for DISABLE_INTS, LEDS_INIT, LED0_TOGGLE, LED1_TOGGLE
#include <rtcom.h>              // for rtcom global variable
#include <macros.h>             // for Test()
#include <property.h>
#include <propcomms.h>          // for PropComms()
#include <timer30.h>        // for TIMER_ADDR()
#include <bus32.h>          // for BUS_ADDR()
#include <serprt30.h>       // for SERIAL_PORT_ADDR()
#include <definfo.h>            // for FGC_CLASS_ID
#if FGC_CLASS_ID == 59
    #define PARS_GLOBALS
#endif
#include <pars.h>               // for fcm_pars_buf, scm_pars_buf global variables
#include <pars_service.h>       // for ParsePkt()
#include <pfloat.h>             // for PrintFloat()

/*---------------------------------------------------------------------------------------------------------*/
void InitDsp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called a the start of main.c to set up of the DSP internal registers.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Set up Memory Strobes

    BUS_ADDR->strb0_gcontrol  =  EXTERNAL_RDY | DATA_32 | MEMW_32 | STRB_SW_XTRA;   // DSP RAM
    BUS_ADDR->strb1_gcontrol  =  EXTERNAL_RDY | DATA_32 | MEMW_32;          // MCU RAM
    BUS_ADDR->iostrb_gcontrol =  EXTERNAL_RDY;                      // MCU interrupts

    // Set up Timer 0 to time RT processing

    TIMER_ADDR(0)->period       = 0xFFFFFFFF;
    TIMER_ADDR(0)->gcontrol_bit.clksrc  = INTERNAL;

    // Set up serial port to communicate with USB spy interface

    SERIAL_PORT_ADDR(0)->gcontrol =
                FSXOUT   |  // FSXOUT is an output pin
                XCLKSRCE |  // XCLKSRCE is internal transmit clock
                CLKXP    |  // CLKXP(Polarity) is active low (data on neg edge)
                XLEN_32  |  // 32 bits transmitted
                XVAREN;     // Frame synch FSX active when all data transmitted

    SERIAL_PORT_ADDR(0)->s_x_control =      // Serial output port direction control
                CLKXFUNC |  // CLKX0 is serial output port pin
                DXFUNC   |  // DX0 is serial output port pin
                FSXFUNC ;   // FX0 is serial output port pin

    SERIAL_PORT_ADDR(0)->s_rxt_period  = 0x00;  // 8 Mhz clock (4us/long word: this is the maximum possible)

    SERIAL_PORT_ADDR(0)->s_rxt_control =
                XGO      |  // Timer enabled
                XCLKSRC  |  // Internal clock
                XHLD_    |  // Counter enabled
                XCP_;       // Clock mode chosen

    SERIAL_PORT_ADDR(0)->gcontrol |= XRESET;    // Transmit reset - start serial port

    // Set up control of port that drives the LEDs on the development front panel

    LEDS_INIT;                  // I/O XF0/1 are both outputs to drive front panel LEDs
}
/*---------------------------------------------------------------------------------------------------------*/
void InitLib(uint32_t n_props, uint32_t max_user, uint32_t ppm_start, uint32_t ppm_length)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main.c to set up the library variables and to wait for the MCU to start.
  Note that it is the responsibility of the main function to clear the property structures to integer
  zero before calling this function.  The property structures must contain the property lengths when they
  are indirect (which is always true for PPM properties).
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t prop_idx;

    // Zero common structures

    memset(&rtcom, 0, sizeof(rtcom));
    memset(&bgcom, 0, sizeof(bgcom));
    memset(&db,    0, sizeof(db));

    // Save PPM structure range and length

    rtcom.ppm_start  = ppm_start;
    rtcom.ppm_length = ppm_length;
    rtcom.ppm_end    = ppm_start + ppm_length;

    // Set floating point properties to TI floating point zero

    for(prop_idx = 0; prop_idx < n_props; prop_idx++)
    {
        if(prop[prop_idx].float_f)
        {
            uint32_t value_addr = (uint32_t)prop[prop_idx].value;
            uint32_t prop_size  = prop[prop_idx].max_elements * prop[prop_idx].el_size; // Property length for each user index
            uint32_t ppm_step   = 0;
            uint32_t n_users;
            uint32_t user;

            if(!Test(prop[prop_idx].flags,PF_PPM))
            {
                // If not PPM property, slot 0 only

                n_users = 1;
            }
            else
            {
                // PPM property, add one for slot 0

                n_users = max_user + 1;

                if(value_addr < rtcom.ppm_start || value_addr >= rtcom.ppm_end)
                {
                    // If not in PPM zone, PPM array is contiguous

                    ppm_step = prop_size;
                }
                else
                {
                    // In PPM zone, PPM array is in struct ppm array

                    ppm_step = ppm_length;
                }
            }

            // Initialise property values to FP32 zero

            for(user = 0; user < n_users; user++)
            {
                uint32_t * mem_addr = (uint32_t *)(value_addr + ppm_step * user);

                memset(mem_addr, FP32ZERO, prop_size);
            }
        }
    }

    // Pointer to common Dual Port ram structure

    dpcom = (volatile struct dpcom *)DPRAM_DPCOM_32;

    // Get slot5 interface type

    rtcom.interfacetype = dpcom->mcu.interfacetype;

    // flat_qspi_board_number gives the flat qspi addr for the corresponding item in the dim_list[]
    // address values
    // 0x00..0x1F are real physical DIM boards
    // 0x80, b7=1 means a composite virtual DIM
    // 0xff means empty

    memset(dim_list.flat_qspi_board_number, 0xFF, FGC_MAX_DIMS);

    // Start DIAG debug logging

    dsp_debug.samples_to_acq = DIAG_DEBUG_TO_ACQ;

    // Prepare to detect triggered DIMs
    // not triggered bit from flat qspi addr packed in uint32_t
    // by default non was triggered

    qspi_misc.flat_qspi_not_triggered_packed = 0xFFFFFFFF;

    // Clear DIM logs buffer

    dim_log = (struct TDimLogSpace *)DIMLOGS_32;
    memset(dim_log, 0, sizeof(struct TDimLogSpace));

    // Default debug address is the debug struct

    dpcom->mcu.debug_mem_dsp_addr = (uint32_t)&db;

    // Wait for MCU to permit DSP to start

    while(dpcom->mcu.started != DSP_STARTED)
    {
        MS_SLEEP;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DebugMemTransfer(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called repetitively from Bgp() to transfer the debug zone of 32 words to the MCU where it
  is mapped onto the property FGC.DEBUG.MEM.DSP.  The user can define the address of the 32 words to
  copy using the same property.
\*---------------------------------------------------------------------------------------------------------*/
{
    bgcom.debug_idx++;
    if (bgcom.debug_idx >= DB_DSP_MEM_LEN)      // Increment DB index
    {
        bgcom.debug_idx = 0;
    }                           // Get value as FP32 and int

    dpcom->dsp.debug_mem_dsp_i[bgcom.debug_idx] =
                     ((uint32_t*)dpcom->mcu.debug_mem_dsp_addr)[bgcom.debug_idx];

    bgcom.db_float = (   (FP32*)dpcom->mcu.debug_mem_dsp_addr)[bgcom.debug_idx];

    if(bgcom.db_float > 1.0E+30)        // Clip FP32 at +/- 1E+30 to protect MCU printf
    {
        bgcom.db_float = 1.0E+30;
    }
    else if(bgcom.db_float < -1.0E+30)
    {
        bgcom.db_float = -1.0E+30;
    }

    dpcom->dsp.debug_mem_dsp_f[bgcom.debug_idx] = ToIEEE(bgcom.db_float); // Translate to IEEE FP32
}
/*---------------------------------------------------------------------------------------------------------*/
bool BgpPropComms(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the background processing loop.  It manages property communications with the
  MCU and parameter parsing.

  This function returns true if there is no pending action, false otherwise.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Check for DSP property set/get requests from MCU

    if(dpcom->pcm_prop.action)                          // Publication task action
    {
        PropComms(&dpcom->pcm_prop);
        return false;
    }

    if(dpcom->fcm_prop.action)                          // Fieldbus command task action
    {
        PropComms(&dpcom->fcm_prop);
        return false;
    }

    if(dpcom->tcm_prop.action)                          // Terminal command task action
    {
        PropComms(&dpcom->tcm_prop);
        return false;
    }

    // Check for command packet parsing requests from MCU

    if(dpcom->fcm_pars.type)
    {
        ParsePkt(&dpcom->fcm_pars,&fcm_pars_buf);       // Fieldbus command packet
        return false;
    }

    if(dpcom->tcm_pars.type)
    {
        ParsePkt(&dpcom->tcm_pars,&tcm_pars_buf);       // Terminal command packet
        return false;
    }

    // Check for print FP32 requests from MCU

    // Publication command task request

    if(dpcom->pcm_pfloat.format_idx != FLOAT_FMT_NONE)
    {
        PrintFloat(&dpcom->pcm_pfloat);
        return false;
    }

    // Fieldbus command task request

    if(dpcom->fcm_pfloat.format_idx != FLOAT_FMT_NONE)
    {
        PrintFloat(&dpcom->fcm_pfloat);
        return false;
    }

    // Terminal command task request

    if(dpcom->tcm_pfloat.format_idx != FLOAT_FMT_NONE)
    {
        PrintFloat(&dpcom->tcm_pfloat);
        return false;
    }

    return true;
}
/*---------------------------------------------------------------------------------------------------------*/
void FromMCUString(char *to, volatile uint32_t *from, uint32_t n_mcu_lw)
/*---------------------------------------------------------------------------------------------------------*\
  This function converts a MCU string to a DSP string.  The DSP string has one character per long word
  while the MCU string has four bytes packed in each long word. The function is told the number of long
  words containing the MCU string in n_mcu_lw.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      i;                  // Loop variable
    uint32_t      ch4;                    // Four characters compressed into one uint32_t int

    for(i=0 ; i < n_mcu_lw ; i++)           // For complete string
    {
    ch4 = *(from++);                    // Get next block of 4 characters

    to[3] = ch4 & 0xFF;                     // Transfer fourth byte
    ch4 >>= 8;
    to[2] = ch4 & 0xFF;                     // Transfer third byte
    ch4 >>= 8;
    to[1] = ch4 & 0xFF;                     // Transfer second byte
        to[0] = ch4 >> 8;                       // Transfer first byte

    to += 4;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ToMCUString(volatile uint32_t * to, const char * from, uint32_t n_chars)
/*---------------------------------------------------------------------------------------------------------*\
  This function converts a MCU string to a DSP string.  The DSP string has one character per long word
  while the MCU string has four bytes packed in each long word.  The function must be told the number
  of characters to transfer (excluding null).
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      i;                  // Loop variable
    uint32_t            n_lw;

    n_lw = (n_chars + 4) / 4;               // Calc length in long words allowing for null

    for(i=0 ; i < n_lw ; i++)               // For complete string
    {
        *(to++) = from[0] << 24 | from[1] << 16 | from[2] << 8 | from[3];       // Pack four characters                                                                                // into a long word
        from += 4;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: lib.c
\*---------------------------------------------------------------------------------------------------------*/
