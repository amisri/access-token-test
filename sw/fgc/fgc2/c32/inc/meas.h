/*---------------------------------------------------------------------------------------------------------*\
  File:         meas.h

  Purpose:      FGC DSP measurement function and variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MEAS_H   // Header encapsulation
#define MEAS_H

#ifdef MEAS_GLOBALS
    #define MEAS_VARS_EXT
#else
    #define MEAS_VARS_EXT extern
#endif

#include <stdint.h>

#include <cc_types.h>
#include <definfo.h>    // for FGC_CLASS_ID

/*---------------------------------------------------------------------------------------------------------*/

#define MEAS_HIST_MASK                  3                       // Mask for B and I meas history buffer index

// TODO duplicated definitions
#define VMEAS1_CAL                      5.117e-3                // Into +/-10V
#define VMEAS10_CAL                     5.117e-4                // Into +/-10V

/*---------------------------------------------------------------------------------------------------------*/

struct meas_hist
{
    FP32                meas;                           // Measurement now based on history
    FP32                rate;                           // Rate of change based on history
    FP32                estimated;                      // Estimated measurement on the next regulation period
    FP32                buf[MEAS_HIST_MASK+1];          // Measurement history buffer
    uint32_t            idx;                            // Index of most recent sample in history
};

struct meas
{
    FP32                b;                              // ms measured field used for feedback (G) (class 53)
    FP32                i;                              // ms measured current used for feedback
    FP32                v;                              // Voltage source output in volts

    struct meas_hist  * hist;                           // Pointer to b_hist or i_hist according to reg.active->state
    struct meas_hist    b_hist;                         // Field measurement history
    struct meas_hist    i_hist;                         // Current measurement history (one sample every iteration period)
    struct meas_hist    i_hist_reg;                     // Current measurement history (one sample every regulation period)

    FP32                i_earth_pcnt;                   // I_EARTH_PCNT (% of trip level)
    FP32                i_earth;                        // I_EARTH (A)
    FP32                i_diff_1s;                      // 1s difference between measurements
    FP32                u_lead_pos;                     // U_LEAD_POS accumulator
    FP32                u_lead_neg;                     // U_LEAD_NEG accumulator
    FP32                u_leads[2];                     // U_LEADS values
};

/*---------------------------------------------------------------------------------------------------------*/

void            MeasHistory             (struct meas_hist *hist, FP32 meas);
void            MeasHistoryRegPeriod    (struct meas_hist *hist, FP32 meas);
void            MeasSelect              (void);
FP32            MeasSpyMpx              (uint32_t);

#if FGC_CLASS_ID != 53
void            MeasSpySend             (void);
void            MeasDiag                (void);
#endif
/*---------------------------------------------------------------------------------------------------------*/

MEAS_VARS_EXT struct meas meas;

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation MEAS_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: meas.h
\*---------------------------------------------------------------------------------------------------------*/
