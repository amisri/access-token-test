/*---------------------------------------------------------------------------------------------------------*\
  File:         pars.h

  Purpose:      FGC DSP Parsing functions declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PARS_H   // Header encapsulation
#define PARS_H

#ifdef PARS_GLOBALS
    #define PARS_VARS_EXT
#else
    #define PARS_VARS_EXT extern
#endif


#include <cc_types.h>
#include <dpcom.h>      // for MAX_CMD_PKT_LEN, MAX_CMD_TOKEN_LEN
#include <mcu_dsp_common.h>

// --------------------------------------------------------------------------------------------------------

// Functions that are not declared in defprops_dsp_fgc.h

void ParsLoadSim(void);
void ParsGroupSet(void);

// --------------------------------------------------------------------------------------------------------

// Command Packet parse buffers

struct cmd_pkt_parse_buf
{
    uint32_t      n_carryover_chars;              // Number of characters from last token of prev pkt
    char        buf[MAX_CMD_PKT_LEN + MAX_CMD_TOKEN_LEN];  // Parse buffer with extra space for last token of previous packet
};

// --------------------------------------------------------------------------------------------------------

PARS_VARS_EXT struct cmd_pkt_parse_buf  fcm_pars_buf;
PARS_VARS_EXT struct cmd_pkt_parse_buf  tcm_pars_buf;

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation PARS_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars.h
\*---------------------------------------------------------------------------------------------------------*/
