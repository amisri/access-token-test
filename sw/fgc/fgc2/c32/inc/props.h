/*---------------------------------------------------------------------------------------------------------*\
  File:        props.h

  Purpose:    FGC DSP global property variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PROPS_H   // Header encapsulation
#define PROPS_H

#ifdef PROPS_GLOBALS
    #define PROPS_VARS_EXT
#else
    #define PROPS_VARS_EXT extern
#endif

#include <cc_types.h>
#include <ref.h>        // will include libfg1.h
#include <libreg1.h>
#include <dpcom.h>
#include <defconst.h>   // for FGC_N_ADCS
#include <definfo.h>    // for FGC_CLASS_ID
#include <ppm.h>        // Required for defprops_dsp_fgc.h
#include <diag.h>

// DSP Property variables

struct property
{
    struct                                                      // ---------------------------------
    {
        FP32          tau_temp;                                 // ADC.ADC16.TAU_TEMP
        FP32          temperature;                              // ADC.ADC16.TEMPERATURE
// TODO Check if the macro is needed
#if FGC_CLASS_ID == 51 || FGC_CLASS_ID == 53
        uint32_t        filter_reset_mask;                        // ADC.FILTER.UNEXP_RESETS.ENABLED
        uint32_t        num_unexpected_resets;                    // ADC.FILTER.UNEXP_RESETS.COUNT
        uint32_t        reset_hist_chan [FGC_ADC_RESET_HIST_LEN]; // Reset channel history: channel id
        uint32_t        reset_hist_flags[FGC_ADC_RESET_HIST_LEN]; // Reset channel history: measurement status
                                                                // flags limited to ADC_RESET_STATUS_MASK
#endif
    } adc;
    struct                                                      // ---------------------------------
    {
        struct                                  // # = A or B
        {
            FP32        headerr;                // CAL.#.DCCT.HEADERR
            FP32        tc[3];                  // CAL.#.DCCT.TC
            FP32        dtc[3];                 // CAL.#.DCCT.DTC
        } dcct[2];
        struct
        {
            struct                              // # = A, B, C or D
            {
                uint32_t gain;                   // CAL.#.ADC.INTERNAL.GAIN
                FP32    tc[3];                  // CAL.#.ADC.INTERNAL.TC
                FP32    dtc[3];                 // CAL.#.ADC.INTERNAL.DTC
                int32_t vraw[FGC_CAL_VRAW_LEN]; // CAL.#.ADC.INTERNAL.VRAW
            } internal[FGC_N_ADCS];
            struct                              // # = A or B
            {
                uint32_t gain;                   // CAL.#.ADC.EXTERNAL..GAIN
            } external[2];
        } adc;
        struct
        {
            FP32    err[6];                     // CAL.VREF.ERR
        } vref;

        FP32    ext_ref_err;                    // CAL.EXT_REF_ERR
        FP32    dac[3];                         // CAL.A.DAC
    } cal;
    struct                                                      // ---------------------------------
    {
        uint32_t        primary_turns;                          // DCCT.PRIMARY_TURNS
        FP32            tau_temp;                               // DCCT.TAU_TEMP
        FP32            gain[2];                                // DCCT.#.GAIN
        FP32            temperature[2];                         // DCCT.#.TEMPERATURE
        uint32_t        position[2];                            // DCCT.#.POSITION
    } dcct;
    struct                                                      // ---------------------------------
    {
        struct
        {
            uint32_t int_dsp_div;                                 // FGC.ITER_PERIOD.INT_DSP_DIV
        } iter_period;
        struct
        {
            struct
            {
                FP32 *   fpval;                                 // FGC.DEBUG.SPY.FPVAL
                int32_t *intval;                                // FGC.DEBUG.SPY.INTVAL
            } spy;
#if FGC_CLASS_ID == 51
            struct
            {
                uint32_t            out_of_limits;              // FGC.DEBUG.PSU_5V.OUT_OF_LIMITS
                uint32_t            log_state;                  // FGC.DEBUG.PSU_5V.LOG_STATE
                struct diag_psulog  log;                        // FGC.DEBUG.PSU_5V.LOG
            } psu_5v;
            struct
            {
                uint32_t            out_of_limits;              // FGC.DEBUG.PSU_NEG_15V.OUT_OF_LIMITS
                uint32_t            log_state;                  // FGC.DEBUG.PSU_NEG_15V.LOG_STATE
                struct diag_psulog  log;                        // FGC.DEBUG.PSU_NEG_15V.LOG
            } psu_neg_15v;
            struct
            {
                uint32_t            out_of_limits;              // FGC.DEBUG.PSU_POS_15V.OUT_OF_LIMITS
                uint32_t            log_state;                  // FGC.DEBUG.PSU_POS_15V.LOG_STATE
                struct diag_psulog  log;                        // FGC.DEBUG.PSU_POS_15V.LOG
            } psu_pos_15v;
#endif
        } debug;
    } fgc;
    struct                                                      // ---------------------------------
    {
        uint32_t rt_bad_values;                                  // FIELDBUS.RT_BAD_VALUES
    } fieldbus;
    struct                                                      // ---------------------------------
    {
        struct
        {
            FP32        err_warning;                              // LIMITS.B.ERR_WARNING
            FP32        err_fault;                              // LIMITS.B.ERR_FAULT
        } b;
        struct
        {
            FP32        access;                                 // LIMITS.I.ACCESS
            FP32        earth;                                  // LIMITS.I.EARTH
            FP32        hardware;                               // LIMITS.I.HARDWARE
            FP32        rms;                                    // LIMTIS.I.RMS
            FP32        meas_diff;                              // LIMITS.I.MEAS_DIFF
            FP32        closeloop       [FGC_N_LOADS];          // LIMITS.I.CLOSELOOP
            FP32        pos             [FGC_N_LOADS];          // LIMITS.I.POS
            FP32        min             [FGC_N_LOADS];          // LIMITS.I.MIN
            FP32        neg             [FGC_N_LOADS];          // LIMITS.I.NEG
            FP32        rate            [FGC_N_LOADS];          // LIMITS.I.RATE
            FP32        acceleration    [FGC_N_LOADS];          // LIMITS.I.ACCELERATION
            FP32        err_warning     [FGC_N_LOADS];          // LIMITS.I.ERR_WARNING
            FP32        err_fault       [FGC_N_LOADS];          // LIMITS.I.ERR_FAULT
            FP32        quadrants41     [2];                    // LIMITS.I.QUADRANTS41
            FP32        low;                                    // LIMITS.I.LOW
            FP32        zero;                                   // LIMITS.I.ZERO
        } i;
        struct
        {
            FP32        pos             [FGC_N_LOADS];          // LIMITS.V.POS
            FP32        neg             [FGC_N_LOADS];          // LIMITS.V.NEG
            FP32        err_warning;                            // LIMITS.V.ERR_WARNING
            FP32        err_fault;                              // LIMITS.V.ERR_FAULT
            FP32        quadrants41     [2];                    // LIMITS.V.QUADRANTS41
        } v;
        struct
        {
            struct fg_limits    b;                              // LIMITS.OP.B.POS/RATE & LIMITS.B.POS/RATE
            struct fg_limits    i;                              // LIMITS.OP.I.POS/MIN/NEG/RATE/ACCELERATION
            FP32                i_available;                    // LIMITS.OP.I.AVAILABLE
            struct fg_limits    v;                              // LIMITS.OP.V.POS/NEG/RATE & LIMITS.V.RATE
        } op;
    } limits;
    struct                                                      // ---------------------------------
    {
        FP32            ohms_ser        [FGC_N_LOADS];          // LOAD.OHMS_SER
        FP32            ohms_par        [FGC_N_LOADS];          // LOAD.OHMS_PAR
        FP32            ohms_mag        [FGC_N_LOADS];          // LOAD.OHMS_MAG
        FP32            henrys          [FGC_N_LOADS];          // LOAD.HENRYS
        FP32            henrys_sat      [FGC_N_LOADS];          // LOAD.HENRYS_SAT
        FP32            i_sat_start     [FGC_N_LOADS];          // LOAD.I_SAT_START
        FP32            i_sat_end       [FGC_N_LOADS];          // LOAD.I_SAT_END
        FP32            gauss_per_amp   [FGC_N_LOADS];          // LOAD.GAUSS_PER_AMP
        uint32_t          select;                                 // LOAD.SELECT
    } load;
    struct                                                      // ---------------------------------
    {
        struct
        {
            uint32_t   status          [FGC_MAX_USER_PLUS_1];   // LOG.CYC.STATUS
            FP32       max_abs_err     [FGC_MAX_USER_PLUS_1];   // LOG.CYC.MAX_ABS_ERR
        } cyc;
    } log;
    struct                                                      // ---------------------------------
    {
        uint32_t        sim;                                    // MEAS.SIM
        FP32            i_sim;                                  // MEAS.I_SIM
        FP32            i_rms;                                  // MEAS.I_RMS
        struct
        {
            FP32        i_earth;                                // MEAS.MAX.I_EARTH
            FP32        u_leads[2];                             // MEAS.MAX.U_LEADS
        } max;
    } meas;
    struct                                                      // ---------------------------------
    {
        uint32_t        rt;                                     // MODE.RT
    } mode;
    struct                                                      // ---------------------------------
    {
        uint32_t            on_failure;                         // REF.ON_FAILURE
        int32_t             dac;                                // REF.DAC
        struct abs_time_us  run;                                // REF.RUN
        struct abs_time_us  abort;                              // REF.ABORT
        FP32                run_delay;                          // REF.RUN_DELAY
        FP32                ccv;                                // REF.CCV (Current Control Value)
        struct
        {
            struct
            {
                FP32   data[FGC_N_CYC_CHK_DATA];               // REF.CYC.FAULT.DATA
            } fault;
        } cyc;

        struct
        {
            struct
            {
                FP32    acceleration;                           // REF.DEFAULTS.B.ACCELERATION
            } b;
        } defaults;
        struct
        {
            struct
            {
                FP32   starting        [FGC_N_LOADS];           // REF.RATE.I.STARTING
                FP32   to_standby      [FGC_N_LOADS];           // REF.RATE.I.TO_STANDBY
                FP32   stopping        [FGC_N_LOADS];           // REF.RATE.I.STOPPING
            } i;
        } rate;
        struct fg_plep_config           plep;                   // REF.PLEP.*
        struct fg_trim_config           trim;                   // REF.TRIM.FINAL/PERIOD
        struct fg_test_config           test;                   // REF.TEST.*
        struct fg_openloop_config       openloop;               // REF.OPENLOOP.FINAL
    } ref;
    struct                                                      // ---------------------------------
    {
        struct
        {
            uint32_t              period_div[FGC_N_LOADS];      // REG.B.PERIOD_DIV
            FP32                  period    [FGC_N_LOADS];      // REG.B.PERIOD     (read-only)
            struct reg_rst        manual_rst;                   // REG.B.EXTERNAL.OP.R/S/T ; REG.B.EXTERNAL.TRACK_DELAY
        } b;
        struct
        {
            uint32_t              period_div[FGC_N_LOADS];      // REG.I.PERIOD_DIV
            FP32                  period    [FGC_N_LOADS];      // REG.I.PERIOD     (read-only)
            FP32                  clbw      [FGC_N_LOADS];      // REG.I.CLBW
            FP32                  clbw2     [FGC_N_LOADS];      // REG.I.CLBW2
            FP32                  z         [FGC_N_LOADS];      // REG.I.Z
            FP32                  pure_delay[FGC_N_LOADS];      // REG.I.PURE_DELAY
            struct reg_rst        manual_rst;                   // REG.I.EXTERNAL.OP.R/S/T ; REG.I.EXTERNAL.TRACK_DELAY
            struct reg_rst_pars   op;                           // REG.I.LAST.OP.STATUS/R/S/T    (read-only)
        } i;
        struct
        {
            uint32_t              period_div;                   // REG.V.PERIOD_DIV
            struct reg_rst_pars   rst_pars;                     // REG.V.PERIOD (read-only) ; REG.V.TRACK_DELAY
        } v;
        struct
        {
            uint32_t              reset;                        // REG.TEST.RESET
            uint32_t              user;                         // REG.TEST.USER
            uint32_t              ref_user;                     // REG.TEST.REF_USER
            struct
            {
                uint32_t              period_div;               // REG.TEST.B.PERIOD_DIV
                FP32                  period;                   // REG.TEST.B.PERIOD
                struct reg_rst        rst;                      // REG.TEST.B.R/S/T ; REG.TEST.B.TRACK_DELAY
            } b;
            struct
            {
                uint32_t              period_div;               // REG.TEST.I.PERIOD_DIV
                FP32                  period;                   // REG.TEST.I.PERIOD
                struct reg_rst        rst;                      // REG.TEST.I.R/S/T ; REG.TEST.I.TRACK_DELAY
            } i;
        } test;
    } reg;
    struct                                                      // ---------------------------------
    {
        uint32_t        mpx[FGC_N_SPY_CHANS];                   // SPY.MPX
    } spy;
    struct                                                      // ---------------------------------
    {
        struct
        {
            int32_t    test_int32s[FGC_DSP_TEST_PROP_LEN];      // TEST.DSP.int32_t
            FP32       test_float [FGC_DSP_TEST_PROP_LEN];      // TEST.DSP.FLOAT
        } dsp;
    } test;
    struct                                                      // ---------------------------------
    {
        struct
        {
            uint32_t  len_basic_per   [FGC_MAX_USER_PLUS_1];    // TIME.CYCLE.LEN_BASIC_PER
        } cycle;
    } time;
    struct                                                      // ---------------------------------
    {
        FP32            offset;                                 // VS.OFFSET
        FP32            gain;                                   // VS.GAIN
        struct reg_sim_vs_pars sim;                             // VS.SIM.NUM/DEN
        FP32            ctrl_delay;                             // VS.SIM.CTRL_DELAY
        FP32            dac_clamp;                              // VS.DAC_CLAMP (+/-10V)
        struct
        {
            FP32        gain;                                   // VS.I_LIMIT.GAIN
            FP32        ref;                                    // VS.I_LIMIT.REF
        } i_limit;
    } vs;
#if FGC_CLASS_ID == 53
    struct                                                      // ---------------------------------
    {
        uint32_t        num_resets;                             // PAL.NUM_RESETS
        uint32_t        version[FGC_N_PAL_VERS_WORDS];          // PAL.VERSION
        uint32_t        debug  [FGC_N_PAL_DEBUG_WORDS];         // PAL.DEBUG

        struct
        {
            uint32_t    status;                                 // PAL.LINKS.STATUS
            uint32_t    errors;                                 // PAL.LINKS.ERRORS
            uint32_t    num_errors  [FGC_N_PAL_LINKS];          // PAL.LINKS.NUM_ERRORS
            uint32_t    time_err_s  [FGC_N_PAL_LINKS];          // PAL.LINKS.TIME_ERR_S
            uint32_t    time_err_ms [FGC_N_PAL_LINKS];          // PAL.LINKS.TIME_ERR_MS
        } links;
        struct
        {
            uint32_t    divpam_id   [FGC_N_PAL_CHANS];          // PAL.CHAN.DIVPAM_ID
            uint32_t    no_sig_count[FGC_N_PAL_CHANS];          // PAL.CHAN.NO_SIG_COUNT
        } chan;

        struct
        {
            FP32        vref_pos    [FGC_N_DIVPAM_CARDS];       // PAL.DIVPAM.VREF_POS
            FP32        vref_zero   [FGC_N_DIVPAM_CARDS];       // PAL.DIVPAM.VREF_ZERO
            FP32        vref_neg    [FGC_N_DIVPAM_CARDS];       // PAL.DIVPAM.VREF_NEG
            FP32        cal_high    [FGC_N_DIVPAM_CARDS];       // PAL.DIVPAM.CAL_HIGH
            FP32        cal_low     [FGC_N_DIVPAM_CARDS];       // PAL.DIVPAM.CAL_LOW
        } divpam;

        struct whiterabbit
        {
            uint32_t b_meas[FGC_MAX_USER_PLUS_1];                 // PAL.WHITERABBIT.B_MEAS
            uint32_t errors;                                      // PAL.WHITERABBIT.ERRORS
            uint32_t num_errors[FGC_N_PAL_BMEAS_ERRORS];          // PAL.WHITERABBIT.NUM_ERRORS
        } whiterabbit;

    } pal;
#endif
};

PROPS_VARS_EXT  struct property property;

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation PROPS_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp\inc\props.h
\*---------------------------------------------------------------------------------------------------------*/
