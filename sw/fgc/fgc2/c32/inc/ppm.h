/*---------------------------------------------------------------------------------------------------------*\
  File:         ppm.h

  Purpose:      FGC DSP global PPM variable declaration
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PPM_H   // Header encapsulation
#define PPM_H

#ifdef PPM_GLOBALS
#define PPM_VARS_EXT
#else
#define PPM_VARS_EXT extern
#endif

#include <stdint.h>

#include <cc_types.h>
#include <ref.h>        // will include libfg1.h
#include <libreg1.h>
#include <dpcom.h>
#include <defconst.h>

/*----- PPM Property/variable structure -----*/

struct cyc_warn
{
    uint32_t             chk;                            // REF.CYC.WARNING.CHK
    FP32                 data[FGC_N_CYC_CHK_DATA];       // REF.CYC.WARNING.DATA
};

// The PPM cycling structure contains all the information needed to play the reference function
// of one user on one cycle

struct ppm_ref_start
{
    FP32                    vref;               // REF.START.VREF
    FP32                    time;               // REF.START.TIME
    FP32                    duration;           // REF.START.DURATION
};

struct ppm_cycling                              // --- PPM cycling variables/parameters ---
{
    uint32_t                func_type;          // Reference function type
    uint32_t                reg_mode;           // Reference regulation mode
    uint32_t                stc_func_type;      // Reference function type symbol index (for RTD)
    struct fg_meta          meta;               // Reference function meta data
    uint32_t                min_basic_periods;  // Reference minimum length in basic periods
    struct ppm_ref_start    ref_start;          // REF.START properties
    struct fg_first_plateau first_plateau;      // First Plateau parameters
    struct fg_table_data    table_data;         // Table data
    union  fg_ppm_pars      pars;               // Function generation parameters
    struct cyc_warn         warning;            // (In the context of 'ppm' global var) REF.CYC.WARNING properties
};

// The PPM structure is divided into three sub-structures:
//
//   - The "config" part is used as input for the fgTableArm()/fgPpplArm() functions,
//     the output of which is usually a "pars" structure in the next PPM sub-structure, "armed".
//
//   - The "armed" part corresponds to the function to be played, i.e. the user request. It maps directly
//     with the REF properties (e.g REF.TABLE.REF/REF_TIME).
//
//   - The "cycling" part corresponds to the function as it is currently played. The data in that structure
//     is basically a copy of the data in the "armed" PPM sub-structure. The copy is done while the reference
//     function is not playing. These data is used as input for functions fgTableGen, fgPpplGen, etc.
//

struct ppm
{
    struct ppm_config                                   // --- PPM function configurations ---
    {
        struct fg_table_config  table;                  // Table config data
        struct fg_pppl_config   pppl;                   // REF.PPPL.xxx
    } config;

    struct ppm_armed                                    // --- PPM armed function parameters ---
    {
        uint32_t                stc_func_type;          // Reference function type symbol index (for RTD)
        uint32_t                reg_mode;               // Reference regulation mode
        struct fg_meta          meta;                   // REF.INFO properties
        uint32_t                min_basic_periods;      // Armed reference minimum length in basic periods
        struct fg_first_plateau first_plateau;          // REF.FIRST_PLATEAU properties
        struct fg_table_data    table_data;             // REF.TABLE data
        union  fg_ppm_pars      pars;                   // Armed function configuration
    } armed;

    struct ppm_cycling  cycling;                        // --- PPM cycling variables/parameters ---
};

PPM_VARS_EXT struct ppm ppm[FGC_MAX_USER_PLUS_1];

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation PPM_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp\inc\ppm.h
\*---------------------------------------------------------------------------------------------------------*/
