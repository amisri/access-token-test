/*!
 * @file  assert.h
 */

#include <string.h>



/*!
 * Copies arguments to the dual-port memory and calls panic function
 */
void assertFunc(const char * filename, const int line);



/*!
 * Returns basename of current filename
 */
#define __FILENAME__ (strrchr("/" __FILE__, '/') + 1)



/*!
 * Asserts expression at run time
 */
#ifdef NDEBUG

#define assert(expr) ((void)0)

#else

#define assert(expr) ((expr) ? (void)0 : assertFunc(__FILENAME__, __LINE__))

#endif



/*!
 * Asserts expression at compile time
 *
 * When using this macro inside of a function, it has to be called in the
 * variable declaration section.
 *
 * This implementation shouldn't generate any warnings during compilation.
 * No assembly code should be generated either.
 */
#define static_assert(expr) extern char static_assert_failed[(expr) ? 1 : -1]

// EOF
