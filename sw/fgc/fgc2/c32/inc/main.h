/*---------------------------------------------------------------------------------------------------------*\
  File:         main.h

  Purpose:      FGC DSP global function and variable declarations
\*---------------------------------------------------------------------------------------------------------*/


#ifndef MAIN_H   // Header encapsulation
#define MAIN_H

#ifdef MAIN_GLOBALS
    #define MAIN_VARS_EXT
#else
    #define MAIN_VARS_EXT extern
#endif

#include <cc_types.h>
#include <definfo.h>
#include <libcal.h>
#include <reg.h>

// The DSP iteration period (= DSP interrupt period) is for now equal to the V regulation period on classes 51, 53 and 61.
// (i.e. REG.V.PERIOD_DIV is always equal to 1)

#define DSP_ITER_PERIOD         (property.reg.v.rst_pars.period)

// Maximum number of iteration per milliseconds. That integer value is used to dimension measurement log arrays/buffers/history, etc.
// If Fmax is the maximum DSP iteration frequency, then DSP_MAX_ITERS_PER_MS must be set to ceil(Fmax/1kHz).

#define DSP_MAX_ITERS_PER_MS    10

// Delay length (in units of the iteration period)

// Voltage measurement delay.
// NB: 1.4ms is the delay on class 53 (POPS) introduced by the PAM+PBL cards.
//     Use the same for class 51 even though such delay is not relevant on that platform (Vmeas comes from the DIMs)

#define VMEAS_DELAY_ITERS               1.4

// Current measurement delay

#define IMEAS_DELAY_ITERS               1.3

// Class 51/53 macro definitions

#define DAC_RESOLUTION              20                      // DAC resolution (bits)
#define DAC_CAL_REF                 367000                  // DAC raw cal value (~8V)

#define VMEAS1_CAL                      5.117e-3        // Into +/-10V
#define VMEAS10_CAL                     5.117e-4        // Into +/-10V

#define ULEAD_WARN_SET_LIMIT            0.100           // Current warning set threshold 100mV
#define ULEAD_WARN_CLR_LIMIT            0.090           // Current warning clear threshold 90mV

#define DSP_INITIALISED                 PROP_SPY_MPX.n_elements


// Macros used to define the type of function generation and regulation
// based on the type of load and type of power converter

#define TWO_QUADRANT_CONVERTER          (!lim_v_ref.flags.unipolar && \
                                          lim_i_ref.flags.unipolar)

#define INDUCTIVE_LOAD                  (load.active->pars.tc > 1.0)

#define RESISTIVE_LOAD                  !(INDUCTIVE_LOAD)

#define RESISTIVE_ZERO_IS_SIGNIFICANT   (load.active->pars.gain10 <= 1.0E05)

// A note regarding RESISTIVE_ZERO_IS_SIGNIFICANT: Assuming Rm (the magnet resistance) is zero, gain10 is the ratio
// (Rp / Rs) (parallel resistance divided by the serial resistance). The value 100000 is empirical. It is still to
// be discussed if a better criteria can apply. For information, the OP class 51 used in the LHC in 2010/2011 used a
// different criteria: the resistive zero is significant if the time constant of the magnet in open-circuit, (L / Rp),
// [in fact this should be L / (Rp + Rm) but assume Rm is zero] be greater than 0.1s. In any case with both those
// criteria the experiment magnets (ATLAS and CMS) will NOT use a dIdT regulation in the STARTING and STOPPING
// references, which is important because that type of regulation is known to be unstable for those magnets.
// A change in the criteria above must account for that.


// In I regulation, on a 2-quadrant converter, with an inductive load with insignificant resistive zero,
// it is required to run the STOPPING reference function to stop the converter.

#define REF_STOPPING_REQUIRED           (reg.active->state != FGC_REG_V && \
                                         TWO_QUADRANT_CONVERTER &&         \
                                         INDUCTIVE_LOAD &&                 \
                                         !(RESISTIVE_ZERO_IS_SIGNIFICANT))

// Global variables

MAIN_VARS_EXT uint32_t    state_pc;               // PC state machine
MAIN_VARS_EXT uint32_t    dcct_select;            // DCCT.SELECT

MAIN_VARS_EXT struct cal_temp_filter temperature_filter_adcs;
MAIN_VARS_EXT struct cal_temp_filter temperature_filter_dcct[2];

// Function declarations

void    main                            (void);
void    InitGlobals                     (void);
void    TemperatureCompensationInit     (void);
void    TemperatureCompensation         (void);

// time.c function declarations
// TODO Rename time.c and create corresponding header file. time.h already exists as part of the compiler header files and dsp_time.h is already taken.

void    TimeIteration                   (void);
void    TimeRunningEvents               (void);
void    TimeRunning                     (void);
void    TimeCyclingEvents               (void);
void    TimeCycling                     (void);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation MAIN_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
