/*---------------------------------------------------------------------------------------------------------*\
  File:         ref.h

  Purpose:      FGC DSP reference generation variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef REF_H   // Header encapsulation
#define REF_H

#ifdef REF_GLOBALS
#define REF_VARS_EXT
#else
#define REF_VARS_EXT extern
#endif

#include <stdint.h>

#include <cc_types.h>
#include <dpcom.h>                              // For struct abs_time_us, struct abs_time_ms
#include <libfg1.h>                              // Function generation library
#include <libfg/plep.h>                               // Function generation library: plep structures
#include <libfg/pppl.h>                               // Function generation library: pppl structures
#include <libfg/table.h>                              // Function generation library: table structures
#include <libfg/trim.h>                               // Function generation library: trim structures
#include <libfg/test.h>                               // Function generation library: test structures
#include <macros.h>
#include <mcu_dsp_common.h>
#include <dsp_time.h>                           // For struct fp_time

// ---------------------------------------------------------------------------------------------------------
// Constants
//

#define NON_PPM_USER                    (0)
#define PPM_ALL_USERS                   (0)

#define NAN_MASK                0x7F800000              // NaN mask (Not a Number in floating point) in IEEE-754 standard
#define INTERPOLATION_TIME      0.02                    // Real-time control data interpolation time
#define TFA_END_DURATION_MARGIN 1.20                    // 20% margin on the estimated ramp down duration
// at the end of a TFA reference

// Minimal time interval required between two points of a TABLE reference

#define REF_TABLE_MIN_TIME_STEP 1.0E-04                 // 100 us

#define EnableRegBypass()       Set(ref.reg_bypass_mask,(1<<ref.func_type))
#define DisableRegBypass()      Clr(ref.reg_bypass_mask,(1<<ref.func_type))
#define TestRegBypass()         Test(ref.reg_bypass_mask,(1<<ref.func_type))
#define TestRefAbortable()      Test(REF_ABORTABLE,(1<<ref.func_type))
#define TestRefCheckLimits()    Test(REF_CHECK_LIMITS,(1<<ref.func_type))

#define DEFAULT_REF_REG_BYPASS  (1 << FGC_REF_STARTING)  | \
    (1 << FGC_REF_STOPPING)  | \
    (1 << FGC_REF_RAMP)      | \
    (1 << FGC_REF_END)

#define REF_ARM_FOR_RUNNING     (1 << FGC_REF_NONE)      | \
    (1 << FGC_REF_TABLE)     | \
    (1 << FGC_REF_PLEP)      | \
    (1 << FGC_REF_LTRIM)     | \
    (1 << FGC_REF_CTRIM)     | \
    (1 << FGC_REF_STEPS)     | \
    (1 << FGC_REF_SQUARE)    | \
    (1 << FGC_REF_SINE)      | \
    (1 << FGC_REF_COSINE)    | \
    (1 << FGC_REF_RAMP)

#define REF_ARM_FOR_CYCLING     (1 << FGC_REF_NONE)      | \
    (1 << FGC_REF_ZERO)      | \
    (1 << FGC_REF_PPPL)      | \
    (1 << FGC_REF_TABLE)

#define REF_ABORTABLE           (1 << FGC_REF_TABLE)     | \
    (1 << FGC_REF_PLEP)      | \
    (1 << FGC_REF_PLEP_NO_CLIP)| \
    (1 << FGC_REF_TO_STANDBY)| \
    (1 << FGC_REF_LTRIM)     | \
    (1 << FGC_REF_CTRIM)     | \
    (1 << FGC_REF_STEPS)     | \
    (1 << FGC_REF_SQUARE)    | \
    (1 << FGC_REF_SINE)      | \
    (1 << FGC_REF_COSINE)    | \
    (1 << FGC_REF_RAMP)

#define REF_CHECK_LIMITS        (1 << FGC_REF_PLEP)      | \
    (1 << FGC_REF_TO_STANDBY)| \
    (1 << FGC_REF_ABORTING)  | \
    (1 << FGC_REF_PPPL)      | \
    (1 << FGC_REF_TABLE)

// ---------------------------------------------------------------------------------------------------------
// Structures
//

// First plateau cannot be in union fg_ppm_pars because it is used with the PPPL

struct fg_first_plateau                                 // Field function first plateau
{
    FP32                ref;                            // First plateau field reference
    FP32                time;                           // First plateau start time
    FP32                duration;                       // First plateau duration
};

// Openloop function is not (yet) part of libfg

struct fg_openloop_config                               // Open loop function configuration
{
    FP32               final;                          // Final reference
};

struct fg_openloop_pars                                 // Open loop function parameters
{
    FP32                run_delay;                      // Run delay
    uint32_t              pos_ramp_flag;                  // Positive ramp flag
    FP32                v_ref;                          // Open loop voltage reference
    FP32                d_v_ref;                        // Vref steps to produce required acceleration
    FP32                i_closeloop;                    // Current threshold for reclosing loop
    FP32                final;                          // Final current
};

// Table data works with libfg table functions

struct fg_table_data                                    // Function generator table data
{
    struct FG_point        function[FGC_TABLE_LEN];
};

// 1 or 2 quadrant converter ramp to I_MIN (uses dI/dt regulation)

struct fg_starting_pars
{
    FP32                i_rate;                         // Current ref ramp rate (A/s)
    FP32                v_rate;                         // Voltage ref ramp rate (V/s)
    FP32                delay;                          // Min delay before closing Iloop
    uint32_t              openloop_f;                     // Unipolar openloop start mode flag
};

// End - fast open-loop current ramp down

struct fg_end_pars
{
    FP32                direction;                      // 1.0 for positive current, -1.0 for negative current
    FP32                v_ref;                          // Voltage to use to ramp down the current
    FP32                clip_factor;                    // Factor to calculate voltage clip based on current
};

// Stopping of a slow 2 quadrant converter (uses dI/dT regulation)

struct fg_stopping_pars
{
    FP32                i_rate_step;                    // Irate step based on I acceleration limit
    FP32                i_rate_init_meas;               // Initial Irate (measured), kept for debugging purpose
    FP32                i_rate_init;                    // Initial Irate (clipped),  kept for debugging purpose
    FP32                i_rate;                         // Intermediate target for Irate during 2Q stopping
    FP32                i_rate_min;                     // Final        target for Irate during 2Q stopping
};

// PPM function parameter structures (used in struct ppm in props51.h)

union fg_ppm_pars                                       // Function generator ppm parameters union
{
    struct fg_trim_pars     zero;                       // ZERO parameters (uses a CTRIM)
    struct fg_pppl_pars     pppl;                       // PPPL parameters
    struct fg_table_pars    table;                      // Linearly interpolated table parameters
};

// Non-PPM function parameters

union fg_non_ppm_pars                                   // Function generator non-ppm parameters union
{
    struct fg_plep_pars     plep;                       // PLEP parameters (user PLEP only)
    struct fg_trim_pars     trim;                       // TRIM parameters
    struct fg_test_pars     test;                       // Test function parameters
    struct fg_openloop_pars openloop;                   // Openloop function parameters
    struct fg_starting_pars starting;                   // 1Q/2Q starting parameters
    struct fg_end_pars      end;                        // Fast open-loop current rampdown parameters
    struct fg_stopping_pars stopping;                   // 2Q stopping parameters
};

// Function generator non-ppm armed parameters union
REF_VARS_EXT union fg_non_ppm_pars ref_fg;

// Working PLEP function (must be separate from ref_fg because it is used for TO_STANDBY and SLOW_ABORT)

// PLEP parameters (User PLEP or TO_STANDBY, etc...)
REF_VARS_EXT struct fg_plep_pars    plep;

#include <ppm.h>        // For struct ppm_cycling. Cannot be included sooner in the file, because ppm.h
// needs the declarations of several of the fg_ structures above.

// Structure used to describe a reference: its value, its rate of change, etc.

struct ref_container
{
    FP32                    value;                      // Reference value
    FP32                    rate;                       // Rate of change of the reference (per second)
    FP32                    estimated;                  // Estimated value after one regulation iteration
    FP32                    prev_value;                 // Value on the previous regulation iteration
};

// Global reference structure

struct ref                                              // Reference variables
{
    uint32_t                  func_type;                  // Reference function type (REF_XXX)

    FP32                    rt;                         // Real-time control value
    struct ref_container    fg;                         // Function generator value
    struct ref_container    fg_plus_rt;                 // Function generator value plus RT contribution

    struct ref_container    b;                          // Field reference (G)
    struct ref_container    i_clipped;                  // Current reference after clipping (A)
    struct ref_container    i;                          // Final current reference (A)
    struct ref_container    v;                          // Voltage reference (V)

    // Field or current reference (G or A) (points to ref.i or ref.b)
    struct ref_container * now;

    FP32                    v_err_avg;                  // Averaged V error over one regulation period

    struct fp_time          time;                       // Time elapsed running ref (in seconds)
    FP32                    advance;                    // Reference function advance (s)
    uint32_t                  advance_us;                 // Reference function advance (us)
    uint32_t                  reg_bypass_mask;            // Regulation bypass mask for all reference types
    uint32_t                  cycling_f;                  // Reference cycling

    struct abs_time_us      run;        // Time to run   non-PPM ref change (mirror copy of property REF.RUN)
    struct abs_time_us      abort;      // Time to abort non-PPM ref change (mirror copy of property REF.ABORT)

    FP32                    v_fltr;                     // Filtered version of vref (V)
    FP32                    v_openloop;                 // Vref request when regulation bypass is active (V)

    int32_t                 dac_raw;                    // Raw DAC value

    struct fp_time          time_running;
    struct fp_time          time_remaining;

    struct ppm_cycling   *  active;                     // Reference function of the current cycle/user
    struct ppm_cycling   *  next;                       // Reference function of the next    cycle/user
};

REF_VARS_EXT struct ref  ref;

// Real-time control data

struct rt_ref_data                                      // Real-time reference from GW
{
    DP_IEEE_FP32         value_gw;                      // Value from GW in IEEE format
    FP32                 time_s;                        // Interpolation time
    FP32                 final;                         // Final RT value at the end of interpolation
    FP32                 step;                          // Interpolation step per millisecond
    FP32                 value;                         // The current RT value
};

REF_VARS_EXT struct rt_ref_data rt_ref_data;

// Global variables defined in ref.c

extern uint32_t(*ref_func[])(void);

// ref.c function and variable declarations

void    Ref(void);
void    RefRtCtrl(void);
void    RefFirstPlateau(void);
uint32_t  RefNone(void);
uint32_t  RefZero(void);
uint32_t  RefPppl(void);
uint32_t  RefTable(void);
uint32_t  RefPlep(void);
uint32_t  RefTest(void);
uint32_t  RefTrim(void);
uint32_t  RefOpenloop(void);
uint32_t  RefEnd(void);
uint32_t  RefStarting(void);
uint32_t  RefStopping(void);
void    RefRefreshValue(struct ref_container * ref_ptr);
void    RefResetValue(struct ref_container * ref_ptr, FP32 reset_value);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation REF_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ref.h
\*---------------------------------------------------------------------------------------------------------*/
