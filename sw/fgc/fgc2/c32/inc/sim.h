/*---------------------------------------------------------------------------------------------------------*\
  File:         sim.h

  Purpose:      FGC DSP Software - VS and Load simulation
\*---------------------------------------------------------------------------------------------------------*/

#ifndef SIM_H   // Header encapsulation
#define SIM_H

#ifdef SIM_GLOBALS
    #define SIM_VARS_EXT
#else
    #define SIM_VARS_EXT extern
#endif

#include <cc_types.h>
#include <libreg1.h>                             // Regulation library
#include <definfo.h>            // for FGC_CLASS_ID
#include <mcu_dsp_common.h>

/*---------------------------------------------------------------------------------------------------------*/

// Max history length for simulated measurement delays - includes the voltage control delay
// Assumes that the maximum voltage control delay is 30 times the voltage regulation period,
// which is true if FGC_VS_MAX_CTRL_DELAY = 0.030

#define V_MEAS_HIST_LEN 30 + (2 + FGC_SIM_NUM_DEN_LEN)
#define I_MEAS_HIST_LEN 30 + 10                         // Max current measurement delay is 10 x voltage reg period
#define B_MEAS_HIST_LEN 30                              // No measurement delay for field

// Field, current and voltage simulation

struct sim_param
{
    uint32_t                      enabled;                // Simulation of measurement control

    FP32                        b;                      // Simulated field
    FP32                        i;                      // Simulated current
    FP32                        v;                      // Simulated voltage

    struct reg_sim_load_vars    load;                   // Circuit load simulation variables
    struct reg_sim_vs_vars      vs;                     // Voltage source simulation variables

    struct reg_delay            vmeas_delay;            // Voltage measurement delay parameters
    struct reg_delay            imeas_delay;            // Current measurement delay parameters
    struct reg_delay            bmeas_delay;            // Field measurement delay parameters
};

SIM_VARS_EXT struct sim_param           sim_param;      // Simulation state variables

struct sim
{
    struct sim_param*           active;
    struct sim_param*           next;
};

SIM_VARS_EXT struct sim                 sim;

/*---------------------------------------------------------------------------------------------------------*/

void SimNextRegState(FP32 vref);
void SimInitDelays(struct sim_param* sim_param, FP32 vref_init, FP32 vmeas_init, FP32 imeas_init, FP32 bmeas_init);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation SIM_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sim.h
\*---------------------------------------------------------------------------------------------------------*/

