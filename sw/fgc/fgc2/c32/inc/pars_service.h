/*---------------------------------------------------------------------------------------------------------*\
  File:         pars_service.h

  Purpose:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PARS_SERVICE_H   // Header encapsulation
#define PARS_SERVICE_H

#ifdef PARS_SERVICE_GLOBALS
    #define PARS_SERVICE_VARS_EXT
#else
    #define PARS_SERVICE_VARS_EXT extern
#endif

#include <cc_types.h>
#include <dpcom.h>      // for struct pars_buf
#include <pars.h>       // for struct cmd_pkt_parse_buf

/*---------------------------------------------------------------------------------------------------------*/

void    ParsePkt        (volatile struct pars_buf *pars, struct cmd_pkt_parse_buf *buf);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation PARS_SERVICE_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars_service.h
\*---------------------------------------------------------------------------------------------------------*/
