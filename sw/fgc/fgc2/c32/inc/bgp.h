/*---------------------------------------------------------------------------------------------------------*\
  File:         bgp.h

  Purpose:      FGC DSP Cycling state variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef BGP_H   // Header encapsulation
#define BGP_H

#ifdef BGP_GLOBALS
#define BGP_VARS_EXT
#else
#define BGP_VARS_EXT extern
#endif

#include <stdint.h>

#include <cc_types.h>
#include <libfg1.h>      // Function generation library: for enum fg_error, struct fg_limits
#include <libfg/trim.h>       // Function generation library: for enum fg_trim_type
#include <libfg/test.h>       // Function generation library: for enum fg_test_type
#include <mcu_dsp_common.h>

// ---------------------------------------------------------------------------------------------------------

BGP_VARS_EXT BOOLEAN bgp_no_pending_action;

// ---------------------------------------------------------------------------------------------------------

void          BgpLoop(void);

void          BgpRefChange(uint32_t user);
uint32_t        BgpCopyRefRunning(void);

enum fg_error BgpCheckIref(struct fg_limits *limits,
                           uint32_t invert_limits,
                           float ref,
                           float rate,
                           float acceleration);
uint32_t        BgpInitNone(uint32_t reg_mode, uint32_t user);
uint32_t        BgpInitZero(uint32_t reg_mode, uint32_t user,
                          enum fg_limits_polarity limits_polarity);
uint32_t        BgpInitPppl(uint32_t reg_mode, uint32_t user,
                          enum fg_limits_polarity limits_polarity);
uint32_t        BgpInitTable(uint32_t reg_mode, uint32_t user, uint32_t func_state,
                           enum fg_limits_polarity limits_polarity);
uint32_t        BgpInitPlep(uint32_t reg_mode, enum fg_limits_polarity limits_polarity);
uint32_t        BgpInitTrim(uint32_t reg_mode, enum fg_trim_type trim_type,
                          enum fg_limits_polarity limits_polarity);
uint32_t        BgpInitTestFunc(uint32_t reg_mode, enum fg_test_type test_type,
                              enum fg_limits_polarity limits_polarity);
uint32_t        BgpInitRamp(uint32_t reg_mode);
uint32_t        BgpTranslateFgError(enum fg_error fg_error);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation BGP_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: bgp.h
\*---------------------------------------------------------------------------------------------------------*/
