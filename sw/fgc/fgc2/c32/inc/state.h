/*---------------------------------------------------------------------------------------------------------*\
  File:         state.h

  Purpose:      FGC DSP State declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef STATE_H   // Header encapsulation
#define STATE_H

#include <stdint.h>

#include <cc_types.h>

void            StatePc                 (uint32_t new_state_pc);
void            StateOp                 (uint32_t new_state_op);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation STATE_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: state.h
\*---------------------------------------------------------------------------------------------------------*/
