/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp_dependent.h

  Contents:     FGC2

  History:

    20 jul 07   doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DSP_DEPENDENT_H // header encapsulation
#define DSP_DEPENDENT_H

#include <cc_types.h>
#include <dpcls.h>

// ----- Floating point conventions -----

FP32            FromIEEE                (DP_IEEE_FP32 ieee32_float);
DP_IEEE_FP32    ToIEEE                  (FP32 ti_float);

#define TIFPZERO                        0x80000000              // TI C32 floating point zero
#define FP32ZERO                        TIFPZERO

// ----- Global macro definitions -----

#define RUNCODE(rc)                     dpcom->dsp.run_code=((rtcom.time_now_ms.ms_time<<16)|(rc & 0x0000FFFF))
#define NOP                             asm(" NOP")
#define ENABLE_INTS                     asm(" OR  06000h,ST")
#define ENABLE_CACHE                    asm(" OR  00800h,ST")
#define DISABLE_INTS                    asm(" AND 0DFFFh,ST")
#define CLEAR_EXT_INTS                  asm(" ANDN 0Fh,IF")
#define CLEAR_EXT_INT2                  asm(" ANDN  4h,IF")
#define ENABLE_EXT_INTS                 asm(" LDI 04h,IE")
#define LEDS_INIT                       asm(" OR 0022h,IOF")
#define LED0_TOGGLE                     asm(" XOR 0004h,IOF")
#define LED1_TOGGLE                     asm(" XOR 0040h,IOF")
#define SPY_SYNCH                       0xFFFFFFFF
#define SPY(out)                        SERIAL_PORT_ADDR(0)->x_data=(uint32_t)out
#define WAITSPY                         while(!((SERIAL_PORT_ADDR(0)->gcontrol & (XSREMPTY|XRDY)) == XRDY))
#define TICTAC                          *((volatile uint32_t *)0x810000)
#define CPU_MODE_C32IRQ_2_MASK16        (CPU_MODEH_C32IRQ_2_MASK8<<8)
#define MS_SLEEP                        TIMER_ADDR(0)->gcontrol|=(HLD_|GO);while(TIMER_ADDR(0)->counter<8000)
#define UPDATE_PARS(pars_idx)           rtcom.par_group_set_mask|=1<<(pars_idx-1)

// DAC_SET: Pass DAC value to MCU, right justified to 32 bit

#define DAC_SET(dac_raw_value)          dpcls->dsp.ref.dac_raw = (dac_raw_value << 12)

//-----------------------------------------------------------------------------------------------------------

#endif  // DSP_DEPENDENT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_dependent.h
\*---------------------------------------------------------------------------------------------------------*/
