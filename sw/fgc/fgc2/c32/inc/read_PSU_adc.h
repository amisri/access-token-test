/*!
 * @file  read_PSU_adc.h
 */

#ifndef READ_PSU_ADC_H
#define READ_PSU_ADC_H

#ifdef READ_PSU_ADC_GLOBALS
    #define READ_PSU_ADC_VARS_EXT
#else
    #define READ_PSU_ADC_VARS_EXT extern
#endif

#include <stdint.h>

#include <cc_types.h>

struct TpowerSupplies
{
    FP32        v5;             // 5V value in mV
    FP32        vp15;           // +15V value in mV
    FP32        vm15;           // -15V value in mV
    uint32_t      fails_counter;
    uint32_t      fails_counter_ana_interfaz;
};



/*!
 * This function is called from InIrqProcessQSPIbusDataSlice() on millisecond 3 out of 20.
 * It calculates and checks the PSU voltages every time (50Hz) and reports the values to the MCU at 5Hz.
 * The fault is latched in the latched status if the limits are exceeded for 50 consecutive samples (1s)
 *
 * The DIPS board has an embedded DIM that is the first on both branch A and branch B,
 * so it is scanned at 100Hz.
 * The analogue channels are different for the two scans (there are 8 channels in all)
 * and are used to measure the voltage source output voltage (Vmeas) twice (x1 and x10)
 * and the +5V and +/-15V PSU voltages for the FGC and the FGC's analogue interface.
 * These are checked by the DSP against threshold values.
 * If the qspi bus is reset, these channels get out of sync for 2 20ms cycles,
 * so the DSP must suppress PSU monitoring for 2 cycles.
 * This is signalled by the MCU using dpcom.mcu.diag.reset.
 * This should be set to the number of cycles that need to be blocked (2).
 * It is decremented by the DSP every 20ms cycle.
 */
void    VerifyPsuVoltages   (void);

READ_PSU_ADC_VARS_EXT struct TpowerSupplies psu;

#endif // READ_PSU_ADC_H

// EOF
