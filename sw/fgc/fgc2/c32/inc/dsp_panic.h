/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp_panic.h

  Purpose:      FGC DSP Software - VS and Load simulation
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DSP_PANIC_H   // Header encapsulation
#define DSP_PANIC_H

#ifdef DSP_PANIC_GLOBALS
#define DSP_PANIC_FGC_EXT
#else
#define DSP_PANIC_FGC_EXT extern
#endif

#include <cc_types.h>

/*---------------------------------------------------------------------------------------------------------*/

// DSP panic codes

enum dsp_panic_codes
{
    DSP_PANIC_ISR_TRAP              = 0x00801000,
    DSP_PANIC_BAD_PARAMETER         = 0x00802000,
    DSP_PANIC_INDEX_OUT_OF_RANGE    = 0x00802001,
    DSP_PANIC_HW_ANATYPE            = 0x00803000,
    DSP_PANIC_HW_ANALOG_CARD        = 0x00803001,
    DSP_PANIC_ISR_SWITCH            = 0x00804000,
    DSP_PANIC_BGP_1HZ_OVERRUN       = 0x00805000,
    DSP_PANIC_REG_V_PERIOD_DIV      = 0x00806000,
    DSP_PANIC_FILTER_20MS_LEN       = 0x00807000,
    DSP_PANIC_ASSERT                = 0x00808000
};

/*---------------------------------------------------------------------------------------------------------*/

void DspPanic(enum dsp_panic_codes panic_code);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation DSP_PANIC_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_panic.h
\*---------------------------------------------------------------------------------------------------------*/

