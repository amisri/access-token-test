/*---------------------------------------------------------------------------------------------------------*\
  File:         property.h

  Purpose:      Property structures of the DSP

\*---------------------------------------------------------------------------------------------------------*/

#ifndef PROPERTY_H
#define PROPERTY_H

#include <cc_types.h>

struct prop
{
    uint32_t              float_f;                        // Data requires conversion from IEEE to TI FP format on FGC2 DSP.
    uint32_t              flags;                          // Property flags
    uint32_t              el_size;                        // Number of long words per element
    uint32_t              n_elements;                     // Number of elements of data in the property
    uint32_t              max_elements;                   // Max number of elements of data in the property
    uint32_t              pars_idx;                       // Parameter function index (1-32)
    void *              value;                          // Data buffer
};

extern struct prop prop[];

// For DSP properties, el_size is the size of an element in units of 32-bit words.
// This is FGC2's legacy, where the DSP's addressing is 32 bits (therefore char, short and imt C types are all 32 bits)
// TODO decide if we keep that strategy on FGC3
//
// The macro PROPERTY_ELEM_SIZEOF is a portable way to compute the size of an element (in size_t unit)

#define PROPERTY_ELEM_SIZEOF(prop_ptr)  (prop_ptr * sizeof(uint32_t))

#endif // End of PROPERTY_H encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: property.h
\*---------------------------------------------------------------------------------------------------------*/
