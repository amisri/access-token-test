/*!
 * @file diag.h
 */

#ifndef DIAG_H      // header encapsulation
#define DIAG_H

#ifdef DIAG_GLOBALS
    #define DIAG_VARS_EXT
#else
    #define DIAG_VARS_EXT extern
#endif

// Includes

#include <cc_types.h>           // basic typedefs
#include <defconst.h>           // for FGC_MAX_DIMS, FGC_N_DIM_ANA_CHANS, FGC_MAX_DIM_BUS_ADDR, FGC_PSU_LOG_LEN
#include <dpcom.h>              // for struct abs_time_us
#include <definfo.h>            // for FGC_CLASS_ID
#include <qspi_bus.h>           // for QSPI_BRANCHES

// Constants

#define DEBUG_LOG_DIPS              1            //! 0=Log first DIM on branch A, 1=Log DIPS board
#define DIPS_MASK                   0x00010001   //! DIPS boards mask (0 and 16)

#define DIMS_IN_BOTH_CYCLES         0            //! array index for flat_qspi_board_is_present_packed[]
#define DIMS_IN_ACTUAL_CYCLE        1
#define DIMS_IN_PREVIOUS_CYCLE      2

#define MISMATCHES_FAULT_THRESHOLD  50           //! If mismatch for > 1s, FGC_LAT_DIMS_EXP_FLT is fired

// External structures, unions and enumerations

#if (FGC_CLASS_ID == 51)

/*!
 * Structure for type PSULOG, used in properties FGC.DEBUG.PSU_5V.LOG, FGC.DEBUG.PSU_NEG_15V.LOG, FGC.DEBUG.PSU_POS_15V.LOG
 */
struct diag_psulog
{
    uint32_t          idx;
    DP_IEEE_FP32    log_ieee[FGC_PSU_LOG_LEN];
};

#endif

/*!
 * @brief For list (0..19) process
 */
struct TDimCollectionInfo
{
    /*!
     * @brief DIAG.BUS_ADDRESSES
     *
     * here the flat qspi bus board number can be
     * 0x00..0x1F are real physical DIM boards (0x00..0x0F on branch A, 0x10..0x1F on branch B)
     * 0x80   b7=1 means a virtual DIM (composite)
     * 0xFF   means not used (empty)
     */
    uint32_t        flat_qspi_board_number[FGC_MAX_DIMS];

    /*!
     * analogue register converted to physical values
     * DIAG.ANASIGS
     */
    FP32          analogue_in_physical[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];

    /*!
     * DIAG.TRIGUS
     */
    uint32_t        trig_us[FGC_MAX_DIMS];

    /*!
     * @brief DIAG.GAIN
     *
     * ToDo check with property fill
     * originally [FGC_MAX_DIM_BUS_ADDR] now fixed to [FGC_MAX_DIMS]
     * gains and offset for the 4 analogue registers in the DIMs
     */
    FP32          gains  [FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];

    /*!
     * DIAG.OFFSET
     */
    FP32          offsets[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];

    /*!
     *  Logging stopping flag
     */
    BOOLEAN       dont_log[FGC_MAX_DIMS];
};

/*!
 * Common diagnostic (DIM) data processing
 */
struct TDimLogData
{
    uint32_t    log[2];  //!< 4 Unsigned Shorts packed into 2 longs
                       //!< We use uint32_t due to the DSP fake uint16_t
};

/*!
 * 256Kb space buffer allocated

 * to handle FGC_MAX_DIMS (20) slots of FGC_LOG_DIM_LEN (1600) elements
 * each element has 8 bytes data

 * FGC_MAX_DIMS * FGC_LOG_DIM_LEN * sizeof(TDimLogData)
 *     20       *    1600         *        8              = 256000
 *
 * struct TDimLogData        log_space[FGC_MAX_DIMS * FGC_LOG_DIM_LEN];  // 32000
 * struct TDimLogData        log_space[32768];                           // real allocated space
 */
struct TDimLogSpace
{
    struct TDimLogData        log_space[FGC_MAX_DIMS][FGC_LOG_DIM_LEN];   // 32000
};


struct TQspiMiscellaneousInfo
{
    uint32_t      expected[QSPI_BRANCHES];        //!< DIAG.DIMS_EXPECTED
    uint32_t      detected[QSPI_BRANCHES];        //!< DIAG.DIMS_DETECTED
    uint32_t      expected_detected_mismatches;   //!< DIAG.DIMS_EXP_ERRS
    uint32_t      mismatches_level;               //!< counter to fire the fault FGC_LAT_DIMS_EXP_FLT

    uint32_t      flat_qspi_board_is_present_packed[3]; //<! DIM present bit packed in uint32_t
                                                      //!< 0 is [1] & [2], DIMS present in the two consecutive cycles
                                                      //!< 1 is DIMS present in actual cycle
                                                      //!< 2 is DIMS present in previous cycle

    uint32_t      flat_qspi_board_is_valid_packed;      //!< DIM board is valid bit packed in uint32_t
    uint32_t      flat_qspi_not_triggered_packed;       //!< not triggered bit packed in uint32_t
    uint32_t      flat_qspi_non_valid_counts[FGC_MAX_DIM_BUS_ADDR];       //!< DIAG.DIM_SYNC_FLTS

    uint32_t      scantime_us[QSPI_BRANCHES];           //!< Scan start times relative to start of second
    uint32_t      prev_scantime_us[QSPI_BRANCHES];      //!< Previous scan times

    uint32_t      unix_time;                            //!< Unix time for current cycle
    uint32_t      prev_unix_time;                       //!< Unix time for previous cycle

    //! DIM registers analog_0..3 + trigCounter for all the potential DIM boards
    uint32_t      analog_regs_from_all_dim_boards[FGC_N_DIM_ANA_CHANS+1][FGC_MAX_DIM_BUS_ADDR];
};


// External function declarations

/*!
 * This function is called from InIrqProcessQSPIbusDataSlice() on millisecond 18 of 20 (just after the last scan is received).
 * It must check to see how many DIMs are present on each bus and check for DIM sync faults.
 * If the number of DIMs detected is not the number expected, it sets DIMS_EXP_FLT in latched status.
 *
 * the QSPI bus has 2 branches [A,B] with up to 16 DIM boards connected to each branch,
 * each DIM card has 7 registers (of 16 bits)
 *
 * the 7 registers are received in this order
 *        0 - Dig0        DIM register ID:0
 *        1 - Dig1        DIM register ID:1
 *        2 - Ana0        DIM register ID:4
 *        3 - Ana1        DIM register ID:5
 *        4 - Ana2        DIM register ID:6
 *        5 - Ana3        DIM register ID:7
 *        6 - TrigCounter DIM register ID:2
 *                         there is no ID:3
 */
void ValidatePresentDimBoards(uint32_t scan_step);

/*!
 * This function processes the QSPI data acquired by the MCU. In particular, it stores and processes
 * the DIM's analogue signals and trigger counters. The digital signals are stored on the MCU side.
 * Millisecond 0 of 10 is skipped because it is too busy already.
 */
void InIrqProcessQSPIbusDataSlice(void);

// External variable definitions

DIAG_VARS_EXT struct TQspiMiscellaneousInfo     qspi_misc;
DIAG_VARS_EXT struct TDimCollectionInfo         dim_list;
DIAG_VARS_EXT struct TDimLogSpace *             dim_log;    //! Pointer to DIM logs (256Kb buffer)


#endif  // DIAG_H end of header encapsulation

// EOF
