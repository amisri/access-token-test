/*---------------------------------------------------------------------------------------------------------*\
  File:         pfloat.h

  Purpose:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PFLOAT_H   // Header encapsulation
#define PFLOAT_H

#include <cc_types.h>
#include <dpcom.h>                              // for struct pfloat_buf

void    PrintFloat(volatile struct pfloat_buf *pfloat);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation PFLOAT_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pfloat.h
\*---------------------------------------------------------------------------------------------------------*/
