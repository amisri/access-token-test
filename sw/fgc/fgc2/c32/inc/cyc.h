/*---------------------------------------------------------------------------------------------------------*\
  File:         cyc.h

  Purpose:      FGC2 Class 51/53 DSP Cycling state variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CYC_H   // Header encapsulation
#define CYC_H

#ifdef CYC_GLOBALS
    #define CYC_VARS_EXT
#else
    #define CYC_VARS_EXT extern
#endif

// Specific macros for data buffer that are to be mapped in external RAM

#ifdef CYC_GLOBALS_FAR
    #define CYC_VARS_EXT_FAR
#else
    #define CYC_VARS_EXT_FAR extern
#endif

/*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>
#include <dpcom.h>
#include <defconst.h>   // for FGC_MAX_USER_PLUS_1
#include <mcu_dsp_common.h>
#include <dsp_time.h>   // For struct fp_time
#include <ref.h>
#include <meas.h>

/*---------------------------------------------------------------------------------------------------------*/
#define TRIGG_LOG       TRUE
#define DONT_TRIGG_LOG  FALSE


#if   (FGC_CLASS_ID == 53)
    // Same as the default list but adding B_MEAS warning

    #define STRETCHED_CYC_WARNINGS (  FGC_WRN_TIMING_EVT    \
                                    | FGC_WRN_CYCLE_START   \
                                    | FGC_WRN_I_MEAS        \
                                    | FGC_WRN_REG_ERROR     \
                                    | FGC_WRN_B_MEAS        )
#elif (FGC_CLASS_ID == 51)
    // Class 51 does not support cycling therefore it does not define the CYCLE_START and TIMING_EVT warnings

    #define STRETCHED_CYC_WARNINGS (  FGC_WRN_I_MEAS \
                                    | FGC_WRN_REG_ERROR )
#else
    // Default list

    #define STRETCHED_CYC_WARNINGS (  FGC_WRN_TIMING_EVT    \
                                    | FGC_WRN_CYCLE_START   \
                                    | FGC_WRN_I_MEAS        \
                                    | FGC_WRN_REG_ERROR )
#endif

#define MAX_CONSECUTIVE_WRN     5                       // Maximum number of consecutive warnings without a
                                                        // clean cycle. Reaching that number causes a fault,
                                                        // FGC_CYC_FLT_CYC_WARN_LIM, and should end cycling.

// Exceptions to the consecutive warnings fault mechanism. Those warnings are not counted.

#define CONSECUTIVE_WRN_IGNORE ( FGC_CYC_WRN_SC_EVT_EARLY | FGC_CYC_WRN_REF_NOT_ARMED | FGC_CYC_WRN_REF_DISABLED)

// [FGC3] Enumeration used for variable cyc.next_cycle_state.
// This enumeration represents the states that the DSP must go through in sequence to prepare for a new
// cycle. The user event is received 200ms prior to the start of a new cycle. On C0 (start of cycle), there
// is a cycle check to assert that cyc.next_cycle_state is NEXT_CYCLE_READY.

enum next_cycle
{
    NEXT_CYCLE_WAIT_USER_EVENT,                 // Wait for the user event
    NEXT_CYCLE_PREPARE_REF,                     // Prepare the reference function    for the next cycle
    NEXT_CYCLE_PREPARE_REG,                     // Prepare the regulation parameters for the next cycle
    NEXT_CYCLE_PREPARE_REG_REQUEST,             // Reg request sent
    NEXT_CYCLE_READY                            // Next cycle is ready
};

// [FGC2] Enumeration for the state of the copy of the reference function from the ppm->armed structure to the
// ppm->cycling. That copy will occur during ms 0 and ms 1 at the beginning of a new cycle, if the armed reference
// has changed for the cycle user.

enum cyc_prop_copy
{
    REF_CYCLING_OK,
    REF_CYCLING_COPY_FROM_ARMED,
    REF_CYCLING_COPY_ONGOING
};

/*---------------------------------------------------------------------------------------------------------*/

// A note on cyc.timing_user, cyc.user, cyc.ref_user. On a PPM device usually all three are equal, but
// they can differ in some situations:
//
// cyc.timing_user = the user received from the external timing (e.g. PS timing), in NORMAL     mode
//                 = the simulated user received from the MCU,                    in SIMULATION mode
//
// cyc.user        = cyc.timing_user, if DEVICE.PPM == ENABLED
//                 = 0,               otherwise
//
// cyc.ref_user    = REG.TEST.REF_USER, if cyc.user == REG.TEST.USER && REG.TEST.REF_USER != 0
//                 = cyc.user,          otherwise
//

struct cyc
{
    struct abs_time_us  start_time;                             // Time of start of this cycle
    struct abs_time_us  start_time_next;                        // Time of start of the next cycle
    struct fp_time      time;                                   // Time since start of cycle (s)
    uint32_t              timing_user;                            // Cycle user index from timing
    uint32_t              user;                                   // Cycle user index (always zero if PPM disabled)
    uint32_t              ref_user;                               // Cycle user index for reference function
    uint32_t              next_user;                              // Next cycle user index (always zero if PPM disabled)
    uint32_t              next_ref_user;                          // Next cycle user index for reference function
    uint32_t              ref_func_type;                          // Function type for the cycle
    uint32_t              start_cycle_evt_f;
    uint32_t              ssc_evt_f;                              // Start super-cycle event received this cycle
    enum next_cycle     next_cycle_state;                       // State of the next cycle (Only used on FGC3)
    BOOLEAN             trigger_bgp_prepare_ref_f;              // Trigger the BGP task to prepare the reference function
    uint32_t              biv_check_f;                            // BIV checks enabled flag
    FP32                biv_check_time;                         // Time for BIV checks (s)
    uint32_t              stretched_warnings;                     // Check warnings (stretches to >= 1 supercycle)
    uint32_t              dysfunction_code;                       // Cycle warning or fault
    uint32_t              dysfunction_is_a_fault;                 // warning (0) or fault (1)
    uint32_t              consecutive_wrn_cnt;                    // Count the number of consecutive warnings
    enum cyc_prop_copy  ref_copy_state[FGC_MAX_USER_PLUS_1];    // State of the copy from ppm->armed to ppm->cycling
};


// RMS Current measurement

struct i_rms
{
    FP32        sc_i_meas_sqr;          // Imeas^2 accumulator for current super-cycle
    FP32        sc_length_s;            // Super-cycle length accumulator (in seconds)
};


// Capture log

struct capture
{
   uint32_t       freeze_f;               // Capture log freeze flag
   uint32_t       n_samples_recorded;     // Number of samples recorded
};

struct capture_buf
{
    FP32        ref;
    FP32        v_ref;
    FP32        b_meas;
    FP32        i_meas;
    FP32        v_meas;
    FP32        err;
    FP32        mpx0;
    FP32        mpx1;
    FP32        mpx2;
};

// --------------------------------------------------------------------------
// Cycle log (e.g. LOG.OASIS property)
//

#define CYC_LOG_MAX_SIGNALS     (2)

// enum log_idx_color: buffer index colouring, to distinguish between one pass on the circular buffer and the next

enum log_idx_color
{
    LOG_ODD   = 1,                      // 1st, 3rd, 5th, etc. pass on the circular buffer
    LOG_EVEN                            // 2nd, 4th, 6th, etc. pass on the circular buffer
};

// struct cyc_log_index: An index in the circular buffer, along with its colouring

struct cyc_log_index
{
    uint32_t              index;          // Index value
    enum log_idx_color  color;          // Binary state to distinguish between one pass on the circular buffer and the next
};

// Generic log control structure

struct cyc_log_ctrl
{
    struct cyc_log_index    write_idx;                          // Circular buffers write index
    struct cyc_log_index    last_start_idx;                     // Index of the last start index (usually, C0)
    uint32_t                  max_index;                          // Circular buffers max index (= max size)
    uint16_t                  nb_signals;
    FP32*                   signal_src_ptr[CYC_LOG_MAX_SIGNALS];
    FP32*                   log_buf_ptr   [CYC_LOG_MAX_SIGNALS];
};

/*---------------------------------------------------------------------------------------------------------*/

void            CycEnter                (void);
void            CycExit                 (void);
void            CycCancelNextUser       (void);
void            CycStartSuperCycle      (void);
void            CycStartCycle           (void);
void            CycCopyRefFunction      (void);
uint32_t          CycEnableRefFunction    (void);
void            CycSendMCUCheckResults  (void);
void            CycResetCheckWarning    (uint32_t user);
void            CycResetCheckFault      (void);
void            CycSaveCheckWarning     (uint32_t trig_log_capture_f, uint32_t check, uint32_t warning, FP32 data0, FP32 data1, FP32 data2, FP32 data3);
void            CycSaveCheckFault       (uint32_t check, uint32_t warning, FP32 data0, FP32 data1, FP32 data2, FP32 data3);
void            CycCaptureLogStart      (void);
void            CycCaptureLog           (void);
void            CycLogAbsMaxErr         (void);
void            CycLogSignals           (struct cyc_log_ctrl * cyc_log_ctrl);

/*---------------------------------------------------------------------------------------------------------*/

CYC_VARS_EXT struct cyc         cyc;
CYC_VARS_EXT struct i_rms       i_rms;
CYC_VARS_EXT struct capture     capture;

#if FGC_CLASS_ID == 53
    #define LOG_CAPTURE_BASEADDR    0x10100                 // Address of start of capture log on FGC2
#else

    // Log buffer for property LOG.CAPTURE

    CYC_VARS_EXT_FAR struct capture_buf log_capture_buffer[FGC_LOG_CAPTURE_LEN];    // total size = FGC_LOG_CAPTURE_BUF_LEN
                                                                                    //            = FGC_LOG_CAPTURE_LEN * sizeof(struct capture_buf)
#endif

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation CYC_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cyc.h
\*---------------------------------------------------------------------------------------------------------*/
