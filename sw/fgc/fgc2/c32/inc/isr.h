/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp\inc\isr.h

  Purpose:      FGC DSP Interrupt declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ISR_H   // Header encapsulation
#define ISR_H

#ifdef ISR_GLOBALS
    #define ISR_VARS_EXT
#else
    #define ISR_VARS_EXT extern
#endif

#include <cc_types.h>
#include <isr_plat.h>
#include <reg.h>
#include <sim.h>
#include <ref.h>
#include <cal.h>
#include <mcu_dsp_common.h>

#pragma         INTERRUPT               (IsrTrap);      // #pragma INTERRUPT is legitimate on TMS320C32 and TMS320C6x

void            IsrMain                 (void);
void            IsrRegStateB            (void);
void            IsrRegStateI            (void);
void            IsrRegStateV            (void);
void            IsrDacSet               (FP32 v_dac);
void            IsrDacDirect            (int32_t ref_dac);
void            IsrLogging              (void);
void            IsrTrap                 (void);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation ISR_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp\src\isr.h
\*---------------------------------------------------------------------------------------------------------*/
