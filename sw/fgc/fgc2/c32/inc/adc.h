/*---------------------------------------------------------------------------------------------------------*\
  File:         adc.h

  Purpose:      FGC2 ADC support
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ADC_H   // Header encapsulation
#define ADC_H

#ifdef ADC_GLOBALS
    #define ADC_VARS_EXT
#else
    #define ADC_VARS_EXT extern
#endif

#include <cc_types.h>
#include <libcal.h>
#include <defconst.h>   // for FGC_N_ADCS

/*!
 * ADC interface type
 */
enum ADC_INTERFACE_TYPE
{
    ADC_INTERFACE_SD_22,
    ADC_INTERFACE_SAR_400,
    ADC_INTERFACE_SD_350 ,
    ADC_INTERFACE_SD_351 = ADC_INTERFACE_SD_350
};

//
// ADC constants
//

#define FLT_CLIP                        36000000        // Filter clip level
#define FLT_MAX_INT                     9000000         // 100% value for internal ADC
#define FLT_MAX_EXT                     30000000        // 100% value for external ADC
#define VRAW_SUM_HIST_LEN               100             // 20ms filter history length

// Mask of the channel status flags that can trigger a reset of the ADC filters

#define ADC_RESET_STATUS_MASK (FGC_MEAS_SIGNAL_STUCK|FGC_MEAS_SIGNAL_NOISE|FGC_MEAS_SIGNAL_JUMP|FGC_MEAS_ADC_BUSERR)

#define ADC_MIN_DSP_RESET_DELAY_S       120             // Minimum delay between two DSP-triggered ADC resets (s)

// ADC variables

struct adc
{
    uint32_t              type;                           // ADC interface type, effectively enum ADC_INTERFACE_TYPE
    uint32_t              stuck_limit;                    // Limit for number of samples with no change
    uint32_t              i_dcct_match_f;                 // I_dcct match flag
    uint32_t              last_dsp_reset;                 // Unix time of the last ADC filter reset executed
                                                        // after being triggered by the DSP
    uint32_t              reset_hist_idx;                 // index used by property.adc.reset_hist_flags[]
    uint32_t              reset_req_copy;                 // Local copy of the dpcsl->dsp.adc.reset_req

    struct filter_20ms
    {
        uint32_t              history_idx;                    // Current index in adc_chan.filter_20ms.vraw_sum_history[]
        uint32_t              history_offset;                 // Index offset = 10, 20, ... 100 depending on the regulation period
        FP32                time_ratio;                     // = 10.4 ms / (history_offset * 1 ms)
    }                   filter_20ms;                    // Filter 20ms
};

ADC_VARS_EXT struct adc adc;

// Structure used to hold the filtered, calibrated current measurements

struct filtered_cal_current
{
    struct cal_current cal;                 // Calibrated measurement
    int32_t             err_downcounter;     // Equal to zero if this measurement is based on
                                            // I_MEAS_OK samples only
};

// ADC channels

struct adc_chan
{
    int32_t                      adc_direct;                     // Low word of raw ADC value
    int32_t                      adc_raw;                        // Raw ADC value per millisecond
    int32_t                      last_adc_raw;                   // Raw ADC value for previous millisecond
    uint32_t                      stuck_counter;                  // Number of samples with no change
    signed                      shift;                          // Bits to shift right to get raw data in bits
    uint32_t                      st_check;                       // Signal tests status (jump, stuck, noise)
    uint32_t                      last_st_check;                  // Previous signal tests status
    uint32_t                      st_meas;                        // ADC  status bits
    uint32_t                      st_dcct;                        // DCCT status bits
    uint32_t                      i_meas_bad_counter;             // Bad Imeas millisecond down counter
    struct cal_current          meas;                           // Calibrated measurement per acquisition period
    struct filtered_cal_current ms20;                           //  20 ms measurements (@ l KHz, with prediction)
    struct filtered_cal_current ms200;                          // 200 ms measurements (@ 5 Hz without prediction)
    struct filtered_cal_current s;                              //   1 s  measurements (@ l Hz without prediction)
    struct cal_current          max;                            // Max values for peak-peak calculation
    struct cal_current          min;                            // Min values for peak-peak calculation

    struct filter_20ms_chan
    {
        int32_t                      vraw_buffer[20];                        // 20ms sliding window filter buffer
        int32_t                      vraw_sum;                               // Sum of the last 20 Vraw values
        int32_t                      vraw_sum_history[VRAW_SUM_HIST_LEN];    // 20ms filter's history circular buffer
    }                           filter_20ms;                    // 20ms filter

    FP32                        sum_raw_200ms;                  // 200ms average accumulator
    int32_t                      offset_raw_200ms;               // 200ms average offset
    uint32_t                      raw_pp_200ms;                   // 200ms peak-peak
    FP32                        sum_raw_1s;                     // 1s average accumulator
    int32_t                      offset_raw_1s;                  // 1s average offset
    BOOLEAN                     i_meas_ok_200ms;                // True if all I_MEAS_OK since the last 200ms boundary
    BOOLEAN                     i_meas_ok_1s;                   // True if all I_MEAS_OK since the last 1s boundary
};

ADC_VARS_EXT struct adc_chan adc_chan[FGC_N_ADCS];              // Two ADC channels on FGC2 (A & B)

// External function declarations

/*!
 * This function initialises access to the ADCs
 */
void            AdcInit                 (void);
void            AdcRead                 (void);
void            AdcFilter               (void);
void            AdcMpx                  (void);
void            AdcFifo                 (void);
void            AdcGatePars             (void);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation ADC_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: adc.h
\*---------------------------------------------------------------------------------------------------------*/
