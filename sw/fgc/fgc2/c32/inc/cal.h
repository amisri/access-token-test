/*---------------------------------------------------------------------------------------------------------*\
  File:         fgc2\inc\cal.h

  Purpose:      FGC2 Class 51/53 DSP calibration variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CAL_H   // Header encapsulation
#define CAL_H

#ifdef CAL_GLOBALS
    #define CAL_VARS_EXT
#else
    #define CAL_VARS_EXT extern
#endif

#include <cc_types.h>
#include <defconst.h>   // for FGC_N_ADCS
#include <libcal.h>     // for struct cal_dac
#include <adc.h>        // for struct filtered_cal_current

/*---------------------------------------------------------------------------------------------------------*/

// Calibration variables

struct cal_factors
{
    struct cal_adc          adc_factors             [FGC_N_ADCS];   // ADC (int or ext) calibration at ambient temperature
    struct cal_dcct         dcct_factors            [2];            // DCCT  calibration at ambient temperature
    struct cal_dac          dac_factors;                            // DAC   calibration (no temperature compensation)
};

struct cal
{
    uint32_t              ongoing;                        // Calibration active counter
    uint32_t              imeas_zero_f;                   // Imeas zeroed request from MCU
    uint32_t              step_200ms;                     // Counter of 200ms steps used during calibration
    FP32                t_adc_C;                        // Filtered ADC16 temperature       (units: C)
    FP32                t_dcct_C[2];                    // Filtered DCCT electronics temperature (units: C)
    DP_IEEE_FP32        days;                           // Date stamp for calibration (days since 1-1-1970)
    DP_IEEE_FP32        seconds;                        // Time stamp for calibration (secs since midnight)
    struct cal_factors *active;                         // Active calibration parameters
    struct cal_factors *next;                           // Next   calibration parameters (= cal.active on FGC2)
    BOOLEAN             ongoing_dac_cal;                // Flag indicating an ongoing calibration of the DAC

    // NB: The three arrays of cal_event structures below are nothing but a copy of the ADC/DCCT factors in
    // shared memory: dpcls->mcu.cal.adc.internal/external, dpcls->mcu.cal.dcct.
    // This is to cope with the floating point format of C32 (that differs from IEEE standards)

    struct cal_event    normalized_adc16_factors[FGC_N_ADCS];   // ADC16 calibration factors at T0
    struct cal_event    normalized_adc22_factors[2];            // ADC22 calibration factors
    struct cal_event    normalized_dcct_factors [2];            // DCCT  calibration factors at T0

    struct cal_average_v_raw    average_v_raw[FGC_N_ADCS];      // Structures used to compute the average
                                                                // Vraw during calibration

    FP32                vraw[2][CAL_NUM_ERRS];                  // Vraw values used for external ADC or DCCT calibration
};

CAL_VARS_EXT struct cal_factors cal_factors;    //!< Active and next calibration factors
CAL_VARS_EXT struct cal         cal;            //!< Calibration variables structure

// External function declarations

void    CalInit                 (void);
void    CalMeas                 (int32_t v_raw_int, uint32_t channel_idx, BOOLEAN sim_flag, struct cal_current *meas);
void    CalCalc                 (void);
BOOLEAN CalAuto                 (void);
void    CalAutoBothAdc16s       (void);
void    CalAutoInternalAdc      (enum cal_idx cal_signal);
void    CalAutoExternalAdc      (enum cal_idx cal_signal);
void    CalAutoDcct             (enum cal_idx cal_signal);
BOOLEAN CalAutoDac              (void);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation CAL_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: fgc2\inc\cal.h
\*---------------------------------------------------------------------------------------------------------*/
