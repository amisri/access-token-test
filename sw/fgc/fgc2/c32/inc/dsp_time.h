/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp_time.h

  Purpose:      Floating point time for the DSP.
                Time conversion for the DSP.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DSP_TIME_H
#define DSP_TIME_H

#include <stdint.h>
#include <cc_types.h>

// ---------------------------------------------------------------------------------------------------------
// Floating point time structure
//

// Very often in the DSP the reference time will be expressed in seconds and will be a multiple of a number
// of iteration periods. The time in seconds being a floating-point value, one must be careful with the problem
// that arises adding a small increment (e.g. one iteration period) to an already large time. One solution to
// deal with that issue is to store the reference time as a combination of an integer (the number of periods)
// and a floating-point value (= iteration period * number of periods). Then on each iteration it is the
// integer counter that is incremented and not the floating-point value. The data structure below is intended
// to be used for that purpose:

struct fp_time
{
    uint32_t      period_counter;
    FP32        s;
};

// Additional notes for FGC2:
// Calculating the floating-point value will be done as follows on FGC2 (with a C32 DSP):
//
//    fp_time.s = (FP32)fp_time.period_counter * period;                // On FGC2, period = 0.001 = 1 ms
//
// Since the period_counter is converted to a 32-bit float, with a 23+1 significant (whether it is in IEEE
// format or in TI format on C32), it will be exactly represented as a floating-point value if it is lesser
// than 2^24, but it will not be accurate if greater than 2^24 = 16777216. With an iteration period of 1ms
// (as on FGC2), that represents a reference time of 4 hours and 40 minutes. Therefore a loss of precision
// will be observed whenever a reference exceeds that duration. In addition to that, the multiplication
// itself will introduce rounding errors even below that limit. The array below provides the resolution of
// the floating point time in different time ranges:
//
// Time resolution with 32-bit floating-point time:
// ------------------------------------------------
//
// Between  4096s (1h08) and  8191s (2h16) : around    0.5 ms (= 2^-11 s)
// Between  8192s (2h16) and 16383s (4h32) : around    1.0 ms (= 2^-10 s)
// Above   16384s (4h32)                   : more than 2.0 ms (= 2^-9 s)


// ---------------------------------------------------------------------------------------------------------
// Time conversion
//

// DSP_ROUND_TIME_MS converts a floating time in seconds to a rounded integer time in ms.

#define DSP_ROUND_TIME_MS(time_s) ((uint32_t)( (time_s) * 1000 + 0.5 ))

// DSP_ROUND_TIME_US converts a floating time in seconds to a rounded integer time in us.

#define DSP_ROUND_TIME_US(time_s) ((uint32_t)( (time_s) * 1000000 + 0.5 ))

// DSP_ROUND_TIME_VP converts a floating time in seconds to a rounded integer time in units of the V regulation period.

#define DSP_ROUND_TIME_VP(time_s) ((uint32_t)( (time_s)/(FP32)property.reg.v.rst_pars.period + 0.5 ))

// DSP_CEIL_TIME_VP converts a floating time in seconds to the smallest following integer time in units of the V regulation period.

#define DSP_CEIL_TIME_VP(time_s)  ((uint32_t)( (time_s)/(FP32)property.reg.v.rst_pars.period + 0.99999 ))

// DSP_TIME_GREATER_THAN tests if time_1 > time_2 + epsilon, with epsilon the rounding error that we take equal
// to half a V regulation period. Times are in seconds. DSP_TIME_LOWER_THAN tests if time_1 + epsilon < time_2

#define DSP_TIME_GREATER_THAN(time_1, time_2)  ((time_1) > (time_2) + property.reg.v.rst_pars.period / 2)
#define DSP_TIME_LOWER_THAN(time_1, time_2)    DSP_TIME_GREATER_THAN(time_2, time_1)

#endif // End of DSP_TIME_H encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_time.h
\*---------------------------------------------------------------------------------------------------------*/
