/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp\inc\isr_plat.h

  Purpose:      FGC DSP Interrupt declarations for FGC2
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ISR_PLAT_H   // Header encapsulation
#define ISR_PLAT_H

#ifdef ISR_PLAT_GLOBALS
    #define ISR_PLAT_VARS_EXT
#else
    #define ISR_PLAT_VARS_EXT extern
#endif

#include <cc_types.h>

struct isr
{
    struct isr_trigger
    {
        BOOLEAN         calibration_refresh;
    } trigger;
};

ISR_PLAT_VARS_EXT struct isr isr;


#pragma         INTERRUPT               (IsrMst);

void            IsrMst                  (void);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation ISR_PLAT_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp\src\isr_plat.h
\*---------------------------------------------------------------------------------------------------------*/
