/*----------------------------------------------------------------------------*\
 File:          51/out2mot.cmd

 Purpose:       Class 51 FGC2_LHC_PC DSP Software HEX30 drive file

 Notes:         This drive file will generate S-Records from the COFF file
\*----------------------------------------------------------------------------*/

/* Memory Definition */

ROMS
{
    EPROM:      org = 0000H, length = 18000H    /* 96KB starting at 0 */
}

/* End of file: 51/out2mot.cmd */

