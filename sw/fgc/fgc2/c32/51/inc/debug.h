/*---------------------------------------------------------------------------------------------------------*\
  File:		debug.h

  Purpose:	FGC

  History:

    8 feb 2011	doc	Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DEBUG_H	// header encapsulation
#define DEBUG_H

#ifdef DEBUG_GLOBALS
    #define DEBUG_VARS_EXT
#else
    #define DEBUG_VARS_EXT extern
#endif

#include <cc_types.h>



#if (!defined(__RX610__) && !defined(__M16C62__) && !defined(__HC16__)) \
    || defined(__RX610__) || defined(__M16C62__) || defined(__HC16__)
#endif

// this to encapsulate modifications during debugging
// #define      DEBUGGING_MODIFICATION

//-----------------------------------------------------------------------------------------------------------

// Default debug space

struct  db
{
    union
    {
        uint32_t         i[DB_DSP_MEM_LEN];      // Unsigned integers
        FP32           f[DB_DSP_MEM_LEN];      // Floating point
    } buf;

    uint32_t            idx;
};

DEBUG_VARS_EXT struct db       db;

//-----------------------------------------------------------------------------------------------------------

#endif	// DEBUG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: debug.h
\*---------------------------------------------------------------------------------------------------------*/
