/*------------------------------------------------------------------------------*\
 File:          51\link.cmd

 Purpose:       Class 51 DSP Program Linker file for FGC2 Hardware

 Notes:         The program is built with the register parameter passing model

                Ram pages 4-7 are used for the DIM Logs

                The stack is NOT in internal SRAM to protect against radiation.
\*------------------------------------------------------------------------------*/

/* Memory Map Definition */

/*
 * Note: The 2KB stack for classes 53 and 59 is in internal SRAM at 0x87FE00 as there is not a
 *       radiation problem for these systems and internal SRAM is faster than external SRAM.
 *       The 2KB stack for classes 51 is in external SRAM at 0xFE00.
 *       See comments in file dsp/fgc2/src/lib.c for more information about the C32 memory map.
 */

MEMORY
{
    VECS:       org = 000000H   len = 00040H    /* 256B:  Reset and Interrupt Vectors   */
    CODE:       org = 000040H   len = 05FC0H    /* ~96KB: Program Code and init data. Edit out2mot.cmd file accordingly */
    VARS:       org = 006000H   len = 09E00H    /* 158KB: Program variables             */
    STACK:      org = 00FE00H   len = 00200H    /* 2KB:   Stack at top of page 3        */
}

/* Section Allocation */

SECTIONS
{
     vecs:      > VECS                                  /* Vector table (vecs.asm)      */
    .cinit:     > CODE                                  /* C Initialisation data        */
    .data:      > CODE                                  /* Asm data                     */
    .const:     > CODE                                  /* FP constants and switch tbls */
    .text:      > CODE                                  /* Program code                 */
    .stack:     > STACK                                 /* Program stack                */
    .bss:       > VARS                                  /* Global and Static variables  */
}

/* End of file: 51\link.cmd */

