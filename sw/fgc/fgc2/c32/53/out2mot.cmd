/*----------------------------------------------------------------------------*\
 File:          53/out2mot.cmd

 Purpose:       Class 53 FGC2_POPS DSP Software HEX30 drive file

 Notes:         This drive file will generate S-Records from the COFF file
\*----------------------------------------------------------------------------*/

/* Memory Definition */

ROMS
{
    EPROM:      org = 0000H, length = 1C000H    /* 112KB starting at 0 */
}

/* End of file: 53/out2mot.cmd */

