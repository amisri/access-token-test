/*---------------------------------------------------------------------------------------------------------*\
  File:         cyc_class.h

  Purpose:      FGC2 Class 53 DSP Cycling state variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CYC_CLASS_H   // Header encapsulation
#define CYC_CLASS_H

// constants

#define MAX_IRATE_ERR                    1000.0               // Maximum Irate measurement error for BIV checks
#define MAX_BMEAS_ERR                    50.0                 // Maximum Bmeas measurement error for BIV checks
#define MAX_BRATE_ERR                    1000.0               // Maximum Brate measurement error for BIV checks

#define BMEAS_CAL_WHITERABBIT            0.0001               // Gauss per raw bit (White Rabbit)

// Functions

void    CycBIVchecks    (void);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation CYC_CLASS_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cyc_class.h
\*---------------------------------------------------------------------------------------------------------*/
