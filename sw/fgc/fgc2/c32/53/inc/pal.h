/*---------------------------------------------------------------------------------------------------------*\
  File:         pal.h

  Purpose:      FGC2 Class 53 POPS Acquisition Link (PAL) support
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <cc_types.h>
#include <defconst.h>   // for FGC_N_PAL_CHANS

#ifdef PAL_GLOBALS
    #define PAL_VARS_EXT
#else
    #define PAL_VARS_EXT extern
#endif


// PAL constant declarations

#define PAL_OPT_SWITCHES                1                       // Propery function codes for PAL...
#define PAL_FPGA_VERSION                2
#define PAL_MC_VERSION                  3
#define PAL_MC_PLL_VERSION              4
#define PBL_OPT_SWITCHES                200
#define PBL_FPGA_VERSION                201
#define PBL_MC_TASK_VERSION             202
#define PBL_MC_USB_VERSION              203
#define PAL_SET_FILTER_GAIN             128
#define PAL_SET_CAL                     125
#define PAL_V_MEAS_POPS                 30
#define PAL_FILTER_GAIN                 32767998.0              //!< PAL filter gain calculated externally

// Minimum voltages for the 2 charger capacitors, and for the 4 floating ones

#define PAL_MIN_VCAP_CHARGER            3100.0
#define PAL_MIN_VCAP_FLOATING            300.0

                                                                // PBL status bits...
#define PAL2PBL_CRC                     0x00200000
#define PAL2PBL_NODAT                   0x00400000
#define FGC2PBL_NODAT                   0x00800000
                                                                // PAL status bits...
#define PAL_BOOT                        0x00010000              // Note: PAL_BOOT is not used by SW
#define PAL_VME_IRQ                     0x00080000
#define PBL2PAL_CRC                     0x00200000
#define PBL2PAL_NODAT                   0x00400000
#define PBL2PAL_NOSYN                   0x00800000

// PAL variables

struct  pal
{
    uint32_t            prop_func;                      // PAL/PBL property function
    uint32_t            prop_idx;                       // PAL property data index
    DP_IEEE_FP32        prop_data;                      // PAL property data sent by FGC
    DP_IEEE_FP32        prop_data_rcv;                  // PAL property data recevied by FGC
    uint32_t            pops_control;                   // POPS control byte
    uint32_t            pops_status;                    // POPS status word
    uint32_t            link_check;                     // Copy of  sent on spy link
    uint32_t            prev_link_check;                // Copy of control sent on spy link
    uint32_t            vme_irq_enabled;                // POPS has enabled VME IRQs from the PAL
    uint32_t            err_downcounter;                // Error down counter (in milliseconds)
    uint32_t            pam_nosig_flt_f;                // Boolean indicating a fault on one of the PAM signals

    int32_t             b_raw;                          // Raw operational field measurement
    FP32                v_pops;                         // Voltage as measured by POPS

    FP32                v_pam   [FGC_N_PAL_CHANS];      // PAM ADC values calibrated into volts
    FP32                offset  [FGC_N_PAL_CHANS];      // Acquistion channel offset
    FP32                gain_pos[FGC_N_PAL_CHANS];      // Acquistion channel gain_pos
    FP32                gain_neg[FGC_N_PAL_CHANS];      // Acquistion channel gain_neg
};

PAL_VARS_EXT    struct pal      pal;

// pal53.c function declarations

void     PalReset        (void);
void     PalLinkReceive  (void);
void     PalCheckLinks   (void);
uint32_t PalCheckData    (void);
void     PalPrepareFunc  (uint32_t pal_state);
void     PalPamSignals   (void);
void     PalLinkSend     (void);
void     PalBVsignals    (void);

/*---------------------------------------------------------------------------------------------------------*\
  End of file: pal.h
\*---------------------------------------------------------------------------------------------------------*/
