/*!
 * @file pal_filter_gain_calc.c
 *
 * Calculates the gain of PAL filter based on coefficients provided in
 * pal_filter_coefficients.txt file
 *
 * The gain is a sum of all filter coefficients divided by 2048, which is the
 * 12-bit shift performed by the PAL filter on the accumulated 40-bit value.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    float gain = 0.0;
    int i;
    size_t len = 0;
    char *line = NULL;

    FILE *fp = fopen("pal_filter_coefficients.txt", "r");

    float  sum_float  = 0.0;
    double sum_double = 0.0;

    for(i = 0; i < 8000; i++)
    {
        getline(&line, &len, fp);
        sum_float  += atof(line);
        sum_double += atof(line);
    }

    printf("PAL filter gain (float) : %f\n" , sum_float /2048.0);
    printf("PAL filter gain (double): %lf\n", sum_double/2048.0);

    fclose(fp);
}
