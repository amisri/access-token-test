#!/usr/bin/perl -w
#
# Calculates the gain of PAL filter based on coefficients provided in
# pal_filter_coefficients.txt file
#
# The gain is a sum of all filter coefficients divided by 2048, which is the
# 12-bit shift performed by the PAL filter on the accumulated 40-bit value.
#

use strict;
use warnings;

use File::Slurp;

my @coeffs = read_file($ARGV[0], chomp => 1);

my $coeffs_sum = 0.0;

foreach my $coeff(@coeffs)
{
    $coeffs_sum += $coeff;
}

$coeffs_sum /= 2048.0;

printf "PAL filter gain: $coeffs_sum\n";
