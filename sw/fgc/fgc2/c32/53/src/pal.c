/*---------------------------------------------------------------------------------------------------------*\
  File:         pal.c

  Purpose:      FGC2 Class 53 Software - contains functions concerning the POPS Acquisition Link (PAL) card
\*---------------------------------------------------------------------------------------------------------*/

#define PAL_GLOBALS

#include <stdbool.h>

#include <pal.h>
#include <cyc_class.h>          // For BMEAS_CAL_WHITERABBIT
#include <serprt30.h>           // for SERIAL_PORT_ADDR
#include <dsp_dependent.h>      // for ToIEEE()
#include <string.h>             // for memset()
#include <props.h>              // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <rtcom.h>              // for rtcom global variable
#include <main.h>               // for state_pc global variable
#include <cyc.h>                // for cyc global variable
#include <debug.h>              // for db global variable
#include <meas.h>               // for meas global variable
#include <reg.h>                // for reg global variable
#include <sim.h>                // for sim global variable
#include <dsp_time.h>           // for DSP_ROUND_TIME_MS()
#include <ppm.h>                // for ppm[]
#include <macros.h>             // for Test() macro


static void PalGetWhiteRabbitBmeasErrors(uint32_t plc_pbl_bmeas_pam_status)
{
    uint32_t error_index;

    // Get the Bmeas status

    property.pal.whiterabbit.errors = (plc_pbl_bmeas_pam_status >> 8) & 0xFF;

    if (property.pal.whiterabbit.errors != 0)
    {
        Set(rtcom.warnings, FGC_WRN_B_MEAS);

        // Accumulate the number of Bmeas errors

        for(error_index = 0; error_index < FGC_N_PAL_BMEAS_ERRORS; error_index++)
        {
            if(Test(property.pal.whiterabbit.errors, (1 << error_index)))
            {
                property.pal.whiterabbit.num_errors[error_index]++;
            }
        }
    }
    else
    {
        Clr(rtcom.warnings, FGC_WRN_B_MEAS);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void PalReset(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main() if the PAL communications need to be reset.  When this happens,
  the PAL acquisition channel calibrations and the PAL filter gain are recalculated before being sent to
  the PAL via the PBL.

  The filter gain factor is equal to the sum of all filter coefficients divided by 2048.0  This is
  the 12-bit shift performed by the PAL filter on the accumulated 40-bit value.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t chan_idx;
    uint32_t divpam_id;

    // Calculate PAL acquisition channel calibrations

    for(chan_idx = 0 ; chan_idx < FGC_N_PAL_CHANS ; chan_idx++)
    {
        divpam_id = property.pal.chan.divpam_id[chan_idx];

        // If unipolar

        if(property.pal.divpam.vref_neg[divpam_id] == 0.0)
        {
            pal.gain_pos[chan_idx] = property.pal.divpam.vref_pos[divpam_id] /
                                    (PAL_FILTER_GAIN * (property.pal.divpam.cal_high[divpam_id] -
                                                        property.pal.divpam.cal_low[divpam_id]));

            pal.gain_neg[chan_idx] = pal.gain_pos[chan_idx];

            pal.offset  [chan_idx] = PAL_FILTER_GAIN * pal.gain_pos[chan_idx] *
                                     property.pal.divpam.cal_low[divpam_id];
        }
        else // Else bipolar
        {
            pal.gain_pos[chan_idx] = (property.pal.divpam.vref_pos [divpam_id] -
                                      property.pal.divpam.vref_zero[divpam_id]) /
                                      (PAL_FILTER_GAIN * property.pal.divpam.cal_high[divpam_id]);

            pal.gain_neg[chan_idx] = (property.pal.divpam.vref_zero[divpam_id] -
                                      property.pal.divpam.vref_neg [divpam_id]) /
                                      (-PAL_FILTER_GAIN * property.pal.divpam.cal_low[divpam_id]);

            pal.offset  [chan_idx] = -property.pal.divpam.vref_zero[divpam_id];
        }

        // Convert to IEEE before sending to PAL

        dpcls->dsp.pal.cal.offset  [chan_idx] = ToIEEE(pal.offset  [chan_idx]);
        dpcls->dsp.pal.cal.gain_pos[chan_idx] = ToIEEE(pal.gain_pos[chan_idx]);
        dpcls->dsp.pal.cal.gain_neg[chan_idx] = ToIEEE(pal.gain_neg[chan_idx]);
    }

    memset( &(property.pal.version), 0, sizeof(property.pal.version) );

    property.pal.num_resets++;

    // Change reset request to "in progress"

    dpcls->dsp.pal.reset_req = 1;
    pal.prop_idx             = 0;

    // Reset VME IRQ flag

    pal.vme_irq_enabled  = false;

    // Start communications reset sequence

    dpcls->dsp.pal.state = FGC_PAL_GET_VERSION;
    db.idx               = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void PalLinkReceive(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function runs the link with the PAL card.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Return immediately if PAL unconfigured

    if(dpcls->dsp.pal.state == FGC_PAL_UNCONFIGURED)
    {
        return;
    }

    // Check and manage links with PBL/PAL

    if(dpcls->mcu.adc.filter_state == FGC_FILTER_STATE_READY)
    {
        PalCheckLinks();                                        // Check links
    }

    PalPrepareFunc(PalCheckData());                     // Pass new PAL state to PalPrepareFunc()

    if(rtcom.state_op != FGC_OP_SIMULATION &&
       (dpcls->dsp.pal.state == FGC_PAL_LINK_FAULT ||
        dpcls->dsp.pal.state == FGC_PAL_CONFIG_FAULT))
    {
        Set(rtcom.faults,FGC_FLT_VS_COMMS);
    }

    // If TESTING operational state then return immediately to allow pal.pops_control to be set manually

    if(rtcom.state_op == FGC_OP_TEST)
    {
        return;
    }

    // Manage communication flags sent to POPS

    // FGC_READY

    if(rtcom.state_op == FGC_OP_NORMAL &&
       state_pc != FGC_PC_FLT_OFF      &&
       state_pc != FGC_PC_FLT_STOPPING &&
       pal.vme_irq_enabled)
    {
        Set(pal.pops_control, FGC_POPS_CONTROL_READY);
    }
    else
    {
        Clr(pal.pops_control, FGC_POPS_CONTROL_READY);
    }

    // Standby state: POPSRUN

    if(state_pc > FGC_PC_ON_STANDBY)
    {
        Set(pal.pops_control,FGC_POPS_CONTROL_POPSRUN);
    }
    else
    {
        Clr(pal.pops_control,FGC_POPS_CONTROL_POPSRUN);
    }

    // NOBEAM flag

    if(state_pc != FGC_PC_CYCLING || cyc.ref_func_type == FGC_REF_NONE || cyc.ref_func_type == FGC_REF_ZERO)
    {
        Set(pal.pops_control,FGC_POPS_CONTROL_NOBEAM);
    }
    else
    {
        Clr(pal.pops_control,FGC_POPS_CONTROL_NOBEAM);
    }

    // SIMPOPS flag

    if(rtcom.state_op == FGC_OP_SIMULATION)
    {
        Set(pal.pops_control,FGC_POPS_CONTROL_SIMPOPS);
    }
    else
    {
        Clr(pal.pops_control,FGC_POPS_CONTROL_SIMPOPS);
    }

    // Choose operational measurement - always from WhiteRabbit network

    Set(pal.pops_control, FGC_POPS_CONTROL_WR_ENABLED);

    // Choose style of data coming from WhiteRabbit network - always new measurement system

    Set(pal.pops_control, FGC_POPS_CONTROL_WR_SELECT);

    // C0 flag

    if(cyc.time.period_counter < 2)
    {
        Set(pal.pops_control,FGC_POPS_CONTROL_C0);
    }
    else
    {
        Clr(pal.pops_control,FGC_POPS_CONTROL_C0);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void PalCheckLinks(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from PalLinkReceive() to check the status of the FGC-PBL-PAL-CTL-PAL-PBL-FGC
  links. This function is never called if state == UNCONFIGURED.
  Regarding the loopback counters (the one for PBL and the one for the VME), we check that, first, the
  loopback value is changing and, second, that it is equal to the value sent by the FGC. Indeed it had
  happened that the counter was stuck on the VME side and every 128 ms the FGC would interpret the value
  as being correct.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      idx;
    uint32_t      link_errors       = 0;
    uint32_t      plc_pbl_pam_stat  = dpcls->mcu.pal.lk1.plc_pbl_pam_stat;
    uint32_t      clc_pal_pops_stat = dpcls->mcu.pal.lk2.clc_pal_pops_stat;

    static uint32_t prev_state_pc;      // Previous PC state (to detect changes)
    static uint32_t pbl_lb_counter;     // Loopback counters
    static uint32_t ctl_lb_counter;     // Loopback counters

    // Detect when converter starts and reset error counters and time stamps

    if(state_pc >= FGC_PC_STARTING && prev_state_pc < FGC_PC_STARTING)
    {
        memset(property.pal.whiterabbit.num_errors, 0, sizeof(property.pal.whiterabbit.num_errors));
        memset(property.pal.links.num_errors,       0, sizeof(property.pal.links.num_errors));
        memset(property.pal.links.time_err_s,       0, sizeof(property.pal.links.time_err_s));
        memset(property.pal.links.time_err_ms,      0, sizeof(property.pal.links.time_err_ms));
        memset(property.pal.whiterabbit.num_errors, 0, sizeof(property.pal.whiterabbit.num_errors));
    }

    prev_state_pc = state_pc;

    // Analyse received link status data

    if( pbl_lb_counter == (plc_pbl_pam_stat >> 24) ||                 // If equal to the previous value (stuck)
       (pbl_lb_counter =  (plc_pbl_pam_stat >> 24))!= pal.link_check) // Or different from value sent by FGC
    {
        Set(link_errors, FGC_PAL_LINKS_PBL_LOOPBACK);               // 5:PBL_LOOPBACK
    }

    if(Test(plc_pbl_pam_stat, FGC2PBL_NODAT))
    {
        Set(link_errors, FGC_PAL_LINKS_FGC2PBL_NODAT);              // 7:FGC2PBL_NODAT
    }
    else if(Test(plc_pbl_pam_stat, PAL2PBL_NODAT))
    {
        Set(link_errors, FGC_PAL_LINKS_PAL2PBL_NODAT);              // 3:PAL2PBL_NODAT
    }
    else if(Test(plc_pbl_pam_stat, PAL2PBL_CRC))
    {
        Set(link_errors, FGC_PAL_LINKS_PAL2PBL_CRC);                // 4:PAL2PBL_CRC
    }
    else
    {
        if(Test(clc_pal_pops_stat, PBL2PAL_NOSYN))
        {
            Set(link_errors, FGC_PAL_LINKS_PBL2PAL_NOSYN);          // 0:PBL2PAL_NOSYN
        }

        if(Test(clc_pal_pops_stat, PAL_VME_IRQ) == false && pal.vme_irq_enabled == true)
        {
            Set(link_errors, FGC_PAL_LINKS_PAL_VME_IRQ);            // 8:PAL_VME_IRQ
        }

        if(Test(clc_pal_pops_stat, PBL2PAL_NODAT))
        {
            Set(link_errors, FGC_PAL_LINKS_PBL2PAL_NODAT);          // 1:PBL2PAL_NODAT
        }
        else if(Test(clc_pal_pops_stat, PBL2PAL_CRC))
        {
            Set(link_errors, FGC_PAL_LINKS_PBL2PAL_CRC);            // 2:PBL2PAL_CRC
        }
        else if(Test(clc_pal_pops_stat, PAL_VME_IRQ) && pal.vme_irq_enabled == true)
        {
            if( ctl_lb_counter == (clc_pal_pops_stat >> 24) ||                       // If equal to previous (stuck)
                (ctl_lb_counter =  (clc_pal_pops_stat >> 24)) != pal.prev_link_check) // Or different from FGC's value
            {
                Set(link_errors, FGC_PAL_LINKS_CTL_LOOPBACK);   // 6:CTL_LOOPBACK
            }
            else
            {
                if(!link_errors
                    && dpcls->dsp.pal.state == FGC_PAL_WAIT_POPS
                    && !(sim.active->enabled && ref.cycling_f))             // Remain in WP if simulating and already cycling
                {
                    dpcls->dsp.pal.state = FGC_PAL_READY;
                }
            }
        }

        // Remember PAL VME IRQ state

        pal.vme_irq_enabled = Test(clc_pal_pops_stat, PAL_VME_IRQ);
    }

    // If any link error detected

    if(link_errors)
    {
        if(!pal.err_downcounter)
        {
            pal.err_downcounter = 10000;                                // Start a 10s delay if not yet started
        }

        if(link_errors               == FGC_PAL_LINKS_CTL_LOOPBACK &&   // If the error is simply a CTL_LOOPBACK
           property.pal.links.errors == FGC_PAL_LINKS_CTL_LOOPBACK &&
           dpcls->dsp.pal.state      == FGC_PAL_READY)                  // And, if the state is READY
        {
            dpcls->dsp.pal.state = FGC_PAL_WAIT_POPS;                       // Switch to state WAIT_POPS

            if(rtcom.state_op != FGC_OP_SIMULATION && ref.cycling_f)            // If not simulating, and cycling
            {
                Set(rtcom.faults, FGC_FLT_VS_COMMS);                            // Switch to FLT_OFF
            }
        }
        else if((link_errors & ~FGC_PAL_LINKS_CTL_LOOPBACK) &&
                (property.pal.links.errors & ~FGC_PAL_LINKS_CTL_LOOPBACK))
        {
            // In other states ignore the CTL_LOOPBACK link error

            dpcls->dsp.pal.state = FGC_PAL_LINK_FAULT;                      // Switch to state LINK_FAULT
            pal.err_downcounter  = 10000;                                   // Reset the delay to 10s
        }

        // Copy the current link_errors to the property before link_errors is erased by the next loop

        property.pal.links.errors = link_errors;

        // Accumulate the number of link errors

        for(idx = 0 ; link_errors ; idx++, link_errors >>= 1)
        {
            if(Test(link_errors, 0x01))
            {
                property.pal.links.num_errors[idx]++;

                // Remember time of first error of this type

                if(property.pal.links.time_err_s[idx] == 0)
                {
                    property.pal.links.time_err_s[idx]  = rtcom.time_now_ms.unix_time;
                    property.pal.links.time_err_ms[idx] = rtcom.time_now_ms.ms_time;
                }
            }
        }
    }
    else
    {
        property.pal.links.errors = 0;                  // Reset the property links.errors
    }

    // Get WR Bmeas errors

    PalGetWhiteRabbitBmeasErrors(plc_pbl_pam_stat);

    // If any link error detected recently, set VS_COMMS warning for ten seconds

    if(pal.err_downcounter > 1)
    {
        pal.err_downcounter--;
        Set(rtcom.warnings, FGC_WRN_VS_COMMS);
    }
    else if(pal.err_downcounter == 1)
    {
        pal.err_downcounter--;                          // Downcounter is now zero
        Clr(rtcom.warnings, FGC_WRN_VS_COMMS);

        if(dpcls->dsp.pal.reset_req <= 1 &&             // If not currently in reset or reset was on-going
            (dpcls->dsp.pal.state == FGC_PAL_LINK_FAULT ||
             dpcls->dsp.pal.state == FGC_PAL_CONFIG_FAULT))
        {
            dpcls->dsp.pal.reset_req = 2;                       // Request an immediate communications reset
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t PalCheckData(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from PalLinkReceive() to analyse the property data returned from the previous
  millisecond.  The function returns the pal state.
\*---------------------------------------------------------------------------------------------------------*/
{
    // If in LINK FAULT state stay in FAULT state and don't process prop data

    if(dpcls->dsp.pal.state == FGC_PAL_LINK_FAULT)
    {
        return(FGC_PAL_LINK_FAULT);
    }

    pal.prop_data_rcv = dpcls->mcu.pal.lk2.pal_pbl_prop;

    switch(pal.prop_func)
    {

    // Do nothing

    case 0:     break;

    // Get POPS controller's calculation of the converter voltage

    case PAL_V_MEAS_POPS:

        pal.b_raw = dpcls->mcu.pal.lk1.bmeas_raw;

        if(!property.pal.links.errors)
        {
            pal.pops_status = dpcls->mcu.pal.lk2.clc_pal_pops_stat & 0xFFFF;
            pal.v_pops = FromIEEE(pal.prop_data_rcv);
            PalPamSignals();
        }
        break;

    // Get Version data

    default:

        property.pal.version[pal.prop_idx] = pal.prop_data_rcv;

        // If all version data read

        if(++pal.prop_idx >= FGC_N_PAL_VERS_WORDS)
        {
            // Switch to set calibration data

            pal.prop_idx  = 0;
            pal.prop_func = PAL_SET_FILTER_GAIN;

            return(FGC_PAL_SET_FILTER);
        }
        break;

    // Set Filter Gain

    case PAL_SET_FILTER_GAIN:

        // If read back data is wrong switch to CONFIG FAULT state

        if(pal.prop_data_rcv != pal.prop_data)
        {
            return(FGC_PAL_CONFIG_FAULT);
        }

        // Switch to set calibration data

        pal.prop_idx  = 0;
        pal.prop_func = PAL_SET_CAL;

        return(FGC_PAL_SET_CAL);

    // Set Calibration

    case PAL_SET_CAL:

        // If read back data is wrong

        if(pal.prop_data_rcv != pal.prop_data)
        {
            // Switch to CONFIG FAULT state

            return(FGC_PAL_CONFIG_FAULT);
        }

        // else if all cal data sent

        else if(++pal.prop_idx >= (3 * FGC_N_PAL_CHANS))
        {
            // Switch to get POPS calculated measure of output voltage

            pal.prop_idx  = 0;
            pal.prop_func = PAL_V_MEAS_POPS;

            // Terminate reset sequence

            dpcls->dsp.pal.reset_req = 0;

            return(FGC_PAL_WAIT_POPS);
        }
        break;
    }

    // No state change

    return(dpcls->dsp.pal.state);
}
/*---------------------------------------------------------------------------------------------------------*/
void PalPrepareFunc(uint32_t pal_state)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from PalLinkReceive() to prepare the property function and data for this millisecond.
  The function is given the new pal state.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t              chan_idx;
    static uint32_t       get_version_prop_funcs[FGC_N_PAL_VERS_WORDS] =
    {
        PAL_OPT_SWITCHES, PAL_FPGA_VERSION, PAL_MC_VERSION,      PAL_MC_PLL_VERSION,
        PBL_OPT_SWITCHES, PBL_FPGA_VERSION, PBL_MC_TASK_VERSION, PBL_MC_USB_VERSION
    };

    dpcls->dsp.pal.state = pal_state;                   // Save pal state in DPRAM


    switch(pal_state)
    {
    case FGC_PAL_GET_VERSION:

        pal.prop_func = get_version_prop_funcs[pal.prop_idx];
        break;

    case FGC_PAL_SET_FILTER:

        pal.prop_data = ToIEEE(PAL_FILTER_GAIN);

        break;

    case FGC_PAL_SET_CAL:

        switch(pal.prop_idx % 3)
        {
        case 0: pal.prop_data = dpcls->dsp.pal.cal.offset  [pal.prop_idx / 3]; break;
        case 1: pal.prop_data = dpcls->dsp.pal.cal.gain_pos[pal.prop_idx / 3]; break;
        case 2: pal.prop_data = dpcls->dsp.pal.cal.gain_neg[pal.prop_idx / 3]; break;
        }
        break;

    case FGC_PAL_LINK_FAULT:
    case FGC_PAL_CONFIG_FAULT:

        if(pal.prop_func)
        {
            // Copy pal.prop_func, pal.prop_idx, pal.prop_data and pal.prop_data_rcv in property PAL.DEBUG

            memcpy(property.pal.debug, &pal.prop_func, FGC_N_PAL_DEBUG_WORDS);

            pal.prop_func            = 0;
            dpcls->dsp.pal.reset_req = 0;
            pal.v_pops               = 0.0;
            meas.v                   = 0.0;
            pal.b_raw                = 0;
            pal.pops_status          = 0;
            pal.vme_irq_enabled      = false;
            pal.err_downcounter      = 10000;                              // Start a 10s delay

            for(chan_idx = 0 ; chan_idx < FGC_N_PAL_CHANS ; chan_idx++)
            {
                pal.v_pam[chan_idx] = 0.0;
                dpcls->dsp.pal.pam_volts[chan_idx] = 0;
            }
        }
        break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void PalPamSignals(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called every millisecond to process the PAM acquisition signals provided there are no
  communication link errors reported
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      chan_idx;
    uint32_t      pam_no_sig;
    int32_t      raw_pam[FGC_N_PAL_CHANS];

    // Process the PAM raw data and status using calibrations

    pam_no_sig = dpcls->mcu.pal.lk1.plc_pbl_pam_stat;

    pal.pam_nosig_flt_f = false;
    for(chan_idx = 0 ; chan_idx < FGC_N_PAL_CHANS ; chan_idx++, pam_no_sig >>= 1)
    {
        if(!(pam_no_sig & 1))                   // If PAM signal detected
        {
            raw_pam[chan_idx] = dpcls->mcu.pal.pam_raw[chan_idx];

            pal.v_pam[chan_idx] =
                  raw_pam[chan_idx]
               * (raw_pam[chan_idx] >= 0 ? pal.gain_pos[chan_idx] : pal.gain_neg[chan_idx])
               - pal.offset[chan_idx];

            dpcls->dsp.pal.pam_volts[chan_idx] = ToIEEE(pal.v_pam[chan_idx]);
        }
        else                                    // else no PAM signal
        {
            property.pal.chan.no_sig_count[chan_idx]++;
            pal.v_pam[chan_idx]                = 0.0;
            dpcls->dsp.pal.pam_volts[chan_idx] = 0;
            Set(rtcom.st_latched,FGC_LAT_PAM_NOSIG_FLT);     // Flag in STATUS.ST_LATCHED is set
            pal.pam_nosig_flt_f = true;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void PalLinkSend(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends values for the PAL interface via the link to the PBL card.  The PBL card also has a
  USB spy interface which can monitor some of the signals.  Ten words are sent per millisecond:

        0: Control Prop: [31 Prop Function 24][23 Reserved 16][15 Property offset 0]
        1: Control POPS: [31 LINK_CHECK 24][23 POPS Control 16][15 Next millisecond time 0]
        2: UTC_TIME or PROP_DATA
        3: p80_cyclic_data
        4: SPY.MPX[0] VREF  (fixed)
        5: SPY.MPX[1] IMEAS (fixed)
        6: SPY.MPX[2] BMEAS (default)
        7: SPY.MPX[3] VMEAS (default)
        8: SPY.MPX[4] REF   (default)
        9: SPY.MPX[5] ERR   (default)

  The first 6 signals are fixed as they are required by the POPS controls.  Signals 6-9 can be selected
  by the user.  Signals 4-9 will be emitted on the USB SPY link on the PBL interface board.

  The UTC/Next milliseconds timestamp applies to the next millisecond.

  Bit 23 of Control POPS is the C0_FLAG.  This will be set on the first two milliseconds of each cycle.

  The LINK_CHECK byte has bit 7 set (0x80) while bits 0-6 cycle from 0x00 to 0x7F.  This byte is checked in
  the data returned on the serial links from the MPC interface at the start of the next millisecond.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t            idx;
    DP_IEEE_FP32        v;
    uint32_t            next_ms_time;
    uint32_t            unix_time;
    uint32_t            pppl_seg_idx;
    struct ppm         *ppm_user = ppm + cyc.ref_user;

    // [0] Control Property word: [31 Prop Function 24][23 Reserved 16][15 Property offset 0]

    dpcls->dsp.pal.fgc2pbl[0] = v = (pal.prop_func << 24) | pal.prop_idx;
    SPY(v);

    // [1] Control POPS word: [31 LINK_CHECK 24][23 POPS Control 16][15 Next millisecond time 0]

    next_ms_time = rtcom.time_now_ms.ms_time + 1;
    unix_time = rtcom.time_now_ms.unix_time;

    if(next_ms_time > 999)
    {
        next_ms_time = 0;
        unix_time++;
    }

    pal.prev_link_check = pal.link_check;

    // Loop link check byte from 0x80-0xFF

    if(++pal.link_check > 0xFF)
    {
        pal.link_check = 0x80;
    }

    dpcls->dsp.pal.fgc2pbl[1] = v = (pal.link_check << 24) | (pal.pops_control << 16) | next_ms_time;

    WAITSPY;
    SPY(v);

    // [2] Property Data or Unix time

    if(pal.prop_func == PAL_SET_CAL || pal.prop_func == PAL_SET_FILTER_GAIN)
    {
        v = pal.prop_data;
    }
    else
    {
        v = unix_time;
    }

    dpcls->dsp.pal.fgc2pbl[2] = v;
    WAITSPY;                                                    // Wait for previous word to be sent
    SPY(v);                                                     // Send word to SPY

    // [3] Write p80_cyclic_data word.
    //
    // See https://wikis.cern.ch/display/TEEPCCCS/Class+53%3A+FGC2_POPS for the sequence
    //
    // Notes:   The PPPL function time is relative to the end of the first plateau.
    //          pppl.time[] contain the function time at the end of each PPPL segment
    //          pppl.a0[] contains the reference at the end of each PPPL segment

    if(cyc.time.period_counter < 2)
    {
        // C0 & C1 : ref_user as a float, repeated for redundancy

        v = ToIEEE((FP32)cyc.ref_user);
    }
    else if(cyc.time.period_counter < 3)
    {
        // C2 : Open loop voltage for cycle start for field regulation cycles. 0.0 for current regulation cycles.

        v = ppm_user->cycling.reg_mode == FGC_REG_B
          ? ToIEEE(ppm[cyc.ref_user].cycling.ref_start.vref)
          : ToIEEE(0.0);
    }
    else if(   cyc.time.period_counter < 6
            && (   ppm_user->cycling.func_type == FGC_REF_ZERO
                || ppm_user->cycling.func_type == FGC_REF_PPPL
                || (   ppm_user->cycling.func_type == FGC_REF_TABLE
                    && ppm_user->cycling.reg_mode == FGC_REG_B)))
    {
        switch(cyc.time.period_counter)
        {
            case 3: // C3: First plateau reference in Amps for PPPL, ZERO or field regulation TABLE, -1.0 otherwise.

                v = ToIEEE(ppm_user->cycling.reg_mode == FGC_REG_B
                           ? regLoadFieldToCurrent(&(load.active->pars),ppm_user->armed.first_plateau.ref)
                           : ppm_user->armed.first_plateau.ref);
                break;

            case 4: // C4: First plateau start time for PPPL, ZERO or field regulation TABLE, -1.0 otherwise.

                v = ToIEEE(ppm_user->armed.first_plateau.time);

                break;

            case 5: // C5: First plateau duration for PPPL, ZERO or field regulation TABLE, -1.0 otherwise.

                v = ToIEEE(ppm_user->armed.first_plateau.duration);

                break;
        }
    }
    else if(   cyc.time.period_counter > 5
            && cyc.time.period_counter < 30
            && ppm_user->cycling.func_type == FGC_REF_PPPL
            && (pppl_seg_idx = ((cyc.time.period_counter - 6) / 3) * 4) < ppm_user->armed.pars.pppl.num_segs)
    {
        switch((cyc.time.period_counter - 6) % 3)
        {
            case 0: // C6, C9, C12, ..., C27: Plateau reference in Amps for PPPL, -1.0 otherwise.

                // Plateau reference is taken from the end of last parabolic segment

                v = ToIEEE(ppm_user->cycling.reg_mode == FGC_REG_B
                           ? regLoadFieldToCurrent(&(load.active->pars), ppm_user->armed.pars.pppl.a0[pppl_seg_idx + 2])
                           : ppm_user->armed.pars.pppl.a0[pppl_seg_idx + 2]);
                break;

            case 1: // C7, C10, C13, ..., C28: Plateau start time for PPPL, -1.0 otherwise.

                // Plateau start time is the time of the end of the last parabolic segment

                v = ToIEEE(ppm_user->armed.pars.pppl.time[pppl_seg_idx + 2]);

                break;

            case 2: // C8, C11, C14, ..., C29: Plateau duration for PPPL, -1.0 otherwise.

                // Plateau duration is calulated by the difference in the third and fourth segment end times

                v = ToIEEE(ppm_user->armed.pars.pppl.time[pppl_seg_idx + 3] - ppm_user->armed.pars.pppl.time[pppl_seg_idx + 2]);

                break;
        }
    }
    else
    {
        // Set -1.0 as padding between sequences

        v = ToIEEE(-1.0);
    }

    dpcls->dsp.pal.fgc2pbl[3] = v;

    WAITSPY;                                                    // Wait for previous word to be sent
    SPY(v);                                                     // Send word to SPY

    // [4-9] Write six words to SPY interface, corresponding to the SPY.MPX property

    property.spy.mpx[0] = FGC_SPY_VREF;                         // Fix first two SPY signals as they are
    property.spy.mpx[1] = FGC_SPY_IMEAS;                        // required by the POPS controls

    for(idx = 0 ; idx < FGC_N_SPY_CHANS; idx++)                 // Write six data words
    {
        dpcls->dsp.pal.fgc2pbl[idx+4] =
        v = ToIEEE(MeasSpyMpx(property.spy.mpx[idx]));          // Get data word for this channel
        WAITSPY;                                                // Wait for previous word to be sent
        SPY(v);                                                 // Send word to SPY
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void PalBVsignals(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called every millisecond to acquire the field and voltage measurements for POPS.
  It also implements the Bmeas checks 1 and 2 at the start of each cycle.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!sim.active->enabled)
    {
        // Real field signal from PBL

        pal.b_raw = dpcls->mcu.pal.lk1.bmeas_raw;

        // Total converter voltage is the sum of both output filter voltages

        meas.v = pal.v_pam[0] + pal.v_pam[1];
    }
    else
    {
        //Field simulation

        pal.b_raw = (int32_t)( (1.0 / BMEAS_CAL_WHITERABBIT) * sim.active->b);

        // Voltage simulation

        meas.v = sim.active->v;
    }

    // Add cycle user (from timing) as a marker to B_MEAS on C0

    if(cyc.time.period_counter == 0)
    {
        meas.b = (FP32)cyc.timing_user;
    }
    else
    {
        meas.b = BMEAS_CAL_WHITERABBIT * (FP32)pal.b_raw;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pal.c
\*---------------------------------------------------------------------------------------------------------*/

