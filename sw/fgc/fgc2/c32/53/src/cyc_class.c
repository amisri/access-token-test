/*---------------------------------------------------------------------------------------------------------*\
  File:         cyc_class.c

  Purpose:      FGC2 Class 53 Software - contains cycling related functions specific to the class
\*---------------------------------------------------------------------------------------------------------*/

#include <cyc_class.h>

#include <cyc.h>
#include <defconst.h>
#include <dpcom.h>
#include <dpcls.h>
#include <ref.h>
#include <meas.h>
#include <reg.h>
#include <refto.h>              // for RefToEnd()
#include <math.h>               // for fabs()

/*---------------------------------------------------------------------------------------------------------*/
void CycBIVchecks(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will perform field-current-voltage checks at time REF.START.TIME + REF.START.DURATION.
  It will return on the first failure having recorded the results in REF.CYC.WARNING or REF.CYC.FAULT.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32        checked_value;

    // Return immediately if check is not armed or time is before check time

    if(!cyc.biv_check_f || cyc.time.s < cyc.biv_check_time)
    {
        return;
    }

    cyc.biv_check_f = false;

    // Cycle Check - IRATE_VREF : Current rate to voltage reference

    checked_value = fabs(meas.i_hist.rate - ref.v.value * load.active->pars.inv_henrys);

    if(checked_value > MAX_IRATE_ERR)
    {
        CycSaveCheckFault(FGC_CYC_FLT_IRATE_VREF, FGC_WRN_I_MEAS,
                            MAX_IRATE_ERR,                                           // REF.CYC.FAILED.DATA[0]
                            checked_value,                                           // REF.CYC.FAILED.DATA[1]
                            meas.i_hist.rate,                                        // REF.CYC.FAILED.DATA[2]
                            ref.v.value);                                            // REF.CYC.FAILED.DATA[3]
        return;
    }

    // Field checks are only needed with field regulation

    if(reg.active->state == FGC_REG_B)
    {
    // Cycle Check - BMEAS_IMEAS : Measured field to measured current

        // Use previous B meas to compensate for 1ms delay in current measurement

        checked_value = fabs(meas.b_hist.buf[(meas.b_hist.idx - 1) & MEAS_HIST_MASK] -
                             load.active->pars.gauss_per_amp * meas.i);

        if(checked_value > MAX_BMEAS_ERR)
        {
            CycSaveCheckWarning(true, FGC_CYC_WRN_BMEAS_IMEAS, FGC_WRN_B_MEAS,
                                MAX_BMEAS_ERR,                                       // REF.CYC.FAILED.DATA[0]
                                checked_value,                                       // REF.CYC.FAILED.DATA[1]
                                meas.b_hist.buf[(meas.b_hist.idx-1)&MEAS_HIST_MASK], // REF.CYC.FAILED.DATA[2]
                                meas.i);                                             // REF.CYC.FAILED.DATA[3]
            RefToEnd();
            return;
        }

    // Cycle Check - BRATE_IRATE : Measured Field rate to current rate

        checked_value = fabs(meas.b_hist.rate - load.active->pars.gauss_per_amp * meas.i_hist.rate);

        if(checked_value > MAX_BRATE_ERR)
        {
            CycSaveCheckWarning(true, FGC_CYC_WRN_BRATE_IRATE, FGC_WRN_B_MEAS,
                                MAX_BRATE_ERR,                                       // REF.CYC.FAILED.DATA[0]
                                checked_value,                                       // REF.CYC.FAILED.DATA[1]
                                meas.b_hist.rate,                                    // REF.CYC.FAILED.DATA[2]
                                meas.i_hist.rate);                                   // REF.CYC.FAILED.DATA[3]
            RefToEnd();
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cyc_class.c
\*---------------------------------------------------------------------------------------------------------*/
