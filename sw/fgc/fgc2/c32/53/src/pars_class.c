/*---------------------------------------------------------------------------------------------------------*\
  File:         pars_class.c

  Purpose:      Class 53 DSP Software - Parameter group set functions.

  Author:       Quentin.King@cern.ch

  Notes:        These functions are called at the end of the millisecond ISR provided it is not a
                10ms millisecond boundary when lots of parameters are sent to the MCU.
\*---------------------------------------------------------------------------------------------------------*/

#include <dpcls.h>              // for dpcls global variable

/*---------------------------------------------------------------------------------------------------------*/
void ParsResetPal(void)
/*---------------------------------------------------------------------------------------------------------*\
  Class 53 POPS only: Any property concerned with the PAL configuration has been changed so set the
  PAL reset downcounter to trigger a PAL reset after the specified time delay.
\*---------------------------------------------------------------------------------------------------------*/
{
    dpcls->dsp.pal.reset_req = PAL_RESET_DELAY_MS;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars_class.c
\*---------------------------------------------------------------------------------------------------------*/

