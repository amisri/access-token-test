/*----------------------------------------------------------------------------*\
 File:          59/out2mot.cmd

 Purpose:       FGC2_LHC_PC DSP Software HEX30 drive file

 Notes:         This drive file will generate S-Records from the COFF file
\*----------------------------------------------------------------------------*/

ROMS
{
    EPROM:      org = 0000H, length = 10000H    /* 64KB starting at 0 */
}

/* End of file: 59/out2mot.cmd */

