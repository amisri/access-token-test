/*------------------------------------------------------------------------------*\
 File:          59\link.cmd

 Purpose:       DSP Program Linker file for FGC2 Hardware

 Notes:         The program is built with the register parameter passing model
                Class 59 uses the internal SRAM for the stack.  This
                increases the speed of execution of the program by 10% but is
                less radiation tolerant.

                Pages 2,3,4 are used for function time arrays
                Pages 5,6,7 are used for function value arrays
\*------------------------------------------------------------------------------*/

/* Memory Map Definition */

/*
 * Note: The 2KB stack for classes 53 and 59 is in internal SRAM at 0x87FE00 as there is not a
 *       radiation problem for these systems and internal SRAM is faster than external SRAM.
 *       The 2KB stack for classes 51 is in external SRAM at 0xFE00.
 *       See comments in file dsp/fgc2/src/lib.c for more information about the C32 memory map.
 */

MEMORY
{
    VECS:       org = 000000H   len = 00040H    /* 256 B:  Reset and Interrupt Vectors  */
    CODE:       org = 000040H   len = 03FC0H    /* ~64KB: Program Code and init data. Edit out2mot.cmd file accordingly */
    VARS:       org = 004000H   len = 04000H    /*  64KB: Program variables             */
    STACK:      org = 87FE00H   len = 00200H    /*   2KB: Stack in internal C32 RAM     */
}

/* Section Allocation */

SECTIONS
{
     vecs:      > VECS                                  /* Vector table (vecs.asm)      */
    .cinit:     > CODE                                  /* C Initialisation data        */
    .data:      > CODE                                  /* Asm data                     */
    .const:     > CODE                                  /* FP constants and switch tbls */
    .text:      > CODE                                  /* Program code                 */
    .stack:     > STACK                                 /* Program stack                */
    .bss:       > VARS                                  /* Global and Static variables  */
}

/* End of file: 59\link.cmd */

