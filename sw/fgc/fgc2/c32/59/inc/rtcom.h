/*---------------------------------------------------------------------------------------------------------*\
  File:         rtcom.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef RTCOM_H // header encapsulation
#define RTCOM_H

#ifdef RTCOM_GLOBALS
    #define RTCOM_VARS_EXT
#else
    #define RTCOM_VARS_EXT extern
#endif

#include <cc_types.h>
#include <dpcom.h>                              // For struct abs_time_us, struct abs_time_ms

//-----------------------------------------------------------------------------------------------------------

// Common Millisecond interrupt RT processing variables

struct  rtcom
{
    uint32_t            class_id;               // 0    // Class ID
    uint32_t            par_group_set_mask;             // Parameter group set mask
    uint32_t            ppm_start;                      // Start of PPM property structure
    uint32_t            ppm_end;                        // End of PPM property structure
    uint32_t            ppm_length;                     // Length of PPM property structure

    // Time and watchdog

    struct abs_time_ms  time_now_ms;            // 5    // Time now in ms
    struct abs_time_us  time_now_us;            // 7    // Time now in us

    uint32_t            led_counter;            // 9    // Millisecond counter for FP LED
    uint32_t            ms10;                           //   time_now_ms.ms_time%10
    uint32_t            ms20;                           //   time_now_ms.ms_time%20
    uint32_t            ms200;                          //   time_now_ms.ms_time%200
    uint32_t            ms10_f;                         // !(time_now_ms.ms_time%10)
    uint32_t            ms20_f;                         // !(time_now_ms.ms_time%20)
    uint32_t            ms200_f;                        // !(time_now_ms.ms_time%200)
    uint32_t            cpu_usage;                      // DSP RT cpu usage

    // States and modes, faults and warnings

    uint32_t            state_op;               // 19   // OP state machine
    uint32_t            faults;                         // Fault flags
    uint32_t            warnings;                       // Warning flags
    uint32_t            st_unlatched;                   // Unlatched status flags
    uint32_t            st_latched;                     // Latched status flags
    uint32_t            interfacetype;                  // Slot 5 interface type
};

RTCOM_VARS_EXT struct rtcom    rtcom;

//-----------------------------------------------------------------------------------------------------------

#endif  // RTCOM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rtcom.h
\*---------------------------------------------------------------------------------------------------------*/
