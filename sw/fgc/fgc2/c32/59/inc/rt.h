/*---------------------------------------------------------------------------------------------------------*\
  File:         rt.h

  Purpose:      FGC DSP Cycling state variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef RT_H   // Header encapsulation
#define RT_H

#ifdef RT_GLOBALS
    #define RT_VARS_EXT
#else
    #define RT_VARS_EXT extern
#endif
/*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>
#include <defconst.h>           // for FGC_N_FGEN_CHANS
#include <mcu_dsp_common.h>     // for struct abs_time_us

/*---------------------------------------------------------------------------------------------------------*/

struct  rt
{
    uint32_t    ref_type     [FGC_N_FGEN_CHANS];        // [  0] Reference type (REF_XXX)
    uint32_t    ref_time_tb  [FGC_N_FGEN_CHANS];        // [ 16] Time elapsed running ref
    uint32_t    ref_running_f[FGC_N_FGEN_CHANS];        // [ 32] Ref gen is running
    uint32_t    chk_stat_f[2][FGC_N_FGEN_CHANS];        // [ 48] Check status flags

    FP32       fspy_0       [FGC_N_FGEN_CHANS];        // [ 80] fspy_0: Floating point system spy values
    FP32       last_fval_10 [FGC_N_FGEN_CHANS];        // [ 96] Last fval_10 used for aborting a function
    FP32       fval_10      [FGC_N_FGEN_CHANS];        // [112] fval_10 - Function value
    FP32       sval_11      [FGC_N_FGEN_CHANS];        // [128] sval_11 - Sum of function and RT values
    FP32       xval_12      [FGC_N_FGEN_CHANS];        // [144] xval_12 - sval_11 after max clipping
    FP32       nval_13      [FGC_N_FGEN_CHANS];        // [160] nval_13 - xval_12 after min clipping
    FP32       oval_14      [FGC_N_FGEN_CHANS];        // [176] oval_14 - nval_13 after positive rate limiting
    FP32       cval_15      [FGC_N_FGEN_CHANS];        // [192] cval_15 - oval_14 after negative rate limiting
    FP32       ival_16      [FGC_N_FGEN_CHANS];        // [208] ival_16 - cval_15 after scaling into raw int units
    uint32_t    wval_raw     [FGC_N_FGEN_CHANS];        // [224] wval_raw: IEEE floats from MCU
    FP32       wval_17      [FGC_N_FGEN_CHANS];        // [240] wval_17 - TI floats
    FP32       lval_18      [FGC_N_FGEN_CHANS];        // [256] lval_18 - wval_17 selected by FGRT.CONTROL
    FP32       tval_19      [FGC_N_FGEN_CHANS];        // [272] tval_19 - lval_18 after interpolation
    FP32       pval_20      [FGC_N_FGEN_CHANS];        // [288] pval_20 - tval_19 after positive clip
    FP32       rval_21      [FGC_N_FGEN_CHANS];        // [304] rval_21 - pval_20 after negative clip

    uint32_t    trigger;                                // [320] Primary trigger register
    uint32_t    stretch_counter;                        // [321] Status stretch up counter (minimum 10s)
};

struct rt_spyfgmpx                                      // Spy multiplexers
{
    uint32_t    sig_idx         [FGC_N_SPY_CHANS];      // [  0] Spy FG MPX signal index (first two digits)
    uint32_t    chan_idx        [FGC_N_SPY_CHANS];      // [ 16] Spy FG MPX chanel index (last two digits)
};

struct rt_fgfunc                                        // Function generator run/abort times
{
    struct abs_time_us  run   [FGC_N_FGEN_CHANS];
    struct abs_time_us  abort [FGC_N_FGEN_CHANS];
};

struct rt_abort                                         // Function generator abort variablese
{
    uint32_t    time            [FGC_N_FGEN_CHANS];      // [  0] ref_time_tb at start of abort
    uint32_t    period          [FGC_N_FGEN_CHANS];      // [ 16] Abort period (100us)
    FP32       half_period_ms  [FGC_N_FGEN_CHANS];      // [ 32] 0.05 * Abort period (100us)
    FP32       inv_period_ms   [FGC_N_FGEN_CHANS];      // [ 48] 10 / period_100us
    FP32       d               [FGC_N_FGEN_CHANS];      // [ 64] Abort deceleration
    FP32       ref             [FGC_N_FGEN_CHANS];      // [ 80] Final ref value at the end of the abort
};

struct rt_state                                         // Channel state machine
{
    uint32_t    idle;                                   // Idle state mask
    uint32_t    armed;                                  // Armed state mask
    uint32_t    running;                                // Running state mask
    uint32_t    aborting;                               // Aborting state mask
    uint32_t    not_ack;                                // Not ack state mask
};

struct rt_status                                        // Channel status
{
    uint32_t    rt_ctrl;                                // RT control mask
    uint32_t    start_evt;                              // Start event mask
    uint32_t    abort_evt;                              // Abort event mask
    uint32_t    lim_rt_pos;                             // RT positive limit mask
    uint32_t    lim_rt_neg;                             // RT negative limit mask
    uint32_t    lim_chan_max;                           // Chan max limit mask
    uint32_t    lim_chan_min;                           // Chan min limit mask
    uint32_t    lim_rate_pos;                           // Idle state mask
    uint32_t    lim_rate_neg;                           // Idle state mask
    uint32_t    timeout_f;                              // Link time out mask
    uint32_t    ack;                                    // Acknowledge received mask
    uint32_t    nack;                                   // Not acknowledge received mask
    uint32_t    not_ack;                                // Channel Not Acknowledged mask (millisecond)
    uint32_t    prev_not_ack;                           // Channel Not Acknowledged mask for prev millisecond
    uint32_t    not_ack_20ms;                           // Channel Not Acknowledged mask (20ms)
    uint32_t    lim_any;                                // Any limit active mask
    uint32_t    crc_err_f;                              // CRC error flag
    uint32_t    not_ack_10s;                            // Channel Not Acknowledged mask (stretched to 10s)
    uint32_t    lim_any_10s;                            // Any limit active mask (stretched to 10s)
    uint32_t    crc_err_10s_f;                          // CRC error flag (stretched to 10s)
};

struct rt_control                                       // Real-time control
{
    uint32_t    count           [FGC_N_FGEN_CHANS];     // [  0] Interpolation down counter
    FP32       step            [FGC_N_FGEN_CHANS];     // [ 16] Millisecond step
    uint32_t    period_ms       [FGC_N_FGEN_CHANS];     // [ 32] Used to initialise count[]
    FP32       inv_period      [FGC_N_FGEN_CHANS];     // [ 48] 1/period_ms (used as factor in RtControl())
};

struct rt_limit                                         // Rate of change limits
{
    FP32       rate_pos        [FGC_N_FGEN_CHANS];     // [  0] Change limit per millisecond
    FP32       rate_neg        [FGC_N_FGEN_CHANS];     // [ 16] Change limit per millisecond
};

/*---------------------------------------------------------------------------------------------------------*/

void            RtEvents                (void);
void            RtData                  (void);
void            RtStates                (uint32_t chan_idx, uint32_t chan_mask);
void            RtControl               (uint32_t chan_idx, uint32_t chan_mask);
void            RtChannel               (uint32_t chan_idx, uint32_t chan_mask);
void            RtStatusAndStats        (uint32_t chan_idx, uint32_t chan_mask);
void            RtReport20ms            (void);
void            RtSpySend               (void);
DP_IEEE_FP32    RtSpyMpx                (uint32_t idx);

/*---------------------------------------------------------------------------------------------------------*/

RT_VARS_EXT struct rt_state         rt_state;
RT_VARS_EXT struct rt_status        rt_status;
RT_VARS_EXT struct rt_control       rt_control;
RT_VARS_EXT struct rt_limit         rt_limit;
RT_VARS_EXT struct rt_spyfgmpx      rt_spyfgmpx;
RT_VARS_EXT struct rt_fgfunc        rt_fgfunc;
RT_VARS_EXT struct rt_abort         rt_abort;
RT_VARS_EXT struct rt               rt;

#endif  // End of header encapsulation RT_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rt.h
\*---------------------------------------------------------------------------------------------------------*/
