/*---------------------------------------------------------------------------------------------------------*\
  File:         main.h

  Purpose:      FGC DSP global function and variable declarations
\*---------------------------------------------------------------------------------------------------------*/


#ifndef MAIN_H   // Header encapsulation
#define MAIN_H

#ifdef MAIN_GLOBALS
    #define MAIN_VARS_EXT
#else
    #define MAIN_VARS_EXT extern
#endif

/*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>
#include <defconst.h>           // for FGC_N_FGEN_CHANS
#include <lib.h>

/*---------------------------------------------------------------------------------------------------------*/

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// FOR BACKWARD COMPATIBILITY ONLY (some time periods are still in RELTIME in class 59)
#define FGC_NO_CYC_TIMEBASE_HZ 10000


#define FG_CONV_S_TO_TB(time_s)         (uint32_t)((time_s) * FGC_NO_CYC_TIMEBASE_HZ + 0.49999)
#define FG_CONV_TB_TO_S(time_tb)        ((FP32)(time_tb) / FGC_NO_CYC_TIMEBASE_HZ)
// END FOR BACKWARD COMPATIBILITY ONLY
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/*!
 * Base addres of the table function data for all users
 */
#define TABLE_FUNCTION_BASE_ADDR        0x08000

/*!
 * Returns pointer to table function vector for given user
 *
 * Table functions for all users are placed continuously in memory.
 * On C32, all basic C types are of size 1, so to find the right function
 * it's enough to shift the base pointer by the table length multiplied by
 * the channel index and by two (two floats per point).
 */
#define TABLE_FUNCTION(ch)              (void*)(TABLE_FUNCTION_BASE_ADDR + FGC_FG_TABLE_LEN * ch * 2)

#define IT_PERIOD_TB                    (FGC_NO_CYC_TIMEBASE_HZ / 1000)

/*---------------------------------------------------------------------------------------------------------*/

// Background processing variables

struct  bg
{
    uint32_t            ref_type [FGC_N_FGEN_CHANS];    // Function type that has been armed
};

/*---------------------------------------------------------------------------------------------------------*/

void    main    (void);

/*---------------------------------------------------------------------------------------------------------*/

MAIN_VARS_EXT struct bg       bg;

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation MAIN_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
