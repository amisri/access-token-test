/*---------------------------------------------------------------------------------------------------------*\
  File:         props_class.h

  Purpose:      FGC2 Class 59 DSP global property variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PROPS_CLASS_H // header encapsulation
#define PROPS_CLASS_H

#ifdef PROPS_CLASS_GLOBALS
    #define PROPS_CLASS_VARS_EXT
#else
    #define PROPS_CLASS_VARS_EXT extern
#endif

#include <cc_types.h>
#include <defconst.h>        // for FGC_N_FGEN_CHANS
#include <dpcom.h>      // for struct abs_time_us

/*---------------------------------------------------------------------------------------------------------*/

struct property
{
    struct
    {
        uint32_t          trigger         [FGC_N_FGEN_CHANS];     // FGCONF.TRIGGER[]
        FP32            gain            [FGC_N_FGEN_CHANS];     // FGCONF.GAIN[]
        FP32            offset          [FGC_N_FGEN_CHANS];     // FGCONF.OFFSET[]

        struct
        {
            struct
            {
                FP32    pos             [FGC_N_FGEN_CHANS];     // FGCONF.LIMIT.RT.POS[]
                FP32    neg             [FGC_N_FGEN_CHANS];     // FGCONF.LIMIT.RT.NEG[]
            } rt;

            struct
            {
                FP32    max             [FGC_N_FGEN_CHANS];     // FGCONF.LIMIT.USER.MAX[]
                FP32    min             [FGC_N_FGEN_CHANS];     // FGCONF.LIMIT.USER.MIN[]
            } user;

            struct
            {
                FP32    max             [FGC_N_FGEN_CHANS];     // FGCONF.LIMIT.CHAN.MAX[]
                FP32    min             [FGC_N_FGEN_CHANS];     // FGCONF.LIMIT.CHAN.MIN[]
            } chan;

            struct
            {
                FP32    pos             [FGC_N_FGEN_CHANS];     // FGCONF.LIMIT.RATE.POS[]
                FP32    neg             [FGC_N_FGEN_CHANS];     // FGCONF.LIMIT.RATE.NEG[]
            } rate;

        } limit;

    } fgconf;

    struct
    {
        struct abs_time_us  run         [FGC_N_FGEN_CHANS];     // FGFUNC.RUN[]
        struct abs_time_us  abort       [FGC_N_FGEN_CHANS];     // FGFUNC.ABORT[]
        uint32_t      run_delay   [FGC_N_FGEN_CHANS];     // FGFUNC.RUN_DELAY[]
        uint32_t      err_idx     [FGC_N_FGEN_CHANS];     // FGFUNC.ERR_IDX[]

        struct
        {
            FP32        final           [FGC_N_FGEN_CHANS];     // FGFUNC.PLP.FINAL[]
            FP32        acceleration    [FGC_N_FGEN_CHANS];     // FGFUNC.PLP.ACCELERATION[]
            FP32        linear_rate     [FGC_N_FGEN_CHANS];     // FGFUNC.PLP.LINEAR_RATE[]
        } plp;

        struct
        {
            FP32        final           [FGC_N_FGEN_CHANS];     // FGFUNC.CTRIM.FINAL[]
            uint32_t      period          [FGC_N_FGEN_CHANS];     // FGFUNC.CTRIM.PERIOD[]
        } ctrim;

        struct
        {
            FP32        amplitude       [FGC_N_FGEN_CHANS];     // FGFUNC.SINE.AMPLITUDE
            FP32        num_cycles      [FGC_N_FGEN_CHANS];     // FGFUNC.SINE.NUM_CYCLES
            uint32_t      period          [FGC_N_FGEN_CHANS];     // FGFUNC.SINE.PERIOD
        } sine;

        struct
        {
            uint32_t      function_n_els  [FGC_N_FGEN_CHANS];     // FGFUNC.TABLE.CHANn.FUNCTION number of elements
        } table;

    } fgfunc;

    struct
    {
        uint32_t  control         [FGC_N_FGEN_CHANS];     // FGRT.CONTROL[]
        uint32_t  period          [FGC_N_FGEN_CHANS];     // FGRT.PERIOD[]
    } fgrt;

    struct
    {
        uint32_t  triggers        [FGC_N_FGEN_CHANS];     // FGSTATS.TRIGGERS[]
        uint32_t  data_sents      [FGC_N_FGEN_CHANS];     // FGSTATS.DATA_SENTS[]
        uint32_t  acks            [FGC_N_FGEN_CHANS];     // FGSTATS.ACKS[]
        uint32_t  nacks           [FGC_N_FGEN_CHANS];     // FGSTATS.NACKS[]
        uint32_t  bad_crcs        [FGC_N_FGEN_CHANS];     // FGSTATS.BAD_CRCS[]
        uint32_t  timeouts        [FGC_N_FGEN_CHANS];     // FGSTATS.TIMEOUTS[]
    } fgstats;

    struct
    {
        uint32_t  fgmpx           [FGC_N_SPY_CHANS];      // SPY.FGMPX
    } spy;
};

//-----------------------------------------------------------------------------------------------------------

PROPS_CLASS_VARS_EXT    struct property property;

//-----------------------------------------------------------------------------------------------------------

#endif  // PROPS_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: props_class.h
\*---------------------------------------------------------------------------------------------------------*/
