/*---------------------------------------------------------------------------------------------------------*\
  File: ref.h

  Purpose: FGC DSP reference generation variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef REF_H   // Header encapsulation
#define REF_H

#ifdef REF_GLOBALS
    #define REF_VARS_EXT
#else
    #define REF_VARS_EXT extern
#endif
/*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>
#include <defconst.h>           // for FGC_N_FGEN_CHANS
#include <dpcom.h>

/*---------------------------------------------------------------------------------------------------------*/

/*!
 * Point structure for compatibility with other classes using libfg
 */
struct FG_point
{
    float    time;
    float    ref;
};

struct plp                                              // Parabolic - Linear - Parablic function
{
    int32_t     t     [4][FGC_N_FGEN_CHANS];            // [  0] End of segment times (100us)
    FP32       r     [4][FGC_N_FGEN_CHANS];            // [ 64] End of segment references
    FP32       a        [FGC_N_FGEN_CHANS];            // [128] Parabolic acceleration
    FP32       l        [FGC_N_FGEN_CHANS];            // [144] Linear rate of change
};

struct ctrim                                            // Cubic trim function
{
    uint32_t    tstart   [FGC_N_FGEN_CHANS];            // [  0] Start time (100us)
    uint32_t    tend     [FGC_N_FGEN_CHANS];            // [ 16] End time (100us)
    FP32       rend     [FGC_N_FGEN_CHANS];            // [ 32] Final reference
    FP32       t0       [FGC_N_FGEN_CHANS];            // [ 48] Timebase offset (s)
    FP32       r0       [FGC_N_FGEN_CHANS];            // [ 64] Reference offset
    FP32       a        [FGC_N_FGEN_CHANS];            // [ 80] Cubic factor r = t(at^2 + c)
    FP32       c        [FGC_N_FGEN_CHANS];            // [ 96] Cubic factor
};


struct sine                                             // Sine function
{
    uint32_t    period   [FGC_N_FGEN_CHANS];            // [  0] Period (100us)
    uint32_t    run_delay[FGC_N_FGEN_CHANS];            // [ 16] Run delay (100us)
    uint32_t    end_time [FGC_N_FGEN_CHANS];            // [ 32] End time (100s)
    FP32       freq_rad [FGC_N_FGEN_CHANS];            // [ 48] 2.PI / Period_100s
    FP32       ref0     [FGC_N_FGEN_CHANS];            // [ 64] Mid ref value
    FP32       ref_amp  [FGC_N_FGEN_CHANS];            // [ 80] Ref amplitude
};


struct tbl                                            // Table function
{
    FP32              run_delay[FGC_N_FGEN_CHANS];    // [ 0] Run delay in seconds
    uint32_t            nels     [FGC_N_FGEN_CHANS];    // [16] Number of elements in the table
    uint32_t            idx      [FGC_N_FGEN_CHANS];    // [32] Index of current segment
    FP32              end_time [FGC_N_FGEN_CHANS];    // [48] End time in seconds of current segment
    struct FG_point * function [FGC_N_FGEN_CHANS];    // [64] Pointers to time-reference pairs
    FP32              end_value[FGC_N_FGEN_CHANS];    // [80] Start value of current segment
    FP32              grad     [FGC_N_FGEN_CHANS];    // [96] Gradient of current segment
};

/*---------------------------------------------------------------------------------------------------------*/

extern FP32    (*ref_func[])           (uint32_t);
FP32           RefNone                 (uint32_t chan_idx);
FP32           RefSine                 (uint32_t chan_idx);
FP32           RefPlp                  (uint32_t chan_idx);
FP32           RefCTrim                (uint32_t chan_idx);
FP32           RefTable                (uint32_t chan_idx);
void           RefPrepareAbort         (uint32_t chan_idx);
FP32           RefAbort                (uint32_t chan_idx);

/*---------------------------------------------------------------------------------------------------------*/

REF_VARS_EXT FP32    (*ref_func[])(uint32_t)      // Ref gen functions
#ifdef REF_GLOBALS
= {
    RefNone,                                    // 0  FGC_REF_NONE
    RefNone,                                    // 1  FGC_REF_STARTING
    RefNone,                                    // 2  FGC_REF_STOPPING
    RefNone,                                    // 3  FGC_REF_TO_STANDBY
    RefNone,                                    // 4  FGC_REF_ARMED
    RefAbort,                                   // 5  FGC_REF_ABORTING
    RefNone,                                    // 6  FGC_REF_NOW
    RefNone,                                    // 7  FGC_REF_STEPS
    RefNone,                                    // 8  FGC_REF_SQUARE
    RefSine,                                    // 9  FGC_REF_SINE
    RefNone,                                    // 10 FGC_REF_COSINE
    RefPlp,                                     // 11 FGC_REF_PLP
    RefNone,                                    // 12 FGC_REF_PLEP
    RefNone,                                    // 13 FGC_REF_PELP
    RefNone,                                    // 14 FGC_REF_LTRIM
    RefCTrim,                                   // 15 FGC_REF_CTRIM
    RefTable,                                   // 16 FGC_REF_TABLE
}
#endif
;

REF_VARS_EXT struct ctrim    ctrim;
REF_VARS_EXT struct sine     sine;
REF_VARS_EXT struct tbl      tbl;
REF_VARS_EXT struct plp      plp;

/*---------------------------------------------------------------------------------------------------------*/

#endif  // End of header encapsulation REF_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ref.h
\*---------------------------------------------------------------------------------------------------------*/
