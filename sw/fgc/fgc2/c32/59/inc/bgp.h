/*---------------------------------------------------------------------------------------------------------*\
  File:         bgp.h

  Purpose:      FGC DSP Cycling state variable declarations
\*---------------------------------------------------------------------------------------------------------*/

#ifndef BGP_H   // Header encapsulation
#define BGP_H

#ifdef BGP_GLOBALS
    #define BGP_VARS_EXT
#else
    #define BGP_VARS_EXT extern
#endif
/*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <cc_types.h>

/*---------------------------------------------------------------------------------------------------------*/

BGP_VARS_EXT BOOLEAN bgp_no_pending_action;

/*---------------------------------------------------------------------------------------------------------*/

void          BgpLoop                 (void);
void          BgpRefChange            (uint32_t chan_idx);
uint32_t        BgpInitPlp              (uint32_t chan_idx);
uint32_t        BgpInitCTrim            (uint32_t chan_idx);
uint32_t        BgpInitTable            (uint32_t chan_idx);
uint32_t        BgpInitSine             (uint32_t chan_idx);
uint32_t        BgpCheckChannelLimits   (uint32_t chan_idx, FP32 r);
uint32_t        BgpCheckRateLimits      (uint32_t chan_idx, FP32 drdt);

/*---------------------------------------------------------------------------------------------------------*/

#endif  // End of header encapsulation BGP_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: bgp.h
\*---------------------------------------------------------------------------------------------------------*/
