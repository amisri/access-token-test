/*---------------------------------------------------------------------------------------------------------*\
  File:     59\src\rt.c

  Purpose:  FGC DSP Software - contains RT processing functions (called from IsrMst())

  Notes:    All real-time processing runs at interrupt level under IsrMst().
\*---------------------------------------------------------------------------------------------------------*/

#define RT_GLOBALS

#include <rt.h>                 // for rt_fgfunc global variable
#if defined(__TMS320C32__)
    #include <serprt30.h>       // for SERIAL_PORT_ADDR
#endif
#include <dsp_dependent.h>      // for ToIEEE()
#include <main.h>               // for bg global variable
#include <dpcom.h>              // For struct abs_time_us, struct abs_time_ms
#include <rtcom.h>              // for rtcom global variable
#include <dpcls.h>              // for dpcls global variable
#include <props_class.h>        // for property global variable
#include <diag.h>               // for qspi_misc, dim_list global variable
#include <defprops_dsp_fgc.h>   // for DSP_PARS_RunTime
#include <ref_class.h>          // for RefPrepareAbort()
#include <macros.h>
#include <mcu_dsp_common.h>

// ToDo : try to clean this, here is DSP so no MCU memory map
#include <memmap_mcu.h>         // for RFC_STATUS_ACK_MASK16

/*---------------------------------------------------------------------------------------------------------*/
void RtEvents(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the timing event delays from the MCU to see if a Start or Abort timing event has
  been received.  It sets the relevant property value and processes the time.  This function is called
  every millisecond except ms 0 and to spread the load, it only processes one channel per millisecond (2-17).

  Note that functions are calculated on the millisecond preceding their transmission, so the functions are
  run 1 ms in advance of the event time.  This is done by the ParsRunTime() and ParsAbortTime() functions.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t                      ms_time;
    static uint32_t               chan_idx;
    static uint32_t               chan_mask;
    static uint32_t               run_time_f;
    static uint32_t               abort_time_f;
    static struct abs_time_ms   fbs_time;


    switch(rtcom.ms20)
    {
        case 1:             // Millisecond 1 of 20

            fbs_time     = dpcom->mcu.time.fbs; // Take local copy for efficiency reasons
            run_time_f   = false;
            abort_time_f = false;
            chan_idx     = 0;
            chan_mask    = 1;
            break;

        default:                // Millisecond 2-17 of 20

            // If Start event is active

            if(dpcls->mcu.ref.start_event_delay[chan_idx])
            {
                if(dpcls->dsp.fgstate.func[chan_idx] == FGC_FG_ARMED)
                {
                    ms_time = fbs_time.ms_time + dpcls->mcu.ref.start_event_delay[chan_idx];
                    property.fgfunc.run[chan_idx].unix_time = fbs_time.unix_time + ms_time/1000;
                    property.fgfunc.run[chan_idx].us_time   = (ms_time % 1000) * 1000;

                    Set(rt_status.start_evt,chan_mask);
                    run_time_f = true;
                }

                dpcls->mcu.ref.start_event_delay[chan_idx] = 0;
            }

            // If Abort event is active

            if(dpcls->mcu.ref.abort_event_delay[chan_idx])
            {
                if(dpcls->dsp.fgstate.func[chan_idx] == FGC_FG_RUNNING ||
                   dpcls->dsp.fgstate.func[chan_idx] == FGC_FG_ARMED)
                {
                    ms_time = dpcom->mcu.time.fbs.ms_time + dpcls->mcu.ref.abort_event_delay[chan_idx];
                    property.fgfunc.abort[chan_idx].unix_time = dpcom->mcu.time.fbs.unix_time + ms_time/1000;
                    property.fgfunc.abort[chan_idx].us_time   = (ms_time % 1000) * 1000;

                    Set(rt_status.abort_evt,chan_mask);
                    abort_time_f = true;
                }
                dpcls->mcu.ref.abort_event_delay[chan_idx] = 0;
            }

            chan_idx++;
            chan_mask <<= 1;
            break;

        case 18:                // Millisecond 18 of 20

            if(run_time_f)
            {
                UPDATE_PARS(DSP_PARS_RunTime);  // Prepare to process new run times with 1 ms advance
            }
            break;

        case 19:                // Millisecond 19 of 20

            if(abort_time_f)
            {
                UPDATE_PARS(DSP_PARS_AbortTime);    // Prepare to process new abort times with 1 ms advance
            }
            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RtData(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the new RT data from the MCU and converts the IEEE floats to TI floats for the
  channels that have changed
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t          rt_chan_idx;        // RT channel index (1-15)
    DP_IEEE_FP32    rt_ieee_val;        // RT data value in IEEE format

    for( rt_chan_idx = 1; rt_chan_idx < FGC_N_FGEN_CHANS; rt_chan_idx++ )
    {
        rt_ieee_val = dpcls->mcu.wval[rt_chan_idx];

        if(rt_ieee_val != rt.wval_raw[rt_chan_idx])
        {
            rt.wval_raw[rt_chan_idx] = rt_ieee_val;
            rt.wval_17[rt_chan_idx]  = FromIEEE(rt_ieee_val);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RtStates(uint32_t chan_idx, uint32_t chan_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function runs the channel state machines.
\*---------------------------------------------------------------------------------------------------------*/
{
    /*--- NOT_ACK state ---*/

    if(Test(rt_state.not_ack, chan_mask))                       // If in NOT_ACK state
    {
        if(!Test(rt_status.not_ack|rt_status.prev_not_ack,chan_mask))   // if channel acknowledged for 2 ms
        {
            dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_IDLE;                    // Move to IDLE
            Clr(rt_state.not_ack,chan_mask);
            Set(rt_state.idle,   chan_mask);
        }
    }
    else if((chan_mask & rt_status.not_ack & rt_status.prev_not_ack))   // else if chan not ack for 2 ms
    {
        rt.ref_type[chan_idx] = FGC_REF_NONE;                   // Reset Ref type
        dpcls->mcu.ref.type_old[chan_idx] = FGC_REF_NONE;               // Set old value first
        dpcls->mcu.ref.type    [chan_idx] = FGC_REF_NONE;               // then new value
        dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_NOT_ACK;             // Move to NOT_ACK state
        Set(rt_state.not_ack,  chan_mask);
        Clr(rt_state.idle,     chan_mask);
        Clr(rt_state.armed,    chan_mask);
        Clr(rt_state.running,  chan_mask);
        Clr(rt_state.aborting, chan_mask);
        return;
    }

    /*--- Run state machine for other states ---*/

    switch(dpcls->dsp.fgstate.func[chan_idx])
    {
        case FGC_FG_IDLE:               // IDLE STATE

            if(rt.ref_type[chan_idx] == FGC_REF_ARMED)      // If function has been armed
            {
                dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_ARMED;       // Advance to ARMED
                Clr(rt_state.idle, chan_mask);
                Set(rt_state.armed,chan_mask);
            }
            break;

        case FGC_FG_ARMED:              // ARMED STATE

            if(AbsTimeUs_IsGreaterThan(rtcom.time_now_us, rt_fgfunc.abort[chan_idx]))       // If abort time has arrived
            {
                rt.ref_type[chan_idx] = FGC_REF_NONE;           // Reset Ref type
                dpcls->mcu.ref.type_old[chan_idx] = FGC_REF_NONE;       // Set old value first
                dpcls->mcu.ref.type    [chan_idx] = FGC_REF_NONE;       // then new value
                dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_IDLE;        // Return to IDLE
                Clr(rt_state.armed,chan_mask);
                Set(rt_state.idle, chan_mask);
            }
            else if(AbsTimeUs_IsGreaterThan(rtcom.time_now_us, rt_fgfunc.run[chan_idx]))    // else if run time has arrived
            {
                rt.ref_type[chan_idx] = bg.ref_type[chan_idx];      // Set function to run
                dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_RUNNING;     // Advance to RUNNING
                Clr(rt_state.armed,  chan_mask);
                Set(rt_state.running,chan_mask);
            }
            break;

        case FGC_FG_RUNNING:            // RUNNING STATE

            if(rt.ref_type[chan_idx] == FGC_REF_NONE)       // If function has completed
            {
                dpcls->mcu.ref.type_old[chan_idx] = FGC_REF_NONE;       // Set old value first
                dpcls->mcu.ref.type    [chan_idx] = FGC_REF_NONE;       // then new value
                dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_IDLE;        // Return to IDLE
                Clr(rt_state.running,chan_mask);
                Set(rt_state.idle,    chan_mask);
            }
            else if(AbsTimeUs_IsGreaterThan(rtcom.time_now_us, rt_fgfunc.abort[chan_idx]))  // else if abort time has arrived
            {
                rt.ref_type  [chan_idx] = FGC_REF_ABORTING;         // Set Ref type to ABORTING
                dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_ABORTING;    // Advance to ABORTING

                RefPrepareAbort(chan_idx);                  // Prepare to abort the functions

                if(rt.ref_type[chan_idx] == FGC_REF_NONE)           // If function has now completed
                {
                    dpcls->mcu.ref.type_old[chan_idx] = FGC_REF_NONE;       // Set old value first
                    dpcls->mcu.ref.type    [chan_idx] = FGC_REF_NONE;       // then new value
                    dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_IDLE;        // Return to IDLE
                    Clr(rt_state.running,chan_mask);
                    Set(rt_state.idle,    chan_mask);
                }
                else                            // else run abort
                {
                    Clr(rt_state.running, chan_mask);
                    Set(rt_state.aborting,chan_mask);
                }
            }

            rt.ref_time_tb[chan_idx] += IT_PERIOD_TB;

            break;

        case FGC_FG_ABORTING:           // ABORTING STATE

            rt.ref_time_tb[chan_idx] += IT_PERIOD_TB;

            if(rt.ref_type[chan_idx] == FGC_REF_NONE)       // If function has completed
            {
                dpcls->mcu.ref.type_old[chan_idx] = FGC_REF_NONE;       // Set old value first
                dpcls->mcu.ref.type    [chan_idx] = FGC_REF_NONE;       // then new value
                dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_IDLE;        // Return to IDLE
                Clr(rt_state.aborting,chan_mask);
                Set(rt_state.idle,    chan_mask);
            }
            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RtControl(uint32_t chan_idx, uint32_t chan_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function processes the real-time control part of the channel.  This includes linear interpolation
  between the current value and a new value with the period specified by the user in FGRT.PERIOD.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32    val;

    if(property.fgrt.control[chan_idx])         // If RT channel is in use
    {
        Set(rt_status.rt_ctrl,chan_mask);

        val = rt.wval_17[property.fgrt.control[chan_idx]];

        if(val != rt.lval_18[chan_idx])             // If RT value has changed
        {
            rt.lval_18[chan_idx] = val;                     // Save new value
            rt_control.count[chan_idx] = rt_control.period_ms[chan_idx];    // Set count to period in ms
            rt_control.step[chan_idx]  = (val - rt.tval_19[chan_idx]) *     // Calculate interpolation
                              rt_control.inv_period[chan_idx];  // step size
        }

        if(rt_control.count[chan_idx])              // If interpolation is running
        {
            rt_control.count[chan_idx]--;                   // Dec interplation counter
            val = rt.tval_19[chan_idx] = rt.tval_19[chan_idx] +         // Add interpolation step
                             rt_control.step[chan_idx];
        }
        else                            // else interpolation has finished
        {
            rt.tval_19[chan_idx] = val;                     // Use final value
        }

        /* Positive RT control clip -> pval_20 */

        if(val > property.fgconf.limit.rt.pos[chan_idx])
        {
            val = rt.pval_20[chan_idx] = property.fgconf.limit.rt.pos[chan_idx];
            Set(rt_status.lim_rt_pos,chan_mask);
        }
        else
        {
            rt.pval_20[chan_idx] = val;
        }

        /* Negative RT control clip -> rval_21 */

        if(val < property.fgconf.limit.rt.neg[chan_idx])
        {
            val = rt.rval_21[chan_idx] = property.fgconf.limit.rt.neg[chan_idx];
            Set(rt_status.lim_rt_neg,chan_mask);
        }
        else
        {
            rt.rval_21[chan_idx] = val;
        }
    }
    else
    {
        rt.lval_18[chan_idx] = rt.tval_19[chan_idx] = rt.pval_20[chan_idx] = rt.rval_21[chan_idx] = 0.0;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RtChannel(uint32_t chan_idx, uint32_t chan_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function implements the signal change for a channel starting with fval_10 and rval_21 and leading
  top cval_15 and ival_16, which are transfered to the MCU.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32    val;
    signed  ival;

    /* fval_10 + rval_21 -> sval_11 */

    val = rt.sval_11[chan_idx] = rt.fval_10[chan_idx] + rt.rval_21[chan_idx];

    /* Max chan clip -> xval_12 */

    if(val > property.fgconf.limit.chan.max[chan_idx])
    {
        val = rt.xval_12[chan_idx] = property.fgconf.limit.chan.max[chan_idx];
        Set(rt_status.lim_chan_max,chan_mask);
    }
    else
    {
        rt.xval_12[chan_idx] = val;
    }

    /* Min chan clip -> nval_13 */

    if(val < property.fgconf.limit.chan.min[chan_idx])
    {
        val = rt.nval_13[chan_idx] = property.fgconf.limit.chan.min[chan_idx];
        Set(rt_status.lim_chan_min,chan_mask);
    }
    else
    {
        rt.nval_13[chan_idx] = val;
    }

    /* Positive rate clip -> oval_14 */

    if((val - rt.oval_14[chan_idx]) > rt_limit.rate_pos[chan_idx])
    {
        val = rt.oval_14[chan_idx] = rt.oval_14[chan_idx] + rt_limit.rate_pos[chan_idx];
        Set(rt_status.lim_rate_pos,chan_mask);
    }
    else
    {
        rt.oval_14[chan_idx] = val;
    }

    /* Negative rate clip -> cval_15 */

    if((val - rt.cval_15[chan_idx]) < rt_limit.rate_neg[chan_idx])
    {
        val = rt.cval_15[chan_idx] = rt.cval_15[chan_idx] + rt_limit.rate_neg[chan_idx];
        Set(rt_status.lim_rate_neg,chan_mask);
    }
    else
    {
        rt.cval_15[chan_idx] = val;
    }

    /* Scaling -> ival_16 */

    ival = (signed)(property.fgconf.gain[chan_idx] * (val - property.fgconf.offset[chan_idx])+ 0.49999999);

    /* Clip to 16 bits */

    if(ival > 32767)
    {
        ival = 32767;
    }

    if(ival < -32768)
    {
        ival = -32768;
    }

    rt.ival_16[chan_idx] = (FP32)ival;

    /* Send values to MCU */

    dpcls->dsp.fgfunc.cval[chan_idx] = ToIEEE(val);
    dpcls->dsp.fgfunc.ival[chan_idx] = ival;
}
/*---------------------------------------------------------------------------------------------------------*/
void RtStatusAndStats(uint32_t chan_idx, uint32_t chan_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function analyses the status word returned by the RFC card for a channel and sets the status
  properties accordingly.  It also accumulates statistics.  Note that the status for a transmission are
  only available 2 iterations later.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  status;

    /* Use real status in NORMAL mode, otherwise assume status is ACK */

    if(rtcom.state_op == FGC_OP_NORMAL)
    {
        status = dpcls->mcu.rfc_status[chan_idx];
    }
    else
    {
        status = RFC_STATUS_ACK_MASK16;
    }

    if(Test(status,RFC_STATUS_ACK_MASK16))
    {
        property.fgstats.acks[chan_idx]++;
        Set(rt_status.ack,chan_mask);
        Clr(rt_status.not_ack,chan_mask);
    }
    else
    {
        Set(rt_status.not_ack,chan_mask);
        Set(rt_status.not_ack_20ms,chan_mask);
    }

    if(Test(status,RFC_STATUS_NACK_MASK16))
    {
        property.fgstats.nacks[chan_idx]++;
        Set(rt_status.nack,        chan_mask);
        Set(rt_status.not_ack_20ms,chan_mask);
    }

    if(Test(status,RFC_STATUS_TRIGGERED_MASK16))
    {
        property.fgstats.triggers[chan_idx]++;
    }

    if(Test(status,RFC_STATUS_DATASENT_MASK16))
    {
        property.fgstats.data_sents[chan_idx]++;
    }

    if(Test(status,RFC_STATUS_BADCRC_MASK16))
    {
        property.fgstats.bad_crcs[chan_idx]++;
        rt_status.crc_err_f = true;
    }

    if(Test(status,RFC_STATUS_LINKTO_MASK16))
    {
        property.fgstats.timeouts[chan_idx]++;
        rt_status.timeout_f = true;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RtReport20ms(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reports values to the MCU on 20ms boundaries
\*---------------------------------------------------------------------------------------------------------*/
{
    /*----- Send status/state data to MCU -----*/

    dpcom->dsp.st_latched = rtcom.st_latched;       // Latched status flags (set by lib for PSU V check)
    rtcom.st_latched    = 0;                // Clear latched status

    dpcls->dsp.idle     = rt_state.idle;        // Transfer FGEN state masks
    dpcls->dsp.armed    = rt_state.armed;
    dpcls->dsp.running  = rt_state.running;
    dpcls->dsp.aborting = rt_state.aborting;
    dpcls->dsp.not_ack  = rt_state.not_ack;

    /*----- Send stretched fault data to MCU for RTD/Front Panel LEDs -----*/

    rt_status.lim_any = rt_status.lim_rt_pos   |
            rt_status.lim_rt_neg   |
            rt_status.lim_chan_max |
            rt_status.lim_chan_min |
            rt_status.lim_rate_pos |
            rt_status.lim_rate_neg;

    if(++rt.stretch_counter > 500)          // Every 10s
    {
        rt.stretch_counter = 0;

        dpcls->dsp.lim_any_10s   &= rt_status.lim_any_10s;
        dpcls->dsp.not_ack_10s   &= rt_status.not_ack_10s;
        dpcls->dsp.crc_err_10s_f &= rt_status.crc_err_10s_f;

        rt_status.lim_any_10s   = rt_status.lim_any;
        rt_status.not_ack_10s   = rt_status.not_ack_20ms;
        rt_status.crc_err_10s_f = rt_status.crc_err_f;
    }
    else                        // else
    {
        dpcls->dsp.lim_any_10s   |= (rt_status.lim_any_10s   |= rt_status.lim_any);
        dpcls->dsp.not_ack_10s   |= (rt_status.not_ack_10s   |= rt_status.not_ack_20ms);
        dpcls->dsp.crc_err_10s_f |= (rt_status.crc_err_10s_f |= rt_status.crc_err_f);
    }

    dpcls->dsp.loop_f = rt_status.timeout_f ||
              ((rt_status.ack | rt_status.nack) & rt.trigger) != rt.trigger;

    /*--- Send FGSTATUS property values ---*/

    dpcls->dsp.rt_ctrl      = rt_status.rt_ctrl;
    dpcls->dsp.ack          = rt_status.ack;
    dpcls->dsp.nack         = rt_status.nack;
    dpcls->dsp.start_evt    = rt_status.start_evt;
    dpcls->dsp.abort_evt    = rt_status.abort_evt;
    dpcls->dsp.lim_rt_pos   = rt_status.lim_rt_pos;
    dpcls->dsp.lim_rt_neg   = rt_status.lim_rt_neg;
    dpcls->dsp.lim_chan_max = rt_status.lim_chan_max;
    dpcls->dsp.lim_chan_min = rt_status.lim_chan_min;
    dpcls->dsp.lim_rate_pos = rt_status.lim_rate_pos;
    dpcls->dsp.lim_rate_neg = rt_status.lim_rate_neg;

    /*--- Clear status variables ready for the next 20ms period ---*/

    rt_status.rt_ctrl      = 0;
    rt_status.ack          = 0;
    rt_status.nack         = 0;
    rt_status.not_ack_20ms = 0;
    rt_status.start_evt    = 0;
    rt_status.abort_evt    = 0;
    rt_status.lim_rt_pos   = 0;
    rt_status.lim_rt_neg   = 0;
    rt_status.lim_chan_max = 0;
    rt_status.lim_chan_min = 0;
    rt_status.lim_rate_pos = 0;
    rt_status.lim_rate_neg = 0;
    rt_status.crc_err_f    = 0;
    rt_status.timeout_f    = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void RtSpySend(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends values to the SPY interface.  There are six channels, all of which can be selected
  from a range of choices by the user.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  idx;
    uint32_t  v;

    /*--- Write six words to SPY (29us for V2.3) ---*/

    WAITSPY;
    SPY(SPY_SYNCH);                     // Write synch word (0xFFFFFFFF)

    for( idx = 0; idx < FGC_N_SPY_CHANS; idx++ )          // Write data words
    {
        v = RtSpyMpx(idx);              // Get data word for this channel
        WAITSPY;                        // Wait for previous word to be sent
        SPY(v);                         // Send word to SPY
    }
}
/*---------------------------------------------------------------------------------------------------------*/
inline DP_IEEE_FP32 RtSpyMpx(uint32_t idx)
/*---------------------------------------------------------------------------------------------------------*\
  This returns the relevant signal based on the choice in spy_mpx.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(rt_spyfgmpx.sig_idx[idx])
    {
        case  0:    return(ToIEEE(rt.fspy_0 [rt_spyfgmpx.chan_idx[idx]]));
        case 10:    return(ToIEEE(rt.fval_10[rt_spyfgmpx.chan_idx[idx]]));
        case 11:    return(ToIEEE(rt.sval_11[rt_spyfgmpx.chan_idx[idx]]));
        case 12:    return(ToIEEE(rt.xval_12[rt_spyfgmpx.chan_idx[idx]]));
        case 13:    return(ToIEEE(rt.nval_13[rt_spyfgmpx.chan_idx[idx]]));
        case 14:    return(ToIEEE(rt.oval_14[rt_spyfgmpx.chan_idx[idx]]));
        case 15:    return(dpcls->dsp.fgfunc.cval[rt_spyfgmpx.chan_idx[idx]]);
        case 16:    return(ToIEEE(rt.ival_16[rt_spyfgmpx.chan_idx[idx]]));
        case 17:    return(ToIEEE(rt.wval_17[rt_spyfgmpx.chan_idx[idx]]));
        case 18:    return(ToIEEE(rt.lval_18[rt_spyfgmpx.chan_idx[idx]]));
        case 19:    return(ToIEEE(rt.tval_19[rt_spyfgmpx.chan_idx[idx]]));
        case 20:    return(ToIEEE(rt.pval_20[rt_spyfgmpx.chan_idx[idx]]));
        case 21:    return(ToIEEE(rt.rval_21[rt_spyfgmpx.chan_idx[idx]]));
    }

    return(0);                  // Unknown channel
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: 59\src\rt.c
\*---------------------------------------------------------------------------------------------------------*/
