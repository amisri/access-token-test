/*---------------------------------------------------------------------------------------------------------*\
  File: ref.c

  Purpose: FGC DSP Software - contains real-time Ref functions
\*---------------------------------------------------------------------------------------------------------*/

#define REF_GLOBALS

#include <ref_class.h>
#include <main.h>               // for bg global variable
#include <rt.h>                 // for rt global variable
#include <math.h>               // for sin()

/*---------------------------------------------------------------------------------------------------------*/
FP32 RefNone(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when ref.type is FGC_REF_NONE or FGC_REF_ARMED.  In this state, the predefined reference
  is held constant.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(rt.fval_10[chan_idx]);
}
/*---------------------------------------------------------------------------------------------------------*/
FP32 RefPlp(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function derives the reference if the PLP function is selected
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32   t;
    uint32_t ref_time_tb = rt.ref_time_tb[chan_idx];

    /*--- Run-delay coast ---*/

    if(ref_time_tb <= plp.t[0][chan_idx])
    {
        return(rt.fval_10[chan_idx]);
    }

    /*--- Parabolic acceleration---*/

    else if(ref_time_tb <= plp.t[1][chan_idx])
    {
        t = FG_CONV_TB_TO_S(ref_time_tb - plp.t[0][chan_idx]);

        return(plp.r[0][chan_idx] + 0.5 * plp.a[chan_idx] * t * t);
    }

    /*--- Linear ramp ---*/

    else if(ref_time_tb <= plp.t[2][chan_idx])
    {
        t = FG_CONV_TB_TO_S(ref_time_tb - plp.t[1][chan_idx]);

        return(plp.r[1][chan_idx] + plp.l[chan_idx] * t);
    }

    /*--- Parabolic deceleration ---*/

    else if(ref_time_tb < plp.t[3][chan_idx])
    {
        t = FG_CONV_TB_TO_S(plp.t[3][chan_idx] - ref_time_tb);

        return(plp.r[3][chan_idx] - 0.5 * plp.a[chan_idx] * t * t);
    }

    /*--- Coast ---*/

    rt.ref_type[chan_idx] = FGC_REF_NONE;

    return(plp.r[3][chan_idx]);
}
/*---------------------------------------------------------------------------------------------------------*/
FP32 RefCTrim(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function derives the reference if the CTRIM function is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32   t;
    uint32_t ref_time_tb = rt.ref_time_tb[chan_idx];

    /*--- Pre trim coast ---*/

    if(ref_time_tb <= ctrim.tstart[chan_idx])
    {
        return(rt.fval_10[chan_idx]);
    }

    /*--- Trim ---*/

    else if(ref_time_tb <= ctrim.tend[chan_idx])
    {
        t = FG_CONV_TB_TO_S(ref_time_tb - ctrim.tstart[chan_idx]) - ctrim.t0[chan_idx];

        return(ctrim.r0[chan_idx] + (ctrim.c [chan_idx] + ctrim.a[chan_idx] * t * t) * t);
    }

    /*--- Post-trim coast ---*/

    rt.ref_type[chan_idx] = FGC_REF_NONE;

    return(ctrim.rend[chan_idx]);
}
/*---------------------------------------------------------------------------------------------------------*/
FP32 RefTable(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function derives the reference if the TABLE function is selected.
\*---------------------------------------------------------------------------------------------------------*/
{
    const FP32 ref_time_s = FG_CONV_TB_TO_S(rt.ref_time_tb[chan_idx]);

    uint32_t idx;
    FP32   next_time;
    FP32   next_value;
    FP32   t;

    // Check for end of segment

    if(ref_time_s >= tbl.end_time[chan_idx])
    {
        idx = ++tbl.idx[chan_idx];

        // Terminate function if table is finished

        if(idx >= tbl.nels[chan_idx])
        {
            rt.ref_type[chan_idx] = FGC_REF_NONE;
            return(tbl.end_value[chan_idx]);
        }

        ++tbl.function[chan_idx];

        next_time  = tbl.function[chan_idx]->time + tbl.run_delay[chan_idx];
        next_value = tbl.function[chan_idx]->ref;

        tbl.grad[chan_idx] = (next_value - tbl.end_value[chan_idx]) /
                             (next_time  - tbl.end_time [chan_idx]);

        tbl.end_time [chan_idx] = next_time;
        tbl.end_value[chan_idx] = next_value;
    }

    t = tbl.end_time[chan_idx] - ref_time_s;

    return(tbl.end_value[chan_idx] - t * tbl.grad[chan_idx]);
}
/*---------------------------------------------------------------------------------------------------------*/
FP32 RefSine(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function derives the reference for the Sine function.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t	ref_time_tb = rt.ref_time_tb[chan_idx];
    FP32	fval_10;

    /*--- Pre-function coast ---*/

    if(ref_time_tb <= sine.run_delay[chan_idx])
    {
        return(rt.fval_10[chan_idx]);
    }

    /*--- Operate N cycles ---*/

    if(ref_time_tb < sine.end_time[chan_idx])
    {
        fval_10 = sine.ref0[chan_idx] + sine.ref_amp[chan_idx] *
                  sin(sine.freq_rad[chan_idx] * (FP32)(ref_time_tb - sine.run_delay[chan_idx]));

        if(rt.ref_type[chan_idx] != FGC_REF_ABORTING ||
                fval_10 < sine.ref0[chan_idx]          ||
                rt.fval_10[chan_idx] >= sine.ref0[chan_idx])
        {
            return(fval_10);
        }
    }

    /*--- Coast ---*/

    rt.ref_type[chan_idx] = FGC_REF_NONE;

    return(sine.ref0[chan_idx]);
}
/*---------------------------------------------------------------------------------------------------------*/
void RefPrepareAbort(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares for an abort
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 dr;    // Reference relative to final value

    if(bg.ref_type[chan_idx] != FGC_REF_SINE)    // If function was not SINE
    {
        rt_abort.time[chan_idx] = rt.ref_time_tb[chan_idx];    // Register start of abort time

        dr = (rt.fval_10[chan_idx] - rt.last_fval_10[chan_idx]);    // Calculate change of function per ms

        if(dr == 0.0)    // If function is stationary
        {
            rt.ref_type[chan_idx] = FGC_REF_NONE;    // End abort immediately
            return;
        }

        rt_abort.d[chan_idx]   = dr * rt_abort.inv_period_ms[chan_idx];    // Deceleration (/100us^2)
        rt_abort.ref[chan_idx] = rt.fval_10[chan_idx] + dr * rt_abort.half_period_ms[chan_idx];
    }
    else                                                            // else function is SINE
    {
        if(rt.ref_time_tb[chan_idx] <= sine.run_delay[chan_idx])    // If function never started
        {
            rt.ref_type[chan_idx] = FGC_REF_NONE;                   // End abort immediately
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
FP32 RefAbort(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function derives the reference during an abort.  It uses the abort deceleration and final referance
  derived on the first iteration.  The abort function is simply a parabola, i.e. the rate of change of
  reference is reduced linearly using the default acceleration for the deceleration rate
  unless a SINE function is active, in which case the function ends at the end of the current period.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32   t;               // Abort time in seconds (counts down to zero)
    FP32   dr;              // Reference relative to final value
    uint32_t ref_time_tb;     // Time since start of abort
    uint32_t abort_period;    // Period of abort

    /*--- Abort of SINE function ---*/

    if(bg.ref_type[chan_idx] == FGC_REF_SINE)    // If function was SINE
    {
        return(RefSine(chan_idx));
    }

    /*--- Abort of PLP, TABLE or CTRIM function ---*/

    ref_time_tb     = rt.ref_time_tb[chan_idx] - rt_abort.time[chan_idx];
    abort_period = rt_abort.period[chan_idx];

    if(ref_time_tb < abort_period)                  // If abort in progress
    {
        t = (FP32)(abort_period - ref_time_tb);     // Calculate abort time (in 100us)
        dr = 0.5 * rt_abort.d[chan_idx] * t * t;    // and reference value
    }
    else                                            // else abort complete
    {
        dr = 0.0;
        rt.ref_type[chan_idx] = FGC_REF_NONE;       // Reset Ref type
    }

    return(rt_abort.ref[chan_idx] - dr);            // return reference
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file ref.c
\*---------------------------------------------------------------------------------------------------------*/
