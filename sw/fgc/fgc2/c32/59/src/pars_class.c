/*---------------------------------------------------------------------------------------------------------*\
  File:         pars_class.c

  Purpose:      FGC DSP Software - Parameter group conversion functions.

  Notes:        Unlike Class 51/52, these functions are run at background level to avoid overruns of the
                real-time ISR.
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS                // Force property table definition

#include <pars_class.h>
#include <rt.h>                 // for rt_fgfunc global variable
#include <dsp_dependent.h>      // for DISABLE_INTS, LEDS_INIT, LED0_TOGGLE, LED1_TOGGLE
#include <rtcom.h>              // for rtcom global variable
#include <props_class.h>        // for property global variable
#include <dpcls.h>              // for dpcls global variable
#include <lib.h>                // for
#include <main.h>               // for TABLE_TIME()
#include <diag.h>               // for qspi_misc, dim_list global variable
#include <property.h>
#include <defprops_dsp_fgc.h>   // for pars_func[idx]()
#include <stdio.h>              // for sprintf()
#include <math.h>               // for fabs()
#include <macros.h>
#include <mcu_dsp_common.h>

/*---------------------------------------------------------------------------------------------------------*/
void ParsGroupSet(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at the end of the millisecond ISR if the par_group_set_mask is non-zero, which
  indicates that at least one parameter group has had a property set. It does not necessarily mean that
  the property has changed value. The function identifies the related property group from the bit set in
  the mask and calls the appropriate function.  Only one function is ever executed per millisecond to reduce
  the risk of overrun.  If a function is run, the associated bit is cleared in the change mask.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      idx;
    uint32_t      mask;

    // Search for LSB which is set

    for(idx=0,mask=1 ; !(rtcom.par_group_set_mask & mask) ; mask<<=1,idx++);

    // Run parameter group function and clear the bit in the mask

    pars_func[idx]();

    rtcom.par_group_set_mask &= ~mask;
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsTrigger(void)
/*---------------------------------------------------------------------------------------------------------*\
  The trigger property has been set.  This checks for each channel if it should enabled or disabled.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      chan_idx;                       // FGEN channel 0-15
    uint32_t      chan_mask;
    char        trig_str[5];

    for(chan_idx=0,chan_mask=1;chan_idx < FGC_N_FGEN_CHANS;chan_idx++,chan_mask<<=1)
    {
        if(property.fgconf.trigger[chan_idx] == FGC_CTRL_DISABLED)      // If channel should be disabled
        {
            if(Test(rt.trigger,chan_mask))                                      // If channel is currently active
            {
                DISABLE_INTS;
                Clr(rt.trigger, chan_mask);                                     // Clear trigger bit
                dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_OFF;                 // Change to OFF
                rt.ref_type            [chan_idx] = FGC_REF_NONE;               // Clear reference
                dpcls->mcu.ref.type_old[chan_idx] = FGC_REF_NONE;               // Set old value first
                dpcls->mcu.ref.type    [chan_idx] = FGC_REF_NONE;               // then new value
                Clr(rt_state.idle,     chan_mask);                              // Clear state bits
                Clr(rt_state.armed,    chan_mask);
                Clr(rt_state.running,  chan_mask);
                Clr(rt_state.aborting, chan_mask);
                Clr(rt_state.not_ack,  chan_mask);
                ENABLE_INTS;
            }
        }
        else                                              // else channel should be enabled
        {
            if(!Test(rt.trigger,chan_mask))                     // If channel is currently inactive
            {
                DISABLE_INTS;
                rt.cval_15[chan_idx] = rt.oval_14[chan_idx] = rt.fval_10[chan_idx];
                Set(rt_state.idle,chan_mask);
                Set(rt.trigger,chan_mask);                      // Set trigger bit
                rt_status.ack |= rt.trigger;                    // Init ack to avoid transient loop faults
                dpcls->dsp.fgstate.func[chan_idx] = FGC_FG_IDLE;// Start in Idle
                ENABLE_INTS;
            }
        }
    }

    // Cast rt.trigger to (unsigned long) to satisfy cppcheck. This shouldn't be needed
    // because rt.trigger is an uint32_t, which is a typedef for unsigned long.

    sprintf(trig_str,"%04lX",(unsigned long)rt.trigger);        // Produce ASCII hex string version of trigger for display

    dpcls->dsp.trig_str = (trig_str[3] <<  0) |
                          (trig_str[2] <<  8) |
                          (trig_str[1] << 16) |
                          (trig_str[0] << 24);
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsLimits(void)
/*---------------------------------------------------------------------------------------------------------*\
  A property concerned with the channel limits and scaling has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      chan_idx;                       // FGEN channel 0-15
    FP32        cval_15_max;
    FP32        cval_15_min;
    FP32        delta;
    FP32        inv_gain;

    for(chan_idx=0;chan_idx < FGC_N_FGEN_CHANS;chan_idx++)
    {
        // Protect against a zero gain

        if(property.fgconf.gain[chan_idx] != 0.0)
        {
            inv_gain = 1.0 / fabs(property.fgconf.gain[chan_idx]);
        }
        else
        {
            inv_gain = 0.0;
        }

        cval_15_max = property.fgconf.offset[chan_idx] + 32767.0 * inv_gain;
        cval_15_min = property.fgconf.offset[chan_idx] - 32768.0 * inv_gain;

        if(property.fgconf.limit.user.max[chan_idx] < cval_15_max)
        {
            cval_15_max = property.fgconf.limit.user.max[chan_idx];
        }
        if(property.fgconf.limit.user.min[chan_idx] > cval_15_min)
        {
            cval_15_min = property.fgconf.limit.user.min[chan_idx];
        }

        delta = 0.5 * inv_gain;                                                 // 1/2 a bit
        property.fgconf.limit.chan.max[chan_idx] = cval_15_max + delta;         // Expand by 1/2 bit to
        property.fgconf.limit.chan.min[chan_idx] = cval_15_min - delta;         // avoid rounding errors

        if(rt.fval_10[chan_idx] > cval_15_max)                                  // Clip function value
        {                                                                       // within the new limits
            rt.fval_10[chan_idx] = cval_15_max;
        }
        else if(rt.fval_10[chan_idx] < cval_15_min)
        {
            rt.fval_10[chan_idx] = cval_15_min;
        }

        // Iteration period is 1 ms on FGC2.

        rt_limit.rate_pos[chan_idx] = property.fgconf.limit.rate.pos[chan_idx] * 0.001;         // positive rate times 1 ms
        rt_limit.rate_neg[chan_idx] = property.fgconf.limit.rate.neg[chan_idx] * 0.001;         // negative rate times 1 ms
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsSpyFgMpx(void)
/*---------------------------------------------------------------------------------------------------------*\
  The SPY.FGMPX property has changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      spy_idx;                        // Spy channel index 0-5
    uint32_t      sig_idx;                        // Spy signal index 0,10,11,12,...,20,21
    uint32_t      chan_idx;                       // FGEN channel 0-15

    for(spy_idx=0;spy_idx < FGC_N_SPY_CHANS;spy_idx++)
    {
        sig_idx  = property.spy.fgmpx[spy_idx] / 100;
        chan_idx = property.spy.fgmpx[spy_idx] - 100 * sig_idx;

        if(chan_idx >= FGC_N_FGEN_CHANS ||
          (sig_idx > 0 && sig_idx < 10) ||
          (sig_idx > 21))
        {
            sig_idx  = 0;
            chan_idx = 0;
        }

        rt_spyfgmpx.sig_idx [spy_idx] = sig_idx;
        rt_spyfgmpx.chan_idx[spy_idx] = chan_idx;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsRunTime(void)
/*---------------------------------------------------------------------------------------------------------*\
  An Absolute Run Time parameter has changed.  Note that functions are calculated on the millisecond
  preceding their transmission, so the functions are run 1 ms in advance of the event time.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      chan_idx;                       // RF channel 0-15

    for(chan_idx=0;chan_idx < FGC_N_FGEN_CHANS;chan_idx++)
    {
        rt_fgfunc.run[chan_idx] = property.fgfunc.run[chan_idx];

        if(rt_fgfunc.run[chan_idx].unix_time == 0)      // If run time is zero
        {
            rt_fgfunc.run[chan_idx].unix_time = TIME_NEVER;     // Set to NEVER
            rt_fgfunc.run[chan_idx].us_time   = 0;
        }
        else
        {
            AbsTimeUsAddLongDelay(&rt_fgfunc.run[chan_idx], -1, 999000); // Advance by 1 ms (i.e. -1s + 0.999s = -1 ms)
        }

        NOP;                                            // Required due to compiler bug
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsAbortTime(void)
/*---------------------------------------------------------------------------------------------------------*\
  An Absolute Abort Time parameter has changed.  Note that functions are calculated on the millisecond
  preceding their transmission, so the functions are aborted 1 ms in advance of the event time.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      chan_idx;                       // RF channel 0-15

    for(chan_idx=0;chan_idx < FGC_N_FGEN_CHANS;chan_idx++)
    {
        rt_fgfunc.abort[chan_idx] = property.fgfunc.abort[chan_idx];

        if(rt_fgfunc.abort[chan_idx].unix_time == 0)    // If abort time is zero
        {
            rt_fgfunc.abort[chan_idx].unix_time = TIME_NEVER;   // Set to NEVER
            rt_fgfunc.abort[chan_idx].us_time   = 0;
        }
        else
        {
            AbsTimeUsAddLongDelay(&rt_fgfunc.abort[chan_idx], -1, 999000);  // Advance by 1 ms (i.e. -1s + 0.999s = -1 ms)
        }

        NOP;                                            // Required due to compiler bug
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void ParsRtPeriod(void)
/*---------------------------------------------------------------------------------------------------------*\
  The RT period property has changed.  Some factors need to be calculated to improved real-time
  performance by avoiding divisions in RtControl()
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      chan_idx;                       // RF channel 0-15

    for(chan_idx=0;chan_idx < FGC_N_FGEN_CHANS;chan_idx++)
    {
        rt_control.period_ms [chan_idx] = property.fgrt.period[chan_idx] / 10;          // Convert period to ms
        rt_control.inv_period[chan_idx] = 1.0 / (FP32)rt_control.period_ms[chan_idx];   // Calculate factor
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: 59\src\pars.c
\*---------------------------------------------------------------------------------------------------------*/

