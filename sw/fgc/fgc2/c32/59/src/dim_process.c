/*---------------------------------------------------------------------------------------------------------*\
  File:         dimsProcess.c

  Purpose:      Process DIMs boards data

\*---------------------------------------------------------------------------------------------------------*/


#include <dimsProcess.h>
#include <dpcom.h>              // for dpcom global variable
#include <diag.h>               // for VerifyPsuVoltages(), qspi_misc global variable
#include <rtcom.h>              // for rtcom global variable
#include <read_PSU_adc.h>       // for function VerifyPsuVoltages()

/*---------------------------------------------------------------------------------------------------------*/
void InIrqProcessDimsDataSlice(void)
/*---------------------------------------------------------------------------------------------------------*\
  Class 59 only: The Class 59 FGCs have no DIMs connected but the DIPS board channels must still be
  processed to measure the FGC supply voltages.  This function is called from IsrMst() on ms 2-8 of
  every 10ms.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(rtcom.ms20)
    {
        case 2:

            if ( dpcom->dsp.diag.dips_are_valid )
            {
                VerifyPsuVoltages();
            }
            break;

        default: // ms 4, 5, 6, 7, 8, 14, 15, 16, 17, 18

            // ms  4 - store analog_0      from 16 DIM boards in branch A
            // ms  5 - store analog_1      from 16 DIM boards in branch A
            // ms  6 - store analog_2      from 16 DIM boards in branch A
            // ms  7 - store analog_3      from 16 DIM boards in branch A
            // ms  8 - store trigCounter   from 16 DIM boards in branch A
            // ms 14 - store analog_0      from 16 DIM boards in branch B
            // ms 15 - store analog_1      from 16 DIM boards in branch B
            // ms 16 - store analog_2      from 16 DIM boards in branch B
            // ms 17 - store analog_3      from 16 DIM boards in branch B
            // ms 18 - store trigCounter   from 16 DIM boards in branch B

            // this is in flat_qspi array format, branch A starts at 0, branch B starts at 16
            qspi_misc.analog_regs_from_all_dim_boards[rtcom.ms10 - 4][16 * (rtcom.ms20 / 10)]
               = dpcom->mcu.diag.qspi[ 1 - (rtcom.ms10 & 1)][0] >> 16;

            if ( rtcom.ms20 == 18 )                     // After last transfer (ms 18)
            {
                ValidatePresentDimBoards(16);           // Check if DIPS channels present and valid
            }
            break;

        case 3:
        case 12:
        case 13:

            break;                                      // Do nothing
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dimsProcess.c
\*---------------------------------------------------------------------------------------------------------*/

