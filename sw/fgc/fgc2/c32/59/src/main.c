/*---------------------------------------------------------------------------------------------------------*\
  File:         main.c

  Purpose:      FGC DSP RF_FGEN Class 59 Software - contains main()

  Notes:        The DSP is normally under the control of the MCU.  The DSP program is copied into SRAM
                by the MCU main program.

                The RF_FGEN system are not exposed to radiation, but are very demanding for processing
                speed because each unit generates 16 functions.  For this reason, two optimisations are
                enabled that cannot be used for the power converter classes:

                    o The stack is in the 2KB internal DSP SRAM

                    o The DSP cache is enabled

                These speed up the execution by more than 10% which is essential!
\*---------------------------------------------------------------------------------------------------------*/

#define DPRAM_CLASS_GLOBALS     // define dpcls global variable
#define DPCOM_GLOBALS           // define dpcom global variable
#define LIB_GLOBALS             // define
#define MAIN_GLOBALS            // define bg global variable
#define PROPS_CLASS_GLOBALS
#define DEBUG_GLOBALS           // define db global variable

#include <main.h>
#include <version.h>            // for version structure from version_def.txt
#include <definfo.h>            // for FGC_CLASS_ID
#include <rt.h>                 // for rt_fgfunc global variable
#include <lib.h>                // InitDsp()
#include <string.h>             // for memset()
#include <dsp_dependent.h>      // ENABLE_CACHE
#include <props_class.h>        // for property global variable
#include <dpcom.h>              // for TIFPZERO
#include <dpcls.h>              // for dpcls global variable
#include <diag.h>               // for qspi_misc, dim_list global variable
#include <defprops_dsp_fgc.h>   // for N_DSP_PROPS
#include <pars_class.h>         // for ParsRtPeriod()
#include <memmap_dsp.h>         // for DPRAM_DPCLS_32
#include <bgp.h>                // for BgpRefChange()
#include <rtcom.h>              // for rtcom global variable
#include <debug.h>              // for db structure

/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*\
  This is the main function for the FGC DSP Program.  The DSP is controlled by the MCU, and
  will be started only after the MCU has prepared the Dual Port RAM contents.  This function initialises
  the memory strobes, serial port for SPY, dev panel LEDs and global variables.  It then enables interrupts
  and enters a background processing loop in which it polls for requests from the MCU.
\*---------------------------------------------------------------------------------------------------------*/
{
    static uint32_t       i;                      // Static to save stack space

    InitDsp();

    // Set/Clear global variables (note this initialises floats to 1.0 due to TI's FP format)

    memset(&bg,          0, sizeof(bg));                // Clear BgpLoop() variables
    memset(&rt,          0, sizeof(rt));                // Clear all RT variables
    memset(&rt_state,    0, sizeof(rt_state));          // Clear all RT variables
    memset(&rt_status,   0, sizeof(rt_status));         // Clear all RT variables
    memset(&rt_control,  0, sizeof(rt_control));        // Clear all RT variables
    memset(&rt_limit,    0, sizeof(rt_limit));          // Clear all RT variables
    memset(&rt_spyfgmpx, 0, sizeof(rt_spyfgmpx));       // Clear all RT variables
    memset(&rt_fgfunc,   0, sizeof(rt_fgfunc));         // Clear all RT variables
    memset(&rt_abort,    0, sizeof(rt_abort));          // Clear all RT variables
    memset(&property,    0, sizeof(property));          // Clear property variables to integer zero
    memset(&dsp_debug,   0, sizeof(dsp_debug));         // Clear dsp_debug data structure
    memset(&qspi_misc,   0, sizeof(qspi_misc));         // Clear qspi_misc data structure
    memset(&dim_list,    0, sizeof(dim_list));          // Clear dim_list data structure

    memset(&rt.wval_raw, 0xFFC00000, sizeof(rt.wval_raw));              // Set non-value initially
    memset(&rt.fval_10,    TIFPZERO, sizeof(rt.fval_10));               // Set floating point zero
    memset(&rt.wval_17,    TIFPZERO, sizeof(rt.wval_17));               // Set floating point zero

    InitLib(N_DSP_PROPS, FGC_MAX_USER_PLUS_1, 0, 0);

    memset(&property.fgfunc.plp.acceleration,     0,   FGC_N_FGEN_CHANS);       // Set back to 1.0
    memset(&property.fgfunc.plp.linear_rate,      0,   FGC_N_FGEN_CHANS);       // Set back to 1.0
    memset(&property.fgrt.period,                 200, FGC_N_FGEN_CHANS);       // Set RT periods to 20ms
    memset(&property.fgfunc.table.function_n_els, 0,   FGC_N_FGEN_CHANS);

    // Zero all tables

    for(i = 0; i < FGC_N_FGEN_CHANS; i++)
    {
        struct point * table = TABLE_FUNCTION(i);

        memset(table, TIFPZERO, FGC_FG_TABLE_LEN * 2);
    }

    for(i=0 ; i < FGC_N_FGEN_CHANS ; i++)               // Don't use memset() for this action
    {
        dpcls->dsp.fgstate.func[i] = FGC_FG_OFF;        // Access different strobe to release MCU RAM
        TICTAC;                                         // otherwise MCU is killed by continuous DSP access
    }

    dpcom->dsp.tb_freq_hz = FGC_NO_CYC_TIMEBASE_HZ;

    ParsRtPeriod();                                     // Process the RT interpolation periods

    dpcom->dsp.version = version.unixtime;              // Report DSP version

    ENABLE_CACHE;                       // Enable DSP's tiny cache (not rad tolerant)

    // Enable interrupts

    TICTAC = 0x00000F00;                // Clear all interrupts source (int3 .. int0);
    CLEAR_EXT_INTS;                     // Clear all interrupts flag;
    ENABLE_INTS;                        // Enable global interrupts
    ENABLE_EXT_INTS;                    // Enable external interrupts (int2 only)

    dpcom->dsp.started = DSP_STARTED;   // Flag that DSP software is running

    // Start Background Processing

    BgpLoop();
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/

