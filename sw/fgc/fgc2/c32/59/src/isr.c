/*---------------------------------------------------------------------------------------------------------*\
  File:         59\src\isr.c

  Purpose:      FGC DSP Software - contains IsrMst function

  Notes:        The FGC uses only one of the DSP's four external interrupt inputs: int2.
\*---------------------------------------------------------------------------------------------------------*/

#define BGCOM_GLOBALS           // define bgcom global variable

#include <isr.h>
#include <timer30.h>            // for TIMER_ADDR()
#include <dsp_dependent.h>      // for RUNCODE()
#include <dpcom.h>              // for dpcom global variable
#include <rtcom.h>              // for rtcom global variable
#include <rt.h>                 // for RtData()
#include <dpcls.h>              // for dpcls global variable
#include <memmap_mcu.h>         // CPU_MODEH_C32IRQ_2_MASK8
#include <bgcom.h>              // for bgcom global variable
#include <ref_class.h>          // for ref_func[]
#include <dimsProcess.h>        // for InIrqProcessDimsDataSlice()

/*---------------------------------------------------------------------------------------------------------*/
void IsrMst(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the Interrupt Service Routine for the DSP int2 external interrupt request.  This will be
  triggered at the start of each millisecond (as defined by the MCU GPT OC2 clock).  It is responsible for
  all the millisecond level real-time processing of the FGC.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      trigger;
    uint32_t      n_trig_chans;
    uint32_t      chan_idx;
    uint32_t      chan_mask;
    uint32_t      report_f;                                       // Set when status is ready to report to MCU

    RUNCODE(1);                                                 // Starting interrupt

    // Acknowledge interrupt

    TIMER_ADDR(0)->gcontrol |= (HLD_|GO);                       // Restart 8MHz timer from zero.

    dpcom->dsp.cpu_usage     = rtcom.cpu_usage;                 // Return CPU usage for previous millisecond
    TICTAC                   = CPU_MODE_C32IRQ_2_MASK16;        // Acknowledge MCU interrupt
    bgcom.run_f              = true;                            // Signal to background proc to run after ISR

    dpcom->dsp.ms_irq_alive_counter.int32u++;

    report_f = false;                                           // No report by default

    /*----- Prepare time related Spy channels for previous millisecond -----*/

    dpcom->dsp.cpu_usage = (uint32_t)rt.fspy_0[1];                // Send DSP CPU usage for previous millisecond
    rt.fspy_0[2] = (FP32)dpcom->mcu.mst_time * 0.05;            // Convert MCU MstTsk time to percent
    rt.fspy_0[3] = (FP32)rtcom.ms20;
    rt.fspy_0[4] = (FP32)rtcom.time_now_ms.ms_time;
    rt.fspy_0[5] = (FP32)bgcom.idle_counter;
    rt.fspy_0[6] = (FP32)dpcom->mcu.pll.period;
    rt.fspy_0[7] = (FP32)dpcom->mcu.pll.integrator;
    rt.fspy_0[8] = (FP32)dpcom->mcu.pll.error;

    bgcom.idle_counter = 0;

    /*----- Get STATE.OP from dpcom into DSP rtcom variable -----*/

    rtcom.state_op = dpcom->mcu.state_op;

    /*----- Calculate time boundary flags -----*/

    rtcom.time_now_ms = dpcom->mcu.time.now_ms;                         // Get time now from MCU via DPRAM

    AbsTimeCopyFromMsToUs(&rtcom.time_now_ms, &rtcom.time_now_us);

    rtcom.ms10        = dpcom->mcu.time.ms_mod_10;                      // Used by InIrqProcessDimsDataSlice()
    rtcom.ms200       = dpcom->mcu.time.ms_mod_200;                     // Used by InIrqProcessDimsDataSlice()
    rtcom.ms20_f      = !(rtcom.ms20 = dpcom->mcu.time.ms_mod_20);      // Get start-of-20ms-period flag

    /*----- Process RT data and events on different milliseconds -----*/

    if(rtcom.ms20_f)            // Ms 0 of 20
    {
        RtData();                       // Process RT Data
    }
    else                        // Ms 1-19 of 20
    {
        RtEvents();                     // Process event data
    }

    /*----- Loop for each FGEN channel -----*/

    trigger      = 0;
    n_trig_chans = 0;
    rt_status.prev_not_ack = rt_status.not_ack;

    for(chan_idx=0,chan_mask=1;chan_idx <  FGC_N_FGEN_CHANS;chan_idx++,chan_mask<<=1)
    {
        if(!dpcls->mcu.rfc_reset)               // If RFC can be triggered on the next millisecond
        {
            if(rt.chk_stat_f[1][chan_idx])              // If status is available to be checked
            {
                RtStatusAndStats(chan_idx,chan_mask);           // Process channel status and statistics
            }

            // Report status on 20ms boundaries

            report_f = rtcom.ms20_f;

            rt.chk_stat_f[1][chan_idx] = rt.chk_stat_f[0][chan_idx];

            if ( rt.trigger & chan_mask )                       // If channel is enabled
            {
                RtStates (chan_idx,chan_mask);                  // Manage channel state machine
                RtControl(chan_idx,chan_mask);                  // Apply real-time control if active

                rt.last_fval_10[chan_idx] = rt.fval_10[chan_idx];                 // Keep last value for abort

                rt.fval_10[chan_idx] = ref_func[rt.ref_type[chan_idx]](chan_idx); // Calculate function

                RtChannel(chan_idx,chan_mask);                  // Calculate channel value

                rt.chk_stat_f[0][chan_idx] = true;              // Enable status check in 2 ms time
                trigger |= chan_mask;                           // Set trigger bit (to be sent to MCU)
                n_trig_chans++;                                 // Count number of chans to be triggered
            }
            else                                        // else channel is disabled
            {
                rt.chk_stat_f[0][chan_idx] = false;             // Disable status check in 2 ms time
            }
        }
        else                                    // else RFC reset is in progress
        {
            rt.chk_stat_f[0][chan_idx] = false;         // Suppress status checking when operation resumes
            rt.chk_stat_f[1][chan_idx] = false;
        }
    }

    dpcls->dsp.trigger      = trigger;          // Send trigger to MCU
    dpcls->dsp.n_trig_chans = n_trig_chans;     // Send number of triggered channels to MCU

    /*----- Report to MCU on ms 0 of 20 unless a reset is in progress -----*/

    if(report_f)
    {
        RtReport20ms();
    }

    /*----- Write to SPY interface ---*/

    RtSpySend();

    /*----- Visual operational check via development panel LED0 -----*/

    if(!(++rtcom.led_counter & 0x00FF))                         // ~0.5 Hz
    {
        LED0_TOGGLE;
    }

    /*----- Receive and Process Diagnostic Data from MCU om ms 2-8 of 10 -----*/

    if(rtcom.ms10 > 1 && rtcom.ms10 < 9)
    {
        InIrqProcessDimsDataSlice();
    }

    /*----- Calculate ISR's CPU usage -----*/

    RUNCODE(2);                                                 // Returning to background processing
    rt.fspy_0[1] = 0.0125 * (FP32)(TIMER_ADDR(0)->counter);     // Calculate RT ISR cpu usage in percent
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrTrap(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used as the target for all unexpected interrupts
\*---------------------------------------------------------------------------------------------------------*/
{
    RUNCODE(0xDEAD);                            // Returning to background processing

    for(;;);                                    // Loop forever and wait for watchdog reset
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: 59\src\isr.c
\*---------------------------------------------------------------------------------------------------------*/

