/*---------------------------------------------------------------------------------------------------------*\
  File:         bgp.c

  Purpose:      FGC DSP Software - contains Background Processing Functions

  Notes:        The DSP code does not run under an operating system.  There is just the background
                processing level, and interrupt level.  The background processing is used for non-real-time
                operations.  These background functions are used to process reference change requests
                from the MCU.
\*---------------------------------------------------------------------------------------------------------*/

#define RTCOM_GLOBALS           // define rtcom global variable
#define BGP_GLOBALS

#include <bgp.h>
#include <props_class.h>        // for property global variable
#include <rt.h>                 // for rt_fgfunc global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>
#include <fgc_errs.h>           // for FGC_OK_NO_RSP, FGC_BAD_PARAMETER
#include <main.h>               // for FGC_NO_CYC_TIMEBASE_HZ, bg global variable
#include <ref_class.h>          // for plp global variable
#include <dsp_dependent.h>      // for DISABLE_INTS, LEDS_INIT, LED0_TOGGLE, LED1_TOGGLE
#include <rtcom.h>              // for rtcom global variable
#include <math.h>               // for sqrt()
#include <bgcom.h>
#include <pars_class.h>
#include <lib.h>
#include <mcu_dsp_common.h>

/*---------------------------------------------------------------------------------------------------------*/
void BgpLoop(void)
/*---------------------------------------------------------------------------------------------------------*\
  This is the Background Processing Loop, common to FGC2/FGC3.
  It calls BgpPlatform() for platform specific processing.
\*---------------------------------------------------------------------------------------------------------*/
{
    for(;;)
    {
        dpcom->dsp.mcu_counter_dsp_alive.int32u = 0; // reset the counter, incremented at MCU MstTsk()

        // Check for change of reference from MCU

        if ( dpcls->mcu.ref.change_f )                          // If reference change requested
        {
            BgpRefChange(dpcls->mcu.ref.chan_idx);              // Process reference change request
        }

        // Process parameter group sets

        if ( rtcom.par_group_set_mask )
        {
            ParsGroupSet();
        }

        // Check for property communications and parameter parsing

        bgp_no_pending_action = BgpPropComms();

        // On other classes, we would call BgpPlatform() here.


        // Flash front panel LED to indicate that IsrMst interrupt is running
        // The DSP will stay in that IDLE loop until the next periodic ISR.

        while(bgp_no_pending_action && !bgcom.run_f)        // Wait for IsrMst() to set flag
        {
            bgcom.i = TICTAC;                                       // Read TICTAC to trigger memory refresh
            bgcom.idle_counter++;                                   // Counter for Spy to trace BG CPU usage

            if(!bgcom.led_counter)                                  // If downcounter reaches zero
            {
                LED1_TOGGLE;                                        // Toggle LED1
                bgcom.led_counter     = 0x20000;                    // Reset counter
                dpcom->dsp.mcu_counter_dsp_alive.int32u = 0;        // signal MCU that we are alive
            }
            else
            {
                --bgcom.led_counter;
                if ( !(bgcom.led_counter & ~0x7F) )                 // Once per 128 iterations
                {
                    DebugMemTransfer();                             // Transfer debug memory zone to MCU
                }
            }
        }

        bgcom.run_f = false;
        dpcom->dsp.mcu_counter_dsp_alive.int32u = 0;                // signal MCU that we are alive
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void BgpRefChange(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main() if the MCU has set the ref change flag to request that a new
  function be processed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      ref_type;
    uint32_t      abort_period;
    FP32        half_period_ms;
    FP32        inv_period_ms;

    /*--- Prepare to process the new function ---*/

    property.fgfunc.err_idx     [chan_idx]              = 0;            // Clear segment error index
    property.fgfunc.run         [chan_idx].unix_time    = 0;            // Clear run time property
    property.fgfunc.run         [chan_idx].us_time      = 0;
    rt_fgfunc.run               [chan_idx].unix_time    = TIME_NEVER;   // Clear RT run time
    rt_fgfunc.run               [chan_idx].us_time      = 0;
    property.fgfunc.abort       [chan_idx].unix_time    = 0;            // Clear abort time property
    property.fgfunc.abort       [chan_idx].us_time      = 0;
    rt_fgfunc.abort             [chan_idx].unix_time    = TIME_NEVER;   // Clear RT abort time
    rt_fgfunc.abort             [chan_idx].us_time      = 0;
    dpcls->dsp.fgfunc.run_period[chan_idx]              = 0;            // Clear function run period

    /*--- Check state of channel ---*/

    if(dpcls->dsp.fgstate.func[chan_idx] != FGC_FG_IDLE)        // If channel not IDLE
    {
        dpcom->dsp.errnum = FGC_BAD_STATE;                              // Report BAD STATE
    }
    else                                                        // else try to arm the channel
    {
        ref_type = dpcls->mcu.ref.type[chan_idx];                       // Get type of function

        switch(ref_type)
        {
        case FGC_REF_NOW:               // NOW (PLP) function

            rt_fgfunc.run[chan_idx].unix_time = rtcom.time_now_us.unix_time + 1;// Set run time at least 1s in the future,
            rt_fgfunc.run[chan_idx].us_time   = 999000;                         // rounded on a 1s boundary, -1 ms.

            property.fgfunc.run[chan_idx] = rt_fgfunc.run[chan_idx];            // Copy the runtime to the FGFUNC.RUN property
            AbsTimeUsAddDelay(&property.fgfunc.run[chan_idx], 1000);            // +1 ms to get a rounded value.

            ref_type = FGC_REF_PLP;
                                                                            // Fall through to PLP
        case FGC_REF_PLP:               // PLP/NOW function

            dpcom->dsp.errnum = BgpInitPlp(chan_idx);
            break;

        case FGC_REF_SINE:              // SINE function

            dpcom->dsp.errnum = BgpInitSine(chan_idx);
            break;

        case FGC_REF_CTRIM:             // CTRIM function

            dpcom->dsp.errnum = BgpInitCTrim(chan_idx);
            break;

        case FGC_REF_TABLE:             // TABLE function

            dpcom->dsp.errnum = BgpInitTable(chan_idx);
            break;
        }

        abort_period   = 10 * FGC_NO_CYC_TIMEBASE_HZ;                       // 10s
        half_period_ms = 0.05 * (FP32)abort_period;
        inv_period_ms  = 0.10 / (FP32)abort_period;
    }

    if(!dpcom->dsp.errnum)                                      // If S FGFUNC succeeded
    {
        DISABLE_INTS;
        bg.ref_type[chan_idx] = ref_type;                               // Remember type of function
        rt.ref_type[chan_idx] = FGC_REF_ARMED;                          // Arm the function
        rt.ref_time_tb[chan_idx] = 0;                                   // Reset reference time
        rt_abort.d[chan_idx]  = 0.0;                                    // Prepare for an Abort

        rt_abort.period[chan_idx]         = abort_period;
        rt_abort.half_period_ms[chan_idx] = half_period_ms;
        rt_abort.inv_period_ms[chan_idx]  = inv_period_ms;

        dpcls->mcu.ref.type[chan_idx]     = ref_type;                   // Replace NOW with PLP
        dpcls->mcu.ref.type_old[chan_idx] = ref_type;                   // Remember new ref type
        ENABLE_INTS;
    }

    dpcls->mcu.ref.change_f  = false;                           // Report to MCU that processing completed
    dpcom->dsp.bg_complete_f = true;
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t BgpInitPlp(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to prepare the PLP parameters when a PLP or NOW functions have been specified
  by the user.  The input parameters are:

        rt.fval_10[chan_idx]                            Initial function value
        property.fgfunc.run_delay[chan_idx]             Time before parabolic acceleration (100us units)
        property.fgfunc.plp.final[chan_idx]             Final function value
        property.fgfunc.plp.acceleration[chan_idx]      Parabolic acceleration/deceleration
        property.fgfunc.plp.linear_rate[chan_idx]       Linear ramp rate

  The output parameters are:

        plp.r[][chan_idx]                               Normalised end of segment reference values
        plp.t[][chan_idx]                               End of segment times (100us)
        plp.a[chan_idx]                                 Parabolic acceleration/deceleration
        plp.l[chan_idx]                                 Linear rate of change of reference
        dpcls->dsp.fgfunc.run_period[chan_idx]          Total time for PLP function (100us units)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      i;                      // Loop variable
    uint32_t      stat;                   // Limits status
    FP32        t;                      // Segment time accummulator
    FP32        dt1pp;                  // Time until parabolic section
    FP32        dt1pl;                  // Time until linear section
    FP32        dr;                     // change in ref value
    FP32        dt[3];                  // Segment durations (s)

    // Check change in function

    plp.r[0][chan_idx] = rt.fval_10[chan_idx];                          // Initial value
    plp.r[3][chan_idx] = property.fgfunc.plp.final[chan_idx];           // Final value

    dr = plp.r[3][chan_idx] - plp.r[0][chan_idx];                       // Function change

    if((stat = BgpCheckChannelLimits(chan_idx,plp.r[3][chan_idx])))     // If final value is out of limits
    {
        property.fgfunc.err_idx[chan_idx] = 2;
        return(stat);                                                           // Report error
    }

    /*--- Adjust parameters if PLP is descending ---*/

    if(dr > 0.0)                        // Ascending PLP
    {
        plp.a[chan_idx] = property.fgfunc.plp.acceleration[chan_idx];           // Acceleration    (+ve)
        plp.l[chan_idx] = property.fgfunc.plp.linear_rate[chan_idx];            // Max linear rate (+ve)
    }
    else                                // Descending PLP
    {
        plp.a[chan_idx] = -property.fgfunc.plp.acceleration[chan_idx];          // Acceleration    (-ve)
        plp.l[chan_idx] = -property.fgfunc.plp.linear_rate[chan_idx];           // Max linear rate (-ve)
    }

    // Check for minimum function change

    dt1pp = sqrt(dr / plp.a[chan_idx]);                 // Time to reach end of parabola (P-P)
    dt1pl = plp.l[chan_idx] / plp.a[chan_idx];          // Time to reach linear section  (P-L-P)

    /*--- Case 1 : P-P ---*/

    if(dt1pp <= dt1pl)
    {
        dt[0] = dt[2] = dt1pp;
        dt[1] = 0.0;
        plp.r[1][chan_idx] = plp.r[2][chan_idx] = 0.5 * (plp.r[0][chan_idx] + plp.r[3][chan_idx]);
    }

    /*--- Case 2 : P-L-P ---*/

    else
    {
        dr = 0.5 * plp.a[chan_idx] * dt1pl * dt1pl;
        plp.r[1][chan_idx] = plp.r[0][chan_idx] + dr;
        plp.r[2][chan_idx] = plp.r[3][chan_idx] - dr;
        dt[0] = dt[2] = dt1pl;
        dt[1] = (plp.r[2][chan_idx] - plp.r[1][chan_idx]) / plp.l[chan_idx];
    }

    /*--- Calculate Segment times in 100us units and absolute segment references ---*/

    plp.t[0][chan_idx] = property.fgfunc.run_delay[chan_idx];

    for(i=1,t=0.0;i < 4;i++)
    {
        t += dt[i-1];
        plp.t[i][chan_idx] = FG_CONV_S_TO_TB(t) + property.fgfunc.run_delay[chan_idx];
    }

    /*--- Check rate of change limits ---*/

    if((stat = BgpCheckRateLimits(chan_idx,plp.a[chan_idx] * dt[0])))
    {
        property.fgfunc.err_idx[chan_idx] = 1;
        return(stat);
    }

    dpcls->dsp.fgfunc.run_period[chan_idx] = plp.t[3][chan_idx];

    return(FGC_OK_NO_RSP);                      // Report success always
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t BgpInitCTrim(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to prepare the Trim Function parameters.  The input parameters are:

        rt.fval_10[chan_idx]                            Initial function value
        property.fgfunc.run_delay[chan_idx]             Time before start of CTrim function (100us units)
        property.fgfunc.ctrim.final[chan_idx]           Final function value
        property.fgfunc.ctrim.period[chan_idx]          CTrim period (100us units)

  The output parameters are:

        ctrim.tstart[chan_idx]                          Start time (100us units)
        ctrim.tend[chan_idx]                            End time (100us units)
        ctrim.t0[chan_idx]                              Timebase offset (100us units)
        ctrim.rend[chan_idx]                            Final function value
        ctrim.r0[chan_idx]                              Function offset
        ctrim.a[chan_idx]                               Cubic factor r = t(at^2 + c)
        ctrim.c[chan_idx]                               Cubic factor (max rate of change)
        dpcls->dsp.fgfunc.run_period[chan_idx]          Total time for PLP function (100us units)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      stat;                   // Status from limits checking
    uint32_t      period_tb;              // Trim period (100us)
    FP32        period_s;               // Trim peirod (s)
    FP32        dr;                     // Trim range

    /*--- Get parameters from MCU ---*/

    ctrim.tstart[chan_idx] = property.fgfunc.run_delay[chan_idx];
    ctrim.rend[chan_idx]   = property.fgfunc.ctrim.final[chan_idx];
    period_tb = property.fgfunc.ctrim.period[chan_idx];

    dr = ctrim.rend[chan_idx] - rt.fval_10[chan_idx];

    if((stat = BgpCheckChannelLimits(chan_idx,ctrim.rend[chan_idx]))) // If final value is out of limits
    {
        return(stat);                                                           // Report error
    }

    /*--- Calculate or check period and complete cubic factors ---*/

    if(period_tb < 50)                                  // If period is less than 5ms
    {
        ctrim.c[chan_idx] = (dr > 0 ? property.fgconf.limit.rate.pos[chan_idx] :  // Set max rate of change
                                        property.fgconf.limit.rate.neg[chan_idx]);
        period_s  = 1.5 * dr / ctrim.c[chan_idx];                                   // Calculate period
        period_tb = FG_CONV_S_TO_TB(period_s);
        property.fgfunc.ctrim.period[chan_idx] = period_tb;         // Set period property to calculated value
    }
    else
    {
        period_s = FG_CONV_TB_TO_S(period_tb);
        ctrim.c[chan_idx] = 1.5 * dr / period_s;

        if((stat = BgpCheckRateLimits(chan_idx,ctrim.c[chan_idx])))   // If max rate is out of limits
        {
            return(stat);                                                         // Report error
        }
    }

    ctrim.a[chan_idx] = -2.0 * dr / (period_s * period_s * period_s);

    /*--- Calculate range and offsets ---*/

    ctrim.tend[chan_idx] = ctrim.tstart[chan_idx] + period_tb;
    ctrim.t0[chan_idx]   = 0.5 * period_s;
    ctrim.r0[chan_idx]   = 0.5 * (rt.fval_10[chan_idx] + ctrim.rend[chan_idx]);

    dpcls->dsp.fgfunc.run_period[chan_idx] = ctrim.tend[chan_idx];

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t BgpInitTable(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to prepare the Table Function parameters.  The input parameters are:

        rt.fval_10[chan_idx]                            Initial function value
        property.fgfunc.run_delay[chan_idx]             Time before start of table function (100us units)
        TABLE_VALUE(chan_idx)                           Refence vector
        TABLE_TIME(chan_idx)                            Time vector (100us units)

  The output parameters are:

        tbl.run_delay [chan_idx]                        Run delay (100us)
        tbl.tbl_time  [chan_idx]                        Pointer to start of time array
        tbl.tbl_value [chan_idx]                        Pointer to start of value array
        tbl.end_value [chan_idx]                        Initialised to rt.fval_10[chan_idx]
        tbl.end_time  [chan_idx]                        Initialised to run delay
        tbl.grad      [chan_idx]                        Initialised to zero
        tbl.idx       [chan_idx]                        Initialised to zero
        dpcls->dsp.fgfunc.run_period[chan_idx]          Total time for table function (100us units)
\*---------------------------------------------------------------------------------------------------------*/
{
    struct FG_point * function = TABLE_FUNCTION(chan_idx);

    uint32_t status;    // Limit checking status
    uint32_t i;         // Loop variable
    FP32   dref;      // Segment reference change
    FP32   grad;      // Segment gradient

    // Check that table have at least 2 points

    tbl.nels[chan_idx] = property.fgfunc.table.function_n_els[chan_idx];

    if(tbl.nels[chan_idx] < 2)
    {
        return(FGC_BAD_ARRAY_LEN);
    }

    // Assure that time of first point is 0

    if(function[0].time != 0.0)
    {
        return(FGC_INVALID_TIME);
    }

    // Assure that first reference value is close to current value

    dref = function[0].ref - rt.fval_10[chan_idx];

    if(fabs(dref) * property.fgconf.gain[chan_idx] > 1.0)
    {
        return(FGC_REF_MISMATCH);
    }

    // Prepare first segment using run delay

    tbl.run_delay[chan_idx] = FG_CONV_TB_TO_S(property.fgfunc.run_delay[chan_idx]);
    tbl.function [chan_idx] = function;
    tbl.end_value[chan_idx] = rt.fval_10[chan_idx];
    tbl.end_time [chan_idx] = tbl.run_delay[chan_idx];
    tbl.grad     [chan_idx] = 0.0;
    tbl.idx      [chan_idx] = 0;

    // Check that all segments are within limits

    for(i = 1; i < tbl.nels[chan_idx]; i++)
    {
        property.fgfunc.err_idx[chan_idx] = i;

        // Report error if point is out of channel limits

        status = BgpCheckChannelLimits(chan_idx, function[i].ref);

        if(status != FGC_OK_NO_RSP)
        {
            return(status);
        }

        // Don't allow for point spacing to be less than 1ms
        // We need to allow for some floating point arithmetics errors

        if(function[i].time < (function[i - 1].time + 0.0009))
        {
            return(FGC_INVALID_TIME);
        }

        // Check gradient

        grad = (function[i].ref - function[i - 1].ref) / (function[i].time - function[i - 1].time);

        status = BgpCheckRateLimits(chan_idx, grad);

        if(status != FGC_OK_NO_RSP)
        {
            return(status);
        }
    }

    dpcls->dsp.fgfunc.run_period[chan_idx] = FG_CONV_S_TO_TB(function[i - 1].time + tbl.run_delay[chan_idx]);
    property.fgfunc.err_idx[chan_idx]      = 0;

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t BgpInitSine(uint32_t chan_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to prepare the Test Function parameters.  The input parameters are:

        rt.fval_10[chan_idx]                            Initial function value
        property.fgfunc.run_delay[chan_idx]             Time before start of test function (100us units)
        property.fgfunc.sine.amplitude[chan_idx]        Ref p-p amplitude (A or V)
        property.fgfunc.sine.num_cycles[chan_idx]       Number of cycles/steps (integer)
        property.fgfunc.sine.period[chan_idx]           Period (100us units)

  The output parameters are:

        sine.freq_rad[chan_idx]                         2.PI/period_100us
        sine.run_delay[chan_idx]                        Run delay (100us units)
        sine.end_time[chan_idx]                         Total time for ref change (100us units)
        sine.ref0[chan_idx]                             Initial reference (A or V)
        sine.ref_amp[chan_idx]                          Reference amplitude
        dpcls->dsp.fgfunc.run_period[chan_idx]          Total function time (100us units)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      stat;                   // Limit checking status
    uint32_t      n_cyc;                  // int(num_cycles)
    FP32        period;                 // Period (s)
    FP32        duration;               // Duration (s)
    FP32        max_drdt;               // Max rate of change

    n_cyc     = (uint32_t)(property.fgfunc.sine.num_cycles[chan_idx] + 0.4999);
    period    = FG_CONV_TB_TO_S(property.fgfunc.sine.period[chan_idx]);
    duration  = (FP32)n_cyc * period;

    sine.period   [chan_idx] = property.fgfunc.sine.period[chan_idx];
    sine.run_delay[chan_idx] = property.fgfunc.run_delay[chan_idx];
    sine.freq_rad [chan_idx] = TWO_PI / (period * FGC_NO_CYC_TIMEBASE_HZ);
    sine.ref_amp  [chan_idx] = property.fgfunc.sine.amplitude[chan_idx];

    /*--- Check parameters ---*/

    if(duration > 4.0E5)                        // If total time is too long or
    {
        return(FGC_INVALID_TIME);                               // Report INVALID TIME
    }

    /*--- Calculate amplitude related parameters ---*/

    sine.ref0[chan_idx]     = rt.fval_10[chan_idx];     // Set initial reference to actual function reference
    sine.ref_amp[chan_idx] *= 0.5;                      // Convert amp to 1/2 peak-peak

    /*--- Check limits (assuming no RT control) ---*/

    max_drdt = sine.ref_amp[chan_idx] * TWO_PI / period;

    if((stat = BgpCheckChannelLimits(chan_idx,sine.ref0[chan_idx] + sine.ref_amp[chan_idx])) ||
       (stat = BgpCheckChannelLimits(chan_idx,sine.ref0[chan_idx] - sine.ref_amp[chan_idx])) ||
       (stat = BgpCheckRateLimits   (chan_idx,max_drdt)) ||
       (stat = BgpCheckRateLimits   (chan_idx,-max_drdt)))
    {
        return(stat);
    }

    /*--- Transfer end time (in 100us units) to MCU via DPRAM ---*/

    dpcls->dsp.fgfunc.run_period[chan_idx] =
    sine.end_time[chan_idx] = sine.run_delay[chan_idx] + FG_CONV_S_TO_TB(duration);

    return(FGC_OK_NO_RSP);                              // Report success
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t BgpCheckChannelLimits(uint32_t chan_idx, FP32 r)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to check the function value against the limits for the channel.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(r > property.fgconf.limit.chan.max[chan_idx] ||
       r < property.fgconf.limit.chan.min[chan_idx])
    {
        return(FGC_OUT_OF_LIMITS);
    }

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint32_t BgpCheckRateLimits(uint32_t chan_idx, FP32 drdt)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to check the function rate of change against the rate of change limits for the
  channel.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(drdt > property.fgconf.limit.rate.pos[chan_idx] ||
       drdt < property.fgconf.limit.rate.neg[chan_idx])
    {
        return(FGC_OUT_OF_RATE_LIMITS);
    }

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: 59\src\bgp.c
\*---------------------------------------------------------------------------------------------------------*/

