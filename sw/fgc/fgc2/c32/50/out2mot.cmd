/*----------------------------------------------------------------------------*\
 File:          50/out2mot.cmd

 Purpose:       Class 50 FGC2_LHC_PC DSP Software HEX30 drive file

 Notes:         This drive file will generate S-Records from the COFF file
\*----------------------------------------------------------------------------*/

/* Memory Definition */

ROMS
{
    EPROM:	org = 0000H, length = 1400H
}

/* End of file: 50/out2mot.cmd */

