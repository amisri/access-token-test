/*------------------------------------------------------------------------------*\
 File:		link.cmd

 Purpose:	fgc2_BootTest Software Linker file

 Notes:		The program is built with the register parameter passing model

		The program is processed by hex30 using the mot.cmd drive file.
		This specifies the size of the zone that can receive the program
		and must be big enough otherwise the program will be incomplete
		and will crash.  If the length of the CODE segment is changed
		then you must change mot.cmd.  Be careful of units: long words
		in this file and bytes in mot.cmd.
\*------------------------------------------------------------------------------*/

/* Memory Map Definition */

MEMORY
{
    VECS:	org = 000000H	len = 00040H	/* Reset and Interrupt Vectors	*/
    CODE:	org = 000040H	len = 004C0H	/* Program Code and init data	*/
    STACK:	org = 000500H	len = 00200H	/* Stack		 	*/
    VARS:	org = 000700H	len = 00100H	/* Program variables		*/
}

/* Section Allocation */

SECTIONS
{
     vecs:	> VECS				/* Vector table (vecs.asm)	*/
    .cinit:	> CODE				/* C Initialisation data	*/
    .data:	> CODE				/* Asm data			*/
    .const:	> CODE				/* FP constants and switch tbls */
    .text:	> CODE				/* Program code			*/
    .stack:	> STACK				/* Program stack		*/
    .bss:	> VARS				/* Global and Static variables	*/
}

/* End of file: link.cmd */

