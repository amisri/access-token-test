/*---------------------------------------------------------------------------------------------------------*\
  File:         main.c

  Purpose:      fgc2_BootTest - FGC C32 test program used by fgc2_BootProg - contains main()

  Author:       Quentin.King@cern.ch

  Notes:        This small test program is used by the HC16 boot program to test:

                        - the C32 is running
                        - the C32 memory (patterns and address)
                        - the HC16->C32 interrupts
                        - the Spy/FIFO interface lines
                        - the PBL link (uses the Spy/Fifo interface)

                The memory map is defined in prog.prm:

                    0x00000-0x0003F     Vectors
                    0x00040-0x004FF     Program code
                    0x00500-0x006FF     Stack
                    0x00700-0x007FF     Variables
                    0x00800-0x1FFFF     Memory test area (patterns and address)

                If extra functions or variables are added, take care to check and possibly
                resize the CODE and VARS segments in link.cmd, and the length in mot.cmd which
                must be the size (in bytes) of the VECS and CODE segments combined.

                The link between the HC16 Boot and this C32 Boot test program is via the top
                2KB of the DPRAM zone of the HC16 memory (0xE800-0xE807 in HC16 memory space):
                       C32       HC16
                    0x903A00    0xE800    C32 status - reported by C32
                    0x903A01    0xE804    Serial port control (see page 8-19) - set by HC16
                    0x903A02    0xE808    HC16 SRAM Single Bit Error counter (from EDAC)
                    0x903A03    0xE80C    C32 SRAM Single Bit Error counter (from EDAC)
                    0x903A04    0xE810    C32 internal SRAM Single Bit Error counter
                    0x903A05    0xE814    C32 internal registers Single Bit Error counter
                    0x903A06    0xE818    C32 internal registers failed counter
                    0x903A07    0xE81C    C32 internal register failed index
                    0x903A10    0xE840    Data sent to PBL card via serial link (loopback checked by HC16)

                C32 status:

                        Bit 4  - HC16 single bit errors detected
                        Bit 5  - C32 single bit errors detected
                        Bit 6  - Pattern test failed
                        Bit 7  - Address test failed
                        Bit 24 - Set to indicate activity of background loop

                Serial Port control:

                        Bits 0-11 : Serial port control
                        Bit 15    : 0=Direct serial port control, 1=PBL link test

  History:

    04/01/00    qak     Created
    14/04/00    qak     Copy version string to dual port ram
    29/08/00    qak     ParsCheckChangeMask() now called during startup to init par groups
    20/08/01    qak     Ported to support V1.0 and V1.1 hardware
    17/01/03    qak     Ported to support V2.2
    30/06/04    qak     FIFO output not static only
    18/03/05    qak     Read of TICTAC added to let poller operate to detect single bit errors
    31/03/05    qak     DPRAM zone moved to 0xF800 to avoid code block buffers
    07/07/06    qak     Moved DPRAM zone to 0xE800 to avoid temperature buffers
    07/12/06    qak     Status bits moved from bits 0-3 to bits 4-7 to avoid TICTAC bits in boot
    11/04/08    qak     Program now returns to HC16 the SBE counters for HC16/C32 SRAM
    09/07/08    sdg     Rad test functions added for internal ram and registers
    12/08/08    qak     Add support to test the SimConv link
    29/08/08    qak     SimConv link data written to DPRAM so that it can be checked by the HC16
    28/01/09    qak     AccessToHC16() removed from the loop to prevent HC16 watchdog problem
    02/04/09    sdg     program now returns the internal register failed index
    30/11/09    qak     Ten words sent to PBL card (no longer works with SimConv
\*---------------------------------------------------------------------------------------------------------*/

#include <string.h>             // Contains memset
#include <bus32.h>              // C32 Memory bus control declarations
#include <serprt30.h>           // C32 Serial Port control declarations
#include <timer30.h>            // C32 Timers control declarations
#include <dma32.h>              // C32 Timers control declarations

/*----- Global macro definitions -----*/

#define ENABLE_INTS             asm(" OR  06000h,ST")
#define DISABLE_INTS            asm(" AND 0DFFFh,ST")
#define CLEAR_EXT_INTS          asm(" ANDN 0Fh,IF")
#define ENABLE_EXT_INTS         asm(" LDI 0Fh,IE")
#define LEDS_INIT               asm(" OR 0022h,IOF")
#define LED0_TOGGLE             asm(" XOR 0004h,IOF")
#define LED1_TOGGLE             asm(" XOR 0040h,IOF")
#define SPY(out)                SERIAL_PORT_ADDR(0)->x_data=(unsigned)(out)
#define WAITSPY                 while(!((SERIAL_PORT_ADDR(0)->gcontrol & (XSREMPTY|XRDY)) == XRDY))
#define TICTAC                  *((volatile unsigned int *)0x810000)
#define DPRAM                   ((struct dpram*)0x903A00)               // 0xE800 in HC16 page
#define CLEAR_EXT_INT0          asm(" ANDN  1h,IF")
#define CLEAR_EXT_INT1          asm(" ANDN  2h,IF")
#define CLEAR_EXT_INT2          asm(" ANDN  4h,IF")
#define CLEAR_EXT_INT3          asm(" ANDN  8h,IF")
#define READ_HC16_RAM           asm(" LDIU  *AR0,R0")
#define FIFO_TEST_LINK          0x00001000
#define H4IRRAD_DAC_TEST        0x00002000
#define TICTAC_INT0             0x00000100
#define TICTAC_INT1             0x00000200
#define TICTAC_INT2             0x00000400
#define TICTAC_INT3             0x00000800
#define STATUS_HC16_SBE         0x00000010
#define STATUS_C32_SBE          0x00000020
#define STATUS_PATTERN_FAULT    0x00000040
#define STATUS_ADDRESS_FAULT    0x00000080
#define STATUS_ALIVE            0x01000000
#define MEM_START               0x00000800                              // Allow 16KB for program
#define MEM_END                 0x00020000                              // End of page 8 (512KB)
#define INTERNAL_RAM_START      0x0087FE00                              // Start Address of the internal RAM
#define INTERNAL_RAM_LEN        0x00000200                              // Length in long words
#define SERIAL_DATA_LEN         0x0000000A                              // Number of words to send

/*----- function declarations -----*/

#pragma INTERRUPT (Isr0);
#pragma INTERRUPT (Isr1);
#pragma INTERRUPT (Isr2);
#pragma INTERRUPT (Isr3);

void    main                    (void);
void    MemPatternTest          (unsigned pattern);
void    MemAddressTest          (void);
void    CheckEdacSbe            (void);
void    AccessToHC16            (void);
void    Isr0                    (void);
void    Isr1                    (void);
void    Isr2                    (void);
void    Isr3                    (void);
void    InternalRamTest         (void);
void    InternalRegTest         (void);
void    SerialPort              (void);

unsigned int ToIEEE(double ti_double);          // From conv.asm

/*----- global variables -----*/

struct dpram
{
    unsigned    status;
    unsigned    serial_cmd;
    unsigned    hc16_sram_sbe_counter;
    unsigned    c32_sram_sbe_counter;
    unsigned    internal_ram_sbe_counter;
    unsigned    internal_reg_sbe_counter;
    unsigned    internal_reg_failed_counter;
    unsigned    internal_reg_failed_idx;
    unsigned    pad[8];
    unsigned    serial_data[SERIAL_DATA_LEN];   // 0x903A10 0xE840 Data sent to PBL card via serial link
};

unsigned        isr_counter;
unsigned        tictac;
unsigned        sbe_hc16_last;
unsigned        sbe_c32_last;
unsigned        serial_cmd_last;
unsigned        time_ms;
unsigned        time_s;
unsigned        dacNbStep;
unsigned        dacStepDuration;
unsigned        step;
unsigned        dacClkFreq;

struct c32_reg
{
    unsigned    addr_c32;               // 24 bit address of register
    unsigned    value;                  // value in the register
    unsigned    state;                  // Status of the register. if broken during the test it's equal to 1
};

static struct    c32_reg        internal_regs[]
= {
        { 0x00808000,   0x00000000,     0 },    // DMA :   DMA0 GCONTROL
        { 0x00808004,   0x00000000,     0 },    // DMA :   DMA0 SOURCE
        { 0x00808006,   0x00000000,     0 },    // DMA :   DMA0 DESTINATION
        { 0x00808008,   0x00000000,     0 },    // DMA :   DMA0 TRANSFER_COUNTER
        { 0x00808010,   0x00000000,     0 },    // DMA :   DMA1 GCONTROL
        { 0x00808014,   0x00000000,     0 },    // DMA :   DMA1 SOURCE
        { 0x00808016,   0x00000000,     0 },    // DMA :   DMA1 DESTINATION
        { 0x00808018,   0x00000000,     0 },    // DMA :   DMA1 TRANSFER_COUNTER
        { 0x00808020,   0x00000081,     0 },    // TIMER:  TIMER0 GCONTROL
        { 0x00808028,   0xFFFFFFFF,     0 },    // TIMER:  TIMER0 PERIOD
        { 0x00808030,   0x00000081,     0 },    // TIMER:  TIMER1 GCONTROL
        { 0x00808038,   0xFFFFFFFF,     0 },    // TIMER:  TIMER1 PERIOD
        { 0x00808040,   0x00000002,     0 },    // SERIAL: SERIAL PORT GLOBAL CONTROL
        { 0x00808042,   0x00000222,     0 },    // SERIAL: FSX/DX/CLKX PORT
        { 0x00808043,   0x00000000,     0 },    // SERIAL: FSR/DR/CLKR PORT
        { 0x00808044,   0x00000000,     0 },    // SERIAL: R/X TIMER CONTROL
        { 0x00808045,   0x00000000,     0 },    // SERIAL: R/X TIMER COUNTER
        { 0x00808046,   0x00000000,     0 },    // SERIAL: R/X TIMER PERIOD
        { 0x00808060,   0x00000000,     0 },    // BUS :   IOSTRB_ BUS
        { 0x00808064,   0x004F0000,     0 },    // BUS :   STRB0_ BUS
        { 0x00808068,   0x000F0000,     0 },    // BUS :   STRB1_ BUS
        { 0 },                                  // End of list
};

/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    /*----- Set up Memory Strobes -----*/

    BUS_ADDR->strb0_gcontrol  =  EXTERNAL_RDY | DATA_32 | MEMW_32 | STRB_SW_XTRA;       // SRAM
    BUS_ADDR->strb1_gcontrol  =  EXTERNAL_RDY | DATA_32 | MEMW_32;                      // HC16 RAM
    BUS_ADDR->iostrb_gcontrol =  EXTERNAL_RDY;                                          // TICTAC

    /*----- Init timers to count EDAC single bit errors -----*/

    TIMER_ADDR(0)->gcontrol  = FUNC;
    TIMER_ADDR(1)->gcontrol  = FUNC;
    TIMER_ADDR(0)->period    = 0xFFFFFFFF;
    TIMER_ADDR(1)->period    = 0xFFFFFFFF;
    TIMER_ADDR(0)->counter   = 0x00000000;
    TIMER_ADDR(1)->counter   = 0x00000000;
    TIMER_ADDR(0)->gcontrol |= GO | HLD_;
    TIMER_ADDR(1)->gcontrol |= GO | HLD_;

    sbe_hc16_last = TIMER_ADDR(0)->counter;
    sbe_c32_last  = TIMER_ADDR(1)->counter;

    /*----- Init Front Panel LEDs -----*/

    LEDS_INIT;                                  // I/O XF0/1 are both outputs to drive front panel LEDs

    /*----- Clear counters and internal ram -----*/

    memset((void *)INTERNAL_RAM_START,  0, INTERNAL_RAM_LEN);

    DPRAM->internal_ram_sbe_counter    = 0;
    DPRAM->internal_reg_sbe_counter    = 0;
    DPRAM->internal_reg_failed_counter = 0;

    /*----- Prepare Spy serial data for PBL interface tests -----*/

    DPRAM->serial_data[0] = 0;                          // Control Property
    DPRAM->serial_data[2] = 0x12345678;
    DPRAM->serial_data[3] = ToIEEE( 3000.0);
    DPRAM->serial_data[4] = ToIEEE(-4000.0);
    DPRAM->serial_data[5] = ToIEEE(-5000.0);
    DPRAM->serial_data[6] = ToIEEE( 6000.0);
    DPRAM->serial_data[7] = ToIEEE(-7000.0);
    DPRAM->serial_data[8] = ToIEEE( 8000.0);
    DPRAM->serial_data[9] = ToIEEE(-9000.0);

    /*----- Prepare for interrupts from HC16 -----*/

    TICTAC = 0x00000F00;                        // Clear all interrupt sources (int3 .. int0);
    CLEAR_EXT_INTS;                             // Clear all interrupts flag;
    ENABLE_INTS;                                // Enable global interrupts
    ENABLE_EXT_INTS;                            // Enable external interrupts
    DPRAM->status = STATUS_ALIVE;               // Report that start up has completed

    /*----- Background loop - flash LED0,  test memory, check registers -----*/

    for(;;)
    {
        LED0_TOGGLE;
        MemPatternTest(0x00000000);
        MemPatternTest(0xFFFFFFFF);
        MemPatternTest(0x5A5A5A5A);
        MemPatternTest(0xA5A5A5A5);
        MemAddressTest();
        CheckEdacSbe();
        InternalRamTest();
//      AccessToHC16();

        if(serial_cmd_last == 0x00000222)
        {
            InternalRegTest();
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MemPatternTest(unsigned pattern)
/*---------------------------------------------------------------------------------------------------------*/
{
    unsigned    addr = MEM_START;
    unsigned    inv_pattern = ~pattern;

    DPRAM->status |= STATUS_ALIVE;

    while(addr <  MEM_END)
    {
        tictac = TICTAC;                        // Read tic tac register (IOSTRB) to activate memory poller

        *((unsigned *)addr++) = pattern;
        *((unsigned *)addr++) = inv_pattern;
        *((unsigned *)addr++) = pattern;
        *((unsigned *)addr++) = inv_pattern;
        *((unsigned *)addr++) = pattern;
        *((unsigned *)addr++) = inv_pattern;
        *((unsigned *)addr++) = pattern;
        *((unsigned *)addr++) = inv_pattern;
    }

    DPRAM->status |= STATUS_ALIVE;

    addr = MEM_START;

    while(addr <  MEM_END)
    {
        tictac = TICTAC;                        // Read tic tac register (IOSTRB) to activate memory poller

        if(((unsigned *)addr)[0] != pattern     ||
           ((unsigned *)addr)[1] != inv_pattern ||
           ((unsigned *)addr)[2] != pattern     ||
           ((unsigned *)addr)[3] != inv_pattern ||
           ((unsigned *)addr)[4] != pattern     ||
           ((unsigned *)addr)[5] != inv_pattern ||
           ((unsigned *)addr)[6] != pattern     ||
           ((unsigned *)addr)[7] != inv_pattern)
        {
            DPRAM->status |= STATUS_PATTERN_FAULT;
            return;
        }
        addr += 8;
    }

    DPRAM->status &= ~STATUS_PATTERN_FAULT;
}
/*---------------------------------------------------------------------------------------------------------*/
void MemAddressTest(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    unsigned    addr = MEM_START;

    DPRAM->status |= STATUS_ALIVE;

    while(addr <  MEM_END)
    {
        tictac = TICTAC;                        // Read tic tac register (IOSTRB) to activate memory poller

        *((unsigned *)addr) = addr;     addr++;
        *((unsigned *)addr) = addr;     addr++;
        *((unsigned *)addr) = addr;     addr++;
        *((unsigned *)addr) = addr;     addr++;
        *((unsigned *)addr) = addr;     addr++;
        *((unsigned *)addr) = addr;     addr++;
        *((unsigned *)addr) = addr;     addr++;
        *((unsigned *)addr) = addr;     addr++;
    }

    DPRAM->status |= STATUS_ALIVE;

    for(addr = MEM_START;addr <  MEM_END;addr++)
    {
        if(*((unsigned *)addr) != addr)
        {
            DPRAM->status |= STATUS_ADDRESS_FAULT;
            return;
        }
    }

    DPRAM->status &= ~STATUS_ADDRESS_FAULT;
}
/*---------------------------------------------------------------------------------------------------------*/
void CheckEdacSbe(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    unsigned    sbe_hc16, sbe_c32;

    sbe_hc16 = DPRAM->hc16_sram_sbe_counter = TIMER_ADDR(0)->counter;
    sbe_c32  = DPRAM->c32_sram_sbe_counter  = TIMER_ADDR(1)->counter;

    if((sbe_hc16  - sbe_hc16_last) > 0)
    {
        DPRAM->status |= STATUS_HC16_SBE;
    }
    else
    {
        DPRAM->status &= ~STATUS_HC16_SBE;
    }

    if((sbe_c32 - sbe_c32_last) > 0)
    {
        DPRAM->status |= STATUS_C32_SBE;
    }
    else
    {
        DPRAM->status &= ~STATUS_C32_SBE;
    }

    sbe_hc16_last = sbe_hc16;
    sbe_c32_last  = sbe_c32;
}
/*---------------------------------------------------------------------------------------------------------*/
void AccessToHC16(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    unsigned    access;
    unsigned    delay;

    for (delay = 0; delay<1000000; delay++)
    {
        access = DPRAM->status;
        READ_HC16_RAM;
        READ_HC16_RAM;
        READ_HC16_RAM;
        READ_HC16_RAM;
        READ_HC16_RAM;
        READ_HC16_RAM;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void Isr0(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    DISABLE_INTS;
    TICTAC = TICTAC_INT0;
    CLEAR_EXT_INT0;

    SerialPort();
}
/*---------------------------------------------------------------------------------------------------------*/
void Isr1(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    DISABLE_INTS;
    TICTAC = TICTAC_INT1;
    CLEAR_EXT_INT1;

    SerialPort();
}
/*---------------------------------------------------------------------------------------------------------*/
void Isr2(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    DISABLE_INTS;
    TICTAC = TICTAC_INT2;
    CLEAR_EXT_INT2;

    SerialPort();
}
/*---------------------------------------------------------------------------------------------------------*/
void Isr3(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    DISABLE_INTS;
    TICTAC = TICTAC_INT3;
    CLEAR_EXT_INT3;

    SerialPort();
}
/*---------------------------------------------------------------------------------------------------------*/
void InternalRamTest(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    unsigned    i;
    unsigned *  addr = (unsigned *)INTERNAL_RAM_START;

    for(i=0;i < INTERNAL_RAM_LEN;i++)           // For each location in the internal memory
    {
        if(*addr != 0)                                  // If memory location has been corrupted
        {
            *addr = 0;                                          // Reset memory to zero
            DPRAM->internal_ram_sbe_counter++;                  // increment SBE counter
        }

        addr++;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void InternalRegTest(void)
/*---------------------------------------------------------------------------------------------------------*\
  Test the C32 internal register. If there is a SBE, the software corrects the error and read an other time
  the internal register. If there is always an error the register is considered broken.
\*---------------------------------------------------------------------------------------------------------*/
{
    unsigned    i;
    unsigned    value;

    for (i=0; internal_regs[i].addr_c32; i++)                           // Scan all internal registers
    {
        if (internal_regs[i].state == 0)                                        // if register doesn't break scan register
        {
            value = *((unsigned *)internal_regs[i].addr_c32);

            if (value != internal_regs[i].value)                                // If value in the internal register is corrupted
            {
                DPRAM->internal_reg_sbe_counter++;                                              // increment the SBE counter
                DPRAM->internal_reg_failed_idx = i;

                *((unsigned *)internal_regs[i].addr_c32) = internal_regs[i].value;              // reload the correct value in the register

                if( *((unsigned *)internal_regs[i].addr_c32) != internal_regs[i].value )        // if the reloaded value is correct
                {
                    internal_regs[i].state = 1;                                                         // mark register as failed
                    DPRAM->internal_reg_failed_counter++;                                               // increment ref failed counter
                }
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void SerialPort(void)
/*---------------------------------------------------------------------------------------------------------*\
  Test the C32 serial port that goes to the TEST connector.  This is used for the Spy interface or the
  link to the SIM_CONV card in the PS Main Power Converter controls.
  H4IRRAD_DAC_TEST is used to send a step functions to DACs under test.
\*---------------------------------------------------------------------------------------------------------*/
{
    unsigned            idx;
    unsigned            serial_cmd = DPRAM->serial_cmd;
    unsigned            control;


    if(++time_ms > 999)
    {
        time_ms = 0;
        time_s++;
    }

    if (serial_cmd != serial_cmd_last)
    {
        if ( (serial_cmd & FIFO_TEST_LINK) && !(serial_cmd_last & FIFO_TEST_LINK) )
        // Initialise serial port for FIFO test
        {
            SERIAL_PORT_ADDR(0)->gcontrol =
                                        FSXOUT   |      // FSXOUT is an output pin
                                        XCLKSRCE |      // XCLKSRCE is internal transmit clock
                                        CLKXP    |      // CLKXP(Polarity) is active low (data on neg edge)
                                        XLEN_32  |      // 32 bits transmitted
                                        XVAREN;         // Frame synch FSX active when all data transmitted

            SERIAL_PORT_ADDR(0)->s_x_control =          // Serial output port direction control
                                        CLKXFUNC |      // CLKX0 is serial output port pin
                                        DXFUNC   |      // DX0 is serial output port pin
                                        FSXFUNC ;       // FX0 is serial output port pin

            SERIAL_PORT_ADDR(0)->s_rxt_control =
                                        XGO      |      // Timer enabled
                                        XCLKSRC  |      // Internal clock
                                        XHLD_    |      // Counter enabled
                                        XCP_;           // Clock mode chosen

            SERIAL_PORT_ADDR(0)->gcontrol |= XRESET;    // Transmit reset - start serial port
        }

        if (( !(serial_cmd & FIFO_TEST_LINK) && (serial_cmd_last & FIFO_TEST_LINK) ) ||
                ( !(serial_cmd & H4IRRAD_DAC_TEST) && (serial_cmd_last & H4IRRAD_DAC_TEST) ))
        // Reset serial port config at end of FIFO test/H4IRRAD tests
        {
            SERIAL_PORT_ADDR(0)->gcontrol = 0;
            SERIAL_PORT_ADDR(0)->s_rxt_control = 0;
            SERIAL_PORT_ADDR(0)->s_rxt_counter = 0;
        }

        if ( (serial_cmd & H4IRRAD_DAC_TEST) && !(serial_cmd_last & H4IRRAD_DAC_TEST) )
        // Initialise serial port for DAC test
        {

            dacClkFreq = DPRAM->serial_data[0];
            if (dacClkFreq < 62)
            {
                dacClkFreq = 62;
            }
            dacNbStep = DPRAM->serial_data[1];
            if (dacNbStep < 1)
            {
                dacNbStep = 1;
            }
            dacStepDuration = DPRAM->serial_data[2];
            if (dacStepDuration < 1)
            {
                dacStepDuration = 1;
            }
            SERIAL_PORT_ADDR(0)->gcontrol =
                                        FSXOUT   |      // FSXOUT is an output pin
                                        XCLKSRCE |      // XCLKSRCE is internal transmit clock
                                        // CLKXP    |   // CLKXP(Polarity) is active high (data on pos edge)
                                        XLEN_16  |      // 16 bits transmitted
                                        // XFSM     |   // Transm frame sync mode: XFS goes back to inactive between words
                                        XVAREN   |      // Frame synch FSX active all along data transmitting
                                        FSXP     ;  // Transm frame sync polarity. Active low, used as /CS

            SERIAL_PORT_ADDR(0)->s_x_control =          // Serial output port direction control
                                        CLKXFUNC | // CLKX0 is serial output port pin
                                        DXFUNC   | // DX0 is serial output port pin
                                        FSXFUNC ;  // FX0 is serial output port pin

            SERIAL_PORT_ADDR(0)->s_rxt_control =
                                        XGO      |      // Timer enabled
                                        XCLKSRC  |      // Internal clock
                                        XHLD_    |  // Counter enabled
                                        XCP_;           // Clock mode chosen

            if (dacClkFreq > 4000000)
            {
                SERIAL_PORT_ADDR(0)->s_rxt_period_bit.x_period = 0; // Special case with x_period = 0
            }
            else
            {
                SERIAL_PORT_ADDR(0)->s_rxt_period_bit.x_period = 4000000/dacClkFreq; // Fclk = 4000kHz/x_period
            }

            SERIAL_PORT_ADDR(0)->gcontrol |= XRESET;  // Transmit reset - start serial port
        }

    }
    else
    {
        if (serial_cmd & FIFO_TEST_LINK)
        {

            DPRAM->serial_data[1] = time_ms | ( DPRAM->serial_data[1] & 0xFF7F0000 );            // Prepare CONTROL POPS word

            if(time_ms < 2)                             // If millisecond 0 or 1
            {
                DPRAM->serial_data[1] |= 0x00800000;            // Set C0 flag (bit 23)
            }

            for(idx=0;idx < SERIAL_DATA_LEN;idx++)      // Send words to C32 serial link
            {
                WAITSPY;
                SPY(DPRAM->serial_data[idx]);
            }

            if(!(++isr_counter % 100))
            {
                LED1_TOGGLE;
            }
        }
        else if (serial_cmd & H4IRRAD_DAC_TEST)
        {

            // Generate the step function according to parameters
            if (time_s > dacStepDuration) // every dacStepDuration seconds
            {
                time_s = 0;
                WAITSPY;
                SPY(step*(0xFFFF/dacNbStep));
                step = (step+1)%(dacNbStep+1);
            }
            if(!(++isr_counter % 200))
            {
                LED1_TOGGLE;
            }
        }
        else
        {
            // Toggle the output according the command

            SERIAL_PORT_ADDR(0)->s_x_control = serial_cmd;

            if(!(++isr_counter % 500))
            {
                LED1_TOGGLE;
            }
        }
    }








    serial_cmd_last = serial_cmd;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/

