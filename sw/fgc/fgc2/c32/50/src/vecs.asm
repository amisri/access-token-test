*---------------------------------------------------------------------------------------------------------
* File:     vecs.asm
*
* Contents: This file defines the interrupt and reset vectors for the C32 program
*
* Notes:
*
* History:
*
*   03/02/04    qak Adapted from main C32 program
*---------------------------------------------------------------------------------------------------------

    .sect   "vecs"          ; reset and interrupt vectors

    .ref    _c_int00        ; compiler defined C initialization routine
    .ref    _Isr0           ; HC16 interrupt 0
    .ref    _Isr1           ; HC16 interrupt 1
    .ref    _Isr2           ; HC16 interrupt 2
    .ref    _Isr3           ; HC16 interrupt 3

reset:  .word   _c_int00        ; Reset
int0:   .word   _Isr0           ; Interrupt 0
int1:   .word   _Isr1           ; Interrupt 1
int2:   .word   _Isr2           ; Interrupt 2
int3:   .word   _Isr3           ; Interrupt 3

*---------------------------------------------------------------------------------------------------------
*   End of file: vecs.asm
*---------------------------------------------------------------------------------------------------------

