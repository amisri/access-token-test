/*---------------------------------------------------------------------------------------------------------*\
  File:         abs_time.c

  Purpose:      ABSOLUTE TIME FUNCTIONS

                The contents of this file is shared between the MCU and DSP programs

\*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>
#include <stdbool.h>
#include <mcu_dsp_common.h>

/*---------------------------------------------------------------------------------------------------------*/
void AbsTimeUsAddDelay(struct abs_time_us * abs_time, INT32U delay_us)
/*---------------------------------------------------------------------------------------------------------*\
  This function will add a positive delay expressed in microsecond to the given absolute time.
  Note that the function is intended for small delay_us. Even though delay_us can be greater than 1E+06 (1s),
  this function will run slowly if delay_us is much higher than a second.

  To add a delay longer than several seconds, use AbsTimeUsAddLongDelay.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Ignore a negative delay. E.g. The unwary AbsTimeUsAddDelay(time_a, -1);
    // Comparison delay_us > 0x7FFFFFFFU was issuing compiler a warning (compiler bug?)

    if (delay_us >= 0x80000000U)
    {
        return;
    }

    abs_time->us_time += delay_us;

    while (abs_time->us_time >  999999)
    {
        abs_time->us_time  -= 1000000;
        abs_time->unix_time++;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AbsTimeUsAddOffset(struct abs_time_us * abs_time, INT32S offset)
/*---------------------------------------------------------------------------------------------------------*\
  This function will add a positive or negative offset in microsecond to the given absolute time.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (offset == 0)
    {
        return;
    }

    offset += abs_time->us_time;

    if (offset >= 0)
    {
        while (offset >  999999)
        {
            offset -= 1000000;
            abs_time->unix_time++;
        }

        abs_time->us_time = offset;
    }
    else
    {
        while (offset < 0)
        {
            offset += 1000000;
            abs_time->unix_time--;
        }

        abs_time->us_time = offset;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AbsTimeMsAddDelay(struct abs_time_ms * abs_time, INT32U delay_ms)
/*---------------------------------------------------------------------------------------------------------*\
  This function will add a positive delay expressed in millisecond to the given absolute time.
  Note that the function is intended for small delay_ms. Even though delay_ms can be greater than 1000 (1s),
  this function will run slowly if delay_ms is much higher than a second.

  To add a delay longer than several seconds, use AbsTimeMsAddLongDelay.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Ignore a negative delay. E.g. The unwary AbsTimeMsAddDelay(time_a, -1);
    // Comparison delay_us > 0x7FFFFFFFU was issuing compiler a warning (compiler bug?)

    if (delay_ms >= 0x80000000U)
    {
        return;
    }

    abs_time->ms_time += delay_ms;

    while (abs_time->ms_time >  999)
    {
        abs_time->ms_time  -= 1000;
        abs_time->unix_time++;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AbsTimeMsAddOffset(struct abs_time_ms * abs_time, INT32S offset)
/*---------------------------------------------------------------------------------------------------------*\
  This function will add a positive or negative offset in milliseconds to the given absolute time.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (offset == 0)
    {
        return;
    }

    offset += abs_time->ms_time;

    if (offset >= 0)
    {
        while (offset >  999)
        {
            offset -= 1000;
            abs_time->unix_time++;
        }

        abs_time->ms_time = offset;
    }
    else
    {
        while (offset < 0)
        {
            offset += 1000;
            abs_time->unix_time--;
        }

        abs_time->ms_time = offset;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AbsTimeUsAddLongDelay(struct abs_time_us * abs_time, INT32S delay_second, INT32U delay_us)
/*---------------------------------------------------------------------------------------------------------*\
  This function will add a delay expressed in second and microsecond to the given absolute time.
  The parameter 'second' can be negative, whereas the parameter 'delay_us' is unsigned.
  Therefore is is possible to add a negative delay, for example to subtract -1.400000 s, use the following
  function call: AbsTimeUsAddLongDelay(time_a, -2, 600000);     // -2 + 0.600000 = -1.400000

  Note that the function is intended for small delay_us. Even though delay_us can be greater than 1E+06 (1s),
  this function will run slowly if delay_us is much higher than a second.
\*---------------------------------------------------------------------------------------------------------*/
{
    abs_time->unix_time += delay_second;
    AbsTimeUsAddDelay(abs_time, delay_us);
}
/*---------------------------------------------------------------------------------------------------------*/
void AbsTimeMsAddLongDelay(struct abs_time_ms * abs_time, INT32S delay_second, INT32U delay_ms)
/*---------------------------------------------------------------------------------------------------------*\
  This function will add a delay expressed in second and microsecond to the given absolute time.
  The parameter 'second' can be negative, whereas the parameter 'delay_us' is unsigned.
  Therefore is is possible to add a negative delay, for example to subtract -1.400 s, use the following
  function call: AbsTimeMsAddLongDelay(time_a, -2, 600);     // -2 + 0.600 = -1.400

  Note that the function is intended for small delay_ms. Even though delay_ms can be greater than 1000 (1s),
  this function will run slowly if delay_ms is much higher than a second.
\*---------------------------------------------------------------------------------------------------------*/
{
    abs_time->unix_time += delay_second;
    AbsTimeMsAddDelay(abs_time, delay_ms);
}
/*---------------------------------------------------------------------------------------------------------*/
INT32S AbsTimeMsDiff(struct abs_time_ms const * const abs_time1,
                     struct abs_time_ms const * const abs_time2)
/*---------------------------------------------------------------------------------------------------------*\
  This function will return the difference in ms. between abs_time1 and abs_time2.
  diff = abs_time2 - abs_time1
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32S diff;

    diff  = (abs_time2->unix_time - abs_time1->unix_time) * 1000;
    diff += ((INT32S)abs_time2->ms_time - (INT32S)abs_time1->ms_time);

    return (diff);
}
/*---------------------------------------------------------------------------------------------------------*/
INT32S AbsTimeUsDiff(struct abs_time_us const * const abs_time1,
                     struct abs_time_us const * const abs_time2)
/*---------------------------------------------------------------------------------------------------------*\
  This function will return the difference in us. between abs_time1 and abs_time2.
  diff = abs_time2 - abs_time1
\*---------------------------------------------------------------------------------------------------------*/
{
    INT32S diff;

    diff  = (abs_time2->unix_time - abs_time1->unix_time) * 1000000;
    diff += ((INT32S)abs_time2->us_time - (INT32S)abs_time1->us_time);

    return (diff);
}
/*---------------------------------------------------------------------------------------------------------*/
void AbsTimeCopyFromMsToUs(const struct abs_time_ms * abs_time, struct abs_time_us * converted_abs_time)
/*---------------------------------------------------------------------------------------------------------*\
  This function will copy the input abs_time into the output converted_abs_time, converting the fractional
  time from millisecond to microsecond as it does so.
\*---------------------------------------------------------------------------------------------------------*/
{
    converted_abs_time->unix_time = abs_time->unix_time;
    converted_abs_time->us_time   = abs_time->ms_time * (INT32U)1000;
}
/*---------------------------------------------------------------------------------------------------------*/
void AbsTimeCopyFromUsToMs(const struct abs_time_us * abs_time, struct abs_time_ms * converted_abs_time)
/*---------------------------------------------------------------------------------------------------------*\
  This function will copy the input abs_time into the output converted_abs_time, converting the fractional
  time from microsecond to millisecond as it does so.
\*---------------------------------------------------------------------------------------------------------*/
{
    converted_abs_time->unix_time = abs_time->unix_time;

    // Round to the smaller, closest millisecond
    converted_abs_time->ms_time   = abs_time->us_time / (INT32U) 1000;
}
/*---------------------------------------------------------------------------------------------------------*/
bool AbsTimeLessThanMs(struct abs_time_ms const * time1, struct abs_time_ms const * time2)
/*---------------------------------------------------------------------------------------------------------*\
  Returns True if time1 is less than time 2.
\*---------------------------------------------------------------------------------------------------------*/
{
    bool retval;

    if (time1->unix_time == time2->unix_time)
    {
        retval = (time1->ms_time < time2->ms_time);
    }
    else
    {
        retval = (time1->unix_time < time2->unix_time);
    }

    return retval;
}
/*---------------------------------------------------------------------------------------------------------*/
bool AbsTimeMoreThanMs(struct abs_time_ms const * time1, struct abs_time_ms const * time2)
/*---------------------------------------------------------------------------------------------------------*\
  Returns True if time1 is more than time 2.
\*---------------------------------------------------------------------------------------------------------*/
{
    bool retval;

    if (time1->unix_time == time2->unix_time)
    {
        retval = (time1->ms_time > time2->ms_time);
    }
    else
    {
        retval = (time1->unix_time > time2->unix_time);
    }

    return retval;
}
/*---------------------------------------------------------------------------------------------------------*/
bool AbsTimeMoreThanEqualMs(struct abs_time_ms const * time1, struct abs_time_ms const * time2)
/*---------------------------------------------------------------------------------------------------------*\
  Returns True if time1 is more than or equal to time 2.
\*---------------------------------------------------------------------------------------------------------*/
{
    bool retval;

    if (time1->unix_time == time2->unix_time)
    {
        retval = (time1->ms_time >= time2->ms_time);
    }
    else
    {
        retval = (time1->unix_time > time2->unix_time);
    }

    return retval;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: abs_time.c
\*---------------------------------------------------------------------------------------------------------*/
