/*---------------------------------------------------------------------------------------------------------*\
  File:         fbs_class.h

  Contents:

  Notes:

\*---------------------------------------------------------------------------------------------------------*/

#ifndef FBS_CLASS_H      // header encapsulation
#define FBS_CLASS_H

#ifdef FBS_CLASS_GLOBALS
    #define FBS_CLASS_VARS_EXT
#else
    #define FBS_CLASS_VARS_EXT extern
#endif
//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

// Status variable constants

#define FAULTS                  fbs.u.fgc_stat.class_data.c53.st_faults
#define WARNINGS                fbs.u.fgc_stat.class_data.c53.st_warnings
#define ST_LATCHED              fbs.u.fgc_stat.class_data.c53.st_latched
#define ST_UNLATCHED            fbs.u.fgc_stat.class_data.c53.st_unlatched
#define STATE_PLL               fbs.u.fgc_stat.class_data.c53.state_pll
#define STATE_OP                fbs.u.fgc_stat.class_data.c53.state_op
#define STATE_VS                fbs.u.fgc_stat.class_data.c53.state_vs
#define STATE_PC                fbs.u.fgc_stat.class_data.c53.state_pc
#define ST_MEAS_A               fbs.u.fgc_stat.class_data.c53.st_meas_a
#define ST_MEAS_B               fbs.u.fgc_stat.class_data.c53.st_meas_b
#define ST_DCCT_A               fbs.u.fgc_stat.class_data.c53.st_dcct_a
#define ST_DCCT_B               fbs.u.fgc_stat.class_data.c53.st_dcct_b
#define REG_STATE               fbs.u.fgc_stat.class_data.c53.state_reg
#define REF                     fbs.u.fgc_stat.class_data.c53.ref
#define V_REF                   fbs.u.fgc_stat.class_data.c53.v_ref
#define B_MEAS                  fbs.u.fgc_stat.class_data.c53.b_meas
#define I_MEAS                  fbs.u.fgc_stat.class_data.c53.i_meas
#define V_MEAS                  fbs.u.fgc_stat.class_data.c53.v_meas

// For compatibility with Class 51

#define I_REF                   dpcls.dsp.ref.i
#define I_ERR_MA                (int16_t)dpcls.dsp.reg.i_err_ma
#define I_DIFF_MA               (int16_t)dpcls.dsp.meas.i_diff_ma

//-----------------------------------------------------------------------------------------------------------

#endif  // FBS_CLASS_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fbs_class.h
\*---------------------------------------------------------------------------------------------------------*/
