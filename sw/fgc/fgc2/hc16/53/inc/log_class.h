/*---------------------------------------------------------------------------------------------------------*\
  File:         log_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef LOG_CLASS_H      // header encapsulation
#define LOG_CLASS_H

#ifdef LOG_CLASS_GLOBALS
    #define LOG_CLASS_VARS_EXT
#else
    #define LOG_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------
#define DEFPROPS_INC_ALL    // defprops.h

#include <cc_types.h>   // basic typedefs
#include <definfo.h>    // for FGC_CLASS_ID
#include <log_event.h>  // for struct log_evt_rec
#include <defconst.h>   // for FGC_LOG_CYCLES_LEN
#include <dpcom.h>      // for struct abs_time_us
#include <log.h>        // for struct log_ana_vars
#include <memmap_mcu.h> // for LOG_IAB_32, LOG_IAB_W
#include <fbs.h>        // for fbs global variable
#include <fgc_log.h>    // for FGC_LOG_SIG_INFO_STEPS
#include <defprops.h>   // for PROP_STATE_P

//-----------------------------------------------------------------------------------------------------------

// Event log related constants

#define FGC_LAST_LOG            log_cycle

//-----------------------------------------------------------------------------------------------------------

struct timing_log_vars                                  // Initialised in log.c
{
    struct log_evt_rec  log_rec;                                // Event log record workspace
    uint16_t              out_idx;                                // Buffer out index
    uint16_t              in_idx;                                 // Circular buffer input index
    struct abs_time_us  time         [FGC_LOG_CYCLES_LEN];      // Start of cycles time
    char                length_bp    [FGC_LOG_CYCLES_LEN];      // Cycle length in basic periods
    char                user         [FGC_LOG_CYCLES_LEN];      // Cycle user (negative for start of super cycle)
    uint16_t              stc_func_type[FGC_LOG_CYCLES_LEN];      // Cycle function type symbol index
    char                reg_mode_flag[FGC_LOG_CYCLES_LEN];      // Cycle regulation mode flag=[B|I|V]
    uint16_t              dysfunction_code[FGC_LOG_CYCLES_LEN];       // Cycle warning or fault number
    uint8_t               dysfunction_is_a_fault[FGC_LOG_CYCLES_LEN]; // warning(0) or fault (1)
};

// POPS Post mortem log signals structure

struct pm_sigs                                          // 1ms update with 1ms delay
{
    FP32                ref;                            // Field reference (G) or Current reference (A)
    FP32                v_ref;                          // Voltage reference (V)
    FP32                b_meas;                         // Field measurement (G)
    FP32                i_meas;                         // Current measurement (I) for previous ms
    FP32                v_meas;                         // Voltage measurement (V) for previous ms
    FP32                err;                            // Field or current loop error (G or A)
};

//-----------------------------------------------------------------------------------------------------------

void    LogTiming       (void);
void    LogEvtTiming    (void);
void    LogIab          (void);
void    LogIearth       (void);
void    LogIleads       (void);
void    LogIreg         (void);
void    LogCycle        (void);

/*---------------------------------------------------------------------------------------------------------*/

// Event log property table definition
LOG_CLASS_VARS_EXT struct pm_sigs               pm_sigs;                        // PM log signals



LOG_CLASS_VARS_EXT struct log_evt_prop          log_evt_prop[]
#ifdef LOG_CLASS_GLOBALS
= {     // Property pointer                Old value       +-Property Name----------+
        {   &PROP_STATE_PC,                 0xFFFF,         "STATE.PC"                      },
        {   &PROP_STATE_OP,                 0xFFFF,         "STATE.OP"                      },
        {   &PROP_CONFIG_STATE,             0xFFFF,         "CONFIG.STATE"                  },
        {   &PROP_CONFIG_MODE,              0xFFFF,         "CONFIG.MODE"                   },
        {   &PROP_FGC_PLL_STATE,            0xFFFF,         "FGC_PLL.STATE"                 },
        {   &PROP_VS_STATE,                 0xFFFF,         "VS.STATE"                      },
        {   &PROP_LOG_PM_TRIG,              0x0000,         "LOG.PM.TRIG"                   },
        {   &PROP_DIG_STATUS,               0x0000,         "DIG.STATUS"                    },
        {   &PROP_STATUS_FAULTS,            0x0000,         "STATUS.FAULTS"                 },
        {   &PROP_STATUS_WARNINGS,          0x0000,         "STATUS.WARNINGS"               },
        {   &PROP_STATUS_ST_LATCHED,        0x0000,         "STATUS.ST_LATCHED"             },
        {   &PROP_STATUS_ST_UNLATCHED,      0x0000,         "STATUS.ST_UNLATCHED"           },
        {   &PROP_MEAS_STATUS_A,            0x0000,         "MEAS.STATUS.A"                 },
        {   &PROP_MEAS_STATUS_B,            0x0000,         "MEAS.STATUS.B"                 },
        {   &PROP_DCCT_A_STATUS,            0x0000,         "DCCT.A.STATUS"                 },
        {   &PROP_DCCT_B_STATUS,            0x0000,         "DCCT.B.STATUS"                 },
        {   &PROP_ADC_FILTER_STATE,         0xFFFF,         "ADC.FILTER.STATE"              },
        {   &PROP_PAL_STATE,                0xFFFF,         "PAL.STATE"                     },
        {   &PROP_PAL_LINKS_LOGGEDCONTROL,  0x0000,         "PAL.LINKS.LOGGEDCONTROL"       },
        {   &PROP_PAL_LINKS_STATUS,         0x0000,         "PAL.LINKS.STATUS"              },
        {   &PROP_LOG_PM_TRIG,              0x0000,         "LOG.PM.TRIG"                   },
        {   &PROP_REF_CYC_FAULT_CHK,        0xFFFF,         "REF.CYC.FAULT.CHK"             },
        {   NULL    },
}
#endif
;

// Define/Initialise timing log variable

LOG_CLASS_VARS_EXT struct timing_log_vars       timing_log
#ifdef LOG_CLASS_GLOBALS
= {
        { 0,0,"USER=-- LENGTH=-- SSC=-","REG_MODE=- FUNC_TYPE=-------------","START" },     // timing_log.log_rec
        FGC_LOG_CYCLES_LEN,                                                                 // timing_log.out_idx
}
#endif
;

// Analogue log structure definitions for class 53

LOG_CLASS_VARS_EXT struct log_ana_vars     log_cycle              // CYCLE log variables structure
#ifdef LOG_CLASS_GLOBALS
= {
        LOG_CYCLE_32,                               // Buffer base address
        LOG_CYCLE_W,                                // Buffer size in words
        0,                                          // Not used
        FGC_LOG_CYCLE_LEN,                          // Number of samples in the log
        FGC_BASIC_PERIOD_MS,                        // Number of post trig samples
        6,                                          // Number of signals per sample
        24,                                         // Number of bytes per sample
        0,                                          // Wait time (ms) (real-time readout is not supported)
        1,                                          // Period in milliseconds (1kHz)
        { FGC_LOG_SIG_INFO_STEPS,                   // Signal info
          FGC_LOG_SIG_INFO_STEPS,
          0, 0, 0,
          FGC_LOG_SIG_INFO_STEPS},
        { "REF",        "V_REF_(V)",  "B_MEAS_(G)", // Signal labels and units
          "I_MEAS_(A)", "V_MEAS_(V)", "ERR" },
}
#endif
;



//-----------------------------------------------------------------------------------------------------------

#endif  // LOG_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: log_class.h
\*---------------------------------------------------------------------------------------------------------*/
