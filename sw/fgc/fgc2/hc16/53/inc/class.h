/*---------------------------------------------------------------------------------------------------------*\
  File:     class.h

  Contents: This file contains all class 53 related definitions

  Notes:    The CLASS_ID constant is not defined when header files are parsed because defs.h must be
        the last header file.  So all class related definitions are collected in this file
        for class 53.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CLASS_H      // header encapsulation
#define CLASS_H

#ifdef CLASS_GLOBALS
    #define CLASS_VARS_EXT
#else
    #define CLASS_VARS_EXT extern
#endif
//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

// MST constants

#define DEV_STA_TSK_PHASE   1       // Millisecond to run StaTsk()

// Warnings/unlatched/latched status masks

#define FGC_FAULTS      (   FGC_FLT_FGC_HW          \
                          | FGC_FLT_I_MEAS          \
                          | FGC_FLT_REG_ERROR       \
                          | FGC_FLT_FGC_STATE       \
                          | FGC_FLT_LIMITS )

#define FGC_WARNINGS    (   FGC_WRN_FGC_HW          \
                          | FGC_WRN_I_MEAS          \
                          | FGC_WRN_REG_ERROR       \
                          | FGC_WRN_TEMPERATURE     \
                          | FGC_WRN_VS_COMMS        \
                          | FGC_WRN_CONFIG )         

// all DSP warning bits MUST be here to be cleared
#define ALL_DSP_WARNINGS_MASK (   FGC_WRN_I_MEAS          \
                          | FGC_WRN_REF_LIM         \
                          | FGC_WRN_REF_RATE_LIM    \
                          | FGC_WRN_REG_ERROR       \
                          | FGC_WRN_TIMING_EVT      \
                          | FGC_WRN_B_MEAS          \
                          | FGC_WRN_V_ERROR         \
                          | FGC_WRN_VS_COMMS        \
                          | FGC_WRN_CYCLE_START     \
                          | FGC_WRN_I_RMS_LIM )

#define DSP_PM_WARNINGS (   FGC_WRN_B_MEAS          \
                          | FGC_WRN_V_ERROR         \
                          | FGC_WRN_CYCLE_START )

#define DSP_UNLATCHED   (   FGC_UNL_I_MEAS_DIFF )

#define DSP_MEAS        (   FGC_MEAS_I_MEAS_OK      \
                          | FGC_MEAS_V_MEAS_OK      \
                          | FGC_MEAS_IN_USE         \
                          | FGC_MEAS_SIGNAL_STUCK   \
                          | FGC_MEAS_SIGNAL_NOISE   \
                          | FGC_MEAS_SIGNAL_JUMP    \
                          | FGC_MEAS_ADC22_IN_USE   \
                          | FGC_MEAS_FLT_NO_FRAME   \
                          | FGC_MEAS_FLT_FRAME_ERR  \
                          | FGC_MEAS_MPX_INVALID    \
                          | FGC_MEAS_CAL_FAILED )

#define DSP_DCCT_MASK   (   FGC_DCCT_CAL_FLT        \
                          | FGC_DCCT_FAULT )

#define MCU_DCCT_MASK   (   FGC_DCCT_ZERO_I         \
                          | FGC_DCCT_NO_HEAD        \
                          | FGC_DCCT_TEMP_FLT       \
                          | FGC_DCCT_MCB_OFF )

#define ST_LATCHED_RESET (  FGC_LAT_VDC_FAIL        \
                          | FGC_LAT_PSU_V_FAIL      \
                          | FGC_LAT_DIM_SYNC_FLT    \
                          | FGC_LAT_DIMS_EXP_FLT    \
                          | FGC_LAT_DIM_TRIG_FLT    \
                          | FGC_LAT_ANA_FLT         \
                          | FGC_LAT_DAC_FLT         \
                          | FGC_LAT_DALLAS_FLT      \
                          | FGC_LAT_PAM_NOSIG_FLT   \
                          | FGC_LAT_DCCT_PSU_FAIL   \
                          | FGC_LAT_DCCT_PSU_STOP )

// Missing definitions for class specific symbol lists

#define SYM_LST_CYC_FLT &sym_lst_53_cyc_flt[0]
#define SYM_LST_CYC_WRN &sym_lst_53_cyc_wrn[0]

// Include class related header files

#include <dpcls.h>      // Include dual port RAM structure definitions

//-----------------------------------------------------------------------------------------------------------

/*
     reg=00000 cy=1 then shl (<<) number
     or
     reg=00001 then shl (<<) (number-1)

     1 - > 0x0001,
     2 - > 0x0002,
     3 - > 0x0004,
     4 - > 0x0008,
     5 - > 0x0010,
     6 - > 0x0020,
     7 - > 0x0040,
     8 - > 0x0080,
     9 - > 0x0100,
    10 - > 0x0200,
    11 - > 0x0400,
    12 - > 0x0800,
    13 - > 0x1000,
    14 - > 0x2000,
    15 - > 0x4000,
    16 - > 0x8000,

 */

// Do NOT define the masks array as a const, at least not on FGC2. If defined as const, the linker will not map it
// into the 1st memory page (where all the non-const data reside), but in the same memory pages as the code and the
// other constant data. To access this kind of data the processor needs to switch between memory pages using
// register K. Parts of the FGC2 code that are written in assembly does not manipulate the K register and as a
// consequence can only access data located in the first memory page. Function LogEvtProp is partly written in
// in assembly and needs access to the masks array, and it must be mapped in the 1st memory page.

CLASS_VARS_EXT uint16_t    masks[16]
#ifdef CLASS_GLOBALS
=
{
    0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080,
    0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000, 0x4000, 0x8000,
}
#endif
;
//-----------------------------------------------------------------------------------------------------------

#endif  // CLASS_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: 53\inc\class.h
\*---------------------------------------------------------------------------------------------------------*/
