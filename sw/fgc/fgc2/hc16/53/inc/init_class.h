/*---------------------------------------------------------------------------------------------------------*\
  File:         init_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef INIT_CLASS_H      // header encapsulation
#define INIT_CLASS_H

#ifdef INIT_CLASS_GLOBALS
    #define INIT_CLASS_VARS_EXT
#else
    #define INIT_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------
#define DEFPROPS_INC_ALL    // defprops.h

#include <cc_types.h>   // basic typedefs
#include <definfo.h>    // for FGC_CLASS_ID
#include <defprops.h>   // for PROP_MODE_OP
#include <dpcls.h>      // for CAL_DEF_INTERNAL_ADC_GAIN, CAL_DEF_EXTERNAL_ADC_GAIN
#include <init.h>
//-----------------------------------------------------------------------------------------------------------

// Generic macro to identify the references' symbolic list (the variable name is class specific.)

#define SYM_LST_REF sym_lst_53_ref

/*---------------------------------------------------------------------------------------------------------*/

// INIT_CLASS_VARS_EXT static char    devtype[]
INIT_CLASS_VARS_EXT char    devtype[]
#ifdef INIT_CLASS_GLOBALS
= {
    "RFM"
}
#endif
;


// INIT_CLASS_VARS_EXT static uint32_t   conf_l[]
INIT_CLASS_VARS_EXT int32_t  conf_l[]
#ifdef INIT_CLASS_GLOBALS
= {
/* 0 */ FGC_CTRL_DISABLED,
/* 1 */ FGC_CTRL_ENABLED,
/* 2 */ CAL_DEF_INTERNAL_ADC_GAIN,
/* 3 */ CAL_DEF_EXTERNAL_ADC_GAIN,
/* 4 */ 0,
}
#endif
;


// INIT_CLASS_VARS_EXT static uint16_t   conf_s[]
INIT_CLASS_VARS_EXT uint16_t  conf_s[]
#ifdef INIT_CLASS_GLOBALS
= {
/* 0 */ FGC_CTRL_DISABLED,
/* 1 */ FGC_CTRL_ENABLED,
/* 2 */ FGC_OP_NORMAL
}
#endif
;


// INIT_CLASS_VARS_EXT static FP32    conf_f[]
INIT_CLASS_VARS_EXT FP32    conf_f[]
#ifdef INIT_CLASS_GLOBALS
= {
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 8.0, -8.0, 10.0, 80.0
}
#endif
;

INIT_CLASS_VARS_EXT FP32    conf_vs_sim[FGC_SIM_NUM_DEN_LEN]
#ifdef INIT_CLASS_GLOBALS
= {
    1.0, 0.0, 0.0, 0.0
}
#endif
;

INIT_CLASS_VARS_EXT FP32    conf_vs_delay
#ifdef INIT_CLASS_GLOBALS
= 0.001
#endif
;


INIT_CLASS_VARS_EXT uint32_t  adc_reset_mask
#ifdef INIT_CLASS_GLOBALS
= FGC_MEAS_SIGNAL_STUCK|FGC_MEAS_SIGNAL_NOISE|FGC_MEAS_SIGNAL_JUMP|FGC_MEAS_ADC_BUSERR
#endif
;

// INIT_CLASS_VARS_EXT static struct init_prop init_prop[]
//      CAUTION: If the property is NON-VOLATILE, its default size (numels in the XML definition) must be zero for it to
//               be initialized based on the data in init_prop

INIT_CLASS_VARS_EXT struct init_prop        init_prop[]
#ifdef INIT_CLASS_GLOBALS
= {
    // Non-volatile, non-config properties

    {   &PROP_MODE_OP,                          1,                      &conf_s[2]      },
    {   &PROP_MEAS_SIM,                         1,                      &conf_l[1]      },
    {   &PROP_CAL_A_ADC_INTERNAL_ERR,           6,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_INTERNAL_ERR,           6,                      &conf_f[0]      },
    {   &PROP_ADC_INTERNAL_LAST_CAL_TIME,       1,                      &conf_l[4]      },
    {   &PROP_FGC_NAME,                         3,                      &devtype[0]     },
    {   &PROP_DEVICE_TYPE,                      3,                      &devtype[0]     },

    {   &PROP_VS_SIM_NUM,                       FGC_SIM_NUM_DEN_LEN,    &conf_vs_sim[0] },
    {   &PROP_VS_SIM_DEN,                       FGC_SIM_NUM_DEN_LEN,    &conf_vs_sim[0] },
    {   &PROP_VS_SIM_CTRL_DELAY,                1,                      &conf_vs_delay  },

    {   &PROP_ADC_FILTER_UNEXP_RESETS_ENABLED,  1,                      &adc_reset_mask },

    // Global config properties

    {   &PROP_DEVICE_PPM,                       1,                      &conf_s[0]      },

    {   &PROP_ADC_EXTERNAL,                     1,                      &conf_s[0]      },

    {   &PROP_CAL_A_DCCT_HEADERR,               1,                      &conf_f[0]      },
    {   &PROP_CAL_A_DCCT_ERR,                   6,                      &conf_f[0]      },
    {   &PROP_CAL_A_DCCT_TC,                    3,                      &conf_f[0]      },
    {   &PROP_CAL_A_DCCT_DTC,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_A_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_A_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_A_ADC_INTERNAL_DTC,           3,                      &conf_f[0]      },
    {   &PROP_CAL_A_ADC_EXTERNAL_ONE_MHZ,       1,                      &conf_l[0]      },
    {   &PROP_CAL_A_ADC_EXTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_A_ADC_EXTERNAL_ERR,           6,                      &conf_f[0]      },

    {   &PROP_CAL_B_DCCT_HEADERR,               1,                      &conf_f[0]      },
    {   &PROP_CAL_B_DCCT_ERR,                   6,                      &conf_f[0]      },
    {   &PROP_CAL_B_DCCT_TC,                    3,                      &conf_f[0]      },
    {   &PROP_CAL_B_DCCT_DTC,                   3,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_INTERNAL_GAIN,          1,                      &conf_l[2]      },
    {   &PROP_CAL_B_ADC_INTERNAL_TC,            3,                      &conf_f[0]      },
    {   &PROP_CAL_B_ADC_INTERNAL_DTC,           3,                      &conf_f[1]      },
    {   &PROP_CAL_B_ADC_EXTERNAL_ONE_MHZ,       1,                      &conf_l[0]      },
    {   &PROP_CAL_B_ADC_EXTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_B_ADC_EXTERNAL_ERR,           6,                      &conf_f[0]      },

    {   &PROP_CAL_VREF_ERR,                     6,                      &conf_f[0]      },
    {   &PROP_DCCT_TAU_TEMP,                    1,                      &conf_f[8]      },

    {   &PROP_BARCODE_FGC_CASSETTE,             0,                      NULL            },
    {   &PROP_BARCODE_A_EXADC_CASSETTE,         0,                      NULL            },
    {   &PROP_BARCODE_B_EXADC_CASSETTE,         0,                      NULL            },
    {   &PROP_BARCODE_A_DCCT_HEAD,              0,                      NULL            },
    {   &PROP_BARCODE_A_DCCT_ELEC,              0,                      NULL            },
    {   &PROP_BARCODE_B_DCCT_HEAD,              0,                      NULL            },
    {   &PROP_BARCODE_B_DCCT_ELEC,              0,                      NULL            },

    // Normal properties

    {   &PROP_CAL_A_DAC,                        3,                      &conf_f[5]      },
    {   &PROP_ADC_INTERNAL_TAU_TEMP,            1,                      &conf_f[9]      },
    {   &PROP_VS_SIM_INTLKS,                    1,                      &conf_s[1]      },

    {   NULL    }       // End of list
}
#endif
;

// The DF_TIMESTAMP_SELECT dynamic flag is set for all the properties in the following list and their children.
// The same list is also used in function CmdPrintTimestamp to read the timestamp selector.

INIT_CLASS_VARS_EXT struct init_dynflag_timestamp_select init_dynflag_timestamp_select[]
#ifdef INIT_CLASS_GLOBALS
= {
    {   &PROP_REF_CYC,                          TIMESTAMP_PREVIOUS_CYCLE        },
    {   &PROP_REF_CYC_FAULT,                    TIMESTAMP_CURRENT_CYCLE         },
    {   &PROP_REF_CYC_WARNING,                  TIMESTAMP_CURRENT_CYCLE         },
    {   &PROP_LOG_CAPTURE_SIGNALS,              TIMESTAMP_LOG_CAPTURE           },
    {   NULL    }       // End of list
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // INIT_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: init_class.h
\*---------------------------------------------------------------------------------------------------------*/
