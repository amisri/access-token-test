/*---------------------------------------------------------------------------------------------------------*\
  File:         mst_class.c

  Purpose:      FGC HC16 Class 53 Software - Class related millisecond actions
\*---------------------------------------------------------------------------------------------------------*/


#define DEFPROPS_INC_ALL    // defprops.h

#include <mst_class.h>

#include <stdbool.h>
#include <class.h>              // for ALL_DSP_WARNINGS_MASK
#include <fbs_class.h>          // for FAULTS, WARNINGS, ST_LATCHED, ST_UNLATCHED, STATE_OP, STATE_PC, ST_MEAS_A, ST_MEAS_B, ST_DCCT_A, ST_DCCT_B, I_EARTH_CPCNT, V_REF, I_MEAS, V_MEAS, I_REF, I_ERR_MA, I_DIFF_MA
#include <dev.h>                // for dev global variable
#include <fbs.h>                // for fbs global variable
#include <mst.h>                // for mst global variable
#include <dpcom.h>              // for dpcom global variable
#include <macros.h>             // for Test(), Clr()
#include <sta.h>                // for sta global variable
#include <sta_class.h>          // for vs global variable
#include <dpcls.h>              // for dpcls global variable
#include <log_class.h>          // for log_iab global variable, LogTiming(), LOG_IEARTH_PHASE, LOG_IREG_PHASE, LOG_ILEADS_PHASE
#include <defprops.h>           // for PROP_REF_CYC
#include <adc_class.h>

/*---------------------------------------------------------------------------------------------------------*/
void MstClass(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the millisecond task, MstTsk(), to perform the class related millisecond
  activities.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* Every Millisecond: Handle ADC filters reset requests, and on-going reset */

    if(dpcls.mcu.adc.filter_state != FGC_FILTER_STATE_READY)   // If on-going ADC filters reset
    {                                                       // To be called before AdcFiltersReset every ms
        AdcFiltersResetCheck();
    }
    if(adc.trigger_reset_f &&                               // If ADC filters reset is triggered
       mst.dsp_reg_run_f)                                   // and the DSP was regulating on the previous ms
    {
        AdcFiltersReset();                                      // Reset ADC filters
    }
    if(dpcls.dsp.adc.reset_req == ADC_DSP_RESET_REQ)        // If new ADC reset request coming from the DSP
    {
        AdcFiltersResetRequest(false);                          // Treat the DSP request
    }

    // Every Millisecond: Log POPS data

    if(log_cycle.samples_to_acq)                        // If POPS logging samples still to acquire
    {
        LogCycle();
    }

    // Millisecond 1 of 20: Record DSP signals

    if(mst.ms_mod_20 == MST_FIP_STAT_VAR_TIME)
    {
        REG_STATE    = dpcls.dsp.reg.state.int32u;
        REF          = pm_sigs.ref;                             // Get ms data aligned on ms 0 of 20
        V_REF        = pm_sigs.v_ref;
        B_MEAS       = pm_sigs.b_meas;
        I_MEAS       = pm_sigs.i_meas;
        V_MEAS       = pm_sigs.v_meas;
    }

    // Millisecond 2 of 20: Read DSP status

    if(mst.ms_mod_20 == MST_FIP_STAT_VAR_TIME)
    {
        FAULTS      |= (uint16_t)dpcom.dsp.faults;                // Latch faults from DSP
        ST_LATCHED  |= (uint16_t)dpcom.dsp.st_latched;            // Latch status from DSP

        WARNINGS     = (WARNINGS & ~ALL_DSP_WARNINGS_MASK) |            // Mix in DSP warnings
                       (uint16_t)dpcom.dsp.warnings;
        ST_UNLATCHED = (ST_UNLATCHED & ~DSP_UNLATCHED) |        // Mix in DSP unlatched status
                       (uint16_t)(dpcom.dsp.st_unlatched & DSP_UNLATCHED);

        ST_MEAS_A    = (ST_MEAS_A & ~DSP_MEAS) |                // Mix in DSP meas A flags
                       (uint16_t)dpcls.dsp.meas.st_meas[0];
        ST_MEAS_B    = (ST_MEAS_B & ~DSP_MEAS) |                // Mix in DSP meas B flags
                       (uint16_t)dpcls.dsp.meas.st_meas[1];

        ST_DCCT_A    = (ST_DCCT_A & ~DSP_DCCT_MASK) |           // Mix in DSP dcct A flags
                       (uint16_t)dpcls.dsp.meas.st_dcct[0];
        ST_DCCT_B    = (ST_DCCT_B & ~DSP_DCCT_MASK) |           // Mix in DSP dcct B flags
                       (uint16_t)dpcls.dsp.meas.st_dcct[1];

        if(Test(FAULTS,FGC_FLT_I_MEAS))                         // If I_MEAS fault is present
        {
            Clr(WARNINGS,FGC_WRN_I_MEAS);                               // Suppress the I_MEAS warning
        }
    }

    // Millisecond 5 of 20: Set DICO alive signal status

    else if(mst.ms_mod_20 == 5)
    {                                                         // Enable dico alive for digital interface if
        mst.dicoalive = (STATE_OP == FGC_OP_NORMAL      ||              // operation mode is normal or
                         STATE_OP == FGC_OP_CALIBRATING ||              // calibrating or
                         STATE_OP == FGC_OP_TEST        ||              // testing or
                        (STATE_OP == FGC_OP_SIMULATION && !vs.sim_intlks)); // simulating VS but not interlocks
    }

    // Millisecond 12 of 20: Check events if time variable was received

    else if(mst.ms_mod_20 == 12 && fbs.time_rcvd_f)
    {
        RefEvent();
    }

    // Millisecond 1 of 1000: Check if logging has finished

    else if(!SM_MSTIME_P == 1)
    {
        if(dev.log_pm_state == FGC_LOG_STOPPING &&                      // If PM log state is STOPPING, and
           !log_cycle.samples_to_acq)                                   // POPS log has stopped
        {
            dev.log_pm_state = FGC_LOG_FROZEN;                          // Set PM Log state to Frozen
            Clr(fbs.u.fieldbus_stat.ack,FGC_SELF_PM_REQ);               // Reset self-trig PM dump request
        }

        // ADC: Handle periodic reset of the ADC filters

        if(sta.adc_external_f &&                                        // If ADC interface is SD-350 or SD-351
           (mst.time.unix_time - adc.last_reset_unix_time) >= ADC_RESET_PERIOD_S) // And reset period exceeded
        {
            AdcFiltersResetRequest(false);                              // Attempt to reset the ADC filters
        }
    }

    // Every 200ms: Check any pending ADC reset request

    if(mst.ms_mod_200 == 1 && adc.pending_reset_req_f)
    {
        AdcFiltersResetRequest(false);          // Attempt to reset the ADC filters
    }


    // Every Millisecond: Check and log start of PPM cycles

    if((uint16_t)dpcom.dsp.cyc.start_cycle_f)
    {
        sta.cyc_start_new_cycle_f = true;           // Trigger publication of property LOG.CYC by STA task

        LogTiming();

        dpcom.dsp.cyc.start_cycle_f = false;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mst_class.c
\*---------------------------------------------------------------------------------------------------------*/
