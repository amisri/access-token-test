/*---------------------------------------------------------------------------------------------------------*\
  File:         set_class.c

  Purpose:      FGC HC16 Class 53 Software - Set Command Functions
\*---------------------------------------------------------------------------------------------------------*/


#define DEFPROPS_INC_ALL    // defprops.h

#include <stdbool.h>
#include <set_class.h>
#include <cmd.h>                // for struct cmd
#include <fbs_class.h>          // for ST_UNLATCHED, STATE_OP, STATE_PC, ST_MEAS_A, ST_MEAS_B
#include <defprops.h>
#include <definfo.h>            // for FGC_CLASS_ID
#include <dev.h>                // for dev global variable
#include <fbs.h>                // for fbs global variable
#include <dpcom.h>              // for dpcom global variable
#include <macros.h>             // for Test(), Set(), DEVICE_PPM
#include <sta.h>                // for sta global variable
#include <sta_class.h>          // for vs global variable, DDOP_CMD_RESET, DDOP_CMD_POL_POS, DDOP_CMD_POL_NEG, StaDdip()
#include <dpcls.h>              // for dpcls global variable
#include <cal_class.h>          // for cal global variable, NON_PPM_REG_MODE
#include <prop.h>               // for PropSet()
#include <math.h>               // for fabs()
#include <fgc_errs.h>           // for FGC_BAD_PARAMETER, FGC_BAD_STATE
#include <pars.h>               // for ParsSet()
#include <log.h>                // for LogStartAll()
#include <ref_class.h>          // for RefArm(), RefPlep(), RefOpenloop(), RefTrim(), RefTest()


/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetLogCaptureUser(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Set LOG.CAPTURE.USER
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  errnum;
    int32_t* new_val;
    int16_t  user;

    // If LOG.CAPTURE.CYC_CHK_TIME is not zero, meaning that the capture log is already used for a cycle
    // check warning, do not set LOG.CAPTURE.USER

    if(dpcls.dsp.log.capture.cyc_chk_time.unix_time)
    {
        return(FGC_LOG_WAITING);
    }

    // Parse user value

    errnum  = ParsValueGet(c, p, &new_val);                     // Get new value from command packet

    if(!c->last_par_f)
    {
        return(FGC_BAD_PARAMETER);
    }

    if(errnum == 0)
    {
        user = *new_val;

        // LOG.CAPTURE.USER can be modified from -1 to a positive value (i.e. ask to capture a log for the
        // specified user), or from a positive value to -1 (i.e. cancel current capture log). All other
        // transitions are forbidden.

        if(user == dpcls.dsp.log.capture.user)
        {
            errnum = FGC_OK_NO_RSP;
        }
        else if((user == -1  && dpcls.dsp.log.capture.user >= 0) ||
                (user >= 0   && dpcls.dsp.log.capture.user == -1))
        {
            c->changed_f = true;
            dpcls.dsp.log.capture.user = user;
        }
        else
        {
            errnum = FGC_BAD_STATE;
        }
    }

    return(errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetTableFunc(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    POINT properties
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t errnum = ParsSet(c, p);

    if(errnum == 0)
    {
        // Attempt to arm the table function

        errnum = RefArm(c, c->mux_idx, FGC_REF_TABLE, STC_TABLE);
    }

    // Recover table from NVS if arming failed

    if(errnum != 0 && STATE_PC != FGC_PC_IDLE)
    {
        NvsRecallValuesForOneProperty(c, p, c->mux_idx);
    }

    return(errnum);
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: set_class.c
\*---------------------------------------------------------------------------------------------------------*/

