/*---------------------------------------------------------------------------------------------------------*\
 File:      get_class.c

 Purpose:   FGC HC16 Class 53 Software - Get Command Functions
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h
#define GET_CLASS_GLOBALS   // define meas global variable

#include <get_class.h>
#include <stdbool.h>
#include <stdio.h>              // for fprintf()
#include <cmd.h>                // for scm global variable, struct cmd
#include <class.h>
#include <defprops.h>
#include <dev.h>
#include <property.h>           // for struct prop, and typedef prop_size_t
#include <definfo.h>            // for FGC_CLASS_ID
#include <macros.h>             // for Test()
#include <dpcls.h>              // for dpcls global variable
#include <fgc_errs.h>           // for FGC_BAD_STATE
#include <log_class.h>          // for timing_log, log_iab global variables, FGC_LAST_LOG
#include <mcu_dependent.h>      // for ENTER_SR(), EXIT_SR
#include <os_hardware.h>        // for OS_ENTER_CRITICAL
#include <fbs.h>                // for FbsOutLong()
#include <prop.h>               // for PropValueGet()
#include <mem.h>                // for MemCpyWords()
#include <string.h>             // for strlen()
#include <spy_v2.h>


/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetLogCaptureSig(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: Capture signal log property

  The PPM log buffer (LOG.CAPTURE.BUF) contains upto 7200 records of 9 floats.  These signals are:

    0   0x0001      REF
    1   0x0002      V_REF
    2   0x0004      B_MEAS
    3   0x0008      I_MEAS
    4   0x0010      V_MEAS
    5   0x0020      ERR
    6   0x0040      Signal from LOG.SPY.MPX[0]
    7   0x0080      Signal from LOG.SPY.MPX[1]
    8   0x0100      Signal from LOG.SPY.MPX[2]

  Properties are provided to give an application access to the first six only.  The value of the property
  is the offset to the signal (0-5).
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t     n;
    uint16_t          errnum;
    prop_size_t     sample_idx;
    uint16_t          sample_step;
    FP32 *          f_p;            // Pointer to float value
    char * FAR      fmt;            // Pointer to format string

    static char * FAR   decfmt[]   = { "%.7E","%13.5E" };
    static char * FAR   hexfmt[]   = { "   0x%08lX","0x%lX" };

    errnum = CmdPrepareGet(c, p, 4, &n);
    if(errnum)
    {
        return(errnum);
    }

    // Check that Get command and options are valid for a capture log signal

    if(Test(c->getopts,GET_OPT_BIN))        // If BIN get option
    {
        if(!dpcls.dsp.log.capture.n_samples)            // If no log data available
        {
            return(0);                          // Return nothing
        }

        CmdStartBin(c, p, (uint32_t)(sizeof(float) * n));
    }
    else                    // else ASCII
    {
        if(Test(c->getopts,GET_OPT_HEX))        // If HEX
        {
            fmt = hexfmt[c->token_delim == ','];
        }
        else                        // else DECIMAL
        {
            fmt = decfmt[c->token_delim == ' '];
        }
    }

    sample_idx = c->from * FGC_LOG_CAPTURE_N_SIGS + (uint16_t)p->value;

    sample_step = FGC_LOG_CAPTURE_N_SIGS * c->step;

    while(n--)                  // For all elements in the array range
    {
        errnum = PropValueGet(c, &PROP_LOG_CAPTURE_BUF, NULL, sample_idx, (uint8_t**)&f_p);
        if ( errnum != 0 )
        {
            return(errnum);
        }

        if(Test(c->getopts,GET_OPT_BIN))                // If BIN get option
        {
            FbsOutLong( (char *) f_p, c);               // Send in binary
        }
        else                                            // else not binary
        {
            errnum = CmdPrintIdx(c,c->from);
            if ( errnum )       // Print index if required
            {
                return(errnum);
            }

            fprintf(c->f,fmt,*f_p);             // Display as x.xxxxxxEyy or 0xXXXXXXXX

            c->from += c->step;
        }

        sample_idx += sample_step;
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetLogCaptureSpy(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: Capture log spy property

  The capture log buffer (LOG.CAPTURE.BUF) contains up to 7200 records of 9 floats.  These signals are:

    0   0x0001      REF      (I_REF or B_REF)
    1   0x0002      V_REF
    2   0x0004      B_MEAS
    3   0x0008      I_MEAS
    4   0x0010      V_MEAS
    5   0x0020      ERR
    6   0x0040      Signal from LOG.SPY.MPX[0]
    7   0x0080      Signal from LOG.SPY.MPX[1]
    8   0x0100      Signal from LOG.SPY.MPX[2]
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint16_t      s;
    uint16_t      reg_state;
    prop_size_t sample_idx;
    uint16_t      n_sigs;
    uint16_t      n_samples;
    uint16_t      mask;
    uint16_t      sigs_mask;
    uint16_t      errnum;
    int8_t * FAR label;
    char *      ch_ptr;

    union                   // Union to reduce impact on stack
    {
        struct SPY_v2_buf_header   log;
        struct SPY_v2_sig_header   sig;
    } header;

    static struct capture_log signals[3][9] =
    {
        {
            { 0,        "",   0 },                          //  V reg mode
            { "V_REF",      "V",      FGC_LOG_SIG_INFO_STEPS },
            { "B_MEAS",     "G",      0 },
            { "I_MEAS",     "A",      0 },
            { "V_MEAS",     "V",      0 },
            { 0,        "",   0 },
            { 0,        "",   0 },
            { 0,        "",   0 },
            { 0,        "",   0 },
        },
        {
            { "I_REF",      "A",      FGC_LOG_SIG_INFO_STEPS }, //  I reg mode
            { "V_REF",      "V",      FGC_LOG_SIG_INFO_STEPS },
            { "B_MEAS",     "G",      0 },
            { "I_MEAS",     "A",      0 },
            { "V_MEAS",     "V",      0 },
            { "I_ERR",      "A",      FGC_LOG_SIG_INFO_STEPS },
            { 0,        "",   0 },
            { 0,        "",   0 },
            { 0,        "",   0 },
        },
        {
            { "B_REF",      "G",      FGC_LOG_SIG_INFO_STEPS }, // B reg mode
            { "V_REF",      "V",      FGC_LOG_SIG_INFO_STEPS },
            { "B_MEAS",     "G",      0 },
            { "I_MEAS",     "A",      0 },
            { "V_MEAS",     "V",      0 },
            { "B_ERR",      "G",      FGC_LOG_SIG_INFO_STEPS },
            { 0,        "",   0 },
            { 0,        "",   0 },
            { 0,        "",   0 },
        }
    };

    // Check that Get command and options are valid for a Spy log

    n_samples = dpcls.dsp.log.capture.n_samples;

    // If the DSP BG or DSP RT is not running then report it

    if(!mst.dsp_bg_is_alive || !mst.dsp_rt_is_alive)
    {
        return(FGC_DSP_NOT_AVL);
    }

    if(c->n_arr_spec || c->step > 1)        // If array indecies are specified
    {
        return(FGC_BAD_ARRAY_IDX);      // Report error
    }

    if(!Test(c->getopts,GET_OPT_BIN))       // If BIN get option not specified (BIN cannot be set for SCM)
    {
        return(FGC_BAD_GET_OPT);        // Report bad get option
    }

    if(!n_samples)                          // If no data available
    {
        return(0);              // Return nothing
    }

    if(c->mux_idx != 0 &&                       // If wrong user due to cycle warning
       c->mux_idx != dpcls.dsp.log.capture.user)
    {
        return(FGC_BAD_CYCLE_SELECTOR);                   // Report error
    }

    // Select signals according to property and reg mode

    reg_state = dpcls.dsp.log.capture.reg_state;

    switch(p->sym_idx)
    {
        case STP_ALL:

            switch(reg_state)
            {
                case FGC_REG_B: sigs_mask = 0x01FF; break; // B_REF|V_REF|B_MEAS|I_MEAS|V_MEAS|B_ERR|MPX[0]|MPX[1]|MPX[2]
                case FGC_REG_I: sigs_mask = 0x01FB; break; // I_REF|V_REF|      |I_MEAS|V_MEAS|I_ERR|MPX[0]|MPX[1]|MPX[2]
                case FGC_REG_V: sigs_mask = 0x01DE; break; //      |V_REF|B_MEAS|I_MEAS|V_MEAS|     |MPX[0]|MPX[1]|MPX[2]
            }
            break;

        case STP_OP:

            switch(reg_state)
            {
                case FGC_REG_B: sigs_mask = 0x003F; break; // B_REF|V_REF|B_MEAS|I_MEAS|V_MEAS|B_ERR
                case FGC_REG_I: sigs_mask = 0x003B; break; // I_REF|V_REF|      |I_MEAS|V_MEAS|I_ERR
                case FGC_REG_V: sigs_mask = 0x001E; break; //      |V_REF|B_MEAS|I_MEAS|V_MEAS|
            }
            break;

        case STP_REG:

            switch(reg_state)
            {
                case FGC_REG_B: sigs_mask = 0x0025; break; // B_REF|     |B_MEAS|      |      |B_ERR
                case FGC_REG_I: sigs_mask = 0x0029; break; // I_REF|     |      |I_MEAS|      |I_ERR
                case FGC_REG_V: return(FGC_BAD_STATE);
            }
            break;
    }

    // Count the number of signals specified in sigs_mask

    for( n_sigs = 0, mask = sigs_mask; mask; n_sigs++ )
    {
        mask &= mask - 1;                               // Clear least significant bit set
    }

    // Generate log header

    spySetBufHeader(&header.log,                                      // buf_header
                    false,                                            // little_endian
                    true,                                             // ana_signals
                    true,                                             // ana_sigs_in_columns
                    true,                                             // timestamp_is_lst
                    n_sigs,                                           // num_signals
                    n_samples,                                        // num_samples
                    0,                                                // time_origin_s
                    0,                                                // time_origin_us
                    dpcls.dsp.log.capture.last_sample_time.unix_time, // time_stamp_s
                    dpcls.dsp.log.capture.last_sample_time.us_time,   // time_stamp_us
                    dpcls.dsp.log.capture.sampling_period_us);        // period_us

    CmdStartBin(c, p, (uint32_t)(sizeof(header.log) + sizeof(header.sig) * n_sigs) +
              (uint32_t)(sizeof(float)) * (uint32_t)(n_sigs * n_samples));

    FbsOutBuf((uint8_t *)&header.log, sizeof(header.log), c);          // Write log header

    // Generate signal headers

    for( i = 0, mask = 1; i < FGC_LOG_CAPTURE_N_SIGS; i++, mask <<= 1)
    {
        if(Test(sigs_mask,mask))            // if signal is included
        {
            char * dst = header.sig.name;

            label = signals[reg_state][i].label;

            if(!label)
            {
                label = CmdPrintSymLst(sym_lst_53_spy, (uint16_t)dpcls.mcu.log_capture_mpx[i-6]);
            }

            spySetAnaSigHeader(&header.sig,                    // sig_header
                               signals[reg_state][i].info,     // step
                               false,                          // use_scaling
                               FGC_LOG_TYPE_FLOAT,             // sig_type
                               sizeof(float),                  // size
                               1.0,                            // gain
                               0.0,                            // offset
                               0,                              // time_offset_us
                               NULL,                          // name
                               NULL);   // units

            // The function above cannot copy FAR objects so copy the signal name explictely

            dst += MemCpyStrFar(dst, label);
            *dst++ = '_';
            *dst++ = '(';
            dst += MemCpyStrFar(dst, signals[reg_state][i].units);
            *dst++ = ')';
            *dst= '\0';

            FbsOutBuf((uint8_t *)&header.sig, sizeof(header.sig), c);      // Write signal header
        }
    }

    // Write data

    for( s = sample_idx = 0; s < n_samples; s++)
    {
        for( i = 0, mask = 1; i < FGC_LOG_CAPTURE_N_SIGS; i++, mask <<= 1)
        {
            if(Test(sigs_mask,mask))            // if signal is included
            {
                errnum = PropValueGet(c, &PROP_LOG_CAPTURE_BUF, NULL, sample_idx+i, (uint8_t**)&ch_ptr);
                if(errnum != 0)
                {
                    return(errnum);
                }
                FbsOutLong(ch_ptr, c);
            }
        }
        sample_idx += FGC_LOG_CAPTURE_N_SIGS;
    }

    // Arm log capture to allow new Spy acquistions, and reset LOG.CAPTURE.CYC_CHK_TIME

    dpcls.dsp.log.capture.state                  = FGC_LOG_ARMED;
    dpcls.dsp.log.capture.cyc_chk_time.unix_time = 0;
    dpcls.dsp.log.capture.cyc_chk_time.us_time   = 0;

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: get_class.c
\*---------------------------------------------------------------------------------------------------------*/
