//! @file  transaction.c
//! @brief Provides the framework for transactional settings
//!
//! The transactions specification is documented in the link below:
//! https://wikis.cern.ch/display/COTEC/Transactions+support+in+the+Control+System


// ---------- Includes

#include <stdint.h>
#include <macros.h> 
#include <fgc_errs.h>


// ---------  Internal structures, unions and enumerations  -------------



// ---------  External variable definitions  ----------------------------



// ---------  Internal variable definitions  ----------------------------




// ---------- External function definitions

uint16_t transactionGetId(void)
{
    return 0;
}



uint16_t transactionCommit(uint16_t const transaction_id, uint16_t const delay_ms)
{
    return FGC_NOT_IMPL;
}



uint16_t transactionRollback(uint16_t const transaction_id)
{
    return FGC_NOT_IMPL;
}



// EOF
