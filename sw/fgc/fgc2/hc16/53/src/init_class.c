/*!
 *  @file  init_class.c
 */

#include <stdint.h>

#define INIT_CLASS_GLOBALS

#include <init_class.h>
#include <dev.h>
#include <prop.h>
#include <macros.h>
#include <set.h>


/*---------------------------------------------------------------------------------------------------------*/
void InitClassPreInts(void)
/*---------------------------------------------------------------------------------------------------------*\
  Class specific initialisation before interrupts are started or the NVS properties have been set or
  the DSP is started.
\*---------------------------------------------------------------------------------------------------------*/
{
    AdcFiltersReadFunctions();                              // Read SD filter function meta data
}
/*---------------------------------------------------------------------------------------------------------*/
void InitClassPostInts(void)
/*---------------------------------------------------------------------------------------------------------*\
  Class specific initialisation after interrupts are started and the NVS properties have been set
\*---------------------------------------------------------------------------------------------------------*/
{
    NvsInit(&init_prop[0]);                             // Initialise non-volatile properties

    // Initialise the default non-PPM reg mode
    // This is the FIRST COPY of the REG.MODE_INIT property to the non-PPM reg mode.
    // (The second copy will happen after synchronising with the Database.)

    dpcls.mcu.ref.func.reg_mode[0] = ref.reg_mode_init;

    // Other initialisations

    dpcom.mcu.device_ppm = dev.ppm;
    dev.max_user         = GET_FGC_MAX_USER();

    rtd.ctrl = FGC_RTD_CYC;
}



void InitClassSpyMpx(struct cmd *c)
{
    static uint32_t init_spy_mpx[FGC_N_SPY_CHANS] = {   FGC_SPY_VREF,   FGC_SPY_IMEAS,
                                                        FGC_SPY_BMEAS,  FGC_SPY_VMEAS,
                                                        FGC_SPY_REF,    FGC_SPY_ERR             };

    static uint32_t init_log_spy_mpx[FGC_LOG_CAPTURE_MPX_LEN] = { FGC_SPY_VOUT_DXP1,
                                                                  FGC_SPY_VOUT_DXP2,
                                                                  FGC_SPY_UCAP_DSP1 };

    PropSet(c, &PROP_SPY_MPX, init_spy_mpx, FGC_N_SPY_CHANS);

    PropSet(c, &PROP_LOG_CAPTURE_SPY_MPX, init_log_spy_mpx, FGC_LOG_CAPTURE_MPX_LEN);
}



#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void InitClassPostSync(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  Class specific initialisation after FGC has been synchronised
\*---------------------------------------------------------------------------------------------------------*/
{
    // After synchronisation with the config DB, this is the SECOND COPY of the REG.MODE_INIT property
    // to the non-PPM reg mode. (The first copy happened after NVS init.)

    dpcls.mcu.ref.func.reg_mode[0] = ref.reg_mode_init;

    if(STATE_OP == FGC_OP_UNCONFIGURED)                     // If still unconfigured
    {
        return;                                                 // Return without setting PPM refs
    }

    // Do a S REG.TEST RESET - disabled from March 2019. REG.TEST properties have been archived and
    // are no longer in the property tree. This will be reviewed in 2021, when Class 53 will be refreshed
    // to no longer support the old B-Train measurement. At that point, the future of REG.TEST will be assessed.

//     PropSet(c, &PROP_REG_TEST_RESET, &zero, 1);

    // Set the PLEP parameters from the defaults

    SetDefaults(c, NULL);
}

// EOF
