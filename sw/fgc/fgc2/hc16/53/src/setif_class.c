/*---------------------------------------------------------------------------------------------------------*\
 File:          setif_class.c

 Purpose:       FGC HC16 Class 53 Software - Set Command Condition Functions
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#include <setif_class.h>
#include <cmd.h>        // for struct cmd
#include <fbs_class.h>  // for ST_UNLATCHED, STATE_OP, STATE_VS, STATE_PC, ST_DCCT_A, ST_DCCT_B
#include <defprops.h>
#include <definfo.h>    // for FGC_CLASS_ID
#include <mst.h>        // for mst global variable
#include <crate.h>      // for crate global variable
#include <macros.h>     // for Test()
#include <sta.h>        // for sta global variable
#include <sta_class.h>  // // for vs global variable
#include <dpcls.h>      // for dpcls global variable
#include <fgc_errs.h>   // for FGC_BAD_STATE
#include <log_class.h>  // for FGC_LAST_LOG
#include <fbs.h>        // for fbs global variable

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifModeOpOk(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Can the user set MODE.OP?
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!SetifPcOff(c) && STATE_OP != FGC_OP_CALIBRATING)
    {
        return(0);
    }

    return(FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: setif_class.c
\*---------------------------------------------------------------------------------------------------------*/

