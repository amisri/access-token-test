/*---------------------------------------------------------------------------------------------------------*\
  File:         registers.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created

        Note: there is already a reg.h, z:\projects\fgc\sw\inc\reg.h (Regulation algorithm library header file)
        this is why I named this one registers

\*---------------------------------------------------------------------------------------------------------*/

#ifndef REGISTERS_H     // header encapsulation
#define REGISTERS_H

/*---------------------------------------------------------------------------------------------------------*/

#include <cc_types.h>           // basic typedefs
#include <stdint.h>
#include <stdbool.h>

/*---------------------------------------------------------------------------------------------------------*/

uint16_t  RegTest         (uint32_t address, uint16_t data, uint16_t mask);
uint16_t  RegWrite        (uint32_t address, uint16_t data, bool runlog_f); // Return 1 on error
uint16_t  RegRead         (uint32_t address, uint16_t *data, bool runlog_f); // Return 1 on error

/*---------------------------------------------------------------------------------------------------------*/


#endif  // REGISTERS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: registers.h
\*---------------------------------------------------------------------------------------------------------*/
