/*---------------------------------------------------------------------------------------------------------*\
  File:         log.h

  Purpose:      FGC MCU Software - Logging related functions

  Note:         LogInitEvtProp() makes links between bits and enums for a property with a symlist
                in the structure log_evt_prop.  This means that the symbol constant values for any
                property logged must NEVER be outside the range 0-15.  If this happens then the
                system will crash itself at start up with E=0xBAD0.  This will be visible in the run log.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef LOG_H      // header encapsulation
#define LOG_H

#ifdef LOG_GLOBALS
    #define LOG_VARS_EXT
#else
    #define LOG_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------
#define DEFPROPS_INC_ALL    // defprops.h

#include <cc_types.h>   // basic typedefs
#include <stdio.h>      // for FILE
#include <cmd.h>        // for struct cmd
#include <dpcom.h>      // for struct abs_time_us
#include <memmap_mcu.h> // for LOG_THOUR_32, LOG_THOUR_W, LOG_TDAY_32, LOG_TDAY_W
#include <defprops.h>   // for STP_THOUR, STP_TDAY
#include <definfo.h>    // for FGC_CLASS_ID

//-----------------------------------------------------------------------------------------------------------

// State machine for logging commands in the event log

#define EVT_LOG_RESET_PROP              0
#define EVT_LOG_PROP                    1
#define EVT_LOG_PROP_FULL               2
#define EVT_LOG_RESET_VALUE             3
#define EVT_LOG_VALUE                   4
#define EVT_LOG_VALUE_FULL              5


#if (FGC_CLASS_ID == 51)
#define LOG_MENU_DIM            8
#define LOG_NUM_MENUS           (LOG_MENU_DIM + FGC_MAX_DIMS)
#define LOG_MENU_PM_MASK        0x5F  // IAB(0), IEARTH(1), ILEADS(2), IREG(3), THOUR(4), EVENTS(6)
#define LOG_MAX_SIGS            4
#endif
#if (FGC_CLASS_ID == 53)
#define LOG_MENU_DIM            7
#define LOG_NUM_MENUS           (LOG_MENU_DIM + FGC_MAX_DIMS)
#define LOG_MENU_PM_MASK        0x6A  // OP(1), THOUR(3), EVENTS(5), CONFIG(6)
#define LOG_MAX_SIGS            6
#endif
#if (FGC_CLASS_ID == 59)
#define LOG_MENU_DIM            0
#define LOG_NUM_MENUS           1
#define LOG_MAX_SIGS            4
#endif



/*---------------------------------------------------------------------------------------------------------*/

// Analog signals log variables

struct log_ana_vars                                     // These fields are initialised in log.c
{
    uint32_t              baseaddr;                       // ptr Buffer base address
    uint32_t              bufsize_w;                      // ptr Buffer size in words
    uint16_t              type;                           // ptr Sym_idx for log type
    uint16_t              n_samples;                      // ptr Number of samples in the log
    uint16_t              post_trig;                      // ptr Number of post trig samples
    uint16_t              n_signals;                      // ptr Number of signals per sample
    uint16_t              sample_size;                    // ptr Number of bytes per sample
    int16_t              wait_time_ms;                   // ptr Number of bytes per sample
    uint32_t              period_ms;                      // ptr Period in milliseconds
    uint8_t               sig_info[LOG_MAX_SIGS];         // Log info byte per signal
    char * FAR          sig_label_units[LOG_MAX_SIGS];  // Signal labels:units
    uint16_t              samples_to_acq;                 // Number of samples still to acquire
    uint16_t              last_sample_idx;                // Index of last sample
    struct abs_time_us  last_sample_time;               // Time of last sample
    bool                run_f;                          // Logging active flag (cleared on trip)
};

#if (FGC_CLASS_ID != 59)
struct Log_menus
{
    uint32_t            status_bit_mask[LOG_NUM_MENUS]; // LOG.MENU.STATUS
    struct abs_time_us  post_mortem_time;               // LOG.MENU.POST_MORTEM
};
#endif

//-----------------------------------------------------------------------------------------------------------

void       LogInit          (void);
void       LogGetPmSigBuf   (struct log_ana_vars *log, struct cmd *c);
void       LogGetPmEvtBuf   (struct cmd *c);
void       LogEvtCheck      (void);
void       LogInitEvtProp   (void);
void       LogStartAll      (void);
void       LogStart         (struct log_ana_vars *log_ana);
void       LogTStart        (struct log_ana_vars *log_ana);
void       LogEvtSaveValue  (struct cmd *c);
void       LogEvtSaveCh     (struct cmd *c, char ch);
void       LogEvtCmd        (struct cmd *c, uint16_t errnum);
void       LogEvtProp       (void);
void       LogEvtPropStore  (const char * FAR prop_name, const char * FAR symbol, uint16_t set_f, uint16_t bitmask_f);
void       LogEvtDim        (void);
void       LogEvtDimInput   (bool input_zero_f, bool fault_input_f, bool dimdb_lbl_is_null);
void * FAR LogNextAnaSample (struct log_ana_vars * log_ana);
void       LogThour         (void);
void       LogTday          (void);


void    logSetPostMortem(void);
void    logProcess      (void);
void    logFgcLoggerSetEnable(bool const enabled);
void    logFgcLoggerReset(void);
bool    logFgcLoggerIsPmEnabled(void);
bool    logFgcLoggerIaPmActive(void);

/*---------------------------------------------------------------------------------------------------------*/

#if (FGC_CLASS_ID != 59)
LOG_VARS_EXT struct Log_menus  log_menus;     // LOG.MENU.STATUS and LOG.MENSU.POST_MORTEM
#endif

// Analogue log structure definitions

LOG_VARS_EXT struct log_ana_vars     log_thour              // THOUR log variables structure
#ifdef LOG_GLOBALS
={
    LOG_THOUR_32,                               // Buffer base address
    LOG_THOUR_W,                                // Buffer size in words
    STP_THOUR,                                  // Sym_idx for log type
    FGC_LOG_THOUR_LEN,                          // Number of samples in the log
    1,                                          // Number of post trig samples
    4,                                          // Number of signals per sample
    8,                                          // Number of bytes per sample
    0,                                          // Wait time (ms)
    10000,                                      // Period in milliseconds (10s)
    { 0, 0, 0, 0 },                             // Signal info
    { "T_FGC_IN_(C)", "T_FGC_OUT_(C)",              // Signal labels and units
      "T_DCCT_A_(C)", "T_DCCT_B_(C)" },
}
#endif
;

LOG_VARS_EXT struct log_ana_vars     log_tday               // TDAY log variables structure
#ifdef LOG_GLOBALS
={
    LOG_TDAY_32,                                // Buffer base address
    LOG_TDAY_W,                                 // Buffer size in words
    STP_TDAY,                                   // Sym_idx for log type
    FGC_LOG_TDAY_LEN,                           // Number of samples in the log
    1,                                          // Number of post trig samples
    4,                                          // Number of signals per sample
    8,                                          // Number of bytes per sample
    0,                                          // Wait time (ms)
    600000,                                     // Period in milliseconds (10 minutes)
    { 0, 0, 0, 0 },                             // Signal info
    { "T_FGC_IN_(C)", "T_FGC_OUT_(C)",              // Signal labels and units
      "T_DCCT_A_(C)", "T_DCCT_B_(C)" },
}
#endif
;

LOG_VARS_EXT const char * FAR   bitmask_default_sym[16]
#ifdef LOG_GLOBALS
={
        "BIT_00",
        "BIT_01",
        "BIT_02",
        "BIT_03",
        "BIT_04",
        "BIT_05",
        "BIT_06",
        "BIT_07",
        "BIT_08",
        "BIT_09",
        "BIT_10",
        "BIT_11",
        "BIT_12",
        "BIT_13",
        "BIT_14",
        "BIT_15"
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // LOG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: log.h
\*---------------------------------------------------------------------------------------------------------*/
