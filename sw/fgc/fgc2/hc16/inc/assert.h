/*!
 * @file  assert.h
 */

/*!
 * Asserts expression at compile time
 *
 * When using this macro inside of a function, it has to be called in the
 * variable declaration section.
 *
 * This implementation shouldn't generate any warnings during compilation.
 * No assembly code should be generated either.
 */
#define static_assert(expr) extern char static_assert_failed[(expr) ? 1 : -1]

// EOF
