/*---------------------------------------------------------------------------------------------------------*\
 File:          DIMsDataProcess.h

 Notes:

\*---------------------------------------------------------------------------------------------------------*/

#ifndef DIMSDATAPROCESS_H       // header encapsulation
#define DIMSDATAPROCESS_H

#ifdef DIMSDATAPROCESS_GLOBALS
    #define DIMSDATAPROCESS_VARS_EXT
#else
    #define DIMSDATAPROCESS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <property.h>   // for struct prop
#include <cmd.h>        // for struct cmd
#include <dpcom.h>      // for struct dpcom_diag_log

//-----------------------------------------------------------------------------------------------------------

void    InitQspiBus             (void);
uint16_t  ProcessReceivedQSPIdata (uint16_t dpram);

/*!
 * This function will be called from the 1 millisecond task at the start of every millisecond.
 * It must quickly read any data waiting from a previous scan, and trigger the next scan if required.
 *
 * The readout data came from up to 16 boards in the chain, boards[0..15]
 * so QSM_RR[] is filled with a specific register 15 times, one per each board in the chain
 * The register in the QSM_RR[] depends in which scan burst we are after the reset.
 * After a reset of the QSPI bus the order in which the DIM registers will appear for each scan burst is
 *       0 - Dig0        DIM register ID:0
 *       1 - Dig1        DIM register ID:1
 *       2 - Ana0        DIM register ID:4
 *       3 - Ana1        DIM register ID:5
 *       4 - Ana2        DIM register ID:6
 *       5 - Ana3        DIM register ID:7
 *       6 - TrigCounter DIM register ID:2
 *                        there is no ID:3
 */
void    QSPIbusStateMachine     (void);

uint16_t  DimGetDimLog            (struct cmd *c, struct prop *p);
void    DimGetDimLogStatus      (struct dpcom_diag_log *log_status, uint16_t number_of_requests);
uint16_t  DimGetDimNames          (struct cmd *c, struct prop *p);
uint16_t  DimGetAnaLbls           (struct cmd *c, struct prop *p);
uint16_t  DimGetDigLbls           (struct cmd *c, struct prop *p, uint16_t bank);
uint16_t  DimGetAna               (struct cmd *c, struct prop *p);
uint16_t  DimGetDig               (struct cmd *c, struct prop *p);
uint16_t  DimGetFaults            (struct cmd *c, struct prop *p);
uint16_t  DimAnaLabel             (char *lbl_buf, uint16_t buf_len, const char * FAR dimdb_ana_lbl, uint16_t sym_idx);

//-----------------------------------------------------------------------------------------------------------

#endif  // DIMSDATAPROCESS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: DIMsDataProcess.h
\*---------------------------------------------------------------------------------------------------------*/
