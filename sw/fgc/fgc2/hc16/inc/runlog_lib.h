/*---------------------------------------------------------------------------------------------------------*\
  File:         runlog_lib.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef RUNLOG_LIB_H            // header encapsulation
#define RUNLOG_LIB_H

#include <cc_types.h>
#include <stdint.h>
#include <stdbool.h>
#include <memmap_mcu.h>
#include <fgc_runlog.h>
#include <fgc_runlog_entries.h>

#ifdef RUNLOG_LIB_GLOBALS
    #define RUNLOG_LIB_VARS_EXT
#else
    #define RUNLOG_LIB_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#define MAGIC_RL                0xBEEF
#define MAGIC_NVRAM             0x6651

#define IS_RL_READY()           (RL_MAGIC_P == MAGIC_RL)


//-----------------------------------------------------------------------------------------------------------

struct TRunlogResetCounters
{
    uint16_t              all;
    uint16_t              power;
    uint16_t              program;
    uint16_t              manual;
    uint16_t              fast_watchdog;
    uint16_t              slow_watchdog;
    uint16_t              network;
};


struct TRunlog
{
    struct fgc_runlog                   buffer[FGC_RUNLOG_N_ELS];
    uint16_t                              index;
    struct TRunlogResetCounters         resets;
    uint16_t                              magic;
};

//-----------------------------------------------------------------------------------------------------------

void    RunlogInit              (void);
void    RunlogSaveResets        (bool reset_f);
void    RunlogWrite             (uint8_t id, void * data);
uint8_t * RunlogRead              (uint8_t *outp, uint8_t *seq, uint8_t *id, void *data);

//-----------------------------------------------------------------------------------------------------------
// INLINE FUNCTIONS
//-----------------------------------------------------------------------------------------------------------

/*---------------------------------------------------------------------------------------------------------*/
#pragma INLINE
static void RunlogTimestamp(void)
/*---------------------------------------------------------------------------------------------------------*\
 Helper function to time-stamp logs.
\*---------------------------------------------------------------------------------------------------------*/
{
    RunlogWrite(FGC_RL_UXTIME, (void *) SM_UXTIME_16);  // Unix time
    RunlogWrite(FGC_RL_MSTIME, (void *) SM_MSTIME_16);  // Millisecond time
}
//-----------------------------------------------------------------------------------------------------------

// RUNLOG_LIB_VARS_EXT struct TRunlog   runlog_in_ram;

//-----------------------------------------------------------------------------------------------------------

#endif  // RUNLOG_LIB_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: runlog_lib.h
\*---------------------------------------------------------------------------------------------------------*/
