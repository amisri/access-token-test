/*---------------------------------------------------------------------------------------------------------*\
  File:     cmd.h

  Contents: Header file for cmd.c - Command processing functions

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CMD_H
#define CMD_H

#ifdef CMD_GLOBALS
    #define CMD_VARS_EXT
#else
    #define CMD_VARS_EXT extern
#endif

#include <cc_types.h>       // basic typedefs
#include <os.h>             // for OS_SEM type
#include <stdio.h>          // for FILE
#include <defconst.h>       // for FGC_MAX_PROP_DEPTH
#include <definfo.h>
#include <dpcom.h>          // for MAX_CMD_TOKEN_LEN
#include <log_event.h>      // for struct log_evt_rec
#include <property.h>       // for struct sym_lst, struct prop, typedef prop_size_t
#include <stdbool.h>
#include <mcu_dsp_common.h>

#define PPM_ALL_USERS       0xFFFF

//-----------------------------------------------------------------------------------------------------------

#define PARENT_GET_NOT_HIDDEN           0
#define PARENT_GET_CONFIG_CHANGED       1
#define PARENT_GET_CONFIG_SET           2
#define PARENT_GET_CONFIG_UNSET         3

//-----------------------------------------------------------------------------------------------------------

/*! Callback function to store character in command */
typedef void (*fputc_store_cb)(char ch);

// Fieldbus and serial command tasks and publication task variables

struct cmd
{
    OS_SEM *            sem;                // For fcm and tcm: Packet ready   For pcm: Lock
    FILE *              f;                  // Pointer to output stream FILE
    fputc_store_cb      store_cb;           // Callback to store character
    bool                abort_f;            // Command abort flag
    bool                get_parent_f;       // Get parent property flag
    bool                timestamp_f;        // Timestamp produced flag
    uint16_t              stat;               // Command status (reported to FIP for fcm)
    uint16_t              errnum;             // Command errnum (reported by Set/Get func())

    uint16_t              header;
    uint16_t              pkt_buf_idx;        // Current cmd packet index
    uint16_t              prop_blk_idx;       // Current property block index
    prop_size_t         n_elements;         // Current property length in elements
    struct prop    *    last_prop;          //!< Last property accessed
    struct prop *       prop;               // Current property
    struct prop *       cached_prop;        // Cached property
    struct pars_buf *   pars_buf;           // Double buffered command packet structure (in DPRAM on FGC2)
    struct prop_buf *   prop_buf;           // Property communications structure (in DPRAM)

    struct pfloat_buf * pfloat_buf;         // Print float support structure (in DPRAM)

    uint16_t              cmd_type;           // FGC_FIELDBUS_XXX_CMD (SET, GET, SUB, etc...)
    bool                new_cmd_f;          // New command starting - first packet expected
    bool                end_cmd_f;          // End of last command packet reached

    uint16_t              getopts;            // Get option flags
    uint16_t              n_prop_name_lvls;   // Number of levels in the property name
    uint16_t              si_idx;             // Index of next free element in .sym_idxs[]
    uint16_t              sym_idxs[FGC_MAX_PROP_DEPTH];   // Parent property names
    uint16_t              line;               // elements in line counter (get)
    uint16_t              linelen;            // elements per line (get)

    uint16_t              n_token_chars;      // Length of token in buffer
    uint16_t              token_idx;          // Position in token buffer
    char                token[MAX_CMD_TOKEN_LEN+1]; // Token buffer
    char                token_delim;        // Token delimiter character

    uint16_t              pars_idx;           // Position in pars_buf results array
    uint16_t              device_par_err_idx; // Element array when parsing error occurred
    uint16_t              n_pars;             // Total number of parameters specified in the command
    bool                last_par_f;         // Last parameter flag
    uint16_t              n_arr_spec;         // Number of array range specifiers (0-2)
    prop_size_t         from;               // array range specifier
    prop_size_t         to;                 // array range specifier
    uint16_t              step;               // array step specifier
    uint16_t              changed_f;          // Property changed flag
    uint16_t              mux_idx;            // Multiplexor index (user idx or sub_dev_idx)
    uint16_t              transaction_id;     //!< Transaction ID

    uint16_t              evt_log_state;      // State machine to control event logging
    uint16_t              evt_log_idx;        // Index for event logging commands
    struct log_evt_rec  evt_rec;            // Set command event log entry buffer

    bool                write_nvs_f;        // Flag to indicate if the property data must also be written in NVS (used in PropBlkSet)
};

//-----------------------------------------------------------------------------------------------------------

CMD_VARS_EXT struct cmd fcm;                // Fieldbus task command structure
CMD_VARS_EXT struct cmd tcm;                // Serial communications task command structure
CMD_VARS_EXT struct cmd pcm;                // Publication task command structure

//-----------------------------------------------------------------------------------------------------------

void            CmdTsk                  (void *init_par);
uint16_t          CmdNextCh               (struct cmd *c, char *ch_p);
uint16_t          CmdNextCmdPkt           (struct cmd *c);
uint16_t          CmdParentGet            (struct cmd *c, struct prop *p, uint16_t filter);
uint16_t          CmdGetChangedProps      (void);
uint16_t          CmdPrepareGet(struct cmd * const c, struct prop * const p, uint16_t linelen, prop_size_t *n_els_ptr);
void            CmdStartBin             (struct cmd *c, struct prop *p, uint32_t n_bytes);
void            CmdPrintTimestamp       (struct cmd *c, struct prop *p);
void            CmdPrintLabel           (struct cmd *c, struct prop *p);
uint16_t          CmdPrintIdx             (struct cmd *c, prop_size_t idx);
char * FAR      CmdPrintSymLst          (struct sym_lst *sym_lst, uint16_t value);
void            CmdPrintBitMask         (FILE *f, struct sym_lst *sym_lst, uint16_t value);
void            CmdWaitUntil            (uint32_t unix_time, uint16_t ms_time);
uint16_t          CmdPrintfDsp            (struct cmd *c, enum FGC_float_fmt format_idx, FP32 value);

//-----------------------------------------------------------------------------------------------------------

#endif  // CMD_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cmd.h
\*---------------------------------------------------------------------------------------------------------*/
