/*---------------------------------------------------------------------------------------------------------*\
  File:         startmain.h

  Purpose:      FGC2

  History:

    14 July 2011        doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef STARTMAIN_H     // header encapsulation
#define STARTMAIN_H

void    StartMain               (void);

#endif  // STARTMAIN_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: startmain.h
\*---------------------------------------------------------------------------------------------------------*/
