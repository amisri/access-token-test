/*---------------------------------------------------------------------------------------------------------*\
  File:         trap.h

  Purpose:      FGC2

  History:

    14 July 2011        doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef TRAP_H  // header encapsulation
#define TRAP_H

#ifdef TRAP_GLOBALS
    #define TRAP_VARS_EXT
#else
    #define TRAP_VARS_EXT extern
#endif
//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <cc_types.h>           // basic typedefs

/*----- Constants -----*/

                                                // FIP  FGC  PSU  VS   DCCT PIC
                                                // 001  004  010  040  100  400    RED LEDS
#define PANIC_OTHER             0x0540          //  -    -    -    -    *    *
#define PANIC_BRKPNT            0x0540          //  -    -    -    -    *    *
#define PANIC_BUS_ERR           0x0540          //  -    -    -    *    *    *
#define PANIC_ILEGAL_INS        0x0550          //  -    -    *    *    *    *


/*----- function definitions -----*/

void        IsrBusError             (void);
void        IsrTrap                 (void);
void        TrapTaskStackUsage      (uint16_t tid, uint8_t *usage);
void        Panic                   (uint16_t leds);

/*----- Global variables -----*/

TRAP_VARS_EXT    uint16_t                  buserr_chk;             // Set when bus error expected

//-----------------------------------------------------------------------------------------------------------

#endif  // TRAP_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: trap.h
\*---------------------------------------------------------------------------------------------------------*/
