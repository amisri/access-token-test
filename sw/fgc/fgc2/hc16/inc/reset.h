/*---------------------------------------------------------------------------------------------------------*\
  File:         reset.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef RESET_H // header encapsulation
#define RESET_H

void    ResetPeripherals        (void);

#endif  // RESET_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: reset.h
\*---------------------------------------------------------------------------------------------------------*/
