/*---------------------------------------------------------------------------------------------------------*\
  File:         isr.h

  Purpose:      FGC HC16 Software - Interrupt Service Routines

  Notes:        The FGC HC16 software has two ISRs to catch the two user interrupts:

                    1. IsrFbs()         MicroFIP interrupt via GPT/IC1
                    2. IsrMst()         Millisecond Tick Interrupt from GPT/OC2

                Both interrupts come from the GPT, IsrMst from OC2 and IsrFbs from IC1.  All
                GPT interrupts have the same priority, so interrupts cannot be nested.

                Both ISRs are written in assembler (unfortunately) to improve performance.

  Test Points:  Normally TP1 is used for IsrFbs() and TP2 for IsrMst().  If you disable this using
                the constant below (set to 0) then you can use the test points elsewhere in the code
                using:
                                TP_SET(1); or TP_SET(2);
                                TP_RST(1); or TP_RST(2);
                                TP_TGL(1); or TP_TGL(2);
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ISR_H      // header encapsulation
#define ISR_H

#ifdef ISR_GLOBALS
    #define ISR_VARS_EXT
#else
    #define ISR_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

void    IsrMst                  (void);
void    IsrFbs                  (void);

//-----------------------------------------------------------------------------------------------------------

#endif  // ISR_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.h
\*---------------------------------------------------------------------------------------------------------*/
