/*---------------------------------------------------------------------------------------------------------*\
  File:         start.h

  Purpose:      FGC HC16 Software - Start up, Restart, Reset, Crash, DSP & NvSram functions

  Author:       Hiware, Quentin.King@cern.ch

  Notes:        This file contains the function _Startup().  It assumes that the program uses the
                Medium Memory Model and that chipselects are programmed either by the Boot program
                or the BDI interface.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef START_H      // header encapsulation
#define START_H

#ifdef START_GLOBALS
    #define START_VARS_EXT
#else
    #define START_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

void    _Startup                (void);
void    Reset                   (void);
void    Crash                   (uint16_t e);
void    LoadProgramIntoDsp      (void);
void    StartRunningDsp         (uint32_t delay);
void    WaitForDspStarted       (void);

//-----------------------------------------------------------------------------------------------------------

#endif  // START_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: start.h
\*---------------------------------------------------------------------------------------------------------*/
