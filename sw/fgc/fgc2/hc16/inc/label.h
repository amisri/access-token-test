/*---------------------------------------------------------------------------------------------------------*\
  File:     label.h

  Purpose:  FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef LABEL_H // header encapsulation
#define LABEL_H

#ifdef LABEL_GLOBALS
    #define LABEL_VARS_EXT
#else
    #define LABEL_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>
#include <memmap_mcu.h>         // Include memory map consts
#include <defconst.h>           // for FGC_FLT_, FGC_LAT_
#include <definfo.h>            // for FGC_CLASS_ID
#if FGC_CLASS_ID == 50
    #include <c32.h>            // for C32_STATUS_
#endif

//-----------------------------------------------------------------------------------------------------------

struct sym_name
{
    uint16_t          type;               // Type index
    char * FAR      label;              // Far pointer to label (string constant)
};
// when using this structure in arrays
// remember to add at the end one line filled with 0s
// as the label part == 0  marks the end of the table for several functions like TypeLabel()

//-----------------------------------------------------------------------------------------------------------

void    BitLabel        (struct sym_name const * types, uint16_t bit_mask);
char *  FAR TypeLabel   (struct sym_name const * types, uint16_t type);

//-----------------------------------------------------------------------------------------------------------

// don't use const for this as Q.K. need it near (so in RAM), with const will be far and will crash H16 code
LABEL_VARS_EXT struct sym_name              warnings_lbl[]
#ifdef LABEL_GLOBALS
= {
    {  FGC_WRN_FGC_HW,          "FGC_HW"        },
    {  FGC_WRN_FGC_PSU,         "FGC_PSU"       },
#if (FGC_CLASS_ID == 50) || (FGC_CLASS_ID == 51)
    {  FGC_WRN_DCCT_PSU,        "DCCT_PSU"      },
#endif
    { 0 } // the label part == 0  marks the end of the table
}
#endif
;

// don't use const for this as Q.K. need it near (so in RAM), with const will be far and will crash H16 code
LABEL_VARS_EXT struct sym_name              faults_lbl[]
#ifdef LABEL_GLOBALS
= {
    {  FGC_FLT_FGC_HW,          "FGC_HW"        },
    {  FGC_FLT_FGC_STATE,       "FGC_STATE"     },
    { 0 } // the label part == 0  marks the end of the table
}
#endif
;

LABEL_VARS_EXT struct sym_name          reset_reg_lbl[]
#ifdef LABEL_GLOBALS
= {
    {  CPU_RESET_SRC_POWER_MASK16,      "POWER CYCLE"           },
    {  CPU_RESET_SRC_PROGRAM_MASK16,    "SOFTWARE"              },
    {  CPU_RESET_SRC_MANUAL_MASK16,     "PUSH BUTTON"           },
    {  CPU_RESET_SRC_FASTWD_MASK16,     "FAST WATCHDOG"         },
    {  CPU_RESET_SRC_SLOWWD_MASK16,     "SLOW WATCHDOG"         },
    {  CPU_RESET_SRC_HC16RAM_MASK16,    "HC16 MEM FAULT"        },
    {  CPU_RESET_SRC_C32RAM_MASK16,     "C32 MEM FAULT"         },
    { 0 }
}
#endif
;

LABEL_VARS_EXT struct sym_name          c32_status_lbl[]
#ifdef LABEL_GLOBALS
= {
    {  CPU_MODEH_C32IRQ_0_MASK8,        "IRQ0 FAILED"           },
    {  CPU_MODEH_C32IRQ_1_MASK8,        "IRQ1 FAILED"           },
    {  CPU_MODEH_C32IRQ_2_MASK8,        "IRQ2 FAILED"           },
    {  CPU_MODEH_C32IRQ_3_MASK8,        "IRQ3 FAILED"           },
    {  C32_STATUS_HC16_SBE,             "HC16 SBE"              },
    {  C32_STATUS_C32_SBE,              "C32 SBE"               },
    {  C32_STATUS_PATTERN_FAULT,        "PATTERN FAULT"         },
    {  C32_STATUS_ADDRESS_FAULT,        "ADDRESS FAULT"         },
    {  C32_STATUS_FAILED_TO_START,      "FAILED TO START"       },
    {  C32_STATUS_BG_FAILED,            "BG LOOP FAILED"        },
    {  C32_STATUS_MEM_FAULT,            "MEM FAULT"             },
    {  C32_STATUS_STANDALONE,           "STANDALONE"            },
    { 0 }
}
#endif
;

LABEL_VARS_EXT const struct sym_name        net_type_lbl[]
#ifdef LABEL_GLOBALS
= {
    { 0,    "FIP 2"     },
    { 1,    "ETHERNET"  },
    { 0                 }  // the label part == 0  marks the end of the table
}
#endif
;

LABEL_VARS_EXT  struct sym_name         reset_source_lbl[]
#ifdef FGC_GLOBALS
= {
    {  CPU_RESET_SRC_POWER_MASK16,      "POWER CYCLE"           },
    {  CPU_RESET_SRC_PROGRAM_MASK16,    "SOFTWARE"              },
    {  CPU_RESET_SRC_MANUAL_MASK16,     "PUSH BUTTON"           },
    {  CPU_RESET_SRC_FASTWD_MASK16,     "FAST WATCHDOG"         },
    {  CPU_RESET_SRC_SLOWWD_MASK16,     "SLOW WATCHDOG"         },
    {  CPU_RESET_SRC_HC16RAM_MASK16,    "HC16 MEM FAULT"        },
    {  CPU_RESET_SRC_C32RAM_MASK16,     "C32 MEM FAULT"         },
    { 0 }
}
#endif
;


#if FGC_CLASS_ID == 50
    LABEL_VARS_EXT      struct sym_name     st_latched_lbl[]
    #ifdef FGC_GLOBALS
    = {
        {  FGC_LAT_VDC_FAIL,                "VDC_FAIL"              },
        {  FGC_LAT_PSU_V_FAIL,              "PSU_V_FAIL"            },
        {  FGC_LAT_DIM_SYNC_FLT,            "DIM_SYNC_FLT"          },
        {  FGC_LAT_FBS_FLT,                 "FBS_FLT"               },
        {  FGC_LAT_DSP_FLT,                 "DSP_FLT"               },
        {  FGC_LAT_DIG_FLT,                 "DIG_FLT"               },
        {  FGC_LAT_ANA_FLT,                 "ANA_FLT"               },
        {  FGC_LAT_RFC_FLT,                 "RFC_FLT"               },
        {  FGC_LAT_MEM_FLT,                 "MEM_FLT"               },
        {  FGC_LAT_CODE_FLT,                "CODE_FLT"              },
        {  FGC_LAT_FGC_PSU_FAIL,            "FGC_PSU_FAIL"          },
        {  FGC_LAT_DAC_FLT,                 "DAC_FLT"               },
        {  FGC_LAT_DALLAS_FLT,              "DALLAS_FLT"            },
        {  FGC_LAT_DCCT_PSU_FAIL,           "DCCT_PSU_FAIL"         },
        {  FGC_LAT_DCCT_PSU_STOP,           "DCCT_PSU_STOP"         },
        { 0 }
    }
    #endif
    ;
#endif

//-----------------------------------------------------------------------------------------------------------

#endif  // LABEL_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: label.h
\*---------------------------------------------------------------------------------------------------------*/
