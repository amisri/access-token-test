/*---------------------------------------------------------------------------------------------------------*\
  File:     codes_holder.h

  Purpose:  FGC3

  History:

    28 jun 2010 doc Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CODES_HOLDER_H  // header encapsulation
#define CODES_HOLDER_H

#ifdef CODES_HOLDER_GLOBALS
#define CODES_HOLDER_VARS_EXT
#else
#define CODES_HOLDER_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <memmap_mcu.h>         // Include memory map consts, SRAM_TMPCODEBUF_32
#include <fgc_consts_gen.h>         // Global FGC constants
#include <fgc_codes_gen.h>      // Global FGC constants
#include <defconst.h>
// defconst.h is needed by fgc_fip.h because it includes fgc_code.h that needs FGC_MAX_DEVS_PER_GW
#include <fgc_code.h>           // code communication structures, for struct fgc_dev_name
#include <label.h>              // for struct sym_name
#include <m16c62_link.h>        // for C62_BLK3_ADDR, C62_BLK456_ADDR, C62_BLK1_ADDR

//-----------------------------------------------------------------------------------------------------------

#define CODE_STATE_UNKNOWN          0
#define CODE_STATE_DEV_FAILED       1
#define CODE_STATE_CORRUPTED        2
#define CODE_STATE_NOT_AVL          3
#define CODE_STATE_VALID            4

#define ADV_CODE_STATE_NOT_IN_USE   0
#define ADV_CODE_STATE_OK           1
#define ADV_CODE_STATE_UNKNOWN_CODE 2
#define ADV_CODE_STATE_BAD_LEN      3

#define UPDATE_STATE_OK         0
#define UPDATE_STATE_UPDATE     1
#define UPDATE_STATE_LOCKED     2
#define UPDATE_STATE_NOT_AVL    3

// Databases (this is only for the MainProg

#define SYSDB                       ( (struct fgc_sysdb *  FAR) CODES_SYSDB_SYSDB_32 )
#define COMPDB                      ( (struct fgc_compdb * FAR) CODES_COMPDB_COMPDB_32 )
#define DIMDB                       ( (void * FAR) CODES_DIMDB_DIMDB_32 )                   // DIMDB now points to the beginning of the DimDb code

//-----------------------------------------------------------------------------------------------------------

// this is the data structured where we organise the information received via the FIP (in time_var)
// from the gateway about "codes"
struct code_block
{
    uint8_t       class_id;
    uint8_t       code_id;
    uint16_t      block_idx;
    uint16_t      code[(FGC_CODE_BLK_SIZE / 2)];
};


struct advertised_code_info
{
    uint8_t     code_id;            // Code ID
    uint8_t     class_id;           // FGC class ID
    uint16_t    len_blks;           // not used at FGC3 boot
    uint32_t    version;            // Version (unixtime)
    uint16_t    old_version;        // Version used for the old versioning system
    uint16_t    crc;
};

// this is a small subset from enum fgc_code_ids (customised for the FGC3 boot)
// warning!!! changes here affects the pre filled variables codes_holder_info[] (in this file) and
// erase_dev[], program_dev[], write_dev[], check_dev[] (in codes_fgc.h)

enum fgc_code_ids_short_list
{
    SHORT_LIST_CODE_MP_DSP, // MP blocks 0-6  (Main programs)
    SHORT_LIST_CODE_BTB,    // BB blocks 0-1  (Boot B - second copy of boot)
    SHORT_LIST_CODE_DIMDB,  // BB block 2     (DimDB)
    SHORT_LIST_CODE_COMPDB, // BB block 3     (CompDB)
    SHORT_LIST_CODE_NAMEDB, // BB block 5     (NameDB)
    SHORT_LIST_CODE_SYSDB,  // BB block 6     (SysDB)
    SHORT_LIST_CODE_BTA,    // BA blocks 0-1  (Boot A - first copy of boot)
    SHORT_LIST_CODE_PTDB,   // C62 block 1    (PTDB)
    SHORT_LIST_CODE_IDPROG, // C62 block 3    (IdProg)
    SHORT_LIST_CODE_IDDB,   // C62 blocks 4-6 (IDDB)
    SHORT_LIST_CODE_LAST    // dummy to have the number of elements to use in the for/while loops
};


struct TCodesHolderInfo
{
    uint16_t              len_blks;       // Flash size (blocks)
    uint32_t              base_addr;      // Flash base address
    struct advertised_code_info update; // Update code info (from Advertised Code table)
    char * FAR          label;          // Flash label, max 7 chars, as used in MenuCodesShowInstalled
    uint8_t               locked_f;       // Code locked flag - 1 byte because FGC2 FAR pointer is 3 bytes
    uint16_t              read_only_f;    // Read only flag
    struct fgc_code_info dev;           // Installed code info
    uint16_t              update_state;
    uint16_t              dev_state;      // State of installed code
    uint16_t              calc_crc;       // not used at FGC3 boot
};

//-----------------------------------------------------------------------------------------------------------

// this variable MUST reside in RAM as it is used by the code to flash the boot, resident in RAM
CODES_HOLDER_VARS_EXT struct TCodesHolderInfo codes_holder_info[SHORT_LIST_CODE_LAST]
#ifdef CODES_HOLDER_GLOBALS
        =
{
    { FGC_CODE_51_MAINPROG_SIZE_BLKS,     CODES_FLASH_SA0_32,     { 0                     }, "MP"      },   // Completed by CheckSlot5Registers()
    { FGC_CODE_50_BOOTPROG_SIZE_BLKS,     CODES_FLASH_SA0_32,     { FGC_CODE_50_BOOTPROG  }, "BB_01"   },
    { FGC_CODE_50_DIMDB_SIZE_BLKS,        CODES_FLASH_SA2_32,     { FGC_CODE_50_DIMDB     }, "BB_2"    },
    { FGC_CODE_50_COMPDB_SIZE_BLKS,       CODES_FLASH_SA3_32,     { FGC_CODE_50_COMPDB    }, "BB_3"    },
    { FGC_CODE_NAMEDB_SIZE_BLKS,          CODES_FLASH_SA5_32,     { FGC_CODE_NAMEDB       }, "BB_5"    },
    { FGC_CODE_50_SYSDB_SIZE_BLKS,        CODES_FLASH_SA6_32,     { FGC_CODE_50_SYSDB     }, "BB_6"    },
    { FGC_CODE_50_BOOTPROG_SIZE_BLKS,     CODES_FLASH_SA0_32,     { FGC_CODE_50_BOOTPROG  }, "BA_01"   },
    { FGC_CODE_50_PTDB_SIZE_BLKS,         C62_BLK1_ADDR,          { FGC_CODE_50_PTDB      }, "C62_1"   },
    { FGC_CODE_31_IDPROG_SIZE_BLKS,       C62_BLK3_ADDR,          { FGC_CODE_31_IDPROG    }, "C62_3"   },
    { FGC_CODE_50_IDDB_SIZE_BLKS,         C62_BLK456_ADDR,        { FGC_CODE_50_IDDB      }, "C62_456" },
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

// don't use const for this as FGC2 need it near (so in RAM), with const will be far and will crash H16 code
CODES_HOLDER_VARS_EXT struct sym_name       code_state[]
#ifdef CODES_HOLDER_GLOBALS
        =
{
    {  CODE_STATE_UNKNOWN,      "Unknown"       },
    {  CODE_STATE_DEV_FAILED,   "Dev failed"    },
    {  CODE_STATE_CORRUPTED,    "Corrupted"     },
    {  CODE_STATE_NOT_AVL,      "Not avail."    },
    {  CODE_STATE_VALID,        "Valid"         },
    { 0 } // the label part == 0  marks the end of the table
}
#endif
;

// don't use const for this as FGC2 need it near (so in RAM), with const will be far and will crash H16 code
CODES_HOLDER_VARS_EXT struct sym_name       update_state[]
#ifdef CODES_HOLDER_GLOBALS
        =
{
    {  UPDATE_STATE_OK,         "OK"        },
    {  UPDATE_STATE_UPDATE,     "UPDATE"    },
    {  UPDATE_STATE_LOCKED,     "LOCKED"    },
    {  UPDATE_STATE_NOT_AVL,    "NOT_AVL"   },
    { 0 } // the label part == 0  marks the end of the table
}
#endif
;

// don't use const for this as FGC2 need it near (so in RAM), with const will be far and will crash H16 code
CODES_HOLDER_VARS_EXT struct sym_name       adv_code_state[]
#ifdef CODES_HOLDER_GLOBALS
        =
{
    {  ADV_CODE_STATE_NOT_IN_USE,   "Not in use"        },
    {  ADV_CODE_STATE_OK,           "OK"                },
    {  ADV_CODE_STATE_UNKNOWN_CODE, "Unknown code ID"   },
    {  ADV_CODE_STATE_BAD_LEN,      "Bad code len"      },
    { 0 } // the label part == 0  marks the end of the table
}
#endif
;

CODES_HOLDER_VARS_EXT struct fgc_dev_name   dev_name;       // Extracted from NameDB
CODES_HOLDER_VARS_EXT struct fgc_dev_name   gw_name;        // Extracted from NameDB

//-----------------------------------------------------------------------------------------------------------

#endif  // CODES_HOLDER_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_holder.h
\*---------------------------------------------------------------------------------------------------------*/
