/*---------------------------------------------------------------------------------------------------------*\
 File:      pll_main.h

 Purpose:   FGC MCU Software - RT Clock Phase Locked Loop Functions.

 Author:    Quentin.King@cern.ch

 Notes:     This file contains the functions to support synchronisation of the HC16 GPT clock with
            the WorldFIP cycle (driven by GPS time).

            Units:  tic HC16 GPT_TCNT clock period.  The clock prescalar is 4, so the
                    GPT_TCNT clock frequency is ~16MHz/4, and therefore 1 tic = ~0.25 us.

            qtic    tic/65536 = ~3.8 ps.  Many PLL variables are in qtics.
            qtic variables are long words with the most significant word being tics.

\*---------------------------------------------------------------------------------------------------------*/

#ifndef PLL_MAIN_H      // header encapsulation
#define PLL_MAIN_H

#ifdef PLL_MAIN_GLOBALS
    #define PLL_MAIN_VARS_EXT
#else
    #define PLL_MAIN_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>

#include <cc_types.h>

//-----------------------------------------------------------------------------------------------------------

// All constants below are in units of 2 MHz (see PLD_MASTER_CLOCK_KHZ defined in mcu_dsp_common.h)

#define PLL_DT_REF_TICS                 7100            // 3.55 ms
#define PLL_HIGH_FAST_SLEW              10000
#define PLL_LOW_FAST_SLEW               6000
#define PLL_LOCKED_RANGE                100
#define PLL_CAPTURE_RANGE               3000
#define PLL_FAST_SLEW_RATE              (100L<<16)      // Slew at +50us/ms (5% slower clock)

#define PLL_N_MISSED_FAIL               3

//-----------------------------------------------------------------------------------------------------------

struct pll_vars
{
    bool                sync_f;                         // Local copy of fbs.sync.tic0_f (sync received flag)
    uint32_t              dT;                             // dT = t.ms_t0 - t.fip_t0 = offset between clocks
    int32_t              dT_ref_tics;                    // dT reference
    int32_t              Terr;                           // ref - dT
    uint32_t              qtics_per_ms;                   // RT clock period (used by IsrMst)
    uint32_t              int_qtics_per_ms;               // Integrator for RT clock period
    int16_t              int_qtics_error;                // int_qtics_per_ms - (PLD_MASTER_CLOCK_KHZ << 16)
    uint16_t              n_missed;                       // Number of consecutive missed synchs
    uint16_t              num_unlocks;                    // Number of times PLL unlocks
    uint16_t              frac_counter;                   // PLL fraction counter for ISR
};

//-----------------------------------------------------------------------------------------------------------

void    PllInit                 (void);
void    PllSync                 (void);
void    PllFAILED               (void);
void    PllNO_SYNC              (void);
void    PllFAST_SLEW            (void);
void    PllCAPTURE              (void);

//-----------------------------------------------------------------------------------------------------------

PLL_MAIN_VARS_EXT struct pll_vars        pll;                    // PLL global structure

//-----------------------------------------------------------------------------------------------------------

#endif  // PLL_MAIN_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pll_main.h
\*---------------------------------------------------------------------------------------------------------*/
