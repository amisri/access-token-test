/*---------------------------------------------------------------------------------------------------------*\
  File:         mst.h

  Purpose:      FGC MCU Software - millisecond task function

  Author:       Quentin.King@cern.ch

  Notes:        Activity alignment:

                Class 51:

                0       FipRcvTimeVar   DiagTermNext       DiagSciStart/200ms
                1       Get DSP data    PrepareStatVar    WriteStatVar
                2       StaTsk
                3       FbsProcessTimeVar     RtdTsk/100ms
                4       PLL
                5       LogIearth
                6       LogIreg
                7       StaTsk
                8       LogIleads/1000ms
                9       LogThour/10s
                10      TskTiming/100ms DlsTsk/100ms
                11
                12      StaTsk
                13      PubTsk
                14
                15
                16      LogIreg
                17      StaTsk
                18
                19      PrepareFipMsg

                Class 53:

                NOTE: PrepareStatVar is run on ms 2 for class 53 because Class 53 FGCs should
                never have a FIP ID of less than 10.

                0       FipRcvTimeVar
                1       StaTsk          DiagTermNext     DiagSciStart/200ms
                2       Get DSP data    PrepareStatVar *************************** IMPORTANT NOTE ABOVE
                3       FbsProcessTimeVar RtdTsk/100ms
                4       PLL
                5
                6       StaTsk
                7
                8       LogIleads/1000ms
                9       LogThour/10s
                10      TskTiming/100ms DlsTsk/100ms
                11      StaTsk
                12
                13      PubTsk
                14
                15
                16      StaTsk
                17
                18
                19      PrepareFipMsg

                Class 59:

                0       FipRcvTimeVar   DiagTermNext     DiagSciStart/200ms
                1       Get DSP data    PrepareStatVar  WriteStatVar
                2       StaTsk
                3       FbsProcessTimeVar RtdTsk/100ms
                4       PLL
                5
                6
                7       StaTsk
                8
                9       LogThour/10s
                10      TskTiming/100ms DlsTsk/100ms
                11
                12      StaTsk
                13      PubTsk
                14
                15
                16
                17      StaTsk
                18
                19      PrepareFipMsg
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MST_H      // header encapsulation
#define MST_H

#ifdef MST_GLOBALS
    #define MST_VARS_EXT
#else
    #define MST_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <stdbool.h>
#include <dpcom.h>      // for struct abs_time_us
#include <defconst.h>   // for FGC_N_SD_FLTRS
#include <definfo.h>    // for FGC_PLATFORM_ID

//-----------------------------------------------------------------------------------------------------------

struct mst_vars
{
    uint16_t              tic0;                           // GPT_TOC2 for this ms
    uint16_t              isr_start_us;                   // Starting time of the ISR in us (uses GET_US)
    uint16_t              wd_isr;                         // Watchdog value set by IsrMst
    uint16_t              watchdog;                       // Watchdog value return by MstTsk
    uint16_t              dicoalive;                      // Dicoalive value return by MstTsk
    uint16_t              reset;                          // Copy of reset register (to force 16-bit access only)
    uint16_t              mode;                           // Readback of MODE register
    uint16_t              set_cmd_counter;                // Set command downcounter (to inhibit auto cal)
    uint16_t              ms_mod_5;                       // mstime % 5
    uint16_t              ms_mod_10;                      // mstime % 10
    uint16_t              ms_mod_20;                      // mstime % 20
    uint16_t              ms_mod_100;                     // mstime % 100
    uint16_t              ms_mod_200;                     // mstime % 200
    uint16_t              s_mod_10;                       // unix_time % 10
    uint16_t              s_mod_600;                      // unix_time % 600
    bool                dsp_is_standalone;              // DSP is in standalone mode
    union TUnion32Bits  prev_dsp_ms_irq_alive_counter;  // last used DSP 1ms irq counter
    uint16_t              diff_irq_alive_counter;         // delta of irq_alive_counter during the last millisecond.
    uint16_t              expected_dsp_irqs;              // Expected number of interrupts per millisecond
    bool                dsp_rt_is_alive;                // DSP RT isr running
    bool                dsp_bg_is_alive;                // DSP BG task running
    uint16_t              dsp_reg_run_f;                  // DSP regulation state on the previous iteration (running or not)
    uint16_t              cpu_usage;                      // CPU usage in % (calculated at 1Hz)
    uint16_t              cpu32_usage;                    // CPU usage in % (calculated at 1Hz)
    uint16_t              cpu32_usage_ms;                 // CPU usage in % (max since start of second)
    uint16_t              duration;                       //     MstTsk execution duration (FGC2: in 0.5us ticks; FGC3: in us)
    uint16_t              max_duration[20];               // Max MstTsk execution duration (FGC2: in 0.5us ticks; FGC3: in us)
    struct abs_time_us  time;                           // Timestamp of current millisecond
    struct abs_time_us  prev_time;                      // Timestamp of previous millisecond
    struct abs_time_us  time10ms;                       // Timestamp of last start of 10ms period
};

MST_VARS_EXT struct mst_vars    mst;                    // Millisecond task global structure

//-----------------------------------------------------------------------------------------------------------

void    MstTsk                  (void *);
void    MstTskTiming            (void);

#endif  // MST_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mst.h
\*---------------------------------------------------------------------------------------------------------*/
