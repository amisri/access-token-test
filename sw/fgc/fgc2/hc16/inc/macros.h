/*---------------------------------------------------------------------------------------------------------*\
  File:         macros.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MACROS_H        // header encapsulation
#define MACROS_H

#include <cc_types.h>
#include <definfo.h>            // For FGC_PLATFORM_ID
#include <defconst.h>

#ifdef __HC16__
    #pragma NO_STRING_CONSTR        // Stop #xxx in macro from making a string literal "xxx" (needed for asm)
#endif

//-----------------------------------------------------------------------------------------------------------
// Bit manipulation macros: Set, Clr, Test, TestAll
//

// Set(v,b): in v, set all bits identified by bitmask b

#define Set(v,b)                ( (v) |=  (b) )         // Atomic action on the FGC2

// Clr(v,b): in v, clear all bits identified by bitmask b

#define Clr(v,b)                ( (v) &= ~(b) )

// Implementation notes regarding the Test() macro:
//
//  On the FGC2 MCU, the result of the macro is not a boolean (i.e. 0 or 1). This is for
//  performance reason that we chose not to do the conversion to a boolean, knowing
//  that on HC16 the BOOLEAN type is a 16-bit integer and the macro is used to compare
//  16-bit masks.
//  On the FGC3 MCU, however, the choice was to do an explicit conversion to a boolean
//  (this is the meaning of the double not operator !!). This way the BOOLEAN type
//  can be freely defined and the macro will always work. Here is a code example to
//  appreciate the difference between the two implementations:
//
//      BOOLEAN set_f;                      // Let us say that BOOLEAN is a uint8_t
//      set_f = Test(value, 0x0100);        // set_f = 0 or 1 ?

// Test(v,b): Test if at least one bit in bitmask b is set in v. Returns a BOOLEAN on FGC3, not FGC2.

#define Test(v,b)               ( (v) & (b) )

// TestAll(v,b): Test if all bits in bitmask b are set in v. Returns a BOOLEAN.

#define TestAll(v,b)          ( ( (v) & (b) ) == (b) )

#define ArrayLen(arr)           ( sizeof arr / sizeof arr[0] )


//-----------------------------------------------------------------------------------------------------------

// ToDo: move this to dpcom.h or better don't use the macro
#define DEVICE_PPM                      (uint16_t)dpcom.mcu.device_ppm            // DEVICE PPM flag

// TODO Move the macros below to another header (macros_main.h?)
#if (FGC_CLASS_ID != 50)

// In case of FGC_MAX_USER == 0 the compiler complains about both branches of the conditional statement
// to be equal, hence the second macro definition

/*!
 * Returns number of users available to the device
 */
#if (FGC_MAX_USER != 0)
#define GET_FGC_MAX_USER() (dev.ppm ? FGC_MAX_USER : 0)
#else
#define GET_FGC_MAX_USER() 0
#endif

// In case of FGC_MAX_USER_PLUS_1 == 1 the compiler complains about both branches of the conditional statement
// to be equal, hence the second macro definition

/*!
 * Returns maximum user index for property
 */
#if (FGC_MAX_USER_PLUS_1 != 1)
#define NVS_GET_NUM_PROP_USERS(p) ((Test((p)->flags, PF_PPM)) ? (FGC_MAX_USER_PLUS_1) : 1)
#else
#define NVS_GET_NUM_PROP_USERS(p) 1
#endif

#endif // (FGC_CLASS_ID != 50)

//-----------------------------------------------------------------------------------------------------------

#endif  // MACROS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: macros.h
\*---------------------------------------------------------------------------------------------------------*/
