/*---------------------------------------------------------------------------------------------------------*\
  File:         hash.h

  Contents:     Header file for hash.c - Hashing functions

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef HASH_H
#define HASH_H

/*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <cc_types.h>
#include <definfo.h>

/*---------------------------------------------------------------------------------------------------------*/

typedef uintptr_t HASH_VALUE;                   // needed by defprops.h

// Hash table Structure Declaration

struct hash_entry
{
    uint16_t              sym_idx;                // Symbol index
    HASH_VALUE          value;                  // Pointer to top level property OR getopt mask
    union hash_key
    {
        char * FAR      c;                      // Symbol string (nul padded to 14 characters)
    } key;
    uint8_t               pad;                    // Pad to word boundary (HC16 FAR pointers are 3 bytes)
    struct hash_entry * next;                   // Link to next hash entry
};

/*---------------------------------------------------------------------------------------------------------*/

void    HashInit                (void);
uint8_t   HashInitDIM             (char * key, uint16_t n_dims);
uint16_t  HashFindProp            (char * prop_symbol);
uint16_t  HashFindConst           (char * const_symbol);
uint16_t  HashFindGetopt          (char * getopt_symbol);

//-----------------------------------------------------------------------------------------------------------

#endif  // HASH_H end of header encapsulation

// EOF
