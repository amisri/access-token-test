/*---------------------------------------------------------------------------------------------------------*\
 File:          dls.h

 Purpose:       FGC MCU Software - Dallas bus functions

 Author:        Quentin.King@cern.ch

 Notes:         This file contains the function for the Dallas Task and all the functions related to
                reading the IDs and temperatures from the Dallas devices controlled by the C62
                microcontroller.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DLS_H      // header encapsulation
#define DLS_H

#ifdef DLS_GLOBALS
    #define DLS_VARS_EXT
#else
    #define DLS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>

#include <cc_types.h>           // basic typedefs
#include <property.h>           // for struct prop
#include <defconst.h>           // for FGC_MAX_DIMS, FGC_ID_N_BRANCHES, FGC_MAX_T_PROBES
#include <microlan_1wire.h>     // for MICROLAN_NETWORK_ALL_ELEMENTS_MAX, struct TBranchSummary, struct TMicroLAN_element_summary
#include <definfo.h>            // for FGC_CLASS_ID
#include <property.h>           // for prop_size_t
#include <fgc_consts_gen.h>     // for FGC_C62_ERR_RSP_LEN, FGC_C62_ERR_RSP_LEN, FGC_C62_ERR_RSP_LEN, FGC_C62_ERR_RSP_LEN


/*---------------------------------------------------------------------------------------------------------*/

#define M16C62_MAX_EXPECTED_RESPONSE     12      // Buffer to hold slow (background) command error responses

// dls.state machine constants

enum FGC_dls_state                                   // Use enum to ensure that states are sequential
{
    DLS_START,
    DLS_AT_MAINPROG,
    DLS_READID,
    DLS_CONVTEMPA,
    DLS_READTEMPA,
    DLS_CONVTEMPB,
    DLS_READTEMPB,
    DLS_CONVTEMPL,
    DLS_READTEMPL,
    DLS_LOGTEMPS,
};

// Dallas bus constants

#define DLS_ERROR_LOCATION_10   10
#define DLS_ERROR_LOCATION_20   20
#define DLS_ERROR_LOCATION_30   30
#define DLS_ERROR_LOCATION_39   39
#define DLS_ERROR_LOCATION_40   40
#define DLS_ERROR_LOCATION_60   60      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_70   70      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_80   80      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_90   90      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_100  100      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_110  110      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_120  120      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_130  130      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_140  140
#define DLS_ERROR_LOCATION_150  150      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_160  160      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_170  170      // + logical_path [0..5]
#define DLS_ERROR_LOCATION_180  180      // + logical_path [0..5]


#define REPORT_INVALID_TEMPERATURE      TRUE

//-----------------------------------------------------------------------------------------------------------

// Dallas ID variables

struct id_vars
{
    uint16_t                              n_devs;                         // Number of devices in devs[] array
    uint16_t                              start_in_elements_summary[FGC_ID_N_BRANCHES];// Index of 1st device per branch in dev array
    uint16_t                              to_summary_sorted_by_codebar[MICROLAN_NETWORK_ALL_ELEMENTS_MAX];   // Sorted array to Dallas device information
    struct TBranchSummary               logical_paths_info[FGC_ID_N_BRANCHES];
    struct TMicroLAN_element_summary *  element_info_ptr[FGC_ID_N_BRANCHES];
    struct TMicroLAN_element_summary    devs[MICROLAN_NETWORK_ALL_ELEMENTS_MAX];              // Dallas device information array
    struct prop *                       prop_temp[MICROLAN_NETWORK_ALL_ELEMENTS_MAX];         // Link to temperature properties
};

// Dallas variables

struct Trsp
{
    uint8_t               global[FGC_C62_ERR_RSP_LEN];    // global response (for all branches)
    uint8_t               local [FGC_C62_ERR_RSP_LEN];    // local branch response (logical_path=4)
    uint8_t               meas_a[FGC_C62_ERR_RSP_LEN];    // measurement A branch response (logical_path=1)
    uint8_t               meas_b[FGC_C62_ERR_RSP_LEN];    // measurement B branch response (logical_path=3)
};

struct dls_vars
{
    bool                active_f;                       // Dallas buses read flag
    bool                temp_fail_f;                    // Dallas temperature read failed
    uint16_t              inhibit_f;                      // Dallas temperature read inhibit (DLS.INHIBIT)
    enum FGC_dls_state  state;                          // Dallas communications state
    uint16_t              timeout_cnt;                    // Command timeout down counter
    uint16_t              logical_path;                   // Branch idx of branch been scanned for temps
    uint32_t              num_errors;                     // Error counter
    uint32_t              error_time;                     // Abstime of last error
    struct Trsp         rsp;
};


// Temperature measurement variables

// this temperatures are 16 bits raw value form the sensor
// and are converter to FP32 by GetTemp() that make a conversion when access via a property

struct TTemperatureValues
{                                                       // for the client level and must therefore be size 4
    uint32_t              unix_time;                      // Acquisition unixtime
    bool                thour_f;                        // Acquisition should be logged in THOUR
    bool                tday_f;                         // Acquisition should be logged in THOUR

    struct temperatures_to_logbook                      // Temperature log structure
    {
        uint16_t          fgc_in;                         // FGC inlet temperature
        uint16_t          fgc_out;                        // FGC outlet temperature
        uint16_t          dcct_a;                         // DCCT A electronics temperature (if present)
        uint16_t          dcct_b;                         // DCCT B electronics temperature (if present)
    }log;

// temp.fgc.in and temp.fgc.out are linked with the corresponding temperature sensor
// via the assignment in components.xml with
//  temp_prop = "TEMP.FGC.IN"
// and
//  temp_prop = "TEMP.FGC.OUT"
// (this information is extracted from CompDB by the MCU program)

    struct
    {
        uint16_t          in;                             // FGC inlet temperature
        uint16_t          out;                            // FGC outlet temperature
        uint16_t          delta;                          // Difference between FGC outlet and inlet temps
    }fgc;
#if FGC_CLASS_ID == 59
    // nothing to do
#else
    bool                dcct_temp_available[2];         // DCCT temperature measurements expected flags

    struct
    {
        uint16_t          mod;                            // External ADC modulator temperature
        uint16_t          psu;                            // External ADC PSU temperature
    }ext_adc[2];

    struct
    {
        uint16_t          head;                           // DCCT head temperature
        uint16_t          elec;                           // DCCT electronics temperature
    }dcct[2];
#endif
};

struct barcode_vars
{

    struct
    {
        int8_t * FAR     L         [FGC_MAX_L_BARCODES]; // Local barcodes
#if FGC_CLASS_ID == 59
    // nothing to do
#else
        int8_t * FAR     V         [FGC_MAX_V_BARCODES]; // Voltage source barcodes
        int8_t * FAR     A         [FGC_MAX_A_BARCODES]; // Measurement channel A barcodes
        int8_t * FAR     B         [FGC_MAX_B_BARCODES]; // Measurement channel B barcodes
#endif
    }                   bus;

    struct
    {
        uint8_t           cassette  [FGC_ID_BARCODE_LEN]; // FGC cassette barcode
        uint8_t           ana       [FGC_ID_BARCODE_LEN]; // FGC Analogue Interface board barcode
    }                   fgc;
    struct
    {
        uint8_t           head      [FGC_ID_BARCODE_LEN]; // DCCT head barcode
        uint8_t           elec      [FGC_ID_BARCODE_LEN]; // DCCT electronics barcode
    }                   dcct[2];
    struct
    {
        uint8_t           cassette  [FGC_ID_BARCODE_LEN]; // ADC22 cassette barcode
        uint8_t           elec      [FGC_ID_BARCODE_LEN]; // ADC22 Delta-Sigma modulator barcode
        uint8_t           psu       [FGC_ID_BARCODE_LEN]; // ADC22 PSU barcode
    }                   ext_adc[2];
};



struct barcode_n_els
{
    struct
    {
        prop_size_t     L;                              // Local barcodes
        prop_size_t     V;                              // Voltage source barcodes
        prop_size_t     A;                              // Measurement channel A barcodes
        prop_size_t     B;                              // Measurement channel B barcodes
    } bus;
};

//-----------------------------------------------------------------------------------------------------------

void    DlsTsk                  (void*);
char *  DlsIdString             (union TMicroLAN_unique_serial_code * devid, char *id_string);


//-----------------------------------------------------------------------------------------------------------

DLS_VARS_EXT struct dls_vars            dls;                   // Dallas task variables structure

DLS_VARS_EXT struct id_vars             id;                    // Dallas ID variables structure

DLS_VARS_EXT struct TTemperatureValues  temp;                  // Dallas Temperature variables structure

// used by defprops.h
DLS_VARS_EXT struct barcode_vars        barcode;                // Barcode variables structure

// used by defprops.h
DLS_VARS_EXT struct barcode_n_els       barcode_n_els;          // Barcode property n_els structure

//-----------------------------------------------------------------------------------------------------------

#endif  // DLS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dls.h
\*---------------------------------------------------------------------------------------------------------*/
