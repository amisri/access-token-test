/*---------------------------------------------------------------------------------------------------------*\
 File:          rtd.h

 Purpose:       FGC MCU Software - Real-time display Functions.

 Notes:         Global option line display Layout (DIAG, PSU):

                          1         2         3         4         5         6         7         8
                +---------+---------+---------+---------+---------+---------+---------+---------++
     DIAG   20  |aa:nnnn aa:xnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn |
     PSU    20  |+5: n.nn    +15: nn.nn  -15:-nn.nn                                              |
                +---------+---------+---------+---------+---------+---------+---------+---------++

\*---------------------------------------------------------------------------------------------------------*/

#ifndef RTD_H           // header encapsulation
#define RTD_H

#ifdef RTD_GLOBALS
    #define RTD_VARS_EXT
#else
    #define RTD_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <cc_types.h>   // basic typedefs
#include <term.h>       // for TERM_BOLD
#include <definfo.h>    // for FGC_CLASS_ID

//-----------------------------------------------------------------------------------------------------------

#define RTD_START_POS           "\33" "7\33[%sH" TERM_BOLD      // Save then set cursor position
#define RTD_END                 "\33" "8\v"                     // Restore cursor position
#define RTD_OPTLINE_VPOS        20u

#define RTD_OPTLINE_POS         "20;1"
#define RTD_DIAGDATA_HPOS       1u
#define RTD_DIAGDATA_HWIDTH     8u
#define RTD_PSU_HPOS            1u
#define RTD_PSU_HWIDTH          12u

//-----------------------------------------------------------------------------------------------------------

struct rtd_flags                                        // RTD flag display structure
{
    uint16_t *            var;                            // Pointer to flag variable
    uint16_t              mask;                           // Flag bit mask
    int8_t               flag[2];                        // flag[0]: #=inv on 1 ~=inv on 0 else use flags
    char * FAR          pos;                            // Flag position "yy;xx"
};

struct rtd_vars
{
    uint16_t              enabled;                        // Global enable flag
    uint16_t              ctrl;                           // Option line control
    uint16_t              last_ctrl;                      // Old value of option line control
    FILE *              f;                              // Pointer to RTD output stream
    uint16_t              idx;                            // Field index
    uint16_t              flag_idx;                       // Flag array index
    uint16_t              flag_end_idx;                   // Flag index loop end

    struct rtd_vars_last                                // Last displayed values:
    {
        bool            flag_off[40];                   // Flag is not set
        uint16_t          tout;                           // Outlet temperature
        uint16_t          tin;                            // Inlet temperature
        int16_t          cpu_usage;                      // Last displayed CPU usage
        int16_t          cpu32_usage;                    // Last displayed CPU32 usage
        uint16_t          state_pll;                      // Last displayed PLL State
        uint16_t          state_op;                       // Operational state
    } last;

    struct optline
    {
        uint16_t          idx;                            // Option line index
        uint16_t          diag_list_len;                  // Last value of rtd diagnostic channel list len
        uint16_t          diag_chan[10];                  // Last displayed diag channel
        uint16_t          diag_data[10];                  // Last displayed diag data
        FP32            psu[3];                         // +5V,+15V,-15V PSU floats
        FP32            i_earth;                        // IEARTH as a float
        char            float_s[3][10];                 // Option line float values as strings
    } optline;
};

//-----------------------------------------------------------------------------------------------------------

void RtdTsk                  (void *);
bool RtdInt16s               (int16_t *source, int16_t *last, char * FAR fmt, char * FAR pos);
bool RtdInt32u               (uint32_t *source, uint32_t *last, char * FAR fmt, char * FAR pos);
bool RtdTemp                 (uint16_t temperature, uint16_t * previous_temperature_ptr, char * FAR pos);
bool RtdFloat                (FP32 * source, FP32 * last, char * last_s, char * FAR fmt, char * FAR pos);
bool RtdFlags                (void);
bool RtdTout                 (void);
bool RtdTin                  (void);
bool RtdCpuUsage             (void);
bool RtdCpu32Usage           (void);
bool RtdOptionLine           (void);
void RtdResetOptionLine      (void);
bool RtdDiagList             (void);
bool RtdPsu                  (void);

//-----------------------------------------------------------------------------------------------------------

RTD_VARS_EXT struct rtd_vars        rtd;                    // Real-time display structure

//-----------------------------------------------------------------------------------------------------------

#endif  // RTD_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rtd.h
\*---------------------------------------------------------------------------------------------------------*/
