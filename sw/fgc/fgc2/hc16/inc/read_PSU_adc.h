/*!
 * @file  read_PSU_adc.h
 */

#ifndef READ_PSU_ADC_H
#define READ_PSU_ADC_H

#ifdef READ_PSU_ADC_GLOBALS
    #define READ_PSU_ADC_VARS_EXT
#else
    #define READ_PSU_ADC_VARS_EXT extern
#endif

#include <stdint.h>

#include <cc_types.h>
#include <definfo.h>

/*!
 * This function process the DIPS board digital signals.
 * These are available in diag.data[0x80] and diag.data[0xA0].
 *                       diag.data.r.digital_0[0][0]     diag.data.r.digital_1[0][0]
 *
 * This is a quick initial implementation.
 *
 *                 psu_stat
 *       -----------------------
 *       0x001: PSU_DCCT_STOP
 *       0x002: PSU2_DCCT_WARNING *
 *       0x004: PSU_DCCT_WARNING
 *       0x008: PSU_FGC_STOP
 *       0x010: VDC_FAIL
 *       0x020: PSU_FGC_WARNING
 *       0x040:
 *       0x080:
 *       0x100:        * Initial production of the DCCT PSUs
 *       0x200:          does not include redundancy, so this
 *       0x400:          signal is permanently active.
 *       0x800:
 */
void    CheckPsuStatus          (uint16_t psu_stat);

/*!
 * This function processes the dcct status bits received from the DIPS board and sets the appropriate
 * bits in the published data
 *
 *       0x01:  DCCT_ZERO_I_NOT
 *       0x02:  DCCT_NO_HEAD
 *       0x04: (DCCT_FAULT)  Latched
 *       0x08:  DCCT_TEMP_FLT
 *       0x10:  DCCT_LOW_I_NOT   Not used because it's not calibrated
 *       0x20:  DCCT_MCB_OFF
 *       0x40: (DCCT_ID_FLT) Latched
 *       0x80: (DCCT_CAL_FLT)        Latched
 *
 * The signals in brackets () are not received from the DIAG data and must be preserved.  The mask MCU_DCCT_MASK
 * identifies only the signals received from the DIAG data.
 */
#if FGC_CLASS_ID != 59
uint8_t   CheckDcctStat           (uint16_t dcct_diag_stat, uint8_t st_dcct);
#endif

#endif  // READ_PSU_ADC_H

// EOF
