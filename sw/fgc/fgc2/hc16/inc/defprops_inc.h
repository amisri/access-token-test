/*---------------------------------------------------------------------------------------------------------*\
  File:         defprops_inc.h

  Contents:     this file is needed by defprops.h
\*---------------------------------------------------------------------------------------------------------*/


#ifndef DEFPROPS_INC_H     // header encapsulation
#define DEFPROPS_INC_H

#include <cc_types.h>
#include <definfo.h>            // for FGC_CLASS_ID
#include <hash.h>               // for HASH_VALUE and struct hash_entry
#include <property.h>           // for DEFPROPS_FUNC_SET(), struct prop, struct sym_lst
#include <nvs.h>                // for nvs global variable
#include <fbs.h>                // for fbs global variable
#include <fbs_class.h>          // for FAULTS, WARNINGS, ST_MEAS_A, ST_MEAS_B, ST_DCCT_A, ST_DCCT_B
#include <fip.h>                // for fip global variable
#include <dls.h>                // for barcode_n_els global variable
#include <version.h>            // for version global variable
#include <memmap_mcu.h>
#include <rtd.h>                // for rtd global variable
#include <sta_class.h>          // for vs global variable
#include <dpcls.h>              // for dpcls global variable
#include <sta.h>                // for sta global variable
#include <pll_main.h>           // for pll global variable
#include <diag.h>               // for diag global variable
#include <ref_class.h>          // for ref global variable, EVENT_GROUP
#include <mst.h>                // for mst global variable
#include <get.h>                // for debug global variable
#include <core.h>               // for core global variable
#include <get_class.h>          // for meas global variable
#include <crate.h>              // for crate global variable
#include <log_class.h>          // for log_iab global variable
#include <edac.h>               // for edac global variable
#include <class.h>              // for B_MEAS macro
#include <pops.h>               // for pops global variable on class 53
#if (FGC_CLASS_ID == 51)
#include <transaction.h>
#endif
#if (FGC_CLASS_ID == 59)
#include <init_class.h>         // for rfc data structure
#else
#include <cal_class.h>          // for cal global variable
#include <adc_class.h>          // for adc global variable
#endif
#include <version.h>

#endif  // DEFPROPS_INC_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: defprops_inc.h
\*---------------------------------------------------------------------------------------------------------*/
