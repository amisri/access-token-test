/*!
 * @file init.h
 *
 * @brief FGC MCU Software - initialisation functions that are not linked to other files
 */

#ifndef INIT_H      // header encapsulation
#define INIT_H

#ifdef INIT_GLOBALS
    #define INIT_VARS_EXT
#else
    #define INIT_VARS_EXT extern
#endif

#include <cc_types.h>
#include <property.h>   // for struct prop
#include <cmd.h>        // for struct cmd

struct init_dynflag_timestamp_select
{
    struct prop *               prop;                   // Pointer to property
    enum timestamp_select       timestamp_select;       // dynamic flags
};

/*!
 * @brief Initialises the operation of the MCU
 */
void    InitMCU                                 (void);

/*!
 * @brief Initialises the operation of the RTOS (NanOs)
 */
void    InitOS                                  (void);

/*!
 * @brief Initialises the operation of the M68HC16 General Purpose Timer (GPT)
 */
void    EnableMcuTimersAndInterrupts            (void);

/*!
 * @brief Initialises the operation of the serial communications interface
 */
void    EnableMcuSerialInterface_Terminal       (void);

/*!
 * @brief Initialises the system related data structures based on the device name/type
 *
 * Depending on the mode the function gets the device name from NameDB (FIP-driven) or from the property (stand-alone mode).
 * After that it extracts the type from the name and queries SysDB for it. If it cannot find the type then it tries
 * the same with the type property. This second check is done because of RPH_ converters. RPH_ type is not in the database
 * - users will set the correct type (RPHA, RPHB etc.) manually. If the second query also fails then the type is set
 * to unknown. At the end system structures are initialised.
 */
void    InitSystem                              (void);

/*!
 * @brief Initialises the Spy multiplexer channel property SPY.MPX both on startup and on the command S SPY RESET
 */
void    InitSpyMpx                              (struct cmd * c);

/*!
 * @brief Called from TrmTsk() on startup to set the parent pointers in the property structures. The parent links are needed for subscriptions.
 */
void    InitPropertyTree                        (void);

/*!
 * This function is called for PARENT and child properties.  It is RECURSIVE and scans the property tree to
 * set the parent links so that each property knows it's parent and to count the potential size of data
 * produced by the property using a get function.  This is passed back up so that parent properties can
 * guess the size of data they will produce.  This is used to decide if the publication can be direct or
 * must be via a GET request to the gateway.
 */
uint32_t  InitPropParentLinksScan                 (struct prop * p);

void            InitClassPreInts        (void);
void            InitClassPostInts       (void);
void            InitClassPostSync       (struct cmd * c);
void            InitClassSpyMpx         (struct cmd * c);

#endif  // INIT_H end of header encapsulation

// EOF
