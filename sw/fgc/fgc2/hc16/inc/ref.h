/*---------------------------------------------------------------------------------------------------------*\
  File:         ref.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef REF_H           // header encapsulation
#define REF_H

#ifdef REF_GLOBALS
    #define REF_VARS_EXT
#else
    #define REF_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <stdbool.h>
#include <property.h>   // for struct prop
#include <cmd.h>        // for struct cmd
#include <defconst.h>   // for FGC_SIMSC_LEN, FGC_MAX_USER_PLUS_1

//-----------------------------------------------------------------------------------------------------------

#define NON_PPM_USER                    (0)

// List of states which permit arming a NON-CYCLING function
// - is also -
// List of states which do not permit arming a CYCLING function.

#define RUNNING_FUNC_STATE_MASK         ((1 << FGC_PC_IDLE)         | \
                                         (1 << FGC_PC_ARMED)        | \
                                         (1 << FGC_PC_RUNNING)      | \
                                         (1 << FGC_PC_ABORTING)     | \
                                         (1 << FGC_PC_SLOW_ABORT))

//-----------------------------------------------------------------------------------------------------------

struct simsc
{
    uint16_t              cycles[FGC_SIMSC_LEN];          // Property FGC.CYC.SIM
};

struct ref_vars
{
    uint16_t              reg_mode_init;                  // REG.MODE_INIT property
    uint16_t              event_group;                    // REF.EVENT_GROUP

    union                                               // Union for reference parameters
    {
        FP32            f[5];
        uint32_t          i[5];
    } pars;
};

//-----------------------------------------------------------------------------------------------------------

void     RefEvent              (void);
uint16_t RefArm                (struct cmd *c, uint16_t user, uint32_t func_type, uint16_t stc_func_type);
uint16_t RefPelp               (struct cmd *c);
uint16_t RefPlep               (struct cmd *c);
uint16_t RefOpenloop           (struct cmd *c);
uint16_t RefTrim               (struct cmd *c);
uint16_t RefTest               (struct cmd *c);
uint16_t RefGetPars            (struct cmd *c, struct prop **p, FP32 *pars, uint16_t n_pars);
void     RefSetPars            (struct cmd *c, struct prop **p, FP32 *pars, uint16_t n_pars);
bool     RefGetFuncTypeFromProp(struct prop * property, uint16_t * func_type, uint16_t * stc_func_type);

//-----------------------------------------------------------------------------------------------------------

REF_VARS_EXT struct ref_vars      ref;                    // Reference variables structure
REF_VARS_EXT struct simsc         simsc;

//-----------------------------------------------------------------------------------------------------------

#endif  // REF_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ref.h
\*---------------------------------------------------------------------------------------------------------*/
