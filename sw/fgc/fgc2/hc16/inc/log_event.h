/*---------------------------------------------------------------------------------------------------------*\
  File:         log_event.h

  Contents:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef LOG_EVENT_H
#define LOG_EVENT_H

#ifdef LOG_EVENT_GLOBALS
    #define LOG_EVENT_VARS_EXT
#else
    #define LOG_EVENT_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <fgc_consts_gen.h>         // for FGC_LOG_EVT_PROP_LEN, FGC_LOG_EVT_ACT_LEN
#include <defconst.h>           // for FGC_MAX_DIMS
#include <property.h>           // for struct prop
#include <mcu_dsp_common.h>     // for struct abs_time_us
#include <fgc_consts_gen.h>

//-----------------------------------------------------------------------------------------------------------

struct log_evt_prop
{
    struct prop *       prop;                           // Pointer to property
    uint16_t              old_value;                      // Old property value
    char * FAR          name;                           // Pointer to property name in full
    const char * FAR    symbols[FGC_MAX_SYMLIST_VALUE + 1];              // Table of symbols
};

struct log_evt_rec
{
    struct abs_time_us  timestamp;                      // Timestamp (Unix time + microsecond)
    char                prop[FGC_LOG_EVT_PROP_LEN];     // Property name field
    char                value[FGC_LOG_EVT_VAL_LEN];     // Value field
    char                action[FGC_LOG_EVT_ACT_LEN];    // Action field
};

struct log_evt_vars
{
    uint16_t              index;                          // Index of last sample
    uint16_t              sample_size;                    // Number of bytes per sample
    uint16_t              dim_props_idx;                  // DIM index (0 -> (diag.n_dims-1))
    uint16_t              logical_dim;
    uint16_t              dim_state;                      // DIM event log state
    uint16_t              dim_bank_idx;                   // DIM bank (0-1)
    uint16_t              dim_input_idx;                  // DIM input index (0-11)
    uint16_t              dim_input_mask;                 // DIM input mask (0x001 - 0x800)
    uint16_t              fault_mask[2];                  // DIM bank fault mask
    uint16_t              dim_new_data[2];                // New DIM data for 2 banks
    uint16_t              dim_old_data[2][FGC_MAX_DIMS];  // Old data for 2 banks for all DIMs
    struct fgc_dimdb_bank * FAR dimdb_bank_fp[2];       // Pointer to DimDB bank info
    struct log_evt_rec          dim_log_rec;            // DIM log record
};

//-----------------------------------------------------------------------------------------------------------

LOG_EVENT_VARS_EXT struct log_evt_vars    log_evt;                // Event log variables structure

//-----------------------------------------------------------------------------------------------------------

#endif  // LOG_EVENT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: log_event.h
\*---------------------------------------------------------------------------------------------------------*/
