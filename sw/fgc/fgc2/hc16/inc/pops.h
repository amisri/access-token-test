/*---------------------------------------------------------------------------------------------------------*\
  File:         pops.h

  Contents:     FGC2, class 53

  History:

   01 July 2011         Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef POPS_H  // header encapsulation
#define POPS_H

#ifdef POPS_GLOBALS
    #define POPS_VARS_EXT
#else
    #define POPS_VARS_EXT extern
#endif

/*----- POPS variables -----*/

struct pops_vars                                        // Initialised in log53.c
{
    uint16_t              loggedcontrol;                  // Masked POPS control word (logged in event log)
    uint16_t              status;                         // POPS status word (set in IsrMst())
};

POPS_VARS_EXT struct pops_vars pops;

#endif  // POPS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pops.h
\*---------------------------------------------------------------------------------------------------------*/
