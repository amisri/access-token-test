/*!
 * @file   bgp.h
 *
 * @brief  FGC MCU Software - Background processing Task
 *
 * The background task takes the role of an idle task. It never blocks and is used to do
 * background monitoring of the FGC.
 */

#ifndef BGP_H
#define BGP_H

#ifdef BGP_GLOBALS
    #define BGP_VARS_EXT
#else
    #define BGP_VARS_EXT extern
#endif

// Function declarations

/*!
 * Background processing Task
 *
 * It is the lowest priority task and it never blocks, so it plays the role of an Idle task.
 * It is responsible for background monitoring of the task stack consumption.
 */
void BgpTsk(void * unused);

#endif // BGP_H

// EOF
