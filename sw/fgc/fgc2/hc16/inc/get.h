/*---------------------------------------------------------------------------------------------------------*\
 File:          get.h

 Purpose:       FGC MCU Software - Get Command Functions

 Author:        Quentin.King@cern.ch

 Notes:         This file contains the function for the Get Task and all the property Get functions.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef GET_H      // header encapsulation
#define GET_H

#ifdef GET_GLOBALS
    #define GET_VARS_EXT
#else
    #define GET_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <property.h>   // for struct prop
#include <cmd.h>        // for struct cmd

//-----------------------------------------------------------------------------------------------------------

struct debug_vars
{
    uint32_t              mem[2];                         // DEBUG.MEM control: address and number of words
};

//-----------------------------------------------------------------------------------------------------------

uint16_t  GetFifo         (struct cmd *c, struct prop *p);
uint16_t  GetLogTiming    (struct cmd *c, struct prop *p);
uint16_t  GetComp         (struct cmd *c, uint16_t * list_idx, uint16_t n_found, uint16_t n_req, uint16_t comp_idx);
void    GetPropInfo     (struct cmd *c, struct prop *);
uint16_t  GetPropSize     (struct cmd *c, struct prop *);

//-----------------------------------------------------------------------------------------------------------

GET_VARS_EXT struct debug_vars      debug;                  // Debug variables structure

//-----------------------------------------------------------------------------------------------------------

#endif  // GET_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: get.h
\*---------------------------------------------------------------------------------------------------------*/
