/*---------------------------------------------------------------------------------------------------------*\
  File:         main.h

  Purpose:      FGC MCU Software - contains main()
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MAIN_H      // header encapsulation
#define MAIN_H

#ifdef MAIN_GLOBALS
    #define MAIN_VARS_EXT
#else
    #define MAIN_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

void    main    (void);
void    stop    (void);

//-----------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------

#endif  // MAIN_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
