/*---------------------------------------------------------------------------------------------------------*\
  File:         prop.h

  Purpose:      FGC MCU Software - Property Functions.

  Author:       Quentin.King@cern.ch

  Notes:        This file contains the functions to manipulate the properties and symbols
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PROP_H      // header encapsulation
#define PROP_H

#ifdef PROP_GLOBALS
    #define PROP_VARS_EXT
#else
    #define PROP_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <property.h>   // for struct prop, and typedef prop_size_t
#include <definfo.h>    // for FGC_PLATFORM_ID, FGC_CLASS_ID
#include <cmd.h>        // for struct cmd
#include <defprops.h>
//-----------------------------------------------------------------------------------------------------------

// Valid array index: the following mask is used to test if a property element index is valid or not
// On FGC2 the max property size and therefore the max element index is 65535.
// On FGC3 the max property size and therefore the max element index is 4294967295.

#define PROP_ARRAY_IDX_MASK    ((uint32_t)0xFFFFu)

//-----------------------------------------------------------------------------------------------------------

// PropMap() function arguments

enum prop_map_specifier
{
    PROP_MAP_ALL,
    PROP_MAP_ONLY_PARENTS,
    PROP_MAP_ONLY_CHILDREN
};

typedef void (*prop_map_func)(uint16_t lvl, struct prop *p);

// External function declarations

/*!
 * @brief This function is used to find a property from it's name.
 *
 * This function is used to find a property from it's name.  It uses the pcm command structure in a
 * special way and is separate from the tcm and fcm structures so that it can be used by the Dallas task
 * in parallel with FIP and serial command processing.  It is also used during startup to to find non-volatile
 * storage properties.
 *
 * Output parameters (if successful):
 *
 *       pcm.prop                Pointer to identified property
 *       pcm.n_arr_spec          Number of array specifiers (0,1,2)
 *       pcm.from                Array start index
 *       pcm.to                  Array end index specifier
 *       pcm.sym_idxs            Array of symbol indexes for parent properties leading to identified property
 *       pcm.si_idx              Number of entries in c->sym_idxs
 *       pcm.n_prop_name_lvls    Number of entries in c->sym_idxs (this is number of levels in property name)
 *
 * @param property_name Far pointer to the nvs property name.
 *
 * @retval 0                       Successful match
 * @retval FGC_NO_DELIMITER        End of buffer found before a valid delimiter
 * @retval FGC_SYNTAX_ERROR        Token buffer full or contains a space before a valid delimiter
 * @retval FGC_NO_SYMBOL           No symbol found
 * @retval FGC_UNKNOWN_SYM         Token didn't match any known property symbol
 * @retval FGC_BAD_ARRAY_IDX       The array specifier has a bad syntax or values
 */
uint16_t      PropFind                (char        *propname);

/*!
 * @brief Far variant of PropFind function
 *
 * The function uses an intermediate storage for the property name. This allows to pass a far pointer directly to the function.
 * Casting a far pointer to a near one (using PropFind) results in reading the current page instead of the one with nvs.
 *
 * @see PropFind
 *
 * @param property_name Far pointer to the nvs property name.
 *
 * @retval 0                       Successful match
 * @retval FGC_NO_DELIMITER        End of buffer found before a valid delimiter
 * @retval FGC_SYNTAX_ERROR        Token buffer full or contains a space before a valid delimiter
 * @retval FGC_NO_SYMBOL           No symbol found
 * @retval FGC_UNKNOWN_SYM         Token didn't match any known property symbol
 * @retval FGC_BAD_ARRAY_IDX       The array specifier has a bad syntax or values
 */
uint16_t      PropFindFar             (char * FAR   property_name);


uint16_t      PropIdentifyProperty    (struct cmd  *c);
uint16_t      PropIdentifySymbol      (struct cmd  *c, char * FAR delim, uint16_t sym_type, uint16_t *sym);
uint16_t      PropIdentifyArray       (struct cmd  *c);
uint16_t      PropIdentifyUser        (struct cmd  *c, uint16_t user_from_header);
uint16_t      PropIdentifyGetOptions  (struct cmd  *c, struct prop *p);
prop_size_t PropGetNumEls           (const struct cmd * const c, const struct prop * const p);

void        PropSetNumEls           (struct cmd  *c,
                                     struct prop *p,
                                     prop_size_t  n_elements);

void        PropSet                 (struct cmd  *c,
                                     struct prop *p,
                                     void        *value,
                                     prop_size_t  n_elements);

void        PropSetFar              (struct cmd  *c,
                                     struct prop *p,
                                     void   * FAR value,
                                     prop_size_t  n_elements);

uint16_t      PropValueGet            (struct cmd  *c,
                                     struct prop *p,
                                     bool        *set_f,
                                     prop_size_t  el_idx,
                                     uint8_t      **data_ptr);

uint16_t      PropBlkGet              (struct cmd  *c,
                                     prop_size_t *n_elems_ptr);

uint16_t      PropBlkSet              (struct cmd  *c,
                                     prop_size_t  n_elements,
                                     bool         set_f,
                                     uint16_t       last_blk_f);

uint16_t      PropBufWait             (struct cmd  *c);

void        PropMap                 (prop_map_func           func,
                                     enum prop_map_specifier specifier);

//-----------------------------------------------------------------------------------------------------------

#endif  // PROP_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: prop.h
\*---------------------------------------------------------------------------------------------------------*/
