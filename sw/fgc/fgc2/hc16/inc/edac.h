/*---------------------------------------------------------------------------------------------------------*\
  File:         edac.h

  Contents:     FGC2

  History:

   30 jun 11    doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef EDAC_H  // header encapsulation
#define EDAC_H

#ifdef EDAC_GLOBALS
    #define EDAC_VARS_EXT
#else
    #define EDAC_VARS_EXT extern
#endif
/*----- Error Detection and Correction Variables ----*/

struct edac_vars
{
    uint32_t              error_time;                     // Unix time of last EDAC increment
    uint16_t              num_errors;                     // EDACS single bit errors counter
    uint16_t              last_num_errors;                // Last value of EDACS single bit errors counter
    uint8_t               last_gpt_pacnt;                 // Last value of GPT pulse accumulator counter
};

EDAC_VARS_EXT struct edac_vars       edac;                   // Edac global structure




#endif  // EDAC_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: edac.h
\*---------------------------------------------------------------------------------------------------------*/
