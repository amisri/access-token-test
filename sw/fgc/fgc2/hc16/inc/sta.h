/*---------------------------------------------------------------------------------------------------------*\
 File:          sta.h

 Purpose:

 Author:        Quentin.King@cern.ch

 Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef STA_H      // header encapsulation
#define STA_H

#ifdef STA_GLOBALS
    #define STA_VARS_EXT
#else
    #define STA_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>                   // basic typedefs
#include <definfo.h>                    // for FGC_CLASS_ID
#include <mcu_dsp_common.h>             // For struct abs_time_us/ms
#include <defconst.h>                   // for FGC_N_ADCS

//-----------------------------------------------------------------------------------------------------------

/*----- State Task Variables -----*/

struct sta_vars
{
    struct abs_time_us  timestamp_us;                   // Iteration timestamp (for logging)
    struct abs_time_ms  timestamp_ms;                   // Iteration timestamp (with millisecond precision)
    bool                sec_f;                          // Start of new second flag
    uint16_t              cal_active;                     // CAl.ACTIVE property
    uint16_t              mode_op;                        // Operational mode
    uint16_t              config_mode;                    // Configuration mode
    uint16_t              config_state;                   // Configuration state
    uint16_t              watchdog;                       // Watchdog value zero for MstTsk
    uint32_t              time_ms;                        // Time since entering the state (ms)
    uint16_t              faults;                         // Active faults mask (unlatched)
    uint16_t              first_faults;                   // Logged Fault mask (latched)
#if FGC_CLASS_ID != 59
    uint16_t              gw_pc_permit;                   // GW PC_PERMIT down counter (20ms units)
#if FGC_CLASS_ID == 51 || FGC_CLASS_ID == 53
    uint16_t              adc_external_f;                 // Use external ADCs? (ENABLED or DISABLED)
    uint16_t              adc_ext_inputs_f;               // ADC is SD_350 or SD_351 (i.e. with external inputs)
    uint16_t              adc_one_mhz[2];                 // ADC ONE_MHZ selector (ENABLED or DISABLED)
    uint16_t              last_adc_one_mhz[2];            // Last ADC ONE_MHZ selector (ENABLED or DISABLED)
    uint16_t              adc_ads1281[2];                 // External ADS1281 selector (ENABLED or DISABLED)
    uint16_t              last_adc_ads1281[2];            // Last external ADS1281 selector (ENABLED or DISABLED)
    int32_t              adc_filter_mpx[FGC_N_ADCS];     // ADC filter A/B digital input selector
    uint16_t              adc_filter_select[2];           // ADC.FILTER.SELECT: ADC filter A/B function selector (0 or 1)
    uint16_t              last_adc_filter_select[2];      // Last value of ADC.FILTER.SELECT
    uint16_t              internal_adc_mpx[FGC_N_ADCS];   // ADC16A/B analogue input selector (moved to adc global var for FGC3)
#endif
    bool                cyc_start_new_cycle_f;          // Flag used to warn the STA task about the starting of a new cycle
    uint16_t              copy_log_capture_state;         // Copy of the last value of LOG.CAPTURE.STATE
    int16_t              copy_log_capture_user;          // Copy of the last value of LOG.CAPTURE.USER
    struct abs_time_us  copy_log_capture_cyc_chk_time;  // Copy of the last value of LOG.CAPTURE.CYC_CHK_TIME
    uint16_t              cal_type;                       // Calibration type (CAL_REQ_ADC16, CAL_REQ_ADC16, CAL_REQ_DAC)
    uint16_t              cal_chan;                       // Calibration channel (0=A, 1=B)
    uint16_t              cal_seq_idx;                    // Calibration sequence index
    uint16_t              cal_counter;                    // Calibration down counter
    uint16_t              dcct_flts;                      // DCCT faults latch
    uint16_t              flags;                          // State machine control flags
    uint16_t              inputs;                         // Direct digital inputs
    uint16_t              ip_direct;                      // Raw digital inputs

// this variable use the flags DDOP_CMD_xxxx (is not directly related to the DIG_OP)
    uint16_t              cmd;                            // Command request (ON/OFF/RESET/POLPOS/POLNEG)

// this variable is directly related to the DIG_OP, use the DIG_OP_SET_xxxx , DIG_OP_RST_xxxx flags
    uint16_t              cmd_req;                        // Requested command mask (read back from interface)

    uint16_t              mode_pc;                        // MODE.PC property
#endif
    uint16_t              crash_code;                     // SW crash codes (Crash(0xBADx) inherited from FGC2) will be redirected to a McuPanic on FGC3
    struct mcu_panic
    {
        int32_t              code;                       // Panic ID. See McuPanic function (FGC3 only for now)
        uint32_t *            sp;                         // Stack pointer (for exceptions)
        uint32_t              pc;                         // Program Counter before the exception.
    }                   mcu_panic;
};

//-----------------------------------------------------------------------------------------------------------

STA_VARS_EXT struct sta_vars        sta;                    // State variables structure

//-----------------------------------------------------------------------------------------------------------

#endif  // STA_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sta.h
\*---------------------------------------------------------------------------------------------------------*/
