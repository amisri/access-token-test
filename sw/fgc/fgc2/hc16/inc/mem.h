/*---------------------------------------------------------------------------------------------------------*\
  File:         mem.h

  Purpose:      FGC2 Memory manipulation functions
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MEM_H   // header encapsulation
#define MEM_H
//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <cc_types.h>

//-----------------------------------------------------------------------------------------------------------

uint16_t  MemCmpBytes     (const uint8_t * d1, const uint8_t * d2, uint16_t n_bytes);
uint16_t  MemCmpStrFar    (const char * FAR s1f, const char * s2);
uint16_t  MemCmpWords     (const uint32_t d1, const uint32_t d2, uint16_t n_words);
void    MemCpyBytes     (uint8_t * d1, const uint8_t * d2, uint16_t n_bytes);
void    MemCpyFip       (void * d1, const void * d2, uint16_t n_bytes);
uint16_t  MemCpyStrFar    (char * s1, const char * FAR s2f);
void    MemCpyWords     (uint32_t  d1, uint32_t d2, uint16_t n_words);
uint16_t  MemCrc16        (const uint32_t address, uint32_t n_words);
void    MemSetBytes     (void * d1, uint8_t value, uint16_t n_bytes);
void    MemSetWords     (uint32_t d1, uint16_t value, uint32_t n_words);

//-----------------------------------------------------------------------------------------------------------

#endif  // MEM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mem.h
\*---------------------------------------------------------------------------------------------------------*/
