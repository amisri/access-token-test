/*---------------------------------------------------------------------------------------------------------*\
  File:         serial_stream.h

  Contents:

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef SERIAL_STREAM_H
#define SERIAL_STREAM_H


#ifdef SERIAL_STREAM_GLOBALS
    #define SERIAL_STREAM_VARS_EXT
#else
    #define SERIAL_STREAM_VARS_EXT extern
#endif

#include <cc_types.h>           // basic typedefs
#include <stdio.h>              // for FILE

//-----------------------------------------------------------------------------------------------------------

void    SerialStreamInternal_WriteChar          (char ch, FILE * f);
void    TermPutcChar                             (char ch, FILE * f);
void    SerialStreamInternal_FlushBuffer        (FILE * f);

//-----------------------------------------------------------------------------------------------------------

#endif  // SERIAL_STREAM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: serial_stream.h
\*---------------------------------------------------------------------------------------------------------*/
