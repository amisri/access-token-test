/*---------------------------------------------------------------------------------------------------------*\
  File:     mcu_dependent.h

  Contents: FGC2 constants.

  History:

    20 jul 07   doc Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MCU_DEPENDENT_H // header encapsulation
#define MCU_DEPENDENT_H

#include <dev.h>                // for dev.leds
#include <definfo.h>            // for FGC_CLASS_ID
#include <memmap_mcu.h>

#pragma NO_STRING_CONSTR        // Stop #xxx in macro from making a string literal "xxx" (needed for asm)

// FIND FIRST ONE:
// Macro to find first bit set in a 16 bit mask (starting from least significant bit, i.e. bit 0)

#define MCU_FF1(mask,resp)      {                                                                       \
                                    OS_ENTER_CRITICAL();                                                \
                                    CPU_LSSB_P = (mask);                                                \
                                    resp = CPU_LSSB_P;                                                  \
                                    OS_EXIT_CRITICAL();                                                 \
                                }

#define     Get2MHzCounter()        GPT_TCNT_P

#define     MSLEEP(ms)              {timeout_ms=ms;while(timeout_ms);}

#if FGC_CLASS_ID == 50
    #define     LED_SET(led)            asm BSETW RGLEDS_16, # RGLEDS_ ## led ## _MASK16
    #define     LED_RST(led)            asm BCLRW RGLEDS_16, # RGLEDS_ ## led ## _MASK16
#else
    #define     LED_SET(led)            asm BSETW dev.leds, # RGLEDS_ ## led ## _MASK16
    #define     LED_RST(led)            asm BCLRW dev.leds, # RGLEDS_ ## led ## _MASK16
#endif

#define     TST_CPU_RESET(bit)      (CPU_RESET_P &   CPU_RESET_##bit##_MASK16)

#define     USLEEP(us)              {uint16_t ticks=us*2,t0=GPT_TCNT_P;while((GPT_TCNT_P-t0)<ticks);}

#define     NVRAM_LOCK()            CPU_RESET_P&=~CPU_RESET_ULKFRAM_MASK16
#define     NVRAM_UNLOCK()          CPU_RESET_P|=CPU_RESET_ULKFRAM_MASK16
#define     NVRAM_LOCKED()          !(CPU_RESET_P&CPU_RESET_ULKFRAM_MASK16)

#define     TP_SET(tp)              SIM_PORTC_P |=  tp              // Two test points (1,2) using port C
#define     TP_RST(tp)              SIM_PORTC_P &= ~tp
#define     TP_TGL(tp)              SIM_PORTC_P ^=  tp

#define     SELFLASH(sf)            CPU_MODEL_P=sf|(CPU_MODEL_P&~(CPU_MODEL_SELFLS_MASK8))
#define     SELDPWPG(pg)            CPU_MODEH_P=(pg<<CPU_MODEH_DPWBASE_SHIFT)|(CPU_MODEH_P&~CPU_MODEH_DPWBASE_MASK8)
#define     SEL_MP                  0
#define     SEL_BB                  1
#define     SEL_BA                  2
#define     SEL_SR                  3

// ENTER_SR/EXIT_SR section forces memory mapping of pages 2-3 of the SRAM
// For more information: http://proj-fgc.web.cern.ch/proj-fgc/static/Platforms/FGC2/MemoryDevices.htm

#define     ENTER_SR()              {asm LDD CPU_MODE_16;asm PSHM D;asm BSET CPU_MODEL_16,#CPU_MODEL_SELSR_MASK8;}
#define     EXIT_SR()               {asm PULM D;asm STAB CPU_MODEL_16;}

// ENTER_BB/EXIT_BB section forces memory mapping of pages 2-3 of the Boot B flash. (This is where NameDB, SysDB,
// CombDB and DimDB are stored)

#define     ENTER_BB()              {asm LDD CPU_MODE_16;asm PSHM D;asm BCLR CPU_MODEL_16,#CPU_MODEL_SELSR_MASK8;}
#define     EXIT_BB()               {asm PULM D;asm STAB CPU_MODEL_16;}

#define     SET_CPU_MODEH(bit)      (CPU_MODEH_P |= CPU_MODEH_##bit##_MASK8)
#define     TST_CPU_MODEH(bit)      (CPU_MODEH_P &  CPU_MODEH_##bit##_MASK8)
#define     TST_CPU_MODEL(bit)      (CPU_MODEL_P &  CPU_MODEL_##bit##_MASK8)

// Magic number that, written to the RGLEDS_P register, triggers power cycle

#define     DEV_PWR_CYC_REQ         0xF00E

// Triggers power cycle by writing a magic number to the RGLEDS_P register.
// CPU_PWRCYC_P is an alias for RGLEDS_P. The magic number has to stay in the
// register for ~100ms, so all writing to it must be prevented.

#define     POWER_CYCLE()           (CPU_PWRCYC_P = DEV_PWR_CYC_REQ)

//-----------------------------------------------------------------------------------------------------------
// used in the BOOT

#define BOOT_STATE_SELF_TEST        RGLEDS_VS_GREEN_MASK16
#define BOOT_STATE_WAIT_USER        RGLEDS_PIC_GREEN_MASK16|RGLEDS_PIC_RED_MASK16
#define BOOT_STATE_RUN_MENU         RGLEDS_PIC_GREEN_MASK16

// Register access constants

#define REG_LEDS                    0x0001          // LEDs register access
#define REG_C62                     0x0002          // C62 register access
#define REG_FIP                     0x0004          // FIP register access
#define REG_DIG                     0x0008          // DIG register access
#define REG_SLOT5                   0x0010          // Slot5 (ANA/RFC) register access

#endif  // MCU_DEPENDENT_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: mcu_dependent.h
\*---------------------------------------------------------------------------------------------------------*/
