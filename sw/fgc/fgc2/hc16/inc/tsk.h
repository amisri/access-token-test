/*---------------------------------------------------------------------------------------------------------*\
  File:         mcu/fgc2/inc/tsk.h

  Contents:     Header file FGC2 MCU tasks

  Notes:        This header file is NOT associated with a C source file
\*---------------------------------------------------------------------------------------------------------*/

#ifndef TSK_H
#define TSK_H

#ifdef TSK_GLOBALS
    #define TSK_VARS_EXT
#else
    #define TSK_VARS_EXT extern
#endif
#include <os.h>         // for OS_STK type
#include <definfo.h>    // for FGC_CLASS_ID

//-----------------------------------------------------------------------------------------------------------

// Task Identifiers (cannot be enum because of asm in vecs.c)

#define TSK_MST                         0
#define TSK_FBS                         1
#define TSK_STA                         2
#define TSK_FCM                         3
#define TSK_TCM                         4
#define TSK_DLS                         5
#define TSK_RTD                         6
#define TSK_TRM                         7
#define TSK_BGP                         8
#define MCU_NB_OF_TASKS                 (TSK_BGP + 1)

// Task masks (required in asm in isr.c)

#define TSK_MST_MASK                    0x0001
#define TSK_FBS_MASK                    0x0002
#define TSK_STA_MASK                    0x0004
#define TSK_FCM_MASK                    0x0008
#define TSK_TCM_MASK                    0x0010
#define TSK_DLS_MASK                    0x0020
#define TSK_RTD_MASK                    0x0040
#define TSK_TRM_MASK                    0x0080
#define TSK_BGP_MASK                    0x0100

// Task Stack sizes (words)

// NB: it is advised to have STK_SIZE_FCM = STK_SIZE_TCM since both tasks share the same code (CmdTsk)

#define STK_SIZE_MST                    128                 // 0
#define STK_SIZE_FBS                    64                  // 1
#define STK_SIZE_STA                    96                  // 2
#define STK_SIZE_FCM                    256                 // 3
#define STK_SIZE_TCM                    256                 // 4
#define STK_SIZE_DLS                    160                 // 5
#define STK_SIZE_RTD                    128                 // 6
#define STK_SIZE_TRM                    256                 // 7
#define STK_SIZE_BGP                    64                  // 8

//-----------------------------------------------------------------------------------------------------------
// Task Stack Buffers

struct tsk_stk
{
    OS_STK              mst[STK_SIZE_MST];              // Millisecond tick task stack
    OS_STK              fbs[STK_SIZE_FBS];              // Fieldbus task stack
    OS_STK              sta[STK_SIZE_STA];              // State task stack
    OS_STK              fcm[STK_SIZE_FCM];              // Fieldbus command task stack
    OS_STK              tcm[STK_SIZE_TCM];              // Terminal command task stack
    OS_STK              dls[STK_SIZE_DLS];              // Dallas task stack
    OS_STK              rtd[STK_SIZE_RTD];              // Real-time display task stack
    OS_STK              trm[STK_SIZE_TRM];              // Terminal task stack
    OS_STK              bgp[STK_SIZE_BGP];              // Idle task stack
};

TSK_VARS_EXT struct tsk_stk     tsk_stk;                // Task Stack Buffers Structure

//-----------------------------------------------------------------------------------------------------------

#endif  // TSK_H end of header encapsulation

// EOF
