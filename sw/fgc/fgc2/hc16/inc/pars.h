/*---------------------------------------------------------------------------------------------------------*\
 File:          pars.h

 Purpose:       FGC MCU Software - Parameter parsing Functions.

 Author:        Quentin.King@cern.ch
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PARS_H      // header encapsulation
#define PARS_H

#ifdef PARS_GLOBALS
    #define PARS_VARS_EXT
#else
    #define PARS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <property.h>   // for struct prop
#include <cmd.h>        // for struct cmd
#include <dpcom.h>      // for struct abs_time_us

//-----------------------------------------------------------------------------------------------------------

uint16_t  ParsScanAbsTime         (struct cmd *c, struct abs_time_us **value);
uint16_t  ParsScanChar            (struct cmd *c, char **value);
uint16_t  ParsScanInteger         (struct cmd *c, char * FAR delim, uint32_t *value);
uint16_t  ParsScanStd             (struct cmd *c, struct prop *p, uint16_t par_type, void **value);
uint16_t  ParsScanToken           (struct cmd *c, char * FAR delim);
uint16_t  ParsScanSymList         (struct cmd *c, char * FAR delim, const struct sym_lst * sym_lst, struct sym_lst **sym_const);
uint16_t  ParsScanSymListAndNone  (struct cmd *c, char * FAR delim, struct sym_lst *sym_lst, struct sym_lst **sym_const);
uint16_t  ParsSet                 (struct cmd *c, struct prop *p);
uint16_t  ParsValueGet            (struct cmd *c, struct prop *p, void **value);
uint16_t  ParsValueGetSymList     (struct cmd *c, struct prop *p, void **value);
#define ParsScanHexDigit(ch)    (ch>='0' && ch<='9'?ch-'0':(ch>='A' && ch<='F'?ch-('A'-10):0x10))

//-----------------------------------------------------------------------------------------------------------

#endif  // PARS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars.h
\*---------------------------------------------------------------------------------------------------------*/
