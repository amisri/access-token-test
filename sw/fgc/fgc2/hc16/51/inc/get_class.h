/*---------------------------------------------------------------------------------------------------------*\
  File:         get_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef GET_CLASS_H      // header encapsulation
#define GET_CLASS_H

#ifdef GET_CLASS_GLOBALS
    #define GET_CLASS_VARS_EXT
#else
    #define GET_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <stdbool.h>
#include <log_class.h>
#include <property.h>           // for struct prop
#include <cmd.h>                // for struct cmd

//-----------------------------------------------------------------------------------------------------------

#define FGC_PM_BUF_LEN          FGC_PM_BUF_LEN_51

//-----------------------------------------------------------------------------------------------------------

// IAB measurement variables

struct meas_vars
{
    FP32                ia;                             // Unfiltered 1kHz measurement of DCCT A
    FP32                ib;                             // Unfiltered 1kHz measurement of DCCT B
};

//-----------------------------------------------------------------------------------------------------------

uint16_t  GetLogPmBuf     (struct cmd *c, struct prop *p);
uint16_t  GetLogPSU       (struct cmd *c, struct prop *p);

//-----------------------------------------------------------------------------------------------------------

GET_CLASS_VARS_EXT struct meas_vars         meas;                         // IA/B FIFO variables

//-----------------------------------------------------------------------------------------------------------

#endif  // GET_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: get_class.h
\*---------------------------------------------------------------------------------------------------------*/
