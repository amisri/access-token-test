/*---------------------------------------------------------------------------------------------------------*\
  File:         sta_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef STA_CLASS_H      // header encapsulation
#define STA_CLASS_H

#ifdef STA_CLASS_GLOBALS
    #define STA_CLASS_VARS_EXT
#else
    #define STA_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>           // basic typedefs
#include <stdbool.h>

//-----------------------------------------------------------------------------------------------------------

// Direct digital command requests (sta.cmd)

#define DDOP_CMD_ON             0x01
#define DDOP_CMD_OFF            0x02
#define DDOP_CMD_RESET          0x04
#define DDOP_CMD_POL_POS        0x08
#define DDOP_CMD_POL_NEG        0x10
#define DDOP_CMD_VSCLLOOP       0x20
#define DDOP_CMD_FGCFLT         0x80

#define VS_SIGS  DIG_IP1_INTLKSPARE_MASK16 | DIG_IP1_VSRUN_MASK16 | DIG_IP1_VSREADY_MASK16 | DIG_IP1_VSPOWERON_MASK16

#define VS_STATE_INVALID_TO     100             // Timeout (5ms units)

// Vsource

struct vs_vars
{
    uint32_t              polarity_timeout;               // Polarity switch timeout (s)
    uint32_t              vsrun_timeout_ms;               // Voltage source startup timeout (ms)
    uint32_t              polarity;                       // Polarity switch state
    FP32                vsrun_timeout;                  // Voltage source startup timeout (s)
    uint16_t              sim_intlks;                     // Property VS.SIM_INTLKS value
    uint16_t              active_filter;                  // Property VS.ACTIVE_FILTER value
    uint16_t              req_polarity;                   // Requested polarity switch state
    uint16_t              polarity_counter;               // Polarity switch check counter (s)
    uint16_t              vsstate_counter;                // VS invalid state test counter
    uint16_t              n_sub_convs;                    // Number of sub converters
    uint16_t              fw_diode;                       // 0=FWD Not Present  1=No FWD Fault  2=FWD Fault
    uint16_t              fabort_unsafe;                  // 0=FAU Not Present  1=No FAU Fault  2=FAU Fault
    uint16_t              fwd_diag_chan;                  // Index in diag.data[] containing FWD status
    uint16_t              fwd_diag_mask;                  // Mask in the diag.data chan containing FWD status
    bool                fwd_is_fault;                   // True if FWD is a fault, false if it's only a status
    uint16_t              fau_diag_chan;                  // Index in diag.data[] containing FAU status
    uint16_t              fau_diag_mask;                  // Mask in the diag.data chan containing FAU status

    struct vs_sub_conv
    {
        uint16_t          data_idx;
        uint16_t          mask;
    } sub_conv[8];
};

//-----------------------------------------------------------------------------------------------------------

void            StaTsk                  (void *);
void            StaInit                 (void);
void            StaOpState              (void);
void            StaDdips                (void);
void            StaDdipsPops            (void);
void            StaVsState              (void);
void            StaAdcControl           (void);
void            StaCheck                (void);
void            StaLeds                 (void);
void            StaPcState              (void);
void            StaCalControl           (void);
void            StaCmdControl           (void);
uint16_t          StaStartPC              (void);
void            StaPubCyclingProp       (void);

//-----------------------------------------------------------------------------------------------------------

STA_CLASS_VARS_EXT struct vs_vars vs;                             // Reference variables structure

//-----------------------------------------------------------------------------------------------------------

#endif  // STA_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sta_class.h
\*---------------------------------------------------------------------------------------------------------*/
