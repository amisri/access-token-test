//! @file  transaction.h
//! @brief Provides the framework for transactional settings
//!
//!  The transactions specification is documented in the link below:
//!  https://wikis.cern.ch/display/COTEC/Transactions+support+in+the+Control+System

#ifndef TRANSACTION_H
#define TRANSACTION_H

// ---------- Includes

#include <stdint.h>

#include <defconst.h>

#include <dpcom.h>
#include <property.h>



// ---------- External structures, unions and enumerations

//! This structure contains the variables associated to transactional settings

struct Transaction_prop
{
    uint16_t   id;       //!< TRANSACTION.ID
    uint16_t   state;    //!< TRANSACTION.STATE
};



// ---------- External variable declarations

extern struct Transaction_prop transaction_prop;



// ---------- External function declarations

//! Initializes the transaction module

void transactionInit(void);


//! Initiates a transaction
//!
//! @param  transaction_id               New transaction ID
//!
//! @return FGC_OK_NO_RSP                The Transaction ID has been set
//! @return FGC_TRANSACTION_IN_PROGRESS  A transaction is already in progress

uint16_t transactionSetId(uint16_t const transaction_id);


//! Returns the transaction ID
//!
//! @retval                              Transaction ID

uint16_t transactionGetId(void);


//! Validates the transaction ID
//!
//! @param  transaction_id               Transaction ID to validate
//!
//! @return FGC_OK_NO_RSP                Valid transaction ID
//! @return FGC_TRANSACTION_ID_UNKNOWN   Invalid transaction ID

uint16_t transactionValidateId(uint32_t const transaction_id);



//! Check property access rules for transactions
//!
//! @param  cmd_type                     Command type
//! @param  cmd_transaction_id           Transaction ID passed in the command
//! @param  property                     Property
//!
//! @return FGC_OK_NO_RSP                Valid transaction ID
//! @return FGC_TRANSACTION_IN_PROGRESS  Transaction in progress
//! @return FGC_INVALID_TRANSACTION_ID   Invalid transaction ID
//! @return FGC_PROPERTY_NOT_TRANSACTIONAL Non-transactional property

uint32_t transactionAccessCheck(uint16_t   const   cmd_type,
                                uint16_t   const   cmd_transaction_id,
                                struct prop      * property);


//! Tests transactions for all users that have a transaction ID that
//! matches the ID provided in the value.
//!
//! @param  transaction_id               Transaction ID to test
//!
//! @return FGC_OK_NO_RSP                The reference is correct
//! @return FGC_TRANSACTION_ID_UNKNOWN   The transaction ID is unknown
//! @return error                        Error when arming the reference

uint16_t transactionTest(uint16_t const transaction_id);


//! Commits transactions for all users that have a transaction ID that
//! matches the ID provided in the value.
//!
//! @param  transaction_id               Transaction ID to commit
//! @param  delay_us                     Delay in milliseconds when to commit the transaction
//!
//! @return FGC_OK_NO_RSP                The reference is correct
//! @return FGC_TRANSACTION_ID_UNKNOWN   Transaction ID not found
//! @return error                        Error when arming the reference

uint16_t transactionCommit(uint16_t const transaction_id, uint16_t const delay_ms);


//! Rolls-back transactions for all users that have a transaction ID that
//! matches the ID provided in the value.
//!
//! @param  transaction_id               Transaction ID to rollback
//!
//! @return FGC_OK_NO_RSP                Rollback successful
//! @return FGC_TRANSACTION_ID_UNKNOWN   Transaction ID not found

uint16_t transactionRollback(uint16_t const transaction_id);


//! Acknowledges failed commits by transitioning the state from COMMIT_FAILED to NONE.

void transactionAckCommitFailed(void);


#endif  // TRANSACTION_H end of header encapsulation

// EOF
