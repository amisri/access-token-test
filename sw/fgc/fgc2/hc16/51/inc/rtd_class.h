/*---------------------------------------------------------------------------------------------------------*\
  File:         rtd_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef RTD_CLASS_H      // header encapsulation
#define RTD_CLASS_H

#ifdef RTD_CLASS_GLOBALS
    #define RTD_CLASS_VARS_EXT
#else
    #define RTD_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <stdbool.h>
#include <dpcls.h>      // for dpcls global variable
#include <stdio.h>      // for FILE
#include <definfo.h>    // for FGC_CLASS_ID
#include <rtd.h>        // for RtdFlags(), RtdTout(), RtdTin(), RtdCpuUsage(), RtdCpu32Usage(), RtdOptionLine(), RtdOptionLine()
#include <fbs_class.h>  // for FAULTS, WARNINGS, ST_MEAS_A, ST_MEAS_B, ST_DCCT_A, ST_DCCT_B
#include <sta.h>        // for sta global variable
#include <fbs.h>        // for fbs global variable

//-----------------------------------------------------------------------------------------------------------

/*----- RTD Field position constants -----*/

#define RTD_TOUT_POS            "21;76"
#define RTD_TIN_POS             "22;76"
#define RTD_CPUUSE_POS          "23;75"
#define RTD_CPU32USE_POS        "23;78"
#define RTD_STATES_POS          "21;9"
#define RTD_IREF_POS            "21;30"
#define RTD_IMEAS_POS           "22;30"
#define RTD_IRATE_POS           "23;30"
#define RTD_VREF_POS            "21;46"
#define RTD_VMEAS_POS           "22;46"
#define RTD_IERR_POS            "23;46"
#define RTD_VA_POS              "21;59"
#define RTD_VB_POS              "22;59"
#define RTD_IDIFF_POS           "23;60"
#define RTD_TRUN_POS            "24;60"
#define RTD_TREM_POS            "24;75"
#define RTD_REFTYPE_POS         "24;6"
#define RTD_REF_START_POS       "24;30"
#define RTD_REF_END_POS         "24;43"

#define RTD_ADCS_HWIDTH         41u
#define RTD_ADCS_HPOS_RAW       1u
#define RTD_ADCS_HPOS_VOLTS     11u
#define RTD_ADCS_HPOS_PP        22u
#define RTD_ADCS_HPOS_AMPS      30u

#define RTD_LOAD_R_POS          "20;1"
#define RTD_LOAD_L_POS          "20;12"

#define RTD_PROT_IEARTH         "20;1"
#define RTD_PROT_POS_POS        "20;27"
#define RTD_PROT_NEG_POS        "20;39"

#define RTD_CYC                 "20;1"

//-----------------------------------------------------------------------------------------------------------

struct rtd_cls_vars
{
    struct rtd_cls_vars_last                            // Last displayed values:
    {
        FP32            iref;                           // Iref
        FP32            imeas;                          // Imeas
        FP32            vref;                           // Vref
        FP32            vmeas;                          // Vmeas
        FP32            i_rate;                         // Irate
        FP32            v[2];                           // Measured voltages
        char            iref_s[10];                     // Iref string
        char            imeas_s[10];                    // Imeas string
        char            vref_s[10];                     // Vref string
        char            vmeas_s[10];                    // Vmeas string
        char            i_rate_s[10];                   // Irate string
        char            v_s[2][12];                     // Measured voltages string
        uint16_t          state_vs;                       // Voltage source state
        uint16_t          state_pc;                       // Power converter state
        uint16_t          state_lk;                       // PAL links state
        uint16_t          ref_stc_type;                   // Ref type symbol index
        FP32            ref_end;                        // ref end
        int16_t          ierr;                           // Current loop error (mA)
        int16_t          idiff;                          // Measurement difference (mA)
        uint32_t          trun;                           // Ref run time
        uint32_t          trem;                           // Ref remain time
    } last;

    struct rtd_cls_optline
    {
        int32_t          adc_raw[2];                     // Last displayed ADC raw value
        int16_t          user;                           // Last displayed user
        FP32            adc_volts[2];                   // Last displayed ADC volts value
        FP32            adc_pp[2];                      // Last displayed ADC pp noise value
        FP32            adc_amps[2];                    // Last displayed ADC amps value
        FP32            ohms;                           // Measured load resistance
        FP32            henrys;                         // Measured load inductance
        FP32            u_leads[2];                     // Measured U_LEAD_POS/NEG
        char            ohms_s[10];                     // Measured load resistance string
        char            henrys_s[10];                   // Measured load inductance string
    } optline;
};

//-----------------------------------------------------------------------------------------------------------

void            RtdInit                 (FILE *);
bool            RtdStates               (void);
bool            RtdIref                 (void);
bool            RtdImeas                (void);
#if 0   // Iref rate is not computed anymore
bool            RtdIrefRate             (void);
#endif
bool            RtdImeasRate            (void);
bool            RtdVref                 (void);
bool            RtdVmeas                (void);
bool            RtdIerr                 (void);
bool            RtdVa                   (void);
bool            RtdVb                   (void);
bool            RtdIdiff                (void);
bool            RtdRef                  (void);
bool            RtdTrun                 (void);
bool            RtdTrem                 (void);
bool            RtdAdcs                 (void);
bool            RtdProt                 (void);
bool            RtdCyc                  (void);

//-----------------------------------------------------------------------------------------------------------

RTD_CLASS_VARS_EXT struct rtd_cls_vars    rtd_cls;                // Real-time display structure for class 51

RTD_CLASS_VARS_EXT bool                 (*rtd_func[])(void)    // RTD functions
#ifdef RTD_CLASS_GLOBALS
= {
    RtdStates,                                  // Display states
    RtdFlags,                                   // Flag characters
    RtdIref,                                    // Display current reference value
    RtdImeas,                                   // Display current measurement value
    RtdImeasRate,                               // Display rate of change of Imeas value
    RtdVref,                                    // Display voltage reference value
    RtdVmeas,                                   // Display voltage measurement value
    RtdIerr,                                    // Display feedback error term value
    RtdVa,                                      // Display ADC A voltage value
    RtdVb,                                      // Display ADC B voltage value
    RtdIdiff,                                   // Display current difference value
    RtdTout,                                    // Display outlet temperature
    RtdTin,                                     // Display inlet temperature
    RtdRef,                                     // Display reference function type and range
    RtdTrun,                                    // Display run time value
    RtdTrem,                                    // Display end time value
    RtdCpuUsage,                                // CPU usage
    RtdCpu32Usage,                              // CPU32 usage
    RtdOptionLine,                              // Option line
    RtdOptionLine,                              // Option line
    0
}
#endif
;

RTD_CLASS_VARS_EXT struct rtd_flags        rtd_flags[]                    // rtd.last.flag_off[] must have at least as
#ifdef RTD_CLASS_GLOBALS
= {                       // ----- CLASS 51 & 53 -----    // many elements as rtd_flags (40 at the moment)
        { &FAULTS,              FGC_FLT_FGC_HW,                 { '#', 'H'}, "22;5"  },         // 1
        { &FAULTS,              FGC_FLT_I_MEAS,                 { '#', 'M'}, "22;6"  },         // 2
        { &FAULTS,              FGC_FLT_REG_ERROR,              { '#', 'F'}, "22;7" },          // 3
        { &FAULTS,              FGC_FLT_VS_RUN_TO,              { '#', 'T'}, "22;8"  },         // 4
        { &FAULTS,              FGC_FLT_VS_FAULT,               { '#', 'V'}, "22;9"  },         // 5
        { &FAULTS,              FGC_FLT_VS_EXTINTLOCK,          { '#', 'E'}, "22;10" },         // 6
        { &FAULTS,              FGC_FLT_LIMITS,                 { '#', 'L'}, "22;11" },         // 7

        { &WARNINGS,            FGC_WRN_FGC_HW,                 { '#', 'H'}, "22;13" },         // 8
        { &WARNINGS,            FGC_WRN_I_MEAS,                 { '#', 'M'}, "22;14" },         // 9
        { &WARNINGS,            FGC_WRN_REG_ERROR,              { '#', 'F'}, "22;15" },         // 10
        { &WARNINGS,            FGC_WRN_SUBCONVTR_FLT,          { '#', 'S'}, "22;16" },         // 11
        { &WARNINGS,            FGC_WRN_FGC_PSU,                { '#', 'P'}, "22;17" },         // 12
        { &WARNINGS,            FGC_WRN_V_ERROR,                { '#', 'G'}, "22;18" },         // 13
        { &WARNINGS,            FGC_WRN_CONFIG,                 { '#', 'C'}, "22;19" },         // 14

        { &ST_UNLATCHED,        FGC_UNL_POL_SWI_POS,            { '#', 'P'}, "23;10" },         // 15
        { &ST_UNLATCHED,        FGC_UNL_POL_SWI_NEG,            { '#', 'N'}, "23;11" },         // 16
        { (uint16_t *)(&ST_DCCT_A-1), FGC_DCCT_FAULT,             { '#', 'A'}, "23;12" },         // 17
        { (uint16_t *)(&ST_DCCT_B-1), FGC_DCCT_FAULT,             { '#', 'B'}, "23;13" },         // 18
        { &ST_UNLATCHED,        FGC_UNL_POST_MORTEM,            { '#', 'M'}, "23;14" },         // 19

        { &sta.config_state,    FGC_CFG_STATE_SYNCHRONISED,     { '~', 'U'}, "23;16" },         // 20
        { &sta.config_mode,     FGC_CFG_MODE_SYNC_FGC,          { '#', 'F'}, "23;17" },         // 21
        { &sta.config_mode,     FGC_CFG_MODE_SYNC_DB,           { '#', 'D'}, "23;18" },         // 22
        { &sta.config_mode,     FGC_CFG_MODE_SYNC_DB_CAL,       { '#', 'C'}, "23;19" },         // 23
        { &sta.config_mode,     FGC_CFG_MODE_SYNC_FAILED,       { '#', 'F'}, "23;20" },         // 24

        { &ST_MEAS_A,           FGC_MEAS_IN_USE,                { '#', 'a'}, "21;57" },         // 25
        { &ST_MEAS_B,           FGC_MEAS_IN_USE,                { '#', 'b'}, "22;57" },         // 26
        { &ST_MEAS_A,           FGC_MEAS_V_MEAS_OK,             { ' ', '*'}, "21;69" },         // 27
        { &ST_MEAS_B,           FGC_MEAS_V_MEAS_OK,             { ' ', '*'}, "22;69" },         // 28

        {(uint16_t *)(&dpcls.mcu.ref.func.reg_mode)+1, FGC_REG_I, { '#', 'I'}, "21;25" },         // 29
        {(uint16_t *)(&dpcls.mcu.ref.func.reg_mode)+1, FGC_REG_I, { '~', 'V'}, "21;41" },         // 30

#if FGC_CLASS_ID == 51  // ----- CLASS 51 -----

        { &WARNINGS,            FGC_WRN_ILIM_EXPECTED,          { '#', 'I'}, "22;20" },         // 32
        { &WARNINGS,            FGC_WRN_REF_LIM,                { '#', 'L'}, "22;21" },         // 33
        { &WARNINGS,            FGC_WRN_REF_RATE_LIM,           { '#', 'R'}, "22;22" },         // 34

        { &ST_UNLATCHED,        FGC_UNL_PC_PERMIT,              { '#', 'P'}, "23;5" },          // 35
        { &FAULTS,              FGC_FLT_FAST_ABORT,             { '#', 'F'}, "23;6"  },         // 36
        { &ST_UNLATCHED,        FGC_UNL_PC_DISCH_RQ,            { '#', 'D'}, "23;7"  },         // 37
        { &ST_UNLATCHED,        FGC_UNL_PWR_FAILURE,            { '#', 'P'}, "23;8"  },         // 38

#else                   // ----- CLASS 53 -----

        { &WARNINGS,            FGC_WRN_B_MEAS,                 { '#', 'B'}, "22;20" },         // 31
        { &WARNINGS,            FGC_WRN_I_RMS_LIM,              { '#', 'R'}, "22;21" },         // 32
        { &WARNINGS,            FGC_WRN_VS_COMMS,               { '#', 'K'}, "22;22" },         // 33

        { &WARNINGS,            FGC_WRN_TIMING_EVT,             { '#', 'E'}, "23;7"  },         // 34
        { &WARNINGS,            FGC_WRN_CYCLE_START,            { '#', 'S'}, "23;8"  },         // 35
#endif
        { NULL },                                       // # = Reverse flag if 1, ~ = Reverse flag if 0
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // RTD_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rtd_class.h
\*---------------------------------------------------------------------------------------------------------*/
