/*---------------------------------------------------------------------------------------------------------*\
  File:         adc_class.h

  Purpose:

  Author:       pierre.dejoue@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ADC_CLASS_H      // header encapsulation
#define ADC_CLASS_H

#ifdef ADC_CLASS_GLOBALS
    #define ADC_CLASS_VARS_EXT
#else
    #define ADC_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>

#include <cc_types.h>   // basic typedefs
#include <defconst.h>

//-----------------------------------------------------------------------------------------------------------

#define SD_FLTR_LEN_W           0x4000          // 0x2000 samples max per filter function

//-----------------------------------------------------------------------------------------------------------

struct adc                                              // Structure used for driving the SD-350/351 analog cards
{
    bool    in_reset_f;                                 // ADC filters in reset
    bool    trigger_reset_f;                            // Reset the ADC filters after next regulation period
    bool    pending_reset_req_f;                        // Flag for pending reset request (checked every 200ms)
    uint16_t  reset_counter_ms;                           // 1kHz counter started when resetting the ADC filters
    bool    reset_timeout_occured_once_f;               // Flag indicating that we had a reset timeout once
    uint16_t  unl_i_meas_diff_counter;                    // counter of resets caused by an unlatched I_MEAS_DIFF
    uint16_t  filter_idx_a;                               // Last filter index set for channel A (range: 0-4)
    uint16_t  filter_idx_b;                               // Last filter index set for channel B (range: 0-4)
    uint32_t  last_reset_unix_time;                       // Property ADC.FILTER.LAST_RESET
    uint16_t  sd_shift[FGC_N_SD_FLTRS];                   // ADC SD filter shifts
    uint16_t  sd_halftaps[FGC_N_SD_FLTRS];                // ADC SD filter half taps
};

ADC_CLASS_VARS_EXT struct adc         adc;

//-----------------------------------------------------------------------------------------------------------

void AdcInit(void);
void AdcFiltersResetRequest(bool user_request_f);
void AdcFiltersReset(void);
void AdcFiltersResetCheck(void);
void AdcFiltersReadFunctions(void);
void AdcFiltersSetIndex(uint16_t filter_idx_a, uint16_t filter_idx_b);

#endif  // ADC_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: adc_class.h
\*---------------------------------------------------------------------------------------------------------*/
