/*---------------------------------------------------------------------------------------------------------*\
  File:         mst_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MST_CLASS_H      // header encapsulation
#define MST_CLASS_H

#ifdef MST_CLASS_GLOBALS
    #define MST_CLASS_VARS_EXT
#else
    #define MST_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

#define MST_FIP_STAT_VAR_TIME   1               // Millisecond to write STAT var to FIP

// Constants related to ADC filters reset

#define ADC_RESET_PERIOD_S                      86400   // Period for automatic ADC filters reset (in seconds)

#define ADC_RESET_MAX_UNLATCHED_PER_PERIOD      5       // Maximum number of ADC resets due to an unlatched
                                                        // I_MEAS_DIFF during one automatic reset period


//-----------------------------------------------------------------------------------------------------------

void    MstClass        (void);

//-----------------------------------------------------------------------------------------------------------

#endif  // MST_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mst_class.h
\*---------------------------------------------------------------------------------------------------------*/
