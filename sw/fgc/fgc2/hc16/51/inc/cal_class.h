/*---------------------------------------------------------------------------------------------------------*\
  File:         cal_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CAL_CLASS_H      // header encapsulation
#define CAL_CLASS_H

#ifdef CAL_CLASS_GLOBALS
    #define CAL_CLASS_VARS_EXT
#else
    #define CAL_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <property.h>   // for struct prop

//-----------------------------------------------------------------------------------------------------------

/*----- CAL constants -----*/

#define CAL_NO_AUTO_TIME        86400L          // No automatic ADC calibration time (s)
#define CAL_IN_STATE_DELAY      600000L         // No automatic ADC calibration before being in state (ms)
#define CAL_SUM16_PERIOD        40              // ADC16 calibration period (200ms)
#define CAL_SUM22_PERIOD        50              // ADC22 calibration period (200ms)
#define CAL_DCCT_PERIOD         50              // DCCT  calibration period (200ms)
#define CAL_VREF_LIMIT          0.2             // Max error in reference measurement to allow calibration.

#define REF_FUNC_TYPE           (uint16_t)dpcls.dsp.ref.func_type         // Ref func now
#define REF_ARMED_FUNC_TYPE     (uint16_t)dpcls.dsp.ref.func.type         // Armed funcs array for [user]
#define REF_STC_ARMED_FUNC_TYPE (uint16_t)dpcls.dsp.ref.stc_func_type     // Ref func sym_idx now for RTD
#define NON_PPM_REG_MODE        (uint16_t)dpcls.mcu.ref.func.reg_mode[0]  // Reg mode for user 0

//-----------------------------------------------------------------------------------------------------------

struct cal_seq
{
    uint16_t              (*func)(uint16_t,uint16_t);         // Sequence function
    uint16_t              par1;                           // Sequence parameter
    uint16_t              par2;                           // Sequence parameter
};

/*----- Auto Calibration Variables -----*/

struct cal_vars
{
    struct prop *       prop;                           // Calibration property
    uint16_t              seq_idx;                        // Calibration sequence index
    uint16_t              counter;                        // Calibration delay counter
    uint32_t              inhibit_cal;                    // Seconds down counter for inhibiting auto-cal
    uint32_t              last_cal_time_unix;             // Last calibration time for internal ADC (Unix time)
    FP32                dac[3];                         // Workspace for calibrating DAC
    struct cal_calsys
    {
        char    cdc_name[24];                           // Property CAL.CALSYS.CDC_NAME
        char    sm_name [24];                           // Property CAL.CALSYS.SM_NAME
        uint16_t  sm_channel;                             // Property CAL.CALSYS.SM_CHANNEL
    } calsys;
};

//-----------------------------------------------------------------------------------------------------------

void            CalRunSequence          (void);
uint16_t          CalSetAdcMpxInternal    (uint16_t, uint16_t);
uint16_t          CalSetAdc16Mpx          (uint16_t, uint16_t);
uint16_t          CalWaitDsp              (uint16_t, uint16_t);
uint16_t          CalBothAdc16s           (uint16_t, uint16_t);
uint16_t          CalAdc16                (uint16_t, uint16_t);
uint16_t          CalAdc22OrDcctErr       (uint16_t, uint16_t);
uint16_t          CalDac                  (uint16_t, uint16_t);
uint16_t          CalEndBothAdc16s        (uint16_t, uint16_t);
uint16_t          CalEndAdc16             (uint16_t, uint16_t);
uint16_t          CalEndAdc22             (uint16_t, uint16_t);
uint16_t          CalEndDcct              (uint16_t, uint16_t);
uint16_t          CalEnd                  (uint16_t, uint16_t);

//-----------------------------------------------------------------------------------------------------------

// used by defprops.h
CAL_CLASS_VARS_EXT struct cal_vars        cal;                    // Auto calibration structure

//-----------------------------------------------------------------------------------------------------------

#endif  // CAL_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cal_class.h
\*---------------------------------------------------------------------------------------------------------*/
