/*---------------------------------------------------------------------------------------------------------*\
  File:         transitions_class.h

  Contents:

  Notes:


\*---------------------------------------------------------------------------------------------------------*/

#ifndef TRANSITIONS_CLASS_H      // header encapsulation
#define TRANSITIONS_CLASS_H

#ifdef TRANSITIONS_CLASS_GLOBALS
    #define TRANSITIONS_CLASS_VARS_EXT
#else
    #define TRANSITIONS_CLASS_VARS_EXT extern
#endif
//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>           // basic typedefs
#include <stdbool.h>
#include <state_class.h>        // for StateXX ...
#include <defconst.h>
#include <macros.h>

//-----------------------------------------------------------------------------------------------------------

// Transition function constants

// each one has it corresponding function
enum state_pc_transitions
{
    tr_OFtoFO,          //  0 off to fault (off)
    tr_FStoFO,          //  1 fault (stopping) to fault (off)
    tr_FStoSP,          //  2 fault (stopping) to stopping
    tr_FOtoOF,          //  3 fault (off) to off
    tr_SPtoOF,          //  4 stopping to off
    tr_STtoFS,          //  5 starting to fault (stopping)
    tr_XXtoFS,          //  6 ??? to fault (stopping)
    tr_STtoSP,          //  7 starting to stopping
    tr_XXtoSP,          //  8 ??? to stopping
    tr_OFtoST,          //  9 off to starting
    tr_XXtoSA,          // 10 ??? to slow abort
    tr_STtoTS,          // 11 starting to to_standBy
    tr_XXtoTS,          // 12 ??? to to_standBy
    tr_TStoSB,          // 13 to_standBy to standBy
    tr_TStoAB,          // 14 to_standBy to aborting
    tr_SBtoIL,          // 15 standBy to idle
    tr_ARtoIL,          // 16 armed to idle
    tr_RNtoIL,          // 17 running to idle
    tr_ABtoIL,          // 18 aborting to idle
    tr_SAtoAB,          // 19 slow_abort to aborting
    tr_ILtoTC,          // 20 idle to to_cycling
    tr_ILtoAR,          // 21 idle to armed
    tr_ARtoRN,          // 22 armed to running
    tr_RNtoAB,          // 23 running to aborting
    tr_SBtoTC,          // 24 standBy to to_cycling
    tr_TCtoCY,          // 25 to_cycling to cycling (running pulse to pulse modulation)
};

struct transition
{
    uint16_t      (*condition)(void);
    uint8_t       next_state;
};

struct state
{
    void        (*state_func)(bool); // state function
    uint8_t       n_trans;                // number of possible transitions from this state
    uint8_t       *trans;                 // list of possible transitions (bifurcations) from this state
};

//-----------------------------------------------------------------------------------------------------------

// Transition functions

uint16_t OFtoFO    (void);         //  0
uint16_t FStoFO    (void);         //  1
uint16_t FStoSP    (void);         //  2
uint16_t FOtoOF    (void);         //  3
uint16_t SPtoOF    (void);         //  4
uint16_t STtoFS    (void);         //  5
uint16_t XXtoFS    (void);         //  6
uint16_t STtoSP    (void);         //  7
uint16_t XXtoSP    (void);         //  8
uint16_t OFtoST    (void);         //  9
uint16_t XXtoSA    (void);         // 10
uint16_t STtoTS    (void);         // 11
uint16_t XXtoTS    (void);         // 12
uint16_t TStoSB    (void);         // 13
uint16_t TStoAB    (void);         // 14
uint16_t SBtoIL    (void);         // 15
uint16_t ARtoIL    (void);         // 16
uint16_t RNtoIL    (void);         // 17
uint16_t ABtoIL    (void);         // 18
uint16_t SAtoAB    (void);         // 19
uint16_t ILtoTC    (void);         // 20
uint16_t ILtoAR    (void);         // 21
uint16_t ARtoRN    (void);         // 22
uint16_t RNtoAB    (void);         // 23
uint16_t SBtoTC    (void);         // 24
uint16_t TCtoCY    (void);         // 25

//-----------------------------------------------------------------------------------------------------------

//  State variables & functions(14)

// the order is based in enum pc_state_machine_states !!!!

TRANSITIONS_CLASS_VARS_EXT char pc_str[]
#ifdef TRANSITIONS_CLASS_GLOBALS
= "FOOFFSSPSTSATSSBILTCARRNABCY"
#endif
;

// the order is based in enum state_pc_transitions !!!!

TRANSITIONS_CLASS_VARS_EXT struct transition pc_transitions[]
#ifdef TRANSITIONS_CLASS_GLOBALS
= {
    {OFtoFO, FGC_PC_FLT_OFF     },          //  0     tr_OFtoFO      off to fault (off)
    {FStoFO, FGC_PC_FLT_OFF     },          //  1     tr_FStoFO      fault (stopping) to fault (off)
    {FStoSP, FGC_PC_STOPPING    },          //  2     tr_FStoSP      fault (stopping) to stopping
    {FOtoOF, FGC_PC_OFF         },          //  3     tr_FOtoOF      fault (off) to off
    {SPtoOF, FGC_PC_OFF         },          //  4     tr_SPtoOF      stopping to off
    {STtoFS, FGC_PC_FLT_STOPPING},          //  5     tr_STtoFS      starting to fault (stopping)
    {XXtoFS, FGC_PC_FLT_STOPPING},          //  6     tr_XXtoFS      ??? to fault (stopping)
    {STtoSP, FGC_PC_STOPPING    },          //  7     tr_STtoSP      starting to stopping
    {XXtoSP, FGC_PC_STOPPING    },          //  8     tr_XXtoSP      ??? to stopping
    {OFtoST, FGC_PC_STARTING    },          //  9     tr_OFtoST      off to starting
    {XXtoSA, FGC_PC_SLOW_ABORT  },          // 10     tr_XXtoSA      ??? to slow abort
    {STtoTS, FGC_PC_TO_STANDBY  },          // 11     tr_STtoTS      starting to to_standBy
    {XXtoTS, FGC_PC_TO_STANDBY  },          // 12     tr_XXtoTS      ??? to to_standBy
    {TStoSB, FGC_PC_ON_STANDBY  },          // 13     tr_TStoSB      to_standBy to on_standBy
    {TStoAB, FGC_PC_ABORTING    },          // 14     tr_TStoAB      to_standBy to aborting
    {SBtoIL, FGC_PC_IDLE        },          // 15     tr_SBtoIL      standBy to idle
    {ARtoIL, FGC_PC_IDLE        },          // 16     tr_ARtoIL      armed to idle
    {RNtoIL, FGC_PC_IDLE        },          // 17     tr_RNtoIL      running to idle
    {ABtoIL, FGC_PC_IDLE        },          // 18     tr_ABtoIL      aborting to idle
    {SAtoAB, FGC_PC_ABORTING    },          // 19     tr_SAtoAB      slow abort to aborting
    {ILtoTC, FGC_PC_TO_CYCLING  },          // 20     tr_ILtoTC      idle to to_cycling
    {ILtoAR, FGC_PC_ARMED       },          // 21     tr_ILtoAR      idle to armed
    {ARtoRN, FGC_PC_RUNNING     },          // 22     tr_ARtoRN      armed to running
    {RNtoAB, FGC_PC_ABORTING    },          // 23     tr_RNtoAB      running to aborting
    {SBtoTC, FGC_PC_TO_CYCLING  },          // 24     tr_SBtoTC      on_standby to to_cycling
    {TCtoCY, FGC_PC_CYCLING     },          // 25     tr_TCtoCY      to_cycling to cycling (running pulse to pulse modulation)
}
#endif
;

// the 1st transitions checked is the rightmost and the last checked is the leftmost
// the first one that meet the requirements is the one chosen

#ifdef TRANSITIONS_CLASS_GLOBALS
uint8_t trans_FO[] = { tr_FOtoOF,};
uint8_t trans_OF[] = { tr_OFtoFO, tr_OFtoST,};
uint8_t trans_FS[] = { tr_FStoFO, tr_FStoSP,};
uint8_t trans_SP[] = { tr_XXtoFS, tr_SPtoOF,};
uint8_t trans_ST[] = { tr_STtoFS, tr_STtoSP, tr_STtoTS,};
uint8_t trans_SA[] = { tr_XXtoFS, tr_XXtoSP, tr_SAtoAB,};
uint8_t trans_TS[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_TStoAB, tr_TStoSB,};
uint8_t trans_SB[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_SBtoIL, tr_SBtoTC,};
uint8_t trans_IL[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS, tr_ILtoAR, tr_ILtoTC,};
uint8_t trans_TC[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS, tr_TCtoCY,};
uint8_t trans_AR[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS, tr_ARtoIL, tr_ARtoRN,};
uint8_t trans_RN[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS, tr_RNtoIL, tr_RNtoAB,};
uint8_t trans_AB[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS, tr_ABtoIL,};
uint8_t trans_CY[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS,};
#endif

// the order is based in enum pc_state_machine_states !!!!
TRANSITIONS_CLASS_VARS_EXT struct state pc_states[]       // The order must match the state constant values in the XML
#ifdef TRANSITIONS_CLASS_GLOBALS
= {
    {StateFO, ArrayLen(trans_FO), trans_FO},    // FGC_PC_FLT_OFF
    {StateOF, ArrayLen(trans_OF), trans_OF},    // FGC_PC_OFF
    {StateFS, ArrayLen(trans_FS), trans_FS},    // FGC_PC_FLT_STOPPING
    {StateSP, ArrayLen(trans_SP), trans_SP},    // FGC_PC_STOPPING
    {StateST, ArrayLen(trans_ST), trans_ST},    // FGC_PC_STARTING
    {StateSA, ArrayLen(trans_SA), trans_SA},    // FGC_PC_SLOW_ABORT
    {StateTS, ArrayLen(trans_TS), trans_TS},    // FGC_PC_TO_STANDBY
    {StateSB, ArrayLen(trans_SB), trans_SB},    // FGC_PC_ON_STANDBY
    {StateIL, ArrayLen(trans_IL), trans_IL},    // FGC_PC_IDLE
    {StateTC, ArrayLen(trans_TC), trans_TC},    // FGC_PC_TO_CYCLING
    {StateAR, ArrayLen(trans_AR), trans_AR},    // FGC_PC_ARMED
    {StateRN, ArrayLen(trans_RN), trans_RN},    // FGC_PC_RUNNING
    {StateAB, ArrayLen(trans_AB), trans_AB},    // FGC_PC_ABORTING
    {StateCY, ArrayLen(trans_CY), trans_CY},    // FGC_PC_CYCLING
}
#endif
;
//-----------------------------------------------------------------------------------------------------------

#endif  // TRANSITIONS_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: transitions_class.h
\*---------------------------------------------------------------------------------------------------------*/
