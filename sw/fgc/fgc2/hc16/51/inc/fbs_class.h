/*---------------------------------------------------------------------------------------------------------*\
  File:         fbs_class.h

  Contents:

  Notes:

\*---------------------------------------------------------------------------------------------------------*/

#ifndef FBS_CLASS_H      // header encapsulation
#define FBS_CLASS_H

#ifdef FBS_CLASS_GLOBALS
    #define FBS_CLASS_VARS_EXT
#else
    #define FBS_CLASS_VARS_EXT extern
#endif
//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

// Status variable constants

#define FAULTS                  fbs.u.fgc_stat.class_data.c51.st_faults
#define WARNINGS                fbs.u.fgc_stat.class_data.c51.st_warnings
#define ST_LATCHED              fbs.u.fgc_stat.class_data.c51.st_latched
#define ST_UNLATCHED            fbs.u.fgc_stat.class_data.c51.st_unlatched
#define STATE_PLL               fbs.u.fgc_stat.class_data.c51.state_pll
#define STATE_OP                fbs.u.fgc_stat.class_data.c51.state_op
#define STATE_VS                fbs.u.fgc_stat.class_data.c51.state_vs
#define STATE_PC                fbs.u.fgc_stat.class_data.c51.state_pc
#define ST_MEAS_A               fbs.u.fgc_stat.class_data.c51.st_meas_a
#define ST_MEAS_B               fbs.u.fgc_stat.class_data.c51.st_meas_b
#define ST_DCCT_A               fbs.u.fgc_stat.class_data.c51.st_dcct_a
#define ST_DCCT_B               fbs.u.fgc_stat.class_data.c51.st_dcct_b
#define I_ERR_MA                fbs.u.fgc_stat.class_data.c51.i_err_ma
#define I_DIFF_MA               fbs.u.fgc_stat.class_data.c51.i_diff_ma
#define I_EARTH_CPCNT           fbs.u.fgc_stat.class_data.c51.i_earth_cpcnt
#define REF                     fbs.u.fgc_stat.class_data.c51.i_ref
#define I_REF                   fbs.u.fgc_stat.class_data.c51.i_ref
#define I_MEAS                  fbs.u.fgc_stat.class_data.c51.i_meas
#define V_REF                   fbs.u.fgc_stat.class_data.c51.v_ref
#define V_MEAS                  fbs.u.fgc_stat.class_data.c51.v_meas

#define REG_STATE               dpcls.dsp.reg.state.byte[3]
#define B_MEAS                  dpcls.dsp.meas.b

//-----------------------------------------------------------------------------------------------------------

#endif  // FBS_CLASS_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fbs_class.h
\*---------------------------------------------------------------------------------------------------------*/
