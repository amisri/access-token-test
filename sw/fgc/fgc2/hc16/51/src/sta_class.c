/*---------------------------------------------------------------------------------------------------------*\
  File:         sta_class.c

  Purpose:      FGC HC16 Class 51/53 Software - State Functions.

  Author:       Quentin.King@cern.ch

  Notes:        This file contains the functions to support the Power Converter
                State Machine
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h
#define STA_CLASS_GLOBALS   // define vs global variable
#define STA_GLOBALS         // define sta global variable

#include <stdbool.h>
#include <sta_class.h>
#include <cmd.h>
#include <class.h>              // for DEV_STA_TSK_PHASE
#include <fbs_class.h>          // for FAULTS, WARNINGS, ST_LATCHED, ST_UNLATCHED, STATE_OP, STATE_VS, STATE_PC, ST_DCCT_A, ST_DCCT_B
#include <defprops.h>
#include <definfo.h>            // for FGC_CLASS_ID
#include <defconst.h>           // For FGC_LOG_RUNNING
#include <dev.h>                // for dev global variable
#include <fbs.h>                // for fbs global variable
#include <mst.h>                // for mst global variable
#include <dpcom.h>              // for dpcom global variable
#include <crate.h>              // for crate global variable
#include <macros.h>             // for Test(), Set()
#include <sta.h>                // for sta global variable
#include <diag.h>               // for diag global variable
#include <os.h>                 // for OSTskSuspend()
#include <dpcls.h>              // for dpcls global variable
#include <cal_class.h>          // for cal global variable
#include <fgc_errs.h>           // for FGC_BAD_STATE
#include <log.h>                // for LogStartAll()
#include <log_class.h>          // for timing_log global variable, LogEvtTiming()
#include <rtd.h>                // for rtd global variable
#include <state_class.h>        // for STAF_XXXX ...
#include <transitions_class.h>  // for pc_states[], pc_transitions[]
#include <diag_class.h>         // for DiagCheckConverter()
#include <mcu_dependent.h>      // for ENTER_BB(), EXIT_BB(), LED_SET()
#include <adc_class.h>          // for AdcInit(), AdcFiltersSetIndex()
#include <memmap_mcu.h>         // for SM_UXTIME_P, DIG_OP_P
#include <shared_memory.h>      // for FGC_MAINPROG_RUNNING
#include <mcu_dsp_common.h>     // For struct abs_time_us/ms

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void StaTsk(void *unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the PC State manager task.  It is triggered by OSTskResume() in MstTsk() on millisecond
  2 and 7, provided the FGC is configured.
\*---------------------------------------------------------------------------------------------------------*/
{
    sta.gw_pc_permit = fbs.gw_timeout;  // Set GW PC_PERMIT down counter
    cal.inhibit_cal  = 30;              // Suppress auto-calibration for the first 30s

    for(;;)                             // Main loop @ 200Hz
    {
        OSTskSuspend();                         // Wait for OSTskResume() from MstTsk();

        sta.sec_f     = (SM_MSTIME_P == DEV_STA_TSK_PHASE);

        sta.time_ms  += 5L;                             // Increment state time by 5ms
        sta.timestamp_ms.unix_time = SM_UXTIME_P;       // Calculate iteration timestamp for logging
        sta.timestamp_ms.ms_time   = SM_MSTIME_P;

        AbsTimeCopyFromMsToUs(&sta.timestamp_ms, &sta.timestamp_us);

        if(STATE_OP != FGC_OP_UNCONFIGURED)     // If configured
        {
            StaOpState();                               // Control operational state

#if FGC_CLASS_ID == 51          // PC_LHC
            StaDdips();                                 // Read or simulate direct digital i/o
#else                           // POPS
            StaDdipsPops();                             // Read or simulate direct digital i/o
#endif
            StaVsState();                               // Evaluate voltage source state
            StaCalControl();                            // Control calibration sequences
            StaAdcControl();                            // Control ADC multiplexors
            StaCheck();                                 // Run health checks
            StaLeds();                                  // Set front panel LEDs
            StaPcState();                               // Control Power Converter state

#if FGC_CLASS_ID == 51          // PC_LHC
            DiagCheckConverter();                       // Check sub converter & FW diode status (at 20ms)
#endif
            StaCmdControl();                            // Control direct command outputs
            if(!fbs.id ||                               // If FIP not connected (standalone), or
               *((uint16_t*)&sta.timestamp_us.unix_time)) // UTC time received
            {
                LogEvtProp();                                   // Log property changes in event log

#if FGC_CLASS_ID != 51          // Not PC_LHC
                if(STATE_PC != FGC_PC_CYCLING && timing_log.out_idx < FGC_LOG_CYCLES_LEN)
                {
                    LogEvtTiming();                                     // Log cycle datas in event log
                }
#endif

                if(diag.n_dims)                                 // If system contains DIMs
                {
                    ENTER_BB();
                    LogEvtDim();                                        // Log DIM statuses in the event log
                    EXIT_BB();
                }
            }

#if FGC_CLASS_ID == 53                       // POPS
           StaPubCyclingProp();
#endif
        }
        else                                    // else unconfigured
        {
            STATE_PC = FGC_PC_FLT_OFF;                  // Force
            StaCheck();                                 // Run health checks
            StaLeds();                                  // Set front panel LEDs
        }

        sta.watchdog = 0;                       // Flag that StaTsk has completed it's iteration
    }
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*/
void StaInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the interface and states related to the power converter.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dig_ip2;

    dpcom.mcu.interfacetype = SM_INTERFACETYPE_P;       // Transfer interface type to DSP

    AdcInit();                                          // ADC inits

    dig_ip2 = DIG_IP2_P;                                // Force word read on secondary digital inputs

    crate.type     =    dig_ip2 & DIG_IP2_BPTYPE_MASK16;
    crate.position = !!(dig_ip2 & DIG_IP2_BPSIDE_MASK16);

    rtd.ctrl              = FGC_RTD_OFF;
    STATE_OP              = FGC_OP_UNCONFIGURED;
    STATE_PC              = FGC_PC_OFF;                 // Start PC state in OFF
    dpcls.mcu.state_pc    = STATE_PC;                   // Transfer state to DSP

    sta.copy_log_capture_state = FGC_LOG_RUNNING;
    sta.copy_log_capture_user  = -1;
    sta.copy_log_capture_cyc_chk_time.unix_time = 0;
    sta.copy_log_capture_cyc_chk_time.us_time   = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void StaOpState(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to control the operational state.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(STATE_OP != FGC_OP_CALIBRATING)        // If not auto calibrating
    {
        STATE_OP = sta.mode_op;                         // Set the state to equal the mode
    }

    if(STATE_OP != (uint16_t)dpcom.mcu.state_op)  // If state has changed
    {
        if(STATE_OP == FGC_OP_SIMULATION)                       // If simulating voltage source
        {
            if((uint16_t)dpcom.mcu.state_op != FGC_OP_CALIBRATING)// If not calibrating before
            {
                vs.sim_intlks = true;                                   // Enable interlock simulation
                sta.cmd_req = 0;                                        // Clear command request register
                sta.inputs  = DIG_IP1_PCPERMIT_MASK16;                  // Simulated PC_PERMIT
                WARNINGS    = 0;                                        // Reset warnings
                FAULTS      = 0;                                        // Reset latched faults
                Clr(sta.faults,FGC_FLT_FGC_STATE);                      // Clear FGC_STATE fault
#if FGC_CLASS_ID == 51

                // If FW_DIODE is present

                if(vs.fw_diode != FGC_VDI_NOT_PRESENT)
                {
                    // Clear FW_DIODE fault bit

                    Clr(sta.faults, FGC_FLT_FW_DIODE);

                    // Clear the VS.FW_DIODE property, but only if the FWD signal is a fault
                    // If it's a status, then the fault should stay latched in the property until it's
                    // cleared manually or by the FGC monitor.

                    if(vs.fwd_is_fault)
                    {
                        vs.fw_diode = FGC_VDI_NO_FAULT;
                    }
                }

                // If FABORT_UNSAFE is present

                if(vs.fabort_unsafe != FGC_VDI_NOT_PRESENT)
                {
                    // Clear the FAU fault state

                    vs.fabort_unsafe = FGC_VDI_NO_FAULT;

                    // Clear ABU fault bit

                    Clr(sta.faults, FGC_FLT_FABORT_UNSAFE);
                }
#endif
                if(vs.polarity_timeout)                                 // If polarity switch expected
                {
                    Set(sta.inputs,DIG_IP1_POLSWIPOS_MASK16);                   // Simulate in positive pos
                }
            }
            Set(WARNINGS,FGC_WRN_SIMULATION);                           // Set SIMULATION warning
        }
        else                                            // else not simulating
        {
            Clr(WARNINGS,FGC_WRN_SIMULATION);                           // Clear SIMULATION warning

            if(STATE_OP == FGC_OP_NORMAL ||                     // If state is NORMAL or
               STATE_OP == FGC_OP_CALIBRATING)                  // CALIBRATING
            {
                Clr(sta.faults,FGC_FLT_FGC_STATE);                      // Clear FGC_STATE fault
            }
            else                                                // else
            {
                Set(sta.faults,FGC_FLT_FGC_STATE);                      // Set FGC_STATE fault
            }
        }

        dpcom.mcu.state_op = STATE_OP;                  // Copy state to DPRAM
    }

    if(STATE_OP == FGC_OP_NORMAL && !(uint16_t)dpcls.dsp.load_select && !(uint16_t)dpcls.mcu.cal.active)
    {
        Set(ST_UNLATCHED,FGC_UNL_NOMINAL_LOAD);
    }
    else
    {
        Clr(ST_UNLATCHED,FGC_UNL_NOMINAL_LOAD);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StaDdips(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will read amd/or simulate the direct I/O registers for Class 51.  There are two properties
  that control the way the simulation works and three different modes:

            MODE.OP      VS.SIM_INTLKS           VSRUN/VSREADY             All other
          sta.mode_op    vs.sim_intlks             VSPOWERON                signals
        --------------------------------------------------------------------------------------
        1: !SIMULATION  not significant             REAL                      REAL
        2:  SIMULATION     DISABLED               SIMULATED                   REAL
        3:  SIMULATION     ENABLED                SIMULATED                 SIMULATED
        --------------------------------------------------------------------------------------

                     Input/Cmd         Mode 1           Mode 2          Mode 3
                      Signal           NORMAL        !SIM_INTLKS       SIM ALL
                    -----------------------------------------------------------------
                    DIG watchdog        TRIGGERED       TRIGGERED       NON TRIGGERED
                    -----------------------------------------------------------------
                    VSNOCABLE           REAL            REAL            0
                    TIMEOUT             REAL            REAL            0
                    VSRUN               REAL            SIM             SIM
                    PWRFAILURE          REAL            REAL            SIM
                    INTLKSPARE          Software overlay based on PCPERMIT
                    PCPERMIT            REAL            REAL            1
                    PCDISCHRQ           REAL            REAL            0
                    FASTABORT           REAL            REAL            0
                    POLSWINEG           REAL            REAL            SIM
                    POLSWIPOS           REAL            REAL            SIM
                    DCCTAFLT            REAL            REAL            0
                    DCCTBFLT            REAL            REAL            0
                    VSEXTINTLK          REAL            REAL            0
                    VSFAULT             REAL            REAL            0
                    VSREADY             REAL            SIM             SIM
                    VSPOWERON           REAL            SIM             SIM
                    -----------------------------------------------------------------
                    FGCOKCMD            REAL            REAL            SIM
                    INTLKOUTCMD         REAL            REAL            0
                    AFRUNCMD            REAL            SIM             SIM

                    POLTONEGCMD         REAL            REAL            0
                    POLTOPOSCMD         REAL            REAL            0
                    VSRESETCMD          REAL            REAL            0
                    VSRUNCMD            REAL            REAL            SIM
\*---------------------------------------------------------------------------------------------------------*/
{
#if FGC_CLASS_ID == 51

    if(sta.mode_op != FGC_OP_SIMULATION || !vs.sim_intlks)      // If not SIMULATION or sim of INTERLOCKS disabled
    {
        if(sta.mode_op != FGC_OP_SIMULATION)    // ====================== Mode 1: NORMAL OPERATION
        {
            sta.cmd_req = DIG_OP_P;                                     // Read command register
            sta.inputs &= DIG_IP1_INTLKSPARE_MASK16;                    // Clear all inputs except INTLKSPARE
            Set(sta.inputs,(DIG_IP1_P & ~DIG_IP1_INTLKSPARE_MASK16));   // Read inputs while preserving INTLKSPARE
            sta.ip_direct = DIG_IPDIRECT_P;    // Read raw inputs
        }
        else                                    // ====================== Mode 2: SIMULATE VS SIGNALS ONLY
        {
            sta.cmd_req &= (DIG_OP_SET_VSRUNCMD_MASK16 |                        // Clear commands except VSRUN
                            DIG_OP_SET_AFRUNCMD_MASK16);                        // and AFRUN

            Set(sta.cmd_req,(DIG_OP_P & ~(DIG_OP_SET_VSRUNCMD_MASK16 |          // Readback commands except VSRUN
                                          DIG_OP_SET_AFRUNCMD_MASK16)));        // and AFRUN

            sta.inputs &= VS_SIGS;                                      // Preserve simulated VS signals,
            Set(sta.inputs,(DIG_IP1_P & ~(VS_SIGS)));                   // Read inputs except sim VS signals

            if(Test(sta.inputs,(DIG_IP1_PWRFAILURE_MASK16 |             // If any faults occured or
                                DIG_IP1_FASTABORT_MASK16)))             // FAST ABORT received
            {
                Clr(sta.cmd_req,(DIG_OP_SET_VSRUNCMD_MASK16|            // Simulate Dig interface by
                                 DIG_OP_SET_AFRUNCMD_MASK16));          // reseting VSRUN and AFRUN
            }
        }                                       // ====================== Mode 1 & 2: REAL INTERLOCKS

        if(fbs.id && !sta.gw_pc_permit)                         // If gateway PC_PERMIT is missing
        {
             Clr(sta.inputs,DIG_IP1_PCPERMIT_MASK16);                   // Clear PC_PERMIT input
        }

        if(!Test(sta.inputs,DIG_IP1_PCPERMIT_MASK16))           // If PCPERMIT is missing
        {
            Set(sta.inputs,DIG_IP1_INTLKSPARE_MASK16);                  // Latch legacy INTLKSPARE input
        }
    }
    else                                         // ====================== Mode 3: SIMULATE ALL SIGNALS
    {
        sta.inputs &= VS_SIGS;                                  // Preserve key VS signals,

        Set(sta.inputs,DIG_IP1_PCPERMIT_MASK16);                // Set PC_PERMIT

        if(FAULTS)
        {
            Set(sta.inputs,DIG_IP1_PWRFAILURE_MASK16);
        }

        if((uint16_t)vs.polarity_timeout)                         // Simulate simplified polarity switch
        {                                                       // with instant response
            if(vs.req_polarity == FGC_POL_POSITIVE)
            {
                Set(sta.inputs,DIG_IP1_POLSWIPOS_MASK16);
            }
            else if(vs.req_polarity == FGC_POL_NEGATIVE)
            {
                Set(sta.inputs,DIG_IP1_POLSWINEG_MASK16);
            }
        }
    }

    if(sta.mode_op == FGC_OP_SIMULATION)                 // ==================== Modes 2 & 3: SIMULATE ANY SIGNALS
    {
        /*--- Simulate response to VS_RUN request ---*/

        if(Test(sta.cmd_req,DIG_OP_SET_VSRUNCMD_MASK16))                // If VS_RUN command requested
        {
            if(!Test(sta.inputs,DIG_IP1_VSRUN_MASK16))          // If readback of this command missing
            {
                Set(sta.inputs,(DIG_IP1_VSRUN_MASK16|DIG_IP1_VSPOWERON_MASK16)); // Set VS_RUN and POWER_ON
            }
        }
        else                                                    // else VS_RUN command not requested
        {
            if(Test(sta.inputs,DIG_IP1_VSRUN_MASK16))                   // If read back of this command present
            {
                Clr(sta.inputs,(DIG_IP1_VSREADY_MASK16|DIG_IP1_VSRUN_MASK16));  // Reset VSREADY and VS_RUN
            }
        }

        /*--- Simulate starting (3s) and stopping (5s) of voltage source ---*/

        if(STATE_PC == FGC_PC_STARTING && sta.time_ms > 3000L)
        {
            Set(sta.inputs,DIG_IP1_VSREADY_MASK16);
        }

        if(  (sta.inputs & (DIG_IP1_VSRUN_MASK16|DIG_IP1_VSPOWERON_MASK16)) == DIG_IP1_VSPOWERON_MASK16
           && sta.time_ms > 5000L)
        {
            Clr(sta.inputs,DIG_IP1_VSPOWERON_MASK16);
        }
    }

    // Block DISCHRQ except for kA (RQ) and thyristor 2 bridge (RB)

    if (    (crate.type != FGC_CRATE_TYPE_PC_THYRISTOR2)
         && (crate.type != FGC_CRATE_TYPE_PC_KA)
       )
    {
        Clr(sta.inputs,DIG_IP1_PCDISCHRQ_MASK16);                       // Mask discharge request signal
    }

    // Determine polarity changer state from input signals

    Clr(ST_UNLATCHED,(FGC_UNL_POL_SWI_POS|FGC_UNL_POL_SWI_NEG));

    if((uint16_t)vs.polarity_timeout)
    {
        if(sta.mode_op != FGC_OP_SIMULATION)
        {
            vs.polarity = 0;

            if(Test(sta.inputs,DIG_IP1_POLSWIPOS_MASK16))
            {
                vs.polarity = FGC_POL_POSITIVE;
                Set(ST_UNLATCHED,FGC_UNL_POL_SWI_POS);
            }
            if(Test(sta.inputs,DIG_IP1_POLSWINEG_MASK16))
            {
                vs.polarity = FGC_POL_NEGATIVE;
                Set(ST_UNLATCHED,FGC_UNL_POL_SWI_NEG);
            }
            if(Test(sta.inputs,DIG_IP1_POLSWIPOS_MASK16) &&
            Test(sta.inputs,DIG_IP1_POLSWINEG_MASK16))
            {
                vs.polarity = FGC_POL_SWITCH_FAULT;
            }
            else if(!Test(sta.inputs,DIG_IP1_POLSWIPOS_MASK16) &&
                    !Test(sta.inputs,DIG_IP1_POLSWINEG_MASK16))
            {
                vs.polarity = FGC_POL_SWITCH_MOVING;
            }
            else if(!vs.req_polarity &&
            (vs.polarity == FGC_POL_NEGATIVE || vs.polarity == FGC_POL_POSITIVE))
            {
                vs.req_polarity = vs.polarity;
            }
        }
        else
        {
            // The polarity switch is not simulated so set it to NO_SWITCH 
            // irrespective of the timeout

            vs.polarity = FGC_POL_NO_SWITCH;
        }
    }
    else
    {
        vs.polarity = FGC_POL_NO_SWITCH;
    }
#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void StaDdipsPops(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will read amd/or simulate the direct I/O registers for the PS POPS (Class 53).

            MODE.OP       VSRUN/VSREADY            All other
          sta.mode_op       VSPOWERON               signals
        -------------------------------------------------------------
        1: !SIMULATION        REAL                    REAL
        3:  SIMULATION      SIMULATED               SIMULATED
        -------------------------------------------------------------

                     Input/Cmd         Mode 1           Mode 3
                      Signal           NORMAL          SIM ALL
                    -------------------------------------------------
                    DIG watchdog        TRIGGERED       NON TRIGGERED
                    -------------------------------------------------
                    VSNOCABLE           0               0
                    TIMEOUT             0               0
                    VSRUN               SIM             SIM
                    PWRFAILURE          SIM             SIM
                    INTLKSPARE          0               0
                    PCPERMIT            1               1
                    PCDISCHRQ           0               0
                    FASTABORT           0               0
                    POLSWINEG           0               0
                    POLSWIPOS           0               0
                    DCCTAFLT            REAL            0
                    DCCTBFLT            REAL            0
                    VSEXTINTLK          0               0
                    VSFAULT             0               0
                    VSREADY             From POPS       SIM from PAL.LINKS.STATUS
                    VSPOWERON           From POPS       SIM from PAL.LINKS.STATUS
                    -------------------------------------------------
                    FGCOKCMD            REAL            SIM
                    INTLKOUTCMD         REAL            0

                    PERMITBEAMCMD       REAL            0
                    NOTIFYRFCMD         REAL            0
                    TUNNELBEEPCMD       REAL            0
                    REALBDOTCMD         REAL            0
                    VSRESETCMD          REAL            0
                    VSRUNCMD            REAL            SIM
\*---------------------------------------------------------------------------------------------------------*/
{
#if FGC_CLASS_ID == 53

    // Process and merge inputs from digital interface and from POPS link

    sta.inputs = DIG_IP1_PCPERMIT_MASK16;               // Force PC PERMIT to be always active

    if(sta.mode_op != FGC_OP_SIMULATION)                    // If not simulating
    {
        if(dpcls.dsp.pal.state == FGC_PAL_LINK_FAULT)       // If link is bad
        {
            pops.status = 0;                                    // Clear POPS status to shutdown
        }
        else                                                // else link is good
        {
            pops.status = (uint16_t)dpcls.mcu.pal.lk2.clc_pal_pops_stat;  // Get low word containing POPS status
        }

        Set(sta.inputs, DIG_IP1_P & (DIG_IP1_DCCTAFLT_MASK16|DIG_IP1_DCCTBFLT_MASK16));
    }

    // Get control byte and mask to keep only non-toggling bits for event log

    pops.loggedcontrol = ((uint16_t*)dpcls.dsp.pal.fgc2pbl)[2] &
                         (FGC_POPS_CONTROL_READY | FGC_POPS_CONTROL_POPSRUN | FGC_POPS_CONTROL_SIMPOPS);

    /*--- POPS STANDBY_REQ -> START flag and VSPOWERON---*/

    if(Test(pops.status, FGC_POPS_STATUS_STANDBY_REQ) &&        // If POPS requires ON_STANDBY, and
       STATE_PC > FGC_PC_ON_STANDBY)                            // PC state is above ON_STANDBY
    {
        sta.flags |= STAF_TO_STANDBY;                           // Set flag to move to TO_STANDBY
    }

    /*--- POPS STARTING -> START flag ---*/

    if(Test(pops.status, FGC_POPS_STATUS_STARTING) &&           // If POPS starting, and
       STATE_PC != FGC_PC_STARTING)                             // PC state is not yet starting
    {
        LogStartAll();                                                  // Restart PM logging
        // Clear IDLE and CYCLING flags
        sta.flags &= ~(STAF_IDLE | STAF_CYCLING);
        sta.flags |= STAF_START;                                // Switch to starting
    }

    /*--- POPS STOPPING -> VSRUN command ---*/

    if(Test(pops.status, FGC_POPS_STATUS_STOPPING) &&           // If POPS stopping
       Test(sta.cmd_req, DIG_OP_SET_VSRUNCMD_MASK16))           // and VSRUN active
    {
        Set(sta.cmd,DDOP_CMD_OFF);                                    // Switch off VS_RUN
    }

    /*--- VSRUN command active ---*/

    if(Test(sta.cmd_req, DIG_OP_SET_VSRUNCMD_MASK16))            // If VSRUN command active
    {
        Set(sta.inputs, DIG_IP1_VSRUN_MASK16);                          // Set VSRUN status

        /*--- POPS ON -> VSREADY ---*/

        if(Test(pops.status, FGC_POPS_STATUS_ON))                       // If POPS on
        {
            Set(sta.inputs, DIG_IP1_VSREADY_MASK16);                        // Set VSREADY
        }

        /*--- POPS STOPPING or OFF -> Stop VSRUN ---*/

        if(Test(pops.status, FGC_POPS_STATUS_STOPPING) ||               // If POPS STOPPPING or
          !Test(pops.status, FGC_POPS_STATUS_STOPPING |                 // POPS is now off
                             FGC_POPS_STATUS_STARTING |
                             FGC_POPS_STATUS_ON))
        {
            Set(sta.cmd,DDOP_CMD_OFF);                                        // Switch off VS_RUN
        }
    }

    /*--- VSPOWERON STATUS ---*/

    if(Test(sta.cmd_req, DIG_OP_SET_VSRUNCMD_MASK16) ||          // If VSRUN command active, or
       Test(pops.status, FGC_POPS_STATUS_STOPPING))             // POPS stopping
    {
        Set(sta.inputs, DIG_IP1_VSPOWERON_MASK16);                      // Set VSPOWERON status
    }

    /*--- POPS FAULT -> VSFAULT ---*/

    if(Test(pops.status, FGC_POPS_STATUS_FAULT))
    {
        Set(FAULTS,FGC_FLT_VS_FAULT);
    }
    else
    {
        Clr(FAULTS,FGC_FLT_VS_FAULT);
    }

    /*--- PWRFAILURE ---*/

    if(FAULTS)
    {
        Set(sta.inputs,DIG_IP1_PWRFAILURE_MASK16);
    }
#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void StaLeds(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set some of the LEDs.  This must be done with LED_SET() and LED_RST() macros, which
  are atomic and not vulnerable to interrupts.
\*---------------------------------------------------------------------------------------------------------*/
{
    /*--- PSU LED ---*/

    uint8_t dcct_flt_mask;
    uint8_t dcct_flt;

    // Implementation note: On class 53 the following test is always false, on purpose.

#if (FGC_CLASS_ID != 53)
    if(Test(WARNINGS,FGC_WRN_DCCT_PSU))
    {
#endif
        LED_SET(PSU_RED);                       // RED
        LED_RST(PSU_GREEN);
#if (FGC_CLASS_ID != 53)
    }
    else
#endif
    if(Test(WARNINGS,FGC_WRN_FGC_PSU))
    {
        LED_SET(PSU_RED);                       // ORANGE
        LED_SET(PSU_GREEN);
    }
    else
    {
        LED_RST(PSU_RED);                       // GREEN
        LED_SET(PSU_GREEN);
    }

    /*--- VS LED ---*/

    if(Test(sta.inputs,DIG_IP1_VSPOWERON_MASK16))
    {
        LED_SET(VS_GREEN);
    }
    else
    {
        LED_RST(VS_GREEN);
    }

    if(Test(sta.inputs,(DIG_IP1_VSFAULT_MASK16 | DIG_IP1_VSEXTINTLK_MASK16)) || Test(FAULTS,FGC_FLT_VS_STATE))
    {
        LED_SET(VS_RED);
    }
    else
    {
        LED_RST(VS_RED);
    }

    /*--- DCCT LED ---*/

    LED_RST(DCCT_GREEN);
    LED_RST(DCCT_RED);

    dcct_flt_mask =   (!!(ST_DCCT_A & FGC_DCCT_FAULT) << 0)
                    + (!!(ST_DCCT_B & FGC_DCCT_FAULT) << 1);
    dcct_flt      = dcct_flt_mask & (dpcls.mcu.dcct_select + 1);

    if (dcct_flt == 0)
    {
        LED_SET(DCCT_GREEN);
    }
    else if (dcct_flt == 3)
    {
        LED_SET(DCCT_RED);
    }
    else
    {
        LED_SET(DCCT_RED);

        if (dpcls.mcu.dcct_select == FGC_DCCT_SELECT_AB)
        {
            LED_SET(DCCT_GREEN);
        }
    }

#if FGC_CLASS_ID == 51
    /*--- PIC LED ---*/

    if( (FAULTS & FGC_FLT_FAST_ABORT) || (ST_UNLATCHED & (FGC_UNL_PWR_FAILURE | FGC_UNL_PWR_FAILURE)) )
    {
        LED_SET(PIC_RED);
    }
    else
    {
        LED_RST(PIC_RED);
    }

    if(ST_UNLATCHED & FGC_UNL_PC_PERMIT)
    {
        LED_SET(PIC_GREEN);
    }
    else
    {
        LED_RST(PIC_GREEN);
    }
#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void StaVsState(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to determine the voltage source state from five direct digital inputs using
  a loopup table.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              idx;
    uint16_t              state_vs;

    static uint8_t        vs_state[] =
    {                           //     IDX  VSRUN:VSPOWERON:VSREADY:FASTABORT:VSFAULT|VSEXTINTLK
        FGC_VS_OFF,             //      0       0       0       0       0       0
        FGC_VS_FLT_OFF,         //      1       0       0       0       0       1
        FGC_VS_FASTPA_OFF,      //      2       0       0       0       1       0
        FGC_VS_FLT_OFF,         //      3       0       0       0       1       1
        FGC_VS_INVALID,         //      4       0       0       1       0       0
        FGC_VS_INVALID,         //      5       0       0       1       0       1
        FGC_VS_INVALID,         //      6       0       0       1       1       0
        FGC_VS_INVALID,         //      7       0       0       1       1       1
        FGC_VS_STOPPING,        //      8       0       1       0       0       0
        FGC_VS_FAST_STOP,       //      9       0       1       0       0       1
        FGC_VS_FAST_STOP,       //      10      0       1       0       1       0
        FGC_VS_FAST_STOP,       //      11      0       1       0       1       1
        FGC_VS_INVALID,         //      12      0       1       1       0       0
        FGC_VS_INVALID,         //      13      0       1       1       0       1
        FGC_VS_INVALID,         //      14      0       1       1       1       0
        FGC_VS_INVALID,         //      15      0       1       1       1       1
        FGC_VS_STARTING,        //      16      1       0       0       0       0
        FGC_VS_FLT_OFF,         //      17      1       0       0       0       1
        FGC_VS_FASTPA_OFF,      //      18      1       0       0       1       0
        FGC_VS_FLT_OFF,         //      19      1       0       0       1       1
        FGC_VS_INVALID,         //      20      1       0       1       0       0
        FGC_VS_INVALID,         //      21      1       0       1       0       1
        FGC_VS_INVALID,         //      22      1       0       1       1       0
        FGC_VS_INVALID,         //      23      1       0       1       1       1
        FGC_VS_STARTING,        //      24      1       1       0       0       0
        FGC_VS_FAST_STOP,       //      25      1       1       0       0       1
        FGC_VS_FAST_STOP,       //      26      1       1       0       1       0
        FGC_VS_FAST_STOP,       //      27      1       1       0       1       1
        FGC_VS_READY,           //      28      1       1       1       0       0
        FGC_VS_INVALID,         //      29      1       1       1       0       1
        FGC_VS_INVALID,         //      30      1       1       1       1       0
        FGC_VS_INVALID          //      31      1       1       1       1       1
    };

/*--- Create state index from the five direct digital inputs ---*/

    idx = 0;

    if(Test(sta.inputs,DIG_IP1_VSFAULT_MASK16|DIG_IP1_VSEXTINTLK_MASK16))
    {
        Set(idx,0x01);
    }

    if(Test(sta.inputs,DIG_IP1_FASTABORT_MASK16))
    {
        Set(idx,0x02);
    }

    if(Test(sta.inputs,DIG_IP1_VSREADY_MASK16))
    {
        Set(idx,0x04);
    }

    if(Test(sta.inputs,DIG_IP1_VSPOWERON_MASK16))
    {
        Set(idx,0x08);
    }

    if(Test(sta.inputs,DIG_IP1_VSRUN_MASK16))
    {
        Set(idx,0x10);
    }

    /*--- Check for unexpected loss of voltage source ---*/

    state_vs = vs_state[idx];

    if(state_vs == FGC_VS_STARTING &&   // If VSRUN active but VSREADY and/or VSPOWERON are not, and
       STATE_PC != FGC_PC_STARTING)     // converter is NOT starting
    {
        state_vs = FGC_VS_INVALID;              // Mark state as invalid
    }

#if FGC_CLASS_ID == 53

    // If POPS faults have been reset the reset FGC faults

    if(state_vs == FGC_VS_OFF && STATE_VS == FGC_VS_FLT_OFF)
    {
        Set(sta.cmd,DDOP_CMD_RESET);
    }
#endif

    STATE_VS = state_vs;
}
#pragma MESSAGE DISABLE C5902
/*---------------------------------------------------------------------------------------------------------*/
void StaAdcControl(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will control the ADC analogue and digital multiplexors (if card is present)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              c;                      // Channel 0=A  1=B
    uint16_t              mpx;
    uint16_t              mpx_flt;
    uint16_t              filter_idx[2];          // SD filter index (0-4)
    uint16_t              adc_one_mhz[2];

    // Note about mpx_check[]: On FGC2, do not define it as a const array, else the linker will map that array
    // to a FAR memory region (maybe a const FAR declaration would work)

    static uint16_t       mpx_check[2][2] =
    {
            { 0x0002, 0x1010 },                 // Internal ADCs
            { 0x0800, 0x9000 }                  // External ADCS
    };

    if(sta.adc_external_f == FGC_CTRL_ENABLED &&                        // If ADC type is EXTERNAL
       !sta.adc_ext_inputs_f)                                           // but ADC card does not support external
    {
        sta.adc_external_f  = FGC_CTRL_DISABLED;                                // Force ADC type to INTERNAL
    }

    if(STATE_OP == FGC_OP_NORMAL || STATE_OP == FGC_OP_SIMULATION)      // If normal or sim_vs
    {
        if(sta.adc_external_f == FGC_CTRL_DISABLED)                     // if internal ADCS
        {
            sta.adc_filter_mpx[0]   = FGC_ADC_FILTER_MPX_ADC16A;                                // Set MPX to ADC16 DCCTs
            sta.adc_filter_mpx[1]   = FGC_ADC_FILTER_MPX_ADC16B;
            sta.internal_adc_mpx[0] = FGC_INTERNAL_ADC_MPX_DCCTA;
            sta.internal_adc_mpx[1] = FGC_INTERNAL_ADC_MPX_DCCTB;
        }
        else                                                            // else ADC type = EXTERNAL
        {
            sta.internal_adc_mpx[0] = FGC_INTERNAL_ADC_MPX_AUX;                 // Set ADC16 MPX to AUX for
            sta.internal_adc_mpx[1] = FGC_INTERNAL_ADC_MPX_AUX;                 // thyristor backplanes

            sta.adc_filter_mpx[0]   = FGC_ADC_FILTER_MPX_ADC22A;                                // Set MPX to ADC22 DCCTs
            sta.adc_filter_mpx[1]   = FGC_ADC_FILTER_MPX_ADC22B;
        }
    }

    if((sta.internal_adc_mpx[0]    != (uint16_t)dpcls.mcu.internal_adc_mpx[0])    ||  //   If internal MPX has changed
       (sta.internal_adc_mpx[1]    != (uint16_t)dpcls.mcu.internal_adc_mpx[1])    ||  // OR
       (sta.adc_ext_inputs_f &&                                                            //   If SD-350/1 and
        dpcls.mcu.adc.filter_state == FGC_FILTER_STATE_READY &&                     //   ADC filters are in ready state
        ((sta.adc_filter_mpx[0]    != (uint16_t)dpcls.mcu.adc_filter_mpx[0])      ||  //   If digital MPX has changed, or
         (sta.adc_filter_mpx[1]    != (uint16_t)dpcls.mcu.adc_filter_mpx[1])      ||
         (sta.adc_filter_select[0] != sta.last_adc_filter_select[0])            ||  //   filter selector changed, or
         (sta.adc_filter_select[1] != sta.last_adc_filter_select[1])            ||
         (sta.adc_one_mhz[0]       != sta.last_adc_one_mhz[0])                  ||  //   ONE_MHZ selector has changed, or
         (sta.adc_one_mhz[1]       != sta.last_adc_one_mhz[1])                  ||
         (sta.adc_ads1281[0]       != sta.last_adc_ads1281[0])                  ||  //   ADS1281 selector has changed
         (sta.adc_ads1281[1]       != sta.last_adc_ads1281[1]))))
    {
        dpcls.mcu.adc_block_f = true;                                   // Block ADCs for 5ms

        for(c=0; c < 2; c++)                                            // For both channels A/B
        {
            dpcls.mcu.adc_filter_mpx   [c] = sta.adc_filter_mpx    [c];
            dpcls.mcu.internal_adc_mpx [c] = sta.internal_adc_mpx  [c];
            sta.last_adc_filter_select [c] = sta.adc_filter_select [c];
            sta.last_adc_one_mhz[c] = sta.adc_one_mhz[c];
            sta.last_adc_ads1281[c] = sta.adc_ads1281[c];

            if(sta.adc_ext_inputs_f)                                            // If ADC interface is an SD-350/351
            {
                if(sta.adc_filter_mpx[c] == FGC_ADC_FILTER_MPX_ADC22A ||
                   sta.adc_filter_mpx[c] == FGC_ADC_FILTER_MPX_ADC22B)
                {
                    adc_one_mhz[c] = sta.adc_one_mhz[(sta.adc_filter_mpx[c] - FGC_ADC_FILTER_MPX_ADC22A)];
                }
                else
                {
                    adc_one_mhz[c] = (sta.adc_filter_mpx[c] >= FGC_ADC_FILTER_MPX_ADC22A);
                }

                if(sta.adc_external_f == FGC_CTRL_DISABLED)             // If internal ADCs then always 500kHz
                {
                    filter_idx[c] = sta.adc_filter_select[c];
                }
                else
                {
                    if(sta.last_adc_ads1281[c])
                    {
                        filter_idx[c] = 4;
                    }
                    else
                    {
                        filter_idx[c] = sta.adc_filter_select[c] + adc_one_mhz[c] * 2;
                    }
                }
            }
            else                                        // else SAR-400/SD-360 interface
            {
                adc_one_mhz[c] = 0;
            }
        }

        if(sta.adc_ext_inputs_f)                                // If ADC interface is an SD-350/351
        {
            AdcFiltersSetIndex(filter_idx[0], filter_idx[1]);

            dpcls.mcu.sd_shift[0]    = adc.sd_shift[filter_idx[0]];
            dpcls.mcu.sd_shift[1]    = adc.sd_shift[filter_idx[1]];
        }
        else                                                    // else SAR_400/SD_360 interface
        {
            dpcls.mcu.sd_shift[0]    = 21;                      // Shift required for SD_360
            dpcls.mcu.sd_shift[1]    = 21;                      // Shift not used for SAR_400
        }

        mpx = (sta.adc_filter_mpx  [0] << ANA_MPX_DIGA_SHIFT) |    // Notify interface
              (sta.internal_adc_mpx[0] << ANA_MPX_ANAA_SHIFT) |
              (adc_one_mhz  [0] << ANA_MPX_FRQA_SHIFT) |
              (sta.adc_filter_mpx  [1] << ANA_MPX_DIGB_SHIFT) |
              (sta.internal_adc_mpx[1] << ANA_MPX_ANAB_SHIFT) |
              (adc_one_mhz  [1] << ANA_MPX_FRQB_SHIFT);

        ANA_MPX_P = mpx;

        dpcls.mcu.adc_mpx_f = true;                             // Signal DSP to set filters
    }
    else
    {
        dpcls.mcu.adc_block_f = false;
    }

    mpx_flt = 0;                                        // Clear MPX fault mask

    if(STATE_OP != FGC_OP_CALIBRATING)                  // If not calibrating
    {
        mpx = ANA_MPX_P;                                        // Readback interface and check mpx settings

        sta.adc_filter_mpx  [0] = (mpx & ANA_MPX_DIGA_MASK16) >> ANA_MPX_DIGA_SHIFT;
        sta.internal_adc_mpx[0] = (mpx & ANA_MPX_ANAA_MASK16) >> ANA_MPX_ANAA_SHIFT;
        sta.adc_filter_mpx  [1] = (mpx & ANA_MPX_DIGB_MASK16) >> ANA_MPX_DIGB_SHIFT;
        sta.internal_adc_mpx[1] = (mpx & ANA_MPX_ANAB_MASK16) >> ANA_MPX_ANAB_SHIFT;

        if((mpx & (ANA_MPX_DIGA_MASK16|ANA_MPX_ANAA_MASK16)) != mpx_check[sta.adc_external_f][0])
        {
            mpx_flt = 0x01;
        }

        if((mpx & (ANA_MPX_DIGB_MASK16|ANA_MPX_ANAB_MASK16)) != mpx_check[sta.adc_external_f][1])
        {
            mpx_flt |= 0x02;
        }
    }
    dpcls.mcu.adc_mpx_flt = mpx_flt;                    // Pass MPX fault mask to DSP
}
#pragma MESSAGE DEFAULT C5902
/*---------------------------------------------------------------------------------------------------------*/
void StaCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to check the operational health of the system.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Faults

    if(Test(sta.inputs,DIG_IP1_VSFAULT_MASK16))
    {
        Set(sta.faults,FGC_FLT_VS_FAULT);
    }
    else
    {
        Clr(sta.faults,FGC_FLT_VS_FAULT);
    }

    if(Test(sta.inputs,DIG_IP1_VSEXTINTLK_MASK16))
    {
        Set(sta.faults,FGC_FLT_VS_EXTINTLOCK);
    }
    else
    {
        Clr(sta.faults,FGC_FLT_VS_EXTINTLOCK);
    }

# if (FGC_CLASS_ID == 51)

    if(Test(sta.inputs,DIG_IP1_FASTABORT_MASK16))
    {
        Set(sta.faults,FGC_FLT_FAST_ABORT);
    }
    else
    {
        Clr(sta.faults,FGC_FLT_FAST_ABORT);
    }

    if(!Test(sta.inputs,DIG_IP1_VSRUN_MASK16))
    {
        if(Test(sta.inputs,DIG_IP1_INTLKSPARE_MASK16))
        {
            Set(sta.faults,FGC_FLT_NO_PC_PERMIT);
        }
        else
        {
            Clr(sta.faults,FGC_FLT_NO_PC_PERMIT);
        }
    }

    /*--- VS State invalid fault test ---*/

    if(STATE_VS == FGC_VS_INVALID)
    {
        vs.vsstate_counter++;

        if(!Test(FAULTS,FGC_FLT_VS_STATE) &&
          ((Test(sta.inputs,DIG_IP1_VSRUN_MASK16) && vs.vsstate_counter > 1) ||
            vs.vsstate_counter >= VS_STATE_INVALID_TO))
        {
            Set(FAULTS,FGC_FLT_VS_STATE);
        }
    }
    else
    {
        vs.vsstate_counter = 0;
    }

    /*--- Polarity switch warning and fault ---*/

    Clr(sta.faults,FGC_FLT_POL_SWITCH);                 // Clear fault

    if(vs.polarity_timeout)                             // If polarity switch is present
    {
        if(vs.polarity == FGC_POL_SWITCH_FAULT)                 // If switch fault (POS and NEG at same time)
        {
            Set(sta.faults,FGC_FLT_POL_SWITCH);                         // Set fault immediately
        }
        else if(vs.polarity_counter < vs.polarity_timeout)      // else if during timeout period after change
        {
            if(sta.sec_f)                                               // If start of the second
            {
               vs.polarity_counter++;                                           // Increment timeout counter
            }
        }
        else if(vs.polarity != vs.req_polarity)                 // else if pol state is not requested state
        {
            Set(sta.faults,FGC_FLT_POL_SWITCH);                         // Set fault
            vs.req_polarity = 0;
        }
    }
#endif
    /*--- Warnings ---*/

    if(Test(ST_LATCHED,(FGC_LAT_ANA_FLT|FGC_LAT_MEM_FLT)))
    {
        Set(WARNINGS,FGC_WRN_FGC_HW);
    }
    else
    {
        Clr(WARNINGS,FGC_WRN_FGC_HW);
    }

    if(Test(ST_LATCHED,(FGC_LAT_FGC_PSU_FAIL|FGC_LAT_PSU_V_FAIL|FGC_LAT_VDC_FAIL)))
    {
        Set(WARNINGS,FGC_WRN_FGC_PSU);
    }
    else
    {
        Clr(WARNINGS,FGC_WRN_FGC_PSU);
    }

    /*--- Unlatched status ---*/

    if(Test(sta.inputs,DIG_IP1_VSPOWERON_MASK16))
    {
        Set(ST_UNLATCHED,FGC_UNL_VS_POWER_ON);
    }
    else
    {
        Clr(ST_UNLATCHED,FGC_UNL_VS_POWER_ON);
    }

#if FGC_CLASS_ID == 51
    if(Test(ST_LATCHED,(FGC_LAT_DCCT_PSU_FAIL|FGC_LAT_DCCT_PSU_STOP)))
    {
        Set(WARNINGS,FGC_WRN_DCCT_PSU);
    }
    else
    {
        Clr(WARNINGS,FGC_WRN_DCCT_PSU);
    }

    if(dpcls.mcu.vs.u_lead_pos_chan == -1)              // If U_LEAD_POS DIM channel not known
    {
        Clr(WARNINGS,FGC_WRN_CURRENT_LEAD);                             // Mask CURRENT_LEAD warning
    }

    /*--- Unlatched status ---*/

    if(Test(sta.inputs,DIG_IP1_PWRFAILURE_MASK16))
    {
        Set(ST_UNLATCHED,FGC_UNL_PWR_FAILURE);
    }
    else
    {
        Clr(ST_UNLATCHED,FGC_UNL_PWR_FAILURE);
    }

    if(Test(sta.inputs,DIG_IP1_PCDISCHRQ_MASK16))
    {
        Set(ST_UNLATCHED,FGC_UNL_PC_DISCH_RQ);
    }
    else
    {
        Clr(ST_UNLATCHED,FGC_UNL_PC_DISCH_RQ);
    }
    if(!Test(sta.inputs,DIG_IP1_PCPERMIT_MASK16))
    {
        Clr(ST_UNLATCHED,FGC_UNL_PC_PERMIT);
    }
    else
    {
        Set(ST_UNLATCHED,FGC_UNL_PC_PERMIT);
    }

    if(Test(sta.inputs,DIG_IP1_VSNOCABLE_MASK16))
    {
        Set(ST_UNLATCHED,FGC_UNL_NO_VS_CABLE);
    }
    else
    {
        Clr(ST_UNLATCHED,FGC_UNL_NO_VS_CABLE);
    }

    if((uint16_t)dpcls.dsp.meas.i_low_f)
    {
        Set(ST_UNLATCHED,FGC_UNL_LOW_CURRENT);
    }
    else
    {
        Clr(ST_UNLATCHED,FGC_UNL_LOW_CURRENT);
    }

    if((uint16_t)dpcls.mcu.mode_rt == FGC_CTRL_ENABLED)
    {
        Set(ST_UNLATCHED,FGC_UNL_REF_RT_ACTIVE);
    }
    else
    {
        Clr(ST_UNLATCHED,FGC_UNL_REF_RT_ACTIVE);
    }

    // Imin Flag

    if((uint16_t)dpcls.dsp.meas.i_min_f)
    {
        sta.flags |=  STAF_IMIN;
    }
    else
    {
        sta.flags &= ~STAF_IMIN;
    }
#else

    /*--- Class 53: TO_STANDBY request from DSP ---*/

    if((uint16_t)dpcls.dsp.cyc.to_standby_f)      // If DSP requests move to ON_STANDBY
    {
        dpcls.dsp.cyc.to_standby_f = false;             // Clear DSP request

        if(STATE_PC == FGC_PC_CYCLING)                  // If in CYCLING state
        {
            sta.flags |= STAF_TO_STANDBY;               // Set flag to request to go to ON_STANDBY
        }
    }
#endif
    /*--- DCCT status for DSP ---*/

    if(Test(sta.inputs,DIG_IP1_DCCTAFLT_MASK16))
    {
        Set(sta.dcct_flts,0x01);
    }

    if(Test(sta.inputs,DIG_IP1_DCCTBFLT_MASK16))
    {
        Set(sta.dcct_flts,0x02);
    }

    dpcls.mcu.dcct_flt = (uint32_t)sta.dcct_flts;

    /*--- Latch new faults ---*/

    Set(FAULTS,sta.faults);
}
/*---------------------------------------------------------------------------------------------------------*/
void StaPcState(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to control the power converter state.  It uses transition functions from
  pcstates.h and state functions in state.c.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t *     t;
    uint16_t      n;

    /*--- Check I_ACCESS flag if SECTOR ACCESS is enabled ---*/

    if((sta.mode_op != FGC_OP_SIMULATION || !vs.sim_intlks) &&  // If not SIMULATION or not sim interlocks, and
       STATE_PC >= FGC_PC_IDLE                          &&      // PC state is IL or higher, and
       fbs.sector_access                                &&      // sector access flag is set and
       (uint16_t)dpcls.dsp.meas.i_access_f)                       // |Imeas| > LOAD.LIMITS.I_ACCESS
    {
        sta.flags |= STAF_TO_STANDBY;                           // Set flag to move to TO_STANDBY
    }

    // Run PC state machine

    t = pc_states[STATE_PC].trans;                              // Get transition list for current state

    for(n = pc_states[STATE_PC].n_trans;n && !pc_transitions[*t].condition();n--,t++);  // Scan transitions

    if(n)                                               // If active transition found
    {
        sta.time_ms = 0L;                                       // Reset state time
        n = pc_transitions[*t].next_state;                      // Get next state number
        dpcls.mcu.state_pc = n;                                 // Pass new state to DSP

        pc_states[n].state_func(true);                          // Run next state function for the first time

        STATE_PC = n;                                           // Update state machine
    }
    else                                                // else no transitions are active
    {
        pc_states[STATE_PC].state_func(false);                  // Run current state function
    }

#if FGC_CLASS_ID == 51
    // If PC state is not ARMED or RUNNING, clear EVENT_GROUP

    if(ref.event_group && STATE_PC != FGC_PC_ARMED && STATE_PC != FGC_PC_RUNNING)
    {
        ref.event_group = 0;
    }
#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void StaCalControl(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to run the auto calibration of ADCs, DCCTs and DAC.  The function
  CalRunSequence() is called when sta.cal_f is set, which is only every 200ms.

  The function also checks if an automatic calibration of the internal ADCs is required.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(sta.sec_f)
    {
        if(cal.inhibit_cal)                                                 // Down count inhibit_cal time at 1Hz
        {
            cal.inhibit_cal--;
        }
    }

    if(STATE_OP == FGC_OP_CALIBRATING)          // If auto calibrating
    {
        if(((uint16_t)sta.timestamp_ms.ms_time % 200) == (100 + DEV_STA_TSK_PHASE))
        {
            CalRunSequence();
        }
    }
    else if(sta.sec_f                                   &&      // If start of new second and
           !mst.set_cmd_counter                         &&      // no set command run in the past 5 seconds and
            sta.config_mode == FGC_CFG_MODE_SYNC_NONE   &&      // no sync request is outstanding and
           !cal.inhibit_cal                             &&      // calibration is NOT inhibited at the moment and
           (dpcls.mcu.cal.active == 0)                  &&      // CAL.ACTIVE is DISABLED
            dev.set_lock->counter                       &&      // set command not running (might be S CAL) and
            !SetifPcOff(NULL)                           &&      // converter is OFF and
            !SetifCalOk(NULL)                           &&      // states are okay for calibration and
            sta.time_ms > CAL_IN_STATE_DELAY            &&      // time in state is more than minimum
#if FGC_CLASS_ID == 51
            (!fbs.id ||                                         // not FIP connected or
            (fbs.pll_locked_f && Test(ST_UNLATCHED,FGC_UNL_LOW_CURRENT))) && // GW present and LOW CURRENT and time
#endif
            (SM_UXTIME_P - cal.last_cal_time_unix) > CAL_NO_AUTO_TIME) // to calibrate
    {
        dpcls.mcu.cal.action = CAL_REQ_BOTH_ADC16S;
        cal.seq_idx          = 0;
        STATE_OP             = FGC_OP_CALIBRATING;      // Start auto ADC16 calibration
    }
}
#if FGC_CLASS_ID == 51
/*---------------------------------------------------------------------------------------------------------*/
void StaCmdControl(void)
/*---------------------------------------------------------------------------------------------------------*\
  Class 51: This function will drive or simulate the direct command outputs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t set;
    uint16_t rst;
    static uint16_t reset_counter;
    static uint16_t polarity_counter;

    set = rst = 0;                                      // Clear command masks

    /*--- VS_RUN control ---*/

    if(!Test(sta.cmd_req,DIG_OP_SET_VSRUNCMD_MASK16) && // if VS_RUN is not active and
        Test(sta.cmd,DDOP_CMD_ON))                      // ON requested
    {
        Set(set,DIG_OP_SET_VSRUNCMD_MASK16);                    // Set VS_RUN
        sta.cal_active = FGC_CTRL_DISABLED;                     // Disable CAL.ACTIVE property
    }

    if((Test(sta.cmd,DDOP_CMD_OFF) || FAULTS) &&        // OFF requested or faults have been detected and
        Test(sta.cmd_req,DIG_OP_SET_VSRUNCMD_MASK16|    // If VS_RUN is active or
                        DIG_OP_SET_AFRUNCMD_MASK16))    // AFRUN is active
    {
        Set(rst,(DIG_OP_SET_VSRUNCMD_MASK16 |                   // Clear VS_RUN
                 DIG_OP_SET_AFRUNCMD_MASK16));                  // and AFRUN commands
    }

    /*--- AFRUN control (used to turn on Active Filter if present) ---*/

    if(vs.active_filter == FGC_CTRL_ENABLED &&          // If active filter is enabled
       STATE_PC >= FGC_PC_IDLE &&                       // state.pc is IDLE or higher, and
       sta.time_ms > 1000 &&                            // time in state is > 1s, and
       !Test(sta.cmd_req,DIG_OP_SET_AFRUNCMD_MASK16))   // active filter command is OFF
    {
        Set(set,DIG_OP_SET_AFRUNCMD_MASK16);                    // Set AFRUN to enabled active filter
    }
    else if((vs.active_filter == FGC_CTRL_DISABLED ||   // else if (active filter is disabled, or
             STATE_PC < FGC_PC_IDLE) &&                 // state.pc is less than IDLE), and
        Test(sta.cmd_req,DIG_OP_SET_AFRUNCMD_MASK16))   // activer filter command is ON
    {
        Set(rst,DIG_OP_SET_AFRUNCMD_MASK16);                    // Reset AFRUN to disable active filter
    }

    /*--- Polarity switch control ---*/

    if(!polarity_counter)                       // If Polarity command not in progress
    {
        if(Test(sta.cmd,DDOP_CMD_POL_POS))              // If POL POS requested
        {
            polarity_counter = 2;                               // Start polarity command sequence (10ms)

            Set(set,DIG_OP_SET_POLTOPOSCMD_MASK16);                     // Set external POL POS line
            vs.req_polarity = FGC_POL_POSITIVE;
            vs.polarity_counter = 0;
        }
        else if(Test(sta.cmd,DDOP_CMD_POL_NEG))         // If POL NEG requested
        {
            polarity_counter = 2;                               // Start polarity command sequence (10ms)

            Set(set,DIG_OP_SET_POLTONEGCMD_MASK16);                     // Set external POL NEG line
            vs.req_polarity = FGC_POL_NEGATIVE;
            vs.polarity_counter = 0;
        }
    }
    else                                        //else polarity command is in progress
    {
        if(!(--polarity_counter))                       // After 10ms
        {
            Set(rst,(DIG_OP_SET_POLTOPOSCMD_MASK16 | DIG_OP_SET_POLTONEGCMD_MASK16));   // Clear polarity request
        }
    }

    /*--- FGCOKCMD  control ---*/

    if((FAULTS & ~(FGC_FLT_NO_PC_PERMIT | FGC_FLT_FAST_ABORT))) // If non-interlock faults are latched
    {
        if(!Test(sta.cmd_req,DIG_OP_SET_FGCOKCMD_MASK16))       // if FGCOKCMD command is not active
        {
            Set(set,DIG_OP_SET_FGCOKCMD_MASK16);                        // Set FGCOKCMD command
        }
    }
    else                                                // else there are no latched faults
    {
        if(Test(sta.cmd_req,DIG_OP_SET_FGCOKCMD_MASK16))        // if FGCOKCMD command is active
        {
            Set(rst,DIG_OP_SET_FGCOKCMD_MASK16);                        // Clear FGCOKCMD command
        }
    }

    /*--- VSRESETCMD control ---*/

    if(!reset_counter)                          // If reset not in progress
    {
        if(Test(sta.cmd,DDOP_CMD_RESET))                        // If reset requested
        {
            reset_counter = 5;                                  // Reset sequence is 20ms

            Set(set,DIG_OP_SET_VSRESETCMD_MASK16);                      // Set external reset line
        }
    }

    if(reset_counter)
    {
        FAULTS          = (sta.faults | (uint16_t)dpcom.dsp.faults);      // Clear faults latch
        vs.req_polarity = 0;                                            // Clear requested polarity state
        sta.dcct_flts   = 0;                                            // Clear DCCT faults latch

        if(Test(sta.inputs,DIG_IP1_PCPERMIT_MASK16))    // If PCPERMIT is present
        {
            Clr(sta.inputs,DIG_IP1_INTLKSPARE_MASK16);                      // Clear INTLKSPARE latch
        }

        if(!(--reset_counter))                          // After 20ms of reset sequence
        {
            Set(rst,DIG_OP_SET_VSRESETCMD_MASK16);                      // Clear external reset line
        }
    }

    /*--- Send or simulate command to digital interface ---*/

    if(sta.mode_op == FGC_OP_SIMULATION)                // If operating mode is SIMULATION
    {
        sta.cmd_req = (sta.cmd_req & ~rst) | set;       // Simulate command

        Clr(set,(DIG_OP_SET_VSRUNCMD_MASK16|            // Block VSRUN and
                 DIG_OP_SET_AFRUNCMD_MASK16));          // AFRUN from being generated
    }

    if(set ^ rst)                               // If any command bits need to be changed
    {
        DIG_OP_P = (rst<<8) | set;                      // Write reset/set mask to command register
    }

    sta.cmd = 0;                        // Clear command variable
}
#else
/*---------------------------------------------------------------------------------------------------------*/
void StaCmdControl(void)
/*---------------------------------------------------------------------------------------------------------*\
  Class 53: This function will drive or simulate the direct command outputs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t set;
    uint16_t rst;
    static uint16_t reset_counter;

    if(STATE_OP == FGC_OP_TEST)                 // If in TEST state
    {
        return;                                                 // Return immediately
    }

    set = rst = 0;                                      // Clear command masks

    /*--- VS_RUN control ---*/

    if(!Test(sta.cmd_req,DIG_OP_SET_VSRUNCMD_MASK16) && // if VS_RUN is not active and
        Test(sta.cmd,DDOP_CMD_ON))                      // ON requested
    {
        Set(set,DIG_OP_SET_VSRUNCMD_MASK16);                    // Set VS_RUN
        sta.cal_active = FGC_CTRL_DISABLED;                     // Disable CAL.ACTIVE property
    }

    if((Test(sta.cmd,DDOP_CMD_OFF) || FAULTS) &&        // OFF requested or faults have been detected and
        Test(sta.cmd_req,DIG_OP_SET_VSRUNCMD_MASK16))   // If VS_RUN is active
    {
        Set(rst,DIG_OP_SET_VSRUNCMD_MASK16);                    // Clear VS_RUN command
    }

    /*--- REAL_BDOT & NOTIFY_RF control ---*/

    if((STATE_PC == FGC_PC_CYCLING ||           // If in CYCLING state, or
        STATE_PC == FGC_PC_TO_CYCLING) &&       // in TO_CYCLING state, and
        STATE_OP == FGC_OP_NORMAL      &&       // normal operation, and
       !(uint16_t)dpcls.dsp.cyc.to_standby_f)     // no outstanding request to return to STANDBY
    {
        if(!Test(sta.cmd_req,DIG_OP_SET_REALBDOTCMD_MASK16))
        {
            Set(set,DIG_OP_SET_REALBDOTCMD_MASK16 |
                    DIG_OP_SET_NOTIFYRFCMD_MASK16);
        }
    }
    else
    {
        if(Test(sta.cmd_req,DIG_OP_SET_REALBDOTCMD_MASK16))
        {
            Set(rst,DIG_OP_SET_REALBDOTCMD_MASK16 |
                    DIG_OP_SET_NOTIFYRFCMD_MASK16);
        }
    }

    /*--- MAGNET_ON control ---*/

    if(STATE_PC > FGC_PC_OFF &&
       STATE_OP == FGC_OP_NORMAL)
    {
        if(!Test(sta.cmd_req,DIG_OP_SET_MAGNETONCMD_MASK16))
        {
            Set(set,DIG_OP_SET_MAGNETONCMD_MASK16);
        }
    }
    else
    {
        if(Test(sta.cmd_req,DIG_OP_SET_MAGNETONCMD_MASK16))
        {
            Set(rst,DIG_OP_SET_MAGNETONCMD_MASK16);
        }
    }

    /*--- PERMIT_BEAM control ---*/

    if(STATE_PC == FGC_PC_CYCLING &&            // If in CYCLING state, and
       STATE_OP == FGC_OP_NORMAL  &&            // normal operation, and
       !(uint16_t)dpcom.mcu.evt.sim_supercycle_f) // not simulating the super-cycle
    {
        if(!Test(sta.cmd_req,DIG_OP_SET_PERMITBEAMCMD_MASK16))
        {
            Set(set,DIG_OP_SET_PERMITBEAMCMD_MASK16);
        }
    }
    else
    {
        if(Test(sta.cmd_req,DIG_OP_SET_PERMITBEAMCMD_MASK16))
        {
            Set(rst,DIG_OP_SET_PERMITBEAMCMD_MASK16);
        }
    }

    /*--- VSRESETCMD control ---*/

    if(!reset_counter)                                  // If reset not in progress
    {
        if(Test(sta.cmd,DDOP_CMD_RESET))                        // If reset requested
        {
            reset_counter = 5;                                          // Reset sequence is 20ms

            Set(set,DIG_OP_SET_VSRESETCMD_MASK16);                      // Set external reset line
        }
    }

    if(reset_counter)
    {
        FAULTS          = (sta.faults | (uint16_t)dpcom.dsp.faults);      // Clear faults latch
        vs.req_polarity = 0;                                            // Clear requested polarity state
        sta.dcct_flts   = 0;                                            // Clear DCCT faults latch

        if(Test(sta.inputs,DIG_IP1_PCPERMIT_MASK16))    // If PCPERMIT is present
        {
            Clr(sta.inputs,DIG_IP1_INTLKSPARE_MASK16);                      // Clear INTLKSPARE latch
        }

        if(!(--reset_counter))                          // After 20ms of reset sequence
        {
            Set(rst,DIG_OP_SET_VSRESETCMD_MASK16);                      // Clear external reset line
        }
    }

    /*--- FGCOKCMD control ---*/

    if((FAULTS))                                        // If faults are latched
    {
        if(!Test(sta.cmd_req,DIG_OP_SET_FGCOKCMD_MASK16))      // if FGCOKCMD command is not active
        {
            Set(set,DIG_OP_SET_FGCOKCMD_MASK16);                       // Set FGCOKCMD command
        }
    }
    else                                                // else there are no latched faults
    {
        if(Test(sta.cmd_req,DIG_OP_SET_FGCOKCMD_MASK16))       // if FGCOKCMD command is active
        {
            Set(rst,DIG_OP_SET_FGCOKCMD_MASK16);                       // Clear FGCOKCMD command
        }
    }

    /*--- Send and simulate command to digital interface ---*/

    sta.cmd_req = (sta.cmd_req & ~rst) | set;   // Simulate command

    if(set ^ rst)                               // If any command bits need to be changed
    {
        OS_ENTER_CRITICAL();
        DIG_OP_P = (rst<<8) | set;                      // Write reset/set mask to command register
        OS_EXIT_CRITICAL();
    }

    sta.cmd = 0;                        // Clear command variable
}
#endif
/*---------------------------------------------------------------------------------------------------------*/
uint16_t StaStartPC(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SetPC() to start the voltage source if this is possible.  This will be in
  response to either S PC SB, S PC IL or S PC CY.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(dpcls.mcu.cal.active == FGC_CTRL_ENABLED)
    {
        return FGC_CAL_ACTIVE;
    }

    if(STATE_PC        != FGC_PC_OFF            ||    // If PC state isn't OFF or
       SM_MPRUN_P      != FGC_MAINPROG_RUNNING  ||    // Dallas ID scan in progress or
       sta.config_mode == FGC_CFG_MODE_SYNC_FGC)      // configuration in progress or
    {
        return FGC_BAD_STATE;
    }

    // If post mortem logging is active in the gateway, and the PM flag is set

    if(  (dev.pm_enabled_f && Test(ST_UNLATCHED, FGC_UNL_POST_MORTEM))
       || Test(ST_UNLATCHED, FGC_UNL_LOG_PLEASE))
    {
        return FGC_PM_IN_PROGRESS;
    }

    LogStartAll();                              // Restart logging
    Clr(sta.flags,STAF_IDLE|STAF_CYCLING);      // Clear IDLE and CYCLING flags
    sta.flags |= STAF_START;                    // Request start power of converter after calibrating DAC

    return FGC_OK_NO_RSP;
}
// TODO Make this function class-specific - it should be used in class 53 only
/*---------------------------------------------------------------------------------------------------------*/
void StaPubCyclingProp()
/*---------------------------------------------------------------------------------------------------------*\
  This function will check whether some of the cycling properties need to be published
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( sta.cyc_start_new_cycle_f == true)              // Property LOG.CYC
    {
        sta.cyc_start_new_cycle_f = false;
    }

#if (FGC_CLASS_ID != 51)
    if((int16_t)dpcls.dsp.log.capture.state != sta.copy_log_capture_state)
    {
        sta.copy_log_capture_state = dpcls.dsp.log.capture.state;
    }

    if((int16_t)dpcls.dsp.log.capture.user != sta.copy_log_capture_user)
    {
        sta.copy_log_capture_user = dpcls.dsp.log.capture.user;
    }

    if(!AbsTimeUs_IsEqual(dpcls.dsp.log.capture.cyc_chk_time, sta.copy_log_capture_cyc_chk_time))
    {
        sta.copy_log_capture_cyc_chk_time = dpcls.dsp.log.capture.cyc_chk_time;
    }

    if((int16_t)dpcls.dsp.notify.log_capture_user >= 0)
    {
        dpcls.dsp.notify.log_capture_user = -1;
    }

    if((int16_t)dpcls.dsp.notify.ref_cyc_fault >= 0)
    {
        dpcls.dsp.notify.ref_cyc_fault = -1;
    }

    if((int16_t)dpcls.dsp.notify.ref_cyc_warning >= 0 &&
       (int16_t)dpcls.dsp.notify.ref_cyc_warning <= FGC_MAX_USER)
    {
        dpcls.dsp.notify.ref_cyc_warning = -1;
    }
#endif
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sta_class.c
\*---------------------------------------------------------------------------------------------------------*/

