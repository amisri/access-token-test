/*---------------------------------------------------------------------------------------------------------*\
 File:          diag_class.c

 Purpose:       FGC HC16 Software - Class 51 diagnostic functions

 Author:        Quentin.King@cern.ch

 Notes:
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#include <diag_class.h>

#include <stdbool.h>
#include <cmd.h>
#include <class.h>      // for DEV_STA_TSK_PHASE
#include <fbs_class.h>  // for FAULTS, WARNINGS, ST_LATCHED, STATE_OP
#include <defprops.h>
#include <definfo.h>    // for FGC_CLASS_ID
#include <dev.h>        // for dev global variable
#include <dpcom.h>      // for dpcom global variable
#include <macros.h>     // for Test(), Set()
#include <sta.h>        // for sta global variable
#include <diag.h>       // for diag global variable
#include <dpcls.h>      // for dpcls global variable
#include <fbs.h>        // for fbs global variable
#include <sta_class.h>  // for vs global variable

/*---------------------------------------------------------------------------------------------------------*/
void DiagInitClass(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises aspects of the diagnostic system that are related to the Class 51 version.
  Some power converters are made from subconverters which can trip off without stopping the whole converter.
  The state of the subconverters is available via the diagnostic system and this function will find the
  relevant channels and save them so that they can be checked during operation.
\*---------------------------------------------------------------------------------------------------------*/
{
    bool     match_f;
#if (FGC_CLASS_ID == 51)
    bool     is_rphe_or_rphk = false;
#endif
    uint16_t      logical_dim;

    static int8_t chan_name[]     = "SUB n";
    static int8_t fwd_name[]      = "FREE WHEEL DIODE";
    static int8_t fau_name[]      = "FAST ABORT UNSAFE";

    /*--- Find analog channels ---*/

    dpcls.mcu.vs.i_earth_chan    = DiagFindAnaChan("I_EARTH");
    dpcls.mcu.vs.u_lead_pos_chan = DiagFindAnaChan("U_LEAD_POS");
    dpcls.mcu.vs.u_lead_neg_chan = DiagFindAnaChan("U_LEAD_NEG");

    if(dpcls.mcu.vs.i_earth_chan < 0)
    {
        return;
    }

    /*--- Find digital channel for converters containing subconverters ---*/

    vs.n_sub_convs = 0;
    logical_dim    = dpcls.mcu.vs.i_earth_chan % FGC_MAX_DIMS;  // VS DIM contains I_EARTH

    do
    {
        chan_name[4] = '1' + vs.n_sub_convs;

        match_f = DiagFindDigChan(logical_dim,          // Only check the VS DIM
                                  chan_name,
                                  &vs.sub_conv[vs.n_sub_convs].data_idx,
                                  &vs.sub_conv[vs.n_sub_convs].mask);

        if(match_f)
        {
            vs.n_sub_convs++;
        }
    }
    while(match_f && vs.n_sub_convs < 8);

    /*--- Determine sub converter current when applicable ---*/

    if(vs.n_sub_convs)
    {
        if(dev.sys.sys_type[0] == 'R' &&
           dev.sys.sys_type[1] == 'P' &&
           dev.sys.sys_type[2] == 'H' &&
          (dev.sys.sys_type[3] == 'E' || dev.sys.sys_type[3] == 'K'))
        {
            dpcls.mcu.vs.amps_per_sub_conv = 3250;              // Transtechnik  3.25kA
#if (FGC_CLASS_ID == 51)
            is_rphe_or_rphk = true;
#endif
        }
        else
        {
            dpcls.mcu.vs.amps_per_sub_conv = 2000;              // Kempower 2kA
        }
    }

#if (FGC_CLASS_ID == 51)

    // Find digital channel for Free Wheel Diode monitoring (always on VS dim)

    if(DiagFindDigChan(logical_dim, fwd_name, &vs.fwd_diag_chan, &vs.fwd_diag_mask))
    {
        vs.fw_diode = FGC_VDI_NO_FAULT;

        // Check if the Free Wheel Diode signal triggers a VS fault. It happens only with RPHE and RPHK converters.
        // For the rest this signal is not a trigger, but it should still appear in DIAG.FAULTS and also be latched in
        // VS.FW_DIODE property

        vs.fwd_is_fault = is_rphe_or_rphk;
    }

    // Find digital channel for Fast Abort Unsafe monitoring (always on VS dim)

    if(DiagFindDigChan(logical_dim, fau_name, &vs.fau_diag_chan, &vs.fau_diag_mask))
    {
        vs.fabort_unsafe = FGC_VDI_NO_FAULT;
    }
#endif
}
#if FGC_CLASS_ID == 51
/*---------------------------------------------------------------------------------------------------------*/
void DiagCheckConverter(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from StaTsk() every 5ms.  Provided the converter is not being simulated then
  every 4th iteration it will check the number of active subconverters based on the diag data channels
  and masks identified by DiagInitClass().  It provides the number to the DSP so that the actual maximum
  current can be calculated.

  It also checks to see if the VS DIM is reporting a FREE WHEEL DIODE fault and if so it sets the
  VS.FW_DIODE property and asserts a FW_DIODE fault.  Since this can trip the converter and the signal
  comes from the DIM, a filter is included to avoid spurious DIAG data from stopping the converter. This
  requires a VS_FAULT to be present and the FW_DIODE fault bit in the DIM data to be active for 200ms.

  It also checks to see if a DIM is reporting an unlatched trigger while the converter is not reporting
  a fault.  If this condition persists, the DIAG_TRIG_FLT flag will be set in the latched status, which
  will generate an FGC_HW warning.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint16_t      n_active_sub_convs;
    static uint16_t fw_diode_counter;                     // Filter counter for FW_DIODE fault
    static uint16_t fabort_unsafe_counter;                // Filter counter for FABORT_UNSAFE fault
    static uint16_t dim_trig_counter;                     // Filter counter for DIM_TRIG_FLT latched status

    if(sta.mode_op != FGC_OP_SIMULATION &&                      // If not simulating the converter and
       ((uint16_t)sta.timestamp_ms.ms_time % 20) == (15 + DEV_STA_TSK_PHASE))     // time is end of 20ms cycle
    {
        /*--- Check for subconverter faults ---*/

        if(vs.n_sub_convs)                                      // If converter includes subconverters, and
        {
            n_active_sub_convs = vs.n_sub_convs;                        // Initialise sub converter count

            if (     (uint16_t) dpcom.dsp.diag.dims_are_valid
                  && (sta.mode_op != FGC_OP_SIMULATION)                         // not simulating VS
                )
            {
                for( i = 0; i < vs.n_sub_convs; i++ )                                   // For all sub converters
                {
                    if((diag.data[vs.sub_conv[i].data_idx] & vs.sub_conv[i].mask)) // If sub is in fault
                    {
                        n_active_sub_convs--;                                           // Dec active subs
                    }
                }
            }

            dpcls.mcu.vs.n_sub_convs = n_active_sub_convs;              // Inform DSP of number of active subs

            if(vs.n_sub_convs != n_active_sub_convs)                    // If any subconverters are inactive
            {
                Set(WARNINGS,FGC_WRN_SUBCONVTR_FLT);                            // Set warning
            }
            else                                                        // else
            {
                // If the warning was previously set, then clear the latched status DIM_TRIG_FLT. When a
                // subconverter falls, it also causes a DIM_TRIG_FLT and as a consequence a FGC_HW warning. Since
                // the operator does not always think of resetting the latched status (S STATUS.ST_LATCHED RESET),
                // the FGC_HW warning remains after the subconverter problem is fixed.

                if(Test(WARNINGS,FGC_WRN_SUBCONVTR_FLT))
                {
                    Clr(ST_LATCHED,FGC_LAT_DIM_TRIG_FLT);
                }

                Clr(WARNINGS,FGC_WRN_SUBCONVTR_FLT);                            // Clear warning


            }
        }

        // Check for a Free Wheel Diode fault with 200ms filter
        // If the signal is present on the VS DIM

        if(vs.fw_diode != FGC_VDI_NOT_PRESENT)
        {
            if(Test(diag.data[vs.fwd_diag_chan], vs.fwd_diag_mask))
            {
                ++fw_diode_counter;
            }
            else
            {
                fw_diode_counter = 0;
            }

            if(fw_diode_counter > 10       &&
               STATE_OP == FGC_OP_NORMAL   &&
               !Test(sta.cmd_req, DIG_OP_SET_VSRESETCMD_MASK16))
            {
                if(vs.fwd_is_fault                  &&
                   !Test(FAULTS, FGC_FLT_FW_DIODE)  &&
                   Test(FAULTS, FGC_FLT_VS_FAULT))
                {
                    // If the signal is a fault then set FW_DIODE fault and latch the signal in VS.FW_DIODE property.
                    // The FW_DIODE fault will be set only when VS_FAULT is present.

                    vs.fw_diode = FGC_VDI_FAULT;
                    Set(sta.faults, FGC_FLT_FW_DIODE);
                }
                else if(!vs.fwd_is_fault)
                {
                    // If the signal is just a status, then latch it in VS.FW_DIODE property,
                    // but do not set any fault. The latched status should be picked up by FGC monitor.

                    vs.fw_diode = FGC_VDI_FAULT;
                }
            }
        }

        /*--- Check for a Fast Abort fault - with 100ms filter ---*/

        if(vs.fau_diag_mask                                     &&
           STATE_OP == FGC_OP_NORMAL                            &&
          !Test(sta.cmd_req,DIG_OP_SET_VSRESETCMD_MASK16)       &&
          !Test(FAULTS,FGC_FLT_FABORT_UNSAFE)                   &&
           Test(FAULTS,FGC_FLT_VS_FAULT)                        &&
           Test(diag.data[vs.fau_diag_chan],vs.fau_diag_mask))
        {
            if(++fabort_unsafe_counter > 5)                     // If fault present for > 100ms
            {
                vs.fabort_unsafe = FGC_VDI_FAULT;
                Set(sta.faults,FGC_FLT_FABORT_UNSAFE);
            }
        }
        else
        {
            fabort_unsafe_counter = 0;
        }

        /*--- Check global unlatched trigger flag ---*/

        if((uint16_t)dpcom.dsp.diag.global_unlatched_trig_f &&    // If a DIM has an unlatched trigger and
            Test(sta.inputs,DIG_IP1_VSRUN_MASK16)         &&    // VSRUN is active and
            !Test(FAULTS,FGC_FLT_VS_FAULT)                &&    // no VS_FAULT is present and
            !Test(FAULTS,FGC_FLT_VS_EXTINTLOCK)           &&    // no VS_EXTINTLOCK is present and
            !Test(FAULTS,FGC_FLT_FAST_ABORT)              &&    // no FAST_ABORT is present and
            !Test(ST_LATCHED,FGC_LAT_DIM_TRIG_FLT))             // the DIM_TRIG_FLT is not yet set
        {
            if(++dim_trig_counter > 5)                              // If fault present for > 100ms
            {
                Set(ST_LATCHED,FGC_LAT_DIM_TRIG_FLT);                   // Set DIM_TRIG_FLT in latched status
            }
        }
        else
        {
            dim_trig_counter = 0;
        }
    }
}
#endif
/*---------------------------------------------------------------------------------------------------------*\
  End of file: diag_class.c
\*---------------------------------------------------------------------------------------------------------*/
