/*---------------------------------------------------------------------------------------------------------*\
  File:         mst_class.c

  Purpose:      FGC HC16 Class 51 Software - Class related millisecond actions
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#include <mst_class.h>

#include <stdbool.h>
#include <class.h>              // for ALL_DSP_WARNINGS_MASK
#include <fbs_class.h>          // for FAULTS, WARNINGS, ST_LATCHED, ST_UNLATCHED, STATE_OP, STATE_PC, ST_MEAS_A, ST_MEAS_B, ST_DCCT_A, ST_DCCT_B, I_EARTH_CPCNT, V_REF, I_MEAS, V_MEAS, I_REF, I_ERR_MA, I_DIFF_MA
#include <dev.h>                // for dev global variable
#include <fbs.h>                // for fbs global variable
#include <mst.h>                // for mst global variable
#include <dpcom.h>              // for dpcom global variable
#include <macros.h>             // for Test(), Clr()
#include <sta.h>                // for sta global variable
#include <sta_class.h>          // for vs global variable
#include <dpcls.h>              // for dpcls global variable
#include <log_class.h>          // for log_iab global variable, LogTiming(), LOG_IEARTH_PHASE, LOG_IREG_PHASE, LOG_ILEADS_PHASE
#include <ref_class.h>          // for RefEvent()
#include <defprops.h>           // for PROP_REF_CYC
#include <adc_class.h>
#include <shared_memory.h>      // for FGC_MAINPROG_RUNNING
#include <crate.h>              // for crate.type

/*---------------------------------------------------------------------------------------------------------*/
void MstClass(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the millisecond task, MstTsk(), to perform the class related millisecond
  activities.

  Sector Access Flag: The flag used by the C32 dpcls.mcu.sector_access is linked to the settable property
  VS.SECTOR_ACCESS.  If this is disabled by the user when the real sector access flag received from the
  gateway is set (fbs.sector_access) then the C32 flag must be reasserted.  This is done every millisecond.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Every Millisecond: Handle ADC filters reset requests, and on-going reset

    if(dpcls.mcu.adc.filter_state != FGC_FILTER_STATE_READY)   // If on-going ADC filters reset
    {                                                       // To be called before AdcFiltersReset every ms
        AdcFiltersResetCheck();
    }
    if(adc.trigger_reset_f &&                               // If ADC filters reset is triggered
       mst.dsp_reg_run_f)                                   // and the DSP was regulating on the previous ms
    {
        AdcFiltersReset();                                      // Reset ADC filters
    }
    if(dpcls.dsp.adc.reset_req == ADC_DSP_RESET_REQ)        // If new ADC reset request coming from the DSP
    {
        AdcFiltersResetRequest(false);                          // Treat the DSP request
    }

    // Every Millisecond: Log IAB and assert SECTOR ACCESS flag if enabled

    if(log_iab.samples_to_acq)                  // If IAB logging samples still to acquire
    {
        LogIab();                                       // Log latest sample
    }

    if(fbs.sector_access_gw)                    // If SECTOR ACCESS flag from gateway is active
    {
        fbs.sector_access = FGC_CTRL_ENABLED;           // Enable FGC's SECTOR ACCESS flag
    }

    // Millisecond 1 of 10: Read analog values for FIP and ANALOG log buffer if not unconfigured

    if(mst.ms_mod_10 == 1 &&
       SM_MPRUN_P    == FGC_MAINPROG_RUNNING &&
       STATE_OP      != FGC_OP_UNCONFIGURED)
    {
        I_REF     = dpcls.dsp.ref.i;
        V_REF     = dpcls.dsp.ref.v;
        I_MEAS    = dpcls.dsp.meas.i;
    }

    /* Millisecond 1 of 20: Read DSP status and run Gateway PC_PERMIT timeout */

    if(mst.ms_mod_20 == 1)
    {
        FAULTS      |= (uint16_t)dpcom.dsp.faults;                // Latch faults from DSP
        ST_LATCHED  |= (uint16_t)dpcom.dsp.st_latched;            // Latch status from DSP

        WARNINGS     = (WARNINGS & ~ALL_DSP_WARNINGS_MASK) |            // Mix in DSP warnings
                       (uint16_t)dpcom.dsp.warnings;
        ST_UNLATCHED = (ST_UNLATCHED & ~DSP_UNLATCHED) |        // Mix in DSP unlatched status
                       (uint16_t)dpcom.dsp.st_unlatched;

        ST_MEAS_A    = (ST_MEAS_A & ~DSP_MEAS) |                // Mix in DSP meas A flags
                       (uint16_t)dpcls.dsp.meas.st_meas[0];
        ST_MEAS_B    = (ST_MEAS_B & ~DSP_MEAS) |                // Mix in DSP meas B flags
                       (uint16_t)dpcls.dsp.meas.st_meas[1];

        ST_DCCT_A    = (ST_DCCT_A & ~DSP_DCCT_MASK) |           // Mix in DSP dcct A flags
                       (uint16_t)dpcls.dsp.meas.st_dcct[0];
        ST_DCCT_B    = (ST_DCCT_B & ~DSP_DCCT_MASK) |           // Mix in DSP dcct B flags
                       (uint16_t)dpcls.dsp.meas.st_dcct[1];

        V_MEAS       = dpcls.dsp.meas.v;                        // Vmeas is only measured at 50Hz
        I_ERR_MA     = (int16_t)dpcls.dsp.reg.i_err_ma;         // Already clipped to int16_t range
        I_DIFF_MA    = (int16_t)dpcls.dsp.meas.i_diff_ma;       // Already clipped to int16_t range
        I_EARTH_CPCNT = (int16_t)dpcls.dsp.meas.i_earth_cpcnt;  // Already clipped to int16_t range

        if(Test(FAULTS,FGC_FLT_I_MEAS))                 // If I_MEAS fault is present
        {
            Clr(WARNINGS,FGC_WRN_I_MEAS);                               // Suppress the I_MEAS warning
        }

        /* Gateway PC_PERMIT */

        if(fbs.id)                                      // If connected to FIP network
        {
            if (fbs.pll_locked_f)
            {
                // Set the gw_pc_permit to 0 only when:
                // 1) the PC_PERMIT from the gateway is not set
                // 2) AND the crate type is PC_60A

                if (!(fbs.time_v.flags & FGC_FLAGS_PC_PERMIT) && (crate.type == FGC_CRATE_TYPE_PC_60A))
                {
                    sta.gw_pc_permit = 0;
                }
                else
                {
                    // otherwise set the timeout to its maximum value

                    sta.gw_pc_permit = fbs.gw_timeout;
                }
            }
            else
            {
                // If Gateway is not online and and PC_PERMIT is active,
                // decrement counter only for PC_60A.

                if (sta.gw_pc_permit && crate.type == FGC_CRATE_TYPE_PC_60A)
                {
                    sta.gw_pc_permit--;
                }
            }

        }
    }

    /* Millisecond 5 of 20: Log Iearth signal and set DICO alive signal status */

    else if(mst.ms_mod_20 == LOG_IEARTH_PHASE)
    {
        if(log_iearth.samples_to_acq)   // If IEARTH logging samples still to be acquired
        {
            LogIearth();
        }

        /* Dico alive */

        if(STATE_OP == FGC_OP_NORMAL      ||                    // if operation mode is normal or
           STATE_OP == FGC_OP_CALIBRATING ||                    // calibrating or
           STATE_OP == FGC_OP_TEST     ||                       // testing or
           (STATE_OP == FGC_OP_SIMULATION && !vs.sim_intlks))   // simulating VS but not interlocks
        {
            mst.dicoalive = true;                               // enable dico alive for digital interface
        }
        else
        {
            mst.dicoalive = false;                              // disable dico alive for digital interface
        }
    }

    /* Millisecond 6 of 10: Log Ireg signals */

    else if(mst.ms_mod_10 == LOG_IREG_PHASE)
    {
        if(log_ireg.samples_to_acq)     // If IREG logging samples still to be acquired
        {
            LogIreg();
        }
    }

    /* Millisecond 8 of 1000: Log Ileads signals */

    else if(SM_MSTIME_P == LOG_ILEADS_PHASE)
    {
        if(log_ileads.samples_to_acq)   // If ILEADS logging samples still to be acquired
        {
            LogIleads();
        }
    }

    // Millisecond 12 of 20: Check events if time variable was received
    else if(mst.ms_mod_20 == 12 && fbs.time_rcvd_f)
    {
        RefEvent();
    }

    // Millisecond 0 of 1000: Check logging and ADC periodic reset

    else if(!SM_MSTIME_P)
    {
        // LOG: Check if logging has finished

        if(dev.log_pm_state == FGC_LOG_STOPPING &&              // If PM log state is STOPPING, and
           !log_ireg.samples_to_acq             &&              // IREG log has stopped, and
           !log_iab.samples_to_acq)                             // IAB log has stopped
        {
            dev.log_pm_state = FGC_LOG_FROZEN;                          // Set PM Log state to Frozen
            Clr(fbs.u.fieldbus_stat.ack,FGC_SELF_PM_REQ);               // Reset self-trig PM dump request
        }

        // ADC: Handle periodic reset of the ADC filters

        if(sta.adc_ext_inputs_f &&                                     // If ADC interface is SD-350 or SD-351
           (mst.time.unix_time - adc.last_reset_unix_time) >= ADC_RESET_PERIOD_S) // And reset period exceeded
        {
            AdcFiltersResetRequest(false);                      // Attempt to reset the ADC filters

            // Reset counter of ADC resets due to an unlatched I_MEAS_DIFF during one automatic reset period

            adc.unl_i_meas_diff_counter = 0;

            if(dpcls.mcu.adc.filter_state != FGC_FILTER_STATE_RESET_TIMEOUT)
            {
                // Reset the flag indicating a reset timeout occurred.

                adc.reset_timeout_occured_once_f = false;
            }
        }

        // ADC: If there is no pending ADC reset request, if there is an I_MEAS_DIFF unlatched flag,
        // and if the maximum number of ADC resets caused by such a flag has not been reached yet, then
        // reset the ADC filters.

        if(sta.adc_ext_inputs_f &&                                     // If ADC interface is SD-350 or SD-351
           !adc.pending_reset_req_f &&
           Test(ST_UNLATCHED,FGC_UNL_I_MEAS_DIFF) &&
           adc.unl_i_meas_diff_counter < ADC_RESET_MAX_UNLATCHED_PER_PERIOD)
        {
            AdcFiltersResetRequest(false);                      // Attempt to reset the ADC filters
            adc.unl_i_meas_diff_counter++;
        }
    }

    // Every 200ms: Check any pending ADC reset request

    if(mst.ms_mod_200 == 1 && adc.pending_reset_req_f)
    {
        AdcFiltersResetRequest(false);          // Attempt to reset the ADC filters
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mst_class.c
\*---------------------------------------------------------------------------------------------------------*/
