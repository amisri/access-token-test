/*!
 *  @file     post_mortem.c
 *  @brief    This file handles post mortems
 */

#define PC_POST_MORTEM_GLOBALS


// Includes

#include <defconst.h>
#include <fbs.h>
#include <log.h>
#include <dev.h>
#include <macros.h>



// Internal structures, unions and enumerations


// External function definitions

#if FGC_CLASS_ID == 51
void postMortemRequest(void)
{
    if(   fbs.id                                         // If FIP connected, and
       && dev.log_pm_state == FGC_LOG_STOPPING           // PM log is stopping, and
       && (FAULTS & (~FGC_FLT_NO_PC_PERMIT)))            // any fault other than NO_PC_PERMIT is set
    {
        logFgcLoggerSetPostMortem();
    }
}
#elif FGC_CLASS_ID == 53
void postMortemRequest(void)
{
    bool error = (FAULTS & (~FGC_FLT_NO_PC_PERMIT)) || dpcls.dsp.cyc.fault.chk != FGC_CYC_FLT_NONE;

    if(   error  == true
       && fbs.id != 0
       && dev.log_pm_state == FGC_LOG_STOPPING)
    {
        Set(fbs.u.fieldbus_stat.ack,FGC_SELF_PM_REQ);
        logFgcLoggerSetPostMortem();
    }
}
#endif


// EOF
