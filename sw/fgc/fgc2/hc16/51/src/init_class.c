/*---------------------------------------------------------------------------------------------------------*\
  File:         init_class.c

  Purpose:      FGC HC16 Software - initialisation functions for class 51/53 (LHC_PC/POPS)
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h
#define INIT_CLASS_GLOBALS  // define devtype[],conf_l[], conf_s[], conf_f[], init_prop[], init_dyn_flags[] global variables

#include <init_class.h>
#include <cmd.h>                // for struct cmd
#include <nvs.h>
#include <fbs_class.h>          // for STATE_OP
#include <defprops.h>
#include <definfo.h>            // for FGC_CLASS_ID
#include <defconst.h>           // for FGC_CTRL_ENABLED
#include <dev.h>                // for dev global variable
#include <dpcom.h>              // for dpcom global variable
#include <sta.h>                // for sta global variable
#include <dpcls.h>              // for dpcls global variable
#include <prop.h>               // for PropSet()
#include <macros.h>             // for Set()
#include <rtd.h>                // for rtd global variable
#include <ref_class.h>          // for ref global variable, RefArm(), NON_PPM_USER
#include <structs_bits_big.h>
#include <adc_class.h>
#include <ref_class.h>
#include <set.h>
#include <transaction.h>


/*---------------------------------------------------------------------------------------------------------*/
void InitClassPreInts(void)
/*---------------------------------------------------------------------------------------------------------*\
  Class specific initialisation before interrupts are started or the NVS properties have been set or
  the DSP is started.
\*---------------------------------------------------------------------------------------------------------*/
{
    AdcFiltersReadFunctions();                              // Read SD filter function meta data
    transactionInit();
}
/*---------------------------------------------------------------------------------------------------------*/
void InitClassPostInts(void)
/*---------------------------------------------------------------------------------------------------------*\
  Class specific initialisation after interrupts are started and the NVS properties have been set
\*---------------------------------------------------------------------------------------------------------*/
{
    NvsInit(&init_prop[0]);                             // Initialise non-volatile properties

    // Initialise the default non-PPM reg mode
    // This is the FIRST COPY of the REG.MODE_INIT property to the non-PPM reg mode.
    // (The second copy will happen after synchronising with the Database.)

    dpcls.mcu.ref.func.reg_mode[0] = ref.reg_mode_init;

    // Other initialisations

    dpcom.mcu.device_ppm = dev.ppm;
    dev.max_user         = GET_FGC_MAX_USER();
}



void InitClassSpyMpx(struct cmd * c)
{
    static uint32_t init_spy_mpx[FGC_N_SPY_CHANS] =
    {
        FGC_SPY_REF,    FGC_SPY_IMEAS,
        FGC_SPY_VREF,   FGC_SPY_VMEAS,
        FGC_SPY_IA,     FGC_SPY_IB
    };

    PropSet(c, &PROP_SPY_MPX, init_spy_mpx, FGC_N_SPY_CHANS);
}



#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void InitClassPostSync(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  Class specific initialisation after FGC has been synchronised
\*---------------------------------------------------------------------------------------------------------*/
{
    // After synchronisation with the config DB, this is the SECOND COPY of the REG.MODE_INIT property
    // to the non-PPM reg mode. (The first copy happened after NVS init.)

    dpcls.mcu.ref.func.reg_mode[0] = ref.reg_mode_init;

    // Set the PLEP parameters from the defaults

    SetDefaults(c, NULL);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: init_class.c
\*---------------------------------------------------------------------------------------------------------*/
