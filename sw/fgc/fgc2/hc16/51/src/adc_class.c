/*---------------------------------------------------------------------------------------------------------*\
  File:         adc_class.c

  Purpose:      FGC MCU Software - ADC functions

  Author:       pierre.dejoue@cern.ch
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#define ADC_CLASS_GLOBALS

#include <stdbool.h>
#include <adc_class.h>
#include <fbs_class.h>
#include <hash.h>                               // for HASH_VALUE and struct hash_entry needed by defprops.h
#include <defprops.h>                           // Include property related definitions
#include <dpcls.h>
#include <macros.h>
#include <defconst.h>                           // for FGC_INTERFACE_*

/*---------------------------------------------------------------------------------------------------------*/

static uint32_t  SdFlashRead(uint16_t * datainc);

/*---------------------------------------------------------------------------------------------------------*/
void AdcInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  ADC initialisation
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t ana_ctrl;

    // For ADC interface SD-350 or SD-351:

    if(SM_INTERFACETYPE_P == FGC_INTERFACE_SD_350 ||
       SM_INTERFACETYPE_P == FGC_INTERFACE_SD_351)
    {
        do                                              // Wait for ADC filters to program
        {
            ana_ctrl = ANA_CTRL_P;
        } while(ana_ctrl & (ANA_CTRL_FLTANOTRDY_MASK16|ANA_CTRL_FLTBNOTRDY_MASK16));

        adc.last_reset_unix_time = dpcom.mcu.time.now_ms.unix_time;
    }

    // For all ADCs:
    // Consider the ADC filters are ready

    dpcls.mcu.adc.filter_state = FGC_FILTER_STATE_READY;

    // Reset counter of ADC resets due to an unlatched I_MEAS_DIFF during one automatic reset period

    adc.unl_i_meas_diff_counter = 0;

    // Reset the flag indicating a reset timeout occurred.

    adc.reset_timeout_occured_once_f = false;

}
/*---------------------------------------------------------------------------------------------------------*/
void AdcFiltersResetRequest(bool user_request_f)
/*---------------------------------------------------------------------------------------------------------*\
  Treat any request to reset the ADC filters. The argument user_request_f is set in case the reset is
  requested by the end user with one of the following commands: S ADC RESET, S ADC.FILTER RESET or S PC OF
\*---------------------------------------------------------------------------------------------------------*/
{
    // Clear request flags from MCU and DSP to acknowledge that the reset request is being treated

    adc.pending_reset_req_f = false;                    // 200ms flag

    if(dpcls.dsp.adc.reset_req == ADC_DSP_RESET_REQ)
    {
        dpcls.dsp.adc.reset_req = ADC_DSP_RESET_WAIT;   // DSP reset request in WAIT state
    }

    // If analog card is SD-350 or SD-351

    if(SM_INTERFACETYPE_P == FGC_INTERFACE_SD_350 ||
       SM_INTERFACETYPE_P == FGC_INTERFACE_SD_351)
    {
        // Early return if user request. A user request (S ADC RESET) will always be served

        if(user_request_f)
        {
            adc.trigger_reset_f = true;

            return;
        }

        // Early return if the filters are in reset. Acknowledge the DSP in case it has just made the request

        if(dpcls.mcu.adc.filter_state == FGC_FILTER_STATE_RESETTING)
        {
            dpcls.dsp.adc.reset_req = ADC_DSP_RESET_ACK;
            return;
        }

        // If the filters are ready, and the DSP regulation mode allows for ADC reset,
        // and the FGC is in normal mode, OF/FO/IL/SB state, then trigger the reset.
        // Note that the FGC shall not reset while calibrating.

        if(dpcls.mcu.adc.filter_state == FGC_FILTER_STATE_READY &&
           STATE_OP == FGC_OP_NORMAL  &&
              (STATE_PC == FGC_PC_FLT_OFF || STATE_PC == FGC_PC_OFF
// the other case can be class 53
#if (FGC_CLASS_ID == 51)
                || (dpcls.dsp.adc.reset_gate_f && (STATE_PC == FGC_PC_IDLE || STATE_PC == FGC_PC_ON_STANDBY))
#endif
              ))
        {
            adc.trigger_reset_f = true;             // Trigger the reset (will happen after next reg period)
        }
        else
        {
            adc.pending_reset_req_f = true;         // Leave the request as pending (200ms flag)
        }
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void AdcFiltersReset(void)
/*---------------------------------------------------------------------------------------------------------*\
  Trigger a reprogramming of the FPGA used for filters on SD-350 and SD-351 cards.
  On other analog cards the reset trigger is cleared which means the reset request is simply ignored.
\*---------------------------------------------------------------------------------------------------------*/
{
    adc.trigger_reset_f = false;

    if(SM_INTERFACETYPE_P == FGC_INTERFACE_SD_350 ||    // If analog card is SD-350 or SD-351
       SM_INTERFACETYPE_P == FGC_INTERFACE_SD_351)
    {
        Set(ANA_CTRL_P,ANA_CTRL_FLTRESET_MASK16);               // Reset filter FPGAs
        Clr(ANA_CTRL_P,ANA_CTRL_FLTRESET_MASK16);               // Reprogramming filters and run in normal operation

        adc.reset_counter_ms = 0;                               // Start a timeout counter

        adc.in_reset_f              = true;
        adc.last_reset_unix_time    = mst.time.unix_time;
        dpcls.mcu.adc.filter_state  = FGC_FILTER_STATE_RESETTING;
        dpcls.dsp.adc.reset_req     = ADC_DSP_RESET_ACK;        // Acknowledge the reset if asked by the DSP
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcFiltersResetCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  Check that the filter reset triggered by AdcFilterReset has finished. That is a three stage process:
        1. Wait for FPGA reprogramming to complete (usually 28ms);
        2. Set the filter index for each channel to enable data acquisition (immediate);
        3. Wait for the first valid data to come out of the filter, i.e. 2ms which is the filter length.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t          ana_ctrl;
    static  uint16_t  filter_downcounter = 3;     // This downcounter will account for the filter length

    adc.reset_counter_ms++;                     // Count the number of milliseconds spent in reset

    ana_ctrl = ANA_CTRL_P;
    if(ana_ctrl & (ANA_CTRL_FLTANOTRDY_MASK16|ANA_CTRL_FLTBNOTRDY_MASK16))
    {
        // Reset has not completed yet

        if(dpcls.mcu.adc.filter_state == FGC_FILTER_STATE_RESETTING &&
           adc.reset_counter_ms >= ADC_RESET_MAX_MS)
        {
            dpcls.mcu.adc.filter_state = FGC_FILTER_STATE_RESET_TIMEOUT;

            if(adc.reset_timeout_occured_once_f == false)
            {
                // First reset timeout: give it another try (this is our last chance)

                adc.reset_timeout_occured_once_f = true;

                // Reset immediately. That way the total reset time, if the second reset succeeds,
                // should be 57 + ~32 = ~89 ms, which is below the SIGNAL_STUCK limit (100 ms).

                AdcFiltersReset();
            }
            else
            {
                // Second timeout, this is a fault

                Set(sta.faults, FGC_FLT_I_MEAS);
            }
        }
    }
    else if(filter_downcounter == 3)
    {
        // Reset of the ADC's FPGA has completed, restore the functions indexes

        adc.in_reset_f = false;

        AdcFiltersSetIndex(adc.filter_idx_a, adc.filter_idx_b);

        filter_downcounter--;
    }
    else if(!(--filter_downcounter))    // Wait for the filter downcounter to reach 0 (= 2 ms)
    {
        // If in RESET_TIMEOUT state the only way to clear the fault is to trigger a new reset
        // with "S ADC RESET" or "S PC OF"

        if(dpcls.mcu.adc.filter_state != FGC_FILTER_STATE_RESET_TIMEOUT)
        {
            dpcls.mcu.adc.filter_state = FGC_FILTER_STATE_READY;
            Clr(sta.faults, FGC_FLT_I_MEAS);
        }

        filter_downcounter = 3;                 // Arm downcounter for the next reset
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void AdcFiltersReadFunctions(void)
/*---------------------------------------------------------------------------------------------------------*\
   Read SD filter functions meta data. The SD Filter flash contains five functions (0-4):

        0 - Operational filter for 500kHz ADCs (internal or external)
        1 - Test filter for 500kHz ADCs (internal or external)
        2 - Operational filter for 1MHz ADCs (external CERN 22-bit ADC)
        3 - Test filter for 1MHz ADCs (external CERN 22-bit ADC)
        4 - Operational filter for external ADS1281 ADC
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      filter_idx;     // SD filter index (0-4)
    uint16_t      func_tap_addr;  // Address in taps of start of function

    if(SM_INTERFACETYPE_P == FGC_INTERFACE_SD_350 ||    // If ADC interface is an SD-350, or
       SM_INTERFACETYPE_P == FGC_INTERFACE_SD_351)      // an SD-351
    {
        sta.adc_ext_inputs_f = true;                            // Flag that ADC board has external inputs
        ANA_CTRL_P = ANA_CTRL_SDMODE_MASK16;            // Select mode to access the SD flash

        for(filter_idx=0; filter_idx < FGC_N_SD_FLTRS; filter_idx++)
        {
            func_tap_addr = filter_idx * (SD_FLTR_LEN_W/2);
            ANA_SD_PG_A_ADDL_P = func_tap_addr << 1;            // Set address registers
            ANA_SD_PG_A_ADDH_P = !!(func_tap_addr & 0x8000);    // func_tap_addr >> 15

            adc.sd_shift[filter_idx]    = SdFlashRead((uint16_t*)ANA_SD_PG_A_DATAINC_16);
            adc.sd_halftaps[filter_idx] = SdFlashRead((uint16_t*)ANA_SD_PG_A_DATAINC_16);
        }

        ANA_CTRL_P = 0;                                 // Return filters to normal operating mode
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AdcFiltersSetIndex(uint16_t filter_idx_a, uint16_t filter_idx_b)
/*---------------------------------------------------------------------------------------------------------*\
  Set the ADC filter index for channels A/B. Index range goes from 0 to 4 for SD-350/SD-351 cards: see
  comments for function AdcFiltersReadFunctions.
\*---------------------------------------------------------------------------------------------------------*/
{
    if((SM_INTERFACETYPE_P == FGC_INTERFACE_SD_350 ||   // If analog card is SD-350 or SD-351
        SM_INTERFACETYPE_P == FGC_INTERFACE_SD_351)
       && !adc.in_reset_f)                              // and not in reset
    {
        ANA_SD_NL_A_W_HALFTAPS_P = adc.sd_halftaps[filter_idx_a];
        ANA_SD_NL_B_W_HALFTAPS_P = adc.sd_halftaps[filter_idx_b];

        ANA_SD_NL_A_W_INDEX_P    = filter_idx_a * (SD_FLTR_LEN_W/2) + 2;
        ANA_SD_NL_B_W_INDEX_P    = filter_idx_b * (SD_FLTR_LEN_W/2) + 2;

        adc.filter_idx_a = filter_idx_a;                // For later restore
        adc.filter_idx_b = filter_idx_b;
    }
}
#pragma MESSAGE DISABLE C1404 // Disable 'return expected' warning
/*---------------------------------------------------------------------------------------------------------*/
static uint32_t SdFlashRead(uint16_t * datainc)
/*---------------------------------------------------------------------------------------------------------*\
  Read a long word from the SD flash.  This requires two consecutive reads to the 16-bit data inc register,
  which has an automatic post increment of the address.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDX     datainc
        LDE     0,X
        LDD     0,X
    }                   // Return value in ED
}
#pragma MESSAGE DEFAULT C1404
/*---------------------------------------------------------------------------------------------------------*\
  End of file: adc_class.c
\*---------------------------------------------------------------------------------------------------------*/
