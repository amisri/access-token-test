/*---------------------------------------------------------------------------------------------------------*\
 File:      rtd_class.c

 Purpose:   FGC Class 51/53 Software - Real-time display Functions for LHC Power Converters

 Notes:     Display Layout:

                  1         2         3         4         5         6         7         8
        +---------+---------+---------+---------+---------+---------+---------+---------++
     ADCS   20  |-nnnnnnnn -nn.nnnnnn n.nE+nn -nnnnn.nnn  -nnnnnnnn -nn.nnnnnn n.nE+nn -nnnnn.nnn|
     LOAD   20  |R:n.nnE+nn  L:n.nnE+nn                              |
     PROT   20  |IEARTH:+n.nnnnA nnn%  POS:+n.nnn  NEG:+n.nnn                    |
     PPM    20  |USER: nn  CYCLEN: nn  MAXREF: -nnnnn.nnu                |
        +---------+---------+---------+---------+---------+---------+---------+---------++
  Class 51  21  |States: PL.OP.VS.PC     Iref:-nnnnn.nn  Vref:-nnnnn.nn Va:-nn.nnnnnn* Tout:nn.nn|
            22  |F/W:HMFTVEL/HMFSPGCILR Imeas:-nnnnn.nn Vmeas:-nnnnn.nn Vb:-nn.nnnnnn* Tin: nn.nn|
            23  |I/S:PFDP/PNABM-UDFCF   Irate:-nnnnn.nn  Ierr:-nnnnn mA Id: -nnnnn mA  Cpu:nn:nn%|
            24  |Ref: xxxxxxxxxxxxxxxxx Range:-nnnnn.nn -> -nnnnn.nn  Trun: nnnnnn    Trem:nnnnnn|
        +---------+---------+---------+---------+---------+---------+---------+---------++
  Class 53  21  |States: PL.OP.VS.PC.LK  Iref:-nnnnn.nn  Vref:-nnnnn.nn Va:-nn.nnnnnn* Tout:nn.nn|
            22  |F/W:HMFTVEL/HMFSPGCBRK Imeas:-nnnnn.nn Vmeas:-nnnnn.nn Vb:-nn.nnnnnn* Tin: nn.nn|
            23  |I/S:  ES/PNABM-UDFCF   Irate:-nnnnn.nn  Ierr:-nnnnn mA Id: -nnnnn mA  Cpu:nn:nn%|
            24  |Ref: xxxxxxxxxxxxxxxxx Range:-nnnnn.nn -> -nnnnn.nn  Trun: nnnnnn    Trem:nnnnnn|
        +---------+---------+---------+---------+---------+---------+---------+---------++
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL            // defprops.h
#define RTD_CLASS_GLOBALS           // define rtd_cls, rtd_func[], rtd_flags[] global variables
#define TERM_GLOBALS                // for term, edit_func[] global variables


#include <rtd_class.h>
#include <stdio.h>                  // for fprintf(), fputs()
#include <cmd.h>
#include <trm.h>
#include <fbs_class.h>              // for STATE_OP, STATE_VS, STATE_PC, I_EARTH_CPCNT, V_REF, I_MEAS, V_MEAS, I_REF, I_ERR_MA
#include <defprops.h>
#include <definfo.h>                // for FGC_CLASS_ID
#if (FGC_CLASS_ID == 51)
    #define CLASS_FIELD         c51
#endif
#if (FGC_CLASS_ID == 53)
    #define CLASS_FIELD         c53
#endif

#include <dpcom.h>                  // for dpcom global variable
#include <dpcls.h>                  // for dpcls global variable
#include <rtd.h>                    // for rtd global variable
#include <cal_class.h>              // for REF_STC_ARMED_FUNC_TYPE
#include <mem.h>                    // for MemSetBytes()
#include <term.h>                   // for TERM_BOLD
#include <fbs.h>                    // for fbs global variable
#include <serial_stream.h>          // for SerialStreamInternal_WriteChar()



/*---------------------------------------------------------------------------------------------------------*/
void RtdInit(FILE *f)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares the screen for the 4 line real-time display.  It uses the following vt100
  control codes (ESC='\33'):

      ESC [y;xH     Move cursor to (x,y)
      ESC [H        Move cursor to top left of screen
      ESC [1;19r    Set scroll window to lines 1 to 19
\*---------------------------------------------------------------------------------------------------------*/
{
    // Initialise RTD display area

    fputs("\33[21;1H",f);           // Move to RTD display area

    //    0         10        20        30        40        50        60        70        80
    //    |---------+---------+---------+---------+---------+---------+---------+---------+|
    fputs("States: __.__.__.__     Iref:______.__  Vref:______.__ Va:___.______+ Tout:__.__\n\r",f);
    fputs("F/W:_______/_________  Imeas:______.__ Vmeas:______.__ Vb:___.______+ Tin: __.__\n\r",f);
    fputs("I/S:  __/_____-_____   Irate:______.__  Ierr:______ "
          TERM_BOLD "mA" TERM_NORMAL " Id: ______ "
          TERM_BOLD "mA" TERM_NORMAL "  Cpu:__:__%\n\r",f);
    fputs("Ref: ______            Range:     0.00 ->      0.00  Trun: ______    Trem:______",f);

    fputs("\33[H\33[1;19r\v",f);        // Set up scroll window to protect RTD lines

    // Initialise RTD variables

    MemSetBytes(&rtd.last,       0xFF, sizeof(rtd.last));       // Reset all last value variables
    MemSetBytes(&rtd_cls.last,   0xFF, sizeof(rtd_cls.last));   // Reset all last value variables
    MemSetBytes(&rtd_cls.optline,0xFF, sizeof(rtd_cls.optline));// Reset all optline value variables
    rtd.idx = 0;                        // Reset field counter
    rtd_cls.last.ierr  = 0x7FFF;        // Reset the int16_t values to +32767
    rtd_cls.last.idiff = 0x7FFF;

    if(rtd.ctrl != FGC_RTD_OFF)         // If option line is active
    {
        rtd.last_ctrl = FGC_RTD_OFF;    // Reset last choice value to trigger reset
    }
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdStates(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the device states in abbreviated form on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Static arrays go into RAM (page 0), while static const would go into STRINGS memory zone and would
    // have to be accessed using FAR pointer

    static char pll_str[] = "LKCPFSNSFL";                           // FGC.PLL.STATE
    static char op_str[]  = "UCNLSMCLTTBTPRSO";                     // STATE.OP
    static char vs_str[]  = "IVFOPAOFFSSPSTRD";                     // VS.STATE
    static char lk_str[]  = "UCGVSFSCRDWPCFLF";                     // PAL.LINKS.STATE
    static char pc_str[]  = "FOOFFSSPSTSATSSBILTCARRNABCYPLBKECDT"; // STATE.PC

    char const * p;

    // If a state has changed

    if(fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll != rtd.last.state_pll    ||
       STATE_OP  != rtd.last.state_op     ||
       STATE_VS  != rtd_cls.last.state_vs ||
#if FGC_CLASS_ID == 53
       dpcls.dsp.pal.state != rtd_cls.last.state_lk ||
#endif
       STATE_PC  != rtd_cls.last.state_pc)
    {
        // Remember states

        rtd.last.state_pll     = fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll;
        rtd.last.state_op      = STATE_OP;
        rtd_cls.last.state_vs  = STATE_VS;
        rtd_cls.last.state_pc  = STATE_PC;
#if FGC_CLASS_ID == 53
        rtd_cls.last.state_lk  = dpcls.dsp.pal.state;
#endif

        // Move cursor

        fprintf(rtd.f,RTD_START_POS,RTD_STATES_POS);

        // Display PLL state

        p = pll_str + 2 * rtd.last.state_pll;
        SerialStreamInternal_WriteChar(*(p++),rtd.f);
        SerialStreamInternal_WriteChar(*p,rtd.f);

        // Display operational state

        SerialStreamInternal_WriteChar('.',rtd.f);
        p = op_str + 2 * rtd.last.state_op;
        SerialStreamInternal_WriteChar(*(p++),rtd.f);
        SerialStreamInternal_WriteChar(*p,rtd.f);

        // If operational state is not UNCONFIGURED

        if(rtd.last.state_op != FGC_OP_UNCONFIGURED)
        {
            // Display voltage source state

            SerialStreamInternal_WriteChar('.',rtd.f);
            p = vs_str + 2 * rtd_cls.last.state_vs;
            SerialStreamInternal_WriteChar(*(p++),rtd.f);
            SerialStreamInternal_WriteChar(*p,rtd.f);

            // Display power converter state

            SerialStreamInternal_WriteChar('.',rtd.f);
            p = pc_str + 2 * rtd_cls.last.state_pc;
            SerialStreamInternal_WriteChar(*(p++),rtd.f);
            SerialStreamInternal_WriteChar(*p,rtd.f);

#if FGC_CLASS_ID == 53

            // Display PAL links state

            SerialStreamInternal_WriteChar('.',rtd.f);
            p = lk_str + 2 * rtd_cls.last.state_lk;
            SerialStreamInternal_WriteChar(*(p++),rtd.f);
            SerialStreamInternal_WriteChar(*p,rtd.f);
#endif
        }

        // Restore cursor position

        fputs(RTD_END,rtd.f);

        return(true);
    }

    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdIref(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the current reference value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(RtdFloat(&I_REF, &rtd_cls.last.iref, rtd_cls.last.iref_s, "%9.2f", RTD_IREF_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdImeas(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the current measurement value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(RtdFloat(&I_MEAS, &rtd_cls.last.imeas, rtd_cls.last.imeas_s, "%9.2f", RTD_IMEAS_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdVref(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the voltage reference value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(RtdFloat(&V_REF, &rtd_cls.last.vref, rtd_cls.last.vref_s, "%9.2f", RTD_VREF_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdVmeas(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the voltage reference value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(RtdFloat(&V_MEAS, &rtd_cls.last.vmeas, rtd_cls.last.vmeas_s, "%9.2f", RTD_VMEAS_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdVa(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the ADC A voltage value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(RtdFloat(&dpcls.dsp.adc.volts_200ms[0], &rtd_cls.last.v[0],
                    rtd_cls.last.v_s[0], "%10.6f", RTD_VA_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdVb(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the ADC B voltage value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(RtdFloat(&dpcls.dsp.adc.volts_200ms[1], &rtd_cls.last.v[1],
                    rtd_cls.last.v_s[1], "%10.6f", RTD_VB_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdImeasRate(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the rate of change of Imeas value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32 irate = dpcls.dsp.meas.i_rate;

    // Clip I_RATE to +/- 999.99 to avoid overflow on terminal real-time display

    irate = MAX(-999.99, irate);
    irate = MIN(         irate, 999.99);

    return(RtdFloat(&irate, &rtd_cls.last.i_rate,
                    rtd_cls.last.i_rate_s, "%9.2f", RTD_IRATE_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdIerr(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the feedback error value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t i_err_ma = I_ERR_MA;

    return(RtdInt16s(&i_err_ma, &rtd_cls.last.ierr, "%6d", RTD_IERR_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdIdiff(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the feedback error value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t i_diff_ma = I_DIFF_MA;

    return(RtdInt16s(&i_diff_ma, &rtd_cls.last.idiff, "%6d", RTD_IDIFF_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdRef(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the reference function type & the non-CYCLING range.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      stc_ref;
    bool        changed_f = false;

    stc_ref = REF_STC_ARMED_FUNC_TYPE;

    // Reference function type (Cycling and non-Cycling)

    if(stc_ref != rtd_cls.last.ref_stc_type)
    {
        rtd_cls.last.ref_stc_type = stc_ref;

        if(!stc_ref)
        {
            stc_ref = STC_NONE;
        }

        fprintf(rtd.f,RTD_START_POS,RTD_REFTYPE_POS);           // Move cursor
        fprintf(rtd.f,"%-10s",SYM_TAB_CONST[rtd_cls.last.ref_stc_type].key.c);// Display symbol for type
        fputs(RTD_END,rtd.f);                       // Restore cursor position

        changed_f = true;
    }

    // Range (start -> end) (non-Cycling only)

    if(STATE_PC != FGC_PC_CYCLING)
    {
        if(rtd_cls.last.ref_end != dpcls.dsp.ref.end)
        {
            rtd_cls.last.ref_end = dpcls.dsp.ref.end;

            fprintf(rtd.f,RTD_START_POS,RTD_REF_START_POS);             // Move cursor
            fprintf(rtd.f,"%9.2f",dpcls.dsp.ref.start);                 // Display start of ref range
            fputs(RTD_END,rtd.f);                                       // Restore cursor position

            fprintf(rtd.f,RTD_START_POS,RTD_REF_END_POS);               // Move cursor
            fprintf(rtd.f,"%9.2f",dpcls.dsp.ref.end);                   // Display end of ref range
            fputs(RTD_END,rtd.f);                                       // Restore cursor position

            changed_f = true;
        }
    }

    return(changed_f);
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdTrun(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the change run time (in full seconds, no fractional part) on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t s;    // Time in seconds

    OS_ENTER_CRITICAL();

    // Copy from DPRAM, protected from interrupts
    // Double cast is needed to satisfy the compiler

    s = (uint32_t)(int32_t)dpcls.dsp.ref.time_running;

    OS_EXIT_CRITICAL();

    return(RtdInt32u(&s, &rtd_cls.last.trun, "%6lu", RTD_TRUN_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdTrem(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the change remain time (in full seconds, no fractional part) on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t s;    // Time in seconds

    OS_ENTER_CRITICAL();

    // Copy from DPRAM, protected from interrupts
    // Double cast is needed to satisfy the compiler

    s = (uint32_t)(int32_t)dpcls.dsp.ref.time_remaining;

    OS_EXIT_CRITICAL();

    return(RtdInt32u(&s, &rtd_cls.last.trem, "%6lu", RTD_TREM_POS));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdAdcs(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the extra information about the ADC signals on the option line.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  adc_chan;
    uint16_t  adc_field;
    uint16_t  hpos;
    char    pos[6];
    char    value[12];
    uint32_t  raw;
    FP32    f;

    hpos = 0;

    rtd.optline.idx = (rtd.optline.idx + 1) % 8;    // Adjust index

    adc_chan  = rtd.optline.idx / 4;            // Chan:  0=A, 1=B
    adc_field = rtd.optline.idx % 4;            // Field: 0=Raw, 1=Volts, 2=PP noise, 3=Amps

    switch(adc_field)       // Check field for specified channel
    {
        case 0: // Raw ADC value field

            OS_ENTER_CRITICAL();
            raw = dpcls.dsp.adc.raw_1s[adc_chan];           // Take local copy of raw ADC value
            OS_EXIT_CRITICAL();

            if(raw != rtd_cls.optline.adc_raw[adc_chan])    // If raw value has changed
            {
                hpos = RTD_ADCS_HPOS_RAW;
                rtd_cls.optline.adc_raw[adc_chan] = raw;
                sprintf(value,"%9lu",raw);
            }
            break;

        case 1: // Volts ADC value field

            OS_ENTER_CRITICAL();
            f = dpcls.dsp.adc.volts_1s[adc_chan];           // Take local copy of ADC volts value
            OS_EXIT_CRITICAL();

            if(f != rtd_cls.optline.adc_volts[adc_chan])    // If volts value has changed
            {
                hpos = RTD_ADCS_HPOS_VOLTS;
                rtd_cls.optline.adc_volts[adc_chan] = f;
                sprintf(value,"%10.6f",f);
            }
            break;

        case 2: // Peak-peak noise ADC value field

            OS_ENTER_CRITICAL();
            f = dpcls.dsp.adc.pp_volts_1s[adc_chan];    // Take local copy of ADC pp noise value
            OS_EXIT_CRITICAL();

            if(f != rtd_cls.optline.adc_pp[adc_chan])   // If PP noise value has changed
            {
                hpos = RTD_ADCS_HPOS_PP;
                rtd_cls.optline.adc_pp[adc_chan] = f;
                sprintf(value,"%.1E",f);
            }
            break;

        case 3: // Amps ADC value field

            OS_ENTER_CRITICAL();
            f = dpcls.dsp.adc.amps_1s[adc_chan];        // Take local copy of ADC pp noise value
            OS_EXIT_CRITICAL();

            if(f != rtd_cls.optline.adc_amps[adc_chan]) // If PP noise value has changed
            {
                hpos = RTD_ADCS_HPOS_AMPS;
                rtd_cls.optline.adc_amps[adc_chan] = f;
                sprintf(value,"%10.3f",f);
            }
            break;
    }

    if(hpos)                // If field has changed
    {
        sprintf(pos, "%u;%u",                           // Prepare cursor position
                RTD_OPTLINE_VPOS,
                (hpos + RTD_ADCS_HWIDTH * adc_chan));
        fprintf(rtd.f,RTD_START_POS,(char * FAR)pos);   // Move cursor
        fputs(value,rtd.f);                             // Display value field
        fputs(RTD_END,rtd.f);                           // Restore cursor position
        return(true);
    }

    return(false);              // Report that field has not been written
}
/*---------------------------------------------------------------------------------------------------------*/
#if FGC_CLASS_ID == 51
    /*---------------------------------------------------------------------------------------------------------*/
    bool RtdProt(void)
    /*---------------------------------------------------------------------------------------------------------*\
      This function will display the extra information about the current lead voltages and IEARTH signal
      on the option line.  If the ILEAD signals are not present then they will not be displayed.
    \*---------------------------------------------------------------------------------------------------------*/
    {
        static char * FAR       fmt[]           = { "IEARTH:%+7.4fA %+6dc%%", "POS:%6.3f", "NEG:%6.3f" };
        static char * FAR       pos[]           = { RTD_PROT_IEARTH, RTD_PROT_POS_POS, RTD_PROT_NEG_POS  };

        rtd.optline.idx++;
        if ( rtd.optline.idx > (dpcls.mcu.vs.u_lead_pos_chan == -1 ? 0 : 2) )
        {
            rtd.optline.idx = 0;

            /*--- IEARTH ---*/

            if(dpcls.dsp.meas.i_earth != rtd.optline.i_earth)
            {
                fprintf(rtd.f,RTD_START_POS,pos[0]);                        // Move cursor
                fprintf(rtd.f,fmt[0],dpcls.dsp.meas.i_earth,I_EARTH_CPCNT); // Write values
                fputs(RTD_END,rtd.f);                                       // Restore cursor position
                return(true);
            }
            return(false);
        }

        /*--- ILEADS ---*/

        return(RtdFloat(&dpcls.dsp.meas.ileads.u_leads[rtd.optline.idx-1],
                &rtd_cls.optline.u_leads[rtd.optline.idx-1],
                &rtd.optline.float_s[rtd.optline.idx][0],
                fmt[rtd.optline.idx],
                pos[rtd.optline.idx]));
    }
#endif
/*---------------------------------------------------------------------------------------------------------*/
bool RtdCyc(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the extra information about the current cycle on the option line.
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t          user;
    FP32            max_ref;
    uint16_t          reg_mode;
    static char     ref_units[] ="VAG ";    // volts, amps, gauss, none

    OS_ENTER_CRITICAL();

    user     = dpcom.dsp.cyc.user;
    max_ref  = dpcls.dsp.ref.max;
    reg_mode = dpcls.dsp.reg.state.int32u;

    OS_EXIT_CRITICAL();

    if(user != rtd_cls.optline.user)
    {
        fprintf(rtd.f,RTD_START_POS "USR:%3d MX:%6.0f%c" RTD_END,
                RTD_CYC,user,max_ref,ref_units[reg_mode]);

        rtd_cls.optline.user = user;
        return(true);
    }
    return(false);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rtd_class.c
\*---------------------------------------------------------------------------------------------------------*/
