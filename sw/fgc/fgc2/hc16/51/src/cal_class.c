/*---------------------------------------------------------------------------------------------------------*\
  File:         cal_class.c

  Purpose:      FGC HC16 Class 51 Software - Automatical ADC/DAC calibration Functions

  Author:       Quentin.King@cern.ch

  Notes:        This file contains functions which support the automatic calibration of the
                ADCs, DCCTs and DAC.
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h
#define CAL_CLASS_GLOBALS   // define cal global variable

#include <cal_class.h>

#include <stdbool.h>
#include <cmd.h>
#include <nvs.h>        // for nvs global variable
#include <fbs_class.h>  // for STATE_OP, ST_MEAS_A, ST_MEAS_B
#include <defprops.h>
#include <dev.h>        // for dev global variable
#include <prop.h>       // for PropSetNumEls()
#include <mst.h>        // for mst global variable
#include <dpcom.h>      // for dpcom global variable
#include <sta.h>        // for sta global variable
#include <dpcls.h>      // for dpcls global variable
#include <dls.h>        // for temp  global variable
#include <macros.h>     // for Set()
#include <fbs.h>        // for fbs global variable
#include <memmap_mcu.h> // for SM_UXTIME_P

/*---------------------------------------------------------------------------------------------------------*/
void CalRunSequence(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the Sta Task when the OP state is CALIBRATING and the millisecond time
  is 102, 302, 502, 702, or 902. It is responsible for directing the DSP in running the auto calibration
  sequence for the ADCs, DAC and DCCTs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              last_seq_idx;
    struct cal_seq *    next_seq;

    /*--- Define calibration sequences ---*/

    static struct cal_seq       cal_seq_both_adc16s[] =         // ADC16 complete auto calibration
    {
        { CalSetAdcMpxInternal, 0,                              0                           },
        { CalSetAdc16Mpx,       FGC_INTERNAL_ADC_MPX_CALZERO,   FGC_INTERNAL_ADC_MPX_CALZERO},
        { CalBothAdc16s,        CAL_SUM16_PERIOD,               0                           },
        { CalWaitDsp,           0,                              0                           },
        { CalSetAdc16Mpx,       FGC_INTERNAL_ADC_MPX_CALPOS,    FGC_INTERNAL_ADC_MPX_CALNEG },
        { CalBothAdc16s,        CAL_SUM16_PERIOD,               1                           },
        { CalWaitDsp,           0,                              0                           },
        { CalSetAdc16Mpx,       FGC_INTERNAL_ADC_MPX_CALPOS,    FGC_INTERNAL_ADC_MPX_CALPOS },
        { CalBothAdc16s,        CAL_SUM16_PERIOD,               2                           },
        { CalWaitDsp,           0,                              0                           },
        { CalSetAdc16Mpx,       FGC_INTERNAL_ADC_MPX_CALNEG,    FGC_INTERNAL_ADC_MPX_CALPOS },
        { CalBothAdc16s,        CAL_SUM16_PERIOD,               3                           },
        { CalWaitDsp,           0,                              0                           },
        { CalSetAdc16Mpx,       FGC_INTERNAL_ADC_MPX_CALNEG,    FGC_INTERNAL_ADC_MPX_CALNEG },
        { CalBothAdc16s,        CAL_SUM16_PERIOD,               4                           },
        { CalWaitDsp,           0,                              0                           },
        { CalSetAdc16Mpx,       FGC_INTERNAL_ADC_MPX_DCCTA,     FGC_INTERNAL_ADC_MPX_DCCTB  },
        { CalEndBothAdc16s,     0,                              0                           },
    };

    static struct cal_seq       cal_seq_adc16[] =               // ADC16 partial auto calibration
    {
        { CalAdc16,             CAL_SUM16_PERIOD,               0                           },
        { CalWaitDsp,           0,                              0                           },
        { CalEndAdc16,          0,                              0                           },
    };

    static struct cal_seq       cal_seq_adc22[] =               // ADC22 partial auto calibration
    {
        { CalAdc22OrDcctErr,    CAL_SUM22_PERIOD,               0                           },
        { CalWaitDsp,           0,                              0                           },
        { CalEndAdc22,          0,                              0                           },
    };

    static struct cal_seq       cal_seq_dcct[] =                // DCCT partial auto calibration
    {
        { CalAdc22OrDcctErr,    CAL_DCCT_PERIOD,                0                           },
        { CalWaitDsp,           0,                              0                           },
        { CalEndDcct,           0,                              0                           },
    };

    static struct cal_seq       cal_seq_dac[] =                 // DAC complete auto calibration
    {
        { CalSetAdcMpxInternal, 0,                              0                           },
        { CalSetAdc16Mpx,       FGC_INTERNAL_ADC_MPX_DAC,       FGC_INTERNAL_ADC_MPX_DAC    },
        { CalDac,               0,                              0                           },
        { CalWaitDsp,           0,                              0                           },
        { CalSetAdc16Mpx,       FGC_INTERNAL_ADC_MPX_DCCTA,     FGC_INTERNAL_ADC_MPX_DCCTB  },
        { CalEnd,               0,                              0                           },
    };

    static struct cal_seq *     cal_seq[] =                     // List of sequences (dpcls.mcu.cal.action)
    {                                                           // The order must match CAL_REQ_xxx constants
        cal_seq_both_adc16s,                                    // CAL_REQ_BOTH_ADC16S
        cal_seq_adc16,                                          // CAL_REQ_ADC16
        cal_seq_adc22,                                          // CAL_REQ_ADC22
        cal_seq_dcct,                                           // CAL_REQ_DCCT
        cal_seq_dac                                             // CAL_REQ_DAC
    };

    /*--- Run sequence for specified type ---*/

    do
    {
        last_seq_idx = cal.seq_idx;                                             // Save sequence position
        next_seq     = &cal_seq[(uint16_t)dpcls.mcu.cal.action][cal.seq_idx];     // Get next sequence structure
        cal.seq_idx += next_seq->func(next_seq->par1,next_seq->par2);           // Run sequence function
    }
    while(cal.seq_idx && cal.seq_idx != last_seq_idx);          // Repeat until sequence ends or waits
}
#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalSetAdcMpxInternal(uint16_t unused1, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  Set ADC filter MPX to select the internal ADCs - this is needed to calibrate the ADC16s or DAC
\*---------------------------------------------------------------------------------------------------------*/
{
    dpcls.mcu.cal.imeas_zero = 200;                     // Block IMEAS till ~200ms after cal

    sta.adc_filter_mpx[0] = FGC_ADC_FILTER_MPX_ADC16A;  // Select internal ADCs
    sta.adc_filter_mpx[1] = FGC_ADC_FILTER_MPX_ADC16B;

    return(1);                                          // Advance to next sequence element
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalSetAdc16Mpx(uint16_t internal_adc_mpx_A, uint16_t internal_adc_mpx_B)
/*---------------------------------------------------------------------------------------------------------*\
  Set ADC16 analogue MPX to specified channels
\*---------------------------------------------------------------------------------------------------------*/
{
    sta.internal_adc_mpx[0] = internal_adc_mpx_A;       // Set analog MPX to select relevant mpx input
    sta.internal_adc_mpx[1] = internal_adc_mpx_B;

    return(1);                                          // Advance to next sequence element
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalWaitDsp(uint16_t unused1, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  Wait until DSP completes calibration
\*---------------------------------------------------------------------------------------------------------*/
{
    if((uint16_t)dpcls.dsp.cal_complete_f ||              // If calibration complete in DSP or
       !mst.dsp_rt_is_alive)                            // DSP is no longer available
    {
        dpcls.mcu.cal.req_f      = false;
        dpcls.dsp.cal_complete_f = false;
        return(1);                                      // Advance to next step
    }

    return(0);                                  // Remain at this sequence element
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalBothAdc16s(uint16_t ave_steps, uint16_t meas_idx)
/*---------------------------------------------------------------------------------------------------------*\
  Request DSP to calibrate internal ADC16s.
\*---------------------------------------------------------------------------------------------------------*/
{
    Set(ST_MEAS_A,FGC_MEAS_CAL_ACTIVE);
    Set(ST_MEAS_B,FGC_MEAS_CAL_ACTIVE);

    dpcls.mcu.cal.idx       = meas_idx;
    dpcls.mcu.cal.ave_steps = ave_steps;
    dpcls.mcu.cal.req_f     = true;

    return(1);                                          // Advance to next sequence element
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalAdc16(uint16_t ave_steps, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  Request DSP to calibrate the error for one channel of an ADC16.  The function first sets the ADC
  multiplexor to select the analogue calibration source
\*---------------------------------------------------------------------------------------------------------*/
{
    static uint16_t  cal_internal_adc_mpx[] = { FGC_INTERNAL_ADC_MPX_CALZERO, FGC_INTERNAL_ADC_MPX_CALPOS, FGC_INTERNAL_ADC_MPX_CALNEG };

    if(dpcls.mcu.cal.chan_mask == 0x01) // Channel A
    {
        Set(ST_MEAS_A, FGC_MEAS_CAL_ACTIVE);
        sta.internal_adc_mpx[0] = cal_internal_adc_mpx[dpcls.mcu.cal.idx];
    }
    else                                // Channel B
    {
        Set(ST_MEAS_B, FGC_MEAS_CAL_ACTIVE);
        sta.internal_adc_mpx[1] = cal_internal_adc_mpx[dpcls.mcu.cal.idx];
    }

    dpcls.mcu.cal.ave_steps = ave_steps;
    dpcls.mcu.cal.req_f     = true;

    return(1);                                          // Advance to next sequence element
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalAdc22OrDcctErr(uint16_t ave_steps, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  Request DSP to calibrate the error for one channel of an ADC22 or DCCT.
\*---------------------------------------------------------------------------------------------------------*/
{
    if((dpcls.mcu.cal.chan_mask & 0x01))
    {
        Set(ST_MEAS_A,FGC_MEAS_CAL_ACTIVE);
    }

    if((dpcls.mcu.cal.chan_mask & 0x02))
    {
        Set(ST_MEAS_B,FGC_MEAS_CAL_ACTIVE);
    }

    dpcls.mcu.cal.ave_steps = ave_steps;
    dpcls.mcu.cal.req_f     = true;

    return(1);                                          // Advance to next sequence element
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalDac(uint16_t unused1, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  Request DSP to calibrate the DAC using the internal ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    dpcls.mcu.cal.req_f = true;

    return(1);                                          // Advance to next sequence element
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalEndBothAdc16s(uint16_t unused1, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  End auto calibration of Internal ADC errors.  The function must save the values in the non-volatile storage.
\*---------------------------------------------------------------------------------------------------------*/
{
    cal.last_cal_time_unix = SM_UXTIME_P;

    dpcls.mcu.cal.adc.internal[0].err[3] =
    dpcls.mcu.cal.adc.internal[1].err[3] = ID_TEMP_FLOAT(temp.log.fgc_in);

    PropSetNumEls(NULL, &PROP_CAL_A_ADC_INTERNAL_ERR, 6);
    PropSetNumEls(NULL, &PROP_CAL_B_ADC_INTERNAL_ERR, 6);

    NvsStoreBlockForOneProperty(NULL, &PROP_ADC_INTERNAL_LAST_CAL_TIME, 0, 1, true, (uint8_t *)&cal.last_cal_time_unix);
    NvsStoreBlockForOneProperty(NULL, &PROP_CAL_A_ADC_INTERNAL_ERR,  0, 6, true, (uint8_t *)&dpcls.mcu.cal.adc.internal[0].err);
    NvsStoreBlockForOneProperty(NULL, &PROP_CAL_B_ADC_INTERNAL_ERR,  0, 6, true, (uint8_t *)&dpcls.mcu.cal.adc.internal[1].err);

    return(CalEnd(true,0));             // End with SYNC_DB_CAL
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalEndAdc16(uint16_t unused1, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  End auto calibration of one error for one ADC16
\*---------------------------------------------------------------------------------------------------------*/
{
    if(dpcls.mcu.cal.chan_mask == 0x01) // Channel A
    {
        dpcls.mcu.cal.adc.internal[0].err[3] = ID_TEMP_FLOAT(temp.log.fgc_in);
        PropSetNumEls(NULL, &PROP_CAL_A_ADC_INTERNAL_ERR, 6);
        NvsStoreBlockForOneProperty(NULL, &PROP_CAL_A_ADC_INTERNAL_ERR, 0, 6, true, (uint8_t *)&dpcls.mcu.cal.adc.internal[0].err);
    }
    else                                // Channel B
    {
        dpcls.mcu.cal.adc.internal[1].err[3] = ID_TEMP_FLOAT(temp.log.fgc_in);
        PropSetNumEls(NULL, &PROP_CAL_B_ADC_INTERNAL_ERR, 6);
        NvsStoreBlockForOneProperty(NULL, &PROP_CAL_B_ADC_INTERNAL_ERR, 0, 6, true, (uint8_t *)&dpcls.mcu.cal.adc.internal[1].err);
    }

    return(CalEnd(true,0));             // End with SYNC_DB_CAL
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalEndAdc22(uint16_t unused1, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  End auto calibration of one error for one ADC22
\*---------------------------------------------------------------------------------------------------------*/
{
    if((dpcls.mcu.cal.chan_mask & 0x01))
    {
        dpcls.mcu.cal.adc.external[0].err[3] = ID_TEMP_FLOAT(temp.ext_adc[0].mod);
        PropSetNumEls(NULL, &PROP_CAL_A_ADC_EXTERNAL_ERR, 6);
        NvsStoreBlockForOneProperty(NULL, &PROP_CAL_A_ADC_EXTERNAL_ERR, 0, 6, true, (uint8_t *)&dpcls.mcu.cal.adc.external[0].err);
    }

    if((dpcls.mcu.cal.chan_mask & 0x02))
    {
        dpcls.mcu.cal.adc.external[1].err[3] = ID_TEMP_FLOAT(temp.ext_adc[1].mod);
        PropSetNumEls(NULL, &PROP_CAL_B_ADC_EXTERNAL_ERR, 6);
        NvsStoreBlockForOneProperty(NULL, &PROP_CAL_B_ADC_EXTERNAL_ERR, 0, 6, true, (uint8_t *)&dpcls.mcu.cal.adc.external[1].err);
    }

    return(CalEnd(false,0));             // End the sequence, but do not call SYNC_DB_CAL
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalEndDcct(uint16_t unused1, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  End auto calibration of one error for one DCCT
\*---------------------------------------------------------------------------------------------------------*/
{
    if((dpcls.mcu.cal.chan_mask & 0x01))
    {
        dpcls.mcu.cal.dcct[0].err[3] = ID_TEMP_FLOAT(temp.dcct[0].elec);
        PropSetNumEls(NULL, &PROP_CAL_A_DCCT_ERR, 6);
        NvsStoreBlockForOneProperty(NULL, &PROP_CAL_A_DCCT_ERR, 0, 6, true, (uint8_t *)&dpcls.mcu.cal.dcct[0].err);
    }

    if((dpcls.mcu.cal.chan_mask & 0x02))
    {
        dpcls.mcu.cal.dcct[1].err[3] = ID_TEMP_FLOAT(temp.dcct[1].elec);
        PropSetNumEls(NULL, &PROP_CAL_B_DCCT_ERR, 6);
        NvsStoreBlockForOneProperty(NULL, &PROP_CAL_B_DCCT_ERR, 0, 6, true, (uint8_t *)&dpcls.mcu.cal.dcct[1].err);
    }

    return(CalEnd(false,0));             // End the sequence, but do not call SYNC_DB_CAL
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CalEnd(uint16_t sync_db_f, uint16_t unused2)
/*---------------------------------------------------------------------------------------------------------*\
  End auto calibration: This has a built in pause of one 200ms iteration to allow the multiplexers to
  be reset to the standard values in case the system is unconfigured. By clearing the FGC_OP_CALIBRATING state
  in STATE_OP, this function terminates the calibration sequence.
\*---------------------------------------------------------------------------------------------------------*/
{
    static bool end_f;

    if(sync_db_f &&                                     // If SYNC_DB is required and
       sta.config_state != FGC_CFG_STATE_STANDALONE &&  // CONFIG.MODE can be modified, and
       sta.config_mode  == FGC_CFG_MODE_SYNC_NONE)      // no synchronisation is in progress or failed
    {
        sta.config_mode = FGC_CFG_MODE_SYNC_DB_CAL;
    }

    if(!end_f)
    {
        end_f = true;                   // Pause for one 200ms iteration
    }
    else
    {
        end_f = false;
        cal.seq_idx = 0;

        if(!nvs.n_unset_config_props)           // If all properties are configured
        {
            STATE_OP = sta.mode_op;                     // set operational state to operational mode
        }
        else                                    // else some properties are unconfigured
        {
            STATE_OP = FGC_OP_UNCONFIGURED;             // stay in UNCONFIGURED state
        }

        Clr(ST_MEAS_A,FGC_MEAS_CAL_ACTIVE);
        Clr(ST_MEAS_B,FGC_MEAS_CAL_ACTIVE);
    }

    return(0);
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*\
  End of file: cal_class.c
\*---------------------------------------------------------------------------------------------------------*/
