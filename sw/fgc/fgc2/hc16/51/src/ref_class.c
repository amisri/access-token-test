/*---------------------------------------------------------------------------------------------------------*\
 File:          ref_class.c

 Purpose:       FGC HC16 Software - Reference conversion functions

 Author:        Quentin.King@cern.ch
\*---------------------------------------------------------------------------------------------------------*/

#define REF_CLASS_GLOBALS

#include <ref_class.h>
#include <os.h>         // for OS_ENTER_CRITICAL / OS_EXIT_CRITICAL
#include <dpcls.h>      // for dpcls global variable
#include <defconst.h>

/*---------------------------------------------------------------------------------------------------------*/
void RefClr(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to clear a reference.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_ENTER_CRITICAL();

    // Clear RTD "Ref:"

    dpcls.dsp.ref.stc_func_type = 0;

    // Property REF.FUNC.TYPE(0) = NONE

    dpcls.mcu.ref.func.type[0] = FGC_REF_NONE;

    // Clear range in RTD

    if(dpcls.mcu.state_pc != FGC_PC_TO_STANDBY &&
       dpcls.mcu.state_pc != FGC_PC_SLOW_ABORT)
    {
        dpcls.dsp.ref.start = 0.0;
        dpcls.dsp.ref.end   = 0.0;
    }

    OS_EXIT_CRITICAL();
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ref_class.c
\*---------------------------------------------------------------------------------------------------------*/
