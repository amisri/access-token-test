/*---------------------------------------------------------------------------------------------------------*\
  File:         log_class.c

  Purpose:      FGC HC16 Class 51/53 Software - Logging related functions

  Note:         LogInitEvtProp() makes links between bits and enums for a property with a symlist
                in the structure log_evt_prop.  This means that the symbol constant values for any
                property logged must NEVER be outside the range 0-15.
\*---------------------------------------------------------------------------------------------------------*/

#include <stdbool.h>
#include <log_class.h>                          // Include all header files
#include <cmd.h>
#include <class.h>                              // Include class dependent definitions
#include <hash.h>
#include <defprops.h>                           // Include property related definitions
#include <mcu_dependent.h>
#include <mem.h>                                // for MemCpyBytes(), MemCpyWords()
#include <dpcom.h>

/*---------------------------------------------------------------------------------------------------------*/
void LogTiming(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is filling the timing log at the beginning of each cycle provided the state is OFF, FLT_OFF
  or CYCLING.
\*---------------------------------------------------------------------------------------------------------*/
{
    static int8_t reg_mode_flag[] = "VIB ";

    if(STATE_PC == FGC_PC_CYCLING || STATE_PC <= FGC_PC_OFF)
    {
        // Warning or Fault that occured during the previous cycle

        timing_log.dysfunction_code[timing_log.in_idx]       = dpcom.dsp.cyc.dysfunction_code;
        timing_log.dysfunction_is_a_fault[timing_log.in_idx] = dpcom.dsp.cyc.dysfunction_is_a_fault;

        // Increment log index

        if(++timing_log.in_idx >= FGC_LOG_CYCLES_LEN)
        {
            timing_log.in_idx = 0;
        }

        // Transfer user last to timing_log last to validate the timing_log record

        timing_log.time         [timing_log.in_idx].unix_time = dpcom.dsp.cyc.start_time.unix_time;
        timing_log.time         [timing_log.in_idx].us_time   = dpcom.dsp.cyc.start_time.us_time;
        timing_log.stc_func_type[timing_log.in_idx]           = REF_STC_ARMED_FUNC_TYPE;
        timing_log.reg_mode_flag[timing_log.in_idx]           = reg_mode_flag[dpcls.dsp.reg.state.int32u];
        timing_log.dysfunction_code[timing_log.in_idx]        = FGC_CYC_WRN_NONE;
        timing_log.dysfunction_is_a_fault[timing_log.in_idx]  = false;

        // Keep as last line. Note that user==0 means empty record

        timing_log.user         [timing_log.in_idx]           = dpcom.dsp.cyc.user_timing;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtTiming(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at 200Hz from StaTsk() to check if the timing log needs to be written to the
  event log.  This will be done whenever the system leaves the CYCLING state.  It will write a maximum of
  one entry to the event log per call.

  Log record:

        Unix time       Start of cycle time
        us_time         Start of cycle time
        prop[24]        "USER=NN LENGTH=LL SSC=Y/N"
        value[36]       "REG_MODE=x FUNC_TYPE=xxxxxxxxxxxxx"
        action[8]       "START"
\*---------------------------------------------------------------------------------------------------------*/
{
    int8_t       user;
    int8_t       ssc_f;
    uint16_t      idx;
    uint16_t      s;
    uint32_t      log_evt_addr;

    // Log cycle time stamps from circular buffer

    idx  = timing_log.out_idx++;
    user = timing_log.user[idx];

    // If log record is complete

    if(user != 0)
    {
        // Negative user indicates start of supercycle

        if(user < 0)
        {
            ssc_f = 'Y';
            user  = -user;
        }
        else
        {
            ssc_f = 'N';
        }

        timing_log.log_rec.timestamp = timing_log.time[idx];
        timing_log.log_rec.prop[5]   = '0' + (user / 10);
        timing_log.log_rec.prop[6]   = '0' + (user % 10);
        timing_log.log_rec.prop[12]  = ssc_f;
        timing_log.log_rec.value[9]  = timing_log.reg_mode_flag[idx];

        MemCpyStrFar(&timing_log.log_rec.value[21], SYM_TAB_CONST[timing_log.stc_func_type[idx]].key.c);

        if(++log_evt.index >= FGC_LOG_EVT_LEN)
        {
            log_evt.index = 0;
        }

        // Use intermediate var to avoid compiler bug

        s = sizeof(struct log_evt_rec) * log_evt.index;
        log_evt_addr = LOG_EVT_32 + s;

        MemCpyWords(log_evt_addr, (uint32_t)&timing_log.log_rec, sizeof(struct log_evt_rec)/2);

        SM_LOGEVTIDX_P = log_evt.index;
    }
}
#if FGC_CLASS_ID == 51
/*---------------------------------------------------------------------------------------------------------*/
void LogIab(void)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function stores one sample in the IAB log.
  The unfiltered DCCT current measurements are copied from dpram into meas.ia and meas.ib by IsrMst().
  This function is partly written in assembler because it is time critical - it is called at the
  beginning of each millisecond from the MstClass() function.
\*---------------------------------------------------------------------------------------------------------*/
{
    ENTER_SR();                         // Select RAM in pages 23
    LogNextAnaSample(&log_iab);         // Return next record address in A:B:Y

    asm
    {
        PSHM    K                       // Save page registers
        TBYK                            // YK = B

        LDX     @meas.ia                // X = Address of data in page 0
        LDD      0,X                    // Copy 4 words (Ia,Ib) from (IX) to (IY)
        STD      0,Y
        LDD      2,X
        STD      2,Y
        LDD      4,X
        STD      4,Y
        LDD      6,X
        STD      6,Y

        PULM    K                       // Restore page registers

        TSTW    log_iab.run_f           // If logging is still active
        BNE     end                     // Skip decrement

        DECW    log_iab.samples_to_acq  // Decrement samples remaining counter
    end:
    }

    log_iab.last_sample_time = mst.prev_time;   // Save time stamp from previous sample

    EXIT_SR();                          // Deselect RAM in pages 23
}
/*---------------------------------------------------------------------------------------------------------*/
void LogIearth(void)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function stores one sample in the IEARTH log.
  The Iearth signal comes from an analogue DIM channel, processed by the DSP and placed in the DPRAM at
  20ms intervals.  This function is partly written in assembler because it is time critical - it is
  called at the end of millisecond 5 of 20 from the MstClass() function.
\*---------------------------------------------------------------------------------------------------------*/
{
    ENTER_SR();                         // Select RAM in pages 23
    LogNextAnaSample(&log_iearth);      // Return next record address in A:B:Y

    asm
    {
        PSHM    K                       // Save page registers
        TBYK                            // YK = B = Page address

        LDX     @dpcls.dsp.meas.i_earth // X = Address of data in page 0
        LDD      0,X                    // Copy 2 words (Iearth) from (IX) to (IY)
        STD      0,Y
        LDD      2,X
        STD      2,Y

        PULM    K                       // Restore page registers

        TSTW    log_iearth.run_f        // If logging is still active
        BNE     end                     // Skip decrement

        DECW    log_iearth.samples_to_acq // Decrement samples remaining counter
    end:
    }

    log_iearth.last_sample_time = mst.time10ms;                 // Time adjusted to start of 10 ms

    EXIT_SR();                          // Deselect RAM in pages 23
}
/*---------------------------------------------------------------------------------------------------------*/
void LogIleads(void)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function stores one sample in the ILEADS log.
  The ILEADS signals comes from analogue DIM channels are are only significant in the 60A and 120A power
  converters.  They processed by the DSP and placed in the DPRAM at the start of each second, along with
  the 20ms measurement of the current from the same instant.  This function is partly written in
  assembler because it is time critical - it is called at the end of millisecond 8 of each second
  from the MstClass() function.
\*---------------------------------------------------------------------------------------------------------*/
{
    ENTER_SR();                         // Select RAM in pages 23
    LogNextAnaSample(&log_ileads);      // Return next record address in A:B:Y

    asm
    {
        PSHM    K                       // Save page registers
        TBYK                            // YK = B

        LDX     @dpcls.dsp.meas.ileads  // X = Address of data in page 0
        LDD      0,X                    // Copy 8 words (Imeas,Vmeas,UleadPos,UleadNeg) from (IX) to (IY)
        STD      0,Y
        LDD      2,X
        STD      2,Y
        LDD      4,X
        STD      4,Y
        LDD      6,X
        STD      6,Y
        LDD      8,X
        STD      8,Y
        LDD     10,X
        STD     10,Y
        LDD     12,X
        STD     12,Y
        LDD     14,X
        STD     14,Y

        PULM    K                       // Restore page registers

        TSTW    log_ileads.run_f        // If logging is still active
        BNE     end                     // Skip decrement

        DECW    log_ileads.samples_to_acq // Decrement samples remaining counter
    end:
    }

    log_ileads.last_sample_time = mst.time10ms;                 // Time adjusted to start of 10 ms

    EXIT_SR();                          // Deselect RAM in pages 23
}
/*---------------------------------------------------------------------------------------------------------*/
void LogIreg(void)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function stores one sample in the IREG log.  The IREG signals are copied into the FIP
  status variable buffer on millisecond 1 of 20 by MstTsk() which calls this function on millisecond 6 of 20
  to record the values in the IREG log buffer.  Note that the log must start at the beginning of the page
  because of the XGDY instruction!
\*---------------------------------------------------------------------------------------------------------*/
{
    ENTER_SR();                         // Select RAM in pages 23
    LogNextAnaSample(&log_ireg);        // Return next record address in A:B:Y

    asm
    {
        PSHM    K                       // Save page registers
        TBYK                            // YK = B

        LDX     @dpcls.dsp.ref.i_rst
        LDD      0,X                    // Copy 2 words (Iref_rst) from (IX) to (IY)
        STD      0,Y
        LDD      2,X
        STD      2,Y

        LDX     @I_MEAS                 // X = offset in page 0 for analogue signals data
        LDD      0,X                    // Copy 6 words (Imeas, Vref, Vmeas) from (IX) to (IY)
        STD      4,Y
        LDD      2,X
        STD      6,Y
        LDD      4,X
        STD      8,Y
        LDD      6,X
        STD     10,Y
        LDD      8,X
        STD     12,Y
        LDD     10,X
        STD     14,Y

        PULM    K                       // Restore page registers

        TSTW    log_ireg.run_f          // If logging is still active
        BNE     end                     // Skip decrement

        DECW    log_ireg.samples_to_acq // Decrement samples remaining counter
    end:
    }

    log_ireg.last_sample_time = mst.time10ms;                   // Time adjusted to start of 10 ms

    EXIT_SR();                          // Deselect RAM in pages 23
}
#else
/*---------------------------------------------------------------------------------------------------------*/
void LogCycle(void)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function stores one sample in the POPS log. The 1kHz signals are copied from dpram into
  the pmsigs structure by IsrMst().  This function is partly written in assembler because it is time
  critical - it is called at the beginning of each millisecond from the MstClass() function.
\*---------------------------------------------------------------------------------------------------------*/
{
    ENTER_SR();                         // Select RAM in pages 23

    asm
    {
        LDD     log_cycle.last_sample_idx       // D = log.index
        ADDD    #1                              // D = log.index + 1
        CPD     log_cycle.n_samples             // If D >= buffer length
        BLT     store                           // Skip to store
        CLRD                                    // D = 0 (reset buffer index to zero)

    store:
        STD     log_cycle.last_sample_idx       // Save new index
        LDE     log_cycle.sample_size           // E = size of sample in bytes
        EMUL                                    // E:D = E * D = offset to next sample to write in buffer
        ADDD    log_cycle.baseaddr:2            // Add low word of base address
        ADCE    log_cycle.baseaddr:0            // Add high word of base address
        XGDY                                    // Y = offset in page to start of buffer
        XGDE                                    // D = page address
        PSHM    K                               // Save page registers
        TBYK

        LDED    pm_sigs.ref                     // Copy 24 words from pm_sigs to (IY)
        STE     0,Y
        STD     2,Y
        LDED    pm_sigs.v_ref
        STE     4,Y
        STD     6,Y
        LDED    pm_sigs.b_meas
        STE     8,Y
        STD     10,Y
        LDED    pm_sigs.i_meas
        STE     12,Y
        STD     14,Y
        LDED    pm_sigs.v_meas
        STE     16,Y
        STD     18,Y
        LDED    pm_sigs.err
        STE     20,Y
        STD     22,Y

        PULM    K                               // Restore page registers

        TSTW    log_cycle.run_f                 // If logging is still active
        BNE     end                             // Skip decrement

        DECW    log_cycle.samples_to_acq                // Decrement samples remaining counter
    end:
    }

    EXIT_SR();                                  // Deselect RAM in pages 23

    log_cycle.last_sample_time = mst.prev_time; // Timestamp from 1ms ago
}
#endif
/*---------------------------------------------------------------------------------------------------------*\
  End of file: log_class.c
\*---------------------------------------------------------------------------------------------------------*/
