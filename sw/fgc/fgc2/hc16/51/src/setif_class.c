/*---------------------------------------------------------------------------------------------------------*\
 File:          setif_class.c

 Purpose:       FGC HC16 Class 51 Software - Set Command Condition Functions
\*---------------------------------------------------------------------------------------------------------*/


#define DEFPROPS_INC_ALL    // defprops.h

#include <setif_class.h>

#include <cmd.h>        // for struct cmd
#include <crate.h>      // for crate global variable
#include <definfo.h>    // for FGC_CLASS_ID
#include <defprops.h>
#include <dpcls.h>      // for dpcls global variable
#include <fbs.h>        // for fbs global variable
#include <fbs_class.h>  // for ST_UNLATCHED, STATE_OP, STATE_VS, STATE_PC, ST_DCCT_A, ST_DCCT_B
#include <fgc_errs.h>   // for FGC_BAD_STATE
#include <log_class.h>  // for FGC_LAST_LOG
#include <macros.h>     // for Test()
#include <mst.h>        // for mst global variable
#include <sta.h>        // for sta global variable
#include <sta_class.h>  // for vs global variable

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifModeOpOk(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Can the user set MODE.OP?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (   STATE_OP != FGC_OP_NORMAL
        || crate.type == FGC_CRATE_TYPE_RECEPTION
        || STATE_PC <= FGC_PC_STOPPING)
    {
        return (0);
    }

    return(FGC_BAD_STATE);
}
#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifSwitchOk(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is operational state NORMAL, VS is OFF and all working DCCTs are report zero current
\*---------------------------------------------------------------------------------------------------------*/
{
    if(     !((STATE_OP != FGC_OP_NORMAL)                       ||
              (STATE_PC != FGC_PC_OFF)                          ||
              (vs.polarity != FGC_POL_POSITIVE                  &&
               vs.polarity != FGC_POL_NEGATIVE)                 ||
             !(ST_DCCT_A & (FGC_DCCT_FAULT | FGC_DCCT_ZERO_I))  ||
             !(ST_DCCT_B & (FGC_DCCT_FAULT | FGC_DCCT_ZERO_I))  ||
             !(uint16_t)dpcls.dsp.meas.i_zero_f) )
    {
        return(0);
    }

    return(FGC_BAD_STATE);
}
#pragma MESSAGE DEFAULT C5703



#if FGC_CLASS_ID == 51

uint16_t SetifTxIdOk(struct cmd * c)
{
    uint16_t errnum = FGC_OK_NO_RSP;

    if (STATE_PC == FGC_PC_ARMED)
    {
        errnum = transactionGetId() == 0 ? FGC_BAD_STATE : FGC_TRANSACTION_IN_PROGRESS;
    }

    return errnum;
}
#endif // FGC_CLASS_ID == 51


/*---------------------------------------------------------------------------------------------------------*\
  End of file: setif_class.c
\*---------------------------------------------------------------------------------------------------------*/

