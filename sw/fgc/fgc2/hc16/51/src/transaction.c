//! @file  transaction.c
//! @brief Provides the framework for transactional settings
//!
//! The transactions specification is documented in the link below:
//! https://wikis.cern.ch/display/COTEC/Transactions+support+in+the+Control+System


// ---------- Includes

#include <string.h>

#include <defprops.h>
#include <fgc_errs.h>

#include <transaction.h>
#include <property.h>
#include <sta.h>
#include <state.h>



// ---------  Internal structures, unions and enumerations  -------------

struct Transaction
{
    struct prop   * last_property_set;     //!< Last property set
};



// ---------  External variable definitions  ----------------------------

struct Transaction_prop transaction_prop;



// ---------  Internal variable definitions  ----------------------------

static struct Transaction transaction;



// ---------- Internal function definitions

static uint16_t transactionProcessTest(void)
{
    uint16_t func_type;
    uint16_t stc_func_type;

    if (STATE_PC != FGC_PC_IDLE)
    {
        return FGC_BAD_STATE;
    }

    if (RefGetFuncTypeFromProp(transaction.last_property_set, &func_type, &stc_func_type) == false)
    {
        return FGC_BAD_PARAMETER;
    }

    // @TODO: try to use pcm instead of tcm

    return RefArm(&tcm, NON_PPM_USER, func_type, stc_func_type);
}


static void transactionProcessCommit(uint16_t const delay_ms)
{
    dpcom.mcu.evt.start_event_delay = delay_ms == 0 ? 1000 : delay_ms;
}



static uint16_t transactionProcessRollback(void)
{
    uint16_t errnum = FGC_OK_NO_RSP;

    if (STATE_PC == FGC_PC_ARMED)
    {
        sta.flags |= STAF_IDLE;

        errnum = RefArm(&tcm, NON_PPM_USER, FGC_REF_NONE, STC_NONE);
    }

    return errnum;
}



// ---------- External function definitions

void transactionInit(void)
{
    transaction.last_property_set = NULL;
}



uint16_t transactionSetId(uint16_t const transaction_id)
{
    if (transaction_prop.id != 0)
    {
        return FGC_TRANSACTION_IN_PROGRESS;
    }

    if (transactionValidateId(transaction_id) != FGC_OK_NO_RSP)
    {
        return FGC_INVALID_TRANSACTION_ID;
    }

    transaction_prop.id    = transaction_id;
    transaction_prop.state = FGC_TRANSACTION_STATE_NOT_TESTED;

    return FGC_OK_NO_RSP;
}



uint16_t transactionGetId(void)
{
    return transaction_prop.id;
}



uint16_t transactionValidateId(uint32_t const transaction_id)
{
    return (  (transaction_id > 0 && transaction_id < UINT16_MAX)
            ? FGC_OK_NO_RSP
            : FGC_INVALID_TRANSACTION_ID);
}



uint32_t transactionAccessCheck(uint16_t    const   cmd_type,
                                uint16_t    const   cmd_transaction_id,
                                struct prop       * property)
{
    uint16_t const transaction_id = transaction_prop.id;
    uint16_t       errnum         = FGC_OK_NO_RSP;

    if (cmd_type == FGC_FIELDBUS_FLAGS_GET_CMD)
    {
        if (   cmd_transaction_id != 0 
            && cmd_transaction_id != transaction_id)
        {
            errnum = transaction_id == 0 
                   ? FGC_TRANSACTION_NOT_IN_PROGRESS 
                   : FGC_INCORRECT_TRANSACTION_ID;
        }
    }
    else if (   cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD 
             || cmd_type == FGC_FIELDBUS_FLAGS_SET_BIN_CMD)
    {
        if (cmd_transaction_id != transaction_id)
        {
            if (transaction_id == 0)
            {
                errnum = FGC_TRANSACTION_NOT_IN_PROGRESS;
            }
            else if (cmd_transaction_id == 0)
            {
                errnum = FGC_TRANSACTION_IN_PROGRESS;
            }
            else
            {
                errnum = FGC_INCORRECT_TRANSACTION_ID;
            }
        }
        else
        {
            transaction_prop.state = FGC_TRANSACTION_STATE_NOT_TESTED;
            transaction.last_property_set = property;    
        }
    }
    else
    {
        // Ignore ubscribe commands: FGC_FIELDBUS_FLAGS_SUB_CMD,
        // FGC_FIELDBUS_FLAGS_UNSUB_CMD and FGC_FIELDBUS_FLAGS_GET_SUB_CMD
    }

    return errnum;
}



uint16_t transactionTest(uint16_t const transaction_id)
{
    uint16_t errnum = FGC_UNKNOWN_TRANSACTION_ID;
    
    if (transaction_prop.id == transaction_id)
    {
        errnum = transactionProcessTest();

        transaction_prop.state = errnum == FGC_OK_NO_RSP 
                               ? FGC_TRANSACTION_STATE_TEST_OK 
                               : FGC_TRANSACTION_STATE_TEST_FAILED;
    }

    return errnum;
}


uint16_t transactionCommit(uint16_t const transaction_id, uint16_t const delay_ms)
{
    uint16_t errnum = FGC_UNKNOWN_TRANSACTION_ID;

    if (transaction_prop.id == transaction_id)
    {
        errnum = FGC_OK_NO_RSP;

        if (   transaction_prop.state == FGC_TRANSACTION_STATE_TEST_OK
            && STATE_PC == FGC_PC_ARMED)
        {
            transactionProcessCommit(delay_ms);
            transaction_prop.state = FGC_TRANSACTION_STATE_NONE;
        }
        else
        {
            transactionProcessRollback();
            transaction_prop.state = FGC_TRANSACTION_STATE_COMMIT_FAILED;
        }

        // End the transaction
        transaction_prop.id = 0;
    }

    return (transaction_prop.state == FGC_TRANSACTION_STATE_COMMIT_FAILED ? FGC_TRANSACTION_COMMIT_FAILED : errnum);
}



uint16_t transactionRollback(uint16_t const transaction_id)
{
    uint16_t errnum = FGC_UNKNOWN_TRANSACTION_ID;

    if (transaction_prop.id == transaction_id)
    {
        errnum = FGC_OK_NO_RSP;

        transactionProcessRollback();

        // End the transaction
        transaction_prop.id    = 0;
        transaction_prop.state = FGC_TRANSACTION_STATE_NONE;
    }

    return errnum;
}



void transactionAckCommitFailed(void)
{
    if (transaction_prop.state == FGC_TRANSACTION_STATE_COMMIT_FAILED)
    {
        transaction_prop.state = FGC_TRANSACTION_STATE_NONE;
    }
}


// EOF
