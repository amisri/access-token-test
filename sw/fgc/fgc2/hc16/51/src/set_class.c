/*---------------------------------------------------------------------------------------------------------*\
  File:		set_class.c

  Purpose:	FGC HC16 Class 51 Software - Set Command Functions
\*---------------------------------------------------------------------------------------------------------*/


#define DEFPROPS_INC_ALL    // defprops.h

#include <set_class.h>

#include <cal_class.h>          // for cal global variable, NON_PPM_REG_MODE
#include <cmd.h>                // for struct cmd
#include <definfo.h>            // for FGC_CLASS_ID
#include <defprops.h>
#include <dev.h>                // for dev global variable
#include <dpcls.h>              // for dpcls global variable
#include <dpcom.h>              // for dpcom global variable
#include <fbs.h>                // for fbs global variable
#include <fbs_class.h>          // for ST_UNLATCHED, STATE_OP, STATE_PC, ST_MEAS_A, ST_MEAS_B
#include <fgc_errs.h>           // for FGC_BAD_PARAMETER, FGC_BAD_STATE
#include <log.h>                // for LogStartAll()
#include <macros.h>             // for Test(), Set(), DEVICE_PPM
#include <math.h>               // for fabs()
#include <pars.h>               // for ParsSet()
#include <prop.h>               // for PropSet()
#include <ref_class.h>          // for RefArm(), RefPlep(), RefOpenloop(), RefTrim(), RefTest()
#include <sta.h>                // for sta global variable
#include <sta_class.h>          // for vs global variable, DDOP_CMD_RESET, DDOP_CMD_POL_POS, DDOP_CMD_POL_NEG, StaDdip()
#include <transaction.h>

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetPolaritySwitch(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  SetPolarity allows the FGC change the polarity switch position.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t		errnum;
    struct sym_lst * 	sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);
    if ( errnum )
    {
	return(errnum);
    }

    switch(sym_const->sym_idx)
    {
    case STC_POSITIVE:			// Request positive polarity

	Set(sta.cmd,DDOP_CMD_POL_POS);
	break;

    case STC_NEGATIVE:			// Request negative polarity

	Set(sta.cmd,DDOP_CMD_POL_NEG);
	break;

    default:				// Should never happen

    	return(FGC_BAD_PARAMETER);
    }
    return(0);
}


uint16_t SetPoint(struct cmd * c, struct prop * p)
{
    return (ParsSet(c, p));
}



uint16_t SetTableFunc(struct cmd * c, struct prop * p)
{
    uint16_t errnum; 

    // Auto-arm only if the device is in the LHC: the lsb of the omode mask is non-zero

    if ((dev.omode_mask & 0x00FF) == 0)
    {
        errnum = SetPoint(c, p);
    }
    else
    {
        errnum = ParsSet(c, p);
        if (errnum == 0)
        {
            // Attempt to arm the table function
            errnum = RefArm(c, c->mux_idx, FGC_REF_TABLE, STC_TABLE);
        }
    }

    return errnum;
}


uint16_t SetTransactionId(struct cmd * c, struct prop * p)
{
    uint16_t  * value;

    // Overwrite the error with FGC_INVALID_TRANSACTION_ID

    return (  ParsValueGet(c, p, (void**)&value) == FGC_OK_NO_RSP
            ? transactionSetId(*value)
            : FGC_INVALID_TRANSACTION_ID);
}



uint16_t SetTransactionCommit(struct cmd * c, struct prop * p)
{
    uint16_t  * value;
    uint16_t    errnum = FGC_OK_NO_RSP;

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum == FGC_OK_NO_RSP)
    {
        errnum = transactionCommit(*value, 0);
    }

    return errnum;
}



uint16_t SetTransactionRollback(struct cmd * c, struct prop * p)
{
    uint16_t  * value;
    uint16_t    errnum = FGC_OK_NO_RSP;

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum == FGC_OK_NO_RSP)
    {
        errnum = transactionRollback(*value);
    }

    return errnum;
}



uint16_t SetTransactionTest(struct cmd * c, struct prop * p)
{
    uint16_t  * value;
    uint16_t    errnum = FGC_OK_NO_RSP;

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum == FGC_OK_NO_RSP)
    {
        errnum = transactionTest(*value);
    }

    return errnum;
}



uint16_t SetTransactionAckCommitFail(struct cmd * c, struct prop * p)
{
    transactionAckCommitFailed();

    return FGC_OK_NO_RSP;
}


// EOF