/*---------------------------------------------------------------------------------------------------------*\
 File:          get_class.c

 Purpose:       FGC HC16 Class 51 Software - Get Command Functions
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h
#define GET_CLASS_GLOBALS   // define meas global variable

#include <get_class.h>

#include <class.h>
#include <stdbool.h>
#include <cmd.h>                // for tcm global variable, struct cmd
#include <definfo.h>            // for FGC_CLASS_ID
#include <defprops.h>
#include <dpcls.h>              // for dpcls global variable
#include <fbs.h>                // for FbsOutLong()
#include <fgc_errs.h>           // for FGC_BAD_STATE
#include <log_class.h>          // for timing_log, log_iab global variables, FGC_LAST_LOG
#include <macros.h>             // for Test()
#include <mcu_dependent.h>      // for ENTER_SR(), EXIT_SR
#include <property.h>           // for struct prop
#include <prop.h>               // for PropValueGet()

/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetLogPmBuf(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     Post mortem log property

  Notes:        This function is rather special and is only used by the gateway to get the LOG.PM.BUF in
                binary. It is only used in Class 51 and not in Class 53.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(c->n_arr_spec || c->step > 1)            // If array indecies are specified
    {
        return(FGC_BAD_ARRAY_IDX);
    }

    if(c == &tcm || !Test(c->getopts,GET_OPT_BIN)) // If command is not via the FIP or BIN not specified
    {
        return(0);
    }

    if(FGC_LAST_LOG.samples_to_acq)             // If logging in progress
    {
        return(FGC_BAD_STATE);
    }

    CmdStartBin(c, p, FGC_PM_BUF_LEN);          // Write binary data header

    LogGetPmEvtBuf(c);                          // Send Event  log  ( 49096     bytes)
    LogGetPmSigBuf(&log_iab,    c);             // Send IAB    log  ( 65536 + 8 bytes)
    LogGetPmSigBuf(&log_ireg,   c);             // Send IREG   log  ( 61440 + 8 bytes)
    LogGetPmSigBuf(&log_ileads, c);             // Send ILEADS log  ( 12288 + 8 bytes)
    LogGetPmSigBuf(&log_iearth, c);             // Send IEARTH log  (  4096 + 8 bytes)
                                                //                 -------------------
                                                //        TOTAL:     192488 bytes
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetLogPSU(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     PSU voltage log

  Important note: The properties FGC.DEBUG.PSU_5V.LOG / PSU_NEG_15V.LOG / PSU_POS_15V.LOG for which this
                  get function is used have the type PSULOG, where PSULOG has a fixed size of
                  52 corresponding to a log size equal to 12. The DSP will send a buffer of exactly that
                  size to the MCU and this function will cast that buffer with struct psulog defined below.

                  Therefore this function can only work correctly if the two parameters defined in the def
                  project, FGC_PSU_LOG_LEN (=12) and the PSULOG type size (=52), match.

\*---------------------------------------------------------------------------------------------------------*/
{
    bool                hex_f;                  // Formatting flag
    char * FAR          fmt;                    // Pointer to format string
    prop_size_t         n;                      // Number of elements of type PSULOG
    uint16_t              format_idx;             // Format index for DSP
    uint16_t              errnum;                 // Error number
    uint16_t              idx;
    uint16_t              circ_idx;
    struct psulog
    {
        uint32_t  start_idx;
        FP32    psu[FGC_PSU_LOG_LEN];
    }                  *psulog_ptr;             // PSU log pointer

    static char * FAR   decfmt[]   = { "","%.5E","%.7E","%13.5E" };     // Max length is 12 chars
    static char * FAR   hexfmt[]   = { "   0x%08lX","0x%lX" };          // CmdPrintfDsp in DSP lib

    hex_f = Test(c->getopts,GET_OPT_HEX);               // Prepare hex flag

    if(hex_f)                                               // If HEX
    {
        fmt = hexfmt[c->token_delim == ','];
    }
    else                                                // else DECIMAL
    {
        if(c->token_delim == ' ')                               // If NEAT
        {
            format_idx = 3;
        }
        else                                                    // else not NEAT
        {
            format_idx = 1 + !Test(p->flags,PF_NON_VOLATILE);
        }

        fmt = decfmt[format_idx];
    }

    errnum = CmdPrepareGet(c, p, 4, &n);        // Handle standard print options
    if(errnum)
    {
        return(errnum);
    }
    if(!n)                                      // If no data
    {
        return(0);                                      // Return immediately
    }

    Set(c->getopts,GET_OPT_IDX);                // Force element index option

    errnum = PropValueGet(c, p, false, c->from, (uint8_t**)&psulog_ptr);
    if(errnum)
    {
        return(errnum);
    }

    for(idx = 0; idx < FGC_PSU_LOG_LEN; idx++)
    {
        // Index in the circular buffer

        circ_idx = idx + psulog_ptr->start_idx;
        if(circ_idx >= FGC_PSU_LOG_LEN)
        {
            circ_idx -= FGC_PSU_LOG_LEN;
        }

        errnum = CmdPrintIdx( c, idx);                 // Print index if required
        if(errnum)
        {
            return(errnum);
        }

        if(hex_f)
        {
            fprintf(c->f,fmt,*((uint32_t*)&(psulog_ptr->psu[circ_idx])));           // Display as 0xXXXXXXXX
        }
        else
        {
            errnum = CmdPrintfDsp(c,format_idx,psulog_ptr->psu[circ_idx]);       // Display as x.xxxxxxEyy
            if(errnum)
            {
                return(errnum);
            }
        }
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: get_class.c
\*---------------------------------------------------------------------------------------------------------*/
