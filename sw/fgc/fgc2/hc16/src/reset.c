/*---------------------------------------------------------------------------------------------------------*\
  File:         reset.c

  Purpose:      FGC V2 HC16 Library - function to reset the HC16 peripherals

  Author:       Quentin.King@cern.ch,
\*---------------------------------------------------------------------------------------------------------*/

#include <reset.h>              // Include FGC library headers

#include <memmap_mcu.h>         // Include memory map consts
#include <mcu_dependent.h>      // for MSLEEP(), NVRAM_LOCK()
#include <iodefines.h>              // specific processor registers and constants
#include <startmain.h>          // for StartMain function

/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void ResetPeripherals(void)
/*---------------------------------------------------------------------------------------------------------*\
  This is jump to from _Startup() in the boot and main programs to reset the HC16 peripherals.  It cannot
  be called because the stack is not set up.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        /* Reset peripherals */

        LDAB    #0x00                                           // Access page 0
        TBEK

        BSETW   CPU_RESET_16,#CPU_RESET_CTRL_NETWORK_MASK16     // Reset uFIP
        BSETW   CPU_RESET_16,#CPU_RESET_CTRL_C62OFF_MASK16      // Turn off C62

        BRSET   CPU_MODEL_16,#CPU_MODEL_DSPSTAL_MASK8,runmain   // Skip ahead if C32 is standalone

        BSETW   CPU_RESET_16,#CPU_RESET_CTRL_DSP_MASK16         // Stop C32 and set in reset+tristate mode
        NOP
        BSETW   CPU_RESET_16,#CPU_RESET_CTRL_DSPTSM_MASK16

        /* Set up C context and run main() */

    runmain:
        JMP     StartMain                                       // Jump to StartMain
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: reset.c
\*---------------------------------------------------------------------------------------------------------*/

