/*---------------------------------------------------------------------------------------------------------*\
  File:         mem.c

  Purpose:      FGC2 Memory manipulation functions

  Notes:        These memory functions are written in assembler for maximum efficiency
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <mem.h>

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*--- CRC16 - Poly=1021 - Reverse=true ---*/

static const uint16_t crctab[256] =       // defined const to keep it in the boot flash in page 0
{
    0x0000, 0x1189, 0x2312, 0x329B, 0x4624, 0x57AD, 0x6536, 0x74BF,
    0x8C48, 0x9DC1, 0xAF5A, 0xBED3, 0xCA6C, 0xDBE5, 0xE97E, 0xF8F7,
    0x1081, 0x0108, 0x3393, 0x221A, 0x56A5, 0x472C, 0x75B7, 0x643E,
    0x9CC9, 0x8D40, 0xBFDB, 0xAE52, 0xDAED, 0xCB64, 0xF9FF, 0xE876,
    0x2102, 0x308B, 0x0210, 0x1399, 0x6726, 0x76AF, 0x4434, 0x55BD,
    0xAD4A, 0xBCC3, 0x8E58, 0x9FD1, 0xEB6E, 0xFAE7, 0xC87C, 0xD9F5,
    0x3183, 0x200A, 0x1291, 0x0318, 0x77A7, 0x662E, 0x54B5, 0x453C,
    0xBDCB, 0xAC42, 0x9ED9, 0x8F50, 0xFBEF, 0xEA66, 0xD8FD, 0xC974,
    0x4204, 0x538D, 0x6116, 0x709F, 0x0420, 0x15A9, 0x2732, 0x36BB,
    0xCE4C, 0xDFC5, 0xED5E, 0xFCD7, 0x8868, 0x99E1, 0xAB7A, 0xBAF3,
    0x5285, 0x430C, 0x7197, 0x601E, 0x14A1, 0x0528, 0x37B3, 0x263A,
    0xDECD, 0xCF44, 0xFDDF, 0xEC56, 0x98E9, 0x8960, 0xBBFB, 0xAA72,
    0x6306, 0x728F, 0x4014, 0x519D, 0x2522, 0x34AB, 0x0630, 0x17B9,
    0xEF4E, 0xFEC7, 0xCC5C, 0xDDD5, 0xA96A, 0xB8E3, 0x8A78, 0x9BF1,
    0x7387, 0x620E, 0x5095, 0x411C, 0x35A3, 0x242A, 0x16B1, 0x0738,
    0xFFCF, 0xEE46, 0xDCDD, 0xCD54, 0xB9EB, 0xA862, 0x9AF9, 0x8B70,
    0x8408, 0x9581, 0xA71A, 0xB693, 0xC22C, 0xD3A5, 0xE13E, 0xF0B7,
    0x0840, 0x19C9, 0x2B52, 0x3ADB, 0x4E64, 0x5FED, 0x6D76, 0x7CFF,
    0x9489, 0x8500, 0xB79B, 0xA612, 0xD2AD, 0xC324, 0xF1BF, 0xE036,
    0x18C1, 0x0948, 0x3BD3, 0x2A5A, 0x5EE5, 0x4F6C, 0x7DF7, 0x6C7E,
    0xA50A, 0xB483, 0x8618, 0x9791, 0xE32E, 0xF2A7, 0xC03C, 0xD1B5,
    0x2942, 0x38CB, 0x0A50, 0x1BD9, 0x6F66, 0x7EEF, 0x4C74, 0x5DFD,
    0xB58B, 0xA402, 0x9699, 0x8710, 0xF3AF, 0xE226, 0xD0BD, 0xC134,
    0x39C3, 0x284A, 0x1AD1, 0x0B58, 0x7FE7, 0x6E6E, 0x5CF5, 0x4D7C,
    0xC60C, 0xD785, 0xE51E, 0xF497, 0x8028, 0x91A1, 0xA33A, 0xB2B3,
    0x4A44, 0x5BCD, 0x6956, 0x78DF, 0x0C60, 0x1DE9, 0x2F72, 0x3EFB,
    0xD68D, 0xC704, 0xF59F, 0xE416, 0x90A9, 0x8120, 0xB3BB, 0xA232,
    0x5AC5, 0x4B4C, 0x79D7, 0x685E, 0x1CE1, 0x0D68, 0x3FF3, 0x2E7A,
    0xE70E, 0xF687, 0xC41C, 0xD595, 0xA12A, 0xB0A3, 0x8238, 0x93B1,
    0x6B46, 0x7ACF, 0x4854, 0x59DD, 0x2D62, 0x3CEB, 0x0E70, 0x1FF9,
    0xF78F, 0xE606, 0xD49D, 0xC514, 0xB1AB, 0xA022, 0x92B9, 0x8330,
    0x7BC7, 0x6A4E, 0x58D5, 0x495C, 0x3DE3, 0x2C6A, 0x1EF1, 0x0F78
};
#pragma MESSAGE DISABLE C1404 // Disable 'return expected' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MemCmpBytes(const uint8_t *d1, const uint8_t *d2, uint16_t n_bytes)
/*---------------------------------------------------------------------------------------------------------*\
  This function compares n_bytes of data at d1 and d2.  If they are the same it returns zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDX     d1              // IX = d1
        LDY     d2              // IY = d2
        TDE                     // E = number of bytes
        BEQ     end             // Exit immediately if E is zero

    loop:
        LDAA    0,X             // Compare (IX) to (IY)
        CMPA    0,Y
        BNE     end
        AIX     #1              // Increment X by 1
        AIY     #1              // Increment Y by 1
        ADDE    #-1             // Decrement number of bytes left to compare
        BNE     loop
    end:
        TED                     // Return 0 if data matched, non-zero otherwise
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MemCmpStrFar(const char * far s1f, const char * s2)
/*---------------------------------------------------------------------------------------------------------*\
  This function compares two strings, s1f and s2 where the first is far and the second is near.  IY = s2
  on entry.  The function returns zero if the strings match and non-zero otherwise.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        PSHM    K               // Save page registers
        LDX     s1f:1           // IX = address of s1f within page
        LDAB    s1f:0           // B  = page address for s1f
        TBXK                    // XK = B
        LDE     #1              // E  = 1 to flag mismatch

    loop:
        LDAA    0,X             // Compare (IX) to (IY)
        CMPA    0,Y
        BNE     mismatch        // Branch if different
        TSTA                    // Test if A is now zero (end of string)
        BEQ     match           // Branch if end of string reached

        AIX     #1              // Increment X by 1
        AIY     #1              // Increment Y by 1
        BRA     loop

    match:
        CLRE                    // E = 0 to flag match
    mismatch:
        TED                     // Return 0 if strings matched, non-zero otherwise
        PULM    K               // Restore page registers
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MemCmpWords(const uint32_t s1, const uint32_t s2, uint16_t n_words)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function compares the specified number of words from anywhere in the 1MB HC16 memory maps
  with anywhere else.  It uses the page registers XK and YK which must be reset by interrupt routines.
  ZK is not affected.  n_words can be in the range 0x0000 to 0xFFFF.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        /* Set up XK:IX = from address, YK:IY = to address */

        PSHM    K               // Save page registers
        LDX     s1:2            // IX = address within "s1" page
        LDAB    s1:1            // B  = number of "s1" page
        TBXK                    // XK = B
        LDY     s2:2            // IY = address within "s2" page
        LDAB    s2:1            // B  = number of "s2" page
        TBYK                    // YK = B

        /* Loop for n_words to compare s1 and s2 */

        LDE     n_words         // E  = number of words to compare
        BEQ     end             // Exit immediated if E is zero

    loop:
        LDD     0,X             // Compare (IX) to (IY)
        CPD     0,Y
        BNE     end
        AIX     #2              // Increment X by 1
        AIY     #2              // Increment Y by 1
        ADDE    #-1             // Decrement number of words left to compare
        BNE     loop
    end:
        PULM    K               // Restore page registers
        TED                     // Return (in D) 0 if data matched, non-zero otherwise
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MemCpyBytes(uint8_t *to, const uint8_t *from, uint16_t n_bytes)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function copies the specified number of bytes from one area to another in the default
  data page as efficiently as possible.  The areas shouldn't overlap and n_bytes must be <= 0x8000 (32768).
  For best efficiency both *to and *from should be word aligned.  This function must save and restore the
  page registers even though it doesn't directly manipulate then because if IX reaches the end of page
  zero, it will roll over to page 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        PSHM    K               // Save page registers
        LDX     from            // IX = address of source buffer
        LDY     to              // IY = address of destination buffer
        LDE     n_bytes         // Get number of bytes

        /* Copy blocks of 8 bytes */

    blockcopyloop:
        ADDE    #-8             // Decrement loop counter by 8 bytes
        BMI     wordcopy        // Branch when no more block copies required
        LDD     0,X             // Copy 4 words from (IX) to (IY)
        STD     0,Y
        LDD     2,X
        STD     2,Y
        LDD     4,X
        STD     4,Y
        LDD     6,X
        STD     6,Y
        AIX     #8              // Increment IX by 16 bytes
        AIY     #8              // Increment IY by 16 bytes
        BRA     blockcopyloop   // Loop back for next block

        /* Copy words in excess of an 8 byte block individually */

    wordcopy:
        ADDE    #8              // E = number of bytes left to copy (0-7)
    wordcopyloop:
        ADDE    #-2             // Decrement byte counter by 2
        BMI     bytecopy        // Branch when no more words to copy
        LDD     0,X
        STD     0,Y
        AIX     #2              // Increment addresses
        AIY     #2
        BRA     wordcopyloop    // Branch to copy next word

        /* Copy byte in excess of word */

    bytecopy:
        ADDE    #2              // E = number of bytes left to copy (0-1)
        BEQ     end             // Branch to end if no byte left to copy
        LDAB    0,X             // Copy last byte
        STAB    0,Y
    end:
        PULM    K               // Restore page registers
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MemCpyFip(void * to, const void *from, uint16_t n_bytes)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function copies blocks of 8 bytes within the default data page.  This is used to transfer
  data to and from the Fip interface.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        TDE                     // E = length in bytes
        ANDE    #255            // Limit length to 0-256 bytes
        LDY     to              // Y = to
        LDX     from            // X = from

    loop:
        LDD     0,X             // Read first word
        STD     0,Y             // Store first word
        LDD     2,X             // Read second word
        STD     2,Y             // Store second word
        LDD     4,X             // Read third word
        STD     4,Y             // Store third word
        LDD     6,X             // Read fourth word
        STD     6,Y             // Store fourth word

        ADDE    #-8             // Decrement length by 8
        BLE     end             // Finish when length is <= 0

        AIX     #8              // Increment X by 8
        AIY     #8              // Increment Y by 8
        BRA     loop            // Goto loop to copy next 8 bytes
    end:
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MemCpyStrFar(char * s1, const char * far s2f)
/*---------------------------------------------------------------------------------------------------------*\
  This function copies a far string to a near string.  s2f is passed in B:IY.  It returns the number of
  characters copied (excluding the null).
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        PSHM    K               // Save page registers
        TBYK                    // YK = B = page of s2f   IY = sf2 in page
        LDX     s1              // IX = address of s1 in default data page
        CLRE                    // Clear E (bytes copied counter)

    loop:
        LDAA    0,Y             // Copy byte
        STAA    0,X
        BEQ     end             // End when nul character is seen
        AIX     #1
        AIY     #1
        ADDE    #1				// Increment bytes copied  counter
        BRA     loop

    end:
        PULM    K               // Restore page registers
        XGDE					// Transfer E to D (bytes copied) to return to calling function
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MemCpyWords(uint32_t to, uint32_t from, uint16_t n_words)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function copies the specified number of words from anywhere in the 1MB M68HC16 memory maps
  to anywhere else.  It uses the page registers XK and YK which must be reset by interrupt routines.
  ZK is not affected.  n_words can be in the range 0x0000 to 0xFFFF.  Note that to copy one page,
  n_words = 0x8000.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        /* Set up XK:IX = from address, YK:IY = to address */

        PSHM    K               // Save page registers
        LDX     from:2          // IX = address within "from" page
        LDAB    from:1          // B  = number of "from" page
        TBXK                    // XK = B
        LDY     to:2            // IY = address within "to" page
        LDAB    to:1            // B  = number of "to" page
        TBYK                    // YK = B

        /* Copy words in excess of an 8 word block individually */

        LDE     n_words         // E  = number of words to copy
        ANDE    #7              // E  = E & 0x07
        BEQ     blockcopy       // Jump if there are no excess word to copy

    wordcopyloop:
        LDD     0,X             // Copy 1 word...
        STD     0,Y             // from (IX) to (IY)
        AIX     #2              // Increment IX by 2 bytes
        AIY     #2              // Increment IY by 2 bytes
        ADDE    #-1             // Decrement loop counter
        BNE     wordcopyloop    // Loop back until zero

        /* Copy blocks of 8 words */

    blockcopy:
        LDE     n_words         // E = number of words to copy
        ANDE    #0xFFF8         // E = E & ~7
        BEQ     end             // Jump if there are no 8 word blocks to copy

    blockcopyloop:
        LDD      0,X            // Copy 8 words from (IX) to (IY)
        STD      0,Y
        LDD      2,X
        STD      2,Y
        LDD      4,X
        STD      4,Y
        LDD      6,X
        STD      6,Y
        LDD      8,X
        STD      8,Y
        LDD     10,X
        STD     10,Y
        LDD     12,X
        STD     12,Y
        LDD     14,X
        STD     14,Y
        AIX     #16             // Increment IX by 16 bytes
        AIY     #16             // Increment IY by 16 bytes
        ADDE    #-8             // Decrement loop counter by 8 words
        BNE     blockcopyloop   // Loop back until zero
    end:
        PULM    K               // Restore page registers
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t  MemCrc16(const uint32_t address, uint32_t n_words)
/*---------------------------------------------------------------------------------------------------------*\
  Perform CRC16 calculation on the specified zone of words in HC16 flash.  The resulting crc is the return
  value for the function.  The main word loop takes 72 cycles = ~ 8us.  So 256KB will take 1s.  The crc
  table is defined as const so it is maintained in the flash in page 2. XK is changed to get access
  to the table.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      crc = 0;

    asm
    {
        PSHM    K                               // Save page registers

        LDY     address:2                       // Set YK:IY = addresss
        LDAB    address:1
        TBYK

        LDX     @crctab                         // Set XK:IX = pointer to start of crctab in page 0
        LDAB    @crctab:PAGE
        TBXK

        LDD     crc                             // D = initial CRC value
        TDE                                     // E = D
        BRA     while_n_words                   // Branch to while(n_words--)

    loop:                                       // Calc CRC for next word:

        DECW    n_words:2                       // [8]  Decrement low word of n_words

        EORB    0,Y                             // [6]  B = (crc ^ *address))
        AIY     #1                              // [2]  address++
        CLRA                                    // [2]  D = (crc ^ *address)) & 0xFF
        ASLD                                    // [2]  D *= 2 = offset in crctab
        XGDE                                    // [2]  D<->E : E = offset in crctab  D = crc
        XGAB                                    // [2]  D = crc byte swapped
        CLRA                                    // [2]  D = (crc>>8)
        EORD    E,X                             // [6]  D = (crc>>8) ^ crctab[(crc ^ *address) & 0xFF];
        TDE                                     // [2]  Save crc new value in E

        EORB    0,Y                             // [6]  B = (crc ^ *address))
        AIY     #1                              // [2]  address++
        CLRA                                    // [2]  D = (crc ^ *address)) & 0xFF
        ASLD                                    // [2]  D *= 2 = offset in crctab
        XGDE                                    // [2]  D<->E : E = offset in crctab  D = crc
        XGAB                                    // [2]  D = crc byte swapped
        CLRA                                    // [2]  D = (crc>>8)
        EORD    E,X                             // [6]  D = (crc>>8) ^ crctab[(crc ^ *address) & 0xFF];
        TDE                                     // [2]  Save crc new value in E

    while_n_words:                              // while(n_words--)
        TSTW    n_words:2                       // [6]   if low word is not zero
        BNE     loop                            // [2,6] branch to start of loop to dec low word
        TSTW    n_words:0                       // [6]   if high word is zero
        BEQ     end                             // [2,6] branch to end
        DECW    n_words:0                       // [8]   Decrement high word
        BRA     loop                            // [6]   Branch back to start of loop to dec low word

    end:
        PULM    K                               // Recover page registers
    }                                           // Return new CRC in D
}
/*---------------------------------------------------------------------------------------------------------*/
void MemSetBytes(void *from, uint8_t value, uint16_t n_bytes)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function sets the specified number of bytes in the default data page as efficiently as
  possible to the given value. n_bytes must be less than 0x8000 (32768) and to be efficient, the zone to be
  set should start word aligned.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDX     from            // IX = address of source buffer
        LDE     n_bytes         // Get number of bytes
        LDAA    value           // Get byte value to set
        TAB                     // Transfer to B to make D the same byte twice

        /* Block set of 8 bytes */

    blocksetloop:
        ADDE    #-8             // Decrement loop counter by 8 bytes
        BMI     wordset         // Branch when no more block sets required
        STD     0,X             // Set 8 bytes from (IX)
        STD     2,X
        STD     4,X
        STD     6,X
        AIX     #8              // Increment IX by 8 bytes
        BRA     blocksetloop    // Loop back for next block

        /* Set words in excess of an 8 byte block individually */

    wordset:
        ADDE    #8              // E = number of bytes left to set (0-7)
    wordsetloop:
        ADDE    #-2             // Decrement byte counter by 2
        BMI     byteset         // Branch when no more words to set
        STD     0,X
        AIX     #2              // Increment addresses
        BRA     wordsetloop     // Branch to set next word

        /* Set byte in excess of a word */

    byteset:
        ADDE    #2              // E = number of bytes left to set (0-1)
        BEQ     end             // Branch to end if no byte left to set
        STAA    0,X             // Set last byte
    end:
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MemSetWords(uint32_t from, uint16_t value, uint32_t n_words)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function sets the specified number of words starting from the given 32-bit address.
  n_words must be < 0x7FFFF (512KB) and to be efficient, the zone to be set should start word aligned.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        PSHM    K               // Save page registers
        LDX     from:2          // IX = address within "from" page
        LDAB    from:1          // B  = number of "from" page
        TBXK                    // XK = B
        LDD     n_words:0       // Get high word of n_words
        LDE     n_words:2       // Get low word of n_words
        ASRD                    // 32-bit shift right (divide by 2)
        RORE
        ASRD                    // 32-bit shift right (divide by 2)
        RORE
        ASRD                    // 32-bit shift right (divide by 2)
        LDD     value           // D = value
        RORE                    // E = n_words / 8
        BEQ     wordset

        /* Block set of 8 words */

    blocksetloop:
        STD      0,X            // Set 8 words from (IX)
        STD      2,X
        STD      4,X
        STD      6,X
        STD      8,X
        STD     10,X
        STD     12,X
        STD     14,X
        AIX     #16             // Increment IX by 8 words
        ADDE    #-1             // Decrement block loop counter
        BNE     blocksetloop    // Loop back for next block

        /* Set words in excess of an 8 word block individually */

    wordset:
        LDE     n_words:2       // Get low word of n_words
        ANDE    #7              // Mask low three bits (number of individual words to set)
        BEQ     end
    wordsetloop:
        STD     0,X
        AIX     #2              // Increment addresses
        ADDE    #-1             // Decrement byte counter by 2
        BNE     wordsetloop     // Branch to set next word

    end:
        PULM    K               // Restore page registers
    }
}
// EOF
