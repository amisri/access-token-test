/*!
 * @file init.c
 *
 * @brief FGC MCU Software - initialisation functions that are not linked to other files
 */

#define DEFPROPS_INC_ALL    // defprops.h
#define TSK_GLOBALS         // define tsk_stk global variable

#include <init.h>
#include <init_class.h>

#include <nvs.h>
#include <tsk.h>
#include <class.h>
#include <hash.h>           // for HashInit()
#include <defprops.h>       // Include property related definitions
#include <definfo.h>        // for FGC_CLASS_ID
#include <fgc/fgc_db.h>     // for access to SysDB, CompDB and DIMDB
#include <dev.h>            // for dev global variable
#include <codes_holder.h>   // for codes_holder_info global variable
#include <cmd.h>            // for tcm, fcm, pcm global variable, struct cmd, CmdTsk()
#include <fbs.h>            // for fbs global variable, FbsTsk()
#include <defconst.h>       // defconst.h is needed by fgc_fip.h because it includes fgc_code.h that needs FGC_MAX_DEVS_PER_GW
#include <fgc_code.h>       // code communication structures, for struct fgc_dev_name, struct fgc_code_info
#include <mst.h>            // for mst global variable, MstTsk()
#include <dpcom.h>          // for dpcom global variable
#include <macros.h>         // for Test(), Set()
#include <sta.h>            // for sta global variable
#include <sta_class.h>      // for StaTsk()
#include <memmap_mcu.h>     // for CPU_RESET_CTRL_DSP_MASK16, SM_UXTIME_P
#include <fip.h>            // for fip global variable
#include <runlog_lib.h>     // for RunlogWrite(), RunlogInit()
#include <fgc_runlog_entries.h> // Include run log definition, for FGC_RL_
#include <iodefines.h>      // specific processor registers and constants
#include <mem.h>            // for MemCpyWords(), MemSetBytes()
#include <trm.h>            // for TrmTsk()
#include <dls.h>            // for DlsTsk()
#include <rtd.h>            // for RtdTsk()
#include <bgp.h>            // for BgpTsk()
#if (FGC_CLASS_ID != 59)
#include <cal_class.h>      // for cal global variable
#endif
#include <string.h>         // for strlen(), strncpy()
#include <start.h>          // for StartRunningDsp(), WaitForDspStarted()
#include <prop.h>           // for PropSet() and PropMap()
#include <mcu_dependent.h>  // for SELFLASH()
#include <shared_memory.h>  // for FGC_MAINPROG_RUNNING
#include <stdint.h>

/*!
 * @brief Used for specifying type initialisation method
 */
enum InitTypeFrom
{
    INIT_TYPE_FROM_NAME,
    INIT_TYPE_FROM_PROPERTY,
    INIT_TYPE_FROM_UNKNOWN_SYSTEM_RECORD,
};

// Internal functions declarations

/*!
 * @brief Compares DB versions and initialises some of the DB-related structures
 */
static void InitDbStructures(void);

/*!
 * @brief Initialises device name based on mode (stand-alone/on-line)
 *
 * @param[out] dev_name A structure to which the function will save the name
 */
static void InitDeviceName(struct fgc_dev_name *dev_name);

/*!
 * @brief Queries SysDB for device type
 *
 * @return True if type found in DB, false otherwise
 */
static bool InitFindTypeInSysDb(void);

/*!
 * @brief Initialises device type using suggested method
 *
 * This function can initialise the type using three methods: extracting type from name, reading type from property and
 * setting type to unknown. In the last case the function will modify the structure passed as an argument.
 *
 * @param[in] from Method used for type initialisation
 * @param[in] dev_name Name structure initialised earlier
 */
static void InitType(enum InitTypeFrom from, struct fgc_dev_name const * const dev_name);

/*!
 * @brief Initialises system-related structures based on name/type
 *
 * @param[in] dev_name Name structure initialised earlier
 */
static void InitSystemStructures(struct fgc_dev_name const * const dev_name);



// Internal functions definitions



static void InitDbStructures(void)
{
    SysDbInit(SYSDB);
    CompDbInit(COMPDB);
    DimDbInit(DIMDB);

    // Check that the version of COMPDB, SYSDB and DIMDB is the same

    dev.dbs_ok_f = (SysDbVersion() == CompDbVersion() &&
                    SysDbVersion() == DimDbVersion());

}



static void InitDeviceName(struct fgc_dev_name *dev_name)
{
    // In stand-alone mode get device name from the property
    // When FIP is connected get name from NameDB

    if (fbs.id == FIP_STANDALONE_ID)
    {
        strncpy(fbs.host.name, "NONE", FGC_MAX_DEV_LEN);
        fbs.host.name[FGC_MAX_DEV_LEN] = '\0';

        strncpy(dev_name->name, "STANDALONE", FGC_MAX_DEV_LEN);
        dev_name->name[FGC_MAX_DEV_LEN] = '\0';

#if FGC_CLASS_ID != 59
        // Clear Last Calibration time if not connected to the Gateway
        cal.last_cal_time_unix = 0;
#endif
    }
    else
    {
        struct fgc_dev_name * FAR dev_name_fp;

        // Get FAR access to NameDB
        dev_name_fp    = ( (struct fgc_name_db * FAR) CODES_NAMEDB_32 )->dev;

        // Extract gateway name record
        fbs.host       = dev_name_fp[0];

        // If the gateway name field in NameDb is empty, set the host name to 'UNKNOWN'

        if(fbs.host.name[0] == '\0')
        {
            strncpy(fbs.host.name, "UNKNOWN", FGC_MAX_DEV_LEN);
            fbs.host.name[FGC_MAX_DEV_LEN] = '\0';
        }

        *dev_name      = dev_name_fp[fbs.id];

        // If the name field in NameDb is empty, set the device name to 'UNKNOWN'

        if(dev_name->name[0] == '\0')
        {
            strncpy(dev_name->name, "UNKNOWN", FGC_MAX_DEV_LEN);
            dev_name->name[FGC_MAX_DEV_LEN] = '\0';
        }

        // Take omode_mask from NameDB record
        dev.omode_mask = dev_name->omode_mask;
    }
}



static bool InitFindTypeInSysDb(void)
{
    int16_t                cmp;     // For system type comparisons
    const fgc_db_t    n_sys = SysDbLength();  // Num of systems

    // Search for type in SysDB
    // Start from 1, because record 0 is an unknown system

    dev.sys.sys_idx = 1;

    do
    {
        cmp = MemCmpStrFar(SysDbType(dev.sys.sys_idx), dev.sys.sys_type);
    }
    while ((cmp != 0) && (++dev.sys.sys_idx < n_sys));

    return (cmp == 0);
}



static void InitType(enum InitTypeFrom from, struct fgc_dev_name const * const dev_name)
{
    if (from == INIT_TYPE_FROM_NAME)
    {
        // Extract type from device name

        char *c;

        strncpy(dev.sys.sys_type, dev_name->name, FGC_SYSDB_SYS_LEN);
        dev.sys.sys_type[FGC_SYSDB_SYS_LEN] = '\0';

        // Replace first '.' with null

        for (c = dev.sys.sys_type; (*c) && (*c != '.'); c++);

        *c = '\0';
    }
    else if (from == INIT_TYPE_FROM_PROPERTY)
    {
        strncpy(dev.sys.sys_type, dev.type, FGC_SYSDB_SYS_LEN);
        dev.sys.sys_type[FGC_SYSDB_SYS_LEN] = '\0';
    }
    else
    {
        // System type is unknown

        dev.sys.sys_idx = 0;

        // Device type: 'UKNWN'

        // TODO Replace this with a safe copying function (when it's available)
        MemCpyStrFar(dev.sys.sys_type, SysDbType(0));
        dev.sys.sys_type[FGC_SYSDB_SYS_LEN] = '\0';
    }
}



static void InitSystemStructures(struct fgc_dev_name const * const dev_name)
{
    PropSet(&tcm, &PROP_FGC_NAME,    dev_name->name,   strlen(dev_name->name));
    PropSet(&tcm, &PROP_DEVICE_TYPE, dev.sys.sys_type, strlen(dev.sys.sys_type));
}



// External functions definitions



void InitMCU(void)
{
    uint16_t      i;

    // ToDo: move this to LoadProgramIntoDsp()
    mst.dsp_is_standalone = (CPU_MODEL_P & CPU_MODEL_DSPSTAL_MASK8) != 0;

    mst.reset  = CPU_RESET_P & ~(CPU_RESET_CTRL_DSP_MASK16 |    // Clear DSP reset
                                 CPU_RESET_CTRL_DSPTSM_MASK16); // Clear DSP tristate


    // ToDo: move this to the fieldbus init
    Clr(mst.reset,CPU_RESET_CTRL_DIGITAL_MASK16|CPU_RESET_CTRL_NETWORK_MASK16);

    CPU_RESET_P &= ~CPU_RESET_CTRL_NETWORK_MASK16;                                      // Remove hardware reset of uFIP
    // ToDo: move this to LoadProgramIntoDsp()
    CPU_RESET_P |= CPU_RESET_CTRL_DSPTSM_MASK16;                                        // Set CPU reset reg


    FIP_RESET_P = 0;                                            // Reset uFIP (takes 1us)
    RGLEDS_P    = 0x0FFF;                                       // Turn on all Front Panel LEDs except PSU

    if(TST_CPU_MODEL(SRAM))                                     // If running from SRAM
    {
        uint16_t sm_bytes_to_erase = SM_B;

        // If watch debug control is active for this run

        if(SM_WATCH_CTRL_P == 0x04 || SM_WATCH_CTRL_P == 0x08)
        {
            // Preserve the WATCH zone

            sm_bytes_to_erase -= SM_WATCH_B;
        }

        MemSetBytes((void*)SM_16,0,sm_bytes_to_erase);                  // Clear complete SM area but preserve watch zone if in use
        RunlogInit();                                                   // Initialise runlog

        i = SLOT5_P;                                                    // Force word read

        SM_INTERFACETYPE_P = (i & SLOT5_TYPE_MASK16)    >> SLOT5_TYPE_SHIFT;
        SM_CODES_A[0]      = (i & SLOT5_PLDVERS_MASK16) >> SLOT5_PLDVERS_SHIFT;
    }

    i = FGC_MAINPROG_RUNNING;
    RunlogWrite(FGC_RL_UXTIME,   (void*)SM_UXTIME_16);  // Unix time
    RunlogWrite(FGC_RL_MSTIME,   (void*)SM_MSTIME_16);  // Millisecond time
    RunlogWrite(FGC_RL_MP_START, &version.unixtime);
    dpcom.mcu.time.now_ms.unix_time = SM_UXTIME_P;

    /*--- Initial read of FIP connector address ---*/
    // ToDo: move this to the fieldbus init

    SELFLASH(SEL_BB);                           // Set memory mode 9 to make BB_23 visible
    dev.namedb_crc   = CODES_NAMEDB_INFO_CRC_FP;// Read NameDB CRC from flash
    sta.config_state = FGC_CFG_STATE_UNSYNCED;  // Initialise config state to UNSYNCED

    /*--- Initialise the test property data ---*/

    for(i=0 ; i < DEV_TEST_DATA_SZ; i++)
    {
        dev.test_data[1+i*2] = i;               // Big endian !
    }
}



void InitOS(void)
{
   /* Create tasks */

    OSTskInit(10);                              // Initialise task control blocks

    OSTskCreate(MstTsk, NULL, tsk_stk.mst, STK_SIZE_MST);       // 0. MstTsk - Millisecond task
    OSTskCreate(FbsTsk, NULL, tsk_stk.fbs, STK_SIZE_FBS);       // 1. FbsTsk - Fieldbus task
    OSTskCreate(StaTsk, NULL, tsk_stk.sta, STK_SIZE_STA);       // 2. StaTsk - State machine task
    OSTskCreate(CmdTsk, &fcm, tsk_stk.fcm, STK_SIZE_FCM);       // 3. FcmTsk - Fieldbus commands task
    OSTskCreate(CmdTsk, &tcm, tsk_stk.tcm, STK_SIZE_TCM);       // 4. TcmTsk - Serial commands task
    OSTskCreate(DlsTsk, NULL, tsk_stk.dls, STK_SIZE_DLS);       // 5. DlsTsk - Dallas bus task
    OSTskCreate(RtdTsk, NULL, tsk_stk.rtd, STK_SIZE_RTD);       // 6. RtdTsk - Real-time display task
    OSTskCreate(TrmTsk, NULL, tsk_stk.trm, STK_SIZE_TRM);       // 7. TrmTsk - Serial communications task
    OSTskCreate(BgpTsk, NULL, tsk_stk.bgp, STK_SIZE_BGP);       // 8. BgpTsk - Background processing task

    /* Create Semaphores */

    OSSemInit(8);                               // Initialise semaphore control blocks

    fcm.sem              = OSSemCreate(0);                      // 0. FbsTsk -> FcmTsk (next cmd pkt waiting)
    tcm.sem              = OSSemCreate(0);                      // 1. TrmTsk -> TcmTsk (next cmd pkt waiting)
    pcm.sem              = OSSemCreate(1);                      // 2. Pub cmd struct reservation semaphore
    fbs.cmd.q_not_full   = OSSemCreate(0);                      // 3. Fieldbus cmd.q flow control semaphore
    fbs.pub.q_not_full   = OSSemCreate(0);                      // 4. Fieldbus pub.q flow control semaphore
    dev.set_lock         = OSSemCreate(1);                      // 5. Set command lock semaphore

    /* Create Message queues */

    OSMsgInit(1);                               // Initialise message queue control blocks

    terminal.msgq = OSMsgCreate(terminal.msgq_buf,TRM_MSGQ_SIZE);         // 0. IsrMst & FbsTsk -> TrmTsk (kbd chars)

    /* Create Memory partition */

    OSMemInit(1);                               // Initialise memory partition control blocks

    terminal.mbuf = OSMemCreate(terminal.mem_buf, TRM_N_BUFS,TRM_BUF_SIZE); // 0. Mem buffers for SCI output characters

    /* Initialise command structure links before OS starts */

    fcm.prop_buf = &dpcom.fcm_prop;             // Link fcm property buffer structure
    tcm.prop_buf = &dpcom.tcm_prop;             // Link tcm property buffer structure

    fcm.pars_buf = &dpcom.fcm_pars;             // Link fcm command pkt structure
    tcm.pars_buf = &dpcom.tcm_pars;             // Link tcm command pkt structure

    pcm.pfloat_buf = &dpcom.pcm_pfloat;         // Link pcm print float support structure
    fcm.pfloat_buf = &dpcom.fcm_pfloat;         // Link fcm print float support structure
    tcm.pfloat_buf = &dpcom.tcm_pfloat;         // Link tcm print float support structure
}



void EnableMcuTimersAndInterrupts(void)
{
    uint16_t      dummy;

    GPT_MCR_P   = 0x0005;               // IARB = 5
    GPT_ICR_P   = 0x0630;               // Timer interrupt is priority 6, base vector is 0x30
    GPT_PDR_P   = 0x00;                 // Port GP all outputs to zero
    GPT_PDDR_P  = 0xF0;                 // PGP0-3 input 4-7 output
    GPT_TCTL_P  = 0x0502;               // IC1 will capture on falling edge, OC2/3 will toggle
    dummy       = GPT_TFLG_P;           // Clear all ...
    GPT_TFLG_P  = 0;                    // ... outstanding GPT interrupts
    GPT_TOC2_P  = GPT_TCNT_P + 2000;    // Prepare for first tick in ~1 ms
    GPT_TOC3_P  = GPT_TOC2_P;           // Prepare for first tick in ~1 ms
    GPT_TMSK_P |= GPT_TMSK_OC2I_MASK16; // Enable GPT OC2 (ms tick) interrupts
    GPT_PACLT_P = 0x50;                 // Pulse Accumulator enabled for rising edges (single bit err)
    GPT_PACNT_P = 0x00;                 // Zero pulse accumulator register

    // The network interrupt is enabled later by FipInit() in MsTsk()
}



void EnableMcuSerialInterface_Terminal(void)
{
    QSM_SCCR1_P = 0x0000;               // SCI disabled to clear Tx/Rx buffers
    QSM_SCCR0_P = 0x0034;               // SCI Tx/Rx speed 0x34 @ 16MHz = 9600 Baud
    QSM_SCCR1_P = 0x000C;               // SCI Tx/Rx enabled, no parity, 1 start & 1 stop bit
}



void InitSystem(void)
{
    bool                type_in_db; // Flag for SysDB queries
    struct fgc_dev_name dev_name;   // Used for device name initialisation
    enum InitTypeFrom   init_from;  // Type initialisation method

    InitDbStructures();

    InitDeviceName(&dev_name);

    // Extract type from device name and check if it can be found in SysDB
    // If not then get type from DEVICE.TYPE property and check again
    // If still couldn't find the system then set the type and the name to unknown

    init_from = INIT_TYPE_FROM_NAME;
    do
    {
        InitType(init_from, &dev_name);
        type_in_db = InitFindTypeInSysDb();
    }
    while ( (!type_in_db) && (++init_from < INIT_TYPE_FROM_UNKNOWN_SYSTEM_RECORD) );

    if (!type_in_db)
    {
        InitType(init_from, &dev_name);
    }

    InitSystemStructures(&dev_name);
}



void InitPropertyTree(void)
{
    uint16_t        idx;
    struct prop * tlp = PROPS;                         // Pointer to array of top level properties (TLPs)

    struct init_dynflag_timestamp_select * property_init_info = &init_dynflag_timestamp_select[0];

    while(property_init_info->prop)                     // Initialise the timestamp dynamic flag
    {
        Set(property_init_info->prop->dynflags, DF_TIMESTAMP_SELECT);
        property_init_info++;
    }

    for(idx = 0; idx < FGC_N_TLPS ; idx++, tlp++)       // For all top level properties (TLP)
    {
        InitPropParentLinksScan(tlp);                           // Call recursive scan function
    }
}


#pragma MESSAGE DISABLE C1855 // Disable 'recursive function call' warning
uint32_t InitPropParentLinksScan(struct prop *p)
{
    uint16_t      idx;
    uint32_t      max_size = 0;

    if(p->parent && Test(p->parent->dynflags, DF_TIMESTAMP_SELECT))     // Propagate the DF_TIMESTAMP_SELECT flag to all children
    {
        Set(p->dynflags, DF_TIMESTAMP_SELECT);
    }

    if(p->type == PT_PARENT)                    // If PARENT property
    {
        if(p->value != &PROPS)                      // If property doesn't loop back to top level
        {
            for(idx=0 ; idx < p->max_elements ; idx++)  // For each child property
            {
                ((struct prop *)p->value)[idx].parent = p;                           // Link child to parent
                max_size += InitPropParentLinksScan((struct prop *)p->value + idx);  // Call recursively
            }
        }
    }
    else                                        // else CHILD property
    {
        if(p->get_func_idx)                         // If get function present
        {
            max_size = p->max_elements * prop_type_size[p->type];        // Calc max size in bytes
        }
    }

    if(Test(p->flags,PF_HIDE))                  // If property is HIDDEN
    {
        max_size = 0;                               // Report size 0 to parent
    }

    if(p->parent &&                             // If property has a parent, and
       Test(p->flags,PF_PPM) &&                 // property is PPM and
       max_size)                                // property is visible to a get of the parent
    {
        Set(p->parent->flags,PF_PPM);               // Set PPM flag for the parent
    }

    return(max_size);
}
#pragma MESSAGE DEFAULT C1855

// EOF
