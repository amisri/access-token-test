/*---------------------------------------------------------------------------------------------------------*\
  File:     log.c

  Purpose:  FGC MCU Software - Logging related functions

  Note:     LogInitEvtProp() makes links between bits and enums for a property with a symlist
        in the structure log_evt_prop.  This means that the symbol constant values for any
        property logged must NEVER be outside the range 0-15.  If this happens then the
        system will crash itself at start up with E=0xBAD0.  This will be visible in the run log.
\*---------------------------------------------------------------------------------------------------------*/

#define LOG_GLOBALS         // define log_thour, log_tday global variables
#define LOG_CLASS_GLOBALS   // define timing_log, log_evt_prop[], timing_log, log_iab, log_iearth, log_ileads, log_ireg, log_cycle global variables
#define CLASS_GLOBALS       // define masks[] global variable

#define DEFPROPS_INC_ALL    // defprops.h

#include <log_class.h>      // for log_evt_prop, log_iab global variables // Include this one first!!
#include <log.h>
#include <cmd.h>            // for tcm global variable, struct cmd
#include <stdio.h>          // for FILE
#include <class.h>          // for masks[] global variable, DEV_STA_TSK_PHASE
#include <fbs_class.h>      // for ST_UNLATCHED
#include <defprops.h>       // Include property related definitions
#include <fgc_consts_gen.h>     // for FGC_LOG_EVT_PROP_LEN, FGC_LOG_EVT_ACT_LEN
#include <property.h>       // for struct prop, ST_MAX_SYM_LEN
#include <definfo.h>        // for FGC_CLASS_ID
#include <dev.h>            // for dev global variable
#include <mcu_dependent.h>  // for ENTER_SR(), EXIT_SR(), MCU_FF1
#include <fbs.h>            // for fbs global variable, FbsOutLong()
#include <dpcom.h>          // for dpcom global variable
#include <macros.h>         // for Test(), Clr()
#include <sta.h>            // for sta global variable
#include <diag.h>           // for diag global variable
#include <codes_holder.h>   // for codes_holder_info global variable
#include <dls.h>            // for temp  global variable
#include <mem.h>            // for MemCpyWords(), MemCpyBytes(), MemSetWords()
#include <start.h>          // for Crash()
#include <fgc/fgc_db.h>     // for access to DIMDB
#include <log_event.h>      // for struct log_evt_rec
#include <string.h>         // for strncpy()
#include <memmap_mcu.h>     // for SM_UXTIME_P
#include <mcu_dsp_common.h>
#include <shared_memory.h>  // for shared_mem global variable
#include <ref_class.h>      // for NON_PPM_USER
#include <qspi_bus.h>       // for QSPI_BRANCHES



#if FGC_CLASS_ID != 59

#include <log_menu_status.h>

struct Log
{

    bool     fgc_logger_enabled;           //!< True if the FGC logger is enabled
};

static struct Log local;

#endif



static void logFgcLoggerInit(void);
static void logFgcLoggerRunLogs(void);


/*---------------------------------------------------------------------------------------------------------*/
void LogInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main() to set up the logs - this is before the OS has started.
\*---------------------------------------------------------------------------------------------------------*/
{
    log_evt.dim_log_rec.prop[0]  = 'D';             // Prepare DIM log entry header
    log_evt.dim_log_rec.prop[1]  = 'I';
    log_evt.dim_log_rec.prop[2]  = 'M';
    log_evt.dim_log_rec.prop[3]  = '.';

    log_evt.sample_size = sizeof(struct log_evt_rec);

    // Restore temperature log times and indexes from before reset
    log_thour.last_sample_idx = SM_THOURIDX_P;
    log_tday.last_sample_idx  = SM_TDAYIDX_P;

    log_thour.last_sample_time.unix_time = SM_THOURTIME_P;
    log_tday.last_sample_time.unix_time  = SM_TDAYTIME_P;

    LogEvtCheck();                      // Check event log and clear if corrupted

    LogInitEvtProp();                       // Init event log table

#if (FGC_CLASS_ID == 51)
    LogStart(&log_iab);                     // Clear and start IAB log
    LogStart(&log_iearth);                  // Clear and start IEARTH log
    LogStart(&log_ileads);                  // Clear and start ILEADS log
    LogStart(&log_ireg);                    // Clear and restart IREG log
#endif
#if (FGC_CLASS_ID == 53)
    LogStart(&log_cycle);                   // Clear and restart CYCLE log
#endif
#if FGC_CLASS_ID == 59
    LogStart(&log_cval);                    // Clear and start Cval log
    LogStart(&log_ival);                    // Clear and start Ival log
    LogStart(&log_wval);                    // Clear and start Wval log
#endif
    dev.log_pm_state = FGC_LOG_RUNNING;                 // Set PM log state to RUNNING

    logFgcLoggerInit();
}
/*---------------------------------------------------------------------------------------------------------*/
void LogGetPmSigBuf(struct log_ana_vars * log, struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This is a helper function for GetLogPmBuf(), which is class specific.  It will export an operational log
  in binary format followed by its timestamp to the FIP command stream.  The log will always be frozen
  before this is called.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      n;
    uint16_t      s;
    uint16_t      sample_size;
    uint16_t      idx;
    uint8_t *     bin_data;
    uint32_t      offset;
    uint32_t      addr;
    uint8_t       values[64];     // Buffer must be size of largest log sample

    n   = log->n_samples;           // Get length of buffer in samples
    idx = log->last_sample_idx;         // Get most recent entry index
    sample_size = log->sample_size;     // Get local copy of sample size in bytes

    while(n--)                  // Write each sample in binary to FIP stream
    {
        idx    = (idx + 1) % log->n_samples;        // Pre-increment sample index
        offset = (uint32_t)sample_size * (uint32_t)idx; // Use intermediate offset to avoid compiler bug
        addr   = log->baseaddr + offset;

        ENTER_SR();                     // Select RAM in pages 23
        MemCpyWords((uint32_t)&values, addr, sample_size/2);  // Extract one sample
        EXIT_SR();                      // Deselect RAM in pages 23

        s = sample_size;
        bin_data = (uint8_t *) &values;

        while(s--)
        {
            c->store_cb(*(bin_data++));
        }
    }

    FbsOutLong( (char *) &log->last_sample_time.unix_time, c);      // as unix time and
    FbsOutLong( (char *) &log->last_sample_time.us_time, c);        // microseconds
}
/*---------------------------------------------------------------------------------------------------------*/
void LogGetPmEvtBuf(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This is a helper function for GetLogPmBuf(), which is class specific.  It will export the event log
  in binary format.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              n;
    uint16_t              n_bytes;
    uint16_t              idx;
    int8_t *             bin_data;
    struct log_evt_rec  evt_rec;

    n = FGC_LOG_EVT_LEN;            // Get number of data elements in array
    idx = log_evt.index;            // Get most recent entry index

    while(n--)
    {
        idx++;
        if ( idx >= FGC_LOG_EVT_LEN )       // Export oldest first
        {
            idx = 0;
        }

        OS_ENTER_CRITICAL();                            // This calculation of the sample address is
                                // tweaked with (uint32_t) on the sample size to
        MemCpyWords( (uint32_t) &evt_rec,         // satisfy the HC16 compiler - don't change it!
                     (uint32_t) (LOG_EVT_32 + (uint32_t) log_evt.sample_size * idx),
                 sizeof(struct log_evt_rec) / 2 );

        OS_EXIT_CRITICAL();

        n_bytes  = log_evt.sample_size;
        bin_data = (int8_t*)&evt_rec;

        while(n_bytes--)
        {
            c->store_cb( *bin_data );
            bin_data++;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks to see if the contents of the event log are corrupted.  If so, it clears the log.
  To pass the check, the saved event log index must be in range (0-699) and every record's unix time stamp
  must be zero, or within one month of the restart time.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                      i;
    uint32_t                      unix_time;
    uint32_t                      low_limit;
    uint32_t                      high_limit;
    struct log_evt_rec * FAR    evt_rec_fp;

    log_evt.index = SM_LOGEVTIDX_P;             // Restore event log index from before reset

    if(log_evt.index < FGC_LOG_EVT_LEN)
    {
        evt_rec_fp = (struct log_evt_rec * FAR) LOG_EVT_32;
        high_limit = SM_UXTIME_P;
        low_limit  = high_limit - 2500000;          // Last month only

        for( i = 0; i < FGC_LOG_EVT_LEN; ++i, ++evt_rec_fp )
        {
            unix_time = evt_rec_fp->timestamp.unix_time;

            if ( unix_time && ( (unix_time < low_limit) || (unix_time > high_limit) ) )
            {
                log_evt.index = FGC_LOG_EVT_LEN;        // Flag event log as corrupted
                break;
            }
        }
    }

    if ( log_evt.index >= FGC_LOG_EVT_LEN )     // If log was found to be corrupted
    {
        MemSetWords(LOG_EVT_32, 0, LOG_EVT_W);              // Clear Event log
        log_evt.index = SM_LOGEVTIDX_P = 0;                 // Reset log index
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogInitEvtProp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from LogInit() to initialise the event log table that is partially defined at
  the start of the logXX.c file where XX is the class ID.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                  bit_idx;
    struct sym_lst *        sym_lst;
    struct log_evt_prop *   lp;         // Pointer to entry in event log property table

    for ( lp = (struct log_evt_prop *) &log_evt_prop; lp->prop ; lp++ ) // For each entry in the event log property table
    {
        if(lp->prop->flags & PF_SYM_LST)        // If property has a symbol list
        {
            sym_lst = (struct sym_lst *)lp->prop->range;    // Get start of symlist

            if(!(lp->prop->flags & PF_BIT_MASK))        // If property is not a bit mask symlist
            {
                while(sym_lst->sym_idx)                 // For each symbol in the list
                {
                    if(sym_lst->value > FGC_MAX_SYMLIST_VALUE)                 // If const data out of range
                    {
                    Crash(0xBAD0);                          // Signal the failure
                    }

                    lp->symbols[sym_lst->value] =
                            SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c; // Link to symbol
                    sym_lst++;
                }
            }
            else                    // else property is a bit mask symlist
            {
                for(bit_idx = 0; bit_idx < 16; bit_idx++)       // For each bit 0 to 15
                {
                    lp->symbols[bit_idx] = bitmask_default_sym[bit_idx];// Set a default symbol name
                }
                while(sym_lst->sym_idx)             // For each symbol in list
                {
                    MCU_FF1(sym_lst->value, bit_idx);                   // Find first bit that is set. Answer stored in bit_idx.

                    lp->symbols[bit_idx] = SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c; // Link to symbol

                    sym_lst++;
                }
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogStartAll(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to restart the all data logging.  It will clear the FGC_UNL_POST_MORTEM bit.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 51)
    LogStart(&log_iab);                 // Clear and restart IAB log
    LogStart(&log_iearth);              // Clear and restart IEARTH log
    LogStart(&log_ileads);              // Clear and restart ILEADS log
    LogStart(&log_ireg);                // Clear and restart IREG log
#endif

#if (FGC_CLASS_ID == 53)
    // Cannot call LogStart because it clears the buffer
                                                        // and this takes too long when run from StaTsk()
    log_cycle.samples_to_acq = log_cycle.post_trig;     // Set number of post trig samples
    log_cycle.run_f = true;             // Start logging
#endif

#if FGC_CLASS_ID == 59
    LogStart(&log_cval);                // Clear and restart Cval log
    LogStart(&log_ival);                // Clear and restart Ival log
    LogStart(&log_wval);                // Clear and restart Wval log
#endif

    LogTStart(&log_thour);              // Restart THOUR log
    Clr(ST_UNLATCHED,FGC_UNL_POST_MORTEM);      // Clear post mortem flag
    Clr(fbs.u.fieldbus_stat.ack,FGC_SELF_PM_REQ);   // Reset self-trig PM dump request

    logFgcLoggerReset();
    logFgcLoggerRunLogs();

    dev.log_pm_state = FGC_LOG_RUNNING;         // Set PM log state to RUNNING
}
/*---------------------------------------------------------------------------------------------------------*/
void LogStart(struct log_ana_vars *log_ana)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to restart logging for the specified buffer
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!log_ana->run_f)                 // If not already running
    {
        ENTER_SR();                     // Select RAM in pages 23
        MemSetWords(log_ana->baseaddr, 0, log_ana->bufsize_w);  // Clear log buffer
        EXIT_SR();                      // Deselect RAM in pages 23
        log_ana->samples_to_acq = log_ana->post_trig;       // Set number of post trig samples
        log_ana->run_f = true;                  // Start logging
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogTStart(struct log_ana_vars *log_ana)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to restart the temperature logging for the specified buffer.  This attempts to
  preserve as much of the existing data as possible.  To do this it uses the time of the last sample saved
  in the SM area.  It will clear to zero every entry that was missed.  The temperature buffers are in
  page 0 so the mode register doesn't need to be manipulated.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  duration_ms;                // Log duration in milliseconds
    uint32_t  t_missing_ms;               // Missed time in milliseconds
    uint16_t  n_missing;              // Missed samples
    uint16_t  offset;
    uint16_t  sample_size_w;              // Sample size in words

    if( !log_ana->run_f )               // If not already running
    {
        t_missing_ms = (SM_UXTIME_P - log_ana->last_sample_time.unix_time) * 1000;
        duration_ms  = log_ana->period_ms * log_ana->n_samples;

        if(t_missing_ms > duration_ms)              // If no valid data is left in the buffer
        {
            MemSetWords(log_ana->baseaddr, 0, log_ana->bufsize_w);  // Clear log buffer
            log_ana->last_sample_idx = 0;               // Reset index
        }
        else
        {
            n_missing     = t_missing_ms / log_ana->period_ms;
            sample_size_w = log_ana->sample_size/2;

            while(n_missing--)
            {
                log_ana->last_sample_idx = (log_ana->last_sample_idx + 1) % log_ana->n_samples;
                offset = log_ana->last_sample_idx * log_ana->sample_size;

                MemSetWords(log_ana->baseaddr + offset, 0, sample_size_w);
            }
        }
        log_ana->samples_to_acq = log_ana->post_trig;       // Set number of post trig samples
        log_ana->run_f = true;                  // Start logging
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtSaveValue(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function stores set command value characters in the event log record for the command.  It can only
  go as FAR as the end of the current packet.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              out_idx;
    uint16_t              n_chars;
    char *              buf;
    struct pars_buf *   pars_buf;


    pars_buf = c->pars_buf;
    out_idx  = pars_buf->out_idx;
    n_chars  = pars_buf->pkt[c->pkt_buf_idx].n_chars;
    buf      = &pars_buf->pkt[c->pkt_buf_idx].buf[out_idx];

    while(c->evt_log_state != EVT_LOG_VALUE_FULL && out_idx++ < n_chars)
    {
        LogEvtSaveCh(c,*(buf++));
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtSaveCh(struct cmd *c, char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function stores characters from the command buffer in the event log record for the command.
  Characters are either a property name or a property value.  The function uses a state machine to know
  which is currently being accummulated in the event record for the command.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct log_evt_rec *evt_rec  = &c->evt_rec;

    switch(c->evt_log_state)
    {
        case EVT_LOG_RESET_PROP:

            c->evt_log_idx    = 0;
            c->evt_log_state  = EVT_LOG_PROP;
            evt_rec->value[0] = '\0';

            // Fall through !!!!
            // no break here !!!!
        case EVT_LOG_PROP:

            if(ch && ch != ' ')
            {
                evt_rec->prop[  c->evt_log_idx] = ch;
                evt_rec->prop[++c->evt_log_idx] = '\0';

                if(c->evt_log_idx >= (FGC_LOG_EVT_PROP_LEN-1))
                {
                    c->evt_log_state = EVT_LOG_PROP_FULL;
                }
            }
            break;

        case EVT_LOG_RESET_VALUE:

            c->evt_log_idx   = 0;
            c->evt_log_state = EVT_LOG_VALUE;

            // Fall through !!!!
            // no break here !!!!

        case EVT_LOG_VALUE:

            if(!ch || ch == ';')
            {
                c->evt_log_state = EVT_LOG_VALUE_FULL;
            }
            else if(ch != ' ')
            {
                evt_rec->value[c->evt_log_idx++] = ch;
                evt_rec->value[c->evt_log_idx]   = '\0';

                if(c->evt_log_idx >= (FGC_LOG_EVT_VAL_LEN - 1))
                {
                    c->evt_log_state = EVT_LOG_VALUE_FULL;
                }
            }
            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtCmd(struct cmd *c, uint16_t errnum)
/*---------------------------------------------------------------------------------------------------------*\
  This function completes the event record for a set command by writing the action and copying the
  record into the event buffer atomically by suspending interrupts.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct log_evt_rec *        evt_rec  = &c->evt_rec;

    // Prepare command error number in the action field

    sprintf(evt_rec->action,(c == &tcm ? "TRM.%03u" : "NET.%03u"),errnum);

    // Write event record to event log atomically

    OS_ENTER_CRITICAL();

    log_evt.index++;
    if ( log_evt.index >= FGC_LOG_EVT_LEN )
    {
        log_evt.index = 0;
    }

    OS_EXIT_CRITICAL();             // This calculation of the sample address is
                        // tweaked with (uint32_t) on the sample size to
                        // satisfy the HC16 compiler - don't change it!

    MemCpyWords((uint32_t)(LOG_EVT_32 + (uint32_t)log_evt.sample_size * log_evt.index),
    (uint32_t)evt_rec,
    sizeof(struct log_evt_rec)/2);

    SM_LOGEVTIDX_P = log_evt.index;
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtProp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the property value for changes and records entries in the log if they are detected.
  For bit mask properties, every bit change uses one entry in the log. For numeric values, the value will
  always be written in hex to log for speed reasons.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              new_value;          // New property value
    uint16_t              old_value;          // Old property value
    uint16_t              chg_mask;           // Change mask
    uint16_t              bit_idx = 0;
    uint16_t              set_f   = 0;
    struct prop *           p;              // Pointer to property
    struct log_evt_prop *       lp;             // Pointer to entry in event log property table

    static char         hexdigits[] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F' };
    static char         hexvalue[ST_MAX_SYM_LEN+1] = { '0','x' };

    for(lp = log_evt_prop; lp->prop != NULL; lp++)
    {
        p = lp->prop;

        /*--- Check if property value has changed ---*/

        switch(p->type)
        {
            case PT_INT8U:
                new_value = *((uint8_t*)p->value);
                break;
            case PT_INT16U:
                new_value = *((uint16_t*)p->value);
                break;
            case PT_INT32U:
                new_value = *((uint32_t*)p->value);   // Take low word only
                break;
        }

        old_value = lp->old_value;

        if ( new_value == old_value )   // if value has not changed
        {
            continue;                       // continue to next property
        }

        /*--- Prepare log entry ---*/

        lp->old_value = new_value;          // Remember new property value

        if(p->flags & PF_SYM_LST)       // If property has a symbol list
        {
            if(p->flags & PF_BIT_MASK)      // If property is a bit mask
            {
                chg_mask = new_value ^ old_value;   // Calculate change mask

                while(chg_mask)             // While bits remain in the change mask
                {
                    asm
                    {
                        LDD chg_mask
                        PSHM    CCR             // OS_ENTER_CRITICAL();
                        ORP #0x00E0                         //
                        STD CPU_LSSB_16         //   CPU_LSSB_P = chg_mask;
                        LDE CPU_LSSB_16         //   bit_idx = CPU_LSSB_P;
                        PULM    CCR                             // OS_EXIT_CRITICAL();
                        STE bit_idx

                        ASLE                    // set_f = masks[bit_idx] & new_value;
                        LDX @masks
                        LDD new_value
                        ANDD    E,X
                        STD set_f

                        LDD E,X             // chg_mask &= ~masks[bit_idx];
                        COMD
                        ANDD    chg_mask
                        STD chg_mask
                    }

                    LogEvtPropStore(lp->name, lp->symbols[bit_idx], set_f, true);
                }
            }
            else                // else property is NOT a bit mask
            {
                LogEvtPropStore(lp->name, lp->symbols[new_value], true, false);
            }
        }
        else                // else numeric property
        {
            asm                 // Convert 16-bit integer value into HEX ASCII string
            {
                LDX @hexdigits          // Fourth digit
                LDE #0x000F
                ANDE    new_value
                LDAA    E,X
                STAA    hexvalue:5

                LDE #0x00F0             // Third digit
                ANDE    new_value
                LSRE
                LSRE
                LSRE
                LSRE
                LDAA    E,X
                STAA    hexvalue:4

                LDD new_value           // Second digit
                XGAB
                TDE
                ANDE    #0x000F
                LDAA    E,X
                STAA    hexvalue:3

                TDE                 // First digit
                ANDE    #0x00F0
                LSRE
                LSRE
                LSRE
                LSRE
                LDAA    E,X
                STAA    hexvalue:2
            }

            LogEvtPropStore(lp->name, (char * FAR) &hexvalue, true, false); // Log new hex value
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtPropStore(const char * FAR far_prop_name, const char * FAR far_sym, uint16_t set_f, uint16_t bitmask_f)
/*---------------------------------------------------------------------------------------------------------*\
  This function writes a property log entry in the event log.  It is called from LogEvtProp() which takes
  responsibility for the ENTER/EXIT SRAM so this function doesn't need to touch the MODEL register.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  rec_addr;

    log_evt.index++;
    if ( log_evt.index >= FGC_LOG_EVT_LEN )     // No interrupt protection required as
    {                           // called from StaTsk() which is highest priority
        log_evt.index = 0;
    }

    // This calculation of the sample address is
    // tweaked with (uint32_t) on the sample size to
    // satisfy the HC16 compiler - don't change it!

    rec_addr = (uint32_t)(LOG_EVT_32 + (uint32_t)log_evt.sample_size * log_evt.index);

    asm
    {
        PSHM    K           // Save page registers
        LDD rec_addr:0      // B  = page for event log buffer
        TBYK                // YK = B
        LDY rec_addr:2      // Y  = address in page

        //  Write timestamp: Unix time, us_time

        LDED    sta.timestamp_us.unix_time
        STE 0,Y
        STD 2,Y
        LDED    sta.timestamp_us.us_time
        STE 4,Y
        STD 6,Y

        // Write property name (24 bytes fixed length)

        LDAB    far_prop_name:0
        LDX far_prop_name:1
        TBXK

        LDD  0,X
        STD  8,Y
        LDD  2,X
        STD 10,Y
        LDD  4,X
        STD 12,Y
        LDD  6,X
        STD 14,Y
        LDD  8,X
        STD 16,Y
        LDD 10,X
        STD 18,Y
        LDD 12,X
        STD 20,Y
        LDD 14,X
        STD 22,Y
        LDD 16,X
        STD 24,Y
        LDD 18,X
        STD 26,Y
        LDD 20,X
        STD 28,Y
        LDD 22,X
        STD 30,Y

        // Write symbol (14 bytes fixed length)

        LDAB    far_sym:0
        LDX far_sym:1
        TBXK

        LDD  0,X
        STD 32,Y
        LDD  2,X
        STD 34,Y
        LDD  4,X
        STD 36,Y
        LDD  6,X
        STD 38,Y
        LDD  8,X
        STD 40,Y
        LDD 10,X
        STD 42,Y
        LDD 12,X
        STD 44,Y

        // Write action (SET or CLR)

        TSTW    set_f           // Test set flag
        BNE setaction

    clraction:
        LDD #0x434C         // D = "CL"
        LDE #0x525F         // E = "R_"
        BRA set

    setaction:
        LDD #0x5345         // D = "SE"
        LDE #0x545F         // E = "T_"

    set:
        STD 68,Y            // Write to action field
        STE 70,Y

        // Write action (_BIT or nul)

        TSTW    bitmask_f
        BEQ     notbitmask

    bitmask:
        LDD     #0x4249                 // D = "BI"
        LDE     #0x5400                 // E = "T\0"
        STD 72,Y            // Write to action field
        STE 74,Y
        BRA     end

    notbitmask:
        CLR     71,Y                    // Nul terminate "SET" or "CLR"

    end:
        PULM    K           // Recover page registers
    }

    SM_LOGEVTIDX_P = log_evt.index;
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtDim(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at 200Hz from StaTsk() to log the triggered DIM digital inputs.
\*---------------------------------------------------------------------------------------------------------*/
{
    fgc_db_t                           dim_type_idx;
    fgc_db_t                          dim_props_idx;          // 0 - (n_dims-1)
    fgc_db_t                          board_number;
    bool                            input_zero_f;
    bool                            input_zero_old_f;
    bool                            fault_input_f;
    bool                            end_f;      // Log entry made so end this iteration flag

    dim_props_idx = log_evt.dim_props_idx;

    do
    {
        if(log_evt.dim_input_mask)      // If logging a DIM
        {
            do
            {
                dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,log_evt.logical_dim);
                if (DimDbIsDigitalInput(dim_type_idx, (log_evt.dim_bank_idx+4), log_evt.dim_input_idx)) // If channel is in use
                {

                    // fault_input_f:    Is the data bit indicating a fault?
                    // input_zero_f:     Is the new data bit zero?
                            // input_zero_old_f: Is the old data bit zero?

                    fault_input_f     =   log_evt.dim_input_mask & log_evt.fault_mask  [log_evt.dim_bank_idx];
                    input_zero_f      = !(log_evt.dim_input_mask & log_evt.dim_new_data[log_evt.dim_bank_idx]);
                    input_zero_old_f  = !(log_evt.dim_input_mask & log_evt.dim_old_data[log_evt.dim_bank_idx][log_evt.logical_dim]);

                    // In TRIG  state: Log all non zero values, and faults, if they are set
                    // In WATCH state: Log any fault that has just appeared

                    if((log_evt.dim_state == DIM_EVT_TRIG  && (!fault_input_f || !input_zero_f)) ||
                       (log_evt.dim_state == DIM_EVT_WATCH && fault_input_f && !input_zero_f && input_zero_old_f))
                    {
                        LogEvtDimInput(input_zero_f, fault_input_f, true);
                        end_f = true;
                    }
                }

                log_evt.dim_input_mask <<= 1;

                log_evt.dim_input_idx++;
                if ( log_evt.dim_input_idx >= FGC_N_DIM_DIG_INPUTS )
                {
                    log_evt.dim_bank_idx++;
                    if ( log_evt.dim_bank_idx < FGC_N_DIM_DIG_BANKS )
                    {
                        log_evt.dim_input_idx  = 0;
                        log_evt.dim_input_mask = 1;
                    }
                    else
                    {
                        log_evt.dim_input_mask = 0;

                        if ( log_evt.dim_state == DIM_EVT_TRIG )
                        {
                            dpcom.dsp.diag.dim_evt_log_state[log_evt.logical_dim] = DIM_EVT_TRIGGED;
                        }
                        else
                        {
                            log_evt.dim_old_data[0][log_evt.logical_dim] = log_evt.dim_new_data[0];
                            log_evt.dim_old_data[1][log_evt.logical_dim] = log_evt.dim_new_data[1];
                        }
                    }
                }

                if(end_f)
                {
                    log_evt.dim_props_idx = dim_props_idx;
                    return;
                }
            }
            while(log_evt.dim_input_mask);
        }

        // Check for new DIMs to log only at the start of each 20ms period

        if(((uint16_t)sta.timestamp_ms.ms_time % 20) != DEV_STA_TSK_PHASE)
        {
            return;
        }

        // Analyse next DIM

        dim_props_idx++;
        if ( dim_props_idx >= diag.n_dims )
        {
            dim_props_idx = 0;
        }

        log_evt.logical_dim = ( (struct TAccessToDimInfo *) diag.dim_props[dim_props_idx].value)->logical_dim;

        board_number = SysDbDimBusAddress(dev.sys.sys_idx, log_evt.logical_dim);

        log_evt.dim_state = dpcom.dsp.diag.dim_evt_log_state[log_evt.logical_dim];
        if ( log_evt.dim_state == DIM_EVT_TRIGGED )     // Following a TRIG state and until dim_state is changed by the DSP
        {
            continue;
        }

        if ( log_evt.dim_state == DIM_EVT_NONE )
        {
            if ( board_number < FGC_MAX_DIM_BUS_ADDR )
            {
                diag.data[DIAG_LT_DIG_DATA_0 + board_number] = 0;
                diag.data[DIAG_LT_DIG_DATA_1 + board_number] = 0;
            }
            continue;
        }

        dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,log_evt.logical_dim);
        log_evt.fault_mask   [0] = DimDbDigitalFaults(dim_type_idx,4);
        log_evt.fault_mask   [1] = DimDbDigitalFaults(dim_type_idx,5);

        log_evt.dim_new_data [0] = diag.data[DIAG_UL_DIG_DATA_0 + board_number];
        log_evt.dim_new_data [1] = diag.data[DIAG_UL_DIG_DATA_1 + board_number];

        if(log_evt.dim_state == DIM_EVT_WATCH &&        // if DIM_EVT_WATCH state
           !((log_evt.dim_new_data[0] ^ log_evt.dim_old_data[0][log_evt.logical_dim]) & log_evt.fault_mask[0]) &&
           !((log_evt.dim_new_data[1] ^ log_evt.dim_old_data[1][log_evt.logical_dim]) & log_evt.fault_mask[1]))
        {
            continue;                           // Nothing changed so continue
        }

        log_evt.dim_input_mask = 1;     // Set mask to process DIM
        log_evt.dim_input_idx  = 0;     // Start with first input of bank 0
        log_evt.dim_bank_idx   = 0;

        MemCpyWords((uint32_t)&log_evt.dim_log_rec.prop[4],
                (uint32_t)SYM_TAB_PROP[diag.dim_props[dim_props_idx].sym_idx].key.c,
                (ST_MAX_SYM_LEN+1)/2);

        if(log_evt.dim_state == DIM_EVT_TRIG)   // if DIM_EVT_TRIG state
        {

            diag.data[DIAG_LT_DIG_DATA_0 + board_number] = log_evt.dim_old_data[0][log_evt.logical_dim] = log_evt.dim_new_data[0];
            diag.data[DIAG_LT_DIG_DATA_1 + board_number] = log_evt.dim_old_data[1][log_evt.logical_dim] = log_evt.dim_new_data[1];

            log_evt.dim_log_rec.timestamp = dpcom.dsp.diag.dim_trig_time[log_evt.logical_dim];
            LogEvtDimInput(0, 0, false);
        }
        else                        // else DIM_EVT_WATCH state
        {
            log_evt.dim_log_rec.timestamp = sta.timestamp_us;
        }
    }
    while(log_evt.dim_input_mask || dim_props_idx != log_evt.dim_props_idx);    // until all DIMS checked
}
/*---------------------------------------------------------------------------------------------------------*/
void LogEvtDimInput(bool input_zero_f, bool fault_input_f, bool dimdb_lbl_is_null)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called LogEvtDim() to record the state of a DIM digital input in the event log.
\*---------------------------------------------------------------------------------------------------------*/
{
    const fgc_db_t dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,log_evt.logical_dim);
    if (!dimdb_lbl_is_null)              // If pointer to label is NULL
    {
        log_evt.dim_log_rec.value[0] = 'T';     // Report DIM trigger time always
        log_evt.dim_log_rec.value[1] = 'R';
        log_evt.dim_log_rec.value[2] = 'I';
        log_evt.dim_log_rec.value[3] = 'G';
        log_evt.dim_log_rec.value[4] = 'G';
        log_evt.dim_log_rec.value[5] = 'E';
        log_evt.dim_log_rec.value[6] = 'R';
        log_evt.dim_log_rec.value[7] = '\0';

        log_evt.dim_log_rec.action[0] = 'S';
        log_evt.dim_log_rec.action[1] = 'E';
        log_evt.dim_log_rec.action[2] = 'T';
        log_evt.dim_log_rec.action[3] = '\0';
    }
    else                    // else use label
    {
        if(fault_input_f)
        {
            log_evt.dim_log_rec.value[0] = 'T';
            log_evt.dim_log_rec.value[1] = 'R';
            log_evt.dim_log_rec.value[2] = 'G';
            log_evt.dim_log_rec.value[3] = '|';
        }
        else
        {
            log_evt.dim_log_rec.value[0] = 'S';
            log_evt.dim_log_rec.value[1] = 'T';
            log_evt.dim_log_rec.value[2] = 'A';
            log_evt.dim_log_rec.value[3] = '|';
        }

        MemCpyWords((uint32_t)&log_evt.dim_log_rec.value[4],
                (uint32_t)DimDbDigitalLabel(dim_type_idx, (log_evt.dim_bank_idx+4), log_evt.dim_input_idx),
                (FGC_LOG_EVT_VAL_LEN-4)/2);

        MemCpyWords((uint32_t)&log_evt.dim_log_rec.action,
                (input_zero_f ?
                    (uint32_t)DimDbDigitalLabelZero(dim_type_idx, (log_evt.dim_bank_idx+4), log_evt.dim_input_idx) :
                    (uint32_t)DimDbDigitalLabelOne(dim_type_idx, (log_evt.dim_bank_idx+4), log_evt.dim_input_idx)) ,
                FGC_LOG_EVT_ACT_LEN/2);
    }

    log_evt.index++;
    if ( log_evt.index >= FGC_LOG_EVT_LEN )     // No interrupt protection required as
    {                           // called from StaTsk() which is highest priority
        log_evt.index = 0;
    }

    // This calculation of the sample address is tweaked with (uint32_t) on the sample size to
    // satisfy the HC16 compiler - don't change it without checking the assembler output in the list file!

    MemCpyWords((uint32_t)(LOG_EVT_32 + (uint32_t)log_evt.sample_size * log_evt.index),
    (uint32_t)&log_evt.dim_log_rec,
    sizeof(struct log_evt_rec) / 2);

    SM_LOGEVTIDX_P = log_evt.index;
}
/*---------------------------------------------------------------------------------------------------------*/
void * FAR LogNextAnaSample(struct log_ana_vars * log_ana)
/*---------------------------------------------------------------------------------------------------------*\
  This function will advance the sample index for the analogue log and return the address of the next
  record to be written.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(++log_ana->last_sample_idx >= log_ana->n_samples)
    {
        log_ana->last_sample_idx = 0;
    }

    return((void* FAR)(log_ana->baseaddr + (uint32_t)log_ana->last_sample_idx * log_ana->sample_size));
}
/*---------------------------------------------------------------------------------------------------------*/
void LogThour(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will log the FGC and DCCT temperatures in the THOUR log. It is called from MstTsk() and
  records one sample per 10 s.  Note that the THOUR log is in page 1 so ENTER/EXIT_SR is NOT required.
\*---------------------------------------------------------------------------------------------------------*/
{
    LogNextAnaSample(&log_thour);       // Return next record address in A:B:Y

    asm
    {
        PSHM    K           // Save page registers
        TBYK                // YK = B (Contains page address of record)
        LDX @temp.log       // X = start of temperature log variables
        LDD 0,X
        STD 0,Y         // Save FGC_IN temp
        LDD 2,X
        STD 2,Y         // Save FGC_OUT temp
        LDD 4,X
        STD 4,Y         // Save DCCT_A temp
        LDD 6,X
        STD 6,Y         // Save DCCT_B temp
        PULM    K           // Restore page registers
    }

    SM_THOURIDX_P  = log_thour.last_sample_idx;                 // Save new index in SM area
    SM_THOURTIME_P = log_thour.last_sample_time.unix_time = temp.unix_time; // Save time in SM area
}
/*---------------------------------------------------------------------------------------------------------*/
void LogTday(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will log the FGC and DCCT temperatures in the TDAY log.  It is called from MstTsk() and
  records one sample per 10 min.  Note that the TDAY log is in page 1 so ENTER/EXIT_SR is NOT required.
\*---------------------------------------------------------------------------------------------------------*/
{
    LogNextAnaSample(&log_tday);        // Return next record address in A:B:Y

    asm
    {
        PSHM    K           // Save page registers
        TBYK                // YK = B
        LDX @temp.log       // X = start of temperature log variables
        LDD 0,X
        STD 0,Y         // Save FGC_IN temp
        LDD 2,X
        STD 2,Y         // Save FGC_OUT temp
        LDD 4,X
        STD 4,Y         // Save DCCT_A temp
        LDD 6,X
        STD 6,Y         // Save DCCT_B temp
        PULM    K           // Restore page registers
    }

    SM_TDAYIDX_P  = log_tday.last_sample_idx;                   // Save new index in SM area
    SM_TDAYTIME_P = log_tday.last_sample_time.unix_time = temp.unix_time;   // Save time in SM area
}


#if FGC_CLASS_ID != 59

static void logFgcLoggerInit(void)
{
    uint8_t idx;

    for (idx = 0; idx < LOG_NUM_MENUS; idx++)
    {
        if (Test(LOG_MENU_PM_MASK, 1<<idx))
        {
            log_menus.status_bit_mask[idx] |=   LOG_MENU_STATUS_POSTMORTEM_BIT_MASK
                                              | LOG_MENU_STATUS_FREEZABLE_BIT_MASK;
        }
    }

    for (idx = LOG_MENU_DIM; idx < LOG_NUM_MENUS; idx++)
    {
        log_menus.status_bit_mask[idx] |= LOG_MENU_STATUS_DIM_BIT_MASK;
    }

    local.fgc_logger_enabled = false;
}



static void logFgcLoggerRunLogs(void)
{
    uint8_t idx;

    for (idx = 0; idx < LOG_NUM_MENUS; idx++)
    {
        log_menus.status_bit_mask[idx] |= LOG_MENU_STATUS_RUNNING_BIT_MASK;
    }
}



void logFgcLoggerReset(void)
{
    uint8_t idx;

    for (idx = 0; idx < LOG_NUM_MENUS; idx++)
    {
        if (Test(LOG_MENU_PM_MASK, 1<<idx))
        {
            log_menus.status_bit_mask[idx] &= ~LOG_MENU_STATUS_SAVE_BIT_MASK;
        }
    }

    log_menus.post_mortem_time.unix_time = 0;
    log_menus.post_mortem_time.us_time   = 0;

    Clr(ST_UNLATCHED, FGC_UNL_LOG_PLEASE);
}



void logFgcLoggerSetEnable(bool const enabled)
{
    local.fgc_logger_enabled = enabled;
}



void logFgcLoggerSetPostMortem(void)
{
    if (   dev.pm_enabled_f                        == true
        && Test(ST_UNLATCHED, FGC_UNL_POST_MORTEM) == 0)
    {
        Set(ST_UNLATCHED,FGC_UNL_POST_MORTEM);
        Set(fbs.u.fieldbus_stat.ack,FGC_SELF_PM_REQ);
    }

    if (local.fgc_logger_enabled == true)
    {
        uint8_t idx;

        for (idx = 0; idx < LOG_NUM_MENUS; idx++)
        {
            if (Test(LOG_MENU_PM_MASK, 1<<idx))
            {
                log_menus.status_bit_mask[idx] |= LOG_MENU_STATUS_SAVE_BIT_MASK;
            }
        }

        log_menus.post_mortem_time = mst.time;
        Set(ST_UNLATCHED, FGC_UNL_LOG_PLEASE);
    }
}


void logFgcLoggerProcess(void)
{
    if (local.fgc_logger_enabled == false)
    {
        return;
    }

    // Once the FGC logger has zeroed LOG.MENU.POST_MORTEM, clear the SAVE flag on all logs

    if (   Test(ST_UNLATCHED, FGC_UNL_LOG_PLEASE) != 0
        && log_menus.post_mortem_time.unix_time   == 0)
    {
        uint8_t idx;

        for (idx = 0; idx < LOG_NUM_MENUS; idx++)
        {
            if (Test(LOG_MENU_PM_MASK, 1<<idx))
            {
                log_menus.status_bit_mask[idx] &= ~LOG_MENU_STATUS_SAVE_BIT_MASK;
            }
        }

        Clr(ST_UNLATCHED, FGC_UNL_LOG_PLEASE);

        // Unfreeze the logs if the CERN PM is not active

        if (Test(ST_UNLATCHED, FGC_UNL_POST_MORTEM) == 0)
        {
            LogStartAll();
        }
    }

    // Check if logs have been frozen

    if (   Test(ST_UNLATCHED, FGC_UNL_LOG_PLEASE) != 0
        && dev.log_pm_state == FGC_LOG_FROZEN)
    {
        uint8_t idx;

        for (idx = 0; idx < LOG_NUM_MENUS; idx++)
        {
            if (Test(LOG_MENU_PM_MASK, 1<<idx))
            {
                log_menus.status_bit_mask[idx] &= ~LOG_MENU_STATUS_RUNNING_BIT_MASK;
            }
        }
    }
}


bool logFgcLoggerIsPmEnabled(void)
{
    return (local.fgc_logger_enabled == true || dev.pm_enabled_f == true);
}


bool logFgcLoggerIsPmActive(void)
{
    return (   Test(ST_UNLATCHED, FGC_UNL_LOG_PLEASE) != 0
            || Test(ST_UNLATCHED, FGC_UNL_LOG_PLEASE) != 0);
}

#else
static void logFgcLoggerInit(void)
{
    ; // Do nohting
}

static void logFgcLoggerRunLogs(void)
{
    ; // Do nohting
}

void logFgcLoggerReset(void)
{
    ; // Do nohting
}

void logFgcLoggerSetEnable(bool const enabled)
{
    ; // Do nohting
}

void logFgcLoggerProcess(void)
{
    ; // Do nohting
}

#endif


/*---------------------------------------------------------------------------------------------------------*\
  End of file: log.c
\*---------------------------------------------------------------------------------------------------------*/
