/*---------------------------------------------------------------------------------------------------------*\
  File:         runlog.c

  Purpose:      FGC2 HC16 library - Run log functions

  Author:       Quentin.King@cern.ch

  Notes:        This file contains the functions needed to implement the run log used by the FGC2 Boot and
                Main HC16 programs.  The run log is stored in the FRAM memory in page F and is copied
                to SRAM in page 0 when the Main program is running, or Page 1 when the Boot is running.
                The SRAM copy is needed because FRAM has a limited life for read and write cycles,
                so the export of the run log via the FIP must come from RAM.  When a record is updated,
                it is written first in the FRAM and then in the SRAM.

                The run log record has a fixed length 6-byte structure as follows:

                            +0      +1      +2      +3      +4      +5
                        +-------+-------+-------+-------+-------+-------+
                        |RL SEQ | RL ID |   RL DATA[0]  |   RL DATA[1]  |
                        +-------+-------+-------+-------+-------+-------+

                The run log contains 100 elements arranged in a circular buffer.  The input pointer
                is stored in a word following the end of the buffer.  The run log will be exported
                via the WorldFIP to the gateway in the runlog byte in the status variable published
                by every FGC.  This can send 50 bytes/s, so the complete run log will be sent every
                16 seconds.

                The run log IDs are defined in def/src/platforms/fgc2/runlog.xml which the
                parser uses to generate inc/platforms/fgc2/runlog.h  This specifies a
                constant (enum) for each type of record, a label and the number of data bytes (0-4).

                The FRAM is normally read only - to write the FRAM, the ULKFRAM bit must
                be set in the CPU_RESET register.  The bit must be cleared afterward to relock the
                FRAM.

  History:

    01/06/04    qak     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <runlog_lib.h>
#include <stdbool.h>

#include <mem.h>
#include <mcu_dependent.h>
#include <iodefines.h>  // specific processor registers and constants
#include <string.h>     // for strcmp(), strcpy()
#include <memmap_mcu.h>
#include <os.h>         // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <fgc_runlog_entries.h>
#include <macros.h>     // for Test()

/*---------------------------------------------------------------------------------------------------------*/
void RunlogInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called when the boot starts or by the boot Reset Run Log menu option, and when the
  main program starts running out of SRAM during software development.

  The function will check that the run log in the FRAM is valid (using two magic numbers).
  The function will check that the run log input pointer is valid.  If not, it will reinitialise the
  pointer to the start of the run log and will reset the sequence number to restart at 1.  It will then
  increment the run number and will copy the complete run log zone from FRAM to SRAM in page 0 or 1.
  Finally it will add two entries to the run log, FGC_RL_FGC2_PROG_ID and FGC_RL_FGC2_RESETS.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      init_resets_f = false;
    uint16_t      reset_reg;

    ENTER_SR();                                         // Map FRAM to pages 7/F in Boot
    OS_ENTER_CRITICAL();

    asm
    {

        /* Set XK and EK page registers to page F and 0 respectively  */

        PSHM    K                                       // Save page registers
        LDY     #0x0000                                 // Clear Y
        LDAB    #0x7                                    // Set YK = 7 (to access FRAM)
        TBYK
        LDAB    #0x00                                   // Set EK = 0 (to access RESET register)
        TBEK
        BSETW   CPU_RESET_16,#CPU_RESET_ULKFRAM_MASK16  // Unlock write access to FRAM

        /* Check if run log not initialised */

        LDD     #MAGIC_RL                               // Check magic number
        CPD     NVRAM_RL_MAGIC_16,Y
        BNE     fullinit

        /* Check that input pointer is within run log buffer and aligned with start of a record */

        LDD     NVRAM_RL_INP_16,Y                       // D = record input pointer
        SUBD    #NVRAM_RL_BUF_16                        // D = offset of last record in buffer
        CPD     #NVRAM_RL_BUF_B                         // If D > RL_BUF_B (len of buffer in bytes)
        BCC     init                                    // Skip to init run log
        LDX     #FGC_RUNLOG_EL_SIZE                     // X = run log element size in bytes
        IDIV                                            // IX = D/IX   D = remainder
        TSTD                                            // Test if remainder is zero
        BEQ     end                                     // Jump if zero (point is valid)
        BRA     init                                    // Jump to initialise run log


    fullinit:   /* Full initialise */

        INCW    init_resets_f;                          // Set flag to initialise reset counters

    init:       /* Initialise input pointer to start of run log and reset sequence number */

        LDY     #NVRAM_RL_BUF_16                                // Y = start of run log buffer
        LDD     #NVRAM_RL_BUF_W                         // D = length of buffer in words

    clrrl:
        CLRW    0,Y                                     // Clear word in run log
        AIY     #2                                      // Increment pointer
        ADDD    #-1                                     // Decrement word counter
        BNE     clrrl                                   // Loop if not yet zero

        LDY     #0x0000                                 // Reset Y to start of page F

        LDD     #NVRAM_RL_BUF_16                                // Initialise input pointer to the 1st element
        STD     NVRAM_RL_INP_16,Y
        LDD     #FGC_RL_RLRESET                         // Set RLRESET ID in first element
        STD     NVRAM_RL_BUF_16,Y
        LDD     #MAGIC_RL                               // Set magic number
        STD     NVRAM_RL_MAGIC_16,Y

    end:
        BCLRW   CPU_RESET_16,#CPU_RESET_ULKFRAM_MASK16  // Lock write access to FRAM
        PULM    K                                       // Recover page registers
    }

    OS_EXIT_CRITICAL();

    /*--- Copy complete run log zone from FRAM to SRAM ---*/

    MemCpyWords(RL_32, NVRAM_RL_32, NVRAM_RL_W);
    EXIT_SR();

    /*--- Process reset register ---*/

    if(!init_resets_f)
    {
        RL_RESETS_ALL_P++;                                      // Increment RESETS_ALL counter

        reset_reg = CPU_RESET_P;                                // Word read of reset register

        if(Test(reset_reg,CPU_RESET_SRC_POWER_MASK16))          // If POWER reset
        {
            RL_RESETS_POWER_P++;                                        // Inc counter
        }

        if(Test(reset_reg,CPU_RESET_SRC_PROGRAM_MASK16))        // If PROGRAM reset
        {
            RL_RESETS_PROGRAM_P++;                                      // Inc counter
        }

        if(Test(reset_reg,CPU_RESET_SRC_MANUAL_MASK16))         // If MANUAL reset
        {
            RL_RESETS_MANUAL_P++;                                       // Inc counter
        }

        if(Test(reset_reg,CPU_RESET_SRC_FASTWD_MASK16))         // If FASTWD reset
        {
            RL_RESETS_FASTWD_P++;                                       // Inc counter
        }

        if(Test(reset_reg,CPU_RESET_SRC_SLOWWD_MASK16))         // If SLOWWD reset
        {
            RL_RESETS_SLOWWD_P++;                                       // Inc counter
        }

        if(Test(reset_reg,CPU_RESET_SRC_C32RAM_MASK16))         // If C32RAM reset
        {
            RL_RESETS_C32RAM_P++;                                       // Inc counter
        }

        if(Test(reset_reg,CPU_RESET_SRC_HC16RAM_MASK16))        // If HC16RAM reset
        {
            RL_RESETS_HC16RAM_P++;                                      // Inc counter
        }
    }

    RunlogSaveResets(init_resets_f);                    // Write reset counters back to FRAM

    RunlogWrite(FGC_RL_RESETS, (void*)RL_RESETS_ALL_16);

    /*--- Reset NanOS SM fields to prevent spurious RL entries in the event of a trap ---*/

    SM_TSKID_P   = 0x0010;
    SM_ISRMASK_P = 0x0000;
}
/*---------------------------------------------------------------------------------------------------------*/
void RunlogSaveResets(bool reset_f)
/*---------------------------------------------------------------------------------------------------------*\
  This function will save the reset counters from SRAM to FRAM.  If reset_f is true, all the reset counters
  will be reset to zero first.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(reset_f)                                         // If reset flag is true
    {
        MemSetBytes( (void *) RL_RESETS_A, 0, RL_RESETS_B);             // Zero reset counters
    }

    NVRAM_UNLOCK();

    ENTER_SR();
    MemCpyWords(NVRAM_RL_RESETS_32, RL_RESETS_32, RL_RESETS_W); // Copy reset counters to FRAM
    EXIT_SR();

    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void RunlogWrite(uint8_t id, void *data)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write a record in the run log.  It first adds the record to the FRAM run log zone, and
  then to the SRAM run log zone.  Interrupts are disabled during this function to ensure the run log stays
  coherent.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      saved_cpu_mode;
    uint16_t      saved_cpu_reset;

    if(!IS_RL_READY())                          // If run log not ready
    {
        return;                                         // Return immediately
    }

    ENTER_SR();
    OS_ENTER_CRITICAL();                                // Block interrupts
    saved_cpu_mode = CPU_MODE_P;                        // Save CPU mode
    saved_cpu_reset = CPU_RESET_P;                      // Save CPU reset (it has FRAM unlock bit in 0x100)

    asm
    {
        /* Get sequence number from last record and increment */

        LDX     RL_INP_16                               // X = pointer to last record
        LDD     0,X                                     // D = A:B = SEQ:ID of last record
        INCA                                            // Increment sequence number

        /* Adjust input record pointer */

        AIX     #FGC_RUNLOG_EL_SIZE                     // Increment IX to start of next record
        CPX     #RL_INP_16                              // Compare IX with end of run log buffer
        BCS     write                                   // Branch if IX is less than end of log buffer
        LDX     #RL_BUF_16                              // Reset IX to start of log buffer

        /* Write new record into FRAM */

    write:
        PSHM    K                                       // Save page register
        CLRB                                            // Clear B
        TBEK                                            // Set EK = B =0 (to access RESET register)
        LDAB    #0x7                                    // Set XK = 7 (to access FRAM)
        TBXK
        BSETW   CPU_RESET_16,#CPU_RESET_ULKFRAM_MASK16  // Unlock write access to FRAM

        LDAB    id                                      // A  = run log ID
        LDY     data                                    // IY = pointer to data
        STD     0,X                                     // Store SEQ:ID in record
        LDE     0,Y                                     // Get first word of data
        STE     2,X                                     // Store first word of data in record
        LDE     2,Y                                     // Get second word of data
        STE     4,X                                     // Store second word of data in record

        TXY                                             // Transfer X into Y (pointer to record)
        LDX     #0x0000                                 // Clear X
        STY     NVRAM_RL_INP_16,X                       // Store pointer to record in FRAM

        PULM    K                                       // Recover page register

        /* Write new record into SRAM */

        STY     RL_INP_16                               // Store pointer to record in SRAM
        LDX     data                                    // IY = pointer to data
        STD     0,Y                                     // Store SEQ:ID in record
        LDD     0,X                                     // Get first word of data
        STD     2,Y                                     // Store first word of data in record
        LDD     2,X                                     // Get second word of data
        STD     4,Y                                     // Store second word of data in record
    }

    CPU_MODE_P = saved_cpu_mode;                        // Restore CPU mode
    CPU_RESET_P = saved_cpu_reset;                      // Restore CPU reset
    OS_EXIT_CRITICAL();                                 // Allow interrupts
    EXIT_SR();
}
#pragma MESSAGE DISABLE C1404 // Disable 'return expected' warning
/*---------------------------------------------------------------------------------------------------------*/
uint8_t * RunlogRead(uint8_t *outp, uint8_t *seq, uint8_t *id, void *data)
/*---------------------------------------------------------------------------------------------------------*\
  This function will read the specified record from the run log.  If outp is zero, it will be initialised
  to the most recently entered record.  The function returns the address of the previous record in outp.
\*---------------------------------------------------------------------------------------------------------*/
{
    /*--- Copy record atomically to local buffer ---*/

    asm
    {
        LDD     outp                                    // D = output pointer
        SUBD    #RL_BUF_16                              // D = input pointer relative to start of buffer
        CPD     #RL_BUF_B                               // If D > RL_BUF_B (len in bytes)
        BCC     initoutp                                // Skip to initialise outp
        LDX     #FGC_RUNLOG_EL_SIZE                     // X = run log element size in bytes
        IDIV                                            // IX = D/IX   D = remainder
        TSTD                                            // if remainder is zero (pointer is valid)
        BEQ     readlog                                 // Skip to read log

    initoutp:
        LDD     RL_INP_16                               // Initialise outp to last stored record
        STD     outp

    readlog:
        PSHM    CCR                                     // Block interrupts
        ORP     #0x00E0
        LDY     outp
        LDD     0,Y
        LDX     seq
        STAA    0,X                                     // *seq = outp[0]
        LDX     id
        STAB    0,X                                     // *id  = outp[1]
        LDX     data
        LDD     2,Y
        STD     0,X                                     // *data = outp[2-5]
        LDD     4,Y
        STD     2,X
        PULM    CCR                                     // Allow interrupts

        AIY     #-FGC_RUNLOG_EL_SIZE                    // Adjust outp to previous record
        CPY     #RL_BUF_16                              // If not before start of buffer
        BCC     end                                     // Skip to end

        AIY     #RL_BUF_B                               // Adjust outp to end of buffer
    end:
    }                                                   // Return IY (outp)
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: runlog.c
\*---------------------------------------------------------------------------------------------------------*/

