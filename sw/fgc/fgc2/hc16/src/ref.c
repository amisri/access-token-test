/*---------------------------------------------------------------------------------------------------------*\
 File:      ref.c

 Purpose:   FGC Software - Reference conversion functions

 Author:    Quentin.King@cern.ch
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL        // defprops.h
#define REF_GLOBALS             // define ref, simsc global variables

#include <stdbool.h>
#include <stdint.h>

#include <ref.h>

#include <cmd.h>                // for struct cmd
#include <class.h>              // for masks[] global variable
#include <fbs_class.h>          // for ST_UNLATCHED, STATE_PC, V_REF, I_REF
#include <defprops.h>
#include <definfo.h>            // for FGC_CLASS_ID
#include <fbs.h>                // for fbs global variable
#include <dpcom.h>              // for dpcom global variable
#include <macros.h>             // for Test(), Set(), Clr(), DEVICE_PPM
#include <os.h>                 // for OSTskSuspend()
#include <dpcls.h>              // for dpcls global variable
#include <prop.h>               // for PropSet()
#include <fgc_errs.h>           // for FGC_BAD_PARAMETER, FGC_BAD_STATE
#include <log_class.h>          // for timing_log global variable
#include <cal_class.h>          // for NON_PPM_REG_MODE
#include <pars.h>               // for ParsScanStd()
#include <shared_memory.h>      // for FGC_MAINPROG_RUNNING
#include <set.h>                // for SetRefLockInternal
#include <mcu_dependent.h>

#define NEXT_SSC_DELAY_US         200000
#define NEXT_CYCLE_START_DELAY_US 200000


// ---------  Internal structures, unions and enumerations  -------------

enum sim_event_slot
{
    SIM_EVENT_SLOT_SSC,
    SIM_EVENT_SLOT_CYCLE_START,
};

// PPM Simulated Super-cycle variables

struct vars
{
    bool     enabled;        // Simulated super-cycle enabled flag
    uint16_t cyc_idx;        // Index in simulated super-cycle
    uint16_t cyc_time_ms;    // Milliseconds since start of simulated cycle
    uint16_t cyc_len_ms;     // Length of current cycle in milliseconds
} ssc_sim;


struct Ref_prop_to_func_type
{
    struct prop                 ** property;              //!< Property
    uint16_t                       func_type;             //!< Function type constant
    uint16_t                       stc_func_type;         //!< Function type symbol index
};


// ---------  Internal variable definitions  ----------------------------

static struct fgc_event sim_events[FGC_NUM_EVENT_SLOTS] =
{
    { 0, 0, 0, NEXT_SSC_DELAY_US         },
    { 0, 0, 0, NEXT_CYCLE_START_DELAY_US },
};


static struct prop *plep[] =
{
    &PROP_REF_PLEP_FINAL,
    &PROP_REF_PLEP_ACCELERATION,
    &PROP_REF_PLEP_LINEAR_RATE,
    &PROP_REF_PLEP_EXP_TC,
    &PROP_REF_PLEP_EXP_FINAL,
    NULL
};

static struct prop *pppl[] =
{
    &PROP_REF_FIRST_PLATEAU_DURATION,
    &PROP_REF_FIRST_PLATEAU_REF,
    &PROP_REF_FIRST_PLATEAU_TIME,
    &PROP_REF_PPPL_ACCELERATION1,
    &PROP_REF_PPPL_ACCELERATION2,
    &PROP_REF_PPPL_ACCELERATION3,
    &PROP_REF_PPPL_RATE2,
    &PROP_REF_PPPL_RATE4,
    &PROP_REF_PPPL_REF4,
    &PROP_REF_PPPL_DURATION4,
    NULL
};

static struct prop *trim[] =
{
    &PROP_REF_TRIM_FINAL,
    &PROP_REF_TRIM_DURATION,
    NULL
};

struct prop *test[] =
{
    &PROP_REF_TEST_AMPLITUDE,
    &PROP_REF_TEST_NUM_CYCLES,
    &PROP_REF_TEST_PERIOD,
    &PROP_REF_TEST_WINDOW,
    NULL
};

static struct prop *table[] =
{
    &PROP_REF_FIRST_PLATEAU_DURATION,
    &PROP_REF_FIRST_PLATEAU_REF,
    &PROP_REF_FIRST_PLATEAU_TIME,
    &PROP_REF_TABLE_FUNC_VALUE,
    NULL
};

static struct prop *zero[] =
{
    &PROP_REF_FIRST_PLATEAU_DURATION,
    &PROP_REF_FIRST_PLATEAU_REF,
    &PROP_REF_FIRST_PLATEAU_TIME,
    NULL
};

static struct prop *ramp[] =
{
    &PROP_REF_RAMP_FINAL,
    NULL
};


static struct Ref_prop_to_func_type ref_prop_to_func_type[] =
{
    // FGC_REF_NONE
    // FGC_REF_STARTING
    // FGC_REF_STOPPING
    // FGC_REF_TO_STANDBY
    // FGC_REF_ARMED
    // FGC_REF_ABORTING
    // FGC_REF_NOW
    // FGC_REF_STEPS
    // FGC_REF_SQUARE
    { test , FGC_REF_SINE , STC_SINE  },
    // FGC_REF_COSINE
    { plep , FGC_REF_PLEP , STC_PLEP  },
    { pppl , FGC_REF_PPPL , STC_PPPL  },
    // FGC_REF_LTRIM
    { trim , FGC_REF_CTRIM, STC_CTRIM },
    { table, FGC_REF_TABLE, STC_TABLE },
    { ramp , FGC_REF_RAMP , STC_RAMP  },
    // FGC_REF_ZERO
    // FGC_REF_END
    // FGC_REF_PLEP_NO_CLIP
    { NULL , 0            , 0         },
};



bool RefGetFuncTypeFromProp(struct prop * property, uint16_t * func_type, uint16_t * stc_func_type)
{
    struct Ref_prop_to_func_type * prop_to_func_type = ref_prop_to_func_type;

    while (prop_to_func_type->property != NULL)
    {
        struct prop ** type_property = prop_to_func_type->property;

        while (*type_property != NULL && *type_property != property)
        {
            type_property++;
        }

        if (*type_property != NULL)
        {
            *func_type     = prop_to_func_type->func_type;
            *stc_func_type = prop_to_func_type->stc_func_type;
            break;           
        }

        prop_to_func_type++;
    }

    return (prop_to_func_type->property != NULL);
}



static void refEnableSim(void)
{
    ssc_sim.enabled     = true;
    ssc_sim.cyc_idx     = 0;
    ssc_sim.cyc_time_ms = 0;
    ssc_sim.cyc_len_ms  = 0;

    // Inform DSP that we are simulating supercycle

    dpcom.mcu.evt.sim_supercycle_f = true;
}



static void refDisableSim(void)
{
    ssc_sim.enabled = false;

    // Inform DSP that we stopped simulating supercycle

    dpcom.mcu.evt.sim_supercycle_f = false;
}



static void refSimulateEvents(void)
{
    ssc_sim.cyc_time_ms += 20;

    // Cycle has ended

    if(ssc_sim.cyc_time_ms >= ssc_sim.cyc_len_ms)
    {
        ssc_sim.cyc_len_ms  = (simsc.cycles[ssc_sim.cyc_idx] % 100) * FGC_BASIC_PERIOD_MS;
        ssc_sim.cyc_time_ms = 0;

        // Cancel EVT_CYCLE_START

        sim_events[SIM_EVENT_SLOT_CYCLE_START].type = FGC_EVT_NONE;

        // If first simulated cycle, set EVT_SSC

        if(ssc_sim.cyc_idx == 0)
        {
            sim_events[SIM_EVENT_SLOT_SSC].type = FGC_EVT_SSC;
        }

        ssc_sim.cyc_idx++;

        if(ssc_sim.cyc_idx >= PROP_FGC_CYC_SIM.n_elements)
        {
            ssc_sim.cyc_idx = 0;
        }
    }
    else if(ssc_sim.cyc_time_ms == (ssc_sim.cyc_len_ms - 160))  // else if 160ms before end of cycle
    {
        // Cancel EVT_CYCLE_START

        sim_events[SIM_EVENT_SLOT_CYCLE_START].type = FGC_EVT_NONE;
    }
    else if(ssc_sim.cyc_time_ms == (ssc_sim.cyc_len_ms - 200))  // else if 200ms before end of cycle
    {
        const uint16_t user = simsc.cycles[ssc_sim.cyc_idx] / 100;

        // Prepare simulated EVT_NEXT_CYC_USER event

        sim_events[SIM_EVENT_SLOT_CYCLE_START].type    = FGC_EVT_CYCLE_START;
        sim_events[SIM_EVENT_SLOT_CYCLE_START].payload = user;
    }
    else if(ssc_sim.cyc_time_ms == 20)      // else if just after start of new cycle
    {
        // Cancel EVT_SSC

        sim_events[SIM_EVENT_SLOT_SSC].type = FGC_EVT_NONE;
    }
}



/*---------------------------------------------------------------------------------------------------------*/
void RefEvent(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from FipReadTimeVar() when a time variable has been received.  The variable
  contains the event table from the gateway which must be checked.  This is class dependent so it is
  treated here.
\*---------------------------------------------------------------------------------------------------------*/
{
    const struct fgc_event * event   = fbs.time_v.event;
    uint16_t                 event_group;
    uint16_t                 i;

    // Simulate super cycle

    if(SM_MPRUN_P == FGC_MAINPROG_RUNNING)
    {
        if(!ssc_sim.enabled && PROP_FGC_CYC_SIM.n_elements != 0)
        {
            refEnableSim();
        }
        else if(ssc_sim.enabled && PROP_FGC_CYC_SIM.n_elements == 0)
        {
            refDisableSim();
        }
    }

    if(ssc_sim.enabled)
    {
        refSimulateEvents();

        event = sim_events;
    }

    // Always clear START_EVENT. It will be set if the event START or TRANSACTION_COMMIT
    // has been receveid and processed correctly

    Clr(ST_UNLATCHED, FGC_UNL_START_EVENT);

    // Analyse event slots

    for(i = 0; i < FGC_NUM_EVENT_SLOTS; i++, event++)
    {
        switch(event->type)
        {
            case FGC_EVT_START:
                event_group = event->payload;

                if (event_group == 0 || event_group == ref.event_group)
                {
                    // Make sure the event is not already active
                    if (Test(ST_UNLATCHED, FGC_UNL_START_EVENT) == 0)
                    {
                        Set(ST_UNLATCHED, FGC_UNL_START_EVENT);
                        dpcom.mcu.evt.start_event_delay = event->delay_us / 1000;
                    } 
                }

                break;

            case FGC_EVT_TRANSACTION_COMMIT:
                // Make sure the event is not already active
                if (Test(ST_UNLATCHED, FGC_UNL_START_EVENT) == 0)
                {
                    transactionCommit(event->payload, event->delay_us / 1000);
                    Set(ST_UNLATCHED, FGC_UNL_START_EVENT);
                }

                break;

            case FGC_EVT_ABORT:
                // Do nothing
                break;

            case FGC_EVT_TRANSACTION_ROLLBACK:
                transactionRollback(event->payload);
                break;

            case FGC_EVT_PM:
                // If Ext PM request is NOT active, set Ext PM request
                if(!Test(fbs.u.fieldbus_stat.ack, FGC_EXT_PM_REQ))
                {
                    Set(fbs.u.fieldbus_stat.ack, FGC_EXT_PM_REQ);
                }
                break;

            case FGC_EVT_CYCLE_START:
                if (dpcom.mcu.evt.next_cycle_start_delay_ms == 0)
                {
                    dpcom.mcu.evt.next_cycle_user           = event->payload;
                    dpcom.mcu.evt.next_cycle_start_delay_ms = event->delay_us / 1000;
                }

                break;

            case FGC_EVT_SSC:    // Class 53: Start of supercycle Event
                if (dpcom.mcu.evt.ssc_f == false)
                {
                    dpcom.mcu.evt.ssc_f = true;
                    timing_log.user[timing_log.in_idx] = -timing_log.user[timing_log.in_idx];
                }

                break;

            case FGC_EVT_CYCLE_TAG:
                // Pass high byte of cycle tag to DSP
                dpcom.mcu.evt.cycle_tag = event->payload;
                break;

            default:
                break;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefArm(struct cmd *c, uint16_t user, uint32_t func_type, uint16_t stc_func_type)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to arm or disarm a reference function in the DSP.  The rules about when a given
  user can be armed/disarmed are rather complex and are summarised here: doc\ref_arming_rules.xlsx
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              dsp_status;

    // Wait for property block buffer to be free

    dsp_status = PropBufWait(c);
    if(dsp_status)
    {
        return(dsp_status);
    }

    // Determine target state for the armed function (RUNNING or CYCLING) and reject invalid states

    if(Test(masks[STATE_PC],RUNNING_FUNC_STATE_MASK))
    {
        if(user == NON_PPM_USER && (STATE_PC == FGC_PC_IDLE  || (STATE_PC == FGC_PC_ARMED && func_type == FGC_REF_NONE)))
        {
            dpcls.mcu.ref.func_state = FGC_PC_RUNNING;
        }
        else
        {
            return(FGC_BAD_STATE);
        }

#if (FGC_CLASS_ID == 53)
        // B regulation mode is not permitted in RUNNING
        if(NON_PPM_REG_MODE == FGC_REG_B)
        {
            return(FGC_BAD_REG_MODE);
        }
#endif
    }
    else // CYCLING
    {

#if (FGC_MAX_USER == 0)
        return(FGC_BAD_STATE);
#else
        if((user == NON_PPM_USER && DEVICE_PPM && func_type != FGC_REF_NONE))
        {
            return(FGC_BAD_STATE);
        }

        dpcls.mcu.ref.func_state = FGC_PC_CYCLING;
#endif

    }

    // Pass Arm/Disarm request to DSP

    dpcls.mcu.ref.user          = user;
    dpcls.mcu.ref.func_type     = func_type;
    dpcls.mcu.ref.stc_func_type = stc_func_type;

    OS_ENTER_CRITICAL();

    dpcom.dsp.bg_complete_f = false;
    dpcls.mcu.ref.arm_f     = true;

    // Sleep till MstTsk() resumes on next ms

    while(!(uint16_t)dpcom.dsp.bg_complete_f)
    {
        OSTskSuspend();
    }

    OS_EXIT_CRITICAL();

    // Record armed func type in property and set non-volatile memory (NONE always for RUNNING)

    if(dpcls.mcu.ref.func.type[user] != dpcls.dsp.ref.func.type[user])
    {
        dpcls.mcu.ref.func.type[user] = dpcls.dsp.ref.func.type[user];
        c->changed_f = true;
    }

    func_type = (dpcls.mcu.ref.func_state == FGC_PC_RUNNING ? FGC_REF_NONE : dpcls.mcu.ref.func.type[user]);

    return dpcom.dsp.errnum;
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefPlep(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the user enters "s ref plep,..." to process the plep reference setup.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32      rate;
    FP32      acceleration;

    uint16_t  errnum;
    uint16_t  load_select;

    static struct prop *p[] =
    {
        &PROP_REF_PLEP_FINAL,
        &PROP_REF_PLEP_ACCELERATION,
        &PROP_REF_PLEP_LINEAR_RATE,
        &PROP_REF_PLEP_EXP_TC,
        &PROP_REF_PLEP_EXP_FINAL,
    };

    // Prepare default values
    // Use REF.DEFAULTS.I if not zero, otherwise LIMITS.I


    
    if(NON_PPM_REG_MODE == FGC_REG_V)                                          // If open loop
    {
        ref.pars.f[0] = V_REF;                                              // FINAL
        ref.pars.f[1] = dpcls.mcu.ref.defaults.v.acceleration;              // ACCELERATION
        ref.pars.f[2] = dpcls.mcu.ref.defaults.v.linear_rate;               // LINEAR_RATE
        ref.pars.f[3] = 0.0;                                                // EXP_TC
        ref.pars.f[4] = 0.0;                                                // EXP_FINAL
    }
    else                                                                    // else, close loop
    {
        load_select = (uint16_t)dpcls.dsp.load_select;

        ref.pars.f[0] = I_REF;                                              // FINAL    
        ref.pars.f[1] = dpcls.mcu.ref.defaults.i.acceleration[load_select]; // ACCELERATION
        ref.pars.f[2] = dpcls.mcu.ref.defaults.i.linear_rate[load_select];  // LINEAR_RATE
        ref.pars.f[3] = dpcls.dsp.ref.exp_tc;                               // EXP_TC
        ref.pars.f[4] = dpcls.dsp.ref.exp_final;                            // EXP_FINAL
    }

    acceleration = ref.pars.f[1];
    rate         = ref.pars.f[2];

    // Get up to five parameters (final,acceleration,linear_rate,exp_tc,exp_final)
    
    errnum = RefGetPars(c, (struct prop **) &p, ref.pars.f, 5);
    if ( errnum )
    {
        return(errnum);
    }

    // If the user specified a 0 value, use REF.DEFAULTS

    if (ref.pars.f[1] == 0.0F && acceleration != 0)
    {
        ref.pars.f[1] = acceleration;
    }

    if (ref.pars.f[2] == 0.0F && rate != 0)
    {
        ref.pars.f[2] = rate;
    }

    // Set properties

    RefSetPars(c, (struct prop **) &p, ref.pars.f, 5);

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefOpenloop(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the user enters "s ref ramp,..." to process an openloop reference function
  setup.  Parameters are:

    s ref ramp, final

  Defaults are:     final       reference now
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  errnum;

    static struct prop *p[] =
    {
        &PROP_REF_RAMP_FINAL,
    };

    // Check that REF.FUNC.REG_MODE[0] is not V (closed-loop) and RT is NOT enabled

    if ( NON_PPM_REG_MODE == FGC_REG_V || (uint16_t)dpcls.mcu.mode_rt)
    {
    return(FGC_BAD_STATE);
    }

    //Prepare default values

    ref.pars.f[0] = I_REF;

    // Get up to two parameters (final,period)

    errnum = RefGetPars(c, (struct prop **) &p, ref.pars.f, 1);
    if ( errnum )
    {
    return(errnum);
    }

    // Set properties

    RefSetPars(c, (struct prop **) &p, ref.pars.f, 1);

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefTrim(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the user enters "s ref ltrim|ctrim,..." to process a trim reference function
  setup.  Parameters are:

    s ref func, final, period

  Defaults are:     final       reference now
            period      0 (as fast as possible)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  errnum;

    static struct prop *p[] =
    {
        &PROP_REF_TRIM_FINAL,
        &PROP_REF_TRIM_DURATION,
    };

    // Prepare default values

    ref.pars.f[0] = (NON_PPM_REG_MODE == FGC_REG_V ? V_REF : I_REF);
    ref.pars.f[1] = 0.0;

    // Get up to two parameters (final,period)

    errnum = RefGetPars(c, (struct prop **) &p, ref.pars.f, 2);
    if ( errnum )
    {
    return(errnum);
    }

    // Set properties

    RefSetPars(c, (struct prop **) &p, ref.pars.f, 2);

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefTest(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the user enters "s ref steps|square|sine|cosine,..." to process the
  test reference function setup.  Parameters are:

    s ref func, amplitude, num_cycles, period

  Defaults are:     amplitude   1.0
            num_cycles  1.0
            period      1.0
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  errnum;

    static struct prop *p[] =
    {
        &PROP_REF_TEST_AMPLITUDE,
        &PROP_REF_TEST_NUM_CYCLES,
        &PROP_REF_TEST_PERIOD
    };

    /*--- Prepare default values ---*/

    ref.pars.f[0] = 1.0;
    ref.pars.f[1] = 1.0;
    ref.pars.f[2] = 1.0;

    // Get up to three parameters (amp,n_cyc,period)

    errnum = RefGetPars(c, (struct prop **) &p, ref.pars.f, 3);
    if ( errnum )
    {
    return(errnum);
    }

    // Set properties

    RefSetPars(c, (struct prop **) &p, ref.pars.f, 3);

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefGetPars(struct cmd * c, struct prop ** p, FP32 * pars, uint16_t n_pars)
/*---------------------------------------------------------------------------------------------------------*\
  This function will get and check the required number of reference property parameters.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  i;
    uint16_t  errnum;
    float * newval;
    float   value;
    struct prop * pr;

    for ( i = 0; i < n_pars && !c->last_par_f; p++, i++, pars++)
    {
        // User PLEP_FINAL as it
        errnum = ParsScanStd(c, &PROP_REF_PLEP_FINAL, PKT_FLOAT, (void**)&newval);      
                                             // has the widest limits
        if(errnum != FGC_NO_SYMBOL)
        {
            if(errnum)
            {
                // If not first parameter
                if(i)
                {
                    c->device_par_err_idx = i;
                }
                return(errnum);
            }

            value = *newval;
            pr    = *p;

            if (   value < ((struct float_limits *)pr->range)->min
                || value > ((struct float_limits *)pr->range)->max)
            {
                c->device_par_err_idx = i;
                return(FGC_OUT_OF_LIMITS);
            }

            *pars = value;
        }
    }

    // If more parameters remain
    if(!c->last_par_f)
    {
        c->device_par_err_idx = i;
        return(FGC_BAD_PARAMETER);
    }

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
void RefSetPars(struct cmd *c, struct prop **p, FP32 *pars, uint16_t n_pars)
/*---------------------------------------------------------------------------------------------------------*\
  This function sets the properties identified in the array p with the values in the array pars
\*---------------------------------------------------------------------------------------------------------*/
{
    while(n_pars--)
    {
        PropSet(c, *(p++), pars++, 1);
    }
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: ref_class.c
\*---------------------------------------------------------------------------------------------------------*/
