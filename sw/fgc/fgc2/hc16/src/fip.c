/*!
 * @file fip.c
 *
 * @brief FGC MCU Software - uFIP Interface Functions.
 *
 * The uFIP MSGSA register has a bit (EMP_MST) to indicate if the msg TX buffer is empty or occupied.
 * When message transmission is disabled, this claims the buffer is occupied,
 * so a flag is needed to override EMP_MST when messaging has been enabled for the first time (when the PLL locks).
 * This flag is fip.start_tx.
 */

#define FIP_GLOBALS         // define fip global variable

#include <stdbool.h>
#include <fip.h>
#include <fgc_fip.h>        // for FGC_FIELDBUS_MAX_CMD_LEN. Order dependent!! Before fbs.h
#include <fbs.h>            // for struct fbs_stream, fbs global variable
#include <mcu_dependent.h>  // for LED_SET, LED_RST, USLEEP
#include <cmd.h>            // for fcm global variable
#include <class.h>          // Include class dependent definitions
#include <definfo.h>        // for FGC_CLASS_ID
#include <queue_fgc.h>      // struct queue
#include <macros.h>         // for Test(), Set()
#include <sta.h>            // for sta global variable
#include <memmap_mcu.h>     // for CPU_RESET_
#include <iodefines.h>      // specific processor registers and constants
#include <fgc_errs.h>       // for FGC_EXECUTING
#include <mem.h>            // for MemCpyFip()
#if defined(__HC16__) || defined(__RX610__)
   #include <structs_bits_big.h>
#endif
#include <dpcls.h>
#include <tsk.h>
#include <dpcom.h>              // for dpcom global variable
#include <shared_memory.h>      // for FGC_MAINPROG_RUNNING
#include <fbs_class.h>
#include <fgc_stat_consts.h>
#include <start.h>

// Local structures

struct fip_cmd_msg                                      // FIP command message
{
    struct fgc_fip_addr         address;
    struct fgc_fieldbus_cmd     msg;
};

struct fip_rsp_msg                                      // FIP response message
{
    struct fgc_fip_addr         address;
    struct fgc_fieldbus_rsp     msg;
};

// Constants

#define FIP_STAT_V              ((struct fgc_fieldbus_stat *)FIP_DATMPS_32)
#define FIP_CMD_MSG             ((struct fip_cmd_msg *)FIP_RXDMSG_32)
#define FIP_RSP_MSG             ((struct fip_rsp_msg *)FIP_TXDMSG_32)
#define FIP_MSG_HDR             ((struct fgc_fieldbus_cmd_header *)FIP_MSGHEAD_32)

#define FIP_ID_CHECK_RETRIES    2

// These macros are not present in class 59
// They are however used in classes 51 and 53 to check if the FGC can safely reset
// after going from stand-alone to FIP-driven mode (in FipWatch function)

#if (FGC_CLASS_ID == 59)
#define STATE_PC       0
#define FGC_PC_OFF     0
#define FGC_PC_FLT_OFF 0
#endif

// Internal functions declarations

/*!
 * @brief Returns FIP address
 *
 * @return Current FIP address readout
 */
static uint16_t FipReadAddress(void);

/*!
 * @brief Returns stable FIP address (or 0 if isn't stable)
 *
 * The function reads FIP address twice in 200ms interval. If the readouts don't match then it waits another 800ms
 * and reads the address twice again. If that one also fails then the function returns 0.
 *
 * @return FIP address.
 */
static uint16_t FipGetAddress(void);

/*!
 * @brief Sets FIP ID based on FIP address
 *
 * When FIP address passed to the function isn't in range [1,30] then ID will be set to FIP_STANDALONE_ID.
 * Otherwise it will be set to the value passed as fip_address.
 *
 * @param[in] fip_address Last address read by FipGetAddress function
 */
static void FipSetId(uint16_t fip_address);

// Internal functions definitions



uint16_t FipReadAddress(void)
{
    // Reset uFIP (takes 1us)

    FIP_RESET_P = 0;
    USLEEP(2);

    return FIP_STADR_P;
}



uint16_t FipGetAddress(void)
{
    uint16_t retry;
    uint16_t prev_address;
    uint16_t curr_address;
    uint16_t i;

    for (retry = 0; retry < FIP_ID_CHECK_RETRIES; retry++)
    {
        prev_address = FipReadAddress();

        // Sleep for 200ms between ID readouts

        for (i = 0; i < 20; i++)
        {
            USLEEP(10000);
        }

        curr_address = FipReadAddress();

        // If address is stable

        if(curr_address == prev_address)
        {
            break;
        }

        // Set address to 0 if it's not stable

        curr_address = 0;

        // Sleep for additional 800ms after every but last failure

        for (i = 0; (retry < FIP_ID_CHECK_RETRIES - 1) && (i < 80); i++)
        {
            USLEEP(10000);
        }
    }

    return curr_address;
}



void FipSetId(uint16_t fip_address)
{
    if (fip_address < 1 || fip_address > 30)
    {
        fbs.id = FIP_STANDALONE_ID;
    }
    else
    {
        fbs.id = fip_address;
    }
}



// External functions definitions



void FipInit(void)
{
    fbs.cmd_rsp_len = sizeof(((struct fgc_fieldbus_rsp * )0)->rsp_pkt);
    fbs.gw_online_f = false;

    fbs.gw_timeout = FGC_FIELDBUS_CYCLE_FREQ * (600 + 2 * fbs.id); // Set GW timeout to (600 + 2 * FIP_ID) secs

#if FGC_CLASS_ID == 59
    fip.rt_ref_vidgl        = 2;                // Prepare for RT data variable (always 1st)
#else
    fip.rt_ref_offset       = 4 + 4 * ((fbs.id-1)%15);  // RD Data offset for this DC
    fip.rt_ref_vidgl        = 2 + (fbs.id > 15);        // Prepare for RT data variable
#endif
    fbs.last_runlog_idx      = 0x0000;                  // Prepare repeat Var 7 detection
    fip.runlog_idx_offset        = (uint32_t)&fbs.time_v.runlog_idx - (uint32_t)&fbs.time_v;
    fbs.time_v.ms_time       = 0xFFFF;                  // Cancel initial time packet
    fbs.u.fieldbus_stat.ack &= ~FGC_ACK_STATUS;         // Reset stat var sequence to 0
    fbs.u.fieldbus_stat.class_id = FGC_CLASS_ID;        // Report FGC class ID to gateway

    // Store REGIS bank register values

    FIP_CONFA_P = 0x05;     // No Msg ACK, 0x4: Msg snd disabled, 0x1: Msg rcv disabled
    FIP_CONFB_P = 0x80;     // 0x80: Var 7 rcv enabled, others disabled
    FIP_CONFC_P = 0x0A;     // RP time 4us, Field-drive, 0x8: WorldFIP frame, 0x2: 2.5MB/s
    FIP_CONFD_P = 0x0F;     // 0xE: Silence time 100us, 0x1: Var 7 uses global ID
    FIP_CONFE_P = 0x84;     // 0x80: Int on Var 7 rcv, 0x4: Int on msg rcv
    FIP_MPSPR_P = 0x00;     // Var 7 promptness 50ms, Var 6 refreshment 250ms
    FIP_VIDGL_P = 0x00;     // Var 7 global ID (low byte)
    FIP_VIDGH_P = 0x30;     // Var 7 global ID (high byte)

    // Store REGI2 bank register values

    FIP_MSSEG_P = 0x00;     // Msg segment number
    FIP_ARCNL_P = 0x01;     // UART config (low byte):  40MHz clock, 2.5MHz bus
    FIP_ARCNH_P = 0x00;     // UART config (high byte): 40MHz clock, 2.5MHz bus

    // Set up COMBPS area to define variable lengths and buffers

    FIP_COBMPS_A[6] = 0x71; // Var 6 (stat): Length = 7 blocks (56 bytes), first block = 1
    FIP_COBMPS_A[7] = 0x88; // Var 7 (time): Length = 8 blocks (64 bytes), first block = 8

    // Set up Response message address block

    FIP_RSP_MSG->address.dest_addr_h = 0x00;    // GW address is 0x0000
    FIP_RSP_MSG->address.dest_addr_l = 0x00;
    FIP_RSP_MSG->address.dest_seg    = 0x00;    // Segment 0
    FIP_RSP_MSG->address.src_addr_h  = 0x00;    // DC address (0x0001-0x001E)
    FIP_RSP_MSG->address.src_addr_l  = fbs.id;
    FIP_RSP_MSG->address.src_seg     = 0x00;    // Segment 0

    // Start interface

    FIP_CONFD_P |= 0x80;                // Start uFIP interface
    GPT_TMSK_P  |= 0x0100;              // Enable GPT IC1 (uFIP IRQ) interrupts
}



void FipWatch(void)
{
    if (!fbs.pll_locked_f &&
        (sta.config_state == FGC_CFG_STATE_STANDALONE))
    {
        fip.watchdog++;
        if (fip.watchdog >= FBS_WATCHDOG_PERIOD)
        {
            fip.watchdog = 0;

            // Reset uFIP (takes 1us)

            FIP_RESET_P = 0;
            USLEEP(2);

            // Reset the device if the FIP address doesn't indicate stand-alone mode anymore
            // Reset only when the converter is off

            if ( (FIP_STADR_P > 0 && FIP_STADR_P < 31)
#if (FGC_CLASS_ID != 59)
                 && (STATE_PC == FGC_PC_OFF || STATE_PC == FGC_PC_FLT_OFF)
#endif
                 )
            {
                Reset();
            }
        }
    }
    else
    {
        fip.watchdog = 0;
    }
}



void FipSetOperatingMode(void)
{
    uint16_t  fip_address = UINT16_MAX;

    fip_address = FipGetAddress();

    FipSetId(fip_address);

    if (fbs.id == FIP_STANDALONE_ID)
    {
        sta.config_state = FGC_CFG_STATE_STANDALONE;
        sta.config_mode  = FGC_CFG_MODE_SYNC_NONE;
    }
    else
    {
        FipInit();
    }
}



bool FipSendStatVar(void)
{
    if (Test(FIP_MPSSA_P, 0x08))                        // If Var 6 is NOT accessable...
    {
        return (false);                                 // Report failure
    }

    FIP_CONFD_P = (6 << 4) | (FIP_CONFD_P & 0x8f);      // Identify Var 6 access to uFIP

    MemCpyFip((void*) FIP_STATVAR_32,                   // Write digital data to Var 6 in uFIP
            &fbs.u.fieldbus_stat, FIP_STATVAR_B);

    FIP_COBMPS_A [6] = 0x71;                            // Re-assert Var 6 size/position
    Set(FIP_CONFB_P, 0x08);
    // Re-enable Var 6 transmission

    return (true);
}



void FipSendMsg(void)
{
    FIP_CONFA_P = 0x04;                                         // Disable transmit channel

    FIP_RSP_MSG->msg.header.flags = fbs.pend_header;            // Set header with pkt description
    FIP_RSP_MSG->msg.header.n_term_chs = fbs.n_term_chs;        // Set number of remote term chars

    MemCpyFip(&FIP_RSP_MSG->msg.rsp_pkt,                        // Copy msg packet to uFIP
            fbs.rsp_pkt, fbs.rsp_msg_len);

    FIP_MSCOT_P = fbs.rsp_msg_len + 8;                          // Write msg length into microfip
    FIP_CONFA_P = 0x08;                                         // Enable transmit channel

    fip.stats.msg_sent++;                                       // Update statistics

}



void FipCheckMsgTx(void)
{
    /*--- Check for message transmission timeout ---*/

    if (fbs.send_pkt && !Test(FipMsgsa(),0x02))                 // If pkt being sent, and uFIP TX buf is still full
    {
        if (++fip.send_msg_timer > fip.stats.max_msg_send_delay)// If tx delay greater than max
        {
            fip.stats.max_msg_send_delay = fip.send_msg_timer;  // update max delay
        }

        if (fip.send_msg_timer > FIP_SEND_MSG_TIMEOUT)          // If TX timeout has expired
        {
            FIP_CONFA_P = 0x04;                 // Disable transmit channel
            fip.start_tx = true;                // Flag that EMP_MST will say FULL
            fip.stats.msg_send_timeout++;       // Increment statistics

            if (fcm.stat == FGC_EXECUTING &&                     // If command is executing and
                    fbs.send_pkt == FBS_CMD_PKT)                 // cmd rsp pkt is waiting to be sent
            {
                FbsCancelCmd(FGC_RSP_TX_FAILED);                // Cancel command response
            }

            fbs.send_pkt = 0;                                   // Clear send packet flags
        }
    }
}



uint8_t FipMsgsa(void)
{
    uint8_t msgsa = FIP_MSGSA_P;          // Read status register from uFIP

    if (Test(msgsa,0x20))               // Check for reception errors
    {
        fip.stats.err_art++;
    }

    if ((msgsa & 0x11) == 0x11)         // Check for ignored messages
    {
        fip.stats.msg_rcv_ignored++;
    }

    if ((msgsa & 0x11) == 0x10)         // Check for buffer overflow
    {
        fip.stats.msg_rcv_too_long++;
    }

    return (msgsa);
}



void FipISR(void)
{
    uint8_t               fip_msg;
    uint8_t               fip_irq;
    uint16_t              payload;
    uint16_t              runlog_idx;
    uint16_t              data;
#if FGC_CLASS_ID != 59
    union TUnion32Bits  tmp_fp32;
#endif

    fip_irq = FIP_IRQSA_P;                                      // read FIP IRQ reg to ack (clear) the irq

    if ( ( fip_irq & 0x80 ) != 0 )                              // If Var 7 is accessible...
    {
        if ( ( FIP_MPSSA_P & 0x80 ) != 0 )                      // If Var 7 is accessible...
        {
            // Clear CONFB bit 7 to lock Var 7
            FIP_CONFB_P &= 0x7F;                                // Disable FIP access to Var 7

// in the Boot is done like this
//          FIP_CONFD_P = (7 << 4) | (FIP_CONFD_P & 0x8F);      // Identify Var 7 access to uFip
            FIP_CONFD_P |= 0x70;                                // Identify Var 7 access to uFip

            // Check type of variable received
            if ( FIP_VIDGL_P != 0 )                             // Test FIP variable address (low byte) (0=Time 4=Code)
            {
                // Reset Variable 7 index to receive time variable next
                FIP_VIDGL_P = 0x00;                             // Reactivate Time variable reception (ID=0x3000)

                data = *((uint8_t*)FIP_RTDATA0_32);               // Get RD var header byte
                if (data != fip.rd_header)                      // Check against last header
                {

                    fip.rd_header = data;                       // Save new header byte
                    fip.rd_rcvd_f = data;                       // Set RD rcvd flag (D cannot be zero)
#if FGC_CLASS_ID == 59
                    // Prepare to copy to wval[] array from the RT, skip index 0 and copy 15 floats (= 60 bytes)
                    MemCpyFip( (const void *) &dpcls.mcu.wval[1], (const void *)(FIP_RTDATA0_32+4), 60);
#else
                    tmp_fp32.part.hi = *((uint16_t*)(FIP_RTDATA0_32 + fip.rt_ref_offset));            // Store in dpram
                    tmp_fp32.part.lo = *((uint16_t*)(FIP_RTDATA2_32 + fip.rt_ref_offset));            // Store in dpram
                    dpcls.mcu.ref.rt_data = tmp_fp32.fp32;

                    dpcls.mcu.ref.rt_data_f = true;        // Flag to DSP that new ref data is waiting
#endif
                    fbs.u.fieldbus_stat.ack |= FGC_ACK_RD_PKT;  // Set RD rcvd flag in status variable
                }
            }
            else // Time variable
            {
                runlog_idx = *((uint16_t*)(FIP_TIMEVAR_32 + fip.runlog_idx_offset));
                if ( runlog_idx == fbs.last_runlog_idx )        // if fbs.time_v.runlog_idx == fbs.last_runlog_idx
                {
                    fip.stats.v7_repeated++;                    // fip.stats.v7_repeated++
                    if (fip.stats.v7_repeated == 0)
                    {
                        // ToDo the code that follows was duplicated just to honour this case, can be done differently?
                    }
                    else
                    {
                        fbs.last_runlog_idx = runlog_idx;       // fbs.last_runlog_idx = fbs.time_v.runlog_idx

                        FIP_VIDGL_P = fip.rt_ref_vidgl;         // Change var 7 global index to receive RT data variable

                        MemCpyFip( (void *) &(fbs.time_v), (const void *) FIP_TIMEVAR_32, FIP_TIMEVAR_B);

                        fbs.time_rcvd_f = 1;                    // Set time rcvd flag (D cannot be zero)
                        fbs.events_rcvd_f = 1;                  // Set events rcvd flag (D cannot be zero)
                        // D = GPT_TIC1 = GPT input capture 1 latch = time of IRQ
                        fbs.sync.tic0 = GPT_TIC1_16;                 // Save in global variable
                        fbs.sync.tic0_f = true;                      // Set time received flag (cleared in PllSync())
                        fbs.u.fieldbus_stat.ack |= FGC_ACK_TIME;// Acknowledge time_var reception

                        dpcom.mcu.time.fbs.unix_time =         fbs.time_v.unix_time;        // dpcom.mcu.time.fbs.unix_time = fbs.time_v.unix_time
                        dpcom.mcu.time.fbs.ms_time   = (uint32_t)fbs.time_v.ms_time;          // dpcom.mcu.time.fbs.ms_time = fbs.time_v.ms_time
                    }
                }
                else
                {
                    fbs.last_runlog_idx = runlog_idx;           // fbs.last_runlog_idx = fbs.time_v.runlog_idx

                    FIP_VIDGL_P = fip.rt_ref_vidgl;             // Change var 7 global index to receive RT data variable

                    MemCpyFip( (void *) &(fbs.time_v), (const void *) FIP_TIMEVAR_32, FIP_TIMEVAR_B);

                    fbs.time_rcvd_f = 1;                        // Set time rcvd flag (D cannot be zero)
                    fbs.events_rcvd_f = 1;                      // Set events rcvd flag (D cannot be zero)
                    // D = GPT_TIC1 = GPT input capture 1 latch = time of IRQ
                    fbs.sync.tic0 = GPT_TIC1_16;                     // Save in global variable
                    fbs.sync.tic0_f = true;                          // Set time received flag (cleared in PllSync())

                    fbs.u.fieldbus_stat.ack |= FGC_ACK_TIME;    // Acknowledge time_var reception

                    dpcom.mcu.time.fbs.unix_time =         fbs.time_v.unix_time;    // dpcom.mcu.time.fbs.unix_time = fbs.time_v.unix_time
                    dpcom.mcu.time.fbs.ms_time   = (uint32_t)fbs.time_v.ms_time;      // dpcom.mcu.time.fbs.ms_time = fbs.time_v.ms_time
                }
            }
            FIP_CONFB_P |= 0x80;                               // Set bit 7 to unlock Var 7
        }
        else        // if Var 7 is not accessible
        {
            fip.stats.v7_no_access++;                           // Increment no access stats
        }
    }
    else
    {
        // Message received - pass to FbsTsk
        fip_msg = FipMsgsa();                                   // Call FipMsgsa() to test msg status (returned in B)

        if ((fip_msg & 0x01) != 0 )                             // Test bit 0 (rdy_msg = 1 if valid msg is waiting in buffer)
        {
            payload = FIP_MSCOR_P - 8;                          // Sub 8 from Message length to get payload length (1-120)
            if (payload <= 0 )                                  // if message is too short (no payload)
            {
                fip.stats.msg_too_short++;                      // Increment statistics counter for short messages
                FIP_CONFA_P = 0x01;                             // Disable message reception...
                FIP_CONFA_P = 0x03;                             // Enable message reception...
            }
            else
            {
                fbs.pkt_len = payload;                          // Save payload length for FbsTsk() to use

                // Extract header word (flags:user) from FIP msg
                fbs.rcvd_header = *FIP_MSG_HDR;                 // Save header word for FbsTsk()

                OSTskResume(TSK_FBS);
            }
        }
        else
        {
            // no valid message is waiting
            FIP_CONFA_P = 0x01;                                 // Disable message reception...
            FIP_CONFA_P = 0x03;                                 // Enable message reception...
        }
    }
}



void FipCpyCmd(void * to, uint16_t length)
{
    // Copy command packet into packet buffer

    MemCpyFip(to, &(FIP_CMD_MSG->msg.cmd_pkt), length);

    FIP_CONFA_P = 0x01;                         // Disable message reception
    FIP_CONFA_P = 0x03;                         // Enable message reception
}

//EOF
