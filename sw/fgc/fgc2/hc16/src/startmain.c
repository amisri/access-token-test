/*---------------------------------------------------------------------------------------------------------*\
  File:         startmain.c

  Purpose:      FGC V2 HC16 Library - function to set up C context and run main()

  Author:       Quentin.King@cern.ch,

  Notes:        This file contains the function StartMain(), which will be called from the _Startup()
                function of the boot and main HC16 programs.  It is based on the function provided
                by Hiware and is responsible for preparing the critical HC16 registers and the
                C environment.

 History:

    04/06/04    qak     Created based on Startup from boot program
\*---------------------------------------------------------------------------------------------------------*/

#include <start16.h>                                    // Hiware linker structure declaration

#include <startmain.h>
#include <memmap_mcu.h>

extern struct _tagStartup _startupData;

/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void StartMain(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare the HC16 C context and call main().  It is jumped to from ResetPeripherals().
  It cannot be called because the stack is not set up until this function executes.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        /* Zero out global/static variables */

    Setup:
        LDAB    @_startupData:PAGE              // load page of _startupData
        TBEK                                    // into EK
        TST     _startupData.flags              // Test startup data flags
        BNE     ZeroOut                         // Skip Stack setup if flag is set
        LDAB    _startupData.stackOffset        // Get Stack page
        LDS     _startupData.stackOffset:1      // and initialise stack pointer
        TBSK                                    // and stack page register
        TBZK                                    // and ZK page register

// ---------------------------------------
// debugging
// fill the 50 (Boot) with CAFE

        LDY     #STACK_16                       // IY = bottom of stack
        LDD     #STACK_W                        // stack size in words
        LDE     #0xCAFE

    loop:                                       // Loop to fill stack with this recognisable pattern
        STE     0,Y
        AIY     #2
        ADDD    #-1
        BNE     loop                            // On exit, IY points to bottom of new task's stack frame
// ---------------------------------------

    ZeroOut:
        LDAB    _startupData.pZeroOut           // Get pointer to first descriptor
        LDY     _startupData.pZeroOut:1         // and load into IY
        TBYK                                    // and YK
        LDE     _startupData.nofZeroOuts        // Get 'nofZeroOuts'
        BEQ     CopyDown                        // Branch when all zero outs are done
    NextZeroOut:
        LDAB    0,Y                             // Get start address of block to clear
        LDX     1,Y                             // Into XK:IX
        TBXK
        LDD     3,Y                             // Get zero out byte count
        AIY     #5                              // Set ptr to next descriptor
        BITB    #1
        BEQ     TestForZero                     // Test of odd number of bytes
        CLR     0,X                             // ODD: clear one byte
        AIX     #1
        SUBD    #1
    TestForZero:                                // Even number of bytes remain
        TSTD                                    // Test for zero
        BEQ     TestNextBlock                   // Jump if zero
    NextWord:
        CLRW    0,X                             // Clear word
        AIX     #2                              // Inc pointer
        SUBD    #2                              // Decrement byte count
        BNE     NextWord
    TestNextBlock:
        SUBE    #1                              // Decrement 'nofZeroOuts'
        BNE     NextZeroOut                     // Jump back if more zero out segments remain

        /* Copy initialization data */

    CopyDown:
        LDAB    _startupData.toCopyDownBeg      // Get adress of first descriptor
        LDY     _startupData.toCopyDownBeg:1    // into YK:IY
        TBYK
    NextBlock:
        LDE     0,Y                             // Load copy byte count
        BEQ     CallMain                        // Zero terminates copy down list
        LDAB    3,Y                             // Get destination address
        LDX     4,Y                             // into XK:IX
        TBXK
        AIY     #6                              // Data is following immediately!
    Copy:
        SUBE    #2                              // Decrement byte counter
        BMI     CopyByte                        // If < 0 --> 1 byte left
        LDD     0,Y
        STD     0,X
        AIX     #2                              // Increment addresses
        AIY     #2
        TSTE
        BNE     Copy                            // Not finished yet
        BRA     NextBlock                       // Copy next block
    CopyByte:
        LDAB    0,Y                             // Copy last byte
        STAB    0,X
        AIY     #2                              // Set Y to next counter
        BRA     NextBlock                       // Copy next block

        /* Set up page registers before calling main() */

    CallMain:
        LDX     _startupData.main               // load address of main() function
        LDE     _startupData.main:2             // into X:E
        ADDE    #2                              // Adjust E for HC16 pipeline
        PSHM    E,X                             // push X:E to become PK:PC so RTS calls main()

        LDAB    _startupData.dataPage           // Get default data page
        TBEK                                    // and set EK,
        TBXK                                    // XK
        TBYK                                    // and YK (SK & ZK were already set above)
    }                                           // return is call to main
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: startmain.c
\*---------------------------------------------------------------------------------------------------------*/

