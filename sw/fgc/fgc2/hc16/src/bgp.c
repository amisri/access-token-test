/*!
 * @file   bgp.c
 *
 * @brief  FGC MCU Software - Background processing Task
 *
 * The background task takes the role of an idle task. It never blocks and is used to do
 * background monitoring of the FGC.
 */

#include <stdint.h>

#include <bgp.h>
#include <os.h>
#include <tsk.h>
#include <memmap_mcu.h>
#include <log_class.h>


/*!
 * Checks the consumption of the task stack
 *
 * The results (in percents) are stored in shared memory registers
 * which are recorded in the runlog if the system crashes.
 * They can also be seen through the property FGC.DEBUG.STACKUSE.
 *
 * @param[in]  tsk_id    ID of the task
 * @param[in]  stk       Pointer to the task stack
 * @param[in]  stk_size  Size of the stack
 */
static void BgpCheckStack(const int16_t tsk_id, const OS_STK * stk, const int16_t stk_size)
{
    const int16_t empty_value = 0xCAF0 + tsk_id;
    int16_t n;

    // Search for the first non-empty value on the stack

    for(n = 0; n < stk_size && *(stk++) == empty_value; n++);

    SM_STKUSE_A[tsk_id] = 100 - ((n * 100) / stk_size);
}



#pragma MESSAGE DISABLE C5703    // Disable 'parameter declared but not referenced' warning

void BgpTsk(void * unused)
{
    for(;;)
    {
        BgpCheckStack(0, tsk_stk.mst, STK_SIZE_MST);    // 0. MstTsk - Millisecond task
        BgpCheckStack(1, tsk_stk.fbs, STK_SIZE_FBS);    // 1. FbsTsk - Fieldbus task
        BgpCheckStack(2, tsk_stk.sta, STK_SIZE_STA);    // 2. StaTsk - State machine task
        BgpCheckStack(3, tsk_stk.fcm, STK_SIZE_FCM);    // 3. FcmTsk - Fieldbus commands task
        BgpCheckStack(4, tsk_stk.tcm, STK_SIZE_TCM);    // 4. TcmTsk - Terminal commands task
        BgpCheckStack(5, tsk_stk.dls, STK_SIZE_DLS);    // 5. DlsTsk - Dallas bus task
        BgpCheckStack(6, tsk_stk.rtd, STK_SIZE_RTD);    // 6. RtdTsk - Real-time display task
        BgpCheckStack(7, tsk_stk.trm, STK_SIZE_TRM);    // 7. TrmTsk - Terminal communications task
        BgpCheckStack(8, tsk_stk.bgp, STK_SIZE_BGP);    // 8. BgpTsk - Background processing task

        logFgcLoggerProcess();
    }
}

#pragma MESSAGE DEFAULT C5703

// EOF
