
/*---------------------------------------------------------------------------------------------------------*\
  File:         trap.c

  Purpose:      FGC2 HC16 Library - Trap related interrupt functions

  Author:       Quentin.King@cern.ch

  Notes:

 History:

    05/11/04    qak     Assembled from various places
\*---------------------------------------------------------------------------------------------------------*/


#define TRAP_GLOBALS            // define buserr_chk global variable

#include <trap.h>

#include <stdbool.h>
#include <memmap_mcu.h>         // Include memory map consts
#include <mcu_dependent.h>      // for MSLEEP(), NVRAM_UNLOCK(), NVRAM_LOCK(), ENTER_SR(), EXIT_SR
#include <iodefines.h>          // specific processor registers and constants
#include <fgc_runlog_entries.h> // Include run log definition, for FGC_RL_
#include <runlog_lib.h>         // for struct TRunlog, RunlogInit(), RunlogWrite
#include <core.h>               // for CoreDump()
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <registers.h>
#include <mem.h>                // for MemCpyBytes(), MemCpyWords()
#include <start16.h>
#include <tsk.h>                // Main program task numbers

/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT
/*---------------------------------------------------------------------------------------------------------*/
void IsrBusError(void)
/*---------------------------------------------------------------------------------------------------------*\
  This ISR will be called in response to a bus error.  This may be expected or unexpected - if a program
  is testing a register and must tolerate a bus error, it should first set buserr_chk to 1 (or, set buserr_chk
  to N to tolerate N bus errors. If a bus error then occurs, it will be decremented and execution will
  continue, otherwise execution is terminated in the trap.  The function does not assume that the stack is
  valid and saves EK in A in order to set EK to the default data page so that it can access buserr_chk.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        TEKB                                    // B = EK
        LDAA    @buserr_chk:PAGE                // A = default data page (DDP)
        XGAB                                    // A = EK  B = DDP
        TBEK                                    // EK = B = DDP to access buserr_chk
        TSTW    buserr_chk                      // Check if bus error testing enabled (!= 0)
        BEQ     continue_to_trap                // If not then go to trap

        DECW    buserr_chk                      // Decrement bus error check flag to indicate bus error happened
        XGAB                                    // B = EK
        TBEK                                    // EK is restored
        RTI                                     // Return from interrupt

    continue_to_trap:
        XGAB                                    // B = EK
        TBEK                                    // EK is restored
        LDD     #0x05                           // Set exception ID to indicate a bus error
        JMP     IsrTrap                         // Continue in IsrTrap
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT
/*---------------------------------------------------------------------------------------------------------*/
void IsrTrap(void)
/*---------------------------------------------------------------------------------------------------------*\
  IsrTrap will be called for all unexpected interrupts.  The interrupt jump function will set the
  exception ID in the register D so that it can be stored along with all the other registers in the run
  log.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        /* Save E, EK and Exception ID in register AM */

        ORP     #0x00E0                         // Disable all further maskable interrupts
        TBA                                     // Save Exception ID in A
        TEKB                                    // Save EK in B
        TEDM                                    // Save E:D in AM

        /* Reconfigure sensitive HC16 registers that may have been corrupted */

        LDAB    #0x0F                           // Set EK to page F
        TBEK                                    // to access Internal HC16 registers

        LDAA    #0x04                           // SYPCR: watch dog disabled, BM enabled 64 clk
        STAA    SIM_SYPCR_16

        LDE     #0x7F00                         // SYNCR: syncr w=0, x=1 16.0 mhz
        STE     SIM_SYNCR_16

        LDE     #0x40CF                         // SIMCR: mcr frzsw enable, frzmb enable, MM at ffff
        STE     SIM_MCR_16

        LDAA    #0xE0                           // Set Port GP bits 7, 6, 5 as outputs to 0
        STAA    GPT_PDDR_16                     // These are the WD, analog and digital triggers

        LDE     #0x0003                         // CSPAR0: CSBoot: 16-bit  CS0-5: discrete outputs
        STE     SIM_CSPAR0_16
        CLRW    SIM_CSPAR1_16                   // CSPAR1: CS6-10: discrete outputs
        CLRW    SIM_CSORBT_16                   // Disable CSBoot
        CLRW    SIM_CSOR0_16                    // Disable CS0
        CLRW    SIM_CSOR1_16                    // Disable CS1
        CLRW    SIM_CSOR2_16                    // Disable CS2
        CLRW    SIM_CSOR3_16                    // Disable CS3
        CLRW    SIM_CSOR4_16                    // Disable CS4
        CLRW    SIM_CSOR5_16                    // Disable CS5
        CLRW    SIM_CSOR6_16                    // Disable CS6
        CLRW    SIM_CSOR7_16                    // Disable CS7
        CLRW    SIM_CSOR8_16                    // Disable CS8
        CLRW    SIM_CSOR9_16                    // Disable CS9
        CLRW    SIM_CSOR10_16                   // Disable CS10

        /* Reset C32 and switch off C62 */

        LDAB    #0x00                           // Set EK to page 0
        TBEK                                    // to access Internal FGC registers and variables
        LDD     #(CPU_RESET_CTRL_C62OFF_MASK16+CPU_RESET_CTRL_DSP_MASK16)
        STD     CPU_RESET_16                    // Set RESET register to reset C32 and switch off C62

        /* Save CPU_MODE register */

        LDD     CPU_MODE_16                     // Read MODE register
        STD     TRAP_MODE_16                    // Save in SM area

        /* Save SK:SP in temp storage in SM zone */

        CLRA                                    // Clear high byte of D
        TSKB                                    // Set B = D = SK
        STD     TRAP_SKSP_16                    // Save SK
        STS     TRAP_SKSP_16+2                  // Save SP

        /* Save XK:IX, YK:IY and ZK:IZ in temp storage in SM zone */

        TXKB                                    // Set B = D = XK
        STD     TRAP_XKIX_16                    // Save XK
        STX     TRAP_XKIX_16+2                  // Save IX
        TYKB                                    // Set B = D = YK
        STD     TRAP_YKIY_16                    // Save YK
        STY     TRAP_YKIY_16+2                  // Save IY
        TZKB                                    // Set B = D = ZK
        STD     TRAP_ZKIZ_16                    // Save ZK
        STZ     TRAP_ZKIZ_16+2                  // Save IZ

        /* Save exception ID, EK and E in temp storage in SM zone */

        TMXED                                   // Recover E & D from AM where D = EX_ID:EK
        CLRA                                    // Clear high byte of D so that D = EK
        STD     TRAP_EK_16                      // Save EK
        TMXED                                   // Recover E & D from AM where D = EX_ID:EK
        XGAB                                    // Switch A (EX_ID) and B (EK)
        CLRA                                    // Clear high byte of D so that D = EX_ID
        STD     TRAP_EXID_16                    // Save exception ID
        STE     TRAP_E_16                               // Save E

        /* Check if stack pointer is still valid */

        TEKB                                    // B = default data page
        CMPB    TRAP_SKSP_16+1                  // Compare B with saved copy of SK
        BNE     invalid_sp                      // Branch if not the same
        LDD     TRAP_SKSP_16+2                  // D = saved copy of SP
        CPD     #STACK_16                       // Compare with the start of the stack area
        BCS     invalid_sp                      // Branch if SP if before the variables area
        CPD     #DPRAM_16                       // Compare with end of the variables area
        BCC     invalid_sp                      // Branch if SP is after the variables area

        /* SP is valid so recover CCR and PK:PC from stack */

        TSX                                     // X = SKSP+2
        STD     -2,X                            // Write SP onto the current stack location
        PULM    D,E                             // E = CCR:PK, D = PC
        SUBD    #6                              // PC -=6
        SBCE    #0                              // Subtract carry from PK
        STD     TRAP_PKPC_16+2                  // Save PC
        STE     TRAP_CCR_16                     // Save CCR:PK
        ANDE    #0x000F                         // Mask out CCR to just leave PK
        STE     TRAP_PKPC_16                    // Save PK
        BRA     init_frame

        /* SP is invalid to clear CCR and PK:PC in temp storage in SM zone */

    invalid_sp:

        CLRW    TRAP_PKPC_16                    // Clear PK
        CLRW    TRAP_PKPC_16+2                  // Clear PC
        CLRW    TRAP_CCR_16                     // Clear CCR

        /* Reinitialise C stack frame so that calls to C functions can be made */

    init_frame:

        LDAB    @_startupData:PAGE              // load page of _startupData
        TBEK                                    // into EK
        LDAB    _startupData.stackOffset        // Get Stack page
        LDS     _startupData.stackOffset:1      // and initialise stack pointer
        TSZ                                     // and frame pointer
        TBSK                                    // and stack page register
        TBZK                                    // and ZK page register
        LDAB    _startupData.dataPage           // Get default data page
        TBEK                                    // and set EK,
        TBXK                                    // XK
        TBYK                                    // and YK
    }

    /*--- Check if run log has been initialised ---*/

    if(!IS_RL_READY())                          // If run log not ready
    {
        RunlogInit();                                   // Initialise run log
    }

    /*--- Write trap dump to run log ---*/

    RunlogWrite(FGC_RL_EXID,    (void*)TRAP_EXID_16);           // Exception ID
    RunlogWrite(FGC_RL_PWRTIME, (void*)SM_PWRTIME_16);          // Power up time
    RunlogWrite(FGC_RL_RUNTIME, (void*)SM_RUNTIME_16);          // Run time
    RunlogWrite(FGC_RL_UXTIME,  (void*)SM_UXTIME_16);           // Unix time
    RunlogWrite(FGC_RL_MSTIME,  (void*)SM_MSTIME_16);           // Millisecond time
    RunlogWrite(FGC_RL_PKPC,    (void*)TRAP_PKPC_16);           // PKPC
    RunlogWrite(FGC_RL_CCR,     (void*)TRAP_CCR_16);            // CCR
    RunlogWrite(FGC_RL_SKSP,    (void*)TRAP_SKSP_16);           // SKSP
    RunlogWrite(FGC_RL_XKIX,    (void*)TRAP_XKIX_16);           // XKIX
    RunlogWrite(FGC_RL_YKIY,    (void*)TRAP_YKIY_16);           // YKIY
    RunlogWrite(FGC_RL_ZKIZ,    (void*)TRAP_ZKIZ_16);           // ZKIZ
    RunlogWrite(FGC_RL_EK,              (void*)TRAP_EK_16);             // EK
    RunlogWrite(FGC_RL_E,               (void*)TRAP_E_16);              // E
    RunlogWrite(FGC_RL_MODE_REG,        (void*)TRAP_MODE_16);           // Log mode register

    /*--- Write stack usage if running main program ---*/

    if(TST_CPU_MODEL(RUNMAIN))
    {
        TrapTaskStackUsage(TSK_MST, (uint8_t*)SM_STKUSE_MST_16);  // Millisecond task stack usage
        TrapTaskStackUsage(TSK_FBS, (uint8_t*)SM_STKUSE_FBS_16);  // Fieldbus task stack usage
        TrapTaskStackUsage(TSK_STA, (uint8_t*)SM_STKUSE_STA_16);  // State task stack usage
        TrapTaskStackUsage(TSK_FCM, (uint8_t*)SM_STKUSE_FCM_16);  // Fieldbus command task stack usage
        TrapTaskStackUsage(TSK_TCM, (uint8_t*)SM_STKUSE_SCM_16);  // Terminal command task stack usage
        TrapTaskStackUsage(TSK_DLS, (uint8_t*)SM_STKUSE_DLS_16);  // Dallas task stack usage
        TrapTaskStackUsage(TSK_RTD, (uint8_t*)SM_STKUSE_RTD_16);  // Real-time display task stack usage
        TrapTaskStackUsage(TSK_TRM, (uint8_t*)SM_STKUSE_TRM_16);  // Terminal interface task stack usage
        TrapTaskStackUsage(TSK_BGP, (uint8_t*)SM_STKUSE_BGP_16);  // Background processing task stack usage
    }

    /*--- Write conditional run log records ---*/

    if(SM_TSKID_P < 16)
    {
        RunlogWrite(FGC_RL_TSK_ID,      (void*)SM_TSKID_16);            // NanOS Task ID
    }

    if(SM_ISRMASK_P)
    {
        RunlogWrite(FGC_RL_ISR_ID,      (void*)SM_ISRMASK_16);          // NanOS ISR ID
    }

    /*--- Stack dump if SP was valid (i.e. if PKPC was recoverable) ---*/

    NVRAM_UNLOCK();
    ENTER_SR();

    if(TRAP_PKPC_P)                                                     // If stack pointer was valid
    {
        MemCpyWords(NVRAM_CORESTACK_32,*((uint32_t*)TRAP_SKSP_16),NVRAM_CORESTACK_W);     // Save stack
    }
    else                                                                // else SP invalid
    {
        MemSetWords(NVRAM_CORESTACK_32,0,NVRAM_CORESTACK_W);                    // Clear stack dump
    }

    EXIT_SR();
    NVRAM_LOCK();

    /*--- Core dump ---*/

    CoreDump();

    /*--- Panic with one code for bus error and another for all other exceptions ---*/

    switch(TRAP_EXID_P)
    {
    case 0x04:  Panic(PANIC_BRKPNT);            // Breakpoint
    case 0x05:  Panic(PANIC_BUS_ERR);           // Bus error
    case 0x07:  Panic(PANIC_ILEGAL_INS);        // Illegal instruction
    default:    Panic(PANIC_OTHER);             // Other traps
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TrapTaskStackUsage(uint16_t tid, uint8_t *usage)
/*---------------------------------------------------------------------------------------------------------*\
  This function will save an entry in the runlog that records the task ID and the task stack.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      tid_usage;

    tid_usage = (tid << 8) | *usage;

    RunlogWrite(FGC_RL_STKUSE,(void*)&tid_usage);
}
/*---------------------------------------------------------------------------------------------------------*/
void Panic(uint16_t leds)
/*---------------------------------------------------------------------------------------------------------*\
  This function will try to use the front panel LEDs to flash a code that will indicate the reason for
  the panic.  The function never exits - it just waits for a watchdog or human to reset the system.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;

    /*--- Stop interrupts and halt C32 and C62 ---*/

    OS_ENTER_CRITICAL();
    CPU_RESET_P |= (CPU_RESET_CTRL_C62OFF_MASK16|CPU_RESET_CTRL_DSP_MASK16);

    /*--- Flash front panel LEDs if possible and wait for a reset ---*/

    if(RegWrite(RGLEDS_16,0,true))              // If LEDs register is not available
    {
        asm WAI;                                        // Just stop and wait for the watchdog!
    }

    for(;;)                                     // Loop while waiting for watchdog
    {
        RGLEDS_P = leds;                                // Set pattern

        for(i=0;i < 0xFFFFu;i++);                       // Delay

        RGLEDS_P = 0;                                   // Set all black

        for(i=0;i < 0xFFFFu;i++);                       // Delay
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: trap.c
\*---------------------------------------------------------------------------------------------------------*/

