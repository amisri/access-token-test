/*---------------------------------------------------------------------------------------------------------*\
  File:     cmd.c

  Purpose:  FGC MCU Software - Command functions

  Author:   Quentin.King@cern.ch
\*---------------------------------------------------------------------------------------------------------*/


#define CMD_GLOBALS             // define fcm, tcm, pcm global variables
#define DEFPROPS_INC_ALL    // defprops.h

#include <cmd.h>                // for tcm, fcm global variable, struct cmd, PPM_ALL_USERS
#include <class.h>          // Include class dependent definitions
#include <hash.h>               // for HASH_VALUE and struct hash_entry needed by defprops.h

#include <defprops.h>
#include <definfo.h>

#include <stdbool.h>
#include <stdio.h>              // for fputc(), fprintf(), fputs()
#include <dev.h>                // for dev global variable
#include <fbs.h>                // for fbs global variable, FbsOutLong()
#include <mst.h>                // for mst global variable
#include <definfo.h>            // for FGC_PLATFORM_ID
#include <dpcom.h>              // for dpcom global variable, struct abs_time_us
#include <dpcls.h>              // for dpcls global variable
#include <macros.h>             // for Test(), Set(), Clr()
#include <sta.h>                // for sta global variable
#include <fgc_errs.h>           // for FGC error codes
#include <os.h>                 // for OSTskSuspend()
#include <prop.h>               // for PropValueGet()
#include <log.h>                // for EVT_LOG_RESET_PROP, LogEvtSaveValue(), LogEvtCmd()
#include <get.h>                // for GetPropSize()
#include <memmap_mcu.h>         // for SM_UXTIME_P
#include <property.h>           // for DEFPROPS_FUNC_GET prototype and typedef prop_size_t
#include <shared_memory.h>      // for shared_mem global variable
#include <init_class.h>
#include <mcu_dsp_common.h>

// Static function declarations

static  uint16_t  CmdSet                  (struct cmd *c);
static  uint16_t  CmdGet                  (struct cmd *c);
static  void    CmdPrintRange           (struct cmd *c, struct prop *p);
static  void    CmdPrintIdxFormatted    (struct cmd *c, uint16_t idx);
static  uint16_t  CmdGetChangedPropsHelper(const struct prop * p);

/*---------------------------------------------------------------------------------------------------------*/
void CmdTsk(void *init_par)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used for the fieldbus AND serial Command tasks: FcmTsk and TcmTsk. FcmTsk is triggered
  by a semaphore from the Fbs task and TcmTsk is triggered by a semaphore from the terminal task. The tasks
  are triggered when a command packet is ready for processing.

  Notes on "Force last packet": The FGC sends command responses in packets to the gateway. In the header
  flags for each packet there are two bits indicating if it is a first and/or last packet. It is essential
  that the final packet of the response has the last packet bit set. To do this, this function adds a
  trailing ';' character to ensure that a packet will be produced as the output from the get function
  might exactly fill a packet that might already be sent without the last packet bit set.  By adding one
  character, it is guaranteed that a final packet with the last packet bit set will be produced to end the
  command response.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct cmd * c = init_par;

    // Main loop - process each command

    for(;;)
    {
        c->mux_idx     = 0;
        c->n_elements  = 0;
        c->cached_prop = NULL;
        c->new_cmd_f   = true;
        c->end_cmd_f   = false;
        c->write_nvs_f = true;

        c->evt_log_state = EVT_LOG_RESET_PROP;

        c->errnum = PropIdentifyProperty(c);

        // If property identified successfully

        if(c->errnum == 0)
        {
            switch(c->cmd_type)
            {
                case FGC_FIELDBUS_FLAGS_SET_CMD:

                    // SET - From fieldbus or serial

                    c->errnum = CmdSet(c);
                    break;

                case FGC_FIELDBUS_FLAGS_GET_CMD:

                    // GET - From fieldbus or serial

                    c->errnum = CmdGet(c);
                    break;

                default:

                    c->errnum = FGC_NOT_IMPL;
                    break;
            }
        }

        // Cancel packet unless CMD_RESTARTED error reported

        if(c->errnum != FGC_CMD_RESTARTED)
        {
            c->pars_buf->pkt[c->pkt_buf_idx].n_chars = 0;
        }

        dev.cmdrcvd++;

        // If fieldbus command

        if(c == &fcm)
        {
            if(fcm.errnum == FGC_OK_NO_RSP)
            {
                // If no response or still receiving, set stat to end cmd

                if(fbs.cmd.rsp_len == 0 && fcm.stat != FGC_RECEIVING)
                {
                    fcm.stat = FGC_OK_NO_RSP;
                }
                else    // Response was generated
                {
                    OS_ENTER_CRITICAL();

                    // Set last rsp packet flag

                    Set(fbs.cmd.header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT);

                    // Force last pkt (see notes above)

                    fcm.store_cb(';');

                    OS_EXIT_CRITICAL();
                }
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static uint16_t CmdSet(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from CmdTsk() if a set command has been received.  It will run the appropriate
  set function if permitted.  Set commands are logged in the event log.
  The function PropIdentifyUser() will only allow a non-zero p->mux_idx if the property is either
  SUB_DEV or PPM.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t          errnum          = 0;
    uint16_t          ms_time;
    struct prop *   p               = c->prop;

    OS_ENTER_CRITICAL();            // Take event log timestamp atomically
    c->evt_rec.timestamp.unix_time = SM_UXTIME_P;
    ms_time                        = SM_MSTIME_P;

    OS_EXIT_CRITICAL();

    c->evt_rec.timestamp.us_time   = (uint32_t)ms_time * 1000L;   // Convert milliseconds to microseconds

    if(!p->set_func_idx)                    // If Set Function is not defined for this property
    {
        errnum = FGC_SET_NOT_PERM;          // Report that set is not permitted
    }
    else if(p->setif_func_idx)              // If SetIf function is defined
    {
        errnum = SETIF_FUNC[p->setif_func_idx](c);  // Call SetIf function
    }

    if(!errnum &&                           // if no error yet, and
       Test(p->flags,PF_CONFIG) &&          // property is part of the global configuration, and
       sta.config_mode == FGC_CFG_MODE_SYNC_DB) // config mode is SYNC_DB
    {
        errnum = FGC_SYNC_IN_PROGRESS;      // Report SYNC IN PROGRESS
    }

    c->device_par_err_idx = 0;              // Reset parsing error index
    c->evt_log_state = EVT_LOG_RESET_VALUE; // Prepare to log set value

    if(!errnum)                             // If no error so FAR
    {
        c->n_pars           = 0;            // Prepare to parse parameters
        c->pars_buf->n_pars = 0;
        c->last_par_f       = false;
        c->changed_f        = false;

        OSSemPend(dev.set_lock);            // Pend on set lock semaphore
        errnum = SET_FUNC[p->set_func_idx](c, p);   // Call SET function
        OSSemPost(dev.set_lock);            // Post set lock semaphore
    }
    else
    {
        LogEvtSaveValue(c);                 // Log value string (up to end of current packet only)
    }

    LogEvtCmd(c, errnum);                   // Log set command

    mst.set_cmd_counter = 5;                // Inhibit autocalibration for 5s

    return(errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
static uint16_t CmdGet(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from CmdTsk() when a get command must be executed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t          errnum = FGC_OK_NO_RSP;
    prop_size_t     from;
    prop_size_t     to;
    uint16_t          getopts;

    struct prop *p = c->prop;

    if(!p->get_func_idx)                    // if Get Function is not defined for this property
    {
        return(FGC_GET_NOT_PERM);           // Report GET NOT PERMITTED
    }

    errnum = PropIdentifyGetOptions(c, p);
    if ( errnum )                           // if get options are not valid
    {
        return(errnum);                     // Report error
    }

    c->timestamp_f = false;                 // Reset timestamp flag so that it will be produced once

    if(Test(c->getopts,GET_OPT_INFO))       // If INFO get option set
    {
        GetPropInfo(c,p);                   // Run PropInfo function on property
    }
    else if(Test(c->getopts,GET_OPT_SIZE))  // else if SIZE get option set
    {
        errnum = GetPropSize(c,p);          // Run PropSize function on property
    }
    else if (Test(c->getopts, GET_OPT_SYNCHED))
    {
        Clr(p->dynflags, DF_CFG_CHANGED);
    }
    else
    {
        if(c->mux_idx == PPM_ALL_USERS)     // If get for all users (only possible if !PARENT)
        {
            Clr(c->getopts,GET_OPT_LBL);    // Suppress labels

            CmdPrintTimestamp(c, p);        // Produce timestamp

            from    = c->from;              // Save array range and flags
            to      = c->to;
            getopts = c->getopts;

            for(c->mux_idx=0 ; c->mux_idx <= dev.max_user ; c->mux_idx++)
            {
                CmdPrintIdxFormatted(c, c->mux_idx);        // Write mux_idx with neat/direct/hex/dec

                c->cached_prop = NULL;      // Clear cached property pointer
                c->from    = from;          // Restore array range and flags
                c->to      = to;
                c->getopts = getopts;

                errnum = GET_FUNC[p->get_func_idx](c, p);
                if ( errnum )                                   // If get function returns an error
                {
                    return(errnum);         // Report the error
                }

                fputc('\n',c->f);           // write newline
            }
        }
        else                                // else get for one user only
        {
            return(GET_FUNC[p->get_func_idx](c,p)); // Call Get function and return errnum
        }
    }

    return(errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CmdNextCh(struct cmd *c, char *ch_p)
/*---------------------------------------------------------------------------------------------------------*\
  This function returns the next character from the command packet, converted to upper case.
  The function is used with fieldbus and terminal cmd structures to extract fieldbus and serial commands.
  It also works with he pcm structure which is used to look up property names.

  If the packet is empty the function sleeps on the command's semaphore, waiting for the controlling task
  (FbsTsk or TrmTsk) to post a semaphore when a new packet is available.  If the command being executed is
  a set command, then the task will post the set lock semaphore before waiting so as not to block a set
  command by the other source (Fieldbus or Terminal).  If the character returned is the last in the last packet of
  the command, the function will set c->end_cmd_f. If a new first packet is received when not expected,
  the function returns FGC_CMD_RESTARTED to end the command that's being received.
\*---------------------------------------------------------------------------------------------------------*/
{
    char                ch;
    uint16_t              idx;
    uint16_t              errnum;
    struct pars_buf *   pars_buf;
    struct cmd_pkt *    cmd_pkt;
    char *              ch_ptr;

    // Process pcm structure

    pars_buf = c->pars_buf;                         // local copy

    // In the case of PCM, the prop_buf pointer is hijacked to hold a char pointer to the property name.

    if ( c == &pcm )                                // If PCM command
    {
        ch_ptr      = (char *)c->prop_buf;
        ch          = *ch_ptr++;
        c->prop_buf = (struct prop_buf *)ch_ptr;

        if(ch >= 'a' && ch <= 'z')                  // Convert lower case to UPPER CASE
        {
            Clr(ch,0x20);
        }

        *ch_p = ch;

        // Set end_cmd_f if nul

        c->end_cmd_f = !(ch == '\0');

        return(0);
    }

    // Process FCM and SCM commands

    if(pars_buf == NULL)
    {
        return(FGC_NULL_ADDRESS);
    }

    do
    {
        cmd_pkt = &pars_buf->pkt[c->pkt_buf_idx];   // Get address of command packet
        idx     = pars_buf->out_idx++;              // Get index of new character in packet

        if(idx < cmd_pkt->n_chars)                  // If packet is NOT empty
        {
            ch = cmd_pkt->buf[idx];                 // Get character

            if(ch >= 'a' && ch <= 'z')              // Convert lower case to UPPER CASE
            {
                Clr(ch,0x20);
            }

            c->new_cmd_f = false;                   // Clear new-cmd-pkt-expected flag
            *ch_p = ch;                             // Return character to calling function
            return(0);                              // return good status
        }

        if(!c->new_cmd_f &&                         // If not starting a new command and
            Test(cmd_pkt->header_flags,FGC_FIELDBUS_FLAGS_LAST_PKT)) // last packet of command
        {
            c->end_cmd_f = true;                    // Set end_cmd_f
            *ch_p = 0;                              // return nul character
            return(0);
        }

        errnum = CmdNextCmdPkt(c);
    }
    while(errnum == 0);            // while next command packet is expected

    return(errnum);                                 // Report unexpected first packet
}
/*---------------------------------------------------------------------------------------------------------*/
// ToDo: the return value is enum fgc_errno
uint16_t CmdNextCmdPkt(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function sleeps on the command's semaphore, waiting for the controlling task (FbsTsk or TrmTsk) to
  post a semaphore when a new packet is available.  If the command being executed is a set command, then the
  task will post the set lock semaphore so as not to block a set command by the other source (Fieldbus or Terminal).
  The function checks if a first-packet is expected and if it is received using the following logic:

                first packet of a command expected
                    No              Yes
            +-----------------------+-----------------------+
            |           |   Ignore new packet.  |
            No  |    Use new packet |    Wait in function   |
  FIRST_CMD_PKT     |           |     for next packet   |
  bit set in        +-----------------------+-----------------------+
  packet header     |         Return    |           |
           Yes  |   FGC_CMD_RESTARTED   |    Use new packet |
            |           |           |
            +-----------------------+-----------------------+
\*---------------------------------------------------------------------------------------------------------*/
{
    struct pars_buf *   pars_buf;
    struct cmd_pkt *    cmd_pkt;

    pars_buf = c->pars_buf;

    /*--- Loop waiting for packet(s) ---*/

    do                                              // Loop waiting for packets
    {
        pars_buf->pkt[c->pkt_buf_idx].header_flags = 0;     // Cancel old packet

        if(c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD)       // If executing a set command
        {
            OSSemPost(dev.set_lock);                // Post set lock semaphore
        }

        OSSemPend(c->sem);                          // Wait on task semaphore for next packet

        if(c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD) // If executing a set command
        {
            OSSemPend(dev.set_lock);                // Pend on set lock semaphore
        }

        c->pkt_buf_idx   ^= 1;                      // Flip to other packet buffer
        pars_buf->out_idx = 0;                      // Reset out_idx to beginning of packet

        cmd_pkt = &pars_buf->pkt[c->pkt_buf_idx];   // Get address of new cmd packet
    }
    while(c->new_cmd_f &&                           // While new command starting and
          !Test(cmd_pkt->header_flags,FGC_FIELDBUS_FLAGS_FIRST_PKT)); // packet does not start a command

    /*--- Check new packet ---*/

    if(!c->new_cmd_f &&                             // If command already running and
    Test(cmd_pkt->header_flags,FGC_FIELDBUS_FLAGS_FIRST_PKT)) // packet starts a command
    {
        return(FGC_CMD_RESTARTED);                  // Report that cmd has been aborted
    }

    return(FGC_OK_NO_RSP);                          // Return good status
}
#pragma MESSAGE DISABLE C1855 // Disable 'recursive function call' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CmdGetChangedProps(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares retrieves the number of configuration properties that have been changed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t tmp  = 0;
    struct prop * p = &PROP_CONFIG_CHANGED;

    tmp =  PropGetNumEls (NULL, p);

    p->n_elements = p->max_elements;

    tmp = CmdGetChangedPropsHelper(p);

    return tmp;
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CmdParentGet(struct cmd *c, struct prop *p, uint16_t filter)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares the get structure based on the to/from/linelen specifiers.  It display
  label and/or range as required.  It returns the number of elements in the selected from-to range
  (may be zero).
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              errnum;
    uint16_t              idx;
    uint16_t              mux_idx;
    prop_size_t         n_els;
    prop_size_t         from;
    prop_size_t         to;
    uint16_t              getopts;
    struct prop *       child_prop;
    DEFPROPS_FUNC_GET(  (*get_func) );             // function pointer declaration

    if(c->abort_f)                                  // If command was aborted (CTRL-C via keyboard or FIP unlock)
    {
        return(FGC_ABORTED);                        // Report ABORTED
    }

    CmdPrintTimestamp(c, p);                        // Produce timestamp (required for Fieldbus commands)

    if(Test(p->flags,   PF_SYM_LST)   &&            // If Parent property with an associated symlist (e.g. CAL, REF)
       Test(c->getopts, GET_OPT_RANGE))             // and RANGE option is present
    {
        CmdPrintRange(c,p);                         // Then, print the associated symlist

        return(FGC_OK_NO_RSP);                      // And return
    }

    from       = c->from;                           // Save array range and flags
    to         = c->to;
    getopts    = c->getopts;
    mux_idx    = c->mux_idx;
    child_prop = p->value;                          // Get address of array of lower level properties

    Set(getopts,GET_OPT_LBL);                       // Enable option labels by default

    n_els = PropGetNumEls(c, p);

    for(idx=0 ; idx < n_els ; idx++, child_prop++ ) // For each child property
    {
        if(!child_prop->get_func_idx || child_prop->value == &PROPS)
        {
            continue;
        }

        if(filter == PARENT_GET_NOT_HIDDEN)
        {
            if(Test(child_prop->flags,PF_HIDE))
            {
                continue;
            }
        }
        else if(child_prop->type != PT_PARENT &&
                (!Test(child_prop->flags,PF_CONFIG) ||
                 (filter == PARENT_GET_CONFIG_CHANGED && !Test(child_prop->dynflags,DF_CFG_CHANGED)) ||
                 (filter == PARENT_GET_CONFIG_SET     && !Test(child_prop->dynflags,DF_NVS_SET)) ||
                 (filter == PARENT_GET_CONFIG_UNSET   &&  Test(child_prop->dynflags,DF_NVS_SET))
                )
               )
        {
            continue;
        }

        c->from    = from;                      // Restore array range and flags
        c->to      = to;
        c->getopts = getopts;
        c->prop    = child_prop;
        c->sym_idxs[c->si_idx++] = child_prop->sym_idx;

        c->mux_idx = (child_prop->type == PT_PARENT ||
                       Test(child_prop->flags,PF_PPM|PF_SUB_DEV) ? mux_idx : 0);

        get_func = GET_FUNC[child_prop->get_func_idx];
        if(get_func == GetParent)
        {
            errnum = CmdParentGet(c, child_prop, filter);
        }
        else
        {
            errnum = get_func(c,child_prop);    // Call child's get function
        }

        c->si_idx--;                            // Pop prop name symbols stack index

        if(errnum)                              // If function returns an error
        {
            return(errnum);                     // Report error to calling function
        }

        if(get_func != GetParent)               // If not a parent property
        {
            fputc('\n',c->f);                   // write newline
        }
    }

    return(0);
}
#pragma MESSAGE DEFAULT C1855
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CmdPrepareGet(struct cmd * const c, struct prop * const p, uint16_t linelen, prop_size_t *n_els_ptr)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares the get structure based on the to/from/linelen specifiers.  It display
  label and/or range as required.  It set the returned argument pointed by n_els_ptr to the number of
  elements in the selected from-to range (may be zero). It returns a status (FGC_OK_NO_RSP if OK)
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t     n_els;
    uint16_t          type_idx;
    uint16_t          errnum;

    c->line    = 0;                             // Reset line counter
    c->linelen = linelen;                       // And set elements per line

    // Prepare array range

    errnum = PropValueGet(c, p, NULL, c->from, NULL);   // Get first element to retrieve n_element in case
    // property iS on the DSP. This will cache the first block.
    if ( errnum )
    {
        if(n_els_ptr != NULL)
        {
            *n_els_ptr = 0;                     // In case of errnum, the number of elements is forced to 0
        }
        return(errnum);
    }

    n_els = c->n_elements;

    if(n_els)                                   // Get number of data elements in array
    {
        if(c->n_arr_spec < 2 || c->to >= n_els) // if to index not specified, or to beyond n_elemens
        {
            c->to = n_els - 1;                  // Clip 'to' to end of contents
        }

        if(c->from > c->to)                     // If start is after end
        {
            n_els = c->from = c->to = 0;        // Reset from, to & n_els to zero
        }
        else
        {
            n_els = (c->to - c->from + c->step) / c->step;  // Calc number of elements in range
        }                                       // incorporating the array step
    }

    /*--- Prepare IDX flags ---*/

    if((c->token_delim != ',')         &&       // if delimiter is NOT a comma, and
       !Test(c->getopts,GET_OPT_NOIDX) &&       // NOIDX is NOT specified, and
       (Test(c->getopts,GET_OPT_IDX)   ||       // IDX is specifed, or
       (n_els > linelen)))                      // the number of elements exceeds line length
    {
        Set(c->getopts,GET_OPT_IDX);            // set IDX flag in opt structure
    }
    else
    {
        Clr(c->getopts,GET_OPT_IDX);            // reset IDX flag in opt structure
    }

    /*--- Add timestamp to response on first call ---*/

    CmdPrintTimestamp(c, p);

    /*--- Display label and/or type and/or range as required ---*/

    if(Test(c->getopts,GET_OPT_LBL))            // If LBL option defined
    {
        CmdPrintLabel(c,p);                     // Display property label
    }

    if(Test(c->getopts,GET_OPT_TYPE))           // If TYPE option defined
    {
        if(Test(p->flags,PF_SYM_LST))           // If symlist
        {
            if(p->max_elements > 1)             // If array
            {
                type_idx = PT_STRING;           // Specify STRINGS
            }
            else
            {
                type_idx = PT_CHAR;             // Specify CHAR
            }
        }
        else
        {
            type_idx = p->type;
        }

        // In order to distinguis between arrays and scalars:
        //
        // Scalar properties (max_elements = 1) return the property type in lower case
        // Array properties (max_element > 1) return the property type in upper case
        //
        // This is needed by the FGCD to correctly report properties with the CMW as
        // either scalars or arrays. Further information in EPCCCS-4715

        fprintf(c->f, "%s:", (p->max_elements == 1 ? prop_type_name_lower[type_idx] : prop_type_name[type_idx]));
    }

    if(Test(c->getopts,GET_OPT_RANGE))          // If RANGE get option specified
    {
        CmdPrintRange(c,p);                     // Print range if relevant to this property
    }

    /*--- Write newline if required ---*/

    if(Test(c->getopts,GET_OPT_IDX) ||
      (Test(c->getopts,GET_OPT_LBL) && (n_els > linelen) && c->token_delim == ' '))
    {
        fputc('\n',c->f);
    }

    if(n_els_ptr != NULL)
    {
        *n_els_ptr = n_els;
    }

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
void CmdStartBin(struct cmd *c, struct prop *p, uint32_t n_bytes)
/*---------------------------------------------------------------------------------------------------------*\
  This will write the binary data header when starting the export of binary log data.  The header is five
  bytes long and starts with 0xFF, followed by the length of the data in bytes.
\*---------------------------------------------------------------------------------------------------------*/
{
    CmdPrintTimestamp(c, p);                    // Produce timestamp
    c->store_cb(0xFF);                          // Write Binary data header byte (0xFF)
    FbsOutLong( (char *) &n_bytes,c);           // Write payload length in bytes
}
/*---------------------------------------------------------------------------------------------------------*/
void CmdPrintTimestamp(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write the timestamp to the stream for c provided it is not the serial command stream
  and the timestamp has not already been produced.  If timestamp is NULL it will take the time now as the
  timestamp.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct abs_time_us                     timestamp = { 0, 0 };                            // Local copy of the timestamp
    struct abs_time_us *                   timestamp_ptr;
    enum timestamp_select                  timestamp_select = TIMESTAMP_NOW;     // The default timestamp (time now)
    bool                                   timestamp_found  = false;
    struct prop *                          parent_p;
    struct init_dynflag_timestamp_select * property_timestamp_info;

    // If not from serial terminal and timestamp not already produced

    if ( (c != &tcm) && (!c->timestamp_f) )
    {
        // Only include a valid acquisition stamp for acquisitions (no SET function)
        if (p->set_func_idx == SET_NONE)
        {
            if(!Test(p->dynflags, DF_TIMESTAMP_SELECT))
            {
                // The most common case: the property uses the default timestamp (time now).

                timestamp_ptr = &mst.time;
            }
            else
            {
                // If the property has the DF_TIMESTAMP_SELECT flag then look for the property and its parents in the
                // init_dynflag_timestamp_select list, in order to know which timestamp to use.

                parent_p = p;
                while(!timestamp_found && parent_p)
                {
                    property_timestamp_info = &init_dynflag_timestamp_select[0];

                    while(property_timestamp_info->prop)
                    {
                        if(property_timestamp_info->prop == parent_p)
                        {
                            timestamp_select = property_timestamp_info->timestamp_select;
                            timestamp_found  = true;
                            break;
                        }

                        property_timestamp_info++;
                    }

                    parent_p = parent_p->parent;
                }

                // Switch on the timestamp selector

                switch(timestamp_select)
                {
                    case TIMESTAMP_CURRENT_CYCLE:
                        timestamp_ptr = &dpcom.dsp.cyc.start_time;
                        break;
                    case TIMESTAMP_PREVIOUS_CYCLE:
                        timestamp_ptr = &dpcom.dsp.cyc.prev_start_time;
                        break;
    #if (FGC_CLASS_ID != 59)
                    case TIMESTAMP_LOG_CAPTURE:
                        timestamp_ptr = &dpcls.dsp.log.capture.first_sample_time;
                        break;
    #endif
                    case TIMESTAMP_NOW:
                    default:
                        timestamp_ptr = &mst.time;
                        break;
                }
            }

            // Copy of timestamp in a critical section

            OS_ENTER_CRITICAL();
            timestamp = *timestamp_ptr;
            OS_EXIT_CRITICAL();
        }

        FbsOutLong( (char *) &timestamp.unix_time, c);
        FbsOutLong( (char *) &timestamp.us_time,   c);

        c->timestamp_f = true;
    }
}
#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void CmdPrintLabel(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  This will print the property symbol name based on the sym_idxs[] array in the get structure.  This must
  handle two cases - when getting a single property and when getting children of a parent.
  For a single property, the complete name is reported, while for getting a parent, only the name extension
  is reported.  For this c->n_prop_name_lvls is the level of parent, while c->si_idx will be the level
  of the child.
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t      n;                      // n: Length of the last symbol name. Relevant on FGC2 only
    uint16_t      line_offset_parent = 0; // Line offset after the last parent symbol name
    uint16_t      line_offset_child  = 0; // Line offset after the child symbol name
    uint16_t      idx;
    uint16_t      si_idx;

    si_idx = c->si_idx;                 // Number of symbols in the name of the child property
    idx    = c->n_prop_name_lvls;       // Number of levels in the name of the parent property (if present)

    // ToDo: vulnerability
    // if (idx = c->n_prop_name_lvls)  is bigger than (si_idx = c->si_idx) then later
    // n can be used uninitialised !!! can we assure that always c->n_prop_name_lvls <= c->si_idx


    if(idx == si_idx)                   // If just getting this child property
    {
        idx = 0;                        // Show complete name, otherwise only show symbols below
    }
    // the parent level

    // If a leaf property, do not return the property name but "value" as the label.
    // This is required by CMW and was previously hacked in the fgcd (EPCCCS-4855)

    if (   c->last_prop       != NULL
        && c->last_prop->type != PT_PARENT)
    {
        line_offset_child = fputs("value:", c->f);
    }
    else
    {
        n = -1;

        while(idx < c->si_idx)
        {
            line_offset_parent += (n + 1);

            n = fputs(SYM_TAB_PROP[c->sym_idxs[idx++]].key.c,c->f);         // On M68HC16 (FGC2), n holds the length of the
            // last symbol name being written
            fputc((idx == si_idx ? ':' : '.'),c->f);

            line_offset_child += (n + 1);
        }

        if(c->token_delim == ' ')                           // If neat formating required
        {
            // Compute the number of spaces to add to the current printout for neat formatting

            n  = line_offset_parent + ST_MAX_SYM_LEN;               // Min column number
            n += ST_NEAT_FORMAT_TAB_SZ - n % ST_NEAT_FORMAT_TAB_SZ; // Add a delta for tabulation
            n -= line_offset_child;                                 // Number of spaces to add

            while(n--)                                              // For each space required
            {
                fputc(' ', c->f);                                   // write a space to output stream
            }
        }
    }
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*/
static void CmdPrintRange(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the range for the property in ().  The range can either be integer limits,
  float limits or a symbol list.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct sym_lst *    sym_lst;
    bool        space_f;
    uint8_t       delim;

    delim = (c->token_delim == ',' ? ' ' : '\n');

    if(Test(p->flags, PF_SYM_LST))              // If property uses a symlist
    {
        space_f = false;                        // Suppress leading space
        sym_lst = (struct sym_lst*)p->range;    // Get pointer to symbol list

        fputc('(',c->f);                        // Write opening bracket (

        while(sym_lst->sym_idx)                 // For each symbol index (until end of list)
        {
            if(space_f)
            {
                fputc(' ',c->f);                // Write leading space if not first time
            }
            else
            {
                space_f = true;
            }

            fputs(SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c,c->f);  // Write symbol

            sym_lst++;
        }
        fputc(')',c->f);
        fputc(delim,c->f);                      // Write closing delimiter
    }
    else if (Test(p->flags, PF_LIMITS))
    {
        // Types FLOAT, POINT, FL_POINT use floating-point limits.
        // The rest use integer limits

        if (p->type == PT_FLOAT || p->type == PT_POINT || p->type == PT_FL_POINT)
        {
            fprintf(c->f,
                    "(%.5E %.5E)%c",                // Float limits as x.xxxxxEyy
                    (double)((struct float_limits *)p->range)->min,
                    (double)((struct float_limits *)p->range)->max,
                    delim);
        }
        else if ((p->type == PT_ABSTIME || p->type == PT_ABSTIME8 || p->type == PT_INT32U) && !Test(c->getopts, GET_OPT_HEX))
        {
            fprintf(c->f,
                    "(%lu %lu)%c",
                    ((struct intu_limits *)p->range)->min,
                    ((struct intu_limits *)p->range)->max,
                    delim);
        }
        else
        {
            fprintf(c->f,
                    (Test(c->getopts, GET_OPT_HEX) ? "(0x%08lX 0x%08lX)%c" : "(%ld %ld)%c"),
                    ((struct int_limits *)p->range)->min,
                    ((struct int_limits *)p->range)->max,
                    delim);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CmdPrintIdx(struct cmd *c, prop_size_t idx)
/*---------------------------------------------------------------------------------------------------------*\
  This will check the get flags to decide if/how to display the passed index.  It also adds a space or
  command delimiter character, according to the GET OPTION.

  The function also checks the abort flag and returns a error if it is set.

  Get Option:  !IDX     Display nothing
        HEX     Display index in hex rather than decimal
\*---------------------------------------------------------------------------------------------------------*/
{
    if(c->abort_f)                              // If command was aborted (CTRL-C via keyboard or FIP unlock)
    {
        return(FGC_ABORTED);                    // Report ABORTED
    }

    if((c->token_delim == ' ') &&               // If not comma delimiter and
       !(c->line % c->linelen))                 // it's time for a new line
    {
        if(Test(c->getopts,GET_OPT_IDX))        // If Index required
        {
            fputc('\n',c->f);                   // write newline
            CmdPrintIdxFormatted(c, idx);       // Write IDX with neat/direct/hex/dec
        }
        else if(c->line)                        // else if not the first element
        {
            fputc('\n',c->f);                   // Write newline
        }
    }
    else if(c->line && c->token_delim)          // else if delimiter required
    {
        fputc(c->token_delim,c->f);             // Write delimiter character
    }

    c->line++;

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
static void CmdPrintIdxFormatted(struct cmd *c, uint16_t idx)
/*---------------------------------------------------------------------------------------------------------*\
  This will print idx according to the command token delimiter (' '=Neat, ','=Direct) and the HEX get
  option flag.
\*---------------------------------------------------------------------------------------------------------*/
{
                                   //        ---- TERM ----      --- DIRECT ---
                                   //        DEC        HEX      DEC        HEX
    static char * FAR   int4fmtstr[]    = { "%4u: ", "0x%04X: ", "%u:", "0x%04X:" };
    uint16_t              i               = 0;


    if(Test(c->getopts,GET_OPT_HEX))
    {
        i = 1;
    }

    if(c->token_delim == ',')
    {
        i += 2;
    }

    fprintf(c->f,int4fmtstr[i],idx);            // Write idx using format
}
/*---------------------------------------------------------------------------------------------------------*/
char * FAR CmdPrintSymLst(struct sym_lst * sym_lst, uint16_t value)
/*---------------------------------------------------------------------------------------------------------*\
  This will return a pointer to the symbol string for the value in the symbol list (if valid) or the
  string "<invalid>" if not.
\*---------------------------------------------------------------------------------------------------------*/
{
    while(sym_lst->sym_idx)                     // For each symbol in list
    {
        if(value == sym_lst->value)             // If value matches symbol constant
        {
            return(SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c); // Return pointer to symbol string
        }
        sym_lst++;
    }

    return("!invalid.");                        // No match so report invalid
}
/*---------------------------------------------------------------------------------------------------------*/
void CmdPrintBitMask(FILE *f, struct sym_lst *sym_lst, uint16_t value)
/*---------------------------------------------------------------------------------------------------------*\
  This will write to the f the symbols for which the bits are set in value.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  mask;
    bool    space_f = false;

    while(sym_lst->sym_idx)                     // For each symbol in list
    {
        mask = sym_lst->value;                  // Get mask constant for symbol

        if((value & mask) == mask)              // If bits in value match bits in mask
        {
            if(space_f)
            {
                fputc(' ',f);                   // Prefix space if not first symbol
            }
            else
            {
                space_f = true;
            }
            fputs(SYM_TAB_CONST[sym_lst->sym_idx & ~FGC_NOT_SETTABLE].key.c,f); // Write symbol
        }

        sym_lst++;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CmdWaitUntil(uint32_t unix_time, uint16_t ms_time)
/*---------------------------------------------------------------------------------------------------------*\
  This function will wait until the specified time has arrived.
\*---------------------------------------------------------------------------------------------------------*/
{
    while(       (SM_UXTIME_P < unix_time)
            || ( (SM_UXTIME_P == unix_time) && (SM_MSTIME_P < ms_time) )
         )
    {
       OSTskSuspend();  // Resumed by MstTsk on next millisecond
    }
}
#pragma MESSAGE DISABLE C5909 // Disable 'assignment in codition' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CmdPrintfDsp(struct cmd *c, enum FGC_float_fmt format_idx, FP32 value)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets the help of the DSP to convert the float value into ASCII on commands stream using the
  supplied format. The DSP will normally respond in 1ms.
\*---------------------------------------------------------------------------------------------------------*/
{
    int8_t                       ch;
    int8_t *                     ch_p;
    struct pfloat_buf *         pfloat = c->pfloat_buf;

    if (!mst.dsp_bg_is_alive || !mst.dsp_rt_is_alive)
    {
        return(FGC_DSP_NOT_AVL);                // Report error
    }

    pfloat->value      = value;                 // Pass floating point value to DSP
    pfloat->format_idx = format_idx;            // Trigger print float action in DSP

    // Wait while DSP is busy

    while(pfloat->format_idx != FLOAT_FMT_NONE)
    {
        OSTskSuspend();                         // Resumed by MstTsk() on next ms
    }

    ch_p = pfloat->result;                      // Get pointer to ASCII result

    while((ch = *(ch_p++)))                     // Write ASCII result to file stream
    {
        fputc(ch,c->f);
    }

    return(0);
}
#pragma MESSAGE DEFAULT C5909

#pragma MESSAGE DISABLE C1855 // Disable 'Recursive function call' warning
/*---------------------------------------------------------------------------------------------------------*/
static uint16_t CmdGetChangedPropsHelper(const struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Helper function for CmdGetChangedProps()
\*---------------------------------------------------------------------------------------------------------*/
{
    struct prop    *child_prop;
    prop_size_t     n_els;
    uint16_t          idx;
    uint16_t          changed = 0;

    child_prop = p->value;
    n_els      = PropGetNumEls(NULL, p);

    for (idx = 0 ; idx < n_els ; idx++, child_prop++)
    {
        if (child_prop->value == &PROPS)
        {
            continue;
        }

        if (child_prop->type == PT_PARENT)
        {
            changed += CmdGetChangedPropsHelper(child_prop);
        }
        else
        {
            if (Test(child_prop->flags, PF_CONFIG) &&
                Test(child_prop->dynflags, DF_CFG_CHANGED))
            {
                changed++;
            }
        }
    }

    return (changed);
}
#pragma MESSAGE DEFAULT C1855

/*---------------------------------------------------------------------------------------------------------*\
  End of file: cmd.c
\*---------------------------------------------------------------------------------------------------------*/
