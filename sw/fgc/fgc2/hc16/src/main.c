/*---------------------------------------------------------------------------------------------------------*\
  File:         main.c

  Purpose:      FGC MCU Software - contains main()
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h
#define FGC_PARSER_CONSTS   // Force parser global variable definitions
#define DEF_VERSION         // define def_version global variable
#define DEFPROPS            // define property global variables
#define DEV_GLOBALS         // to instantiate dev global variable


#define MAIN_GLOBALS        // define main_misc global variable
#define FGC_COMPDB_GLOBALS  // define compdb global variable
#define FGC_SYSDB_GLOBALS   // define sysdb global variable
#define FGC_DIMDB_GLOBALS   // define dimdb global variable
#define OS_GLOBALS          // define os global variable
#define CRATE_GLOBALS       // define crate global variable
#define LOG_EVENT_GLOBALS   // define log_evt global variable
#define EDAC_GLOBALS        // define edac global variable
#define CORE_GLOBALS        // define core global variable
#define POPS_GLOBALS        // define edac global variable

#define DPRAM_CLASS_GLOBALS // define dpcls global variable
#define DPCOM_GLOBALS       // define dpcom global variable


#include <main.h>
#include <cmd.h>
#include <start.h>          // for Crash()
#include <init.h>           // for InitMCU(), InitOS()
#include <os.h>             // for OSTskStart()
#include <dev.h>            // for dev global variable
#include <defprops.h>       // for STP_RESET, STP_BOOT, STP_CRASH
#include <core.h>
#include <shared_memory.h>  // for FGC_STAY_IN_BOOT
#include <DIMsDataProcess.h> // for InitQspiBus()
#include <mcu_dependent.h>

/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by startup() and is the function that does all the initialisation duties.  It
  then starts the operating system to launch multi-tasking.
\*---------------------------------------------------------------------------------------------------------*/
{
    InitMCU();
    LoadProgramIntoDsp();
    InitQspiBus();
    HashInit();
    PllInit();
    InitOS();

    OSTskStart();
}
/*---------------------------------------------------------------------------------------------------------*/
void stop(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from MstTsk() if the dev.ctrl_counter reaches zero.  This will be set by the
  SetDevice property to trigger a delayed stop action to allow the command communication to complete.
  Note that the PWRCYC option is activated in MstTsk() via the LEDS register which must be activated for
  many milliseconds, during which time the fast watchdog trigger must be maintained, otherwise a crash
  response occurs.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(dev.ctrl.action)
    {
    case STP_PWRCYC:

        // This will prevent overwriting of CPU_PWRCYC_P (alias for RGLEDS) register in the millisecond task

        dev.ctrl.power_cycle_pending = true;

        POWER_CYCLE();
        break;

    case STP_RESET:             // Global Reset

        Reset();
        break;

    case STP_BOOT:              // Global Reset and stay in boot

        SM_MPRUN_P = FGC_STAY_IN_BOOT;
        Reset();
        break;

    case STP_CRASH:             // Bus error crash

        Crash(0xDEAD);
        break;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/
