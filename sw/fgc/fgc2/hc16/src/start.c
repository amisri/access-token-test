/*---------------------------------------------------------------------------------------------------------*\
  File:         start.c

  Purpose:      FGC HC16 Software - Start up, Restart, Reset, Crash, DSP & NvSram functions

  Author:       Hiware, Quentin.King@cern.ch

  Notes:        This file contains the function _Startup().  It assumes that the program uses the
                Medium Memory Model and that chipselects are programmed either by the Boot program
                or the BDI interface.
\*---------------------------------------------------------------------------------------------------------*/

#include <start.h>

#include <start16.h>        // Include Hiware Linker Startup header file
#include <cmd.h>            // Command header
#include <fbs_class.h>      // for ST_LATCHED
#include <dev.h>            // for dev global variable
#include <mst.h>            // for mst global variable
#include <dpcom.h>          // for dpcom global variable
#include <sta.h>            // for sta global variable
#include <memmap_mcu.h>     // for CPU_RESET_CTRL_DSP_MASK16
#include <fbs.h>            // for fbs global variable
#include <mem.h>            // for MemCpyWords()
#include <macros.h>         // for Set()
#include <mcu_dependent.h>  // for SELFLASH()
#include <reset.h>
#include <start.h>

struct _tagStartup      _startupData;           // Linker start up data structure

/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT
/*---------------------------------------------------------------------------------------------------------*/
void _Startup(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the starting point for the main program.  It completes by JUMPING to ResetPeripherals
  in the FGC library.  This resets the uFIP, C62 and DSP before jumping to StartMain() to set up the C
  context.

  ***** No subroutine calls can be made from this function because the stack has not been set up yet *****
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDAB    #0x0F                           // Switch to Page F to access HC16 registers
        TBEK

        CLRW    GPT_TCTL_16                     // Disable GPT OC2
        LDD     #0x0001
        STD     GPT_TMSK_16                     // Stop all GPT IRQs and set clock prescalar to 8 (2 MHz)
        LDD     GPT_TFLG_16                     // Clear outstanding GPT interrupts
        CLRW    GPT_TFLG_16

        LDAB    #0xE1                           // SIM_PFPAR: Bits 1-4 are port F, the rest are IRQ or MODCLK
        STAB    SIM_PFPAR_16

        LDAB    #0x1E                           // SIM_DDRF: Bits 1-4 are outputs to drive LEDs
        STAB    SIM_DDRF_16

        LDAB    #0x0E                           // SIM_PORTF: Set LED 3 only
        STAB    SIM_PORTF_16

        LDAB    #0x00                           // Return to default page 0
        TBEK

        BSET    CPU_MODEL_16,#CPU_MODEL_RUNMAIN_MASK8  // Set RUNMAIN bit in MODE Low byte

        JMP     ResetPeripherals                // Reset uFIP, C62 and DSP then jumps to StartMain()
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_ENTRY
#pragma NO_EXIT
/*---------------------------------------------------------------------------------------------------------*/
void Reset(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function performs the following task:

        - Write the MP command into the SM register
        - Mask Interrupts
        - Activate a global reset
        - Wait to be reset
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_DISABLE_INTS();                          // Disable interrupts

    CPU_RESET_P |= CPU_RESET_CTRL_GLOBAL_MASK16;                        // Request a global reset

    for(;;);                                    // Wait for reset to take effect
}
/*---------------------------------------------------------------------------------------------------------*/
void Crash(uint16_t reg_e)
/*---------------------------------------------------------------------------------------------------------*\
  This function reads from an unmapped memory area to trigger a bus error exception.  It sets recognisable
  values in the main registers so that the trap dump can be checked.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_ENTER_CRITICAL();

    asm
    {
        PSHM    D,E,X,Y,Z,K                     // Save all registers

        LDE     reg_e                           // E  = parameter from calling function
        LDAB    #0                              // EK = 0
        TBEK
        LDAB    #2                              // YK = 2
        TBYK
        LDY     #0x3456                         // YK:IY = 0x23456

        LDAB    #3                              // ZK = 3
        TBZK
        LDZ     #0x4567                         // ZK:IZ = 0x34567

        CLRW    CPU_BUSERR_16                   // Write to bus error register to trigger bus error
        NOP
        NOP
        PULM    D,E,X,Y,Z,K                     // Restore all registers in case bus error fails to occur
    }

    OS_EXIT_CRITICAL();
}
/*---------------------------------------------------------------------------------------------------------*/
void LoadProgramIntoDsp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will copy the DSP code into the DSP SRAM
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( !mst.dsp_is_standalone )
    {
        // Set TRISTATE only

        CPU_RESET_P &= ~CPU_RESET_CTRL_DSP_MASK16;

        // Set memory mode 8 to make MP23 visible

        CPU_MODEL_P = CPU_MODEL_RUNMAIN_MASK8;

        // Wait for initial DSP mem refresh to complete

        while( TST_CPU_RESET(C32INIT) )
        {
        }

        // Copy first page

        SELDPWPG(0);
        MemCpyWords(DPW_32,CODES_DSPPROG_P20PG0_32,CODES_DSPPROG_P20PG0_W);

        // Copy second page

        SELDPWPG(1);
        MemCpyWords(DPW_32,CODES_DSPPROG_P20PG1_32,CODES_DSPPROG_P20PG1_W);

        // Set DSP in RESET + TRISTATE

        CPU_RESET_P |= CPU_RESET_CTRL_DSP_MASK16;

        // Set DSP in RESET

        CPU_RESET_P &= ~CPU_RESET_CTRL_DSPTSM_MASK16;
    }

    SELFLASH(SEL_BB);                                   // Set memory mode 9 to make BB_23 visible
}
/*---------------------------------------------------------------------------------------------------------*/
void StartRunningDsp(uint32_t delay)
/*---------------------------------------------------------------------------------------------------------*\
  This function will start the DSP if it is under M68HC16 control
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!mst.dsp_is_standalone)                          // If DSP is under control of M68HC16
    {
        while(--delay)                                  // Wait for DSP PLL to settle before starting
        {
        }

        CPU_RESET_P = mst.reset;                        // Set DSP in !RESET to start DSP
    }

    dpcom.mcu.started = DSP_STARTED;                    // Tell the DSP to start main program operation
}
/*---------------------------------------------------------------------------------------------------------*/
void WaitForDspStarted(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will wait for the DSP software to start, if it is under M68HC16 control.  Timeout is in
  ~5 us units.  Problems are reported through the DSP version string in the DPRAM.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t timeout = 100000;

    while(!dpcom.dsp.started && --timeout)              // Wait for DSP to start
    {
    }

    if(dpcom.dsp.started != DSP_STARTED)                // If DSP didn't start
    {
        CPU_RESET_P |= CPU_RESET_CTRL_DSP_MASK16;
        Set(ST_LATCHED,FGC_LAT_DSP_FLT);                // Report that DSP didn't start
        Set(sta.faults,FGC_FLT_FGC_HW);                 // and set a FGC_HW fault
    }

    dpcom.mcu.started = 0;                              // Tell the DSP to start main program operation
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: start.c
\*---------------------------------------------------------------------------------------------------------*/

