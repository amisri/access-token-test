/*---------------------------------------------------------------------------------------------------------*\
  File:     set.c

  Purpose:  FGC MCU Software - Set Command Functions
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#include <set.h>

#include <stdbool.h>
#include <definfo.h>        // for FGC_CLASS_ID
#if (FGC_CLASS_ID != 59)
#include <adc_class.h>      // for AdcFiltersResetRequest()
#include <cal_class.h>      // for cal global variable, NON_PPM_REG_MODE
#endif
#include <class.h>          // for ST_LATCHED_RESET
#include <cmd.h>            // for struct cmd & struct fgc_corectrl
#include <core.h>           // for core global variable
#include <defprops.h>       // Include property related definitions
#include <dev.h>            // for dev global variable
#include <diag.h>           // for diag global variable
#include <dls.h>            // for dls global variable
#include <dpcls.h>          // for dpcls global variable
#include <dpcom.h>          // for dpcom global variable
#include <fbs.h>            // for fbs global variable
#include <fbs_class.h>      // for FAULTS, ST_LATCHED
#include <fgc_errs.h>       // for FGC_BAD_PARAMETER, FGC_BAD_STATE
#include <init.h>           // for InitSpyMpx()
#include <init_class.h>     // for InitClassPostSync()
#include <iodefines.h>      // processor specific registers and constants
#include <log.h>            // for LogStartAll()
#include <macros.h>         // for Test(), Set(), Clr()
#include <math.h>           // for fabs()
#include <mcu_dependent.h>  // for MSLEEP(), NVRAM_UNLOCK(), NVRAM_LOCK(), ENTER_SR(), EXIT_SR
#include <mem.h>            // for MemSetBytes()
#include <memmap_mcu.h>     // for DIG_OP_P
#include <nvs.h>
#include <pars.h>           // for ParsSet(), ParsScanSymList()
#include <prop.h>           // for PropSetNumEls()
#include <ref_class.h>      // for RefArm(), RefPlep(), RefOpenloop(), RefTrim(), RefTest(), RefReset(), NON_PPM_USER
#include <runlog_lib.h>     // for RunlogClrResets(), RunlogSaveResets()
#include <sta.h>            // for sta global variable
#include <sta_class.h>      // for vs global variable, DDOP_CMD_RESET
#include <state_class.h>    // for STAF_... flags
#include <macros.h>         // for Test() macro

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning



#if FGC_CLASS_ID != 59
uint16_t SetResetFaults(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  This function will:
    - Clear the latched faults in STATUS.ST_LATCHED
    - Send the reset signal to the VS to clear the external faults
  It is called by doing a S VS RESET or a S STATUS.RESET_FAULTS.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 51)

    // Block the reset request if a FW DIODE fault was received
    // and the signal is a fault, not a status

    if (vs.fw_diode == FGC_VDI_FAULT && vs.fwd_is_fault)
    {
        return (FGC_FW_DIODE_FAULT);
    }

    // Block the reset if a FABORT_UNSAFE fault was received

    if (vs.fabort_unsafe == FGC_VDI_FAULT)
    {
        return (FGC_FABORT_UNSAFE);
    }

#endif

    sta.cmd |= DDOP_CMD_RESET;

    dpcom.mcu.diag.relaunch_dim_logging = true;

    return (0);
}
#endif

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetAbsTime(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    DOUBLE
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetChar(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    CHAR
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetConfig(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetConfig allows the Configuration to be:

    UNSET       Magic number in non-volatile memory is erased so that complete config will be
            wiped on next reset
    SET     Magic number in non-volatile memory is reinstated, so configuration will be
            preserved
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (p->sym_idx)
    {
        case STP_UNSET: // Erase configuration

            NVRAM_UNLOCK();
            NVSIDX_MAGIC_P = ~NVS_MAGIC;
            NVRAM_LOCK();
            break;

        case STP_SET:   // Preserve configuration

            NVRAM_UNLOCK();
            NVSIDX_MAGIC_P = NVS_MAGIC;
            NVRAM_LOCK();
            break;
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetCore(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetCore is used to check and save the Core control table
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                  i;
    uint32_t                  total_length_words = 0;
    uint32_t                  addr_in_core       = NVRAM_COREDATA_32;
    struct fgc_corectrl     corectrl;


    for (i = 0; i < FGC_N_CORE_SEGS; i++)           // Calculate total length of segments
    {
        total_length_words += core.n_words[i];
    }

    if (total_length_words > NVRAM_COREDATA_W)      // If length is too long
    {
        MemSetBytes(&core.n_words, 0, sizeof(core.n_words)); // Clear all lengths
        return (FGC_BAD_PARAMETER);                 // Report error
    }

    for (i = 0; i < FGC_N_CORE_SEGS; i++)           // Create core control table
    {
        corectrl.seg[i].addr     = core.addr[i];
        corectrl.seg[i].n_words  = core.n_words[i];
        corectrl.seg[i].pad      = 0xFFFF;
        corectrl.addr_in_core[i] = addr_in_core;
        addr_in_core += (2 * core.n_words[i]);
    }

    CoreTableSet(&corectrl);                        // Write table to FRAM and clear core data

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetFloat(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    FLOAT
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetInteger(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    uint8_t, int8_t, uint16_t, int16_t, uint32_t, int32_t including symlist properties
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetConfigMode(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Used to synchronize either the FGC or the DB
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t config_mode = sta.config_mode;
    uint16_t errnum;

    errnum = SetInteger(c, p);

    if (errnum != 0)
    {
        return (errnum);
    }

#if (FGC_CLASS_ID != 59)
    if (   STATE_PC != FGC_PC_OFF
        && STATE_PC != FGC_PC_FLT_OFF
        && sta.config_mode != FGC_CFG_MODE_SYNC_DB)
    {
        sta.config_mode = config_mode;
        return (FGC_BAD_STATE);
    }
#endif

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetReset(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetReset is used to allow a S PROP RESET for
  S CONFIG.STATE RESET
  S DIAG RESET
  S DLS RESET
  S ID RESET
  S LOG RESET
  S DEVICE.RESETS RESET
  S SPY RESET
  S MEAS.STATUS RESET
  S FGC.ST_LATCHED RESET
\*---------------------------------------------------------------------------------------------------------*/
{
    // RESET is the only choice in the symlist

    switch (p->sym_idx)
    {
#if FGC_CLASS_ID != 59

        case STP_ADC:                               // ADC
        case STP_FILTER:                            // ADC.FILTER

            AdcFiltersResetRequest(true);

            break;
#endif

        case STP_STATE:             // CONFIG.STATE

            if (sta.config_mode == FGC_CFG_MODE_SYNC_FGC)
            {
                InitClassPostSync(c);
            }

            sta.config_mode  = FGC_CFG_MODE_SYNC_NONE;

            if (CmdGetChangedProps() == 0)
            {
                sta.config_state = FGC_CFG_STATE_SYNCHRONISED;
            }
            break;

        case STP_DIAG:              // DIAG RESET

            diag.req_QSPI_reset = 1;                    // Request immediate diag reset

            // ToDo: I guess that as the reset is demanded from the property
            // cleaning also the number of reset counter is a nice effect
            // can this line stay?
            diag.sync_resets = 0;                       // DIAG.SYNC_RESETS
            break;

        case STP_DLS:               // DLS RESET

            dls.num_errors = 0;                         // Reset Dallas errors counter
            MemSetBytes(&dls.rsp, 0, sizeof(dls.rsp));  // Reset response buffers
            PropSetNumEls(NULL, &PROP_FGC_DLS_GLOBAL, 0);   // Reset response buffer lengths for properties
            PropSetNumEls(NULL, &PROP_FGC_DLS_LOCAL,  0);
#if FGC_CLASS_ID != 59
            PropSetNumEls(NULL, &PROP_FGC_DLS_MEAS_A, 0);
            PropSetNumEls(NULL, &PROP_FGC_DLS_MEAS_B, 0);
#endif
            Clr(ST_LATCHED, FGC_LAT_DALLAS_FLT);        // Clear DALLAS_FLT in latched status
            break;

#if FGC_CLASS_ID == 59

        case STP_FGSTATUS:                      // FGSTATUS

            FAULTS = (sta.faults | (uint16_t)dpcom.dsp.faults);   // Clear faults latch
            break;

        case STP_FGSTATS:                       // FGSTATS

            rfc.reset_f = true;                 // Request reset of RFC interface (to IsrMst)
            break;
#endif

        case STP_FLASH_LOCKS:                   // CODE.FLASH_LOCKS
            ENTER_SR();                         // Map FRAM to page 7
            NVRAM_UNLOCK();
            NVRAM_MAGIC_FP = 0;                 // Clear FRAM flash locks magic number
            NVRAM_LOCK();
            EXIT_SR();
            break;

#if (FGC_CLASS_ID == 51)

        case STP_FW_DIODE:              // VS.FW_DIODE
            // Clear the fault bit (if applicable) and reset the status property,
            // but only if the Free Wheel Diode signal is present on the VS DIM

            if(vs.fw_diode != FGC_VDI_NOT_PRESENT)
            {
                vs.fw_diode = FGC_VDI_NO_FAULT;
                Clr(sta.faults, FGC_FLT_FW_DIODE);
            }

            break;

        case STP_FABORT_UNSAFE:         // VS.FABORT_UNSAFE
            if (Test(sta.faults, FGC_FLT_FABORT_UNSAFE))
            {
                vs.fabort_unsafe = FGC_VDI_NO_FAULT;
                Clr(sta.faults, FGC_FLT_FABORT_UNSAFE);             // Clear fault
            }

            break;
#endif

        case STP_LOG:               // LOG
#if (FGC_CLASS_ID == 51)
            if (Test(sta.cmd_req, DIG_OP_SET_VSRUNCMD_MASK16))
            {
                return (FGC_BAD_STATE);
            }
            if (Test(ST_UNLATCHED, FGC_UNL_POST_MORTEM) != 0)
            {
                Clr(ST_UNLATCHED,FGC_UNL_POST_MORTEM);      // Clear post mortem flag
                Clr(fbs.u.fieldbus_stat.ack,FGC_SELF_PM_REQ);   // Reset self-trig PM dump request
            }
            else if (Test(ST_UNLATCHED, FGC_UNL_LOG_PLEASE) != 0)
            {
                logFgcLoggerReset();
            }

            if (   Test(ST_UNLATCHED, FGC_UNL_POST_MORTEM) == 0
                && Test(ST_UNLATCHED, FGC_UNL_LOG_PLEASE)  == 0)
            {
                LogStartAll();
            }
#else
            LogStartAll();
#endif
            break;

        case STP_NUM_RESETS:            // DEVICE.NUM_RESETS
            RunlogSaveResets(true);
            break;

#if (FGC_CLASS_ID == 53)

            // POPS controls
        case STP_PAL:               // PAL (POPS Acquisition Link)
            dpcls.dsp.pal.reset_req = 2;        // Request a PAL communications reset
            break;
#endif

        case STP_SPY:               // SPY
            InitClassSpyMpx(c);
            break;

        case STP_ST_LATCHED:            // STATUS.ST_LATCHED
            Clr(ST_LATCHED, ST_LATCHED_RESET);
            break;

#if (FGC_CLASS_ID != 59)

        case STP_STATUS:                            // MEAS.STATUS
            dpcls.mcu.meas.status_reset_f = true;
            break;
#endif
#if FGC_CLASS_ID != 59

        case STP_VS:                // VS
            return (SetResetFaults(c, p));      // S VS RESET has the same effect as S STATUS.RESET_FAULTS
            break;
#endif

        default:
            return FGC_BAD_PARAMETER;           // In case one defined this function as the set function
            // for a property and forgot to add a switch case for it
            break;

    }

    return (FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetRterm(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    NULL (CLIENT.RTERM)  This is a dummy function because the GW will intercept the set
        request so that it can apply an RBAC rule.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (FGC_SET_NOT_PERM);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetRtermLock(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    NULL (CLIENT.RTERMLOCK)  This is a dummy function because the GW will intercept the set
        request so that it can apply an RBAC rule.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (FGC_SET_NOT_PERM);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetTerminate(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetTerminate is used by the S DEVICE.RESET/BOOT/PWRCYC/CRASH properties.
\*---------------------------------------------------------------------------------------------------------*/
{
    dev.ctrl.action  = p->sym_idx;
    dev.ctrl.counter = DEV_CTRL_ACTION_DELAY;

    return 0;
}
#if FGC_CLASS_ID != 59
/*!
 * @brief Set function for CAL.AUTO.DCCTS property
 *
 * This function is used when setting a set-only CAL.AUTO.DCCTS property.
 * It tells the DSP to calibrate the DCCTs using given reference.
 *
 * @param c Pointer to command structure
 * @param p Pointer to property structure
 *
 * @return Error number
 */
uint16_t SetCalDccts(struct cmd * c, struct prop * p)
{
    uint16_t             errnum;
    struct sym_lst *     sym_const;

    const FP32           vref[3] = { 0.0, 10.0, -10.0 };

    // Using this array instead of full symlist narrows the symlist range
    // Constants others that these will be discarded with bad parameter error

    const struct sym_lst sym_lst_cal_ref[] =
    {
        {STC_CALZERO, 0},
        {STC_CALPOS,  1},
        {STC_CALNEG,  2},
        {0}
    };

    // Get the constant passed by the user

    errnum = ParsScanSymList(c, "Y", sym_lst_cal_ref, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    cal.seq_idx     = 0;
    cal.prop        = p;

    dpcls.mcu.cal.idx       = sym_const->value;
    dpcls.mcu.cal.chan_mask = 0x03;

    // Check if measured voltages indicate that a valid reference is applied
    // Only for calibration of external ADCs and DCCTs

    if ((((dpcls.mcu.cal.chan_mask & 0x01) &&
        fabs(dpcls.dsp.adc.volts_200ms[0] - vref[dpcls.mcu.cal.idx]) > CAL_VREF_LIMIT) ||
        ((dpcls.mcu.cal.chan_mask & 0x02) &&
        fabs(dpcls.dsp.adc.volts_200ms[1] - vref[dpcls.mcu.cal.idx]) > CAL_VREF_LIMIT)))
    {
        return (FGC_REF_MISMATCH);
    }

    dpcls.mcu.cal.action = CAL_REQ_DCCT;

    // Start auto calibration

    STATE_OP = FGC_OP_CALIBRATING;

    return 0;
}

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetCal(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:        FLOAT

  This a special set function because it must be able to set manually the calibration factors for the
  ADCs, DCCTs and DAC as well launch auto-calibrations.

  S CAL ADCS                    Automatic calibration of all errors of both ADC16s (channels A and B)
  S CAL ADC_A,{p}               Automatic calibration of ADC16 A error (p=CALZERO,CALPOS,CALNEG)
  S CAL ADC_B,{p}               Automatic calibration of ADC16 B error (p=CALZERO,CALPOS,CALNEG)
  S CAL EXT_ADCS, {p}           Automatic calibration of an error for both ADC22s (p=CALZERO,CALPOS,CALNEG)
  S CAL EXT_ADC_A,{p}           Automatic calibration of ADC22 A error (p=CALZERO,CALPOS,CALNEG)
  S CAL EXT_ADC_B,{p}           Automatic calibration of ADC22 B error (p=CALZERO,CALPOS,CALNEG)
  S CAL DCCTS, {p}              Automatic calibration of an error for both DCCTs (p=CALZERO,CALPOS,CALNEG)
  S CAL DCCT_A,{p}              Automatic calibration of DCCT A error (p=CALZERO,CALPOS,CALNEG)
  S CAL DCCT_B,{p}              Automatic calibration of DCCT B error (p=CALZERO,CALPOS,CALNEG)
  S CAL DAC_A                   Automatic calibration of DAC (done automatically when converter starts)
  S CAL DAC                     Is the same as S CAL DAC_A (shorter command & backward compatibility with old OP51)

  Auto calibrations are requested to the DSP via the following variables:

  dpcls.mcu.cal.action          CAL_REQ_BOTH_ADC16S|CAL_REQ_ADC16|CAL_REQ_ADC22|CAL_REQ_DCCT|CAL_REQ_DAC
  dpcls.mcu.cal.chan_mask       channel mask (channel A: 0x01; channel B: 0x02; channels A&B: 0x03)
  dpcls.mcu.cal.idx             0=Offset, 1=GainPos, 2=GainNeg

  The function uses two symbol lists - only one can be defined per property in the XML (sym_lst_cal_type)
  while the second (sym_lst_cal_ref) must be defined statically in the function.

  For all the cases were {p} is defined as CALZERO, CALPOS or CALNEG the function will check that the
  measured voltage is within +/-CAL_VREF_LIMIT of the nominal value (0V,+10V,-10V) and will return
  REF_MISMATCH if not.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                  errnum;
    uint16_t                  action;
    uint16_t                  cal_type;
    struct sym_lst     *    sym_const;

    const FP32              vref[3] = { 0.0, 10.0, -10.0 };

    const struct sym_lst    sym_lst_cal_ref[] =
    {
        {STC_CALZERO, 0},
        {STC_CALPOS,  1},
        {STC_CALNEG,  2},
        {0}
    };

    errnum = ParsScanSymList(c, "Y,", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    cal_type = sym_const->value;

    if (cal_type == FGC_CAL_TYPE_DAC_A || cal_type == FGC_CAL_TYPE_ADCS)
    {
        if (c->token_delim == ',')
        {
            return (FGC_BAD_PARAMETER);
        }

        dpcls.mcu.cal.idx = 0;
    }
    else
    {
        errnum = ParsScanSymList(c, "Y", sym_lst_cal_ref, &sym_const);

        if (errnum)
        {
            return (errnum);
        }

        dpcls.mcu.cal.idx = sym_const->value;
    }

    cal.seq_idx     = 0;
    cal.prop        = p;

    if (cal_type ==
        FGC_CAL_TYPE_DAC_A)           // DAC Calibration. Note that FGC_CAL_TYPE_DAC_A = FGC_CAL_TYPE_DAC
    {
        if (STATE_PC == FGC_PC_IDLE)
        {
            return (FGC_BAD_STATE);
        }

        dpcls.mcu.cal.action = CAL_REQ_DAC;
    }
    else                                        // ADC or DCCT Calibration
    {
        dpcls.mcu.cal.chan_mask = 0;    // Reset channel mask

        // ------------------------------------------------------------------------
        // Internal ADCs calibration (channels A & B)
        // ------------------------------------------------------------------------

        if (cal_type == FGC_CAL_TYPE_ADCS)            // Auto calibration of internal ADCs
        {
            if (STATE_PC == FGC_PC_IDLE)
            {
                return (FGC_BAD_STATE);
            }

            dpcls.mcu.cal.action    = CAL_REQ_BOTH_ADC16S;
        }
        else                                            // Calibration of one ADC/DCCT channel
        {
            switch (cal_type)
            {

                    // ------------------------------------------------------------------------
                    // Individual internal ADC calibration (channel A or B)
                    // ------------------------------------------------------------------------

                case FGC_CAL_TYPE_ADC_A:
                case FGC_CAL_TYPE_ADC_B:

                    dpcls.mcu.cal.chan_mask = (cal_type == FGC_CAL_TYPE_ADC_A ? 0x01 : 0x02);

                    if (STATE_PC == FGC_PC_IDLE &&                          // If in Idle and
                        (dpcls.mcu.dcct_select == FGC_DCCT_SELECT_AB ||       // both channels are in use or
                         !(ST_MEAS_A & ST_MEAS_B & FGC_MEAS_I_MEAS_OK) ||       // both channels are not OK or
                         dpcls.mcu.dcct_select == dpcls.mcu.cal.chan_mask))   // measurement is in use
                    {
                        return (FGC_BAD_STATE);                                     // report BAD STATE
                    }

                    action = CAL_REQ_ADC16;
                    break;

                    // ------------------------------------------------------------------------
                    // External ADCs calibration
                    // ------------------------------------------------------------------------

                case FGC_CAL_TYPE_EXT_ADC_A:
                case FGC_CAL_TYPE_EXT_ADC_B:
                case FGC_CAL_TYPE_EXT_ADCS:

                    if (cal_type != FGC_CAL_TYPE_EXT_ADC_B)            // i.e. if EXT_ADC_A or EXT_ADCS
                    {
                        dpcls.mcu.cal.chan_mask |= 0x01;
                    }

                    if (cal_type != FGC_CAL_TYPE_EXT_ADC_A)            // i.e. if EXT_ADC_B or EXT_ADCS
                    {
                        dpcls.mcu.cal.chan_mask |= 0x02;
                    }

                    if (STATE_PC == FGC_PC_IDLE)
                    {
                        return (FGC_BAD_STATE);
                    }

                    action = CAL_REQ_ADC22;
                    break;

                    // ------------------------------------------------------------------------
                    // DCCTs calibration
                    // ------------------------------------------------------------------------

                case FGC_CAL_TYPE_DCCT_A:
                case FGC_CAL_TYPE_DCCT_B:
                case FGC_CAL_TYPE_DCCTS:

                    if (cal_type != FGC_CAL_TYPE_DCCT_B)            // i.e. if DCCTA or DCCTS
                    {
                        dpcls.mcu.cal.chan_mask |= 0x01;
                    }

                    if (cal_type != FGC_CAL_TYPE_DCCT_A)            // i.e. if DCCTB or DCCTS
                    {
                        dpcls.mcu.cal.chan_mask |= 0x02;
                    }

                    action = CAL_REQ_DCCT;
                    break;
            }

            // --- Check if measured voltages indicate that a valid reference is applied ---

            // (Only for calibration of external ADCs and DCCTs)

            if (cal_type != FGC_CAL_TYPE_ADC_A &&
                cal_type != FGC_CAL_TYPE_ADC_B &&
                (((dpcls.mcu.cal.chan_mask & 0x01) &&
                  fabs(dpcls.dsp.adc.volts_200ms[0] - vref[dpcls.mcu.cal.idx]) > CAL_VREF_LIMIT) ||
                 ((dpcls.mcu.cal.chan_mask & 0x02) &&
                  fabs(dpcls.dsp.adc.volts_200ms[1] - vref[dpcls.mcu.cal.idx]) > CAL_VREF_LIMIT)))
            {
                return (FGC_REF_MISMATCH);
            }

            dpcls.mcu.cal.action = action;                // Launch DSP to do calibration
        }
    }

    STATE_OP = FGC_OP_CALIBRATING;              // Start auto calibration
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetDefaults(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetDefaults provides support for S REF.DEFAULTS[] action.  It will assert the default values in the
  REF properties.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      prop_idx;
    uint16_t      ls;
    union
    {
        float   f;
        uint32_t  i;
    } value[7];                 // Size of props[] array below

    static struct prop * props[] =
    {
        &PROP_REF_PLEP_ACCELERATION,    //  0
        &PROP_REF_PLEP_LINEAR_RATE,     //  1
        &PROP_REF_PLEP_EXP_TC,          //  2
        &PROP_REF_PLEP_EXP_FINAL,       //  3
        &PROP_REF_TRIM_DURATION,        //  4
        &PROP_REF_RUN_DELAY,            //  5
        NULL,                           //  6
    };

    ls = (uint16_t)dpcls.dsp.load_select;

    if (NON_PPM_REG_MODE == FGC_REG_V)                          // If openloop
    {
        value[0].f = dpcls.mcu.ref.defaults.v.acceleration;             // PROP_REF_PLEP_ACCELERATION
        value[1].f = dpcls.mcu.ref.defaults.v.linear_rate;              // PROP_REF_PLEP_LINEAR_RATE
        value[2].f = 0.0;                                               // PROP_REF_PLEP_EXP_TC
        value[3].f = 0.0;                                               // PROP_REF_PLEP_EXP_FINAL
    }
    else                                                        // else closed-loop (on B or I)
    {
        value[0].f = dpcls.mcu.ref.defaults.i.acceleration[ls];         // PROP_REF_PLEP_ACCELERATION
        value[1].f = dpcls.mcu.ref.defaults.i.linear_rate [ls];         // PROP_REF_PLEP_LINEAR_RATE
        value[2].f = dpcls.dsp.ref.exp_tc;                              // PROP_REF_PLEP_EXP_TC
        value[3].f = dpcls.dsp.ref.exp_final;                           // PROP_REF_PLEP_EXP_FINAL
    }

    value[4].f = 0.0;                                                   // PROP_REF_TRIM_DURATION
    value[5].f = 0.0;                                                   // PROP_REF_RUN_DELAY

    /* For each property that must be reset */

    c->mux_idx = 0;         // Essential for Class 53, where different users are armed before this call

    for (prop_idx = 0; props[prop_idx] != NULL; prop_idx++)
    {
        PropSet(c, props[prop_idx], &value[prop_idx], 1);
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetDevicePPM(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Used with DEVICE.PPM property.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;

    // If DEVICE.PPM has changed

    errnum = ParsSet(c, p);

    if(errnum == 0 && dev.ppm != DEVICE_PPM)
    {
        dpcom.mcu.device_ppm = dev.ppm;
        dev.max_user         = GET_FGC_MAX_USER();

        // If DEVICE.PPM is now ENABLED then disarm user 0 if armed

        if (dev.ppm && dpcls.mcu.ref.func.type[0] != FGC_REF_NONE)
        {
            errnum = RefArm(c, NON_PPM_USER, FGC_REF_NONE, STC_NONE);
        }
    }

    return (errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetDirectCommand(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Set direct command register:  0xRRSS     SS=Set mask, RR=Reset mask
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;
    uint32_t      value;

    errnum = ParsScanInteger(c, "Y", &value);

    if (errnum)
    {
        return (errnum);
    }

    if (value > 0xFFFF)
    {
        return (FGC_OUT_OF_LIMITS);
    }

    DIG_OP_P = (uint16_t)value;           // Write word direct (high byte is reset, low byte is set)

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetFifo(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:        FIFO    - Flush fifo
\*---------------------------------------------------------------------------------------------------------*/
{
    struct dpcls_fifo_head * fifo;

    fifo = (struct dpcls_fifo_head *)p->value;

    fifo->out_idx = fifo->in_idx;

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetInternalAdcMpx(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:        INTERNAL_ADC_MPX
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t errnum;

    errnum = SetifMpxOk(c);

    if (errnum)
    {
        return errnum;
    }

    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetPC(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Set PC command        (OFF/OF, ON_STANDBY/SB, IDLE/IL, SLOW_ABORT/SA, CYCLING/CY)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              errnum;
    struct sym_lst   *  sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    switch (sym_const->value)   // Switch according to symbol constant value
    {
        case FGC_PC_OFF:    // ============ OFF: Move to STOPPING or OFF

            if (Test(sta.inputs, DIG_IP1_VSRUN_MASK16) &&   // If voltage source is running
                STATE_PC != FGC_PC_STOPPING)                // and not already in stopping state
            {
                sta.flags |= STAF_STOP;                             // Switch to STOPPING state
                break;
            }

            if (STATE_PC == FGC_PC_FLT_OFF &&
                dpcls.mcu.adc.filter_state == FGC_FILTER_STATE_RESET_TIMEOUT)
            {
                // Attempt another reset

                Clr(sta.faults, FGC_FLT_I_MEAS);
                AdcFiltersResetRequest(true);
            }

            if (STATE_PC == FGC_PC_OFF ||           // If OFF or FLT_OFF
                STATE_PC == FGC_PC_FLT_OFF)
            {
#if FGC_CLASS_ID == 51

                // Block the reset request if a FW DIODE fault was received
                // and the signal is a fault, not a status

                if (vs.fw_diode == FGC_VDI_FAULT && vs.fwd_is_fault)
                {
                    return (FGC_FW_DIODE_FAULT);
                }

                // Block the reset if a FABORT_UNSAFE fault was received

                if (vs.fabort_unsafe == FGC_VDI_FAULT)
                {
                    return (FGC_FABORT_UNSAFE);
                }

#endif
                Set(sta.cmd, DDOP_CMD_RESET);               // Send a reset
            }

            break;

        case FGC_PC_ON_STANDBY: // ========== ON_STANDBY: Move to ON_STANDBY

            if (!Test(sta.inputs, DIG_IP1_VSRUN_MASK16))    // If VS is not requested to run at the moment
            {
                errnum = StaStartPC();                      // Request converter start
            }
            else                                    // else VS_RUN is already active
            {
                OS_ENTER_CRITICAL();

                if (STATE_PC >= FGC_PC_IDLE)        // If PC state is IL or higher
                {
                    sta.flags |= STAF_TO_STANDBY;                   // Set flag to move to TO_STANDBY
                }
                else if (STATE_PC != FGC_PC_ON_STANDBY) // else if state is not already SB
                {
                    errnum = FGC_BAD_STATE;
                }

                OS_EXIT_CRITICAL();
            }

            break;

        case FGC_PC_IDLE:   // ============ IDLE: Move to IDLE

            if (!Test(sta.inputs, DIG_IP1_VSRUN_MASK16))    // If VS is not requested to run at the moment
            {
                errnum = StaStartPC();

                if (!errnum)                  // If converter start request succeeds
                {
                    sta.flags |= STAF_IDLE;                         // Set IDLE flag to move to idle
                }
            }
            else                                    // else VS_RUN is already active
            {
                OS_ENTER_CRITICAL();

                if (STATE_PC == FGC_PC_ON_STANDBY                                                    ||
                    (STATE_PC == FGC_PC_ARMED && !Test(ST_UNLATCHED, FGC_UNL_START_EVENT))             ||
                    STATE_PC == FGC_PC_RUNNING                                                       ||
                    ((STATE_PC == FGC_PC_TO_STANDBY ||
                      (STATE_PC == FGC_PC_SLOW_ABORT && Test(sta.inputs,DIG_IP1_INTLKSPARE_MASK16) == false))  &&
                     (!fbs.sector_access ||
                      !(uint16_t)dpcls.dsp.meas.i_access_f ||
                      (STATE_OP == FGC_OP_SIMULATION && vs.sim_intlks))))
                {
                    if (STATE_PC == FGC_PC_SLOW_ABORT    ||         // For SLOW_ABORT and RUNNING - Goto IDLE via ABORTING
                        STATE_PC == FGC_PC_RUNNING       ||
                        (STATE_PC == FGC_PC_TO_STANDBY       &&      // For TO_STANDBY, check that we are not starting the converter
                         ((dpcls.dsp.unipolar_f  && Test(sta.flags, STAF_IMIN)) ||       //    i.e. 1Q/2Q converter and I > I_MIN, or
                          (!dpcls.dsp.unipolar_f &&
                           !(uint16_t)dpcls.dsp.meas.i_low_f)     //         4Q converter and I_LOW flag is not set
                         )
                        )
                       )
                    {
                        // Clear the IDLE and TO_STANDBY flags to stay in AB state

                        sta.flags &= ~(STAF_TO_STANDBY | STAF_IDLE);

                        // Trigger the switch to AB state

                        sta.flags |= STAF_ABORTING;
                    }
                    else
                    {
                        sta.flags |= STAF_IDLE;

                        if (STATE_PC == FGC_PC_ARMED)       // "S PC IL" command to disarm a function in RUNNING mode
                        {
                            errnum = RefArm(c, NON_PPM_USER, FGC_REF_NONE, STC_NONE);   // Disarm user 0
                        }
                    }

                }
                else if (STATE_PC != FGC_PC_IDLE)
                {
                    errnum = FGC_BAD_STATE;
                }

                OS_EXIT_CRITICAL();
            }

            break;

        case FGC_PC_CYCLING:        // ============ CYCLING: Move to TO_CYCLING state
#if FGC_CLASS_ID == 53
            if (!Test(sta.inputs, DIG_IP1_VSRUN_MASK16))    // If VS is not requested to run at the moment
            {
                sta.flags |= STAF_TO_CYCLING;               // (POPS) Move to CYCLING as soon as the VS is on
                errnum = StaStartPC();

                if (!errnum)                                // If converter start request succeeds
                {
                    sta.flags |= STAF_TO_CYCLING;                    // Set TO_CYCLING flag to move to CYCLING state
                }
            }
            else if (STATE_PC == FGC_PC_TO_STANDBY)         // If going to STANDBY
            {
                sta.flags |= STAF_TO_CYCLING;
            }
            else if (STATE_PC == FGC_PC_ON_STANDBY ||       // If STATE.PC is ON_STANDBY, or
                     STATE_PC == FGC_PC_IDLE)                // IDLE
            {
                if(  (dev.pm_enabled_f && Test(ST_UNLATCHED, FGC_UNL_POST_MORTEM))
                   || Test(ST_UNLATCHED, FGC_UNL_LOG_PLEASE))
                {
                    return (FGC_PM_IN_PROGRESS);                               // Report log waiting
                }

                if (dev.log_pm_state != FGC_LOG_RUNNING)
                {
                    LogStartAll();
                }

                sta.flags |= STAF_TO_CYCLING;                               // Set flag to move to TO_CYCLING
            }
            else                                            // else
            {
                errnum = FGC_BAD_STATE;                             // Report BAD STATE
            }
#else
            errnum = FGC_BAD_STATE;
            break;
#endif

            break;
    }

    if (!errnum)                                        // If command succeeded
    {
        if (sta.mode_pc != sym_const->value)            // Update only if the value changed.
        {
            sta.mode_pc = sym_const->value;
        }
    }

    return (errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetRef(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetRef provides support for S REF FUNC_TYPE,args,... and S REF.FUNC.TYPE(user) FUNC_TYPE
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              errnum;
    uint16_t              user;                  // User index
    uint16_t              func_type;             // Function type constant
    uint16_t              stc_func_type;         // Function type symbol index
    struct sym_lst   *  sym_const;

    // Scan first argument for settable function type symbol

    errnum = ParsScanSymList(c, "Y,", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    user          = c->mux_idx;
    func_type     = sym_const->value;
    stc_func_type = sym_const->sym_idx;

    /*--- Process request according to user (PPM/NON-PPM) and property (REF or REF.FUNC.TYPE) ---*/

    if (user == NON_PPM_USER)               // If NON-PPM (user == 0)
    {
        dpcls.mcu.ref.run_now_f = 0;            // Clear run now flag

        if (stc_func_type == STC_NONE)          // If NONE supplied (disarm NON-PPM function)
        {
            if (STATE_PC == FGC_PC_ARMED &&             // If function is ARMED and
                !Test(ST_UNLATCHED, FGC_UNL_START_EVENT)) // start event not yet received
            {
                sta.flags |= STAF_IDLE;                                     // Change state to IDLE
                return (RefArm(c, NON_PPM_USER, FGC_REF_NONE, STC_NONE));   // Disarm user 0 and return
            }

            return (FGC_BAD_STATE);                             // Reject request
        }

        if (p->sym_idx == STP_REF)              // if S REF FUNC_TYPE,args...
        {
            switch (stc_func_type)                  // Switch according to FUNC_TYPE to process args
            {
                case STC_NOW:       dpcls.mcu.ref.run_now_f = 1;    // Replace NOW with PLEP run now
                    func_type     = FGC_REF_PLEP;
                    stc_func_type = STC_PLEP;

                    // Fall through

                case STC_PLEP:      errnum = RefPlep(c);            break;

                case STC_RAMP:      errnum = RefOpenloop(c);        break;

                case STC_LTRIM:
                case STC_CTRIM:     errnum = RefTrim(c);            break;

                case STC_TABLE:     errnum = 0;                     break;

                case STC_STEPS:
                case STC_SQUARE:
                case STC_SINE:
                case STC_COSINE:    errnum = RefTest(c);            break;

                default:            errnum = FGC_BAD_PARAMETER;     break;

            }

            if (errnum)                                 // If error returned
            {
                dpcls.mcu.ref.run_now_f = 0;                    // Clear run now flag
                return (errnum);                                // Report the error
            }
        }
        else                                    // else S REF.FUNC.TYPE FUNC_TYPE
        {
            switch (stc_func_type)                      // Switch according to FUNC_TYPE
            {
                case STC_NOW:                                   // If NOW

                    dpcls.mcu.ref.run_now_f = 1;                    // Replace NOW with PLEP run now
                    func_type     = FGC_REF_PLEP;
                    stc_func_type = STC_PLEP;
                    break;

                case STC_RAMP:                                  // If RAMP

                    if (NON_PPM_REG_MODE == FGC_REG_V)              // Check that reg mode is closed-loop
                    {
                        return (FGC_BAD_STATE);
                    }

                    break;
            }
        }
    }
    else                                // else PPM (user > 0)
    {
        if (p->sym_idx == STP_REF)              // If setting REF then user must be zero
        {
            return (FGC_BAD_CYCLE_SELECTOR);                      // Report BAD USER
        }
    }

    errnum = RefArm(c, user, func_type, stc_func_type);

    if (errnum)
    {
        dpcls.mcu.ref.run_now_f = 0;            // Clear run now flag
    }

    return (errnum);
}
#endif  // #if FGC_CLASS_ID != 59






uint16_t SetSimIntlks(struct cmd * c, struct prop * p)
{
    if ( sta.mode_op != FGC_OP_SIMULATION)
    {
        return FGC_BAD_STATE;
    }

    return ParsSet(c, p);
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: set.c
\*---------------------------------------------------------------------------------------------------------*/
