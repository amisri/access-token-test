/*---------------------------------------------------------------------------------------------------------*\
 File:      diag.c

 Purpose:   FGC MCU Software - QSPI related functions

 Author:    Quentin.King@cern.ch

 Notes:     The DIPS board has an embedded DIM that is the first on both branch A and branch B, so it is
        scanned at 100Hz.
        The analogue channels are different for the two scans (there are 8 channels in all)
        and are used to measure the voltage source output voltage (Vmeas) twice (x1 and x10)
        and the +5V and +/-15V PSU voltages for the FGC and the FGC's analogue interface.
        These are checked by the DSP against threshold values.
        If the qspi bus is reset, these channels get out of sync for 2 20ms cycles, so the DSP must suppress
        PSU monitoring for 2 cycles.
        This is signalled by the MCU using dpcom.mcu.diag.reset.
        This should be set to the number of cycles that need to be blocked (2).
        It is decremented by the DSP every 20ms cycle.
\*---------------------------------------------------------------------------------------------------------*/

#define DIAG_GLOBALS            // define diag global variable
#define DEFPROPS_INC_ALL        // defprops.h

#include <stdbool.h>
#include <diag.h>
#include <cmd.h>                // for tcm global variable
#include <trm.h>
#include <fbs_class.h>          // for ST_DCCT_A, ST_DCCT_B
#include <property.h>           // for ST_MAX_SYM_LEN
#include <defprops.h>           // for PROP_LOG_MENU_NAMES, PROP_LOG_MENU_PROPS
#include <defconst.h>           // for FGC_MAX_DIMS
#include <definfo.h>            // for FGC_CLASS_ID
#include <fgc/fgc_db.h>         // for access to SysDB
#include <dev.h>                // for dev global variable
#include <codes_holder.h>       // for codes_holder_info global variable
#include <prop.h>               // for PropSetNumEls()
#include <diag_class.h>         // for DiagInitClass()
#include <mem.h>                // for MemCpyStrFar(), MemCpyBytes(), MemSetWords()
#include <fbs.h>                // for fbs global variable
#include <mst.h>                // for mst global variable
#include <dpcom.h>              // for dpcom global variable
#include <mcu_dependent.h>      // for SMALL_USLEEP()
#include <macros.h>             // for Test(), Set()
#include <memmap_mcu.h>         // for SM_UXTIME_P
#include <shared_memory.h>      // for shared_mem global variable
#include <qspi_bus.h>           // for QSPI_BRANCHES


// Macro arguments are expanded except if with # or ##, so we need 2 level of indirection
#define CLASS_FIELD_STRING(name)        c##name
#define CLASS_FIELD_STRING2(name)       CLASS_FIELD_STRING(name)
#define CLASS_FIELD                     CLASS_FIELD_STRING2(FGC_CLASS_ID)   // c##FGC_CLASS_ID

static    log_dim_name_t    buf[FGC_MAX_DIMS];

/*---------------------------------------------------------------------------------------------------------*/
void DiagDimHashInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function add the DIM to the properties HASH structure for posterios usage
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t ii;
    uint8_t n_dims = 0;
    char    sym[ST_MAX_SYM_LEN + 1];

    for (ii =  0; ii < FGC_MAX_DIMS; ii++)
    {
        if (SysDbDimEnabled(dev.sys.sys_idx,ii))
        {
            MemCpyStrFar(sym, SysDbDimName(dev.sys.sys_idx,ii));
            if (HashInitDIM(sym, n_dims)) n_dims++;
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void DiagDimDbInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare the DIM database for use with the DIAG properties
\*---------------------------------------------------------------------------------------------------------*/
{
    fgc_db_t                    dim_idx;
    fgc_db_t                    ii;             // Loop index
    fgc_db_t                    n_log_menus;            // Number of log menus
    fgc_db_t                    board_number;
    uint32_t                    dims_expected[QSPI_BRANCHES];   // Number of real dims expected per branch
    uint32_t *                  val_scm_ptr;
    char                        sym[ST_MAX_SYM_LEN + 1];
    struct prop *               dim_props;
    struct TAccessToDimInfo *   logical_dim_info_ptr;

    // If databases are not the same version or system is unknown then return immediately

    if(!dev.dbs_ok_f || !dev.sys.sys_idx)
    {
        DiagSetPropNumEls(0);               // Reset the dynamic DIAG property lengths
        return;
    }

    // Prepare pointers to codes in flash memory

    // note : warning while debugging, if SYSDB is not in the ROM this will crash horrendously
    // overwriting the stack in the following for() statement

    n_log_menus = SysDbLogMenuLength(dev.sys.sys_idx);
    val_scm_ptr = &tcm.prop_buf->blk.intu[0];
    // Set up LOG menu names/properties lists for LOG.MENU properties

    for( ii = 0; ii < n_log_menus; ii++)
    {
        diag.menu_names[ii] = SysDbLogMenuName(dev.sys.sys_idx,ii);
        diag.menu_props[ii] = SysDbLogMenuProperty(dev.sys.sys_idx,ii);
    }

    // Initialize DIM logs

    for (dim_idx = 0; dim_idx < FGC_MAX_DIMS && n_log_menus < MAX_LOG_NUM_MENUS; ++dim_idx)
    {
        // Check if the logical DIM is used
        fgc_db_t  dim_type_idx    = SysDbDimDbType(dev.sys.sys_idx, dim_idx);
        uint8_t   dim_ana_active  = 0;
        fgc_db_t  ana_idx;

        // Make sure there is at least one analogue signal

        for (ana_idx = 0; ana_idx < FGC_N_DIM_ANA_CHANS; ++ana_idx)
        {
            if (dim_type_idx && DimDbIsAnalogChannel(dim_type_idx, ana_idx))
            {
                dim_ana_active = 1;
            }
        }

        // Set the DIM name and prop if the DIM exists and has analogue signals
        if (dim_ana_active != 0)
        {
            sprintf(buf[dim_idx], "LOG.DIM[%d]", dim_idx);

            diag.menu_names[n_log_menus] = SysDbDimName(dev.sys.sys_idx, dim_idx);
            diag.menu_props[n_log_menus] = &buf[dim_idx];
            n_log_menus++;
        }
    }

    PropSetNumEls(NULL, &PROP_LOG_MENU_NAMES, n_log_menus); // Set property length
    PropSetNumEls(NULL, &PROP_LOG_MENU_PROPS, n_log_menus); // Set property length

    // Set up DIM property structures

    dims_expected[BRANCH_A] = dims_expected[BRANCH_B] = 0;          // Clear DIM expected counters
    dim_props = &diag.dim_props[0];
    logical_dim_info_ptr = &diag.logical_dim_info[0];


    PropBufWait(&tcm);                      // Wait for buffer to be free


    for ( ii = diag.n_dims = 0; ii < FGC_MAX_DIMS; ii++)
    {
        logical_dim_info_ptr->logical_dim = SysDbDimLogicalAddress(dev.sys.sys_idx,ii);

        if (SysDbDimEnabled(dev.sys.sys_idx,ii))     // If this DIM index is occupied (0xFF=unused)
        {
            diag.n_dims++;

            *(val_scm_ptr) = board_number = logical_dim_info_ptr->flat_qspi_board_number = SysDbDimBusAddress(dev.sys.sys_idx,ii);

            val_scm_ptr++;

            MemCpyStrFar(sym, SysDbDimName(dev.sys.sys_idx,ii)); // Transfer symbol to near memory

            dim_props->sym_idx      = HashFindProp(sym);        // Look up symbol in property hash
            dim_props->type         = PT_STRING;
            dim_props->get_func_idx = PROP_DIAG_DIM_NAMES.get_func_idx;
            dim_props->n_elements   = dim_props->max_elements = FGC_N_DIM_DIG_BANKS * FGC_N_DIM_DIG_INPUTS;
            dim_props->value        = logical_dim_info_ptr;

            if ( board_number < FGC_MAX_DIM_BUS_ADDR )
            {
                // 0..15 belongs to branch A=0, 16..31 belongs to branch B=1
                dims_expected[board_number / (FGC_MAX_DIM_BUS_ADDR / 2)]++; // Increment DIMs expected bus branch
            }
            dim_props++;
        }
        else                    // else DIM index is unused
        {
            *(val_scm_ptr) = FGC_DIAG_UNUSED_CHAN;

            val_scm_ptr++;
        }
        logical_dim_info_ptr++;
    }

    tcm.cached_prop  = &PROP_DIAG_BUS_ADDRESSES;        // Write SCM buffer to DIAG.BUS_ADDRESSES
    tcm.prop_blk_idx = 0;                   // Write buffer to first property block
    tcm.n_elements   = 0;

    PropBlkSet(&tcm, FGC_MAX_DIMS, true, FGC_FIELDBUS_FLAGS_LAST_PKT);// Write SCM buffer
    PropSet(&tcm, &PROP_DIAG_DIMS_EXPECTED, &dims_expected, 2); // Write DIMS_EXPECTED to DSP property

    DiagSetPropNumEls(diag.n_dims);             // Set the property lengths
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagSetPropNumEls(uint16_t n_dims)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set the length of the parent properties which use the dynamic list of children that
  corresponds to the list of DIMS for the device type.
\*---------------------------------------------------------------------------------------------------------*/
{
    PropSetNumEls(NULL, &PROP_DIAG_DIM_NAMES,  n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_FAULTS,     n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_ANA_LBLS,   n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_DIG80_LBLS, n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_DIGA0_LBLS, n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_ANA,        n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_DIG,        n_dims);
    PropSetNumEls(NULL, &PROP_LOG_DIM,         n_dims);
    //PropSetNumEls(NULL, &PROP_LOG_DIM2STEP1,  (n_dims > 1 ? (n_dims-1) : 0));
    //PropSetNumEls(NULL, &PROP_LOG_DIM2STEP8,  (n_dims > 1 ? (n_dims-1) : 0));
    //PropSetNumEls(NULL, &PROP_LOG_DIM3STEP1,  (n_dims > 2 ? (n_dims-2) : 0));
}



void DiagSetOffsetGain(struct prop *p, uint16_t prop_idx)
{
    FP32    buf[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];
    fgc_db_t  logical_dim;
    fgc_db_t  chan;
    fgc_db_t   dim_type_idx;

    // Link SCM structure to DSP property

    tcm.cached_prop = p;

    // Wait for buffer to be free

    PropBufWait(&tcm);

    for ( chan = 0; chan < FGC_N_DIM_ANA_CHANS; chan++ )
    {
        for( logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++ )
        {
            // If DIM channel is in use
            dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,logical_dim);
            if (SysDbDimEnabled(dev.sys.sys_idx,logical_dim)) // If DIM channel is in use
            {
                if ( prop_idx == STP_GAINS )                    // If DIAG.GAIN property
                {
                    buf[chan][logical_dim] = DimDbAnalogGain(dim_type_idx,chan);
                }
                else if ( prop_idx == STP_OFFSETS )             // else DIAG.OFFSET property
                {
                    buf[chan][logical_dim] = DimDbAnalogOffset(dim_type_idx,chan);
                }
            }
        }
    }

    PropSetFar(&tcm, p, (void * FAR)buf, p->max_elements);
}



/*---------------------------------------------------------------------------------------------------------*/
int32_t DiagFindAnaChan(char * FAR chan_name)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search for an analogue channel with the name given in chan_name.
  It will stop at the first match and will return the channel index based on the ANASIGS array.
  If no match is found it will return -1.
\*---------------------------------------------------------------------------------------------------------*/
{
    fgc_db_t                      logical_dim;
    fgc_db_t                      ana_reg;                                // 0-3
    char                        chan_name_near[ST_MAX_SYM_LEN+1];
    fgc_db_t                       dim_type_idx;

    MemCpyStrFar(chan_name_near,chan_name);                             // Get near copy of channel name

    for( logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,logical_dim);
        if (SysDbDimEnabled(dev.sys.sys_idx,logical_dim)) // If DIM channel is in use
        {

            for( ana_reg = 0; ana_reg < FGC_N_DIM_ANA_CHANS; ana_reg++) // For each analog channel
            {
                if(DimDbIsAnalogChannel(dim_type_idx,ana_reg)                // If channel in use, and
                    && !MemCmpStrFar(DimDbAnalogLabel(dim_type_idx,ana_reg),chan_name_near)) // name matches
                {
                    return(ana_reg * FGC_MAX_DIMS + logical_dim);       // Return ANASIGS index
                }
            }
        }
    }

    return(-1);                 // No match
}
/*---------------------------------------------------------------------------------------------------------*/
//bool DiagFindDigChan(struct TAccessToDimInfo * logical_dim_info_ptr, char * chan_name, uint16_t * data_idx, uint16_t * diag_mask)
bool DiagFindDigChan(uint16_t logical_dim, char * chan_name, uint16_t * data_idx, uint16_t * diag_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search for a digital channel with the name given in chan_name in the DIM specified
  by logical_dim_info_ptr.  It will stop at the first match and will return true and the channel index in the diag.data[]
  array and the bit mask for the channel.  If no match is found it will return false.
\*---------------------------------------------------------------------------------------------------------*/
{
    fgc_db_t                      mask;
    fgc_db_t                      bank_idx;       // 0 - 1
    fgc_db_t                      ana_reg;        // 0 - 3

    const fgc_db_t dim_type_idx = SysDbDimDbType(dev.sys.sys_idx,logical_dim);

    for ( bank_idx = 0; bank_idx < FGC_N_DIM_DIG_BANKS; bank_idx++ )
    {
        mask = 1;

        for ( ana_reg = 0; ana_reg < FGC_N_DIM_DIG_INPUTS; ana_reg++ )
        {
            if(DimDbIsDigitalChannel(dim_type_idx,(bank_idx+4)) &&
                !MemCmpStrFar(DimDbDigitalLabel(dim_type_idx,(bank_idx+4),ana_reg), chan_name))
            {
                *diag_mask = mask;

                // DIAG_MAX_ANALOG is 0x80
                // 16 DIM boards is 0x20
                // I guess this is to point to
                //  diag.data.r.digital_0[flat_qspi_board_number]
                // or
                //  diag.data.r.digital_1[flat_qspi_board_number]
                *data_idx = SysDbDimBusAddress(dev.sys.sys_idx,logical_dim) + 0x80 + (0x20 * bank_idx);

                return(true);
            }
            mask <<= 1;
        }
    }

    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagSciStart(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare each 200ms period of diagnostic output via the SCI RS232 port.  This function
  is called from DiagTermNext() on ms 1 of every 200ms period.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct diag_term *   sci_diag;



    if ( diag.term_byte_idx > DIAG_TERM_LEN ) // If starting
    {
        diag.term_byte_idx = DIAG_TERM_LEN;       // Block transmission for first cycle
    }
    else
    {
        if ( diag.term_byte_idx == DIAG_TERM_LEN )    // else running
        {
            sci_diag                 = &diag.term_buf[diag.term_buf_idx]; // Get diag buffer address
            sci_diag->data.time_sec  = SM_UXTIME_P;                             // unix time
            sci_diag->data.time_usec = 1000L * (uint32_t) (SM_MSTIME_P - 1);      // us time (0,200000,400000,...)

            MemCpyBytes( &sci_diag->data.class_data.reserved[0],
                         (uint8_t *) &fbs.u.fgc_stat.class_data.CLASS_FIELD,
                         sizeof(sci_diag->data.class_data.reserved) );

            diag.term_buf_p    = (uint8_t*)sci_diag;
            diag.term_byte_idx = 0;
        }
    }

    diag.term_buf_idx     = 1 - diag.term_buf_idx;        // Prepare to acquire new data in
    sci_diag             = &diag.term_buf[diag.term_buf_idx];
    diag.term_buf_chan    = (uint8_t *)  &(sci_diag->data.chan);
    diag.term_buf_data    = (uint16_t *) &(sci_diag->data.data);
    diag.term_buf_counter = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagTermNext(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will transfer the next set of diagnostic data values.  This is called on ms 1 of every
  20 ms while the diag mode is active.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  ii;

    if ( diag.term_buf_counter++ < FGC_DIAG_PERIOD )     // Protect against overrun
    {
        for( ii = 0; ii < FGC_DIAG_N_LISTS; ii++ )
        {
            *(diag.term_buf_chan++) = fbs.u.fieldbus_stat.diag_chan[ii];
            *(diag.term_buf_data++) = fbs.u.fieldbus_stat.diag_data[ii];
        }
    }

    if ( mst.ms_mod_200 == 1 )                  // Every 200ms, start diag period
    {
        DiagSciStart();
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: diag.c
\*---------------------------------------------------------------------------------------------------------*/
