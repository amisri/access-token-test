/*!
 * @file  read_PSU_adc.c
 */

#define READ_PSU_ADC_GLOBALS

#include <read_PSU_adc.h>
#include <crate.h>
#include <defconst.h>
#include <definfo.h>
#include <fbs_class.h>
#include <fbs.h>
#include <class.h>



void CheckPsuStatus(uint16_t psu_stat)
{
    // If DCCT PSU expected

    if((crate.type == FGC_CRATE_TYPE_PC_120A) ||
       (crate.type == FGC_CRATE_TYPE_PC_600A))
    {
        if(psu_stat & 0x001)
        {
            ST_LATCHED |= FGC_LAT_DCCT_PSU_STOP;
        }

        if(psu_stat & 0x004)
        {
            ST_LATCHED |= FGC_LAT_DCCT_PSU_FAIL;
        }
    }

    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        if(psu_stat & 0x010)
        {
            // If failure not yet memorised, trigger an external post-mortem dump by the GW

            if(!(ST_LATCHED & FGC_LAT_VDC_FAIL))
            {
                fbs.u.fieldbus_stat.ack |= FGC_EXT_PM_REQ;
            }
            ST_LATCHED |= FGC_LAT_VDC_FAIL;
        }

        if(psu_stat & 0x020)
        {
            ST_LATCHED |= FGC_LAT_FGC_PSU_FAIL;
        }
    }
}



#if (FGC_CLASS_ID != 59)
uint8_t CheckDcctStat(uint16_t dcct_diag_stat, uint8_t st_dcct)
{
    // If simulating or converter uses DCCTs without status information, force inverted diag DCCT signal

    if(STATE_OP   == FGC_OP_SIMULATION     ||
       crate.type == FGC_CRATE_TYPE_PC_60A ||
       crate.type == FGC_CRATE_TYPE_PC_120A)
    {
        dcct_diag_stat = FGC_DCCT_ZERO_I;
    }

#if (FGC_CLASS_ID == 53)
    // Class 53: Force DCCT_ZERO_I to avoid filling event log

    dcct_diag_stat |= FGC_DCCT_ZERO_I;
#endif // (FGC_CLASS_ID == 53)

    return((st_dcct & ~MCU_DCCT_MASK) | ((dcct_diag_stat ^ FGC_DCCT_ZERO_I) & MCU_DCCT_MASK));
}
#endif // (FGC_CLASS_ID != 59)

// EOF
