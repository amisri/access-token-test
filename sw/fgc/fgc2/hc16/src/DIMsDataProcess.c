/*---------------------------------------------------------------------------------------------------------*\
  File:         DIMsDataProcess.c

  Purpose:      FGC2 DSP Software - Process data received via QSPI bus

\*---------------------------------------------------------------------------------------------------------*/

#define DIMSDATAPROCESS_GLOBALS
#define DEFPROPS_INC_ALL    // defprops.h

#include <stdbool.h>
#include <DIMsDataProcess.h>
#include <memmap_mcu.h>         // for SM_UXTIME_P
#include <read_PSU_adc.h>       // for CheckPsuStatus(), CheckDcctStat()
#include <diag.h>               // for diag global variable
#include <mst.h>                // for mst global variable
#include <fbs_class.h>          // for ST_DCCT_A, ST_DCCT_B
#include <fbs.h>                // for fbs global variable
#include <stdio.h>          // for fprintf()
#include <cmd.h>            // for struct cmd
#include <class.h>          // Include class dependent definitions
#include <defconst.h>       // for FGC_MAX_DIMS
#include <defprops.h>
#include <definfo.h>        // for FGC_CLASS_ID
#include <dev.h>            // for dev global variable
#include <fbs.h>            // for fbs global variable, FbsOutBuf()
#include <mst.h>            // for mst global variable
#include <dpcom.h>          // for dpcom global variable, struct dpcom_diag_log
#include <macros.h>         // for Test(), Set()
#include <fgc/fgc_db.h>
#include <fgc_log.h>        // for struct fgc_log_header, struct fgc_log_sig_header
#include <fgc_errs.h>       // for FGC_DSP_NOT_AVL, FGC_BAD_ARRAY_IDX, FGC_BAD_GET_OPT
#include <diag.h>           // for diag global variable
#include <codes_holder.h>   // for codes_holder_info global variable
#include <mem.h>            // for MemCpyBytes(), MemSetBytes()
#include <os.h>             // for OSTskSuspend()
#include <prop.h>           // for PropValueGet()
#include <mcu_dsp_common.h>
#include <qspi_bus.h>       // for QSPI_BRANCHES
#include <string.h>

/*---------------------------------------------------------------------------------------------------------*/
void InitQspiBus(void)
/*---------------------------------------------------------------------------------------------------------*\
  FGC2 set the hardware again, even if was already done by the Boot program because under debugging
  they can start running the Main on SRAM by-passing the Boot
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      b;
    uint16_t      ii;

    // Initialise QSM

    QSM_PORTQS_P        = 0x00;                 // If pin is defined as output, the logic state is "0"
    QSM_PQSPAR_P        = 0x03;                 // MISO,MOSI are used for QSPI, the rest are PORTQ I/O
    QSM_DDRQS_P         = 0x7E;                 // Inputs:MISO Outputs:PCS3, PCS2,PCS1,PCS0,SCK,MOSI
    QSM_SPCR2_P         = 0x0F00;               // Scan from 0 to F, no wrap
    QSM_SPCR0_P         = 0x8110;               // QSPI master, 16 bit, CPHA 1, SCK inactive low, 500 Khz

    // Select branch A and branch B and set clk high to trigger an initial reset of all DIMS
    // This is cancelled in IsrMst
    QSM_PORTQS_P |= (QSM_PORTQS_CLK_MASK8 | QSM_PORTQS_DIAGBUSA_MASK8 | QSM_PORTQS_DIAGBUSB_MASK8);


    for ( ii = 0; ii < 16; ii++ )
    {
        QSM_CR_A[ii] = 0x40;                    // Set commands (size from SPCR0)
        QSM_TR_A[ii] = 0xBAB0 + ii;             // Can never be returned by a DIM (DIM register ID:3 doesn't exist)
    }

    // Initialisation for later use
    diag.copy_from = (uint16_t*)(QSM_RR_16 - 0x8000);     // QSM_RR - 0x8000

    // Prepare diag header with sync bytes (6 x 0xFE)

    for ( b = 0; b < 2; b++ )                           // For double buffer
    {
        diag.term_buf[b].data.data_status = 0x0F;        // Set status in the header to all OK
        diag.term_buf[b].data.class_id = FGC_CLASS_ID;   // Set class ID in the header

        for ( ii = 0; ii < FGC_DIAG_N_SYNC_BYTES; ii++ )        // Prepare each sync byte
        {
            diag.term_buf[b].sync[ii] = FGC_DIAG_SYNC;
        }
    }
}
#pragma MESSAGE DISABLE C1404 // Disable 'return expected' warning
#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ProcessReceivedQSPIdata(uint16_t dpram)
/*---------------------------------------------------------------------------------------------------------*\
  This function will copy 16 words from the QSM receive ram into the diag.data[] array and into the
  DPRAM pointer provided for the DSP (passed reg D).
  It also collects the trigger bit from every channel and returns the resulting trigger mask.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm                                     // D = c32buf
    {
        PSHM  Z                             // Save Frame Pointer (IZ)
        LDX   diag.copy_from                // QSM_RR - 0x8000,      0xFD00 - 0x8000 = 0x7D00
        LDY   diag.copy_to                  // Destination for data
        XGDZ                                // IZ = D = dpram = buffer in DP area for DSP
        CLRD                                // Clear trigger word

        LDE   -0x7FE2,X                     // Read Word 15,  0x7D00-0x7FE2=0xFD1E=QSM_RR+0x001E
        STE   30,Y                          // Write Word to [ diag.copy_to + 30 ]
        STE   30,Z                          // Write Word to [ dpcom.mcu.diag.qspi[n][0] + 30 ]
        ROLE                                // Trigger bit b15 into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FE4,X                     // Read Word 14,  0x7D00-0x7FE4=0xFD1C=QSM_RR+0x001C
        STE   28,Y                          // Write Word to [ diag.copy_to + 28 ]
        STE   28,Z                          // Write Word    [ dpcom.mcu.diag.qspi[n][0] + 30 ]
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FE6,X                     // Read Word 13
        STE   26,Y                          // Write Word
        STE   26,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FE8,X                     // Read Word 12
        STE   24,Y                          // Write Word
        STE   24,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FEA,X                     // Read Word 11
        STE   22,Y                          // Write Word
        STE   22,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FEC,X                     // Read Word 10
        STE   20,Y                          // Write Word
        STE   20,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FEE,X                     // Read Word 9
        STE   18,Y                          // Write Word
        STE   18,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FF0,X                     // Read Word 8
        STE   16,Y                          // Write Word
        STE   16,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FF2,X                     // Read Word 7
        STE   14,Y                          // Write Word
        STE   14,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FF4,X                     // Read Word 6
        STE   12,Y                          // Write Word
        STE   12,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FF6,X                     // Read Word 5
        STE   10,Y                          // Write Word
        STE   10,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FF8,X                     // Read Word 4
        STE    8,Y                          // Write Word
        STE    8,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FFA,X                     // Read Word 3
        STE    6,Y                          // Write Word
        STE    6,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FFC,X                     // Read Word 2
        STE    4,Y                          // Write Word
        STE    4,Z                          // Write Word
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x7FFE,X                     // Read Word 1,  0x7D00-0x7FFE=0xFD02=QSM_RR+0x0002
        STE    2,Y                          // Write Word to [ diag.copy_to + 2 ]
        STE    2,Z                          // Write Word    [ dpcom.mcu.diag.qspi[n][0] + 2 ]
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        LDE   -0x8000,X                     // Read Word 0,  0x7D00-0x8000=0xFD00=QSM_RR+0x0000
        STE    0,Y                          // Write Word to [ diag.copy_to + 0 ]
        STE    0,Z                          // Write Word    [ dpcom.mcu.diag.qspi[n][0] + 0 ]
        ROLE                                // Trigger bit into carry
        ROLD                                // Carry bit in D0
        PULM   Z                            // Recover Frame Pointer (IZ)
    }                                       // Return trigger word in D
}
#pragma MESSAGE DEFAULT C1404
#pragma MESSAGE DEFAULT C5703



void QSPIbusStateMachine(void)
{
    // Offset value to be used in the corresponding ms slice [0..19]
    // unused (trig branch A), digital_0[A][], digital_1[A][], analog_0[A][], analog_1[A][], analog_2[A][], analog_3[A][], trigCounter[A][], skip, skip,
    // unused (trig branch B), digital_0[B][], digital_1[B][], analog_0[B][], analog_1[B][], analog_2[B][], analog_3[B][], trigCounter[B][], skip, skip,
    static uint16_t   diag_wordOffsetInData[] = { 0, 0x80, 0xA0, 0x00, 0x20, 0x40, 0x60, 0x100, 0xFF, 0xFF,
                                                0, 0x90, 0xB0, 0x10, 0x30, 0x50, 0x70, 0x110, 0xFF, 0xFF };

    // Act according to current position in 20ms cycle

    switch(mst.ms_mod_20)
    {

    case 0:

        // Select branch A and DIPS ADC bank A (Clk low)
        QSM_PORTQS_P = QSM_PORTQS_DIAGBUSA_MASK8;

        OS_ENTER_CRITICAL();

        // Start scan
        QSM_SPCR1_P = DIAG_TRIGGER;
        // Inform DSP of scan time relative to tic0
        dpcom.mcu.diag.scantime_tcnt[BRANCH_A] = GPT_TCNT_P - mst.tic0;

        OS_EXIT_CRITICAL();

        break;

    case 1:   // Dig0  Branch A  DIM register ID:0  x 16 DIM boards
    case 2:   // Dig1  Branch A  DIM register ID:1  x 16 DIM boards
    case 3:   // Ana0  Branch A  DIM register ID:4  x 16 DIM boards (stored by the DSP on ms 4)
    case 4:   // Ana1  Branch A  DIM register ID:5  x 16 DIM boards (stored by the DSP on ms 5)
    case 5:   // Ana2  Branch A  DIM register ID:6  x 16 DIM boards (stored by the DSP on ms 6)
    case 6:   // Ana3  Branch A  DIM register ID:7  x 16 DIM boards (stored by the DSP on ms 7)

    case 11:  // Dig0  Branch B  DIM register ID:0  x 16 DIM boards
    case 12:  // Dig1  Branch B  DIM register ID:1  x 16 DIM boards
    case 13:  // Ana0  Branch B  DIM register ID:4  x 16 DIM boards (stored by the DSP on ms 14)
    case 14:  // Ana1  Branch B  DIM register ID:5  x 16 DIM boards (stored by the DSP on ms 15)
    case 15:  // Ana2  Branch B  DIM register ID:6  x 16 DIM boards (stored by the DSP on ms 16)
    case 16:  // Ana3  Branch B  DIM register ID:7  x 16 DIM boards (stored by the DSP on ms 17)

        // Prepare pointer to data buffer

        diag.copy_to = diag.data + diag_wordOffsetInData[mst.ms_mod_20];

        // Copy 16 words from the QSM receive ram into the diag.data[] array and into the DPRAM pointer provided
        // also collects the trigger bit from every channel and returns the resulting trigger mask

        diag.triggers_from_ms[mst.ms_mod_20] = ProcessReceivedQSPIdata( (uint16_t) &dpcom.mcu.diag.qspi[mst.ms_mod_10 & 1][0] );

        // Start next scan

        QSM_SPCR1_P = DIAG_TRIGGER;

        break;

    case 7:  // TrigCounter  Branch A  DIM register ID:2  x16 DIM boards (stored by the DSP on ms 8)
    case 17: // TrigCounter  Branch B  DIM register ID:2  x16 DIM boards (stored by the DSP on ms 18)

        // Prepare pointer to data buffer

        diag.copy_to = diag.data + diag_wordOffsetInData[mst.ms_mod_20];

        // Copy 16 words from the QSM receive ram into the diag.data[] array and into the DPRAM pointer provided
        // also collects the trigger bit from every channel and returns the resulting trigger mask

        diag.triggers_from_ms[mst.ms_mod_20] = ProcessReceivedQSPIdata( (uint16_t) &dpcom.mcu.diag.qspi[mst.ms_mod_10 & 1][0] );

        if (mst.ms_mod_20 == 17) {
            // the following values are packed in uint32_t, but MCU side is big endian and DSP side is little endian

            // access the little endian LOW part
            ( (uint16_t*) &dpcom.mcu.diag.flat_qspi_latched_trig_bits   )[1] = diag.data[DIAG_LATCHED_TRIGS]     = diag.triggers_from_ms[ 7]; // branch A triggerCounter registers b15 collection
            // access the little endian HI part
            ( (uint16_t*) &dpcom.mcu.diag.flat_qspi_latched_trig_bits   )[0] = diag.data[DIAG_LATCHED_TRIGS+1]   = diag.triggers_from_ms[17]; // branch B triggerCounter registers b15 collection
            // access the little endian LOW part
            ( (uint16_t*) &dpcom.mcu.diag.flat_qspi_UNlatched_trig_bits )[1] = diag.data[DIAG_UNLATCHED_TRIGS]   = diag.triggers_from_ms[ 1]; // branch A dig0 registers b15 collection
            // access the little endian HI part
            ( (uint16_t*) &dpcom.mcu.diag.flat_qspi_UNlatched_trig_bits )[0] = diag.data[DIAG_UNLATCHED_TRIGS+1] = diag.triggers_from_ms[11]; // branch B dig0 registers b15 collection
        }

        break;

    case 8:
    case 9:
         break;

    case 10:

        // Select branch B and DIPS ADC bank B
        QSM_PORTQS_P = ( QSM_PORTQS_DIAGBUSB_MASK8   |
                         QSM_PORTQS_DIPSDIMAD3_MASK8 );

        OS_ENTER_CRITICAL();

        // Start scan
        QSM_SPCR1_P = DIAG_TRIGGER;
        // Inform DSP of scan time relative to tic0
        dpcom.mcu.diag.scantime_tcnt[BRANCH_B] = GPT_TCNT_P - mst.tic0;

        OS_EXIT_CRITICAL();

        break;

    case 18: // On millisecond 18 there is no DIM data acquisition, so it's safe to start a reset there

        // We fire the reset in ms 18.
        // If we fire in ms 19, we have the QSPI_CLK in HIGH during ms 19 and going to LOW in ms 0
        // and in the same ms 0 the first burst read is fire but too close from the start of QSPI_CLK in LOW
        // (less than 15.7us) so this little LOW time is not recognised as LOAD by the DIMS
        // starting reset in ms 18 let going to LOW in ms 19 and remain in LOW until ms 0 fires the read

        // If DSP signals that a DIM trigger occurred

        if( (uint16_t) dpcom.dsp.diag.dim_trig_f )
        {
            // Clear DSP flag reset request flag
            dpcom.dsp.diag.dim_trig_f = false;
            // Set 2s timeout for DIAG reset
            diag.req_QSPI_reset = 100;
        }

        // If DSP requests a DIAG reset

        if ( dpcom.dsp.diag.reset_req_packed != 0 )
        {
            // Clear DSP reset request bits
            dpcom.dsp.diag.reset_req_packed = 0x00000000;
            // Set immediate DIAG reset request
            diag.req_QSPI_reset = 1;
            // DIAG.SYNC_RESETS, increment DSP diag reset request counter
            diag.sync_resets++;
            // Increment up counter (decremented at 1Hz)
            diag.fault_too_much_resets_counter += DIAG_SYNC_RESET_PERIOD / 2;
        }

        // If any DIAG reset request is pending

        if ( diag.req_QSPI_reset != 0 )
        {
            diag.req_QSPI_reset--;

            // If reset request timer expired

            if( !diag.req_QSPI_reset )
            {
                // Select branch A and branch B and set clk high
                // This is cancelled in IsrMst

                QSM_PORTQS_P |= ( QSM_PORTQS_DIAGBUSA_MASK8 |
                                  QSM_PORTQS_DIAGBUSB_MASK8 |
                                  QSM_PORTQS_CLK_MASK8      );
            }
        }

        break;

    case 19:

        if ( (uint16_t) dpcom.dsp.diag.dips_are_valid )
        {
            // Check PSU status from DIPS board

            CheckPsuStatus(diag.data[DIAG_PSU_STAT_CHAN]);

#if FGC_CLASS_ID != 59

            // Check DCCT status

            ST_DCCT_A = CheckDcctStat((diag.data[DIAG_DCCT_STAT_CHAN]     ), ST_DCCT_A);
            ST_DCCT_B = CheckDcctStat((diag.data[DIAG_DCCT_STAT_CHAN] >> 6), ST_DCCT_B);
#endif
        }

        break;

    default:
        break;
    }
}

#pragma MESSAGE DISABLE C5909 // Disable 'assignment in codition' warning
/*---------------------------------------------------------------------------------------------------------*/
//uint16_t DimGetDimLog(struct cmd *c, struct prop *p, uint16_t number_of_requests, uint16_t idx_step)
uint16_t DimGetDimLog(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Get DIM log properties.  These can only be acquired via the FIP with the BIN get option.  There is no
  ASCII access to these properties.  The function needs the support of the DSP to acquire the data from the
  C32s memory.  The DSP will service one GetLog request per millisecond and always returns 8 samples for
  between 1 and 3 DIMs.
\*---------------------------------------------------------------------------------------------------------*/
{
    fgc_db_t       dim_type_idx; 
    uint8_t       sub_sym_idx; 
    fgc_db_t      e;
    uint16_t      n;
    uint16_t      s;
    bool        stopped_f;
    uint16_t      n_sigs;
    uint16_t      chan_idx[FGC_N_DIM_ANA_CHANS];
    uint16_t      chan_offset_us[FGC_N_DIM_ANA_CHANS];
    uint16_t *    buf_data_p;
    uint16_t      twait;
    uint16_t      dTsync;
    uint16_t      dTlast;
    uint16_t      dIdx;
    uint32_t      d_unix_time;
    uint32_t      sync_time;
    uint32_t      unix_time;
    uint16_t      ms_time;

    char      aux[FGC_LOG_SIG_UNITS_LEN];

    struct prop *   dim_info_ptr;

    struct dpcom_diag_log       log_status1; // Log status before readout
    struct dpcom_diag_log       log_status2; // Log status after readout

    union                                       // Union to reduce impact on stack
    {
        struct fgc_log_header      log;
        struct fgc_log_sig_header  sig;
    } header;

    // Check that Get command and options are valid for a DIM log
    if (c->to != c->from || c->n_arr_spec != 2)     // If array indecies are more than one
    {
        return(FGC_BAD_ARRAY_IDX);                      // Report error
    }

    if (!mst.dsp_bg_is_alive || !mst.dsp_rt_is_alive)
    {
        return(FGC_DSP_NOT_AVL);                        // Report that DSP not available
    }

    if(!Test(c->getopts,GET_OPT_BIN))           // If BIN get option not specified
    {
        return(FGC_BAD_GET_OPT);                        // Report error
    }
    
    // Identify active analogue channels from the DIMs

    dim_type_idx = SysDbDimDbType(dev.sys.sys_idx, c->to);
    dpcom.mcu.diag.log.logical_dim_to_log_for_req[0] = c->to;        // Set DIM index

    // Get status of DIM log from DSP
    DimGetDimLogStatus( (struct dpcom_diag_log *) &(log_status1), 1);
    stopped_f = !((uint16_t)log_status1.samples_to_acq);       // Get state of first DIM log

    dim_info_ptr = &diag.dim_props[dpcom.mcu.diag.log.logical_dim_to_log_for_req[0]];        // Get DIM Prop
    sub_sym_idx = (dim_info_ptr ? dim_info_ptr->sym_idx : 0);     // Get DIM name sym index

    n_sigs = 0;
    
    for( e = 0; e < FGC_N_DIM_ANA_CHANS; e++)          // For each analog channel (0-3)
    {
        if(DimDbIsAnalogChannel(dim_type_idx,e))
        {
            chan_idx    [n_sigs] = e;                                      // Remember index to data

            // The chan_offset_us must be done on two lines to not provoke a compiler bug
            chan_offset_us[n_sigs]  = (SysDbDimBusAddress(dev.sys.sys_idx,dim_type_idx) & 0x10 ? 11680 : 1680); // Calc sig time offset
            chan_offset_us[n_sigs] += (1000 * e);

            n_sigs++;
        }
    }

    // Calculate synchronisation

    unix_time = log_status1.time.unix_time;
    ms_time   = (uint16_t)(log_status1.time.us_time / 1000);

    if(!stopped_f)                              // If logging active
    {
        sync_time = fbs.log_sync_time;
        if ( !sync_time )               // If sync time is not active
        {
            sync_time = unix_time;                              // Set sync time to start of current second
            dTsync    = ms_time;
        }
        else                                            // else sync is active
        {
            d_unix_time = unix_time - sync_time;                // Must be done to ensure 32-bit sub
            dTsync = (uint16_t)d_unix_time * 1000 + ms_time;      // !!!! Compiler bug:do not merge these lines
        }

        twait = 2500;

        dIdx   = ((n_sigs * FGC_LOG_DIM_LEN)/6 // 6 bytes/ms over FIP fieldbus
                    + twait - dTsync) / FGC_FIELDBUS_CYCLE_PERIOD_MS;
        dTlast = dIdx * FGC_FIELDBUS_CYCLE_FREQ;

        log_status1.idx = (uint16_t)log_status1.idx + dIdx;

        unix_time += dTlast / 1000;                     // Adjust time to be of last sample
        ms_time   += dTlast % 1000;

        if(ms_time > 999)
        {
            ms_time -= 1000;
            unix_time++;
        }
    }

    dpcom.mcu.diag.log.entry_for_req[0] = ((uint16_t)log_status1.idx + 1) % FGC_LOG_DIM_LEN;

    //--- Generate log header ---//

    header.log.version   = FGC_LOG_VERSION;
    header.log.info      = (stopped_f ? FGC_LOG_BUF_INFO_STOPPED : 0);
    header.log.n_signals = n_sigs;
    header.log.n_samples = FGC_LOG_DIM_LEN;
    header.log.period_us = 20000L;                      // 20000us = 20 ms
    header.log.unix_time = unix_time;
    header.log.us_time   = (uint32_t)ms_time * 1000L;

    CmdStartBin(c, p, sizeof(header.log) +                      // Write binary data header
                      sizeof(header.sig) * n_sigs +
                      sizeof(uint16_t)     * n_sigs * FGC_LOG_DIM_LEN);

    FbsOutBuf( (uint8_t *) &header.log, sizeof(header.log), c);   // Write log header

    //--- Generate signal headers ---//

    header.sig.type = FGC_LOG_TYPE_INT16U;
    header.sig.info = 0;

    for( s = 0; s < n_sigs; s++ )
    {
        header.sig.us_time_offset = (int32_t)chan_offset_us[s];
        header.sig.gain           = DimDbAnalogGain(dim_type_idx,chan_idx[s]);
        header.sig.offset         = DimDbAnalogOffset(dim_type_idx,chan_idx[s]);
        header.sig.label_len      = DimAnaLabel(header.sig.label,
                                                FGC_LOG_SIG_LABEL_LEN,
                                                DimDbAnalogLabel(dim_type_idx,chan_idx[s]),
                                                sub_sym_idx);
        header.sig.units_len       = 0;
        
        MemCpyStrFar(aux,DimDbAnalogUnits(dim_type_idx,chan_idx[s]));
        strncpy(header.sig.units, aux, FGC_LOG_SIG_UNITS_LEN);

        FbsOutBuf( (uint8_t *) &header.sig, sizeof(header.sig), c);       // Write signal header
    }

    //--- Wait for synchronisation ---//

    if(!stopped_f)                                      // If logging active
    {
        CmdWaitUntil(sync_time+2, 500);                         // Wait till 2.5 s after sync time
    }

    //--- Get and write data in blocks of 8 samples ---//

    for( n = 0;n < FGC_LOG_DIM_LEN; n += 8)
    {
        dpcom.mcu.diag.log.number_of_requests = 1;             // Request DSP to get next 8 samples

        OSTskSuspend();                                  // Resumed by MstTsk on next millisecond

        while(mst.dsp_rt_is_alive &&                     // Wait while DSP is running and
              dpcom.mcu.diag.log.number_of_requests);    // data isn't yet available

        buf_data_p = (uint16_t*)&dpcom.fcm_prop.blk.intu; // Get pointer to start of 8 samples in DPRAM

        for(e=0;e < 8;e++)                              // For each of 8 samples
        {
            for(s=0;s < n_sigs;s++)                             // For each signal
            {
                FbsOutShort((uint8_t *)&buf_data_p[chan_idx[s]],c);       // Write the signal to the FIP
            }

            buf_data_p += (FGC_N_DIM_ANA_CHANS * 1);       // Advance to next sample
        }
    }

    //--- Check if logging state has changed ---//

    DimGetDimLogStatus( (struct dpcom_diag_log *) &log_status2, 1);                // Get status of DIM log(s)

    if((!((uint16_t)log_status1.samples_to_acq)) != (!((uint16_t)log_status2.samples_to_acq)))
    {
        return(FGC_LOG_NOT_SYNCED);
    }

    return(0);
}
/**/
#pragma MESSAGE DISABLE C5909 // Disable 'assignment in codition' warning
/*---------------------------------------------------------------------------------------------------------*/
void DimGetDimLogStatus(struct dpcom_diag_log * log_status, uint16_t number_of_requests)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used by DimGetDimLog() to acquire the status of the log for the 1-3 DIMs being acquired.
  The status includes the samples to acquire, the last sample index and time.
  Care is taken when acquiring these values to avoid inconsistent data during the update of the log
  by the DSP (on millisecond 18).
  If necessary, the data is acquired twice.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                      req;
    uint16_t                      log_lock_f;
    uint16_t                      log_lock_counter[2];
    struct dpcom_diag_log *     stat_buf;

    do                                          // Loop back if data changed during the acquisition
    {
        stat_buf = log_status;

        OS_ENTER_CRITICAL();
        log_lock_counter[0] = dpcom.dsp.diag.log_lock_counter;
        log_lock_f          = dpcom.dsp.diag.log_lock_f;

        for( req = 0; req < number_of_requests; req++)                            // For each DIM (1-3)
        {
            MemCpyBytes( (uint8_t *) stat_buf++,                                          // Copy log status structure
                         (uint8_t *) &dpcom.dsp.diag.log[dpcom.mcu.diag.log.logical_dim_to_log_for_req[req]],   // from DPRAM while
                         sizeof(struct dpcom_diag_log));                                // protected from interrupts
        }

        log_lock_counter[1] = dpcom.dsp.diag.log_lock_counter;
        OS_EXIT_CRITICAL();
    }
    while(   mst.dsp_rt_is_alive                                // Read again if DSP alive
          && (    log_lock_f                                    // and data modified by DSP
               || (log_lock_counter[0] != log_lock_counter[1])
              )
         );



    // Take copy of status for DIAG.LOGSTAT debug property

    stat_buf = log_status;
    MemSetBytes( diag.dim_log_status, 0, sizeof(diag.dim_log_status) );

    for( req = 0; req < number_of_requests; req++)                                // For each DIM (1-3)
    {
        MemCpyBytes( (uint8_t *) &diag.dim_log_status[ req * 4 ],
                     (uint8_t *) stat_buf++,
                     sizeof(struct dpcom_diag_log));                            // protected from interrupts
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t DimGetDimNames(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     DIM log properties
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t                 n;
    uint16_t                      errnum;                 // Error number
    struct TAccessToDimInfo *   dim_info_ptr;
    struct prop *               dim_props;

    errnum = CmdPrepareGet(c, p, 1, &n);
    if ( errnum )
    {
        return(errnum);
    }

    while(n--)     // For all elements
    {
        errnum = CmdPrintIdx( c, c->from );
        if ( errnum )                           // Print index if required
        {
            return(errnum);
        }

        dim_props = &diag.dim_props[c->from]; // [logical dim]
        dim_info_ptr = (struct TAccessToDimInfo *)dim_props->value;

        fprintf(c->f,   "%s:%u:0x%02X",
                        SYM_TAB_PROP[dim_props->sym_idx].key.c,
                        dim_info_ptr->logical_dim,
                        dim_info_ptr->flat_qspi_board_number);

        // Next element

        c->from += c->step;
    }
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t DimGetAnaLbls(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     DIM log properties
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t                 n;
    uint16_t                      offset;
    uint16_t                      errnum;                         // Error number
    char                        ana_lbl[FGC_LOG_SIG_LABEL_LEN+1];
    struct TAccessToDimInfo *   dim_info_ptr;

    const fgc_db_t dim_type_idx =  SysDbDimDbType(dev.sys.sys_idx, ((struct TAccessToDimInfo *)p->value)->logical_dim);

    if(c->n_arr_spec == 2 && c->to >= FGC_N_DIM_ANA_CHANS)
    {
        return(FGC_BAD_ARRAY_IDX);
    }

    p->n_elements = FGC_N_DIM_ANA_CHANS;        // Set property size to number of analog channels

    dim_info_ptr = (struct TAccessToDimInfo *)p->value;

    errnum = CmdPrepareGet(c, p, 1, &n);
    if(errnum)
    {
        return(errnum);
    }

    p->n_elements = p->max_elements;

    offset   = FGC_MAX_DIM_BUS_ADDR * c->from;

    while(n--)                                  // For all elements
    {
        errnum = CmdPrintIdx(c,c->from);
        if ( errnum )           // Print index if required
        {
            return(errnum);
        }

        DimAnaLabel(ana_lbl, (FGC_LOG_SIG_LABEL_LEN + 1), DimDbAnalogLabel(dim_type_idx,c->from), p->sym_idx);
        fprintf(c->f,"0x%02X:%s:%s",offset,(char * FAR)ana_lbl,DimDbAnalogUnits(dim_type_idx,c->from));


        offset += (c->step * FGC_MAX_DIM_BUS_ADDR);
        c->from += c->step;
    }
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t DimGetDigLbls(struct cmd *c, struct prop *p, uint16_t bank)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     DIM log properties
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t                 n;
    uint16_t                      chan_mask;
    uint16_t                      errnum;                 // Error number

    const fgc_db_t dim_type_idx =  SysDbDimDbType(dev.sys.sys_idx, ((struct TAccessToDimInfo *)p->value)->logical_dim);

    if(c->n_arr_spec == 2 && c->to >= FGC_N_DIM_DIG_INPUTS)
    {
        return(FGC_BAD_ARRAY_IDX);
    }

    p->n_elements = FGC_N_DIM_DIG_INPUTS;       // Set property size to number of digital channels

    errnum = CmdPrepareGet(c, p, 1, &n);
    if(errnum)
    {
        return(errnum);
    }

    p->n_elements = p->max_elements;

    while(n--)                                  // For all elements
    {
        errnum = CmdPrintIdx(c,c->from);
        if ( errnum )           // Print index if required
        {
            return(errnum);
        }

        chan_mask = 1 << (uint8_t)c->from;

        fprintf(c->f,"0x%04X:%c:%s:%s:%s",
            chan_mask,
                (DimDbDigitalIsFault(dim_type_idx,bank+4,c->from) ? 'T' : 'S'), // "TRG" "STA"
                DimDbDigitalLabel(dim_type_idx,bank+4,c->from),
                DimDbDigitalLabelOne(dim_type_idx,bank+4,c->from),
                DimDbDigitalLabelZero(dim_type_idx,bank+4,c->from));

        c->from += c->step;
    }
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t DimGetAna(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     DIM analogue channel properties
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t                 n;                      // Number of element in array range
    FP32                        ana_val;
    FP32*                       ana_val_ptr;            // Analogue value pointer
    fgc_db_t                       dim_type_idx;    
    uint16_t                      errnum;                 // Error number
    char                        ana_lbl[FGC_LOG_SIG_LABEL_LEN+1];
    struct TAccessToDimInfo *   dim_info_ptr;

    if(c->n_arr_spec == 2 && c->to >= FGC_MAX_DIMS)
    {
        return(FGC_BAD_ARRAY_IDX);
    }

    Set(c->getopts,GET_OPT_NOIDX);              // Suppress automatic indexes
    dim_info_ptr = (struct TAccessToDimInfo *)p->value; // Get DIM info structure for this DIM
    dim_type_idx = SysDbDimDbType(dev.sys.sys_idx, dim_info_ptr->logical_dim);
    p->n_elements = FGC_N_DIM_ANA_CHANS;        // Set number of channels

    errnum = CmdPrepareGet(c, p, 1, &n);
    if(errnum)
    {
        return(errnum);
    }

    p->n_elements = p->max_elements;

    while(n--)                                  // For all elements in the array range
    {
        if (DimDbIsAnalogChannel(dim_type_idx,c->from))  // If input is in use
        {
            errnum = PropValueGet(c, &PROP_DIAG_ANASIGS, NULL,
                                  dim_info_ptr->logical_dim + FGC_MAX_DIMS * c->from, (uint8_t**)&ana_val_ptr);
            if(errnum)
            {
                return(errnum);
            }

            ana_val = *ana_val_ptr;

            DimAnaLabel(ana_lbl,(FGC_LOG_SIG_LABEL_LEN+1),DimDbAnalogLabel(dim_type_idx, c->from),p->sym_idx);

            errnum = CmdPrintIdx(c,c->from);

            if(errnum)
            {
                return(errnum);
            }
            fprintf(c->f, "%u:%-15s:%10.3E:%s",c->from, (char * FAR)ana_lbl, ana_val, DimDbAnalogUnits(dim_type_idx,c->from));
        }

        c->from += c->step;
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t DimGetDig(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     DIM digital channel properties
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t                         n;
    uint16_t                              input_state;
    uint16_t                              errnum;
    fgc_db_t                              board_number;
    uint16_t                              bank_idx;
    uint16_t                              chan_idx;
    uint16_t                              chan_mask;
    bool                                latched_trig;
    bool                                unlatched_trig;
    uint16_t                              ul_values[FGC_N_DIM_DIG_BANKS];         // Unlatched values
    uint16_t                              lt_values[FGC_N_DIM_DIG_BANKS];         // Latched values
    static char * FAR                   fltsta[] = { "TRG", "STA" };
    struct TAccessToDimInfo *           dim_info_ptr;
    fgc_db_t dim_type_idx;

    Set(c->getopts,GET_OPT_NOIDX);                              // Suppress indexes

    dim_info_ptr = (struct TAccessToDimInfo *)p->value;
    dim_type_idx = SysDbDimDbType(dev.sys.sys_idx, dim_info_ptr->logical_dim);
    board_number = SysDbDimBusAddress(dev.sys.sys_idx, dim_info_ptr->logical_dim);

    if(board_number & 0x80)                                           // If DIM is a composite (ANA only)
    {
        return(0);                                                      // return with no data
    }

    ul_values[0] = diag.data[DIAG_UL_DIG_DATA_0 + board_number];
    ul_values[1] = diag.data[DIAG_UL_DIG_DATA_1 + board_number];
    lt_values[0] = diag.data[DIAG_LT_DIG_DATA_0 + board_number];
    lt_values[1] = diag.data[DIAG_LT_DIG_DATA_1 + board_number];


    errnum = CmdPrepareGet(c, p, 1, &n);
    if(errnum)
    {
        return(errnum);
    }

    while(n--)  // For all elements
    {
        bank_idx       = (c->from / FGC_N_DIM_DIG_INPUTS);
        chan_idx       = (c->from % FGC_N_DIM_DIG_INPUTS);

        if (!DimDbIsDigitalInput(dim_type_idx, bank_idx+4, chan_idx))     // If input is not used      
        {
            // Skip to next element

            c->from += c->step;
            continue;
        }

        chan_mask      = 1 << (uint8_t)chan_idx;
        input_state    = ul_values[bank_idx] & chan_mask;

        // bit manipulation on bool
        // TODO Review this instruction
        latched_trig   = lt_values[bank_idx] & chan_mask;
        unlatched_trig = diag.data[board_number] & 0x8000;


        errnum = CmdPrintIdx(c, c->from);                       // Field delimiter
        if ( errnum )
        {
            return(errnum);
        }

        fprintf(c->f,"%2u:%s|%-31s:%-7s:%2s:%2s",
                c->from,
                fltsta[!(DimDbDigitalIsFault(dim_type_idx, bank_idx+4, chan_idx))],
                DimDbDigitalLabel(dim_type_idx, bank_idx+4, chan_idx),
                (input_state ?  DimDbDigitalLabelOne(dim_type_idx, bank_idx+4, chan_idx) :
                                DimDbDigitalLabelZero(dim_type_idx, bank_idx+4, chan_idx)),
                DimDbDigitalIsFault(dim_type_idx, bank_idx+4, chan_idx)?(unlatched_trig ? "UT" : ""):"",
                DimDbDigitalIsFault(dim_type_idx, bank_idx+4, chan_idx)?(latched_trig   ? "LT" : ""):"");

        // Go to next element

        c->from += c->step;
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t DimGetFaults(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     DIM digital channels in Fault
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t                 n;
    uint16_t                      idx;
    uint16_t                      errnum;
    uint16_t                      board_number;
    fgc_db_t                      bank_idx;
    fgc_db_t                      chan_idx;
    uint16_t                      chan_mask;
    fgc_db_t                       dim_type_idx;
    bool                        latched_trig;
    bool                        unlatched_trig;
    struct TAccessToDimInfo *   dim_info_ptr;
    struct prop *               dim_props;
    uint16_t                      ul_values[FGC_N_DIM_DIG_BANKS];         // Unlatched values
    uint16_t                      lt_values[FGC_N_DIM_DIG_BANKS];         // Latched values

    if(c->step > 1)                             // If array step is not 1
    {
        return(FGC_BAD_ARRAY_IDX);                      // Report bad array index
    }

    idx = 0;            // for CmdPrintIdx

    errnum = CmdPrepareGet(c, p, 1, &n);
    if(errnum)
    {
        return(errnum);
    }

    dim_props = &diag.dim_props[c->from]; // [logical dim]

    while(n--)                                  // For all elements
    {
        dim_info_ptr = (struct TAccessToDimInfo *) dim_props->value;
        dim_type_idx = SysDbDimDbType(dev.sys.sys_idx, dim_info_ptr->logical_dim);
        board_number = SysDbDimBusAddress(dev.sys.sys_idx, dim_info_ptr->logical_dim);

        if(board_number & 0x80)                               // If DIM is a composite (ANA only)
        {
            continue;                                                   // Skip this DIM
        }

        ul_values[0] = diag.data[DIAG_UL_DIG_DATA_0 + board_number];
        ul_values[1] = diag.data[DIAG_UL_DIG_DATA_1 + board_number];
        lt_values[0] = diag.data[DIAG_LT_DIG_DATA_0 + board_number];
        lt_values[1] = diag.data[DIAG_LT_DIG_DATA_1 + board_number];

        // bit manipulation on bool
        // TODO Review this instruction
        unlatched_trig = diag.data[board_number] & 0x8000;

        for( bank_idx = 0; bank_idx < FGC_N_DIM_DIG_BANKS; bank_idx++ )
        {
            chan_mask = 1;

            for( chan_idx = 0; chan_idx < FGC_N_DIM_DIG_INPUTS; chan_idx++, chan_mask <<= 1 )
            {
                if(!DimDbIsDigitalInput(dim_type_idx, bank_idx+4, chan_idx)       // If input is not used, or
                   || !(DimDbDigitalIsFault(dim_type_idx, bank_idx+4, chan_idx))  // input is not part of the trigger, or
                   || !(ul_values[bank_idx] & chan_mask)                    // input is not active
                   )
                {
                    continue;                                           // Skip to next
                }

                // bit manipulation on bool
                // TODO Review this instruction

                latched_trig = lt_values[bank_idx] & chan_mask;

                errnum = CmdPrintIdx(c, idx++);           // Field delimiter
                if( errnum )
                {
                    return(errnum);
                }

                fprintf(c->f,"%-6s:%-31s:%-7s:%2s:%2s",
                        SYM_TAB_PROP[dim_props->sym_idx].key.c,
                        DimDbDigitalLabel(dim_type_idx, bank_idx+4, chan_idx),
                        DimDbDigitalLabelOne(dim_type_idx, bank_idx+4, chan_idx),
                        (unlatched_trig ? "UT" : ""),
                        (latched_trig   ? "LT" : ""));
            }
        }

        dim_props++;
    }
    return(0);
}
#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t DimAnaLabel(char *lbl_buf, uint16_t buf_len, const char * FAR dimdb_ana_lbl, uint16_t sym_idx)
/*---------------------------------------------------------------------------------------------------------*\
  Extracts the analogue signal label from the DimDB code and copies to lbl_buf (max len defined in buf_len).
  If the property sym_idx is for SUB1-SUB8 or SUB1B-SUB8B then the sub converter index will be derived and
  substituted for any # characters found in the name.  The function returns the length of the label not
  including the null termination.
\*---------------------------------------------------------------------------------------------------------*/
{
    char        ch;
    int8_t       sub_convt_idx;
    uint16_t      n_ch;
 
    if(!dimdb_ana_lbl)                           // If no label present
    {
        *lbl_buf = '\0';                                // Return empty string
        return(0);
    }

    n_ch  = 0;
    sub_convt_idx = '-';

#if (FGC_CLASS_ID == 51)
    if(sym_idx >= STP_SUB1 && sym_idx <= STP_SUB8B)     // If DIM is in a sub converter
    {
        sub_convt_idx = '1' + (int8_t)((sym_idx - STP_SUB1)/2);  // Calc sub converter index
    }
#endif

    do                                          // Copy string until buffer full
    { 
        ch = *dimdb_ana_lbl++;

        if(ch == '#')                                   // If name includes '#'
        {
            ch = sub_convt_idx;                                 // Substitute sub converter index
        }

        *(lbl_buf++) = ch;

        if(ch)
        {
            n_ch++;
        }
    }
    while(ch && n_ch < buf_len);

    return(n_ch);
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*\
  End of file: DIMsDataProcess.c
\*---------------------------------------------------------------------------------------------------------*/
