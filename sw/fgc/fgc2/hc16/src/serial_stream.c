/*---------------------------------------------------------------------------------------------------------*\
  File:         serial_stream.c

  Purpose:      FGC Software - Serial Communication Callback Functions.

\*---------------------------------------------------------------------------------------------------------*/

#include <serial_stream.h>
#include <stdio.h>              // for FILE
#include <os.h>                 // for OS_MSG, OS_MEM types
#include <trm.h>                // for terminal_state global variable
#include <macros.h>             // for Test(), Set(), Clr()
#include <stdio.h>              // this is only in FGC2, for NL contant

/*---------------------------------------------------------------------------------------------------------*/
void TermPutcChar(char ch, FILE *f)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SerialStreamInternal_WriteChar() to put the character ch into the buffer for the specified
  stream f.  If the buffer is full it will be flushed automatically.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!f->_p)                          // If no buffer is in use with this stream
    {
        f->_p = OSMemPend(terminal.mbuf);            // Pend of a free buffer from partition
        f->_offset = 0;                         // Reset buffer index
    }

    f->_p[f->_offset++] = ch;           // Save character in current buffer

    if(f->_offset >= TRM_BUF_SIZE)              // If buffer is now full...
    {
         SerialStreamInternal_FlushBuffer(f);                           // then flush buffer to terminal
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void SerialStreamInternal_WriteChar(char ch, FILE *f)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the callback for the terminal output streams (TrmTsk() commands or RtdTsk()).  It supports
  the following special codes:

        \v              Flush buffer immediately
        \n              Insert one newline even if multiple \n are seen and if editor is enabled
                        then follow by \r to produce a carriage return on the ANSI/VT100 screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(ch == '\n')                      // If character is newline (linefeed)
    {
        Set(f->_flags,NL);                      // Set newline flag
        return;
    }

    if(Test(f->_flags,NL))                      // If newline flag is set
    {
        TermPutcChar('\n',f);                    // Write a single newline to buffer
        Clr(f->_flags,NL);                      // Clear newline flag

        if(terminal.mode == TRM_EDITOR_MODE)         // If the line editor is enabled
        {
            TermPutcChar('\r',f);                        // Write a single CR to buffer
        }
    }

    if(ch == '\v')                      // If character is vertical tab
    {
        SerialStreamInternal_FlushBuffer(f);                            // then flush buffer to terminal immediately
        return;                                 // and return
    }

    TermPutcChar(ch,f);                  // Store character in buffer
}
/*---------------------------------------------------------------------------------------------------------*/
void SerialStreamInternal_FlushBuffer(FILE *f)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to flush the stream buffer so that it is queued for transmission to the SCI by
  the TrmMsOut() function.  This function can be called with a NULL FILE pointer.
   Note that terminal_state.bufq can
  never fill up because it is one element longer than the number of buffers it needs to address.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(f->_p)                                   // If buffer is defined
    {
        OS_ENTER_CRITICAL();

        terminal_state.bufq_adr[terminal_state.bufq.in_idx] = (char *) f->_p;   // Queue point to terminal buffer
        terminal_state.bufq_len[terminal_state.bufq.in_idx] = f->_offset;       // and length of contents

        if(++terminal_state.bufq.in_idx > TRM_N_BUFS)           // If input index has advanced beyond end of queue
        {
            terminal_state.bufq.in_idx = 0;                             // reset queue input index
        }

        OS_EXIT_CRITICAL();

        f->_p = 0;                                      // Clear local copy of buffer pointer
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: serial_stream.c
\*---------------------------------------------------------------------------------------------------------*/
