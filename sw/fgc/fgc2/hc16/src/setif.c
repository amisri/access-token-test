/*---------------------------------------------------------------------------------------------------------*\
 File:          setif.c

 Purpose:       FGC MCU Software - Set Command Condition Functions
\*---------------------------------------------------------------------------------------------------------*/

#define SETIF_GLOBALS
#define DEFPROPS_INC_ALL    // defprops.h

#include <setif.h>

#include <definfo.h>        // for FGC_CLASS_ID
#if (FGC_CLASS_ID != 59)
#include <cal_class.h>      // for REF_ARMED_FUNC_TYPE
#endif
#include <cmd.h>            // for tcm global variable and for struct cmd
#include <crate.h>          // for crate global variable
#include <defprops.h>
#include <pars.h>
#include <dev.h>            // for dev global variable
#include <dpcls.h>          // for dpcls global variable
#include <fbs.h>            // for fbs global variable
#include <fbs_class.h>      // for STATE_OP
#include <fgc_errs.h>       // for FGC_BAD_STATE
#include <log_class.h>      // for FGC_LAST_LOG
#include <macros.h>         // for Test()
#include <mst.h>            // for mst global variable
#include <sta.h>            // for sta global variable
#include <sta_class.h>      // for vs global variable
#include <defconst.h>       // for FGC_INTERFACE_*
#include <set.h>            // for SetRefLockInternal()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifFbsCmd(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is command from the FIELDBUS (FIP or ETH)?
                        This is mainly to ensure that the command is passed via a tool with secured access,
                        such as FGCRun+.
                        Note: Commands typed on the remote console are considered non-Fieldbus since in
                        that case the Fieldbus is only a transport pipe for the SCM task.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (c != &tcm)
    {
        return (0);
    }

    return (FGC_USE_FGCRUN_PLUS);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifConfigModeOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Can the user set CONFIG.MODE?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (   STATE_OP != FGC_OP_CALIBRATING
        && sta.config_state != FGC_CFG_STATE_STANDALONE
        && sta.config_mode == FGC_CFG_MODE_SYNC_NONE
       )
    {
      return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifOpNormalSim(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is operational state NORMAL or SIMULATION?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_OP == FGC_OP_NORMAL ||
        STATE_OP == FGC_OP_SIMULATION)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifStandalone(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is FGC running standalone (no FIP connector plugged in)?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (sta.config_state == FGC_CFG_STATE_STANDALONE)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifOpTestingSim(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is operational state TEST or SIMULATION?
\*---------------------------------------------------------------------------------------------------------*/
{
    if ((STATE_OP == FGC_OP_TEST) || (STATE_OP == FGC_OP_SIMULATION))
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
#if (FGC_CLASS_ID == 51) || (FGC_CLASS_ID == 53)
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifPcArmed(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is PC State ARMED?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_PC == FGC_PC_ARMED)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifPcNotCycling(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is STATE.PC not CYCLING?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_PC != FGC_PC_CYCLING)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifRefDac(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is operational state TEST
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_OP == FGC_OP_TEST)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifCalActiveOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Can CAL.ACTIVE be set
\*---------------------------------------------------------------------------------------------------------*/
{
    if (mst.dsp_rt_is_alive &&
        STATE_OP == FGC_OP_NORMAL &&
        sta.config_mode == FGC_CFG_MODE_SYNC_NONE &&
        !SetifPcOff(c) &&
        (uint16_t)dpcls.dsp.meas.i_zero_f)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifCalOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is Auto-Calibration of ADCs allowed?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (mst.dsp_rt_is_alive
        && (sta.config_mode == FGC_CFG_MODE_SYNC_NONE)
        && (STATE_OP != FGC_OP_CALIBRATING)
        && ((STATE_OP == FGC_OP_UNCONFIGURED)
            || ((STATE_OP == FGC_OP_NORMAL)
                && ((STATE_PC == FGC_PC_IDLE)
                    || (!SetifPcOff(c)
                        && !(!FGC_LAST_LOG.run_f && FGC_LAST_LOG.samples_to_acq)
                       )
                   )
               )
           )
       )
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
#endif
#if (FGC_CLASS_ID != 59)
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifRefOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: PPM reference is always accepted, whilst a non-PPM reference (0) is only
                    accepted when the state is FGC_PC_IDLE.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t errnum = FGC_OK_NO_RSP;

    if (c->mux_idx == 0 && STATE_PC != FGC_PC_IDLE)
    {
        errnum = FGC_BAD_STATE;
    }

    return (errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifRefUnlock(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is REF function lock DISABLED?

  NB: Keep the Bad State error for non-cycling or non-PPM operations, in the case the reference is
      already ARMED. In this case, the procedure to edit the reference is first to disarm it (with a
      S REF.FUNC.TYPE NONE) then to edit it and arm it again.
\*---------------------------------------------------------------------------------------------------------*/
{
    // TODO Rename this function, the functionality in FGC2 doesn't correspond to the name anymore

    uint16_t errnum = FGC_OK_NO_RSP;

    // If user 0 and ARMED or RUNNING state, return "bad state" error

    if (c->mux_idx == 0 && (STATE_PC == FGC_PC_ARMED || STATE_PC == FGC_PC_RUNNING))
    {
        errnum = FGC_BAD_STATE;
    }

    return (errnum);
}
#endif  // #if (FGC_CLASS_ID != 59)
#if 0
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifRefNone(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is REF function type for selected user NONE?

  NB: Keep the Bad State error for non-cycling or non-PPM operations
\*---------------------------------------------------------------------------------------------------------*/
{
    if (REF_ARMED_FUNC_TYPE[c->mux_idx] != FGC_REF_NONE)
    {
        if (c->mux_idx == 0)            // If user 0
        {
            return (FGC_BAD_STATE);
        }
        else
        {
            return (FGC_USER_LOCKED);
        }
    }

    return (0);
}
#endif
#if (FGC_CLASS_ID == 51) || (FGC_CLASS_ID == 53)
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifMpxOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is operational state TEST and adc interface is present?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (SM_INTERFACETYPE_P &&    // for FGC3 shared_mem.analog_card_model
        STATE_OP == FGC_OP_TEST)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifOpNotCal(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is operational state not CALIBRATING
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_OP != FGC_OP_CALIBRATING)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifResetOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is Voltage Source OFF and not calibrating?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_OP != FGC_OP_CALIBRATING   &&
        (STATE_PC == FGC_PC_FLT_OFF      ||
         STATE_PC == FGC_PC_OFF          ||
         STATE_PC == FGC_PC_FLT_STOPPING ||
         STATE_PC == FGC_PC_STOPPING     ||
         STATE_PC == FGC_PC_SLOW_ABORT   ||
         STATE_PC == FGC_PC_ON_STANDBY   ||
         STATE_VS == FGC_VS_INVALID))
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifRegStateNone(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is Power Converter OFF?
  This is a wrapper for SetifPcOff(). It's needed for compatibility with libref v2 classes.
\*---------------------------------------------------------------------------------------------------------*/
{
    return SetifPcOff(c);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifPcOff(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is Power Converter OFF?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (STATE_PC == FGC_PC_FLT_OFF ||
        STATE_PC == FGC_PC_OFF)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
#endif  // #if (FGC_CLASS_ID == 51) || (FGC_CLASS_ID == 53)
/*---------------------------------------------------------------------------------------------------------*\
  End of file: setif.c
\*---------------------------------------------------------------------------------------------------------*/

