/*---------------------------------------------------------------------------------------------------------*\
  File:     mst.c

  Purpose:  FGC MCU Software - millisecond task function

  Author:   Quentin.King@cern.ch

  Notes:    Activity alignment:

        Class 51:

        0   FipRcvTimeVar   DiagTermNext   DiagSciStart/200ms
        1   Get DSP data    PrepareStatVar    WriteStatVar
        2   StaTsk
        3   FbsProcessTimeVar     RtdTsk/100ms
        4   PLL
        5   LogIearth
        6   LogIreg
        7   StaTsk
        8   LogIleads/1000ms
        9   LogThour/10s
        10  TskTiming/100ms DlsTsk/100ms
        11
        12  RefEvent   StaTsk
        13  PubTsk
        14
        15
        16  LogIreg
        17  StaTsk
        18
        19  PrepareFipMsg

        Class 53:

                NOTE: PrepareStatVar is run on ms 2 for class 53 because Class 53 FGCs should
                never have a FIP ID of less than 10.

        0   FipRcvTimeVar
        1   StaTsk          DiagTermNext DiagSciStart/200ms
        2   Get DSP data    PrepareStatVar *************************** IMPORTANT NOTE ABOVE
        3   FbsProcessTimeVar RtdTsk/100ms
        4   PLL
        5
        6   StaTsk
        7
        8   LogIleads/1000ms
        9   LogThour/10s
        10  TskTiming/100ms DlsTsk/100ms
        11  StaTsk
        12  RefEvent
        13  PubTsk
        14
        15
        16  StaTsk
        17
        18
        19  PrepareFipMsg

        Class 59:

        0   FipRcvTimeVar   DiagTermNext DiagSciStart/200ms
        1   Get DSP data    PrepareStatVar  WriteStatVar
        2   StaTsk
        3   FbsProcessTimeVar RtdTsk/100ms
        4   PLL
        5
        6
        7   StaTsk
        8
        9   LogThour/10s
        10  TskTiming/100ms DlsTsk/100ms
        11
        12  StaTsk
        13  PubTsk
        14
        15
        16
        17  StaTsk
        18
        19  PrepareFipMsg
\*---------------------------------------------------------------------------------------------------------*/


#define MST_GLOBALS             // define mst global variable
#define DEFPROPS_INC_ALL        // defprops.h

#include <stdbool.h>
#include <mst.h>
#include <cmd.h>
#include <trm.h>
#include <nvs.h>
#include <tsk.h>
#include <class.h>              // for DEV_STA_TSK_PHASE, FGC_FAULTS, FGC_WARNINGS
#include <fbs_class.h>          // for FAULTS, WARNINGS, ST_LATCHED
#include <defprops.h>           // Include property related definitions
#include <definfo.h>            // for FGC_CLASS_ID, FGC_PLATFORM_ID
#include <dev.h>                // for dev global variable
#include <fbs.h>                // for fbs global variable
#include <dpcom.h>              // for dpcom global variable
#include <macros.h>             // for Test(), Set(), Clr()
#include <sta.h>                // for sta global variable
#include <diag.h>               // for diag global variable
#include <os.h>                 // for OSTskSuspend()
#include <dls.h>                // for temp  global variable
#include <runlog_lib.h>         // for RunlogWrite()
#include <mcu_dependent.h>      // for DEV_PWR_CYC_REQ, LED_SET()
#include <fgc_runlog_entries.h> // for FGC_RL_*
#include <mst_class.h>          // for MstClass(), MST_FIP_STAT_VAR_TIME
#include <pll_main.h>           // for PllSync()

#include <log.h>                // for LogThour(), LogTday()
#include <rtd.h>                // for rtd global variable
#include <dpcom.h>              // for dpcom variable
#include <main.h>
#include <shared_memory.h>      // for shared_mem global variable
#include <mcu_dsp_common.h>

#include <edac.h>

#include <memmap_mcu.h>         // for SM_UXTIME_P, RTC_FIP_P

#include <DIMsDataProcess.h>    // for QSPIbusStateMachine()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void MstTsk(void * unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the Millisecond Task.  During initialisation, it auto-suspends until resumed by the
  Idle Task.  Only then does it enable the GPT to start normal synchronous operation.  The main loop is
  controlled by IsrMst via an OSTskResume() call.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              tic0;
    uint16_t              next_ms;
    union TUnion32Bits  dsp_irq_alive_counter;
    uint16_t              dsp_alive_counter;
    uint16_t              cpu32;

    dev.leds   = 0;                 // Start with all LEDs off
    mst.wd_isr = 0x0005;            // Prepare watchdog value (non-zero)
    mst.dsp_bg_is_alive = true;     // Set the DSP background watchdog
    mst.dsp_rt_is_alive = false;     // Set DSP RT ISR watchdog

    // MST expects 1 DSP IRQ per millisecond. On FGC3, this variable is set in main.c
    mst.expected_dsp_irqs = 1;

    // Main Loop - synchronised to ms interrupts

    for (;;)
    {
        OSTskSuspend();             // Wait for TskResume by IsrMst()

        // Read the DSP counters, to check their status later on in this function.

        dsp_irq_alive_counter = dpcom.dsp.ms_irq_alive_counter;
        dsp_alive_counter     = dpcom.dsp.mcu_counter_dsp_alive.part.lo;        // Take low word only
        dpcom.dsp.mcu_counter_dsp_alive.part.lo++;  // increment to see if it is zeroed later by DSP background task

        // Remember timestamp of previous millisecond

        mst.prev_time = mst.time;

        SM_UXTIME_P = dpcom.mcu.time.now_ms.unix_time;
        SM_MSTIME_P = dpcom.mcu.time.now_ms.ms_time;
        mst.time.unix_time = dpcom.mcu.time.now_ms.unix_time;
        mst.time.us_time   = dpcom.mcu.time.now_ms.ms_time * 1000;

        tic0           = mst.tic0;                      // Take local copy of tic0 (set by IsrMst)
        mst.ms_mod_5   = SM_MSTIME_P % 5;               // Modulo   5 counter for   5 ms period activity
        mst.ms_mod_10  = dpcom.mcu.time.ms_mod_10;      // Modulo  10 counter for  10 ms period activity
        mst.ms_mod_20  = dpcom.mcu.time.ms_mod_20;      // Modulo  20 counter for  20 ms period activity
        mst.ms_mod_100 = SM_MSTIME_P % 100;             // Modulo 100 counter for 100 ms period activity
        mst.ms_mod_200 = dpcom.mcu.time.ms_mod_200;     // Modulo 200 counter for 200 ms period activity

        if (mst.ms_mod_10 == 0)
        {
            mst.time10ms = mst.time;                    // Save time of start of 20ms for logging
        }

        mst.watchdog = mst.wd_isr;  // Prepare watchdog response

        // Every Millisecond: Diagnostic acquisition and trigger and run class related actions
        QSPIbusStateMachine();
        MstClass();

        // Millisecond 0 of 20: terminal diag and PrepareStatVar with diag data

        if (mst.ms_mod_20 == 0)
        {

            if (terminal.mode == TRM_DIAG_MODE)
            {
                DiagTermNext();                          // SCI diag data
            }

            FbsPrepareStatVar();                        // Prepare diag part of status var
        }

        // Millisecond 1 or 2 of 20: Trigger FIP task to write status variable into uFIP

        else if ((mst.ms_mod_20 == MST_FIP_STAT_VAR_TIME) && (fbs.id != 0))
        {
            if (fbs.pll_locked_f                // PLL is locked
                && (fip.start_tx               // Messaging has just been enabled
                    || ((FipMsgsa() & 0x02) != 0)    // uFIP transmit buffer is empty (EMP_MST=1)
                   )
               )
            {
                FbsWriteMsg();                  // Write Message to uFIP (if pending)
            }

            FbsWriteStatVar();                  // Write Status variable
        }

        // Millisecond 3 of 20: if new time var received then process time var

        else if (mst.ms_mod_20 == 3)
        {
            if (fbs.time_rcvd_f)
            {
                FbsProcessTimeVar();
            }
        }

        // Millisecond 4 of 20: Run RT Clock PLL function to sync with WFIP

        else if ((mst.ms_mod_20 == 4) && (fbs.id != 0))
        {
            PllSync();
        }

        // Millisecond 7 of 20: Check fieldbus

        else if (mst.ms_mod_20 == 7)
        {
            if ((fbs.id != 0))
            {
                FbsCheck();
            }
        }

        // Millisecond 19 of 20: Trigger FIP task to prepare response message

        else if (mst.ms_mod_20 == 19)
        {
            if (fbs.id != 0)
            {
                FbsPrepareMsg();

                if (!fbs.time_rcvd_f)       // If time var was NOT received...
                {
                    fip.stats.v7_missed++;          // Increment missed packet count
                    fbs.last_runlog_idx = 0x0000;   // and prepare repeat Var 7 detection

                }
                else                        // else Var 7 WAS received...
                {
                    fip.stats.v7_rcvd++;            // Increment Var 7 received counter
                    fbs.time_rcvd_f = false;        // Prepare missed time var detection
                }
            }

            FipWatch();
        }

        // Milliseconds 2 of 5: Trigger state task (STA task) and check for device control actions

        if (mst.ms_mod_5 == DEV_STA_TSK_PHASE)
        {
            // If DEVICE.RESET/BOOT/CRASH/PWRCYC command pending

            if (dev.ctrl.counter != 0)
            {
                dev.ctrl.counter--;

                if (dev.ctrl.counter == 0)
                {
                    stop();
                }
            }

            sta.watchdog++;

            // TODO Review
            // If StaTsk watchdog failed to complete within 500ms
            // The value was increased from 25ms to 500ms. A failed table recovery from the nvs
            // caused the state task to timeout. After increasing the value it works fine.
            // However, we do not know the full impact of this change.

            if (sta.watchdog > 100)
            {
                mst.watchdog = 0;               // Clear Mst watchdog
                RunlogWrite(FGC_RL_UXTIME, (void *)SM_UXTIME_16); // Unix time
                RunlogWrite(FGC_RL_MSTIME, (void *)SM_MSTIME_16); // Millisecond time
                RunlogWrite(FGC_RL_STATSK, &sta.watchdog);  // StaTsk failed
            }

            // Set PM trig property from FIP ack byte

            dev.log_pm_trig = fbs.u.fieldbus_stat.ack & (FGC_EXT_PM_REQ | FGC_SELF_PM_REQ);

            // Wake up STA task

            OSTskResume(TSK_STA);
        }

        // Millisecond 3 of 100: Real-time Display and manual post mortem flags

        if (mst.ms_mod_100 == 3)
        {
            if ((terminal.mode == TRM_EDITOR_MODE) && rtd.enabled)
            {
                OSTskResume(TSK_RTD);
            }
        }

        //===============================================================================================
        // Every millisecond: Drive terminal interface

        TrmMsOut();

        // Every millisecond: Update time for DSP (ready for next millisecond interrupt)

        if (SM_MSTIME_P >= 999)                 // If second rollover is due
        {
            next_ms = 0;            // Reset millisecond time
            dpcom.mcu.time.now_ms.unix_time++;  // Increment unix time
            (SM_PWRTIME_P)++;           // Increment seconds-since-power-up time
            (SM_RUNTIME_P)++;           // Increment seconds-since-reset time
        }
        else                // else not second rollover
        {
            next_ms = SM_MSTIME_P + 1;          // Increment millisecond time
        }

        dpcom.mcu.time.now_ms.ms_time = (uint32_t)next_ms;    // Send millisecond time to DSP for next interation
        dpcom.mcu.time.ms_mod_10   = (uint32_t)(next_ms % 10);
        dpcom.mcu.time.ms_mod_20   = (uint32_t)(next_ms % 20);
        dpcom.mcu.time.ms_mod_200  = (uint32_t)(next_ms % 200);

        // Write to the LED register only when there is no power cycle request pending
        // Power cycle is requested by writing a magic number into the LED register. After that the register cannot
        // be changed until the request is complete

        if (!dev.ctrl.power_cycle_pending)
        {
            RGLEDS_P = dev.leds;
        }

        //===============================================================================================
        // Every second - check for DIAG sync resets and run set command downcounter - check for memory flt

        if (!SM_MSTIME_P)                                       // Once per second
        {
            if (diag.fault_too_much_resets_counter > DIAG_SYNC_RESET_PERIOD)
            {
                Set(ST_LATCHED, FGC_LAT_DIM_SYNC_FLT);
                diag.fault_too_much_resets_counter = 0;
            }

            if (diag.fault_too_much_resets_counter)     // Downcount the diag sync resets detector
            {
                diag.fault_too_much_resets_counter--;
            }

            if (mst.set_cmd_counter)                    // Decrement autocalibration inhibit counter
            {
                mst.set_cmd_counter--;
            }

            if ((edac.num_errors - edac.last_num_errors) > 2)   // If more than 2 SB error in past second
            {
                Set(ST_LATCHED, FGC_LAT_MEM_FLT);           // Set MEM_FLT in latched status
            }

            edac.last_num_errors = edac.num_errors;
        }

        // Every second - toggle FGC LED to indicate faults and warnings

        if (SM_MSTIME_P == 11)
        {
            if (Test(FAULTS, FGC_FAULTS)            // If any FGC related FGCs or warnings
                || Test(WARNINGS, FGC_WARNINGS)
               )
            {
                LED_SET(FGC_RED);
            }

            if (!Test(FAULTS, FGC_FAULTS))      // If no FGC related faults
            {
                LED_SET(FGC_GREEN);
            }
        }
        else
        {
            if (SM_MSTIME_P == 811)
            {
                LED_RST(FGC_RED);
                LED_RST(FGC_GREEN);
            }
            else // Every second - Log temperatures when required
            {
                if (SM_MSTIME_P == 9)
                {
                    mst.s_mod_600 = (uint16_t)(SM_UXTIME_P % 600L);
                    mst.s_mod_10  = mst.s_mod_600 % 10;

                    if (!mst.s_mod_10 && temp.thour_f)
                    {
                        LogThour();

                        if (temp.tday_f)
                        {
                            LogTday();
                        }

                        temp.thour_f = false;
                    }
                }
            }
        }

        // Every 100ms - Task timings, wake up Dallas task and check config state/mode

        if (mst.ms_mod_100 == 10)
        {
            MstTskTiming();
            OSTskResume(TSK_DLS);

            if (sta.config_mode == FGC_CFG_MODE_SYNC_FAILED)
            {
                sta.config_state = FGC_CFG_STATE_UNSYNCED;
            }

            if (sta.config_state == FGC_CFG_STATE_UNSYNCED || nvs.n_corrupted_indexes)
            {
                Set(WARNINGS, FGC_WRN_CONFIG);
            }
            else
            {
                Clr(WARNINGS, FGC_WRN_CONFIG);
            }
        }

        // Calculate CPU usage every second

        cpu32 = (uint16_t) dpcom.dsp.cpu_usage;       // Get DSP CPU usage (%) for previous ms

        if (cpu32 > mst.cpu32_usage_ms)             // If usage exceeds max so FAR this second
        {
            mst.cpu32_usage_ms = cpu32;             // Keep maximum usage
        }

        if (!SM_MSTIME_P)                           // Once per second
        {
            mst.cpu32_usage    = mst.cpu32_usage_ms;        // Save max DSP CPU usage for previous sec
            mst.cpu32_usage_ms = 0;

            mst.cpu_usage      = (50000 - diag.data[DIAG_TASK_TIMES + TSK_BGP]) / 500;    // MCU CPU usage
        }

        mst.diff_irq_alive_counter = dsp_irq_alive_counter.part.lo - mst.prev_dsp_ms_irq_alive_counter.part.lo;

        // Watchdog trigger

        if (!mst.dsp_is_standalone                                      // If DSP is not standalone and
            &&  mst.diff_irq_alive_counter != mst.expected_dsp_irqs    // DSP RT isr is not responding
           )
        {
            if (mst.dsp_rt_is_alive)
            {
                mst.dsp_rt_is_alive = false;
                Set(ST_LATCHED, FGC_LAT_DSP_FLT);
                Set(FAULTS, FGC_FLT_FGC_HW);
                RunlogWrite(FGC_RL_UXTIME, (void *) SM_UXTIME_16);      // Unix time
                RunlogWrite(FGC_RL_MSTIME, (void *) SM_MSTIME_16);      // Millisecond time
                RunlogWrite(FGC_RL_MODE_REG, &mst.mode);                // CPU_MODE after IRQ request
                RunlogWrite(FGC_RL_MODE_REG, (void *) CPU_MODE_16);     // CPU_MODE now
                // DSP Runcode (upper 16 bits: ms_time, lower 16 bits: a code)
                RunlogWrite(FGC_RL_DSP_RT, (void *) &dpcom.dsp.run_code);
                // DSP IRQ alive counter (current)
                RunlogWrite(FGC_RL_DSP_RT, (void *) &dsp_irq_alive_counter.int32u);
                // DSP IRQ alive counter on the previous ms
                RunlogWrite(FGC_RL_DSP_RT, (void *) &mst.prev_dsp_ms_irq_alive_counter.int32u);

                if ((dpcom.dsp.run_code & 0x0000FFFF) == RUNCODE_DSP_PANIC)
                {
                    RunlogWrite(FGC_RL_DSP_PANIC, (void *)&dpcls.dsp.panic.panic_code);
                }
            }
        }
        else
        {
            mst.dsp_rt_is_alive = true;
        }

        // Remember DSP wd value for next iteration
        mst.prev_dsp_ms_irq_alive_counter = dsp_irq_alive_counter;

        // if the MCU incremented the counter without being reseted by the DSP background task
        if (dsp_alive_counter > 1000)
        {
            if (mst.dsp_bg_is_alive)
            {
                mst.dsp_bg_is_alive = false;
                Set(ST_LATCHED, FGC_LAT_DSP_FLT);
                Set(FAULTS, FGC_FLT_FGC_HW);

                RunlogWrite(FGC_RL_UXTIME, (void *) SM_UXTIME_16);  // Unix time
                RunlogWrite(FGC_RL_MSTIME, (void *) SM_MSTIME_16);  // Millisecond time
                RunlogWrite(FGC_RL_DSP_BG, NULL);           // DSP BG task failed
            }
        }
        else
        {
            mst.dsp_bg_is_alive = true;
        }

        if (!mst.dsp_is_standalone                  // If DSP is under MCU control and
            && (!mst.dsp_bg_is_alive || !mst.dsp_rt_is_alive)  // DSP ISR or BG has failed
           )
        {
            mst.watchdog = 0;                   // Clear Mst watchdog
        }

        if (mst.dicoalive)                      // If DICO alive provided by MstClass()
        {
            mst.dicoalive = mst.watchdog;           // Set to mst.watchdog for IsrMst()
        }

#if (FGC_CLASS_ID == 53)
        // POPS : Reset PBL card links, if not in ADC filter reset

        if (dpcls.mcu.adc.filter_state == FGC_FILTER_STATE_READY)
        {
            LINK_LK1_RESET_P = 0;
            LINK_LK2_RESET_P = 0;
        }

#endif

        // Every millisecond: Resume FcmTsk, TcmTsk and TrmTsk if suspended

        OS_ENTER_CRITICAL();
        OSTskResumeLP(TSK_FCM);
        OSTskResumeLP(TSK_TCM);
        OSTskResumeLP(TSK_TRM);

        OS_EXIT_CRITICAL();

        // Time MstTsk() function
        mst.duration = GPT_TCNT_P - tic0;               // 2 MHz counter (i.e. units of 0.5 us)

        if (mst.duration > mst.max_duration[mst.ms_mod_20])
        {
            mst.max_duration[mst.ms_mod_20] = mst.duration;
        }

        dpcom.mcu.mst_time = mst.duration;          // Pass MstTsk time to DSP for SPY
    }
}
#pragma MESSAGE DEFAULT C5703

/*---------------------------------------------------------------------------------------------------------*/
void MstTskTiming(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called at 10Hz from MstTsk() to work out the task/ISR/MstTsk timing - the results are
  put in the diag data structure.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t   *  diagdata;

    // Transfer Task and ISR timing

    diagdata = diag.data + DIAG_TASK_TIMES;
    asm
    {
        LDAA    #(TSK_BGP+1)            // loop2 count: Number of tasks
        LDX     diagdata
        LDY     os.tsk

    loop2:

        LDE     OS_TSK_RUN_TIME, Y
        CLRW    OS_TSK_RUN_TIME, Y
        STE     0, X
        LDE     OS_ISR_RUN_TIME, Y
        CLRW    OS_ISR_RUN_TIME, Y
        STE     0x20, X                 // Offset to [0xD0 + i] in bytes (0x10 words)
        AIX     #2
        AIY     #OS_TCB_SIZE
        DECA
        BNE loop2
    }

    // Transfer MstTsk timing
    diagdata = diag.data + DIAG_MST_TIMES;
    asm
    {
        LDAA    #20                     // loop1 count: Number of milliseconds in FIP cycle
        LDX     diagdata
        LDY     @mst.max_duration

        loop1:

        LDE     0, Y                    // Get Max MstTsk time in 0.5us units for previous 100ms
        CLRW    0, Y                    // Clear max value for next period of 100ms
        ASRE                            // Divide by 2 to get the max duration in microseconds
        STE     0, X
        AIX     #2
        AIY     #2
        DECA
        BNE     loop1
    }
}
/*---------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------*\
  End of file: mst.c
\*---------------------------------------------------------------------------------------------------------*/
