/*!
 * @file      nvs.c
 * @defgroup  FGC:MCU:FGC2
 * @brief     Non-Volatile Storage Management.
 *
 * This file contains the functions which support the non-volatile storage and recovery
 * of Property data to/from the NVRAM memory.
 *
 * NVRAM is accessible by WORD only - BYTE access will result in a bus error!
 *
 * Some non-volatile properties are part of the configuration, meaning they are stored
 * in the central property database and will be set by the FGC configuration manager.
 */

#define NVS_GLOBALS         // define nvs global variable
#define DEFPROPS_INC_ALL    // defprops.h

#include <nvs.h>
#include <cmd.h>            // for tcm, pcm global variable, struct cmd
#include <property.h>       // defprops.h needs DEFPROPS_FUNC_SET(), typedef prop_size_t, and for struct prop
#include <defprops.h>       // for PROP_MODE_OP
#include <fbs_class.h>      // for FAULTS, STATE_OP
#include <macros.h>         // for Test(), Set(), Clr()
#include <sta.h>            // for sta global variable
#include <mem.h>            // for MemCpyWords(), MemSetBytes(), MemSetWords()
#include <core.h>           // for core global variable, CoreTableGet()
#include <start.h>          // for Crash()
#include <prop.h>           // for PropSet() and PropMap()
#include <core.h>           // for struct fgc_corectrl
#include <mcu_dependent.h>  // for MSLEEP(), NVRAM_UNLOCK(), NVRAM_LOCK()
#include <mcu_dsp_common.h>
#include <memmap_mcu.h>     // for NVSIDX_MAGIC_P, NVSIDX_SWVER_P, NVSIDX_REC_NAME_B
#include <fbs.h>            // for fbs global variable
#include <fgc_errs.h>
#include <fgc_runlog_entries.h>
#include <string.h>
#include <runlog_lib.h>

// Constants

#define NVS_DATA_BUF_SIZE(p) ((p->max_elements * prop_type_size[p->type] + sizeof(uint16_t) + 1) & ~1)

// Internal function declarations

static void NvsResetNvsIdx(uint16_t lvl, struct prop * p);
static void NvsCountUnsetConfig(uint16_t lvl, struct prop * p);
static void NvsInitCore(void);
static void NvsRecallValuesForAllProperties(bool new_sw_f);
static void NvsStoreValuesForAllProperties(uint16_t lvl, struct prop * p);
static void NvsFormat(void);
static void NvsFormatRecord(uint16_t lvl, struct prop * p);
static void NvsClearCfgChanged(uint16_t lvl, struct prop * p);
static void NvsPropDynflags(struct prop * p);
static void NvsSanityCheck(struct prop * p);

// Internal function definitions

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

static void NvsResetNvsIdx(uint16_t lvl, struct prop * p)
{
    /* This function is called by PropMap() via NvsInit() to set the NVS record
     * index to the default value INVALID_NVS_IDX.
     */
    p->nvs_idx = INVALID_NVS_IDX;
}



static void NvsCountUnsetConfig(uint16_t lvl, struct prop * p)
{
    if (Test(p->flags, PF_CONFIG) && !Test(p->dynflags, DF_NVS_SET))
    {
        nvs.n_unset_config_props++;
    }
}



static void NvsStoreValuesForAllProperties(uint16_t lvl, struct prop * p)
{
    /*
     * This function is called by PropMap() in NvsInit(). Remember that since
     * PropMap() is a recursive function, the deeper the property tree, the
     * more stack space is required.
     */
    if (Test(p->flags, PF_NON_VOLATILE))
    {
        NvsStoreValuesForOneProperty(&tcm, p, NVS_ALL_USERS);
    }
}



static void NvsClearCfgChanged(uint16_t lvl, struct prop * p)
{
    /* Function of type prop_map_func used in NvsResetChanged() */

    Clr(p->dynflags, DF_CFG_CHANGED);
}



static void NvsFormatRecord(uint16_t lvl, struct prop * p)
{
    /*
     * This function is called from the recursive function NvsScanNvsProps()
     * when it finds a non-volatile property. It will create a new storage
     * space in NVRAM for that property and copy its contents into it.
     */

    uint16_t      idx;
    uint16_t      name_length;
    uint16_t      n_bufs;
    uint16_t      bufsize;
    char        namebuf[NVSIDX_REC_NAME_B + 1];
    char    *   namebuf_p = namebuf;

    struct nvs_record * FAR NVS_RECORDS = (struct nvs_record * FAR) NVSIDX_REC_32;

    // Stack the symbol index of each part of the property name

    nvs.sym_idxs[lvl] = p->sym_idx;

    // If it is non volatile return immediately

    if (p->type != PT_PARENT && Test(p->flags, PF_NON_VOLATILE))
    {
        // If no space left in NVS index

        if (nvs.idx >= (NVSIDX_W / NVSIDX_REC_W))
        {
            Crash(0xBAD1);
        }

        // Link property with its new NVS record

        p->nvs_idx = nvs.idx;

        // Format NVS index record

        bufsize = NVS_DATA_BUF_SIZE(p);
        NVS_RECORDS[nvs.idx].prop    = (uint16_t) p;
        NVS_RECORDS[nvs.idx].bufsize = bufsize;
        NVS_RECORDS[nvs.idx].data    = nvs.data_addr;

        // Assemble and copy property name into NVS index record

        // Clear the name buffer

        memset(namebuf, 0, NVSIDX_REC_NAME_B + 1);

        // Generate name string from symbols

        for (idx = 0, name_length = 0; idx <= lvl; idx++)
        {
            namebuf_p += sprintf(namebuf_p, "%s.", SYM_TAB_PROP[nvs.sym_idxs[idx]].key.c);

            name_length = namebuf_p - namebuf;

            if ((name_length) > (NVSIDX_REC_NAME_B + 1))
            {
                Crash(0xBADE);
            }
        }

        // Drop trailing '.'

        *(--namebuf_p) = '\0';

        MemCpyWords((uint32_t) & (NVS_RECORDS[nvs.idx].name),
                    (uint32_t) namebuf,
                    NVSIDX_REC_NAME_W);

        // Set every NVS data buffer to have the number of elements equal to NVS_UNSET

        n_bufs = NVS_GET_NUM_PROP_USERS(p);

        for (idx = 0; idx < n_bufs ; idx++, nvs.data_addr += bufsize)
        {
            *((uint16_t * FAR)nvs.data_addr) = (uint16_t)NVS_UNSET;
        }

        // If NVS data area has been overrun or overlap with the NVS index (next in memory map) then crash

        if (nvs.data_addr > (NVSDATA_32 + NVSDATA_B))
        {
            Crash(0xBAD8);
        }

        nvs.idx++;
    }
}

#pragma MESSAGE DEFAULT C5703



static void NvsInitCore(void)
{
    /* This function is called by NvsInit() to initialise the core control
     * table properties in RAM from the table in the NVRAM.  If the table has
     * never been initialised, then it will be set to a default table.
     */

    uint16_t  i;
    struct fgc_corectrl corectrl;

    CoreTableGet(&corectrl);

    // Transfer to CORE properties

    for (i = 0; i < FGC_N_CORE_SEGS; i++)
    {
        core.addr[i]    = corectrl.seg[i].addr;
        core.n_words[i] = corectrl.seg[i].n_words;
    }
}



static void NvsRecallValuesForAllProperties(bool new_sw_f)
{
    /*
     * It recover the data for all properties in the NVS. If new_sw_f is false
     * then it can use the stored pointer to find the property, otherwise it
     * must look up the property by name and check to see if it is still
     * non-volatile.
     */

    struct nvs_record * FAR NVS_RECORDS = (struct nvs_record * FAR) NVSIDX_REC_32;

    uint16_t nvs_idx;
    uint16_t prop_addr;
    uint32_t nv_data_addr;

    // Recover number of NVS properties as recorded in the NVS index header
    // and check it is valid

    nvs.n_props = NVSIDX_NUMPROPS_P;

    if (nvs.n_props >= (NVSIDX_W / NVSIDX_REC_W))
    {
        nvs.n_corrupted_indexes++;

        // Return immediately to reformat NVS

        return;
    }

    // Loop for all NVS index records

    for (nvs_idx = 0 ; nvs_idx < nvs.n_props ; nvs_idx++)
    {
        // Check if NVS record is inside valid memory range

        // Data address in NVSDATA zone (must be word aligned)

        nv_data_addr = NVS_RECORDS[nvs_idx].data;

        if ((nv_data_addr <  NVSDATA_32)               ||
            (nv_data_addr >= (NVSDATA_32 + NVSDATA_B)) ||
            (nv_data_addr & 1))
        {
            nvs.n_corrupted_indexes++;
            continue;
        }

        // Find property

        if (new_sw_f)
        {
            // PropFind uses struct cmd pcm, but since this is the init,
            // one does not need to take pcm's semaphore.
            // Do not use standard PropFind here, it will break

            if ( (PropFindFar(NVS_RECORDS[nvs_idx].name) != 0)                 ||
                 !Test(pcm.prop->flags, PF_NON_VOLATILE)                       ||
                 (NVS_RECORDS[nvs_idx].bufsize != NVS_DATA_BUF_SIZE(pcm.prop)) )
            {
                continue;
            }

            tcm.cached_prop = pcm.prop;
        }
        else
        {
            prop_addr       = NVS_RECORDS[nvs_idx].prop;
            tcm.cached_prop = (struct prop *) prop_addr;

            // If property pointer is corrupted.
            // Warning, this depends on how the variables are located in the
            // RAM memory the properties are pre filled variables so they go
            // to the .data (not .bss) we test that the properties are in the
            // RAM area defined by the linker

            if (prop_addr < 0x1000 ||
                prop_addr >= DPRAM_16 ||
                !Test(tcm.cached_prop->flags, PF_NON_VOLATILE) ||
                NVS_RECORDS[nvs_idx].bufsize != NVS_DATA_BUF_SIZE(tcm.cached_prop))
            {
                nvs.n_corrupted_indexes++;
                continue;
            }
        }

        // Link property to NVS record (this will be updated if NVS is
        // reformatted due to a corrupted index). This is used in
        // NvsRecallValuesForOneProperty()

        tcm.cached_prop->nvs_idx = nvs_idx;

        // Extract property data from NVS

        NvsRecallValuesForOneProperty(&tcm, tcm.cached_prop, NVS_ALL_USERS);

        // If this is an old software version, there will be no call to
        // NvsStoreValuesForAllProperties() to copy back the property
        // values in NVS, and therefore this property is now correctly
        // set in both RAM and NVS.

        NvsPropDynflags(tcm.cached_prop);
    }
}



static void NvsFormat(void)
{
    /*
     * This function is called by NvsInit() if it finds that the software
     * version has changed or the NVS area needs to be reformatted. It scans
     * the property tree and for NVS properties it creates a record and saves
     * the property data in the record.
     */

    nvs.data_addr = NVSDATA_32;

    NVRAM_UNLOCK();

    MemSetWords(NVSIDX_32,  0, NVSIDX_W);
    MemSetWords(NVSDATA_32, 0, NVSDATA_W);

    // Format and fill NVS records for all NVS properties

    PropMap(NvsFormatRecord, PROP_MAP_ALL);

    // Update software version and magic number

    NVSIDX_NUMPROPS_P = nvs.idx;
    NVSIDX_SWVER_P    = version.unixtime;
    NVSIDX_MAGIC_P    = NVS_MAGIC;

    NVRAM_NVS_COMPATIBILITY_FP = NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION;
    NVRAM_LOCK();
}



static void NvsPropDynflags(struct prop * p)
{
    /*
     * This function is used to update the property dynflags after editing the
     * data stored in NVRAM. It will check the data stored in NVS and set the
     * DF_NVS_SET flag only if the number of elements is different from NVS_UNSET
     * for one of the mux_idx.
     */

    struct nvs_record * FAR NVS_RECORDS = (struct nvs_record * FAR) NVSIDX_REC_32;

    uint16_t  mux_idx;
    uint16_t  n_users;
    uint16_t  n_els;
    uint32_t  bufsize;
    uint32_t  nv_data_addr;
    bool      just_set_f = false;
    uint16_t  set_users_counter;

    // If property is still unset

    if (!Test(p->dynflags, DF_NVS_SET))
    {
        // Number of PPM buffers
        n_users           = NVS_GET_NUM_PROP_USERS(p);
        nv_data_addr      = NVS_RECORDS[p->nvs_idx].data;
        bufsize           = NVS_RECORDS[p->nvs_idx].bufsize;
        set_users_counter = 0;

        for (mux_idx  = 0;
             mux_idx < n_users;
             mux_idx++, nv_data_addr += bufsize)
        {
            // If number of elements for this user (stored in first word of data buffer) is not UNSET
            // and if it's not 0 (so the property was set at least once) and the propery is either
            // not CONFIG, or not DONT_SHRINK or has max_elements set, then increment set_users_counter

            n_els = *(uint16_t * FAR)nv_data_addr;

            if (   n_els != NVS_UNSET
                && n_els != 0
                && (   !Test(p->flags, PF_CONFIG)
                    || (   !Test(p->flags, PF_DONT_SHRINK)
                        || n_els == p->max_elements )))
            {
                set_users_counter++;
            }
        }

        // Consider the property set only if it's set for all users

        if( set_users_counter == n_users )
        {
            Set(p->dynflags, DF_NVS_SET);
            just_set_f = true;
        }
    }

    // For config properites, keep track of the number unset

    if (Test(p->flags, PF_CONFIG))
    {
        // If property has been set for the first time

        if (just_set_f)
        {
            if (--nvs.n_unset_config_props == 0)
            {
                STATE_OP = sta.mode_op;

                Clr(sta.faults, FGC_FLT_FGC_STATE);
                Clr(FAULTS, FGC_FLT_FGC_STATE);
            }
        }

        if (sta.config_mode == FGC_CFG_MODE_SYNC_FGC)
        {
            Clr(p->dynflags, DF_CFG_CHANGED);
        }
        else
        {
            Set(p->dynflags, DF_CFG_CHANGED);
        }

        // Change config state to UNSYNCED, but only when not in stand-alone mode
        if (sta.config_state != FGC_CFG_STATE_STANDALONE)
        {
            sta.config_state = FGC_CFG_STATE_UNSYNCED;
        }
    }
}



static void NvsSanityCheck(struct prop * p)
{
    /*
     * Check that the property is non-volatile and bound to a NVS record.
     */

    if (!Test(p->flags, PF_NON_VOLATILE))
    {
        Crash(0xBADA);
    }

    if (p->nvs_idx == INVALID_NVS_IDX)
    {
        Crash(0xBADB);
    }
}



// External function definitions



void NvsInit(struct init_prop * property_init_info)
{
    /*
     * This function is called during startup, just after interrupts are
     * enabled so that the DSP will run and allow DSP properties to be
     * configured. It is responsible for the recovery/formatting of
     * non-volatile properties using data from the NVS area of the NVRAM
     * memory. This can take three forms:
     *
     *  1. NVS never used - Erase & format
     *  2. SW Version changed - Recover data where possible, then erase
     *     and re-format
     *  3. SW Version not changed - Recover data.
     *
     *  This function sets the operational state (STATE_OP) to UNCONFIGURED or
     *  NORMAL according to whether there are unset configuration properties
     *  after the NVS property initialisation.
     */

    bool new_sw_f;

    NvsInitCore();

    sta.mode_op = FGC_OP_NORMAL;

    // Set the nvs_idx for all properties to INVALID_NVS_IDX

    PropMap(NvsResetNvsIdx, PROP_MAP_ALL);

    // If NVS never formatted (or is to be reformatted) or config
    // is too out of date

    if ((NVSIDX_MAGIC_P != NVS_MAGIC) ||
       (NVRAM_NVS_COMPATIBILITY_FP != NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION))
    {
        NvsFormat();                    // Erase and store all NVS properties in NVRAM
    }
    else
    {
        new_sw_f = (NVSIDX_SWVER_P != version.unixtime);

        NvsRecallValuesForAllProperties(new_sw_f);

        if (new_sw_f || nvs.n_corrupted_indexes)
        {
            NvsFormat();
            PropMap(NvsStoreValuesForAllProperties, PROP_MAP_ONLY_CHILDREN);
        }
    }

    nvs.n_unset_config_props = 0;
    PropMap(NvsCountUnsetConfig, PROP_MAP_ONLY_CHILDREN);

    if (nvs.n_unset_config_props)
    {
        Set(sta.faults, FGC_FLT_FGC_STATE);
    }
    else
    {
        STATE_OP = sta.mode_op;
    }

    // If MODE.OP is TEST, then reset it to value in property_init_info (NORMAL)

    if (sta.mode_op == FGC_OP_TEST)
    {
        Clr(PROP_MODE_OP.dynflags, DF_NVS_SET);
    }

    // Initialise properties with default values

    while (property_init_info->prop)
    {
        // If the property has never been set

        if (!Test(property_init_info->prop->dynflags, DF_NVS_SET))
        {
            if (Test(property_init_info->prop->flags, PF_PPM))
            {
                uint16_t prev_mux_idx = tcm.mux_idx;
                uint16_t mux_idx;

                // PropSet can only set a property for one user at a time
                // We need to iterate through all users using the command structure to properly initialise a PPM property

                for (mux_idx = 0; mux_idx < FGC_MAX_USER_PLUS_1; mux_idx++)
                {
                    tcm.mux_idx = mux_idx;

                    PropSet(&tcm, property_init_info->prop, property_init_info->data, property_init_info->n_elements);
                }

                // Restore the multiplexer index

                tcm.mux_idx = prev_mux_idx;
            }
            else
            {
                PropSet(&tcm, property_init_info->prop, property_init_info->data, property_init_info->n_elements);
            }
        }

        property_init_info++;
    }

    // Reset the DF_CFG_CHANGED flag set during the NVS initialiation

    PropMap(NvsClearCfgChanged, PROP_MAP_ONLY_CHILDREN);
}



void NvsRecallValuesForOneProperty(struct cmd * c, struct prop * p, uint16_t user)
{
    uint16_t      nvs_idx;
    uint16_t      user_from;
    uint16_t      user_to;
    prop_size_t   n_els;
    uint32_t      nv_data_addr;
    uint32_t      nv_block_addr;
    uint32_t      bufsize;

    struct nvs_record * FAR NVS_RECORDS = (struct nvs_record * FAR) NVSIDX_REC_32;

    // Get NVS record

    NvsSanityCheck(p);

    nvs_idx = p->nvs_idx;

    if (user == NVS_ALL_USERS)
    {
        user_from = 0;
        user_to   = NVS_GET_NUM_PROP_USERS(p) - 1;
    }
    else
    {
        user_from = user_to = user;
    }

    // Prepare data address for the first user

    bufsize = NVS_RECORDS[nvs_idx].bufsize;
    nv_data_addr = NVS_RECORDS[nvs_idx].data + user_from * bufsize;

    // Extract property data from NVS

    // This is required to call the PropBlkSet() function

    c->cached_prop = p;

    // This is the only function where we set a property without writing it
    // in the NVRAM

    c->write_nvs_f = false;

    for (c->mux_idx  = user_from;
         c->mux_idx <= user_to ;
         c->mux_idx++, nv_data_addr += bufsize)
    {
        n_els = *((uint16_t * FAR)nv_data_addr);

        if (n_els == NVS_UNSET)
        {
            n_els = 0;
        }
        else if (n_els > p->max_elements)
        {
            nvs.n_corrupted_indexes++;
        }
        else
        {
            // If property has not been set for that particular mux_idx

            // Skip the n_els word at the start of the buffer and set
            // the property

            nv_block_addr = nv_data_addr + sizeof(uint16_t);

            PropSetFar(c, p, (void * FAR) nv_block_addr, n_els);
        }
    }

    // Restore c->mux_idx to a value inside the input range

    c->mux_idx = user_from;

    // Restore the default behaviour of the PropBlkSet() function

    c->write_nvs_f = true;
}



void NvsStoreBlockForOneProperty(struct      cmd * c,
                                 struct      prop * p,
                                 uint16_t    mux_idx,
                                 prop_size_t n_els,
                                 uint16_t    last_blk_f,
                                 uint8_t *   data)
{
    /*
     * On FGC2 and FGC3 the maximum n_els for a property saved in NVS
     * is 65534 = 0xFFFE, despite the fact that on FGC3 the number of elements
     * of a property is a uint32_t. Moreover, n_els = 0xFFFF is a reserved
     * keyword (= NVS_UNSET).
     */

    prop_size_t   n_els_old;          // Number of elements of property data
    uint16_t      n_bytes;            // Number of bytes to copy
    uint16_t      block_idx;          // Property transfer block index
    uint16_t      el_size;
    uint32_t      nv_data_addr;
    uint32_t      nv_block_addr;

    struct nvs_record * FAR NVS_RECORDS = (struct nvs_record * FAR) NVSIDX_REC_32;

    NvsSanityCheck(p);

    nv_data_addr = NVS_RECORDS[p->nvs_idx].data;

    if (mux_idx)
    {
        // Select data buffer for mux idx.

        nv_data_addr += (uint32_t)(mux_idx * NVS_RECORDS[p->nvs_idx].bufsize);
    }

    // Get local copy of el_size

    el_size = prop_type_size[p->type];

    // Use prop block index if supplied

    if (c != NULL)
    {
        block_idx = c->prop_blk_idx;
    }
    else
    {
        block_idx = 0;
    }

    n_els_old = *((uint16_t * FAR)nv_data_addr);

    nv_block_addr = nv_data_addr + sizeof(uint16_t) + (uint32_t) block_idx * PROP_BLK_SIZE;

    n_bytes = el_size * n_els - block_idx * PROP_BLK_SIZE;

    if (n_bytes > PROP_BLK_SIZE)
    {
        n_bytes = PROP_BLK_SIZE;
    }
    else
    {
        // If data has an odd number of bytes pad with zero to word boundary.

        if (n_bytes & 1)
        {
            data[n_bytes++] = 0;
        }
    }

    // Early RETURN if data has not changed

    if (n_els == n_els_old && !MemCmpWords(nv_block_addr, (uint32_t) data, n_bytes / 2))
    {
        return;
    }

    // If property was never sets et old number of elements to zero

    if (n_els_old == NVS_UNSET)
    {
        n_els_old = 0;
    }

    NVRAM_UNLOCK();

    MemCpyWords(nv_block_addr, (uint32_t)data, n_bytes / 2);

    if (last_blk_f || n_els > n_els_old)
    {
        *((uint16_t * FAR)nv_data_addr) = (uint16_t)n_els;
    }

    // If last block and data has shrunk

    if (last_blk_f && n_els < n_els_old)
    {
        n_bytes = (n_els_old - n_els) * el_size;
        MemSetWords((nv_data_addr + sizeof(uint16_t) + (uint32_t)n_els * (uint32_t)el_size), 0, (uint32_t)(n_bytes / 2));
    }

    NVRAM_LOCK();

    NvsPropDynflags(p);
}



uint16_t NvsStoreValuesForOneProperty(struct cmd *  c,
                                      struct prop * p,
                                      uint16_t      user)
{
    uint16_t    user_from;
    uint16_t    user_to;
    uint16_t    el_size;
    uint32_t    bufsize;
    uint32_t    runlog_error;
    uint16_t    n_bytes;
    prop_size_t n_els;         // Number of elements in case the property has not been set
    prop_size_t n_els_old;     // Previous number of elements
    uint32_t    nv_data_addr;
    uint32_t    nv_block_addr;
    uint16_t    errnum;
    uint16_t    n_blocks;      // Number of property transfer blocks
    uint16_t    remaining_w;   // Number of words remaining to copy

    struct nvs_record * FAR NVS_RECORDS = (struct nvs_record * FAR) NVSIDX_REC_32;

    // Get NVS record info

    NvsSanityCheck(p);

    if (user == NVS_ALL_USERS)
    {
        user_from = 0;
        user_to = NVS_GET_NUM_PROP_USERS(p) - 1;
    }
    else
    {
        user_from = user_to = user;
    }

    // Data address in NVSDATA zone

    el_size = prop_type_size[p->type];
    bufsize = NVS_RECORDS[p->nvs_idx].bufsize;
    nv_data_addr = NVS_RECORDS[p->nvs_idx].data + user_from * bufsize;

    // This is required to call the PropBlkGet() function

    c->cached_prop = p;

    NVRAM_UNLOCK();

    for (c->mux_idx  = user_from;
         c->mux_idx <= user_to;
         c->mux_idx++, nv_data_addr += bufsize)
    {
        c->prop_blk_idx = 0;
        errnum          = PropBlkGet(c, &n_els);

        if (errnum != FGC_OK_NO_RSP)
        {
            runlog_error = 0x01000000L + ((uint32_t)p->nvs_idx << 16) + ((uint32_t)c->mux_idx << 8) + (uint32_t)errnum;
            RunlogWrite(FGC_RL_NVS_ERR, &runlog_error);
        }
        else
        {
            if (n_els > 0)
            {
                // IMPORTANT: It is essential to divide by unsigned 2, otherwise the result is completely wrong
                // once (n_els * el_size + 1) is greater than 32767, due to a compiler error

                remaining_w = (n_els * el_size + 1) / 2U;
                n_blocks    = (remaining_w + PROP_BLK_SIZE_W - 1) / PROP_BLK_SIZE_W;

                // Skip the word containing number of elements

                nv_block_addr = nv_data_addr + sizeof(uint16_t);

                while (n_blocks--)
                {
                    // If not first block

                    if (c->prop_blk_idx)
                    {
                        errnum = PropBlkGet(c, NULL);

                        if (errnum != FGC_OK_NO_RSP)
                        {
                            runlog_error = 0x02000000L + ((uint32_t)p->nvs_idx << 16) + ((uint32_t)c->mux_idx << 8) + (uint32_t)errnum;
                            RunlogWrite(FGC_RL_NVS_ERR, &runlog_error);
                        }
                    }

                    MemCpyWords(nv_block_addr,
                                (uint32_t)c->prop_buf->blk.intu,
                                (n_blocks ? PROP_BLK_SIZE_W : remaining_w));

                    nv_block_addr += PROP_BLK_SIZE;
                    remaining_w -= PROP_BLK_SIZE_W;
                    c->prop_blk_idx++;
                }
            }

            if (errnum == FGC_OK_NO_RSP)
            {
                // Cleaning up the buffer if the length has shrunk

                n_els_old = *((uint16_t * FAR)nv_data_addr);

                if (n_els_old != NVS_UNSET && n_els < n_els_old)
                {
                    n_bytes = (n_els_old - n_els) * el_size;
                    MemSetWords((nv_data_addr + sizeof(uint16_t) + (uint32_t)n_els * (uint32_t)el_size), 0, (uint32_t)(n_bytes / 2));
                }

                // Copy number of elements in first word of NVRAM data
                // (= NVS_UNSET if the property has not been set)

                *((uint16_t * FAR)nv_data_addr) = (uint16_t)n_els;
            }
        }
    }

    // Restore c->mux_idx to a value inside the input range

    c->mux_idx  = user_from;

    NVRAM_LOCK();

    // Mark property as set in NVS (if relevant) and additional process
    // for config properties

    NvsPropDynflags(p);

    return errnum;
}



void NvsResetChanged(void)
{
    PropMap(NvsClearCfgChanged, PROP_MAP_ONLY_CHILDREN);
}

// EOF
