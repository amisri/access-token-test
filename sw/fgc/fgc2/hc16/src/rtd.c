/*---------------------------------------------------------------------------------------------------------*\
 File:      rtd.c

 Purpose:   FGC MCU Software - Real-time display Functions.

 Notes:     Global option line display Layout (DIAG, PSU):

                  1         2         3         4         5         6         7         8
        +---------+---------+---------+---------+---------+---------+---------+---------++
     DIAG   20  |aa:nnnn aa:xnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn aa:nnnn |
     PSU    20  |+5: n.nn    +15: nn.nn  -15:-nn.nn                      |
        +---------+---------+---------+---------+---------+---------+---------+---------++

\*---------------------------------------------------------------------------------------------------------*/

#define RTD_GLOBALS             // define rtd global variable

#include <rtd.h>
#include <stdio.h>              // for fprintf(), fputs()
#include <cmd.h>
#include <class.h>              // Include class dependent definitions
#include <definfo.h>            // for FGC_CLASS_ID
#include <mst.h>                // for mst global variable
#include <dpcom.h>              // for dpcom global variable
#include <diag.h>               // for diag global variable
#include <os.h>                 // for OSTskSuspend()
#include <dls.h>                // for temp  global variable
#include <math.h>               // for fabs()
#include <rtd_class.h>          // for RTD_TOUT_POS, rtd_cls, rtd_flags[], rtd_func[] global variables
#include <mem.h>                // for MemSetBytes()
#include <term.h>               // for TERM_CLR_LINE, TERM_REVERSE
#include <string.h>             // for strcmp(), strcpy()
#include <serial_stream.h>      // for SerialStreamInternal_WriteChar()
#include <macros.h>

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void RtdTsk(void *unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the Real-time display task.  It makes use of the following vt100 control codes
  (ESC = '\33'):

      ESC 7     Save cursor position and attributes
      ESC [y;xH     Move cursor to (x,y)
      ESC 8     Restore saved cursor position and attributes

  The real-time display is active only if the line editor is enabled.  The Rtd Task is
  triggered every 100 ms by the millisecond task.  It works through the different fields in turn, looking
  for a field with changed data.  If one is found, it refreshes the display for this value.  Only one field
  is updated per cycle, so a maximum of 10 fields may be updated per second.

  This function never exits so "local" variables are static to save stack space.
\*---------------------------------------------------------------------------------------------------------*/
{
    FILE                cmd_file;
    static uint16_t   idx;
    static bool     done_f;

    rtd.f       = &cmd_file;    // Prepare RTD output stream pointer
    rtd.f->cb = SerialStreamInternal_WriteChar; // Use terminal char output callback function

    rtd.enabled = true;             // RTD Display is enabled by default

    for(;;)                         // Main loop @ RTD_PERIOD_MS
    {
        OSTskSuspend();             // Wait for OSTskResume() from MstTsk();

        idx = rtd.idx;              // Take local copy of rtd index

        do
        {
            done_f = rtd_func[rtd.idx++]();     // Call RTD function for the current index value

            if(!rtd_func[rtd.idx])              // If index reaches the end
            {
                rtd.idx = 0;                    // Wrap back to start
            }
        }
        while(!done_f && rtd.idx != idx);   // while nothing displayed and all elements not checked
    }
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*/
bool RtdInt16s(int16_t *source, int16_t *last, char * FAR fmt, char * FAR pos)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display a 16 bit signed value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t  value;

    value = *source;            // Take local copy of value

    if(value != *last)          // If value has changed since last display
    {
        *last = value;                  // Take "last" copy

        fprintf(rtd.f,RTD_START_POS,pos);   // Move cursor
        fprintf(rtd.f,fmt,value);           // Display float value
        fputs(RTD_END,rtd.f);               // Restore cursor position

        return(true);
    }
    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdInt32u(uint32_t *source, uint32_t *last, char * FAR fmt, char * FAR pos)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display a 32 unsigned integer value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  value;

    value = *source;            // Take local copy of value

    if(value != *last)          // If value has changed since last display
    {
        *last = value;                  // Take "last" copy

        fprintf(rtd.f,RTD_START_POS,pos);       // Move cursor
        fprintf(rtd.f,fmt,value);           // Display float value
        fputs(RTD_END,rtd.f);               // Restore cursor position

        return(true);
    }
    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdTemp( uint16_t temperature, uint16_t * previous_temperature_ptr, char * FAR pos )
/*---------------------------------------------------------------------------------------------------------*\
  This function will display a measured temperature value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( temperature != *previous_temperature_ptr )
    {
        *previous_temperature_ptr = temperature;

        fprintf( rtd.f, RTD_START_POS, pos);
        fprintf( rtd.f,"%2u.%02u", (temperature >> 4), ID_TEMP_FRAC_C(temperature) );
        fputs( RTD_END, rtd.f);

        return(true);
    }
    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdFloat(FP32 * source, FP32 *last, char *last_s, char * FAR fmt, char * FAR pos)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display a float value on the screen.  The function checks if the string value of
  the float has changed before displaying it.
\*---------------------------------------------------------------------------------------------------------*/
{
    bool            no_change_f;
    FP32            value;
    static char     str[20];        // String buffer. This buffer is sufficient to print floating-point values
                                    // in range [-1.0E10; +1.0E10] with 6 decimal places.


    OS_ENTER_CRITICAL();
    value = *source;            // Take local copy of value
    OS_EXIT_CRITICAL();


    if(fabs(value) > 1.0E+10)       // Clip max value to avoid crashing due to overruns of the string buffer
    {
        value = (value > 0.0 ? 1.0E+10 : -1.0E-10);
    }

    if(value != *last)          // If value has changed since last display
    {
        sprintf(str,fmt,value);         // Write float value to temporary buffer
        str[10] = '\0';             // Clip to 10 characters

        asm                 // Fast string compare
        {
            LDX   last_s;               // X = address of last string value

            LDD   8,X                   // Copy new string with last string
            CPD   str:8
            BNE   Changed               // Branch if change detected
            LDD   6,X
            CPD   str:6
            BNE   Changed
            LDD   4,X
            CPD   str:4
            BNE   Changed
            LDD   2,X
            CPD   str:2
            BNE   Changed
            LDD   0,X
            CPD   str:0
            BNE   Changed

            INCW  no_change_f               // No change detected so set no change flag
            BRA   End

        Changed:                    // Change detected
            CLRW  no_change_f               // Clear no change flag
            LDD   str:0                 // Fast copy of string to last string
            STD   0,X
            LDD   str:2
            STD   2,X
            LDD   str:4
            STD   4,X
            LDD   str:6
            STD   6,X
            LDD   str:8
            STD   8,X
        End:
        }

        if ( no_change_f )                              // If no change detected in string
        {
            return(false);                              // Return false
        }

        *last = value;                  // Take "last" copy of numeric value

        fprintf(rtd.f,RTD_START_POS,pos);       // Move cursor
        fputs((char * FAR)str,rtd.f);           // Write string
        fputs(RTD_END,rtd.f);               // Restore cursor position

        return(true);
    }
    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdFlags(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the bit status flags on the screen, either as a reverse video character, or
  as the choice between two different characters.  If the previous call detected a flag that had toggled
  then that flag will not be check on this call.  If no flags changed on the previous call then all flags
  will be check on this call.  This behaviour prevents the function from blocking the RTD in the event that
  a flag is toggling constantly since the function will make sure that it is recalled immediately if a
  flag is found to have toggled.  This is used to ensure that all flags are written quickly when the RTD
  is enabled.
\*---------------------------------------------------------------------------------------------------------*/
{
    bool        flag_off;
    char *      flags;
    uint16_t      var;
    uint16_t      flag_end_idx;

    do                      // Loop for all flags
    {
        flag_end_idx = rtd.flag_idx;

        if(!(rtd_flags[++rtd.flag_idx].var))        // Adjust flag index to check next flag
        {
            rtd.flag_idx = 0;
        }

        var = *rtd_flags[rtd.flag_idx].var;     // Get flag variable

        flag_off = !(var & rtd_flags[rtd.flag_idx].mask); // Extract bit(s) identified by mask

        if(flag_off != rtd.last.flag_off[rtd.flag_idx]) // If flag has changed
        {
            rtd.last.flag_off[rtd.flag_idx] = flag_off;             // Save new flag
            fprintf(rtd.f,RTD_START_POS,rtd_flags[rtd.flag_idx].pos);       // Move cursor
            flags = (char *) &rtd_flags[rtd.flag_idx].flag;

            switch(flags[0])                // Act on flag control character
            {
                case '#':                       // Reverse video if flag is 1

                    if(!flag_off)                       // If flag is active
                    {
                        fputs(TERM_REVERSE,rtd.f);                  // Enable reverse video
                    }
                    SerialStreamInternal_WriteChar(flags[1],rtd.f);             // Write flag character
                    break;

                case '~':                       // Reverse video if flag is 0

                    if(flag_off)                        // If flag is inactive
                    {
                        fputs(TERM_REVERSE,rtd.f);                  // Enable reverse video
                    }
                    SerialStreamInternal_WriteChar(flags[1],rtd.f);             // Write flag character
                    break;

                default:                        // Normal video with two flag characters

                    SerialStreamInternal_WriteChar(flags[flag_off],rtd.f);              // Write flag character
                    break;
            }

            fputs(RTD_END,rtd.f);                   // Restore cursor position
            rtd.flag_end_idx = flag_end_idx;                // Set end index for next iteration
            rtd.idx--;                          // Return to flags next time
            return(true);                       // Report that field has been written
        }

    } while(rtd.flag_idx != rtd.flag_end_idx);          // until all flags checked

    return(false);                  // Report that field has not been written
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdTout(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the measured FGC outlet temperature value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    return( RtdTemp( temp.fgc.out, &rtd.last.tout, RTD_TOUT_POS ) );
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdTin(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the measured FGC inlet temperature value on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    return( RtdTemp( temp.fgc.in, &rtd.last.tin, RTD_TIN_POS) );
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdCpuUsage(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the CPU usage on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t usage = mst.cpu_usage;

    if ( usage > 99 )       // Clip at 99%!
    {
        usage = 99;
    }

    return( RtdInt16s( &usage, &rtd.last.cpu_usage, "%02d", RTD_CPUUSE_POS) );
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdCpu32Usage(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the CPU32 usage on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t usage = mst.cpu32_usage;

    if ( usage > 99 )       // Clip at 99%!
    {
        usage = 99;
    }

    return( RtdInt16s( &usage, &rtd.last.cpu32_usage, "%02d", RTD_CPU32USE_POS) );
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdOptionLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will control the display on line 20 of:         -----    Class     ------
                                                                51      53      59     61
    0. OFF    = Nothing                                         Y       Y       Y      Y
    1. DIAG   = Diagnostic data                                 Y       Y       Y      Y
    2. PROT   = Measured current lead voltages and Iearth       Y       N       N      N
    3. LOAD   = Measured load                                           (deleted)
    4. ADCS   = ADC1 & ADC2 data                                Y       Y       N      Y
    5. PSU    = Measured FGC PSU voltages                       Y       Y       Y      Y
    6. CYC    = Cycling & PPM data                              N       Y       N      Y
    7. ADC14  = ADC1 & ADC4 data                                N       N       N      Y
    8. ADC24  = ADC2 & ADC4 data                                N       N       N      Y
    9. ADC34  = ADC3 & ADC4 data                                N       N       N      Y

  The choice is controlled by rtd.ctrl
\*---------------------------------------------------------------------------------------------------------*/
{
    if(rtd.ctrl != rtd.last_ctrl)       // If option line control has changed
    {
        rtd.last_ctrl = rtd.ctrl;           // Save option line control
        RtdResetOptionLine();               // Reset option line
        return(true);                   // Report that a field has been written
    }

    switch(rtd.ctrl)
    {
        case FGC_RTD_DIAG:  return(RtdDiagList());
        case FGC_RTD_PSU:   return(RtdPsu());

#if (FGC_CLASS_ID == 51) || (FGC_CLASS_ID == 53)
        case FGC_RTD_ADCS:  return(RtdAdcs());
#endif
#if(FGC_CLASS_ID == 51)
        case FGC_RTD_PROT:  return(RtdProt());
#endif
#if (FGC_CLASS_ID == 53)
        case FGC_RTD_CYC:   return(RtdCyc());
#endif
    }

    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
void RtdResetOptionLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will clear the option line and will reset the option line variables
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(rtd.f,RTD_START_POS,RTD_OPTLINE_POS);       // Move cursor to option line
    fputs(TERM_CLR_LINE,rtd.f);                 // Erase complete line
    fputs(RTD_END,rtd.f);                   // Restore cursor position

    MemSetBytes(&rtd.optline,    0xFF,sizeof(rtd.optline)); // Reset all option line variables
    MemSetBytes(&rtd_cls.optline,0xFF,sizeof(rtd_cls.optline)); // Reset all the class option line variables

    rtd.optline.diag_list_len = diag.list_rtd_len;      // Remember number of diag data fields
    rtd.optline.idx = 0;                    // Zero option line index
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdDiagList(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the diagnostic data on the option line.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  chan;
    uint16_t  data;
    char    pos[6];

    if(diag.list_rtd_len != rtd.optline.diag_list_len)      // If the diag chan list length has changed
    {
        RtdResetOptionLine();                       // Reset option line
        return(true);
    }

    if(!diag.list_rtd_len)                  // If diag chan list is empty
    {
        return(false);                          // Report no field written
    }

    rtd.optline.idx++;
    if (rtd.optline.idx >= diag.list_rtd_len)           // Pre increment index to next field
    {
        rtd.optline.idx = 0;
    }

    chan = diag.list_rtd[rtd.optline.idx];
    if ( chan != rtd.optline.diag_chan[rtd.optline.idx] )       // If channel address address has changed
    {
        rtd.optline.diag_chan[rtd.optline.idx] = chan;      // Remember new channel
    }
    else
    {           // else if data has not changed
        if ( diag.data[chan] == rtd.optline.diag_data[rtd.optline.idx] )
        {
            return(false);                          // Report that no field was written
        }
    }

    data = diag.data[chan];                                     // Get new data

    rtd.optline.diag_data[rtd.optline.idx] = data;      // Save new data

    sprintf(pos,"%u;%u", RTD_OPTLINE_VPOS, (RTD_DIAGDATA_HPOS+RTD_DIAGDATA_HWIDTH*rtd.optline.idx));

    fprintf(rtd.f,RTD_START_POS,(char * FAR)pos);       // Move cursor

    if(chan < DIAG_MAX_ANALOG)                  // If data is analogue
    {
        data &= 0xFFF;
        fprintf(rtd.f,"%02X:%-4.0f",chan,               // Display address in hex and
        ((FP32)data * DIAG_ANALOG_CAL));           // data in millivolts
    }
    else
    {
        if(chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL)) // else if data is digital
        {
            data &= 0xFFF;
            fprintf(rtd.f,"%02X:x%03X",chan,data);              // Display address and data in hex
        }
        else                            // else data is software value
        {
            fprintf(rtd.f,"%02X:%04X",chan,data);               // Display address and data in hex
        }
    }

    fputs(RTD_END,rtd.f);                   // Restore cursor position

    return(true);
}

// This warning looks like a compiler bug - it complains about first parameter in calls to RtdFloat

#pragma MESSAGE DISABLE C1825 // Disable 'indirection to different types' warning
/*---------------------------------------------------------------------------------------------------------*/
bool RtdPsu(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the extra information about the measured load on the option line
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              psu_idx   = rtd.optline.idx;
    char                pos[6];
    static char * FAR   psu_fmt[] = { "+5:%5.2f","+15:%6.2f","-15:%6.2f" };
    FP32                psu_value;

    do
    {
        rtd.optline.idx++;
        if ( rtd.optline.idx > 2 )
        {
            rtd.optline.idx = 0;
        }

        sprintf(pos,"%u;%u", RTD_OPTLINE_VPOS,(RTD_PSU_HPOS + RTD_PSU_HWIDTH * rtd.optline.idx));

        psu_value = dpcom.dsp.psu.fgc[rtd.optline.idx];

        if ( RtdFloat( &psu_value ,
                       &rtd.optline.psu[rtd.optline.idx] ,
                       &rtd.optline.float_s[rtd.optline.idx][0],
                       psu_fmt[rtd.optline.idx],
                       (char * FAR) pos )
           )
        {
            return(true);
        }
    }
    while(psu_idx != rtd.optline.idx);

    return(false);
}
#pragma MESSAGE DEFAULT C1825

/*---------------------------------------------------------------------------------------------------------*\
  End of file: rtd.c
\*---------------------------------------------------------------------------------------------------------*/
