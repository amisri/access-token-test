/*---------------------------------------------------------------------------------------------------------*\
  File:     fbs.c

  Purpose:  FGC MCU Software - Fieldbus communications Functions.

  Author:   Quentin.King@cern.ch

  Notes:    This file contains the functions to support communications via the fieldbus.

                For the FGC2, the only fieldbus supported is WorldFIP 2.5Mbps
\*---------------------------------------------------------------------------------------------------------*/

#define FBS_GLOBALS                 // define fbs global variable

#include <fbs.h>
#include <stdio.h>                  // for FILE
#include <trm.h>                    // for terminal global variable
#include <fbs_class.h>              // for ST_UNLATCHED
#include <dev.h>                    // for dev global variable
#include <cmd.h>                    // for tcm, fcm, pcm global variables, struct cmd
#include <mem.h>                    // for MemCpyStrFar(), MemCpyFip()
#include <fip.h>                    // for fip global variable, FIP_OUTQ_SIZE
#include <definfo.h>                // for FGC_PLATFORM_ID
#include <defconst.h>
// defconst.h is needed by fgc_fip.h because it includes fgc_code.h that needs FGC_MAX_DEVS_PER_GW
#include <fgc_code.h>               // for struct fgc_dev_name
#include <dpcom.h>                  // for dpcom global variable
#include <macros.h>                 // for Test(), Set(), Clr()
#include <diag.h>                   // for diag global variable
#include <os.h>                     // for OSTskSuspend()
#include <fgc_errs.h>               // for FGC_PKT_BUF_NOT_AVL, FGC_EXECUTING
#include <runlog_lib.h>             // for RunlogWrite()
#include <pll_main.h>               // for pll global variable
#include <iodefines.h>
#include <mcu_dependent.h>          // for ENTER_BB(), EXIT_BB(), LED_SET()
#include <fgc_runlog_entries.h>     // Include run log definition, for FGC_RL_
#include <sta.h>                    // for sta global variable
#include <codes_holder.h>           // for codes_holder_info global variable
#include <string.h>                 // for strcmp()
#include <queue_fgc.h>              // for struct queue
#include <definfo.h>                // for FGC_CLASS_ID
#include <shared_memory.h>          // for shared_mem global variable
#include <log_class.h>

// Macro arguments are expanded except if with # or ##, so we need 2 level of indirection
#define CLASS_FIELD_STRING(name)        c##name
#define CLASS_FIELD_STRING2(name)       CLASS_FIELD_STRING(name)
#define CLASS_FIELD                     CLASS_FIELD_STRING2(FGC_CLASS_ID)   // c##FGC_CLASS_ID

static void     FbsReconnect            (void);

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void FbsTsk(void *unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the fieldbus task.  It is responsible for reading and processing fieldbus messages.

  This function never exits, so the "local" variables are static which saves stack space.
\*---------------------------------------------------------------------------------------------------------*/
{
    FILE                cmd_file;

    fcm.f                = &cmd_file;               // Prepare cmd output stream pointer
    fcm.f->cb            = FbsPutcFcm;              // Use FIP character output callback function
    fcm.store_cb         = FbsPutcStoreFcm;         // Use FIP/ETH character output callback function

    fip.watchdog         = FBS_WATCHDOG_PERIOD-2;   // Init FIP watchdog so interface will be initialised
    fbs.pkt_buf_idx      = 1;                       // Fill pkt 1 buffer first
    fbs.cmd.q.buf        = cmd_q_buf;               // Link cmd response stream queue to buffer
    fbs.pub.q.buf        = pub_q_buf;               // Link publication stream queue to buffer
    fbs.termq.buf        = fbs.termq_buf;           // Link rterm queue to buffer
    fbs.cmd.q.dec_mask   = (FBS_OUTQ_SIZE-1);       // Set dec mask
    fbs.pub.q.dec_mask   = (FBS_OUTQ_SIZE-1);       // Set dec mask
    fbs.termq.dec_mask   = (FBS_TERMQ_SIZE-1);      // Set dec mask
    fbs.cmd.header_flags = FGC_FIELDBUS_FLAGS_CMD_PKT;  // Initialise fcm response header flags

    for (;;)                    // Main loop - process each message received
    {
        OSTskSuspend();         // wait to be resumed by IsrFbs

        FbsReadCmdMsg();        // read command message
    }
}
#pragma MESSAGE DEFAULT C5703

/*---------------------------------------------------------------------------------------------------------*/
void FbsReadCmdMsg(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will read and process a command packet message.  The IsrFbs() has already established
  that a valid message is waiting in the buffer.  IsrFbs() prepares fbs.pkt_len and fbs.rcvd_header with
  the length and header word.  Get commands are limited by this function to one packet.  Set commands can be
  any length and will be executed on the fly.  During the reception of packets the status will be
  FGC_RECEIVING until the last packet is received when the status will change to FGC_EXECUTING.  The gateway
  will not attempt to send a new command when the status is executing, but it may do so if the status is
  receiving.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              cmd_type;
    struct cmd_pkt *    pkt = &dpcom.fcm_pars.pkt[fbs.pkt_buf_idx];     // Get address of new packet buffer;

    fip.stats.msg_rcvd++;                               // Update msg received statistics

    if (pkt->header_flags)                              // If the new packet buffer is not yet available
    {
        fcm.stat = FGC_PKT_BUF_NOT_AVL;                 // report BUF NOT AVL error
        fbs.u.fieldbus_stat.ack ^= FGC_ACK_CMD_TOG;     // Acknowledge receipt of command packet
        return;
    }

    if (Test(fbs.rcvd_header.flags,FGC_FIELDBUS_FLAGS_FIRST_PKT))       // If this command pkt starts a new command...
    {
        if (fcm.stat == FGC_EXECUTING)                  // If command is still executing
        {
            return;                                     // Ignore new packet
        }

        fcm.errnum = FGC_OK_NO_RSP;

        cmd_type = fbs.rcvd_header.flags & FGC_FIELDBUS_FLAGS_CMD_TYPE_MASK;    // Extract cmd type from header

        if(cmd_type == FGC_FIELDBUS_FLAGS_GET_CMD   ||          // If new pkt contains a command that will return
           cmd_type == FGC_FIELDBUS_FLAGS_SUB_CMD   ||      // data through the get command stream
           cmd_type == FGC_FIELDBUS_FLAGS_UNSUB_CMD ||
           cmd_type == FGC_FIELDBUS_FLAGS_GET_SUB_CMD)
        {
            if (!Test(fbs.rcvd_header.flags,FGC_FIELDBUS_FLAGS_LAST_PKT))   // If the cmd is longer than one packet
            {
                fcm.stat = FGC_CMD_BUF_FULL;                    // report CMD BUF FULL error
                fbs.u.fieldbus_stat.ack ^= FGC_ACK_CMD_TOG;                 // Acknowledge receipt of command packet
                return;                             // Ignore packet
            }

            Set(fbs.cmd.header_flags,FGC_FIELDBUS_FLAGS_FIRST_PKT);  // Set first rsp packet flag
            Clr(fbs.cmd.header_flags,FGC_FIELDBUS_FLAGS_LAST_PKT);   // Clear last rsp packet flag

            fcm.f->_flags = 0;                      // Clear stream flags
            fcm.abort_f = false;                    // Clear abort flag for new command
        }

        fbs.cmd.rsp_len = 0;
        fcm.stat    = FGC_RECEIVING;                // report state change
    }
    else                                            // else packet isn't the first for command...
    {
        if (fcm.stat != FGC_RECEIVING)              // If a new packet is not expected
        {
            return;                                 // Ignore packet
        }
    }

    if (Test(fbs.rcvd_header.flags,FGC_FIELDBUS_FLAGS_LAST_PKT)) // If this packet is the last for the command
    {
        fcm.stat = FGC_EXECUTING;                        // Indicate that command now executing
    }

    fbs.pkt_buf_idx   ^= 1;                     // Flip to other buffer for next packet
    fbs.u.fieldbus_stat.ack ^= FGC_ACK_CMD_TOG; // Acknowledge receipt of command packet
    pkt->n_chars       = fbs.pkt_len;           // Pass packet length and
    pkt->header_flags  = fbs.rcvd_header.flags; // msg header flags and
    pkt->header_user   = fbs.rcvd_header.user;  // user to command task

    FipCpyCmd (pkt->buf, fbs.pkt_len);          // callback for FIP

    OSSemPost(fcm.sem);                         // Trigger command task to process new packet
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from MstTsk() on millisecond 7 of 20.
\*---------------------------------------------------------------------------------------------------------*/
{
    FipCheckMsgTx();                    // Check msg TX for timeout

    if(!fip.rd_rcvd_f)                  // If real-time data msg was NOT received...
    {
        fip.stats.rd_missed++;          // Increment missed ref data msg count
    }
    else                                // else Var 7 WAS received...
    {
        fip.stats.rd_rcvd++;            // Increment ref data msg received counter
        fip.rd_rcvd_f = false;          // Prepare missed ref data msg detection
    }

    if(fbs.pll_locked_f &&              // If was PLL locked and
       fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll != FGC_PLL_LOCKED)     // has now unlocked
    {
        fbs.pll_locked_f = false;       // Reset PLL locked flag
        FIP_CONFA_P    = 0x05;          // Message rx/tx disabled
        fbs.gw_online_f = false;
        pll.num_unlocks++;              // Increment unlocks counter
        FbsDisconnect(FGC_PLL_NOT_LOCKED);
    }

    // Check for post mortem request bit changes

    if ( (fbs.u.fieldbus_stat.ack ^ fbs.last_ack) & (FGC_SELF_PM_REQ | FGC_EXT_PM_REQ) )
    {
        fbs.last_ack = fbs.u.fieldbus_stat.ack;

        if ( Test(fbs.u.fieldbus_stat.ack, FGC_SELF_PM_REQ) )
        {
            RunlogWrite(FGC_RL_UXTIME,(void*)SM_UXTIME_16);
            RunlogWrite(FGC_RL_MSTIME,(void*)SM_MSTIME_16);
            RunlogWrite(FGC_RL_PM_REQ, &fbs.u.fieldbus_stat.ack);
        }
    }

    // Reset EXT_PM_REQ after 500ms

    if (Test(fbs.u.fieldbus_stat.ack, FGC_EXT_PM_REQ))
    {
        fbs.ext_pm_req_count++;
        if (fbs.ext_pm_req_count >= 25)         // If EXT_PM_REQ active for 25*20ms = 500ms
        {
            Clr( fbs.u.fieldbus_stat.ack, FGC_EXT_PM_REQ);  // Clear Ext PM request
        }
    }
    else
    {
        fbs.ext_pm_req_count = 0;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void FbsReconnect(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function performs some initialisation needed when the link comes back.
  Link up is equivalent to :
   * Fip case: pll has locked
\*---------------------------------------------------------------------------------------------------------*/
{
    {
        FipMsgsa();                         // Read status register from uFIP to clear it
        FIP_CONFA_P          = 0x07;        // Message reception enabled and transmission disabled
        fip.start_tx         = true;        // Flag that messaging has just been enabled
        fbs.gw_online_f      = true;
    }

    fbs.pub.header_flags = 1;           // (Re-)start pub message sequence number at 1
    fbs.pend_pkt         = 0;           // Clear pend flags
    fbs.rsp_msg_len      = 0;
    fbs.n_term_chs       = 0;
    LED_RST(FIP_RED);                   // Clear RED FIELDBUS LED (PLL not locked)
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsDisconnect(uint8_t sta)
/*---------------------------------------------------------------------------------------------------------*\
  This function performs initialisation needed when the link disappears (GW no longer online).
\*---------------------------------------------------------------------------------------------------------*/
{
    LED_SET(FIP_RED);                   // Set RED FIP LED (PLL not locked)

    if (fcm.stat == FGC_EXECUTING)      // If FIP command executing
    {
        FbsCancelCmd(sta);              // Cancel command response
    }

    fbs.send_pkt       = 0;             // Discard packet being sent
    fbs.termq.is_full  = false;         // Flush rterm queue
    fbs.termq.out_idx  = fbs.termq.in_idx;
}

/*---------------------------------------------------------------------------------------------------------*/
void FbsProcessTimeVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from MstTsk() on millisecond 3 of 20 if a valid time var was received on ms 0.
  The delay in processing avoids overloading the very busy ms 0 and ms 1 of 20.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (!fbs.log_sync_time)                             // If log sync not active
    {
        if (Test(fbs.time_v.flags,FGC_FLAGS_SYNC_LOG))  // if sync pulse arrives
        {
            fbs.log_sync_time = dpcom.mcu.time.fbs.unix_time;       // Set log sync to current time
        }
    }
    else                                                // else log sync is active
    {
        if (!Test(fbs.time_v.flags,FGC_FLAGS_SYNC_LOG)) // if sync pulse ends
        {
            fbs.log_sync_time = 0;                      // Clear log sync time
        }
    }

    if (fbs.sector_access_gw &&                         // If SECTOR ACCESS flag has just been disabled
        !Test(fbs.time_v.flags,FGC_FLAGS_SECTOR_ACCESS))
    {
        fbs.sector_access = FGC_CTRL_DISABLED;          // Reset sector access flag
    }

    fbs.sector_access_gw = Test(fbs.time_v.flags,FGC_FLAGS_SECTOR_ACCESS);
    dev.pm_enabled_f     = Test(fbs.time_v.flags,FGC_FLAGS_PM_ENABLED) != 0;

    logFgcLoggerSetEnable(Test(fbs.time_v.flags, FGC_FLAGS_LOGGER_ENABLED) != 0);

    if (fbs.time_v.fgc_id == fbs.id)                // If rterm char is for this FGC
    {
        if (fbs.time_v.fgc_char == 0x03)            // If char is CTRL-C
        {
            tcm.abort_f = true;                     // Abort terminal cmd
            terminal.xoff_timer = 0;                     // Reset XOFF timer
            OSMsgFlush(terminal.msgq);                   // Clear kbd buffer
        }
        else                                        // else normal character
        {
            // the compiler warns because we are storing in the pointer (uint32_t) a char (8 bits)
            OSMsgPost(terminal.msgq, (void *) (fbs.time_v.fgc_char) );      // send character to TrmTsk
                                                                       // willingly use a cast from char to void*
        }
    }

    fbs.last_runlog_idx = fbs.time_v.runlog_idx;    // Remember new runlog index
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsPrepareStatVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare the status variable.  It is called by MstTsk on ms 0 of every 20 ms, even if
  there is no fieldbus interface active, because the data can also be used by the terminal diag mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint16_t      data_chan;          // Channel within diag.data[] array
    uint16_t      reported_chan;      // Channel reported to user in diag_chan[]
    uint16_t      logical_dim;
    uint16_t      board_number;

    // Sync please bit in unlatched status

    if (fbs.pll_locked_f
            && Test(sta.config_mode,(FGC_CFG_MODE_SYNC_DB | FGC_CFG_MODE_SYNC_FGC | FGC_CFG_MODE_SYNC_DB_CAL)))
    {
        Set(ST_UNLATCHED, FGC_UNL_SYNC_PLEASE);
    }
    else
    {
        Clr(ST_UNLATCHED, FGC_UNL_SYNC_PLEASE);
    }

    // Diagnostic mpx & data

    for (i = 0; i < FGC_DIAG_N_LISTS; i++)              // For each diag list
    {
        fbs.u.fieldbus_stat.diag_chan[i] = FGC_DIAG_UNUSED_CHAN;    // Set reported channel to UNUSED by default

        if (diag.list_len[i])                           // If list is in use
        {
            if (fbs.diag_list_idx[i] >= diag.list_len[i]) // If beyond end of list
            {
                fbs.diag_list_idx[i] = 0;               // Reset index to start of list
            }

            reported_chan = diag.list[i][fbs.diag_list_idx[i]++];       // Set reported channel from diag list

            if (reported_chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL))  // If DIM channel
            {
                data_chan = reported_chan + diag.list_offset[i];        // Include list offset

                logical_dim = data_chan & (FGC_MAX_DIM_BUS_ADDR - 1);   // Get DIM index

                board_number = diag.logical_dim_info[logical_dim].flat_qspi_board_number;
                if (board_number < FGC_MAX_DIM_BUS_ADDR)
                {
                    fbs.u.fieldbus_stat.diag_chan[i] = reported_chan;
                    fbs.u.fieldbus_stat.diag_data[i] = diag.data[board_number + (data_chan & 0xE0)];
                }
            }
            else
                if (reported_chan < FGC_DIAG_UNUSED_CHAN)       // else if valid non-DIM channel
                {
                    fbs.u.fieldbus_stat.diag_chan[i] = reported_chan;           // Report channel
                    fbs.u.fieldbus_stat.diag_data[i] = diag.data[reported_chan];            // Return data
                }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsWriteStatVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare the header part of the status variable and will write it into the uFIP.
  This is run on millisecond 1 (class 51/59) or millisecond 2 (class 53), just ahead of the GW polling
  period (1.9ms - 10ms).  If the write is successful, the STATUS bits of the ack byte are incremented
  to let the GW know that it is fresh. These bits provide a sequence number that goes 0, 1, 2, 3, 1, 2,
  3, ... so 0 is only sent once.
\*---------------------------------------------------------------------------------------------------------*/
{

    /*--- Export the requested runlog data byte ---*/

    fbs.u.fieldbus_stat.runlog = RL_BUF_A[fbs.time_v.runlog_idx];


    /*--- Prepare header part of status variable ---*/

    if (fbs.pll_locked_f && fbs.gw_online_f)            // ack byte
    {
        // The gateway needs to receive status w/o this flag and then with this flag to properly reset communication.
        // Thus we check for fbs.gw_online_f which is not true for the first packets sent.
        Set(fbs.u.fieldbus_stat.ack,FGC_ACK_PLL);
    }

    fbs.u.fieldbus_stat.cmd_stat = fcm.stat;            // command status

    /*--- Write status variable to uFIP/ETH ---*/

    if (FipSendStatVar())                             // If send to uFIP/ETH succeeds
    {
        fbs.u.fieldbus_stat.ack = (((fbs.u.fieldbus_stat.ack + 1) & FGC_ACK_STATUS)
                | (fbs.u.fieldbus_stat.ack & (FGC_ACK_CMD_TOG | FGC_SELF_PM_REQ | FGC_EXT_PM_REQ)));

        if (!(fbs.u.fieldbus_stat.ack & FGC_ACK_STATUS))// If status bits are 0
        {
            fbs.u.fieldbus_stat.ack++;                  // Advance to 1
        }
    }
    else                                                // else send to uFIP/ETH failed
    {
        fbs.u.fieldbus_stat.ack &= (FGC_SELF_PM_REQ | FGC_EXT_PM_REQ |  // Keep STATUS, CMD_TOG
                FGC_ACK_STATUS | FGC_ACK_CMD_TOG);                      // and PM triggers

        fip.stats.v6_no_access++;                       // Update Var 6 no access counter
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void FbsPrepareMsg(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by MstTsk to prepare the next message
\*---------------------------------------------------------------------------------------------------------*/
{

    if (fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED)     // If PLL sync is locked
    {
        if (!fbs.pll_locked_f)          // If sync has just locked or gateway is just online.
        {
            fbs.pll_locked_f = true;    // Latch Sync Locked state
            FbsReconnect();
        }

        // Prepare Msg to be sent

        if (fcm.stat == FGC_EXECUTING)                  // If a command is executing
        {
            if (fbs.cmd.rsp_len > FGC_MAX_VAL_LEN)      // If response has overrun the buffer
            {
                fcm.errnum = FGC_RSP_BUF_FULL;          // Set error number
            }

            if (fcm.errnum != 0)                        // If an error has occurred and data is being sent
            {
                FbsCancelCmd(fcm.errnum);               // Cancel command response
            }
        }

        /* Transfer remote terminal characters to response string for FIP only*/

        if (fbs.n_term_chs < FGC_FIELDBUS_MAX_RTERM_CHARS  &&    // if less than max rterm chars already in msg and
           (fbs.termq.is_full     ||                        // term queue is full or
            fbs.termq.in_idx != fbs.termq.out_idx))         // at least not empty
        {
            FbsCpyFromQueue(&fbs.termq, &fbs.termq.out_idx, &fbs.n_term_chs, FGC_FIELDBUS_MAX_RTERM_CHARS);
            fbs.rsp_msg_len = fbs.n_term_chs;               // Set response length to rem term len
            fbs.pub.out_idx = fbs.pub.q.out_idx;            // Cancel previous pub data
            fbs.cmd.out_idx = fbs.cmd.q.out_idx;            // Cancel previous cmd rsp data
            fbs.pend_stream = NULL;                         // Clear pend stream pointer
            fbs.pend_pkt    = FBS_RTERM_PKT;                // Mark pending packet as RTERM only
            fbs.pend_header = FGC_FIELDBUS_FLAGS_CMD_PKT |  // Mark Remote terminal response as pending
                              FGC_FIELDBUS_FLAGS_SEQ_MASK;
        }

        /* Create a pub or cmd packet - pub takes priority over cmd data */

        if (fbs.pub.q.is_full ||                                // If publication queue is full or
            fbs.pub.out_idx != fbs.pub.q.in_idx)                // at least not empty
        {
            if (fbs.pend_pkt == FBS_CMD_PKT)                    // If pending pkt contained cmd data
            {
                fbs.cmd.out_idx = fbs.cmd.q.out_idx;            // Cancel this cmd data
                fbs.rsp_msg_len = fbs.n_term_chs;               // Set rsp length to rterm length
            }
            FbsQData(&fbs.pub, FBS_PUB_PKT);                    // Add pub data to packet
        }
        else if ((fbs.cmd.q.is_full ||                          // else if get cmd rsp queue is full or
                  fbs.cmd.out_idx != fbs.cmd.q.in_idx) &&       // at least not empty, and
                 (fbs.pend_pkt != FBS_PUB_PKT)                  // pending packet is not a pub packet
                 )
        {
            FbsQData(&fbs.cmd, FBS_CMD_PKT);                    // Add pub data to packet
        }

    }
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsWriteMsg(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from MstTsk() on ms 1 provided the PLL is locked and the TX msg buffer in the
  uFIP is empty.  It will queue a new message if one is pending, and it tidies up if a message has been sent.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       next_seq;                           // Next sequence number
    next_seq = fbs.pend_header + 1;                 // Inc sequence number for stream

    /*--- Message Sent ---*/

    if (fbs.send_pkt)                               // If message was being sent
    {

        if (Test(fbs.send_header, FGC_FIELDBUS_FLAGS_LAST_PKT)  // If pkt sent was a last packet, and
                && (fbs.send_pkt == FBS_CMD_PKT))               // pkt contained a command response
        {
            if (fcm.stat == FGC_EXECUTING)          // If command executing
            {
                fcm.stat = FGC_OK_RSP;              // Report exec completed OK
            }
        }

        fbs.send_pkt = 0;                           // Clear send rsp flags
    }

    /*--- Send Pending Message ---*/

    if (fbs.pend_pkt)                               // If new packet is pending
    {

        FipSendMsg();                               // Send pending msg to uFIP/ETH

        fbs.send_header    = fbs.pend_header;       // Save packet header
        fbs.send_pkt       = fbs.pend_pkt;          // Mark packet as "being sent"
        fbs.pend_pkt       = 0;                     // Clear pending packet buffer
        fbs.n_term_chs     = 0;
        fbs.rsp_msg_len    = 0;
        fip.send_msg_timer = 0;                     // Reset send timer
        fip.start_tx       = false;                 // Clear the start transmission flag

        if (fbs.pend_stream)                        // If pkt contains cmd or pub data
        {
            next_seq = fbs.pend_header + 1;         // Inc sequence number for stream

            Clr (fbs.pend_stream->header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT| FGC_FIELDBUS_FLAGS_SEQ_MASK);

            fbs.pend_stream->header_flags += (next_seq & FGC_FIELDBUS_FLAGS_SEQ_MASK);

            {
                fbs.pend_stream->q.out_idx = fbs.pend_stream->out_idx;  // Advance circular buffer pointer
                fbs.pend_stream->q.is_full = false;                     // Mark queue as not full

                if (fbs.pend_stream->q_not_full->pending)               // If tasks are pending on sem
                {
                    OSSemPost(fbs.pend_stream->q_not_full);             // Post semaphore
                }
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsQData(struct fbs_stream *stream, uint8_t pkt_type)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from FbsPrepareMsg to prepare the response packet by taking data from the
  specified stream (pub or cmd).
\*---------------------------------------------------------------------------------------------------------*/
{
    if (fbs.rsp_msg_len < fbs.cmd_rsp_len)                              // If space remains in packet
    {
        fbs.pend_stream = stream;                                       // Save stream pointer for packet
        fbs.pend_pkt = pkt_type;                                        // Save pkt type

        FbsCpyFromQueue(&stream->q, &stream->out_idx, &fbs.rsp_msg_len, // Fill packet from stream buffer
                fbs.cmd_rsp_len);

        if (stream->out_idx == stream->q.in_idx)                        // If output queue now empty
        {
            fbs.pend_header = stream->header_flags;                     // Save pkt header

            if (Test(fbs.pend_header,FGC_FIELDBUS_FLAGS_LAST_PKT))      // If last pkt
            {
                fbs.pend_pkt = pkt_type;                                // Save pkt type
            }
        }
        else
        {
            fbs.pend_header = stream->header_flags & ~FGC_FIELDBUS_FLAGS_LAST_PKT;  // Save pkt header without
        }                                                                           // last pkt flag bit
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsCpyFromQueue(struct queue *q, uint16_t *out_idx, uint16_t *rsp_idx, uint16_t max_rsp_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will copy from either the Eth/Fip cmd output queue or the remote terminal output queue into
  the linear response buffer.  It is called from FbsPrepareMsg().
\*---------------------------------------------------------------------------------------------------------*/
{
    char *      in_circular_buffer;
    char *      out_linear_buffer = &fbs.rsp_pkt[*rsp_idx];

    uint8_t       n_bytes;
    uint8_t       n_blocks;
    uint8_t       n_chs;
    uint8_t       max_chs;
    uint8_t       qidx;
    uint8_t       dec_mask;

    max_chs  = max_rsp_idx - *rsp_idx;                  // Max number of characters which may be transferred
    dec_mask = q->dec_mask;                             // Local copy of dec mask for the queue
    qidx      = *out_idx;                               // Out index in queue
    n_chs    = (q->in_idx - qidx) & dec_mask;           // Number of characters in queue (0=FULL)

    if(!n_chs || n_chs > max_chs)                       // If more characters than space
    {
        n_chs = max_chs;                                // Clip to space available
    }

    n_blocks  = 0;                                      // Initialise block copy count to zero
    n_bytes   = n_chs;                                  // Initialise byte copy count to n_chs
    in_circular_buffer = q->buf;                        // Address of queue buffer

    // Important note for FIP on FGC2. The implementation below is doing a block copy and depending
    // on the alignment of the source and target addr, it will copy an excess of up to 8 bytes in the
    // target buffer (fbs.rsp_pkt[]). For that reason the buffer fbs.rsp_pkt[] must have a size that
    // is equal to the maximum message size (120) + a provision for the copy excess (8).

#ifndef __CDT_PARSER__   // Suppress Eclipse errors for asm block
    asm
    {
      // Calculate number of bytes and blocks to copy

      LDAA    qidx                    // A = qidx
      ANDA    #7                      // A = qidx & 7
      BEQ     ZeroExcess              // Branch if zero (no excess characters)
      LDAB    #8                      // B = 8
      XGAB                            // A = 8, B = (qidx & 7)
      SBA                             // A = n_excess = 8 - (qidx & 7)
      CMPA    n_chs                   // if(n_excess >= n_chs)
      BCC     ByteCopy                // then branch to ByteCopy n_chs bytes
    ZeroExcess:
      STAA    n_bytes                 // n_bytes = n_excess
      XGAB                            // B = n_excess
      LDAA    n_chs                   // A = n_chs
      ADDA    #7                      // A = n_chs + 7
      SBA                             // A = n_chs + 7 - n_excess
      LSRA                            // A /= 2
      LSRA                            // A /= 2
      LSRA                            // A /= 2
      STAA    n_blocks                // n_blocks = (n_chs + 7 - n_excess) / 8

      // Byte copy

   ByteCopy:
      LDY     out_linear_buffer       // IY = response buffer address
      LDAB    qidx                    // B  = out index in queue
      TST     n_bytes                 // Test n_bytes to copy
      BEQ     BlockCopy               // Branch to block copy if no individual bytes
      LDX     in_circular_buffer      // IX = &in_circular_buffer
      ABX                             // IX = &in_circular_buffer[qidx] = address of first character
    CopyByteLoop:
      LDAA    0,X                     // [6] Read byte from queue
      STAA    0,Y                     // [4] Store in response buffer
      AIX     #1                      // [2] Adjust queue pointer
      AIY     #1                      // [2] Adjust response buffer pointer
      DEC     n_bytes                 // [8] Decrement byte count
      BNE     CopyByteLoop            // [6] Branch back until no more bytes to copy

      ANDB    #0xF8                   // Adjust index to start of excess block
      ADDB    #8                      // Adjust qidx to start of next block

      // Block copy

    BlockCopy:
      LDAA    n_blocks                // A = number of 8 byte blocks to copy
      BEQ     End                     // Branch if no blocks to copy
    BlockCopyLoop:
      LDX     in_circular_buffer      // [6] IX = queue buffer
      ANDB    dec_mask                // [6] Clear bits outside index range
      ABX                             // [2] IX = IX[idx] = address of first character
      TDE                             // [2] Save D in E
      LDD     0,X                     // [6] Read first word of block (use D as it's quicker than E)
      STD     0,Y                     // [4] Store first word
      LDD     2,X                     // [6] Read second word of block
      STD     2,Y                     // [4] Store second word
      LDD     4,X                     // [6] Read third word of block
      STD     4,Y                     // [4] Store third word
      LDD     6,X                     // [6] Read last word of block
      STD     6,Y                     // [4] Store last word
      AIY     #8                      // [2] Adjust response buffer pointer by 8 bytes
      TED                             // [2] Recover D from E
      ADDB    #8                      // [2] Adjust index to start of next block
      DECA                            // [2] Decrement block count
      BNE     BlockCopyLoop           // [6] Branch back until no more bytes to copy
      End:
    }
#endif

    *out_idx  = (*out_idx + n_chs) & dec_mask;          // Update queue out index
    *rsp_idx += n_chs;                                  // Update index in response buffer
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsCancelCmd(uint8_t stat)
/*---------------------------------------------------------------------------------------------------------*\
  This function will cancel the get command response.  It flushes the queue, aborts the command
  and inhibits further output through the fcm stream.  If there is a pending packet with get command
  response characters it flushes those characters, preserving remote terminal characters if present.
\*---------------------------------------------------------------------------------------------------------*/
{
    fcm.stat          = stat;                               // Report status to GW
    fcm.abort_f       = true;                               // Abort fip command and inhibit output to cmd stream
    fbs.cmd.q.out_idx = fbs.cmd.q.in_idx;                   // Reset fifo pointers
    fbs.cmd.out_idx   = fbs.cmd.q.in_idx;                   // Reset local queue out pointer
    fbs.cmd.q.is_full = false;                              // Mark queue as not full

    if (fbs.cmd.q_not_full->pending)                        // If tasks are pending on sem
    {
        OSSemPost(fbs.cmd.q_not_full);                      // Post semaphore
    }

    if (stat != FGC_PLL_NOT_LOCKED &&                       // If PLL not unlocked
      fbs.pend_pkt == FBS_CMD_PKT)                          // pending contains command response
    {
        if (fbs.n_term_chs)                                 // If pkt also contains RTERM chars
        {
            fbs.rsp_msg_len = fbs.n_term_chs;               // Remove get cmd rsp from msg
            fbs.pend_stream = NULL;                         // Clear pend stream pointer
            fbs.pend_pkt    = FBS_RTERM_PKT;                // Mark pending packet as RTERM only
            fbs.pend_header = FGC_FIELDBUS_FLAGS_CMD_PKT|   // Mark Remote terminal response as pending
                              FGC_FIELDBUS_FLAGS_SEQ_MASK;
        }
        else                                                // else pkt has no RTERM chars
        {
            fbs.pend_pkt    = 0;                            // Cancel pending msg completely
            fbs.rsp_msg_len = 0;
            fbs.n_term_chs  = 0;
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void FbsPutcFcm(char ch, FILE *f)
/*---------------------------------------------------------------------------------------------------------*\
  This function is defined as the callback for the output FILE stream used by FcmTsk.
  It reduces multiple consecutive newlines to a single newline before storing the characters in the
  appropriate stream buffer.
\*---------------------------------------------------------------------------------------------------------*/
{

    if (ch == '\n')                     // If character is newline (linefeed)
    {
        // Set newline flag
        Set(f->_flags,NL);

        return;
    }

    // If newline flag is set
    if (Test(f->_flags, NL))
    {
        FbsPutcStoreFcm('\n');          // Write a single newline to buffer
        // Clear newline flag
        Clr(f->_flags,NL);
    }

    FbsPutcStoreFcm(ch);                // Write character to buffer;
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsPutcStoreFcm(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to store the character ch in the buffer for the FIP get command stream.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_ENTER_CRITICAL();                                // Protect against interrupts

    if (!fcm.abort_f)                                   // If command has not been aborted
    {
        if (fbs.cmd.q.is_full)                          // If buffer is full
        {
            OSSemPend(fbs.cmd.q_not_full);              // Wait on semaphore
        }

        fbs.cmd.q.buf[fbs.cmd.q.in_idx] = ch;           // Store character in queue

        fbs.cmd.q.in_idx = (fbs.cmd.q.in_idx + 1) & fbs.cmd.q.dec_mask; // Adjust in index

        if (fbs.cmd.q.in_idx == fbs.cmd.q.out_idx)      // If buffer now full
        {
            fbs.cmd.q.is_full = true;                   // set buf full flag
        }

        fbs.cmd.rsp_len++;                              // Increment total response length
    }

    OS_EXIT_CRITICAL();
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsOutShort(uint8_t * ch, struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write two bytes from *ch to the cmd stream c.
  It is used to send the binary log data.
\*---------------------------------------------------------------------------------------------------------*/
{
    c->store_cb(ch[0]);
    c->store_cb(ch[1]);
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsOutLong(char * ch, struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write four bytes from *ch to the cmd stream c.
  It is used to send the binary log data.
\*---------------------------------------------------------------------------------------------------------*/
{
    c->store_cb(ch[0]);
    c->store_cb(ch[1]);
    c->store_cb(ch[2]);
    c->store_cb(ch[3]);
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsOutBuf(uint8_t * ch, uint16_t n_bytes, struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write n_bytes from ch to the cmd stream c.
  It is used to send the binary log data.
\*---------------------------------------------------------------------------------------------------------*/
{
    while (n_bytes--)
    {
        c->store_cb(*(ch++));
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void FbsOutTermCh(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by TrmMsOut() only, which checks that fbs.termq.stat does *not* equal Q_FULL
  before calling the function.  For this reason, it assumes that there is space for the character ch in
  the buffer.  It must then protect the calculation of the new input index and the buffer full flag.
\*---------------------------------------------------------------------------------------------------------*/
{
    fbs.termq_buf[fbs.termq.in_idx] = ch;       // Store character in queue

    OS_ENTER_CRITICAL();                        // Protect against interrupts

    fbs.termq.in_idx = (fbs.termq.in_idx + 1) & fbs.termq.dec_mask; // Increment input index

    if (fbs.termq.in_idx == fbs.termq.out_idx)  // Test for buffer now full
    {
        fbs.termq.is_full = true;
    }
    OS_EXIT_CRITICAL();
}

uint16_t  FbsStreamGetSize        (struct fbs_stream *s)
/*---------------------------------------------------------------------------------------------------------*\
  This function returns the size of the given stream.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (s->q.is_full)
    {
        return FBS_OUTQ_SIZE;
    }
    else if (s->out_idx <= s->q.in_idx)
    {
        return (s->q.in_idx - s->out_idx);
    }
    else // s->out_idx > s->q.in_idx
    {
        return (FBS_OUTQ_SIZE - s->out_idx + s->q.in_idx);
    }
}
// EOF
