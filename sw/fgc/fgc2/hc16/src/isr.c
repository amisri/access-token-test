/*---------------------------------------------------------------------------------------------------------*\
  File:         isr.c

  Purpose:      FGC HC16 Software - Interrupt Service Routines

  Notes:        The FGC HC16 software has two ISRs to catch the two user interrupts:

                    1. IsrFbs()         MicroFIP interrupt via GPT/IC1
                    2. IsrMst()         Millisecond Tick Interrupt from GPT/OC2

                Both interrupts come from the GPT, IsrMst from OC2 and IsrFbs from IC1.  All
                GPT interrupts have the same priority, so interrupts cannot be nested.

                Both ISRs are written in assembler (unfortunately) to improve performance.

  Test Points:  Normally TP1 is used for IsrFbs() and TP2 for IsrMst().  If you disable this using
                the constant below (set to 0) then you can use the test points elsewhere in the code
                using:
                                TP_SET(1); or TP_SET(2);
                                TP_RST(1); or TP_RST(2);
                                TP_TGL(1); or TP_TGL(2);
\*---------------------------------------------------------------------------------------------------------*/

#include <isr.h>

#include <stdbool.h>
#include <stdint.h>
#include <cmd.h>
#include <trm.h>
#include <tsk.h>
#include <fbs_class.h>      // for ST_LATCHED
#include <definfo.h>        // for FGC_CLASS_ID
#include <cmd.h>            // for tcm global variable
#include <fbs.h>            // for fbs global variable
#include <mst.h>            // for mst global variable
#include <dpcom.h>          // for dpcom global variable
#include <dpcls.h>          // for dpcls global variable
#include <memmap_mcu.h>     // for CPU_RESET_CTRL_ANALOG_MASK16
#include <fip.h>            // for fip global variable
#include <pll_main.h>       // for pll global variable, PllSync()
#include <mem.h>            // for MemCpyFip()
#include <start.h>          // for Crash()
#include <log_class.h>      // for log_iab global variable
#include <get_class.h>      // for meas global variable
#include <edac.h>
#include <trap.h>           // for global variable buserr_chk
#include <defconst.h>       // for FGC_INTERFACE_
#if (FGC_CLASS_ID == 59)
#include <init_class.h>     // for ref data structure
#endif
#include <macros.h>

#define TP1_ENABLE      1                       // 1: TP1 linked to IsrFbs    0: TP1 available
#define TP2_ENABLE      1                       // 1: TP2 linked to IsrMst    0: TP2 available

/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT                         // Returns via OSIntExit()
/*---------------------------------------------------------------------------------------------------------*/
void IsrFbs(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called in response to the GPT IC1 interrupt.  This is connected to the MicroFIP
  interface IRQ.  It reads and interprets the MicroFIP interrupt status register (FIP_IRQSA) to identify
  the reason for the interrupt:

                Bit   Enabled   Meaning
                --------------------------------
  FIP_IRQSA     0x01    No      Var 6 sent
                0x02    No      Synchro received
                0x04    No      Message sent
                0x08    Yes     Message received
                0x10    No      Var 1 received
                0x20    No      Var 3 received
                0x40    No      Var 5 received
                0x80    Yes     Var 7 received
                --------------------------------

  Only two of the eight possible interrupt sources are enabled: Message reception and Variable 7
  reception.  In theory, both bits could be set if the interrupt response was so slow that first
  a message was received and then Variable 7, however, in practice this should never happen.
  This IRQ is written assuming that only one of the two bits will ever be set at one time.

  0x08  The FGC can receive command packets in messages from the gateway.  The ISR checks
        that the message length is sufficient and if so it extracts the header word and
        resumes the FbsTsk to read out the new message.

  0x80  Variable 7 is a consumed variable that has a special feature within the MicroFIP.
        Normally, the variable ID is 0x07xy, where XY is the WorldFIP node number for the FGC,
        however, with variable 7 it is possible to override this ID with any value, thus
        enabling a variable to be broadcast by the gateway.  This facility is used for three
        different broadcast variables: 0x3000 (time), 0x3002 (RT data for FGCs 1-15) and
        0x3003 (RT data for FGCs 16-30).  Every FGC receives the time variable, and one of
        the two RT data variables, according their node number.

        The time variable is the first to be sent each WorldFIP cycle.  The cycle period is 20ms
        and the arrival of the time variable provides the sychronisation pulse for the FGC
        phase locked loop (PLL).  The jitter on this IRQ is less than 7us.  The GPT input
        capture 1 channel logs the time of the IRQ with a resolution of 0.5us, so the interrupt
        response time doesn't affect the behaviour of the PLL.  The global variables fbs.sync.tic0
        and fbs.sync.tic0_f are used to pass the GPT IC1 time and a validity flag respectively.
        When the time variable is received, the ISR passes the FIP_ACT_TIM_RCV action code
        to the FbsTsk() via the message queue.

        The RT data variables are the last to be sent each WorldFIP cycle.  The ISR directly
        accesses the variable and extracts the four bytes specific to the FGC and passes them
        to the DSP (class 51) or copies the entire variable (class 59).  It is not used in class 53.



    in the FGC3 is done by FieldbusISR()

\*---------------------------------------------------------------------------------------------------------*/
{
    static uint8_t tmp = 0;

    asm
    {
        LDAA    FIP_IRQSA_16                    // Load content of FIP_IRQSA to register A. Read-out is destructive!
        STAA    tmp                             // Store the content in a variable

        BITA    #136                            // Check if both 0x80 and 0x08 bits are set (it should never happen!)
        BNE     normal                          // Skip next instruction if everything's fine
        INCW    fip.stats.clash_int_src         // Increment error counter otherwise

    normal:

        LDX     #0x0000                         // Prepare IX to access GPT registers in page F
        BCLR    GPT_TFLG1_16,X,#GPT_TFLG1_IC1F_MASK8 // Clear IC1 interrupt in GPT_TFLG register

#if TP1_ENABLE == 1
        BSET    SIM_PORTC_16,X,#0x01            // Set TP1
#endif
        BRSET   tmp,#0x80,var7                  // Branch to check time var if "Var 7 rcv" bit is set

        // Message received - pass to FbsTsk

        JSR     FipMsgsa                        // Call FipMsgsa() to test msg status (returned in B)
        BITB    #0x01                           // Test bit 0 (rdy_msg = 1 if valid msg is waiting in buffer)
        BEQ     clrmsg                          // Branch to clear message if no valid message is waiting

        CLRA                                    // Clear high byte of D
        LDAB    FIP_MSCOR_16                    // B = D = Message length
        SUBD    #8                              // Sub 8 from D to get payload length (1-120)
        BLE     shortmsg                        // Branch if message is too short (no payload)
        STD     fbs.pkt_len                     // Save payload length for FbsTsk() to use

        LDD     FIP_MSGHEAD_16                  // Extract header word (flags:user) from FIP msg
        STD     fbs.rcvd_header                 // Save header word for FbsTsk()

        LDD     #TSK_FBS
        JSR     OSTskResume                     // OSTskResume(TSK_FBS)
        BRA     end

    shortmsg:
        INCW    fip.stats.msg_too_short         // Increment statistics counter for short messages

    clrmsg:
        LDAB    #0x01                           // Disable message reception...
        STAB    FIP_CONFA_16                    // FIP_CONFA = 0x01
        LDAB    #0x03                           // Enable message reception...
        STAB    FIP_CONFA_16                    // FIP_CONFA = 0x03
        BRA     end                             // Skip to end

        // Variable received - process in ISR

    var7:
        BRCLR   FIP_MPSSA_16,#0x80,noacc        // Branch if Var 7 is not accessible
        BCLR    FIP_CONFB_16,#0x80              // Clear CONFB bit 7 to lock Var 7
        BSET    FIP_CONFD_16,#0x70              // Identify Var 7 access to uFIP

        TST     FIP_VIDGL_16                    // Check type of variable received
        BEQ     timevar                         // Branch if time variable received

    rdvar:
        CLR     FIP_VIDGL_16                    // Reset Variable 7 index to receive time variable next
        LDAA    FIP_RTDATA0_16                  // Get RD var header byte
        CMPA    fip.rd_header                   // Check against last header
        BEQ     endacc                          // Branch if no new data received

        STAA    fip.rd_header                   // Save new header byte
        STD     fip.rd_rcvd_f                   // Set RD rcvd flag (D cannot be zero)

#if FGC_CLASS_ID == 59
        LDD     @dpcls.mcu.wval:4               // Prepare to copy to wval[] array
        LDX     #(FIP_RTDATA0_16+4)             // from the RT
        PSHM    D,X
        LDD     #60                             // Copy 15 floats
        JSR     MemCpyFip
        AIS     #4
#else
        LDY     fip.rt_ref_offset               // Y = offset to RT data for this controller
        LDD     FIP_RTDATA0_16,Y                // D = most significant word
        STD     dpcls.mcu.ref.rt_data:0         // Store in dpram
        LDD     FIP_RTDATA2_16,Y                // D = least significant word
        STD     dpcls.mcu.ref.rt_data:2         // Store in dpram
        STY     dpcls.mcu.ref.rt_data_f         // Flag to DSP that new ref data is waiting
#endif
        BSET    fbs.u.fieldbus_stat.ack,#FGC_ACK_RD_PKT // Set RD rcvd flag in status variable
        BRA     endacc

    noacc:
        INCW    fip.stats.v7_no_access          // Increment no access stats
        BRA     end

    timevar:
        LDY     fip.runlog_idx_offset
        LDD     FIP_TIMEVAR_32,Y
        CPD     fbs.last_runlog_idx             // if fbs.time_v.runlog_idx != fbs.last_runlog_idx
        BNE     timevarfresh                    // then jump to process fresh time var

        INCW    fip.stats.v7_repeated           // fip.stats.v7_repeated++
        BEQ     endacc                          // skip this time var

    timevarfresh:
        STD     fbs.last_runlog_idx             // fbs.last_runlog_idx = fbs.time_v.runlog_idx

        LDAA    fip.rt_ref_vidgl                // Change var 7 global index to receive RT data variable
        STAA    FIP_VIDGL_16

        LDD     @fbs.time_v                     // Prepare to copy to time_v structure
        LDX     #FIP_TIMEVAR_32                 // from the MicroFIP
        PSHM    D,X
        LDD     #FIP_TIMEVAR_B
        JSR     MemCpyFip
        AIS     #4

        LDD     #1
        STD     fbs.time_rcvd_f                 // Set time rcvd flag (D cannot be zero)
        STD     fbs.events_rcvd_f               // Set events rcvd flag (D cannot be zero)
        STD     fbs.sync.tic0_f                 // Set time received flag (cleared in PllSync())

        LDX     #0x0000                         // Prepare IX to access GPT registers in page F
        LDE     GPT_TIC1_16,X                   // D = GPT_TIC1 = GPT input capture 1 latch = time of IRQ
        STE     fbs.sync.tic0                   // Save in global variable

        BSET    fbs.u.fieldbus_stat.ack,#FGC_ACK_TIME  // Acknowledge time_var reception

        LDED    fbs.time_v.unix_time
        STED    dpcom.mcu.time.fbs.unix_time    // dpcom.mcu.time.fbs.unix_time = fbs.time_v.unix_time

        LDD     fbs.time_v.ms_time
        STD     dpcom.mcu.time.fbs.ms_time:2    // dpcom.mcu.time.fbs.ms_time = fbs.time_v.ms_time

    endacc:
        BSET    FIP_CONFB_16,#0x80              // Set bit 7 to unlock Var 7
        BRA     end
    end:
#if TP1_ENABLE == 1
        LDX     #0x0000                         // Prepare IX to access GPT registers in page F
        BCLR    SIM_PORTC_16,X,#0x01            // Reset TP1
#endif
    }
    OS_INT_EXIT();                      // Exit from interrupt through scheduler (Never returns)
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT                         // Returns via OSIntExit()
/*---------------------------------------------------------------------------------------------------------*/
void IsrMst(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called in response to the GPT OC2 interrupt.  It must clear the interrupt and reschedule
  for the next one, maintaining an average frequency of 1 kHz.  When connected to the WorldFIP network,
  the period is controlled by the Phase Locked Loop software (see pll_main.c).

  IsrMst() is responsible for reading and writing characters from/to the terminal, while respecting XON/XOFF flow
  control and the status of the remote terminal connection.  It detects Ctrl-C to abort the terminal get command
  and Ctrl-X to disconnect the remote terminal.

  Finally, it uses OSTskResume() to wake up the millisecond task.

  in the FGC3 is done by INT_Excep_IRQ4() or INT_Excep_TMR0_CMIA0()

\*---------------------------------------------------------------------------------------------------------*/
{
//  static uint16_t       frac_counter; now replaced by pll.frac_counter

    asm
    {
        LDX     #0x0000                         // Prepare to access GPT registers in page F

#if TP2_ENABLE == 1
        BSET    SIM_PORTC_16,X,#0x02            // Set TP2
#endif

        // ----- CLASS 51/53 - PM/IAB logs - Read ADC and write DAC -----

#if FGC_CLASS_ID != 59
        LDD     dpcls.dsp.reg.run_f:2           // Class 51/53: copy the regulation state of the previous DSP iteration
        STD     mst.dsp_reg_run_f

#if FGC_CLASS_ID == 51
        LDED    dpcls.dsp.meas.ia               // Class 51: Get 1 kHz DCCT current measurements from DSP
        STED    meas.ia
        LDED    dpcls.dsp.meas.ib
        STED    meas.ib
#else
        LDED    dpcls.dsp.ref.now               // Class 53: Get 1 kHz PM signals from DSP
        STED    pm_sigs.ref
        LDED    dpcls.dsp.ref.v
        STED    pm_sigs.v_ref
        LDED    dpcls.dsp.meas.b
        STED    pm_sigs.b_meas
        LDED    dpcls.dsp.meas.i
        STED    pm_sigs.i_meas
        LDED    dpcls.dsp.meas.v
        STED    pm_sigs.v_meas
        LDED    dpcls.dsp.reg.err
        STED    pm_sigs.err
#endif

        BRCLR   dpcls.dsp.ref.start_f:3,#1,slot5 // Branch if ref start flag is NOT set
        CLRW    dpcls.dsp.ref.start_f:2         // Clear ref start flag

    slot5:
        LDD     SM_INTERFACETYPE_16
        CPD     #FGC_INTERFACE_SAR_400          // Test interface type (1=SAR 2=SD)
        BNE     adcsd

        // First reading of SAR ADCs

    adcsar0:
        LDD     #0x8000                         // Prepare to toggle sign bit
        EORD    ANA_SAR_A_16                    // Read 16 bit value from ADC and convert to unsigned
        STD     dpcls.mcu.adc.chan:2                    // Pass to DSP

        LDD     #0x8000                         // Prepare to toggle sign bit
        EORD    ANA_SAR_B_16                    // Read 16 bit value from ADC and convert to unsigned
        STD     dpcls.mcu.adc.chan:14           // Pass to DSP

        LDD     GPT_TCNT_16,X                   // Take time now
        ADDD    #0x0002                         // Advance to next us
        STD     GPT_TOC3_16,X                   // Arm analogue trigger

        BRA     writedac

        // Only reading of SD ADCs (350/351). Note: SD-360 is no more supported.

    adcsd:
#if FGC_CLASS_ID == 51
        BSET    CPU_MODEH_16,#CPU_MODEH_C32IRQ_2_MASK8  // Trigger INT2 on DSP
        MOVW    CPU_MODE_16,mst.mode                    // Read back MODE register
#endif

        LDD     #FGC_FILTER_STATE_READY         // If ADC filters are in reset
        CPD     dpcls.mcu.adc.filter_state:2
        BNE     writedac                        // Skip the ADC readings

        CLRW    dpcls.mcu.adc.buserr_f:2        // Clear the bus error flags
        CLRW    dpcls.mcu.adc.buserr_f:6

        BSETW   buserr_chk,#3                   // Set bus error check counter to prevent IsrTrap
                                                // The MCU will temporarily tolerate up to 3 bus errors
    adcsda:
        LDD     ANA_SD_NL_A_R_HIGH_16           // Read high 16 bits from ADC A
        STD     dpcls.mcu.adc.chan:0            // Pass to DSP
        LDD     ANA_SD_NL_A_R_MID_16            // Read middle 16 bits from ADC A
        STD     dpcls.mcu.adc.chan:2            // Pass to DSP
        LDD     ANA_SD_NL_A_R_LOW_16            // Read low 16 bits from ADC A
        STD     dpcls.mcu.adc.chan:4            // Pass to DSP

        LDD     #3
        CPD     buserr_chk                      // Check for bus error during ADC A reading
        BEQ     adcsdb                          // If all OK, proceed to channel B reading
        BSETW   dpcls.mcu.adc.buserr_f:2,#1     // Else set the bus error flag for ADC channel A
        BSETW   buserr_chk,#3                   // Set bus error check flag

    adcsdb:
        LDD     ANA_SD_NL_B_R_HIGH_16           // Read high 16 bits from ADC B
        STD     dpcls.mcu.adc.chan:12           // Pass to DSP
        LDD     ANA_SD_NL_B_R_MID_16            // Read middle 16 bits from ADC B
        STD     dpcls.mcu.adc.chan:14           // Pass to DSP
        LDD     ANA_SD_NL_B_R_LOW_16            // Read low 16 bits from ADC B
        STD     dpcls.mcu.adc.chan:16           // Pass to DSP

        LDD     #3
        CPD     buserr_chk                      // Check for bus error during ADC B reading
        BEQ     adcsdend                        // If all OK, proceed to end of ADC reading
        BSETW   dpcls.mcu.adc.buserr_f:6,#1     // Else set the bus error flag for ADC channel B

    adcsdend:
        CLRW    buserr_chk                      // Clear bus error check flag to enable IsrTrap again
        BRA     writedac

        // Write to DAC

    writedac:
        LDED    dpcls.dsp.ref.dac_raw           // Get 32-bit value from DSP
        STE     ANA_DAC_HIGH_16                 // Write high word
        NOP                                     // Pause
        STD     ANA_DAC_LOW_16                  // Write low word to trigger transfer of 20 msbs
#endif  // #if FGC_CLASS_ID != 59

        // ----- CLASS 59 - Write functions to RFC card unless card was just reset -----

#if FGC_CLASS_ID == 59

        // If RFC was triggered then check RFC watchdog register

        TSTW    rfc.trigger_f                   // Was RFC triggered on previous millisecond
        BEQ     checkreset                      // Skip forward if not

        LDD     rfc.counter                     // D = expected Tx counter value
        CPD     RFC_COUNTER_16                  // Compare with actual Tx counter value
        BEQ     readstatus                      // Branch if the same (OK)

        INCW    rfc.faults:2                    // rfc.faults++
        BNE     noinchigh
        INCW    rfc.faults:0
   noinchigh:
        BSETW   ST_LATCHED,#FGC_LAT_RFC_FLT     // Set RFC_FLT latched status bit (linked to HW warning)

        // Read RFC status from previous millisecond and pass to DSP

   readstatus:
        LDED    RFC_CHANDATA_16+0
        STE     dpcls.mcu.rfc_status:2
        STD     dpcls.mcu.rfc_status:6
        LDED    RFC_CHANDATA_16+4
        STE     dpcls.mcu.rfc_status:10
        STD     dpcls.mcu.rfc_status:14
        LDED    RFC_CHANDATA_16+8
        STE     dpcls.mcu.rfc_status:18
        STD     dpcls.mcu.rfc_status:22
        LDED    RFC_CHANDATA_16+12
        STE     dpcls.mcu.rfc_status:26
        STD     dpcls.mcu.rfc_status:30
        LDED    RFC_CHANDATA_16+16
        STE     dpcls.mcu.rfc_status:34
        STD     dpcls.mcu.rfc_status:38
        LDED    RFC_CHANDATA_16+20
        STE     dpcls.mcu.rfc_status:42
        STD     dpcls.mcu.rfc_status:46
        LDED    RFC_CHANDATA_16+24
        STE     dpcls.mcu.rfc_status:50
        STD     dpcls.mcu.rfc_status:54
        LDED    RFC_CHANDATA_16+28
        STE     dpcls.mcu.rfc_status:58
        STD     dpcls.mcu.rfc_status:62

        // Check if an RFC reset is active or required

   checkreset:
        TSTW    dpcls.mcu.rfc_reset:2           // If reset is not progress
        BEQ     rfcresetreq                     // jump to test if a reset is needed
        DECW    dpcls.mcu.rfc_reset:2           // Decrement reset counter
        BRA     endrfc                          // Not zero to exit

    rfcresetreq:
        TSTW    rfc.reset_f                     // Test RFC reset request
        BEQ     setdata                         // Skip forward if no request active

        BSETW   CPU_RESET_16,#CPU_RESET_CTRL_ANALOG_MASK16 // Activate reset to analogue interface slot (RFC)
        CLRW    rfc.reset_f                             // Clear reset request flag
        CLRW    rfc.trigger_f                           // Clear trigger variable
        LDD     #RFC_RESET_TIME                         // Prepare to suspend access to RFC
        STD     dpcls.mcu.rfc_reset:2                   // Initialise reset down counter used by DSP
        BCLRW   CPU_RESET_16,#CPU_RESET_CTRL_ANALOG_MASK16 // Clear reset request to launch FPGA programming
        BRA     endrfc

        // If RFC to be triggered read Ival from DSP and save for logging and transfer to RFC interface

   setdata:
        LDD     dpcls.dsp.fgfunc.ival:2
        STD     fgfunc.ival:0
        STD     RFC_CHANDATA_16+0
        LDD     dpcls.dsp.fgfunc.ival:6
        STD     fgfunc.ival:2
        STD     RFC_CHANDATA_16+2
        LDD     dpcls.dsp.fgfunc.ival:10
        STD     fgfunc.ival:4
        STD     RFC_CHANDATA_16+4
        LDD     dpcls.dsp.fgfunc.ival:14
        STD     fgfunc.ival:6
        STD     RFC_CHANDATA_16+6
        LDD     dpcls.dsp.fgfunc.ival:18
        STD     fgfunc.ival:8
        STD     RFC_CHANDATA_16+8
        LDD     dpcls.dsp.fgfunc.ival:22
        STD     fgfunc.ival:10
        STD     RFC_CHANDATA_16+10
        LDD     dpcls.dsp.fgfunc.ival:26
        STD     fgfunc.ival:12
        STD     RFC_CHANDATA_16+12
        LDD     dpcls.dsp.fgfunc.ival:30
        STD     fgfunc.ival:14
        STD     RFC_CHANDATA_16+14
        LDD     dpcls.dsp.fgfunc.ival:34
        STD     fgfunc.ival:16
        STD     RFC_CHANDATA_16+16
        LDD     dpcls.dsp.fgfunc.ival:38
        STD     fgfunc.ival:18
        STD     RFC_CHANDATA_16+18
        LDD     dpcls.dsp.fgfunc.ival:42
        STD     fgfunc.ival:20
        STD     RFC_CHANDATA_16+20
        LDD     dpcls.dsp.fgfunc.ival:46
        STD     fgfunc.ival:22
        STD     RFC_CHANDATA_16+22
        LDD     dpcls.dsp.fgfunc.ival:50
        STD     fgfunc.ival:24
        STD     RFC_CHANDATA_16+24
        LDD     dpcls.dsp.fgfunc.ival:54
        STD     fgfunc.ival:26
        STD     RFC_CHANDATA_16+26
        LDD     dpcls.dsp.fgfunc.ival:58
        STD     fgfunc.ival:28
        STD     RFC_CHANDATA_16+28
        LDD     dpcls.dsp.fgfunc.ival:62
        STD     fgfunc.ival:30
        STD     RFC_CHANDATA_16+30

        // Trigger RFC interface

        LDD     RFC_COUNTER_16                  // Read rolling Tx counter
        ADDD    dpcls.dsp.n_trig_chans:2        // Add number of channels that are going to be sent
        STD     rfc.counter                     // Save result as expect Tx counter value after transmission

        STD     rfc.trigger_f                   // Set trigger flag (even if trigger value is zero)
        LDD     dpcls.dsp.trigger:2             // Get trigger from DSP
        STD     RFC_TRIGGER_16                  // Trigger the RFC

        INCW    rfc.triggers:2                  // rfc.triggers++   (32-bit counter in FGSTATS.RFC_TRIGGERS)
        BNE     copycval
        INCW    rfc.triggers:0

        // Copy Cval from DSP for logging

   copycval:
        LDED    dpcls.dsp.fgfunc.cval:0
        STED    fgfunc.cval:0
        LDED    dpcls.dsp.fgfunc.cval:4
        STED    fgfunc.cval:4
        LDED    dpcls.dsp.fgfunc.cval:8
        STED    fgfunc.cval:8
        LDED    dpcls.dsp.fgfunc.cval:12
        STED    fgfunc.cval:12
        LDED    dpcls.dsp.fgfunc.cval:16
        STED    fgfunc.cval:16
        LDED    dpcls.dsp.fgfunc.cval:20
        STED    fgfunc.cval:20
        LDED    dpcls.dsp.fgfunc.cval:24
        STED    fgfunc.cval:24
        LDED    dpcls.dsp.fgfunc.cval:28
        STED    fgfunc.cval:28
        LDED    dpcls.dsp.fgfunc.cval:32
        STED    fgfunc.cval:32
        LDED    dpcls.dsp.fgfunc.cval:36
        STED    fgfunc.cval:36
        LDED    dpcls.dsp.fgfunc.cval:40
        STED    fgfunc.cval:40
        LDED    dpcls.dsp.fgfunc.cval:44
        STED    fgfunc.cval:44
        LDED    dpcls.dsp.fgfunc.cval:48
        STED    fgfunc.cval:48
        LDED    dpcls.dsp.fgfunc.cval:52
        STED    fgfunc.cval:52
        LDED    dpcls.dsp.fgfunc.cval:56
        STED    fgfunc.cval:56
        LDED    dpcls.dsp.fgfunc.cval:60
        STED    fgfunc.cval:60

        // Trigger DSP IsrMst

    endrfc:
        BSET    CPU_MODEH_16,#CPU_MODEH_C32IRQ_2_MASK8  // Trigger INT2 on DSP
        MOVW    CPU_MODE_16,mst.mode            // Read back MODE register

#endif

        // ----- All Classes: Set GPT TOC2 to trigger next millisecond interrupt -----

        LDE     GPT_TOC2_16,X                   // Get GPT_TOC2 value...
        STE     mst.tic0                        // ... and save in global variable to enable timings
        BCLR    GPT_TFLG1_16,X,#GPT_TFLG1_OC2F_MASK8 // Clear OC2 interrupt in GPT_TFLG1 register
        LDD     pll.qtics_per_ms:2              // Load D = lsw(sy.qtics_per_ms)
        ADDD    pll.frac_counter                // Add this ...
        STD     pll.frac_counter                // ... to frac counter.  Carry is set on wrap around.
        ADCE    pll.qtics_per_ms                // E = Carry_bit + msw(sy.qtics_per_ms) + GPT_TOC2
        STE     GPT_TOC2_16,X                   // Store E in GPT_TOC2 to trigger next interrupt

        // Clear DIAG reset if active

        BRCLR   QSM_PORTQS_16,X,#QSM_PORTQS_CLK_MASK8,watchdog  // Skip if clock low (reset not active)
        CLR     QSM_PORTQS_16,X                 // Clear DIAG mpx and clock to cancel reset

        // Toggle watchdog lines

    watchdog:
        LDE     mst.wd_isr                      // E = last watchdog value
        CPE     mst.watchdog                    // Compare with watchdog value returned by MstTsk
        BNE     chgwd                           // Branch if value not correct (i.e. watchdog fault)
    good:
        LDAA    #GPT_PDR_TRIGWDMS_MASK8         // Prepare to toggle ms watchdog trigger

        CPE     mst.dicoalive                   // Compare with dicoalive value returned by MstTsk
        BNE     wdout                           // Branch if not set (no dico alive signal)

        ORAA    #GPT_PDR_TRIGDIG_MASK8          // Prepare to toggle digital trigger

    wdout:
        EORA    GPT_PDR_16,X                    // EOR with GPT_PDR
        STAA    GPT_PDR_16,X                    // GPT_PDR = A

    chgwd:
        ADDE    #0x10                           // Change watchdog value (always non-zero)
        STE     mst.wd_isr                      // Write back to variable

        // ----- CLASS 51 - Second reading of SAR ADCs -----

#if FGC_CLASS_ID == 51
        LDD     #FGC_INTERFACE_SAR_400
        CPD     SM_INTERFACETYPE_16             // Test interface type (1=SAR 2=SD)
        BNE     serial

        LDD     #0x8000                         // Prepare to toggle sign bit
        EORD    ANA_SAR_A_16                    // Read 16 bit value from ADC and convert to unsigned
        STD     dpcls.mcu.adc.chan:6                    // Pass to DSP

        LDD     #0x8000                         // Prepare to toggle sign bit
        EORD    ANA_SAR_B_16                    // Read 16 bit value from ADC and convert to unsigned
        STD     dpcls.mcu.adc.chan:18           // Pass to DSP

        LDD     GPT_TCNT_16,X                   // Take time now
        ADDD    #0x0002                         // Advance to next us
        STD     GPT_TOC3_16,X                   // Arm analogue trigger
#endif

        // ----- All Classes: Check SCI for I/O from terminal -----

        BRSET   CPU_MODEL_16,#CPU_MODEL_SRAM_MASK8,serial // If SRAM base, skip overrun check
        BRCLR   os.ready_mask:1,#TSK_MST_MASK,serial      // If MstTsk is still running then crash with BAD5 signature
        LDD     #0xBAD5                                   // Flag overrun failure in run log
        JSR     Crash

    serial:
        BRSET   QSM_SCSRL_16,X,#QSM_SCSRL_RDRF_MASK8,in  // Jump if character is waiting

    chkout:
        LDD     terminal.char_waits_for_tx      // Get flag for character to output
        BEQ     resume                          // Skip if no character is waiting
        LDE     terminal.xoff_timer             // Check if XOFF is active...
        BNE     decxoff                         // jump to decrement XOFF timer if it is
        LDX     #0x0000                         // Prepare to access registers in page F
        BRCLR   QSM_SCSRH_16,X,#QSM_SCSRH_TDRE_MASK8,resume // Skip if output buffer is full
        LDAA    terminal.snd_ch                 // Get character to output
        STAA    QSM_SCDRL_16,X                  // Put character into QSM_SCDR output register
        CLRW    terminal.char_waits_for_tx      // Clear flag to say the character has been sent

    resume:
        LDD     #TSK_MST                        // D = TSK_MST
        JSR     OSTskResume                     // Resume MstTsk
        BRA     endserial                       // Branch to end of serial code

    decxoff:
        DECW    terminal.xoff_timer             // decrement XOFF timer by 1 ms
        BRA     resume                          // Branch to resume MstTsk

    in:
        LDAB    QSM_SCDRL_16,X                  // Read character from input data register (QSM_SCDR)
        BMI     chkout                          // Ignore if bit 7 is set (B=128->255)
        CMPB    #19                             // Check if character is XOFF (19) (halt SCI output)
        BEQ     xoff                            // ... and jump if true
        CMPB    #17                             // Check if character is XON (17) (resume SCI output)
        BEQ     xon                             // ... and jump if true
        CMPB    #3                              // Check if character is CTRL-C (3) (abort SCI command)
        BEQ     ctrlc                           // ... and jump if true
    sendch:
        CLRA                                    // Clear A leaving D = B (new character)
        XGDY                                    // Move to Y (last paramemeter)
        LDD     terminal.msgq                   // Get msgq_sci (first parameter)
        PSHM    D                               // and push onto stack
        JSR     OSMsgPost                       // OSMsgPost(terminal.msgq,(void*)new_char)
        AIS     #2                              // Remove parameter from stack
        BRA     chkout                          // Branch to output character if required
    xon:
        CLRW    terminal.xoff_timer             // XON received: clear XOFF timer
        BRA     chkout                          // Branch to output character
    xoff:
        LDD     #60000                          // XOFF received: Set XOFF timer
        STD     terminal.xoff_timer             // to 60s and fall through...
        BRA     resume                          // Branch to resume MstTsk
    ctrlc:
        STAB    tcm.abort_f                     // Abort SCI command
        CLRW    terminal.xoff_timer             // clear XOFF timer
        LDY     terminal.msgq                   // Flush the SCI message queue...
        JSR     OSMsgFlush                      // of any waiting characters
        BRA     chkout                          // Go to check for output character

    endserial:
        LDX     #0x0000                         // Prepare to access GPT registers in page F

        // ----- All Classes: Check for single bit memory errors -----

        LDAA    GPT_PACNT_16,X                  // Get counter
        TAB                                     // Copy counter to B
        SUBB    edac.last_gpt_pacnt             // Subtract previous value
        BEQ     endsbe                          // No SBE detected

    sbedetected:
        STAA    edac.last_gpt_pacnt             // Remember new counter value
        CLRA                                    // D = A:B = 0:{difference in counter}
        ADDD    edac.num_errors                 // Add to SBE counter
        STD     edac.num_errors
        LDED    SM_UXTIME_16                    // Record timestamp for the SBE
        STED    edac.error_time

    endsbe:                                     // IX = 0000

#if FGC_CLASS_ID == 53  // POPS

        // ----- CLASS 53 - Read first 3 long words from serial links from PBL interface card -----

        LDD     #FGC_FILTER_STATE_READY         // If ADC filters are in reset
        CPD     dpcls.mcu.adc.filter_state:2
        BNE     trigdsp                         // Skip the links readings

#if TP2_ENABLE == 1
        BCLR    SIM_PORTC_16,X,#0x02            // Reset TP2
#endif
        LDE     LINK_LK1_DATA_16                // Word 1       PLC_PBL_PAM_status      CLC_PAL_POPS_status
        LDD     LINK_LK1_DATA_16
        STED    dpcls.mcu.pal.lk1.plc_pbl_pam_stat
        LDE     LINK_LK2_DATA_16
        LDD     LINK_LK2_DATA_16
        STED    dpcls.mcu.pal.lk2.clc_pal_pops_stat

        LDE     LINK_LK1_DATA_16                // Word 2       BmeasRaw                PAL/PBL Prop data
        LDD     LINK_LK1_DATA_16
        STED    dpcls.mcu.pal.lk1.bmeas_raw
        LDE     LINK_LK2_DATA_16
        LDD     LINK_LK2_DATA_16
        STED    dpcls.mcu.pal.lk2.pal_pbl_prop

        LDE     LINK_LK1_DATA_16                // Word 3       VOUT_DXP1               VOUT_DXP2
        LDD     LINK_LK1_DATA_16
        STED    dpcls.mcu.pal.lk1.vout_dxp1
        STED    dpcls.mcu.pal.pam_raw:0
        LDE     LINK_LK2_DATA_16
        LDD     LINK_LK2_DATA_16
        STED    dpcls.mcu.pal.lk2.vout_dxp2
        STED    dpcls.mcu.pal.pam_raw:4

        LDE     LINK_LK1_DATA_16                // Word 4       UCAP_DSP1               UCAP_DSP2
        LDD     LINK_LK1_DATA_16
        STED    dpcls.mcu.pal.lk1.ucap_dsp1
        STED    dpcls.mcu.pal.pam_raw:8
        LDE     LINK_LK2_DATA_16
        LDD     LINK_LK2_DATA_16
        STED    dpcls.mcu.pal.lk2.ucap_dsp2
        STED    dpcls.mcu.pal.pam_raw:12

        LDE     LINK_LK1_DATA_16                // Word 5       UCAP_DSP3               UCAP_DSP4
        LDD     LINK_LK1_DATA_16
        STED    dpcls.mcu.pal.lk1.ucap_dsp3
        STED    dpcls.mcu.pal.pam_raw:16
        LDE     LINK_LK2_DATA_16
        LDD     LINK_LK2_DATA_16
        STED    dpcls.mcu.pal.lk2.ucap_dsp4
        STED    dpcls.mcu.pal.pam_raw:20

        LDE     LINK_LK1_DATA_16                // Word 6       UCAP_DSP5               UCAP_DSP6
        LDD     LINK_LK1_DATA_16
        STED    dpcls.mcu.pal.lk1.ucap_dsp5
        STED    dpcls.mcu.pal.pam_raw:24
        LDE     LINK_LK2_DATA_16
        LDD     LINK_LK2_DATA_16
        STED    dpcls.mcu.pal.lk2.ucap_dsp6
        STED    dpcls.mcu.pal.pam_raw:28

        LDE     LINK_LK1_DATA_16                // Word 7       BmeasRawTest            PAL/PBL Prop data
        LDD     LINK_LK1_DATA_16
        STED    dpcls.mcu.pal.lk1.bmeas_raw_test

#if TP2_ENABLE == 1
        BSET    SIM_PORTC_16,X,#0x02            // Set TP2 - this must be later than T=114us
#endif

    trigdsp:

        BSET    CPU_MODEH_16,#CPU_MODEH_C32IRQ_2_MASK8  // Trigger INT2 on DSP
        MOVW    CPU_MODE_16,mst.mode                    // Read back MODE register
#endif
#if FGC_CLASS_ID == 51

        // ----- CLASS 51 - Third reading of SAR ADCs -----

        LDD     #FGC_INTERFACE_SAR_400
        CPD     SM_INTERFACETYPE_16             // Test interface type (1=SAR 2=SD)
        BNE     wrttoc

        LDD     #0x8000                         // Prepare to toggle sign bit
        EORD    ANA_SAR_A_16                    // Read 16 bit value from ADC and convert to unsigned
        STD     dpcls.mcu.adc.chan:10           // Pass to DSP

        LDD     #0x8000                         // Prepare to toggle sign bit
        EORD    ANA_SAR_B_16                    // Read 16 bit value from ADC and convert to unsigned
        STD     dpcls.mcu.adc.chan:22           // Pass to DSP

        BSET    CPU_MODEH_16,#CPU_MODEH_C32IRQ_2_MASK8  // Trigger INT2 on DSP
        MOVW    CPU_MODE_16,mst.mode            // Read back MODE register
    wrttoc:

#endif

#if FGC_CLASS_ID != 59

        // ----- CLASS 51/53 - Arm GPT to generate analogue trigger next millisecond -----

        LDD     GPT_TOC2_16,X                   // Get time of next millisecond trigger
        STD     GPT_TOC3_16,X                   // Store in GPT_TOC3 to trigger next analogue acq
#endif

        // ----- All Classes: Reset test point -----

#if TP2_ENABLE == 1
        BCLR    SIM_PORTC_16,X,#0x02            // Reset TP2
#endif
    }

    OS_INT_EXIT();                      // Exit from interrupt through scheduler (Never returns)
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.c
\*---------------------------------------------------------------------------------------------------------*/
