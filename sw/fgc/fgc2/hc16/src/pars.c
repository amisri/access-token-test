/*---------------------------------------------------------------------------------------------------------*\
 File:          pars.c

 Purpose:       FGC MCU Software - Parameter parsing Functions.

 Author:        Quentin.King@cern.ch
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#include <pars.h>
#include <cmd.h>            // for pcm global variable, struct cmd
#include <class.h>
#include <defprops.h>
#include <dev.h>            // for dev global variable
#include <mem.h>            // for MemCpyStrFar(), MemCpyBytes()
#include <dpcom.h>          // for dpcom global variable, struct abs_time_us
#include <macros.h>         // for Test()
#include <fgc_errs.h>       // for FGC_DSP_NOT_AVL, FGC_BAD_PARAMETER
#include <os.h>             // for OSTskSuspend()
#include <prop.h>           // for PropValueGet()
#include <log.h>            // for EVT_LOG_VALUE_FULL
#include <mst.h>            // for mst global variable
#include <string.h>         // for strlen()
#include <property.h>       // for typedef prop_size_t
#include <definfo.h>        // for FGC_CLASS_ID

/*---------------------------------------------------------------------------------------------------------*/


#ifdef DEBUG
    #define DBLEN       20
    static uint16_t       dbidx;
    static struct
    {
        uint16_t  pars_idx;
        uint16_t  n_pars;
        uint16_t  out_idx;
        uint16_t  n_chars;
        uint16_t  pkt_buf_idx;
        uint16_t  n_carryover_chars;
        uint16_t  last_par_f;
        uint16_t  header;
        uint16_t  end_cmd_f;
    } db[DBLEN];
#endif

/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsScanAbsTime(struct cmd *c, struct abs_time_us **value)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to convert an AbsTime parameter from the command packet.  An AbsTime is formated
  as UNIX_TIME.US or as a double precision value formatted as "%.17e".  This must be parsed as two integers
  as there is no double precision support.  If the conversion succeeds, the value will be returned in *value.
  If no token is supplied or the value is zero then the function returns TIME.NOW instead.  The function
  returns an errnum:

        0                       AbsTime successfuly converted in *value
        FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
        FGC_NO_SYMBOL           Token buffer empty
        FGC_SYNTAX_ERROR        Token too long or contains a space before a valid delimiter
        FGC_BAD_INTEGER         Token contains non-valid characters
        FGC_INVALID_TIME        Microseconds > 999999
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              i;
    uint16_t              errnum;
    uint32_t              val;
    uint16_t              expon;
    uint16_t              n_chars;
    char *              token;                          // Pointer to character in token
    char *              c1;
    char *              c2;
    struct abs_time_us * result;

    result = (struct abs_time_us *)&c->pars_buf->results;       // Use the pars_buf results area for the value
    *value = result;                                    // Return the address of the value

    errnum = ParsScanToken(c,"Y,");
    if ( errnum )               // Get next token, delimiters: ',' and '\0'
    {
        return(errnum);
    }

    c->last_par_f = c->end_cmd_f;                       // Set last_par_f to end_cmd_f

    n_chars = c->n_token_chars;
    if ( !n_chars )                     // If no token
    {
        *result = mst.time;                                     // Set result to time now
        return(0);                                              // Return time now
    }

    token = (char *) &c->token;

    if(token[0] < '0' || token[0] > '9')                // If first character isn't a digit
    {
        return(FGC_INVALID_TIME);                               // Report bad integer
    }

    OSSemPend(pcm.sem);                                 // Reserve access to the PCM structure

    if(n_chars > 6 && token[1] == '.' && token[n_chars-4] == 'E')
    {
        if(token[n_chars-3] != '+')
        {
            OSSemPost(pcm.sem);
            return(FGC_INVALID_TIME);
        }

        token[n_chars-4] = '\0';
        pcm.end_cmd_f = false;
        pcm.prop_buf  = (struct prop_buf *)&token[n_chars-2];

        if(ParsScanInteger(&pcm, "Y", &val) || val > 9 || ((uint16_t)val + 6) > n_chars)  // delim = '\0'
        {
            OSSemPost(pcm.sem);
            return(FGC_INVALID_TIME);
        }

        expon = (uint16_t)val;

        c1 = &token[2];
        i  = n_chars - 6;

        while(i--)                              // Check all the digits are valid
        {
            if(*c1 < '0' || *c1 > '9')
            {
                OSSemPost(pcm.sem);
                return(FGC_INVALID_TIME);               // Report bad time if any digit is invalid
            }
            c1++;
        }

        c1 = &token[1];
        c2 = &token[2];
        i  = expon;

        while(i--)                              // Shift digits to move decimal point
        {
            *(c1++) = *(c2++);
        }

        *c1 = '.';                              // Add new decimal point

        if((n_chars - expon - 6) > 6)           // If more than 6 digits after decimal
        {
            token[expon + 8] = '\0';                    // Nul terminate after sixth digit
        }
    }


    pcm.end_cmd_f = false;
    pcm.prop_buf  = (struct prop_buf *)&token[0];

    errnum = ParsScanInteger(&pcm, "Y.", &val);
    if ( errnum != 0 )  // Delimiters: '.'  '\0'
    {
        OSSemPost(pcm.sem);
        return(errnum);
    }

    result->unix_time = val;                            // Prepare time now (to the second) as default
    result->us_time   = 0;

    if(pcm.token_delim == '.')
    {
        switch((errnum = ParsScanInteger(&pcm, "Y", &val)))     // Delimiters: '\0'
        {
        case 0:

            if(val > 999999)                    // If too may characters found
            {
                OSSemPost(pcm.sem);
                return(FGC_INVALID_TIME);
            }

            if(val < 10)
            {
                result->us_time = val * 100000;
            }
            else if(val < 100)
            {
                result->us_time = val * 10000;
            }
            else if(val < 1000)
            {
                result->us_time = val * 1000;
            }
            else if(val < 10000)
            {
                result->us_time = val * 100;
            }
            else if(val < 100000)
                {
                    result->us_time = val * 10;
                }
                else
                {
                    result->us_time = val;
                }

            break;

        case FGC_NO_SYMBOL:                             // If no ms specified then return seconds only

            break;

        default:                                        // Report errors

            OSSemPost(pcm.sem);
            return(errnum);
        }
    }

    if(!result->unix_time)                              // If time is zero seconds (any milliseconds)
    {
        *result = mst.time;                                     // Set result to time now
    }

    OSSemPost(pcm.sem);
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsScanChar(struct cmd *c, char **value)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to convert a Char parameter from the command packet.  Char parameters can be
  comma separated if required, or can simply be concatenated as a string.  If the scan succeeds, the
  function returns zero and the character will be returned in *value.  The calling function will know
  that the returned value is the last when the c->last_par_f is set.  The function returns an errnum:

        0                       Character successfully converted in *value
        FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
        FGC_NO_SYMBOL           Token buffer empty
        FGC_NO_DELIMITER        End of buffer found before a valid delimiter
        FGC_SYNTAX_ERROR        Token buffer full or space found before a valid delimiter
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;

    if(c->token_idx >= c->n_token_chars)        // If token buffer is empty
    {
        errnum = ParsScanToken(c,"Y,");
        if ( errnum )           // Delimiters: ',' '\0'
        {
            return(errnum);                                     // Return error
        }
    }

    *value = &c->token[c->token_idx++];         // Return address of the character in the token buffer

    if(**value == ':')                          // If colon
    {
        return(FGC_SYNTAX_ERROR);                       // Report error
    }

    if(c->token_idx >= c->n_token_chars)        // If token now empty
    {
        c->last_par_f = c->end_cmd_f;                   // Set last_par_f to end_cmd_f
    }

    if(!c->n_token_chars)                       // If new token is empty
    {
        return(FGC_NO_SYMBOL);                          // Report NO_SYMBOL
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsScanInteger(struct cmd *c, char * FAR delim, uint32_t *value)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to convert unsigned 32-bit integers from the command packet.  The delim string
  will be used in a call to ParsScanToken() to extract the next token from the packet.  If the conversion
  succeeds, the value will be returned in *value.  For speed reasons, the maximum decimal value that can be
  converted is 3999999999 even though this is less than the real maximum that can be stored in an uint32_t.
  Hex values can be specified by a leading "0X" or just "X".  The function returns an errnum:

        0                       Number successfuly converted in *value
        FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
        FGC_BAD_INTEGER         Token contains spaces or non-valid characters
        FGC_NO_DELIMITER        End of buffer found before a valid delimiter
        FGC_SYNTAX_ERROR        Token buffer full or contains a space before a valid delimiter
        FGC_NO_SYMBOL           Token buffer empty
\*---------------------------------------------------------------------------------------------------------*/
{
    char        ch;                     // Get first character from command buffer
    uint16_t      errnum;                 // Return status
    uint16_t      n_ch;                   // Number of characters in token
    char *      buf;                    // Character pointer into token buffer
    uint16_t      digit;                  // Working digit value
    uint32_t      val;                    // Unsigned working value

    if(delim)
    {
        errnum = ParsScanToken(c,delim);        // Get next token
        if ( errnum != 0 )
        {
            return(errnum);
        }
    }

    c->last_par_f = c->end_cmd_f;       // Set last_par_f to end_cmd_f

    n_ch = c->n_token_chars;
    if ( !n_ch )                        // If token contains no characters
    {
        return(FGC_NO_SYMBOL);                  // Report NO_SYMBOL
    }

    buf  = c->token;                    // Set up character pointer into token buffer
    ch   = *(buf++);                    // Extract first character
    val  = 0;                           // Clear resulting unsigned value

    while(ch == '0')                    // While character is a zero
    {
        ch = *(buf++);                          // Extract next character
        n_ch--;                                 // keep track of chars remaining
    }

    if(ch == 'X')                       // If hex value specified
    {
        if(n_ch <= 1)                           // If no characters follow the 'X'
        {
            return(FGC_BAD_INTEGER);                    // Report BAD INTEGER
        }

        do
        {
            ch = *(buf++);                              // Extract next character
            n_ch--;                                     // keep track of chars remaining
        }
        while(ch == '0');                       // While character is a zero

        if(n_ch > 8)                            // If token still has more than 8 characters
        {
            return(FGC_BAD_INTEGER);                    // Report BAD INTEGER
        }

        while(ch)                               // Loop until token buffer is empty;
        {
            digit = ParsScanHexDigit(ch);
            if ( digit == 0x10 )                // If character is not a valid hex digit
            {
                return(FGC_BAD_INTEGER);                        // Report BAD INTEGER
            }

            val = val * 16 + digit;                     // Accumulate the new digit
            ch  = *(buf++);                             // Get next character from token buffer
        }
    }
    else                                // else for Decimal number
    {
        if(n_ch > 10 ||                         // If integer has more than 10 more characters
          (n_ch == 10 && ch > '3'))             // or if it has exactly 10, but first digit is > '3'
        {
            return(FGC_BAD_INTEGER);                    // Report BAD INTEGER
        }

        while(ch)                       // Loop until token buffer is empty;
        {
            if(ch < '0' || ch > '9')            // If character isn't a valid decimal digit
            {
                return(FGC_BAD_INTEGER);                // Report BAD INTEGER
            }

            digit = ch - '0';                   // Calculate value of new digit
            val   = val * 10 + digit;           // Accumulate the new digit
            ch    = *(buf++);                   // Get next character from token buffer
        }
    }

    *value = val;                       // Return value
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsScanStd(struct cmd *c, struct prop *p, uint16_t par_type, void **value)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets the help of the DSP to parse the standard types: Integers, Floats, Points and RelTimes.
  It returns a pointer to the result value in 'value'.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              errnum;
    struct pars_buf *   pars_buf;

    if (!mst.dsp_bg_is_alive || !mst.dsp_rt_is_alive)
    {
        return(FGC_DSP_NOT_AVL);                        // Report error
    }

    pars_buf = c->pars_buf;

    if(c->pars_idx >= pars_buf->n_pars)                 // If results buffer is empty
    {
        do
        {
            if(pars_buf->out_idx >= pars_buf->pkt[c->pkt_buf_idx].n_chars)// If command packet is empty
            {
                if(c->end_cmd_f ||                                      // If command already empty
                   Test(pars_buf->pkt[c->pkt_buf_idx].header_flags,FGC_FIELDBUS_FLAGS_LAST_PKT))
                {
                    c->last_par_f = true;                                       // Report NO_SYMBOL
                    return(FGC_NO_SYMBOL);
                }

                errnum = CmdNextCmdPkt(c);
                if ( errnum )                           // If next packet restarts the cmd
                {
                    return(errnum);                                             // report error
                }
            }

            if(c->evt_log_state != EVT_LOG_VALUE_FULL)          // If log record isn't full
            {
                LogEvtSaveValue(c);                                     // Record value in record
            }

            pars_buf->pkt_buf_idx    = c->pkt_buf_idx;                 // Request DSP to parse the packet
            pars_buf->limits.integer = *(struct int_limits *)p->range; // Prepare limits (int or uint or float)
            pars_buf->type           = par_type;                       // This triggers PropParsePkt() in DSP (on FGC2)

            // On FGC2, the command parsing is done on the DSP

            // While DSP still parsing packet

            while(pars_buf->type != PACKET_TYPE_NONE)
            {
                // Resumed by MstTsk() on next ms

                OSTskSuspend();
            }

            // If DSP reported parsing error

            if(pars_buf->errnum != 0)
            {
                c->device_par_err_idx = pars_buf->par_err_idx;          // Offset error index
                return(pars_buf->errnum);                               // Return the error
            }
        }
        while(!pars_buf->n_pars);

        c->pars_idx = 0;                                        // Reset parameter index

        if(Test(pars_buf->pkt[c->pkt_buf_idx].header_flags,FGC_FIELDBUS_FLAGS_LAST_PKT)) // If last pkt
        {
            c->end_cmd_f = true;                                                // Set end of command flag
        }
    }

    if(par_type == PKT_POINT)
    {
        // Get address of next parameter in results buffer

        struct point * val = &pars_buf->results.point[c->pars_idx++];

        if(c->pars_idx >= pars_buf->n_pars)                 // If that was the last parameter of the packet
        {
            c->last_par_f = c->end_cmd_f;                           // Set last_par_f to end_cmd_f
        }

        if(*((uint32_t *)val) == PAR_NO_FLOAT)                // If DSP signals that parameter was missing
        {
            return(FGC_NO_SYMBOL);                                  // Return NO_SYMBOL
        }

        *value = (void *)val;                               // Set pointer to the value
    }
    else
    {
        // Get address of next parameter in results buffer

        uint32_t * val = &pars_buf->results.intu[c->pars_idx++];

        if(c->pars_idx >= pars_buf->n_pars)                 // If that was the last parameter of the packet
        {
            c->last_par_f = c->end_cmd_f;                           // Set last_par_f to end_cmd_f
        }

        if((par_type == PKT_FLOAT && *val == PAR_NO_FLOAT) || // If DSP signals that parameter was missing
           (par_type != PKT_FLOAT && *val == PAR_NO_INT))
        {
            return(FGC_NO_SYMBOL);                                  // Return NO_SYMBOL
        }

        *value = (void *)((uint32_t)(val+1) - prop_type_size[p->type]);       // Set pointer to the value according to type size
    }

    /*
        if(dbidx < DBLEN)
        {
            db[dbidx].pars_idx    = c->pars_idx;
            db[dbidx].n_pars      = pars_buf->n_pars;
            db[dbidx].out_idx     = pars_buf->out_idx;
            db[dbidx].n_chars     = pars_buf->pkt[c->pkt_buf_idx].n_chars;
            db[dbidx].pkt_buf_idx = c->pkt_buf_idx;
            db[dbidx].n_carryover_chars = pars_buf->pkt_buf_idx;
            db[dbidx].last_par_f  = c->last_par_f;
            db[dbidx].header      = pars_buf->pkt[c->pkt_buf_idx].header;
            db[dbidx].end_cmd_f   = c->end_cmd_f;
            dbidx++;
        }
    */

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsScanToken(struct cmd * c, char * FAR delim)
/*---------------------------------------------------------------------------------------------------------*\
  This function will extract the next token from the command buffer into the command token buffer.

  A leading space will be ignored.  The token is terminated by any of up to six delimiter characters
  in the string pointed to by delim.  The nul delimiter has special handling:

        delim[0] = 'Y'          Nul delimiter is allowed
        delim[0] = 'N'          Nul delimiter is not allowed

  delim[1] to delim[5] can supply up to five more delimiter characters.

  For example:

        delim                   Delimiter characters
        "Y"                     '\0'
        "Y "                    '\0' or ' '
        "N "                    ' '
        "Y.:([ "                '\0' or '.' or ':' or '(' or '[' or ' '

  The token length must be less than (MAX_CMD_TOKEN_LEN-1) and it may not contain spaces.

  The token will be nul terminated in the token buffer.  The length of the token is returned in the
  command structure, along with the delimiter character.

  Inputs parameters:

        struct cmd *c           Command structure containing a command buffer and token buffer

        int8_t *delim            Delimiter character specifier (see above)

  Output parameters:

        c->token[]              Nul terminated token string

        c->token_delim          Delimiter character found

        c->n_token_chars        Token length

  Return value:

        0                       Token successfully extracted from command buffer into token buffer
        FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
        FGC_NO_DELIMITER        End of buffer found before a valid delimiter
        FGC_SYNTAX_ERROR        Token buffer full or space found before a valid delimiter
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              errnum;         // Error number
    char                ch;             // Working character from command buffer
    char *              buf;            // Pointer into token buffer
    uint16_t              n_ch;           // Number of characters transfered
    char                ldelim[8];      // Local copy of FAR string delimiter

    errnum = CmdNextCh(c,&ch);

    if ( errnum != 0 )  // If next character aborts the command
    {
        return(errnum);                         // Return error
    }

    if ( ch == ' ' )                    // If leading space - skip and take another character
    {
        errnum = CmdNextCh(c,&ch);
        if ( errnum != 0 )              // If next character aborts the command
        {
            return(errnum);                             // Return error
        }
    }

    ldelim[2] = '\a';                   // Pre pad local delimiter with "impossible" \a character
    ldelim[3] = '\a';
    ldelim[4] = '\a';
    ldelim[5] = '\a';

    ldelim[MemCpyStrFar(ldelim, delim)] = '\a';  // Replace nul from string copy with \a

    buf  = c->token;                    // Set up character pointer into destination token buffer
    n_ch = 0;

    while(n_ch < MAX_CMD_TOKEN_LEN)     // While token buffer isn't full
    {
        LogEvtSaveCh(c,ch);             // Log character

        if((!ch && ldelim[0] == 'Y') || // If character is nul and nul delimiter is allowed , or
           ch == ldelim[1] ||           // character matches any of the other five possible
           ch == ldelim[2] ||           // delimiter characters
           ch == ldelim[3] ||
           ch == ldelim[4] ||
           ch == ldelim[5])
        {
            *buf             = '\0';            // Nul terminate token
            c->token_delim   = ch;              // Report the delimiter character
            c->n_token_chars = n_ch;            // Report token length
            c->token_idx     = 0;               // Reset token index to start of token buffer
            return(0);                          // Return success
        }

        if(!ch)                                 // Buffer empty unexpectedly
        {
            return(FGC_NO_DELIMITER);                   // Report NO DELIMITER
        }

        if(ch == ' ')                           // Character is a space (and space is not a valid delimiter)
        {
            break;                                      // Break to return SYNTAX_ERROR
        }

        *(buf++) = ch;                          // Put character into token buffer
        n_ch++;                                 // Increment token length counter

        errnum = CmdNextCh(c,&ch);
        if ( errnum != 0 )              // If next character aborts the command
        {
            return(errnum);                             // Return error
        }
    }

    return(FGC_SYNTAX_ERROR);                   // Buffer overrun - report SYNTAX ERROR
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsScanSymList(struct cmd *c, char * FAR delim, const struct sym_lst * sym_lst, struct sym_lst **sym_const)
/*---------------------------------------------------------------------------------------------------------*\
  This function will extract a token and will see if it is a settable symbol from the specified symbol list.

  Inputs parameters:

        struct cmd *c           Command structure containing the token buffer

        int8_t *delim            This will be used as the delimiter string for ParsScanToken()

        struct sym_lst *sym_lst Pointer to symbol list


  Output parameters:

        struct sym_lst *sym_const  If no symbol is given then *sym_const will be zero.  If a symbol
                                   matches one in the symbol list, then a pointer to the symbol list
                                   record is returned - this contains the sym_idx and constant value.

  Note:                            The sym_idx in the sym_lst will have FGC_NOT_SETTABLE (0x8000) set if
                                   the symbol may not be set.

  Return value:

        0                               Successful match returned in *sym
        FGC_NO_SYMBOL                   No token found
        FGC_CMD_RESTARTED               Unexpected first-command packet received (new command starting)
        FGC_NO_DELIMITER                End of buffer found before a valid delimiter
        FGC_SYNTAX_ERROR                Token buffer full or contains a space before a valid delimiter
        FGC_UNKNOWN_SYM                 Token didn't match any symbol with given type
        FGC_BAD_PARAMETER               Constant symbol identified but not in symlist
        FGC_SYMBOL_IS_NOT_SETTABLE      Symbol is identified and present in the symlist, but is not settable
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;                 // Return status

    errnum = ParsScanSymListAndNone(c, delim, sym_lst, sym_const);

    if(errnum == FGC_GENERIC_NONE_SYMBOL)       // This error message should stay internal to the FGC, so
                                                // replace it by a more generic "BAD PARAMETER" message.
    {
        errnum = FGC_BAD_PARAMETER;
    }

    return errnum;
}
#pragma MESSAGE DISABLE C5909 // Disable 'assignment in codition' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsScanSymListAndNone(struct cmd *c, char * FAR delim, struct sym_lst * sym_lst, struct sym_lst **sym_const)
/*---------------------------------------------------------------------------------------------------------*\
  See the documentation for function ParsScanSymList() above.

  This function has the same functionality except it adds one more possible error status: FGC_GENERIC_NONE_SYMBOL.
  That additional error is used for the purpose of the generic "NONE" symbol available by default on all BIT_MASK
  properties. That symbol allows to reset the bit mask. E.g. S TEST.BIT_MASK NONE
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;                 // Return status
    uint16_t      sl_idx;                 // Symbol index in symbol list (FGC_NOT_SETTABLE bit may be set)
    uint16_t      sym_idx;                // Identified symbol index
    bool        not_settable_f;         // Flag indicating that the symbol was found, but cannot be (FGC_NOT_SETTABLE)

    errnum = PropIdentifySymbol(c, delim, ST_CONST, &sym_idx);  // Identify Constant Symbol

    c->last_par_f = c->end_cmd_f;       // Set last_par_f to end_cmd_f

    if ( errnum != 0 )                          // If Symbol not identified
    {
        return(errnum);                         // Report error status
    }

    not_settable_f = false;

    while((!not_settable_f) && (sl_idx = sym_lst->sym_idx) && (sl_idx != sym_idx))  // Scan supplied sym_lst for a match
    {                                                                               // Non-settable symbols will NEVER
                                                                                    // match because the FGC_NOT_SETTABLE
                                                                                    // bit is set in sym_idx in the sym_lst
        not_settable_f = (sl_idx == (sym_idx | FGC_NOT_SETTABLE));

        sym_lst++;
    }

    // If the symbol was found but ORed with mask FGC_NOT_SETTABLE, then return the "symbol is not settable" error.

    if(not_settable_f == true)
    {
        return(FGC_SYMBOL_IS_NOT_SETTABLE);
    }

    // Else, if no match found (sl_idx == NULL)

    else if(!sl_idx)
    {
        // Generic NONE symbol. The NONE symbol is recognised by default for all bit mask properties. Note that if
        // STC_NONE or (STC_NONE|FGC_NOT_SETTABLE) is present in the sym_lst, this is not the generic NONE symbol but
        // a symbol explicitly defined in the property symbol list.

        if(sym_idx == STC_NONE)
        {
            return(FGC_GENERIC_NONE_SYMBOL);    // If the property is a bit mask, that error gets caught by the FGC SW.
        }

        // In any other case, report BAD PARAMETER

        else
        {
            return(FGC_BAD_PARAMETER);
        }
    }

    *sym_const = sym_lst;               // Pass pointer to sym_lst record to calling function
    return(0);                          // Return success
}
#pragma MESSAGE DEFAULT C5909
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsSet(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the standard method for setting a property from ASCII values in the command buffer.
  The following types are supported: AbsTime, RelTime, Float, Point and all integers including properties
  with symlists.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;
    prop_size_t el_idx;
    uint16_t      el_size;
    prop_size_t n_els_old;
    uint16_t      max_pars;
    bool        set_f;
    uint8_t *     cur_val;                        // Pointer to current element value
    void *      new_val;                        // Pointer to new element value

    /*--- Prepare array range ---*/

    el_idx = c->from;

    errnum = PropValueGet(c, p, NULL, el_idx, &cur_val);        // Get first property block into cache and n_elements
    if(errnum)
    {
        return(errnum);
    }

    n_els_old = c->n_elements;

    max_pars  = c->to - el_idx + 1;             // Calculate number of elements in array range
    el_size   = prop_type_size[p->type];        // Get element size for this property
    set_f     = false;                          // Clear parameter set flag

    if(p->type == PT_CHAR &&                    // If CHAR type and
       el_idx > n_els_old)                      // array start is beyond the end of the existing data
    {
        return(FGC_BAD_ARRAY_IDX);                      // Report bad array index
    }

    // Parse command packets

    while(!c->last_par_f && c->n_pars < max_pars)
    {
        errnum  = PropValueGet(c, p, &set_f, el_idx, &cur_val); // Get existing value from prop (as default)
        if(errnum)
        {
            return(errnum);
        }

        errnum  = ParsValueGet(c, p, &new_val);                 // Get new value from command packet

        switch(errnum)
        {
        case 0:

            if(MemCmpBytes(cur_val, new_val, el_size))
            {
                MemCpyBytes(cur_val, new_val, el_size);

                c->changed_f = true;
                set_f        = true;
            }

            c->n_pars++;
            el_idx++;
            break;

        case FGC_NO_SYMBOL:             // TOKEN is empty (i.e. blank between two commas or after last comma)

            if(p->type == PT_CHAR)              // If CHAR type
            {
                if(c->n_pars)                           // If at least one character was provided
                {
                    return(FGC_NO_SYMBOL);                      // Report NO_SYMBOL
                }
            }

            if(el_idx || !c->last_par_f)        // If not first element or not last parameter
            {
                c->n_pars++;                            // Don't change existing value for this element
                el_idx++;
            }
            break;

        default:

            c->device_par_err_idx += c->n_pars;
            return(errnum);
        }
    }

    /*--- Check number of parameters set and adjust property length ---*/

    if(!c->last_par_f && c->n_pars >= max_pars)
    {
        c->device_par_err_idx += c->n_pars;
        return(FGC_BAD_PARAMETER);
    }

    // Keep the original data length if it has been reduced and the property 
    // cannot shrink, is a scalar or the complete range has been supplied

    if (    el_idx < n_els_old 
        && (   Test(p->flags, PF_DONT_SHRINK)
            || p->max_elements == 1
            || c->n_arr_spec   == 2))
    {
        el_idx = n_els_old;
    }

    if(el_idx != n_els_old)
    {
        c->changed_f = true;
    }

    return(PropBlkSet(c, el_idx, set_f, FGC_FIELDBUS_FLAGS_LAST_PKT)); // Write back zeroed value(s)

}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsValueGet(struct cmd *c, struct prop *p, void ** value)
/*---------------------------------------------------------------------------------------------------------*\
  This function will return a pointer to the next parameter parsed from the command packet.  For integers,
  floats, points and reltimes, the MCU gets help from the DSP to parse all the parameters in the packet.  For
  chars, symlists and abstime, it does the conversion itself.  If the current packet is empty, the function
  will sleep on the semaphore from the fieldbus or terminal task until a new packet is available.  The function
  will return the pointer to the value in *value and an errnum:

        0                       Valid value is waiting at *value
        FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
        FGC_NO_SYMBOL           Token buffer empty (c->cmd_end_f set if no more tokens in command)
        FGC_SYNTAX_ERROR        Token too long or contains a space before a valid delimiter
        FGC_BAD_INTEGER         Token contains non-valid integer characters
        FGC_BAD_FLOAT           Token contains non-valid floating point characters
        FGC_INVALID_TIME        AbsTime Milliseconds > 999
        FGC_UNKNOWN_SYM         Token didn't match any symbol with given type
        FGC_BAD_PARAMETER       Constant symbol identified but not in symlist
        FGC_OUT_OF_LIMITS       Value is out of min/max limits for the property
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              par_type;

    switch(p->get_func_idx)
    {
    case GET_ABSTIME:                           // AbsTime

        return(ParsScanAbsTime(c, (struct abs_time_us **)value));

    case GET_CHAR:                              // Char

        return(ParsScanChar(c, (char **)value));


#if FGC_CLASS_ID == 59
    case GET_RELTIME:                           // Reltime

        par_type = PKT_RELTIME;
        break;
#endif

    case GET_FLOAT:                             // Float

        par_type = PKT_FLOAT;
        break;

    case GET_POINT:                             // Point

        par_type = PKT_POINT;
        break;

    default:                                    // Integer

        if(Test(p->flags,PF_SYM_LST))                   // If symlist
        {
            return(ParsValueGetSymList(c, p, value));           // Return symlist value
        }

        par_type = (p->type == PT_INT32U ? PKT_INTU : PKT_INT);
        break;
    }

    return(ParsScanStd(c, p, par_type, value)); // Standard type (Integer, Float, RelTime)
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ParsValueGetSymList(struct cmd *c, struct prop *p, void ** value)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from ParsValueGet() if a symlist value is required.  If it is a bit mask symlist
  then multiple symbols can be entered separated by | characters.
\*---------------------------------------------------------------------------------------------------------*/
{
    bool                bitmask_f;
    uint16_t              errnum;
    uint16_t *            results;
    struct sym_lst *    sym_const;

    bitmask_f  = Test(p->flags,PF_BIT_MASK);
    results    = (uint16_t *)&c->pars_buf->results;       // Use the pars_buf results area for the value
    results[0] = 0;
    results[1] = 0;
    *value     = (uint8_t *)&c->pars_buf->results + (4 - prop_type_size[p->type]);

    do
    {
        errnum = ParsScanSymListAndNone(c, (bitmask_f ? "Y|," : "Y,"), p->range, &sym_const);

        // Generic "NONE" symbol for bit masks.
        // If the property is a bit mask, and the symbol NONE is found in the command but that symbol is not explicitly
        // mentioned in the symbol list for that property, then the default interpretation is to reset the bit mask.

        if (bitmask_f && errnum == FGC_GENERIC_NONE_SYMBOL)
        {
            results[1] = 0;     // Reset the property value
            errnum = 0;         // Ignore the error
        }
        else if(errnum == FGC_GENERIC_NONE_SYMBOL)
        {
            errnum = FGC_BAD_PARAMETER;         // Replace internal error GENERIC NONE SYMBOL by user error BAD PARAMETER
        }
        else if ( !errnum )
        {
            results[1] |= sym_const->value;
        }
    }
    while(!errnum && bitmask_f && c->token_delim == '|');

    return(errnum);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pars.c
\*---------------------------------------------------------------------------------------------------------*/
