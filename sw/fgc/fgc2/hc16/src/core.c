/*---------------------------------------------------------------------------------------------------------*\
  File:     core.c

  Purpose:  Core dump related functions

  Notes:    The core dump system allows up to FGC_N_CORE_SEGS segments of memory to be saved to
        NVRAM in the event of an unexpected trap (e.g. bus error or illegal instruction).
        A core control table in NVRAM defines the memory segments to be saved.
\*---------------------------------------------------------------------------------------------------------*/

#define CORE_GLOBALS            // define core global variable

#include <stdint.h>

#include <core.h>
#include <memmap_mcu.h>         // Include memory map consts
#include <iodefines.h>          // specific processor registers and constants
#include <mem.h>                // for MemCpyWords(), MemSetBytes(), MemSetWords()
#include <mcu_dependent.h>      // for NVRAM_UNLOCK(), NVRAM_LOCK(), ENTER_SR()
#include <definfo.h>            // for FGC_PLATFORM_ID


/*---------------------------------------------------------------------------------------------------------*/
void CoreDump(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will save the memory segments specified in the core control table to NVRAM.  If the table
  is corrupted, it will be overwritten with the default table first.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                  ii;
    struct fgc_corectrl     corectrl;

    CoreTableGet(&corectrl);                // Get Core control table from NVRAM

    NVRAM_UNLOCK();
    ENTER_SR();

    for ( ii = 0; ii < FGC_N_CORE_SEGS; ii++ )      // Write each memory segment to NVRAM
    {
        if ( corectrl.seg[ii].n_words )
        {
            MemCpyWords(corectrl.addr_in_core[ii], corectrl.seg[ii].addr, corectrl.seg[ii].n_words);
        }
    }

    EXIT_SR();
    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void CoreTableGet(struct fgc_corectrl * corectrl)
/*---------------------------------------------------------------------------------------------------------*\
  This function will retrieve the core control table from NVRAM to *corectrl.  It will check that the table
  is valid - if it is the function returns with the table in *corectrl.  If it finds that the table is
  not valid, it writes the default table into *corectrl and into NVRAM and clears the Core data.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  ii;
    uint32_t  total_length_words = 0;

    ENTER_SR();
    MemCpyWords( (uint32_t) corectrl,         // Get CORE control table from NVRAM
                  NVRAM_CORECTRL_32,
                  (sizeof (struct fgc_corectrl) ) / 2 );
    EXIT_SR();

    for( ii = 0; ii < FGC_N_CORE_SEGS; ii++ )           // Check that table has valid lengths
    {
        if (    ( corectrl->seg[ii].pad != 0xFFFF )
             || (     corectrl->seg[ii].n_words
                  && (    corectrl->addr_in_core[ii] <  NVRAM_COREDATA_32
                       || corectrl->addr_in_core[ii] > (NVRAM_COREDATA_32 + 2 * NVRAM_COREDATA_W)
                     )
                )
           )
        {
            break;                  // Terminate loop
        }

        total_length_words += corectrl->seg[ii].n_words;
    }

    if ( ii < FGC_N_CORE_SEGS ||              // If core ctrl table is not valid
         total_length_words > NVRAM_COREDATA_W )
    {
        MemSetBytes(corectrl, 0, sizeof (struct fgc_corectrl)); // Clear table

        corectrl->seg[0].n_words  = NVRAM_COREDATA_W;   // Set up default table
        corectrl->seg[0].addr     = SM_16;      // with one max length segment from
        corectrl->seg[1].addr     = SM_16;      // start of SRAM in page 0
        corectrl->seg[2].addr     = SM_16;
        corectrl->seg[3].addr     = SM_16;
        corectrl->seg[0].pad      = 0xFFFF;             // Padding word
        corectrl->seg[1].pad      = 0xFFFF;             // Padding word
        corectrl->seg[2].pad      = 0xFFFF;             // Padding word
        corectrl->seg[3].pad      = 0xFFFF;             // Padding word

        corectrl->addr_in_core[0] = NVRAM_COREDATA_32;

        CoreTableSet(corectrl);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CoreTableSet(struct fgc_corectrl *corectrl)
/*---------------------------------------------------------------------------------------------------------*\
  This function will write the corectrl table to the NVRAM and clear the CORE data area.
\*---------------------------------------------------------------------------------------------------------*/
{
    NVRAM_UNLOCK();
    ENTER_SR();
    MemCpyWords( NVRAM_CORECTRL_32,              // Set CORE control table in NVRAM
                 (uint32_t)corectrl,
                 (sizeof (struct fgc_corectrl)) / 2 );
    MemSetWords(NVRAM_COREDATA_32, 0, NVRAM_COREDATA_W);        // Clear CORE data area
    EXIT_SR();
    NVRAM_LOCK();
}

// EOF
