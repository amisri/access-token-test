/*---------------------------------------------------------------------------------------------------------*\
  File:     reg.c

  Purpose:  FGC2 Register access functions

  Author:   Quentin.King@cern.ch

  Notes:    These functions provided protected access to peripheral registers.  If the access results
        in a bus error, the functions can make an entry in the runlog if required.  They
        return 1 but otherwise, operation is not disturbed.
\*---------------------------------------------------------------------------------------------------------*/

#include <registers.h>

#include <stdbool.h>
#include <memmap_mcu.h>         // Include memory map constants
#include <fgc_runlog_entries.h> // Include run log definition, for FGC_RL_
#include <runlog_lib.h>         // for RunlogWrite
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <trap.h>               // for global variable buserr_chk

static  uint32_t  reg_addr;

/*---------------------------------------------------------------------------------------------------------*/
uint16_t RegTest(uint32_t address, uint16_t data, uint16_t mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function will attempt to write the data word to the specified address. It copies what is in this place
  write a data reads it back and copies the initial data back in its place.
  If the read back fails, it returns 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  save_reg;
    uint16_t  stat = 1;                   // Fail

    OS_ENTER_CRITICAL();

    save_reg = *(uint16_t*)address;       // Save initial data
    *(uint16_t*)address = data;           // Write data

    *((uint16_t*)RXCODEBUF_16) = ~data;           // Dummy write to inverse data bus

    if(((*(uint16_t*)address) & mask) != data)        // Read back with maskfails
    {
        RunlogWrite(FGC_RL_REG_ERR,&address);
    }
    else
    {
        stat = 0;                       // success
    }

    *(uint16_t*)address = save_reg;       // Write initial data in reg

    OS_EXIT_CRITICAL();
    return(stat);
}
#pragma MESSAGE DISABLE C1404 // Disable 'return expected' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RegWrite(uint32_t address, uint16_t data, bool runlog_f)
/*---------------------------------------------------------------------------------------------------------*\
  This function will attempt to write the data word to the specified address.  It will enable
  bus error checking and if the write is successful, the funtion returns 0.  If a bus error
  occurs, it will write a record to the run log if runlog_f is set and will return 1.  Interrupts
  are disabled during the operation to prevent conflicts between RegWrite() and RegRead().
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_ENTER_CRITICAL();

    reg_addr = address;         // Transfer address from stack to fixed location

    asm
    {
        STZ     buserr_chk      // Store Z (must not be zero) in bus error check flag
        PSHM    K               // Save page registers
        LDAB    reg_addr:1      // Set XK to page containing register
        TBXK
        LDX     reg_addr:2      // Set X = register address within page
        LDD     data            // D = data to write into register
        STD     0,X             // Try to write data to the address
        PULM    K               // Restore page register

        TSTW    buserr_chk      // Check if bus error happened (flag cleared)
        BEQ buserr              // Branch if so

        CLRW    buserr_chk      // Clear flag to disable bus error check
        CLRD                    // Clear D to return zero
        BRA end

    buserr:

        TSTW    runlog_f        // Test runlog_f parameter
        BEQ returnone           // Skip runlog entry if flag is zero

        LDAB    #FGC_RL_BUS_ERR_W       // RunlogWrite(FGC_RL_BUS_ERR_W,&reg_addr)
        LDY     @reg_addr
        PSHM    D
        JSR RunlogWrite
        AIS #2

    returnone:
        LDD     #0x0001         // Return 1 to indicate error occurred
    end:
    }

    OS_EXIT_CRITICAL();
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RegRead(uint32_t address, uint16_t * data, bool runlog_f)
/*---------------------------------------------------------------------------------------------------------*\
  This function will attempt to read a word from the specified address.  It will enable
  bus error checking in advance.  If the read is successful, the function will transfer the value to
  *data returns 0.  If a bus error occurs, it will write a line to the run log if runlog_f is set
  and will return 1.  Interrupts are disabled during the operation to prevent conflicts between RegWrite()
  and RegRead().
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_ENTER_CRITICAL();

    reg_addr = address;             // Transfer address from stack to fixed location

    asm
    {
        STZ     buserr_chk          // Store Z (must not be zero) in bus error check flag
        PSHM    K                   // Save page registers
        LDAB    reg_addr:1          // Set XK to page containing register
        TBXK
        LDX     reg_addr:2          // Set X = register address within page
        LDD     0,X                 // Try to read a word from the address
        PULM    K                   // Restore page register

        TSTW    buserr_chk          // Check if bus error happened (flag cleared)
        BEQ buserr                  // Branch if so

        CLRW    buserr_chk          // Clear flag to disable bus error check
        LDX     data                // Get pointer to data
        STD     0,X                 // Store word in *data
        CLRD                        // Clear D to return zero
        BRA end

    buserr:

        TSTW    runlog_f            // Test runlog_f
        BEQ returnone               // Skip runlog entry if flag is zero

        LDAB    #FGC_RL_BUS_ERR_R   // RunlogWrite(FGC_RL_BUS_ERR_R,&reg_addr)
        LDY     @reg_addr
        PSHM    D
        JSR RunlogWrite
        AIS #2

    returnone:
        LDD     #0x0001             // Return 1 to indicate error occurred
    end:
    }

    OS_EXIT_CRITICAL();
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: reg.c
\*---------------------------------------------------------------------------------------------------------*/
