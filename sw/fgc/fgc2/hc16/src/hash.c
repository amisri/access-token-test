/*---------------------------------------------------------------------------------------------------------*\
  File:         hash.c

  Purpose:      FGC MCU Software - Hash Functions.

  Author:       Quentin.King@cern.ch

  Notes:        This file contains the functions to set up and search hash tables of FGC symbols
                There are three seperate hash tables: Property symbols, constant symbols and
                get option symbols
\*---------------------------------------------------------------------------------------------------------*/

#include <hash.h>               // for HASH_VALUE and struct hash_entry needed by defprops.h
#include <cmd.h>
#include <class.h>              // Include class dependent definitions
#include <property.h>           // defprops.h needs DEFPROPS_FUNC_SET(), and for ST_MAX_SYM_LEN
#include <defprops.h>           // for PROP_HASH_SIZE, CONST_HASH_SIZE
#include <fgc_parser_consts.h>  // for GETOPT_HASH_SIZE
#include <mem.h>                // for MemCpyStrFar(), MemSetWords(), MemCpyBytes()
#include <string.h>             // for strcmp(), strlen()
#include <start.h>


// Hash table arrays

static struct hash_entry *     prop_hash_table  [PROP_HASH_SIZE];
static struct hash_entry *     const_hash_table [CONST_HASH_SIZE];
static struct hash_entry *     getopt_hash_table[GETOPT_HASH_SIZE];

static char dims_symbols [FGC_MAX_DIMS][ST_MAX_SYM_LEN];

// Constants

#define KEY_LEN_W             ((ST_MAX_SYM_LEN+1)/2)

#pragma MESSAGE DISABLE C1404 // Disable 'return expected' warning
/*---------------------------------------------------------------------------------------------------------*/
static uint16_t HashSymbol(uint16_t size, char *key)
/*---------------------------------------------------------------------------------------------------------*\
  This function will generate a hash index from the symbol.  This is the result of adding all the words
  of the symbol (with rollover) and then taking the modulus with the size of the hash table.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDY     key
        LDD     0,Y                     // Sum all words of key
        ADDD    2,Y
        ADDD    4,Y
        ADDD    6,Y
        ADDD    8,Y
        ADDD    10,Y
        ADDD    12,Y                    // D = SUM(key)
        LDX     size                    // X = size
        IDIV                            // D = SUM(key) % size
    }                                   // Return hash index in D
}
#pragma MESSAGE DEFAULT C1404
/*---------------------------------------------------------------------------------------------------------*/
static void HashInsert(struct hash_entry **table, struct hash_entry *entry, uint16_t size)
/*---------------------------------------------------------------------------------------------------------*\
  This function will add the entry to the hash table.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              index;
    struct hash_entry * node;
    char                key[ST_MAX_SYM_LEN+1];

    // Clear key array

    asm
    {
        CLRW    key:0
        CLRW    key:2
        CLRW    key:4
        CLRW    key:6
        CLRW    key:8
        CLRW    key:10
        CLRW    key:12
    }

    // Transfer far symbol to near key and check the length does not exceed the space on the stack

    if(MemCpyStrFar(key, entry->key.c) > ST_MAX_SYM_LEN)
    {
        Crash(0xBADD);
    }

    // Get root node in hash table for this key

    index = HashSymbol(size, key);
    node  = table[index];

    // Add hash entry into hash table

    if ( !node )
    {
        table[index] = entry;           // Add first hash entry for this node index
    }
    else
    {
        while(node->next)               // node index in use so add entry to end of the chain
        {
            node = node->next;
        }

        node->next = entry;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
static uint16_t HashFind(struct hash_entry **table, uint16_t size, char * sym)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search the hash table for an entry with the near symbol sym.  It returns zero if no
  match found, or the symbol index if successful.  The symbol can be assumed to be null terminated and no
  longer than ST_MAX_SYM_LEN characters in length, but not nul padded.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                      index;
    struct hash_entry *         node;
    char                        key[ST_MAX_SYM_LEN+1];

    char *                      key_p;
    char * FAR                  farsym;

    // Clear key array

    asm
    {
        CLRW    key:0
        CLRW    key:2
        CLRW    key:4
        CLRW    key:6
        CLRW    key:8
        CLRW    key:10
        CLRW    key:12
    }

    MemCpyStrFar(key, sym);

    // Look up hash table node for this symbol and return zero if node is empty

    node = table[ HashSymbol(size, key) ];

    if ( !node )
    {
        return(0);
    }

    // Search chain of nodes for a match between the symbol and the hash table entries

    while ( node != 0 )
    {
        index = node->sym_idx;

        key_p  = key;
        farsym = node->key.c;

        asm
        {
                PSHM    K
                LDY     farsym:1
                LDAB    farsym:0
                TBYK
                LDX     key_p

                LDD     0,Y
                CPD     0,X
                BNE     nomatch
                TSTB
                BEQ     match

                AIX     #2
                LDD     2,Y
                TSTA
                BEQ     checklast
                CPD     0,X
                BNE     nomatch
                TSTB
                BEQ     match

                AIX     #2
                LDD     4,Y
                TSTA
                BEQ     checklast
                CPD     0,X
                BNE     nomatch
                TSTB
                BEQ     match

                AIX     #2
                LDD     6,Y
                TSTA
                BEQ     checklast
                CPD     0,X
                BNE     nomatch
                TSTB
                BEQ     match

                AIX     #2
                LDD     8,Y
                TSTA
                BEQ     checklast
                CPD     0,X
                BNE     nomatch
                TSTB
                BEQ     match

                AIX     #2
                LDD     10,Y
                TSTA
                BEQ     checklast
                CPD     0,X
                BNE     nomatch
                TSTB
                BEQ     match

                AIX     #2
                LDD     12,Y
                TSTA
                BEQ     checklast
                CPD     0,X
                BNE     nomatch

            match:
                PULM    K
                BRA     ret
            checklast:
                TST     0,X
                BEQ     match
            nomatch:
                PULM    K
        }
        node = node->next;
    }

    index = 0;                                          // No match - return zero

    asm
    {
        ret:
    }

    return(index);
}

uint8_t HashInitDIM(char * key, uint16_t n_dims)
{
    if(HashFindProp(key)!=0)
    {
        return 0;
    }

    strncpy(dims_symbols[n_dims], key, ST_MAX_SYM_LEN);
    SYM_TAB_PROP[N_PROP_SYMBOLS + n_dims].key.c = dims_symbols[n_dims];

    HashInsert(prop_hash_table, &SYM_TAB_PROP[N_PROP_SYMBOLS + n_dims], PROP_HASH_SIZE);
    return 1;
}

/*---------------------------------------------------------------------------------------------------------*/
void HashInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will initialise the hash tables for the three types of FGC symbols: property names,
  constants and get options.  The symbols are defined in defprops.h - the first symbol in each table is
  not used so that a zero symbol index can indicate no symbol match.  The length of the tables is defined
  by the parser to be the next prime number up from the number of symbols in the table.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      ii;

    for ( ii = 1; ii < N_PROP_SYMBOLS; ii++ )
    {
        HashInsert(prop_hash_table, &SYM_TAB_PROP[ii], PROP_HASH_SIZE);
    }

    for ( ii = 1; ii < N_CONST_SYMBOLS; ii++ )
    {
        HashInsert(const_hash_table, &SYM_TAB_CONST[ii], CONST_HASH_SIZE);
    }

    for ( ii = 1; ii < N_GETOPT_SYMBOLS; ii++ )
    {
        HashInsert(getopt_hash_table, &sym_tab_getopt[ii], GETOPT_HASH_SIZE);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t HashFindProp(char * prop_symbol)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search the property hash table for the property symbol
\*---------------------------------------------------------------------------------------------------------*/
{
    return(HashFind(prop_hash_table, PROP_HASH_SIZE, prop_symbol));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t HashFindConst(char * const_symbol)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search the constant hash table for the constant symbol
\*---------------------------------------------------------------------------------------------------------*/
{
    return(HashFind(const_hash_table, CONST_HASH_SIZE, const_symbol));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t HashFindGetopt(char * getopt_symbol)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search the get options hash table for the get option symbol
\*---------------------------------------------------------------------------------------------------------*/
{
    return(HashFind(getopt_hash_table, GETOPT_HASH_SIZE, getopt_symbol));
}
// EOF
