/*---------------------------------------------------------------------------------------------------------*\
 File:      get.c

 Purpose:   FGC MCU Software - Get Command Functions

 Author:    Quentin.King@cern.ch

 Notes:     This file contains the function for the Get Task and all the property Get functions.
\*---------------------------------------------------------------------------------------------------------*/

#define GET_GLOBALS         // define debug global variable
#define DEFPROPS_INC_ALL    // defprops.h

#include <get.h>

#include <class.h>          // Include class dependent definitions
#include <cmd.h>            // for struct cmd
#include <core.h>           // for CoreTableGet(), struct fgc_corectrl & fgc_coreseg
#include <microlan_1wire.h> // for struct TBranchSummary, struct TMicroLAN_element_summary
#include <m16c62_link.h>    // for MICROLAN_UNIQUE_SERIAL_CODE_LEN
#include <definfo.h>        // for FGC_CLASS_ID, FGC_PLATFORM_ID
#include <defprops.h>       // Include property related definitions
#include <dev.h>            // for dev global variable,
#include <diag.h>           // for diag global variable
#include <DIMsDataProcess.h>// for DimGetDimLog()
#include <dls.h>            // for dls, id  global variable
#include <dpcls.h>          // for dpcls global variable
#include <dpcom.h>          // for dpcom global variable, struct abs_time_us
#include <fbs.h>            // for fbs global variable, FbsOutLong(), FbsOutBuf()
#include <fgc/fgc_db.h>     // for access SysDB and CompsDB
#include <fgc_errs.h>       // for FGC_BAD_ARRAY_IDX
#include <fgc_log.h>        // for struct fgc_log_header, struct fgc_log_sig_header
#include <log.h>            // for LOG_MAX_SIGS, struct log_ana_vars
#include <log_class.h>      // for timing_log, log_iab global variables
#include <log_event.h>      // for struct log_evt_rec
#include <macros.h>         // for Test(), Set(), Clr()
#include <mcu_dependent.h>  // for ENTER_BB(), EXIT_BB(), ENTER_SR(), EXIT_SR
#include <mem.h>            // for MemCpyWords()
#include <nvs.h>
#include <prop.h>           // for PropValueGet()
#include <property.h>       // for struct prop and typedef prop_size_t
#include <registers.h>      // for RegRead()
#include <start.h>          // for Crash()
#include <stdio.h>          // for fputc(), fprintf(), fputs()
#include <memmap_mcu.h>     // for SM_UXTIME_P
#include <mcu_dsp_common.h>
#include <shared_memory.h>  // for shared_mem global variable

#define SPY_GLOBALS
#define SPY_FAR
#include <spy_v2.h>
#undef SPY_GLOBALS
#undef SPY_FAR



#if (FGC_CLASS_ID != 59)
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetFifo(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: Fifo data for ADC.
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t                 n;
    uint16_t                      nn_f;
    uint16_t                      mask;
    uint16_t                      out_idx;
    uint16_t                      errnum;
    struct dpcls_fifo_head   *  fifo;
    static char * FAR           fmt0[] = { "%10lu.%06u %9ld %9ld %7lu %7lu",    "%lu.%06u %ld %ld %lu %lu"  };
    //  static char * FAR           fmt1[] = { "%02u %10lu.%03u %13.6E",        "%u %lu.%03u %.6E"      };


    fifo = (struct dpcls_fifo_head *)p->value;

    out_idx = (uint16_t) fifo->out_idx;
    mask    = p->max_elements - 1;

    p->n_elements = ((uint16_t) fifo->in_idx - out_idx) & mask;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    if (c->from)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    nn_f = (c->token_delim == ','); // Not neat flag

    while (n--)             // For all elements in the array range
    {
        errnum = CmdPrintIdx(c, c->from++);

        if(errnum != 0)       // Print index if required
        {
            return(errnum);
        }

        fprintf(c->f, fmt0[nn_f], dpcls.dsp.fifo.adc_time [out_idx].unix_time,
                dpcls.dsp.fifo.adc_time [out_idx].us_time,
                dpcls.dsp.fifo.adc_raw_a[out_idx],
                dpcls.dsp.fifo.adc_raw_b[out_idx],
                dpcls.dsp.fifo.adc_pp_a [out_idx],
                dpcls.dsp.fifo.adc_pp_b [out_idx]);
        out_idx = (out_idx + 1) & mask;
    }

    fifo->out_idx = out_idx;
    p->n_elements = p->max_elements;

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetLogCycles(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: Timing log property
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t         n;
    uint16_t              out_idx;
    int8_t               user;
    uint16_t              errnum;
    bool                is_start_super_cycle;
    struct sym_lst   *  dysfunction_symbol_list;
    char * FAR          dysfunction_label;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    out_idx = (timing_log.in_idx + 1 + c->from) % FGC_LOG_CYCLES_LEN;

    while (n--)             // For all elements in the array range
    {
        errnum = CmdPrintIdx(c, c->from);      // Print index if required

        c->from++;

        if (errnum)
        {
            return (errnum);
        }

        user = timing_log.user[out_idx];

        // If log record is complete

        if(user != 0)
        {
            // A negative user indicates the start of the super-cycle

            if(user < 0)
            {
                is_start_super_cycle = true;
                user = -user;
            }
            else
            {
                is_start_super_cycle = false;
            }

            if(timing_log.dysfunction_code[out_idx] != 0)
            {
                dysfunction_symbol_list = (timing_log.dysfunction_is_a_fault[out_idx] ? SYM_LST_CYC_FLT : SYM_LST_CYC_WRN);
                dysfunction_label = CmdPrintSymLst(dysfunction_symbol_list, timing_log.dysfunction_code[out_idx]);
            }
            else
            {
                dysfunction_label = "";
            }

            fprintf(c->f, "%10lu.%06lu %2u %3s %c %-8s %s",
                    timing_log.time[out_idx].unix_time,
                    timing_log.time[out_idx].us_time,
                    (uint16_t)user,
                    is_start_super_cycle ? "SSC" : "",
                    timing_log.reg_mode_flag[out_idx],
                    SYM_TAB_CONST[timing_log.stc_func_type[out_idx]].key.c,
                    dysfunction_label);
        }

        out_idx = (out_idx + 1) % FGC_LOG_CYCLES_LEN;
    }

    return (0);
}
#endif
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetAbsTime(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: ABSTIME     ZERO get option is not supported
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t          n;          // Number of element in array range
    uint16_t               errnum;         // Error number
    char * FAR           fmt;
    struct abs_time_us * value_p;
    struct abs_time_us   value;
    static char * FAR    decfmt[]   = { "%10lu.%06lu",    "%lu.%06lu"   };
    static char * FAR    hexfmt[]   = { " 0x%08lX.%05lX", "0x%lX.%05lX" };
    bool              set_f;
    bool              ze_f;           // ZERO get option active

    ze_f  = Test(p->flags, PF_GET_ZERO) &&              // Prepare zero flag
            Test(c->getopts, GET_OPT_ZERO);

    errnum = PropValueGet(c, p, NULL, 0, (uint8_t **)&value_p); // Get first abstime element to use as timestamp

    if (errnum)
    {
        return (errnum);
    }

    errnum = CmdPrepareGet(c, p, 2, &n);

    if (errnum)
    {
        return (errnum);
    }

    fmt = (Test(c->getopts, GET_OPT_HEX) ? hexfmt[(c->token_delim == ',')] :
           decfmt[(c->token_delim == ',')]);

    while (n--)                 // For all elements in the array range
    {
        errnum = PropValueGet(c, p, NULL, c->from, (uint8_t **)&value_p); // Get property (from DSP is necessary)

        if (errnum)
        {
            return (errnum);
        }

        value = *value_p;

        if (ze_f)
        {
            value_p->unix_time = 0.0;
            value_p->us_time   = 0.0;
        }

        set_f = ze_f;

        errnum = CmdPrintIdx(c, c->from);            // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        fprintf(c->f, fmt, value.unix_time, value.us_time);

        c->from += c->step;
    }

    if (set_f)
    {
        PropBlkSet(c, c->n_elements, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT); // Write back zeroed value(s)
    }

    return (0);
}
#pragma MESSAGE DISABLE C5909 // Disable 'assignment in codition' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetChar(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: CHAR properties
\*---------------------------------------------------------------------------------------------------------*/
{
    char            ch = 1;                 // Character to display (initialise to non-zero for while())
    char      *     ch_ptr;
    char            delim_save;             // Saved token delimiter
    uint16_t          hex_f;                  // HEX get option flag
    prop_size_t     n;                      // Number of array elements to display
    uint16_t          errnum = FGC_OK_NO_RSP; // Error number

    hex_f = Test(c->getopts, GET_OPT_HEX);

    errnum = CmdPrepareGet(c, p, (hex_f ? 8 : 64), &n);

    if (errnum)
    {
        return (errnum);
    }

    delim_save = c->token_delim;            // Save delimiter so that it can be restored at the end

    if (!hex_f)    // If displaying ASCII and not comma delimiter
    {
        c->token_delim = '\0';              // Suppress space delimiter
    }

    // For all characters in array range

    while (n-- &&
           (errnum = PropValueGet(c, p, NULL, c->from, (uint8_t **)&ch_ptr)) == 0 &&
           (ch = *ch_ptr) != 0)
    {
        errnum = CmdPrintIdx(c, c->from);   // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        if (hex_f)                          // If hex flag is set
        {
            fprintf(c->f, "0x%02X", ch);    // Display character as hex
        }
        else
        {
            fputc(ch, c->f);                // Display character as ASCII
        }

        c->from += c->step;
    }

    c->token_delim = delim_save;            // Restore delimiter in case called from parent or config.set

    return (errnum);
}
#pragma MESSAGE DEFAULT C5909
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetComp(struct cmd * c, uint16_t * list_idx, uint16_t n_found, fgc_db_t n_req, fgc_db_t comp_idx)
/*---------------------------------------------------------------------------------------------------------*\
  Helper function for GetComponent() below - This will display the type and description for one particular
  type of component.  It will also add the prefix for the number found and required before the first
  element of the list.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  errnum;                 // Error number
    bool    neat_f;                 // Neat get option flag
    char    comp_label[FGC_COMPDB_COMP_LBL_LEN + 2];    // Buffer to receive the component description

    neat_f = (c->token_delim == ' ');

    // Display found/req before first element

    if (! *list_idx)                            // If first element in list
    {
        errnum = CmdPrintIdx(c, c->from++);

        if (errnum)                                 // Print index if required
        {
            return (errnum);
        }

        fprintf(c->f, "%02u/%02u", n_found, n_req);
    }
    else
    {
        if (neat_f)
        {
            fputs("\n     ", c->f);         // Add spaces to line up colunns
        }
    }

    fputc(((*list_idx)++ ? '\\' : '|'), c->f);      // Add list element delimiter

    ENTER_BB();

    fprintf(c->f, (neat_f ? "%10s = " : "%s="), CompDbType(comp_idx));

    MemCpyWords((uint32_t)&comp_label, (uint32_t)CompDbLabel(comp_idx), ((sizeof comp_label) / 2));

    EXIT_BB();

    if (neat_f && comp_label[61])
    {
        comp_label[58] = '.';
        comp_label[59] = '.';
        comp_label[60] = '.';
        comp_label[61] = '\0';
    }

    fputs(comp_label, c->f);

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetComponent(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: COMPONENT properties
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t n;              // Number of array elements to display
    uint16_t      errnum;         // Error number
    fgc_db_t      grp_idx;        // Group index
    fgc_db_t      comp_idx = 0;   // Comp index
    uint16_t      list_idx;       // List element index
    bool        unknown_f;      // Scan for unknown components
    const fgc_db_t    n_comps = CompDbLength(); // Number of components

    if (!dls.active_f)              // If ID scan has not completed successfully
    {
        return (FGC_ID_NOT_READY);          // Report that ID is not ready
    }

    if (c->n_arr_spec)              // If array is specified
    {
        return (FGC_BAD_ARRAY_IDX);         // report BAD ARRAY INDEX
    }

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    Clr(c->getopts, GET_OPT_IDX);       // Turn of printing of element indexes
    unknown_f = false;                  // Start with known groups
    grp_idx = 0;                        // Reset group index

    while (n--)                         // For all elements
    {
        grp_idx++;                      // Consider next group
        list_idx = 0;

        if (!unknown_f)                 // If showing Matched/mismatched groups
        {
            switch (p->sym_idx)
            {
                case STP_MATCHED:

                    while (grp_idx < FGC_N_COMP_GROUPS &&       // Scan to find next matching group
                           (!SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) ||
                            SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) != dev.sys.detected[grp_idx]))
                    {
                        grp_idx++;
                    }

                    break;

                case STP_MISMATCHED:

                    while (grp_idx < FGC_N_COMP_GROUPS &&       // Scan to find next mismatching group
                           (!SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) ||
                            SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) == dev.sys.detected[grp_idx]))
                    {
                        grp_idx++;
                    }

                    break;
            }

            if (grp_idx >= FGC_N_COMP_GROUPS)           // If all groups checked
            {
                comp_idx = 0;                           // Switch to unknown comps
                list_idx = 0;
                unknown_f = true;
            }
            else
            {
                for (comp_idx = 1; comp_idx < n_comps; comp_idx++)
                {
                    ENTER_BB();

                    if (CompDbGroupIndex(comp_idx,dev.sys.sys_idx) == grp_idx)
                    {
                        errnum = GetComp(c, &list_idx, dev.sys.detected[grp_idx], SysDbRequiredGroups(dev.sys.sys_idx,grp_idx), comp_idx);

                        if (errnum)
                        {
                            EXIT_BB();
                            return (errnum);
                        }
                    }

                    EXIT_BB();
                }
            }
        }

        if (unknown_f)                  // If showing unknown components
        {
            while (comp_idx < n_comps && !dev.sys.n_unknown_grp[comp_idx])
            {
                comp_idx++;
            }

            if (comp_idx >= n_comps)         // If end of components reached
            {
                n = 0;                          // Abort early
            }
            else
            {
                errnum = GetComp(c, &list_idx, dev.sys.n_unknown_grp[comp_idx], 0, comp_idx);
                comp_idx++;

                if (errnum != 0)
                {
                    return (errnum);
                }
            }
        }
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetConfigChanged(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     Config property names and data for changed config properties
\*---------------------------------------------------------------------------------------------------------*/
{
    c->si_idx = c->n_prop_name_lvls = 0;               // Force complete name to be written

    p->n_elements = p->max_elements;

    return (CmdParentGet(c, p, PARENT_GET_CONFIG_CHANGED));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetConfigSet(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     Config property names and data for set config properties
\*---------------------------------------------------------------------------------------------------------*/
{
    c->si_idx = c->n_prop_name_lvls = 0;               // Force complete name to be written

    p->n_elements = p->max_elements;

    return (CmdParentGet(c, p, PARENT_GET_CONFIG_SET));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetConfigUnset(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     Unset config property names
\*---------------------------------------------------------------------------------------------------------*/
{
    c->si_idx = c->n_prop_name_lvls = 0;               // Force complete name to be written

    p->n_elements = p->max_elements;

    return (CmdParentGet(c, p, PARENT_GET_CONFIG_UNSET));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetCore(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     Core data properties.   Only supplies results with the BIN get option
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              n_bytes;
    uint16_t              n_words;
    prop_size_t         idx;
    prop_size_t         n_segs;
    uint8_t * FAR         core_data;
    uint8_t       *       table_data;
    uint16_t              errnum;
    struct fgc_corectrl corectrl;

    if (c->step > 1)                            // If array step is not 1
    {
        return (FGC_BAD_ARRAY_IDX);                     // Report bad array index
    }

    if (!Test(c->getopts, GET_OPT_BIN))     // If BIN get option not specified
    {
        return (FGC_BAD_GET_OPT);           // Report bad get option!
    }

    n_words    = 0;
    c->getopts = 0;             // Cancel all get options

    errnum = CmdPrepareGet(c, p, 1, &n_segs);

    if (errnum)
    {
        return (errnum);
    }

    /*--- Get Stack dump ---*/

    if (p->sym_idx == STP_STACK)
    {
        n_bytes = NVRAM_CORESTACK_W * 2;        // Length of stack dump in bytes

        CmdStartBin(c, p, (uint32_t)n_bytes);

        core_data = (uint8_t * FAR)NVRAM_CORESTACK_32;

        while (n_bytes--)               // Write data
        {
            c->store_cb(*(core_data++));
        }

        return (0);
    }

    /*--- Get Core dump ---*/

    CoreTableGet(&corectrl);            // Retrieve table from FRAM

    for (idx = c->from; idx <= c->to; idx++)
    {
        n_words += (corectrl.seg[idx].n_words + (sizeof(struct fgc_coreseg)) / 2);
    }

    CmdStartBin(c, p, (uint32_t)n_words * 2);

    /*--- Write data segments from FRAM ---*/

    for (idx = c->from; idx <= c->to; idx++)    // For each segment
    {
        n_bytes = corectrl.seg[idx].n_words * 2;
        core_data = (uint8_t * FAR)corectrl.addr_in_core[idx];

        while (n_bytes--)                       // Write data
        {
            c->store_cb(*(core_data++));
        }
    }

    /*--- Write one table entry per segment ---*/

    n_bytes = n_segs * sizeof(struct fgc_coreseg);
    table_data = (uint8_t *)&corectrl.seg[c->from];

    while (n_bytes--)
    {
        c->store_cb(*(table_data++));
    }

    return (0);
}

#define FGC_DIM_LOGICAL_ADDR_BITMASK (FGC_MAX_DIM_BUS_ADDR - 1)

/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetDiagChan(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: DIAGCHAN properties + the addressed diagnostic data!
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t     n;              // Number of array elements to display
    uint8_t     *     chan;           // Pointer to channel element
    uint16_t          data;           // data for channel
    uint16_t          errnum;         // Error number
    uint16_t          data_chan;      // Channel within diag.data[] array
    uint16_t          logical_dim;
    uint16_t          board_number;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    while (n--)   // For all elements
    {
        errnum = CmdPrintIdx(c, c->from);            // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        chan      = &((uint8_t *)p->value)[c->from];  // Get address of channel
        data_chan = *(chan);                        // Get current channel (list_offset is not used)

        fprintf(c->f, "0x%02X ", data_chan);        // Show raw channel number in hex always

        if (data_chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL))   // If DIM register
        {
            // Translate to logical address

            logical_dim = data_chan & FGC_DIM_LOGICAL_ADDR_BITMASK;

            // Protect against out-of-bounds access

            if(logical_dim >= ArrayLen(diag.logical_dim_info))
            {
                return FGC_BAD_ARRAY_IDX;
            }

            board_number = diag.logical_dim_info[logical_dim].flat_qspi_board_number;

            if (board_number < FGC_MAX_DIM_BUS_ADDR)
            {
                data_chan = board_number + (data_chan & 0x00E0);
            }
        }

        // Protect against out-of-bounds access

        if(data_chan >= ArrayLen(diag.data))
        {
            return FGC_BAD_ARRAY_IDX;
        }

        data = diag.data[data_chan];

        if (Test(c->getopts, GET_OPT_HEX))      // If hex get option
        {
            fprintf(c->f, "0x%04X", data);      // Display address and data in hex
        }
        else
        {
            if (data_chan < DIAG_MAX_ANALOG)
            {
                data &= 0xFFF;
                fprintf(c->f, "%4.0f", ((FP32)data * DIAG_ANALOG_CAL)); // Display as millivolts
            }
            else
            {
                if (data_chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL))   // if data is digital
                {
                    data &= 0xFFF;
                    fprintf(c->f, "0x%03X", data);  // Display address and data in hex
                }
                else
                {
                    fprintf(c->f, "%u", data);      // Display software channel in decimal
                }
            }
        }

        // Next element

        c->from += c->step;
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetDim(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: DIM logs properties
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  errnum = FGC_OK_NO_RSP;

    ENTER_BB();

    switch (c->sym_idxs[1])
    {
        case STP_DIM:
            errnum = DimGetDimLog(c, p); // G LOG.DIM
            break;

        case STP_DIM_NAMES:
            errnum = DimGetDimNames(c, p);  // G DIAG.DIM_NAMES
            break;

        case STP_FAULTS:
            errnum = DimGetFaults(c, p);    // G DIAG.FAULTS
            break;

        case STP_ANA_LBLS:
            errnum = DimGetAnaLbls(c, p);   // G DIAG.ANA_LBLS.{NAME}
            break;

        case STP_DIG80_LBLS:
            errnum = DimGetDigLbls(c, p, 0); // G DIAG.DIG80_LBLS.{NAME}
            break;

        case STP_DIGA0_LBLS:
            errnum = DimGetDigLbls(c, p, 1); // G DIAG.DIGA0_LBLS.{NAME}
            break;

        case STP_ANA:
            errnum = DimGetAna(c, p);       // G DIAG.ANA.{NAME}
            break;

        case STP_DIG:
            errnum = DimGetDig(c, p);       // G DIAG.DIG.{NAME}
            break;

        default:
            Crash(0xBAD7);
    }

    EXIT_BB();
    return (errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetElapsedTime(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: Integer properties

  This is a wrapper for GetInteger because the gateway needs a different handling for elapsed time
  properties to the FGC.  It is only used for the property DEVICE.RUN_TIME.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (GetInteger(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetErrIdx(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: ERR_IDX property only

  Both fieldbus and terminal commands have their own error index variables.  This function will link to the relevant
  variable and then use GetInteger() to report the value.
\*---------------------------------------------------------------------------------------------------------*/
{
    p->value = &c->device_par_err_idx;      // Link property value to error index for the command

    return (GetInteger(c, p));          // Display the error index
}

/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetFloat(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: FLOAT properties

  Notes: Measurement properties must be available in full resolution (8 digits) while configuration
  properties should be limited to 6 digits to prevent 5.0 appearing as 4.9999999, for example.  The
  NON_VOLATILE flag is used to control this choice of format when the NEAT get option is not active.
  The function supports the ZERO get option.
\*---------------------------------------------------------------------------------------------------------*/
{
    const bool bin_f  = Test(c->getopts, GET_OPT_BIN);
    const bool hex_f  = Test(c->getopts, GET_OPT_HEX);
    const bool zero_f = Test(c->getopts, GET_OPT_ZERO) && Test(p->flags, PF_GET_ZERO);

    prop_size_t     n;              // Number of element in array range
    FP32      *     value_p;        // Pointer to float value
    FP32            value;          // Float value
    uint16_t        errnum;         // Error number

    enum FGC_float_fmt format_idx = FLOAT_FMT_NONE;

    if (hex_f == false)
    {
        if (c->token_delim == ' ')
        {
            format_idx = FLOAT_FMT_NEAT;
        }
        else
        {
            format_idx = FLOAT_FMT_REGULAR;
        }
    }

    errnum = CmdPrepareGet(c, p, 4, &n);

    if (errnum)
    {
        return (errnum);
    }

    if (n && bin_f)
    {
        CmdStartBin(c, p, (uint32_t)(sizeof(float)) * (uint32_t)n);
    }

    while (n--)   // For all elements
    {
        errnum = PropValueGet(c, p, &zero_f, c->from, (uint8_t **)&value_p);

        if (errnum != 0)   // Get element value from property
        {
            return (errnum);
        }

        value = *value_p;

        // Zero value when the ZERO get option is active

        if (zero_f)
        {
            *value_p = 0.0;
        }

        // Export value in binary, hex or floating point

        if (bin_f)
        {
            FbsOutLong((char *) &value, c);
        }
        else
        {
            errnum = CmdPrintIdx(c, c->from);                // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            if (hex_f)
            {
                // Display as 0xXXXXXXXX

                fprintf(c->f,"0x%08lX", *((uint32_t *)&value));
            }
            else
            {
                // Display as x.xxxxxxxEyy

                errnum = CmdPrintfDsp(c, format_idx, value);

                if (errnum)
                {
                    return (errnum);
                }
            }

        }

        c->from += c->step;
    }

    if (zero_f)
    {
        // ZERO get option is active so write back zeroed value(s)

        PropBlkSet(c, c->n_elements, true, FGC_FIELDBUS_FLAGS_LAST_PKT);
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetPoint(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: POINT properties as TIME|REF
\*---------------------------------------------------------------------------------------------------------*/
{
    const bool bin_f = Test(c->getopts, GET_OPT_BIN);          // Binary get option active
    const bool hex_f = Test(c->getopts, GET_OPT_HEX);          // HEX get option active

    prop_size_t     n;              // Number of element in array range
    struct point   *value_p;        // Pointer to point value
    struct point    value;          // Point value
    uint16_t        errnum;         // Error number

    errnum = CmdPrepareGet(c, p, 2, &n);

    if(errnum != 0)
    {
        return(errnum);
    }

    if(n != 0 && bin_f)
    {
        CmdStartBin(c, p, (uint32_t)(sizeof(value)) * (uint32_t)n);
    }

    while(n--)   // For all elements
    {
        errnum = PropValueGet(c, p, NULL, c->from, (uint8_t **)&value_p);

        if(errnum != 0)   // Get element value from property
        {
            return(errnum);
        }

        value = *value_p;

        if(bin_f)
        {
            FbsOutBuf((uint8_t *)&value, sizeof(value), c);
        }
        else
        {
            errnum = CmdPrintIdx(c, c->from);                // Print index if required

            if(errnum != 0)
            {
                return(errnum);
            }

            if(hex_f)
            {
                // Display point time and reference as 0xTTTTTTTT|0xRRRRRRRR

                fprintf(c->f, "0x%08lX|0x%08lX", *((uint32_t *)&value.time), *((uint32_t *)&value.ref));
            }
            else
            {
                // Display time as n.nnnn (format index 4 (%.4f) for PrintFloat() in DSP pfloat.c)

                errnum = CmdPrintfDsp(c, FLOAT_FMT_TABLE_TIME, value.time);

                if(errnum != 0)
                {
                    return(errnum);
                }

                fputc('|',c->f);

                // Display reference as n.nnnnnnnE+/-nn (format index 1 (%.7E) for PrintFloat() in DSP pfloat.c)

                errnum = CmdPrintfDsp(c, FLOAT_FMT_REGULAR, value.ref);

                if(errnum != 0)
                {
                    return(errnum);
                }
            }
        }

        c->from += c->step;
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetId(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: ID properties only

  Note: The max_elements field of the property structure has been used to indicate the Dallas branch
  associated with the property:

    ID.VS.A     0
    ID.MEAS.A   1
    ID.VS.B     2
    ID.MEAS.B   3
    ID.LOCAL    4
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t         n;
    uint16_t              errnum;         // Error number
    bool                neat_f;         // Neat get option flag
    uint16_t              logical_path;
    uint16_t              id_dev_idx;
    uint16_t       *      sorted_idx;
    char * FAR          fmt;            // Pointer to format string
    struct TMicroLAN_element_summary  * id_dev;
    char                comp_label[FGC_SYSDB_SYS_LBL_LEN + 2];
    char                id_string[ 2 * MICROLAN_UNIQUE_SERIAL_CODE_LEN + 2 ];
    static char * FAR   slfmt[] = { "%s%3u.%02u %-19s ", "%s:%02u.%02u:%s=" };


    if (p->sym_idx == STP_SUMMARY)      // If getting ID - report scan summary (ID.SUMMARY)
    {
        errnum = CmdPrepareGet(c, p, 1, &n);

        if (errnum)
        {
            return (errnum);
        }

        while (n--)             // For all elements
        {
            errnum = CmdPrintIdx(c, c->from);                        // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            fprintf(c->f, "%u|cplrs %u devs %2u {idonly %2u idtemp %2u other %2u} max_reads %u",
                    c->from,
                    id.logical_paths_info[c->from].ds2409s,
                    id.logical_paths_info[c->from].total_devices,
                    id.logical_paths_info[c->from].ds2401s,
                    id.logical_paths_info[c->from].ds18b20s,
                    id.logical_paths_info[c->from].unknow_devices,
                    id.logical_paths_info[c->from].max_reads);

            // Next element

            c->from += c->step;
        }
    }
    else                // Get results data
    {
        Set(c->getopts, GET_OPT_IDX);               // Set IDX get option
        neat_f        = (c->token_delim == ' ');        // Prepare neat flag
        logical_path  = p->max_elements;            // Max Elements in XML is branch number
        p->n_elements = id.logical_paths_info[logical_path].total_devices;
        fmt           = slfmt[!neat_f];             // Prepare format for symbol string

        errnum = CmdPrepareGet(c, p, 1, &n);

        if (errnum)
        {
            return (errnum);
        }

        while (n--)             // For all elements
        {
            errnum = CmdPrintIdx(c, c->from);

            if (errnum)             // Print index if required
            {
                return (errnum);
            }

            sorted_idx = &id.to_summary_sorted_by_codebar[id.start_in_elements_summary[logical_path] + c->from];
            id_dev_idx = *(sorted_idx);
            id_dev     = &id.devs[id_dev_idx];

            fprintf(c->f, fmt,
                    (char * FAR)DlsIdString(&id_dev->unique_serial_code, id_string),
                    id_dev->temp_C,
                    ID_TEMP_FRAC_C(id_dev->temp_C_16),
                    (char * FAR)id_dev->barcode.c);

            ENTER_BB();
            MemCpyWords((uint32_t)&comp_label,
                        (uint32_t)CompDbLabel(dev.sys.comp_idx[id_dev_idx]),
                        ((sizeof comp_label) / 2));
            EXIT_BB();

            if (neat_f && comp_label[30])       // There is space for up to 30 characters
            {
                // on the terminal window (NEAT mode)
                comp_label[27] = '.';
                comp_label[28] = '.';
                comp_label[29] = '.';
                comp_label[30] = '\0';
            }

            fputs(comp_label, c->f);

            // Next element

            c->from += c->step;
        }
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetInteger(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: uint8_t, int8_t, uint16_t, int16_t, uint32_t & int32_t properties
\*---------------------------------------------------------------------------------------------------------*/
{
    bool                bin_f;          // Binary get option active
    bool                sl_f;           // Property uses symlist flag
    bool                bm_f;           // Property uses bit mask (symbol list)
    bool                ze_f;           // ZERO get option active
    bool                set_f;          // Value has been set (to zero by ZERO get opt)
    prop_size_t         n;              // Number of array elements to display
    uint16_t              type;           // Integer type index (0-5)
    uint16_t              size;           // Integer size index (0-2)
    int8_t               v8   = 0;       // 8-bit value
    int16_t              v16  = 0;       // 16-bit value
    uint16_t              u16  = 0;       // 16-bit (unsigned) value
    int32_t              v32  = 0;       // 32-bit value
    uint16_t              errnum;         // Error number
    struct sym_lst   *  sym_lst;        // Symlist address
    char * FAR          fmt;            // Pointer to format string
    union
    {
        int8_t     *     p8;
        int16_t     *    p16;
        int32_t     *    p32;
    } value;

    static char * FAR   slfmt[]     = { "%13s", "%s" };
    static char * FAR   hexfmt[]    = { "0x%02X", "0x%04X", "0x%08lX" };
    static char * FAR   hexneatfmt[]    = { "         0x%02X", "       0x%04X", "   0x%08lX" };
    static char * FAR   decfmt[]    = { "%u", "%d", "%u", "%d", "%lu", "%ld" };
    static char * FAR   decneatfmt[]    = { "%13u", "%13d", "%13u", "%13d", "%13lu", "%13ld" };
    static uint16_t       size_bytes[]    = { 1, 2, 4 };



    sl_f   = Test(p->flags,  PF_SYM_LST) &&         // Prepare symlist flag
             !Test(c->getopts, GET_OPT_HEX);

    ze_f   = Test(p->flags,  PF_GET_ZERO) &&            // Prepare zero flag
             Test(c->getopts, GET_OPT_ZERO);

    bm_f   = Test(p->flags,  PF_BIT_MASK) && sl_f;      // Prepare bit mask flag

    bin_f  = Test(c->getopts, GET_OPT_BIN) && !sl_f;    // Prepare binary flag

    errnum = CmdPrepareGet(c, p, (bm_f ? 1 : 4), &n);   // Handle standard print options

    if (errnum)
    {
        return (errnum);
    }

    if (!n)                                     // If no data
    {
        return (FGC_OK_NO_RSP);                         // Return immediately with OK status
    }

    switch (p->type)            // Prepare integer type index (0-5)
    {
        case PT_INT8U:  type = 0;   break;

        case PT_INT8S:  type = 1;   break;

        case PT_INT16U: type = 2;   break;

        case PT_INT16S: type = 3;   break;

        case PT_INT32U: type = 4;   break;

        case PT_INT32S: type = 5;   break;

        default:
            return (FGC_BAD_PARAMETER);
            break;
    }

    size = type / 2;                // Prepare integer size index (0-2)

    if (sl_f)                   // If property uses symbol list
    {
        sym_lst = (struct sym_lst *)p->range;       // Get address of symbol list
        fmt     = slfmt[(c->token_delim == ',')];   // Prepare format for symbol string
        ze_f    = 0;                    // Cancel zero flag (should never be set)
    }
    else                    // else property doesn't use a sym list
    {
        if (Test(c->getopts, GET_OPT_HEX))      // Prepare format to use for integer data
        {
            fmt = ((c->token_delim == ' ') ? hexneatfmt[size] : hexfmt[size]);
        }
        else
        {
            fmt = ((c->token_delim == ' ') ? decneatfmt[type] : decfmt[type]);
        }
    }

    set_f = false;

    if (bin_f)
    {
        CmdStartBin(c, p, (uint32_t)size_bytes[size] * (uint32_t)n);
    }

    while (n--)             // For all elements in the array range
    {
        errnum = PropValueGet(c, p, &set_f, c->from, (uint8_t **) &value.p8);

        if (errnum  != 0)
        {
            return (errnum);
        }

        switch (size)               // Get data and zero if required
        {
            case 0:                 // Size 0: 8-bit

                v8 = *value.p8;

                if (ze_f) { *value.p8 = 0; }

                if (bin_f)
                {
                    c->store_cb(v8);
                }

                break;

            case 1:                 // Size 1: 16-bit

                v16 = *value.p16;

                if (ze_f) { *value.p16 = 0; }

                if (bin_f)
                {
                    FbsOutShort((uint8_t *) &v16, c);
                }

                break;

            case 2:                 // Size 2: 32-bit

                v32 = *value.p32;

                if (ze_f) { *value.p32 = 0; }

                if (bin_f)
                {
                    FbsOutLong((char *) &v32, c);
                }

                break;
        }

        set_f = ze_f;

        if (!bin_f)
        {
            errnum = CmdPrintIdx(c, c->from);              // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            if (sl_f)               // If property uses a sym list
            {
                u16 = (uint16_t)(size ? (size == 1 ? v16 : v32) : v8);

                if (bm_f)                   // If property is a bit mask
                {
                    CmdPrintBitMask(c->f, sym_lst, u16);    // Display all symbols which are active
                }
                else                    // else numeric symlist
                {
                    fprintf(c->f, fmt, CmdPrintSymLst(sym_lst, u16)); // Display value as a symbol
                }
            }
            else                    // else property is simply integer data
            {
                switch (size)               // Display according to data size
                {
                    case 0:
                        v16 = (type ? v8 : (uint8_t)v8);

                        // Fall through

                    case 1:
                        if (type == 2)          // meaning, PT_INT16U
                        {
                            u16 = (uint16_t)v16;
                            fprintf(c->f, fmt, u16);
                        }
                        else
                        {
                            fprintf(c->f, fmt, v16);
                        }

                        break;

                    case 2:
                        fprintf(c->f, fmt, v32);
                        break;
                }
            }
        }

        c->from += c->step;
    }

    if (set_f)
    {
        PropBlkSet(c, c->n_elements, true, FGC_FIELDBUS_FLAGS_LAST_PKT); // Write back zeroed value(s)
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetLogEvt(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: LOGEVT properties

  Notes:    For the terminal only 20 of the maximum 23 property name characters will be displayed.
\*---------------------------------------------------------------------------------------------------------*/
{
    static char * FAR evtfmt[] = { "%lu%06lu:%s:%s:%s", "%08lX.%06lu:%-20s:%-35s:%s" };

    uint16_t           offset;
    int16_t            idx;
    struct log_evt_rec evt_rec;
    char * FAR         fmt;
    uint16_t           errnum;
    prop_size_t        n;
    bool               neat_f;

    if(c->step > 1)                            // If array step is not 1
    {
        return(FGC_BAD_ARRAY_IDX);                     // Report bad array index
    }

    errnum = CmdPrepareGet(c, p, 1, &n);    // Handle standard print options

    if (errnum)
    {
        return (errnum);
    }

    idx    = log_evt.index - c->from;
    neat_f = (c->token_delim == ' ');
    fmt    = evtfmt[neat_f];            // Prepare format string

    Clr(c->getopts, GET_OPT_IDX);       // Suppress display of element index

    if(Test(c->getopts, GET_OPT_BIN))
    {
        // Always dump all event log records

        if(c->n_arr_spec != 0 || c->step > 1)
        {
            return(FGC_BAD_ARRAY_IDX);
        }

        CmdStartBin(c, p, FGC_LOG_EVT_LEN * sizeof(struct log_evt_rec));

        for(idx = 0; idx < FGC_LOG_EVT_LEN; idx++)
        {
            offset = idx * sizeof(struct log_evt_rec);

            OS_ENTER_CRITICAL();
            MemCpyWords((uint32_t)&evt_rec, LOG_EVT_32 + offset, sizeof(struct log_evt_rec) / 2);
            OS_EXIT_CRITICAL();

            // Always put a null as the last action character. This is for compatibility with libevtlog,
            // where the last character is a record status and should be set to null or space

            evt_rec.action[FGC_LOG_EVT_ACT_LEN - 1] = '\0';

            FbsOutBuf((uint8_t *)&evt_rec, sizeof(struct log_evt_rec), c);
        }
    }
    else
    {
        uint16_t i;
        char     ch;

        // For all elements in the array range

        while (n--)
        {
            if (idx < 0)
            {
                idx += FGC_LOG_EVT_LEN;
            }

            // Use intermediate variable to avoid compiler bug

            offset = sizeof(struct log_evt_rec) * idx;

            OS_ENTER_CRITICAL();
            MemCpyWords((uint32_t)&evt_rec, LOG_EVT_32 + offset, sizeof(struct log_evt_rec) / 2);
            OS_EXIT_CRITICAL();

            if (!evt_rec.timestamp.unix_time)           // If end of buffer reached
            {
                return (0);                     // Return immediately
            }

            if (neat_f)                 // If neat (terminal)
            {
                evt_rec.prop[20] = '\0';                // Clip at 20 chars for prop name
            }

            errnum = CmdPrintIdx(c, c->from++);          // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            // Substitute all commas in the property name with single quotes
            // and all colons with pipes. Commas and colons are special characters
            // used by the communication protocol.

            for(i = 0; i < FGC_LOG_EVT_PROP_LEN; i++)
            {
                ch = evt_rec.prop[i];

                if(ch == '\0')
                {
                    break;
                }
                else if(ch == ',')
                {
                    evt_rec.prop[i] = '\047';
                }
                else if(ch == ':')
                {
                    evt_rec.prop[i] = '|';
                }
            }

            // Substitute all commas in the property value with single quotes
            // Comma is a character reserved by the communication protocol

            for(i = 0; i < FGC_LOG_EVT_VAL_LEN; i++)
            {
                ch = evt_rec.value[i];

                if(ch == '\0')
                {
                    break;
                }
                else if(ch == ',')
                {
                    evt_rec.value[i] = '\047';
                }
            }

            // Always put a null as the last action character. This is for compatibility with libevtlog,
            // where the last character is a record status and should be set to null or space

            evt_rec.action[FGC_LOG_EVT_ACT_LEN - 1] = '\0';

            fprintf(c->f, fmt, evt_rec.timestamp.unix_time,
                    evt_rec.timestamp.us_time,
                    (char * FAR)evt_rec.prop,
                    (char * FAR)evt_rec.value,
                    (char * FAR)evt_rec.action);
            idx--;
        }
    }

    return (0);
}
#pragma MESSAGE DISABLE C5909 // Disable 'assignment in codition' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetLogPmSpy(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: Post mortem LOG properties in binary in fgc_log.h format for Spy

  Notes:    This get function only returns data for commands via the WorldFIP with the BIN get option
        set.

        Interfaces such as FGCspy can read from up to 10 FGCs on a FIP segment simultaneously.
        To achieve synchronisation, the fbs.log_sync_time variable is used.  This is set to
        the current unix time in FbsProcessTimeVar() if the FGC_SYNC_LOG bit of the flags word
        of the FIP time variable makes a transition from 0 to 1.  The value of fbs.log_sync_time is
        cleared to zero when this bit makes a transition from 1 to 0.  The bit is controlled by
        the gateway property FGC.SYNC_LOGS.  Setting the property will set the bit.

        The synchronisation of the logs depends on the log being read:

        Log Fill time   Min Read time   KB  Twait   Comment
        -----------------------------------------------------------------------------------------
        IAB    8.192s      10.925s  64  2.681s  This is a fudge factor - no real wait
        IEARTH    20.480s       0.685s   4  2.5s
        ILEADS  1024.000s       2.050s  12  2.5s
        IREG      38.400s      10.242s  60  0s
        THOUR   3600.000s       0.482s  2.8 2.2s
        TDAY   91200.000s       0.203s  1.2 2.2s

        The temperature logs (THOUR, TDAY) are treated slightly differently to the others because
        they fill so slowly (10s or 600s periods).  The synchronisation delay is completed before
        taking the last sample time/index.  For the others, a more complicated calculation is
        needed to determine which will be the first/last samples read out so as to avoid crossing
        the filling line.  This is then followed by the synchronisation delay for IEARTH and ILEADS
        but not for the others.  IAB is the most demanding buffer because it fills faster than it
        is read out, and for this the same calculation works by setting the wait time to a
        particular value (2681) but without this actually resulting in a sync delay of 2.6s.  This
        is signalled by making the wait time negative (-2681).
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              s;
    uint16_t              n_sigs;
    uint16_t              n_samples;
    uint16_t              sample_size;
    uint16_t              value_size;
    uint16_t              sample_idx;
    uint16_t              sig_type;
    int16_t              twait_ms;
    uint32_t              d_unix_time;
    uint16_t              dTsync_ms;
    uint16_t              dTlast_ms;
    uint16_t              dIdx;
    uint8_t       *       bin_data;
    uint32_t              addr;
    uint16_t              last_sample_idx;
    struct abs_time_us  last_sample_time;
    uint16_t              last_sample_ms_time;
    uint32_t              offset;
    struct log_ana_vars    *    log;
    struct abs_time_ms          sync_time;
    FP32                        values[LOG_MAX_SIGS];   // Buffer must be size of largest log sample
    union                   // Union to reduce impact on stack
    {
        struct SPY_v2_buf_header   log;
        struct SPY_v2_sig_header   sig;
    } header;

    /*--- Check that Get options and array specifiers are valid for a DIM log ---*/

    if (c->n_arr_spec || c->step > 1)       // If array indecies are specified or step > 1
    {
        return (FGC_BAD_ARRAY_IDX);         // Report error
    }

    if (!Test(c->getopts, GET_OPT_BIN))     // If BIN get option not specified
    {
        return (FGC_BAD_GET_OPT);           // Report bad get option
    }

    /*--- Calculate synchronisation according to type of log ---*/

    log = (struct log_ana_vars *)p->value;  // Get address of log structure for this property
    n_sigs      = log->n_signals;       // Get local copy of number of signals
    n_samples   = log->n_samples;       // Get length of buffer in samples
    sample_size = log->sample_size;     // Get local copy of sample size in bytes

    if (log->type == STP_THOUR ||       // THOUR log    (2880B 0.1 Hz data) or
        log->type == STP_TDAY)           // TDAY  log    (1152B 10 min data)
    {
        CmdWaitUntil(((fbs.log_sync_time ?              // Wait now for 2.2 seconds to be sure
                       fbs.log_sync_time :              // not to collide with acquisition of
                       SM_UXTIME_P) + 2), 200);                // temperatures (on 10s boundaries)

        last_sample_idx          = log->last_sample_idx;        // Get most recent entry index
        last_sample_time         = log->last_sample_time;       // and time stamp
        last_sample_time.us_time = 0;
        last_sample_ms_time      = 0;
        sync_time.unix_time      = 0;                   // No extra sync delay
        sig_type = FGC_LOG_TYPE_INT16U;                 // Temps are 16-bit unsigned shorts
    }
    else                    // All other logs
    {
        sig_type = FGC_LOG_TYPE_FLOAT;                  // All other logs are floats
        sync_time.unix_time = 0;                    // No sync delay by default

        OS_ENTER_CRITICAL();                        // Protect against interrupts
        last_sample_idx       = log->last_sample_idx;           // Get most recent entry index
        last_sample_time      = log->last_sample_time;                  // and time stamp
        OS_EXIT_CRITICAL();
        last_sample_ms_time   = log->last_sample_time.us_time / 1000;

        if (log->samples_to_acq)            // If log running
        {
            sync_time.unix_time = fbs.log_sync_time;

            if (!sync_time.unix_time)                   // If sync time is not active
            {
                sync_time.unix_time = last_sample_time.unix_time;   // Set sync time to start
                dTsync_ms = last_sample_ms_time;            // of current second
            }
            else                        // else sync is active
            {
                d_unix_time = last_sample_time.unix_time - sync_time.unix_time; // This must be done in 2 steps to
                dTsync_ms   = (uint16_t)d_unix_time * 1000           //  ensure 32-bit subtraction
                              + last_sample_ms_time;               // !!! Do not merge these lines !!!
            }

            twait_ms = log->wait_time_ms;

            if (twait_ms  > 0)                                  // If wait time is positive
            {
                sync_time.unix_time += twait_ms / 1000;     // Prepare to wait for the specified time
                sync_time.ms_time    = twait_ms % 1000;
            }
            else                    // else time is zero or negative
            {
                twait_ms = -log->wait_time_ms;          // Abs value should be used in calculation
                sync_time.unix_time = 0;            // but not real delay required
            }

            dIdx      = (((uint16_t)log->bufsize_w) / 6 + twait_ms - dTsync_ms) / log->period_ms;
            dTlast_ms = dIdx * log->period_ms;

            last_sample_idx       += dIdx;          // Adjust index to point to last sample
            last_sample_time.unix_time += dTlast_ms / 1000; // Adjust time to be of last sample
            last_sample_ms_time   += dTlast_ms % 1000;

            if (last_sample_ms_time > 999)
            {
                last_sample_ms_time -= 1000;
                last_sample_time.unix_time++;
            }
        }
    }

    sample_idx = last_sample_idx;   // Prepare sample index (will be pre-incremented to first sample)

    /*--- Generate log header ---*/

    spySetBufHeader(&header.log,                           // buf_header
                    false,                                 // little_endian
                    true,                                  // ana_signals
                    true,                                  // ana_sigs_in_columns
                    true,                                  // timestamp_is_lst
                    n_sigs,                                // num_signals
                    n_samples,                             // num_samples
                    0,                                     // time_origin_s
                    0,                                     // time_origin_us
                    last_sample_time.unix_time,            // time_stamp_s
                    last_sample_ms_time * 1000L,           // time_stamp_us
                    1000L * (uint32_t)log->period_ms);     // period_us

    CmdStartBin(c, p, (uint32_t)(sizeof(header.log)) +        // Write Binary data header (including length)
                (uint32_t)(sizeof(header.sig) * n_sigs) + log->bufsize_w * 2);


    FbsOutBuf((uint8_t *) &header.log, sizeof(header.log), c);        // Write log header

    /*--- Generate signal headers ---*/

    value_size = log->sample_size / log->n_signals;

    for (s = 0; s < n_sigs; s++)
    {
        spySetAnaSigHeader(&header.sig,                                                          // sig_header
                           log->sig_info[s],                                                     // step
                           (sig_type == FGC_LOG_TYPE_INT16U ? true : false),                     // use_scaling
                           sig_type,                                                             // sig_type
                           value_size,                                                           // size
                           (sig_type == FGC_LOG_TYPE_INT16U ? (1.0 / ID_TEMP_GAIN_PER_C) : 1.0), // gain
                           0.0,                                                                  // offset
                           0,                                                                    // time_offset_us
                           NULL,                                                                 // name
                           NULL);                                                                // units

        // The function above cannot copy FAR objects so copy the signal name explictely

        MemCpyStrFar(header.sig.name, log->sig_label_units[s]);

        FbsOutBuf((uint8_t *) &header.sig, sizeof(header.sig), c);    // Write signal header
    }

    /*--- Wait for synchronisation if required ---*/

    if (sync_time.unix_time)                // If sync delay required
    {
        CmdWaitUntil(sync_time.unix_time, sync_time.ms_time);           // Sleep required delay time
    }

    /*--- Write data ---*/

    while (n_samples--)                 // Write each sample in binary to FIP stream
    {
        sample_idx = (sample_idx + 1) % log->n_samples;     // Pre-increment sample index
        offset     = (uint32_t)sample_size * (uint32_t)sample_idx;  // Use intermediate s to avoid compiler bug
        addr       = log->baseaddr + offset;

        ENTER_SR();                     // Select RAM in pages 23
        MemCpyWords((uint32_t)&values, addr, sample_size / 2); // Copy one sample from log to the stack
        EXIT_SR();                      // Deselect RAM in pages 23

        s = sample_size;
        bin_data = (uint8_t *)&values;

        while (s--)                     // Write bytes from the stack to the stream
        {
            c->store_cb(*(bin_data++));
        }
    }

    return (0);
}
#pragma MESSAGE DEFAULT C5909
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetMemDsp(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: DEBUG.MEM.DSP property - DSP memory dump.
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t n;                  // Number of array elements to display
    uint32_t  addr;                   // Address in DSP memory
    uint32_t  value_int;              // Integer read from memory
    FP32    value_float;                // Float read from memory
    uint32_t   *  addr_int_p;             // Address to memory as int
    FP32  * addr_fp_p;              // Address to memory as float
    uint16_t      errnum;

    if (c->n_arr_spec)                  // If array indexes given
    {
        return (FGC_BAD_ARRAY_IDX);             // Report BAD_ARRAY_IDX
    }

    addr = dpcom.mcu.debug_mem_dsp_addr;        // Address in DSP memory
    addr_int_p = (uint32_t *) &dpcom.dsp.debug_mem_dsp_i;     // Address of data in DPCOM zone
    addr_fp_p  = (FP32 *)   &dpcom.dsp.debug_mem_dsp_f;     // Address of data in DPCOM zone

    c->to = DB_DSP_MEM_LEN;             // Set number elements

    p->n_elements = c->to;              // Set n_elements to fool CmdPrepareGet()

    errnum = CmdPrepareGet(c, p, 2, &n);        // Prepare to report 2 32-bit words per line

    if (errnum)
    {
        return (errnum);
    }

    p->n_elements = p->max_elements;            // Restore n_elements

    while (n--)             // For all elements to show
    {
        if (c->abort_f)             // If command was aborted
        {
            return (FGC_ABORTED);           // Report ABORTED
        }

        value_float = *(addr_fp_p++);           // Get float from DPCOM
        value_int   = *(addr_int_p++);          // Get integer from DPCOM

        if ((c->token_delim == ' ') &&      // If not comma delimiter and
            !(c->line % c->linelen))      // it's time for a new line
        {
            if (Test(c->getopts, GET_OPT_IDX))      // If Index required
            {
                fprintf(c->f, "\n0x%05lX:", addr);      // Write address
            }
            else if (c->line)               // else if not the first element
            {
                fputc('\n', c->f);              // Write newline
            }
        }
        else if (c->line)           // else if delimiter required
        {
            fputc(c->token_delim, c->f);        // Write delimiter character (space or comma)
        }

        c->line++;

        fprintf(c->f, "%13.5E:0x%08lX", value_float, value_int); // Write int and float values in ASCII

        addr++;                 // Advance address to next word
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetMemMcu(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: DEBUG.MEM.MCU property - MCU memory dump.  BIN get option is supported.
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t n;                  // Number of array elements to display
    uint16_t  value;                  // Word read from memory
    uint32_t  addr;                   // Address to memory
    uint32_t  max_n_words;                // Max num of words (to end of 20 bit address space)
    uint16_t      errnum;

    if (c->n_arr_spec || c->step > 1)           // If array indexes given or step > 1
    {
        return (FGC_BAD_ARRAY_IDX);             // Report BAD_ARRAY_IDX
    }

    if (!debug.mem[1])                  // If n_elements is zero
    {
        debug.mem[1] = 128;                 // Set 128 words by default
    }

    addr = debug.mem[0] & (0xFFFFFFFE);         // Round address to word boundary
    max_n_words = (0x100000 - addr) / 2;        // Max number of words to end of 20-bit address space

    if (debug.mem[1] < max_n_words)         // If user supplied number of words is less than max
    {
        max_n_words = debug.mem[1];             // Use user supplied number of words
    }

    if (Test(c->getopts, GET_OPT_BIN))          // if BIN get option flag is set
    {
        n = (max_n_words < 0x8000 ? max_n_words : 0x8000);  // Clip number of words to 32K (64KB)

        CmdStartBin(c, p, (uint32_t)n * sizeof(uint16_t));      // Start binary transmission
    }
    else
    {
        c->to = (max_n_words < 512 ? max_n_words : 512);    // Clip number of words to 512 (1KB)

        p->n_elements = c->to;                  // Set n_elements to fool CmdPrepareGet()

        errnum = CmdPrepareGet(c, p, 8, &n);            // Prepare to report 8 words per line

        if (errnum)
        {
            return (errnum);
        }

        p->n_elements = p->max_elements;            // Restore n_elements
    }

    while (n--)             // For all words to show
    {
        if (RegRead(addr, &value, false))        // If bus error occurs when reading word from memory
        {
            value = 0xDEAD;
        }

        if (Test(c->getopts, GET_OPT_BIN))  // If BIN get option
        {
            FbsOutShort((uint8_t *)&value, c);        // Write word in binary
        }
        else                    // else (ASCII)
        {
            if (c->abort_f)             // If command was aborted
            {
                return (FGC_ABORTED);               // Report ABORTED
            }

            if ((c->token_delim == ' ') &&      // If not comma delimiter and
                !(c->line % c->linelen))          // it's time for a new line
            {
                if (Test(c->getopts, GET_OPT_IDX))          // If Index required
                {
                    fprintf(c->f, "\n0x%05lX:", addr);          // Write address
                }
                else    // else if not the first element
                {
                    if (c->line)
                    {
                        fputc('\n', c->f);                  // Write newline
                    }
                }
            }
            else // else if delimiter required
            {
                if (c->line)
                {
                    fputc(c->token_delim, c->f);        // Write delimiter character (space or comma)
                }
            }

            c->line++;

            fprintf(c->f, "0x%04X", value);     // Write word in ASCII using HEX
        }

        addr += 2;              // Advance address to next word
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetParent(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: PARENT properties
\*---------------------------------------------------------------------------------------------------------*/
{
    return (CmdParentGet(c, p, PARENT_GET_NOT_HIDDEN));
}
/*---------------------------------------------------------------------------------------------------------*/
void GetPropInfo(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: Diagnostic info about specified property

  Notes:    This is a special Get function because it is activated by the get option INFO rather than
        a property itself.  It is intended to provide diagnostic information about the property.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              dsp_status = FGC_OK_NO_RSP;

    struct TInternalInfoFmt
    {
        char * FAR      label;
        char * FAR      valfmt;
    };


    char * FAR                      fmt;
    static char * FAR                   lblfmt[] = { "%-16s", "%s" };
    static struct TInternalInfoFmt      info[] =
    {
        { "struct prop *:",     "0x%04X\n"      },      // 0
        { "sym_idx:",       "%u %s\n"   },              // 1
        { "flags:",     "0x%04X\n"  },                  // 2
        { "type:",      "%u %s\n"   },                  // 3
        { "setif_func_idx:",    "%u 0x%05lX\n"  },      // 4
        { "set_func_idx:",  "%u 0x%05lX\n"  },          // 5
        { "get_func_idx:",  "%u 0x%05lX\n"  },          // 6
        { "n_elements:",    "%u"        },              // 7
        { "max_elements:",  "%u\n"      },              // 8
        { "range:",     "0x%04X\n"  },                  // 9
        { "value:",     "0x%04X\n"  },                  // 10
        { "dsp_idx:",       "%u\n"      },              // 11
        { "nvs_idx:",       "0x%02X\n"  },              // 12
        { "sub_idx:",       "%u\n"          },          // 13
        { "dynflags:",      "0x%02X\n"  },              // 14
        { "parent *:",          "0x%04X\n"      },      // 15
    };


    if (Test(p->flags, PF_DSP_FGC))
    {
        dsp_status = PropValueGet(c, p, NULL, 0, NULL);// Get n_element if DSP property
    }

    CmdPrintTimestamp(c, p);                         // Produce timestamp

    fmt = lblfmt[c->token_delim == ','];

    fprintf(c->f, fmt, info[0].label);                  // Property pointer
    fprintf(c->f, info[0].valfmt, (uint16_t)p);

    fprintf(c->f, fmt, info[15].label);                 // Parent pointer
    fprintf(c->f, info[15].valfmt, (uint16_t)p->parent);

    fprintf(c->f, fmt, info[1].label);                  // Symbol index (and symbol)
    fprintf(c->f, info[2].valfmt, p->sym_idx, SYM_TAB_PROP[p->sym_idx].key.c);

    fprintf(c->f, fmt, info[2].label);                  // Flags
    fprintf(c->f, info[2].valfmt, p->flags);

    fprintf(c->f, fmt, info[14].label);                 // Dynamic flags
    fprintf(c->f, info[14].valfmt, p->dynflags);

    fprintf(c->f, fmt, info[3].label);                  // Property type
    fprintf(c->f, info[3].valfmt, p->type, prop_type_name[p->type]);

    fprintf(c->f, fmt, info[4].label);                  // Setif function
    fprintf(c->f, info[4].valfmt, p->setif_func_idx, (uint32_t) SETIF_FUNC[p->setif_func_idx]);

    fprintf(c->f, fmt, info[5].label);                  // Set function
    fprintf(c->f, info[5].valfmt, p->set_func_idx, (uint32_t) SET_FUNC[p->set_func_idx]);

    fprintf(c->f, fmt, info[6].label);                  // Get function
    fprintf(c->f, info[6].valfmt, p->get_func_idx, (uint32_t) GET_FUNC[p->get_func_idx]);

    fprintf(c->f, fmt, info[7].label);                  // Number of elements

    if (dsp_status == FGC_DSP_NOT_AVL)
    {
        fprintf(c->f, "%s", fgc_errmsg[dsp_status]);
    }
    else
    {
        fprintf(c->f, info[7].valfmt, PropGetNumEls(c, p));

        if (Test(p->flags, PF_INDIRECT_N_ELS))              // Indirect number of elements
        {
            fprintf(c->f, " (0x%04X)", (unsigned int)p->n_elements);
        }
    }

    fputc('\n', c->f);

    fprintf(c->f, fmt, info[8].label);                  // Max elements
    fprintf(c->f, info[8].valfmt, p->max_elements);

    fprintf(c->f, fmt, info[9].label);                  // Range
    fprintf(c->f, info[9].valfmt, p->range);

    fprintf(c->f, fmt, info[10].label);                 // Value pointer
    fprintf(c->f, info[10].valfmt, p->value);

    fprintf(c->f, fmt, info[11].label);                 // DSP property index
    fprintf(c->f, info[11].valfmt, p->dsp_idx);

    fprintf(c->f, fmt, info[12].label);                 // NVS index
    fprintf(c->f, info[12].valfmt, p->nvs_idx);

    fprintf(c->f, fmt, info[13].label);                 // Subscription index
    fprintf(c->f, info[13].valfmt, p->sub_idx);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetPropSize(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: Size info about specified property (el_size, num_els, max_els)

  Notes:    This is a special Get function because it is activated by the get option SIZE rather than
        a property itself.  It is intended to provide diagnostic information about the property.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              i;
    prop_size_t         size[3];
    char * FAR          fmt;
    static char * FAR   decfmt[] = { "%13u", "%u" };
    static char * FAR   hexfmt[] = { "       0x%04X", "0x%04X" };
    uint16_t              errnum;

    if (Test(p->flags, PF_DSP_FGC))
    {
        errnum = PropValueGet(c, p, NULL, 0, NULL);     // Get n_element if DSP property

        if (errnum)
        {
            return (errnum);
        }
    }

    CmdPrintTimestamp(c, p);                            // Produce timestamp

    size[0] = (prop_size_t)prop_type_size[p->type]; // Get element size in bytes
    size[1] = PropGetNumEls(c, p);          // Get number of elements in property
    size[2] = p->max_elements;              // Get max number of elements in property

    fmt = (Test(c->getopts, GET_OPT_HEX) ? hexfmt[(c->token_delim == ',')] :    // Prepare format to respect
           decfmt[(c->token_delim == ',')]);     // Hex and Neat options

    if (Test(c->getopts, GET_OPT_LBL))          // If LBL option defined
    {
        CmdPrintLabel(c, p);            // Display property label
    }

    for (i = 0; i < 3; i++)             // For each element of size[]
    {
        if (i)                      // If not first element
        {
            fputc(c->token_delim, c->f);        // then write delimiter
        }

        fprintf(c->f, fmt, size[i]);            // Display size parameter
    }

    fputc('\n', c->f);                  // write newline

    return (FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetString(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: STRING

  This supports scalar near strings and arrays of far strings.
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t     n;
    uint16_t      errnum;         // Error number
    uint16_t      mux_offset;
    char * FAR      str;
    char * FAR   *  strs;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    mux_offset = p->max_elements * c->mux_idx;

    if (p->max_elements == 1 &&         // If scalar property, and
        !Test(p->flags, PF_SUB_DEV))             // not a SUB_DEV property
    {
        str  = ((char * FAR)p->value) + mux_offset; // Add address of scalar string
        strs = &str;                // Point array at this scalar
    }
    else                    // else array property
    {
        strs = ((char * FAR *)p->value) + (c->from + mux_offset);
    }

    while (n--)                 // For all elements in the array range
    {
        errnum = CmdPrintIdx(c, c->from);          // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        if (*strs)                  // If not a null pointer
        {
            fputs(*(strs), c->f);           // Display string
        }

        strs += c->step;
        c->from += c->step;
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetTemp(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays: Temperatures (uint32_t)
\*---------------------------------------------------------------------------------------------------------*/
{
    prop_size_t         n;
    uint16_t              hex_f;          // Hex get option flag
    uint16_t              errnum;
    uint16_t              temp;                   // Temperature
    uint16_t       *      temp_ptr;
    char * FAR          fmt;
    static char * FAR   hexfmt[] = { "       0x%04X", "0x%04X" };
    static char * FAR   decfmt[] = { "%10u.%02u", "%u.%02u" };

    errnum = CmdPrepareGet(c, p, 4, &n);

    if (errnum)
    {
        return (errnum);
    }

    hex_f =  Test(c->getopts, GET_OPT_HEX);
    fmt   = (hex_f ? hexfmt[(c->token_delim == ',')] : decfmt[(c->token_delim == ',')]);

    while (n--)                 // For all elements in the array range
    {
        errnum = PropValueGet(c, p, NULL, c->from, (uint8_t **)&temp_ptr);

        if (errnum)
        {
            return (errnum);
        }

        temp = *temp_ptr;

        errnum = CmdPrintIdx(c, c->from);          // Print index if required
        c->from++;

        if (errnum)
        {
            return (errnum);
        }

        if (hex_f)
        {
            fprintf(c->f, fmt, temp);                             // Display temperature in hex
        }
        else
        {
            fprintf(c->f, fmt, (temp >> 4), ID_TEMP_FRAC_C(temp));  // Display temperature as a float
        }
    }

    return (0);
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: get.c
\*---------------------------------------------------------------------------------------------------------*/
