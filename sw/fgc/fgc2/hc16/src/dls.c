/*---------------------------------------------------------------------------------------------------------*\
 File:      dls.c

 Purpose:   FGC MCU Software - Dallas bus functions

 Author:    Quentin.King@cern.ch

 Notes:     This file contains the function for the Dallas Task and all the functions related to
        reading the IDs and temperatures from the Dallas devices controlled by the C62
        microcontroller.
\*---------------------------------------------------------------------------------------------------------*/

#define DLS_GLOBALS             // define dls, id, temp, barcode, barcode_n_els global variables
#define DEFPROPS_INC_ALL        // defprops.h

#include <dls.h>
#include <stdlib.h>             // for abs()
#include <cmd.h>                // for pcm global variable
#include <fbs_class.h>          // for WARNINGS, ST_LATCHED
#include <property.h>           // for DF_SET_BY_DALLAS_ID
#include <defprops.h>           // Include property related definitions
#include <defconst.h>           // for FGC_CRATE_TYPE, FGC_ID_N_BRANCHES
#include <definfo.h>            // for FGC_CLASS_ID
#include <fgc/fgc_db.h>         // for access to CompDB
#include <prop.h>               // for PropSetNumEls()
#include <mst.h>                // for mst global variable
#include <crate.h>              // for crate global variable
#include <macros.h>             // for Test(), Set(), Clr()
#include <sta.h>                // for sta global variable
#include <mem.h>                // for MemCpyBytes(), MemCpyWords()
#include <os.h>                 // for OSTskSuspend()
#include <m16c62_link.h>        // for C62_SWITCH_PROG
#include <microlan_1wire.h>     // for struct TBranchSummary, struct TMicroLAN_element_summary
#include <dpcls.h>              // for dpcls global variable, CAL_T0
#include <memmap_mcu.h>         // for CPU_RESET_CTRL_C62OFF_MASK16, SM_UXTIME_P
#include <fbs.h>                // for fbs global variable
#include <dev.h>                // for dev global variable
#include <log.h>                // for LogTStart()
#include <string.h>             // for strncmp(), strlen()
#include <mcu_dependent.h>      // for Get2MHzCounter()
#include <shared_memory.h>      // for FGC_MAINPROG_RUNNING



enum FGC_dls_status
{
    DLS_OK,
    DLS_WAIT,
    DLS_TIMEOUT,
    DLS_NO_TEMP_SENSORS,
    DLS_SET_BRANCH_FAILED,
    DLS_SET_DEV_FAILED,
    DLS_READ_TEMP_FAILED,
    DLS_READ_RESPONSE_FAILED,
    DLS_EMPTY_RESPONSE,
    DLS_ISR_CMD_ERR,
    DLS_ISR_CMD_TIMEOUT,
    DLS_BG_CMD_TIMEOUT
};



enum FGC_dls_power_control
{
    DLS_POWER_OFF,
    DLS_POWER_ON,
};



static enum FGC_dls_power_control DlsRun(uint16_t m16c62_ctrl_reg);
static enum FGC_dls_status DlsConvTemp (uint16_t logical_path);
static enum FGC_dls_status DlsReadTemp (struct prop *p, uint16_t m16c62_ctrl_reg);
static enum FGC_dls_status DlsReadIDs  (uint16_t m16c62_ctrl_reg);
static void    DlsLogTemp              (void);
static bool    ValidateTemperatureAndCopyForDSP(bool report_invalid, uint16_t temp_to_check, uint32_t *dsp_temp);
static void    DlsBarcodeBus           (struct prop *p, uint16_t logical_path);
static void    DlsPropTemp             (uint16_t logical_path);
static void    DlsSetPropertyChan      (uint16_t logical_path, char *barcode_buf);
static enum FGC_dls_status DlsSendIsrCmd(uint8_t cmd, uint8_t arg);
static enum FGC_dls_status DlsSendByte (uint8_t tx, uint8_t *rx);
static enum FGC_dls_status DlsReadRsp  (uint8_t *buf, uint16_t *max_bytes);
static void    DlsRspError             (struct prop *p, uint16_t errloc, uint16_t errnum, uint16_t rsplen);
static void    DlsResetBarcodes        (struct prop *p);


#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void DlsTsk(void *unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the Dallas task.  It is woken by the MstTsk() at the start of every 100ms period in
  order to read the temperatures from the M16C62.  A temperature reading is make once per 10s, but the process
  takes several seconds and the task polls at 10Hz to know when the new measurements are ready to be
  read out from the M16C62.  It will log the results at 0.1Hz in the THOUR log and once per 10 minutes in the
  TDAY log.

  When the task first runs (or after a S ID RESET command), the task will also read out the Dallas IDs of
  all the connected devices and will analyse them to see if all the components that are expected for the
  type of power converter identified by the device name are in fact present.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  m16c62_ctrl_reg;

    // Initialise ADC and DCCT temperatures used by the DSP calibration system

#if FGC_CLASS_ID == 59
    // nothing to do
#else
    dpcls.mcu.cal.t_adc = CAL_T0 * ID_TEMP_GAIN_PER_C;
    dpcls.mcu.cal.t_dcct[0] = dpcls.mcu.cal.t_dcct[1] = dpcls.mcu.cal.t_adc;
#endif

    // Dallas Task main loop

    for(;;)
    {
        OSTskSuspend();             // Wait for MstTsk() to wake DslTsk at 10 Hz

        m16c62_ctrl_reg = C62_P;

        if ( m16c62_ctrl_reg & C62_POWER_MASK16 )       // If M16C62 power is ON
        {
            // If M16C62 must be switched off

            if(DlsRun(m16c62_ctrl_reg) == DLS_POWER_OFF)
            {
                CPU_RESET_P |= CPU_RESET_CTRL_C62OFF_MASK16;    // Switch off M16C62

                if ( dls.temp_fail_f )              // If Temperature failure detected
                {
                    WARNINGS |= FGC_WRN_TEMPERATURE;        // Set TEMPERATURE warning
                }
                else
                {
                    WARNINGS &= ~FGC_WRN_TEMPERATURE;       // Clear TEMPERATURE warning
                }
            }
        }
        else
        {
            if (    !mst.s_mod_10                               // If on the 10s boundary
                 && (temp.unix_time != SM_UXTIME_P)             // for the first time
               )
            {
                temp.unix_time = SM_UXTIME_P;                   // Record time stamp
                temp.tday_f    = !mst.s_mod_600;                // Make TDAY logging flag (10 minute period)

                if ( !dls.inhibit_f )                   // If not inhibited
                {
                    C62_P = C62_SWITCH_PROG;                // Tell M16C62 boot to run in slave mode
                    CPU_RESET_P &= ~CPU_RESET_CTRL_C62OFF_MASK16;       // Turn on M16C62
                    dls.state = DLS_START;              // Prepare state machine
                    dls.temp_fail_f = false;            // Clear temperature failed flag
                }
                else                            // else skip this acquisition
                {
                    DlsLogTemp();               // Log previous point again
                    dls.inhibit_f = FGC_CTRL_DISABLED;      // Remove inhibit
                }
            }
        }
    }
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_dls_power_control DlsRun(uint16_t m16c62_ctrl_reg)
/*---------------------------------------------------------------------------------------------------------*\
  [10,20,30,40]
  This function is called at 10Hz if the M16C62 is running.
  It executes according to a state machine that aims to move the M16C62 from boot to main program
  to running the temperature scans for the  different branches.
  The function returns 0 if the process is still in progress and the M16C62 should remain powered.
  It returns 1 when the operation is complete (successfully or otherwise) and the M16C62 should be
  powered off.
\*---------------------------------------------------------------------------------------------------------*/
{
    enum FGC_dls_status status;

    switch(dls.state)
    {
        case DLS_START:

            // If M16C62 boot did not respond

            if(((m16c62_ctrl_reg ^ C62_SWITCH_PROG) & 0x00FF) != 0xFF)
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_10, M16C62_NO_RSP, 0);
                return DLS_POWER_OFF;
            }

            // Request to switch to main prog

            status = DlsSendByte(C62_SWITCH_PROG, NULL);

            if(status != DLS_OK)
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_20, status, 0);
                return DLS_POWER_OFF;
            }

            dls.state = DLS_AT_MAINPROG;

            break;

        case DLS_AT_MAINPROG:

            // If main M16C62 program not started

            if(m16c62_ctrl_reg & C62_CMDBUSY_MASK16)
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_30, M16C62_NO_RSP, 0);
                return DLS_POWER_OFF;
            }

            // If M16C62 boot did not respond

            if(((m16c62_ctrl_reg ^ C62_SWITCH_PROG) & 0x00FF) != 0xFF)
            {
                DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_39, M16C62_NO_RSP, 0);
                return DLS_POWER_OFF;
            }

            // If ID not read yet

            if(!dls.active_f)
            {
                status = DlsSendByte(C62_READ_IDS, NULL);

                if(status != DLS_OK)
                {
                    DlsRspError(&PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_40, status, 0);
                    return DLS_POWER_OFF;
                }

                dls.state       = DLS_READID;
                // dls.timeout_cnt = C62_READID_TIMEOUT * 10;    // Set timeout in 100ms units
            }
            else
            {
                dls.state = DLS_CONVTEMPA;
            }

            break;

        case DLS_READID:

            // Get IDs and barcodes

            status = DlsReadIDs(m16c62_ctrl_reg);

            switch(status)
            {
                case DLS_WAIT:             // waiting, wait another 100ms

                    return DLS_POWER_ON;

                case DLS_OK:

                    dls.state++;
                    // no break; on purpose
            }

            return DLS_POWER_OFF;

        case DLS_CONVTEMPA:

            status = DlsConvTemp(ID_LOGICAL_PATH_FOR_MEAS_A);

            switch(status)
            {
                case DLS_NO_TEMP_SENSORS:

                    dls.state++;            // Advance to next CONVTEMPx
                    // no break; on purpose

                case DLS_OK:

                    dls.state++;            // Advance to next READTEMPx
                    return DLS_POWER_ON;
            }

            return DLS_POWER_OFF;

        case DLS_READTEMPA:

            status = DlsReadTemp(&PROP_FGC_DLS_MEAS_A, m16c62_ctrl_reg);

            switch(status)
            {
                case DLS_OK:

                    dls.state++;            // Advance to next CONVTEMPx
                    // no break; on purpose

                case DLS_WAIT:              // waiting, wait another 100ms

                    return DLS_POWER_ON;
            }

            return DLS_POWER_OFF;

        case DLS_CONVTEMPB:

            status = DlsConvTemp(ID_LOGICAL_PATH_FOR_MEAS_B);

            switch(status)
            {
                case DLS_NO_TEMP_SENSORS:

                    dls.state++;            // Advance to next CONVTEMPx
                    // no break; on purpose

                case DLS_OK:

                    dls.state++;            // Advance to next READTEMPx
                    return DLS_POWER_ON;

            }

            return DLS_POWER_OFF;

        case DLS_READTEMPB:

            status = DlsReadTemp(&PROP_FGC_DLS_MEAS_B, m16c62_ctrl_reg);

            switch(status)
            {
                case DLS_OK:

                    dls.state++;            // Advance to next CONVTEMPx
                    // no break; on purpose

                case DLS_WAIT:              // waiting, wait another 100ms

                    return DLS_POWER_ON;
            }

            return DLS_POWER_OFF;

        case DLS_CONVTEMPL:

            status = DlsConvTemp(ID_LOGICAL_PATH_FOR_LOCAL);

            switch(status)
            {
                case DLS_NO_TEMP_SENSORS:

                    dls.state++;            // Advance to next CONVTEMPx
                    // no break; on purpose

                case DLS_OK:

                    dls.state++;            // Advance to next READTEMPx
                    return DLS_POWER_ON;
            }

            return DLS_POWER_OFF;

        case DLS_READTEMPL:

            status = DlsReadTemp(&PROP_FGC_DLS_LOCAL, m16c62_ctrl_reg);

            switch(status)
            {
                case DLS_OK:

                    dls.state++;            // Advance to next CONVTEMPx
                    // no break; on purpose

                case DLS_WAIT:              // waiting, wait another 100ms

                    return DLS_POWER_ON;
            }

            return DLS_POWER_OFF;

        case DLS_LOGTEMPS:

            DlsLogTemp();
            return DLS_POWER_OFF;
    }

    return DLS_POWER_ON;
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_dls_status DlsConvTemp(uint16_t logical_path)
/*---------------------------------------------------------------------------------------------------------*\
  [6x,7x]
  This function is called to start the conversion of temperatures from sensors on the specified
  logical path.
  A failure will be considered to be a global failure and operation will stop.
\*---------------------------------------------------------------------------------------------------------*/
{
    enum FGC_dls_status errnum;

    // Check if there are temp sensors to be read

    if(id.logical_paths_info[logical_path].ds18b20s == 0)
    {
        return DLS_NO_TEMP_SENSORS;
    }

    // Request M16C62 to scan branch for temperatures

    dls.logical_path = logical_path;

    // Set logical_path for temp conversion

    errnum = DlsSendIsrCmd(C62_SET_BRANCH, logical_path);

    if(errnum != 0)
    {
        DlsRspError( &PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_60 + logical_path), errnum, 0);
        return DLS_SET_BRANCH_FAILED;
    }

    // Request scan for temperatures on this branch

    errnum = DlsSendByte(C62_READ_TEMPS_BRANCH, NULL);

    if(errnum != 0)
    {
        DlsRspError( &PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_70 + logical_path), errnum, 0);
        return DLS_READ_TEMP_FAILED;
    }

    // Set timeout in 100ms units

    dls.timeout_cnt = C62_READTEMP_TIMEOUT * 10;

    return DLS_OK;
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_dls_status DlsReadTemp(struct prop *p, uint16_t m16c62_ctrl_reg)
/*---------------------------------------------------------------------------------------------------------*\
  [8x,9x,10x,11x,12x,13x]
  This function is called once a READ_TEMPS_BRANCH command has been launched in the M16C62.
  It checks for the command to finish and then tries to read out the new temperature values
  for the temp sensors on the branch.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                              errnum;
    bool                                err_f;
    uint16_t                              in_elements_summary;
    uint16_t                              n_temps;
    uint16_t                              n_temps_read;
    uint16_t                              rsp_len;
    uint8_t                               data[M16C62_MAX_EXPECTED_RESPONSE];
    struct TMicroLAN_element_summary *  dev;
    struct prop **                      prop_temp;

    // Check if READ_TEMPS_BRANCH has completed

    if ( (m16c62_ctrl_reg & C62_CMDBUSY_MASK16) != 0 )      // If C62_READ_TEMPS_BRANCH command NOT completed
    {
        dls.timeout_cnt--;

        if(dls.timeout_cnt == 0)
        {
            DlsRspError(p,(DLS_ERROR_LOCATION_80 + dls.logical_path), DLS_BG_CMD_TIMEOUT, 0);
            return DLS_TIMEOUT;
        }

        return DLS_WAIT;      // waiting, wait another 100ms
    }

    // Check response to READ_TEMPS_BRANCH BG command

    rsp_len = M16C62_MAX_EXPECTED_RESPONSE - 1;

    errnum = DlsReadRsp( (uint8_t *) &data, (uint16_t *) &rsp_len);

    if(errnum != 0)
    {
        DlsRspError( p, (DLS_ERROR_LOCATION_90 + dls.logical_path), errnum, 0);
        return DLS_READ_RESPONSE_FAILED;
    }

    if(rsp_len == 0)
    {
        DlsRspError( p, (DLS_ERROR_LOCATION_100 + dls.logical_path), M16C62_NO_MORE_DATA, 0);
        return DLS_EMPTY_RESPONSE;
    }

    err_f = (data[0] != 0);

    // If READ_TEMPS_BRANCH failed

    if(err_f)
    {
        MemCpyBytes( ((uint8_t *)(p->value))+1, (uint8_t *) &data, rsp_len);

        // Report failure for branch and prepare "BAD TEMP" value

        DlsRspError( p, (DLS_ERROR_LOCATION_110 + dls.logical_path), data[0], (rsp_len - 1) );

        data[2] = ID_BAD_TEMP;
        data[3] = 0;
    }

    dev       = id.element_info_ptr  [dls.logical_path];
    n_temps   = id.logical_paths_info[dls.logical_path].ds18b20s;
    prop_temp = &id.prop_temp[id.start_in_elements_summary[dls.logical_path]];

    // Scan the list of devices for the temp sensors

    for (in_elements_summary = n_temps_read = 0 ; n_temps_read < n_temps ; in_elements_summary++, dev++, prop_temp++)
    {
        if(dev->unique_serial_code.p.family == MICROLAN_DEVICE_DS18B20_THERMOMETER)    // If device is a temp sensor
        {
            if(!err_f)    // If no error for branch
            {
                // Set device index

                errnum = DlsSendIsrCmd(C62_SET_DEV, in_elements_summary);

                if(errnum != 0)
                {
                    DlsRspError( p, (DLS_ERROR_LOCATION_120 + dls.logical_path), errnum, 0);
                    return DLS_SET_DEV_FAILED;
                }

                // Get temperature for device

                rsp_len = sizeof(data);

                errnum = DlsReadRsp( (uint8_t *) &data, (uint16_t *) &rsp_len);

                if(errnum != 0)
                {
                    DlsRspError( p, (DLS_ERROR_LOCATION_130 + dls.logical_path), errnum, 0);
                    return DLS_READ_RESPONSE_FAILED;
                }
            }

            dev->temp_C    = data[2];               // Recover temperature from response
            dev->temp_C_16 = data[3];               // or BAD TEMP if error occurred

            if(*prop_temp != NULL)
            {
                *((uint16_t*)((*prop_temp)->value)) = dev->temp_C * 16 + dev->temp_C_16;
            }

            n_temps_read++;
        }
    }

    return DLS_OK;
}
/*---------------------------------------------------------------------------------------------------------*/
void DlsLogTemp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called once all the temperatures have been read to transfer the ADC and DCCT temperatures
  (if valid) to the DSP.

  temp.fgc.in and temp.fgc.out are linked with the corresponding temperature sensor
  via the assignment in components.xml with
      temp_prop = "TEMP.FGC.IN"
  and
      temp_prop = "TEMP.FGC.OUT"
  (this information is extracted from CompDB by the MCU program)
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID != 59)
    bool dcct_A_temp_is_valid;
    bool dcct_B_temp_is_valid;
#endif

    // Calculate FGC delta (outlet - inlet)

    if (
#if FGC_CLASS_ID == 59
            ValidateTemperatureAndCopyForDSP(REPORT_INVALID_TEMPERATURE, temp.fgc.in, NULL)
#else
            ValidateTemperatureAndCopyForDSP(REPORT_INVALID_TEMPERATURE, temp.fgc.in, &dpcls.mcu.cal.t_adc)
#endif
         && ValidateTemperatureAndCopyForDSP(REPORT_INVALID_TEMPERATURE, temp.fgc.out, NULL)
         && ( temp.fgc.out > temp.fgc.in )
        )
    {
        temp.fgc.delta = temp.fgc.out - temp.fgc.in;
    }
    else
    {
        temp.fgc.delta = ID_BAD_TEMP * ID_TEMP_GAIN_PER_C;      // Bad data: 99.0C
        dls.temp_fail_f = true;
    }

    // Transfer DCCT temperatures to DSP - use other channel if measurement fails, or ADC

#if FGC_CLASS_ID == 59
    // nothing to do
#else
        if(crate.type == FGC_CRATE_TYPE_PC_60A ||       // If crate type uses RITZ DCCTs
           crate.type == FGC_CRATE_TYPE_PC_120A)
        {
            dpcls.mcu.cal.t_dcct[0] = dpcls.mcu.cal.t_dcct[1] = dpcls.mcu.cal.t_adc; // Use FGC temp for DCCTs
        }
        else
        {
            dcct_A_temp_is_valid = ValidateTemperatureAndCopyForDSP(temp.dcct_temp_available[0], temp.dcct[0].elec, &dpcls.mcu.cal.t_dcct[0]);
            dcct_B_temp_is_valid = ValidateTemperatureAndCopyForDSP(temp.dcct_temp_available[1], temp.dcct[1].elec, &dpcls.mcu.cal.t_dcct[1]);

            if ( !dcct_A_temp_is_valid && dcct_B_temp_is_valid )
            {
                dpcls.mcu.cal.t_dcct[0] = dpcls.mcu.cal.t_dcct[1];
            }

            if ( dcct_A_temp_is_valid && !dcct_B_temp_is_valid )
            {
                dpcls.mcu.cal.t_dcct[1] = dpcls.mcu.cal.t_dcct[0];
            }
        }

    // Collect temperatures (16-bit) ready to be logged

    temp.log.dcct_a  = temp.dcct[0].elec;
    temp.log.dcct_b  = temp.dcct[1].elec;

#endif

    temp.log.fgc_in  = temp.fgc.in;
    temp.log.fgc_out = temp.fgc.out;

    temp.thour_f = true;
}
/*---------------------------------------------------------------------------------------------------------*/
bool ValidateTemperatureAndCopyForDSP(bool report_invalid, uint16_t temp_to_check, uint32_t * dsp_temp)
/*---------------------------------------------------------------------------------------------------------*\
  This function will check if the measured temperature is valid and if so, it will set the DSP temp if
  a pointer is supplied.
  It returns true if valid and false if not valid and sets the temp_fail_f if
  the report flag is true.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(temp_to_check != 0 &&
       temp_to_check < (ID_BAD_TEMP * ID_TEMP_GAIN_PER_C))
    {
        // If DSP temperature pointer supplied, also set this variable to the temperature

        if(dsp_temp != NULL)
        {
            *dsp_temp = (uint32_t) temp_to_check;
        }

        return true;
    }

    if(report_invalid)
    {
        dls.temp_fail_f = true;
    }

    return false;
}

/*---------------------------------------------------------------------------------------------------------*/
enum FGC_dls_status DlsReadIDs(uint16_t m16c62_ctrl_reg)
/*---------------------------------------------------------------------------------------------------------*\
  [140,15x,16x,17x,18x]
  This will read the IDs for all branches and will look for specific components

  fill the table id.to_summary_sorted_by_codebar[] sorting the elements in elements summary
\*---------------------------------------------------------------------------------------------------------*/
{
    bool                                id_flt_f;
    uint16_t                              c;
    uint16_t                              ii;
    uint16_t                              errnum;
    int16_t                              mismatch;
    uint16_t                              rsp_len;
    fgc_db_t                              grp_idx;
    uint16_t                              in_elements_summary;
    fgc_db_t                              comp_idx;
    uint16_t                              logical_path;
    char                                comp_type[FGC_COMPDB_COMP_LEN+2];
    char *                              codebar_ptr;
    struct TMicroLAN_element_summary *  element_info_ptr;
    uint16_t                              in_sorted;

    const fgc_db_t n_comps = CompDbLength();

    // Check if READ_IDS has completed

    if ( m16c62_ctrl_reg & C62_CMDBUSY_MASK16 )     // If C62_READ_IDS command NOT completed
    {
        dls.timeout_cnt--;
        if ( !dls.timeout_cnt )                     // if timeout
        {
            DlsRspError( &PROP_FGC_DLS_GLOBAL, DLS_ERROR_LOCATION_140, DLS_BG_CMD_TIMEOUT, 0);
            return DLS_TIMEOUT;
        }
        return DLS_WAIT;      // wait another 100ms
    }

    // Read out ID device information for all branches

    element_info_ptr = (struct TMicroLAN_element_summary *) &id.devs;

    for( logical_path = 0; logical_path < FGC_ID_N_BRANCHES; logical_path++ )
    {
        errnum = DlsSendIsrCmd(C62_SET_BRANCH, logical_path);
        if ( errnum != 0 )
        {
            DlsRspError( &PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_150 + logical_path), errnum, 0);
            return DLS_SET_BRANCH_FAILED;
        }

        rsp_len = sizeof(struct TBranchSummary);

        errnum = DlsReadRsp( (uint8_t *) &id.logical_paths_info[logical_path], &rsp_len);
        if ( errnum != 0 )
        {
            DlsRspError( &PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_160 + logical_path), errnum, 0);
            return DLS_READ_RESPONSE_FAILED;
        }

        id.element_info_ptr[logical_path] = element_info_ptr;
        id.start_in_elements_summary[logical_path] = id.n_devs;

        for( in_elements_summary = 0; in_elements_summary < id.logical_paths_info[logical_path].total_devices; in_elements_summary++)
        {
            errnum = DlsSendIsrCmd(C62_SET_DEV, in_elements_summary);
            if ( errnum != 0)
            {
                DlsRspError( &PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_170 + logical_path), errnum, 0);
                return DLS_SET_DEV_FAILED;
            }

            rsp_len = sizeof(struct TMicroLAN_element_summary);

            errnum = DlsReadRsp( (uint8_t *) element_info_ptr, &rsp_len);
            if ( errnum != 0 )
            {
                DlsRspError( &PROP_FGC_DLS_GLOBAL, (DLS_ERROR_LOCATION_180 + logical_path), errnum, 0);
                return DLS_READ_RESPONSE_FAILED;
            }

            element_info_ptr++;
            id.n_devs++;
        }
    }

    dls.active_f = id.n_devs;       // Mark Dallas system as active if at least one ID device found

    // Sort on barcode for each branch

    for( logical_path = 0; logical_path < FGC_ID_N_BRANCHES; logical_path++)
    {
        element_info_ptr = id.element_info_ptr[logical_path];

        in_elements_summary = id.start_in_elements_summary[logical_path];

        for( c = 0; c < id.logical_paths_info[logical_path].total_devices; c++, element_info_ptr++, in_elements_summary++)
        {
            for( ii = c, in_sorted = in_elements_summary;
                (ii > 0)
                && strcmp(&element_info_ptr->barcode.c[0] , &id.devs[id.to_summary_sorted_by_codebar[in_sorted - 1] ].barcode.c[0]) < 0;
                    ii--, in_sorted--)
            {
                id.to_summary_sorted_by_codebar[in_sorted] = id.to_summary_sorted_by_codebar[in_sorted - 1];
            }

            id.to_summary_sorted_by_codebar[in_sorted] = in_elements_summary;
        }
    }

    // Identify and count the number of components found

    element_info_ptr = (struct TMicroLAN_element_summary *) &id.devs;

    for ( in_elements_summary = 0; in_elements_summary < id.n_devs; in_elements_summary++, element_info_ptr++ ) // For every 1-wire device found
    {
        codebar_ptr = (char *) &element_info_ptr->barcode.c;

        // we consider barcode valid if starts upper case
        if ( *codebar_ptr <= 'Z' )
        {
            comp_idx = 1;               // Start from component 1 (0=Unknown)

            do                          //  Scan list of comps in TYPEDB to find a match
            {
                MemCpyWords( (uint32_t)&comp_type, (uint32_t)CompDbType(comp_idx), ((FGC_COMPDB_COMP_LEN + 2) / 2));

                mismatch = strncmp(codebar_ptr, comp_type, FGC_COMPDB_COMP_LEN);
            }
            while(mismatch > 0 && ++comp_idx < n_comps);

            if(mismatch != 0)
            {
                comp_idx = 0;           // system 0 (unknown)
            }
        }
        else                            // else barcode is not valid
        {
            comp_idx = 0;               // system 0 (unknown)
        }

        dev.sys.comp_idx[in_elements_summary] = comp_idx;

        grp_idx = CompDbGroupIndex(comp_idx,dev.sys.sys_idx);

        if ( grp_idx < FGC_N_COMP_GROUPS )
        {
            dev.sys.detected[grp_idx]++;

            if ( !grp_idx )
            {
                dev.sys.n_unknown_grp[comp_idx]++;
            }
        }
    }

    // Check found-per-group count against expected-per-group for the given system

    id_flt_f = false;
    PROP_BARCODE_MISMATCHED.n_elements = dev.sys.detected[0];

    for ( grp_idx = 1; grp_idx < FGC_N_COMP_GROUPS; grp_idx++ )
    {
        if ( dev.sys.detected[grp_idx] != SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) )    // If any group is mismatched
        {
            if ( dev.sys.detected[grp_idx] < SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) ) // If reqd comps are missing
            {
                id_flt_f = true;
            }

            PROP_BARCODE_MISMATCHED.n_elements++;
        }
        else
        {
            if ( SysDbRequiredGroups(dev.sys.sys_idx,grp_idx) )
            {
                PROP_BARCODE_MATCHED.n_elements++;
            }
        }
    }

    if ( id_flt_f )                 // If any required components are missing
    {
        ST_LATCHED |= FGC_LAT_ID_FLT;       // Set ID_FLT in latched status
    }
    else
    {
        ST_LATCHED &= ~FGC_LAT_ID_FLT;      // Clear ID_FLT in latched status
    }

    // Link BARCODE.BUS properties to devices and store barcodes in BARCODE properties

#if FGC_CLASS_ID == 59
    // nothing to do
#else
    DlsBarcodeBus(&PROP_BARCODE_BUS_V, ID_LOGICAL_PATH_FOR_VS_A);
    DlsBarcodeBus(&PROP_BARCODE_BUS_V, ID_LOGICAL_PATH_FOR_VS_B);
    DlsBarcodeBus(&PROP_BARCODE_BUS_A, ID_LOGICAL_PATH_FOR_MEAS_A);
    DlsBarcodeBus(&PROP_BARCODE_BUS_B, ID_LOGICAL_PATH_FOR_MEAS_B);
#endif
    DlsBarcodeBus(&PROP_BARCODE_BUS_L, ID_LOGICAL_PATH_FOR_LOCAL);

    // Link devices to temperatures properties

    DlsPropTemp(ID_LOGICAL_PATH_FOR_VS_A);
    DlsPropTemp(ID_LOGICAL_PATH_FOR_VS_B);
    DlsPropTemp(ID_LOGICAL_PATH_FOR_MEAS_A);
    DlsPropTemp(ID_LOGICAL_PATH_FOR_MEAS_B);
    DlsPropTemp(ID_LOGICAL_PATH_FOR_LOCAL);

    // Start temperature logging

    LogTStart(&log_thour);
    LogTStart(&log_tday);


#if FGC_CLASS_ID == 59
    // nothing to do
#else
    // Determine if DCCT's have temperature sensors

    // If crate type might have Hitec DCCTs and a Hitec DCCT has been found

    if (    (crate.type == FGC_CRATE_TYPE_PC_600A)
         || (crate.type == FGC_CRATE_TYPE_PC_KA)
         || (crate.type == FGC_CRATE_TYPE_PC_THYRISTOR2)
         || (crate.type == FGC_CRATE_TYPE_PC_IN_TRIPLET)
       )
    {
        if (    ( PropGetNumEls( NULL, &PROP_BARCODE_A_DCCT_ELEC) == (FGC_ID_BARCODE_LEN - 1) )
             && (barcode.dcct[0].elec[13] == 'I')
            )
        {
            temp.dcct_temp_available[0] = true;     // Set DCCT temperature expected flag
        }

        if (    ( PropGetNumEls( NULL, &PROP_BARCODE_B_DCCT_ELEC) == (FGC_ID_BARCODE_LEN - 1) )
             && (barcode.dcct[1].elec[13] == 'I')
           )
        {
            temp.dcct_temp_available[1] = true;     // Set DCCT temperature expected flag
        }
    }
#endif

    // Set CONFIG.MODE/CONFIG.STATE according to whether SYNC_FGC will be attempted

    if (    dls.active_f                                        // If at least one component found and
         && (sta.config_state != FGC_CFG_STATE_STANDALONE)      // not running STANDALONE
         && (sta.config_mode  != FGC_CFG_MODE_SYNC_FAILED)      // and SYNC_FAILED not set and
        )
    {
        DlsResetBarcodes(&PROP_BARCODE);            // Reset barcodes not set by Dallas IDs
        sta.config_mode = FGC_CFG_MODE_SYNC_FGC;    // Initialise CONFIG.MODE to SYNC_FGC
    }
    else if (sta.config_state != FGC_CFG_STATE_STANDALONE)
    {
        sta.config_state = FGC_CFG_STATE_UNSYNCED;  // Set config state to UNSYNCED
    }

    // Tell boot that main program has completed it's start up

    SM_MPRUN_P = FGC_MAINPROG_RUNNING;  // This will tell the boot to do a fast restart if there is a Fast WD

    return DLS_OK;
}
/*---------------------------------------------------------------------------------------------------------*/
void DlsBarcodeBus(struct prop *p, uint16_t logical_path)
/*---------------------------------------------------------------------------------------------------------*\
  This will scan the barcodes of the devices for the specified branch looking for those that are not
  DB lookup errors (which begin with a *).  Valid bar codes are linked to the BARCODE.BUS property and
  also if the component type was found for the device and if the type has a barcode property specified,
  then the barcode is also copied to this property.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                              ii;
    uint16_t                              n_els;
    uint16_t                              idx_in_summary;
    uint16_t                              comp_idx;
    char                                propertyname[FGC_COMPDB_PROP_LEN];
    uint16_t                              total_devices_on_logical_path;
    uint8_t * FAR *                       bc;
    struct TMicroLAN_element_summary *  element_info_ptr;


    bc = (uint8_t**)p->value;

    idx_in_summary = id.start_in_elements_summary[logical_path];

    element_info_ptr  = id.element_info_ptr[logical_path];

    total_devices_on_logical_path = id.logical_paths_info[logical_path].total_devices;

    p->dynflags |= DF_SET_BY_DALLAS_ID;


    for( ii = 0; ii < total_devices_on_logical_path; ii++, element_info_ptr++, idx_in_summary++ )// For all devices on the specified branch
    {
        if ( element_info_ptr->barcode.c[0] <= 'Z')         // If barcode is valid (starts upper case)
        {
            n_els = PropGetNumEls(NULL,p);
            if ( n_els < p->max_elements )
            {
                bc[n_els] = (uint8_t * FAR)&element_info_ptr->barcode.c;      // Add barcode to bus property
                PropSetNumEls(NULL, p, n_els+1);
            }

            comp_idx = dev.sys.comp_idx[idx_in_summary];

            if ( comp_idx )                     // If component was identified
            {
                MemCpyWords( (uint32_t)&propertyname,         // Get barcode property name
                         (uint32_t)CompDbBarcodeProperty(comp_idx),
                         (FGC_COMPDB_PROP_LEN / 2) );

                if ( *propertyname )
                {
                    DlsSetPropertyChan(logical_path, propertyname); // Replace '?' with A or B

                    OSSemPend(pcm.sem);

                    if ( !PropFind(propertyname) )
                    {
                        MemCpyBytes( pcm.prop->value, (uint8_t *) &(element_info_ptr->barcode.c), FGC_ID_BARCODE_LEN);
                        PropSetNumEls(NULL, pcm.prop, strlen(pcm.prop->value));

                        pcm.prop->dynflags |= DF_SET_BY_DALLAS_ID;
                    }

                    OSSemPost(pcm.sem);
                }
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DlsPropTemp(uint16_t logical_path)
/*---------------------------------------------------------------------------------------------------------*\
  This will scan the devices on each branch, looking for those for which a component has been identified
  and for which the component has a temperature property defined.
  For these devices, the link is made to the temperature property.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  ii;
    uint16_t  idx_in_summary;
    uint16_t  comp_idx;
    char    propertyname[FGC_COMPDB_PROP_LEN];
    uint16_t  total_devices_on_logical_path;

    idx_in_summary = id.start_in_elements_summary[logical_path];
    total_devices_on_logical_path = id.logical_paths_info[logical_path].total_devices;

    for( ii = 0; ii < total_devices_on_logical_path; ii++, idx_in_summary++ )
    {
        comp_idx = dev.sys.comp_idx[idx_in_summary];

        if ( comp_idx )                                     // If component was identified
        {
            MemCpyWords( (uint32_t)&propertyname,         // Get temperature property name
                 (uint32_t)CompDbTempProperty(comp_idx),
                 (FGC_COMPDB_PROP_LEN / 2) );

            if ( *propertyname )
            {
                DlsSetPropertyChan(logical_path, propertyname); // Replace '?' with A or B

                OSSemPend(pcm.sem);

                if ( !PropFind(propertyname) )          // If property found
                {
                    id.prop_temp[idx_in_summary] = pcm.prop;        // Link to device
                }

                OSSemPost(pcm.sem);
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DlsSetPropertyChan(uint16_t logical_path, char *barcode_buf)
/*---------------------------------------------------------------------------------------------------------*\
  This function is used to replace the "?" character in the temperature and barcode property names with
  "A" or "B" according to the branch idx (0,1 -> 'A'  2,3 -> 'B')
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( logical_path < ID_LOGICAL_PATH_FOR_LOCAL )
    {
        while(     *barcode_buf
                && (*barcode_buf != '?')
              )
        {
            barcode_buf++;
        }

        if ( *barcode_buf )
        {
            *barcode_buf = 'A' + logical_path / 2;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
char * DlsIdString(union TMicroLAN_unique_serial_code * devid, char *id_string)
/*---------------------------------------------------------------------------------------------------------*\
  This function will translate the supplied device 1-wire ID into an ASCII string so that it can be
  printed with the most significant byte (CRC) first.
\*---------------------------------------------------------------------------------------------------------*/
{
    sprintf(id_string,"%02X%02X%02X%02X%02X%02X%02X%02X",
        (uint16_t)devid->b[7],(uint16_t)devid->b[6],(uint16_t)devid->b[5],(uint16_t)devid->b[4],
        (uint16_t)devid->b[3],(uint16_t)devid->b[2],(uint16_t)devid->b[1],(uint16_t)devid->b[0]);

    return(id_string);
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_dls_status DlsSendIsrCmd(uint8_t cmd, uint8_t arg)
/*---------------------------------------------------------------------------------------------------------*\
  Send a M16C62 ISR command with one argument byte
\*---------------------------------------------------------------------------------------------------------*/
{
    enum FGC_dls_status errnum = DlsSendByte(cmd, NULL);

    if(errnum != 0)
    {
        return errnum;
    }

    return DlsSendByte(arg, NULL);
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_dls_status DlsSendByte(uint8_t tx, uint8_t *rx)
/*---------------------------------------------------------------------------------------------------------*\
  Sends a byte to the M16C62 and waits up to 30us for an immediate response
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      reg;
    uint16_t      t0;

    OS_ENTER_CRITICAL();

    C62_P = (uint16_t) tx;            // Send byte
    t0 = Get2MHzCounter();

    OS_EXIT_CRITICAL();

    do
    {
        reg = C62_P;
    }
    while ( !(reg & (C62_DATARDY_MASK16 | C62_CMDERR_MASK16)) && ( (Get2MHzCounter() - t0) < (C62_MAX_ISR_US * 2) ) );

    reg = C62_P;

    if(rx != NULL)
    {
        *rx = (reg & 0xFF);             // Return data byte
    }

    if ( reg & C62_DATARDY_MASK16 )     // If data ready
    {
        return DLS_OK;
    }

    if ( reg & C62_CMDERR_MASK16 )      // If command error
    {
        return DLS_ISR_CMD_ERR;
    }

    return DLS_ISR_CMD_TIMEOUT;
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_dls_status DlsReadRsp(uint8_t * buf, uint16_t * max_bytes)
/*---------------------------------------------------------------------------------------------------------*\
  Use DlsSendByte to read the response buffer from the M16C62
\*---------------------------------------------------------------------------------------------------------*/
{
    enum FGC_dls_status errnum;
    uint16_t              n_bytes;

    for ( n_bytes = 0; n_bytes < *max_bytes; n_bytes++, buf++)
    {
        errnum = DlsSendByte(C62_GET_RESPONSE, buf);

        if(errnum == DLS_ISR_CMD_ERR &&
           *buf   == M16C62_NO_MORE_DATA)
        {
            *max_bytes = n_bytes;
            return DLS_OK;
        }

        if(errnum != DLS_OK)
        {
            *max_bytes = n_bytes;
            return errnum;
        }
    }

    return DLS_OK;
}
/*---------------------------------------------------------------------------------------------------------*/
void DlsRspError(struct prop *p, uint16_t errloc, uint16_t errnum, uint16_t rsp_len)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare the response property when an error is detected.  The function also sets
  the MPRUN registers to indicate that the dallas scan has completed, even though an error may have
  stopped the scan early.
\*---------------------------------------------------------------------------------------------------------*/
{
    static uint32_t last_time_start  = 0;
    static uint16_t last_error_count = 0;

    uint8_t * rsp = (uint8_t*)p->value;

    // Store error location and errnum in property

    rsp[0] = errloc;
    rsp[1] = errnum;

    last_error_count++;

    // Set the latched condition only if the rate of dallas errors is
    // larger than 5 per hour.

    if(last_error_count > 5)
    {
        if((mst.time.unix_time - last_time_start) < 3600UL)
        {
            Set(ST_LATCHED, FGC_LAT_DALLAS_FLT);
        }

        last_time_start  = mst.time.unix_time;
        last_error_count = 0;
    }

    dls.num_errors++;

    // Time of last error

    dls.error_time = SM_UXTIME_P;

    // Set property length (Header bytes + C62 response)

    PropSetNumEls(NULL, p, rsp_len + 2);

    // This will tell the boot to do a fast restart if there is a Fast WD

    SM_MPRUN_P = FGC_MAINPROG_RUNNING;
}
#pragma MESSAGE DISABLE C1855 // Disable 'recursive function call' warning
/*---------------------------------------------------------------------------------------------------------*/
void DlsResetBarcodes(struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
 This recursive function will scan all the (CHAR) barcode properties and will reset the lengths of all that
 were not set by a Dallas ID.  This is done before requesting a SYNC_FGC by the FGC manager so that old
 barcodes don't remain to confuse the users.  Properties that were set by a Dallas ID have the
 DF_SET_BY_DALLAS_ID dynamic property flag set.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t          ii;
    uint16_t          n_els;
    struct prop *   child_prop;

    if ( p->type == PT_PARENT )                     // If property is a parent
    {
        child_prop = p->value;                      // Get address of array of lower level properties
        n_els = PropGetNumEls(NULL,p);              // Get number of low level properties

        for( ii = 0; ii < n_els; ii++, child_prop++ ) // For each child property
        {
            DlsResetBarcodes(child_prop);           // Call function recursively for the child
        }
    }
    else                                            // else property is NOT a parent
    {
        if (    (p->type == PT_CHAR)                         // If property is CHAR type (i.e. contains a barcode)
             && !Test(p->dynflags, DF_SET_BY_DALLAS_ID)      // and property was set NOT by a Dallas ID
               )
        {
            PropSetNumEls(NULL, p, 0);              // Clear property length
        }
    }
}
#pragma MESSAGE DEFAULT C1855
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dls.c
\*---------------------------------------------------------------------------------------------------------*/
