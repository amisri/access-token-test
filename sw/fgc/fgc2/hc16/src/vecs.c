/*---------------------------------------------------------------------------------------------------------*\
  File:         vecs.c

  Contents:     This file contains intermediate interrupt functions.

  Notes:        The intermediate interrupt functions are defined in the VEC_TABLE code segment which is
                allocated in the VEC_TABLE section in the PRM file.

                These functions are necessary because the HC16 interrupt vectors are only 16 bit, so the
                interrupt functions must be in page zero.  Since code is in page 4-5, the intermediate
                functions are needed to jump to the ISRs in the code page.  The HC16 vectors and the jump
                functions are stored in the first KB of the program, which is visible at the start of
                page 0, where it is needed by the HC16.  This window works when the RUNMAIN bit is set
                in the CPU_RESET register.  When it is not set, the window points to the start of page 2
                where the boot's vectors and jump functions are stored.

                The functions can perform a vital service before jumping to the relevant ISR in page one.
                In the case of user interrupts under NanOS, the push the register on the stack and
                prepare several registers before jumping to OSTskIntEnter().  In the case of trap interrupts,
                the first instruction can set register D with a code indicating which exception caused the
                interrupt.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef __DAC__                 // Skip when building DAC database to avoid errors

#define VARS_EXT

#include <os.h>                 // Include all header files
#include <tsk.h>

extern void     IsrBusError     (void);
extern void     IsrTrap         (void);
extern void     IsrMst          (void);
extern void     IsrFbs          (void);

#pragma NO_STRING_CONSTR        // Stop #xxx in macro from making a string literal "xxx" (needed for asm)

#define TRAP_VEC(id) void trap_vec_##id(void){asm LDD # id;asm JMP IsrTrap;}
#define BERR_VEC(id) void trap_vec_##id(void){asm JMP IsrBusError;}

#pragma CODE_SEG VEC_TABLE

#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x04)                          // 0x04         Breakpoint

#pragma NO_FRAME
#pragma NO_EXIT
BERR_VEC(0x05)                          // 0x05         Bus Error

#pragma NO_FRAME
#pragma NO_EXIT
OS_SWI_VEC(0x06)                        // 0x06         Software Interrupt

#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x07)                          // 0x07         Illegal instruction

#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x08)                          // 0x08         Division by zero

#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x0F)                          // 0x0F         Unitialised interrupt

#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x11)                          // 0x11-0x17    IRQ1 - IRQ7 (not used)

#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x18)                          // 0x18         Spurious interrupt

#pragma NO_FRAME
#pragma NO_EXIT
OS_USR_VEC(0x31,TSK_FBS,IsrFbs)         // 0x31         GPT IC1 (uFIP IRQ)

#pragma NO_FRAME
#pragma NO_EXIT
OS_USR_VEC(0x35,TSK_MST,IsrMst)         // 0x35         GPT OC2 (MS tick)

#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x00)                          // The rest     All other sources

#pragma CODE_SEG DEFAULT
#endif

/*---------------------------------------------------------------------------------------------------------*\
  End of file: vecs.c
\*---------------------------------------------------------------------------------------------------------*/
