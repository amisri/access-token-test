/*---------------------------------------------------------------------------------------------------------*\
  File:         menuDiag.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MENUDIAG_H  // header encapsulation
#define MENUDIAG_H

#ifdef MENUDIAG_GLOBALS
    #define MENUDIAG_VARS_EXT
#else
    #define MENUDIAG_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <diag_boot.h>          // for DIAG_

//-----------------------------------------------------------------------------------------------------------

void    MenuDiagShow            (uint16_t argc, char **argv);
void    MenuDiagControl         (uint16_t argc, char **argv);
void    MenuDiagSetMpx          (uint16_t argc, char **argv);
void    MenuDiagSetSprClk       (uint16_t argc, char **argv);
void    MenuDiagDisplayDipsRaw  (uint16_t argc, char **argv);
void    MenuDiagDisplayDipsAna  (uint16_t argc, char **argv);

//-----------------------------------------------------------------------------------------------------------

MENUDIAG_VARS_EXT        struct sym_name        diag_ana_lbl[]
#ifdef MENUDIAG_GLOBALS
= {
    {  DIAG_VMEAS1,                     "Vmeas1"                },
    {  DIAG_VMEAS10,                    "Vmeas10"               },
    {  DIAG_PSU5,                       "PSU+5V"                },
    {  DIAG_PSU15P,                     "PSU+15V"               },
    {  DIAG_PSU15N,                     "PSU-15V"               },
    {  DIAG_ANA5,                       "ANA+5V"                },
    {  DIAG_ANA15P,                     "ANA+15V"               },
    {  DIAG_ANA15N,                     "ANA-15V"               },
    { 0 }
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // MENUDIAG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuDiag.h
\*---------------------------------------------------------------------------------------------------------*/
