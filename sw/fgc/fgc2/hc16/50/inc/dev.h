/*---------------------------------------------------------------------------------------------------------*\
  File:         dev.h

  Purpose:      for class 50

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DEV_H   // header encapsulation
#define DEV_H

#ifdef DEV_GLOBALS
    #define DEV_VARS_EXT
#else
    #define DEV_VARS_EXT extern
#endif


//-----------------------------------------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>

#include <cc_types.h>           // basic typedefs

// for shared_mem.boot_seq (SM_BOOTTYPE_P)
#define BOOT_PWR                0               // Power start - do full self test and update codes
#define BOOT_NORMAL             1               // Normal start  - do partial test and update codes
#define BOOT_UNKNOWN            0xFF            // Uninitialised value for boot type

//-----------------------------------------------------------------------------------------------------------

struct dev_vars
{
    uint16_t                      state;                          // Boot state
    uint16_t                      rst_src;                        // Reset source from reset register
    uint16_t                      fieldbus_id;                    // Fip address
    uint16_t                      run_menu_f;                     // Run menu flag
    uint16_t                      n_rw_devs_to_update;            // Number of flash devices to update
    uint16_t                      n_ro_devs_to_update;            // Number of read-only flash devices to update
    uint16_t                      n_devs_corrupted;               // Number of corrupted codes detected
    volatile uint16_t             abort_f;                        // Abort operation flag (Ctrl-C pressed)
    // volatile because is used in a while() (MenuC62RunMonitor/CodesUpdateGetBlock/xsvfRun)
    // and at that moment is externally
    // modified by the interrupt TermProcessHardware/TermRxCh, if it is not volatile the compiler will get rid off it
    uint32_t                      mem_sbe_cnt;                    // FGC2: Memory single bit error counter
    uint8_t                       last_gpt_pacnt;                 // FGC2: Memory single bit error counter
    uint8_t                       mem_sbe_f;                      // FGC2: Memory single bit error flag
    uint16_t                      enable_slow_wd;                 // Enable triggering of slow watchdog
    uint16_t                      enable_fast_wd;                 // Enable triggering of fast watchdog
    uint16_t                      leds_read;                      // Read of LEDs register
    uint16_t                      slot5;                          // FGC2: Slot5 info register
    uint16_t                      slot5_type;                     // FGC2: Slot5 interface type
    uint16_t                      slot5_type_index;
    uint16_t                      class_id;                       // FGC class id based on Slot5 interface type
    uint16_t                      bus_type;                       // Bus type
    uint16_t                      pld_version;                    // FGC2: PLD version read from slot 5 register
    uint8_t                       device;                         // FGC2: 'A', 'B' or 'R'
    uint8_t                       reg_r;                          // Register access faults (read)
    uint8_t                       reg_w;                          // Register access faults (write)
};

//-----------------------------------------------------------------------------------------------------------

DEV_VARS_EXT struct dev_vars            dev;

//-----------------------------------------------------------------------------------------------------------
#pragma MESSAGE DISABLE C4301
#pragma INLINE
static bool DevIdValid(void)
{
    return (dev.fieldbus_id >= 1 && dev.fieldbus_id <= 30);
}

#endif  // DEV_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dev.h
\*---------------------------------------------------------------------------------------------------------*/
