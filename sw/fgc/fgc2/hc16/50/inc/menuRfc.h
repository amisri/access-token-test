/*---------------------------------------------------------------------------------------------------------*\
  File:         menuRfc.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MENURFC_H  // header encapsulation
#define MENURFC_H

#ifdef MENURFC_GLOBALS
    #define MENURFC_VARS_EXT
#else
    #define MENURFC_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <defconst.h>           // for FGC_N_FGEN_CHANS
#include <label.h>              // for struct sym_name

//-----------------------------------------------------------------------------------------------------------

struct menurfc
{
    uint16_t              trigger;
    uint16_t              counter;
    uint16_t *            spinner;
    uint16_t              spinner_idx;
    uint16_t              spinner_addr;
    uint16_t              n_active_chans;
    uint16_t              data[FGC_N_FGEN_CHANS];
    uint16_t              status[FGC_N_FGEN_CHANS];
};

//-----------------------------------------------------------------------------------------------------------

void    MenuRfcShowStatus       (uint16_t argc, char **argv);
void    MenuRfcManualTrig       (uint16_t argc, char **argv);
void    MenuRfcSetData          (uint16_t argc, char **argv);
void    MenuRfcAutoTrig         (uint16_t argc, char **argv);
void    RfcIsrMst               (void);

//-----------------------------------------------------------------------------------------------------------

MENURFC_VARS_EXT        struct menurfc          menurfc;


MENURFC_VARS_EXT        struct sym_name         rfc_stat_lbl[]
#ifdef MENURFC_GLOBALS
= {
    { RFC_STATUS_TRIGGERED_MASK16, "TRIGD"    },
    { RFC_STATUS_DATASENT_MASK16,  "DATASENT" },
    { RFC_STATUS_ACK_MASK16,       "ACK"      },
    { RFC_STATUS_NACK_MASK16,      "NACK"     },
    { RFC_STATUS_LINKTO_MASK16,    "LINK_TO"  },
    { RFC_STATUS_POWERUP_MASK16,   "POWER_UP" },
    { RFC_STATUS_BADCRC_MASK16,    "BAD_CRC"  },
    { 0,                           NULL       }
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // MENURFC_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuRfc.h
\*---------------------------------------------------------------------------------------------------------*/
