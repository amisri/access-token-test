/*---------------------------------------------------------------------------------------------------------*\
  File:         m16c62.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef M16C62_H        // header encapsulation
#define M16C62_H

#ifdef M16C62_GLOBALS
    #define M16C62_VARS_EXT
#else
    #define M16C62_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <label.h>              // for struct sym_name
#include <m16c62_link.h>        // for struct M16C62linkResponseArea

//-----------------------------------------------------------------------------------------------------------

struct data_exchange_with_m16c62
{
    volatile uint16_t                     rsp_bytes_in_buffer;
    union M16C62linkResponseArea        rsp;                            // Response buffer
    uint16_t                              flash_stat;                     // Flash prog/erase status
    uint32_t                              address;                        // Flash address
    uint16_t                              exp_value;                      // Expected value
    uint16_t                              bad_value;                      // Bad value
    uint16_t                              crc_calc;                       // Calculated CRC value
    uint16_t                              crc_code;                       // Code CRC value
    uint32_t                              code_address;                   // Code sector address
    uint8_t                               code_buf[256];                  // Code sector buffer
};

//-----------------------------------------------------------------------------------------------------------

uint16_t  C62PowerOn              (uint16_t bus_initial_value);
uint16_t  C62PowerOff             (void);
uint16_t  C62SwitchProg           (void);
uint16_t  C62SendIsrCmd           (uint16_t cmd, uint16_t arg_count, uint16_t arg_offset, uint8_t *args);
uint16_t  C62SendBgCmd            (uint16_t cmd, uint16_t ms_timeout);
uint16_t  C62FlashCode            (void);
uint16_t  C62SendByte             (uint8_t tx, uint8_t * rx);
uint16_t  C62GetRsp               (uint8_t * buf, uint16_t * max_bytes);

//-----------------------------------------------------------------------------------------------------------

M16C62_VARS_EXT struct data_exchange_with_m16c62        c62;

// don't use const for this as Q.K. need it near (so in RAM), with const will be far and will crash M68H16 code
M16C62_VARS_EXT struct sym_name c62_isr_stat[]
#ifdef M16C62_GLOBALS
= {
    {  M16C62_BUSY,                     "Busy"                  },
    {  M16C62_UNKNOWN_CMD,              "Unknown command"       },
    {  M16C62_BAD_ARG,                  "Bad arg"               },
    {  M16C62_NO_RSP,                   "No response"           },
    { 0 } // the label part == 0  marks the end of the table
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // M16C62_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: m16c62.h
\*---------------------------------------------------------------------------------------------------------*/
