/*---------------------------------------------------------------------------------------------------------*\
  File:         h4irrad.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef H4IRRAD_H       // header encapsulation
#define H4IRRAD_H

#ifdef H4IRRAD_GLOBALS
    #define H4IRRAD_VARS_EXT
#else
    #define H4IRRAD_VARS_EXT extern
#endif

// /!\ Important:
// please declare H4IRRAD to enable power supply control in isr.c

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <stdbool.h>

//-----------------------------------------------------------------------------------------------------------

#define NB_DUT              6

#define MAX_FAULTS          100
#define DUT_STATUS          DIG_IP1_P
#define DUT_CMD             DIG_OP_P

/*----- DAC settings constants ------*/
#define NB_STEPS            20                      // 10 steps between -10/+10V
#define DAC_RANGE           873814                  // (2^20/24)*20, 20 bits from -12 to +12
#define DAC_STEP            DAC_RANGE/NB_STEPS
#define DAC_MIN             -436908                 // -(DAC_RANGE/2 + 1)

/*----- DIG_OP constants ------*/
#define SET_DUT1_MASK       0x80                    // Set DUT 1
#define SET_DUT2_MASK       0x40                    // Set DUT 2
#define SET_DUT3_MASK       0x10                    // Set DUT 3
#define SET_DUT4_MASK       0x04                    // Set DUT 4
#define SET_DUT5_MASK       0x01                    // Set DUT 5
#define SET_DUT6_MASK       0x02                    // Set DUT 6

/*----- DIG_IP1 constants ------*/
#define STATUS_DUT1_MASK    0x1000                  // Set DUT 1
#define STATUS_DUT2_MASK    0x0400                  // Set DUT 2
#define STATUS_DUT3_MASK    0x0100                  // Set DUT 3
#define STATUS_DUT4_MASK    0x0040                  // Set DUT 4
#define STATUS_DUT5_MASK    0x0010                  // Set DUT 5
#define STATUS_DUT6_MASK    0x0004                  // Set DUT 6

/*----- DUT states -----*/
#define NC                  0
#define RUNNING             1
#define RETRY               2
#define FAULT               3

/*----- DUT states -----*/
// #define RETRY_TIME_S                    4  // values for debugging
// #define STEP_WIDTH_S                    4  // values for debugging
#define RETRY_TIME_S                    10
#define STEP_WIDTH_S                    300
#define REFRESH_STATUS_TIME_MS          9
#define REFRESH_SCREEN_TIME_S           3


//-----------------------------------------------------------------------------------------------------------

// DAC function vars
H4IRRAD_VARS_EXT        uint16_t  dac_function;
H4IRRAD_VARS_EXT        int32_t  dac_mV;         // dac output in mV
H4IRRAD_VARS_EXT        int32_t  dac_value;       // dac output
H4IRRAD_VARS_EXT        uint8_t   dac_step_idx;

// DUT cmd & status vars
H4IRRAD_VARS_EXT        uint8_t   dut_cmd_vector;           // Config of used DUT. Set by "Config DUT". Stored between tests
H4IRRAD_VARS_EXT        uint8_t   dut_state[NB_DUT];
H4IRRAD_VARS_EXT        uint8_t   dut_faults_cnt[NB_DUT];
H4IRRAD_VARS_EXT        uint16_t  dut_last_status;
H4IRRAD_VARS_EXT        uint16_t  dut_status;
H4IRRAD_VARS_EXT        bool    h4irrad_run_f;
// scheduling vars
H4IRRAD_VARS_EXT        uint32_t  retry_time[NB_DUT];
H4IRRAD_VARS_EXT        uint32_t  update_fct_time;
H4IRRAD_VARS_EXT        uint8_t   refresh_screen_f;


H4IRRAD_VARS_EXT        char * FAR dac_fct_str[2]
#ifdef H4IRRAD_GLOBALS
= {
        "step",
        "constant"
}
#endif
;

H4IRRAD_VARS_EXT        char * FAR status_str[4]
#ifdef H4IRRAD_GLOBALS
= {
        "NC",
        "OK",
        "HALT",
        "NOK"
}
#endif
;

// DUT mapping
H4IRRAD_VARS_EXT        uint16_t dut_on_mask[NB_DUT]
#ifdef H4IRRAD_GLOBALS
= {
        SET_DUT1_MASK,
        SET_DUT2_MASK,
        SET_DUT3_MASK,
        SET_DUT4_MASK,
        SET_DUT5_MASK,
        SET_DUT6_MASK
}
#endif
;

H4IRRAD_VARS_EXT        uint16_t dut_status_mask[NB_DUT]
#ifdef H4IRRAD_GLOBALS
= {
        STATUS_DUT1_MASK,
        STATUS_DUT2_MASK,
        STATUS_DUT3_MASK,
        STATUS_DUT4_MASK,
        STATUS_DUT5_MASK,
        STATUS_DUT6_MASK
}
#endif
;

//-----------------------------------------------------------------------------------------------------------
void    h4irradInitTest         (void);
void    h4irradADCUpdateStepFct (void);
void    h4irradSupplyControl    (void);


// DAC parameters
H4IRRAD_VARS_EXT        uint32_t  dac_clk;                 // dac clok in KHz
H4IRRAD_VARS_EXT        uint32_t  dac_nbStep;              // dac number of step from 0000 to FFFF
H4IRRAD_VARS_EXT        uint32_t  dac_step_uration;        // dac step duration in s

#endif  // H4IRRAD_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: h4irrad.h
\*---------------------------------------------------------------------------------------------------------*/
