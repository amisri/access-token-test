/*---------------------------------------------------------------------------------------------------------*\
  File:         menu_node.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MENU_NODE_H     // header encapsulation
#define MENU_NODE_H

// just to avoid circular reference between term.h and menu.h

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

struct menu_node
{
    char * FAR          name;
    char * FAR          args_help;
    uint8_t               argc;
    uint8_t               confirm_f;
    uint8_t               fatal_f;
    uint8_t               recurse_f;
    void                (*function)(uint16_t argc, char **argv);
    uint16_t              n_children;
    struct menu_node ** children;
};

//-----------------------------------------------------------------------------------------------------------

#endif  // MENU_NODE_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menu_node.h
\*---------------------------------------------------------------------------------------------------------*/
