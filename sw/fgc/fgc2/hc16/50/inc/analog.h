/*---------------------------------------------------------------------------------------------------------*\
  File:         analog.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ANALOG_H        // header encapsulation
#define ANALOG_H

#ifdef ANALOG_GLOBALS
    #define ANALOG_VARS_EXT
#else
    #define ANALOG_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <label.h>              // for struct sym_name
#include <defconst.h>           // for FGC_INTERFACE_
#include <fgc_codes_gen.h>       // for FGC_CODE_xx_MAINPROG
//-----------------------------------------------------------------------------------------------------------

// Slot5 constants

#define SLOT5_DAC_10V                   450000          // DAC value for about 10V
#define SLOT5_DAC_GAIN                  1024            // DAC gain (1 * 1024)
#define SLTO5_GAIN_DAC_TOL              50              // DAC gain tolerance (~%5)

#define SLOT5_DAC_SAR_TOL               0x00000084      // SAR ADC tolerance for DAC offset reading (about 45mV)
#define SLOT5_DAC_SAR_TOL_REC           0x000000DD      // SAR ADC tolerance for DAC offset reading (about 75mV)
#define SLOT5_DAC_SAR_P2P               0x0007          // SAR ADC tolerance on p2p noise

#define SLOT5_DAC_SD_TOL                0x00030000      // SD ADC tolerance for DAC offset reading (about 45mV)
#define SLOT5_DAC_SD_TOL_REC            0x0006006E      // SD ADC tolerance for DAC offset reading (about 100mV)
#define SLOT5_DAC_SD_P2P                0x0700          // SD ADC tolerance on p2p noise
#define SLOT5_DAC_MS_WR_ON              0x01            // DAC value is sent every ms
#define SLOT5_DAC_MS_WR_OFF             0x00            // DAC value is sent once when changes

#define SLOT5_ADC_SAR_CAL0              0xFFFF8CD0      // SAR ADC value for about -10V
#define SLOT5_ADC_SAR_CAL1              0x00000000      // SAR ADC value for about   0V
#define SLOT5_ADC_SAR_CAL2              0x00007340      // SAR ADC value for about  10V
#define SLOT5_ADC_SAR_TOL               0x00000050      // SAR ADC tolerance
#define SLOT5_ADC_SAR_P2P               0x0007          // SAR ADC tolerance on p2p noise

#define SLOT5_ADC_SD_CAL0               0xFDA67048      // SD ADC value for about -10V
#define SLOT5_ADC_SD_CAL1               0x00000000      // SD ADC value for about   0V
#define SLOT5_ADC_SD_CAL2               0x02582B0A      // SD ADC value for about  10V
#define SLOT5_ADC_SD_TOL                0x00100000      // SD ADC tolerance
#define SLOT5_ADC_SD_P2P                0x0680          // SD ADC tolerance on p2p noise

#define SLOT5_ADC_SD_INDEX_500k         0x0002          // Index of operational 500kHz filter
#define SLOT5_ADC_SD_INDEX_1M           0x4002          // Index of operational 1MHz filter

#define SLOT5_SAMPLES_REF               512             // Nb of samples for reference voltages check
#define SLOT5_SAMPLES_REF_STAB          1024            // Nb of samples for reference voltages stability check

// SD Filter constants

#define SD_FLASH_SIZE                   131072          // Length in bytes of each sigma-delta flash bank
#define SD_DIG_CAL3                     0xFC180000      // ADC result for calibration channel 3
#define SD_DIG_CAL4                     0xFD120000      // ADC result for calibration channel 4
#define SD_DIG_CAL5                     0x00000000      // ADC result for calibration channel 5
#define SD_DIG_CAL6                     0x02EDFFFF      // ADC result for calibration channel 6
#define SD_DIG_CAL7                     0x03E7FFFF      // ADC result for calibration channel 7
#define SD_DIG_CAL11                    0xF82E0000      // ADC result for calibration channel 11
#define SD_DIG_CAL12                    0xFA228000      // ADC result for calibration channel 12
#define SD_DIG_CAL13                    0x00000000      // ADC result for calibration channel 13
#define SD_DIG_CAL14                    0x05DD7FFF      // ADC result for calibration channel 14
#define SD_DIG_CAL15                    0x07D1FFFF      // ADC result for calibration channel 15


//-----------------------------------------------------------------------------------------------------------

// ---- ANA interface structures ----

struct adcs_cal
{
    struct adc_cal_ref
    {
        int32_t          val[3];
    }   adc[2];
};

struct dac_cal
{
    int32_t              val[3];
    uint32_t              stab_threshold;
};

struct dac
{
    int32_t              val;                            // left justified DAC value
    uint16_t              ms_wr;                          // initial value 0: value is not written every ms
};

// SD interface variables

struct sd_flash
{
    uint16_t *addr;
    uint16_t *data;
    uint16_t *datainc;
};

struct menusd
{
    struct  adc_chan                                    // Do not change this structure
    {                                                   // without changing MenuAnaSum()
        uint16_t          raw_h;                          // + 0
        uint16_t          raw_m;                          // + 2
        uint16_t          raw_l;                          // + 4

        uint16_t          min_h;                          // + 6
        uint16_t          min_m;                          // + 8
        uint16_t          min_l;                          // + 10

        uint16_t          max_h;                          // + 12
        uint16_t          max_m;                          // + 14
        uint16_t          max_l;                          // + 16

        uint16_t          p2p_h;                          // + 18
        uint16_t          p2p_m;                          // + 20
        uint16_t          p2p_l;                          // + 22

        uint16_t          sum[4];                         // + 24

        uint16_t          index;                          // + 32

        uint16_t          sar_ave_h;                      // + 34
        uint16_t          sar_ave_m;                      // + 36
        uint16_t          sar_ave_l;                      // + 38
    }   a, b;

    uint16_t              num_samples;
    uint16_t              sar_ave_ct;
    uint16_t              carry;
};

// SD-351 Link variables

struct link
{
    uint32_t              n_rx;
    uint16_t              lk1[16];
    uint16_t              lk2[16];                        // The order of the lk2, tx and rx2 is important
    uint16_t              tx[16];                         // as it is used in the asm function AnaLink()
    uint16_t              rx2[16];
    uint16_t              n_errs[8];
};

//-----------------------------------------------------------------------------------------------------------

void    AnaMpxAna               (uint16_t mpx_a, uint16_t mpx_b);
void    AnaMpxDig               (uint16_t mpx_a, uint16_t mpx_b);
void    AnaFltFrq               (uint16_t frq_a, uint16_t frq_b);
void    AnaInitSD               (uint16_t mpx);
void    AnaGetSingleSample      (void);
void    AnaGetMultiSample       (uint16_t samples);
void    AnaRefAdcs              (uint16_t samples);
void    AnaDac                  (uint16_t samples);
void    AnaDacDataTransfer      (int32_t dac_value, int32_t glitch_val, uint32_t threshold);
void    AnaIsrMst               (void);
void    AnaSum                  (void);
void    AnaSarAverage           (void);
void    AnaPP                   (void *chan_p);
void    AnaLink                 (void);

//-----------------------------------------------------------------------------------------------------------
ANALOG_VARS_EXT struct adcs_cal       adcs_cal;

ANALOG_VARS_EXT struct dac_cal        dac_cal;

ANALOG_VARS_EXT struct dac            dac;

ANALOG_VARS_EXT struct menusd         menusd;

ANALOG_VARS_EXT struct link           link;



ANALOG_VARS_EXT struct sym_name                       slot5_types[]
#ifdef ANALOG_GLOBALS
= {
    {  0,                               "UNKNOWN",              },
    {  FGC_INTERFACE_SAR_400,           "SAR-400",              },
    {  FGC_INTERFACE_SD_350,            "SD-350",               },
    {  FGC_INTERFACE_RFC_500,           "RFC-500",              },
    {  FGC_INTERFACE_SD_351,            "SD-351",               },
    { 0 }
}
#endif
;

ANALOG_VARS_EXT uint8_t                                 slot5_code_ids[]
#ifdef ANALOG_GLOBALS
= {
    FGC_CODE_51_MAINPROG,               // UNKNOWN
    FGC_CODE_51_MAINPROG,               // SAR-400
    FGC_CODE_51_MAINPROG,               // SD-350
    FGC_CODE_59_MAINPROG,               // RFC-500
    FGC_CODE_53_MAINPROG,               // SD-351
    FGC_CODE_51_MAINPROG,               // SD-360
}
#endif
;

ANALOG_VARS_EXT uint8_t                                 slot5_class_ids[]
#ifdef ANALOG_GLOBALS
= {
    51,                                 // UNKNOWN
    51,                                 // SAR-400
    51,                                 // SD-350
    59,                                 // RFC-500
    53,                                 // SD-351
    51,                                 // SD-360
}
#endif
;

ANALOG_VARS_EXT struct sd_flash                       sd_flash_addr[2]
#ifdef ANALOG_GLOBALS
= {
    {
        (uint16_t*)ANA_SD_PG_A_ADDL_16,
        (uint16_t*)ANA_SD_PG_A_DATA_16,
        (uint16_t*)ANA_SD_PG_A_DATAINC_16,
    },
    {
        (uint16_t*)ANA_SD_PG_B_ADDL_16,
        (uint16_t*)ANA_SD_PG_B_DATA_16,
        (uint16_t*)ANA_SD_PG_B_DATAINC_16,
    },
}
#endif
;


//-----------------------------------------------------------------------------------------------------------

#endif  // ANALOG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: analog.h
\*---------------------------------------------------------------------------------------------------------*/
