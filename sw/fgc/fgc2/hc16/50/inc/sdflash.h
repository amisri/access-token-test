/*---------------------------------------------------------------------------------------------------------*\
  File:         sdflash.h

  Contents:     FGC tester - this file declares/defines functions for configuring filter on analog card

  Notes:        500 kHz:  500-368-103-28
                  1 MHz: 1000-736-206-56
                ADS1281: 1000-736-206-59 -> 1000-736-140-125

  History:

    27/08/03    stp     Created
    06/04/04    qak     Renamed filters.h and included directly in menuflash.c
    13/05/04    af      adapted to Fgc tester project
    03/08/04    PFR     0x8000000 value added at the beginning of filter coefficients
    10/03/08    PFR     2 olds filters kept, 6 new ones added (cf. ./filters/readme.txt)
    28/05/08    qak     Definitive filter in slots 0-2 and legacy filter in 3
    28/01/09    qak     ADS1281 function and multipliers added and number of filters reduced to 2 per freq
    29/01/09    qak     Old filters are now full length and prefix includes HALFTAPS
\*---------------------------------------------------------------------------------------------------------*/

#ifndef SDFLASH_H      // header encapsulation
#define SDFLASH_H

#ifdef SDFLASH_GLOBALS
    #define SDFLASH_VARS_EXT
#else
    #define SDFLASH_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs

//-----------------------------------------------------------------------------------------------------------

#define SD_FLTR_LEN_W           0x4000          // 0x2000 samples max per filter function

//-----------------------------------------------------------------------------------------------------------

void    SDFlashSelect                   (uint8_t channel, uint16_t block, uint16_t address);
uint32_t  SDFlashRead                     (uint8_t channel, uint16_t block, uint16_t address);
uint16_t  SDFlashCheck                    (uint8_t channel, uint16_t block, uint16_t address, uint16_t exp_value);
uint16_t  SDFlashCheckBlk                 (uint8_t channel, uint16_t block, uint16_t exp_value);
void    SDFlashErase                    (uint8_t channel);
void    SDFlashMemSet                   (uint16_t value);
void    SDFlashAllFunctions             (void);
uint16_t  SDFlashAllFunctionsCheck        (void);
uint16_t  SDFlashFunction                 (uint16_t block, uint16_t addr, int16_t multiplier, int32_t * FAR function, uint16_t length);
uint16_t  SDFlashFunctionCheck            (uint16_t block, uint16_t addr, int16_t multiplier, int32_t * FAR function_p, uint16_t length);
uint16_t  SDFlashProgAB                   (uint16_t block, uint16_t address, uint16_t value);
uint16_t  SDFlashWrite                    (uint8_t channel, uint16_t block, uint16_t address, uint16_t value);
uint16_t  SDFlashReset                    (uint8_t channel);

//-----------------------------------------------------------------------------------------------------------

// Filter functions for sigma-delta

// 500kHz filters

SDFLASH_VARS_EXT const FAR int32_t              sd_func0[998]
#ifdef SDFLASH_GLOBALS
= {
    #include <fir_500k_500-368-103-28.txt>
}
#endif
;

SDFLASH_VARS_EXT const FAR int32_t              sd_func1[998]
#ifdef SDFLASH_GLOBALS
= {
    #include <fir_500k_bh.txt>
}
#endif
;

// 1MHz filters

SDFLASH_VARS_EXT const FAR int32_t              sd_func2[1998]
#ifdef SDFLASH_GLOBALS
= {
    #include <fir_1M_1000-736-206-56.txt>
}
#endif
;

SDFLASH_VARS_EXT const FAR int32_t              sd_func3[1996]
#ifdef SDFLASH_GLOBALS
= {
    #include <fir_1M_1250-250-250-250.txt>
}
#endif
;

// ADS1281 filter

SDFLASH_VARS_EXT const FAR int32_t              sd_func1281[1000]
#ifdef SDFLASH_GLOBALS
= {
    #include <fir_test_1281_mxgain49-1000-736-206-57-_int.txt>
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // SDFLASH_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sdflash.h
\*---------------------------------------------------------------------------------------------------------*/
