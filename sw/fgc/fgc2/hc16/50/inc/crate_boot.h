/*---------------------------------------------------------------------------------------------------------*\
  File:         crate_boot.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CRATE_BOOT_H    // header encapsulation
#define CRATE_BOOT_H

#ifdef CRATE_BOOT_GLOBALS
    #define CRATE_BOOT_VARS_EXT
#else
    #define CRATE_BOOT_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <label.h>      // for struct sym_name
#include <defconst.h>   // for FGC_CRATE_TYPE_
//-----------------------------------------------------------------------------------------------------------

struct crate_vars
{
    uint16_t      type;               // Crate code number
    uint8_t       side;               // 'L' or 'R'
};

struct TCrateModelInfo
{
    uint16_t          type_code;
    char * FAR      label;                  // Far pointer to label (string constant)
    uint16_t          software_class;
};

//-----------------------------------------------------------------------------------------------------------

CRATE_BOOT_VARS_EXT struct crate_vars           crate;

CRATE_BOOT_VARS_EXT        struct sym_name        crate_types[]
#ifdef CRATE_BOOT_GLOBALS
= {
    {  31,                              "Unknown"               },
    {  FGC_CRATE_TYPE_PC_60A,           "60A"                   },
    {  FGC_CRATE_TYPE_PC_120A,          "120A"                  },
    {  FGC_CRATE_TYPE_PC_600A,          "600A"                  },
    {  FGC_CRATE_TYPE_PC_KA,            "kA"                    },
    {  FGC_CRATE_TYPE_RF_FGEN,          "RF FuncGen"            },
    {  FGC_CRATE_TYPE_PC_KLYSTRON,      "RF Klystron"           },
    {  FGC_CRATE_TYPE_PC_THYRISTOR2,    "Thyristor-2"           },
    {  FGC_CRATE_TYPE_PC_THYRISTOR4,    "Thyristor-4"           },
    {  FGC_CRATE_TYPE_PC_THYRISTOR6,    "Thyristor-6"           },
    {  FGC_CRATE_TYPE_PC_IN_TRIPLET,    "InnerTriplet"          },
    {  FGC_CRATE_TYPE_PC_AUX_PS,        "AuxPS"                 },
    {  FGC_CRATE_TYPE_PC_POPS,          "POPS"                  },
    {  FGC_CRATE_TYPE_HOT_SPARE,        "HotSpare"              },
    {  FGC_CRATE_TYPE_TESTER,           "Tester"                },
    {  FGC_CRATE_TYPE_RECEPTION,        "Reception"             },
    { 0 }
}
#endif
;
//-----------------------------------------------------------------------------------------------------------

#endif  // CRATE_BOOT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: crate_boot.h
\*---------------------------------------------------------------------------------------------------------*/
