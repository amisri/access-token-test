/*---------------------------------------------------------------------------------------------------------*\
  File:         main.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MAIN_H  // header encapsulation
#define MAIN_H

#ifdef MAIN_GLOBALS
    #define MAIN_VARS_EXT
#else
    #define MAIN_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>           // basic typedefs
//#define H4IRRAD

//-----------------------------------------------------------------------------------------------------------

void            RunMenus                (void);
void            CheckRegisters          (void);
void            CheckSlot5Registers     (void);
void            DetectCauseOfStartup    (void);
void            InitShareMem            (void);
void            InitGpt                 (void);
void            StartBoot               (void);
void            SelfTest                (void);
void            ReportBoot              (void);
void            ReportFip               (void);
void            CheckStatus             (void);
uint16_t          CheckSlot5IfNot         (uint16_t type0, uint16_t type1);
uint16_t          CheckSlot5If            (uint16_t type);
void            RunMainProg             (void);

//-----------------------------------------------------------------------------------------------------------

// ATTENTION timeout_ms is a global variable decremented in the 1ms tick
// and used by TermGetCh() and by CodesUpdateGetBlock()
MAIN_VARS_EXT volatile uint16_t                   timeout_ms;             // Millisecond timeout down counter

//-----------------------------------------------------------------------------------------------------------

#endif  // MAIN_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
