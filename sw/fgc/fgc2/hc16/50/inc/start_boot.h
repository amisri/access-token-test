/*---------------------------------------------------------------------------------------------------------*\
  File:         start_boot.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef START_BOOT_H    // header encapsulation
#define START_BOOT_H

#ifdef START_BOOT_GLOBALS
    #define START_BOOT_VARS_EXT
#else
    #define START_BOOT_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#pragma NO_STRING_CONSTR        // Stop #xxx in macro from making a string literal "xxx" (needed for asm)

//-----------------------------------------------------------------------------------------------------------

#pragma CODE_SEG SWITCH
void StartSwitchBoot            (void);
#pragma CODE_SEG DEFAULT

//-----------------------------------------------------------------------------------------------------------

#endif  // START_BOOT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: start_boot.h
\*---------------------------------------------------------------------------------------------------------*/
