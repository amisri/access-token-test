/*---------------------------------------------------------------------------------------------------------*\
  File:         menuRunlog.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MENURUNLOG_H  // header encapsulation
#define MENURUNLOG_H

#ifdef MENURUNLOG_GLOBALS
    #define MENURUNLOG_VARS_EXT
#else
    #define MENURUNLOG_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

void    MenuRunlogShowPrevRun   (uint16_t argc, char **argv);
void    MenuRunlogShowNewRun    (uint16_t argc, char **argv);
void    MenuRunlogShowNext      (uint16_t argc, char **argv);
void    MenuRunlogShowNew       (uint16_t argc, char **argv);
void    MenuRunlogShowAll       (uint16_t argc, char **argv);
uint8_t * MenuRunlogShow          (uint8_t *outp, uint8_t *id_p);

//-----------------------------------------------------------------------------------------------------------

MENURUNLOG_VARS_EXT        uint8_t *      runlog_outp;            // Runlog output pointer

//-----------------------------------------------------------------------------------------------------------

#endif  // MENURUNLOG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuRunlog.h
\*---------------------------------------------------------------------------------------------------------*/
