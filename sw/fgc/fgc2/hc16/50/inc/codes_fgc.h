/*---------------------------------------------------------------------------------------------------------*\
  File:         codes_fgc.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CODES_FGC_H     // header encapsulation
#define CODES_FGC_H

#ifdef CODES_FGC_GLOBALS
    #define CODES_FGC_VARS_EXT
#else
    #define CODES_FGC_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <codes_holder.h>

// needed when CODES_GLOBALS
#include <codes_mcu.h>          // for EraseMcuBootDev(), EraseMcuDev(), WriteMcuDev(), WriteMcuBootDev(), CheckMcuDev()
#include <codes_m16c62.h>       // for EraseC62Dev(), CheckC62Dev(), WriteM16C62dev()

//-----------------------------------------------------------------------------------------------------------

/*!
 * @brief Specifies FGC code visibility
 */
enum CodesVisibility
{
    CODES_VISIBILITY_MCU,       //! Code is visible in MCU's flash
    CODES_VISIBILITY_M16C62,    //! Code is visible for M16C62
    CODES_VISIBILITY_NONE,      //! Invalid code, not visible at all
};

void    CodesUpdateLast         (void);

/*!
 * @brief Checks for which processor a code is visible
 *
 * @param[in] code_id Code for which the function should check the visibility
 *
 * @return Code visibility range
 */
enum CodesVisibility CodesGetCodeVisibility(enum fgc_code_ids_short_list code_id);

/*!
 * @brief Returns a pointer to code version in the shared memory
 *
 * @param[in] code_id Code for which the function should return the pointer
 *
 * @return Pointer to code version
 */
uint32_t * CodesGetCodeVersionPointer(enum fgc_code_ids_short_list code_id);

//-----------------------------------------------------------------------------------------------------------

CODES_FGC_VARS_EXT uint16_t           (* erase_dev[SHORT_LIST_CODE_LAST])(enum fgc_code_ids_short_list dev_idx)                         // Erase flash device functions
#ifdef CODES_FGC_GLOBALS
= {
    EraseMcuDev,                                        // SHORT_LIST_CODE_MP_DSP
    EraseMcuDev,                                        // SHORT_LIST_CODE_BT
    EraseMcuDev,                                        // SHORT_LIST_CODE_DIMDB
    EraseMcuDev,                                        // SHORT_LIST_CODE_COMPDB
    EraseMcuDev,                                        // SHORT_LIST_CODE_NAMEDB
    EraseMcuDev,                                        // SHORT_LIST_CODE_SYSDB
    EraseMcuDev,                                        // SHORT_LIST_CODE_BTST
    EraseC62Dev,                                        // SHORT_LIST_CODE_PTDB
    EraseC62Dev,                                        // SHORT_LIST_CODE_IDPROG
    EraseC62Dev                                         // SHORT_LIST_CODE_IDDB
}
#endif
;

CODES_FGC_VARS_EXT uint16_t           (* program_dev[SHORT_LIST_CODE_LAST])(enum fgc_code_ids_short_list dev_idx, struct code_block *)  // Program flash device functions
#ifdef CODES_FGC_GLOBALS
= {
    ProgramMcuDev,                                      // SHORT_LIST_CODE_MP_DSP
    ProgramMcuDev,                                      // SHORT_LIST_CODE_BT
    ProgramMcuDev,                                      // SHORT_LIST_CODE_DIMDB
    ProgramMcuDev,                                      // SHORT_LIST_CODE_COMPDB
    ProgramMcuDev,                                      // SHORT_LIST_CODE_NAMEDB
    ProgramMcuDev,                                      // SHORT_LIST_CODE_SYSDB
    ProgramMcuDev,                                      // SHORT_LIST_CODE_BTST
    ProgramC62Dev,                                      // SHORT_LIST_CODE_PTDB
    ProgramC62Dev,                                      // SHORT_LIST_CODE_IDPROG
    ProgramC62Dev                                       // SHORT_LIST_CODE_IDDB
}
#endif
;


CODES_FGC_VARS_EXT uint16_t           (* check_dev[SHORT_LIST_CODE_LAST])(enum fgc_code_ids_short_list dev_idx) // Check code in flash device functions
#ifdef CODES_FGC_GLOBALS
= {
    CheckMcuDev,                                        // SHORT_LIST_CODE_MP_DSP
    CheckMcuDev,                                        // SHORT_LIST_CODE_BT
    CheckMcuDev,                                        // SHORT_LIST_CODE_DIMDB
    CheckMcuDev,                                        // SHORT_LIST_CODE_COMPDB
    CheckMcuDev,                                        // SHORT_LIST_CODE_NAMEDB
    CheckMcuDev,                                        // SHORT_LIST_CODE_SYSDB
    CheckMcuDev,                                        // SHORT_LIST_CODE_BTST
    CheckC62Dev,                                        // SHORT_LIST_CODE_PTDB
    CheckC62Dev,                                        // SHORT_LIST_CODE_IDPROG
    CheckC62Dev                                         // SHORT_LIST_CODE_IDDB
}
#endif
;
//-----------------------------------------------------------------------------------------------------------

#endif  // CODES_FGC_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_fgc.h
\*---------------------------------------------------------------------------------------------------------*/
