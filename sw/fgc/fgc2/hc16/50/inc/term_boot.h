/*---------------------------------------------------------------------------------------------------------*\
  File:         term_boot.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef TERM_BOOT_H      // header encapsulation
#define TERM_BOOT_H

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>               // basic typedefs
#include <stdio.h>                  // for FILE
#include <definfo.h>

//-----------------------------------------------------------------------------------------------------------

void    EnableTerminalStream(void);

void    EnableQSPIbus           (void);

void    TermRxCh                (uint8_t ch);
uint16_t  TermRxFlush             (void);
uint8_t   TermGetCh               (uint16_t to_ms);
void    TermPutc                (char ch, FILE *f);
void    TermProcessHardware     (void);
void    TermTxCh                (uint8_t ch);

//-----------------------------------------------------------------------------------------------------------

#endif  // TERM_BOOT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sci_boot.h
\*---------------------------------------------------------------------------------------------------------*/
