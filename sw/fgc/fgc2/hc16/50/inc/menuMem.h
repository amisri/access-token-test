/*---------------------------------------------------------------------------------------------------------*\
  File:         menuMem.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MENUMEM_H  // header encapsulation
#define MENUMEM_H

#ifdef MENUMEM_GLOBALS
    #define MENUMEM_VARS_EXT
#else
    #define MENUMEM_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

void    MenuMemHc16Read                 (uint16_t argc, char **argv);
void    MenuMemHc16Pattern              (uint16_t argc, char **argv);
void    MenuMemHc16Address              (uint16_t argc, char **argv);
void    MenuMemC32Read                  (uint16_t argc, char **argv);
void    MenuMemC32Pattern               (uint16_t argc, char **argv);
void    MenuMemC32Address               (uint16_t argc, char **argv);
void    MenuMemDisplaySBEcounters       (uint16_t argc, char **argv);

//-----------------------------------------------------------------------------------------------------------

// don't use const for this as Q.K. need it near (so in RAM), with const will be far and will crash H16 code
MENUMEM_VARS_EXT        uint16_t  patterns_16[]
#ifdef MENUMEM_GLOBALS
= {
        0x0000,
        0xFFFF,
        0x5A5A,
        0xA5A5
}
#endif
;

// don't use const for this as Q.K. need it near (so in RAM), with const will be far and will crash H16 code
MENUMEM_VARS_EXT        uint16_t  num_patterns_16
#ifdef MENUMEM_GLOBALS
= sizeof(patterns_16) / sizeof(uint16_t)
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // MENUMEM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuMem.h
\*---------------------------------------------------------------------------------------------------------*/
