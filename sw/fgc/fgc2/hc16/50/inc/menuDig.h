/*---------------------------------------------------------------------------------------------------------*\
  File:         menuDig.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MENUDIG_H  // header encapsulation
#define MENUDIG_H

#ifdef MENUDIG_GLOBALS
    #define MENUDIG_VARS_EXT
#else
    #define MENUDIG_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

#define DIG_TIMEOUT_MAX                 300             // Dig Timeout max in ms
#define DIG_TIMEOUT_MIN                 100             // Dig Timeout min in ms

//-----------------------------------------------------------------------------------------------------------

void    MenuDigGet      (uint16_t argc, char **argv);
void    MenuDigSet      (uint16_t argc, char **argv);
void    MenuDigIP1      (uint16_t argc, char **argv);
void    MenuDigIP2      (uint16_t argc, char **argv);
void    MenuDigOP       (uint16_t argc, char **argv);
void    MenuDigTimeout  (uint16_t argc, char **argv);

//-----------------------------------------------------------------------------------------------------------
// don't use const for this as Q.K. need it near (so in RAM), with const will be far and will crash H16 code
MENUDIG_VARS_EXT        uint8_t   patterns_8[]
#ifdef MENUDIG_GLOBALS
= {
        0x00,
        0xFF,
        0x55,
        0xAA
}
#endif
;


// don't use const for this as Q.K. need it near (so in RAM), with const will be far and will crash H16 code
MENUDIG_VARS_EXT        uint16_t  num_patterns_8
#ifdef MENUDIG_GLOBALS
= sizeof(patterns_8)
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // MENUDIG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuDig.h
\*---------------------------------------------------------------------------------------------------------*/
