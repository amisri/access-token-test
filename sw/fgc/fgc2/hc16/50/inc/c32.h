/*---------------------------------------------------------------------------------------------------------*\
  File:         c32.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef C32_H  // header encapsulation
#define C32_H

#ifdef C32_GLOBALS
    #define C32_VARS_EXT
#else
    #define C32_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>           // basic typedefs


#define C32_ALIVE                       0x01            // Mask for top byte only (used with BRSET/BRCLR)
#define C32_STATUS_HC16_SBE             0x0010
#define C32_STATUS_C32_SBE              0x0020
#define C32_STATUS_PATTERN_FAULT        0x0040
#define C32_STATUS_ADDRESS_FAULT        0x0080


#define C32_STATUS_FAILED_TO_START      0x0100
#define C32_STATUS_BG_FAILED            0x0200
#define C32_STATUS_MEM_FAULT            0x0400
#define C32_STATUS_STANDALONE           0x0800

// FIFO test constants

#define FIFO_PORT_CTRL                  0x00000222
#define FIFO_DX                         0x00000040
#define FIFO_FSX                        0x00000400
#define FIFO_CLX                        0x00000004
#define FIFO_TEST_LINK                  0x00001000

#define H4IRRAD_DAC_TEST                0x00002000

//-----------------------------------------------------------------------------------------------------------

struct TDSP_info
{
    uint32_t *            bl_data;                        // pointer to data for bootloader
    uint16_t              is_running;                     // Dsp is running flag
    uint16_t              status;                         // Dsp status, not used by FGC3 boot
    uint16_t              bg_counter;                     // Low word of bg counter
    uint16_t              irq_mask;                       // IRQ trigger mask
    uint16_t              irq_fail;                       // IRQ fail mask
};

//-----------------------------------------------------------------------------------------------------------

uint16_t  DspLoadProgramAndRun            (void);
uint16_t  StopDsp                         (void);
void    RefreshStatusWithDspState       (void);

//-----------------------------------------------------------------------------------------------------------

C32_VARS_EXT struct TDSP_info     dsp;                            // C32 global structure

//-----------------------------------------------------------------------------------------------------------

#endif  // C32_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: c32.h
\*---------------------------------------------------------------------------------------------------------*/
