/*---------------------------------------------------------------------------------------------------------*\
  File:     codes.h

  Purpose:

\*---------------------------------------------------------------------------------------------------------*/

#ifndef CODES_H // header encapsulation
#define CODES_H

#ifdef CODES_GLOBALS
    #define CODES_VARS_EXT
#else
    #define CODES_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>       // basic typedefs
#include <fgc_consts_gen.h> // Global FGC constants
#include <codes_holder.h>

//-----------------------------------------------------------------------------------------------------------

#define CODE_TIMEOUT_INIT   40000       // Initial timeout: 40 seconds
#define CODE_TIMEOUT_RUN    2000        // Running timeout: 2 seconds
#define CODE_SIZEOF_BLOCK   36          // Used in asm so must be numeric (sizeof doesn't work)

#define CODE_RX_NONE        0           // code.rx_mode
#define CODE_RX_FIELDBUS    1
#define CODE_RX_TERMINAL    2

//-----------------------------------------------------------------------------------------------------------


struct code
{
    uint16_t                                  running_dev_idx;    // Device index for running code
    uint32_t                                  request_mask;       // Code request mask (Gateway slots 0-31)
    uint16_t                                  rx_active_f;        // Reception in progress flag
    uint16_t                                  rx_dev_idx;         // Device being programmed
    uint16_t                                  rx_mode;            // Reception mode: FIELDBUS or TERMINAL. If None, code vars are discarded
    uint16_t                                  rx_term_idx;        // Terminal reception index within block (0-35)
    uint16_t                                  rx_prev_blk_idx;    // Previous block index for received code
    uint16_t                                  rx_last_blk_idx;    // Last block index for received code
    uint16_t                                  rx_class_id;        // Reception code class
    uint16_t                                  rx_code_id;         // Reception code id
    uint16_t                                  n_blks_rcvd;        // Number of blocks received
    uint16_t                                  n_blks_saved;       // Number of blocks saved
    struct code_block * volatile            in_blk;             // Block in pointer
    // volatile because is used in a while(), function CodesUpdateGetBlock()
    // and at that moment is externally modified by FipReadCodeVar() triggered by FipIsr()
    struct code_block *                     next_in_blk;        // Next block in pointer
    struct code_block * volatile            out_blk;            // Block out pointer
    // volatile because is used in a while(), function CodesUpdateGetBlock()
    // and at that moment is externally modified by FipReadCodeVar() triggered by FipIsr()
    struct advertised_code_info volatile    adv[FGC_CODE_MAX_SLOTS];    // Advertised code information per GW slot
    // TODO Move state into adv array?
    uint16_t                                  state[FGC_CODE_MAX_SLOTS];  // State of each advertised codes
};

//-----------------------------------------------------------------------------------------------------------

void    CodesInit           (void);
void    CodesInitAdvTable   (void);
void    CodesCheckUpdates   (void);
void    CodesUpdate         (uint16_t rx_mode);
uint16_t  CodesUpdateGetBlock (void);
void    CodesUpdateFirst    (void);
void    CodesUpdateBlock    (uint16_t blk_idx);

//-----------------------------------------------------------------------------------------------------------

CODES_VARS_EXT struct code      code;

//-----------------------------------------------------------------------------------------------------------

#endif  // CODES_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes.h
\*---------------------------------------------------------------------------------------------------------*/
