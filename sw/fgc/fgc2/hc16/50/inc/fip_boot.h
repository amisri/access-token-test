/*---------------------------------------------------------------------------------------------------------*\
  File:     fip_boot.h

  Purpose:  FGC

\*---------------------------------------------------------------------------------------------------------*/

#ifndef FIP_BOOT_H  // header encapsulation
#define FIP_BOOT_H

#ifdef FIP_BOOT_GLOBALS
    #define FIP_BOOT_VARS_EXT
#else
    #define FIP_BOOT_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>       // basic typedefs
#include <defconst.h>

//-----------------------------------------------------------------------------------------------------------

#define FIP_INIT_TIMEOUT    2000        // FipInit period when not connected (ms)

//-----------------------------------------------------------------------------------------------------------

struct TFipBoot
{
    uint16_t          stadr;              // Fip address read from uFIP STADR register
    uint16_t          init_timeout;       // 100ms FIP init timeout counter
};

//-----------------------------------------------------------------------------------------------------------
void    FipInit         (void);
void    FipTxCh         (char ch);
void    FipISR          (void);
void    FipWaitLinkUp   (void);
// void MemCpyFip       (const void *     to,   const void *  from,  uint16_t       len); // in mem.c now
//-----------------------------------------------------------------------------------------------------------
void    FipReadTimeVar  (void);
void    FipReadCodeVar  (void);
void    FipWriteStatVar (void);
//--------------------------------------------------------------------

FIP_BOOT_VARS_EXT struct TFipBoot       fip;


// the following one is for local use of fip_boot.c and resides in it
// can't be on fip.h due to visibility problems from Stephen include files fgc_fip_....

// it is used by FipReadCodeVar() which is is called from IsrFip() ISR

// FIP_BOOT_VARS_EXT struct fgc_fip_code    local_fip_code_var;     // FIP code variable

//-----------------------------------------------------------------------------------------------------------

#endif  // FIP_BOOT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: fip_boot.h
\*---------------------------------------------------------------------------------------------------------*/
