/*---------------------------------------------------------------------------------------------------------*\
 File:          mcu_time_consumed.h

 Purpose:       structures declarations

 Notes:

    28 jun 2010 doc     Created

    Sub clock structures
    maps the registers existing in the PLD to control several clock outputs


\*---------------------------------------------------------------------------------------------------------*/

#ifndef MCU_TIME_CONSUMED_H     // header encapsulation
#define MCU_TIME_CONSUMED_H

#ifdef MCU_TIME_CONSUMED_GLOBALS
    #define MCU_TIME_CONSUMED_VARS_EXT
#else
    #define MCU_TIME_CONSUMED_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>
//-----------------------------------------------------------------------------------------------------------

struct TMcuTimeConsumeInfo
{
    uint16_t              fip_pll_us;                 // fip pll it time (us)
    uint16_t              fip_pll_us_max;         // fip pll it time (us) max
};

//-----------------------------------------------------------------------------------------------------------

    MCU_TIME_CONSUMED_VARS_EXT struct TMcuTimeConsumeInfo   mcu_time_consumed_in;

//-----------------------------------------------------------------------------------------------------------

#endif  // MCU_TIME_CONSUMED_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mcu_time_consumed.h
\*---------------------------------------------------------------------------------------------------------*/
