/*---------------------------------------------------------------------------------------------------------*\
  File:         flash.h

  Purpose:      FGC2

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef FLASH_H // header encapsulation
#define FLASH_H

#ifdef FLASH_GLOBALS
    #define FLASH_VARS_EXT
#else
    #define FLASH_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

#define FLS_TIMEOUT_MS                  13000           // Flash erase/program timeout (13 seconds)

//-----------------------------------------------------------------------------------------------------------

void    FlashReset              (uint32_t address);
uint16_t  FlashChipErase          (uint32_t address);
uint16_t  FlashSectorErase        (uint32_t address);
uint16_t  FlashMemSet             (uint32_t address, uint16_t value, uint32_t n_words);
uint16_t  FlashProgram            (uint32_t address, uint16_t value);
uint16_t  FlashCheck              (uint32_t address, uint16_t value);

//-----------------------------------------------------------------------------------------------------------

#endif  // FLASH_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: flash.h
\*---------------------------------------------------------------------------------------------------------*/
