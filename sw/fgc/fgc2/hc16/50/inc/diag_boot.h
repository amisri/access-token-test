/*---------------------------------------------------------------------------------------------------------*\
  File:     diag_boot.h

  Purpose:  FGC3 Boot


 The DIM register is 16 bits info composed
    tiii dddd dddd dddd


    t : trigger
    iii : ID
    dddddddddddd :  data

\*---------------------------------------------------------------------------------------------------------*/

#ifndef DIAG_BOOT_H // header encapsulation
#define DIAG_BOOT_H

#ifdef DIAG_BOOT_GLOBALS
    #define DIAG_BOOT_VARS_EXT
#else
    #define DIAG_BOOT_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>       // basic typedefs

#include <label.h>          // for struct sym_name

#include <definfo.h>        // For FGC_PLATFORM_ID
//-----------------------------------------------------------------------------------------------------------

#define DIAG_VMEAS1         0       // Diag analogue channels indexes...
#define DIAG_VMEAS10        1
#define DIAG_PSU5           2
#define DIAG_PSU15P         3
#define DIAG_PSU15N         4
#define DIAG_ANA5           5
#define DIAG_ANA15P         6
#define DIAG_ANA15N         7

#define DIAG_BUF_LEN        13      // Max number of all channels

#define DIAG_TRIGGER        0x8000  // Trigger scan bit in  QSM_SPCR1
#define DIAG_MAX_RESETS     10      // Max resets before state changes to FAILED

#define DIAG_N_ANA          8       // Number of analogue channels

#define DIAG_OFF            0       // No scan
#define DIAG_RESET          1       // Diag Reset
#define DIAG_START          2       // End of reset
#define DIAG_SCAN           3       // Start scan
#define DIAG_RUN            4       // Scanning
#define DIAG_FAILED         5       // Failed

#define DIAG_STATUS_A       8
#define DIAG_STATUS_B       9

#define DIAG_DATA_A         11
#define DIAG_DATA_B         12

//-----------------------------------------------------------------------------------------------------------

// ToDo: decouple channels A and B to handle the buses independently
// and not to stop bus A if B is faulty or viceversa

struct TDiagnosticBusInfo
{
                    // Boot: ISR calls QSPIbusStateMachine() which update it,
                    // and I use it in while() to check when QSPIbusStateMachine() modifies it
volatile    uint16_t      state;          // Diag control: DIAG_OFF, DIAG_START, DIAG_RUN
            uint16_t      n_resets;       // Number of resets
            uint16_t      check_f;        // Run diag check flag
            uint32_t      ana_check_time; // Unix time of last analogue check
            uint16_t      qspi_branch;    // branch being accessed 0=A,1=B
            uint16_t      scan_idx;       // Scan number (0-6) this is the ordinal of DIM internal register we are theorically reading
            uint16_t      data[DIAG_BUF_LEN]; // diagnostic data buffer
        /*
            .data[] = diagnostic data buffer will be filled like this

               0 register ID:4   Ana0 A
               1 register ID:4   Ana0 B
               2 register ID:5   Ana1 A
               3 register ID:5   Ana1 B
               4 register ID:6   Ana2 A
               5 register ID:6   Ana2 B
               6 register ID:7   Ana3 A
               7 register ID:7   Ana3 B
               8 register ID:0   Dig0 A or B
               9 register ID:1   Dig1 A or B
              10 register ID:2   TrigCounter A or B
                 ----------------------------------- filled during the normal reception
              11 register ID:1   Dig1 A
              12 register ID:1   Dig1 B
                 ----------------------------------- filled at the end of the reception
        */
        uint16_t      n_dims[2];          // Number of DIMs detected on each branch [A,B]
        uint16_t      reception_buffer[2][16];          // Copy of QSM receive ram
        FP32        ana[DIAG_N_ANA];    // Analogue data
};
//-----------------------------------------------------------------------------------------------------------

void    Check_FGC2_PSU      (uint16_t psu_stat);
void    QSPIbusStateMachine (void);
void    DiagCheck           (void);
void    DiagCheckBus        (void);
void    DiagCheckAna        (void);

//-----------------------------------------------------------------------------------------------------------

DIAG_BOOT_VARS_EXT struct TDiagnosticBusInfo    diag_bus;       // Diag variables structure

// don't use const for this as Q.K. need it near (so in RAM), with const will be far and will crash H16 code
DIAG_BOOT_VARS_EXT struct sym_name      diag_ctrl_lbl[]
#ifdef DIAG_BOOT_GLOBALS
= {
    {  DIAG_OFF,            "OFF"           },
    {  DIAG_RESET,          "RESET"         },
    {  DIAG_START,          "START"         },
    {  DIAG_SCAN,           "SCAN"          },
    {  DIAG_RUN,            "RUN"           },
    {  DIAG_FAILED,         "FAILED"        },
    { 0 } // the label part == 0  marks the end of the table
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // DIAG_BOOT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: diag_boot.h
\*---------------------------------------------------------------------------------------------------------*/
