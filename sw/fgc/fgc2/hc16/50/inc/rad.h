/*---------------------------------------------------------------------------------------------------------*\
  File:         rad.h

  Contents:


  History:

\*---------------------------------------------------------------------------------------------------------*/

#ifndef RAD_H      // header encapsulation
#define RAD_H

#ifdef RAD_GLOBALS
    #define RAD_VARS_EXT
#else
    #define RAD_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>   // basic typedefs
#include <debug.h>

//-----------------------------------------------------------------------------------------------------------

#define RAD_WRITE               0                       // action on the internal register : rewrite the value
#define RAD_RESET               1                       // action on the internal register : reset WFIP

#define STUCK_FLT               1                       // Stuck fault
#define P2P_AVE_FLT             2                       // p2p or average fault
#define TAKE_POINT              3                       // data save before a reset
#define JUMP_FLT                4                       // Jump fault

#define TX_RX_MSG_FIP           256                     // TXDMSG and RXDMSG size


#define ANA_SAMPLES             4096                    // number of samples for ANA test
#define MAX_ADC_ARRAY_SIZE      10                      // DAC and +10V/-10V array size
#define RAM_WRITE_VALUE         0x0000                  // default value writes into the RAM

// constants SD350

#define ADC_P2P_10V_TOL_SD      0x0680                  // p2p tolerance for -10V and +10V
#define ADC_AVE_10V_TOL_SD      0x00100000              // average tolerance for -10V and +10V

#define V10_JUMP_TOL_SD         0x07CC                  // min difference accepted to detect a jump on +/-10V


#define ADC_P2P_DAC_TOL_SD      0x0700                  // p2p tolerance for DAC
#define ADC_AVE_DAC_TOL_SD      0x00100000              // average tolerance for DAC

#ifdef DEBUG_NO_ADC_RESET
    // debug value to force ADC card reset more often
    #define     DAC_JUMP_TOL_SD         0x0055                  // min difference accepted to detect a jump on DAC
#else
    // original value
    #define     DAC_JUMP_TOL_SD         0x0866                  // min difference accepted to detect a jump on DAC
#endif

#define ADC_P2P_DAC_TOL_SD      0x0700                  // p2p tolerance for DAC
#define ADC_AVE_DAC_TOL_SD      0x00100000              // average tolerance for DAC


#define NB_SAMPLE_STUCK_SD      5                       // Number of identical samples before the stuck flag for SD card

// constants SD360

#define ADC_P2P_10V_TOL_SD360   0x8500                  // p2p tolerance for -10V and +10V (~200uV)
#define ADC_AVE_10V_TOL_SD360   0x00100000              // average tolerance for -10V and +10V (~ 6mV)
#define V10_JUMP_TOL_SD360      0xC000                  // min difference accepted to detect a jump on +/-10V  (~280uV)
#define ADC_P2P_DAC_TOL_SD360   0x8500                  // p2p tolerance for DAC (~200uV)
#define ADC_AVE_DAC_TOL_SD360   0x00100000              // average tolerance for DAC  (~ 6mV)
#define DAC_JUMP_TOL_SD360      0xC000                  // min difference accepted to detect a jump on DAC  (~280uV)
#define NB_SAMPLE_STUCK_SD360   5                       // Number of identical samples before the stuck flag for SD card

// constants SAR
#define ADC_P2P_10V_TOL_SAR     0x0007                  // p2p tolerance for -10V and +10V
#define ADC_AVE_10V_TOL_SAR     0x00000050              // average tolerance for -10V and +10V
#define V10_JUMP_TOL_SAR        0x000E                  // min difference accepted to detect a jump on +/-10V
#define ADC_P2P_DAC_TOL_SAR     0x0007                  // p2p tolerance for DAC
#define ADC_AVE_DAC_TOL_SAR     0x000000DD              // average tolerance for DAC
#define DAC_JUMP_TOL_SAR        0x000E                  // min difference accepted to detect a jump on DAC
#define NB_SAMPLE_STUCK_SAR     120                     // Number of identical samples before the stuck flag for SAR card

// "Data to send" constants

#define SEND_10V                0
#define SEND_DAC                1
#define SEND_HC16_WFIP_REGISTER 2
#define SEND_C32_REGISTER       3
#define SEND_SBE                4
#define SEND_INT_RAM_HC16       5
#define SEND_INT_RAM_C32        6
#define SEND_ALL                7


struct hc16_reg
{
    uint32_t      addr;                   // 20 bit address of register
    uint16_t      size;                   // Size in bytes
    uint16_t      action;                 // Action to take if corrupted
    uint16_t      value;                  // value in the register
    uint16_t      state;                  // Status of the register. if broken during the test it's equal to 1
};

struct adc_10V_value
{
    uint32_t      unix_time;                      // unix time
    uint32_t      p2p_10Vpos;                     // p2p value for +10V
    int32_t      ave_10Vpos;                     // average value for +10V
    uint32_t      p2p_10Vneg;                     // p2p value for -10V
    int32_t      ave_10Vneg;                     // average value for -10V
    uint8_t       fault_type;                     // type of fault : Stuck fault or p2p/average fault
};
struct adc_DAC_value
{
    uint32_t      unix_time;                      // unix time
    uint32_t      p2p_a_DAC;                      // p2p value for DAC on channelA
    int32_t      ave_a_DAC;                      // average value for DAC on channelA
    uint32_t      p2p_b_DAC;                      // p2p value for DAC on channelB
    int32_t      ave_b_DAC;                      // average value for DAC on channelB
    uint8_t       fault_type;                     // type of fault : Stuck fault or p2p/average fault
};
//-----------------------------------------------------------------------------------------------------------

void RadInitAnaAcq              (void);
void RadInitAverageP2PRef       (void);
void RadAverageP2PMeas          (uint16_t samples);
uint8_t RadCheckAverageP2PValue   (uint16_t adc_p2p_tol, uint32_t adc_ave_tol, uint32_t adc_ref_a_value, uint32_t adc_ref_b_value);
void RadGetMultiSample          (void);
void RadGetSample               (void);
void RadInitFIP                 (void);
void RadTest_RX_TX_FIP          (void);
void RadHc16MicrofipC32RegTest  (void);
void RadHc16C32ExtRAM           (void);
void RadHc16InitRAM             (void);
void RadHc16C32IntRAM           (void);
void RadSendData                (uint8_t DataToSend);

//-----------------------------------------------------------------------------------------------------------

// HC16 and C32 variable for SBE test : EDAC

RAD_VARS_EXT uint32_t                     sbe_hc16_last;
RAD_VARS_EXT uint32_t                     sbe_c32_last;

// HC16 and C32 variable for SBE test : Internal C32 and HC16 RAM

RAD_VARS_EXT uint32_t                     c32_sbe_int_ram;                // counter for number of SBE on the internal C32 RAM
RAD_VARS_EXT uint16_t                     hc16_sbe_int_ram;               // counter for number of SBE on the internal HC16 RAM

// HC16, C32 and WFIP variable for internal register test

RAD_VARS_EXT uint32_t                     c32_sbe_int_reg_counter;        // counter for number of internal C32 Register failed
RAD_VARS_EXT uint32_t                     c32_sbe_int_reg_idx;            // C32 register failed index
RAD_VARS_EXT uint32_t                     c32_sbe_int_reg_failed;         // counter for number of SBE on the internal C32 Register

// ANA variables for filter test

RAD_VARS_EXT int32_t                     pos10V_average_old;             // Last channelA average value for -10V/+10V
RAD_VARS_EXT int32_t                     neg10V_average_old;             // Last channelB average value for -10V/+10V
RAD_VARS_EXT int32_t                     dac_a_average_old;              // Last channelA average value for DAC
RAD_VARS_EXT int32_t                     dac_b_average_old;              // Last channelB average value for DAC
RAD_VARS_EXT uint32_t                     pos10V_p2p_old;                 // Last channelA p2p value for -10V/+10V
RAD_VARS_EXT uint32_t                     neg10V_p2p_old;                 // Last channelB p2p value for -10V/+10V
RAD_VARS_EXT uint32_t                     DAC_a_p2p_old;                  // Last channelA p2p value for DAC
RAD_VARS_EXT uint32_t                     DAC_b_p2p_old;                  // Last channelB p2p value for DAC

RAD_VARS_EXT uint32_t                     adc_10Vpos_ref;                 // +10V reference
RAD_VARS_EXT uint32_t                     adc_10Vneg_ref;                 // +10V reference
RAD_VARS_EXT uint32_t                     adc_a_dac_ref;                  // DAC reference
RAD_VARS_EXT uint32_t                     adc_b_dac_ref;                  // DAC reference

RAD_VARS_EXT int32_t                     adc_a_average_value;            // current average value on channelA for DAC or -10V/+10V
RAD_VARS_EXT int32_t                     adc_b_average_value;            // current average value on channelB for DAC or -10V/+10V
RAD_VARS_EXT uint32_t                     adc_a_p2p_value;
RAD_VARS_EXT uint32_t                     adc_b_p2p_value;
RAD_VARS_EXT uint8_t                      V10_array_index;                // index for the +10V/-10V array
RAD_VARS_EXT uint8_t                      DAC_array_index;                // index for the DAC array
RAD_VARS_EXT uint8_t                      dac_a_stuck_index;              // index for the DAC stuck value detection
RAD_VARS_EXT uint8_t                      dac_b_stuck_index;              // index for the DAC stuck value detection
RAD_VARS_EXT uint8_t                      adc_10Vpos_stuck_index;         // index for the +10V stuck value detection
RAD_VARS_EXT uint8_t                      adc_10Vneg_stuck_index;         // index for the +10V stuck value detection

RAD_VARS_EXT struct adc_10V_value       adc_10V_meas[MAX_ADC_ARRAY_SIZE];

RAD_VARS_EXT struct adc_DAC_value       adc_DAC_meas[MAX_ADC_ARRAY_SIZE];

RAD_VARS_EXT uint16_t                     nb_sample_before_stuck;

RAD_VARS_EXT uint8_t                      ana_test_selection               // selection of the ANA test : 0 = +10V/-10V ; 2 = DAC
#ifdef RAD_GLOBALS
= 0
#endif
;

RAD_VARS_EXT uint16_t                     hc16_sbe_reg_counter            // SBE counter for the HC16 internal register
#ifdef RAD_GLOBALS
= 0
#endif
;

RAD_VARS_EXT uint16_t                     hc16_sbe_int_reg_failed         // SBE counter for the internal register
#ifdef RAD_GLOBALS
= 0
#endif
;

RAD_VARS_EXT char * FAR                 SelectAna[4]
#ifdef RAD_GLOBALS
= {
        "+10V",
        "-10V",
        "DAC",
        "DAC"
}
#endif
;

RAD_VARS_EXT struct hc16_reg            internal_regs[]         // list of the register tested
#ifdef RAD_GLOBALS
= {
        { 0xFFA00,      2,      RAD_WRITE,      0x40CF,         0 },    // SIM : SIMCR Register
        { 0xFFA04,      2,      RAD_WRITE,      0x7F08,         0 },    // SIM : SYNCR Register
        { 0xFFA11,      1,      RAD_WRITE,        0xFF,         0 },    // SIM : PORTE0 Register
        { 0xFFA13,      1,      RAD_WRITE,        0xFF,         0 },    // SIM : PORTE1 Register
        { 0xFFA15,      1,      RAD_WRITE,        0x00,         0 },    // SIM : DDRE Register
        { 0xFFA17,      1,      RAD_WRITE,        0xFF,         0 },    // SIM : PORTF0 Register
        { 0xFFA19,      1,      RAD_WRITE,        0xE1,         0 },    // SIM : PROTF1 Register
        { 0xFFA1D,      1,      RAD_WRITE,        0x1E,         0 },    // SIM : DDRF Register
        { 0xFFA1F,      1,      RAD_WRITE,        0x01,         0 },    // SIM : PFPAR Register
        { 0xFFA21,      1,      RAD_WRITE,        0x04,         0 },    // SIM : SYPCR Register
        { 0xFFA44,      2,      RAD_WRITE,      0x0003,         0 },    // SIM : CSPAR0 Register
        { 0xFFA46,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSPAR1 Register
        { 0xFFA48,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBARBT Register
        { 0xFFA4A,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSORBT Register
        { 0xFFA4C,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR0 Register
        { 0xFFA4E,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR0 Register
        { 0xFFA50,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR1 Register
        { 0xFFA52,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR1 Register
        { 0xFFA54,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR2 Register
        { 0xFFA56,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR2 Register
        { 0xFFA58,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR3 Register
        { 0xFFA5A,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR3 Register
        { 0xFFA5C,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR4 Register
        { 0xFFA5E,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR4 Register
        { 0xFFA60,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR5 Register
        { 0xFFA62,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR5 Register
        { 0xFFA64,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR6 Register
        { 0xFFA66,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR6 Register
        { 0xFFA68,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR7 Register
        { 0xFFA6A,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR7 Register
        { 0xFFA6C,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR8 Register
        { 0xFFA6E,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR8 Register
        { 0xFFA70,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR9 Register
        { 0xFFA72,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR9 Register
        { 0xFFA74,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSBAR10 Register
        { 0xFFA76,      2,      RAD_WRITE,      0x0000,         0 },    // SIM : CSOR10 Register
        { 0xFFB00,      1,      RAD_WRITE,        0x08,         0 },    // RAM : RAMMCR Register
        { 0xFFB05,      1,      RAD_RESET,        0xFF,         0 },    // RAM : RAMBAH Register
        { 0xFFB06,      2,      RAD_RESET,      0x0000,         0 },    // RAM : RAMBAL Register
        { 0xFF70B,      1,      RAD_WRITE,        0x03,         0 },    // ADC : ADCTL0 Register
        { 0xFF70D,      1,      RAD_WRITE,        0x00,         0 },    // ADC : ADCTL1 Register
        { 0xFF70E,      2,      RAD_WRITE,      0x0000,         0 },    // ADC : ADCTL0 Register
        { 0xFFC00,      2,      RAD_WRITE,      0x0080,         0 },    // QSM : QSMCR Register
        { 0xFFC08,      2,      RAD_WRITE,      0x0034,         0 },    // QSM : SCCR0 Register
        { 0xFFC0A,      2,      RAD_WRITE,      0x000C,         0 },    // QSM : SCCR1 Register
        { 0xFFC16,      2,      RAD_WRITE,      0x037E,         0 },    // QSM : PQSPAR and DDRTQS Register
        { 0xFFC18,      2,      RAD_WRITE,      0x8110,         0 },    // QSM : SPCR0 Register
        { 0xFFC1C,      2,      RAD_WRITE,      0x0F00,         0 },    // QSM : SPCR2 Register
        { 0xFFC1E,      1,      RAD_WRITE,        0x00,         0 },    // QSM : SPCR3 Register
        { 0xFF900,      2,      RAD_WRITE,      0x0001,         0 },    // GPT : GPTMCR Register
        { 0xFF904,      2,      RAD_WRITE,      0x0630,         0 },    // GPT : ICR Register
        { 0xFF906,      1,      RAD_WRITE,        0xF0,         0 },    // GPT : DDRGP Register
        { 0xFF908,      1,      RAD_WRITE,        0x00,         0 },    // GPT : OX1M Register
        { 0xFF909,      1,      RAD_WRITE,        0x00,         0 },    // GPT : OX1D Register
        { 0xFF90C,      1,      RAD_WRITE,        0x50,         0 },    // GPT : PACLT Register
        { 0xFF910,      2,      RAD_WRITE,      0xFFFF,         0 },    // GPT : TIC2 Register
        { 0xFF912,      2,      RAD_WRITE,      0xFFFF,         0 },    // GPT : TIC3 Register
        { 0xFF914,      2,      RAD_WRITE,      0xFFFF,         0 },    // GPT : TOC1 Register
        { 0xFF91E,      2,      RAD_WRITE,      0x0502,         0 },    // GPT : TCTL1/TCTL2 Register
        { 0xFF920,      2,      RAD_WRITE,      0x1101,         0 },    // GPT : TMSK1/TSMK2 Register
        { 0xFF924,      1,      RAD_WRITE,        0x00,         0 },    // GPT : PWMC Register
        { 0xFF925,      1,      RAD_WRITE,        0x00,         0 },    // GPT : CFORC Register
        {  0x0690,      1,      RAD_RESET,        0x00,         0 },    // FIP : FIP_CONFA_P Register
        {  0x0692,      1,      RAD_RESET,        0x0A,         0 },    // FIP : FIP_CONFC_P Register
        {  0x0694,      1,      RAD_RESET,        0x80,         0 },    // FIP : FIP_CONFE_P Register
        {  0x0696,      1,      RAD_RESET,        0x00,         0 },    // FIP : FIP_MPSPR_P Register
        {  0x069C,      1,      RAD_RESET,        0x00,         0 },    // FIP : FIP_VIDGL_P Register
        {  0x069D,      1,      RAD_RESET,        0x30,         0 },    // FIP : FIP_VIDGH_P Register
        {  0x06A1,      1,      RAD_RESET,        0x01,         0 },    // FIP : FIP_ARCNL_P Register
        {  0x06A2,      1,      RAD_RESET,        0x00,         0 },    // FIP : FIP_ARCNH_P Register
        { 0 },                                                          // End of list
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // RAD_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rad.h
\*---------------------------------------------------------------------------------------------------------*/
