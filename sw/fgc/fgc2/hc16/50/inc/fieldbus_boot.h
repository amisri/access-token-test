/*---------------------------------------------------------------------------------------------------------*\
  File:         fieldbus_boot.h

  Purpose:  physical network (Ethernet or FIP) wrapper

\*---------------------------------------------------------------------------------------------------------*/

#ifndef FIELDBUS_BOOT_H // header encapsulation
#define FIELDBUS_BOOT_H

#ifdef FIELDBUS_BOOT_GLOBALS
    #define FIELDBUS_BOOT_VARS_EXT
#else
    #define FIELDBUS_BOOT_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------
#include <cc_types.h>
#include <iodefines.h>
#include <mcu_dependent.h>
#include <definfo.h>
#include <fgc_fip.h>

#include <fgc_code.h>           // For FGC_CODE_BLK_SIZE

//-----------------------------------------------------------------------------------------------------------

// Macro arguments are expanded except if with # or ##, so we need 2 level of indirection
#define CLASS_FIELD_STRING(name)        c##name
#define CLASS_FIELD_STRING2(name)       CLASS_FIELD_STRING(name)
#define CLASS_FIELD                     CLASS_FIELD_STRING2(FGC_CLASS_ID)   // c##FGC_CLASS_ID

struct Tfieldbus
{
    uint16_t              progress_refresh_rate;  // Change terminal refresh rate depending on the net type
    bool                is_gateway_online;      // gateway is online or not
    uint16_t              last_runlog_idx;        // Previous runlog_idx
    uint16_t              gw_timeout_ms;          // Millisecond down counter since last reception
    uint16_t              rterm_ch_idx;           // Idx in rterm[]
};

//-----------------------------------------------------------------------------------------------------------
// Functions shared with the application

// During boot, processing of received/send packets is done within the network interrupt
void (*FieldbusISR)             (void);
void (*FieldbusWaitLinkUp)      (void);
void FieldbusInit               (void);
void FieldbusWatchDog           (void);
void FieldbusCheckGatewayAlive  (void);
void FieldbusEnableInterrupts   (void);

//-----------------------------------------------------------------------------------------------------------
// Functions shared with fip_boot.c and ether.c
// This is part of the application, it performs the processing of the packets
// Those functions are called by fip_boot.c/ether.c depending of the received/sent data
void FieldbusRxTime             (void);
void FieldbusRxCode             (uint16_t nb_code_in_block, uint8_t p_code[][FGC_CODE_BLK_SIZE]);
void (*FieldbusTxCh)            (char ch);


//-----------------------------------------------------------------------------------------------------------

FIELDBUS_BOOT_VARS_EXT struct Tfieldbus          fieldbus;              // fieldbus variable structure

FIELDBUS_BOOT_VARS_EXT union  fgc_stat_var       stat_var;              // status variable structure

FIELDBUS_BOOT_VARS_EXT struct fgc_fieldbus_time  time_var;              // time variable structure

//-----------------------------------------------------------------------------------------------------------

#endif  // FIELDBUS_BOOT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: fieldbus_boot.h
\*---------------------------------------------------------------------------------------------------------*/
