/*---------------------------------------------------------------------------------------------------------*\
  File:         codes_mcu.h

  Purpose:      FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef CODES_MCU_H     // header encapsulation
#define CODES_MCU_H

#ifdef CODES_MCU_GLOBALS
    #define CODES_MCU_VARS_EXT
#else
    #define CODES_MCU_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <codes_holder.h>

//-----------------------------------------------------------------------------------------------------------

uint16_t  EraseMcuDev             (enum fgc_code_ids_short_list dev_idx);
uint16_t  WriteMcuBootDev         (enum fgc_code_ids_short_list dev_idx);
uint16_t  CheckMcuDev             (enum fgc_code_ids_short_list dev_idx);

uint16_t  ProgramMcuDev           (enum fgc_code_ids_short_list dev_idx, struct code_block *block); // only FGC2

uint16_t  EraseMcuBootDev         (enum fgc_code_ids_short_list dev_idx); // only FGC3
uint16_t  WriteMcuDev             (enum fgc_code_ids_short_list dev_idx); // only FGC3
uint16_t  RAM_WriteBoot           (void);                                 // only FGC3

//-----------------------------------------------------------------------------------------------------------

#endif  // CODES_MCU_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_mcu.h
\*---------------------------------------------------------------------------------------------------------*/
