/*---------------------------------------------------------------------------------------------------------*\
  File:         pll_boot.h

 Purpose:   FGC MCU Software - RT Clock Phase Locked Loop Functions.

 Author:    Quentin.King@cern.ch

 Notes:     This file contains the functions to support synchronisation of the HC16 GPT clock with
            the WorldFIP cycle (driven by GPS time).

            Units:  tic HC16 GPT_TCNT clock period.  The clock prescalar is 4, so the
                    GPT_TCNT clock frequency is ~16MHz/4, and therefore 1 tic = ~0.25 us.

            qtic    tic/65536 = ~3.8 ps.  Many PLL variables are in qtics.
            qtic variables are long words with the most significant word being tics.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef PLL_BOOT_H      // header encapsulation
#define PLL_BOOT_H

#ifdef PLL_BOOT_GLOBALS
    #define PLL_BOOT_VARS_EXT
#else
    #define PLL_BOOT_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

#define GATEWAY_ALIVE_TIMEOUT_MS                45          // 45ms (allow two missed packed before change of state)

#define PLL_LOCKED_LIMIT_FIPNET         20              // +/-10us
#define TICK_PERIOD                     2000L   // 1ms in 2MHz GPT clock ticks
#define PLL_REF                         1100    // Reference in 2MHz GPT clock ticks

#define PLL_LOCKED_DETECT_FIPNET                50              // Number of cycle below limit for locked state

//-----------------------------------------------------------------------------------------------------------

struct TPllGlobalData
{
    uint16_t              locked_detect;                  // Down counter to detect locked state
    uint16_t              frac_counter;                   // PLL fraction counter for ISR
    uint16_t              ms_t0;                              // Time of ms interrupt (ticks)
    int32_t              Terr;                               // Phase error between ms IRQ and Fip IRQ
    uint32_t              qtics_per_ms;                   // RT clock period (used by IsrMst)
    uint32_t              int_qtics_per_ms;               // Integrator for RT clock period
};

//-----------------------------------------------------------------------------------------------------------
void    PllInit                         (void);
void    GatewayAliveAndPllAlgoBlocking  (void);
void    PllAlgoWithFipSynchro           (void);
//--------------------------------------------------------------------

PLL_BOOT_VARS_EXT struct TPllGlobalData         pll;

//-----------------------------------------------------------------------------------------------------------

#endif  // PLL_BOOT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pll_boot.h
\*---------------------------------------------------------------------------------------------------------*/
