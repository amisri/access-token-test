/*---------------------------------------------------------------------------------------------------------*\
  File:         codes_pld.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CODES_PLD_H     // header encapsulation
#define CODES_PLD_H

#ifdef CODES_PLD_GLOBALS
#define CODES_PLD_EXT
#else
#define CODES_PLD_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <codes_holder.h>

//-----------------------------------------------------------------------------------------------------------

uint16_t  CheckPldDev(enum fgc_code_ids_short_list dev_idx);
uint16_t  ErasePldDev(enum fgc_code_ids_short_list dev_idx);
uint16_t  WritePldDev(enum fgc_code_ids_short_list dev_idx);
void    CodesPLDSelect(void);

//-----------------------------------------------------------------------------------------------------------

#endif  // CODES_PLD_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_pld.h
\*---------------------------------------------------------------------------------------------------------*/

