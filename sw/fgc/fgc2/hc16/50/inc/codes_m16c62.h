/*---------------------------------------------------------------------------------------------------------*\
  File:         codes_m16c62.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CODES_M16C62_H          // header encapsulation
#define CODES_M16C62_H

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <codes_holder.h>
//-----------------------------------------------------------------------------------------------------------

uint16_t  EraseC62Dev     (enum fgc_code_ids_short_list dev_idx);
uint16_t  CheckC62Dev     (enum fgc_code_ids_short_list dev_idx);
uint16_t  SelectM16C62Dev (enum fgc_code_ids_short_list dev_idx, uint8_t *block_mask_p);

uint16_t  WriteM16C62dev  (enum fgc_code_ids_short_list dev_idx); // only FGC3

uint16_t  ProgramC62Dev   (enum fgc_code_ids_short_list dev_idx, struct code_block *block);       // only FGC2

//-----------------------------------------------------------------------------------------------------------

#endif  // CODES_M16C62_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_m16c62.h
\*---------------------------------------------------------------------------------------------------------*/

