/*---------------------------------------------------------------------------------------------------------*\
 File:      m16c62.c

 Purpose:   functions to comunicate/control the Renesas M16C62 mcu

 Notes:     The Renesas M16C62 mcu handles the Dallas 1-wire ID bus

\*---------------------------------------------------------------------------------------------------------*/

#define M16C62_GLOBALS          // for c62 global variable

#include <m16c62.h>
#include <debug.h>              // for DEBUG_M16C62_STANDALONE
#include <codes.h>
#include <iodefines.h>          // specific processor registers and constants
#include <mcu_dependent.h>      // for MSLEEP(), USLEEP()

#define BIG_ENDIAN_OFFSET_TO_BYTE3 1
#include <structs_bits_big.h>
#include <main.h>               // for main_misc global variable
#include <definfo.h>

#include <memmap_mcu.h>         // Include memory map consts
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <menu.h>               // for MenuRspArg(), MenuRspError()
#include <microlan_1wire.h>     // for ERR_MICROLAN_

/*---------------------------------------------------------------------------------------------------------*/
uint16_t C62PowerOn(uint16_t bus_initial_value)
/*---------------------------------------------------------------------------------------------------------*\
  This function will turn ON the M16C62
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  tmp;

#ifndef DEBUG_M16C62_STANDALONE
    if ( (C62_P & C62_STANDALONE_MASK16) != 0 )
    {
        MenuRspError("M16C62 is in standalone mode");   // Report error
        return(1);
    }

    if ( (C62_P & C62_POWER_MASK16) != 0 )      // If M16C62 is already on
    {
        MenuRspError("M16C62 is already powered");  // Report error
        return(2);
    }

    // when M16C62 boots if bus_initial_value is C62_SWITCH_PROG the M16C62 will remain in IdBoot
    // otherwise it will run IdProg wich in turn will run the its monitor

    // REMEMBER!!! that PROG bit is not READ ONLY!!! (for the FGC3)

    // writing the register fires the reset of DATARDY and CMDERR bits in the PLD internal register
    // and the set of CMDBUSY
    C62_P = bus_initial_value;

    CPU_RESET_P &= ~CPU_RESET_CTRL_C62OFF_MASK16;   // Turn on M16C62

    MSLEEP(3);                                          // Wait for M16C62 to power up

    tmp = C62_P;
    if ( (tmp & C62_POWER_MASK16) == 0 )        // If M16C62 failed to power up
    {
        MenuRspError("M16C62 power on failed - M16C62 reg:0x%04X",tmp); // Report error
        CPU_RESET_P |= CPU_RESET_CTRL_C62OFF_MASK16; // Turn off M16C62
        return(3);
    }

    MSLEEP(20);                     // Wait for M16C62 boot to start

    tmp = C62_P;
    if ( ( (tmp ^ bus_initial_value) & 0x00FF) != 0xFF )    // If M16C62 boot did not respond
    {
        MenuRspError("M16C62 boot failed to respond - M16C62 reg:0x%04X",tmp);  // Report error
        CPU_RESET_P |= CPU_RESET_CTRL_C62OFF_MASK16;    // Turn off M16C62
        return(4);
    }
#endif

/*
    data = 3;
    tmp = C62SendIsrCmd(C62_SET_DEBUG_LEVEL, 1, 0, &data);  // set debug level to 3
    if ( tmp != 0 )
    {
        return(tmp);
    }
*/
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t C62PowerOff(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will turn off the M16C62.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  tmp;

#ifndef DEBUG_M16C62_STANDALONE
    if ( (C62_P & C62_STANDALONE_MASK16) != 0 )         // If M16C62 is in standalone
    {
        MenuRspError("M16C62 is in standalone mode");       // Report error
        return(1);
    }

    if ( (C62_P & C62_POWER_MASK16) == 0 )          // If M16C62 is already off
    {
        MenuRspError("M16C62 is already NOT powered");      // Report error
        return(2);
    }

    if ( (C62_P & C62_CMDBUSY_MASK16) != 0 )                // if still busy
    {
      // 2 seconds delay before OFF to let the terminal print their debug info
        MSLEEP(2000);
    }

    CPU_RESET_P |= CPU_RESET_CTRL_C62OFF_MASK16;        // Turn off M16C62

    MSLEEP(3);                          // Wait for M16C62 to power off

    tmp = C62_P;
    if ( (tmp & C62_POWER_MASK16) != 0  )           // If M16C62 is still on
    {
        MenuRspError("M16C62 power off failed - M16C62 reg:0x%04X", tmp);   // Report error
        return(3);
    }
#endif

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t C62SendByte(uint8_t tx, uint8_t * rx)
/*---------------------------------------------------------------------------------------------------------*\
  Send a byte to the M16C62 and wait for a response.
  The function returns 0 on success or a M16C62 error code if there is an error.

  ToDo: This has especific code of the M32C87B
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      tmp;
    uint16_t      start_stamp;

    // for the Rx610 we leave the level 7 active, so the timer will continue to run and can be used (USLEEP,MSLEEP,...)
    OS_ENTER_CRITICAL();            // the exchange with the M16C62 must be atomic

    start_stamp = Get2MHzCounter();                           // Note time now

    // we are working with the word that also contains the control part
    // REMEMBER!!! that PROG bit is not READ ONLY!!! (for the FGC3)

    // writing the register fires the reset of DATARDY and CMDERR bits in the PLD internal register
    // and the set of CMDBUSY
    C62_P = ((uint16_t) tx) & (~C62_PROG_MASK16);         // send byte to M16C62

    // we can't use BUSY going to 1 to know that the M16C62 start the INT3 as it remain active until the
    // the whole command is finish, and this can involve several bytes exchanged

    // at the end of M16C62 INT3 ISR the DATARDY or the CMDERR signals is updated in the PLD internal register

    // I get a max delay of 1.3ms before DATARDY when programing the M16C62
    while (    ( (C62_P & (C62_DATARDY_MASK16 | C62_CMDERR_MASK16) ) == 0 )
            && ( (Get2MHzCounter() - start_stamp) < (C62_MAX_ISR_US * 2) )
          )
    {
    }

    // What happened with the M68HC16? why it has not timeout check?


    // the response is ready, we can read it
    tmp = C62_P;

    OS_EXIT_CRITICAL();

    *rx = (tmp & 0xFF);             // notify data byte to the caller

    if ( (tmp & C62_DATARDY_MASK16) != 0 )  // If command ran correctly
    {
        return(M16C62_NO_ERROR);
    }

    if ( (tmp & C62_CMDERR_MASK16) != 0 )   // If M16C62 reported an error
    {
        return(*rx);                // Return the error code.
    }

    // we arrive here in case of timeout, neither DATARDY nor CMDERR active

    if ( tx == C62_GET_TERM_CHAR )      // if we are polling the chars (like when in terminal)
    {                       // don't Turn off, just say no char gently
        *rx = 0;
        return(0);
    }

    // exit with BUSY on, we assumed NO RESPONSE

    CPU_RESET_P |= CPU_RESET_CTRL_C62OFF_MASK16;    // Turn off M16C62
    MSLEEP(2);                      // Wait for M16C62 to power off
    return(M16C62_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t C62GetRsp(uint8_t * buf, uint16_t * max_bytes)
/*---------------------------------------------------------------------------------------------------------*\
  read the response buffer from the M16C62 polling with C62_GET_RESPONSE until
  the buffer is empty (M16C62_NO_MORE_DATA)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  stat;
    uint16_t  n_bytes;

    for( n_bytes = 0 ; n_bytes < *max_bytes; n_bytes++, buf++)
    {
        stat = C62SendByte(C62_GET_RESPONSE, buf);

        if ( stat == M16C62_NO_MORE_DATA )
        {
            *max_bytes = n_bytes;
            return(0);
        }

        if ( stat != 0 )
        {
            *max_bytes = n_bytes;
            return(stat);
        }
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t C62SwitchProg(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will switch between the ID boot and Program (and back)
\*---------------------------------------------------------------------------------------------------------*/
{
#ifndef DEBUG_M16C62_STANDALONE
    if ( (C62_P & C62_STANDALONE_MASK16) != 0  )        // If M16C62 is in standalone
    {
        MenuRspError("M16C62 is in standalone mode");       // Report error
        return(1);
    }
#endif

    if ( (C62_P & C62_POWER_MASK16) == 0 )          // If M16C62 is off
    {
        MenuRspError("M16C62 is not powered");          // Report error
        return(2);
    }

    if ( C62SendBgCmd(C62_SWITCH_PROG, 25) != 0 )       // Try to switch programs, timeout 20ms
    {
        return(3);
    }

    if ( c62.rsp_bytes_in_buffer == 2 )                 // 2 bytes recevied -> M16C62 Boot running
    {
        MenuRspArg("%s","IdBoot");              // Report that Boot is running
        MenuRspArg("%u",c62.rsp.bytes[1]);          // Report Boot version
    }
    else
    {
        if ( c62.rsp_bytes_in_buffer == 3 )                 // 3 bytes received -> ID Program running
        {
            MenuRspArg("%s","IdProg");              // Report that IdProg is running
            MenuRspArg("%u",(c62.rsp.bytes[1] << 8) + c62.rsp.bytes[2]);// Report IdProg version
        }
        else                            // else unknown program running!
        {
            MenuRspError("Bad rsp len:%u",c62.rsp_bytes_in_buffer);     // Report error
            return(1);
        }
    }
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t C62SendIsrCmd(uint16_t cmd, uint16_t arg_count, uint16_t arg_offset, uint8_t * args)
/*---------------------------------------------------------------------------------------------------------*\
  Send an M16C62 Fast command with the specified number of argument bytes

    --- Fast Commands (processed inside the ISR) ---

    at IdBootProg

     0, C62_GET_RESPONSE        FastCmdGetResponse
     1, C62_SET_ADDRESS         FastCmdSetAddress
     2, C62_SET_DATA            FastCmdSetData
     3, C62_SET_BLOCK_MASK  FastCmdSetBlockMask
     4, C62_SET_CRC_LEN         FastCmdSetCrcLen
    17, C62_SET_BIG_ENDIAN
    18, C62_SET_LITTLE_ENDIAN
    19, C62_SET_DEBUG_LEVEL

    at IdProg

     0, C62_GET_RESPONSE        FastCmdGetResponse
     1, C62_SET_ADDRESS         FastCmdSetAddress
     2, C62_SET_DATA            FastCmdSetData
     3, C62_SET_BLOCK_MASK  FastCmdSetBlockMask
     4, C62_SET_DB              FastCmdSetDb
     5, C62_SET_BRANCH          FastCmdSetBranchIdx
     6, C62_SET_DEV             FastCmdSetDevIdx
     7, C62_SET_KBD_CHAR        FastCmdSetKbdChar
     8, C62_GET_TERM_CHAR       FastCmdGetTermChar
    17, C62_SET_BIG_ENDIAN
    18, C62_SET_LITTLE_ENDIAN
    19, C62_SET_DEBUG_LEVEL

\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  stat;
    uint8_t   rx;

// ToDo: the exchange with the M16C62 must be atomic, theorically there is activity to drive the M16C62
// in the interrupt services, but is better to block interrupts until the exchange with the M16C62 is finish

    stat = C62SendByte(cmd, &rx);
    if ( stat != 0 )
    {
        MenuRspError("Failed to send M16C62 Fast cmd %u: %s", cmd, TypeLabel(c62_isr_stat, stat) );
        return(1);
    }

    args += arg_offset;

    while( arg_count != 0 )
    {
        if ( rx != (uint8_t) arg_count )
        {
            MenuRspError("M16C62 cmd %u expects %u args, %u still to be sent", cmd, rx, arg_count );
            return(2);
        }

        stat = C62SendByte( *args, &rx);
        if (stat != 0)
        {
            MenuRspError("Failed to send arg %u (0x%02X):%s", arg_count, *args, TypeLabel(c62_isr_stat, stat) );
            return(3);
        }

        args++;
        arg_count--;
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t C62SendBgCmd(uint16_t cmd, uint16_t ms_timeout)
/*---------------------------------------------------------------------------------------------------------*\
  Send a M16C62 Slow (background) command and wait up to timeout_ms for busy to be cleared

    --- Slow (background) Commands ---

    at IdBootProg

    10, C62_SWITCH_PROG         SlowCmdSwitchProg
    11, C62_ERASE_FLASH         SlowCmdEraseFlash
    12, C62_PROG_FLASH          SlowCmdProgFlash
    13, C62_CALC_CRC            SlowCmdCalcCrc

    at IdProg

    10, C62_SWITCH_PROG         SlowCmdSwitchProg
    11, C62_ERASE_FLASH         SlowCmdEraseFlash
    12, C62_PROG_FLASH          SlowCmdProgFlash
    13, C62_READ_IDS            SlowCmdReadIds
    14, C62_READ_TEMPS          SlowCmdReadTemps
    15, C62_READ_TEMPS_BRANCH   SlowCmdReadTempsBranch
    16, C62_RUN_MONITOR         SlowCmdRunMonitor

\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  time_ms = 0;
    uint16_t  tmp;
    uint16_t  stat;
    uint8_t   rx;

// ToDo: the exchange with the M16C62 must be atomic, theoretically there is activity to drive the M16C62
// in the interrupt services, but is better to block interrupts until the exchange with the M16C62 is finish


    stat = C62SendByte(cmd,&rx);        // Send M16C62 Slow command
    if ( stat != 0 )
    {
        MenuRspError("Failed to send M16C62 Slow cmd %u: %s", cmd, TypeLabel(c62_isr_stat,stat) );
        return(1);
    }

    while ( ++time_ms <= ms_timeout )
    {
        tmp = C62_P;            // Read M16C62 register word

        if ( (tmp & C62_CMDBUSY_MASK16) == 0 )      // If command no longer busy
        {
            c62.rsp_bytes_in_buffer = 10;

            stat = C62GetRsp( (uint8_t *) &(c62.rsp.bytes), (uint16_t *) &(c62.rsp_bytes_in_buffer) ); // read the response buffer from the M16C62

            if ( (stat != 0) || (c62.rsp_bytes_in_buffer == 0) )
            {
                MenuRspError("M16C62 Slow cmd %u get rsp fail: %s (%u bytes rcvd)",
                             cmd, TypeLabel(c62_isr_stat, stat), c62.rsp_bytes_in_buffer);
                return(2);
            }

            switch(c62.rsp.err.code)    // buff[0] error byte returned by Slow command
            {
                case M16C62_NO_ERROR:
                    return(0);

                case M16C62_CRC_FAILED:

                    c62.crc_calc = c62.rsp.err.v.crc.calculated.word;
                    c62.crc_code = c62.rsp.err.v.crc.hardcoded.word;
                    return(M16C62_CRC_FAILED);

                case M16C62_ERASE_FAILED:   // extra info: flsh_stat l/h,bad_val l/h,addr l/m/h

                    c62.flash_stat = c62.rsp.err.v.erase.flash_stat.word;
                    c62.bad_value  = c62.rsp.err.v.erase.bad_value.word;

            // Address comes only as 3 bytes, clear the 4th
                    c62.rsp.err.v.erase.address.int32u = c62.rsp.err.v.erase.address.int32u >> 8; // big endian
                    c62.address = c62.rsp.err.v.erase.address.int32u;

                    MenuRspError("M16C62, flash erase fail - 0x%04X found at 0x%05lX - flash stat:0x%04X",
                                    c62.bad_value,c62.address,c62.flash_stat);
                    return(M16C62_ERASE_FAILED);

                case M16C62_PROG_FAILED:    // extra info: flsh_stat h/l,exp_val,bad_val,bad_addr h/m/l

                    c62.flash_stat = c62.rsp.err.v.write.flash_stat.word;
                    c62.exp_value  = c62.rsp.err.v.write.exp_value; // !!! from uint8_t to uint16_t
                    c62.bad_value  = c62.rsp.err.v.write.bad_value; // !!! from uint8_t to uint16_t

                    // Address cames only as 3 bytes, clear the 4th
                    c62.rsp.err.v.write.address.int32u = c62.rsp.err.v.write.address.int32u >> 8; // big endian
                    c62.address = c62.rsp.err.v.write.address.int32u;
                    MenuRspError("M16C62 prog failed - exp:0x%04X got:0x%04X at 0x%05lX - flash stat:0x%04X",
                                    c62.exp_value,c62.bad_value,c62.address,c62.flash_stat);
                    return(M16C62_PROG_FAILED);

                case M16C62_INVALID_SECTOR: // extra info: bad_addr h/m/l
                    // Address cames only as 3 bytes, clear the 4th
                    c62.rsp.err.v.address.int32u = c62.rsp.err.v.address.int32u >> 8; // big endian
                    c62.address    = c62.rsp.err.v.address.int32u;
                    MenuRspError("Invalid M16C62 sector:0x%05lX",c62.address);
                    return(M16C62_INVALID_SECTOR);

                case ERR_MICROLAN_DS2480_TIMEOUT:

                    MenuRspError("M16C62, DS2480 timeout");
                    return(ERR_MICROLAN_DS2480_TIMEOUT);

                case ERR_MICROLAN_TOP_LINK_SHORTED:

                    MenuRspError("M16C62, top bus is shorted");
                    return(ERR_MICROLAN_TOP_LINK_SHORTED);

                case ERR_MICROLAN_LOGICAL_PATH_NO_DEVICES:      // extra info: branch_idx (This may be seen if bus is shorted)

                    if ( c62.rsp_bytes_in_buffer == 2 )
                    {
                        MenuRspError("M16C62, no devices found on path %u",c62.rsp.err.v.logical_path);
                    }
                    else
                    {
                        MenuRspError("M16C62, no devices found");
                    }
                    return(ERR_MICROLAN_LOGICAL_PATH_NO_DEVICES);

                case ERR_MICROLAN_COUPLER_UNKNOW_STATUS:    // extra info: status

                    MenuRspError("M16C62, DS2409 unknown status:%u",c62.rsp.err.v.state);
                    return(ERR_MICROLAN_TOP_LINK_SHORTED);

                case ERR_MICROLAN_COUPLERS_FOUND:   // extra info: n_couplers

                    MenuRspError("M16C62, DS2409 expected 3, found %u",c62.rsp.err.v.number_of_couplers);
                    return(ERR_MICROLAN_COUPLERS_FOUND);

                case ERR_MICROLAN_UNEXPECTED_DEVICES:   // extra info: n_unexpected

                    MenuRspError("M16C62, %u unexpected devices",c62.rsp.err.v.devices_number);
                    return(ERR_MICROLAN_UNEXPECTED_DEVICES);

                case ERR_MICROLAN_COUPLER_CONTROL_LINES:    // extra info: c_ctrl_h, c_ctrl_l or c_ctrl_l, c_ctrl_h

                    MenuRspError("M16C62, unknown status:%u",c62.rsp.err.v.control.word);
                    return(ERR_MICROLAN_COUPLER_CONTROL_LINES);

                case ERR_MICROLAN_UNRELIABLE_FIND:  // extra info: branch_idx

                    MenuRspError("M16C62, unreliable search on branch %u", c62.rsp.err.v.logical_path);
                    return(ERR_MICROLAN_UNRELIABLE_FIND);

                case ERR_MICROLAN_COUPLERS_READBACK:      // extra info: port1 value

                    MenuRspError("M16C62, DS2409 readback fail, P1: 0x%02X", c62.rsp.err.v.state);
                    return(ERR_MICROLAN_COUPLERS_READBACK);

                default:

                    MenuRspError("M16C62, unknown response (%u) and %u byte(s)", c62.rsp.err.code, c62.rsp_bytes_in_buffer);
                    return(c62.rsp.err.code);
            }
        }

        MSLEEP(1);
    }

    MenuRspError("Timed out after waiting %u ms for M16C62 slow cmd %u", ms_timeout, cmd);  // Report error
    CPU_RESET_P |= CPU_RESET_CTRL_C62OFF_MASK16;    // Turn off M16C62
    MSLEEP(2);                  // Wait for M16C62 to power off
    return(M16C62_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t C62FlashCode(void)
/*---------------------------------------------------------------------------------------------------------*\
  Send a code sector to the M16C62 to be programmed into flash.

  it needs
  c62.code_address : destination address in M16C62 memory map (3 bytes)
  c62.code_buf     : with the 256 bytes to transfer

\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t stat;

    stat = C62SendIsrCmd(C62_SET_ADDRESS, 3, BIG_ENDIAN_OFFSET_TO_BYTE3, (uint8_t*)&c62.code_address);
    if ( stat == 0 )                                // if OK
    {
        stat = C62SendIsrCmd(C62_SET_DATA, 0x100, 0, c62.code_buf);
        if ( stat == 0 )                            // if OK
        {
            stat = C62SendBgCmd(C62_PROG_FLASH, 1000); // timeout 1000ms
        }
    }
    c62.code_address = 0;       // Clear address to signal that sector has been written

    return(stat);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: m16c62.c
\*---------------------------------------------------------------------------------------------------------*/
