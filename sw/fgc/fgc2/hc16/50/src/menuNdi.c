/*---------------------------------------------------------------------------------------------------------*\
  File:         menuNdi.c

  Purpose:      Functions to run for the ndi boot menu options

  Notes:

  History:

    22/09/03    stp     Created
    26/03/08    qak     MenuNdiC62SwitchProg() implemented and MenuNdiC62RunMonitor() added
\*---------------------------------------------------------------------------------------------------------*/

#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <menu.h>               // for MenuGetInt16U(), MenuRspError()
#include <m16c62.h>             // for C62PowerOn(), C62PowerOff(), C62SwitchProg()
#include <dev.h>                // for dev global variable
#include <term_boot.h>           // for TermPutc()
#include <mcu_dependent.h>      // for USLEEP()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuNdiC62Power(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Power control for C62.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      pwr = !(C62_P & C62_POWER_MASK16);


    if(*argv[0] && MenuGetInt16U(argv[0], 0, 1, &pwr))          // Get power control (0=off 1=on)
    {
        return;
    }

    if(pwr)
    {
        C62PowerOn(C62_SWITCH_PROG);
    }
    else
    {
        C62PowerOff();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuNdiC62SwitchProg(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This will run the Switch Prog background command to switch between the C62 boot and ID program (and
  back again).
\*---------------------------------------------------------------------------------------------------------*/
{
    C62SwitchProg();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuNdiC62RunMonitor(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Run ID Program monitor
\*---------------------------------------------------------------------------------------------------------*/
{
    char        ch;
    uint16_t      errno;

    if ( (C62_P & C62_POWER_MASK16)  && C62PowerOff() )         // Power Off M16C62, we'll start from scratch
    {
        return;
    }

    if(C62PowerOn(C62_SWITCH_PROG))                             // Power On M16C62 and remain in IdBoot
    {
        return;
    }

    if(C62SwitchProg())                                         // switch to IdMain
    {
        return;
    }

    menu.response_buf_pos = 0;                  // Clear response from C62SwitchProg()

    if(C62SendBgCmd(C62_RUN_MONITOR, 2))        // Try to run the monitor
    {
        return;
    }

    while(!dev.abort_f)                         // While CTRL-C not yet pressed
    {
        OS_ENTER_CRITICAL();

        if(term.rx.n_ch)                                // If character received
        {
            ch = term.rx.buf[term.rx.out++];                    // Get character from the rx buffer
            term.rx.n_ch--;                                     // Decrement the buffer occupancy counter

            OS_EXIT_CRITICAL();                                 // Enable interrupts

            if(C62SendIsrCmd(C62_SET_KBD_CHAR, 1, 0, (uint8_t *)&ch))      // If transmission of kbd char fails
            {
                break;                                          // Quit communication with M16C62
            }
        }
        else
        {
            OS_EXIT_CRITICAL();
        }

        errno = C62SendByte(C62_GET_TERM_CHAR, (uint8_t *)&ch);

        if(errno != 0)       // If read of term char fails
        {
            MenuRspError("C62_GET_TERM_CHAR failed:%u",errno);
            break;                                              // Quit communication with M16C62
        }

        if(ch)                                                  // If a new M16C62 term char received
        {
            TermPutc(ch,TERM_TX);                               // Write to M68HC16 terminal
        }

        USLEEP(600);                                    // Sleep for 600us
    }

    if (C62_P & C62_POWER_MASK16)                               // If M16C62 is still on
    {
        C62PowerOff();                                          // Power Off M16C62
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuNdiFipMem(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Test MicroFip memory - not yet implemented
\*---------------------------------------------------------------------------------------------------------*/
{
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuNdi.c
\*---------------------------------------------------------------------------------------------------------*/
