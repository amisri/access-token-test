/*---------------------------------------------------------------------------------------------------------*\
  File:         menuFeedback.c

  Purpose:      Functions for Feedback tests of the FGC in reception racks

  Author:       P. Fraboulet

  Notes:

  History:

    13/04/04    pfr     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <analog.h>             // for menusd global variable
#include <mcu_dependent.h>      // for MSLEEP()
#include <main.h>               // for timeout_ms global variable used by MSLEEP()
#include <defconst.h>           // for FGC_INTERFACE_
#include <stdlib.h>             // for labs()
#include <memmap_mcu.h>         // for DIG_OP_P
#include <defconst.h>           // for FGC_CRATE_TYPE_
#include <crate_boot.h>         // for crate global variable
#include <menu.h>               // for MenuRspError()
#include <diag_boot.h>          // for diag_bus global variable, DIAG_
#include <dev.h>                // for dev global variable
#include <crate_boot.h>         // for crate global variable

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

static char * FAR       chan_lbls[4]
= {
        "DCCTA",
        "DCCTB",
        "AUXA",
        "AUXB"
};

/*---------------------------------------------------------------------------------------------------------*/
void MenuFeedbackAnalogLevel(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Test Analogue inputs (DCCTA, DCCTB, AUXA, AUXB) with analogue channel feedback in reception rack.
  Set DAC and readback the values on the ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       i,j;
    uint16_t      feedback_coef[4]        = {1024, 896, 768, 640};        // (1, 0.875, 0.75, 0.625)*1024
    uint16_t      mpx_list[4]             = {2,1,0,8};                    // correspond to DCCTA,B,AUX,AUX
    uint16_t      mpx;
    int32_t      adc_tol;
    uint16_t      adc_p2p_tol;
    int32_t      dac_value;
    int32_t      num;
    int32_t      den;

    /* Test bustype */

    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }

    /* Test bus acces */

    if((dev.reg_r | dev.reg_w) & REG_SLOT5)
    {
        MenuRspError("No access to Ana register");
        return;
    }

    /* Initialise ADC reference */

    switch(dev.slot5_type)
    {
        case FGC_INTERFACE_SD_350:                      // for SD, take high byte
        case FGC_INTERFACE_SD_351:

            adc_tol        = SLOT5_ADC_SD_TOL;
            adc_p2p_tol    = SLOT5_ADC_SD_P2P;

            /* Set digital mpx on ADCA and ADCB, 500kHz filter */

            AnaMpxDig(0,1);
            AnaFltFrq(0,0);
            break;

        case FGC_INTERFACE_SAR_400:                     // for SD, take high byte

            adc_tol        = SLOT5_ADC_SAR_TOL;
            adc_p2p_tol    = SLOT5_ADC_SAR_P2P;
            break;

        default :

            MenuRspError("No SD or SAR");
            return;
    }

    /* Check feedback signals on DCCTB, AUXA and AUXB */

    for(j=0 ; j<4 ; j++)                // MPX select DCCTB, AUXA, AUXB
    {
        if(MenuRspProgress(true, (j+1) ,4))     // If user abort
        {
            return;                                             // Quit
        }

        for(i=0 ; i<2 ; i++)            // -10, 10V
        {
            /* Set DAC */

            dac_value = (int32_t)(2*i-1)*SLOT5_DAC_10V;          // -10 and 10V
            dac_value = (int32_t)(dac_value << 12);              // Value in DAC is left justified (bits 31-12)

            ANA_DAC_HIGH_P = ((uint16_t*)&dac_value)[0];
            ANA_DAC_LOW_P  = ((uint16_t*)&dac_value)[1];

            /* Set Ana MPX */

            mpx = ANA_MPX_P;                            // Force word read

            if(mpx_list[j] != 8)                        // on the 4th run leave mpx on AUX (mpx=0)
            {
                AnaMpxAna(mpx_list[j],mpx_list[j]);     // MPX on DCCTA,DCCTB,AUX
            }

            /* Wait 500ms */

            MSLEEP(500);

            /* Wait for a sample to be taken */

            AnaGetMultiSample(SLOT5_SAMPLES_REF);

            /* Check p2p noise */

            if(menusd.a.p2p_m > adc_p2p_tol)
            {
                MenuRspError("Chan %s ADC A p2p: 0x%04X", chan_lbls[j], menusd.a.p2p_m);
                return;
            }
            if(menusd.b.p2p_m > adc_p2p_tol)
            {
                MenuRspError("Chan %s ADC B p2p: 0x%04X", chan_lbls[j], menusd.b.p2p_m);
                return;
            }

            /* Checks DAC is calibrated - dac_cal is initialised with the most negative value */

            if(dac_cal.val[2] == 0x80000000)
            {
                MenuRspError("DAC not calibrated");
                return;
            }

            /* Check results */

            if(mpx_list[j] == 8)                // AUXB read back on ADC B only
            {
                num = (int32_t)((uint32_t)menusd.b.raw_m | ((uint32_t)menusd.b.raw_h << 16));
            }
            else
            {
                num = (int32_t)((uint32_t)menusd.a.raw_m | ((uint32_t)menusd.a.raw_h << 16));
            }

            if(num > dac_cal.val[1])
            {
                den  = dac_cal.val[2] - dac_cal.val[1];
                num -=  dac_cal.val[1];
            }
            else
            {
                den =  dac_cal.val[1] - dac_cal.val[0];
                num =  dac_cal.val[1] - num;
            }

            if(dev.slot5_type == FGC_INTERFACE_SAR_400) // num/den * 1024 without overflow
            {
                num <<= 10;
            }
            else
            {
                num <<= 2;
                den >>= 8;
            }

            if(!den)
            {
                MenuRspError("ADCs and DAC must be calibrated");
                return;
            }

            if(labs( (num/den) - (int32_t)(feedback_coef[j]) ) > SLTO5_GAIN_DAC_TOL)     // 5%
            {
                MenuRspError("Feedbk chan %s ratio: %ld",
                                chan_lbls[j], (int32_t)(num/den));
                return;
            }
        }
    }

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFeedbackDiagDig(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Test Diag digital inputs in reception rack. Set the Dig Commands (OP register) which are mapped onto Diag
  digital inputs. Read back the Diag inputs and check.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       walk;
    uint8_t       i;
    uint16_t      test_vector;
    uint16_t      diag_dig_a;
    uint16_t      diag_dig_b;

    /* Test bustype */

    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }

    /* Test bus acces */

    if((dev.reg_r | dev.reg_w) & REG_DIG)
    {
        MenuRspError("No access to Dig register");
        return;
    }

    /* Check that Diag bus is running */

    if(diag_bus.state != DIAG_RUN)
    {
        /* Enable Diag bus */

        diag_bus.state = DIAG_RESET;
        diag_bus.n_resets = 0;                  // Clear number of resets counter
    }

    /* Walking bit tests */

    for(walk=0 ; walk<2 ; walk++)       // walking bit test (walking 0 then 1)
    {
        test_vector = (walk ? 0x0001 : 0x00FE);
        for(i=0 ; i<8 ; i++)
        {
            /* Set OP register */

            DIG_OP_P = 0xFF00;                  // reset Commands
            DIG_OP_P = ~test_vector & 0x00FF;   // invert commands

            /* Wait for 20 ms */

            MSLEEP(20);

            /* Check Diag digital inputs */

            diag_dig_a = diag_bus.data[DIAG_STATUS_A] & 0x0FFF;
            diag_dig_b = diag_bus.data[DIAG_STATUS_B] & 0x0FFF;

            if(   (diag_dig_a !=  ((test_vector | (test_vector<<8)) & 0x0FFF))
               || ( (diag_dig_b & 0x0FC0) != ((test_vector<<4) & 0x0FC0) )  )           // only the 6 msb of Diag B are used
            {
                MenuRspError("Digital Diag error, got: 0x%03X%03X exp:0x%03X%03X",
                                diag_dig_b & 0x0FC0, diag_dig_a,
                                (test_vector<<4) & 0x0FC0, (test_vector | (test_vector<<8)) & 0x0FFF );
                return;
            }

            /* Prepare test_vector for next run */

            test_vector = ((test_vector << 1) | ~walk) & 0x00FF;        // shift and keep low byte
        }
    }
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFeedbackDiagData(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Diag Spare A and B are mapped on Diag Data A and B. This function sets Spare A and Band reads back
  Data A and B.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       i;
    uint16_t      data;

    /* Test bustype */

    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }

    /* Check that Diag bus is running */

    if(diag_bus.state != DIAG_RUN)
    {
        /* Enable Diag bus */

        diag_bus.state = DIAG_RESET;
        diag_bus.n_resets = 0;                  // Clear number of resets counter
    }

    /* Test feedback */

    for(i=0 ; i<2 ; i++)                // channel A and B
    {
        /* Set channel */

        QSM_PORTQS_P = ((i+1) << 5);    // channel A or B (i+1 = 1 or 2)

        MSLEEP(20);                     // wait for 20 ms

        /* Check Data */

        data = diag_bus.data[DIAG_DATA_A+i];
        if(data != 0xBABE)
        {
            MenuRspError("Diag Data error (chan %c), got:0x%04X exp: 0xBABE", ('A'+i), data);
            return;
        }
    }
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFeedbackDiagAna(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  In reception racks, the DAC output is linked on DIPS VMEAS inputs. This test sets the DAC and checks the
  analogue Diag value.
\*---------------------------------------------------------------------------------------------------------*/
{
    int32_t      dac_value;
    uint8_t       i;

    /* Test bustype */

    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }

    /* Check that Diag bus is running */

    if(diag_bus.state != DIAG_RUN)
    {
        /* Enable Diag bus */

        diag_bus.state = DIAG_RESET;
        diag_bus.n_resets = 0;                  // Clear number of resets counter
    }

    for(i=0 ; i<3 ; i++)
    {
        /* Set DAC */

        dac_value = (int32_t)(i-1)*SLOT5_DAC_10V;        // -10, 0 and 10V
        dac_value <<= 12;                               // Value in DAC is left justified (bits 31-12)

        ANA_DAC_HIGH_P = ((uint16_t*)&dac_value)[0];
        ANA_DAC_LOW_P  = ((uint16_t*)&dac_value)[1];

        /* Wait for 20 ms */

        MSLEEP(20);

        /* Update analog values */

        DiagCheckAna();

        /* Get DIPS ana value */

        if(abs(diag_bus.ana[DIAG_VMEAS1] - (float)(i-1)*10)>0.5)
        {
            MenuRspError("Error on DAC readback %0.2f (tolerance 0.5V)", diag_bus.ana[DIAG_VMEAS1]);
            return;
        }
    }
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFeedbackDigitalTest(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Test Digital Commands and Status in reception rack. Write pattern in OP register and read back IP1 (should
  equal OP:OP)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       walk;
    uint8_t       i;
    uint16_t      test_vector;
    uint16_t      vector_got;

    /* Test bustype */

    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }

    /* Test bus acces */

    if((dev.reg_r | dev.reg_w) & REG_DIG)
    {
        MenuRspError("No access to Dig register");
        return;
    }

    /* walking bit test */

    for(walk=0 ; walk<2 ; walk++)
    {
        test_vector = (walk ? 0x0001 : 0x00FE);
        for(i=0 ; i<8 ; i++)
        {
            /* Set Command outputs */

            DIG_OP_P = 0xFF00;                          // reset Commands
            MSLEEP(1);
            DIG_OP_P = (~test_vector) & 0x00FF;

            /* Wait for 1ms - propagation through buffers and acces to bus */

            MSLEEP(2);

            /* Check Status inputs */

            vector_got = DIG_IP1_P;

            if(vector_got != (test_vector | (test_vector<<8)))
            {
                MenuRspError("Dig Command or Status failure: got 0x%04X exp 0x%04X",
                                                vector_got, (test_vector | (test_vector<<8)));
                return;
            }

            /* Prepare test_vector for next run */

            test_vector = ((test_vector << 1) | !walk) & 0x00FF;        // Shift and keep low byte
        }
    }
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuFeedbackInterlock(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  In reception racks, the Interlock outputs are mapped on the Interlock inputs. The outputs are set and
  the inputs are read back. Every signal is set and read through digital interface.
  Set CMD6 command to set OUTLINK, read back PC_PERMIT.
  Set OP bits 0 and 1 to set IP1 bits DCCTA_OK_NOT and DCCTB_OK_NOT to set PWRING_FAILURE, read back IN_INTLK.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              test_vector;
    uint16_t              got_ip2;

    /* Test bustype */

    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }

    /* Test bus acces */

    if((dev.reg_r | dev.reg_w) & REG_DIG)
    {
        MenuRspError("No access to Dig register");
        return;
    }

    /* Test Interlock signals */

    for(test_vector=0 ; test_vector<4 ; test_vector++)
    {
        /* Set OP CMD6 bit and IP1 DCCTA_OK and DCCTB_OK */

        DIG_OP_P = 0xFF00;
        DIG_OP_P = ((test_vector & 0x0001)<<6) | (((~test_vector) & 0x0002)>>1) | ((~test_vector) & 0x0002);

        /* Wait 100ms for the relays to be set */

        MSLEEP(100);

        /* Check Interlock inputs on IP2 */

        got_ip2 = DIG_IP2_P;

        if(((got_ip2>>14) ^ test_vector) & 0x02)        // compare bit15 of IP2 with bit1 of test_vetor
        {
            MenuRspError("Error on PWRING_FAILURE/IN_INTLK feedback");
            return;
        }

        if(((got_ip2>>12) ^ test_vector) & 0x01)        // compare bit12 of IP2 with bit0 of test_vetor
        {
            MenuRspError("Error on OUT_INTLK/PC_PERMIT feedback");
            return;
        }

    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuFeedback.c
\*---------------------------------------------------------------------------------------------------------*/


