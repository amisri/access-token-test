/*---------------------------------------------------------------------------------------------------------*\
  File:         menuRfc.c

  Purpose:      Functions to test the RFC-500 interface

  Author:       Quentin.King@cern.ch

  Notes:

  History:

    21/09/05    qak     Created
\*---------------------------------------------------------------------------------------------------------*/

#define MENURFC_GLOBALS

#include <menuRfc.h>

#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <menu.h>               // for MenuRspArg(), MenuGetInt16U(), MenuRspError()
#include <defconst.h>           // for FGC_INTERFACE_
#include <mem.h>                // for MemCpyWords()
#include <main.h>               // for CheckSlot5IfNot()
#include <crate_boot.h>         // for crate global variable
#include <fieldbus_boot.h>      // for stat_var global variable
#include <macros.h>             // for Set()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuRfcShowStatus(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show RFC channel status.  If auto triggering, the status is read synchronously by the ISR, otherwise
  it's read directly from the interface.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              i;
    uint16_t              channels;
    uint16_t              mask;
    uint16_t              status;
    char * FAR          label;
    struct sym_name *   lbl;

    if(CheckSlot5IfNot(FGC_INTERFACE_RFC_500,0))
    {
        return;
    }

    channels = (menurfc.trigger ? menurfc.trigger : 0xFFFF);

    for(i=0,mask=1;i < FGC_N_FGEN_CHANS;i++,mask<<=1)
    {
        status = (menurfc.trigger ? menurfc.status[i] : RFC_STATUS_A[i]);

        MenuRspArg("%2u,0x%04X",i,status);

        for(lbl = rfc_stat_lbl; lbl->label != NULL; lbl++)
        {
            label = lbl->label;

            if(lbl->type & status)
            {
                MenuRspArg("%-8s",label);
            }
            else
            {
                MenuRspArg("        ");
            }
        }
        MenuRspArg(NULL);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRfcManualTrig(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Manual trigger (only if not auto triggering)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      value;

    if(CheckSlot5IfNot(FGC_INTERFACE_RFC_500,0))
    {
        return;
    }

    if(menurfc.trigger)
    {
        MenuRspError("Auto trigger active");
        return;
    }

    if(MenuGetInt16U(argv[0], 0, 0xFFFF, &value))
    {
        return;
    }

    RFC_TRIGGER_P = value;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRfcSetData(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set channel data (only if not auto triggering)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      chan;
    uint16_t      value;

    if(CheckSlot5IfNot(FGC_INTERFACE_RFC_500,0))
    {
        return;
    }

    if(menurfc.trigger)
    {
        MenuRspError("Auto trigger active");
        return;
    }

    if(MenuGetInt16U(argv[0], 0, 15, &chan) || MenuGetInt16U(argv[1], 0, 0xFFFF, &value))
    {
        return;
    }

     RFC_CHANDATA_A[chan] = value;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRfcAutoTrig(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the RFC auto trigger.  If zero, auto triggering is disabled.
\*---------------------------------------------------------------------------------------------------------*/
{
    static char         spinner_left [8] = { '<', '-', '<', '/', '<', '|', '<', '\\' };
    static char         spinner_right[8] = { '-', '>', '\\', '>', '|', '>', '/', '>' };

    uint16_t              chan;
    uint16_t              value;
    uint16_t              mask;
    uint16_t              display[4];

    if(CheckSlot5IfNot(FGC_INTERFACE_RFC_500,0))
    {
        return;
    }

    if(MenuGetInt16U(argv[0], 0, 0xFFFF, &value))
    {
        return;
    }

    for(chan=menurfc.n_active_chans=0,mask=1;mask;mask <<= 1,chan++)
    {
        menurfc.data[chan] = (chan << 8);

        if(mask & value)
        {
            menurfc.n_active_chans++;
        }
    }

    sprintf((char*)&display,"  %04X  ",value);
    MemCpyWords(RFC_DISPLAY_16, (uint32_t)&display, 4);           // Word access only to RFC registers

    if(crate.side == 'L')                                       // Prepare display with spinner
    {
        menurfc.spinner      = (uint16_t *)&spinner_left;
        menurfc.spinner_addr = RFC_DISPLAY_16;
    }
    else
    {
        menurfc.spinner      = (uint16_t *)&spinner_right;
        menurfc.spinner_addr = RFC_DISPLAY_16 + 6;
    }


    OS_ENTER_CRITICAL();                                        // Enable/Disable triggering
    menurfc.counter = 0;
    menurfc.trigger = value;
    OS_EXIT_CRITICAL();
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void RfcIsrMst(void)
/*---------------------------------------------------------------------------------------------------------*\
  If auto triggering is active, increment channel data low bytes and then trigger RFC if required.
  This function must exit with XK:IX equal to zero (for the TermProcessHardware function which is called next).
\*---------------------------------------------------------------------------------------------------------*/
{
    static uint16_t       i;

    if(menurfc.trigger)
    {
        if(menurfc.counter && (menurfc.counter != RFC_COUNTER_P))
        {
            Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_RFC_FLT);
        }

        for(i=0;i < FGC_N_FGEN_CHANS;i++)
        {
            asm
            {
                LDE     i
                ASLE
                LDX     #RFC_CHANDATA_16
                LDY     @menurfc.status
                LDD     E,X
                STD     E,Y
                LDY     @menurfc.data
                LDD     E,Y
                INCB
                STD     E,Y
                STD     E,X
            }
        }

        if(!(SM_MSTIME_P % 50))
        {
            asm
            {
                INCW    menurfc.spinner_idx
                LDE     menurfc.spinner_idx
                ANDE    #0x0003
                ASLE
                LDX     menurfc.spinner
                LDY     menurfc.spinner_addr
                LDD     E,X
                STD     0,Y
            }
        }

        menurfc.counter = RFC_COUNTER_P + menurfc.n_active_chans;
        RFC_TRIGGER_P = menurfc.trigger;

        asm     LDX     #0x0000;                // Reset IX for SciIsrMsr
    }

}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuRfc.c
\*---------------------------------------------------------------------------------------------------------*/
