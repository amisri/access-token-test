/*---------------------------------------------------------------------------------------------------------*\
  File:         term_boot.c

  Purpose:      Serial I/O functions

  Author:       Quentin.King@cern.ch

  Notes:

  History:

    27/08/03    stp     Created based upon v2 boot serial I/O
    10/03/05    qak     File renamed sci.c.  Rx/Tx buffers introduced with IsrMst() resp for transfers to SCI
    25/07/07    pfr     ported for FGC3 boot (no asm)
\*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>              // for NULL
#include <term_boot.h>
#include <term.h>
#include <codes.h>              // for code global variable
#include <dev.h>                // for dev global variable
#include <memmap_mcu.h>         // Include memory map consts
#include <diag_boot.h>          // for diag_bus global variable
#include <iodefines.h>          // specific processor registers and constants
#include <main.h>               // for CheckStatus(), timeout_ms global variable
#include <fieldbus_boot.h>      // for FieldbusTxCh()
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <mcu_dependent.h>      // for MSLEEP(), USLEEP()
#include <fip_boot.h>           // for FipTxCh()
#include <definfo.h>

/*---------------------------------------------------------------------------------------------------------*/
void EnableTerminalStream(void)
/*---------------------------------------------------------------------------------------------------------*\
  This is called to set up the SCI interface and reset the terminal.

  For FGC2 it also inits QSPI bus
\*---------------------------------------------------------------------------------------------------------*/
{
    term.f.cb = TermPutc;        // Connect file stream to the character output callback

    // Init Serial Communication Interface for RS232 communications

    QSM_SCCR1_P = 0x0000;       // SCI disabled to clear Tx/Rx buffers
    QSM_SCCR0_P = 0x0034;       // SCI Tx/Rx speed 0x34 @ 16MHz = 9600 Baud
    QSM_SCCR1_P = 0x000C;       // SCI Tx/Rx enabled, no parity, 1 start & 1 stop bit
}
/*---------------------------------------------------------------------------------------------------------*/
void EnableQSPIbus(void)
/*---------------------------------------------------------------------------------------------------------*\
  This is called to set up the QSPI bus interface
\*---------------------------------------------------------------------------------------------------------*/
{

    uint16_t      ii;

    // Init QSPI for diag bus

    QSM_PORTQS_P = 0x00;        // If pin is define as output, the logic state is "0" (optional)
    QSM_PQSPAR_P = 0x03;        // MISO,MOSI are used for QSPI, the rest are PORTQ I/O
    QSM_DDRQS_P  = 0x7E;        // Inputs:MISO   Outputs:PCS3,PCS2,PCS1,PCS0,SCK,MOSI
    QSM_SPCR0_P  = 0x8110;      // QSPI master, 16 bit, CPHA 1, SCK inactive low, 500 Khz
    QSM_SPCR1_P  = 0x0000;      // DSCKL and DTL set to zero
    QSM_SPCR2_P  = 0x0F00;      // Scan from channels 0 and 1 only, no wrap

    for ( ii = 0; ii < 16; ii++)  // Initialise QSPI Transmit and Control zones
    {
        QSM_TR_A[ii] = 0xBABE;  // Can never be returned by a DIM (chan 3 doesn't exist)
        QSM_CR_A[ii] = 0x40;    // Set commands (size from SPCR0)
    }

    diag_bus.state = DIAG_RESET;  // Start running diag

//  the function QSPIbusStateMachine() will be called from the millisecond task at the start of every millisecond
//  with the previous state it will do a reset and a read of the devices attached to QSPI bus

/*
    while(diag_bus.state == DIAG_RESET) // wait until QSPIbusStateMachine() runs
    {
    };
    USLEEP(1000);                       // 1ms, leave time to QSPIbusStateMachine() to finish
*/
}
/*---------------------------------------------------------------------------------------------------------*/
void TermRxCh(uint8_t ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the Mst or fieldbus interrupt functions to queue a received terminal character.
  The handling depends upon the code.rx_mode:

  If code.rx_mode is SERIAL, then the character is a byte of code and it must be stored in the correct
  place in the next code block buffer.

  If code.rx_mode is not SERIAL, then the character is treated as a keyboard character.  The function
  detects if Ctrl-C is pressed and sets the abort flag if it is.  It also detects Xon/Xoff and sets or
  clears the xoff timeout.  This is used to block character transmission in TermProcessHardware().  If the buffer
  is full then the new character is discarded.

  This function is called with XK:IX=0x00000 so it must not use IX.  This requires it to be written in
  assembler.  It is also quicker which is important because it is running at interrupt level.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDAB    ch                              // B = ch
        LDE     #CODE_RX_TERMINAL
        CPE     code.rx_mode                    // if(code.rx_mode != CODE_RX_TERMINAL)
        BNE     rxterminal                      //    jump to treat terminal character

    /*--- Code download mode ---*/

        LDY     code.next_in_blk                // Y = address of next in block
        LDE     code.rx_term_idx                // E = rx_term_idx = offset inside new record
        STAB    E,Y                             // code.next_in_blk->byte[rx_term_idx] = ch

        INCW    code.rx_term_idx                // rx_term_idx++
        LDD     #CODE_SIZEOF_BLOCK
        CPD     code.rx_term_idx                // if rx_term_idx < CODE_SIZEOF_BLOCK
        BHI     end                             // jump to end

        CLRW    code.rx_term_idx                // Clear rx_term_idx
        STY     code.in_blk                     // code.in_blk = code.next_in_blk
        AIY     #CODE_SIZEOF_BLOCK              // code.next_in_blk++
        CPY     #RXCODEBUF_16+RXCODEBUF_B       // If code.next_in_blk < end of block buffers
        BCS     checkoutblk                     // skip forward
        LDY     #RXCODEBUF_16                   // code.next_in_blk = start of block buffers

    checkoutblk:
        STY     code.next_in_blk                // Save new next_in_blk
        LDD     code.in_blk                     // D = in_blk
        CPD     code.out_blk                    // if D != out_blk
        BNE     end                             // jump to end
        STY     code.out_blk                    // out_blk = next_in_blk
        BRA     end                             // jump to end

    /*--- Terminal character mode ---*/

    rxterminal:
        TSTB                                    // if(!ch || ch > 127)  /* Ignore null/8-bit characters */
        BEQ     end                             // {
        BMI     end                             //     return;
                                                // }
        CMPB    #3                              // if(ch == '\3')       /* CTRL-C = Abort */
        BNE     notabort                        // {
        STD     dev.abort_f                     //     dev.abort_f = 3;
        BRA     end                             //     return;
                                                // }
    notabort:
        CMPB    #19                             // if(ch == 19)         /* CTRL-S = XOFF */
        BNE     checkxon                        // {
        LDD     #TERM_XOFF_TIMEOUT_MS           //     term.xoff_timeout = TERM_XOFF_TIMEOUT_MS;
        STD     term.xoff_timeout               // }
        BRA     end

    checkxon:
        CMPB    #17                             // else if(ch == 17)    /* CTRL-Q = XON */
        BNE     storech                         // {
        CLRW    term.xoff_timeout               //      term.xoff_timeout = 0;
        BRA     end                             // }

    storech:
        LDD     term.rx.n_ch                    // else if(term.rx.n_ch < TERM_RX_BUF_SIZE)
        CPD     #TERM_RX_BUF_SIZE               // {
        BCC     end
        LDAB    term.rx.in                      //     term.rx.buf[term.rx.in++] = ch;
        INC     term.rx.in
        TDE
        LDY     @term.rx.buf
        LDAB    ch
        STAB    E,Y
        INCW    term.rx.n_ch                    //     term.rx.n_ch++;
                                                // }
    end:
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TermRxFlush(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will flush the character reception buffer while checking for CTRl-Z.  If at least one is
  included, it returns 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t ctrl_z_f = 0;

    OS_ENTER_CRITICAL();                                // Disable interrupts, except timers that are level 7

    while( term.rx.n_ch != 0 )                          // For all characters waiting in the buffer
    {
        if ( term.rx.buf[term.rx.out++] == 0x1A )       // If character is CTRL-Z
        {
            ctrl_z_f = 1;                               // Set the ctrl-Z flag
        }
        if (term.rx.out == TERM_RX_BUF_SIZE)
        {
            term.rx.out = 0;
        }
        term.rx.n_ch--;                                 // Decrement the buffer occupancy counter
    }

    OS_EXIT_CRITICAL();                                 // Enable interrupts

    return(ctrl_z_f);
}
/*---------------------------------------------------------------------------------------------------------*/
uint8_t TermGetCh(uint16_t to_ms)
/*---------------------------------------------------------------------------------------------------------*\
  This function will wait for the specified timeout period for a keyboard character from the TermIsr or
  FipIsr functions.  If the timeout expires, it will return zero, otherwise it returns the received
  character forced to lower case.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t ch = 0;                         // Initialise character to zero in case of timeout

    // ATTENTION timeout_ms is a global variable decremented in the 1ms tick
    timeout_ms = to_ms;

    while(    ( (timeout_ms != 0) || (to_ms == 0) )     // Wait for timeout to expire if set or
           && (term.rx.n_ch == 0)                       // for new keyboard character
         )
    {
        CheckStatus();                          // Poll status variables
        USLEEP(1000);                           // Sleep for 1ms
    }

    OS_ENTER_CRITICAL();                        // Disable interrupts, except timers that are level 7

    if ( term.rx.n_ch != 0 )                    // If character received
    {
        ch = term.rx.buf[term.rx.out++];        // Get character from the rx buffer
        term.rx.n_ch--;                         // Decrement the buffer occupancy counter
        if (term.rx.out == TERM_RX_BUF_SIZE)
        {
            term.rx.out = 0;
        }
    }

    OS_EXIT_CRITICAL();                         // Enable interrupts

    if ( (ch >= 'A') && (ch <= 'Z') )           // If upper case character
    {
        ch |= 0x20;                             // force to lower case
    }

    return(ch);                                 // Return character or zero if timeout occurred
}
// TODO Check if File parameter is neede. Remove the pragma if it isn't
#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void TermPutc(char ch, FILE *f)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the callback for the terminal output stream.  It will add a carriage return before every
  newline.
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( ch == '\n' )
    {
        TermTxCh('\r');
    }

    TermTxCh(ch);
}
/*---------------------------------------------------------------------------------------------------------*/
void TermTxCh(uint8_t ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the callback for the terminal output stream.
\*---------------------------------------------------------------------------------------------------------*/
{
    while ( term.tx.n_ch == TERM_TX_BUF_SIZE );            // Wait for queue to not be full

    OS_ENTER_CRITICAL();                                // Disable interrupts, except timers that are level 7

    term.tx.buf[term.tx.in++] = ch;                     // Insert character in the queue
    term.tx.n_ch++;                                     // Increment character counter

    OS_EXIT_CRITICAL();                                 // Enable interrupts
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void TermProcessHardware(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called every millisecond from the IsrMst() function to process Rx and Tx characters from
  the SCI.
  FGC2 : When called, XK:IX = 0x00000 to access HC16 registers.
\*---------------------------------------------------------------------------------------------------------*/
{
   asm
   {
        BRCLR   QSM_SCSRH_16,X,#QSM_SCSRH_TDRE_MASK8,rx // Skip if output buffer is full

        TSTW    term.tx.n_ch                    // Check if there are characters to send
        BEQ     rx                              // Jump to end if tx buffer is empty
        TSTW    term.xoff_timeout               // Check if XOFF is active
        BNE     xoff                            // Jump if XOFF was received

        LDY     @term.tx.buf                    // IY = pointer to start of tx buffer
        LDAB    term.tx.out                     // Get tx output index
        INC     term.tx.out                     // post increment output index
        CLRA                                    // Clear top byte of D
        TDE
        LDAB    E,Y                             // Get byte to send from buffer
        DECW    term.tx.n_ch                    // Decrement buffer length

        STAB    QSM_SCDRL_16,X                  // Put character into SCI output register
        JSR     FipTxCh                         // Call function to send character via FIP
        BRA     rx                              // Jump to receive character
    xoff:
        DECW    term.xoff_timeout               // Decrement Xoff timeout
    rx:
        BRCLR   QSM_SCSRL_16,X,#QSM_SCSRL_RDRF_MASK8,end // Skip if no character is waiting
        LDAB    QSM_SCDRL_16,X                  // Read character from input data register (QSM_SCDR)
        JSR     TermRxCh                         // Store the new character in the rx buffer
    end:
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sci_boot.c
\*---------------------------------------------------------------------------------------------------------*/
