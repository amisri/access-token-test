/*---------------------------------------------------------------------------------------------------------*\
  File:     fip_boot.c

  Purpose:  FGC boot - uFIP Interface Functions.

  Author:   Quentin.King@cern.ch
\*---------------------------------------------------------------------------------------------------------*/

#define FIP_BOOT_GLOBALS            // to instantiate fip global variable

#include <memmap_mcu.h>             // Include memory map consts
#include <fip_boot.h>
#include <definfo.h>
#include <pll_boot.h>               // for PllAlgoWithFipSynchro()
#include <main.h>                   // for timeout_ms global variable used by MSLEEP()
#include <codes.h>                  // for code global variable
#include <dev.h>                    // for dev global variable
#include <codes_holder.h>
#include <fgc_consts_gen.h>
#include <fgc_consts_gen.h>
#include <defconst.h>               // for FGC_OP_BOOT
// defconst.h is needed by fgc_fip.h because it includes fgc_code.h that needs FGC_MAX_DEVS_PER_GW
#include <fgc_fip.h>                // FIP communication structures
#include <iodefines.h>              // specific processor registers and constants
#include <macros.h>                 // for Test(), Set()
#include <fgc_runlog_entries.h>     // Include run log definition, for FGC_RL_
#include <term_boot.h>
#include <mem.h>                    // for MemCpyBytes()
#include <mcu_dependent.h>          // for USLEEP(), LED_SET(), LED_RST()
#include <fieldbus_boot.h>          // for fieldbus, stat_var global variable, FieldbusRxTime()


/*---------------------------------------------------------------------------------------------------------*/

// this one is for local use of fip.c and resides in it
// can't be on fip.h due to visibility problems from Stephen include files fgc_fip_....

// it is used by FipReadCodeVar() which is is called from IsrFip() ISR

struct fgc_fip_code     local_fip_code_var;         // FIP code variable

/*---------------------------------------------------------------------------------------------------------*/
void FipInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main() and CheckStatus() to prepare the uFIP interface.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  last_stadr;

    // --- test if FIP reg is accessible ---

    if ( ( dev.reg_w | dev.reg_r) & REG_FIP)    // If uFIP not accessible
    {
        return;                 // Return immediately
    }

    CPU_RESET_P &= ~CPU_RESET_CTRL_NETWORK_MASK16;          // Clear hard reset of uFIP

    FIP_RESET_P = 0;                // Request a soft reset (takes 1us)

    last_stadr = fip.stadr;         // Take copy of last reading of FIP address
    fip.stadr  = FIP_STADR_P;           // Get FIP address

    // the debugger, clears the memory so we get always last_stadr != FIP_STADR_P
    // this prevents the development version to automatically download codes
    // at start-up

    if (   (fip.stadr != last_stadr)        // If FIP address has changed or
        || (fip.stadr < 1)              // not in valid range (1-30)
        || (fip.stadr > 30)
       )
    {
        CPU_RESET_P |= CPU_RESET_CTRL_NETWORK_MASK16;           // Hard reset uFIP
        LED_RST(FIP_RED);           // Clear RED FIP LED
        dev.fieldbus_id = 0;                // return with fieldbus_id = 0
        return;
    }

    dev.fieldbus_id = fip.stadr;

    // Store REGIS bank register values

    FIP_CONFA_P = 0x05;         // No Msg ACK, 0x4: Msg snd disabled, 0x1: Msg rcv disabled
    FIP_CONFB_P = 0x80;         // 0x80: Var 7 rcv enabled, others disabled
    FIP_CONFC_P = 0x0A;         // RP time 4us, Field-drive, 0x8: WorldFIP frame, 0x2: 2.5MB/s
    FIP_CONFD_P = 0x0F;         // 0xE: Silence time 100us, 0x1: Var 7 uses global ID
    FIP_CONFE_P = 0x80;         // 0x80: IRQ on Var 7 rcv
    FIP_MPSPR_P = 0x00;         // Var 7 promptness 50ms, Var 6 refreshment 250ms
    FIP_VIDGL_P = 0x00;         // Var 7 global ID (low byte)
    FIP_VIDGH_P = 0x30;         // Var 7 global ID (high byte)

    // Store REGI2 bank register values

    FIP_ARCNL_P = 0x01;         // UART config (low byte):  40MHz clock, 2.5MHz bus
    FIP_ARCNH_P = 0x00;         // UART config (high byte): 40MHz clock, 2.5MHz bus

    // Set up COMBPS area to define variable lengths and buffers

    FIP_COBMPS_A[6] = 0x71;     // Var 6 (stat): Length = 7 blocks (56 bytes), first block = 1
    FIP_COBMPS_A[7] = 0x88;     // Var 7 (time): Length = 8 blocks (64 bytes), first block = 8

    FIP_CONFD_P    |= 0x80;     // Start uFIP interface

    LED_SET(FIP_RED);           // Turn on red FIP LED
}

/*---------------------------------------------------------------------------------------------------------*/
void FipWaitLinkUp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will wait for the link to be ready.
\*---------------------------------------------------------------------------------------------------------*/
{
    MSLEEP(100);
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
void FipISR(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will manage the interrupt from the FIP chip
  FGC2: to use the pragma NO_FRAME. must not have local variables
\*---------------------------------------------------------------------------------------------------------*/
{
    // FGC3: FIP_IRQSA_P is volatile so is not removed by the optimisation

    // read FIP IRQ reg to ack (clear) the irq
    if ( FIP_IRQSA_P )
    {
    }


    if ( FIP_VIDGL_P != 0 ) // Test FIP variable address (low byte) (0=Time 4=Code)
    {
        FipReadCodeVar();
    }
    else            // Time variable
    {
        FipReadTimeVar();   // Read time var
        FipWriteStatVar();  // Write status variable
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
void FipReadTimeVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from IsrFip() to read VAR7 (Time, ID=0x3000) from the uFIP into memory.
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( ( FIP_MPSSA_P & 0x80 ) != 0 )                      // If Var 7 is accessible...
    {
        FIP_CONFB_P &= 0x7F;                                // Disable FIP access to Var 7
        FIP_CONFD_P  = (7 << 4) | (FIP_CONFD_P & 0x8F);     // Identify Var 7 access to uFip

        // Copy complete 64 byte variable to memory
        MemCpyFip(                &time_var,        (void*) FIP_TIMEVAR_32, FIP_TIMEVAR_B);

        FIP_CONFB_P |= 0x80;                                // Re-enable Var 7

        if ( code.rx_mode == CODE_RX_FIELDBUS )             // If code reception from FIP enabled
        {
            FIP_VIDGL_P = 0x04;                             // Activate Code variable reception
        }                                                   // (ID=0x3004)

        FieldbusRxTime();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
void FipReadCodeVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from IsrFip() to read VAR7 (code, ID=0x3004) from the uFIP into memory.
\*---------------------------------------------------------------------------------------------------------*/
{

    if ( (FIP_MPSSA_P & 0x80) != 0 )                // If VAR7 is accessible...
    {
        FIP_CONFB_P &= 0x7F;                    // Disable FIP access to VAR7
        FIP_CONFD_P  = (7 << 4) | (FIP_CONFD_P & 0x8F);     // Identify VAR7 access to uFip

        MemCpyFip(                &(local_fip_code_var),        (void*) FIP_CODEVAR_16, FIP_CODEVAR_B); // Copy complete 64 byte variable to memory

        FIP_CONFB_P |= 0x80;                    // re-enable VAR7

        // ToDo: call FieldbusRxCode() only when we want to receive 'codes' ?
        // like with reception == true or code.rx_mode != CODE_RX_NONE
        FieldbusRxCode(FGC_FIELDBUS_CODE_BLOCKS_PER_MSG, local_fip_code_var.code);
    }

    FIP_VIDGL_P = 0x00;             // Reactivate Time variable reception (ID=0x3000)
}
/*---------------------------------------------------------------------------------------------------------*/
void FipWriteStatVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  this is called every 20ms by the FIP tick

  This function will write the status variable to the uFIP interface and will clear the rterm character
  buffer.  It also adjusts the status variable sequence number (Least significant 2 bits in ack byte).
  This goes 0, 1, 2, 3, 1, 2, 3, ....  Note that only the first packet has sequence # 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Prepare status variable

    if ( stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED )             // If PLL is "locked"
    {
        stat_var.fieldbus_stat.ack |= FGC_ACK_PLL;      // Set PLL bit in ACK byte
    }

    // Write status variable to uFIP

    if ( (FIP_MPSSA_P & 0x08) == 0 )                // If Var 6 is accessible...
    {
        FIP_CONFD_P = (6 << 4) | (FIP_CONFD_P & 0x8F);      // Identify Var 6 access to uFIP

    // Write state var buffer to uFIP
        MemCpyFip(       (void *) FIP_STATVAR_32,                &stat_var, FIP_STATVAR_B);

        FIP_COBMPS_A[6]  = 0x71;                // Re-assert Var 6 size/position
        FIP_CONFB_P     |= 0x08;                // Re-enable Var 6 transmission
    }

    stat_var.fgc_stat.class_data.boot.rterm[0] = '\0';      // Reset rterm buffer
    fieldbus.rterm_ch_idx = 0;                  // Reset rterm buffer index

    // Adjust status sequence number to go 0, 1, 2, 3, 1, 2, 3, 1, 2, 3, ...

    stat_var.fieldbus_stat.ack = (stat_var.fieldbus_stat.ack + 1) & FGC_ACK_STATUS; // Increment stat seq number

    if ( stat_var.fieldbus_stat.ack == 0 )          // If sequence has rolled back to zero
    {
        stat_var.fieldbus_stat.ack = 1;             // Advance to 1
    }
}
/*---------------------------------------------------------------------------------------------------------*/

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void FipTxCh(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermProcessHardware() when there is a character to send to the gateway remote terminal.
  It will add the character to the RTERM char buffer in the status variable to be sent to the gateway.
  It keeps the string null terminated by added a trailing zero after the character.  If the buffer is full,
  the new character is discarded.

  When called, XK:IX = 0x00000 to access HC16 registers, it should not be modified.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDE     fieldbus.rterm_ch_idx        // Get index in rterm buffer
        CPE     #22                     // Check if full
        BCC     end                     // Jump to end if full

        LDY     @stat_var.fgc_stat.class_data.boot.rterm        // Y = pointer to rterm char buf in stat var

        CLRA                            // B=ch A=0
        XGAB                            // A=ch B=0 -> D=0xch00
        STD     E,Y                     // Store null terminated character in buffer

        INCW    fieldbus.rterm_ch_idx        // Increment index
    end:
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: fip_boot.c
\*---------------------------------------------------------------------------------------------------------*/
