/*---------------------------------------------------------------------------------------------------------*\
  File:         menuGeneral.c

  Purpose:      Functions to run for the general boot menu options

  Author:       Stephen.Page@cern.ch

  Notes:

  History:

    13/04/04    stp     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdbool.h>
#include <menu.h>               // for MenuRspArg(), MenuGetInt16U(), MenuRspError()
#include <registers.h>          // for RegWrite(), RegRead()
#include <defconst.h>           // for FGC_CRATE_TYPE_
#include <crate_boot.h>         // for crate global variable
#include <codes_holder.h>       // for codes_holder_info[]
#include <main.h>               // for RunMainProg()
#include <stdio.h>              // for fprintf(), fputs()
#include <label.h>              // for BitLabel()
#include <memmap_mcu.h>         // for RL_RESETS_
#include <fieldbus_boot.h>      // for stat_var global variable
#include <diag_boot.h>          // for diag_bus global variable
#include <c32.h>                // for c32 global variable

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralRunMain(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    if(codes_holder_info[SHORT_LIST_CODE_MP_DSP].dev_state == CODE_STATE_VALID)
    {
        RunMainProg();                                          // Run main program
    }

    MenuRspError("Main program is not valid");
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralShowPsuVolts(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    if(diag_bus.state != DIAG_RUN)
    {
        MenuRspError("Diagnostic bus is not running");
        return;
    }

    fprintf(TERM_TX, RSP_DATA ",%5.2f,%5.2f,%5.2f\n",diag_bus.ana[DIAG_PSU5],   FGC_PSU_MIN_5V,  FGC_PSU_MAX_5V);
    fprintf(TERM_TX, RSP_DATA ",%5.1f,%5.1f,%5.1f\n",diag_bus.ana[DIAG_PSU15P], FGC_PSU_MIN_15V, FGC_PSU_MAX_15V);
    fprintf(TERM_TX, RSP_DATA ",%5.1f,%5.1f,%5.1f\n",diag_bus.ana[DIAG_PSU15N],-FGC_PSU_MAX_15V,-FGC_PSU_MIN_15V);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralShowResetReg(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      rst = CPU_RESET_P;

    MenuRspArg("0x%04X",rst);
    BitLabel(reset_reg_lbl,rst);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralShowFaults(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("0x%04X",stat_var.fgc_stat.class_data.c50.st_faults);
    BitLabel(faults_lbl,stat_var.fgc_stat.class_data.c50.st_faults);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralShowWarnings(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("0x%04X",stat_var.fgc_stat.class_data.c50.st_warnings);
    BitLabel(warnings_lbl,stat_var.fgc_stat.class_data.c50.st_warnings);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralShowLatchedStatus(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("0x%04X",stat_var.fgc_stat.class_data.c50.st_latched);
    BitLabel(st_latched_lbl,stat_var.fgc_stat.class_data.c50.st_latched);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralShowC32Status(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("0x%04X", dsp.status);
    BitLabel(c32_status_lbl, dsp.status);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralShowResets(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;

    for(i=0;i < RL_RESETS_W;i++)
    {
        MenuRspArg("%u",RL_RESETS_A[i]);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralCheckBpType(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralPeekWord(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t i;
    uint16_t value;
    uint16_t num_values = 1;
    uint32_t address;

    if(MenuGetInt32U(argv[0], 0, 0x000FFFFF, &address))         // Get address argument
    {
        return;
    }

    if(address & 0x00000001)                                    // If address is not word aligned
    {
        MenuRspError("Address is not word-aligned");
        return;
    }

    if(*argv[1] && MenuGetInt16U(argv[1], 1, 0xFFFF, &num_values))      // Check optional num_values arg
    {
        return;
    }

    for(i = 0;i < num_values;i++,address+=2)
    {
        if(RegRead(address,&value, true))
        {
            MenuRspError("Bus error reading 0x%05lX",address);
            return;
        }

        MenuRspArg("0x%04X", value);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralPokeWord(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t i;
    uint16_t value;
    uint16_t num_values = 1;
    uint32_t address;

    if(MenuGetInt32U(argv[0], 0, 0x000FFFFF, &address))         // Get address argument
    {
        return;
    }

    if(address & 0x00000001)                                    // If address is not word aligned
    {
        MenuRspError("Address is not word-aligned");
        return;
    }

    if(MenuGetInt16U(argv[1], 0, 0xFFFF, &value))               // Get value
    {
        return;
    }

    if(*argv[2] && MenuGetInt16U(argv[2], 0, 0xFFFF, &num_values))      // Get optional num_values arg
    {
        return;
    }

    for(i=0;i < num_values;i++,address+=2)
    {
        if(RegWrite(address,value, true))
        {
            MenuRspError("Bus error writing 0x%04X at 0x%05lX",value,address);
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuGeneral.c
\*---------------------------------------------------------------------------------------------------------*/
