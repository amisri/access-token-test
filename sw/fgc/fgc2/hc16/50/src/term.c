/*---------------------------------------------------------------------------------------------------------*\
  File:         term.c

  Purpose:      FGC3 - VT100/ANSI terminal support functions

  Author:       Quentin King & Martin Christophe

  Notes:        This file contains the functions for communications with an ANSI type terminal.

  History:

    31/10/00    qak     Port from HC16 version (was called sci.c)
    12/08/03    qak     TermSendCmd() adjusted to send unprocessed command line to Tst task
    02/04/04    stp     Ported back to HC16 for use in boot
\*---------------------------------------------------------------------------------------------------------*/

#define TERM_GLOBALS    // for edit_func[]

#include <stdint.h>

#include <term.h>       // for TERM_TX, for edit_func[]
#include <main.h>       // for ReportBoot(), ReportFieldbus()
#include <term_boot.h>   // for TermPutc()
#include <menu.h>       // for menu, MenuFollow()
/*---------------------------------------------------------------------------------------------------------*/
void TermGetMenuOption(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a menu branch is being processed.  The user needs to enter the menu option
  they want, or various control keys (e.g. backspace to return up one level).  When in direct mode, there
  is no visual echo of the selected command and command sequences must start with a $.
\*---------------------------------------------------------------------------------------------------------*/
{
    char        ch;
    uint16_t      menu_option;

    for(;;)                     // Loop until function returns
    {
        ch = TermGetCh(0);               // Get keyboard character

        if(term.edit_state)                     // If interactive
        {
            switch(ch)                                  // Switch according to character
            {
            case 0x08:                                          // [Backspace]
            case 0x7F:  menu.up_lvls = 1;       return;         // [Delete]     Return up on level

            case '!':                                           // [$]          Switch to direct mode
            case 0x1A:  TermDirect();           return;         // [CTRL-Z]     Switch to direct mode

            case 0x1B:  TermInteractive();      return;         // [ESC]        Reset in interactive

            case 0x0A:                                          // [Enter]
            case 0x0D:  MenuFollow(node);       return;         // [Return]     Follow children

            default:                                            // [Any]        Menu option character

                menu_option = ch - '0';

                if(menu_option < node->n_children)              // If option is valid
                {
                    menu.node_id[menu.depth++] = ch;
                    MenuMenu(node->children[menu_option]);
                    menu.depth--;
                    return;
                }
                else                                            // invalid option
                {
                    TermPutc('\a',TERM_TX);                              // Just beep
                }
            }
        }
        else                                    // else direct mode
        {
            if(ch != 0x1B && !term.recv_cmd_f)          // If not ESC and not yet receiving a command
            {
                if(ch == '$')                                   // If character is start of command delimiter
                {
                    term.recv_cmd_f = 1;                                // Flag reception of command as active
                }
                continue;                                       // Wait for next character
            }

            switch(ch)                                  // Switch according to character
            {
            case 0x1B:  TermInteractive();      return;         // [ESC]        Switch to interactive operation

            case 0x0A:                                          // [Enter]
            case 0x0D:  MenuFollow(node);       return;         // [Return]     Follow children

            default:                                            // [Any]        Menu option character

                menu_option = ch - '0';

                if(menu_option < node->n_children)              // If option is valid
                {
                    menu.node_id[menu.depth++] = ch;
                    MenuMenu(node->children[menu_option]);
                    menu.depth--;
                }
                else                                    // invalid option
                {
                    fputs(RSP_ERROR ",Invalid menu option\n",TERM_TX);  // Report error and
                    menu.up_lvls = menu.depth + 1;                      // return to top level
                }
                return;
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermRunFuncWithArgs(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  Run the terminal.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t ch;

    term.recv_args_f    = 1;                    // Enable receiving of args
    term.line_idx       = 0;                    // Reset line buffer cursor index
    term.line_end       = 0;                    // Reset line buffer end index
    menu.node           = node;                 // Save node for use by TermEnter

    if(term.edit_state)                         // If interactive mode
    {
        fprintf(TERM_TX,"%c,",menu.node_id[menu.depth-1]);      // Prompt user for arguments
    }

    while(term.recv_args_f)                     // While args reception is active
    {
        ch = TermGetCh(0);                                       // Get a character
        term.edit_state = edit_func[term.edit_state](ch);       // Process character according to edit state
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermEnter(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE0() or TermLE1() if Enter or Return been received.  It will run the
  function of the current leaf node using the line buffer for the arguments.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(term.edit_state)                         // If interactive
    {
        TermPutc('\n',TERM_TX);                          // Move cursor to begining of next line
    }

    term.linebuf[term.line_end] = '\0';         // Nul terminate line buffer

    MenuRunFunc(menu.node, term.linebuf);       // Run command with arguments

    term.recv_args_f = 0;                       // Terminate character input
}
/*---------------------------------------------------------------------------------------------------------*/
void TermInteractive(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will enable interactive mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    term.recv_args_f = 0;                   // Terminate character input
    menu.up_lvls = menu.depth+1;            // Request return to top level of menu
    ReportBoot();                           // Enable interactive mode and report FGC boot state
    ReportFip();                            // Report Fip ID, gateway and FGC name
}
/*---------------------------------------------------------------------------------------------------------*/
void TermDirect(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will enable direct mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    term.recv_args_f = 0;                   // Terminate character input
    menu.up_lvls     = menu.depth+1;        // Request return to top level of menu
    term.edit_state  = 0;                   // Enable direct mode
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TermLE0(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 0.  This state applies to direct mode in
  which characters are entered directly in the buffer and are not echoed to the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)                  // Switch according to the key pressed
    {
        case 0x0A:              // [Enter]
        case 0x0D:              // [Return]     Run node function
            TermEnter();
            break;

        case 0x1B:              // [ESC]        Switch to interactive operation
            TermInteractive();
            return(1);

        default:                // [others]     Insert character in buffer
            term.recv_args_f = TermInsertChar(ch);
    }

    return(0);                  // Continue with edit state 0
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TermLE1(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 1. This is interactive mode in which characters
  are echoed to the terminal and simple line editing functions are supported. In state 1, the character
  is analysed directly.  If the ESC code (0x1B) is received, the state changes to 2, otherwise the
  character is processed and the state remains 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)                                  // Switch according to the key pressed
    {
    case 0x01:  TermStartOfLine();      break;          // [CTRL-A]     Move to start of line

    case 0x05:  TermEndOfLine();        break;          // [CTRL-E]     Move to end of line

    case 0x08:                                          // [Backspace]
    case 0x7F:  TermDeleteLeft();       break;          // [Delete]     Delete left

    case 0x04:  TermDeleteRight();      break;          // [CTRL-D]     Delete right

    case 0x1A:  TermDirect();           return(0);      // [CTRL-Z]     Switch to Direct mode

    case 0x0A:                                          // [Enter]
    case 0x0D:  TermEnter();            return(1);      // [Return]     Send command

    case 0x1B:                          return(2);      // [ESC]        Start escape sequence

    default:    term.recv_args_f = TermInsertChar(ch);  // [others]     Insert character
    }

    return(1);                                  // Continue with edit state 1
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TermLE2(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 2. The previous character was [ESC].
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)                                  // Switch according to next code
    {
    case 0x5B:                          return(3);      // [Cursor/Fxx] Change state to analyse

    case 0x4F:                          return(5);      // [PF1-4]      Change state to ignore
    }

    return(1);                                  // All other characters - return to edit state 1
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TermLE3(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 3. The key was either a cursor key or a function
  key.  Cursors keys have the code sequence "ESC[A" to "ESC[D", while function keys have the code
  sequence "ESC[???~" where ??? is a variable number of alternative codes.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)                                  // Switch according to next code
    {
    case 0x41:                          return(1);      // [Up]

    case 0x42:                          return(1);      // [Down]

    case 0x43:  TermCursorRight();      return(1);      // [Right]      Move cursor right

    case 0x44:  TermCursorLeft();       return(1);      // [Left]       Move cursor left
    }

    return(4);                                  // Function key - change to edit state 4
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TermLE4(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 4.  Orignial Key was a Function with sequence
  terminated by 0x7E (~).  However, the function will also accept a new [ESC] to allow an escape route
  in case of corrupted reception.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(ch)                                  // Switch according to next code
    {
    case 0x1B:                          return(1);      // [ESC]        Escape to edit state 1

    case 0x7E:                          return(1);      // [~]          Sequence complete - return to state 1
    }

    return(4);                                  // No change to edit state
}
#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TermLE5(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 5.  The original key was PF1-4 and one more
  character must be ignored.
\*---------------------------------------------------------------------------------------------------------*/
{
    return(1);                                  // Return to edit state 1
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TermInsertChar(char ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE0() or TermLE1() if a standard character has been received.
  It returns the new value for term.recv_args_f (0=stop 1=continue).
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;

    /*--- Direct mode ---*/

    if(!term.edit_state)                // If direct mode
    {
        if(ch >= 0x20 && ch < 0x80)             // If character is printable 7-bit ASCII symbol
        {
            if(term.line_end < (TERM_LINE_SIZE-2))      // If line buffer is NOT full...
            {
                term.linebuf[term.line_end++] = ch;     // Save new character in line buffer
                return(1);                              // Return 1 to continue line entry
            }

            fputs(RSP_ERROR ",Argument buffer overflow\n",TERM_TX);
        }
        else                                    // else charcter not printable and if non-interactive
        {
            fprintf(TERM_TX,RSP_ERROR ",Invalid character: 0x%02X\n",ch);
        }
        return(0);                              // Return zero to end line entry
    }

    /*---Interactive mode ---*/

    if(ch >= 0x20 && ch < 0x80)                 // If character is printable 7-bit ASCII symbol
    {
        if(term.line_end < (TERM_LINE_SIZE-3))          // If line buffer is NOT full...
        {
            for(i=term.line_end++;i > term.line_idx;i--)// For the rest of the line (backwards)
            {
                term.linebuf[i] = term.linebuf[i-1];            // Shift characters one column in buffer
            }

            term.linebuf[term.line_idx] = ch;           // Save new character in line buffer

            for(i=term.line_idx++;i < term.line_end;i++)// For the rest of the line (forwards)
            {
                TermPutc(term.linebuf[i],TERM_TX);               // Output chars to screen
            }

            i = term.line_end - term.line_idx;
            if ( i != 0 )       // If cursor is now offset from true position
            {
                fprintf(TERM_TX, "\33[%uD",i);          // Move cursor back
            }
        }
        else                                    // else buffer is full
        {
            TermPutc('\a',TERM_TX);                      // Just beep
        }
    }

    return(1);                                  // Return 1 to continue line entry
}
/*---------------------------------------------------------------------------------------------------------*/
void TermCursorLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE3() if the Cursor left key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!term.line_idx)                  // If cursor is already at the start of the line
    {
        TermPutc('\a',TERM_TX);                   // Just beep
    }
    else                                // else
    {
        TermPutc('\b',TERM_TX);                  // Move cursor left one character
        term.line_idx--;                // Decrement cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermCursorRight(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE3() if the Cursor right key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(term.line_idx == term.line_end)          // If cursor is already at the end of the line
    {
        TermPutc('\a',TERM_TX);                          // Just beep
    }
    else                                        // else
    {
        fputs("\33[C",TERM_TX);                         // Move cursor right one character
        term.line_idx++;                                // Increment cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermStartOfLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the Ctrl-A key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(term.line_idx)                           // If cursor is not already at the start of the line
    {
        fprintf(TERM_TX ,"\33[%uD", term.line_idx);     // Move cursor the required number of columns left
        term.line_idx = 0;                              // Reset cursor position index
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermEndOfLine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the Ctrl-E key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;

    i = term.line_end - term.line_idx;

    if ( i != 0 )       // If cursor is not already at the end of the line
    {
        fprintf(TERM_TX, "\33[%uC", i);                 // Move cursor the required number of columns right
        term.line_idx = term.line_end;                  // Set cursor position index to end of line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermDeleteLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the backspace or delete keys have been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!term.line_idx)                                  // If cursor is already at the start of the line
    {
        term.recv_args_f = 0;                           // Leave terminal mode;
    }
    else                                                // else
    {
        TermPutc('\b',TERM_TX);                          // Cursor left
        term.line_idx--;                                // Adjust cursor position index
        TermShiftRemains();                             // Shift remains of line left
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermDeleteRight(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the Ctrl-D key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(term.line_idx == term.line_end)          // If cursor is already at the end of the line
    {
        TermPutc('\a',TERM_TX);                          // Just beep
    }
    else                                        // else
    {
        TermShiftRemains();                             // Shift remains of line left
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermShiftRemains(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermDeleteLeft() and TermDeleteRight() to shift the remains of the line
  one character to the left.
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t  diff;
    uint16_t left_shift;
    uint16_t i;

    term.line_end--;                            // Adjust end of line index

    for(i=term.line_idx;i < term.line_end;i++)  // For the remainder of the line
    {
        TermPutc((term.linebuf[i]=term.linebuf[i+1]),TERM_TX);// Shift character in buffer and display it
    }

    TermPutc(' ',TERM_TX);                       // Clear last character

    // Move cursor the required number of columns left
    // TODO Can this ever be negative?

    diff       = term.line_end - term.line_idx;
    left_shift = (diff < 0 ? 0 : (1 + (uint16_t)diff));

    fprintf(TERM_TX, "\33[%uD", left_shift);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: term.c
\*---------------------------------------------------------------------------------------------------------*/
