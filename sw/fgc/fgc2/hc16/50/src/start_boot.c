
/*---------------------------------------------------------------------------------------------------------*\
  File:         start.c

  Purpose:      Digital controller HC16 Boot Software - Start up function

  Author:       Quentin.King@cern.ch,
\*---------------------------------------------------------------------------------------------------------*/


#include <start_boot.h>
#include <start16.h>            // Hiware linker structure declaration
#include <term.h>               // for TERM_TX
#include <memmap_mcu.h>         // for SIM_SYPCR_16, SIM_SYNCR_16, SIM_MCR_16
#include <reset.h>              // for ResetPeripherals()
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <stdio.h>              // for fputs()
#include <mcu_dependent.h>      // for MSLEEP()
#include <main.h>               // for timeout_ms global variable used by MSLEEP()

/*---------------------------------------------------------------------------------------------------------*/

struct _tagStartup _startupData;

/*---------------------------------------------------------------------------------------------------------*/

#pragma NO_STRING_CONSTR        // Stop #xxx in macro from making a string literal "xxx" (needed for asm)

#pragma CODE_SEG START_UP
#pragma NO_FRAME
#pragma NO_EXIT
/*---------------------------------------------------------------------------------------------------------*/
void _Startup(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the first to be executed when the boot starts.  It must configure the critical HC16
  registers, the GPT I/O lines and the SIM registers.  It then jumps to ResetPeripherals in the FGC library
  to reset the uFIP, C62 and C32, which then jumps to StartMain() to set up the C context.

  ***** No calls to subroutines are possible in this function because the stack pointer is not set up *****
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        /* Set critical HC16 control registers */

        LDAB    #0xF                            // Set
        TBEK                                    // EK = F

        LDAA    #0x04                           // SYPCR: watch dog disabled, BM enabled 64 clk
        STAA    SIM_SYPCR_16

        LDE     #0x7F00                         // SYNCR: syncr w=0, x=1 16.0 mhz
        STE     SIM_SYNCR_16

        LDE     #0x40CF                         // SIMCR: mcr frzsw enable, frzmb disabled, MM at ffff
        STE     SIM_MCR_16

        /* Configure GPT Bit 7, 6, 5 as outputs to 0
           This is used to prevent the 3 lines TRIGWDMS, TRIGDIGITAL & TRIGANALOG
           to float and generate false activity as a Fast watchdog reset */

        LDE     #0xFE00
        STE     GPT_PDDR_16                     // Set bit 0 (IC1) as input, bits 1-7 as outputs

        /* Configure Port F to drive LEDs */

        LDAA    #0x01                           // All port F except MODCLK configured as I/O
        STAA    SIM_PFPAR_16

        LDAA    #0x1E                           // Bits 1-4 are outputs to drive LEDs, the rest are inputs
        STAA    SIM_DDRF_16

        LDE     #0x0000                         // All LEDs on
        STE     SIM_PORTF_16

        /* Configure Chip Select Widths */

        LDE     #0x0003                         // CSPAR0: CSBoot: 16-bit  CS0-5: discrete outputs
        STE     SIM_CSPAR0_16

        LDE     #0x0000                         // CSPAR1: CS6-10: discrete outputs
        STE     SIM_CSPAR1_16

        CLRW    SIM_CSBARBT_16                  // Disable CSBaseAdressRegisterBoot
        CLRW    SIM_CSORBT_16                   // Disable CSBoot
        CLRW    SIM_CSOR0_16                    // Disable CS0
        CLRW    SIM_CSOR1_16                    // Disable CS1
        CLRW    SIM_CSOR2_16                    // Disable CS2
        CLRW    SIM_CSOR3_16                    // Disable CS3
        CLRW    SIM_CSOR4_16                    // Disable CS4
        CLRW    SIM_CSOR5_16                    // Disable CS5
        CLRW    SIM_CSOR6_16                    // Disable CS6
        CLRW    SIM_CSOR7_16                    // Disable CS7
        CLRW    SIM_CSOR8_16                    // Disable CS8
        CLRW    SIM_CSOR9_16                    // Disable CS9
        CLRW    SIM_CSOR10_16                   // Disable CS10

        /* Initialise internal SRAM */

        LDD     #0x00FF                         // Set SRAM high base address: always page F
        STD     RAM_BA_H_16
        LDD     #INTRAM_16                      // Set SRAM low base address
        STD     RAM_BA_L_16
        LDD     #0x0800                         // Enable internal SRAM (this can only be done once)
        STD     RAM_MCR_16

        JMP     ResetPeripherals                // Reset uFIP, C62 and C32 then jumps to StartMain()
    }
}
#pragma CODE_SEG DEFAULT

/*---------------------------------------------------------------------------------------------------------*/
#pragma CODE_SEG SWITCH
#pragma NO_FRAME
#pragma NO_EXIT
/*---------------------------------------------------------------------------------------------------------*/
void StartSwitchBoot(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to switch operation of the boot from flash BA01 to flash BB01 and back again.
  It must be present identically in both flashes so that the switch does not perturb the opcode fetch
  of the HC16.  Having made the switch, it then runs Startup to relaunch the boot from the other flash
  device.
\*---------------------------------------------------------------------------------------------------------*/
{
    fputs(RSP_DATA ",Switching to other boot...\n" RSP_OK "\n",TERM_TX);

    MSLEEP(term.edit_state ? 1000 : 100);       // Wait to allow reponse to be sent (and read)

    OS_ENTER_CRITICAL();                        // Stop interrupts

    CPU_RESET_P |= CPU_RESET_CTRL_DSP_MASK16;                           // C32 Reset
    asm NOP;
    CPU_RESET_P |= CPU_RESET_CTRL_DSPTSM_MASK16;                        // C32 Reset+Tristate

    CPU_MODEL_P ^= CPU_MODEL_TSTBOOT_MASK8;     // Flip TSTBOOT bit in mode register

    asm
    {
        NOP                                     // NOPs to allow for the HC16 pre-fetch pipeline
        NOP
        NOP
        NOP

        JMP     _Startup                        // Restart boot in other flash
    }
}
#pragma CODE_SEG DEFAULT
/*---------------------------------------------------------------------------------------------------------*\
  End of file: start.c
\*---------------------------------------------------------------------------------------------------------*/

