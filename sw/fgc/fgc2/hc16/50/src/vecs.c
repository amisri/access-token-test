/*---------------------------------------------------------------------------------------------------------*\
  File:         vecs.c

  Contents:     This file contains intermediate interrupt functions for the Boot program.

  Notes:        The intermediate interrupt functions are defined in a special code segment (VEC_TABLE)
                which is allocated in the VEC_TABLE section in the PRM file.  This is assigned to addresses
                0x00200 - 0x003FF.

                Only unexpected interupts require intermediate interrupts functions. The intermediate
                function sets a code in register D to identify the type of interrupt, which is then
                saved in the run log by the IsrTrap() function.

                The uFIP IRQ interrupt (GPT IC1), Millisecond tick interrupt (GPT OC2) and bus error
                interrupt do not pass via intermediate interrupt functions.

  History:

    05/11/99    qak     Created from version in main software
    08/08/00    qak     Tidied up
    28/05/04    qak     Only include unexpected interrupts in this list
\*---------------------------------------------------------------------------------------------------------*/

extern void     IsrTrap         (void); // trap.h
extern void     isr             (void);
extern void     IsrBusError     (void);
extern void     IsrFip          (void);
extern void     IsrTick         (void);

#pragma NO_STRING_CONSTR

#define TRAP_VEC(id)            void trap_vec_##id(void){asm LDD # id;asm JMP IsrTrap;}
#define ISR_VEC(id,isr)         void isr_vec_##id(void){asm JMP isr;}

#pragma CODE_SEG VEC_TABLE

/* Intermediate vector functions */

#pragma NO_ENTRY
#pragma NO_EXIT
TRAP_VEC(0x04)                  // 0x04         Breakpoint
#pragma NO_FRAME
#pragma NO_EXIT
ISR_VEC(0x05,IsrBusError)       // The rest     All other sources
#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x06)                  // 0x06         Software Interrupt
#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x07)                  // 0x07         Illegal instruction
#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x08)                  // 0x08         Division by zero
#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x0F)                  // 0x0F         Unitialised interrupt
#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x11)                  // 0x11         IRQ1-7
#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x18)                  // 0x18         Spurious interrupt
#pragma NO_FRAME
#pragma NO_EXIT
ISR_VEC(0x31,IsrFip)            // 0x31         GPT IC1
#pragma NO_FRAME
#pragma NO_EXIT
ISR_VEC(0x35,IsrTick)           // 0x35         GPT OC2
#pragma NO_FRAME
#pragma NO_EXIT
TRAP_VEC(0x00)                  // The rest     All other sources

#pragma CODE_SEG DEFAULT

/*---------------------------------------------------------------------------------------------------------*\
  End of file: vecs.c
\*---------------------------------------------------------------------------------------------------------*/

