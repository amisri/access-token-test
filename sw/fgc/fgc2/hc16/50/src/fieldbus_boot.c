/*---------------------------------------------------------------------------------------------------------*\
  File:     fieldbus_boot.c

  Purpose:  Common Network Interface Functions
            Wraper functions to access the right hardware, FIP or ethernet

\*---------------------------------------------------------------------------------------------------------*/

#define FIELDBUS_BOOT_GLOBALS

#include <stdbool.h>
#include <fieldbus_boot.h>      // for stat_var global variable
#include <fip_boot.h>           // for FipISR(), FipInit(), FIP_INIT_TIMEOUT
// #include <stdio.h>           // for fprintf()
#include <pll_boot.h>           // for GATEWAY_ALIVE_TIMEOUT_MS

#include <fgc_runlog_entries.h> // for FGC_RL_
#include <runlog_lib.h>         // for RunlogWrite()
#include <term_boot.h>
#include <codes.h>              // for code global variable
#include <dev.h>                // for dev global variable
#include <mem.h>                // for MemCpyBytes()
#include <definfo.h>            // for FGC_CLASS_ID
#include <shared_memory.h>      // for shared_mem global variable
#include <memmap_mcu.h>         // for SM_UXTIME_P, UTC_*
#include <mcu_dependent.h>

/*---------------------------------------------------------------------------------------------------------*/
void FieldbusInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main() and CheckStatus() to prepare the network interface.
\*---------------------------------------------------------------------------------------------------------*/
{
    stat_var.fgc_stat.class_data.CLASS_FIELD.state_op = FGC_OP_BOOT;    // Initialise operational state

    fieldbus.progress_refresh_rate = 100; // each second
    FieldbusISR = FipISR;
    FieldbusTxCh = FipTxCh;
    FieldbusWaitLinkUp = FipWaitLinkUp;

    FipInit();                      // Start network interface

    stat_var.fieldbus_stat.class_id = FGC_CLASS_ID;         // Boot class ID

    if (DevIdValid())
    {
        RunlogWrite(FGC_RL_FIP_ID, (void *)&dev.fieldbus_id);   // Store FIP station ID in run log
    }

    MemSetBytes(&stat_var.fieldbus_stat.diag_data, 0xFF, sizeof(stat_var.fieldbus_stat.diag_data));

    CodesInitAdvTable();
}

/*---------------------------------------------------------------------------------------------------------*/
void FieldbusEnableInterrupts(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function enables network interrupt
\*---------------------------------------------------------------------------------------------------------*/
{
}

/*---------------------------------------------------------------------------------------------------------*/
void FieldbusRxTime(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function process the time received data
\*---------------------------------------------------------------------------------------------------------*/
{
    struct advertised_code_info * ac;

    // to say GW is present 2 consecutive packets are required

    if ((fieldbus.is_gateway_online == false) && (fieldbus.gw_timeout_ms != 0))
    {
        fieldbus.is_gateway_online = true;                  // Mark gateway as online
        SM_UXTIME_P = time_var.unix_time;                   // Update UTC time
        SM_MSTIME_P = time_var.ms_time;

        if (SM_ONTIME_P == 0)                               // If Power on time never set
        {
            SM_ONTIME_P = SM_UXTIME_P;
        }

        RunlogWrite(FGC_RL_UXTIME, (void *) &time_var.unix_time);
        RunlogWrite(FGC_RL_MSTIME, (void *) &time_var.ms_time);
        RunlogWrite(FGC_RL_GW_STATE, &fieldbus.is_gateway_online);      // Log gw state in run log
    }

    if (time_var.runlog_idx != fieldbus.last_runlog_idx)    // If runlog_idx has changed
    {
        stat_var.fieldbus_stat.ack   |= FGC_ACK_TIME;               // Ack time reception
        fieldbus.last_runlog_idx      = time_var.runlog_idx;        // Remember new runlog index

        // this is the byte by byte transmission of the runlog to the gateway
        stat_var.fieldbus_stat.runlog = RL_BUF_A[time_var.runlog_idx];  // Return one byte from runlog

        if (time_var.fgc_id == dev.fieldbus_id)                 // If remote terminal kbd char received
        {
            TermRxCh(time_var.fgc_char);                         // Submit ch to rx buffer
        }

        if (time_var.adv_list_idx < FGC_CODE_MAX_SLOTS)         // If adv list index is in valid range
        {
            ac = code.adv + time_var.adv_list_idx;              // Get pointer to adv code structure

            ac->class_id = time_var.adv_code_class_id;          // Transfer advertised code info
            ac->code_id  = time_var.adv_code_id;                // to adv code structure
            ac->version  = time_var.adv_code_version;           // Advertised code version
            ac->crc = time_var.adv_code_crc;                    // Code CRC16
            ac->len_blks = time_var.adv_code_len_blk;
            ac->old_version = time_var.old_adv_code_version;
        }

        fieldbus.gw_timeout_ms = GATEWAY_ALIVE_TIMEOUT_MS;      // Reset gw timeout counter
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void FieldbusRxCode(uint16_t nb_code_block, uint8_t p_code[][FGC_CODE_BLK_SIZE])
/*---------------------------------------------------------------------------------------------------------*\
    This function process the received code
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t ii;

    if ((time_var.code_var_id < FGC_NUM_CODES)          // If code ID and block index are valid
        && (time_var.code_var_idx < fgc_code_sizes_blks[time_var.code_var_id])
       )
    {
        for (ii = 0; ii < nb_code_block; ii++)      // For each code block in the code variable
        {


            if (code.out_blk == code.next_in_blk)       // If code buffer has overrun
            {
                return; // discard end of packet
            }
            else
            {
                code.in_blk = code.next_in_blk;

                // increment the circular buffer pointer, if it is beyond the buffer area reset it
                code.next_in_blk++;

                if ((uint16_t)code.next_in_blk >= (RXCODEBUF_16 + RXCODEBUF_B))
                {
                    code.next_in_blk = RXCODEBUF_A;
                }

                code.in_blk->class_id  = time_var.code_var_class_id;    // Copy code header from time var
                code.in_blk->code_id   = time_var.code_var_id;
                code.in_blk->block_idx = time_var.code_var_idx++;

                // Copy into the buffer the code contained in code_var
                MemCpyBytes((uint8_t *) &code.in_blk->code,
                            p_code[ii], // this is &code_var.code[ii][0] &local_fip_code_var.code[ii][0]
                            FGC_CODE_BLK_SIZE);
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void FieldbusWatchDog(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function periodically check the network status and reset the network if not responding

  ToDo : check if we want the same mechanism for ethernet, then move fip.init_timeout and FIP_INIT_TIMEOUT
        to fieldbus level.
\*---------------------------------------------------------------------------------------------------------*/
{
    {
        fip.init_timeout++;

        if (fip.init_timeout > FIP_INIT_TIMEOUT)
        {
            FieldbusInit();             // Try to initialise the FIP connection
            fip.init_timeout = 0;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: fieldbus_boot.c
\*---------------------------------------------------------------------------------------------------------*/
