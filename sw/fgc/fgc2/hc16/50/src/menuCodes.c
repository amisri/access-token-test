/*---------------------------------------------------------------------------------------------------------*\
  File:         menuCodes.c

  Purpose:      Functions for the Codes submenu

  Author:       Quentin.King@cern.ch

  Notes:

  History:

    13/04/04    qak     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdbool.h>
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <stdio.h>              // for fprintf(), fputs()
#include <menu.h>               // for MenuGetInt16U(), MenuRspError()
#include <codes_holder.h>       // for codes_holder_info[]
#include <dev.h>                // for dev global variable
#include <codes_mcu.h>          // for WriteMcuBootDev()
#include <codes.h>              // for CodesCheckUpdates()
#include <codes_fgc.h>          // for check_dev[], program_dev[], erase_dev[]
#include <fgc_codes_gen.h>      // for fgc_code_names[]
#include <term.h>               // for TERM_TX
#include <mcu_dependent.h>      // for SELFLASH()
#include <fieldbus_boot.h>      // for fieldbus global variable
#include <mem.h>                // for MemSetWords() for FGC3
#include <flash.h>

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesShowInstalled(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Report installed codes
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dev_idx;

    CodesCheckUpdates();

    fputs(RSP_LABEL "," TERM_UL "FLASH ZONE|    RESIDENT CODE   |CLS:CODE:CALC:STATE     |CODE AFTER UPDATE |\n" TERM_NORMAL,TERM_TX);

    for(dev_idx=0;dev_idx < SHORT_LIST_CODE_LAST;dev_idx++)
    {
        struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;

        if(code_info->update.version != 0 || code_info->update_state == UPDATE_STATE_UPDATE)
        {
            fputs(TERM_BOLD,TERM_TX);
        }

        fprintf(TERM_TX, RSP_DATA ",%2u:%-7s|%-20s|%3u:%04X:%04X:%-10s|%-7s|%04X      |\n"
                RSP_DATA "," TERM_UL "          |          %10lu|     %19s|       |%10lu|\n" TERM_NORMAL,
                dev_idx,
                code_info->label,
                fgc_code_names[code_info->update.code_id],
                code_info->dev.class_id,
                code_info->dev.crc,
                code_info->calc_crc,
                TypeLabel(code_state, code_info->dev_state),
                TypeLabel(update_state, code_info->update_state),
                code_info->update.crc,
                (code_info->dev.version == 0xFFFFFFFF ? (uint32_t)code_info->dev.old_version : code_info->dev.version),
                (char * FAR)code_info->dev.version_string,
                (code_info->update.version == 0xFFFFFFFF ? (uint32_t)code_info->update.old_version : code_info->update.version));
    }

    fprintf(TERM_TX,RSP_DATA ",Writable devices:%u,Read-only devices:%u\n",
                dev.n_rw_devs_to_update, dev.n_ro_devs_to_update);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesShowAdvertised(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show advertised code table received from the gateway and indicate which code is being transmitted.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      adv_idx;
    uint16_t      fip_class_id;                   // Class ID in most recent time var
    uint16_t      fip_code_id;                    // Code ID in most recent time var
    uint16_t      fip_blk_idx;                    // Code block index in most recent time var
    uint16_t      blk_idx;

    if ( !fieldbus.is_gateway_online )
    {
        MenuRspError("GW offline");
        return;
    }

    CodesCheckUpdates();

    OS_ENTER_CRITICAL();

    fip_class_id = time_var.code_var_class_id;  // Take local copy of most recent time var code header
    fip_code_id  = time_var.code_var_id;
    fip_blk_idx =  time_var.code_var_idx;

    OS_EXIT_CRITICAL();

    for ( adv_idx = 0; adv_idx < FGC_CODE_MAX_SLOTS; adv_idx++) // or FGC_NUM_CODES ?
    {
        struct advertised_code_info * code_adv = code.adv + adv_idx;

        if(code.state[adv_idx] == ADV_CODE_STATE_OK &&
           code_adv->class_id  == fip_class_id &&
           code_adv->code_id   == fip_code_id)
        {
            fputs(TERM_BOLD,TERM_TX);
            blk_idx = fip_blk_idx;
        }
        else
        {
            blk_idx = 0;
        }

        fprintf(TERM_TX, RSP_DATA ",%2u:%-15s:%10lu:%04X:%5u:%5u:%s\n" TERM_NORMAL,
            adv_idx,            (code_adv->code_id < FGC_NUM_CODES ? fgc_code_names[code_adv->code_id] : ""),
            code_adv->version != 0xFFFFFFFF ? code_adv->version: code_adv->old_version,
            code_adv->crc,
            blk_idx,
            code_adv->len_blks,
            TypeLabel(adv_code_state, code.state[adv_idx])
            );
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesFieldbusUpdate(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Update all codes from Fieldbus.
\*---------------------------------------------------------------------------------------------------------*/
{
    CodesCheckUpdates();
    CodesUpdate(CODE_RX_FIELDBUS);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesTerminalUpdate(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Update one code from terminal.
\*---------------------------------------------------------------------------------------------------------*/
{
    CodesUpdate(CODE_RX_TERMINAL);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesToggleFlashLock(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Toggle flash device lock
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dev_idx;
    uint8_t *FAR  dev_lock;

    if(MenuGetInt16U(argv[0], 0, (SHORT_LIST_CODE_LAST-1), &dev_idx))// Get flash device index
    {
        return;
    }

    SELFLASH(SEL_SR);                                           // Map FRAM into page 7/F
    NVRAM_UNLOCK();

    dev_lock = (uint8_t *FAR)(NVRAM_FLASHLOCKS_32 + (uint32_t)dev_idx);

    if(*dev_lock)
    {
        *dev_lock = false;
        codes_holder_info[dev_idx].locked_f = false;
        (SM_FLASHLOCKS_P)--;
    }
    else
    {
        *dev_lock = true;
        codes_holder_info[dev_idx].locked_f = true;
        (SM_FLASHLOCKS_P)++;
    }

    NVRAM_LOCK();

    CodesCheckUpdates();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesFlashErase(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Erase a CPU flash device
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dev_idx;

    if(MenuGetInt16U(argv[0], 0, (SHORT_LIST_CODE_LAST-1), &dev_idx))   // Get target flash device
    {
        return;
    }

    if(codes_holder_info[dev_idx].update_state == CODE_STATE_NOT_AVL)
    {
        MenuRspError("Flash device is not available");
        return;
    }

    fprintf(TERM_TX, RSP_DATA ",Erasing flash device: %s\n",codes_holder_info[dev_idx].label);

    erase_dev[dev_idx](dev_idx);
    check_dev[dev_idx](dev_idx);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesFlashDataTest(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Perform erase and verify on flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dev_idx;
    uint16_t      data;
    uint16_t      walk;
    uint16_t      bit;
    uint32_t      address;

    if(MenuGetInt16U(argv[0], 0, (SHORT_LIST_CODE_PTDB-1), &dev_idx))   // Get target flash device
    {
        return;
    }

    if(codes_holder_info[dev_idx].dev_state == CODE_STATE_NOT_AVL)
    {
        MenuRspError("Flash device is not available");
        return;
    }

    /* Check if Flash can be erased or corrupted */

    if(((dev.device == 'A') && (dev_idx == SHORT_LIST_CODE_BTA)) ||
       ((dev.device == 'B') && (dev_idx == SHORT_LIST_CODE_BTB)))
    {
        MenuRspError("Boot is running out this device");
        return;
    }

    fprintf(TERM_TX, RSP_DATA ",Erasing flash device: %s\n",codes_holder_info[dev_idx].label);

    if(erase_dev[dev_idx](dev_idx))
    {
        check_dev[dev_idx](dev_idx);
        return;
    }

    /* Walking bit test on data */

    fprintf(TERM_TX, RSP_DATA ",Testing device %s\n",codes_holder_info[dev_idx].label);

    address = codes_holder_info[dev_idx].base_addr;

    for(walk=bit=0; !bit && walk < 2; walk++)
    {
        data = (walk ? 0xFFFE : 0x0001);        // initialise test data: first walking 0, then walking 1

        // Write test pattern and verify that the write worked

        for(bit = 18; (--bit != 0 && FlashProgram(address, data) == 0); address += 2)
        {
            data = (data << 1) | walk;          // shift test pattern
        }
    }

    check_dev[dev_idx](dev_idx);
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesFlashAddressTest(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Perform erase and verify on flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dev_idx;
    uint16_t      data;
    uint32_t      address;
    uint32_t      flash_size;
    uint32_t      n_words;

    if(MenuGetInt16U(argv[0], 0, (SHORT_LIST_CODE_PTDB-1), &dev_idx))   // Get target flash device
    {
        return;
    }

    if(codes_holder_info[dev_idx].dev_state == CODE_STATE_NOT_AVL)
    {
        MenuRspError("Flash device is not available");
        return;
    }

    /* Check if Flash can be erased or corrupted */

    if(((dev.device == 'A') && (dev_idx == SHORT_LIST_CODE_BTA)) ||
       ((dev.device == 'B') && (dev_idx == SHORT_LIST_CODE_BTB)))
    {
        MenuRspError("Boot is running out this device");
        return;
    }

    fprintf(TERM_TX, RSP_DATA ",Erasing flash device: %s\n",codes_holder_info[dev_idx].label);

    if(erase_dev[dev_idx](dev_idx))
    {
        check_dev[dev_idx](dev_idx);
        return;
    }

    address    = codes_holder_info[dev_idx].base_addr;
    flash_size = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE/2;

    /* Write & readback flash */

    fprintf(TERM_TX, RSP_DATA ",Testing device %s\n",codes_holder_info[dev_idx].label);

    for(n_words=data=0;n_words < flash_size;address+=2,n_words++)       // write whole memory
    {
        if(FlashProgram(address, data++) != 0)
        {
            check_dev[dev_idx](dev_idx);
            return;                                                     // If user abort, quit
        }

        if(!((uint16_t)n_words & 0x07FF))
        {
            if(MenuRspProgress(true,(uint16_t)(n_words>>3), (uint16_t)(flash_size>>2)))
            {
                check_dev[dev_idx](dev_idx);
                return;                                                 // If user abort, quit
            }
        }
    }

    address = codes_holder_info[dev_idx].base_addr;
    n_words = flash_size / 2;

    for(n_words=data=0;n_words < flash_size;address+=2,n_words++)       // read & check whole memory
    {
        if(FlashCheck(address, data) != 0)
        {
            MenuRspError("ADDR 0x%05lX write 0x%04X failed (look at runlog)",address,data);
            check_dev[dev_idx](dev_idx);
            return;                                                     // If user abort, quit
        }
        data++;

        if(!((uint16_t)n_words & 0x0FFF))
        {
            if(MenuRspProgress(true,(uint16_t)((n_words>>3)+(flash_size>>3)), (uint16_t)(flash_size>>2)))
            {
                check_dev[dev_idx](dev_idx);
                return;                                                 // If user abort, quit
            }
        }
    }
    MenuRspProgress(true,1,1);          // if completed ok
    fputs("\n",TERM_TX);
    check_dev[dev_idx](dev_idx);
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesWriteBootToFlash(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Copy boot running in pages 2-3 to flash in pages 4-5.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(dev.device)
    {
    case 'R':

        WriteMcuBootDev(SHORT_LIST_CODE_BTA);
        WriteMcuBootDev(SHORT_LIST_CODE_BTB);
        break;

    case 'A':

        WriteMcuBootDev(SHORT_LIST_CODE_BTA);
        break;

    case 'B':

        WriteMcuBootDev(SHORT_LIST_CODE_BTB);
        break;
    }
}

// EOF
