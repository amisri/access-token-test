/*---------------------------------------------------------------------------------------------------------*\
  File:         menu.c

  Purpose:      Functions to support the boot menu

  Author:       Stephen.Page@cern.ch

\*---------------------------------------------------------------------------------------------------------*/

#define MENU_GLOBALS            // for menu global variable

#include <menu.h>
#include <stdio.h>              // for fprintf(), sprintf(), NULL
#include <stdlib.h>             // for strtof(), strtol(), strtoul()
#include <stdarg.h>             // for va_list, va_start(), va_end()
#include <term.h>               // for TERM_TX
#include <menutree.h>
#include <dev.h>                // for dev global variable
#include <term_boot.h>           // for TermPutc(), TermGetCh()
#include <fieldbus_boot.h>      // for fieldbus, stat_var global variable, CLASS_FIELD
#include <memmap_mcu.h>         // for SM_UXTIME_P
#include <main.h>               // for main_misc global variable, CheckStatus()
#include <shared_memory.h>      // for shared_mem global variable
#include <definfo.h>


/*---------------------------------------------------------------------------------------------------------*/
/*
//--- Functions not yet implemented ---

void MenuIdQueryLocal           (uint16_t argc, uint8_t **argv) {MenuRspError("Not yet implemented");}
void MenuIdQueryMainA           (uint16_t argc, uint8_t **argv) {MenuRspError("Not yet implemented");}
void MenuIdQueryMainB           (uint16_t argc, uint8_t **argv) {MenuRspError("Not yet implemented");}
void MenuIdQueryAuxA            (uint16_t argc, uint8_t **argv) {MenuRspError("Not yet implemented");}
void MenuIdQueryAuxB            (uint16_t argc, uint8_t **argv) {MenuRspError("Not yet implemented");}
void MenuDallasReceptionRack    (uint16_t argc, uint8_t **argv) {MenuRspError("Not yet implemented");}
*/
/*---------------------------------------------------------------------------------------------------------*/
void MenuMenu(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  Move through the menu tree.  This function is called recursively for each level.  It is called
  recursively by TermGetMenuOption().
\*---------------------------------------------------------------------------------------------------------*/
{
    while(!menu.up_lvls)                    // While no request to move up a level
    {
        menu.node_id[menu.depth] = '\0';    // Null terminate node ID at this depth

        if(node->function)                  // If node is a LEAF
        {
            if(node->argc)                  // If function expect args
            {
                TermRunFuncWithArgs(node);  // Get args then run function
            }
            else                            // else no args
            {
                MenuRunFunc(node, NULL);    // Run function directly
            }

            menu.up_lvls = (term.edit_state ? 1 : menu.depth+1); // Set up levels
        }
        else                                // else node is a BRANCH
        {
            if(term.edit_state)             // If interactive mode
            {
                MenuDisplay(node);          // Print the menu
            }

            TermGetMenuOption(node);        // Get user option and call MenuMenu() recursively
        }
    }

    menu.up_lvls--;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDisplay(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called in interactive mode to display the menu for the current branch node.  It
  indicates if the branch is recursive and for each child, whether it is a leaf or a sub-branch.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      ii;
    uint16_t      nn;

    // Display menu title

    if ( !menu.depth )                      // If root menu
    {
        nn = fprintf(TERM_TX, "\n" TERM_BOLD "%s\n",node->name) - 2;    // Print node name underlined
    }
    else                            // else lower level menu
    {
        nn = fprintf(TERM_TX, "\n" TERM_BOLD "%s%c %s\n", (char * FAR)menu.node_id, // Print node name underlined
                    (node->recurse_f ? '*' : ' '),node->name) - 2;
    }

    // Display system status

    CheckStatus();                      // Refresh status values

    fprintf(TERM_TX, TERM_NORMAL TERM_UL "B:%c G:%u T:%lu F:%04X W:%04X L:%04X\n" TERM_NORMAL,
                        dev.device,                                             // Boot device
                        fieldbus.is_gateway_online,                             // Gateway status
                        SM_UXTIME_P,                                            // Unix time
                        stat_var.fgc_stat.class_data.CLASS_FIELD.st_faults,     // Faults
                        stat_var.fgc_stat.class_data.CLASS_FIELD.st_warnings,   // Warnings
                        stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched     // Unlatched status
                        );

    // Display list of menu options

    nn = node->n_children;

    for ( ii = 0; ii < nn; ii++ )               // Show list of menu options
    {
        if ( (ii+1) == nn )                     // If this is the last option
        {
            fputs("\33[4m",TERM_TX);            // Enable underlining
        }

        fprintf(TERM_TX, "%i%c %s", ii,
                (node->children[ii]->function ? '*' : ' '), // Mark leaves with a '*'
                node->children[ii]->name);

        if(node->children[ii]->argc)            // If node has arguments
        {
            fprintf(TERM_TX, " (%u,%s)",ii,node->children[ii]->args_help);  // Display help with args
        }

        fputs("\33[0m\n",TERM_TX);              // Disable attributes (underlining) and newline
    }
}
#pragma MESSAGE DISABLE C1855 // Disable 'recursive function call' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MenuFollow(struct menu_node *node)
/*---------------------------------------------------------------------------------------------------------*\
  RECURSIVE function!!!!

  Follow all children of node and run their functions.  If a child fails then result depends upon whether
  the node is flagged as fatal or not.  If it is, the execution on that branch terminates and the error
  is reported to the level above.  Note that all leaves on a recursive branch must not require arguments.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  ii;

    //  Check recursive flag for leaves and branches

    if ( !node->recurse_f )         // If node is not recursive
    {
        return(0);                  // Return immediately without error
    }

    menu.node_id[menu.depth]   = '\0';

    // Node is a LEAF

    if ( node->function )                               // If node is a leaf
    {
        return(MenuRunFunc(node, NULL));                // Run leaf's function (there can be no args)
    }

    // Node is a BRANCH

    for ( ii = 0; node->children[ii]; ii++ )            // For each child of branch
    {
        menu.node_id[menu.depth++] = '0' + ii;          // Set node ID for child

        if ( MenuFollow(node->children[ii] ) &&         // If execution of child failed and
             node->children[ii]->fatal_f)               // child node is flagged as fatal
        {
            menu.depth--;
            return(1);                                  // Report failure to level above
        }
        menu.depth--;
    }

    return(0);                  // Return no error
}
#pragma MESSAGE DEFAULT C1855
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MenuRunFunc(struct menu_node *node, char * args_buf)
/*---------------------------------------------------------------------------------------------------------*\
  This function will run the function for the LEAF node.
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(TERM_TX,RSP_ID_NAME,            // Report that command is starting execution
        (char * FAR)menu.node_id, (char * FAR)node->name);

    if(MenuPrepareArgs(args_buf, node->argc))       // If arguments are not valid
    {
        return(1);                      // Report failure
    }

    menu.response_buf_pos = 0;              // Clear response and error buffers
    menu.error_buf[0] = '\0';

    if(MenuConfirm(node))               // If request is cancelled by user
    {
        fprintf(TERM_TX,RSP_ERROR ",Execution cancelled\n");    // Write error report
        return(1);
    }

    dev.abort_f = 0;                    // Clear abort flag

    node->function(menu.argc, (char**)&menu.argv);      // Run function

    if(menu.response_buf_pos)               // If rsp buffer is not empty
    {
        fprintf(TERM_TX,RSP_DATA "%s\n", (char * FAR)menu.response_buf);    // Write last response buffer
    }

    if(!*menu.error_buf)                // If no error reported
    {
        fputs(RSP_OK "\n",TERM_TX);             // Report success
        return(0);
    }

    fprintf(TERM_TX,RSP_ERROR ",%s\n", (char * FAR)menu.error_buf); // Write error report
    return(1);
}
#pragma MESSAGE DISABLE C5909 // Disable 'assignment in codition' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MenuConfirm(struct menu_node * node)
/*---------------------------------------------------------------------------------------------------------*\
  This function will get confirmation from the user if required for the node and in interactive mode.
  The function returns 0 if confirmation is given or not required (function will be called) and 1 if
  confirmation was not given (function will not be called).
\*---------------------------------------------------------------------------------------------------------*/
{
    char    ch;

    // If direct mode or node does not require confirmation
    if ( (term.edit_state == 0) || (node->confirm_f == 0) )
    {
        TermPutc('\n',TERM_TX);              // Newline
        return(0);                  // Run function
    }

    fputs(": Y/[N] ",TERM_TX);

    // is not a mistake, assignment in the evaluation statement on purpose
    while ((ch = TermGetCh(30000)) != 0)              // While characters are entered with 30s timeout period
    {
        switch(ch)
        {
            case 'y':                   // Y = Run command

                fputs("Y\n",TERM_TX);
                return(0);

            case '\n':                  // Newline, Carriage return or N = cancel command
            case '\r':
            case 'n':

                fputs("N\n",TERM_TX);
                return(1);

            default:                    // otherwise ring bell and get another character

                TermPutc('\a',TERM_TX);
                break;
        }
    }

    fputs("- Time out\a\n",TERM_TX);        // Report timeout and cancel execution
    return(1);
}
#pragma MESSAGE DEFAULT C5909
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MenuPrepareArgs(char * buffer, uint16_t argc_exp)
/*---------------------------------------------------------------------------------------------------------*\
  Prepare arguments for function.  The argv buffer should be big enough to hold the maximum number of
  args that the buffer can contain (~40).  The function chops up the arguments in the buffer and sets
  menu.argc and menu.argv.  White space is removed from the start and end of arguments.  Missing arguments
  are set to point to a null string.  If number of arguments expected doesn't equal the number found,
  an error message is written and the function returns 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    char            delimiter;
    char *          end;
    char *          cp     = buffer;
    char **         argv   = menu.argv;
    static char     null[] = "";            // Blank string for missing arguments

    menu.argc = 0;                  // Reset number of arguments

    if ( buffer != 0 )              // If arguments buffer supplied
    {
        do                  // Loop for each argument (comma separated)
        {
            for(;*cp == ' ';cp++);                  // Skip leading white space
            {
            }

            if ( (*cp == '\0') || (*cp == ',') )        // If argument is empty
            {
                end  = cp - 1;                  // Prepare end pointer
                *argv = null;                   // Set arg to null string
            }
            else                            // else argument exists
            {
                for(*argv = cp;*cp && *cp != ',';cp++);     // Find argument separater (, or null)
                {
                }

                for(end   = cp;*(--end) == ' ';);       // Find end of argument
                {
                }
            }

            delimiter = *(cp++);        // Remember delimiter character
            argv++;             // Increment argument pointer
            menu.argc++;            // Increment argument count
            *(++end) = '\0';            // Terminate the current argument
        }
        while( delimiter != 0 );        // Loop while more arguments are waiting
    }

    if ( menu.argc != argc_exp )        // If number of arguments is invalid
    {
        fprintf(TERM_TX,"\n" RSP_ERROR ",Invalid number of arguments (exp:%u got:%u)\n",argc_exp,menu.argc);
        return(1);
    }

    return(0);              // Return 0 = success
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRspArg(char * FAR format, ...)
/*---------------------------------------------------------------------------------------------------------*\
  Append argument to response.
\*---------------------------------------------------------------------------------------------------------*/
{
    va_list argv;
    uint16_t  buf_len;
    char    buf[TERM_LINE_SIZE];

    if ( format == 0 )                          // If format is NULL
    {
        fprintf(TERM_TX, RSP_DATA "%s\n", (char * FAR)menu.response_buf);       // Send response buffer
        menu.response_buf_pos = 0;                  // Reset response buffer
        return;
    }

    va_start(argv, format);
    buf_len = vsprintf(buf, format, argv);
    va_end(argv);

    if ( (menu.response_buf_pos + buf_len) >= (TERM_LINE_SIZE-3) )  // If end of line exceeded
    {
        fprintf(TERM_TX, RSP_DATA "%s\n", (char * FAR)menu.response_buf);       // Send response buffer
        menu.response_buf_pos = 0;                  // Reset response buffer
    }

    menu.response_buf_pos += sprintf(&menu.response_buf[menu.response_buf_pos],",%s", (char * FAR)buf);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MenuRspProgress(uint16_t allow_abort_f, uint16_t level, uint16_t total)
/*---------------------------------------------------------------------------------------------------------*\
  This will report the progress in percent (level/total * 100).  If allow_abort_f is set, it will also check
  if the user has aborted the test, and returns 1 if so.  Otherwise it will return 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t level32 = (uint32_t)level * 100L;

    if ( (allow_abort_f != 0) && (dev.abort_f != 0) )
    {
        MenuRspError("Aborted by user");
        return(1);
    }

    if ( dev.state == RGLEDS_VS_GREEN_MASK16 ) // boot state : self test
    {
        fprintf(TERM_TX," %3u%%\r",(uint16_t)(level32/total));
    }
    else
    {
        fprintf(TERM_TX,RSP_PROGRESS ",%u\r",(uint16_t)(level32/total));
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRspError(char * FAR format, ...)
/*---------------------------------------------------------------------------------------------------------*\
  Write error report to error buffer.
\*---------------------------------------------------------------------------------------------------------*/
{
    va_list argv;

    va_start(argv, format);
    vsprintf(menu.error_buf, format, argv);
    va_end(argv);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MenuGetInt16U(char * arg, uint16_t min, uint16_t max, uint16_t * result)
/*---------------------------------------------------------------------------------------------------------*\
  This function will convert one argument into a 16-bit unsigned integer value.  If the ASCII value in *arg
  starts "0x", the value will be interpretted in hex.  If a valid integer is found, it will be checked
  against the min/max limits supplied.  The result is returned in *result.  If an error occurs, the function
  writes an error report and returns 1, otherwise it returns 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  value;
    uint16_t  base    = 10; // 16:Hex  10:Decimal (default)
    char *  cp      = arg;
    char *  remains;

    if ( *cp == 0 )                 // If argument is empty
    {
        MenuRspError("Missing argument");       // Report error
        return(1);
    }

    if ( (cp[0] == '0') && (cp[1] == 'x') )     // If prefix is "0x"
    {
        cp  += 2;                   // Skip prefix
        base = 16;                  // Interpret as hex
    }

    value = strtoul(cp,&remains,base);              // Convert arg to unsigned long

    if ( *remains != 0 )                    // If any characters remain after conversion
    {
        MenuRspError("Invalid integer:%s",(char * FAR)arg);
        return(1);
    }

    if (   (value < (uint32_t)min)            // If value is outside of min/max range
        || (value > (uint32_t)max) )
    {
        MenuRspError("%s is outside limits (%u:%u)", (char * FAR)arg, min, max);
        return(1);
    }

    *result = value;                // Return value via *result

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MenuGetInt32U(char * arg, uint32_t min, uint32_t max, uint32_t *result)
/*---------------------------------------------------------------------------------------------------------*\
  This function will convert one argument into a 32-bit unsigned integer value.  If the ASCII value in *arg
  starts "0x", the value will be interpretted in hex.  If a valid integer is found, it will be checked
  against the min/max limits supplied.  The result is returned in *result.  If an error occurs, the function
  writes an error report and returns 1, otherwise it returns 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      value;
    uint16_t      base    = 10;      // 16:Hex  10:Decimal (default)
    char *      cp      = arg;
    char *      remains;

    if ( *cp == 0 )                 // If argument is empty
    {
        MenuRspError("Missing argument");       // Report error
        return(1);
    }

    if ( (cp[0] == '0') && (cp[1] == 'x') )     // If prefix is "0x"
    {
        cp  += 2;                   // Skip prefix
        base = 16;                  // Interpret as hex
    }

    value = strtoul(cp, &remains, base);                // Convert arg to unsigned long

    if ( *remains != 0 )                                // If any characters remain after conversion
    {
        MenuRspError("Invalid integer:%s",(char * FAR)arg);
        return(1);
    }

    if ( (value < min) || (value > max) )       // If value is outside of min/max range
    {
        MenuRspError("%s is outside limits (%lu:%lu)", (char * FAR)arg, min, max);
        return(1);
    }

    *result = value;                    // Return value via *result

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MenuGetInt32S(char * arg, int32_t min, int32_t max, int32_t *result)
/*---------------------------------------------------------------------------------------------------------*\
  This function will convert one argument into a 32-bit signed integer value.  If the ASCII value in *arg
  starts "0x", the value will be interpretted in hex.  If a valid integer is found, it will be checked
  against the min/max limits supplied.  The result is returned in *result.  If an error occurs, the function
  writes an error report and returns 1, otherwise it returns 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    int32_t      value;
    uint16_t      base    = 10;      // 16:Hex  10:Decimal (default)
    char *      cp      = arg;
    char *      remains;

    if(!*cp)                    // If argument is empty
    {
        MenuRspError("Missing argument");       // Report error
        return(1);
    }

    if(cp[0] == '0' && cp[1] == 'x')            // If prefix is "0x"
    {
        cp  += 2;                                       // Skip prefix
        base = 16;                                      // Interpret as hex
    }

    value = strtol(cp,&remains,base);       // Convert arg to signed long

    if(*remains)                                // If any characters remain after conversion
    {
        MenuRspError("Invalid integer:%s",(char * FAR)arg);
        return(1);
    }

    if((value < min) || (value > max))          // If value is outside of min/max range
    {
        MenuRspError("%s is outside limits (%ld:%ld)",(char * FAR)arg,min,max);
        return(1);
    }

    *result = value;                            // Return value via *result

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t MenuGetFloat(char * arg, FP32 min, FP32 max, FP32 *result)
/*---------------------------------------------------------------------------------------------------------*\
  This function will convert one argument into a float value.  If a valid float is found, it will be checked
  against the min/max limits supplied.  The result is returned in *result.  If an error occurs, the function
  writes an error report and returns 1, otherwise it returns 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    FP32        value;
    char *      cp      = arg;
    char *      remains;

    if(!*cp)                    // If argument is empty
    {
        MenuRspError("Missing argument");       // Report error
        return(1);
    }

    value = strtod(cp,&remains);                // Convert arg to float

    if(*remains)                                // If any characters remain after conversion
    {
        MenuRspError("Invalid float:%s",(char * FAR)arg);
        return(1);
    }

    if((value < min) || (value > max))          // If value is outside of min/max range
    {
        MenuRspError("%s is outside limits (%f:%f)",(char * FAR)arg,min,max);
        return(1);
    }

    *result = value;                            // Return value via *result

    return(0);
}

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void DUMMY(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Dummy boot function.
\*---------------------------------------------------------------------------------------------------------*/
{
   MenuRspError("Dummy function");
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menu.c
\*---------------------------------------------------------------------------------------------------------*/
