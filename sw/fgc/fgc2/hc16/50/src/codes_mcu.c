/*---------------------------------------------------------------------------------------------------------*\
  File:         codes_mcu.c

  Purpose:      FGC Boot code update functions

  Notes:        These functions are shared by the FGC2 and FGC3 boot programs
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdbool.h>
#include <codes_mcu.h>
#include <stdio.h>              // for fprintf(), fputs()
#include <term.h>
#include <codes_holder.h>       // for codes_holder_info
#include <memmap_mcu.h>         // Include memory map consts
#include <mem.h>                // for MemCpyBytes(), MemCpyWords()
#include <mcu_dependent.h>      // for SEL_BB, SEL_BA, SEL_MP
#include <codes.h>
#include <menu.h>               // for menu global variable
#include <dev.h>                // for dev global variable
#include <definfo.h>            // for FGC_CLASS_ID
#include <codes_fgc.h>          // for check_dev[]()
#include <flash.h>              // for FlashProgram()
#include <version.h>            // for version

/*!
 * @brief Selects the flash device corresponding to given ID
 *
 * This function will select the HC16 flash device and return it's address and SM address (if pointer is
 * supplied). The function returns:
 * - 0 - Read/Write
 * - 1 - Read only
 * - 2 - Not visible
 * The function also sets the device and update states according to whether the flash can be written and/or
 * read.
 *
 * @param[in] dev_idx Device ID
 * @param[out] sm_address_p Pointer to which the code version should be saved
 *
 * @return Device accessibility
 *
 * @todo Move version and code visibility checking out of the function
 */
static uint16_t SelectMcuDev(enum fgc_code_ids_short_list dev_idx, uint32_t **sm_address_p);

/*---------------------------------------------------------------------------------------------------------*/
uint16_t CheckMcuDev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will check the code in an MCU flash device.  It returns 0 if the code is valid, 1 otherwise.

  The codes_holder_info[device].dev structure is filled with the info from the code we already
  have in the FLASH.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t *                    sm_address = 0;
    uint32_t                      len;
    struct TCodesHolderInfo *   code_info = codes_holder_info + dev_idx;

    // Return immediately if Flash device isn't available

    if ( SelectMcuDev(dev_idx, &sm_address) == 2 )
    {
        return(1);
    }

    // Fill the code_info structure with the data we have in the FLASH

    len = (uint32_t)code_info->len_blks * FGC_CODE_BLK_SIZE;

    MemCpyWords((uint32_t)&code_info->dev,
                (code_info->base_addr + len - FGC_CODE_INFO_SIZE),
                 FGC_CODE_INFO_SIZE / 2);

    // Null terminate string in case it is invalid and to protect maximum length

    if(code_info->dev.version_string[0] < 0)
    {
        code_info->dev.version_string[0] = '\0';
    }
    else
    {
        code_info->dev.version_string[FGC_CODE_VERSION_STRING_LEN - 1] = '\0';
    }

    // If SM address defined

    if ( sm_address != 0 )
    {
        *sm_address = code_info->dev.version;       // Copy code version in SM field
    }

    // calculate CRC
    code_info->calc_crc = MemCrc16(code_info->base_addr, (len / 2) - 1);

    if ( code_info->calc_crc == code_info->dev.crc )
    {
        code_info->dev_state = CODE_STATE_VALID;

        return(0);          // Return 0 for valid code in Flash
    }

    code_info->dev_state = CODE_STATE_CORRUPTED;

    return(1);              // Return 1 for corrupted code in Flash
}



uint16_t SelectMcuDev(enum fgc_code_ids_short_list dev_idx, uint32_t **sm_address_p)
{
    uint32_t * code_version    = NULL;
    uint16_t   flash_sel       = SEL_BB;  // Select BB by default (6 out of 8 devices are in BB flash)

    // If code is not visible for MCU (is not in MCU's flash) then return
    // TODO Move version and visibility checking outside the function

    if (CodesGetCodeVisibility(dev_idx) != CODES_VISIBILITY_MCU)
    {
        return 2;
    }

    code_version = CodesGetCodeVersionPointer(dev_idx);

    if (dev_idx == SHORT_LIST_CODE_BTA)
    {
        flash_sel = SEL_BA;
    }
    else if (dev_idx == SHORT_LIST_CODE_MP_DSP)
    {
        flash_sel = SEL_MP;
    }

    SELFLASH(flash_sel);

    if(sm_address_p)
    {
        *sm_address_p = code_version;
    }

    // Return true for read-only

    return(codes_holder_info[dev_idx].read_only_f);
}



/*---------------------------------------------------------------------------------------------------------*/
uint16_t EraseMcuDev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will erase an HC16 flash device (may be a sector or complete chip).  If the erase fails,
  the device state is set to indicate DEV_FAILED and the function returns 1.  Otherwise it returns zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      stat;
    struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;

    if(SelectMcuDev(dev_idx, NULL)) // If device isn't writable
    {
        return(1);                              // Return 1 (fail)
    }

    switch(dev_idx)                     // Erase device or sectors according to flash device index
    {
    case SHORT_LIST_CODE_BTA:
    case SHORT_LIST_CODE_MP_DSP:

        stat = FlashChipErase(CODES_FLASH_SA0_32);
        break;

    case SHORT_LIST_CODE_BTB:                 // BB_01

        stat = FlashSectorErase(CODES_FLASH_SA0_32) | FlashSectorErase(CODES_FLASH_SA1_32);
        break;

    default:                            // BB_2, BB_3, BB_4, BB_5, BB_6

        stat = FlashSectorErase(code_info->base_addr);
        break;
    }

    if(stat)
    {
        code_info->dev_state = CODE_STATE_DEV_FAILED;        // Set dev state to DEV_FAILED

        if(*menu.error_buf)                                             // If an error was reported
        {
            fprintf(TERM_TX,RSP_ERROR ",%s:%s\n",                               // Write error report
                    code_info->label,(char * FAR)menu.error_buf);
            *menu.error_buf = '\0';                                             // Clear error buffer
        }

        return(1);
    }

    return(0);                                          // Return 0 (success)
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ProgramMcuDev(enum fgc_code_ids_short_list dev_idx, struct code_block *block)
/*---------------------------------------------------------------------------------------------------------*\
  This function will program a 32 byte code block into an HC16 flash device.  It has a special
  behaviour for the first block which is stored in memory in block0 until the last block has been written.
  Only then does it write the first block.  This is a protection against a failure to complete
  the programming of the boot or main programs since the first block contains the reset and bus error
  and illegal instruction vectors.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      len;
    uint16_t *    from;
    uint32_t      flash_address;
    struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;
    static uint16_t block0[FGC_CODE_BLK_SIZE/2];  // Storage for first block of code

    if(SelectMcuDev(dev_idx, NULL))        // If device isn't writable
    {
        return(1);                                              // Return 1 (fail)
    }

    /*--- First Block ---*/

    if(!block->block_idx)                       // If first block
    {
        MemCpyBytes((uint8_t*)&block0, (uint8_t*)&block->code, FGC_CODE_BLK_SIZE);  // Store block0
        return(0);
    }

    /*--- Other Blocks ---*/

    len  = FGC_CODE_BLK_SIZE/2;                 // Block length in words
    from = &block->code[0];                        // Prepare to copy code block

    flash_address = code_info->base_addr +                   // Calc block address in flash
                   ((uint32_t)block->block_idx * FGC_CODE_BLK_SIZE);

    while(len--)                                // Loop for all words in the code block
    {
        if ( FlashProgram(flash_address,*(from++)) )       // Copy and verify word from ram to flash
        {
            code_info->dev_state = CODE_STATE_DEV_FAILED;    // Set dev state to DEV_FAILED
            return(1);                                                  // Report that operation failed
        }

        flash_address += 2;
    }

    /*--- Last block ---*/

    if(block->block_idx == code.rx_last_blk_idx)        // If last block
    {
        len  = FGC_CODE_BLK_SIZE/2;                     // Block length in words
        from = &block0[FGC_CODE_BLK_SIZE/2];

        flash_address = code_info->base_addr + FGC_CODE_BLK_SIZE;

        while(len--)                                    // Write block 0 backwards (reset vector last)
        {
            flash_address -= 2;

            if(FlashProgram(flash_address,*(--from)))           // Copy and verify word from ram to flash
            {
                code_info->dev_state = CODE_STATE_DEV_FAILED;        // Set dev state to DEV_FAILED
                return(1);                                                      // Report that operation failed
            }
        }
    }

   return(0);                                   // Return 0 (success)
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t WriteMcuBootDev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will save the running boot (pages 2-3) into flash in page 4-5.  The boot maybe running out
  of SR2-3 during development, or from flash BA_01 or BB_01.  The dev_idx parameter specifies which
  flash device to write to (BB_01 or BA_01).  After the copy, the target flash is checked and the function
  returns zero if the CRC is valid.

  The copy is done from the end of the code to the beginning to ensure that the boot vectors remain
  unprogrammed (0xFFFF) until the end of the programming.  This reduces the chance of a corrupted code
  that can block the FGC in a permanent loop of resets.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      len;
    uint32_t      from;
    uint32_t      to;
    uint16_t      progress = 0;

    fprintf(TERM_TX, RSP_DATA ",Copying boot to %s\n",codes_holder_info[dev_idx].label);
    MenuRspProgress( false, progress, 1);

    if(dev.device == 'R')               // If running from SRAM
    {
        // Prepare code info data in page 3 (SR3) - use boot program build time (from version) as the code version)

        MemCpyWords(CODES_BOOTPROG23_INFO_VERSION_A_32,(uint32_t)version.unixtime_a,CODES_BOOTPROG23_INFO_VERSION_A_W);
        CODES_BOOTPROG23_INFO_VERSION_FP     = version.unixtime;
        CODES_BOOTPROG23_INFO_CLASSID_FP     = FGC_CLASS_ID;
        CODES_BOOTPROG23_INFO_CODEID_FP      = FGC_CODE_50_BOOTPROG;
        CODES_BOOTPROG23_INFO_OLD_VERSION_FP = 0;
        CODES_BOOTPROG23_INFO_CRC_FP         = MemCrc16(CODES_BOOTPROG23_32, (CODES_BOOTPROG23_W-1));
    }

    SelectMcuDev(dev_idx, NULL);   // Set memmap mode according to target flash device

    /* Erase target flash and copy code from pages 2-3 to pages 4-5 */

    if(FlashSectorErase(CODES_FLASH_SA0_32) ||  // If erase of flash sectors fails
       FlashSectorErase(CODES_FLASH_SA1_32))
    {
        check_dev[dev_idx](dev_idx);                    // Return immediately
        return(1);
    }

    from = CODES_BOOTPROG23_32 + CODES_BOOTPROG23_B;    // Prepare to copy Code 1 from pages 2-3 to pages 4-5
    to   = CODES_BOOTPROG45_32 + CODES_BOOTPROG23_B;    // from high to low to program vectors last
    len  = CODES_BOOTPROG23_W;

    while(len--)                                // Loop for all words in the code
    {
        from -= 2;                                      // Decrement from and to addresses by one word
        to   -= 2;

        if(FlashProgram(to,*((uint16_t* FAR)from)))       // Copy and verify word
        {
            check_dev[dev_idx](dev_idx);
            return(1);                                          // Flash operation failed
        }

        if(!((uint16_t)len & 0x0FFF))                     // Report progress every 4KB
        {
            progress += 0x100;
            MenuRspProgress( false, progress, (uint16_t)((uint32_t)CODES_BOOTPROG23_W/16));
        }
    }

    if(check_dev[dev_idx](dev_idx))             // Check the CRC for the new Code
    {
        MenuRspError("CRC BAD");
        return(1);
    }

    fputs(RSP_DATA ",CRC OK\n", TERM_TX);
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_mcu.c
\*---------------------------------------------------------------------------------------------------------*/
