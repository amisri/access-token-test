/*---------------------------------------------------------------------------------------------------------*\
  File:         ana.c

  Purpose:      Functions related to the management of the analogue interfaces

  Author:       Quentin.King@cern.ch

  Notes:

  History:

    08/03/05    qak     Created from menuana.c
    13/05/05    pfr     modified to add function AnaRefAdcs
\*---------------------------------------------------------------------------------------------------------*/

#define ANALOG_GLOBALS  // to instantiate slot5_types[], slot5_code_ids[], slot5_class_ids[], sd_flash_addr[] global variables


#include <analog.h>
#include <stdio.h>              // for fprintf()
#include <term.h>               // for TERM_TX
#include <sdflash.h>            // for sd_func0[], sd_func2[]
#include <menu.h>               // for MenuRspError()
#include <dev.h>                // for dev global variable
#include <fieldbus_boot.h>      // for stat_var global variable
#include <mcu_dependent.h>      // for MSLEEP()
#include <main.h>               // for timeout_ms global variable used by MSLEEP(), CheckSlot5IfNot()
#include <c32.h>                // for FIFO_TEST_LINK
#include <pll_boot.h>           // for pll global variable
#include <macros.h>             // for Set()
#include <defconst.h>           // for FGC_INTERFACE_
#include <memmap_mcu.h>         // for DPRAM_BT_FIFOCTRL_P
#include <stdlib.h>             // for labs()


/*---------------------------------------------------------------------------------------------------------*/
void AnaMpxAna(uint16_t mpx_a, uint16_t mpx_b)
/*---------------------------------------------------------------------------------------------------------*\
  Set the analogue MPX directly from arguments
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      mpx;

    mpx = ANA_MPX_P;            // Force word read

    mpx =   ((mpx & ~(ANA_MPX_ANAA_MASK16 | ANA_MPX_ANAB_MASK16)) |
             (mpx_a << ANA_MPX_ANAA_SHIFT) |
             (mpx_b << ANA_MPX_ANAB_SHIFT));

    ANA_MPX_P = mpx;
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaMpxDig(uint16_t mpx_a, uint16_t mpx_b)
/*---------------------------------------------------------------------------------------------------------*\
  Set the digital MPX directly from arguments
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t mpx;

    mpx = ANA_MPX_P;

    mpx =   ((mpx & ~(ANA_MPX_DIGA_MASK16 | ANA_MPX_DIGB_MASK16)) |
             (mpx_a << ANA_MPX_DIGA_SHIFT) |
             (mpx_b << ANA_MPX_DIGB_SHIFT));

    ANA_MPX_P = mpx;

    AnaInitSD(mpx);
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaFltFrq(uint16_t frq_a, uint16_t frq_b)
/*---------------------------------------------------------------------------------------------------------*\
  Set the SD digital filter frequencies directly from arguments
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      mpx;

    mpx = ANA_MPX_P;            // Force word read

    mpx =   ((mpx & ~(ANA_MPX_FRQA_MASK16 | ANA_MPX_FRQB_MASK16)) |
             (frq_a << ANA_MPX_FRQA_SHIFT) |
             (frq_b << ANA_MPX_FRQB_SHIFT));

    ANA_MPX_P = mpx;

    AnaInitSD(mpx);
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaInitSD(uint16_t mpx)
/*---------------------------------------------------------------------------------------------------------*\
  Set the SD digital filter index and length according to filter frequency
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))              // If not an SD filter
    {
        return;
    }

    ANA_SD_NL_A_W_RESET_P = 0xFFFF;     // Reset filters to allow index and length to be written
    ANA_SD_NL_B_W_RESET_P = 0xFFFF;

    if(mpx & ANA_MPX_FRQA_MASK16)       // If channel A needs a 1MHz filter
    {
        ANA_SD_NL_A_W_HALFTAPS_P = sd_func2[1];
        ANA_SD_NL_A_W_INDEX_P    = SLOT5_ADC_SD_INDEX_1M;
    }
    else                                // else channel A at 500kHz
    {
        ANA_SD_NL_A_W_HALFTAPS_P = sd_func0[1];
        ANA_SD_NL_A_W_INDEX_P    = SLOT5_ADC_SD_INDEX_500k;
    }

    if(mpx & ANA_MPX_FRQB_MASK16)       // If channel B needs a 1MHz filter
    {
        ANA_SD_NL_B_W_HALFTAPS_P = sd_func2[1];
        ANA_SD_NL_B_W_INDEX_P    = SLOT5_ADC_SD_INDEX_1M;
    }
    else                                // else channel B at 500kHz
    {
        ANA_SD_NL_B_W_HALFTAPS_P = sd_func0[1];
        ANA_SD_NL_B_W_INDEX_P    = SLOT5_ADC_SD_INDEX_500k;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaGetSingleSample(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets ADCs values (1 sample).
\*---------------------------------------------------------------------------------------------------------*/
{
    menusd.a.raw_h = 0;
    menusd.a.raw_m = 0;
    menusd.a.raw_l = 0;
    menusd.b.raw_h = 0;
    menusd.b.raw_m = 0;
    menusd.b.raw_l = 0;

    menusd.num_samples  = 1;
    menusd.sar_ave_ct   = 1;

    // Wait for sample to be taken

    while(menusd.num_samples);
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaGetMultiSample(uint16_t samples)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets ADCs values (for nb samples specified).
\*---------------------------------------------------------------------------------------------------------*/
{
    menusd.a.max_h      = 0x8000;               // Most negative value
    menusd.a.max_m      = 0x0000;
    menusd.a.max_l      = 0x0000;
    menusd.a.min_h      = 0x7FFF;               // Most positive value
    menusd.a.min_m      = 0xFFFF;
    menusd.a.min_l      = 0xFFFF;
    menusd.a.sum[0]     = 0;
    menusd.a.sum[1]     = 0;
    menusd.a.sum[2]     = 0;
    menusd.a.sum[3]     = 0;

    menusd.b.max_h      = 0x8000;               // Most negative value
    menusd.b.max_m      = 0x0000;
    menusd.b.max_l      = 0x0000;
    menusd.b.min_h      = 0x7FFF;
    menusd.b.min_m      = 0xFFFF;
    menusd.b.min_l      = 0xFFFF;
    menusd.b.sum[0]     = 0;
    menusd.b.sum[1]     = 0;
    menusd.b.sum[2]     = 0;
    menusd.b.sum[3]     = 0;

    menusd.sar_ave_ct   = 4;                    // counter for 4 point average (SAR) reset

    menusd.num_samples  = samples;

    // Wait for samples to be taken

    while(menusd.num_samples)
    {
        if(samples > 1000)                      // > 1s
        {
            if(!(menusd.num_samples % 512))
            {
                if(MenuRspProgress( true, (samples - menusd.num_samples) ,samples))     // If user abort
                {
                    return;                                                             // Quit
                }
            }
        }
    }

    // Process results

    AnaPP(&menusd.a);                   // Calculate peak-peak value for channel A
    AnaPP(&menusd.b);                   // Calculate peak-peak value for channel B

    /* if SAR divide p2p and raw by 4 because of 4 pt average */

    if(dev.slot5_type == FGC_INTERFACE_SAR_400)
    {
        menusd.a.p2p_l = (menusd.a.p2p_m << 14) | (menusd.a.p2p_l >> 2);
        menusd.a.p2p_m = (menusd.a.p2p_h << 14) | (menusd.a.p2p_m >> 2);
        menusd.a.p2p_h = menusd.a.p2p_h >> 2;

        menusd.b.p2p_l = (menusd.b.p2p_m << 14) | (menusd.b.p2p_l >> 2);
        menusd.b.p2p_m = (menusd.b.p2p_h << 14) | (menusd.b.p2p_m >> 2);
        menusd.b.p2p_h = menusd.b.p2p_h >> 2;

        menusd.a.raw_l = (menusd.a.raw_m << 14) | (menusd.a.raw_l >> 2);
        menusd.a.raw_m = (menusd.a.raw_h << 14) | (menusd.a.raw_m >> 2);
        menusd.a.raw_h = (menusd.a.raw_h >> 2) | (menusd.a.raw_h & 0x8000) | (menusd.a.raw_h & 0x4000);

        menusd.b.raw_l = (menusd.b.raw_m << 14) | (menusd.b.raw_l >> 2);
        menusd.b.raw_m = (menusd.b.raw_h << 14) | (menusd.b.raw_m >> 2);
        menusd.b.raw_h = (menusd.b.raw_h >> 2) | (menusd.b.raw_h & 0x8000) | (menusd.b.raw_h & 0x4000);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaRefAdcs(uint16_t samples)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets ADCs values (SLOT5_SAMPLES_REF samples) for reference voltages -10, 0 and 10V. Checks
  these values are inside limits. Stores theses values in global variables adcs_cal.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    int32_t      adc_ref[3];
    int32_t      adc_tol;
    uint16_t      adc_p2p_tol;
    uint16_t      ref_mpx[3]      = {4,5,7};

    switch(dev.slot5_type)
    {
    case FGC_INTERFACE_SD_350:
    case FGC_INTERFACE_SD_351:

        /* Set digital mpx on ADCA and ADCB, filter freq 500kHz */

        AnaMpxDig(0,1);
        AnaFltFrq(0,0);

        /* Initialise limits */

        adc_ref[0]      = SLOT5_ADC_SD_CAL0;
        adc_ref[1]      = SLOT5_ADC_SD_CAL1;
        adc_ref[2]      = SLOT5_ADC_SD_CAL2;
        adc_tol         = SLOT5_ADC_SD_TOL;
        adc_p2p_tol     = SLOT5_ADC_SD_P2P;
        break;

    case FGC_INTERFACE_SAR_400:

        /* Initialise limits */

        adc_ref[0]      = SLOT5_ADC_SAR_CAL0;
        adc_ref[1]      = SLOT5_ADC_SAR_CAL1;
        adc_ref[2]      = SLOT5_ADC_SAR_CAL2;
        adc_tol         = SLOT5_ADC_SAR_TOL;
        adc_p2p_tol     = SLOT5_ADC_SAR_P2P;
        break;

    default:

        MenuRspError("No SAR or SD");
        return;
    }

    for(i=0; i<3; i++)                          // ref: -10, 0, 10V
    {
        /* Set Ana MPX */

        AnaMpxAna(ref_mpx[i],ref_mpx[i]);

        /* Wait 500ms */

        MSLEEP(500);

        /* Wait for measurement to be taken */

        AnaGetMultiSample(samples);

        /* Check p2p noise */

        if(menusd.a.p2p_m > adc_p2p_tol)
        {
            MenuRspError("Chan A p2p %dV: 0x%04X", 10*(i-1), menusd.a.p2p_m);
            return;
        }
        if(menusd.b.p2p_m > adc_p2p_tol)
        {
            MenuRspError("Chan B p2p %dV: 0x%04X", 10*(i-1), menusd.b.p2p_m);
            return;
        }

        /* ADC calibration */

        adcs_cal.adc[0].val[i] = (int32_t)((uint32_t)menusd.a.raw_m | ((uint32_t)menusd.a.raw_h << 16));
        adcs_cal.adc[1].val[i] = (int32_t)((uint32_t)menusd.b.raw_m | ((uint32_t)menusd.b.raw_h << 16));

        /* Check results */

        if(labs(adcs_cal.adc[0].val[i] - adc_ref[i]) > adc_tol)
        {
            MenuRspError("Chan A ref %dV out of range: 0x%08lX",
                                                        10*(i-1), adcs_cal.adc[0].val[i]);
            return;
        }
        if(labs(adcs_cal.adc[1].val[i] - adc_ref[i]) > adc_tol)
        {
            MenuRspError("Chan B ref %dV out of range: 0x%08lX",
                                                        10*(i-1), adcs_cal.adc[1].val[i]);
            return;
        }
    }

    /* Compute DAC stability threshold (500uV for SD and 1000uV for SAR) on ADC A */

    switch(dev.slot5_type)
    {
        case FGC_INTERFACE_SD_350:
        case FGC_INTERFACE_SD_351:
            dac_cal.stab_threshold = (adcs_cal.adc[0].val[2] - adcs_cal.adc[0].val[1])/20000;   //500uV
            break;
        case FGC_INTERFACE_SAR_400:
            //dac_cal.stab_threshold = (adcs_cal.adc[0].val[2] - adcs_cal.adc[0].val[1])/12500; //800uV
            dac_cal.stab_threshold = 0xA;       // 1mV = 2.9 (thresh. is x by 4 for more precision, A=830uV)
            break;
        default:
            dac_cal.stab_threshold = 0;
            MenuRspError("Not SAR or SD, DAC stab threshold=0");
            return;
    }
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaDac(uint16_t samples)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by MenuSdDac() and MenuSarDac().
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  i;
    uint16_t  dac_p2p_tol;
    uint32_t  gain;
    int32_t  dac_offset_tol;
    uint32_t  dac_value;
    uint32_t  average[2];
    uint32_t  tmp;
    uint32_t  chan[2];

    switch(dev.slot5_type)
    {
    case FGC_INTERFACE_SD_350 :

        /* Set digital mpx on ADCA and ADCB, 500kHz filter */

        AnaMpxDig(0,1);
        AnaFltFrq(0,0);

        /* Initialise limits */

        dac_offset_tol = SLOT5_DAC_SD_TOL_REC;
        dac_p2p_tol     = SLOT5_DAC_SD_P2P;
        break;

    case FGC_INTERFACE_SAR_400 :

        /* Initialise limits */

        dac_offset_tol = SLOT5_DAC_SAR_TOL_REC;
        dac_p2p_tol     = SLOT5_DAC_SAR_P2P;
        break;

    default :

        MenuRspError("Not SAR or SD type");
        return;
    }

    /* Set ana mpx on DAC*/

    AnaMpxAna(3,3);

    /* Initialise dac_cal to the most negative value */

    for(i=0 ; i<3 ; i++)
    {
        dac_cal.val[i] = 0x80000000;
    }

    for(i=0 ; i<3 ; i++)                // about -10, 0, 10V
    {
        /* Set DAC regiter */

        dac_value = (uint32_t)((i-1)*SLOT5_DAC_10V);

        dac_value <<= 12;               // Value in DAC is left justified (bits 31-12)

        ANA_DAC_HIGH_P = ((uint16_t*)&dac_value)[0];
        ANA_DAC_LOW_P  = ((uint16_t*)&dac_value)[1];

        MSLEEP(2000);           // Wait for 2s

        if(i==2)                // 10V
        {
            MSLEEP(1000);                                       // wait for 1s
            AnaGetMultiSample(SLOT5_SAMPLES_REF_STAB);          // for stability check


            if(dev.slot5_type == FGC_INTERFACE_SAR_400)
            {
                /* divided by 256 for better precision */

                average[0] = (((*(uint32_t*)&menusd.a.sum[0])) << 8) |            // shifts depend on nb samples
                             ((uint32_t)menusd.a.sum[2] >> 8);

                average[1] = (((*(uint32_t*)&menusd.b.sum[0])) << 8) |            // shifts depend on nb samples
                             ((uint32_t)menusd.b.sum[2] >> 8);

                /* Check maximum deviation */

                chan[0] = *(uint32_t*)&menusd.a.max_h;
                chan[1] = *(uint32_t*)&menusd.b.max_h;

            }
            else
            {
                average[0] = ((*(uint32_t*)&menusd.a.sum[0]) << 6) |              // shifts depend on nb samples
                             ((uint32_t)menusd.a.sum[2] >> 10);

                average[1] = ((*(uint32_t*)&menusd.b.sum[0]) << 6) |              // shifts depend on nb samples
                             ((uint32_t)menusd.b.sum[2] >> 10);

                /* Check maximum deviation */

                chan[0] = *(uint32_t*)&menusd.a.max_h;
                chan[1] = *(uint32_t*)&menusd.b.max_h;
            }

        fprintf(TERM_TX,RSP_DATA",ave: 0x%08lX - 0x%08lX\n",average[0], average[1]);
        fprintf(TERM_TX,RSP_DATA",p2p: 0x%08lX - 0x%08lX\n",*(uint32_t*)&menusd.a.p2p_m,*(uint32_t*)&menusd.b.p2p_m);
        fprintf(TERM_TX,RSP_DATA",devp: 0x%08lX - 0x%08lX\n",chan[0]-average[0],chan[1]-average[1]);


            if(((chan[0] - average[0]) > dac_cal.stab_threshold)||((chan[1] - average[1]) > dac_cal.stab_threshold))
            {
                MenuRspError("Max std dev chanA:0x%08lX chanB: 0x%08lX",(chan[0] - average[0]),(chan[1] - average[1]));
                return;
            }

            /* Check minimum deviation */

            chan[0] = *(uint32_t*)&menusd.a.min_h;
            chan[1] = *(uint32_t*)&menusd.b.min_h;

        fprintf(TERM_TX, RSP_DATA",devm: 0x%08lX - 0x%08lX\n",average[0]-chan[0],average[1]-chan[1]);

            if(((average[0] - chan[0]) > dac_cal.stab_threshold)||((average[1] - chan[1]) > dac_cal.stab_threshold))
            {
                MenuRspError("Min std dev chanA:0x%08lX chanB: 0x%08lX",(average[0] - chan[0]),(average[1] - chan[1]));
                return;
            }

            /* Check p2p noise */

            if(menusd.a.p2p_m > dac_p2p_tol)
            {
                Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DAC_FLT);
                MenuRspError("Stab Chan A p2p: 0x%04X",menusd.a.p2p_m);
                return;
            }
            if(menusd.b.p2p_m > dac_p2p_tol)
            {
                Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DAC_FLT);
                MenuRspError("Stab Chan B p2p: 0x%04X",menusd.b.p2p_m);
                return;
            }
        }
        else
        {
            AnaGetMultiSample(samples);
        }

        /* Calibrates with chan A */

        dac_cal.val[i] = (int32_t)((uint32_t)menusd.a.raw_m | ((uint32_t)menusd.a.raw_h << 16));

        /* Check p2p noise */

        if(menusd.a.p2p_m > dac_p2p_tol)
        {
            Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DAC_FLT);
            MenuRspError("Chan A p2p: 0x%04X (DAC %u)",menusd.a.p2p_m, i);
            return;
        }
        if(menusd.b.p2p_m > dac_p2p_tol)
        {
            Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DAC_FLT);
            MenuRspError("Chan B p2p: 0x%04X (DAC %u)",menusd.b.p2p_m, i);
            return;
        }
    }

    /* Check offset */

    if(labs(dac_cal.val[1] - adcs_cal.adc[0].val[1]) > dac_offset_tol)
    {
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DAC_FLT);
        MenuRspError("DAC offset: 0x%08lX",labs(dac_cal.val[1] - adcs_cal.adc[0].val[1]));
        return;
    }

    /* Check positive gain */

    gain = dac_cal.val[2] - adcs_cal.adc[0].val[1];
    tmp  = adcs_cal.adc[0].val[2] - adcs_cal.adc[0].val[1] ;

    if(!tmp)
    {
        MenuRspError("ADCs not calibrated");
        return;
    }

    if(dev.slot5_type == FGC_INTERFACE_SAR_400)
    {
        gain <<= 10;
    }                                                           // (shift<p2p noise)
    else
    {
        gain <<= 2;                                             // shift to avoid overflow
        tmp  >>= 8;                                             // no quantization error
    }

    gain = (uint32_t)(gain/tmp);

    if(labs(gain - SLOT5_DAC_GAIN) > SLTO5_GAIN_DAC_TOL)                // more than 5% error
    {
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DAC_FLT);
        MenuRspError("DAC pos gain error (*1024): %ld",gain);
        return;
    }

    /* Check negative gain */

    gain = (adcs_cal.adc[0].val[1] - dac_cal.val[0]);
    tmp  = (adcs_cal.adc[0].val[1] - adcs_cal.adc[0].val[0]);

    if(dev.slot5_type == FGC_INTERFACE_SAR_400)
    {
        gain <<= 10;
    }                                                           // (shift<p2p noise)
    else
    {
        gain <<= 2;                                             // shift to avoid overflow
        tmp  >>= 8;                                             // no quantization error
    }

    gain = (uint32_t)(gain/tmp);

    if(labs(gain - SLOT5_DAC_GAIN) > SLTO5_GAIN_DAC_TOL)                // more than 5% error
    {
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DAC_FLT);
        MenuRspError("DAC neg gain error (*1024): %ld",gain);
        return;
    }

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void AnaDacDataTransfer(int32_t dac_value, int32_t glitch_val, uint32_t threshold)
/*---------------------------------------------------------------------------------------------------------*\
  This function tests the data transfert to the DAC. Every ms, the given value is written in the DAC. Then
  the DAC is read back through the ADC.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  glitch_event = 0;
    uint32_t  average[2];
    uint32_t  chan[2];
    uint32_t  raw10v;

    if((dev.reg_r | dev.reg_w) & REG_SLOT5)
    {
        MenuRspError("No access to Slot 5 interface");
        return;
    }

    /* Set ana mpx on DAC, DIG mpx on ADC16A and ADC16B 500kHz*/

    AnaMpxAna(3,3);

    if(dev.slot5_type == FGC_INTERFACE_SAR_400)
    {
        raw10v = SLOT5_ADC_SAR_CAL2 - SLOT5_ADC_SAR_CAL1;
        threshold = (raw10v*threshold)/10000000;        // convert threshold from from uV to ADC raw value
    }
    else
    {
        AnaMpxDig(0,1);
        AnaFltFrq(0,0);

        raw10v = SLOT5_ADC_SD_CAL2 - SLOT5_ADC_SD_CAL1;
        threshold = (raw10v/1000)*threshold/10000;      // convert threshold from from uV to ADC raw value
    }

    /* Set DAC with given value for the 1st time */

    dac_value <<= 12;           // Value in DAC is left justified (bits 31-12)
    glitch_val <<= 12;          // Value in DAC is left justified (bits 31-12)

    ANA_DAC_HIGH_P = ((uint16_t*)&dac_value)[0];
    ANA_DAC_LOW_P  = ((uint16_t*)&dac_value)[1];

    dac.val   = dac_value;
    dac.ms_wr = SLOT5_DAC_MS_WR_ON;     // dac_value is written into DAC every ms

    /* Wait for 1s for the DAC to be stable */

    MSLEEP(1000);

    /* Check deviation for chunks of 4s - DAC is written every ms */

    for(;;)
    {
        /* If user abort */

        if(dev.abort_f)
        {
            dac.ms_wr = SLOT5_DAC_MS_WR_OFF;    // Stop DAC writting every ms
            MenuRspError("Aborted by user");
            return;
        }

        /* after 4s, create DAC glitch if asked - glitch value not written to DAC struct to be temporary */

        if(glitch_val && glitch_event)
        {
            ANA_DAC_HIGH_P = ((uint16_t*)&glitch_val)[0];
            ANA_DAC_LOW_P  = ((uint16_t*)&glitch_val)[1];
        }

        AnaGetMultiSample(4096);        // ADCs get DAC output for about 4s
        glitch_event = 1;               // Glitch event armed

        if(dev.slot5_type == FGC_INTERFACE_SAR_400)
        {
            average[0] = (((*(uint32_t*)&menusd.a.sum[0])) << 6) |                // shifts depend on nb samples
                         ((uint32_t)menusd.a.sum[2] >> 10);

            average[1] = (((*(uint32_t*)&menusd.b.sum[0])) << 6) |                // shifts depend on nb samples
                         ((uint32_t)menusd.b.sum[2] >> 10);
        }
        else
        {
            average[0] = ((*(uint32_t*)&menusd.a.sum[0]) << 4) |          // shifts depend on nb samples
                         ((uint32_t)menusd.a.sum[2] >> 12);

            average[1] = ((*(uint32_t*)&menusd.b.sum[0]) << 4) |          // shifts depend on nb samples
                         ((uint32_t)menusd.b.sum[2] >> 12);
        }

        chan[0] = *(uint32_t*)&menusd.a.max_h;
        chan[1] = *(uint32_t*)&menusd.b.max_h;

        chan[0] = labs(chan[0]-average[0]);
        chan[1] = labs(chan[1]-average[1]);

        if((chan[0] > threshold)||(chan[1] > threshold))
        {
            dac.ms_wr = SLOT5_DAC_MS_WR_OFF;    // Stop DAC writting every ms
            MenuRspError("Pos dev chanA-B: 0x%08lX - 0x%08lX (%0.3f - %0.3f mV)",
                                chan[0],chan[1], (float)chan[0]*10000/raw10v, (float)chan[1]*10000/raw10v);
            return;
        }

        /* Check minimum deviation */

        chan[0] = *(uint32_t*)&menusd.a.min_h;
        chan[1] = *(uint32_t*)&menusd.b.min_h;

        chan[0] = labs(chan[0]-average[0]);
        chan[1] = labs(chan[1]-average[1]);

        if((chan[0] > threshold)||(chan[1] > threshold))
        {
            dac.ms_wr = SLOT5_DAC_MS_WR_OFF;    // Stop DAC writting every ms
            MenuRspError("Neg dev chanA-B: 0x%08lX - 0x%08lX (%0.3f - 0.3f mV)",
                                chan[0],chan[1], (float)chan[0]*10000/raw10v, (float)chan[1]*10000/raw10v);
            return;
        }
        fprintf(TERM_TX,"      \r");    // Clear display
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void AnaIsrMst(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called every millisecond from IsrMst() to read out and process the ADC values if
  required on that millisecond
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDD     menusd.num_samples              // Load capture flag into D
        BEQ     end                             // End if no samples required

        PSHM    Z                               // Preserve Z

        LDD     dev.slot5_type                  // Get analogue interface type
        CPD     #FGC_INTERFACE_SAR_400          // Check if standard SAR interface
        BEQ     readasar                        // Branch if SAR
        CPD     #FGC_INTERFACE_SD_350                   // Check if standard SD interface
        BEQ     readsd                          // Branch if SD
        CPD     #FGC_INTERFACE_SD_351           // Check if modified SD interface
        BEQ     readsd                          // Branch if modified SD
        BRA     endreadana                      // Not an ANA interface so jump to end

    readasar:                                   // Type 1 (SAR) ADC A
        LDX     @menusd.a                       // Set IX to point to adc_chan structure for ADC A
        CLRD                                    // D = high word (zero)
        LDZ     #0x0000                         // Z = low word (zero)
        LDE     ANA_SAR_A_16                    // E = mid word (from SAR)
        BPL     apositive                       // Jump if value is positive
        LDD     #0xFFFF                         // D = 0xFFFF (sign extended SAR value)

    apositive:
        ADDE    36,X                            // Add mid byte of SAR average
        ADCD    34,X                            // Add high byte of SAR average plus carry
        STE     36,X                            // Store mid byte of SAR average
        STD     34,X                            // Store high byte SAR average
        DECW    menusd.sar_ave_ct               // decrememt SAR average counter
        BNE     readbsar                        // goto ADC B reading

        XGDZ                                    // to have word in ZED
        JSR     AnaSum                          // Call function to sum new sample
        CLRW    34,X                            // reset average accumulator (4 values added)
        CLRW    36,X
        CLRW    38,X

    readbsar:                                   // Type 1 (SAR) ADC B
        LDX     @menusd.b                       // Set IX to point to adc_chan structure for ADC B
        CLRD                                    // D = high word (zero)
        LDZ     #0x0000                         // Z = low word (zero)
        LDE     ANA_SAR_B_16                    // E = mid word (from SAR)
        BPL     bpositive                       // Jump if value is positive
        LDD     #0xFFFF                         // D = 0xFFFF (sign extended SAR value)

    bpositive:
        ADDE    36,X                            // Add mid byte of SAR average
        ADCD    34,X                            // Add high byte of SAR average plus carry
        STE     36,X                            // Store mid byte of SAR average
        STD     34,X                            // Store high byte SAR average
        TSTW    menusd.sar_ave_ct
        BNE     endreadana

        XGDZ                                    // to have word in ZED
        JSR     AnaSum                          // Call function to sum new sample
        CLRW    34,X                            // reset average accumulator (4 values added)
        CLRW    36,X
        CLRW    38,X
        LDZ     #0x0004
        STZ     menusd.sar_ave_ct               // reset average counter to 4

        BRA     endreadana


    readsd:                                     // Type 2/4 : SD
        LDE     ANA_SD_NL_A_R_MID_16            // E = mid word         * Separate LDE and LDD to avoid
        LDZ     ANA_SD_NL_A_R_HIGH_16           // Z = high word        * optimisation into DPRAM_BT_
        LDD     ANA_SD_NL_A_R_LOW_16            // D = low word
        LDX     @menusd.a                       // Set IX to point to adc_chan structure for ADC A
        JSR     AnaSum                          // Call function to sum new sample
        LDD     ANA_SD_NL_A_R_INDEX_16          // Get last index from filter A
        STD     menusd.a.index

        LDE     ANA_SD_NL_B_R_MID_16            // E = mid word         * Separate LDE and LDD to avoid
        LDZ     ANA_SD_NL_B_R_HIGH_16           // Z = high word        * optimisation into DPRAM_BT_
        LDD     ANA_SD_NL_B_R_LOW_16            // D = low word
        LDX     @menusd.b                       // Set IX to point to adc_chan structure for ADC A
        JSR     AnaSum                          // Call function to sum new sample
        LDD     ANA_SD_NL_B_R_INDEX_16          // Get last index from filter B
        STD     menusd.b.index

    endreadana:
        DECW    menusd.num_samples              // Decrement number of samples remaining to take
        PULM    Z                               // Recover Z

    writedac:                                   // Write dac.val to DAC
        LDD     dac.ms_wr
        BEQ     end                             // if dac.ms_wr = 0, don't write DAC
        LDX     #ANA_DAC_16
        LDD     dac.val                         // dac.val must be left justified
        STD     0,X                             // Store high word of DAC
        LDD     dac.val:2
        STD     2,X                             // Store low word of DAC

    end:
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void AnaSum(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called with the new ADC raw value in ZED and the struct adc_chan pointer in IX.
\*---------------------------------------------------------------------------------------------------------*/
{
    static      uint16_t  sign_extend;

    asm
    {
        CLRW    sign_extend             // Prepare sign extended word to zero
        STD     4,X                     // Save new sample in ->raw
        STE     2,X
        STZ     0,X
        BPL     checkmax                // Branch if raw value is positive
        COMW    sign_extend             // Value in negative so prepare sign extended word to 0xFFFF

    checkmax:                           // Check max limit
        CPZ     12,X                    // Compare Z against ->max_h
        BLT     checkmin                // value is less than max
        BGT     newmax                  // value is greater than max
        CPE     14,X                    // Compare E against ->max_m
        BCS     checkmin                // value is less than max
        BHI     newmax                  // value is greater than max
        CPD     16,X                    // compare D against ->max_l
        BCS     checkmin                // value is less than or equal to max

    newmax:
        STZ     12,X                    // Save new sample in ->max
        STE     14,X
        STD     16,X

    checkmin:                           // Check min limit
        CPZ     6,X                     // Compare Z against ->min_h
        BGT     sum                     // value is greater than min
        BLT     newmin                  // value is less than min
        CPE     8,X                     // Compare E against ->min_h
        BHI     sum                     // value is greater than min
        BCS     newmin                  // value is less than min
        CPD     10,X                    // compare D against ->min_l
        BHI     sum                     // value is greater than or equal to min

    newmin:
        STZ      6,X                    // Save new sample in ->min
        STE      8,X
        STD     10,X

    sum:
        ADDD    30,X                    // 64-bit accummulate of raw value
        STD     30,X
        ADCE    28,X
        STE     28,X
        XGDZ                            // Recover top word of raw value from Z
        ADCD    26,X
        STD     26,X
        LDD     sign_extend             // Recover signed extended word
        ADCD    24,X
        STD     24,X
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void AnaSarAverage(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by AnaIsrMst in SAR reading. AnaIsrMst accumulates 4 measurments. When it's done,
  it calls AnaSarAverage to computes the average value (/4). When entering, D and E are the high and mid bytes
  of SAR 4 point sum (value to divide by 4). This function must exit with IX unchanged and high, mid and low
  byte of average value in ZED.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
    shift1:
        LDD     34,X                            // Load high byte
        LDE     36,X                            // Load mid byte
        CLRW    menusd.carry
        ASRD                                    // divide D by 2
        STD     34,X                            // Store high byte
        BCC     nocarryhigh1
        LDZ     #0x8000                         // Set carry
        STZ     menusd.carry

    nocarryhigh1:
        LSRE                                    // Divide E by 2
        ORE     menusd.carry                    // Add carry from high byte
        LDZ     #0x0000                         // Reset carry without changing CCR carry
        STZ     menusd.carry
        BCC     nocarrymid1
        LDZ     #0x8000                         // Set carry
        STZ     menusd.carry

    nocarrymid1:
        LDD     38,X                            // Load low byte
        ADDD    menusd.carry                    // Add carry from mid byte
        CLRW    menusd.carry                    // Clear carry
        STD     38,X                            // Store low byte

    shift2:
        LDD     34,X                            // Load high byte
        ASRD                                    // divide D by 2
        STD     34,X                            // Store high byte
        BCC     nocarryhigh2
        LDZ     #0x8000                         // Set carry
        STZ     menusd.carry

    nocarryhigh2:
        LSRE                                    // Divide E by 2
        ORE     menusd.carry                    // Add carry from high byte
        STE     36,X                            // Store mid byte
        LDZ     #0x0000
        STZ     menusd.carry                    // Clear carry
        BCC     nocarrymid2
        LDZ     #0x8000                         // Set carry
        STZ     menusd.carry

    nocarrymid2:
        LDD     38,X                            // Load low byte
        LSRD
        ADDD    menusd.carry                    // Add carry from mid byte
        CLRW    menusd.carry
        STD     38,X                            // Store low byte

    resetaverage:
        LDZ     34,X                            // Z = high average byte
        LDE     36,X                            // E = mid average byte
        LDD     38,X                            // D = low average byte
        CLRW    34,X
        CLRW    36,X
        CLRW    38,X
    }
}
#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void AnaPP(void *chan_p)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called with the adc structure pointed to by IY to calculate the peak-peak noise from
  the 48-bit max and min values.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        LDD     16,Y            // 48-bit subtract of min from max
        SUBD    10,Y            // storing result in ->p2p
        STD     22,Y

        LDD     14,Y
        SBCD     8,Y
        STD     20,Y

        LDD     12,Y
        SBCD     6,Y
        STD     18,Y
    }
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void AnaLink(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from IsrMst to read out the FIFO links on an SD-351 interface. The C32 will
  write a copy of what it sent into the 10 longwords at DPRAM_BT_SERIALDATA_16.  The card in test mode
  will reflect back 8 words on link 2.  The either word is skipped.
\*---------------------------------------------------------------------------------------------------------*/
{
    static      uint16_t  err_f;


    if(DPRAM_BT_FIFOCTRL_P == FIFO_TEST_LINK)           // If link is active (SD-351 only)
    {
        err_f = false;

        while((GPT_TCNT_P - pll.ms_t0) < 200);                  // Wait too 100us for data to arrive

        asm
        {
            LDE         #16                             // Copy 8 long words from each link FIFO
            LDX         @link.lk1
            LDY         @link.lk2

        copyloop:
            LDD         LINK_LK1_DATA_16
            STD         0,X
            LDD         LINK_LK2_DATA_16
            STD         0,Y

            AIX         #2
            AIY         #2
            SUBE        #1
            BNE         copyloop

            CLRW        LINK_LK1_RESET_16               // Reset reception FIFO
            CLRW        LINK_LK2_RESET_16               // Reset reception FIFO

            LDED        DPRAM_BT_SERIALDATA_16+36       // Replace word 8 with word 10
            STED        DPRAM_BT_SERIALDATA_16+28

            PSHM        Z
            LDE         #0

            LDX         #DPRAM_BT_SERIALDATA_16
            LDY         @link.lk2
            LDZ         @link.n_errs

        cmploop:                                        // Compare 8 long words from link 2
            LDD         0,X
            CPD         0,Y
            BNE         mismatch
            LDD         2,X
            CPD         2,Y
            BEQ         match
        mismatch:

            LDD         #1
            STD         err_f
            ADDD        E,Z
            STD         E,Z
            LDD         0,X
            STD         32,Y
            LDD         2,X
            STD         34,Y
            LDD         0,Y
            STD         64,Y
            LDD         2,Y
            STD         66,Y

        match:
            AIX         #4
            AIY         #4
            ADDE        #2
            CPE         #16
            BLT         cmploop

            PULM        Z
        }

        link.n_rx++;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ana.c
\*---------------------------------------------------------------------------------------------------------*/

