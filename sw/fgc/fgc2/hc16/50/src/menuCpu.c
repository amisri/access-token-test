/*---------------------------------------------------------------------------------------------------------*\
  File:         menuCpu.c

  Purpose:      Functions to run for the cpu boot menu options

  Notes:

  History:

    05/01/04    stp     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <c32.h>                // for FIFO_TEST_LINK, StopDsp()
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <menu.h>               // for MenuRspArg(), MenuGetInt16U(), MenuRspError()
#include <defconst.h>           // for FGC_INTERFACE_
#include <memmap_mcu.h>         // for DPRAM_BT_FIFOCTRL_P
#include <analog.h>             // for struct link
#include <main.h>               // for CheckSlot5IfNot()
#include <mem.h>                // for MemCpyBytes()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuCpuStartC32(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Start the C32 running
\*---------------------------------------------------------------------------------------------------------*/
{
    dsp.status = 0;             // Clear status to force a restart attempt

    DspLoadProgramAndRun();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCpuStopC32(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Stop the TMS320C32 running
\*---------------------------------------------------------------------------------------------------------*/
{
    StopDsp();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCpuSetFifo(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the FIFO port control bits (Bit3: Test F28 Link, Bit2: CLX,  Bit1: FSX,  Bit0: DX).
  This leaves the C32 running!
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      value;
    uint32_t      ctrl;

    if(MenuGetInt16U(argv[0], 0, 8, &value))    // Get FIFO value
    {
        return;
    }

    if ( !dsp.is_running )                              // If C32 is not running
    {
        MenuRspError("C32 not running");                // Report error
        return;
    }

    ctrl = FIFO_PORT_CTRL;                      // Translate into C32 serial port register value

    if(value & 0x1)
    {
        ctrl |= FIFO_DX;
    }

    if(value & 0x2)
    {
        ctrl |= FIFO_FSX;
    }

    if(value & 0x4)
    {
        ctrl |= FIFO_CLX;
    }

    if(value & 0x8)                             // If user specifies 8
    {
        if ( CheckSlot5IfNot(FGC_INTERFACE_SD_351,0) )          // Check that FGC contains an SD-351
        {
            return;
        }

        ctrl = FIFO_TEST_LINK;                          // Enable Tx and Rx of data with SimPConv card
    }

    DPRAM_BT_FIFOCTRL_P = ctrl;                 // Transfer to C32 via DPRAM area of HC16 memory
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCpuSimPConvLinks(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              idx;
    struct link         link_data;

    if(CheckSlot5IfNot(FGC_INTERFACE_SD_351,0))         // Check that FGC contains an SD-351
    {
        return;
    }

    OS_ENTER_CRITICAL();

    MemCpyBytes((uint8_t*)&link_data, (uint8_t*)&link, sizeof(link_data));

    OS_EXIT_CRITICAL();

    for(idx=0;idx < 16;idx+=2)
    {
        MenuRspArg("0x%04X%04X", link_data.lk1[idx],link_data.lk1[idx+1]);      //
        MenuRspArg("0x%04X%04X", link_data.lk2[idx],link_data.lk2[idx+1]);
        MenuRspArg("0x%04X%04X", link_data.tx [idx],link_data.tx [idx+1]);
        MenuRspArg("0x%04X%04X", link_data.rx2[idx],link_data.rx2[idx+1]);
        MenuRspArg("%u", link_data.n_errs[idx/2]);

        MenuRspArg(NULL);                               // Flush buffer to force one line per entry
    }

    MenuRspArg("%lu",link_data.n_rx);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCpuPBLLinks(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the data sent by the DSP to the PBL/SimPConv cards
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      data_idx;
    uint16_t      high_byte;
    uint16_t      mid_byte;
    uint16_t      low_word;
    uint32_t      value;

    if(MenuGetInt16U(argv[0], 0, 9, &data_idx))         // Get data index (1-8)
    {
        return;
    }
    if(MenuGetInt16U(argv[1], 0, 255, &high_byte))      // Get high byte
    {
        return;
    }
    if(MenuGetInt16U(argv[2], 0, 255, &mid_byte))       // Get mid byte
    {
        return;
    }
    if(MenuGetInt16U(argv[3], 0, 65535, &low_word))     // Get low word
    {
        return;
    }

    if ( !dsp.is_running )                              // If C32 is not running
    {
        MenuRspError("C32 not running");                // Report error
        return;
    }

    value = (high_byte <<  8) | mid_byte;
    value = (value     << 16) | low_word;

    DPRAM_BT_SERIALDATA_A[data_idx] = value;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuCpu.c
\*---------------------------------------------------------------------------------------------------------*/


