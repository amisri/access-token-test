/*---------------------------------------------------------------------------------------------------------*\
  File:         C32.c

  Purpose:      Start and stop the C32 boot test program.

  Author:       Stephen.Page@cern.ch

  History:

    05/02/04    stp     Created
    09/03/05    qak     File renamed to c32.c and C32 standalone flag now handled
    07/04/05    qak     Detect memory faults
\*---------------------------------------------------------------------------------------------------------*/

#define C32_GLOBALS

#include <c32.h>

#include <dsp_50.h>
#include <memmap_mcu.h>         // for CPU_RESET_CTRL_DSP_MASK16, DPRAM_BT_FIFOCTRL_P
#include <mcu_dependent.h>      // for TST_CPU_MODEL(), MSLEEP()
#include <main.h>               // for timeout_ms global variable used by MSLEEP()
#include <menu.h>               // for MenuRspError(), menu global variable
#include <fieldbus_boot.h>      // for stat_var global variable
#include <runlog_lib.h>         // for RunlogWrite()
#include <defconst.h>           // for FGC_FLT_, FGC_LAT_
#include <fgc_runlog_entries.h> // for FGC_RL_DSP_LOAD
#include <macros.h>             // for Set()
#include <mem.h>                // for MemCpyWords()
#include <registers.h>          // for RegWrite(), RegRead()
#include <stdbool.h>



/*---------------------------------------------------------------------------------------------------------*/
uint16_t DspLoadProgramAndRun(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will return 1 if the TMS320C32 is in standalone mode or the memory isn't available.
  Otherwise it will copy the C32 test code into the C32 SRAM via the DPW and will start the C32.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      t0;
    uint16_t      data;

    CPU_RESET_P |= CPU_RESET_CTRL_DSP_MASK16;                           // C32 Reset

    if (    dsp.is_running                                              // If C32 is already running or
         || (dsp.status == C32_STATUS_FAILED_TO_START)                  // already failed to start
       )
    {
        return(0);                                                      // Return 0
    }

    if(TST_CPU_MODEL(DSPSTAL))                                          // If C32 in standalone mode
    {
        MenuRspError("C32 is in stand-alone mode");                     // Report error
        dsp.status = C32_STATUS_STANDALONE;
        return(1);                                                      // Return 1
    }

    CPU_RESET_P |= CPU_RESET_CTRL_DSPTSM_MASK16;                        // C32 Reset+Tristate
    asm NOP;
    CPU_RESET_P &= ~CPU_RESET_CTRL_DSP_MASK16;                          // C32 Tristate

    timeout_ms = 100;                                                   // Set timeout to 100ms

    while(timeout_ms && TST_CPU_RESET(C32INIT));                        // Wait for C32 memory to initialise

    if(!timeout_ms)                                                     // If timeout expired
    {
        MenuRspError("C32 Memory fault - failed to initialise");        // Report error
        dsp.status = C32_STATUS_MEM_FAULT;
        return(1);
    }

    SELDPWPG(0);                        // Select C32 SRAM page 0
    SELFLASH(SEL_SR);                   // Select DPW in page 6 and reset DPW base address to page 0

    if(RegRead(DPW_32,  &data, true)    ||      // If read of first word of DPW fails, or
       RegRead(DPW_32+2,&data, true)    ||      // read of second word of DPW fails, or
       RegWrite(DPW_32,  0, true)       ||      // write of first word of DPW fails, or
       RegWrite(DPW_32+2,0, true))              // write of second word of DPW fails
    {
        MenuRspError("C32 Memory fault - bus error");           // Report error
        dsp.status = C32_STATUS_MEM_FAULT;
        CPU_RESET_P |= CPU_RESET_CTRL_DSP_MASK16;                                       // C32 Reset
        return(1);
    }

    MemCpyWords( DPW_32, CODES_BOOTPROG23_32 + (uint32_t) &dsp_50_code, DSP_50_CODE_SIZE/2); // Write test program into C32 SRAM

    DPRAM_BT_HC16EDACSBES_P = 0;                                // Clear C32 EDAC counters
    DPRAM_BT_C32EDACSBES_P  = 0;
    DPRAM_BT_FIFOCTRL_P     = FIFO_PORT_CTRL;                   // Initialise FIFO port control
    DPRAM_BT_C32STATUS_P    = 0;                                // Clear C32 status
    dsp.irq_mask = 0;
    dsp.irq_fail = 0;

    CPU_RESET_P |= CPU_RESET_CTRL_DSP_MASK16;                                           // C32 Reset+Tristate
    asm NOP;
    CPU_RESET_P &= ~CPU_RESET_CTRL_DSPTSM_MASK16;                                       // C32 Reset
    asm NOP;
    CPU_RESET_P &= ~CPU_RESET_CTRL_DSP_MASK16;                                          // C32 Run...

    t0 = GPT_TCNT_P;

    while((GPT_TCNT_P-t0) < 1000 && !DPRAM_BT_C32STATUS_P);     // Wait up to 500us for C32 to start

    // There is an issue with the DSP not starting. Since it is not needed by the BOOT
    // just ignore any errors related to tihs to make sure the MAIN is launched.
    
    if(!DPRAM_BT_C32STATUS_P)                                   // If C32 did not respond
    {
        // MenuRspError("C32 failed to start but ignoed");                            // Report error
        CPU_RESET_P |= CPU_RESET_CTRL_DSP_MASK16;                                               // C32 Reset
        asm NOP;
        CPU_RESET_P |= CPU_RESET_CTRL_DSPTSM_MASK16;                                            // C32 Reset+Tristate
        // dsp.status = C32_STATUS_FAILED_TO_START;
        return(1);
    }

    dsp.is_running = -1;                                                // Flag to IsrTick() to test C32

    MSLEEP(10);                                                 // Wait 10ms for IRQ tests

    if(dsp.irq_fail)                                            // If IRQ failures reported
    {
        MenuRspError("C32 IRQ fail mask: 0x%04X",dsp.irq_fail);         // Report error
        return(1);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t StopDsp(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will stop the TMS320C32 and enable the dual port window onto the C32 SRAM.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(TST_CPU_MODEL(DSPSTAL))                          // If C32 in standalone mode
    {
        MenuRspError("C32 is in stand-alone mode");             // Report error
        return(1);                                              // Return 1
    }

    if ( !dsp.is_running )                                      // If C32 isn't runing
    {
        return(0);                                              // Return immediately
    }

    dsp.is_running  = 0;                                        // Flag to ISR to stop testing C32
    dsp.status = 0;                                     // Clear C32 status

    CPU_RESET_P |= CPU_RESET_CTRL_DSP_MASK16;                                   // C32 Reset
    asm NOP;
    CPU_RESET_P |= CPU_RESET_CTRL_DSPTSM_MASK16;                                // C32 Reset+Tristate

    timeout_ms = 100;                                   // Set timeout to 100ms

    while(timeout_ms && TST_CPU_RESET(C32INIT));        // Wait for C32 to initialise

    if(!timeout_ms)                                     // If timeout expired
    {
        MenuRspError("C32 Memory fault - failed to initialise");// Report error
        dsp.status = C32_STATUS_MEM_FAULT;
        return(1);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
void RefreshStatusWithDspState(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by CheckStatus() to check the TMS320C32 status.
\*---------------------------------------------------------------------------------------------------------*/
{
    static uint16_t       c32_status_old;

    if(dsp.status &    (C32_STATUS_FAILED_TO_START      |
                        C32_STATUS_BG_FAILED            |
                        C32_STATUS_MEM_FAULT            |
                        CPU_MODEH_C32IRQ_0_MASK8        |
                        CPU_MODEH_C32IRQ_1_MASK8        |
                        CPU_MODEH_C32IRQ_2_MASK8        |
                        CPU_MODEH_C32IRQ_3_MASK8))
    {
        Set(stat_var.fgc_stat.class_data.c50.st_faults,    FGC_FLT_FGC_HW);
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DSP_FLT);
    }

    if(dsp.status &    (C32_STATUS_HC16_SBE             |
                        C32_STATUS_C32_SBE              |
                        C32_STATUS_PATTERN_FAULT        |
                        C32_STATUS_ADDRESS_FAULT))
    {
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DSP_FLT);
    }

    if(dsp.status != c32_status_old)                            // If C32 status changed
    {
        RunlogWrite(FGC_RL_DSP_LOAD, &dsp.status);    // Write C32 status as entry to run log
        c32_status_old = dsp.status;                            // Remember old C32 status
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: c32.c
\*---------------------------------------------------------------------------------------------------------*/

