/*---------------------------------------------------------------------------------------------------------*\
  File:         menuRadTests.c

  Purpose:      Functions to run for the flash boot menu options

  Author:       sdubetti

  Notes:

  History:

    10/03/08    stp     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <stdio.h>              // for fprintf()
#include <mcu_dependent.h>      // for MSLEEP()
#include <main.h>               // for timeout_ms global variable used by MSLEEP()
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <defconst.h>           // for FGC_INTERFACE_
#include <defconst.h>           // for FGC_CRATE_TYPE_
#include <dev.h>                // for dev global variable
#include <memmap_mcu.h>         // for CPU_RESET_P
#include <crate_boot.h>         // for crate global variable
#include <rad.h>                // for RadHc16InitRAM(), RadInitAverageP2PRef(), RadInitFIP()
#include <menu.h>               // for menu global variable, MenuRspError()
#include <term.h>               // for TERM_TX

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

#define UNIX_TIME_SHIFT         300     // shift for the next unix time (sec)
#define UNIX_TIME_SHIFT_POINT   10
#define SEND_ALL                7

uint32_t  sbe_hc16_last;
uint32_t  sbe_c32_last;
uint32_t  next_unix_time;
uint32_t  next_unix_time_point;
uint32_t  c32_sbe_int_ram;
uint32_t  c32_sbe_int_reg_failed;
uint32_t  c32_sbe_int_reg_counter;
uint32_t  c32_sbe_int_reg_idx;
uint16_t  ana_ctrl_register;

/*---------------------------------------------------------------------------------------------------------*/
void MenuRadiationTestSequence(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    c32_sbe_int_reg_idx         = 0;                                //init  the register failed index
    next_unix_time_point        = SM_UXTIME_P + UNIX_TIME_SHIFT_POINT; //init the next unix time before write a point
    next_unix_time              = SM_UXTIME_P + UNIX_TIME_SHIFT;    //init the next unix time before send data
    c32_sbe_int_ram             = 0;                                //init the nb of internal RAM SBE for C32
    c32_sbe_int_reg_counter     = 0;                                //init the nb of register corrupted for C32
    c32_sbe_int_reg_failed      = 0;                                //init the nb of register failed for C32
    sbe_hc16_last               = 0;                                //init the nb of EDAC SBE for HC16
    sbe_c32_last                = 0;                                //init the nb of EDAC SBE for C32
    dev.mem_sbe_f               = 0;                                //clear the EDAC SBE Flag

    if(dev.state == BOOT_STATE_SELF_TEST && crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        return;
    }

    // no needed anymore, was for CNGS test to disable the serial port due to problems
//  QSM_SCCR1_P = 0x0008;                               // RS232 Rx disable.

    CPU_RESET_P &= ~CPU_RESET_CTRL_NETWORK_MASK16;                              // Clear hard reset of uFIP
    RadHc16InitRAM ();                                  // init to 0x0000 the internal HC16 RAM
    RadInitAverageP2PRef();                             // init average and p2p reference
    RadInitFIP();                                       // init to 0x0000 the registers : TXDMSG and RXDMSG

    for (;;)
    {
        if (dev.abort_f)                                // stop if the user tape <Ctrl> + C
        {
            menu.error_buf[0] = '\0';                   // Suppress error
            return;
        }

        RadHc16C32ExtRAM();                             // test the external RAM of the C32 and HC16
        RadHc16C32IntRAM();                             // test the internal RAM for the HC16 and C32
        RadHc16MicrofipC32RegTest();                    // test if the internal registers are corrupted

        ana_ctrl_register = ANA_CTRL_P;                 // test ana card
        switch(dev.slot5_type)
        {
        case FGC_INTERFACE_SD_350 :
            if (!( ana_ctrl_register & (ANA_CTRL_FLTANOTRDY_MASK16 | ANA_CTRL_FLTBNOTRDY_MASK16)))
            {
                RadGetMultiSample();                            // test the adc filter
            }
            break;

        case FGC_INTERFACE_SAR_400 :
            RadGetMultiSample();                                // test the adc
            break;


        default :                                                       // problem with the software
            fprintf(TERM_TX,"\n" RSP_ERROR ",software corrupted : ana test selection problem.\n");
            RadSendData (SEND_ALL);                                     // Send all datas
            MSLEEP(2000);                                               // Wait for 2s
            OS_ENTER_CRITICAL();                                        // Stop interrupts to prevent IsrMst() from writing LEDs register
                                                                        // and there for losing power cycle request (same location)
            CPU_PWRCYC_P = 0xF00E;                                      // Write power cycle request to the NDI register

            for(;;);                                                    // Wait for power cycle to act with interrupts disabled
        }

        if ( next_unix_time <= SM_UXTIME_P)             // if the delay is finish send all information
        {
             RadSendData (SEND_ALL);
             next_unix_time = SM_UXTIME_P + UNIX_TIME_SHIFT;
        }

        if ( next_unix_time_point <= SM_UXTIME_P)               // if the delay is finish write a point
        {
            fprintf(TERM_TX, ".");
            next_unix_time_point = SM_UXTIME_P + UNIX_TIME_SHIFT_POINT;
        }
   }
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuRadTests.c
\*---------------------------------------------------------------------------------------------------------*/


