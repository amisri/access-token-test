/*---------------------------------------------------------------------------------------------------------*\
  File:         codes_fgc.c

  Purpose:      FGC Boot code update functions

  Notes:        These functions are shared by the FGC2 and FGC3 boot programs
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdio.h>

#include <codes_fgc.h>
#include <memmap_mcu.h>         // Include memory map consts
#include <codes_holder.h>
#include <dev.h>                // for dev global variable
#include <fgc_consts_gen.h>
#include <term.h>               // for TERM_TX
#include <codes.h>              // for code global variable
#include <stdbool.h>

/*---------------------------------------------------------------------------------------------------------*/
void CodesUpdateLast(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will process the last block of a code (blk_idx == code.rx_last_blk_idx)
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(TERM_TX,RSP_DATA ",Programming %s into %s complete - CRC ",
            fgc_code_names[codes_holder_info[code.rx_dev_idx].update.code_id],
            codes_holder_info[code.rx_dev_idx].label);

    fputs((check_dev[code.rx_dev_idx](code.rx_dev_idx) ? "BAD\n" : "OK\n"),TERM_TX);

    code.rx_active_f = false;                           // Stop reception of this code

    CodesCheckUpdates();                                // Check updates

    if(code.rx_mode == CODE_RX_FIELDBUS &&                      // If receiving from FIP and
       !dev.n_rw_devs_to_update)                        // no more codes to update
    {
        code.rx_mode = CODE_RX_NONE;                            // Disable code reception
    }
}



enum CodesVisibility CodesGetCodeVisibility(enum fgc_code_ids_short_list code_id)
{
    switch(code_id)
    {

    case SHORT_LIST_CODE_MP_DSP:
    case SHORT_LIST_CODE_BTB:
    case SHORT_LIST_CODE_BTA:
    case SHORT_LIST_CODE_DIMDB:
    case SHORT_LIST_CODE_COMPDB:
    case SHORT_LIST_CODE_NAMEDB:
    case SHORT_LIST_CODE_SYSDB:

        return CODES_VISIBILITY_MCU;

    case SHORT_LIST_CODE_PTDB:
    case SHORT_LIST_CODE_IDPROG:
    case SHORT_LIST_CODE_IDDB:

        return CODES_VISIBILITY_M16C62;

    default:

        return CODES_VISIBILITY_NONE;
    }
}



uint32_t * CodesGetCodeVersionPointer(enum fgc_code_ids_short_list code_id)
{

    switch(code_id)
    {

    case SHORT_LIST_CODE_MP_DSP:

        return (uint32_t *)SM_CODES_MAINPROG_32;

    case SHORT_LIST_CODE_BTB:

        // If running out of boot B

        if(dev.device == 'B')
        {
            return (uint32_t *)SM_CODES_BOOTPROG_32;
        }
        return NULL;

    case SHORT_LIST_CODE_BTA:

        // If running out of boot A

        if(dev.device == 'A')
        {
            return (uint32_t *)SM_CODES_BOOTPROG_32;
        }
        return NULL;

    case SHORT_LIST_CODE_DIMDB:

        return (uint32_t *)SM_CODES_DIMDB_32;

    case SHORT_LIST_CODE_COMPDB:

        return (uint32_t *)SM_CODES_COMPDB_32;

    case SHORT_LIST_CODE_NAMEDB:

        return NULL;

    case SHORT_LIST_CODE_SYSDB:

        return (uint32_t *)SM_CODES_SYSDB_32;

    case SHORT_LIST_CODE_PTDB:

        return (uint32_t *)SM_CODES_PTDB_32;

    case SHORT_LIST_CODE_IDPROG:

        return (uint32_t *)SM_CODES_IDPROG_32;

    case SHORT_LIST_CODE_IDDB:

        return (uint32_t *)SM_CODES_IDDB_32;

    default:

        return NULL;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_fgc.c
\*---------------------------------------------------------------------------------------------------------*/
