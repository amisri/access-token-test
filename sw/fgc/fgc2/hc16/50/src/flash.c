/*---------------------------------------------------------------------------------------------------------*\
  File:         flash.c

  Purpose:      Flash programming functions

  Author:       Stephen.Page@cern.ch

  Notes:

  History:

    13/08/03    stp     Created
    04/03/05    qak     Changed to use 32-bit address instead of separate page address and 16-bit offset
    04/03/05    qak     FlashSectorErase() added
\*---------------------------------------------------------------------------------------------------------*/

#include <flash.h>
#include <stdbool.h>
#include <main.h>               // for timeout_ms global variable
#include <menu.h>               // for MenuRspError(), menu global variable
#include <fgc_runlog_entries.h> // for FGC_RL_
#include <runlog_lib.h>         // for RunlogWrite()
#include <registers.h>          // for RegRead()


/*---------------------------------------------------------------------------------------------------------*/
void FlashReset(uint32_t address)
/*---------------------------------------------------------------------------------------------------------*\
  Reset flash device.
\*---------------------------------------------------------------------------------------------------------*/
{
    *((uint16_t *FAR)address) = 0x00F0;   // Write Flash reset code to address
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t FlashChipErase(uint32_t address)
/*---------------------------------------------------------------------------------------------------------*\
  Erase a flash chip completely.  Address should be the start of the chip.  The function returns zero on
  success or 1 on failure (with an entry in the run log).
\*---------------------------------------------------------------------------------------------------------*/
{
    FlashReset(address);                // Reset flash device

    asm
    {
        PSHM    K                       // Save page registers

        LDAB    address:1               // B  = number of page
        TBXK                            // XK = B
        LDX     #0x0000

        LDD     #0x00AA                 // Write unlock code
        STD     0x0AAA,X

        LDD     #0x0055
        STD     0x0554,X

        LDD     #0x0080                 // Write command: set
        STD     0x0AAA,X

        LDD     #0x00AA                 // Write unlock code
        STD     0x0AAA,X

        LDD     #0x0055
        STD     0x0554,X

        LDD     #0x0010                 // Write command: chip erase
        STD     0x0AAA,X

        PULM    K                       // Restore page registers
    }

    if(FlashCheck(address, 0xFFFF))
    {
        MenuRspError("FlashChipErase at 0x%05lX failed",address);       // Report error
        return(1);
    }

    return(0);                  // Return no error
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t FlashSectorErase(uint32_t address)
/*---------------------------------------------------------------------------------------------------------*\
  Erase a flash sector.  Address should be the start of the sector.  The function returns zero on
  success or 1 on failure (with an entry in the run log).
\*---------------------------------------------------------------------------------------------------------*/
{
    FlashReset(address);                // Reset flash device

    asm
    {
        PSHM    K                       // Save page registers

        LDAB    address:1               // B  = number of page
        TBXK                            // XK = B
        LDX     #0x0000

        LDD     #0x00AA                 // Write unlock code
        STD     0x0AAA,X

        LDD     #0x0055
        STD     0x0554,X

        LDD     #0x0080                 // Write command: set
        STD     0x0AAA,X

        LDD     #0x00AA                 // Write unlock code
        STD     0x0AAA,X

        LDD     #0x0055
        STD     0x0554,X

        LDD     #0x0030                 // Write command: sector erase
        LDX     address:2               // to address in sector
        STD     0,X

        PULM    K                       // Restore page registers
    }

    if(FlashCheck(address, 0xFFFF))
    {
        MenuRspError("FlashSectorErase at 0x%05lX failed",address);     // Report error
        return(1);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t FlashMemSet(uint32_t address, uint16_t value, uint32_t n_words)
/*---------------------------------------------------------------------------------------------------------*\
  Set a region of flash memory to a value.  The function returns zero on success and 1 if an error occurs.
  An entry will be made in the runlog in case of error.
\*---------------------------------------------------------------------------------------------------------*/
{
    while(n_words--)                            // For the specified number of words
    {
        FlashProgram(address, value);                   // Write value to address

        if(FlashCheck(address, value))                  // Verify write
        {
            MenuRspError("FlashMemSet 0x%04X failed at 0x%05lX failed",value,address);
            return(1);
        }

        address += 2;                                   // Increment address by one word
    }

    return(0);                                  // Return 0 on success
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t FlashProgram(uint32_t address, uint16_t value)
/*---------------------------------------------------------------------------------------------------------*\
  Program a flash location with value.  If check the current value of the location (with bus error
  protection) and returns immediately if the value is already correct.  It also checks that the programming
  request is not impossible (bits cannot go from 0 to 1).  The function returns zero on success or 1 on
  failure with an entry in the run log if required.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      flash_value;
    uint16_t      new_flash_value;

    if ( RegRead(address, &flash_value, true) )         // Read value in flash
    {
        MenuRspError("Bus error reading(1) word from flash at 0x%05lX",address);        // Report bus error
        return(1);
    }

    if(flash_value == value)                            // If flash already has new value
    {
        return(0);                                              // Return immediately
    }

    if(value & ~flash_value)                            // If write is not possible
    {
        MenuRspError("Erase required to write 0x%04X to flash at 0x%05lX (0x%04X)",     // Report error
                        value,address,flash_value);
        return(1);
    }

    asm                                                 // Attempt to write word to address
    {
        PSHM    K                       // Save page registers

        LDAB    address:1               // B  = number of page
        TBXK                            // XK = B
        LDX     #0x0000

        LDD     #0x00AA                 // Write flash unlock sequence
        STD     0x0AAA,X

        LDD     #0x0055
        STD     0x0554,X

        LDD     #0x00A0                 // Write command code: Program
        STD     0x0AAA,X

        LDX     address:2               // Write data to specified address
        LDD     value
        STD     0,X

        PULM    K                       // Restore page registers
    }

    if(FlashCheck(address, value))
    {
        if(RegRead(address, &new_flash_value, true))            // Read value in flash
        {
            MenuRspError("Bus error reading(2) word from flash at 0x%05lX",address);    // Report bus error
            return(1);
        }
        MenuRspError("FlashProgram 0x%04X at 0x%05lX failed (0x%04X->0x%04X)",          // Report error
                        value,address,flash_value,new_flash_value);
        return(1);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t FlashCheck(uint32_t address, uint16_t value)
/*---------------------------------------------------------------------------------------------------------*\
  Check whether the last flash write or erase operation completed successfully.  The function will
  check if the expected value is now at the specified address.  It will return 0 on success and 1 on failure
  with an entry in the run log.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       result = 0;

    timeout_ms = FLS_TIMEOUT_MS;        // Set millisecond timeout for flash operation

    asm
    {
        PSHM    K                       // Save page registers

        LDX     address:2               // XK:IX = address
        LDAB    address:1
        TBXK

    check:
        LDD     0x0000,X                // Read location in flash twice and check for toggling bits
        EORD    0x0000,X
        BITB    #0x40                   // Check XOR on toggle bit (D6)
        BEQ     verify                  // Branch to verify value if bits no longer toggling

        TST     timeout_ms              // Check for timeout on flash operation
        BEQ     timeout                 // Branch to timeout if timeout period expired

        LDD     0x0000,X                // Read location in flash to test timeout bit
        BITB    #0x20                   // Check Flash Timeout bit (D5)
        BEQ     check                   // Branch to check if Flash Timeout not expired

        LDD     0x0000,X                // Check again if toggle bits are now stable
        EORD    0x0000,X
        BITB    #0x40                   // Check XOR on toggle bit (D6)
        BEQ     verify                  // Branch to verify value if bits no longer toggling

    timeout:
        LDAA    #FGC_RL_FLS_TO  // Prepare to report flash timeout via run log
        STAA    result
        BRA     end

    verify:
        LDD     0x0000,X                // Readback value from location
        CPD     value                   // Check against expected value
        BEQ     end                     // Branch if okay

        LDAA    #FGC_RL_FLS_VERIFY      // Prepare to report verify failure via run log
        STAA    result

    end:
        PULM    K                       // Restore page registers
    }

    /*--- Report failues to the run log ---*/

    if(result)                          // If failure occured
    {
        RunlogWrite(result, &address);          // Write entry to run log with address as data
        FlashReset(address);                    // Reset flash
        return(1);                              // return 1 to indicate failure
    }

    return(0);                          // Return zero to indicate success
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: flash.c
\*---------------------------------------------------------------------------------------------------------*/
