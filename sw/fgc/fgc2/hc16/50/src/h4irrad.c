/*---------------------------------------------------------------------------------------------------------*\
  File:         h4irrad.c

  Purpose:      Functions for radiation tests.

  Notes:

  History:

    07/06/11    hl      Created
\*---------------------------------------------------------------------------------------------------------*/

#define H4IRRAD_GLOBALS

#include <stdint.h>

#include <h4irrad.h>

#include <stdio.h>              // for fprintf()
#include <stdbool.h>
#include <term.h>               // for TERM_TX
#include <memmap_mcu.h>         // for SM_UXTIME_P
#include <defconst.h>           // for FGC_CRATE_TYPE_
#include <dev.h>                // for dev global variable
#include <crate_boot.h>         // for crate global variable
#include <mcu_dependent.h>      // for MSLEEP()
#include <main.h>               // for timeout_ms global variable used by MSLEEP()
#include <menu.h>               // for MenuRspError(), MenuGetInt16U()
#include <c32.h>                // for FIFO_PORT_CTRL

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuRadiationH4IRRADDutStopAll(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
    Stop all tests
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t i;

    h4irrad_run_f  = false;
    ANA_DAC_HIGH_P = 0;
    ANA_DAC_LOW_P  = 0;
    DPRAM_BT_FIFOCTRL_P = FIFO_PORT_CTRL;

    // Switch off all
    dut_cmd_vector = 0;
    DUT_CMD = 0xFF00;
    for (i = 0; i < NB_DUT; i++)
    {
        dut_state[i] = NC;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void MenuRadiationH4IRRADDutSelectAndRun(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
    This function read the DUT configuration.
    Switch DUTs ON/OFF according to config
    Initialise config if at least one DUT on
    Reset config if no DUT selected
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t i;

    dut_cmd_vector = 0;

    if (crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }

    if (!dsp.is_running) // If C32 is not running
    {
        MenuRspError("C32 not running"); // Report error
        return;
    }
#ifndef H4IRRAD
    MenuRspError("Please compile with H4IRRAD flag"); // H4IRRAD is mandatory to enable control in ISR
    return;
#endif

    for (i = 0; i < NB_DUT; i++)
    {
        if (*argv[i] == 'y' || *argv[i] == 'Y')
        {
            dut_cmd_vector = dut_cmd_vector | dut_on_mask[i];
        }
        else if (*argv[i] == 'n' || *argv[i] == 'N')
        {
            // do nothing
        }
        else
        {
            MenuRspError("Unexpected value %c", *argv[i]);
            return;
        }
    }

    if (!dut_cmd_vector)
    {
        MenuRadiationH4IRRADDutStopAll(0, NULL );

    }
    else
    {
        h4irradInitTest();
        dut_last_status = DUT_STATUS;
        h4irrad_run_f = true;

        // Switch on only selected
        DUT_CMD = ((~dut_cmd_vector) << 8) | dut_cmd_vector;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void h4irradSupplyControl(void)
/*---------------------------------------------------------------------------------------------------------*\
    Run every ms by ISR.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t i;
    uint8_t refresh_screen_event_f;

    if (crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        return;
    }

    if (!h4irrad_run_f)
    {
        return;
    }

    if (SM_MSTIME_P % REFRESH_STATUS_TIME_MS)
    {
        refresh_screen_event_f = 0;
        dut_status = DUT_STATUS & dut_last_status;
        for (i = 0; i < NB_DUT; i++)
        {
            switch (dut_state[i])
            {
                case RUNNING:
                    // check status. Status is active low
                    // status is checked every REFRESH_STATUS_TIME_MS ms+processing time
                    // only faults longer than this time are taken in account
                    if (dut_status_mask[i] & (~dut_status))
                    {
                        dut_faults_cnt[i]++;
                        dut_state[i] = RETRY;
                        retry_time[i] = SM_UXTIME_P + RETRY_TIME_S;
                        DUT_CMD = dut_on_mask[i] << 8; // Reset DUT i
                        refresh_screen_event_f = true;
                    }
                    break;
                case RETRY:
                    // halt, wait 1min before trying again
                    if (dut_faults_cnt[i] == MAX_FAULTS)
                    {
                        dut_state[i] = FAULT;
                        refresh_screen_event_f = true;
                    }
                    else if (retry_time[i] <= SM_UXTIME_P ) // count approximately 1min
                    {
                        dut_state[i] = RUNNING;
                        DUT_CMD = dut_on_mask[i]; // Set DUT i
                        refresh_screen_event_f = true;
                    }
                    break;
                case FAULT:
                    // too much fault, stop forever
                case NC:
                    // Not connected. Do nothing
                    break;
            }
        }
        dut_last_status = DUT_STATUS;

        if (update_fct_time <= SM_UXTIME_P && dac_function == 0) // count 1 min
        {
            h4irradADCUpdateStepFct();
            update_fct_time = SM_UXTIME_P + STEP_WIDTH_S;
            refresh_screen_event_f = true;
        }

        if (refresh_screen_event_f)
        {
            refresh_screen_f++;
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void h4irradInitTest(void)
/*---------------------------------------------------------------------------------------------------------*\
  Initialisation before running test.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t i;

    refresh_screen_f++;

    for (i = 0; i < NB_DUT; i++)
    {
        dut_faults_cnt[i] = 0;
        if (dut_cmd_vector & dut_on_mask[i])
        {
            dut_state[i] = RUNNING;
        }
        else
        {
            dut_state[i] = NC;
        }
    }

    update_fct_time = SM_UXTIME_P + STEP_WIDTH_S;

    if (dac_function == 0)
    {
        dac_step_idx = 0;
        h4irradADCUpdateStepFct();
    }
    else
    {
        dac_value = -dac_mV * 43.69;
    }
    dac_value <<= 12; // Value in DAC is left justified (bits 31-12)
    ANA_DAC_HIGH_P = ((uint16_t*) &dac_value)[0];
    ANA_DAC_LOW_P = ((uint16_t*) &dac_value)[1];

    if ((dac_clk == 0) && (dac_nbStep == 0) && (dac_step_uration == 0))
    {
        // Default values
        dac_clk = 100;
        dac_nbStep = 2;
        dac_step_uration = 1;
    }

    DPRAM_BT_SERIALDATA_A [0] = dac_clk;
    DPRAM_BT_SERIALDATA_A [1] = dac_nbStep;
    DPRAM_BT_SERIALDATA_A [2] = dac_step_uration;

    DPRAM_BT_FIFOCTRL_P = H4IRRAD_DAC_TEST;
}

/*---------------------------------------------------------------------------------------------------------*/
void MenuRadiationH4IRRADReportTest (void)
/*---------------------------------------------------------------------------------------------------------*\
  This function print out the status of the running tests.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }
    fprintf(TERM_TX, RSP_DATA ", %lu, ADCs Run %s function.\n", SM_UXTIME_P, dac_fct_str[dac_function]);
    fprintf(TERM_TX, RSP_DATA ", %lu, DACs config: %lu steps of %lu sec at %lu Hz clock.\n",
            SM_UXTIME_P, dac_nbStep, dac_step_uration, dac_clk);

    refresh_screen_f = 1;
    while (!dev.abort_f)
    {
        if (refresh_screen_f>0) //
        {
            fprintf(TERM_TX, RSP_DATA
                    ", State [#faults]:\t%s [%d]\t%s [%d]\t%s [%d]\t%s [%d]\t%s [%d]\t%s [%d]\t| %ld mV\n",
                    status_str[dut_state[0]], dut_faults_cnt[0], status_str[dut_state[1]], dut_faults_cnt[1],
                    status_str[dut_state[2]], dut_faults_cnt[2], status_str[dut_state[3]], dut_faults_cnt[3],
                    status_str[dut_state[4]], dut_faults_cnt[4], status_str[dut_state[5]], dut_faults_cnt[5], dac_mV);
            refresh_screen_f--;
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void h4irradADCUpdateStepFct(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function update the step function for ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Compute new DAC value
    dac_value = -(dac_step_idx * DAC_STEP + DAC_MIN);
    dac_step_idx = (dac_step_idx + 1) % (NB_STEPS + 1);
    dac_mV = -dac_value / 43.69;

    // Write ADC
    dac_value <<= 12; // Value in DAC is left justified (bits 31-12)
    ANA_DAC_HIGH_P = ((uint16_t*) &dac_value)[0];
    ANA_DAC_LOW_P = ((uint16_t*) &dac_value)[1];
}



/*---------------------------------------------------------------------------------------------------------*/
void MenuRadiationH4IRRADAdcFunction(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
      This function read the DAC configuration.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (MenuGetInt16U(argv[0], 0, 1, &dac_function))
    {
        MenuRspError("Error parsing arg");
        return;
    }
    if (dac_function == 1)
    {
        if (MenuGetInt32S(argv[1], -12000, +12000, &dac_mV))
        {
            MenuRspError("Error parsing arg");
            return;
        }
    }
}


/*---------------------------------------------------------------------------------------------------------*/
void MenuRadiationH4IRRADDacSetClk(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function set the clock CLKX frequency in Hz.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }
    if (!dsp.is_running)                        // If C32 is not running
    {
        MenuRspError("C32 not running");        // Report error
        return;
    }

    if (MenuGetInt32U(argv[0], 62, 8000000, &dac_clk))
    {
        MenuRspError("Error parsing arg");
        return;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void MenuRadiationH4IRRADDacSetNbStep(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function set the number of step for step function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }
    if (!dsp.is_running)                        // If C32 is not running
    {
        MenuRspError("C32 not running");        // Report error
        return;
    }

    if (MenuGetInt32U(argv[0], 2, 0xFFFF, &dac_nbStep))
    {
        MenuRspError("Error parsing arg");
        return;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRadiationH4IRRADDacSetStepDuration(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function set the duration of each steps.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }
    if (!dsp.is_running)                        // If C32 is not running
    {
        MenuRspError("C32 not running");        // Report error
        return;
    }

    if (MenuGetInt32U(argv[0], 1, 0xFFFF, &dac_step_uration))
    {
        MenuRspError("Error parsing arg");
        return;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRadiationH4IRRADDacUpdateConfig(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function set the duration of each steps.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
        return;
    }
    if (!dsp.is_running)                        // If C32 is not running
    {
        MenuRspError("C32 not running");        // Report error
        return;
    }

    DPRAM_BT_FIFOCTRL_P = FIFO_PORT_CTRL;
    MSLEEP(1);

    DPRAM_BT_SERIALDATA_A [0] = dac_clk;
    DPRAM_BT_SERIALDATA_A [1] = dac_nbStep;
    DPRAM_BT_SERIALDATA_A [2] = dac_step_uration;

    DPRAM_BT_FIFOCTRL_P = H4IRRAD_DAC_TEST;
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: h4irrad.c
\*---------------------------------------------------------------------------------------------------------*/
