/*---------------------------------------------------------------------------------------------------------*\
  File:     isr.c

  Purpose:  FGC HC16 Software - Interrupt service routines

  Author:   Quentin.King@cern.ch

  Notes:    This file contains all the interrupt service routine functions for the millisecond and
        FIP interrupts.  Both are coming from the GPT.  The FGC2 library has a bus error
        function and a trap function.

  Test Points:  Normally TP1 is used for IsrFip() and TP2 for IsrMst().  If you disable this using
        the constant below (set to 0) then you can use the test points elsewhere in the code
        using:
                TP_SET(1); or TP_SET(2);
                TP_RST(1); or TP_RST(2);
                TP_TGL(1); or TP_TGL(2);
\*---------------------------------------------------------------------------------------------------------*/

#include <mcu_dependent.h>      // for REG_LEDS ...
#include <main.h>               // for timeout_ms global variable
#include <fieldbus_boot.h>      // for fieldbus, stat_var global variable
#include <memmap_mcu.h>
#include <dev.h>                // for dev global variable
#include <pll_boot.h>           // for pll global variable
#include <c32.h>                // for dsp global variable
#include <defconst.h>           // for FGC_FLT_, FGC_LAT_
#include <codes.h>              // for code global variable
#include <menuRfc.h>            // for RfcIsrMst()
#include <term_boot.h>           // for TermProcessHardware()
#include <analog.h>             // for AnaIsrMst(), AnaLink()
#include <diag_boot.h>          // for QSPIbusStateMachine()
#include <fip_boot.h>           // for FipReadTimeVar(), FipWriteStatVar()
#include <debug.h>
#ifdef H4IRRAD
    #include <h4irrad.h>        // for h4irradSupplyControl()
#endif

#define TP1_ENABLE  1           // 1: TP1 linked to IsrFip    0: TP1 available
#define TP2_ENABLE  1           // 1: TP2 linked to IsrMst    0: TP2 available

#define LEDS_UPDATING_CODE      RGLEDS_DCCT_RED_MASK16

/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT
/*---------------------------------------------------------------------------------------------------------*/
void IsrTick(void)
/*---------------------------------------------------------------------------------------------------------*\
  This ISR will run every millisecond in response to in IRQ from the GPT OC2.  It is responsible for
  providing a number of services to the boot's background process including:

   - Trigger of the slow watch dog if required
   - set the period between ms interrupts according to PLL period
   - flash the VS green LED to indicate activity
   - maintenance of the time since reset and time now (updated by the FIP)
   - read out of the analogue interface
   - operation of the QSPI bus
   - check on the presence of the FIP Gateway

  The function will set TP2 at the beginning and reset it at the end.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        PSHM    D,E,X,Y,Z,K         // Save all registers

        /* Set page registers back to default data page */

        TSKB                    // Transfer SK to B
        TBXK                    // Transfer B to XK
        TBYK                    // Transfer B to YK
        TBEK                    // Transfer B to EK

        LDX #0x0000             // Clear X to access HC16 register in top of page F
#if TP2_ENABLE == 1
        BSET    SIM_PORTC_16,X,#0x02        // Set TP2
#endif

        /* Clear IRQ and trigger watchdogs as required */

        BCLR    GPT_TFLG1_16,X,#GPT_TFLG1_OC2F_MASK8 // Clear GPT OC2 IRQ

        TSTW    dev.enable_slow_wd      // If triggering of slow watchdog is not enabled
        BEQ     fastwd              // Skip forward


        CLRW    CPU_TIMER_16            // Clear timer register to trigger slow watchdog
//      CLRW    OS_2US_CLK                      // Clear timer register to trigger slow watchdog

    fastwd:
        TSTW    dev.enable_fast_wd      // If triggering of fast watchdog is not enabled
        BEQ     setms               // Skip forward
        LDAA    GPT_PDR_16,X
        EORA    #GPT_PDR_TRIGWDMS_MASK8     // Toggle ms watchdog trigger signal
        STAA    GPT_PDR_16,X

        /* Rearm OC2/OC3 using PLL period to generate the next IRQ/analogue trigger in 1 ms */

    setms:
        LDE     GPT_TOC2_16,X           // Get GPT_TOC2 value...
        STE     pll.ms_t0           // ... and save for use by PLL algorithm in IsrFip()
        LDD     pll.qtics_per_ms:2      // Load D = lsw(qtics_per_ms)
        ADDD    pll.frac_counter        // Add D ...
        STD     pll.frac_counter        // ... to frac counter.  Carry is set on overflow.
        ADCE    pll.qtics_per_ms:0      // E = Carry_bit + msw(qtics_per_ms) + GPT_TOC2
        STE     GPT_TOC2_16,X           // Store E in GPT_TOC2 to trigger next ms interrupt
        STE     GPT_TOC3_16,X           // Store E in GPT_TOC3 to trigger next analogue acquisition

        /* Maintain ms time and on ms 500 clear LEDs and check C32 background loop */

        INCW    SM_MSTIME_16            // SM_MSTIME++
        LDD     SM_MSTIME_16
        CPD     #500                // If(SM_MSTIME != 500)
        BNE     checkendsec         //    Skip to check for end of second

        BRSET   dev.reg_r,#REG_LEDS,timeout // Skip forward if LEDS register isn't readable
        BRSET   dev.reg_w,#REG_LEDS,timeout // Skip forward if LEDS register isn't writable
        BCLRW   RGLEDS_16,#0x0FCC       // Clear all LEDs except WORLDFIP and PSU

        BRCLR   dsp.is_running, #1, timeout     // Skip forward if C32 isn't running

        BRSET   DPRAM_BT_C32STATUS_16,#C32_ALIVE,c32bgok // Skip forward if C32 bg has responded
        LDD     #C32_STATUS_BG_FAILED       // Set C32 status to BG FAILED
        STD     DPRAM_BT_C32STATUS_16+2
        BRA     timeout

    c32bgok:
        CLR     DPRAM_BT_C32STATUS_16       // Clear C32 ALIVE bit in the status long word
        BRA     timeout

    /* Maintain seconds time and set LEDs */

    checkendsec:
        CPD     #1000
        BLT     timeout             // if(SM_MSTIME == 1000)
                        // {
        CLRW    SM_MSTIME_16            //  SM_MSTIME = 0
        INCW    SM_UXTIME_16 + 2        //  SM_UXTIME++
        BNE     incpwrtime
        INCW    SM_UXTIME_16 + 0

    incpwrtime:
        INCW    SM_PWRTIME_16 + 2       //  SM_PWRTIME++
        BNE     incruntime
        INCW    SM_PWRTIME_16 + 0

    incruntime:
        INCW    SM_RUNTIME_16 + 2       //  SM_RUNTIME++
        BNE     flashleds
        INCW    SM_RUNTIME_16 + 0

    flashleds:
        BRSET   dev.reg_r,#REG_LEDS,timeout //  Skip forward if LEDS register isn't readable
        LDD     dev.state           //  Flash leds:
        ORD     RGLEDS_16

        TSTW    stat_var.fgc_stat.class_data.c50.st_warnings
        BEQ     nowarning
        ORD     #RGLEDS_FGC_RED_MASK16      //  FGC HW Warning: Set RED FGC LED

    nowarning:
        LDE     #FGC_FLT_FGC_HW
        ANDE    stat_var.fgc_stat.class_data.c50.st_faults
        BEQ     nofault
        ORD     #RGLEDS_FGC_RED_MASK16      //  FGC HW Fault: Set RED FGC LED
        BRA     rxcode

    nofault:
        ORD     #RGLEDS_FGC_GREEN_MASK16    //  No FGC HW Fault: Set GREEN FGC LED

    rxcode:
        TSTW    code.rx_active_f        //  Receiving code
        BEQ     setleds
        ORD     #LEDS_UPDATING_CODE

    setleds:
        BRSET   dev.reg_w,#REG_LEDS,timeout //  Skip forward if LEDS register isn't writable
        STD     RGLEDS_16           // }

        /* Run millisecond timeout register */

    timeout:
        TSTW    timeout_ms          // Check if millisecond timeout
        BEQ     chkmem
        DECW    timeout_ms          // Decrement timeout if not yet zero

        /* Check for single bit memory errors */

    chkmem:
        LDAA    GPT_PACNT_16,X          // Get counter
        TAB                 // Copy counter to B
        SUBB    dev.last_gpt_pacnt      // Subtrat previous value
        BNE     sbedetected         // Jump if changed
        BRSET   GPT_TFLG2_16,X,#GPT_TFLG2_PAOVF_MASK8,sbeoverflow // Jump if a sbe counter overflow occured
        BRA     rstchkmem           // No SBE detected

    sbedetected:
        STAA    dev.last_gpt_pacnt      // Remember new counter value
        CLRA                    // D = A:B = 0:{difference in counter}
        ADDD    dev.mem_sbe_cnt:2       // D = low word of accumulator
        STD     dev.mem_sbe_cnt:2       // D = low word of accumulator
        BCC     sbeoverflow         // Exit if no overflow of low word
        INCW    dev.mem_sbe_cnt         // Carry set so increment high word

    sbeoverflow:
        BCLR    GPT_TFLG2_16,X,#GPT_TFLG2_PAOVF_MASK8   // Clear PCNT overflow flag
        LDAA    #1
        STAA    dev.mem_sbe_f           // Set SBE flag

    rstchkmem:
        BRSET   dev.reg_r,#REG_LEDS,periph  // Skip forward if LEDS register isn't readable
        LDD     #RGLEDS_TEST_MASK16
        ANDD    RGLEDS_16           // if(TEST LEDS button pressed)
        BEQ     periph
        STD     dev.abort_f         //    Set abort flag

    /* Process peripherals: SCI, ADCs, QSPI, Link from F28 */

    periph:
#ifdef DEBUG_NO_ADC_RESET
        LDD     #0x0001
        STD     SM_ISRMASK_16
#endif
        JSR RfcIsrMst           // Run RFC interface if present and triggered
#ifdef DEBUG_NO_ADC_RESET
        LDD     #0x0002
        STD     SM_ISRMASK_16
#endif
        JSR TermProcessHardware      // Process SCI characters (XK:IX must be zero)
#ifdef DEBUG_NO_ADC_RESET
        LDD     #0x0003
        STD     SM_ISRMASK_16
#endif
        JSR AnaIsrMst           // Process ADC values
#ifdef DEBUG_NO_ADC_RESET
        LDD     #0x0004
        STD     SM_ISRMASK_16
#endif
        JSR QSPIbusStateMachine     // Acquire diag data
#ifdef DEBUG_NO_ADC_RESET
        LDD     #0x0005
        STD     SM_ISRMASK_16
#endif
        JSR AnaLink             // Get SimPConv link data when enabled
#ifdef DEBUG_NO_ADC_RESET
        LDD     #0x0006
        STD     SM_ISRMASK_16
#endif

        /* Send IRQ to C32 if running - must be after AnaLink */

        BRCLR   dsp.is_running, #1, end     // Skip forward if C32 isn't running
        LDD     #CPU_MODEH_C32IRQ_MASK8     // D = CPU_MODEH_C32IRQ_MASK8
        ANDD    dsp.irq_mask            // D = irq_mask & CPU_MODEH_C32IRQ_MASK8
        BNE     sendirq             // If not zero then skip to send request
        LDD     #CPU_MODEH_C32IRQ_0_MASK8   // Initialise irq mask to IRQ0
        STD     dsp.irq_mask            // irq_mask = IRQ0
    sendirq:
        ORAB    CPU_MODEH_16            // Merge with mode register value
        STAB    CPU_MODEH_16            // Write back to mode register to trigger IRQ

        /* Check for FIP GW timeout */
                            // This is used here as a delay between the C32 IRQ
        JSR GatewayAliveAndPllAlgoBlocking          // and the check of the C32 ISR

        /* Check C32 IRQ response if C32 is running */

    c32chk:
        BRCLR   dsp.is_running, #1, end     // Skip forward if C32 isn't running
        LDD     dsp.irq_mask            // Get irq_mask
        COMD                    // D = ~irq_mask
        ANDD    dsp.irq_fail            // D = irq_fail & ~irq_mask
        STD     dsp.irq_fail            // irq_fail = D
        LDAB    CPU_MODEH_16            // B = CPU_MODEH (IRQ bits are from 0-3)
        ANDD    dsp.irq_mask            // D = irq_mask & CPU_MODE
        ORD     dsp.irq_fail            // D = irq_fail | (irq_mask & CPU_MODE)
        STD     dsp.irq_fail            // irq_fail = D
        LDD     DPRAM_BT_C32STATUS_16+2     // D = C32 status
        ORD     dsp.irq_fail            // D = C32 status | irq_fail
        STD     dsp.status          // dsp.status = D
        LDD     dsp.irq_mask            // D = irq_mask
        ASLD                    // D <<= 1
        STD     dsp.irq_mask            // irq_mask = D

    end:
        LDX     #0x0000             // Clear X to access HC16 register in top of page F
        BCLR    SIM_PORTC_16,X,#0x02        // Reset TP2

#ifdef DEBUG_NO_ADC_RESET
        LDD     #0x0000
        STD     SM_ISRMASK_16
#endif
#ifdef H4IRRAD
        JSR     h4irradSupplyControl            // H4IRRAD control of power supply  of DUTs
#endif
        PULM    D,E,X,Y,Z,K         // Restore all registers
        RTI                 // Return from interrupt
    }
}
/*---------------------------------------------------------------------------------------------------------*/
#pragma NO_FRAME
#pragma NO_EXIT
/*---------------------------------------------------------------------------------------------------------*/
void IsrFip(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called in response to the GPT IC1 interrupt.  This is connected to the MicroFIP
  interface IRQ.  Only the Var 7 reception has an interrupt enabled, so there can be no other reason for
  the interrupt.  Var 7 is used twice per WorldFIP cycle - first for the time variable (0x3000) and then
  for the code variable (0x3004).  When the time variable is received it first reads the time variable
  and then writes the status variable. It then runs the phase locked loop to maintain the phase between
  the millisecond interupts and the FIP interrupts.  If code reception from the FIP is enabled, the
  FipReadTimeVar function will set the Var 7 global variable identifier to the 0x3004 to receive the
  code variable - which will return the identifier to 0x3000 for the time variable on the next cycle.
  The function sets TP1 high at the start and low at the end.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        PSHM    D,E,X,Y,Z,K         // Save all registers
        TSKB                    // Transfer SK to B
        TBEK                    // Transfer B to EK
        TBXK                    // Transfer B to XK
        TBYK                    // Transfer B to YK

        LDX     #0x0000             // Set X to access HC16 registers in page F
#if TP1_ENABLE == 1
        BSET    SIM_PORTC_16,X,#0x01        // Set TP1
#endif

        LDAA    FIP_IRQSA_16            // Read FIP IRQ register to clear it

        BCLR    GPT_TFLG1_16,X,#GPT_TFLG1_IC1F_MASK8 // Clear IC1 interrupt in GPT_TFLG register

        TST     FIP_VIDGL_16            // Test FIP variable address (low byte) (0=Time 4=Code)
        BNE     codevar

        /*  Time Variable received */

        LDE     GPT_TIC1_16,X           // Read GPT TIC1 register (time of Fip interrupt)

        TSTW    fieldbus.is_gateway_online  // If GW not present (2 consecutive packets required)
        BEQ     rwfip               // Skip PLL

        JSR     PllAlgoWithFipSynchro       // Run PLL (E=Fip IRQ time)

    rwfip:
        JSR     FipReadTimeVar          // Read time var
        JSR     FipWriteStatVar         // Write status variable
        BRA     end

        /* Code Variable received */

    codevar:
        JSR     FipReadCodeVar          // Read code var

    end:
#if TP1_ENABLE == 1
        LDX     #0x0000             // Prepare IX to access GPT registers in page F
        BCLR    SIM_PORTC_16,X,#0x01        // Reset TP1
#endif
        PULM    D,E,X,Y,Z,K         // Recover all registers
        RTI
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: isr.c
\*---------------------------------------------------------------------------------------------------------*/
