/*---------------------------------------------------------------------------------------------------------*\
  File:     main.c

  Purpose:  FGC2 HC16 Boot Software - main functions
\*---------------------------------------------------------------------------------------------------------*/

#define FGC_GLOBALS                     // to instantiate fgc_code_sizes_blks[], fgc_code_names[] in fgc_codes.h
#define GLOBAL_VARS                     // to instantiate fgc_rl_length[], fgc_rl_label[], global variables in fgc_runlog_entries.h
#define MENUTREE                        // to instantiate global variables in menutree.h
#define DEFSYMS                         // to instantiate global variables in defsyms.h

#define BOOT_DUAL_PORT_RAM_GLOBALS      // to instantiate dpram
#define CRATE_BOOT_GLOBALS              // to instantiate crate
#define MCU_TIME_CONSUMED_GLOBALS       // to instantiate mcu_time_consumed_in
#define PLL_BOOT_GLOBALS                // to instantiate pll
#define DEV_GLOBALS                     // to instantiate dev
#define MAIN_GLOBALS                    // to instantiate timeout_ms, main_misc

#include <main.h>
#include <stdbool.h>
#include <version.h>                    // Include version struct from version_def.txt
#include <memmap_mcu.h>                 // for SM_UXTIME_P
#include <stdio.h>                      // for fprintf(), fputs(), NULL
#include <boot_dual_port_ram.h>         // for dpram global variable that is instantiated here
#include <term.h>
#include <mcu_dependent.h>              // for MSLEEP(), NVRAM_LOCK(), LED_SET(), LED_RST(), BOOT_STATE_SELF_TEST, REG_LEDS ...
#include <definfo.h>
#include <codes_holder.h>
#include <dev.h>
#include <codes.h>                      // for global var code, CodesInit()
#include <fgc_codes_gen.h>              // Global FGC constants
#include <macros.h>                     // for Test(), Set(), Clr()
#include <defconst.h>                   // for FGC_INTERFACE_
#include <crate_boot.h>                 // for crate, crate_types[]
#include <diag_boot.h>                  // for diag_bus
#include <mcu_time_consumed.h>          // for mcu_time_consumed_in global variable
#include <fgc_runlog_entries.h>         // Include run log definition, for FGC_RL_
#include <runlog_lib.h>                 // for struct TRunlog
#include <menu_node.h>
#include <menutree.h>                   // for dpram_test, dsp_prog
#include <term_boot.h>
#include <analog.h>                     // for slot5_types[]
#include <fip_boot.h>                   // for FipInit()
#include <pll_boot.h>                   // for PllInit(), TICK_PERIOD, pll global variable
#include <initterm.h>                   // for InitTerm()
#include <fieldbus_boot.h>              // for fieldbus, stat_var global variable
#include <mem.h>                        // for MemSetBytes()
#include <registers.h>                  // for RegTest(), RegWrite(), RegRead()
#include <runlog_lib.h>                 // for RunlogInit(), RunlogWrite()
#include <menu.h>                       // for menu global variable, MenuRspError(), MenuRspArg()
#include <os.h>                         // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <label.h>                      // for struct sym_name needed by defsyms.h
#include <defsyms.h>                    // for sym_names_interface[]
#include <analog.h>                     // for slot5_types[]
#include <debug.h>
#include <shared_memory.h>              // for FGC_MAINPROG_RUNNING, FGC_STAY_IN_BOOT



static void InitFromNameDb(void)
{
    // Extract name and class from NameDB if fieldbus address is available

    if(DevIdValid())
    {
        struct fgc_dev_name * FAR dev_name_p;

        // Make BB flash visible in 4-7

        SELFLASH(SEL_BB);
        dev_name_p = (struct fgc_dev_name * FAR) codes_holder_info[SHORT_LIST_CODE_NAMEDB].base_addr;

        gw_name  = dev_name_p[0];
        gw_name.name[FGC_MAX_DEV_LEN] = 0;      // for the case when the flash is empty

        dev_name = dev_name_p[dev.fieldbus_id];
        dev_name.name[FGC_MAX_DEV_LEN] = 0;     // for the case when the flash is empty
    }
}


/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*\
  This is the main() function for the FGC2 boot program.  It is called from the _Startup() function
  after the C context has been set up.
\*---------------------------------------------------------------------------------------------------------*/
{
//  uint16_t  rl        = FGC_BOOT_RUNNING;

    RunlogInit();                   // Initialise run log
    RunlogWrite(FGC_RL_BOOT_START, NULL);       // Log boot running

//----------------------------------
#ifdef OPT_RADTEST
    #ifdef DEBUG_NO_ADC_RESET
        RunlogWrite(FGC_RL_TSK_ID, (void*)SM_WATCH_CTRL_16);
        // this is used by the RadTest as a Magic Number
        SM_WATCH_CTRL_P = 0xA5A5;
    #endif
#endif
//----------------------------------


    CheckRegisters();                   // Check peripheral registers for bus errors
    DetectCauseOfStartup();             // cause of startup (PWR, SLOW, FAST)
    InitShareMem();                     // Initialise shared memory

    *menu.error_buf = '\0';             // Clear error buffer

    EnableTerminalStream();     // Start SCI interfaces
    EnableQSPIbus();            // Start QSPI interfaces
    PllInit();
    FieldbusInit();             // Start Fip interface
    InitGpt();                  // Start GPT and interrupts
    CheckSlot5Registers();      // Check Slot 5 registers after a delay on power up
    StartBoot();                // Start boot according to reset
    ReportBoot();               // Report information about boot
    SelfTest();                 // Run self-tests
    InitFromNameDb();
    CodesInit();                // Update codes if required
    ReportFip();                // Report information about boot
    RunMenus();                 // Run menus if required
    RunMainProg();              // Run main program
}
/*---------------------------------------------------------------------------------------------------------*/
void RunMenus(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by main() to start the menu system if required.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  i;
    uint16_t  t;
    uint16_t  test_leds;
    char    ch = 0;
//  uint8_t   node_id[32] = "";

    /*--- Check for direct mode operation ---*/

    if(TermRxFlush())                // If CTRL-Z was pressed
    {
        term.edit_state = 0;                // Start menus in direct mode
        dev.run_menu_f = 1;
    }

    fprintf(TERM_TX,TERM_CLR_LINE "$S,%u\n",term.edit_state);   // Report edit_state

    /*--- Check with user if the menus should be started ---*/

    CheckStatus();

    if(!dev.run_menu_f &&
       !dev.abort_f &&
       !(stat_var.fgc_stat.class_data.c50.st_faults & FGC_FLT_FGC_HW)   &&
       !(stat_var.fgc_stat.class_data.c50.st_warnings & FGC_WRN_FGC_HW))
    {
        dev.state = BOOT_STATE_WAIT_USER;

        fputs("\nPress ESC to stay in boot [ ] ",TERM_TX);

        for(i=0,t=5;i <= 50;i++)
        {
            if(!(i % 10))
            {
                fprintf(TERM_TX,"\b\b\b%u] ",t--);
            }

            ch = TermGetCh(100);         // Wait up to 100ms for a character

            if(ch == 0x1B)          // If ESC received
            {
                dev.run_menu_f = 1;         // Run menus interfactively
                fputs(TERM_CLR_LINE,TERM_TX);
                break;
            }

            test_leds = RGLEDS_P;       // Read the LEDS reg to get the state of TEST LEDS button

            if(ch == 0x1A ||            // If CTRL-Z received or
               dev.abort_f)         // CTRL-C or TEST LEDS pressed
            {
                term.edit_state = 0;            // Run menus in direct mode
                dev.run_menu_f = 1;
                break;
            }

            if(ch == ' ')           // Space  - Run main program
            {
                break;
            }
        }

        if(!dev.run_menu_f)         // If menus not required
        {
            fputs(TERM_CLR_LINE "\r",TERM_TX);
            return;
        }
    }

    /*--- Run Boot menu system ---*/

    dev.state = BOOT_STATE_RUN_MENU;        // Set boot state

    for(;;)                 // Run the menus indefinitely
    {
        term.recv_cmd_f = 0;                // Reset receiving command flag
        MenuMenu(&root_menu);               // Run menus
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CheckRegisters(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by main() to check access to the peripheral registers except slot 5 which needs
  a delay in case it's an RFC interface.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  data;

    /*--- LEDS ---*/

    if(RegRead(RGLEDS_16,&dev.leds_read, true))
    {
        dev.reg_r |= REG_LEDS;
    }

    if(RegWrite(RGLEDS_16,0x0000, true))
    {
        dev.reg_w |= REG_LEDS;
    }

    /*--- C62 ---*/

    if(RegRead(C62_16,&data, true))
    {
        dev.reg_r |= REG_C62;
    }

    if(RegWrite(C62_16,0x0000, true))
    {
        dev.reg_w |= REG_C62;
    }

    /*--- FIP ---*/

    if(RegRead(FIP_COBMPS_32,&data, true))
    {
        dev.reg_r |= REG_FIP;
    }

    if(RegWrite(FIP_COBMPS_32,0x0000, true))
    {
        dev.reg_w |= REG_FIP;
    }

    if(!((dev.reg_w | dev.reg_r) & REG_FIP))        // If FIP access is okay
    {
        (CPU_RESET_P &= ~CPU_RESET_CTRL_NETWORK_MASK16);                    // Remove hardware reset of uFIP
        FIP_RESET_P = 0;                    // Reset uFIP (takes 1us)
        data        = fip.stadr/10;             // Delay to allow uFIP reset
        fip.stadr   = FIP_STADR_P;              // Initial reading of FIP address
        CPU_RESET_P |= CPU_RESET_CTRL_NETWORK_MASK16;                   // Set hardware reset of uFIP
    }

    /*--- DIG ---*/

    crate.type = 0xFFFF;                // Default to unknown bus type

    if(RegRead(DIG_OP_32, &data, true) ||
       RegRead(DIG_IP1_32,&data, true) ||
       RegRead(DIG_IP2_32,&crate.type, true))
    {
        dev.reg_r |= REG_DIG;
    }

    if(RegWrite(DIG_OP_32,0x0000, true))
    {
        dev.reg_w |= REG_DIG;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CheckSlot5Registers(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by main() to check access to the peripheral registers for slot 5 which needs
  a delay in case it's an RFC interface.  The SD interface has three different programmable devices, all
  of which need checking.  The RFC and SAR have only one device.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  data;
    uint16_t  idx;

    /* Process Slot 5 interface information to get interface type and PLD version */

    MSLEEP(1000);      // Wait 1s for FPGA on Slot5 card (if present) to re-program after reset

    if(RegRead(SLOT5_16,&dev.slot5, true))  // If Slot5 control register is not visible
    {
        dev.reg_r |= REG_SLOT5;             // Mark device as totally unreachable
        dev.reg_w |= REG_SLOT5;
        dev.pld_version = 999;              // Set PLD version as unknown
    }
    else                    // else
    {
        SM_CODES_A[0]   =               // Code 0 version is set to PLD version
        dev.pld_version = (dev.slot5 & SLOT5_PLDVERS_MASK16) >> SLOT5_PLDVERS_SHIFT;
        SM_INTERFACETYPE_P  =               // Get Slot5 card type
        dev.slot5_type  = (dev.slot5 & SLOT5_TYPE_MASK16)    >> SLOT5_TYPE_SHIFT;

        if(RegWrite(SLOT5_16,0x0000, true))         // Check write access to control register
        {
            dev.reg_w |= REG_SLOT5;
        }

        if(dev.slot5_type == FGC_INTERFACE_SD_350 ||        // If SD interface is found
           dev.slot5_type == FGC_INTERFACE_SD_351)
        {
            if(RegRead(ANA_SD_NL_A_R_INDEX_16,&data, true) ||   // Check either filter is not readable
               RegRead(ANA_SD_NL_B_R_INDEX_16,&data, true))
            {
                dev.reg_r |= REG_SLOT5;
            }

            if(RegWrite(ANA_SD_NL_A_W_INDEX_16,0x0000, true) || // Check either filter is not writeable
               RegWrite(ANA_SD_NL_B_W_INDEX_16,0x0000, true))
            {
                dev.reg_w |= REG_SLOT5;
            }

            if(!((dev.reg_r | dev.reg_w) & REG_SLOT5))      // If SD interface is working
            {
                AnaFltFrq(0,0);                     // Init SD index register
            }
        }
    }

    // Find main class IDs associated with the slot 5 interface type

    if(!((dev.reg_r | dev.reg_w) & REG_SLOT5))
    {
        for(idx = 0; slot5_types[idx].label && dev.slot5_type != slot5_types[idx].type;idx++);

        if(!slot5_types[idx].label)
        {
            idx = 0;
        }

        dev.slot5_type_index = idx;
    }

    dev.class_id = slot5_class_ids[dev.slot5_type_index];
}
/*---------------------------------------------------------------------------------------------------------*/
void DetectCauseOfStartup(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by main() to determine the cause of startup (PWR, SLOW or FAST).
  The cause is returned via the SM BOOTTYPE field and is recorded in the run log.
\*---------------------------------------------------------------------------------------------------------*/
{
    CPU_RESET_P |= CPU_RESET_CTRL_C62OFF_MASK16;    // Turn off M16C62

    // Check reset type

    RunlogWrite(FGC_RL_RST_REG, (void *) CPU_RESET_16);
    RunlogWrite(FGC_RL_MODE_REG, (void *) CPU_MODE_16);

    if ( CPU_RESET_P & CPU_RESET_SRC_POWER_MASK16 ) // if power cycle reset
    {
        SM_BOOTTYPE_P = BOOT_PWR;           // Power boot
    }
    else
    {
        SM_BOOTTYPE_P = BOOT_NORMAL;
    }

    RunlogWrite(FGC_RL_BOOT_TYPE,(void*)SM_BOOTTYPE_16);// Record boot type in run log

    /*--- Check boot device: SR, BA, BB ---*/

    if(TST_CPU_MODEL(SRAM))
    {
        dev.device = 'R';                   // Boot device is SRAM
        code.running_dev_idx = 0xFFFF;              // Indicate no real device
    }
    else
    {
        dev.device       = (TST_CPU_MODEL(TSTBOOT) ? 'B' : 'A');
        code.running_dev_idx = (dev.device == 'A' ? SHORT_LIST_CODE_BTA : SHORT_LIST_CODE_BTB);
    }

    if(dev.device == 'R'          ||        // If running boot out of SRAM, or
       TST_CPU_RESET(SRC_HC16RAM) ||        // reset was due to HC16 memory error, or
       TST_CPU_RESET(SRC_C32RAM)  ||        // reset was due to C32 memory error, or
       SM_MPRUN_P == FGC_STAY_IN_BOOT)      // the main program has signalled to stay in boot
    {
        dev.run_menu_f = 1;             // Signal to stay in boot menus
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void InitShareMem(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will prepare the SM block in page 1.  If starting from a power up, the whole SM block is
  cleared, otherwise, the unix time is recorded if not zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(SM_BOOTTYPE_P == BOOT_PWR)           // If Power reset
    {
//----------------------------------
#ifdef OPT_RADTEST
   #ifndef DEBUG_NO_ADC_RESET
        MemSetBytes((void*)SM_16,0,SM_B);           // Clear complete SM area
        MemSetBytes((void*)TRAP_16,0,TRAP_B);       // Clear complete TRAP area
   #endif
#else
        MemSetBytes((void*)SM_16,0,SM_B);           // Clear complete SM area
        MemSetBytes((void*)TRAP_16,0,TRAP_B);       // Clear complete TRAP area
#endif
//----------------------------------
    }
    else
    {
        RunlogWrite(FGC_RL_PWRTIME,(void*)SM_PWRTIME_16);   // Record power up time

        if(SM_UXTIME_P)                             // If unix time isn't zero
        {
            RunlogWrite(FGC_RL_UXTIME,(void*)SM_UXTIME_16); // Record unix time in run log
            RunlogWrite(FGC_RL_MSTIME,(void*)SM_MSTIME_16);

            SM_STARTTIME_P = SM_UXTIME_P;
            SM_UXTIME_P = 0;                        // Reset time
            SM_MSTIME_P = 0;
        }

        SM_RUNTIME_P = 0;               // Reset run time
        SM_MPRUN_P = 0;                 // Reset MP run flag registers
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void InitGpt(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set up the GPT so that interrupts will be generated every millisecond, and on
  receipt of traffic from the WorldFIP.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  word;

    dev.enable_slow_wd  = 1;            // Enable slow watchdog triggers
    pll.qtics_per_ms     = TICK_PERIOD << 16;   // Initialise PLL period
    pll.int_qtics_per_ms = pll.qtics_per_ms;    // Initialise PLL integrator

    GPT_MCR_P    = 0x0001;          // Set the interrupt arbitration level to 1
    GPT_ICR_P    = 0x0630;          // GPT interrupt is priority 6, base vector is 0x30
    GPT_PDR_P    = 0x00;            // Clear all Port GP outputs
    GPT_PDDR_P   = 0xF0;            // PGP0-3 inputs, PGP4-7 outputs
    GPT_TCTL_P   = 0x0502;          // IC1 will capture on falling edge, OC2/3 will toggle
    GPT_TMSK_P   = 0x0001;          // Set clock prescalar to divide by 8 (2MHz)
    word     = GPT_TFLG_P;          // Clear all ... (must be read as 16 bits)
    GPT_TFLG_P   = 0;               // ... outstanding GPT interrupts
    GPT_TOC2_P   = GPT_TCNT_P + TICK_PERIOD;    // Prepare for first tick in ~1 ms
    GPT_TMSK_P  |= GPT_TMSK_OC2I_MASK16;    // Enable GPT OC2 (ms tick) interrupts
    GPT_TMSK_P  |= GPT_TMSK_IC1I_MASK16;    // Enable GPT IC1 (uFIP IRQ) interrupts
    GPT_PACLT_P  = 0x50;            // Pulse Accumulator enabled for rising edges (single bit err)
    GPT_TFLG2_P &= ~GPT_TFLG2_PAOVF_MASK8;  // Clear overflow flag

    OS_ENABLE_INTS();               // Allow all interrupt levels
}
/*---------------------------------------------------------------------------------------------------------*/
void StartBoot(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the boot starts to prepare for operation
\*---------------------------------------------------------------------------------------------------------*/
{
    stat_var.fgc_stat.class_data.c50.state_op = FGC_OP_BOOT;            // Initialise operational state
    Set(stat_var.fgc_stat.class_data.c50.st_faults,FGC_FLT_FGC_STATE);          // Assert a fault because converter cannot be controlled

    /* Process digital interface data to get backplane type and side information */

    if(!(dev.reg_r & REG_DIG))          // If digital interface is visible
    {
        crate.side  = (crate.type & DIG_IP2_BPSIDE_MASK16 ? 'R' : 'L');
        crate.type &= DIG_IP2_BPTYPE_MASK16;
    }
    else
    {
        crate.type = 31;                // Set bus type and side to unknown
        crate.side = '?';
    }

    /* Prepare reset source ID for ReportBoot() - reference reset_source_lbl[] */

    dev.rst_src = (CPU_RESET_P &            (CPU_RESET_SRC_POWER_MASK16 |
        CPU_RESET_SRC_PROGRAM_MASK16|CPU_RESET_SRC_MANUAL_MASK16|
        CPU_RESET_SRC_FASTWD_MASK16 |CPU_RESET_SRC_SLOWWD_MASK16|
        CPU_RESET_SRC_HC16RAM_MASK16|CPU_RESET_SRC_C32RAM_MASK16));
}
/*---------------------------------------------------------------------------------------------------------*/
void SelfTest(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function performs FGC self-tests.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct menu_node **test;

    /*--- Self-test functions - POWER BOOT ---*/
    
    // ************************ IMPORTANT ************************
    // * Since release 1490088451 the C32 DSP fails to start in 
    // * the BOOT. As a result all the C32 related functions must
    // * be removed to make sure the FGC2 switches to the MAIN.
    // ***********************************************************
    
    static struct menu_node *power_boot_tests[] =
    {
        // &start_c32,
        &read_check_hc16_ram,
        &pattern_check_hc16_ram,
        &address_check_hc16_ram,
        // &read_check_c32_ram,
        // &pattern_check_c32_ram,
        // &address_check_c32_ram,
        &store_filter_functions,
#ifdef OPT_RADTEST
        &show_all_entries,
        &run_radiation_test,
#endif
        NULL,
    };

    /*--- Self-test functions - SLOW BOOT ---*/

    static struct menu_node *slow_boot_tests[] =
    {
        // &start_c32,
        &read_check_hc16_ram,
        // &read_check_c32_ram,
        &store_filter_functions,
#ifdef OPT_RADTEST
        &show_all_entries,
        &run_radiation_test,
#endif
        NULL,
    };

    /*--- Run through list of self test functions according to boot type ---*/

    fprintf(TERM_TX,TERM_CLR_LINE "\r$R,BOOT_%c,%lu\n",dev.device,version.unixtime);
    dev.state = BOOT_STATE_SELF_TEST;

    test = (SM_BOOTTYPE_P == BOOT_PWR ?         // Select test function list according to boot type
            power_boot_tests :                  //   - Power boot functions, or
            slow_boot_tests);                   //   - Slow boot functions

    while(*test)                                // For all function in the list
    {
        fprintf(TERM_TX, RSP_ID_NAME "\n", "ST",(char * FAR)(*test)->name); // Report function name

        menu.response_buf_pos = 0;              // Clear response and error buffers
        menu.error_buf[0] = '\0';
        dev.abort_f = 0;                        // Clear abort flag

        (*(test++))->function(0,NULL);          // Run self-test function (no args permitted)

        if(menu.response_buf_pos)               // If rsp buffer is not empty
        {
            fprintf(TERM_TX,RSP_DATA "%s\n",(char * FAR)menu.response_buf); // Write last response buffer
        }

        if(*menu.error_buf)                     // If error reported
        {
            dev.run_menu_f = 1;                 // Stay in boot
            fprintf(TERM_TX,RSP_ERROR ",%s\n",(char * FAR)menu.error_buf); // Write error report
            *menu.error_buf = '\0';                                        // Clear error buffer
        }
        else
        {
            fputs(RSP_OK "\r",TERM_TX);         // Report success
        }
    }

    dev.abort_f = 0;                            // Clear abort flag
}
/*---------------------------------------------------------------------------------------------------------*/
void ReportBoot(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will report the state of the FGC.
\*---------------------------------------------------------------------------------------------------------*/
{
    term.edit_state = 1;        // Enable in interactive mode

    InitTerm(TERM_TX);

    fprintf(TERM_TX, TERM_INIT "PLD VERS: " TERM_BOLD "%-30u" TERM_NORMAL,dev.pld_version);
    fprintf(TERM_TX,"WATCHDOG: " TERM_BOLD "%s" TERM_NORMAL,(TST_CPU_MODEL(WDINHBT) ? "INHIBITED" : "ENABLED"));

    fprintf(TERM_TX,"\nBUILT:    " TERM_BOLD "%-30s" TERM_NORMAL,version.unixtime_a);
    fprintf(TERM_TX,"C32:      " TERM_BOLD "%s" TERM_NORMAL,(TST_CPU_MODEL(DSPSTAL) ? "STANDALONE" : "SLAVE"));

    fprintf(TERM_TX,"\nRESETS:   " TERM_BOLD "%-30u" TERM_NORMAL,RL_RESETS_ALL_P);
    fprintf(TERM_TX,"C62:      " TERM_BOLD "%s" TERM_NORMAL,( (C62_P & C62_STANDALONE_MASK16) ? "STANDALONE" : "SLAVE"));

    fprintf(TERM_TX,"\nSLOT5:    " TERM_BOLD "%2u:%-27s" TERM_NORMAL, dev.class_id,TypeLabel(slot5_types, dev.slot5_type));
    fprintf(TERM_TX,"RESET:    " TERM_BOLD "%s" TERM_NORMAL,TypeLabel(reset_source_lbl,dev.rst_src));

    fprintf(TERM_TX,"\nCRATE:    " TERM_BOLD "%-30s" TERM_NORMAL,TypeLabel(crate_types, crate.type));
    fprintf(TERM_TX,"POSITION: " TERM_BOLD "%c" TERM_NORMAL "\n",crate.side);
}
/*---------------------------------------------------------------------------------------------------------*/
void ReportFip(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will report the FIP related data for the FGC according to the gateway.
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(TERM_TX,"CLASS ID: " TERM_BOLD "%-30u" TERM_NORMAL,dev_name.class_id);
    fprintf(TERM_TX,"DEVICE:   " TERM_BOLD "%s" TERM_NORMAL,(char * FAR)dev_name.name);

    fprintf(TERM_TX,"\nGATEWAY:  " TERM_BOLD "%-30s" TERM_NORMAL,(char * FAR)gw_name.name);
    fprintf(TERM_TX,"FIP ID:   " TERM_BOLD "%u" TERM_NORMAL "\n",dev.fieldbus_id);
}
/*---------------------------------------------------------------------------------------------------------*/
void CheckStatus(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called whenever the boot is waiting for keyboard characters.  It will be called at up
  to 1kHz.  It polls various status variables that are set by the Isr functions and set the appropriate
  faults, warnings and status bits.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  reg_rw = dev.reg_w | dev.reg_r;

    if(reg_rw)              // If any registers are unavailable
    {
        Set(stat_var.fgc_stat.class_data.c50.st_faults,FGC_FLT_FGC_HW);
    }

    if(reg_rw & REG_C62)        // If C62 register is not accessible
    {
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DALLAS_FLT);
    }

    if(reg_rw & REG_DIG)        // If digital interface is not accessible
    {
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_DIG_FLT);
    }

    if(reg_rw & REG_SLOT5)      // If Slot 5 interface is not accessible
    {
        Set(stat_var.fgc_stat.class_data.c50.st_latched,(FGC_LAT_ANA_FLT|FGC_LAT_RFC_FLT));
    }

    if(dev.mem_sbe_f)           // If single bit memory errors are being detected
    {
        dev.mem_sbe_f = 0;          // Reset flag (set by IsrTick())
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_MEM_FLT);   // Set MEM_FLT
    }

    RefreshStatusWithDspState();                // Check C32 status

    if(diag_bus.check_f)        // If diag data/state should be checked
    {
        DiagCheck();                // Check analogue and digital values
    }

    if(reg_rw & REG_FIP)        // If FIP interface is not accessible
    {
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_FBS_FLT);
    }
    else                // else FIP is accessible
    {
        if(!dev.fieldbus_id || !fieldbus.is_gateway_online)     // If FIP not connected or no comms from GW
        {
            fip.init_timeout++;
            if (fip.init_timeout > FIP_INIT_TIMEOUT)
            {
//              PllInit();
                FipInit();              // Try to initialise the FIP connection
                RunlogWrite(FGC_RL_FIP_ID, (void*)&dev.fieldbus_id);        // Store FIP station ID in run log
                MemSetBytes(&stat_var.fieldbus_stat.diag_data, 0xFF, sizeof(stat_var.fieldbus_stat.diag_data) );
                fip.init_timeout = 0;
            }
        }
        else
        {
            fip.init_timeout = 0;
        }
    }

    /*--- Set/Clr boot warnings ---*/

    if(Test(stat_var.fgc_stat.class_data.c50.st_latched,(FGC_LAT_CODE_FLT | FGC_LAT_DSP_FLT | FGC_LAT_DIM_SYNC_FLT |
                                                         FGC_LAT_MEM_FLT  | FGC_LAT_RFC_FLT)))
    {
        Set(stat_var.fgc_stat.class_data.c50.st_warnings,FGC_WRN_FGC_HW);
    }
    else
    {
        Clr(stat_var.fgc_stat.class_data.c50.st_warnings,FGC_WRN_FGC_HW);
    }

    if(Test(stat_var.fgc_stat.class_data.c50.st_latched,(FGC_LAT_FGC_PSU_FAIL | FGC_LAT_PSU_V_FAIL | FGC_LAT_VDC_FAIL)))
    {
        Set(stat_var.fgc_stat.class_data.c50.st_warnings,FGC_WRN_FGC_PSU);
    }
    else
    {
        Clr(stat_var.fgc_stat.class_data.c50.st_warnings,FGC_WRN_FGC_PSU);
    }

    if(Test(stat_var.fgc_stat.class_data.c50.st_latched,(FGC_LAT_DCCT_PSU_STOP | FGC_LAT_DCCT_PSU_FAIL)))
    {
        Set(stat_var.fgc_stat.class_data.c50.st_warnings,FGC_WRN_DCCT_PSU);
    }
    else
    {
        Clr(stat_var.fgc_stat.class_data.c50.st_warnings,FGC_WRN_DCCT_PSU);
    }

    /*--- PSU LED ---*/

    if(Test(stat_var.fgc_stat.class_data.c50.st_warnings,FGC_WRN_DCCT_PSU))
    {
        LED_SET(PSU_RED);
        LED_RST(PSU_GREEN);
    }
    else if(Test(stat_var.fgc_stat.class_data.c50.st_warnings,FGC_WRN_FGC_PSU))
    {
        LED_SET(PSU_RED);
        LED_SET(PSU_GREEN);
    }
    else
    {
        LED_RST(PSU_RED);
        LED_SET(PSU_GREEN);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CheckSlot5IfNot(uint16_t type0, uint16_t type1)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by test functions to see if the slot 5 interface is the specified type(s).  The
  function returns 1 on error, zero if the interface is okay.  If only one type needs to be checked then
  type1 should be zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    if((dev.reg_r | dev.reg_w) & REG_SLOT5)
    {
        MenuRspError("No access to Slot 5 interface");
        return(1);
    }

    if(type1)
    {
        if(dev.slot5_type != type0 && dev.slot5_type != type1)
        {
            MenuRspError("Slot 5 contains an %s not an %s nor an %s",
                        TypeLabel(slot5_types, dev.slot5_type),
                        TypeLabel(slot5_types, type0),
                        TypeLabel(slot5_types, type1));
            return(2);
        }
    }
    else
    {
        if(dev.slot5_type != type0)
        {
            MenuRspError("Slot 5 contains an %s not an %s",
                        TypeLabel(slot5_types, dev.slot5_type),
                        TypeLabel(slot5_types, type0));
            return(2);
        }
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CheckSlot5If(uint16_t type)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by test functions to see if the slot 5 interface is not the specified type.  The
  function returns 1 on error, zero if the interface is visible but not the specified type.
\*---------------------------------------------------------------------------------------------------------*/
{
    if((dev.reg_r | dev.reg_w) & REG_SLOT5)
    {
        MenuRspError("No access to Slot 5 interface");
        return(1);
    }

    if(dev.slot5_type == type)
    {
        MenuRspError("Slot 5 contains an %s",TypeLabel(slot5_types, dev.slot5_type));
        return(2);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
void RunMainProg(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to run the main program from the MP01 flash in page 4.
\*---------------------------------------------------------------------------------------------------------*/
{
    fputs(RSP_DATA ",Running main program...\n" RSP_OK "\n",TERM_TX);

    MSLEEP(1000);                       // Wait to allow response to be sent (and read)

    OS_ENTER_CRITICAL();                // Stop interrupts

    RGLEDS_P       = 0x0000;            // Turn off all LEDs
    SM_STARTTIME_P = SM_UXTIME_P;       // Set main program start time to Unix time
    SELFLASH(SEL_MP);                   // Select MP in 4-7

    asm
    {
        JMP CODES_MAINPROG_MCUPROG_START_32     // Jump to main program in page 4
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/
