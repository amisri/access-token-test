/*---------------------------------------------------------------------------------------------------------*\
  File:         sdflash.c

  Purpose:      Flash programming functions for sigma-delta

  Notes:        The SD-350/351 interface has two channels (A and B) with a 256KB flash for each channel
                The flashes are always programmed identically at the same time

                The flash memory is divided into two halfs (blocks 0 and 1) and provides 32KLW per block

                Block 0 is used for the internal ADCs (500kHz) and the external CERN 22-bit ADCs (500kHz
                or 1MHz)

                Block 1 is used for the external ADS1251 filters.  There are 32 pre-multiplied filters
                with the multipliers provided in the static array sd_func1281_multiplier[] in filters.h

  Filters in SD flash:

  +--------+--------------+---------------------+-----------------------+---------------+
  | FIR    | Flash Block  | 16bit Addr in block | Index of start sample | Use           |
  +--------+--------------+---------------------+-----------------------+---------------+
  | FIR 0  |    0         |  0x0000             |  0x0002               | 500kHz Filters|
  | FIR 1  |    0         |  0x4000             |  0x2002               |               |
  +--------+--------------+---------------------+-----------------------+---------------+
  | FIR 2  |    0         |  0x8000             |  0x4002               |  1MHz Filters |
  | FIR 3  |    0         |  0xC000             |  0x6002               |               |
  +--------+--------------+---------------------+-----------------------+---------------+
  |FIR 1281|    1         |  0x10000            |  0x8002               | 1MHz 1281     |
  |        |    1         |  0x10800            |  0x8402               | filters       |
     ...       ...             ...                   ...                   ...
  |        |    1         |  0x1F800            |  0xFC02               |               |
  +--------+--------------+---------------------+-----------------------+---------------+

\*---------------------------------------------------------------------------------------------------------*/

#define SDFLASH_GLOBALS

#include <stdint.h>

#include <sdflash.h>            // for sd_func0[], sd_func1[], sd_func2[], sd_func3[], sd_func1281[]

#include <stdbool.h>
#include <stdio.h>              // for fprintf()
#include <analog.h>
#include <term.h>               // for TERM_TX
#include <main.h>               // for timeout_ms global variable
#include <flash.h>              // for FLS_TIMEOUT_MS
#include <fgc_runlog_entries.h> // for FGC_RL_
#include <menu.h>               // for MenuRspError()
#include <macros.h>             // for Clr()


/*---------------------------------------------------------------------------------------------------------*/

static uint16_t * addr;
static uint16_t * data;
static uint16_t * datainc;

/*---------------------------------------------------------------------------------------------------------*/
void SDFlashSelect(uint8_t channel, uint16_t block, uint16_t address)
/*---------------------------------------------------------------------------------------------------------*\
  Select access to a flash location
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t flash_index = channel - 'A';

    addr    = sd_flash_addr[flash_index].addr;
    data    = sd_flash_addr[flash_index].data;
    datainc = sd_flash_addr[flash_index].datainc;

    ANA_CTRL_P = ANA_CTRL_SDMODE_MASK16;

    if(channel == 'A')
    {
        ANA_SD_PG_A_ADDH_P = block;
        ANA_SD_PG_A_ADDL_P = address;
    }
    else
    {
        ANA_SD_PG_B_ADDH_P = block;
        ANA_SD_PG_B_ADDL_P = address;
    }
}
#pragma MESSAGE DISABLE C1404 // Disable 'return expected' warning
/*---------------------------------------------------------------------------------------------------------*/
uint32_t SDFlashRead(uint8_t channel, uint16_t block, uint16_t address)
/*---------------------------------------------------------------------------------------------------------*\
  Read a long word from the SD flash
\*---------------------------------------------------------------------------------------------------------*/
{
    SDFlashSelect(channel, block, address);

    asm
    {
        LDX     datainc         // Address of flash data register with auto increment
        LDE     0,X             // Read first word and auto increment address
        LDD     0,X             // Read second word
        CLRW    ANA_CTRL_16
    }                           // Return uint32_t in ED registers
}
#pragma MESSAGE DEFAULT C1404
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SDFlashCheck(uint8_t channel, uint16_t block, uint16_t address, uint16_t exp_value)
/*---------------------------------------------------------------------------------------------------------*\
  Check whether the last flash operation completed successfully.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       result = 0;
    uint16_t      value;

    SDFlashSelect(channel, block, address);

    timeout_ms = FLS_TIMEOUT_MS;

    asm
    {
        LDX     data
    check:
        LDD     0x0000,X                // Read location in flash twice and check for toggling bits
        EORD    0x0000,X
        BITB    #0x40                   // Check XOR on toggle bit (D6)
        BEQ     verify                  // Branch to verify value if bits no longer toggling

        TST     timeout_ms              // Check for timeout on flash operation
        BEQ     timeout                 // Branch to timeout if timeout period expired

        LDD     0x0000,X                // Read location in flash to test timeout bit
        BITB    #0x20                   // Check Flash Timeout bit (D5)
        BEQ     check                   // Branch to check if Flash Timeout not expired

        LDD     0x0000,X                // Check again if toggle bits are now stable
        EORD    0x0000,X
        BITB    #0x40                   // Check XOR on toggle bit (D6)
        BEQ     verify                  // Branch to verify value if bits no longer toggling

    timeout:
        LDAA    #FGC_RL_FLS_TO  // Prepare to report flash timeout via run log
        STAA    result
        BRA     end

    verify:
        LDD     0x0000,X                // Readback value from location
        STD     value;                  // Remember value
        CPD     exp_value               // Check against expected value
        BEQ     end                     // Branch if okay

        LDAA    #FGC_RL_FLS_VERIFY      // Prepare to report verify failure via run log
        STAA    result

    end:
        CLRW    ANA_CTRL_16
    }
    /*--- Report failues to the run log ---*/

    if(result)                          // If failure occured
    {
        MenuRspError("SDFlashCheck %c at 0x%u%04X failed (0x%04X->0x%04X): %u", // Report error
                        channel, block, address, exp_value, value, result);
        return(1);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SDFlashCheckBlk(uint8_t channel, uint16_t block, uint16_t exp_value)
/*---------------------------------------------------------------------------------------------------------*\
  Check that all addresses in flash block have expected value.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      status = 0;
    uint16_t      bad_value;

    SDFlashSelect(channel, block, 0);

    asm
    {
        LDX     datainc                         // X is address of data with address increment
        LDE     #0x0000                         // E = length (0 = 64K)

    loop:
        LDD     0,X
        CPD     exp_value
        BEQ     okay

        STD     bad_value
        INCW    status
        BRA     end

    okay:
        ADDE    #1
        BNE     loop

    end:
    }

    if(status)
    {
        MenuRspError("SDFlashCheckBlk %c at 0x%u%04X failed (0x%04X->0x%04X)",          // Report error
                        channel, block,(*addr - 1), exp_value, bad_value);
        return(1);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
void SDFlashErase(uint8_t channel)
/*---------------------------------------------------------------------------------------------------------*\
  Erase flash chip.
\*---------------------------------------------------------------------------------------------------------*/
{
    SDFlashSelect(channel, 0, 0);

    asm                 // Erase chip
    {
        LDX     addr
        LDY     data

        LDD     #0x0555
        STD     0,X
        LDD     #0xAA
        STD     0,Y

        LDD     #0x02AA
        STD     0,X
        LDD     #0x55
        STD     0,Y

        LDD     #0x0555
        STD     0,X
        LDD     #0x80
        STD     0,Y

        LDD     #0x0555
        STD     0,X
        LDD     #0xAA
        STD     0,Y

        LDD     #0x02AA
        STD     0,X
        LDD     #0x55
        STD     0,Y

        LDD     #0x0555
        STD     0,X
        LDD     #0x10
        STD     0,Y

        CLRW    ANA_CTRL_16
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void SDFlashMemSet(uint16_t value)
/*---------------------------------------------------------------------------------------------------------*\
  Set the whole of both SD flashes to value
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      address  = 0;
    uint16_t      progress = 0;

    MenuRspProgress( false, 0, 64);

    SDFlashErase('A');                                  // Erase Flash A and Flash B
    SDFlashErase('B');

    if(SDFlashCheck('A', 0, 0, 0xFFFF) ||               // Check that Erase action has completed
       SDFlashCheck('B', 0, 0, 0xFFFF))
    {
        return;
    }

    do                                                  // Loop for 64K words
    {
        if(SDFlashProgAB(0, address, value) ||                  // Set value in block 0
           SDFlashProgAB(1, address, value))                    // Set value in block 1
        {
            return;
        }

        if(!(address & 0x3FF))
        {
            if(MenuRspProgress( true, ++progress, 64))
            {
                return;
            }
        }
    }
    while(++address);

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void SDFlashAllFunctions(void)
/*---------------------------------------------------------------------------------------------------------*\
  Programs all SD flash functions.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      idx;

    SDFlashErase('A');                                  // Erase Flash A and Flash B
    SDFlashErase('B');

    if(SDFlashCheck('A', 0, 0, 0xFFFF) ||               // Check that Erase action has completed
       SDFlashCheck('B', 0, 0, 0xFFFF))
    {
        return;
    }

    if (SDFlashFunction(0, 0 * SD_FLTR_LEN_W, 1, &sd_func0[0], sizeof(sd_func0) / sizeof(sd_func0[0])) ||
        SDFlashFunction(0, 1 * SD_FLTR_LEN_W, 1, &sd_func1[0], sizeof(sd_func1) / sizeof(sd_func1[0])) ||
        SDFlashFunction(0, 2 * SD_FLTR_LEN_W, 1, &sd_func2[0], sizeof(sd_func2) / sizeof(sd_func2[0])) ||
        SDFlashFunction(0, 3 * SD_FLTR_LEN_W, 1, &sd_func3[0], sizeof(sd_func3) / sizeof(sd_func3[0])))
    {
        return;
    }

    for(idx=0;idx < 26;idx++)
    {
        if(SDFlashFunction(1, (idx * 0x800), 2*idx+1, &sd_func1281[0], sizeof(sd_func1281) / sizeof(sd_func1281[0])))
        {
            return;
        }
    }

}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SDFlashAllFunctionsCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  Checks all SD flash functions.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      idx;

    if(SDFlashFunctionCheck(0, 0 * SD_FLTR_LEN_W, 1, &sd_func0[0], sizeof(sd_func0) / sizeof(sd_func0[0])) ||
       SDFlashFunctionCheck(0, 1 * SD_FLTR_LEN_W, 1, &sd_func1[0], sizeof(sd_func1) / sizeof(sd_func1[0])) ||
       SDFlashFunctionCheck(0, 2 * SD_FLTR_LEN_W, 1, &sd_func2[0], sizeof(sd_func2) / sizeof(sd_func2[0])) ||
       SDFlashFunctionCheck(0, 3 * SD_FLTR_LEN_W, 1, &sd_func3[0], sizeof(sd_func3) / sizeof(sd_func3[0])))
    {
        return(1);
    }

    for(idx=0;idx < 26;idx++)
    {
        if(SDFlashFunctionCheck(1, (idx * 0x800), 2*idx+1, &sd_func1281[0], sizeof(sd_func1281) / sizeof(sd_func1281[0])))
        {
            return(1);
        }
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SDFlashFunction(uint16_t block, uint16_t addr, int16_t multiplier, int32_t * FAR function, uint16_t length)
/*---------------------------------------------------------------------------------------------------------*\
  Program a flash location with a function. Variable length is in 32 bit samples. Return 0 if OK.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    int32_t      lvalue;
    uint16_t      value_h;
    uint16_t      value_l;

    fprintf(TERM_TX,RSP_DATA ",Writing filter @ 0x%u%04X\r",block,addr);

    for(i=0;i <= length;i++)
    {
        if(i < length)
        {
            lvalue  = (i > 1 && multiplier != 1 ? *(function++) * multiplier : *(function++));

//          if(i>1) lvalue = multiplier;

            value_h = lvalue >> 16;
            value_l = lvalue;
        }
        else
        {
            value_h = 0x8000;                   // Stop bit for old filter
            value_l = 0x0000;
        }

        if(SDFlashProgAB(block,  addr++, value_h) ||
           SDFlashProgAB(block,  addr++, value_l))
        {
            fprintf(TERM_TX,RSP_ERROR ",Failed to write filter at 0x%u%04X\r",block,--addr);
            return(1);
        }
    }
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SDFlashFunctionCheck(uint16_t block, uint16_t addr, int16_t multiplier,
                            int32_t * FAR function_p, uint16_t length)
/*---------------------------------------------------------------------------------------------------------*\
  Compares SD flash area with the function that should be programmed for channel A & B.
  Length is in in samples (32 bits).  Returns 0 if ok.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       channel;
    uint16_t      i;
    int32_t *FAR function;
    int32_t      lvalue;
    uint16_t      value_h;
    uint16_t      value_l;

    for(channel = 'A'; channel <= 'B'; channel++)
    {
        SDFlashSelect(channel, block, addr);            // Enter programming mode

        function = function_p;

        for(i=0;i <= length;i++)
        {
            if(i < length)
            {
                lvalue  = (i > 1 && multiplier != 1 ? *(function++) * multiplier : *(function++));

                value_h = lvalue >> 16;
                value_l = lvalue;
            }
            else
            {
                value_h = 0x8000;                       // Stop bit for old filter
                value_l = 0x0000;
            }

            if(*datainc != value_h ||
               *datainc != value_l)
            {
                Clr(ANA_CTRL_P,ANA_CTRL_SDMODE_MASK16); // Return to normal mode
                return(1);
            }
        }
    }

    Clr(ANA_CTRL_P,ANA_CTRL_SDMODE_MASK16);     // Return to normal mode
    return(0);                                  // success
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SDFlashProgAB(uint16_t block, uint16_t address, uint16_t value)
/*---------------------------------------------------------------------------------------------------------*\
  Program a flash location with value.
\*---------------------------------------------------------------------------------------------------------*/
{
    SDFlashWrite('A', block, address, value);
    SDFlashWrite('B', block, address, value);

    if(SDFlashCheck('A', block, address, value) || SDFlashCheck('B', block, address, value))
    {
        return(1);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SDFlashWrite(uint8_t channel, uint16_t block, uint16_t address, uint16_t value)
/*---------------------------------------------------------------------------------------------------------*\
  Program a flash location with value.
\*---------------------------------------------------------------------------------------------------------*/
{
    SDFlashSelect(channel, block, address);

    asm
    {
        LDX     addr
        LDY     data

        LDD     #0x0555
        STD     0,X
        LDD     #0xAA
        STD     0,Y

        LDD     #0x02AA
        STD     0,X
        LDD     #0x55
        STD     0,Y

        LDD     #0x0555
        STD     0,X
        LDD     #0xA0
        STD     0,Y

        LDD     address
        STD     0,X
        LDD     value
        STD     0,Y
        CLRW    ANA_CTRL_16
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SDFlashReset(uint8_t channel)
/*---------------------------------------------------------------------------------------------------------*\
  Reset flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    SDFlashSelect(channel, 0, 0);

    asm
    {
        LDX     addr
        CLRD
        STD     0,X
        LDD     #0x00F0
        STD     0,X
        CLRW    ANA_CTRL_16
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sdflash.c
\*---------------------------------------------------------------------------------------------------------*/
