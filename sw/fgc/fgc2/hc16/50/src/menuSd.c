/*---------------------------------------------------------------------------------------------------------*\
  File:         menuSd.c

  Purpose:      Functions to run for the flash boot menu options

  Notes:

  History:

    28/08/03    stp     Created
    10/08/08    qak     Reorganised and function names updated
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <defconst.h>           // for FGC_INTERFACE_
#include <menu.h>               // for MenuGetInt16U()
#include <dev.h>                // for dev global variable
#include <main.h>               // for CheckSlot5IfNot()
#include <sdflash.h>            // for SDFlashReset()
#include <stdio.h>              // for fprintf()
#include <mcu_dependent.h>      // for BOOT_STATE_SELF_TEST
#include <memmap_mcu.h>         // for RGLEDS_VS_GREEN_MASK16
#include <analog.h>             // for SD_FLASH_SIZE

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuSDFlashReset(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Reset SD flashes
\*---------------------------------------------------------------------------------------------------------*/
{
    /* Check that card type is sigma-delta */

    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }

    SDFlashReset('A');
    SDFlashReset('B');
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSDFlashErase(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Erase and check SD flashes
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }

    SDFlashErase('A');                                  // Erase Flash A and Flash B
    SDFlashErase('B');

    if(SDFlashCheck('A', 0, 0, 0xFFFF) ||               // Check that Erase action has completed
       SDFlashCheck('B', 0, 0, 0xFFFF))
    {
        return;
    }

    if(SDFlashCheckBlk('A', 0, 0xFFFF) ||               // Check that every location has been erased
       SDFlashCheckBlk('A', 1, 0xFFFF) ||
       SDFlashCheckBlk('B', 0, 0xFFFF) ||
       SDFlashCheckBlk('B', 1, 0xFFFF))
    {
        return;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSDFlashValue(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Write value to sigma-delta flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      value;

    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }

    if(MenuGetInt16U(argv[0], 0, 0xFFFF, &value))
    {
        return;
    }

    SDFlashMemSet(value);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSDStoreFunctions(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Check the SD filters in SD flash and reprograms it if necessary.  If the SD board is missing, the
  function will report this as a response rather than an error because the function is called from the
  start up self tests and it must not abort in the case of an SAR interface.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      stat;

    stat = CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351);

    if(stat)                            // If not the right card or Slot5 interface not visible
    {
        if(stat == 2 && dev.state == BOOT_STATE_SELF_TEST)      // If not the right card during self test
        {
            menu.error_buf[0] = '\0';                                   // Suppress error
        }
        return;
    }

    if(SDFlashAllFunctionsCheck())
    {
        SDFlashAllFunctions();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSDDisplayFilters(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Usage: Start,length

  This will report the long word tap values for both filters A and B.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      start_l;
    uint32_t      length_l;
    uint16_t      i;
    uint16_t      tapidx;
    uint16_t      block;
    uint16_t      addr;

    uint32_t      a;                      // Flash A
    uint32_t      b;                      // Flash B

    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }

    // Parse start argument

    if(MenuGetInt32U(argv[0], 0, (SD_FLASH_SIZE / 2) - 1,       &start_l) |
       MenuGetInt32U(argv[1], 1, (SD_FLASH_SIZE / 2) - start_l, &length_l))
    {
        return;
    }

    for(i = 0; i < (uint16_t)length_l ; i++)
    {
        tapidx = (uint16_t)start_l + i;
        block  = !!(tapidx & 0x8000);
        addr   = tapidx << 1;

        a = SDFlashRead('A', block, addr);
        b = SDFlashRead('B', block, addr);

        fprintf(TERM_TX,RSP_DATA ",0x%04X:0x%08lX:0x%08lX\n",tapidx,a,b);
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuSd.c
\*---------------------------------------------------------------------------------------------------------*/
