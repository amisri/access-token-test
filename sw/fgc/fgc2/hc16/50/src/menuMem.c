/*---------------------------------------------------------------------------------------------------------*\
  File:         menuMem.c

  Purpose:      Functions to run for the MEM-250 boot menu options

  Author:       Stephen.Page@cern.ch

  Notes:

  History:

    22/09/03    stp     Created
\*---------------------------------------------------------------------------------------------------------*/

#define MENUMEM_GLOBALS

#include <menuMem.h>

#include <stdbool.h>
#include <menu.h>               // for MenuRspArg(), MenuRspError()
#include <registers.h>          // for RegWrite(), RegRead()
#include <fieldbus_boot.h>      // for stat_var global variable
#include <dev.h>                // for dev global variable
#include <c32.h>                // for StopDsp()
#include <mcu_dependent.h>      // for SELFLASH()
#include <defconst.h>           // for FGC_FLT_, FGC_LAT_
#include <macros.h>             // for Set()
#include <memmap_mcu.h>         // for CPU_RESET_P

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuMemHc16Read(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Read check HC16 RAM.  This is a simple check that will fail with a bus error if the memory is not
  accessible (the program is unlikely to be running in this case).  It also exercises the memory and
  checks for single bit errors.
\*---------------------------------------------------------------------------------------------------------*/
{
    dev.mem_sbe_f = 0;                  // Clear single bit error detector

    asm                                         // Read all words in the page
    {
        PSHM    K                               // Save page registers
        LDX     #0x0800                         // Start with X at beginning of SRAM in page 0

    loop:                                       // Read block of 16 bytes
        TSTW    0,X
        TSTW    2,X
        TSTW    4,X
        TSTW    6,X
        TSTW    8,X
        TSTW    10,X
        TSTW    12,X
        TSTW    14,X
        AIX     #16                             // Advance X
        BNE     loop                            // Loop until X is at the start of the next page

        PULM    K                               // Recover page register
    }

    if(dev.mem_sbe_f)                   // If single bit error(s) detected
    {
        MenuRspError("Single bit error(s) detected while reading HC16 SR0");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMemHc16Pattern(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function performs a write/read test on the top half of the HC16 SR0 memory page.
  This test is probably a waste of time since any significant memory problem will prevent the boot
  from running.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint16_t      addr;
    uint16_t      value16;
    uint16_t      test_pattern;

    if ( StopDsp() )                    // If C32 memory fault or standalone
    {
        return;                                 // Return error
    }

    dev.mem_sbe_f = 0;                  // Clear single bit error detector

    /* Perform pattern tests */

    for(i=0;i < num_patterns_16;i++)    // For every pattern
    {
        test_pattern = patterns_16[i];          // Initialise pattern

        asm                                     // Read all words in the page
        {
            PSHM        K                       // Save page registers
            LDX         #0x8000                 // Start with X at middle of the page
            LDD         test_pattern            // D = test_pattern
            TDE
            COME                                // E = ~test_pattern

        loop1:                                  // Write block of 16 bytes
            STD         0,X
            STE         2,X
            STD         4,X
            STE         6,X
            STD         8,X
            STE         10,X
            STD         12,X
            STE         14,X
            AIX         #16                     // Advance X by 16 bytes
            BNE         loop1                   // Loop until X is at the start of the next page

            LDAB        #0                      // Reset XK:IX = 0x18000
            TBXK
            LDX         #0x8000
        loop2:
            COME                                // Check patterns in the page
            CPE         0,X
            BNE         fail
            AIX         #2
            BNE         loop2
            BRA         endloop

        fail:
            LDD         0,X
            STD         value16
            STE         test_pattern
            STX         addr
            PULM        K                       // Recover page register
        }

            MenuRspError("HC16 mem pattern failure at 0x0%04X: exp:0x%04X got:0x%04X",
                        addr,test_pattern,value16);
            Set(stat_var.fgc_stat.class_data.c50.st_faults, FGC_FLT_FGC_HW);                            // Report FGC HW fault
            Set(stat_var.fgc_stat.class_data.c50.st_latched, FGC_LAT_MEM_FLT);                  // Report MEM fault
            DspLoadProgramAndRun();
            return;

        asm                                                     // Read all words in the page
        {
        endloop:
            PULM        K                                       // Recover page register
        }
    }

    if(dev.mem_sbe_f)                           // If single bit error(s) detected
    {
        MenuRspError("Single bit error(s) detected while pattern testing HC16 SR0");
    }

    DspLoadProgramAndRun();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMemHc16Address(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function partially tests the HC16 address bus by writing the top half of SR0 with the
  address of each location in the location.  It then reads back and checks that the values are correct.
  This test is probably a waste of time since any significant memory problem will prevent the boot from
  running.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint16_t      value;

    if(StopDsp())                       // If C32 memory fault or standalone
    {
        return;                                 // Return error
    }

    dev.mem_sbe_f = 0;                  // Clear single bit error detector

    /* Perform address test */

    for(i=0;i < 0x4000;i++)                     // For words in the top of page 1
    {
        ((uint16_t*)(0x8000))[i] =  i;                    // Write location index into location
    }

    for(i=0;i < 0x4000;i++)                     // For words in the top of page 1
    {
        value = ((uint16_t*)(0x8000))[i];                 // Read back location

        if(value != i)                          // If location doesn't contain index
        {
            MenuRspError("HC16 mem addr failure at 0x0%04X: exp:0x%04X got:0x%04X",
                        (0x8000 + i*2),i,value);
            Set(stat_var.fgc_stat.class_data.c50.st_faults,FGC_FLT_FGC_HW);                             // Report FGC HW fault
            Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_MEM_FLT);                   // Report MEM fault
            DspLoadProgramAndRun();
            return;
        }
    }

    if(dev.mem_sbe_f)                           // If single bit error(s) detected
    {
        MenuRspError("Single bit error(s) detected while address testing HC16 SR0");
    }

    DspLoadProgramAndRun();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMemC32Read(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Read check C32 RAM.  This will access every long word of the C32 memory checking for single-bit errors
  from the C32 EDAC.  To do this, it reads every even word of the C32 memory via the dual port window
  in page 6 (C32 in tristate).
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint16_t      p;
    uint16_t      value;

    if(StopDsp())
    {
        return;
    }

    CPU_RESET_P &= ~CPU_RESET_CTRL_DSP_MASK16;                  // Remove C32 reset to open dual-port window

    dev.mem_sbe_f = 0;                  // Clear single bit error flag

    for(i=0;i < 0x1000;i++);            // Wait while access HC16 ram for many milliseconds

    if(dev.mem_sbe_f)                   // If single bit errors detected (more than 1 in a millisecond)
    {
        MenuRspError("HC16 single bit memory errors are being generated");
        DspLoadProgramAndRun();
        return;
    }

    SELDPWPG(0);                        // Select C32 SRAM page 0
    SELFLASH(SEL_SR);                   // Select DPW in page 6 and reset DPW base address to page 0

    if(TST_CPU_RESET(C32INIT)           ||      // If C32 memory not initialised, or
       RegRead(DPW_32,  &value, true)   ||      // read of first word of DPW fails, or
       RegRead(DPW_32+2,&value, true))          // read of second word of DPW fails, or
    {
        MenuRspError("C32 Memory fault");               // Report error
        Set(stat_var.fgc_stat.class_data.c50.st_faults,FGC_FLT_FGC_HW);                 // Report FGC HW fault
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_MEM_FLT);               // Report MEM fault
        DspLoadProgramAndRun();
        return;
    }

    for(p=0;p < 8;p++)
    {
        SELDPWPG(p);                                            // Select C32 SRAM page

        asm                                                     // Read all words in the page
        {
            PSHM        K                                       // Save page registers
            LDAB        #6
            TBXK                                                // Set XK to DPW page
            LDX         #0x0000                                 // Start with X at beginnig of page

        loop:                                                   // Read block of 16 bytes
            TSTW        0,X
            TSTW        2,X
            TSTW        4,X
            TSTW        6,X
            TSTW        8,X
            TSTW        10,X
            TSTW        12,X
            TSTW        14,X
            AIX         #16                                     // Advance X
            BNE         loop                                    // Loop until X is at the start of the next page

            PULM        K                                       // Recover page register
        }
    }

    if(dev.mem_sbe_f)                                   // If single bit error(s) detected
    {
        MenuRspError("Single bit error(s) detected while reading C32 memory");
    }

    DspLoadProgramAndRun();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMemC32Pattern(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Write/read patters in C32 memory
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t      i;
    uint16_t      p;
    uint16_t      progress = 0;
    uint16_t      test_pattern;
    uint16_t      value16;
    uint32_t      addr32;

    if(StopDsp())
    {
        return;
    }

    CPU_RESET_P &= ~CPU_RESET_CTRL_DSP_MASK16;                  // Remove C32 reset to open dual-port window

    dev.mem_sbe_f = 0;                  // Clear single bit error flag

    for(i=0;i < 0x1000;i++);            // Wait while access HC16 ram for many milliseconds

    if(dev.mem_sbe_f)                   // If single bit errors detected (more than 1 in a millisecond)
    {
        MenuRspError("HC16 single bit memory errors are being generated");
        DspLoadProgramAndRun();
        return;
    }

    SELFLASH(SEL_SR);                   // Select DPW in page 6 and reset DPW base address to page 0

    if(TST_CPU_RESET(C32INIT)           ||      // If C32 memory not initialised, or
       RegRead(DPW_32,  &value16, true) ||      // read of first word of DPW fails, or
       RegRead(DPW_32+2,&value16, true) ||      // read of second word of DPW fails, or
       RegWrite(DPW_32,  0, true)       ||      // write of first word of DPW fails, or
       RegWrite(DPW_32+2,0, true))              // write of second word of DPW fails
    {
        MenuRspError("C32 Memory fault");               // Report error
        Set(stat_var.fgc_stat.class_data.c50.st_faults,FGC_FLT_FGC_HW);                 // Report FGC HW fault
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_MEM_FLT);               // Report MEM fault
        DspLoadProgramAndRun();
        return;
    }

    /* Perform pattern tests */

    for(i = 0;i < num_patterns_16;i++)
    {
        for(p=0;p < 8;p++)
        {
            SELDPWPG(p);                                        // Select C32 SRAM page

            test_pattern = patterns_16[i];

            asm                                                 // Read all words in the page
            {
                PSHM    K                                       // Save page registers
                LDAB    #6
                TBXK                                            // Set XK to DPW page
                LDX     #0x0000                                 // Start with X at beginnig of page

                LDD     test_pattern
                TDE
                COME

            loop1:                                              // Write block of 16 bytes
                STD     0,X
                STE     2,X
                STD     4,X
                STE     6,X
                STD     8,X
                STE     10,X
                STD     12,X
                STE     14,X
                AIX     #16                                     // Advance X
                BNE     loop1                                   // Loop until X is at the start of the next page

                LDAB    #6
                TBXK                                            // Set XK to DPW page
                LDX     #0x0000                                 // Start with X at beginnig of page

            loop2:
                COME                                            // Check patterns in the page
                CPE     0,X
                BNE     fail
                AIX     #2
                BNE     loop2
                BRA     endloop2

            fail:
                STE     test_pattern
                LDD     0,X
                STD     value16
                LDD     p
                STD     addr32:0
                STX     addr32:2
                PULM    K                                       // Recover page register
            }

                MenuRspError("C32 mem pattern failure at 0x%05lX: exp:0x%04X got:0x%04X",
                            addr32,test_pattern,value16);
                Set(stat_var.fgc_stat.class_data.c50.st_faults,FGC_FLT_FGC_HW);                         // Report FGC HW fault
                Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_MEM_FLT);                       // Report MEM fault
                DspLoadProgramAndRun();
                return;

            asm                                                 // Read all words in the page
            {
            endloop2:
                PULM    K                                       // Recover page register
            }

            if(MenuRspProgress(true,++progress,num_patterns_16*8))      // If user abort
            {
                DspLoadProgramAndRun();
                return;                                                                 // Quit
            }
        }
    }

    if(dev.mem_sbe_f)                                   // If single bit error(s) detected
    {
        MenuRspError("Single bit error(s) detected while pattern testing C32 memory");
    }

    DspLoadProgramAndRun();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMemC32Address(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Check the C32 memory address bus.
\*---------------------------------------------------------------------------------------------------------*/
{
    int16_t      i;
    uint16_t      p;
    uint16_t      progress = 0;
    uint16_t      value16;
    uint32_t      value32;
    uint32_t      addr32;

    if(StopDsp())
    {
        return;
    }

    CPU_RESET_P &= ~CPU_RESET_CTRL_DSP_MASK16;                  // Remove C32 reset to open dual-port window

    dev.mem_sbe_f = 0;                  // Clear single bit error flag

    for(i=0;i < 0x1000;i++);            // Wait while access HC16 ram for many milliseconds

    if(dev.mem_sbe_f)                   // If single bit errors detected (more than 1 in a millisecond)
    {
        MenuRspError("HC16 single bit memory errors are being generated");
        DspLoadProgramAndRun();
        return;
    }

    SELFLASH(SEL_SR);                   // Select DPW in page 6 and reset DPW base address to page 0

    if(TST_CPU_RESET(C32INIT)           ||      // If C32 memory not initialised, or
       RegRead(DPW_32,  &value16, true) ||      // read of first word of DPW fails, or
       RegRead(DPW_32+2,&value16, true) ||      // read of second word of DPW fails, or
       RegWrite(DPW_32,  0, true)       ||      // write of first word of DPW fails, or
       RegWrite(DPW_32+2,0, true))              // write of second word of DPW fails
    {
        MenuRspError("C32 Memory fault");               // Report error
        Set(stat_var.fgc_stat.class_data.c50.st_faults,FGC_FLT_FGC_HW);                 // Report FGC HW fault
        Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_MEM_FLT);               // Report MEM fault
        DspLoadProgramAndRun();
        return;
    }

    for(p=0;p < 8;p++)
    {
        SELDPWPG(p);                                            // Select C32 SRAM page

            asm                                                 // Read all words in the page
            {
                PSHM    K                                       // Save page registers
                LDAB    #6
                TBXK                                            // Set XK to DPW page
                LDX     #0x0000                                 // Start with X at beginnig of page

                LDE     p
                CLRD

            loop1:                                              // Write address of location at the location
                STE     0,X
                STD     2,X
                ADDD    #4
                STE     4,X
                STD     6,X
                ADDD    #4
                STE     8,X
                STD     10,X
                ADDD    #4
                STE     12,X
                STD     14,X
                ADDD    #4
                AIX     #16                                     // Advance X
                BNE     loop1                                   // Loop until X is at the start of the next page

                PULM    K                                       // Recover page register
            }

        if(MenuRspProgress(true,++progress,16))         // If user abort
        {
            DspLoadProgramAndRun();
            return;                                                     // Quit
        }
    }

    for(p=0;p < 8;p++)
    {
        SELDPWPG(p);                                            // Select C32 SRAM page

            asm                                                 // Read all words in the page
            {
                PSHM    K                                       // Save page registers
                LDAB    #6
                TBXK                                            // Set XK to DPW page
                LDX     #0x0000                                 // Start with X at beginnig of page
                LDE     p

            loop2:
                CPE     0,X
                BNE     fail
                CPX     2,X
                BNE     fail
                AIX     #4
                BNE     loop2
                BRA     endloop2

            fail:
                STE     addr32:0
                STX     addr32:2
                LDD     0,X
                STD     value32:0
                LDD     2,X
                STD     value32:2
                PULM    K                                       // Recover page register
            }

                MenuRspError("C32 mem addr failure at 0x%08lX: exp:0x%08lX got:0x%08lX",addr32,addr32,value32);
                Set(stat_var.fgc_stat.class_data.c50.st_faults,FGC_FLT_FGC_HW);                         // Report FGC HW fault
                Set(stat_var.fgc_stat.class_data.c50.st_latched,FGC_LAT_MEM_FLT);                       // Report MEM fault
                DspLoadProgramAndRun();
                return;

            asm                                                 // Read all words in the page
            {
            endloop2:
                PULM    K                                       // Recover page register
            }

        if(MenuRspProgress(true,++progress,16))         // If user abort
        {
            DspLoadProgramAndRun();
            return;                                                     // Quit
        }
    }

    if(dev.mem_sbe_f)                                   // If single bit error(s) detected
    {
        MenuRspError("Single bit error(s) detected while address testing C32 memory");
    }

    DspLoadProgramAndRun();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMemDisplaySBEcounters(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Report EDAC SBE counters from C32 test boot program
\*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("HC16,%lu,C32,%lu", DPRAM_BT_HC16EDACSBES_P,DPRAM_BT_C32EDACSBES_P);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuMem.c
\*---------------------------------------------------------------------------------------------------------*/
