/*---------------------------------------------------------------------------------------------------------*\
  File:         MenuAna.c

  Purpose:      Functions to run for the Analog card boot menu options

  Author:       Stephen.Page@cern.ch

  Notes:

  History:

    11/11/03    stp     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdio.h>

#include <analog.h>             // for menusd global variable
#include <mcu_dependent.h>      // for MSLEEP()
#include <main.h>               // for timeout_ms global variable used by MSLEEP(), CheckSlot5IfNot()
#include <menu.h>               // for MenuRspArg(), MenuGetInt32S(), MenuGetInt32U(), MenuGetInt16U(), MenuRspError()
#include <defconst.h>           // for FGC_INTERFACE_
#include <stdlib.h>             // for labs()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

void MenuAnaSdRefStab           (uint16_t argc, char **argv)      {MenuRspError("Not yet implemented");}
void MenuAnaSarRefStab          (uint16_t argc, char **argv)      {MenuRspError("Not yet implemented");}

/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSetCtrl(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the Control register to value provided
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t ctrl_value;

    if(CheckSlot5If(FGC_INTERFACE_RFC_500))
    {
        return;
    }

    if(!MenuGetInt16U(argv[0], 0, 0xFFFF, &ctrl_value))
    {
        ANA_CTRL_P = ctrl_value;
        MenuRspArg("0x%04X", ANA_CTRL_P);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSingleSample(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Take a single sample from the analogue interface
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5If(FGC_INTERFACE_RFC_500))
    {
        return;
    }

    AnaGetSingleSample();

    // Output 48-bit values

    MenuRspArg("0x%04X%04X%04X",
                menusd.a.raw_h,menusd.a.raw_m,menusd.a.raw_l);

    MenuRspArg("0x%04X",menusd.a.index);

    MenuRspArg("0x%04X%04X%04X",
                menusd.b.raw_h,menusd.b.raw_m,menusd.b.raw_l);

    MenuRspArg("0x%04X",menusd.b.index);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaMultiSample(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Take ~4s of samples from the analogue interface
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5If(FGC_INTERFACE_RFC_500))
    {
        return;
    }

    AnaGetMultiSample(4096);

    MenuRspArg("0x%03X%04X%04X%X",
                    menusd.a.sum[0]&0xFFF,menusd.a.sum[1],menusd.a.sum[2],(menusd.a.sum[3] >> 12));

    MenuRspArg("0x%04X%04X%04X",
                    menusd.a.p2p_h,menusd.a.p2p_m,menusd.a.p2p_l);

    MenuRspArg("0x%03X%04X%04X%X",
                    menusd.b.sum[0]&0xFFF,menusd.b.sum[1],menusd.b.sum[2],(menusd.b.sum[3] >> 12));

    MenuRspArg("0x%04X%04X%04X",
                    menusd.b.p2p_h,menusd.b.p2p_m,menusd.b.p2p_l);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSetDac(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the dac to value provided
\*---------------------------------------------------------------------------------------------------------*/
{
    int32_t dac_value;

    if(CheckSlot5If(FGC_INTERFACE_RFC_500))
    {
        return;
    }

    if(!MenuGetInt32S(argv[0], -524288, 524287, &dac_value))
    {
        dac_value <<= 12;               // Value in DAC is left justified (bits 31-12)

        ANA_DAC_HIGH_P = ((uint16_t*)&dac_value)[0];              // High must be written first
        ANA_DAC_LOW_P  = ((uint16_t*)&dac_value)[1];
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaDacDataTransfer(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function tests the data transfert to the DAC. Every ms, the given value is written in the DAC. Then
  the DAC is read back through the ADC.
\*---------------------------------------------------------------------------------------------------------*/
{
    int32_t      dac_val;
    int32_t      dac_glitch;
    uint32_t      threshold;

    if(CheckSlot5If(FGC_INTERFACE_RFC_500))
    {
        return;
    }

    if(!MenuGetInt32S(argv[0], -524288, 524287,     &dac_val)    &&
       !MenuGetInt32S(argv[1], -524288, 524287,     &dac_glitch) &&
       !MenuGetInt32U(argv[2], 0,       0xFFFFFFFF, &threshold))
    {
        AnaDacDataTransfer(dac_val, dac_glitch, threshold);
    }
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaMpxAna(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the analogue MPX directly from arguments
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      mpx_a, mpx_b;

    if(CheckSlot5If(FGC_INTERFACE_RFC_500))
    {
        return;
    }

    if(!MenuGetInt16U(argv[0], 0, 7, &mpx_a) && !MenuGetInt16U(argv[1], 0, 7, &mpx_b))
    {
        AnaMpxAna(mpx_a, mpx_b);

        MenuRspArg("0x%04X", ANA_MPX_P);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaMpxDig(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the digital MPX directly from arguments
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      mpx_a, mpx_b;

    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }

    if(!MenuGetInt16U(argv[0], 0, 15, &mpx_a) && !MenuGetInt16U(argv[1], 0, 15, &mpx_b))
    {
        AnaMpxDig(mpx_a, mpx_b);

        MenuRspArg("0x%04X", ANA_SD_NL_A_R_INDEX_P);
        MenuRspArg("0x%04X", ANA_SD_NL_B_R_INDEX_P);
        MenuRspArg("0x%04X", ANA_MPX_P);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaFltFrq(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the digital filter frequency directly from arguments, 0 for 500kHz, 1 for 1MHz.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      frq_a, frq_b;

    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }

    if(!MenuGetInt16U(argv[0], 0, 1, &frq_a) && !MenuGetInt16U(argv[1], 0, 1, &frq_b))
    {
        AnaFltFrq(frq_a, frq_b);

        MenuRspArg("0x%04X", ANA_SD_NL_A_R_INDEX_P);
        MenuRspArg("0x%04X", ANA_SD_NL_B_R_INDEX_P);
        MenuRspArg("0x%04X", ANA_MPX_P);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSdType(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
   This function checks type is SD.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSdFilters(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
   This function sets digital MPX on calibration channels and reads back with the ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t i,j,filter_freq;
    uint32_t val[2];
    static uint16_t mpx_list[10]  = {3,4,5,6,7,11,12,13,14,15};
    static uint32_t ref[10]       = {SD_DIG_CAL3,SD_DIG_CAL4,SD_DIG_CAL5,SD_DIG_CAL6,SD_DIG_CAL7,
                                   SD_DIG_CAL11,SD_DIG_CAL12,SD_DIG_CAL13,SD_DIG_CAL14,SD_DIG_CAL15};

    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }

    /* Set dig MPX on calibration channels */

    for(i=0 ; i<10 ; i++)       // 500kHz, 1MHz
    {
        filter_freq = ((mpx_list[i]>7) ? 1 : 0);        // Dig mpx: 3-7 for 500kHz, 11-15 for 1MHz
        AnaFltFrq(filter_freq,filter_freq);             // Set filter freq
        AnaMpxDig(mpx_list[i],mpx_list[i]);

        MSLEEP(10);

        AnaGetSingleSample();

        val[0] = ((uint32_t)menusd.a.raw_h << 16) | (uint32_t)menusd.a.raw_m;
        val[1] = ((uint32_t)menusd.b.raw_h << 16) | (uint32_t)menusd.b.raw_m;

        for(j=0 ; j<2 ; j++)
        {
            if ( labs(val[j] - ref[i]) > 1)
            {
                MenuRspError("Calibration err on dig chan %u-ADC %c",mpx_list[i],'A' + j);
                return;
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSdRef(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
   This function checks reference voltages (p2p noise, values tolerance).
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }

    AnaRefAdcs(SLOT5_SAMPLES_REF);
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSdDac(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function calibrates the DAC. Sets min, zero and max values and reads back the ADCs. Check levels and
  10V stability.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5IfNot(FGC_INTERFACE_SD_350,FGC_INTERFACE_SD_351))
    {
        return;
    }

    AnaDac(SLOT5_SAMPLES_REF);
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSarType(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
   This function checks type is SAR.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5IfNot(FGC_INTERFACE_SAR_400,0))
    {
        return;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSarRef(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
   This function checks reference voltages (p2p noise, values tolerance).
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5IfNot(FGC_INTERFACE_SAR_400,0))
    {
        return;
    }

    AnaRefAdcs(SLOT5_SAMPLES_REF);
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuAnaSarDac(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function calibrates the DAC. Sets min, zero and max values and reads back the ADCs.
\*---------------------------------------------------------------------------------------------------------*/
{
    if(CheckSlot5IfNot(FGC_INTERFACE_SAR_400,0))
    {
        return;
    }

    AnaDac(SLOT5_SAMPLES_REF);
    return;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: MenuAna.c
\*---------------------------------------------------------------------------------------------------------*/
