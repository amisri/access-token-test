/*---------------------------------------------------------------------------------------------------------*\
 File:      diag_boot.c

 Purpose:   QSPI related functions

\*---------------------------------------------------------------------------------------------------------*/

#define DIAG_BOOT_GLOBALS       // for diag_bus, diag_ctrl_lbl[]

#include <diag_boot.h>
#include <memmap_mcu.h>         // for SM_UXTIME_P
#include <macros.h>             // for Test(), Set()
#include <mem.h>                // for MemCpyWords()
#include <defconst.h>
#include <fieldbus_boot.h>      // for stat_var global variable
#include <defconst.h>           // for FGC_CRATE_TYPE_
#include <crate_boot.h>         // for crate global variable
#include <fieldbus_boot.h>      // For CLASS_FIELD
#include <memmap_mcu.h>         // for UTC_*
#include <definfo.h>    // for FGC_CLASS_ID


/*---------------------------------------------------------------------------------------------------------*/
void Check_FGC2_PSU(uint16_t psu_stat)
/*---------------------------------------------------------------------------------------------------------*\
  The DIPS board monitors the status signals for the FGC and DCCT power supplies:

                  psu_stat
        -----------------------
        0x001: PSU_DCCT_STOP
        0x002: PSU2_DCCT_WARNING *
        0x004: PSU_DCCT_WARNING
        0x008: PSU_FGC_STOP
        0x010: VDC_FAIL
        0x020: PSU_FGC_WARNING
        0x040:
        0x080:
        0x100:        * Initial production of the DCCT PSUs
        0x200:          does not include redundancy, so this
        0x400:          signal is not used
        0x800:

  This function is called at INTERRUPT LEVEL!  It is called every 10ms immediately after the Digital B
  scan of the DIPS board is complete in order to check the FGC and DCCT PSU statuses.  It is important
  to check the FGC PSU status quickly because it may be about to switch off and we want to report this
  to the gateway before the FGC disappears offline.
\*---------------------------------------------------------------------------------------------------------*/
{
    // If DCCT PSU expected

    if(crate.type == FGC_CRATE_TYPE_PC_120A ||
       crate.type == FGC_CRATE_TYPE_PC_600A ||
       crate.type == FGC_CRATE_TYPE_RECEPTION)
    {
        if(psu_stat & 0x001)
        {
            Set(stat_var.fgc_stat.class_data.c50.st_latched, FGC_LAT_DCCT_PSU_STOP);
        }

        if(psu_stat & 0x004)
        {
            Set(stat_var.fgc_stat.class_data.c50.st_latched, FGC_LAT_DCCT_PSU_FAIL);
        }
    }

    if(crate.type != FGC_CRATE_TYPE_RECEPTION)
    {
        if(psu_stat & 0x010)
        {
            Set(stat_var.fgc_stat.class_data.c50.st_latched, FGC_LAT_VDC_FAIL);
        }

        if(psu_stat & 0x020)
        {
            Set(stat_var.fgc_stat.class_data.c50.st_latched, FGC_LAT_FGC_PSU_FAIL);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/

#ifdef __HC16__
    #pragma NO_FRAME
#endif
void QSPIbusStateMachine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called from the 1 millisecond Tick interrupt.
  It must quickly read any data waiting from a previous scan, and trigger the next scan if required.

  The reset process for the diag uses 3 states because there needs to be a delay between the end
  of the reset pulse (clock line high) and the start of the scan.

  reset process
  a) DIAG_RESET = clock high
  b) DIAG_START = clock low
  c) DIAG_SCAN  = trigger
  d) DIAG_RUN   = readout + next trigger

  for next DIM register only
     DIAG_RUN = readout + next trigger


  the readout data will be stored in
    FGC2  reception_buffer   [2][16]
    FGC3  reception_buffer[2][7][16]  // [A,B][registers] for [16 DIM cards]
        the 7 registers are stored in this order
        0 - Dig0    DIM register ID:0
        1 - Dig1    DIM register ID:1
        2 - Ana0    DIM register ID:4
        3 - Ana1    DIM register ID:5
        4 - Ana2    DIM register ID:6
        5 - Ana3    DIM register ID:7
        6 - TrigCounter DIM register ID:2
        there is no ID:3
\*---------------------------------------------------------------------------------------------------------*/
{
    static uint16_t   data;
    // ID:3 is never received this is why for ID:3 we address also index 0
    // offset value to be used for each ID
    static uint16_t   IDtoIndexInData[] = { 8, 9, 10, 0, 0, 2, 4, 6,        // branch A
                                          8, 9, 10, 0, 1, 3, 5, 7 };      // branch B
/*
    .data[] = diagnostic data buffer will be filled like this

       0 DIM register ID:4   Ana0 A         ID:3 A or B
       1 DIM register ID:4   Ana0 B
       2 DIM register ID:5   Ana1 A
       3 DIM register ID:5   Ana1 B
       4 DIM register ID:6   Ana2 A
       5 DIM register ID:6   Ana2 B
       6 DIM register ID:7   Ana3 A
       7 DIM register ID:7   Ana3 B
       8 DIM register ID:0   Dig0 A or B
       9 DIM register ID:1   Dig1 A or B
      10 DIM register ID:2   TrigCounter A or B
         ----------------------------------- filled during the normal reception
      11 DIM register ID:1   Dig1 A
      12 DIM register ID:1   Dig1 B
         ----------------------------------- filled at the end of the reception
*/

    static uint16_t   register_id_a;


    // ToDo: can this be moved to diag_boot.h as const instead of static ?
    // the DIM register arrive in this order, first, register with ID:0 then ID:1, ID:4, ID:5, ID:6, ID:7 and last ID:2
    static uint16_t rx_order_to_register_id[] = { 0, 1, 4, 5, 6, 7, 2 };
    // this is used to verify data consistency

    switch(diag_bus.state)
    {
        case DIAG_RESET:                        // 1st RESET state
            // Set QSPI_CLK high, start of RESET signal
            // Select branch A and branch B and set clk high to trigger reset
            QSM_PORTQS_P |= (QSM_PORTQS_CLK_MASK8 | QSM_PORTQS_DIAGBUSA_MASK8 | QSM_PORTQS_DIAGBUSB_MASK8);

            diag_bus.state = DIAG_START;        // DIAG_RESET state is done
            break;

        case DIAG_START:                        // 2nd RESET state
            // more than 15.7us has passed with QSPI_CLK in HIGH, so the DIM board already done the RESET cmd
            // we can pass to the low state for the LOAD cmd

            // when QSPI_CLK goes low the DIM starts checking the line, if QSPI_CLK remains low at least 15.7us
            // this is recognised as a LOAD cmd, so the next DIM register gets ready to by transmitted with the next
            // 256 clk burst
            QSM_PORTQS_P = QSM_PORTQS_DIAGBUSA_MASK8;   // Clear reset, select branch A & ADC bank 0

            diag_bus.qspi_branch = 0;           // 0=A, 1=B
            diag_bus.scan_idx = 0;              // this is the ordinal of DIM internal register we are theoretically reading
            diag_bus.state    = DIAG_SCAN;      // DIAG_START state is done
            break;

        case DIAG_SCAN:                         // 3rd RESET state
            // at least 15.7us must passed with QSPI_CLK low after the last read (256 clk burst) for the LOAD cmd
            // to be recognised, so this acquire with this new 256 clk burst the next DIM register

            // Set READA/READB bit to fire the state machine which generates the 256 QSPI_CLK to shift
            // the active register in the 16 DIM boards chain
            QSM_SPCR1_P = DIAG_TRIGGER;

            diag_bus.state = DIAG_RUN;          // 1st register read is done
            break;

        case DIAG_RUN:                      // Read and restart scan
            // we assume that the buffer is filled by a previous READ action
            // so we save the buffer and trigg a new READ action
            data = QSM_RR_P;                // Read first word

            register_id_a = (data >> 12) & 0x7;     // Extract register ID number from top nibble

            if ( register_id_a != rx_order_to_register_id[diag_bus.scan_idx++] )
            {
                diag_bus.check_f = 1;           // Flag that the state should be checked

                diag_bus.n_resets++;
                if ( diag_bus.n_resets >= DIAG_MAX_RESETS )
                {
                    diag_bus.state = DIAG_FAILED;
//                  diag_bus.n_resets = DIAG_MAX_RESETS; // if we want to avoid the overflow
                }
                else
                {
                    // we know that QSPI_CLK is high so no need to go to DIAG_RESET to put it high
                    // ToDo: is this true for FGC2? it is not true for FGC3
                    diag_bus.state = DIAG_START;
                }
                break; // exit the switch()
            }

            // Copy data into buffer .data[] in the desired order
            diag_bus.data[IDtoIndexInData[register_id_a + diag_bus.qspi_branch]] = data;

            if ( register_id_a == 1 )           // If 2nd scan (channel 1 = Digital Bank B)
            {
                Check_FGC2_PSU(data);           // Check FGC/DCCT PSU status
            }

            if ( register_id_a == 2 )           // If 7th scan (channel 2 = trig counter)
            {
                diag_bus.scan_idx = 0;

                if ( !diag_bus.qspi_branch )        // If branch 0=A has been scanned
                {
                    diag_bus.qspi_branch = 8;   // ToDo: check if this can be 1=B, to be proper
                    MemCpyWords( (uint32_t) &diag_bus.reception_buffer[0], QSM_RR_32, 16);

                    // extra copy of register ID:1 Dig1 A to .data[11]
                    diag_bus.data[11] = diag_bus.reception_buffer[0][1];

                    QSM_PORTQS_P  = (QSM_PORTQS_DIAGBUSB_MASK8 | QSM_PORTQS_DIPSDIMAD3_MASK8); // Select branch B & ADC bank 1
                }
                else                    // else branch B has been scanned
                {
                    diag_bus.qspi_branch  = 0;      // 0=A
                    MemCpyWords( (uint32_t) &diag_bus.reception_buffer[1], QSM_RR_32, 16);

                            // extra copy of register ID:1 Dig1 B to .data[12]
                    diag_bus.data[12] = diag_bus.reception_buffer[1][1];

                    QSM_PORTQS_P      = QSM_PORTQS_DIAGBUSA_MASK8; // Select branch A & ADC bank 0
                    diag_bus.check_f  = 1;          // Flag that new data is available
                }
            }

            QSM_SPCR1_P = DIAG_TRIGGER;     // Start next scan
            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called from CheckStatus(), which is called when the boot is waiting for keyboard
  characters.  It will process the new DIPS-DIM data - analogue and digital.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch(diag_bus.state)
    {
        case DIAG_FAILED:
            stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched &= ~(FGC_LAT_FGC_PSU_FAIL | FGC_LAT_PSU_V_FAIL | FGC_LAT_VDC_FAIL | FGC_LAT_DCCT_PSU_STOP | FGC_LAT_DCCT_PSU_FAIL);
            stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched |= FGC_LAT_DIM_SYNC_FLT;
            break;

        case DIAG_RUN:
            if ( SM_UXTIME_P != diag_bus.ana_check_time)      // Limit checking rate to 1 Hz
            {
                DiagCheckBus(); // calculate the number of DIMs in the chain of each branch
                DiagCheckAna();
                diag_bus.ana_check_time = SM_UXTIME_P;      // Remember unix time for this check
            }
            break;
    }

    diag_bus.check_f = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagCheckBus(void)
/*---------------------------------------------------------------------------------------------------------*\
  calculate the number of DIMs in the chain of each branch

    FGC3  reception_buffer[2][7][16]  // [A,B][registers] for [16 DIM cards]
        the 7 registers are stored in this order
        0 - Dig0    DIM register ID:0
        1 - Dig1    DIM register ID:1
        2 - Ana0    DIM register ID:4
        3 - Ana1    DIM register ID:5
        4 - Ana2    DIM register ID:6
        5 - Ana3    DIM register ID:7
        6 - TrigCounter DIM register ID:2
        there is no ID:3

FGC2 note:
  This function will be called from DiagCheck() when new analogue data is ready to be processed.  This
  takes quite a bit of time because it uses floating point maths, so the function is only called at 1Hz.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      qspi_branch;    // 0=A, 1=B
    uint16_t      dim_board;
    uint16_t      number_of_boards_in_branch;
    uint16_t *    dim_register;

    for ( qspi_branch = 0; qspi_branch < 2; qspi_branch++ )
    {
        number_of_boards_in_branch = 0;
        dim_register = diag_bus.reception_buffer[qspi_branch];

        for ( dim_board = 1; dim_board < 16; dim_board++ )
        {
            dim_register++;
            if (  ( ( *dim_register >> 12) & 0x07 ) == 2  )
            {
                number_of_boards_in_branch++;
            }
        }
        diag_bus.n_dims[qspi_branch] = number_of_boards_in_branch;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagCheckAna(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called from DiagCheck() when new analogue data is ready to be processed.
  This takes quite a bit of time because it uses floating point maths, so the function is only called at 1Hz.

    FGC3  reception_buffer[2][7][16]  // [A,B][registers] for [16 DIM cards]
        the 7 registers are stored in this order
        0 - Dig0    DIM register ID:0
        1 - Dig1    DIM register ID:1
        2 - Ana0    DIM register ID:4
        3 - Ana1    DIM register ID:5
        4 - Ana2    DIM register ID:6
        5 - Ana3    DIM register ID:7
        6 - TrigCounter DIM register ID:2
        there is no ID:3

  FGC2:
  The DIPS board has an embedded DIM that is the first on both branch A and branch B.
  The analogue channels are different for the two scans (there are 8 channels in all)
  and are used to measure the voltage source output voltage (Vmeas) twice (x1 and x10)
  and the +5V and +/-15V PSU voltages for the FGC and the FGC's analogue interface.

\*---------------------------------------------------------------------------------------------------------*/
{
    FP32    r5p;
    FP32    r15p;
    FP32    r15n;

    // Calculate Vmeas
    diag_bus.ana[DIAG_VMEAS1]  = 5.117E-3 * (FP32)(0x800 - (int16_t)(diag_bus.data[1] & 0xFFF));
    diag_bus.ana[DIAG_VMEAS10] = 5.117E-4 * (FP32)(0x800 - (int16_t)(diag_bus.data[2] & 0xFFF));

    // Calculate FGC PSU voltage measurements

    r5p  = (FP32)(diag_bus.data[4] & 0xFFF);
    r15p = (FP32)(diag_bus.data[0] & 0xFFF);
    r15n = (FP32)(diag_bus.data[3] & 0xFFF);

    diag_bus.ana[DIAG_PSU5]   = 2.4414E-3 * r5p;
    diag_bus.ana[DIAG_PSU15P] = 7.3120E-3 * r15p;
    diag_bus.ana[DIAG_PSU15N] = 2.9282E-3 * r15n - 1.02280E-2 * r15p;

    if ( diag_bus.ana[DIAG_PSU5]   <  FGC_PSU_MIN_5V  ||
         diag_bus.ana[DIAG_PSU5]   >  FGC_PSU_MAX_5V  ||
         diag_bus.ana[DIAG_PSU15P] <  FGC_PSU_MIN_15V ||
         diag_bus.ana[DIAG_PSU15P] >  FGC_PSU_MAX_15V ||
         diag_bus.ana[DIAG_PSU15N] < -FGC_PSU_MAX_15V ||
         diag_bus.ana[DIAG_PSU15N] > -FGC_PSU_MIN_15V
       )
    {
        Set(stat_var.fgc_stat.class_data.CLASS_FIELD.st_warnings, FGC_WRN_FGC_HW);
        Set(stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched, FGC_LAT_PSU_V_FAIL);
    }

    // Calculate Analogue interface voltage measurements

    r5p  = (FP32)(diag_bus.data[6] & 0xFFF);
    r15p = (FP32)(diag_bus.data[5] & 0xFFF);
    r15n = (FP32)(diag_bus.data[7] & 0xFFF);

    diag_bus.ana[DIAG_ANA5]   = 2.5659E-3 * r5p;
    diag_bus.ana[DIAG_ANA15P] = 7.5860E-3 * r15p + 2.4952E-5 * r15n;
    diag_bus.ana[DIAG_ANA15N] = 2.9532E-3 * r15n - 1.0377E-2 * r15p;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: diag_boot.c
\*---------------------------------------------------------------------------------------------------------*/
