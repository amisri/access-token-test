/*---------------------------------------------------------------------------------------------------------*\
  File:     codes.c

  Purpose:  FGC Boot code update functions

  Notes:    These functions are shared by the FGC2 and FGC3 boot programs
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdbool.h>

#include <definfo.h>    // for FGC_CLASS_ID
#include <start_boot.h>     // for StartSwitchBoot()

#define CODES_GLOBALS           // for code global variable
#define CODES_FGC_GLOBALS       // for erase_dev[], program_dev[], write_dev[], check_dev[] global variables
#define CODES_HOLDER_GLOBALS    // for codes_holder_info[]

#include <codes.h>
#include <fieldbus_boot.h>      // For CLASS_FIELD
#include <codes_fgc.h>          // for CodesUpdateLast(), erase_dev[], program_dev[], write_dev[], check_dev[]
#include <codes_mcu.h>          // for SelectMcuDev()
#include <debug.h>              // for DEBUG_M16C62_STANDALONE
#include <codes_holder.h>
#include <memmap_mcu.h>         // Include memory map consts
#include <stdio.h>              // for fprintf(), fputs(), NULL
#include <fgc_consts_gen.h>         // Global FGC constants
#include <fgc_consts_gen.h>     // Global FGC constants
#include <term.h>               // for TERM_TX
#include <m16c62.h>
#include <iodefines.h>          // specific processor registers and constants
#include <menu.h>               // for MenuRspError(), menu global variable
#include <fieldbus_boot.h>      // for fieldbus, stat_var global variable
#include <main.h>               // for timeout_ms
#include <mem.h>
#include <mcu_dependent.h>      // for NVRAM_UNLOCK(), NVRAM_LOCK()
#include <shared_memory.h>      // for shared_mem global variable
#include <dev.h>                // for global variable dev
#include <crate_boot.h>         // for crate, crate_types[]
#include <fgc_runlog_entries.h> // For Runlog entries
#include <runlog_lib.h>
#include <analog.h>

/*---------------------------------------------------------------------------------------------------------*/
void CodesInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will check to see if any codes need to be updated from the network.  If so it will
  wait for the correct code(s) to be transmitted so that it can save them in the relevant flash device(s).
  If a new boot is received, it will trigger special handling to prevent the system being left without a
  valid boot code.
\*---------------------------------------------------------------------------------------------------------*/
{
    enum fgc_code_ids_short_list dev_idx;
    uint8_t * FAR                  dev_lock;

    // Find main program code and class IDs associated with the slot 5 interface type
    // Class 53 and 52 share the same slot5 interface, so we need information from nameDB
    // to choose the right one

    if(dev_name.class_id == 52 && dev.class_id == 53)
    {
        codes_holder_info[SHORT_LIST_CODE_MP_DSP].update.code_id = FGC_CODE_52_MAINPROG;
    }
    else
    {
        codes_holder_info[SHORT_LIST_CODE_MP_DSP].update.code_id = slot5_code_ids[dev.slot5_type_index];
    }

    //--- Prepare to receive code ---

    // previously this was done later in CodesUpdate()
    // but that can fail to pass when dev.n_rw_devs_to_update == 0
    // and the code. variable can be used later via the FieldBusRxCode() uninitialised!!!

    // this can be avoided if FieldbusRxCode() is called only when we want to receive 'codes'
    // like with reception == true or code.rx_mode != CODE_RX_NONE
    // being sure that CodesUpdate() was executed

    code.rx_active_f   = false;             // Reset reception active flag
    code.rx_dev_idx    = 0xFF;              // Set flash device index to "unknown"
    code.in_blk        = RXCODEBUF_A;       // Initialise code buffer pointers
    code.out_blk       = RXCODEBUF_A;
    code.next_in_blk   = RXCODEBUF_A + 1;

    // ---------------------------------------------------------

    dev.state         = RGLEDS_DCCT_GREEN_MASK16;               // boot state : code update
    SM_FLASHLOCKS_P   = 0;                                      // Reset number of locks counter in shared memory area

    menu.error_buf[0] = '\0';                           // Clear error msg that can be set by the self test functions

    // --- Check Flash Locks magic number and reset all locks if not present ---

    if (NVRAM_MAGIC_FP != 0x6651)               // If magic number is missing
    {
        SELFLASH(SEL_SR);                       // Map FRAM to page 7
        NVRAM_UNLOCK();
        MemSetWords(NVRAM_FLASHLOCKS_32, 0, NVRAM_FLASHLOCKS_W);// Erase Flash locks area of NVRAM
        NVRAM_MAGIC_FP = 0x6651;
        NVRAM_LOCK();
    }

    // --- Check and report codes installed in flash devices ---

    fprintf(TERM_TX, RSP_ID_NAME "\n", "ST", "Check installed codes");

    dev_lock = (uint8_t * FAR)NVRAM_FLASHLOCKS_32;

    for (dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
    {
        struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;

        if (code_info->label[1] == dev.device)   // BA and BB cannot be written while running
        {
            // from the boot in the same device
            code_info->read_only_f = true;
        }

        SELFLASH(SEL_SR);                       // Make FRAM visible in 7/F

        if (*(dev_lock++) != 0)
        {
            code_info->locked_f = true;
            (SM_FLASHLOCKS_P)++;
        }

        fprintf(TERM_TX, RSP_DATA ",%s    \r", code_info->label);

        // Check state of this device and fill the codes_holder_info[device].dev structure with the
        // info from the code already in FLASH
        check_dev[dev_idx](dev_idx);

        if (*menu.error_buf != 0)               // If an error was reported
        {
            fprintf(TERM_TX, RSP_ERROR ",%s\n", (char * FAR)menu.error_buf); // Write error report
            *menu.error_buf = '\0';             // Clear error
        }
    }

    CodesCheckUpdates();                    // Check which devices need updating

    if (dev.abort_f == 0)                   // If no abort (TEST LEDS or Ctrl-C)
    {
        // --- Copy boot to other flash if required ---

        if (((dev.device == 'A')
             && (codes_holder_info[SHORT_LIST_CODE_BTB].update_state == UPDATE_STATE_UPDATE)
             && (codes_holder_info[SHORT_LIST_CODE_BTB].update.crc == codes_holder_info[SHORT_LIST_CODE_BTA].calc_crc)
            )
            ||
            ((dev.device == 'B')
             && (codes_holder_info[SHORT_LIST_CODE_BTA].update_state == UPDATE_STATE_UPDATE)
             && (codes_holder_info[SHORT_LIST_CODE_BTA].update.crc == codes_holder_info[SHORT_LIST_CODE_BTB].calc_crc)
            )
           )
        {
            WriteMcuBootDev(dev.device == 'A' ? SHORT_LIST_CODE_BTB : SHORT_LIST_CODE_BTA); // Copy boot to flash
            CodesCheckUpdates();
        }

        // --- If codes to update then report to terminal the state of all the codes and update ---

        if (dev.n_rw_devs_to_update != 0)           // If writable devices need updating
        {
            fputs(RSP_LABEL "," TERM_UL "FLASH ZONE|      INSTALLED VERSION     |CODE STATE|  UPDATE VERSION  \n" TERM_NORMAL, TERM_TX);

            for (dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
            {
                struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;

                if ((code_info->update.version != 0) || (code_info->update_state == UPDATE_STATE_UPDATE))
                {
                    fputs(TERM_BOLD, TERM_TX);
                }

                fprintf(TERM_TX, RSP_DATA ",%2u:%-7s|%-17s:%10lu|%-10s|%-7s:%10lu\n" TERM_NORMAL,
                        (uint16_t)dev_idx,
                        code_info->label,
                        fgc_code_names[code_info->update.code_id],
                        code_info->dev.version,
                        TypeLabel(code_state, code_info->dev_state),
                        TypeLabel(update_state, code_info->update_state),
                        code_info->update.version);
            }

            fprintf(TERM_TX, RSP_ID_NAME "Writable devices:%u  Read-only devices:%u\n",
                    "ST",
                    "Update codes from Fieldbus - ",
                    dev.n_rw_devs_to_update, dev.n_ro_devs_to_update);

            CodesUpdate(CODE_RX_FIELDBUS);

            if (*menu.error_buf != 0)                       // If error reported
            {
                fprintf(TERM_TX, RSP_ERROR ",%s\n", (char * FAR)menu.error_buf); // Write error report
            }
            else
            {
                fputs(RSP_OK "\r", TERM_TX);
            }
        }

        if ((dev.n_ro_devs_to_update != 0)                  // read-only devices need updating
            && ((dev.rst_src & CPU_RESET_SRC_SLOWWD_MASK16) == 0)      // previous reset wasn't a slow watchdog
           )
        {
            StartSwitchBoot();                      // Switch to other boot
        }
    }

    if (dev.n_devs_corrupted != 0)
    {
        stat_var.fgc_stat.class_data.CLASS_FIELD.st_faults  |= FGC_FLT_FGC_HW;
        stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched |= FGC_LAT_CODE_FLT;
    }
    else
    {
        if ((dev.n_rw_devs_to_update != 0)
            || (dev.n_ro_devs_to_update != 0)
           )
        {
            stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched |= FGC_LAT_CODE_FLT;
        }
    }

    // --- Extract name and class from NameDB if fieldbus address is available and check class_id ---

    if (DevIdValid())
    {
        // if the class according to the name (from gateway)
        // is different that the class according to the FGC hardware

        if (dev_name.class_id != dev.class_id && dev_name.class_id != 52 && dev.class_id != 53)
        {
            if (dev_name.class_id == FGC_CLASS_UNKNOWN)
            {
                fprintf(TERM_TX, RSP_ERROR ",No device expected at fieldbus address %u\n", dev.fieldbus_id);
            }
            else
            {
                // ToDo: may be good to report in the runlog
                fprintf(TERM_TX, RSP_ERROR ",FGC class_id is %u, gateway expects %u\n",
                        dev.class_id,
                        dev_name.class_id);

                stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched |= FGC_LAT_CODE_FLT;
            }

            stat_var.fgc_stat.class_data.CLASS_FIELD.st_faults |= FGC_FLT_FGC_HW;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CodesInitAdvTable(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the fieldbus is initialised to mark all the advertised codes as NOT_IN_USE.
  This is because the gateway may have fewer slots than the maximum allowed in the FGC.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  slot;

    for (slot = 0; slot < FGC_CODE_MAX_SLOTS; slot++)
    {
        code.adv[slot].code_id = FGC_CODE_NULL;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CodesCheckUpdates(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will check all the installed codes against the advertised codes from the gateway.  It
  counts how many writable and read-only devices need updating and sets the update state for each device.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t                           slot;
    uint16_t                          code_id;
    uint16_t                          dev_idx;

    code.request_mask       = 0;            // Reset code request mask (GW slots 0-31)
    dev.n_rw_devs_to_update = 0;            // Reset count of devices to be updated
    dev.n_ro_devs_to_update = 0;            // Reset count of read-only devices to be updated
    dev.n_devs_corrupted    = 0;            // Reset count of corrupted devices

    // validate the information we receive from the Gateway PC
    // Check advertised code IDs and lengths and set the states
    for (slot = 0; slot < FGC_CODE_MAX_SLOTS; slot++)   // For each advertised code
    {
        code_id = code.adv[slot].code_id;               // Get advertised code ID

        if (code_id == FGC_CODE_NULL)                   // If code ID is NULL (0xFF)
        {
            code.state[slot] = ADV_CODE_STATE_NOT_IN_USE;           // Set state NOT_IN_USE
        }
        else
        {
            if (code_id >= FGC_NUM_CODES)               // else if CODE ID is not valid
            {
                code.state[slot] = ADV_CODE_STATE_UNKNOWN_CODE; // Set state BAD_CODE
            }
            else                            // else code ID is valid
            {
                if (code.adv[slot].len_blks == fgc_code_sizes_blks[code_id])    // If length is correct
                {
                    code.state[slot] = ADV_CODE_STATE_OK;       // Set state OK
                }
                else                            // else length is wrong
                {
                    code.state[slot] = ADV_CODE_STATE_BAD_LEN;  // Set state BAD_LEN
                }
            }
        }
    }

    // Check each of our code holders, if they are accessible and up to date
    for (dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
    {
        struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;

        // fill with default states
        code_info->update.version    = 0;
        code_info->update.crc        = 0;
        code_info->update_state      = UPDATE_STATE_OK;

        if ((code_info->dev_state == CODE_STATE_CORRUPTED)           // If device is corrupted or
            || (code_info->dev_state == CODE_STATE_DEV_FAILED))     // failed
        {
            dev.n_devs_corrupted++;                 // Increment corrupted counter
        }

        if (code_info->locked_f == true) // If device is locked
        {
            code_info->update_state = UPDATE_STATE_LOCKED;
            continue;                           // Prevent update
        }

        if (code_info->read_only_f == true)             // If device is read only
        {
            code_info->update_state = UPDATE_STATE_NOT_AVL;
        }

        // look in the previously validated slots if there is some code that match for this code holder
        for (slot = 0; slot < FGC_CODE_MAX_SLOTS; slot++)
        {
            struct advertised_code_info * code_adv = code.adv + slot;

            if(code_info->update.code_id == code_adv->code_id)
            {
                if ((code.state[slot] == ADV_CODE_STATE_OK) &&
                        ( (code_info->dev_state == CODE_STATE_CORRUPTED) ||
                             ((code_info->dev_state == CODE_STATE_VALID) &&
                             ((code_info->calc_crc != code_adv->crc) ||
                              (code_adv->version != 0 && code_info->dev.version != code_adv->version)))
                        )
                   )
                {
                    code_info->update.version = code_adv->version;
                    code_info->update.crc     = code.adv[slot].crc;

                    if (code_info->read_only_f == true)
                    {
                        dev.n_ro_devs_to_update++;
                    }
                    else
                    {
                        dev.n_rw_devs_to_update++;
                        code_info->update_state = UPDATE_STATE_UPDATE;
                        code.request_mask |= (1L << slot);
                    }
                }

                break;
            }
        }
    }

    if (code.rx_mode == CODE_RX_FIELDBUS)       // If receiving code from the fieldbus
    {
        // Tell GW which codes are needed
        stat_var.fgc_stat.class_data.boot.code_request = code.request_mask;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CodesUpdate(uint16_t rx_mode)
/*---------------------------------------------------------------------------------------------------------*\
  This function will receive code blocks from the fieldbus or terminal interfaces and will process each block
  according to it's header: class_id, code_id, block_idx.
  When the rx_mode is TERMINAL, one code will be received and programmed.
  When rx_mode is FIELDBUS, all outstanding codes that require updating will be updated.
  This may take several revolutions of the gateway's code broadcast cycle if packets are dropped.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  blk_idx;

    //--- Prepare to receive code ---

    code.rx_active_f   = false;                 // Reset reception active flag
    code.rx_term_idx   = 0;                     // Reset terminal buffer index
    code.rx_dev_idx    = 0xFF;                  // Set flash device index to "unknown"
    code.n_blks_rcvd   = 0;                     // Reset number of blocks received counter
    code.n_blks_saved  = 0;                     // Reset number of blocks saved
    code.in_blk        = RXCODEBUF_A;           // Initialise code buffer pointers
    code.out_blk       = RXCODEBUF_A;
    code.next_in_blk   = RXCODEBUF_A + 1;
    code.rx_mode       = rx_mode;               // Set reception mode
    stat_var.fgc_stat.class_data.CLASS_FIELD.state_op = FGC_OP_PROGRAMMING;            // Set Operational State

    if (rx_mode == CODE_RX_FIELDBUS)                // If receiving code from the fieldbus
    {
        // Tell the gateway which codes are needed
        stat_var.fgc_stat.class_data.boot.code_request = code.request_mask;
    }

    //--- Loop to get and process code blocks ---

    while (CodesUpdateGetBlock() == 0)              // While a new code block has been received
    {
        blk_idx = code.out_blk->block_idx;          // Take local copy of new code block index

        if ((term.edit_state != 0)                  // If interactive
            && ((blk_idx % fieldbus.progress_refresh_rate) == 0)        // Periodically refresh progress
           )
        {
            fprintf(TERM_TX, "  %c %s:%u:%u%%        \r",   // Report block header on terminal
                    code.rx_active_f ? 'P' : 'W',
                    fgc_code_names[code.out_blk->code_id],
                    blk_idx,
                    (uint16_t)((uint32_t)blk_idx * 100L / fgc_code_sizes_blks[code.out_blk->code_id]));
        }

        if (blk_idx == 0)                           // If first block
        {
            CodesUpdateFirst();                     // Check the new block
        }

        if (code.rx_active_f == true)               // If code reception is (now) active
        {
            CodesUpdateBlock(blk_idx);              // Save the block to flash or sram for FGC3
        }

        if ((code.rx_active_f == true)              // If code reception is (still) active and
            && (blk_idx == code.rx_last_blk_idx)   // block is last for this code
           )
        {
            CodesUpdateLast();                      // Check code in flash
        }
    }

    stat_var.fgc_stat.class_data.boot.code_request = 0; // Clear code request mask to gateway

    // --- Check update results and set fault or warning if required ---

    if (dev.n_devs_corrupted != 0)
    {
        stat_var.fgc_stat.class_data.CLASS_FIELD.st_faults  |= FGC_FLT_FGC_HW;
        stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched |= FGC_LAT_CODE_FLT;
    }
    else
    {
        if (dev.n_rw_devs_to_update != 0)
        {
            stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched |= FGC_LAT_CODE_FLT;
        }
    }

    stat_var.fgc_stat.class_data.CLASS_FIELD.state_op = FGC_OP_BOOT;                    // Set Operational State
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CodesUpdateGetBlock(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will wait for a new code block.  If a block is received, the function will return zero with
  code.out_blk pointing to the new block.  If a block is not received for any reason (rx_mode is now NONE,
  or error/timeout/abort detected) then the function will return 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (code.rx_mode)                       // Wait according to reception mode
    {
        case CODE_RX_NONE:                      // Reception has been disabled

            code.rx_active_f = false;           // Reset reception active flag
            return (1);                         // Return 1 to end update

        case CODE_RX_FIELDBUS:                  // Reception from network interface

            // code.out_blk is externally modified by FipReadCodeVar() triggered by FipIsr()
            while ((dev.abort_f == 0)                   // Wait while abort flag (Ctrl-C) not active and
                   && (code.out_blk == code.in_blk)    // no new code block is available
                  )
            {
            }

            if (dev.abort_f != 0)                       // If user aborted reception (Ctrl-C)
            {
                MenuRspError("Update aborted by user"); // Report error
            }

            break;

        case CODE_RX_TERMINAL:                            // Reception from terminal

            // ATTENTION timeout_ms is a global variable decremented in the 1ms tick
            // Set timeout
            if (code.n_blks_rcvd != 0)
            {
                timeout_ms = CODE_TIMEOUT_RUN;
            }
            else                                        // else reception completed
            {
                timeout_ms = CODE_TIMEOUT_INIT;
            }

            while ((timeout_ms != 0)                    // Wait while timeout has not expired and
                   && (code.out_blk == code.in_blk)    // no new code block is available
                  )
            {
            }

            if (timeout_ms == 0)                        // If timeout expired
            {
                if ((code.rx_active_f == true)          // If reception in progress or
                    || (code.n_blks_saved == 0)     // never started
                   )
                {
                    MenuRspError("Timeout waiting for code from terminal");    // Report error
                }
                else                                    // else reception completed
                {
                    code.rx_mode = CODE_RX_NONE;        // Disable code reception
                    return (1);                         // End update
                }
            }

            break;
    }

    // --- For error reported ---

    if (*menu.error_buf != 0)                           // If error reported
    {
        if (code.rx_dev_idx < SHORT_LIST_CODE_LAST)     // If device index is valid
        {
            // Check state of this device and fill the codes_holder_info[device].dev structure with the
            // info from the code already in FLASH
            check_dev[code.rx_dev_idx](code.rx_dev_idx);
        }

        code.rx_active_f = false;                   // Reset reception active flag
        code.rx_mode     = CODE_RX_NONE;            // Disable code reception
        return (1);                     // End update
    }

    code.out_blk++;

    // Cast to 16 bit because RXCODEBUF has to be in default data page (0)
    if ((uint16_t) code.out_blk >= (RXCODEBUF_16 + RXCODEBUF_B))              // Adjust code buffer pointer
    {
        code.out_blk = RXCODEBUF_A;
    }

    code.n_blks_rcvd++;         // Increment blocks received counter
    return (0);             // Return 0 to process received block
}
/*---------------------------------------------------------------------------------------------------------*/
void CodesUpdateFirst(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will process the first block of a new code (blk_idx = 0).  If the function finds that
  there are no more codes to update, it resets the rx_mode to NONE.  If the new code needs to be received,
  it sets rx_active_f and prepares to receive the new code.
\*---------------------------------------------------------------------------------------------------------*/
{
    enum fgc_code_ids_short_list    dev_idx;


    if (code.rx_active_f == true)               // If code is being received
    {
        fprintf(TERM_TX, RSP_ERROR ",Programming %s abandoned - missed last block\n",
                codes_holder_info[code.rx_dev_idx].label);

        code.rx_active_f = false;           // Stop receiving this code
        // Check state of this device and fill the codes_holder_info[device].dev structure with the
        // info from the code already in FLASH
        check_dev[code.rx_dev_idx](code.rx_dev_idx);
    }

    if (code.rx_mode == CODE_RX_FIELDBUS)           // If reception mode is FIP/ETHERNET
    {
        CodesCheckUpdates();

        if (dev.n_rw_devs_to_update == 0)       // if no more codes to update
        {
            code.rx_mode = CODE_RX_NONE;        // Disable code reception
            return;
        }
    }

    for (dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)    // For all flash devices
    {
        // Local pointer to the current device code info (for efficiency on HC16)
        struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;

        // If device code_id matches first block's code_id
        if ( (code_info->update.code_id  == code.out_blk->code_id) &&
             (dev_idx != code.running_dev_idx) &&  // FGC2: and not running out of this device
             // and reception mode is TERMINAL and dev state is VALID or CORRUPTED (i.e. can be written)
             (  (   (code.rx_mode == CODE_RX_TERMINAL) &&
                    (code_info->dev_state == CODE_STATE_VALID ||
                     code_info->dev_state == CODE_STATE_CORRUPTED) )
                || // or reception mode is fieldbus and dev should be updated
                (    code.rx_mode == CODE_RX_FIELDBUS &&
                     code_info->update_state == UPDATE_STATE_UPDATE
                )
             )
              && (erase_dev[dev_idx](dev_idx) == 0)  // FGC2: and try to erase succeeded
        )
        {
            fprintf(TERM_TX, RSP_DATA ",Programming %s %lu into %s to replace %lu\n",
                    fgc_code_names[code_info->update.code_id],
                    code_info->update.version,
                    code_info->label,
                    code_info->dev.version);

            // Prepare to receive new code
            code.rx_active_f     = true;
            code.rx_dev_idx      = dev_idx;
            code.rx_class_id     = code.out_blk->class_id;
            code.rx_code_id      = code.out_blk->code_id;
            code.rx_prev_blk_idx = 0;
            code.rx_last_blk_idx = fgc_code_sizes_blks[code.rx_code_id] - 1;

            // clear the code request to the Gateway
            stat_var.fgc_stat.class_data.boot.code_request = 0;

            // return after first flash device is matched because when FGC2 is running from SRAM, both
            // BOOTA and BOOTB can match the first received block
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void CodesUpdateBlock(uint16_t blk_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will process a block of code during reception of the code.
\*---------------------------------------------------------------------------------------------------------*/
{
    if ((blk_idx != 0 && blk_idx <= code.rx_prev_blk_idx)  // If not first block & blk_idx hasn't increased
        || ((blk_idx == 0) && (code.rx_prev_blk_idx != 0)) // or it's a first block when not expected
        || (blk_idx  > code.rx_last_blk_idx)               // or blk_idx exceeds code length
        || (code.out_blk->class_id != code.rx_class_id)    // or class ID has changed
        || (code.out_blk->code_id  != code.rx_code_id)     // or code ID has changed
       )
    {
        fprintf(TERM_TX, RSP_ERROR ",Programming %s abandoned due to bad block header: %u:%u:%u:%u\n",
                codes_holder_info[code.rx_dev_idx].label,
                code.out_blk->class_id, code.out_blk->code_id, blk_idx, code.rx_prev_blk_idx);

        code.rx_active_f = false;               // Stop reception
        // Check state of this device and fill the codes_holder_info[device].dev structure with the
        // info from the code already in FLASH
        check_dev[code.rx_dev_idx](code.rx_dev_idx);
    }
    else
    {
        if (program_dev[code.rx_dev_idx](code.rx_dev_idx, code.out_blk) != 0)   // if programming of block fails
        {
            fprintf(TERM_TX, RSP_ERROR ",Programming %s abandoned due to flash programming failure\n",
                    codes_holder_info[code.rx_dev_idx].label);

            code.rx_active_f = false;               // Stop reception of this code
            // Check state of this device and fill the codes_holder_info[device].dev structure with the
            // info from the code already in FLASH
            check_dev[code.rx_dev_idx](code.rx_dev_idx);
        }
        else                            // else block stored OK
        {
            code.rx_prev_blk_idx = blk_idx;         // Remember block index
            code.n_blks_saved++;                    // Count blocks saved
        }
    }
}
// EOF
