/*---------------------------------------------------------------------------------------------------------*\
  File:         menuRunlog.c

  Purpose:      Functions for the Run Log submenu

  Author:       Quentin.King@cern.ch

  Notes:

  History:

    13/04/04    qak     Created
\*---------------------------------------------------------------------------------------------------------*/

#define MENURUNLOG_GLOBALS

#include <menuRunlog.h>

#include <fgc_runlog_entries.h> // Include run log definition, for FGC_RL_
#include <fgc_runlog.h>         // for FGC_RUNLOG_N_ELS
#include <stdio.h>              // for fprintf(), fputs()
#include <menu.h>               // for MenuRspError()
#include <dev.h>                // for dev global variable
#include <runlog_lib.h>         // for RunlogRead()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShowPrevRun(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show all run log entries in the previous run period
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint8_t       id = 0;

    for(i=0;i < FGC_RUNLOG_N_ELS && id != FGC_RL_RESETS;i++)
    {
        runlog_outp = MenuRunlogShow(runlog_outp,&id);

        if(dev.abort_f)
        {
            MenuRspError("Aborted by user");
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShowNewRun(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show all run log entries in the current run period
\*---------------------------------------------------------------------------------------------------------*/
{
    runlog_outp = NULL;                         // Reset global runlog output pointer
    MenuRunlogShowPrevRun(argc, argv);          // Display this run
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShowNext(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show 10 more entries
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;

    for(i=0;i < 10;i++)
    {
        runlog_outp = MenuRunlogShow(runlog_outp,NULL);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShowNew(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show 10 most recent entries
\*---------------------------------------------------------------------------------------------------------*/
{
    runlog_outp = NULL;                         // Reset global runlog output pointer
    MenuRunlogShowNext(argc, argv);             // Display 10 entries
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShowAll(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show all run log entries
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint8_t *     outp = NULL;

    for(i=0;i < FGC_RUNLOG_N_ELS;i++)
    {
        outp = MenuRunlogShow(outp,NULL);

        if(dev.abort_f)
        {
            MenuRspError("Aborted by user");
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint8_t * MenuRunlogShow(uint8_t *outp, uint8_t *id_p)
/*---------------------------------------------------------------------------------------------------------*\
  Get and show one runlog entry and return the id in *id if defined.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       seq;
    uint8_t       id;
    uint16_t      data[FGC_RUNLOG_MAX_DATA_LEN];


    outp = RunlogRead(outp, &seq, &id, data);                   // Read run log entry

    if(id_p)                                                    // If id_p defined
    {
        *id_p = id;                                                     // return id via the pointer
    }

    fprintf(TERM_TX, RSP_DATA ",0x%02X: 0x%02X : ",seq,id);     // Display record

    switch(fgc_rl_length[id])
    {
    case 0:                     // No data

        fputs("          ",TERM_TX);
        break;

    case 1:                     // One word

        fprintf(TERM_TX,"0x    %04X",data[0]);
        break;

    case 2:                     // Two words

        fprintf(TERM_TX,"0x%04X%04X",data[0],data[1]);
        break;
    }

    fprintf(TERM_TX, " : %s\n",fgc_rl_label[id]);

    return(outp);                                               // Return new out pointer
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuRunlog.c
\*---------------------------------------------------------------------------------------------------------*/
