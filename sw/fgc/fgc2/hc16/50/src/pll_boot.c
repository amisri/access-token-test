/*---------------------------------------------------------------------------------------------------------*\
  File:         pll_boot.c

 Purpose:   FGC MCU Software - RT Clock Phase Locked Loop Functions.

 Author:    Quentin.King@cern.ch

 Notes:     This file contains the functions to support synchronisation of the HC16 GPT clock with
            the WorldFIP cycle (driven by GPS time).

            Units:  tic HC16 GPT_TCNT clock period.  The clock prescalar is 4, so the
                    GPT_TCNT clock frequency is ~16MHz/4, and therefore 1 tic = ~0.25 us.

            qtic    tic/65536 = ~3.8 ps.  Many PLL variables are in qtics.
            qtic variables are long words with the most significant word being tics.
\*---------------------------------------------------------------------------------------------------------*/

#include <stdbool.h>
#include <memmap_mcu.h>             // Include memory map consts
#include <pll_boot.h>
#include <fieldbus_boot.h>          // for fieldbus, stat_var global variable
#include <macros.h>                 // for Test(), Set()
#include <mcu_dependent.h>          // for LED_SET(), LED_RST()
#include <fgc_runlog_entries.h>     // Include run log definition, for FGC_RL_
#include <runlog_lib.h>             // for RunlogWrite()
#include <mcu_time_consumed.h>
#include <shared_memory.h>          // for shared_mem global variable
#include <main.h>                   // for main_misc global variable

/*---------------------------------------------------------------------------------------------------------*/
void PllInit(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll = FGC_PLL_NO_SYNC;               // Reset PLL state

    pll.locked_detect = PLL_LOCKED_DETECT_FIPNET;               // Reset locked detect down counter
}
/*---------------------------------------------------------------------------------------------------------*/
void GatewayAliveAndPllAlgoBlocking(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called every millisecond from the 1ms Tick.
  It will check for a gateway timeout.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              pll_state;


    // check if we lost the gateway for a long time
    if ( fieldbus.gw_timeout_ms != 0 )
    {
        fieldbus.gw_timeout_ms--;
    }
    else
    {
        // we know we lost the gateway a long time ago
        // if it is the 1st time we enter here block the pll and put an entry in the runlog

        // to say GW is present 2 consecutive packets are required
        if ( fieldbus.is_gateway_online == true )
        {
            if ( stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED )     // If PLL is "locked"
            {
                pll.qtics_per_ms = pll.int_qtics_per_ms;                // Force period to PLL integrator value
            }
            else                                                // else PLL is not locked
            {
                pll.qtics_per_ms     = TICK_PERIOD << 16;       // Initialise PLL period
                pll.int_qtics_per_ms = pll.qtics_per_ms;// Initialise PLL integrator
            }

            fieldbus.is_gateway_online = false;                 // Clear GW active flag
            LED_SET(FIP_RED);                                   // Turn on red FIP LED

            pll_state = stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll = FGC_PLL_NO_SYNC;   // Reset PLL state
            pll.locked_detect = PLL_LOCKED_DETECT_FIPNET;       // Reset locked detect down counter

            RunlogWrite(FGC_RL_UXTIME, (void*)SM_UXTIME_16);        // Log Unix time
            RunlogWrite(FGC_RL_MSTIME, (void*)SM_MSTIME_16);        // Log Millisecond time
            RunlogWrite(FGC_RL_GW_STATE, &fieldbus.is_gateway_online);              // Log gw state in run log
            RunlogWrite(FGC_RL_PLL_STATE, &pll_state);              // Log PLL state in run log
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void PllAlgoWithFipSynchro(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from IsrFip() to run the PLL algorithm.
  The E register contains the value of GPT_TIC1, which is the time of the Fip IRQ.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              pll_state;

    asm
    {
        TED                                 // D = GPT_TIC1
        ADDD    #PLL_REF                    // pll.Terr = (int32_t)(GPT_TIC1 + PLL_REF - pll.ms_t0)
        SUBD    pll.ms_t0                   //             - TICK_PERIOD;
        CLRE
        SUBD    #TICK_PERIOD
        SBCE    #0
        STED    pll.Terr
        ADDD    pll.int_qtics_per_ms:2      // pll.qtics_per_ms = (pll.int_qtics_per_ms += Terr)
        ADCE    pll.int_qtics_per_ms:0      //                    + (Terr * 256)
        STED    pll.int_qtics_per_ms
        LDED    pll.Terr:1
        CLRB
        ADDD    pll.int_qtics_per_ms:2
        ADCE    pll.int_qtics_per_ms:0
        STED    pll.qtics_per_ms

        LDD     pll.Terr:2                  // Take LSW
        BPL     pos_err
        NEGD
    pos_err:
        CPD     #PLL_LOCKED_LIMIT_FIPNET    // |Terr| > PLL_LOCKED_LIMIT_FIPNET
        BCC     capture
    }


    {
        if ( pll.locked_detect != 0 )       // If not locked yet
        {
            if ( --pll.locked_detect == 0 )                        // If now locked
            {
                LED_RST(FIP_RED);                                  // Turn off red FIP LED
                stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll = FGC_PLL_LOCKED;                             // Set PLL as locked (byte)
                pll_state = FGC_PLL_LOCKED;                        // (Word - reqd below)

                RunlogWrite(FGC_RL_UXTIME, (void*)SM_UXTIME_16);   // Log Unix time
                RunlogWrite(FGC_RL_MSTIME, (void*)SM_MSTIME_16);   // Log ms time
                RunlogWrite(FGC_RL_PLL_STATE, &pll_state);         // Log new PLL state
            }
        }
        return;
    }

    asm
    {
        capture:
    }

    {
        if ( stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll != FGC_PLL_CAPTURE )                     // If PLL state is not "capture"
        {
            LED_SET(FIP_RED);                                           // Turn on red FIP LED
            pll_state = FGC_PLL_CAPTURE;                                // Set PLL as capture (byte)
            stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll = FGC_PLL_CAPTURE;                                // (Word - reqd below)
            pll.locked_detect = PLL_LOCKED_DETECT_FIPNET;                      // Reset locked detect down counter
            RunlogWrite(FGC_RL_UXTIME, (void*)SM_UXTIME_16);       // Log Unix time
            RunlogWrite(FGC_RL_MSTIME, (void*)SM_MSTIME_16);       // Log Millisecond time
            RunlogWrite(FGC_RL_PLL_STATE, &pll_state);             // Log new PLL state in runlog
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pll_boot.c
\*---------------------------------------------------------------------------------------------------------*/
