/*---------------------------------------------------------------------------------------------------------*\
  File:     codes_m16c62.c

  Purpose:  FGC3 Boot code update functions

\*---------------------------------------------------------------------------------------------------------*/

#define BIG_ENDIAN_OFFSET_TO_BYTE3 1

#include <stdint.h>
#include <stdio.h>

#include <codes_m16c62.h>
#include <codes_holder.h>       // for SHORT_LIST_CODE_PTDB, SHORT_LIST_CODE_IDPROG, SHORT_LIST_CODE_IDDB
#include <codes.h>
#include <fgc_consts_gen.h>         // Global FGC constants
#include <mem.h>                // for MemCpyBytes(), MemCpyWords()
#include <m16c62.h>             // for C62PowerOn()
#include <term.h>               // for TERM_TX
#include <menu.h>               // for MenuRspError(), menu global variable
#include <codes_fgc.h>

/*---------------------------------------------------------------------------------------------------------*/
uint16_t ProgramC62Dev(enum fgc_code_ids_short_list dev_idx, struct code_block *block)
/*---------------------------------------------------------------------------------------------------------*\
  This function will program the C62 flash.  This requires buffering of 32 byte code blocks because the
  C62 only accepts sectors of 256 bytes for programming.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      block_address;
    uint32_t      flash_address;

    if(SelectM16C62Dev(dev_idx, NULL))
    {
        c62.code_address = 0;
        return(1);
    }

    flash_address  = codes_holder_info[dev_idx].base_addr +                  // Calculate flash address
                     ((uint32_t)block->block_idx * FGC_CODE_BLK_SIZE);
    block_address  = flash_address & 0x00FF;                            // Get address within sector
    flash_address &= 0x000FFF00;                                        // Get address of start of sector

    if(c62.code_address &&                      // If buffer has some code already in it, and
       flash_address != c62.code_address &&     // the new block is in a different sector, and
       C62FlashCode())                          // flashing of sector fails
    {
        return(1);                                      // Return immediately
    }

    if(!c62.code_address)                       // If buffer is empty
    {
        MemSetBytes(c62.code_buf, 0xFF, 256);           // Prepare code buffer with all 0xFF's
        c62.code_address = flash_address;
    }

    MemCpyBytes( c62.code_buf+block_address, (uint8_t *) (block->code), 32);

    if(block_address == (0x100 - FGC_CODE_BLK_SIZE) &&  // If block is last in the sector, and
       C62FlashCode())                                  // flashing of sector fails
    {
        return(1);                                              // Return reporting error
    }
    return(0);                                   // Return 0 (success)
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t EraseC62Dev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will erase M16C62 flash blocks.

  This routine calls SelectM16C62Dev() to retrieve the list of block to erase.
  ToDo: this must be done in an explicit way or better arrange in some way in codes_holder_info[]

\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t erase_mask;

    if ( SelectM16C62Dev(dev_idx, &erase_mask) != 0 )     // If device not available
    {
        return(1);                          // Return immediately
    }

    if (    (C62SendIsrCmd(C62_SET_BLOCK_MASK, 1, 0, &erase_mask) != 0) // If set erase block mask fails, or
         || (C62SendBgCmd(C62_ERASE_FLASH, 1000) != 0) )        // erase of blocks fails, timeout 1000ms
    {
        codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;   // Set dev state to DEV_FAILED

        if ( *menu.error_buf != 0 )                 // If an error was reported
        {
            fprintf(TERM_TX,RSP_ERROR ",%s:%s\n",           // Write error report
                    codes_holder_info[dev_idx].label,(char * FAR)menu.error_buf);
            *menu.error_buf = '\0';                 // Clear error buffer
        }

        return(1);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t CheckC62Dev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will check the code in an M16C62 flash device.  It return 0 if the code is valid, 1 otherwise.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t          flash_address;
    uint32_t          code_info_addr;
    uint32_t          len;
    uint16_t          stat;
    uint32_t * code_version;

    // Clear code info

    codes_holder_info[dev_idx].dev.class_id = 0;
    codes_holder_info[dev_idx].dev.code_id  = 0;
    codes_holder_info[dev_idx].dev.version  = 0;
    codes_holder_info[dev_idx].dev.crc      = 0;
    codes_holder_info[dev_idx].calc_crc     = 0;

    if ( SelectM16C62Dev(dev_idx, NULL) != 0 )
    {
        return(1);                          // Return immediately
    }

    // the code info is stored after the "usable" data, appended at the end, by Code Delivery Software
    // so with the len we can calculate were it starts

    len = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE; // Code length in bytes

    flash_address  = codes_holder_info[dev_idx].base_addr;
    code_info_addr = flash_address + len - FGC_CODE_INFO_SIZE;

    c62.rsp_bytes_in_buffer = FGC_CODE_INFO_SIZE;

    stat = C62SendIsrCmd(C62_SET_ADDRESS, 3, BIG_ENDIAN_OFFSET_TO_BYTE3, (uint8_t *) &code_info_addr);

    if ( stat == 0 )                            // if OK
    {
        stat = C62GetRsp( (uint8_t *) &(c62.rsp.bytes), (uint16_t *) &(c62.rsp_bytes_in_buffer) );

        if ( stat == 0 )                        // if OK
        {
            if ( c62.rsp_bytes_in_buffer != FGC_CODE_INFO_SIZE )
            {
                stat = 1;
            }
        }
    }

    if ( stat != 0 )                            // if not OK
    {
        codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;   // Set dev state to DEV_FAILED
        return(1);
    }

    codes_holder_info[dev_idx].dev.class_id = c62.rsp.code_info.class_id;       // Copy code info to device structure
    codes_holder_info[dev_idx].dev.code_id  = c62.rsp.code_info.code_id;
    codes_holder_info[dev_idx].dev.version  = c62.rsp.code_info.version;
    codes_holder_info[dev_idx].dev.crc      = c62.rsp.code_info.crc;
    codes_holder_info[dev_idx].calc_crc     = c62.rsp.code_info.crc;            // Preset calculated CRC to info CRC

    // Check if the code is visible for M16C62
    // TODO Move the check to the beginning of the function and replace the check in SelectM16C62Dev

    if (CodesGetCodeVisibility(dev_idx) == CODES_VISIBILITY_M16C62)
    {
        // Save the code version in the shared memory

        code_version = CodesGetCodeVersionPointer(dev_idx);

        if (code_version)
        {
            *code_version = c62.rsp.code_info.version;
        }
    }

    stat = C62SendIsrCmd(C62_SET_ADDRESS, 3, BIG_ENDIAN_OFFSET_TO_BYTE3, (uint8_t*) &flash_address); // set block address
    if ( stat == 0 )                            // if OK
    {
        stat = C62SendIsrCmd(C62_SET_CRC_LEN, 3, BIG_ENDIAN_OFFSET_TO_BYTE3, (uint8_t*) &len);    // set of block lengths
        if ( stat == 0 )                        // if OK
        {
            stat = C62SendBgCmd(C62_CALC_CRC, 1500);
//          measures with FGC3
//          crc for PTDB     52ms
//          crc for IdProg  222ms
//          crc for IDDB   1310ms
        }
    }

    if ( stat != 0 )
    {
        codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;   // by default, later can be CODE_STATE_CORRUPTED

        if ( stat == M16C62_CRC_FAILED )                    // If C62 reports CRC failure
        {
            codes_holder_info[dev_idx].calc_crc  = c62.crc_calc;
            codes_holder_info[dev_idx].dev_state = CODE_STATE_CORRUPTED;
        }
        return(1);
    }

    codes_holder_info[dev_idx].dev_state = CODE_STATE_VALID;
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SelectM16C62Dev(enum fgc_code_ids_short_list dev_idx, uint8_t *block_mask_p)
/*---------------------------------------------------------------------------------------------------------*\
  This function will select the M16C62 flash device.
  The function returns: 0 = Read/Write  2 = Not visible

  Additionally it fills the erase_mask (block_mask_p)
  usually each code is contained in 1 block but IDDB uses 3 blocks so EraseC62Dev() call this routine to
  get which are the 3 block to erase

  ToDo: find another solution for EraseC62Dev() get the block erase info

\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       block_mask;

#ifndef DEBUG_M16C62_STANDALONE
    if ( (C62_P & C62_STANDALONE_MASK16) != 0 ) // If M16C62 is in standalone
    {
        codes_holder_info[dev_idx].dev_state = CODE_STATE_NOT_AVL;  // Set dev state to NOT AVAILABLE
        return(2);
    }
#endif

    //  ToDo: try arrange this data in codes_holder_info[]
    switch(dev_idx)
    {
       case SHORT_LIST_CODE_PTDB:          // placed in M16C62 block 1 (Part Type DB)

           block_mask  = C62_BLK_PT_DB;
           break;

       case SHORT_LIST_CODE_IDPROG:            // placed in M16C62 block 3 (IdProg)
           block_mask  = C62_BLK_ID_PROG;
           break;

       case SHORT_LIST_CODE_IDDB:          // placed in M16C62 blocks 4,5,6 (ID DB)
           block_mask  = C62_BLK_ID_DB;
           break;

       default:                // Unknown device
           return(2);
    }

    if ( block_mask_p != 0 )
    {
        *block_mask_p = block_mask;
    }

// while debugging, we need the PowerOn(), not to put M16C62 on but to set correctly C62_SET_LITTLE_ENDIAN
#ifndef DEBUG_M16C62_STANDALONE
    if ( (C62_P & C62_POWER_MASK16) == 0 )  // If M16C62 is off
#endif
    {
        // when M16C62 boots if bus_initial_value is C62_SWITCH_PROG the M16C62 will remain in IdBoot
    // otherwise it will run IdProg which in turn will run the its monitor
        if ( C62PowerOn(C62_SWITCH_PROG) != 0 ) // If an attempt to start IdBoot fails
        {
            codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;   // Set dev state to DEV_FAILED

            if ( *menu.error_buf != 0 )                 // If an error was reported
            {
                fprintf(TERM_TX, RSP_ERROR ",%s:%s\n",              // Write error report
                        codes_holder_info[dev_idx].label,(char * FAR) menu.error_buf );
                *menu.error_buf = '\0';                     // Clear error buffer
            }
            return(2);
        }
    }
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_m16c62.c
\*---------------------------------------------------------------------------------------------------------*/
