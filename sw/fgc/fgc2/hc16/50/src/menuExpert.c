/*---------------------------------------------------------------------------------------------------------*\
  File:         menuExpert.c

  Purpose:      Functions for the Expert submenu

  Author:       Quentin.King@cern.ch

  Notes:

  History:

    13/04/04    qak     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdbool.h>
#include <mcu_dependent.h>      // for MSLEEP(), POWER_CYCLE()
#include <main.h>               // for timeout_ms global variable used by MSLEEP()
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <menu.h>               // for MenuRspArg(), MenuGetInt16U(), MenuRspError()
#include <registers.h>          // for RegRead()
#include <fieldbus_boot.h>      // for stat_var global variable
#include <start_boot.h>         // for StartSwitchBoot()
#include <defconst.h>           // for FGC_FLT_, FGC_LAT_
#include <memmap_mcu.h>         // for CPU_PWRCYC_P
#include <dev.h>                // for dev global variable
#include <runlog_lib.h>         // for RunlogSaveResets()
#include <mem.h>                // for MemSetWords()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertResetFaults(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Reset faults and latched status
\*---------------------------------------------------------------------------------------------------------*/
{
    stat_var.fgc_stat.class_data.c50.st_faults  = FGC_FLT_FGC_STATE;
    stat_var.fgc_stat.class_data.c50.st_latched = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertSwitchBoot(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Switch to boot in the other boot device (BA0 or BB0) after a 1s delay.
\*---------------------------------------------------------------------------------------------------------*/
{
    StartSwitchBoot();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertPowerCycle(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Trigger a FGC power cycle using the NDI power cycle register
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_ENTER_CRITICAL();                // Stop interrupts to prevent IsrMst() from writing LEDs register
                                        // and there for losing power cycle request (same location)
    POWER_CYCLE();                      // Write power cycle request to the NDI register

    for(;;);                            // Wait for power cycle to act with interrupts disabled
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertGlobalReset(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show 15 most recent entries
\*---------------------------------------------------------------------------------------------------------*/
{
    CPU_RESET_P |= CPU_RESET_CTRL_GLOBAL_MASK16;                // Trigger global reset
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertSlowWatchdog(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Toggle the slow watchdog trigger enable.  When zero, the slow watchdog will not be triggered and after
  about 6 seconds the FGC will reset and switch to boot B, or if already running boot B, it will power
  cycle.
\*---------------------------------------------------------------------------------------------------------*/
{
    dev.enable_slow_wd = 1 - dev.enable_slow_wd;

    MenuRspArg("%u",dev.enable_slow_wd);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertFastWatchdog(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Toggle the fast watchdog trigger enable.  Once activated, it arms and should be triggers stop, the
  fast watchdog will act.
\*---------------------------------------------------------------------------------------------------------*/
{
    dev.enable_fast_wd = 1 - dev.enable_fast_wd;                // Toggle watchdog enable

    MSLEEP(5);                                                  // Wait 5 ms for watchdog to arm

    MenuRspArg("%u",!TST_CPU_MODEL(WDNOTRD));                   // Report watchdog ready state
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertBusError(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set known values in the HC16 registers and then trigger a bus error in order to test
  the trap function and the run log.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_ENTER_CRITICAL();

    asm
    {
        PSHM    D,E,X,Y,Z,K                     // Save all registers

        LDE     #0xDEAD                         // E  = DEAD
        LDAB    #0                              // EK = 0
        TBEK
        LDAB    #1                              // XK = 1
        TBXK
        LDX     #0x2345                         // XK:IX = 12345
        LDAB    #2                              // YK = 2
        TBYK
        LDY     #0x3456                         // YK:IY = 23456
        LDAB    #3                              // ZK = 3
        TBZK
        LDZ     #0x4567                         // ZK:IZ = 34567

        CLRW    CPU_BUSERR_16                   // Write to bus error register to trigger bus error

        PULM    D,E,X,Y,Z,K                     // Restore all registers in case bus error fails to occur
    }

    OS_EXIT_CRITICAL();

    MenuRspError("Bus error register failed");
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertIllegalInstruction(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function includes an illegal instruction to test the illegal instruction trap.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_ENTER_CRITICAL();

    asm
    {
        PSHM    D,E,X,Y,Z,K                     // Save all registers

        LDE     #0xDEAD                         // E  = DEAD
        LDAB    #0                              // EK = 0
        TBEK
        LDAB    #4                              // XK = 4
        TBXK
        LDX     #0x5678                         // XK:IX = 45678
        LDAB    #5                              // YK = 5
        TBYK
        LDY     #0x6789                         // YK:IY = 56789
        LDAB    #6                              // ZK = 6
        TBZK
        LDZ     #0x789A                         // ZK:IZ = 6789A

        DCW     0xFFFF                          // Illegal instruction

        PULM    D,E,X,Y,Z,K                     // Restore all registers in case bus error fails to occur
    }

    MenuRspError("Illegal instruction failed");

    OS_EXIT_CRITICAL();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertZeroResets(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Zero reset counters
\*---------------------------------------------------------------------------------------------------------*/
{
    RunlogSaveResets(true);                     // Save reset counters with reset_f = true to zero them first
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertRunlogReset(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Reset run log
\*---------------------------------------------------------------------------------------------------------*/
{
    NVRAM_UNLOCK();
    NVRAM_RL_MAGIC_FP = 0;                      // Reset magic number

    RunlogInit();                               // Initialise run log
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertConfigReset(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Config reset
\*---------------------------------------------------------------------------------------------------------*/
{
    SELFLASH(SEL_SR);                           // Map FRAM to page 7
    NVRAM_UNLOCK();

    MemSetWords(NVSIDX_32, 0, NVSIDX_W);        // Erase NVS index area

    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertFlashLockReset(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Flash Lock reset - clear flashlock magic number so that locks are wiped after next reset
\*---------------------------------------------------------------------------------------------------------*/
{
    SELFLASH(SEL_SR);                           // Map FRAM to page 7
    NVRAM_UNLOCK();
    NVRAM_MAGIC_FP = 0;                         // Clear magic number
    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertFramReset(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  FRAM reset - Wipes everything!  Flash Locks, Core config, RunLog and NVS
\*---------------------------------------------------------------------------------------------------------*/
{
    SELFLASH(SEL_SR);                           // Map FRAM to page 7
    NVRAM_UNLOCK();

    MemSetWords(NVRAM_32, 0, NVRAM_W);          // Erase complete FRAM

    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertFramDislay(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t i;
    uint16_t value;
    uint16_t num_values = 16;
    uint16_t offset;
    uint32_t address;

    // Get address argument

    if(MenuGetInt16U(argv[0], 0, 0xFFFE, &offset) != 0)
    {
        return;
    }

    // If offset is not word aligned

    if((offset & 0x0001) != 0)
    {
        MenuRspError("Offset is not word-aligned");
        return;
    }

    // Check optional num_values arg

    if(*argv[1] && MenuGetInt16U(argv[1], 1, 0xFFFF, &num_values))
    {
        return;
    }

    address = NVRAM_32 + (uint32_t)offset;

    // Map FRAM to page 7

    SELFLASH(SEL_SR);

    for(i = 0; i < num_values; i++)
    {
        if(RegRead(address, &value, true) != 0)
        {
            MenuRspError("Bus error reading 0x%05lX", address);
            return;
        }

        MenuRspArg("0x%04X", value);

        address += 2;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuExpert.c
\*---------------------------------------------------------------------------------------------------------*/
