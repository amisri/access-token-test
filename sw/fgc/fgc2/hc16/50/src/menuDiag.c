/*---------------------------------------------------------------------------------------------------------*\
  File:     menuDiag.c

  Purpose:  Functions to run for the diag menu options

\*---------------------------------------------------------------------------------------------------------*/

#define MENUDIAG_GLOBALS

#include <stdint.h>

#include <menuDiag.h>

#include <menu.h>               // for MenuRspArg(), MenuRspError()
#include <stdio.h>              // for fprintf(), fputs()
#include <diag_boot.h>          // for diag_bus global variable
#include <term_boot.h>           // for TermPutc()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuDiagShow(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      qspi_branch;    // 0=A, 1=B
    uint16_t      qspi_board_number;
    uint16_t *    dim_data;

    fprintf(TERM_TX,RSP_DATA ",%u,%s,%u,%u",
            diag_bus.n_resets,
            TypeLabel(diag_ctrl_lbl,diag_bus.state),
            diag_bus.n_dims[0],
            diag_bus.n_dims[1]);

    for( qspi_branch = 0; qspi_branch < 2; qspi_branch++)
    {
        dim_data = diag_bus.reception_buffer[qspi_branch];

        for( qspi_board_number = 0; qspi_board_number < 16; qspi_board_number++)
        {
            if( !(qspi_board_number % 8) )
            {
                fputs("\n" RSP_DATA,TERM_TX);
            }

            fprintf(TERM_TX,",0x%04X",*(dim_data++));
        }
    }

    TermPutc('\n', TERM_TX);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDiagControl(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Enable or disable the diag bus operation
\*---------------------------------------------------------------------------------------------------------*/
{
    MenuGetInt16U(argv[0], 0, 1, &diag_bus.state);

    if ( diag_bus.state )          // If enabling the bus
    {
        diag_bus.n_resets = 0;          // Clear number of resets counter
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDiagSetMpx(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the QSPI mpx control lines (BusB:BusA).  This clears the clock and spare outputs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  mpx_ctrl;

    if ( !MenuGetInt16U(argv[0], 0, 3, &mpx_ctrl) )
    {
        QSM_PORTQS_P = (mpx_ctrl << 5); // Set port QS to specified mpx ctrl clearing clock and spare
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDiagSetSprClk(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set the QSPI Spare and Clock lines (Clk:Spr)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  spr_clk;

    if ( !MenuGetInt16U(argv[0], 0, 3, &spr_clk) )
    {
        QSM_PORTQS_P = (QSM_PORTQS_P & ~(QSM_PORTQS_CLK_MASK8|QSM_PORTQS_SPAREOUT_MASK8))|(spr_clk << 1);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDiagDisplayDipsRaw(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Report the contents of the DIPS channels
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  i;

    if ( diag_bus.state != DIAG_RUN )
    {
        MenuRspError("Diag not running");
        return;
    }

    for( i = 0; i < DIAG_BUF_LEN; i++ )
    {
        MenuRspArg("0x%04X",diag_bus.data[i]);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDiagDisplayDipsAna(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Report the analogue voltages read by the DIPS DIM.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  i;

    if ( diag_bus.state != DIAG_RUN )
    {
        MenuRspError("Diag not running");
        return;
    }

    for ( i = 0; i < DIAG_N_ANA; i++ )
    {
        MenuRspArg("%s,%.3f",TypeLabel(diag_ana_lbl,i),diag_bus.ana[i]);
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuDiag.c
\*---------------------------------------------------------------------------------------------------------*/
