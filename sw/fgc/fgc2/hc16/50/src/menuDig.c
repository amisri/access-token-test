/*---------------------------------------------------------------------------------------------------------*\
  File:         menuDig.c

  Purpose:      Functions to run for the DIG boot menu options

  Author:       Stephen.Page@cern.ch

  Notes:

  History:

    22/09/03    stp     Created
\*---------------------------------------------------------------------------------------------------------*/

#define MENUDIG_GLOBALS

#include <stdint.h>

#include <menuDig.h>

#include <stdbool.h>
#include <mcu_dependent.h>      // for MSLEEP(), USLEEP()
#include <main.h>               // for timeout_ms global variable used by MSLEEP()
#include <menu.h>               // for MenuRspArg(), MenuGetInt16U(), MenuRspError()
#include <memmap_mcu.h>         // for DIG_OP_P
#include <dev.h>                // for dev global variable
#include <crate_boot.h>         // for crate global variable
#include <registers.h>          // for RegRead()
#include <defconst.h>           // for FGC_CRATE_TYPE_

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning

/*---------------------------------------------------------------------------------------------------------*/
void MenuDigGet(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Get and return both output and input registers
\*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("0x%04X,0x%04X,0x%04X,0x%04X", DIG_OP_P, DIG_IP1_P, DIG_IP2_P, DIG_IPDIRECT_P);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDigSet(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set and readback output register
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t value;

    if(!MenuGetInt16U(argv[0], 0, 0xFFFF, &value))
    {
        DIG_OP_P = value;
        MenuRspArg("0x%04X", DIG_OP_P);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDigIP1(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Test IP1 register
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t value;

    if(RegRead(DIG_IP1_32, &value, true))
    {
        MenuRspError("Bus error reading Dig IP1");      // Report error
    }
    else
    {
        MenuRspArg("0x%04X", value);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDigIP2(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Test IP2 register
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t value;

    if(RegRead(DIG_IP2_32, &value, true))
    {
        MenuRspError("Bus error reading Dig IP2");      // Report error
    }
    else
    {
        MenuRspArg("0x%04X", value);
    }

    /* Configure serial data direction register */

    QSM_DDRQS_P |= 0x60;

    /* Check that internal links function correctly */

    QSM_PORTQS_P &= ~QSM_PORTQS_DIAGBUSA_MASK8;

    value = DIG_IP2_P;

    if(value & DIG_IP2_PCS2_MASK16)
    {
        MenuRspError("PCS2 stuck high");        // Report error
        return;
    }

    QSM_PORTQS_P |= QSM_PORTQS_DIAGBUSA_MASK8;

    value = DIG_IP2_P;

    if(!(value & DIG_IP2_PCS2_MASK16))
    {
        MenuRspError("PCS2 stuck low");         // Report error
        return;
    }
    MenuRspArg("0");

    QSM_PORTQS_P &= ~QSM_PORTQS_DIAGBUSB_MASK8;

    value = DIG_IP2_P;
    if(value & DIG_IP2_PCS3_MASK16)
    {
        MenuRspError("PCS3 stuck high");        // Report error
        return;
    }

    QSM_PORTQS_P |= QSM_PORTQS_DIAGBUSB_MASK8;

    value = DIG_IP2_P;
    if(!(value & DIG_IP2_PCS3_MASK16))
    {
        MenuRspError("PCS3 stuck low");         // Report error
        return;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDigOP(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  Test OP register
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t i;
    uint16_t value;
    uint16_t bustype;

    if(RegRead(DIG_OP_32, &value, true))
    {
        MenuRspError("Bus error reading Dig OP");       // Report error
    }
    else
    {
        MenuRspArg("0x%04X", value);
    }

    bustype = DIG_IP2_P & 0x1F;

    /* Perform pattern tests */

    for(i = 0 ; i < num_patterns_8 ; i++)
    {
        /* Reset all bits, then set pattern */

        DIG_OP_P = 0xFF00;
        DIG_OP_P = patterns_8[i];

        if( (bustype == 0x1D) || (bustype == 0x1E) )            // 0x1D: tester bustype
        {                                                       // 0x1E: reception racks bustype
            if(DIG_OP_P != patterns_8[i])
            {
                MenuRspError("Dig OP Readback error: exp:0x%02X got:0x%02X",
                        patterns_8[i],DIG_OP_P);
                return;
            }
        }
        else                                                    // other bustypes
        {
            if(DIG_OP_P != ((patterns_8[i] | 0x80) & 0xFE))
            {
                MenuRspError("Dig OP Readback error: exp:0x%02X got:0x%02X",
                        ((patterns_8[i] | 0x80) & 0xFE),DIG_OP_P);
                return;
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuDigTimeout(uint16_t argc, char **argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function tests the Digital interface watchdog timeout. It toggles the bit CMWDTRIG, checks that the
  timeout bit goes to 0 and get back to 1 after about 2 seconds. It fails if it is less than 1 or more
  than 3 seconds.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      cpt = 0;
    uint16_t      val;

    /* Test bus acces */

    if((dev.reg_r | dev.reg_w) & REG_DIG)
    {
        MenuRspError("No access to Dig register");
        return;
    }

    if(crate.type == FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC in reception crate, cannot test Dig timeout");
        return;
    }

    /* Toggle CMDWDTRIG */

    GPT_PDR_P ^= GPT_PDR_TRIGDIG_MASK8;
    GPT_PDR_P ^= GPT_PDR_TRIGDIG_MASK8;

    /* Wait for 1us */

    USLEEP(1);

    /* Check TIMEOUT bit goes down */

    val = DIG_IP1_P;

    if(val & DIG_IP1_TIMEOUT_MASK16)
    {
        MenuRspError("Dig Timeout bit high after trigger");
        return;
    }

    /* Check TIMEOUT bit goes back up after about 2 sec */

    do
    {
        MSLEEP(10);
        val = DIG_IP1_P;
    }
    while(!(val & DIG_IP1_TIMEOUT_MASK16) && (cpt++ < DIG_TIMEOUT_MAX));

    MenuRspArg("%u0", cpt);

    if((cpt >= DIG_TIMEOUT_MAX) || (cpt < DIG_TIMEOUT_MIN))
    {
        MenuRspError("Error Dig Timeout: %u0 ms (1-3 sec)", cpt);
    }
    return;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuDig.c
\*---------------------------------------------------------------------------------------------------------*/
