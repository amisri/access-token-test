/*---------------------------------------------------------------------------------------------------------*\
  File:     rad.c

  Purpose:  Functions for radiation tests.

  History:

    19 Aug 2003    sdg Created
\*---------------------------------------------------------------------------------------------------------*/

#define RAD_GLOBALS

#include <rad.h>
#include <stdio.h>              // for fprintf()
#include <term.h>               // for TERM_TX
#include <memmap_mcu.h>         // for SM_UXTIME_P
#include <analog.h>             // for menusd global variable
#include <dev.h>                // for dev global variable
#include <fgc_runlog_entries.h> // for FGC_RL_
#include <mcu_dependent.h>      // for MSLEEP()
#include <main.h>               // for timeout_ms global variable used by MSLEEP()
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <runlog_lib.h>         // for RunlogWrite()
#include <mem.h>                // for MemSetBytes()
#include <fieldbus_boot.h>      // for stat_var global variable
#include <macros.h>             // for Set(), Clr()
#include <defconst.h>           // for FGC_INTERFACE_
#include <stdlib.h>             // for labs()
#include <pll_main.h>
#include <fip.h>
#include <debug.h>


/*---------------------------------------------------------------------------------------------------------*/
void RadInitAnaAcq (void)
/*---------------------------------------------------------------------------------------------------------*\
  This function init value before ana acquisition.
\*---------------------------------------------------------------------------------------------------------*/
{
    menusd.a.max_h  = 0x8000;       // Most negative value
    menusd.a.max_m  = 0x0000;
    menusd.a.max_l  = 0x0000;
    menusd.a.min_h  = 0x7FFF;       // Most positive value
    menusd.a.min_m  = 0xFFFF;
    menusd.a.min_l  = 0xFFFF;
    menusd.a.sum[0] = 0;
    menusd.a.sum[1] = 0;
    menusd.a.sum[2] = 0;
    menusd.a.sum[3] = 0;

    menusd.b.max_h  = 0x8000;       // Most negative value
    menusd.b.max_m  = 0x0000;
    menusd.b.max_l  = 0x0000;
    menusd.b.min_h  = 0x7FFF;       // Most positive value
    menusd.b.min_m  = 0xFFFF;
    menusd.b.min_l  = 0xFFFF;
    menusd.b.sum[0] = 0;
    menusd.b.sum[1] = 0;
    menusd.b.sum[2] = 0;
    menusd.b.sum[3] = 0;

    menusd.sar_ave_ct   = 4;            // counter for 4 point average (SAR) reset

    // Set digital mpx on ADCA and ADCB, 500kHz filter
    AnaMpxDig(0,1);
    AnaFltFrq(0,0);
}
/*---------------------------------------------------------------------------------------------------------*/
 void RadInitAverageP2PRef (void)
/*---------------------------------------------------------------------------------------------------------*\
  This function init average reference value for the 10V, -10V and DAC.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  dac_value;

    RadInitAnaAcq();            // init ana value
    AnaMpxAna(7,4);         // +10V on channelA and -10V on channelB
    MSLEEP(500);

    // Calculation of the average values
    RadAverageP2PMeas(ANA_SAMPLES);

    pos10V_average_old  = adc_a_average_value;      // init meas +10V value
    neg10V_average_old  = adc_b_average_value;      // init meas -10V value
    pos10V_p2p_old  = adc_a_p2p_value;          // init p2p +10V value
    neg10V_p2p_old  = adc_b_p2p_value;          // init p2p -10V value
    adc_10Vpos_ref  = adc_a_average_value;      // save +10V reference
    adc_10Vneg_ref  = adc_b_average_value;      // save -10V reference
    fprintf(TERM_TX, RSP_DATA ",1,unix time,%lu,ref +10V:0x%08lX,ref -10V:0x%08lX\n",SM_UXTIME_P,adc_10Vpos_ref,adc_10Vneg_ref);


    RadInitAnaAcq();                    // init ana value
    AnaMpxAna(3,3);                 // DAC on channelA and channelB

    // Set DAC regiter
    dac_value = (uint32_t)(SLOT5_DAC_10V);

    dac_value <<= 12;                   // Value in DAC is left justified (bits 31-12)

    ANA_DAC_HIGH_P = ((uint16_t*)&dac_value)[0];
    ANA_DAC_LOW_P  = ((uint16_t*)&dac_value)[1];

    MSLEEP(2000);                   // Wait for 2s

    // Calculation of the average values
    RadAverageP2PMeas(ANA_SAMPLES);

    dac_a_average_old   = adc_a_average_value;      // init meas DAC value on channel A
    dac_b_average_old   = adc_b_average_value;      // init meas DAC value on channel B
    DAC_a_p2p_old   = adc_a_p2p_value;          // init meas DAC p2p on channel A
    DAC_b_p2p_old   = adc_b_p2p_value;          // init meas DAC p2p on channel B
    adc_a_dac_ref   = adc_a_average_value;      // save DAC reference on channel A
    adc_b_dac_ref   = adc_b_average_value;      // save DAC reference on channel B
    fprintf(TERM_TX, RSP_DATA ",2,unix time,%lu,ref DAC chanA:0x%08lX,ref DAC chanB:0x%08lX\n",SM_UXTIME_P,adc_a_dac_ref,adc_b_dac_ref);

}
/*---------------------------------------------------------------------------------------------------------*/
void RadAverageP2PMeas(uint16_t samples)
/*---------------------------------------------------------------------------------------------------------*\
  Measure of the p2p and average.
\*---------------------------------------------------------------------------------------------------------*/
{
    menusd.num_samples  = samples;

    // Wait for samples to be taken
    while(menusd.num_samples);

    // Process results
    AnaPP(&menusd.a);           // Calculate peak-peak value for channel A
    AnaPP(&menusd.b);           // Calculate peak-peak value for channel B


    switch(dev.slot5_type)
    {
        case FGC_INTERFACE_SD_350 :

            // Calculate average value for channel A and channel B
            adc_a_average_value = (int32_t)( ((uint32_t)menusd.a.sum[0] << 20) |
                      ((uint32_t)menusd.a.sum[1] << 4) |
                      ((uint32_t)menusd.a.sum[2] >> 12));

            adc_b_average_value = (int32_t)( ((uint32_t)menusd.b.sum[0] << 20) |
                      ((uint32_t)menusd.b.sum[1] << 4) |
                  ((uint32_t)menusd.b.sum[2] >> 12));

            adc_a_p2p_value =(int32_t)( ((uint32_t)menusd.a.p2p_h << 16) |
                          ((uint32_t)menusd.a.p2p_m));

            adc_b_p2p_value =(int32_t)( ((uint32_t)menusd.b.p2p_h << 16) |
                          ((uint32_t)menusd.b.p2p_m));
            break;

        case FGC_INTERFACE_SAR_400 :

            // Calculate p2p value for channel A and channel B
            menusd.a.p2p_l = (menusd.a.p2p_m << 14) | (menusd.a.p2p_l >> 2);
            menusd.a.p2p_m = (menusd.a.p2p_h << 14) | (menusd.a.p2p_m >> 2);
            menusd.a.p2p_h = menusd.a.p2p_h >> 2;

            menusd.b.p2p_l = (menusd.b.p2p_m << 14) | (menusd.b.p2p_l >> 2);
            menusd.b.p2p_m = (menusd.b.p2p_h << 14) | (menusd.b.p2p_m >> 2);
            menusd.b.p2p_h = menusd.b.p2p_h >> 2;

            adc_a_p2p_value =(int32_t)( ((uint32_t)menusd.a.p2p_h << 16) |
                          ((uint32_t)menusd.a.p2p_m));

            adc_b_p2p_value =(int32_t)( ((uint32_t)menusd.b.p2p_h << 16) |
                          ((uint32_t)menusd.b.p2p_m));

            // Calculate average value for channel A and channel B
            adc_a_average_value = (int32_t)( ((uint32_t)menusd.a.sum[0] << 22) |
                      ((uint32_t)menusd.a.sum[1] << 6) |
                      ((uint32_t)menusd.a.sum[2] >> 10));

            adc_b_average_value = (int32_t)( ((uint32_t)menusd.b.sum[0] << 22) |
                      ((uint32_t)menusd.b.sum[1] << 6) |
                      ((uint32_t)menusd.b.sum[2] >> 10));
            break;

        default :                   // problem with the software

            fprintf(TERM_TX,"\n" RSP_ERROR ",software corrupted : problem with ANA card type.\n");
            RadSendData (SEND_ALL);         // Send all datas
            MSLEEP(2000);               // Wait for 2s
            OS_ENTER_CRITICAL();            // Stop interrupts to prevent IsrMst() from writing LEDs register
                                // and there for losing power cycle request (same location)
            CPU_PWRCYC_P = 0xF00E;          // Write power cycle request to the NDI register

            for(;;);                // Wait for power cycle to act with interrupts disabled
            return;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint8_t RadCheckAverageP2PValue(uint16_t adc_p2p_tol, uint32_t adc_ave_tol,
                  uint32_t adc_ref_a_value, uint32_t adc_ref_b_value)
/*---------------------------------------------------------------------------------------------------------*\
  Test if the p2p and average value are in the limit.
\*---------------------------------------------------------------------------------------------------------*/
{

#ifndef DEBUG_NO_ADC_RESET
    uint8_t   adc_test_result = 0;

    // Check p2p noise, if problem p2p print the chanA and chanB values
    if((menusd.a.p2p_h != 0) | (menusd.a.p2p_m > adc_p2p_tol)
        | (menusd.b.p2p_h!= 0)|(menusd.b.p2p_m > adc_p2p_tol))
    {
        fprintf(TERM_TX,"\n" RSP_ERROR ",Chan A p2p %s:0x%04X%04X\n",
                SelectAna[ana_test_selection], menusd.a.p2p_h,
                menusd.a.p2p_m);

        fprintf(TERM_TX, RSP_ERROR ",Chan B p2p %s:0x%04X%04X\n",
                SelectAna[ana_test_selection+1], menusd.b.p2p_h,
                menusd.b.p2p_m);
        adc_test_result = 1;
    }

    // Check average value
    if(( labs(adc_a_average_value - adc_ref_a_value)> adc_ave_tol) ||
        (labs(adc_b_average_value - adc_ref_b_value) > adc_ave_tol))
    {
        fprintf(TERM_TX,"\n" RSP_ERROR ",Chan A average %s:0x%08lX,0x%08lX\n",
                SelectAna[ana_test_selection],
                adc_a_average_value,
                labs(adc_a_average_value -  adc_ref_a_value));

        fprintf(TERM_TX, RSP_ERROR ",Chan B average %s:0x%08lX,0x%08lX\n",
                SelectAna[ana_test_selection+1],
                adc_b_average_value,
                labs(adc_b_average_value -adc_ref_b_value));
        adc_test_result = 1;
    }
    return (adc_test_result);
#else
    return (0);
#endif
}
/*---------------------------------------------------------------------------------------------------------*/
void RadGetMultiSample(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets p2p and average ADCs values for 4096 samples.
\*---------------------------------------------------------------------------------------------------------*/

{
    uint8_t   test_result = 0;
    uint32_t  dac_value;
    uint16_t  V10_jump_tol;
    uint16_t  DAC_jump_tol;

    RadInitAnaAcq();                            // init ana value

    // selection the channel configuration
    switch(ana_test_selection)
    {
        case 0 :

            AnaMpxAna(7,4);                         // +10V on channelA and -10V on channelB
            MSLEEP(500);
            break;

        case 2 :

            AnaMpxAna(3,3);                         // DAC on channelA and channelB

            /* Set DAC regiter */
            dac_value = (uint32_t)(SLOT5_DAC_10V);
            dac_value <<= 12;                       // Value in DAC is left justified (bits 31-12)
            ANA_DAC_HIGH_P = ((uint16_t*)&dac_value)[0];
            ANA_DAC_LOW_P  = ((uint16_t*)&dac_value)[1];
            MSLEEP(2000);                           // Wait for 2s
            break;

        default :                               // problem with the software

            fprintf(TERM_TX,"\n" RSP_ERROR ",software corrupted : ana test selection problem.\n");
            RadSendData (SEND_ALL);                     // Send all datas
            MSLEEP(2000);                           // Wait for 2s
            OS_ENTER_CRITICAL();                        // Stop interrupts to prevent IsrMst() from writing LEDs register
                                        // and there for losing power cycle request (same location)
            CPU_PWRCYC_P = 0xF00E;                      // Write power cycle request to the NDI register

            for(;;);                            // Wait for power cycle to act with interrupts disabled
    }

    MSLEEP(500);


    // Calculation of the P2P and average values
    RadAverageP2PMeas(ANA_SAMPLES);


    // selection the channel configuration
    switch(ana_test_selection)
    {
        case 0 :

                // selection the card type : SD or SAR
            switch(dev.slot5_type)
            {
                case FGC_INTERFACE_SD_350 :                     // SD card

                    test_result = RadCheckAverageP2PValue(ADC_P2P_10V_TOL_SD, ADC_AVE_10V_TOL_SD,
                                          adc_10Vpos_ref, adc_10Vneg_ref);
                    V10_jump_tol = V10_JUMP_TOL_SD;
                    nb_sample_before_stuck = NB_SAMPLE_STUCK_SD;
                    break;

                case FGC_INTERFACE_SAR_400 :                        // SAR card

                    test_result = RadCheckAverageP2PValue(ADC_P2P_10V_TOL_SAR, ADC_AVE_10V_TOL_SAR,
                                          adc_10Vpos_ref, adc_10Vneg_ref);
                    V10_jump_tol = V10_JUMP_TOL_SAR;
                    nb_sample_before_stuck = NB_SAMPLE_STUCK_SAR;
                    break;

                default :                           // problem with the software

                    fprintf(TERM_TX,"\n" RSP_ERROR ",software corrupted : ana test selection problem.\n");
                    RadSendData (SEND_ALL);                     // Send all datas
                    MSLEEP(2000);                       // Wait for 2s
                    OS_ENTER_CRITICAL();                    // Stop interrupts to prevent IsrMst() from writing LEDs register
                                            // and there for losing power cycle request (same location)
                    CPU_PWRCYC_P = 0xF00E;                  // Write power cycle request to the NDI register

                    for(;;);                            // Wait for power cycle to act with interrupts disabled
            }

            // store values if was a problem
            if (test_result == 1)
            {
                if ( V10_array_index < MAX_ADC_ARRAY_SIZE)          // Test if it's the last line of the array
                {
                    adc_10V_meas[V10_array_index].unix_time  = SM_UXTIME_P;
                    adc_10V_meas[V10_array_index].ave_10Vpos = adc_a_average_value;
                    adc_10V_meas[V10_array_index].ave_10Vneg = adc_b_average_value;
                    adc_10V_meas[V10_array_index].p2p_10Vpos = adc_a_p2p_value;
                    adc_10V_meas[V10_array_index].p2p_10Vneg = adc_b_p2p_value;
                    adc_10V_meas[V10_array_index].fault_type = P2P_AVE_FLT;

                    V10_array_index++;                  // next line

                    if ( V10_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
                    {
                        RadSendData (SEND_10V);             // and data are sent
                    }
                }
            }

            // test if one channel is stuck
            if ((menusd.a.max_h == menusd.a.min_h && menusd.a.max_m == menusd.a.min_m && menusd.a.max_l == menusd.a.min_l)
             || (menusd.b.max_h == menusd.b.min_h && menusd.b.max_m == menusd.b.min_m && menusd.b.max_l == menusd.b.min_l))
            {

            if ( V10_array_index < MAX_ADC_ARRAY_SIZE)          // Test if it's the last line of the array
            {
                adc_10V_meas[V10_array_index].unix_time  = SM_UXTIME_P;
                adc_10V_meas[V10_array_index].ave_10Vpos = adc_a_average_value;
                adc_10V_meas[V10_array_index].ave_10Vneg = adc_b_average_value;
                adc_10V_meas[V10_array_index].p2p_10Vpos = adc_a_p2p_value;
                adc_10V_meas[V10_array_index].p2p_10Vneg = adc_b_p2p_value;
                adc_10V_meas[V10_array_index].fault_type = STUCK_FLT;
                V10_array_index++;                  // next line

                if ( V10_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
                {
                    RadSendData (SEND_10V);             // and data are sent
                }
            }

            fprintf(TERM_TX,"\n" RSP_ERROR ",+10V/-10V value stuck.\n");
            RadSendData (SEND_ALL);                 // Send all datas
            MSLEEP(2000);                       // Wait for 2s
            OS_ENTER_CRITICAL();                    // Stop interrupts to prevent IsrMst() from writing LEDs register
                                        // and there for losing power cycle request (same location)
            CPU_PWRCYC_P = 0xF00E;                  // Write power cycle request to the NDI register

            for(;;);                        // Wait for power cycle to act with interrupts disabled
        }

#ifndef DEBUG_NO_ADC_RESET
    if( (labs(adc_a_average_value - pos10V_average_old) > V10_jump_tol) ||
        (labs(adc_b_average_value - neg10V_average_old) > V10_jump_tol ) )
    {
        if ( V10_array_index < MAX_ADC_ARRAY_SIZE)          // Test if it's the last line of the array
        {
            adc_10V_meas[V10_array_index].unix_time  = SM_UXTIME_P;
            adc_10V_meas[V10_array_index].ave_10Vpos = pos10V_average_old;
            adc_10V_meas[V10_array_index].ave_10Vneg = neg10V_average_old;
            adc_10V_meas[V10_array_index].p2p_10Vpos = pos10V_p2p_old;
            adc_10V_meas[V10_array_index].p2p_10Vneg = neg10V_p2p_old;
            adc_10V_meas[V10_array_index].fault_type = JUMP_FLT;

            V10_array_index++;                  // next line

            if ( V10_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
            {
                RadSendData (SEND_10V);             // and data are sent
            }

            adc_10V_meas[V10_array_index].unix_time  = SM_UXTIME_P;
            adc_10V_meas[V10_array_index].ave_10Vpos = adc_a_average_value;
            adc_10V_meas[V10_array_index].ave_10Vneg = adc_b_average_value;
            adc_10V_meas[V10_array_index].p2p_10Vpos = adc_a_p2p_value;
            adc_10V_meas[V10_array_index].p2p_10Vneg = adc_b_p2p_value;
            adc_10V_meas[V10_array_index].fault_type = JUMP_FLT;

            V10_array_index++;                  // next line

            if ( V10_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
            {
                RadSendData (SEND_10V);             // and data are sent
            }
        }
        pos10V_average_old = adc_a_average_value;
        neg10V_average_old = adc_b_average_value;
        pos10V_p2p_old = adc_a_p2p_value;
        neg10V_p2p_old = adc_b_p2p_value;

        fprintf(TERM_TX,"\n" RSP_ERROR ",10V value jump.\n");
        RadSendData (SEND_ALL);                     // Send all datas
        MSLEEP(2000);                       // Wait for 2s

        if(dev.slot5_type == FGC_INTERFACE_SD_350)
        {
//              fprintf(TERM_TX,"\n" RSP_ERROR ",reprogramming ADC filters.\n");
            Set(ANA_CTRL_P,ANA_CTRL_FLTRESET_MASK16);       // Reset filter FPGAs
            Clr(ANA_CTRL_P,ANA_CTRL_FLTRESET_MASK16);       // reprogramming filter and run in normal operation
        }
        else
        {
                fprintf(TERM_TX,"\n" RSP_ERROR ",power cycle analogue components.\n");
            Set(ANA_CTRL_P,ANA_CTRL_PWRCYCL_MASK16);        // power cycle ADCs and DACs in the SAR400 card
        }
    }
    else
    {
#endif
        pos10V_average_old = adc_a_average_value;
        neg10V_average_old = adc_b_average_value;
        pos10V_p2p_old = adc_a_p2p_value;
        neg10V_p2p_old = adc_b_p2p_value;

#ifndef DEBUG_NO_ADC_RESET
    }

    if ( V10_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
    {
        RadSendData (SEND_10V);             // and data are sent
    }
#endif

    ana_test_selection = 2;
    break;


    case 2 :
        switch(dev.slot5_type)
        {
            case FGC_INTERFACE_SD_350 :
                test_result = RadCheckAverageP2PValue(ADC_P2P_DAC_TOL_SD, ADC_AVE_DAC_TOL_SD,
                                  adc_a_dac_ref, adc_b_dac_ref);
                DAC_jump_tol = DAC_JUMP_TOL_SD;
                nb_sample_before_stuck = NB_SAMPLE_STUCK_SD;
                break;

            case FGC_INTERFACE_SAR_400 :
                test_result = RadCheckAverageP2PValue(ADC_P2P_DAC_TOL_SAR, ADC_AVE_DAC_TOL_SAR,
                                  adc_a_dac_ref, adc_b_dac_ref);
                DAC_jump_tol = DAC_JUMP_TOL_SAR;
                nb_sample_before_stuck = NB_SAMPLE_STUCK_SAR;
                break;

            default :                           // problem with the software

                fprintf(TERM_TX,"\n" RSP_ERROR ",software corrupted : ana test selection problem.\n");
                RadSendData (SEND_ALL);                     // Send all datas
                MSLEEP(2000);                       // Wait for 2s
                OS_ENTER_CRITICAL();                    // Stop interrupts to prevent IsrMst() from writing LEDs register
                                        // and there for losing power cycle request (same location)
                CPU_PWRCYC_P = 0xF00E;                  // Write power cycle request to the NDI register

                for(;;);                            // Wait for power cycle to act with interrupts disabled
        }

    // storage value if there is a problem
        if (test_result == 1)
        {
            if ( DAC_array_index < MAX_ADC_ARRAY_SIZE)          // Test if the last line of the array
            {
                adc_DAC_meas[DAC_array_index].unix_time = SM_UXTIME_P;
                adc_DAC_meas[DAC_array_index].ave_a_DAC = adc_a_average_value;
                adc_DAC_meas[DAC_array_index].ave_b_DAC = adc_b_average_value;
                adc_DAC_meas[DAC_array_index].p2p_a_DAC = adc_a_p2p_value;
                adc_DAC_meas[DAC_array_index].p2p_b_DAC = adc_b_p2p_value;
                adc_DAC_meas[DAC_array_index].fault_type = P2P_AVE_FLT;

                DAC_array_index++;                  // next line

                if ( DAC_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
                {
                    RadSendData (SEND_DAC);             // and data are sent
                }
            }
        }


    if ((menusd.a.max_h == menusd.a.min_h && menusd.a.max_m == menusd.a.min_m && menusd.a.max_l == menusd.a.min_l)
     || (menusd.b.max_h == menusd.b.min_h && menusd.b.max_m == menusd.b.min_m && menusd.b.max_l == menusd.b.min_l))
    {
        if ( DAC_array_index < MAX_ADC_ARRAY_SIZE)          // Test if the last line of the array
        {
            adc_DAC_meas[DAC_array_index].unix_time = SM_UXTIME_P;
            adc_DAC_meas[DAC_array_index].ave_a_DAC = adc_a_average_value;
            adc_DAC_meas[DAC_array_index].ave_b_DAC = adc_b_average_value;
            adc_DAC_meas[DAC_array_index].p2p_a_DAC = adc_a_p2p_value;
            adc_DAC_meas[DAC_array_index].p2p_b_DAC = adc_b_p2p_value;
            adc_DAC_meas[DAC_array_index].fault_type = STUCK_FLT;
            DAC_array_index++;                  // next line

            if ( DAC_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
            {
                RadSendData (SEND_DAC);             // and data are sent
            }
        }

        fprintf(TERM_TX,"\n" RSP_ERROR ",DAC value stuck.\n");
        RadSendData (SEND_ALL);                     // Send all datas
        MSLEEP(2000);                       // Wait for 2s
        OS_ENTER_CRITICAL();                    // Stop interrupts to prevent IsrMst() from writing LEDs register
                                    // and there for losing power cycle request (same location)
        CPU_PWRCYC_P = 0xF00E;                  // Write power cycle request to the NDI register

        for(;;);                            // Wait for power cycle to act with interrupts disabled

    }

#ifndef DEBUG_NO_ADC_RESET
    if( (labs(adc_a_average_value - dac_a_average_old) > DAC_jump_tol) ||
        (labs(adc_b_average_value - dac_b_average_old) > DAC_jump_tol) )
    {
        if ( DAC_array_index < MAX_ADC_ARRAY_SIZE)          // Test if the last line of the array
        {
            adc_DAC_meas[DAC_array_index].unix_time = SM_UXTIME_P;
            adc_DAC_meas[DAC_array_index].ave_a_DAC = dac_a_average_old;
            adc_DAC_meas[DAC_array_index].ave_b_DAC = dac_b_average_old;
            adc_DAC_meas[DAC_array_index].p2p_a_DAC = DAC_a_p2p_old;
            adc_DAC_meas[DAC_array_index].p2p_b_DAC = DAC_b_p2p_old;
            adc_DAC_meas[DAC_array_index].fault_type = JUMP_FLT;

            DAC_array_index++;                  // next line

            if ( DAC_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
            {
                RadSendData (SEND_DAC);             // and data are sent
            }
        }

        if ( DAC_array_index < MAX_ADC_ARRAY_SIZE)          // Test if the last line of the array
        {
            adc_DAC_meas[DAC_array_index].unix_time = SM_UXTIME_P;
            adc_DAC_meas[DAC_array_index].ave_a_DAC = adc_a_average_value;
            adc_DAC_meas[DAC_array_index].ave_b_DAC = adc_b_average_value;
            adc_DAC_meas[DAC_array_index].p2p_a_DAC = adc_a_p2p_value;
            adc_DAC_meas[DAC_array_index].p2p_b_DAC = adc_b_p2p_value;
            adc_DAC_meas[DAC_array_index].fault_type = JUMP_FLT;

            DAC_array_index++;                  // next line

            if ( DAC_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
            {
                RadSendData (SEND_DAC);             // and data are sent
            }
        }
        dac_a_average_old = adc_a_average_value;
        dac_b_average_old = adc_b_average_value;
        DAC_a_p2p_old = adc_a_p2p_value;
        DAC_b_p2p_old = adc_b_p2p_value;

        // Was "10V value jump." - Fixed June 24
        fprintf(TERM_TX,"\n" RSP_ERROR ",DAC value jump.\n");
        RadSendData (SEND_ALL);                     // Send all datas
        MSLEEP(2000);                       // Wait for 2s

        if(dev.slot5_type == FGC_INTERFACE_SD_350)
        {
//              fprintf(TERM_TX,"\n" RSP_ERROR ",reprogramming ADC filters.\n");
            Set(ANA_CTRL_P,ANA_CTRL_FLTRESET_MASK16);       // Reset filter FPGAs
            Clr(ANA_CTRL_P,ANA_CTRL_FLTRESET_MASK16);       // reprogramming filter and run in normal operation
        }
        else
        {
            Set(ANA_CTRL_P,ANA_CTRL_PWRCYCL_MASK16);        // power cycle ADCs and DACs in the SAR400 card
        }
    }
    else
    {
#endif
        dac_a_average_old = adc_a_average_value;
        dac_b_average_old = adc_b_average_value;
        DAC_a_p2p_old = adc_a_p2p_value;
        DAC_b_p2p_old = adc_b_p2p_value;
#ifndef DEBUG_NO_ADC_RESET
    }

    if ( DAC_array_index >= MAX_ADC_ARRAY_SIZE)     // Test if the last line of the array had been completed
    {
        RadSendData (SEND_DAC);             // and data are sent
    }
#endif
    ana_test_selection = 0;
    break;

    default :                               // problem with the software

        fprintf(TERM_TX,"\n" RSP_ERROR ",software corrupted : ana test selection problem.\n");
        RadSendData (SEND_ALL);                     // Send all datas
        MSLEEP(2000);                           // Wait for 2s
        OS_ENTER_CRITICAL();                        // Stop interrupts to prevent IsrMst() from writing LEDs register
                                    // and there for losing power cycle request (same location)
        CPU_PWRCYC_P = 0xF00E;                      // Write power cycle request to the NDI register

        for(;;);                            // Wait for power cycle to act with interrupts disabled
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RadGetSample(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets one p2p and average ADCs values.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  dac_value;

    RadInitAnaAcq();            // init ana value

    // selection the channel configuration
    AnaMpxAna(7,4);         // +10V on channelA and -10V on channelB
    MSLEEP(500);

    // Calculation of the P2P and average values
    RadAverageP2PMeas(ANA_SAMPLES);

    fprintf(TERM_TX, RSP_DATA ",3,unix time,+10V average,+10V p2p,-10V average,-10V p2p, type of fault :\n");
    fprintf(TERM_TX, RSP_DATA ",3,%lu,0x%08lX,0x%08lX,0x%08lX,0x%08lX,0x%02X\n",
            SM_UXTIME_P,
            adc_a_average_value,
            adc_a_p2p_value,
            adc_b_average_value,
            adc_b_p2p_value,
            TAKE_POINT);


    RadInitAnaAcq();            // init ana value
    AnaMpxAna(3,3);         // DAC on channelA and channelB

    // Set DAC regiter

    dac_value = (uint32_t)(SLOT5_DAC_10V);

    dac_value <<= 12;           // Value in DAC is left justified (bits 31-12)

    ANA_DAC_HIGH_P = ((uint16_t*)&dac_value)[0];
    ANA_DAC_LOW_P  = ((uint16_t*)&dac_value)[1];

    MSLEEP(2000);           // Wait for 2s

    // Calculation of the P2P and average values
    RadAverageP2PMeas(ANA_SAMPLES);

    fprintf(TERM_TX, RSP_DATA ",4,unix time,DAC chanA average,DAC chanA p2p,DAC chanB average,DAC chanB p2p, type of fault :\n");
    fprintf(TERM_TX, RSP_DATA ",4,%lu,0x%08lX,0x%08lX,0x%08lX,0x%08lX,0x%02X\n\n",
            SM_UXTIME_P,
            adc_a_average_value,
            adc_a_p2p_value,
            adc_b_average_value,
            adc_b_p2p_value,
            TAKE_POINT);
}

/*---------------------------------------------------------------------------------------------------------*/
void RadInitFIP (void)
/*---------------------------------------------------------------------------------------------------------*\
  Init TXDMSG and RXDMSG register to 0x0000.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  addr = FIP_TXDMSG_32;       // first address of the TXDMSG register
    uint16_t  n_words;

    for(n_words = 0;n_words < TX_RX_MSG_FIP;n_words++)
    {
        *(uint8_t *)addr = RAM_WRITE_VALUE;   // Write define value in the TXDMSG and TXDMSG register
        addr ++;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RadTest_RX_TX_FIP (void)
/*---------------------------------------------------------------------------------------------------------*\
  check the TXDMSG and RXDMSG register.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t   value;
    uint32_t  addr = FIP_TXDMSG_32;       // first address of the internal HC16 RAM;
    uint16_t  n_words;

    for(n_words = 0;n_words < TX_RX_MSG_FIP;n_words++)
    {
        value = *(uint8_t *)addr;             // Read back value in the internal RAM HC16
        if (value != RAM_WRITE_VALUE)               // test if the value is corrupted
        {
            hc16_sbe_reg_counter++;             // incrementation of the SBE counter

            RadSendData (SEND_HC16_WFIP_REGISTER);          // send data
            fprintf(TERM_TX, RSP_ERROR ",register corrupted:add:0x%08lX,exp:0x%04X,read:0x%04X\n",
                    addr,
                    RAM_WRITE_VALUE,
                    *(uint8_t *)addr);
            *(uint8_t *)addr = RAM_WRITE_VALUE;           // Write define value into location
        }
        addr ++;                        // next address
    }
 }
/*---------------------------------------------------------------------------------------------------------*/
void RadHc16MicrofipC32RegTest(void)
/*---------------------------------------------------------------------------------------------------------*\
  Test the HC16, C32 and the WFIP internal register. If there is a SBE, the software corrects the error and
  read again the HC16 or C32 internal register. If there is always an error the register is considered
  broken.If there is a SBE on the WFIP register, the software send all data and reset the WFIP.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  i;

    // test if C32 Registers are corrupted
    if (c32_sbe_int_reg_counter != DPRAM_BT_C32INTREGSBES_P)
    {
        c32_sbe_int_reg_counter = DPRAM_BT_C32INTREGSBES_P;     // save counter for send it
        c32_sbe_int_reg_idx     = DPRAM_BT_C32INTREGFAILIDX_P;      // save index to know which register failed
        RadSendData (SEND_C32_REGISTER);                // send data

        if (c32_sbe_int_reg_failed  !=  DPRAM_BT_C32INTREGFAILS_P)  // test if the register is broken
        {
            c32_sbe_int_reg_failed  =  DPRAM_BT_C32INTREGFAILS_P;
            fprintf(TERM_TX,"\n" RSP_DATA ",11,unix time,%lu,SBE C32 register failed,%lu\n", SM_UXTIME_P, c32_sbe_int_reg_failed);
            }
         }


        for (i=0; internal_regs[i].addr; i++)               // Scan all internal registers
        {
            if (internal_regs[i].state == 0)                // if register doesn't break scan register
            {
                if (internal_regs[i].size == 1)                 // cast for one octet
                {
                // If value in the internal register is corrupted
                if (*((uint8_t * FAR)internal_regs[i].addr) != internal_regs[i].value)
                {
                    hc16_sbe_reg_counter++;             // increment the SBE counter

                    RadSendData (SEND_HC16_WFIP_REGISTER);      // send data
                    fprintf(TERM_TX, RSP_ERROR ",register corrupted:add:0x%08lX,exp:0x%02X,read:0x%02X\n",
                            internal_regs[i].addr,
                            internal_regs[i].value,
                            *((uint8_t* FAR)internal_regs[i].addr));

                    if(internal_regs[i].action == RAD_WRITE)
                    {                           // reload the correct value
                        *((uint8_t * FAR)internal_regs[i].addr) = internal_regs[i].value;
                                                // test if the reloaded value is correct
                        if( *((uint8_t * FAR)internal_regs[i].addr) != internal_regs[i].value )
                        {
                                hc16_sbe_int_reg_failed++;
                                fprintf(TERM_TX, RSP_DATA ",9,unix time,%lu,SBE register failed,%u\n",
                                        SM_UXTIME_P,
                                        hc16_sbe_int_reg_failed);
                                fprintf(TERM_TX, RSP_ERROR ",register broken:add:0x%08lX,exp:0x%02X,read:0x%02X\n",
                                        internal_regs[i].addr,
                                        internal_regs[i].value,
                                        *((uint8_t* FAR)internal_regs[i].addr));
                            internal_regs[i].state = 1;         // if no,indication that register is broken
                        }
                    }
                    else if(internal_regs[i].action == RAD_RESET)
                    {
                        PllInit();
                        FipInit();                  // reset and reconfig the WFIP
                        RunlogWrite(FGC_RL_FIP_ID, (void*)&dev.fieldbus_id);        // Store FIP station ID in run log
                        MemSetBytes(&stat_var.fieldbus_stat.diag_data, 0xFF, sizeof(stat_var.fieldbus_stat.diag_data) );
                    }
                    else
                    {
                        fprintf(TERM_TX,"\n" RSP_ERROR ",software corrupted:Action different that WRITE or RESET.\n");
                        RadSendData (SEND_ALL);             // send all datas
                        MSLEEP(2000);                   // Wait for 2s
                        OS_ENTER_CRITICAL();                // Stop interrupts to prevent IsrMst() from writing LEDs register
                                            // and there for losing power cycle request (same location)
                        CPU_PWRCYC_P = 0xF00E;              // Write power cycle request to the NDI register

                        for(;;);                    // Wait for power cycle to act with interrupts disabled
                    }
                }
        }
        else if (internal_regs[i].size == 2)
        {
        // If value in the internal register is corrupted
        if ( *((uint16_t * FAR)internal_regs[i].addr) != internal_regs[i].value )
        {
            hc16_sbe_reg_counter++;             // incrementation of the SBE counter

            RadSendData (SEND_HC16_WFIP_REGISTER);          // send data
            fprintf(TERM_TX, RSP_ERROR ",register corrupted:add:0x%08lX,exp:0x%04X,read:0x%04X\n",
                internal_regs[i].addr,
                internal_regs[i].value,
                *((uint16_t* FAR)internal_regs[i].addr));

            if(internal_regs[i].action == RAD_WRITE)
            {                           // reload the correct value
            *((uint16_t * FAR)internal_regs[i].addr) = internal_regs[i].value;
                                    // test if the reloaded value is correct
            if ( *((uint16_t * FAR)internal_regs[i].addr) != internal_regs[i].value )
            {
                            hc16_sbe_int_reg_failed++;
                fprintf(TERM_TX, RSP_DATA ",9, unix time,%lu,SBE register failed,%u\n",
                        SM_UXTIME_P,
                    hc16_sbe_int_reg_failed);
                fprintf(TERM_TX, RSP_ERROR ",register broken:add:0x%08lX,exp:0x%04X,read:0x%04X\n",
                    internal_regs[i].addr,
                    internal_regs[i].value,
                    *((uint16_t* FAR)internal_regs[i].addr));
                internal_regs[i].state = 1;         // if no,indication that register is broken
            }
            }
            else if(internal_regs[i].action == RAD_RESET)
            {
                PllInit();
            FipInit();                  // reset and reconfig the WFIP
                        RunlogWrite(FGC_RL_FIP_ID, (void*)&dev.fieldbus_id);        // Store FIP station ID in run log
                        MemSetBytes(&stat_var.fieldbus_stat.diag_data, 0xFF, sizeof(stat_var.fieldbus_stat.diag_data) );
            }
            else
            {
                fprintf(TERM_TX,"\n" RSP_ERROR ",software corrupted:Action different that WRITE or RESET.\n");
                RadSendData (SEND_ALL);             // send all datas
                MSLEEP(2000);                   // Wait for 2s
                OS_ENTER_CRITICAL();                // Stop interrupts to prevent IsrMst() from writing LEDs register
                                    // and there for losing power cycle request (same location)
                CPU_PWRCYC_P = 0xF00E;              // Write power cycle request to the NDI register
                for(;;);                    // Wait for power cycle to act with interrupts disabled
                }
            }
        }
        else
        {
            fprintf(TERM_TX,"\n" RSP_ERROR ",software corrupted: size register is different of 1 or 2.\n");
            RadSendData (SEND_ALL);                 // send all datas
            MSLEEP(2000);                       // Wait for 2s
            OS_ENTER_CRITICAL();                    // Stop interrupts to prevent IsrMst() from writing LEDs register
                                        // and there for losing power cycle request (same location)
            CPU_PWRCYC_P = 0xF00E;                  // Write power cycle request to the NDI register
            for(;;);                        // Wait for power cycle to act with interrupts disabled
        }
    }
    }

    RadTest_RX_TX_FIP ();                       // test the TXDMSG and RXDMSG register
}
/*---------------------------------------------------------------------------------------------------------*/
void RadHc16C32ExtRAM(void)
/*---------------------------------------------------------------------------------------------------------*\
  check HC16 RAM and C32 RAM and detect a SBE.

  The DSP TMS320C32 keeps two 32 bits counters, following the M68HC16 RAM EDAC triggers and the TMS320C32 RAM EDAC
  triggers. These are the variables sbe_hc16 and sbe_c32.
  The M68HC16 keep an 8 bits counter that counts both EDAC triggers. This is mem_sbe_cnt.
  In theory the sum of both DSP registers must match the count of the M68HC16.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  sbe_hc16;               // local copy of the HC16 SBE
    uint32_t  sbe_c32;                // local copy of the C32 SBE
    uint8_t   mem_sbe_f;                  // local copy of the SBE flag gived by the HC16
    uint32_t  mem_sbe_cnt;                // local copy of the SBE counter

    // save copy of the datas
    OS_ENTER_CRITICAL();
    mem_sbe_cnt     = dev.mem_sbe_cnt;
    sbe_hc16        = DPRAM_BT_HC16EDACSBES_P;
    sbe_c32     = DPRAM_BT_C32EDACSBES_P;
    mem_sbe_f       = dev.mem_sbe_f;
    dev.mem_sbe_f   = 0;                // Clear Flag
    OS_EXIT_CRITICAL();


    if((mem_sbe_f) ||                   // If single bit error(s) detected
    (sbe_hc16_last != sbe_hc16) ||
    (sbe_c32_last  != sbe_c32))
    {
    fprintf(TERM_TX,"\n" RSP_DATA ",5,unix time,%lu,mem_sbe,%c,SBE cnt,%lu,HC16,%lu,C32,%lu\n",  // print SBE counter
            SM_UXTIME_P,
            mem_sbe_f ? 'T' : 'F',
            mem_sbe_cnt,
            sbe_hc16,
            sbe_c32);

    sbe_hc16_last    = sbe_hc16;            //save last SBE value for the HC16
    sbe_c32_last     = sbe_c32;         //save last SBE value for the C32
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RadHc16InitRAM (void)
/*---------------------------------------------------------------------------------------------------------*\
  Init the Internal HC16 RAM to 0x0000.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  addr = RAM_BA_P;            // first address of the internal HC16 RAM
    uint16_t  n_words;

    for(n_words = 0;n_words < INTRAM_W;n_words++)
    {
        *((uint16_t * FAR)addr) = RAM_WRITE_VALUE;    // Write define value in the internal RAM HC16
        addr += 2;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RadHc16C32IntRAM (void)
/*---------------------------------------------------------------------------------------------------------*\
  Check the Internal TM320C32 and M68HC16 RAM and count the number of SBE.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  value;
    uint32_t  addr = RAM_BA_P;                // first address of the internal HC16 RAM;
    uint16_t  n_words;

    for(n_words = 0;n_words < INTRAM_W;n_words++)
    {
        value = *((uint16_t * FAR)addr);              // Read back value in the internal RAM HC16

        if (value != RAM_WRITE_VALUE)               // test if the value is corrupted
        {
            hc16_sbe_int_ram ++;                // if different, increase counter
            *((uint16_t * FAR)addr) = RAM_WRITE_VALUE;        // Write define value into location
            RadSendData (SEND_INT_RAM_HC16);            // send data
        }
        addr += 2;                      // next address
    }

    if(c32_sbe_int_ram != DPRAM_BT_C32INTRAMSBES_P)         // test if the internal C32 RAM is corrupted
    {
        c32_sbe_int_ram = DPRAM_BT_C32INTRAMSBES_P;        // save new value
        RadSendData (SEND_INT_RAM_C32);            // if yes, send data
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void RadSendData (uint8_t DataToSend)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends all data : SBE register, DAC array, 10V array, SBE.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t i;
    uint32_t  sbe_hc16;               // local copy of the HC16 SBE
    uint32_t  sbe_c32;                // local copy of the C32 SBE
    uint8_t   mem_sbe_f;                  // local copy of the SBE flag gived by the HC16
    uint32_t  mem_sbe_cnt;                // local copy of the SBE counter
    uint16_t  ana_register;

    switch (DataToSend)
    {
    case SEND_10V :                     // print ref +10V and -10V table
        fprintf(TERM_TX,"\n" RSP_DATA ",3,unix time,+10V average,+10V p2p,-10V average,-10V p2p, type of fault :\n");
        for (i=0;i<= MAX_ADC_ARRAY_SIZE-1;i++)
            {
            fprintf(TERM_TX, RSP_DATA ",3,%lu,0x%08lX,0x%08lX,0x%08lX,0x%08lX,0x%02X\n",
                        adc_10V_meas[i].unix_time,
                    adc_10V_meas[i].ave_10Vpos,
                    adc_10V_meas[i].p2p_10Vpos,
                    adc_10V_meas[i].ave_10Vneg,
                    adc_10V_meas[i].p2p_10Vneg,
                    adc_10V_meas[i].fault_type);
        }
        for (i=0; i<= MAX_ADC_ARRAY_SIZE-1;i++)     // clear array
        {
            adc_10V_meas[i].unix_time  = 0;
            adc_10V_meas[i].ave_10Vpos = 0;
            adc_10V_meas[i].ave_10Vneg = 0;
            adc_10V_meas[i].p2p_10Vpos = 0;
            adc_10V_meas[i].p2p_10Vneg = 0;
            adc_10V_meas[i].fault_type = 0;
        }
        V10_array_index = 0;                // init index of the array
        break;


    case SEND_DAC :                     // print DAC table
        fprintf(TERM_TX,"\n" RSP_DATA ",4,unix time,DAC chanA average,DAC chanA p2p,DAC chanB average,DAC chanB p2p, type of fault :\n");
        for (i=0;i<= MAX_ADC_ARRAY_SIZE-1;i++)
            {
            fprintf(TERM_TX, RSP_DATA ",4,%lu,0x%08lX,0x%08lX,0x%08lX,0x%08lX,0x%02X\n",
                        adc_DAC_meas[i].unix_time,
                    adc_DAC_meas[i].ave_a_DAC,
                    adc_DAC_meas[i].p2p_a_DAC,
                    adc_DAC_meas[i].ave_b_DAC,
                    adc_DAC_meas[i].p2p_b_DAC,
                    adc_DAC_meas[i].fault_type);
            }
        for (i=0; i<= MAX_ADC_ARRAY_SIZE-1;i++)     // clear array
        {
            adc_DAC_meas[i].unix_time = 0;
            adc_DAC_meas[i].ave_a_DAC = 0;
            adc_DAC_meas[i].ave_b_DAC = 0;
            adc_DAC_meas[i].p2p_a_DAC = 0;
            adc_DAC_meas[i].p2p_b_DAC = 0;
            adc_DAC_meas[i].fault_type =0;
        }
        DAC_array_index = 0;                // init index of the array
        break;


    case SEND_HC16_WFIP_REGISTER :              // print unix time and SBE on the internal register(WFIP + HC16)
        fprintf(TERM_TX,"\n" RSP_DATA ",8,unix time,%lu,SBE register,%u\n", SM_UXTIME_P, hc16_sbe_reg_counter);
        break;


    case SEND_C32_REGISTER :
        fprintf(TERM_TX,"\n" RSP_DATA ",10,unix time,%lu,SBE C32 register,%lu,%lu\n", SM_UXTIME_P,c32_sbe_int_reg_idx, c32_sbe_int_reg_counter);
        break;


    case SEND_INT_RAM_HC16 :                // print unix time and SBE on the internal HC16 RAM
        fprintf(TERM_TX,"\n" RSP_DATA ",6,unix time,%lu,SBE internal RAM HC16,%u\n",
                    SM_UXTIME_P,
                    hc16_sbe_int_ram);
        break;


    case SEND_INT_RAM_C32 :                 // print unix time and SBE on the internal C32 RAM
        fprintf(TERM_TX,"\n" RSP_DATA ",7,unix time,%lu,SBE C32 internal RAM,%lu\n",
                    SM_UXTIME_P,
                    c32_sbe_int_ram);
        break;


    case SEND_ALL :                     // print all datas
        if (V10_array_index)                // print ref +10V and -10V table
        {
        fprintf(TERM_TX,"\n" RSP_DATA ",3,unix time,+10V average,+10V p2p,-10V average,-10V p2p, type of fault :\n");
        for (i=0;i<= V10_array_index-1;i++)
            {
            fprintf(TERM_TX, RSP_DATA ",3,%lu,0x%08lX,0x%08lX,0x%08lX,0x%08lX,0x%02X\n",
                    adc_10V_meas[i].unix_time,
                adc_10V_meas[i].ave_10Vpos,
                adc_10V_meas[i].p2p_10Vpos,
                adc_10V_meas[i].ave_10Vneg,
                adc_10V_meas[i].p2p_10Vneg,
                adc_10V_meas[i].fault_type);
            }
        }
    if (DAC_array_index)                    // print DAC table
    {
        fprintf(TERM_TX,"\n" RSP_DATA ",4,unix time,DAC chanA average,DAC chanA p2p,DAC chanB average,DAC chanB p2p, type of fault :\n");

        for (i=0;i<= DAC_array_index-1;i++)
            {
            fprintf(TERM_TX, RSP_DATA ",4,%lu,0x%08lX,0x%08lX,0x%08lX,0x%08lX,0x%02X\n",
                    adc_DAC_meas[i].unix_time,
                adc_DAC_meas[i].ave_a_DAC,
                adc_DAC_meas[i].p2p_a_DAC,
                adc_DAC_meas[i].ave_b_DAC,
                adc_DAC_meas[i].p2p_b_DAC,
                adc_DAC_meas[i].fault_type);
            }
    }
        if ((DAC_array_index) || (V10_array_index))
    {
        for (i=0; i<= MAX_ADC_ARRAY_SIZE-1;i++) // clear array
        {
            adc_10V_meas[i].unix_time  = 0;
            adc_10V_meas[i].ave_10Vpos = 0;
            adc_10V_meas[i].ave_10Vneg = 0;
            adc_10V_meas[i].p2p_10Vpos = 0;
        adc_10V_meas[i].p2p_10Vneg = 0;
        adc_10V_meas[i].fault_type = 0;
            adc_DAC_meas[i].unix_time = 0;
            adc_DAC_meas[i].ave_a_DAC = 0;
            adc_DAC_meas[i].ave_b_DAC = 0;
            adc_DAC_meas[i].p2p_a_DAC = 0;
        adc_DAC_meas[i].p2p_b_DAC = 0;
        adc_DAC_meas[i].fault_type = 0;
        }
        DAC_array_index = 0;             // init index of the array
        V10_array_index = 0;
        }

    ana_register = ANA_CTRL_P;
    if (!( ana_register & (ANA_CTRL_FLTANOTRDY_MASK16 | ANA_CTRL_FLTBNOTRDY_MASK16)))
    {
        fprintf(TERM_TX,"\n" RSP_DATA ",12,take ANA values :\n");
        RadGetSample();
    }

        // print unix time and SBE on the internal HC16 and WFIP register
    fprintf(TERM_TX, RSP_DATA ",8,unix time,%lu,SBE register,%u\n", SM_UXTIME_P, hc16_sbe_reg_counter);

        // print unix time and SBE on the internal C32 register
    fprintf(TERM_TX,"\n" RSP_DATA ",10,unix time,%lu,SBE C32 register,%lu\n", SM_UXTIME_P, c32_sbe_int_reg_counter);



        // save copy of the datas
        OS_ENTER_CRITICAL();
        mem_sbe_cnt     = dev.mem_sbe_cnt;
        mem_sbe_f       = dev.mem_sbe_f;
        dev.mem_sbe_f       = 0;
        sbe_hc16        = DPRAM_BT_HC16EDACSBES_P;
        sbe_c32             = DPRAM_BT_C32EDACSBES_P;
        OS_EXIT_CRITICAL();

    // print the SBE of the HC16 and C32
    fprintf(TERM_TX,"\n" RSP_DATA ",5,unix time,%lu,mem_sbe,%c,SBE cnt,%lu,HC16,%lu,C32,%lu\n",
            SM_UXTIME_P,
            mem_sbe_f ? 'T' : 'F',
            mem_sbe_cnt,
            sbe_hc16,
            sbe_c32);

        // print the SBE of the internal HC16 and C32 RAM
    fprintf(TERM_TX,"\n" RSP_DATA ",6,unix time,%lu,SBE internal RAM HC16,%u\n",
                SM_UXTIME_P,
                hc16_sbe_int_ram);

    fprintf(TERM_TX,"\n" RSP_DATA ",7,unix time,%lu,SBE internal RAM C32,%lu\n",
                SM_UXTIME_P,
                c32_sbe_int_ram);
    break;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rad.c
\*---------------------------------------------------------------------------------------------------------*/
