/*---------------------------------------------------------------------------------*\
 File:		50\link.txt

 Purpose:	Class 50 FGC P10-Boot Software Linker file

 Notes:		This boot program uses the medium memory model with code in
 		pages 2 and 3 and the default data page 0.  The FGC2 memory manager
		makes the first KB of page 2 visible at the first KB of page 0.
\*---------------------------------------------------------------------------------*/

/*--- Define memory sections ---*/

SECTIONS

    /* Section for intermediate interrupt vector table (512 bytes) */

    VEC_TABLE = READ_ONLY  0x20200 TO 0x203FF;

    /* Section for program code and initialisation data */

    PROG_CODE = READ_ONLY 0x20400 TO 0x3FFF0;

    /* Section for stack (1K) */

    MY_STACK = READ_WRITE 0x00C10 TO 0x00FFF;

    /* Section for program variables (23K) */

    PROG_VARS = READ_WRITE 0x01000 TO 0x067FF;

/* Assign data segments to memory sections */

PLACEMENT

    VEC_TABLE						INTO VEC_TABLE;
    _PRESTART, SWITCH, START_UP, ROM_VAR, DEFAULT_ROM, STRINGS 	INTO PROG_CODE;
    DEFAULT_RAM 					INTO PROG_VARS;
    SSTACK						INTO MY_STACK;

END

/* HC16 Reset Vector (0x20000 - 0x20007) */

VECTOR ADDRESS	0x20000	0x0002		/* 0x00	Initial ZK, SK, PK					*/
VECTOR ADDRESS	0x20002	_Startup	/* 0x01	Initial PC						*/
VECTOR ADDRESS	0x20004	0x0FFE		/* 0x02 Initial SP     						*/
VECTOR ADDRESS	0x20006	0x0000		/* 0x03 Initial IZ     						*/

/* HC16 Interrupt Vector (0x20008 - 0x201FF) */

VECTOR ADDRESS	0x20008	trap_vec_0x04	/* 0x04	Breakpoint			- IsrTrap(4)		*/
VECTOR ADDRESS	0x2000A	isr_vec_0x05	/* 0x05	Bus Error			- IsrBusError()		*/
VECTOR ADDRESS	0x2000C	trap_vec_0x06	/* 0x06	Software interrupt		- IsrTrap(6)	 	*/
VECTOR ADDRESS	0x2000E	trap_vec_0x07	/* 0x07	Illegal instruction		- IsrTrap(7) 		*/
VECTOR ADDRESS	0x20010	trap_vec_0x08	/* 0x08	Division by zero		- IsrTrap(8) 		*/
VECTOR ADDRESS	0x20012	trap_vec_0x00	/* 0x09	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20014	trap_vec_0x00	/* 0x0A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20016	trap_vec_0x00	/* 0x0B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20018	trap_vec_0x00	/* 0x0C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2001A	trap_vec_0x00	/* 0x0D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2001C	trap_vec_0x00	/* 0x0E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2001E	trap_vec_0x0F	/* 0x0F	Uninitialised interrupts	- IsrTrap(F) 		*/
VECTOR ADDRESS	0x20020	trap_vec_0x00	/* 0x10	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20022	trap_vec_0x11	/* 0x11	IRQ1				- IsrTrap(11) 		*/
VECTOR ADDRESS	0x20024	trap_vec_0x11	/* 0x12	IRQ2				- IsrTrap(11) 		*/
VECTOR ADDRESS	0x20026	trap_vec_0x11	/* 0x13	IRQ3				- IsrTrap(11) 		*/
VECTOR ADDRESS	0x20028	trap_vec_0x11	/* 0x14	IRQ4				- IsrTrap(11) 		*/
VECTOR ADDRESS	0x2002A	trap_vec_0x11	/* 0x15	IRQ5				- IsrTrap(11) 		*/
VECTOR ADDRESS	0x2002C	trap_vec_0x11	/* 0x16	IRQ6				- IsrTrap(11) 		*/
VECTOR ADDRESS	0x2002E	trap_vec_0x11	/* 0x17	IRQ7				- IsrTrap(11) 		*/
VECTOR ADDRESS	0x20030	trap_vec_0x18	/* 0x18	Spurious interrupt		- IsrTrap(18)		*/
VECTOR ADDRESS	0x20032	trap_vec_0x00	/* 0x19	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20034	trap_vec_0x00	/* 0x1A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20036	trap_vec_0x00	/* 0x1B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20038	trap_vec_0x00	/* 0x1C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2003A	trap_vec_0x00	/* 0x1D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2003C	trap_vec_0x00	/* 0x1E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2003E	trap_vec_0x00	/* 0x1F	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20040	trap_vec_0x00	/* 0x20	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20042	trap_vec_0x00	/* 0x21	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20044	trap_vec_0x00	/* 0x22	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20046	trap_vec_0x00	/* 0x23	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20048	trap_vec_0x00	/* 0x24	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2004A	trap_vec_0x00	/* 0x25	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2004C	trap_vec_0x00	/* 0x26	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2004E	trap_vec_0x00	/* 0x27	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20050	trap_vec_0x00	/* 0x28	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20052	trap_vec_0x00	/* 0x29	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20054	trap_vec_0x00	/* 0x2A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20056	trap_vec_0x00	/* 0x2B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20058	trap_vec_0x00	/* 0x2C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2005A	trap_vec_0x00	/* 0x2D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2005C	trap_vec_0x00	/* 0x2E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2005E	trap_vec_0x00	/* 0x2F	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20060	trap_vec_0x00	/* 0x30	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20062	isr_vec_0x31	/* 0x31	GPT IC1 (uFIP IRQ)		- IsrFip()		*/
VECTOR ADDRESS	0x20064	trap_vec_0x00	/* 0x32	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20066	trap_vec_0x00	/* 0x33	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20068	trap_vec_0x00	/* 0x34	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2006A	isr_vec_0x35	/* 0x35	GPT OC2 (Millisecond tick) 	- IsrTick()		*/
VECTOR ADDRESS	0x2006C	trap_vec_0x00	/* 0x36	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2006E	trap_vec_0x00	/* 0x37	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20070	trap_vec_0x00	/* 0x38	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20072	trap_vec_0x00	/* 0x39	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20074	trap_vec_0x00	/* 0x3A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20076	trap_vec_0x00	/* 0x3B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20078	trap_vec_0x00	/* 0x3C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2007A	trap_vec_0x00	/* 0x3D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2007C	trap_vec_0x00	/* 0x3E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2007E	trap_vec_0x00	/* 0x3F	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20080	trap_vec_0x00	/* 0x40	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20082	trap_vec_0x00	/* 0x41	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20084	trap_vec_0x00	/* 0x42	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20086	trap_vec_0x00	/* 0x43	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20088	trap_vec_0x00	/* 0x44	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2008A	trap_vec_0x00	/* 0x45	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2008C	trap_vec_0x00	/* 0x46	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2008E	trap_vec_0x00	/* 0x47	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20090	trap_vec_0x00	/* 0x48	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20092	trap_vec_0x00	/* 0x49	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20094	trap_vec_0x00	/* 0x4A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20096	trap_vec_0x00	/* 0x4B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20098	trap_vec_0x00	/* 0x4C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2009A	trap_vec_0x00	/* 0x4D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2009C	trap_vec_0x00	/* 0x4E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2009E	trap_vec_0x00	/* 0x4F	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200A0	trap_vec_0x00	/* 0x50	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200A2	trap_vec_0x00	/* 0x51	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200A4	trap_vec_0x00	/* 0x52	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200A6	trap_vec_0x00	/* 0x53	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200A8	trap_vec_0x00	/* 0x54	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200AA	trap_vec_0x00	/* 0x55	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200AC	trap_vec_0x00	/* 0x56	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200AE	trap_vec_0x00	/* 0x57	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200B0	trap_vec_0x00	/* 0x58	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200B2	trap_vec_0x00	/* 0x59	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200B4	trap_vec_0x00	/* 0x5A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200B6	trap_vec_0x00	/* 0x5B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200B8	trap_vec_0x00	/* 0x5C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200BA	trap_vec_0x00	/* 0x5D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200BC	trap_vec_0x00	/* 0x5E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200BE	trap_vec_0x00	/* 0x5F	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200C0	trap_vec_0x00	/* 0x60	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200C2	trap_vec_0x00	/* 0x61	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200C4	trap_vec_0x00	/* 0x62	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200C6	trap_vec_0x00	/* 0x63	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200C8	trap_vec_0x00	/* 0x64	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200CA	trap_vec_0x00	/* 0x65	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200CC	trap_vec_0x00	/* 0x66	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200CE	trap_vec_0x00	/* 0x67	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200D0	trap_vec_0x00	/* 0x68	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200D2	trap_vec_0x00	/* 0x69	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200D4	trap_vec_0x00	/* 0x6A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200D6	trap_vec_0x00	/* 0x6B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200D8	trap_vec_0x00	/* 0x6C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200DA	trap_vec_0x00	/* 0x6D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200DC	trap_vec_0x00	/* 0x6E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200DE	trap_vec_0x00	/* 0x6F	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200E0	trap_vec_0x00	/* 0x70	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200E2	trap_vec_0x00	/* 0x71	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200E4	trap_vec_0x00	/* 0x72	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200E6	trap_vec_0x00	/* 0x73	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200E8	trap_vec_0x00	/* 0x74	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200EA	trap_vec_0x00	/* 0x75	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200EC	trap_vec_0x00	/* 0x76	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200EE	trap_vec_0x00	/* 0x77	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200F0	trap_vec_0x00	/* 0x78	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200F2	trap_vec_0x00	/* 0x79	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200F4	trap_vec_0x00	/* 0x7A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200F6	trap_vec_0x00	/* 0x7B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200F8	trap_vec_0x00	/* 0x7C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200FA	trap_vec_0x00	/* 0x7D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200FC	trap_vec_0x00	/* 0x7E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x200FE	trap_vec_0x00	/* 0x7F	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20100	trap_vec_0x00	/* 0x80	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20102	trap_vec_0x00	/* 0x81	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20104	trap_vec_0x00	/* 0x82	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20106	trap_vec_0x00	/* 0x83	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20108	trap_vec_0x00	/* 0x84	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2010A	trap_vec_0x00	/* 0x85	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2010C	trap_vec_0x00	/* 0x86	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2010E	trap_vec_0x00	/* 0x87	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20110	trap_vec_0x00	/* 0x88	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20112	trap_vec_0x00	/* 0x89	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20114	trap_vec_0x00	/* 0x8A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20116	trap_vec_0x00	/* 0x8B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20118	trap_vec_0x00	/* 0x8C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2011A	trap_vec_0x00	/* 0x8D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2011C	trap_vec_0x00	/* 0x8E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2011E	trap_vec_0x00	/* 0x8F	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20120	trap_vec_0x00	/* 0x90	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20122	trap_vec_0x00	/* 0x91	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20124	trap_vec_0x00	/* 0x92	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20126	trap_vec_0x00	/* 0x93	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20128	trap_vec_0x00	/* 0x94	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2012A	trap_vec_0x00	/* 0x95	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2012C	trap_vec_0x00	/* 0x96	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2012E	trap_vec_0x00	/* 0x97	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20130	trap_vec_0x00	/* 0x98	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20132	trap_vec_0x00	/* 0x99	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20134	trap_vec_0x00	/* 0x9A	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20136	trap_vec_0x00	/* 0x9B	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20138	trap_vec_0x00	/* 0x9C	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2013A	trap_vec_0x00	/* 0x9D	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2013C	trap_vec_0x00	/* 0x9E	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2013E	trap_vec_0x00	/* 0x9F	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20140	trap_vec_0x00	/* 0xA0	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20142	trap_vec_0x00	/* 0xA1	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20144	trap_vec_0x00	/* 0xA2	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20146	trap_vec_0x00	/* 0xA3	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20148	trap_vec_0x00	/* 0xA4	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2014A	trap_vec_0x00	/* 0xA5	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2014C	trap_vec_0x00	/* 0xA6	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2014E	trap_vec_0x00	/* 0xA7	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20150	trap_vec_0x00	/* 0xA8	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20152	trap_vec_0x00	/* 0xA9	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20154	trap_vec_0x00	/* 0xAA	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20156	trap_vec_0x00	/* 0xAB	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20158	trap_vec_0x00	/* 0xAC	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2015A	trap_vec_0x00	/* 0xAD	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2015C	trap_vec_0x00	/* 0xAE	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2015E	trap_vec_0x00	/* 0xAF	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20160	trap_vec_0x00	/* 0xB0	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20162	trap_vec_0x00	/* 0xB1	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20164	trap_vec_0x00	/* 0xB2	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20166	trap_vec_0x00	/* 0xB3	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20168	trap_vec_0x00	/* 0xB4	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2016A	trap_vec_0x00	/* 0xB5	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2016C	trap_vec_0x00	/* 0xB6	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2016E	trap_vec_0x00	/* 0xB7	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20170	trap_vec_0x00	/* 0xB8	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20172	trap_vec_0x00	/* 0xB9	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20174	trap_vec_0x00	/* 0xBA	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20176	trap_vec_0x00	/* 0xBB	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20178	trap_vec_0x00	/* 0xBC	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2017A	trap_vec_0x00	/* 0xBD	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2017C	trap_vec_0x00	/* 0xBE	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2017E	trap_vec_0x00	/* 0xBF	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20180	trap_vec_0x00	/* 0xC0	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20182	trap_vec_0x00	/* 0xC1	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20184	trap_vec_0x00	/* 0xC2	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20186	trap_vec_0x00	/* 0xC3	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20188	trap_vec_0x00	/* 0xC4	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2018A	trap_vec_0x00	/* 0xC5	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2018C	trap_vec_0x00	/* 0xC6	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2018E	trap_vec_0x00	/* 0xC7	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20190	trap_vec_0x00	/* 0xC8	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20192	trap_vec_0x00	/* 0xC9	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20194	trap_vec_0x00	/* 0xCA	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20196	trap_vec_0x00	/* 0xCB	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x20198	trap_vec_0x00	/* 0xCC	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2019A	trap_vec_0x00	/* 0xCD	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2019C	trap_vec_0x00	/* 0xCE	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x2019E	trap_vec_0x00	/* 0xCF	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201A0	trap_vec_0x00	/* 0xD0	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201A2	trap_vec_0x00	/* 0xD1	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201A4	trap_vec_0x00	/* 0xD2	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201A6	trap_vec_0x00	/* 0xD3	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201A8	trap_vec_0x00	/* 0xD4	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201AA	trap_vec_0x00	/* 0xD5	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201AC	trap_vec_0x00	/* 0xD6	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201AE	trap_vec_0x00	/* 0xD7	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201B0	trap_vec_0x00	/* 0xD8	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201B2	trap_vec_0x00	/* 0xD9	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201B4	trap_vec_0x00	/* 0xDA	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201B6	trap_vec_0x00	/* 0xDB	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201B8	trap_vec_0x00	/* 0xDC	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201BA	trap_vec_0x00	/* 0xDD	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201BC	trap_vec_0x00	/* 0xDE	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201BE	trap_vec_0x00	/* 0xDF	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201C0	trap_vec_0x00	/* 0xE0	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201C2	trap_vec_0x00	/* 0xE1	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201C4	trap_vec_0x00	/* 0xE2	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201C6	trap_vec_0x00	/* 0xE3	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201C8	trap_vec_0x00	/* 0xE4	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201CA	trap_vec_0x00	/* 0xE5	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201CC	trap_vec_0x00	/* 0xE6	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201CE	trap_vec_0x00	/* 0xE7	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201D0	trap_vec_0x00	/* 0xE8	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201D2	trap_vec_0x00	/* 0xE9	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201D4	trap_vec_0x00	/* 0xEA	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201D6	trap_vec_0x00	/* 0xEB	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201D8	trap_vec_0x00	/* 0xEC	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201DA	trap_vec_0x00	/* 0xED	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201DC	trap_vec_0x00	/* 0xEE	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201DE	trap_vec_0x00	/* 0xEF	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201E0	trap_vec_0x00	/* 0xF0	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201E2	trap_vec_0x00	/* 0xF1	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201E4	trap_vec_0x00	/* 0xF2	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201E6	trap_vec_0x00	/* 0xF3	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201E8	trap_vec_0x00	/* 0xF4	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201EA	trap_vec_0x00	/* 0xF5	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201EC	trap_vec_0x00	/* 0xF6	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201EE	trap_vec_0x00	/* 0xF7	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201F0	trap_vec_0x00	/* 0xF8	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201F2	trap_vec_0x00	/* 0xF9	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201F4	trap_vec_0x00	/* 0xFA	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201F6	trap_vec_0x00	/* 0xFB	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201F8	trap_vec_0x00	/* 0xFC	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201FA	trap_vec_0x00	/* 0xFD	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201FC	trap_vec_0x00	/* 0xFE	Unassigned			- IsrTrap(0) 		*/
VECTOR ADDRESS	0x201FE	trap_vec_0x00	/* 0xFF	Unassigned			- IsrTrap(0) 		*/

/* EOF */

