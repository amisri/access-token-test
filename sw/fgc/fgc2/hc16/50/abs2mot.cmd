/*----------------------------------------------------------------------------*\
 File:		50\mot.cmd

 Purpose:	Class 50 FGC HC16 Boot Software V2 burner drive file

 Notes:
\*----------------------------------------------------------------------------*/

OPENFILE ".\prog.mot"				/* Open file for S-Records */

format		= motorola
SRECORD		= S3
busWidth	= 1
undefByte	= 0xFF
origin		= 0x20000
len		= 0x20000			/* TEMP: Will be 0x20000 */
destination	= 0

SENDBYTE 1 ".\prog.abs"				/* Write prog.abs to S-Records */

CLOSE

/* End of file: 50\mot.cmd */

