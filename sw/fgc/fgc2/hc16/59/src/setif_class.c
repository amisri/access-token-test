/*---------------------------------------------------------------------------------------------------------*\
 File:          setif_class.c

 Purpose:       FGC MCU Class 59 Software - Set Command Condition Functions
\*---------------------------------------------------------------------------------------------------------*/

#include <setif_class.h>                        // Include all header files
#include <cmd.h>
#include <class.h>                              // Include class dependent definitions
#include <hash.h>                               // Include hash header file
#include <defprops.h>                           // Include property related definitions
#include <fgc_errs.h>
#include <macros.h>

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifModeOpOk(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Always true
\*---------------------------------------------------------------------------------------------------------*/
{
    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifResetOk(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     If no channels are armed, running or aborting
\*---------------------------------------------------------------------------------------------------------*/
{
    if(!dpcls.dsp.armed && !dpcls.dsp.running && !dpcls.dsp.aborting)
    {
        return(0);
    }

    return(FGC_BAD_STATE);
}
#pragma MESSAGE DEFAULT C5703
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifFgTableOk(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     FGEN channel is IDLE.

  The channel is defined by the property name, e.g. FGFUNC.TABLE.CHAN5.T or by the mux_idx for sub-device
  properties.  Thus for device properties the third property symbol index (CHAN5 is this case) provides
  the function with the channel index.  However, the constants are sorted alphabetically, so it goes CHAN0,
  CHAN1, CHAN10, CHAN11, ..., and this is decoded using a static lookup array.
\*---------------------------------------------------------------------------------------------------------*/
{
    static uint16_t chan_idxs[] = { 0,1,10,11,12,13,14,15,2,3,4,5,6,7,8,9 };

    /* Sub-device table properties: FG.FUNC.TABLE.T/R */

    if(Test(c->prop->flags, PF_SUB_DEV))
    {
        if((uint16_t)dpcls.dsp.fgstate.func[c->mux_idx] != FGC_FG_IDLE)
        {
            return(FGC_BAD_STATE);
        }
    }

    /* Device table properties: FGFUNC.TABLE.CHANn.T/R */

    else if((uint16_t)dpcls.dsp.fgstate.func[chan_idxs[c->sym_idxs[2] - STP_CHAN0]] != FGC_FG_IDLE)
    {
        return(FGC_BAD_STATE);
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: setif_class.c
\*---------------------------------------------------------------------------------------------------------*/

