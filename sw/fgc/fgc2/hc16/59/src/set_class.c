/*---------------------------------------------------------------------------------------------------------*\
  File:         set_class.c

  Purpose:      FGC MCU Class 59 Software - Set Command Functions
\*---------------------------------------------------------------------------------------------------------*/

#include <stdbool.h>
#include <set_class.h>                          // Include all header files
#include <cmd.h>
#include <class.h>                              // Include class dependent definitions
#include <hash.h>                               // Include hash header file
#include <defprops.h>                           // Include property related definitions
#include <dev.h>
#include <macros.h>                             // For DEVICE_PPM, Test()
#include <fgc_errs.h>
#include <dpcls.h>
#include <property.h>
#include <prop.h>
#include <pars.h>
#include <mem.h>


/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetDefaults(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  SetDefaults provides support for S FGFUNC.DEFAULTS[] action (and its equivalent S FG.FUNC.DEFAULTS[]).
  It will assert the default values in the FGFUNC properties.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      prop_idx;
    uint16_t      chan_idx;
    uint16_t      errnum;

    union
    {
        float * f;
        uint32_t *i;
    } value;

    static struct prop *props[] =
    {
        &PROP_FGFUNC_PLP_ACCELERATION,
        &PROP_FGFUNC_PLP_LINEAR_RATE,
        &PROP_FGFUNC_CTRIM_PERIOD,
        &PROP_FGFUNC_RUN_DELAY,
        NULL
    };

    /* If SUB_DEV property being addressed then use mux_idx instead of array indexes */

    if(p != NULL && Test(p->flags, PF_SUB_DEV))
    {
        c->from = c->to = c->mux_idx;
        c->mux_idx = 0;
    }

    /* For each property that must be reset */

    for(prop_idx = 0; props[prop_idx] != NULL; prop_idx++)
    {
        p = props[prop_idx];

        for(chan_idx = c->from ; chan_idx <= c->to ; chan_idx++)        // For each Channel
        {
            errnum = PropValueGet(c, p, (bool*)NULL, (prop_size_t)chan_idx, (uint8_t**)&value.f);

            if(errnum)
            {
                return errnum;
            }

            switch(prop_idx)
            {
            case 0:     *value.f = fgconf.Default.acceleration[chan_idx];       break;
            case 1:     *value.f = fgconf.Default.linear_rate[chan_idx];        break;
            case 2:     *value.i = 0;                                           break;
            case 3:     *value.i = 0;                                           break;
            }
        }

        errnum = PropBlkSet(c, FGC_N_FGEN_CHANS, true, FGC_FIELDBUS_FLAGS_LAST_PKT);

        if(errnum)
        {
            return errnum;
        }
    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetDevicePPM(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Used with DEVICE.PPM property.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;

    // If DEVICE.PPM has changed

    errnum = ParsSet(c,p);

    if(errnum == 0 && dev.ppm != DEVICE_PPM)
    {
        dpcom.mcu.device_ppm = dev.ppm;
        dev.max_user         = GET_FGC_MAX_USER();
    }

    return(errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetFgFunc(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  SetFgFunc provides support for S FGFUNC FUNC,args,....
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              errnum;
    uint16_t              sym_idx;
    struct sym_lst *    sym_const;

    errnum = ParsScanSymList(c, "Y,", (struct sym_lst *)p->range, &sym_const);

    if(errnum != 0)  // delim = '\0' or ','
    {
        return(errnum);
    }

    sym_idx  = sym_const->sym_idx;

    switch(sym_idx)                     // Switch accord to reference type to process command line args
    {
    case STC_NONE:      return(FGC_BAD_PARAMETER);

    case STC_NOW:
    case STC_PLP:       errnum = RefPlp(c);             break;

    case STC_CTRIM:     errnum = RefCTrim(c);           break;

    case STC_SINE:      errnum = RefSine(c);            break;

    case STC_TABLE:     errnum = RefTable(c);           break;
    }

    if(errnum)                          // If error detected
    {
        return(errnum);                         // Abort request
    }

    dpcls.mcu.ref.type[fgfunc.chan_idx] = sym_const->value;     // Prepare request to arm function

    return(RefSet(c));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetFgFuncType(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  SetFgFuncType allows a reference to be set using preset parameters.  This is used by Java applications
  that cannot set composite properties.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;

    // Check if sub-device state is IDLE
    // If the property is sub-dev, then it means that FG.FUNC.TYPE is being set
    // If the property isn't sub-dev, then FGFUNC.TYPE is being set

    if(( Test(p->flags, PF_SUB_DEV) && dpcls.dsp.fgstate.func[c->mux_idx] != FGC_FG_IDLE) ||
       (!Test(p->flags, PF_SUB_DEV) && dpcls.dsp.fgstate.func[c->from]    != FGC_FG_IDLE))
    {
        return(FGC_BAD_STATE);
    }

    // Set the FG.FUNC.TYPE property with the argument

    errnum = ParsSet(c,p);

    if(errnum)                  // If error report
    {
        MemCpyBytes((uint8_t *)&dpcls.mcu.ref.type[0],                 // Reset type array
                    (uint8_t *)&dpcls.mcu.ref.type_old[0],
                    sizeof(dpcls.mcu.ref.type_old));

        return(errnum);
    }

    return(RefSet(c));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetRelTime(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    RELTIME
\*---------------------------------------------------------------------------------------------------------*/
{
    return(ParsSet(c,p));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetTableFunc(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    POINT properties
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t sub_dev_index = 0;
    uint16_t errnum        = ParsSet(c, p);

    if(Test(p->flags, PF_SUB_DEV))
    {
        // We are setting FG.FUNC.TABLE.FUNCTION property
        // Sub-device index is passed as mux index

        sub_dev_index = c->mux_idx;
    }
    else
    {
        // We are setting FGFUNC.TABLE.CHANx.FUNCTION property
        // We don't have access to sub-device index, so we need to find it

        static const struct prop * channel_functions[] =
        {
            &PROP_FGFUNC_TABLE_CHAN0_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN1_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN2_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN3_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN4_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN5_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN6_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN7_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN8_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN9_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN10_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN11_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN12_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN13_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN14_FUNCTION,
            &PROP_FGFUNC_TABLE_CHAN15_FUNCTION,
        };

        // Find sub device index

        for(sub_dev_index = 0; sub_dev_index < 16; sub_dev_index++)
        {
            if(p == channel_functions[sub_dev_index])
            {
                break;
            }
        }
    }

    if(sub_dev_index > 15)
    {
        return(FGC_BAD_ARRAY_IDX);
    }

    if(dpcls.dsp.fgstate.func[sub_dev_index] != FGC_FG_IDLE)
    {
        return(FGC_BAD_STATE);
    }

    // Set the FG.FUNC.TYPE property with the argument

    dpcls.mcu.ref.type[sub_dev_index] = FGC_REF_TABLE;

    errnum = RefSet(c);

    return(errnum);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: set_class.c
\*---------------------------------------------------------------------------------------------------------*/

