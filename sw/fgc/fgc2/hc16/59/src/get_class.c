/*---------------------------------------------------------------------------------------------------------*\
 File:          get_class.c

 Purpose:       FGC MCU Class 59 Software - Get Command Functions
\*---------------------------------------------------------------------------------------------------------*/

#include <stdbool.h>
#include <get_class.h>                          // Include all header files
#include <cmd.h>
#include <class.h>                              // Include class dependent definitions
#include <hash.h>                               // Include hash header file
#include <property.h>
#include <defprops.h>                           // Include property related definitions
#include <fgc_errs.h>
#include <macros.h>                             // For Test(), DEVICE_PPM
#include <prop.h>


/*---------------------------------------------------------------------------------------------------------*/
uint16_t GetRelTime(struct cmd *c, struct prop *p)
/*---------------------------------------------------------------------------------------------------------*\
  Displays:     RELTIME

  Time is stored as an integer but reported as a float with 4 digits.
\*---------------------------------------------------------------------------------------------------------*/
{
    int8_t               buf[12];
    uint16_t              dec_f;                  // Not HEX get option flag
    uint16_t              dec_nd;                 // Number of digits after decimal point in decimal format
    prop_size_t         n;
    uint16_t              idx;
    uint16_t              nc;
    uint16_t              errnum;
    uint32_t              t;
    uint8_t *             t_ptr;
    int8_t * FAR         timefmt_p;
    int8_t * FAR         neatfmt_p;
    static int8_t * FAR  timefmt[] = { "0x%08lX","%05lu" };
    static int8_t * FAR  neatfmt[] = { "%13s","%s" };

    if(Test(c->getopts,GET_OPT_HEX))
    {
        dec_f     = 0;
        dec_nd    = 0;
        timefmt_p = timefmt[0];
    }
    else
    {
        dec_f     = 1;
        dec_nd    = 4;
        timefmt_p = timefmt[1];
    }

    neatfmt_p = neatfmt[(c->token_delim == ',')];       // Prepare format

    errnum = CmdPrepareGet(c,p,4,&n);
    if(errnum)
    {
        return errnum;
    }
    for(; n-- ; c->from += c->step) // For all elements
    {
        errnum = PropValueGet(c, p, (bool*)NULL, c->from, (uint8_t**)&t_ptr); // Get element value from MCU or DSP property
        if(errnum)
        {
            return errnum;
        }
        t = *((uint32_t*)t_ptr);

        errnum = CmdPrintIdx(c, c->from);

        if(errnum != 0)
        {
            return errnum;
        }

        nc = sprintf(buf+dec_f,timefmt_p,t) - dec_nd;   // Write time to buffer
                                                        // keep one spare byte at the beginning for dec formatting

        if(dec_f)                                       // If not hex
        {
            for(idx = 0; idx < nc; idx++)
            {
                buf[idx] = buf[idx+1];
            }
            buf[idx] = '.';
        }

        fprintf(c->f,neatfmt_p,(char * FAR)buf);

    }

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: get_class.c
\*---------------------------------------------------------------------------------------------------------*/
