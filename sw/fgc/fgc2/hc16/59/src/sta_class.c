/*---------------------------------------------------------------------------------------------------------*\
  File:         sta_class.c

  Purpose:      FGC MCU Class 59 Software - State Functions.

  Author:       Quentin.King@cern.ch

  Notes:        This file contains the functions that record state changes in the event log
\*---------------------------------------------------------------------------------------------------------*/

#define STA_CLASS_GLOBALS   // define vs global variable
#define STA_GLOBALS         // define sta global variable

#include <sta_class.h>                          // Include all header files
#include <sta.h>
#include <class.h>                              // Include class dependent definitions
#include <mcu_dsp_common.h>                     // For struct abs_time_us/ms
#include <fbs_class.h>
#include <crate.h>
#include <rtd.h>
#include <mcu_dependent.h>
#include <memmap_mcu.h>
#include <init_class.h>                         // For rfc data structure
#include <macros.h>                             // for Test(), Set(), Clr()

#pragma MESSAGE DISABLE C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void StaTsk(void *unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the PC State manager task.  It is triggered by OSTskResume() in MstTsk() on millisecond
  2 and 7, provided the FGC is configured.
\*---------------------------------------------------------------------------------------------------------*/
{
    for(;;)                             // Main loop @ 200Hz
    {
        OSTskSuspend();                         // Wait for OSTskResume() from MstTsk();

        sta.sec_f = (SM_MSTIME_P == DEV_STA_TSK_PHASE);

        sta.time_ms  += 5L;                             // Increment state time by 5ms
        sta.timestamp_ms.unix_time = SM_UXTIME_P;       // Calculate iteration timestamp for logging
        sta.timestamp_ms.ms_time   = SM_MSTIME_P;

        AbsTimeCopyFromMsToUs(&sta.timestamp_ms, &sta.timestamp_us);

        if(STATE_OP != FGC_OP_UNCONFIGURED)
        {
            StaOpState();                               // Control operational state
            StaCheck();                                 // Run health checks
            StaLeds();                                  // Set front panel LEDs
            StaEventGroups();                           // Reset Event Groups

            if(!fbs.id ||                               // If FIP not connected (standalone), or
               *((uint16_t*)&sta.timestamp_us.unix_time)) // UTC time received
            {
                LogEvtProp();                                   // Log property changes
            }

            if(fbs.events_rcvd_f)                       // If time var containing events received
            {
                RefEvent();                                     // Treat event table (class dependent)
            }
        }
        else                                    // else unconfigured
        {
            StaCheck();                                 // Run health checks
            StaLeds();                                  // Set front panel LEDs
        }

        sta.watchdog = 0;                       // Flag that StaTsk has completed it's iteration
    }
}
#pragma MESSAGE DEFAULT C5703 // Disable 'parameter declared but not referenced' warning
/*---------------------------------------------------------------------------------------------------------*/
void StaInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the interface and states related to the power converter.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dig_ip2;

    dpcom.mcu.interfacetype = SM_INTERFACETYPE_P;       // Transfer interface type to DSP

    dig_ip2 = DIG_IP2_P;                                // Force word read on secondary digital inputs

    crate.type     =    dig_ip2 & DIG_IP2_BPTYPE_MASK16;
    crate.position = !!(dig_ip2 & DIG_IP2_BPSIDE_MASK16);

    rtd.ctrl              = FGC_RTD_OFF;
    STATE_OP              = FGC_OP_UNCONFIGURED;
}
/*---------------------------------------------------------------------------------------------------------*/
void StaOpState(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to control the operational state.
\*---------------------------------------------------------------------------------------------------------*/
{
    STATE_OP = sta.mode_op;                       // Set the state to equal the mode

    if(STATE_OP != (uint16_t)dpcom.mcu.state_op)  // If state has changed
    {
        dpcom.mcu.state_op = STATE_OP;                  // Copy state to DPRAM
    }

    if(STATE_OP == FGC_OP_SIMULATION)
    {
        Set(WARNINGS, FGC_WRN_SIMULATION);
    }
    else
    {
        Clr(WARNINGS, FGC_WRN_SIMULATION);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StaLeds(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set some of the LEDs.  This must be done with LED_SET() and LED_RST() macros, which
  are atomic and not vulnerable to interrupts.  The function also sets the associated bits in the unlatched
  status: NONE_ACK, ALL_ACK, CRC_BAD and LOOP_BROKEN
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      vs_leds;

    /*--- PSU LED ---*/

    if(Test(WARNINGS,FGC_WRN_FGC_PSU))
    {
        LED_SET(PSU_RED);
        LED_SET(PSU_GREEN);
    }
    else
    {
        LED_RST(PSU_RED);
        LED_SET(PSU_GREEN);
    }

    /*--- ALL ACK - GREEN = All Ack   ORANGE = Some Ack   RED = No Ack ---*/

    if(TRIGGER & ~rfc.not_ack_10s)
    {
        LED_SET(VS_GREEN);
    }
    else
    {
        LED_RST(VS_GREEN);
    }

    if(TRIGGER && rfc.not_ack_10s)
    {
        LED_SET(VS_RED);
    }
    else
    {
        LED_RST(VS_RED);
    }

    vs_leds = (dev.leds & (RGLEDS_VS_RED_MASK16|RGLEDS_VS_GREEN_MASK16));

    if(vs_leds == RGLEDS_VS_GREEN_MASK16)
    {
        Set(ST_UNLATCHED,FGC_UNL_ALL_ACK);
    }
    else
    {
        Clr(ST_UNLATCHED,FGC_UNL_ALL_ACK);
    }

    if(vs_leds == RGLEDS_VS_RED_MASK16)
    {
        Set(ST_UNLATCHED,FGC_UNL_NONE_ACK);
    }
    else
    {
        Clr(ST_UNLATCHED,FGC_UNL_NONE_ACK);
    }

    /*--- LOOP OPEN ---*/

    if(TRIGGER && dpcls.dsp.loop_f)
    {
        LED_SET(DCCT_RED);
        Set(ST_UNLATCHED,FGC_UNL_LOOP_BROKEN);
    }
    else
    {
        LED_RST(DCCT_RED);
        Clr(ST_UNLATCHED,FGC_UNL_LOOP_BROKEN);
    }

    if(TRIGGER && !dpcls.dsp.loop_f)
    {
        LED_SET(DCCT_GREEN);
    }
    else
    {
        LED_RST(DCCT_GREEN);
    }

    /*--- CRC ERR ---*/

    if(TRIGGER && dpcls.dsp.crc_err_10s_f)
    {
        LED_SET(PIC_RED);
        Set(ST_UNLATCHED,FGC_UNL_CRC_BAD);
    }
    else
    {
        LED_RST(PIC_RED);
        Clr(ST_UNLATCHED,FGC_UNL_CRC_BAD);
    }

    if(TRIGGER && !dpcls.dsp.crc_err_10s_f)
    {
        LED_SET(PIC_GREEN);
    }
    else
    {
        LED_RST(PIC_GREEN);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StaCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to check the operational health of the system.
\*---------------------------------------------------------------------------------------------------------*/
{
    /*--- Warnings ---*/

    if(Test(ST_LATCHED,(FGC_LAT_RFC_FLT||FGC_LAT_MEM_FLT)))
    {
        Set(WARNINGS,FGC_WRN_FGC_HW);
    }
    else
    {
        Clr(WARNINGS,FGC_WRN_FGC_HW);
    }

    if(Test(ST_LATCHED,(FGC_LAT_FGC_PSU_FAIL|FGC_LAT_PSU_V_FAIL|FGC_LAT_VDC_FAIL)))
    {
        Set(WARNINGS,FGC_WRN_FGC_PSU);
    }
    else
    {
        Clr(WARNINGS,FGC_WRN_FGC_PSU);
    }

    /*--- Latch new faults ---*/

    FAULTS |= sta.faults;
}
/*---------------------------------------------------------------------------------------------------------*/
void StaEventGroups(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to check reset the event group for each channel once it returns to IDLE.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      chan_idx;

    /*--- Reset Event Groups to zero for channels that are IDLE ---*/

    for(chan_idx=0;chan_idx < FGC_N_FGEN_CHANS;chan_idx++)
    {
        if(dpcls.dsp.fgstate.func[chan_idx] == FGC_FG_IDLE)
        {
            fgfunc.event_group[chan_idx] = 0;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sta_class.c
\*---------------------------------------------------------------------------------------------------------*/

