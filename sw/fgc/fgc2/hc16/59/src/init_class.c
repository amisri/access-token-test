/*---------------------------------------------------------------------------------------------------------*\
  File:         init_class.c

  Purpose:      FGC MCU Software - initialisation functions for class 59 (RF_FGEN)

  Author:       Quentin.King@cern.ch

\*---------------------------------------------------------------------------------------------------------*/

#define INIT_CLASS_GLOBALS

#include <init_class.h>                         // Include all header files
#include <cmd.h>
#include <nvs.h>
#include <class.h>                              // Include class dependent definitions
#include <hash.h>                               // Include hash header file
#include <property.h>
#include <defprops.h>                           // Include property related definitions
#include <dpcom.h>
#include <dev.h>
#include <memmap_mcu.h>
#include <ref_class.h>                          // For RefReset()
#include <macros.h>

/*---------------------------------------------------------------------------------------------------------*/
void InitClassPreInts(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called before interrupts or the DSP are started.  It will prepare the RFC display.
\*---------------------------------------------------------------------------------------------------------*/
{
    // TODO Shouldn't these be chars?

    static uint8_t spinner_left [8] = { '<', '-', '<', '/', '<', '|', '<', '\\' };
    static uint8_t spinner_right[8] = { '-', '>', '\\', '>', '|', '>', '/', '>' };

    fgfunc.err_chan_idx = -1;                                   // Set to -1 to say "no channel"

    dpcls.mcu.rfc_reset = RFC_RESET_TIME;                       // Suppress access to RFC during reset

    /*--- Prepare the alpha numeric display ---*/

    if(crate.position == FGC_CRATE_POS_LEFT)                    // Prepare display with spinner
    {
        rfc.spinner      = (uint16_t *)&spinner_left;
        rfc.spinner_addr = RFC_DISPLAY_16;
    }
    else
    {
        rfc.spinner      = (uint16_t *)&spinner_right;
        rfc.spinner_addr = RFC_DISPLAY_16 + 6;
    }
}



void InitClassSpyMpx(struct cmd * c)
{
    static uint32_t init_spy_mpx[FGC_N_SPY_CHANS] = {     0,1,2,3,4,5     };

    PropSet(c, &PROP_SPY_FGMPX, init_spy_mpx, FGC_N_SPY_CHANS);
}



/*---------------------------------------------------------------------------------------------------------*/
void InitClassPostInts(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the channel names from the NAMEDB in Flash BB_5.  It also prepares
  the RFC alpha numeric display.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint16_t      dev_idx;
    uint16_t      num_sub_devs;
    uint8_t * FAR num_sub_devs_fp;

    struct fgc_sub_dev_name * FAR sub_dev_name_fp;

    NvsInit(init_prop);                                        // Initialise non-volatile properties

    dpcom.mcu.device_ppm = dev.ppm;
    dev.max_user         = GET_FGC_MAX_USER();

    /*--- If connected to FIP then search for channel/sub-device names in NAMEDB ---*/

    if(fbs.id)                                  // If FIP is connected
    {
        sub_dev_name_fp = &((struct fgc_name_db * far)CODES_NAMEDB_32)->sub_dev[0];
        num_sub_devs_fp = &((struct fgc_name_db * far)CODES_NAMEDB_32)->num_sub_devs[1];

        for(dev_idx=1 ; dev_idx < fbs.id ; dev_idx++)
        {
            sub_dev_name_fp += *(num_sub_devs_fp++);
        }

        num_sub_devs = *num_sub_devs_fp;

        for(i=0 ; i < num_sub_devs ; i++,sub_dev_name_fp++)
        {
            fgconf.chan.name[sub_dev_name_fp->index-1] = &sub_dev_name_fp->name[0];
        }
    }

    /*--- Start logging ---*/

    LogStartAll();
}
/*---------------------------------------------------------------------------------------------------------*/
void InitClassPostSync(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  Class specific initialisation after FGC has been synchronised
\*---------------------------------------------------------------------------------------------------------*/
{
    RefReset(c);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: init_class.c
\*---------------------------------------------------------------------------------------------------------*/
