/*---------------------------------------------------------------------------------------------------------*\
 File:          rtd_class.c

 Purpose:       FGC MCU Class 59 Software - Real-time display Functions.

 Notes:         Display Layout:

                          1         2         3         4         5         6         7         8
                +---------+---------+---------+---------+---------+---------+---------+---------++
            21  |States:PL.OP     00:ST.RL:+nnnnn 01:ST.RL:+nnnnn 02:ST.RL:+nnnnn 03:ST.RL:+nnnnn|
            22  |FWS:H/HRFC/MUFDF 04:ST.RL:+nnnnn 05:ST.RL:+nnnnn 06:ST.RL:+nnnnn 07:ST.RL:+nnnnn|
            23  |T:23.42:45.32    08:ST.RL:+nnnnn 09:ST.RL:+nnnnn 10:ST.RL:+nnnnn 11:ST.RL:+nnnnn|
            24  |Cpu:nn:nn%       12:ST.RL:+nnnnn 13:ST.RL:+nnnnn 14:ST.RL:+nnnnn 15:ST.RL:+nnnnn|
                +---------+---------+---------+---------+---------+---------+---------+---------++
\*---------------------------------------------------------------------------------------------------------*/

#define RTD_CLASS_GLOBALS

#include <stdbool.h>
#include <rtd_class.h>                          // Include all header files
#include <class.h>                              // Include class dependent definitions
#include <trm.h>
#include <init_class.h>
#include <serial_stream.h>
#include <rtd.h>
#include <mem.h>
#include <definfo.h>            // for FGC_CLASS_ID

#define CLASS_FIELD             c59

/*---------------------------------------------------------------------------------------------------------*/
void RtdInit(FILE *f)
/*---------------------------------------------------------------------------------------------------------*\
  This function prepares the screen for the 4 line real-time display.  It uses the following vt100
  control codes (ESC='\33'):

      ESC [y;xH         Move cursor to (x,y)
      ESC [H            Move cursor to top left of screen
      ESC [1;19r        Set scroll window to lines 1 to 19
\*---------------------------------------------------------------------------------------------------------*/
{
    /*--- Initialise RTD display area ---*/

    fputs("\33[21;1H",f);                       // Move to RTD display area

    //    0         10        20        30        40        50        60        70        80
    //    |---------+---------+---------+---------+---------+---------+---------+---------+|
    fputs("States:__.__     00:__.__:______ 01:__.__:______ 02:__.__:______ 03:__.__:______\n\r",f);
    fputs("FWS:_/____/_____ 04:__.__:______ 05:__.__:______ 06:__.__:______ 07:__.__:______\n\r",f);
    fputs("T:__.__:__.__    08:__.__:______ 09:__.__:______ 10:__.__:______ 11:__.__:______\n\r",f);
    fputs("Cpu:__:__%       12:__.__:______ 13:__.__:______ 14:__.__:______ 15:__.__:______",f);

    fputs("\33[H\33[1;19r\v",f);                // Set up scroll window to protect RTD lines

    /*--- Initialise RTD variables ---*/

    MemSetBytes((void*)&rtd.last, 0xFF, sizeof(rtd.last));  // Reset all last value variables
    rtd.idx      = 0;                                       // Reset field counter
    rtd_cls.base = 10;                                      // Base 10 by default

    if(rtd.ctrl != FGC_RTD_OFF)                             // If option line is active
    {
        rtd.last_ctrl = FGC_RTD_OFF;                        // Reset last choice value to trigger reset
    }

    MemSetBytes((void*)&rtd_cls.last, 0xAAAA, sizeof(rtd_cls.last));
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdStates(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the device states in abbreviated form on the screen.
\*---------------------------------------------------------------------------------------------------------*/
{
    int8_t *             p;
    static int8_t        pll_str[] = "LKCPFSNSFL";
    static int8_t        op_str[]  = "UCNLSMCLTTBTPRSO";

    if(fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll != rtd.last.state_pll ||                       // If a state has changed
       STATE_OP  != rtd.last.state_op)
    {
        rtd.last.state_pll = fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll;                         // Remember states
        rtd.last.state_op  = STATE_OP;

        fprintf(rtd.f,RTD_START_POS,RTD_STATES_POS);            // Move cursor

        p = pll_str + 2 * rtd.last.state_pll;                   // Display PLL state
        SerialStreamInternal_WriteChar(*(p++),rtd.f);
        SerialStreamInternal_WriteChar(*p,rtd.f);

        SerialStreamInternal_WriteChar('.',rtd.f);
        p = op_str + 2 * rtd.last.state_op;                     // Display operational state
        SerialStreamInternal_WriteChar(*(p++),rtd.f);
        SerialStreamInternal_WriteChar(*p,rtd.f);

        fputs(RTD_END,rtd.f);                                   // Restore cursor position

        return(true);
    }

    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdFGenChan(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the channel status and value on the RTD.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              end_field_idx;
    uint16_t              chan_idx;               // 0 - 15
    static uint16_t       field_idx;              // 0 - 31  (Two fields per channel: status and value)
    static uint16_t       chan_mask[] = { 0x0001,0x0002,0x0004,0x0008,0x0010,0x0020,0x0040,0x0080,
                                        0x0100,0x0200,0x0400,0x0800,0x1000,0x2000,0x4000,0x8000 };
    end_field_idx = field_idx;

    do
    {
        field_idx = (field_idx + 1) & 31;       // 0 - 31
        chan_idx  = field_idx / 2;              // 0 - 15

        if(!(field_idx & 1))
        {
            if(RtdFGenChanState(chan_idx, chan_mask[chan_idx]))
            {
                return(true);
            }
        }
        else
        {
            if(RtdFGenChanValue(chan_idx, chan_mask[chan_idx]))
            {
                return(true);
            }
        }
    } while(field_idx != end_field_idx);

    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdFGenChanState(uint16_t chan_idx, uint16_t chan_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the channel state and status.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              state;
    uint16_t              rt_ctrl;
    uint16_t              lim_any_10s;
    int8_t               pos[6];
    int8_t *             p;
    static int8_t        state_str[] = "OFILARRNABNA";

    state = (uint16_t)dpcls.dsp.fgstate.func[chan_idx];

    if(state == FGC_FG_OFF)                                     // If state is now OFF
    {
        if(rtd_cls.last.fgstate_func[chan_idx] != FGC_FG_OFF)   // If state has just changed
        {
            sprintf(pos,"%u;%u",                                // Move cursor
                    RTD_CHAN_ORIGIN_ROW + chan_idx/4,
                    RTD_CHAN_ORIGIN_COL + RTD_CHAN_WIDTH_COL * (chan_idx & 0x3));
            fprintf(rtd.f,RTD_START_POS,(char * FAR)pos);
            fputs("OF" RTD_CUR_RIGHT "  " RTD_CUR_RIGHT "      ",rtd.f);// Clear channel completely
            fputs(RTD_END,rtd.f);                                       // Restore cursor position

            rtd_cls.last.fgstate_func[chan_idx] = FGC_FG_OFF;   // Remember state
            rtd_cls.last.fgfunc_ival [chan_idx] = 100000;       // Set impossible ival
            return(true);                                       // Report field changed
        }
        return(false);                                          // else skip to next channel
    }

    rt_ctrl     = !!(chan_mask & dpcls.dsp.rt_ctrl);
    lim_any_10s = !!(chan_mask & dpcls.dsp.lim_any_10s);

    if(state       != rtd_cls.last.fgstate_func[chan_idx] ||
       rt_ctrl     != rtd_cls.last.rt_ctrl     [chan_idx] ||
       lim_any_10s != rtd_cls.last.lim_any_10s [chan_idx])
    {
        sprintf(pos,"%u;%u",                                    // Move cursor
                RTD_CHAN_ORIGIN_ROW + chan_idx/4,
                RTD_CHAN_ORIGIN_COL + RTD_CHAN_WIDTH_COL * (chan_idx & 0x3));
        fprintf(rtd.f,RTD_START_POS,(char * FAR)pos);

        p = state_str + 2 * state;                              // Display channel state
        SerialStreamInternal_WriteChar(*(p++),rtd.f);
        SerialStreamInternal_WriteChar(*p,rtd.f);
        fputs(RTD_CUR_RIGHT,rtd.f);

        if(rt_ctrl)
        {
            fputs(TERM_REVERSE "R",rtd.f);
        }
        else
        {
            SerialStreamInternal_WriteChar('R',rtd.f);
        }

        if(lim_any_10s)
        {
            fputs(TERM_REVERSE "L",rtd.f);
        }
        else
        {
            fputs(TERM_NORMAL TERM_BOLD "L",rtd.f);
        }

        fputs(RTD_END,rtd.f);                                   // Restore cursor position
        rtd_cls.last.fgstate_func[chan_idx] = state;
        rtd_cls.last.rt_ctrl     [chan_idx] = rt_ctrl;
        rtd_cls.last.lim_any_10s [chan_idx] = lim_any_10s;
        return(true);
    }

    return(false);
}
/*---------------------------------------------------------------------------------------------------------*/
bool RtdFGenChanValue(uint16_t chan_idx, uint16_t chan_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function will display the channel value provided the channel is not OFF.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              not_ack_10s;
    int32_t              fgfunc_ival;
    int8_t               pos[6];

    not_ack_10s = !!(chan_mask & dpcls.dsp.not_ack_10s);
    fgfunc_ival = fgfunc.ival[chan_idx];

    if((uint16_t)dpcls.dsp.fgstate.func[chan_idx] != FGC_FG_OFF  &&
      (not_ack_10s  != rtd_cls.last.not_ack_10s[chan_idx] ||
       fgfunc_ival  != rtd_cls.last.fgfunc_ival[chan_idx] ||
       rtd_cls.base != rtd_cls.last.base       [chan_idx]))
    {
        sprintf(pos,"%u;%u",                                // Move cursor
                RTD_CHAN_ORIGIN_ROW + chan_idx/4,
                RTD_CHAN_ORIGIN_COL + RTD_CHAN_WIDTH_COL * (chan_idx & 0x3) + 6);
        fprintf(rtd.f,RTD_START_POS,(char * FAR)pos);       // Move cursor

        if(not_ack_10s)
        {
            fputs(TERM_REVERSE,rtd.f);
        }

        if(rtd_cls.base == 10)
        {
            fprintf(rtd.f,"%+06d",(int16_t)fgfunc_ival);     // Report ival in decimal
        }
        else
        {
            fprintf(rtd.f,"0x%04X",(uint16_t)fgfunc_ival);    // Report ival in hex
        }

        fputs(RTD_END,rtd.f);                               // Restore cursor position

        rtd_cls.last.not_ack_10s[chan_idx] = not_ack_10s;
        rtd_cls.last.fgfunc_ival[chan_idx] = fgfunc_ival;
        rtd_cls.last.base       [chan_idx] = rtd_cls.base;

        return(true);
    }

    return(false);
 }
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rtd_class.c
\*---------------------------------------------------------------------------------------------------------*/
