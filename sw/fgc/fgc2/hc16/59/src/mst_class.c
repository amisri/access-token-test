/*---------------------------------------------------------------------------------------------------------*\
  File:		mst_class.c

  Purpose:	FGC MCU Class 59 Software - Class related millisecond actions
\*---------------------------------------------------------------------------------------------------------*/

#include <mst_class.h>		                // Include all header files
#include <stdbool.h>
#include <class.h>				// Include class dependent definitions
#include <log_class.h>
#include <dev.h>
#include <macros.h>                             // for Test(), Set(), Clr()

/*---------------------------------------------------------------------------------------------------------*/
void MstClass(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the millisecond task, MstTsk(), to perform the class related millisecond
  activities.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* Every Millisecond: Log CVAL */

    if(log_cval.samples_to_acq)		// If CVAL logging samples still to acquire
    {
	LogCval();
    }

    if(log_ival.samples_to_acq)		// If IVAL logging samples still to acquire
    {
	LogIval();
    }

    /* Millisecond 5 of 20: Log Iearth signal */

    if(mst.ms_mod_20 == LOG_WVAL_PHASE)
    {
	if(log_wval.samples_to_acq)		// If WVAL logging samples still to acquire
	{
	    LogWval();
	}
    }

    /* Millisecond 1 of 20: Read C32 status and manage PM state/request */

    else if(mst.ms_mod_20 == 1)
    {
	ST_LATCHED     |= (uint16_t)dpcom.dsp.st_latched;		// Latch status from C32 (PSU V FAIL)
	TRIGGER		= dpcls.dsp.trigger;
	FUNC_IDLE	= dpcls.dsp.idle;
	FUNC_ARMED	= dpcls.dsp.armed;
	FUNC_RUNNING	= dpcls.dsp.running;
	FUNC_ABORTING	= dpcls.dsp.aborting;
	FUNC_NOT_ACK	= dpcls.dsp.not_ack;
	RT_CONTROL	= dpcls.dsp.rt_ctrl;
	START_EVENT	= dpcls.dsp.start_evt;
	ABORT_EVENT	= dpcls.dsp.abort_evt;
	LIMIT_RT_POS	= dpcls.dsp.lim_rt_pos;
	LIMIT_RT_NEG	= dpcls.dsp.lim_rt_neg;
	LIMIT_CHAN_MAX	= dpcls.dsp.lim_chan_max;
	LIMIT_CHAN_MIN	= dpcls.dsp.lim_chan_min;
	LIMIT_RATE_POS	= dpcls.dsp.lim_rate_pos;
	LIMIT_RATE_NEG	= dpcls.dsp.lim_rate_neg;

        rfc.ack         = dpcls.dsp.ack;			// FGSTATUS.ACK  (not published)
	rfc.nack        = dpcls.dsp.nack;			// FGSTATUS.NACK (not published)
	rfc.not_ack_10s = dpcls.dsp.not_ack_10s;

	if(rfc.not_ack_10s              ||
           rfc.warning                  ||
	  (uint16_t)dpcls.dsp.loop_f	||
	  (uint16_t)dpcls.dsp.crc_err_10s_f	||
	  (uint16_t)dpcls.mcu.rfc_reset)
	{
	    Set(WARNINGS,FGC_WRN_RF_FGEN_COMMS);
	}
	else
	{
	    Clr(WARNINGS,FGC_WRN_RF_FGEN_COMMS);
	}

	rfc.warning = 0;			// Set by IsrMst() if RFC tx counter watchdog fails

	if(fbs.id &&					// If FIP connected, and
	   dev.log_pm_state == FGC_LOG_STOPPING &&	// PM log is stopping, and
	  !Test(ST_UNLATCHED,FGC_UNL_POST_MORTEM))	// Post mortem not yet requested
	{
	    Set(ST_UNLATCHED,FGC_UNL_POST_MORTEM);		// Set post mortem flag
	    Set(fbs.u.fieldbus_stat.ack,FGC_SELF_PM_REQ);	// Request self-trig PM dump by GW
	}
    }

    /* At the end of an RFC reset clear the display */

    if((uint16_t)dpcls.mcu.rfc_reset == 1)
    {
	RFC_DISPLAY_A[0] = 0x2020;		// Write spaces to characters
	RFC_DISPLAY_A[3] = 0x2020;		// 0,1, 6 and 7
    }

    /* Rotate spinner at 20Hz */

    if((SM_MSTIME_P % 50) == 8  && !(uint16_t)dpcls.mcu.rfc_reset)
    {
	RFC_DISPLAY_TRIG_P = dpcls.dsp.trig_str;

	asm
	{
	    INCW	rfc.spinner_idx
	    LDE		rfc.spinner_idx
	    ANDE	#0x0003
	    ASLE
	    LDX		rfc.spinner
	    LDY		rfc.spinner_addr
	    LDD		E,X
	    STD		0,Y
	}
    }

    /* Millisecond 0 of 1000: Check if logging has finished */

    else if(!SM_MSTIME_P)
    {
	if(dev.log_pm_state == FGC_LOG_STOPPING && 		// If PM log state is STOPPING, and
	   !log_ival.samples_to_acq)				// IVAL log has stopped
	{
	    Clr(fbs.u.fieldbus_stat.ack,FGC_SELF_PM_REQ);		// Reset self-trig PM dump request
	    dev.log_pm_state = FGC_LOG_FROZEN;				// Set PM Log state to Frozen
	}
    }

    /* Dico alive */

    mst.dicoalive = true;					// Enable dico alive for digital interface
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mst_class.c
\*---------------------------------------------------------------------------------------------------------*/
