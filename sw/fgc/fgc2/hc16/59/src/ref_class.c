/*---------------------------------------------------------------------------------------------------------*\
 File:          ref_class.c

 Purpose:       FGC MCU Software Class 59 - Reference conversion functions

 Author:        Quentin.King@cern.ch
\*---------------------------------------------------------------------------------------------------------*/

#define REF_CLASS_GLOBALS

#include <stdbool.h>
#include <ref_class.h>                          // Include all header files
#include <cmd.h>
#include <class.h>                              // Include class dependent definitions
#include <hash.h>                               // Include hash header file
#include <property.h>
#include <defprops.h>                           // Include property related definitions
#include <dev.h>
#include <fgc_errs.h>
#include <property.h>
#include <mcu_dsp_common.h>
#include <prop.h>
#include <pars.h>
#include <math.h>


/*---------------------------------------------------------------------------------------------------------*/
void RefEvent(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the State task when a time variable has been received.  The time variable
  contains the event table from the gateway which must be checked.  This is class dependent so it is
  treated here.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t           slot_idx;
    uint16_t           chan_idx;
    uint16_t           event_group;
    struct fgc_event * event;

    event = fbs.time_v.event;

    for(slot_idx=0 ; slot_idx < FGC_NUM_EVENT_SLOTS ; slot_idx++, event++)
    {
        switch(event->type)
        {
        case FGC_EVT_PM:                                // Global Post Mortem Event - Use SELF trigger

            if(dev.log_pm_state == FGC_LOG_RUNNING)             // If logging is running
            {
                dev.log_pm_state = FGC_LOG_STOPPING;                    // Set PM state to STOPPING
                log_cval.run_f   = false;                               // Stop logging
                log_ival.run_f   = false;
                log_wval.run_f   = false;
            }
            break;

        case FGC_EVT_START:                             // Start reference Event

            event_group = event->payload;

            for(chan_idx=0;chan_idx < FGC_N_FGEN_CHANS;chan_idx++)
            {
                if(!dpcls.mcu.ref.start_event_delay[chan_idx] &&         // If event is not yet active and
                  (!event_group ||                                      // event group is zero or
                    event_group == fgfunc.event_group[chan_idx]))       // matches channel's event group
                {
                    dpcls.mcu.ref.start_event_delay[chan_idx] = event->delay_us / 1000; // Pass event to DSP
                }
            }
            break;

        case FGC_EVT_ABORT:                             // Abort reference Event

            event_group = event->payload;

            for(chan_idx=0;chan_idx < FGC_N_FGEN_CHANS;chan_idx++)
            {
                if(!dpcls.mcu.ref.abort_event_delay[chan_idx] &&        // If event is not yet active and
                  (!event_group ||                                      // event group is zero or
                    event_group == fgfunc.event_group[chan_idx]))       // matches channel's event group
                {
                    dpcls.mcu.ref.abort_event_delay[chan_idx] = event->delay_us / 1000; // Pass event to DSP
                }
            }
            break;
        }
    }

    fbs.events_rcvd_f = false;
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefSet(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when a user runs the command S FGFUNC or S FGFUNC.TYPE.  It will try to arm
  all the outstanding functions (there can be many if S FGFUNC.TYPE is used to write the whole array).
  For each request it asks the DSP to process the function.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum = 0;
    uint16_t      chan_idx;
    uint16_t      ref_type;

    for(chan_idx=0;!errnum && chan_idx < FGC_N_FGEN_CHANS;chan_idx++)
    {
        ref_type = dpcls.mcu.ref.type[chan_idx];
        fgfunc.err_chan_idx = chan_idx;

        if(ref_type != dpcls.mcu.ref.type_old[chan_idx])
        {
            if(ref_type == FGC_REF_NOW   ||             // If function is a known type
               ref_type == FGC_REF_PLP   ||
               ref_type == FGC_REF_SINE  ||
               ref_type == FGC_REF_CTRIM ||
               ref_type == FGC_REF_TABLE)
            {
                errnum = PropBufWait(c);                         // Wait for property block buffer to be free
                if(errnum)
                {
                    return errnum;
                }

                OS_ENTER_CRITICAL();

                dpcom.dsp.bg_complete_f = false;        // Clear job completed flag
                dpcls.mcu.ref.chan_idx  = chan_idx;     // Tell DSP which channel to process
                dpcls.mcu.ref.change_f  = true;         // Trigger DSP background processing (bgp)

                while(!(uint16_t)dpcom.dsp.bg_complete_f)   // While DSP busy
                {
                    OSTskSuspend();                             // Sleep till MstTsk() resumes next ms
                }

                OS_EXIT_CRITICAL();

                errnum = (uint16_t)dpcom.dsp.errnum;      // Record DSP errnum
            }
            else                                // else request is not a valid function
            {
                errnum = FGC_BAD_PARAMETER;             // Report BAD PARAMETER
            }
        }
    }

    if(errnum)                                  // If error detected
    {
        for(chan_idx--;chan_idx < FGC_N_FGEN_CHANS;chan_idx++)  // Cancel function requests
        {
            dpcls.mcu.ref.type[chan_idx] = dpcls.mcu.ref.type_old[chan_idx];
        }
    }
    else                                        // else no error
    {
        fgfunc.err_chan_idx = -1;                       // report no error channel
    }

    return(errnum);                             // Report status
}
/*---------------------------------------------------------------------------------------------------------*/
void RefReset(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when CONFIG.STATE is reset to assert the default values for the FGFUNC functions
  (PLP, CTRIM).
\*---------------------------------------------------------------------------------------------------------*/
{
    c->from = 0;                        // Assert defaults for all channels
    c->to   = FGC_N_FGEN_CHANS-1;
    SetDefaults(c, &PROP_FGFUNC_DEFAULTS);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefPlp(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the user enters "s ref plp,..." to process the plp reference setup.
  Parameters are:

        s fgfunc plp, chan_idx, final, acceleration, linear_rate

  Defaults are:         final           reference now
                        acceleration    FGCONF.DEFAULT.ACCELERATION[chan_idx]
                        linear_rate     FGCONF.DEFAULT.LINEAR_RATE[chan_idx]
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;

    static struct prop *p[] =
    {
        &PROP_FGFUNC_PLP_FINAL,
        &PROP_FGFUNC_PLP_ACCELERATION,
        &PROP_FGFUNC_PLP_LINEAR_RATE,
    };

    /*--- Get chan index ---*/

    errnum = RefGetChanIdx(c);

    if(errnum != 0)
    {
        return(errnum);
    }

    /*--- Prepare default values ---*/

    fgfunc.pars.f[0] = fgfunc.cval[fgfunc.chan_idx];                    // FINAL
    fgfunc.pars.f[1] = fgconf.Default.acceleration[fgfunc.chan_idx];    // ACCELERATION
    fgfunc.pars.f[2] = fgconf.Default.linear_rate[fgfunc.chan_idx];     // LINEAR_RATE

    /*--- Get up to three parameters (final,acceleration,linear_rate) ---*/

    errnum = RefGetPars(c, p, fgfunc.pars.f, 3);

    if(errnum != 0)
    {
        return(errnum);
    }

    /*--- Set properties ---*/

    return(RefSetPars(c, p, fgfunc.pars.f, 3));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefCTrim(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the user enters "s ref ctrim,..." to process a ctrim reference function
  setup.  Parameters are:

        s fgfunc ctrim, chan_idx, final, period

  Defaults are:         final           reference now
                        period          0 (as fast as possible)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;

    static struct prop *p[] =
    {
        &PROP_FGFUNC_CTRIM_FINAL,
        &PROP_FGFUNC_CTRIM_PERIOD,
    };

    /*--- Get chan index ---*/

    errnum = RefGetChanIdx(c);

    if(errnum != 0)
    {
        return(errnum);
    }

    /*--- Prepare default values ---*/

    fgfunc.pars.f[0] = fgfunc.cval[fgfunc.chan_idx];            // FINAL
    fgfunc.pars.f[1] = 0.0;                                     // PERIOD

    /*--- Get up to two parameters (final,period) ---*/

    errnum = RefGetPars(c, p, fgfunc.pars.f, 2);

    if(errnum != 0)
    {
        return(errnum);
    }

    /*--- Set properties ---*/

    fgfunc.pars.i[1] = RefRelTime(fgfunc.pars.f[1]);            // Convert period to RelTime

    return(RefSetPars(c, p, fgfunc.pars.f, 2));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefSine(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the user enters "s ref sine,..." to process the sine function setup.
  Parameters are:

        s fgfunc sine, chan_idx, amplitude, num_cycles, period

  Defaults are:         amplitude       1.0
                        num_cycles      1.0
                        period          1.0
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;

    static struct prop *p[] =
    {
        &PROP_FGFUNC_SINE_AMPLITUDE,
        &PROP_FGFUNC_SINE_NUM_CYCLES,
        &PROP_FGFUNC_SINE_PERIOD
    };

    /*--- Get chan index ---*/

    errnum = RefGetChanIdx(c);

    if(errnum != 0)
    {
        return(errnum);
    }

    /*--- Prepare default values ---*/

    fgfunc.pars.f[0] = 1.0;
    fgfunc.pars.f[1] = 1.0;
    fgfunc.pars.f[2] = 1.0;

    /*--- Get up to three parameters (amp,n_cyc,period) ---*/

    errnum = RefGetPars(c, p, fgfunc.pars.f, 3);

    if(errnum != 0)
    {
        return(errnum);
    }

    /*--- Set properties ---*/

    fgfunc.pars.i[2] = RefRelTime(fgfunc.pars.f[2]);            // Convert PERIOD to RelTime

    return(RefSetPars(c, p, fgfunc.pars.f, 3));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefTable(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the user enters "s fgfunc table,...".  Parameters are:

        s fgfunc table, chan_idx
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;

    errnum = RefGetChanIdx(c);

    if(errnum != 0)             // If chan_idx is not valid
    {
        return(errnum);                                 // Report error
    }

    if(!c->last_par_f)                          // If more parameters remain
    {
        c->device_par_err_idx = 2;
        return(FGC_BAD_PARAMETER);                      // Report BAD PARAMETER
    }

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefGetChanIdx(struct cmd *c)
/*---------------------------------------------------------------------------------------------------------*\
  This function will get the first parameter and confirm that it is an integer in the range from 0-15.
  Although the channel index parameter must be an integer, it's necessary to convert it as a floating
  point value because the following parameters are floats.  The ParsScanStd() will convert all the
  parameters in one go and cache the results, even though it only returns one at a time.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;
    float *     value;
    float       int_part;

    errnum = ParsScanStd(c, &PROP_FGFUNC_PLP_FINAL, PKT_FLOAT, (void **)&value);// Use PLP_FINAL as it has
                                                                                // the widest limits
    if(errnum == FGC_NO_SYMBOL)
    {
        return(FGC_BAD_ARRAY_IDX);
    }

    if(modff(*value, &int_part) != 0.0)                         // If not an integer
    {
        return(FGC_BAD_INTEGER);                                        // Report BAD INTEGER
    }

    fgfunc.chan_idx = (uint16_t)(int16_t)*value;                           // Make channel index available

    if(fgfunc.chan_idx < 0 || fgfunc.chan_idx > 15)             // If not a valid channel index
    {
        return(FGC_OUT_OF_LIMITS);                                      // Report OUT OF LIMITS
    }

    if(dpcls.dsp.fgstate.func[fgfunc.chan_idx] != FGC_FG_IDLE)  // If function isn't IDLE
    {
        return(FGC_BAD_STATE);                                          // Reject request
    }

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefGetPars(struct cmd *c, struct prop **p, float *pars, uint16_t n_pars)
/*---------------------------------------------------------------------------------------------------------*\
  This function will get and checkthe required number of reference property parameters.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      i;
    uint16_t      errnum;
    float *     newval;
    float       value;
    struct prop * pr;

    for(i=0;i < n_pars && !c->last_par_f;p++,i++,pars++)
    {
        errnum = ParsScanStd(c, &PROP_FGFUNC_PLP_FINAL, PKT_FLOAT, &newval);    // Use PLP_FINAL as it has
                                                                                // the widest limits
        if(errnum != FGC_NO_SYMBOL)
        {
            if(errnum)
            {
                if(i)                                   // If not first parameter
                {
                    c->device_par_err_idx = i;                  // Set err_idx to parameter idx
                }
                return(errnum);
            }

            value = *newval;
            pr    = *p;

            if(value < ((struct float_limits *)pr->range)->min ||
               value > ((struct float_limits *)pr->range)->max)
            {
                c->device_par_err_idx = i;
                return(FGC_OUT_OF_LIMITS);                              // Report OUT OF LIMITS
            }

            *pars = value;
        }
    }

    if(!c->last_par_f)                          // If more parameters remain
    {
        c->device_par_err_idx = i;
        return(FGC_BAD_PARAMETER);                      // Report BAD PARAMETER
    }

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RefSetPars(struct cmd *c, struct prop **p, float *pars, uint16_t n_pars)
/*---------------------------------------------------------------------------------------------------------*\
  This function sets the properties identified in the array p with the values in the array of floats pars,
  at index fgfunc.chan_idx.
  The number of properties to set must match the size of the array pars, and passed as arguement n_pars.
\*---------------------------------------------------------------------------------------------------------*/
{
    float *     value;
    bool        set_f = false;
    uint16_t      errnum;

    while(n_pars--)
    {
        errnum = PropValueGet(c, *(p++), &set_f, fgfunc.chan_idx, (uint8_t**)&value);
        if(errnum)
        {
            return errnum;
        }

        *value = *(pars++);             // Copy floating point value from pars[] to the property

        errnum = PropBlkSet(c, FGC_N_FGEN_CHANS, true, FGC_FIELDBUS_FLAGS_LAST_PKT);
        if(errnum)
        {
            return errnum;
        }
    }

    return(FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ref_class.c
\*---------------------------------------------------------------------------------------------------------*/
