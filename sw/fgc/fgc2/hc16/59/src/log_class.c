/*---------------------------------------------------------------------------------------------------------*\
  File:		log_class.c

  Purpose:	FGC MCU Class 59 Software - Logging related functions

  Note:		LogInitEvtProp() makes links between bits and enums for a property with a symlist
		in the structure log_evt_prop.  This means that the symbol constant values for any
		property logged must NEVER be outside the range 0-15.
\*---------------------------------------------------------------------------------------------------------*/

#include <log_class.h>		                // Include all header files
#include <cmd.h>
#include <class.h>				// Include class dependent definitions
#include <hash.h>			        // Include hash header file
#include <defprops.h>		                // Include property related definitions
#include <ref_class.h>                          // For fgconf
#include <mcu_dependent.h>                      // For ENTER_SR(), EXIT_SR()
#include <mem.h>

/*---------------------------------------------------------------------------------------------------------*/
void LogGetPmDict(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function gets the post mortem dictionary.  This contains 32 strings of 30 characters.  The first
  16 are formed by the Channel Name and Units and the second 16 are formed by the Channel name with the
  suffix "_RAW".
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t	i;
    uint16_t	c;
    uint16_t	n;
    int8_t *	cp;
    char	name[FGC_MAX_DEV_LEN+1];	// Sub-device names

    for(i=0;i < 2;i++)				// For 2 sets of names (with UNITS and with _RAW)
    {
	for(c=0;c < FGC_N_FGEN_CHANS;c++)		// For each channel
	{
	    if(fgconf.chan.name[c])
	    {
	        MemCpyStrFar(name,fgconf.chan.name[c]);		// Get name from far Flash to near stack

		for(cp=name;*cp;cp++)					// Replace dots with underscores
		{
		    if(*cp == '.')
		    {
			*cp = '_';
		    }
		}

		n = fputs(name,fcm.f);
	    }
	    else
	    {
		n = fprintf(fcm.f,"%s_%u","UNUSED_CHAN",c);		// Create standard name
	    }

	    if(i)						// If second set
	    {
		n += fputs("_RAW",fcm.f);				// Add _RAW suffix
	    }
	    else if(fgconf.chan.units[c] != FGC_FG_UNITS_NONE)		// else if UNITS defined
	    {
		n += fprintf(fcm.f,"_%s",CmdPrintSymLst(sym_lst_59_fg_units,fgconf.chan.units[c]));
	    }

	    while(n++ < 30)					// Pad to 30 characters
	    {
		fputc(0,fcm.f);
	    }
	}
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void LogCval(void)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function stores one sample in the CVAL log.  The sample is transfered from the DSP to the
  MCU by IsrMst() at the start of each millisecond and corresponds to the sample for that millisecond.
\*---------------------------------------------------------------------------------------------------------*/
{
    ENTER_SR();				// Select RAM in pages 23
    LogNextAnaSample(&log_cval);	// Return next record address in A:B:Y

    asm
    {
	PSHM	K			// Save page registers
	TBYK

	LDED	fgfunc.cval:0		// Copy 64 words from fgfunc.cval to (IY)
	STE	0,Y
	STD	2,Y
	LDED	fgfunc.cval:4
	STE	4,Y
	STD	6,Y
	LDED	fgfunc.cval:8
	STE	8,Y
	STD	10,Y
	LDED	fgfunc.cval:12
	STE	12,Y
	STD	14,Y
	LDED	fgfunc.cval:16
	STE	16,Y
	STD	18,Y
	LDED	fgfunc.cval:20
	STE	20,Y
	STD	22,Y
	LDED	fgfunc.cval:24
	STE	24,Y
	STD	26,Y
	LDED	fgfunc.cval:28
	STE	28,Y
	STD	30,Y
	LDED	fgfunc.cval:32
	STE	32,Y
	STD	34,Y
	LDED	fgfunc.cval:36
	STE	36,Y
	STD	38,Y
	LDED	fgfunc.cval:40
	STE	40,Y
	STD	42,Y
	LDED	fgfunc.cval:44
	STE	44,Y
	STD	46,Y
	LDED	fgfunc.cval:48
	STE	48,Y
	STD	50,Y
	LDED	fgfunc.cval:52
	STE	52,Y
	STD	54,Y
	LDED	fgfunc.cval:56
	STE	56,Y
	STD	58,Y
	LDED	fgfunc.cval:60
	STE	60,Y
	STD	62,Y

	PULM	K			// Restore page registers

	TSTW	log_cval.run_f		// If logging is still active
	BNE	end			// Skip decrement

	DECW	log_cval.samples_to_acq	// Decrement samples remaining counter
    end:
    }

    log_cval.last_sample_time.unix_time = SM_UXTIME_P;
    log_cval.last_sample_time.us_time   = SM_MSTIME_P * 1000;

    EXIT_SR();				// Deselect RAM in pages 23
}
/*---------------------------------------------------------------------------------------------------------*/
void LogIval(void)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function stores one sample in the IVAL log.  The sample is transfered from the DSP to the
  MCU by IsrMst() at the start of each millisecond and corresponds to the sample for that millisecond.
\*---------------------------------------------------------------------------------------------------------*/
{
    ENTER_SR();				// Select RAM in pages 23
    LogNextAnaSample(&log_ival);	// Return next record address in A:B:Y

    asm
    {
	PSHM	K			// Save page registers
	TBYK

	LDED	fgfunc.ival:0		// Copy 32 words from fgfunc.ival to (IY)
	STE	0,Y
	STD	2,Y
	LDED	fgfunc.ival:4
	STE	4,Y
	STD	6,Y
	LDED	fgfunc.ival:8
	STE	8,Y
	STD	10,Y
	LDED	fgfunc.ival:12
	STE	12,Y
	STD	14,Y
	LDED	fgfunc.ival:16
	STE	16,Y
	STD	18,Y
	LDED	fgfunc.ival:20
	STE	20,Y
	STD	22,Y
	LDED	fgfunc.ival:24
	STE	24,Y
	STD	26,Y
	LDED	fgfunc.ival:28
	STE	28,Y
	STD	30,Y

	PULM	K			// Restore page registers

	TSTW	log_ival.run_f		// If logging is still active
	BNE	end			// Skip decrement

	DECW	log_ival.samples_to_acq // Decrement samples remaining counter
    end:
    }

    log_ival.last_sample_time.unix_time = SM_UXTIME_P;
    log_ival.last_sample_time.us_time   = SM_MSTIME_P * 1000;

    EXIT_SR();				// Deselect RAM in pages 23
}
/*---------------------------------------------------------------------------------------------------------*/
void LogWval(void)
/*---------------------------------------------------------------------------------------------------------*\
  This assembler function stores one sample in the WVAL log.  The sample is received from the WorldFIP
  on millisecond 19 of 20 and is logged on millisecond 5 of 20.
\*---------------------------------------------------------------------------------------------------------*/
{
    LogNextAnaSample(&log_wval);	// Return next record address in A:B:Y

    asm
    {
	PSHM	K			// Save page registers
	TBYK				// YK = B

	LDED	dpcls.mcu.wval:0		// Copy 64 words from dpcls.mcu.wval to (IY)
	STE	0,Y
	STD	2,Y
	LDED	dpcls.mcu.wval:4
	STE	4,Y
	STD	6,Y
	LDED	dpcls.mcu.wval:8
	STE	8,Y
	STD	10,Y
	LDED	dpcls.mcu.wval:12
	STE	12,Y
	STD	14,Y
	LDED	dpcls.mcu.wval:16
	STE	16,Y
	STD	18,Y
	LDED	dpcls.mcu.wval:20
	STE	20,Y
	STD	22,Y
	LDED	dpcls.mcu.wval:24
	STE	24,Y
	STD	26,Y
	LDED	dpcls.mcu.wval:28
	STE	28,Y
	STD	30,Y
	LDED	dpcls.mcu.wval:32
	STE	32,Y
	STD	34,Y
	LDED	dpcls.mcu.wval:36
	STE	36,Y
	STD	38,Y
	LDED	dpcls.mcu.wval:40
	STE	40,Y
	STD	42,Y
	LDED	dpcls.mcu.wval:44
	STE	44,Y
	STD	46,Y
	LDED	dpcls.mcu.wval:48
	STE	48,Y
	STD	50,Y
	LDED	dpcls.mcu.wval:52
	STE	52,Y
	STD	54,Y
	LDED	dpcls.mcu.wval:56
	STE	56,Y
	STD	58,Y
	LDED	dpcls.mcu.wval:60
	STE	60,Y
	STD	62,Y

	PULM	K			// Restore page registers

	TSTW	log_wval.run_f		// If logging is still active
	BNE	end			// Skip decrement

	DECW	log_wval.samples_to_acq // Decrement samples remaining counter
    end:
    }

    log_wval.last_sample_time.unix_time = SM_UXTIME_P;
    log_wval.last_sample_time.us_time   = SM_MSTIME_P * 1000;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: log_class.c
\*---------------------------------------------------------------------------------------------------------*/
