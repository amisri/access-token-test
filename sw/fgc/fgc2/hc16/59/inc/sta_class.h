/*---------------------------------------------------------------------------------------------------------*\
  File:         sta_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef STA_CLASS_H      // header encapsulation
#define STA_CLASS_H

#ifdef STA_CLASS_GLOBALS
    #define STA_CLASS_VARS_EXT
#else
    #define STA_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

void            StaTsk                  (void *);
void            StaInit                 (void);
void            StaOpState              (void);
void            StaCheck                (void);
void            StaLeds                 (void);
void            StaEventGroups          (void);

//-----------------------------------------------------------------------------------------------------------

#endif  // STA_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sta_class.h
\*---------------------------------------------------------------------------------------------------------*/
