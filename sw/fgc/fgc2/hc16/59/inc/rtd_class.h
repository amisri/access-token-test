/*---------------------------------------------------------------------------------------------------------*\
  File:         rtd_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef RTD_CLASS_H      // header encapsulation
#define RTD_CLASS_H

#ifdef RTD_CLASS_GLOBALS
    #define RTD_CLASS_VARS_EXT
#else
    #define RTD_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <stdbool.h>
#include <dpcls.h>      // for dpcls global variable
#include <stdio.h>      // for FILE
#include <definfo.h>    // for FGC_CLASS_ID
#include <rtd.h>        // for RtdFlags(), RtdTout(), RtdTin(), RtdCpuUsage(), RtdCpu32Usage(), RtdOptionLine(), RtdOptionLine()
#include <fbs_class.h>  // for FAULTS, WARNINGS, ST_MEAS_A, ST_MEAS_B, ST_DCCT_A, ST_DCCT_B
#include <sta.h>        // for sta global variable
#include <fbs.h>        // for fbs global variable

//-----------------------------------------------------------------------------------------------------------

/*----- RTD Field position constants -----*/

#define RTD_TIN_POS             "23;3"
#define RTD_TOUT_POS            "23;9"
#define RTD_CPUUSE_POS          "24;5"
#define RTD_CPU32USE_POS        "24;8"
#define RTD_CUR_RIGHT           "\33[C"
#define RTD_STATES_POS          "21;8"
#define RTD_CHAN_ORIGIN_ROW     21u
#define RTD_CHAN_ORIGIN_COL     21u
#define RTD_CHAN_WIDTH_COL      16u

//-----------------------------------------------------------------------------------------------------------

/*----- RTD Class Variables -----*/

struct rtd_cls_vars
{
    uint16_t              base;                           // Base (10 or 16) for channel values

    struct rtd_cls_vars_last                            // Last displayed values:
    {
        uint16_t          fgstate_func[FGC_N_FGEN_CHANS]; // Function generator state
        uint16_t          rt_ctrl     [FGC_N_FGEN_CHANS]; // RT control enabled
        uint16_t          lim_any_10s [FGC_N_FGEN_CHANS]; // Any limit active (10s stretch)
        uint16_t          not_ack_10s [FGC_N_FGEN_CHANS]; // Not ack (10s stretch)
        int32_t          fgfunc_ival [FGC_N_FGEN_CHANS]; // Channel ival
        uint16_t          base        [FGC_N_FGEN_CHANS]; // Base (10 or 16) for channel values
    } last;

    struct rtd_cls_optline
    {
        int8_t           unused[2];                      // Padding
    } optline;
};

RTD_CLASS_VARS_EXT struct rtd_cls_vars    rtd_cls;                // Real-time display structure for class 59

//-----------------------------------------------------------------------------------------------------------

void         RtdInit                 (FILE *);
bool         RtdStates               (void);
bool         RtdFGenChan             (void);
bool         RtdFGenChanState        (uint16_t chan_idx, uint16_t chan_mask);
bool         RtdFGenChanValue        (uint16_t chan_idx, uint16_t chan_mask);

//-----------------------------------------------------------------------------------------------------------

/*----- RTD function list -----*/

RTD_CLASS_VARS_EXT bool (*rtd_func[])(void)    // RTD functions
#ifdef RTD_CLASS_GLOBALS
= {
    RtdStates,                                  // Display states
    RtdFlags,                                   // Flag characters
    RtdTout,                                    // Display outlet temperature
    RtdTin,                                     // Display inlet temperature
    RtdFGenChan,                                // Function generator channels
    RtdOptionLine,                              // Option line
    RtdCpuUsage,                                // CPU usage
    RtdCpu32Usage,                              // CPU32 usage
    RtdFGenChan,                                // Function generator channels
    0
}
#endif
;

RTD_CLASS_VARS_EXT struct rtd_flags        rtd_flags[]                    // rtd.last.flag_off[] must have at least as
#ifdef RTD_CLASS_GLOBALS
= {                                             // many elements (40 at the moment)
        { &FAULTS,              FGC_FLT_FGC_HW,                 { '#', 'H'}, "22;5"  },         // 1

        { &WARNINGS,            FGC_WRN_FGC_HW,                 { '#', 'H'}, "22;7"  },         // 2
        { &WARNINGS,            FGC_WRN_RF_FGEN_COMMS,          { '#', 'R'}, "22;8"  },         // 3
        { &WARNINGS,            FGC_WRN_FGC_PSU,                { '#', 'F'}, "22;9"  },         // 4
        { &WARNINGS,            FGC_WRN_CONFIG,                 { '#', 'C'}, "22;10" },         // 5

        { &ST_UNLATCHED,        FGC_UNL_POST_MORTEM,            { '#', 'M'}, "22;12" },         // 6

        { &sta.config_state,    FGC_CFG_STATE_SYNCHRONISED,     { '~', 'U'}, "22;13" },         // 7
        { &sta.config_mode,     FGC_CFG_MODE_SYNC_FGC,          { '#', 'F'}, "22;14" },         // 8
        { &sta.config_mode,     FGC_CFG_MODE_SYNC_DB,           { '#', 'D'}, "22;15" },         // 9
        { &sta.config_mode,     FGC_CFG_MODE_SYNC_FAILED,       { '#', 'F'}, "22;16" },         // 10

        { NULL },
}
#endif
;


//-----------------------------------------------------------------------------------------------------------

#endif  // RTD_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: rtd_class.h
\*---------------------------------------------------------------------------------------------------------*/
