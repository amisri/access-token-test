/*---------------------------------------------------------------------------------------------------------*\
  File:         log_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef LOG_CLASS_H      // header encapsulation
#define LOG_CLASS_H

#ifdef LOG_CLASS_GLOBALS
    #define LOG_CLASS_VARS_EXT
#else
    #define LOG_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------
#define DEFPROPS_INC_ALL    // defprops.h

#include <cc_types.h>   // basic typedefs
#include <definfo.h>    // for FGC_CLASS_ID
#include <log_event.h>  // for struct log_evt_rec
#include <defconst.h>   // for FGC_LOG_CYCLES_LEN
#include <dpcom.h>      // for struct abs_time_us
#include <log.h>        // for struct log_ana_vars
#include <memmap_mcu.h> // for LOG_IAB_32, LOG_IAB_W
#include <fbs.h>        // for fbs global variable
#include <fgc_log.h>    // for FGC_LOG_SIG_INFO_STEPS
#include <defprops.h>   // for PROP_STATE_P

//-----------------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------------

void    LogGetPmDict    (void);
void    LogCval         (void);
void    LogIval         (void);
void    LogWval         (void);

/*---------------------------------------------------------------------------------------------------------*/

LOG_CLASS_VARS_EXT struct log_evt_prop          log_evt_prop[]
#ifdef LOG_CLASS_GLOBALS
= {     // Property pointer                Old value       +-Property Name----------+
        {   &PROP_STATE_OP,                 0xFFFF,         "STATE.OP"              },
        {   &PROP_FGC_PLL_STATE,            0xFFFF,         "FGC.PLL.STATE"         },
        {   &PROP_CONFIG_STATE,             0xFFFF,         "CONFIG.STATE"          },
        {   &PROP_CONFIG_MODE,              0xFFFF,         "CONFIG.MODE"           },
        {   &PROP_LOG_PM_TRIG,              0x0000,         "LOG.PM.TRIG"           },
        {   &PROP_STATUS_FAULTS,            0x0000,         "STATUS.FAULTS"         },
        {   &PROP_STATUS_WARNINGS,          0x0000,         "STATUS.WARNINGS"       },
        {   &PROP_STATUS_ST_LATCHED,        0x0000,         "STATUS.ST_LATCHED"     },
        {   &PROP_STATUS_ST_UNLATCHED,      0x0000,         "STATUS.ST_UNLATCHED"   },
        {   &PROP_FGSTATUS_TRIGGER,         0x0000,         "TRIGGER",              },
        {   &PROP_FGSTATUS_FUNC_IDLE,       0x0000,         "FUNC.IDLE",            },
        {   &PROP_FGSTATUS_FUNC_ARMED,      0x0000,         "FUNC.ARMED",           },
        {   &PROP_FGSTATUS_FUNC_RUNNING,    0x0000,         "FUNC.RUNNING",         },
        {   &PROP_FGSTATUS_FUNC_ABORTING,   0x0000,         "FUNC.ABORTING",        },
        {   &PROP_FGSTATUS_FUNC_NOT_ACK,    0x0000,         "FUNC.NOT_ACK",         },
        {   &PROP_FGSTATUS_RT_CONTROL,      0x0000,         "RT_CONTROL",           },
        {   &PROP_FGSTATUS_NOT_ACK,         0x0000,         "NOT_ACK",              },
        {   &PROP_FGSTATUS_START_EVENT,     0x0000,         "START_EVENT",          },
        {   &PROP_FGSTATUS_ABORT_EVENT,     0x0000,         "ABORT_EVENT",          },
        {   &PROP_FGSTATUS_LIMIT_CHAN_MAX,  0x0000,         "LIMIT.CHAN.MAX",       },
        {   &PROP_FGSTATUS_LIMIT_CHAN_MIN,  0x0000,         "LIMIT.CHAN.MIN",       },
        {   &PROP_FGSTATUS_LIMIT_RATE_POS,  0x0000,         "LIMIT.RATE.POS",       },
        {   &PROP_FGSTATUS_LIMIT_RATE_NEG,  0x0000,         "LIMIT.RATE.NEG",       },
        {   &PROP_FGSTATUS_LIMIT_RT_POS,    0x0000,         "LIMIT.RT.POS",         },
        {   &PROP_FGSTATUS_LIMIT_RT_NEG,    0x0000,         "LIMIT.RT.NEG",         },
        {   NULL    },
}
#endif
;



/*--- Analogue log structure definitions ---*/

LOG_CLASS_VARS_EXT struct log_ana_vars log_cval // Cval log variables structure
#ifdef LOG_CLASS_GLOBALS
= {
    LOG_CVAL_32,                                // Buffer base address
    LOG_CVAL_W,                                 // Buffer size in words
    STP_CVAL,                                   // Sym_idx for log type
    FGC_LOG_CVAL_LEN,                           // Number of samples in the log
    65,                                         // Number of post trig samples
    16,                                         // Number of signals per sample
    64,                                         // Number of bytes per sample
    0,                                          // Wait time (ms)
    1,                                          // Period in milliseconds (50 Hz)
}
#endif
;

LOG_CLASS_VARS_EXT struct log_ana_vars log_ival // Ival log variables structure
#ifdef LOG_CLASS_GLOBALS
= {
    LOG_IVAL_32,                                // Buffer base address
    LOG_IVAL_W,                                 // Buffer size in words
    STP_IVAL,                                   // Sym_idx for log type
    FGC_LOG_IVAL_LEN,                           // Number of samples in the log
    65,                                         // Number of post trig samples
    16,                                         // Number of signals per sample
    32,                                         // Number of bytes per sample
    0,                                          // Wait time (ms)
    1,                                          // Period in milliseconds (50 Hz)
}
#endif
;

LOG_CLASS_VARS_EXT struct log_ana_vars log_wval // Wval log variables structure
#ifdef LOG_CLASS_GLOBALS
= {
    LOG_WVAL_32,                                // Buffer base address
    LOG_WVAL_W,                                 // Buffer size in words
    STP_WVAL,                                   // Sym_idx for log type
    FGC_LOG_WVAL_LEN,                           // Number of samples in the log
    2,                                          // Number of post trig samples
    16,                                         // Number of signals per sample
    64,                                         // Number of bytes per sample
    0,                                          // Wait time (ms)
    20,                                         // Period in milliseconds (50 Hz)
}
#endif
;


//-----------------------------------------------------------------------------------------------------------

#endif  // LOG_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: log_class.h
\*---------------------------------------------------------------------------------------------------------*/
