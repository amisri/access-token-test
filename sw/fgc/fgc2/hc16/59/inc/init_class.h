/*---------------------------------------------------------------------------------------------------------*\
  File:         init_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef INIT_CLASS_H      // header encapsulation
#define INIT_CLASS_H

#ifdef INIT_CLASS_GLOBALS
    #define INIT_CLASS_VARS_EXT
#else
    #define INIT_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------
#define DEFPROPS_INC_ALL    // defprops.h

#include <stdbool.h>
#include <cc_types.h>   // basic typedefs
#include <cmd.h>        // for struct cmd
#include <definfo.h>    // for FGC_CLASS_ID
#include <defprops.h>   // for PROP_MODE_OP
#include <dpcls.h>      // for CAL_DEF_INTERNAL_ADC_GAIN, CAL_DEF_EXTERNAL_ADC_GAIN
#include <init.h>

//-----------------------------------------------------------------------------------------------------------

/*----- RFC interface constants -----*/

#define RFC_RESET_TIME          1000            // Milliseconds to block access to interface after a reset

//-----------------------------------------------------------------------------------------------------------

/*----- RFC variables -----*/

struct rfc_vars
{
    bool        reset_f;                                // Reset request flag (to IsrMst)
    uint16_t      warning;                                // FGC_RF_COMMS warning set if RFC watchdog fails
    uint32_t      triggers;                               // FGSTATS.RFC_TRIGGERS
    uint32_t      faults;                                 // FGSTATS.RFC_FAULTS (#tx watchdog faults)
    uint16_t      ack;                                    // RFC acknowledge received mask
    uint16_t      nack;                                   // RFC not acknowledge received mask
    uint16_t      not_ack_10s;                            // RFC not (or no) ack stetched to 10s
    bool        trigger_f;                              // RFC trigger flag from last millisecond
    uint16_t      counter;                                // RFC tx watchdog counter
    uint16_t *    spinner;
    uint16_t      spinner_idx;
    uint16_t      spinner_addr;
};

INIT_CLASS_VARS_EXT struct rfc_vars        rfc;                    // RF Communications structure

//-----------------------------------------------------------------------------------------------------------

// INIT_CLASS_VARS_EXT static uint16_t   conf_s[]
INIT_CLASS_VARS_EXT uint16_t  conf_s[]
#ifdef INIT_CLASS_GLOBALS
= {
/* 0 */ FGC_CTRL_DISABLED,
/* 1 */ FGC_CTRL_ENABLED,
/* 2 */ FGC_OP_NORMAL
}
#endif
;

// INIT_CLASS_VARS_EXT static char    devtype[]
INIT_CLASS_VARS_EXT char    devtype[]
#ifdef INIT_CLASS_GLOBALS
= {
    "RFM"
}
#endif
;

INIT_CLASS_VARS_EXT struct init_prop        init_prop[]
#ifdef INIT_CLASS_GLOBALS
= {
    {   &PROP_MODE_OP,                          1,                      &conf_s[2]      },
    {   &PROP_FGC_NAME,                         5,                      &devtype[0]     },
    {   &PROP_DEVICE_TYPE,                      5,                      &devtype[0]     },
    {   &PROP_BARCODE_FGC_CASSETTE,             0,                      NULL            },

    {   NULL    }       // End of list
}
#endif
;

// The DF_TIMESTAMP_SELECT dynamic flag is set for all the properties in the following list and their children.
// The same list is also used in function CmdPrintTimestamp to read the timestamp selector.

INIT_CLASS_VARS_EXT struct init_dynflag_timestamp_select init_dynflag_timestamp_select[]
#ifdef INIT_CLASS_GLOBALS
= {
    {   NULL    }       // End of list
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // INIT_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: init_class.h
\*---------------------------------------------------------------------------------------------------------*/
