/*---------------------------------------------------------------------------------------------------------*\
  File:		dev.h

  Purpose:

  History:

    28 jun 2010	doc	Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DEV_H	// header encapsulation
#define DEV_H

#ifdef DEV_GLOBALS
    #define DEV_VARS_EXT
#else
    #define DEV_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>		// basic typedefs
#include <stdbool.h>
#include <fgc_consts_gen.h>	// Global FGC constants
#include <definfo.h>            // for FGC_CLASS_ID, FGC_PLATFORM_ID, FGC_PLATFORM_NAME, FGC_CLASS_NAME
#include <microlan_1wire.h>     // for MICROLAN_NETWORK_ALL_ELEMENTS_MAX
#include <os.h>                 // for OS_SEM type
#include <property.h>           // for prop_size_t
#include <defconst.h>           // for FGC_MAX_DIMS, FGC_N_COMP_GROUPS, FGC_SYSDB_SYS_LEN, FGC_SYSDB_MENU_NAME_LEN, FGC_SYSDB_SYS_LBL_LEN, SYSDB_MENU_PROP_LEN
#include <fgc/fgc_db.h>

//-----------------------------------------------------------------------------------------------------------

#define DEV_NAME_SIZE           32
#define DEV_TEST_DATA_SZ        32              // in 16-bit words
#define DEV_CTRL_ACTION_DELAY   40              // 40 x 5ms = 200ms

//-----------------------------------------------------------------------------------------------------------
struct sys_info
{
    //Sys Info
    fgc_db_t            sys_idx;                        // System index in SYSDB
    char                sys_type[FGC_SYSDB_SYS_LEN + 1];
    uint8_t               detected[FGC_N_COMP_GROUPS];    // Number of detected Dallas devices in each group
    fgc_db_t            comp_idx[MICROLAN_NETWORK_ALL_ELEMENTS_MAX];  // Component index for id.devs[] (0 = unknown)
    uint8_t               n_unknown_grp[FGC_COMPDB_N_COMPS];            // Number of unknown group members for each comp type
};

/*!
 * Variables used to trigger RESET, POWER_CYCLE, CRASH and BOOT actions
 */
struct dev_ctrl
{
    uint16_t              counter;                // Device control action delay down counter in units of 5ms. The delay
                                                // allows to finish the set command gracefully before switching off the FGC.
                                                // Without the delay, the gateway would return an error in response to the command.
    uint16_t              action;                 // Device control action.
    bool                power_cycle_pending;    // Used to prevent overwriting of RGLEDS register in mst task after power cycle request
};

struct dev_vars
{
    uint8_t               platform_id;                    // Platform ID     |
    uint8_t               class_id;                       // Class ID        | These four elements are
    int8_t               platform_nm[8];                 // Platform name   | initialised in main.c
    int8_t               class_nm[16];                   // Class name      |
    uint32_t              ppm;                            // DEVICE.PPM property (ENABLED/DISABLED)
    uint16_t              max_user;                       // Set according to DEVICE.PPM and class
    uint16_t              omode_mask;                     // omode_mask (from NameDB)
    uint16_t              run_number;                     // Run number
    uint32_t              cmdrcvd;                        // Commands received counter (DEVICE.CMDRCVD)
    uint16_t              namedb_crc;                     // Resident NameDB CRC
    uint16_t              leds;                           // Front panel LED values (see MstTsk())
    bool                dbs_ok_f;                       // SysDB, CompDB and DimDB match versions
    bool                pm_enabled_f;                   // Post morten enabled flag (from FIP time var)
    uint16_t              log_pm_state;                   // PM Log state
    uint16_t              log_pm_test;                    // Manual post mortem trigger property
    uint16_t              log_pm_trig;                    // Readback of post mortem trigger bits in ACK byte
    OS_SEM *            set_lock;                       // Set command lock semaphore
    prop_size_t         name_nels;                      // Device name length
    prop_size_t         type_nels;                      // Length of dev.type
    FP32                psu_fgc[3];                     // FGC PSU voltages: +5V, +15V, -15V
    FP32                psu_ana[3];                     // Ana interface PSU voltages: +5V, +15V, -15V
    char                test_data[2*DEV_TEST_DATA_SZ];  // Buffer for TEST.XXXX properties
    char                name[DEV_NAME_SIZE+1];          // System (power converter) name
    char                type[6];                        // Device type (system type property DEVICE.TYPE)
    struct sys_info     sys;
    struct dev_ctrl     ctrl;
};


//-----------------------------------------------------------------------------------------------------------

DEV_VARS_EXT struct dev_vars		dev
#ifdef DEV_GLOBALS
= {
    FGC_PLATFORM_ID,
    FGC_CLASS_ID,
    FGC_PLATFORM_NAME,
    FGC_CLASS_NAME,
}
#endif
;
//-----------------------------------------------------------------------------------------------------------

#endif	// DEV_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dev.h
\*---------------------------------------------------------------------------------------------------------*/
