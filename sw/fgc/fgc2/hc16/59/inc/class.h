/*---------------------------------------------------------------------------------------------------------*\
  File:		59\inc\class.h

  Contents:	This file contains all class 59 related defintions

  Notes:	The CLASS_ID constant is not defined when header files are parsed because defs.h must be
		the last header file.  So all class related definitions are collected in this file
		for class 59.
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CLASS_H      // header encapsulation
#define CLASS_H

#ifdef CLASS_GLOBALS
    #define CLASS_VARS_EXT
#else
    #define CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs

/*----- MST constants -----*/

#define MST_FIP_STAT_VAR_TIME	1		// Millisecond to write STAT var to FIP
#define DEV_STA_TSK_PHASE	2		// Millisecond to run StaTsk()
#define	LOG_WVAL_PHASE		5		// Millisecond boundary @ 50Hz

/*----- Warnings/unlatched/latched status masks ------*/

#define	FGC_FAULTS		(FGC_FLT_FGC_HW|FGC_FLT_FGC_STATE)
#define	FGC_WARNINGS		(FGC_WRN_FGC_HW|FGC_WRN_TEMPERATURE|FGC_WRN_CONFIG)
#define	ST_LATCHED_RESET	(FGC_LAT_VDC_FAIL|FGC_LAT_PSU_V_FAIL|FGC_LAT_DIM_SYNC_FLT|\
				 FGC_LAT_DIMS_EXP_FLT|FGC_LAT_DSP_FLT|FGC_LAT_RFC_FLT|\
				 FGC_LAT_MEM_FLT|FGC_LAT_DALLAS_FLT|FGC_LAT_FGC_PSU_FAIL)

/*------ For compatibility with other classes -------*/

#define SETIF_REFUNLOCK 0xFFF           /* A value associated no real setif function */

/*------ Include class related header files ------*/

#include <dpcls.h>				// Include dual port RAM structure definitions

/*------ Bit masks ------*/
/*
     reg=00000 cy=1 then shl (<<) number
     or
     reg=00001 then shl (<<) (number-1)

     1 - > 0x0001,
     2 - > 0x0002,
     3 - > 0x0004,
     4 - > 0x0008,
     5 - > 0x0010,
     6 - > 0x0020,
     7 - > 0x0040,
     8 - > 0x0080,
     9 - > 0x0100,
    10 - > 0x0200,
    11 - > 0x0400,
    12 - > 0x0800,
    13 - > 0x1000,
    14 - > 0x2000,
    15 - > 0x4000,
    16 - > 0x8000,

 */

// Do NOT define the masks array as a const, at least not on FGC2. If defined as const, the linker will not map it
// into the 1st memory page (where all the non-const data reside), but in the same memory pages as the code and the
// other constant data. To access this kind of data the processor needs to switch between memory pages using
// register K. Parts of the FGC2 code that are written in assembly does not manipulate the K register and as a
// consequence can only access data located in the first memory page. Function LogEvtProp is partly written in
// in assembly and needs access to the masks array, and it must be mapped in the 1st memory page.

CLASS_VARS_EXT uint16_t  masks[16]
#ifdef CLASS_GLOBALS
=
{
    0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080,
    0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000, 0x4000, 0x8000,
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // CLASS_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: 59\inc\class.h
\*---------------------------------------------------------------------------------------------------------*/
