/*---------------------------------------------------------------------------------------------------------*\
  File:		fbs_class.h

  Contents:

  Notes:

\*---------------------------------------------------------------------------------------------------------*/

#ifndef FBS_CLASS_H      // header encapsulation
#define FBS_CLASS_H

#ifdef FBS_CLASS_GLOBALS
    #define FBS_CLASS_VARS_EXT
#else
    #define FBS_CLASS_VARS_EXT extern
#endif
//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>           // basic typedefs
#include <fbs.h>

//-----------------------------------------------------------------------------------------------------------

/*----- Status variable constants -----*/

#define FAULTS                  fbs.u.fgc_stat.class_data.c59.st_faults
#define WARNINGS                fbs.u.fgc_stat.class_data.c59.st_warnings
#define ST_LATCHED              fbs.u.fgc_stat.class_data.c59.st_latched
#define ST_UNLATCHED            fbs.u.fgc_stat.class_data.c59.st_unlatched
#define STATE_PLL               fbs.u.fgc_stat.class_data.c59.state_pll
#define STATE_OP                fbs.u.fgc_stat.class_data.c59.state_op
#define TRIGGER                 fbs.u.fgc_stat.class_data.c59.st_trigger
#define FUNC_IDLE               fbs.u.fgc_stat.class_data.c59.st_idle
#define FUNC_ARMED              fbs.u.fgc_stat.class_data.c59.st_armed
#define FUNC_RUNNING            fbs.u.fgc_stat.class_data.c59.st_running
#define FUNC_ABORTING           fbs.u.fgc_stat.class_data.c59.st_aborting
#define FUNC_NOT_ACK            fbs.u.fgc_stat.class_data.c59.st_not_ack
#define RT_CONTROL              fbs.u.fgc_stat.class_data.c59.st_rt_ctrl
#define START_EVENT             fbs.u.fgc_stat.class_data.c59.st_start_evt
#define ABORT_EVENT             fbs.u.fgc_stat.class_data.c59.st_abort_evt
#define LIMIT_RT_POS            fbs.u.fgc_stat.class_data.c59.st_lim_rt_pos
#define LIMIT_RT_NEG            fbs.u.fgc_stat.class_data.c59.st_lim_rt_neg
#define LIMIT_CHAN_MAX          fbs.u.fgc_stat.class_data.c59.st_lim_chan_max
#define LIMIT_CHAN_MIN          fbs.u.fgc_stat.class_data.c59.st_lim_chan_min
#define LIMIT_RATE_POS          fbs.u.fgc_stat.class_data.c59.st_lim_rate_pos
#define LIMIT_RATE_NEG          fbs.u.fgc_stat.class_data.c59.st_lim_rate_neg


//-----------------------------------------------------------------------------------------------------------

#endif  // FBS_CLASS_H end of header encapsulation

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fbs_class.h
\*---------------------------------------------------------------------------------------------------------*/
