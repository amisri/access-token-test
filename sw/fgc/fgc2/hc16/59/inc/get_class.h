/*---------------------------------------------------------------------------------------------------------*\
  File:         get_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef GET_CLASS_H      // header encapsulation
#define GET_CLASS_H

#ifdef GET_CLASS_GLOBALS
    #define GET_CLASS_VARS_EXT
#else
    #define GET_CLASS_VARS_EXT extern
#endif

// Include files

#include <cc_types.h>           // basic typedefs
#include <property.h>           // for struct prop
#include <cmd.h>                // for struct cmd

#endif  // GET_CLASS_H end of header encapsulation

// EOF
