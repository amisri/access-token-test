/*---------------------------------------------------------------------------------------------------------*\
  File:         ref_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef REF_CLASS_H      // header encapsulation
#define REF_CLASS_H

#ifdef REF_CLASS_GLOBALS
    #define REF_CLASS_VARS_EXT
#else
    #define REF_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <cc_types.h>   // basic typedefs
#include <property.h>   // for struct prop
#include <cmd.h>        // for struct cmd
#include <defconst.h>   // for FGC_SIMSC_LEN, FGC_MAX_USER_PLUS_1
#include <dpcom.h>

//-----------------------------------------------------------------------------------------------------------

#define NON_PPM_USER            (0)

#define RefRelTime(f)           (uint32_t)(int32_t)(f * dpcom.dsp.tb_freq_hz + 0.4999)

//-----------------------------------------------------------------------------------------------------------

void            RefEvent                (void);
uint16_t          RefSet                  (struct cmd *c);
void            RefReset                (struct cmd *c);
uint16_t          RefPlp                  (struct cmd *c);
uint16_t          RefCTrim                (struct cmd *c);
uint16_t          RefSine                 (struct cmd *c);
uint16_t          RefTable                (struct cmd *c);
uint16_t          RefGetChanIdx           (struct cmd *c);
uint16_t          RefGetPars              (struct cmd *c, struct prop **p, float *pars, uint16_t n_pars);
uint16_t          RefSetPars              (struct cmd *c, struct prop **p, float *pars, uint16_t n_pars);

//-----------------------------------------------------------------------------------------------------------

/*----- FGEN channel variables -----*/

struct fgfunc_vars
{
    uint16_t              chan_idx;                               // Channel index for S FGFUNC func,chan_idx,...
    uint16_t              err_chan_idx;                           // Channel index for failed arming attempt
    uint16_t              event_group     [FGC_N_FGEN_CHANS];     // FGFUNC.EVENT_GROUP
    float                 cval            [FGC_N_FGEN_CHANS];     // [1ms]   Floating point channel values
    int16_t               ival            [FGC_N_FGEN_CHANS];     // [1ms]   Integer channel values
    uint16_t              type            [FGC_N_FGEN_CHANS];     // Function types

    union                                                       // Union for reference parameters
    {
        float           f[3];
        uint32_t          i[3];
    }                   pars;
};

REF_CLASS_VARS_EXT struct fgfunc_vars fgfunc;                             // FG function channel variables

/*----- FGCONF Property Variables -----*/

struct fgconf
{
    struct fgconf_default
    {
        float           acceleration    [FGC_N_FGEN_CHANS];     // FGCONF.DEFAULT.ACCELERATION[]
        float           linear_rate     [FGC_N_FGEN_CHANS];     // FGCONF.DEFAULT.LINEAR_RATE[]
    } Default;                                                  // Capitalised to avoid being a C keyword

    struct fgconf_chan
    {
        uint16_t          units           [FGC_N_FGEN_CHANS];     // FGCONF.CHAN.UNITS[]
        char *   FAR    name            [FGC_N_FGEN_CHANS];     // FGCONF.CHAN.NAME[]
    } chan;
};

REF_CLASS_VARS_EXT       struct fgconf fgconf;                           // FGCONF property variables

/*----- Property struct for table lengths that are in the DSP -----*/

struct property
{
    struct
    {
        struct
        {
            uint32_t      t_n_els         [FGC_N_FGEN_CHANS];     // FGFUNC.TABLE.CHANn.T number of elements
            uint32_t      r_n_els         [FGC_N_FGEN_CHANS];     // FGFUNC.TABLE.CHANn.R number of elements
        } table;
    } fgfunc;
};

//-----------------------------------------------------------------------------------------------------------

#endif  // REF_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ref_class.h
\*---------------------------------------------------------------------------------------------------------*/
