#!/usr/bin/python
#
# Copyright (C) 2012 CERN.
#
# DESCRIPTION:
#
# CONTACT: main author, Pierre DEJOUE.
#

import sys, os

# Somewhat inelegant way to import the struct_map.py module
scripts_rel_path = '../../../scripts'
sys.path.append(os.path.join(*scripts_rel_path.split('/')))
from struct_map import *
sys.path.pop()

# Keep intermediate files (with the mapping of cmd_pkt, pars_buf, prop_buf and pfloat_buf)?
keep_intermediate_files = False

# Class used to parse cmd_pkt
class struct_map_cmd_pkt(struct_map):

    def __init__(self):
        self.input_source_file = "./lst/main.i"
        self.input_info_file   = "dpcom_map.info.csv"

        self.output_file_flag  = keep_intermediate_files
        self.output_file       = "cmd_pkt.map"

        self.struct_name       = "cmd_pkt"

        self.start_address_mcu = 0x00
        self.start_address_dsp = 0x00

        self.dsp_c32_flag      = True                                           # The C32 DSP addresses 32 bits of data.
                                                                                # The addr_offset therefore needs to be divided by four.

# Class used to parse pars_buf
class struct_map_pars_buf(struct_map):

    def __init__(self):
        self.input_source_file = "./lst/main.i"
        self.input_info_file   = "dpcom_map.info.csv"

        self.output_file_flag  = keep_intermediate_files
        self.output_file       = "pars_buf.map"

        self.struct_name       = "pars_buf"

        self.start_address_mcu = 0x00
        self.start_address_dsp = 0x00

        self.dsp_c32_flag      = True                                           # The C32 DSP addresses 32 bits of data.
                                                                                # The addr_offset therefore needs to be divided by four.


# Class used to parse prop_buf
class struct_map_prop_buf(struct_map):

    def __init__(self):
        self.input_source_file = "./lst/main.i"
        self.input_info_file   = "dpcom_map.info.csv"

        self.output_file_flag  = keep_intermediate_files
        self.output_file       = "prop_buf.map"

        self.struct_name       = "prop_buf"

        self.start_address_mcu = 0x00
        self.start_address_dsp = 0x00

        self.dsp_c32_flag      = True                                           # The C32 DSP addresses 32 bits of data.
                                                                                # The addr_offset therefore needs to be divided by four.

# Class used to parse pfloat_buf
class struct_map_pfloat_buf(struct_map):

    def __init__(self):
        self.input_source_file = "./lst/main.i"
        self.input_info_file   = "dpcom_map.info.csv"

        self.output_file_flag  = keep_intermediate_files
        self.output_file       = "pfloat_buf.map"

        self.struct_name       = "pfloat_buf"

        self.start_address_mcu = 0x00
        self.start_address_dsp = 0x00

        self.dsp_c32_flag      = True                                           # The C32 DSP addresses 32 bits of data.
                                                                                # The addr_offset therefore needs to be divided by four.

# Class used to parse dpcom
class struct_map_dpcom(struct_map):

    def __init__(self):
        self.input_source_file = "./lst/main.i"
        self.input_info_file   = "dpcom_map.info.csv"

        self.output_file_flag  = True
        self.output_file       = "dpcom.map"

        self.struct_name       = "dpcom"

        self.start_address_mcu = 0x0000D800
        self.start_address_dsp = 0x00903600

        self.dsp_c32_flag      = True                                           # The C32 DSP addresses 32 bits of data.
                                                                                # The addr_offset therefore needs to be divided by four.

if __name__ == "__main__":

    mapping0 = struct_map_cmd_pkt()                                             # Parse "struct cmd_pkt"       (specific to FGC2)
    mapping0.parse()

    mapping1 = struct_map_pars_buf()                                            # Parse "struct pars_buf"      (specific to FGC2)
    mapping1.parse()

    mapping2 = struct_map_prop_buf()                                            # Parse "struct prop_buf"
    mapping2.parse()

    mapping3 = struct_map_pfloat_buf()                                          # Parse "struct pfloat_buf"    (specific to FGC2)
    mapping3.parse()

    mapping4 = struct_map_dpcom()                                               # Parse "struct dpcom"
    mapping4.parse()
