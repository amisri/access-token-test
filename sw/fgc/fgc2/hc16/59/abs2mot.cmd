/*----------------------------------------------------------------------------*\
 File:		mot.cmd

 Purpose:	fgc2_MainProg burner drive file
		This file drives the generation of a motorola S-Record file
		for the FGC2 HC16 main program.
\*----------------------------------------------------------------------------*/

OPENFILE ".\prog.mot"				/* Open file for S-Records */

format		= motorola
SRECORD		= S3
busWidth	= 1
undefByte	= 0xFF
origin		= 0x40000
len			= 0x20000
destination	= 0

SENDBYTE 1 ".\prog.abs"				/* Write prog.abs to S-Records */

CLOSE

/* End of file: mot.cmd */

