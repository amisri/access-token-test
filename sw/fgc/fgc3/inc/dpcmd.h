//! @file  dpcmd.h
//! @brief Command passing between the MCU to the DSP with access protection
//!
//! Important notes on the implamentation and use of this module:
//!
//! 1 - The commands are encapsulated in a 32 bit mask
//! 2 - The most significant bit is used for the token, given 31 commands per mask
//! 3 - The MCU sets the command and the DSP reads them
//! 4 - A single semaphore is used for all command masks to protect access from
//!     different MCU tasks
//! 5 - The write action blocks until the token is valid. This assumes the DSP
//!     background task runs continuously and frees the token almost immediately

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#ifdef __RX610__

#include <os.h>

#include <bitmap.h>
#include <sleep.h>

#define MEMORY_BARRIER   __sync_synchronize()

#else // __TMS320C6727__

#include "fgc3/inc/bitmap.h"

#define MEMORY_BARRIER   _restore_interrupts(_disable_interrupts())

#endif



// ---------- Constants

#define DPCMD_TOKEN_BIT     0x80000000



// ---------- External structures, unions and enumerations


//! Mask shared between MCU and DSP with protection mechanism

struct Dpcmd
{
    uint32_t mask;
};


//! Command mask token values

enum Dpcom_token
{
    DPCMD_TOKEN_MCU  =  0x00000000,
    DPCMD_TOKEN_DSP  =  0x80000000
};



// ---------- External variable declarations

#ifdef __RX610__

extern OS_SEM * dpcmd_sem;

#endif



// ---------- External function definitions

#ifdef __RX610__

static inline void dpcmdSet(volatile struct Dpcmd * dpcmd, uint32_t const command)
{
    OSSemPend(dpcmd_sem);

    uint32_t timeout_ms = 20;

    while ((dpcmd->mask & DPCMD_TOKEN_BIT) != DPCMD_TOKEN_MCU && timeout_ms-- > 0)
    {
        sleepMs(1);
    }

    // Overwrite previous commands even if it timesout

    dpcmd->mask = command | DPCMD_TOKEN_DSP;

    OSSemPost(dpcmd_sem);
}



static inline uint32_t dpcmdGet(volatile struct Dpcmd * dpcmd)
{
    return dpcmd->mask;
}



static inline bool dpcmdTest(volatile struct Dpcmd * dpcmd, uint32_t const mask)
{
    return testBitmap(dpcmdGet(dpcmd), mask);
}


#else // __TMS320C6727__


static inline bool dpcmdGet(volatile struct Dpcmd * dpcmd, uint32_t * command)
{
    if ((dpcmd->mask & DPCMD_TOKEN_BIT) != DPCMD_TOKEN_DSP)
    {
        return false;
    }

    *command = dpcmd->mask & ~DPCMD_TOKEN_BIT;

    MEMORY_BARRIER;

    dpcmd->mask = DPCMD_TOKEN_MCU;

    return true;
}

#endif


// EOF
