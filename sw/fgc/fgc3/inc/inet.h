//! @file  inet.h
//! @brief Implments honl() and hons() needed by cclibs
//!
//! Data transmitted from the DSP to the MCU becomes big endian in the DPRAM.
//! The MCU (RX610) is big endian so these functions do nothing.

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External function declarations

static inline uint32_t htonl(uint32_t value);

static inline uint16_t htons(uint16_t value);

static inline uint32_t ntohl(uint32_t value);

static inline uint16_t ntohs(uint16_t value);



// ---------- External function definitions

static inline uint32_t htonl(uint32_t value)
{
    return value;
}


static inline uint16_t htons(uint16_t value)
{
    return value;
}

static inline uint32_t ntohl(uint32_t value)
{
    return value;
}


static inline uint16_t ntohs(uint16_t value)
{
    return value;
}


// EOF
