//!	@file 	memTest.h
//!	@brief	Memory test functions

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External function declarations

//! Test the data bus wiring in a memory region by
//! performing a walking 1's test at a fixed address
//!	within that region.  The address (and hence the
//!	memory region) is selected by the caller.
//!
//!	@param[in]	base_address	Memory address on which to perform the test
//!	@param[in]	data_backup		Address for backing up original memory values
//!
//!	@return		0 if the test succeeds.
//!             A non-zero result is the first pattern that failed.

uint32_t memTestDataBus(volatile uint32_t * base_address, uint32_t * data_backup);


//!	Test the address bus wiring in a memory region by
//! performing a walking 1's test on the relevant bits
//! of the address and checking for aliasing. This test
//! will find single-bit address failures such as stuck
//! -high, stuck-low, and shorted pins.  The base address
//! and size of the region are selected by the caller.
//!
//!	Notes: For best results, the selected base address should
//!         have enough LSB 0's to guarantee single address bit
//!         changes.  For example, to test a 64-Kbyte region,
//!         select a base address on a 64-Kbyte boundary.  Also,
//!         select the region size as a power-of-two--if at all
//!         possible.
//!
//!	@param[in]	base_address	Memory address on which to perform the test
//!	@param[in]	n_bytes			Size of the memory block to be tested, in bytes
//!	@param[in]	data_backup		Address for backing up original memory values
//!
//!	@return		NULL if the test succeeds.
//!              A non-zero result is the first address at which an
//!              aliasing problem was uncovered.  By examining the
//!              contents of memory, it may be possible to gather
//!              additional information about the problem.

uint32_t * memTestAddressBus(volatile uint32_t * base_address, uint32_t n_bytes, uint32_t * data_backup);


//! Test the integrity of a physical memory device by
//! performing an increment/decrement test over the
//! entire region.  In the process every storage bit
//! in the device is tested as a zero and a one.  The
//! base address and the size of the region are
//! selected by the caller.
//!
//!	@param[in]	base_address	Memory address on which to perform the test
//!	@param[in]	n_bytes			Size of the memory block to be tested, in bytes
//!	@param[in]	data_backup		Address for backing up original memory values
//!
//!	@return		NULL if the test succeeds.
//!              A non-zero result is the first address at which an
//!              incorrect value was read back.  By examining the
//!              contents of memory, it may be possible to gather
//!              additional information about the problem.

uint32_t * memTestDevice(volatile uint32_t * base_address, uint32_t n_bytes, uint32_t * data_backup);


// EOF
