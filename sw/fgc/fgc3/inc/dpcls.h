//! @file  dpcls.h
//! @brief Proxy to include the class specific dpcls_##.h header file

#pragma once


// ---------- Includes

#include <definfo.h>


#if (FGC_CLASS_ID == 62)

#ifdef DPCLS_GLOBALS
    #define DPCLS_62_GLOBALS
#endif

#include "fgc3/inc/class/dpcls_62.h"

#elif (FGC_CLASS_ID == 63)

#ifdef DPCLS_GLOBALS
    #define DPCLS_63_GLOBALS
#endif


#include "fgc3/inc/class/dpcls_63.h"

#elif (FGC_CLASS_ID == 64)

#ifdef DPCLS_GLOBALS
    #define DPCLS_64_GLOBALS
#endif

#include "fgc3/inc/class/dpcls_64.h"

#elif (FGC_CLASS_ID == 65)

#ifdef DPCLS_GLOBALS
    #define DPCLS_65_GLOBALS
#endif

#include "fgc3/inc/class/dpcls_65.h"


#elif (FGC_CLASS_ID == 66)

#ifdef DPCLS_GLOBALS
    #define DPCLS_66_GLOBALS
#endif

#include "fgc3/inc/class/dpcls_66.h"


#else

#error "FGC_CLASS_ID is not defined"

#endif


// EOF
