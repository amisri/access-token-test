//! @file     time_fgc.h
//! @brief    Provides time related functions for the FGC

#pragma once


// ---------- Includes

#include <stdint.h>

#if defined(__RX610__)

#include <memmap.h>
#include <cclibs.h>

#endif

#if defined(__TMS320C6727__)

#include <cclibs.h>
#include "fgc3/inc/memmap.h"

#endif



// ---------- Constants

#if defined(__RX610__)
#define  TIME_FGC_UTC_UNIXTIME     UTC_MCU_UNIXTIME_P
#define  TIME_FGC_UTC_MS           UTC_MCU_MS_P
#define  TIME_FGC_UTC_US           UTC_US_P
#define  TIME_FGC_UTC_OR_US        0
#endif

#if defined(__TMS320C6727__)
#define  TIME_FGC_UTC_UNIXTIME     UTC_DSP_UNIXTIME_P
#define  TIME_FGC_UTC_MS           (UTC_DSP_US_P / 1000)
#define  TIME_FGC_UTC_US           UTC_DSP_US_P
#define  TIME_FGC_UTC_OR_US        0
#endif

#define TIME_NEVER                 0xFFFFFFFF



// ---------- External function declarations

//! This function adds a positive delay in microseconds to the given absolute time.
//!
//! Note that the function is intended for small delay_us. Even though delay_us can be greater
//! than 1E+06 (1s), this function will run slowly if delay_us is much higher than a second.
//!
//! @param abs_time Time to add the delay to.
//! @param delay_us Delay in microseconds.

void timeUsAddDelay(struct CC_us_time * abs_time, uint32_t const delay_us);


//! This function will add a positive or negative offset in microseconds to the given absolute time.
//!
//! @param abs_time        Time to add the offset to.
//! @param offset_us       Delay in microseconds.

void timeUsAddOffset(struct CC_us_time * abs_time, int32_t const offset_us);


//! This function will add a delay expressed in seconds and microseconds to the given absolute time.
//!
//! It is possible to add a negative delay, for example to subtract -1.400000 s, use the following
//! function call: timeUsAddLongDelay(time_a, -2, 600000); -> -2 + 0.600000 = -1.400000
//!
//! @param abs_time        Time to add the offset to.
//! @param delay_second    Delay in seconds. It can be negative.
//! @param delay_us        Delay in microseconds.

void timeUsAddLongDelay(struct CC_us_time       * abs_time,
                        int32_t           const   delay_second,
                        uint32_t          const   delay_us);


//! This function adds a positive delay in milliseconds to the given absolute time.
//!
//! Note that the function is intended for small delay_ms. Even though delay_ms can be greater
//! than 1E+03 (1s), this function will run slowly if delay_ms is much higher than a second.
//!
//! @param abs_time        Time to add the delay to.
//! @param delay_ms        Delay in milliseconds.

void timeMsAddDelay(struct CC_ms_time * abs_time, uint32_t const delay_ms);


//! This function will add a positive or negative offset in milliseconds to the given absolute time.
//!
//! @param abs_time        Time to add the offset to.
//! @param offset_ms       Delay in milliseconds.

void timeMsAddOffset(struct CC_ms_time * abs_time, int32_t const offset_ms);


//! This function will add a delay expressed in seconds and milliseconds to the given absolute time.
//!
//! It is possible to add a negative delay, for example to subtract -1.400000 s, use the following
//! function call: timeUsAddLongDelay(time_a, -2, 600000); -> -2 + 0.600000 = -1.400000
//!
//! @param abs_time        Time to add the offset to.
//! @param delay_second    Delay in seconds. It can be negative.
//! @param delay_ms        Delay in milliseconds.

void timeMsAddLongDelay(struct CC_ms_time       * abs_time,
                        int32_t           const   delay_second,
                        uint32_t          const   delay_ms);


//! This function returns the difference in microseconds between time1 and time2.
//! diff = time2 - time1
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                Difference between time2 and time1.

int32_t timeUsDiff(struct CC_us_time const * time1, struct CC_us_time const * time2);


//! This function returns the difference in milliseconds between time1 and time2.
//! diff = time2 - time1
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                Difference between time2 and time1.

int32_t timeMsDiff(struct CC_ms_time const * time1, struct CC_ms_time const * time2);


//! Returns True if time1 equals time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 == time1. False otherwise.

bool timeUsEqual(struct CC_us_time const * time1, struct CC_us_time const * time2);


//! Returns True if time1 is less than time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 < time1. False otherwise.

bool timeUsLessThan(struct CC_us_time const * time1, struct CC_us_time const * time2);


//! Returns True if time1 is greater than time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 > time1. False otherwise.

bool timeUsGreaterThan(struct CC_us_time const * time1, struct CC_us_time const * time2);


//! Returns True if time1 is less than or equal to time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 <= time1. False otherwise.

bool timeUsLessThanEqual(struct CC_us_time const * time1, struct CC_us_time const * time2);


//! Returns True if time1 is greater than or equal to time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 >= time1. False otherwise.

bool timeUsGreaterThanEqual(struct CC_us_time const * time1, struct CC_us_time const * time2);


//! Returns True if time1 equal to time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 == time1. False otherwise.

bool timeMsEqual(struct CC_ms_time const * time1, struct CC_ms_time const * time2);


//! Returns True if time1 is less than time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 < time1. False otherwise.

bool timeMsLessThan(struct CC_ms_time const * time1, struct CC_ms_time const * time2);


//! Returns True if time1 is greater than time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 > time1. False otherwise.

bool timeMsGreaterThan(struct CC_ms_time const * time1, struct CC_ms_time const * time2);


//! Returns True if time1 is less than or equal to time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 <= time1. False otherwise.

bool timeMsLessThanEqual(struct CC_ms_time const * time1, struct CC_ms_time const * time2);


//! Returns True if time1 is greater than or equal to time2.
//!
//! @param time1           First absolute time.
//! @param time2           Second absolute time.
//! @retval                True if time1 >= time1. False otherwise.

bool timeMsGreaterThanEqual(struct CC_ms_time const * time1, struct CC_ms_time const * time2);


//! This function will copy the input abs_time into the output converted_abs_time, converting
//! the fractional time from millisecond to microsecond as it does so.
//!
//! @param time_ms         Absolute time in milliseconds.
//! @param time_us         Absolute time in microseconds.

void timeCopyFromMsToUs(struct CC_ms_time const * time_ms, struct CC_us_time * time_us);


//! This function will copy the input abs_time into the output converted_abs_time, converting
//! the fractional time from microsecond to millisecond as it does so.
//!
//! @param time_us         Absolute time in microseconds.
//! @param time_ms         Absolute time in milliseconds.

void timeCopyFromUsToMs(struct CC_us_time const * time_us, struct CC_ms_time * time_ms);



// ---------- External function definitions

#if defined(__TMS320C6727__)

//! Returns the value in microsecond of the latched DSP tick
//!
//! @retval The value in microsecond of the latched DSP tick

static inline uint32_t timeGetDspTickHeldUs(void)
{
    return (TIME_DSP_HELD_TICK_US_P);
}


//! Returns the value in microsecond within the DSP ticks
//!
//! @retval The value in microsecond within the DSP ticks

static inline uint32_t timeGetDspTickUs(void)
{
    return (TIME_DSP_ITER_US_P);
}

#endif


//! Returns the value of the free running microsecond counter
//!
//! @retval Free running microsecond counter value

static inline uint32_t timeGetUs(void)
{
    return (TIME_US_P);
}


//! Returns the UTC Unix time in seconds.
//!
//! @retval UTC Unix time in seconds.

static inline uint32_t timeGetUtcTime(void)
{
    return (TIME_FGC_UTC_UNIXTIME);
}


//! Returns the UTC Unix millisecond time within the second..
//!
//! @retval UTC Unix millisecond time within the millisecond.

static inline uint32_t timeGetUtcTimeMs(void)
{
    return (TIME_FGC_UTC_MS);
}


//! Returns the UTC Unix microsecond time within the second.
//!
//! @retval UTC Unix microsecond time within the second..

static inline uint32_t timeGetUtcTimeUs(void)
{
    return (TIME_FGC_UTC_US);
}


//! Returns the between the current time and the one provided.
//!
//! @param time Time to compare against.
//! @retval Time difference.

static inline uint32_t timeGetUsDiffNow(uint32_t const time_us)
{
    return (timeGetUs() - time_us);
}


//! Returns the UTC microsecond time within the millisecond.
//!
//! @retval UTC microsecond time within the millisecond.

static inline uint32_t timeGetAbsUs(void)
{
    uint32_t now = timeGetUtcTimeUs();

    return (now - 1000 * (now / 1000));
}

// EOF
