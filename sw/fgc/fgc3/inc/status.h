//! @file  status.h
//! @brief Faults, warning, latched and unlatched status

#pragma once


// ---------- Includes

#include <stdint.h>

#include "fgc3/inc/bitmap.h"

#ifdef __RX610__

 extern uint16_t status_faults;
 extern uint16_t status_warnings;
 extern uint16_t status_latched;
 extern uint16_t status_unlatched;

 #include <fbs_class.h>

 #define STATUS_FAULTS       status_faults
 #define STATUS_WARNINGS     status_warnings
 #define STATUS_LATCHED      status_latched
 #define STATUS_UNLATCHED    status_unlatched

#else // __TMS320C6727__

 #include "fgc3/inc/dpcom.h"

 #define STATUS_FAULTS       dpcom.dsp.faults
 #define STATUS_WARNINGS     dpcom.dsp.warnings
 #define STATUS_LATCHED      dpcom.dsp.latched
 #define STATUS_UNLATCHED    dpcom.dsp.unlatched

#endif



// ---------- External function declarations

//! This function is called to check the operational health of the system.

void statusProcess(void);


//! Clears faults latch.

void statusFaultsReset(void);
void statusFaultsClr(uint32_t const bitmap);


//! Clears faults latch.

void statusLatchedReset(void);


//! Returns TRUE if PC_PERMIT is enabled
//!
//! Returns the logical AND of PC_PERMIT on the digital interface and PC_PERMIT in the gateway.
//! If VS.SIM_INTLKS is enabled for the device, this function always return true.
//!
//! @retval    true    PC_PERMIT is enabled in the device and in the gateway,
//!                    or simulated interlocks is enabled for the device
//!            false   At least one of the above conditions is not true

bool statusIsPcPermit(void);


// ---------- External function definitions

//  STATUS.FAULTS

//! Retreives the faults.
//!
//! @return                      Faults bitmap.

static inline uint32_t statusGetFaults(void)
{
#ifdef __RX610__
    return FAULTS;
#else // __TMS320C6727__
    return dpcom.dsp.faults;
#endif
}


//! Tests if the condition is asserted in the faults.
//!
//! @return                      True if the condition is asserted. False otherwise.

static inline bool statusTestFaults(uint16_t condition)
{
    return testBitmap(statusGetFaults(), condition);
}


//! Sets the faults.
//!
//! @param[in]   bitmap          Faults bitmap.

static inline void statusSetFaults(uint32_t const bitmap)
{
    setBitmap(STATUS_FAULTS, bitmap);
}


//! Clears the faults.
//!
//! @param[in]   bitmap          Faults bitmap.

static inline void statusClrFaults(uint32_t const bitmap)
{
    clrBitmap(STATUS_FAULTS, bitmap);
}


//! Clears all faults.

static inline void statusClrAllFaults(void)
{
    STATUS_FAULTS = 0;
}


//! Updates the status with a new bitmap
//!
//! @param[in]   bitmap          Faults bitmap.

static inline void statusUpdateFaults(uint32_t const bitmap)
{
    STATUS_FAULTS = bitmap;
}


//! If status is true, set the bitmap in the faults, otherwise, clear the bitmap.
//!
//! @param[in]   bitmap          Faults bitmap.
//! @param[in]   status          Status.

static inline void statusEvaluateFaults(uint32_t const bitmap, bool const status)
{
    if (status == true)
    {
        statusSetFaults(bitmap);
    }
    else
    {
        statusClrFaults(bitmap);
    }
}



//  STATUS.WARNINGS

//! Retreives the warnings.
//!
//! @return                      Warnings bitmap.

static inline uint32_t statusGetWarnings(void)
{
#ifdef __RX610__
    return (status_warnings | dpcom.dsp.warnings);
#else // __TMS320C6727__
    return dpcom.dsp.warnings;
#endif
}


//! Tests if the condition is asserted in the warnings.
//!
//! @return                      True if the condition is asserted. False otherwise.

static inline bool statusTestWarnings(uint16_t condition)
{
    return testBitmap(statusGetWarnings(), condition);
}


//! Sets the warnings.
//!
//! @param[in] bitmap            Warnings bitmap.

static inline void statusSetWarnings(uint32_t const bitmap)
{
    setBitmap(STATUS_WARNINGS, bitmap);
}


//! Clears the warnings.
//!
//! @param[in]   bitmap          Warnings bitmap.

static inline void statusClrWarnings(uint32_t const bitmap)
{
    clrBitmap(STATUS_WARNINGS, bitmap);
}


//! Clears all faults.

static inline void statusClrAllWarnings(void)
{
    STATUS_WARNINGS = 0;
}


//! Updates the status with a new bitmap
//!
//! @param[in]   bitmap          Faults bitmap.

static inline void statusUpdateWarnings(uint32_t const bitmap)
{
    STATUS_WARNINGS = bitmap;
}


//! If status is true, set the bitmap in the warnings, otherwise, clear the bitmap.
//!
//! @param[in]   bitmap          Warnings bitmap.
//! @param[in]   status          Status.

static inline void statusEvaluateWarnings(uint32_t const bitmap, bool const status)
{
    if (status == true)
    {
        statusSetWarnings(bitmap);
    }
    else
    {
        statusClrWarnings(bitmap);
    }
}



//  STATUS.ST_LATCHED

//! Retreives the latched conditions.
//!
//! @return                      Latched status bitmap.

static inline uint32_t statusGetLatched(void)
{
#ifdef __RX610__
    return (status_latched | dpcom.dsp.latched);
#else // __TMS320C6727__
    return dpcom.dsp.latched;
#endif
}


//! Tests if the condition is asserted in the latched status.
//!
//! @return                      True if the condition is asserted. False otherwise.

static inline bool statusTestLatched(uint16_t condition)
{
    return testBitmap(statusGetLatched(), condition);
}


//! Sets the latched conditions.
//!
//! @param[in]   bitmap          Latched status bitmap.

static inline void statusSetLatched(uint32_t const bitmap)
{
    setBitmap(STATUS_LATCHED, bitmap);
}


//! Clears the latched conditions.
//!
//! @param[in]   bitmap          Latched status bitmap.

static inline void statusClrLatched(uint32_t const bitmap)
{
    clrBitmap(STATUS_LATCHED, bitmap);
}


//! Updates the status with a new bitmap
//!
//! @param[in]   bitmap          Faults bitmap.

static inline void statusUpdateLatched(uint32_t const bitmap)
{
    STATUS_LATCHED = bitmap;
}


//! If status is true, set the bitmap in the latched conditions, otherwise, clear the bitmap.
//!
//! @param[in]   bitmap          Latched status bitmap.
//! @param[in]   status          Status.

static inline void statusEvaluateLatched(uint32_t const bitmap, bool const status)
{
    if (status == true)
    {
        statusSetLatched(bitmap);
    }
    else
    {
        statusClrLatched(bitmap);
    }
}



//  STATUS.ST_UNLATCHED

//! Retreives the unlatched conditions.
//!
//! @return                      Unlatched status bitmap.

static inline uint32_t statusGetUnlatched(void)
{
#ifdef __RX610__
    return (status_unlatched | dpcom.dsp.unlatched);
#else // __TMS320C6727__
    return dpcom.dsp.unlatched;
#endif
}


//! Tests if the condition is asserted in the unlatched status.
//!
//! @return                      True if the condition is asserted. False otherwise.

static inline bool statusTestUnlatched(uint16_t condition)
{
    return testBitmap(statusGetUnlatched(), condition);
}


//! Sets the unlatched conditions.
//!
//! @param[in]   bitmap          Unlatched status bitmap.

static inline void statusSetUnlatched(uint32_t const bitmap)
{
    setBitmap(STATUS_UNLATCHED, bitmap);
}


//! Clears the unlatched conditions.
//!
//! @param[in]   bitmap          Unlatched status bitmap.

static inline void statusClrUnlatched(uint32_t const bitmap)
{
    clrBitmap(STATUS_UNLATCHED, bitmap);
}


//! Updates the status with a new bitmap
//!
//! @param[in]   bitmap          Faults bitmap.

static inline void statusUpdateUnlatched(uint32_t const bitmap)
{
    STATUS_UNLATCHED = bitmap;
}


//! If status is true, set the bitmap in the unlatched conditions, otherwise, clear the bitmap.
//!
//! @param[in]   bitmap          Unlatched status bitmap.
//! @param[in]   status          Status.

static inline void statusEvaluateUnlatched(uint32_t const bitmap, bool const status)
{
    if (status == true)
    {
        statusSetUnlatched(bitmap);
    }
    else
    {
        statusClrUnlatched(bitmap);
    }
}


//EOF
