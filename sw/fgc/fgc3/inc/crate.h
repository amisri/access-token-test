//! @file     crate.h
//! @brief    Contains information regarding the crate

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>

#include <defconst.h>

#if defined(__RX610__)

#include <memmap.h>

#endif

#if defined(__TMS320C6727__)

#include "fgc3/inc/memmap.h"

#endif



// ---------- Internal structures, unions and enumerations

//! Actuation destination for the crate type

enum crateActuationDest
{
    CRATE_ACTUATION_DEST_NONE,
    CRATE_ACTUATION_DEST_DAC,
    CRATE_ACTUATION_DEST_SPIVS
};

//! Global variables for the module crate

typedef struct crate
{
    uint16_t                 position;          //!< FGC position in crate (0:left or 1:right)
    uint16_t                 type;              //!< Crate type number
    enum crateActuationDest  actuation_dest;    //!< Actuation destination
    bool                     popsb;             //!< True for POPS-B
} crate_t;



// ---------- External variable declarations

extern struct crate crate;



// ---------- External function declarations

//! Initialises the crate interface.

static inline void crateInit(void);


//! Returns the crate type.
//!
//! @retval Crate type.

static inline uint16_t crateGetType(void);


//! Returns the crate position.
//!
//! @retval Crate position.

static inline uint16_t crateGetPosition(void);


//! Returns the reference transmission type.
//!
//! @return CRATE_ACTUATION_DEST_NONE   No actuation destination
//! @return CRATE_ACTUATION_DEST_DAC    The actuation is sent to the DAC.
//! @return CRATE_ACTUATION_DEST_SPIVS  The actuation is sent to the SPIVS bus.

static inline enum crateActuationDest crateGetActuationDest(void);


//! S ets the reference transmission type.

static inline void crateSetActuationDest(enum crateActuationDest actuation_dest);



// ---------- External function definitions

static inline void crateInit(void)
{
    crate.type           = MODULE_CRATETYPE;
    crate.position       = 0;
    crate.actuation_dest = ((   crate.type == FGC_CRATE_TYPE_DSP_IGBT_GEN2
                             || crate.type == FGC_CRATE_TYPE_DSP_IGBT
                             || crate.type == FGC_CRATE_TYPE_RF25KV
                             || crate.type == FGC_CRATE_TYPE_MACAO
                             || crate.type == FGC_CRATE_TYPE_CANCUN20
                             || crate.type == FGC_CRATE_TYPE_CANCUN50
                             || crate.type == FGC_CRATE_TYPE_MAXIDISCAP
                             || crate.type == FGC_CRATE_TYPE_MEGADISCAP
                             || crate.type == FGC_CRATE_TYPE_HMINUS_DISCAP
                             || crate.type == FGC_CRATE_TYPE_HL_KA_MAIN
                             || crate.type == FGC_CRATE_TYPE_HL_KA_SUB)                          
                            ? CRATE_ACTUATION_DEST_SPIVS
                            : CRATE_ACTUATION_DEST_DAC);
    crate.popsb          = false;
}



static inline uint16_t crateGetType(void)
{
    return crate.type;
}



static inline uint16_t crateGetPosition(void)
{
    return crate.position;
}



static inline enum crateActuationDest crateGetActuationDest(void)
{
    return crate.actuation_dest;
}



static inline void crateSetActuationDest(enum crateActuationDest const actuation_dest)
{
    crate.actuation_dest = actuation_dest;
}



static inline void crateSetPopsB(void)
{
    crate.popsb = true;
}



static inline bool crateGetPopsB(void)
{
    return crate.popsb;
}



// EOF
