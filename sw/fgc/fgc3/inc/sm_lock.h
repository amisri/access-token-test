//! @file  sm_lock.h
//! @brief Shared memory lock mechanism
//!
//! The goal of this synchronisation mechanism is to assure read and write atomicity
//! on any data structure shared between the MCU and the DSP, when the data flow is
//! unidirectional, either MCU --> DSP or DSP --> MCU.
//!
//! Important:
//!  The SM_LOCK mechanism only applies to the use case where one processor only can write
//!  the protected data!
//!
//! Usage notes:
//!  Let us consider a typical example where the DSP needs to write a measurement associated
//!  with a timestamp. The MCU only reads those data, yet it needs a confirmation that during
//!  the read of those variables the DSP is not editing them, which could lead to an incoherent
//!  pair timestamp and measurement.
//!
//! Solution:
//!  Add in the shared memory a lock variable dedicated to the protection of this measurement.
//!
//!     SM_LOCK   meas_lock;              // measurement lock
//!     uint32_t  meas_time;              // measurement timestamp
//!     float     meas;                   // measurement value
//!
//! The DSP will protect its editing by taking and then releasing the lock
//!
//!   sm_lock_write_take(&meas_lock);         // Atomic write in shared memory
//!
//!     meas      = ...
//!     meas_time = ...
//!
//!     sm_lock_write_release(&meas_lock);
//!
//! On the MCU side:
//!
//!     bool    valid_read;
//!     SM_LOCK lock_val;
//!
//!     lock_val = sm_lock_read_poll(&meas_lock);
//!
//!     do
//!     {
//!         a = meas;
//!         b = meas_time;
//!         valid_read = sm_lock_read_check(&meas_lock, lock_val);
//!     }
//!     while(!valid_read);

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>



// ---------- External structures, unions and enumerations

typedef uint32_t   SM_LOCK;



// ---------- External function declarations

//! This function resets the shared memory lock. It should be called by the "write"
//! processor during its initialisation
//!
//! @param sm_lock Shared memory lock

void smLockWriteReset  (volatile SM_LOCK * sm_lock);


//! This function takes a shared memory lock to perform a write operation.
//!
//! @param sm_lock Shared memory lock

void smLockWriteTake(volatile SM_LOCK * sm_lock);


//! This function releases a shared memory lock used to perform a write operation.
//!
//! @param sm_lock Shared memory lock

void smLockWriteRelease(volatile SM_LOCK * sm_lock);


//! This function takes a shared memory lock to perform a read operation.
//!
//! @param sm_lock Shared memory lock

SM_LOCK smLockReadPoll(volatile SM_LOCK * sm_lock);


//! This function checks if the shared memory lock can be taken to perform a read operation.
//!
//! @param sm_lock Shared memory lock
//! @retval True if the read operation can be performed. False otherwise.

bool smLockReadCheck(volatile SM_LOCK * sm_lock, SM_LOCK lock_val);


// EOF
