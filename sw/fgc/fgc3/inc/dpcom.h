//! @file  dpcls.h
//! @brief Data structures shared between MCU and DSP
//!
//! The FPGA implements a conversion between big endian / little endian but
//! only handles correctly 32 bit data (addresses starting on 32bits boundary).
//! All fields must be 32 bit long.
//!
//! The TMS320C6727 compiler pads the structure to be multiple of the biggest
//! type inside, in this case uint32_t. But it can also access in 8, 16 or 32
//! bits. The structures needs to be packed to fit the access of registers in
//! the DPRAM (dual port RAM) implemented in the FPGA.

#pragma once


// ---------- Includes

#include <stdint.h>

#include <defconst.h>
#include <definfo.h>

#include <liblog.h>

#ifdef __RX610__

#include <inc/fgc_ether.h>
#include <common.h>
#include <dpcmd.h>
#include <histogram.h>
#include <time_fgc.h>

#else // __TMS320C6727__

#ifndef __TMS320C6727__
#define __TMS320C6727__
#endif

#include "inc/fgc_ether.h"

#include "fgc3/inc/common.h"
#include "fgc3/inc/dpcmd.h"
#include "fgc3/inc/histogram.h"
#include "fgc3/inc/time_fgc.h"

#endif



// ---------- Constants

#define PROP_BLK_SIZE           2048                         //!< Property block size in bytes (must be a power of 2)
#define PROP_BLK_SIZE_W         (PROP_BLK_SIZE/2)            //!< Property block size in words
#define PROP_BLK_SIZE_LW        (PROP_BLK_SIZE/4)            //!< Property block size in long words

#define PROP_DSP_RSP_OK         0                            //!< DSP response to a property GET/SET: OK
#define PROP_DSP_RSP_ERR_NELEM  1                            //!< DSP response to a property GET/SET: NULL pointer to n_elements
#define PROP_DSP_RSP_ERR_BUF    2                            //!< DSP response to a property GET/SET: NULL buffer pointer
#define PROP_DSP_RSP_ERR_NO_PPM 3                            //!< DSP response to a property GET/SET: property cannot be PPM

#define PFLOAT_LEN              16                           //!< Print float buffer size (bytes)

#define MCU_STARTED             0x12345678
#define DSP_STARTED             0x87654321

#define DPCOM_LOG_BLK_LEN       (FGC_ETHER_MAX_RSP_LEN / sizeof(uint32_t))

#define SPIVS_HISTORY_LENGTH    20

#if (FGC_CLASS_ID == 64)
   #define SPIVS_DBG_NUM         7
#else
   #define SPIVS_DBG_NUM         1
#endif



// ---------- Constants

//! MCU to DSP commands

enum Dpcom_mcu_commands
{
    DPCOM_MCU_COMMAND_NONE            = 0x0000,
    DPCOM_MCU_COMMAND_RESET           = 0x0001,
    DPCOM_MCU_COMMAND_INIT            = 0x0002,
    DPCOM_MCU_COMMAND_NVS_INIT        = 0x0004,
    DPCOM_MCU_COMMAND_DB_INIT         = 0x0008,
    DPCOM_MCU_COMMAND_UPDATE_REG_PARS = 0x0010,
    DPCOM_MCU_COMMAND_ADC_TEMP        = 0x0020,
    DPCOM_MCU_COMMAND_ILC_RESET       = 0x0040,
    DPCOM_MCU_COMMAND_REF_DISARM      = 0x0080,
    DPCOM_MCU_COMMAND_ADC_EXT_LIMITS  = 0x0100,

};

//! Menus to ignore

enum Dpcom_log_ignore
{
    DPCOM_LOG_IGNORE_B       ,
    DPCOM_LOG_IGNORE_V_REG   ,
    DPCOM_LOG_IGNORE_I_MEAS  ,
    DPCOM_LOG_IGNORE_I_EARTH ,
    DPCOM_LOG_IGNORE_BIS     ,
    DPCOM_LOG_IGNORE_BIS_CH1 ,
    DPCOM_LOG_IGNORE_BIS_CH2 ,
    DPCOM_LOG_IGNORE_BIS_CH3 ,
    DPCOM_LOG_IGNORE_BIS_CH4 ,
    DPCOM_LOG_IGNORE_ILC_CYC ,
    DPCOM_LOG_IGNORE_V_AC_HZ ,
};

enum Dpcom_log_ignore_bitmask
{
    DPCOM_LOG_IGNORE_B_BIT_MASK       = (1 << DPCOM_LOG_IGNORE_B       ),
    DPCOM_LOG_IGNORE_V_REG_BIT_MASK   = (1 << DPCOM_LOG_IGNORE_V_REG   ),
    DPCOM_LOG_IGNORE_I_MEAS_BIT_MASK  = (1 << DPCOM_LOG_IGNORE_I_MEAS  ),
    DPCOM_LOG_IGNORE_I_EARTH_BIT_MASK = (1 << DPCOM_LOG_IGNORE_I_EARTH ),
    DPCOM_LOG_IGNORE_BIS_BIT_MASK     = (1 << DPCOM_LOG_IGNORE_BIS     ),
    DPCOM_LOG_IGNORE_BIS_CH1_BIT_MASK = (1 << DPCOM_LOG_IGNORE_BIS_CH1 ),
    DPCOM_LOG_IGNORE_BIS_CH2_BIT_MASK = (1 << DPCOM_LOG_IGNORE_BIS_CH2 ),
    DPCOM_LOG_IGNORE_BIS_CH3_BIT_MASK = (1 << DPCOM_LOG_IGNORE_BIS_CH3 ),
    DPCOM_LOG_IGNORE_BIS_CH4_BIT_MASK = (1 << DPCOM_LOG_IGNORE_BIS_CH4 ),
    DPCOM_LOG_IGNORE_BIS_ALL_BIT_MASK = (  DPCOM_LOG_IGNORE_BIS_CH1_BIT_MASK
                                         | DPCOM_LOG_IGNORE_BIS_CH2_BIT_MASK
                                         | DPCOM_LOG_IGNORE_BIS_CH3_BIT_MASK
                                         | DPCOM_LOG_IGNORE_BIS_CH4_BIT_MASK),
    DPCOM_LOG_IGNORE_ILC_CYC_BIT_MASK = (1 << DPCOM_LOG_IGNORE_ILC_CYC ),
    DPCOM_LOG_IGNORE_V_AC_HZ_BIT_MASK = (1 << DPCOM_LOG_IGNORE_V_AC_HZ ),
};


enum Dpcom_log_signals_request
{
    DPCOM_LOG_SIGNALS_REQUEST_NONE_BIT_MASK = 0x00,
    DPCOM_LOG_SIGNALS_REQUEST_FCM_BIT_MASK  = 0x01,
    DPCOM_LOG_SIGNALS_REQUEST_PCM_BIT_MASK  = 0x02,
};



//! Calibration action

enum Dpcom_cal_action
{
    DPCOM_CAL_REQ_INTERNAL_ADC,
    DPCOM_CAL_REQ_EXTERNAL_ADC,
    DPCOM_CAL_REQ_DCCT,
    DPCOM_CAL_REQ_DAC,
    DPCOM_CAL_NUM_REQ,
    DPCOM_CAL_REQ_NONE
};

//! Calibration signal index. Must coincide with the enum SIG_cal_level
//! declared in libsig.h.

enum Dpcom_cal_level
{
    DPCOM_CAL_OFFSET,
    DPCOM_CAL_POSITIVE,
    DPCOM_CAL_NEGATIVE,
    DPCOM_CAL_NUM_CALS
};

#define CAL_TEMP                    0.0625                           //!< (= 1/16)Temperature calibration (C/raw)
#define CAL_T0                      23.0                             //!< T0 calibration temperature
#define CAL_T1                      28.0                             //!< T1 calibration temperature
#define CAL_T2                      33.0                             //!< T2 calibration temperature



// ---------- External structures, unions and enumerations

//! Buffer to exchange properties between the MCU and the DSP

struct Dpcom_prop_buf
{
    uint32_t                    action;                         //!< Action - uses fieldbus cmd pkt header bits
    uint32_t                    dsp_prop_idx;                   //!< Index in DSP property table for property
    uint32_t                    sub_sel;                        //!< Sub device selector
    uint32_t                    cyc_sel;                        //!< Cycle selector (0=not used, 1-N=slot idx)
    uint32_t                    blk_idx;                        //!< Index of block
    uint32_t                    n_elements;                     //!< Number of elements
    uint32_t                    dsp_rsp;                        //!< DSP response

    union Dpcom_prop_data_blk
    {
        uint32_t                intu[PROP_BLK_SIZE_LW];         //!< Unsigned integer results
        int32_t                 ints[PROP_BLK_SIZE_LW];         //!< Signed integer results
        float                   fp  [PROP_BLK_SIZE_LW];         //!< Floating point results

        uint32_t                log[2][DPCOM_LOG_BLK_LEN];      //!< Double buffered log read-out blocks
    } blk;
};


//! Cycle information

struct Dpcom_cycle_info
{
    struct CC_us_time           start_time;                     //!< Time-stamp of C0
    uint32_t                    sub_sel_ref;                    //!< Reference sub-device selector
    uint32_t                    cyc_sel_ref;                    //!< Reference cycle selector
    uint32_t                    sub_sel_acq;                    //!< Reference sub-device selector
    uint32_t                    cyc_sel_acq;                    //!< Reference cycle selector    int32_t
    uint32_t                    func_type;                      //!< Function type
};



//! Command Dual Port Ram Structure.
//!
//! Cmd packet parsing and DSP property communications

struct Dpcom
{
    struct Dpcom_prop_buf             pcm_prop;                      //!< PubTsk property communications
    struct Dpcom_prop_buf             fcm_prop;                      //!< FcmTsk property communications
    struct Dpcom_prop_buf             tcm_prop;                      //!< TcmTsk property communications

    struct Dpcom_dbg
    {
        uint32_t                      u[20];                         //!< TEST.DEBUG.uint32_t
        float                         f[20];                         //!< TEST.DEBUG.FLOAT

        struct Histogram              mcu_mst_us;                    //!< FGC.DEBUG.HISTOGRAM.MCU

        float                         tsk_id;                        //!< Task ID mapped to log signal ACQ:TASK_ID
    } dbg;

    struct Dpcom_log
    {
        struct LOG_mgr                log_mgr;                       //!< Structure used access logging data from liblog
        uint32_t                      cmd_idx;                       //!< 0 = fcm, 1 = pcm
        uint32_t                      ignore_mask;                   //!< Logs to be ignored
        uint32_t                      read_request;                  //!< Log read request flag
    } log;

    struct Dpcom_spivs
    {
        uint32_t                      num_sent;                      //!< Number of words sent
        uint32_t                      num_faults;                    //!< Number of faults detected

        struct Dpcom_spivs_dbg
        {
            uint32_t                  tx[SPIVS_DBG_NUM];             //!< Log of words sent
            uint32_t                  rx[SPIVS_DBG_NUM];             //!< Log of words read
            uint32_t                  error;                         //!< True if an error was detected
        } dbg[SPIVS_HISTORY_LENGTH];

        uint32_t                      dbg_idx;                       //!< Log index

    } spivs;

    uint32_t                          dsp_bgp_alive;                 //!< Used by the MCU to verify the DSP background task is alive
    uint32_t                          dsp_rtp_alive;                 //!< Used by the MCU to verify the DSP real-time task is alive

    //! MCU to DSP communication

    struct Dpcom_mcu
    {
        int32_t                       handshake;                     //!< Software handshake magic number (DSP_STARTED)
        uint32_t                      state_op;                      //!< Operational state
        uint32_t                      state_vs;                      //!< State of the VS
        uint32_t                      dsp_in_standalone;             //!< DSP is in standalone mode

        struct Dpcmd                  commands;                      //!< MCU to DSP commands

        uint32_t                      device_ppm;                    //!< DEVICE.PPM
        uint32_t                      device_multi_ppm;              //!< DEVICE.MULTI_PPM

        uint32_t                      debug_mem_dsp_addr;            //!< Start of address for debug memory zone

        struct Dpcom_pc
        {
            uint32_t                   mode;                         //!< PC mode: this is the target REF state
            uint32_t                   state;                        //!< PC state
            uint32_t                   state_bitmask;                //!< PC state bitmask
        } pc;

        struct Dpcom_mcu_ana
        {
            int32_t                    anabus_input;                 //!< Analogue bus input requested by the MCU
            uint32_t                   adc_mask;                     //!< ADCs to be connected to the analogue bus, as requested by MCU
            uint32_t                   req_f;                        //!< Set to TRUE by MCU, cleared (set to FALSE) by DSP
        } ana;

        struct Dpcom_mcu_cal
        {
            uint32_t                   active;                       //!< CAL.ACTIVE property
            uint32_t                   req_f;                        //!< Set by MCU, cleared by DSP
            int32_t                    action;                       //!< Cast to 'enum cal_action' on FGC3
            uint32_t                   chan_mask;                    //!< Mask of the input channels (2 on the FGC2, 4 on the FGC3)
            uint32_t                   ave_steps;                    //!< Averaging period in 200-sample steps (max 50)
            uint32_t                   idx;                          //!< CAL_IDX_OFFSET/CAL_IDX_POS/CAL_IDX_NEG, or measurement number (0-4)
        } cal;

        struct Dpcom_mcu_ref
        {
            float                      rt;                           //!< REF.RT
        } ref;

        struct Dpcom_mcu_meas
        {
           float                      i_earth[4];                    //!< Earth current
           uint32_t                   dcct_flt;                      //!< DCCT fault flags (A:0x1 B:0x2)
        } meas;

        struct Dpcom_mcu_temp
        {
            float                     in;                            //!< FGC3 inlet temperature
            float                     out;                           //!< FGC3 outlet temperature
            float                     adc;                           //!< ADC.INTERNAL.TEMPERATURE Internal ADC temperature (units: 1/16 C)
            float                     dcct[2];                       //!< DCCT.#.TEMPERATURE       DCCT electronics temperature (units: 1/16 C)
        } temp;

        struct Dpcom_mcu_vref_temp
        {
            float                     ref;                           //!< ADC.INTERNAL.VREF_TEMP.REF V_REF temperature reference
            float                     meas;                          //!< ADC.INTERNAL.VREF_TEMP.MEAS Measured V_REF temperature
        } vref_temp;

        struct Dpcom_mcu_cycle
        {
            struct Dpcom_cycle_info   info;                          //!< Next cyle information
            uint32_t                  start_latched;                 //!< True when a new start cycle event has been received
            uint32_t                  sim_f;                         //!< Super-cycle simulation flag
        } cycle;

        struct Dpcom_white_rabbit
        {
            uint32_t                  enabled;                       //!< Retrieve Bmeas from the WhiteRabbit interface
        } wr;

        struct Dpcom_mcu_pll
        {
            float                     dac;                           //!< PLL DAC value
            int32_t                   e18_error;                     //!< PLL ETH18 phase error
            int32_t                   e19_error;                     //!< PLL ETH19 phase error
            float                     error;                         //!< PLL phase error applied to the PI
            float                     eth_sync_cal;                  //!< PLL ETH sync
            float                     ext_avg_error;                 //!< PLL external average phase error
            int32_t                   ext_error;                     //!< PLL external phase error
            float                     filtered_integrator;           //!< PLL filtered integrator
            float                     integrator;                    //!< PLL integrator
            float                     net_avg_error;                 //!< PLL network phase error
            uint32_t                  state;                         //!< PLL state
        } pll;

        struct Dpcom_mcu_dim_ana
        {
            struct CC_us_time          time_stamp;                   //!< Timestamp when the DIMs were acquired
            uint32_t                   dim_idx;                      //!< DIM index
            float                      values[4];                    //!< Four analogue values for a DIM
        } dim_ana;

        struct Dpcom_mcu_log
        {
            uint32_t                   zero_slow_abort_time;         //!< When True, zero LOG.MENU.SLOW_ABORT_TIME
            uint32_t                   ignore_mask;                  //!< Logs to be ignored
            uint32_t                   signals_request[2];           //!< Request from the MCU to the DSP to transfer the signals
            uint32_t                   post_mortem_active;           //!< True if a post-mortem or FGC Logger event is active
        } log;
    } mcu;

    //!DSP to MCU communication

    struct Dpcom_dsp
    {
        int32_t                       handshake;                      //!< Software handshake magic number (DSP_STARTED)
        uint32_t                      version;                        //!< Version number
        uint32_t                      cpu_usage;                      //!< [1ms]   CPU usage in %
        uint32_t                      run_code;                       //!< [isr]   Isr run codes
        uint32_t                      errnum;                         //!< Background processing error number
        uint32_t                      faults;                         //!< [20ms]  Faults
        uint32_t                      warnings;                       //!< [20ms]  Warnings
        uint32_t                      unlatched;                      //!< [20ms]  Unlatched status
        uint32_t                      latched;                        //!< [20ms]  Latched status

        // Debug zone for DSP

        uint32_t                      debug[2];

        char                          assert_file[16];
        uint32_t                      assert_line;
        uint32_t                      stack_usage;                    //!< Stack usage

        // Debug zone for DEBUG.MEM.DSP

        uint32_t                      debug_mem_dsp_i[DB_DSP_MEM_LEN]; //!< For DEBUG.MEM.DSP (int value)
        float                         debug_mem_dsp_f[DB_DSP_MEM_LEN]; //!< For DEBUG.MEM.DSP (float value)

        struct Dpcom_dsp_panic
        {
            uint32_t                   panic_code;                   //!< DSP panic code
        } panic;

        struct Dpcom_dsp_ana_state
        {
            int32_t                    mpx[FGC_N_INT_ADCS];          //!< Read part of property ADC.INTERNAL.MPX (write done via a
                                                                     //!< function SetInternalAdcMpx). On the DSP, it can be casted
                                                                     //!< with enum type ANALOGUE_BUS_INPUT.
        } ana;

        struct Dpcom_dsp_cal
        {
            uint32_t                   complete_f;                   //!< Calibration complete.
        } cal;

        struct Dpcom_dsp_adc                                         //! Data for all ADCs
        {
            int32_t                    raw_200ms     [FGC_N_ADCS];   //!< [200ms] Average calibrated (Vraw)
            int32_t                    raw_1s        [FGC_N_ADCS];   //!< [1s]    Average calibrated (Vraw)
            float                      volts_200ms   [FGC_N_ADCS];   //!< [200ms] Average calibrated in Volts (Vadc)
            float                      volts_1s      [FGC_N_ADCS];   //!< [1s]    Average calibrated in Volts (Vadc)
            float                      pp_volts_1s   [FGC_N_ADCS];   //!< [1s]    Peak-peak range for volts over 1s
            float                      cal_meas_1s   [FGC_N_ADCS];   //!< [1s]    Average calibrated measurement (field, current or voltage)
            float                      cal_meas_200ms[FGC_N_ADCS];   //!< [200ms] Average calibrated measurement (field, current or voltage)
            float                      amps_200ms    [2];            //!< [200ms] Average calibrated in Amps  (DCCT: IA, IB)
        } adc;

        struct Dpcom_dsp_fifo
        {
            uint32_t                   in_idx;                       //!< Next sample to write
            uint32_t                   out_idx;                      //!< Next sample to read
        } fifo;

        struct Dpcom_dsp_meas
        {
            uint32_t                   adc_state[FGC_N_INT_ADCS];    //!< ADC status
            uint32_t                   dcct_state[2];                //!< DCCT status
        } meas;

        struct Dpcom_dsp_cycle
        {
            struct Dpcom_cycle_info    current;                      //!< Current cycle
            struct Dpcom_cycle_info    previous;                     //!< Previous cycle
            int32_t                    pub_previous_cycle;           //!< Flag to indicate the MCU to publish the data of the cycle ended
        } cycle;

        struct Dpcom_dsp_log
        {
            struct CC_us_time          slow_abort_time;             //!< Time recorded by libref when entering TO_OFF (SLOW_ABORT)
            uint32_t                   slow_abort_faults;           //!< Copy of MCU.SLOW_ABORT_FAULTS

            struct Dpcom_dsp_log_timing
            {
                struct Dpcom_cycle_info info;                       //!< Log timing: cycle start, cycle subdevice selector, cycle cycle selector, function type
                uint32_t               status_bitmask;              //!< Log timing: status bitmask
            } timing;

            struct Dpcom_dsp_log_comms
            {
                uint32_t               read_req_status;              //!< Status from Read Request
                struct CC_us_time      first_sample_time;            //!< Time of first sample (unix time + us time)
                uint32_t               header_size;                  //!< Header size in bytes
                uint32_t               data_size;                    //!< Data size in bytes
                uint32_t               blk_len[2];                   //!< Double buffered block lengths in elements. Index = buf_index & 1. Cleared to zero by MCU once data has been copied.
            } comms[2];                                              //!< Log readout communications: [0] = fcm, [1] = pcm
        } log;
    } dsp;
};



// ---------- External variable declarations

extern volatile struct Dpcom dpcom;


// EOF

