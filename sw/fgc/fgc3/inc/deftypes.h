//! @file  deftypes.h
//! @brief Type declarations for property definitions

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- Constants

#define ABSTIME_SIZE  8  // Size of ABSTIME property element on FGC3 (is it sizeof(struct timeval) on FGCD)



// ---------- External structures, unions and enumerations

//! Type to use with string constants

typedef const char * def_const_string;

//! Type to use within defprops.h for struct prop flags to truncate flags to
//! the size of the field 16-bit for FGCs and 32-bit for FGCD

typedef uint16_t   def_props_flags;


#define CONST const


// EOF
