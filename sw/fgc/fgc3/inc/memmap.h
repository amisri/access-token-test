//! @file     memmap.h
//! @brief    Common to memmap_mcu.h and memmap_dsp.h

#pragma once



// ---------- Includes

#if defined(__RX610__)

#include "platforms/fgc3/memmap_mcu.h"

#define MODULE_CRATETYPE MID_CRATETYPE_P

#endif

#if defined(__TMS320C6727__)

#include "inc/platforms/fgc3/memmap_dsp.h"

#define MODULE_CRATETYPE DSP_MID_CRATETYPE_P

#endif


// EOF
