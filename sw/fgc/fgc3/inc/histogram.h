//! @file  histogram.h
//! @brief File providing functionality for generic histograms

#pragma once



// ---------- Includes

#include <stdint.h>

#include <defconst.h>



// ---------- Constants

//! One histogram for each millisecond in the 20 ms cycle

#define FGC_HISTOGRAM_COM_NUM   FGC_HISTOGRAM_LEN



// ---------- External structures, unions and enumerations

//! Histogram structure

struct Histogram
{
    uint32_t   bins[FGC_HISTOGRAM_NUM_BINS];
    int32_t    bin_min;
    uint32_t   bin_width;
    int32_t    min;
    int32_t    max;
};


//! Histogram structure for fieldbus communication

struct Histogram_com
{
    struct Histogram histos[FGC_HISTOGRAM_COM_NUM];
};



// ---------- External function declarations

//! Initialises the histogram
//!
//! @param[out]  histo      Pointer to histogram structure
//! @param[in]   bin_min    Minimum bin value
//! @param[in]   bin_width  Bin width

void histogramInit(struct Histogram * histo,
                   int16_t            bin_min,
                   uint16_t           bin_width);


//! Zeroes the histogram bins
//!
//! @param[out]  histo      Pointer to histogram structure

void histogramZero(struct Histogram * histo);


//! Adds a new value to the histogram
//!
//! @param[in,out]  histo      Pointer to histogram structure
//! @param[in]      value      Value to add

void histogramAddValue(struct Histogram * histo,
                       int32_t            value);



#if defined(__RX610__)

#include <stdio.h>

//! Initialises the communication histograms
//!
//! @param[out]  histo      Pointer to communication histogram structure
//! @param[in]   bin_min    Minimum bin value
//! @param[in]   bin_width  Bin width

void histogramComInit(struct Histogram_com * histo_com,
                      int16_t                bin_min,
                      uint16_t               bin_width);


//! Adds a new value to the histogram
//!
//! @param[in,out]  histo      Pointer to communication histogram structure
//! @param[in]      value      Value to add

void histogramComAddValue(struct Histogram_com * histo_com,
                          int32_t                value);


//! Prints the histogram data
//!
//! @param[in]      fd         File descriptor
//! @param[in]      delim      Line delimeter
//! @param[in]      histo      Pointer to the histogram structure
//! @param[in]      num        Number of histograms to dump

void histogramPrint(FILE * fd, char delim, struct Histogram const * histo, uint8_t num);


#endif


// EOF
