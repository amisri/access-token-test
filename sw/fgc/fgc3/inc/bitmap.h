//! @file  bitmap.h
//! @brief Miscellaneous functions

#pragma once



// ---------- External function definitions

//! These macro functions are not inline functions because often FPGA registers
//! (type REG16U or REG32U) are set or clear. This causes a compilation error
//! unless the argument is converted to uint32_t *.


//! Sets a bit mask in a value

#define setBitmap(value, bit_mask)       ((value) |= (bit_mask))

//! Clears the bit mask from a value

#define clrBitmap(value, bit_mask)       ((value) &= ~(bit_mask))

//! Tests if at least one bit in the bit mask is set in value

#define testBitmap(value, bit_mask)      (!!((value) & (bit_mask)))

//! Test if all bits in bit mask are set in a value.

#define testAllBitmap(value, bit_mask)   (((value) & (bit_mask)) == (bit_mask))

//! Sets a bit mask if the condition is true, clears it otherwise

#define updateBitmap(value, bit_mask, condition)   do {                                  \
                                                       if ((condition) == true)          \
                                                       {                                 \
                                                           setBitmap(value , bit_mask);  \
                                                       }                                 \
                                                       else                              \
                                                       {                                 \
                                                           clrBitmap(value , bit_mask);  \
                                                       }                                 \
                                                    } while(0)                           \



#define ArrayLen(arr)         ( sizeof(arr) / sizeof(arr[0]) )

// EOF
