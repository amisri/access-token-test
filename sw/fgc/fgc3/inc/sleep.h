//! @file     sleep.h
//! @brief    Provides sleep functions

#pragma once


// ---------- Includes

#include <stdint.h>

#if defined(__RX610__)

#include <time_fgc.h>

#endif

#if defined(__TMS320C6727__)

#include "fgc3/inc/time_fgc.h"

#endif



// ---------- External function declarations

//! Sleeps during the number of microseconds specified in the argument.
//!
//! @param time Time in microseconds to sleep.

static inline void sleepUs(uint32_t const time);


//! Sleeps during the number of milliseconds specified in the argument.
//!
//! @param time Time in milliseconds to sleep.

static inline void sleepMs(uint32_t const time);



// ---------- External function definitions

static inline void sleepUs(uint32_t const time)
{
    uint32_t start = timeGetUs();

    while (timeGetUsDiffNow(start) <= time)
    {
        ;
    }
}


static inline void sleepMs(uint32_t const time)
{
    sleepUs(time * 1000);
}


// EOF
