//! @file  boot_dual_port_ram.h
//! @brief duap port memory
//!
//! The dual port ram is implemented inside the xilinx
//! and can be accessed by the DSP TMS320c6727b and the MCU Rx610
//!
//! The TI compiler for the TMS320c6727b pads the structure to be multiple of the biggest type inside,
//! in this case uint32_t (but it can access in 8, 16 or 32 bits) or bigger (Compiler guide 8.2.1.6).
//! To ensure dpram is aligned on both sides (DSP and MCU), we compile on dsp side with -gcc option.
//! Thus we can use aligned(4) attribute on both side. If a struct on dsp side is actually aligned on
//! bigger amount (i.e. 8 aligned if the struct or nested struct contains a 64 bit type), this will produce
//! a warning: "warning #1236-D: a reduction in alignment without the "packed" attribute is ignored".
//! Packed attribute is not supported on c6727. Thus we _must_ increased the aligned size to get rid of this warning,
//! and add padding if necessary. This way we know we follow the same alignment on both mcu and dsp.
//!
//! The PLD allows only 16 bits access for its normal registers
//!
//! For the Rx610 the PLD implements a conversion between big endian / little endian
//! but ONLY HANDLES CORRECTLY 32 BITS DATA !!!! (addresses starting on 32bits boundary)
//! so all fields need to be 32bits long

#pragma once

#ifdef BOOT_DUAL_PORT_RAM_GLOBALS
#define BOOT_DUAL_PORT_RAM_VARS_EXT
#else
#define BOOT_DUAL_PORT_RAM_VARS_EXT extern
#endif


// ---------- Includes

#include <stdbool.h>

#ifdef __TMS320C6727__
#include <structs_bits_little.h>
#include <memmap_dsp.h>
#endif
#ifdef __RX610__
#include <structs_bits_big.h>
#include <memmap_mcu.h>
#endif

#include <defconst.h>
#include <definfo.h>
#include <version.h>


#define  DPRAM_ARG_MAX      10

#if defined (__RX610__)
#define PACKED_ATTRIBUTE_IF_SUPPORTED   packed,
#elif defined (__TMS320C6727__)
#define PACKED_ATTRIBUTE_IF_SUPPORTED
#endif



// ---------- External structures, unions and enumerations

//! DSP zone in the Dual Port RAM
//! (even the ADC came from the DSP they have their own zone)

struct TDspSlowCmdRsp_GET_INFO  // DSP_CMD_SLOW_GET_INFO
{
    uint32_t  time_consumed_in_msTickIsr;             // [us]
    uint32_t  time_consumed_in_interpolation;         // [us]
    uint32_t  time_consumed_in_msTickIsr_max;         // [us]
    uint32_t  time_consumed_in_interpolation_max;     // [us]
    uint32_t  msTick;     // [us] between the last 2 ISRs
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;
struct TMcuSlowCmd_spivs
{
    union TUnion32Bits  control;    // Control SPIVS
    union TUnion32Bits  num_lwords; // Number of words for transaction
    uint32_t            buf[7];     // Buffer used for rx and tx data
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;

union TDspSlowCmdRsp
{
    uint32_t                        dw[DPRAM_ARG_MAX];
    char                            ch[4 * DPRAM_ARG_MAX];    // DSP_CMD_SLOW_GET_VERSION
    uint32_t                        data;       // DSP_CMD_SLOW_RETURN_NEG_DATA
    struct TDspSlowCmdRsp_GET_INFO  get_info;   // DSP_CMD_SLOW_GET_INFO
    struct TMcuSlowCmd_spivs        spivs;
    union TUnion32Bits              data32;
    // union TUnion64Bits              data64;
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(8)))
#endif
;

union TDspFastCmdRsp
{
    uint32_t              data;   // DSP_CMD_FAST_RETURN_NEG_DATA
}
#if defined (__GNUC__)
__attribute__((PACKED_ATTRIBUTE_IF_SUPPORTED aligned(4)))
#endif
;


struct TDualPortRam_dsp_zone    // DPRAM zone writable by DSP TMS320c6727
{
    volatile union TDspSlowCmdRsp   slowRsp;
    volatile union TDspFastCmdRsp   fastRsp;
    volatile uint32_t               status;
    volatile uint32_t               version;
    volatile uint32_t               sdram_test_n;
    volatile uint32_t               irq_counter;
    volatile float                  regulated_current;
    volatile float                  regulated_voltage;
};


struct TMcuSlowCmdArg_ADC_PATH  // DSP_CMD_SLOW_ADC_PATH
{
    uint32_t  external_signal_access;     // ANA_SWIN
    uint32_t  adc_selector_bus_or_signal; // SW_BUS
    uint32_t  internal_signal_bus_access; // MPX
};


union TMcuSlowCmdArg
{
    uint32_t                                dw[DPRAM_ARG_MAX];
    uint32_t                                data;
    float                                   regulation_reference;
    struct TMcuSlowCmdArg_ADC_PATH          adc_path;                   // DSP_CMD_SLOW_ADC_PATH
    struct TMcuSlowCmd_spivs                spivs;
    // I only needs 24 for the moment but due to the PLD implementation of the big / little endian conversion (based on 32bits words) I made it multiple of 32bits words
    char                                    str[32];
};

union TMcuFastCmdArg
{
    uint32_t                  data;
    int32_t                   int32s;
};


struct TDualPortRam_mcu_zone        // DPRAM zone writable by M32C87 / RX610
{
    volatile uint32_t                 fastCmd;        // Set by MCU, reset to 0 by DSP when finished
    volatile uint32_t                 slowCmd;        // Set by MCU, reset to 0 by DSP when finished
    volatile uint32_t                 mode;
    volatile union TMcuFastCmdArg   fastArg;
    volatile union TMcuSlowCmdArg   slowArg;
    uint32_t      pll_debug_data[6];
};


enum Adc_test
{
    ADC_TEST_10V   = 0,
    ADC_TEST_N10V  = 2,
    ADC_TEST_0V    = 4
};

struct Adc_error_data
{
    uint32_t detected;
    uint32_t channel;
    int32_t  data;
    int32_t  limits[2];
};

struct TDualPortRam_AnalogueData
{
    volatile union TUnion64Bits    sum[4];         // Sum values for each channel
    volatile struct Adc_error_data error;
    volatile uint32_t                n_samples;      // number of samples to be taken
    volatile uint32_t                is_first_sample;// 1st sample flag
    volatile int32_t                max[4];         // Max values for each channel
    volatile int32_t                min[4];         // Min values for each channel
    volatile uint32_t              test;
    volatile uint32_t              testing_on;
    volatile uint32_t              reading_on;
};


struct TDualPortRam_ExchangeZone
{
    uint32_t                            handshake;
    uint32_t                            dbg[24];
    struct TDualPortRam_dsp_zone        dsp;
    struct TDualPortRam_mcu_zone        mcu;
    struct TDualPortRam_AnalogueData    adc;
};



// ---------- External variable declarations

BOOT_DUAL_PORT_RAM_VARS_EXT volatile struct TDualPortRam_ExchangeZone * dpram

#ifdef BOOT_DUAL_PORT_RAM_GLOBALS

#ifdef __RX610__
      = (struct TDualPortRam_ExchangeZone *) DPRAM_START_32
#endif

#ifdef __TMS320C6727__
      = (struct TDualPortRam_ExchangeZone *) DPRAM_32;
#endif

#endif
        ;


// EOF
