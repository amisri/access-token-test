//! @file  boot_dsp_cmds.h
//! @brief constants for FCG shared by CPU and DSP

#pragma once



// ---------- External structures, unions and enumerations

enum dsp_command
{
    DSP_CMD_NOTHING,
    DSP_CMD_SLOW_TST_SDRAM,
    DSP_CMD_SLOW_TST_PLD_DPRAM,
    DSP_CMD_SLOW_ADC_PATH,
    DSP_CMD_SLOW_SET_DAC1,
    DSP_CMD_SLOW_SET_DAC2,
    DSP_CMD_SLOW_RETURN_NEG_DATA,
    DSP_CMD_SLOW_GET_INFO,
    DSP_CMD_SLOW_GET_SPY_INFO,
    DSP_CMD_SLOW_SET_REFERENCE,
    DSP_CMD_SLOW_START_STOP_REGULATION,
    DSP_CMD_SLOW_SELECT_ALGORITHM,
    DSP_CMD_SLOW_StrToFP32,
    DSP_CMD_SLOW_StrToFP64,
    DSP_CMD_FAST_RETURN_NEG_DATA,
    DSP_CMD_FAST_STRESS_TST_PLD_DPRAM,
    DSP_CMD_FAST_CLEAR_INFO,
    DSP_CMD_SLOW_SET_SPY_OWNER,
    DSP_CMD_SLOW_SPY_TEST,
    DSP_CMD_SLOW_SPY_ONE_SHOT,
    DSP_CMD_SLOW_SET_SPIVS_CTRL,
    DSP_CMD_SLOW_SET_SPIVS_SEND_VALUE,
    DSP_CMD_SLOW_SET_SPIVS_NUM_LWORDS,
    DSP_CMD_SLOW_GET_SPIVS_SEND_VALUE,
    DSP_CMD_SLOW_GET_SPIVS_RETURN_VALUE,
    DSP_CMD_SLOW_GET_VERSION_UNIXTIME_A,
    DSP_CMD_SLOW_GET_VERSION_HOSTNAME,
    DSP_CMD_SLOW_GET_VERSION_ARCHITECTURE,
    DSP_CMD_SLOW_GET_VERSION_CC_VERSION,
    DSP_CMD_SLOW_GET_VERSION_USER_GROUP,
    DSP_CMD_SLOW_GET_VERSION_DIRECTORY,
    DSP_CMD_FAST_SIGNAL_MEASURE,
    DSP_CMD_SLOW_MID_FGCTYPE_TEST,
    DSP_CMD_SLOW_MID_FGCVER_TEST,
    DSP_CMD_SLOW_MID_PLDTYPE_TEST,
    DSP_CMD_SLOW_MID_PLDVER_TEST,
    DSP_CMD_SLOW_MID_CRATETYPE_TEST,
    DSP_CMD_SLOW_MID_NETTYPE_TEST,
    DSP_CMD_SLOW_MID_ANATYPE_TEST,
    DSP_CMD_SLOW_MID_MISMATCH_TEST,
    DSP_CMD_SLOW_MID_EOMID_TEST,
    DSP_CMD_SLOW_PEEK_WORD,
    DSP_CMD_SLOW_POKE_WORD_ADDRESS,
    DSP_CMD_SLOW_POKE_WORD_VALUE,
    DSP_CMD_SLOW_ADC_TEST,
    DSP_CMD_SLOW_ADC_DEFAULT,
    DSP_CMD_SLOW_IRQTRL_DSP_TRIG,
    DSP_CMD_LAST        // dummy to have the number of elements to use in the for/while loops
};


// DSP MID test error masks

#define DSP_MID_OK      0x00000000
#define DSP_MID_ERROR   0x00010000


// DSP STATUS

#define  DSP_STS_NOTHING                0x0000
#define  DSP_STS_ON                     0x0001
//                                      0x0002
//                                      0x0004
//                                      0x0008
#define  DSP_STS_BAD_IRQ                0x0010
#define  DSP_STS_CMD_UNKNOWN            0x0020
//                                      0x0040
#define  DSP_STS_SPY_OVERUN             0x0080
#define  DSP_STS_MEM_DATA_FAULT         0x0100
#define  DSP_STS_MEM_ADDRESS_FAULT      0x0200
#define  DSP_STS_MEM_DEVICE_FAULT       0x0400
//                                      0x0800


// EOF
