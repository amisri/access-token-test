//! @file  dpcls_64.h
//! @brief Class specific data structres shared between the MCU and the DSP
//!
//! The FPGA implements a conversion between big endian / little endian but
//! only handles correctly 32 bit data (addresses starting on 32bits boundary).
//! All fields must be 32 bit long.
//!
//! The TMS320C6727 compiler pads the structure to be multiple of the biggest
//! type inside, in this case uint32_t. But it can also access in 8, 16 or 32
//! bits. The structures needs to be packed to fit the access of registers in
//! the DPRAM (dual port RAM) implemented in the FPGA.

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- Constants

//! Structure containing class-specific parameters shared by the MCU and the DSP.

struct Dpcls
{
    struct Dpcls_oasis
    {
        uint32_t                      subsampling;                   //!< LOG.OASIS.SUBSAMPLE
    } oasis;

    //! MCU to DSP communication

    struct Dpcls_mcu_t
    {
        struct Dpcls_mcu_meas
        {
            uint32_t                   acq_group;                    //!< MEAS.ACQ_GROUP
        } meas;

        struct Dpcls_mcu_ref
        {
            struct Dpcls_mcu_ref_rt
            {
                float                  value;                        //!< Real-time value: not used
            } rt;

            uint32_t                   reg_mode;                     //!< Regulation mode
        } ref;

        struct Dpcls_mcu_vs
        {
            uint32_t                   blockable;                    //!< VS.BLOCKABLE
        } vs;

    } mcu;
};



// ---------- External variable declarations

extern volatile struct Dpcls dpcls;


// EOF
