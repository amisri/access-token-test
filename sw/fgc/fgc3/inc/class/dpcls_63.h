//! @file  dpcls_63.h
//! @brief Class specific data structres shared between the MCU and the DSP
//!
//! The FPGA implements a conversion between big endian / little endian but
//! only handles correctly 32 bit data (addresses starting on 32bits boundary).
//! All fields must be 32 bit long.
//!
//! The TMS320C6727 compiler pads the structure to be multiple of the biggest
//! type inside, in this case uint32_t. But it can also access in 8, 16 or 32
//! bits. The structures needs to be packed to fit the access of registers in
//! the DPRAM (dual port RAM) implemented in the FPGA.

#pragma once


// ---------- Includes

#include <stdint.h>

#include <libpolswitch.h>

#include <defconst.h>

#include <interFgcMgr.h>

#ifdef __TMS320C6727__

#include <libref/refFgParIdx.h>
#include <libreg/regDecoStructs.h>

#include "fgc3/inc/dpcmd.h"
#include "fgc3/inc/memmap.h"
#include "fgc3/inc/sm_lock.h"
#include "fgc3/inc/time_fgc.h"

#endif

#ifdef __RX610__

#include <cclibs/libref/inc/libref/refFgParIdx.h>
#include <cclibs/libreg/inc/libreg/regDecoStructs.h>

#include <dpcmd.h>
#include <memmap.h>
#include <sm_lock.h>
#include <time_fgc.h>

#endif



// ---------- Constants

//! Invalid function argument

#define FUNC_ARM_INVALID_PARAM      0xFFFFFFFF

//! MCU to DSP commands

enum Dpcls_event_received_bitmask
{
    DPCLS_EVT_RECEIVED_REF                  = 0x0001,
    DPCLS_EVT_RECEIVED_PAUSE                = 0x0002,
    DPCLS_EVT_RECEIVED_RESUME               = 0x0004,
    DPCLS_EVT_RECEIVED_START                = 0x0008,
    DPCLS_EVT_RECEIVED_ABORT                = 0x0010,
    DPCLS_EVT_RECEIVED_ECONOMY_FULL_ENABLE  = 0x0020,
    DPCLS_EVT_RECEIVED_ECONOMY_FULL_DISABLE = 0x0040,
    DPCLS_EVT_RECEIVED_ECONOMY_FULL_ONCE    = 0x0080,
    DPCLS_EVT_RECEIVED_ECONOMY_DYNAMIC      = 0x0100,
    DPCLS_EVT_RECEIVED_ACQUISITION          = 0x0200,
    DPCLS_EVT_RECEIVED_COAST                = 0x0400,
    DPCLS_EVT_RECEIVED_RECOVER              = 0x0800,
    DPCLS_EVT_RECEIVED_START_HARMONICS      = 0x1000,
    DPCLS_EVT_RECEIVED_STOP_HARMONICS       = 0x2000,
};


//! Types of arming a function

enum Dpcls_func_arm_type
{
    DPCLS_FUNC_ARM_TYPE_NONE    = 0,
    DPCLS_FUNC_ARM_TYPE_TEST    = 1,
    DPCLS_FUNC_ARM_TYPE_ARM     = 2,
    DPCLS_FUNC_ARM_TYPE_COMMIT  = 3,
    DPCLS_FUNC_ARM_TYPE_RESTORE = 4,
};


//! Structure containing class-specific parameters shared by the MCU and the DSP.

struct Dpcls
{
    struct Dpcls_oasis
    {
        uint32_t                       v_interval_ns;                 //!< LOG.OASIS.V.INTERVAL_NS
        uint32_t                       i_interval_ns;                 //!< LOG.OASIS.I.INTERVAL_NS
        uint32_t                       b_interval_ns;                 //!< LOG.OASIS.B.INTERVAL_NS
        uint32_t                       cycle_tag_next;                //!< Next cycle tag
        uint32_t                       cycle_tag[FGC_MAX_USER_PLUS_1]; //!< LOG.OASIS.*.CYCLE_TAG
        struct CC_us_time              timestamp;                     // Timestamp for the OASIS data
        uint32_t                       subsampling;                   //!< LOG.OASIS.SUBSAMPLE
    } oasis;

    struct Inter_fgc_mgr               inter_fgc_mgr;
    struct POLSWITCH_mgr               polswitch_mgr;                 //!< Polarity switch manager
    struct REG_deco_shared             deco;                          //!< Decoupling data shared between Inter-FGC functions on the MCU and libreg on the DSP

    //! MCU to DSP communication

    struct Dpcls_mcu_t
    {
        struct Dpcls_mcu_adc
        {
            uint32_t                   ext_type[FGC_N_EXT_ADCS];     //!< ADC.EXTERNAL.TYPE
        } adc;

        struct Dpcls_mcu_ref
        {
            uint32_t                   force_off;                    //!< Forces the converter to OFF when S PC OFF in SLOW_ABORT

            struct Dpcls_mcu_ref_func
            {
                uint32_t               arm_type;                     //!< New function is available to be armed
                uint32_t               cyc_sel;                      //!< Cycle selector to arm
                uint32_t               sub_sel;                      //!< Subdevice selector to arm
                uint32_t               type;                         //!< Function type
                uint32_t               num_pars;                     //!< Number of parameters set in the array below
                float                  pars[REF_FG_PAR_MAX_ARM_PAR_VALUES]; //!< Function parameters when S REF type,pars
            } func;
        } ref;

        struct Dpcls_mcu_transaction
        {
            uint32_t                   last_param[FGC_MAX_SUB_DEVS]; //!< Last transactional parameter set
        } transaction[FGC_MAX_USER_PLUS_1];

        struct Dpcls_mcu_event
        {
            struct CC_us_time          func_run_start_time_us;       //!< Start function delay in unix time

            uint32_t                   sub_sel;                      //!< Sub-device selector
            uint32_t                   cyc_sel;                      //!< Cycle selector
            struct CC_us_time          start_event_time_us;          //!< Start event time
            struct CC_us_time          pause_event_time_us;          //!< Pause event time
            struct CC_us_time          resume_event_time_us;         //!< Resume event time
            struct CC_us_time          acquisition_event_time_us;    //!< Acquisition event time
            uint32_t                   acquisition_event_cyc_sel;    //!< Acquisition event cycle selector
            struct CC_us_time          coast_event_time_us;          //!< Coast event time
            struct CC_us_time          recover_event_time_us;        //!< Recover event time
            struct CC_us_time          start_harmonics_event_time_us;//!< Start harmonics event time
            struct CC_us_time          stop_harmonics_event_time_us; //!< Stop harmonics event time
            struct Dpcmd               received;                     //!< Event mask MCU->DSP
        } event;

        struct Dpcls_mcu_vs
        {
            uint32_t                   blockable;                    //!< VS.BLOCKABLE
        } vs;
    } mcu;

    //! DSP to MCU communication

    struct Dpcls_dsp
    {
        uint32_t                       load_select;                  //!< Load select
        uint32_t                       dcct_select;                  //!< Copy of DCCT.SELECT stored in CCLIBS

        struct Dpcls_dsp_limits
        {
            float                      i_pos;                        //!< LIMITS.I.POS[LOAD.SELECT]
        } limits;

        struct Dpcls_dsp_status
        {
            uint32_t                   reg_faults;                   //!< STATUS.REG_FAULTS
            uint32_t                   reg_warnings;                 //!< STATUS.REG_WARNINGS

            uint32_t                   meas_v;                       //!< Voltage measurement status
            uint32_t                   meas_i;                       //!< Current measurement status
            uint32_t                   meas_b;                       //!< Field measurement status
        } status;

        struct Dpcls_dsp_dsp
        {
            uint32_t                   ref;                          //!< REF state
            uint32_t                   direct;                       //!< STATE.DIRECT
        } state;

        struct Dpcls_dsp_meas
        {
            float                      v;                            //!< Measured load voltage
            float                      v_capa;                       //!< Measured capacitor voltage
            float                      i;                            //!< Measured current
            float                      ia;                           //!< Measured current from DCCT A (unfiltered)
            float                      ib;                           //!< Measured current from DCCT B (unfiltered)
            float                      i_capa;                       //!< Measured capacitor current
            int32_t                    i_diff_ma;                    //!< Measurement difference
            uint32_t                   i_access_f;                   //!< Iaccess flag (|Imeas| > Iaccess)
            uint32_t                   i_min_f;                      //!< Imin flag    (|Imeas| < Imin)
            uint32_t                   i_low_f;                      //!< Ilow flag    (|Imeas| < 0.1 * Ipos)
            uint32_t                   i_zero_f;                     //!< Izero flag   (|Imeas| < 0.0001 * Ipos)
            float                      i_rate;                       //!< Measured current rate for feedback
            float                      b;                            //!< Measured field
            float                      b_rate;                       //!< Measured field rate for feedback
            uint32_t                   pub_acq1_cyc_sel;             //!< Cycle selector of the acquisition measurement to be published
            uint32_t                   pub_acq2_cyc_sel;             //!< Cycle selector of the second acquisition measurement to be published
            uint32_t                   oasis_subsample_i_reg;        //!< Subsampling factor if using I_REG log for the OASIS data
            uint32_t                   oasis_subsample_b_reg;        //!< Subsampling factor if using B_REG log for the OASIS data
        } meas;

        struct Dpcls_dsp_ref
        {
            uint32_t                   reg_mode;                     //!< Actual regulation mode
            uint32_t                   actuation;                    //!< Actual actuation type
            uint32_t                   func_type;                    //!< Actual reference function
            float                      b;                            //!< Field   reference (G)
            float                      i;                            //!< Current reference (A)
            float                      v;                            //!< Voltage reference (V)
            float                      peak;                         //!< Reference function peak value
            uint32_t                   fg_status_pre_func;           //!< Copy of libref FG_STATE variable
            uint32_t                   fg_pars_group[REF_FG_PAR_MAX_PER_GROUP]; // Group of libref parameters to be recoverd/stored to NVS
        } ref;

        struct Dpcls_dsp_reg
        {
            float                      err;                          //!< [1ms]   B/I loop error (G or A)
            int32_t                    i_err_ma;                     //!< [20ms]  I loop error (mA)
        } reg;

        struct Dpcls_dsp_interlock
        {
            uint32_t                    ready;                       //!< When true, the interlock values have been latched
            uint32_t                    cyc_sel;                     //!< Cycle selector of the interlock latched values
        } interlock;

    } dsp;
};



// ---------- External variable declarations

extern volatile struct Dpcls dpcls;



static inline bool vsIsBlockable(void)
{
    return (dpcls.mcu.vs.blockable == FGC_CTRL_ENABLED);
}



static inline enum REG_mode getRegMode(void)
{
    return (enum REG_mode)dpcls.dsp.ref.reg_mode;
}


// EOF
