//! @file  dpcls_65.h
//! @brief Class specific data structres shared between the MCU and the DSP
//!
//! The FPGA implements a conversion between big endian / little endian but
//! only handles correctly 32 bit data (addresses starting on 32bits boundary).
//! All fields must be 32 bit long.
//!
//! The TMS320C6727 compiler pads the structure to be multiple of the biggest
//! type inside, in this case uint32_t. But it can also access in 8, 16 or 32
//! bits. The structures needs to be packed to fit the access of registers in
//! the DPRAM (dual port RAM) implemented in the FPGA.

#pragma once


// ---------- Includes

#include <stdint.h>

#include <defconst.h>

#ifdef __TMS320C6727__

#include "fgc3/inc/time_fgc.h"

#endif

#ifdef __RX610__

#include <time_fgc.h>

#endif



// ---------- Constants

//! Structure containing class-specific parameters shared by the MCU and the DSP.

struct Dpcls
{
    struct Dpcls_oasis
    {
        uint32_t                      subsampling;                   //!< LOG.OASIS.SUBSAMPLE
    } oasis;

    //! MCU to DSP communication

    struct Dpcls_mcu_t
    {
        struct Dpcls_mcu_ref
        {
            struct Dpcls_mcu_ref_rt
            {
                float                  value;                        //!< Real-time value: not used
            } rt;
        } ref;

        struct Dpcls_mcu_vs
        {
            uint32_t                   blockable;                    //!< VS.BLOCKABLE
        } vs;

        struct
        {
            // Recall that new DIM data (e.g. EPIC diagnostics) is received by the MCU once every 20 ms.
            // During this interval, device_index is cycled through the 8 EPICs, so that the effort
            // that the DSP has with the logging is spread over time.

            int32_t                 device_index;
            struct CC_ms_time       time_stamp;                   //!< Timestamp of the DIM cycle start
            uint32_t                epic_en_wn[FGC_MAX_EPICS];
            uint32_t                epic_res_w1[FGC_MAX_EPICS];
            uint32_t                epic_res_w2[FGC_MAX_EPICS];
        } log_data;

        struct
        {
            // 1 bit per logical EPIC, LSB first (use setBitmap/clearBitmap/testBitmap)
            uint32_t                 configured_devices_mask;
        } epic;
    } mcu;
};



// ---------- External variable declarations

extern volatile struct Dpcls dpcls;


// EOF
