//! @file  dpcls_62.h
//! @brief Class specific data structres shared between the MCU and the DSP
//!
//! The FPGA implements a conversion between big endian / little endian but
//! only handles correctly 32 bit data (addresses starting on 32bits boundary).
//! All fields must be 32 bit long.
//!
//! The TMS320C6727 compiler pads the structure to be multiple of the biggest
//! type inside, in this case uint32_t. But it can also access in 8, 16 or 32
//! bits. The structures needs to be packed to fit the access of registers in
//! the DPRAM (dual port RAM) implemented in the FPGA.

#pragma once


// ---------- Includes

#include <stdint.h>

#include <libpolswitch.h>

#include <defconst.h>

#include <interFgcMgr.h>

#ifdef __TMS320C6727__

#include "fgc3/inc/memmap.h"
#include "fgc3/inc/sm_lock.h"
#include "fgc3/inc/time_fgc.h"

#endif

#ifdef __RX610__

#include <memmap.h>
#include <sm_lock.h>
#include <time_fgc.h>

#endif



// ---------- Constants

//! Structure containing class-specific parameters shared by the MCU and the DSP.

struct Dpcls
{
    //! Log OASIS data.

    struct Dpcls_oasis
    {
        uint32_t                       fast_interval_ns;              //!< LOG.OASIS.{I,V}_MEAS_FAST.INTERVAL_NS
        uint32_t                       cycle_tag_next;                //!< Next cycle tag
        uint32_t                       cycle_tag[FGC_MAX_USER_PLUS_1]; //!< LOG.OASIS.*.CYCLE_TAG
        struct CC_us_time              timestamp;                     // Timestamp for the OASIS data
        struct CC_us_time              timestamp_fast;                // Timestamp for the OASIS data with fast measurements
        uint32_t                       subsampling;                   //!< LOG.OASIS.SUBSAMPLE
    } oasis;

    struct Inter_fgc_mgr               inter_fgc_mgr;
    struct POLSWITCH_mgr               polswitch_mgr;                 //!< Polarity swtich manager

    //! MCU to DSP communication

    struct Dpcls_mcu_t
    {
        struct Dpcls_mcu_pulse
        {
            struct Dpcls_mcu_pulse_ref
            {
                float                  user;                         //!< User reference
                float                  act;                          //!< Actuation reference: user + ILC
                float                  duration;                     //!< Duration
                uint32_t               cyc_sel;                      //!< Cycle selector
                uint32_t               sub_sel;                      //!< Sub-device selector
                struct CC_us_time      time_us;                      //!< Time the pulse has to be generated
                uint32_t               ready;                        //!< True when a new reference is ready
            } ref;

            struct Dpcls_mcu_pulse_acquisition
            {
                struct CC_us_time      time_us;                      //!< Time B2 was detected
                uint32_t               ready;                        //!< True when a new acquisition is ready
            } acquisition;

            struct Dpcls_mcu_pulse_fast_acquisition
            {
                struct CC_us_time      start;                        //!< Start of the fast acquisition logging
                struct CC_us_time      end;                          //!< End of the fast acquisition logging
                uint32_t               ready;                        //!< True when a new fast acquisition window has been set
            } fast_acquisition;

            float                      nominal;                      //!< Nominal limit
            uint32_t                   status;                       //!< Pulse status
            uint32_t                   reg_mode;                     //!< Regulation mode
       } pulse;

       struct Dpcls_mcu_log
       {
           float                       ref_user;                     //!< ILC_CYC log data
           float                       ref_ilc;                      //!< ILC_CYC log data
           float                       ref;                          //!< ILC_CYC log data
           float                       meas;                         //!< ILC_CYC log data
           float                       error;                        //!< ILC_CYC log data
           float                       error_pct;                    //!< ILC_CYC log data
           float                       state;                        //!< ILC_CYC log data
           int32_t                     cyc_sel_acq;                  //!< ILC_CYC log data
           int32_t                     cyc_sel_log;                  //!< ILC_CYC log data
       } log;

        struct Dpcls_mcu_vs
        {
            uint32_t                   blockable;                    //!< VS.BLOCKABLE
        } vs;

    } mcu;

    //! DSP to MCU communication

    struct Dpcls_dsp
    {
        uint32_t                       dcct_select;                  //!< Copy of DCCT.SELECT stored in CCLIBS

        struct Dpcls_dsp_status_t
        {
            uint32_t                   meas_v;                       //!< Voltage measurement status
            uint32_t                   meas_i;                       //!< Current measurement status
         } status;

        struct Dpcls_mcu_dsp_t
        {
            uint32_t                   ref;                          //!< REF state:   not used
        } state;

        struct Dpcls_dsp_meas
        {
            float                      v_load;                       //!< Voltage acrosss the load
            float                      i_load;                       //!< Current acrosss the load

            float                      v_capa;                       //!< Voltage acrosss the capacitor
            float                      i_capa;                       //!< Current acrosss the capacitor

            uint32_t                   pub_acq_cyc_sel;              //!< Cycle selector of the acquisition measurement to be published
            uint32_t                   acq_ready;                    //!< True if an aquisition was triggered
            uint32_t                   fast_meas_ready;              //!< True if teh fast aquisition data is ready
        } meas;

        struct Dpcls_dsp_interlock
        {
            uint32_t                    ready;                       //!< When true, the interlock values have been latched
            uint32_t                    cyc_sel;                     //!< Cycle selector of the interlock latched values
        } interlock;

    } dsp;
};



// ---------- External variable declarations

extern volatile struct Dpcls dpcls;



static inline bool vsIsBlockable(void)
{
    return (dpcls.mcu.vs.blockable == FGC_CTRL_ENABLED);
}



static inline enum REG_mode getRegMode(void)
{
    return REG_NONE;
}


// EOF
