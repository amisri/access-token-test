//! @file pc_state.h
//! @brief Functions returning the state of PC state machine

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#if defined(__RX610__)

   #include <fbs_class.h>
   #include <dpcom.h>

   #define PC_STATE_VALUE    STATE_PC

#endif

#if defined(__TMS320C6727__)

   #include "fgc3/inc/dpcom.h"

   #define PC_STATE_VALUE    dpcom.mcu.pc.state

#endif



// ---------- External structures, unions and enumerations

//! State PC bit masks - these can be used to quickly check if a state is part of
//! a group of states.

enum Pc_state_bit_mask
{
    FGC_STATE_FLT_OFF_BIT_MASK       = (1 << FGC_PC_FLT_OFF      ),
    FGC_STATE_OFF_BIT_MASK           = (1 << FGC_PC_OFF          ),
    FGC_STATE_FLT_STOPPING_BIT_MASK  = (1 << FGC_PC_FLT_STOPPING ),
    FGC_STATE_STOPPING_BIT_MASK      = (1 << FGC_PC_STOPPING     ),
    FGC_STATE_STARTING_BIT_MASK      = (1 << FGC_PC_STARTING     ),
    FGC_STATE_SLOW_ABORT_BIT_MASK    = (1 << FGC_PC_SLOW_ABORT   ),
    FGC_STATE_TO_STANDBY_BIT_MASK    = (1 << FGC_PC_TO_STANDBY   ),
    FGC_STATE_ON_STANDBY_BIT_MASK    = (1 << FGC_PC_ON_STANDBY   ),
    FGC_STATE_IDLE_BIT_MASK          = (1 << FGC_PC_IDLE         ),
    FGC_STATE_TO_CYCLING_BIT_MASK    = (1 << FGC_PC_TO_CYCLING   ),
    FGC_STATE_ARMED_BIT_MASK         = (1 << FGC_PC_ARMED        ),
    FGC_STATE_RUNNING_BIT_MASK       = (1 << FGC_PC_RUNNING      ),
    FGC_STATE_ABORTING_BIT_MASK      = (1 << FGC_PC_ABORTING     ),
    FGC_STATE_CYCLING_BIT_MASK       = (1 << FGC_PC_CYCLING      ),
    FGC_STATE_POL_SWITCHING_BIT_MASK = (1 << FGC_PC_POL_SWITCHING),
    FGC_STATE_BLOCKING_BIT_MASK      = (1 << FGC_PC_BLOCKING     ),
    FGC_STATE_ECONOMY_BIT_MASK       = (1 << FGC_PC_ECONOMY      ),
    FGC_STATE_DIRECT_BIT_MASK        = (1 << FGC_PC_DIRECT       ),
    FGC_STATE_PAUSED_BIT_MASK        = (1 << FGC_PC_PAUSED       ),

    // Below are test helper bitmasks
    // EQ = EQUAL
    // LT = LESS          THAN
    // LE = LESS EQUAL    THAN
    // GT = GREATER       THAN
    // GE = GREATER EQUAL THAN

    FGC_STATE_EQ_OFF_BIT_MASK      = ( FGC_STATE_FLT_OFF_BIT_MASK
                                     | FGC_STATE_OFF_BIT_MASK),

    FGC_STATE_LE_STOPPING_BIT_MASK = ( FGC_STATE_FLT_OFF_BIT_MASK
                                     | FGC_STATE_OFF_BIT_MASK
                                     | FGC_STATE_FLT_STOPPING_BIT_MASK
                                     | FGC_STATE_STOPPING_BIT_MASK),

    FGC_STATE_LT_BLOCKING_BIT_MASK = ( FGC_STATE_FLT_OFF_BIT_MASK
                                     | FGC_STATE_OFF_BIT_MASK
                                     | FGC_STATE_FLT_STOPPING_BIT_MASK
                                     | FGC_STATE_STOPPING_BIT_MASK
                                     | FGC_STATE_STARTING_BIT_MASK),

    FGC_STATE_LE_BLOCKING_BIT_MASK = ( FGC_STATE_LT_BLOCKING_BIT_MASK
                                     | FGC_STATE_BLOCKING_BIT_MASK),

    FGC_STATE_GE_BLOCKING_BIT_MASK = ~FGC_STATE_LT_BLOCKING_BIT_MASK,

    FGC_STATE_GT_BLOCKING_BIT_MASK = ~FGC_STATE_LE_BLOCKING_BIT_MASK,

    FGC_STATE_VALID_RESET_BIT_MASK = ( FGC_STATE_OFF_BIT_MASK
                                     | FGC_STATE_FLT_OFF_BIT_MASK
                                     | FGC_STATE_FLT_STOPPING_BIT_MASK),

    FGC_STATE_VALID_CALIBRATION_BIT_MASK = ( FGC_STATE_OFF_BIT_MASK
                                           | FGC_STATE_FLT_OFF_BIT_MASK
                                           | FGC_STATE_STARTING_BIT_MASK),
};



// ---------- External function definitions

// Only the MCU can modify the state

#if defined(__RX610__)
static inline void pcStateSet(uint32_t const state)
{
    PC_STATE_VALUE = state;

    // Notify the DSP of the new state

    dpcom.mcu.pc.state         = state;
    dpcom.mcu.pc.state_bitmask = 1 << state;
}
#endif



static inline uint32_t pcStateGet(void)
{
    return PC_STATE_VALUE;
}



static inline uint32_t pcStateTest(uint32_t const bitmask)
{
    return ((dpcom.mcu.pc.state_bitmask & bitmask) != 0);
}



static inline uint32_t pcStateTestState(uint32_t const state,
                                        uint32_t const bitmask)
{
    return ((state & bitmask) != 0);
}


// EOF
