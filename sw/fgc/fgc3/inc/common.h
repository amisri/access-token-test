//! @file  common.h
//! @brief Constants shared between MCU and DSP

#pragma once



// ---------- Constants

//! Command packet types

#define PKT_FLOAT               1
#define PKT_INT                 2
#define PKT_INTU                4

//! No float token (NaN)

#define PAR_NO_FLOAT            0xFFFFFFFF

//! Long words

#define DB_DSP_MEM_LEN          32

//! Runcode use to identify that a DSP panic occurred.

#define RUNCODE_DSP_PANIC        0xDEAD

// DSP loader constants

//! MCU handshake request value

#define  DSP_LOADER_MCU_REQUEST      0xAAAAAAAA

//! DSP handshake acknowdlge value

#define  DSP_LOADER_DSP_ACK          0x55555555

//! DSP running

#define  DSP_LOADER_RUNNING          0x600D0D57



// ---------- External function definitions

#define MIN(a,b)        (((a) < (b)) ? (a) : (b))
#define MAX(a,b)        (((a) > (b)) ? (a) : (b))


// EOF
