//! @file   memTest.c
//! @brief  Functions for memory testing


// ---------- Includes

#include <stdlib.h>

#include <memTest.h>



// ---------- External function definitions

uint32_t memTestDataBus(volatile uint32_t * base_address, uint32_t * data_backup)
{
    uint32_t pattern;


    // Perform a walking 1's test at the given address.

    for (pattern = 1; pattern != 0; pattern <<= 1)
    {
        // Write the test pattern.

        data_backup[0] = *base_address;
        *base_address  = pattern;


        // Read it back (immediately is okay for this test).

        if (*base_address != pattern)
        {
            return (pattern);
        }

        *base_address = data_backup[0];
    }

    return (0);
}



uint32_t * memTestAddressBus(volatile uint32_t * base_address, uint32_t n_bytes, uint32_t * data_backup)
{
    uint32_t address_mask = (n_bytes/sizeof(uint32_t) - 1);
    uint32_t offset;
    uint32_t test_offset;

    uint32_t pattern     = (uint32_t) 0xAAAAAAAA;
    uint32_t antipattern = (uint32_t) 0x55555555;

    uint32_t index;
    uint32_t index2;
    uint32_t max_index;


    // Determine number of addresses used for address bus testing

    for (offset = 1, max_index = 1; (offset & address_mask) != 0; offset <<= 1, max_index++)
    {
        ;
    }


    // Write the default pattern at each of the power-of-two offsets.

    for (offset = 0, index = 0; index < max_index; index++)
    {
        data_backup[index]   = base_address[offset];
        base_address[offset] = pattern;

        offset = 1 << index;
    }


    // Check for address bits stuck or shorted.

    for (test_offset = 0, index = 0; index < max_index; index++)
    {
        base_address[test_offset] = antipattern;

        for (offset = 0, index2 = 0; index2 < max_index; index2++)
        {
            if ((base_address[offset] != pattern) && (offset != test_offset))
            {
                return ((uint32_t *) &base_address[test_offset]);
            }

            offset = 1 << index2;
        }

        base_address[test_offset] = pattern;

        test_offset = 1 << index;
    }


    // Restore original data

    for (offset = 0, index = 0; index < max_index; index++)
    {
        base_address[offset] = data_backup[index];

        offset = 1 << index;
    }


    return (NULL);
}



uint32_t * memTestDevice(volatile uint32_t * base_address, uint32_t n_bytes, uint32_t * data_backup)
{
    uint32_t offset;
    uint32_t n_words = n_bytes / sizeof(uint32_t);

    uint32_t pattern;
    uint32_t antipattern;


    // Fill memory with a known pattern.

    for (pattern = 1, offset = 0; offset < n_words; pattern++, offset++)
    {
        data_backup[offset]  = base_address[offset];
        base_address[offset] = pattern;
    }


    // Check each location and invert it for the second pass.

    for (pattern = 1, offset = 0; offset < n_words; pattern++, offset++)
    {
        if (base_address[offset] != pattern)
        {
            return ((uint32_t *) &base_address[offset]);
        }

        antipattern          = ~pattern;
        base_address[offset] = antipattern;
    }


    // Check each location for the inverted pattern and zero it.

    for (pattern = 1, offset = 0; offset < n_words; pattern++, offset++)
    {
        antipattern = ~pattern;

        if (base_address[offset] != antipattern)
        {
            return ((uint32_t *) &base_address[offset]);
        }

        // Restore original data

        base_address[offset] = data_backup[offset];
    }

    return (NULL);
}


// EOF
