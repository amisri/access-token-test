//! @file  histogram.c
//! @brief File providing functionality for generic histograms.


// ---------- Includes

#include <string.h>

#if defined(__RX610__)

#include <histogram.h>
#include <time_fgc.h>
#include <msTask.h>

#elif defined(__TMS320C6727__)

#include "fgc3/inc/histogram.h"
#include "fgc3/inc/time_fgc.h"

#endif



// ---------- Constants

#define FGC_HISTOGRAM_BIN_MAX   (FGC_HISTOGRAM_NUM_BINS - 1)



// ---------- External function definitions

void histogramInit(struct Histogram * histo,
                   int16_t            bin_min,
                   uint16_t           bin_width)
{
    histo->bin_min    = bin_min;
    histo->bin_width  = bin_width;

    histogramZero(histo);
}




void histogramZero(struct Histogram * histo)
{
    memset(histo->bins, 0, sizeof(histo->bins));

    histo->min = 0;
    histo->max = 0;
}



void histogramAddValue(struct Histogram * histo,
                       int32_t            value)
{
    int16_t bin_index;

    // Calculate the bin index

    bin_index = (value - histo->bin_min) / histo->bin_width;

    if (value < histo->bin_min)
    {
        bin_index = 0;
    }
    else if (bin_index > FGC_HISTOGRAM_BIN_MAX)
    {
        bin_index = FGC_HISTOGRAM_BIN_MAX;
    }

    // Increment bin

    histo->bins[bin_index]++;

    // Update min/max values

    if (value < histo->min || histo->max == 0)
    {
        histo->min = value;
    }

    if (value > histo->max)
    {
        histo->max = value;
    }
}



#if defined(__RX610__)

void histogramComInit(struct Histogram_com * histo_com,
                      int16_t                bin_min,
                      uint16_t               bin_width)
{
    uint8_t i;

    for (i = 0; i < FGC_HISTOGRAM_COM_NUM; i++)
    {
        histogramInit(&histo_com->histos[i], bin_min, bin_width);
    }
}



void histogramComAddValue(struct Histogram_com * histo_com,
                          int32_t                value)
{
    histogramAddValue(&histo_com->histos[ms_task.ms_mod_20], value);
}



void histogramPrint(FILE * fd, char delim, struct Histogram const * histo, uint8_t num)
{
    int16_t  bin_legend = histo->bin_min;
    uint8_t  i;
    uint8_t  j;

    // Print the header from the first histogram and assume it is the same for the rest

    for (i = 0; i < FGC_HISTOGRAM_NUM_BINS; i++, bin_legend += histo->bin_width)
    {
        // Print the header

        if (i == 0)
        {
            fprintf(fd, "<%5d ", bin_legend);
        }
        else if (i == (FGC_HISTOGRAM_NUM_BINS - 1))
        {
            fprintf(fd, ">%5d ", bin_legend);
        }
        else
        {
            fprintf(fd, " %5d ", bin_legend);
        }

        for (j = 0; j < num; j++)
        {
            fprintf(fd, "%10u ", (unsigned int)histo[j].bins[i]);
        }

        fprintf(fd, "%c", delim);
    }

    // Print histogram Min values

    fprintf(fd, "   Min ");

    for (j = 0; j < num; j++)
    {
        fprintf(fd, "%10u ", (unsigned int)histo[j].min);
    }

    fprintf(fd, "%c", delim);

    // Print histogram Max values

    fprintf(fd, "   Max ");

    for (j = 0; j < num; j++)
    {
        fprintf(fd, "%10u ", (unsigned int)histo[j].max);
    }

    fprintf(fd, "%c", delim);
}


#endif


// EOF
