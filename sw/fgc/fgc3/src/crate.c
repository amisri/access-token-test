//! @file     crate.c
//! @brief    Contains information regarding the crate.


// ---------- Includes

#if defined(__RX610__)

#include <crate.h>

#endif

#if defined(__TMS320C6727__)

#include "fgc3/inc/crate.h"

#endif



// ---------- External variable definitions

struct crate crate;


// EOF
