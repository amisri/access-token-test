//! @file  time_fgc.c
//! @brief This file defines the structures and functions related to absoulte time

// ---------- Includes

#if defined(__RX610__)

#include <time_fgc.h>

#endif

#if defined(__TMS320C6727__)

#include "fgc3/inc/time_fgc.h"

#endif


// ---------- External function definitions

void timeUsAddDelay(struct CC_us_time * abs_time, uint32_t const delay_us)
{
    // Ignore a negative delay if by mistake timeUsAddDelay(time_a, -1);

    if (delay_us > 0x7FFFFFFF)
    {
        return;
    }

    abs_time->us += delay_us;

    while (abs_time->us >  999999)
    {
        abs_time->us  -= 1000000;
        abs_time->secs.abs++;
    }
}



void timeUsAddOffset(struct CC_us_time * abs_time, int32_t const offset_us)
{
    if (offset_us == 0)
    {
        return;
    }

    int32_t offset = offset_us + abs_time->us;

    if (offset >= 0)
    {
        while (offset >  999999)
        {
            offset -= 1000000;
            abs_time->secs.abs++;
        }

        abs_time->us = offset;
    }
    else
    {
        while (offset < 0)
        {
            offset += 1000000;
            abs_time->secs.abs--;
        }

        abs_time->us = offset;
    }
}



void timeUsAddLongDelay(struct CC_us_time       * abs_time,
                        int32_t          const  delay_second,
                        uint32_t         const  delay_us)
{
    abs_time->secs.abs += delay_second;
    timeUsAddDelay(abs_time, delay_us);
}



void timeMsAddDelay(struct CC_ms_time * abs_time, uint32_t const delay_ms)

{
    // Ignore a negative delay if by mistake timeUsAddDelay(time_a, -1);

    if (delay_ms > 0x7FFFFFFF)
    {
        return;
    }

    abs_time->ms += delay_ms;

    while (abs_time->ms >  999)
    {
        abs_time->ms  -= 1000;
        abs_time->secs.abs++;
    }
}



void timeMsAddOffset(struct CC_ms_time * abs_time, int32_t const offset_ms)
{
    if (offset_ms == 0)
    {
        return;
    }

    int32_t offset = offset_ms + abs_time->ms;

    if (offset >= 0)
    {
        while (offset >  999)
        {
            offset -= 1000;
            abs_time->secs.abs++;
        }

        abs_time->ms = offset;
    }
    else
    {
        while (offset < 0)
        {
            offset += 1000;
            abs_time->secs.abs--;
        }

        abs_time->ms = offset;
    }
}



void timeMsAddLongDelay(struct CC_ms_time       * abs_time,
                        int32_t           const   delay_second,
                        uint32_t          const   delay_ms)
{
    abs_time->secs.abs += delay_second;
    timeMsAddDelay(abs_time, delay_ms);
}



int32_t timeMsDiff(struct CC_ms_time const * const time1,
                   struct CC_ms_time const * const time2)
{
    int32_t diff;

    diff  = (time2->secs.abs - time1->secs.abs) * 1000;
    diff += ((int32_t)time2->ms - (int32_t)time1->ms);

    return (diff);
}



int32_t timeUsDiff(struct CC_us_time const * const time1,
                   struct CC_us_time const * const time2)
{
    int32_t diff;

    diff  = (time2->secs.abs - time1->secs.abs) * 1000000;
    diff += ((int32_t)time2->us - (int32_t)time1->us);

    return (diff);
}



bool timeUsEqual(struct CC_us_time const * time1, struct CC_us_time const * time2)
{
    return ((time1->secs.abs == time2->secs.abs) && (time1->us == time2->us));
}



bool timeUsLessThan(struct CC_us_time const * time1, struct CC_us_time const * time2)
{
    return ((time1->secs.abs < time2->secs.abs) || ((time1->secs.abs == time2->secs.abs) && (time1->us < time2->us)));
}



bool timeUsGreaterThan(struct CC_us_time const * time1, struct CC_us_time const * time2)
{
    return ((time1->secs.abs > time2->secs.abs) || ((time1->secs.abs == time2->secs.abs) && (time1->us > time2->us)));
}



bool timeUsLessThanEqual(struct CC_us_time const * time1, struct CC_us_time const * time2)
{
    return (time1->secs.abs == time2->secs.abs ? (time1->us <= time2->us) : (time1->secs.abs < time2->secs.abs));
}



bool timeUsGreaterThanEqual(struct CC_us_time const * time1, struct CC_us_time const * time2)
{
    return (time1->secs.abs == time2->secs.abs ? (time1->us >= time2->us) : (time1->secs.abs > time2->secs.abs));
}



bool timeMsEqual(struct CC_ms_time const * time1, struct CC_ms_time const * time2)
{
    return ((time1->secs.abs == time2->secs.abs) && (time1->ms == time2->ms));
}



bool timeMsLessThan(struct CC_ms_time const * time1, struct CC_ms_time const * time2)
{
    return ((time1->secs.abs < time2->secs.abs) || ((time1->secs.abs == time2->secs.abs) && (time1->ms < time2->ms)));
}



bool timeMsGreaterThan(struct CC_ms_time const * time1, struct CC_ms_time const * time2)
{
    return ((time1->secs.abs > time2->secs.abs) || ((time1->secs.abs == time2->secs.abs) && (time1->ms > time2->ms)));
}



bool timeMsLessThanEqual(struct CC_ms_time const * time1, struct CC_ms_time const * time2)
{
    return (time1->secs.abs == time2->secs.abs ? (time1->ms <= time2->ms) : (time1->secs.abs < time2->secs.abs));
}



bool timeMsGreaterThanEqual(struct CC_ms_time const * time1, struct CC_ms_time const * time2)
{
    return (time1->secs.abs == time2->secs.abs ? (time1->ms >= time2->ms) : (time1->secs.abs > time2->secs.abs));
}



void timeCopyFromMsToUs(const struct CC_ms_time * time_ms, struct CC_us_time * time_us)
{
    time_us->secs.abs = time_ms->secs.abs;
    time_us->us   = time_ms->ms * (uint32_t)1000;
}



void timeCopyFromUsToMs(const struct CC_us_time * time_us, struct CC_ms_time * time_ms)
{
    time_ms->secs.abs = time_us->secs.abs;
    time_ms->ms   = time_us->us / (uint32_t) 1000;
}


// EOF
