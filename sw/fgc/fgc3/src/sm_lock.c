//! @file  sm_lock.h
//! @brief Shared memory lock mechanism


// ---------- Includes

#if defined(__RX610__)

#include <sm_lock.h>

#endif

#if defined(__TMS320C6727__)

#include "fgc3/inc/sm_lock.h"

#endif



// ---------- External function definitions

void smLockWriteReset(volatile SM_LOCK * sm_lock)
{
    *sm_lock = 0;
}



void smLockWriteTake(volatile SM_LOCK * sm_lock)
{
    if ((*sm_lock) & 1)
    {
        // We'd better panic here...

        smLockWriteReset(sm_lock);
    }

    // Odd value means the processor is writing

    (*sm_lock)++;
}



void smLockWriteRelease(volatile SM_LOCK * sm_lock)
{
    // Even value means the processor has finished writing

    (*sm_lock)++;
}



SM_LOCK smLockReadPoll(volatile SM_LOCK * sm_lock)
{
    SM_LOCK read_val;

    // Wait until the other processor has finished writing

    while ((read_val = (*sm_lock)) & 1);

    // Return the local copy, which is guaranteed to be even

    return read_val;
}



bool smLockReadCheck(volatile SM_LOCK * sm_lock, SM_LOCK lock_val)
{
    return ((*sm_lock) == lock_val);
}


// EOF
