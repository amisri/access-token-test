//! @file   digitalIo.c
//! @brief  Manages the digital inputs and outputs of the FGC.
//!
//! These functions are used to process the digital inputs and outputs.
//! There are two properties that control the way the simulation works and three
//! different modes:
//!
//!       STATE.OP        VS.SIM_INTLKS           VSRUN/VSREADY        All other
//!   stateOpGetState()  digitalIo.simIntlks    VSPOWERON/OPBLOCKED     signals
//!    --------------------------------------------------------------------------------------
//!    1: !SIMULATION    not significant               REAL              REAL
//!    2:  SIMULATION        DISABLED               SIMULATED            REAL
//!    3:  SIMULATION        ENABLED                SIMULATED          SIMULATED


// ---------- Includes

#include <bitmap.h>
#include <crate.h>
#include <defconst.h>
#include <digitalIo.h>
#include <dpcls.h>
#include <dpcom.h>
#include <interFgc.h>
#include <iodefines.h>
#include <mcu.h>
#include <memmap_mcu.h>
#include <pc_state.h>
#include <polSwitch.h>
#include <sta.h>
#include <stateOp.h>
#include <statePc.h>
#include <status.h>

#if FGC_CLASS_ID == 65
#include <diag_class.h>
#endif



// ---------- Constatns

#define RESET_COMMAND_DURATION 10 + 1 // Reset sequence is 50ms, number of iterations + 1, the iterations are spaced 5ms



// ---------- External variable definitions

struct DigitalIo digitalIo;



// ---------  Internal function definitions  ----------------------------

static void digitalIoSupInit(void)
{
    // Bank A: output

    setBitmap(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_PERMIT_MASK16);
    setBitmap(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_CTRL_MASK16);

    // Bank B: input

    setBitmap(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_PERMIT_MASK16);
    clrBitmap(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_CTRL_MASK16);

    digitalIoSupInitClass();
}



static void digitalIoSimVS(void)
{
    // Simulate response to VS_RUN request

    if (testBitmap(digitalIo.outputs, DIG_OP_SET_VSRUNCMD_MASK16) == true)
    {
        if (testBitmap(digitalIo.inputs, DIG_IP_VSRUN_MASK32) == false)
        {
            setBitmap(digitalIo.inputs, (DIG_IP_VSRUN_MASK32 | DIG_IP_VSPOWERON_MASK32));
        }
    }
    else
    {
        if (testBitmap(digitalIo.inputs, DIG_IP_VSRUN_MASK32) == true)
        {
            clrBitmap(digitalIo.inputs, (DIG_IP_VSREADY_MASK32 | DIG_IP_VSRUN_MASK32));
        }
    }

    // Simulate starting (1s) of voltage source

#if (FGC_CLASS_ID == 63)

    if (   pcStateGet()          == FGC_PC_STARTING
        && statePcGetStateTime() > 1000)
    {
        setBitmap(digitalIo.inputs, DIG_IP_VSREADY_MASK32);
    }

#elif (FGC_CLASS_ID == 62)

    if (   pcStateGet()          == FGC_PC_TO_CYCLING
        && statePcGetStateTime() > 1000)
    {
        setBitmap(digitalIo.inputs, DIG_IP_VSREADY_MASK32);
    }

#endif

    // Simulate OPBLOCKED

    if (testBitmap(digitalIo.outputs, DIG_OP_SET_UNBLOCKCMD_MASK16) == true)
    {
        clrBitmap(digitalIo.inputs, DIG_IP_OPBLOCKED_MASK32);
    }
    else
    {
        setBitmap(digitalIo.inputs, DIG_IP_OPBLOCKED_MASK32);
    }

    // Simulate stopping (1s) of voltage source

    if (   (digitalIo.inputs & (DIG_IP_VSRUN_MASK32 | DIG_IP_VSPOWERON_MASK32)) == DIG_IP_VSPOWERON_MASK32
        && statePcGetStateTime() > 1000)
    {
        clrBitmap(digitalIo.inputs, DIG_IP_VSPOWERON_MASK32);
    }
}



static void digitalIoSimAllInputs(void)
{
    digitalIo.inputs &= (  DIG_IP_PCPERMIT_MASK32  | DIG_IP_VSRUN_MASK32     | DIG_IP_VSREADY_MASK32   | DIG_IP_VSPOWERON_MASK32
                         | DIG_IP_POLSWIPOS_MASK32 | DIG_IP_POLSWINEG_MASK32 | DIG_IP_OPBLOCKED_MASK32);

    if (statusGetFaults() != 0)
    {
        setBitmap(digitalIo.inputs, DIG_IP_PWRFAILURE_MASK32);
    }

    digitalIoSimVS();
}



static void digitalIoRealIntlks(void)
{
    // Preserve simulated VS signals

    digitalIo.inputs &= DIG_IP_VSRUN_MASK32 | DIG_IP_VSREADY_MASK32 | DIG_IP_VSPOWERON_MASK32 | DIG_IP_OPBLOCKED_MASK32;

    // Read inputs except sim VS signals

    setBitmap(digitalIo.inputs, (DIG_IP_P & ~(  DIG_IP_VSRUN_MASK32     | DIG_IP_VSREADY_MASK32 | DIG_IP_VSPOWERON_MASK32
                                              | DIG_IP_OPBLOCKED_MASK32 | DIG_IP_VSFAULT_MASK32 | DIG_IP_PWRFAILURE_MASK32)));

    digitalIoSimVS();
}



static void digitalIoRealInputs(void)
{
    // Read inputs except those use to encode PLC commands:
    // COMHV-PS: VSNOCABLE
    // SVC: Status11
    // @TODO remove this when FGC_64 for the SVC is developed
    // When done: digitalIo.inputs = digitalIoGetIpInputs;

    uint32_t ip_mask        = 0;
    uint32_t ip_direct_mask = 0;

    if (crateGetType() == FGC_CRATE_TYPE_COMHV_PS)
    {
        ip_mask        = DIG_IP_VSNOCABLE_MASK32;
        ip_direct_mask = DIG_IPDIRECT_VSNOCABLE_MASK32;
    }
 #if (FGC_CLASS_ID == 64)
    else
    {
        ip_mask        = DIG_IP_OPBLOCKED_MASK32;
        ip_direct_mask = DIG_IPDIRECT_OPBLOCKED_MASK32;
    }
#endif

    digitalIo.inputs = DIG_IP_P       & (~ip_mask);
    digitalIo.direct = DIG_IPDIRECT_P & (~ip_direct_mask);

    //  Hack to make prototype 2 work on delta converter

    if (crateGetType() == FGC_CRATE_TYPE_COMRCIAL_PC1)
    {
        // Inhibit subsequent signals, not properly mapped on interface board
        // Always OFF

        clrBitmap(digitalIo.inputs, DIG_IP_VSNOCABLE_MASK32);

        // Simulate VS_READY after VSPOWERON, this signal does not exist on this type of converter

        if (testBitmap(digitalIo.inputs, DIG_IP_VSPOWERON_MASK32) == true)
        {
            setBitmap(digitalIo.inputs, DIG_IP_VSREADY_MASK32);
        }
        else
        {
            clrBitmap(digitalIo.inputs, (DIG_IP_VSREADY_MASK32));
        }

        // Simulate OPBLOCKED, this signal does not exist on this type of converter

        if (testBitmap(DIG_OP_P, DIG_OP_SET_UNBLOCKCMD_MASK16) == true)
        {
            clrBitmap(digitalIo.inputs, DIG_IP_OPBLOCKED_MASK32);
        }
        else
        {
            setBitmap(digitalIo.inputs, DIG_IP_OPBLOCKED_MASK32);
        }
    }
}



// ---------- External function definitions

void digitalIoInit(void)
{
    digitalIoSupInit();
}



void digitalIoSetFiringMode(uint16_t const mode)
{
    // Encode the firing mode in DIG_SUP A1,A2 as follows:
    // A0  A1  MODE
    // --------------------
    // 0   0   CASSEL
    // 0   1   RECONSTITUE
    // 1   0   MIXED
    // 1   1   ASAD

    static uint16_t const DIG_MODE_MASK = 0x0003;

    // The LSB maps to the set register and the MSB maps to the reset register

    uint16_t set   = mode;
    uint16_t reset = (~mode & DIG_MODE_MASK);

    DIG_SUP_A_SET_P = (reset << 8) | set;
}



uint32_t digitalIoGetInputs(void)
{
    return digitalIo.inputs;
}



uint32_t digitalIoGetRawInputs(void)
{
    return digitalIo.direct;
}



uint16_t digitalIoGetOutputs(void)
{
    return digitalIo.outputs;
}



uint16_t digitalIoGetVsSimIntlks(void)
{
    return digitalIo.simIntlks;
}



void digitalIoEnableVsSimIntlks(void)
{
    digitalIo.simIntlks = FGC_CTRL_ENABLED;
}



void digitalIoDisableVsSimIntlks(void)
{
    digitalIo.simIntlks = FGC_CTRL_DISABLED;
}



void digitalIoSetCmdRequest(enum DigitalIo_cmd_request cmd)
{
    setBitmap(digitalIo.cmdRequest, cmd);
}



void digitalIoInputsProcess(void)
{
    static bool firstSim = true;

    // Read raw inputs

    digitalIo.direct = DIG_IPDIRECT_P;

    if (stateOpGetState() == FGC_OP_SIMULATION)
    {
        if (firstSim == true)
        {
            // Clear commands register

            digitalIo.outputs = 0;

            // Simulate PC_PERMIT

            digitalIo.inputs = DIG_IP_PCPERMIT_MASK32;

            firstSim = false;
        }

        if (digitalIo.simIntlks == FGC_CTRL_DISABLED)
        {
            digitalIoRealIntlks();
        }
        else
        {
            digitalIoSimAllInputs();
        }
    }
    else
    {
        firstSim = true;
        digitalIoRealInputs();
    }
}



void digitalIoOutputsProcess(void)
{
    static uint16_t resetCounter = 0;

    uint16_t set = 0;
    uint16_t rst = 0;
    uint16_t polSwitchCmd = 0;

    interFgcProcess(resetCounter != 0);

    // Handle VS_RUN

    // if VS_RUN is not active and ON requested

    if (   testBitmap(digitalIo.outputs, DIG_OP_SET_VSRUNCMD_MASK16) == false
        && testBitmap(digitalIo.cmdRequest, DDOP_CMD_ON)             == true)
    {
        setBitmap(set, DIG_OP_SET_VSRUNCMD_MASK16);

        // Disable CAL.ACTIVE property

        sta.cal_active = FGC_CTRL_DISABLED;
    }

    // OFF requested or faults have been detected and If VS_RUN is active

    if (   (   testBitmap(digitalIo.cmdRequest, DDOP_CMD_OFF) == true
            || statusGetFaults() != 0)
        && testBitmap(digitalIo.outputs, DIG_OP_SET_VSRUNCMD_MASK16) == true)
    {
        // Clear VS_RUN command

        setBitmap(rst, DIG_OP_SET_VSRUNCMD_MASK16);
    }

    // Handle Polarity Switch

    polSwitchCmd = polSwitchGetCommand();

    setBitmap(set, polSwitchCmd);
    setBitmap(rst, (polSwitchCmd ^ (DDOP_CMD_POL_POS | DDOP_CMD_POL_NEG)));

    // Handle FGCOKCMD and PWRFAILURE interlock

    if (statusTestFaults(~(FGC_FLT_NO_PC_PERMIT | FGC_FLT_FAST_ABORT)) == true)
    {
        if (testBitmap(digitalIo.outputs, DIG_OP_SET_FGCOKCMD_MASK16) == true)
        {
            setBitmap(rst, DIG_OP_SET_FGCOKCMD_MASK16);

            DIG_INTLK_OUT_P = DIG_INTLK_OUT_SET_PWRFAILURE_MASK16;
        }
    }
    else
    {
        if (testBitmap(digitalIo.outputs, DIG_OP_SET_FGCOKCMD_MASK16) == false)
        {
            setBitmap(set, DIG_OP_SET_FGCOKCMD_MASK16);

            DIG_INTLK_OUT_P = DIG_INTLK_OUT_RST_PWRFAILURE_MASK16;
        }
    }

    // Handle UNBLOCKCMD

    if (testBitmap(digitalIo.cmdRequest, DDOP_CMD_UNBLOCK) == true)
    {
        setBitmap(set, DIG_OP_SET_UNBLOCKCMD_MASK16);
    }

    if (testBitmap(digitalIo.cmdRequest, DDOP_CMD_BLOCKING) == true)
    {
        setBitmap(rst, DIG_OP_SET_UNBLOCKCMD_MASK16);
    }

    // Handle VSRESET

    if (resetCounter == 0)
    {
        if (testBitmap(digitalIo.cmdRequest, DDOP_CMD_RESET) == true)
        {
            resetCounter = RESET_COMMAND_DURATION;
        }
    }

    if (resetCounter != 0)
    {
        if (resetCounter == RESET_COMMAND_DURATION)
        {
            setBitmap(set, DIG_OP_SET_VSRESETCMD_MASK16);

            // Send RESET command to the DSP in case it needs to do something

            dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_RESET);
        }
        else if (resetCounter < RESET_COMMAND_DURATION)
        {
#if FGC_CLASS_ID == 65
            diagClassClrAllFaults();
#endif

            statusFaultsReset();

            // Clear DCCT faults latch

            sta.dcct_flts = 0;
        }

        resetCounter--;

        if (resetCounter == 0)
        {
            setBitmap(rst, DIG_OP_SET_VSRESETCMD_MASK16);
        }
    }

    // Send or simulate commands to digital interface

    if (stateOpGetState() == FGC_OP_SIMULATION)
    {
        // Simulate commands

        digitalIo.outputs = (digitalIo.outputs & ~rst) | set;

        if (digitalIo.simIntlks == FGC_CTRL_DISABLED)
        {
            // Block VSRUN from being generated

            clrBitmap(set, DIG_OP_SET_VSRUNCMD_MASK16);

            // Ouput the signals

            if ((set ^ rst) != 0)
            {
                DIG_OP_P = (rst << 8) | set;
            }

            // Clear commands except VSRUN and UNBLOCK

            digitalIo.outputs &= DIG_OP_SET_VSRUNCMD_MASK16 | DIG_OP_SET_UNBLOCKCMD_MASK16;

            // Readback commands except VSRUN and UNBLOCK

            setBitmap(digitalIo.outputs, (DIG_OP_P & ~(DIG_OP_SET_VSRUNCMD_MASK16 | DIG_OP_SET_UNBLOCKCMD_MASK16)));

            // If any faults occured or FAST ABORT received

            if (testBitmap(digitalIo.inputs, (DIG_IP_PWRFAILURE_MASK32 | DIG_IP_FASTABORT_MASK32)))
            {
                // Simulate Dig interface by reseting VSRUN

                clrBitmap(digitalIo.outputs, DIG_OP_SET_VSRUNCMD_MASK16);
            }
        }
    }
    else
    {
        // Ouput the signals

        if ((set ^ rst) != 0)
        {
            DIG_OP_P = (rst << 8) | set;
        }

        digitalIo.outputs = DIG_OP_P;
    }

    digitalIo.cmdRequest = 0;
}



void digitalIoEnableOutputs(void)
{
    P7.DR.BIT.B6 = 0;
}



void digitalIoDisableOutputs(void)
{
    P7.DR.BIT.B6 = 1;
}


// EOF
