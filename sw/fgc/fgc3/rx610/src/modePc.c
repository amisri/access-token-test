//! @file  modePc.c
//! @brief This file hanldes the setting of the PC mode


// ---------- Includes

#include <stdint.h>

#include <modePc.h>
#include <defprops.h>
#include <fbs.h>
#include <fgc_errs.h>
#include <pc_state.h>
#include <pub.h>
#include <sta.h>
#include <statePc.h>
#include <status.h>



// ---------- External variable definitions

struct ModePc mode_pc;



// ---------- Internal function definitions

static uint8_t modePcModeToSimplified(uint8_t const mode)
{
    // Return the current simplified mode if the switch case fails

    switch(mode)
    {
        case FGC_PC_SLOW_ABORT: // Fall through;
        case FGC_PC_OFF:        return FGC_PC_SIMPLIFIED_OFF;      break;
        case FGC_PC_BLOCKING:   return FGC_PC_SIMPLIFIED_BLOCKING; break;
        case FGC_PC_ON_STANDBY: // Fall through;
        case FGC_PC_IDLE:       // Fall through;
        case FGC_PC_CYCLING:    // Fall through;
        case FGC_PC_DIRECT:     return FGC_PC_SIMPLIFIED_ON;       break;
        default:                return mode_pc.pc_simplified;      break;
    }
}



// ---------- External function definitions

void modePcInit(void)
{
    // Initialize variables associated to properties

    mode_pc.pc            = FGC_PC_OFF;
    mode_pc.pc_simplified = FGC_PC_OFF;

    // Initialize state machine parameters shared with the DSP

    dpcom.mcu.pc.mode = FGC_PC_OFF;

#if (FGC_CLASS_ID == 62)
    mode_pc.pc_on = FGC_PC_ON_CYCLING;
#elif (FGC_CLASS_ID == 64 || FGC_CLASS_ID == 65)
    mode_pc.pc_on = FGC_PC_ON_DIRECT;
#endif

    pubPublishProperty(&PROP_MODE_PC,            PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, FALSE);
    pubPublishProperty(&PROP_MODE_PC_SIMPLIFIED, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, FALSE);
}



void modePcProcess(void)
{
    if (interFgcIsSlave() == true)
    {
        return;
    }

    // If PC permit is disabled

    if (statusIsPcPermit() == false)
    {
        // Force MODE_REF to OFF to trigger SLOW_ABORT when running

        dpcom.mcu.pc.mode = FGC_PC_OFF;
    }

    // PC_PERMIT is active

    else if (statusGetFaults() != 0)
    {
        // If there are faults, force the internal mode to OFF, but not MODE.PC.

        dpcom.mcu.pc.mode = FGC_PC_OFF;
    }

#if (FGC_CLASS_ID == 63)
    else if (   fbs.sector_access         == FGC_CTRL_ENABLED
             && dpcls.dsp.meas.i_access_f == true
             && pcStateTest(FGC_STATE_GT_BLOCKING_BIT_MASK) == true)
    {
        // Access to sector is active and the current is too high, so go to standby

        dpcom.mcu.pc.mode = FGC_PC_ON_STANDBY;
    }
    else if (pcStateGet() == FGC_PC_BLOCKING)
    {
        if (mode_pc.pc == FGC_PC_SLOW_ABORT)
        {
            dpcom.mcu.pc.mode = FGC_PC_OFF;
        }
    }
#endif
}



enum fgc_errno modePcSet(uint16_t mode)
{
    // Check that CAL.ACTIVE is not set (calibration of DCCTs on-going)

    if (   pcStateGet() == FGC_PC_OFF
        && (mode == FGC_PC_IDLE || mode == FGC_PC_ON_STANDBY)
        && dpcom.mcu.cal.active == FGC_CTRL_ENABLED)
    {
        return FGC_CAL_ACTIVE;
    }

    if (interFgcIsSlave() == true)
    {
        return FGC_BAD_STATE;
    }

    if (vs.present == FGC_CTRL_DISABLED && interFgcIsMaster() == false)
    {
        return FGC_BAD_STATE;
    }

    // Do not allow power converter to be started if a self-triggered
    // Post Mortem acquisition is in progress

    if (mode != FGC_PC_OFF && (statusTestUnlatched(FGC_UNL_POST_MORTEM | FGC_UNL_LOG_PLEASE) == true))
    {
        return FGC_PM_IN_PROGRESS;
    }

    enum fgc_errno retval = modePcClassValidateMode(mode);

    if (retval != FGC_OK_NO_RSP)
    {
        return retval;
    }

    // Check if mode has changed

    if (mode != mode_pc.pc)
    {
        mode_pc.pc = mode;

        // Update the simplified mode

        mode_pc.pc_simplified = modePcModeToSimplified(mode);

        // Inform the DSP

        dpcom.mcu.pc.mode = mode;
    }
    else
    {
        // The condition below is true when a converter trip forces the state to be OFF
        // whilst MODE.PC was till not OFF. Holding this condition prevents the converter
        // from starting unexpectedly when the faults are cleared with S VS RESET. The user
        // is then forced to do S MODE.PC {BK,SB,IL,DT,CY} to transition out of OFF.

        if (mode_pc.pc != FGC_PC_OFF && dpcom.mcu.pc.mode == FGC_PC_OFF)
        {
            dpcom.mcu.pc.mode = mode_pc.pc;
        }
    }

    // Always publish MODE.PC and MODE.PC_SIMPLIFIED

    pubPublishProperty(&PROP_MODE_PC,            PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, TRUE);
    pubPublishProperty(&PROP_MODE_PC_SIMPLIFIED, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, TRUE);

    return FGC_OK_NO_RSP;
}



enum fgc_errno modePcSetSimplified(uint16_t mode_simplified)
{
    uint8_t mode;

    switch (mode_simplified)
    {
        case FGC_PC_SIMPLIFIED_OFF:      mode = FGC_PC_OFF;          break;
        case FGC_PC_SIMPLIFIED_ON:       mode = mode_pc.pc_on;       break;
        case FGC_PC_SIMPLIFIED_BLOCKING: mode = FGC_PC_BLOCKING;     break;
        default:                         return FGC_BAD_PARAMETER;   break;
    }

    return modePcSet(mode);
}



uint16_t modePcGet(void)
{
    return mode_pc.pc;
}



uint16_t modePcGetInternal(void)
{
    return dpcom.mcu.pc.mode;
}



uint16_t modePcGetPcOn(void)
{
    return mode_pc.pc_on;
}



uint16_t modePcGetSimplified(void)
{
    return mode_pc.pc_simplified;
}



void modePcSetModeInternalFromSimplified(uint16_t simplified)
{
    uint8_t mode;

    switch (simplified)
    {
        case FGC_PC_SIMPLIFIED_ON:       mode = mode_pc.pc_on;     break;
        case FGC_PC_SIMPLIFIED_BLOCKING: mode = FGC_PC_BLOCKING;   break;
        case FGC_PC_SIMPLIFIED_OFF:      // Fall through
        default:                         mode = FGC_PC_OFF;        break;
    }

    dpcom.mcu.pc.mode = mode;
}


// EOF
