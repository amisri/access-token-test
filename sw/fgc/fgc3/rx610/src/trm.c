/*---------------------------------------------------------------------------------------------------------*\
  File:     trm.c

  Purpose:  FGC Software - TerminalInterface Functions.

  Notes:    This file contains the functions for communications via the MCU's terminal interface
        (normally used for a ANSI/VT100 type terminal) or via a remote terminal connection over
        the fieldbus.  It includes the Trm task function TrmTsk().

        The number of terminal output buffers is defined by TRM_N_BUFS.  The circular queue for passing
        pointers to the terminal output buffers has a length of TRM_N_BUFS+1 so that it can never fill
        up.  This avoids the need for code to detect if the queue is full.

        The terminal can be in one of three modes (terminal.mode): Editor or Direct.
        When in Editor mode, a edit state machine (terminal_state.edit_state) controls the interpretation
        of cursor and function keys from the ANSI/VT100 keyboard.
\*---------------------------------------------------------------------------------------------------------*/

#define TRM_GLOBALS             // define terminal global variable
#define FGC_GLOBALS             // Force global variable definition


// ---------- Includes

#include <bitmap.h>
#include <cmd.h>
#include <device.h>
#include <function.h>
#include <init.h>
#include <libterm.h>
#include <parsService.h>
#include <prop.h>
#include <serialStream.h>
#include <status.h>
#include <trm.h>



// ---------- Constants

#define TRM_PROMPT ';'


// ---------- External variable definitions
struct terminal_vars   terminal;
struct terminal_static __attribute__((section("sram"))) terminal_state;



// ---------- Internal function declarations

static  void     trmChangeMode(uint16_t);
static  void     SciDirect(char);
static  void     SciAddCmd(char const *);
static  void     SciDefaultPrefix(char *);
static  uint16_t SciAddCh(char ch);
static  void     SciSendPkt(void);
static  void     trmProcessLine(char *line, uint16_t line_len);
static  void     trmCallback(char keyboard_ch);



// ---------- Internal variable definitions

static struct TERM_vars __attribute__((section("sram"))) term_vars;

static char prev_ch;    //!< Used to detect consecutive ESC characters when switching to editor mode



// ---------- External function definitions

void trmInit(void)
{
    // Initialise FILE structures for 'tcm' stream

    tcm.f = fopen("tcm", "wb");

    if (setvbuf(tcm.f, NULL, _IONBF, 0) != 0)  // not buffered, buffer size 0
    {
        // 0   = ok
        // EOF = error
    }

    TermLibInit(&term_vars, tcm.f, trmProcessLine, trmCallback, TRM_PROMPT);

    // Initialise packet buffer index

    terminal_state.pkt_buf_idx = 1;

    // Start SCI in direct mode

    trmChangeMode(TRM_DIRECT_MODE);
}



void trmTask(void * unused)
{
    static char ch;

    // On OSTskStart() all the task are started and suspended
    // msTask -> fbsTsk -> stateTask -> PubTsk -> CmdTsk(fcm) -> CmdTsk(tcm) -> dallasTask -> rtdTsk -> TrmTsk
    // So, at this point, the interrupts and peripherals are set to work

    initProcess();

    // Task loop - process each character received

    for (;;)
    {
        prev_ch = ch;

        // Wait for character from IsrMst or msTask or fbsTsk

        ch = (char)(uintptr_t) OSMsgPend(terminal.msgq);

        // Reset editor mode timeout timer

        terminal_state.edit_mode_timer_min = 0;

        // Check character for mode change

        switch (ch)
        {
            // Direct Mode

            case '!':
            case 0x1A:  // Ctrl-Z

                if (terminal.mode != TRM_DIRECT_MODE)
                {
                    trmChangeMode(TRM_DIRECT_MODE);
                }

                break;

            // Editor Mode

            case 0x1B:  // ESC

                if (terminal.mode != TRM_EDITOR_MODE && prev_ch == 0x1B)
                {
                    // Switch to editor mode after two consecutive ESC characters

                    rtdEnable();
                    trmChangeMode(TRM_EDITOR_MODE);
                    continue;
                }

                break;

            default:
                // Do nothing
                break;
        }

        // Process new character according to mode

        switch (terminal.mode)
        {
            case TRM_DIRECT_MODE:

                SciDirect(ch);
                break;

            case TRM_EDITOR_MODE:

                TermChar(&term_vars, ch);
                serialStreamInternalFlushBuffer(term_vars.file);

                break;
        }
    }
}



void trmMsOut(void)
{
    static uint16_t   idx;
    static uint16_t   len;
    static char     * buf;
    char              ch;

    // Pass output character to IsrMst and FIP remote terminal buffer (if in use)

    if (   terminal.char_waits_for_tx == false
        && fbs.termq.is_full          == false)   // If output character buffer(s) are empty...
    {
        if (!buf)   // If no output buffer is being processed...
        {
            // if a new output buffer is waiting in Q...
            if (terminal_state.bufq.in_idx != terminal_state.bufq.out_idx)
            {
                idx = 0;                        // Reset buffer index
                buf = terminal_state.bufq_adr[terminal_state.bufq.out_idx];         // Get buffer address
                len = terminal_state.bufq_len[terminal_state.bufq.out_idx];     // and length
                terminal_state.bufq.out_idx++;

                if (terminal_state.bufq.out_idx > TRM_N_BUFS)           // Adjust buf Q out idx
                {
                    terminal_state.bufq.out_idx = 0;
                }
            }
        }

        if (buf)        // If an output buffer is being processed...
        {
            if (len)        // and still contains unsent characters...
            {
                terminal.snd_ch = ch = buf[idx++];   // put next character in SCI send buffer for IsrMst()
                terminal.char_waits_for_tx = TRUE;   // Set flag to say that byte is waiting

                if (fbs.gw_online_f)            // If FIP/ETH link is working
                {
                    fbsOutTermCh(ch);           // send character to FIP task as well
                }

                len--;                          // Decrement length of string remaining
            }

            if (!len)                           // If buffer is now empty...
            {
                OSMemPost(terminal.mbuf, buf);       // Return buffer to partition
                buf = 0;                // Clear buffer pointer
            }
        }
    }

    // Editor mode timeout (1 hour)

    if (terminal.mode == TRM_EDITOR_MODE)
    {
        terminal_state.edit_mode_timer_ms++;

        // If millisecond timer has reached 1 minute

        if (terminal_state.edit_mode_timer_ms >= 60000)
        {
            terminal_state.edit_mode_timer_min++;

            // If minute timer has reach timeout

            if (terminal_state.edit_mode_timer_min >= TRM_EDITOR_MODE_TIMEOUT_MIN)
            {
                trmChangeMode(TRM_DIRECT_MODE);
            }

            // Reset millisecond timer

            terminal_state.edit_mode_timer_ms = 0;
        }
    }
}



void trmSendCommand(char const * command)
{
    char const * c = command;

    while (*c)
    {
        // Post characters to the terminal queue
        // Character value SHOULD be saved directly in the pointer
        // Two casts suppress compiler warnings

        OSMsgPost(terminal.msgq, (void *)(intptr_t) *c++);
    }
}


// ---------- Internal function definitions

// This function will set the SCI mode according to the parameter.  The mode can be Editor or Direct.

static void trmChangeMode(uint16_t sci_mode)
{
    terminal.mode = TRM_DIRECT_MODE;

    // Cancel any command in progress

    SciDirect('!');

    // Reset command state

    terminal_state.cmd_state = 0;

    // Reset previous character to avoid dangling ESC

    prev_ch = 0;

    switch (sci_mode)
    {
        case TRM_DIRECT_MODE:

            rtdDisable();

            // Clear SCI command receiving state

            tcm.errnum = 0;
            break;

        case TRM_EDITOR_MODE:

            // Reset terminal

            TermInit(&term_vars, 80);

            if (rtdIsEnabled() == true)
            {
                rtdReset(tcm.f);
            }

            // Write heading

            fprintf(tcm.f,
                    "PLD VERSION: %lu (%04x)\n\r"    // PLD version
                    "CODE (BOOT): %lu\n\r"           // Boot code version
                    "CODE (MP):   %lu\n\r"           // Main programs version
                    "NAME:        %s\n\r"            // Device name
                    "SYSTEM:      %s\n\r",           // System label
                    shared_mem.codes_version[SHORT_LIST_CODE_PLD],
                    MID_PLDVER_P,
                    shared_mem.codes_version[SHORT_LIST_CODE_BT],
                    shared_mem.codes_version[SHORT_LIST_CODE_MP_DSP],
                    deviceGetName(),
                    SysDbLabel(device.sys.sys_idx));

            if (fbsIsStandalone() == false)
            {
                // Write gateway name and Fieldbus address

                fprintf(tcm.f,
                        "FBS_ID:      %s:%u (0x%02x)\n\r",
                        (char *)fbs.host_name,
                        fbs.id,
                        fbs.pld_ver);
            }

            fprintf(tcm.f, "%c\v", TRM_PROMPT);

            break;
    }

    terminal.mode = sci_mode;
}



// This function is called when in Direct or Editor mode to prepare command packets for parsing by the TcmTsk.
// A command must have the syntax: !S PROPERTY VALUES\n or !G PROPERTY GETOPTIONS\n.  All characters before
// the ! are ignored.  If ! is repeated, all characters in the current packet are discarded.  Commands may
// also be terminated with TRM_PROMPT or \r.  The $, { and } characters are ignored because they are delimiter
// characters.

static void SciDirect(char ch)
{
    // Process new command character

    switch (ch)
    {
        // Server protocol delimiters

        case '{':   // Fall through
        case '}':   // Fall through
        case '$':
            // Ignore
            return;

        case ' ':
            // Set space flag

            terminal_state.cmd_space_f = true;
            return;

        case '!':   // New command

            // Set packet header to get command by default

            terminal_state.pkt_header_flags = FGC_FIELDBUS_FLAGS_GET_CMD;

            if (terminal_state.cmd_state == TRM_CMD_RECEIVING)
            {
                terminal_state.cmd_state = TRM_CMD_START;

                // Report new command started

                tcm.stat = FGC_CMD_RESTARTED;
                break;
            }
            else
            {
                terminal_state.cmd_state = TRM_CMD_START;

                // Silently start new command

                return;
            }

        case '\n':  // newline
        case '\r':  // carriage return

            // Replace with execute character TRM_PROMPT and fall through

            ch = TRM_PROMPT;

        default:    // All other characters

            // Ignore all control characters

            if (ch < 0x20)
            {
                return;
            }

            // Convert lower case to UPPER CASE

            if (ch >= 'a' && ch <= 'z')
            {
                clrBitmap(ch, 0x20);
            }

            // Switch on direct reception state machine

            switch (terminal_state.cmd_state)
            {
                default:    // Not receiving command

                    // Ignore character
                    return;

                case TRM_CMD_START:    // ! received - wait for S or G

                    // Switch on character

                    switch (ch)
                    {
                        case 'S':   // 'S' : Set comamnd

                            // Prepare pkt header and fall through

                            terminal_state.pkt_header_flags = FGC_FIELDBUS_FLAGS_SET_CMD;

                        case 'G':   // 'G' : Get comamnd

                            // Advance state

                            terminal_state.cmd_state = TRM_CMD_DEFINED;

                            // Clear space flag

                            terminal_state.cmd_space_f = false;
                            return;

                        default:    // All other characters

                            // Report unknown command

                            tcm.stat = FGC_UNKNOWN_CMD;
                            break;
                    }

                    break;

                case TRM_CMD_DEFINED:   // !S or !G received - next char must be a space

                    if (terminal_state.cmd_space_f == false)
                    {
                        tcm.stat = FGC_UNKNOWN_CMD;
                        break;
                    }

                    // If first char of property is not valid

                    if (   (ch < 'A' || ch > 'Z')
                        && (ch < '0' || ch > '9'))
                    {
                        // Report unknown symbol

                        tcm.stat = FGC_UNKNOWN_SYM;
                        break;
                    }

                    terminal_state.pkt_in_idx  = 0;
                    terminal_state.cmd_space_f = false;

                    // Advance state machine

                    terminal_state.cmd_state   = TRM_CMD_RECEIVING;

                    // Set first packet bit in header

                    setBitmap(terminal_state.pkt_header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT);

                    // Write cmd header

                    fputc((terminal.mode == TRM_DIRECT_MODE ? '$' : '\n') , tcm.f);

                case TRM_CMD_RECEIVING:    // Command being received

                    // End of command

                    if (ch == TRM_PROMPT)
                    {
                        // If at least one char in pkt

                        if (terminal_state.pkt_in_idx != 0)
                        {
                            // Clear SCI Abort flag

                            tcm.abort_f = false;

                            // Set last pkt bit in header

                            setBitmap(terminal_state.pkt_header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT);

                            // Send last packet to TcmTsk

                            SciSendPkt();

                            // Wait for cmd to finish

                            while (terminal_state.pkt->header_flags != 0)
                            {
                                OSTskSuspend();
                            }

                            // Get errnum from TcmTsk

                            tcm.stat = tcm.errnum;
                        }
                        else
                        {
                            // report NO_SYMBOL error

                            tcm.stat = FGC_NO_SYMBOL;
                        }

                        // break to report error and end cmd

                        break;
                    }

                    if (terminal_state.cmd_space_f == true)
                    {
                        // Add space to packet

                        tcm.stat = SciAddCh(' ');

                        if (tcm.stat)
                        {
                            // Report error

                            break;
                        }

                        terminal_state.cmd_space_f = false;
                    }

                    // Add character to packet

                    tcm.stat = SciAddCh(ch);

                    if (tcm.stat)
                    {
                        // Report error

                        break;
                    }

                    return;
            }

            break;
    }

    // Report errors if reported

    if (tcm.stat)
    {
        fprintf(tcm.f,
                (terminal.mode == TRM_DIRECT_MODE ? "$%u %s\n!\v" : "\n\a!%u %s"),
                tcm.stat, fgc_errmsg[tcm.stat]);
        tcm.errnum = 0;
    }
    else
    {
        if (terminal.mode == TRM_DIRECT_MODE)
        {
            // Add trailing '\n;'

            fprintf(tcm.f, "\n%c\v", TRM_PROMPT);
        }
    }

    // Reset direct mode state machine

    terminal_state.cmd_state = 0;
}



// This function is called from SciNewline() and SciDefaultPrefix() to add a text string to the command
// packet buffer.

static void SciAddCmd(char const * cp)
{
    char    ch;

    while ((ch = *(cp++)))              // loop while characters remain
    {
        SciDirect(ch);          // Submit character to command packet
    }
}



// This function helps to make the user interface easier to use when the editor is running.  It provides
// the following support:
//
//   1.  If the command buffer starts with a number:
//   1.1 If class FGC_63 in IDLE: the function adds the prefix "S REF NOW,".
//   1.2 If class FGC_63 in DIRECT: the function adds the prefix "S REF.{B,C,V}CV.VALUE ".
//   1.3 If class FGC_62 in CYCLING: the function adds the prefix "S REF.PULSE.REF_LOCAL ".
//   1.4 If class FGC_64 in DIRECT: the function adds the prefix "S REF.{Q,A,V}CV.VALUE ".
//
//   2.  Default get command - if the command doesn't start "S " or "s " or "G " or "g ", the
//   function adds the prefix "G " in the command buffer.

static void SciDefaultPrefix(char * cp)
{
    char ch1;
    char ch2;

    // Get first two characters of command

    ch1 = cp[0];
    ch2 = cp[1];

    // If command starts with a number

    if ( ch1 == '+' || ch1 == '-' || ch1 == '.' || (ch1 >= '0' && ch1 <= '9'))
    {
        SciAddCmd(functionGetDefaultPrefix());
        return;
    }

    // Default G command

    if (   (ch1 != 'S' && ch1 != 's' && ch1 != 'G' && ch1 != 'g')
        || (ch2 != 0   && ch2 != ' ')
       )
    {
        SciAddCmd("!G ");
        return;
    }

    // Start command

    SciDirect('!');
    return;
}



// This function is called from SciDirect() when a character must be added to the current packet.  If the
// character results in the packet overflowing, the packet will be submitted to the TcmTsk.  The character
// is the first of the packet it checks that the packet buffer is available.

static uint16_t SciAddCh(char ch)
{
    if (tcm.errnum)                                     // If Scm has reported a parsing error
    {
        return (tcm.errnum);                                    // Report the error to stop the command
    }

    if (terminal_state.pkt_in_idx == 0)                             // If starting a new packet
    {
    CheckPktBuf:

        terminal_state.pkt = &tcm_pars.pkt[terminal_state.pkt_buf_idx];   // Get address of new packet

        if (terminal_state.pkt->header_flags)                           // If packet buffer is not free
        {
            return (FGC_PKT_BUF_NOT_AVL);                               // report BUF NOT AVL error
        }
    }
    else if (terminal_state.pkt_in_idx >= PARS_MAX_CMD_PKT_LEN)      // else if packet is full
    {
        if (terminal_state.pkt_header_flags == FGC_FIELDBUS_FLAGS_GET_CMD) // If get command
        {
            return (FGC_CMD_BUF_FULL);                                  // Report error (1 pkt only for get)
        }

        SciSendPkt();                                           // Send the packet to TcmTsk

        goto CheckPktBuf;                                       // Jump back to check if new packet is free
    }

    terminal_state.pkt->buf[terminal_state.pkt_in_idx++] = ch;          // Save character in buffer
    return (0);
}



// This function is called from SciDirect()and SciAddCh() when a packet is ready to be sent to the
// TcmTsk.  A semaphore is used to inform the TcmTsk that the packet is waiting.

static void SciSendPkt(void)
{
    terminal_state.pkt->header_flags = terminal_state.pkt_header_flags;     // Prepare new packet header
    terminal_state.pkt->n_chars      = terminal_state.pkt_in_idx;

    OSSemPost(tcm.sem);                     // Trigger command task to process new packet

    clrBitmap(terminal_state.pkt_header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT); // Clear first pkt bit in header

    // Move to the next packet buffer

    if (++terminal_state.pkt_buf_idx >= PARS_MAX_CMD_PKTS)
    {
        terminal_state.pkt_buf_idx = 0;
    }

    terminal_state.pkt_in_idx   = 0;
}



static void trmProcessLine(char *line, uint16_t line_len)
{
    if (line_len > 0)
    {
        // Check for default commands

        SciDefaultPrefix(line);

        // Add command line to packet

        SciAddCmd(line);

        // Execute command (blocks until execution completed)

        SciDirect(TRM_PROMPT);
    }
}



static void trmCallback(char keyboard_ch)
{
    switch(keyboard_ch)
    {
        case 0x0C: // [CTRL-L] Reset FGC.ST_LATCHED
            statusLatchedReset();
            break;

        case 0x14: // [CTRL-T] Terminal only (no RTD)
            rtdDisable();
            trmChangeMode(TRM_EDITOR_MODE);
            break;

        case 0x1B: // [ESC]    Restart editor
            rtdEnable();
            trmChangeMode(TRM_EDITOR_MODE);
            break;

        default:
            // Do nothing
            break;
    }
}


// EOF
