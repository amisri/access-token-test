//! @file  polSwitch.c
//! @brief Class specific handling of the polarity switch


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <polSwitch.h>
#include <defprops.h>
#include <defconst.h>
#include <digitalIo.h>
#include <memmap_mcu.h>
#include <dpcls.h>
#include <sta.h>
#include <pub.h>
#include <crate.h>
#include <pc_state.h>
#include <stateOp.h>
#include <status.h>
#include <bitmap.h>



// ---------- Constants ------------------------------------------------

//! Command pulse duration: 50 ms

static uint32_t const POLSWITCH_CMD_TIMER = 10;



// ---------- Internal structures, unions and enumerations --------------

//! Internal global variables for the module polswitch

struct Polswitch
{
    struct POLSWITCH_mgr  *  mgr;                 //!< Handler to the libpolswitch manager
    enum POLSWITCH_state     requested_state;     //!< Previous requested state to allow changes to be detected
    uint16_t                 command;             //!< Command to drive the polarity switch
    uint8_t                  timer;               //!< Command pulse timer
    bool                     zero_current_flag;   //!< Pointer to the zero current flag
    bool                     comhv_ps_once;       //!< Used to trigger COMHV-PS spectific actions only once
};



// ---------- Internal variable definitions -----------------------------

static struct Polswitch polswitch;



// ---------- Platform/class specific function definitions  -------------

void polSwitchInit(void)
{
    polswitch.mgr               = (struct POLSWITCH_mgr *)&dpcls.polswitch_mgr;
    polswitch.command           = 0;
    polswitch.zero_current_flag = true;
    polswitch.comhv_ps_once     = true;

    // Initialize libpolswitch

    polswitchMgrInit(polswitch.mgr, 0.005, &polswitch.zero_current_flag, NULL);

    // By default disable AUTO. FGC_63 maps these parameters to properties.

    polswitchParValue(polswitch.mgr, AUTOMATIC) = CC_DISABLED;
}



void polSwitchProcess(void)
{
    static enum POLSWITCH_state prev_state;

    uint32_t dig_ip = digitalIoGetInputs();

    bool simulate = stateOpGetState() == FGC_OP_SIMULATION;
    bool positive = testBitmap(dig_ip, DIG_IP_POLSWIPOS_MASK32)    == true;
    bool negative = testBitmap(dig_ip, DIG_IP_POLSWINEG_MASK32)    == true;
    bool fault    = testBitmap(dig_ip, DIG_IP_POLSWIFLT_MASK32)    == true;
    bool locked   = testBitmap(dig_ip, DIG_IP_POLSWILOCKED_MASK32) == true;

    // ComHV-PS chassis with external polarity switch behaves specially. The polarity switch
    // is only powered on in the blocking state. When the state is OFF, the polarity switch
    // is powered off and transitioned to the negative state even if the mode is positive.
    // That is why for this crate type, the libpolswitch functions should not be called
    // to prevent faults.

    if (   crateGetType() != FGC_CRATE_TYPE_COMHV_PS
        || pcStateTest(FGC_STATE_GE_BLOCKING_BIT_MASK) == true)
    {
        polswitchMgr(polswitch.mgr, simulate, positive, negative, fault, locked);
    }

    // Drive the hardware

    if (polswitch.command == 0)
    {
        // Command not in progress so check for a new state change request

        enum POLSWITCH_state requested_state = polswitchVarValue(polswitch.mgr, REQUESTED_STATE);

        if(requested_state != polswitch.requested_state)
        {
            if (requested_state == POLSWITCH_STATE_POSITIVE)
            {
                polswitch.command = DDOP_CMD_POL_POS;
                polswitch.timer   = POLSWITCH_CMD_TIMER;
            }
            else if (requested_state == POLSWITCH_STATE_NEGATIVE)
            {
                polswitch.command = DDOP_CMD_POL_NEG;
                polswitch.timer   = POLSWITCH_CMD_TIMER;
            }
        }

        polswitch.requested_state = requested_state;
    }
    else
    {
        // Command in progress - clear when pulse timer finishes

        if (--polswitch.timer == 0)
        {
            // Clear the command pulse

            polswitch.command = 0;
        }
    }

    // Update STATUS.FAULTS

    if (polSwitchGetFaults() != 0)
    {
        statusSetFaults(FGC_FLT_POL_SWITCH);
    }
    else
    {
        statusClrFaults(FGC_FLT_POL_SWITCH);
    }

    // Publish SWITCH.POLARITY.STATE if needed

    if (prev_state != polswitchVarValue(polswitch.mgr, STATE))
    {
        prev_state = polswitchVarValue(polswitch.mgr, STATE);

        pubPublishProperty(&PROP_SWITCH_POLARITY_STATE, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, FALSE);
    }
}



void polSwitchReset(void)
{
    polswitchReset(polswitch.mgr);
}



uint16_t polSwitchGetCommand(void)
{
    return polswitch.command;
}


void polSwitchSetMode(enum POLSWITCH_mode mode)
{
    polswitchParValue(polswitch.mgr, MODE_USER) = mode;
}



enum POLSWITCH_state polSwitchGetState(void)
{
    return polswitchVarValue(polswitch.mgr, STATE);
}



enum POLSWITCH_fault_bits polSwitchGetFaults(void)
{
    return polswitchVarValue(polswitch.mgr, LATCHED_FAULTS);
}



bool polSwitchIsEnabled(void)
{
    return (polswitchVarValue(polswitch.mgr, TIMEOUT) != 0);
}


// EOF
