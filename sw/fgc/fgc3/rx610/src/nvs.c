//! @file  nvs.c
//! @brief Non-Volatile Storage Management.
//!
//! This file contains the functions which support the non-volatile storage and recovery
//! of Property data to/from the NVRAM memory.
//!
//! NVRAM is accessible by WORD only - BYTE access will result in a bus error!
//!
//! Some non-volatile properties are part of the configuration, meaning they are stored
//! in the central property database and will be set by the FGC configuration manager.


// ---------- Includes

#include <string.h>

#include <bitmap.h>
#include <class.h>
#include <config.h>
#include <defprops.h>
#include <fgc_errs.h>
#include <init.h>
#include <iodefines.h>
#include <logRun.h>
#include <mem.h>
#include <nvs.h>
#include <panic.h>
#include <prop.h>
#include <prop_class.h>
#include <stateOp.h>
#include <status.h>



// ---------- Constants

//! Magic number in NVS to say it is formatted.

#define NVS_MAGIC               0x1566

//! Invalid NVS index (p->nvs_idx). It means the property is not associated with a NVS record.

#define INVALID_NVS_IDX         0xFFFF

//! NVS property unset.

#define NVS_UNSET               ((prop_size_t)0xFFFFu)

//! NVS property corrupted.

#define NVS_CORRUPTED           ((prop_size_t)0xFFFEu)

//! Memory address with the first record.

#define NVS_RECORDS             ((struct nvs_record *)NVSIDX_REC_32)

//! Buffer size for a given property.

#define NVS_DATA_BUF_SIZE(p)    ((p->max_elements * prop_type_size[p->type] + sizeof(uint16_t) + 1) & ~1)



// ---------- Internal structures, unions and enumerations

//! Internal local variables for the module nvs

struct nvs
{
    uint16_t     num_unlock_requests;     //!< Number of unlocking requests
};



// ---------- External variable definitions

struct nvs_dbg nvs_dbg;



// ---------- Internal variable definitions

static struct nvs nvs;

// Import labels defined by the linker to know possible range for property structures in RAM

extern const char ram_dst_of_data; // Start address for variables
extern const char data_end;        // End address for variables



// ---------- Internal function definitions

//! Initialises the core control table properties in RAM from
//! the values in NVRAM.

static void nvsInitCore(void)
{
    // This function is called by nvsInit(). If the core control table has
    // never been initialised, then it will be set it to default values.

    struct Panic_core_ctrl core_ctrl;
    uint16_t               i;

    panicCoreTableGet(&core_ctrl);

    // Transfer to CORE properties

    for (i = 0; i < FGC_N_CORE_SEGS; i++)
    {
        core.addr[i]    = core_ctrl.seg[i].addr;
        core.n_words[i] = core_ctrl.seg[i].n_words;
    }
}



//! Create a new storage space in NVRAM for that property and copies
//! its contents into it.

static void nvsFormatRecord(uint16_t lvl, struct prop * p)
{
    // This function is called from the recursive function nvsScanNvsProps()
    // when it finds a non-volatile property.

    uint16_t    idx;
    uint16_t    name_length;
    uint16_t    n_bufs;
    uint16_t    bufsize;
    char        namebuf[NVSIDX_REC_NAME_B + 1];
    char      * namebuf_p = namebuf;

    // Stack the symbol index of each part of the property name

    nvs_dbg.sym_idxs[lvl] = p->sym_idx;

    // If it is non volatile return immediately

    if (p->type != PT_PARENT && testBitmap(p->flags, PF_NON_VOLATILE) == true)
    {
        // If no space left in NVS index

        if (nvs_dbg.idx >= (NVSIDX_W / NVSIDX_REC_W))
        {
            panicCrash(0xBAD1);
        }

        // Link property with its new NVS record

        p->nvs_idx = nvs_dbg.idx;

        // Format NVS index record

        bufsize = NVS_DATA_BUF_SIZE(p);

        NVS_RECORDS[nvs_dbg.idx].prop    = (uint32_t) p;
        NVS_RECORDS[nvs_dbg.idx].bufsize = bufsize;
        NVS_RECORDS[nvs_dbg.idx].data    = nvs_dbg.data_addr;

        // Assemble and copy property name into NVS index record

        // Clear the name buffer

        memset(namebuf, 0, NVSIDX_REC_NAME_B + 1);

        // Generate name string from symbols

        for (idx = 0, name_length = 0; idx <= lvl; idx++)
        {
            namebuf_p += snprintf(namebuf_p,
                                  NVSIDX_REC_NAME_B + 1 - name_length, "%s.",
                                  SYM_TAB_PROP[nvs_dbg.sym_idxs[idx]].key.c);

            name_length = namebuf_p - namebuf;

            if (name_length > (NVSIDX_REC_NAME_B + 1))
            {
                panicCrash(0xBADE);
            }
        }

        // Drop trailing '.'

        *(--namebuf_p) = '\0';

        memcpy(&(NVS_RECORDS[nvs_dbg.idx].name), namebuf, NVSIDX_REC_NAME_B);

        // Set every NVS data buffer to have the number of elements equal to NVS_UNSET

        n_bufs =   (testBitmap(p->flags, PF_PPM)     ? FGC_MAX_USER_PLUS_1 : 1)
                 * (testBitmap(p->flags, PF_SUB_DEV) ? FGC_MAX_SUB_DEVS    : 1);

        for (idx = 0; idx < n_bufs; idx++, nvs_dbg.data_addr += bufsize)
        {
            *((uint16_t *)nvs_dbg.data_addr) = (uint16_t)NVS_UNSET;
        }

        // If NVS data area has been overrun or overlap with the NVS index (next in memory map) then crash

        if (nvs_dbg.data_addr > (NVSDATA_32 + NVSDATA_B) || nvs_dbg.data_addr > NVSIDX_32)
        {
            panicCrash(0xBAD8);
        }

        nvs_dbg.idx++;
    }
}



//! Sets the NVS record index to the default value INVALID_NVS_IDX.

static void nvsResetNvsIdx(uint16_t lvl, struct prop * p)
{
    // This function is called by PropMap() via nvsInit().

    p->nvs_idx = INVALID_NVS_IDX;
}



//! Increases the number of corrupted config properties if the property
//! has not been set in NVRAM.

static void nvsCountUnsetConfig(uint16_t lvl, struct prop * p)
{
    if (testBitmap(p->flags, PF_CONFIG) && !testBitmap(p->dynflags, DF_NVS_SET))
    {
        nvs_dbg.n_unset_config_props++;
    }
}



//! Clears the DF_CFG_CHANGED flag in the property.

static void nvsClearCfgChanged(uint16_t lvl, struct prop * p)
{
    clrBitmap(p->dynflags, DF_CFG_CHANGED);
}



//! Stores in NVRAM the data of all non-volatile properties.

static void nvsStoreValuesForAllProperties(uint16_t lvl, struct prop * p)
{
    // This function is called by PropMap() in nvsInit(). Remember that since
    // PropMap() is a recursive function, the deeper the property tree, the
    // more stack space is required.

    if (testBitmap(p->flags, PF_NON_VOLATILE))
    {
        nvsStoreValuesForOneProperty(&tcm, p, NVS_ALL_SUB_SELS, NVS_ALL_CYC_SELS);
    }
}



//! This function is used to update the property dynflags after editing the
//! data stored in NVRAM. It will set the DF_NVS_SET flag only if the number
//! of elements is different from NVS_UNSET for one of the cycle selector.

static void nvsPropDynflags(struct prop * p)
{
    bool prop_set_f = false;

    // If property is still unset

    if (testBitmap(p->dynflags, DF_NVS_SET) == false)
    {
        uint32_t  const bufsize    = NVS_RECORDS[p->nvs_idx].bufsize;
        uint32_t        data_addr  = NVS_RECORDS[p->nvs_idx].data;
        uint16_t  const n_sub_sels = (testBitmap(p->flags, PF_SUB_DEV) ? FGC_MAX_SUB_DEVS    : 1);
        uint16_t  const n_cyc_sels = (testBitmap(p->flags, PF_PPM)     ? FGC_MAX_USER_PLUS_1 : 1);
        uint16_t        sub_sel;
        uint16_t        cyc_sel;

        for (sub_sel = 0; prop_set_f == false && sub_sel < n_sub_sels; ++sub_sel)
        {
            for (cyc_sel = 0; prop_set_f == false && cyc_sel < n_cyc_sels; ++cyc_sel)
            {
                if (   (*((uint16_t *)data_addr) != NVS_UNSET)
                    && (*((uint16_t *)data_addr) != NVS_CORRUPTED)
                    && (*((uint16_t *)data_addr) != 0))
                {
                    if (testBitmap(p->flags, PF_DONT_SHRINK) == false || *((uint16_t *)data_addr) == p->max_elements)
                    {
                        setBitmap(p->dynflags, DF_NVS_SET);
                        prop_set_f = true;
                    }
                }

                data_addr += bufsize;
            }
        }
    }

    // For config properties, keep track of the number unset

    if (testBitmap(p->flags, PF_CONFIG) == true)
    {
        // If property has been set for the first time

        if (prop_set_f == true)
        {
            --nvs_dbg.n_unset_config_props;
        }

        if (configGetMode() == FGC_CFG_MODE_SYNC_FGC)
        {
            clrBitmap(p->dynflags, DF_CFG_CHANGED);
        }
        else
        {
            setBitmap(p->dynflags, DF_CFG_CHANGED);
            configSetState(FGC_CFG_STATE_UNSYNCED, false);
        }
    }
}



//! Recovers the data for all the properties in NVS

static void nvsRecallValuesForAllProperties(bool new_sw_f)
{
    // If the flag new_sw_f is false, then it can use the stored pointer
    // to find the property, otherwise it must look up the property by
    // name and check to see if it is still non-volatile.

    uint32_t prop_addr;
    uint32_t data_addr;
    uint16_t nvs_idx;

    // Recover the number of NVS properties as recorded in the NVS index
    // header and check if it is still valid

    nvs_dbg.n_props = NVSIDX_NUMPROPS_P;

    if (nvs_dbg.n_props >= (NVSIDX_W / NVSIDX_REC_W))
    {
        nvs_dbg.n_corrupted_indexes++;

        // Return immediately to reformat NVS

        return;
    }

    // Loop through all NVS index records

    for (nvs_idx = 0 ; nvs_idx < nvs_dbg.n_props ; nvs_idx++)
    {
        // Check if NVS record is inside the valid memory range

        // Data address in NVSDATA zone (must be word aligned)

        data_addr = NVS_RECORDS[nvs_idx].data;

        if (   (data_addr <  NVSDATA_32)
            || (data_addr > (NVSDATA_32 + NVSDATA_B))
            || (data_addr & 1))
        {
            nvs_dbg.n_corrupted_indexes++;

            continue;
        }

        // Find property

        if (new_sw_f == true)
        {
            // PropFind uses struct cmd pcm, but since this is the init,
            // one does not need to take pcm's semaphore.

            if (   PropFind((char *)NVS_RECORDS[nvs_idx].name) != 0
                || testBitmap(pcm.prop->flags, PF_NON_VOLATILE) == false
                || NVS_RECORDS[nvs_idx].bufsize != NVS_DATA_BUF_SIZE(pcm.prop))
            {
                continue;
            }

            tcm.cached_prop = pcm.prop;
        }
        else
        {
            prop_addr       = NVS_RECORDS[nvs_idx].prop;
            tcm.cached_prop = (struct prop *) prop_addr;

            // Check if the property pointer is corrupted.
            // Warning, this depends on how the variables are located in the
            // RAM memory. The properties are pre filled variables so they go
            // to the .data (not .bss) we test that the properties are in the
            // RAM area as defined by the linker.

            if (   prop_addr < (uint32_t) &ram_dst_of_data
                || prop_addr >= (uint32_t) &data_end
                || testBitmap(tcm.cached_prop->flags, PF_NON_VOLATILE) == false
                || NVS_RECORDS[nvs_idx].bufsize != NVS_DATA_BUF_SIZE(tcm.cached_prop))
            {
                nvs_dbg.n_corrupted_indexes++;

                continue;
            }
        }

        // Link property to NVS record (this will be updated if NVS is
        // reformatted due to a corrupted index). This is used in
        // nvsRecallValuesForOneProperty()

        tcm.cached_prop->nvs_idx = nvs_idx;

        // Extract property data from NVS

        nvsRecallValuesForOneProperty(&tcm, tcm.cached_prop, NVS_ALL_SUB_SELS, NVS_ALL_CYC_SELS);

        // If this is an old software version, there will be no call to
        // nvsStoreValuesForAllProperties() to copy back the property
        // values in NVS, and therefore this property is now correctly
        // set in both RAM and NVS.

        nvsPropDynflags(tcm.cached_prop);
    }
}



//! Checks that the property is non-volatile and bound to a NVS record.

static void nvsSanityCheck(struct prop * p)
{
    if (testBitmap(p->flags, PF_NON_VOLATILE) == false)
    {
        panicCrash(0xBADA + ((uint32_t)p->sym_idx << 16));
    }

    if (p->nvs_idx == INVALID_NVS_IDX)
    {
        panicCrash(0xBADB);
    }
}



//! Scans the property tree and for NVS properties it creates a record
//! and saves the property data in the record.

static void nvsFormat(void)
{
    // This function is called by nvsInit() if it finds that the software
    // version has changed or the NVS area needs to be reformatted.

    nvs_dbg.data_addr = NVSDATA_32;

    nvsUnlock();

    MemSetWords((void *) NVSIDX_32,  0x0000, NVSIDX_W);
    MemSetWords((void *) NVSDATA_32, 0x0000, NVSDATA_W);

    // Format and fill NVS records for all NVS properties

    PropMap(nvsFormatRecord, PROP_MAP_ALL);

    // Update software version and magic number

    NVSIDX_NUMPROPS_P = nvs_dbg.idx;
    NVSIDX_SWVER_P    = version.unixtime;
    NVSIDX_MAGIC_P    = NVS_MAGIC;
    NVRAM_NVS_COMPATIBILITY_FP = NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION;

    nvsLock();
}



// ---------- External function definitions

void nvsInit(void)
{
    // This function is called during startup, just after interrupts are
    // enabled so that the DSP will run and allow DSP properties to be
    // configured. It is responsible for the recovery/formatting of
    // non-volatile properties using data from the NVS area of the NVRAM
    // memory. This can take three forms:
    //
    //  1. NVS never used:         erase & format
    //  2. SW version changed:     recover data where possible, then erase and re-format
    //  3. SW version not changed: recover data.
    //
    //  This function sets the operational state (STATE_OP) to UNCONFIGURED or
    //  NORMAL according to whether there are unset configuration properties
    //  after the NVS property initialisation.

    struct init_prop const * property_list = &prop_class_init_nvs[0];

    bool new_sw_f;

    nvs.num_unlock_requests = 1;

    nvsInitCore();

    // Set the nvs_idx for all properties to INVALID_NVS_IDX

    PropMap(nvsResetNvsIdx, PROP_MAP_ALL);

    // If NVS never formatted (or is to be reformatted) or config
    // is too out of date

    if (   NVSIDX_MAGIC_P             != NVS_MAGIC
        || NVRAM_NVS_COMPATIBILITY_FP != NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION)
    {
        nvsFormat();
    }
    else
    {
        new_sw_f = (NVSIDX_SWVER_P != version.unixtime);

        nvsRecallValuesForAllProperties(new_sw_f);

        if (new_sw_f == true || nvs_dbg.n_corrupted_indexes != 0)
        {
            nvsFormat();
            PropMap(nvsStoreValuesForAllProperties, PROP_MAP_ONLY_CHILDREN);
        }
    }

    nvs_dbg.n_unset_config_props = 0;

    PropMap(nvsCountUnsetConfig, PROP_MAP_ONLY_CHILDREN);

    // Initialise MODE.OP if not recovered from the NVS

    if (stateOpGetModeOp() == FGC_OP_UNCONFIGURED)
    {
        stateOpSetModeOp(FGC_OP_NORMAL);
    }

    // If MODE.OP is TEST, then reset it to value in property_list (NORMAL)

    if (stateOpGetModeOp() == FGC_OP_TEST)
    {
        clrBitmap(PROP_MODE_OP.dynflags, DF_NVS_SET);
    }

    // Initialise properties

    while (property_list->prop)
    {
        if (testBitmap(property_list->prop->dynflags, DF_NVS_SET) == false)
        {
            PropSet(&tcm, property_list->prop, property_list->data, property_list->n_elements);
        }

        property_list++;
    }

    // Reset the DF_CFG_CHANGED flag set during the NVS initialiation

    PropMap(nvsClearCfgChanged, PROP_MAP_ONLY_CHILDREN);

    interFgcPopulateStatus();

    initPostNvs();

    // Signal the DSP that the properties have been retrieved from the NVS

    dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_NVS_INIT);
}



uint16_t nvsStoreValuesForOneProperty(struct cmd         * c,
                                      struct prop        * p,
                                      uint16_t     const   sub_sel,
                                      uint16_t     const   cyc_sel)
{
    uint16_t      cyc_sel_from;
    uint16_t      cyc_sel_to;
    uint16_t      sub_sel_from;
    uint16_t      sub_sel_to;
    uint32_t      el_size;
    uint32_t      bufsize;
    uint32_t      runlog_error;
    size_t        n_bytes;
    prop_size_t   n_els;
    prop_size_t   n_els_old;
    uint32_t      data_addr;
    uint32_t      block_addr;
    uint16_t      n_blocks;
    uint16_t      remaining_w;
    uint16_t      errnum = FGC_OK_NO_RSP;


    if (testBitmap(p->flags, PF_NON_VOLATILE) == false)
    {
        return 0;
    }


    // Get NVS record info

    nvsSanityCheck(p);

    // Sub-device selector range

    if (testBitmap(p->flags, PF_SUB_DEV) == true)
    {
        if (sub_sel == NVS_ALL_SUB_SELS)
        {
            sub_sel_from = 0;
            sub_sel_to   = FGC_MAX_SUB_DEVS - 1;
        }
        else
        {
            sub_sel_from = sub_sel_to = sub_sel;
        }
    }
    else
    {
        sub_sel_from = sub_sel_to = 0;
    }

    // Cycle selector range

    if (testBitmap(p->flags, PF_PPM) == true)
    {
        if (cyc_sel == NVS_ALL_CYC_SELS)
        {
            cyc_sel_from = 0;
            cyc_sel_to   = FGC_MAX_USER;
        }
        else
        {
            cyc_sel_from = cyc_sel_to = cyc_sel;
        }
    }
    else
    {
        cyc_sel_from = cyc_sel_to = 0;
    }

    // Data address in NVSDATA zone

    el_size   = prop_type_size[p->type];
    bufsize   = NVS_RECORDS[p->nvs_idx].bufsize;
    data_addr = NVS_RECORDS[p->nvs_idx].data + bufsize * (cyc_sel_from + sub_sel_from * FGC_MAX_USER_PLUS_1);

    // This is required to call the PropBlkGet() function

    c->cached_prop = p;

    nvsUnlock();

    // Loop through all the sub-devices and cycles

    for (c->sub_sel = sub_sel_from; c->sub_sel <= sub_sel_to; ++c->sub_sel)
    {
        for (c->cyc_sel  = cyc_sel_from; c->cyc_sel <= cyc_sel_to; ++c->cyc_sel, data_addr += bufsize)
        {
            c->prop_blk_idx = 0;
            errnum          = PropBlkGet(c, &n_els);

            if (errnum != FGC_OK_NO_RSP)
            {
                runlog_error = 0x01000000 + (p->nvs_idx << 16) + (c->cyc_sel << 8) + errnum;
                logRunAddEntry(FGC_RL_NVS_ERR, &runlog_error);

                continue;
            }

            if (n_els == 0)
            {
                continue;
            }

            remaining_w = (n_els * el_size + 1) / 2;
            n_blocks    = (remaining_w + PROP_BLK_SIZE_W - 1) / PROP_BLK_SIZE_W;

            // Skip the word containing number of elements

            block_addr = data_addr + sizeof(uint16_t);

            while (n_blocks--)
            {
                // If not first block

                if (c->prop_blk_idx != 0)
                {
                    errnum = PropBlkGet(c, NULL);

                    if (errnum != FGC_OK_NO_RSP)
                    {
                        runlog_error = 0x02000000 + (p->nvs_idx << 16) + (c->cyc_sel << 8) + errnum;
                        logRunAddEntry(FGC_RL_NVS_ERR, &runlog_error);
                    }
                }

                memcpy((void *) block_addr,
                       c->prop_buf->blk.intu,
                       (n_blocks ? PROP_BLK_SIZE_W : remaining_w) * sizeof(uint16_t));

                block_addr  += PROP_BLK_SIZE;
                remaining_w -= PROP_BLK_SIZE_W;

                c->prop_blk_idx++;
            }

            if (errnum == FGC_OK_NO_RSP)
            {
                n_els_old  = *((uint16_t *)data_addr);

                if (n_els_old == NVS_CORRUPTED)
                {
                    // Cleaning up the buffer until the end if it was corrupted

                    n_bytes = (p->max_elements - n_els) * el_size;
                    memset((void *)(data_addr + sizeof(uint16_t) + n_els * el_size), 0, n_bytes);
                }
                else if (n_els_old != NVS_UNSET && n_els < n_els_old)
                {
                    // Cleaning up the buffer if the length has shrunk

                    n_bytes = (n_els_old - n_els) * el_size;
                    memset((void *)(data_addr + sizeof(uint16_t) + n_els * el_size), 0, n_bytes);
                }

                // Copy number of elements in the first word of NVRAM data

                *((uint16_t *)data_addr) = (uint16_t)n_els;

            }
        }
    }

    nvsLock();

    // Restore the sub-device and cycle selector indexes in the command structure

    c->cyc_sel = cyc_sel_from;
    c->sub_sel = sub_sel_from;

    // Mark the property as set in NVS

    nvsPropDynflags(p);

    return errnum;
}



uint16_t nvsStoreValuesForOneDspProperty(struct prop        * p,
                                         uint16_t     const   sub_sel,
                                         uint16_t     const   cyc_sel)
{
    uint16_t retval;

    // Acquire the mutex to make sure the publication task is not using the
    // pcm structure

    OSSemPend(pcm.sem);

    pcm.prop_buf = (struct Dpcom_prop_buf *) &dpcom.pcm_prop;

    retval = nvsStoreValuesForOneProperty(&pcm, p, sub_sel, cyc_sel);

    OSSemPost(pcm.sem);

    return retval;
}



void nvsStoreBlockForOneProperty(struct cmd         * c,
                                 struct prop        * p,
                                 uint8_t            * data,
                                 uint16_t     const   sub_sel,
                                 uint16_t     const   cyc_sel,
                                 prop_size_t  const   n_elements,
                                 bool         const   last_blk_f)
{
    char        * data_addr;
    char        * block_addr;
    prop_size_t   n_els_old;
    uint16_t      n_bytes;
    uint16_t      block_idx;
    uint16_t      el_size;

    nvsSanityCheck(p);

    data_addr = (char *)NVS_RECORDS[p->nvs_idx].data;

    if (cyc_sel != 0 || sub_sel != 0)
    {
        // Select data buffer based on the sub-device and cycle selectors.

        data_addr += (uint32_t)NVS_RECORDS[p->nvs_idx].bufsize * (cyc_sel + sub_sel * FGC_MAX_USER_PLUS_1);
    }

    // Get local copy of el_size

    el_size = prop_type_size[p->type];

    // Use prop block index if supplied

    block_idx = (c != NULL ? c->prop_blk_idx : 0);

    // Store the original number of elements

    n_els_old = *((uint16_t *)data_addr);

    // Offset the address by the block index

    block_addr = data_addr + sizeof(uint16_t) + (uint32_t) block_idx * PROP_BLK_SIZE;

    n_bytes = el_size * n_elements - block_idx * PROP_BLK_SIZE;

    if (n_bytes > PROP_BLK_SIZE)
    {
        n_bytes = PROP_BLK_SIZE;
    }
    else
    {
        // If data has an odd number of bytes pad with zero to word boundary.

        if (n_bytes & 1)
        {
            data[n_bytes++] = 0;
        }
    }

    // Early return if the data has not changed

    if (   n_elements == n_els_old
        && MemCmpWords(block_addr, data, n_bytes / 2) == 0)
    {
        return;
    }

    // If the property was never set or it was corrupted, zero the original number of elements

    if (n_els_old == NVS_UNSET || n_els_old == NVS_CORRUPTED)
    {
        n_els_old = 0;
    }

    nvsUnlock();

    // Set the number of elements to an invalid value until the entire property
    // has been written. This will help detect inconsistent property values if
    // the FGC3 is for example reset whilst writing into NVRAM.

    *((uint16_t *)data_addr) = (uint16_t)NVS_CORRUPTED;

    memcpy(block_addr, data, n_bytes);

    if (last_blk_f == true)
    {
        if (n_elements < n_els_old)
        {
            // Zero data if the property has shrunk

            n_bytes = (n_els_old - n_elements) * el_size;

            memset((void *)(data_addr + sizeof(uint16_t) + n_elements * el_size), 0, n_bytes);
        }

        *((uint16_t *)data_addr) = (uint16_t)n_elements;
    }

    nvsLock();

    nvsPropDynflags(p);
}



void nvsRecallValuesForOneProperty(struct cmd         * c,
                                   struct prop        * p,
                                   uint16_t     const   sub_sel,
                                   uint16_t     const   cyc_sel)
{
    uint32_t      data_addr;
    uint32_t      block_addr;
    uint32_t      bufsize;
    prop_size_t   n_els;
    uint16_t      nvs_idx;
    uint16_t      cyc_sel_from;
    uint16_t      cyc_sel_to;
    uint16_t      sub_sel_from;
    uint16_t      sub_sel_to;

    // Get NVS record

    nvsSanityCheck(p);

    nvs_idx = p->nvs_idx;

   // Sub-device selector range

    if (testBitmap(p->flags, PF_SUB_DEV) == true)
    {
        if (sub_sel == NVS_ALL_SUB_SELS)
        {
            sub_sel_from = 0;
            sub_sel_to   = FGC_MAX_SUB_DEVS - 1;
        }
        else
        {
            sub_sel_from = sub_sel_to = sub_sel;
        }
    }
    else
    {
        sub_sel_from = sub_sel_to = 0;
    }

    // Cycle selector range

    if (testBitmap(p->flags, PF_PPM) == true)
    {
        if (cyc_sel == NVS_ALL_CYC_SELS)
        {
            cyc_sel_from = 0;
            cyc_sel_to   = FGC_MAX_USER;
        }
        else
        {
            cyc_sel_from = cyc_sel_to = cyc_sel;
        }
    }
    else
    {
        cyc_sel_from = cyc_sel_to = 0;
    }

    // Prepare data address for the first cyc_sel

    bufsize   = NVS_RECORDS[nvs_idx].bufsize;
    data_addr = NVS_RECORDS[p->nvs_idx].data + bufsize * (cyc_sel_from + sub_sel_from * FGC_MAX_USER_PLUS_1);

    // Extract property data from NVS. This is the only function where property
    // is set without writing it in the NVRAM

    c->write_nvs_f = false;

    // Loop through all the sub-devices and cycles

    for (c->sub_sel = sub_sel_from; c->sub_sel <= sub_sel_to; ++c->sub_sel)
    {
        for (c->cyc_sel  = cyc_sel_from; c->cyc_sel <= cyc_sel_to; ++c->cyc_sel, data_addr += bufsize)
        {
            n_els = *((uint16_t *)data_addr);

            if (n_els == NVS_UNSET)
            {
                n_els = 0;
            }
            else if (n_els == NVS_CORRUPTED || n_els > p->max_elements)
            {
                nvs_dbg.n_corrupted_indexes++;
            }
            else
            {
                // Skip the n_els word at the start of the buffer and set the property

                block_addr = data_addr + sizeof(uint16_t);

                PropSetFar(c, p, (void *) block_addr, n_els);
            }
        }
    }

    // Restore the sub-device and cycle selector indexes in the command structure

    c->cyc_sel = cyc_sel_from;
    c->sub_sel = sub_sel_from;

    // Restore the default behaviour of the PropBlkSet() function

    c->write_nvs_f = true;
}



void nvsClearMagicNumber(void)
{
    nvsUnlock();

    NVSIDX_MAGIC_P = ~NVS_MAGIC;

    nvsLock();
}



void nvsSetMagicNumber(void)
{
    nvsUnlock();

    NVSIDX_MAGIC_P = NVS_MAGIC;

    nvsLock();
}



void nvsUnlock(void)
{
    nvs.num_unlock_requests++;

    P7.DR.BIT.B5 = 0;
}



void nvsLock(void)
{
    // In case a write instruction to NVRAM is done right before nvsLock(), the CPU
    // pipeline can result in the NVRAM becoming locked before the write is executed.
    // To prevent this, a dummy read is executed to provide the necessary delay.

    uint16_t dummy_read;

    (void) dummy_read;

    if (--nvs.num_unlock_requests == 0)
    {
        dummy_read = NVRAM_SAFE_MODE_P;

        P7.DR.BIT.B5 = 1;
    }
}


// EOF
