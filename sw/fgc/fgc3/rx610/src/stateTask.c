//! @file  stateTask.c
//! @brief Power Converter State Machine


// ---------- Includes --------------------------------------------------

#include <stdint.h>

#include <calibration.h>
#include <class.h>
#include <defprops.h>
#include <digitalIo.h>
#include <logEvent.h>
#include <modePc.h>
#include <os_hardware.h>
#include <pc_state.h>
#include <pub.h>
#include <sta.h>
#include <statePc.h>
#include <stateOp.h>
#include <stateVs.h>
#include <stateTask.h>
#include <status.h>
#include <time_fgc.h>



// ---------- External variable definitions

pc_state_vars_t  sta;



// ---------- External function definitions -----------------------------

void stateTaskInit(void)
{
    stateOpInit();
    pcStateSet(FGC_PC_OFF);

#ifdef CCLIBS
    sta.copy_log_capture_state = FGC_LOG_RUNNING;
    sta.copy_log_capture_user  = -1;
    sta.copy_log_capture_cyc_chk_time.secs.abs  = 0;
    sta.copy_log_capture_cyc_chk_time.us = 0;
#endif
}



void stateTask(void * unused)
{
    calibrationInhibit(30);

    // Main loop @ 200Hz

    for (;;)
    {
        // Wait for OSTskResume() from msTask();

        OSTskSuspend();

        taskTraceReset();

        sta.sec_f     = (timeGetUtcTimeMs() == DEV_STA_TSK_PHASE);

        // Calculate iteration timestamp for logging

        sta.timestamp_ms.secs.abs = timeGetUtcTime();
        sta.timestamp_ms.ms       = timeGetUtcTimeMs();

        timeCopyFromMsToUs(&sta.timestamp_ms, &sta.timestamp_us);

        stateOpProcess();

        if (stateOpGetState() == FGC_OP_UNCONFIGURED)
        {
            statusProcess();
        }
        else
        {
            // The order of these functions matters

            digitalIoInputsProcess();

            stateVsProcess();

            statusProcess();

            taskTraceInc();

            calibrationProcess();

            taskTraceInc();

            modePcProcess();

            statePcProcess();

            digitalIoOutputsProcess();

            diagClassCheckConverter();

            taskTraceInc();

            // Update the event log

            logEventStoreProperties();

            taskTraceInc();

            // If standalone or UTC time received

            if (fbsIsStandalone() || sta.timestamp_ms.secs.abs != 0)
            {
                // If system contains DIMs

                if (diag.n_dims)
                {
                    logEventDimProcess();
                }
            }

            taskTraceInc();

            // Publication of properties

            // If start of the second

            if (sta.sec_f)
            {
                // Publish TIME.NOW

                pubPublishProperty(&PROP_TIME_NOW, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, FALSE);
            }
        }

        msTaskResetStateTaskWatchdog();
    }
}


// EOF
