//! @file  logCycle.c
//! @brief Interface to the library libcyclog


// ---------- Includes

#include <stdint.h>

#include <libcyclog.h>

#include <logCycle.h>
#include <dpcom.h>



// ---------- Internal structures, unions and enumerations

//! Internal global variables for the module logCycle

struct logCycle
{
    struct CYCLOG_device   device;            //!< Allocation of memory for device data
    bool                   start_super_cycle; //!< True on the first cycle of the super-cycle
};



// ---------- Internal variable definitions

static struct logCycle __attribute__((section("sram"))) cycle_log;



// ---------- External function definitions

void logCycleInit(void)
{
    // Make sure all FGC and cclibs constants agree

    CC_STATIC_ASSERT((CYCLOG_NUM_RECORDS == FGC_LOG_CYCLES_LEN), CYCLOG_NUM_RECORDS);

    cyclogInit(&cycle_log.device,
               logCycleClassFuncTypeCallback,
               logCycleClassStatusCallback,
               logCycleClassGetStatusOk(),
               logCycleClassGetStatusWarnings(),
               logCycleClassGetStatusFaults());
}



void logCycleStore(void)
{
    volatile struct Dpcom_dsp_log_timing * log_timing = &dpcom.dsp.log.timing;

    if (log_timing->info.cyc_sel_acq == 0)
    {
        return;
    }

    cyclogStore(&cycle_log.device,
                (struct CC_us_time *)&log_timing->info.start_time,
                log_timing->info.sub_sel_acq,
                log_timing->info.cyc_sel_acq,
                log_timing->info.func_type,
                log_timing->status_bitmask,
                cycle_log.start_super_cycle);

    cycle_log.start_super_cycle = false;

    log_timing->info.cyc_sel_acq = 0;
}



void logCycleStartSuperCycle(void)
{
    cycle_log.start_super_cycle = true;
}



void logCycleGet(uint32_t const log_index, char * read_buf, uint32_t const read_buf_len)
{
    cyclogRead(&cycle_log.device, log_index, read_buf, read_buf_len);
}


// EOF
