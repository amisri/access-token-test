//! @file  main.c
//! @brief Contains the main() function


// ---------- Includes

#define DEFINECCPRINTFDUMMY
#include <cclibs.h>

#include <main.h>
#include <init.h>
#include <os_hardware.h>
#include <sharedMemory.h>
#include <taskTrace.h>

#include <dpcom.h>

// ---------- External variable definitions

//! Memory area shared by boot and main, mapped to 0x00000560 by the linker (sharedMemory.h)
volatile struct Shared_memory __attribute__((section("shared_area"))) shared_mem;

//! Task trace (taskTrace.h)
struct Task_trace task_trace;


// ---------- External function definitions

int main(void)
{
    initInit();

    // Never returns

    OSTskStart(); 

    // Appease the compiler
    
    return 0;
}


// EOF
