//! @file  logEvent.h
//! @brief Interface to libevtlog


// ---------- Includes

#include <string.h>

#include <defprops.h>

#include <libevtlog.h>
#include <class.h>
#include <cmd.h>
#include <fbs.h>
#include <logEvent.h>
#include <pub.h>
#include <pc_state.h>
#include <time_fgc.h>


// Declare libevtlog callbacks

static EVTLOG_lock_callback    logEventLock;
static EVTLOG_lock_callback    logEventUnlock;
static EVTLOG_changed_callback logEventChanged;

// Auto-generated libevtlog headers

#include <evtlogStructs.h>
#include <evtlogStructsInit.h>



// ---------- Internal structures, unions and enumerations

//! DIM log event record

struct LogEvent_dim_rec
{
    struct CC_us_time       timestamp;                      //!< Timestamp (Unix time + microsecond)
    char                    prop[EVTLOG_PROPERTY_LEN];      //!< Property name field
    char                    value[EVTLOG_VALUE_LEN];        //!< Value field
    char                    action[EVTLOG_ACTION_LEN];      //!< Action field
};


//! DIM log event data

struct LogEvent_dim
{
    uint16_t                sample_size;                    //!< Number of bytes per sample
    uint16_t                dim_props_idx;                  //!< DIM index (0 -> (diag.n_dims-1))
    uint16_t                logical_dim;
    uint16_t                dim_state;                      //!< DIM event log state
    uint16_t                dim_bank_idx;                   //!< DIM bank (0-1)
    uint16_t                dim_input_idx;                  //!< DIM input index (0-11)
    uint16_t                dim_input_mask;                 //!< DIM input mask (0x001 - 0x800)
    uint16_t                fault_mask[2];                  //!< DIM bank fault mask
    uint16_t                dim_new_data[2];                //!< New DIM data for 2 banks
    uint16_t                dim_old_data[2][FGC_MAX_DIMS];  //!< Old data for 2 banks for all DIMs
    struct LogEvent_dim_rec dim_log_rec;                    //!< DIM log record
};



//! Internal global variables for the module logEvent

struct LogEvent
{
    struct EVTLOG_data   data;       //!< Allocation of memory for device data, prop_data, last_evt and records
};



// ---------- External variable definitions

OS_SEM * event_log_sem;



// ---------- Internal variable definitions

static struct LogEvent      __attribute__((section("sram"))) log_event;
static struct LogEvent_dim  __attribute__((section("sram"))) log_event_dim;



// ---------- Internal function definitions

static void logEventStoreRecord(struct CC_us_time const * time_stamp,
                                char              const * name,
                                char              const * value,
                                char              const * action,
                                char              const   status)
{
    if (time_stamp == NULL)
    {
        struct CC_us_time now = { { timeGetUtcTime() }, timeGetUtcTimeMs() * 1000 };

        evtlogStoreRecord(&log_event.data.device, now, name, value, action, status);
    }
    else
    {
        evtlogStoreRecord(&log_event.data.device, *time_stamp, name, value, action, status);
    }
}



static void logEventLock(uint32_t device_index, void * const data)
{
    OSSemPend(event_log_sem);
}



static void logEventUnlock(uint32_t device_index, void * const data)
{
    OSSemPost(event_log_sem);
}



static void logEventChanged(uint32_t device_index, char const * const name, void * const data)
{
    pubPublishProperty((struct prop *)data, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
}



//! This function is called logDimProcess() to record the state of a DIM digital input in the event log

static void logEvtentDimInput(bool const input_zero, const bool fault_input, bool const dimdb_lbl_is_null)
{
    uint16_t dim_idx = SysDbDimDbType(device.sys.sys_idx,log_event_dim.logical_dim);

    if (!dimdb_lbl_is_null)
    {
        // Report DIM trigger time always

        log_event_dim.dim_log_rec.value[0] = 'T';
        log_event_dim.dim_log_rec.value[1] = 'R';
        log_event_dim.dim_log_rec.value[2] = 'I';
        log_event_dim.dim_log_rec.value[3] = 'G';
        log_event_dim.dim_log_rec.value[4] = 'G';
        log_event_dim.dim_log_rec.value[5] = 'E';
        log_event_dim.dim_log_rec.value[6] = 'R';
        log_event_dim.dim_log_rec.value[7] = '\0';

        log_event_dim.dim_log_rec.action[0] = 'S';
        log_event_dim.dim_log_rec.action[1] = 'E';
        log_event_dim.dim_log_rec.action[2] = 'T';
        log_event_dim.dim_log_rec.action[3] = '\0';
    }
    else                    // else use label
    {
        if (fault_input)
        {
            log_event_dim.dim_log_rec.value[0] = 'F';
            log_event_dim.dim_log_rec.value[1] = 'L';
            log_event_dim.dim_log_rec.value[2] = 'T';
            log_event_dim.dim_log_rec.value[3] = '|';
        }
        else
        {
            log_event_dim.dim_log_rec.value[0] = 'S';
            log_event_dim.dim_log_rec.value[1] = 'T';
            log_event_dim.dim_log_rec.value[2] = 'A';
            log_event_dim.dim_log_rec.value[3] = '|';
        }

        memcpy(&log_event_dim.dim_log_rec.value[4],
               DimDbDigitalLabel(dim_idx, (log_event_dim.dim_bank_idx+4), log_event_dim.dim_input_idx),
               EVTLOG_VALUE_LEN - 4);

        memcpy(&log_event_dim.dim_log_rec.action,
               (  input_zero
                ? DimDbDigitalLabelZero(dim_idx, (log_event_dim.dim_bank_idx+4), log_event_dim.dim_input_idx)
                : DimDbDigitalLabelOne(dim_idx, (log_event_dim.dim_bank_idx+4), log_event_dim.dim_input_idx)),
                EVTLOG_ACTION_LEN);
    }

    // Do not log signals that have no label associated to it

    if (strlen(log_event_dim.dim_log_rec.action) != 0)
    {
        logEventStoreRecord(&log_event_dim.dim_log_rec.timestamp,
                            log_event_dim.dim_log_rec.prop,
                            log_event_dim.dim_log_rec.value,
                            log_event_dim.dim_log_rec.action,
                            EVTLOG_STATUS_NORMAL);
    }
}



// ---------- External function definitions

void logEventInit(void)
{
    // Call auto-generated function to initialise evtlog structs

    evtlogStructsInitDevice(&log_event.data.device, 0, 0);

    // Prepare DIM log entry header

    log_event_dim.dim_log_rec.prop[0]  = 'D';
    log_event_dim.dim_log_rec.prop[1]  = 'I';
    log_event_dim.dim_log_rec.prop[2]  = 'M';
    log_event_dim.dim_log_rec.prop[3]  = '.';

    log_event_dim.sample_size = sizeof(struct LogEvent_dim_rec);
}



void logEventStoreProperties(void)
{
    // Scan event log properties

    struct CC_us_time now = { { timeGetUtcTime() }, timeGetUtcTimeMs() * 1000 };

    evtlogStoreProperties(&log_event.data.device, now);
}



void logEventStoreEventRef(uint32_t const delay_us,
                           uint8_t  const sub_sel,
                           uint8_t  const cyc_sel,
                           uint8_t  const type)
{
    char value[EVTLOG_VALUE_LEN];

    struct CC_us_time now = { { timeGetUtcTime() }, timeGetUtcTimeMs() * 1000 };

    timeUsAddOffset(&now, delay_us);

    snprintf(value, EVTLOG_VALUE_LEN, "Sub-dev %2u  User %2u", (unsigned int)sub_sel, (unsigned int)cyc_sel);

    logEventStoreRecord(&now, fgc_event_names[type], value, "EVENT", EVTLOG_STATUS_FREQ);

    // Only log events if LOG.LOG_EVENTS is ENABLED

    if (log_props.log_events == FGC_CTRL_ENABLED)
    {
        snprintf(value, EVTLOG_VALUE_LEN, "%-19s (%2u-%2u) %7u", fgc_event_names[type],
                                                                 (unsigned int)sub_sel,
                                                                 (unsigned int)cyc_sel,
                                                                 (unsigned int)delay_us);

        logEventStoreRecord(NULL, "TIME_EVENT", value, "RECV", EVTLOG_STATUS_FREQ);
    }
}



void logEventStoreEventCommon(uint32_t const delay_us,
                              uint8_t  const type,
                              uint16_t const payload)
{
    if (log_props.log_events == FGC_CTRL_ENABLED)
    {
        char value[EVTLOG_VALUE_LEN];

        snprintf(value, EVTLOG_VALUE_LEN, "%-19s (%5u) %7u", fgc_event_names[type],
                                                             (unsigned int)payload,
                                                             (unsigned int)delay_us);

        logEventStoreRecord(NULL, "TIME_EVENT", value, "RECV", EVTLOG_STATUS_FREQ);
    }
}



void logEventStoreEventNextDest(uint32_t const delay_us,
                                uint8_t  const type,
                                uint8_t  const event_type,
                                uint16_t const dest_mask)
{
    if (log_props.log_events == FGC_CTRL_ENABLED)
    {
        char value[EVTLOG_VALUE_LEN];

        snprintf(value, EVTLOG_VALUE_LEN, "NEXT_DEST       (%2u-0x%04x) %7u", (unsigned int)event_type,
                                                                              (unsigned int)dest_mask,
                                                                              (unsigned int)delay_us);

        logEventStoreRecord(NULL, "TIME_EVENT", value, "RECV", EVTLOG_STATUS_FREQ);
    }
}



void logEventStorePulse(struct CC_us_time const * time_stamp,
                        char              const * status,
                        int32_t           const   offset_us,
                        float             const   ref_user,
                        float             const   meas)
{
    char prop[EVTLOG_PROPERTY_LEN];

    snprintf(prop, EVTLOG_PROPERTY_LEN, "PULSE   %15s", status);

    char value[EVTLOG_VALUE_LEN];

    float ref_int;
    float ref_fract;
    float meas_int;
    float meas_fract;

    ref_fract  = modff(ref_user, &ref_int);
    meas_fract = modff(meas,     &meas_int);

    snprintf(value, EVTLOG_VALUE_LEN,  "%6d %7d.%03u %7d.%03u",
                                       (int)offset_us,
                                       (int)ref_int,
                                       (int)(fabs(ref_fract) * 1000),
                                       (int)meas_int,
                                       (int)(fabs(meas_fract) * 1000));

    logEventStoreRecord(time_stamp, prop, value, "ACQ", EVTLOG_STATUS_FREQ);
}



void logEventStoreSetCmd(char     const * name,
                         char     const * value,
                         char     const * source,
                         uint16_t         errnum)
{
    char action[EVTLOG_ACTION_LEN + 1];

    snprintf(action, EVTLOG_ACTION_LEN + 1, "%s.%03u", source, errnum);

    logEventStoreRecord(NULL, name, value, action, EVTLOG_STATUS_NORMAL);
}



void logEventStoreTimingEdge(struct CC_us_time const * edge_time_us,
                             char              const * name,
                             char              const * value,
                             bool              const   rise)
{
    logEventStoreRecord(edge_time_us, name, value, rise == true ? "RISE" : "FALL", EVTLOG_STATUS_FREQ);
}



void logEventStoreTimeout(char  const * timeout_name,
                          float const   timeout_value)
{
    char value[EVTLOG_VALUE_LEN];

    // Parsing "%f" crashes the FGC3. Extract the integral and decimal parts individually

    int integral   = (int)timeout_value;
    int fractional = (timeout_value - (float)integral) * 1000;

    snprintf(value, EVTLOG_VALUE_LEN, "%s %u.%3us", timeout_name, integral, fractional);

    logEventStoreRecord(NULL, "TIMEOUT", value, "TMO", EVTLOG_STATUS_NORMAL);
}


uint16_t logEventGetLogEvt(struct cmd * c, bool is_bin)
{
    uint16_t errnum       = 0;
    uint16_t record_index = 0;
    char     buffer[EVTLOG_READ_FGC_BUF_LEN];

    if (is_bin == true)
    {
        while (evtlogReadBin(&log_event.data.device, &record_index, buffer, EVTLOG_NETWORK_BYTE_ORDER) != 0)
        {
            fbsOutBuf((uint8_t*)buffer, EVTLOG_RECORD_LEN, c);
        }
    }
    else
    {
        while (errnum == 0 && evtlogReadFgc(&log_event.data.device, &record_index, buffer) != 0)
        {
            errnum = CmdPrintIdx(c, c->from);

            fprintf(c->f, "%s", buffer);
        }
    }

    return errnum;
}



void logEventDimProcess(void)
{
    // This function is called at 200Hz from stateTask() to log the triggered DIM digital inputs.

    uint16_t   dim_props_idx;
    uint16_t   board_number;
    bool       input_zero_f;
    bool       input_zero_old_f;
    bool       fault_input_f;
    bool       end_f;
    uint16_t   dim_idx;

    dim_props_idx = log_event_dim.dim_props_idx;

    do
    {
        // If logging a DIM

        if (log_event_dim.dim_input_mask)
        {
            do
            {
                dim_idx = SysDbDimDbType(device.sys.sys_idx,log_event_dim.logical_dim);

                // If channel is in use

                if (DimDbIsDigitalInput(dim_idx, (log_event_dim.dim_bank_idx+4), log_event_dim.dim_input_idx))
                {

                    // fault_input_f:    Is the data bit indicating a fault?
                    // input_zero_f:     Is the new data bit zero?
                    // input_zero_old_f: Is the old data bit zero?

                    fault_input_f     =  testBitmap(log_event_dim.dim_input_mask, log_event_dim.fault_mask  [log_event_dim.dim_bank_idx]);
                    input_zero_f      = !testBitmap(log_event_dim.dim_input_mask, log_event_dim.dim_new_data[log_event_dim.dim_bank_idx]);
                    input_zero_old_f  = !testBitmap(log_event_dim.dim_input_mask,
                                              log_event_dim.dim_old_data[log_event_dim.dim_bank_idx][log_event_dim.logical_dim]);

                    // In TRIG  state: Log all non zero values, and faults, if they are set
                    // In WATCH state: Log any fault that has just appeared

                    if ((log_event_dim.dim_state == DIM_EVT_TRIG  && (!fault_input_f || !input_zero_f)) ||
                        (log_event_dim.dim_state == DIM_EVT_WATCH && fault_input_f && !input_zero_f && input_zero_old_f))
                    {
                        // logDimRequest(log_event_dim.logical_dim);
                        logEvtentDimInput(input_zero_f, fault_input_f,true);
                        end_f = TRUE;
                    }
                }

                log_event_dim.dim_input_mask <<= 1;

                log_event_dim.dim_input_idx++;

                if (log_event_dim.dim_input_idx >= FGC_N_DIM_DIG_INPUTS)
                {
                    log_event_dim.dim_bank_idx++;

                    if (log_event_dim.dim_bank_idx < FGC_N_DIM_DIG_BANKS)
                    {
                        log_event_dim.dim_input_idx  = 0;
                        log_event_dim.dim_input_mask = 1;
                    }
                    else
                    {
                        log_event_dim.dim_input_mask = 0;

                        if (log_event_dim.dim_state == DIM_EVT_TRIG)
                        {
                            dim_collection.evt_log_state[log_event_dim.logical_dim] = DIM_EVT_TRIGGED;
                        }
                        else
                        {
                            log_event_dim.dim_old_data[0][log_event_dim.logical_dim] = log_event_dim.dim_new_data[0];
                            log_event_dim.dim_old_data[1][log_event_dim.logical_dim] = log_event_dim.dim_new_data[1];
                        }
                    }
                }

                if (end_f)
                {
                    log_event_dim.dim_props_idx = dim_props_idx;
                    return;
                }
            }
            while (log_event_dim.dim_input_mask);
        }

        // Check for new DIMs to log only at the start of each 20ms period

        if (((uint16_t)sta.timestamp_ms.ms % 20) != DEV_STA_TSK_PHASE)
        {
            return;
        }

        // Analyse next DIM

        dim_props_idx++;

        if (dim_props_idx >= diag.n_dims)
        {
            dim_props_idx = 0;
        }

        log_event_dim.logical_dim = ((struct TAccessToDimInfo *) diag.dim_props[dim_props_idx].value)->logical_dim;

        board_number = SysDbDimBusAddress(device.sys.sys_idx, log_event_dim.logical_dim);

        log_event_dim.dim_state = dim_collection.evt_log_state[log_event_dim.logical_dim];

        // Following a TRIG state and until dim_state is changed by the DSP
        if (log_event_dim.dim_state == DIM_EVT_TRIGGED)
        {
            continue;
        }

        if (log_event_dim.dim_state == DIM_EVT_NONE)
        {
            if (board_number < FGC_MAX_DIM_BUS_ADDR)
            {
                diag.data.r.latched_0[QSPI_BRANCH_A][board_number] = 0;
                diag.data.r.latched_1[QSPI_BRANCH_A][board_number] = 0;
            }

            continue;
        }

        dim_idx = SysDbDimDbType(device.sys.sys_idx,log_event_dim.logical_dim);
        log_event_dim.fault_mask   [0] = DimDbDigitalFaults(dim_idx,4);
        log_event_dim.fault_mask   [1] = DimDbDigitalFaults(dim_idx,5);

        log_event_dim.dim_new_data [0] = diag.data.r.digital_0[QSPI_BRANCH_A][board_number];
        log_event_dim.dim_new_data [1] = diag.data.r.digital_1[QSPI_BRANCH_A][board_number];

        if (log_event_dim.dim_state == DIM_EVT_WATCH &&       // if DIM_EVT_WATCH state
            !((log_event_dim.dim_new_data[0] ^ log_event_dim.dim_old_data[0][log_event_dim.logical_dim]) & log_event_dim.fault_mask[0]) &&
            !((log_event_dim.dim_new_data[1] ^ log_event_dim.dim_old_data[1][log_event_dim.logical_dim]) & log_event_dim.fault_mask[1]))
        {
            continue;                           // Nothing changed so continue
        }

        log_event_dim.dim_input_mask = 1;     // Set mask to process DIM
        log_event_dim.dim_input_idx  = 0;     // Start with first input of bank 0
        log_event_dim.dim_bank_idx   = 0;

        memcpy(&log_event_dim.dim_log_rec.prop[4], SYM_TAB_PROP[diag.dim_props[dim_props_idx].sym_idx].key.c,
               ST_MAX_SYM_LEN + 1);

        if (log_event_dim.dim_state == DIM_EVT_TRIG)  // if DIM_EVT_TRIG state
        {

            diag.data.r.latched_0[QSPI_BRANCH_A][board_number] = log_event_dim.dim_old_data[0][log_event_dim.logical_dim] =
                                                                log_event_dim.dim_new_data[0];
            diag.data.r.latched_1[QSPI_BRANCH_A][board_number] = log_event_dim.dim_old_data[1][log_event_dim.logical_dim] =
                                                                log_event_dim.dim_new_data[1];

            log_event_dim.dim_log_rec.timestamp.secs.abs  = dim_collection.trig_s[log_event_dim.logical_dim];
            log_event_dim.dim_log_rec.timestamp.us = dim_collection.trig_us[log_event_dim.logical_dim];
            logEvtentDimInput(0, 0, false);

        }
        else                        // else DIM_EVT_WATCH state
        {
            log_event_dim.dim_log_rec.timestamp = sta.timestamp_us;
        }
    }
    while (log_event_dim.dim_input_mask || dim_props_idx != log_event_dim.dim_props_idx);   // until all DIMS checked
}




// EOF
