//! @file  prop.c
//! @brief Property functions
//!
//! This file contains the functions to manipulate the properties and symbols


// ---------- Includes

#include <prop_class.h>
#include <prop.h>
#include <string.h>
#include <class.h>
#include <fbs.h>
#include <fgc_errs.h>
#include <logSpy.h>
#include <logRun.h>
#include <panic.h>
#include <nvs.h>
#include <os.h>
#include <pars.h>
#include <time_fgc.h>
#include <trm.h>
#include <transaction.h>
#include <function.h>



// ---------- Internal structures, unions and enumerations

struct Prop_init
{
    struct prop * parent_prop[FGC_MAX_PROP_DEPTH];
};



// ---------- External variable definitions

prop_debug_t prop_debug;



// ---------- Internal function definitions

static uint16_t propertyIdentifyGetDataValues(struct cmd * c, struct prop * p)
{
    uint16_t errnum = FGC_OK_NO_RSP;

    while (errnum == FGC_OK_NO_RSP && c->n_token_chars > 0)
    {
        errnum = ParsScanToken(c, "Y, ");

        if (c->n_token_chars > 0)
        {
            if (c->data_len < CMD_MAX_DATA_VALUES)
            {
                strncpy(c->data[c->data_len], c->token, c->n_token_chars + 1);
            }

            c->data_len++;
        }
    }

    if (c->data_len > CMD_MAX_DATA_VALUES)
    {
        errnum = FGC_BAD_GET_OPT;
    }

    return errnum;
}



//! This function to link each property to its parent and propagate the
//! DF_TIMESTAMP_SELECT flag to children

static void propInitParentLinksSet(uint16_t lvl, struct prop * p)
{
    static struct Prop_init prop_init;

    if (lvl > 0)
    {
        p->parent = prop_init.parent_prop[lvl - 1];

        if (testBitmap(p->parent->dynflags, DF_TIMESTAMP_SELECT))
        {
            setBitmap(p->dynflags, DF_TIMESTAMP_SELECT);
        }
    }
    else
    {
        p->parent = NULL;
    }

    if (p->type == PT_PARENT)
    {
        prop_init.parent_prop[lvl] = p;
    }
}



//! This function is called for PARENT and child properties. It is RECURSIVE
//!  and scans the property tree to:
//!   - Set the PPM flag for all properties that have a PPM child property.
//!   - Set the flag is_setting if any child property is settable.
//!   - Count the potential size of data produced by the property using a
//!     get function. This is used to decide if the publication can be direct
//!     or must be via a GET request to the gateway.

static uint32_t propInitPPMFlagAndPubSize(struct prop * p)
{
    uint16_t idx;
    uint32_t max_size = 0;

    if (p->type == PT_PARENT)
    {
        // Make sure the property doesn't loop back to top level

        if (p->value != &PROPS)
        {
            for (idx = 0 ; idx < p->max_elements ; idx++)
            {
                max_size += propInitPPMFlagAndPubSize((struct prop *)p->value + idx);
            }
        }
    }
    else
    {
        if (p->get_func_idx != GET_NONE)
        {
            // Calculate the maximum size in bytes

            max_size = p->max_elements * prop_type_size[p->type];
        }

        if (p->set_func_idx != SET_NONE)
        {
            p->is_setting         = true;
            p->parent->is_setting = true;
        }
    }

    // Report size 0 to parent if the property is hidden

    if (testBitmap(p->flags, PF_HIDE))
    {
        max_size = 0;
    }

    if (p->parent != NULL && testBitmap(p->flags, PF_PPM) && max_size != 0)
    {
        setBitmap(p->parent->flags, PF_PPM);
    }

    return (max_size);
}



//! This function is RECURSIVE and called by PropMap() only.

static void PropMapRecurse(uint16_t lvl, struct prop * p, prop_map_func func, enum prop_map_specifier specifier)
{
    uint16_t idx;

    if (p->type == PT_PARENT)                           // If PARENT property
    {
        if (specifier == PROP_MAP_ALL || specifier == PROP_MAP_ONLY_PARENTS)
        {
            func(lvl, p);
        }

        if (p->value != &PROPS)     // Avoid a loopback to upper level on property such as CONFIG.SET
        {
            for (idx = 0; idx < p->max_elements; idx++) // For each CHILD property
            {
                PropMapRecurse(lvl + 1, &((struct prop *) p->value)[idx], func, specifier);
            }
        }
    }
    else                                                // If CHILD property
    {
        if (specifier == PROP_MAP_ALL || specifier == PROP_MAP_ONLY_CHILDREN)
        {
            func(lvl, p);
        }
    }
}



static uint16_t PropIdentifyTransactionId(struct cmd * c, struct prop * p)
{
    c->transaction_id = 0;

    // If available, retrieve the transaction ID. It is encapulsated between <>.

    if (c->token_delim != '<')
    {
        return FGC_OK_NO_RSP;
    }

    uint32_t transaction_id = 0;

    if (ParsScanInteger(c, "N> ", &transaction_id) != FGC_OK_NO_RSP)
    {
        return FGC_INVALID_TRANSACTION_ID;
    }

    char ch;

    CmdNextCh(c, &ch);

    if (ch && ch != ' ' && ch != '(' && ch != '[')
    {
        return FGC_SYNTAX_ERROR;
    }

    c->token_delim = ch;

    uint16_t errnum;

    if ((errnum = transactionValidateId(transaction_id)) != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    if (transaction_id != 0 && testBitmap(p->flags, PF_TRANSACTIONAL) == false)
    {
        return FGC_PROPERTY_NOT_TRANSACTIONAL;
    }

    c->transaction_id = transaction_id;

    return FGC_OK_NO_RSP;
}


// ---------- External function definitions

uint16_t PropFind(const char * propname)
{
    pcm.end_cmd_f = FALSE;

    // BE CAREFUL: Here pcm.prop_buf is used as a char pointer (to the property name) so that the parsing
    // functions can be used thanks to pcm's cmd structure.

    pcm.prop_buf  = (struct Dpcom_prop_buf *)propname;        // pcm.prop_buf is later used as a char* in CmdNextCh

    return (PropIdentifyProperty(&pcm));
}



uint16_t PropIdentifyProperty(struct cmd * c)
{
    uint16_t              errnum;         // Status return from ST_MAX_SYM_LEN() or PropIdentifyArray()
    uint16_t              sym_idx;        // Symbol index returned from ST_MAX_SYM_LEN()
    prop_size_t         n_els;          // Number of property elements
    uint32_t              sub_sel;        // Sub-device selector
    struct prop    *    p;              // Pointer to property
    struct prop    *    sub_p;          // Pointer to sub property

    // Process sub-device index if present

    errnum = ParsScanToken(c, "Y.:<([ ");

    if (errnum != 0)                                            // If token is bad
    {
        return (errnum);                                        // Report status
    }

    if (c->token_delim == ':')                                  // If sub_dev index supplied
    {

        errnum = ParsScanInteger(c, 0, &sub_sel);           // Parse index

        if (errnum != 0)
        {
            return (errnum);
        }

        if (sub_sel >= FGC_MAX_SUB_DEVS)      // Check limits
        {
            return (FGC_INVALID_SUBDEV);
        }

        errnum = ParsScanToken(c, "Y.<([ ");             // If next token is bad

        if (errnum != 0)                                // If next token is bad
        {
            return (errnum);                            // Report status
        }
    }
    else
    {
        sub_sel = 0;                                // No sub-dev index supplied
    }

    // Process property name

    errnum = PropIdentifySymbol(c, 0, ST_TLPROP, &sym_idx); // Try to identify top level property

    if (errnum != 0)
    {
        return (errnum);                                // Report failure if search failed
    }

    p = (struct prop *) SYM_TAB_PROP[sym_idx].value;    // Get pointer to top level property

    c->sym_idxs[0] = sym_idx;                           // Store symbol for top level property
    c->si_idx = 1;

    while ((p->type == PT_PARENT)                       // while property contains further properties and
           && (c->token_delim == '.'))                 // delimiter is .
    {
        errnum = PropIdentifySymbol(c, "Y.<([ ", ST_LLPROP, &sym_idx); // Try to identify low level prop sym

        if (errnum != 0)
        {
            return (errnum);                    // Report failure if search failed
        }

        sub_p = p->value;                       // Get pointer to sub property array
        n_els = PropGetNumEls(c, p);            // and number of sub properties

        if (n_els)                              // If there are children
        {
            while (sym_idx != sub_p->sym_idx && // While symbol doesn't match sub property
                   --n_els)                    // and not all sub properties have been checked
            {
                sub_p++;                                // Try next sub property
            }
        }

        if (!n_els)                                     // If no match was found
        {
            return (FGC_UNKNOWN_SYM);                   // Report UNKNOWN SYMBOL
        }

        c->sym_idxs[c->si_idx++] = sym_idx;             // Store sub property symbol
        p = sub_p;                                      // Move down to sub property
    }

    if (c->token_delim == '.')                          // If delimiter was still '.'
    {
        return (FGC_UNKNOWN_SYM);                       // Report UNKNOWN SYMBOL
    }

    c->prop = p;                        // Save property address in command structure
    c->n_prop_name_lvls = c->si_idx;    // Remember number of levels in property name

    memset(&c->sym_idxs[c->si_idx], 0, FGC_MAX_PROP_DEPTH - c->si_idx); // Clear rest of symbols stack

    if (!c->pars_buf)                                   // If pcm (used for PropFind())
    {
        return (0);                                     // Return immediately
    }

    // Continue for Serial or FIP commands

    c->header = c->pars_buf->pkt[c->pkt_buf_idx].header_flags;
    c->cmd_type = c->header & FGC_FIELDBUS_FLAGS_CMD_TYPE_MASK;

    // Set the last property if it as leaf so that the correct label can be appended in cmd.c::CmdPrintLabel()

    c->last_prop = (p->type != PT_PARENT ? p  : NULL);

    // Check if the sub_sel is valid.

    if (sub_sel != NON_MULTI_PPM_SUB_DEV)
    {
        if (dpcom.mcu.device_multi_ppm == FGC_CTRL_DISABLED)
        {
            return FGC_INVALID_SUBDEV;
        }

        // Collapse the sub-device selector if the child property is non multi-PPM
        // or a property is an acquisition (SET function not defined), which are
        // never multi-PPM by definition.

        if (p->type != PT_PARENT)
        {
            if (p->set_func_idx == SET_NONE || testBitmap(p->flags, PF_SUB_DEV) == false)
            {
                sub_sel = NON_MULTI_PPM_SUB_DEV;
            }
        }
    }

    c->sub_sel = sub_sel;

    // Process transaction ID

    errnum = PropIdentifyTransactionId(c, p);

    if (errnum != FGC_OK_NO_RSP)
    {
        return (errnum);
    }

    // Process user index and array indexes if present

    errnum = PropIdentifyUser(c, c->pars_buf->pkt[c->pkt_buf_idx].header_user);

    if (errnum != FGC_OK_NO_RSP)
    {
        return (errnum);
    }

    if (testBitmap(p->flags, PF_TRANSACTIONAL) == true)
    {
        errnum = transactionAccessCheck(c->cmd_type,
                                        c->transaction_id,
                                        c->sub_sel,
                                        c->cyc_sel,
                                        c->prop);

        if (errnum != FGC_OK_NO_RSP)
        {
            return (errnum);
        }
    }

    errnum = PropIdentifyArray(c);

    return errnum;              // Analyse array specifier if supplied
}



uint16_t PropIdentifySymbol(struct cmd * c, const char * delim, uint16_t sym_type, uint16_t * sym)
{
    uint16_t errnum;               // Return status
    uint16_t n_ch;                 // Number of chars in the token
    uint16_t sym_idx = 0;          // Symbol index (zero means 'symbol not found')

    // Warning!
    // in short circuit evaluation if delim == 0 then ParsScanToken() is not evaluated
    // but if complete evaluation compiler switch is used then ParsScanToken() wiil be evaluated always
    if (delim && (errnum = ParsScanToken(c, delim))) // If delimiter supplied and token is bad
    {
        return (errnum);                        // Report status
    }

    n_ch = c->n_token_chars;

    if (!n_ch)                                  // If token buffer is empty
    {
        return (FGC_NO_SYMBOL);                 // Report NO_SYMBOL
    }

    if (n_ch > ST_MAX_SYM_LEN)                  // If token is too long
    {
        return (FGC_UNKNOWN_SYM);               // Report UNKNOWN SYM
    }

    switch (sym_type)
    {
        case ST_TLPROP:

            sym_idx = hashFindProp(c->token);

            if (sym_idx && !SYM_TAB_PROP[sym_idx].value)        // If symbol is a low level property
            {
                sym_idx = 0;                                    // Clear symbol index to indicate not found
            }

            break;

        case ST_LLPROP:

            sym_idx = hashFindProp(c->token);
            break;

        case ST_CONST:

            sym_idx = hashFindConst(c->token);
            break;

        case ST_GETOPT:

            sym_idx = hashFindGetopt(c->token);
            break;
    }

    if (!sym_idx)                               // If no match made...
    {
        return (FGC_UNKNOWN_SYM);               // Report unknown symbol
    }

    *sym = sym_idx;                             // Pass symbol index to calling function

    return (0);                                 // Return OK status
}



uint16_t PropIdentifyUser(struct cmd * c, uint16_t user_from_header)
{
    char ch;                            // first character after array specifier
    uint32_t value = 0;                   // Long integer value for ScanInteger
    struct prop * p = c->prop;          // Local copy of pointer property

    if (c->token_delim == '(')          // If ASCII user index specifier is included
    {
        if (c->cyc_sel)                 // If sub device index already supplied
        {
            return (FGC_NOT_IMPL);      // report not implemented
        }

        switch (ParsScanInteger(c, "N)", &value))               // scan (delim = ')') for an integer value
        {
            case FGC_NO_SYMBOL:                                 // No value

                if ((c->cmd_type == FGC_FIELDBUS_FLAGS_GET_CMD) // if command is GET and
                    && (p->type != PT_PARENT)               // not a parent and
                    && testBitmap(p->flags, PF_PPM))               // PPM property
                {
                    if (   dpcom.mcu.device_ppm == FGC_CTRL_ENABLED
                        || p->set_func_idx      == SET_NONE)
                    {
                        value = PPM_ALL_USERS;                      // Signal get all users
                    }
                }

                break;

            case 0:                                     // Valid integer

                break;                                  // Use value

            default:                                    // Invalid integer

                return (FGC_BAD_CYCLE_SELECTOR);                  // Report BAD USER
        }

        CmdNextCh(c, &ch);                              // Take next char from cmd pkt

        if (ch && ch != ' ' && ch != '[')               // If not zero, space or [
        {
            return (FGC_SYNTAX_ERROR);                  // Report SYNTAX_ERROR
        }

        c->token_delim = ch;
    }

    // If user supplied in command packet header (for SUB_GET), then use it unless sub_dev index supplied

    if (user_from_header)
    {
        if (c->cyc_sel && c->cyc_sel != user_from_header) // The user ID can be redundant but the user in the packet
            // header should match the user in the cmd.
        {
            return (FGC_BAD_CYCLE_SELECTOR);
        }

        value = user_from_header;
    }

    // Check that value is within valid range

    if (   (dpcom.mcu.device_ppm   == FGC_CTRL_ENABLED  && value > FGC_MAX_USER        && value != PPM_ALL_USERS)
        || (dpcom.mcu.device_ppm   == FGC_CTRL_DISABLED && p->set_func_idx != SET_NONE && value  > 0            )
        || (testBitmap(p->flags, PF_PPM) == false             && value > 0                                            ))
    {
        return (FGC_BAD_CYCLE_SELECTOR);
    }

    // Condition to decide if a non-zero User can be accepted for this command/property

    if ((dpcom.mcu.device_ppm == FGC_CTRL_ENABLED || p->set_func_idx == SET_NONE)
        && ((c->cmd_type == FGC_FIELDBUS_FLAGS_SUB_CMD)
            || (c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD && testBitmap(p->flags, PF_PPM))
            || ((c->cmd_type == FGC_FIELDBUS_FLAGS_GET_CMD || c->cmd_type == FGC_FIELDBUS_FLAGS_GET_SUB_CMD)
                && (p->type == PT_PARENT || testBitmap(p->flags, PF_PPM))
               )
           ))
    {
        c->cyc_sel = (uint16_t)value;
    }

    return (0);                                            // Report success
}



uint16_t PropIdentifyArray(struct cmd * c)
{
    char ch;                            // first character after array specifier
    uint32_t value;                       // Unsigned long value (from ScanInteger)
    struct prop * p = c->prop;          // Local copy of pointer property

    // Set default values

    c->n_arr_spec = 0;                  // Report no specifiers
    c->from = 0;                        // Set default array range
    c->to = p->max_elements - 1;
    c->step = 1;                        // Set default step to 1

    // FROM

    if (c->token_delim != '[')          // If no array specifiers
    {
        c->n_token_chars = 0;           // Cancel token
        return (0);
    }

    if ((p->type == PT_PARENT)                          // If property is a PARENT
        && (c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD))     // and it is a set command
    {
        return (FGC_BAD_ARRAY_IDX);                     // report BAD ARRAY INDEX
    }

    switch (ParsScanInteger(c, "N,]", &value))          // Switch on result of scan for 1st integer
    {
        case FGC_NO_SYMBOL:                             // Token buffer is empty

            break;

        case 0:                                         // Valid integer

            if (testBitmap(value, ~PROP_ARRAY_IDX_MASK) ||    // If idx is out of possible range, or
                (p->type != PT_PARENT &&            // (Not a parent, and
                value >= p->max_elements)) //  longer than max elements)
            {
                return (FGC_BAD_ARRAY_IDX);             // Report BAD ARRAY INDEX
            }

            c->from = value;                   // Save new from value
            c->to = c->from;                            // Pass index to calling function
            c->n_arr_spec = 2;
            break;

        default:                                        // Invalid number

            return (FGC_BAD_ARRAY_IDX);                 // Report BAD ARRAY INDEX
    }

    // TO

    if (c->token_delim == ',')                          // If a second index is expected
    {
        switch (ParsScanInteger(c, "N,]", &value))      // Switch on result of scan for 2nd integer
        {
            case FGC_NO_SYMBOL:                         // Token buffer is empty

                c->n_arr_spec = 1;
                c->to = p->max_elements - 1;            // to = max_els-1
                break;

            case 0:                                     // Valid integer

                if (testBitmap(value, ~PROP_ARRAY_IDX_MASK) || // If idx is out of possible range, or
                    (p->type != PT_PARENT &&        // (property is not a PARENT and
                     value >= p->max_elements) || //  'to' is beyond the end of array) or
                     value < c->from)       // 'to' index is less than from index
                {
                    return (FGC_BAD_ARRAY_IDX);         // Report BAD ARRAY INDEX
                }

                c->to = value;                 // Save new to value
                c->n_arr_spec = 2;
                break;

            default:                                    // Invalid number

                return (FGC_BAD_ARRAY_IDX);             // Report BAD ARRAY INDEX
        }
    }

    // STEP

    if (c->token_delim == ',')                          // If a step is expected
    {
        switch (ParsScanInteger(c, "N]", &value))       // Switch on result of scan for 3rd integer
        {
            case FGC_NO_SYMBOL:                         // Token buffer is empty

                break;                                  // Keep default value of 1

            case 0:                                     // Valid integer

                if (testBitmap(value, ~PROP_ARRAY_IDX_MASK) || !value // if step is out of possible range, or
                    || ((value > 1)        // step > 1, and
                        && ((c->cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD) // (Set command, or
                            || (p->type == PT_PARENT))      //  property is a PARENT)
                       ))
                {
                    return (FGC_BAD_ARRAY_IDX);         // Report BAD ARRAY INDEX
                }

                // Clip step to max_elements

                c->step = (value < p->max_elements ? value : p->max_elements);

                break;

            default:                                    // Invalid number

                return (FGC_BAD_ARRAY_IDX);             // Report BAD ARRAY INDEX
        }
    }

    CmdNextCh(c, &ch); // Take next char from cmd pkt (it is safe to ignore errnum)

    if (ch && ch != ' ')                                // If not zero or a space
    {
        return (FGC_SYNTAX_ERROR);                      // Report SYNTAX_ERROR
    }

    c->n_token_chars = 0;                               // Cancel token
    return (0);                                         // Return success
}



uint16_t PropIdentifyGetOptions(struct cmd * c, struct prop * p)
{
    uint16_t      sym;
    uint16_t      errnum;
    uint16_t      flag;
    uint16_t      flags;

    errnum = 0;
    flags  = 0;
    flag   = 0;

    c->data_len = 0;

    while (errnum == FGC_OK_NO_RSP)
    {
        errnum = PropIdentifySymbol(c, "Y| ", ST_GETOPT, &sym);

        if (errnum == FGC_OK_NO_RSP)
        {
            flag = sym_tab_getopt[sym].value;

            // Do not allow for duplicated get options

            if (testBitmap(flags, flag))
            {
                return (FGC_BAD_GET_OPT);
            }

            setBitmap(flags, flag);

            if (flag == GET_OPT_DATA)
            {
                // Retrieve the DATA values

                errnum = propertyIdentifyGetDataValues(c, p);

                if (errnum != FGC_OK_NO_RSP)
                {
                    return errnum;
                }
            }
        }
        else if (errnum != FGC_NO_SYMBOL)
        {
            return errnum;
        }
    }

    setBitmap(flags, flag);

    if (testAllBitmap(flags, GET_OPT_IDX | GET_OPT_NOIDX) ||  // If index options contradict (both are set)
        (testBitmap(flags, GET_OPT_BIN) &&                // Or, if invalid BIN option
         (c == &tcm || p->type == PT_PARENT)))
    {
        return (FGC_BAD_GET_OPT);                       // Report BAD GET OPTION
    }

    // Set token delimiter: Space forces NEAT for terminal command, Comma is standard for FIP cmds or direct

    c->token_delim = (c == &tcm && terminal.mode == TRM_EDITOR_MODE ? ' ' : ',');

    c->getopts = flags;                                 // Return get option flags in command structure

    return FGC_OK_NO_RSP;                                         // Return OK status
}



prop_size_t PropGetNumEls(struct cmd * c, struct prop const * p)
{
    if (testBitmap(p->flags, PF_DSP_FGC))             // If DSP property
    {
        if (c)                                  // If command structure provided
        {
            return (c->n_elements);             // Return cached length
        }

        return (0);                             // Unknown so return 0
    }

    if (testBitmap(p->flags, PF_INDIRECT_N_ELS))      // MCU Property : if indirect
    {
        if (c)
        {
            return (((prop_size_t *) p->n_elements)[c->cyc_sel]);
        }

        return (*((prop_size_t *) p->n_elements));
    }

    return ((prop_size_t) p->n_elements);
}



void PropSetNumEls(struct cmd * c, struct prop * p, prop_size_t n_elements)
{
    if (testBitmap(p->flags, PF_DSP_FGC))             // If attempt to set n_elements for a DSP property
    {
        panicCrash(0xBAD3);                          // Crash to generate a run log dump
    }

    if (testBitmap(p->flags, PF_INDIRECT_N_ELS))
    {
        if (c)
        {
            ((prop_size_t *)p->n_elements)[c->cyc_sel] = n_elements;
        }
        else
        {
            *((prop_size_t *)p->n_elements)            = n_elements;
        }
    }
    else
    {
        p->n_elements = (uintptr_t)n_elements;
    }
}



void PropSet(struct cmd * c, struct prop * p, const void * value, prop_size_t n_elements)
{
    if ((prop_type_size[p->type] * n_elements) > PROP_BLK_SIZE)
    {
        panicCrash(0xBAD2);
    }

    PropBufWait(c);                             // Wait for property block buffer to be free

    c->cached_prop  = p;                        // Prepare command structure variables
    c->prop_blk_idx = 0;

    // Copy data to block buffer, from supplied value
    memcpy(&c->prop_buf->blk, value, prop_type_size[p->type] * n_elements);

    PropBlkSet(c, n_elements, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT);
}



void PropSetFar(struct cmd * c, struct prop * p, const void * value, prop_size_t n_elements)
{
    uint16_t      n_blocks;                       // Number of property transfer blocks
    uint16_t      remaining_w;                    // Number of words remaining to copy
    uint16_t      errnum = FGC_OK_NO_RSP;

    c->cached_prop  = p;                        // Prepare command structure variables
    c->prop_blk_idx = 0;
    c->n_elements   = 0;                        // TODO to be reviewed

    if (n_elements == 0)
    {
        remaining_w = 0;
        n_blocks    = 1;
    }
    else
    {
        remaining_w = (n_elements * prop_type_size[p->type] + 1) / 2;
        n_blocks    = (remaining_w + PROP_BLK_SIZE_W - 1) / PROP_BLK_SIZE_W;
    }

    while (n_blocks--)
    {
        if (testBitmap(p->flags, PF_DSP_FGC))
        {
            errnum = PropBufWait(c);

            if (errnum != FGC_OK_NO_RSP)
            {
                return;
            }
        }

        // Copy data to property buffer

        memcpy(c->prop_buf->blk.intu, value, (n_blocks ? PROP_BLK_SIZE_W : remaining_w) * sizeof(uint16_t));

        errnum = PropBlkSet(c, n_elements, TRUE, (n_blocks ? 0 : FGC_FIELDBUS_FLAGS_LAST_PKT));

        if (errnum)
        {
            logRunAddTimestamp();
            logRunAddEntry(FGC_RL_PROP_ERR, &errnum);
            return;
        }

        value  = (uint16_t *) value + PROP_BLK_SIZE_W;
        remaining_w -= PROP_BLK_SIZE_W;
        c->prop_blk_idx++;
    }
}



uint16_t PropValueGet(struct cmd * c, struct prop * p, bool * set_f, prop_size_t el_idx, uint8_t ** data_ptr)
{
    uint32_t      start_byte;
    uint16_t      blk_idx;
    uint16_t      errnum;

    start_byte = (uint32_t) el_idx * (uint32_t) prop_type_size[p->type]; // Calculate start byte for element in value
    blk_idx = start_byte / PROP_BLK_SIZE;    // Calculate block index containing the element

    if (testBitmap(p->flags, PF_SUB_DEV) == false)
    {
        c->sub_sel = 0;
    }

    if (c->cached_prop != p || c->prop_blk_idx != blk_idx)      // If required block is NOT already in buffer
    {
        if (set_f && *set_f)                                    // If pointer to set flag provided and set
        {
            errnum = PropBlkSet(c, 0, TRUE, 0);                 // Set current block, 0=!last blk

            if (errnum)
            {
                return (errnum);
            }

            *set_f = FALSE;                                     // Clear set flag
        }

        c->cached_prop = p;                                     // Get new block
        c->prop_blk_idx = blk_idx;
        errnum = PropBlkGet(c, &(c->n_elements));

        if (errnum)
        {
            return (errnum);
        }
    }

    if (data_ptr != NULL)
    {
        *data_ptr = (uint8_t *) &c->prop_buf->blk + (start_byte & (PROP_BLK_SIZE -
                                                                 1)); // Return pointer to required element
    }

    return (FGC_OK_NO_RSP);
}



uint16_t PropBlkGet(struct cmd * c, prop_size_t * n_elems_ptr)
{
    uint32_t              n_bytes;
    uint32_t              value_addr;
    uint32_t              el_offset;
    uint32_t              offset;                         // byte offset to start of block in the property
    struct prop    *    p = c->cached_prop;
    struct Dpcom_prop_buf  *  pbuf = c->prop_buf;
    uint8_t       *       value;
    uint16_t              dsp_status;

    PropBufWait(c);                                     // Wait for property block buffer to be free

    if (testBitmap(p->flags, PF_DSP_FGC))                     // If FGC DSP property
    {
        pbuf->dsp_prop_idx = p->dsp_idx;                // Set DSP property index
        pbuf->blk_idx      = c->prop_blk_idx;           // Set block index of required block
        pbuf->sub_sel      = c->sub_sel;                // Set the sub-device selector
        pbuf->cyc_sel      = c->cyc_sel;                // Set cycle selector
        pbuf->action       = FGC_FIELDBUS_FLAGS_GET_CMD;      // Set GET action to trigger DSP PropComms()
        dsp_status         = PropBufWait(c);            // Wait for DSP to respond

        if (dsp_status != FGC_OK_NO_RSP)
        {
            return (dsp_status);
        }

        if (pbuf->dsp_rsp != PROP_DSP_RSP_OK)
        {
            return (FGC_DSP_PROPERTY_ISSUE);
        }
    }
    else                                                // else MCU property
    {
        n_bytes    = prop_type_size[p->type] * p->max_elements;         // Size of property (for one cyc_sel)
        el_offset  = c->sub_sel * FGC_MAX_USER_PLUS_1 + c->cyc_sel;     // Element offset taking into account the sub_sel and cyc_sel
        offset     = c->prop_blk_idx * PROP_BLK_SIZE;                   // Byte offset to start of block

        value_addr = (uint32_t)p->value + el_offset * n_bytes;            // Address for data for cyc_sel
        value      = (uint8_t *) value_addr + offset;                     // Start of block
        n_bytes   -= offset;                                            // Number of bytes to end of property

        if (n_bytes > PROP_BLK_SIZE)                                    // Clip to size of one block
        {
            n_bytes = PROP_BLK_SIZE;
        }

        if(p->type == PT_DEV_NAME)
        {
            memcpy(&pbuf->blk, *(char **)value_addr, n_bytes);               // Copy data to block buffer
        }
        else
        {
            memcpy(&pbuf->blk, value, n_bytes);               // Copy data to block buffer
        }

        if (n_bytes & 1)                                                // If odd number of bytes
        {
            ((uint8_t *) &pbuf->blk)[n_bytes] = 0;                        // Pad zero to word boundary
        }

        pbuf->n_elements = PropGetNumEls(c, p);
    }

    if (n_elems_ptr != NULL)
    {
        *n_elems_ptr = (prop_size_t) pbuf->n_elements;                  // Return number of elements in the property
    }

    return (FGC_OK_NO_RSP);
}



uint16_t PropBlkSet(struct cmd * c, prop_size_t n_elements, bool set_f, uint16_t last_blk_f)
{
    uint16_t                el_size;
    int16_t                 n_bytes;                // Crash if n_bytes turns out to be negative
    uint32_t                offset;
    prop_size_t             n_els_old;
    uint32_t                value_addr;
    uint8_t               * value_p;
    struct prop           * p    = c->cached_prop;
    struct Dpcom_prop_buf * pbuf = c->prop_buf;

    el_size = prop_type_size[p->type];

    if (el_size == 0)           // Would cause a division by zero later on
    {
        return (FGC_NULL_ADDRESS);
    }

    if (!last_blk_f)
    {
        // NB: Integer multiplication must be done before the division

        n_elements = ((c->prop_blk_idx + 1) * (prop_size_t) PROP_BLK_SIZE) / el_size;
    }

    if (testBitmap(p->flags, PF_DSP_FGC))                             // If FGC DSP property
    {
        if (set_f || last_blk_f)                                // If set required, or last_block
        {
            pbuf->dsp_prop_idx = p->dsp_idx;                    // Set DSP property index
            pbuf->blk_idx      = c->prop_blk_idx;                    // Set block index of block to be set
            pbuf->sub_sel      = c->sub_sel;
            pbuf->cyc_sel      = c->cyc_sel;
            pbuf->n_elements   = n_elements;                      // Set new n_els in property

            if (set_f)                                          // If set flag
            {
                pbuf->action = FGC_FIELDBUS_FLAGS_SET_CMD | last_blk_f; // Trig DSP to update contents and length
            }
            else                                                // else
            {
                pbuf->action = last_blk_f;                      // Trig DSP to update length only
            }

            PropBufWait(c);
        }
    }
    else                                                        // else MCU property
    {
        uint32_t el_offset;

        offset = c->prop_blk_idx * PROP_BLK_SIZE;               // Byte offset to start of block

        el_offset  = c->sub_sel * FGC_MAX_USER_PLUS_1 + c->cyc_sel;

        value_addr = (uint32_t) p->value + el_offset * p->max_elements * el_size;

        value_p = (uint8_t *) value_addr + offset;                // Start of block in buffer

        if (set_f)                                              // If set required
        {
            n_bytes = el_size * n_elements - offset;            // Num of bytes from start of block

            if (n_bytes < 0)
            {
                panicCrash(0xBAD4);
            }

            if (n_bytes > PROP_BLK_SIZE)                        // Clip to size of one block
            {
                n_bytes = PROP_BLK_SIZE;
            }

            if(p->type == PT_DEV_NAME)
            {
                memcpy(*(char **)value_addr, &pbuf->blk, n_bytes); // Copy data from block buffer
            }
            else
            {
                memcpy(value_p, &pbuf->blk, n_bytes); // Copy data from block buffer
            }
        }

        n_els_old = PropGetNumEls(c, p);

        // If last block and data has shrunk

        if (last_blk_f && n_elements < n_els_old)
        {
            // Clear to zero all trailing elements in property

            if(p->type == PT_DEV_NAME)
            {
                memset(*(((char **)p->value) + n_elements), 0, (n_els_old - n_elements) * el_size);
            }
            else
            {
                memset((void *)(value_addr + n_elements * el_size), 0, (n_els_old - n_elements) * el_size);
            }
        }

        if (last_blk_f ||                                       // If last block, or
            n_elements > n_els_old)                         // property has grown
        {
            PropSetNumEls(c, p, n_elements);                    // Set property length
        }
    }

    // If write_nvs_f == TRUE, and property is non-volatile

    if (c->write_nvs_f == true && testBitmap(p->flags, PF_NON_VOLATILE) == true)
    {
        // Save property data in NVS

        nvsStoreBlockForOneProperty(c, p, (uint8_t *) &pbuf->blk, c->sub_sel, c->cyc_sel, n_elements, (last_blk_f != 0));
    }

    return (FGC_OK_NO_RSP);
}



uint16_t PropBufWait(struct cmd * c)
{
    uint32_t start;
    uint16_t retval;

    // Wait until the buffer is no longer in use or
    // the DSP is not responding (5 second time-out)

    start = timeGetUs();

    while (!dpcom.mcu.dsp_in_standalone &&
           c->prop_buf->action          &&
           (timeGetUsDiffNow(start)) <= 5000000U)
    {
        // Resumed by msTask() on next millisecond

        OSTskSuspend();
    }

    if (c->prop_buf->action == 0)
    {
        retval = FGC_OK_NO_RSP;
    }
    else
    {
        // Only notify if the DSP is not in stand-alone

        if (!dpcom.mcu.dsp_in_standalone)
        {
            prop_debug.access_timeouts++;
            logRunAddTimestamp();
            logRunAddEntry(FGC_RL_PROP_TIMEOUT, &c->prop->dsp_idx);
        }

        // Reset the buffer for further use.

        c->prop_buf->action = 0;

        retval = FGC_DSP_NOT_AVL;
    }

    return (retval);
}



void PropMap(prop_map_func func, enum prop_map_specifier specifier)
{
    uint16_t idx;

    for (idx = 0; idx < FGC_N_TLPS; idx++)                 // For each higher level PARENT property
    {
        PropMapRecurse(0, &((struct prop *) &PROPS)[idx], func, specifier);
    }
}



void propInitSpyMpx(struct cmd * c)
{
    PropSet(c, &PROP_SPY_MPX, prop_class_init_spy_mpx, FGC_N_SPY_CHANS);
}



void propInitPropertyTree(void)
{
    uint16_t idx;

    struct Prop_class_init_dynflag_timestamp_select * property_init_info = &prop_class_init_dynflag_timestamp_select[0];

    // Initialise the timestamp dynamic flag

    while (property_init_info->prop)
    {
        setBitmap(property_init_info->prop->dynflags, DF_TIMESTAMP_SELECT);
        property_init_info++;
    }

    // Link each property to its parent and propagate
    // the DF_TIMESTAMP_SELECT flag to children

    PropMap(propInitParentLinksSet, PROP_MAP_ALL);

    // The rest of the procedure to initialise the properties is too complex to be handled with a PropMap,
    // so one needs to manually recurse on all properties.

    for (idx = 0; idx < FGC_N_TLPS; idx++)
    {
        propInitPPMFlagAndPubSize(&((struct prop *)&PROPS)[idx]);
    }
}


// EOF
