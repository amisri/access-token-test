//! @file  pub.c
//! @brief This module manages the publication of subscriptions


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <pub.h>
#include <bitmap.h>
#include <defprops.h>
#include <extendMask.h>
#include <fbs.h>
#include <fgc_errs.h>
#include <mcu.h>
#include <mem.h>
#include <logRun.h>
#include <taskTrace.h>



// ---------- Constants

#define PUB_SUB_INIT                 0                            //!< Signal to pubInitSubs() to init only
#define PUB_SUB_REINIT               1                            //!< Signal to pubInitSubs() to re-init
#define PUB_SUB_INDEX_LEN            ((FGC_MAX_SUBS + 15) / 16)   //!< Number of 16 bit index masks
#define PUB_SUB_USER_LEN             (FGC_MAX_USER / 16 + 1)      //!< Number of 16 bit index masks
#define PUB_ALL_SUB_DEVICES          0xFF                         //!< Publish all sub-devices. Use for the first update.



// ---------- Internal structures, unions and enumerations

//! Index for subscriptions (publish and cancel)

struct pub_sub_idx
{
    uint16_t                    master;                           //!< Top index mask for the mask array
    uint16_t                    mask_array[PUB_SUB_INDEX_LEN];    //!< Array of masks that must follow 'master'
};

//! Index of cycle selectors for one subscription (publish and get pending)

struct pub_cyc_sel_idx
{
    uint16_t                    master;                           //!< Top index mask for the mask array
    uint16_t                    mask_array[PUB_SUB_USER_LEN];     //!< Array of masks that must follow 'master'
};

//! Subscription table

struct pub_sub_table
{
    struct pub_cyc_sel_idx      sub_notify_msk[FGC_MAX_SUB_DEVS]; //!< Mask of publication notifications for spcecific cycle selectors
    struct pub_cyc_sel_idx      set_f_msk[FGC_MAX_SUB_DEVS];      //!< Mask of publications triggered by a set command
    struct prop               * prop;                             //!< Pointer to subscribed property
    uint8_t                     sub_idx;                          //!< Next free index when not in use, subscription index when in use
    uint8_t                     notify_count;                     //!< Notification counter (number of cycle selectors pending)

};

//! Structure containin variables for the subscription bookeeping of one sub-device.

struct pub
{
    struct pub_sub_table        table[FGC_MAX_SUBS];              //!< Subscription tables
    struct pub_sub_idx          new_sub_msk;                      //!< Mask of new subscriptions
    struct pub_sub_idx          sub_acked_msk;                    //!< Mask of subscriptions acknowledge by the gateway that need first update
    struct pub_sub_idx          sub_notify_msk;                   //!< Mask of subscriptions with publish notification
    uint16_t                    n_subs;                           //!< Number of subscriptions
    uint16_t                    n_pubs;                           //!< Number of pending publications
    uint8_t                     free_sub_idx;                     //!< Free list for subscription table entries
    uint8_t                     new_sub_idx;                      //!< New subscription pending to be sent to the gateway
    uint8_t                     pub_idx;                          //!< Current publication subscription index
};



// ---------- External variable definitions

struct pub_sema  pub_sema;



// ---------- Internal variable definitions

static struct pub __attribute__((section("sram"))) pub;



// ---------- Internal function definitions

//! Checks if at least one child property is multi-PPM

static bool pubCheckCycSel(struct prop const * prop)
{
    if (GET_FUNC[prop->get_func_idx] != GetParent)
    {
        return testBitmap(prop->flags, PF_PPM);
    }

    // Parent property, traverse all children

    struct prop const * childProp = prop->value;
    uint16_t            idx;
    bool                retval = false;

    for (idx = 0 ; idx < prop->n_elements ; idx++, childProp++)
    {
        retval |= pubCheckCycSel(childProp);
    }

    return retval;
}


//! Checks if at least one child property is multi-PPM

static bool pubCheckSubDev(struct prop const * prop)
{
    if (GET_FUNC[prop->get_func_idx] != GetParent)
    {
        return testBitmap(prop->flags, PF_SUB_DEV);
    }

    // Parent property, traverse all children

    struct prop const * childProp = prop->value;
    uint16_t            idx;
    bool                retval = false;

    for (idx = 0 ; idx < prop->n_elements ; idx++, childProp++)
    {
        retval |= pubCheckSubDev(childProp);
    }

    return retval;
}



//! This function will initialise or re-initialise the sub structure

static void pubInitSubscriptions(uint16_t const reinit_f)
{
    uint8_t sub_idx;

    // If re-initalising, cancel subscribed flags in the properties

    if (reinit_f == PUB_SUB_REINIT)
    {
        for (sub_idx = 0; sub_idx < FGC_MAX_SUBS; sub_idx++)
        {
            if (pub.table[sub_idx].prop != NULL)
            {
                clrBitmap(pub.table[sub_idx].prop->dynflags, DF_SUBSCRIBED);
            }
        }

        // Clear the subsctiption structure

        MemSetWords(&pub, 0x0000, sizeof(pub) / 2);
    }


    // Link all the subscription table entries to the free list

    for (sub_idx = 0 ; sub_idx < FGC_MAX_SUBS ; sub_idx++)
    {
        pub.table[sub_idx].sub_idx = sub_idx + 1;
    }
}


//! This function will finish sending a publication response to the
//! gateway and will wait for the last packet to be written to the
//! fieldbus interface. This does not mean that it has actually been
//! sent to the gateway.

static void pubWaitForLastPkt(void)
{
    OS_CPU_SR  cpu_sr;

    OS_ENTER_CRITICAL();

    if (pcm.abort_f == false)
    {
        setBitmap(fbs.pub.header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT);

        fbsPutcStorePub(';');

        OSSemPend(pub_sema.last_pkt_send);
    }

    OS_EXIT_CRITICAL();
}



//! Publishes a subscription for a given user.

static void pubPublishUser(uint8_t const sub_idx,
                           uint8_t const sub_sel,
                           uint8_t const cyc_sel,
                           bool    const set_f)
{
    // This function is called from pubProperty and pubPublishSubscription
    // to mark a particular cycle selector of a particular subscription
    // as needing publication. The calling function must have lock the
    // sub semaphore beforehand. It only notifies if:
    //  - It is not already notified
    //  - It is not a new subscription pending the initial sub_idx being
    //    sent to the gateway

    struct pub_sub_table * sub_table = &pub.table[sub_idx];

    if (   extendMaskTestBit(pub.sub_acked_msk,                  sub_idx) == false
        && extendMaskTestBit(sub_table->sub_notify_msk[sub_sel], cyc_sel) == false)
    {
        // Notify this cycle selector and this subscription

        extendMaskSetBit((struct ExtendMask_gen *)&pub.sub_notify_msk,                 sub_idx);
        extendMaskSetBit((struct ExtendMask_gen *)&sub_table->sub_notify_msk[sub_sel], cyc_sel);

        if (set_f == true)
        {
            extendMaskSetBit((struct ExtendMask_gen *)&sub_table->set_f_msk[sub_sel], cyc_sel);
        }
        else
        {
            extendMaskClrBit((struct ExtendMask_gen *)&sub_table->set_f_msk[sub_sel], cyc_sel);
        }

        sub_table->notify_count++;

        pub.n_pubs++;
    }
}



//! This function is called for a new subscription once the sub_idx has been
//! sent to the gateway in a command response packet, or if an existing
//! subscription is re-subscribed. It sets the publish flags for all cycle
//! selectors for the property, as well as the start new subscription flag.

static void pubPublishSubscription(uint8_t const sub_idx)
{
    struct prop const * prop = pub.table[sub_idx].prop;

    if (prop == NULL)
    {
        return;
    }

    extendMaskSetBit((struct ExtendMask_gen *)&pub.new_sub_msk, sub_idx);

    if (testBitmap(prop->flags, PF_DONT_PUB_ON_SUB) == true)
    {
        return;
    }

    uint8_t sub_sel_max   = 1;
    uint8_t cyc_sel_first = 0;
    uint8_t cyc_sel_max   = 1;
    uint8_t cyc_sel;
    uint8_t sub_sel;

    // If the DEVICE.PPM property is set to ENABLED or the property is an acquisition (no set
    // function defined leaf properties and not a setting for parent propertis) calculate the
    // number of cycle selectors and sub-device selectors

    if (   dpcom.mcu.device_ppm == FGC_CTRL_ENABLED
        || (prop->set_func_idx == SET_NONE && testBitmap(prop->flags, PF_PARENT_SET) == false))
    {
        if (pubCheckCycSel(prop) == true)
        {
            cyc_sel_max   = FGC_MAX_USER_PLUS_1;
            cyc_sel_first = 1;
        }
    }

    // Calculate the cylces and sub-devices to send a first update

    if (dpcom.mcu.device_multi_ppm == FGC_CTRL_ENABLED)
    {
        // Acquisition properties (set function not defined) are non-multi-PPM (SUB_DEV
        // flag not set). However, the first update must be sent to all the sub-devices.

        if (prop->set_func_idx == SET_NONE || pubCheckSubDev(prop) == true)
        {
            sub_sel_max = FGC_MAX_SUB_DEVS;
        }
    }

    // First update: publish all sub-devices and users for this subscription

    extendMaskSetBit((struct ExtendMask_gen *)&pub.sub_notify_msk, sub_idx);

    for (sub_sel = 0; sub_sel < sub_sel_max; sub_sel++)
    {
        for (cyc_sel = cyc_sel_first; cyc_sel < cyc_sel_max; cyc_sel++)
        {
            pubPublishUser(sub_idx, sub_sel, cyc_sel, false);
        }
    }
}



//! This function is called from pubTsk() to send the publication specified by
//! sub_idx and cyc_sel when the proeprty value has changed or on first updates.

static void pubSendPublication(uint8_t  const sub_idx,
                               uint8_t  const sub_sel,
                               uint8_t  const cyc_sel,
                               bool     const new_sub_f,
                               bool     const set_f)
{
    struct pub_sub_table * sub_table = &pub.table[sub_idx];
    struct prop          * prop;
    uint16_t               sym_idxs[FGC_MAX_PROP_DEPTH];
    uint16_t               i;
    uint8_t                flags = 0;

    // Reset pub response length

    fbs.pub.rsp_len = 0;

    clrBitmap(fbs.pub.header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT | FGC_FIELDBUS_FLAGS_RSP_TYPE_MASK);

    // The 'set' and the 'new subscription' flags cannot be active simultaneously.
    // The latter will preempt the former.

    if (new_sub_f == true)
    {
        // The publication was triggered by the start of a subscription

        flags |= FGC_FIELDBUS_PUB_TRIG_NEW_SUB;
    }
    else if (set_f == true)
    {
        // The publication was triggered by a set command

        flags |= FGC_FIELDBUS_PUB_TRIG_SET;
    }

    pub.n_pubs--;

    OSSemPost(pub_sema.lock);

    OSSemPend(pcm.sem);

    pcm.prop_buf     = (struct Dpcom_prop_buf *)&dpcom.pcm_prop;
    pcm.prop         = sub_table->prop;
    pcm.timestamp_f  = false;
    pcm.cyc_sel      = cyc_sel;
    pcm.sub_sel      = sub_sel;
    pcm.cached_prop  = NULL;
    // Clear newline flag, is the only flag that can be handled externally
    pcm.f->_flags2  &= ~NL;
    pcm.n_arr_spec   = 0;
    pcm.from         = 0;
    pcm.to           = sub_table->prop->max_elements - 1;
    pcm.step         = 1;
    pcm.getopts      = GET_OPT_TYPE | GET_OPT_LBL | GET_OPT_BIN;
    pcm.token_delim  = ',';

    // Prepare property symbol stack so that the property names are correctly reported

    pcm.si_idx = 0;

    prop = pcm.prop;

    do
    {
        sym_idxs[pcm.si_idx++] = prop->sym_idx;
        prop = prop->parent;
    }
    while (prop != NULL);

    pcm.n_prop_name_lvls = pcm.si_idx;

    for (i = 0; i < pcm.n_prop_name_lvls; i++)
    {
        pcm.sym_idxs[i] = sym_idxs[--pcm.si_idx];
    }

    pcm.si_idx = pcm.n_prop_name_lvls;

    // Create publication header and call get function

    setBitmap(fbs.pub.header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT | FGC_FIELDBUS_FLAGS_PUB_DIRECT_PKT);

    // Add sub-device selector in the header. Check if the property or at least
    // one of its children is multi-PPM.

    if (   pubCheckSubDev(sub_table->prop) == true
        && sub_sel != PUB_ALL_SUB_DEVICES)
    {
        // Add the sub-device selector in the flags

        flags |= FGC_FIELDBUS_PUB_MULT_PPM;
        flags |= (sub_sel << FGC_FIELDBUS_PUB_SUB_DEV_SEL_SHIFT) & FGC_FIELDBUS_PUB_SUB_DEV_SEL_MASK;
    }

    // Output the publication request

    fbsPutcStorePub(sub_idx);
    fbsPutcStorePub(flags);
    fbsPutcStorePub(cyc_sel);

    // Set the last property if it as leaf so that the correct label can be appended in cmd.c::CmdPrintLabel()

    pcm.last_prop = (pcm.prop->type != PT_PARENT ? pcm.prop : NULL);

    // Call the get function for the property

    GET_FUNC[pcm.prop->get_func_idx](&pcm, pcm.prop);

    OSSemPost(pcm.sem);

    pubWaitForLastPkt();

    OSSemPend(pub_sema.lock);
}



// ---------- External function definitions

void pubTask(void * unused)
{
    // This function never exits, so the "local" variables are
    // static which saves stack space.

    static struct pub_sub_table       * sub_table;
    static struct prop          const * prop;
    static uint8_t                      pub_idx;
    static uint8_t                      sub_sel;
    static uint8_t                      cyc_sel;
    static bool                         set_f;
    static bool                         new_sub_f;

    // Initialise FILE structures for 'pcm' stream

    pcm.f = fopen("pcm", "wb");

    if (setvbuf(pcm.f, NULL, _IONBF, 0) != 0)
    {
        // 0   = ok
        // EOF = error
    }

    // Use PUB character to store the callback function

    pcm.store_cb    = fbsPutcStorePub;
    pcm.write_nvs_f = true;

    pub.new_sub_idx = 0xFF;

    // Initialise the free subs list only

    pubInitSubscriptions(PUB_SUB_INIT);

    pub_idx = 0;
    sub_sel = 0;

    // To balance out the publications, each sub-device can only publish one property
    // before the next subdevice is scanned for pending publications

    for (;;)
    {
        taskTraceInc();

        OSSemPost(pub_sema.lock);

        taskTraceReset();

        // Resumed by msTask() every 1 ms

        OSTskSuspend();

        taskTraceInc();

        OSSemPend(pub_sema.lock);

        taskTraceInc();

        // Cancel all the subscriptions if the gateway is not online

        if (fbs.gw_online_f == false)
        {
            // If subscriptions are still present, re-initialise the subscription structure

            if (pub.n_subs != 0)
            {
                pubInitSubscriptions(PUB_SUB_REINIT);
            }

            continue;
        }

        taskTraceInc();

        // Send first update after the subscription has been acknowledged to the gateway.

        while (extendMaskScan((struct ExtendMask_gen *)&pub.sub_acked_msk, &pub_idx) == true)
        {
            extendMaskClrBit((struct ExtendMask_gen *)&pub.sub_acked_msk, pub_idx);

            pubPublishSubscription(pub_idx);
        }

        taskTraceInc();

        // Publish subscriptions if the stream is not aborted and publication
        // requests are outstanding.

        if (pub.n_pubs == 0 || pcm.abort_f == true)
        {
            continue;
        }

        // Identify the next subscription to process

        if (extendMaskScan((struct ExtendMask_gen *)&pub.sub_notify_msk, &pub.pub_idx) == false)
        {
            uint16_t pub_error = 1;

            logRunAddTimestamp();
            logRunAddEntry(FGC_RL_PUB_ERR, &pub_error);

            pub.n_pubs = 0;

            continue;
        }

        sub_table = &pub.table[pub.pub_idx];
        prop      = sub_table->prop;

        if (prop == NULL)
        {
            uint16_t pub_error = 2;

            logRunAddTimestamp();
            logRunAddEntry(FGC_RL_PUB_ERR, &pub_error);

            continue;
        }

        new_sub_f = extendMaskTestBit(pub.new_sub_msk, pub.pub_idx);

        // Process the subscription

        bool ppm_prop_f = (testBitmap(prop->flags, PF_PPM) == true);

        // Identify next user to publish for the current sub-device selector
        // and clear corresponding bit in the notify mask of the subscription

        uint8_t sub_sel_max = dpcom.mcu.device_multi_ppm == FGC_CTRL_ENABLED ? FGC_MAX_SUB_DEVS : 1;

        for (sub_sel = 0; sub_sel < sub_sel_max && sub_table->notify_count > 0; sub_sel++)
        {
            if (extendMaskScan((struct ExtendMask_gen *)&sub_table->sub_notify_msk[sub_sel], &sub_table->sub_idx) == false)
            {
                continue;
            }

            cyc_sel = (ppm_prop_f == true ? sub_table->sub_idx : NON_PPM_USER);

            // Is the publication the result of a set cmd?

            set_f = extendMaskTestBit(sub_table->set_f_msk[sub_sel], cyc_sel);

            // Clear notification and set flag bit

            extendMaskClrBit((struct ExtendMask_gen *)&sub_table->sub_notify_msk[sub_sel], cyc_sel);
            extendMaskClrBit((struct ExtendMask_gen *)&sub_table->set_f_msk[sub_sel],      cyc_sel);

            sub_table->notify_count--;

            if (sub_table->notify_count == 0)
            {
                sub_table->sub_idx = 0;

                // Reset notify bit in the subscription

                extendMaskClrBit((struct ExtendMask_gen *)&pub.sub_notify_msk, pub.pub_idx);
                extendMaskClrBit((struct ExtendMask_gen *)&pub.new_sub_msk,    pub.pub_idx);
            }
            else
            {
                // Advance to next user

                if (++sub_table->sub_idx > device.max_user)
                {
                    sub_table->sub_idx = 0;
                }
            }

            pubSendPublication(pub.pub_idx, sub_sel, cyc_sel, new_sub_f, set_f);
        }

        // Advance to next subscription

        if (++pub.pub_idx >= FGC_MAX_SUBS)
        {
            pub.pub_idx = 0;
        }
    }
}



uint16_t pubSubscribe(struct prop * const prop,
                      uint8_t     * const sub_idx_ptr)
{
    // If the property is already subscribed, acknoweldge it and send the first update

    OSSemPend(pub_sema.lock);

    if (testBitmap(prop->dynflags, DF_SUBSCRIBED) == true)
    {
        pub.new_sub_idx = prop->sub_idx;

        *sub_idx_ptr = prop->sub_idx;

        OSSemPost(pub_sema.lock);

        return FGC_OK_NO_RSP;
    }

    // Report an error if the subscription table is full

    if (pub.n_subs >= FGC_MAX_SUBS)
    {
        OSSemPost(pub_sema.lock);

        return FGC_BUSY;
    }

    pub.n_subs++;

    // Acquire the next free entry in the subscription table

    uint16_t const sub_idx = pub.free_sub_idx;

    pub.free_sub_idx = pub.table[sub_idx].sub_idx;

    pub.table[sub_idx].sub_idx = 0;

    // Record this publication until completion

    pub.new_sub_idx = sub_idx;

    // Fill in the subsciption

    prop->sub_idx           = sub_idx;
    pub.table[sub_idx].prop = prop;

    setBitmap(prop->dynflags, DF_SUBSCRIBED);

    *sub_idx_ptr = sub_idx;

    OSSemPost(pub_sema.lock);

    return (FGC_OK_NO_RSP);
}



uint16_t pubUnsubscribe(struct prop * const prop,
                        uint16_t    * const sub_idx_ptr)
{
    OSSemPend(pub_sema.lock);

    // Return error if there are no subscriptions or the property
    // was never subscribed to

    if (   pub.n_subs == 0
        || testBitmap(fcm.prop->dynflags, DF_SUBSCRIBED) == false)
    {
        OSSemPost(pub_sema.lock);
        return (FGC_PROTO_ERROR);
    }

    // Retrieve the publication structure

    uint8_t                 const sub_idx   = prop->sub_idx;
    struct pub_sub_table  * const sub_table = &pub.table[sub_idx];

    pub.n_subs--;

    // Decrement the number of publication

    pub.n_pubs -= sub_table->notify_count;

    // Cancel the subsctiption flag

    clrBitmap(prop->dynflags, DF_SUBSCRIBED);

    // Clear the notification bit in the subscription mask

    extendMaskClrBit((struct ExtendMask_gen *)&pub.sub_notify_msk, sub_idx);

    // Erase subscription table

    memset(sub_table, 0, sizeof(*sub_table));

    sub_table->sub_idx = pub.free_sub_idx;
    pub.free_sub_idx   = sub_idx;

    OSSemPost(pub_sema.lock);

    *sub_idx_ptr = sub_idx;

    return (FGC_OK_NO_RSP);
}



void pubCancel(void)
{
    pcm.abort_f       = true;              // Inhibit output to pcm stream
    fbs.pub.q.out_idx = fbs.pub.q.in_idx;  // Reset fifo pointers
    fbs.pub.out_idx   = fbs.pub.q.in_idx;  // Reset local queue out pointer
    fbs.pub.q.is_full = false;             // Mark queue as not full

    // If the pubTsk is pending on queue full semaphore, wake up the task.

    if (fbs.pub.q_not_full->pending != 0)
    {
        OSSemPost(fbs.pub.q_not_full);
    }

    // If the pubTsk is pending on last packet send semaphore, wake up the task.

    if (pub_sema.last_pkt_send->pending != 0)
    {
        OSSemPost(pub_sema.last_pkt_send);
    }

    // Resume PubTsk if suspended

    OSTskResume(MCU_TSK_PUB);
}



void pubPublishProperty(struct prop       * prop,
                        uint8_t     const   sub_sel,
                        uint8_t     const   cyc_sel,
                        bool        const   set_f)
{
    OSSemPend(pub_sema.lock);

    // Loop for all levels up the property tree until the property is
    // hidden or has no parent.

    do
    {
        if (testBitmap(prop->dynflags, DF_SUBSCRIBED))
        {
            pubPublishUser(prop->sub_idx, sub_sel, cyc_sel, set_f);
        }
    }
    while (testBitmap(prop->flags, PF_HIDE) == false && (prop = prop->parent) != NULL);

    OSSemPost(pub_sema.lock);
}



void pubAcknowledgeSubscription(void)
{
    OSSemPend(pub_sema.lock);

    if (pub.new_sub_idx != 0xFF)
    {
        extendMaskSetBit((struct ExtendMask_gen *) &pub.sub_acked_msk, pub.new_sub_idx);

        pub.new_sub_idx = 0xFF;
    }

    OSSemPost(pub_sema.lock);
}



uint16_t pubPrintSubscriptionTable(struct cmd        * c,
                                   prop_size_t const   num_elements)
{
    struct pub_sub_table  *  sub_table;
    uint16_t                 errnum = FGC_OK_NO_RSP;
    prop_size_t              n       = num_elements;
    uint8_t                  cnt     = 0;
    char                     delim;

    delim = (c->token_delim == ' ' ? '\n' : ',');

    fprintf(c->f, "n_subs=%u n_pubs=%u free_sub_idx=%u new_sub_idx=%u %c",
            pub.n_subs,
            pub.n_pubs,
            pub.free_sub_idx,
            pub.new_sub_idx,
            delim);

    fprintf(c->f, "nti=0x%04X 0x%04X 0x%04X %c",
            pub.sub_notify_msk.master,
            pub.sub_notify_msk.mask_array[0],
            pub.sub_notify_msk.mask_array[1],
            delim);

    fprintf(c->f, "acked_msk nti=0x%04X 0x%04X 0x%04X %c",
            pub.sub_acked_msk.master,
            pub.sub_acked_msk.mask_array[0],
            pub.sub_acked_msk.mask_array[1],
            delim);

    sub_table = &pub.table[c->from];

    while (n-- > 0)
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)
        {
            return (errnum);
        }

        fprintf(c->f, "%2u idx=%2u prop*=0x%08lX notify_cnt=%u nti=0x%04X nix0=0x%04X %.6s",
                cnt++,
                sub_table->sub_idx,
                (uint32_t) sub_table->prop,
                sub_table->notify_count,
                sub_table->sub_notify_msk[0].master,
                sub_table->sub_notify_msk[0].mask_array[0],
                SYM_TAB_PROP[sub_table->prop->sym_idx].key.c);

        sub_table += c->step;
        c->from   += c->step;
    }

    return (errnum);
}



void pub2HzPublication(void)
{
    // List of properties to publish @ 2Hz

#if (FGC_CLASS_ID == 62)
    static struct prop * publish_list[] =
    {
        &PROP_MEAS_I_VALUE,
        &PROP_MEAS_V_VALUE,
        &PROP_MEAS_V_CAPA,
        &PROP_MEAS_I_EARTH,
        &PROP_MEAS_I_EARTH_CPCNT,
        &PROP_MEAS_I_ERR_MA,
        NULL
    };
#elif (FGC_CLASS_ID == 63)
    static struct prop * publish_list[] =
    {
        &PROP_MEAS_V_REF,
        &PROP_MEAS_I_REF,
        &PROP_MEAS_B_REF,
        &PROP_MEAS_I_VALUE,
        &PROP_MEAS_V_VALUE,
        &PROP_MEAS_B_VALUE,
        &PROP_MEAS_I_EARTH,
        &PROP_MEAS_I_EARTH_CPCNT,
        &PROP_MEAS_I_DIFF_MA,
        &PROP_MEAS_I_ERR_MA,
        NULL
    };
#elif (FGC_CLASS_ID == 64 || FGC_CLASS_ID == 65 || FGC_CLASS_ID == 66)
    static struct prop * publish_list[] =
    {
        NULL
    };
#endif

    OSSemPend(pub_sema.lock);

    // Pointer to one of the properties in publish_list (or NULL if end of list)

    struct prop ** property = &publish_list[0];

    while (*property != NULL)
    {
        struct prop * p = *property++;

        // Traverse all levels until the property is  hidden or has no parent.

        do
        {
            if (testBitmap(p->dynflags, DF_SUBSCRIBED))
            {
                pubPublishUser(p->sub_idx, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
            }
        }
        while (testBitmap(p->flags, PF_HIDE) == false && (p = p->parent) != NULL);
    }

    OSSemPost(pub_sema.lock);
}


// EOF
