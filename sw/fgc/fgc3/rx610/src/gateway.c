//! @file  gateway.c
//! @brief Ethernet ethernet Interface Functions


// ---------- Includes

#include <iodefines.h>
#include <string.h>

#include <gateway.h>
#include <dpcls.h>
#include <etherComm.h>
#include <fbs.h>
#include <lan92.h>
#include <parsService.h>
#include <pll.h>
#include <time_fgc.h>
#include <mcu.h>
#include <msTask.h>



// ---------- Internal structures, unions and enumerations

enum Gateway_payload_received
{
    FGC_GATEWAY_PAYLOAD_TIME,
    FGC_GATEWAY_PAYLOAD_COMMAND,
    FGC_GATEWAY_PAYLOAD_NUMBER
};


union Gateway_payload
{
    fgc_ether_cmd_payload_t     cmd;
    fgc_ether_pub_payload_t     pub;
    fgc_ether_rsp_payload_t     rsp;
    fgc_ether_status_payload_t  status;
};


struct Gateway_packet
{
    fgc_ether_header_t      header;
    union Gateway_payload   payload;
};


struct Gateway_packets
{
    struct Gateway_packet   rx;
    struct Gateway_packet   tx;
};


struct Gateway_functions
{
    void (*receive_packet[FGC_ETHER_PAYLOAD_NUM_TYPES])(void);
};


struct Gateway
{
    struct Gateway_packets   packets;
    uint16_t                 payload_to_index[FGC_ETHER_PAYLOAD_NUM_TYPES];
    struct Gateway_functions functions;
    uint32_t                 status_pkt_cnt;
    uint32_t                 time_pkt_cnt;
};




// ---------- External variable definitions

fgc_gateway_t              fgc_gateway;
ethernet_t                 ethernet;
communication_histograms_t communication_histograms;



// ---------- Internal variable definitions

static struct Gateway gateway;



// ---------- Internal function definitions

static inline void gatewayResetRemoteTerminalBuffer(void)
{
    fgc_gateway.rterm[0] = '\0';
}



static inline void gatewayInitVariables(void)
{
    gateway.payload_to_index[FGC_ETHER_PAYLOAD_TIME]  = FGC_GATEWAY_PAYLOAD_TIME;
    gateway.payload_to_index[FGC_ETHER_PAYLOAD_CMD]   = FGC_GATEWAY_PAYLOAD_COMMAND;
    gateway.status_pkt_cnt = 0;
    gateway.time_pkt_cnt   = 0;

    gatewayResetRemoteTerminalBuffer();
}



static inline void gatewayInitHistograms(void)
{
    histogramComInit(&communication_histograms.rx.gw, 0, 40);
    histogramComInit(&communication_histograms.tx.gw, 0, 40);
    histogramInit(&ethernet.stats.frame_count.sent.status.histogram, 40, 1);
    histogramInit(&ethernet.stats.frame_count.rcvd.time.histogram,   80, 1);
}



static inline void gatewaySetHeader(void)
{
    gateway.packets.tx.header = ethernet.tx_header;
}



static inline void gatewayReadTime(void)
{
    gateway.time_pkt_cnt++;

    if (fbs.count_time_rcvd < 5)
    {
        fbs.count_time_rcvd++;
    }

    // Process Time
    // ToDo to be refactorized

    fbs.last_runlog_idx = fbs.time_v.runlog_idx;

    lan92ReadFifo(&fbs.time_v, sizeof(fbs.time_v), &ethernet.current_rx_fifo_byte_length);

    fbs.time_rcvd_f = TRUE;                             // Set time rcvd flag (D cannot be zero)
    fbs.events_rcvd_f = TRUE;                           // Set events rcvd flag (D cannot be zero)

    fbs.u.fieldbus_stat.ack |= FGC_ACK_TIME;            // Acknowledge time_var reception

    // we need to validate that this irq comes from MSG18 or MSG19
    if ((fbs.time_v.ms_time % 20) == 18)
    {
        pll.eth18.sync_time = PLL_NETIRQTIME_P;
    }
    else if ((fbs.time_v.ms_time % 20) == 19)
    {
        pll.eth19.sync_time = PLL_NETIRQTIME_P;
    }
    else
    {
        ethernet.stats.frame_count.rcvd.bad.time++;
    }

    // Process RT data

    lan92ReadFifo(&fgc_gateway.rt_ref, sizeof(fgc_gateway.rt_ref), &ethernet.current_rx_fifo_byte_length);

    dpcom.mcu.ref.rt = (isnan(fgc_gateway.rt_ref[fbs.id - 1]) == false
                       ? fgc_gateway.rt_ref[fbs.id - 1]
                       : 0.0);

    fbs.u.fieldbus_stat.ack |= FGC_ACK_RD_PKT;  // Set RD rcvd flag in status variable

    histogramComAddValue(&communication_histograms.rx.gw, timeGetAbsUs());
}



static inline void gatewayReadCmd(void)
{
    lan92ReadFifo(&fbs.pkt_len, sizeof(fbs.pkt_len), &ethernet.current_rx_fifo_byte_length);
    fbs.pkt_len -= sizeof(struct fgc_fieldbus_cmd_header);

    lan92ReadFifo(&fbs.rcvd_header, sizeof(fbs.rcvd_header), &ethernet.current_rx_fifo_byte_length);
    lan92ReadFifo(fcm_pars.pkt[fbs.pkt_buf_idx].buf, fbs.pkt_len, &ethernet.current_rx_fifo_byte_length);

    histogramComAddValue(&communication_histograms.rx.gw, timeGetAbsUs());

    OSTskResume(MCU_TSK_FBS);
}



static inline void gatewayReceivePacketTypeTime(void)
{
    if (!fbs.gw_online_f)   // if Gateway never seen online, or connection lost, recover gateway address
    {
        memcpy(&gateway.packets.tx.header.ethernet.dest_addr, &ethernet.rx_header.ethernet.src_addr,
               FGC_ETHER_ADDR_SIZE);
    }

    gatewayReadTime();
}



static inline void gatewayReceivePacketTypeCmd(void)
{
    gatewayReadCmd();
}



static inline void gatewayInitFunctions(void)
{
    gateway.functions.receive_packet[FGC_GATEWAY_PAYLOAD_TIME]    = &gatewayReceivePacketTypeTime;
    gateway.functions.receive_packet[FGC_GATEWAY_PAYLOAD_COMMAND] = &gatewayReceivePacketTypeCmd;
}



static inline uint8_t gatewaySendHeader(uint32_t packet_tag)
{
    return lan92SendPacketOpt(&gateway.packets.tx.header,
                              sizeof(gateway.packets.tx.header),
                              sizeof(struct fgc_ether_status),
                              TX_CMDA_FIRST_SEG,
                              packet_tag);
}



static inline uint8_t gatewaySendStatusPayload(uint32_t packet_tag)
{
    return lan92SendPacketOpt(&fbs.u, 
                              sizeof(union fgc_stat_var), 
                              sizeof(struct fgc_ether_status), 
                              0,
                              packet_tag);
}



static inline uint8_t gatewayAddPadding(uint32_t packet_tag)
{
    return lan92SendPacketOpt(fgc_gateway.rterm,
                              sizeof(fgc_gateway.rterm) + sizeof(((struct fgc_ether_status *) 0)->payload.reserved),
                              sizeof(struct fgc_ether_status), 
                              TX_CMDA_LAST_SEG, 
                              packet_tag);
}



static inline uint8_t gatewaySendStatus(uint32_t packet_tag)
{
    uint8_t err = 0;

    err = gatewaySendHeader(packet_tag);
    err |= gatewaySendStatusPayload(packet_tag);
    // Note: We fill the padding of the struct (member reserved) with data beyond fgc_gateway.rterm, but no write access is performed
    err |= gatewayAddPadding(packet_tag);

    gatewayResetRemoteTerminalBuffer();

    gateway.status_pkt_cnt++;

    return err;
}



// ---------- External function definitions

void gatewayInit(void)
{
    gatewayInitVariables();
    gatewayInitFunctions();
    gatewaySetHeader();
    gatewayInitHistograms();
}



bool gatewaySendStatVar(void)
{
    uint8_t err = 0;
    uint32_t packet_tag;

    if (!fbs.gw_online_f)
    {
        return (FALSE);
    }

    gateway.packets.tx.header.fgc.payload_type = FGC_ETHER_PAYLOAD_STATUS;
    packet_tag = gateway.packets.tx.header.fgc.payload_type << 16;

    err = gatewaySendStatus(packet_tag);

    if (err)
    {
        ethernet.stats.frame_count.sent.bad.fail++;
    }

    histogramComAddValue(&communication_histograms.tx.gw, timeGetAbsUs());

    return (err == 0);
}



void gatewaySendMsg(void)
{
    uint8_t       err;
    uint16_t      segment1_size, segment2_size, segment3_size;
    uint16_t      packet_size;
    uint32_t      packet_tag;

    // Prepare ETH header
    if (fbs.pend_pkt == FBS_CMD_PKT)
    {
        gateway.packets.tx.header.fgc.payload_type = FGC_ETHER_PAYLOAD_RSP;
    }
    else if (fbs.pend_pkt == FBS_PUB_PKT)
    {
        gateway.packets.tx.header.fgc.payload_type = FGC_ETHER_PAYLOAD_PUB;
    }

    packet_tag = gateway.packets.tx.header.fgc.payload_type << 16;

    // Prepare RSP header

    // Set header with pkt description
    gateway.packets.tx.payload.rsp.data.header.flags = fbs.pend_header;
    // term chars are sent in status
    gateway.packets.tx.payload.rsp.data.header.n_term_chs = 0;
    gateway.packets.tx.payload.rsp.length = fbs.rsp_msg_len + sizeof(struct fgc_fieldbus_rsp_header);

    // NB:      A middle segment cannot be less than 4bytes, thus the length cannot be sent alone to the Tx FIFO.
    // NB:      Automatic padding if size less than minimum payload size.

    segment1_size = sizeof(gateway.packets.tx.header);
    segment2_size = sizeof(gateway.packets.tx.payload.rsp.length)
                  + sizeof(gateway.packets.tx.payload.rsp.data.header);
    segment3_size = fbs.rsp_msg_len * sizeof(uint8_t);
    packet_size   = segment1_size + segment2_size + segment3_size;


    err = lan92SendPacketOpt(&gateway.packets.tx.header, segment1_size, packet_size,
                               TX_CMDA_FIRST_SEG, packet_tag);
    err |= lan92SendPacketOpt(&gateway.packets.tx.payload.rsp, segment2_size, packet_size, 0,
                                packet_tag);
    err |= lan92SendPacketOpt(NULL, segment3_size, packet_size,
                                TX_CMDA_LAST_SEG | (ethernet.rsp_dma.offset << 16),
                                packet_tag);

    if (err)
    {
        ethernet.stats.frame_count.sent.bad.fail++;
    }
    else
    {
        if (ethernet.rsp_dma.size_bytes2 != 0)
        {
            DMAC0.DMCSA = ethernet.rsp_dma.source2;
            DMAC0.DMCBC = ethernet.rsp_dma.size_dwords2 * 4;
            DMAC0.DMCRE.BIT.DEN = 1;                    // Enable DMA
            DMAC0.DMCRD.BIT.DREQ = 1;                   // DMA Request (SW trigger)
        }

        DMAC1.DMCSA = ethernet.rsp_dma.source;
        DMAC1.DMCBC = ethernet.rsp_dma.size_dwords * 4;
        DMAC1.DMCRE.BIT.DEN = 1;                        // Enable DMA
        DMAC1.DMCRD.BIT.DREQ = 1;                       // DMA Request (SW trigger)
    }

    histogramComAddValue(&communication_histograms.tx.gw, timeGetAbsUs());
}



void gatewayCheckStatusTimePackets(void)
{
    static bool     ignore_first        = true;
    static uint32_t status_pkt_cnt_prev = 0;
    static uint32_t time_pkt_cnt_prev   = 0;

    uint32_t status_pkt_cnt = gateway.status_pkt_cnt;
    uint32_t time_pkt_cnt   = gateway.time_pkt_cnt;

    if (ignore_first == false)
    {
        uint32_t status_pkt_cnt_sec = status_pkt_cnt - status_pkt_cnt_prev;
        uint32_t time_pkt_cnt_sec   = time_pkt_cnt   - time_pkt_cnt_prev;

        histogramAddValue(&ethernet.stats.frame_count.sent.status.histogram, status_pkt_cnt_sec);
        if (status_pkt_cnt_sec != 50)
        {
            ethernet.stats.frame_count.sent.status.utc_failed = ms_task.time_us;
        }

        histogramAddValue(&ethernet.stats.frame_count.rcvd.time.histogram, time_pkt_cnt_sec);
        if (time_pkt_cnt_sec != 100)
        {
            ethernet.stats.frame_count.rcvd.time.utc_failed = ms_task.time_us;
        }
    }
    else
    {
        ignore_first = false;
    }

    status_pkt_cnt_prev = status_pkt_cnt;
    time_pkt_cnt_prev   = time_pkt_cnt;
}



void gatewayDmac1End(void)
{
    DMAC_COMMON.DMEDET.BIT.DEDET1 = 1;                          // Clear interrupt

    fbs.pend_stream->q.out_idx = fbs.pend_stream->out_idx;      // Advance circular buffer pointer
    fbs.pend_stream->q.is_full = FALSE;                         // Mark queue as not full

    if (fbs.pend_stream->q_not_full->pending)                   // If tasks are pending on sem
    {
        OSSemPost(fbs.pend_stream->q_not_full);                 // Post semaphore
    }
}



void gatewayReceivePacket(void)
{
    uint16_t payload_index = gateway.payload_to_index[ethernet.rx_header.fgc.payload_type];
    
    gateway.functions.receive_packet[payload_index]();
}


// EOF
