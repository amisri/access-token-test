//! @file  logTime.c
//! @brief Interface to the library libtimelog


// ---------- Includes

#include <stdint.h>

#include <libtimelog.h>

#include <logTime.h>
#include <defconst.h>
#include <fgc_event.h>
#include <time_fgc.h>



// ---------- Internal structures, unions and enumerations

//! Internal global variables for the module logTime

struct logTime
{
    struct TIMELOG_device   device;            //!< Allocation of memory for device data
};



// ---------- Internal variable definitions

static struct logTime __attribute__((section("sram"))) log_time;



// ---------- Internal function definitions

//! Callback returning the event type

static char * logTimeEventTypeCallback(uint32_t type)
{
    // Generalize SUB_DEBVICE specific events

    if (type == FGC_EVT_SUB_DEVICE_CYCLE_START) { type = FGC_EVT_CYCLE_START; }
    if (type == FGC_EVT_SUB_DEVICE_START_REF_1) { type = FGC_EVT_START_REF_1; }
    if (type == FGC_EVT_SUB_DEVICE_START_REF_2) { type = FGC_EVT_START_REF_2; }
    if (type == FGC_EVT_SUB_DEVICE_START_REF_3) { type = FGC_EVT_START_REF_3; }
    if (type == FGC_EVT_SUB_DEVICE_START_REF_4) { type = FGC_EVT_START_REF_4; }
    if (type == FGC_EVT_SUB_DEVICE_START_REF_5) { type = FGC_EVT_START_REF_5; }

    return (char*)(type < FGC_NUM_EVENT_TYPES ? fgc_event_names[type] : "INVALID");
}



// ---------- External function definitions

void logTimeInit(void)
{
    // Make sure all FGC and cclibs constants agree

    CC_STATIC_ASSERT((TIMELOG_NUM_RECORDS == FGC_LOG_TIMING_LEN), TIMELOG_NUM_RECORDS);

    timelogInit(&log_time.device, logTimeEventTypeCallback);
}



void logTimeStore(uint32_t          const   delay_us,
                  uint8_t           const   sub_sel,
                  uint8_t           const   cyc_sel,
                  uint8_t           const   type)
{
    struct CC_us_time now = { { timeGetUtcTime() }, timeGetUtcTimeMs() * 1000 };

    timelogStore(&log_time.device, &now, delay_us, sub_sel, cyc_sel, type);
}



void logTimeGet(uint32_t const log_index, char read_buf[TIMELOG_READ_BUF_LEN])
{
    timelogRead(&log_time.device, log_index, read_buf);
}


// EOF
