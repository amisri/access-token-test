//! @file  syscalls.c
//! @brief NewLib system calls
//!
//! Method 1 (there 2 more ways of doing this, read reent.h of NewLib)
//!
//! Define the reentrant versions of the syscalls directly.
//! (eg: _open_r, _close_r, etc.).  Please keep the namespace clean.
//! When you do this, set "syscall_dir" to "syscalls" and add
//! -DREENTRANT_SYSCALLS_PROVIDED to newlib_cflags in configure.host.
//!
//! Based on the article by Bill Gatliff at Embedded.com - The Official Site of the Embedded Development Community
//! Embedding GNU: Newlib, Part 2 - ( Embedded Systems Design, Porting newlib )
//! also _exit is needed, in the FGC3 case it is implemented in assembler in the start-up file start.S


// ---------- Includes

#include <syscalls.h>
#include <sys/stat.h>
#include <sys/reent.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <cmd.h>
#include <rtd.h>
#include <serialStream.h>
#include <fbs.h>
#include <panic.h>
#include <memmap_mcu.h>
#include <sharedMemory.h>
#include <os_hardware.h>
#include <mcu.h>



// ---------- External variable definitions

// Malloc/calloc buffer pools

struct TMallocManagerCtrl malloc_mngr_ctrl;
struct TMallocManagerData malloc_mngr_data;
struct TMallocManagerDbg  malloc_mngr_dbg;

struct _reent * newlib_reent_ptr[MCU_NUM_TASKS];



// ---------- Internal variable definitions

static struct _reent newlib_reent_fbs;     // FBS
static struct _reent newlib_reent_sta;     // STA
static struct _reent newlib_reent_sci;     // SCIVS
static struct _reent newlib_reent_pub;     // PUB
static struct _reent newlib_reent_fcm;     // FCM
static struct _reent newlib_reent_tcm;     // TCM
static struct _reent newlib_reent_rtd;     // RTD
static struct _reent newlib_reent_trm;     // TRM

// Tasks pointing to the following reent structure (which is the default newlib one) are not allowed to call malloc/calloc.

static struct _reent * newlib_no_malloc_reent_ptr;



// ---------- External function definitions

/*---------------------------------------------------------------------------------------------------------*\
 * IMPORTANT NOTES ON MALLOC_R, CALLOC_R, FREE_R:
 * ==============================================
 *
 * An unfortunate limitation of Newlib's stdio library is that it requires at least a minimal malloc()
 * for proper operation. When the printf() machinery detects a floating-point format specifier,
 * it allocates a few bytes of memory to use as a workspace for constructing the text
 * representation of the number. It's actually a poor design decision, because in other places,
 * newlib statically allocates memory to accomplish the same thing. But that's the way it is.
 *
 * The internal buffer for _vfprintf_r is defined as 40 characters so printf() expansions is smaller than this
 * will not need malloc()
 *
 * The interesting thing about newlib, however, is that it allocates new buffers as it needs them, and then
 * handles its own internal freelist. It means that NEWLIB DOES NOT NEED A FREE() function. This allows
 * for simplifications in the malloc_r and calloc_r functions, since every buffer is allocated at most once
 * and is never freed.
 *
 * The way Newlib manages its freelists is as follows: each task, (i.e. each thread, i.e. each _reent
 * structure) first allocates a freelist array to store pointers to chained lists of free buffer of a given
 * size. That freelist array has a size of precisely 132 bytes, that is the reason why  a buffer pool exists
 * on FGC3 with exactly that size. The memory structures in Newlib look like this. Buffers allocated thanks
 * to the malloc_r and calloc_r functions are represented a BUF(size).
 *
 *      THREAD 1 (e.g. PUB task)
 *        --> struct _reentr (= newlib_reent_pub)
 *              --> 1x Freelist_arr[]: BUF(132B)
 *                      0: pointer to a chained list of buffers --> BUF(24B) -->  BUF(24B) --> ...
 *                      1: pointer to a chained list of buffers --> BUF(28B) -->  BUF(28B) --> ...
 *                      2: pointer to a chained list of buffers --> BUF(32B) -->  BUF(32B) --> ...
 *                      3: pointer to a chained list of buffers --> BUF(36B) -->  BUF(36B) --> ...
 *                      4: pointer to a chained list of buffers --> BUF(40B) -->  BUF(40B) --> ...
 *                      5: ...
 *                      6: ...
 *
 *      THREAD 2 (e.g. FCM task)
 *        --> struct _reentr (= newlib_reent_fcm)
 *              --> 1x Freelist_arr[]: BUF(132B)
 *                      0: pointer to a chained list of buffers --> BUF(24B) -->  BUF(24B) --> ...
 *                      1: pointer to a chained list of buffers --> BUF(28B) -->  BUF(28B) --> ...
 *                      2: pointer to a chained list of buffers --> BUF(32B) -->  BUF(32B) --> ...
 *                      3: pointer to a chained list of buffers --> BUF(36B) -->  BUF(36B) --> ...
 *                      4: pointer to a chained list of buffers --> BUF(40B) -->  BUF(40B) --> ...
 *                      5: ...
 *                      6: ...
 *
 *      THREAD 3
 *        --> struct__reentr
 *              -->  x Freelist_arr[]: BUF(132B)
 *                      0: pointer to a chained list of buffers --> BUF(24B) -->  BUF(24B) --> ...
 *                      ...
 *
 *      ...
 *      ...
 *
 * The Newlib allocates new buffers everytime one of the freelists is empty and a buffer of the corresponding
 * size is needed for its operations. In practice, the allocated buffers are quite small. Hence our
 * strategy to implement functions malloc_r, calloc_r, free_r, which is based on the following principles:
 *
 *  - Newlib allocates buffers but never frees them. Therefore free_r() should never be called and shall Panic
 *  - The malloc buffers are stored into several pools, with one large pool of small buffer (32B), and also
 *    one pool of buffers with size exactly 132B, to cope with the freelist arrays for each thread.
 *  - Each MCU task is linked to its own struct _reent to ensure proper memory separation. However that
 *    structure being quite big, only the tasks that might use the malloc/calloc functions are associated with
 *    a specific struct _reent. All other tasks point to the default reentrant structure in Newlib, and those
 *    tasks shall never call calloc_r or malloc_r (else, the MCU will panic).
 *  - If in the future more tasks need to do mallocs and callocs, new struct _reent must be allocated
 *    for those tasks, pointers to which must be initialised in function InitMalloc()
\*---------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------*/
void InitMalloc(void)
/*---------------------------------------------------------------------------------------------------------*\
  THIS FUNCTION MUST BE CALLED EARLY DURING THE MCU INIT, I.E. BEFORE STARTING THE OS!
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                tid, pool_idx, idx;
    struct _pool_dbg    *   pool_dbg_ptr[] =
    {
        &malloc_mngr_dbg.pool1[0],
        &malloc_mngr_dbg.pool2[0],
        &malloc_mngr_dbg.pool3[0],
        &malloc_mngr_dbg.pool4[0],
        &malloc_mngr_dbg.pool5[0],
    };

    // Init REENT structures for all tasks, THIS MUST BE COMPLETED BEFORE STARTING THE OS TASKS!!!

    // As initialized in newlib. This is the default struct _reent.
    // MCU task pointing to this structure cannot use malloc/calloc.
    newlib_no_malloc_reent_ptr = _impure_ptr;

    for (tid = 0; tid < MCU_NUM_TASKS; tid++)
    {
        // As initialized in newlib. This is the default struct _reent.
        newlib_reent_ptr[tid] = _impure_ptr;
    }

    // The following tasks point to a specific struct _reent and can use malloc/calloc.

    newlib_reent_ptr[MCU_TSK_FBS] = &newlib_reent_fbs;
    newlib_reent_ptr[MCU_TSK_STA] = &newlib_reent_sta;
    newlib_reent_ptr[MCU_TSK_SCI] = &newlib_reent_sci;
    newlib_reent_ptr[MCU_TSK_PUB] = &newlib_reent_pub;
    newlib_reent_ptr[MCU_TSK_FCM] = &newlib_reent_fcm;
    newlib_reent_ptr[MCU_TSK_TCM] = &newlib_reent_tcm;
    newlib_reent_ptr[MCU_TSK_RTD] = &newlib_reent_rtd;
    newlib_reent_ptr[MCU_TSK_TRM] = &newlib_reent_trm;

    // Init malloc POOLS

    malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL1] = NEWLIB_MALLOC_POOL1_BLOCK_COUNT;
    malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL2] = NEWLIB_MALLOC_POOL2_BLOCK_COUNT;
    malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL3] = NEWLIB_MALLOC_POOL3_BLOCK_COUNT;
    malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL4] = NEWLIB_MALLOC_POOL4_BLOCK_COUNT;
    malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL5] = NEWLIB_MALLOC_POOL5_BLOCK_COUNT;

    malloc_mngr_ctrl.buffer_size  [NEWLIB_MALLOC_POOL1] = NEWLIB_MALLOC_POOL1_BLOCK_SIZE;
    malloc_mngr_ctrl.buffer_size  [NEWLIB_MALLOC_POOL2] = NEWLIB_MALLOC_POOL2_BLOCK_SIZE;
    malloc_mngr_ctrl.buffer_size  [NEWLIB_MALLOC_POOL3] = NEWLIB_MALLOC_POOL3_BLOCK_SIZE;
    malloc_mngr_ctrl.buffer_size  [NEWLIB_MALLOC_POOL4] = NEWLIB_MALLOC_POOL4_BLOCK_SIZE;
    malloc_mngr_ctrl.buffer_size  [NEWLIB_MALLOC_POOL5] = NEWLIB_MALLOC_POOL5_BLOCK_SIZE;

    for (pool_idx = 0; pool_idx < NEWLIB_MALLOC_NB_OF_POOLS; pool_idx++)
    {
        uint16_t block_count = malloc_mngr_ctrl.nb_of_buffers[pool_idx];

        malloc_mngr_ctrl.free_idx[pool_idx] = 0;

        for (idx = 0; idx < block_count; idx++)
        {
            pool_dbg_ptr[pool_idx][idx].free           = true;
            pool_dbg_ptr[pool_idx][idx].task_id        = 0;
            pool_dbg_ptr[pool_idx][idx].requested_size = 0;
        }
    }

    // Reset panic data

    malloc_mngr_dbg.panic_data.requested_size = 0;
    malloc_mngr_dbg.panic_data.task_id        = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
static bool memory_alloc(size_t req_size, size_t * buf_size, void ** buf_ptr)
/*---------------------------------------------------------------------------------------------------------*\
 This static function is called by malloc_r and calloc_r.
 This function returns a bool to indicate if a buffer with the appropriate size was found.
 If the return value is true, then:
     - buf_size is used to return the total size of the allocated buffer
     - buf_ptr  is used to return the buffer pointer.

 Implementation note:the access to malloc_mngr_ctrl.free_idx[pool_idx] MUST be protected against interrupts.

 See above: "IMPORTANT NOTES ON MALLOC_R, CALLOC_R, FREE_R"
\*---------------------------------------------------------------------------------------------------------*/
{
    bool      found   = false;
    uint16_t  free_idx;
    OS_CPU_SR cpu_sr;

    // Look in POOL1

    if (req_size <= NEWLIB_MALLOC_POOL1_BLOCK_SIZE)
    {
        OS_ENTER_CRITICAL();                                                    // Protect against interrupts (START)
        free_idx = malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL1];

        if (free_idx < malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL1])
        {
            malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL1]++;
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)

            *buf_ptr  = (void *)&malloc_mngr_data.pool1[free_idx].buffer;
            *buf_size = NEWLIB_MALLOC_POOL1_BLOCK_SIZE;
            found     = true;

            // Additional debug info

            malloc_mngr_dbg.pool1[free_idx].free           = false;
            malloc_mngr_dbg.pool1[free_idx].task_id        = OS_TID_CODE;
            malloc_mngr_dbg.pool1[free_idx].requested_size = req_size;
        }
        else
        {
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)
        }
    }

    // Look in POOL2

    if (!found && req_size <= NEWLIB_MALLOC_POOL2_BLOCK_SIZE)
    {
        OS_ENTER_CRITICAL();                                                    // Protect against interrupts (START)
        free_idx = malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL2];

        if (free_idx < malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL2])
        {
            malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL2]++;
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)

            *buf_ptr  = (void *)&malloc_mngr_data.pool2[free_idx].buffer;
            *buf_size = NEWLIB_MALLOC_POOL2_BLOCK_SIZE;
            found     = true;

            // Additional debug info

            malloc_mngr_dbg.pool2[free_idx].free           = false;
            malloc_mngr_dbg.pool2[free_idx].task_id        = OS_TID_CODE;
            malloc_mngr_dbg.pool2[free_idx].requested_size = req_size;
        }
        else
        {
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)
        }
    }

    // Look in POOL3

    if (!found && req_size <= NEWLIB_MALLOC_POOL3_BLOCK_SIZE)
    {
        OS_ENTER_CRITICAL();                                                    // Protect against interrupts (START)
        free_idx = malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL3];

        if (free_idx < malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL3])
        {
            malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL3]++;
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)

            *buf_ptr  = (void *)&malloc_mngr_data.pool3[free_idx].buffer;
            *buf_size = NEWLIB_MALLOC_POOL3_BLOCK_SIZE;
            found     = true;

            // Additional debug info

            malloc_mngr_dbg.pool3[free_idx].free           = false;
            malloc_mngr_dbg.pool3[free_idx].task_id        = OS_TID_CODE;
            malloc_mngr_dbg.pool3[free_idx].requested_size = req_size;
        }
        else
        {
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)
        }
    }

    // Look in POOL4

    if (!found && req_size <= NEWLIB_MALLOC_POOL4_BLOCK_SIZE)
    {
        OS_ENTER_CRITICAL();                                                    // Protect against interrupts (START)
        free_idx = malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL4];

        if (free_idx < malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL4])
        {
            malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL4]++;
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)

            *buf_ptr  = (void *)&malloc_mngr_data.pool4[free_idx].buffer;
            *buf_size = NEWLIB_MALLOC_POOL4_BLOCK_SIZE;
            found     = true;

            // Additional debug info

            malloc_mngr_dbg.pool4[free_idx].free           = false;
            malloc_mngr_dbg.pool4[free_idx].task_id        = OS_TID_CODE;
            malloc_mngr_dbg.pool4[free_idx].requested_size = req_size;
        }
        else
        {
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)
        }
    }

    // Look in POOL5

    if (!found && req_size <= NEWLIB_MALLOC_POOL5_BLOCK_SIZE)
    {
        OS_ENTER_CRITICAL();                                                    // Protect against interrupts (START)
        free_idx = malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL5];

        if (free_idx < malloc_mngr_ctrl.nb_of_buffers[NEWLIB_MALLOC_POOL5])
        {
            malloc_mngr_ctrl.free_idx[NEWLIB_MALLOC_POOL5]++;
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)

            *buf_ptr  = (void *)&malloc_mngr_data.pool5[free_idx].buffer;
            *buf_size = NEWLIB_MALLOC_POOL5_BLOCK_SIZE;
            found     = true;

            // Additional debug info

            malloc_mngr_dbg.pool5[free_idx].free           = false;
            malloc_mngr_dbg.pool5[free_idx].task_id        = OS_TID_CODE;
            malloc_mngr_dbg.pool5[free_idx].requested_size = req_size;
        }
        else
        {
            OS_EXIT_CRITICAL();                                                 // Protect against interrupts (END)
        }
    }

    // Requested size too large

    if (!found && req_size > NEWLIB_MALLOC_POOL5_BLOCK_SIZE)
    {
        malloc_mngr_dbg.panic_data.requested_size = req_size;
        malloc_mngr_dbg.panic_data.task_id        = OS_TID_CODE;

        panic(MCU_PANIC_SYSCALL_MALLOC_SIZE_R, RGLEDS_VS_RED_MASK16, RGLEDS_PSU_RED_MASK16);
    }

    return found;
}
/*---------------------------------------------------------------------------------------------------------*/
void * _malloc_r(struct _reent * ptr, size_t n)
/*---------------------------------------------------------------------------------------------------------*\
 See above: "IMPORTANT NOTES ON MALLOC_R, CALLOC_R, FREE_R"
\*---------------------------------------------------------------------------------------------------------*/
{
    void    *   allocated_buf  = NULL;
    size_t      allocated_size = 0;

    if (ptr == newlib_no_malloc_reent_ptr)
    {
        malloc_mngr_dbg.panic_data.requested_size = n;
        malloc_mngr_dbg.panic_data.task_id        = OS_TID_CODE;

        panic(MCU_PANIC_SYSCALL_MALLOC_REENT_R, RGLEDS_VS_RED_MASK16, RGLEDS_PSU_RED_MASK16);
    }

    if (!memory_alloc(n, &allocated_size, &allocated_buf))
    {
        malloc_mngr_dbg.panic_data.requested_size = n;
        malloc_mngr_dbg.panic_data.task_id        = OS_TID_CODE;

        panic(MCU_PANIC_SYSCALL_MALLOC_OUTOFMEM_R, RGLEDS_VS_RED_MASK16, RGLEDS_PSU_RED_MASK16);
    }

    return (allocated_buf);
}
/*---------------------------------------------------------------------------------------------------------*/
void * _calloc_r(struct _reent * ptr, size_t n, size_t s)
/*---------------------------------------------------------------------------------------------------------*\
  Use calloc to request a block of memory sufficient to hold an array of n elements, each of which has size s.
  The memory allocated by calloc comes out of the same memory pool used by malloc,
  but the memory block is initialized to all zero bytes.
  See above: "IMPORTANT NOTES ON MALLOC_R, CALLOC_R, FREE_R"
\*---------------------------------------------------------------------------------------------------------*/
{
    void    *   allocated_buf  = NULL;
    size_t      allocated_size = 0;

    if (ptr == newlib_no_malloc_reent_ptr)
    {
        malloc_mngr_dbg.panic_data.requested_size = n * s;
        malloc_mngr_dbg.panic_data.task_id        = OS_TID_CODE;

        panic(MCU_PANIC_SYSCALL_CALLOC_REENT_R, RGLEDS_VS_RED_MASK16, RGLEDS_PSU_RED_MASK16);
    }

    if (!memory_alloc(n * s, &allocated_size, &allocated_buf))
    {
        malloc_mngr_dbg.panic_data.requested_size = n * s;
        malloc_mngr_dbg.panic_data.task_id        = OS_TID_CODE;

        panic(MCU_PANIC_SYSCALL_CALLOC_OUTOFMEM_R, RGLEDS_VS_RED_MASK16, RGLEDS_PSU_RED_MASK16);
    }

    memset((void *)allocated_buf, 0, allocated_size);

    return (allocated_buf);
}
/*---------------------------------------------------------------------------------------------------------*/
void * _realloc_r(struct _reent * ptr,  void * aptr, size_t n)
/*---------------------------------------------------------------------------------------------------------*\
  Not supported on FGC3.
  See above: "IMPORTANT NOTES ON MALLOC_R, CALLOC_R, FREE_R"
\*---------------------------------------------------------------------------------------------------------*/
{
    ptr->_errno = ENOTSUP; //  return "not supported"

    panic(MCU_PANIC_SYSCALL_REALLOC_R, RGLEDS_VS_RED_MASK16, RGLEDS_PSU_RED_MASK16);

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
void _free_r(struct _reent * ptr, void * aptr)
/*---------------------------------------------------------------------------------------------------------*\
  For the usage of the newlib on FGC3, the allocated buffers are never freed.
  See above: "IMPORTANT NOTES ON MALLOC_R, CALLOC_R, FREE_R"
\*---------------------------------------------------------------------------------------------------------*/
{
    ptr->_errno = ENOTSUP; //  return "not supported"

    panic(MCU_PANIC_SYSCALL_FREE_R, RGLEDS_VS_RED_MASK16, RGLEDS_PSU_RED_MASK16);
}
/*---------------------------------------------------------------------------------------------------------*/
int _open_r(struct _reent * ptr, _CONST char * file, int flags, int mode)
/*---------------------------------------------------------------------------------------------------------*\
  Open a file.
  the reentrant version of open. It takes a pointer to the global data block, which holds errno.

  This stub's primary purpose is to translate a string device or file "name" to a file descriptor.
  With the exception of the standard input, standard output, and standard error devices, this function
  can also be used to provide advance notice of an impending write() or read() request.

  You can safely ignore the _open_r stub's flags and mode parameters, because they're only of interest
  when talking to a true filesystem.

  library standard is in  openr.c
\*---------------------------------------------------------------------------------------------------------*/
{
    int     fd  = -1;
    // fd = 0 = standard input (reserved)
    // fd = 1 = standard output (reserved)
    // fd = 2 = standard error (reserved)

    if (strcmp("scm", file) == 0)
    {
        fd = 3;
    }
    else
    {
        if (strcmp("rtd", file) == 0)
        {
            fd = 4;
        }
        else
        {
            if (strcmp("pcm", file) == 0)
            {
                fd = 5;
            }
            else
            {
                if (strcmp("fcm", file) == 0)
                {
                    fd = 6;
                }
            }
        }
    }


    if (fd != -1)   // we found the device ?
    {
        // nothing more to do
    }
    else // it doesn't exist!
    {
        ptr->_errno = ENODEV;
    }

    return (fd);
}
/*---------------------------------------------------------------------------------------------------------*/
//int _open64_r (void *reent, const char *file, int flags, int mode)
/*---------------------------------------------------------------------------------------------------------*\
    the reentrant version of open64. It takes a pointer to the global data block, which holds errno.
\*---------------------------------------------------------------------------------------------------------*/
/*
{
}
*/
/*---------------------------------------------------------------------------------------------------------*/
int _close_r(struct _reent * ptr, int fd)
/*---------------------------------------------------------------------------------------------------------*\
  Close a file.
  the reentrant version of close. It takes a pointer to the global data block, which holds errno.

  library standard is in closer.c
\*---------------------------------------------------------------------------------------------------------*/
{
    return (0);  // nothing to do, they give as minimal implementation return(-1) ???
}
/*---------------------------------------------------------------------------------------------------------*/
_ssize_t _write_r(struct _reent * ptr, int fd, _CONST _PTR buf, size_t cnt)
/*---------------------------------------------------------------------------------------------------------*\
  Write to a file.
  libc subroutines will use this system routine for output to all files, including stdout
  so if you need to generate any output, for example to a serial port for debugging, you should make
  your minimal write capable of doing this.

  the reentrant version of write. It takes a pointer to the global data block, which holds errno.

  library standard is in  writer.c
  must return the number of characters written

  return 0 or negative indicates an error
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  written       = cnt;
    uint16_t  ch;
    FILE  * serial_stream   = 0;
    void (*WriteCharCallback)(char, FILE *);


    // fd 0,1,2 are reserved

    switch (fd)
    {
        case 0: // 0 = standard input
        case 1: // 1 = standard output
        case 2: // 2 = standard error
        case 3: // scm
            serial_stream = tcm.f;
            WriteCharCallback = serialStreamInternalWriteChar;
            break;

        case 4: // rtd
            serial_stream = rtd.f;
            WriteCharCallback = serialStreamInternalWriteChar;
            break;

        case 5: // pcm
            serial_stream = pcm.f;
            WriteCharCallback = FieldbusPubStreamInternal_WriteChar;
            break;

        case 6: // fcm
            serial_stream = fcm.f;
            WriteCharCallback = FieldbusFcmStreamInternal_WriteChar;
            break;

        default :
            return (0); // error, 0 bytes written
    }

    //ToDo: this must be implemented

    // with newlib, when nonbuffered, we receive parts of the string (max 16 chars = BUFSIZ)
    // so this detection of string ending is not correct
    //  while ((ch != 0) && (err_code == 0))

    while (cnt > 0)
    {
        ch =  * ((char *) buf);
        buf++;
        WriteCharCallback(ch, serial_stream);
        cnt--;
    }

    // must return the number of characters written as we don't handle any error and there is no condition
    // to no send cnt chars we return always cnt

    return (written);
}
/*---------------------------------------------------------------------------------------------------------*/
_ssize_t _read_r(struct _reent * ptr, int fd, _PTR buf, size_t cnt)
/*---------------------------------------------------------------------------------------------------------*\
__attribute__ ((noreturn))
  Read from a file.
  the reentrant version of read. It takes a pointer to the global data block, which holds errno.

  library standard is in readr.c
\*---------------------------------------------------------------------------------------------------------*/
{
    panic(MCU_PANIC_SYSCALL_READ_R, RGLEDS_VS_RED_MASK16, RGLEDS_PSU_RED_MASK16);

    return (0);  // nothing to do
}
/*---------------------------------------------------------------------------------------------------------*/
//int _stat_r (struct _reent * ptr, _CONST char * file, struct stat * pstat)
/*---------------------------------------------------------------------------------------------------------*\
  Status of a file (by name).
  the reentrant version of stat. It takes a pointer to the global data block, which holds errno.

  library standard is in  statr.c

  we'll just tell the caller that the requested file or descriptor is a character device
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    pstat->st_mode = S_IFCHR;
    return(0);
}
*/
/*---------------------------------------------------------------------------------------------------------*/
int _fstat_r(struct _reent * ptr, int fd, struct stat * pstat)
/*---------------------------------------------------------------------------------------------------------*\
  Status of an open file.
  For consistency with other minimal implementations in these examples, all files are regarded as character
  special devices.
  The sys/stat.h header file required is distributed in the include subdirectory for this C library.
  the reentrant version of fstat. It takes a pointer to the global data block, which holds errno.

  library standard is in  fstatr.c

  we'll just tell the caller that the requested file or descriptor is a character device
\*---------------------------------------------------------------------------------------------------------*/
{
    pstat->st_mode = S_IFCHR;
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
//int _fstat64_r (struct _reent *ptr, int fd, struct stat *pstat)
/*---------------------------------------------------------------------------------------------------------*\
  the reentrant version of fstat64. It takes a pointer to the global data block, which holds errno.
\*---------------------------------------------------------------------------------------------------------*/
//{
//}
/*---------------------------------------------------------------------------------------------------------*/
_off_t _lseek_r(struct _reent * ptr, int fd, _off_t pos, int whence)
/*---------------------------------------------------------------------------------------------------------*\
__attribute__ ((noreturn))

  Set position in a file.
  the reentrant version of lseek. It takes a pointer to the global data block, which holds errno.

  library standard is in  lseekr.c

  we'll just pretend that the request is always successful
\*---------------------------------------------------------------------------------------------------------*/
{
    ptr->_errno = ENOTSUP; //  return "not supported"

    panic(MCU_PANIC_SYSCALL_LSEEK_R, RGLEDS_VS_RED_MASK16, RGLEDS_PSU_RED_MASK16);

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
//off_t _lseek64_r(void *reent, int fd, off_t pos, int whence)
/*---------------------------------------------------------------------------------------------------------*\
    the reentrant version of lseek64. It takes a pointer to the global data block, which holds errno.
\*---------------------------------------------------------------------------------------------------------*/
/*
{
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _link_r (struct _reent * ptr, _CONST char * old, _CONST char * new)
/*---------------------------------------------------------------------------------------------------------*\
  Establish a new name for an existing file.
  the reentrant version of link. It takes a pointer to the global data block, which holds errno.

  library standard is in  linkr.c

  we'll claim that the operation always fails
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = EMLINK;
    return(-1);
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _unlink_r (struct _reent * ptr, _CONST char * file)
/*---------------------------------------------------------------------------------------------------------*\
  Remove a file's directory entry.
  the reentrant version of unlink. It takes a pointer to the global data block, which holds errno.
  library standard is in  unlinkr.c

  we'll claim that the operation always fails
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = EMLINK;   // also ENOENT can be used
    return(-1);
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _fork_r ( struct _reent *ptr )
/*---------------------------------------------------------------------------------------------------------*\
  Create a new process.
  the reentrant version of fork. It takes a pointer to the global data block, which holds errno.

  library standard is in  execr.c

  Newlib calls upon this stub to do the work for the fork() system call, which, in POSIX environments,
  is used to create a clone of the current processing context.
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported", also EAGAIN can be used
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _execve_r (struct _reent * ptr, char * name, char **argv, char **env)
/*---------------------------------------------------------------------------------------------------------*\
  Transfer control to a new process.
  library standard is in  execr.c

  context management-related stubs
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported", also ENOMEM can be used
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _kill_r (struct _reent * ptr, int pid, int sig)
/*---------------------------------------------------------------------------------------------------------*\
  Send a signal.
  library standard is in  signalr.c

  context management-related stubs
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported", also EINVAL can be used
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _wait_r ( struct _reent * ptr, int * status)
/*---------------------------------------------------------------------------------------------------------*\
  Wait for a child process.
  the reentrant version of wait. It takes a pointer to the global data block, which holds errno.

  library standard is in  execr.c

  context management-related stubs
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported", also ECHILD can be used
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _getpid_r (struct _reent * ptr)
/*---------------------------------------------------------------------------------------------------------*\
  Process-ID;
  this is sometimes used to generate strings unlikely to conflict with other processes.
  library standard is in  signalr.c

  context management-related stubs

  This function returns the context's unique process ID,
  which we can emulate using uC/OS's OSTaskQuery() function.

\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported"
    return -1;

    // use our priority as a task id
//    OSTaskQuery( OS_PRIO_SELF, &tcb );
//    id = tcb.OSTCBPrio;
      return(id);
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//_CLOCK_T_ _times_r (struct _reent * ptr, struct tms * ptms)
/*---------------------------------------------------------------------------------------------------------*\
  Timing information for current process.
  library standard is in  timesr.c

  This stub returns various times for the current context.
  uC/OS doesn't keep statistics on a task's run time, so we leave this unimplemented:

\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported"
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//void * _sbrk_r (struct _reent * ptr, ptrdiff_t incr)
//char * _sbrk_r (void *          reent,  size_t incr)
/*---------------------------------------------------------------------------------------------------------*\
  Increase program data space.
  As malloc and related functions depend on this, it is useful to have a working implementation.
  The following suffices for a standalone system;
  it exploits the symbol _end automatically defined by the GNU linker.

  the reentrant version of sbrk. It takes a pointer to the global data block, which holds errno.

  library standard is in  sbrkr.c
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported"
    return(0);

    or can be implemented like

    char     _end;       // Defined by the linker
    static char *   heap_end;
    char *      prev_heap_end;

    if (heap_end == 0)
    {
      heap_end = &_end;
    }
    prev_heap_end = heap_end;
    if (heap_end + incr > stack_ptr)
    {
      write (1, "Heap and stack collision\n", 25);
      abort ();
    }

    heap_end += incr;
    return (caddr_t) prev_heap_end;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
int _isatty_r(struct _reent * ptr, int fd)
/*---------------------------------------------------------------------------------------------------------*\
  Query whether output stream is a terminal.
  the reentrant version of isatty. It takes a pointer to the global data block, which holds errno.

  library standard is in isattyr.c

\*---------------------------------------------------------------------------------------------------------*/
{
    //  ptr->_errno = 0; //  no error
    return (1);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: syscalls.c
\*---------------------------------------------------------------------------------------------------------*/
