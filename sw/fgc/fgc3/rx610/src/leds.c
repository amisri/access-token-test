//! @file   leds.c
//! @brief  LED interface function definitions


// ---------- Includes

#include <leds.h>
#include <bitmap.h>
#include <class.h>
#include <digitalIo.h>
#include <dpcom.h>
#include <fbs_class.h>
#include <mcu.h>
#include <pll.h>
#include <status.h>
#include <sta.h>
#include <time_fgc.h>



// ---------- Constants

static uint16_t const ALL_RED   = (  LEDS_RED_NET | LEDS_RED_FGC  | LEDS_RED_PSU \
                                   | LEDS_RED_VS  | LEDS_RED_DCCT | LEDS_RED_PIC);
static uint16_t const ALL_GREEN = (  LEDS_GREEN_NET | LEDS_GREEN_FGC  | LEDS_GREEN_PSU \
                                   | LEDS_GREEN_VS  | LEDS_GREEN_DCCT | LEDS_GREEN_PIC);



// ---------- Internal structures, unions and enumerations

typedef struct
{
    uint16_t status;
}leds_t;



// ---------- Internal variable definitions

static leds_t leds; // Front panel LED values (see msTask())



// ---------  Internal function definitions  ----------------------------

static void ledsOn(enum Leds_state state)
{
    setBitmap(leds.status, state);
}



static void ledsOff(enum Leds_state state)
{
    clrBitmap(leds.status, state);
}



static void ledsNETLedProcess(void)
{
    // Set the LED NET if the external sync is not available, or there are no network packets

    if (   (   pll.ext_sync_f == 0
            && fbs.id         != FBS_STANDALONE_ID)
        || (   pll.iter_count         != 0
            && pll.network.sync_count == 0))
    {
        ledsOn(LEDS_RED_NET);
    }
    else
    {
        // Turn NET_RED off. It will appear as orange (see doc, NET_GREEN is lighted by FPGA)

        ledsOff(LEDS_RED_NET);
    }
}



static void ledsFGCLedProcess(uint16_t ms)
{
    // Millisecond   0: updated the LED warning and fault status.
    // Millisecond 800: reset the LEDs for them to blink if warning or fault.

    if (ms == 0)
    {
        if (   statusTestFaults  (FGC_FAULTS)   == true
            || statusTestWarnings(FGC_WARNINGS) == true)
        {
            ledsOn(LEDS_RED_FGC);
        }

        if (statusTestFaults(FGC_FAULTS) == false)
        {
            ledsOn(LEDS_GREEN_FGC);
        }
    }
    else if (ms == 800)
    {
        ledsOff(LEDS_RED_FGC);
        ledsOff(LEDS_GREEN_FGC);
    }
}



static void ledsPSULedProcess(void)
{
    bool mcuVoltageFailed = mcuVoltageOutsideTolerance();

    if (   statusTestWarnings(FGC_WRN_FGC_PSU) == true
        || mcuVoltageFailed == true)
    {
        ledsOn(LEDS_RED_PSU);

        // Check limits

        if (mcuVoltageFailed == false)
        {
            ledsOn(LEDS_GREEN_PSU);
        }
        else
        {
            ledsOff(LEDS_GREEN_PSU);
        }
    }
    else
    {
        ledsOn(LEDS_GREEN_PSU);
        ledsOff(LEDS_RED_PSU);
    }
}



static void ledsVSLedProcess(void)
{
    if (testBitmap(digitalIoGetInputs(), DIG_IP_VSPOWERON_MASK32) == true)
    {
        ledsOn(LEDS_GREEN_VS);
    }
    else
    {
        ledsOff(LEDS_GREEN_VS);
    }

    if (   testBitmap(digitalIoGetInputs(), (DIG_IP_VSFAULT_MASK32 | DIG_IP_VSEXTINTLK_MASK32)) == true
        || statusTestFaults(FGC_FLT_VS_STATE) == true)
    {
        ledsOn(LEDS_RED_VS);
    }
    else
    {
        ledsOff(LEDS_RED_VS);
    }
}



static void ledsDCCTLedProcess(void)
{
#if (FGC_CLASS_ID == 62) || (FGC_CLASS_ID == 63)
    uint8_t dcct_flt_mask =   (testBitmap(ST_DCCT_A, FGC_DCCT_FAULT) << 0)
                            + (testBitmap(ST_DCCT_B, FGC_DCCT_FAULT) << 1);

    uint8_t dcct_flt      = dcct_flt_mask & (dpcls.dsp.dcct_select + 1);

    ledsOff(LEDS_GREEN_DCCT);
    ledsOff(LEDS_RED_DCCT);

    if (dcct_flt == 0)
    {
        ledsOn(LEDS_GREEN_DCCT);
    }
    else
    {
        ledsOn(LEDS_RED_DCCT);

        if (   dcct_flt != 3
            && dpcls.dsp.dcct_select == FGC_DCCT_SELECT_AB)
        {
            ledsOn(LEDS_GREEN_DCCT);
        }
    }
#endif
}



static void ledsPICLedProcess(void)
{
    if (   statusTestFaults   (FGC_FLT_FAST_ABORT)  == true
        || statusTestUnlatched(FGC_UNL_PWR_FAILURE) == true)
    {
        ledsOn(LEDS_RED_PIC);
    }
    else
    {
        ledsOff(LEDS_RED_PIC);
    }

    if (statusTestUnlatched(FGC_UNL_PC_PERMIT) == true)
    {
        ledsOn(LEDS_GREEN_PIC);
    }
    else
    {
        ledsOff(LEDS_GREEN_PIC);
    }
}



// ---------- External function definitions

void ledsToggle(enum Leds_state state)
{
    leds.status ^= state;
}



void ledsResetAll(void)
{
    leds.status = 0x0000;
}



void ledsProcess(uint16_t ms)
{
    // NET LED

    ledsNETLedProcess();

    // FGC LED

    ledsFGCLedProcess(ms);

    // PSU LED

    ledsPSULedProcess();

    // VS LED

    ledsVSLedProcess();

    // DCCT LED

    ledsDCCTLedProcess();

    // PIC LED

    ledsPICLedProcess();

    // Toggle Red light at 2 Hz
    // Above 500 ms: mask to turn off Red lights (if red set and green unset)

    uint16_t blink_mask = (  ms < 500
                           ? 0xFFFF
                           : ~((leds.status & ALL_RED) & ~((leds.status & ALL_GREEN) >> 1)));

    RGLEDS_P = leds.status & blink_mask;
}


// EOF
