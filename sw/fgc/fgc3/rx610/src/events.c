//! @file events.c
//! @brief Process timing events


#define FGC_EVENT_GLOBALS


// ---------- Includes

#include <stdint.h>

#include <events.h>
#include <bitmap.h>
#include <cycSim.h>
#include <cycTime.h>
#include <defprops.h>
#include <logEvent.h>
#include <fbs.h>
#include <fgc_event.h>
#include <function.h>
#include <logSpy.h>
#include <logTime.h>
#include <memmap_mcu.h>
#include <msTask.h>
#include <postMortem.h>
#include <time_fgc.h>



// ---------- Constants

static uint32_t const CPU_MODE_EXT_TIMING  = 0x0200;
static uint32_t const EVENT_TIME_WINDOW_MS =     60;

#define EVENTS_QUEUE_SIZE   FGC_NUM_EVENT_SLOTS


// NOTE:
// The event NEXT_DEST must be associated to its correspoinding START_REF_#
// event. The transmission order of these two events is not guaranteed. As a
// result, they can only be processsed once the pair of events have been
// received


// ---------- Internal structures, unions and enumerations

//! Structure containing the event information

struct Events_queue_node
{
    EventsClassFunc            class_func;               //!< Event function
    struct CC_us_time          start;                    //!< Event start time
    struct Events_data         data;                     //!< Event payload data
};


//! Queue for reference events

struct Events_queue
{
    struct Events_queue_node events[EVENTS_QUEUE_SIZE];  //!< Array with elements
    uint8_t                  size;                       //!< Number of elements stored in the queue
    uint8_t                  head;                       //!< Queue head
    uint8_t                  tail;                       //!< Queue tail
};


//! Internal local variables for the module Events

struct Events
{
    enum Events_source          source;              //!< Fieldbus event source

    struct Events_next_dest_start_ref
    {
        struct CC_ms_time      next_dest_received;  //!< NEXT_DEST reveived time
        struct CC_ms_time      start_ref_received;  //!< START_REF reveived time
        struct CC_us_time      start_ref_start;     //!< START_REF start time
        EventsClassFunc        class_func;          //!< Class function
        struct Events_data     data;                //!< Event data
    } next_dest_start_ref;

    struct Events_queue        queue_ref_events;    //!< Queue with reference events
    struct Events_queue        queue_trn_events;    //!< Queue with transactions events
};


struct Events_info
{
    EventsFunc                 func;                //!< Function associated to an event
    EventsClassFunc            class_func;          //!< Class-specific function associated to an event
    struct CC_ms_time          last_time;           //!< Last time an event was regsitered
};



// ---------- External variable definitions

struct Events_prop  events_prop;



// ---------- Internal variable definitions

static struct Events        events;
static struct Events_info   events_info[FGC_NUM_EVENT_TYPES];



// ---------- Internal function definitions

//! Pushes an event in the queue
//!
//! @param[in]    func           Event function
//! @param[in]    start          Event UTC start time
//! @param[in]    data           Event payload data

static void eventsPushEvent(struct Events_queue      * queue,
                            EventsClassFunc            class_func,
                            struct CC_us_time  const * start,
                            struct Events_data const   data)
{
    // This function is called within the context of the msTask in millisecond 12 out of 20.
    // eventsProcessNextEvent() cannot preempt this function because it runs within the
    // context of the eventTask, which is resumed by the msTask. As a result tehre is no
    // need to protect the queue.

    // Check the queue is not already full. Return silently if so

    if (queue->size >= EVENTS_QUEUE_SIZE)
    {
        return;
    }

    struct Events_queue_node * event = &queue->events[queue->head];

    event->class_func =  class_func;
    event->start      = *start;
    event->data       =  data;

    queue->size++;
    queue->head++;

    if (queue->head >= EVENTS_QUEUE_SIZE)
    {
        queue->head = 0;
    }
}



//! Pops an event from the queue and processes it.

static void eventsProcessNextEvent(struct Events_queue * queue)
{
    // This function is called within the context of the eventTask task, which is resumed by the
    // msTask and has higher priority than this one. As a result this function cannot be preempted
    // by eventsPushEvent() and therefore does not need to be proptected.

    if (queue->size == 0)
    {
        return;
    }

    struct Events_queue_node * event = &queue->events[queue->tail];

    event->class_func(&event->start, event->data);

    queue->size--;
    queue->tail++;

    if (queue->tail >= EVENTS_QUEUE_SIZE)
    {
        queue->tail = 0;
    }
}



//! Returns fieldbus events from an external trigger source

static void eventsExternalSource(struct fgc_event * events)
{
    static bool time_till_c0    = false;
    static bool time_till_event = false;

    // FGC_EVT_CYCLE_START

    events->type     = FGC_EVT_NONE;
    events->payload  = 0;
    events->delay_us = 0;

    if (cycTimeGetTimeTillC0() > 0)
    {
        if (time_till_c0 == false)
        {
            // Add FGC_EVT_CYCLE_START event with sub_sel = 0 and cyc_sel = 1

            events->type     = FGC_EVT_CYCLE_START;
            events->payload  = 1;
            events->delay_us = cycTimeGetTimeTillC0();

            time_till_c0 = true;
        }
    }
    else
    {
        time_till_c0 = false;
    }

    events++;

    // FGC_EVT_START_REF_1

    events->type     = FGC_EVT_NONE;
    events->payload  = 0;
    events->delay_us = 0;

    if (cycTimeGetTimeTillEvent() > 0)
    {
        if (time_till_event == false)
        {
            // Add FGC_EVT_START_REF_1 event with sub_sel = 0 and cyc_sel = 1

            events->type     = FGC_EVT_START_REF_1;
            events->payload  = 1;
            events->delay_us = cycTimeGetTimeTillEvent();

            time_till_event = true;
        }
    }
    else
    {
        time_till_event = false;
    }
}



//! Returns an array with either real or simulated events.

static struct fgc_event const * eventsSource(struct CC_ms_time const * utcTime)
{
    // This function checks whether simulated or real events are to be used based on
    // the following priority:
    //
    // 1) If the property PROP_FGC_EVENT_SIM has been set, use its values even if that
    //    means overwriting the Gateway timing events.
    // 2) Timing events from the Gateway.

    static struct fgc_event events_list[FGC_NUM_EVENT_SLOTS];
    static uint32_t         ext_lab_prev_time_till_c0  = 0;
    static uint32_t         ext_lab_prev_time_till_evt = 0;

    // Switch in or out of simulation or external timing on cycle boundaries.

    if (cycTimeGetTimeTillC0() <= 0)
    {
        // Only enable the external timing if the super-cycle simulation is disabled

        if (cycSimIsEnabled() == true)
        {
            if (events.source != EVENTS_SOURCE_SIMULATED)
            {
                events.source = EVENTS_SOURCE_SIMULATED;

                ext_lab_prev_time_till_c0  = 0;
                ext_lab_prev_time_till_evt = 0;

                clrBitmap(CPU_MODE_P, CPU_MODE_EXT_TIMING);
            }
        }
        else if (   testBitmap(events_prop.ref_event, FGC_EVENT_CYC_EXTERNAL_LAB) == true
                 && events_prop.external_us[0] != 0
                 && events_prop.external_us[1] != 0)
        {
            if (events.source != EVENTS_SOURCE_EXTERNAL)
            {
                events.source = EVENTS_SOURCE_EXTERNAL;

                setBitmap(CPU_MODE_P, CPU_MODE_EXT_TIMING);
            }

            if (ext_lab_prev_time_till_c0 != events_prop.external_us[0])
            {
                ext_lab_prev_time_till_c0 = events_prop.external_us[0];
                cycTimeSetTimeTillC0(events_prop.external_us[0], true);
            }

            if (ext_lab_prev_time_till_evt != events_prop.external_us[1])
            {
                ext_lab_prev_time_till_evt = events_prop.external_us[1];
                cycTimeSetTimeTillEvent(events_prop.external_us[1], true);
            }
        }
        else
        {
            if (events.source != EVENTS_SOURCE_GATEWAY)
            {
                events.source = EVENTS_SOURCE_GATEWAY;

                ext_lab_prev_time_till_c0  = 0;
                ext_lab_prev_time_till_evt = 0;

                clrBitmap(CPU_MODE_P, CPU_MODE_EXT_TIMING);
            }
        }
    }

    // Generate internal events when the super-cycle simulation is enabled

    if (events.source == EVENTS_SOURCE_SIMULATED)
    {
        cycSimProcess(utcTime, events_list);

        return events_list;
    }

    // Generate internal events when the timing is external

    if (events.source == EVENTS_SOURCE_EXTERNAL)
    {
        eventsExternalSource(events_list);

        return events_list;
    }

    // Gateway events

    return fbs.time_v.event;
}



//! Calculates the absolute UTC time for the event and returns the delay in microseconds

static uint32_t eventsGetEventTime(struct CC_us_time * start, uint32_t delay_us)
{

    if (events.source == EVENTS_SOURCE_GATEWAY)
    {
        // The gateway sends the fieldbus events at millisecond 18/19, whilst the processing
        // is done at millisecond 12 of the next fieldbus cycle. The parameter time_till_event_ms
        // refers to the beginning of the fieldbus cycle when the event is sent by the gateway.
        // Therefore there is an offset of 20 + 12 = 32 ms.
        //     ^                       ^                     ^
        //  _ _|_______________|___|___|___________|_________|_____ _ _ _ _ ___*|*___ _ _
        //     0              18  19 20/0 ms.      12         20/0 ms.
        //                  Event sent        Event processed                 Event
        //     <--------------------------- time_till_event_ms ----------------->
        //     <------------- offset -------------->
        //

        uint32_t offset_us = (ms_task.ms_mod_20 + 20) * 1000;

        // Protect against underflow

        if (delay_us < offset_us)
        {
            offset_us = delay_us;
        }

        delay_us -= offset_us;
    }

    if (start != NULL)
    {
        struct CC_us_time const start_us_time = { { timeGetUtcTime() }, timeGetUtcTimeMs() * 1000 };

        *start = cctimeUsAddDelayRT(start_us_time, delay_us);
    }

    return delay_us;
}



static bool eventsProcessGetCycSelSubSel(struct fgc_event const * event,
                                         uint8_t                * sub_sel_ref,
                                         uint8_t                * cyc_sel_ref,
                                         uint8_t                * sub_sel_acq,
                                         uint8_t                * cyc_sel_acq)
{
    *sub_sel_ref = *sub_sel_acq = dpcom.mcu.device_multi_ppm == FGC_CTRL_DISABLED
                                ? NON_MULTI_PPM_SUB_DEV
                                : (event->payload >> FGC_MULTI_PPM_SUB_SEL_SHIFT);

    *cyc_sel_acq = (event->payload & FGC_MULTI_PPM_USER_MASK);
    *cyc_sel_ref = dpcom.mcu.device_ppm == FGC_CTRL_DISABLED ? NON_PPM_USER : *cyc_sel_acq;

    return (*cyc_sel_acq > 0 && *cyc_sel_acq < FGC_MAX_USER_PLUS_1 && *sub_sel_acq < FGC_MAX_SUB_DEVS);
}


// Event function handlers

//! Function handling the event FGC_EVT_PM

static void eventsProcessPostMortem(struct fgc_event const * event)
{
    postMortemTriggerPmExt();
}



//! Function handling the events FGC_EVT_START, FGC_EVT_ABORT, FGC_EVT_CYCLE_TAG,
//! FGC_EVT_PAUSE, FGC_EVT_RESUME or FGC_EVT_ACQUISITION

static void eventsProcessCommon(struct fgc_event const * event)
{
    uint8_t type = event->type;

    if (events_info[type].class_func != NULL)
    {
        struct CC_us_time  start;
        uint32_t           delay_us;
        struct Events_data data = { .payload.data = event->payload };

        delay_us = eventsGetEventTime(&start, event->delay_us);

        events_info[type].class_func(&start, data);

        if (type != FGC_EVT_CYCLE_TAG)
        {
            logTimeStore(delay_us, 0, 0, type);
            logEventStoreEventCommon(delay_us, type, event->payload);
        }
    }
}



static void eventsProcessTransaction(struct fgc_event const * event)
{
    uint8_t const type = event->type;

    if (events_info[type].class_func != NULL)
    {
        struct CC_us_time  start;
        uint32_t           delay_us;
        struct Events_data data = { .payload.data = event->payload };

        delay_us = eventsGetEventTime(&start, event->delay_us);

        eventsPushEvent(&events.queue_trn_events, events_info[type].class_func, &start, data);

        if (type == FGC_EVT_TRANSACTION_COMMIT)
        {
            logTimeStore(delay_us, 0, 0, type);
            logEventStoreEventCommon(delay_us, type, event->payload);
        }
    }
}



//! Function handling the event FGC_EVT_START_REF_#

static void eventsProcessStartRef(struct fgc_event const * event)
{
    bool                  ref_event_cyc;
    uint8_t         const type          = event->type;
    EventsClassFunc const class_func    = events_info[type].class_func;

    if (class_func != NULL)
    {
        if (dpcom.mcu.device_multi_ppm == FGC_CTRL_DISABLED)
        {
            // If REF.EVENT_CYC = EXTERNAL_LAB, the event type 'artificially' created is FGC_EVT_START_REF_1

            ref_event_cyc = (   (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_EXTERNAL_LAB) == true && type == FGC_EVT_START_REF_1)
                             || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_1)  == true && type == FGC_EVT_START_REF_1)
                             || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_2)  == true && type == FGC_EVT_START_REF_2)
                             || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_3)  == true && type == FGC_EVT_START_REF_3)
                             || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_4)  == true && type == FGC_EVT_START_REF_4)
                             || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_5)  == true && type == FGC_EVT_START_REF_5));
        }
        else
        {
            ref_event_cyc = (   (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_1) == true && type == FGC_EVT_SUB_DEVICE_START_REF_1)
                             || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_2) == true && type == FGC_EVT_SUB_DEVICE_START_REF_2)
                             || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_3) == true && type == FGC_EVT_SUB_DEVICE_START_REF_3)
                             || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_4) == true && type == FGC_EVT_SUB_DEVICE_START_REF_4)
                             || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_5) == true && type == FGC_EVT_SUB_DEVICE_START_REF_5));
        }
    }
    else
    {
        ref_event_cyc = false;
    }

    if (ref_event_cyc == true)
    {
        struct Events_data data;
        struct CC_us_time  start;
        uint32_t           delay_us;

        delay_us = eventsGetEventTime(&start, event->delay_us);

        // Only process the event if the cycle selector and sub-device selector are valid

        if (eventsProcessGetCycSelSubSel(event,
                                         &data.payload.cycle.sub_sel_ref,
                                         &data.payload.cycle.cyc_sel_ref,
                                         &data.payload.cycle.sub_sel_acq,
                                         &data.payload.cycle.cyc_sel_acq) == true)
        {

            // Only register the event if the timing source is not external

            if (events.source != EVENTS_SOURCE_EXTERNAL)
            {
                events.next_dest_start_ref.start_ref_received = ms_task.time_ms;
                events.next_dest_start_ref.start_ref_start    = start;
                events.next_dest_start_ref.class_func         = class_func;
                events.next_dest_start_ref.data.payload       = data.payload;
            }
            else
            {
                class_func(&start, data);
            }
        }

        logTimeStore         (delay_us, data.payload.cycle.sub_sel_acq, data.payload.cycle.cyc_sel_acq, type);
        logEventStoreEventRef(delay_us, data.payload.cycle.sub_sel_acq, data.payload.cycle.cyc_sel_acq, type);
    }
}



//! Function handling the event FGC_EVT_SSC

static void eventsProcessSSC(struct fgc_event const * event)
{
    if (events_info[event->type].class_func != NULL)
    {
        // data not used so it is not initialized

        struct Events_data data;

        events_info[event->type].class_func(NULL, data);
    }
}



//! Function handling the event FGC_EVT_NEXT_DEST

static void eventsProcessNextDest(struct fgc_event const * event)
{
    events.next_dest_start_ref.next_dest_received    = ms_task.time_ms;
    events.next_dest_start_ref.data.next_destination = event->payload;

    logEventStoreEventNextDest(eventsGetEventTime(NULL, event->delay_us),
                               event->type,
                               event->trigger_type,
                               event->payload);
}



//! Function handling the event FGC_EVT_CYCLE_START

static void eventsProcessCycleStart(struct fgc_event const * event)
{
    // Return if the event type is not associated to the value of DEVICE.MULTI_PPM

    if (   (dpcom.mcu.device_multi_ppm == FGC_CTRL_ENABLED  && event->type == FGC_EVT_CYCLE_START)
        || (dpcom.mcu.device_multi_ppm == FGC_CTRL_DISABLED && event->type == FGC_EVT_SUB_DEVICE_CYCLE_START))
    {
        return;
    }

    // Calculate the start time

    struct Events_data data;
    struct CC_us_time  start;
    uint32_t           delay_us;

    delay_us = eventsGetEventTime(&start, event->delay_us);

    // Only process the event if the cycle selector and sub-device selector are valid

    if (eventsProcessGetCycSelSubSel(event,
                                    &data.payload.cycle.sub_sel_ref,
                                    &data.payload.cycle.cyc_sel_ref,
                                    &data.payload.cycle.sub_sel_acq,
                                    &data.payload.cycle.cyc_sel_acq) == true)
    {
        // Set up the TimeTillC0 register to start counting down as soon as possible

        if (events.source != EVENTS_SOURCE_EXTERNAL)
        {
            cycTimeSetTimeTillC0(delay_us, false);
        }
        else
        {
            delay_us = event->delay_us;
        }

        // Inform the DSP of the next cycle

        dpcom.mcu.cycle.info.sub_sel_ref   = data.payload.cycle.sub_sel_ref;
        dpcom.mcu.cycle.info.cyc_sel_ref   = data.payload.cycle.cyc_sel_ref;
        dpcom.mcu.cycle.info.sub_sel_acq   = data.payload.cycle.sub_sel_acq;
        dpcom.mcu.cycle.info.cyc_sel_acq   = data.payload.cycle.cyc_sel_acq;
        dpcom.mcu.cycle.info.start_time    = start;

        // Protect the flag with with a memroy barrier to guarantee the cycle data above are
        // ready before the DSP starts processing the cycle

        __sync_synchronize();

        dpcom.mcu.cycle.start_latched = true;

        // Check if the start reference is associated to the START_CYCLE event

        EventsClassFunc class_func = events_info[event->type].class_func;

        if (   class_func != NULL
            && testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_CYCLE) == true)
        {
            // Only register the event if the timing source is not external

            if (events.source != EVENTS_SOURCE_EXTERNAL)
            {
                events.next_dest_start_ref.start_ref_received = ms_task.time_ms;
                events.next_dest_start_ref.start_ref_start    = start;
                events.next_dest_start_ref.class_func         = class_func;
                events.next_dest_start_ref.data.payload       = data.payload;
            }
            else
            {
                class_func(&start, data);
            }
        }
    }

    logTimeStore         (delay_us, data.payload.cycle.sub_sel_acq, data.payload.cycle.cyc_sel_acq, event->type);
    logEventStoreEventRef(delay_us, data.payload.cycle.sub_sel_acq, data.payload.cycle.cyc_sel_acq, event->type);
}



static bool eventsAcceptNextDest(struct fgc_event const * event)
{
    uint8_t next_dest_type = event->trigger_type;

    return (   (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_CYCLE) == true && next_dest_type == FGC_EVT_CYCLE_START)
            || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_1) == true && next_dest_type == FGC_EVT_START_REF_1)
            || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_2) == true && next_dest_type == FGC_EVT_START_REF_2)
            || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_3) == true && next_dest_type == FGC_EVT_START_REF_3)
            || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_4) == true && next_dest_type == FGC_EVT_START_REF_4)
            || (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_START_REF_5) == true && next_dest_type == FGC_EVT_START_REF_5));
}



// ---------- External function definitions

void eventsInit(void)
{
    struct Events_info * event_info;
    struct CC_ms_time    now = { { timeGetUtcTime() }, timeGetUtcTimeMs() };

    for (event_info = &events_info[0]; event_info < &events_info[FGC_NUM_EVENT_TYPES]; event_info++)
    {
        event_info->last_time  = now;
        event_info->class_func = NULL;
    }

    memset(&events, 0, sizeof(events));

    // Map generic funcitons to all events

    events_info[FGC_EVT_NONE                  ].func =  NULL;
    events_info[FGC_EVT_PM                    ].func =  eventsProcessPostMortem;
    events_info[FGC_EVT_START                 ].func =  eventsProcessCommon;
    events_info[FGC_EVT_ABORT                 ].func =  eventsProcessCommon;
    events_info[FGC_EVT_SSC                   ].func =  eventsProcessSSC;
    events_info[FGC_EVT_NEXT_DEST             ].func =  eventsProcessNextDest;
    events_info[FGC_EVT_CYCLE_TAG             ].func =  eventsProcessCommon;
    events_info[FGC_EVT_CYCLE_START           ].func =  eventsProcessCycleStart;
    events_info[FGC_EVT_START_REF_1           ].func =  eventsProcessStartRef;
    events_info[FGC_EVT_START_REF_2           ].func =  eventsProcessStartRef;
    events_info[FGC_EVT_START_REF_3           ].func =  eventsProcessStartRef;
    events_info[FGC_EVT_START_REF_4           ].func =  eventsProcessStartRef;
    events_info[FGC_EVT_START_REF_5           ].func =  eventsProcessStartRef;
    events_info[FGC_EVT_SUB_DEVICE_CYCLE_START].func =  eventsProcessCycleStart;
    events_info[FGC_EVT_SUB_DEVICE_START_REF_1].func =  eventsProcessStartRef;
    events_info[FGC_EVT_SUB_DEVICE_START_REF_2].func =  eventsProcessStartRef;
    events_info[FGC_EVT_SUB_DEVICE_START_REF_3].func =  eventsProcessStartRef;
    events_info[FGC_EVT_SUB_DEVICE_START_REF_4].func =  eventsProcessStartRef;
    events_info[FGC_EVT_SUB_DEVICE_START_REF_5].func =  eventsProcessStartRef;
    events_info[FGC_EVT_PAUSE                 ].func =  eventsProcessCommon;
    events_info[FGC_EVT_RESUME                ].func =  eventsProcessCommon;
    events_info[FGC_EVT_ECONOMY_DYNAMIC       ].func =  eventsProcessCommon;
    events_info[FGC_EVT_ACQUISITION           ].func =  eventsProcessCommon;
    events_info[FGC_EVT_TRANSACTION_COMMIT    ].func =  eventsProcessTransaction;
    events_info[FGC_EVT_TRANSACTION_ROLLBACK  ].func =  eventsProcessTransaction;
    events_info[FGC_EVT_COAST                 ].func =  eventsProcessCommon;
    events_info[FGC_EVT_RECOVER               ].func =  eventsProcessCommon;
    events_info[FGC_EVT_START_HARMONICS       ].func =  eventsProcessCommon;
    events_info[FGC_EVT_STOP_HARMONICS        ].func =  eventsProcessCommon;

    events.source = EVENTS_SOURCE_GATEWAY;

    eventsClassInit();
};



void eventsProcess(void)
{
    if (shared_mem.mainprog_seq != SEQUENCE_MAIN_RUNNING)
    {
        return;
    }

    uint16_t          type;
    uint16_t          i;
    struct CC_ms_time now = { { timeGetUtcTime() }, timeGetUtcTimeMs() };

    // Retrieve the event from either the gateway or the super-cycle simulation.

    struct fgc_event    * event = (struct fgc_event *)eventsSource(&now);
    struct Events_info  * event_info;

    for (i = 0; i < FGC_NUM_EVENT_SLOTS; i++, event++)
    {
        type       = event->type;
        event_info = &events_info[type];

        // Discard all NEXT_DEST events that are not associated to the event defined in REF.EVENT_CYC

        if (type == FGC_EVT_NEXT_DEST && eventsAcceptNextDest(event) == false)
        {
            continue;
        }

        // Transaction events are sent asyncrhonously to the super-cycle. Potentially, two
        // COMMIT events with different payloads (transacion IDs) can be sent back to back
        // As a result these events cannot be discarded. Since each transaction event is sent
        // twice, the FGC3 will process the commit/rollback twice as well. This is not a
        // problem since the first will close the transaction.

        bool type_transaction = type == FGC_EVT_TRANSACTION_COMMIT || type == FGC_EVT_TRANSACTION_ROLLBACK;

        if (   type              <  FGC_NUM_EVENT_TYPES
            && event_info->func != NULL
            && (   type_transaction == true
                || timeMsGreaterThan(&now, &event_info->last_time) == true))
        {
            // The gateway sends the same event up to 2 consecutive fieldbus cycles: 40ms.
            // Omit events of the same type that are within that time window.

            event_info->last_time = now;

            timeMsAddDelay(&event_info->last_time, 40);

            event_info->func(event);
        }
    }

    // Check if the START_REF and NEXT_DEST event pair has been received

    struct Events_next_dest_start_ref * ndsr = &events.next_dest_start_ref;

    if (   timeMsDiff(&ndsr->next_dest_received, &ms_task.time_ms) < EVENT_TIME_WINDOW_MS
        && timeMsDiff(&ndsr->start_ref_received, &ms_task.time_ms) < EVENT_TIME_WINDOW_MS
        && ndsr->class_func != NULL)
    {
        eventsPushEvent(&events.queue_ref_events, ndsr->class_func, &ndsr->start_ref_start, ndsr->data);

        memset(ndsr, 0, sizeof(ndsr));
    }

    eventsClassProcess();
}



void eventsTask(void * unused)
{
    static bool event_completed = true;

    for (;;)
    {
        OSTskSuspend();

        taskTraceReset();

        // In ELENA the timing can be disabled by the operators. This discontinuous
        // transmission of timing events must be taken into account when checking
        // TIME_TILL_EVET as this free-running counter might overrun to positive
        // values. It will take 2^31 us = 2147s = 35m for the counter to become
        // negative.

        if (cycTimeGetTimeTillEvent() <= (int32_t)0 && event_completed == false)
        {
            event_completed = true;
        }

        if (events.queue_ref_events.size != 0 && event_completed == true)
        {
            eventsProcessNextEvent(&events.queue_ref_events);

            event_completed = false;
        }

        taskTraceInc();

        // Process all transaction events

        while (events.queue_trn_events.size != 0)
        {
            eventsProcessNextEvent(&events.queue_trn_events);
        }
    }
}



void eventsRegisterEventFunc(enum fgc_events type, EventsClassFunc function)
{
    events_info[type].class_func = function;
}



enum Events_source eventsGetSource(void)
{
    return events.source;
}


// EOF
