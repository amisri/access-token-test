//! @file   ethernet.h
//! @brief  Ethernet ethernet Interface Functions


// ---------- Includes

#include <ethernet.h>

#include <bitmap.h>
#include <fbs.h>
#include <fgc_errs.h>
#include <gateway.h>
#include <interFgc.h>
#include <lan92.h>
#include <mcu.h>
#include <os.h>
#include <sleep.h>
#include <sta.h>



// ---------- Constants

#define SUSPEND_INTERRUPT                           ( ETH_IRQ_CFG_P = 0                             )
// note : look for 0x0001000 or 0x00000001 if we don't know endianness
#define DEVICE_NOT_READY                            ( (ETH_PMT_CTRL_P & 0x00010001) == 0            )

#define RIGHT_ENDIANNESS                            ( ETH_BYTE_TEST_P == 0x87654321                 )
#define OTHER_ENDIANNESS                            ( ETH_BYTE_TEST_P == 0x43218765                 )
#define SWITCH_ENDIANNESS                           ( ETH_WORD_SWAP_P = 0xFFFFFFFF                  )

#define WRONG_LAN_CHIP                              ( (ETH_ID_REV_P & 0xFFFF0000) != 0x92210000     )

#define ID_FROM_MAC_ADDR                            ( *((uint8_t *) ETH_MAC_ADDR_32) & 0xFF         )
#define WROND_ID(id)                                ( (id  < 1) || (id  > 64)                       )

#define ENABLE_FULL_DUPLEX                          ( ETH_MAC_CSR_DATA_P  = 0x0010000C              )
#define WRITE_MAC_CR                                ( ETH_MAC_CSR_CMD_P   = 0x80000001              )

#define RESET_DROP_FRAME_COUNTER                    ( dummy_read = ETH_RX_DROP_P                    )
#define POP_PACKET_STATUS                           ( dummy_read = ETH_RX_STATUS_PORT_P             )



// ---------- Internal structures, unions and enumerations

enum Ethernet_communication_devices
{
    ETHERNET_COMMUNICATION_FGC,
    ETHERNET_COMMUNICATION_GATEWAY,
    ETHERNET_COMMUNICATION_DEVICE_NUMBER
};


struct Ethernet
{
    uint8_t                        eth_rsp_buf[sizeof(struct fgc_ether_rsp_payload) - sizeof(((struct fgc_fieldbus_rsp *)0)->rsp_pkt)];
    void     (*specific_receive_packet_functions[ETHERNET_COMMUNICATION_DEVICE_NUMBER])(void);
    struct fgc_ether_rsp_payload * eth_rsp;
    uint16_t                       payload_to_device[FGC_ETHER_PAYLOAD_NUM_TYPES];
    bool                           soft_reset_in_progress;
}; 



// ---------- Internal variable definitions
  
struct Ethernet ethernet_local;



// ---------- Internal function definitions

static inline bool ethernetTxStatusIsAvailable(void)
{
    return ((ETH_TX_FIFO_INF_P & ETH_TX_FIFO_INF_TXSUSED) != 0);
}



static inline bool ethernetRxStatusIsAvailable(void)
{
    return ((ETH_RX_FIFO_INF_P & ETH_RX_FIFO_INF_RXSUSED) != 0);
}



static inline bool ethernetInterruptIsActive(void)
{
    return (ETH_IRQ_CFG_P & ETH_IRQ_CFG_IRQ_INT);
}



static inline void ethernetSoftReset(void)
{
    ETH_HW_CFG_P |= ETH_HW_CFG_SRST;
}



static inline bool ethernetRxStatusIsOk(uint32_t const rx_status)
{
    static uint32_t const mask = (ETH_RX_STATUS_CRC_ERROR        |
                                  ETH_RX_STATUS_MII_ERROR        |
                                  ETH_RX_STATUS_WATCHDOG_TIMEOUT |
                                  ETH_RX_STATUS_COLLISION_SEEN   |
                                  ETH_RX_STATUS_FRAME_TOO_LONG   |
                                  ETH_RX_STATUS_LENGTH_ERROR     |
                                  ETH_RX_STATUS_ERROR_STATUS);

    return ((rx_status & mask) == 0);
}



static inline bool ethernetTxStatusIsOk(uint32_t const tx_status)
{
    static uint32_t const mask = ETH_TX_STATUS_ERROR_STATUS;

    return ((tx_status & mask) == 0);
}



//! Function called from the ISR.
//! This function is called for each packet available in the FIFO. It treats the different error cases
//! and calls the appropriate function depending on the payload type.

static void ethernetReceivePacket(void)
{
    uint32_t rx_payload_type;
    uint16_t rx_device_type;
    uint32_t rx_fifo_status;

    ethernet.stats.frame_count.rcvd.all++;

    //read RX status from the port
    rx_fifo_status = ETH_RX_STATUS_PORT_P;

    if (ethernetRxStatusIsOk(rx_fifo_status) == true)
    {
        ethernet.rx_is_broadcast = testBitmap(rx_fifo_status, ETH_RX_STATUS_BROADCAST_FRAME);
        ethernet.rx_time_us      = timeGetAbsUs();

        ethernet.current_rx_fifo_byte_length = (rx_fifo_status & ETH_RX_STATUS_PACKET_LENGTH) >> 16;
        lan92ReadFifo(&ethernet.rx_header, sizeof(ethernet.rx_header), &ethernet.current_rx_fifo_byte_length);

        if (ethernet.rx_header.ethernet.ether_type == FGC_ETHER_TYPE)
        {
            rx_payload_type = (enum fgc_ether_payload_type) ethernet.rx_header.fgc.payload_type;

            if (rx_payload_type < FGC_ETHER_PAYLOAD_NUM_TYPES)
            {
                ethernet.stats.frame_count.rcvd.payload_types[rx_payload_type]++;

                rx_device_type = ethernet_local.payload_to_device[rx_payload_type];
                ethernet_local.specific_receive_packet_functions[rx_device_type]();
            }
            else
            {
                ethernet.stats.frame_count.rcvd.bad.payload_type++;
                ethernet.stats.frame_count.rcvd.bad.all++;
            }
        }
        else
        {
            ethernet.stats.frame_count.rcvd.bad.ether_type++;
            ethernet.stats.frame_count.rcvd.bad.all++;
        }
    }
    else
    {
        ethernet.stats.frame_count.rcvd.bad.crc++;
        ethernet.stats.frame_count.rcvd.bad.all++;
    }

    // All data in the FIFO must be read
    // Discard end of packet if there is still data.
    // Note: CRC is discarded

    if (ethernet.current_rx_fifo_byte_length != 0)
    {
        lan92DiscardPacket(ethernet.current_rx_fifo_byte_length);
    }
}



//! This function is called for each packet available in the FIFO. It treats the different error cases
//! and calls the appropriate function depending on the payload type.
//! Function called from the ISR.

static void ethernetCheckSentPacketStatus(void)
{
    uint32_t tx_fifo_status;
    uint32_t tx_packet_type;

    //read TX status
    tx_fifo_status = ETH_TX_STATUS_PORT_P;
    tx_packet_type = (tx_fifo_status & ETH_TX_STATUS_PACKET_TAG) >> 16;

    // Note: no error does not ensure the packet is received by the GW
    // Indeed, even if there is no cable plugged, status is ok

    if (ethernetTxStatusIsOk(tx_fifo_status) && (tx_packet_type < FGC_ETHER_PAYLOAD_NUM_TYPES))
    {
        ethernet.stats.frame_count.sent.all++;
        ethernet.stats.frame_count.sent.payload_types[tx_packet_type]++;
    }
    else
    {
        ethernet.stats.frame_count.sent.bad.fail++;
    }
}



static void ethernetInitLinkingArray(void)
{
    ethernet_local.payload_to_device[FGC_ETHER_PAYLOAD_INTER_FGC] = ETHERNET_COMMUNICATION_FGC;
    ethernet_local.payload_to_device[FGC_ETHER_PAYLOAD_TIME]      = ETHERNET_COMMUNICATION_GATEWAY;
    ethernet_local.payload_to_device[FGC_ETHER_PAYLOAD_STATUS]    = ETHERNET_COMMUNICATION_GATEWAY;
    ethernet_local.payload_to_device[FGC_ETHER_PAYLOAD_CMD]       = ETHERNET_COMMUNICATION_GATEWAY;
    ethernet_local.payload_to_device[FGC_ETHER_PAYLOAD_RSP]       = ETHERNET_COMMUNICATION_GATEWAY;
    ethernet_local.payload_to_device[FGC_ETHER_PAYLOAD_PUB]       = ETHERNET_COMMUNICATION_GATEWAY;
}



static void ethernetInitFunctionArrays(void)
{
    ethernet_local.specific_receive_packet_functions[ETHERNET_COMMUNICATION_GATEWAY] = &gatewayReceivePacket;
    ethernet_local.specific_receive_packet_functions[ETHERNET_COMMUNICATION_FGC]     = &interFgcReceivePacket;
}



static void ethernetSetHeader(uint16_t id)
{
    // mac address is as follow: 02:00:00:00:00:XX where XX is the fieldbus id.
    
    ethernet.tx_header.ethernet.src_addr[0] = 0x02;
    ethernet.tx_header.ethernet.src_addr[1] = 0x00;
    ethernet.tx_header.ethernet.src_addr[2] = 0x00;
    ethernet.tx_header.ethernet.src_addr[3] = 0x00;
    ethernet.tx_header.ethernet.src_addr[4] = 0x00;
    ethernet.tx_header.ethernet.src_addr[5] = id;
    ethernet.tx_header.ethernet.ether_type  = FGC_ETHER_TYPE;
}



static void ethernetHwInitEnableTxRx(void)
{
    // Configure MAC_CR : enable RX and TX

    ENABLE_FULL_DUPLEX;
    WRITE_MAC_CR;

    // Enable transmitter

    setBitmap(ETH_TX_CFG_P, ETH_TX_CFG_TX_ON);
}



static void ethernetHwInitDeviceConfig(void)
{
    // Activate leds 1 and 2

    ETH_GPIO_CFG_P  =   0x30000000;
    // ETH_HW_CFG_P:    Byte swap for FIFO port only  (w/o FIFO_SEL)
    //                  Set "Must Be One" bit (even though does not seem to affect operation)
    ETH_HW_CFG_P   |=   ETH_HW_CFG_FPORTEND | ETH_HW_CFG_MBO;
}



static void ethernetHwInitClearRxFIFO(void)
{
    uint32_t dummy_read;
    // Null statement, prevent gcc from warning "set but not used"
    (void)   dummy_read;

    RESET_DROP_FRAME_COUNTER;

    while (ethernetRxStatusIsAvailable())
    {
        POP_PACKET_STATUS;
        lan92DiscardPacket(0);
    }
}



static void ethernetHwInitInterrupt(void)
{
    uint32_t int_en_mask = ETH_INT_STS_TXSO        // TX Status FIFO Overflow
                         | ETH_INT_STS_RXE         // Receiver Error
                         | ETH_INT_STS_TXE         // Transmitter Error
                         | ETH_INT_STS_TDFO        // TX Data FIFO Overrun Interrupt
                         | ETH_INT_STS_TSFF        // TX Status FIFO Full Interrupt TSFF
                         | ETH_INT_STS_TSFL        // TX Status FIFO Level Interrupt
                         | ETH_INT_STS_RXDF_INT    // Rx dropped frame
                         | ETH_INT_STS_RSFF        // RX Status FIFO Full Interrupt
                         | ETH_INT_STS_RSFL;       // RX Status FIFO Level Interrupt

    ETH_INT_STS_P = int_en_mask;
    ETH_INT_EN_P  = int_en_mask;

    // Enable interrupt line

    ETH_IRQ_CFG_P |= ETH_IRQ_CFG_IRQ_EN;

    // Set minimal interrupt de-assertion time

    ETH_IRQ_CFG_P |= 0x01000000;
}



// ---------- External function definitions

void ethernetInit(void)
{
    SUSPEND_INTERRUPT;

    fbsInit();

    ethernet_local.eth_rsp = (struct fgc_ether_rsp_payload *) ethernet_local.eth_rsp_buf;

    ethernetSoftReset();
    ethernetInitLinkingArray();
    ethernetInitFunctionArrays();
    ethernetHwInit(100);                             // Wait for the ethernet chip to respond with 100ms timeout

    // Go to standalone if no ethernet cable plugged at startup
    // ToDo Need to be review. This should be a situation where operation is not allowed
    // Standalone is only allowed if we plug special dongle

    if (lan92IsLinkUp() == false)
    {
        fbsSetStandalone();
    }

    gatewayInit();
}



uint16_t ethernetHwInit(uint16_t timeout_ms)
{
    // Wait for device to be ready, with given timeout

    while (DEVICE_NOT_READY && (timeout_ms != 0))
    {
        sleepMs(1);
        timeout_ms--;
    }

    if (DEVICE_NOT_READY)
    {
        // Time out, the chip is not responding

        return 1;
    }

    // test if LAN9215 reg is accessible and set endianness ---

    if (RIGHT_ENDIANNESS)
    {
        // Do nothing
    }
    else
    {
        if (OTHER_ENDIANNESS)
        {
            // Swap bytes

            SWITCH_ENDIANNESS;
        }
        else
        {
            return 2;
        }
    }

    // Check if ID CODE is 9221 then overwrite bus access times

    if (WRONG_LAN_CHIP)
    {
        return 3;
    }

    // Read fieldbus ID in external EEPROM, set mac address accordingly

    uint16_t id = ID_FROM_MAC_ADDR;
   
    ethernetSetHeader(id);

    if (WROND_ID(id))
    {
        fbsSetStandalone();
        return 4;
    }

    fbs.id      = id;
    fbs.pld_ver = ETH_PLDVER_P & 0x00FF;

    lan92SetMac(ethernet.tx_header.ethernet.src_addr);

    ethernetHwInitDeviceConfig();
    ethernetHwInitEnableTxRx();
    ethernetHwInitClearRxFIFO();
    ethernetHwInitInterrupt();

    // Trigger IRQ4 on both falling edge and rising edge

    ICU.IRQCR4.BYTE = 0x0C;

    return 0;
}



void ethernetWatch(void)
{

    if (fbs.count_time_rcvd == 5)
    {
        fbsReconnect();
        fbsInit();
        fbsLeaveStandalone();
        
        fbs.id            = *((uint8_t *) ETH_MAC_ADDR_32) & 0xFF;
        fbs.gw_online_f   = true;
        fbs.count_gw_lost = 0;
        fbs.count_time_rcvd++;
    }
    else 
    {
        // Check if the connection to the gateway has been temporarily lost

        if (fbs.time_rcvd_f == false && fbs.count_time_rcvd != 0)
        {
            // Check if the connection to the gateway has been permanentely lost

            if (fbs.gw_online_f == true && ++fbs.count_gw_lost > 3)
            {
                fbs.count_time_rcvd = 0;
                fbs.gw_online_f     = false;

                fbsDisconnect(FGC_PLL_NOT_LOCKED);
            }
        }
        else
        {
            fbs.count_gw_lost = 0;
        }
    
    }

    if (ethernet_local.soft_reset_in_progress == true && fbsIsStandalone() == true)
    {
        // Try to initialize ethernet interface (without timeout, not blocking). If succeed then fbs.id is set

        ethernetHwInit(0);

        if (fbsIsStandalone() == false)
        {
            ethernet_local.soft_reset_in_progress = false;
            ETH_INT_STS_P = ETH_INT_EN_P;

            fbsReconnect();
            mcuEnableNetworkInterrupt();
        }
    }
}



void ethernetISR(void)
{
    static uint32_t ETH_INT_TX_ERROR = ETH_INT_STS_TXSO 
                                     | ETH_INT_STS_TXE 
                                     | ETH_INT_STS_TDFO 
                                     | ETH_INT_STS_TSFF;

    while (ethernetInterruptIsActive())
    {
        if (testBitmap(ETH_INT_STS_P, ETH_INT_STS_RSFL) == true)
        {
            while (ethernetRxStatusIsAvailable())
            {
                ethernetReceivePacket();
            }

            lan92AckInterrupt(ETH_INT_STS_RSFL);
        }

        if (testBitmap(ETH_INT_STS_P, ETH_INT_STS_TSFL) == true)
        { 
            while (ethernetTxStatusIsAvailable())
            {
                ethernetCheckSentPacketStatus();
            }

            lan92AckInterrupt(ETH_INT_STS_TSFL);
        }

        //   In case of TX error, reset the chip and configure it again.
        
        if (testBitmap(ETH_INT_STS_P, ETH_INT_TX_ERROR) == true)
        {
            lan92AckInterrupt(ETH_INT_TX_ERROR);

            ethernet_local.soft_reset_in_progress = true;
            ethernet.stats.frame_count.sent.bad.txe++;
            ethernet.stats.soft_reset++;
            fbs.id = FBS_STANDALONE_ID;

            mcuDisableNetworkInterrupt();        
            ethernetSoftReset();            
            fbsDisconnect(FGC_RSP_TX_FAILED);
        }

        if (testBitmap(ETH_INT_STS_P, ETH_INT_STS_RXE | ETH_INT_STS_RSFF) == true)
        {
            ethernet.stats.frame_count.rcvd.bad.all++;
            ethernet.stats.frame_count.rcvd.bad.rxe++;

            lan92RxDump();
            lan92AckInterrupt(ETH_INT_STS_RXE | ETH_INT_STS_RSFF);
        }

        if (testBitmap(ETH_INT_STS_P, ETH_INT_STS_RXDF_INT) == true)
        {
            uint16_t number_of_dropped_frames = ETH_RX_DROP_P;
            
            ethernet.stats.frame_count.rcvd.bad.dropped += number_of_dropped_frames;
            ethernet.stats.frame_count.rcvd.bad.all     += number_of_dropped_frames;
            
            lan92RxDump();
            lan92AckInterrupt(ETH_INT_STS_RXDF_INT);
        }

    }
}


// EOF
