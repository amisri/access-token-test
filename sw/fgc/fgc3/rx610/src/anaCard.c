//! @file  anaCard.c
//! @brief FGC3 analogue card interface


// ---------- Includes

#include <anaCard.h>
#include <defconst.h>
#include <defprops.h>
#include <dpcls.h>
#include <hash.h>
#include <mcuDependent.h>
#include <panic.h>



// ---------- Constants

#define ANA_CARD_TR_ADC_GAIN    14.57    // 0.047*1023/3.3
#define ANA_CARD_TR_PROP_GAIN   400
#define ANA_CARD_TR_INT_GAIN    7
#define ANA_CARD_TR_DIF_GAIN    20

// Macro to read the Vref raw temperature (RX610 on-chip 10-bit ADC)

#define ANA_CARD_VREF_RAW_TEMP()     (AD0.ADDRA & 0x3FF)



// ---------- Internal structures, unions and enumerations

struct Ana_card
{
    enum Ana_card_type  type;       //!< Analogue interface type (ANA-101, ANA-102...)
};



// ---------- External variable definitions

struct Ana_card_adc adc;
struct Ana_card_temp_regul __attribute__((section("sram"))) ana_temp_regul;



// ---------- Internal variable definitions

static struct Ana_card ana_card;




// ---------- Internal function definitions

//! This function stops the temperature regulation.
//! To properly turn off the heater and disable regulation, call:
//! anaCardTempRegulStart(0.0);

static void anaCardTempRegulStop(void)
{
    // reset regulation flag

    ana_temp_regul.on_f = false;

    // reset 1st computation flag

    ana_temp_regul.prev_error = 0;

    // reset integral value

    ana_temp_regul.integrator = 0;
}



// ---------- External function definitions

void anaCardInit(void)
{
    ana_card.type = (enum Ana_card_type)MID_ANATYPE_P;

    if (   ana_card.type != ANA_CARD_TYPE_101
        && ana_card.type != ANA_CARD_TYPE_103
        && ana_card.type != ANA_CARD_TYPE_104)
    {
        panicCrash(0xBAB0);
    }

    // Analogue interface temperature regulation parameters

    ana_temp_regul.prop_gain = ANA_CARD_TR_PROP_GAIN;
    ana_temp_regul.int_gain  = ANA_CARD_TR_INT_GAIN;
    ana_temp_regul.diff_gain = ANA_CARD_TR_DIF_GAIN;
    ana_temp_regul.adc_gain  = ANA_CARD_TR_ADC_GAIN;

    // DAC settings (heater command)

    // set DAC to 0 (no heating)

    DA.DADR0 = 0x000;

    // disable DAC0 output

    DA.DACR.BIT.DAOE0 = 0;

    // Format selector. Bits 0-9 are used in DADR0.

    DA.DADPR.BIT.DPSEL = 0;

    // Start with no regulation by default. (On the Main Program the regulation will start
    // when the configuration property ADC.INTERNAL.VREF_TEMP.REF is initialised.)

    anaCardTempRegulStop();
}



enum Ana_card_type anaCardGetType(void)
{
    return ana_card.type;
}



void anaCardCpyMpx()
{
    // Copy the current state of the ADC Multiplexing from shared memory to the 
    // property. That code must run in a lower priority task than FCM and SCM, 
    // or a critical section must be added to function SetInternalAdcMpx(). 
    // The choice that was made was to call it in the background processing task.

    uint32_t int_adc_idx;

    for (int_adc_idx = 0; int_adc_idx < FGC_N_INT_ADCS; int_adc_idx++)
    {
        adc.internal_adc_mpx[int_adc_idx] = (uint16_t)dpcom.dsp.ana.mpx[int_adc_idx];
    }
}



void anaCardTempInit(void)
{
    // If ADC.INTERNAL.VREF_TEMP.REF property is set, then start regulating (see SetAnaVrefTemp).

    if (PROP_ADC_INTERNAL_VREF_TEMP_REF.n_elements == 1)
    {
        anaCardTempRegulStart(*((float *)PROP_ADC_INTERNAL_VREF_TEMP_REF.value));
    }
}



void anaCardTempRegulStart(float ref_temp)
{
    if (ref_temp == 0.0)
    {
        // Turn off regulation & heater
        // This will set ana_temp_regul.on_f to false

        anaCardTempRegulStop();

        // set DAC to 0 (no heating)

        DA.DADR0 = 0x000;

        // Disable DA0 output

        DA.DACR.BIT.DAOE0 = 0;
    }
    else
    {
        // Turn on regulation

        ana_temp_regul.adc_ref = (uint16_t)(ana_temp_regul.adc_gain * ref_temp);

        // If regulation was OFF, reset the PID algorithm and enable DAC0.

        if (ana_temp_regul.on_f == false)
        {
            ana_temp_regul.on_f = true;

            ana_temp_regul.prev_error = 0;
            ana_temp_regul.integrator = 0;

            // enable DAC0 output

            DA.DACR.BIT.DAOE0 = 1;
        }

        // If regulation was ON, we consider that the DAC was already active and we do not reset the PID parameters.
    }
}



void anaCardTempRegul(void)
{
    int32_t act;        //!< heater actuation (DAC units)
    int32_t error;      //!< error (raw ADC units)
    int32_t derror;     //!< differential of the error (raw ADC units)

    // Calculate error between reference and measurement in raw ADC units

    // Compute error with reference (calculated when set)

    error = (int32_t)ana_temp_regul.adc_ref - (int32_t)(ANA_CARD_VREF_RAW_TEMP());

    // Calculate the derivative of the error

    derror = error - ana_temp_regul.prev_error;

    ana_temp_regul.prev_error = error;

    // PID algorithm: act is calculated in fixed point with a multiplier of 256.

    act = ana_temp_regul.prop_gain * error                                 // P
        + ana_temp_regul.int_gain  * (ana_temp_regul.integrator + error)   // I
        + ana_temp_regul.diff_gain * derror;                               // D

    // Scale actuation in DAC units (10 bit)

    act /= 256;

    // Set DAC with protection against saturation

    if (act < 0)
    {
        // Heater is OFF

        DA.DADR0 = 0x000;
    }
    else if (act > 0x3FF)
    {
        // Heater at full power

        DA.DADR0 = 0x3FF;
    }
    else
    {
        // Set DAC to control the heater

        DA.DADR0 = (uint16_t)act;

        // Integrate the error

        ana_temp_regul.integrator += error;
    }
}



float anaCardGetTemp(void)
{
    float temp = 666.6;

    if (ana_temp_regul.adc_gain != 0.0)
    {
        temp = (float)ANA_CARD_VREF_RAW_TEMP() / ana_temp_regul.adc_gain;
    }

    return temp;
}



bool anaCardTempRegulOn(void)
{
    return ana_temp_regul.on_f;
}


// EOF
