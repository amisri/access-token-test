//! @file  transaction.c
//! @brief Provides the framework for transactional settings
//!
//! The transactions specification is documented in the link below:
//! https://wikis.cern.ch/display/COTEC/Transactions+support+in+the+Control+System


// ---------- Includes

#include <string.h>

#include <defprops.h>

#include <transaction.h>
#include <fgc_errs.h>
#include <property.h>
#include <pub.h>


// ---------  Internal structures, unions and enumerations  -------------

struct Transaction
{
    transactionFuncTest       refTest;
    transactionFuncCommit     refCommit;
    transactionFuncRollback   refRollback;
    transactionSetPropFunc    setProperty;
};



// ---------  External variable definitions  ----------------------------

struct Transaction_prop transaction_prop;



// ---------  Internal variable definitions  ----------------------------

static struct Transaction transaction = { .refTest     = NULL,
                                          .refCommit   = NULL,
                                          .refRollback = NULL,
                                          .setProperty = NULL };



// ---------- External function definitions

void transactionInit(transactionFuncTest     test,
                     transactionFuncCommit   commit,
                     transactionFuncRollback rollback,
                     transactionSetPropFunc  property)
{
    // If the class does not support transactions, the properties TRANSACTION.*
    // will not be instantiated and therefore these functions will never be
    // called. If they are instantiated and transactionInit() is not called,
    // the class will crash miserably

    transaction.refTest     = test;
    transaction.refCommit   = commit;
    transaction.refRollback = rollback;
    transaction.setProperty = property;
}



uint16_t transactionSetId(uint32_t const sub_sel, uint32_t const cyc_sel, uint16_t const transaction_id)
{
    if (transaction_prop.id[sub_sel][cyc_sel] != 0)
    {
        return FGC_TRANSACTION_IN_PROGRESS;
    }

    if (transactionValidateId(transaction_id) != FGC_OK_NO_RSP)
    {
        return FGC_INVALID_TRANSACTION_ID;
    }

    transaction_prop.id   [sub_sel][cyc_sel] = transaction_id;
    transaction_prop.state[sub_sel][cyc_sel] = FGC_TRANSACTION_STATE_NOT_TESTED;

    // PROP_TRANSACTION_ID: is published implicetely on S TRANSACTION.ID

    pubPublishProperty(&PROP_TRANSACTION_STATE, sub_sel, cyc_sel, false);

    return FGC_OK_NO_RSP;
}



uint16_t transactionGetId(uint32_t const sub_sel, uint32_t const cyc_sel)
{
    return transaction_prop.id[sub_sel][cyc_sel];
}



uint16_t transactionValidateId(uint32_t const transaction_id)
{
    return (  (transaction_id > 0 && transaction_id < UINT16_MAX)
            ? FGC_OK_NO_RSP
            : FGC_INVALID_TRANSACTION_ID);
}



uint32_t transactionAccessCheck(uint16_t    const   cmd_type,
                                uint16_t    const   cmd_transaction_id,
                                uint32_t    const   sub_sel,
                                uint32_t    const   cyc_sel,
                                struct prop       * property)
{
    uint16_t const transaction_id = transactionGetId(sub_sel, cyc_sel);
    uint16_t       errnum         = FGC_OK_NO_RSP;

    if (cmd_type == FGC_FIELDBUS_FLAGS_GET_CMD)
    {
        if (   cmd_transaction_id != 0
            && cmd_transaction_id != transaction_id)
        {
            errnum = transaction_id == 0 
                   ? FGC_TRANSACTION_NOT_IN_PROGRESS 
                   : FGC_INCORRECT_TRANSACTION_ID;
        }
    }
    else if (   cmd_type == FGC_FIELDBUS_FLAGS_SET_CMD 
             || cmd_type == FGC_FIELDBUS_FLAGS_SET_BIN_CMD)
    {
        if (cmd_transaction_id != transaction_id)
        {
            if (transaction_id == 0)
            {
                errnum = FGC_TRANSACTION_NOT_IN_PROGRESS;
            }
            else if (cmd_transaction_id == 0)
            {
                errnum = FGC_TRANSACTION_IN_PROGRESS;
            }
            else
            {
                errnum = FGC_INCORRECT_TRANSACTION_ID;
            }
        }
        else
        {
            transaction_prop.state[sub_sel][cyc_sel] = FGC_TRANSACTION_STATE_NOT_TESTED;

            if (transaction.setProperty != NULL)
            {
                transaction.setProperty(sub_sel, cyc_sel, property);
            }
        }
    }
    else
    {
        // Ignore ubscribe commands: FGC_FIELDBUS_FLAGS_SUB_CMD,
        // FGC_FIELDBUS_FLAGS_UNSUB_CMD and FGC_FIELDBUS_FLAGS_GET_SUB_CMD
    }

    return errnum;
}



uint16_t transactionTest(uint32_t const sub_sel, uint16_t const transaction_id)
{
    uint32_t cyc_sel;
    uint16_t errnum = FGC_UNKNOWN_TRANSACTION_ID;

    for (cyc_sel = 0; cyc_sel < FGC_MAX_USER_PLUS_1; cyc_sel++)
    {
        if (transaction_prop.id[sub_sel][cyc_sel] == transaction_id)
        {
            transaction_prop.state[sub_sel][cyc_sel] = (errnum = transaction.refTest(sub_sel, cyc_sel)) == FGC_OK_NO_RSP
                                                     ? FGC_TRANSACTION_STATE_TEST_OK
                                                     : FGC_TRANSACTION_STATE_TEST_FAILED;

            pubPublishProperty(&PROP_TRANSACTION_STATE, sub_sel, cyc_sel, false);

            // Stop and return the first error that occurs
            if (errnum != FGC_OK_NO_RSP)
            {
                break;
            }
        }
    }

    return errnum;
}



uint16_t transactionCommit(uint32_t          const   sub_sel,
                           uint16_t          const   transaction_id,
                           struct CC_us_time const * commit_time)
{
    uint32_t cyc_sel;
    uint16_t errnum = FGC_UNKNOWN_TRANSACTION_ID;
    bool     failed = false;

    for (cyc_sel = 0; cyc_sel < FGC_MAX_USER_PLUS_1; cyc_sel++)
    {
        if (transaction_prop.id[sub_sel][cyc_sel] == transaction_id)
        {
            errnum = FGC_OK_NO_RSP;

            if (   transaction_prop.state[sub_sel][cyc_sel]             == FGC_TRANSACTION_STATE_TEST_OK
                && transaction.refCommit(sub_sel, cyc_sel, commit_time) == FGC_OK_NO_RSP)
            {
                transaction_prop.state[sub_sel][cyc_sel] = FGC_TRANSACTION_STATE_NONE;
            }
            else
            {
                transaction.refRollback(sub_sel, cyc_sel);
                transaction_prop.state[sub_sel][cyc_sel] = FGC_TRANSACTION_STATE_COMMIT_FAILED;
                failed = true;
            }

            // End the transaction for this user
            transaction_prop.id[sub_sel][cyc_sel] = 0;

            pubPublishProperty(&PROP_TRANSACTION_ID,    sub_sel, cyc_sel, false);
            pubPublishProperty(&PROP_TRANSACTION_STATE, sub_sel, cyc_sel, false);
        }
    }

    return (failed == true ? FGC_TRANSACTION_COMMIT_FAILED : errnum);
}



uint16_t transactionRollback(uint32_t const sub_sel, uint16_t const transaction_id)
{
    uint32_t cyc_sel;
    uint16_t errnum = FGC_UNKNOWN_TRANSACTION_ID;

    for (cyc_sel = 0; cyc_sel < FGC_MAX_USER_PLUS_1; cyc_sel++)
    {
        if (transaction_prop.id[sub_sel][cyc_sel] == transaction_id)
        {
            errnum = FGC_OK_NO_RSP;

            transaction.refRollback(sub_sel, cyc_sel);

            // End the transaction
            transaction_prop.id   [sub_sel][cyc_sel] = 0;
            transaction_prop.state[sub_sel][cyc_sel] = FGC_TRANSACTION_STATE_NONE;

            pubPublishProperty(&PROP_TRANSACTION_ID,    sub_sel, cyc_sel, false);
            pubPublishProperty(&PROP_TRANSACTION_STATE, sub_sel, cyc_sel, false);
        }
    }

    return errnum;
}



void transactionAckCommitFailed(uint32_t const sub_sel, uint32_t const cyc_sel)
{
    if (transaction_prop.state[sub_sel][cyc_sel] == FGC_TRANSACTION_STATE_COMMIT_FAILED)
    {
        transaction_prop.state[sub_sel][cyc_sel] = FGC_TRANSACTION_STATE_NONE;

        pubPublishProperty(&PROP_TRANSACTION_STATE, sub_sel, cyc_sel, false);
    }
}


// EOF
