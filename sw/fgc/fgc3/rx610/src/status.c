//! @file   status.c
//! @brief  Process the operational status of the FGC
//!
//! Functions to process FAULTS, WARNINGS, ST_LATCHED, ST_UNLATCHED and DCCT faults.
//! Faults are processed with an internal variable and then latched at the end of statusProcess() to the system status FAULTS latch.


// ---------- Includes

#include <status.h>
#include <bitmap.h>
#include <crate.h>
#include <cycSim.h>
#include <digitalIo.h>
#include <dpcom.h>
#include <msTask.h>
#include <nvs.h>
#include <pc_state.h>
#include <pll.h>
#include <polSwitch.h>
#include <regfgc3Prog.h>
#include <sta.h>
#include <stateVs.h>
#include <stateOp.h>
#include <modePc.h>
#include <class.h>
#include <time_fgc.h>



// ---------- External variable definitions

uint16_t status_faults;
uint16_t status_warnings;
uint16_t status_latched;
uint16_t status_unlatched;



// ---------  Internal function definitions

static void statusUpdateNumFaults(void)
{
    static uint32_t prevFaults = 0;

    // Update STATUS.NUM_FAULTS if new faults occurred

    if (prevFaults != FAULTS)
    {
        uint16_t const newFaults = (prevFaults ^ FAULTS) & FAULTS;
        uint8_t        i;

        for (i = 0; i < 16 && newFaults != 0; i++)
        {
            if ((newFaults & (1 << i)) != 0)
            {
                sta.num_faults[i]++;
            }
        }

        prevFaults = FAULTS;
    }
}



static void statusProcessFaults(void)
{
    if (vs.present == FGC_CTRL_ENABLED)
    {
        statusEvaluateFaults(FGC_FLT_VS_EXTINTLOCK, testBitmap(digitalIoGetInputs(), DIG_IP_VSEXTINTLK_MASK32));
    }
    else
    {
        statusClrFaults(FGC_FLT_FAST_ABORT);
        statusClrFaults(FGC_FLT_VS_EXTINTLOCK);
    }

    statusEvaluateFaults(FGC_FLT_VS_FAULT,   testBitmap(digitalIoGetInputs(), DIG_IP_VSFAULT_MASK32));
    statusEvaluateFaults(FGC_FLT_FAST_ABORT, testBitmap(digitalIoGetInputs(), DIG_IP_FASTABORT_MASK32));

    if (statusIsPcPermit() == false && testBitmap(digitalIoGetInputs(), DIG_IP_VSRUN_MASK32) == false)
    {
        statusSetFaults(FGC_FLT_NO_PC_PERMIT);
    }

    bool fgc_hw = (   statusTestLatched(FGC_LAT_SPIVS_FLT)       == true
                   || statusTestUnlatched(FGC_UNL_REGFGC_PARAMS) == true
                   || regFgc3ProgThereIsProgFault()              == true
                   || regFgc3ProgSwitchProductionFailed()        == true
                   || (   statusTestLatched(FGC_LAT_DSP_FLT)     == true
                       && ms_task.dspFltFault                    == true)
                   );

    statusEvaluateFaults(FGC_FLT_FGC_HW, fgc_hw);

    // VS State invalid fault test

    static uint16_t vsStateCounter = 0;  // VS invalid state test counter

    if (STATE_VS == FGC_VS_INVALID)
    {
        vsStateCounter++;

        if (   statusTestFaults(FGC_FLT_VS_STATE) == false
            && (   (   testBitmap(digitalIoGetInputs(), DIG_IP_VSRUN_MASK32) == true
                    && vsStateCounter  > 1)
                || vsStateCounter >= 100))                      // Timeout in 5ms units
        {
            statusSetFaults(FGC_FLT_VS_STATE);
        }
    }
    else
    {
        vsStateCounter = 0;
    }

    // Evaluate FGC_FLT_FGC_STATE

    // FGC_STATE can be set either when MODE.OP is TEST or the config proeprties
    // have not been set. These are the normal fault conditions, which when not
    // set clear the FGC_STATE fault. This is excpetional since faults are only
    // cleared following a reset.
    //
    // In addition, FGC_STATE can be forced by FGC.FAULTS FGC_STATE. This condition
    // can only be cleared following a reset.

    static bool normal_flt_fgc_state = false;

    if (stateOpGetState() == FGC_OP_TEST || nvs_dbg.n_unset_config_props != 0)
    {
        normal_flt_fgc_state = true;

        statusSetFaults(FGC_FLT_FGC_STATE);
    }
    else
    {
        if (normal_flt_fgc_state == true)
        {
            // Clear FGC_STATE from the latched FAULT. This is an exception since faults
            // are cleared with a RESET command

            clrBitmap(FAULTS, FGC_FLT_FGC_STATE);

            statusClrFaults(FGC_FLT_FGC_STATE);

            normal_flt_fgc_state = false;
        }
    }

    // Update faults

    FAULTS |= status_faults | dpcom.dsp.faults;

    // Update STATUS.NUM_FAULTS

    statusUpdateNumFaults();

    // Clear faults from internal variable

    statusClrFaults(FGC_FLT_VS_STATE);
    statusClrFaults(FGC_FLT_VS_RUN_TO);
    statusClrFaults(FGC_FLT_NO_PC_PERMIT);
}



static uint32_t statusGetStretchedWarnings(void)
{
    static uint32_t const stretched_time             = 10;
    static uint32_t       prev_stretched_warnings    = 0;
    static uint32_t       active_stretched_warnings  = 0;
    static uint32_t       stretched_timeout          = 0;

    // Mix in MCU and DSP warnings

    uint32_t warnings = statusGetWarnings();

    // Record warnings to be stretched

    active_stretched_warnings |= (warnings & FGC_STRETCHED_WARNINGS);

    // If a new streteched warning is enable, update the timeout

    if (prev_stretched_warnings != active_stretched_warnings)
    {
        prev_stretched_warnings = active_stretched_warnings;

        stretched_timeout = timeGetUtcTime() + stretched_time;
    }

    if (stretched_timeout != 0 && timeGetUtcTime() > stretched_timeout)
    {
        // Check if the warnings can be cleared now that timeout has elapsed

        uint32_t clear_warnings = prev_stretched_warnings ^ (warnings & FGC_STRETCHED_WARNINGS);

        clrBitmap(active_stretched_warnings, clear_warnings);
        clrBitmap(prev_stretched_warnings,   clear_warnings);

        if (prev_stretched_warnings == 0)
        {
            stretched_timeout = 0;
        }
    }

    return (warnings |= active_stretched_warnings);
}




static void statusProcessWarnings(void)
{
    static const uint32_t WRN_FGC_PSU_CONDITIONS = FGC_LAT_DCCT_PSU_FAIL
                                                 | FGC_LAT_FGC_PSU_FAIL
                                                 | FGC_LAT_VDC_FAIL
                                                 | FGC_LAT_PSU_V_FAIL;

    bool fgc_hw_warning = (   statusTestLatched(FGC_LATCHED_FGC_HW_WRN) == true
                           || (   statusTestLatched(FGC_LAT_DSP_FLT)    == true
                               && ms_task.dspFltWarning                 == true));

    statusEvaluateWarnings(FGC_WRN_FGC_HW, fgc_hw_warning);

    statusEvaluateWarnings(FGC_WRN_FGC_PSU, statusTestLatched(WRN_FGC_PSU_CONDITIONS));

    // Check the warning FGC_WRN_EXT_SYNC_LOST

    statusEvaluateWarnings(FGC_WRN_EXT_SYNC_LOST, (pll.ext_sync_f  == false && pll.no_ext_sync == false));

    // Assert TIMING_EVT warning if the supercycle is simulated

#if (FGC_CLASS_ID == 62 || FGC_CLASS_ID == 63)
    statusEvaluateWarnings(FGC_WRN_TIMING_EVT, cycSimIsEnabled());
#endif

    // Update warnings

    WARNINGS = statusGetStretchedWarnings();
}



static void statusProcessStLatched(void)
{
    if (ms_task.dspFltWarning == true || ms_task.dspFltFault == true)
    {
        statusSetLatched(FGC_LAT_DSP_FLT);
    }

    // EPCCCS-6371 Some converters leave FGC_DIG_IPDIRECT_DCCTPSUFLT floating
    // instead of grounding it. This activates this input since there is a pull-up
    // resistor. For now, ignore this input until the refactoring of the FPGA
    // masks out its state for those converters not using it.

    // if (testBitmap(digitalIoGetRawInputs(), FGC_DIG_IPDIRECT_DCCTPSUFLT) == true)
    // {
    //     statusSetLatched(FGC_LAT_DCCT_PSU_FAIL);
    // }

    if (testBitmap(digitalIoGetRawInputs(), FGC_DIG_IPDIRECT_FGCPSUFLT) == true)
    {
        statusSetLatched(FGC_LAT_FGC_PSU_FAIL);
    }

    if (testBitmap(digitalIoGetRawInputs(), FGC_DIG_IPDIRECT_VDCFLT) == true)
    {
        statusSetLatched(FGC_LAT_VDC_FAIL);
    }

    // Update latched conditions

    ST_LATCHED |= statusGetLatched();
}



static void statusProcessStUnlatched(void)
{
    if (testBitmap(digitalIoGetInputs(), DIG_IP_VSPOWERON_MASK32) == true)
    {
        statusSetUnlatched(FGC_UNL_VS_POWER_ON);
    }
    else
    {
        statusClrUnlatched(FGC_UNL_VS_POWER_ON);
    }

    if (testBitmap(digitalIoGetInputs(), DIG_IP_PWRFAILURE_MASK32) == true)
    {
        statusSetUnlatched(FGC_UNL_PWR_FAILURE);
    }
    else
    {
        statusClrUnlatched(FGC_UNL_PWR_FAILURE);
    }

    if (testBitmap(digitalIoGetInputs(), DIG_IP_PCPERMIT_MASK32) == false)
    {
        statusClrUnlatched(FGC_UNL_PC_PERMIT);
    }
    else
    {
        statusSetUnlatched(FGC_UNL_PC_PERMIT);
    }

    // Handle PCREGOK interlock

    // This condition is driven by the PC state. It is class-dependent based on the state
    // Class 62: CYCLING
    // Class 63: TO_CYCLING | CYCLING | DIRECT | IDLE | ARMED | RUNNING | ECONOMY | PAUSED

#if (FGC_CLASS_ID == 62)
    statusEvaluateUnlatched(FGC_UNL_PC_REG_OK, pcStateTest(FGC_STATE_TO_CYCLING_BIT_MASK | FGC_STATE_CYCLING_BIT_MASK));
#endif

#if (FGC_CLASS_ID == 63)

    static uint32_t const pcRegOkState[] =
    {
        // FGC_PC_ON_IDLE
        (FGC_STATE_IDLE_BIT_MASK | FGC_STATE_ARMED_BIT_MASK | FGC_STATE_RUNNING_BIT_MASK),
        // FGC_PC_ON_CYCLING
        (  FGC_STATE_TO_CYCLING_BIT_MASK | FGC_STATE_CYCLING_BIT_MASK | FGC_STATE_ECONOMY_BIT_MASK
         | FGC_STATE_PAUSED_BIT_MASK     | FGC_STATE_POL_SWITCHING_BIT_MASK),
        // FGC_PC_ON_DIRECT
        (FGC_STATE_DIRECT_BIT_MASK),
    };

    uint8_t state = (modePcGetPcOn() == FGC_PC_IDLE ? 0 : (modePcGetPcOn() == FGC_PC_CYCLING ? 1 : 2));

    statusEvaluateUnlatched(FGC_UNL_PC_REG_OK, pcStateTest(pcRegOkState[state]));
#endif


#if (FGC_CLASS_ID == 63)
    bool lowCurrentState = (   dpcls.dsp.meas.i_low_f != 0
                            && (pcStateTest(FGC_STATE_TO_CYCLING_BIT_MASK | FGC_STATE_CYCLING_BIT_MASK) == false));
    statusEvaluateUnlatched(FGC_UNL_LOW_CURRENT, lowCurrentState);
#endif

    // Update unlatched conditions

    ST_UNLATCHED = statusGetUnlatched();
}



static void statusProcessDCCT(void)
{
    if (testBitmap(digitalIoGetInputs(), DIG_IP_DCCTAFLT_MASK32) == true)
    {
        setBitmap(sta.dcct_flts, 0x01);
    }

    if (testBitmap(digitalIoGetInputs(), DIG_IP_DCCTBFLT_MASK32) == true)
    {
        setBitmap(sta.dcct_flts, 0x02);
    }

    dpcom.mcu.meas.dcct_flt = (uint32_t)sta.dcct_flts;
}



// ---------- External function definitions

void statusProcess(void)
{
    // Latched Status

    statusProcessStLatched();

    // Faults

    statusProcessFaults();

    // Warnings

    statusProcessWarnings();

    // Unlatched status

    statusProcessStUnlatched();

    // DCCT status for DSP

    statusProcessDCCT();

    // Polarity switch

    polSwitchProcess();
}



void statusFaultsClr(uint32_t const bitmap)
{
    // Un-latch the specified fault(s), but only if they are not set in the source variables
    FAULTS = (FAULTS & ~bitmap) | status_faults | (uint16_t) dpcom.dsp.faults;
}



void statusFaultsReset(void)
{
    FAULTS = status_faults | (uint16_t) dpcom.dsp.faults;
}



void statusLatchedReset(void)
{
    ms_task.dspFltWarning = false;

    dpcom.dsp.latched = 0;
    statusClrLatched(0xFFFF);
    ST_LATCHED        = 0;
}



bool statusIsPcPermit(void)
{
    // Always return true if we are simulating interlocks. Otherwsie,
    // evaluate the PC_PERMIT input

    return (   (   stateOpGetState()         == FGC_OP_SIMULATION
                && digitalIoGetVsSimIntlks() == FGC_CTRL_ENABLED)
            || testBitmap(digitalIoGetInputs(), DIG_IP_PCPERMIT_MASK32) == true);
}


// EOF
