//! @file  stateVs.c
//! @brief VS state processing


// ---------- Includes

#include <bitmap.h>
#include <defconst.h>
#include <digitalIo.h>
#include <dpcls.h>
#include <fbs_class.h>
#include <memmap_mcu.h>
#include <pc_state.h>
#include <stateVs.h>



// ---------- External variable definitions

struct vs_vars vs;



// ---------- External function definitions

void stateVsProcess(void)
{
    uint16_t idx;
    uint16_t state_vs;

    static uint8_t vs_state[] =
    {
                            //  IDX  OPBLOCKED:VSRUN:VSPOWERON:VSREADY:FASTABORT:VSFAULT|VSEXTINTLK
        FGC_VS_OFF,         //   0       0       0       0       0        0        0
        FGC_VS_FLT_OFF,     //   1       0       0       0       0        0        1
        FGC_VS_FASTPA_OFF,  //   2       0       0       0       0        1        0
        FGC_VS_FLT_OFF,     //   3       0       0       0       0        1        1
        FGC_VS_INVALID,     //   4       0       0       0       1        0        0
        FGC_VS_INVALID,     //   5       0       0       0       1        0        1
        FGC_VS_INVALID,     //   6       0       0       0       1        1        0
        FGC_VS_INVALID,     //   7       0       0       0       1        1        1
        FGC_VS_STOPPING,    //   8       0       0       1       0        0        0
        FGC_VS_FAST_STOP,   //   9       0       0       1       0        0        1
        FGC_VS_FAST_STOP,   //  10       0       0       1       0        1        0
        FGC_VS_FAST_STOP,   //  11       0       0       1       0        1        1
        FGC_VS_INVALID,     //  12       0       0       1       1        0        0
        FGC_VS_INVALID,     //  13       0       0       1       1        0        1
        FGC_VS_INVALID,     //  14       0       0       1       1        1        0
        FGC_VS_INVALID,     //  15       0       0       1       1        1        1
        FGC_VS_STARTING,    //  16       0       1       0       0        0        0
        FGC_VS_FLT_OFF,     //  17       0       1       0       0        0        1
        FGC_VS_FASTPA_OFF,  //  18       0       1       0       0        1        0
        FGC_VS_FLT_OFF,     //  19       0       1       0       0        1        1
        FGC_VS_INVALID,     //  20       0       1       0       1        0        0
        FGC_VS_INVALID,     //  21       0       1       0       1        0        1
        FGC_VS_INVALID,     //  22       0       1       0       1        1        0
        FGC_VS_INVALID,     //  23       0       1       0       1        1        1
        FGC_VS_STARTING,    //  24       0       1       1       0        0        0
        FGC_VS_FAST_STOP,   //  25       0       1       1       0        0        1
        FGC_VS_FAST_STOP,   //  26       0       1       1       0        1        0
        FGC_VS_FAST_STOP,   //  27       0       1       1       0        1        1
        FGC_VS_READY,       //  28       0       1       1       1        0        0
        FGC_VS_INVALID,     //  29       0       1       1       1        0        1
        FGC_VS_INVALID,     //  30       0       1       1       1        1        0
        FGC_VS_INVALID,     //  31       0       1       1       1        1        1
        FGC_VS_OFF,         //  32       1       0       0       0        0        0
        FGC_VS_FLT_OFF,     //  33       1       0       0       0        0        1
        FGC_VS_FASTPA_OFF,  //  34       1       0       0       0        1        0
        FGC_VS_FLT_OFF,     //  35       1       0       0       0        1        1
        FGC_VS_INVALID,     //  36       1       0       0       1        0        0
        FGC_VS_INVALID,     //  37       1       0       0       1        0        1
        FGC_VS_INVALID,     //  38       1       0       0       1        1        0
        FGC_VS_INVALID,     //  39       1       0       0       1        1        1
        FGC_VS_STOPPING,    //  40       1       0       1       0        0        0
        FGC_VS_FAST_STOP,   //  41       1       0       1       0        0        1
        FGC_VS_FAST_STOP,   //  42       1       0       1       0        1        0
        FGC_VS_FAST_STOP,   //  43       1       0       1       0        1        1
        FGC_VS_INVALID,     //  44       1       0       1       1        0        0
        FGC_VS_INVALID,     //  45       1       0       1       1        0        1
        FGC_VS_INVALID,     //  46       1       0       1       1        1        0
        FGC_VS_INVALID,     //  47       1       0       1       1        1        1
        FGC_VS_STARTING,    //  48       1       1       0       0        0        0
        FGC_VS_FLT_OFF,     //  49       1       1       0       0        0        1
        FGC_VS_FASTPA_OFF,  //  50       1       1       0       0        1        0
        FGC_VS_FLT_OFF,     //  51       1       1       0       0        1        1
        FGC_VS_INVALID,     //  52       1       1       0       1        0        0
        FGC_VS_INVALID,     //  53       1       1       0       1        0        1
        FGC_VS_INVALID,     //  54       1       1       0       1        1        0
        FGC_VS_INVALID,     //  55       1       1       0       1        1        1
#if (FGC_CLASS_ID == 62)
        FGC_VS_BLOCKED,     //  56       1       1       1       0        0        0
#else
        FGC_VS_STARTING,    //  56       1       1       1       0        0        0
#endif
        FGC_VS_FAST_STOP,   //  57       1       1       1       0        0        1
        FGC_VS_FAST_STOP,   //  58       1       1       1       0        1        0
        FGC_VS_FAST_STOP,   //  59       1       1       1       0        1        1
        FGC_VS_BLOCKED,     //  60       1       1       1       1        0        0
        FGC_VS_INVALID,     //  61       1       1       1       1        0        1
        FGC_VS_INVALID,     //  62       1       1       1       1        1        0
        FGC_VS_INVALID      //  63       1       1       1       1        1        1
    };

    if (vs.present == FGC_CTRL_DISABLED)
    {
        STATE_VS           = FGC_VS_NONE;
        dpcom.mcu.state_vs = FGC_VS_NONE;

        return;
    }

    // Create state index from the five direct digital inputs

    idx = 0;

    if (testBitmap(digitalIoGetInputs(), (DIG_IP_VSFAULT_MASK32 | DIG_IP_VSEXTINTLK_MASK32)))
    {
        setBitmap(idx, 0x01);
    }

    if (testBitmap(digitalIoGetInputs(), DIG_IP_FASTABORT_MASK32))
    {
        setBitmap(idx, 0x02);
    }

    //    DIG_IP_VSREADY_MASK32
    //      0: VS is not ready
    //      1: VS has finished its initialisation

    if (testBitmap(digitalIoGetInputs(), DIG_IP_VSREADY_MASK32))
    {
        setBitmap(idx, 0x04);
    }

    if (testBitmap(digitalIoGetInputs(), DIG_IP_VSPOWERON_MASK32))
    {
        setBitmap(idx, 0x08);
    }

    if (testBitmap(digitalIoGetInputs(), DIG_IP_VSRUN_MASK32))
    {
        setBitmap(idx, 0x10);
    }

    if (   testBitmap(digitalIoGetInputs(), DIG_IP_OPBLOCKED_MASK32)
        && dpcls.mcu.vs.blockable != FGC_VS_BLOCK_DISABLED)
    {
        setBitmap(idx, 0x20);
    }

    state_vs = vs_state[idx];

#if (FGC_CLASS_ID == 63)

    // If VSRUN active but VSREADY and/or VSPOWERON are not, and
    // converter is NOT starting

    if (   state_vs     == FGC_VS_STARTING
        && pcStateGet() != FGC_PC_STARTING)
    {
        // Mark state as invalid

        state_vs = FGC_VS_INVALID;
    }

#endif

    STATE_VS           = state_vs;
    dpcom.mcu.state_vs = state_vs;
}



bool stateVsIsOff(void)
{
    return (STATE_VS == FGC_VS_FLT_OFF || STATE_VS == FGC_VS_OFF);
}


// EOF
