//! @file  setif.c
//! @brief Set Command Condition Functions


#define SETIF_GLOBALS
#define DEFPROPS_INC_ALL    // defprops.h

#include <setif.h>
#include <calibration.h>
#include <cmd.h>
#include <config.h>
#include <crate.h>
#include <defprops.h>
#include <pars.h>
#include <dpcls.h>
#include <fbs.h>
#include <fbs_class.h>
#include <fgc_errs.h>
#include <defconst.h>
#include <set.h>
#include <sharedMemory.h>
#include <pc_state.h>
#include <stateOp.h>
#include <polSwitch.h>
#include <status.h>
#include <transaction.h>


/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifFbsCmd(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is command from the FIELDBUS (FIP or ETH)?
                        This is mainly to ensure that the command is passed via a tool with secured access,
                        such as FGCRun+.
                        Note: Commands typed on the remote console are considered non-Fieldbus since in
                        that case the Fieldbus is only a transport pipe for the SCM task.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (c != &tcm)
    {
        return (0);
    }

    return (FGC_USE_FGCRUN_PLUS);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifConfigModeOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Can the user set CONFIG.MODE?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (   stateOpGetState()           != FGC_OP_CALIBRATING
        && configGetState()            != FGC_CFG_STATE_STANDALONE
        && configGetMode()             == FGC_CFG_MODE_SYNC_NONE
        && configManagerNotAvailable() == false)
    {
      return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifOpNormalSim(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is operational state NORMAL or SIMULATION and the Dallas ID scan
                        or the configuration is not in progress?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (   (   stateOpGetState() == FGC_OP_NORMAL
            || stateOpGetState() == FGC_OP_SIMULATION)
        && shared_mem.mainprog_seq == SEQUENCE_MAIN_RUNNING   // Dallas ID scan in progress
        && configGetMode()         != FGC_CFG_MODE_SYNC_FGC)  // configuration in progress
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifStandalone(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is FGC running standalone (no FIP connector plugged in)?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (configGetState() == FGC_CFG_STATE_STANDALONE)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifOpTestingSim(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition:     Is operational state TEST or SIMULATION?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (   stateOpGetState() == FGC_OP_TEST
        || stateOpGetState() == FGC_OP_SIMULATION)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifRefOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: PPM reference is always accepted, whilst a non-PPM reference (0) is only
                    accepted when the state is FGC_PC_IDLE.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifPcCycling(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is PC State ARMED?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (pcStateGet() == FGC_PC_CYCLING)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifPcArmed(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is PC State ARMED?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (pcStateGet() == FGC_PC_ARMED)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifPcNotCycling(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is STATE.PC not CYCLING?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (   pcStateGet() != FGC_PC_TO_CYCLING
        && pcStateGet() != FGC_PC_CYCLING)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifCalActiveOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Can CAL.ACTIVE be set
\*---------------------------------------------------------------------------------------------------------*/
{
    if ( stateOpGetState() == FGC_OP_NORMAL
      && configGetMode()   == FGC_CFG_MODE_SYNC_NONE
#if (FGC_CLASS_ID == 63)
      && (uint16_t)dpcls.dsp.meas.i_zero_f
#endif
      && !SetifPcOff(c))
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifMpxOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is operational state TEST and adc interface is present?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (   shared_mem.analog_card_model
        && (stateOpGetState() == FGC_OP_TEST))
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifModeOpOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Can the user set MODE.OP?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (   stateOpGetState() != FGC_OP_NORMAL
        || crate.type == FGC_CRATE_TYPE_RECEPTION
        || (   pcStateTest(FGC_STATE_LE_STOPPING_BIT_MASK) == true
#if (FGC_CLASS_ID == 63)
            && statusTestUnlatched(FGC_UNL_LOW_CURRENT) == true
#endif
       ))
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifOpNotCal(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is operational state not CALIBRATING
\*---------------------------------------------------------------------------------------------------------*/
{
    if (stateOpGetState() != FGC_OP_CALIBRATING)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifRefDac(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is operational state TEST
\*---------------------------------------------------------------------------------------------------------*/
{
    if (stateOpGetState() == FGC_OP_TEST)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifResetOk(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is the power converter OFF or STOPPING
\*---------------------------------------------------------------------------------------------------------*/
{
    if (   pcStateTest(FGC_STATE_LE_BLOCKING_BIT_MASK) == true
        || STATE_VS == FGC_VS_INVALID)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifPcOff(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
  Set If Condition: Is Power Converter OFF?
\*---------------------------------------------------------------------------------------------------------*/
{
    if (pcStateGet() == FGC_PC_FLT_OFF ||
        pcStateGet() == FGC_PC_OFF)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetifRefUnlock(struct cmd * c)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{

    // Set If Condition: Is REF function lock DISABLED?
    //   NB: Keep the Bad State error for non-cycling or non-PPM operations, in
    //   the case the reference is already ARMED. In this case, the procedure
    //   to edit the reference is first to disarm it (with a S REF.FUNC.TYPE NONE)
    //   then to edit it and arm it again.


    uint16_t errnum = FGC_OK_NO_RSP;

    // If user 0 and ARMED or RUNNING state, return "bad state" error

    if (   c->cyc_sel == 0
        && pcStateTest(FGC_STATE_ARMED_BIT_MASK | FGC_STATE_RUNNING_BIT_MASK) == true)
    {
        errnum = FGC_BAD_STATE;
    }

    return (errnum);
}

uint16_t SetifCalOk(struct cmd * c)
{
    bool state = (pcStateGet() == FGC_PC_OFF || pcStateGet() == FGC_PC_FLT_OFF);

    if (   stateOpGetState() == FGC_OP_CALIBRATING
        || state             == false
        || stateOpGetState() != FGC_OP_NORMAL)
    {
        return FGC_BAD_STATE;
    }

    return 0;
}



uint16_t SetifSwitchOk(struct cmd * c)
{
    bool polarity_ok;

    polarity_ok = (polSwitchGetState() == POLSWITCH_STATE_POSITIVE ||
                   polSwitchGetState() == POLSWITCH_STATE_NEGATIVE);

    if (pcStateTest(FGC_STATE_LE_BLOCKING_BIT_MASK) == true && polarity_ok == true)
    {
        return (0);
    }

    return (FGC_BAD_STATE);
}



uint16_t SetifTxIdOk(struct cmd * c)
{
    uint16_t errnum = FGC_OK_NO_RSP;

    if (pcStateGet() == FGC_PC_ARMED && c->cyc_sel == 0)
    {
        errnum = transactionGetId(0, 0) == 0 ? FGC_BAD_STATE : FGC_TRANSACTION_IN_PROGRESS;
    }

    return errnum;
}


// EOF
