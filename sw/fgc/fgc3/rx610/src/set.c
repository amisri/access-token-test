/*---------------------------------------------------------------------------------------------------------*\
  File:     set.c

  Purpose:  FGC MCU Software - Set Command Functions
\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#include <set.h>

#include <anaCard.h>
#include <calibration.h>
#include <class.h>
#include <config.h>
#include <cycSim.h>
#include <diag.h>
#include <dimDataProcess.h>
#include <dallasTask.h>
#include <dpcls.h>
#include <dpcom.h>
#include <fbs_class.h>
#include <fbs.h>
#include <fgc_errs.h>
#include <modePc.h>
#include <init.h>
#include <interFgc.h>
#include <logSpy.h>
#include <logRun.h>
#include <math.h>
#include <mcu.h>
#include <mcuDependent.h>
#include <memmap_mcu.h>
#include <panic.h>
#include <pars.h>
#include <prop.h>
#include <pub.h>
#include <regfgc3Pars.h>
#include <regfgc3Prog.h>
#include <regfgc3ProgFsm.h>
#include <regfgc3Task.h>
#include <regfgc3Pars.h>
#include <set_class.h>
#include <statePc.h>
#include <string.h>
#include <postMortem.h>
#include <bitmap.h>
#include <digitalIo.h>
#include <status.h>
#include <scivs.h>
#include <transaction.h>

#if (FGC_CLASS_ID == 62)
#include <ilc.h>
#endif

// For most classes -- but not all -- this is also declared in defprops.h
uint16_t SetResetFaults(struct cmd * c, struct prop * p);

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetChar(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    CHAR
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetConfig(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\

    SET     Magic number in non-volatile memory is reinstated, so configuration will be
            preserved
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (p->sym_idx)
    {
        case STP_UNSET:
            nvsClearMagicNumber();
            break;

        case STP_SET:
            nvsSetMagicNumber();
            break;
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetCore(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetCore is used to check and save the Core control table
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                  i;
    uint32_t                  total_length_words = 0;
    uint32_t                  addr_in_core       = NVRAM_COREDATA_32;
    struct Panic_core_ctrl  core_ctrl;


    for (i = 0; i < FGC_N_CORE_SEGS; i++)           // Calculate total length of segments
    {
        total_length_words += core.n_words[i];
    }

    if (total_length_words > NVRAM_COREDATA_W)      // If length is too long
    {
        memset(&core.n_words, 0, sizeof(core.n_words)); // Clear all lengths
        return (FGC_BAD_PARAMETER);                 // Report error
    }

    for (i = 0; i < FGC_N_CORE_SEGS; i++)           // Create core control table
    {
        core_ctrl.seg[i].addr     = core.addr[i];
        core_ctrl.seg[i].n_words  = core.n_words[i];
        core_ctrl.seg[i].pad      = 0xFFFF;
        core_ctrl.addr_in_core[i] = addr_in_core;
        addr_in_core += (2 * core.n_words[i]);
    }

    panicCoreTableSet(&core_ctrl);                        // Write table to FRAM and clear core data

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetFloat(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    FLOAT
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetInteger(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    uint8_t, int8_t, uint16_t, int16_t, uint32_t, int32_t including symlist properties
\*---------------------------------------------------------------------------------------------------------*/
{
    return (ParsSet(c, p));
}



uint16_t SetRegFgc3CrateCacheScan(struct cmd * c, struct prop * p)
{
    // Although the corresponding property (REGFGC3.SLOT_INFO) is of type STRING,
    // this Set operation does not take any parameters -- it just a triggers a scan.

    regFgc3TaskSetJob(REGFGC3_TASK_SCAN);
    regFgc3TaskAwake();

    return FGC_OK_NO_RSP;
}

uint16_t SetRegFgc3ProgMode(struct cmd * c, struct prop * p)
{
    uint16_t errnum;
    const struct sym_lst *sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum != FGC_OK_NO_RSP)
    {
        return (errnum);
    }

    regFgc3ProgSetMode(sym_const->value);
    return FGC_OK_NO_RSP;
}

uint16_t SetRegFgc3ProgFsmMode(struct cmd * c, struct prop * p)
{
    uint16_t errnum;
    const struct sym_lst * sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum != FGC_OK_NO_RSP)
    {
        return (errnum);
    }

    return regFgc3ProgFsmSetMode(sym_const->value);
}


//TODO: added to debug the program manager. Delete after testing.
uint16_t SetRegFgc3ProgAction(struct cmd *c, struct prop *p)
{
    uint16_t errnum;
    const struct sym_lst * sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum != FGC_OK_NO_RSP)
    {
        return (errnum);
    }
    return regFgc3ProgSetAction(sym_const->value);
}



/*
 * EPCCCS-4496: risk of race condition by setting the config mode before checking whether the action is
 * allowed or not, and rolling it back later on. See EPCCCS-4496.
 */
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetConfigMode(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Used to synchronize either the FGC or the DB
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t config_mode = configGetMode();
    uint16_t errnum;

    errnum = SetInteger(c, p);

    if (errnum != 0)
    {
        return (errnum);
    }

    if (   STATE_PC        != FGC_PC_OFF
        && STATE_PC        != FGC_PC_FLT_OFF
        && configGetMode() != FGC_CFG_MODE_SYNC_DB)
    {
        configSetMode(config_mode);
        return (FGC_BAD_STATE);
    }

    return(0);
}

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetReset(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetReset is used to allow a S PROP RESET for
  S CONFIG.STATE RESET
  S DIAG RESET
  S DLS RESET
  S ID RESET
  S LOG RESET
  S LOG.SPY RESET
  S DEVICE.RESETS RESET
  S SPY RESET
  S MEAS.STATUS RESET
  S FGC.ST_LATCHED RESET
  S REGFGC3.PROG RESET
  S REGFGC3.RAW RESET
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                  errnum;
    const struct sym_lst  * sym_const;
    static struct sym_lst   sym_lst_reset[] = { { STC_RESET, 0 }, { 0, 0 } };

    errnum = ParsScanSymList(c, "Y", sym_lst_reset, &sym_const);   // delimiter: '\0'

    if (errnum)
    {
        return (errnum);
    }

    // RESET is the only choice in the symlist

    switch (p->sym_idx)
    {
        case STP_STATE:             // CONFIG.STATE

            if (CmdGetChangedProps() == 0)
            {
                configSetState(FGC_CFG_STATE_SYNCHRONISED, false);
            }

            if (configGetMode() == FGC_CFG_MODE_SYNC_FGC)
            {
                // Signal the DSP that the properties have been retrieved from the DB

                dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_DB_INIT);

                initPostDb();
            }

            configSetMode(FGC_CFG_MODE_SYNC_NONE);

            break;

        case STP_PROG:
            regFgc3ProgSetState(FGC_REGFGC3_PROG_STATE_SYNCHRONIZED);
            break;

        case STP_DIAG:              // DIAG RESET

            diag.req_QSPI_reset = 1;                    // Request immediate diag reset

            // ToDo: I guess that as the reset is demanded from the property
            // cleaning also the number of reset counter is a nice effect
            // can this line stay?
            diag.sync_resets = 0;                       // DIAG.SYNC_RESETS
            break;

        case STP_DLS:               // DLS RESET

            dls.num_errors = 0;                         // Reset Dallas errors counter
            memset(&dls.rsp, 0, sizeof(dls.rsp));  // Reset response buffers
            PropSetNumEls(NULL, &PROP_FGC_DLS_GLOBAL, 0);   // Reset response buffer lengths for properties
            PropSetNumEls(NULL, &PROP_FGC_DLS_LOCAL,  0);
            PropSetNumEls(NULL, &PROP_FGC_DLS_MEAS_A, 0);
            PropSetNumEls(NULL, &PROP_FGC_DLS_MEAS_B, 0);
            statusClrLatched(FGC_LAT_DALLAS_FLT);
            clrBitmap(ST_LATCHED, FGC_LAT_DALLAS_FLT);
            break;

        case STP_FLASH_LOCKS:                   // CODE.FLASH_LOCKS

            nvsUnlock();
            NVRAM_MAGIC_FP = 0;                 // Clear NVRAM flash locks magic number
            nvsLock();
            break;

#if (FGC_CLASS_ID == 63)
        case STP_FW_DIODE:              // VS.FW_DIODE
            vs.fw_diode = FGC_VDI_NO_FAULT;
            break;

        case STP_FABORT_UNSAFE:
            vs.fabort_unsafe = FGC_VDI_NO_FAULT;
            break;

        case STP_ILC:             // REF.ILC
            dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_ILC_RESET);
            break;
#endif

#if (FGC_CLASS_ID == 62)
        case STP_ILC:             // REF.ILC
            ilcResetDataAll();
            break;
#endif

        case STP_LOG:               // LOG

            if (testBitmap(digitalIoGetOutputs(), DIG_OP_SET_VSRUNCMD_MASK16) == false) // If converter is not running
            {
                postMortemReset();

            }
            else
            {
                return (FGC_BAD_STATE);
            }

            break;

        case STP_NUM_RESETS:            // DEVICE.NUM_RESETS

            // ToDo: change this to be compatible
            logRunClrResets();
            break;

        case STP_SPY:               // SPY

            propInitSpyMpx(c);
            break;

        case STP_ST_LATCHED:            // STATUS.ST_LATCHED

            statusLatchedReset();
            break;

        case STP_VS:                // VS
        case STP_RESET:             // VS.RESET

            return (SetResetFaults(c, p));      // S VS RESET has the same effect as S STATUS.RESET_FAULTS
            break;

        case STP_FAULTS:    // INTER_FGC.STATUS.FAULTS

            interFgcResetRuntimeFaults();
            break;

        case STP_ID:

            dallasTaskBarcodesSet();
            break;

        case STP_RAW:

            regFgc3ParsRawReset(c);
            break;

        default:
            return FGC_BAD_PARAMETER;           // In case one defined this function as the set function
            // for a property and forgot to add a switch case for it
            break;

    }

    return (FGC_OK_NO_RSP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetRterm(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    NULL (CLIENT.RTERM)  This is a dummy function because the GW will intercept the set
        request so that it can apply an RBAC rule.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (FGC_SET_NOT_PERM);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetRtermLock(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:    NULL (CLIENT.RTERMLOCK)  This is a dummy function because the GW will intercept the set
        request so that it can apply an RBAC rule.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (FGC_SET_NOT_PERM);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetTerminate(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  SetTerminate is used by the S DEVICE.RESET/BOOT/PWRCYC/CRASH properties.
\*---------------------------------------------------------------------------------------------------------*/
{
    mcuTerminate(p->sym_idx);
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetResetFaults(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  This function will:
    - Clear the latched faults in STATUS.ST_LATCHED
    - Send the reset signal to the VS to clear the external faults
  It is called by doing a S VS RESET or a S STATUS.RESET_FAULTS.
\*---------------------------------------------------------------------------------------------------------*/
{
#if (FGC_CLASS_ID == 63)
    if (vs.fw_diode == FGC_VDI_FAULT)          // If a FW DIODE fault was received
    {
        return (FGC_FW_DIODE_FAULT);           // Block the reset request
    }

    if (vs.fabort_unsafe == FGC_VDI_FAULT)     // If a FW DIODE fault was received
    {
        return (FGC_FABORT_UNSAFE);            // Block the reset request
    }
#endif

    statePcResetFaults();

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetPC(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Set PC command
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t errnum;
    const struct sym_lst * sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    return (modePcSet(sym_const->value));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetPCSimplified(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Set PC Simplified command
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t errnum;
    const struct sym_lst * sym_const;

    errnum = ParsScanSymList(c, "Y", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    return (modePcSetSimplified(sym_const->value));

    return errnum;
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetAnaVrefTemp(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:        FLOAT

  Property:     CAL.VREF.TEMPERATURE.TARGET
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t errnum;

    errnum = ParsSet(c, p);

    if (!errnum)
    {
        anaCardTempRegulStart(*((float *)p->value));

        dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_ADC_TEMP);

        nvsUnlock();
        NVRAM_TARGET_TEMP_P = *((float *)p->value);
        nvsLock();

    }

    return (errnum);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetCal(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:        FLOAT

  This a special set function because it must be able to set manually the calibration factors for the
  ADCs, DCCTs and DAC as well launch auto-calibrations.

  S CAL ADCS                    Automatic calibration of all errors of all internal ADCs (channels A, B, C, D)
  S CAL EXT_ADC_A,{p}           Automatic calibration of the external ADC A error (p=CALZERO,CALPOS,CALNEG)
  S CAL EXT_ADC_B,{p}           Automatic calibration of the external ADC B error (p=CALZERO,CALPOS,CALNEG)
  S CAL DCCTS, {p}              Automatic calibration of an error for both DCCTs (p=CALZERO,CALPOS,CALNEG)
  S CAL DCCT_A,{p}              Automatic calibration of DCCT A error (p=CALZERO,CALPOS,CALNEG)
  S CAL DCCT_B,{p}              Automatic calibration of DCCT B error (p=CALZERO,CALPOS,CALNEG)
  S CAL DACS                    Automatic calibration of of both DACs

  Auto calibrations are requested to the DSP via the following variables:

  dpcom.mcu.cal.action          CAL_REQ_BOTH_ADC16S|CAL_REQ_ADC16|CAL_REQ_ADC22|CAL_REQ_DCCT|CAL_REQ_DAC
  dpcom.mcu.cal.chan_mask       FGC_DCCT_SELECT_A or FGC_DCCT_SELECT_B or FGC_DCCT_SELECT_AB
  dpcom.mcu.cal.idx             0=Offset, 1=GainPos, 2=GainNeg

  The function uses two symbol lists - only one can be defined per property in the XML (sym_lst_cal_type)
  while the second (sym_lst_cal_ref) must be defined statically in the function.

  For all the cases were {p} is defined as CALZERO, CALPOS or CALNEG the function will check that the
  measured voltage is within +/-CAL_VREF_LIMIT of the nominal value (0V,+10V,-10V) and will return
  REF_MISMATCH if not.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t                    errnum;
    uint16_t                    cal_type;
    const struct sym_lst    * sym_const;
    int32_t                    cal_signal_idx;
    uint32_t                    cal_chan_mask;
    enum Calibration_target   cal_target = CAL_TARGET_NONE;

    static const float   vref[3] = { 0.0, 10.0, -10.0 };

    static const struct sym_lst sym_lst_cal_ref[] =
    {
        {STC_CALZERO, DPCOM_CAL_OFFSET},
        {STC_CALPOS,  DPCOM_CAL_POSITIVE},
        {STC_CALNEG,  DPCOM_CAL_NEGATIVE},
        {0}
    };

    errnum = ParsScanSymList(c, "Y,", (struct sym_lst *)p->range, &sym_const);

    if (errnum)
    {
        return (errnum);
    }

    cal_type = sym_const->value;

    // Set cal_signal_idx

    switch (cal_type)
    {
        case FGC_CAL_TYPE_ADCS:
        case FGC_CAL_TYPE_DACS:

            if (c->token_delim == ',')
            {
                return (FGC_BAD_PARAMETER);
            }

            cal_signal_idx = 0;          // Not used for this type of calibration

            break;

        case FGC_CAL_TYPE_EXT_ADC_A:
        case FGC_CAL_TYPE_EXT_ADC_B:

            errnum = ParsScanSymList(c, "Y", (struct sym_lst *) &sym_lst_cal_ref, &sym_const);

            if (errnum)
            {
                return (errnum);
            }

            cal_signal_idx = sym_const->value;  // = CAL_IDX_OFFSET/POS/NEG

            break;

        case FGC_CAL_TYPE_DCCTS:
        case FGC_CAL_TYPE_DCCT_A:
        case FGC_CAL_TYPE_DCCT_B:

            errnum = ParsScanSymList(c, "Y", (struct sym_lst *) &sym_lst_cal_ref, &sym_const);

            if (errnum)
            {
                return (errnum);
            }

            cal_signal_idx = sym_const->value;  // = CAL_IDX_OFFSET/POS/NEG

            break;

        default:

            return (FGC_BAD_PARAMETER);

            break;
    }

    // cal.prop = p;

    // Set cal_chan_mask
    // and the calibration target

    cal_chan_mask = 0;

    switch (cal_type)
    {
            // ------------------------------------------------------------------------
            // Internal ADCs calibration
            // ------------------------------------------------------------------------

        case FGC_CAL_TYPE_ADCS:
            cal_chan_mask = 0x0F;
            cal_target  = CAL_TARGET_INT_ADCS;
            break;

            // ------------------------------------------------------------------------
            // DACs calibration
            // ------------------------------------------------------------------------

        case FGC_CAL_TYPE_DACS:
            cal_chan_mask = 0x03;
            cal_target    = CAL_TARGET_DACS;
            break;

            // ------------------------------------------------------------------------
            // External ADCs calibration
            // ------------------------------------------------------------------------

        case FGC_CAL_TYPE_EXT_ADC_A:
            cal_chan_mask = 0x01;
            cal_target    = CAL_TARGET_EXT_ADCS;
            break;

        case FGC_CAL_TYPE_EXT_ADC_B:
            cal_chan_mask = 0x02;
            cal_target    = CAL_TARGET_EXT_ADCS;
            break;

              // ------------------------------------------------------------------------
            // DCCTs calibration
            // ------------------------------------------------------------------------

        case FGC_CAL_TYPE_DCCTS:
        case FGC_CAL_TYPE_DCCT_A:
        case FGC_CAL_TYPE_DCCT_B:

            if (cal_type == FGC_CAL_TYPE_DCCTS || cal_type == FGC_CAL_TYPE_DCCT_A)           // DCCT channel A
            {
                cal_chan_mask |= 0x01;
            }

            if (cal_type == FGC_CAL_TYPE_DCCTS || cal_type == FGC_CAL_TYPE_DCCT_B)           // DCCT channel B
            {
                cal_chan_mask |= 0x02;
            }

            cal_target = CAL_TARGET_DCCTS;

            // Check the voltage reference is present

            if (((cal_chan_mask & 0x01) &&
                 fabs(dpcom.dsp.adc.volts_200ms[0] - vref[cal_signal_idx]) > CAL_VREF_LIMIT) ||
                ((cal_chan_mask & 0x02) &&
                 fabs(dpcom.dsp.adc.volts_200ms[1] - vref[cal_signal_idx]) > CAL_VREF_LIMIT))
            {
                return (FGC_REF_MISMATCH);
            }

            break;

        default:

            return (FGC_BAD_PARAMETER);
            break;
    }

    calibrationSetup(cal_target, cal_chan_mask, cal_signal_idx);   // Initiate Calibration sequence

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetDirectCommand(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Set direct command register:  0xRRSS     SS=Set mask, RR=Reset mask
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      errnum;
    uint32_t      value;

    errnum = ParsScanInteger(c, "Y", &value);

    if (errnum)
    {
        return (errnum);
    }

    if (value > 0xFFFF)
    {
        return (FGC_OUT_OF_LIMITS);
    }

    DIG_OP_P = (uint16_t)value;           // Write word direct (high byte is reset, low byte is set)

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SetInternalAdcMpx(struct cmd * c, struct prop * p)
/*---------------------------------------------------------------------------------------------------------*\
  Types:        INTERNAL_ADC_MPX
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t errnum = 0;
    uint32_t adc_idx, adc_mask;

    // Setif conditions (embedded in the set function)
    //
    // Access to the AUX channel is less constrained than to the other 3. To modify its mpx, use a command
    // of the form: S ADC.INTERNAL.MPX[3] CALZERO

    if (c->from == c->to && c->from == 3)
    {
        errnum = SetifOpNotCal(c);      // Setif condition to modify the multiplexing of channel AUX only
    }
    else
    {
        errnum = SetifMpxOk(c);         // Setif condition to modify the multiplexing of other channels
    }

    if (errnum)
    {
        return errnum;
    }

    // Parsing of the user mpx request
    //
    // Note: the property array (adc.internal_adc_mpx[]) is used to process the user request and to translate it,
    //       if possible, in a DSP request. In any case the property array is updated regularly by the lower
    //       priority BGP task to reflect the real state of the Analogue HW. Therefore the modification done by
    //       this function is temporary and the user should not notice it.

    errnum = ParsSet(c, p);     // Will update adc.internal_adc_mpx[]

    if (errnum)
    {
        return errnum;
    }

    // Check that the DSP is ready to take an MPX request

    if (dpcom.mcu.ana.req_f)
    {
        return (FGC_BUSY);
    }

    // Prepare the multiplexing request to the DSP
    //
    // The MPX request coming from the user is an MPX request per ADC channel. The goal here is to summarise
    // that request into a MPX state for the analogue bus and a mask of the ADC that must connect to that bus.
    // It is likely that the user request does not translate into a valid {anabus_input + adc_mask} DSP request,
    // in which case the user is warned with "invalid mpx request" message.

    dpcom.mcu.ana.anabus_input = FGC_INTERNAL_ADC_MPX_NC;       // Request field 1: anabus_input
    dpcom.mcu.ana.adc_mask     = 0;                             // Request field 2: adc_mask

    for (adc_idx = 0, adc_mask = 1; adc_idx < FGC_N_INT_ADCS; adc_idx++, adc_mask <<= 1)
    {
        if (adc.internal_adc_mpx[adc_idx] != FGC_INTERNAL_ADC_MPX_CH_A + adc_idx) // If link through crossbar
        {
            if (dpcom.mcu.ana.anabus_input == FGC_INTERNAL_ADC_MPX_NC)
            {
                dpcom.mcu.ana.anabus_input = adc.internal_adc_mpx[adc_idx];
            }
            else if (dpcom.mcu.ana.anabus_input != adc.internal_adc_mpx[adc_idx])
            {
                return (FGC_INVALID_MPX_REQUEST);     // Requested configuration is invalid (more than one input on the crossbar)
            }

            dpcom.mcu.ana.adc_mask |= adc_mask;
        }
    }

    // Send the request to the DSP

    dpcom.mcu.ana.req_f = TRUE;

    return (0);
}



uint16_t SetTransactionId(struct cmd * c, struct prop * p)
{
    uint16_t  * value;

    // Overwrite the error with FGC_INVALID_TRANSACTION_ID

    return (  ParsValueGet(c, p, (void**)&value) == FGC_OK_NO_RSP
            ? transactionSetId(c->sub_sel, c->cyc_sel, *value)
            : FGC_INVALID_TRANSACTION_ID);
}



uint16_t SetTransactionCommit(struct cmd * c, struct prop * p)
{
    uint16_t  * value;
    uint16_t    errnum = FGC_OK_NO_RSP;

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum == FGC_OK_NO_RSP)
    {
        errnum = transactionCommit(c->sub_sel, *value, NULL);
    }

    return errnum;
}



uint16_t SetTransactionRollback(struct cmd * c, struct prop * p)
{
    uint16_t  * value;
    uint16_t    errnum = FGC_OK_NO_RSP;

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum == FGC_OK_NO_RSP)
    {
        errnum = transactionRollback(c->sub_sel, *value);
    }

    return errnum;
}



uint16_t SetTransactionTest(struct cmd * c, struct prop * p)
{
    uint16_t  * value;
    uint16_t    errnum = FGC_OK_NO_RSP;

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum == FGC_OK_NO_RSP)
    {
        errnum = transactionTest(c->sub_sel, *value);
    }

    return errnum;
}



uint16_t SetTransactionAckCommitFail(struct cmd * c, struct prop * p)
{
    transactionAckCommitFailed(c->sub_sel, c->cyc_sel);

    return FGC_OK_NO_RSP;
}



uint16_t SetId(struct cmd * c, struct prop * p)
{
    // The max_elements field of the property structure has been used to specify the logical path

    char        * barcode;
    uint16_t      id;
    prop_size_t   num_ids      = p->n_elements;
    uint16_t      logical_path = p->max_elements;
    uint16_t      errnum       = FGC_OK_NO_RSP;

    for (id = 0; id < num_ids; id++)
    {
        if (ParsScanString(c, &barcode, "Y,") == FGC_OK_NO_RSP)
        {
            strncpy(dallasTaskGetDeviceInfo(logical_path, id)->barcode.c, barcode, FGC_ELEMENT_BARCODE_LEN);
        }
    }

    return errnum;
}



uint16_t SetDpram(struct cmd * c, struct prop * p)
{
    uint32_t dpram_size = p->n_elements / prop_type_size[p->type];

    if (   c->from >= dpram_size
        || c->to   >= dpram_size
        || c->from >  c->to)
    {
        return FGC_BAD_ARRAY_IDX;
    }

    uint32_t  * value;
    uint16_t    errnum = FGC_OK_NO_RSP;

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum == FGC_OK_NO_RSP)
    {
        uint32_t   size = prop_type_size[p->type] * (c->to - c->from + 1);
        uint32_t * dest = (uint32_t *)p->value;

        memcpy((void*)&dest[c->from], (void*)value, size);
    }

    return errnum;
}



uint16_t SetSimIntlks(struct cmd * c, struct prop * p)
{
    if (stateOpGetState() != FGC_OP_SIMULATION)
    {
        return FGC_BAD_STATE;
    }

    uint16_t errnum = FGC_OK_NO_RSP;

    errnum = ParsSet(c, p);

    if (errnum == FGC_OK_NO_RSP)
    {
        digitalIo.simIntlks == FGC_CTRL_DISABLED ? digitalIoEnableOutputs() :  digitalIoDisableOutputs();
    }

    return errnum;
}


// EOF
