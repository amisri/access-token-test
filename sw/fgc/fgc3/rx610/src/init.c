//! @file  init.h
//! @brief Initialization functions


// ---------- Includes

#include <stdint.h>

#include <anaCard.h>
#include <bitmap.h>
#include <class.h>
#include <cycSim.h>
#include <databases.h>
#include <device.h>
#include <diag.h>
#include <digitalIo.h>
#include <dsp.h>
#include <init.h>
#include <logEvent.h>
#include <logRun.h>
#include <logSpy.h>
#include <logTime.h>
#include <masterClockPll.h>
#include <mcu.h>
#include <modePc.h>
#include <nvs.h>
#include <polSwitch.h>
#include <postMortem.h>
#include <prop.h>
#include <rtd.h>
#include <statePc.h>
#include <stateTask.h>
#include <trm.h>



// ---------- Constants

#define INIT_BEGIN                                  0x10000000
#define INIT_DONE_ANA_CARD                          0x00000001
#define INIT_DONE_STATE_TASK                        0x00000002
#define INIT_DONE_DSP                               0x00000004
#define INIT_DONE_MCU_TIMERS_AND_INTERRUPTS         0x00000010
#define INIT_DONE_NVS                               0x00000020
#define INIT_DONE_RTD                               0x00000040
#define INIT_DONE_CYC_SIM                           0x00000080
#define INIT_DONE_ANA_TEMP                          0x00000100
#define INIT_DONE_DIAG_DIM_HASH                     0x00000200
#define INIT_DONE_DIAG_DIM_DB                       0x00000400
#define INIT_DONE_LOGSPY                            0x00000800
#define INIT_DONE_TRM                               0x00001000
#define INIT_DONE_DATABASES                         0x00002000
#define INIT_DONE_DEVICE                            0x00004000
#define INIT_DONE_DIGITALIO                         0x00008000


#define INIT_DEPENDENCY_ANA_CARD                    INIT_BEGIN
#define INIT_DEPENDENCY_STATE_TASK                  INIT_DONE_ANA_CARD
#define INIT_DEPENDENCY_DSP                         INIT_DONE_ANA_CARD
#define INIT_DEPENDENCY_MCU_TIMERS_AND_INTERRUPTS   INIT_DONE_ANA_CARD
#define INIT_DEPENDENCY_NVS                         INIT_DONE_DSP | INIT_DONE_STATE_TASK  | INIT_DONE_MCU_TIMERS_AND_INTERRUPTS
#define INIT_DEPENDENCY_DATABASES                   INIT_DONE_NVS
#define INIT_DEPENDENCY_DEVICE                      INIT_DONE_NVS | INIT_DONE_DATABASES
#define INIT_DEPENDENCY_DIGITALIO                   INIT_DONE_DEVICE
#define INIT_DEPENDENCY_RTD                         INIT_DONE_NVS
#define INIT_DEPENDENCY_CYC_SIM                     INIT_DONE_NVS
#define INIT_DEPENDENCY_ANA_TEMP                    INIT_DONE_NVS
#define INIT_DEPENDENCY_DIAG_DIM_HASH               INIT_DONE_NVS
#define INIT_DEPENDENCY_DIAG_DIM_DB                 INIT_DONE_NVS | INIT_DONE_DIAG_DIM_HASH
#define INIT_DEPENDENCY_LOGSPY                      INIT_DONE_DIAG_DIM_DB
#define INIT_DEPENDENCY_TRM                         INIT_DONE_LOGSPY



// ---------- Internal structures, unions and enumerations

typedef  void (*init_func)(void);

struct Init_field
{
    init_func   func;
    uint32_t    dependencies;
    uint32_t    initialized;
};



// ---------- Internal variable definitions

static uint32_t init_status = INIT_BEGIN;

static struct Init_field init_funcs[] =
{
    { anaCardInit,                  (uint32_t) INIT_DEPENDENCY_ANA_CARD,                  (uint32_t) INIT_DONE_ANA_CARD                  },
    { stateTaskInit,                (uint32_t) INIT_DEPENDENCY_STATE_TASK,                (uint32_t) INIT_DONE_STATE_TASK                },
    { dspStart,                     (uint32_t) INIT_DEPENDENCY_DSP,                       (uint32_t) INIT_DONE_DSP                       },
    { mcuEnableTimersAndInterrupts, (uint32_t) INIT_DEPENDENCY_MCU_TIMERS_AND_INTERRUPTS, (uint32_t) INIT_DONE_MCU_TIMERS_AND_INTERRUPTS },
    { nvsInit,                      (uint32_t) INIT_DEPENDENCY_NVS,                       (uint32_t) INIT_DONE_NVS                       },
    { databasesInit,                (uint32_t) INIT_DEPENDENCY_DATABASES,                 (uint32_t) INIT_DONE_DATABASES                 },
    { deviceInit,                   (uint32_t) INIT_DEPENDENCY_DEVICE,                    (uint32_t) INIT_DONE_DEVICE                    },
    { digitalIoInit,                (uint32_t) INIT_DEPENDENCY_DIGITALIO,                 (uint32_t) INIT_DONE_DIGITALIO                 },
    { rtdInit,                      (uint32_t) INIT_DEPENDENCY_RTD,                       (uint32_t) INIT_DONE_RTD                       },
    { cycSimInit,                   (uint32_t) INIT_DEPENDENCY_CYC_SIM,                   (uint32_t) INIT_DONE_CYC_SIM                   },
    { anaCardTempInit,              (uint32_t) INIT_DEPENDENCY_ANA_TEMP,                  (uint32_t) INIT_DONE_ANA_TEMP                  },
    { DiagDimHashInit,              (uint32_t) INIT_DEPENDENCY_DIAG_DIM_HASH,             (uint32_t) INIT_DONE_DIAG_DIM_HASH             },
    { DiagDimDbInit,                (uint32_t) INIT_DEPENDENCY_DIAG_DIM_DB,               (uint32_t) INIT_DONE_DIAG_DIM_DB               },
    { logSpyInit,                   (uint32_t) INIT_DEPENDENCY_LOGSPY,                    (uint32_t) INIT_DONE_LOGSPY                    },
    { trmInit,                      (uint32_t) INIT_DEPENDENCY_TRM,                       (uint32_t) INIT_DONE_TRM                       },
};



// ---------- External function definitions

void initInit(void)
{
    uint16_t dsp_load_error;

    mcuInit();

    ENABLE_INTERRUPTS();

    // load DSP main and run it

    dsp_load_error = dspLoadImage(DSP_CODE, DSP_CODE_SIZE);

    if (dsp_load_error != DSP_ERROR_OK)
    {
        logRunAddTimestamp();
        logRunAddEntry(FGC_RL_DSP_LOAD, &dsp_load_error);
    }

    memset((uint32_t *)(&dpcom), 0, sizeof(dpcom));
    memset((uint32_t *)(&dpcls), 0, sizeof(dpcls));

    dpcom.mcu.dsp_in_standalone = ((CPU_MODEL_P & CPU_MODEL_DSPSTAL_MASK8) != 0);

    shared_mem.mainprog_seq = SEQUENCE_MAIN_INIT;

    mcuStartClocks();

    // ToDo: try to encapsulate this call and share with boot
    // INTERNAL ADCs & DACs

    // Release analogue reset signal

    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_ANALOG_MASK16;

    // Releases FIP/ETHERNET hw reset, reset line must have an internal pull-up

    P9.DR.BIT.B7 = 1;

    InitQspiBus();

    hashInit();

    ethernetInit();

    masterClockPllInit();

    crateInit();

    polSwitchInit();

    statePcInit();

    modePcInit();

    regFgc3ProgFsmInit();

    eventsInit();

    logEventInit();

    logTimeInit();

    classInit();

    postMortemInit();

    propInitPropertyTree();

    mcuInitOS();
}



void initProcess(void)
{
    uint16_t init_funcs_total = sizeof(init_funcs)/sizeof(struct Init_field);
    uint16_t init_count       = 0;

    while (init_count < init_funcs_total)
    {
        for (uint8_t i = 0; i < init_funcs_total; i++)
        {
            if (   testAllBitmap(init_status, init_funcs[i].dependencies) == true
                && testBitmap   (init_status, init_funcs[i].initialized)  == false)
            {
                init_funcs[i].func();

                setBitmap(init_status, init_funcs[i].initialized);

                init_count++;
            }
        }
    }

    propInitSpyMpx(&tcm);

    // Signal that the MCU has finished its initialization and configuring the properties

    dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_INIT);
}



void initPostNvs(void)
{
    initClassPostNvs();

    regFgc3ProgMgrInit();
}


void initPostDb(void)
{
    // These functions must run again after the FGC has been synchronized with the DB

    anaCardTempInit();

    initClassPostDb();

    shared_mem.mainprog_seq = SEQUENCE_MAIN_RUNNING;
}


// EOF
