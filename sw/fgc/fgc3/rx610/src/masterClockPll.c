//! @file     masterClockPll.h
//! @brief    RT Clock Phase Locked Loop Functions.


// ---------- Includes

#include <masterClockPll.h>

#include <bitmap.h>
#include <defconst.h>
#include <dpcom.h>
#include <fbs.h>
#include <logRun.h>
#include <memmap_mcu.h>
#include <nvs.h>
#include <pll.h>



// ---------- Constants

// Macros

// Macro arguments are expanded except if with # or ##, so we need 2 level of indirection
#define CLASS_FIELD_STRING(name)        c##name
#define CLASS_FIELD_STRING2(name)       CLASS_FIELD_STRING(name)
#define CLASS_FIELD                     CLASS_FIELD_STRING2(FGC_CLASS_ID)   // c##FGC_CLASS_ID



// ---------- Internal function definitions

// Logs all the signals accessible from SPY

static void masterClockPllSpy(void)
{
    dpcom.mcu.pll.dac                 = pll.dac_ppm;
    dpcom.mcu.pll.e18_error           = pll.eth18.phase_err;
    dpcom.mcu.pll.e19_error           = pll.eth19.phase_err;
    dpcom.mcu.pll.error               = pll.pi_err;
    dpcom.mcu.pll.eth_sync_cal        = pll.eth_sync_cal;
    dpcom.mcu.pll.ext_avg_error       = pll.external.phase_err_average;
    dpcom.mcu.pll.ext_error           = pll.ext.phase_err;
    dpcom.mcu.pll.filtered_integrator = pll.filtered_integrator_ppm;
    dpcom.mcu.pll.integrator          = pll.integrator_ppm;
    dpcom.mcu.pll.net_avg_error       = pll.network.phase_err_average;
    dpcom.mcu.pll.state               = pll.state;
}



// ---------- External function definitions

void masterClockPllInit(void)
{
    pll.enabled = true;

    // for main

    fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll = FGC_PLL_NO_SYNC;

    pllInit(NVRAM_PLL_INTEGRATOR_PPM_P, NVRAM_PLL_ETH_SYNC_CAL_P);

    PLL_DACVCXO_INT_P  = pll.dacvcxo_int;
    PLL_DACVCXO_FRAC_P = pll.dacvcxo_frac;
}



void masterClockPllIteration(void)
{
    uint32_t  int_sync_time;
    uint16_t  previous_state;

    pll.ext.sync_time = PLL_EXTSYNCTIME_P;

    if (pll.enabled == true)
    {
        previous_state = pll.state;

        int_sync_time  = pllCalc(PLL_INTSYNCTIME_P);

        // Log new PLL state

        if (previous_state != pll.state)
        {
            logRunAddTimestamp();
            logRunAddEntry(FGC_RL_PLL_STATE, &pll.state);
        }

        // don't write/lock it until the algorithm is stabilised
        // signalled by the rst mask

        if (   testBitmap(int_sync_time, PLL_INTSYNCTIME_RST_MASK32) == false
            || fbsIsStandalone())
        {
            PLL_INTSYNCTIME_P = int_sync_time;
        }

        PLL_DACVCXO_INT_P  = pll.dacvcxo_int;
        PLL_DACVCXO_FRAC_P = pll.dacvcxo_frac;

        masterClockPllSpy();
    }

    if (pll.state == FGC_PLL_LOCKED)
    {
        // update NVRAM
        // Note : We write NVRAM at PLL iteration frequency.
        // The technology of the MRAM used in FGC3 does not have a limited number of write cycles

        nvsUnlock();

        NVRAM_PLL_INTEGRATOR_PPM_P  = pll.integrator_ppm;
        NVRAM_PLL_ETH_SYNC_CAL_P    = pll.eth_sync_cal;

        nvsLock();
    }


    // update status

    fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll = pll.state;

    if (fbs.u.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED)
    {
        static uint32_t prev_unix_time = 0;

        // Make sure the time sent by the Gateway is valid, that is, larger than 2022-01-01 00:00:00

        if (fbs.time_v.unix_time > 1640995200 && prev_unix_time != fbs.time_v.unix_time)
        {
            prev_unix_time = fbs.time_v.unix_time;
           
            // General case without seconds overflow

            UTC_SET_MS_P       = fbs.time_v.ms_time + 40 - fbs.time_v.ms_time % 20;
            UTC_SET_UNIXTIME_P = fbs.time_v.unix_time;
        }
    }
}


// EOF
