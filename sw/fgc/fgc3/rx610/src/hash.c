//! @file  hash.c
//! @brief Hash functions
//!
//! This file contains the functions to set up and search hash tables of FGC symbols
//! There are three seperate hash tables: Property symbols, constant symbols and
//! get option symbols


// ---------- Includes

#include <hash.h>
#include <defprops.h>
#include <fgc_parser_consts.h>
#include <panic.h>
#include <property.h>
#include <string.h>



// ---------- Constants

#define KEY_LEN_W   ((ST_MAX_SYM_LEN+1)/2)



// ---------- Internal variable definitions

static struct hash_entry * prop_hash_table  [PROP_HASH_SIZE];
static struct hash_entry * const_hash_table [CONST_HASH_SIZE];
static struct hash_entry * getopt_hash_table[GETOPT_HASH_SIZE];

static char dims_symbols [FGC_MAX_DIMS][ST_MAX_SYM_LEN];



// ---------- Internal function definitions

//! This function will generate a hash index from the symbol.  This is the result of adding all the words
//! of the symbol (with rollover) and then taking the modulus with the size of the hash table.

static uint16_t hashSymbol(uint16_t size, char const * key)
{
    uint16_t         hash_sum;
    uint16_t         ii;
    uint16_t const * key_ptr = (uint16_t *) key;

    hash_sum = 0;

    for (ii = 0; ii < KEY_LEN_W; ii++)
    {
        hash_sum += *(key_ptr++);
    }

    return (hash_sum % size);
}



//! This function will add the entry to the hash table.

static void hashInsert(struct hash_entry ** table, struct hash_entry * entry, uint16_t size)
{
    uint16_t            index;
    struct hash_entry * node;
    char                key[ST_MAX_SYM_LEN + 1];

    // Clear key array

    memset(key, 0, sizeof(key));

    // Transfer far symbol to near key and check the length does not exceed the space on the stack

    strcpy(key, entry->key.c);

    if (strlen(key) > ST_MAX_SYM_LEN)
    {
        panicCrash(0xBADD);
    }

    // Get root node in hash table for this key

    index = hashSymbol(size, key);
    node  = table[index];

    // Add hash entry into hash table

    if (node == NULL)
    {
        // Add first hash entry for this node index

        table[index] = entry;
    }
    else
    {
        // node index in use so add entry to end of the chain

        while (node->next != NULL)
        {
            node = node->next;
        }

        node->next = entry;
    }
}



//! This function will search the hash table for an entry with the near symbol sym.  It returns zero if no
//! match found, or the symbol index if successful.  The symbol can be assumed to be null terminated and no
//! longer than ST_MAX_SYM_LEN characters in length, but not nul padded.

static uint16_t hashFind(struct hash_entry * const * table, uint16_t size, char const * sym)
{
    uint16_t                  index;
    struct hash_entry const * node;
    char                      key[ST_MAX_SYM_LEN + 1];

    memset(key, 0, sizeof(key));

    strcpy(key, sym);

    // Look up hash table node for this symbol and return zero if node is empty

    node = table[ hashSymbol(size, key) ];

    if (node == NULL)
    {
        return 0;
    }

    // Search chain of nodes for a match between the symbol and the hash table entries

    while (node != NULL)
    {
        index = node->sym_idx;

        if (strcmp(node->key.c, sym) == 0)
        {
            // match - return the node index

            return index;
        }

        node = node->next;
    }

    // No match - return zero

    index = 0;

    return index;
}



// ---------- External function definitions

void hashInit(void)
{
    uint16_t ii;

    for (ii = 1; ii < N_PROP_SYMBOLS; ii++)
    {
        hashInsert(prop_hash_table, &SYM_TAB_PROP[ii], PROP_HASH_SIZE);
    }

    for (ii = 1; ii < N_CONST_SYMBOLS; ii++)
    {
        hashInsert(const_hash_table, &SYM_TAB_CONST[ii], CONST_HASH_SIZE);
    }

    for (ii = 1; ii < N_GETOPT_SYMBOLS; ii++)
    {
        hashInsert(getopt_hash_table, &sym_tab_getopt[ii], GETOPT_HASH_SIZE);
    }
}



uint8_t hashInsertDIM(char const * key, uint16_t n_dims)
{
    if(hashFindProp(key) != 0)
    {
        return 0;
    }

    strncpy(dims_symbols[n_dims], key, ST_MAX_SYM_LEN);
    SYM_TAB_PROP[N_PROP_SYMBOLS + n_dims].key.c = dims_symbols[n_dims];

    hashInsert(prop_hash_table, &SYM_TAB_PROP[N_PROP_SYMBOLS + n_dims], PROP_HASH_SIZE);

    return 1;
}



uint16_t hashFindProp(char const * prop_symbol)
{
    return (hashFind(prop_hash_table, PROP_HASH_SIZE, prop_symbol));
}



uint16_t hashFindConst(char const * const_symbol)
{
    return (hashFind(const_hash_table, CONST_HASH_SIZE, const_symbol));
}



uint16_t hashFindGetopt(char const * getopt_symbol)
{
    return (hashFind(getopt_hash_table, GETOPT_HASH_SIZE, getopt_symbol));
}


// EOF
