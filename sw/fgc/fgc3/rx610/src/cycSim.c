//! @file    cycSim.c
//! @brief   Module for simulating a supercycle
//!
//! This module receives two types of input: cycle information and event information.
//! Cycle information corresponds to which user the cycle is attributed to and how long it should last (number of basic periods).
//! Events are described by type, payload and when they should be triggered.
//!
//! The module then outputs the event information at the correct moment.
//!
//! Each supercycle is repeated with the current cycle information until indicated otherwise.
//! The events triggered are the same in each cycle.


// ---------- Includes

#include <dpcom.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <cycSim.h>
#include <fgc_event.h>
#include <fgc_fieldbus.h>
#include <time_fgc.h>



// ---------- Internal structures, unions and enumerations

struct CycSim_event_info
{
    int32_t     eventTime;
    uint16_t    delay_ms;
    uint8_t     type;
    uint8_t     trigger_type;
    uint8_t     payload;
    uint8_t     writeCounter;
};


union CycSim_cylce_info
{
    struct
    {
        uint8_t     sub_sel;
        uint8_t     cyc_sel;
        uint8_t     length;
        uint8_t     padding;
    } info;

    uint32_t        data;
};


struct CycSim_settings
{
    uint16_t    bpLengthMs;
    uint8_t     maxNumCycles;
    uint8_t     maxNumEvents;
    uint8_t     eventNumReps;
    uint8_t     eventRepPeriodMs;
};


struct CycSim_time
{
    struct CC_ms_time   cycStartTime;
    uint32_t            cycTimeMs;
};


struct CycSim
{
    struct CycSim_settings     settings;

    union CycSim_cylce_info  * cyclesActive;      //!< Array from which cycle information is read from
    union CycSim_cylce_info  * cyclesNext;        //!< Array to which new cycle information is written

    struct CycSim_event_info * events;

    bool                       simEnabled;
    bool                       dataUpdated;       //!< Triggered when cycle information is changed
    uint8_t                    currentCycleIndex;
    bool                       calculate_phase;   //!< Calculate the phase of the super-cycle when it is first enabled

    struct CycSim_time         timeInfo;
};



// ---------- External variable definitions

struct CycSim_prop cycsim_prop;



// ---------- Internal variable definitions

static struct CycSim cycSim;



// ---------- Internal function definitions

static uint16_t cycSimGetUser(bool const nextCycle)
{
    uint8_t indx = cycSim.currentCycleIndex;

    if (nextCycle == true)
    {
        indx += 1;

        if (indx >= cycSim.settings.maxNumCycles || cycSim.cyclesActive[indx].data == 0)
        {
            indx = 0;
        }
    }

    uint8_t sub_sel = cycSim.cyclesActive[indx].info.sub_sel;
    uint8_t cyc_sel = cycSim.cyclesActive[indx].info.cyc_sel;

    return (sub_sel << FGC_MULTI_PPM_SUB_SEL_SHIFT | cyc_sel);
}



static void cycSimCalculatePhase(struct CC_ms_time const * utcTime)
{
    // Algorithm to align the super-cycle simulation on all FGCs [EPCCCS-7479]:
    //
    // 1) Take the seconds that have elapsed since UTC midnight
    // 2) Calculate the phase of the supercycle based on the current UTC
    // 3) Based on the phase in milliseconds calculate which cycle should be active
    // 4) Calculate C0 for the first active cycle

    // Calculate the length of the super-cycle

    uint8_t  i;
    uint32_t length_ms = 0;

    for (i = 0; i < cycSim.settings.maxNumCycles && cycSim.cyclesActive[i].data != 0; ++i)
    {
        length_ms += cycSim.cyclesActive[i].info.length;
    }

    length_ms *= cycSim.settings.bpLengthMs;

    uint32_t day_s    = timeGetUtcTime() % 86400;
    uint32_t phase_ms = (day_s * 1000 + timeGetUtcTimeMs()) % length_ms;
    uint32_t iter_ms  = 0;

    // Search for the cycle based on the phase of the super-cycle and UTC

    i = 0;

    do
    {
        iter_ms += cycSim.cyclesActive[i].info.length * cycSim.settings.bpLengthMs;

    } while (iter_ms < phase_ms && ++i < cycSim.settings.maxNumCycles);

    cycSim.currentCycleIndex  = i;
    cycSim.timeInfo.cycTimeMs = phase_ms + cycSim.cyclesActive[i].info.length * cycSim.settings.bpLengthMs - iter_ms;

    // Calculate C0 for this cycle

    cycSim.timeInfo.cycStartTime = *utcTime;

    timeMsAddOffset(&cycSim.timeInfo.cycStartTime, -cycSim.timeInfo.cycTimeMs);
}



static void cycSimUpdateCycleInfo(struct CC_ms_time const * utcTime)
{
    if (cycSim.dataUpdated == true)
    {
        // The super-cycle has been modified: swap next/active pointers

        union CycSim_cylce_info * cyclesTemp;

        cyclesTemp          = cycSim.cyclesActive;
        cycSim.cyclesActive = cycSim.cyclesNext;
        cycSim.cyclesNext   = cyclesTemp;

        memset(cycSim.cyclesNext, 0, cycSim.settings.maxNumCycles * sizeof(union CycSim_cylce_info));

        cycSim.currentCycleIndex = 0;
        cycSim.dataUpdated       = false;

        // Check if the cycle simulation has been disabled

        if (cycSim.cyclesActive[0].data == 0)
        {
            cycSim.simEnabled      = false;
            cycSim.calculate_phase = true;

            return;
        }
    }

    if (cycSim.calculate_phase == true)
    {
        // The first time the super-cycle has been activated, calculate the phase based
        // on the current time

        cycSimCalculatePhase(utcTime);

        cycSim.calculate_phase = false;

        return;
    }

    // Prepare the next cycle in the super-cycle

    cycSim.currentCycleIndex++;

    if (   cycSim.currentCycleIndex >= cycSim.settings.maxNumCycles
        || cycSim.cyclesActive[cycSim.currentCycleIndex].data == 0)
    {
        cycSim.currentCycleIndex = 0;
    }

    cycSim.timeInfo.cycStartTime = *utcTime;
    cycSim.timeInfo.cycTimeMs    = 0;
}



static bool cycSimDetermineWriteTime(struct CycSim_event_info const * event, int32_t * writeTime, uint16_t const delay_ms)
{
    *writeTime = event->eventTime - delay_ms;

    if (*writeTime < 0)
    {
        *writeTime += (cycSim.settings.bpLengthMs * cycSim.cyclesActive[cycSim.currentCycleIndex].info.length);

        return(event->eventTime >= 0 ? true : false);
    }

    return false;
}



//! Finds and empty slot in the output array.

static uint8_t cycSimFindWritePosition(struct fgc_event const * simEvents)
{
    uint8_t i;

    for (i = 0; i < FGC_NUM_EVENT_SLOTS; ++i)
    {
        if (simEvents[i].type == 0)
        {
            return i;
        }
    }

    return FGC_NUM_EVENT_SLOTS;
}



//! Transfer event to the output array.

static void cycSimWriteToEventsArray(uint16_t           const delay_ms,
                                     uint8_t            const type,
                                     uint8_t            const trigger_type,
                                     uint8_t            const payloadType,
                                     bool               const nextUserPayload,
                                     struct fgc_event * const simEvents)
{
    uint8_t writeIndex = cycSimFindWritePosition(simEvents);

    if (writeIndex == FGC_NUM_EVENT_SLOTS)
    {
        return;
    }

    simEvents[writeIndex].type         = type;
    simEvents[writeIndex].trigger_type = trigger_type;
    simEvents[writeIndex].delay_us     = delay_ms * 1000;

    switch((enum CycSim_payload_type) payloadType)
    {
        case USER:  simEvents[writeIndex].payload = cycSimGetUser(nextUserPayload);  break;
        case GROUP: // Fall through
        case NONE:  // Fall through
        default:    simEvents[writeIndex].payload = 0;                               break;
    }
}



//! This function goes through the events array and determines if it is
//! the right time to write an event to the output array.

static void cycSimWriteEvents(struct fgc_event * const simEvents)
{
    memset(simEvents, 0, FGC_NUM_EVENT_SLOTS * sizeof(struct fgc_event));

    if (   cycSim.events[0].type == 0
        || cycSim.simEnabled     == false)
    {
        return;
    }

    // All other events

    int32_t  writeTime       = 0;
    uint16_t delay_ms        = 0;
    uint8_t  eventIndex      = 0;
    bool     nextUserPayload = false;

    for (eventIndex = 0 ; cycSim.events[eventIndex].type != 0 && eventIndex < cycSim.settings.maxNumEvents; eventIndex++)
    {
        // Test conditions for SSC event

        if (   cycSim.events[eventIndex].type == FGC_EVT_SSC
            && cycSim.currentCycleIndex       != 0)
        {
            continue;
        }

        delay_ms = cycSim.events[eventIndex].delay_ms - cycSim.events[eventIndex].writeCounter * cycSim.settings.eventRepPeriodMs;

        // Determine if an event is eligible to be written

        if (   delay_ms <= cycSim.settings.eventRepPeriodMs
            || cycSim.events[eventIndex].writeCounter >= cycSim.settings.eventNumReps)
        {
            cycSim.events[eventIndex].writeCounter = 0;
            continue;
        }

        nextUserPayload = cycSimDetermineWriteTime(&cycSim.events[eventIndex], &writeTime, delay_ms);

        // Check if it is time to write the event

        if (   cycSim.timeInfo.cycTimeMs >= writeTime
            && cycSim.timeInfo.cycTimeMs < (writeTime + cycSim.settings.eventRepPeriodMs))
        {
            cycSimWriteToEventsArray(delay_ms,
                                     cycSim.events[eventIndex].type,
                                     cycSim.events[eventIndex].trigger_type,
                                     cycSim.events[eventIndex].payload,
                                     nextUserPayload,
                                     simEvents);

            cycSim.events[eventIndex].writeCounter++;
        }
    }
}



// ---------- External function definitions

void cycSimInit(void)
{
    // FGC_BASIC_PERIOD_MS 1200
    // FGC_SIMSC_LEN 32
    // FGC_NUM_EVENT_SLOTS 4
    // FGC_FIELDBUS_CYCLE_PERIOD_MS 20
    // FGC_FIELDBUS_EVENT_MAX_TX_CYCLES 2

    static uint8_t FGC_FIELDBUS_EVENT_MAX_TX_CYCLES = 2;

    cycSimConfig(FGC_BASIC_PERIOD_MS,
                 FGC_SIMSC_LEN,
                 FGC_CYC_SIM_MAX_EVENTS,
                 FGC_FIELDBUS_EVENT_MAX_TX_CYCLES,
                 FGC_FIELDBUS_CYCLE_PERIOD_MS);

    cycSimClassInit();
}



void cycSimConfig(uint16_t const bpLenInfo,
                  uint8_t  const maxNumCycles,
                  uint8_t  const maxNumEvents,
                  uint8_t  const numReps,
                  uint8_t  const repeatPeriod)
{
    cycSim.cyclesActive = malloc(2 * maxNumCycles * sizeof(union CycSim_cylce_info));
    cycSim.cyclesNext   = &cycSim.cyclesActive[maxNumCycles];
    cycSim.events       = malloc(maxNumEvents * sizeof(struct CycSim_event_info));

    memset(cycSim.cyclesActive, 0, 2 * cycSim.settings.maxNumCycles * sizeof(union CycSim_cylce_info));
    memset(cycSim.events, 0, cycSim.settings.maxNumEvents * sizeof(struct CycSim_event_info));

    CC_ASSERT(cycSim.cyclesNext != NULL);
    CC_ASSERT(cycSim.events     != NULL);

    cycSim.settings.bpLengthMs        = bpLenInfo;
    cycSim.settings.maxNumCycles      = maxNumCycles;
    cycSim.settings.maxNumEvents      = maxNumEvents;
    cycSim.settings.eventNumReps      = numReps;
    cycSim.settings.eventRepPeriodMs  = repeatPeriod;

    cycSim.currentCycleIndex = 0;
    cycSim.simEnabled        = false;
    cycSim.dataUpdated       = false;
    cycSim.calculate_phase   = true;
    cycSim.timeInfo.cycStartTime.secs.abs  = 0;
    cycSim.timeInfo.cycStartTime.ms = 0;
    cycSim.timeInfo.cycTimeMs       = 0;

    // Define default timing events

    cycSimSetEventInfo(FGC_EVT_SSC, 0, NONE,  60, 80);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,   FGC_EVT_CYCLE_START, NONE, 900,  260);
    cycSimSetEventInfo(FGC_EVT_CYCLE_START, 0,                   USER, 600,  0);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,              FGC_EVT_SUB_DEVICE_CYCLE_START, NONE, 900,  260);
    cycSimSetEventInfo(FGC_EVT_SUB_DEVICE_CYCLE_START, 0,                              USER, 600,  0);
}



bool cycSimResetEventInfo(void)
{
    if (cycSim.simEnabled == true)
    {
        return false;
    }

    // memset is done at index 2 in order to not delete the SSC and CYCLE_START events

    memset(&cycSim.events[2], 0, (cycSim.settings.maxNumEvents - 2) * sizeof(struct CycSim_event_info));
    return true;
}



void cycSimSetCycleComplete(void)
{
    cycSim.dataUpdated = true;

    __sync_synchronize();

    cycSim.simEnabled  = true;
}



bool cycSimIsEnabled(void)
{
    return cycSim.simEnabled;
}



bool cycSimSetCycleInfo(uint8_t const sub_sel, uint8_t const cyc_sel, uint8_t const length)
{
    uint8_t i;

    for (i = 0; i < cycSim.settings.maxNumCycles; ++i)
    {
        if (cycSim.cyclesNext[i].data == 0)
        {
            cycSim.cyclesNext[i].info.sub_sel = sub_sel;
            cycSim.cyclesNext[i].info.cyc_sel = cyc_sel;
            cycSim.cyclesNext[i].info.length  = length;

            return true;
        }
    }

    return false;
}



bool cycSimSetEventInfo(enum fgc_events          type,
                        uint8_t                  trigger_type,
	                    enum CycSim_payload_type payloadType,
	                    uint16_t                 delay_ms,
	                    int16_t                  eventTime)
{
    // Check if simulation is enabled and that the time to write the event is a multiple of the event repetition period.
    // Otherwise, this event will never be triggered.

    if (   cycSim.simEnabled == true
        || (eventTime - delay_ms) % cycSim.settings.eventRepPeriodMs != 0)
    {
        return false;
    }

    uint8_t i;

    for (i = 0; i < cycSim.settings.maxNumEvents && cycSim.events[i].type != 0; ++i)
    {
        ;
    }

    if (i < cycSim.settings.maxNumEvents)
    {
        cycSim.events[i].type         = type;
        cycSim.events[i].trigger_type = trigger_type;
        cycSim.events[i].payload      = payloadType;
        cycSim.events[i].delay_ms     = delay_ms;
        cycSim.events[i].eventTime    = eventTime;
        cycSim.events[i].writeCounter = 0;

        return true;
    }
    return false;
}



void cycSimProcess(struct CC_ms_time const * utcTime, struct fgc_event * simEvents)
{
    if (cycSim.simEnabled == false)
    {
        return;
    }

    cycSim.timeInfo.cycTimeMs = (uint32_t)timeMsDiff(&cycSim.timeInfo.cycStartTime, utcTime);

    // Cycle change (C0)

    if (cycSim.timeInfo.cycTimeMs >= (cycSim.settings.bpLengthMs * cycSim.cyclesActive[cycSim.currentCycleIndex].info.length))
    {
        cycSimUpdateCycleInfo(utcTime);
    }

    cycSimWriteEvents(simEvents);
}


// EOF
