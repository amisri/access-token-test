//! @file  post_mortem.c
//! @brief This file handles FGCLogger and Gateway request


// ---------- Includes

#include <liblog.h>

#include <defconst.h>

#include <postMortem.h>
#include <bitmap.h>
#include <fbs.h>
#include <fbs_class.h>
#include <logSpy.h>
#include <msTask.h>
#include <pc_state.h>
#include <status.h>



// ---------- Internal structures, unions and enumerations

//! Internal global variables for the module post_mortem

struct Post_mortem
{
    struct LOG_mgr   * log_mgr;                //!< Structure declared in liblog used to access post_mortem.data

    struct Post_mortem_logger
    {
        bool           enabled;                //!< FGC logger enabled flag
        uint32_t       synched_mask;           //!< Bitmaks with logs frozen for the FGC logger (DIM or VsRegDsp)
    } logger;

    struct Post_mortem_gw
    {
        bool           enabled;                //!< PMD post-mortem enabled flag
    } pmd;
};



// ---------- Internal variable definitions

static struct Post_mortem post_mortem;



// ---------- External function definitions

void postMortemInit(void)
{
    memset(&post_mortem, 0, sizeof(post_mortem));

    post_mortem.log_mgr = (struct LOG_mgr *)&dpcom.log.log_mgr;
}



void postMortemProcess(void)
{
    // If the post-mortem logs are frozen, notify the Gateway to read the buffers

    if (   testBitmap(fbs.u.fieldbus_stat.ack, FGC_SELF_PM_REQ) == true
        && logPostmortemLogsFroze(post_mortem.log_mgr)          == true)
    {
        clrBitmap(fbs.u.fieldbus_stat.ack, FGC_SELF_PM_REQ);
    }
    
    // Tell the DSP if a post-mortem is active

    dpcom.mcu.log.post_mortem_active = statusTestUnlatched(FGC_UNL_LOG_PLEASE | FGC_UNL_POST_MORTEM);

    // FGC_UNL_LOG_PLEASE asserted if:
    //
    // * Slow abort            : logs not frozen - LOG.MENU.SLOW_ABORT_TIME  != 0
    // * Post-morte            : logs frozen     - post_mortem.fgc_log_frozen_mask != 0
    // * DIM trigger           : logs frozen     - post_mortem.synched_frozen_mask != 0
    // * VsReg DSP logs trigger: logs frozen     - post_mortem.synched_frozen_mask != 0

    if (statusTestUnlatched(FGC_UNL_LOG_PLEASE) == true)
    {
        if (   log_props.slow_abort_time.secs.abs     == 0
            && log_props.post_mortem_time.secs.abs    == 0
            && post_mortem.logger.synched_mask == 0)
        {
            statusClrUnlatched(FGC_UNL_LOG_PLEASE);
        }
    }

    // Unfreeze the logs if the PMD service and FGC logger have read them all out

    if (   logFreezeMaskGet(post_mortem.log_mgr)    != 0
        && statusTestUnlatched(FGC_UNL_LOG_PLEASE)  == false
        && statusTestUnlatched(FGC_UNL_POST_MORTEM) == false)
    {
        uint8_t menu_idx;

        for (menu_idx = 0; menu_idx < LOG_NUM_MENUS; menu_idx++)
        {
            logMenuStatusResetBitMask(&log_menus, menu_idx, LOG_MENU_STATUS_SAVE_BIT_MASK);
        }

        uint8_t log_idx;

        for (log_idx = 0; log_idx < LOG_NUM_LOGS; log_idx++)
        {
            logUnfreezeLog(post_mortem.log_mgr, log_idx);
        }
    }
}



void postMortemSetEnable(bool const pmd, bool const logger)
{
    post_mortem.pmd.enabled    = pmd;
    post_mortem.logger.enabled = logger;
}



void postMortemTriggerPm(void)
{
    // This constant holds the faults that are not triggered by the DSP (reference.c)

#if (FGC_CLASS_ID == 63)
    static uint32_t const FAULTS_NO_FAULTS_IN_OFF = ~(  FGC_FLT_MEAS
                                                      | FGC_FLT_REG_ERROR
                                                      | FGC_FLT_LIMITS
                                                      | FGC_FLT_I_RMS_LIM
                                                      | FGC_FLT_V_ERROR);
#else
    static uint32_t const FAULTS_NO_FAULTS_IN_OFF = 0xFFFFFFFF;
#endif

    // PMD event

    if (   post_mortem.pmd.enabled == true
        && statusTestUnlatched(FGC_UNL_POST_MORTEM) == false
        && testBitmap(statusGetFaults(), ~FGC_FLT_NO_PC_PERMIT) == true)
    {
        statusSetUnlatched(FGC_UNL_POST_MORTEM);

        setBitmap(fbs.u.fieldbus_stat.ack, FGC_SELF_PM_REQ);

        // Freeze all PM logs

        logFreezePostmortemLogs(post_mortem.log_mgr);
    }

    // FGC logger event

    if (post_mortem.logger.enabled == true)
    {
        // dpcom.dsp.log.slow_abort_faults          : in SLOW_ABORT a fault not in MODE.SLOW_ABORT_FAULTS occurred
        // statusTestFaults(FAULTS_NO_FAULTS_IN_OFF): any other fault occurred at any time

        if (   dpcom.dsp.log.slow_abort_faults           != 0
            || statusTestFaults(FAULTS_NO_FAULTS_IN_OFF) == true)
        {
            // Freeze the logs and set LOG.MENU.POST_MORTEM_TIME.

            uint8_t menu_idx;

            for (menu_idx = 0; menu_idx < LOG_NUM_MENUS; menu_idx++)
            {
                if (   logMenuIsSave(&log_menus, menu_idx)      == true
                    && (   logMenuIsAlias(&log_menus, menu_idx) == false
                        || logSpyClassIsActiveAlias(menu_idx)   == true))
                {
                    logMenuStatusSetBitMask(&log_menus, menu_idx, LOG_MENU_STATUS_SAVE_BIT_MASK);

                    if (menu_idx < LOG_NUM_LIBLOG_MENUS)
                    {
                        uint32_t const log_idx = logMenuIndexToLogIndex(&log_menus, menu_idx);

                        if (logIsFreezable(post_mortem.log_mgr, log_idx) == true)
                        {
                            logFreezeLog(post_mortem.log_mgr, log_idx);
                        }
                    }
                }
            }

            log_props.post_mortem_time = ms_task.time_us;

            statusSetUnlatched(FGC_UNL_LOG_PLEASE);
        }
    }
}



void postMortemTriggerPmExt(void)
{
    if (   post_mortem.pmd.enabled == true
        && statusTestUnlatched(FGC_UNL_POST_MORTEM) == false)
    {
        setBitmap(fbs.u.fieldbus_stat.ack, FGC_EXT_PM_REQ);
    }
}



void postMortemTriggerSa(void)
{
    // If SLOW_ABORT is caused by a lack of PC_PERMIT, trigger an external PM-buf event

    if (statusIsPcPermit() == false)
    {
        postMortemTriggerPmExt();
    }

    // FGC logger event

    if (post_mortem.logger.enabled == true)
    {
        // Set the SAVE flag but do not freeze the logs

        uint8_t menu_idx;

        for (menu_idx = 0; menu_idx < LOG_NUM_MENUS; menu_idx++)
        {
            if(   logMenuIsSave (&log_menus, menu_idx) == true
               && logMenuIsAlias(&log_menus, menu_idx) == false)
            {
                logMenuStatusSetBitMask(&log_menus, menu_idx, LOG_MENU_STATUS_SAVE_BIT_MASK);
            }
        }

        log_props.slow_abort_time = dpcom.dsp.log.slow_abort_time;

        statusSetUnlatched(FGC_UNL_LOG_PLEASE);
    }
}




void postMortemTriggerDim(uint32_t const dim_index)
{
    // Freeze the DIM log and set the SAVE flag

    if (post_mortem.logger.enabled == true)
    {
        uint8_t menu_idx = LOG_MENU_DIM + dim_index;

        if (logMenuIsSave(&log_menus, menu_idx) == true)
        {
            uint8_t log_idx = LOG_DIM + dim_index;

            logFreezeLog(post_mortem.log_mgr, log_idx);

            setBitmap(post_mortem.logger.synched_mask, 1 << log_idx);

            logMenuStatusSetBitMask(&log_menus, menu_idx, LOG_MENU_STATUS_SAVE_BIT_MASK);

            statusSetUnlatched(FGC_UNL_LOG_PLEASE);
        }
    }
}




void postMortemReset(void)
{
    // Reset the PM-buf event if active

    if (statusTestUnlatched(FGC_UNL_POST_MORTEM) == true)
    {
        statusClrUnlatched(FGC_UNL_POST_MORTEM);
        clrBitmap(fbs.u.fieldbus_stat.ack, FGC_SELF_PM_REQ);
    }

    // Reset the FGC logger event if active

    else if (statusTestUnlatched(FGC_UNL_LOG_PLEASE) == true)
    {
        log_props.post_mortem_time.secs.abs  = 0;
        log_props.post_mortem_time.us = 0;
        log_props.slow_abort_time.secs.abs   = 0;
        log_props.slow_abort_time.us  = 0;

        post_mortem.logger.synched_mask = 0;
    }
}



void postMortemSyncLog(uint8_t const log_idx)
{
    clrBitmap(post_mortem.logger.synched_mask, 1 << log_idx);
}


// EOF
