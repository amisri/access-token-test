//! @file  extendMask.c
//! @brief Provide a bit mask data structure with up to 256 bits.


// ---------- Includes

#include <bitmap.h>
#include <extendMask.h>
#include <memmap_mcu.h>
#include <mcuDependent.h>
#include <os_hardware.h>



// ---------- External variable definitions

uint16_t const MASK16[16] =
{
    0x0001, 0x0002, 0x0004, 0x0008,
    0x0010, 0x0020, 0x0040, 0x0080,
    0x0100, 0x0200, 0x0400, 0x0800,
    0x1000, 0x2000, 0x4000, 0x8000
};

uint16_t const IDX_MASK16[16] =
{
    0xFFFF, 0xFFFE, 0xFFFC, 0xFFF8,
    0xFFF0, 0xFFE0, 0xFFC0, 0xFF80,
    0xFF00, 0xFE00, 0xFC00, 0xF800,
    0xF000, 0xE000, 0xC000, 0x8000
};



// ---------- External function definitions

bool extendMaskScan(struct ExtendMask_gen * const extend_mask_ptr, uint8_t * idx_ptr)
{
    uint16_t mask_idx = *idx_ptr >> 4;
    uint16_t bit_idx  = *idx_ptr & 0xF;

    // If next set bit is not in the same mask_array element

    if (testBitmap(extend_mask_ptr->mask_array[mask_idx], IDX_MASK16[bit_idx]) == false)
    {
        mask_idx++;

        // If next set bit is not in the next mask_arrays either, start the search
        // from bit 0 in lowest mask_array element

        if (testBitmap(extend_mask_ptr->master, IDX_MASK16[mask_idx]) == false)
        {
            mask_idx = 0;
        }

        // Find first bit set. Answer in mask_idx.

        MCU_FF1(extend_mask_ptr->master & IDX_MASK16[mask_idx], mask_idx);

        bit_idx = 0;
    }

    // CPU_LSSB_F returns 0x1F in case it is passed 0 as input (no bit set)

    if (mask_idx >= 0x10)
    {
        return false;
    }

    // Find first bit set. Answer in bit_idx

    MCU_FF1(extend_mask_ptr->mask_array[mask_idx] & IDX_MASK16[bit_idx], bit_idx);

    // CPU_LSSB_F returns 0x1F in case it is passed 0 as input (no bit set)

    if (bit_idx >= 0x10)
    {
        return false;
    }

    // Write the index of the found bit in *idx_ptr

    *idx_ptr = (mask_idx << 4) | bit_idx;

    return true;
}



void extendMaskSetBit(struct ExtendMask_gen * const extend_mask_ptr, uint8_t idx)
{
    extend_mask_ptr->mask_array[idx >> 4] |= MASK16[idx & 0xF];
    extend_mask_ptr->master               |= MASK16[idx >> 4];
}



void extendMaskClrBit(struct ExtendMask_gen * const extend_mask_ptr, uint8_t idx)
{
    extend_mask_ptr->mask_array[idx >> 4] &= ~MASK16[idx & 0xF];

    if (extend_mask_ptr->mask_array[idx >> 4] == 0)
    {
        extend_mask_ptr->master &= ~MASK16[idx >> 4];
    }
}


// EOF
