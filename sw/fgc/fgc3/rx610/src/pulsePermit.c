//! @file  pulse_permit.c
//! @brief Provides the logic to block and unblock the converter based on the pulse permit
//!
//! This functionality is only required by the ComHV-PS chassis and the
//! Modulator converter, used in the RF system. The pulse permit signal is
//! managed by the RF system.


// ---------- Includes

#include <pulsePermit.h>
#include <bitmap.h>
#include <crate.h>
#include <digitalIo.h>
#include <modePc.h>
#include <pc_state.h>
#include <stateOp.h>
#include <trm.h>



// ---------- Internal function definitions

//! Sends command strings to the terminal message queue.
//!
//! The function gets a pointer to a string literal and sends it, character by character,
//! to the terminal message queue. It does not perform any validation of the given strings.
//!
//! @param[in] command A pointer to the command string literal

static void pulsePermitSendCommand(char const * const command)
{
    char const * c = command;

    while (*c)
    {
        // Post characters to the terminal queue.
        // Two casts suppress compiler warnings

        OSMsgPost(terminal.msgq, (void *)(intptr_t) *c++);
    }
}



// ---------- External function definitions

bool pulsePermitValid(void)
{
    if (stateOpGetState() == FGC_OP_SIMULATION)
    {
        return true;
    }

    if (testBitmap(digitalIoGetInputs(), DIG_IP_PULSEPERMIT_MASK32) == false)
    {
        return false;
    }

    return true;
}



void pulsePermitProcess(void)
{
    static bool pc_on_f = false;

    // This functionality is only meaningful to the ComHV-PS chassis
    // and the kystron modulator converter.

    if (   crateGetType() != FGC_CRATE_TYPE_COMHV_PS
        && crateGetType() != FGC_CRATE_TYPE_KLYSTRON_MOD)
    {
        return;
    }

    // Only unblock/block if the desired state is above BLOCKING. Otherwise
    // the converter is already unblocked and should remained so.

    if (pulsePermitValid() == true)
    {
        if (pcStateGet() == FGC_PC_BLOCKING && pc_on_f == true)
        {
            pc_on_f = false;

            pulsePermitSendCommand("!S MODE.PC_SIMPLIFIED ON;");
        }
    }
    else
    {
        if (   pcStateTest(FGC_STATE_GE_BLOCKING_BIT_MASK) == true
            && modePcGetInternal() == FGC_PC_BLOCKING
            && pc_on_f == false)
        {
            // When the Pulse Permit (INTLK SPARE) is enabled again, the FGC can
            // automatically transition back to it MODE.PC_SIMPLIFIED ON, unless
            // the desired state was already BLOCKING.

            pc_on_f = true;

            pulsePermitSendCommand("!S MODE.PC_SIMPLIFIED BLOCKING;");
        }
    }

    // Prevent MODE.PC_SIMPLIFIED ON if transitioning to OFF.

    if (pcStateTest(FGC_STATE_LT_BLOCKING_BIT_MASK) == true)
    {
        pc_on_f = false;
    }
}


// EOF
