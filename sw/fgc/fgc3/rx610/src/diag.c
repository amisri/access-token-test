//! @file    diag.c
//! @brief   QSPI related functions
//!
//! The DIPS board has an embedded DIM that is the first on both branch A and branch B, so it is
//! scanned at 100Hz.
//! The analogue channels are different for the two scans (there are 8 channels in all)
//! and are used to measure the voltage source output voltage (Vmeas) twice (x1 and x10)
//! and the +5V and +/-15V PSU voltages for the FGC and the FGC's analogue interface.
//! These are checked by the DSP against threshold values.
//! If the qspi bus is reset, these channels get out of sync for 2 20ms cycles, so the DSP must suppress
//! PSU monitoring for 2 cycles.
//! This is signalled by the MCU using dpcom.mcu.diag.reset.
//! This should be set to the number of cycles that need to be blocked (2).
//! It is decremented by the DSP every 20ms cycle.

#define DEFPROPS_INC_ALL


// ---------- Includes

#include <diag.h>

#include <class.h>
#include <cmd.h>
#include <databases.h>
#include <definfo.h>
#include <defprops.h>
#include <dimDataProcess.h>
#include <dpcom.h>
#include <fbs_class.h>
#include <fbs.h>
#include <fgc/fgc_db.h>
#include <logSpy.h>
#include <mcuDependent.h>
#include <mem.h>
#include <memmap_mcu.h>
#include <prop.h>
#include <sharedMemory.h>
#include <string.h>
#include <trm.h>
#include <time_fgc.h>
#include <stateOp.h>
#include <bitmap.h>
#include <status.h>


// Macro arguments are expanded except if with # or ##, so we need 2 level of indirection

#define CLASS_FIELD_STRING(name)        c##name
#define CLASS_FIELD_STRING2(name)       CLASS_FIELD_STRING(name)
#define CLASS_FIELD                     CLASS_FIELD_STRING2(FGC_CLASS_ID)   // c##FGC_CLASS_ID


// ---------- External variable definitions

struct diag_vars __attribute__((section("sram"))) diag;
struct meas_i_earth   meas_i_earth;



// ---------- External function definitions


void DiagDimHashInit(void)
{
    int n_dims = 0;
    char sym[ST_MAX_SYM_LEN + 1];

    for (int ii =  0; ii < FGC_MAX_DIMS; ii++)
    {
        if (SysDbDimEnabled(device.sys.sys_idx,ii))     // If this DIM index is occupied (0xFF=unused)
        {
            strncpy(sym, SysDbDimName(device.sys.sys_idx,ii), ST_MAX_SYM_LEN); // Transfer symbol to near memory
            if (hashInsertDIM(sym, n_dims)) n_dims++;
        }
    }
}



void DiagDimDbInit(void)
{
    fgc_db_t                      ii;             // Loop index
    fgc_db_t                      board_number;
    uint32_t                      dims_expected[QSPI_BRANCHES];   // Number of real dims expected per branch
    uint32_t           *          val_scm_ptr;
    char                        sym[ST_MAX_SYM_LEN + 1];
    struct prop        *        dim_props;
    struct TAccessToDimInfo  *  logical_dim_info_ptr;

    // If databases are not the same version or system is unknown then return immediately

    if (databasesOk() == false || !device.sys.sys_idx)
    {
        DiagSetPropNumEls(0);               // Reset the dynamic DIAG property lengths
        return;
    }

    // Prepare pointers to codes in flash memory

    // note : warning while debugging, if SYSDB is not in the ROM this will crash horrendously
    // overwriting the stack in the following for() statement

    val_scm_ptr = &tcm.prop_buf->blk.intu[0];

    // Set up DIM property structures

    dims_expected[QSPI_BRANCH_A] = dims_expected[QSPI_BRANCH_B] = 0;          // Clear DIM expected counters
    dim_props = &diag.dim_props[0];
    logical_dim_info_ptr = &diag.logical_dim_info[0];


    PropBufWait(&tcm);                      // Wait for buffer to be free


    for (ii = diag.n_dims = 0; ii < FGC_MAX_DIMS; ii++)
    {
        logical_dim_info_ptr->logical_dim = SysDbDimLogicalAddress(device.sys.sys_idx,ii);

        if (SysDbDimEnabled(device.sys.sys_idx,ii))     // If this DIM index is occupied (0xFF=unused)
        {
            diag.n_dims++;

            *(val_scm_ptr) = board_number = logical_dim_info_ptr->flat_qspi_board_number = SysDbDimBusAddress(device.sys.sys_idx,ii);

            val_scm_ptr++;

            strcpy(sym, SysDbDimName(device.sys.sys_idx,ii)); // Transfer symbol to near memory

            dim_props->sym_idx      = hashFindProp(sym);        // Look up symbol in property hash
            dim_props->type         = PT_STRING;
            dim_props->get_func_idx = PROP_DIAG_DIM_NAMES.get_func_idx;
            dim_props->n_elements   = dim_props->max_elements = FGC_N_DIM_DIG_BANKS * FGC_N_DIM_DIG_INPUTS;
            dim_props->value        = logical_dim_info_ptr;

            if (board_number < FGC_MAX_DIM_BUS_ADDR)
            {
                // 0..15 belongs to branch A=0, 16..31 belongs to branch B=1
                // Increment DIMs expected bus branch

                dims_expected[board_number / (FGC_MAX_DIM_BUS_ADDR / 2)]++;
            }

            dim_props++;
        }
        else                    // else DIM index is unused
        {
            *(val_scm_ptr) = FGC_DIAG_UNUSED_CHAN;

            val_scm_ptr++;
        }

        logical_dim_info_ptr++;
    }

    tcm.cached_prop  = &PROP_DIAG_BUS_ADDRESSES;        // Write SCM buffer to DIAG.BUS_ADDRESSES
    tcm.prop_blk_idx = 0;                   // Write buffer to first property block
    tcm.n_elements   = 0;

    PropBlkSet(&tcm, FGC_MAX_DIMS, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT);// Write SCM buffer
    PropSet(&tcm, &PROP_DIAG_DIMS_EXPECTED, &dims_expected, 2); // Write DIMS_EXPECTED to DSP property

    DiagSetPropNumEls(diag.n_dims);             // Set the property lengths

    DiagSetOffsetGain(&PROP_DIAG_GAINS,  STP_GAINS);
    DiagSetOffsetGain(&PROP_DIAG_OFFSETS, STP_OFFSETS);

    diagClassInit();

    // Scan for digital inputs with the high state labeled as WARNING

    for (uint8_t dim_idx = 0; dim_idx < FGC_MAX_DIMS; dim_idx++)
    {
        if (SysDbDimEnabled(device.sys.sys_idx, dim_idx) == false)
        {
            continue;
        }

        fgc_db_t dim_bus_addr = SysDbDimBusAddress(device.sys.sys_idx, dim_idx);
        fgc_db_t dim_type_idx = SysDbDimDbType(device.sys.sys_idx, dim_idx);

        for (uint8_t dig_idx = 0; dig_idx < FGC_N_DIM_DIG_BANKS; dig_idx++)
        {
            uint16_t enabled_mask = 0;

            for (uint8_t input_idx = 0; input_idx < FGC_N_DIM_DIG_INPUTS; input_idx++)
            {
                if (   DimDbIsDigitalInput         (dim_type_idx, dig_idx + 4, input_idx) == true
                    && strncmp(DimDbDigitalLabelOne(dim_type_idx, dig_idx + 4, input_idx), "WARNING", 7) == 0)
                {
                    enabled_mask |= 1 << input_idx;
                }
            }

            diag.warning_enabled[dim_bus_addr][dig_idx] = enabled_mask;
        }
    }

    qspi_misc.expected_retrieved = TRUE;
}



void DiagSetPropNumEls(uint16_t n_dims)
{
    PropSetNumEls(NULL, &PROP_DIAG_ANA,        n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_ANA_LBLS,   n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_DIG,        n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_DIG_LBLS,   n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_DIM_NAMES,  n_dims);
    PropSetNumEls(NULL, &PROP_DIAG_FAULTS,     n_dims);
}



void DiagSetOffsetGain(struct prop * p, uint16_t prop_idx)
{
    static float diag_offset_gain_array[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];
    fgc_db_t                       dim_type_idx;
    fgc_db_t                      logical_dim;
    fgc_db_t                      chan;
    float (* buf)[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];

    tcm.cached_prop = p;                // Link SCM structure to DSP property

    buf = &diag_offset_gain_array;

    PropBufWait(&tcm);                 // Wait for buffer to be free

    for (chan = 0; chan < FGC_N_DIM_ANA_CHANS; chan++)
    {
        for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
        {
            dim_type_idx = SysDbDimDbType(device.sys.sys_idx,logical_dim);
            if (SysDbDimEnabled(device.sys.sys_idx,logical_dim)) // If DIM channel is in use
            {
                if (prop_idx == STP_GAINS)                      // If DIAG.GAIN property
                {
                    (*buf)[chan][logical_dim] = DimDbAnalogGain(dim_type_idx,chan);
                }
                else if (prop_idx == STP_OFFSETS)               // else DIAG.OFFSET property
                {
                    (*buf)[chan][logical_dim] = DimDbAnalogOffset(dim_type_idx,chan);
                }
            }
        }
    }

    PropSetFar(&tcm, p, (void *)*buf, p->max_elements);

    MemSetWords(*buf, 0x0000, sizeof(float) * FGC_N_DIM_ANA_CHANS * FGC_MAX_DIMS / 2);     // Clear buffer
}



int32_t DiagFindAnaChan(const char * chan_name)
{
    fgc_db_t                       dim_type_idx;
    fgc_db_t                      logical_dim;
    fgc_db_t                      ana_reg;                                // 0-3
    char                        chan_name_near[ST_MAX_SYM_LEN + 1];


    strcpy(chan_name_near, chan_name);                            // Get near copy of channel name

    for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        dim_type_idx = SysDbDimDbType(device.sys.sys_idx,logical_dim);
        if (SysDbDimEnabled(device.sys.sys_idx,logical_dim)) // If DIM channel is in use
        {
            for (ana_reg = 0; ana_reg < FGC_N_DIM_ANA_CHANS; ana_reg++) // For each analog channel
            {
                if (DimDbIsAnalogChannel(dim_type_idx,ana_reg)
                    && !strcmp(DimDbAnalogLabel(dim_type_idx,ana_reg), chan_name_near)) // name matches
                {
                    return (ana_reg * FGC_MAX_DIMS + logical_dim);      // Return ANASIGS index
                }

            }
        }
    }

    return (-1);                // No match
}



bool DiagFindDigChan(uint16_t logical_dim, const char * chan_name,
                        uint16_t * data_idx, uint16_t * diag_mask)
{
    uint16_t                      mask;
    fgc_db_t                      bank_idx;       // 0 - 1
    fgc_db_t                      ana_reg;        // 0 - 3
    const fgc_db_t dim_type_idx = SysDbDimDbType(device.sys.sys_idx,logical_dim);

    for (bank_idx = 0; bank_idx < FGC_N_DIM_DIG_BANKS; bank_idx++)
    {
        mask = 1;

        for (ana_reg = 0; ana_reg < FGC_N_DIM_DIG_INPUTS; ana_reg++)
        {
            if (DimDbIsDigitalChannel(dim_type_idx,(bank_idx+4)) &&
                !strcmp(DimDbDigitalLabel(dim_type_idx,(bank_idx+4),ana_reg), chan_name))
            {
                *diag_mask = mask;

                // DIAG_MAX_ANALOG is 0x80
                // 16 DIM boards is 0x20
                // I guess this is to point to
                //  diag.data.r.digital_0[flat_qspi_board_number]
                // or
                //  diag.data.r.digital_1[flat_qspi_board_number]
                *data_idx = SysDbDimBusAddress(device.sys.sys_idx,logical_dim) + 0x80 + (0x20 * bank_idx);

                return (TRUE);
            }

            mask <<= 1;
        }
    }

    return (FALSE);
}



void diagUpdateMeas(void)
{
    // Stores one sample in the IEARTH log and MEAS.I_EARTH and MEAS.I_EARTH_CPCNT.

    float   i_earth;
    float   i_earth_cpcnt;
    int8_t  indx;

    // Are there 0, 1 or 4 channels?

    i_earth = 0.0;

    for (indx = 0; indx < LOG_I_EARTH_SIGS; ++indx)
    {
        if (stateOpGetState() != FGC_OP_SIMULATION)
        {
            i_earth = (  meas_i_earth.channel[indx] != -1
                       ? ((float *)&dim_collection.analogue_in_physical)[meas_i_earth.channel[indx]]
                       : 0.0);
        }
        else
        {
            i_earth = meas_i_earth.simulated;
        }

        dpcom.mcu.meas.i_earth[indx] = i_earth;

        if (meas_i_earth.max < fabs(i_earth))
        {
            meas_i_earth.max = fabs(i_earth);
        }
    }

    // Use the last I earth value for the property's value

    meas_i_earth.value = i_earth;

    // MEAS.I.EARTH_PCNT
    //
    // The I_EARTH value is published as a signed short in centi-percent of the trip limit specified
    // in LIMITS.I.EARTH (clipped to -32768 to +32767)

    i_earth_cpcnt = 1000.0 * i_earth / meas_i_earth.limit;

    meas_i_earth.cpcnt = i_earth_cpcnt >  32767 ?  32767 :
                         i_earth_cpcnt < -32768 ? -32768 :
                         (int16_t)i_earth_cpcnt;

    diagClassUpdateMeas();
}


static bool diagCheckDimWarningsBank(uint16_t const warning_enabled, uint16_t const dig_values)
{
    if (warning_enabled != 0 && dig_values != 0)
    {
        uint16_t mask    = 1;
        uint8_t  dig_idx = 0;

        // 12 inputs per bank

        for (; dig_idx < FGC_N_DIM_DIG_INPUTS; dig_idx++, mask <<= 1)
        {
            if ((warning_enabled & mask) != 0 && (dig_values & mask) != 0)
            {
                return true;
            }
        }
    }

    return false;
}



void diagCheckDimWarnings(void)
{
    // Scan for digital inputs with the high state labelled as WARNING

    statusClrUnlatched(FGC_UNL_DIM_WRN);

    for (uint8_t dim_idx = 0; dim_idx < FGC_MAX_DIMS; dim_idx++)
    {
        if (   diagCheckDimWarningsBank(diag.warning_enabled[dim_idx][0],
                                        diag.data.a.digital[0][dim_idx]) == true
            || diagCheckDimWarningsBank(diag.warning_enabled[dim_idx][1],
                                        diag.data.a.digital[1][dim_idx]) == true)
        {
            statusSetUnlatched(FGC_UNL_DIM_WRN);

            break;
        }
    }
}


// EOF
