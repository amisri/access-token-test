//! @file  rtd.c
//! @brief Global option line display


// ---------- Includes

#include <string.h>

#include <rtd.h>
#include <bitmap.h>
#include <class.h>
#include <config.h>
#include <defprops.h>
#include <diag.h>
#include <init.h>
#include <mcu.h>
#include <pc_state.h>
#include <pulsePermit.h>
#include <statePc.h>
#include <stateOp.h>
#include <status.h>



// ---------- Constants

#define RTD_FAULTS_LEN  27



// Field positions

#define RTD_POS_STATES          "21;9"
#define RTD_POS_FAULTS          "22;1"
#define RTD_POS_CONFIG          "23;5"
#define RTD_POS_PERMIT          "24;8"

#define RTD_POS_CPU_MCU         "23;73"
#define RTD_POS_CPU_DSP         "23;77"

#define RTD_POS_TOUT            "24;76"
#define RTD_POS_TIN             "24;70"

#define RTD_POS_PSU5            "20;1"
#define RTD_POS_PSUP15          "20;13"
#define RTD_POS_PSUM15          "20;25"

#define RTD_POS_OPT_LINE        "20;1"
#define RTD_OPTLINE_V_POS       20

#define RTD_DIAG_DATA_H_POS     1
#define RTD_DIAG_DATA_H_WIDTH   8



// ---------- Internal structures, unions and enumerations

struct Rtd_local
{
    bool     enabled;
    char     option_debug[TRM_BUF_SIZE];
    bool     option_debug_updated;
};



// ---------- External variable definitions

struct Rtd_vars __attribute__((section("sram"))) rtd;



// ---------- Internal function definitions

static bool rtdPrintStates(bool reset);
static bool rtdPrintFaults(bool reset);
static bool rtdPrintConfig(bool reset);
static bool rtdPrintPermit(bool reset);

static bool rtdPrintOptionOff  (bool reset);
static bool rtdPrintOptionDiag (bool reset);
static bool rtdPrintOptionPsu  (bool reset);
static bool rtdPrintOptionDebug(bool reset);



// ---------- Internal variable definitions

static struct Rtd_field_func rtd_field_funcs[] =
{
    { rtdPrintStates  },
    { rtdPrintFaults  },
    { rtdPrintConfig  },
    { rtdPrintPermit  },
    { rtdClassProcess },
    { NULL            }
};


static struct Rtd_field_int rtd_field_ints[] =
{
    { (int32_t *)&ms_task.cpu_usage.mcu, 0x7FFFFFFF, "%3d",  RTD_POS_CPU_MCU },
    { (int32_t *)&ms_task.cpu_usage.dsp, 0x7FFFFFFF, "%3d",  RTD_POS_CPU_DSP },
    { NULL                                                               },
};


static struct Rtd_field_temp rtd_field_temps[] =
{
    { &temp.fgc.out, 0xFFFF, RTD_POS_TOUT },
    { &temp.fgc.in,  0xFFFF, RTD_POS_TIN  },
    { NULL                                },
};


static struct Rtd_field_optional rtd_field_optional[] =
{
    { FGC_RTD_OFF   , rtdPrintOptionOff   },
    { FGC_RTD_DIAG  , rtdPrintOptionDiag  },
    { FGC_RTD_PSU   , rtdPrintOptionPsu   },
    { FGC_RTD_DEBUG , rtdPrintOptionDebug },
    { 0xFF,           NULL                },
};


static struct Rtd_process rtd_process[] =
{
    { rtdProcessFuncs,      { (void *)rtd_field_funcs,    (void *)rtd_field_funcs    } },
    { rtdProcessInts,       { (void *)rtd_field_ints,     (void *)rtd_field_ints     } },
    { rtdProcessTemps,      { (void *)rtd_field_temps,    (void *)rtd_field_temps    } },
    { rtdProcessOptionLine, { (void *)rtd_field_optional, (void *)rtd_field_optional } },
    { NULL                                                                             }
};


static struct Rtd_local    rtd_local  = { .enabled              = false,
                                          .option_debug         = { '\0'},
                                          .option_debug_updated = false };



// ---------- Internal function definitions

static bool rtdPrintStates(bool reset)
{
    static uint8_t       state_last_value[4] = { 0 };
    static char    const pll_str[]           = "LKCPFSNSFL";
    static char    const vs_str[]            = "IVFOPAOFFSSPSTRDBKNO";

    // Reset states

    if (reset == true)
    {
        memset(&state_last_value, 0, sizeof(state_last_value));

        return false;
    }

    // If a state has changed

    if (   STATE_PLL         != state_last_value[0]
        || stateOpGetState() != state_last_value[1]
        || STATE_VS          != state_last_value[2]
        || pcStateGet()      != state_last_value[3])
    {
        char const  * p;

        // Remember states

        state_last_value[0] = STATE_PLL;
        state_last_value[1] = stateOpGetState();
        state_last_value[2] = STATE_VS;
        state_last_value[3] = pcStateGet();

        rtdMoveCursor(RTD_POS_STATES, TERM_BOLD);

        // Display PLL state

        p = pll_str + 2 * state_last_value[0];

        fputc(*(p++), rtd.f);
        fputc(*p,     rtd.f);
        fputc('.',    rtd.f);

        // Display operational state

        p = stateOpGetStateName(state_last_value[1]);

        fputc(*(p++), rtd.f);
        fputc(*p,     rtd.f);


        if (state_last_value[1] != FGC_OP_UNCONFIGURED)
        {
            fputc('.', rtd.f);

            // Display voltage source state

            p = vs_str + 2 * state_last_value[2];

            fputc(*(p++), rtd.f);
            fputc(*p,     rtd.f);
            fputc('.',    rtd.f);

            // Display power converter state

            p = statePcGetStateName(state_last_value[3]);

            fputc(*(p++), rtd.f);
            fputc(*p,     rtd.f);
        }

        rtdRestoreCursor();

        return true;
    }

    return false;
}



static bool rtdPrintFaults(bool reset)
{
    static struct Rtd_field_string field_str = { "", RTD_POS_FAULTS, TERM_BOLD TERM_FG_RED };
    static char                    faults[RTD_FAULTS_LEN];

    // Reset faults

    if (reset == true)
    {
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));

        return false;
    }

    // Check if a new active fault has been detected

    uint16_t faults_idx = 1;
    uint8_t  remaining_space = RTD_FAULTS_LEN - 1;

    memset(faults, '\0', sizeof(faults));

    uint32_t active_faults = statusGetFaults();

    while (active_faults != 0 && remaining_space > 0)
    {
        if (testBitmap(active_faults, faults_idx) == true)
        {
            char    * fault     = CmdPrintSymLst(SYM_LST_FAULTS, faults_idx);
            uint8_t   fault_len = strlen(fault);

            if (remaining_space > fault_len)
            {
                strncat(faults, fault, RTD_FAULTS_LEN);
                strncat(faults, " ", 1);
                remaining_space -= (fault_len + 1);
                clrBitmap(active_faults, faults_idx);
            }
            else
            {
                // Write "..." at the end to signal there are more faults

                if (remaining_space > 2)
                {
                    snprintf(&faults[RTD_FAULTS_LEN - (remaining_space + 2)], 4, "...");
                }
                else
                {
                    snprintf(&faults[RTD_FAULTS_LEN - 4], 4, "...");
                }

                remaining_space = 0;
            }
        }

        faults_idx <<= 1;
    }

    return rtdPrintString(faults, &field_str);
}



static bool rtdPrintConfig(bool reset)
{
    static struct Rtd_field_string field_str         = { "", RTD_POS_CONFIG, TERM_BOLD };
    static uint16_t                prev_config_state = 0xFFFF;
    static uint16_t                prev_config_mode  = 0xFFFF;

    // Reset config

    if (reset == true)
    {
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));
        prev_config_state = 0xFFFF;
        prev_config_mode  = 0xFFFF;

        return false;
    }

    if (configGetState() == prev_config_state && configGetMode() == prev_config_mode)
    {
        return false;
    }

    prev_config_state = configGetState();
    prev_config_mode  = configGetMode();

    if (prev_config_state == FGC_CFG_STATE_STANDALONE)
    {
        return rtdPrintString("STANDALONE", &field_str);
    }
    else
    {
        char   config[23];
        char * state = &config[0];
        char * mode  = &config[9];

        memset(config, '\0', sizeof(config));

        if (prev_config_state == FGC_CFG_STATE_SYNCHRONISED)
        {
            strncpy(state, "SYNCED   ", 9);
        }
        else
        {
            strncpy(state, "UNSYNCED ", 9);
        }

        switch (prev_config_mode)
        {
            case FGC_CFG_MODE_SYNC_DB:
                strncpy(mode, "SYNC_DB", 7);
                break;

            case FGC_CFG_MODE_SYNC_DB_CAL:
                strncpy(mode, "SYNC_CAL", 8);
                break;

            case FGC_CFG_MODE_SYNC_FAILED:
                strncpy(mode, "SYNC_FAIL", 9);
                break;

            case FGC_CFG_MODE_SYNC_FGC:
                strncpy(mode, "SYNC_FGC", 8);
                break;

            case FGC_CFG_MODE_SYNC_NONE:  // Fall through

            default:
                // No action
                break;
        }

        return rtdPrintString(config, &field_str);
    }
}



static bool rtdPrintPermit(bool reset)
{
    static struct Rtd_field_string field_str         = { "", RTD_POS_PERMIT, TERM_BOLD };
    static bool                    prev_pc_permit    = false;
    static bool                    prev_pulse_permit = false;

    // Reset permits

    if (reset == true)
    {
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));
        prev_pc_permit    = false;
        prev_pulse_permit = false;

        return false;
    }

    if (   prev_pc_permit    == (testBitmap(digitalIoGetInputs(), DIG_IP_PCPERMIT_MASK32) == true)
        && prev_pulse_permit == pulsePermitValid())
    {
        return false;
    }

    prev_pc_permit    = (testBitmap(digitalIoGetInputs(), DIG_IP_PCPERMIT_MASK32) == true);
    prev_pulse_permit = pulsePermitValid();

    char   permit[8];
    char * src = permit;

    if (testBitmap(digitalIoGetInputs(), DIG_IP_PCPERMIT_MASK32) == true)
    {
        *src++ = 'P';
        *src++ = 'C';
    }
    else
    {
        *src++ = ' ';
        *src++ = ' ';
    }

    if (pulsePermitValid() == true)
    {
        *src++ = ' ';
        *src++ = 'P';
        *src++ = 'U';
        *src++ = 'L';
    }

    *src = '\0';

    return rtdPrintString(permit, &field_str);
}



static bool rtdPrintOptionOff(bool reset)
{
    rtdMoveCursor(RTD_POS_OPT_LINE, TERM_BOLD);

    // Erase the complete line

    fputs(TERM_CLR_LINE, rtd.f);

    rtdRestoreCursor();

    return true;
}



static bool rtdPrintOptionDiag(bool reset)
{
    static uint16_t  diag_chan[FGC_DIAG_RTD_LIST_LEN] = { 0 };
    static uint16_t  diag_data[FGC_DIAG_RTD_LIST_LEN] = { 0 };
    static uint16_t  diag_list_len = 0;
    static uint8_t   diag_data_idx = 0;

    if (reset == true)
    {
        memset(&diag_chan, 0xFF, sizeof(diag_data));

        diag_data_idx = 0;
    }

    // If the diag chan list length has changed

    if (diag.list_rtd_len != diag_list_len)
    {
        rtdMoveCursor(RTD_POS_OPT_LINE, TERM_BOLD);

        fputs(TERM_CLR_LINE, rtd.f);

        rtdRestoreCursor();

        diag_list_len = diag.list_rtd_len;
        diag_data_idx = 0;
        memset(&diag_chan, 0xFF, sizeof(diag_data));

        return true;
    }

    if (diag.list_rtd_len == 0)
    {
        // No field written

        return false;
    }

    // Pre-increment index to next field

    if (++diag_data_idx >= diag.list_rtd_len)
    {
        diag_data_idx = 0;
    }

    uint16_t chan = diag.list_rtd[diag_data_idx];

    if (chan != diag_chan[diag_data_idx])
    {
        // Remember new channel if the address has changed

        diag_chan[diag_data_idx] = chan;
    }
    else
    {

        if (diag.data.w[chan] == diag_data[diag_data_idx])
        {
            // No field written

            return false;
        }
    }

    uint16_t data = diag.data.w[chan];
    char     pos[6];

    diag_data[diag_data_idx] = data;

    snprintf(pos, 6, "%u;%u", (RTD_OPTLINE_V_POS), (RTD_DIAG_DATA_H_POS + RTD_DIAG_DATA_H_WIDTH * diag_data_idx));

    rtdMoveCursor((char *)pos, TERM_BOLD);

    if (chan < DIAG_MAX_ANALOG)
    {
        // Data is analogue

        data &= 0xFFF;

        // Display address in hex and data in millivolts

        fprintf(rtd.f, "%02X:%-4.0f", chan, ((float)data * DIAG_ANALOG_CAL));
    }
    else
    {
        if (chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL))
        {
            // Data is digital

            data &= 0xFFF;

            // Display address and data in hex

            fprintf(rtd.f, "%02X:x%03X", chan, data);
        }
        else
        {
            // Data is a software value

            // Display address and data in hex

            fprintf(rtd.f, "%02X:%04X", chan, data);
        }
    }

    rtdRestoreCursor();

    return true;
}



static bool rtdPrintOptionPsu(bool reset)
{
    static struct Rtd_field_float rtd_field_psu[] =
    {
        { (float *)&psu.v5  , "+5:%*.2f" , 0x6FFFFFFF, 100, RTD_POS_PSU5,   5 },
        { (float *)&psu.vp15, "+15:%*.2f", 0x6FFFFFFF, 100, RTD_POS_PSUP15, 6 },
        { (float *)&psu.vm15, "-15:%*.2f", 0x6FFFFFFF, 100, RTD_POS_PSUM15, 6 },
    };
    static uint8_t prev_psu_idx = 0;

    if (reset == true)
    {
        rtd_field_psu[0].last_value = 0x6FFFFFFF;
        rtd_field_psu[1].last_value = 0x6FFFFFFF;
        rtd_field_psu[2].last_value = 0x6FFFFFFF;

        prev_psu_idx = 0;

        return false;
    }

    uint16_t psu_idx = prev_psu_idx;
    bool     updated;

    do
    {
        updated = rtdPrintFloat((void *)&rtd_field_psu[psu_idx], NULL);

        if (++psu_idx > 2)
        {
            psu_idx = 0;
        }
    }
    while (psu_idx != prev_psu_idx && updated == false);

    prev_psu_idx = psu_idx;

    return updated;
}



static bool rtdPrintOptionDebug(bool reset)
{
    static struct Rtd_field_string field_str = { "", RTD_POS_OPT_LINE, TERM_BOLD };

    if (reset == true)
    {
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));

        rtd_local.option_debug_updated = true;

        return false;
    }

    if (rtd_local.option_debug_updated == false)
    {
        return false;
    }

    bool retval = rtdPrintString(rtd_local.option_debug, (void *) &field_str);

    rtd_local.option_debug_updated = false;

    return retval;
}



// ---------- External function definitions

void rtdTask(void * unused)
{
    // This function never exits so "local" variables are static
    // to save stack space.

    static  struct Rtd_process * prev_process;

    // Initialise FILE structures for 'rtd' stream

    rtd.f = fopen("rtd", "wb");

    if (setvbuf(rtd.f, NULL, _IONBF, 0) != 0)  // not buffered, buffer size 0
    {
        // 0   = ok
        // EOF = error
    }

    rtd.process = rtd_process;
    rtd.ctrl    = FGC_RTD_OFF;


    // Main loop @ RTD_PERIOD_MS

    for (;;)
    {
        // Wait for OSTskResume() from msTask();

        OSTskSuspend();

        taskTraceReset();

        prev_process = rtd.process;

        do
        {
            taskTraceInc();

            if (rtd.process->func(&rtd.process->args) == true)
            {
                break;
            }

            if ((++rtd.process)->func == NULL)
            {
                rtd.process = rtd_process;
            }

        } while (prev_process != rtd.process);
    }
}



void rtdReset(FILE * f)
{
    // Class specific resets

    rtdClassReset(f);

    // Reset int fields

    struct Rtd_field_int * field_int;

    for (field_int = &rtd_field_ints[0]; field_int->source != NULL; field_int++)
    {
        field_int->last_value = 0x7FFFFFFF;
    }

    // Reset temp fields

    struct Rtd_field_temp * field_temp;

    for (field_temp = &rtd_field_temps[0]; field_temp->source != NULL; field_temp++)
    {
        field_temp->last_value = 0xFFFF;
    }

    // Reset func fields

    struct Rtd_field_func * field_func;

    for (field_func = &rtd_field_funcs[0]; field_func->func != NULL; field_func++)
    {
        field_func->func(true);
    }

    // Reset all option line variables

    struct Rtd_field_optional * field;

    for (field = &rtd_field_optional[0]; field->func != NULL; field++)
    {
        field->func(true);
    }

    rtd.process = rtd_process;
}



void rtdInit(void)
{
    rtdClassInit();
}



void rtdEnable(void)
{
    rtd_local.enabled = true;
}



void rtdDisable(void)
{
    rtd_local.enabled = false;
}



bool rtdIsEnabled(void)
{
    return rtd_local.enabled;
}



void rtdMoveCursor(const char * position, char const * const format)
{
    fprintf(rtd.f, RTD_START_POS, position, format);
}



void rtdRestoreCursor(void)
{
    fputs(RTD_END, rtd.f);
}



bool rtdPrintFloat(struct Rtd_field_float * field, struct Rtd_field_error * error)
{
    bool red = false;

    if (error != NULL)
    {
        if (error->value != error->last_value)
        {
            static char const str_none[11] = "      NONE";

            uint8_t width = 0;

            field->last_value = 0x6FFFFFFF;
            error->last_value = error->value;

            if (testBitmap(error->last_value, RTD_VALUE_NO_PRINT) == true)
            {
                width = field->width > 10 ? 10 : field->width;

                rtdMoveCursor(field->position, TERM_BOLD);
                fputs((char *)&str_none[10 - width], rtd.f);
                rtdRestoreCursor();

                return true;
            }
        }
        else if (testBitmap(error->value, RTD_VALUE_NO_PRINT) == true)
        {
            return false;
        }

        if (testBitmap(error->value, RTD_VALUE_FAULT) == true)
        {
            red = true;
        }
    }

    float      sign_v = 1.0;
    float      sign_l = 1.0;
    uint32_t   diff;
    float      value;
    char       print_style[6];
    char       str[11];        // String buffer. This buffer is sufficient to print floating-point values
                               // in range [-1.0E10; +1.0E10] with 6 decimal places.

    value = *field->source;

    // Clip max value to avoid crashing due to overruns of the string buffer

    value = MAX(-1.0E+10, value);
    value = MIN(value,    1.0E+10);

    if (field->last_value < 0)
    {
        sign_l = -1.0;
    }

    if (value < 0)
    {
        sign_v = -1.0;
    }

    diff = fabs((int)((value * field->factor) + sign_v * 0.5) - (int)((field->last_value * field->factor) + sign_l * 0.5));

    if (diff > 0)
    {
        field->last_value = value;

        snprintf(str, 11, field->format, field->width, value);

        snprintf(print_style, 6, (red == true ? TERM_BOLD TERM_FG_RED : TERM_BOLD));

        rtdMoveCursor(field->position, print_style);

        fputs((char *)str, rtd.f);

        rtdRestoreCursor();

        return true;
    }

    return false;
}



bool rtdPrintInt(struct Rtd_field_int * field)
{
    int32_t value = *field->source;

    if (value != field->last_value)
    {
        field->last_value = value;

        rtdMoveCursor(field->position, TERM_BOLD);

        fprintf(rtd.f, field->format, value);

        rtdRestoreCursor();

        return true;
    }

    return false;
}



bool rtdPrintBool(struct Rtd_field_bool * field)
{
    bool value = *field->source;
    char print_style[6];

    if (value != field->last_value || field->first_print == true)
    {
        field->last_value  = value;
        field->first_print = false;

        snprintf(print_style, 6, TERM_BOLD);

        rtdMoveCursor(field->position, print_style);

        fprintf(rtd.f, "%c", (value == true ? 'T' : 'F'));

        rtdRestoreCursor();

        return true;
    }

    return false;
}



bool rtdPrintTemp(struct Rtd_field_temp * field)
{
    uint16_t temperature = *field->source;
    float    temp_fmt    = (float) ID_TEMP_FLOAT(temperature);

    char print_style[6];

    if (temperature != field->last_value)
    {
        field->last_value = temperature;

        snprintf(print_style, 6, (temp_fmt == 0.0F ? TERM_BOLD TERM_FG_RED : TERM_BOLD));

        rtdMoveCursor(field->position, print_style);

        float temp_fmt_int;
        float temp_fmt_fract;
        temp_fmt_fract  = modff(temp_fmt, &temp_fmt_int);

        fprintf(rtd.f, "%2u.%01u", (int)temp_fmt_int, (int)(fabs(temp_fmt_fract) * 10));

        rtdRestoreCursor();

        return true;
    }

    return false;
}



bool rtdPrintString(char const * source, struct Rtd_field_string * field)
{
    size_t  source_len = strlen(source);
    int8_t  len_diff   = strlen(field->last_value) - source_len;

    if (len_diff != 0 || strncmp(source, field->last_value, TRM_BUF_SIZE) != 0)
    {
        strncpy(field->last_value, source, TRM_BUF_SIZE);

        // Pad the string with extra spaces to overwrite characters left from the
        // previous longer string

        if (len_diff > 0)
        {
            char * src = field->last_value + source_len;

            memset(src, ' ', len_diff);

            src[len_diff] = '\0';
        }

        rtdMoveCursor(field->position, field->print_style);

        fputs(field->last_value, rtd.f);

        rtdRestoreCursor();

        return true;
    }

    return false;
}



bool rtdProcessFloats(struct Rtd_process_args * args)
{
    struct Rtd_field_float * field   = (struct Rtd_field_float *)args->field;
    bool                     updated = false;

    for (;  field->source != NULL && updated == false; field++)
    {
        updated = rtdPrintFloat(field, NULL);
    }

    args->field = (field->source == NULL ? (void *)args->list_fields : (void*)field);

    return updated;
}



bool rtdProcessFloatsErrors(struct Rtd_process_args * args)
{
    struct Rtd_field_error_float * field   = (struct Rtd_field_error_float *)args->field;
    bool                           updated = false;

    rtdClassUpdateErrors();

    for (;  field->field.source != NULL && updated == false; field++)
    {
        updated = rtdPrintFloat(&field->field, &field->error);
    }

    args->field = (field->field.source == NULL ? (void *)args->list_fields : (void*)field);

    return updated;
}



bool rtdProcessInts(struct Rtd_process_args * args)
{
    struct Rtd_field_int * field   = (struct Rtd_field_int *)args->field;
    bool                   updated = false;

    for (;  field->source != NULL && updated == false; field++)
    {
        updated = rtdPrintInt(field);
    }

    args->field = (field->source == NULL ? (void *)args->list_fields : (void*)field);

    return updated;
}



bool rtdProcessBools(struct Rtd_process_args * args)
{
    struct Rtd_field_bool * field   = (struct Rtd_field_bool *)args->field;
    bool                    updated = false;

    for (;  field->source != NULL && updated == false; field++)
    {
        updated = rtdPrintBool(field);
    }

    args->field = (field->source == NULL ? (void *)args->list_fields : (void*)field);

    return updated;
}



bool rtdProcessTemps(struct Rtd_process_args * args)
{
    struct Rtd_field_temp * field   = (struct Rtd_field_temp *)args->field;
    bool                    updated = false;

    for (;  field->source != NULL && updated == false; field++)
    {
        updated = rtdPrintTemp(field);
    }

    args->field = (field->source == NULL ? (void *)args->list_fields : (void*)field);

    return updated;
}



bool rtdProcessFuncs(struct Rtd_process_args * args)
{
    struct Rtd_field_func * field   = (struct Rtd_field_func *)args->field;
    bool                    updated = false;

    for (;  field->func != NULL && updated == false; field++)
    {
        updated = field->func(false);
    }

    args->field = (field->func == NULL ? (void *)args->list_fields : (void*)field);

    return updated;
}



bool rtdProcessOptionLine(struct Rtd_process_args * args)
{
    static rtd_func prev_func = NULL;

    // Search the function associated to the selected option line

    struct Rtd_field_optional * field = (struct Rtd_field_optional *)args->field;

    for (;  field->func != NULL && rtd.ctrl != field->option; field++)
    {
        ; // Do nothing
    }

    // If the option line was not found in this list of fields, return.

    if (field->func == NULL)
    {
        return false;
    }

    // If the function has changed, clear the line and reset the new option line,
    // thus guaranting that the fields will be printed

    if (prev_func != field->func)
    {
        rtdPrintOptionOff(false);

        field->func(true);

        prev_func = field->func;

        return true;
    }

    if (field->func == rtdPrintOptionOff)
    {
        return false;
    }

    // Restore the flag for the next time

    field->func(false);

    rtd.last_ctrl = rtd.ctrl;

    return false;
}



void rtdSetOptionDebug(char const * debug)
{
    if (rtd_local.option_debug_updated == false)
    {
        strncpy(rtd_local.option_debug, debug, TRM_BUF_SIZE);

        rtd_local.option_debug_updated = true;
    }
}


// EOF
