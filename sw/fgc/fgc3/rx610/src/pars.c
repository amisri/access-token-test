//! @file  pars.h
//! @brief Pars function invoked when the property is set


// ---------- Includes

#include <pars.h>
#include <string.h>
#include <class.h>
#include <definfo.h>
#include <defprops.h>
#include <fgc_errs.h>
#include <logSpy.h>
#include <msTask.h>
#include <os.h>
#include <parsService.h>
#include <prop.h>



// ---------- Internal structures, unions and enumerations

#ifdef DEBUG
#define DBLEN       20
static uint16_t       dbidx;
static struct
{
    uint16_t  pars_idx;
    uint16_t  n_pars;
    uint16_t  out_idx;
    uint16_t  n_chars;
    uint16_t  pkt_buf_idx;
    uint16_t  n_carryover_chars;
    uint16_t  last_par_f;
    uint16_t  header;
    uint16_t  end_cmd_f;
} db[DBLEN];
#endif



// ---------- Internal function definitions

//! See the documentation for function ParsScanSymList(). This function has the same functionality,
//! except it adds one more possible error status: FGC_GENERIC_NONE_SYMBOL.
//! That additional error is used for the purpose of the generic "NONE" symbol available by default on all BIT_MASK
//! properties. That symbol allows to reset the bit mask. E.g. S TEST.BIT_MASK NONE

static uint16_t ParsScanSymListAndNone(struct cmd * c, const char * delim, const struct sym_lst * sym_lst,
                                     const struct sym_lst ** sym_const)
{
    uint16_t      errnum;             // Return status
    uint16_t      sl_idx;             // Symbol index in symbol list (FGC_NOT_SETTABLE bit may be set)
    uint16_t      sym_idx;            // Identified symbol index
    bool not_settable_f;         // Flag indicating that the symbol was found, but cannot be (FGC_NOT_SETTABLE)

    errnum = PropIdentifySymbol(c, delim, ST_CONST, &sym_idx);  // Identify Constant Symbol

    c->last_par_f = c->end_cmd_f;       // Set last_par_f to end_cmd_f

    if (errnum != 0)                            // If Symbol not identified
    {
        return (errnum);                        // Report error status
    }

    not_settable_f = FALSE;

    // Scan supplied sym_lst for a match
    while ((!not_settable_f) && (sl_idx = sym_lst->sym_idx) && (sl_idx != sym_idx))
    {
        // Non-settable symbols will NEVER
        // match because the FGC_NOT_SETTABLE
        // bit is set in sym_idx in the sym_lst
        not_settable_f = (sl_idx == (sym_idx | FGC_NOT_SETTABLE));

        sym_lst++;
    }

    // If the symbol was found but ORed with mask FGC_NOT_SETTABLE, then return the "symbol is not settable" error.

    if (not_settable_f == TRUE)
    {
        return (FGC_SYMBOL_IS_NOT_SETTABLE);
    }

    // Else, if no match found (sl_idx == NULL)

    else if (!sl_idx)
    {
        // Generic NONE symbol. The NONE symbol is recognised by default for all bit mask properties. Note that if
        // STC_NONE or (STC_NONE|FGC_NOT_SETTABLE) is present in the sym_lst, this is not the generic NONE symbol but
        // a symbol explicitly defined in the property symbol list.

        if (sym_idx == STC_NONE)
        {
            return (FGC_GENERIC_NONE_SYMBOL);   // If the property is a bit mask, that error gets caught by the FGC SW.
        }

        // In any other case, report BAD PARAMETER

        else
        {
            return (FGC_BAD_PARAMETER);
        }
    }

    *sym_const = sym_lst;               // Pass pointer to sym_lst record to calling function
    return (0);                         // Return success
}



//! This function is used to convert an AbsTime parameter from the command packet.  An AbsTime is formated
//! as UNIX_TIME.US or as a double precision value formatted as "%.17e".  This must be parsed as two integers
//! as there is no double precision support.
//! .
//! @param c command packet
//! @param value value of conversion (if success), TIME.NOW if no token is supplied or the value is zero.
//! @return errnum:
//!       0                       AbsTime successfuly converted in *value
//!       FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
//!       FGC_NO_SYMBOL           Token buffer empty
//!       FGC_SYNTAX_ERROR        Token too long or contains a space before a valid delimiter
//!       FGC_BAD_INTEGER         Token contains non-valid characters
//!       FGC_INVALID_TIME        Microseconds > 999999

static uint16_t ParsScanAbsTime(struct cmd * c, struct CC_us_time ** value)
{
    uint16_t              i;
    uint16_t              errnum;
    uint32_t              val;
    uint16_t              expon;
    uint16_t              n_chars;
    char        *       token;                          // Pointer to character in token
    char        *       c1;
    char        *       c2;
    struct CC_us_time * result;

    result = (struct CC_us_time *)&c->pars_buf->results;       // Use the pars_buf results area for the value
    *value = result;                                    // Return the address of the value

    errnum = ParsScanToken(c, "Y,");

    if (errnum)                 // Get next token, delimiters: ',' and '\0'
    {
        return (errnum);
    }

    c->last_par_f = c->end_cmd_f;                       // Set last_par_f to end_cmd_f

    n_chars = c->n_token_chars;

    if (!n_chars)                       // If no token
    {
        *result = ms_task.time_us;                               // Set result to time now
        return (0);                                             // Return time now
    }

    token = (char *) &c->token;

    if (token[0] < '0' || token[0] > '9')               // If first character isn't a digit
    {
        return (FGC_INVALID_TIME);                              // Report bad integer
    }

    OSSemPend(pcm.sem);                                 // Reserve access to the PCM structure

    if (n_chars > 6 && token[1] == '.' && token[n_chars - 4] == 'E')
    {
        if (token[n_chars - 3] != '+')
        {
            OSSemPost(pcm.sem);
            return (FGC_INVALID_TIME);
        }

        token[n_chars - 4] = '\0';
        pcm.end_cmd_f = FALSE;
        pcm.prop_buf  = (struct Dpcom_prop_buf *)&token[n_chars - 2];

        if (ParsScanInteger(&pcm, "Y", &val) || val > 9 || ((uint16_t)val + 6) > n_chars) // delim = '\0'
        {
            OSSemPost(pcm.sem);
            return (FGC_INVALID_TIME);
        }

        expon = (uint16_t)val;

        c1 = &token[2];
        i  = n_chars - 6;

        while (i--)                             // Check all the digits are valid
        {
            if (*c1 < '0' || *c1 > '9')
            {
                OSSemPost(pcm.sem);
                return (FGC_INVALID_TIME);              // Report bad time if any digit is invalid
            }

            c1++;
        }

        c1 = &token[1];
        c2 = &token[2];
        i  = expon;

        while (i--)                             // Shift digits to move decimal point
        {
            *(c1++) = *(c2++);
        }

        *c1 = '.';                              // Add new decimal point

        if ((n_chars - expon - 6) > 6)          // If more than 6 digits after decimal
        {
            token[expon + 8] = '\0';                    // Nul terminate after sixth digit
        }
    }


    pcm.end_cmd_f = FALSE;
    pcm.prop_buf  = (struct Dpcom_prop_buf *)&token[0];

    errnum = ParsScanInteger(&pcm, "Y.", &val);

    if (errnum != 0)    // Delimiters: '.'  '\0'
    {
        OSSemPost(pcm.sem);
        return (errnum);
    }

    result->secs.abs = val;                            // Prepare time now (to the second) as default
    result->us = 0;

    if (pcm.token_delim == '.')
    {
        switch ((errnum = ParsScanInteger(&pcm, "Y", &val)))    // Delimiters: '\0'
        {
            case 0:

                if (val > 999999)                   // If too may characters found
                {
                    OSSemPost(pcm.sem);
                    return (FGC_INVALID_TIME);
                }

                if (val < 10)
                {
                    result->us = val * 100000;
                }
                else if (val < 100)
                {
                    result->us = val * 10000;
                }
                else if (val < 1000)
                {
                    result->us = val * 1000;
                }
                else if (val < 10000)
                {
                    result->us = val * 100;
                }
                else if (val < 100000)
                {
                    result->us = val * 10;
                }
                else
                {
                    result->us = val;
                }

                break;

            case FGC_NO_SYMBOL:                             // If no ms specified then return seconds only

                break;

            default:                                        // Report errors

                OSSemPost(pcm.sem);
                return (errnum);
        }
    }

    if (result->secs.abs == 0)                             // If time is zero seconds (any milliseconds)
    {
        *result = ms_task.time_us;                                     // Set result to time now
    }

    OSSemPost(pcm.sem);
    return (0);
}


uint16_t ParsScanChar(struct cmd * c, char ** value)
{
    uint16_t      errnum;

    if (c->token_idx >= c->n_token_chars)       // If token buffer is empty
    {
        errnum = ParsScanToken(c, "Y,");

        if (errnum)             // Delimiters: ',' '\0'
        {
            return (errnum);                                    // Return error
        }
    }

    *value = &c->token[c->token_idx++];         // Return address of the character in the token buffer

    if (**value == ':')                         // If colon
    {
        return (FGC_SYNTAX_ERROR);                      // Report error
    }

    if (c->token_idx >= c->n_token_chars)       // If token now empty
    {
        c->last_par_f = c->end_cmd_f;                   // Set last_par_f to end_cmd_f
    }

    if (!c->n_token_chars)                      // If new token is empty
    {
        return (FGC_NO_SYMBOL);                         // Report NO_SYMBOL
    }

    return (0);
}



uint16_t ParsScanString(struct cmd * c, char ** value, const char * const delimiters)
{
    uint16_t      errnum;

    if (c->token_idx >= c->n_token_chars)
    {
        errnum = ParsScanToken(c, delimiters);

        if(errnum != 0)
        {
            return errnum;
        }
    }

    c->token_idx = c->n_token_chars;

    // Return address of the character in the token buffer

    *value = &c->token[0];

    // Set last_par_f to end_cmd_f

    c->last_par_f = c->end_cmd_f;

    // If new token is empty, report no symbol

    if(c->n_token_chars == 0)
    {
        return FGC_NO_SYMBOL;
    }

    return (0);
}



//! This function is called from ParsValueGet() if a symlist value is required.
//!
//! @param c command packet
//! @param p property
//! @param value parsed value
//! @return

static uint16_t ParsValueGetSymList(struct cmd * c, struct prop * p, void ** value)
{
    struct sym_lst const * sym_const;
    uint16_t             * results;
    uint16_t               errnum;

    results    = (uint16_t *)&c->pars_buf->results;
    results[0] = 0;
    results[1] = 0;
    *value     = (uint8_t *)&c->pars_buf->results + (4 - prop_type_size[p->type]);

    do
    {
        errnum = ParsScanSymListAndNone(c, "Y,", p->range, &sym_const);

        if (errnum == FGC_GENERIC_NONE_SYMBOL)
        {
            errnum = FGC_BAD_PARAMETER;         // Replace internal error GENERIC NONE SYMBOL by user error BAD PARAMETER
        }
        else if (!errnum)
        {
            results[1] |= sym_const->value;
        }
    }
    while (!errnum && c->token_delim == ' ');

    return (errnum);
}



//! This function is called from ParsValueGet() if a bitmask value is required.
//!
//! @param c command packet
//! @param p property
//! @param value parsed value
//! @return

static uint16_t ParsValueGetBitmask(struct cmd * c, struct prop * p, void ** value)
{
    uint16_t               errnum;
    uint16_t             * results;
    struct sym_lst const * sym_const;

    results    = (uint16_t *)&c->pars_buf->results;
    results[0] = 0;
    results[1] = 0;
    *value     = (uint8_t *)&c->pars_buf->results + (4 - prop_type_size[p->type]);

    do
    {
        errnum = ParsScanSymListAndNone(c, "Y ", p->range, &sym_const);

        // If no symbol is found or the symbol NONE is found but that symbol is not
        // explicitly mentioned in the symbol list for that property, then the default
        // interpretation is to reset the property and clear the error.

        if (errnum == FGC_GENERIC_NONE_SYMBOL || errnum == FGC_NO_SYMBOL)
        {
            results[1] = 0;
            errnum     = 0;
        }
        else if (errnum == 0)
        {
            results[1] |= sym_const->value;
        }
    }
    while (errnum == 0 && c->token_delim == ' ');

    return (errnum);
}



//! Converter char [0-9],[A-F] into integer value
//! @param ch character
//! @return integer value, 0x10 if not a valid char

static inline uint16_t ParsScanHexDigit(char ch)
{
    return (ch >= '0' && ch <= '9' ? ch - '0' : (ch >= 'A' && ch <= 'F' ? ch - ('A' - 10) : 0x10));
}



// ---------- External function definitions

uint16_t ParsScanInteger(struct cmd * c, const char * delim, uint32_t * value)
{
    char        ch;                     // Get first character from command buffer
    uint16_t      errnum;                 // Return status
    uint16_t      n_ch;                   // Number of characters in token
    char    *   buf;                    // Character pointer into token buffer
    uint16_t      digit;                  // Working digit value
    uint32_t      val;                    // Unsigned working value

    if (delim)
    {
        errnum = ParsScanToken(c, delim);       // Get next token

        if (errnum != 0)
        {
            return (errnum);
        }
    }

    c->last_par_f = c->end_cmd_f;       // Set last_par_f to end_cmd_f

    n_ch = c->n_token_chars;

    if (!n_ch)                          // If token contains no characters
    {
        return (FGC_NO_SYMBOL);                 // Report NO_SYMBOL
    }

    buf  = c->token;                    // Set up character pointer into token buffer
    ch   = *(buf++);                    // Extract first character
    val  = 0;                           // Clear resulting unsigned value

    while (ch == '0')                   // While character is a zero
    {
        ch = *(buf++);                          // Extract next character
        n_ch--;                                 // keep track of chars remaining
    }

    if (ch == 'X')                      // If hex value specified
    {
        if (n_ch <= 1)                          // If no characters follow the 'X'
        {
            return (FGC_BAD_INTEGER);                   // Report BAD INTEGER
        }

        do
        {
            ch = *(buf++);                              // Extract next character
            n_ch--;                                     // keep track of chars remaining
        }
        while (ch == '0');                      // While character is a zero

        if (n_ch > 8)                           // If token still has more than 8 characters
        {
            return (FGC_BAD_INTEGER);                   // Report BAD INTEGER
        }

        while (ch)                              // Loop until token buffer is empty;
        {
            digit = ParsScanHexDigit(ch);

            if (digit == 0x10)                  // If character is not a valid hex digit
            {
                return (FGC_BAD_INTEGER);                       // Report BAD INTEGER
            }

            val = val * 16 + digit;                     // Accumulate the new digit
            ch  = *(buf++);                             // Get next character from token buffer
        }
    }
    else                                // else for Decimal number
    {
        if (n_ch > 10 ||                        // If integer has more than 10 more characters
            (n_ch == 10 && ch > '3'))             // or if it has exactly 10, but first digit is > '3'
        {
            return (FGC_BAD_INTEGER);                   // Report BAD INTEGER
        }

        while (ch)                      // Loop until token buffer is empty;
        {
            if (ch < '0' || ch > '9')           // If character isn't a valid decimal digit
            {
                return (FGC_BAD_INTEGER);               // Report BAD INTEGER
            }

            digit = ch - '0';                   // Calculate value of new digit
            val   = val * 10 + digit;           // Accumulate the new digit
            ch    = *(buf++);                   // Get next character from token buffer
        }
    }

    *value = val;                       // Return value
    return (0);
}



uint16_t ParsScanStd(struct cmd * c, struct prop * p, uint16_t par_type, void ** value)
{
    uint16_t          errnum;
    uint32_t        * val;
    struct Pars_buf * pars_buf;
    uint16_t          pars_idx_token;

    pars_buf = c->pars_buf;

    if (c->pars_idx >= pars_buf->n_pars)                // If results buffer is empty
    {
        do
        {
            if (pars_buf->out_idx >= pars_buf->pkt[c->pkt_buf_idx].n_chars) // If command packet is empty
            {
                if (c->end_cmd_f ||                                     // If command already empty
                    testBitmap(pars_buf->pkt[c->pkt_buf_idx].header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT))
                {
                    c->last_par_f = TRUE;                                       // Report NO_SYMBOL
                    return (FGC_NO_SYMBOL);
                }

                errnum = CmdNextCmdPkt(c);

                if (errnum)                             // If next packet restarts the cmd
                {
                    return (errnum);                                            // report error
                }
            }

            // On FGC2, the packet would have been parsed by the DSP, thus the entire request must be prepared in DPRAM.
            // On FGC3.1, we just call a parsing function, but much of the scaffolding remains.
            pars_buf->pkt_buf_idx    = c->pkt_buf_idx;
            pars_buf->limits.integer = *(struct int_limits *)p->range; // Prepare limits (int or uint or float)
            pars_buf->type           = par_type;

            // Distinguish between Fieldbus Commands and Terminal Commands, since they are received in different buffers
            if (c->f == fcm.f)
            {
                parsPktFcm(pars_buf);
            }
            else
            {
                parsPktTcm(pars_buf);
            }

            if (pars_buf->errnum)                               // If DSP reported parsing error
            {
                c->device_par_err_idx = pars_buf->par_err_idx;          // Offset error index
                return (pars_buf->errnum);                              // Return the error
            }
        }
        while (!pars_buf->n_pars);

        c->pars_idx = 0;                                        // Reset parameter index

        if (testBitmap(pars_buf->pkt[c->pkt_buf_idx].header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT)) // If last pkt
        {
            c->end_cmd_f = TRUE;                                                // Set end of command flag
        }
    }

    // Save current pars index for later, to retrieve whether a particular token was empty or not
    pars_idx_token = c->pars_idx;

    val = &pars_buf->results.intu[c->pars_idx++];       // Get address of next parameter in results buffer

    if (c->pars_idx >= pars_buf->n_pars)                // If that was the last parameter of the packet
    {
        c->last_par_f = c->end_cmd_f;                           // Set last_par_f to end_cmd_f
    }

    if ( (par_type == PKT_FLOAT) && pars_buf->empty_tokens[pars_idx_token] == true )
    {
        *val = PAR_NO_FLOAT;
        return FGC_NO_SYMBOL;
    }

    if ( (par_type != PKT_FLOAT) && pars_buf->empty_tokens[pars_idx_token] == true )
    {
        return FGC_NO_SYMBOL;
    }

    // Set pointer to the value according to type size
    *value = (void *)((uint32_t)(val + 1) - prop_type_size[p->type]);
    return (0);
}



uint16_t ParsScanToken(struct cmd * c, const char * delim)
{
    uint16_t              errnum;         // Error number
    char                ch;             // Working character from command buffer
    char        *       buf;            // Pointer into token buffer
    uint16_t              n_ch;           // Number of characters transfered
    char                ldelim[8];      // Local copy of far string delimiter

    errnum = CmdNextCh(c, &ch);

    if (errnum != 0)    // If next character aborts the command
    {
        return (errnum);                        // Return error
    }

    if (ch == ' ')                      // If leading space - skip and take another character
    {
        errnum = CmdNextCh(c, &ch);

        if (errnum != 0)                // If next character aborts the command
        {
            return (errnum);                            // Return error
        }
    }

    // Pre pad local delimiter with "impossible" \a character

    memset(ldelim, '\a', sizeof(ldelim));

    memcpy(ldelim, delim, strlen(delim));

    buf  = c->token;                    // Set up character pointer into destination token buffer
    n_ch = 0;


    while (n_ch < PARS_MAX_CMD_TOKEN_LEN)    // While token buffer isn't full
    {
        if ((!ch && ldelim[0] == 'Y') || // If character is nul and nul delimiter is allowed , or
            ch == ldelim[1] ||           // character matches any of the other five possible
            ch == ldelim[2] ||           // delimiter characters
            ch == ldelim[3] ||
            ch == ldelim[4] ||
            ch == ldelim[5] ||
            ch == ldelim[6])
        {
            *buf             = '\0';            // Nul terminate token
            c->token_delim   = ch;              // Report the delimiter character
            c->n_token_chars = n_ch;            // Report token length
            c->token_idx     = 0;               // Reset token index to start of token buffer

            return (0);                         // Return success
        }

        if (!ch)                                // Buffer empty unexpectedly
        {
            return (FGC_NO_DELIMITER);                  // Report NO DELIMITER
        }

        if (ch == ' ')                          // Character is a space
        {
            break;                                      // Break to return SYNTAX_ERROR
        }

        *(buf++) = ch;                          // Put character into token buffer
        n_ch++;                                 // Increment token length counter

        errnum = CmdNextCh(c, &ch);

        if (errnum != 0)                // If next character aborts the command
        {
            return (errnum);                            // Return error
        }
    }

    return (FGC_SYNTAX_ERROR);                  // Buffer overrun - report SYNTAX ERROR
}



uint16_t ParsScanSymList(struct cmd * c, const char * delim, const struct sym_lst * sym_lst,
                       const struct sym_lst ** sym_const)
{
    uint16_t      errnum;                 // Return status

    errnum = ParsScanSymListAndNone(c, delim, sym_lst, sym_const);

    if (errnum == FGC_GENERIC_NONE_SYMBOL)      // This error message should stay internal to the FGC, so
        // replace it by a more generic "BAD PARAMETER" message.
    {
        errnum = FGC_BAD_PARAMETER;
    }

    return errnum;
}



uint16_t ParsSet(struct cmd * c, struct prop * p)
{
    uint16_t      errnum;
    prop_size_t   el_idx;
    uint32_t      el_size;
    prop_size_t   n_els_old;
    uint32_t      max_pars;
    bool          set_f;
    uint8_t     * cur_val;                        // Pointer to current element value
    void        * new_val;                        // Pointer to new element value

    /*--- Prepare array range ---*/

    el_idx = c->from;

    // Get first property block into cache and n_elements

    errnum = PropValueGet(c, p, NULL, el_idx, &cur_val);

    if (errnum)
    {
        return (errnum);
    }

    n_els_old = c->n_elements;

    max_pars  = c->to - el_idx + 1;             // Calculate number of elements in array range
    el_size   = prop_type_size[p->type];        // Get element size for this property
    set_f     = FALSE;                          // Clear parameter set flag

    if (p->type == PT_CHAR &&                   // If CHAR type and
        el_idx > n_els_old)                      // array start is beyond the end of the existing data
    {
        return (FGC_BAD_ARRAY_IDX);                     // Report bad array index
    }

    // Parse command packets

    while (!c->last_par_f && c->n_pars < max_pars)
    {
        errnum  = PropValueGet(c, p, &set_f, el_idx, &cur_val); // Get existing value from prop (as default)

        if (errnum)
        {
            return (errnum);
        }

        errnum  = ParsValueGet(c, p, &new_val);                 // Get new value from command packet

        switch (errnum)
        {
            case 0:

                if(p->type == PT_DEV_NAME)
                {
                    if (strncmp((char*)cur_val, (char*)new_val, el_size) != 0)
                    {
                        strncpy((char*)cur_val, (char*)new_val, el_size);

                        c->changed_f = true;
                        set_f        = true;
                    }
                }
                else
                {
                    if (memcmp(cur_val, new_val, el_size))
                    {
                        memcpy(cur_val, new_val, el_size);

                        c->changed_f = true;
                        set_f        = true;
                    }
                }

                c->n_pars++;
                el_idx++;
                break;

            case FGC_NO_SYMBOL:             // TOKEN is empty (i.e. blank between two commas or after last comma)

                if (p->type == PT_CHAR)             // If CHAR type
                {
                    if (c->n_pars)                          // If at least one character was provided
                    {
                        return (FGC_NO_SYMBOL);                     // Report NO_SYMBOL
                    }
                }

                if (el_idx || !c->last_par_f)       // If not first element or not last parameter
                {
                    c->n_pars++;                            // Don't change existing value for this element
                    el_idx++;
                }

                break;

            default:

                c->device_par_err_idx += c->n_pars;
                return (errnum);
        }
    }

    /*--- Check number of parameters set and adjust property length ---*/

    if (!c->last_par_f && c->n_pars >= max_pars)
    {
        c->device_par_err_idx += c->n_pars;

        return (FGC_BAD_PARAMETER);
    }

    // Keep the original data length if it has been reduced and the property
    // cannot shrink, is a scalar or a portion of an array has been replaced

    if (    el_idx < n_els_old
        && (   testBitmap(p->flags, PF_DONT_SHRINK) == true
            || p->max_elements == 1
            || c->n_arr_spec   == 2))
    {
        el_idx = n_els_old;
    }

    if (el_idx != n_els_old)
    {
        c->changed_f = TRUE;
    }

    errnum = PropBlkSet(c, el_idx, set_f, FGC_FIELDBUS_FLAGS_LAST_PKT);

    return (errnum); // Write back zeroed value(s)

}



uint16_t ParsValueGet(struct cmd * c, struct prop * p, void ** value)
{
    uint16_t errnum;

    // First try to defer to class-specific code;
    // if the value is *not* handled, FGC_UNKNOWN_ERROR_CODE will be returned.
    //
    // FGC_OK_NO_RSP or any other value indicates that the value has been processed.
    if ((errnum = parsClassValueGet(c, p, value)) != FGC_UNKNOWN_ERROR_CODE)
    {
        return errnum;
    }

    switch (p->type)
    {
        case PT_ABSTIME:
        case PT_ABSTIME8:
            return (ParsScanAbsTime(c, (struct CC_us_time **)value));

        case PT_CHAR:
            return (ParsScanChar(c, (char **)value));

        case PT_FLOAT:
            return (ParsScanStd(c, p, PKT_FLOAT, value));

        case PT_INT16S:
        case PT_INT16U:
        case PT_INT32S:
        case PT_INT32U:
        case PT_INT8S:
        case PT_INT8U:
        {
            // Bit mask has a type of PT_INTxxU + the flag PF_BIT_MASK
            if (testBitmap(p->flags, PF_BIT_MASK))
            {
                return (ParsValueGetBitmask(c, p, value));
            }
            // Sym list has a type of PT_INTxxU + the flag PF_SYM_LST
            else if (testBitmap(p->flags, PF_SYM_LST))
            {
                return (ParsValueGetSymList(c, p, value));
            }

            if (p->type == PT_INT32U)
            {
                return (ParsScanStd(c, p, PKT_INTU, value));
            }
            else
            {
                return (ParsScanStd(c, p, PKT_INT, value));
            }
        }

        default:

            // Parsing of other parameter types may be implemented as needed.
            return FGC_NOT_IMPL;
    }
}


// EOF

