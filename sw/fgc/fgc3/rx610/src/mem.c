//! @file  mem.h
//! @brief Memory manipulation functions


// ---------- Includes

#include <mem.h>
#include <iodefines.h>



// ---------- External function definitions

uint16_t MemCmpWords(const void * d1, const void * d2, uint16_t n_words)
{
    uint16_t * a1 = (uint16_t *) d1;
    uint16_t * a2 = (uint16_t *) d2;

    while (n_words-- != 0)
    {
        if (*a1 != *a2)
        {
            return (*a1 - *a2);        // didn't match
        }

        a1++;
        a2++;
    }

    return (0); // match
}



uint16_t MemCrc16(const void * address, uint32_t n_words)
{
    uint8_t * data_in;                    // pointer to data for CRC input

    // Set CRCCR.BYTE

    CRC.CRCCR.BIT.GPS_10 = 3;           // select CRC - CCITT (X16 + X12 + X5 + 1)
    CRC.CRCCR.BIT.LMS    = 0;           // 0 : CRC.CRCDOR LSB first, 1 : CRC.CRCDOR MSB first
    CRC.CRCCR.BIT.DORCLR = 1;           // clear CRC.CRCDOR

    data_in = (uint8_t *)address;      // init pointer to data

    // Transfer data to CRC input register

    while (n_words--)
    {
        CRC.CRCDIR = *(data_in++);
        CRC.CRCDIR = *(data_in++);
    }

    // Return CRC value

    return (CRC.CRCDOR);
}



void MemSetWords(void * to, const uint16_t value, uint32_t n_words)
{
    __asm__
    (
        "mov.l      %0, r1\n\t"         \
        "mov.w      %1, r2\n\t"         \
        "mov.l      %2, r3\n\t"         \
        "sstr.w"
        :// no output operands 
        :"r"(to), "r"(value), "r"(n_words)
        :// clobbered registers
    );
}



void MemSetDWords(void * to, uint32_t value, uint32_t n_words)
{
    __asm__
    (
        "mov.l      %0, r1\n\t"         \
        "mov.w      %1, r2\n\t"         \
        "mov.l      %2, r3\n\t"         \
        "sstr.l"
        :// no output operands
        :"r"(to), "r"(value), "r"(n_words)
        :// clobbered registers
    );
}


// EOF
