//! @file  interruptHandlers.c
//! @brief RX610 ISRs - Interrupt Service Routines, for MainProg


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>

#include <interruptHandlers.h>
#include <bitmap.h>
#include <cmd.h>
#include <ethernet.h>
#include <gateway.h>
#include <mcu.h>
#include <mcuDependent.h>
#include <memmap_mcu.h>
#include <msTask.h>
#include <iodefines.h>
#include <os_hardware.h>
#include <panic.h>
#include <pll.h>
#include <trm.h>



// ---------- Internal function definitions

static void processMsTimeSlice(void)
{
    bool    skip_xoff = false;
    uint8_t new_char;

    // Read back MODE register

    ms_task.mode = CPU_MODE_P;

    // There is no "end of reception flag" in the uart registers.
    // We can scan the SCI0 RXI interrupt flag or use directly the interrupt
    // SCI0_RXI0 - vector 215 - INT_Excep_SCI0_RXI0()

    // Overrun error

    if (SCI0.SSR.BIT.ORER == 1)
    {
        // Clear error flag

        SCI0.SSR.BIT.ORER = 0;
    }

    // Frame error

    if (SCI0.SSR.BIT.FER == 1)
    {
        SCI0.SSR.BIT.FER = 0;
    }

    // Parity error

    if (SCI0.SSR.BIT.PER == 1)
    {
        SCI0.SSR.BIT.PER = 0;
    }


    // Check if an interrupt was generated (equivalent to receiving a char)

    if (ICU.IR215.BIT.IR == 1)
    {
        // Clear the interrupt request flag

        ICU.IR215.BIT.IR = 0;

        // Read character from input data register

        new_char = SCI0.RDR;

        // Ignore if bit 7 is set (B=128->255)

        if (testBitmap(new_char, 0x80) == false)
        {
            switch(new_char)
            {
                case 19:
                    // Character is XOFF (19) (halt SCI output)

                    // Set XOFF timer to 60s

                    terminal.xoff_timer = 60000;
                    skip_xoff           = true;
                    break;

                case 17:
                    // Character is XON (17) (resume SCI output)

                    terminal.xoff_timer = 0;
                    break;

                case 3:
                    // Character is CTRL-C (3) (abort SCI command)

                    tcm.abort_f         = true;
                    terminal.xoff_timer = 0;

                    // Flush the SCI message queue of any waiting characters

                    OSMsgFlush(terminal.msgq);
                    break;

                default:
                    OSMsgPost(terminal.msgq, (void *)(uintptr_t) new_char);
                    break;
            }
        }
    }

    if (skip_xoff == false)
    {
        if (terminal.char_waits_for_tx == true)
        {
            if (terminal.xoff_timer != 0)
            {
                // decrement XOFF timer by 1 ms

                terminal.xoff_timer--;
            }
            else
            {
                // Chars to transmit

                // TDRE Transmit Data Register Empty
                //   0: Transmit register is not empty.
                //   1: Transmit register is empty, data has been transfered to shift register.

                if (SCI0.SSR.BIT.TDRE == 1)
                {
                    // Write char in transmit buffer

                    SCI0.TDR = terminal.snd_ch;
                    terminal.char_waits_for_tx = false;
                }
            }
        }
    }

    OSTskResume(MCU_TSK_MST);
}



// ---------- External function definitions

// FIXED VECTORS

void INT_Excep_SupervisorInst(void)
{
    GET_ISP(panic_mcu.sp);

    // PC stored in the stack at the time of the interrupt

    panic_mcu.pc = *(panic_mcu.sp + 1);

    panic(MCU_PANIC_EXCEP_SUPERVISOR_INSTR,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}



void INT_Excep_UndefinedInst(void)
{
    GET_ISP(panic_mcu.sp);

    // PC stored in the stack at the time of the interrupt

    panic_mcu.pc = *(panic_mcu.sp + 1);

    panic(MCU_PANIC_EXCEP_INVALID_INSTR,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}



void INT_Excep_FloatingPoint(void)
{
    GET_ISP(panic_mcu.sp);

    // PC stored in the stack at the time of the interrupt

    panic_mcu.pc = *(panic_mcu.sp + 1);

    panic(MCU_PANIC_EXCEP_FLOATING_POINT,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}



void INT_NonMaskableInterrupt(void)
{
    GET_ISP(panic_mcu.sp);

    // PC stored in the stack at the time of the interrupt

    panic_mcu.pc = *(panic_mcu.sp + 1);

    panic(MCU_PANIC_EXCEP_NMI,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}



// RELOCATABLE VECTORS

void INT_Excep_BRK(void)
{
    GET_ISP(panic_mcu.sp);

    // PC stored in the stack at the time of the interrupt

    panic_mcu.pc = *(panic_mcu.sp + 1);

    panic(MCU_PANIC_EXCEP_BRK,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}



void INT_Excep_BUSERR(void)
{
    GET_ISP(panic_mcu.sp);

    // PC stored in the stack at the time of the interrupt

    panic_mcu.pc = *(panic_mcu.sp + 1);

    panic(MCU_PANIC_EXCEP_BUS_ERROR,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);
}



void INT_Excep_FCU_FCUERR(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_FCU_FRDYI(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_CMTU0_CMT0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_CMTU0_CMT1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_CMTU1_CMT2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_CMTU1_CMT3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



// 0x0100 int  64  IRQ0

void INT_Excep_IRQ0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ2(void) // naked, we have OS_INT_ENT
{
    OS_INT_ENT(MCU_TSK_FBS, entryFieldbusISR);

    pll.irqs_count_all++;
    ethernetISR();
#ifdef FGC_DEBUG_OS_HISTORY
    os_history.tsk_switch[os_history.index].last_isr = 2;
#endif

    // Exit from interrupt through scheduler (Never returns)

    OS_INT_EXIT();
}



void INT_Excep_IRQ3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ4(void) // naked, we have OS_INT_ENT
{
    OS_INT_ENT(MCU_TSK_MST, entryIRQ4);

    // P34 is the hardware tick signal coming from the FPGA (active-low)

    if(!P3.PORT.BIT.B4)
    {
        ms_task.isr_start_us = UTC_US_P;
        processMsTimeSlice();
        REFRESH_SLOWWATCHDOG();

    }
    else
    {
        interFgcSendPackets();
    }

#ifdef FGC_DEBUG_OS_HISTORY
    os_history.tsk_switch[os_history.index].last_isr = 4;
#endif

    // Exit from interrupt through scheduler (never returns)

    OS_INT_EXIT();
}



void INT_Excep_IRQ5(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ6(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ7(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ8(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ9(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ10(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ11(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ12(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ13(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ14(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_IRQ15(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_WDT_WOVI(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_AD0_ADI0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_AD1_ADI1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_AD2_ADI2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_AD3_ADI3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU0_TGI0A(void)  // not naked
{
    // ToDo Unused in main, should be disabled
}



// fired by TPU0.TGRB input capture/compare match

void INT_Excep_TPU0_TGI0B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



// fired by TPU0.TGRC input capture/compare match

void INT_Excep_TPU0_TGI0C(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



// fired by TPU0.TGRD input capture/compare match

void INT_Excep_TPU0_TGI0D(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



// fired by TPU0.TCNT overflow

void INT_Excep_TPU0_TCI0V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU1_TGI1A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU1_TGI1B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU1_TCI1V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU1_TCI1U(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU2_TGI2A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU2_TGI2B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU2_TCI2V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU2_TCI2U(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU3_TGI3A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU3_TGI3B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}


void INT_Excep_TPU3_TGI3C(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU3_TGI3D(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU3_TCI3V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU4_TGI4A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU4_TGI4B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU4_TCI4V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU4_TCI4U(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU5_TGI5A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU5_TGI5B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU5_TCI5V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU5_TCI5U(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU6_TGI6A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU6_TGI6B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU6_TGI6C(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU6_TGI6D(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU6_TCI6V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU7_TGI7A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU7_TGI7B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU7_TCI7V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU7_TCI7U(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU8_TGI8A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU8_TGI8B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU8_TCI8V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU8_TCI8U(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU9_TGI9A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU9_TGI9B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU9_TGI9C(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU9_TGI9D(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU9_TCI9V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU10_TGI10A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU10_TGI10B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU10_TCI10V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU10_TCI10U(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU11_TGI11A(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU11_TGI11B(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU11_TCI11V(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TPU11_TCI11U(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR0_CMIA0(void) // not naked
{
    // trigger slowWatchdog SWD if enabled

    REFRESH_SLOWWATCHDOG();
    processMsTimeSlice();
}



void INT_Excep_TMR0_CMIB0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR0_OVI0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR1_CMIA1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR1_CMIB1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR1_OVI1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR2_CMIA2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR2_CMIB2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR2_OVI2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR3_CMIA3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR3_CMIB3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_TMR3_OVI3(void) // not naked
{
    // ToDo Unused in main, should be disabled
}



void INT_Excep_DMAC_DMTEND0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_DMAC_DMTEND1(void)
{
    OS_INT_ENT(MCU_TSK_FBS, entryDMTEND1);

#ifdef FGC_DEBUG_OS_HISTORY

    // Arbitrary constant for debugging

    os_history.tsk_switch[os_history.index].last_isr = 10;
#endif

    gatewayDmac1End();

    OS_INT_EXIT();
}



void INT_Excep_DMAC_DMTEND2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_DMAC_DMTEND3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI0_ERI0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI0_RXI0(void)
{
    //  SCI0.RDR is the received character

    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI0_TXI0(void)
{
    //  SCI0.TDR = char to transmit;

    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI0_TEI0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI1_ERI1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI1_RXI1(void)
{
    //  SCI1.RDR is the received character

    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI1_TXI1(void)
{
    //  SCI0.TDR = char to transmit;

    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI1_TEI1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI2_ERI2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI2_RXI2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI2_TXI2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI2_TEI2(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI3_ERI3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI3_RXI3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI3_TXI3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI3_TEI3(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI4_ERI4(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI4_RXI4(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI4_TXI4(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI4_TEI4(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI5_ERI5(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI5_RXI5(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI5_TXI5(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI5_TEI5(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI6_ERI6(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI6_RXI6(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI6_TXI6(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_SCI6_TEI6(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_RIIC0_EEI0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_RIIC0_RXI0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_RIIC0_TXI0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_RIIC0_TEI0(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_RIIC1_EEI1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_RIIC1_RXI1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_RIIC1_TXI1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}



void INT_Excep_RIIC1_TEI1(void)
{
    // return from interrupt, if __attribute__ ((naked)) is used

    __asm__ volatile("rte");
}


// EOF
