//! @file  logRun.c
//! @brief Run log functions
//!
//! This file contains the functions needed to implement the run log used by the FGC3 Boot and
//! Main Rx610 programs.  The run log is stored in the NV_RAM memory. The NV_RAM has no limited life.
//!
//! The run log record has a fixed length 6-byte structure as follows:
//!
//!       +0      +1      +2      +3      +4      +5
//!    +-------+-------+-------+-------+-------+-------+
//!    |RL SEQ | RL ID |   RL DATA[0]  |   RL DATA[1]  |
//!    +-------+-------+-------+-------+-------+-------+
//!
//! The run log contains 128 elements arranged in a circular buffer.  The input pointer
//! is stored in a word following the end of the buffer.  The run log will be exported
//! via the WorldFIP to the gateway in the runlog byte in the status variable published
//! by every FGC.  This can send 50 bytes/s, so the complete run log will be sent every
//! 16 seconds.
//!
//! The run log IDs are defined in def/src/platforms/fgc3/runlog.xml which the
//! parser uses to generate inc/platforms/fgc3/runlog.h  This specifies a
//! constant (enum) for each type of record, a label and the number of data bytes (0-4).
//!
//! The MRAM is normally read only - to write the MRAM, the ULKMRAM bit must
//! be set in the CPU_RESET register.  The bit must be cleared afterward to relock the
//! MRAM.

#define GLOBAL_VARS


// ---------- Includes

#include <logRun.h>

#include <bitmap.h>
#include <memmap_mcu.h>
#include <nvs.h>
#include <os_hardware.h>
#include <structs_bits_big.h>



// ---------- Internal structures, unions and enumerations

struct logRun_reset_counters
{
    uint16_t all;
    uint16_t power;
    uint16_t program;
    uint16_t manual;
    uint16_t fast_watchdog;
    uint16_t slow_watchdog;
    uint16_t dongle;
    uint16_t dsp;
};

struct logRun
{
    struct fgc_runlog               buffer[FGC_RUNLOG_N_ELS];
    uint16_t                        index;
    struct logRun_reset_counters    resets; //!< Needs to be here due to MCU memory mapping, even if not related to the run log.
    uint16_t                        magic;
};



// ---------- Internal variable definitions

static struct logRun * logRun_ptr = (struct logRun *) NVRAM_RL_BUF_32;



// ---------- Internal function definitions

static void logRunIncrementResets(void)
{
    uint16_t resets_status;

    // Increment RESETS_ALL counter

    logRun_ptr->resets.all++;

    resets_status = CPU_RESET_SRC_P;

    if (testBitmap(resets_status, CPU_RESET_SRC_POWER_MASK16) == true)
    {
        logRun_ptr->resets.power++;
    }

    if (testBitmap(resets_status, CPU_RESET_SRC_PROGRAM_MASK16) == true)
    {
        logRun_ptr->resets.program++;
    }

    if (testBitmap(resets_status, CPU_RESET_SRC_MANUAL_MASK16) == true)
    {
        logRun_ptr->resets.manual++;
    }

    if (testBitmap(resets_status, CPU_RESET_SRC_DONGLE_MASK16) == true)
    {
        logRun_ptr->resets.dongle++;
    }

    if (testBitmap(resets_status, CPU_RESET_SRC_FASTWD_MASK16) == true)
    {
        logRun_ptr->resets.fast_watchdog++;
    }

    if (testBitmap(resets_status, CPU_RESET_SRC_SLOWWD_MASK16) == true)
    {
        logRun_ptr->resets.slow_watchdog++;
    }
}



// ---------- External function definitions

void logRunInit(void)
{
    OS_CPU_SR       cpu_sr;
    bool            clear_all = false;

    // Disable interrupts, except timers that are level 7

    OS_ENTER_CRITICAL();

    nvsUnlock();

    // If the magic number is not initialised, full init

    if (logRun_ptr->magic != 0xBEEF)
    {
        // Set flag to clear reset counters

        clear_all = true;
    }

    // Check that index we will use is within run log buffer

    if (   logRun_ptr->index >= FGC_RUNLOG_N_ELS
        || clear_all == true)
    {
        // Clear the run log buffer area in the NVRAM

        memset(&(logRun_ptr->buffer[0]) , 0x00, sizeof(struct fgc_runlog) * FGC_RUNLOG_N_ELS);

        logRun_ptr->index = 0x0000;
        logRun_ptr->magic = 0xBEEF;

        // First element after a clear
        // Set RLRESET ID in first element

        logRun_ptr->buffer[0].id  = FGC_RL_RLRESET;
    }

    OS_EXIT_CRITICAL();

    if (clear_all == false)
    {
        logRunIncrementResets();
    }
    else
    {
        // Full init

        logRunClrResets();
    }

    logRunAddEntry(FGC_RL_RESETS, &(logRun_ptr->resets.all));
    nvsLock();

    // Reset NanOS SM fields to prevent spurious Runlog entries in the event of a trap

    OS_TID_CODE = 0x10;
    OS_ISR_MASK = 0x00;
}



void logRunInitIfNotReady(void)
{
    if (logRun_ptr->magic != 0xBEEF)
    {
        logRunInit();
    }
}



void logRunClrResets(void)
{
    nvsUnlock();

    // Zero reset counters

    memset(&logRun_ptr->resets, 0x00, sizeof(struct logRun_reset_counters));

    nvsLock();
}



uint16_t logRunGetDspReset(void)
{
    return logRun_ptr->resets.dsp;
}



void logRunIncrementDspReset(void)
{
    logRun_ptr->resets.dsp++;
}



void logRunClrDspReset(void)
{
    logRun_ptr->resets.dsp = 0;
}



void logRunAddEntry(uint8_t id, void const * data)
{
    OS_CPU_SR                  cpu_sr;
    uint16_t                   saved_mode;
    union TUnion32Bits const * data_cast;
    uint8_t                    sequence;

    if (logRun_ptr->magic != 0xBEEF)
    {
        // Run log not ready

        return;
    }

    data_cast = (union TUnion32Bits *) data;

    nvsUnlock();

    OS_ENTER_CRITICAL();

    // Save mode

    saved_mode = CPU_MODE_P;

    // Get sequence number from last record and increment

    sequence = logRun_ptr->buffer[logRun_ptr->index].seq;
    sequence++;

    logRun_ptr->index++;

    if (logRun_ptr->index >= FGC_RUNLOG_N_ELS)
    {
        logRun_ptr->index = 0;
    }

    // Add new item to the buffer

    logRun_ptr->buffer[logRun_ptr->index].seq = sequence;
    logRun_ptr->buffer[logRun_ptr->index].id  = id;

    // We must not read past the real size of input data!

    const uint8_t data_length = fgc_rl_length[id];

    if(data_length == 1)
    {
        logRun_ptr->buffer[logRun_ptr->index].data[0] = data_cast->word[0];
    }
    else if(data_length == 2)
    {
        logRun_ptr->buffer[logRun_ptr->index].data[0] = data_cast->word[0];
        logRun_ptr->buffer[logRun_ptr->index].data[1] = data_cast->word[1];
    }

    nvsLock();

    // Restore mode

    CPU_MODE_P = saved_mode;

    OS_EXIT_CRITICAL();
}



uint16_t logRunReadEntry(uint16_t * index, struct fgc_runlog * runlog_entry)
{
    OS_CPU_SR       cpu_sr;
    uint16_t        previous;

    // Check that the index is within the run log buffer

    if (*index >= FGC_RUNLOG_N_ELS)
    {
        // Initialise index to last stored record

        *index = logRun_ptr->index;
    }

    // Read log

    OS_ENTER_CRITICAL();

    runlog_entry->seq     = logRun_ptr->buffer[*index].seq;
    runlog_entry->id      = logRun_ptr->buffer[*index].id;
    runlog_entry->data[0] = logRun_ptr->buffer[*index].data[0];
    runlog_entry->data[1] = logRun_ptr->buffer[*index].data[1];

    OS_EXIT_CRITICAL();

    // Adjust index to get previous record

    previous = *index;
    previous--;

    if (previous == 0)
    {
        // Adjust past the end of buffer (not a valid runlog entry)

        previous = FGC_RUNLOG_N_ELS;
    }

    return previous;
}



void logRunAddTimestamp(void)
{
    logRunAddEntry(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));    // Unix time
    logRunAddEntry(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));          // Millisecond time
}


// EOF
