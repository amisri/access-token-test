//! @file  get.c
//! @brief Get functions


// ---------- Includes

#include <get.h>

#include <bitmap.h>
#include <class.h>
#include <defprops.h>
#include <diag.h>
#include <dimDataProcess.h>
#include <dallasTask.h>
#include <dpcls.h>
#include <dpcom.h>
#include <fbs.h>
#include <fgc/fgc_db.h>
#include <fgc_errs.h>
#include <fgc_log.h>
#include <logSpy.h>
#include <m16c62_link.h>
#include <mcuDependent.h>
#include <panic.h>
#include <prop.h>
#include <pub.h>
#include <logSpy.h>
#include <regfgc3Pars.h>
#include <regfgc3Task.h>
#include <scivs.h>
#include <sharedMemory.h>
#include <stdio.h>
#include <string.h>
#include <taskTrace.h>
#include <time_fgc.h>
#include <logEvent.h>
#include <logTime.h>
#include <histogram.h>




// ---------- External variable definitions

struct Debug_props  debug;



// ---------- Internal function definitions

//! Helper function for GetComponent(). This will display the type and
//! description for one particular type of component. It will also add
//! the prefix for the number found and required before the first element
//! of the list.

static uint16_t GetComp(struct cmd * c, uint16_t * list_idx, uint16_t n_found, fgc_db_t n_req, fgc_db_t comp_idx)
{
    uint16_t errnum;
    bool     neat_f;
    char     comp_label[FGC_COMPDB_COMP_LBL_LEN + 2];

    neat_f = (c->token_delim == ' ');

    // Display found/req before first element

    if (! *list_idx)
    {
        errnum = CmdPrintIdx(c, c->from++);

        if (errnum)
        {
            return (errnum);
        }

        fprintf(c->f, "%02u/%02u", n_found, n_req);
    }
    else
    {
        if (neat_f)
        {
            fputs("\n     ", c->f);         // Add spaces to line up colunns
        }
    }

    fputc(((*list_idx)++ ? '\\' : '|'), c->f);      // Add list element delimiter

    fprintf(c->f, (neat_f ? "%10s = " : "%s="), CompDbType(comp_idx));

    memcpy(&comp_label, CompDbLabel(comp_idx), sizeof(comp_label));

    if (neat_f && comp_label[61])
    {
        comp_label[58] = '.';
        comp_label[59] = '.';
        comp_label[60] = '.';
        comp_label[61] = '\0';
    }

    fputs(comp_label, c->f);

    return (0);
}



// ---------- External function definitions

uint16_t GetAbsTime(struct cmd * c, struct prop * p)
{
    prop_size_t          n;          // Number of element in array range
    uint16_t               errnum;         // Error number
    const char     *     fmt;
    struct CC_us_time * value_p;
    struct CC_us_time   value;
    bool              ze_f;           // ZERO get option active
    bool              set_f;          // Value has been set (to zero by ZERO get opt)

    static const char  * decfmt[]   = { "%10u.%06u",      "%u.%06u"   };
    static const char  * hexfmt[]   = { " 0x%08X.%05X",   "0x%X.%05X" };

    ze_f  = testBitmap(p->flags, PF_GET_ZERO) &&              // Prepare zero flag
            testBitmap(c->getopts, GET_OPT_ZERO);

    set_f = FALSE;

    errnum = CmdPrepareGet(c, p, 2, &n);

    if (errnum)
    {
        return (errnum);
    }

    errnum = PropValueGet(c, p, NULL, 0, (uint8_t **)&value_p); // Get first abstime element to use as timestamp

    if (errnum)
    {
        return (errnum);
    }

    fmt = (testBitmap(c->getopts, GET_OPT_HEX) ? hexfmt[(c->token_delim == ',')] :
           decfmt[(c->token_delim == ',')]);

    while (n--)                 // For all elements in the array range
    {
        errnum = PropValueGet(c, p, &set_f, c->from, (uint8_t **)&value_p); // Get property (from DSP is necessary)

        if (errnum)
        {
            return (errnum);
        }

        value = *value_p;

        if (ze_f)
        {
            value_p->secs.abs = 0.0;
            value_p->us = 0.0;
        }

        set_f = ze_f;

        errnum = CmdPrintIdx(c, c->from);            // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        fprintf(c->f, fmt, value.secs.abs, value.us);

        c->from += c->step;
    }

    if (set_f)
    {
        PropBlkSet(c, c->n_elements, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT); // Write back zeroed value(s)
    }

    return (0);
}



uint16_t GetChar(struct cmd * c, struct prop * p)
{
    char            ch = 1;                 // Character to display (initialise to non-zero for while())
    char      *     ch_ptr;
    char            delim_save;             // Saved token delimiter
    uint16_t          hex_f;                  // HEX get option flag
    prop_size_t     n;                      // Number of array elements to display
    uint16_t          errnum = FGC_OK_NO_RSP; // Error number

    hex_f = testBitmap(c->getopts, GET_OPT_HEX);

    errnum = CmdPrepareGet(c, p, (hex_f ? 8 : 64), &n);

    if (errnum)
    {
        return (errnum);
    }

    delim_save = c->token_delim;            // Save delimiter so that it can be restored at the end

    if(!hex_f)    // If displaying ASCII and not comma delimiter
    {
        c->token_delim = '\0';              // Suppress space delimiter
    }

    // For all characters in array range

    while (n-- &&
           (errnum = PropValueGet(c, p, NULL, c->from, (uint8_t **)&ch_ptr)) == 0 &&
           (ch = *ch_ptr) != 0)
    {
        errnum = CmdPrintIdx(c, c->from);   // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        if (hex_f)                          // If hex flag is set
        {
            fprintf(c->f, "0x%02X", ch);    // Display character as hex
        }
        else
        {
            fputc(ch, c->f);                // Display character as ASCII
        }

        c->from += c->step;
    }

    c->token_delim = delim_save;            // Restore delimiter in case called from parent or config.set

    return (errnum);
}



uint16_t GetComponent(struct cmd * c, struct prop * p)
{
    prop_size_t n;              // Number of array elements to display
    uint16_t      errnum;         // Error number
    fgc_db_t      grp_idx;        // Group index
    uint16_t      comp_idx = 0;   // Comp index
    uint16_t      list_idx;       // List element index
    bool     unknown_f;      // Scan for unknown components
    const fgc_db_t    n_comps = CompDbLength(); // Number of components

    if (dallasTaskSystemActive() == false)  // If ID scan has not completed successfully
    {
        return (FGC_ID_NOT_READY);          // Report that ID is not ready
    }

    if (c->n_arr_spec)              // If array is specified
    {
        return (FGC_BAD_ARRAY_IDX);         // report BAD ARRAY INDEX
    }

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    clrBitmap(c->getopts, GET_OPT_IDX);       // Turn of printing of element indexes
    unknown_f = FALSE;                  // Start with known groups
    grp_idx = 0;                        // Reset group index

    while (n--)                         // For all elements
    {
        grp_idx++;                      // Consider next group
        list_idx = 0;

        if (!unknown_f)                 // If showing Matched/mismatched groups
        {
            switch (p->sym_idx)
            {
                case STP_MATCHED:

                    while (grp_idx < FGC_N_COMP_GROUPS &&       // Scan to find next matching group
                           (!SysDbRequiredGroups(device.sys.sys_idx,grp_idx) ||
                            SysDbRequiredGroups(device.sys.sys_idx,grp_idx) != dls.detected[grp_idx]))
                    {
                        grp_idx++;
                    }

                    break;

                case STP_MISMATCHED:

                    while (grp_idx < FGC_N_COMP_GROUPS &&       // Scan to find next mismatching group
                           (!SysDbRequiredGroups(device.sys.sys_idx,grp_idx) ||
                            SysDbRequiredGroups(device.sys.sys_idx,grp_idx) == dls.detected[grp_idx]))
                    {
                        grp_idx++;
                    }

                    break;
            }

            if (grp_idx >= FGC_N_COMP_GROUPS)           // If all groups checked
            {
                comp_idx = 0;                           // Switch to unknown comps
                list_idx = 0;
                unknown_f = TRUE;
            }
            else
            {
                for (comp_idx = 1; comp_idx < n_comps; comp_idx++)
                {
                    if (CompDbGroupIndex(comp_idx,device.sys.sys_idx) == grp_idx)
                    {
                        errnum = GetComp(c, &list_idx, dls.detected[grp_idx], SysDbRequiredGroups(device.sys.sys_idx,grp_idx), comp_idx);

                        if (errnum)
                        {
                            return (errnum);
                        }
                    }
                }
            }
        }

        if (unknown_f)                  // If showing unknown components
        {
            while (comp_idx < n_comps && !dls.n_unknown_grp[comp_idx])
            {
                comp_idx++;
            }

            if (comp_idx >= n_comps)         // If end of components reached
            {
                n = 0;                          // Abort early
            }
            else
            {
                errnum = GetComp(c, &list_idx, dls.n_unknown_grp[comp_idx], 0, comp_idx);
                comp_idx++;

                if (errnum != 0)
                {
                    return (errnum);
                }
            }
        }
    }

    return (0);
}



uint16_t GetConfigChanged(struct cmd * c, struct prop * p)
{
    // Displays the modified config property names and data.

    prop_size_t n;              // Number of array elements to display
    uint16_t      errnum;         // Error number

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    c->si_idx = c->n_prop_name_lvls = 0;               // Force complete name to be written

    p->n_elements = p->max_elements;

    return (CmdParentGet(c, p, PARENT_GET_CONFIG_CHANGED));
}



uint16_t GetConfigSet(struct cmd * c, struct prop * p)
{
    // Displays the set config property names and data.

    prop_size_t n;              // Number of array elements to display
    uint16_t      errnum;         // Error number

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    c->si_idx = c->n_prop_name_lvls = 0;               // Force complete name to be written

    p->n_elements = p->max_elements;

    return (CmdParentGet(c, p, PARENT_GET_CONFIG_SET));
}



uint16_t GetConfigUnset(struct cmd * c, struct prop * p)
{
    // Displays the unset config property names.

    prop_size_t n;              // Number of array elements to display
    uint16_t      errnum;         // Error number

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    c->si_idx = c->n_prop_name_lvls = 0;               // Force complete name to be written

    p->n_elements = p->max_elements;

    return (CmdParentGet(c, p, PARENT_GET_CONFIG_UNSET));
}



uint16_t GetCore(struct cmd * c, struct prop * p)
{
    // Displays the core data properties. Only supplies results with
    // the BIN get option.

    uint16_t                   n_bytes;
    uint16_t                   n_words;
    prop_size_t              idx;
    prop_size_t              n_segs;
    uint8_t                  * core_data;
    uint8_t                  * table_data;
    uint16_t                   errnum;
    struct Panic_core_ctrl   core_ctrl;

    if (c->step > 1)                            // If array step is not 1
    {
        return (FGC_BAD_ARRAY_IDX);                     // Report bad array index
    }

    if (!testBitmap(c->getopts, GET_OPT_BIN))     // If BIN get option not specified
    {
        return (FGC_BAD_GET_OPT);           // Report bad get option!
    }

    n_words    = 0;

    errnum = CmdPrepareGet(c, p, 1, &n_segs);

    if (errnum)
    {
        return (errnum);
    }

    /*--- Get Stack dump ---*/

    if (p->sym_idx == STP_STACK)
    {
        n_bytes = NVRAM_CORESTACK_W * 2;        // Length of stack dump in bytes

        CmdStartBin(c, p, (uint32_t)n_bytes);

        core_data = (uint8_t *)NVRAM_CORESTACK_32;

        while (n_bytes--)               // Write data
        {
            c->store_cb(*(core_data++));
        }

        return (0);
    }

    /*--- Get Core dump ---*/

    panicCoreTableGet(&core_ctrl);            // Retrieve table from FRAM

    for (idx = c->from; idx <= c->to; idx++)
    {
        n_words += (core_ctrl.seg[idx].n_words + (sizeof(struct Panic_core_seg)) / 2);
    }

    CmdStartBin(c, p, (uint32_t)n_words * 2);

    /*--- Write data segments from FRAM ---*/

    for (idx = c->from; idx <= c->to; idx++)    // For each segment
    {
        n_bytes = core_ctrl.seg[idx].n_words * 2;
        core_data = (uint8_t *)core_ctrl.addr_in_core[idx];

        while (n_bytes--)                       // Write data
        {
            c->store_cb(*(core_data++));
        }
    }

    /*--- Write one table entry per segment ---*/

    n_bytes = n_segs * sizeof(struct Panic_core_seg);
    table_data = (uint8_t *)&core_ctrl.seg[c->from];

    while (n_bytes--)
    {
        c->store_cb(*(table_data++));
    }

    return (0);
}



uint16_t GetDiagChan(struct cmd * c, struct prop * p)
{
    // Displays the DIAGCHAN properties and the addressed diagnostic data!

    prop_size_t     n;              // Number of array elements to display
    uint8_t     *     chan;           // Pointer to channel element
    uint16_t          data;           // data for channel
    uint16_t          errnum;         // Error number
    uint16_t          data_chan;      // Channel within diag.data[] array
    uint16_t          logical_dim;
    fgc_db_t          board_number;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    while (n--)   // For all elements
    {
        errnum = CmdPrintIdx(c, c->from);            // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        chan      = &((uint8_t *)p->value)[c->from];  // Get address of channel
        data_chan = *(chan);                        // Get current channel (list_offset is not used)

        fprintf(c->f, "0x%02X ", data_chan);        // Show raw channel number in hex always

        if (data_chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL))   // If DIM register
        {
            //                      & 0x001F
            logical_dim = data_chan & (FGC_MAX_DIM_BUS_ADDR - 1); // Translate to logical address

            board_number = SysDbDimBusAddress(device.sys.sys_idx, logical_dim);

            if (board_number < FGC_MAX_DIM_BUS_ADDR)
            {
                data_chan = board_number + (data_chan & 0x00E0);
            }
        }

        // ToDo: can data_chan coded as [register:branch:board] [(0..7)(soft registers):(0..1):(0..31)

        // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)

        data = diag.data.w[data_chan];

        if (testBitmap(c->getopts, GET_OPT_HEX))      // If hex get option
        {
            fprintf(c->f, "0x%04X", data);      // Display address and data in hex
        }
        else
        {
            if (data_chan < DIAG_MAX_ANALOG)
            {
                data &= 0xFFF;
                fprintf(c->f, "%4.0f", ((float)data * DIAG_ANALOG_CAL)); // Display as millivolts
            }
            else
            {
                if (data_chan < (DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL))   // if data is digital
                {
                    data &= 0xFFF;
                    fprintf(c->f, "0x%03X", data);  // Display address and data in hex
                }
                else
                {
                    fprintf(c->f, "%u", data);      // Display software channel in decimal
                }
            }
        }

        // Next element

        c->from += c->step;
    }

    return (0);
}



uint16_t GetElapsedTime(struct cmd * c, struct prop * p)
{
    return (GetInteger(c, p));
}



uint16_t GetErrIdx(struct cmd * c, struct prop * p)
{
    // Both fieldbus and terminal commands have their own error index
    // variables. This function will link to the relevant variable and
    // then use GetInteger() to report the value.

    p->value = &c->device_par_err_idx;      // Link property value to error index for the command

    return (GetInteger(c, p));          // Display the error index
}



uint16_t GetFloat(struct cmd * c, struct prop * p)
{
    bool         bin_f;          // Binary get option active
    bool         ze_f;           // ZERO get option active
    bool         hex_f;          // HEX get option active
    bool         set_f;          // Value has been set (to zero by ZERO get opt)
    prop_size_t  n;              // Number of element in array range
    uint16_t     format_idx = 0; // Format index for DSP
    float      * value_p;        // Pointer to float value
    float        value;          // Float value
    const char * fmt;            // Pointer to format string
    uint16_t     errnum;         // Error number

    static const char   *  decfmt[]   = { "", "%.7E", "%13.5E" };  // Max length is 12 chars
    static const char   *  hexfmt[]   = { "   0x%08X", "0x%X" };           // CmdPrintfDsp in DSP lib

    bin_f = testBitmap(c->getopts, GET_OPT_BIN);              // Prepare binary flag

    hex_f = testBitmap(c->getopts, GET_OPT_HEX);              // Prepare hex flag

    ze_f  = testBitmap(p->flags, PF_GET_ZERO) &&              // Prepare zero flag
            testBitmap(c->getopts, GET_OPT_ZERO);

    if (hex_f)                                  // If HEX
    {
        fmt = hexfmt[c->token_delim == ','];
    }
    else                            // else DECIMAL
    {
        if (c->token_delim == ' ')                  // If NEAT
        {
            format_idx = 2;
        }
        else                                // else not NEAT
        {
            format_idx = 1;
        }

        fmt = decfmt[format_idx];
    }

    set_f = FALSE;
    errnum = CmdPrepareGet(c, p, 4, &n);

    if (errnum)
    {
        return (errnum);
    }

    if (n && bin_f)
    {
        CmdStartBin(c, p, (uint32_t)(sizeof(float)) * (uint32_t)n);
    }

    while (n--)   // For all elements
    {
        errnum = PropValueGet(c, p, &set_f, c->from, (uint8_t **)&value_p);

        if (errnum != 0)   // Get element value from property
        {
            return (errnum);
        }

        value = *value_p;

        if (ze_f)
        {
            *value_p = 0.0;
        }

        set_f = ze_f;

        if (bin_f)
        {
            fbsOutLong((char *) &value, c);
        }
        else
        {
            errnum = CmdPrintIdx(c, c->from);                // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            if (hex_f)
            {
                // Display as 0xXXXXXXXX
                fprintf(c->f, fmt, *((uint32_t *)value_p));
            }
            else
            {
                fprintf(c->f, fmt, (double)value);
            }
        }

        c->from += c->step;
    }

    if (set_f)
    {
        PropBlkSet(c, c->n_elements, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT); // Write back zeroed value(s)
    }

    return (0);
}



uint16_t GetId(struct cmd * c, struct prop * p)
{
    // ToDo split into 2 different functions and eliminate runtime type checking
    //
    // The max_elements field of the property structure has been used to
    // indicate the Dallas branch associated with the property:
    //  ID.VS.A     0
    //  ID.MEAS.A   1
    //  ID.VS.B     2
    //  ID.MEAS.B   3
    //  ID.LOCAL    4
    //  ID.CRATE    5
    //  ID.SUMMARY  6

    prop_size_t         n;
    uint16_t              errnum;         // Error number
    bool             neat_f;         // Neat get option flag
    uint16_t              logical_path;
    const char     *    fmt;            // Pointer to format string
    struct TMicroLAN_element_summary  * id_dev;
    struct TBranchSummary               branch_info;
    char                comp_label[FGC_SYSDB_SYS_LBL_LEN + 2];
    char                id_string[ 2 * MICROLAN_UNIQUE_SERIAL_CODE_LEN + 2 ];
    static const char * slfmt[] = { "%s%3u.%02u %-19s ", "%s:%02u.%02u:%s=" };


    if (p->sym_idx == STP_SUMMARY)      // If getting ID - report scan summary (ID.SUMMARY)
    {
        errnum = CmdPrepareGet(c, p, 1, &n);

        if (errnum)
        {
            return (errnum);
        }

        while (n--)             // For all elements
        {
            errnum = CmdPrintIdx(c, c->from);                        // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            branch_info = dallasTaskGetBranchInfo(c->from);

            fprintf(c->f, "%u|cplrs %u devs %2u {idonly %2u idtemp %2u other %2u} max_reads %u",
                    (unsigned int) c->from,
                    branch_info.ds2409s,
                    branch_info.total_devices,
                    branch_info.ds2401s,
                    branch_info.ds18b20s,
                    branch_info.unknow_devices,
                    branch_info.max_reads);

            // Next element

            c->from += c->step;
        }
    }
    else                // Get results data
    {
        setBitmap(c->getopts, GET_OPT_IDX);               // Set IDX get option
        neat_f        = (c->token_delim == ' ');        // Prepare neat flag
        logical_path  = p->max_elements;            // Max Elements in XML is branch number
        branch_info   = dallasTaskGetBranchInfo(logical_path);
        fmt           = slfmt[!neat_f];             // Prepare format for symbol string

        errnum = CmdPrepareGet(c, p, 1, &n);

        if (errnum)
        {
            return (errnum);
        }

        while (n--)             // For all elements
        {
            errnum = CmdPrintIdx(c, c->from);

            if (errnum)             // Print index if required
            {
                return (errnum);
            }

            id_dev = dallasTaskGetDeviceInfo(logical_path, c->from);

            fprintf(c->f, fmt,
                    (char *)dallasTaskIdString(&id_dev->unique_serial_code, id_string),
                    id_dev->temp_C,
                    ID_TEMP_FRAC_C(id_dev->temp_C_16),
                    (char *)id_dev->barcode.c);

            memcpy(&comp_label, CompDbLabel(dallasTaskGetCompDbIndex(logical_path, c->from)), sizeof comp_label);

            if (neat_f && comp_label[30])       // There is space for up to 30 characters
            {
                // on the terminal window (NEAT mode)
                comp_label[27] = '.';
                comp_label[28] = '.';
                comp_label[29] = '.';
                comp_label[30] = '\0';
            }

            fputs(comp_label, c->f);

            // Next element

            c->from += c->step;
        }
    }

    return (0);
}



uint16_t GetInteger(struct cmd * c, struct prop * p)
{
    bool             bin_f;          // Binary get option active
    bool             sl_f;           // Property uses symlist flag
    bool             bm_f;           // Property uses bit mask (symbol list)
    bool             ze_f;           // ZERO get option active
    bool             set_f;          // Value has been set (to zero by ZERO get opt)
    prop_size_t         n;              // Number of array elements to display
    uint16_t              type;           // Integer type index (0-5)
    uint16_t              size;           // Integer size index (0-2)
    int8_t               v8   = 0;       // 8-bit value
    int16_t              v16  = 0;       // 16-bit value
    uint16_t              u16  = 0;       // 16-bit (unsigned) value
    int32_t              v32  = 0;       // 32-bit value
    uint16_t              errnum;         // Error number
    struct sym_lst   *  sym_lst;        // Symlist address
    const char     *    fmt;            // Pointer to format string
    union
    {
        int8_t     *     p8;
        int16_t     *    p16;
        int32_t     *    p32;
    } value;

    static const char  *  slfmt[]     = { "%13s", "%s" };
    static const char  *  hexfmt[]        = { "0x%02X", "0x%04X", "0x%08X" };
    static const char  *  hexneatfmt[]    = { "         0x%02X", "       0x%04X", "   0x%08X" };
    static const char  *  decfmt[]        = { "%u", "%d", "%u", "%d", "%u", "%d" };
    static const char  *  decneatfmt[]    = { "%13u", "%13d", "%13u", "%13d", "%13u", "%13d" };
    static const uint16_t   size_bytes[]    = { 1, 2, 4 };



    sl_f   = testBitmap(p->flags,  PF_SYM_LST) &&         // Prepare symlist flag
             !testBitmap(c->getopts, GET_OPT_HEX);

    ze_f   = testBitmap(p->flags,  PF_GET_ZERO) &&            // Prepare zero flag
             testBitmap(c->getopts, GET_OPT_ZERO);

    bm_f   = testBitmap(p->flags,  PF_BIT_MASK) && sl_f;      // Prepare bit mask flag

    bin_f  = testBitmap(c->getopts, GET_OPT_BIN) && !sl_f;    // Prepare binary flag

    errnum = CmdPrepareGet(c, p, (bm_f ? 1 : 4), &n);   // Handle standard print options

    if (errnum)
    {
        return (errnum);
    }

    if (!n)                                     // If no data
    {
        return (FGC_OK_NO_RSP);                         // Return immediately with OK status
    }

    switch (p->type)            // Prepare integer type index (0-5)
    {
        case PT_INT8U:  type = 0;   break;

        case PT_INT8S:  type = 1;   break;

        case PT_INT16U: type = 2;   break;

        case PT_INT16S: type = 3;   break;

        case PT_INT32U: type = 4;   break;

        case PT_CYCSEL:
        case PT_INT32S: type = 5;   break;

        default:
            return (FGC_BAD_PARAMETER);
            break;
    }

    size = type / 2;                // Prepare integer size index (0-2)

    if (sl_f)                   // If property uses symbol list
    {
        sym_lst = (struct sym_lst *)p->range;       // Get address of symbol list
        fmt     = slfmt[(c->token_delim == ',')];   // Prepare format for symbol string
    }
    else                    // else property doesn't use a sym list
    {
        if (testBitmap(c->getopts, GET_OPT_HEX))      // Prepare format to use for integer data
        {
            fmt = ((c->token_delim == ' ') ? hexneatfmt[size] : hexfmt[size]);
        }
        else
        {
            fmt = ((c->token_delim == ' ') ? decneatfmt[type] : decfmt[type]);
        }
    }

    set_f = FALSE;

    if (bin_f)
    {
        CmdStartBin(c, p, (uint32_t)size_bytes[size] * (uint32_t)n);
    }

    while (n--)             // For all elements in the array range
    {
        errnum = PropValueGet(c, p, &set_f, c->from, (uint8_t **) &value.p8);

        if (errnum  != 0)
        {
            return (errnum);
        }

        switch (size)               // Get data and zero if required
        {
            case 0:                 // Size 0: 8-bit

                v8 = *value.p8;

                if (ze_f) { *value.p8 = 0; }

                if (bin_f)
                {
                    c->store_cb(v8);
                }

                break;

            case 1:                 // Size 1: 16-bit

                v16 = *value.p16;

                if (ze_f) { *value.p16 = 0; }

                if (bin_f)
                {
                    fbsOutShort((uint8_t *) &v16, c);
                }

                break;

            case 2:                 // Size 2: 32-bit

                v32 = *value.p32;

                if (ze_f) { *value.p32 = 0; }

                if (bin_f)
                {
                    fbsOutLong((char *) &v32, c);
                }

                break;
        }

        set_f = ze_f;

        if (!bin_f)
        {
            errnum = CmdPrintIdx(c, c->from);              // Print index if required

            if (errnum)
            {
                return (errnum);
            }

            if (sl_f)               // If property uses a sym list
            {
                uint32_t val;
                switch (size)
                {
                    case 0:  val =  v8; break;
                    case 1:  val = v16; break;
                    default: val = v32; break;
                }

                if (bm_f)                   // If property is a bit mask
                {
                    CmdPrintBitMask(c->f, sym_lst, val);    // Display all symbols which are active
                }
                else                    // else numeric symlist
                {
                    fprintf(c->f, fmt, CmdPrintSymLst(sym_lst, val)); // Display value as a symbol
                }
            }
            else                    // else property is simply integer data
            {
                switch (size)               // Display according to data size
                {
                    case 0:
                        v16 = (type ? v8 : (uint8_t)v8);

                        // Fall through

                    case 1:
                        if (type == 2)          // meaning, PT_uint16_t
                        {
                            u16 = (uint16_t)v16;
                            fprintf(c->f, fmt, u16);
                        }
                        else
                        {
                            fprintf(c->f, fmt, v16);
                        }

                        break;

                    case 2:
                        fprintf(c->f, fmt, v32);
                        break;
                }
            }
        }

        c->from += c->step;
    }

    if (set_f)
    {
        PropBlkSet(c, c->n_elements, TRUE, FGC_FIELDBUS_FLAGS_LAST_PKT); // Write back zeroed value(s)
    }

    return (0);
}



uint16_t GetLogMenuSignals(struct cmd * c, struct prop * p)
{
    // Check that the menu index is valid (between 0 and less than LOG_NUM_LIBLOG_MENUS)

    if (c->to >= LOG_NUM_LIBLOG_MENUS)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    // Does not make sense to run it from the terminal

    if (c == &tcm)
    {
        return (FGC_USE_FGCRUN_PLUS);
    }

    prop_size_t  n;
    uint16_t     errnum;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    switch (p->sym_idx)
    {
        case STP_MPX_SIG_NAMES:  errnum = logSpyGetSignals(c);          break;
        case STP_SIGNALS:        errnum = logSpyGetSelectedSignals(c);  break;
        default:                 errnum = FGC_BAD_PARAMETER;            break;
    }

    // Retrieve the log

    return (errnum);
}



uint16_t GetLogSpy(struct cmd * c, struct prop * p)
{
    // If either the BIN or ZERO option is specified

    if (   testBitmap(c->getopts, GET_OPT_BIN)     == false
        && testBitmap(c->getopts, GET_OPT_SYNCHED) == false
        && testBitmap(c->getopts, GET_OPT_DATA)    == false)
    {
        return (FGC_BAD_GET_OPT);
    }

    // Verify that a range has not been specified. Only one element (log menu) can be selected.

    if (c->from != c->to)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    // Retrieve the log

    return (logSpyGetSpy(c));
}



uint16_t GetLogPmBuf(struct cmd * c, struct prop * p)
{
    // Verify array indices are not specified

    if (c->n_arr_spec || c->step > 1)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    // Verify the BIN option is specified

    if (!testBitmap(c->getopts, GET_OPT_BIN))
    {
        return (FGC_BAD_GET_OPT);
    }

    // Verify the command is sent via the fieldbus

    if (c == &tcm || !testBitmap(c->getopts, GET_OPT_BIN))
    {
        return (0);
    }

    return (logSpyGetPmBuf(c));
}



uint16_t GetLogOasis(struct cmd * c, struct prop * p)
{
    // Verify the get options supported

    if (testBitmap(c->getopts, (GET_OPT_HEX   | GET_OPT_IDX  | GET_OPT_NOIDX |
                          GET_OPT_RANGE | GET_OPT_ZERO)))
    {
        return (FGC_BAD_GET_OPT);
    }

    // Verify the command is sent via the fieldbus

    if (c == &tcm)
    {
        return (0);
    }

    prop_size_t  n;
    uint16_t     errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    return (logSpyGetBuffer(c, (uint32_t)p->value, dpcls.oasis.subsampling, NULL, 0));
}



uint16_t GetLogEvt(struct cmd * c, struct prop * p)
{
    uint16_t     errnum = 0;
    prop_size_t  n;

    if (c->step > 1)
    {
        return(FGC_BAD_ARRAY_IDX);
    }

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }


    if (   testBitmap(c->getopts, GET_OPT_BIN)
        && (  c->n_arr_spec != 0
            || c->step > 1))
    {
        return(FGC_BAD_ARRAY_IDX);
    }

    clrBitmap(c->getopts, GET_OPT_IDX);

    if (testBitmap(c->getopts, GET_OPT_BIN) == true)
    {
        CmdStartBin(c, p, EVTLOG_SIZE_OF_RECORDS);
    }

    errnum = logEventGetLogEvt(c, testBitmap(c->getopts, GET_OPT_BIN));

    return errnum;
}



uint16_t GetMemDsp(struct cmd * c, struct prop * p)
{
    prop_size_t n;                  // Number of array elements to display
    uint32_t  addr;                   // Address in DSP memory
    uint32_t  value_int;              // Integer read from memory
    float    value_float;                // Float read from memory
    uint32_t   *  addr_int_p;             // Address to memory as int
    float  * addr_fp_p;              // Address to memory as float
    uint16_t      errnum;

    if (c->n_arr_spec)                  // If array indexes given
    {
        return (FGC_BAD_ARRAY_IDX);             // Report BAD_ARRAY_IDX
    }

    addr = dpcom.mcu.debug_mem_dsp_addr;        // Address in DSP memory
    addr_int_p = (uint32_t *) &dpcom.dsp.debug_mem_dsp_i;     // Address of data in DPCOM zone
    addr_fp_p  = (float *)   &dpcom.dsp.debug_mem_dsp_f;     // Address of data in DPCOM zone

    c->to = DB_DSP_MEM_LEN;             // Set number elements

    p->n_elements = c->to;              // Set n_elements to fool CmdPrepareGet()

    errnum = CmdPrepareGet(c, p, 2, &n);        // Prepare to report 2 32-bit words per line

    if (errnum)
    {
        return (errnum);
    }

    p->n_elements = p->max_elements;            // Restore n_elements

    while (n--)             // For all elements to show
    {
        if (c->abort_f)             // If command was aborted
        {
            return (FGC_ABORTED);           // Report ABORTED
        }

        value_float = *(addr_fp_p++);           // Get float from DPCOM
        value_int   = *(addr_int_p++);          // Get integer from DPCOM

        if ((c->token_delim == ' ') &&      // If not comma delimiter and
            !(c->line % c->linelen))      // it's time for a new line
        {
            if (testBitmap(c->getopts, GET_OPT_IDX))      // If Index required
            {
                fprintf(c->f, "\n0x%05lX:", addr);      // Write address
            }
            else if (c->line)               // else if not the first element
            {
                fputc('\n', c->f);              // Write newline
            }
        }
        else if (c->line)           // else if delimiter required
        {
            fputc(c->token_delim, c->f);        // Write delimiter character (space or comma)
        }

        c->line++;

        fprintf(c->f, "%13.5E:0x%08lX", (double) value_float, value_int); // Write int and float values in ASCII

        addr += 4;
    }

    return (0);
}



uint16_t GetMemMcu(struct cmd * c, struct prop * p)
{
    prop_size_t n;                  // Number of array elements to display
    uint16_t  value;                  // Word read from memory
    uint32_t  addr;                   // Address to memory
    uint32_t  max_n_words;                // Max num of words (to end of 20 bit address space)
    uint16_t      errnum;

    if (c->n_arr_spec || c->step > 1)           // If array indexes given or step > 1
    {
        return (FGC_BAD_ARRAY_IDX);             // Report BAD_ARRAY_IDX
    }

    if (!debug.mem[1])                  // If n_elements is zero
    {
        debug.mem[1] = 128;                 // Set 128 words by default
    }

    addr = debug.mem[0] & (0xFFFFFFFE);         // Round address to word boundary
    max_n_words = (0x100000 - addr) / 2;        // Max number of words to end of 20-bit address space

    if (debug.mem[1] < max_n_words)         // If user supplied number of words is less than max
    {
        max_n_words = debug.mem[1];             // Use user supplied number of words
    }

    if (testBitmap(c->getopts, GET_OPT_BIN))          // if BIN get option flag is set
    {
        errnum = CmdPrepareGet(c, p, 8, &n);            // Prepare to report 8 words per line

        if (errnum)
        {
            return (errnum);
        }

        n = (max_n_words < 0x8000 ? max_n_words : 0x8000);  // Clip number of words to 32K (64KB)

        CmdStartBin(c, p, (uint32_t)n * sizeof(uint16_t));      // Start binary transmission
    }
    else
    {
        c->to = (max_n_words < 512 ? max_n_words : 512);    // Clip number of words to 512 (1KB)

        p->n_elements = c->to;                  // Set n_elements to fool CmdPrepareGet()

        errnum = CmdPrepareGet(c, p, 8, &n);            // Prepare to report 8 words per line

        if (errnum)
        {
            return (errnum);
        }

        p->n_elements = p->max_elements;            // Restore n_elements
    }

    while (n--)             // For all words to show
    {
        value = *(uint16_t *) addr;

        if (testBitmap(c->getopts, GET_OPT_BIN))  // If BIN get option
        {
            fbsOutShort((uint8_t *)&value, c);        // Write word in binary
        }
        else                    // else (ASCII)
        {
            if (c->abort_f)             // If command was aborted
            {
                return (FGC_ABORTED);               // Report ABORTED
            }

            if ((c->token_delim == ' ') &&      // If not comma delimiter and
                !(c->line % c->linelen))          // it's time for a new line
            {
                if (testBitmap(c->getopts, GET_OPT_IDX))          // If Index required
                {
                    fprintf(c->f, "\n0x%05lX:", addr);          // Write address
                }
                else    // else if not the first element
                {
                    if (c->line)
                    {
                        fputc('\n', c->f);                  // Write newline
                    }
                }
            }
            else // else if delimiter required
            {
                if (c->line)
                {
                    fputc(c->token_delim, c->f);        // Write delimiter character (space or comma)
                }
            }

            c->line++;

            fprintf(c->f, "0x%04X", value);     // Write word in ASCII using HEX
        }

        addr += 2;              // Advance address to next word
    }

    return (0);
}



uint16_t GetParent(struct cmd * c, struct prop * p)
{
    uint16_t errnum;

#if (FGC_CLASS_ID == 63)
    // Parent properites must have the Parent get function.
    // But LOG.I requires specific behaviour

    if (p == &PROP_LOG_I)
    {
        errnum = GetLogI(c, p);
    }
    else
#endif
    {
        errnum = CmdParentGet(c, p, PARENT_GET_NOT_HIDDEN);
    }

    return errnum;
}


void GetPropInfo(struct cmd * c, struct prop * p)
{
    uint16_t dsp_status = FGC_OK_NO_RSP;

    struct TInternalInfoFmt
    {
        char * label;
        char * valfmt;
    };


    const char               *              fmt;
    static const char           *           lblfmt[] = { "%-16s", "%s" };
    static const struct TInternalInfoFmt    info[] =
    {
        { "struct prop *:",  "0x%08X\n"     }, //  0
        { "sym_idx:",        "%u %s\n"      }, //  1
        { "flags:",          "0x%04X\n"     }, //  2
        { "type:",           "%u %s\n"      }, //  3
        { "setif_func_idx:", "%u 0x%05lX\n" }, //  4
        { "set_func_idx:",   "%u 0x%05lX\n" }, //  5
        { "get_func_idx:",   "%u 0x%05lX\n" }, //  6
        { "n_elements:",     "%u\n"         }, //  7
        { "max_elements:",   "%u\n"         }, //  8
        { "range:",          "0x%04X\n"     }, //  9
        { "value:",          "0x%04X\n"     }, // 10
        { "dsp_idx:",        "%u\n"         }, // 11
        { "nvs_idx:",        "0x%02X\n"     }, // 12
        { "dynflags:",       "0x%02X\n"     }, // 13
        { "parent *:",       "0x%08X\n"     }, // 14
        { "is_setting:",     "%u\n"         }, // 15
    };


    if (testBitmap(p->flags, PF_DSP_FGC))
    {
        dsp_status = PropValueGet(c, p, NULL, 0, NULL);// Get n_element if DSP property
    }

    CmdPrintTimestamp(c, p);                         // Produce timestamp

    fmt = lblfmt[c->token_delim == ','];

    fprintf(c->f, fmt, info[0].label);                  // Property pointer
    fprintf(c->f, info[0].valfmt, p);

    fprintf(c->f, fmt, info[14].label);                 // Parent pointer
    fprintf(c->f, info[14].valfmt, p->parent);

    fprintf(c->f, fmt, info[1].label);                  // Symbol index (and symbol)
    fprintf(c->f, info[2].valfmt, p->sym_idx, SYM_TAB_PROP[p->sym_idx].key.c);

    fprintf(c->f, fmt, info[2].label);                  // Flags
    fprintf(c->f, info[2].valfmt, p->flags);

    fprintf(c->f, fmt, info[13].label);                 // Dynamic flags
    fprintf(c->f, info[13].valfmt, p->dynflags);

    fprintf(c->f, fmt, info[3].label);                  // Property type
    fprintf(c->f, info[3].valfmt, p->type, prop_type_name[p->type]);

    fprintf(c->f, fmt, info[4].label);                  // Setif function
    fprintf(c->f, info[4].valfmt, p->setif_func_idx, (uint32_t) SETIF_FUNC[p->setif_func_idx]);

    fprintf(c->f, fmt, info[5].label);                  // Set function
    fprintf(c->f, info[5].valfmt, p->set_func_idx, (uint32_t) SET_FUNC[p->set_func_idx]);

    fprintf(c->f, fmt, info[6].label);                  // Get function
    fprintf(c->f, info[6].valfmt, p->get_func_idx, (uint32_t) GET_FUNC[p->get_func_idx]);

    fprintf(c->f, fmt, info[7].label);                  // Number of elements

    if (dsp_status == FGC_DSP_NOT_AVL)
    {
        fprintf(c->f, "%s", fgc_errmsg[dsp_status]);
    }
    else
    {
        fprintf(c->f, info[7].valfmt, PropGetNumEls(c, p));

        if (testBitmap(p->flags, PF_INDIRECT_N_ELS))              // Indirect number of elements
        {
            fprintf(c->f, " (0x%08X)", (unsigned int)p->n_elements);
        }
    }

    fputc('\n', c->f);

    fprintf(c->f, fmt, info[8].label);                  // Max elements
    fprintf(c->f, info[8].valfmt, p->max_elements);

    fprintf(c->f, fmt, info[9].label);                  // Range
    fprintf(c->f, info[9].valfmt, p->range);

    fprintf(c->f, fmt, info[10].label);                 // Value pointer
    fprintf(c->f, info[10].valfmt, p->value);

    fprintf(c->f, fmt, info[11].label);                 // DSP property index
    fprintf(c->f, info[11].valfmt, p->dsp_idx);

    fprintf(c->f, fmt, info[12].label);                 // NVS index
    fprintf(c->f, info[12].valfmt, p->nvs_idx);

    fprintf(c->f, fmt, info[15].label);                 // NVS index
    fprintf(c->f, info[15].valfmt, p->is_setting);
}



uint16_t GetPropSize(struct cmd * c, struct prop * p)

{
    uint16_t              i;
    prop_size_t         size[3];
    const char     *    fmt;
    static const char  *  decfmt[] = { "%13u", "%u" };
    static const char  *  hexfmt[] = { "       0x%08X", "0x%08X" };
    uint16_t              errnum;

    if (testBitmap(p->flags, PF_DSP_FGC))
    {
        errnum = PropValueGet(c, p, NULL, 0, NULL);     // Get n_element if DSP property

        if (errnum)
        {
            return (errnum);
        }
    }

    CmdPrintTimestamp(c, p);                            // Produce timestamp

    size[0] = (prop_size_t)prop_type_size[p->type]; // Get element size in bytes
    size[1] = PropGetNumEls(c, p);          // Get number of elements in property
    size[2] = p->max_elements;              // Get max number of elements in property

    fmt = (testBitmap(c->getopts, GET_OPT_HEX) ? hexfmt[(c->token_delim == ',')] :    // Prepare format to respect
           decfmt[(c->token_delim == ',')]);     // Hex and Neat options

    if (testBitmap(c->getopts, GET_OPT_LBL))          // If LBL option defined
    {
        CmdPrintLabel(c);            // Display property label
    }

    for (i = 0; i < 3; i++)             // For each element of size[]
    {
        if (i)                      // If not first element
        {
            fputc(c->token_delim, c->f);        // then write delimiter
        }

        fprintf(c->f, fmt, size[i]);            // Display size parameter
    }

    fputc('\n', c->f);                  // write newline

    return (FGC_OK_NO_RSP);
}



uint16_t GetRegFgc3SlotInfo(struct cmd * c, struct prop * p)
{
    prop_size_t n;
    uint16_t    errnum;
    char        delim;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    delim = (c->token_delim == ' ' ? '\n' : ',');

    regFgc3ProgSlotPrint(c->f, delim, c->from, c->to);

    return FGC_OK_NO_RSP;
}



uint16_t GetSpivs(struct cmd * c, struct prop * p)
{
    // Displays SPIVS debug information

    static const char * fmt_opts[] = { " 0x%08X  ", "%12.5E "};

    if (c == &tcm)
    {
        return (FGC_USE_FGCRUN_PLUS);
    }

    char        const * fmt;
    prop_size_t         n;
    uint16_t            errnum;
    char                delim;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    // Set delimiter

    delim   = (c->token_delim == ' ' ? '\n' : ',');
    fmt     = (testBitmap(c->getopts, GET_OPT_HEX) ? fmt_opts[0] : fmt_opts[1]);

    fprintf(c->f, "Sent: %-10u  Faults: %-6u%c",
            (unsigned int)dpcom.spivs.num_sent,
            (unsigned int)dpcom.spivs.num_faults,
            delim);

    uint8_t i;
    uint8_t j;

    for (i = 0; i < SPIVS_HISTORY_LENGTH; ++i)
    {
        volatile struct Dpcom_spivs_dbg * const spivs_dbg = &dpcom.spivs.dbg[i];

        fprintf(c->f, "%c ", (spivs_dbg->error == true ? '*' : ' '));

        for (j = 0; j < SPIVS_DBG_NUM; ++j)
        {
            fprintf(c->f, fmt, (unsigned int)spivs_dbg->tx[j]);
        }

        fprintf(c->f, "| ");

        for (j = 0; j < SPIVS_DBG_NUM; ++j)
        {
            fprintf(c->f, fmt, (unsigned int)spivs_dbg->rx[j]);
        }

        fprintf(c->f, "%c", delim);
    }

    // Zero all the counters and mark the index for the DSP to resume
    // the logging of SPIVS diagnostics

    if (testBitmap(c->getopts, GET_OPT_ZERO) == true)
    {
        dpcom.spivs.num_sent   = 0;
        dpcom.spivs.num_faults = 0;

        dpcom.spivs.dbg_idx = 0xFFFFFFFF;

    }

    return (0);
}



uint16_t GetString(struct cmd * c, struct prop * p)
{
    // This supports scalar near strings and arrays of far strings.

    prop_size_t     n;
    uint16_t      errnum;         // Error number
    uint16_t      mux_offset;
    char * str;
    char ** strs;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    mux_offset = p->max_elements * c->cyc_sel;

    if (p->max_elements == 1 && p->type != PT_DEV_NAME)
    {
        str  = ((char *)p->value) + mux_offset;   // Add address of scalar string
        strs = &str;                // Point array at this scalar
    }
    else                    // else array property
    {
        strs = ((char **)p->value) + (c->from + mux_offset);
    }

    while (n--)                 // For all elements in the array range
    {
        errnum = CmdPrintIdx(c, c->from);          // Print index if required

        if (errnum)
        {
            return (errnum);
        }

        if (*strs)                  // If not a null pointer
        {
            fputs(*(strs), c->f);           // Display string
        }

        strs += c->step;
        c->from += c->step;
    }

    return (0);
}



uint16_t GetSubTable(struct cmd * c, struct prop * p)
{
    prop_size_t    n;
    uint16_t       errnum;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    return (pubPrintSubscriptionTable(c, n));
}



uint16_t GetTemp(struct cmd * c, struct prop * p)
{
    prop_size_t         n;
    uint16_t              hex_f;          // Hex get option flag
    uint16_t              errnum;
    uint16_t              temp;                   // Temperature
    uint16_t       *      temp_ptr;
    const char     *    fmt;
    static const char * hexfmt[] = { "       0x%04X", "0x%04X" };
    static const char * decfmt[] = { "%10u.%02u", "%u.%02u" };

    errnum = CmdPrepareGet(c, p, 4, &n);

    if (errnum)
    {
        return (errnum);
    }

    hex_f =  testBitmap(c->getopts, GET_OPT_HEX);
    fmt   = (hex_f ? hexfmt[(c->token_delim == ',')] : decfmt[(c->token_delim == ',')]);

    while (n--)                 // For all elements in the array range
    {
        errnum = PropValueGet(c, p, NULL, c->from, (uint8_t **)&temp_ptr);

        if (errnum)
        {
            return (errnum);
        }

        temp = *temp_ptr;

        errnum = CmdPrintIdx(c, c->from);          // Print index if required
        c->from++;

        if (errnum)
        {
            return (errnum);
        }

        if (hex_f)
        {
            fprintf(c->f, fmt, temp);                             // Display temperature in hex
        }
        else
        {
            fprintf(c->f, fmt, (temp >> 4), ID_TEMP_FRAC_C(temp));  // Display temperature as a float
        }
    }

    return (0);
}



uint16_t GetSemInfo(struct cmd * c, struct prop * p)
{
    OS_SEM * current_sem = fcm.sem;
    uint8_t  i;

    for (i = 0; i < FGC_NUM_SEMAPHORES; ++current_sem, ++i)
    {
        task_trace.sem_info[i] = (current_sem->pending << 16) + current_sem->counter;
    }

    return (GetInteger(c, p));
}



uint16_t GetDim(struct cmd * c, struct prop * p)
{
    uint16_t  errnum = FGC_OK_NO_RSP;

    switch (c->sym_idxs[1])
    {
        case STP_DIM_NAMES:
            errnum = DimGetDimNames(c, p);  // G DIAG.DIM_NAMES
            break;

        case STP_ANA_LBLS:
            errnum = DimGetAnaLbls(c, p);   // G DIAG.ANA_LBLS.{NAME}
            break;

        case STP_DIG_LBLS:
            errnum = DimGetDigLbls(c, p, 0); // G DIAG.DIG_LBLS.{NAME}
            break;

        case STP_FAULTS:
            errnum = DimGetFaults(c, p);    // G DIAG.FAULTS
            break;

        case STP_ANA:
            errnum = DimGetAna(c, p);       // G DIAG.ANA.{NAME}
            break;

        case STP_DIG:
            errnum = DimGetDig(c, p);       // G DIAG.DIG.{NAME}
            break;

        default:
            panicCrash(0xBAD7);
    }

    return (errnum);
}



uint16_t GetHistogram(struct cmd * c, struct prop * p)
{

    prop_size_t  n;
    uint16_t     errnum;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    struct Histogram * histo = &((struct Histogram *)p->value)[c->from];

    // Print the histograms

    histogramPrint(c->f, (c->token_delim == ' ' ? '\n' : ','), histo, n);

    // Zero the histograms

    if (testBitmap(c->getopts, GET_OPT_ZERO) == true)
    {
        uint8_t i;

        for (i = 0; i < n; i++, histo++)
        {
            histogramZero(histo);
        }

    }

    return errnum;
}



uint16_t GetLogTiming(struct cmd * c, struct prop * p)
{
    prop_size_t   n;
    uint16_t      errnum;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    // 30 characters for status

    char read_buf[TIMELOG_READ_BUF_LEN];

    while (n-- > 0)
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)
        {
            return (errnum);
        }

        logTimeGet(c->from++, read_buf);

        fprintf(c->f, "%s", read_buf);
    }

    return 0;
}


// EOF
