//! @file  logSpy.c
//! @brief Functionality related to logging
//!
//! Includes handling of I earth, LOG.TIMING and managing the interface to liblog.


// ---------- Includes

#include <string.h>
#include <errno.h>
#include <ctype.h>

#include <fgc/fgc_db.h>
#include <fgc_consts_gen.h>
#include <fgc_ether.h>
#include <fgc_errs.h>

#include <liblog/logClient.h>

#include <bitmap.h>
#include <device.h>
#include <dpcom.h>
#include <logEvent.h>
#include <time_fgc.h>
#include <logSpy.h>
#include <pc_state.h>
#include <postMortem.h>
#include <prop.h>



// ---------- Constants

//! Available payload in bytes for the log data.
//! Payload = gateway response buffer - timestamp (8 bytes) - binary flag (1 byte) - payload length (4 bytes)

static uint32_t const LOG_MAX_PAYLOAD = FGC_MAX_VAL_LEN - 8 - 1 - 4;



// ---------- Internal structures, unions and enumerations

typedef void (*fbsOut_t)(uint8_t const *, uint16_t, struct cmd *);

// NOTE:
//   OP typically subscribes to the properties LOG.OASIS.* and MEAS.*.VALUE.
//   Since log_get_sem is locked, an OASIS publication will be held until the
//   SPY data is sent. This will delay the LOG.OASIS publication and most
//   importantly the MEAS.*.VALUE, which might be needed by the Software
//   Interlock System (SIS). This is the case for the LT.BHZ20. If the SIS
//   does not receive the MEAS.*.VALUE in time, it activates the beam stopper.
//   That is why two instancse of LOG_read are needed

//! Internal local variables for the module logSpy.
//!
//! Index 0 is used with the fieldbus command task whilst
//! index 1 is used with the publication task

struct logSpy
{
    struct LOG_mgr           * log_mgr;                 //!< Structure declared in liblog used to access logging data
    struct LOG_read_request    read_request[2];         //!< Read request structure for communication from the DSP
    union  LOG_header          log_header[2];           //!< Log header
};



// ---------- External variable definitions

struct LOG_menus  __attribute__((section("sram")))  log_menus;
struct Log_props                                    log_props;
OS_SEM                                            * log_get_sem;



// ---------- Internal variable definitions

// Variable name 'log' already taken by the standard library

static struct logSpy  __attribute__((section("sram"))) logSpy;

#if (FGC_CLASS_ID == 62)
   #define LOG_FUNC_TYPE(func_type)  ("PULSE")
#elif (FGC_CLASS_ID == 63)
   #include <class.h>
   #define LOG_FUNC_TYPE(func_type)  (CmdPrintSymLst(SYM_LST_REF_TYPE, func_type))
#else
   #define LOG_FUNC_TYPE(func_type)  ("NONE")
#endif



// ---------- Internal function definitions

static void logSpyGetDimSignalNames(union LOG_header * header, uint32_t const dim_idx)
{
    fgc_db_t dim_type_idx = SysDbDimDbType(device.sys.sys_idx, dim_idx);
    uint8_t  sig_idx      = 0;
    fgc_db_t ana_idx;

    for (ana_idx = 0; ana_idx < FGC_N_DIM_ANA_CHANS; ++ana_idx)
    {
        // If channel is in use, change the signal name

        if (DimDbIsAnalogChannel(dim_type_idx, ana_idx))
        {
            char               name_buf[SPY_SIG_NAME_LEN];
            char const *       sig_name;
            char const * const label = DimDbAnalogLabel(dim_type_idx, ana_idx);
            char const * const units = DimDbAnalogUnits(dim_type_idx, ana_idx);

            if(units[0] == '\0')
            {
                // No units provided so just use the label

                sig_name = label;
            }
            else
            {
                // Units provided so name is {label}_({units})

                snprintf(name_buf, SPY_SIG_NAME_LEN, "%s_(%s)", label, units);

                sig_name = name_buf;
            }

            logChangeSpySignalNameSwap(&header->spy, sig_idx++, sig_name);
        }
    }
}



static uint16_t logSpyProcessRequest(struct cmd                                * c,
                                     struct LOG_read_request                   * read_request,
                                     union  LOG_header                         * log_header,
                                     volatile struct Dpcom_dsp_log_comms const * comms,
                                     uint8_t                             const   cmd_idx)
{
    // LOG_PRIVATE properties must be protected since they can be accessed by
    // the fieldbus and publication commands

    OSSemPend(log_get_sem);

    // Set LOG.PRIVATE.READ_REQUEST to trigger an action on the DSP

    dpcom.log.cmd_idx      = cmd_idx;
    dpcom.log.read_request = true;

    PropSet(c, &PROP_LOG_PRIVATE_READ_REQUEST, read_request, LOG_READ_REQUEST_LEN);

    // Wait for the DSP to complete the request or timeout (5 seconds)

    uint32_t timeout = timeGetUs();

    while (dpcom.log.read_request == true && timeGetUsDiffNow(timeout) <= 5000000U)
    {
        // Resumed by msTask() on next millisecond

        OSTskSuspend();
    }

    if (timeGetUsDiffNow(timeout) > 5000000U)
    {
        OSSemPost(log_get_sem);

        return (FGC_DSP_NOT_AVL);
    }

    // Check if an error has occurred

    if (comms->read_req_status != LOG_READ_CONTROL_SUCCESS)
    {
        OSSemPost(log_get_sem);

        switch (comms->read_req_status)
        {
            case LOG_READ_CONTROL_INVALID_INDEX:
            case LOG_READ_CONTROL_NOT_ENOUGH_SAMPLES: return FGC_NO_DATA;
            case LOG_READ_CONTROL_NO_NEW_DATA:        return FGC_NO_NEW_DATA;
            case LOG_READ_CONTROL_LOG_OVERWRITTEN:    return FGC_LOG_OVERWRITTEN;
            case LOG_READ_CONTROL_BUFFER_TOO_SMALL:   return FGC_RSP_BUF_FULL;
            case LOG_READ_CONTROL_INVALID_CYC_SEL:    return FGC_BAD_CYCLE_SELECTOR;
            default:                                  return FGC_NOT_IMPL;              // LOG_READ_CONTROL_DISABLED or LOG_READ_CONTROL_NO_SIGNALS
        }
    }

    // Retrive the log header if needed

    if (log_header != NULL)
    {
        union LOG_header * const dsp_header;

        PropValueGet(c, &PROP_LOG_PRIVATE_HEADER, NULL, 0, (uint8_t **)&dsp_header);

        // Make sure the header size is a multiple of 4. This is imporant when retrieving
        // the signal names since the data is little endian and the bytes are swapped.
        // If the headr size was 17 the last character would be in the fourth byte of the
        // last word: ___B. However the 'B' would not be memcpy().

        uint32_t size = (comms->header_size + 3) & ~0x0003;

        if (size > sizeof (union LOG_header))
        {
            return FGC_DSP_PROPERTY_ISSUE;
        }

        memcpy((void *)log_header, (void *)dsp_header, size);
    }

    OSSemPost(log_get_sem);

    return FGC_OK_NO_RSP;
}



static void logSpyInterceptLogNames(union  LOG_header       * const log_header,
                                    enum   LOG_menu_index     const menu_index)
{
    // If retrieveing analogue DIM data, rename DIM log signals using the name
    // defined in the DimDb the DIM signal names

    if (   menu_index >= LOG_MENU_DIM
        && menu_index <  (LOG_MENU_DIM + LOG_DIM_NUM_LOGS))
    {
        logSpyGetDimSignalNames(log_header, (menu_index - LOG_MENU_DIM));
    }

    logSpyClassInterceptLogNames(log_header, menu_index);
}



static void logSpySendHeader(struct cmd              * const c,
                             union  LOG_header       * const log_header,
                             enum   LOG_menu_index     const menu_index,
                             uint32_t                  const header_size,
                             fbsOut_t                  const fbsOut)
{
    logSpyInterceptLogNames(log_header, menu_index);

    fbsOut((uint8_t *)log_header, header_size, c);
}



static uint16_t logSpySendSignals(struct cmd                                * c,
                                  volatile struct Dpcom_dsp_log_comms       * comms,
                                  uint8_t                             const   cmd_idx,
                                  fbsOut_t                            const   fbsOut)
{
    int32_t   total_data_len = comms->data_size / sizeof(uint32_t);
    uint32_t  data_len       = 0;
    uint8_t   blk_idx        = 0;
    uint32_t  timeout;

    // Trigger the DSP to start transferring the signals

    setBitmap(dpcom.mcu.log.signals_request[cmd_idx],   cmd_idx == 0
                                                      ? DPCOM_LOG_SIGNALS_REQUEST_FCM_BIT_MASK
                                                      : DPCOM_LOG_SIGNALS_REQUEST_PCM_BIT_MASK);
    while (total_data_len > 0)
    {
        // Wait for the DSP to transfer one block of data over the DPRAM
        // Timeout of 1 seconds

        timeout = timeGetUs();

        while ((data_len = comms->blk_len[blk_idx]) == 0 && timeGetUsDiffNow(timeout) <= 1000000U)
        {
            // Resumed by msTask() on next millisecond

            OSTskSuspend();
        }

        if (timeGetUsDiffNow(timeout) > 1000000U)
        {
            return FGC_DSP_NOT_AVL;
        }

        total_data_len -= data_len;

        // Ouput a block worth of data

        uint32_t * value = &c->prop_buf->blk.log[blk_idx][0];

        while (data_len > 0)
        {
            fbsOut((uint8_t *)value, 4, c);

            data_len--;
            value++;
        }

        comms->blk_len[blk_idx] = 0;
        blk_idx ^= 0x01;
    }

    return FGC_OK_NO_RSP;
}



static void logSpySendSignalNames(struct cmd * c,
                                  enum LOG_read_action const action)
{
    enum LOG_menu_index menu_index;

    for (menu_index = c->from; menu_index <= c->to; menu_index += c->step)
    {
        if (CmdPrintIdx(c, c->from) != 0)
        {
            continue;
        }

        // If retrieving analogue DIM data, rename DIM log signals using the name
        // defined in the DimDb.

        if (   menu_index >= LOG_MENU_DIM
            && menu_index <  (LOG_MENU_DIM + LOG_DIM_NUM_LOGS))
        {
            if (action != LOG_READ_GET_SIG_NAMES)
            {
                continue;
            }

            fgc_db_t  const dim_type_idx = SysDbDimDbType(device.sys.sys_idx, (menu_index - LOG_MENU_DIM));

            if (dim_type_idx == 0)
            {
                continue;
            }

            fgc_db_t ana_idx;

            for (ana_idx = 0; ana_idx < FGC_N_DIM_ANA_CHANS; ++ana_idx)
            {
                // If channel is in use, change the name

                if (DimDbIsAnalogChannel(dim_type_idx, ana_idx) == true)
                {
                    char const * buf = DimDbAnalogLabel(dim_type_idx, ana_idx);

                    // Send the signal names.

                    while (*buf != '\0')
                    {
                        c->store_cb(*(buf++));
                    }

                    c->store_cb(' ');
                }
            }
        }

        // Non-DIM menus

        else
        {
            uint8_t                             const   cmd_idx      = (c == &fcm ? 0 : 1);
            struct LOG_read_request                   * read_request = &logSpy.read_request[cmd_idx];
            union  LOG_header                         * log_header   = &logSpy.log_header[cmd_idx];
            volatile struct Dpcom_dsp_log_comms const * comms        = &dpcom.dsp.log.comms[cmd_idx];

            if (logPrepareReadMenuSigNamesRequest(&log_menus, menu_index, action, read_request) == true)
            {
                // Send request to the DSP

                if (logSpyProcessRequest(c, read_request, log_header, comms, cmd_idx) != FGC_OK_NO_RSP)
                {
                    continue;
                }

                uint32_t const   header_size = comms->header_size;
                char     const * buf         = log_header->sig_names;
                uint16_t         index;
                char             ch;

                for (index = 0; index < header_size && (ch = buf[index ^ 3]) != '\0'; ++index)
                {
                    c->store_cb(ch);
                }
            }
        }
    }
}



static bool logSpyGetSpyParsInt(char      data[CMD_MAX_DAVA_VALUES_SIZE],
                             int32_t * time)
{
    char    * chr       = data;
    size_t    value_len = strlen(data);
    bool      error     = false;
    uint8_t   i;

    // Make sure all the characters are digits

    for (i = 0; i < value_len && error == false; i++, chr++)
    {
        if (i == 0 && (*chr == '-' || *chr == '+'))
        {
            continue;
        }

        error |= isdigit((unsigned char)*chr) == 0;
    }

    *time = strtol(data, NULL, 10);
    error |= (errno != 0);

    return error;
}



static bool logSpyGetSpyParsUInt(char       data[CMD_MAX_DAVA_VALUES_SIZE],
                              uint32_t * time)
{
    char    * chr       = data;
    size_t    value_len = strlen(data);
    bool      error     = false;
    uint8_t   i;

    // Make sure all the characters are digits

    for (i = 0; i < value_len && error == false; i++, chr++)
    {
        if (i == 0 && *chr == '+')
        {
            continue;
        }

        error |= isdigit((unsigned char)*chr) == 0;
    }

    *time = strtoul(data, NULL, 10);
    error |= (errno != 0);

    return error;
}



// ---------- External function definitions

void logSpyInit(void)
{
    CC_ASSERT(LOG_NUM_LIBLOG_MENUS - LOG_DIM <= FGC_MAX_DIMS);

    // Initialize local variables

    // Import LOG_mgr pointer

    logSpy.log_mgr = (struct LOG_mgr *)&dpcom.log.log_mgr;

    // Inirialize global vairables

    dpcom.mcu.log.zero_slow_abort_time = false;
    dpcom.mcu.log.signals_request[0]   = 0;
    dpcom.mcu.log.signals_request[1]   = 0;

    // Initialize liblog menus (log_menus)

    logMenusInit(logSpy.log_mgr, &log_menus, LOG_MAX_PAYLOAD);

    // Initialize DIM logs

    fgc_db_t dim_idx;

    for (dim_idx = 0; dim_idx < LOG_DIM_NUM_LOGS; dim_idx++)
    {
        uint8_t sig_mask = 0;

        if (SysDbDimEnabled(device.sys.sys_idx, dim_idx) == true)
        {
            fgc_db_t dim_type_idx = SysDbDimDbType(device.sys.sys_idx, dim_idx);

            // Make sure there is at least one analogue signal

            for (fgc_db_t ana_idx = 0; ana_idx < FGC_N_DIM_ANA_CHANS; ++ana_idx)
            {
                if (DimDbIsAnalogChannel(dim_type_idx, ana_idx))
                {
                    sig_mask |= 1 << ana_idx;
                }
            }
        }

        // If the DIM does not exist or has no analogue signals, disabled it

        uint8_t log_menu_idx = LOG_MENU_DIM + dim_idx;

        if (sig_mask == 0)
        {
            logMenuDisable(&log_menus, log_menu_idx);
        }
        else
        {
            // Otherwise, update the log menu name and set the SAVE flag so that it can
            // be read out by hte FGC logger

            logChangeMenuName      (&log_menus, log_menu_idx, (char *)SysDbDimName(device.sys.sys_idx, dim_idx));
            logModifyMenuSigSelMask(&log_menus, log_menu_idx, sig_mask);
        }
    }

    logSpyClassInit();
}



void logSpyBgp(void)
{
    static bool bgp_guard_20ms = true;

    // Enable/disable logs and log menus

    logSpyClassEnableDisableLogs();

    // Update the log menu status periodically (every 20 ms)

    if (ms_task.ms_mod_20 == 0)
    {
        if (bgp_guard_20ms == true)
        {
            logMenuUpdate(logSpy.log_mgr, &log_menus);

            bgp_guard_20ms = false;
        }
    }
    else
    {
        bgp_guard_20ms = true;
    }
}



uint16_t logSpyGetSignals(struct cmd * const c)
{
    logSpySendSignalNames(c, LOG_READ_GET_ALL_SIG_NAMES);

    return FGC_OK_NO_RSP;
}



uint16_t logSpyGetSelectedSignals(struct cmd * const c)
{
    logSpySendSignalNames(c, LOG_READ_GET_SIG_NAMES);

    return FGC_OK_NO_RSP;
}



uint16_t logSpyGetSpy(struct cmd * const c)
{
    uint32_t const menu_idx = c->from;

    // Check that the menu index is valid (between 0 and less than LOG_NUM_LIBLOG_MENUS)

    if (menu_idx > LOG_NUM_LIBLOG_MENUS)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    // Unfreeze the log

    if (testBitmap(c->getopts, GET_OPT_SYNCHED) == true)
    {
        postMortemSyncLog(logMenuIndexToLogIndex(&log_menus, menu_idx));

        return FGC_OK_NO_RSP;
    }

    uint16_t errnum;

    // Handle Alias logs in a class-specific manner

    if (   testBitmap(log_menus.status_bit_mask[menu_idx], LOG_MENU_STATUS_ALIAS_BIT_MASK) == true
        && (errnum = logSpyClassProcessAlias(menu_idx)) != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    // Get the DATA values

    struct LOG_read_timing read_timing;

    memset(&read_timing, 0, sizeof(read_timing));

    if (testBitmap(c->getopts, GET_OPT_DATA) == true)
    {
        // There must always be 6 DATA values: time_origin_s, time_origin_ns, time_offset_ms, duration_ms, prev_first_sample_time_s, prev_first_sample_time_ns

        if (c->data_len != 6)
        {
            return FGC_BAD_GET_OPT;
        }

        bool error = false;

        errno = 0;

        error |= logSpyGetSpyParsUInt(c->data[0], &read_timing.time_origin.secs.abs);
        error |= logSpyGetSpyParsInt (c->data[1], &read_timing.time_origin.ns);
        error |= logSpyGetSpyParsInt (c->data[2], &read_timing.time_offset_ms);
        error |= logSpyGetSpyParsUInt(c->data[3], &read_timing.duration_ms);
        error |= logSpyGetSpyParsUInt(c->data[4], &read_timing.prev_first_sample_time.secs.abs);
        error |= logSpyGetSpyParsInt (c->data[5], &read_timing.prev_first_sample_time.ns);

        if (error == true)
        {
            return FGC_BAD_GET_OPT;
        }

        // Allowed ranges
        // Nanoseconds: [0..999999999]
        // Duration:    [0..999999] for cyclic requests [0..999999999] for non-cyclic requests
        // Time offset: [-999999..999999]

        if (   read_timing.time_origin.ns            > 999999999
            || read_timing.prev_first_sample_time.ns > 999999999
            || read_timing.time_offset_ms            < -999999
            || read_timing.time_offset_ms            >  999999
            || read_timing.duration_ms               >  (c->cyc_sel == 0 ? 999999999 : 999999))
        {
            return FGC_BAD_GET_OPT;
        }
    }

    uint8_t                             const   cmd_idx      = (c == &fcm ? 0 : 1);
    struct LOG_read_request                   * read_request = &logSpy.read_request[cmd_idx];
    union  LOG_header                         * log_header   = &logSpy.log_header[cmd_idx];
    volatile struct Dpcom_dsp_log_comms       * comms        = &dpcom.dsp.log.comms[cmd_idx];

    // Prepare struct LOG_read_request.

    logPrepareReadMenuRequest(&log_menus,
                              menu_idx,
                              LOG_READ_GET_SPY,
                              c->cyc_sel,
                              &read_timing,
                              0,                // sub_sampling
                              read_request);

    // Send request to the DSP

    errnum = logSpyProcessRequest(c, read_request, log_header, comms, cmd_idx);

    if (errnum != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    // Write binary data header: timestamp, 0xFF, length

    fbsOutLong((char *) &comms->first_sample_time.secs.abs,  c);
    fbsOutLong((char *) &comms->first_sample_time.us, c);

    c->store_cb(0xFF);

    uint32_t const total_size = comms->header_size + comms->data_size;

    fbsOutLong((char *) &total_size, c);

    // The logs in the DSP are little endian but swapped to big endian when going through the
    // DPRAM. The SPY data format must be little endian. The MCU must therefore swap the logs
    // back to little endian (fbsOutBufHtonl()).

    logSpySendHeader(c, log_header, menu_idx, comms->header_size, fbsOutBufHtonl);

    errnum = logSpySendSignals(c, comms, cmd_idx, fbsOutBufHtonl);

    return errnum;
}



uint16_t logSpyGetBuffer(struct cmd              * c,
                         uint32_t                  log_sig_index,
                         uint32_t                  sub_sampling_period,
                         struct CC_us_time const * log_i_time_origin,
                         uint32_t          const   log_i_duration_ms)
{
    // This function is used to retreive the LOG.OASIS and LOG.I buffers. The former
    // is used by LN4, PSB, CPS, LN3, LEIR, ELENA, ADE, whilst the latter by SPS.

    uint8_t                   const cmd_idx      = (c == &fcm ? 0 : 1);
    struct LOG_read_request * const read_request = &logSpy.read_request[cmd_idx];

    // Oasis buffers do not define log_i_time_origin

    if (log_i_time_origin == NULL)
    {
#if (FGC_CLASS_ID == 63)

        // Discern which log to use REG or MEAS based on the calculated subsample factor
        // calculated in the DSP regulation::regulationUpdatePars()

        if (log_sig_index == LOG_SIG_IDX_I_MEAS_I_REF_DELAYED)
        {
            if (dpcls.dsp.meas.oasis_subsample_i_reg > 0)
            {
                log_sig_index       = LOG_SIG_IDX_I_REG_I_REF_USER;
                sub_sampling_period = dpcls.dsp.meas.oasis_subsample_i_reg;
            }
        }
        else if (log_sig_index == LOG_SIG_IDX_I_MEAS_I_MEAS)
        {
            if (dpcls.dsp.meas.oasis_subsample_i_reg > 0)
            {
                log_sig_index       = LOG_SIG_IDX_I_REG_I_MEAS_REG;
                sub_sampling_period = dpcls.dsp.meas.oasis_subsample_i_reg;
            }
        }
        else if (log_sig_index == LOG_SIG_IDX_B_MEAS_B_REF_DELAYED)
        {
            if (dpcls.dsp.meas.oasis_subsample_b_reg > 0)
            {
                log_sig_index       = LOG_SIG_IDX_B_REG_B_REF_USER;
                sub_sampling_period = dpcls.dsp.meas.oasis_subsample_b_reg;
            }
        }
        else if (log_sig_index == LOG_SIG_IDX_B_MEAS_B_MEAS)
        {
            if (dpcls.dsp.meas.oasis_subsample_b_reg > 0)
            {
                log_sig_index       = LOG_SIG_IDX_B_REG_B_MEAS_REG;
                sub_sampling_period = dpcls.dsp.meas.oasis_subsample_b_reg;
            }
        }
#endif
    }

    // Prepare struct LOG_read_request.

    struct LOG_read_timing read_timing;

    memset(&read_timing, 0, sizeof(read_timing));

    // log_i_time_origin is only defined when retrieving LOG.I buffer for SPS

    if (log_i_time_origin != NULL)
    {
        read_timing.duration_ms = log_i_duration_ms;
        read_timing.time_origin = cctimeUsToNsRT(*log_i_time_origin);
    }

    logPrepareReadSignalRequest(&log_menus,
                                log_sig_index,
                                LOG_READ_GET,
                                c->cyc_sel,
                                &read_timing,
                                sub_sampling_period,
                                read_request);

    volatile struct Dpcom_dsp_log_comms * comms = &dpcom.dsp.log.comms[cmd_idx];

    uint16_t errnum = logSpyProcessRequest(c, read_request, NULL, comms, cmd_idx);

    // Write binary data header: 0xFF, length

    c->store_cb(0xFF);

    fbsOutLong((char *)&comms->data_size, c);

    if (errnum == FGC_OK_NO_RSP)
    {
        // The logs in the DSP are little endian but swapped to big endian when going through the
        // DPRAM. The SPY data format must be little endian. The MCU must therefore swap the logs
        // back to little endian.

        errnum = logSpySendSignals(c, comms, cmd_idx, fbsOutBuf);
    }

    return errnum;
}



uint16_t logSpyGetPmBuf(struct cmd * const c)
{
    static uint32_t const pm_buf_size    = LOG_PM_BUF_SIZE;
    static uint32_t const pm_buf_version = LOG_PM_BUF_VERSION;

    uint8_t                               const cmd_idx      = (c == &fcm ? 0 : 1);
    volatile struct Dpcom_dsp_log_comms *       comms        = &dpcom.dsp.log.comms[cmd_idx];
    struct LOG_read_request             * const read_request = &logSpy.read_request[cmd_idx];
    union  LOG_header                   * const log_header   = &logSpy.log_header[cmd_idx];
    enum   LOG_menu_index                       menu_index;
    uint16_t                                    errnum       = FGC_OK_NO_RSP;

    // Write binary data header in network byte order: time stamp, 0xFF, length, version

    fbsOutLong((char *) &comms->first_sample_time.secs.abs,  c);
    fbsOutLong((char *) &comms->first_sample_time.us, c);

    c->store_cb(0xFF);

    fbsOutLong((char *) &pm_buf_size, c);
    fbsOutLong((char *) &pm_buf_version, c);

    // Add the event logs in network byte order

    logEventGetLogEvt(c, true);

    // The logs in the DSP are little endian but swapped to big endian when going through the
    // DPRAM. The pm_buf data must be sent as little endian so it must be swapped again using
    // fbsOutBufHtonl() before writing to the network.

    for (menu_index = 0; menu_index < LOG_NUM_LIBLOG_MENUS; menu_index++)
    {
        // Skip log menus that are not PM_BUF

        if (logMenuIsInPmBuf(&log_menus, menu_index))
        {
            // Prepare struct LOG_read_request. Setting time_origin to zero will instruct liblog
            // to set it to the log freeze request time, which is considered to be the post mortem time.

            struct LOG_read_timing read_timing;

            memset(&read_timing, 0, sizeof(read_timing));

            logPrepareReadPmBufRequest(&log_menus, menu_index, &read_timing, read_request);

            // Send request to the DSP

            errnum = logSpyProcessRequest(c, read_request, log_header, comms, cmd_idx);

            if (errnum != FGC_OK_NO_RSP)
            {
                break;
            }

            logSpySendHeader(c, log_header, menu_index, comms->header_size, fbsOutBufHtonl);

            logSpySendSignals(c, comms, cmd_idx, fbsOutBufHtonl);
        }
    }

    return errnum;
}



void logSpyDimAnaProcess(void)
{
    // Transfer the analogue values for one DIM to the DSP for logSpy

    if (ms_task.ms_mod_20 >= FGC_NUM_DIMS_BRANCH)
    {
        return;
    }

    // The analogue values are scaled at 19 ms. and transferred to
    // the DSP in the following 20 ms cycle. The timestamp of the
    // values must refer to the previous 20 ms cycle.

    if (ms_task.ms_mod_20 == 0)
    {
        struct CC_us_time time_stamp = ms_task.time_us;

        timeUsAddOffset(&time_stamp, -20000);

        dpcom.mcu.dim_ana.time_stamp = time_stamp;
    }

    uint16_t dim_idx = ms_task.ms_mod_20;
    uint8_t  i;

    for (i = 0; i < FGC_N_DIM_ANA_CHANS; ++i)
    {
        dpcom.mcu.dim_ana.values[i] = dim_collection.analogue_in_physical[i][dim_idx];
    }

    dpcom.mcu.dim_ana.dim_idx = dim_idx;
}


// EOF
