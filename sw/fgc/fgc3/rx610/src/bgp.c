//! @file   bgp.c
//! @brief  FGC MCU Software - Background processing Task
//!
//! The background task takes the role of an idle task. It never blocks and is used to do
//! background monitoring of the FGC.


// ---------- Includes

#include <bgp.h>

#include <anaCard.h>
#include <dpcom.h>
#include <logSpy.h>
#include <logRun.h>
#include <mcu.h>
#include <taskTrace.h>

#include <panic.h>


// ---------- External function definitions

void bgpTask(void * unused)
{
    for (;;)
    {
        taskTraceReset();

        anaCardCpyMpx();

        taskTraceInc();

        // Check Stack Usage

        mcuCheckStack();

        logSpyBgp();
    }
}


// EOF
