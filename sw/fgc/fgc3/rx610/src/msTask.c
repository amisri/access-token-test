//! @file  msTask.c
//! @brief Millisecond actions


// ---------- Includes

#include <anaCard.h>
#include <class.h>
#include <config.h>
#include <defconst.h>
#include <defprops.h>
#include <diag.h>
#include <dimDataProcess.h>
#include <dpcom.h>
#include <events.h>
#include <fbs.h>
#include <fgc_ether.h>
#include <gateway.h>
#include <fgc_runlog_entries.h>
#include <interFgc.h>
#include <leds.h>
#include <logRun.h>
#include <masterClockPll.h>
#include <mcu.h>
#include <msTask.h>
#include <nvs.h>
#include <pulsePermit.h>
#include <rtd.h>
#include <pub.h>
#include <postMortem.h>
#include <regfgc3Prog.h>
#include <status.h>
#include <time_fgc.h>
#include <trm.h>



// ---------- Internal structures, unions and enumerations

typedef void (*msTask_func)(void);

struct MsTask_field
{
    uint16_t    ms_of_action;  //!< The ms, in a certain interval, that the action will be performed
    uint16_t    interval;      //!< The periodicity of the action, in ms
    msTask_func action;        //!< Function that will be executed
};



// ---------- External variable definitions
struct MsTask ms_task;



// ---------- Internal function declarations

static void msTaskIterationStart(void);
static void msTaskResumeRtd(void);
static void msTaskTempRegul(void);
static void msTaskUpdateCounters(void);
static void msTaskResumeStateTask(void);
static void msTaskResumeDallasTask(void);
static void msTaskConfigCheck(void);
static void msTaskGetVrefTemp(void);
static void msTaskPublishAdcAmps200ms(void);
static void msTaskUpdateDiag(void);
static void msTaskUpdateTiming(void);
static void msTaskProcess(void);

static void msTaskUpdateFieldbus(void);
static void msTaskUpdateGwPermit(void);
static void msTaskUpdateCpuUsage(void);
static void msTaskUpdateMsDuration(void);
static void msTaskValidateMsTsks(void);
static void msTaskValidateDspTsks(void);



// ---------- Internal variable definitions

static struct MsTask_field ms_task_actions[] =
{
    { 0,                 1000, msTaskValidateDspTsks           },
    { 0,                 1000, msTaskUpdateDiag                },
    { 0,                 1000, gatewayCheckStatusTimePackets   },
    { 12,                1000, msTaskTempRegul                 },
    { 999,               1000, msTaskUpdateCounters            },
    { 0,                  500, pub2HzPublication               },
    { 20,                 500, regFgc3ProgSetJobInPb           },
    { 1,                  200, msTaskGetVrefTemp               },
    { 1,                  200, msTaskPublishAdcAmps200ms       },
    { 3,                  100, msTaskResumeRtd                 },
    { 5,                  100, fbsCheckGwTimeout               },
    { 10,                 100, msTaskUpdateTiming              },
    { 10,                 100, msTaskResumeDallasTask          },
    { 10,                 100, msTaskConfigCheck               },
    { 2,                   20, mcuReadPsuVoltage               },
    { 10,                  20, masterClockPllIteration         },
    { 12,                  20, eventsProcess                   },
    { DEV_STA_TSK_PHASE,    5, msTaskResumeStateTask           },
    { 0,                    1, msTaskProcess                   }
};



// ---------- Internal function definitions

static void msTaskIterationStart(void)
{
    uint32_t utc_s  = timeGetUtcTime();
    uint32_t utc_ms = timeGetUtcTimeMs();
    uint32_t utc_us = utc_ms * 1000;

    ms_task.prev_time  = ms_task.time_us;
    ms_task.time_us.secs.abs  = utc_s;
    ms_task.time_us.us = utc_us;
    ms_task.time_ms.secs.abs  = utc_s;
    ms_task.time_ms.ms = utc_ms;
    ms_task.ms         = utc_ms;
    ms_task.ms_mod_5   = utc_ms % 5;
    ms_task.ms_mod_10  = utc_ms % 10;       // Modulo  10 counter for  10 ms period activity
    ms_task.ms_mod_20  = utc_ms % 20;       // Modulo  20 counter for  20 ms period activity
    ms_task.s_mod_10   = (uint16_t)(ms_task.time_us.secs.abs % 10L);

    // Read the DSP counters, to check their status later on in this function.

    ms_task.dsp_rtp_alive_cnt = dpcom.dsp_rtp_alive;
}



static void msTaskResumeRtd(void)
{
    if (rtdIsEnabled() == true)
    {
        OSTskResume(MCU_TSK_RTD);
    }
}



static void msTaskTempRegul(void)
{
    if (anaCardTempRegulOn() == true)
    {
        anaCardTempRegul();
    }
}



static void msTaskUpdateCounters(void)
{
    shared_mem.power_time++;
    shared_mem.run_time++;
}



// Trigger state task and check for device control actions

static void msTaskResumeStateTask(void)
{
    static uint32_t latch_state_task_watchodg = 0;

    mcuDeviceControl();

    ms_task.state_task_watchdog++;

    // If stateTask watchdog failed to complete within 25ms
    // Only report it once a second

    if (ms_task.state_task_watchdog > 5 && ms_task.time_us.secs.abs != latch_state_task_watchodg)
    {
        logRunAddTimestamp();
        logRunAddEntry(FGC_RL_STATSK, &ms_task.state_task_watchdog);

        ms_task.state_task_watchdog = 0;
        latch_state_task_watchodg = ms_task.time_us.secs.abs;
    }

    // Set PM trig property from FIP ack byte

    ms_task.log_pm_trig = fbs.u.fieldbus_stat.ack & (FGC_EXT_PM_REQ | FGC_SELF_PM_REQ);

    // Wake up stateTask

    OSTskResume(MCU_TSK_STA);
}



static void msTaskResumeDallasTask(void)
{
    OSTskResume(MCU_TSK_DLS);
}



// Check config state/mode

static void msTaskConfigCheck(void)
{
    if (configGetMode() == FGC_CFG_MODE_SYNC_FAILED)
    {
        configSetState(FGC_CFG_STATE_UNSYNCED, false);
    }

    if (   configGetState() == FGC_CFG_STATE_UNSYNCED
        || nvs_dbg.n_corrupted_indexes >  0)
    {
        statusSetWarnings(FGC_WRN_CONFIG);
    }
    else
    {
        statusClrWarnings(FGC_WRN_CONFIG);
    }
}



static void msTaskGetVrefTemp(void)
{
    dpcom.mcu.vref_temp.meas = anaCardGetTemp();
}



static void msTaskPublishAdcAmps200ms(void)
{
    // Class 65 doesn't control a power supply, so this does not apply

#if FGC_CLASS_ID != 65
    pubPublishProperty(&PROP_ADC_AMPS_200MS, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, FALSE);
#endif
}



static void msTaskUpdateDiag(void)
{
    static uint32_t last_period_start   = 0;
    static uint32_t dim_sync_reset_prev = 0;
    static uint32_t dim_mismatched_prev = 0;
    static uint8_t  dim_sync_counter    = 0;

    // Every second: Check for DIAG faults and run set command counter

    if (last_period_start == 0)
    {
        last_period_start = ms_task.time_us.secs.abs;
    }

    if ((ms_task.time_us.secs.abs - last_period_start) >= 60)
    {
        // Set the latched condition FGC_LAT_DIMS_EXP_FLT only if during
        // periods of 1 minute the rate of faults is larger than ten (10).

        if ((qspi_misc.expected_detected_mismatches - dim_mismatched_prev) >= DIAG_EXPEC_LATCHED_FAULT)
        {
            statusSetLatched(FGC_LAT_DIMS_EXP_FLT);
        }

        // Set the latched condition FGC_LAT_DIM_SYNC_FLT if during three
        // consecutive minutes the number of errors have increased.

        if ((diag.sync_resets - dim_sync_reset_prev) > 0)
        {
            if (++dim_sync_counter == 3)
            {
                statusSetLatched(FGC_LAT_DIM_SYNC_FLT);

                dim_sync_counter = 0;
            }
        }
        else
        {
            dim_sync_counter = 0;
        }

        last_period_start   = ms_task.time_us.secs.abs;
        dim_sync_reset_prev = diag.sync_resets;
        dim_mismatched_prev = qspi_misc.expected_detected_mismatches;
    }

    // Command counter

    if (ms_task.set_cmd_counter)
    {
        ms_task.set_cmd_counter--;
    }
}



static void msTaskUpdateTiming(void)
{
    // This function is called at 10Hz to work out the task/ISR/msTask
    // timing - the results are put in the diag data structure.

    // Transfer Task and ISR timing

    for (uint16_t task_id = 0; task_id < MCU_NUM_TASKS; task_id++)
    {
        diag.data.r.software[TASK_RUN_TIME][task_id] = os.tsk[task_id].task_run_time;
        diag.data.r.software[ISR_RUN_TIME] [task_id] = os.tsk[task_id].isr_run_time;

        os.tsk[task_id].task_run_time = 0;
        os.tsk[task_id].isr_run_time  = 0;
    }

    // Transfer msTask timing

    for (uint16_t i = 0; i < 20; i++)
    {
        // Copy Max msTask time in us  for previous 100ms

        diag.data.r.ms_duration[i] = ms_task.max_duration[i];
        ms_task.max_duration[i]    = 0;
    }
}



// Actions performed every milisecond

static void msTaskProcess(void)
{
    OS_CPU_SR cpu_sr;

    ledsProcess(ms_task.ms);

    QSPIbusStateMachine();

    msTaskClassProcess();

    // Some converters use the spare interlock digital input as a pulse
    // permit signal to block and unblock the converter.

    pulsePermitProcess();

    msTaskUpdateGwPermit();

    logSpyDimAnaProcess();

    postMortemProcess();

    msTaskUpdateFieldbus();

    // Drive terminal interface

    trmMsOut();

    msTaskUpdateCpuUsage();

    msTaskValidateMsTsks();

    OS_ENTER_CRITICAL();

    OSTskResumeLP(MCU_TSK_EVT);
    OSTskResumeLP(MCU_TSK_FCM);
    OSTskResumeLP(MCU_TSK_TCM);
    OSTskResumeLP(MCU_TSK_TRM);
    OSTskResumeLP(MCU_TSK_PUB);

    OS_EXIT_CRITICAL();

    msTaskUpdateMsDuration();
}



static void msTaskUpdateFieldbus(void)
{
    if (ms_task.ms_mod_20 == 0)
    {
        fbsPrepareStatVar();
    }

    // Millisecond 1 of 20: Send status data

    if (   ms_task.ms_mod_20 == 1
        && fbsIsStandalone() == false)
    {
        fbsWriteStatVar();
    }

    // Millisecond 3 of 20: Resume PubTsk and if new time var received then process time var

    if (ms_task.ms_mod_20 == 3 && fbs.time_rcvd_f == true)
    {
        fbsProcessTimeVar();
    }

    // Millisecond 7 of 20: Check fieldbus

    if (ms_task.ms_mod_20 == 7 && fbsIsStandalone() == false)
    {
        fbsCheck();
    }

    // Millisecond 19 of 20: Validate communication with the gateway

    if (ms_task.ms_mod_20 == 19)
    {
        ethernetWatch();
    }

    // Every Millisecond : Send Rsp/Pub packets
    // Millisecond 15: Clear the time received/time processed flags on the Fieldbus

    // Send only full ethernet packet or flush PUB/RSP stream once every 20ms

    if (   fbs.gw_online_f
        && (   (fbsStreamGetSize(&fbs.cmd) > sizeof(struct fgc_fieldbus_rsp))
            || (fbsStreamGetSize(&fbs.pub) > sizeof(struct fgc_fieldbus_rsp))
            || (ms_task.ms_mod_20 == FGC_ETHER_PUB_FLUSH_MS)
            || (ms_task.ms_mod_20 == FGC_ETHER_RSP_FLUSH_MS)))
    {

        fbsPrepareMsg();
        fbsWriteMsg();
    }

    if (ms_task.ms_mod_20 == 15)
    {
        // For Ethernet, time is received at ms 18/19. For ETH/FIP,
        // EventsProces is called on ms 12, so reset flags at ms 15.

        fbs.time_processed_f = false;
        fbs.time_rcvd_f      = false;
    }
}



static void msTaskUpdateGwPermit(void)
{
    // Sector Access Flag: The flag used by the C32 dpcls.mcu.sector_access is
    // linked to the settable property FGC.SECTOR_ACCESS. If this is disabled
    // by the user when the real sector access flag received from the gateway
    // is set (fbs.sector_access), then the C32 flag must be re-asserted.

    if (fbs.sector_access_gw)
    {
        fbs.sector_access = FGC_CTRL_ENABLED;
    }
}



static void msTaskUpdateCpuUsage(void)
{
    static uint32_t const run_log_inhibit_s = 60 * 60;  // 1 hour
    static uint32_t       next_run_log_entry = 0;

    uint32_t utc_now = ms_task.time_us.secs.abs;

    if (   dpcom.dsp.cpu_usage > 100
        && utc_now             > next_run_log_entry)
    {
        logRunAddTimestamp();
        logRunAddEntry(FGC_RL_DSP_RT, (void *)&dpcom.dsp.cpu_usage);

        next_run_log_entry = utc_now + run_log_inhibit_s;
    }

    // Save maximum DSP usage and MCU usage on millisecond 0

    if (ms_task.ms == 0)
    {
        ms_task.cpu_usage.dsp = dpcom.dsp.cpu_usage;
        ms_task.cpu_usage.mcu = (50000 - diag.data.r.software[TASK_RUN_TIME][MCU_TSK_BGP]) / 500;

        // Clip at 99%

        if (ms_task.cpu_usage.dsp > 99)
        {
            ms_task.cpu_usage.dsp = 99;
        }

        if (ms_task.cpu_usage.mcu > 99)
        {
            ms_task.cpu_usage.mcu = 99;
        }
    }
}



static void msTaskValidateMsTsks(void)
{
    static uint32_t const cpu_mst_freq = 1000;
    static uint32_t       cpu_mst_cnt  = 1000;
    static bool           init_done    = false;

    // Check the MCU and DSP only when the PLL is locked to prevent errors to be signalled during start-up.

    if (   dpcom.mcu.pll.state == FGC_PLL_LOCKED
        && ms_task.ms          == 0)
    {
        init_done = true;
    }

    if (init_done == false)
    {
        return;
    }

    // Every second evaluate the MCU frequency

    if (ms_task.ms == 0)
    {
        if (cpu_mst_cnt != cpu_mst_freq)
        {
            logRunAddTimestamp();
            logRunAddEntry(FGC_RL_MCU_RT, (void *)&cpu_mst_cnt);
        }

        cpu_mst_cnt = 0;
    }

    cpu_mst_cnt++;
}



static void msTaskValidateDspTsks(void)
{
    static uint32_t const cpu_dsp_freq           = 10000;
    static uint32_t       prev_dsp_rtp_alive_cnt = 0;
    static uint32_t       next_run_log_entry[]    = { 0, 0, 0 };
    static uint32_t const run_log_inhibit_s      = 60 * 60;  // 1 hour
    static uint32_t       init_timeout           = 0;

    if (dpcom.mcu.dsp_in_standalone == true)
    {
        return;
    }

    if (   init_timeout        == 0
        && dpcom.mcu.pll.state == FGC_PLL_LOCKED)
    {
        init_timeout = timeGetUtcTime() + 5;
    }

    uint32_t utc_now      = ms_task.time_us.secs.abs;
    uint32_t dsp_run_code = dpcom.dsp.run_code;

    dpcom.dsp.run_code = 0;

    // Evaluate if the DSP is running as expected

    if (dsp_run_code == RUNCODE_DSP_PANIC)
    {
        logRunAddTimestamp();
        logRunAddEntry(FGC_RL_DSP_PANIC, (void *)&dpcom.dsp.panic.panic_code);

        ms_task.dspFltFault = true;
    }

    // Check the MCU and DSP only 5 seconds after the PLL is locked.

    if (init_timeout == 0 || timeGetUtcTime() < init_timeout)
    {
        prev_dsp_rtp_alive_cnt = ms_task.dsp_rtp_alive_cnt;

        return;
    }

    // DSP background task monitored by the MCU

    if (dpcom.dsp_bgp_alive++ > cpu_dsp_freq)
    {
        ms_task.dspFltWarning = true;

        if (utc_now > next_run_log_entry[0])
        {
            logRunAddTimestamp();
            logRunAddEntry(FGC_RL_DSP_BG, NULL);

            next_run_log_entry[0] = utc_now + run_log_inhibit_s;
        }
    }

    // DSP real-time task monitored by the MCU

    uint32_t dsp_rtp_cnt_1s = ms_task.dsp_rtp_alive_cnt -  prev_dsp_rtp_alive_cnt;

    prev_dsp_rtp_alive_cnt = ms_task.dsp_rtp_alive_cnt;

    if (dsp_rtp_cnt_1s != cpu_dsp_freq)
    {
        ms_task.dspFltWarning = true;

        // Use next_run_log_entry[1] if the dsp_rtp_cnt_1s is less than expected
        // and next_run_log_entry[2] if it is greater than expected

        uint16_t next_run_log_entry_index = 1 + (dsp_rtp_cnt_1s > cpu_dsp_freq);

        if (utc_now > next_run_log_entry[next_run_log_entry_index])
        {
            logRunAddTimestamp();
            logRunAddEntry(FGC_RL_DSP_RT, (void *)&dsp_rtp_cnt_1s);

            next_run_log_entry[next_run_log_entry_index] = utc_now + run_log_inhibit_s;
        }
    }

    // Update FGC.DSP_FREQ_KHZ

    ms_task.dsp_freq_khz = (float)dsp_rtp_cnt_1s / 1000.0;
}



static void msTaskUpdateMsDuration()
{
    ms_task.duration = (uint16_t)(UTC_US_P - ms_task.isr_start_us);

    if (ms_task.duration > ms_task.max_duration[ms_task.ms_mod_20])
    {
        ms_task.max_duration[ms_task.ms_mod_20] = ms_task.duration;
    }
}



// ---------- External function definitions

void msTask(void * unused)
{
    uint16_t const num_actions = sizeof(ms_task_actions)/sizeof(struct MsTask_field);

    // Main Loop - synchronised to ms interrupts

    for (;;)
    {
        OSTskSuspend();

        taskTraceReset();

        msTaskIterationStart();

        interFgcStartMs();

        // Time specific actions

        for (uint16_t i = 0; i < num_actions; ++i)
        {
            taskTraceInc();

            if (ms_task.ms % ms_task_actions[i].interval == ms_task_actions[i].ms_of_action)
            {
                ms_task_actions[i].action();
            }
        }
    }
}



void msTaskResetStateTaskWatchdog(void)
{
    ms_task.state_task_watchdog = 0;
}


// EOF
