//!  @file  panic.c
//!  @brief Panic functions
//!
//!  The core dump system allows up to FGC_N_CORE_SEGS segments of memory to be saved to
//!  NVRAM in the event of an unexpected trap (e.g. bus error or illegal instruction).
//!  A core control table in NVRAM defines the memory segments to be saved.


// ---------- Includes

#include <definfo.h>
#include <fgc_runlog_entries.h>
#include <logRun.h>
#include <mcuDependent.h>
#include <mem.h>
#include <memmap_mcu.h>
#include <nvs.h>
#include <os.h>
#include <os_hardware.h>
#include <panic.h>
#include <sharedMemory.h>
#include <sta.h>
#include <string.h>
#include <syscalls.h>



// ---------- External variable definitions

struct Panic_mcu panic_mcu;

struct Panic_core_vars core;



// ---------- External function definitions

void panic(enum Panic_mcu_codes panic_code, uint32_t leds0, uint32_t leds1)
{
    OS_CPU_SR cpu_sr;
    uint32_t  wait_count;
    uint32_t  crash_code;

    panic_mcu.code = (int32_t)panic_code;
    logRunAddTimestamp();

    // Panic code

    logRunAddEntry(FGC_RL_MCU_PANIC, &panic_mcu.code);

    if (panic_mcu.code == MCU_PANIC_SW_CRASH)
    {
        crash_code = sta.crash_code;

        // Crash code

        logRunAddEntry(FGC_RL_MCU_PANIC, &crash_code);
    }

    if (panic_mcu.sp)
    {
        // Stack Pointer (for exceptions)

        logRunAddEntry(FGC_RL_SKSP, &panic_mcu.sp);

        // Program Counter before the exception

        logRunAddEntry(FGC_RL_PKPC, &panic_mcu.pc);
    }

    if (OS_TID_CODE < 16)
    {
        // NanOS Task ID

        logRunAddEntry(FGC_RL_TSK_ID, (void *) & (OS_TID_CODE));
    }

    if (OS_ISR_MASK != 0)
    {
        // NanOS ISR ID

        logRunAddEntry(FGC_RL_ISR_ID, (void *) & (OS_ISR_MASK));
    }

    if (   panic_mcu.code >= MCU_PANIC_SYSCALL_MALLOC_SIZE_R
        && panic_mcu.code <= MCU_PANIC_SYSCALL_REALLOC_R)
    {
        logRunAddEntry(FGC_RL_MCU_PANIC, &malloc_mngr_dbg.panic_data.requested_size);
        logRunAddEntry(FGC_RL_MCU_PANIC, &malloc_mngr_dbg.panic_data.task_id);

        uint32_t allocated;

        allocated = malloc_mngr_ctrl.free_idx[0];
        logRunAddEntry(FGC_RL_MCU_PANIC, &allocated);

        allocated = malloc_mngr_ctrl.free_idx[1];
        logRunAddEntry(FGC_RL_MCU_PANIC, &allocated);

        allocated = malloc_mngr_ctrl.free_idx[2];
        logRunAddEntry(FGC_RL_MCU_PANIC, &allocated);

        allocated = malloc_mngr_ctrl.free_idx[3];
        logRunAddEntry(FGC_RL_MCU_PANIC, &allocated);
    }

    // Core dump: See FGC.DEBUG.CORE_LEN_W/CORE_ADDR/CORE_DATA

    panicCoreDump();


    // --- Stop interrupts and halt TMS320C6727 and M16C62 ---

    // Disable ALL interrupts

    DISABLE_INTERRUPTS();

    // OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running

    OS_ENTER_CRITICAL_7();
    CPU_RESET_CTRL_P |= (CPU_RESET_CTRL_C62OFF_MASK16 | CPU_RESET_CTRL_DSP_MASK16);


    // --- Flash front panel LEDs if possible and wait for a reset ---

    // Loop while waiting for watchdog

    for (;;)
    {
        // Set 1st pattern (long)

        RGLEDS_P = leds0;

        // Cannot use USLEEP or MSLEEP because interrupts are disabled

        wait_count = 0x00F00000;

        while (wait_count > 0)
        {
            wait_count--;
        }


        // Set 2nd pattern (short)

        RGLEDS_P   = leds1;
        wait_count = 0x00100000;

        while (wait_count > 0)
        {
            wait_count--;
        }
    }

    // Never returns
}



void panicCrash(uint32_t crash_code)
{
    sta.crash_code = crash_code;

    panic(MCU_PANIC_SW_CRASH,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16  | RGLEDS_PIC_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16 | RGLEDS_DCCT_RED_MASK16);
}



void panicCoreDump(void)
{
    uint16_t               ii;
    struct Panic_core_ctrl core_ctrl;

    // Get Core control table from NVRAM

    panicCoreTableGet(&core_ctrl);

    nvsUnlock();

    // Write each memory segment to NVRAM

    for (ii = 0; ii < FGC_N_CORE_SEGS; ii++)
    {
        if (core_ctrl.seg[ii].n_words > 0)
        {
            memcpy((void *) core_ctrl.addr_in_core[ii], (void *) core_ctrl.seg[ii].addr,
                   core_ctrl.seg[ii].n_words * sizeof(uint16_t));
        }
    }

    nvsLock();
}



void panicCoreTableGet(struct Panic_core_ctrl * core_ctrl)
{
    uint16_t    ii;
    uint32_t    total_length_words = 0;

    // Get CORE control table from NVRAM

    memcpy(core_ctrl, (void *) NVRAM_CORECTRL_32, sizeof(struct Panic_core_ctrl));

    // Check that table has valid lengths

    for (ii = 0; ii < FGC_N_CORE_SEGS; ii++)
    {
        if (   core_ctrl->seg[ii].pad != 0xFFFF
            || (   core_ctrl->seg[ii].n_words > 0
                && (   core_ctrl->addr_in_core[ii] <  NVRAM_COREDATA_32
                    || core_ctrl->addr_in_core[ii] > (NVRAM_COREDATA_32 + 2 * NVRAM_COREDATA_W))))
        {
            break;
        }

        total_length_words += core_ctrl->seg[ii].n_words;
    }

    if (   ii < FGC_N_CORE_SEGS
        || total_length_words > NVRAM_COREDATA_W)
    {
        // CORE ctrl table is not valid

        // Clear table

        memset(core_ctrl, 0, sizeof(struct Panic_core_ctrl));

        // Set up default table with one max length segment from start of SRAM in page 0

        core_ctrl->seg[0].n_words  = NVRAM_COREDATA_W;
        core_ctrl->seg[0].addr     = (uint32_t) &shared_mem;
        core_ctrl->seg[1].addr     = (uint32_t) &shared_mem;
        core_ctrl->seg[2].addr     = (uint32_t) &shared_mem;
        core_ctrl->seg[3].addr     = (uint32_t) &shared_mem;

        // Padding words

        core_ctrl->seg[0].pad      = 0xFFFF;
        core_ctrl->seg[1].pad      = 0xFFFF;
        core_ctrl->seg[2].pad      = 0xFFFF;
        core_ctrl->seg[3].pad      = 0xFFFF;

        core_ctrl->addr_in_core[0] = NVRAM_COREDATA_32;

        panicCoreTableSet(core_ctrl);
    }
}



void panicCoreTableSet(struct Panic_core_ctrl * core_ctrl)
{
    nvsUnlock();

    // Set CORE control table in NVRAM

    memcpy((void *) NVRAM_CORECTRL_32, core_ctrl, sizeof(struct Panic_core_ctrl));

    // Clear CORE data area

    MemSetWords((void *) NVRAM_COREDATA_32, 0x0000, NVRAM_COREDATA_W);

    nvsLock();
}


// EOF

