//! @file   device.c
//! @brief  Functions related to device initialisation


// ---------- Includes

#include <bitmap.h>
#include <config.h>
#include <crate.h>
#include <databases.h>
#include <device.h>
#include <fgc_code.h>
#include <init.h>
#include <prop.h>



// ---------- Internal structures, unions and enumerations

struct Device_nameDb_info
{
	uint16_t    class_id;                   // [2]  Device class
    uint16_t    omode_mask;                 // [2]  Operational mode mask
    char        name[FGC_MAX_DEV_LEN + 1];  // [24] Device name
};



// ---------- Internal variable definitions

static struct Device_nameDb_info device_nameDb;

// static_assert(ArrayLen(device_nameDb.name) == ArrayLen(device.name), "");



// ---------- External variable definitions

struct Device __attribute__((section("sram"))) device = { .spare_id = 0 };



// ---------- Internal function definitions

static void deviceSetProperties(void)
{
    PropSet(&tcm, &PROP_FGC_NAME,    device_nameDb.name,  strlen(device_nameDb.name));
    PropSet(&tcm, &PROP_DEVICE_TYPE, device.sys.sys_type, strlen(device.sys.sys_type));

    // Set DEVICE.PLATFORM_NM and DEVICE.CLASS_NM properties

    strncpy(device.class_name,    FGC_CLASS_NAME,    CLASS_NAME_LEN    - 1)[CLASS_NAME_LEN    - 1] = '\0';
    strncpy(device.platform_name, FGC_PLATFORM_NAME, PLATFORM_NAME_LEN - 1)[PLATFORM_NAME_LEN - 1] = '\0';

    // Set DEVICE.PLATFORM_ID property

    device.platform_id = FGC_PLATFORM_ID;
}



static void deviceSetName(char const * const name)
{
    strncpy(device_nameDb.name, name, FGC_MAX_DEV_LEN);
    device_nameDb.name[FGC_MAX_DEV_LEN] = '\0';
}



static void deviceSetSpareName()
{
    if (device.spare_id == 0)
    {
        memset(device.spare_name, '\0', FGC_MAX_DEV_LEN + 1);
    }
    else
    {
        strcpy (device.spare_name, device_nameDb.name);
        sprintf(device.spare_name, "%s_%02d", device.spare_name, device.spare_id);
    }
}



static void deviceSetHostname(char const * const hostname)
{
    strncpy(fbs.host_name, hostname, FGC_MAX_DEV_LEN);
    fbs.host_name[FGC_MAX_DEV_LEN] = '\0';
}



static char const * deviceSanitizeName(char const * const name)
{
    if (name == NULL || name[0] == '\0')
    {
        return "UNKNOWN";
    }

    return name;
}



static void deviceInitName(void)
{
    if (fbsIsStandalone())
    {
        // Standalone mode will be flagged by DEVICE.NAME set to "STANDALONE"
        // and DEVICE.HOSTNAME set to "NONE"

        deviceSetHostname("NONE");
        deviceSetName    ("STANDALONE");

        // Clear Last Calibration time if not connected to the Gateway
        // TODO This doesn't belong here

        cal.last_cal_time_unix = 0;

        configSetState(FGC_CFG_STATE_STANDALONE, true);
        configSetMode(FGC_CFG_MODE_SYNC_NONE);
        initPostDb();
    }
    else
    {
        // Set DEVICE.NAME and DEVICE.HOSTNAME from NameDB
        // Hostname will always be stored in the first record of the database (index 0)
        // Device name should be taken from the record matching its fieldbus id

        deviceSetHostname (deviceSanitizeName(nameDbGetName(0     )));
        deviceSetName     (deviceSanitizeName(nameDbGetName(fbs.id)));
        deviceSetSpareName();

        device_nameDb.class_id   = nameDbGetClassId(fbs.id);
        device_nameDb.omode_mask = nameDbGetOmodeMask(fbs.id);

        // Take omode_mask from NameDB record

        device.class_id   = device_nameDb.class_id;
        device.omode_mask = device_nameDb.omode_mask;

        pll.no_ext_sync = testBitmap(device.omode_mask, FGC_LHC_SECTORS_NO_EXT_SYNC);

        configSetManagerAvailability();

        if (configManagerNotAvailable() == true)
        {
            configSetState(FGC_CFG_STATE_STANDALONE, true);
            configSetMode(FGC_CFG_MODE_SYNC_NONE);
            initPostDb();
        }
    }
}


static void deviceCopyType(char * device_type, char const * device_name)
{
    uint8_t i;

    for (i = 0; *device_name != '.' && i < FGC_SYSDB_SYS_LEN; i++)
    {
        *device_type++ = *device_name++;
    }

    *device_type = '\0';
}



static fgc_db_t deviceFindTypeInSysDb(char const * type)
{
    fgc_db_t const sys_max = SysDbLength();
    fgc_db_t       sys_idx = 1;

    // Start from 1, because record 0 is an unknown system

    while ((strncmp(SysDbType(sys_idx), type, FGC_SYSDB_SYS_LEN) != 0) && ++sys_idx < sys_max)
    {
        ;
    }

    return sys_idx < sys_max ? sys_idx : 0;
}



static void deviceInitType(void)
{
    // Record index 0 is device type: 'UKNWN'

    char     sys_type[FGC_SYSDB_SYS_LEN + 1];
    fgc_db_t sys_idx;

    // Initialize the device type from one of these sources in this priority:
    //
    //   1) From the device name
    //   2) From the property DEVICE.TYPE
    //   3) From the device spare name
    //   4) Defaulting to UKNWN

    // DEVICE.SPARE_NAME

    if (device.spare_id != 0)
    {
        deviceCopyType(sys_type, deviceSanitizeName(nameDbGetName(device.spare_id)));

        sys_idx = deviceFindTypeInSysDb(sys_type);
    }
    else
    {
        // DEVICE.NAME

        deviceCopyType(sys_type, device_nameDb.name);

        if ((sys_idx = deviceFindTypeInSysDb(sys_type)) == 0)
        {
            // DEVICE.TYPE

            strncpy(sys_type, device.type, FGC_SYSDB_SYS_LEN + 1);

            sys_idx = deviceFindTypeInSysDb(sys_type);
        }
    }

    device.sys.sys_idx = sys_idx;

    strncpy(device.sys.sys_type, sys_type, FGC_SYSDB_SYS_LEN);
}



// ---------- External function definitions

void deviceInit(void)
{
    deviceInitName();

    deviceInitType();

    deviceSetProperties();

    if (strncmp("RPOPB", device.type, FGC_SYSDB_SYS_LEN) == 0)
    {
        crateSetPopsB();
        crateSetActuationDest(CRATE_ACTUATION_DEST_NONE);
    }

    if (configIsStandalone() == true || configManagerNotAvailable() == true)
    {
        regFgc3ParsSendParamsToHw();
    }
}



bool deviceNameIsValid(void)
{
    return device.name[0] != '*' ? true : false;
}



char * deviceGetName(void)
{
    return device.name;
}



uint16_t deviceGetOmodeMask(void)
{
    return device.omode_mask;
}



void deviceUpdateCmdCounter(void)
{
    device.cmds_received++;
}


// EOF
