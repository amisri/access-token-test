//! @file  databases.c
//! @brief Interface to the databases encapsulated in codes


// ---------- Includes

#include <string.h>

#include <databases.h>
#include <fgc_code.h>
#include <fgc/fgc_db.h>
#include <memmap_mcu.h>



// ---------- Constants

#define SYSDB     ( (struct fgc_sysdb * const)   CODES_SYSDB_SYSDB_32 )
#define COMPDB    ( (struct fgc_compdb * const)  CODES_COMPDB_COMPDB_32 )
#define DIMDB     ( (void *)                     CODES_DIMDB_DIMDB_32 )    	// DIMDB now points to the beginning of the DimDb,
																		  	// not to the DimDb types

static struct fgc_name_db const * const name_db          = (const struct fgc_name_db *)CODES_NAMEDB_32;
static bool name_db_is_valid = false;



static void nameDbInit(void)
{
	unsigned i;

	// Make sure that all device names are null-terminated

	for(i = 0; i < FGC_CODE_NAMEDB_MAX_DEVS; i++)
	{
		if(name_db->dev[i].name[FGC_MAX_DEV_LEN] != '\0')
		{
			return;
		}
	}

	// Make sure that all sub-device names are null-terminated

	for(i = 0; i < FGC_CODE_NAMEDB_MAX_SUBDEVS; i++)
	{
		if(name_db->sub_dev[i].name[FGC_MAX_DEV_LEN] != '\0')
		{
			return;
		}
	}

	name_db_is_valid = true;
}



// ---------- External function definitions

void databasesInit(void)
{
    SysDbInit(SYSDB);
    CompDbInit(COMPDB);
    DimDbInit(DIMDB);

    nameDbInit();
}


bool databasesOk(void)
{
	return (   SysDbVersion() == CompDbVersion()
      	    && SysDbVersion() == DimDbVersion());
}



uint16_t nameDbGetClassId(uint16_t index)
{
	return name_db->dev[index].class_id;
}



uint16_t nameDbGetOmodeMask(uint16_t index)
{
	return name_db->dev[index].omode_mask;
}



int16_t nameDbGetIndex(char const * const dev_name)
{
	if(name_db_is_valid == false)
	{
		return -1;
	}

    const struct fgc_dev_name * devs = name_db->dev;

    unsigned i;

    for(i = 0; i < FGC_CODE_NAMEDB_MAX_DEVS; i++)
    {
        if(strcmp(dev_name, devs[i].name) == 0)
        {
            return i;
        }
    }

    return -1;
}



char const * nameDbGetName(uint16_t const index)
{
    if(name_db_is_valid == false || index >= FGC_CODE_NAMEDB_MAX_DEVS)
    {
        return NULL;
    }

    return name_db->dev[index].name;
}


// EOF
