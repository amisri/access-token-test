//! @file   config.c
//! @brief  Functions relating to configuration properties


// ---------- Includes

#include <bitmap.h>
#include <config.h>
#include <defconst.h>
#include <device.h>



// ---------- External variable definitions

struct Config config;



// ---------- External function definitions

bool configIsStandalone(void)
{
    return config.state == FGC_CFG_STATE_STANDALONE ? true : false;
}



void configSetManagerAvailability(void)
{
    config.no_manager = testBitmap(deviceGetOmodeMask(), FGC_LHC_SECTORS_NO_CONFIG_MGR) == true ? true : false;
}



bool configManagerNotAvailable(void)
{
    return config.no_manager;
}



void configSetMode(uint16_t mode)
{
    config.mode = mode;
}



uint16_t configGetMode(void)
{
    return config.mode;
}



void configSetState(uint16_t state, bool force_state)
{
    if (   config.state != FGC_CFG_STATE_STANDALONE
        || force_state  == true)
    {
        config.state = state;
    }
}



uint16_t configGetState(void)
{
    return config.state;
}


// EOF
