//! @file  trap.c
//! @brief Trap related interrupt functions


// ---------- Includes

#include <bitmap.h>
#include <digitalIo.h>
#include <iodefines.h>
#include <logRun.h>
#include <memmap_mcu.h>
#include <mcu.h>
#include <nvs.h>
#include <os_hardware.h>
#include <panic.h>
#include <sharedMemory.h>
#include <trap.h>



// ---------- Internal structures, unions and enumerations

struct Trap_data
{
    uint16_t isrdummy_count;
    uint16_t isrdummy_fix_count;
    uint16_t isrdummy_empty_count;
};



// Internal variable declarations

static struct Trap_data trap;



// ---------- Internal function declarations

static void trapIsr(void);
static void trapPanicFGC3(uint32_t leds0, uint32_t leds1);



// ---------- External function definitions

void IsrDummy(void)
{
    OS_CPU_SR  cpu_sr;

    trap.isrdummy_count++;

    // Disable ALL interrupts

    DISABLE_INTERRUPTS();

    // OS_ENTER_CRITICAL_7(), because OS_ENTER_CRITICAL() leaves the timers running

    OS_ENTER_CRITICAL_7();

    // Safe mode & reg savings

    trapIsr();

    // flash red led

    trapPanicFGC3(RGLEDS_PIC_RED_MASK16, 0);

    // never returns
}



void IsrDummyFix(void)
{
    trap.isrdummy_fix_count++;
}



void IsrDummyEmpty(void)
{
    trap.isrdummy_empty_count++;
}



void IsrUndefinedInstruction(void)
{
    OS_CPU_SR  cpu_sr;

    // Disable ALL interrupts

    DISABLE_INTERRUPTS();

    // OS_ENTER_CRITICAL_7(), because OS_ENTER_CRITICAL() leaves the timers running

    OS_ENTER_CRITICAL_7();

    // Safe mode & reg savings

    trapIsr();

    // flash red : FIP & FGC

    trapPanicFGC3(RGLEDS_NET_RED_MASK16 | RGLEDS_FGC_RED_MASK16, 0);

    // never returns
}



void IsrOverflow(void)
{
    OS_CPU_SR  cpu_sr;

    // Disable ALL interrupts

    DISABLE_INTERRUPTS();

    // OS_ENTER_CRITICAL_7(), because OS_ENTER_CRITICAL() leaves the timers running

    OS_ENTER_CRITICAL_7();

    // Safe mode & reg savings

    trapIsr();

    // flash red FIP,FGC & PSU

    trapPanicFGC3(RGLEDS_NET_RED_MASK16 | RGLEDS_FGC_RED_MASK16 | RGLEDS_PSU_RED_MASK16, 0);

    // never returns
}



void IsrWatchdogTimer(void)
{
    OS_CPU_SR  cpu_sr;

    // Disable ALL interrupts

    DISABLE_INTERRUPTS();

    // OS_ENTER_CRITICAL_7(), because OS_ENTER_CRITICAL() leaves the timers running

    OS_ENTER_CRITICAL_7();

    // Safe mode & reg savings

    trapIsr();

    // flash red FIP,FGC,PSU & VS

    trapPanicFGC3(RGLEDS_NET_RED_MASK16 | RGLEDS_FGC_RED_MASK16 | RGLEDS_PSU_RED_MASK16 | RGLEDS_VS_RED_MASK16, 0);

    // never returns
}



// ---------- Internal function definitions

// trapIsr will be called for all unexpected interrupts. The interrupt jump function will set the
// exception ID in register D so that it can be stored along with all the other registers in the runlog.

static void trapIsr(void)
{
    // Disable interrupts

    // in boot there is a function DisableNetworkInterrupt(), Disable1msTickInterrupt()

    // __asm__ volatile ( "clrpsw I" ); // clear I flag in PSW

    //  IRQ2 - vector 66 - FIP_~MSG_IRQ - INT_Excep_IRQ2()
    ICU.IER08.BIT.IEN2 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR22.BIT.IPR_20 = 0x00;    // Interrupt Priority Register

    //  IRQ3 - vector 67 - FIP_~TIME_IRQ or ETHERNET 50Hz pulse - INT_Excep_IRQ3()
    //  ICU.IER08.BIT.IEN3 = 0;         // Interrupt Request Enable Register
    //  ICU.IPR23.BIT.IPR_20 = 0x00;    // Interrupt Priority Register

    //  IRQ4 - vector 68 - MCU_~TICK - INT_Excep_IRQ4()
    ICU.IER08.BIT.IEN4 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR24.BIT.IPR_20 = 0x00;    // Interrupt Priority Register


    //  TMR0 CMIA0 - Compare Match A Interrupt - vector 174 - INT_Excep_TMR0_CMIA0()
    //  TMR0 it is programmed to generate an interrupt every 1ms
    ICU.IER15.BIT.IEN6 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR68.BIT.IPR_20 = 0x00;    // Interrupt Priority Register


    //  TMR3 OVI3 - Timer Overflow Interrupt - vector 185 - INT_Excep_TMR3_OVI3()
    //  TMR3 will count [0..255] incremented by TMR2 count match (1us) and generating an interrupt when overflows
    ICU.IER17.BIT.IEN1 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR6B.BIT.IPR_20 = 0x00;    // Interrupt Priority Register



    // Reset peripherals

    digitalIoDisableOutputs();

    // FIP/ETHERNET reset is direct from M32C87, set FIP in reset

    P9.DR.BIT.B7 = 0;

    setBitmap(CPU_RESET_CTRL_P, CPU_RESET_CTRL_C62OFF_MASK16);      // Turn M16C62 off
    setBitmap(CPU_RESET_CTRL_P, CPU_RESET_CTRL_DSP_MASK16);         // Reset TMS320C6727
    setBitmap(CPU_RESET_CTRL_P, CPU_RESET_CTRL_ANALOG_MASK16);      // Reset Analog interface

    mcuStopDerivedClocks();    // Dsp, Adc, Dac

    setBitmap(CPU_RESET_CTRL_P, CPU_RESET_CTRL_DIGITAL_MASK16);     // Reset Digital interface
    setBitmap(CPU_RESET_CTRL_P, CPU_RESET_CTRL_QSM_MASK16);         // Reset diagnostics interface
    nvsLock();

    // ToDo: Set temp regul heater to 0


    clrBitmap(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_CTRL_MASK16);      // SUP_A as input
    clrBitmap(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_PERMIT_MASK16);    // output disabled
    clrBitmap(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_CTRL_MASK16);      // SUP_B as input
    clrBitmap(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_PERMIT_MASK16);    // output disabled

    logRunInitIfNotReady();

    // Write trap dump to run log

    logRunAddEntry(FGC_RL_EXID,    (void *)TRAP_EXID_32);           // Exception ID
    logRunAddEntry(FGC_RL_PWRTIME, (void *) & (shared_mem.power_time));
    logRunAddEntry(FGC_RL_RUNTIME, (void *) & (shared_mem.run_time));
    logRunAddTimestamp();

    // Write conditional run log records

    if (OS_TID_CODE < 16)
    {
        // NanOS Task ID

        logRunAddEntry(FGC_RL_TSK_ID, (void *) & (OS_TID_CODE));
    }

    if (OS_ISR_MASK != 0)
    {
        // NanOS ISR ID

        logRunAddEntry(FGC_RL_ISR_ID, (void *) & (OS_ISR_MASK));
    }

    panicCoreDump();
}



// This function will try to use the front panel LEDs to flash a code that will indicate the reason for
// the panic.  The function never exits - it just waits for a watchdog or human to reset the system.
// It is not possible to use MSLEEP() as it needs the interrupts.

static void trapPanicFGC3(uint32_t leds0, uint32_t leds1)
{
    OS_CPU_SR   cpu_sr;
    uint32_t      wait_count;

    // Stop interrupts and halt TMS320C6727 and M16C62

    DISABLE_INTERRUPTS();
    OS_ENTER_CRITICAL_7();
    setBitmap(CPU_RESET_CTRL_P, CPU_RESET_CTRL_C62OFF_MASK16 | CPU_RESET_CTRL_DSP_MASK16);

    // Flash front panel LEDs if possible and wait for a reset
    // Loop while waiting for watchdog

    for (;;)
    {
        // Set first pattern

        RGLEDS_P   = leds0;
        wait_count = 0xFF000000;

        while (wait_count > 0)
        {
            wait_count--;
        }

        // Set second pattern

        RGLEDS_P   = leds1;
        wait_count = 0xFF000000;

        while (wait_count > 0)
        {
            wait_count--;
        }
    }
}


// EOF
