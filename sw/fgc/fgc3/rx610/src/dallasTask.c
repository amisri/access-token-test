//! @file    dallasTask.c
//! @brief   Dallas bus functions
//!
//!  Notes:   This file contains the function for the Dallas Task and all the functions related to
//!           reading the IDs and temperatures from the Dallas devices controlled by the C62 microcontroller.


// ---------- Includes

#include <stdlib.h>
#include <string.h>

#include <bitmap.h>
#include <config.h>
#include <dallasCom.h>
#include <dallasTask.h>
#include <device.h>
#include <dpcls.h>
#include <fbs_class.h>
#include <fbs.h>
#include <fgc/fgc_db.h>
#include <logSpy.h>
#include <memmap_mcu.h>
#include <msTask.h>
#include <os.h>
#include <prop.h>
#include <regfgc3Prog.h>
#include <sharedMemory.h>
#include <status.h>
#include <taskTrace.h>
#include <time_fgc.h>



// ---------- Constants -------------------------------------------------

#define DALLAS_STATE_REF_MAX_TRANSITIONS        3     // Maximum transitions from any state

#define DALLAS_TASK_READID_TIMEOUT              20    // DALLAS_TASK_CMD_READ_IDS command timeout used by MCU (s)
#define DALLAS_TASK_READTEMP_TIMEOUT            2     // DALLAS_TASK_CMD_READ_TEMPS_BRANCH command timeout used by MCU (s)
#define DALLAS_TASK_MAX_EXPECTED_RESPONSE       12    // Buffer to hold slow (background) command error responses
#define DALLAS_TASK_UNDEF_LOGICAL_PATH          66
#define DALLAS_TASK_EXTERNAL_ID_MAX             6     // Maximum number of external IDs that can be sent to microcontroller



// ---------- Internal structures, unions and enumerations --------------

enum DallasTask_states
{
    DALLAS_TASK_STATE_START,
    DALLAS_TASK_STATE_AT_MAINPROG,
    DALLAS_TASK_STATE_READID,
    DALLAS_TASK_STATE_BARCODE,
    DALLAS_TASK_STATE_TEMPA,
    DALLAS_TASK_STATE_TEMPB,
    DALLAS_TASK_STATE_TEMPL,
    DALLAS_TASK_STATE_LOGTEMPS,
    DALLAS_TASK_STATE_OFF,
};

//! Type for state machine state function returning state number

typedef uint32_t DallasTaskState(bool first_call);

//! Type for pointer to transition function returning pointer to state function

typedef DallasTaskState * DallasTaskStateTrans(void);

//! Internal local variables for the dallas state machine

struct DallasTask_state_local
{
    DallasTaskState         * stateFunc;       //!< Pointer to the active state function
    uint16_t                  status;
    enum DallasTask_states    state;
};


struct DallasTask_device
{
    struct TMicroLAN_element_summary   device_info;
    struct prop                      * prop_temp;
    uint16_t                           dev_index;       // For dallasComDeviceIsrCmd(). Represents order of insertion in the list.
    uint16_t                           compDB_index;    // 0 = unknown
    struct DallasTask_device         * next;
};


struct DallasTask_branch
{
    struct TBranchSummary       branch_info;
    struct DallasTask_device  * branch_devs;
};


struct DallasTask
{
    struct DallasTask_branch           branches[FGC_ID_N_BRANCHES];
    struct DallasTask_device           all_devs[MICROLAN_NETWORK_ALL_ELEMENTS_MAX];
    union TMicroLAN_unique_serial_code external_ids[DALLAS_TASK_EXTERNAL_ID_MAX];
    uint8_t                            external_id_num;
    uint16_t                           n_devs;
    uint16_t                           timeout_cnt;    //!< Command timeout down counter
    bool                               barcodes_ready; //!< True when the config manager has set the barcoes
    bool                               active;         //!< Dallas buses read flag
    bool                               temp_fail;      //!< Dallas temperature read failed
    bool volatile                      regfgc3_boards_ready; //!< Set by regFGC3 task when all the baords are in production boot
};



// ---------- External variable definitions

struct DallasTask_vars          __attribute__((section("sram"))) dls;
struct DallasTask_temperatures  __attribute__((section("sram"))) temp;
struct DallasTask_barcode_vars  __attribute__((section("sram"))) barcode;
struct DallasTask_barcode_n_els __attribute__((section("sram"))) barcode_n_els;



// ---------- Internal variable definitions -----------------------------

static struct DallasTask_state_local  __attribute__((section("sram"))) dallas_state;
static struct DallasTask              __attribute__((section("sram"))) dallas_task;

CC_STATIC_ASSERT(DALLAS_TASK_STATE_START       ==  0, Unexpected_DALLAS_STATE_constant_value);
CC_STATIC_ASSERT(DALLAS_TASK_STATE_AT_MAINPROG ==  1, Unexpected_DALLAS_STATE_constant_value);
CC_STATIC_ASSERT(DALLAS_TASK_STATE_READID      ==  2, Unexpected_DALLAS_STATE_constant_value);
CC_STATIC_ASSERT(DALLAS_TASK_STATE_BARCODE     ==  3, Unexpected_DALLAS_STATE_constant_value);
CC_STATIC_ASSERT(DALLAS_TASK_STATE_TEMPA       ==  4, Unexpected_DALLAS_STATE_constant_value);
CC_STATIC_ASSERT(DALLAS_TASK_STATE_TEMPB       ==  5, Unexpected_DALLAS_STATE_constant_value);
CC_STATIC_ASSERT(DALLAS_TASK_STATE_TEMPL       ==  6, Unexpected_DALLAS_STATE_constant_value);
CC_STATIC_ASSERT(DALLAS_TASK_STATE_LOGTEMPS    ==  7, Unexpected_DALLAS_STATE_constant_value);
CC_STATIC_ASSERT(DALLAS_TASK_STATE_OFF         ==  8, Unexpected_DALLAS_STATE_constant_value);



// ---------- Internal function declarations ----------------------------

static void dallasTaskGetExtIds(void);


static void dallasTaskRspError(struct prop * p, uint16_t errloc, uint16_t errnum, uint16_t rsplen, uint16_t logical_path);


static uint16_t dallasTaskReadIds(void);


static uint16_t dallasTaskReadRsp(uint8_t * buf, uint16_t * max_bytes);


static void dallasTaskInsertDevice(uint16_t logical_path, struct DallasTask_device * device, bool ext_id, uint16_t device_idx);


static void dallasTaskIdentifyAndCountDevices(void);


static void dallasTaskCheckGroups(void);


static void dallasTaskLinkPropertiesAndTemperatures(void);


static void dallasTaskBarcodeBus(struct prop * p, uint16_t logical_path);


static void dallasTaskSetPropertyChan(uint16_t logical_path, char * barcode_buf);


static void dallasTaskPropTemp(uint16_t logical_path);


static void dallasTaskResetBarcodes(struct prop * p);


static uint16_t dallasTaskConvTemp(uint16_t logical_path);


static uint16_t dallasTaskReadTemp(struct prop * p, uint16_t logical_path);


static void dallasTaskLogTemp(void);



// -------------------- STATE FUNCTIONS --------------------

// STATE: STARTING (ST)

static uint32_t dallasStateST(bool firstCall)
{
    uint16_t errnum;
    uint8_t  rx;

    // If microcontroller boot did not respond

    if (dallasComBoot() == false)
    {
        dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, DALLAS_TASK_ERROR_NO_RSP, 0, DALLAS_TASK_UNDEF_LOGICAL_PATH);
        dallas_state.status = DALLAS_TASK_ERROR_POWER_OFF;
    }
    else
    {
        errnum = dallasComSendCmd(DALLAS_TASK_CMD_SWITCH_PROG, &rx);

        // If request to switch to main prog fails


        if (errnum != DALLAS_TASK_ERROR_OK)
        {
            dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, errnum, 0, DALLAS_TASK_UNDEF_LOGICAL_PATH);
            dallas_state.status = DALLAS_TASK_ERROR_POWER_OFF;
        }
        else
        {
            dallas_state.status = DALLAS_TASK_ERROR_OK;
        }
    }

    return DALLAS_TASK_STATE_START;
}



// STATE: AT_MAINPROG (MP)

static uint32_t dallasStateMP(bool firstCall)
{
    uint16_t errnum;
    uint8_t  rx;

    // If microcontroller main program has not started

    if (dallasComTestCmdBusy() == true)
    {
        dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__ , DALLAS_TASK_ERROR_NO_RSP, 0, DALLAS_TASK_UNDEF_LOGICAL_PATH);
        dallas_state.status = DALLAS_TASK_ERROR_POWER_OFF;
    }
    else if (dallasComBoot() == false)
    {
        // If microcontroller boot did not respond

        dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, DALLAS_TASK_ERROR_NO_RSP, 0, DALLAS_TASK_UNDEF_LOGICAL_PATH);
        dallas_state.status = DALLAS_TASK_ERROR_POWER_OFF;
    }
    else if (dallas_task.active == false)
    {
        // Read IDs only if IDs not read yet (once at start-up)

        errnum = dallasComSendCmd(DALLAS_TASK_CMD_READ_IDS, &rx);

        if (errnum != DALLAS_TASK_ERROR_OK)
        {
            dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, errnum, 0, DALLAS_TASK_UNDEF_LOGICAL_PATH);
            dallas_state.status = DALLAS_TASK_ERROR_POWER_OFF;
        }
        else
        {
            // Set timeout in 100ms units

            dallas_task.timeout_cnt = DALLAS_TASK_READID_TIMEOUT * 10;

            dallas_state.status = DALLAS_TASK_ERROR_OK;
        }
    }
    else
    {
        dallas_state.status = DALLAS_TASK_ERROR_OK;
    }

    return DALLAS_TASK_STATE_AT_MAINPROG;
}



// STATE: READID (ID)

static uint32_t dallasStateID(bool firstCall)
{
    // Get IDs and barcodes (once at start-up)

    if (firstCall == true || dallas_state.status != DALLAS_TASK_ERROR_OK)
    {
        dallas_state.status = dallasTaskReadIds();
    }

    return DALLAS_TASK_STATE_READID;
}



// STATE: BARCODES (BC)

static uint32_t dallasStateBC(bool firstCall)
{
    if (firstCall == true)
    {
        dallasTaskGetExtIds();

       // Initialize the Barcodes to unknown_ID123_bytes

        uint16_t logical_path;
        uint16_t id;

        for (logical_path = 0; logical_path < FGC_ID_N_BRANCHES; logical_path++)
        {
            uint16_t num_ids = dallas_task.branches[logical_path].branch_info.total_devices;

            for (id = 0; id < num_ids; id++)
            {
                struct TMicroLAN_element_summary * element = dallasTaskGetDeviceInfo(logical_path, id);

                strncpy(element->barcode.c, "unknown_ID123_bytes", FGC_ELEMENT_BARCODE_LEN);
            }
        }

        if (configGetState() != FGC_CFG_STATE_STANDALONE)
        {
            // Signal the config manager to set the Barcodes

            statusSetUnlatched(FGC_UNL_SYNC_BARCODE);

            // Temporary error

            dallas_state.status = DALLAS_TASK_ERROR_WAIT_BC;
        }
        else
        {
            dallas_state.status = DALLAS_TASK_ERROR_OK;
        }
    }

    if (dallas_task.barcodes_ready == true)
    {
        // Identify and count the number of components found

        dallasTaskIdentifyAndCountDevices();

        // Check found-per-group count against expected-per-group for the given system

        dallasTaskCheckGroups();

        dallasTaskLinkPropertiesAndTemperatures();

        dallas_state.status = DALLAS_TASK_ERROR_OK;

        dallas_task.barcodes_ready = false;
    }

    return DALLAS_TASK_STATE_BARCODE;
}



// STATE: TEMPA (TA)

static uint32_t dallasStateTA(bool firstCall)
{
    if (firstCall == true)
    {
        // Convert Temperature

        dallas_state.status = dallasTaskConvTemp(ID_LOGICAL_PATH_FOR_MEAS_A);
    }
    else
    {
        // Read Temperature

        dallas_state.status = dallasTaskReadTemp(&PROP_FGC_DLS_MEAS_A, ID_LOGICAL_PATH_FOR_MEAS_A);
    }

    return DALLAS_TASK_STATE_TEMPA;
}



// STATE: TEMPB (TB)

static uint32_t dallasStateTB(bool firstCall)
{
    if (firstCall == true)
    {
        // Convert Temperature

        dallas_state.status = dallasTaskConvTemp(ID_LOGICAL_PATH_FOR_MEAS_B);
    }
    else
    {
        // Read Temperature

        dallas_state.status = dallasTaskReadTemp(&PROP_FGC_DLS_MEAS_B, ID_LOGICAL_PATH_FOR_MEAS_B);
    }

    return DALLAS_TASK_STATE_TEMPB;
}



// STATE: TEMPL (TL)

static uint32_t dallasStateTL(bool firstCall)
{
    if (firstCall == true)
    {
        // Convert Temperature

        dallas_state.status = dallasTaskConvTemp(ID_LOGICAL_PATH_FOR_LOCAL);
    }
    else
    {
        // Read Temperature

        dallas_state.status = dallasTaskReadTemp(&PROP_FGC_DLS_LOCAL, ID_LOGICAL_PATH_FOR_LOCAL);
    }

    return DALLAS_TASK_STATE_TEMPL;
}



// STATE: LOGTEMPS (LT)

static uint32_t dallasStateLT(bool firstCall)
{
    dallasTaskLogTemp();
    dallas_state.status = DALLAS_TASK_ERROR_POWER_OFF;

    return DALLAS_TASK_STATE_LOGTEMPS;
}



// STATE: OFF (OF)

static uint32_t dallasStateOF(bool firstCall)
{
    if (dallasComPowerOn() == true)
    {
        dallasComSwitchOff();

        taskTraceInc();

        // If Temperature failure detected

        if (dallas_task.temp_fail == true)
        {
            statusSetWarnings(FGC_WRN_TEMPERATURE);
        }
        else
        {
            statusClrWarnings(FGC_WRN_TEMPERATURE);
        }
    }
    else
    {
        // If on the 10s boundary for the first time

        if (   ms_task.s_mod_10   == 0
            && temp.unix_time != timeGetUtcTime())
        {
            // Record time stamp

            temp.unix_time = timeGetUtcTime();

            if (dls.inhibit_f == 0)
            {
                dallasComSlaveMode();
                dallasComSwitchOn();

                dallas_task.temp_fail = false;
            }
            else
            {
                // Log previous point again

                dallasTaskLogTemp();

                dls.inhibit_f = FGC_CTRL_DISABLED;
            }
        }
    }

    return DALLAS_TASK_STATE_OFF;
}



// ------------- TRANSITION CONDITION FUNCTIONS -------------

// TRANSITION: STARTING to AT_MAINPROG

static DallasTaskState* STtoMP(void)
{
    if (dallas_state.status == DALLAS_TASK_ERROR_OK)
    {
        return dallasStateMP;
    }

    return NULL;
}



// TRANSITION: AT_MAINPROG to READID

static DallasTaskState* MPtoID(void)
{
    if (   dallas_state.status == DALLAS_TASK_ERROR_OK
        && dallas_task.active  == false)
    {
        return dallasStateID;
    }

    return NULL;
}



// TRANSITION: READID to BARCODE

static DallasTaskState* IDtoBC(void)
{
    // Barcodes have been defined by the config manager

    if (   dallas_state.status == DALLAS_TASK_ERROR_OK
        && dallas_task.regfgc3_boards_ready == true)
    {
        return dallasStateBC;
    }

    return NULL;
}



// TRANSITION: ANY STATE to TEMPA

static DallasTaskState* XXtoTA(void)
{
    if (   dallas_state.status == DALLAS_TASK_ERROR_OK
        && dallas_task.active  == true)
    {
        return dallasStateTA;
    }

    return NULL;
}



// TRANSITION: TEMPA to TEMPB

static DallasTaskState* TAtoTB(void)
{
    if (   dallas_state.status == DALLAS_TASK_ERROR_NO_TEMP_SENSOR
        || dallas_state.status == DALLAS_TASK_ERROR_OK)
    {
        return dallasStateTB;
    }

    return NULL;
}



// TRANSITION: TEMPB to TEMPL

static DallasTaskState* TBtoTL(void)
{
    if (   dallas_state.status == DALLAS_TASK_ERROR_NO_TEMP_SENSOR
        || dallas_state.status == DALLAS_TASK_ERROR_OK)
    {
        return dallasStateTL;
    }

    return NULL;
}



// TRANSITION: TEMPL to LOGTEMPS

static DallasTaskState* TLtoLT(void)
{
    if (   dallas_state.status == DALLAS_TASK_ERROR_NO_TEMP_SENSOR
        || dallas_state.status == DALLAS_TASK_ERROR_OK)
    {
        return dallasStateLT;
    }

    return NULL;
}



// TRANSIITON: ANY STATE to OFF

static DallasTaskState* XXtoOF(void)
{
    if (   dallasComPowerOn()  == false
        || dallas_state.status == DALLAS_TASK_ERROR_POWER_OFF)
    {
        return dallasStateOF;
    }

    return NULL;
}



// TRANSIITON: OFF to STARTING

static DallasTaskState* OFtoST(void)
{
    if (dallasComPowerOn() == true)
    {
        return dallasStateST;
    }

    return NULL;
}



// Transition functions : called in priority order, from left to right

static DallasTaskStateTrans * StateTransitions[][DALLAS_STATE_REF_MAX_TRANSITIONS + 1] =
{
    /* 0. ST */ { XXtoOF, STtoMP,                                                          NULL },
    /* 1. MP */ { XXtoOF, MPtoID,                 XXtoTA,                                  NULL },
    /* 2. ID */ { XXtoOF,         IDtoBC,                                                  NULL },
    /* 3. BC */ { XXtoOF,                         XXtoTA,                                  NULL },
    /* 4. TA */ { XXtoOF,                                  TAtoTB,                         NULL },
    /* 5. TB */ { XXtoOF,                                          TBtoTL,                 NULL },
    /* 6. TL */ { XXtoOF,                                                  TLtoLT,         NULL },
    /* 7. LT */ { XXtoOF,                                                                  NULL },
    /* 8. OF */ {                                                                  OFtoST, NULL }
};



// ---------- External function definitions -----------------------------

bool dallasTaskSystemActive(void)
{
    return dallas_task.active;
}



struct TBranchSummary dallasTaskGetBranchInfo(uint16_t branch)
{
    return dallas_task.branches[branch].branch_info;
}



struct TMicroLAN_element_summary * dallasTaskGetDeviceInfo(uint16_t logical_path, uint16_t device_idx)
{
    struct DallasTask_device * current = dallas_task.branches[logical_path].branch_devs;

    for (uint16_t i = 0; i < device_idx; ++i)
    {
        current = current->next;
    }

    return &current->device_info;
}



uint16_t dallasTaskGetCompDbIndex(uint16_t logical_path, uint16_t device_idx)
{
    struct DallasTask_device * current = dallas_task.branches[logical_path].branch_devs;

    for (uint16_t i = 0; i < device_idx; ++i)
    {
            current = current->next;
    }

    return current->compDB_index;
}



char * dallasTaskIdString(const union TMicroLAN_unique_serial_code * devid, char * id_string)
{
    sprintf(id_string, "%02X%02X%02X%02X%02X%02X%02X%02X", (uint16_t) devid->b[7], (uint16_t) devid->b[6],
            (uint16_t) devid->b[5], (uint16_t) devid->b[4], (uint16_t) devid->b[3], (uint16_t) devid->b[2],
            (uint16_t) devid->b[1], (uint16_t) devid->b[0]);

    return id_string;
}



void dallasTaskBarcodesSet(void)
{
    dallas_task.barcodes_ready = true;

    statusClrUnlatched(FGC_UNL_SYNC_BARCODE);
}



void dallasTaskRegFgc3Ready(void)
{
    dallas_task.regfgc3_boards_ready = true;
}



void dallasTask(void * unused)
{
    DallasTaskState       * nextState;
    DallasTaskStateTrans  * transition;
    DallasTaskStateTrans ** stateTransitions;

    // Initialise ADC and DCCT temperatures used by the DSP calibration system

    dpcom.mcu.temp.adc     = CAL_T0;
    dpcom.mcu.temp.dcct[0] = dpcom.mcu.temp.dcct[1] = dpcom.mcu.temp.adc;

    memset(&dallas_task, 0, sizeof(struct DallasTask));

    dallas_state.state     = DALLAS_TASK_STATE_OFF;
    dallas_state.stateFunc = dallasStateOF;

    dallas_task.regfgc3_boards_ready = false;

    // Initialze the number of elements that will be initialized as dallas IDs are scanned

    PROP_FGC_ID_VS_A  .n_elements = 0;
    PROP_FGC_ID_VS_B  .n_elements = 0;
    PROP_FGC_ID_MEAS_A.n_elements = 0;
    PROP_FGC_ID_MEAS_B.n_elements = 0;
    PROP_FGC_ID_LOCAL .n_elements = 0;
    PROP_FGC_ID_CRATE .n_elements = 0;

    // Dallas Task main loop

    for (;;)
    {
        taskTraceReset();

        // Wait for msTask() to wake DslTsk at 10 Hz

        OSTskSuspend();

        taskTraceInc();

        // State machine processing

        nextState = NULL;
        stateTransitions = &StateTransitions[dallas_state.state][0];

        // Scan transitions for the current state, testing each condition in priority order

        while(   (transition = *(stateTransitions++)) != NULL   // there is another transition function,
              && (nextState  = (*transition)())       == NULL)  // and it return NULL (transition condition is false)
        {
            ;
        }

        // If a transition condition was true, then switch to the new state

        if (nextState != NULL)
        {
            dallas_state.stateFunc = nextState;

            // Transition condition is true to switch to new state

            dallas_state.state = nextState(true);
        }
        else
        {
            // No transition : stay in current state and run the state function with the first_call parameter set to false

            dallas_state.stateFunc(false);
        }

        taskTraceInc();
    }
}



// ---------- Internal function definitions -----------------------------

// At start-up the mezzanine board's Dallas ID needs to be read

static void dallasTaskGetExtIds(void)
{
    union
    {
        uint64_t integer;
        union TMicroLAN_unique_serial_code serial;
    }
    mezzanine_id;

    union TMicroLAN_unique_serial_code unique_id;

    uint8_t slot;
    uint8_t i;

    for (slot = 0; slot < FGC_SCIVS_N_SLOT; slot++)
    {
        if ((mezzanine_id.integer = regFgc3ProgGetMezzanineId(slot)) == 0)
        {
            continue;
        }

        for (i = 0; i < MICROLAN_UNIQUE_SERIAL_CODE_LEN; i++)
        {
            unique_id.b[i] = mezzanine_id.serial.b[MICROLAN_UNIQUE_SERIAL_CODE_LEN - 1 - i];
        }

        struct barcode temp_barcode = {{0}};
        struct DallasTask_device * device = (struct DallasTask_device *) &dallas_task.all_devs[dallas_task.n_devs];

        device->device_info.unique_serial_code = unique_id;
        device->device_info.barcode            = temp_barcode;
        device->device_info.logical_path       = ID_LOGICAL_PATH_FOR_CRATE;
        device->device_info.temp_C             = 0;
        device->device_info.temp_C_16          = 0;

        dallasTaskInsertDevice(ID_LOGICAL_PATH_FOR_CRATE, device, true, 0);

        dallas_task.external_id_num++;
    }
}



// This function will prepare the response property when an error is detected. The function also sets
// the MPRUN registers to indicate that the dallas scan has completed, even though an error may have
// stopped the scan early.

static void dallasTaskRspError(struct prop * p, uint16_t errloc, uint16_t errnum, uint16_t rsp_len, uint16_t logical_path)
{
    static uint32_t last_time_start  = 0;
    static uint32_t last_error_count = 0;

    uint8_t * rsp = (uint8_t *) p->value;

    // Store error location and errnum in property

    rsp[0] = errloc;
    rsp[1] = errnum;
    rsp[2] = logical_path;

    last_error_count++;

    // Set the latched condition only if the rate of dallas errors is
    // larger than 5 per hour.

    if (last_error_count > 5)
    {
        if ((ms_task.time_us.secs.abs - last_time_start) < 3600)
        {
            statusSetLatched(FGC_LAT_DALLAS_FLT);
        }

        last_time_start = ms_task.time_us.secs.abs;
        last_error_count = 0;
    }

    dls.num_errors++;

    // Time of last error

    dls.error_time = timeGetUtcTime();

    // Set property length (Header bytes + C62 response)

    PropSetNumEls(NULL, p, rsp_len + 2);
}



static uint16_t dallasTaskReadIds(void)
{
    uint16_t errnum;
    uint16_t rsp_len;
    uint16_t device_idx;
    uint16_t logical_path;
    struct DallasTask_device * device;

    // Check if DALLAS_TASK_CMD_READ_IDS has completed

    if (dallasComTestCmdBusy() == true)
    {
        dallas_task.timeout_cnt--;

        if (dallas_task.timeout_cnt == 0)
        {
            dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, DALLAS_TASK_ERROR_BG_CMD_TIMEOUT, 0, DALLAS_TASK_UNDEF_LOGICAL_PATH);
            return DALLAS_TASK_ERROR_POWER_OFF;
        }

        // Wait another 100ms

        return DALLAS_TASK_ERROR_OK_WAIT;
    }

    // Read out ID device information for all branches

    device = (struct DallasTask_device *) &dallas_task.all_devs;

    for (logical_path = 0; logical_path < FGC_ID_N_BRANCHES; logical_path++)
    {
        // Get branch info

        errnum = dallasComBranchIsrCmd(logical_path);

        if (errnum != DALLAS_TASK_ERROR_OK)
        {
            dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, errnum, 0, logical_path);

            return DALLAS_TASK_ERROR_POWER_OFF;
        }

        rsp_len = sizeof(struct TBranchSummary);

        errnum = dallasTaskReadRsp((uint8_t *) &dallas_task.branches[logical_path].branch_info, &rsp_len);

        if (errnum != DALLAS_TASK_ERROR_OK)
        {
            dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, errnum, 0, logical_path);

            return DALLAS_TASK_ERROR_POWER_OFF;
        }

        // Get devices info

        for (device_idx = 0; device_idx < dallas_task.branches[logical_path].branch_info.total_devices; device_idx++)
        {
            errnum = dallasComDeviceIsrCmd(device_idx);

            if (errnum != DALLAS_TASK_ERROR_OK)
            {
                dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, errnum, 0, logical_path);
                return DALLAS_TASK_ERROR_POWER_OFF;
            }

            rsp_len = sizeof(struct TMicroLAN_element_summary);

            errnum = dallasTaskReadRsp((uint8_t *) &device->device_info, &rsp_len);

            if (errnum != DALLAS_TASK_ERROR_OK)
            {
                dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, errnum, 0, logical_path);
                return DALLAS_TASK_ERROR_POWER_OFF;
            }

            dallasTaskInsertDevice(logical_path, device, false, device_idx);

            device++;
        }
    }

    return DALLAS_TASK_ERROR_OK;
}



static uint16_t dallasTaskReadRsp(uint8_t * buf, uint16_t * max_bytes)
{
    uint16_t errnum;
    uint16_t n_bytes;

    for (n_bytes = 0; n_bytes < *max_bytes; n_bytes++, buf++)
    {
        errnum = dallasComSendCmd(DALLAS_TASK_CMD_GET_RESPONSE, buf);

        if (errnum == DALLAS_TASK_ERROR_NO_MORE_DATA)
        {
            *max_bytes = n_bytes;
            return DALLAS_TASK_ERROR_OK;
        }

        if (errnum != DALLAS_TASK_ERROR_OK)
        {
            *max_bytes = n_bytes;
            return errnum;
        }
    }

    return DALLAS_TASK_ERROR_OK;
}



//! Insert a device in a given branch's device list

static void dallasTaskInsertDevice(uint16_t logical_path, struct DallasTask_device * device, bool ext_id, uint16_t device_idx)
{
    static struct prop * fgc_id_props[] = { &PROP_FGC_ID_VS_A  ,
                                            &PROP_FGC_ID_MEAS_A,
                                            &PROP_FGC_ID_VS_B  ,
                                            &PROP_FGC_ID_MEAS_B,
                                            &PROP_FGC_ID_LOCAL ,
                                            &PROP_FGC_ID_CRATE };

    struct DallasTask_device * head = dallas_task.branches[logical_path].branch_devs;

    fgc_id_props[logical_path]->n_elements++;
    dallas_task.n_devs++;

    if (ext_id == true)
    {
        device_idx = dallas_task.branches[logical_path].branch_info.total_devices;
        dallas_task.branches[logical_path].branch_info.total_devices++;
    }

    device->dev_index = device_idx;
    device->next      = NULL;

    // Empty list

    if (head == NULL)
    {
        head = device;
        dallas_task.branches[logical_path].branch_devs = device;
    }
    else
    {
        // Append at the end of the list

        struct DallasTask_device * current = head;

        while (current->next != NULL)
        {
            current = current->next;
        }

        current->next = device;
    }

    dallas_task.active = true;
}


static void dallasTaskIdentifyAndCountDevices(void)
{
    char           * barcode_ptr;
    uint16_t         dev_idx;
    uint16_t         comp_idx;
    uint16_t         grp_idx;
    int16_t          mismatch;
    char             comp_type[FGC_COMPDB_COMP_LEN + 2];
    uint16_t const   n_comps = CompDbLength();

    struct DallasTask_device * device_ptr;

    device_ptr = (struct DallasTask_device *) &dallas_task.all_devs;

    // For every 1-wire device found

    for (dev_idx = 0; dev_idx < dallas_task.n_devs; dev_idx++, device_ptr++)
    {
        barcode_ptr = (char *) &device_ptr->device_info.barcode.c;

        // A barcode is considered valid if it starts in upper case

        if (*barcode_ptr <= 'Z')
        {
            // Start from component 1 (0=Unknown)

            comp_idx = 1;

            //  Scan the list of components in TYPEDB to find a match

            do
            {
                memcpy(&comp_type, CompDbType(comp_idx), FGC_COMPDB_COMP_LEN + 2);
            }
            while (   ((mismatch = strncmp(barcode_ptr, comp_type, FGC_COMPDB_COMP_LEN)) > 0)
                   && (++comp_idx < n_comps));

            if (mismatch != 0)
            {
                // system 0 (unknown)

                comp_idx = 0;
            }
        }
        else
        {
            // system 0 (unknown)

            comp_idx = 0;
        }

        device_ptr->compDB_index = comp_idx;

        grp_idx = CompDbGroupIndex(comp_idx, device.sys.sys_idx);

        if (grp_idx < FGC_N_COMP_GROUPS)
        {
            dls.detected[grp_idx]++;

            if (grp_idx == 0)
            {
                dls.n_unknown_grp[comp_idx]++;
            }
        }
    }
}



static void dallasTaskCheckGroups(void)
{
    bool     id_fault = false;
    uint16_t grp_idx;

    PROP_BARCODE_MISMATCHED.n_elements = dls.detected[0];

    for (grp_idx = 1; grp_idx < FGC_N_COMP_GROUPS; grp_idx++)
    {
        // If any group is mismatched

        if (dls.detected[grp_idx] != SysDbRequiredGroups(device.sys.sys_idx,grp_idx))
        {
            // If required components are missing

            if (dls.detected[grp_idx] < SysDbRequiredGroups(device.sys.sys_idx,grp_idx))
            {
                id_fault = TRUE;
            }

            PROP_BARCODE_MISMATCHED.n_elements++;
        }
        else
        {
            if (SysDbRequiredGroups(device.sys.sys_idx, grp_idx))
            {
                PROP_BARCODE_MATCHED.n_elements++;
            }
        }
    }

    // If any required components are missing

    if (id_fault == true)
    {
        // TODO ID_FLT should eventually be a fault for operational devices

        statusSetLatched(FGC_LAT_ID_FLT);
    }
}



static void dallasTaskLinkPropertiesAndTemperatures(void)
{
    // Link BARCODE.BUS properties to devices and store barcodes in BARCODE properties

    dallasTaskBarcodeBus(&PROP_BARCODE_BUS_V, ID_LOGICAL_PATH_FOR_VS_A);
    dallasTaskBarcodeBus(&PROP_BARCODE_BUS_V, ID_LOGICAL_PATH_FOR_VS_B);
    dallasTaskBarcodeBus(&PROP_BARCODE_BUS_A, ID_LOGICAL_PATH_FOR_MEAS_A);
    dallasTaskBarcodeBus(&PROP_BARCODE_BUS_B, ID_LOGICAL_PATH_FOR_MEAS_B);
    dallasTaskBarcodeBus(&PROP_BARCODE_BUS_C, ID_LOGICAL_PATH_FOR_CRATE);
    dallasTaskBarcodeBus(&PROP_BARCODE_BUS_L, ID_LOGICAL_PATH_FOR_LOCAL);

    // Link devices to temperatures properties

    dallasTaskPropTemp(ID_LOGICAL_PATH_FOR_VS_A);
    dallasTaskPropTemp(ID_LOGICAL_PATH_FOR_VS_B);
    dallasTaskPropTemp(ID_LOGICAL_PATH_FOR_MEAS_A);
    dallasTaskPropTemp(ID_LOGICAL_PATH_FOR_MEAS_B);
    dallasTaskPropTemp(ID_LOGICAL_PATH_FOR_LOCAL);

    // Set CONFIG.MODE/CONFIG.STATE according to whether SYNC_FGC will be attempted

    if (   configGetState()    != FGC_CFG_STATE_STANDALONE  // not running STANDALONE
        && configGetMode()     != FGC_CFG_MODE_SYNC_FAILED  // and SYNC_FAILED not set and
        && deviceNameIsValid() == true)                     // NameDB is valid
    {
        // Reset barcodes not set by Dallas IDs

        dallasTaskResetBarcodes(&PROP_BARCODE);

        if (fbs.id != 0)
        {
            // Initialise CONFIG.MODE to SYNC_FGC

            configSetMode(FGC_CFG_MODE_SYNC_FGC);
        }
    }
    else
    {
        // S ID RESET

        configSetState(FGC_CFG_STATE_UNSYNCED, false);
    }
}



//! This will scan the barcodes of the devices for the specified branch looking for those that are not
//! DB lookup errors (which begin with a *). Valid barcodes are linked to the BARCODE.BUS property and
//! also, if the component type was found for the device, and if the type has a barcode property specified,
//! then the barcode is also copied to this property.

static void dallasTaskBarcodeBus(struct prop * p, uint16_t logical_path)
{
    uint16_t    n_els;
    uint16_t    comp_idx;
    char        property_name[FGC_COMPDB_PROP_LEN];
    uint8_t  ** bc;
    struct DallasTask_device * device;

    bc = (uint8_t **) p->value;

    device = dallas_task.branches[logical_path].branch_devs;

    p->dynflags |= DF_SET_BY_DALLAS_ID;

    // For all devices on the specified branch

    while (device != NULL)
    {
        // If barcode is valid (starts upper case)

        if (device->device_info.barcode.c[0] <= 'Z')
        {
            n_els = PropGetNumEls(NULL, p);

            if (n_els < p->max_elements)
            {
                // Add barcode to bus property

                bc[n_els] = (uint8_t *)&device->device_info.barcode.c;
                PropSetNumEls(NULL, p, n_els + 1);
            }

            comp_idx = device->compDB_index;

            // If the component was identified

            if (comp_idx != 0)
            {
                // Get barcode property name

                memcpy(&property_name, CompDbBarcodeProperty(comp_idx), FGC_COMPDB_PROP_LEN);

                if (*property_name)
                {
                    // Replace '?' with A or B

                    dallasTaskSetPropertyChan(logical_path, property_name);

                    OSSemPend(pcm.sem);

                    if (PropFind(property_name) == 0)
                    {
                        memcpy(pcm.prop->value, &(device->device_info.barcode.c), FGC_ID_BARCODE_LEN);
                        PropSetNumEls(NULL, pcm.prop, strlen(pcm.prop->value));

                        pcm.prop->dynflags |= DF_SET_BY_DALLAS_ID;
                    }

                    OSSemPost(pcm.sem);
                }
            }
        }

        device = device->next;
    }
}



//! This function is used to replace the "?" character in the temperature and barcode property names with
//! "A" or "B" according to the branch idx (0,1 -> 'A'  2,3 -> 'B')

static void dallasTaskSetPropertyChan(uint16_t logical_path, char * barcode_buf)
{
    if (logical_path < ID_LOGICAL_PATH_FOR_LOCAL)
    {
        while (   *barcode_buf
               && *barcode_buf != '?')
        {
            barcode_buf++;
        }

        if (*barcode_buf)
        {
            *barcode_buf = 'A' + logical_path / 2;
        }
    }
}



//! This will scan the devices on each branch, looking for those for which a component has been identified
//! and for which the component has a temperature property defined.
//! For these devices, the link is made to the temperature property.

static void dallasTaskPropTemp(uint16_t logical_path)
{
    uint16_t comp_idx;
    char     property_name[FGC_COMPDB_PROP_LEN];
    struct DallasTask_device * device;

    device = dallas_task.branches[logical_path].branch_devs;

    while (device != NULL)
    {
        comp_idx = device->compDB_index;

        // If the component was identified

        if (comp_idx != 0)
        {
            // Get temperature property name

            memcpy(&property_name, CompDbTempProperty(comp_idx), FGC_COMPDB_PROP_LEN);

            if (*property_name)
            {
                // Replace '?' with A or B

                dallasTaskSetPropertyChan(logical_path, property_name);

                OSSemPend(pcm.sem);

                // If property found

                if (PropFind(property_name) == 0)
                {
                    device->prop_temp = pcm.prop;
                }

                OSSemPost(pcm.sem);
            }
        }
        device = device->next;
    }
}



//! This recursive function will scan all the (CHAR) barcode properties and will reset the lengths of all that
//! were not set by a Dallas ID.  This is done before requesting a SYNC_FGC by the FGC manager so that old
//! barcodes don't remain to confuse the users.  Properties that were set by a Dallas ID have the
//! DF_SET_BY_DALLAS_ID dynamic property flag set.

static void dallasTaskResetBarcodes(struct prop * p)
{
    uint16_t      ii;
    uint16_t      n_els;
    struct prop * child_prop;

    // If the property is a parent

    if (p->type == PT_PARENT)
    {
        // Get the address of the array of lower level properties

        child_prop = p->value;

        // Get the number of low level properties

        n_els = PropGetNumEls(NULL, p);

        // For each child property

        for (ii = 0; ii < n_els; ii++, child_prop++)
        {
            // Call function recursively for the child

            dallasTaskResetBarcodes(child_prop);
        }
    }
    else
    {
        // If the property is CHAR type (i.e. contains a barcode)
        // and was NOT set by a Dallas ID

        if (   p->type == PT_CHAR
            && testBitmap(p->dynflags, DF_SET_BY_DALLAS_ID) == false)
        {
            // Clear property length

            PropSetNumEls(NULL, p, 0);
        }
    }
}



//! This function is called to start the conversion of temperatures from sensors on the specified
//! logical path.
//! A failure will be considered to be a global failure and operation will stop.

static uint16_t dallasTaskConvTemp(uint16_t logical_path)
{
    uint8_t  rx;
    uint16_t errnum;

    // Check if there are temp sensors to be read

    if (dallas_task.branches[logical_path].branch_info.ds18b20s == 0)
    {
        return DALLAS_TASK_ERROR_NO_TEMP_SENSOR;
    }

    // Set logical_path for temp conversion

    errnum = dallasComBranchIsrCmd(logical_path);

    if (errnum != DALLAS_TASK_ERROR_OK)
    {
        dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, errnum, 0, logical_path);
        return DALLAS_TASK_ERROR_POWER_OFF;
    }

    // Request scan for temperatures on this branch

    errnum = dallasComSendCmd(DALLAS_TASK_CMD_READ_TEMPS_BRANCH, &rx);

    if (errnum != DALLAS_TASK_ERROR_OK)
    {
        dallasTaskRspError(&PROP_FGC_DLS_GLOBAL, __LINE__, errnum, 0, logical_path);
        return DALLAS_TASK_ERROR_POWER_OFF;
    }

    // Set timeout in 100ms units

    dallas_task.timeout_cnt = DALLAS_TASK_READTEMP_TIMEOUT * 10;

    return DALLAS_TASK_ERROR_OK_WAIT;
}



//! This function is called once a DALLAS_TASK_CMD_READ_TEMPS_BRANCH command has been launched.
//! It checks for the command to finish and then tries to read out the new temperature values
//! for the temp sensors on the branch.

static uint16_t dallasTaskReadTemp(struct prop * p, uint16_t logical_path)
{
    uint16_t errnum;
    bool     err_f;
    uint16_t n_temps;
    uint16_t n_temps_read;
    uint16_t rsp_len;
    uint8_t  data[DALLAS_TASK_MAX_EXPECTED_RESPONSE];
    struct   DallasTask_device * device;
    struct   prop ** prop_temp;

    // Check if DALLAS_TASK_CMD_READ_TEMPS_BRANCH has completed

    if (dallasComTestCmdBusy() == true)
    {
        dallas_task.timeout_cnt--;

        if (dallas_task.timeout_cnt == 0)
        {
            dallasTaskRspError(p, __LINE__, DALLAS_TASK_ERROR_BG_CMD_TIMEOUT, 0, logical_path);
            return DALLAS_TASK_ERROR_POWER_OFF;
        }

        // Wait another 100ms

        return DALLAS_TASK_ERROR_OK_WAIT;
    }

    // Check response to DALLAS_TASK_CMD_READ_TEMPS_BRANCH BG command

    rsp_len = DALLAS_TASK_MAX_EXPECTED_RESPONSE - 1;

    errnum = dallasTaskReadRsp((uint8_t *) &data, (uint16_t *) &rsp_len);

    if (errnum != DALLAS_TASK_ERROR_OK)
    {
        dallasTaskRspError(p, __LINE__, errnum, 0, logical_path);
        return DALLAS_TASK_ERROR_POWER_OFF;
    }

    if (rsp_len == 0)
    {
        dallasTaskRspError(p, __LINE__, DALLAS_TASK_ERROR_NO_MORE_DATA, 0, logical_path);
        return DALLAS_TASK_ERROR_POWER_OFF;
    }

    err_f = data[0];

    // If DALLAS_TASK_CMD_READ_TEMPS_BRANCH failed

    if (err_f == true)
    {
        memcpy(((uint8_t *)(p->value)) + 1, &data, rsp_len);

        // Report failure for branch

        dallasTaskRspError(p, __LINE__, data[0], (rsp_len - 1), logical_path);

        // Prepare "BAD TEMP" value

        data[2] = ID_BAD_TEMP;
        data[3] = 0;
    }

    device       = dallas_task.branches[logical_path].branch_devs;
    n_temps      = dallas_task.branches[logical_path].branch_info.ds18b20s;
    n_temps_read = 0;

    while(n_temps_read < n_temps)
    {
        prop_temp = &device->prop_temp;

        // If the device is a temp sensor

        if (device->device_info.unique_serial_code.p.family == MICROLAN_DEVICE_DS18B20_THERMOMETER)
        {
            // If the was no error for this branch

            if (err_f == false)
            {
                // Set device index

                errnum = dallasComDeviceIsrCmd(device->dev_index);

                if (errnum != DALLAS_TASK_ERROR_OK)
                {
                    dallasTaskRspError(p, __LINE__, errnum, 0, logical_path);
                    return DALLAS_TASK_ERROR_POWER_OFF;
                }

                rsp_len = sizeof(data);

                // Get the temperature for the device

                errnum = dallasTaskReadRsp((uint8_t *) &data, &rsp_len);

                if (errnum != DALLAS_TASK_ERROR_OK)
                {
                    dallasTaskRspError(p, __LINE__, errnum, 0, logical_path);
                    return DALLAS_TASK_ERROR_POWER_OFF;
                }
            }

            // Recover the temperature from the response, or BAD TEMP if an error occurred

            device->device_info.temp_C    = data[2];
            device->device_info.temp_C_16 = data[3];

            if (*prop_temp)
            {
                *((uint16_t *)((*prop_temp)->value)) = device->device_info.temp_C * 16 + device->device_info.temp_C_16;
            }

            n_temps_read++;
        }
        device = device->next;
    }

    return DALLAS_TASK_ERROR_OK;
}



//! This function is called once all the temperatures have been read to transfer the ADC and DCCT temperatures
//! (if valid) to the DSP.
//!
//! temp.fgc.in and temp.fgc.out are linked with the corresponding temperature sensor
//! via the assignment in components.xml with
//!      temp_prop = "TEMP.FGC.IN"
//!  and
//!      temp_prop = "TEMP.FGC.OUT"
//!  (this information is extracted from CompDB by the MCU program)

static void dallasTaskLogTemp(void)
{
    // Calculate FGC delta (outlet - inlet)

    if (   temp.fgc.in  != 0 && temp.fgc.in  < (ID_BAD_TEMP * ID_TEMP_GAIN_PER_C)
        && temp.fgc.out != 0 && temp.fgc.out < (ID_BAD_TEMP * ID_TEMP_GAIN_PER_C))
    {
        temp.fgc.delta = abs(temp.fgc.in - temp.fgc.out);
    }
    else
    {
        // Bad data: 99.0C

        temp.fgc.delta        = ID_BAD_TEMP * ID_TEMP_GAIN_PER_C;
        dallas_task.temp_fail = true;
    }

    // Transfer the temperatures to DSP

    dpcom.mcu.temp.in      = ID_TEMP_FLOAT(temp.fgc.in);
    dpcom.mcu.temp.out     = ID_TEMP_FLOAT(temp.fgc.out);
    dpcom.mcu.temp.adc     = dpcom.mcu.temp.in + 1;  // EPCCCS-8865
    dpcom.mcu.temp.dcct[0] = ID_TEMP_FLOAT(temp.dcct[0].elec);
    dpcom.mcu.temp.dcct[1] = ID_TEMP_FLOAT(temp.dcct[1].elec);
}


// EOF
