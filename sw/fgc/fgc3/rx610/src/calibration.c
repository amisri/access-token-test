//! @file   calibration.c
//! @brief  Calibration processing of the ADCs, DCCTs and DAC


// ---------- Includes

#include <bitmap.h>
#include <calibration.h>
#include <config.h>
#include <defprops.h>
#include <device.h>
#include <dpcom.h>
#include <fbs.h>
#include <msTask.h>
#include <nvs.h>
#include <property.h>
#include <pc_state.h>
#include <sta.h>
#include <stateOp.h>
#include <status.h>
#include <time_fgc.h>



// ---------- Constants

#define CAL_NO_AUTO_TIME        86400L      //!< No auto ADC calibration time (s)
// #define CAL_IN_STATE_DELAY      600000L     //!< No auto ADC calibration before a minimum amount of time spent in the same state (ms)
#define CAL_SUM16_PERIOD        40          //!< ADC16 calibration period (x200 samples)
#define CAL_DCCT_PERIOD         50          //!< DCCT  calibration period (x200 samples)
#define CAL_DAC_PERIOD          1           //!< Internal DAC calibration period (x200 samples)
#define CAL_INTERNAL_ADCS       0x0F        //!< All internal ADCs



// ---------- Internal structures, unions and enumerations

enum Calibration_state
{
    CAL_STATE_IDLE,                         //!< Idle state
    CAL_STATE_REQUEST,                      //!< Send calibration request to DSP
    CAL_STATE_WAIT,                         //!< Wait until the DSP finishes calibrating
    CAL_STATE_NVS_SAVE,                     //!< Save erros in non-volatile memory
    CAL_STATE_TERMINATE,                    //!< End calibration
    CAL_NUM_STATES
};

//! The next calibration request to be sent to the DSP
//! CAUTION: Right now we assume there is only one active cal request at a time (it basically means that the
//! end user can only do one S CAL command at a time, and wait for the command to complete before sending a
//! new one.)

struct Calibration_request
{
    uint32_t              chan_mask;        //!< To be copied to dpcom.mcu.cal.chan_mask
    uint32_t              ave_steps;        //!< To be copied to dpcom.mcu.cal.ave_steps
    int32_t               idx;              //!< To be copied to dpcom.mcu.cal.idx
    enum Dpcom_cal_action action;           //!< To be copied to dpcom.mcu.cal.action (casted to int32_t)
};

struct Calibration
{
    struct Calibration_request dsp_request;
    enum Calibration_state     state;
    enum Calibration_target    target;
    bool                       active;
};



// ---------- Internal function declarations

static void calibrationStateIdle(void);
static void calibrationStateRequest(void);
static void calibrationStateWait(void);
static void calibrationStateNvsSave(void);
static void calibrationStateTerminate(void);

static void calibrationLastCalTime(void);
static bool calibrationAutoCalTime(void);



// ---------- External variable definitions

struct Calibration_properties cal;



// ---------- Internal variable definitions

typedef void(*state_func_ptr_t)(void);

static state_func_ptr_t stateFuncs[CAL_NUM_STATES] =
{
    calibrationStateIdle,
    calibrationStateRequest,
    calibrationStateWait,
    calibrationStateNvsSave,
    calibrationStateTerminate,
};

static struct Calibration calibration =
{
    { 0x00, 0, 0, DPCOM_CAL_REQ_NONE },
    CAL_STATE_IDLE,
    CAL_TARGET_NONE,
    false,
};

static struct prop * calibration_error_property[][5] =
{
    {
        &PROP_CAL_A_ADC_INTERNAL_ERR,
        &PROP_CAL_B_ADC_INTERNAL_ERR,
        &PROP_CAL_C_ADC_INTERNAL_ERR,
        &PROP_CAL_D_ADC_INTERNAL_ERR,
        NULL,
    },

#if (FGC_CLASS_ID == 63)
    {
        &PROP_CAL_A_ADC_EXTERNAL_ERR,
        &PROP_CAL_A_ADC_EXTERNAL_ERR,
        NULL,
    },
#else
    {
        NULL,
    },
#endif

#if (FGC_CLASS_ID != 64) && (FGC_CLASS_ID != 65)
    {
        &PROP_CAL_A_DCCT_ERR,
        &PROP_CAL_A_DCCT_ERR,
        NULL,
    },
#else
    {
        NULL,
    },
#endif

    {
        NULL,
    },
};



// ---------- Internal function definitions

static void calibrationStateIdle(void)
{
    if (   calibration.active == true
        && calibration.target != CAL_TARGET_NONE)
    {
        calibration.state = CAL_STATE_REQUEST;
    }
}



// Request calibration to the DSP

static void calibrationStateRequest(void)
{
    if (dpcom.mcu.cal.req_f == true)
    {
        // DSP already has a calibration request to read

        return;
    }
    else
    {
        // DSP is ready: copy the calibration request in shared memory and go to next calibration step

        dpcom.mcu.cal.action    = (int32_t)calibration.dsp_request.action;
        dpcom.mcu.cal.chan_mask =          calibration.dsp_request.chan_mask;
        dpcom.mcu.cal.ave_steps =          calibration.dsp_request.ave_steps;
        dpcom.mcu.cal.idx       =          calibration.dsp_request.idx;

        __sync_synchronize();

        dpcom.mcu.cal.req_f     = true;

        calibration.state       = CAL_STATE_WAIT;
    }
}



static void calibrationStateWait(void)
{
    if ((uint16_t) dpcom.dsp.cal.complete_f == true)
    {
        dpcom.mcu.cal.req_f      = false;
        dpcom.dsp.cal.complete_f = false;
        calibration.state        = CAL_STATE_NVS_SAVE;
    }
}



// Save errors in non-volatile memory, according to calibration.dsp_request.chan_mask

static void calibrationStateNvsSave(void)
{
    uint32_t channel = 0x01;

    for (uint16_t i = 0; calibration_error_property[calibration.target][i] != NULL; i++, channel <<= 1)
    {
        if (testBitmap(calibration.dsp_request.chan_mask, channel) == true)
        {
            nvsStoreValuesForOneDspProperty(calibration_error_property[calibration.target][i], NVS_ALL_SUB_SELS, NVS_ALL_CYC_SELS);
        }
    };

    if (   calibration.target                == CAL_TARGET_INT_ADCS
        && calibration.dsp_request.chan_mask == CAL_INTERNAL_ADCS)
    {
        calibrationLastCalTime();
    }

    calibration.state = CAL_STATE_TERMINATE;
}



// End auto calibration: this will run after a pause of 200ms to allow the multiplexers to
// be reset to the standard values in case the system is unconfigured. By clearing the FGC_OP_CALIBRATING state
// in STATE_OP, this function terminates the calibration sequence.

static void calibrationStateTerminate(void)
{
    // SYNC_DB not required for DACS calibration

    if (   calibration.target          != CAL_TARGET_DACS
        && deviceNameIsValid()         == true
        && configGetState()            != FGC_CFG_STATE_STANDALONE  // CONFIG.MODE can be modified
        && configGetMode()             == FGC_CFG_MODE_SYNC_NONE    // No synchronisation is in progress or failed
        && fbsIsStandalone()           == false
        && configManagerNotAvailable() == false)
    {
        // Request SYNC_DB_CAL by the Config Manager

        configSetMode(FGC_CFG_MODE_SYNC_DB_CAL);
    }

    calibration.target = CAL_TARGET_NONE;
    calibration.state  = CAL_STATE_IDLE;
    calibration.active = false;
}



// Save the last calibration time of the internal ADCs (supposedly, calibration of ALL internal ADCS):
// ADC.ADC16.LAST_CAL_TIME

static void calibrationLastCalTime(void)
{
    cal.last_cal_time_unix = timeGetUtcTime();
    nvsStoreBlockForOneProperty(NULL,
                                &PROP_ADC_INTERNAL_LAST_CAL_TIME,
                                (uint8_t *) &cal.last_cal_time_unix,
                                0,
                                0,
                                1,
                                true);
}



static bool calibrationAutoCalTime(void)
{
    if (    vs.present              == FGC_CTRL_ENABLED
         && ms_task.set_cmd_counter == 0                        // no set command run in the past 5 seconds and
         && configGetMode()         == FGC_CFG_MODE_SYNC_NONE   // no sync request is outstanding and
         && cal.inhibit_cal         == 0                        // calibration is NOT inhibited at the moment and
         && mcu.set_lock->counter   != 0                        // set command not running (might be S CAL) and
         && stateVsIsOff()          == true                     // voltage soruce is OFF
         && pcStateTest(FGC_STATE_EQ_OFF_BIT_MASK) == true      // converter is OFF and
         && stateOpGetState()       == FGC_OP_NORMAL
         && (   fbsIsStandalone() == true                       // not FIP/ETH connected or
             || (   fbs.gw_online_f
#if (FGC_CLASS_ID == 63)
                 && statusTestUnlatched(FGC_UNL_LOW_CURRENT) == true // GW present and LOW CURRENT and time
#endif
             ))
         && (timeGetUtcTime() - cal.last_cal_time_unix) > CAL_NO_AUTO_TIME)         // to calibrate
    {
        return true;
    }

    return false;
}



// ---------- External function definitions

void calibrationSetup(enum Calibration_target target, uint32_t chan_mask, int32_t signal)
{
    if (   target            != CAL_TARGET_NONE
        && stateOpGetState() != FGC_OP_CALIBRATING)
    {
        calibration.target = target;

        calibration.dsp_request.chan_mask = chan_mask;
        calibration.dsp_request.idx       = signal;

        switch (calibration.target)
        {
            case CAL_TARGET_INT_ADCS:
                calibration.dsp_request.action    = DPCOM_CAL_REQ_INTERNAL_ADC;
                calibration.dsp_request.ave_steps = CAL_SUM16_PERIOD;
                break;

            case CAL_TARGET_EXT_ADCS:
                calibration.dsp_request.action    = DPCOM_CAL_REQ_EXTERNAL_ADC;
                calibration.dsp_request.ave_steps = CAL_SUM16_PERIOD;
                break;

            case CAL_TARGET_DCCTS:
                calibration.dsp_request.action    = DPCOM_CAL_REQ_DCCT;
                calibration.dsp_request.ave_steps = CAL_DCCT_PERIOD;
                break;

            case CAL_TARGET_DACS:
                calibration.dsp_request.action    = DPCOM_CAL_REQ_DAC;
                calibration.dsp_request.ave_steps = CAL_DAC_PERIOD;
                break;

            default:
                // Do nothing
                break;
        }

        calibration.active = true;
    }
}



void calibrationRun(void)
{
    stateFuncs[calibration.state]();
}



bool calibrationIsActive(void)
{
    return calibration.active;
}



void calibrationInhibit(uint32_t time)
{
    cal.inhibit_cal = time;
}



void calibrationProcess(void)
{
    if (sta.sec_f == false)
    {
        // Do nothing if it is not the start of a new second
        return;
    }

    // Down count inhibit_cal time at 1Hz

    if (cal.inhibit_cal > 0)
    {
        cal.inhibit_cal--;
    }

    // Auto calibration every day (as defined per CAL_NO_AUTO_TIME)

    if (calibrationAutoCalTime() == true)
    {
        calibrationSetup(CAL_TARGET_INT_ADCS, CAL_INTERNAL_ADCS, 0);
    }
}


// EOF
