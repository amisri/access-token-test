//! @file   serialStream.c
//! @brief  FGC Software - Serial Communication Callback Functions


// ---------- Includes

#include <serialStream.h>
#include <bitmap.h>
#include <cmd.h>
#include <fbs.h>
#include <os.h>
#include <rtd.h>
#include <trm.h>


// ---------- Internal function definitions

//! This function is called from serialStreamInternalWriteChar() to put the character ch
//! into the buffer for the specified stream f.  If the buffer is full it will be flushed automatically.

static void serialStreamTermPutcChar(char const ch, FILE * f)
{
    char     ** stream_buffer_ptr;
    uint16_t  * stream_buffer_offset_ptr;

    if (f == tcm.f)
    {
        stream_buffer_ptr        = &(tcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(tcm.serial_stream_buffer_offset);
    }
    else if (f == rtd.f)
    {
        stream_buffer_ptr        = &(rtd.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(rtd.serial_stream_buffer_offset);
    }
    else if (f == pcm.f)
    {
        stream_buffer_ptr        = &(pcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(pcm.serial_stream_buffer_offset);
    }
    else if (f == fcm.f)
    {
        stream_buffer_ptr        = &(fcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(fcm.serial_stream_buffer_offset);
    }
    else
    {
        return;
    }

    if (*stream_buffer_ptr == 0)
    {
        // No buffer is in use with this stream

        // Pend of a free buffer from partition

        *stream_buffer_ptr = (char *) OSMemPend(terminal.mbuf);

        // Reset buffer index

        *stream_buffer_offset_ptr = 0;
    }

    // Save character in current buffer

    (*stream_buffer_ptr)[(*stream_buffer_offset_ptr)++] = ch;

    if (*stream_buffer_offset_ptr >= TRM_BUF_SIZE)
    {
        // Buffer is now full.
        // Flush buffer to SCI.

        serialStreamInternalFlushBuffer(f);
    }
}



// ---------- External function definitions

void serialStreamInternalWriteChar(char const ch, FILE * f)
{
    uint16_t * line_offset_ptr;

    if (f == tcm.f)
    {
        line_offset_ptr = &(tcm.line_offset);
    }
    else if (f == rtd.f)
    {
        line_offset_ptr = &(rtd.line_offset);
    }
    else if (f == pcm.f)
    {
        line_offset_ptr = &(pcm.line_offset);
    }
    else if (f == fcm.f)
    {
        line_offset_ptr = &(fcm.line_offset);
    }
    else
    {
        return;
    }

    // The first newline is written and a newline flag is set.
    // All subsequent newlines are ignored until a different character must be written.

    if (ch == '\n')
    {
        if (testBitmap(f->_flags2, NL) == false)
        {
            *line_offset_ptr = 0;
            serialStreamTermPutcChar(ch, f);

            if (terminal.mode == TRM_EDITOR_MODE)
            {
                serialStreamTermPutcChar('\r', f);
            }

            setBitmap(f->_flags2, NL);
        }
        return;
    }
    else if (testBitmap(f->_flags2, NL) == true)
    {
        clrBitmap(f->_flags2, NL);
    }

    // Vertical tab

    if (ch == '\v')
    {
        // Flush buffer to SCI immediately

        serialStreamInternalFlushBuffer(f);
        return;
    }

    // All other characters

    serialStreamTermPutcChar(ch, f);
    *line_offset_ptr += 1;
}



void serialStreamInternalFlushBuffer(FILE * f)
{
    OS_CPU_SR    cpu_sr;
    char      ** stream_buffer_ptr;
    uint16_t   * stream_buffer_offset_ptr;

    if (f == tcm.f)
    {
        stream_buffer_ptr        = &(tcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(tcm.serial_stream_buffer_offset);
    }
    else if (f == rtd.f)
    {
        stream_buffer_ptr        = &(rtd.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(rtd.serial_stream_buffer_offset);
    }
    else if (f == pcm.f)
    {
        stream_buffer_ptr        = &(pcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(pcm.serial_stream_buffer_offset);
    }
    else if (f == fcm.f)
    {
        stream_buffer_ptr        = &(fcm.serial_stream_buffer_ptr);
        stream_buffer_offset_ptr = &(fcm.serial_stream_buffer_offset);
    }
    else
    {
        return;
    }

    if (*stream_buffer_ptr != 0)
    {
        // Buffer is defined

        OS_ENTER_CRITICAL();

        // Queue point to SCI buffer and length of contents

        terminal_state.bufq_adr[terminal_state.bufq.in_idx] = *stream_buffer_ptr;
        terminal_state.bufq_len[terminal_state.bufq.in_idx] = *stream_buffer_offset_ptr;

        if (++terminal_state.bufq.in_idx > TRM_N_BUFS)
        {
            // Input index has advanced beyond end of queue.
            // Reset queue input index

            terminal_state.bufq.in_idx = 0;
        }

        OS_EXIT_CRITICAL();

        // Clear local copy of buffer pointer

        *stream_buffer_ptr = 0;
    }
}


// EOF
