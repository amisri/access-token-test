//! @file   lan92.c
//! @brief  Ethernet eth Interface Functions.


// ---------- Includes

#include <lan92.h>
#if defined(__RX610__)
#include <structs_bits_big.h>
#endif

#include <bitmap.h>
#include <mcuDependent.h>
#include <sleep.h>
#include <stdio.h>
#include <sleep.h>
#include <assert.h>
#include <ethernet.h>



// ---------- Constants

#define RX_DP_CTRL_RX_FFWD 0x80000000



// ---------- Internal structures, unions and enumerations

struct Lan92
{
    union TUnion32Bits extra_dword;
    uint8_t            extra_dword_size;
    uint16_t           stats_fifo_overrun;
} lan92;




// ---------- Internal function definitions

//! Checks if transmitter is enabled
//!
//! @retval  true   Transmitter is enabled
//! @retval  false  Transmitter is disabled

static inline bool lan92IsTxReady(void)
{
    return (testBitmap(ETH_TX_CFG_P,  ETH_TX_CFG_TX_ON) == true);
}



//! Returns amount of free space in bytes in Tx FIFO

static inline uint16_t lan92GetTxFifoSpace(void)
{
    static_assert(ETH_TX_FIFO_INF_TDFREE <= UINT16_MAX, "uint16_t too small");

    return (uint16_t)(ETH_TX_FIFO_INF_P & ETH_TX_FIFO_INF_TDFREE);
}



// ---------- External function definitions

enum Lan92_status lan92SendPacket(void const * packet_data, uint16_t const packet_size)
{
    return lan92SendPacketOpt(packet_data, packet_size, packet_size, TX_CMDA_FIRST_SEG | TX_CMDA_LAST_SEG, 0);
}



enum Lan92_status lan92SendPacketOpt(void     const * segment,
                                     uint16_t const   segment_size,
                                     uint16_t const   packet_size,
                                     uint32_t const   optA,
                                     uint32_t const   optB)
{
    // Check size boundary

    if (packet_size > 0x07FF)
    {
        return LAN92_SIZE_OUT_OF_BOUNDS;
    }

    // check enough space is available in the FIFO, if not, wait few us

    if (segment_size > lan92GetTxFifoSpace())
    {
        uint16_t timeout_us = 80;

        lan92.stats_fifo_overrun++;

        while (segment_size > lan92GetTxFifoSpace() && timeout_us--)
        {
            sleepUs(1);
        }

        if (timeout_us == 0)
        {
            // Timeout waiting for fifo to be empty

            return LAN92_TX_FIFO_FULL;
        }
    }

    if ((DMAC0.DMCRE.BIT.DEN == 1) || (DMAC1.DMCRE.BIT.DEN == 1)) // DMA is in progress
    {
        return LAN92_DMA_IN_PROGRESS;
    }

    if (lan92IsTxReady() == false)
    {
        return LAN92_TX_NOT_READY;
    }

    // Write command A & B
    // Command A and B must not be byte swapped, because they're not data but processed by
    // the chip, even though header are also processed by the chip and need byte swapping
    // Thus, we use direct FIFO (using FIFO_SEL) w/o byte swapping for commands, port FIFO 
    // with byte swapping for data

    // Command A :  no Interrupt on Completion, no offset
    //              one buffer for packet (buffer is first and last segment)

    *(REG32U *)(ETH_TX_DATA_FIFO_PORT_32 + 0x00000100) = (uint32_t)(segment_size | optA);

    // Command B :  use CRC, use frame padding if < 64
    //              no packet tag by default

    *(REG32U *)(ETH_TX_DATA_FIFO_PORT_32 + 0x00000100) = (uint32_t) packet_size | optB;

    // Total number of bytes to be written is (segment_size+data_offset)
    // divide by 4 and ceil to get size in DWORDS

    uint16_t segment_dword_size = (segment_size >> 2) + ((segment_size & 0x0003) ? 1 : 0);

    if (segment != NULL)
    {
        for (uint16_t i = 0; i < segment_dword_size; i++)
        {
            uint32_t data = *(uint32_t *) segment;
            
            segment = ((uint32_t *) segment) + 1;
            
            ETH_TX_DATA_FIFO_PORT_P = data;
        }
    }

    if (optA & 0x80000000)
    {
        // TODO : Not implemented, useless so far
        // wait status  ..polling
        // read status
        // if error return error
    }

    return LAN92_OK;
}



void lan92DiscardPacket(uint16_t num_bytes)
{
    uint16_t num_dwords = (num_bytes + 3) / 4;

    if (num_dwords == 0)
    {
        return;
    }

    // If there are less than 4 long words to discard, we need to read them using PIO operations.
    // Otherwise, we can use the fast-forward mechanism implemented on the chip

    if (num_dwords < 4)
    {
        while (num_dwords != 0)
        {
            // Dummy read of RX data FIFO

            if (ETH_RX_DATA_FIFO_PORT_P)
            {
            }

            num_dwords--;
        }
    }
    else
    {
        // Set fast-forward bit

        setBitmap(ETH_RX_DP_CTL_P, RX_DP_CTRL_RX_FFWD);

        // Wait for the bit to clear

        while(testBitmap(ETH_RX_DP_CTL_P, RX_DP_CTRL_RX_FFWD))
        {
            ;
        }
    }

    // Flush read buffer

    lan92.extra_dword_size = 0;
}



void lan92RxDump(void)
{
    // Flush read buffer

    lan92.extra_dword_size = 0;             

    // Halt RX
    
    ETH_MAC_CSR_CMD_P = 0xC0000001;

    // Wait CSR busy flag is cleared

    while (ETH_MAC_CSR_CMD_P & 0x80000000)
    {
        ;
    }

    ETH_MAC_CSR_DATA_P &= ~0x00000004;
    ETH_MAC_CSR_CMD_P   =  0x80000001; 

    // RX dump
    
    ETH_RX_CFG_P |= 0x00008000;

    // Wait RX_DUMP bit is cleared

    while (ETH_RX_CFG_P & 0x00008000)
    {
        ;
    }

    //  Restart RX

    ETH_MAC_CSR_CMD_P = 0xC0000001;

    // Wait CSR busy flag is cleared

    while (ETH_MAC_CSR_CMD_P & 0x80000000)
    {
        ;
    }

    ETH_MAC_CSR_DATA_P |= 0x00000004;
    ETH_MAC_CSR_CMD_P   = 0x80000001;
}



void lan92ReadFifo(void const * to, uint16_t read_length, uint16_t * fifo_length)
{
    // Todo check if portable for a little endian processor

    // If bytes left from last read, recover them

    if (lan92.extra_dword_size != 0)
    {
        uint16_t i = 0;

        while (lan92.extra_dword_size > 0 && read_length > 0)
        {
            (*(union TUnion32Bits *) to).byte[i++] = lan92.extra_dword.byte[4 - lan92.extra_dword_size];
            lan92.extra_dword_size--;
            read_length--;
        }

        to = (uint8_t *)to + i;
    }

    // Truncate to number of data available

    if (read_length > *fifo_length)
    {
        read_length = *fifo_length; 
    }

    uint16_t nb_byte_left = read_length & 0x0003;
    uint16_t dword_length = read_length >> 2;

    // Copy by DWORD access

    for (uint16_t i = 0; i < dword_length; i++)
    {
        (*(union TUnion32Bits *) to).int32u = ETH_RX_DATA_FIFO_PORT_P;
        to = ((union TUnion32Bits *) to) + 1;
    }

    // If non DWORD access, byte access for last one, and save extra bytes

    if (nb_byte_left != 0)
    {
        dword_length++;

        lan92.extra_dword.int32u = ETH_RX_DATA_FIFO_PORT_P;
        lan92.extra_dword_size = (sizeof(uint32_t) / sizeof(uint8_t)) - nb_byte_left;

        for (uint16_t i = 0; i < nb_byte_left; i++)
        {
            (*(union TUnion32Bits *) to).byte[i] = lan92.extra_dword.byte[i];
        }
    }
    else
    {
        lan92.extra_dword_size = 0;
    }

    // Update fifo_length

    *fifo_length -= dword_length * sizeof(uint32_t);
}



void lan92SetMac(uint8_t mac[6])
{
    uint32_t   mac_addr;
    uint8_t  * p_mac_addr;

    // Mac address "high" (two low bytes). Don't use high word.

    p_mac_addr = (uint8_t *)&mac_addr;
    p_mac_addr += 2;

    for (uint16_t i = 0; i < 2; i++)
    {
        p_mac_addr[i] = mac[5 - i];
    }

    ETH_MAC_CSR_DATA_P = mac_addr & 0x0000FFFF;
    ETH_MAC_CSR_CMD_P = 0x80000002;

    // Mac address "low" (four high bytes)

    p_mac_addr = (uint8_t *) &mac_addr;

    for (uint16_t i = 0; i < 4; i++)
    {
        p_mac_addr[i] = mac[3 - i];
    }

    ETH_MAC_CSR_DATA_P = mac_addr;
    ETH_MAC_CSR_CMD_P  = 0x80000003;
}



void lan92GetMac(uint8_t mac[6])
{
    uint32_t   mac_addr;
    uint8_t  * p_mac_addr;

    // Mac address "high" (two low bytes). Discard high word.

    ETH_MAC_CSR_CMD_P = 0xC0000002;
   
    mac_addr = ETH_MAC_CSR_DATA_P & 0x0000FFFF;
   
    p_mac_addr  = (uint8_t *)&mac_addr;
    p_mac_addr += 2;

    for (uint16_t i = 0; i < 2; i++)
    {
        mac[5 - i] = p_mac_addr[i];
    }

    // mac address "low" (four high bytes)

    ETH_MAC_CSR_CMD_P = 0xC0000003;

    mac_addr   = ETH_MAC_CSR_DATA_P;
    p_mac_addr = (uint8_t *)&mac_addr;

    for (uint16_t i = 0; i < 4; i++)
    {
        mac[3 - i] = p_mac_addr[i];
    }
}



uint16_t lan92ReadPhyReg(uint16_t const addr)
{
    // Wait busy flag is OK

    ETH_MAC_CSR_CMD_P = 0xC0000006;

    // Wait CSR busy flag is cleared
    
    while (ETH_MAC_CSR_CMD_P & 0x80000000)
    {
        ;
    }

    while (ETH_MAC_CSR_DATA_P & 0x00000001)
    {
        ETH_MAC_CSR_CMD_P = 0xC0000006;

        // Wait CSR busy flag is cleared
        
        while (ETH_MAC_CSR_CMD_P & 0x80000000)
        {
            ;
        }
    }

    // Write MII_ACC with read command

    ETH_MAC_CSR_DATA_P = 0x000000801 | ((addr << 6) & 0x000007C0);
    ETH_MAC_CSR_CMD_P  = 0x80000006;

    // Wait busy flag is ok

    ETH_MAC_CSR_CMD_P = 0xC0000006;

    // Wait CSR busy flag is cleared

    while (ETH_MAC_CSR_CMD_P & 0x80000000)
    {
        ;
    }

    while (ETH_MAC_CSR_DATA_P & 0x00000001)
    {
        ETH_MAC_CSR_CMD_P = 0xC0000006;

        // Wait CSR busy flag is cleared

        while (ETH_MAC_CSR_CMD_P & 0x80000000)
        {
            ;
        }
    }

    // Read data

    ETH_MAC_CSR_CMD_P = 0xC0000007;

    // Wait CSR busy flag is cleared

    while (ETH_MAC_CSR_CMD_P & 0x80000000)
    {
        ;
    }

    return (ETH_MAC_CSR_DATA_P & 0x0000FFFF);
}



void lan92WritePhyReg(uint16_t const value, uint16_t const addr)
{
    // Wait busy flag is OK

    ETH_MAC_CSR_CMD_P = 0xC0000006;

    // Wait CSR busy flag is cleared

    while (ETH_MAC_CSR_CMD_P & 0x80000000)
    {
        ;
    }

    while (ETH_MAC_CSR_DATA_P & 0x00000001)
    {
        ETH_MAC_CSR_CMD_P = 0xC0000006;

        // Wait CSR busy flag is cleared

        while (ETH_MAC_CSR_CMD_P & 0x80000000)
        {
            ;
        }
    }

    // Write value in MII_DATA
    // Write MII_ACC with read command

    ETH_MAC_CSR_DATA_P = value;
    ETH_MAC_CSR_CMD_P = 0x80000007;

    // Wait busy flag is OK

    ETH_MAC_CSR_CMD_P = 0xC0000006;

    // Wait CSR busy flag is cleared

    while (ETH_MAC_CSR_CMD_P & 0x80000000)
    {
        ;
    }

    while (ETH_MAC_CSR_DATA_P & 0x00000001)
    {
        ETH_MAC_CSR_CMD_P = 0xC0000006;

        // Wait CSR busy flag is cleared

        while (ETH_MAC_CSR_CMD_P & 0x80000000)
        {
            ;
        }
    }

    // Write MII_ACC with write command

    ETH_MAC_CSR_DATA_P = 0x00000802 | ((addr << 6) & 0x000007C0);
    ETH_MAC_CSR_CMD_P  = 0x80000006;
}


// EOF
