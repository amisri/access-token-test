//! @file  dimDataProcess.h
//! @brief Process data received via QSPI bus


// ---------- Includes

#include <dimDataProcess.h>

#include <bitmap.h>
#include <class.h>          // Include class dependent definitions
#include <definfo.h>        // for FGC_CLASS_ID
#include <defprops.h>
#include <diag.h>
#include <fbs.h>            // for fbs global variable, fbsOutBuf()
#include <fgc_errs.h>       // for FGC_DSP_NOT_AVL, FGC_BAD_ARRAY_IDX, FGC_BAD_GET_OPT
#include <fgc_log.h>        // for struct fgc_log_header, struct fgc_log_sig_header
#include <memmap_mcu.h>
#include <msTask.h>            // for ms_task global variable
#include <logSpy.h>
#include <os.h>             // for OSTskSuspend()
#include <prop.h>           // for PropValueGet()
#include <sleep.h>
#include <status.h>
#include <stdio.h>          // for fprintf()
#include <string.h>         // for memset()
#include <fgc/fgc_db.h>
#if defined(__RX610__)
#include <structs_bits_big.h>
#endif



// ---------- External variable definition

struct TQspiMiscellaneousInfo __attribute__((section("sram"))) qspi_misc;
struct TDimCollectionInfo     __attribute__((section("sram"))) dim_collection;
struct TDimLogEntryStatus     __attribute__((section("sram"))) dbg_dim_log_status[REQUESTS_FOR_DIM_LOG];



// ---------- Internal structures, unions and enumerations

// we can make up to 3 log request
struct TDimLogRequests
{
    //    uint8_t    number_of_requests;                    // (1-3) number of logical DIMs to log
    uint8_t   logical_dim_to_log_for_req[REQUESTS_FOR_DIM_LOG];
    uint16_t  entry_for_req[REQUESTS_FOR_DIM_LOG];   // list of sample index
};

struct dims_data_process_local_t
{
    bool                   trigger_fired;                        //!< DIM trigger detected flag
    uint32_t               reset_req_packed;                     //!< DIM board reset request bit packed in uint32_t
    struct TDimLogRequests log;                                  //!< we can make up to 3 log request

    uint32_t               flat_qspi_board_is_present_packed[3]; //!< DIM present bit packed in uint32_t
    //!< 0 is [1] & [2], DIMS present in the two consecutive cycles
    //!< 1 is DIMS present in actual cycle
    //!< 2 is DIMS present in previous cycle

    uint32_t               flat_qspi_board_is_valid_packed;      //!< DIM board is valid bit packed in uint32_t
    uint32_t               flat_qspi_not_triggered_packed;       //!< not triggered bit packed in uint32_t

    uint32_t               scantime_us[QSPI_BRANCHES];           //!< Scan start times relative to start of second
    uint32_t               prev_scantime_us[QSPI_BRANCHES];      // !<Previous scan times

    uint32_t               unix_time;                            //!< Unix time for current cycle Private
    uint32_t               prev_unix_time;                       //!< Unix time for previous cycle Private
    struct CC_us_time      dim_cycle_start_time;                 //!< Start of DIM cycle
} local;

// Common diagnostic (DIM) data processing
struct TDimLogData
{
    uint16_t    r[4];
};

// 256Kb space buffer allocated

// to handle FGC_MAX_DIMS (20) slots of FGC_LOG_DIM_LEN (1600) elements
// each element has 8 bytes data

// FGC_MAX_DIMS * FGC_LOG_DIM_LEN * sizeof(TDimLogData)
//     20       *    1600         *        8              = 256000
struct TDimLogEntry
{
    struct TDimLogEntryStatus   status;
    struct TDimLogData          data[FGC_LOG_DIM_LEN];
};

struct TDimLogEntry __attribute__((section("sram"))) dim_log[FGC_MAX_DIMS];



// ---------- Internal function declarations

//!

static void LookForTriggerEvent(void);

//!

static void ProcessReceivedQSPIdata(uint16_t ms_mod_20);

//!

static void PutDIMsamplesInLog(uint32_t logical_dim, struct CC_us_time * sample_time, uint32_t * samples);

//!

static void ScaleDimAnalogueData(void);

//!

static void ValidatePresentDimBoards(uint32_t scan_step);

//! Extracts the analogue signal label from the DimDB code and copies to lbl_buf (max len defined in buf_len).
//! If the property sym_idx is for SUB1-SUB8 or SUB1B-SUB8B then the sub converter index will be derived and
//! substituted for any # characters found in the name.  The function returns the length of the label not
//! including the null termination.

static uint16_t DimAnaLabel(char * lbl_buf, uint16_t buf_len, const char * dim_ana_lbl, uint16_t sym_idx);



// External function definition

void InitQspiBus(void)
{
    uint16_t      b;
    uint16_t      ii;

    // initial reset of all DIMS

    // when QSPI_CLK goes HIGH the DIM starts checking the line, if QSPI_CLK remains HIGH at least 15.7us
    // this is recognised as a RESET cmd by the DIM card
    // (for branches A & B simultaneously)
    setBitmap(QSM_CTRL_P, (QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16));

    sleepUs(20);

    // reset done, we release the line to let time for the LOAD cmd
    clrBitmap(QSM_CTRL_P, QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16);
    // when QSPI_CLK goes low the DIM starts checking the line, if QSPI_CLK remains low at least 15.7us
    // this is recognised as a LOAD cmd, so the next DIM register gets ready to by transmitted with the next
    // 256 clk burst

    for (ii = 0; ii < 16; ii++)
    {
        // Can never be returned by a DIM (DIM register ID:3 doesn't exist)
        ((volatile uint16_t *) QSM_TRA_32)[ii] = 0xBAB0 + ii;
        ((volatile uint16_t *) QSM_TRB_32)[ii] = 0xBAB0 + ii;
    }

    // Prepare diag header with sync bytes (6 x 0xFE)

    for (b = 0; b < 2; b++)                             // For double buffer
    {
        diag.term_buf[b].data.data_status = 0x0F;        // Set status in the header to all OK
        diag.term_buf[b].data.class_id    = FGC_CLASS_ID;   // Set class ID in the header

        for (ii = 0; ii < FGC_DIAG_N_SYNC_BYTES; ii++)  // Prepare each sync byte
        {
            diag.term_buf[b].sync[ii] = FGC_DIAG_SYNC;
        }
    }

    memset(&(qspi_misc), 0, sizeof(qspi_misc));

    memset(&(dim_collection.gains),   0.0, sizeof(dim_collection.gains));
    memset(&(dim_collection.offsets), 0.0, sizeof(dim_collection.offsets));
    memset(&(dim_collection.trig_us), 0.0, sizeof(dim_collection.trig_us));

    // flat_qspi_board_number gives the flat qspi addr for the corresponding item in the dim_collection[]
    // address values
    // 0x00..0x1F are real physical DIM boards
    // 0x80, b7=1 means a composite virtual DIM
    // 0xff means empty
    memset(dim_collection.flat_qspi_board_number, 0xFF, FGC_MAX_DIMS);

    // not triggered bit from flat qspi addr packed in uint32_t
    // by default non was triggered
    local.flat_qspi_not_triggered_packed = 0xFFFFFFFF;

    // Clear DIM logs buffer
    memset(dim_log,  0, sizeof(dim_log));
}



void QSPIbusStateMachine(void)
{
    //! The reset process for the diag uses 3 states because there needs to be a delay between the end
    //! of the reset pulse (clock line high) and the start of the scan.
    //!
    //! reset process
    //! a) DIAG_RESET = clock high
    //! b) DIAG_START = clock low
    //! c) DIAG_SCAN  = trigger
    //! d) DIAG_RUN   = readout + next trigger
    //!
    //! for next DIM register only
    //!    DIAG_RUN = readout + next trigger

    // Act according to 20ms time

    switch (ms_task.ms_mod_20)
    {
        case 0:
        {
            // At least 15.7us must passed with QSPI_CLK low after the last read (256 clk burst) or RESET for the LOAD cmd
            // to be recognised, so this acquire with this new 256 clk burst the next DIM register

            local.prev_scantime_us[QSPI_BRANCH_A] = local.scantime_us[QSPI_BRANCH_A];

            // Set READ A bit to fire the state machine which generates the 256 QSPI_CLK to shift
            // the active DIM register in the reception buffer

            setBitmap(QSM_CTRL_P, QSM_CTRL_READA_MASK16);

            local.scantime_us[QSPI_BRANCH_A] = UTC_US_P;

            // used for the trigger
            local.prev_unix_time       = local.unix_time;
            local.unix_time            = ms_task.time_us.secs.abs;
            local.dim_cycle_start_time = ms_task.time_us;

            break;
        }

        case 10:
        {
            // At least 15.7us must passed with QSPI_CLK low after the last read (256 clk burst) for the LOAD cmd
            // to be recognised, so this acquire with this new 256 clk burst the next DIM register

            local.prev_scantime_us[QSPI_BRANCH_B] = local.scantime_us[QSPI_BRANCH_B];

            // Set READ B bit to fire the state machine which generates the 256 QSPI_CLK to shift
            // the active DIM register in the reception buffer

            setBitmap(QSM_CTRL_P, QSM_CTRL_READB_MASK16);

            local.scantime_us[QSPI_BRANCH_B] = UTC_US_P;

            break;
        }

        case 1:   // Dig0        DIM register ID:0  x 16 DIM boards
        case 2:   // Dig1        DIM register ID:1  x 16 DIM boards
        case 3:   // Ana0        DIM register ID:4  x 16 DIM boards
        case 4:   // Ana1        DIM register ID:5  x 16 DIM boards
        case 5:   // Ana2        DIM register ID:6  x 16 DIM boards
        case 6:   // Ana3        DIM register ID:7  x 16 DIM boards
        {
            ProcessReceivedQSPIdata(ms_task.ms_mod_20);

            // Start next scan

            setBitmap(QSM_CTRL_P, QSM_CTRL_READA_MASK16);

            break;
        }

        case 7: //  TrigCounter DIM register ID:2  x16 DIM boards
            ProcessReceivedQSPIdata(ms_task.ms_mod_20);
            break;

        case 11:  // Dig0        DIM register ID:0  x 16 DIM boards
        case 12:  // Dig1        DIM register ID:1  x 16 DIM boards
        case 13:  // Ana0        DIM register ID:4  x 16 DIM boards
        case 14:  // Ana1        DIM register ID:5  x 16 DIM boards
        case 15:  // Ana2        DIM register ID:6  x 16 DIM boards
        case 16:  // Ana3        DIM register ID:7  x 16 DIM boards
        {
            ProcessReceivedQSPIdata(ms_task.ms_mod_20);

            // Start next scan

            setBitmap(QSM_CTRL_P, QSM_CTRL_READB_MASK16);

            break;
        }

        case 17: // TrigCounter DIM register ID:2  x16 DIM boards
            ProcessReceivedQSPIdata(ms_task.ms_mod_20);

            // branch A triggerCounter registers b15 collection
            diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_A_LATCHED_TRIGS]   = diag.triggers_from_ms[ 7];
            // branch B triggerCounter registers b15 collection
            diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_B_LATCHED_TRIGS]   = diag.triggers_from_ms[17];
            // branch A dig0 registers b15 collection
            diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_A_UNLATCHED_TRIGS] = diag.triggers_from_ms[ 1];
            // branch B dig0 registers b15 collection
            diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_B_UNLATCHED_TRIGS] = diag.triggers_from_ms[11];

            LookForTriggerEvent();

            break;

        case 8:
        case 9:
            break;

        case 18:

            // we fire the reset in ms 18 for FGC3 instead of ms 19 like in FGC2 because ...
            // if we fire in ms 19, we have the QSPI_CLK in HIGH during ms 19 and going to LOW in ms 0
            // and in the same ms 0 the first burst read is fire but too close from the start of QSPI_CLK in LOW
            // (less than 15.7us) so this little LOW time is not recognised as LOAD by the DIMS
            // starting reset in ms 18 let going to LOW in ms 19 and remain in LOW until ms 0 fires the read

            if (local.trigger_fired)
            {
                local.trigger_fired = false;
                diag.req_QSPI_reset = 100;                      // Set 2s (100 x 20ms) timeout for DIAG reset
            }

            if (local.reset_req_packed)                // If requests a DIAG reset
            {
                local.reset_req_packed = 0x00000000;   // Clear reset request bits

                diag.req_QSPI_reset = 1;                        // Set immediate DIAG reset request
                diag.sync_resets++;                             // DIAG.SYNC_RESETS, increment diag reset request counter
            }

            if (diag.req_QSPI_reset != 0)                       // If any DIAG reset request is pending
            {
                diag.req_QSPI_reset--;

                if (diag.req_QSPI_reset == 0)                   // If reset request timer expired
                {
                    // 1st RESET state
                    // Set QSPI_CLK high, start of RESET signal
                    // when QSPI_CLK goes HIGH the DIM starts checking the line, if QSPI_CLK remains HIGH at least 15.7us
                    // this is recognised as a RESET cmd by the DIM card
                    // (for branches A & B simultaneously)
                    setBitmap(QSM_CTRL_P, (QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16));
                    // The 2nd step is done in millisecond 19

                    // when QSPI_CLK goes HIGH the DIM starts checking the line, if QSPI_CLK remains HIGH at least 15.7us
                    // this is recognised as a RESET cmd by the DIM card
                }
            }

            diagCheckDimWarnings();

            break;

        case 19:
            if (testBitmap(QSM_CTRL_P, (QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16)) == true)
            {
                // 2nd RESET state (the 1st step was fired in millisecond 18)
                // Set QSPI_CLK low, generates the fall edge and the low level for the needed RESET signal
                // (for bus A & B simultaneously)

                clrBitmap(QSM_CTRL_P, (QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16));
            }

            ScaleDimAnalogueData();             // Process and log raw values

            diagUpdateMeas();

            ValidatePresentDimBoards(1);

            diag.dim_time = local.dim_cycle_start_time;

            break;
    }
}



uint16_t DimGetDimNames(struct cmd * c, struct prop * p)
{
    prop_size_t                 n;
    uint16_t                    errnum;                 // Error number
    struct TAccessToDimInfo  *  dim_info_ptr;
    struct prop              *  dim_props;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    while (n--)    // For all elements
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)                             // Print index if required
        {
            return (errnum);
        }

        dim_props = &diag.dim_props[c->from]; // [logical dim]
        dim_info_ptr = (struct TAccessToDimInfo *)dim_props->value;

        fprintf(c->f,   "%s:%u:0x%02X",
                SysDbDimName(device.sys.sys_idx, dim_info_ptr->logical_dim),
                dim_info_ptr->logical_dim,
                dim_info_ptr->flat_qspi_board_number);

        c->from += c->step;     // Next element
    }

    return (0);
}



uint16_t DimGetAnaLbls(struct cmd * c, struct prop * p)
{
    prop_size_t  n;
    uint16_t     offset;
    uint16_t     errnum;                         // Error number
    char         ana_lbl[FGC_LOG_SIG_LABEL_LEN + 1];

    if ((c->n_arr_spec == 2)
        && (c->to >= FGC_N_DIM_ANA_CHANS)
       )
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    p->n_elements = FGC_N_DIM_ANA_CHANS;        // Set property size to number of analog channels

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    p->n_elements = p->max_elements;

    offset = FGC_MAX_DIM_BUS_ADDR * c->from;

    fgc_db_t const dim_type_idx = SysDbDimDbType(device.sys.sys_idx, ((struct TAccessToDimInfo *)p->value)->logical_dim);

    while (n--)                                 // For all elements
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)             // Print index if required
        {
            return (errnum);
        }

        DimAnaLabel(ana_lbl, (FGC_LOG_SIG_LABEL_LEN + 1), DimDbAnalogLabel(dim_type_idx,c->from), p->sym_idx);

        fprintf(c->f, "0x%02X:%s:%s", offset, (char *)ana_lbl,  DimDbAnalogUnits(dim_type_idx,c->from));

        offset += (c->step * FGC_MAX_DIM_BUS_ADDR);
        c->from += c->step;
    }

    return (0);
}



uint16_t DimGetDigLbls(struct cmd * c, struct prop * p, uint16_t bank)
{
    prop_size_t  n;
    uint16_t     errnum;                 // Error number

    if ((c->n_arr_spec == 2)
        && (c->to >= FGC_N_DIM_DIG_INPUTS)
       )
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    p->n_elements = FGC_N_DIM_DIG_INPUTS;       // Set property size to number of digital channels

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    p->n_elements = p->max_elements;

    fgc_db_t const dim_type_idx =  SysDbDimDbType(device.sys.sys_idx, ((struct TAccessToDimInfo *)p->value)->logical_dim);

    while (n--)                                 // For all elements
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)             // Print index if required
        {
            return (errnum);
        }

        fprintf(c->f, "0x%04X:%c:%s:%s:%s",
                (1 << c->from),
                (DimDbDigitalIsFault(dim_type_idx,bank+4,c->from) ? 'T' : 'S'), // "TRG" "STA"
                DimDbDigitalLabel(dim_type_idx,bank+4,c->from),
                DimDbDigitalLabelOne(dim_type_idx,bank+4,c->from),
                DimDbDigitalLabelZero(dim_type_idx,bank+4,c->from));

        c->from += c->step;
    }

    return (0);
}



uint16_t DimGetAna(struct cmd * c, struct prop * p)
{
    prop_size_t                 n;                      // Number of element in array range
    float                       ana_val;
    float                    *  ana_val_ptr;            // Analogue value pointer
    uint16_t                    errnum;                 // Error number
    char                        ana_lbl[FGC_LOG_SIG_LABEL_LEN + 1];
    struct TAccessToDimInfo  *  dim_info_ptr;


    if (c->n_arr_spec == 2 && c->to >= FGC_MAX_DIMS)
    {
        return (FGC_BAD_ARRAY_IDX);
    }

    c->getopts |= GET_OPT_NOIDX;              // Suppress automatic indexes
    dim_info_ptr = (struct TAccessToDimInfo *)p->value;// Get DIM info structure for this DIM
    fgc_db_t const dim_type_idx = SysDbDimDbType(device.sys.sys_idx, dim_info_ptr->logical_dim);

    p->n_elements = FGC_N_DIM_ANA_CHANS;        // Set number of channels

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    p->n_elements = p->max_elements;

    while (n--)                                 // For all elements in the array range
    {

        if (DimDbIsAnalogChannel(dim_type_idx,c->from))  // If input is in use
        {
            errnum = PropValueGet(c, &PROP_DIAG_ANASIGS, NULL,
                                  dim_info_ptr->logical_dim + FGC_MAX_DIMS * c->from, (uint8_t **)&ana_val_ptr);

            if (errnum)
            {
                return (errnum);
            }

            ana_val = *ana_val_ptr;

            DimAnaLabel(ana_lbl, (FGC_LOG_SIG_LABEL_LEN + 1), DimDbAnalogLabel(dim_type_idx, c->from), p->sym_idx);

            errnum = CmdPrintIdx(c, c->from);

            if (errnum)
            {
                return (errnum);
            }

            fprintf(c->f, "%u:%-15s:%10.3E:%s", (unsigned int) c->from, (char *)ana_lbl, (double) ana_val, DimDbAnalogUnits(dim_type_idx,c->from));
        }

        c->from += c->step;
    }

    return (0);
}



uint16_t DimGetDig(struct cmd * c, struct prop * p)
{
    prop_size_t               n;
    uint16_t                  bit_state;
    uint16_t                  errnum;
    fgc_db_t                  flat_board_number;
    fgc_db_t                  bank;               // 0 refers to digital_0 and 1 refers to digital_1
    fgc_db_t                  bit_in_bank;
    uint16_t                  bit_mask;
    bool                      trigger_is_latched;
    bool                      unlatched_trig;
    static char             * str_TRG_STA[] = { "FLT", "STA" };
    struct TAccessToDimInfo * dim_info_ptr;

    c->getopts |= GET_OPT_NOIDX;                              // Suppress indexes

    dim_info_ptr = (struct TAccessToDimInfo *)p->value;
    fgc_db_t const dim_type_idx = SysDbDimDbType(device.sys.sys_idx, dim_info_ptr->logical_dim);
    flat_board_number = SysDbDimBusAddress(device.sys.sys_idx, dim_info_ptr->logical_dim);

    if (flat_board_number & 0x80) // If DIM is a composite (ANA only)
    {
        return (0);          // nothing to do
    }

      errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    while (n--) // For all elements
    {
        // pass from [0..23] to [bank][0..11]
        // digital signals 0..11 comes from digital_0 and signals 12..23 comes from digital_1

        // select digital_0 or digital_1 based on the signal number [0..23]
        bank = (c->from / FGC_N_DIM_DIG_INPUTS);

        // this refers to which of the 12 digital signals (bits) in the bank we are talking about
        bit_in_bank    = (c->from % FGC_N_DIM_DIG_INPUTS);


        if (!DimDbIsDigitalInput(dim_type_idx, bank+4, bit_in_bank))     // If input is not used
        {
            c->from += c->step; // Skip to next element
            continue;
        }

        bit_mask  = 0x0001 << bit_in_bank;
        bit_state = diag.data.a.digital[bank][flat_board_number] & bit_mask;

        trigger_is_latched = (diag.data.a.latched[bank][flat_board_number] & bit_mask) != 0;
        unlatched_trig = (diag.data.f.analog_0[flat_board_number] & 0x8000) != 0;


        errnum = CmdPrintIdx(c, c->from);                       // Field delimiter

        if (errnum)
        {
            return (errnum);
        }

        fprintf(c->f, "%2u:%s|%-31s:%-7s:%2s:%2s",
                (unsigned int) c->from,
                str_TRG_STA[!(DimDbDigitalIsFault(dim_type_idx, bank+4, bit_in_bank))],
                DimDbDigitalLabel(dim_type_idx, bank+4, bit_in_bank),
                (bit_state      ?  DimDbDigitalLabelOne(dim_type_idx, bank+4, bit_in_bank) :
                                   DimDbDigitalLabelZero(dim_type_idx, bank+4, bit_in_bank)),
                DimDbDigitalIsFault(dim_type_idx, bank+4, bit_in_bank)?(unlatched_trig ? "UT" : ""):"",
                DimDbDigitalIsFault(dim_type_idx, bank+4, bit_in_bank)?(trigger_is_latched ? "LT" : ""):"");

        // Go to next element

        c->from += c->step;
    }

    return (0);
}



uint16_t DimGetFaults(struct cmd * c, struct prop * p)
{
    prop_size_t                 n;
    uint16_t                    idx;
    uint16_t                    errnum;
    fgc_db_t                    flat_board_number;
    fgc_db_t                    bank; // 0 refers to digital_0 and 1 refers to digital_1
    fgc_db_t                    dim_type_idx;
    fgc_db_t                    bit_in_bank;
    uint16_t                    bit_mask;
    bool                        trigger_is_latched;
    bool                        unlatched_trig;
    struct TAccessToDimInfo  *  dim_info_ptr;
    struct prop              *  dim_props;

    if (c->step > 1)                            // If array step is not 1
    {
        return (FGC_BAD_ARRAY_IDX);                     // Report bad array index
    }

    idx = 0;            // for CmdPrintIdx

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    dim_props = &diag.dim_props[c->from]; // [logical dim]

    while (n--)                                  // For all elements
    {
        dim_info_ptr = (struct TAccessToDimInfo *) dim_props->value;
        dim_type_idx = SysDbDimDbType(device.sys.sys_idx, dim_info_ptr->logical_dim);
        flat_board_number = SysDbDimBusAddress(device.sys.sys_idx, dim_info_ptr->logical_dim);

        if (flat_board_number & 0x80) // If DIM is a composite (ANA only)
        {
            continue;                 // Skip this DIM
        }

        unlatched_trig = (diag.data.f.analog_0[flat_board_number] & 0x8000) != 0;

        // 0 refers to digital_0 and 1 refers to digital_1
        // digital signals 0..11 comes from digital_0 and signals 12..23 comes from digital_1
        for (bank = 0; bank < FGC_N_DIM_DIG_BANKS; bank++)
        {
            bit_mask = 0x0001;

            for (bit_in_bank = 0; bit_in_bank < FGC_N_DIM_DIG_INPUTS; bit_in_bank++, bit_mask <<= 1)
            {

                if (!DimDbIsDigitalInput(dim_type_idx, bank+4, bit_in_bank)    // If input is not used, or
                    || !(DimDbDigitalIsFault(dim_type_idx, bank+4, bit_in_bank))
                    || !(diag.data.a.digital[bank][flat_board_number] & bit_mask)   // input is not active
                   )
                {
                    continue;    // Skip to next
                }

                trigger_is_latched = (diag.data.a.latched[bank][flat_board_number] & bit_mask) != 0;

                errnum = CmdPrintIdx(c, idx++);           // Field delimiter

                if (errnum)
                {
                    return (errnum);
                }

                fprintf(c->f, "%-6s:%-31s:%-7s:%2s:%2s",
                        SYM_TAB_PROP[dim_props->sym_idx].key.c,
                        DimDbDigitalLabel(dim_type_idx, bank+4, bit_in_bank),
                        DimDbDigitalLabelOne(dim_type_idx, bank+4, bit_in_bank),
                        (unlatched_trig ? "UT" : ""),
                        (trigger_is_latched ? "LT" : ""));
            }
        }

        dim_props++;
    }

    return (0);
}



void DimResetExpectedDetectedMismatches(void)
{
    statusClrLatched(FGC_LAT_DIMS_EXP_FLT);
    clrBitmap(ST_LATCHED, FGC_LAT_DIMS_EXP_FLT);
    qspi_misc.expected_detected_mismatches = 0;
}



void DimGetCycleStartTime(struct CC_ms_time * cycle_start_time)
{
    // Generate cycle start timestamp by taking the microsecond scan time and decimating it down to milliseconds

    cycle_start_time->secs.abs = local.unix_time;
    cycle_start_time->ms = local.scantime_us[QSPI_BRANCH_A] / 1000;
}


// ---------- Internal function definitions

static uint16_t
DimAnaLabel(char *lbl_buf, uint16_t buf_len, const char *dimdb_ana_lbl, uint16_t sym_idx)
{
    char        ch;
    int8_t       sub_convt_idx;
    uint16_t      n_ch;

    if (!dimdb_ana_lbl)                          // If no label present
    {
        *lbl_buf = '\0';                                // Return empty string
        return (0);
    }

    n_ch  = 0;
    sub_convt_idx = '-';

    // if (sym_idx >= STP_SUB1 && sym_idx <= STP_SUB8B)    // If DIM is in a sub converter
    // {
    //     sub_convt_idx = '1' + (int8_t)((sym_idx - STP_SUB1) / 2); // Calc sub converter index
    // }

    do                                          // Copy string until buffer full
    {
        ch = *dimdb_ana_lbl++;

        if (ch == '#')                                  // If name includes '#'
        {
            ch = sub_convt_idx;                                 // Substitute sub converter index
        }

        *(lbl_buf++) = ch;

        if (ch)
        {
            n_ch++;
        }
    }
    while (ch && n_ch < buf_len);

    return (n_ch);
}



static void ProcessReceivedQSPIdata(uint16_t ms_mod_20)
{
    struct THandyData
    {
        uint8_t       branch; // 0= branch A, 1= branch B, 2=skip
        uint16_t   *  place_ptr;
    };

    // the 7 DIM registers arrive in this order
    //     0 - Dig0        DIM register ID:0
    //     1 - Dig1        DIM register ID:1
    //     2 - Ana0        DIM register ID:4
    //     3 - Ana1        DIM register ID:5
    //     4 - Ana2        DIM register ID:6
    //     5 - Ana3        DIM register ID:7
    //     6 - TrigCounter DIM register ID:2
    //                      there is no ID:3

    static const struct THandyData ms_to_store_place[20] =              // ms     branch A
    {
        { 2,        NULL                                              },//  0
        { QSPI_BRANCH_A, &(diag.data.r.digital_0[QSPI_BRANCH_A][0])   },//  1       Dig0        DIM register ID:0  x16 DIM boards
        { QSPI_BRANCH_A, &(diag.data.r.digital_1[QSPI_BRANCH_A][0])   },//  2       Dig1        DIM register ID:1  x16 DIM boards
        { QSPI_BRANCH_A, &(diag.data.r.analog_0[QSPI_BRANCH_A][0])    },//  3       Ana0        DIM register ID:4  x16 DIM boards
        { QSPI_BRANCH_A, &(diag.data.r.analog_1[QSPI_BRANCH_A][0])    },//  4       Ana1        DIM register ID:5  x16 DIM boards
        { QSPI_BRANCH_A, &(diag.data.r.analog_2[QSPI_BRANCH_A][0])    },//  5       Ana2        DIM register ID:6  x16 DIM boards
        { QSPI_BRANCH_A, &(diag.data.r.analog_3[QSPI_BRANCH_A][0])    },//  6       Ana3        DIM register ID:7  x16 DIM boards
        { QSPI_BRANCH_A, &(diag.data.r.trigCounter[QSPI_BRANCH_A][0]) },//  7       TrigCounter DIM register ID:2  x16 DIM boards
        { 2,        NULL                                              },//  8
        { 2,        NULL                                              },//  9  branch B
        { 2,        NULL                                              },// 10
        { QSPI_BRANCH_B, &(diag.data.r.digital_0[QSPI_BRANCH_B][0])   },// 11       Dig0        DIM register ID:0  x16 DIM boards
        { QSPI_BRANCH_B, &(diag.data.r.digital_1[QSPI_BRANCH_B][0])   },// 12       Dig1        DIM register ID:1  x16 DIM boards
        { QSPI_BRANCH_B, &(diag.data.r.analog_0[QSPI_BRANCH_B][0])    },// 13       Ana0        DIM register ID:4  x16 DIM boards
        { QSPI_BRANCH_B, &(diag.data.r.analog_1[QSPI_BRANCH_B][0])    },// 14       Ana1        DIM register ID:5  x16 DIM boards
        { QSPI_BRANCH_B, &(diag.data.r.analog_2[QSPI_BRANCH_B][0])    },// 15       Ana2        DIM register ID:6  x16 DIM boards
        { QSPI_BRANCH_B, &(diag.data.r.analog_3[QSPI_BRANCH_B][0])    },// 16       Ana3        DIM register ID:7  x16 DIM boards
        { QSPI_BRANCH_B, &(diag.data.r.trigCounter[QSPI_BRANCH_B][0]) },// 17       TrigCounter DIM register ID:2  x16 DIM boards
        { 2,        NULL                                              },// 18
        { 2,        NULL                                              } // 19
    };

    struct THandyData       idx;
    uint16_t                ii;
    uint16_t                triggers;

    idx = ms_to_store_place[ms_mod_20];

    // copy the received data from the 16 DIM boards to the buffer
    if (idx.branch == QSPI_BRANCH_A)
    {
        memcpy(idx.place_ptr, (void *) QSM_RRA_32, 16 * sizeof(uint16_t));
    }
    else
    {
        if (idx.branch == QSPI_BRANCH_B)
        {
            memcpy(idx.place_ptr, (void *) QSM_RRB_32, 16 * sizeof(uint16_t));
        }
        else
        {
            // just to be safe and not to copy when place_ptr is NULL
        }
    }

    // also collects the trigger bit (b15) from every register

    // prepare the triggers in a single word to be returned
    // insert trigger bit from left to right
    // triggers b15=DIM15...b1=DIM1, b0=DIM0

    triggers = 0;

    for (ii = 0; ii < 16; ii++)
    {
        triggers = triggers >> 1;

        setBitmap(triggers, (idx.place_ptr[ii] & 0x8000));
    }

    diag.triggers_from_ms[ms_mod_20] = triggers;
}



static void LookForTriggerEvent(void)
{
    // Analyse the trigger data from the DIMs
    // and identify the trigger time in the event that a trigger input has been activated.
    //
    // Inputs to the processing are:
    //
    //      local.flat_qspi_board_is_valid_packed
    //      local.flat_qspi_not_triggered_packed
    //      qspi_misc.secs.abs                                            Unix time for current cycle
    //      qspi_misc.scantime_us                                          us scan times from current cycle
    //      qspi_misc.prev_scantime_us                                     us scan times from previous cycle
    //
    // And the outputs are:
    //
    //      dim_collection.trig_s[]                                 DIM trigger times (Unixtime/us time)
    //      dim_collection.trig_us[]
    //      dim_collection.evt_log_state[]                          DIM event log state (NONE,WATCH,TRIG)
    //
    //
    // The MCU will pack the the unlatched and latched bits on ms 17 of 20 so this function is run
    // on ms 1 of 20 of the following cycle.
    //
    // Setting the dim_collection.evt_log_state[] to DIM_EVT_TRIG is the trigger for
    // the MCU to write the active fault(s) and all status bits for the triggered DIM to the event log.
    // One second later it will reset the bus to clear the latch and allow additional faults to be logged.
    // This is signalled by the DIM_EVT_WATCH state.

    uint32_t              logical_dim;
    uint32_t              qspi_branch;
    uint32_t              flat_board_number;
    uint32_t              bit_in_packed;
    uint32_t              dim_board_has_latched_trig;
    uint32_t              dim_board_has_UNlatched_trig;
    uint32_t              at_least_one_UNlatched_trig;
    uint32_t              trig_time_us;                   // Trigger time since start of second in us
    uint32_t              trig_time_s;                    // Trigger time in seconds (unix_time)
    union TUnion32Bits  tmp;

    at_least_one_UNlatched_trig = false;

    // scan the DIM list
    for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        flat_board_number = dim_collection.flat_qspi_board_number[logical_dim];

        if (flat_board_number < FGC_MAX_DIM_BUS_ADDR)      // If it is a physical DIM board
        {
            // bit position corresponding to this board, when packed in uint32_t
            bit_in_packed = 0x00000001 << flat_board_number;

            // process only valid boards
            if (testBitmap(bit_in_packed, local.flat_qspi_board_is_valid_packed) == true)
            {
                tmp.part.lo = diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_A_UNLATCHED_TRIGS];
                tmp.part.hi = diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_B_UNLATCHED_TRIGS];

                dim_board_has_UNlatched_trig = bit_in_packed & tmp.int32u;

                tmp.part.lo = diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_A_LATCHED_TRIGS];
                tmp.part.hi = diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_B_LATCHED_TRIGS];

                dim_board_has_latched_trig = bit_in_packed & tmp.int32u;

                if (dim_board_has_UNlatched_trig)
                {
                    at_least_one_UNlatched_trig = true;
                }

                if (dim_board_has_latched_trig)
                {
                    // check if it is the 1st detection
                    if (testBitmap(bit_in_packed, local.flat_qspi_not_triggered_packed) == true)
                    {
                        // get branch 0=A,1=B
                        // flat [0..15] belongs to A, [16..31] belongs to B
                        qspi_branch = flat_board_number / (FGC_MAX_DIM_BUS_ADDR / 2);

                        // clear bit to mark this DIM board as triggered
                        clrBitmap(local.flat_qspi_not_triggered_packed, bit_in_packed);

                        // The register trigCounter of DIM board has its 8us clock count

                        trig_time_us = 8 * (diag.data.f.trigCounter[flat_board_number] & 0x0FFF);

                        if (dim_board_has_UNlatched_trig)
                        {
                            trig_time_us += local.prev_scantime_us[qspi_branch];

                            if (trig_time_us < 1000000)   // < 1 second
                            {
                                trig_time_s = local.prev_unix_time;
                            }
                            else
                            {
                                trig_time_s   = local.unix_time;
                                trig_time_us -= 1000000;
                            }
                        }
                        else
                        {
                            trig_time_us += local.scantime_us[qspi_branch];
                            trig_time_s   = local.unix_time;
                        }

                        dim_collection.trig_s[logical_dim] = trig_time_s;
                        dim_collection.trig_us[logical_dim] = trig_time_us;

                        dim_collection.evt_log_state[logical_dim] = DIM_EVT_TRIG;
                        local.trigger_fired = true;
                    }
                }
                else
                {
                    dim_collection.evt_log_state[logical_dim] = (dim_board_has_UNlatched_trig ? DIM_EVT_WATCH : DIM_EVT_NONE);
                    // set bit to mark DIM as not triggered
                    setBitmap(local.flat_qspi_not_triggered_packed, bit_in_packed);
                }
            }
            else
            {
                // Board not valid

                dim_collection.evt_log_state[logical_dim] = DIM_EVT_NONE;
                // set bit to mark DIM as not triggered
                setBitmap(local.flat_qspi_not_triggered_packed, bit_in_packed);
            }

        } // If it is a physical DIM board

    } // end for

    dim_collection.unlatched_trigger_fired = at_least_one_UNlatched_trig;
}



static void ScaleDimAnalogueData(void)
{
    // This function is called from QSPIbusStateMachine() on ms 19.
    // It converts the raw analogue diagnostic values to calibrated floating point values in
    // the DIAG.ANASIGS property and logs the raw signals when the logging for a DIM is active.
    //
    // A composite DIM is a virtual DIM that combines the same analogue register [analogue_0..analogue_3]
    // from up to four physical DIMs
    //
    // For composite DIMs the flat_qspi_board_number defines
    //     b7     : composite
    //     b6,b5  : analogue register to use [analogue_0..analogue_3]
    //     b4..b0 : first DIM in list to use (the follow consecutive DIMS in the list will be automatically used)
    //
    //
    //        7     Bus Address     0
    //       +--+--+--+--+--+--+--+--+
    //       |CF|a1|a0|d4|d3|d2|d1|d0|
    //       +--+--+--+--+--+--+--+--+
    //
    //       CF    = Composite flag (0x80)
    //       a1:a0 = Analogue input (0-3)
    //       d4:d0 = DIM channel for first DIM to combine (0-18)

    uint32_t            logical_dim;
    uint32_t            composite_list_idx;
    uint32_t            flat_board_number;
    uint32_t            ana_reg;                        // Analogue Channel index (0-3)
    uint32_t            composite_ana_reg;
    uint32_t            bit_in_packed;
    uint32_t            latched_triggers_packed;
    bool                freeze_all_dim_logs;
    bool                relaunch_dim_logging;
    uint32_t            analogue_value[FGC_N_DIM_ANA_CHANS];// up to 4 values to compose
    struct CC_us_time  sample_time;                    // Sample time (for logging) rounded to 20ms
    union TUnion32Bits  tmp;
    struct CC_ms_time  time_now_ms;                    // Time now in UXTIME and MS

    tmp.part.lo = diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_A_LATCHED_TRIGS];
    tmp.part.hi = diag.data.r.software[ISR_RUN_TIME][QSPI_BRANCH_B_LATCHED_TRIGS];

    // Latched triggered for DIMs that are present and valid
    latched_triggers_packed = tmp.int32u & local.flat_qspi_board_is_valid_packed;

    freeze_all_dim_logs  = qspi_misc.freeze_all_dim_logs;
    relaunch_dim_logging = qspi_misc.relaunch_dim_logging;

    time_now_ms.secs.abs = timeGetUtcTime();
    time_now_ms.ms   = timeGetUtcTimeMs();
    timeCopyFromMsToUs(&time_now_ms, &sample_time);

    // ToDo check this (as it was imported form DSP)
    //  sample_time.us -= 19000;// This function is called on ms 19/20 so round down to start of 20ms

    // process the DIMs in the list
    for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        flat_board_number = dim_collection.flat_qspi_board_number[logical_dim];

        if (flat_board_number == 0xFF)     // If DIM is unused
        {
            continue;
        }

        if (flat_board_number & 0x80)              // if DIM is a composite
        {
            bit_in_packed = 0; // no bit in the packed bits to use, since composite DIM is not physical

            composite_list_idx = (flat_board_number & 0x1F);               // list number of first DIM to combine

            composite_ana_reg = (flat_board_number >> 5) & 0x03;   // analogue register to use

            // the 4 analogue register of this composite DIM are taken from the same
            // composite_ana_reg of 4 consecutive (in the list) DIM boards
            for (ana_reg = 0; ana_reg < FGC_N_DIM_ANA_CHANS; ana_reg++)
            {
                flat_board_number = dim_collection.flat_qspi_board_number[composite_list_idx];

                composite_list_idx++;

                if (flat_board_number < FGC_MAX_DIM_BUS_ADDR)
                {
                    analogue_value[ana_reg] = diag.data.a.analog[composite_ana_reg][flat_board_number] & 0x0FFF;

                    dim_collection.analogue_in_physical[ana_reg][logical_dim] =
                        dim_collection.offsets[ana_reg][logical_dim] +
                        dim_collection.gains[ana_reg][logical_dim] * (float)(analogue_value[ana_reg]);
                }
            }

            if (dim_log[logical_dim].status.samples_to_acq)           // If logging active
            {
                PutDIMsamplesInLog(logical_dim, &sample_time, &analogue_value[0]);                   // Log sample
            }

            if (!dim_log[logical_dim].status.dont_log)              // If logging is active
            {
                if (freeze_all_dim_logs ||
                    (latched_triggers_packed & (0x00000001 <<
                                                dim_collection.flat_qspi_board_number[FGC_COMP_DIM_TRIG_IDX]))   // If DIM VS latched
                   )
                {
                    dim_log[logical_dim].status.dont_log = true; // Stop log for this composite DIM
                }
            }
            else                                                // else logging is not active
            {
                if (relaunch_dim_logging)
                {
                    dim_log[logical_dim].status.dont_log = false;
                    PutDIMsamplesInLog(logical_dim, &sample_time, 0);  // Log NULL sample to mark the start of logging
                }
            }
        }
        else // not composite DIM
        {
            if (flat_board_number < FGC_MAX_DIM_BUS_ADDR)
            {
                // bit position corresponding to this board, when packed in uint32_t
                bit_in_packed = 0x00000001 << flat_board_number;

                if (bit_in_packed & local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES])
                {
                    // log and process the 4 analogue registers of this dim
                    for (ana_reg = 0; ana_reg < FGC_N_DIM_ANA_CHANS; ana_reg++)
                    {
                        analogue_value[ana_reg] = diag.data.a.analog[ana_reg][flat_board_number] & 0x0FFF;

                        dim_collection.analogue_in_physical[ana_reg][logical_dim] =
                            dim_collection.offsets[ana_reg][logical_dim] +
                            dim_collection.gains[ana_reg][logical_dim] * (float)(analogue_value[ana_reg]);
                    }

                    if (dim_log[logical_dim].status.samples_to_acq)         // If logging active
                    {
                        PutDIMsamplesInLog(logical_dim, &sample_time, &analogue_value[0]);
                    }
                }

                if (!dim_log[logical_dim].status.dont_log)                  // If logging is active
                {
                    if (freeze_all_dim_logs
                        || (latched_triggers_packed & bit_in_packed)
                       )
                    {
                        dim_log[logical_dim].status.dont_log = true;        // Stop logging for this DIM
                    }
                }
                else                                                        // else logging is stopped
                {
                    if (relaunch_dim_logging
                        && (local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] & bit_in_packed)
                       )
                    {
                        dim_log[logical_dim].status.dont_log = false;       // Start logging for this DIM
                        PutDIMsamplesInLog(logical_dim, &sample_time, 0);   // Log NULL sample to mark
                    }                                                       // the start of logging
                }
            }
        }

        if (!dim_log[logical_dim].status.dont_log)                  // If logging is active
        {
            dim_log[logical_dim].status.samples_to_acq = FGC_LOG_DIM_LEN / 2; // Set 50% post trig samples
        }
    }

    //  if ( relaunch_dim_logging )
    //  {
    //      dsp_debug.stop_log = false;
    //      dsp_debug.samples_to_acq = DIAG_DEBUG_TO_ACQ;
    //  }

    qspi_misc.freeze_all_dim_logs  = false;
    qspi_misc.relaunch_dim_logging = false;
}



static void ValidatePresentDimBoards(uint32_t scan_step)
{
    // This function is called from QSPIbusStateMachine() on millisecond 19.
    // It must check to see how many DIMs are present on each bus and check for DIM sync faults.
    // If the number of DIMs detected is not the number expected, it sets DIAG_FLT in the ST_LATCHED status.
    //
    // The QSPI bus has 2 branches [A,B] with up to 16 DIM boards connected to each branch,
    // each DIM card has 7 registers (of 16 bits)

    uint8_t   flat_board_number;
    uint32_t  bit_in_packed;
    uint32_t  present_boards_packed[QSPI_BRANCHES];
    uint8_t   ana_regs_id[4];         // ID of DIM registers [analog_0..3 + trigCounter]
    uint8_t   boards_in_qspi[QSPI_BRANCHES];

    //    the 7 registers are received in this order
    //        0 - Dig0        DIM register ID:0       NEXT ID -> 1
    //        1 - Dig1        DIM register ID:1                  4
    //        2 - Ana0        DIM register ID:4                  5
    //        3 - Ana1        DIM register ID:5                  6
    //        4 - Ana2        DIM register ID:6                  7
    //        5 - Ana3        DIM register ID:7                  2
    //        6 - TrigCounter DIM register ID:2                  0
    //                         there is no ID:3                  x

    //   sort by ID weight
    //        0 - Dig0        DIM register ID:0       NEXT ID -> 1
    //        1 - Dig1        DIM register ID:1                  4
    //        6 - TrigCounter DIM register ID:2                  0
    //                         there is no ID:3                  x use 8 as invalid, 3 can't be used as fails with the test pattern BABx
    //        2 - Ana0        DIM register ID:4                  5
    //        3 - Ana1        DIM register ID:5                  6
    //        4 - Ana2        DIM register ID:6                  7
    //        5 - Ana3        DIM register ID:7                  2

    static uint8_t NEXT_ID[] = { 1, 4, 0, 8, 5, 6, 7, 2 };


    // Move DIM present history
    local.flat_qspi_board_is_present_packed[DIMS_IN_PREVIOUS_CYCLE] =
        local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE];

    local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE] = 0x00000000;
    local.flat_qspi_board_is_valid_packed = 0x00000000;

    flat_board_number = 0;

    while (flat_board_number < FGC_MAX_DIM_BUS_ADDR)
    {
        // Extract register ID number from top nibble

        ana_regs_id[0] = (diag.data.f.analog_0[flat_board_number] >> 12) & 0x00000007;
        ana_regs_id[1] = (diag.data.f.analog_1[flat_board_number] >> 12) & 0x00000007;
        ana_regs_id[2] = (diag.data.f.analog_2[flat_board_number] >> 12) & 0x00000007;
        ana_regs_id[3] = (diag.data.f.analog_3[flat_board_number] >> 12) & 0x00000007;

        // If the analogue registers IDs are ok we assume that a DIM board is detected
        if (   ana_regs_id[1] == NEXT_ID[ana_regs_id[0]]
            && ana_regs_id[2] == NEXT_ID[ana_regs_id[1]]
            && ana_regs_id[3] == NEXT_ID[ana_regs_id[2]])
        {
            // bit position corresponding to this board, when packed in uint32_t
            bit_in_packed = 0x00000001 << flat_board_number;

            // Mark DIM as present on this cycle
            setBitmap(local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE], bit_in_packed);

            // If was present in previous cycle
            if (testBitmap(local.flat_qspi_board_is_present_packed[DIMS_IN_PREVIOUS_CYCLE], bit_in_packed) == true)
            {
                // if ID of trigCounter is also correctly 2
                if ((diag.data.f.trigCounter[flat_board_number] & 0x7000) == 0x2000)
                {
                    setBitmap(local.flat_qspi_board_is_valid_packed, bit_in_packed);
                }
                else // channel is not correct
                {
                    setBitmap(local.reset_req_packed, bit_in_packed); // Request a DIAG reset
                    qspi_misc.flat_qspi_non_valid_counts[flat_board_number]++;
                }
            }
        }

        flat_board_number += scan_step;
    }

    if (scan_step == 1)         // not class 59
    {
        // a DIM must be seen on two cycles to be present
        local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] =   local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE]
                                                                       & local.flat_qspi_board_is_present_packed[DIMS_IN_PREVIOUS_CYCLE];

        present_boards_packed[QSPI_BRANCH_B] = (local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES] >> 16);
        present_boards_packed[QSPI_BRANCH_A] = (local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES]) & 0x0000FFFF;

        // Counting the boards

        boards_in_qspi[QSPI_BRANCH_A] = boards_in_qspi[QSPI_BRANCH_B] = 0;

        while (present_boards_packed[QSPI_BRANCH_B] | present_boards_packed[QSPI_BRANCH_A])   // While bits still left to count
        {
            boards_in_qspi[QSPI_BRANCH_A] += (present_boards_packed[QSPI_BRANCH_A] & 0x00000001);
            boards_in_qspi[QSPI_BRANCH_B] += (present_boards_packed[QSPI_BRANCH_B] & 0x00000001);

            present_boards_packed[QSPI_BRANCH_A] >>= 1; // next board
            present_boards_packed[QSPI_BRANCH_B] >>= 1; // next board
        }

        qspi_misc.detected[QSPI_BRANCH_A] = boards_in_qspi[QSPI_BRANCH_A];
        qspi_misc.detected[QSPI_BRANCH_B] = boards_in_qspi[QSPI_BRANCH_B];

        // Check for number of DIMs detected not equal to number expected

        if (   qspi_misc.expected_retrieved == true
            && (   qspi_misc.detected[QSPI_BRANCH_A] != qspi_misc.expected[QSPI_BRANCH_A]
                || qspi_misc.detected[QSPI_BRANCH_B] != qspi_misc.expected[QSPI_BRANCH_B]))
        {
            qspi_misc.expected_detected_mismatches++; // this one can overflow
        }

        dim_collection.dims_are_valid =    (qspi_misc.detected[QSPI_BRANCH_A] == qspi_misc.expected[QSPI_BRANCH_A])
                                        && (qspi_misc.detected[QSPI_BRANCH_B] == qspi_misc.expected[QSPI_BRANCH_B])
                                        && (local.flat_qspi_board_is_present_packed[DIMS_IN_BOTH_CYCLES]
                                            == local.flat_qspi_board_is_valid_packed);
    }

    // if ( dsp_debug.samples_to_acq )
    // {
    //     // First DIM on bus A

    //     // register analog_0 of DIM board, flat[ 1] = A[1]
    //     // register analog_1 of DIM board, flat[ 1] = A[1]
    //     SaveInDspDebugBuffer( (  diag.data.f.analog_0[1] << 16) | diag.data.f.analog_1[1]);

    //     // register analog_2 of DIM board, flat[ 1] = A[1]
    //     // register analog_3 of DIM board, flat[ 1] = A[1]
    //     SaveInDspDebugBuffer( (  diag.data.f.analog_2[1] << 16) | diag.data.f.analog_3[1]);

    //     // register trigCounter of DIM board, flat[ 1] = A[1]
    //     SaveInDspDebugBuffer( (  diag.data.f.trigCounter[1] << 16) | qspi_misc.mismatches_level);

    //     SaveInDspDebugBuffer(rtcom.time_now_ms.s);
    //     SaveInDspDebugBuffer(rtcom.time_now_ms.ms | (boards_in_qspi[QSPI_BRANCH_A] << 16));

    //     SaveInDspDebugBuffer(dim_collection.reset_req_packed);
    //     SaveInDspDebugBuffer(local.flat_qspi_board_is_present_packed[DIMS_IN_ACTUAL_CYCLE]);
    //     SaveInDspDebugBuffer(dpcom.dsp.st_latched);
    //    }
}



static void PutDIMsamplesInLog(uint32_t logical_dim, struct CC_us_time * sample_time, uint32_t * samples)
{
    // This function is called from ScaleDimAnalogueData() to store a DIM sample in a log buffer.
    // If samples is NULL then a zero sample is stored as a marker.
    // The function only really stores data for class 51 as the other classes do not have DIM logs.


    uint32_t      xx;

    xx = dim_log[logical_dim].status.idx + 1;

    if (xx >= FGC_LOG_DIM_LEN)                  // beyond allocated space ?
    {
        xx = 0;
    }

    if (samples)   // samples = 0, is used as mark
    {
        dim_log[logical_dim].data[xx].r[0] = samples[0];
        dim_log[logical_dim].data[xx].r[1] = samples[1];
        dim_log[logical_dim].data[xx].r[2] = samples[2];
        dim_log[logical_dim].data[xx].r[3] = samples[3];
    }
    else
    {
        dim_log[logical_dim].data[xx].r[0] = 0;           // Store a MIN value sample
        dim_log[logical_dim].data[xx].r[1] = 0;
        dim_log[logical_dim].data[xx].r[2] = 0;
        dim_log[logical_dim].data[xx].r[3] = 0;
    }

    dim_log[logical_dim].status.idx  = xx;
    dim_log[logical_dim].status.time = *sample_time;

    if (dim_log[logical_dim].status.dont_log)
    {
        dim_log[logical_dim].status.samples_to_acq--;         // Decrement samples to acquire
    }
}


// EOF
