//! @file     mcu.c
//! @brief    RX610 hardware related stuff


// ---------- Includes

#include <stdlib.h>

#include <bgp.h>
#include <config.h>
#include <dallasTask.h>
#include <defprops.h>
#include <dpcom.h>
#include <dpcls.h>
#include <dpcmd.h>
#include <events.h>
#include <logEvent.h>
#include <fbs.h>
#include <logRun.h>
#include <logSpy.h>
#include <mcu.h>
#include <mcuDependent.h>
#include <msTask.h>
#include <nvs.h>
#include <panic.h>
#include <parsService.h>
#include <pub.h>
#include <regfgc3Pars.h>
#include <regfgc3Task.h>
#include <rtd.h>
#include <sleep.h>
#include <sta.h>
#include <stateTask.h>
#include <status.h>
#include <syscalls.h>
#include <trm.h>
#include <version.h>



// ---------- Constants

// Task Stack sizes (words)

// NB: it is advised to have MCU_STACK_SIZE_FCM = MCU_STACK_SIZE_SCM since both tasks share the same code (CmdTsk)

#define MCU_STACK_SIZE_MST    512    //  0
#define MCU_STACK_SIZE_EVT    512    //  1
#define MCU_STACK_SIZE_FBS   2048    //  2
#define MCU_STACK_SIZE_STA   2048    //  3
#define MCU_STACK_SIZE_PUB   2048    //  4
#define MCU_STACK_SIZE_FCM   2048    //  5
#define MCU_STACK_SIZE_TCM   2048    //  6
#define MCU_STACK_SIZE_SCI   2048    //  7
#define MCU_STACK_SIZE_DLS   1024    //  8
#define MCU_STACK_SIZE_RTD   2048    //  9
#define MCU_STACK_SIZE_TRM   2048    // 10
#define MCU_STACK_SIZE_BGP    512    // 11

#define MCU_TEST_DATA_SIZE     32    //!< in 16-bit words



// ---------- Internal structures, unions and enumerations

// Task stack buffers

struct Mcu_mcu_task_stack
{
    OS_STK   mst[MCU_STACK_SIZE_MST];      // Millisecond tick task stack
    OS_STK   evt[MCU_STACK_SIZE_EVT];      // Event task
    OS_STK   fbs[MCU_STACK_SIZE_FBS];      // Fieldbus task stack
    OS_STK   sta[MCU_STACK_SIZE_STA];      // State task stack
    OS_STK   pub[MCU_STACK_SIZE_PUB];      // Publication task stack
    OS_STK   fcm[MCU_STACK_SIZE_FCM];      // Fieldbus command task stack
    OS_STK   tcm[MCU_STACK_SIZE_TCM];      // Terminal command task stack
    OS_STK   sci[MCU_STACK_SIZE_SCI];      // RegFgc3 task stack
    OS_STK   dls[MCU_STACK_SIZE_DLS];      // Dallas task stack
    OS_STK   rtd[MCU_STACK_SIZE_RTD];      // Real-time display task stack
    OS_STK   trm[MCU_STACK_SIZE_TRM];      // Terminal communications task stack
    OS_STK   bgp[MCU_STACK_SIZE_BGP];      // Idle task stack
};



// ---------- External variable definitions

struct Mcu                mcu;
struct Mcu_power_supplies psu;



// ---------- Internal variable definitions

static struct Mcu_mcu_task_stack mcu_task_stack;



// ---------- Internal function definitions

//! This function checks the consumption of the stack.
//! The results are stored in shared memory registers
//! which are recorded in the runlog if the system crashes.
//! They can also be seen through the property FGC.DEBUG.STACKUSE
//! 100 : all the stack already used (big probability of stack overflow)
//!   0 : all stack available

static void mcuCheckTaskStack(uint16_t tsk_id, OS_STK * stk, uint16_t stack_size)
{
    uint16_t n;
    uint16_t empty_value;

    empty_value = 0xCAF0 + tsk_id;

    for (n = 0; n < stack_size && *(stk++) == empty_value; n++)
    {
    }

    shared_mem.stack_usage.tasks[tsk_id] = 100 - ((n * 100) / stack_size);
}



static void mcuCheckDspStack(void)
{
    static bool report_once = false;

    uint16_t const stack_usage = (uint16_t)dpcom.dsp.stack_usage;

    if (report_once == false && stack_usage > 90)
    {
        logRunAddEntry(FGC_RL_STKUSE, &stack_usage);

        report_once = true;
    }
}



static void mcuStop(void)
{
    switch (mcu.ctrl_sym_idx)
    {
        case STP_PWRCYC:
            POWER_CYCLE();
            break;

        case STP_RESET:
            // Global Reset
            mcuReset();
            break;

        case STP_BOOT:
            // Global Reset and stay in boot
            shared_mem.mainprog_seq = SEQUENCE_STAY_IN_BOOT;
            mcuReset();
            break;

        case STP_CRASH:
            // Force a MCU crash
            panicCrash(0xDEAD);
            break;
    }
}



// ---------- External function definitions

void mcuDisableNetworkInterrupt(void)
{
    //  __asm__ volatile ( "clrpsw I" ); // clear I flag in PSW

    //  IRQ2 - vector 66 - FIP_~MSG_IRQ - INT_Excep_IRQ2()
    ICU.IER08.BIT.IEN2 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR22.BIT.IPR_20 = 0x00;        // Interrupt Priority Register

    //  when  the FIP board this comes from exactly the same event as IRQ2, so we don't use it
    //  IRQ3 - vector 67 - FIP_~TIME_IRQ or ETHERNET 50Hz pulse - INT_Excep_IRQ3()
    //  ICU.IER08.BIT.IEN3 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR23.BIT.IPR_20 = 0x00;        // Interrupt Priority Register
}



void mcuDisable1msTickInterrupt(void)
{
    //  IRQ4 - vector 68 - MCU_~TICK - INT_Excep_IRQ4()
    ICU.IER08.BIT.IEN4 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR24.BIT.IPR_20 = 0x00;        // Interrupt Priority Register


    //  TMR0 CMIA0 - Compare Match A Interrupt - vector 174 - INT_Excep_TMR0_CMIA0()
    //  TMR0 it is programmed to generate an interrupt every 1ms
    ICU.IER15.BIT.IEN6 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR68.BIT.IPR_20 = 0x00;        // Interrupt Priority Register

    // These 2 are never disabled

    // TMR3 OVI3 - Timer Overflow Interrupt - vector 185 - INT_Excep_TMR3_OVI3()
    // TMR3 will count [0..255] incremented by TMR2 count match (1us) and generating an interrupt when overflows
    //  ICU.IER17.BIT.IEN1 = 0;             // Interrupt Request Enable Register
    //  ICU.IPR6B.BIT.IPR_20 = 0x00;        // Interrupt Priority Register

    // TPU0.TGRA input capture/compare match Interrupt - vector 104 - INT_Excep_TPU0_TGI0A()
    //  ICU.IER0D.BIT.IEN0 = 0;             // interrupt Request Enable Register
    //  ICU.IPR4C.BIT.IPR_20 = 0x00;        // Interrupt Priority Register
}



void mcuEnable1msTickInterruptFromMcuTimer(void)
{
    // this 2 events are level 7 !!!, will not be disabled by OS_ENTER_CRITICAL()

    //  TMR3 OVI3 - Timer Overflow Interrupt - vector 185 - INT_Excep_TMR3_OVI3()
    //  TMR3 will count [0..255] incremented by TMR2 count match (1us) and generating an interrupt when overflows
    ICU.IER17.BIT.IEN1 = 1;             // Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20 = 0x07;        // Interrupt Priority Register

    //  TPU0.TGRA input capture/compare match Interrupt - vector 104 - INT_Excep_TPU0_TGI0A()
    ICU.IER0D.BIT.IEN0 = 1;             // interrupt Request Enable Register
    ICU.IPR4C.BIT.IPR_20 = 0x07;        // Interrupt Priority Register
}



void mcuEnable1msTickInterruptFromPLD(void)
{
    //  IRQ4 - vector 68 - MCU_~TICK - INT_Excep_IRQ4()
    ICU.IER08.BIT.IEN4 = 1;             // Interrupt Request Enable Register
    ICU.IPR24.BIT.IPR_20 = 0x03;        // Interrupt Priority Register

    // this 2 events are level 7 !!!, will not be disabled by OS_ENTER_CRITICAL()

    //  TMR3 OVI3 - Timer Overflow Interrupt - vector 185 - INT_Excep_TMR3_OVI3()
    //  TMR3 will count [0..255] incremented by TMR2 count match (1us) and generating an interrupt when overflows
    ICU.IER17.BIT.IEN1 = 1;             // Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20 = 0x07;        // Interrupt Priority Register

    //  TPU0.TGRA input capture/compare match Interrupt - vector 104 - INT_Excep_TPU0_TGI0A()
    ICU.IER0D.BIT.IEN0 = 1;             // interrupt Request Enable Register
    ICU.IPR4C.BIT.IPR_20 = 0x07;        // Interrupt Priority Register
}



void mcuEnableNetworkInterrupt(void)
{
    //  IRQ2 - vector 66 - FIP_~MSG_IRQ - INT_Excep_IRQ2()
    ICU.IER08.BIT.IEN2 = 1;             // Interrupt Request Enable Register
    ICU.IPR22.BIT.IPR_20 = 0x02;        // Interrupt Priority Register

    // when  the FIP board this comes from exactly the same event as IRQ2, so we don't use it
    //  IRQ3 - vector 67 - FIP_~TIME_IRQ or ETHERNET 50Hz pulse - INT_Excep_IRQ3()
    //  ICU.IER08.BIT.IEN3 = 1;             // Interrupt Request Enable Register
    //  ICU.IPR23.BIT.IPR_20 = 0x03;        // Interrupt Priority Register

    // End of DMA0 interrupt
    DMAC_COMMON.DMEDET.BIT.DEDET0 = 1;  // Clear interrupt
    DMAC_COMMON.DMEDET.BIT.DEDET1 = 1;  // Clear interrupt
    ICU.IPR71.BIT.IPR_20 = 0x3;         // Set interrupt priority for DMA 1
    ICU.IER18.BIT.IEN7 = 1;             // Enable interrupt for DMA 1
    DMAC_COMMON.DMSCNT.BIT.DMST = 1;    // Start DMAC
}



void mcuReset(void)
{
    OS_CPU_SR  cpu_sr;

    OS_ENTER_CRITICAL_7();              // Disable ALL interrupts

    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_GLOBAL_MASK16;   // Request a global reset

    // Wait for reset to take effect

    exit(0);       // this will hang in a jump to itself
}



void mcuEnableTimersAndInterrupts(void)
{
    mcuDisable1msTickInterrupt();
    mcuEnable1msTickInterruptFromPLD(); // we can take 1ms Tick from PLD now

    // The network interrupt is enabled later by FipInit() in MsTsk()
    // Todo comment does not match code. Obsolete?
    mcuDisableNetworkInterrupt();
    mcuEnableNetworkInterrupt();

    // Wait for 2ms to all interrupts to start

    sleepUs(2000);
}



void mcuInit(void)
{
    uint16_t ii;


    // If P6.DR.BIT.B5 is 1 (longer integration time) then when the network card
    // receive a "reset SHORT pulse signal" (50) from the gateway it is ignored.
    // It waits for a "reset LONG pulse signal" (100) to trigger the power cycle.

    ACCEPT_ONLY_NET_LONG_PULSE();

    // Reset peripherals

    digitalIoDisableOutputs();

    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_C62OFF_MASK16;   // Turn M16C62 off
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_DSP_MASK16;      // Reset TMS320C6727
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_ANALOG_MASK16;   // Reset Analog interface

     mcuStopDerivedClocks();

    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_DIGITAL_MASK16;  // Reset Digital interface
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_QSM_MASK16;      // Reset diagnostics interface

    // release reset signal
    // ToDo : look for a proper place to do this
    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_DIGITAL_MASK16; // Reset Digital interface
    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_QSM_MASK16;     // Reset diagnostics interface

    nvsLock();

    // NewLib uses malloc to allocate the reentrant structure (1 per thread)

    InitMalloc();

    logRunAddTimestamp();
    logRunAddEntry(FGC_RL_MP_START, &version.unixtime);

    // Initialise config state to UNSYNCED

    configSetState(FGC_CFG_STATE_UNSYNCED, false);

    // Initialise the test property data

    for (ii = 0 ; ii < MCU_TEST_DATA_SIZE; ii++)
    {
        mcu.test_data[ 1 + ii * 2 ] = ii;             // Big endian !
    }

    // Enable tick from MCU timer by default so that USLEEP() and MSLEEP() can be used
    // mcuEnable1msTickInterruptFromPLD() cannot be used at this point because there is a OSTskResume(TSK_MST) in it

    mcuEnable1msTickInterruptFromMcuTimer();
}



void mcuStartClocks(void)
{
    // Enable the ticks

    setBitmap(TICK_CTRL_P,  TICK_CTRL_ACT_MASK16 | TICK_CTRL_ADC_MASK16
                          | TICK_CTRL_DSP_MASK16 | TICK_CTRL_SYNC_TO_C0_MASK16);

    TICK_DSP_ACT_DELAY_US_P = 60;

    // Set TIME_TILL_CO to align the DSP tick to millisecond boundary and
    // wait for the DSP tick to be operational

    TIME_TILL_C0_US_P = 10000;

    sleepUs(11000);
}



void mcuStopDerivedClocks(void)
{
    clrBitmap(TICK_CTRL_P,  TICK_CTRL_ACT_MASK16 | TICK_CTRL_ADC_MASK16
                          | TICK_CTRL_DSP_MASK16 | TICK_CTRL_SYNC_TO_C0_MASK16);
}



void mcuSetExtSyncEnable(void)
{
    setBitmap(TICK_CTRL_P, TICK_CTRL_EXT_SYNC_MASK16);
}



void mcuInitOS(void)
{
    // Create tasks

    // If the number of tasks changes, shared_mem->stack_usage must be updated accordingly

    OSTskInit(MCU_NUM_TASKS);

    OSTskCreate(msTask,      NULL,         mcu_task_stack.mst,  MCU_STACK_SIZE_MST);   //  0. msTask     - Millisecond task
    OSTskCreate(eventsTask,  NULL,         mcu_task_stack.evt,  MCU_STACK_SIZE_EVT);   //  1. eventsTsk  - Events task
    OSTskCreate(fbsTask,     NULL,         mcu_task_stack.fbs,  MCU_STACK_SIZE_FBS);   //  2. fbsTsk     - Fieldbus task
    OSTskCreate(stateTask,   NULL,         mcu_task_stack.sta,  MCU_STACK_SIZE_STA);   //  3. stateTask  - State machine task
    OSTskCreate(pubTask,     NULL,         mcu_task_stack.pub,  MCU_STACK_SIZE_PUB);   //  4. PubTsk     - Publication task
    OSTskCreate(CmdTask,     (void *)&fcm, mcu_task_stack.fcm,  MCU_STACK_SIZE_FCM);   //  5. FcmTsk     - Fieldbus commands task
    OSTskCreate(CmdTask,     (void *)&tcm, mcu_task_stack.tcm,  MCU_STACK_SIZE_TCM);   //  6. TcmTsk     - Terminal commands task
    OSTskCreate(regFgc3Task, NULL,         mcu_task_stack.sci,  MCU_STACK_SIZE_SCI);   //  7. RegFgc3Tsk - RegFgc3 task
    OSTskCreate(dallasTask,  NULL,         mcu_task_stack.dls,  MCU_STACK_SIZE_DLS);   //  8. dallasTask - Dallas bus task
    OSTskCreate(rtdTask,     NULL,         mcu_task_stack.rtd,  MCU_STACK_SIZE_RTD);   //  9. rtdTsk     - Real-time display task
    OSTskCreate(trmTask,     NULL,         mcu_task_stack.trm,  MCU_STACK_SIZE_TRM);   // 10. TrmTsk     - Terminal communication task
    OSTskCreate(bgpTask,     NULL,         mcu_task_stack.bgp,  MCU_STACK_SIZE_BGP);   // 11. BgpTsk     - Background processing task


    // Create Semaphores

    // Initialise semaphore control blocks - THIS MUST MATCH THE NUMBER OF SEMAPHORES CREATED BELOW

    OSSemInit(FGC_NUM_SEMAPHORES);

    fcm.sem                = OSSemCreate(0);  //  0. fbsTsk -> FcmTsk (next cmd pkt waiting)
    tcm.sem                = OSSemCreate(0);  //  1. TrmTsk -> TcmTsk (next cmd pkt waiting)
    pcm.sem                = OSSemCreate(1);  //  2. Pub cmd struct reservation semaphore
    fbs.cmd.q_not_full     = OSSemCreate(0);  //  3. Fieldbus cmd.q flow control semaphore
    fbs.pub.q_not_full     = OSSemCreate(0);  //  4. Fieldbus pub.q flow control semaphore
    mcu.set_lock           = OSSemCreate(1);  //  5. Set command lock semaphore
    pub_sema.lock          = OSSemCreate(0);  //  6. Sub structure lock semaphore
    pub_sema.last_pkt_send = OSSemCreate(0);  //  7. Last pub pkt now being sent
    regfgc3_task_sem_go    = OSSemCreate(0);  //  8. Awake RegFGC3 task
    log_get_sem            = OSSemCreate(1);  //  9. Mutex to retrieve the logs from the DSP
    event_log_sem          = OSSemCreate(1);  // 10. Mutex to access the event log
    dpcmd_sem              = OSSemCreate(1);  // 11. Mutex to access DPCOM commands

    uint8_t num_sem = dpcmd_sem - fcm.sem + 1;

    if (FGC_NUM_SEMAPHORES != num_sem)
    {
        panicCrash(num_sem);
    }

    // Create Message queues

    OSMsgInit(FGC_NUM_MSG_QUEUES);                               // Initialise message queue control blocks

    terminal.msgq = OSMsgCreate(terminal.msgq_buf, TRM_MSGQ_SIZE);    // 0. IsrMst & fbsTsk -> TrmTsk (kbd chars)

    // Create Memory partition

    OSMemInit(FGC_NUM_MEM_PARTITIONS);                               // Initialise memory partition control blocks

    // 0. Mem buffers for terminal output characters
    terminal.mbuf = OSMemCreate(terminal.mem_buf, TRM_N_BUFS, TRM_BUF_SIZE);

    // Initialise command structure links before OS starts

    fcm.prop_buf = (struct Dpcom_prop_buf *) &dpcom.fcm_prop;     // Link fcm property buffer structure
    tcm.prop_buf = (struct Dpcom_prop_buf *) &dpcom.tcm_prop;     // Link scm property buffer structure

    fcm.pars_buf = (struct Pars_buf *) &fcm_pars;       // Link fcm command pkt structure
    tcm.pars_buf = (struct Pars_buf *) &tcm_pars;       // Link scm command pkt structure
}



void mcuCheckStack(void)
{
    mcuCheckTaskStack(MCU_TSK_MST, mcu_task_stack.mst, MCU_STACK_SIZE_MST);  //  0. msTask     - Millisecond task
    mcuCheckTaskStack(MCU_TSK_EVT, mcu_task_stack.evt, MCU_STACK_SIZE_EVT);  //  1. eventsTsk  - Events task
    mcuCheckTaskStack(MCU_TSK_FBS, mcu_task_stack.fbs, MCU_STACK_SIZE_FBS);  //  2. fbsTsk     - Fieldbus task
    mcuCheckTaskStack(MCU_TSK_STA, mcu_task_stack.sta, MCU_STACK_SIZE_STA);  //  3. stateTask  - State machine task
    mcuCheckTaskStack(MCU_TSK_PUB, mcu_task_stack.pub, MCU_STACK_SIZE_PUB);  //  4. pubTsk     - Publication task
    mcuCheckTaskStack(MCU_TSK_FCM, mcu_task_stack.fcm, MCU_STACK_SIZE_FCM);  //  5. fcmTsk     - Fieldbus commands task
    mcuCheckTaskStack(MCU_TSK_TCM, mcu_task_stack.tcm, MCU_STACK_SIZE_TCM);  //  6. tcmTsk     - Terminal commands task
    mcuCheckTaskStack(MCU_TSK_SCI, mcu_task_stack.sci, MCU_STACK_SIZE_SCI);  //  7. scivsTsk   - SCIVS bus task
    mcuCheckTaskStack(MCU_TSK_DLS, mcu_task_stack.dls, MCU_STACK_SIZE_DLS);  //  8. dallasTask - Dallas bus task
    mcuCheckTaskStack(MCU_TSK_RTD, mcu_task_stack.rtd, MCU_STACK_SIZE_RTD);  //  9. rtdTsk     - Real-time display task
    mcuCheckTaskStack(MCU_TSK_TRM, mcu_task_stack.trm, MCU_STACK_SIZE_TRM);  // 10. trmTsk     - Terminal communications task
    mcuCheckTaskStack(MCU_TSK_BGP, mcu_task_stack.bgp, MCU_STACK_SIZE_BGP);  // 11. bgpTsk     - Background processing task

    mcuCheckDspStack();
}



void mcuReadPsuVoltage(void)
{
    psu.v5   = (10.560 * AD0.ADDRC) / 1023.0;
    psu.vp15 = (30.300 * AD0.ADDRD) / 1023.0;
    psu.vm15 = (7.333  * AD1.ADDRA) / 1023.0 - (11.0 * psu.vp15) / 9.0;

    // Check limits

    if (mcuVoltageOutsideTolerance() == true)
    {
        psu.fails_counter++;

        if (psu.fails_counter > 50)
        {
            statusSetLatched(FGC_LAT_PSU_V_FAIL);
        }
    }
    else
    {
        psu.fails_counter = 0;
    }
}



bool mcuVoltageOutsideTolerance(void)
{
    return (   psu.v5   <  FGC_PSU_MIN_5V
            || psu.v5   >  FGC_PSU_MAX_5V
            || psu.vp15 <  FGC_PSU_MIN_15V
            || psu.vp15 >  FGC_PSU_MAX_15V
            || psu.vm15 < -FGC_PSU_MAX_15V
            || psu.vm15 > -FGC_PSU_MIN_15V);
}



void mcuTerminate(uint16_t sym_idx)
{
    mcu.ctrl_sym_idx = sym_idx;

    // 40 x 5ms = 200ms

    mcu.ctrl_counter = 40;
}



void mcuDeviceControl(void)
{
    if (mcu.ctrl_counter > 0)
    {
        mcu.ctrl_counter--;

        if (mcu.ctrl_counter == 0)
        {
            mcuStop();
        }
    }
}


// EOF

