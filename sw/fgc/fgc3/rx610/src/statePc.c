//! @file   statePc.c
//! @brief  Common code for the Power Converter state


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <bitmap.h>
#include <crate.h>
#include <digitalIo.h>
#include <dpcls.h>
#include <fgc_errs.h>
#include <modePc.h>
#include <pc_state.h>
#include <prop.h>
#include <pub.h>
#include <pulsePermit.h>
#include <rtd.h>
#include <statePc.h>
#include <statePc_class.h>
#include <stateOp.h>
#include <stateVs.h>
#include <status.h>
#include <polSwitch.h>
#include <postMortem.h>


// ---------- Internal structures, unions and enumerations

//! Internal local variables for the module statePc

struct statePcLocal
{
    StatePC  * stateFunc;       //!< Pointer to the active state function
    uint32_t   stateTimeMs;     //!< Time in ms the state has bee running
};



// ---------- Internal variable definitions

static struct statePcLocal statePc;



// ---------- External variable declarations

extern StatePCtrans * StateTransitions[][STATE_PC_REF_MAX_TRANSITIONS + 1];


// ---------- External variable declarations

struct StatePc state_pc;



// ---------- Internal function definitions

static uint16_t statePcStateToSimplified(uint16_t const state)
{
    // Map the PC state to the PC simplified state
    static uint8_t const statePcSimplifiedMap[] =
    {
        FGC_PC_SIMPLIFIED_FAULT,      // FO
        FGC_PC_SIMPLIFIED_OFF,        // OF
        FGC_PC_SIMPLIFIED_FAULT,      // FS
        STATE_PC_SIMPLIFIED_NONE,     // SP
        STATE_PC_SIMPLIFIED_NONE,     // ST
        STATE_PC_SIMPLIFIED_NONE,     // SA
        STATE_PC_SIMPLIFIED_NONE,     // TS
#if (FGC_CLASS_ID == 63)
        FGC_PC_SIMPLIFIED_ON_STANDBY, // SB
#else
        STATE_PC_SIMPLIFIED_NONE,     // SB
#endif
        FGC_PC_SIMPLIFIED_ON,         // IL
        FGC_PC_SIMPLIFIED_ON,         // TC
        STATE_PC_SIMPLIFIED_NONE,     // AR
        STATE_PC_SIMPLIFIED_NONE,     // RN
        STATE_PC_SIMPLIFIED_NONE,     // AB
        FGC_PC_SIMPLIFIED_ON,         // CY
        STATE_PC_SIMPLIFIED_NONE,     // PL
        FGC_PC_SIMPLIFIED_BLOCKING,   // BK
        FGC_PC_SIMPLIFIED_ON,         // EC
        FGC_PC_SIMPLIFIED_ON,         // DT
        FGC_PC_SIMPLIFIED_ON,         // PA
    };

    return statePcSimplifiedMap[state];
}



// ------------------------------- Generic STATE FUNCTIONS --------------------------------

// STATE: FLT_OFF (FO)

uint32_t statePcFO(bool firstCall)
{
    // Check for unexpected switch on of the converter

    if (   testBitmap(digitalIoGetInputs(), STATE_PC_VS_POWER_ON) == true
        && vs.present == FGC_CTRL_ENABLED)
    {
        statusSetFaults(FGC_FLT_VS_STATE);
    }

    return FGC_PC_FLT_OFF;
}



// STATE: OFF (OF)

uint32_t statePcOF(bool firstCall)
{
    // Check for unexpected switch on of the converter

    if (   testBitmap(digitalIoGetInputs(), STATE_PC_VS_POWER_ON) == true
        && vs.present == FGC_CTRL_ENABLED)
    {
        statusSetFaults(FGC_FLT_VS_STATE);
    }

    return FGC_PC_OFF;
}



// STATE: FLT_STOPPING (FS)

uint32_t statePcFS(bool firstCall)
{
    if (firstCall == true)
    {
        // Switch off VS_RUN and block the converter since this
        // might have been reached withouth going through BLOKCING.

        digitalIoSetCmdRequest(DDOP_CMD_OFF | DDOP_CMD_BLOCKING);

        postMortemTriggerPm();
    }

    return FGC_PC_FLT_STOPPING;
}



// STATE: STOPPING (SP)

uint32_t statePcSP(bool firstCall)
{
    if (firstCall == true)
    {
        // Entering state - switch off voltage source

        digitalIoSetCmdRequest(DDOP_CMD_OFF | DDOP_CMD_BLOCKING);
    }

    return FGC_PC_STOPPING;
}



// STATE: STARTING (ST)

uint32_t statePcST(bool firstCall)
{
     static uint32_t vsRunTimeoutMs;

    if (firstCall == true)
    {
        // Perform an automatic calibration of the DAC if in NORMAL

        if (   stateOpGetState()       == FGC_OP_NORMAL
            && crateGetActuationDest() == CRATE_ACTUATION_DEST_DAC)
        {
            calibrationSetup(CAL_TARGET_DACS, CAL_DACS, 0);
        }

         vsRunTimeoutMs = 0;
    }
    else
    {
        // If the DAC calibration has completed, start the converter

        if (stateOpGetState() != FGC_OP_CALIBRATING)
        {
            if (testBitmap(digitalIoGetInputs(), DIG_IP_VSRUN_MASK32) == false)
            {
                digitalIoSetCmdRequest(DDOP_CMD_RESET | DDOP_CMD_ON | DDOP_CMD_BLOCKING);

                // Prepare the VSRUN timeout

                vsRunTimeoutMs = statePcGetStateTime() + (uint32_t)(vs.vsrun_timeout * 1000 + 0.5);
            }

            // Check if the timeout has expired

            if (statePcGetStateTime() > vsRunTimeoutMs)
            {
                statusSetFaults(FGC_FLT_VS_RUN_TO);
            }
        }
    }

    return FGC_PC_STARTING;
}

// STATE: BLOCKING (BK)

uint32_t statePcBK(bool firstCall)
{
    // Unblock the converter if necessary

    bool mode_pc_above_blocking = (   modePcGetInternal() != FGC_PC_BLOCKING
                                   && modePcGetInternal() != FGC_PC_OFF);

    if (interFgcIsSlave() == true)
    {
        if (mode_pc_above_blocking == true)
        {
            digitalIoSetCmdRequest(DDOP_CMD_UNBLOCK);
        }
    }

    if (   mode_pc_above_blocking == true
        && pulsePermitValid()     == true
        && dpcls.mcu.vs.blockable != FGC_VS_BLOCK_DISABLED)
    {
        digitalIoSetCmdRequest(DDOP_CMD_UNBLOCK);
    }

    return FGC_PC_BLOCKING;
}


// STATE: SLOW_ABORT (SA)

uint32_t statePcSA(bool firstCall)
{
    if (firstCall == true)
    {
        postMortemTriggerSa();
    }

    return FGC_PC_SLOW_ABORT;
}


// ------------------------------- Generic TRANSITION FUNCTIONS --------------------------------

StatePC * XXtoFS(void)
{
    // Use directly the digital inputs since this is a more direct and reliable check

    if (   testBitmap(digitalIoGetInputs(), (DIG_IP_PWRFAILURE_MASK32 | DIG_IP_FASTABORT_MASK32)) == true
        || statusGetFaults() != 0)
    {
        return statePcFS;
    }

    return NULL;
}



StatePC * FStoFO(void)
{
    if (   vs.present == FGC_CTRL_DISABLED
        || testBitmap(digitalIoGetInputs(), DIG_IP_VSPOWERON_MASK32 | DIG_IP_VSREADY_MASK32) == false)
    {
        return statePcFO;
    }

    return NULL;
}



StatePC * FStoSP(void)
{
    if (statusGetFaults() == 0)
    {
        return statePcSP;
    }

    return NULL;
}



StatePC * SPtoOF(void)
{
    if (   vs.present == FGC_CTRL_DISABLED
        || testBitmap(digitalIoGetInputs(), DIG_IP_VSPOWERON_MASK32 | DIG_IP_VSREADY_MASK32) == false)
    {
        return statePcOF;
    }

    return NULL;
}



StatePC * FOtoOF(void)
{
    if (statusGetFaults() == 0)
    {
        return statePcOF;
    }

    return NULL;
}



StatePC * OFtoFO(void)
{
    if (statusGetFaults() != 0)
    {
        return statePcFO;
    }

    return NULL;
}



StatePC * OFtoST(void)
{
    if (dpcom.mcu.pc.mode != FGC_PC_OFF)
    {
        return statePcST;
    }

    return NULL;
}



StatePC * STtoSP(void)
{
    if (dpcom.mcu.pc.mode == FGC_PC_OFF)
    {
        return statePcSP;
    }

    return NULL;
}



StatePC * STtoBK(void)
{
    if (   (vs.present == FGC_CTRL_DISABLED && interFgcGetSlaveState() == FGC_PC_SIMPLIFIED_BLOCKING)
        || (   STATE_VS  == FGC_VS_BLOCKED
            || (dpcls.mcu.vs.blockable == FGC_VS_BLOCK_DISABLED && STATE_VS == FGC_VS_READY)))
    {
        return statePcBK;
    }

    return NULL;
}



StatePC * BKtoSP(void)
{
    if (dpcom.mcu.pc.mode == FGC_PC_OFF)
    {
        return statePcSP;
    }

    return NULL;
}



// ---------- External function definitions

void statePcInit(void)
{
    CC_STATIC_ASSERT(FGC_PC_FLT_OFF       ==  0, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_OFF           ==  1, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_FLT_STOPPING  ==  2, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_STOPPING      ==  3, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_STARTING      ==  4, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_SLOW_ABORT    ==  5, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_TO_STANDBY    ==  6, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_ON_STANDBY    ==  7, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_IDLE          ==  8, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_TO_CYCLING    ==  9, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_ARMED         == 10, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_RUNNING       == 11, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_ABORTING      == 12, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_CYCLING       == 13, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_POL_SWITCHING == 14, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_BLOCKING      == 15, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_ECONOMY       == 16, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_DIRECT        == 17, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_PAUSED        == 18, Unexpected_STATE_PC_constant_value);

    // Initialize internal states to FLT_OFF

    pcStateSet(FGC_PC_OFF);

    statePc.stateFunc   = statePcOF;
    statePc.stateTimeMs = 0;

    // Initialize state machine parameters shared with the DSP

    dpcom.mcu.pc.state = FGC_PC_OFF;

    // dpcom.dsp.log.slow_abort_requeset  = false;
    // dpcom.dsp.log.slow_abort_completed = false;
}



enum Inter_fgc_master_transitions statePcTransitionFuncToEnum(StatePCtrans transition_func)
{
    static StatePCtrans * transitions[] =
    {
        NULL,      // MASTER_TRANSITION_NONE
        FOtoOF,    // MASTER_TRANSITION_FO_TO_OF
        OFtoST,    // MASTER_TRANSITION_OF_TO_ST
        FStoFO,    // MASTER_TRANSITION_FS_TO_FO
        FStoSP,    // MASTER_TRANSITION_FS_TO_SP
        SPtoOF,    // MASTER_TRANSITION_SP_TO_OF
        STtoBK,    // MASTER_TRANSITION_ST_TO_BK
    };

    static_assert(ArrayLen(transitions) == MASTER_TRANSITION_COUNT, "Array length doesn't match enum used for indexing");

    enum Inter_fgc_master_transitions t;

    for(t = 0; t < MASTER_TRANSITION_COUNT; t++)
    {
        if(transition_func == transitions[t])
        {
            return t;
        }
    }

    return MASTER_TRANSITION_NONE;
}



void statePcProcess(void)
{
    StatePC       * nextState        = NULL;
    StatePCtrans  * transition       = NULL;
    StatePCtrans ** stateTransitions = &StateTransitions[STATE_PC][0];

    // Scan transitions for the current state, testing each condition in priority order

    while (   (transition = *(stateTransitions++)) != NULL
           && (nextState  = (*transition)())       == NULL)
    {
        ;
    }

    // If a transition condition was true then switch to the new state

    if (nextState != NULL)
    {
        // Reset the time since last state change

        statePc.stateTimeMs = 0;

        // Run new state function with firstCall flag set to true

        pcStateSet(nextState(true));

        // Cache the state function and bit mask

        statePc.stateFunc = nextState;

        // Notify CMW subscriptions

        pubPublishProperty(&PROP_STATE_PC, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, TRUE);

        // Update and publish the simplified PC state only if it changed.

        uint8_t statePcSimplified = statePcStateToSimplified(STATE_PC);

        if (   state_pc.pc_simplified != statePcSimplified
            && statePcSimplified      != STATE_PC_SIMPLIFIED_NONE)
        {
            state_pc.pc_simplified = statePcSimplified;

            pubPublishProperty(&PROP_STATE_PC_SIMPLIFIED, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, TRUE);
        }
    }
    else
    {
        statePc.stateTimeMs += STATE_PC_PERIOD_MS;

        // Call the state function

        statePc.stateFunc(false);
    }

    // Class specific processing

    statePcClassProcess();
}




StatePC * statePcGetStateFunc(void)
{
    return statePc.stateFunc;
}



uint32_t statePcGetStateTime(void)
{
    return statePc.stateTimeMs;
}



uint16_t statePcGetSimplified(void)
{
    return state_pc.pc_simplified;
}



char const * statePcGetStateName(uint8_t state)
{
    static const char stateName[] = "FOOFFSSPSTSATSSBILTCARRNABCYPLBKECDTPA";

    return (&stateName[(uint16_t)state * 2]);
}



void statePcResetFaults(void)
{
    polSwitchReset();

    digitalIoSetCmdRequest(DDOP_CMD_RESET);
}


// EOF
