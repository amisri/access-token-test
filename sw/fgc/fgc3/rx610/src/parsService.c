//! @file    pars_service.c
//! @brief   Pars Functions


// ---------- Includes

#include <parsService.h>
#include <bitmap.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <defconst.h>
#include <definfo.h>
#include <deftypes.h>
#include <fgc_errs.h>
#include <common.h>


// ---------- Internal structures, unions and enumerations

//! Command Packet parse buffers

struct cmd_pkt_parse_buf
{
    uint32_t n_carryover_chars;                           //!< Number of characters from last token of prev pkt
    char     buf[PARS_MAX_CMD_PKT_LEN + PARS_MAX_CMD_TOKEN_LEN];    //!< Parse buffer with extra space for last token of previous packet
};



// ---------- External variable definitions

struct Pars_buf __attribute__((section("sram")))  fcm_pars;
struct Pars_buf __attribute__((section("sram")))  tcm_pars;



// ---------- Internal variable definitions

struct cmd_pkt_parse_buf  __attribute__((section("sram")))  fcm_pars_buf;
struct cmd_pkt_parse_buf  __attribute__((section("sram")))  tcm_pars_buf;




// ---------- Internal function definitions

//! This function is called when the MCU wants help to parse a command packet containing Integers or
//! Floats. A token can be split across packets so the function must find the
//! last token delimiter character (,) in the packet and keep all characters following this in the start
//! of the buffer for the next time the function is called.
//! The function supports two parameter types:
//! 1. Integers
//!    All integers are parsed as signed longs, and integer limits are the same. This means that even
//!    uint32_t properties are limited to the range 0 to (2^31)-1.
//! 2. Floats
//!    Floating point values have a different format in the DSP than in the MCU. So after parsing the value
//!    to float, it must be converted to IEEE format before being returned to the MCU.
//! Parses a set command in the form of S PROPERTY a,b,,d,e,,,h.

static void parsPkt(struct Pars_buf * pars, struct cmd_pkt_parse_buf * buf)
{
    uint32_t           i;                   //!< Loop variable
    uint32_t           out_of_limits_f;     //!< Out of limits flag
    uint32_t           base;                //!< Integer base (10=decimal 16=hex)
    uint32_t           type;                //!< Parameter type: PKT_FLOAT|PKT_INT|PKT_RELTIME|PKT_INTU
    uint32_t           n_pars;              //!< Number of parameters converted
    uint32_t           n_pkt_chars;         //!< Number of characters in command pkt
    uint32_t           n_excess_chars;      //!< Number of excess characters in command pkt
    uint32_t           out_idx;             //!< Out index for new pkt
    uint32_t           intuval = 0;         //!< Unsigned integer value
    int32_t            intval  = 0;         //!< Integer value
    float              fpval   = 0.0;       //!< Floating point value
    char             * pkt_buf;             //!< Pointer to start of uncompressed pkt buffer
    char             * dppkt_buf;           //!< Pointer within incoming pkt buffer in DPRAM
    struct Pars_pkt  * cmd_pkt;             //!< Pointer to command pkt structure (selected from float buf)

    // Prepare local variables from new packet data in DPRAM

    n_pars         = 0;
    n_excess_chars = 0;
    type           = pars->type;
    cmd_pkt        = &pars->pkt[pars->pkt_buf_idx];
    dppkt_buf      = &cmd_pkt->buf[0];
    n_pkt_chars    = cmd_pkt->n_chars;

    // Check for first packet

    if (testBitmap(cmd_pkt->header_flags, FGC_FIELDBUS_FLAGS_FIRST_PKT) == true)
    {
        // Cancel any left over characters

        buf->n_carryover_chars = 0;
    }

    // Uncompress new packet characters into byte array

    pkt_buf = &buf->buf[buf->n_carryover_chars];

    memcpy(pkt_buf, dppkt_buf, n_pkt_chars);

    pkt_buf       =  &buf->buf[0];
    out_idx       =  pars->out_idx;                     // Index to start of first new token
    pars->out_idx =  n_pkt_chars;                       // Set out_idx to end of packet to indicate it's empty
    n_pkt_chars   += buf->n_carryover_chars;            // Total number of characters in pkt buffer

    // Check for incomplete token at the end of the packet

    if (testBitmap(cmd_pkt->header_flags, FGC_FIELDBUS_FLAGS_LAST_PKT) == false)
    {
        while (n_pkt_chars > out_idx && pkt_buf[--n_pkt_chars] != ',')  // Scan backwards for ','
        {
            if (++n_excess_chars >= PARS_MAX_CMD_TOKEN_LEN)                  // If token exceeds max length
            {
                pars->errnum      = FGC_SYNTAX_ERROR;                   // Report syntax error
                pars->par_err_idx = 0;                                  // Unknown par index
                pars->type        = 0;                                  // Report action complete
                return;
            }
        }

        // If first token

        if (pkt_buf[n_pkt_chars] != ',')
        {
            n_pkt_chars--;
            goto end;
        }
    }

    // Null terminate previous token

    pkt_buf[n_pkt_chars] = 0;

    // Try to parse each token in turn

    // Set pkt buf pointer to start of first token

    pkt_buf = &buf->buf[out_idx];

    // Remove trailing spaces

    while (*pkt_buf == ' ' || *pkt_buf == ',')
    {
        pkt_buf++;

        if (*pkt_buf == '\0')
        {
            return;
        }
    }

    // Clear errnum

    pars->errnum = 0;

    for (;;)
    {
        if (*pkt_buf == ' ')
        {
            pkt_buf++;
        }

        if (*pkt_buf == '\0')                   // If end of pkt
        {
            pars->empty_tokens[n_pars] = true;
            n_pars++;
            break;
        }

        if (*pkt_buf == ',')                    // If no token before comma
        {
            pars->empty_tokens[n_pars] = true;
            n_pars++;                           // Return NO_INT/NO_FLOAT value
            pkt_buf++;                          // Skip comma
        }
        else                                    // else token present
        {
            errno = 0;                          // Reset errno before the call to strtol/strtoul/strtod
            pars->empty_tokens[n_pars] = false;

            if (type == PKT_INT || type == PKT_INTU)
            {
                // If prefix is "0x" or "0X"

                if (   pkt_buf[0] == '0'
                    && (pkt_buf[1] == 'x' || pkt_buf[1] == 'X'))
                {
                    // Prepare for hex value

                    base = 16;
                    pkt_buf += 2;
                }
                else
                {
                    // Prepare for decimal value

                    base = 10;
                }

                if (type == PKT_INT)
                {
                    // Parse token as signed integer

                    intval = strtol(pkt_buf, &pkt_buf, base);
                    out_of_limits_f = (intval < pars->limits.integer.min ||
                                       intval > pars->limits.integer.max);
                }
                else
                {
                    // The strtoul() implementation in the gcc RX610 library converts a negative
                    // value into its unsigned representation, thus conforming to the standard.
                    // An explicit check for '-' must be added.

                    out_of_limits_f = (*pkt_buf == '-');

                    if (!out_of_limits_f)
                    {
                        // Parse token as unsigned integer

                        intuval = strtoul(pkt_buf, &pkt_buf, base);
                        out_of_limits_f = (intuval < pars->limits.unsigned_integer.min ||
                                           intuval > pars->limits.unsigned_integer.max);
                    }
                }
            }
            else if (type == PKT_FLOAT)
            {
                // Convert token to float

                fpval = strtod(pkt_buf, &pkt_buf);
                out_of_limits_f = (fpval < pars->limits.fp.min || fpval > pars->limits.fp.max);
            }

            // Check errno == ERANGE is a portable way to detect "out of range" errors on uint32_t and int32_t (like for
            // example setting a uint32_t to -1).

            if (errno == ERANGE || out_of_limits_f)
            {
                // Parameter was out of limits

                pars->par_err_idx = n_pars;              // Report parameter index for error
                pars->errnum      = FGC_OUT_OF_LIMITS;   // Report error
                pars->type        = 0;                   // Cancel request to indicate end of action
                return;
            }

            if (*pkt_buf == ' ')
            {
                pkt_buf++;
            }

            // If not a valid delimiter or other type of lib error

            if (   errno
                || (*pkt_buf && *pkt_buf != ','))
            {
                // Report parameter index for error and cancel request to indicate end of action
                
                pars->par_err_idx = n_pars;
                pars->errnum      = (type == PKT_FLOAT ? FGC_BAD_FLOAT : FGC_BAD_INTEGER);
                pars->type        = 0;
                return;
            }

            switch (type)
            {
                case PKT_INT:
                    pars->results.ints[n_pars++] = intval;
                    break;

                case PKT_INTU:
                    pars->results.intu[n_pars++] = intuval;
                    break;

                case PKT_FLOAT:
                    pars->results.fp[n_pars++] = fpval;
                    break;
            }

            if (!*pkt_buf)           // Delimiter is nul
            {
                break;               // End processing of this packet
            }
            else                     // else delimiter is a comma
            {
                pkt_buf++;           // Skip over comma
            }
        }
    }

    // Transfer extra character to start of pkt buffer for next time

end:

    // Reinit pointer to start of pkt buffer

    pkt_buf = &buf->buf[0];

    for (i = 0; i < n_excess_chars; i++)
    {
        // Transfer extra characters to start of buffer

        pkt_buf[i] = pkt_buf[++n_pkt_chars];
    }

    errno = 0;

    // Return results to MCU

    pars->pkt_buf_idx      = n_excess_chars;   // Remember number of extra character for next time
    buf->n_carryover_chars = n_excess_chars;   // Remember number of extra character for next time
    pars->n_pars           = n_pars;           // Return number of parameters found
    pars->type             = 0;                // Cancel request to indicate that action is completed
}



// ---------- External function definitions

void parsPktFcm(struct Pars_buf * pars)
{
    parsPkt(pars, &fcm_pars_buf);
}



void parsPktTcm(struct Pars_buf * pars)
{
    parsPkt(pars, &tcm_pars_buf);
}


// EOF