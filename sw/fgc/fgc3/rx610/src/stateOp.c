//! @file   stateOp.c
//! @brief  Manage the Operational state of the FGC
//!
//! A state machine style of coding is used. Each time stateOpProcess() is called, it checks for a valid state transition
//! and calls the relevant state function.


// ---------- Includes

#include <bitmap.h>
#include <calibration.h>
#include <defprops.h>
#include <digitalIo.h>
#include <dpcls.h>
#include <dpcom.h>
#include <fbs_class.h>
#include <nvs.h>
#include <pc_state.h>
#include <pub.h>
#include <stateOp.h>
#include <statePc.h>
#include <stateVs.h>
#include <status.h>



// ---------- Constants

#define STATE_OP_REF_MAX_TRANSITIONS     3      //!< Maximum transitions from any state



// ---------- Internal structures, unions and enumerations

//! Type for state machine state function returning state number
typedef uint32_t StateOP(bool first_call);

//! Type for pointer to transition function returning pointer to state function
typedef StateOP * StateOPtrans(void);

//! Internal local variables for the module stateOp

struct stateOpLocal
{
    StateOP * stateFunc;       //!< Pointer to the active state function
};



// ---------- Internal function declarations ----------------------------

static uint32_t stateOpUC(bool firstCall);


static uint32_t stateOpNL(bool firstCall);


static uint32_t stateOpSM(bool firstCall);


static uint32_t stateOpCL(bool firstCall);


static uint32_t stateOpTT(bool firstCall);


static StateOP* XXtoNL(void);


static StateOP* XXtoSM(void);


static StateOP* XXtoTT(void);


static StateOP* NLtoCL(void);


static StateOP* CLtoNL(void);



// ---------- External variable definitions

struct stateOp_props stateOpProps;



// ---------- Internal variable definitions

static struct stateOpLocal stateOp;

// Transition functions : called in priority order, from left to right

static StateOPtrans * StateTransitions[][STATE_OP_REF_MAX_TRANSITIONS + 1] =
{
    /* 0. UC */ { XXtoNL, XXtoSM, XXtoTT,         NULL },
    /* 1. NL */ {         XXtoSM, XXtoTT, NLtoCL, NULL },
    /* 2. SM */ { XXtoNL,         XXtoTT,         NULL },
    /* 3. CL */ {                         CLtoNL, NULL },
    /* 4. TT */ { XXtoNL, XXtoSM,                 NULL }
};



// ---------- External function definitions

void stateOpInit(void)
{
    STATE_OP             = FGC_OP_UNCONFIGURED;
    stateOpProps.mode_op = FGC_OP_UNCONFIGURED;
    stateOp.stateFunc    = stateOpUC;
}



void stateOpProcess(void)
{
    StateOP       * nextState = NULL;
    StateOPtrans  * transition;
    StateOPtrans ** stateTransitions = &StateTransitions[STATE_OP][0];

    // Scan transitions for the current operational state, testing each condition in priority order

    while(   (transition = *(stateTransitions++)) != NULL   // there is another transition function,
          && (nextState  = (*transition)())       == NULL)  // and it return NULL (transition condition is false)
    {
        ;
    }

    // If a transition condition was true, then switch to the new state

    if (nextState != NULL)
    {
        stateOp.stateFunc = nextState;

        // Transition condition is true to switch to new state

        uint8_t nextStateOp = nextState(true);

        // New STATE.OP

        if(   STATE_OP    == FGC_OP_SIMULATION
           || nextStateOp == FGC_OP_SIMULATION
           || STATE_OP    == FGC_OP_TEST
           || nextStateOp == FGC_OP_TEST)
        {
            // Moving from/to SIMULATION or TEST - reset faults and warnings

            statePcResetFaults();

            statusClrAllFaults();
            statusClrAllWarnings();
        }

        STATE_OP = nextStateOp;

        // Notify the DSP of the new state

        dpcom.mcu.state_op = STATE_OP; // Move to a background function where value is updated periodically?

        // Notify CMW subscriptions

        pubPublishProperty(&PROP_STATE_OP, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, TRUE);
    }
    else
    {
        // No transition : stay in current state and run the state function with the first_call parameter set to false

        stateOp.stateFunc(false);
    }

#if (FGC_CLASS_ID == 63)
    if (   STATE_OP == FGC_OP_NORMAL
        && dpcls.dsp.load_select == 0
        && dpcom.mcu.cal.active  == FGC_CTRL_DISABLED)
    {
        statusSetUnlatched(FGC_UNL_NOMINAL_LOAD);
    }
    else
    {
        statusClrUnlatched(FGC_UNL_NOMINAL_LOAD);
    }
#endif
}



uint32_t stateOpGetState(void)
{
    return STATE_OP;
}



void stateOpSetModeOp(uint16_t mode_op)
{
    stateOpProps.mode_op = mode_op;
}



uint16_t stateOpGetModeOp(void)
{
    return stateOpProps.mode_op;
}



char const * stateOpGetStateName(uint8_t state)
{
    static const char stateName[] = "UCNLSMCLTTBTPRSO";

    return (&stateName[(uint16_t)state * 2]);
}



// ---------- Internal function definitions -----------------------------

CC_STATIC_ASSERT(FGC_OP_UNCONFIGURED ==  0, Unexpected_STATE_OP_constant_value);
CC_STATIC_ASSERT(FGC_OP_NORMAL       ==  1, Unexpected_STATE_OP_constant_value);
CC_STATIC_ASSERT(FGC_OP_SIMULATION   ==  2, Unexpected_STATE_OP_constant_value);
CC_STATIC_ASSERT(FGC_OP_CALIBRATING  ==  3, Unexpected_STATE_OP_constant_value);
CC_STATIC_ASSERT(FGC_OP_TEST         ==  4, Unexpected_STATE_OP_constant_value);


// -------------------- STATE FUNCTIONS --------------------

// STATE: UNCONFIGURED (UC)

static uint32_t stateOpUC(bool firstCall)
{
    if (firstCall)
    {
        digitalIoDisableOutputs();
    }

    return FGC_OP_UNCONFIGURED;
}



// STATE: NORMAL (NL)

static uint32_t stateOpNL(bool firstCall)
{
    if (firstCall)
    {
        digitalIoEnableOutputs();
        digitalIoDisableVsSimIntlks();

        // Clear Simulation warning

        statusClrWarnings(FGC_WRN_SIMULATION);
    }

    return FGC_OP_NORMAL;
}



// STATE: SIMULATION (SM)

static uint32_t stateOpSM(bool firstCall)
{
    if (firstCall)
    {
        digitalIoDisableOutputs();
        digitalIoEnableVsSimIntlks();

#if (FGC_CLASS_ID == 63)

        // Clear FW_DIODE and FABORT_UNSAFE faults

        if (vs.fw_diode == FGC_VDI_FAULT)
        {
            vs.fw_diode = FGC_VDI_NO_FAULT;
        }

        if (vs.fabort_unsafe == FGC_VDI_FAULT)
        {
            vs.fabort_unsafe = FGC_VDI_NO_FAULT;
        }
#endif

    }

    // Set simulation warning

    statusSetWarnings(FGC_WRN_SIMULATION);

    return FGC_OP_SIMULATION;
}



// STATE: CALIBRATING (CL)

static uint32_t stateOpCL(bool firstCall)
{
    // Run the first step of the calibration sequence

    calibrationRun();

    return FGC_OP_CALIBRATING;
}



// STATE: TEST (TT)

static uint32_t stateOpTT(bool firstCall)
{
    if (firstCall)
    {
        digitalIoDisableOutputs();

        // Clear Simulation warning

        statusClrWarnings(FGC_WRN_SIMULATION);
    }

    return FGC_OP_TEST;
}



// ------------- TRANSITION CONDITION FUNCTIONS -------------

// TRANSITION: ANY STATE to NORMAL

static StateOP* XXtoNL(void)
{
    if(    nvs_dbg.n_unset_config_props == 0
        && stateOpProps.mode_op         == FGC_OP_NORMAL)
    {
        return stateOpNL;
    }

    return NULL;
}



// TRANSITION: ANY STATE to SIMULATION

static StateOP* XXtoSM(void)
{
    if (   nvs_dbg.n_unset_config_props == 0
        && stateOpProps.mode_op         == FGC_OP_SIMULATION)
    {
        return stateOpSM;
    }

    return NULL;
}



// TRANSITION: ANY STATE to TEST

static StateOP* XXtoTT(void)
{
    if (   nvs_dbg.n_unset_config_props == 0
        && stateOpProps.mode_op         == FGC_OP_TEST)
    {
        return stateOpTT;
    }

    return NULL;
}



// TRANSITION: NORMAL to CALIBRATING

static StateOP* NLtoCL(void)
{
    if (   pcStateTest(FGC_STATE_VALID_CALIBRATION_BIT_MASK) == true
        && calibrationIsActive() == true)
    {
        return stateOpCL;
    }

    return NULL;
}



// TRANSITION: CALIBRATING to NORMAL

static StateOP* CLtoNL(void)
{
    if (calibrationIsActive() == false)
    {
        return stateOpNL;
    }

    return NULL;
}


// EOF