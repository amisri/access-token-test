//! @file  get.h
//! @brief Get Task and property Get functions

#pragma once


// ---------- Includes

#include <cmd.h>
#include <property.h>



// ---------- External structures, unions and enumerations

struct Debug_props
{
    uint32_t mem[2];      // DEBUG.MEM control: address and number of words
} debug_vars_t;



// ---------- External variable declarations

extern struct Debug_props  debug;



// ---------- External function declarations

//! Displays the diagnostic info about a specified property.
//!
//! This is a special Get function because it is activated by the get option
//! INFO rather than a property itself. It is intended to provide diagnostic
//! information about the property.

void GetPropInfo(struct cmd * c, struct prop *);


//! Displays the size info about specified property (el_size, num_els, max_els).
//!
//! This is a special Get function because it is activated by the get option
//! SIZE rather than a property itself. It is intended to provide diagnostic
//! information about the property.

uint16_t GetPropSize(struct cmd * c, struct prop *);


// EOF
