//! @file  masterClockPll.h
//! @brief RT Clock Phase Locked Loop Functions
//!
//!  Integration of the PLL. There are 3 version
//!      2 based on Digital PLL - RTC Boot and Main
//!      1 based in Analogue PLL

#pragma once



// ---------- Constants

#define GATEWAY_ALIVE_TIMEOUT_MS            45 						//!< 45ms (allow two missed packed before change of state)
#define PLD_MASTER_CLOCK_KHZ                50000L 					//!< PLD master clock : 50 MHz
#define PLD_MASTER_CLOCK_COUNT_FOR_1MS      PLD_MASTER_CLOCK_KHZ	//!< PLD master clock : 50 MHz



// ---------- External function declarations

//! This function is called from main() to set up the PLL

void masterClockPllInit(void);


//! Return true if the given read/write block at given slot is present in the crate
//! (Has been seen last time we scanned the crate).

void masterClockPllIteration(void);


// EOF
