//! @file  logEvent.h
//! @brief Interface to the library libevtlog

#pragma once


// ---------- Includes

#include <stdint.h>
#include <os.h>

#include <evtlogStructs.h>

#include <cmd.h>



// ---------- External variable declarations

extern OS_SEM * event_log_sem;



// ---------- External function declarations

//! Initialize CCLIBS event log structures and their value.

void logEventInit(void);


//! Store property changes in the event log

void logEventStoreProperties(void);


//! Store a reference fieldbus timing event
//!
//! @param[in]    delay_us       Event delay in microseconds
//! @param[in]    sub_sel        Sub-device selector for the timing event
//! @param[in]    cyc_sel        Cycle selector for the timing event
//! @param[in]    type           Event type

void logEventStoreEventRef(uint32_t delay_us,
                           uint8_t  sub_sel,
                           uint8_t  cyc_sel,
                           uint8_t  type);



//! Store a common fieldbus timing event
//!
//! @param[in]    delay_us       Event delay in microseconds
//! @param[in]    type           Event type
//! @param[in]    payload        Event payload

void logEventStoreEventCommon(uint32_t delay_us,
                              uint8_t  type,
                              uint16_t payload);


//! Store a NEXT_DEST fieldbus timing event
//!
//! @param[in]    delay_us       Event delay in microseconds
//! @param[in]    type           Event type
//! @param[in]    event_type     Event type type for NEXT_DEST
//! @param[in]    dest_mask      Destination mask

void logEventStoreEventNextDest(uint32_t delay_us,
                                uint8_t  type,
                                uint8_t  event_type,
                                uint16_t dest_mask);


//! Store an acquisition log
//!
//! @param[in]    time_stamp    Timestamp when the DIM was triggered
//! @param[in]    status        Pulse status
//! @param[in]    offset_us     Pulse offset
//! @param[in]    ref_user      Pulse user reference
//! @param[in]    meas          Pulse measurement

void logEventStorePulse(struct CC_us_time const * time_stamp,
                        char              const * status,
                        int32_t                   offset_us,
                        float                     ref_user,
                        float                     meas);


//! Store a SET commands.
//!
//! @param[in]    name          String with the property name
//! @param[in]    value         String with the property value
//! @param[in]    source        String with the command source (TRM or NET)
//! @param[in]    errnum        Result of the set command

void logEventStoreSetCmd(char     const * name,
                         char     const * value,
                         char     const * source,
                         uint16_t         errnum);


//! Store a timing pulse edge
//!
//! @param[in]    edge_time_us     Timestamp of the edge
//! @param[in]    name             String with the property name
//! @param[in]    value            String with the property value
//! @param[in]    rise             True when loggin the rising edge. False when loggin the falling edge.

void logEventStoreTimingEdge(struct CC_us_time const * edge_time_us,
                             char              const * name,
                             char              const * value,
                             bool                      rise);


//! Store a timeout event log
//!
//! @param[in]    timeout_name  Timeout name
//! @param[in]    timeout_value Timeout value

void logEventStoreTimeout(char  const * timeout_name,
                          float const   timeout_value);


//! Get the contents of the Event log in FGC format
//!
//! @param[in]    c             Command structure
//! @param[in]    is_bin        If true, return data in binary format. If false, return data in ASCII format.
//!
//! @return                     The error number

uint16_t logEventGetLogEvt(struct cmd * c, bool is_bin);


//! This function is called periodically to check if a DIM has been triggered
//! and needs to be logged in the event log

void logEventDimProcess(void);


// EOF
