//! @file  interruptHandlers.h
//! @brief FGC3 MainProg interrupt handlers

#pragma once



// ---------- External function declarations

// FIXED VECTORS

//! Exception(Supervisor Instruction) - Fixed vector

void INT_Excep_SupervisorInst(void) __attribute__((interrupt, naked));

//! Exception(Undefined Instruction) - Fixed vector

void INT_Excep_UndefinedInst(void) __attribute__((interrupt, naked));

//! Exception(Floating Point) - Fixed vector

void INT_Excep_FloatingPoint(void) __attribute__((interrupt, naked));

//! NMI  - Fixed vector

void INT_NonMaskableInterrupt(void) __attribute__((interrupt, naked));



// RELOCATABLE VECTORS

//! - vector 0 - BRK
//!
//! This vector is triggered when the core executes the BRK (break) instruction. BRK's opcode being 00,
//! it is very likely that this interrupt handler be called after a faulty jump in memory (if the core
//! starts executing junk code)

void INT_Excep_BRK(void) __attribute__((interrupt, naked));

//! - vector 1 - implemented in os_tsk.c

// void OSTskCtxSwitch(void) __attribute__ ((interrupt, naked));    // pure software, hardware reserved OSTskCtxSwitch()

// vector  2 reserved
// vector  3 reserved
// vector  4 reserved
// vector  5 reserved
// vector  6 reserved
// vector  7 reserved
// vector  8 reserved
// vector  9 reserved
// vector 10 reserved
// vector 11 reserved
// vector 12 reserved
// vector 13 reserved
// vector 14 reserved
// vector 15 reserved
void INT_Excep_BUSERR(void) __attribute__((interrupt, naked));
// vector 17 reserved
// vector 18 reserved
// vector 19 reserved
// vector 20 reserved
void INT_Excep_FCU_FCUERR(void) __attribute__((interrupt, naked));
// vector 22 reserved
void INT_Excep_FCU_FRDYI(void) __attribute__((interrupt, naked));
// vector 24 reserved
// vector 25 reserved
// vector 26 reserved
// vector 27 reserved
void INT_Excep_CMTU0_CMT0(void) __attribute__((interrupt, naked));
void INT_Excep_CMTU0_CMT1(void) __attribute__((interrupt, naked));
void INT_Excep_CMTU1_CMT2(void) __attribute__((interrupt, naked));
void INT_Excep_CMTU1_CMT3(void) __attribute__((interrupt, naked));
// vector 32 reserved
// vector 33 reserved
// vector 34 reserved
// vector 35 reserved
// vector 36 reserved
// vector 37 reserved
// vector 38 reserved
// vector 39 reserved
// vector 40 reserved
// vector 41 reserved
// vector 42 reserved
// vector 43 reserved
// vector 44 reserved
// vector 45 reserved
// vector 46 reserved
// vector 47 reserved
// vector 48 reserved
// vector 49 reserved
// vector 50 reserved
// vector 51 reserved
// vector 52 reserved
// vector 53 reserved
// vector 54 reserved
// vector 55 reserved
// vector 56 reserved
// vector 57 reserved
// vector 58 reserved
// vector 59 reserved
// vector 60 reserved
// vector 61 reserved
// vector 62 reserved
// vector 63 reserved
void INT_Excep_IRQ0(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ1(void) __attribute__((interrupt, naked));

//! Network packet reception irq
//!
//! - vector 66 - IRQ2 - Ethernet MSG IRQ
//!
//! ICU.IER08.BIT.IEN2      Interrupt Request Enable Register
//! ICU.IPR22.BIT.IPR_20    Interrupt Priority Register
//! ICU.IR066.BIT.IR        Interrupt request register 066 (status)
//! ISELR066
//!
//! The source of this interrupt is packet reception in the LAN chip

void INT_Excep_IRQ2(void) __attribute__((interrupt, naked));

//! Network time synchronisation irq
//!
//! - vector 67 - IRQ3 - Synchronisation pulse irq
//!
//! ICU.IER08.BIT.IEN3          Interrupt Request Enable Register
//! ICU.IPR23.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR067.BIT.IR            Interrupt request register 067 (status)
//! ISELR067
//!
//! The source of this interrupt is the 50Hz pulse from the timing system in the gateway

void INT_Excep_IRQ3(void) __attribute__((interrupt, naked));

//! - vector 68 - IRQ4 - MCU_~TICK - "hardware" 1ms Tick
//!
//! ICU.IER08.BIT.IEN4      Interrupt Request Enable Register
//! ICU.IPR24.BIT.IPR_20    Interrupt Priority Register
//! ICU.IR068.BIT.IR        Interrupt request register 068 (status)
//! ISELR068
//! PFCR9
//!
//! 1ms Tick used to schedule the program
//! this hardware input comes from the Xilinx Spartan 3AN FPGA (XC3S 700AN - 4FG484C),
//! it is the output of the digital PLL implemented in it
//!
//! (this interrupt must be similar to INT_Excep_TMR0_CMIA0() - vector 174, that is the backup of this
//! interrupt, 1ms generated internally by TMR0)

void INT_Excep_IRQ4(void) __attribute__((interrupt, naked));

void INT_Excep_IRQ5(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ6(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ7(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ8(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ9(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ10(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ11(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ12(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ13(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ14(void) __attribute__((interrupt, naked));
void INT_Excep_IRQ15(void) __attribute__((interrupt, naked));
// vector 80 reserved
// vector 81 reserved
// vector 82 reserved
// vector 83 reserved
// vector 84 reserved
// vector 85 reserved
// vector 86 reserved
// vector 87 reserved
// vector 88 reserved
// vector 89 reserved
// vector 90 reserved
// vector 91 reserved
// vector 92 reserved
// vector 93 reserved
// vector 94 reserved
// vector 95 reserved
void INT_Excep_WDT_WOVI(void) __attribute__((interrupt, naked));
// vector 97 reserved
void INT_Excep_AD0_ADI0(void) __attribute__((interrupt, naked));
void INT_Excep_AD1_ADI1(void) __attribute__((interrupt, naked));
void INT_Excep_AD2_ADI2(void) __attribute__((interrupt, naked));
void INT_Excep_AD3_ADI3(void) __attribute__((interrupt, naked));
// vector 102 reserved
// vector 103 reserved

//! - vector 104 - TPU0 TGI0A
//!
//! ICU.IER0D.BIT.IEN0      Interrupt Request Enable Register
//! ICU.IPR4C.BIT.IPR_20    Interrupt Priority Register
//! ICU.IR104.BIT.IR        Interrupt request register 104 (status)
//! ISELR104
//!
//! fired by TPU0.TGRA input capture/compare match

void INT_Excep_TPU0_TGI0A(void) __attribute__((interrupt)); // not naked

void INT_Excep_TPU0_TGI0B(void) __attribute__((interrupt, naked));
void INT_Excep_TPU0_TGI0C(void) __attribute__((interrupt, naked));
void INT_Excep_TPU0_TGI0D(void) __attribute__((interrupt, naked));
void INT_Excep_TPU0_TCI0V(void) __attribute__((interrupt, naked));
// TPU0 vector 109 reserved
// TPU0 vector 110 reserved
void INT_Excep_TPU1_TGI1A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU1_TGI1B(void) __attribute__((interrupt, naked));
// TPU1 vector 113 reserved
// TPU1 vector 114 reserved
void INT_Excep_TPU1_TCI1V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU1_TCI1U(void) __attribute__((interrupt, naked));
void INT_Excep_TPU2_TGI2A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU2_TGI2B(void) __attribute__((interrupt, naked));
// TPU2 vector 119 reserved
void INT_Excep_TPU2_TCI2V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU2_TCI2U(void) __attribute__((interrupt, naked));
void INT_Excep_TPU3_TGI3A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU3_TGI3B(void) __attribute__((interrupt, naked));
void INT_Excep_TPU3_TGI3C(void) __attribute__((interrupt, naked));
void INT_Excep_TPU3_TGI3D(void) __attribute__((interrupt, naked));
void INT_Excep_TPU3_TCI3V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU4_TGI4A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU4_TGI4B(void) __attribute__((interrupt, naked));
// vector 129 reserved
// vector 130 reserved
void INT_Excep_TPU4_TCI4V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU4_TCI4U(void) __attribute__((interrupt, naked));
void INT_Excep_TPU5_TGI5A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU5_TGI5B(void) __attribute__((interrupt, naked));
// vector 135 reserved
void INT_Excep_TPU5_TCI5V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU5_TCI5U(void) __attribute__((interrupt, naked));
void INT_Excep_TPU6_TGI6A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU6_TGI6B(void) __attribute__((interrupt, naked));
void INT_Excep_TPU6_TGI6C(void) __attribute__((interrupt, naked));
void INT_Excep_TPU6_TGI6D(void) __attribute__((interrupt, naked));
void INT_Excep_TPU6_TCI6V(void) __attribute__((interrupt, naked));
// vector 143 reserved
// vector 144 reserved
void INT_Excep_TPU7_TGI7A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU7_TGI7B(void) __attribute__((interrupt, naked));
// vector 147 reserved
// vector 148 reserved
void INT_Excep_TPU7_TCI7V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU7_TCI7U(void) __attribute__((interrupt, naked));
void INT_Excep_TPU8_TGI8A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU8_TGI8B(void) __attribute__((interrupt, naked));
// vector 153 reserved
void INT_Excep_TPU8_TCI8V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU8_TCI8U(void) __attribute__((interrupt, naked));
void INT_Excep_TPU9_TGI9A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU9_TGI9B(void) __attribute__((interrupt, naked));
void INT_Excep_TPU9_TGI9C(void) __attribute__((interrupt, naked));
void INT_Excep_TPU9_TGI9D(void) __attribute__((interrupt, naked));
void INT_Excep_TPU9_TCI9V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU10_TGI10A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU10_TGI10B(void) __attribute__((interrupt, naked));
// vector 163 reserved
// vector 164 reserved
void INT_Excep_TPU10_TCI10V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU10_TCI10U(void) __attribute__((interrupt, naked));
void INT_Excep_TPU11_TGI11A(void) __attribute__((interrupt, naked));
void INT_Excep_TPU11_TGI11B(void) __attribute__((interrupt, naked));
// vector 169 reserved
void INT_Excep_TPU11_TCI11V(void) __attribute__((interrupt, naked));
void INT_Excep_TPU11_TCI11U(void) __attribute__((interrupt, naked));
// vector 172 reserved
// vector 173 reserved

//! - vector 174 - CMIA0 - Compare Match A Interrupt - "software" 1ms Tick
//!
//! ICU.IER15.BIT.IEN6      Interrupt Request Enable Register
//! ICU.IPR68.BIT.IPR_20    Interrupt Priority Register
//! ICU.IR174.BIT.IR        Interrupt request register 174 (status)
//! ISELR174
//!
//! TMR0 is used as a 16 bit counter incremented by PCLK 50MHz counting 50000 = 1ms
//!
//! It is programmed to generate an interrupt every 1ms
//!
//! (backup) internally generated 1ms Tick used to schedule the program
//! (so this one must be similar to INT_Excep_IRQ4() - vector 68 - MCU_~TICK)
//!
//! It is used when the Xilinx Spartan 3AN FPGA (XC3S 700AN - 4FG484C) is unprogrammed and no tick is present.

void INT_Excep_TMR0_CMIA0(void) __attribute__((interrupt)); // not naked

//! - vector 175 - CMIB0 - Compare Match B Interrupt
//!
//! ICU.IER15.IEN7
//! ICU.IPR68.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR175.BIT.IR            Interrupt request register 175 (status)
//! ISELR175

void INT_Excep_TMR0_CMIB0(void) __attribute__((interrupt, naked));

//! - vector 176 - OVI0 - Timer 0 Overflow Interrupt
//!
//! TMR0.TCNT                   overflow INT enable
//! ICU.IER16.BIT.IEN0          Interrupt Request Enable Register
//! ICU.IPR68.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR176.BIT.IR            Interrupt request register 176 (status)

void INT_Excep_TMR0_OVI0(void) __attribute__((interrupt, naked));

//! - vector 177 - CMIA1 - Compare Match A Interrupt
//!
//! ICU.IER16.BIT.IEN1          Interrupt Request Enable Register
//! ICU.IPR69.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR177.BIT.IR            Interrupt request register 177 (status)
//! ISELR177

void INT_Excep_TMR1_CMIA1(void) __attribute__((interrupt, naked));

//! - vector 178 - CMIB1 - Compare Match B Interrupt
//!
//! ICU.IER16.BIT.IEN2          Interrupt Request Enable Register
//! ICU.IPR69.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR178.BIT.IR            Interrupt request register 178 (status)
//! ISELR178

void INT_Excep_TMR1_CMIB1(void) __attribute__((interrupt, naked));

//! - vector 179 - OVI1 - Timer 1 Overflow Interrupt
//!
//! TMR1.TCNT                   overflow INT enable
//! ICU.IER16.BIT.IEN3          Interrupt Request Enable Register
//! ICU.IPR69.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR179.BIT.IR            Interrupt request register 179 (status) 8870B3h 8

void INT_Excep_TMR1_OVI1(void) __attribute__((interrupt, naked));

//! - vector 180 - CMIA2 - Compare Match A Interrupt
//!
//! ICU.IER16.BIT.IEN4          Interrupt Request Enable Register
//! ICU.IPR6A.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR180.BIT.IR            Interrupt request register 180 (status)
//! ISELR180

void INT_Excep_TMR2_CMIA2(void) __attribute__((interrupt, naked));

//! - vector 181 - CMIB2 - Compare Match B Interrupt
//!
//! ICU.IER16.BIT.IEN5          Interrupt Request Enable Register
//! ICU.IPR6A.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR181.BIT.IR            Interrupt request register 181 (status)
//! ISELR181

void INT_Excep_TMR2_CMIB2(void) __attribute__((interrupt, naked));

//! - vector 182 - OVI2 - Timer 2 Overflow Interrupt
//!
//! TMR2.TCNT                   overflow INT enable
//! ICU.IER16.BIT.IEN6          Interrupt Request Enable Register
//! ICU.IPR6A.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR182.BIT.IR            Interrupt request register 182 (status)

void INT_Excep_TMR2_OVI2(void) __attribute__((interrupt, naked));

//! - vector 183 - CMIA3 - Compare Match A Interrupt
//!
//! ICU.IER16.BIT.IEN7          Interrupt Request Enable Register
//! ICU.IPR6B.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR183.BIT.IR            Interrupt request register 183 (status)
//! ISELR183

void INT_Excep_TMR3_CMIA3(void) __attribute__((interrupt, naked));

//! - vector 184 - CMIB3 - Compare Match B Interrupt
//!
//! ICU.IER17.BIT.IEN0          Interrupt Request Enable Register
//! ICU.IPR6B.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR184.BIT.IR            Interrupt request register 184 (status)
//! ISELR184

void INT_Excep_TMR3_CMIB3(void) __attribute__((interrupt, naked));

//! - vector 185 - OVI3 - Timer 3 Overflow Interrupt
//!
//! TMR3.TCNT                   overflow INT enable
//! ICU.IER17.BIT.IEN1          Interrupt Request Enable Register
//! ICU.IPR6B.BIT.IPR_20        Interrupt Priority Register
//! ICU.IR185.BIT.IR            Interrupt request register 185 (status)
//!
//! TMR3 will count [0..255] incremented by TMR2 count match (1us)
//! and generating an interrupt when overflows
//!
//! TMR2 will count [0..49] increment by PCLK (50MHz,20ns), the period is 1us
//!
//! we count the TMR3 overflows to enhance USLEEP()
//!
//! This interrupt is set to level 7, and never blocked
//!

void INT_Excep_TMR3_OVI3(void) __attribute__((interrupt));  // not naked

// vector 186 reserved
// vector 187 reserved
// vector 188 reserved
// vector 189 reserved
// vector 190 reserved
// vector 191 reserved
// vector 192 reserved
// vector 193 reserved
// vector 194 reserved
// vector 195 reserved
// vector 196 reserved
// vector 197 reserved

//! - vector 198 - DMAC_DMTEND0
//!
//! DMA0 is used to send data from circular buffer in external ram to LAN chip
//! At end of completion we restart DMA if needed (circular goes back to zero, DMA is split in 2 steps),
//! and we update the queue output.
//!
//!    ICU.IER18.BIT.IEN6          Interrupt Request Enable Register

void INT_Excep_DMAC_DMTEND0(void) __attribute__((interrupt, naked));

//! - vector 199 - DMAC_DMTEND1
//!
//! DMA1 is used to send data from circular buffer in external ram to LAN chip
//! At end of completion we update the queue output index.
//!
//!    ICU.IER18.BIT.IEN6          Interrupt Request Enable Register

void INT_Excep_DMAC_DMTEND1(void) __attribute__((interrupt, naked));

void INT_Excep_DMAC_DMTEND2(void) __attribute__((interrupt, naked));
void INT_Excep_DMAC_DMTEND3(void) __attribute__((interrupt, naked));
// vector 202 reserved
// vector 203 reserved
// vector 204 reserved
// vector 205 reserved
// vector 206 reserved
// vector 207 reserved
// vector 208 reserved
// vector 209 reserved
// vector 210 reserved
// vector 211 reserved
// vector 212 reserved
// vector 213 reserved

//! - vector 214
//!   ICU.IER1A.BIT.IEN6           Interrupt Request Enable Register
//!   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
//!
//! UART0

void INT_Excep_SCI0_ERI0(void) __attribute__((interrupt, naked));

//! - vector 215
//!   ICU.IER1A.IEN7
//!   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
//!
//! ISELR215
//! UART0

void INT_Excep_SCI0_RXI0(void) __attribute__((interrupt, naked));

//! - vector 216
//!   ICU.IER1B.BIT.IEN0           Interrupt Request Enable Register
//!   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
//!
//! ISELR216
//! UART0

void INT_Excep_SCI0_TXI0(void) __attribute__((interrupt, naked));

//! - vector 217
//!   ICU.IER1B.BIT.IEN1           Interrupt Request Enable Register
//!   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
//!
//! UART0

void INT_Excep_SCI0_TEI0(void) __attribute__((interrupt, naked));

void INT_Excep_SCI1_ERI1(void) __attribute__((interrupt, naked));

//! - vector 219
//!    ICU.IER1B.IEN3              Interrupt Request Enable Register
//!    ICU.IPR81.BIT.IPR_20        Interrupt Priority Register
//!    ICU.IR220.BIT.IR            Interrupt request register 219 (status)
//!
//! ISELR219
//! UART1

void INT_Excep_SCI1_RXI1(void) __attribute__((interrupt, naked));

//! - vector 220
//!    ICU.IER1B.BIT.IEN4          Interrupt Request Enable Register
//!    ICU.IPR81.BIT.IPR_20        Interrupt Priority Register
//!    ICU.IR220.BIT.IR            Interrupt request register 220 (status)
//!
//! ISELR220
//! UART1

void INT_Excep_SCI1_TXI1(void) __attribute__((interrupt, naked));

void INT_Excep_SCI1_TEI1(void) __attribute__((interrupt, naked));
void INT_Excep_SCI2_ERI2(void) __attribute__((interrupt, naked));
void INT_Excep_SCI2_RXI2(void) __attribute__((interrupt, naked));
void INT_Excep_SCI2_TXI2(void) __attribute__((interrupt, naked));
void INT_Excep_SCI2_TEI2(void) __attribute__((interrupt, naked));
void INT_Excep_SCI3_ERI3(void) __attribute__((interrupt, naked));
void INT_Excep_SCI3_RXI3(void) __attribute__((interrupt, naked));
void INT_Excep_SCI3_TXI3(void) __attribute__((interrupt, naked));
void INT_Excep_SCI3_TEI3(void) __attribute__((interrupt, naked));
void INT_Excep_SCI4_ERI4(void) __attribute__((interrupt, naked));
void INT_Excep_SCI4_RXI4(void) __attribute__((interrupt, naked));
void INT_Excep_SCI4_TXI4(void) __attribute__((interrupt, naked));
void INT_Excep_SCI4_TEI4(void) __attribute__((interrupt, naked));
void INT_Excep_SCI5_ERI5(void) __attribute__((interrupt, naked));
void INT_Excep_SCI5_RXI5(void) __attribute__((interrupt, naked));
void INT_Excep_SCI5_TXI5(void) __attribute__((interrupt, naked));
void INT_Excep_SCI5_TEI5(void) __attribute__((interrupt, naked));

//! - vector 238 (level)
//!    ICU.IER1D.BIT.IEN6          Interrupt Request Enable Register
//!    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
//!    ICU.IR238.BIT.IR            Interrupt request register 238 (status)

void INT_Excep_SCI6_ERI6(void) __attribute__((interrupt, naked));

//! - vector 239 (edge)
//!    ICU.IER1D.BIT.IEN7          Interrupt Request Enable Register
//!    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
//!    ICU.IR239.BIT.IR            Interrupt request register 239 (status)
//!
//! ISELR239
//! UART6

void INT_Excep_SCI6_RXI6(void) __attribute__((interrupt, naked));

//! - vector 240 (edge)
//!    ICU.IER1E.BIT.IEN0          Interrupt Request Enable Register
//!    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
//!    ICU.IR240.BIT.IR            Interrupt request register 240 (status)
//!
//! ISELR240
//! UART6

void INT_Excep_SCI6_TXI6(void) __attribute__((interrupt, naked));

//! - vector 241 (level)
//!    ICU.IER1E.BIT.IEN1          Interrupt Request Enable Register
//!    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
//!    ICU.IR241.BIT.IR            Interrupt request register 241 (status)

void INT_Excep_SCI6_TEI6(void) __attribute__((interrupt, naked));

// vector 242 reserved
// vector 243 reserved
// vector 244 reserved
// vector 245 reserved
void INT_Excep_RIIC0_EEI0(void) __attribute__((interrupt, naked));
void INT_Excep_RIIC0_RXI0(void) __attribute__((interrupt, naked));
void INT_Excep_RIIC0_TXI0(void) __attribute__((interrupt, naked));
void INT_Excep_RIIC0_TEI0(void) __attribute__((interrupt, naked));
void INT_Excep_RIIC1_EEI1(void) __attribute__((interrupt, naked));
void INT_Excep_RIIC1_RXI1(void) __attribute__((interrupt, naked));
void INT_Excep_RIIC1_TXI1(void) __attribute__((interrupt, naked));
void INT_Excep_RIIC1_TEI1(void) __attribute__((interrupt, naked));
// vector 254 reserved
// vector 255 reserved


// EOF
