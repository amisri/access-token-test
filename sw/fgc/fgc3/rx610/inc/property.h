//! @file  property.h
//! @brief Property structures of the MCU

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>



// ---------- Constants

// Symbol Table related constants

#define ST_TLPROP                       0               //!< Symbol is a top level property label
#define ST_LLPROP                       1               //!< Symbol is a low level property label
#define ST_CONST                        2               //!< Symbol is an integer constant
#define ST_GETOPT                       3               //!< Symbol is an get option
#define ST_MAX_SYM_LEN                  20              //!< Maximum symbol length
#define ST_NEAT_FORMAT_TAB_SZ           4               //!< Tab size used for neat formatting in the console

// Dynamic property flags

#define DF_TIMESTAMP_SELECT             0x04            //!< Property uses a timestamp different from time now.
#define DF_NVS_SET                      0x08            //!< Non-volatile property has been set
#define DF_CFG_CHANGED                  0x10            //!< Non-volatile config property has been changed
#define DF_SET_BY_DALLAS_ID             0x20            //!< Property set by Dallas ID (barcode)
#define DF_SUBSCRIBED                   0x40            //!< Property is subscribed

// Used in defprops.h

#define DEFPROPS_FUNC_SET(func)   uint16_t func (struct cmd *, struct prop *)
#define DEFPROPS_FUNC_GET(func)   uint16_t func (struct cmd *, struct prop *)
#define DEFPROPS_FUNC_SETIF(func) uint16_t func (struct cmd *)



// ---------- External structures, unions and enumerations

//! Timestamp selector used if DF_TIMESTAMP_SELECT dynamic flag is set for the property.

enum timestamp_select
{
    TIMESTAMP_NOW,                                      //!< Time now (default)
    TIMESTAMP_CURRENT_CYCLE,                            //!< Time of start of current cycle
    TIMESTAMP_PREVIOUS_CYCLE,                           //!< Time of start of previous cycle
    TIMESTAMP_LOG_CAPTURE,                              //!< Time of start of capture log
    TIMESTAMP_LOG_OASIS,                                //!< Time of start of LOG.OASIS buffer
    TIMESTAMP_LOG_OASIS_FAST,                           //!< Time of start of LOG.OASIS buffer with fast measurements
    TIMESTAMP_DIMS,                                     //!< Time of the DIM acquisition cycle
};

//! Type to be used for the property size and index.
//! Note the distinction between prop_size_t and uintptr_t: the former is always a
//! size expressed in number of elements, whereas the latter (used as the type of
//! prop->n_elements) is either a size OR a pointer to a size.

typedef uint32_t    prop_size_t;

//! Symbol list

struct sym_lst
{
    uint32_t            sym_idx;                        //!< Symbol indexsettable
    uint32_t            value;                          //!< Symbol list constant value
};

//! Structure defining properties initialized with default values

struct init_prop
{
    struct prop       * prop;                           //!< Pointer to property
    prop_size_t         n_elements;                     //!< Number of elements
    void              * data;                           //!< Pointer to initialisation data
};

//! Structure definining properties

struct prop
{
    uint16_t            sym_idx;                        //!< Index in sym_tab[] for property's symbol
    uint16_t            flags;                          //!< Bit flags PF_XXXX
    uint8_t             type;                           //!< Type from enum prop_type_e
    uint8_t     const   setif_func_idx;                 //!< Index from enum setif_func_e
    uint8_t             set_func_idx;                   //!< Index from enum set_func_e
    uint8_t             get_func_idx;                   //!< Index from enum get_func_e
    uintptr_t           n_elements;                     //!< Property size in nb of elements, or pointer to the prop size
    prop_size_t         max_elements;                   //!< Max property size in nb of elements
    void        const * range;                          //!< Pointer to sym_lst or limits
    void              * value;                          //!< Pointer to property value
    uint16_t    const   dsp_idx;                        //!< Type from enum dsp_xxx_prop_e
    uint16_t            nvs_idx;                        //!< Non-volatile storage index
    uint16_t            dynflags;                       //!< Dynamic flags
    uint8_t             sub_idx;                        //!< Index into subscription table
    bool                is_setting;                     //!< True if the property or any of its children has a set function
    struct prop       * parent;                         //!< Pointer to parent property structure
};


// EOF
