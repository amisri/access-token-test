//! @file  sta.h
//! @brief Power converter state functionality

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <definfo.h>
#include <defconst.h>
#include <time_fgc.h>



// ---------- External structures, unions and enumerations

typedef struct pc_state_vars
{
    struct CC_us_time   timestamp_us;                   // Iteration timestamp (for logging)
    struct CC_ms_time   timestamp_ms;                   // Iteration timestamp (with millisecond precision)
    struct CC_us_time   copy_log_capture_cyc_chk_time;  // Copy of the last value of LOG.CAPTURE.CYC_CHK_TIME
    uint32_t            crash_code;                     // SW crash codes (Crash(0xBADx) inherited from FGC2) will be redirected to a panic on FGC3
    uint16_t            cal_active;                     // CAl.ACTIVE property
    uint16_t            num_faults[16];                 // Number of faults
    uint16_t            copy_log_capture_state;         // Copy of the last value of LOG.CAPTURE.STATE
    int16_t             copy_log_capture_user;          // Copy of the last value of LOG.CAPTURE.USER
    uint16_t            cal_type;                       // Calibration type (CAL_REQ_ADC16, CAL_REQ_ADC16, CAL_REQ_DAC)
    uint16_t            cal_chan;                       // Calibration channel (0=A, 1=B)
    uint16_t            cal_seq_idx;                    // Calibration sequence index
    uint16_t            cal_counter;                    // Calibration down counter
    uint16_t            dcct_flts;                      // DCCT faults latch
    bool                sec_f;                          // Start of new second flag

} pc_state_vars_t;


// ---------- External variable declarations

//! State variables structure
extern pc_state_vars_t  sta;

// EOF
