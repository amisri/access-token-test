//! @file  bgp.h
//! @brief FGC MCU Software - Background processing Task
//!
//! The background task takes the role of an idle task.  It never blocks and is used to do
//! background surveillance of the FGC.

#pragma once



// ---------- External function declarations

//! This function is the Background processing Task.  It is the lowest priority task and it never blocks, so
//!  it plays the role of an Idle task. It is responsible for background monitoring of the FGC:
//!
//!        - MCU registers  (Not yet implemented)
//!        - Task stack consumption

void bgpTask(void * unused);


// EOF
