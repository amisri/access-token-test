//! @file  prop.h
//! @brief Functions to manipulate the properties and symbols

#pragma once


// ---------- Includes

#include <cmd.h>
#include <defprops.h>
#include <property.h>



// ---------- Constants

// Valid array index: the following mask is used to test if a property element index is valid or not
// On FGC3 the max property size and therefore the max element index is 4294967295.

#define PROP_ARRAY_IDX_MASK    (0xFFFFFFFF)



// ---------- External structures, unions and enumerations

//! PropMap() function arguments

enum prop_map_specifier
{
    PROP_MAP_ALL,
    PROP_MAP_ONLY_PARENTS,
    PROP_MAP_ONLY_CHILDREN
};

typedef void (*prop_map_func)(uint16_t lvl, struct prop * p);


typedef struct prop_debug
{
    uint16_t access_timeouts;   // FGC.DEBUG.DSP_PROP.TIMEOUT
} prop_debug_t;



// ---------- External variable declarations

extern prop_debug_t prop_debug;



// ---------- External function declarations

//! This function is used to find a property from its name.  It uses the pcm command structure in a
//! special way and is separate from the tcm and fcm structures so that it can be used by the Dallas task
//! in parallel with FIP and serial command processing.  It is also used during startup to to find non-volatile
//! storage properties.
//!
//! Input parameters:
//!
//!       int8_t *propname         Pointer to string containing the name of the property name
//!
//! Output parameters (if successful):
//!
//!       pcm.prop                Pointer to identified property
//!       pcm.n_arr_spec          Number of array specifiers (0,1,2)
//!       pcm.from                Array start index
//!       pcm.to                  Array end index specifier
//!       pcm.sym_idxs            Array of symbol indexes for parent properties leading to identified property
//!       pcm.si_idx              Number of entries in c->sym_idxs
//!       pcm.n_prop_name_lvls    Number of entries in c->sym_idxs (this is number of levels in property name)
//!
//! Return value:
//!
//!       0                       Successful match
//!       FGC_NO_DELIMITER        End of buffer found before a valid delimiter
//!       FGC_SYNTAX_ERROR        Token buffer full or contains a space before a valid delimiter
//!       FGC_NO_SYMBOL           No symbol found
//!       FGC_UNKNOWN_SYM         Token didn't match any known property symbol
//!       FGC_BAD_ARRAY_IDX       The array specifier has a bad syntax or values

uint16_t PropFind(const char * propname);


//! This function is used to identify a property from it's symbolic name, read from the command buffer.
//!
//! Input parameters:
//!
//!       struct cmd *c           Pointer to command structure (fcm or tcm).
//!
//! Output parameters (if successful):
//!
//!       c->prop                 Pointer to identified property
//!       c->cyc_sel              Cycle selector index (set to user idx)
//!       c->sub_sel              Sub-device selector index (set to sub_sel)
//!       c->n_arr_spec           Number of array specifiers (0,1,2)
//!       c->from                 Array start index
//!       c->to                   Array end index specifier
//!       c->step                 Array step specifier
//!       c->sym_idxs             Array of symbol indexes for parent properties leading to identified property
//!       c->si_idx               Number of entries in c->sym_idxs
//!       c->n_prop_name_lvls     Number of entries in c->sym_idxs (this is number of levels in property name)
//!       c->header               Command header
//!       c->cmd_type             Command type from low nibble of command header
//!
//! Return value:
//!
//!       0                       Successful match
//!       FGC_NO_DELIMITER        End of buffer found before a valid delimiter
//!       FGC_SYNTAX_ERROR        Token buffer full or contains a space before a valid delimiter
//!       FGC_NO_SYMBOL           No symbol found
//!       FGC_INVALID_SUBDEV      Sub-device number out of valid range
//!       FGC_UNKNOWN_SYM         Token didn't match any known property symbol
//!       FGC_BAD_ARRAY_IDX       The array specifier has a bad syntax or values

uint16_t PropIdentifyProperty(struct cmd * c);


//! This function is used to identify the symbol in the command token buffer.  A symbol can either be a
//! constant, a top level property name, a low level property name, or a get option.
//!
//! Input parameters:
//!
//!       struct cmd *c           Pointer to command structure containing token to identify.
//!       int8_t *delim            If not NULL then this will be used as the delimiter string
//!       uint16_t sym_type         Symbol type selection: ST_CONST, ST_GETOPT, ST_TLPROP OR ST_LLPROP.
//!
//! Output parameters:
//!
//!       uint16_t *sym             *sym will be set to the index of the symbol if a match is made.
//!
//! Return value:
//!
//!       0                       Successful match - symbol index in *sym
//!       FGC_CMD_RESTARTED       Unexpected first-command packet received (new command starting)
//!       FGC_NO_DELIMITER        End of buffer found before a valid delimiter
//!       FGC_SYNTAX_ERROR        Token buffer full or space found before a valid delimiter
//!       FGC_NO_SYMBOL           Token buffer empty
//!       FGC_UNKNOWN_SYM         Token didn't match any symbol with given type

uint16_t PropIdentifySymbol(struct cmd * c, const char * delim, uint16_t sym_type, uint16_t * sym);


//!  This function is used to identify the array range and step specifiers.
//!
//!  Input parameters:
//!
//!        struct cmd *c           Pointer to command structure.
//!
//!  Output parameters:
//!
//!        c->n_arr_spec           Number of array specifiers (0,1,2)
//!        c->from                 Array start index
//!        c->to                   Array end index specifier
//!        c->step                 Array step specifier (for Set, step must be 1)
//!
//!  Return value:
//!
//!        0                       Successful identification of array range
//!        FGC_BAD_ARRAY_IDX       The array specified has bad syntax or it's a parent property and a set cmd.
//!
//!  Notes:
//!
//!        The following array specifier syntaxes are allowed:
//!
//!            Cmd Buf             from            to              n_arr_spec
//!            no specifier        0               max_els-1       0
//!                [x]             x               x               2
//!                [x,]            x               max_els-1       1
//!                [x,x]           x               x               2
//!                [x,y]           x               y               2     (x != y)
//!
//!        In all cases x and y must be positive.

uint16_t PropIdentifyArray(struct cmd * c);


//!  This function is used to identify the ASCII user index specifier in the command if it is present.  This
//!  has the syntax "(USER)".  The gateway will supply a user from CMW via the user byte in the packet header
//!  and if this is non-zero, it will always take priority over the ASCII user.
//!
//!  If the ASCII user specifier is empty "()" and the property is NOT a parent then the PPM_ALL_USERS
//!  value will be returned forcing a get of all users for that property.
//!
//!  A non-zero user is only acceptable for this command/property if a complex condition is met:
//!
//!      DEVICE.PPM property is ENABLED and
//!            SUB command or
//!            SET command and PPM property, or
//!            GET or GET_SUB command and (PARENT or PPM property)
//!
//!  If the condition is not met then the user (in c->cyc_sel) is forced to zero.
//!
//!  Input parameters:
//!
//!        struct cmd *c           Pointer to command structure.
//!        c->cyc_sel              sub_dev index
//!        user_from_header        user from command packet header (take priority over user inside command)
//!
//!  Output parameters:
//!
//!        c->cyc_sel              User index (sub_dev index must be zero)
//!
//!  Return value:
//!
//!        0                       Successful identification of user index or no index supplied
//!        FGC_BAD_CYCLE_SELECTOR  The cycle index specified has bad syntax or is not allowed
//!        FGC_NOT_IMPL            A sub_dev index has been supplied - sub_dev properties cannot be PPM
//!        FGC_SYNTAX_ERROR        Invalid character after user index

uint16_t PropIdentifyUser(struct cmd * c, uint16_t user_from_header);


//! This function will try to identify the get options which follow a property name.  Options must be
//! separated by spaces.
//!
//! Input parameters:
//!
//!       struct cmd *c           Pointer to command structure
//!       struct prop *p          Pointer to property structure
//!
//! Output parameters:
//!
//!         c->getopts            Get option flags
//!       c->token_delim          Array delimiter
//!
//! Return value:
//!
//!       0                       Successful identification of get options
//!       FGC_BAD_GET_OPT         Invalid get option encountered

uint16_t PropIdentifyGetOptions(struct cmd * c, struct prop * p);


//! This function will return the number of elements contained in the property.  This may either be directly
//! in p->n_elements, or in *p->n_elements, according to p->flags & PF_INDIRECT_N_ELS.

prop_size_t PropGetNumEls(struct cmd * c, struct prop const * p);


//! This function will set the number of elements contained in the property. NB: p->n_elements may either be
//! the number of elements, or a pointer to it, according to (p->flags & PF_INDIRECT_N_ELS).

void PropSetNumEls(struct cmd * c, struct prop * p, prop_size_t n_elements);


//! This function is used to directly set a property value. The value may NOT be greater than PROP_BLK_SIZE
//! bytes long.

void PropSet(struct cmd * c, struct prop * p, const void * value, prop_size_t n_elements);


//! This function is used to directly set a property value.  The value may be greater than PROP_BLK_SIZE bytes long.

void PropSetFar(struct cmd * c, struct prop * p, const void * value, prop_size_t n_elements);


//! This function will recover the specified element of the specified property.  If the value block
//! containing the element (which may not span blocks) has not already been recovered, the function uses
//! PropBlkGet() to read the PROP_BLK_SIZE byte block.  If the *set_f is set, then before recovering a new block,
//! the existing block will be written back to the property value buffer and *set_f will be cleared.
//! The function writes a pointer to the required element data in argument data_ptr and sets:
//!
//!       c->cached_prop          Currently cached property
//!       c->prop_blk_idx         Currently cached block index
//!       c->cyc_sel              Multiplexor index
//!       c->n_elements           Set when caching a block to number of elements in the property
//!       *set_f                  Cleared if current block is saved before reading new block
//!
//! The function returns a status (FGC_OK_NO_RSP if no error)

uint16_t PropValueGet(struct cmd * c, struct prop * p, bool * set_f, prop_size_t el_idx, uint8_t ** data_ptr);


//! This function will get a block of property data into the property block buffer linked to the command
//! structure.  The property may be in the MCU or DSP.
//!
//! Inputs:       c->cached_prop          Pointer to property
//!               c->prop_blk_idx         Block being got
//!               c->cached_prop->flags   Property flags
//!               c->cyc_sel              Multiplexor index
//!
//!               n_elems_ptr             Pointer for the returned number of elements
//!
//! Returns:      An error status.

uint16_t PropBlkGet(struct cmd * c, prop_size_t * n_elems_ptr);


//! This function will set a block of property data into the property value.  The property may be in the
//! MCU or DSP.  If the calling function sets the last_blk_f, the property length will be checked to see
//! if the property has shrunk.  If so, trailing values will be cleared to zero.  If the property has grown,
//! the length will be set.
//!
//! Inputs:       c->cached_prop          Pointer to property
//!               c->prop_blk_idx         Block being set
//!               c->cyc_sel              Multiplexor index
//!               c->cached_prop->flags   Property flags
//!               last_blk_f              0 or FGC_FIELDBUS_FLAGS_LAST_PKT
//!
//! Returns: status (FGC_OK_NO_RSP if no error was detected)

uint16_t PropBlkSet(struct cmd * c, prop_size_t n_elements, bool set_f, uint16_t last_blk_f);


//! This function will wait for the property data buffer to become available.  The buffer may be in use by
//! the DSP to set/get a DSP property.  If the buffer is busy, the function will suspend the task having
//! first set a flag to request that the msTask() resume the task on the next millisecond.
//!
//! Input:   command structure struct cmd
//!
//! Returns: DSP status, FGC_DSP_NOT_AVL if DSP is OFF, FGC_OK_NO_RSP otherwise
//!          That status should not be considered if the property is not a DSP property

uint16_t PropBufWait(struct cmd * c);


//! This function will apply function 'func' on all FGC properties which comply with the argument 'specifier'
//! (specifier = all properties, only parent properties or only child properties).
//! In the case the map applies to parent and child properties, the map function is called on the parent
//! BEFORE calling its children.

void PropMap(prop_map_func func, enum prop_map_specifier specifier);

//! This function initialises the Spy multiplexer channel property SPY.MPX both
//! on startup and on the command S SPY RESET.
//!
//! @param c Command received.

void propInitSpyMpx(struct cmd * c);

//! This function sets the parent pointers in the property structures.
//! The parent links are needed for subscriptions.

void propInitPropertyTree(void);


// EOF
