//! @file  logCycle.h
//! @brief Interface to the library libcyclog

#pragma once


// ---------- Includes

#include <stdint.h>

#include <libcyclog.h>



// ---------- Platform/class specific function declarations

//! Class specific callback returning the function type

CYCLOG_func_type_callback logCycleClassFuncTypeCallback;

//! Class specific callback returning the status

CYCLOG_status_callback    logCycleClassStatusCallback;

//! Returns the bitmask of status that result in Ok
//!
//! @returns        Ok status bitmask

uint32_t logCycleClassGetStatusOk(void);


//! Returns the bitmask of status that result in Warning
//!
//! @returns        Warning status bitmask

uint32_t logCycleClassGetStatusWarnings(void);


//! Returns the bitmask of status that result in Fault
//!
//! @returns        Fault status bitmask

uint32_t logCycleClassGetStatusFaults(void);



// ---------- External function declarations

//! Initialize the cycle logs

void logCycleInit(void);


//! Store a cycle log

void logCycleStore(void);


//! Identify the first cycle of the super-cycle

void logCycleStartSuperCycle(void);


//!< Get one index from the cycle log.
//!
//! The destination buffer must be at least CYCLOG_READ_FGC_FIXED_LEN characters long
//!
//! @param[in]      log_index     Log index to copy into read_buf
//! @param[in]      read_buf      Pointer to destination buffer.
//! @param[in]      read_buf_len  Destination buffer length.

void logCycleGet(uint32_t const log_index, char * read_buf, uint32_t read_buf_len);


// EOF
