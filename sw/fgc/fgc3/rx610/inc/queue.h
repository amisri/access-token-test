//! @file  queue.h
//! @brief Circular queue

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>



// ---------- External structures, unions and enumerations

//! Circular queue indecies and status structure

struct Queue
{
    bool        is_full;         //!< Queue full flag
    uint16_t    in_idx;          //!< Input index  0->(len-1)
    uint16_t    out_idx;         //!< Output index 0->(len-1)
    uint16_t    dec_mask;        //!< Decrement mask (len-1) where len must be power of 2
    char      * buf;             //!< Start of circular buffer
};


// EOF
