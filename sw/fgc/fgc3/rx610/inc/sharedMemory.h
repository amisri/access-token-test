//! @file  sharedMemory.h
//! @brief Runtime values shared between Boot and Main

#pragma once


// ---------- Includes

#include <stdint.h>

#include <codes_holder.h>
#include <definfo.h>



// ---------- External structures, unions and enumerations

enum Shared_memory_mainprog_seq
{
    SEQUENCE_STAY_IN_MAIN,              //!< 0 - Starting (the Boot will clear the value before launching the main program)
    SEQUENCE_STAY_IN_BOOT,              //!< 1 - Stay in boot (S DEVICE.BOOT executed)
    SEQUENCE_BOOT_RUNNING,              //!< 2
    SEQUENCE_MAIN_RUNNING,              //!< 3 - Running (the main program will set this once the ID scan is complete)
    SEQUENCE_TEST_IN_BOOT,              //!< 4
    SEQUENCE_MAIN_INIT   ,              //!< 5 - Initializaing the MAIN
};


enum Shared_memory_network_type
{
    NETWORK_TYPE_FIP,
    NETWORK_TYPE_ETHERNET
};


//! Shared memory structure

struct Shared_memory
{
    uint16_t      task_id;             //!< Task identifier updated at context switch
    uint16_t      isr_mask;            //!< ISR mask updated every time there is an interrupt
    uint16_t      boot_seq;            //!< At dev.h defined BOOT_PWR 0, BOOT_NORMAL 1, BOOT_UNKNOWN 0xFF
    uint16_t      mainprog_seq;        //!< enum Shared_memory_mainprog_seq
    uint16_t      analog_card_model;   //!< FGC.INTERFACE    Analog card model
    uint16_t      network_card_model;  //!< Network card model
    uint32_t      power_on_utc;        //!< TIME. UTC timestamp that the device was powered on
    uint32_t      run_software_utc;    //!< UTC timestamp that the device was started
    uint32_t      power_time;          //!< DEVICE.PWR_TIME  The number of seconds since the device was powered up.
    uint32_t      run_time;            //!< DEVICE.RUN_TIME  The number of seconds since the device was reset
    uint32_t      codes_version[SHORT_LIST_CODE_LAST - 1];  //!< CODE.VERSION  Code versions
    uint16_t      code_locked_count;   //!< CODE.FLASH.LOCKS  Number of locked code_holders

    union Shared_memory_stack_use
    {
        struct Shared_memory_stack_use_task
        {
            uint8_t   mst;              //!< Millisecond task stack usage
            uint8_t   fbs;              //!< Fieldbus task stack usage
            uint8_t   sta;              //!< State task stack usage
            uint8_t   pub;              //!< Publication task stack usage
            uint8_t   fcm;              //!< Fieldbus command task stack usage
            uint8_t   tcm;              //!< Terminal command task stack usage
            uint8_t   scivs;            //!< SCIVS task stack usage
            uint8_t   dls;              //!< Dallas task stack usage
            uint8_t   rtd;              //!< Real-time display task stack usage
            uint8_t   trm;              //!< Terminal communications task stack usage
            uint8_t   bgp;              //!< Background processing task stack usage
        } task;
        uint8_t   tasks[12];
    }             stack_usage;          //!< FGC.DEBUG.STACK_USAGE

    struct Shared_memory_watch_point
    {
        uint32_t  control;              //!< Watchpoint control: 0=OFF 1=EQUALS 2=NOT EQUALS
        uint32_t  address;              //!< Watchpoint address in page 0
        uint32_t  value;                //!< Watchpoint value to check at address
        uint32_t  mask;                 //!< Watchpoint value mask
        uint32_t  hit_count;            //!< Hit counter
        uint32_t  task_id;              //!< Task ID
        uint32_t  isr_mask;             //!< ISR Mask
        uint32_t  timestamp_s;          //!< UTC time in seconds when the hit occurred
        uint32_t  timestamp_us;         //!< UTC time in microseconds when the hit occurred
    }             watch_point;          //!< FGC.DEBGUG.WATCH.MCU
};



// ---------- External variable declarations

//! Memory area shared by boot and main, mapped to 0x00000560 by the linker

extern volatile struct Shared_memory shared_mem;


// EOF
