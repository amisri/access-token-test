//! @file   cmd.h
//! @brief Command processing functions

#pragma once


// ---------- Includes

#include <stdio.h>

#include <os.h>

#include <defconst.h>
#include <definfo.h>
#include <dpcom.h>
#include <property.h>
#include <parsService.h>



// ---------- Constants

//! Magic word for PPM all users
#define PPM_ALL_USERS                   0xFF

//! Filter option for CmdParentGet: not hidden
#define PARENT_GET_NOT_HIDDEN           0

//! Filter option for CmdParentGet: config changed
#define PARENT_GET_CONFIG_CHANGED       1

//! Filter option for CmdParentGet: config set
#define PARENT_GET_CONFIG_SET           2

//! Filter option for CmdParentGet: config unset
#define PARENT_GET_CONFIG_UNSET         3

//! Maximunm number of DATA values
#define CMD_MAX_DATA_VALUES             6
#define CMD_MAX_DAVA_VALUES_SIZE        PARS_MAX_CMD_TOKEN_LEN + 1



// ---------- External structures, unions and enumerations

//! Callback function to store character in command

typedef void (*fputc_store_cb)(char ch);

//! Fieldbus and serial command tasks and publication task variables

struct cmd
{
    OS_SEM              * sem;                //!< For fcm and tcm: Packet ready   For pcm: Lock
    FILE                * f;                  //!< Pointer to output stream FILE
    char                * serial_stream_buffer_ptr;
    uint16_t              serial_stream_buffer_offset;
    uint16_t              line_offset;
    fputc_store_cb        store_cb;           //!< Callback to store character
    bool                  abort_f;            //!< Command abort flag
    bool                  get_parent_f;       //!< Get parent property flag
    bool                  timestamp_f;        //!< Timestamp produced flag
    uint16_t              stat;               //!< Command status (reported to ETH for fcm)
    uint16_t              errnum;             //!< Command errnum (reported by Set/Get func())

    uint16_t              header;
    uint16_t              pkt_buf_idx;        //!< Current cmd packet index
    uint16_t              prop_blk_idx;       //!< Current property block index
    prop_size_t           n_elements;         //!< Current property length in elements
    struct prop         * last_prop;          //!< Last property accessed
    struct prop         * prop;               //!< Current property
    struct prop         * cached_prop;        //!< Cached property
    struct Pars_buf     * pars_buf;           //!< Buffered command packet structure
    struct Dpcom_prop_buf * prop_buf;           //!< Property communications structure (in DPRAM)

    uint16_t              cmd_type;           //!< FGC_FIELDBUS_XXX_CMD (SET, GET, SUB, etc...)
    bool                  new_cmd_f;          //!< New command starting - first packet expected
    bool                  end_cmd_f;          //!< End of last command packet reached

    uint16_t              getopts;            //!< Get option flags
    uint16_t              n_prop_name_lvls;   //!< Number of levels in the property name
    uint16_t              si_idx;             //!< Index of next free element in .sym_idxs[]
    uint16_t              sym_idxs[FGC_MAX_PROP_DEPTH];   //!< Parent property names
    uint16_t              line;               //!< elements in line counter (get)
    uint16_t              linelen;            //!< elements per line (get)

    uint16_t              n_token_chars;      //!< Length of token in buffer
    uint16_t              token_idx;          //!< Position in token buffer
    char                  token[PARS_MAX_CMD_TOKEN_LEN + 1]; //!< Token buffer
    char                  token_delim;        //!< Token delimiter character

    char                  data[CMD_MAX_DATA_VALUES][CMD_MAX_DAVA_VALUES_SIZE]; //!< Data values
    uint8_t               data_len;           //!< Number of DATA values retrieved from the command buffer

    uint16_t              pars_idx;           //!< Position in pars_buf results array
    uint16_t              device_par_err_idx; //!< Element array when parsing error occurred
    uint16_t              n_pars;             //!< Total number of parameters specified in the command
    bool                  last_par_f;         //!< Last parameter flag
    uint16_t              n_arr_spec;         //!< Number of array range specifiers (0-2)
    prop_size_t           from;               //!< array range specifier
    prop_size_t           to;                 //!< array range specifier
    uint16_t              step;               //!< array step specifier
    uint16_t              changed_f;          //!< Property changed flag
    uint16_t              transaction_id;     //!< Transaction ID
    uint8_t               cyc_sel;            //!< Cycle selector
    uint8_t               sub_sel;            //!< Sub_device selector

    bool write_nvs_f;                         //!< Flag to indicate if the property data must also be written in NVS (used in PropBlkSet)
};

//! Last property accessed

struct cmd_last_prop
{
    char             name[PARS_MAX_CMD_TOKEN_LEN];  //!< Name of the last property accessed
};



// ---------- External variable declarations

//! Fieldbus task command structure
extern struct cmd fcm;

//! Serial communications task command structure
extern struct cmd tcm;

//! Publication task command structure
extern struct cmd pcm;

//! Last command property
extern struct cmd_last_prop cmd_last_p;



// ---------- External function declarations

//! This function is used for the fieldbus AND serial Command tasks: FcmTsk and TcmTsk. FcmTsk is triggered
//! by a semaphore from the Fbs task and TcmTsk is triggered by a semaphore from the terminal task. The tasks
//! are triggered when a command packet is ready for processing.
//!
//! Notes on "Force last packet": The FGC sends command responses in packets to the gateway.  In the header
//! flags for each packet there are two bits indicating if it is a first and/or last packet.  It is essential
//! that the final packet of the response has the last packet bit set.  To do this, this function adds a
//! trailing ';' character to ensure that a packet will be produced as the output from the get function
//! might exactly fill a packet that might already be sent without the last packet bit set.  By adding one
//! character, it is guaranteed that a final packet with the last packet bit set will be produced to end the
//! command response.
//!
//! @param init_par pointer to command structure

void CmdTask(void * init_par);


//! This function returns the next character from the command packet, converted to upper case.
//! The function is used with fieldbus and terminal cmd structures to extract fieldbus and serial commands.
//! It also works with he pcm structure which is used to look up property names.
//!
//! If the packet is empty the function sleeps on the command's semaphore, waiting for the controlling task
//! (FbsTsk or TrmTsk) to post a semaphore when a new packet is available.  If the command being executed is
//! a set command, then the task will post the set lock semaphore before waiting so as not to block a set
//! command by the other source (Fieldbus or Terminal).  If the character returned is the last in the last packet of
//! the command, the function will set c->end_cmd_f. If a new first packet is received when not expected,
//! the function returns FGC_CMD_RESTARTED to end the command that's being received.
//!
//! @param c pointer to command structure
//! @param ch_p end of command character
//!
//! @retval fgc_errno

uint16_t CmdNextCh(struct cmd * c, char * ch_p);


//! This function sleeps on the command's semaphore, waiting for the controlling task (FbsTsk or TrmTsk) to
//! post a semaphore when a new packet is available.  If the command being executed is a set command, then the
//! task will post the set lock semaphore so as not to block a set command by the other source (Fieldbus or Terminal).
//! The function checks if a first-packet is expected and if it is received using the following logic:
//!
//!                         first packet of a command expected
//!                              No                      Yes
//!                   +-----------------------+-----------------------+
//!                   |                       |   Ignore new packet.  |
//!               No  |     Use new packet    |    Wait in function   |
//! FIRST_CMD_PKT     |                       |     for next packet   |
//! bit set in        +-----------------------+-----------------------+
//! packet header     |         Return        |                       |
//!              Yes  |   FGC_CMD_RESTARTED   |     Use new packet    |
//!                   |                       |                       |
//!                   +-----------------------+-----------------------+
//!
//! @param c pointer to command structure
//! @retval fgc_errno ToDo: the return value is enum fgc_errno

uint16_t CmdNextCmdPkt(struct cmd * c);


//! This function prepares the get structure based on the to/from/linelen specifiers.  It display
//! label and/or range and/or type as required.  It returns the number of elements in the selected
//! from-to range (may be zero).
//!
//! @param c pointer to command structure
//! @param p pointer to property structure
//! @param filter filter parents according to their property flag
//! @retval fgc_errno

uint16_t CmdParentGet(struct cmd * c, const struct prop * p, uint16_t filter);


//! This function prepares retrieves the number of configuration properties that have been changed.
//!
//! @retval Number of configuration properties changed.

uint16_t CmdGetChangedProps(void);


//! This function prepares the get structure based on the to/from/linelen specifiers.  It display
//! label and/or range as required.  It set the returned argument pointed by n_els_ptr to the number of
//! elements in the selected from-to range (may be zero). It returns a status (FGC_OK_NO_RSP if OK)
//!
//! @param c pointer to command structure
//! @param p pointer to property structure
//! @param linelen line length
//! @param n_els_ptr pointer to number of elements
//! @retval fgc_errno

uint16_t CmdPrepareGet(struct cmd * const c, struct prop * const p, uint16_t linelen, prop_size_t * n_els_ptr);


//! This will write the binary data header when starting the export of binary log data.  The header is five
//! bytes long and starts with 0xFF, followed by the length of the data in bytes.
//!
//! @param c pointer to command structure
//! @param p pointer to property structure
//! @param n_bytes payload length in bytes

void CmdStartBin(struct cmd * c, const struct prop * p, uint32_t n_bytes);


//! This function will write the timestamp to the stream for c provided it is not the serial command stream
//! and the timestamp has not already been produced.  If timestamp is NULL it will take the time now as the
//! timestamp.
//!
//! @param c pointer to command structure
//! @param p pointer to property structure

void CmdPrintTimestamp(struct cmd * c, const struct prop * p);


//! This will print the property symbol name based on the sym_idxs[] array in the get structure.  This must
//! handle two cases - when getting a single property and when getting children of a parent.
//! For a single property, the complete name is reported, while for getting a parent, only the name extension
//! is reported.  For this c->n_prop_name_lvls is the level of parent, while c->si_idx will be the level
//! of the child.
//!
//! @param c pointer to command structure

void CmdPrintLabel(struct cmd * c);


//! This will check the get flags to decide if/how to display the passed index.  It also adds a space or
//! command delimiter character, according to the GET OPTION.
//!
//! The function also checks the abort flag and returns a error if it is set.
//!
//! Get Option:  !IDX     Display nothing
//!              HEX     Display index in hex rather than decimal
//!
//! @param c pointer to command structure
//! @param idx index
//! @retval fgc_errno

uint16_t CmdPrintIdx(struct cmd * c, prop_size_t idx);


//! This will return a pointer to the symbol string for the value in the symbol list (if valid) or the
//! string "<invalid>" if not.
//!
//! @param sym_lst symbol list
//! @param value
//! @retval string for the value in the symbol list

char * CmdPrintSymLst(const struct sym_lst * sym_lst, uint32_t value);


//! This will write to the f the symbols for which the bits are set in value.
//!
//! @param f pointer to file to write
//! @param sym_lst symbol list
//! @param value

void CmdPrintBitMask(FILE * f, const struct sym_lst * sym_lst, uint32_t value);


//! This function will wait until the specified time has arrived.
//!
//! @param unix_time UTC Unix time.
//! @param ms_time millisecond time

void CmdWaitUntil(uint32_t unix_time, uint16_t ms_time);


// EOF
