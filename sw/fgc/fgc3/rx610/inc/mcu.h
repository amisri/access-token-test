//! @file  mcu.h
//! @brief RX610 hardware related functionality

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>
#include <os.h>
#include <os_hardware.h>
#include <iodefines.h>



// ---------- Constants

// Task Identifiers (cannot be enum because of asm in vecs.c)

enum Mcu_task_id
{
    MCU_TSK_MST,
    MCU_TSK_EVT,
    MCU_TSK_FBS,
    MCU_TSK_STA,
    MCU_TSK_PUB,
    MCU_TSK_FCM,
    MCU_TSK_TCM,
    MCU_TSK_SCI,
    MCU_TSK_DLS,
    MCU_TSK_RTD,
    MCU_TSK_TRM,
    MCU_TSK_BGP,
    MCU_NUM_TASKS
};

// If more tasks are added update struct Shared_memory_stack in sharedMemory.h

#define MCU_NUM_TEST_STRINGS    4



// ---------- External structures, unions and enumerations

struct Mcu
{
    uint16_t   ctrl_counter;                         //!< Device Reset/Boot/PwrCyc/Crash down counter (5ms)
    uint16_t   ctrl_sym_idx;                         //!< Device Reset/Boot/PwrCyc/Crash prop sym_idx
    char       test_data[44 * MCU_NUM_TEST_STRINGS]; //!< Buffer for TEST.XXXX properties
    OS_SEM   * set_lock;                             //!< Set command lock semaphore
};

struct Mcu_power_supplies
{
    float    v5;    //!<   5V value in mV
    float    vp15;  //!< +15V value in mV
    float    vm15;  //!< -15V value in mV

    uint32_t fails_counter;
};



// ---------- External variable declarations

extern struct Mcu mcu;

extern struct Mcu_power_supplies psu;



// ---------- External function declarations

//!  This function will disable interrupt generation

void mcuDisableNetworkInterrupt(void);


//!

void mcuDisable1msTickInterrupt(void);


//!  This function will enable the 1ms tick interrupt generation from TIMER A1
//!
//!   Maskable interrupts are enabled and disabled by using
//!   a) the interrupt enable flag (I flag),
//!   b) interrupt priority level select bit
//!   c) processor interrupt priority level (IPL).

void mcuEnable1msTickInterruptFromMcuTimer(void);


//!  This function will enable the 1ms tick interrupt generation from the PLD.
//!
//!   Maskable interrupts are enabled and disabled by using
//!   a) the interrupt enable flag (I flag),
//!   b) interrupt priority level select bit
//!   c) processor interrupt priority level (IPL).

void mcuEnable1msTickInterruptFromPLD(void);


//!

void mcuEnableNetworkInterrupt(void);


//! This function performs the following task:
//!
//!  - Write the MP command into the SM register
//!  - Mask Interrupts
//!  - Activate a global reset
//!  - Wait to be reset

void mcuReset(void);


//! This function initialises the operation of the RX610
//! General Purpose Timer (GPT).

void mcuEnableTimersAndInterrupts(void);


//! This function initialises the operation of the MCU.

void mcuInit(void);


//! This function initialises the operation of the RTOS (NanOs).

void mcuInitOS(void);


//!

void mcuStartClocks(void);


//! Stop Dsp, Adc, Dac derived clock generation

void mcuStopDerivedClocks(void);


//! Sets the external sync enable bit

void mcuSetExtSyncEnable(void);


//! This function checks the consumption of the stack for all tasks and the DSP.

void mcuCheckStack(void);


//! This function will read the ADCs responsible to monitor the external power supplies +5, +15 and -15V.

void mcuReadPsuVoltage(void);


//! This function checks if the voltage values of the external power supplies are outside the expected limits.
//!
//! @retval  true    Voltage outside limits
//! @retval  false   Voltage within limits

bool mcuVoltageOutsideTolerance(void);


//! Sets the Device Reset/Boot/PwrCyc/Crash down counter and prop sym_idx.

void mcuTerminate(uint16_t sym_idx);


//! This function processes DEVICE.PWRCYC/RESET/BOOT/CRASH commands.

void mcuDeviceControl(void);


// EOF
