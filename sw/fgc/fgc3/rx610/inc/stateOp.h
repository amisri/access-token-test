//! @file   stateOp.h
//! @brief  Manage the Operational state of the FGC

#pragma once



// ---------- Includes --------------------------------------------------

#include <stdint.h>



// ---------- External structure, unions and enumerations ---------------

struct stateOp_props
{
    uint16_t   mode_op;         //!< Property MODE.OP
};



// ---------- Exernal variable declarations -----------------------------

extern struct stateOp_props stateOpProps;



// ---------- External function declarations ----------------------------

//! Initialize STATE.OP to be UNCONFIGURED

void stateOpInit(void);


//! Update the Operational State Machine.
//!
//! Transition from current operational state to requested operational state.

void stateOpProcess(void);


//! Returns the current operational state.
//!
//! @returns  Current operational state.

uint32_t stateOpGetState(void);


//! Set MODE.OP to the desired state.
//!
//!  @param[in]  mode_op     desired operational state

void stateOpSetModeOp(uint16_t mode_op);


//! Returns the current value for MODE.OP.
//!
//!  @returns    MODE.OP

uint16_t stateOpGetModeOp(void);


//! Returns a two character array with the reduced state name.
//!
//! @param[in]   state     The state to convert into string
//!
//! @returns               The reduced state name

char const * stateOpGetStateName(uint8_t state);


// EOF
