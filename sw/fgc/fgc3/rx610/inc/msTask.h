//! @file  msTask.h
//! @brief Millisecond actions

#pragma once


// ---------- Includes

#include <stdint.h>

#include <time_fgc.h>



// ---------- External structures, unions and enumerations

struct MsTask
{
    uint32_t            isr_start_us;                   //!< Starting time of the ISR in us
    uint16_t            mode;                           //!< Readback of MODE register
    uint16_t            set_cmd_counter;                //!< Set command downcounter (to inhibit auto cal)
    uint16_t            ms;                             //!< mstime
    uint16_t            ms_mod_5;                       //!< mstime % 5
    uint16_t            ms_mod_10;                      //!< mstime % 10
    uint16_t            ms_mod_20;                      //!< mstime % 20
    uint16_t            s_mod_10;                       //!< unix_time % 10
    uint16_t            state_task_watchdog;
    uint32_t            dsp_rtp_alive_cnt;              //!< Current DSP alive 1 ms IRQ counter.
    float               dsp_freq_khz;                   //!< Property FGC.DSP_FREQ_KHZ: DSP frequency evaluated every second

    struct                                              //!< CPU usage in % (calculated at 1Hz)
    {
        int32_t         mcu;                            //!< MCU CPU usage in % (calculated at 1Hz)
        int32_t         dsp;                            //!< DSP CPU usage in % (calculated at 1Hz)
    } cpu_usage;

    uint16_t            duration;                       //!<     msTask execution duration (FGC2: in 0.5us ticks; FGC3: in us)
    uint16_t            max_duration[20];               //!< Max msTask execution duration (FGC2: in 0.5us ticks; FGC3: in us)
    struct CC_us_time   time_us;                        //!< Timestamp of current microseconds
    struct CC_ms_time   time_ms;                        //!< Timestamp of current millisecond
    struct CC_us_time   prev_time;                      //!< Timestamp of previous millisecond
    bool                dspFltWarning;                  //!< True when either the real-time or the background task fail
    bool                dspFltFault;                    //!< True when the DSP paniced
    uint16_t            log_pm_trig;                    //!< Readback of post mortem trigger bits in ACK byte
};



// ---------- External variable definitions

extern struct MsTask ms_task;



// ---------- Platform/class specific function declarations

void msTaskClassProcess(void);



// ---------- External function declarations

//! This function is the Millisecond Task. It runs every millisecond and coordenates
//! time sensitive actions.

void msTask(void *);


//! Flag that stateTask has completed its iteration.

void msTaskResetStateTaskWatchdog(void);


// EOF
