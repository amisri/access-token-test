//! @file   calibration.h
//! @brief  Calibration processing of the ADCs, DCCTs and DAC

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>



// ---------- Constants

#define CAL_VREF_LIMIT          0.2         //!< Max error in reference measurement to allow calibration.
#define CAL_DACS                0x03        //!< Mask for DAC1 & DAC2 channels



// ---------- External structures, unions and enumerations

enum Calibration_target
{
    CAL_TARGET_INT_ADCS,
    CAL_TARGET_EXT_ADCS,
    CAL_TARGET_DCCTS,
    CAL_TARGET_DACS,
    CAL_TARGET_NONE
};

struct Calibration_properties
{
    uint32_t      inhibit_cal;          //!< Seconds down counter for inhibiting auto-calibration
    uint32_t      last_cal_time_unix;   //!< Last calibration time for internal ADC (Unix time)
};



// ---------- External variable declarations

extern struct Calibration_properties cal;



// ---------- External function declarations

//! This function is used to initiate a new calibration.
//! If the input cal_sequence is not NULL, and STATE_OP is not FGC_OP_CALIBRATING, then this function will:
//!  1. Set the calibration sequence pointer to the one given in input (cal_sequence)
//!  2. Set the channel mask = chan_mask
//!  3. Set the signal id (CAL_IDX_OFFSET/CALPOS/CALNEG) = signal. Note that this parameter is not relevant
//!  4. for all types of calibration. In that case, leave this parameter set to zero.
//!  5. Switch to state FGC_OP_CALIBRATING

void calibrationSetup(enum Calibration_target, uint32_t chan_mask, int32_t signal);


//! This function is called from the Sta Task when the OP state is CALIBRATING and the millisecond time
//! is 102, 302, 502, 702, or 902. It is responsible for directing the DSP in running the auto calibration
//! sequence for the ADCs, DAC and DCCTs.

void calibrationRun(void);


//! This function indicates the state of the calibration sequence.
//!
//! @retval  true    The calibration sequence is under way.
//! @retval  false   The calibration sequence is not running.

bool calibrationIsActive(void);


//! Suppress auto-calibration for the specified number of seconds.
//!
//!  @param  time    Duration of suppression in seconds

void calibrationInhibit(uint32_t time);


//! This function is called to run the auto calibration of ADCs, DCCTs and
//! DAC. The function calibrationRunSequence() is called when sta.cal_f is set,
//! which is only every 200ms.
//!
//! The function also checks if an automatic calibration of the internal
//! ADCs is required.

void calibrationProcess(void);


//EOF
