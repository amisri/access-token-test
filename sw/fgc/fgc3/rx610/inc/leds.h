//! @file     leds.h
//! @brief    FGC3 MCU LED Interface API

#pragma once



// ---------- Includes --------------------------------------------------

#include <stdint.h>
#include <memmap_mcu.h>



//---------- External structures, unions and enumerations --------------

enum Leds_state
{
    LEDS_RED_NET    = RGLEDS_NET_RED_MASK16,
    LEDS_RED_FGC    = RGLEDS_FGC_RED_MASK16,
    LEDS_RED_PSU    = RGLEDS_PSU_RED_MASK16,
    LEDS_RED_VS     = RGLEDS_VS_RED_MASK16,
    LEDS_RED_DCCT   = RGLEDS_DCCT_RED_MASK16,
    LEDS_RED_PIC    = RGLEDS_PIC_RED_MASK16,
    LEDS_GREEN_NET  = RGLEDS_NET_GREEN_MASK16,
    LEDS_GREEN_FGC  = RGLEDS_FGC_GREEN_MASK16,
    LEDS_GREEN_PSU  = RGLEDS_PSU_GREEN_MASK16,
    LEDS_GREEN_VS   = RGLEDS_VS_GREEN_MASK16,
    LEDS_GREEN_DCCT = RGLEDS_DCCT_GREEN_MASK16,
    LEDS_GREEN_PIC  = RGLEDS_PIC_GREEN_MASK16
};



//---------- External function declarations ----------------------------

//! This function toggles the given RED or GREEN LED.
//!
//! @param state LEDS_{NET,FGC,PSU,VS,DCCT,PIC}_{RED,GREEN}

void ledsToggle(enum Leds_state state);


//! This function turns all green/red LEDs off.

void ledsResetAll(void);


//! This function must be called regularly in order to generate a visible orange color for warnings,
//! and blink red lights for error.
//! It also updates the red and green LEDs.

void ledsProcess(uint16_t ms);


//EOF