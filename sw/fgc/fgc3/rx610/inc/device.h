//! @file   device.h
//! @brief  Functions related to device initialisation

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>

#include <defconst.h>

#include <fgc/fgc_db.h>
#include <fgc_consts_gen.h>
#include <property.h>



// ---------- Constants

#define PLATFORM_NAME_LEN    8
#define CLASS_NAME_LEN       16



// ---------- External structures, unions and enumerations

struct Device_sys_info
{
    fgc_db_t sys_idx;                           //!< System index in SYSDB
    char     sys_type[FGC_SYSDB_SYS_LEN + 1];
};

struct Device
{
    char                     name[FGC_MAX_DEV_LEN + 1];         //!< System (power converter) name
    char                     spare_name[FGC_MAX_DEV_LEN + 1];   //!< System (power converter) spare's name
    char                     platform_name[PLATFORM_NAME_LEN];  //!< Platform name
    char                     class_name[CLASS_NAME_LEN];        //!< Class name
    char                     type[6];                           //!< Device type (system type property DEVICE.TYPE)
    uint16_t                 omode_mask;                        //!< omode_mask (from NameDB)
    uint8_t                  platform_id;                       //!< Platform ID
    uint8_t                  class_id;                          //!< Class ID
    uint8_t                  spare_id;                          //!< FGC Ether bus ID of the spare system for this device
    uint16_t                 max_user;                          //!< Set according to DEVICE.PPM and class
    uint16_t                 max_sub_devs;                      //!< Set according to DEVICE.MULTI_PPM and class
    uint32_t                 cmds_received;                     //!< Commands received counter (DEVICE.CMDRCVD)
    prop_size_t              name_nels;                         //!< Device name length
    struct Device_sys_info   sys;
};



// ---------- External variable declarations

extern struct Device device;



// ---------- External function definitions

//! Initialises device info and properties.

void deviceInit(void);


//! This function returns true if the name of the device is valid.
//!
//! @retval     true    Device name is valid
//! @retval     false   Device name is invalid

bool deviceNameIsValid(void);


//! Returns the name of the device.
//!
//! @return     The name of the device

char * deviceGetName(void);


//! Returns the omode mask of the device.
//! This corresponds to the LHC sectors affected by the device.
//!
//! @return     The device's omode mask

uint16_t deviceGetOmodeMask(void);


//! Updates the number of commands received by the device.

void deviceUpdateCmdCounter(void);


//EOF