//! @file  dallasTask.h
//! @brief Dallas bus functions
//!
//! This file contains the function for the Dallas Task and all the functions related to
//! reading the IDs and temperatures from the Dallas devices controlled by the C62 microcontroller.

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>

#include <defconst.h>
#include <fgc_consts_gen.h>
#include <microlan_1wire.h>
#include <property.h>



// ---------- External structures, unions and enumerations

enum DallasTask_error_codes
{
    DALLAS_TASK_ERROR_OK,
    DALLAS_TASK_ERROR_OK_WAIT,
    DALLAS_TASK_ERROR_POWER_OFF,
    DALLAS_TASK_ERROR_WAIT_BC,
    DALLAS_TASK_ERROR_NO_TEMP_SENSOR,
    DALLAS_TASK_ERROR_NO_MORE_DATA,
    DALLAS_TASK_ERROR_ISR_CMD_ERR,
    DALLAS_TASK_ERROR_ISR_CMD_TIMEOUT,
    DALLAS_TASK_ERROR_BG_CMD_TIMEOUT,
    DALLAS_TASK_ERROR_NO_RSP = 99,
};

enum DallasTask_commands
{
    DALLAS_TASK_CMD_SWITCH_PROG,
    DALLAS_TASK_CMD_READ_IDS,
    DALLAS_TASK_CMD_SET_EXTERNAL_ID,
    DALLAS_TASK_CMD_GET_BARCODE_FOR_EXT_ID,
    DALLAS_TASK_CMD_GET_RESPONSE,
    DALLAS_TASK_CMD_READ_TEMPS_BRANCH,
};

struct DallasTask_resonse
{
    uint8_t global[FGC_C62_ERR_RSP_LEN];    //!< global response (for all branches)
    uint8_t local [FGC_C62_ERR_RSP_LEN];    //!< local branch response (logical_path=4)
    uint8_t meas_a[FGC_C62_ERR_RSP_LEN];    //!< measurement A branch response (logical_path=1)
    uint8_t meas_b[FGC_C62_ERR_RSP_LEN];    //!< measurement B branch response (logical_path=3)
};

struct DallasTask_vars
{
    uint16_t    inhibit_f;                          //!< Dallas temperature read inhibit (DLS.INHIBIT)
    uint32_t    num_errors;                         //!< Error counter
    uint32_t    error_time;                         //!< Abstime of last error
    struct DallasTask_resonse rsp;
    uint8_t     detected[FGC_N_COMP_GROUPS];        //!< Number of detected Dallas devices in each group
    uint8_t     n_unknown_grp[FGC_COMPDB_N_COMPS];  //!< Number of unknown group members for each component type
};

// Temperature measurement variables
// These temperatures are 16 bits raw values form the sensor

struct DallasTask_temperatures
{
    uint32_t            unix_time;

    // temp.fgc.in and temp.fgc.out are linked with the corresponding temperature sensor
    // via the assignment in components.xml with
    //  temp_prop = "TEMP.FGC.IN"
    // and
    //  temp_prop = "TEMP.FGC.OUT"
    // (this information is extracted from CompDB by the MCU program)

    struct
    {
        uint16_t        in;                             //!< TEMP.FGC.IN    FGC inlet temperature
        uint16_t        out;                            //!< TEMP.FGC.OUT   FGC outlet temperature
        uint16_t        delta;                          //!< TEMP.FGC.DELTA Difference between FGC outlet and inlet temps
    } fgc;

    bool                dcct_temp_available[2];         //!< DCCT temperature measurements expected flags

    struct
    {
        uint16_t        mod;                            //!< External ADC modulator temperature
        uint16_t        psu;                            //!< External ADC PSU temperature
        uint16_t        mainboard;                      //!< External ADC mainboard temperature
    } ext_adc[2];

    struct
    {
        uint16_t        head;                           //!< DCCT head temperature
        uint16_t        elec;                           //!< DCCT electronics temperature
    } dcct[2];
};

// Structure for barcode properties

struct DallasTask_barcode_vars
{

    struct
    {
        int8_t     *     L         [FGC_MAX_L_BARCODES]; //!< Local barcodes
        int8_t     *     V         [FGC_MAX_V_BARCODES]; //!< BARCODE.Voltage source barcodes
        int8_t     *     A         [FGC_MAX_A_BARCODES]; //!< Measurement channel A barcodes
        int8_t     *     B         [FGC_MAX_B_BARCODES]; //!< Measurement channel B barcodes
        int8_t     *     C         [FGC_MAX_C_BARCODES]; //!< Crate barcodes
    } bus;

    struct
    {
        uint8_t           cassette  [FGC_ID_BARCODE_LEN]; //!< FGC cassette barcode
        uint8_t           ana       [FGC_ID_BARCODE_LEN]; //!< FGC Analogue Interface board barcode
    } fgc;

    struct
    {
        uint8_t           head      [FGC_ID_BARCODE_LEN]; //!< DCCT head barcode
        uint8_t           elec      [FGC_ID_BARCODE_LEN]; //!< DCCT electronics barcode
    } dcct[2];

    struct
    {
        uint8_t           cassette  [FGC_ID_BARCODE_LEN]; //!< External ADC cassette barcode
        uint8_t           elec      [FGC_ID_BARCODE_LEN]; //!< External ADC Delta-Sigma modulator barcode
        uint8_t           psu       [FGC_ID_BARCODE_LEN]; //!< External ADC PSU barcode
        uint8_t           mainboard [FGC_ID_BARCODE_LEN]; //!< External ADC mainboard barcode
    } ext_adc[2];
};

struct DallasTask_barcode_n_els
{
    struct
    {
        prop_size_t     L;                              //!< Local barcodes
        prop_size_t     V;                              //!< Voltage source barcodes
        prop_size_t     A;                              //!< Measurement channel A barcodes
        prop_size_t     B;                              //!< Measurement channel B barcodes
        prop_size_t     C;                              //!< Crate barcodes
    } bus;
};



// ---------- External variable declarations

extern struct DallasTask_vars          dls;            //!< Dallas task variables structure
extern struct DallasTask_temperatures  temp;           //!< Dallas Temperature variables structure
extern struct DallasTask_barcode_vars  barcode;        //!< Barcode variables structure
extern struct DallasTask_barcode_n_els barcode_n_els;  //!< Barcode property n_els structure



// ---------- External function declarations

//! Dallas system is active if at least one ID device found.
//!  This function returns true if the system is active and false if it is not.
//!
//!  @retval true    Dallas system is active
//!  @retval false   Dallas system is not active (no IDs)

bool dallasTaskSystemActive(void);


//! Returns the branch information for the selected branch.
//!  It is the responsability of the user to provide a valid branch.
//!
//!  @return branch information for the selected branch.

struct TBranchSummary dallasTaskGetBranchInfo(uint16_t branch);


//! Returns the device information of a specified device.
//!  It is the responsability of the user to provide a valid branch and index.
//!
//!  @param[in]  logical_path    Branch ID
//!  @param[in]  device_idx      Number of the device in the chosen branch's list of devices
//!
//!  @return device information of a specified device.

struct TMicroLAN_element_summary * dallasTaskGetDeviceInfo(uint16_t logical_path, uint16_t device_idx);


//! Returns the index for CompDB of a specified device.
//!  It is the responsability of the user to provide a valid branch and index.
//!
//!  @param[in]  logical_path    Branch ID
//!  @param[in]  device_idx      Number of the device in the chosen branch's list of devices
//!
//!  @return index in the all_devs array of a specified device.

uint16_t dallasTaskGetCompDbIndex(uint16_t logical_path, uint16_t device_idx);


//! This function will translate the supplied device 1-wire ID into an ASCII string so that it can be
//! printed with the most significant byte (CRC) first.
//!
//! @param devid ID
//! @param id_string Formatted string
//! @return

char * dallasTaskIdString(const union TMicroLAN_unique_serial_code * devid, char * id_string);


//! This function is called when teh config manager sends the command S FGC.ID RESET
//! signaling all the barcodes have been set.

void dallasTaskBarcodesSet(void);


//! Called by the RegFGC3 module once all the boards are in Production boot and
//! the mezzanine ID value ready to be read out

void dallasTaskRegFgc3Ready(void);


//! This function is the Dallas task. It is woken by the msTask() at the start of every 100ms period in
//! order to read the temperatures from the M16C62.  A temperature reading is make once per 10s, but the process
//! takes several seconds and the task polls at 10Hz to know when the new measurements are ready to be
//! read out from the M16C62.  It will log the results at 0.1Hz in the THOUR log and once per 10 minutes in the
//! TDAY log.
//!
//! When the task first runs (or after a S ID RESET command), the task will also read out the Dallas IDs of
//! all the connected devices and will analyse them to see if all the components that are expected for the
//! type of power converter identified by the device name are in fact present.

void dallasTask(void *);


// EOF