//! @file:  parsService.h
//! @brief

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <inc/fgc_ether.h>



// ---------- Constants

#define PARS_MAX_CMD_PKT_LEN    FGC_ETHER_RSP_PKT_LEN        //!< Command packet buffer length in bytes (for MCU)
#define PARS_MAX_CMD_PKT_LEN_LW (FGC_ETHER_RSP_PKT_LEN / 4)  //!< Command packet buffer length in long words (for DSP)
#define PARS_MAX_CMD_PKT_PARS   (FGC_ETHER_RSP_PKT_LEN / 2)  //!< Max parameters that can be in one command packet
#define PARS_MAX_CMD_TOKEN_LEN  32                           //!< Max command token length in characters
#define PARS_MAX_CMD_PKTS       21



// ---------- Internal structures, unions and enumerations

//! Integer limits structure

struct int_limits
{
    int32_t              min;                                   //!< Min integer limit
    int32_t              max;                                   //!< Max integer limit
};


//! Unsigned integer limits structure

struct intu_limits
{
    uint32_t             min;                                   //!< Min unsigned integer limit
    uint32_t             max;                                   //!< Max unsigned integer limit
};


//! Floating point limits structure

struct float_limits
{
    float                min;                                   //!< Min float limit
    float                max;                                   //!< Max float limit
};


//! Structure containing the parameter buffer

struct Pars_pkt
{
    uint32_t                    header_flags;                   //!< Fieldbus packet header flags
    uint32_t                    header_user;                    //!< Fieldbus packet header user
    uint32_t                    n_chars;
    char                        buf[PARS_MAX_CMD_PKT_LEN];
};


//! MCU Parameter parsing

struct Pars_buf
{
    uint32_t                    type;                           //!< Parameter type: PKT_FLOAT|PKT_INT|PKT_RELTIME|PKT_INTU
    uint32_t                    pkt_buf_idx;                    //!< Packet buffer index
    uint32_t                    out_idx;                        //!< Start of first parameter
    uint32_t                    errnum;                         //!< Error number after parsing
    uint32_t                    par_err_idx;                    //!< Parameter number that caused the error
    uint32_t                    n_pars;                         //!< Number of parameters in results

    union Pars_limits
    {
        struct int_limits       integer;                        //!< Integer limits
        struct intu_limits      unsigned_integer;               //!< Unsigned integer limits
        struct float_limits     fp;                             //!< Float limits
    } limits;

    union Pars_results
    {
        uint32_t                intu[PARS_MAX_CMD_PKT_PARS];     //!< Unsigned integer results
        int32_t                 ints[PARS_MAX_CMD_PKT_PARS];     //!< Signed integer results
        float                   fp[PARS_MAX_CMD_PKT_PARS];       //!< Floating point results
    } results;

    bool                        empty_tokens[PARS_MAX_CMD_PKT_PARS];  //!< Whether a token in the set property array is empty or not
    struct Pars_pkt             pkt[PARS_MAX_CMD_PKTS];               //!< Buffers for command packets
};



// ---------- External variable declarations

extern struct Pars_buf    fcm_pars;      //!< FcmTsk parameters
extern struct Pars_buf    tcm_pars;      //!< TcmTsk parameters



// ---------- External funtion declarations

//! Parse a command packet containing Integers or Floats to fcm buffer.

void parsPktFcm(struct Pars_buf * pars);


//! Parse a command packet containing Integers or Floats to tcm buffer.

void parsPktTcm(struct Pars_buf * pars);


// EOF
