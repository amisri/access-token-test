//! @file  pub.h
//! @brief This module manages the publication of subscriptions

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <os.h>

#include <function.h>
#include <defconst.h>
#include <cmd.h>



// ---------- Constants

//! Constant for non-multi-PPM property publications

#define PUB_NO_SUB_SEL                      (0)

//! Constant for non-PPM property publications

#define PUB_NO_CYC_SEL            (NON_PPM_USER)



// ---------- External structures, unions and enumerations

//! Publication semaphore

struct pub_sema
{
    OS_SEM   * lock;                      //!< Semaphore lock for subscription structure
    OS_SEM   * last_pkt_send;             //!< Semaphore used to signal that last packet is sent
};



// ---------- External variable declarations

extern struct pub_sema  pub_sema;



// ---------- External function declarations

//! Publish task function.
//!
//! This function is used to publish subscribed properties. Publication is
//! driven by the subscription table in sub.table[]. The msTask() will
//! resume this task every 20 milliseconds.  The task either blocks on the
//! output stream semaphore when the stream buffer is full, or it suspends
//! itself if it finds nothing to publish.
//!
//! @param unused

void pubTask(void * unused);


//! This function blocks the publication stream (using pcm.abort_f) and request
//! PubTsk to cancel all subscriptions.

void pubCancel(void);


//! This function allocates a new subscription table and associates it with
//! the property passed as argument. It returns the subscription index in the
//! variable pointed by sub_idx_ptr.
//!
//! @param[in]   prop            Property to add.
//! @param[out]  sub_idx_ptr     Subscription index.
//!
//! @retval      FGC_OK_NO_RSP   Subscription added.
//! @retval      FGC_PROTO_ERROR Property already subscribed.
//! @retval      FGC_BUSY        Subscription table is full.

uint16_t pubSubscribe(struct prop * const prop,
                      uint8_t     * const sub_idx_ptr);


//! This function is called from CmdUnSub() to cancel a subscription. It returns
//! the subscription index in the variable pointed by sub_idx_ptr.
//!
//! @param[in]   prop            Property to remove.
//! @param[out]  sub_idx_ptr     Subscription index.
//!
//! @retval      FGC_OK_NO_RSP   Subscription removed.
//! @retval      FGC_PROTO_ERROR Property not subscribed.

uint16_t pubUnsubscribe(struct prop * const prop,
                        uint16_t    * const sub_idx_ptr);


//! This function is called if the property(cyc_sel) value has changed.
//! It will notify pubTsk to publish any subscriptions that include this
//! property(cyc_sel), including parents.
//!
//! @param[in]   prop            Property to publish.
//! @param[in]   dev_sel         Sub-device selector.
//! @param[in]   cyc_sel         Cycle selector that has changed its value in the property.
//! @param[in]   set_f           True if the publication is the result of a set command.

void pubPublishProperty(struct prop      * prop,
                        uint8_t     const  dev_sel,
                        uint8_t     const  cyc_sel,
                        bool        const  set_f);


//! Marks the new subscription ready for the first update to be sent.

void pubAcknowledgeSubscription(void);


//! Prints the subscription table
//!
//! @param[in]     c               Command structure.
//! @param[in]     num_elements    Number of elements to print.

uint16_t pubPrintSubscriptionTable(struct cmd        * c,
                                   prop_size_t const   num_elements);


//! This function will publish the FGC state properties at a frequency of
//! 2Hz. This function is complementary to the publication mechanism in
//! place for the properties tracked in the log event (LOG.EVT) for which
//! the list of properties is the array log_evt_prop[]. The properties
//! published by this function must be non PPM ones.

void pub2HzPublication(void);


// EOF
