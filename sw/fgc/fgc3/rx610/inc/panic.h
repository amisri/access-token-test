//! @file  panic.h
//! @brief FGC3 MCU Software - Panic functions
//!
//! The core dump system allows multiple segments of memory to be recorded in MRAM in the event of
//! an unexpected trap.  This is controlled by the core control table which specifies the start address
//! and length of each segment.  The table defines FGC_N_CORE_SEGS segments.  If a segment is not in
//! use, then the length will be set to zero words.  The table is stored in MRAM but is copied to RAM
//! for use by the boot and the main programs.
//!
//! Theoretically 4KB can be stored but in practice the fast watchdog will reset the FGC before more than
//! about 3KB can be recorded, unless the system is in the lab and is running with the watchdog inhibited.

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External structures, unions and enumerations


enum Panic_mcu_codes
{
    MCU_PANIC_EXCEP_SUPERVISOR_INSTR    = 0x10000000,
    MCU_PANIC_EXCEP_INVALID_INSTR       = 0x10000001,
    MCU_PANIC_EXCEP_FLOATING_POINT      = 0x10000002,
    MCU_PANIC_EXCEP_NMI                 = 0x10000003,
    MCU_PANIC_EXCEP_BRK                 = 0x10000004,
    MCU_PANIC_EXCEP_BUS_ERROR           = 0x10000005,
    MCU_PANIC_ISR_TRAP                  = 0x10001000,
    MCU_PANIC_BAD_PARAMETER             = 0x10002000,
    MCU_PANIC_INDEX_OUT_OF_RANGE        = 0x10002001,
    MCU_PANIC_HW_ANATYPE                = 0x10003000,
    MCU_PANIC_HW_ANALOG_CARD            = 0x10003001,
    MCU_PANIC_BUS_ERR                   = 0x10004000,
    MCU_PANIC_SW_CRASH                  = 0x10005000,
    MCU_PANIC_SYSCALL_READ_R            = 0x10006000,
    MCU_PANIC_SYSCALL_LSEEK_R           = 0x10006001,
    MCU_PANIC_SYSCALL_MALLOC_SIZE_R     = 0x10006002,
    MCU_PANIC_SYSCALL_MALLOC_OUTOFMEM_R = 0x10006003,
    MCU_PANIC_SYSCALL_CALLOC_OUTOFMEM_R = 0x10006004,
    MCU_PANIC_SYSCALL_MALLOC_REENT_R    = 0x10006005,
    MCU_PANIC_SYSCALL_CALLOC_REENT_R    = 0x10006006,
    MCU_PANIC_SYSCALL_REALLOC_R         = 0x10006007,
    MCU_PANIC_SYSCALL_FREE_R            = 0x10006008,
    MCU_PANIC_PROP_REFUNLOCK_1          = 0x10007000,
    MCU_PANIC_PROP_REFUNLOCK_2          = 0x10007001,
    MCU_PANIC_PUB_TSK_NULL_PROP_PTR     = 0x10008000,
    MCU_PANIC_PUB_TSK_SUB_NOTIFY        = 0x10008001,
    MCU_PANIC_PUB_TSK_SUB_TABLE_NOTIFY  = 0x10008002

};


struct Panic_mcu
{
    int32_t    code;    //!< Panic ID. See panic function (FGC3 only for now)
    uint32_t * sp;      //!< Stack pointer (for exceptions)
    uint32_t   pc;      //!< Program Counter before the exception.
};


struct Panic_core_vars
{
    uint32_t addr   [FGC_N_CORE_SEGS];
    uint16_t n_words[FGC_N_CORE_SEGS];
};


//! Core control structure - common to all classes
//! NB: the padding word was not needed in FGC2 (as a consequence, sizeof(fgc_coreseg) was equal to 6).
//! But it is required on FGC3.

struct Panic_core_ctrl
{
    struct Panic_core_seg
    {
        uint16_t n_words;                    //!< Length of memory segment in words
        uint16_t pad;                        //!< Padding word (set to 0xFFFF)
        uint32_t addr;                       //!< Address of start of memory segment
    } seg[FGC_N_CORE_SEGS];

    uint32_t addr_in_core[FGC_N_CORE_SEGS];  //!< Start address in the core data for each segment
};



// ---------- External variable declarations

extern struct Panic_mcu       panic_mcu;
extern struct Panic_core_vars core;



// ---------- External function declarations

//! This function is the MCU panic function. It does not return.
//! This function disables interrupts and runs an infinite loop.

void panic(enum Panic_mcu_codes panic_code, uint32_t leds0, uint32_t leds1);


//! On FGC3 a Crash is redirected to panic

void panicCrash(uint32_t crash_code);


//! This function will save the memory segments specified in the core control table to NVRAM.  If the table
//! is corrupted, it will be overwritten with the default table first.

void panicCoreDump(void);


//! This function will retrieve the core control table from NVRAM to *corectrl.  It will check that the table
//! is valid - if it is the function returns with the table in *corectrl.  If it finds that the table is
//! not valid, it writes the default table into *corectrl and into NVRAM and clears the Core data.
//!
//! @param corectrl fgc_corectrl structure to get

void panicCoreTableGet(struct Panic_core_ctrl * core_ctrl);


//! This function will write the corectrl table to the NVRAM and clear the CORE data area.
//!
//! @param corectrl fgc_corectrl structure to set

void panicCoreTableSet(struct Panic_core_ctrl * core_ctrl);


// EOF
