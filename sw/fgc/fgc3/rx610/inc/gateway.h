//! @file  gateway.h
//! @brief Ethernet ethernet Interface Functions

#pragma once


// ---------- Includes

#include <stdbool.h>



// ---------- External function declarations

// TODO: Make sure that fbs.h no longer needs to include fgc_gateway.h
// External functions declarations

//! Function called by EthInit(void). Its purpose is to initialize variables, function arrays and the packet header
//! which will then be used to send packets to the gateway.

void gatewayInit(void);


//! This function is called by the end of DMA interrupt routine.
//! It updates circular buffer index if all data has been copied.

void gatewayDmac1End(void);


//! This function is called by the EthISR when the packet received has been recognized as being a packet sent by the gateway
//! It then calls the specific function for receiving a packet based on the payload type, Time or Command

void gatewayReceivePacket(void);


//! This function will send the status variable data to the ETH.
//! It returns 1 on success and 0 on error (as FIP counterpart).
//! Copy terminal character if any in the buffer.

bool gatewaySendStatVar(void);


//! This function checks that 50 status packets are sent every second.
//! If not, the property is incremented.

void gatewayCheckStatusTimePackets(void);


//! This function is called by FbsWriteMsg() to send the pending message (PUB/RSP) to the Ethernet
//! The pending message can either be a command packet or a pub packet.
//! It returns 1 on success and 0 on error (as FIP counterpart).

void gatewaySendMsg(void);


// EOF
