//! @file  trap.h
//! @brief

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External function declarations

//! All other interrupts

void IsrDummy(void) __attribute__((interrupt));


//! All other interrupts

void IsrDummyFix(void) __attribute__((interrupt));


//!

void IsrDummyEmpty(void) __attribute__((interrupt));


//! This ISR will be called in response to an undefined instruction.

void IsrUndefinedInstruction(void) __attribute__((interrupt));


//! This ISR will be called in response to an overflow and a INTO instruction

void IsrOverflow(void) __attribute__((interrupt));


//! This ISR will be called by the Rx610 watchdog timer.

void IsrWatchdogTimer(void) __attribute__((interrupt));


// EOF
