//! @file  lan92.h
//! @brief Ethernet eth Interface Functions

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <ethernet.h>
#include <memmap_mcu.h>



// ---------- Constants

// Options for TX commands A & B

#define TX_CMDA_IOC                     0x80000000      //!< Wait for Interrupt on Completion
#define TX_CMDA_FIRST_SEG               0x00002000      //!< Indicates this is the first segment
#define TX_CMDA_LAST_SEG                0x00001000      //!< Indicates this is the last segment

// PHY registers addresses. (The PHY registers are not memory mapped.)

#define ETH_PHY_BSR_ADDR                1                 //!< BSR: Basic Status Register

// PHY registers bit maps
//
//  - BSR (Basic Status Register):

#define ETH_PHY_BSR_LINK_STATUS_MASK    0x0004
#define ETH_PHY_BSR_NEGOC_STATUS_MASK   0x0020



// ---------- External structures, unions and enumerations

enum Lan92_status
{
    LAN92_OK,
    LAN92_SIZE_OUT_OF_BOUNDS,
    LAN92_TX_FIFO_FULL,
    LAN92_DMA_IN_PROGRESS,
    LAN92_TX_NOT_READY,
};



// ---------- External function declarations

//! This function reads the number of bytes from the RX FIFO to the given pointer
//! Number of bytes available in fifo are given and decremented accordingly
//! If read_length is not a multiple of 4, extra words (not read) are saved for further function call
//! in static variable (non-reentrant)

void lan92ReadFifo(void const * to, uint16_t read_length, uint16_t * fifo_length);


//! A RX dump reset the RX data and status FIFO.
//! Useful to restart RX after a FIFO overflow.

void lan92RxDump(void);


//! This function send the given data.
//! Packet_size is in byte and must be a multiple of 4 (32 bits access)
//! Return 0 if OK

enum Lan92_status lan92SendPacket(void const * packet_data, uint16_t packet_size);


//!  This function sends the given data, with extra options optA and optB (not checked) for TXcommanA and B.
//! segment_size is the size of the current segment to be written, pointed by *segment
//! Packet_size is the size of the full packet in byte and must be a multiple of 4 (32 bits access)
//! Giving a NULL segment is not error, but no data will be written.
//! In this case, you must write the data by yourself and must be sure you write exactly segment size bytes
//! Return 0 if OK
//! If not OK, it is the duty of caller to recover. Either by trying again later or by reseting the chip.
//! Note that TX_STOP + TX data and status dump + TX_ON does not enable to recover (TX_STOP stays high).

enum Lan92_status lan92SendPacketOpt(void     const * segment,
                                     uint16_t         segment_size,
                                     uint16_t         packet_size,
                                     uint32_t         optA,
                                     uint32_t         optB);


//! Discards remainder of current packet from RX FIFO
//!
//! @param[in]  num_bytes  Number of bytes to discard

void lan92DiscardPacket(uint16_t num_bytes);


//! Set MAC address

void lan92SetMac(uint8_t mac[6]);


//! Get the MAC address

void lan92GetMac(uint8_t mac[6]);


//! Read the PHY register

uint16_t lan92ReadPhyReg(uint16_t addr);


//! Write the PHY register

void lan92WritePhyReg(uint16_t value, uint16_t addr);



// ---------- External function definitions

//! Return 0 if no error.

static inline bool lan92IsRxError(void)
{
    return ETH_INT_STS_P & ETH_INT_STS_RXE;
}



//! Return 0 if no error.

static inline bool lan92IsTxError(void)
{
    return ETH_INT_STS_P & ETH_INT_STS_TXE;
}



//! Return 0 if no overrun.

static inline bool lan92IsRxOverrun(void)
{
    return ETH_RX_DROP_P;
}



//! Return TRUE if link is up.

static inline bool lan92IsLinkUp(void)
{
    return (lan92ReadPhyReg(ETH_PHY_BSR_ADDR) & ETH_PHY_BSR_LINK_STATUS_MASK) == ETH_PHY_BSR_LINK_STATUS_MASK;
}



//! Acknowledge interrupt

static inline void lan92AckInterrupt(enum Ethernet_int_sts_bitmasks interrupt)
{
    ETH_INT_STS_P |= interrupt;
}


// EOF
