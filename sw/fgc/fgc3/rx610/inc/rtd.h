//! @file  rtd.h
//! @brief Global option line display

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

#include <libterm.h>
#include <trm.h>



// ---------- Constants

//! Save then set cursor position

#define RTD_START_POS           TERM_HIDE_CURSOR TERM_SAVE_POS TERM_CSI "%s" TERM_GOTO TERM_CSI "%s" TERM_SGR

//! Restore cursor position

#define RTD_END                 TERM_SHOW_CURSOR TERM_RESTORE_POS "\v"

//! Value with fault condition

#define RTD_VALUE_FAULT         0x01

//! Value not available

#define RTD_VALUE_NO_PRINT      0x02


typedef  bool (*rtd_func)(bool);



// ---------- External structures, unions and enumerations

//! Error field structure

struct Rtd_field_error
{
    uint8_t                 value;
    uint8_t                 last_value;
};

//! Float field structure

struct Rtd_field_float
{
    float     const *       source;
    char      const * const format;
    float                   last_value;
    uint32_t  const         factor;
    char      const * const position;
    uint8_t                 width;
};

//! Float field structure with possible error conditions

struct Rtd_field_error_float
{
    struct Rtd_field_float  field;
    struct Rtd_field_error  error;
};


//! Integer field structure

struct Rtd_field_int
{
    int32_t const * const source;
    int32_t               last_value;
    char    const * const format;
    char    const * const position;
};

//! Bool field structure

struct Rtd_field_bool
{
    bool    const * const source;
    bool                  last_value;
    bool                  first_print;
    char    const * const position;
};


//! Temperature field structure

struct Rtd_field_temp
{
    uint16_t const * const source;
    uint16_t               last_value;
    char     const * const position;
};

//! String field structure

struct Rtd_field_string
{
    char                   last_value[TRM_BUF_SIZE];
    char     const * const position;
    char     const * const print_style;
};

//! Function field structure

struct Rtd_field_func
{
    rtd_func               func;
};

//! Optional line field structure

struct Rtd_field_optional
{
    uint8_t  const         option;
    rtd_func               func;
};

//! Function field structure

struct Rtd_process_args
{
    void           *       list_fields;
    void           *       field;
};

//! Process structure

struct Rtd_process
{
    bool                     (*func)(struct Rtd_process_args *);
    struct Rtd_process_args  args;
};

//! Struct with external variables. Either used by other modules,
//! or needed by both the general and class specific rtd code.

struct Rtd_vars
{
    struct Rtd_process * process;
    FILE               * f;                              //!< Pointer to RTD output stream
    char               * serial_stream_buffer_ptr;
    uint16_t             serial_stream_buffer_offset;
    uint16_t             line_offset;
    uint16_t             ctrl;                           //!< Option line control
    uint16_t             last_ctrl;                      //!< Old value of option line control
};

enum Rtd_string_fields
{
    FIELD_STRING_FAULTS,
    FIELD_STRING_CONFIG,
    FIELD_STRING_PERMIT,
};



// ---------- External variable declarations

extern struct Rtd_vars rtd;



// ---------- Platform/class specific function definitions

//! Reset last value variables and write rtd template to the terminal.
//!
//! @param[in,out]  f    File descriptor

void rtdClassReset(FILE * f);


//! Processes the class specific fields

bool rtdClassProcess(bool reset);


//! Updates the error conditions for float fields

void rtdClassUpdateErrors(void);


//! Class specific initialisation settings.

void rtdClassInit(void);



// ---------- External function declarations

//! This function is the Real-time display task.
//!
//! The real-time display is active only if the line editor is enabled.
//! The Rtd Task is triggered every 100 ms by the millisecond task. It
//! works through the different fields in turn, looking for a field with
//! changed data. If one is found, it refreshes the display for this value.
//! Only one field is updated per cycle, so a maximum of 10 fields may be
//! updated per second.
//!
//! It makes use of the following vt100 control codes (ESC = '\33'):
//!   ESC 7       Save cursor position and attributes
//!   ESC [y;xH   Move cursor to (x,y)
//!   ESC 8       Restore saved cursor position and attributes

void rtdTask(void *);


//! This function prepares the screen for the 4 line real-time display.
//!
//! It uses the following vt100 control codes (ESC='\33'):
//!   ESC [y;xH     Move cursor to (x,y)
//!   ESC [H        Move cursor to top left of screen
//!   ESC [1;19r    Set scroll window to lines 1 to 19
//!
//! @param[in,out]  f    File descriptor

void rtdReset(FILE * f);


//! Initialise rtd variables.

void rtdInit(void);


//! Enable the real time display.

void rtdEnable(void);


//! Disable the real time display.

void rtdDisable(void);


//! Check if the real time display is enabled.
//!
//! @retval  true    RTD is enabled
//! @retval  false   RTD is disabled

bool rtdIsEnabled(void);


//! Moves the cursor to the desired position
//!
//! @param[in]      position   Position to move the cursor to
//! @param[in]      format     Printing style options (bold, reverse, colours, etc)

void rtdMoveCursor(const char * position, char const * const format);


//! Restore cursor position the cursor to the desired position

void rtdRestoreCursor(void);


//! This function processes a function associated to a field. The function checks
//! if the value has changed before displaying it.
//!
//! @param[in]      args       Arguments for the field process function
//!
//! @retval         true       Value was updated
//! @retval         false      Value was not updated

bool rtdProcessFuncs(struct Rtd_process_args * args);


//! This function displays a float32_t value on the screen. The function checks
//! if the value has changed before displaying it.
//!
//! @param[in]      args       Arguments for the field process function
//!
//! @retval         true       Value was updated
//! @retval         false      Value was not updated

bool rtdProcessFloats(struct Rtd_process_args * args);


//! This function displays a float32_t value on the screen. The function checks
//! if the value has changed before displaying it. If the value is in error
//! or not available it will update the color.
//!
//! @param[in]      args       Arguments for the field process function
//!
//! @retval         true       Value was updated
//! @retval         false      Value was not updated

bool rtdProcessFloatsErrors(struct Rtd_process_args * args);


//! This function displays an int32_t value on the screen. The function checks
//! if the value has changed before displaying it.
//!
//! @param[in]      args       Arguments for the field process function
//!
//! @retval         true       Value was updated
//! @retval         false      Value was not updated

bool rtdProcessInts(struct Rtd_process_args * args);


//! This function displays a bool value on the screen. The function checks
//! if the value has changed before displaying it.
//!
//! @param[in]      args       Arguments for the field process function
//!
//! @retval         true       Value was updated
//! @retval         false      Value was not updated

bool rtdProcessBools(struct Rtd_process_args * args);


//! This function displays a temperature (uint16_t) value on the screen. The function checks
//! if the value has changed before displaying it.
//!
//! @param[in]      args       Arguments for the field process function
//!
//! @retval         true       Value was updated
//! @retval         false      Value was not updated

bool rtdProcessTemps(struct Rtd_process_args * args);


//! This function reversees and bolds a character to signal a field value has an error.
//!
//! @param[in]      args       Arguments for the field process function
//!
//! @retval         true       Value was updated
//! @retval         false      Value was not updated

bool rtdProcessErrors(struct Rtd_process_args * args);


//! This function processes the optional lines.
//!
//! @param[in]      args       Arguments for the field process function
//!
//! @retval         true       Value was updated
//! @retval         false      Value was not updated

bool rtdProcessOptionLine(struct Rtd_process_args * args);


//! This function prints a float value if it has been modified.
//!
//! @param[in]      field      Necessary parametres for writing the value
//! @param[in]      error      Necessary parametres for handling errors

bool rtdPrintFloat(struct Rtd_field_float * field, struct Rtd_field_error * error);


//! This function prints an integer value if it has been modified.
//!
//! @param[in]      rtd_field  Necessary parametres for writing the value

bool rtdPrintInt(struct Rtd_field_int * field);


//! This function prints an bool value if it has been modified.
//!
//! @param[in]      rtd_field  Necessary parametres for writing the value

bool rtdPrintBool(struct Rtd_field_bool * field);


//! This function prints an temperature value if it has been modified.
//!
//! @param[in]      rtd_field  Necessary parametres for writing the value

bool rtdPrintTemp(struct Rtd_field_temp * field);


//! This function prints a string if the value has been modified.
//!
//! @param[in]      rtd_field  Necessary parametres for writing the value

bool rtdPrintString(char const * source, struct Rtd_field_string * field);


//! This function sets the debug string that is output on the optinn line
//! when RTD = DEBUG.
//!
//! @param[in]      debug      Debug string

void rtdSetOptionDebug(char const * debug);


// EOF
