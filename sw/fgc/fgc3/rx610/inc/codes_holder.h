//! @file  codes_holder.h
//! @brief FGC Boot code container

#pragma once


// ---------- Includes

#include <stdint.h>

#include <fgc_codes_gen.h>
#include <fgc_code.h>
#include <fgc_consts_gen.h>
#include <label.h>
#include <m16c62_link.h>



// ---------- Constants

//! code states
#define CODE_STATE_UNKNOWN          0
#define CODE_STATE_DEV_FAILED       1
#define CODE_STATE_CORRUPTED        2
#define CODE_STATE_NOT_AVL          3
#define CODE_STATE_VALID            4

//! Advertised code states
#define ADV_CODE_STATE_NOT_IN_USE   0
#define ADV_CODE_STATE_OK           1
#define ADV_CODE_STATE_UNKNOWN_CODE 2
#define ADV_CODE_STATE_BAD_LEN      3

//! Update states
#define UPDATE_STATE_UNKNOWN    0
#define UPDATE_STATE_UPDATE     1
#define UPDATE_STATE_OK         2
#define UPDATE_STATE_NOT_AVL    3
#define UPDATE_STATE_LOCKED     4



// ---------- Internal structures, unions and enumerations

struct Codes_block
{
    uint8_t     class_id;
    uint8_t     code_id;
    uint16_t    block_idx;
    uint16_t    code[(FGC_CODE_BLK_SIZE / 2)];
};

//! struct for information received via the fieldbus (in time_var) from the gateway about "codes"

struct Codes_advertised_info
{
    uint8_t     code_id;            // Code ID
    uint8_t     class_id;           // FGC class ID
    uint16_t    len_blks;           // not used at FGC3 boot
    uint32_t    version;            // Version (unixtime)
    uint16_t    old_version;        // Version used for the old versioning system
    uint16_t    crc;
};

//! This is a small subset from enum fgc_code_ids (customised for the FGC3 boot)
//! warning!!! changes here affects the pre filled variables codes_holder_info[] (in this file) and
//! erase_dev[], program_dev[], write_dev[], check_dev[] (in codes_fgc.h)

enum Codes_ids_short_list
{
    SHORT_LIST_CODE_PLD,    // Xilinx FPGA bitstream eeprom
    SHORT_LIST_CODE_BT,     // Boot for operation
    SHORT_LIST_CODE_MP_DSP, // Main programs
    SHORT_LIST_CODE_IDPROG, // M16C62 block 3    (IdProg)
    SHORT_LIST_CODE_IDDB,   // M16C62 blocks 4-6 (IDDB)
    SHORT_LIST_CODE_PTDB,   // M16C62 block 1    (PTDB)
    SHORT_LIST_CODE_SYSDB,  // Sys Data base
    SHORT_LIST_CODE_COMPDB, // Comp Data base
    SHORT_LIST_CODE_DIMDB,  // Dim Data base
    SHORT_LIST_CODE_NAMEDB, // Name Data base
    SHORT_LIST_CODE_LAST    // dummy to have the number of elements to use in the for/while loops
};

struct Codes_holder_info
{
    uint16_t            len_blks;       // Flash size (blocks)
    uint32_t            base_addr;      // Flash base address
    struct Codes_advertised_info update; // Update code info (from Advertised Code table)
    char              * label;          // Flash label, max 7 chars, as used in MenuCodesShowInstalled
    uint16_t            locked_f;       // Code locked flag - 2 bytes because FGC3 pointer is 4 bytes
    uint16_t            read_only_f;    // Read only flag
    struct fgc_code_info dev;           // Installed code info
    uint16_t            update_state;
    uint16_t            dev_state;      // State of installed code
    uint16_t            calc_crc;       // not used at FGC3 boot
};



// ---------- External variable declarations

extern struct Codes_holder_info codes_holder_info[SHORT_LIST_CODE_LAST];

//! Code state symbol names.
extern const struct sym_name code_state[];

//! Code update state symbol names.
extern const struct sym_name update_state[];

//! Code advertised state symbol names.
extern const struct sym_name adv_code_state[];

//! Extracted from NameDB
extern struct fgc_dev_name   dev_name;

//! Extracted from NameDB
extern struct fgc_dev_name   gw_name;


//EOF
