//! @file     taskTrace.h
//! @brief    Used for tracing the various MCU tasks

#pragma once


// ---------- Includes

#include <stdint.h>

#include <mcu.h>
#include <defconst.h>



// ---------- External structures, unions and enumerations

struct Task_trace
{
    uint32_t sem_info[FGC_NUM_SEMAPHORES];     //!< FGC.DEBUG.SEM_INFO
    uint8_t  trace   [MCU_NUM_TASKS];          //!< FGC.DEBUG.TASK.TRACE
    uint8_t  iter_cnt[MCU_NUM_TASKS];          //!< FGC.DEBUG.TASK.ITERS
};



// ---------- External variable definitions

extern struct Task_trace task_trace;



// ---------- External function definitions

static inline void taskTraceInc(void)
{
    task_trace.trace[os.curr_tid]++;
}



static inline void taskTraceReset(void)
{
    task_trace.trace[os.curr_tid] = 0;
    task_trace.iter_cnt[os.curr_tid]++;
}


// EOF
