//! @file  events.h
//! @brief Timing event processing

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <fgc_event.h>
#include <time_fgc.h>



// ---------- External structures, unions and enumerations

//! Event source

enum Events_source
{
    EVENTS_SOURCE_GATEWAY,
    EVENTS_SOURCE_SIMULATED,
    EVENTS_SOURCE_EXTERNAL,
};



//! Structure associated to properties

struct Events_prop
{
    uint32_t              ref_event;                  //!< REF.EVENT_CYC     Timing event type associated to the function start
    uint32_t              external_us[2];             //!< REF.EVENT_EXT_US  Delay of the timing events associated to an external trigger
    uint16_t              event_group;                //!< REF.EVENT_GROUP
};


//! Structure with event payload data

struct Events_data
{
    uint16_t              next_destination;           //!< Next destination for START_REF events

    union Events_data_payload
    {
        uint16_t          data;                       //!< Data
        uint16_t          transaction_id;             //!< Transaction ID
        uint16_t          event_group;                //!< Event group
        uint16_t          next_dest;                  //!< Next destination
        uint16_t          cycle_tag;                  //!< Cycle tag

        struct Events_data_payload_cycle
        {
            uint8_t       sub_sel_ref;                //!< Reference sub-device selector
            uint8_t       cyc_sel_ref;                //!< Reference cycle selector
            uint8_t       sub_sel_acq;                //!< Acquisition sub-device selector
            uint8_t       cyc_sel_acq;                //!< Acquisition cycle selector
        } cycle;
    } payload;                                        //!< Event payload
};



typedef void (*EventsClassFunc)(struct CC_us_time const *, struct Events_data const data);
typedef void (*EventsFunc     )(struct fgc_event const *);



// ---------- External variable declarations

extern struct Events_prop events_prop;



// ---------- Platform/class specific function declarations

//! Class specific initialization used to register class-specific event handlers

void eventsClassInit(void);


//! Class specific process for events

void eventsClassProcess(void);



// ---------- External function declarations

//! Initializes the events module.

void eventsInit(void);


//! Processes timing events.

void eventsProcess(void);


//! Events task

void eventsTask(void * unused);


//! Registers a function to a given event

void eventsRegisterEventFunc(enum fgc_events type, EventsClassFunc function);


//! Returns True if the external timing is enabled

enum Events_source eventsGetSource(void);


// EOF
