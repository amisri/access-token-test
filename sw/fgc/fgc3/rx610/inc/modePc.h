//! @file  modePc.h
//! @brief This file hanldes the setting of the PC mode

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <fgc_errs.h>



// ---------- External structures, unions and enumerations

struct ModePc
{
	uint32_t ref_coast;                 //!< Property REF.COAST
    uint16_t pc;                        //!< Property MODE.PC
    uint16_t pc_simplified;             //!< Property MODE.PC_SIMPLIFIED
    uint16_t pc_on;                     //!< Property MODE.PC_ON
};



// ---------- External variable declarations

extern struct ModePc mode_pc;



// ---------- Platform/class specific function declarations

//! Validates the requested mode PC
//!
//! @param[in] mode  Requested PC mode
//! @return          FGC_OK_NO_RSP if no error.

uint32_t modePcClassValidateMode(uint8_t mode);



// ---------- External function declarations

//! Initialize module

void modePcInit(void);


//! Asynchronous change of the CCLIBS mode_ref variable as a function of the
//! external conditions (e.g. PC_PERMIT).

void modePcProcess(void);


//! Sets the mode PC.
//!
//! @param  mode  Mode PC to set
//! @retval       FGC_OK_NO_RSP if no error. Otherwise an error.

enum fgc_errno modePcSet(uint16_t mode);


//! Sets the simplified mode PC.
//!
//! @param  modeSimplified  Mode PC simplified to set
//! @retval                 FGC_OK_NO_RSP if no error. Otherwise an error.

enum fgc_errno modePcSetSimplified(uint16_t mode_simplified);


//! Returns the mode PC.
//!
//! @return                 Mode PC

uint16_t modePcGet(void);


//! Get the internal mode PC.
//!
//! @return                  Internal mode PC

uint16_t modePcGetInternal(void);


//! Get the mode PC on.
//!
//! @return                  Mode PC on

uint16_t modePcGetPcOn(void);


//! Get the simplified mode PC.
//!
//! @return                  Simplified mode PC

uint16_t modePcGetSimplified(void);


//! Set the internal simplified mode PC.
//!
//! @param  mode_pc_simplified Internal mode PC simplified

void modePcSetModeInternalFromSimplified(uint16_t mode_pc_simplified);


// EOF
