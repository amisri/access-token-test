//! @file   ethernet.h
//! @brief  Ethernet eth Interface Function

#pragma once


// ---------- Includes

#include <stdint.h>
#include <etherComm.h>



// ---------- External structures, unions and enumerations

//! Bitmasks used to handle Tx status

enum Ethernet_tx_status_bitmasks
{
    ETH_TX_STATUS_DEFFERED             = 0x00000001,
    ETH_TX_STATUS_EXCESSIVE_DEFERRAL   = 0x00000002,
    ETH_TX_STATUS_COLLISION_COUNT      = 0x00000078,
    ETH_TX_STATUS_EXCESSIVE_COLLISIONS = 0x00000100,
    ETH_TX_STATUS_LATE_COLLISION       = 0x00000200,
    ETH_TX_STATUS_NO_CARRIER           = 0x00000400,
    ETH_TX_STATUS_LOSS_OF_CARRIER      = 0x00000800,
    ETH_TX_STATUS_ERROR_STATUS         = 0x00008000,
    ETH_TX_STATUS_PACKET_TAG           = 0xFFFF0000,
};


//! Bitmasks used to handle Rx status

enum Ethernet_rx_status_bitmasks
{
    ETH_RX_STATUS_CRC_ERROR        = 0x00000002,
    ETH_RX_STATUS_DRIBBLING_BIT    = 0x00000004,
    ETH_RX_STATUS_MII_ERROR        = 0x00000008,
    ETH_RX_STATUS_WATCHDOG_TIMEOUT = 0x00000010,
    ETH_RX_STATUS_FRAME_TYPE       = 0x00000020,
    ETH_RX_STATUS_COLLISION_SEEN   = 0x00000040,
    ETH_RX_STATUS_FRAME_TOO_LONG   = 0x00000080,
    ETH_RX_STATUS_MULTICAST_FRAME  = 0x00000400,
    ETH_RX_STATUS_RUNT_FRAME       = 0x00000800,
    ETH_RX_STATUS_LENGTH_ERROR     = 0x00001000,
    ETH_RX_STATUS_BROADCAST_FRAME  = 0x00002000,
    ETH_RX_STATUS_ERROR_STATUS     = 0x00008000,
    ETH_RX_STATUS_PACKET_LENGTH    = 0x3FFF0000,
    ETH_RX_STATUS_FILTERING_FAIL   = 0x40000000,
};


//! Bitmasks used to handle transmit configuration register (TX_CFG)

enum Ethernet_tx_cfg_bitmasks
{
    ETH_TX_CFG_STOP_TX  = 0x00000001,
    ETH_TX_CFG_TX_ON    = 0x00000002,
    ETH_TX_CFG_TXSAO    = 0x00000004,
    ETH_TX_CFG_TXD_DUMP = 0x00004000,
    ETH_TX_CFG_TXS_DUMP = 0x00008000,
};


//! Bitmasks used to handle transmit FIFO information register (TX_FIFO_INF)

enum Ethernet_tx_fifo_inf_bitmasks
{
    ETH_TX_FIFO_INF_TDFREE  = 0x0000FFFF,
    ETH_TX_FIFO_INF_TXSUSED = 0x00FF0000,
};


//! Bitmasks used to handle receive FIFO information register (RX_FIFO_INF)

enum Ethernet_rx_fifo_inf_bitmasks
{
    ETH_RX_FIFO_INF_RXDUSED = 0x0000FFFF,
    ETH_RX_FIFO_INF_RXSUSED = 0x00FF0000,
};


//! Bitmasks used to handle interrupt status register (INT_STS) and interrupt enable register (INT_EN)

enum Ethernet_int_sts_bitmasks
{
    ETH_INT_STS_GPIO0_INT  = 0x00000001,
    ETH_INT_STS_GPIO1_INT  = 0x00000002,
    ETH_INT_STS_GPIO2_INT  = 0x00000004,
    ETH_INT_STS_RSFL       = 0x00000008,
    ETH_INT_STS_RSFF       = 0x00000010,
    ETH_INT_STS_RXDF_INT   = 0x00000040,
    ETH_INT_STS_TSFL       = 0x00000080,
    ETH_INT_STS_TSFF       = 0x00000100,
    ETH_INT_STS_TDFA       = 0x00000200,
    ETH_INT_STS_TDFO       = 0x00000400,
    ETH_INT_STS_TXE        = 0x00002000,
    ETH_INT_STS_RXE        = 0x00004000,
    ETH_INT_STS_RWT        = 0x00008000,
    ETH_INT_STS_TXSO       = 0x00010000,
    ETH_INT_STS_PME_INT    = 0x00020000,
    ETH_INT_STS_PHY_INT    = 0x00040000,
    ETH_INT_STS_GPT_INT    = 0x00080000,
    ETH_INT_STS_RXD_INT    = 0x00100000,
    ETH_INT_STS_TX_IOC     = 0x00200000,
    ETH_INT_STS_RXDFH_INT  = 0x00800000,
    ETH_INT_STS_RXSTOP_INT = 0x01000000,
    ETH_INT_STS_TXSTOP_INT = 0x02000000,
    ETH_INT_STS_SW_INT     = 0x80000000,
};


//! Bitmasks used to handle interrupt configuration register (IRQ_CFG)

enum Ethernet_irq_cfg_bitmasks
{
    ETH_IRQ_CFG_IRQ_TYPE     = 0x00000001,
    ETH_IRQ_CFG_IRQ_POL      = 0x00000010,
    ETH_IRQ_CFG_IRQ_EN       = 0x00000100,
    ETH_IRQ_CFG_IRQ_INT      = 0x00001000,
    ETH_IRQ_CFG_INT_DEAS_STS = 0x00002000,
    ETH_IRQ_CFG_INT_DEAS_CLR = 0x00004000,
    ETH_IRQ_CFG_INT_DEAS     = 0xFF000000,
};


//! Bitmasks used to handle hardware configuration register (HW_CFG)

enum Ethernet_hw_cfg_bitmasks
{
    ETH_HW_CFG_SRST      = 0x00000001,
    ETH_HW_CFG_SRST_TO   = 0x00000002,
    ETH_HW_CFG_TX_FIF_SZ = 0x000F0000,
    ETH_HW_CFG_MBO       = 0x00100000,
    ETH_HW_CFG_AMDIX_EN  = 0x01000000,
    ETH_HW_CFG_FSELEND   = 0x10000000,
    ETH_HW_CFG_FPORTEND  = 0x20000000,
};



// ---------- External variable declarations

//! This function is called from main() and CheckStatus() to prepare the ethernet LAN9215 interface.

void ethernetInit(void);


//! This function check the LAN chip is in a consistent state. Recover if not

void ethernetWatch(void);


//! This function is called from main() and CheckStatus() to prepare the ethernet LAN9215 interface.

uint16_t ethernetHwInit(uint16_t timeout_ms);


//! This function will manage the interrupt from the lan9221 chip
//! All the processing of received packets takes place in the interrupt
//! Note: IRQ are sensitive to falling edge, therefore we need to clear ALL 
//! the lan92 interrupts so that the interrupt line goes inactive and IRQ is 
//! rearmed. If a new interrupt occurs during the routine, it MUST be processed 
//! and cleared as well. With the de-assertion interval, we are sure no interrupt 
//! can arrive before leaving this routine.

void ethernetISR(void);


// EOF

