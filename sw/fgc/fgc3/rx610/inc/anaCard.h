//! @file  anaCard.h
//! @brief FGC3 analogue card interface

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>

#include <defconst.h>



// ---------- Constants

#define SD_FLTR_LEN_W   0x4000  //!< 0x2000 samples max per filter function
#define FIR_MAX_LENGTH  16000
#define FIR_ORDER       4



// ---------- External structures, unions and enumerations


enum Ana_card_type
{
    ANA_CARD_NO_CARD  = FGC_INTERFACE_NO_CARD,
    ANA_CARD_TYPE_101 = FGC_INTERFACE_ANA_101,
    ANA_CARD_TYPE_103 = FGC_INTERFACE_ANA_103,
    ANA_CARD_TYPE_104 = FGC_INTERFACE_ANA_104,
};

struct Ana_card_adc
{
    uint16_t internal_adc_mpx[FGC_N_ADCS]; //!< Property ADC.ADC16.MPX
};

struct Ana_card_temp_regul
{
    float    adc_gain;      //!< ADC value over temperature value
    int32_t  integrator;    //!< Integral register
    int32_t  prev_error;    //!< Previous error register
    uint16_t prop_gain;     //!< Proportional gain          ADC.INTERNAL.VREF_TEMP.PID[0]
    uint16_t int_gain;      //!< Integral     gain          ADC.INTERNAL.VREF_TEMP.PID[1]
    uint16_t diff_gain;     //!< Derivative   gain          ADC.INTERNAL.VREF_TEMP.PID[2]
    uint16_t adc_ref;       //!< Temperature reference (raw ADC units)
    uint16_t on_f;          //!< Regulation ON flag
};



// ---------- External variable definitions


extern struct Ana_card_adc        adc;
extern struct Ana_card_temp_regul ana_temp_regul;



// ---------- External function declarations

//! Initialisation function

void anaCardInit(void);


//! Returns the analogue card type.
//!
//! @return ANA_CARD_TYPE_101 or ANA_CARD_TYPE_103.

enum Ana_card_type anaCardGetType(void);


//! Copy the current state of the ADC Multiplexing from shared memory to the
//! property variable.

void anaCardCpyMpx(void);


//! This function initialises the temperature regulation.
//!
//! On the MAIN, this function must be called AFTER nvsInit() so that ADC.INTERNAL.VREF_TEMP.REF is set.

void anaCardTempInit(void);


//! This function starts the temperature regulation.
//!
//! @param ref_temp Target temperature in C.

void anaCardTempRegulStart(float ref_temp);


//! This function computes the analogue interface temperature regulation.
//!
//! Uses a PID controller running at 1Hz.

void anaCardTempRegul(void);


//! Reads the temperature value of the voltage reference on the analogue board.
//!
//! ADC values are 10 bit.
//! 1deg = 47mV
//! temp(deg) = ADC_val * 3.3 / 1023 / 0.047  (*0.068634)
//!
//! @retval The temperature value in oC.

float anaCardGetTemp(void);


//! Returns True if the temperature regulation is on, False otherwise.
//!
//! @retval True if the temperature regulation is on, False otherwise.

bool anaCardTempRegulOn(void);


// EOF
