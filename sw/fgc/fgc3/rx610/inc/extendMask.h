//! @file  extendMask.h
//! @brief Provide a bit mask data structure with up to 256 bits

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>



// ---------- Constants

#define ARR_ANY_SIZE (1)



// ---------- External structures, unions and enumerations

//! This is the generic mask structure that the functions defined in this module expect as an argument.
//! There is no point in allocating this structure directly, since it only provides a mask of 16 bits.
//!
//! The user should allocate a similar structure, adapted to its needs, for instance:
//!
//!   struct mask_128bits            // A 128 bit mask. To be casted with struct extended_mask_gen in extendMask.h
//!   {
//!       uint16_t master;
//!       uint16_t mask_array[8];    // 8*16 = 128 bit array
//!   };
//!
//!   struct mask_128bits   mask_128bits_inst;   // An instance of a 128 bit mask
//!
//!
//! And cast &mask_128bits_inst with (struct extended_mask_gen*) before passing it as an argument to the
//! functions of this module. Carrying on with the same example, the user would type:
//!
//!   extendMaskSetBit((struct extended_mask_gen *)&mask_128bits_inst, 42);      // To set   bit 42 in mask_128bits_inst
//!   extendMaskClrBit((struct extended_mask_gen *)&mask_128bits_inst, 24);      // To clear bit 24 in mask_128bits_inst

struct ExtendMask_gen
{
    uint16_t  master;                         //!< Master mask where each bit represent one word in mask_array[]
    uint16_t  mask_array[ARR_ANY_SIZE];       //!< Mask array. The intent here is to use a flexible array member,
                                              //!< but this is only supported in C99. The present syntax, with 1
                                              //!< as the size of the array, achieves the same and is fully portable
};



// ---------- External variable declarations

extern uint16_t const MASK16[16];
extern uint16_t const IDX_MASK16[16];



// ---------- External function declarations

//! Test a bit in an extended mask and returns a bool

#define extendMaskTestBit(extended_mask, idx) ( testBitmap((extended_mask).mask_array[(idx)>>4], MASK16[(idx)&0xF]) )


//! This function scans a bit index to find the next set bit. At least one bit must be set somewhere
//! or the function will return FALSE.
//!
//! @param[in,out]  extend_mask_ptr    Structure with the masks to scan
//! @param[in,out]  idx_ptr            Indicates from which index to start the search. If a set bit if
//!                                    found after that index, it is returned. Else the search is done
//!                                    starting from bit 0. As an output this is the index of the first
//!                                    set bit that has been found
//!
//! @retval         true               A set bit has been found
//! @retval         false              A set bit has not been found

bool extendMaskScan(struct ExtendMask_gen * const extend_mask_ptr, uint8_t * idx_ptr);


//! This function sets the index bit in the index array and the corresponding bit in the
//! master mask index
//!
//! @param[in,out]  extend_mask_ptr    Structure with the masks to set
//! @param[in,out]  idx_ptr            Index bit to set

void extendMaskSetBit(struct ExtendMask_gen * const extend_mask_ptr, uint8_t idx);


//! This function clears the index bit in the index array and the corresponding bit in the
//! master mask index
//!
//! @param[in,out]  extend_mask_ptr    Structure with the masks to clear
//! @param[in,out]  idx_ptr            Index bit to clear

void extendMaskClrBit(struct ExtendMask_gen * const extend_mask_ptr, uint8_t idx);


// EOF
