//! @file  stateTask.h
//! @brief Power Converter State Machine

#pragma once


// ---------- External function declarations ----------------------------

//! This function is the PC State manager task.
//!
//! It is triggered by OSTskResume() in msTask() on milliseconds 2, 7, 12
//! and 17, provided the FGC is configured.

void stateTask(void *);


//! This function initialises the interface and states related to the power
//! converter.

void stateTaskInit(void);


// EOF
