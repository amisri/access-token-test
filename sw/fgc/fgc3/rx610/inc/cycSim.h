//! @file  cycSim.h
//! @brief API for simulating a supercycle
//!
//! In order to correctly use the supercycle simulation module, call this API's functions in the following order (the order of steps 2 and 3 may be changed):
//!
//!  1) cycSimInit()             - This will give the module the necessary basic settings for running the simulation. Call only once.
//!  2) cycSimSetCycleInfo()     - Add cycle information. Adds one user per call.
//!  3) cycSimSetEventInfo()     - Add event information. Adds one event per call. Cannot be used during simulation.
//!  4) cycSimSetCycleComplete() - Called when all desired information has been entered. Starts simulation mode.
//!  5) cycSimProcess()          - Processes events based on current cycle time. Must be called periodically.
//!
//! New cycle information may be entered at any time. To start a new simulation using this information (clearing all previous information),
//! call cycSimSetCycleComplete().
//! If no new information was entered upon calling cycSimSetCycleComplete(), the simulation will be disabled at the end of the current cycle.
//! When disabled, event information may be cleared by calling cycSimResetEventInfo(). New events can also be added.

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>

#include <fgc_event.h>
#include <time_fgc.h>

#include <defconst.h>
#include <deftypes.h>



// ---------- Constants

#define FGC_CYC_SIM_MAX_EVENTS 30



// ---------- External structures, unions and enumerations

enum CycSim_payload_type
{
    NONE,
    GROUP,
    USER
};


struct CycSim_prop
{
    uint16_t  cycles[FGC_SIMSC_LEN];
};



// --------------- External variable declarations ------------------------------

extern struct CycSim_prop cycsim_prop;



// ---------- Platform/class specific function definitions

//! Initialises the super-cycle simulation.

void cycSimClassInit(void);



// --------------- External function declarations ------------------------------

//! Initializes the module

void cycSimInit(void);


//! Configures the super-cycle.
//!
//! @param[in]  bpLenInfo       Length of one basic period.
//! @param[in]  maxNumCycles    Maximum number of cycles in a supercycle.
//! @param[in]  maxNumEvents    Maximum number of events in a cycle.
//! @param[in]  numReps         Number of times the same event is written to the output array.
//! @param[in]  repeatPeriod    Time between each repeated write.
//!
//! @retval     false           Failed do allocate memory
//! @retval     true            Success

void cycSimConfig(uint16_t bpLenInfo, uint8_t maxNumCycles, uint8_t maxNumEvents, uint8_t numReps, uint8_t repeatPeriod);


//! Adds information for one event.
//!  Invalid events are silently ignored as the false condition is not being checked in InitClassCycSim(), where this function
//!  is being called.
//!
//! @param[in]  type            Type of event.
//! @param[in]  trigger_type    Trigger type
//! @param[in]  payloadType     Payload type.
//! @param[in]  delay_ms        Event delay (when it should be written to the output array. E.g. 600 ms before eventTime).
//! @param[in]  eventTime       Time when the event must be triggered (enter a negative value if this time is in respect to the end
//!                              of the cycle).
//!
//! @retval     false           Simulation is enabled or the event time output not on repeat period boundary.
//! @retval     true            Event added successfully.

bool cycSimSetEventInfo(enum fgc_events          type,
                        uint8_t                  trigger_type,
	                    enum CycSim_payload_type payloadType,
	                    uint16_t                 delay_ms,
	                    int16_t                  eventTime);


//! Clears event information. Only works if the simulation is not running.
//!
//! @retval     false           Failed to clear events because the simulation is running.
//! @retval     true            All events cleared.

bool cycSimResetEventInfo(void);


//! Adds information for one cycle.
//!
//! @param[in]  sub_sel User of the cycle.
//! @param[in]  cyc_sel User of the cycle.
//! @param[in]  length  Number of basic periods this cycle should last.
//!
//! @retval     false   Failed to add cycle.
//! @retval     true    Cycle added successfully.

bool cycSimSetCycleInfo(uint8_t sub_sel, uint8_t cyc_sel, uint8_t length);


//! Enables the simulation once all cycle and event information is entered.
//!  If the simulation was already enabled, it will signal an update for the cycle information.
//!  If called without any new information being previously added, the current simulation will be disabled.

void cycSimSetCycleComplete(void);


//! @retval true    The simulation is enbled.
//! @retval false   The simlution is disabled.

bool cycSimIsEnabled(void);


//! Processes event information to be written at the time the function is called.
//!  Returns the elapsed time since the start of the current cycle, in milliseconds.
//!
//! @param[in]      utcTime     Current UTC time.
//! @param[in,out]  simEvents   Output array where event information is written.

void cycSimProcess(struct CC_ms_time const * utcTime, struct fgc_event * simEvents);


//EOF
