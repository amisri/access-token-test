//! @file  dsp.h
//! @brief DSP boot loader.

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>



// ---------- External function declarations

//! DSP boot loader error values

enum Dsp_error
{
    DSP_ERROR_OK,
    DSP_ERROR_HANDSHAKE_TIMEOUT,
    DSP_ERROR_IN_STANDALONE,
    DSP_ERROR_LAST
};



// ---------- External function declarations

//! Transfers the DSP image to the DSP
//!
//! @param  dsp_binary_image  The DSP binary image
//! @param
//! @retval DSP_ERROR_OK      The image was successfully transfered.
//! @retval !DSP_ERROR_OK     An error occurred when transferring the image.

enum Dsp_error dspLoadImage(uint8_t const * dsp_image, uint32_t const dsp_image_length);


//! Returns a human readable string with the DSP image load error

char const * dspLoadErrorStr(enum Dsp_error error);


//! This function will signal the DSP to run

void dspStart(void);


// EOF
