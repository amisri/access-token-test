//! @file  function.h
//! @brief This file manages the arming of functions

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- Constants

//! Non PPM value

#define NON_PPM_USER            (0)

//! Non MULTI_PPM value

#define NON_MULTI_PPM_SUB_DEV   (0)



// ---------- Platform/class specific function declarations

//! This function returns the string to prefix to enter a refernce in the terminal
//!
//! @return                      The prefix to add in the terminal.

char const * functionGetDefaultPrefix(void);


// EOF
