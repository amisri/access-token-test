//! @file  logSpy.h
//! @brief Functionality related to logging
//!
//! Includes handling of I earth, LOG.TIMING and managing the interface to liblog.

#pragma once


// ---------- Includes

#include <os.h>

#include <cmd.h>
#include <definfo.h>
#include <liblog/logClient.h>
#include <logMenus.h>
#include <logMenusInit.h>
#include <time_fgc.h>



// ---------- External structures, unions and enumerations

//! Structure with variables mapped to properties

struct Log_props
{
    struct CC_us_time     post_mortem_time;                    //!< LOG.MENU.PM_TIME
    struct CC_us_time     slow_abort_time;                     //!< LOG.MENU.SA_TIME
    uint32_t              log_events;                          //!< LOG.LOG_EVENTS
};



// ---------- External variable declarations

extern struct LOG_menus         log_menus;
extern struct Log_props         log_props;
extern OS_SEM                 * log_get_sem;



// ---------- Platform/class specific function declarations


//! Class specific initialization

void logSpyClassInit(void);


//! Enable/disable logs and log menus

void logSpyClassEnableDisableLogs(void);


//! Process Alias logs

uint16_t logSpyClassProcessAlias(uint32_t menu_idx);


//! Returns true if the menu_idx is the active index; false otherwise.

bool logSpyClassIsActiveAlias(uint32_t menu_idx);


//! Gives an opportunity to change some signal names on-the-fly.

void logSpyClassInterceptLogNames(union LOG_header * const log_header, enum LOG_menu_index const menu_index);



// ---------- External function declarations


//! This function initializes the logs

void logSpyInit(void);


//! Background task.

void logSpyBgp(void);


//! Sets the enable condition for the FGC logger

void logSpyFgcLoggerSetEnable(bool enabled);


//! Retrieves all possible signals for each log buffer

uint16_t logSpyGetSignals(struct cmd * c);


//! Retrieves all selected signals for each log buffer

uint16_t logSpyGetSelectedSignals(struct cmd * c);


//! Retrieves SPY signals

uint16_t logSpyGetSpy(struct cmd * c);


//! Retrieves the signals for LOG.PM.BUF

uint16_t logSpyGetPmBuf(struct cmd * c);


//! Retrieves the log buffers

uint16_t logSpyGetBuffer(struct cmd              * c,
                         uint32_t                  log_sig_index,
                         uint32_t                  sub_sampling_period,
                         struct CC_us_time const * log_i_time_origin,
                         uint32_t          const   log_i_duration_ms);


//! Transfer analogue DIM values to the DSP for logging

void logSpyDimAnaProcess(void);


// EOF
