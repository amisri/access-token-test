//! @file  hash.h
//! @brief Header file for hash.c - Hashing functions

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External structures, unions and enumerations


typedef uintptr_t HASH_VALUE;       //!< needed by defprops.h

struct hash_entry
{
    uint16_t   const sym_idx;    //!< Symbol index
    HASH_VALUE const value;      //!< Pointer to top level property OR getopt mask

    union hash_key
    {
        char * c;                   //!< Symbol string (nul padded to 14 characters)
    } key;

    struct hash_entry * next;       //!< Link to next hash entry
};



// ---------- External function declarations

//! This function will initialise the hash tables for the three types of FGC symbols: property names,
//! constants and get options.  The symbols are defined in defprops.h - the first symbol in each table is
//! not used so that a zero symbol index can indicate no symbol match.  The length of the tables is defined
//! by the parser to be the next prime number up from the number of symbols in the table.

void hashInit(void);


//! This function inserts a DIM in the properties hash table

uint8_t hashInsertDIM(char const * key, uint16_t n_dims);


//! This function will search the property hash table for the property symbol

uint16_t hashFindProp(char const * prop_symbol);


//! This function will search the constant hash table for the constant symbol

uint16_t hashFindConst(char const * const_symbol);


//! This function will search the get options hash table for the get option symbol

uint16_t hashFindGetopt(char const * getopt_symbol);


// EOF
