//! @file   statePc.h
//! @brief  Common code for the Power Converter state

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <defconst.h>



// ---------- Constants

#define STATE_PC_PERIOD_MS             5       //!< The state machine is evaluated every 5 ms
#define STATE_PC_SIMPLIFIED_NONE       (0xFF)
#define STATE_PC_VS_POWER_ON           (DIG_IP_VSRUN_MASK32 | DIG_IP_VSPOWERON_MASK32 | DIG_IP_VSREADY_MASK32)
#define STATE_PC_REF_MAX_TRANSITIONS   3       //!< Maximum transitions from any state



// ---------- External structures, unions and enumerations

struct StatePc
{
    uint16_t pc_simplified;             //!< Property STATE.PC_SIMPLIFIED property
};


//! Type for state machine state function returning state number

typedef uint32_t StatePC(bool first_call);

//! Type for pointer to transition function returning pointer to state function

typedef StatePC * StatePCtrans(void);



// ---------- External variable declarations

extern struct StatePc state_pc;



// ---------- Platform/class specific function declarations

//! Class specific processing of the state machine

void statePcClassProcess(void);

// ------------------------------- Generic STATE FUNCTIONS --------------------------------

uint32_t statePcFO(bool firstCall);

uint32_t statePcOF(bool firstCall);

uint32_t statePcFS(bool firstCall);

uint32_t statePcSP(bool firstCall);

uint32_t statePcST(bool firstCall);

uint32_t statePcSA(bool firstCall);

uint32_t statePcTS(bool firstCall);

uint32_t statePcSB(bool firstCall);

uint32_t statePcIL(bool firstCall);

uint32_t statePcTC(bool firstCall);

uint32_t statePcAR(bool firstCall);

uint32_t statePcRN(bool firstCall);

uint32_t statePcAB(bool firstCall);

uint32_t statePcCY(bool firstCall);

uint32_t statePcPL(bool firstCall);

uint32_t statePcBK(bool firstCall);

uint32_t statePcEC(bool firstCall);

uint32_t statePcDT(bool firstCall);

uint32_t statePcPA(bool firstCall);


// ------------------------------- Generic TRANSITION FUNCTIONS --------------------------------

StatePC * XXtoFS(void);

StatePC * FStoFO(void);

StatePC * FStoSP(void);

StatePC * SPtoOF(void);

StatePC * FOtoOF(void);

StatePC * OFtoFO(void);

StatePC * OFtoST(void);

StatePC * STtoSP(void);

StatePC * STtoBK(void);

StatePC * BKtoSP(void);



// ---------- External function declarations

//! Initialize module

void statePcInit(void);


//! Update the Power Converter State Machine.
//!
//! Transition from current PC state to requested PC state.

void statePcProcess(void);


//! Returns the current state function
//!
//! @returns The current state function

StatePC * statePcGetStateFunc(void);


//! Returns the current state duration in milliseconds
//!
//! @returns State duration in milliseconds

uint32_t statePcGetStateTime(void);


//! Get the simplified state PC.
//!
//! @return                  Internal state PC

uint16_t statePcGetSimplified(void);


//! Returns true if the current state is in stateBitmask
//!
//! @param  stateGroup State bitmask to compare the current state against
//!
//! @retval true       If the current state is in stateBitmask
//! @retval false      If the current state is not in stateBitmask

bool statePcTestStateBitmask(uint32_t stateBitmask);


//! Returns a two character array with the reduced state name
//!
//! @param   state     The state to convert into string
//! @returns           The reduced state name

char const * statePcGetStateName(uint8_t state);


//! This function will:
//!  - Clear the latched faults in STATUS.ST_LATCHED
//!  - Send the reset signal to the VS to clear the external faults

void statePcResetFaults(void);


// EOF
