//! @file  databases.h
//! @brief Interface to the databases encapsulated in codes


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>



// ---------- External function declarations

//! Initialises some of the DB-related structures

void databasesInit(void);


//! Compares DB versions.
//!
//!  @retval true    Versions match.
//!  @retval false   Versions do not match.

bool databasesOk(void);


//! Get the device's class ID from NameDB.
//!
//!  @param[in]  index   Device index in NameDB.
//!
//!  @return             The device class ID.

uint16_t nameDbGetClassId(uint16_t index);


//! Get the device's omode mask from NameDB.
//!
//!  @param[in]  index   Device index in NameDB.
//!
//!  @return             The device omode mask.

uint16_t nameDbGetOmodeMask(uint16_t index);


//! Returns device name under specified index in NameDB
//!
//! @param[in]  index  Device name index
//!
//! @return  Device name or NULL if index is out of bounds

char const * nameDbGetName(uint16_t index);


//! Returns index of the device name in NameDB
//!
//! @param[in]  dev_name  Device name
//!
//! @return  Index of the device in NameDB or -1 if the name isn't
//!          in the database

int16_t nameDbGetIndex(char const * const dev_name);


// EOF