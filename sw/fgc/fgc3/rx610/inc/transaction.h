//! @file  transaction.h
//! @brief Provides the framework for transactional settings
//!
//!  The transactions specification is documented in the link below:
//!  https://wikis.cern.ch/display/COTEC/Transactions+support+in+the+Control+System

#pragma once



// ---------- Includes

#include <stdint.h>

#include <defconst.h>
#include <property.h>



// ---------- External structures, unions and enumerations

//! This structure contains the variables associated to transactional settings

struct Transaction_prop
{
    uint16_t   id   [FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];   //!< TRANSACTION.ID
    uint16_t   state[FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];   //!< TRANSACTION.STATE
};

typedef uint16_t (*transactionFuncTest)    (uint32_t const sub_sel, uint32_t const cyc_sel);
typedef uint16_t (*transactionFuncCommit)  (uint32_t const sub_sel, uint32_t const cyc_sel, struct CC_us_time const * );
typedef uint16_t (*transactionFuncRollback)(uint32_t const sub_sel, uint32_t const cyc_sel);
typedef void     (*transactionSetPropFunc) (uint32_t const sub_sel, uint32_t const cyc_sel, struct prop * property);



// ---------- External variable declarations

extern struct Transaction_prop transaction_prop;



// ---------- External function declarations

//! Initializes the transaction module
//!
//! @param  test                         Function to test the reference
//! @param  commit                       Function to commit the reference
//! @param  rollback                     Function to rollback the reference
//! @param  property                     Function to set a transactional property

void transactionInit(transactionFuncTest     test,
                     transactionFuncCommit   commit,
                     transactionFuncRollback rollback,
                     transactionSetPropFunc  property);


//! Initiates a transaction
//!
//! @param  sub_sel                      Sub-device selector
//! @param  cyc_sel                      Cycle selector
//! @param  transaction_id               New transaction ID
//!
//! @return FGC_OK_NO_RSP                The Transaction ID has been set
//! @return FGC_TRANSACTION_IN_PROGRESS  A transaction is already in progress

uint16_t transactionSetId(uint32_t sub_sel, uint32_t cyc_sel, uint16_t transaction_id);


//! Returns the transaction ID
//!
//! @param  sub_sel                      Sub-device selector
//! @param  cyc_sel                      Cycle selector
//!
//! @retval                              Transaction ID

uint16_t transactionGetId(uint32_t sub_sel, uint32_t cyc_sel);


//! Validates the transaction ID
//!
//! @param  transaction_id               Transaction ID to validate
//!
//! @return FGC_OK_NO_RSP                Valid transaction ID
//! @return FGC_TRANSACTION_ID_UNKNOWN   Invalid transaction ID

uint16_t transactionValidateId(uint32_t transaction_id);



//! Check property access rules for transactions
//!
//! @param  cmd_type                     Command type
//! @param  cmd_transaction_id           Transaction ID passed in the command
//! @param  sub_sel                      Sub-device selector
//! @param  cyc_sel                      Cycle selector
//! @param  property                     Property
//!
//! @return FGC_OK_NO_RSP                Valid transaction ID
//! @return FGC_TRANSACTION_IN_PROGRESS  Transaction in progress
//! @return FGC_INVALID_TRANSACTION_ID   Invalid transaction ID
//! @return FGC_PROPERTY_NOT_TRANSACTIONAL Non-transactional property

uint32_t transactionAccessCheck(uint16_t      cmd_type,
                                uint16_t      cmd_transaction_id,
                                uint32_t      sub_sel,
                                uint32_t      cyc_sel,
                                struct prop * property);


//! Tests transactions for all users that have a transaction ID that
//! matches the ID provided in the value.
//!
//! @param  sub_sel                      Sub-device selector
//! @param  transaction_id               Transaction ID to test
//!
//! @return FGC_OK_NO_RSP                The reference is correct
//! @return FGC_TRANSACTION_ID_UNKNOWN   The transaction ID is unknown
//! @return error                        Error when arming the reference

uint16_t transactionTest(uint32_t sub_sel, uint16_t transaction_id);


//! Commits transactions for all users that have a transaction ID that
//! matches the ID provided in the value.
//!
//! @param  sub_sel                      Sub-device selector
//! @param  transaction_id               Transaction ID to commit
//! @param  commit_time                  Commit time
//!
//! @return FGC_OK_NO_RSP                The reference is correct
//! @return FGC_TRANSACTION_ID_UNKNOWN   Transaction ID not found
//! @return error                        Error when arming the reference

uint16_t transactionCommit(uint32_t                  sub_sel,
                           uint16_t                  transaction_id,
                           struct CC_us_time const * commit_time);


//! Rolls-back transactions for all users that have a transaction ID that
//! matches the ID provided in the value.
//!
//! @param  sub_sel                      Sub-device selector
//! @param  transaction_id               Transaction ID to rollback
//!
//! @return FGC_OK_NO_RSP                Rollback successful
//! @return FGC_TRANSACTION_ID_UNKNOWN   Transaction ID not found

uint16_t transactionRollback(uint32_t sub_sel, uint16_t transaction_id);


//! Acknowledges failed commits by transitioning the state from COMMIT_FAILED to NONE.
//!
//! @param  sub_sel                      Sub-device selector
//! @param  cyc_sel                      Cycle selector

void transactionAckCommitFailed(uint32_t sub_sel, uint32_t cyc_sel);


// EOF
