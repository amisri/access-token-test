//! @file  logTime.h
//! @brief Interface to the library libtimelog

#pragma once


// ---------- Includes

#include <stdint.h>

#include <libtimelog.h>



// ---------- External function declarations

//! Initialize time time logs

void logTimeInit(void);


//! Store a time log
//!
//! @param[in]    delay_us       Event delay in microseconds
//! @param[in]    sub_sel        Sub-device selector for the timing event
//! @param[in]    cyc_sel        Cycle selector for the timing event
//! @param[in]    type           Event type

void logTimeStore(uint32_t                  delay_us,
                  uint8_t                   sub_sel,
                  uint8_t                   cyc_sel,
                  uint8_t                   type);


//!< Get one index from the time log.
//!
//! @param[in]      log_index     Log index to copy into read_buf
//! @param[in]      read_buf      Destination buffer.

void logTimeGet(uint32_t const log_index, char read_buf[TIMELOG_READ_BUF_LEN]);


// EOF
