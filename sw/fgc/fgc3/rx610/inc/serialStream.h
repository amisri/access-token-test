//! @file  serialStream.h
//! @brief Class specific logging

#pragma once


// ---------- Includes

#include <stdio.h>



// ---------- External function declarations

//! This function is the callback for the SCI output streams (TrmTsk() commands or rtdTsk()).  It supports
//! the following special codes:
//!  \v  Flush buffer immediately
//!  \n  Insert one newline even if multiple \n are seen and if editor is enabled
//!      then follow by \r to produce a carriage return on the ANSI/VT100 screen.

void serialStreamInternalWriteChar(char ch, FILE * f);


//! This function is called to flush the stream buffer so that it is queued for transmission to the SCI by
//! the TrmMsOut() function.  This function can be called with a NULL FILE pointer.
//! Note that terminal_state.bufq can
//! never fill up because it is one element longer than the number of buffers it needs to address.

void serialStreamInternalFlushBuffer(FILE * f);


// EOF
