//! @file  diag.h
//! @brief FGC MCU Software - QSPI related functions
//! The DIPS board has an embedded DIM that is the first on both branch A and branch B,
//! so it is scanned at 100Hz.
//! The analogue channels are different for the two scans (there are 8 channels in all)
//! and are used to measure the voltage source output voltage (Vmeas) twice (x1 and x10)
//! and the +5V and +/-15V PSU voltages for the FGC and the FGC's analogue interface.
//! These are checked by the DSP against threshold values.
//! If the diagnostic buses are reset, these channels get out of sync for 2 20ms cycles,
//! so the DSP must suppress PSU monitoring for 2 cycles.
//! This is signalled by the MCU using dpcom.mcu.diag.reset.
//! This should be set to the number of cycles that need to be blocked (2).
//! It is decremented by the DSP every 20ms cycle.


#pragma once


// ---------- Includes

#include <defconst.h>
#include <fgc_consts_gen.h>
#include <fgc_stat.h>
#include <property.h>
#include <dimDataProcess.h>



// ---------- Constants

#define DIAG_EXPEC_LATCHED_FAULT        10              // Threshold to set a DIM related latched fault.
#define DIAG_MAX_ANALOG                 128             // Max number of analogue channels
#define DIAG_MAX_DIGITAL                64              // Max number of digital channels
#define DIAG_MAX_SOFTWARE               64              // Max number of software channels
#define DIAG_UL_DIG_DATA_0              0x080           // Bank 0 unlatched digital data channels
#define DIAG_UL_DIG_DATA_1              0x0A0           // Bank 1 unlatched digital data channels
#define DIAG_TASK_TIMES                 0x0C0           // Channels for task timing
#define DIAG_ISR_TIMES                  0x0D0           // Channels for ISR timing
#define DIAG_MST_TIMES                  0x0E0           // Channels for msTask timing
#define DIAG_LT_DIG_DATA_0              0x120           // Bank 0 latched digital data channels
#define DIAG_LT_DIG_DATA_1              0x140           // Bank 1 latched digital data channels
#define DIAG_ANALOG_CAL                 1.220703        // Millivolts/raw for analogue channels
#define DIAG_TRIGGER                    0x8000          // Trigger scan bit in QSM_SPCR1
#define DIAG_DCCT_STAT_CHAN             0x80            // DCCT status channel
#define DIAG_PSU_STAT_CHAN              0xA0            // PSU status channel
#define DIAG_UNLATCHED_TRIGS            0xDC            // Unlatched trigger channels
#define DIAG_LATCHED_TRIGS              0xDE            // Latched trigger channels
#define DIAG_TERM_LEN                   sizeof(struct diag_term)

//! There are up to four analogue channels measuring I_EARTH
#define LOG_I_EARTH_SIGS       4

// for software register array index
#define TASK_RUN_TIME   0
#define ISR_RUN_TIME    1

#define QSPI_BRANCH_A_UNLATCHED_TRIGS     12 // branch A dig0 registers b15 collection
#define QSPI_BRANCH_B_UNLATCHED_TRIGS     13 // branch B dig0 registers b15 collection
#define QSPI_BRANCH_A_LATCHED_TRIGS       14 // branch A triggerCounter registers b15 collection
#define QSPI_BRANCH_B_LATCHED_TRIGS       15 // branch B triggerCounter registers b15 collection



// ---------- External structures, unions and enumerations

struct TAccessToDimInfo                                    // DIM link structures (logical_dim order)
{
    uint8_t                           logical_dim;    // DIM index (0-19)
    uint8_t                           flat_qspi_board_number; // 0..31
};

struct diag_term                                        // Double diag buffer for terminal diag mode
{
    uint8_t               sync[FGC_DIAG_N_SYNC_BYTES];    // Sync bytes for terminal diag client
    struct fgc_diag     data;                           // Diag data structure
};


// This can't be modified without adjusting at labels DIAG_FLAT

struct TDimDataFromHwAndMore_1
{
    uint16_t  analog_0[QSPI_BRANCHES][16];        // Ana0  DIM register ID:4  x16 DIM boards per branch A or B
    uint16_t  analog_1[QSPI_BRANCHES][16];        // Ana1  DIM register ID:5  x16 DIM boards per branch A or B
    uint16_t  analog_2[QSPI_BRANCHES][16];        // Ana2  DIM register ID:6  x16 DIM boards per branch A or B
    uint16_t  analog_3[QSPI_BRANCHES][16];        // Ana3  DIM register ID:7  x16 DIM boards per branch A or B
    uint16_t  digital_0[QSPI_BRANCHES][16];       // Dig0  DIM register ID:0  x16 DIM boards per branch A or B
    uint16_t  digital_1[QSPI_BRANCHES][16];       // Dig1  DIM register ID:1  x16 DIM boards per branch A or B
    uint16_t  software[2][16];                    // [0=task_run_time][0..9 tasks] , [10..15]?
    // [1=isr_run_time] [0..9 tasks] , [10,11]? [12=QSPI_BRANCH_A_UNLATCHED_TRIGS][13=QSPI_BRANCH_B_UNLATCHED_TRIGS][14=QSPI_BRANCH_A_LATCHED_TRIGS][15=QSPI_BRANCH_B_LATCHED_TRIGS]
    uint16_t  ms_duration[32];                    // Maximum duration of the mstTask for each millisecond [20..31] not used.
    uint16_t  trigCounter[QSPI_BRANCHES][16];     // TrigCounter DIM register ID:2  x16 DIM boards per branch A or B
    uint16_t  latched_0[QSPI_BRANCHES][16];       // Latched digital
    uint16_t  latched_1[QSPI_BRANCHES][16];       // Latched digital
};

struct TDimDataFromHwAndMore_2
{
    uint16_t              analog_0[FGC_MAX_DIM_BUS_ADDR];
    uint16_t              analog_1[FGC_MAX_DIM_BUS_ADDR];
    uint16_t              analog_2[FGC_MAX_DIM_BUS_ADDR];
    uint16_t              analog_3[FGC_MAX_DIM_BUS_ADDR];
    uint16_t              digital_0[FGC_MAX_DIM_BUS_ADDR];
    uint16_t              digital_1[FGC_MAX_DIM_BUS_ADDR];
    uint16_t              software[2][16];
    uint16_t              ms_duration[32];
    uint16_t              trigCounter[FGC_MAX_DIM_BUS_ADDR];
    uint16_t              latched_0[FGC_MAX_DIM_BUS_ADDR];
    uint16_t              latched_1[FGC_MAX_DIM_BUS_ADDR];
};

struct TDimDataFromHwAndMore_3
{
    uint16_t              analog[4][FGC_MAX_DIM_BUS_ADDR];    // Ana0, Ana1, Ana2, Ana3
    uint16_t              digital[FGC_N_DIM_DIG_BANKS][FGC_MAX_DIM_BUS_ADDR];   // Dig0, Dig1
    uint16_t              software[2][16];
    uint16_t              ms_duration[32];
    uint16_t              trigCounter[FGC_MAX_DIM_BUS_ADDR];
    uint16_t              latched[FGC_N_DIM_DIG_BANKS][FGC_MAX_DIM_BUS_ADDR];   // latched 0, 1
};

struct TDimDataFromHwAndMore_4
{
    uint16_t              analog[DIAG_MAX_ANALOG];          // [0x000-0x07F]  128 Analogue registers
    uint16_t              digital[DIAG_MAX_DIGITAL];        // [0x080-0x0BF]   64 Unlatched digital registers
    uint16_t              software[DIAG_MAX_SOFTWARE];      // [0x0C0-0x0FF]   64 Software (non-DIM) registers
    uint16_t              trigCounter[FGC_MAX_DIM_BUS_ADDR];// [0x100-0x11F]   32 DIM trigger timers
    uint16_t              latched[DIAG_MAX_DIGITAL];        // [0x120-0x15F]   64 Latched digital registers
};

union TDimDataFromHwAndMore
{
    uint16_t w[DIAG_MAX_ANALOG + DIAG_MAX_DIGITAL + DIAG_MAX_SOFTWARE + FGC_MAX_DIM_BUS_ADDR + DIAG_MAX_DIGITAL];
    struct TDimDataFromHwAndMore_1      r;      // registers [A,B][0..15]
    struct TDimDataFromHwAndMore_2      f;      // flat registers [0..31]
    struct TDimDataFromHwAndMore_3      a;      // for analogue process flat registers [0..31]
    struct TDimDataFromHwAndMore_4      c;      // groups
} /* __attribute__ ((__transparent_union__)) */ ;


// ToDo: decouple branches A and B to handle the branches independently
// and not to stop branch A if B is faulty or viceversa

struct diag_vars
{
    union TDimDataFromHwAndMore data;
    uint16_t              req_QSPI_reset;                 // reset request down counter
    uint32_t              sync_resets;                    // DIAG.SYNC_RESETS
    uint16_t              fault_resets_counter;           // Reset counter to detect DIAG_FLT
    uint16_t              qspi_branch;                    // branch being accessed 0=A, 1=B
    uint16_t       *      copy_to;                        // Pointer into destination buffer for data
    uint16_t              triggers_from_ms[20];           // Trigger bits collected in each millisecond
    struct diag_term    term_buf[2];                    // Double diag buffer for terminal diag mode
    uint16_t              term_byte_idx;                   // Data byte index (0 to sizeof(struct diag_buf))
    uint16_t              term_buf_idx;                    // Double buffer index (0 or 1)
    uint8_t       *       term_buf_p;                      // Pointer to next byte to send
    uint8_t       *       term_buf_chan;                   // Pointer to next chan address
    uint16_t       *      term_buf_data;                   // Pointer to next data address
    uint16_t              term_buf_counter;                // Counter of number of samples entered in the buffer
    prop_size_t list_rtd_len;                   // RTD diagnostic channel list length, and size of prop DIAG.RTD
    prop_size_t list_len[FGC_DIAG_N_LISTS];     // Diag list property lengths (DIAG.LIST0/LIST1/LIST2/LIST3)
    uint8_t               list_offset[FGC_DIAG_N_LISTS];  // Diag list channel offsets
    uint8_t               list_rtd[FGC_DIAG_RTD_LIST_LEN];        // RTD diagnostic channel list
    uint8_t               list[FGC_DIAG_N_LISTS][FGC_DIAG_LIST_LEN];      // Diagnostic channel lists
    uint16_t              n_dims;                         // Number DIMs
    const char  *       menu_names[FGC_MAX_LOG_MENUS];  // LOG.MENU.NAMES[] pointers to name strings
    const char  *       menu_props[FGC_MAX_LOG_MENUS];  // LOG.MENU.PROPS[] pointers to name strings
    struct prop         dim_props[FGC_MAX_DIMS];        // prop_idx order for [logical dim]
    struct TAccessToDimInfo    logical_dim_info[FGC_MAX_DIMS];      // This array may have holes
    uint16_t            warning_enabled[FGC_MAX_DIMS][FGC_N_DIM_DIG_BANKS];
    struct CC_us_time   dim_time;                         //!< Start of DIM cycle
};


//! Structure containig I earth data

struct meas_i_earth
{
    float                 value;                                      //!< MEAS.I_EARTH [20ms]  Measured earth current
    float                 simulated;                                  //!< Simulated earth current
    int32_t               cpcnt;                                      //!<  [20ms] Measured earth current (% of trip limit)
    float                 max;                                        //!< MEAS.MAX.I_EARTH
    float                 limit;                                      //!< LIMITS.I.EARTH
    int32_t               channel[LOG_I_EARTH_SIGS];                  //!< DIM channels containing the I_EARTH_##MS values.
};



// ---------- External variable declarations

extern struct diag_vars    diag;
extern struct meas_i_earth meas_i_earth;



// ---------- Platform/class specific function declarations

//! This function initialises class specific aspects of the diagnostic system

void diagClassInit(void);

//! This function checks the converter

void diagClassCheckConverter(void);

//! Updates measurements mapped to DIAG analogue signals

void diagClassUpdateMeas(void);



// ---------- External function declarations

//! Updates measurements mapped to DIAG analogue signals

void diagUpdateMeas(void);

//! This function add the DIM to the properties HASH structure for posterios usage

void DiagDimHashInit(void);


//! This function will prepare the DIM database for use with the DIAG properties

void DiagDimDbInit(void);

//! This function will set the length of the parent properties which use the dynamic list of children that
//! corresponds to the list of DIMS for the device type.

void DiagSetPropNumEls(uint16_t n_dims);


//! This function will prepare the DIAG.GAIN and DIAG.OFFSET properties in the DSP (FGC2) or MCU (FGC3) memory.
//! We prepare the property from dimdb into a buffer before calling PropSet()
//! We use the terminal_state struct as a temporary buffer, this function being called before the terminal
//! can be started.

void DiagSetOffsetGain(struct prop * p, uint16_t prop_idx);


//! This function will search for an analogue channel with the name given in chan_name.
//! It will stop at the first match and will return the channel index based on the ANASIGS array.
//! If no match is found it will return -1.

int32_t DiagFindAnaChan(const char * chan_name);


//! This function will search for a digital channel with the name given in chan_name in the DIM specified
//! by logical_dim_info_ptr.  It will stop at the first match and will return TRUE and the channel index in the diag.data[]
//! array and the bit mask for the channel.  If no match is found it will return FALSE.

bool DiagFindDigChan(uint16_t logical_dim, const char * chan_name, uint16_t * data_idx, uint16_t * diag_mask);


void DiagCheckConverter(void);
void DiagSD360(void);

void diagCheckDimWarnings(void);

// EOF
