//! @file  dimDataProcess.h
//! @brief

#pragma once


// ---------- Includes

#include <cmd.h>
#include <defconst.h>
#include <dpcom.h>
#include <property.h>



// ---------- Constants

#define DEBUG_LOG_DIPS  1                       //! 0=Log first DIM on branch A, 1=Log DIPS board
#define DIPS_MASK       0x00010001              //! DIPS boards mask (0 and 16)

//! array index for flat_qspi_board_is_present_packed[]
#define DIMS_IN_BOTH_CYCLES     0
#define DIMS_IN_ACTUAL_CYCLE    1
#define DIMS_IN_PREVIOUS_CYCLE  2
#define REQUESTS_FOR_DIM_LOG    3

#define DIM_EVT_NONE            0                            //!< DIM event log state: nothing to do
#define DIM_EVT_TRIG            1                            //!< DIM event log state: log all
#define DIM_EVT_TRIGGED         2                            //!< DIM event log state: nothing to do (! Set by the MCU)
#define DIM_EVT_WATCH           3                            //!< DIM event log state: watch for new faults
#define DIAG_DEBUG_TO_ACQ       64                           //!< Post trig debug samples (64 = 8 * 8 words)

#define QSPI_BRANCH_A           0
#define QSPI_BRANCH_B           1
#define QSPI_BRANCHES           2



// ---------- External structures, unions and enumerations

//! For list (0..19) process

struct TDimCollectionInfo
{
    //! DIAG.BUS_ADDRESSES
    //!
    //! here the flat qspi bus board number can be
    //! 0x00..0x1F are real physical DIM boards (0x00..0x0F on branch A, 0x10..0x1F on branch B)
    //! 0x80   b7=1 means a virtual DIM (composite)
    //! 0xFF   means not used (empty)

    uint32_t  flat_qspi_board_number[FGC_MAX_DIMS];

    //! Analogue register converted to physical values DIAG.ANASIGS

    float     analogue_in_physical[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];

    bool      unlatched_trigger_fired;
    bool      dims_are_valid;

    uint32_t  trig_us[FGC_MAX_DIMS];                          //!< DIAG.TRIGUS
    uint32_t  trig_s[FGC_MAX_DIMS];

    uint32_t  evt_log_state[FGC_MAX_DIMS];                    //!< Event log state for DIMs

    float     gains  [FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];     //!< DIAG.GAIN
    float     offsets[FGC_N_DIM_ANA_CHANS][FGC_MAX_DIMS];     //!< DIAG.OFFSET
};


struct TQspiMiscellaneousInfo
{
    uint32_t  expected[QSPI_BRANCHES];        //!< DIAG.DIMS_EXPECTED
    uint32_t  detected[QSPI_BRANCHES];        //!< DIAG.DIMS_DETECTED
    uint32_t  expected_detected_mismatches;   //!< DIAG.DIMS_EXP_ERRS
    bool      expected_retrieved;             //!< TRUE when the expected DIMs have been retrieved.

    uint32_t  flat_qspi_non_valid_counts[FGC_MAX_DIM_BUS_ADDR];   //!< DIAG.DIM_SYNC_FLTS

    bool      freeze_all_dim_logs;
    bool      relaunch_dim_logging;           //!< restart DIM logging
};

struct TDimLogEntryStatus
{
    uint16_t            idx;                    //!< last sample index
    struct CC_us_time  time;                   //!< Time of last sample in the buffer
    bool                dont_log;               //!< Logging stopping flag
    uint16_t            samples_to_acq;         //!< Samples still to acquire
};



// ---------- External variable declarations

extern struct TQspiMiscellaneousInfo qspi_misc;
extern struct TDimCollectionInfo     dim_collection;
extern struct TDimLogEntryStatus     dbg_dim_log_status[REQUESTS_FOR_DIM_LOG];



// ---------- External function declarations

//!

void InitQspiBus(void);


//! This function will be called from the 1 millisecond task at the start of every millisecond.
//! It must quickly read any data waiting from a previous scan, and trigger the next scan if required.

void QSPIbusStateMachine(void);


//! Displays:     DIM log properties

uint16_t DimGetDimNames(struct cmd * c, struct prop * p);


//! Displays:     DIM log properties

uint16_t DimGetAnaLbls(struct cmd * c, struct prop * p);


//! Displays:     DIM log properties

uint16_t DimGetDigLbls(struct cmd * c, struct prop * p, uint16_t bank);


//! Displays:     DIM analogue channel properties

uint16_t DimGetAna(struct cmd * c, struct prop * p);


//! Displays:     DIM digital channel properties

uint16_t DimGetDig(struct cmd * c, struct prop * p);


//! Displays:     DIM digital channels in Fault

uint16_t DimGetFaults(struct cmd * c, struct prop * p);


//! Resets the expected/detected counter
//!
//! The function is called from the Mst through regfgc3_prog to make sure that the expected vs detected
//! dim counters are zeroed once all boards have been switched to production boot, after the program manager
//! intervention

void DimResetExpectedDetectedMismatches(void);


//! Get the start time stamp of the current DIM scan cycle.
//!
//! This function is expected to be used in callbacks of the QSPI/DIM state machine (e.g. diagClassUpdateMeas)
//!
//! @param[out]     cycle_start_time    Pointer to the structure which will be filled.

void DimGetCycleStartTime(struct CC_ms_time * cycle_start_time);


// EOF
