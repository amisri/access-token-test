//! @file     init.h
//! @brief    Initialization functions

#pragma once


// ---------- External function declarations

//! Calls the init functions of all modules

void initInit(void);


//!	This function initialises the modules needed by the MCU that require several tasks already running.

void initProcess(void);


//! Initialization after properties have bee initialized from the NVS

void initPostNvs(void);


//! Initialization after properties have bee initialized from the DB

void initPostDb(void);



// ---------- Platform/class specific functions

//! Class specific initialization after properties have bee initialized from the NVS

void initClassPostNvs(void);


//! Class specific initialization after properties have bee initialized from the DB

void initClassPostDb(void);


//! Class specific initialization after the RegFgc3 cache has been built

void initClassPostRegFgc3Cache(void);


// EOF
