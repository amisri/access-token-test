//! @file   config.h
//! @brief  Functions relating to configuration properties

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>



// ---------- External structures, unions and enumerations

struct Config
{
    uint16_t mode;          //!< CONFIG.MODE property
    uint16_t state;         //!< CONFIG.STATE property
    bool     no_manager;    //!< Flag indicating if the config manager is available
};




// ---------- External variable declarations

extern struct Config config;



// ---------- External function declarations

//! Returns true if the configuration state is standalone.

bool configIsStandalone(void);

//! Checks if the configuration manager is available and sets the no_manager flag accordingly.


void configSetManagerAvailability(void);

//! Returns true if the configuration manager is not available.
//!
//! @retval  true    Configuration manager is not available
//! @retval  false   Configuration manager is available

bool configManagerNotAvailable(void);


//! Sets the configuration mode.
//!
//! @param[in]   mode    Configuration mode to be set

void configSetMode(uint16_t mode);


//! Returns the current configuration mode
//!
//! @returns     Configuration mode

uint16_t configGetMode(void);


//! Sets the configuration state.
//!
//! @param[in]   mode            Configuration state to be set
//! @param[in]   force_state     Forces the setting of the configuration state

void configSetState(uint16_t state, bool force_state);


//! Returns the current configuration state
//!
//! @returns     Configuration state

uint16_t configGetState(void);


// EOF
