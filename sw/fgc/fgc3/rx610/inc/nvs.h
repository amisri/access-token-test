//! @file  nvs.h
//! @brief Non-Volatile Storage Management
//!
//! This file contains the functions which support the non-volatile storage and recovery
//! of Property data to/from the NVRAM memory.
//!
//! NVRAM is accessible by WORD only - BYTE access will result in a bus error!
//!
//! Some non-volatile properties are part of the configuration, meaning they are stored
//! in the central property database and will be set by the FGC configuration manager.

#pragma once


// ---------- Includes

#include <stdint.h>
#include <property.h>
#include <cmd.h>
#include <defconst.h>
#include <memmap_mcu.h>



// ---------- Constants

#define NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION  1399280666

// ToDo: use NVS_OLDEST_COMPATIBLE_MAINPROG_VERSION as MVS_MAGIC instead of this constant one

//! Used to store/recall values for all cycle selectors.

#define NVS_ALL_CYC_SELS        0xFF

 //! Used to store/recall values for all sub-device selectors.

#define NVS_ALL_SUB_SELS        0xFF



// ---------- External structures, unions and enumerations


//! Structure of the NVS record

struct nvs_record
{
    uint32_t       prop;                                 //!< Non-volatile property structure address (cast to struct prop *)
    uint32_t       data;                                 //!< Non-volatile property data address
    uint16_t       bufsize;                              //!< Non-volatile property data size in bytes, for one cyc_sel (if PPM)
    uint8_t        name[NVSIDX_REC_NAME_B];              //!< Non-volatile property name
};


//! Mapped to DEBUG.NVS[4] property

struct nvs_dbg
{
    uint16_t       idx;                                  //!< Record index variable
    uint16_t       n_props;                              //!< Number of NVS properties in NVRAM
    uint16_t       n_unset_config_props;                 //!< Number of config properties unset
    uint16_t       n_corrupted_indexes;                  //!< Number of corrupted index records
    uint32_t       data_addr;                            //!< Next free zone for data
    uint16_t       sym_idxs[FGC_MAX_PROP_DEPTH];         //!< Symbols for nested property
};



// ---------- External variable declarations

extern struct nvs_dbg nvs_dbg;



// ---------- External function declarations

//! This function is responsible for the recovery/formatting of non-volatile
//! properties using data from the NVS area of the NVRAM memory.

void nvsInit(void);


//! This function will recover from NVRAM the data for the specified property
//! and range of cycle selectors.
//!
//! This function requires that an NVS record identified by p->nvs_idx already
//! exists for the specified property.
//!
//! @param       c               Command structure.
//! @param       p               Property to recover.
//! @param       sub_sel         Range of sub-device selectors to store.
//! @param       cyc_sel         Range of cycle selectors to recover.

void nvsRecallValuesForOneProperty(struct cmd         * c,
                                   struct prop        * p,
                                   uint16_t     const   sub_sel,
                                   uint16_t     const   cyc_sel);


//! This function will save in NVRAM the data of the specified property for a
//! given range of cycle selectors.
//!
//! This function requires that an NVS record identified by p->nvs_idx already
//! exists for the specified property.
//!
//! The caller function is responsible for unlocking the NVRAM thanks to a
//! NVRAM_UNLOCK()/NVRAM_LOCK() section.
//!
//! @param       c               Command structure.
//! @param       p               Property to store.
//! @param       sub_sel         Range of sub-device selectors to store.
//! @param       cyc_sel         Range of cycle selectors to store.
//!
//! @retval      FGC_OK_NO_RSP   If no error occurred.
//! @retval      FGC_DSP_NOT_AVL DSP not available
//! @retval      FGC_DSP_PROPERTY_ISSUE  An issue was detected on the DSP property

uint16_t nvsStoreValuesForOneProperty(struct cmd         * c,
                                      struct prop        * p,
                                      uint16_t     const   sub_sel,
                                      uint16_t     const   cyc_sel);


//! This function will save in NVRAM the data of the specified DSP property for a
//! given range of cycle selectors.
//!
//! This function requires a NVS record identified by p->nvs_idx already exists
//! for that property.
//!
//! The caller function is responsible for unlocking the NVRAM thanks to a
//! NVRAM_UNLOCK()/NVRAM_LOCK() section.
//!
//! This property borrows the publication command structure (pcm) and thus might
//! block to acquire the corresponding mutex.
//!
//! @param       c               Command structure.
//! @param       p               Property to store.
//! @param       sub_sel         Range of sub-device selectors to store.
//! @param       cyc_sel         Range of cycle selectors to store.
//!
//! @retval      FGC_OK_NO_RSP   If no error occurred.
//! @retval      FGC_DSP_NOT_AVL DSP not available
//! @retval      FGC_DSP_PROPERTY_ISSUE  An issue was detected on the DSP property

uint16_t nvsStoreValuesForOneDspProperty(struct prop        * p,
                                         uint16_t     const   sub_sel,
                                         uint16_t     const   cyc_sel);


//! This function is called to save a property's data and number of elements in
//! the NVRAM.
//!
//! The data comes from the property block buffer for the command task or directly
//! from a property value. Note that the data size must be no greater than the
//! property block (PROP_BLK_SIZE).
//!
//! @param       c               Command structure.
//! @param       p               Property to store.
//! @param       sub_sel         Range of sub-device selectors to store.
//! @param       cyc_sel         Range of cycle selectors to store.
//! @param       n_elements      Number of elements to store.
//! @param       last_blk_f      True if this is the last block to store.
//! @param       data            Buffer with the block data.

void nvsStoreBlockForOneProperty(struct cmd         * c,
                                 struct prop        * p,
                                 uint8_t            * data,
                                 uint16_t     const   sub_sel,
                                 uint16_t     const   cyc_sel,
                                 prop_size_t  const   n_elements,
                                 bool         const   last_blk_f);


//! Clears the magic number in NVRAM so that complete config will be wiped
//! on next reset.

void nvsClearMagicNumber(void);


//! Reinstates the magic number in NVRAM so the configuration will be preserved.

void nvsSetMagicNumber(void);


//! Unlocks the NVRAM for write operations.

void nvsUnlock(void);


//! Locks the NVRAM for write operations.

void nvsLock(void);


// EOF
