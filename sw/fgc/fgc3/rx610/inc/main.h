//! @file  main.c
//! @brief Contains the main() function

#pragma once


// ---------- External function declarations

//! This function does all the initialisation duties. It then starts the operating
//! system to launch multi-tasking.

int main(void);


// EOF
