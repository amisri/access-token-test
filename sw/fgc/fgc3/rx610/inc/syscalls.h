//! @file  syscalls.h
//! @brief NewLib system calls

#pragma once


// ---------- Includes

#include <stdint.h>

#include <sys/reent.h>
#include <mcu.h>



// ---------- External structures, unions and enumerations

// If this values are changed the linker.txt file needs to be updated

enum
{
    NEWLIB_MALLOC_POOL1 = 0,
    NEWLIB_MALLOC_POOL2,
    NEWLIB_MALLOC_POOL3,
    NEWLIB_MALLOC_POOL4,
    NEWLIB_MALLOC_POOL5,
    NEWLIB_MALLOC_NB_OF_POOLS
};

#define NEWLIB_MALLOC_POOL1_BLOCK_SIZE          32
#define NEWLIB_MALLOC_POOL1_BLOCK_COUNT         25

#define NEWLIB_MALLOC_POOL2_BLOCK_SIZE          64
#define NEWLIB_MALLOC_POOL2_BLOCK_COUNT         20

#define NEWLIB_MALLOC_POOL3_BLOCK_SIZE          128
#define NEWLIB_MALLOC_POOL3_BLOCK_COUNT         35

#define NEWLIB_MALLOC_POOL4_BLOCK_SIZE          512
#define NEWLIB_MALLOC_POOL4_BLOCK_COUNT         7

#define NEWLIB_MALLOC_POOL5_BLOCK_SIZE          1024
#define NEWLIB_MALLOC_POOL5_BLOCK_COUNT         2

struct TMallocManagerCtrl
{
    uint16_t  buffer_size  [NEWLIB_MALLOC_NB_OF_POOLS];                   // FGC.DEBUG.MEM_POOLS.BUF_SIZE
    uint16_t  nb_of_buffers[NEWLIB_MALLOC_NB_OF_POOLS];                   // FGC.DEBUG.MEM_POOLS.TOTAL_NB
    uint16_t  free_idx     [NEWLIB_MALLOC_NB_OF_POOLS];                   // FGC.DEBUG.MEM_POOLS.ALLOCATED
};

struct TMallocManagerData
{
    struct _pool1
    {
        uint8_t   buffer[NEWLIB_MALLOC_POOL1_BLOCK_SIZE];
    } pool1[NEWLIB_MALLOC_POOL1_BLOCK_COUNT];

    struct _pool2
    {
        uint8_t   buffer[NEWLIB_MALLOC_POOL2_BLOCK_SIZE];
    } pool2[NEWLIB_MALLOC_POOL2_BLOCK_COUNT];

    struct _pool3
    {
        uint8_t   buffer[NEWLIB_MALLOC_POOL3_BLOCK_SIZE];
    } pool3[NEWLIB_MALLOC_POOL3_BLOCK_COUNT];

    struct _pool4
    {
        uint8_t   buffer[NEWLIB_MALLOC_POOL4_BLOCK_SIZE];
    } pool4[NEWLIB_MALLOC_POOL4_BLOCK_COUNT];

    struct _pool5
    {
        uint8_t   buffer[NEWLIB_MALLOC_POOL5_BLOCK_SIZE];
    } pool5[NEWLIB_MALLOC_POOL5_BLOCK_COUNT];
};

struct TMallocManagerDbg        // NB: All sub-structures below are aligned on 4 bytes for easier reading in a memory dump
{
    struct _panic_dbg
    {
        uint32_t      requested_size;
        uint32_t      task_id;
    } panic_data;

    struct _pool_dbg
    {
        uint8_t       free;
        uint8_t       task_id;
        uint16_t      requested_size;
    }                   pool1[NEWLIB_MALLOC_POOL1_BLOCK_COUNT];

    struct _pool_dbg    pool2[NEWLIB_MALLOC_POOL2_BLOCK_COUNT];

    struct _pool_dbg    pool3[NEWLIB_MALLOC_POOL3_BLOCK_COUNT];

    struct _pool_dbg    pool4[NEWLIB_MALLOC_POOL4_BLOCK_COUNT];

    struct _pool_dbg    pool5[NEWLIB_MALLOC_POOL5_BLOCK_COUNT];
};



// ---------- External variable declarations

// Malloc/calloc buffer pools

extern struct TMallocManagerCtrl malloc_mngr_ctrl;
extern struct TMallocManagerData malloc_mngr_data;
extern struct TMallocManagerDbg  malloc_mngr_dbg;

// Reentrant structures pointer for all tasks

extern struct _reent * newlib_reent_ptr[MCU_NUM_TASKS];



// ---------- External function declarations

void InitMalloc(void);


// EOF
