//! @file   digitalIo.h
//! @brief  Manages the digital inputs and outputs of the FGC

#pragma once


// ---------- Includes --------------------------------------------------

#include <stdbool.h>
#include <stdint.h>

#include <bitmap.h>
#include <memmap_mcu.h>



// ---------- External structures, unions and enumerations --------------

struct DigitalIo
{
    uint32_t inputs;	   //!< property DIG.STATUS
    uint32_t direct;       //!< property DIG.IP_DIRECT
    uint16_t simIntlks;    //!< property VS.SIM_INTLKS
    uint16_t outputs;	   //!< property DIG.COMMANDS
    uint16_t cmdRequest;
};


enum DigitalIo_cmd_request
{
	DDOP_CMD_ON       = 0x01,
	DDOP_CMD_OFF      = 0x02,
	DDOP_CMD_RESET    = 0x04,
	DDOP_CMD_POL_POS  = 0x08,
	DDOP_CMD_POL_NEG  = 0x10,
	DDOP_CMD_BLOCKING = 0x20,
	DDOP_CMD_UNBLOCK  = 0x40,
	DDOP_CMD_FGCFLT   = 0x80
};


//! Digital supplementary output and input channels.

enum DigitalIo_sup_channel
{
    DIG_SUP_A_CHANNEL0 = 0,
    DIG_SUP_A_CHANNEL1 = 1,
    DIG_SUP_A_CHANNEL2 = 2,
    DIG_SUP_A_CHANNEL3 = 3,
    DIG_SUP_A_CHANNEL4 = 4,
    DIG_SUP_B_CHANNEL0 = 0,
    DIG_SUP_B_CHANNEL1 = 1,
    DIG_SUP_B_CHANNEL2 = 2,
    DIG_SUP_B_CHANNEL3 = 3,
    DIG_SUP_B_CHANNEL4 = 4
};


//! Digital supplementary bitmask

enum DigitalIo_sup_channel_mask
{
    DIG_SUP_A_MASK_CHANNEL0 = (1 << DIG_SUP_A_CHANNEL0),
    DIG_SUP_A_MASK_CHANNEL1 = (1 << DIG_SUP_A_CHANNEL1),
    DIG_SUP_A_MASK_CHANNEL2 = (1 << DIG_SUP_A_CHANNEL2),
    DIG_SUP_A_MASK_CHANNEL3 = (1 << DIG_SUP_A_CHANNEL3),
    DIG_SUP_A_MASK_CHANNEL4 = (1 << DIG_SUP_A_CHANNEL4),
    DIG_SUP_B_MASK_CHANNEL0 = (1 << DIG_SUP_B_CHANNEL0),
    DIG_SUP_B_MASK_CHANNEL1 = (1 << DIG_SUP_B_CHANNEL1),
    DIG_SUP_B_MASK_CHANNEL2 = (1 << DIG_SUP_B_CHANNEL2),
    DIG_SUP_B_MASK_CHANNEL3 = (1 << DIG_SUP_B_CHANNEL3),
    DIG_SUP_B_MASK_CHANNEL4 = (1 << DIG_SUP_B_CHANNEL4)
};


//! Must match definition in DIG_SUP_A_CHAN_CTRL_OUTMPX_MASK16
//! https://accwww.cern.ch/proj-fgc/gendoc/def/Platform_fgc3_memmap_MCU_DIG_SUP_A_CHAN_CTRL_OUTMPX.htm

enum DigitalIo_sup_ctrl_outmpx
{
    DIG_SUP_CTRL_OUTMPX_DATA        ,
    DIG_SUP_CTRL_OUTMPX_PULSE       ,
    DIG_SUP_CTRL_OUTMPX_SERIAL      ,
    DIG_SUP_CTRL_OUTMPX_PULSE_C0    ,
    DIG_SUP_CTRL_OUTMPX_50HZ_CLOCK  ,
    DIG_SUP_CTRL_OUTMPX_1KHZ_CLOCK  ,
    DIG_SUP_CTRL_OUTMPX_1KHZ_SYNC   ,
    DIG_SUP_CTRL_OUTMPX_1MHZ_CLOCK  ,
    DIG_SUP_CTRL_OUTMPX_10KHZ_CLOCK ,
};


enum DigitalIo_sup_status
{
    DIG_SUP_STATUS_SUCCESS,
    DIG_SUP_STATUS_NOT_SUPPORTED,
};



// ---------- External variable declarations ----------------------------

// Should be internal but must be external due to link to properties

extern struct DigitalIo digitalIo;



// ---------- Platform/class specific functions

//! Class specific initialization of the digital supplemental I/O

void digitalIoSupInitClass(void);



// ---------- External function declarations ----------------------------


//! Initializes the digital I/O.

void digitalIoInit(void);


//! This function will read and/or simulate the direct inputs registers. There
//! are two properties that control the way the simulation works and three
//! different modes:
//!
//!     STATE.OP        VS.SIM_INTLKS       VSRUN/VSREADY        All other
//! stateOpGetState()  digitalIo.simIntlks    VSPOWERON/OPBLOCKED     signals
//!  --------------------------------------------------------------------------------------
//!  1: !SIMULATION    not significant           REAL              REAL
//!  2:  SIMULATION        DISABLED           SIMULATED            REAL
//!  3:  SIMULATION        ENABLED            SIMULATED          SIMULATED
//!  --------------------------------------------------------------------------------------

//!   Input/Cmd      Mode 1      Mode 2       Mode 3
//!    Signal        NORMAL      !SIM_INTLKS  SIM ALL
//!  -----------------------------------------------------------------
//!  DIG watchdog    TRIGGERED   TRIGGERED    NON TRIGGERED
//!  -----------------------------------------------------------------
//!  VSNOCABLE       REAL        REAL         0
//!  OPBLOCKED       REAL        SIM          SIM
//!  VSRUN           REAL        SIM          SIM
//!  PWRFAILURE      REAL        0            0
//!  INTLKOUT        REAL        REAL         0
//!  PCPERMIT        REAL        REAL         1
//!  INTLKSPARE      REAL        REAL         0
//!  FASTABORT       REAL        REAL         0
//!  POLSWINEG       REAL        REAL         SIM
//!  POLSWIPOS       REAL        REAL         SIM
//!  DCCTAFLT        REAL        REAL         0
//!  DCCTBFLT        REAL        REAL         0
//!  VSEXTINTLK      REAL        REAL         0
//!  VSFAULT         REAL        0            0
//!  VSREADY         REAL        SIM          SIM
//!  VSPOWERON       REAL        SIM          SIM
//!  -----------------------------------------------------------------
//!  FGCOKCMD        REAL        REAL         SIM
//!  CMD6_CMD        REAL        0            0
//!  AFRUNCMD        REAL        0            0
//!  POLTONEGCMD     REAL        0            0
//!  POLTOPOSCMD     REAL        0            0
//!  UNBLOCKCMD      REAL        SIM          SIM
//!  VSRESETCMD      REAL        0            0
//!  VSRUNCMD        REAL        SIM          SIM

void digitalIoInputsProcess(void);


//!	This function will read and/or simulate the direct outputs registers.

void digitalIoOutputsProcess(void);


//!	Returns the current inputs corresponding to the property DIG.STATUS.
//!
//! @return inputs.

uint32_t digitalIoGetInputs(void);


//!	Returns the current direct inputs.
//!
//!	@return direct inputs.

uint32_t digitalIoGetRawInputs(void);


//!	Returns the current outputs corresponding to the property DIG.COMMANDS.
//!
//!	@return outputs.

uint16_t digitalIoGetOutputs(void);


//! Enables the simulation of VS interlocks

void digitalIoEnableVsSimIntlks(void);


//! Disables the simulation of VS interlocks

void digitalIoDisableVsSimIntlks(void);


//!	Returns the value of the property VS.SIM_INTLKS.
//!	It is ENABLED if the interlocks are being simulated and DISABLED if they are not.
//!
//!	@retval		FGC_CTRL_ENABLED 	Interlocks simulation is enabled.
//!	@retval		FGC_CTRL_DISABLED	Interlocks simulation is disabled.

uint16_t digitalIoGetVsSimIntlks(void);


//!	Sets a request for a command.
//!
//!	@param[in]	cmd 	command request.

void digitalIoSetCmdRequest(enum DigitalIo_cmd_request cmd);


//! Enable digital CMD output.

void digitalIoEnableOutputs(void);


//! Disable digital CMD output.

void digitalIoDisableOutputs(void);


//! Sets the mode used by the firing card
//!
//! @param[in] mode  Firing mode.

void digitalIoSetFiringMode(uint16_t mode);



// ---------- External function definitions

// DIG_SUP functions

//! Gets the channel data on SUP_B
//!
//! @param  channel The supplemental digital channel to get.
//!
//! @retval True    The channel is set
//! @retval False   The channel is not set

static inline bool digitalIoSupGetChannelB(enum DigitalIo_sup_channel const channel)
{
    return testBitmap(DIG_SUP_B_PORT_P, 1 << channel);
}


//! Sets the timing pulse rising edge and width
//!
//! Note: assumes block A is configured as output.
//!
//! @param channel    The supplemental digital channel to configure.
//! @param etim_us    Event time value in microseconds.
//! @param width_us   Width of the pulse in microseconds.

static inline void digitalIoSupSetPulse(enum DigitalIo_sup_channel const channel,
                                        int32_t                    const etim_us,
                                        int32_t                    const width_us)
{
    // Only channels A0, A1, A2, A3 can be used to generate timing pulses.
    // The rising edge of the pulse is synchronized with TIME_TILL_EVT
    // based on the duration of 'fall - rise'.
    //
    // Note that if the ARM bit corresponding to the channel is already set,
    // this action will be silently ignored by the FPGA.
    //
    // Negative delays correspond to times prior to the event. The counter
    // TIME_TILL_EVT is positive during the period prior to the event.
    // Hence the need to flip the value of in pulse->rise.
    //
    // Always clear the ARM bit so that the PULSE can be re-configured.

    clrBitmap(DIG_SUP_A_ARM_P, (1 << channel));

    DIG_SUP_A_PULSE_ETIM_US_A[channel]  = etim_us;
    DIG_SUP_A_PULSE_WIDTH_US_A[channel] = width_us;

    setBitmap(DIG_SUP_A_ARM_P, (1 << channel));
}



//! Returns true if a rising edge has been detected on the given channel.
//!
//! @param  channel     The supplemental digital channel to access.
//! @retval true        If the rising edge has been detected.
//! @retval false       If the rising edge has not been detected.

static inline bool digitalIoSupTestRiseEdge(enum DigitalIo_sup_channel const channel)
{
    return (testBitmap(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_RISING_EDGE_MASK16));
}


//! Returns the time in microseconds - with respect to the time till event register -
//! of the rising edge detected in the given channel.
//!
//! Note: assumes block B is configured as input. *
//!
//! @param channel The supplemental digital channel to access.
//! @retval The time in microseconds when the rising edge occurred.

static inline int32_t digitalIoSupGetRiseEtim(enum DigitalIo_sup_channel const channel)
{
    return DIG_SUP_B_RISE_ETIM_US_A[channel];
}


//! Returns true if a falling edge has been detected on the given channel.
//!
//! @param  channel     The supplemental digital channel to access.
//! @retval true        If the falling edge has been detected.
//! @retval false       If the falling edge has not been detected.

static inline bool digitalIoSupTestFallEdge(enum DigitalIo_sup_channel const channel)
{
    return (testBitmap(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_FALLING_EDGE_MASK16));
}


//! Returns the time in microseconds - with respect to the time till event register -
//! of the falling edge detected in the given channel.
//!
//! Note: assumes block B is configured as input. *
//!
//! @param channel The supplemental digital channel to access.
//! @retval The time in microseconds when the falling edge occurred.

static inline int32_t digitalIoSupGetFallEtim(enum DigitalIo_sup_channel const channel)
{
    return DIG_SUP_B_FALL_ETIM_US_A[channel];
}


// EOF
