//! @file  logRun.h
//! @brief Run log

#pragma once


// ---------- Includes

#include <stdint.h>

#include <fgc_runlog.h>
#include <fgc_runlog_entries.h>



// ---------- External function declarations

//! This function will be called when the boot starts or by the boot Reset Run Log menu option.
//!
//! If the NV_RAM area is not valid or the index is not valid the runlog will be cleared.

void logRunInit(void);


//! Do a logRunInit() if run log is not ready.

void logRunInitIfNotReady(void);


//! All the reset counters will be reset to zero.

void logRunClrResets(void);


//! Returns the DSP reset counter.
//!
//! @returns DSP reset counter

uint16_t logRunGetDspReset(void);


//! Increment DSP reset counter

void logRunIncrementDspReset(void);


//! Set the DSP reset counter to zero.

void logRunClrDspReset(void);


//! This function will write a record in the run log.  It adds the record to the NV_RAM run log zone.
//! Interrupts are disabled during this function to ensure the run log stays coherent.

void logRunAddEntry(uint8_t id, void const * data);


//! This function will read the specified record from the run log.
//! If index is not valid, it will be initialised to the most recently entered record.
//! The function returns the address of the previous record.

uint16_t logRunReadEntry(uint16_t * index, struct fgc_runlog * runlog_entry);


//! Add timestamp to runlog entry.

void logRunAddTimestamp(void);


// EOF
