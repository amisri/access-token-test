//! @file   stateVs.h
//! @brief

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External structures, unions and enumerations

struct dim_channel
{
    uint16_t          channel;
    uint16_t          mask;
};


struct vs_vars
{
    uint16_t            active_filter;                  // VS.ACTIVE_FILTER
    uint16_t            ext_control;                    // VS.EXT_CONTROL
    float               vsrun_timeout;                  // VS.VSRUN_TIMEOUT
    float               gw_com_timeout;                 // VS.GW_COM_TIMEOUT
    uint16_t            vs_ready_timeout;               // VS.VS_READY_TIMEOUT
    uint16_t            vs_not_rdy_count;               // Number of consecuite cycles with VS_READY not active
    uint16_t            req_polarity;                   // Requested polarity switch state
    uint16_t            polarity_counter;               // Polarity switch check counter (s)

    struct dim_channel  vs_redundancy_warning;          // Voltage source redundancy warning
    struct dim_channel  fabort_unsafe_dig;              // Fast abort unsafe DIM digital signal
    uint16_t            fabort_unsafe;                  // 0=FWD Not Present  1=No FWD Fault  2=FWD Fault

    struct dim_channel  fw_diode_dig;                   // Fast abort unsafe DIM digital signal
    uint16_t            fw_diode;                       // 0=FWD Not Present  1=No FWD Fault  2=FWD Fault
    uint16_t            present;                        // VS.PRESENT property
};



// ---------- External variable declarations

extern struct vs_vars vs;



// ---------- External function declarations

//! This function is called to determine the voltage source state from five
//! direct digital inputs using a lookup table.

void stateVsProcess(void);


//! Returns true if the VS is off

bool stateVsIsOff(void);


// EOF
