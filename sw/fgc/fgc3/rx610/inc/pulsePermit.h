//! @file  pulsePermit.h
//! @brief Logic to handle the pulse permit
//!
//!  This functionality is only required by the ComHV-PS chassis and the
//!  Modulator converter, used in the RF system. The pulse permit signal is
//!  managed by the RF system.

#pragma once


// ---------- Includes

#include <stdbool.h>



// ---------- External function declarations

//! Returns True if the pulse permit (INTLKSPARE) is valid.
//!
//! This condition is only valid for the ComHV (RF systems) and the Modulator
//! converter.
//!
//! @retval True if the pulse permit is valid. False otherwise.

bool pulsePermitValid(void);


//! Polls the pulse permit signal (INTLKSPARE digital input) to block
//! and unblock the converter.
//!
//!  This condition is only valid for the ComHV (RF systems) and the Modulator
//!  converter.

void pulsePermitProcess(void);


// EOF
