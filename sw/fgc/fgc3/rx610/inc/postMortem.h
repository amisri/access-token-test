//!  @file     postMortem.h
//!  @brief    This file handles post mortems

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>



// ---------- External function declarations

//! Initialization function

void postMortemInit(void);


//! Runs periodic checks to process the post-mortem status

void postMortemProcess(void);


//! Sets the PM-buf and FGC-logger enable flag

void postMortemSetEnable(bool pmd, bool logger);


//! Triggers a post-mortem event

void postMortemTriggerPm(void);


//! Triggers an external post-mortem

void postMortemTriggerPmExt(void);


//! Triggers a slow-abort event

void postMortemTriggerSa(void);


//! Triggers a DIM event

void postMortemTriggerDim(uint32_t dim_index);


//! Resets the FGC logger or PMD event

void postMortemReset(void);


//! Synchs the log and clears the corresponding bit in the bit-mask

void postMortemSyncLog(uint8_t log_idx);


// EOF
