//! @file  polSwitch.h
//! @brief This file provides the handling of the polarity switch.

#pragma once


// ---------- Includes

#include <stdint.h>

#include <libpolswitch.h>



// ---------- External function declarations

//! Initialises the polarity switch internal state.

void polSwitchInit(void);


//! Updates the polarity switch state and status (warnings and faults).

void polSwitchProcess(void);


//! Resets the polarity switch

void polSwitchReset(void);


//! Sets the polarity switch mode
//!
//! @param[in]   mode        Polarity switch mode

void polSwitchSetMode(enum POLSWITCH_mode mode);


//! Returns the polarity switch command
//!
//! @retval 0                No command.
//! @retval DDOP_CMD_POL_POS Positive polarity command.
//! @retval DDOP_CMD_POL_NEG Negative polarity command.

uint16_t polSwitchGetCommand(void);


//! Returns the polarity swtich state
//!
//! @return                 The polarity switch state

enum POLSWITCH_state polSwitchGetState(void);


//! Returns the polarity swtich faults
//!
//! @return                 The polarity switch faults

enum POLSWITCH_fault_bits polSwitchGetFaults(void);


//! Returns whether the polarity swtich is enabled
//!
//! @retval      True       The polarity switch is enabled
//! @retval      False      The polarity switch is disabled

bool polSwitchIsEnabled(void);


// EOF
