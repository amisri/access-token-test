//! @file  defprops_inc.h
//! @brief Adds all the headers needed by defprops.h

#pragma once


#include <anaCard.h>
#include <calibration.h>
#include <config.h>
#include <crate.h>
#include <cycSim.h>
#include <dallasTask.h>
#include <defprops_inc_class.h>
#include <device.h>
#include <diag.h>
#include <digitalIo.h>
#include <dpcls.h>
#include <etherComm.h>
#include <ethernet.h>
#include <events.h>
#include <fbs.h>
#include <fbs_class.h>
#include <get.h>
#include <hash.h>
#include <logSpy.h>
#include <mcu.h>
#include <memmap_mcu.h>
#include <msTask.h>
#include <nvs.h>
#include <panic.h>
#include <pll.h>
#include <prop.h>
#include <property.h>
#include <regfgc3.h>
#include <regfgc3Pars.h>
#include <regfgc3Prog.h>
#include <regfgc3ProgFsm.h>
#include <rtd.h>
#include <sta.h>
#include <stateOp.h>
#include <stateVs.h>
#include <syscalls.h>
#include <taskTrace.h>
#include <version.h>


// EOF
