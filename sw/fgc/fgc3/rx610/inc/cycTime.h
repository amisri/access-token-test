//! @file  cycTime.h
//! @brief Interface to the Time registers related to cycles

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <bitmap.h>
#include <memmap_mcu.h>



// ---------- External function definitions

static inline void cycTimeSetTimeTillEvent(int32_t const value_us, bool const external)
{
    // For external timing, the counter is decremented immediately after detecting
    // rising edge on DIG_SUP_B4. Otherwise, it is decremented on the next millisecond.

    TIME_TILL_EVT_US_P = value_us - (external == false ? 1000 : 0);
}



static inline int32_t cycTimeGetTimeTillEvent(void)
{
    return TIME_TILL_EVT_US_P;
}



static inline void cycTimeSetTimeTillC0(int32_t const value_us, bool const external)
{
    // For external timing, the counter is decremented immediately after detecting
    // rising edge on DIG_SUP_B4. Otherwise, it is decremented on the next millisecond.

    TIME_TILL_C0_US_P = value_us - (external == false ? 1000 : 0);
}



static inline int32_t cycTimeGetTimeTillC0(void)
{
    return TIME_TILL_C0_US_P;
}



static inline bool cycTimeTstStatus(void)
{
    return testBitmap(TIME_DSP_FLAG_ARM_P, TIME_DSP_FLAG_ARM_SET_MASK16);
}



static inline bool cycTimeTstPulseArm(void)
{
    return testBitmap(TIME_DSP_FLAG_ARM_P, TIME_DSP_FLAG_ARM_SET_MASK16);
}



static inline void cycTimeSetPulseArm(void)
{
    setBitmap(TIME_DSP_FLAG_ARM_P, TIME_DSP_FLAG_ARM_SET_MASK16);
}



static inline void cycTimeClrPulseArm(void)
{
    clrBitmap(TIME_DSP_FLAG_ARM_P, TIME_DSP_FLAG_ARM_SET_MASK16);
}



static inline void cycTimeSetPulseEtim(uint32_t const value_us)
{
    // Sets the estimated time - with respect to the time till event register -
    // for the rising edge of the pulse generated for the DSP flag pulse.

    *TIME_DSP_FLAG_ETIM_US_A = value_us;
}



static inline void cycTimeSetPulseWidth(uint32_t const value_us)
{
    // Sets the width in microseconds of the pulse generated for the
    // DSP flag pulse.

    *TIME_DSP_FLAG_WIDTH_US_A = value_us;
}


// EOF
