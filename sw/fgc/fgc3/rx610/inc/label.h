//!	@file  label.h
//!	@brief Structure for symbolic names

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External structures, unions and enumerations

//! When using this structure in arrays,
//! remember to add at the end one line filled with 0s
//! as the label part == 0  marks the end of the table for several functions like TypeLabel()

struct sym_name
{
    uint32_t   type;   //!< Type index
    char     * label;  //!< Pointer to label (string constant)
};


// EOF
