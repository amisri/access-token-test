//! @file  trm.h
//! @brief Terminal Communications Interface

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <os.h>
#include <queue.h>



// ---------- Constants

// Serial Communication Interface Global Constants

#define TRM_N_BUFS                      6
#define TRM_BUF_SIZE                    55
#define TRM_MSGQ_SIZE                   64


// #define TRM_N_LINEBUFS                        16                      // Must be a power of 2 !!!
// #define TRM_LINEBUFS_MASK                     (TRM_N_LINEBUFS-1)      // Mask for circular history buffer
// #define TRM_LINE_SIZE                         78
#define TRM_EDITOR_MODE_TIMEOUT_MIN           60
#define TRM_CMD_START                          1
#define TRM_CMD_DEFINED                        2
#define TRM_CMD_RECEIVING                      3



// ---------- External structures, unions and enumerations

enum Trm_modes
{
    TRM_DIRECT_MODE,
    TRM_EDITOR_MODE
};


// Serial Communication Interface Task Variables

struct terminal_vars
{
    OS_MSG       *      msgq;                           // Input characters message Q
    OS_MEM       *      mbuf;                           // Output buffers memory partition (in terminal.mem_buf)
    uint16_t            mode;                           // Terminal mode
    uint16_t            xoff_timer;                     // XOFF timer (ms)

    uint8_t             mem_buf[TRM_N_BUFS * TRM_BUF_SIZE ];    // terminal output buffer space
    void        *       msgq_buf[TRM_MSGQ_SIZE];         // Trm task message queue buffer

    bool                char_waits_for_tx;              // Character is waiting to be sent flag
    uint8_t             snd_ch;                         // Single char output buffer to IsrMst
};


struct terminal_static
{
    uint16_t          edit_mode_timer_ms;             //!< Edit mode timer (ms)
    uint16_t          edit_mode_timer_min;            //!< Edit mode timer (minutes)
    uint16_t          cmd_state;                      //!< Direct reception state machine
    uint16_t          pkt_buf_idx;                    //!< Packet buffer index
    uint16_t          pkt_in_idx;                     //!< Packet input index (0-120)
    uint16_t          pkt_header_flags;               //!< Command packet header
    struct Queue      bufq;                           //!< TRM buffer Q indices and status
    struct Pars_pkt * pkt;                            //!< Pointer to current cmd packet
    char            * bufq_adr[TRM_N_BUFS + 1];       //!< TRM buffer address Q
    uint16_t          bufq_len[TRM_N_BUFS + 1];       //!< TRM buffer length Q
    bool              cmd_space_f;                    //!< SciAddCmdCh() space flag
};



// ---------- External variable declarations

extern struct terminal_vars   terminal;         //!< Terminal variables structure
extern struct terminal_static terminal_state;



// ---------- External function declarations

//! Terminal intialization

void trmInit(void);


//! This function is the terminal input task.  It gets characters received from the serial input or from the
//! remote terminal channel.  The communication may either come from a ANSI/VT100 terminal, or a client application.
//! Editor mode is provided to support human interaction via a terminal, while Direct mode and Diagnostic mode
//! are provided for client applications.
//!
//! This function never exits, so "local" variables are static to save stack space.

void trmTask(void *);


//! This function is called from the the Millisecond Task to manage output buffers destined for the SCI.
//!
//! Note that terminal_state.bufq can never fill up because it is one element longer than the number of buffers it
//! needs to address.
//!
//! The diag_sci structure starts with a header field of six sync bytes.
//!
//! Note that term_byte_idx is initialised to 1 and counts to the length of the SCI diag data.
//! diag.term_byte_idx is set to zero when diag transmission starts to signal that it
//! is the first cycle and transmission is to be suppressed.

void trmMsOut(void);


//! Sends a command over the terminal

void trmSendCommand(char const * command);


// EOF
