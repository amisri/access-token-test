
//! @file regfgc3_prog_fsm.c
//! @brief


// ---------- Includes

#include <assert.h>

#include "fgc3/rx610/inc/cmd.h"
#include "fgc3/rx610/inc/mem.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3Prog.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3ProgFsm.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3Task.h"

#include <defprops.h>
#include <fgc_errs.h>



// ---------- Constants

#define STATE_PROG_FSM_MAX_TRANSITIONS 3



// ---------- Internal structures, unions and enumerations

//! Type for state machine state function that returns a state number

typedef uint16_t StateProg(bool const first_time);

//! Type for a state machine transition function that returns a state function

typedef StateProg * StateProgTrans(void);

//! Structure for global local variables

struct Regfgc3_prog_fsm
{
    StateProg * state_prog_func;
    uint16_t     status;
    bool         state_error;
};



// ---------- External variable definitions

struct Regfgc3_prog_fsm_prop __attribute__((section("sram"))) regfgc3_prog_fsm_prop;



// ---------- Internal variable definitions

static struct Regfgc3_prog_fsm __attribute__((section("sram"))) regfgc3_prog_fsm;



// ---------- Internal function definitions


static uint16_t regFgc3ProgFsmValidateMode(uint8_t const mode)
{
    uint8_t state_prog_fsm = regFgc3ProgFsmGetState();
    uint8_t err_code        = FGC_OK_NO_RSP;

    switch (mode)
    {
        case FGC_REGFGC3_PROG_FSM_WAITING:
        {
            if ((state_prog_fsm != FGC_REGFGC3_PROG_FSM_CLEAN_UP)
                && (state_prog_fsm != FGC_REGFGC3_PROG_FSM_WAITING))
            {
                err_code = FGC_BAD_STATE;
            }
            break;
        }
        case FGC_REGFGC3_PROG_FSM_TRANSFERRED:
        {
            if (   (state_prog_fsm != FGC_REGFGC3_PROG_FSM_WAITING)
                && (state_prog_fsm != FGC_REGFGC3_PROG_FSM_TRANSFERRING)
                && (state_prog_fsm != FGC_REGFGC3_PROG_FSM_TRANSFERRED))
            {
                err_code = FGC_BAD_STATE;
            }
            break;
        }
        case FGC_REGFGC3_PROG_FSM_PROGRAMMED:
        {
            if (   (state_prog_fsm != FGC_REGFGC3_PROG_FSM_TRANSFERRED)
                && (state_prog_fsm != FGC_REGFGC3_PROG_FSM_GET_PROG_INFO)
                && (state_prog_fsm != FGC_REGFGC3_PROG_FSM_PROGRAMMING)
                && (state_prog_fsm != FGC_REGFGC3_PROG_FSM_PROG_CHK)
                && (state_prog_fsm != FGC_REGFGC3_PROG_FSM_PROGRAMMED))
            {
                err_code = FGC_BAD_STATE;
            }
            break;
        }
        case FGC_REGFGC3_PROG_FSM_CLEAN_UP:
        {
            break;
        }
        default:
        {
            break;
        }
    }

    return err_code;
}



//! These are the state functions

//! Waiting state.
//!
//! The FSM will state in this state until the program manager starts its actions.
//!
//! @param first_time[in]     Signals to the function that it is the first time it will be executed.
//! @return uint16_t        The value for the waiting state.

static uint16_t stateProgWaiting(bool const first_time)
{
    return FGC_REGFGC3_PROG_FSM_WAITING;
}



//! Transferring state.
//!
//! The FSM will execute this state when the program manager starts sending a binary file. Ideally,
//! the RegFgc3 task should handle this, instead of the command task (QK decided not to). As a result,
//! the current implementation is blocking.
//!
//! @param first_time[in]     Signals to the function that it is the first time it will be executed.
//! @return uint16_t        The value for the waiting state.

static uint16_t stateProgTransferring(bool const first_time)
{
    //TODO: check how much time may take to send the binary
    //TODO: why this does not wait for
    // uint16_t timeout_ms = 2000;
    // prop_size_t received_num_els = 0;

    // received_num_els = PropGetNumEls(&rcm, &PROP_REGFGC3_PROG_BIN);

    // while ((timeout_ms != 0) && (received_num_els == 0))
    // {
    //     timeout_ms--;
    //     sleepMs(1);
    //     received_num_els = PropGetNumEls(&rcm, &PROP_REGFGC3_PROG_BIN);
    // }

    // if (received_num_els == 0)
    // {
    //     //TODO: state_error as property with symlist?
    //     regfgc3_prog_fsm.state_error = true;
    // }
    return FGC_REGFGC3_PROG_FSM_TRANSFERRING;
}



//! Transferred state.
//!
//! The FSM will enter this state after a FW binary has finished being transferred to the FGC3.
//!
//! @param first_time[in]     Signals to the function that it is the first time it will be executed.
//! @return uint16_t        The value for the waiting state.

static uint16_t stateProgTransferred(bool const first_time)
{
    return FGC_REGFGC3_PROG_FSM_TRANSFERRED;
}




//! GetProgInfo state.
//!
//! Makes the FGC3 ask a certain card about how to slice the FW binary before sending it. The data obtained
//! from the card is stored in regfgc3_prog.
//!
//! @param first_time[in]   Signals to the function that it is the first time it will be executed.
//! @return uint16_t        The value for the waiting state.

static uint16_t stateProgGetProgInfo(bool const first_time)
{
    if (first_time == true)
    {
        if (regFgc3ProgGetSlicingInfo() != FGC_OK_NO_RSP)
        {
            regfgc3_prog_fsm.state_error = true;
        }
    }

     return FGC_REGFGC3_PROG_FSM_GET_PROG_INFO;
}



//! Programming state.
//!
//! Commands the FGC3 to start sending the binary to the card.
//!
//! @param first_time[in]   Signals to the function that it is the first time it will be executed.
//! @return uint16_t        The value for the waiting state.

static uint16_t stateProgProgramming(bool const first_time)
{
    if (first_time == true)
    {
        if (regFgc3ProgReprogram() != FGC_OK_NO_RSP)
        {
            regfgc3_prog_fsm.state_error = true;
        }
    }

    return FGC_REGFGC3_PROG_FSM_PROGRAMMING;
}



//! ProgramCheck state.
//!
//! The FGC3 asks the card to compute the binary's CRC, which then the card compares with the one it read from the binary's header.
//!
//! @param first_time[in]   Signals to the function that it is the first time it will be executed.
//! @return uint16_t        The value for the waiting state.

static uint16_t stateProgProgramCheck(bool const first_time)
{
    if (first_time == true)
    {
        if (regFgc3ProgVerifyProgram() != FGC_OK_NO_RSP)
        {
            regfgc3_prog_fsm.state_error = true;
        }
    }

    return FGC_REGFGC3_PROG_FSM_PROG_CHK;
}



//! Programmed state.
//!
//! This state is returned if the card was programmed successfully.
//!
//! @param first_time[in]   Signals to the function that it is the first time it will be executed.
//! @return uint16_t        The value for the waiting state.

static uint16_t stateProgProgrammed(bool const first_time)
{
    return FGC_REGFGC3_PROG_FSM_PROGRAMMED;
}



//! Error state.
//!
//! The FSM enters this state when an error in any other state was detected.
//!
//! @param first_time[in]   Signals to the function that it is the first time it will be executed.
//! @return uint16_t        The value for the waiting state.

static uint16_t stateProgError(bool const first_time)
{
    return FGC_REGFGC3_PROG_FSM_ERROR;
}



//! Clean up state.
//!
//! The FSM will enter this state after the programming cycle has finished or after an error occurred.
//! It performs cleaning of certain programming related structures in regfgc3_prog and the part of the
//! DSP external memory where the binary was temporarily stored.
//!
//! @param first_time[in]   Signals to the function that it is the first time it will be executed.
//! @return uint16_t        The value for the waiting state.

static uint16_t stateProgCleanUp (bool const first_time)
{
    if (first_time == true)
    {
        //TODO: clean up also *.stats and *.debug?
        uint16_t pm_status = regfgc3_prog_prop.prog_data.state;

        memset(&regfgc3_prog_prop.prog_data, 0, sizeof(struct Regfgc3_prog_prog_data));
        memset(&regfgc3_prog_prop.debug,     0, sizeof(struct Regfgc3_prog_debug));

        regfgc3_prog_prop.prog_data.state = pm_status;
        regfgc3_prog_prop.prog_data.lock = FGC_REGFGC3_PROG_LOCK_DISABLED;

        regfgc3_prog_fsm.state_error = false;

        // Clean up DSP external memory
        PropSet(&fcm, &PROP_REGFGC3_PROG_BIN, NULL, regfgc3_prog_prop.prog_data.bin_size_bytes / 4);
    }
    return FGC_REGFGC3_PROG_FSM_CLEAN_UP;
}



//! These are the transition functions

//! Transition from CleanUp to WaiTing
//!
//! @return StateProg*         The Waiting state.

static StateProg* CUtoWT(void)
{
    if (regfgc3_prog_fsm_prop.mode == FGC_REGFGC3_PROG_FSM_WAITING)
    {
        return stateProgWaiting;
    }

    return NULL;
}



//! Transition from WaiTing to TransferrinG
//!
//! @return StateProg*        Transferring state.

static StateProg* WTtoTG(void)
{
    //TODO: can the converter be in FLT_OFF?

    if ((regfgc3_prog_fsm_prop.mode == FGC_REGFGC3_PROG_FSM_TRANSFERRED))
    // && (pcStateGet() == FGC_PC_OFF))
    {
        return stateProgTransferring;
    }

    return NULL;
}



//! Transition from TransferrinG to TransferreD
//!
//! @return StateProg*  Transferred state.

static StateProg* TGtoTD(void)
{
    if (   (regfgc3_prog_fsm_prop.mode == FGC_REGFGC3_PROG_FSM_TRANSFERRED)
        && (regfgc3_prog_fsm_prop.state == FGC_REGFGC3_PROG_FSM_TRANSFERRING))
    {
        return stateProgTransferred;
    }

    return NULL;
}



//! @brief                Transition from TransferreD to CardInfo.
//!
//! @return StateProg*    CardInfo state.

static StateProg* TDtoCI(void)
{
    if (regfgc3_prog_fsm_prop.mode == FGC_REGFGC3_PROG_FSM_PROGRAMMED)
    {
        return stateProgGetProgInfo;
    }

    return NULL;
}



//! @brief                Transition from CardInfo to ProgramminG.
//!
//! @return StateProg*    Programming state.

static StateProg* CItoPG(void)
{
    if (   (regfgc3_prog_fsm_prop.mode == FGC_REGFGC3_PROG_FSM_PROGRAMMED)
        && (regfgc3_prog_fsm_prop.state == FGC_REGFGC3_PROG_FSM_GET_PROG_INFO))
    {
        return stateProgProgramming;
    }

    return NULL;
}



//! @brief                Transition from ProgramminG to ProgramCheck.
//!
//! @return StateProg*    ProgramCheck state.

static StateProg* PGtoPC(void)
{
    if (regfgc3_prog_fsm_prop.mode == FGC_REGFGC3_PROG_FSM_PROGRAMMED)
    {
        return stateProgProgramCheck;
    }
    return NULL;
}



//! @brief                Transition from ProgramCheck to ProgrammeD.
//!
//! @return StateProg*    ProgrammeD state.

static StateProg* PCtoPD(void)
{
    if (regfgc3_prog_fsm_prop.mode == FGC_REGFGC3_PROG_FSM_PROGRAMMED)
    {
        return stateProgProgrammed;
    }

    return NULL;
}



//! @brief                Transition from any state to CleanUp.
//!
//! @return StateProg*    CleanUp state.

static StateProg* XXtoCU(void)
{
    if (regfgc3_prog_fsm_prop.mode == FGC_REGFGC3_PROG_FSM_CLEAN_UP)
    {
        return stateProgCleanUp;
    }

    return NULL;
}



//! @brief                Transition from any state to ERor.
//!
//! @return StateProg*    Error state.

static StateProg* XXtoER(void)
{
    if (regfgc3_prog_fsm.state_error != false)
    {
        return stateProgError;
    }

    return NULL;
}



static StateProgTrans *StateTransitions[][STATE_PROG_FSM_MAX_TRANSITIONS + 1] =
{
    { WTtoTG,                 NULL }, // 0.WT (WaiTing)
    { XXtoER, TGtoTD, XXtoCU, NULL }, // 1.TG (TransferrinG)
    { TDtoCI,         XXtoCU, NULL }, // 2.TD (TransferreD)
    { XXtoER, CItoPG, XXtoCU, NULL }, // 3.CI (Card Info)
    { XXtoER, PGtoPC, XXtoCU, NULL }, // 4.PG (ProGramming)
    { XXtoER, PCtoPD, XXtoCU, NULL }, // 5.PC (Program Check)
    { XXtoCU,                 NULL }, // 6.PD (ProgrammeD)
    { CUtoWT,                 NULL }, // 7.CU (Clean Up)
    { XXtoCU,                 NULL }  // 8.ER (Error)
};



// ---------- External function definitions

//! @brief
//!
//! @param mode
//! @return uint16_t

uint16_t regFgc3ProgFsmSetMode(uint8_t const mode)
{
    uint16_t errnum;

    if (regFgc3ProgGetOpState() == FGC_REGFGC3_PROG_LOCK_ENABLED)
    {
        return FGC_BAD_STATE;
    }

    if ((errnum = regFgc3ProgFsmValidateMode(mode)) != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    regfgc3_prog_fsm_prop.mode = mode;
    regFgc3ProgFsmProcess();

    return FGC_OK_NO_RSP;
}



//! @brief     Initializes the programming finite state machine.

void regFgc3ProgFsmInit(void)
{
    regfgc3_prog_fsm_prop.state      = FGC_REGFGC3_PROG_FSM_WAITING;
    regfgc3_prog_fsm_prop.mode       = FGC_REGFGC3_PROG_FSM_WAITING;
    regfgc3_prog_fsm.state_prog_func = stateProgWaiting;
}



//! @brief    Exercises the finite state machine when a new mode is set.

void regFgc3ProgFsmProcess(void)
{
    StateProg       * nextState         = NULL;
    StateProgTrans  * transition        = NULL;
    StateProgTrans ** stateTransitions  = &StateTransitions[regfgc3_prog_fsm_prop.state][0];

    // Add while here to iterate until desired state is reached or error
    while (   ((transition = *(stateTransitions++)) != NULL)
           && ((nextState  = (*transition)()) == NULL))
    {
        ;
    }

    if (nextState != NULL)
    {
        regfgc3_prog_fsm_prop.last_state = regfgc3_prog_fsm_prop.state;
        regfgc3_prog_fsm_prop.state      = nextState(true);
        regfgc3_prog_fsm.state_prog_func = nextState;
    }
    else
    {
        regfgc3_prog_fsm.state_prog_func(false);
    }
}



//! Mode trivial getter.
//!
//! @return uint16_t    The FSM mode.

uint16_t regFgc3ProgFsmGetMode(void)
{
    return regfgc3_prog_fsm_prop.mode;
}



//! State trivial getter.
//!
//! @return uint16_t     The FSM state.

uint16_t regFgc3ProgFsmGetState(void)
{
    return regfgc3_prog_fsm_prop.state;
}



static_assert(FGC_REGFGC3_PROG_FSM_WAITING        == 0, "Unexpected REGFGC3_PROG_FSM state value");
static_assert(FGC_REGFGC3_PROG_FSM_TRANSFERRING   == 1, "Unexpected REGFGC3_PROG_FSM state value");
static_assert(FGC_REGFGC3_PROG_FSM_TRANSFERRED    == 2, "Unexpected REGFGC3_PROG_FSM state value");
static_assert(FGC_REGFGC3_PROG_FSM_GET_PROG_INFO  == 3, "Unexpected REGFGC3_PROG_FSM state value");
static_assert(FGC_REGFGC3_PROG_FSM_PROGRAMMING    == 4, "Unexpected REGFGC3_PROG_FSM state value");
static_assert(FGC_REGFGC3_PROG_FSM_PROG_CHK       == 5, "Unexpected REGFGC3_PROG_FSM state value");
static_assert(FGC_REGFGC3_PROG_FSM_PROGRAMMED     == 6, "Unexpected REGFGC3_PROG_FSM state value");
static_assert(FGC_REGFGC3_PROG_FSM_CLEAN_UP       == 7, "Unexpected REGFGC3_PROG_FSM state value");
static_assert(FGC_REGFGC3_PROG_FSM_ERROR          == 8, "Unexpected REGFGC3_PROG_FSM state value");


//EOF
