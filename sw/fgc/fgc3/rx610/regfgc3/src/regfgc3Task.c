//! @file regfgc3_task.c
//! RegFgc3 task is an OS task that is in charge of carrying out different jobs related to RegFgc3 hardware
//!
//! The task executes the REGFGC3_TASK_SCAN job by default,
//! which scans the RegFgc3 crate.


// ---------- Includes

#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/rx610/inc/fbs.h"
#include "fgc3/rx610/inc/databases.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3Prog.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3ProgFsm.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3Pars.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3Task.h"

#include <defsyms.h>



// ---------- External variable definitions

OS_SEM * regfgc3_task_sem_go;



// ---------- Internal variable definitions

static uint16_t regfgc3_task_todo_mask = REGFGC3_TASK_IDLE;



// ---------- Internal function definitions

//! Performs rescanning of the RegFgc3 crate
//!
//! The operational conditions of the FGC3 are also taken into account:
//! if regFgc3ProgWaitForProgramManager returns false, there is no need to wait for the
//! program manager (device standalone or not operational), and the boards will be switch to production boot.
//! In such a case, it is the converter expert/user who assumes the responsability that the boards contain the
//! correct firmware. The MsTask will check periodically whether all boards switched to production boot on time.
//! In such a case, it will send the RegFgc3 parameters the first time. If not, it will set a fault on the converter.

static void regFgc3TaskRescan(void)
{
	regFgc3ProgScanBoards();

	// @TODO
	// regFgc3ParsBuildCache();
}



// ---------- External function definitions

void regFgc3Task(void * unused)
{
	// Init structures in regFgc3Prog and regFgc3Pars, respectively.

	regFgc3ProgInitHandler();
	regFgc3ParsInit();

	// Scan the crate

	regFgc3TaskSetJob(REGFGC3_TASK_SCAN);

	for (;;)
	{
		while (regfgc3_task_todo_mask == REGFGC3_TASK_IDLE)
		{
			OSSemPend(regfgc3_task_sem_go);
		}

		if (testBitmap(regfgc3_task_todo_mask, REGFGC3_TASK_PROG) != 0)
		{
			// regFgc3ProgFsmProcess();
			// if ((regFgc3ProgFsmGetMode() == regFgc3ProgFsmGetState())
			// 	|| (regFgc3ProgFsmGetState() == FGC_REGFGC3_PROG_FSM_ERROR))
			// {
			// 	clrBitmap(regfgc3_task_todo_mask, REGFGC3_TASK_PROG);
			// }
		}

		if (testBitmap(regfgc3_task_todo_mask, REGFGC3_TASK_SCAN) != 0)
		{
			regFgc3TaskRescan();
			clrBitmap(regfgc3_task_todo_mask, REGFGC3_TASK_SCAN);
		}

		if (testBitmap(regfgc3_task_todo_mask, REGFGC3_TASK_IN_PB) != 0)
		{
			regFgc3ProgBoardsInProduction();
			clrBitmap(regfgc3_task_todo_mask, REGFGC3_TASK_IN_PB);
		}
	}
}



void regFgc3TaskSetJob(enum Regfgc3_task_job_type const job)
{
	regfgc3_task_todo_mask |= job;
}



void regFgc3TaskAwake(void)
{
	OSSemPost(regfgc3_task_sem_go);
}


// EOF