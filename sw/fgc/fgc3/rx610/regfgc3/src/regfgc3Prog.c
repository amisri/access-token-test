//! @file regfgc3Prog.c
//! @brief


// ---------- Includes

#include <math.h>
#include <sleep.h>
#include <string.h>

#include <dallasTask.h>
#include <databases.h>
#include <defprops.h>
#include <defsyms.h>
#include <dimDataProcess.h>
#include <fbs.h>
#include <fbs_class.h>
#include <fgc_errs.h>
#include <init.h>
#include <microlan_1wire.h>
#include <property.h>
#include <regfgc3.h>
#include <regfgc3Prog.h>
#include <regfgc3Task.h>
#include <status.h>
#include <statePc.h>


// ---------- Constants

//TODO: do not expose the driver
#define REGFGC3_TASK_TX_DEVICE      scivs_tx->w[1]
#define REGFGC3_TASK_TX_ACTION_TYPE scivs_tx->w[2]
#define REGFGC3_TASK_TX_BOOT_TYPE   scivs_tx->w[2]
#define REGFGC3_TASK_RX_DATA        scivs_rx->regfgc3.data



// ---------- Internal structures, unions and enumerations

//TODO: unify enum Slot_state and following array in array of structs
enum Slot_state
{
    SLOT_STATE_EMPTY,
    SLOT_STATE_PB,
    SLOT_STATE_DB,
    SLOT_STATE_FAILED,
    SLOT_N_STATE
};

char * slot_state_to_name[SLOT_N_STATE] =
{
    "Empty",
    "ProductionBoot",
    "DownloadBoot",
    "Failed"
};

enum Regfgc3_prog_boot_type
{
    REGFGC3_PROG_BOOT_DB = 0x00, //!< Download boot
    REGFGC3_PROG_BOOT_PB = 0x01  //!< Production boot
};

struct Regfgc3_prog
{
    struct   Regfgc3_slice_params slice;
    bool     switch_to_pb_issued;
    bool     switch_to_pb_failed;
};



// ---------- External variable definitions

struct Regfgc3_prog_prop regfgc3_prog_prop __attribute__((section("sram")));



// ---------- Internal variable definitions

//<!
static struct Regfgc3_prog regfgc3_prog __attribute__((section("sram")));



// ---------- Internal function definitions

//!
//!
//! @param slot
//! @param boot_type
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgTaskSwitchBoot(uint8_t const slot, enum Regfgc3_prog_boot_type const boot_type)
{
    enum Scivs_error_code error_code;

    REGFGC3_TASK_TX_BOOT_TYPE = boot_type;
    error_code = scivsTaskSend(SCIVS_TASK_SWITCH_BOOT, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);

    // Switching programs prevents the card to send an SCIVS_OK code to the FGC3.
    // The returned code is the one checked here.
    if (   (error_code != SCIVS_ERR_MASTER_TRANSFER_IN_PROGRESS)
        && (error_code != SCIVS_ERR_MASTER_BUSY))
    {
        return error_code;
    }

    return SCIVS_OK;
}



//TODO: erase after commissioning/testing?

static enum Scivs_error_code regFgc3ProgSwitchToDB(uint8_t const slot)
{
    return regFgc3ProgTaskSwitchBoot(slot, REGFGC3_PROG_BOOT_DB);
}



//! @brief
//!
//! @param slot
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgSwitchToPB(uint8_t const slot)
{
    return regFgc3ProgTaskSwitchBoot(slot, REGFGC3_PROG_BOOT_PB);
}



//! @brief
//!
//! @param slot
//! @param device
//! @param slice
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgSliceActionProgram(uint8_t const slot, enum Scivs_device const device, uint16_t const slice)
{
    enum Scivs_error_code error_code;
    uint16_t timeout_program = regfgc3_prog.slice.time_program;

    REGFGC3_TASK_TX_DEVICE      = device;
    REGFGC3_TASK_TX_ACTION_TYPE = SCIVS_TASK_SLICE_ACTION_PROGRAM;
    scivs_tx->w[3]              = slice;

    error_code = scivsTaskSend(SCIVS_TASK_SLICE_ACTION, slot, SCIVS_MF, timeout_program);

    return error_code;
}




//! @brief
//!
//! @param slot
//! @param device
//! @param slice
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgSliceActionReadBack(uint8_t const slot, enum Scivs_device const device, uint8_t const slice)
{
#if 0
    uint16_t write_address;
    uint16_t read_address;
    enum Scivs_error_code error_code;
    uint16_t i;

    REGFGC3_TASK_TX_DEVICE = device;
    REGFGC3_TASK_TX_ACTION_TYPE = SCIVS_TASK_SLICE_ACTION_READBACK;
    scivs_tx->w[3] = slice;

    error_code = scivsTaskSend(SCIVS_TASK_SLICE_ACTION, slot, SCIVS_MF, regfgc3_prog.slice.time_readback);

    if (error_code != SCIVS_OK)
    {
        return error_code;
    }

    for (i = 0; i < regfgc3_prog.slice.number_batches; i++)
    {
        // set addresses for reading
        write_address = SCIVS_SLICE_WRITE + 2 * i * SCIVS_MASTER_FRAME_W;
        read_address  = SCIVS_SLICE_WRITE + 2 * i * SCIVS_MASTER_FRAME_W;
        error_code = scivsWriteCustom(slot, write_address, SCIVS_NO_WRITE, read_address, SCIVS_MASTER_FRAME_W);

        if (error_code != SCIVS_OK)
        {
            return error_code;
        }

        sleepMs(2);
    }
#endif
    return SCIVS_OK;
}



//! @brief
//!
//! @param slot
//! @param device
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgSliceActionGetSlicePars(uint8_t const slot, enum Scivs_device const device)
{
    enum Scivs_error_code error_code;

    const struct Regfgc3_slice_params * const slice_pars = (struct Regfgc3_slice_params *) REGFGC3_TASK_RX_DATA;

    REGFGC3_TASK_TX_DEVICE       = device;
    REGFGC3_TASK_TX_ACTION_TYPE  = SCIVS_TASK_SLICE_ACTION_GETPARS;

    error_code = scivsTaskSend(SCIVS_TASK_SLICE_ACTION, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);
    
    if (error_code != SCIVS_OK)
    {
        return error_code;
    }

    memcpy((void *) &regfgc3_prog.slice, (const void *) slice_pars, sizeof(struct Regfgc3_slice_params));

    //TODO: Added just to commission. Remove afterwards.
    regfgc3_prog_prop.debug.slice_bytes   = regfgc3_prog.slice.number_bytes;
    regfgc3_prog_prop.debug.slice_batches = regfgc3_prog.slice.number_batches;
    regfgc3_prog_prop.debug.time_crc      = regfgc3_prog.slice.time_crc;
    regfgc3_prog_prop.debug.time_params   = regfgc3_prog.slice.time_parameters;
    regfgc3_prog_prop.debug.time_prog     = regfgc3_prog.slice.time_program;

    return error_code;
}



//! @brief
//!
//! @param slot
//! @param device
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgSliceActionEnableChangeDb(const uint16_t slot, const uint16_t device)
{
    if (device != SCIVS_DB)
    {
        return SCIVS_OK;
    }

    REGFGC3_TASK_TX_DEVICE      = device;
    REGFGC3_TASK_TX_ACTION_TYPE = SCIVS_TASK_SLICE_ACTION_ENABLE_CHANGE_DB;

    return scivsTaskSend(SCIVS_TASK_SLICE_ACTION, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);
}



//! @brief
//!
//! @param slot
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgTaskAbort(uint8_t const slot)
{
    enum Scivs_error_code error_code;

    error_code = scivsTaskSend(SCIVS_TASK_ABORT, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);

    return error_code;
}



//! @brief

static void regFgc3ProgCleanProgData(void)
{
    memset(&regfgc3_prog_prop.prog_data, 0, sizeof(struct Regfgc3_prog_prog_data));
}



//! @brief

static void regFgc3ProgCleanDspMem(void)
{
    // memset((void *) SDRAM_REGFGC3_BIN_32, 0, regfgc3_prog_prop.prog_data.bin_size_bytes);
}



//! @brief
//!
//! @return uint16_t

static uint16_t regFgc3ProgGetState(void)
{
    return regfgc3_prog_prop.prog_data.state;
}



//! @brief
//!
//! @param type
//! @return int16_t

static int16_t regFgc3ProgGetBoardSlot(const uint16_t type)
{
    uint8_t i;

    for (i = 0; i < FGC_SCIVS_N_SLOT && regfgc3_prog_prop.detected[i].type != type; i++)
    {
        ; // Do nothing
    }

    return (i < FGC_SCIVS_N_SLOT ? i : -1);
}



//! @brief
//!
//! @param slot
//! @param device
//! @return uint16_t

static uint16_t regFgc3ProgGetVariant(uint8_t const slot, enum Scivs_device const device)
{
    return regfgc3_prog_prop.detected[slot].devices[device].variant;
}



//! @brief
//!
//! @param variant_number
//! @return char*

static char *regFgc3ProgGetVariantName(uint16_t const variant_number)
{
    char     * variant_name;
    uint16_t   i;


    for (i = 0; sym_names_regfgc3_variants[i].label != (char *)0 && sym_names_regfgc3_variants[i].type != variant_number; ++i)
    {
        ;
    }

    if (sym_names_regfgc3_variants[i].label == (char *)0)
    {
        variant_name = "UNKNOWN";
    }
    else
    {
        variant_name = sym_names_regfgc3_variants[i].label;
    }

    return variant_name;
}



//! @brief
//!
//! @param slot

static void regFgc3ProgGetProgrammableBoardData(uint8_t const slot)
{
    enum Scivs_error_code error_code;

    struct Regfgc3_general_constants_ext const * const gen_const_ext =
        (struct Regfgc3_general_constants_ext *)scivs_rx->regfgc3.data;

    error_code = scivsTaskSend(SCIVS_TASK_GENERAL_CONST_EXT, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);

    if (error_code != SCIVS_OK)
    {
        return;
    }

    // Set board type

    regfgc3_prog_prop.detected[slot].type           = gen_const_ext->board_type;
    regfgc3_prog_prop.detected[slot].reprogrammable = true;

    // Gather devices information

    const struct Regfgc3_device_data_ext *dev_data_ptr = &gen_const_ext->device_0;

    for (uint8_t i = 0; i <= gen_const_ext->installed_devices; ++i)
    {
        regfgc3_prog_prop.detected[slot].devices[i].name             = dev_data_ptr->dev_data.device_type;
        regfgc3_prog_prop.detected[slot].devices[i].variant          = dev_data_ptr->dev_data.variant;
        regfgc3_prog_prop.detected[slot].devices[i].variant_revision = dev_data_ptr->dev_data.variant_revision;
        regfgc3_prog_prop.detected[slot].devices[i].api_revision     = dev_data_ptr->dev_data.api_revision;

        dev_data_ptr++;
    }
}



//! @brief
//!
//! @param slot
//! @param general_constants

static void regFgc3ProgGetNonProgrammableBoardData(uint8_t const slot, struct Regfgc3_general_constants const * const general_constants)
{
    if (general_constants->fpga_board_type == FGC_REGFGC3_BOARDS_NO_BOARD)
    {
        regfgc3_prog_prop.detected[slot].slot_state = SLOT_STATE_EMPTY;
        return;
    }

    regfgc3_prog_prop.detected[slot].type = general_constants->fpga_board_type;

    regfgc3_prog_prop.detected[slot].devices[SCIVS_MF].name             = FGC_REGFGC3_DEVICES_MF;

    regfgc3_prog_prop.detected[slot].devices[SCIVS_MF].variant          = general_constants->fpga_program_variant;
    regfgc3_prog_prop.detected[slot].devices[SCIVS_MF].variant_revision = general_constants->fpga_program_variant_revision;
    regfgc3_prog_prop.detected[slot].devices[SCIVS_MF].api_revision     = general_constants->fpga_fgc3cvm_api_revision;

    if (general_constants->dsp_program_variant != FGC_REGFGC3_BOARDS_NO_BOARD)
    {
        regfgc3_prog_prop.detected[slot].devices[SCIVS_DEVICE_2].name             = FGC_REGFGC3_DEVICES_DEVICE_2;

        regfgc3_prog_prop.detected[slot].devices[SCIVS_DEVICE_2].variant          = general_constants->dsp_program_variant;
        regfgc3_prog_prop.detected[slot].devices[SCIVS_DEVICE_2].variant_revision = general_constants->dsp_program_variant_revision;
        regfgc3_prog_prop.detected[slot].devices[SCIVS_DEVICE_2].api_revision     = general_constants->dsp_basemodule_api_revision;
    }
}



//! @brief
//!
//! @param slot

static void regFgc3ProgUpdateSlotStatus(uint8_t const slot)
{
    if (regFgc3ProgIsBoardInDB(slot) == true)
    {
        regfgc3_prog_prop.detected[slot].slot_state = SLOT_STATE_DB;
    }
    else
    {
        regfgc3_prog_prop.detected[slot].slot_state = SLOT_STATE_PB;
    }
}



//! @brief
//!
//! @param slot

static void regFgc3ProgGetBoardInfo(uint8_t const slot)
{
    enum Scivs_error_code error_code;

    struct Regfgc3_general_constants const *const general_constants = (struct Regfgc3_general_constants *)scivs_rx->regfgc3.data;

    error_code = scivsTaskSend(SCIVS_TASK_GENERAL_CONST, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);

    if (error_code != SCIVS_OK)
    {
        // If a board was detected previously, mark slot as failed but do not reset detected information

        if (regfgc3_prog_prop.detected[slot].slot_state != SLOT_STATE_EMPTY)
        {
            regfgc3_prog_prop.detected[slot].slot_state = SLOT_STATE_FAILED;
        }

        return;
    }

    regfgc3_prog_prop.detected[slot].mezzanine_id = general_constants->mezzanine_id;

    if (   general_constants->fpga_program_variant == FGC_REGFGC3_VARIANTS_DOWNLDBOOT_3
        || regfgc3_prog_prop.detected[slot].reprogrammable == true)
    {
        regFgc3ProgGetProgrammableBoardData(slot);
    }
    else
    {
        regFgc3ProgGetNonProgrammableBoardData(slot, general_constants);
    }

    regFgc3ProgUpdateSlotStatus(slot);
}



//! @brief
//!
//! @param slot

static void regFgc3ProgTaskGetHeader(uint8_t const slot)
{
    static uint8_t const HEADER_TASK_TIMEOUT_MS = 3;
    static uint8_t const HEADER_LENGTH         = 128;

    enum Scivs_error_code error_code;

    for (uint8_t device = SCIVS_DB; device < SCIVS_N_DEVICE; ++device)
    {
        if (regfgc3_prog_prop.detected[slot].devices[device].name == FGC_REGFGC3_DEVICES_NO_DEVICE)
        {
            continue;
        }

        REGFGC3_TASK_TX_DEVICE      = device;
        REGFGC3_TASK_TX_ACTION_TYPE = SCIVS_TASK_SLICE_ACTION_NONE;

        error_code = scivsTaskSend(SCIVS_TASK_GET_HEADER, slot, SCIVS_MF, HEADER_TASK_TIMEOUT_MS);

        if (error_code == SCIVS_OK)
        {
            struct Regfgc3_header const * header     = (struct Regfgc3_header *)scivs_rx->regfgc3.data;
            char                  const * header_ptr = header->data;
            char                  const * header_end = header_ptr + HEADER_LENGTH;
            uint8_t                       delim_cnt  = 0;

            // Field:    Header creation time UTC
            // Length:   10
            // Position:  9

            while (header_ptr < header_end && delim_cnt < 8)
            {
                if (*header_ptr++ == ';')
                {
                    delim_cnt++;
                }
            }

            regfgc3_prog_prop.detected[slot].devices[device].header_utc[0] = '\0';

            if (delim_cnt == 8)
            {
                strncpy(regfgc3_prog_prop.detected[slot].devices[device].header_utc, header_ptr, 10);
                regfgc3_prog_prop.detected[slot].devices[device].header_utc[10] ='\0';
            }

            // Field:     Git Hash
            // Length:    8
            // Position: 10

            while (header_ptr < header_end && delim_cnt < 9)
            {
                if (*header_ptr++ == ';')
                {
                    delim_cnt++;
                }
            }

            regfgc3_prog_prop.detected[slot].devices[device].git_sha[0] ='\0';

            if (delim_cnt == 9)
            {
                strncpy(regfgc3_prog_prop.detected[slot].devices[device].git_sha, header_ptr, 8);
                regfgc3_prog_prop.detected[slot].devices[device].git_sha[8] ='\0';
            }
        }
    }
}



//! @brief
//!
//! @param slot
//! @param slice_number
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgTransferSingleSlice(uint8_t const slot, uint16_t const slice_number)
{
    uint8_t  i;
    uint32_t j;
    uint16_t write_address;
    uint16_t read_address;
    uint16_t error_code;
    uint16_t shuffled_word;
    uint32_t * const binary_word;

    for (i = 0; i < regfgc3_prog.slice.number_batches; i++)
    {
        uint32_t first_el_batch = (slice_number * regfgc3_prog.slice.number_bytes +
                                   i * SCIVS_MASTER_FRAME_W * 2) / 4;
        uint32_t last_el_batch  = first_el_batch + SCIVS_MASTER_FRAME_W / 2;

        // Build payload of 64 x 16-bit words
        uint8_t word_in_tx_buffer = 0;

        for (j = first_el_batch; j < last_el_batch; j++)
        {
            error_code = PropValueGet(&fcm, &PROP_REGFGC3_PROG_BIN, NULL, j, (uint8_t **)&binary_word);

            if (error_code != FGC_OK_NO_RSP)
            {
                return error_code;
            }

            shuffled_word = ((*binary_word & 0xFF000000) >> 24) | ((*binary_word & 0x00FF0000) >> 8);
            scivs_tx->w[word_in_tx_buffer] = shuffled_word;

            shuffled_word = ((*binary_word & 0x0000FF00) >> 8) | ((*binary_word & 0x000000FF) << 8);
            scivs_tx->w[word_in_tx_buffer + 1] = shuffled_word;

            word_in_tx_buffer += 2;
        }

        // Send batch

        write_address = SCIVS_SLICE_WRITE + 2 * i * SCIVS_MASTER_FRAME_W;
        read_address  = SCIVS_SLICE_WRITE + 2 * i * SCIVS_MASTER_FRAME_W;

        error_code = scivsWriteCustom(slot, write_address, SCIVS_MASTER_FRAME_W, read_address, SCIVS_MASTER_FRAME_W);

        if (error_code != SCIVS_OK)
        {
            return error_code;
        }

        regfgc3_prog_prop.stats.batches_sent[slot]++;
    }

    return SCIVS_OK;
}



//! @brief
//!
//! @param slot
//! @param device
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgTransferSlices(uint8_t const slot, enum Scivs_device const device)
{
    static uint8_t const SLICE_MAX_TX_ATTEMPTS = 2;
    uint16_t i;
    uint8_t j;
    enum Scivs_error_code error_code;

    uint32_t binary_size_bytes = regfgc3_prog_prop.prog_data.bin_size_bytes;
    uint16_t bytes_per_slice   = regfgc3_prog.slice.number_bytes;
    uint16_t number_of_slices  = ceil((float)binary_size_bytes / (float)bytes_per_slice);

    for (i = 0; i < number_of_slices; i++)
    {
        for (j = 0; j < SLICE_MAX_TX_ATTEMPTS; j++)
        {
            if ((error_code = regFgc3ProgTransferSingleSlice(slot, i)) == SCIVS_OK)
            {
                regfgc3_prog_prop.stats.slices_sent[slot]++;
                break;
            }
        }

        if (j != 0)
        {
            statusSetWarnings(FGC_WRN_PROG_MGR);
        }

        if (j == SLICE_MAX_TX_ATTEMPTS)
        {
            // Issue error here. Mark converter not operational
            statusSetFaults(FGC_FLT_FGC_HW);

            // Abort operation and return
            error_code = regFgc3ProgTaskAbort(slot);
            return error_code;
        }

        if ((error_code = regFgc3ProgSliceActionEnableChangeDb(slot, device)) != SCIVS_OK)
        {
            return error_code;
        }

        if ((error_code = regFgc3ProgSliceActionProgram(slot, device, i)) != SCIVS_OK)
        {
            return error_code;
        }

        regfgc3_prog_prop.stats.slices_programmed[slot]++;

        if ((error_code = regFgc3ProgSliceActionReadBack(slot, device, i)) != SCIVS_OK)
        {
            ;
        }
    }

    return SCIVS_OK;
}



//! @brief
//!
//! @param slot
//! @param device
//! @return enum Scivs_error_code

static enum Scivs_error_code regFgc3ProgVerifyProdBoot(uint8_t const slot, enum Scivs_device const device)
{
    static const uint32_t MASK_16_BITS = 0xFFFF0000;

    enum Scivs_error_code error_code;
    uint16_t crc_timeout = regfgc3_prog.slice.time_crc;

    // Prepare TX buffer
    REGFGC3_TASK_TX_ACTION_TYPE = SCIVS_TASK_SLICE_ACTION_CALCCRC;
    scivs_tx->w[3] = (uint16_t)(regfgc3_prog_prop.prog_data.bin_size_bytes >> 16);
    scivs_tx->w[4] = (uint16_t)(regfgc3_prog_prop.prog_data.bin_size_bytes & ~MASK_16_BITS);

    if ((error_code = scivsTaskSend(SCIVS_TASK_SLICE_ACTION, slot, SCIVS_MF, crc_timeout)) != SCIVS_OK)
    {
        return scivsConvertToFgcErr(error_code);
    }

    uint16_t calc_crc = scivs_rx->w[2];

    if (calc_crc != regfgc3_prog_prop.prog_data.bin_crc)
    {
        // Put the CRC returned from the board into the property to have the value
        // available in case this step fails
        regfgc3_prog_prop.prog_data.bin_crc = calc_crc;
        return SCIVS_ERR_SLAVE_CRC_ERR;
    }

    return FGC_OK_NO_RSP;
}



//! Returns whether the program manager has finished doing its job
//!
//! @return true  If the program manager has finished
//! @return false If the program manager has not finished yet

static bool regFgc3ProgProgramManagerFinished(void)
{
    return (  regfgc3_prog_prop.prog_data.manager == FGC_CTRL_DISABLED 
            ? regFgc3ProgAllBoardsInPb()
            : (   regfgc3_prog_prop.prog_data.state == FGC_REGFGC3_PROG_STATE_SYNCHRONIZED
               || regfgc3_prog_prop.prog_data.mode  == FGC_REGFGC3_PROG_MODE_PROG_WARNING));
 }



//! Switches boards to production boot.
//!
//! Called from the RegFgc3Task if the FGC3 is standalone or not operational; or
//! called by the program manager if it can restart the FGC3 successfully after reprogramming
//! boards or not.

static void regFgc3ProgSwitchAllBoardsToPb(void)
{
    for (uint8_t slot = 0; slot < FGC_SCIVS_N_SLOT; ++slot)
    {
        if (regFgc3ProgIsBoardInDB(slot))
        {
            regFgc3ProgSwitchToPB(slot);
        }
    }

    regfgc3_prog.switch_to_pb_issued = true;
}



// ---------- External function definitions

void regFgc3ProgSetJobInPb(void)
{
    regFgc3TaskSetJob(REGFGC3_TASK_IN_PB);
    regFgc3TaskAwake();
}



void regFgc3ProgBoardsInProduction(void)
{
    static const uint8_t LIMIT_ITERS_500_MS   = 20;
    static       uint8_t n_iters_500_ms       = 0;
    static       bool    boards_in_pb         = false;

    // Execute this function only after issuing the command to switch to
    // production boot and if at least one board has not reached this state.

    if (regfgc3_prog.switch_to_pb_issued == false || boards_in_pb == true)
    {
        return;
    }

    if (statusTestFaults(FGC_FLT_FGC_HW) == true)
    {
        if (regfgc3_prog.switch_to_pb_failed == true)
        {
            regfgc3_prog.switch_to_pb_failed = false;
            n_iters_500_ms = 0;
        }

        return;
    }

    // Set fault if first time time-out and return
    
    if (n_iters_500_ms >= LIMIT_ITERS_500_MS)
    {
        regfgc3_prog.switch_to_pb_failed = true;

        return;
    }

    // After issuing toPbBoot, we have to rescan the crate

    regFgc3ProgScanBoards();

    // Send parameters when there is no need to wait for the PM, or after it has finished

    if (   regFgc3ProgWaitForProgramManager() == false
        || (   regFgc3ProgWaitForProgramManager()  == true
            && regFgc3ProgProgramManagerFinished() == true))
    {
        if (regFgc3ProgAllBoardsInPb() == true)
        {
            regFgc3ParsSendParamsToHw();

            DimResetExpectedDetectedMismatches();
            statePcResetFaults();
            dallasTaskRegFgc3Ready();

            boards_in_pb = true;
        }
        else
        {
            n_iters_500_ms++;
        }
    }

    initClassPostRegFgc3Cache();
}



bool regFgc3ProgThereIsProgFault(void)
{
    return (regfgc3_prog_prop.prog_data.mode == FGC_REGFGC3_PROG_MODE_PROG_FAILED);
}



bool regFgc3ProgSwitchProductionFailed(void)
{
    return regfgc3_prog.switch_to_pb_failed;
}



void regFgc3ProgSetMode(const uint16_t mode)
{
    regfgc3_prog_prop.prog_data.mode = mode;

    // Clear the SYNC_REGFGC3, as no matter what happened,
    // we probably do not want the program manager to repeat the same action
    statusClrUnlatched(FGC_UNL_SYNC_PROG_MGR);

    // If successful, clear the program warning.
    if (mode == FGC_REGFGC3_PROG_MODE_PROG_NONE)
    {
        statusClrWarnings(FGC_WRN_PROG_MGR);
    }

    if (mode == FGC_REGFGC3_PROG_MODE_PROG_WARNING)
    {
        statusSetWarnings(FGC_WRN_PROG_MGR);
    }

    // If unsuccessful, set leave programming warning and set fault (HW).
    // Send parameters to boards in production boot mode already.
    if (mode == FGC_REGFGC3_PROG_MODE_PROG_FAILED)
    {
        statusSetWarnings(FGC_WRN_PROG_MGR);

        // This function will only send parameters to boards in PB mode already.
        regFgc3ProgSetJobInPb();
    }
}



void regFgc3ProgSetState(const uint16_t state)
{
    regfgc3_prog_prop.prog_data.state = state;

    if (regfgc3_prog_prop.prog_data.state == FGC_REGFGC3_PROG_STATE_SYNCHRONIZED)
    {
        regFgc3ProgSwitchAllBoardsToPb();
        // After this, the mst will send the parameters to the cards when ready.
    }
}



void regFgc3ProgInitHandler(void)
{
    uint8_t i;
    uint8_t j;

    // Initialize all to zero

    memset(&regfgc3_prog_prop.detected, 0, FGC_SCIVS_N_SLOT * sizeof(struct Regfgc3_prog_slot_info));

    // Except devices' name to "NO_DEVICE" (zero value is taken for the downloadboot)

    for (i = 0; i < FGC_SCIVS_N_SLOT; i++)
    {
        for (j = 0; j < SCIVS_N_DEVICE; j++)
        {
            regfgc3_prog_prop.detected[i].devices[j].name = FGC_REGFGC3_DEVICES_NO_DEVICE;
        }
    }

    // Initializes the FGC3 prog state to lock
    regfgc3_prog_prop.prog_data.lock = FGC_REGFGC3_PROG_LOCK_ENABLED;
}



void regFgc3ProgMgrInit(void)
{
    regfgc3_prog.switch_to_pb_issued = false;
    regfgc3_prog.switch_to_pb_failed = false;

    if (regFgc3ProgProgramManagerFinished() == true)
    {
        regFgc3ParsSendParamsToHw();
        dallasTaskRegFgc3Ready();
        return;
    }

    if (testBitmap(deviceGetOmodeMask(), FGC_LHC_SECTORS_NO_PROG_MGR) == false)
    {
        regfgc3_prog_prop.prog_data.state = FGC_REGFGC3_PROG_STATE_UNSYNCED;

        if (regfgc3_prog_prop.prog_data.manager == FGC_CTRL_ENABLED)
        {
            statusSetWarnings(FGC_WRN_PROG_MGR);
            statusSetUnlatched(FGC_UNL_SYNC_PROG_MGR);
        }
        else
        {
            statusClrWarnings(FGC_WRN_PROG_MGR);
            statusClrUnlatched(FGC_UNL_SYNC_PROG_MGR);
        }
    }
    else
    {
        regfgc3_prog_prop.prog_data.state = FGC_REGFGC3_PROG_STATE_STANDALONE;
        statusClrUnlatched(FGC_UNL_SYNC_PROG_MGR);
    }

    if (regFgc3ProgWaitForProgramManager() == false)
    {
        regFgc3ProgSwitchAllBoardsToPb();
    }
}



uint64_t regFgc3ProgGetMezzanineId(uint8_t const slot)
{
    return regfgc3_prog_prop.detected[slot].mezzanine_id;
}



void regFgc3ProgScanBoards(void)
{
    uint8_t slot;

    for (slot = 0; slot < FGC_SCIVS_N_SLOT; ++slot)
    {
        regFgc3ProgGetBoardInfo(slot);
        regFgc3ProgTaskGetHeader(slot);
        regFgc3ParsScanBlocks(slot);
    }

    // If there is no BIS card, ignore all the BIS channels.

    if (regFgc3ProgGetBoardSlot(FGC_REGFGC3_BOARDS_VS_BIS_INTK) == -1 && regFgc3ProgGetBoardSlot(FGC_REGFGC3_BOARDS_VS_BIS_INTK_2) == -1)
    {
        setBitmap(dpcom.mcu.log.ignore_mask, DPCOM_LOG_IGNORE_BIS_BIT_MASK);
    }

    // Access to the white-rabbit data (B_MEAS) only if the card is present

    int16_t fmc_slot = regFgc3ProgGetBoardSlot(FGC_REGFGC3_BOARDS_VS_EX_FMC_POF);

    dpcom.mcu.wr.enabled = (fmc_slot != -1 && regFgc3ProgGetVariant(fmc_slot, SCIVS_MF) == FGC_REGFGC3_VARIANTS_EX_FMC_WR_170);
}



void regFgc3ProgSlotPrint(FILE *fd, char const delim, uint8_t const from, uint8_t const to)
{
    uint8_t slot;
    uint8_t device;
    uint16_t name;

    for (slot = from; slot <= to; slot++)
    {
        if (regfgc3_prog_prop.detected[slot].type == FGC_REGFGC3_BOARDS_NO_BOARD)
        {
            continue;
        }

        fprintf(fd, "%s%c", "------------------------------", delim);
        fprintf(fd, "%-10s %d%c", "SLOT",  slot, delim);
        fprintf(fd, "%-10s %s%c", "BOARD", regFgc3ProgGetBoardName(regfgc3_prog_prop.detected[slot].type), delim);
        fprintf(fd, "%-10s %s%c", "STATE", slot_state_to_name[regfgc3_prog_prop.detected[slot].slot_state], delim);

        for (device = SCIVS_DB; device < SCIVS_N_DEVICE; device++)
        {
            name = regfgc3_prog_prop.detected[slot].devices[device].name;

            if (name == FGC_REGFGC3_DEVICES_NO_DEVICE || name == FGC_REGFGC3_DEVICES_DB)
            {
                continue;
            }

            // The field names MUST not contain spaces. The Program Manager splits each row in REGFGC3.SLOT_INFO
            // using spaces as delimeter to construct a dictionary. This requires two tokens only. Having a space
            // in the field name would result in 3 tokens, which results in the Program Manager throwing an
            // exception. That is why underscores'_' are used instead.

            fprintf(fd, "%-10s %s%c", "Device",  scivs_devices_names[device], delim);
            fprintf(fd, "%-10s %s%c", "Variant", regFgc3ProgGetVariantName(regfgc3_prog_prop.detected[slot].devices[device].variant), delim);
            fprintf(fd, "%-10s %d%c", "Var_rev", regfgc3_prog_prop.detected[slot].devices[device].variant_revision, delim);
            fprintf(fd, "%-10s %d%c", "API_rev", regfgc3_prog_prop.detected[slot].devices[device].api_revision, delim);
            fprintf(fd, "%-10s %s%c", "UTC",     regfgc3_prog_prop.detected[slot].devices[device].header_utc, delim);
            fprintf(fd, "%-10s %s%c", "Git_sha", regfgc3_prog_prop.detected[slot].devices[device].git_sha, delim);
            fprintf(fd, "%c", delim);
        }
    }
}



bool regFgc3ProgWaitForProgramManager(void)
{
    return (   regFgc3ProgGetState() != FGC_REGFGC3_PROG_STATE_STANDALONE
            && regfgc3_prog_prop.prog_data.manager == FGC_CTRL_ENABLED);
}



uint16_t regFgc3ProgGetOpState(void)
{
    return regfgc3_prog_prop.prog_data.lock;
}



uint16_t regFgc3ProgGetSlicingInfo(void)
{
    enum Scivs_error_code error_code;
    uint8_t slot = regfgc3_prog_prop.prog_data.slot;
    uint8_t device = regfgc3_prog_prop.prog_data.device;

    if ((error_code = regFgc3ProgSliceActionGetSlicePars(slot, device)) == SCIVS_TSK_ERR_INVALID_OP)
    {
        // Card not prepared for reprogramming: do something special for this error?
        regfgc3_prog_prop.debug.board_error = error_code;
        regFgc3ProgTaskAbort(slot);
        return error_code;
    }
    // This could be another type of error in response to the previous action
    if (error_code != SCIVS_OK)
    {
        regfgc3_prog_prop.debug.board_error = error_code;
        regFgc3ProgTaskAbort(slot);
        return error_code;
    }

    return FGC_OK_NO_RSP;
}



uint16_t regFgc3ProgReprogram(void)
{
    enum Scivs_error_code error_code;
    uint8_t slot   = regfgc3_prog_prop.prog_data.slot;
    uint8_t device = regfgc3_prog_prop.prog_data.device;

    if ((error_code = regFgc3ProgTransferSlices(slot, device)) != SCIVS_OK)
    {
        regfgc3_prog_prop.debug.board_error = error_code;
        regFgc3ProgTaskAbort(slot);
        return error_code;
    }

    return FGC_OK_NO_RSP;
}



uint16_t regFgc3ProgVerifyProgram(void)
{
    enum Scivs_error_code error_code;
    uint8_t slot   = regfgc3_prog_prop.prog_data.slot;
    uint8_t device = regfgc3_prog_prop.prog_data.device;

    if ((error_code = regFgc3ProgVerifyProdBoot(slot, device)) != SCIVS_OK)
    {
        regfgc3_prog_prop.debug.board_error = error_code;
        regFgc3ProgTaskAbort(slot);
        return error_code;
    }

    return FGC_OK_NO_RSP;
}



void regFgc3ProgClean(void)
{
    regFgc3ProgCleanProgData();

    regFgc3ProgCleanDspMem();
}



bool regFgc3ProgIsBoardPresent(uint8_t const slot)
{
    return regfgc3_prog_prop.detected[slot].type != FGC_REGFGC3_BOARDS_NO_BOARD;
}



bool regFgc3ProgIsDevicePresent(uint8_t const slot, const uint8_t device)
{
    return regfgc3_prog_prop.detected[slot].devices[device].name != FGC_REGFGC3_DEVICES_NO_DEVICE;
}



char *regFgc3ProgGetBoardName(const uint16_t board_type)
{
    char     * board_name;
    uint16_t   i;

    for (i = 0; sym_names_regfgc3_boards[i].label != (char *)0 && sym_names_regfgc3_boards[i].type != board_type; i++)
    {
        ; // Do nothing
    }

    if (sym_names_regfgc3_boards[i].label == (char *)0)
    {
        board_name = "UNKNOWN";
    }
    else
    {
        board_name = sym_names_regfgc3_boards[i].label;
    }

    return board_name;
}


//TODO: maybe not necessary from pars.
bool regFgc3ProgIsBoardInDB(uint8_t const slot)
{
    return (regfgc3_prog_prop.detected[slot].devices[SCIVS_DB].variant == FGC_REGFGC3_VARIANTS_DOWNLDBOOT_3);
}



bool regFgc3ProgAllBoardsInPb(void)
{
    uint8_t i;
    for (i = 0; i < FGC_SCIVS_N_SLOT; i++)
    {
        if (   regfgc3_prog_prop.detected[i].reprogrammable == true
            && regfgc3_prog_prop.detected[i].slot_state != SLOT_STATE_PB)
        {
            break;
        }
    }

    return (i == FGC_SCIVS_N_SLOT);
}



// Added temporarily for PM commissioning
uint16_t regFgc3ProgSetAction(const uint16_t action)
{
    enum Scivs_error_code error_code = SCIVS_OK;

    switch (action)
    {
    case 1: // SWITCH BOOT
        if (regFgc3ProgIsBoardInDB(regfgc3_prog_prop.prog_data.slot) == true)
        {
            error_code = regFgc3ProgSwitchToPB(regfgc3_prog_prop.prog_data.slot);
        }
        else
        {
            error_code = regFgc3ProgSwitchToDB(regfgc3_prog_prop.prog_data.slot);
        }
        break;

    case 2: // READ SLICE PARAMETERS

        regfgc3_prog_prop.debug.slice_bytes = 0;
        regfgc3_prog_prop.debug.slice_batches = 0;
        regfgc3_prog_prop.debug.time_crc = 0;
        regfgc3_prog_prop.debug.time_prog = 0;
        regfgc3_prog_prop.debug.time_params = 0;

        error_code = regFgc3ProgSliceActionGetSlicePars(regfgc3_prog_prop.prog_data.slot, regfgc3_prog_prop.prog_data.device);

        break;

    case 3: // RESET SCIVS BUS

      scivsInit();
      break;

    default:
        break;
    }

    return error_code;
}


// EOF
