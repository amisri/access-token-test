//! @file  scivs.c
//! @brief Implementation of the SCIVS communication


// ---------- Includes

#include <sleep.h>

#include <fgc_errs.h>
#include <mcuDependent.h>
#include <scivs.h>



// ---------- Constants

//! Shift to access counter in GMC header (first 16-bit word in header: 4 MSB -> counter; 12 LSB -> task code)

static uint8_t const SCIVS_HEADER_COUNTER_SHIFT16 = 12;



// ---------- Internal structures, unions and enumerations

//!

struct Rw_addresses
{
    enum Scivs_wb_operation_addr write_address;
    enum Scivs_wb_operation_addr read_address;
};

//!

static struct Rw_addresses Device_addresses[SCIVS_N_DEVICE + 1] =
{
    { SCIVS_NO_WRITE,   SCIVS_NO_WRITE  },
    { SCIVS_FPGA_WRITE, SCIVS_FPGA_READ },
    { SCIVS_DSP_WRITE,  SCIVS_DSP_READ  },
    { SCIVS_NO_WRITE,   SCIVS_NO_WRITE  }
};



// ---------- External variable definitions

char * scivs_devices_names[SCIVS_N_DEVICE] =
{
    "DB",
    "MF",
    "DEVICE_2"
};

volatile union Scivs_tx_packet       * const scivs_tx = (union Scivs_tx_packet *) SCIVS_MCU_BUF_32;
volatile union Scivs_rx_packet const * const scivs_rx = (union Scivs_rx_packet *) SCIVS_SCI_BUF_32;



// ---------- Internal variable definitions

//! Counts the frames sent to a given slot. Reply from card should also contain the same number
//! to be considered an error free transaction

static uint8_t scivs_transmissions_counter[FGC_SCIVS_N_SLOT];



// ---------- Internal function definitions

//! Reset the SCIVS
//! Send a reset on SCIVS bus. This will reset all the boards on the crate.
//! Note: a jumper can physically disable the SCIVS reset on the cards, for development only.

static void scivsReset(void)
{
    // Reset is performed by forcing the clock either to High or to Low during at least 5ms

    SCIVS_M_COMMAND_P |= SCIVS_M_COMMAND_CLK_HIGH_MASK16;
    sleepMs(5);
    SCIVS_M_COMMAND_P &= ~SCIVS_M_COMMAND_CLK_HIGH_MASK16;

    // Release the reset

    SCIVS_M_COMMAND_P &= ~SCIVS_M_COMMAND_CLK_HIGH_MASK16;
    SCIVS_M_COMMAND_P &= ~SCIVS_M_COMMAND_CLK_LOW_MASK16;
}


//! Check master status is ok. If not return corresponding error code
//!
//! @retval first found error

static enum Scivs_error_code scivsCheckMasterStatus(void)
{
    if (SCIVS_M_STATUS_P & SCIVS_M_STATUS_BUSY_MASK16)
    {
        // Busy is not sticky bit
        return SCIVS_ERR_MASTER_BUSY;
    }

    if (SCIVS_M_STATUS_P & SCIVS_M_STATUS_TIMEOUT_MASK16)
    {
        // sticky bit is reset before sending GO
        return SCIVS_ERR_MASTER_TIMEOUT;
    }

    if (SCIVS_M_STATUS_P & SCIVS_M_STATUS_ABORT_MASK16)
    {
        // sticky bit is reset before sending GO
        return SCIVS_ERR_MASTER_ABORT;
    }

    if (SCIVS_M_STATUS_P & SCIVS_M_STATUS_TRANSFER_END_MASK16)
    {
        if (SCIVS_M_STATUS_P & SCIVS_M_STATUS_CRC_ERR_MASK16)
        {
            // sticky bit is reset before sending GO
            return SCIVS_ERR_MASTER_BAD_CRC;
        }

        return SCIVS_OK;

    }
    else
    {
        return SCIVS_ERR_MASTER_TRANSFER_IN_PROGRESS;
    }
}



//! Check status status is ok. If not return corresponding error code
//!
//! @param slot expected slot number in slave status
//! @reval  ok or first error found.

static enum Scivs_error_code scivsCheckSlaveStatus(uint16_t const slot)
{
    if (SCIVS_S_STATUS_P & SCIVS_S_STATUS_SLOT_ERR_MASK16)
    {
        return SCIVS_ERR_SLAVE_SLOT_ERR;
    }

    if (SCIVS_S_STATUS_P & SCIVS_S_STATUS_CRC_ERR_MASK16)
    {
        return SCIVS_ERR_SLAVE_CRC_ERR;
    }

    if (SCIVS_S_STATUS_P & SCIVS_S_STATUS_ABORT_MASK16)
    {
        return SCIVS_ERR_SLAVE_ABORT;
    }

    if (SCIVS_S_STATUS_P & SCIVS_S_STATUS_WRITE_ERR_MASK16)
    {
        return SCIVS_ERR_SLAVE_WRITE_ERR;
    }

    if (SCIVS_S_STATUS_P & SCIVS_S_STATUS_READ_ERR_MASK16)
    {
        return SCIVS_ERR_SLAVE_READ_ERR;
    }

    if (SCIVS_S_STATUS_P & SCIVS_S_STATUS_TIME_OUT_MASK16)
    {
        return SCIVS_ERR_SLAVE_TIMEOUT;
    }

    if ((SCIVS_S_STATUS_P & SCIVS_S_STATUS_SLOT_NUM_MASK16) != slot)
    {
        return SCIVS_ERR_SLAVE_BAD_SLOT;
    }

    return SCIVS_OK;
}



//! Check master status then slave status.
//!
//! @param slot expected slot number in slave status
//! @reval  ok or first error found.

static enum Scivs_error_code scivsCheckStatus(uint16_t const slot)
{
    enum Scivs_error_code error_code = scivsCheckMasterStatus();

    if (error_code == SCIVS_OK)
    {
        error_code = scivsCheckSlaveStatus(slot);
    }

    return error_code;
}



//! Wait for master to be ready, with timeout
//!
//! @param timeout_us timeout in us
//! @reval false if the master is not ready after timeout.

static enum Scivs_error_code scivsWaitMasterReady(uint16_t timeout_us)
{
    while (timeout_us != 0)
    {
        sleepUs(1);
        timeout_us--;
    }

    if (SCIVS_M_STATUS_P & SCIVS_M_STATUS_TRANSFER_END_MASK16)
    {
        return SCIVS_OK;
    }

    return SCIVS_ERR_MASTER_TRANSFER_IN_PROGRESS;
}



//! Send a packet (SCIVS protocol).
//! The buffer scivs_tx must be filled before.
//!
//! @param write_length write length of the SCIVS transaction
//! @param read_length read length of the SCIVS transaction
//! @param write_address write address of the SCIVS transaction
//! @param read_address read address of the SCIVS transaction
//! @param slot slot address of the slave on the SCIVS bus
//! @retval error code

static enum Scivs_error_code scivsWrite(uint16_t const slot,
                                        uint16_t const write_address,
                                        uint16_t const write_length,
                                        uint16_t const read_address,
                                        uint16_t const read_length)
{
    if (slot >= SCIVS_N_SLOTS)
    {
        return SCIVS_ERR_BAD_ARG;
    }

    if (   (write_length > SCIVS_MASTER_FRAME_W)
        || (read_length  > SCIVS_MASTER_FRAME_W))
    {
       return SCIVS_ERR_BAD_ARG;
    }

    // Clear slave status

    SCIVS_S_STATUS_P = 0x0000;

    SCIVS_M_LENGTH_P = 0;
    SCIVS_M_LENGTH_P = (write_length & 0x00FF) << 8 | (read_length & 0x00FF);

    SCIVS_M_WADD_P   = write_address;
    SCIVS_M_RADD_P   = read_address;

    SCIVS_M_SLAVE_SLOT_P = slot;

    if (SCIVS_M_COMMAND_P & (SCIVS_M_COMMAND_CLK_HIGH_MASK16 | SCIVS_M_COMMAND_CLK_LOW_MASK16))
    {
        return SCIVS_ERR_CLK_IN_RST;
    }

    if (SCIVS_M_STATUS_P & SCIVS_M_STATUS_BUSY_MASK16)
    {
        return SCIVS_ERR_MASTER_BUSY;
    }

    SCIVS_M_STATUS_P  = 0x0000;                     // clear status register
    SCIVS_M_COMMAND_P = SCIVS_M_COMMAND_GO_MASK16;  // Send GO

    return SCIVS_OK;
}



//! Send a packet to a RegFGC3 board (SCIVS/CVM protocol).
//! The buffer scivs_tx must be filled before.
//!
//! @param write_length write length of the SCIVS transaction
//! @param read_length read length of the SCIVS transaction
//! @param write_address write address of the SCIVS transaction
//! @param read_address read address of the SCIVS transaction
//! @param slot slot address of the slave on the SCIVS bus
//! @retval error code

static enum Scivs_error_code scivsSend(enum Scivs_task_code const operation_code,
                                       uint16_t             const slot,
                                       enum Scivs_device    const dest)
{
    // Clip index to max number of slot (power of 2)
    scivs_transmissions_counter[slot]++;

    scivs_tx->regfgc3.header.count_and_operation  = operation_code | (scivs_transmissions_counter[slot] <<
                                                    SCIVS_HEADER_COUNTER_SHIFT16);


    // Note for communicating with GMC (RegFGC3) this is fixed for all boards. The SCIVS address space is not used.
    // We always read and write the same size at the same place

    if ((dest < SCIVS_MF) || (dest > SCIVS_N_DEVICE))
    {
        return SCIVS_TSK_ERR_DESTINATION_UNKNOWN;
    }

    return scivsWrite(slot,
                      Device_addresses[dest].write_address,
                      SCIVS_MASTER_FRAME_W,
                      Device_addresses[dest].read_address,
                      SCIVS_MASTER_FRAME_W);
}



//! Read a packet on a RegFGC3 board (SCIVS/CVM protocol).
//!
//! @param slot slot address of the slave on the SCIVS bus
//! @param dest instantiator of the CVM module on the slave (FPGA, DSP)
//! @retval error code

static enum Scivs_error_code scivsReceive(uint16_t const slot, enum Scivs_device const dest)
{
    if ((dest < SCIVS_MF) || (dest > SCIVS_N_DEVICE))
    {
        return SCIVS_TSK_ERR_DESTINATION_UNKNOWN;
    }

    return scivsWrite(slot,
                    SCIVS_NO_WRITE,
                    0x0000,
                    Device_addresses[dest].read_address,
                    SCIVS_MASTER_FRAME_W);
}



// ---------- External function definitions

void scivsInit(void)
{
    scivsReset();
}



enum Scivs_error_code scivsTaskBlockSend(enum Scivs_task_code const task_code,
                                         uint16_t             const slot,
                                         enum Scivs_device    const dest,
                                         uint16_t             const block_idx)
{
    enum Scivs_error_code error_code;

    // Set block index in TX buffer
    scivs_tx->regfgc3.header.block_and_size = block_idx << 8;

    error_code = scivsTaskSend(task_code, slot, dest, SCIVS_MASTER_TIMEOUT_MS);

    return error_code;
}



enum Scivs_error_code scivsTaskSend(enum Scivs_task_code const task_code,
                                    uint16_t             const slot,
                                    enum Scivs_device    const dest,
                                    uint16_t                   timeout_ms)
{
    enum Scivs_error_code error_code;
    uint16_t count_and_operation;
    uint16_t expected_count_and_operation;

    // Send the task, basically OK unless master is busy
    if ((error_code = scivsSend(task_code, slot, dest)) != SCIVS_OK)
    {
        return error_code;
    }

    // Wait 1.3 ms for the first received frame (master ready again)
    if ((error_code = scivsWaitMasterReady(SCIVS_MASTER_TIMEOUT_US)) != SCIVS_OK)
    {
        return error_code;
    }

    // Make count for the 1.3 ms spent before waiting for the first frame
    timeout_ms--;

    // Check for low-level SCIVS communication errors (not task-related errors)
    if ((error_code = scivsCheckStatus(slot)) != SCIVS_OK)
    {
        return error_code;
    }

    // Suppose we received the first frame, check task-related error code
    error_code = scivs_rx->regfgc3.header.error;

    while ((error_code == SCIVS_TSK_ERR_EXEC_IN_PROGRESS) && (timeout_ms != 0))
    {
        // Try to read again
        if ((error_code = scivsReceive(slot, dest)) != SCIVS_OK)
        {
            return error_code;
        }

        if ((scivsWaitMasterReady(SCIVS_MASTER_TIMEOUT_US)) != SCIVS_OK)
        {
            return SCIVS_ERR_MASTER_BUSY;
        }

        if ((error_code = scivsCheckStatus(slot)) != SCIVS_OK)
        {
            return error_code;
        }

        error_code = scivs_rx->regfgc3.header.error;

        timeout_ms--;
    }

    if ((timeout_ms == 0) && (error_code == SCIVS_TSK_ERR_EXEC_IN_PROGRESS))
    {
        // Is there an error code for a timeout?
        return SCIVS_TSK_ERR_EXEC_IN_PROGRESS;
    }

    if (error_code != SCIVS_OK)
    {
        return error_code;
    }

    count_and_operation = scivs_rx->regfgc3.header.count_and_operation;

    expected_count_and_operation = task_code | (scivs_transmissions_counter[slot] <<
                                                     SCIVS_HEADER_COUNTER_SHIFT16);


    if (count_and_operation != expected_count_and_operation)
    {
        return SCIVS_TSK_ERR_BAD_COUNT_AND_OPERATION;
    }

    return SCIVS_OK;
}



enum Scivs_error_code scivsWriteCustom(uint16_t const slot,
                                       uint16_t const write_address,
                                       uint16_t const write_length,
                                       uint16_t const read_address,
                                       uint16_t const read_length)
{
    enum Scivs_error_code error_code;

    error_code = scivsWrite(slot, write_address, write_length, read_address, read_length);

    if (error_code != SCIVS_OK)
    {
        return error_code;
    }

    if ((error_code = scivsWaitMasterReady(SCIVS_MASTER_TIMEOUT_US)) != SCIVS_OK)
    {
        return error_code;
    }

    // Check for low-level SCIVS communication errors (not task-related errors)
    if ((error_code = scivsCheckStatus(slot)) != SCIVS_OK)
    {
        return error_code;
    }

    return error_code;
}



enum fgc_errno scivsConvertToFgcErr(enum Scivs_error_code const error_code)
{
    switch (error_code)
    {
        case SCIVS_OK:
        case SCIVS_ERR_SLAVE_WISHBONE_RESET:
        case SCIVS_ERR_SLAVE_BOARD_RST:
            return FGC_OK_NO_RSP;
            break;

        case SCIVS_TSK_ERR_BLK_ACCESS_DENIED:
            return FGC_BAD_STATE;
            break;

        case SCIVS_ERR_SET_NOT_PERM:
            return FGC_SET_NOT_PERM;
            break;

        case SCIVS_TSK_ERR_BLK_NOT_INSTALLED:
        case SCIVS_ERR_WRONG_PARAM_IDX:
            return FGC_NULL_ADDRESS;
            break;

        case SCIVS_ERR_BAD_ARG:
        case SCIVS_TSK_ERR_OVERFLOW:
        case SCIVS_TSK_ERR_INVALID_OP:
        case SCIVS_TSK_ERR_WRONG_N_ELS:
        case SCIVS_TSK_ERR_WRONG_BLK_IDX:
        case SCIVS_ERR_CLK_IN_RST:
        case SCIVS_ERR_MASTER_BUSY:
        case SCIVS_ERR_MASTER_TRANSFER_IN_PROGRESS:
        case SCIVS_ERR_MASTER_BAD_CRC:
        case SCIVS_ERR_MASTER_TIMEOUT:
        case SCIVS_ERR_MASTER_ABORT:
        case SCIVS_ERR_SLAVE_SLOT_ERR:
        case SCIVS_ERR_SLAVE_CRC_ERR:
        case SCIVS_ERR_SLAVE_ABORT:
        case SCIVS_ERR_SLAVE_WRITE_ERR:
        case SCIVS_ERR_SLAVE_READ_ERR:
        case SCIVS_ERR_SLAVE_TIMEOUT:
        case SCIVS_ERR_SLAVE_BAD_SLOT:
        case SCIVS_TSK_ERR_BAD_COUNT_AND_OPERATION:
        case SCIVS_TSK_ENDED_BY_ABORT:
        case SCIVS_TSK_ABORT_WHEN_IDLE:
        case SCIVS_TSK_ERR_EXT_TASK_TIMOUT:
        case SCIVS_TSK_ERR_NO_DSP:
        case SCIVS_TSK_ERR_FLASHING_DSP:
        case SCIVS_TSK_ERR_LOADING_DSP:
        case SCIVS_TSK_ERR_DSP_LOAD_FAILED:
        case SCIVS_TSK_ERR_DATA_LIMIT:
        case SCIVS_TSK_ERR_EXEC_IN_PROGRESS:
        case SCIVS_TSK_ERR_INTERNAL:
        case SCIVS_TSK_ERR_ACTION_NOT_RECOGNIZED:
        case SCIVS_TSK_ERR_DSP_PREV_PARAM_NOT_READ:
        case SCIVS_TSK_ERR_SLICE_INDEX_OVER_MAX:
        case SCIVS_TSK_ERR_PRODBOOT_NOT_STORED:
        case SCIVS_TSK_ERR_DESTINATION_UNKNOWN:
        case SCIVS_TSK_ERR_DB_NOT_ENABLED:
            return FGC_IO_ERROR;
            break;
    }

    return FGC_UNKNOWN_ERROR_CODE;
}


// EOF
