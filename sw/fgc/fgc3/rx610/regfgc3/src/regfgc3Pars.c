//! @file  regfgc3_pars.c
//! @brief Deals with setting / getting RegFGC3 parameters


// ---------- Includes

#include <regfgc3Pars.h>

#include <defprops.h>
#include <fgc_errs.h>

#include <logRun.h>
#include <nvs.h>
#include <pars.h>
#include <regfgc3Prog.h>
#include <status.h>



// ---------- Constants

//! Version of the binary format of REGFGC3.PARAMS blob

static uint8_t const REGFGC3_PARS_PARAMS_VERSION = 4;



// ---------- Internal structures, unions and enumerations

struct Regfgc3_pars
{
    uint8_t block_bitmask[SCIVS_N_SLOTS];
    uint8_t params_error_cnt;
    bool    params_version_ok;
};
    


// ---------- External variable definitions

struct Regfgc3_pars_prop __attribute__((section("sram"))) regfgc3_pars_prop;



// ---------- Internal variable definitions

static struct Regfgc3_pars regfgc3_pars = { .params_error_cnt = 0,
                                            .params_version_ok = true };



// ---------- Internal function definitions

static inline uint8_t regFgc3ParsGetSlot(uint8_t const slot, enum Scivs_device const device)
{
    // DSP paramters are stored in slot 0

    return ((device == SCIVS_DEVICE_2 && slot == regfgc3_pars_prop.parameters.dsp_slot) ? 0 : slot);
}



static void regFgc3ParsEnableBlock(uint8_t const slot, enum Scivs_device const device, uint8_t const block)
{
    setBitmap(regfgc3_pars_prop.parameters.info[regFgc3ParsGetSlot(slot, device)], 1 << block);
}



static void regFgc3ParsDisableBlock(uint8_t const slot, enum Scivs_device const device, uint8_t const block)
{
    clrBitmap(regfgc3_pars_prop.parameters.info[regFgc3ParsGetSlot(slot, device)], 1 << block);
}



static bool regFgc3ParsIsBlockEnabled(uint8_t const slot, enum Scivs_device const device, uint8_t const block)
{
    return testBitmap(regfgc3_pars_prop.parameters.info[regFgc3ParsGetSlot(slot, device)], 1 << block);
}



//! Sets a RegFGC3 params fault
//!
//! @param slot       Slot number
//! @param device     FPGA or DSP
//! @param block      Block number
//! @param error_code SCIVS or RegFGC3 task detected related error

static void regFgc3ParsSetFault(uint8_t               const slot,
                                enum Scivs_device     const device,
                                uint8_t               const block)
{
    uint8_t block_mask = 1 << block;
    uint8_t slot_idx   = regFgc3ParsGetSlot(slot, device);

    if (regfgc3_pars.params_error_cnt == 0)
    {
        statusSetUnlatched(FGC_UNL_REGFGC_PARAMS);
    }

    // Increment the bad counter of the real slot

    regfgc3_pars_prop.bad.count[slot]++;

    // For internal usage, map the VS Regualtion DSP board to slot 0

    if (testBitmap(regfgc3_pars_prop.bad.params[slot_idx], block_mask) == false)
    {
        setBitmap(regfgc3_pars_prop.bad.params[slot_idx], block_mask);
        regfgc3_pars.params_error_cnt++;
    }
}



//! Adds entry in the Run Log
//!
//! @param slot       Slot number
//! @param device     FPGA or DSP
//! @param block      Block number
//! @param error_code SCIVS or RegFGC3 task detected related error

static void regFgc3ParsLogError(uint8_t               const slot,
                                enum Scivs_device     const device,
                                uint8_t               const block,
                                enum Scivs_error_code const error_code)
{
    // Write run log

    uint32_t log_value = (slot & 0xFF) << 24 | (device & 0x0F) << 20 | (block & 0x0F) << 16 | (error_code & 0xFFFF);
    
    logRunAddTimestamp();
    logRunAddEntry(FGC_RL_SCIVS_ERR, &log_value);
}



static void regFgc3ParsClrFault(uint8_t const slot, enum Scivs_device const device, uint8_t const block)
{
    uint8_t block_mask = 1 << block;
    uint8_t slot_idx   = regFgc3ParsGetSlot(slot, device);

    if (testBitmap(regfgc3_pars_prop.bad.params[slot_idx], block_mask) == true)
    {
        clrBitmap(regfgc3_pars_prop.bad.params[slot_idx], block_mask);
        regfgc3_pars.params_error_cnt--;

        if (regfgc3_pars.params_error_cnt == 0 && regfgc3_pars.params_version_ok == true)
        {
            statusClrUnlatched(FGC_UNL_REGFGC_PARAMS);
        }
    }
}



static inline int32_t * regFgc3ParsGetParam(uint8_t const slot, enum Scivs_device const device, uint8_t const block)
{
    return regfgc3_pars_prop.parameters.values[regFgc3ParsGetSlot(slot, device)][block];
}



static uint16_t regFgc3ParsSetRawBlock(struct cmd * c, struct prop * p)
{
    prop_size_t   el_idx     = c->from;
    uint16_t      max_pars   = c->to - c->from + 1;;
    uint16_t      slot       = regfgc3_pars_prop.raw.slot;
    uint16_t      block      = regfgc3_pars_prop.raw.block;
    enum Scivs_device device = (p->sym_idx == STP_PARAM || p->sym_idx == STP_PARAM_S) ? SCIVS_MF : SCIVS_DEVICE_2;
    int32_t     * params     = regFgc3ParsGetParam(slot, device, block);
    void        * new_val;
    int32_t       data[SCIVS_N_PARAMS_PER_BLOCK];

    // Copy the orignal parameters to data, update this array with the values set 
    // by the user and if no errors are detected, copy data back into REGFGC3.PARAMS

    memcpy((void*)data, (void*)params, SCIVS_PARAMS_PER_BLOCK_SIZE);

    // Read the parameters

    while (c->last_par_f == false && c->n_pars < max_pars)
    {
        uint16_t errnum  = ParsValueGet(c, p, &new_val);

        switch (errnum)
        {
            case FGC_OK_NO_RSP:
                data[el_idx] = *((int32_t *) new_val);
                c->changed_f = true;
                c->n_pars++;
                el_idx++;
                break;

            case FGC_NO_SYMBOL:
                if (p->type == PT_CHAR)
                {
                    if (c->n_pars != 0)
                    {
                        return FGC_NO_SYMBOL;
                    }
                }

                if (el_idx != 0 || c->last_par_f == false)
                {
                    c->n_pars++;
                    el_idx++;
                }

                break;

            default:
                c->device_par_err_idx += c->n_pars;
                return (errnum);
        }
    }

    // Check number of parameters set and adjust property length

    if (c->last_par_f == false && c->n_pars >= max_pars)
    {
        c->device_par_err_idx += c->n_pars;

        return FGC_BAD_PARAMETER;
    }

    // If no parameters are set, clear the error corresponding to this block and disable it

    uint16_t retval = FGC_OK_NO_RSP;

    if (c->n_pars == 0)
    {
        regFgc3ParsClrFault(slot, device, block);
        regFgc3ParsDisableBlock(slot, device, block);
        memset((void*)params, 0, SCIVS_PARAMS_PER_BLOCK_SIZE);
    }
    else
    {        
        // Make sure the version and DSP slot are set. This is necessary for new 
        // systems that have never been configured in the past

        if (regfgc3_pars_prop.parameters.version != REGFGC3_PARS_PARAMS_VERSION)
        {
            regfgc3_pars_prop.parameters.version = REGFGC3_PARS_PARAMS_VERSION;
        }

        if (device == SCIVS_DEVICE_2)
        {
            regfgc3_pars_prop.parameters.dsp_slot = regfgc3_pars_prop.raw.slot;
        }

        // Contrary to other properties, perform transaction even if data has not changed. 
        // This behaviour is more predictable and can be useful during the development 
        // cycle of RegFGC3 boards.

        if ((retval = regFgc3ParsSendBlockToHw(slot, device, block, data)) == FGC_OK_NO_RSP)
        {
            nvsStoreValuesForOneProperty(c, &PROP_REGFGC3_PARAMS, 0, 0);
        }
    }

    return retval;
}



static void regFgc3ParsPrintSlot(FILE * fd, char delim, uint8_t const slot)
{
    fprintf(fd, "%s%c", "------------------------------", ',');
    fprintf(fd, "%-10s %d%c", "SLOT", slot, delim);
    fprintf(fd, "%-10s %s%c", "CARD", regFgc3ProgGetBoardName(regfgc3_prog_prop.detected[slot].type), ',');

    enum Scivs_error_code error_code;
    
    for (enum Scivs_device device = SCIVS_MF; device <= SCIVS_N_PROG_DEVICE; device++)
    {
        if (regFgc3ProgIsDevicePresent(slot, device) == false)
        {
            continue;
        }

        error_code = scivsTaskSend(SCIVS_TASK_CVM_INST_BLOCKS, slot, device, SCIVS_MASTER_TIMEOUT_MS);

        if (error_code != SCIVS_OK)
        {
            fprintf(fd, "%-10s %-4s Error: %04X%c", "Device", scivs_devices_names[device], error_code, delim);

            continue;
        }

        union Regfgc3_installed_blocks installed_blocks;
        
        memcpy((void*)&installed_blocks, (void *)scivs_rx->regfgc3.data, sizeof(installed_blocks));
        
        fprintf(fd, "%-10s %-4s%c", "Device", scivs_devices_names[device], delim);

        regfgc3_pars.block_bitmask[slot] = 0;

        uint8_t num_prams;

        for (uint8_t block = 0; block < 7; block++)
        {
            // Read/write block

            if ((num_prams = installed_blocks.access.rw[block]) > 0)
            {
                setBitmap(regfgc3_pars.block_bitmask[slot], 1 << block);

                error_code = scivsTaskBlockSend(SCIVS_TASK_CVM_READBACK, slot, device, block);

                if (error_code == SCIVS_OK)
                {
                    fprintf(fd, "RW block %d size %-2d%c", block, num_prams, delim);

                    for (uint8_t param_idx = 0; param_idx < num_prams; param_idx++)
                    {
                        fprintf(fd, "0x%08X%c", (unsigned int)scivs_rx->regfgc3.data[param_idx], delim);
                    }
                }
                else
                {
                    fprintf(fd, "RW block %d size %-2d Error: %04X%c", block, num_prams, error_code, delim);
                }

                fprintf(fd, "%c", delim);
            }
        }

        for (uint8_t block = 0; block < 7; block++)
        {
            // Read only block

            if ((num_prams = installed_blocks.access.ro[block]) > 0)
            {
                error_code = scivsTaskBlockSend(SCIVS_TASK_CVM_READ, slot, device, block);

                if (error_code == SCIVS_OK)
                {
                    fprintf(fd, "RO block %d size %-2d%c", block, num_prams, delim);

                    for (uint8_t param_idx = 0; param_idx < num_prams; param_idx++)
                    {
                        fprintf(fd, "0x%08X%c", (unsigned int)scivs_rx->regfgc3.data[param_idx], delim);
                    }
                }
                else
                {
                    fprintf(fd, "RO block %d size %-2d Error: %04X%c", block, num_prams, error_code, delim);
                }
           
                fprintf(fd, "%c", delim);           
            }
        }
    }
}



static uint16_t regFgc3ParsSetParams(struct cmd * c, struct prop * p)
{
    uint16_t errnum;

    if ((errnum = ParsSet(c, p)) == FGC_OK_NO_RSP)
    {
        errnum = regFgc3ParsSendParamsToHw();
    }

    return errnum;
}



// ---------- External function definitions

void regFgc3ParsInit(void)
{
    memset(&regfgc3_pars_prop, 0, sizeof(regfgc3_pars_prop));
}



uint16_t regFgc3ParsGetBlockFromHw(uint8_t           const slot, 
                                   enum Scivs_device const device, 
                                   uint8_t           const block, 
                                   int32_t               * cache_block)
{
    // Sanity check. Data cannot be sent to slot 0 because it is reserved for the FGC3

    if (   slot  <  1 || slot >= SCIVS_N_SLOTS
        || block >= SCIVS_N_TOTAL_BLOCKS
        || regFgc3ProgIsBoardPresent (slot)         == false
        || regFgc3ProgIsDevicePresent(slot, device) == false
        || regFgc3ProgIsBoardInDB    (slot)         == true)
    {
        return FGC_NULL_ADDRESS;
    }

    enum Scivs_error_code error_code = scivsTaskBlockSend(SCIVS_TASK_CVM_READBACK, slot, device, block);

    if (error_code == SCIVS_OK)
    {
        memcpy((void*)cache_block, (void*)scivs_rx->regfgc3.data, SCIVS_PARAMS_PER_BLOCK_SIZE);
    }

    return scivsConvertToFgcErr(error_code);
}



uint16_t regFgc3ParsSendBlockToHw(uint8_t              slot,
                                  enum Scivs_device    device,
                                  uint8_t              block,
                                  int32_t      const * params)
{
    // Sanity check. Data cannot be sent to slot 0 because it is reserved for the FGC3

    if (slot < 1 || slot >= SCIVS_N_SLOTS || block >= SCIVS_N_WRITE_BLOCKS)
    {
        return FGC_NULL_ADDRESS;
    }

    memcpy((void *)scivs_tx->regfgc3.data, (void *)params, SCIVS_PAYLOAD_TASK_DATA_B);

    enum Scivs_error_code error_code = scivsTaskBlockSend(SCIVS_TASK_CVM_WRITE, slot, device, block);

    if (error_code == SCIVS_OK)
    {
        int32_t * params_prop = regFgc3ParsGetParam(slot, device, block);
        memcpy((void *)params_prop, (void *)params, SCIVS_PARAMS_PER_BLOCK_SIZE);

        regFgc3ParsClrFault(slot, device, block);
        regFgc3ParsEnableBlock(slot, device, block);
    }
    else
    {
        regFgc3ParsSetFault(slot, device, block);
        regFgc3ParsLogError(slot, device, block, error_code);
    }

    return scivsConvertToFgcErr(error_code);
}


uint16_t regFgc3ParsSendParamsToHw(void)
{
    if (regfgc3_pars_prop.parameters.version != REGFGC3_PARS_PARAMS_VERSION)
    {
        regfgc3_pars.params_version_ok = false;
        statusSetUnlatched(FGC_UNL_REGFGC_PARAMS);

        regFgc3ParsLogError(0, 0, 0, regfgc3_pars_prop.parameters.version << 16 | 0xBAD1);

        return FGC_IO_ERROR;
    }

    regfgc3_pars.params_version_ok = true;

    if (regfgc3_pars.params_error_cnt == 0)
    {
        statusClrUnlatched(FGC_UNL_REGFGC_PARAMS);
    }

    uint16_t retval = FGC_OK_NO_RSP;

    // Send the blocks of parameters to the FPGAs. Slot 0 reserved for the DSP.

    for (uint8_t slot = 1; slot < FGC_SCIVS_N_SLOT; slot++)
    {
        for (uint8_t block = 0; block < SCIVS_N_WRITE_BLOCKS; block++)
        {
            if (regFgc3ParsIsBlockEnabled(slot, SCIVS_MF, block) == true)
            {
                int32_t  * params     = &regfgc3_pars_prop.parameters.values[regFgc3ParsGetSlot(slot, SCIVS_MF)][block][0];
                uint16_t   error_code = regFgc3ParsSendBlockToHw(slot, SCIVS_MF, block, params);
                if (error_code != FGC_OK_NO_RSP)
                {
                    retval = error_code;
                }
            }
            else
            {
                // If a block does not have parameters but is instantiated, throw an error

                if (testBitmap(regfgc3_pars.block_bitmask[slot], 1 << block) == true)
                {
                    regFgc3ParsSetFault(slot, SCIVS_MF, block);
                    regFgc3ParsLogError(slot, SCIVS_MF, block, 0xBAD2);
                }
            }
        }
    }

    // Send the blocks of parameters to the DSP

    uint32_t dsp_slot = regfgc3_pars_prop.parameters.dsp_slot;

    if (dsp_slot > 0)
    {
        for (uint8_t block = 0; block < SCIVS_N_WRITE_BLOCKS; block++)
        {
            if (regFgc3ParsIsBlockEnabled(dsp_slot, SCIVS_DEVICE_2, block) == true)
            {
                int32_t  * params     = &regfgc3_pars_prop.parameters.values[regFgc3ParsGetSlot(dsp_slot, SCIVS_DEVICE_2)][block][0];
                uint16_t   error_code = regFgc3ParsSendBlockToHw(dsp_slot, SCIVS_DEVICE_2, block, params);

                if (error_code != FGC_OK_NO_RSP)
                {
                    retval = error_code;
                }
            }
            else
            {
                // If a block does not have parameters but is instantiated, throw an error

                if (testBitmap(regfgc3_pars.block_bitmask[0], 1 << block) == true)
                {
                    regFgc3ParsSetFault(dsp_slot, SCIVS_DEVICE_2, block);
                    regFgc3ParsLogError(dsp_slot, SCIVS_DEVICE_2, block, 0xBAD2);
                }
            }
        }        
    }

    return retval;
}



void regFgc3ParsScanBlocks(uint8_t slot)
{
    // FPGA parameters

    if (regFgc3ProgIsDevicePresent(slot, SCIVS_MF) == true)
    {
        regfgc3_pars.block_bitmask[slot] = 0;

        enum Scivs_error_code error_code = scivsTaskSend(SCIVS_TASK_CVM_INST_BLOCKS, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);
        
        if (error_code == SCIVS_OK)
        {
            uint8_t num_prams;
            union Regfgc3_installed_blocks installed_blocks;
           
            memcpy((void*)&installed_blocks, (void *)scivs_rx->regfgc3.data, sizeof(installed_blocks));

            for (uint8_t block = 0; block < 7; block++)
            {
                if ((num_prams = installed_blocks.access.rw[block]) > 0)
                {
                    setBitmap(regfgc3_pars.block_bitmask[slot], 1 << block);
                }
            }
        }
    }

    // DSP parameters

    uint8_t dsp_slot = regfgc3_pars_prop.parameters.dsp_slot;

    if (slot == dsp_slot && regFgc3ProgIsDevicePresent(dsp_slot, SCIVS_DEVICE_2) == true)
    {
        regfgc3_pars.block_bitmask[0] = 0;

        enum Scivs_error_code error_code = scivsTaskSend(SCIVS_TASK_CVM_INST_BLOCKS, dsp_slot, SCIVS_DEVICE_2, SCIVS_MASTER_TIMEOUT_MS);
        
        if (error_code == SCIVS_OK)
        {
            uint8_t num_prams;
            union Regfgc3_installed_blocks installed_blocks;
           
            memcpy((void*)&installed_blocks, (void *)scivs_rx->regfgc3.data, sizeof(installed_blocks));

            for (uint8_t block = 0; block < 7; block++)
            {
                if ((num_prams = installed_blocks.access.rw[block]) > 0)
                {
                    setBitmap(regfgc3_pars.block_bitmask[0], 1 << block);
                }
            }
        }
    }
}



void regFgc3ParsRawReset(struct cmd * c)
{
    // Zero all parameters

    memset((void*)&regfgc3_pars_prop.parameters, 0, sizeof(regfgc3_pars_prop.parameters));

    regfgc3_pars_prop.parameters.version = REGFGC3_PARS_PARAMS_VERSION;

    PROP_REGFGC3_PARAMS.n_elements = REGFGC3_PARAMS_LEN;

    nvsStoreValuesForOneProperty(c, &PROP_REGFGC3_PARAMS, 0, 0);
}



// ---------- Property Get/Set functions

uint16_t GetCache(struct cmd * c, struct prop * p)
{
    // Displays all parameters on all boards on REGFGC3 crate.

    char delim;
    prop_size_t n;
    uint16_t errnum;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    // Set delimiter
    delim = (c->token_delim == ' ' ? '\n' : ',');

    for (uint8_t slot = 0; slot < FGC_SCIVS_N_SLOT; ++slot)
    {
        if (   regFgc3ProgIsBoardPresent(slot) == false
            || regFgc3ProgIsBoardInDB(slot)    == true)
        {
            continue;
        }

        regFgc3ParsPrintSlot(c->f, delim, slot);
    }

    return FGC_OK_NO_RSP;
}



uint16_t GetRawParameter(struct cmd * c, struct prop * p)
{
    static char const * fmt_hex        = "0x%08X";
    static char const * fmt_hex_neat   = "   0x%08X";
    static char const * fmt_dec[]      = { "%u"  , "%d"   };
    static char const * fmt_dec_neat[] = { "%13u", "%13d" };

    prop_size_t num_elements;
    uint16_t    errnum;

    if ((errnum = CmdPrepareGet(c, p, 4, &num_elements)) != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    if (num_elements == 0) 
    {
        return FGC_OK_NO_RSP;
    }

    char const * format;

    if (testBitmap(c->getopts, GET_OPT_HEX) == true)
    {
        format = ((c->token_delim == ' ') ? fmt_hex_neat : fmt_hex);
    }
    else
    {
        uint16_t type = p->type == PT_INT32U ? 0 : 1;

        format = ((c->token_delim == ' ') ? fmt_dec_neat[type] : fmt_dec[type]);
    }

    uint8_t           slot   = regfgc3_pars_prop.raw.slot;
    uint8_t           block  = regfgc3_pars_prop.raw.block + (regfgc3_pars_prop.raw.access == FGC_ACCESS_RIGHTS_RO ? SCIVS_N_WRITE_BLOCKS : 0);
    enum Scivs_device device = (p->sym_idx == STP_PARAM || p->sym_idx == STP_PARAM_S) ? SCIVS_MF : SCIVS_DEVICE_2;

    int32_t * param  = &regFgc3ParsGetParam(slot, device, block)[c->from];
    while (num_elements--)
    {
        CmdPrintIdx(c, c->from); 
        fprintf(c->f, format, *param);
        param   += c->step;
        c->from += c->step;
    }

    return FGC_OK_NO_RSP;
}



uint16_t GetRawDspParameterFloat(struct cmd * c, struct prop * p)
{
    static char     const * fmt_hex      = "0x%08X";
    static char     const * fmt_hex_neat = "   0x%08X";
    static uint16_t const   precision[]  = {  5, 5, 7 };
    static int16_t  const   width[]      = { 13, 0, 0 };

    prop_size_t num_elements;
    uint16_t    errnum;

   if ((errnum = CmdPrepareGet(c, p, 4, &num_elements)) != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    if (num_elements == 0) 
    {
        return FGC_OK_NO_RSP;
    }

    uint8_t           slot   = regfgc3_pars_prop.raw.slot;
    uint8_t           block  = regfgc3_pars_prop.raw.block + (regfgc3_pars_prop.raw.access == FGC_ACCESS_RIGHTS_RO ? SCIVS_N_WRITE_BLOCKS : 0);
    enum Scivs_device device = (p->sym_idx == STP_PARAM || p->sym_idx == STP_PARAM_S) ? SCIVS_MF : SCIVS_DEVICE_2;

    int32_t * param  = &regFgc3ParsGetParam(slot, device, block)[c->from];

    if (testBitmap(c->getopts, GET_OPT_HEX) == true)
    {
        char const * format = (c->token_delim == ' ') ? fmt_hex_neat : fmt_hex;

        while (num_elements--)
        {
            CmdPrintIdx(c, c->from); 
            fprintf(c->f, format, *((uint32_t *)param));
            param += c->step;
        }
    }
    else
    {
        uint8_t  idx = (c->token_delim == ' ') ? 0 : (testBitmap(p->flags, PF_NON_VOLATILE) ? 1 : 2);
        char     float_str[15];
        uint16_t val_prec  = precision[idx];
        uint16_t val_width = width[idx];

        while (num_elements--)
        {
            CmdPrintIdx(c, c->from); 
            sprintf(float_str, "%*.*E", val_width, val_prec, (double)*((float*)param));
            fputs(float_str, c->f);
            param   += c->step;
            c->from += c->step;
        }
    }

    return FGC_OK_NO_RSP;
}



uint16_t SetRawParameter(struct cmd * c, struct prop * p)
{
    return (p->sym_idx == STP_PARAMS ? regFgc3ParsSetParams(c, p) : regFgc3ParsSetRawBlock(c, p));
}


// EOF
