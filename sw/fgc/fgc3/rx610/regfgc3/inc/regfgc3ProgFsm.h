//! @file  regfgc3ProgFsm.h
//! Finite state machine for the program manager.
//!
//! The init function is called from the main.c unit. The program manager (or another user/client)
//! can interact with the FSM by setting the property REGFGC3.PROG.FSM.MODE, which triggers the function
//! regFgc3ProgFsmSetMode, which in turns makes a call to the function regFgc3ProgFsmProcess, if the mode
//! is validated.
//! The rest of functions are trivial getters/setters.

#pragma once



// ---------- Includes

#include <stdint.h>



// ---------- External structures, unions and enumerations

struct Regfgc3_prog_fsm_prop
{
    uint8_t state;          //!< State of the reprogramming FSM
    uint8_t mode;           //!< Mode of the reprogramming FSM
    uint8_t last_state;     //!< Mode of the reprogramming FSM
};



// ---------- External variable declarations


extern struct Regfgc3_prog_fsm_prop regfgc3_prog_fsm_prop;



// ---------- External function declarations

//! Initializes the finite state machine.

void regFgc3ProgFsmInit(void);


//! Returns the current finite state machine mode.
//!
//! @return uint16_t The finite state machine mode.

uint16_t regFgc3ProgFsmGetMode(void);


//!  Tries to set the finite state mode and process it.
//!
//! The function will first check if the FGC3 is lock. If not, it will validate the mode.
//! Then the mode will be stored, and the FSM will be exercised to achieve the requested mode.
//!
//! @param[in]  mode The mode the client wants to set, which corresponds to one FSM state.
//!
//! @retval     FGC_BAD_STATE   If the FGC is locked in that moment.
//! @retval     FGC_BAD_STATE   If the mode validation fails.
//! @retval     FGC_OK_NO_RSP   If the mode was successfully set.

uint16_t regFgc3ProgFsmSetMode(const uint8_t mode);


//! Gets the FSM state.
//!
//! @return uint16_t The FSM state, which should be equals to the mode if it could be set.

uint16_t regFgc3ProgFsmGetState(void);


//! Process the FSM current set mode.

void regFgc3ProgFsmProcess(void);


// EOF
