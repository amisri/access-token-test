//! @file  scivs.h
//! @brief Implements communication through the SCIVS bus

#pragma once


// ---------- Includes

#include <float.h>
#include <stdbool.h>
#include <stdint.h>

#include <fgc_consts_gen.h>
#include <memmap_mcu.h>



// ---------- Constants

//! Size of a SCIVS packet in 32 bit long words
#define SCIVS_MASTER_FRAME_LW   32

//! Size of a SCIVS packet in 16 bit words
#define SCIVS_MASTER_FRAME_W   (SCIVS_MASTER_FRAME_LW * sizeof(int16_t))

//! Size of a SCIVS packet in 8 bit bytes
#define SCIVS_MASTER_FRAME_B   (SCIVS_MASTER_FRAME_LW * sizeof(int32_t))

//! Number of slots
#define SCIVS_N_SLOTS                 FGC_SCIVS_N_SLOT

//! Maximum number of read only blocks
#define SCIVS_N_READ_BLOCKS            7

//! Maximum number of read write blocks
#define SCIVS_N_WRITE_BLOCKS           (SCIVS_N_READ_BLOCKS)

//! Maximum number of blocks
#define SCIVS_N_TOTAL_BLOCKS           (SCIVS_N_WRITE_BLOCKS + SCIVS_N_READ_BLOCKS)

//! Maximum size of a block in long words (task data)
#define SCIVS_PAYLOAD_TASK_DATA_LW     (SCIVS_MASTER_FRAME_LW - 1)

//! Maximum size of a block in words (task data)
#define SCIVS_PAYLOAD_TASK_DATA_W      (SCIVS_PAYLOAD_TASK_DATA_LW * sizeof(int16_t))

//! Maximum size of a block in bytes
#define SCIVS_PAYLOAD_TASK_DATA_B      (SCIVS_PAYLOAD_TASK_DATA_LW * sizeof(int32_t))

//! Maximum number of parameters (32 bit wide)
#define SCIVS_N_PARAMS_PER_BLOCK       (SCIVS_PAYLOAD_TASK_DATA_LW)

//! SCIVS block size
#define SCIVS_PARAMS_PER_BLOCK_SIZE    (SCIVS_PAYLOAD_TASK_DATA_B)



//! The default timeout to wait for the SCIVS master to complete a transmission
static const uint16_t SCIVS_MASTER_TIMEOUT_US = 1300;
static const uint16_t SCIVS_MASTER_TIMEOUT_MS = 5000;



// ---------- External structures, unions and enumerations

//! Enumeration of CVM block component

enum Scivs_device
{
    SCIVS_DB            = 0,
    SCIVS_MF            = 1,
    SCIVS_DEVICE_2      = 2,
    SCIVS_N_PROG_DEVICE = 2,
    SCIVS_N_DEVICE      = 3
};


//!
enum Scivs_wb_operation_addr
{
    SCIVS_NO_WRITE     = 0x0000,
    SCIVS_FPGA_WRITE   = 0x1000,
    SCIVS_FPGA_READ    = 0x1100,
    SCIVS_DSP_WRITE    = 0x3000,
    SCIVS_DSP_READ     = 0x3100,
    SCIVS_SLICE_WRITE  = 0x4000
};

//!
enum Scivs_error_code
{
    SCIVS_OK                              = 0x0000,
    SCIVS_TSK_ERR_OVERFLOW                = 0x0001, //<! A new OP has been received before the last being finished. Note that the OP ABORT does not generate this error.
    SCIVS_TSK_ERR_INVALID_OP              = 0x0002, //<! OP not defined for the addressed board
    SCIVS_TSK_ERR_WRONG_N_ELS             = 0x0003, //<! incorrect Number of Elements (N_Elements) in packet
    SCIVS_TSK_ERR_WRONG_BLK_IDX           = 0x0004, //<! incorrect Data Block number in packet
    SCIVS_TSK_ENDED_BY_ABORT              = 0x0005,
    SCIVS_TSK_ABORT_WHEN_IDLE             = 0x0006, //<! Task abort sent but no task being executed
    SCIVS_ERR_SET_NOT_PERM                = 0x0007, //<! Set not allowed on this block
    SCIVS_ERR_CLK_IN_RST                  = 0x0008, //<! Master clock in reset
    SCIVS_ERR_MASTER_BUSY                 = 0x0009, //<! Master status: busy
    SCIVS_ERR_MASTER_TRANSFER_IN_PROGRESS = 0x000A, //<! Master status: transfer in progress
    SCIVS_ERR_MASTER_BAD_CRC              = 0x000B, //<! Master error: Wrong CRC on the slave's frame
    SCIVS_ERR_MASTER_TIMEOUT              = 0x000C, //<! Master error: timeout on master wait
    SCIVS_ERR_MASTER_ABORT                = 0x000D, //<! Master status: Abort during reception
    SCIVS_ERR_SLAVE_SLOT_ERR              = 0x000E, //<! Slave error: invalid slot received (reported by first slave addressed after error)
    SCIVS_ERR_SLAVE_CRC_ERR               = 0x000F, //<! Slave error: CRC error detected by the slave on the master's frame (but valid slot address)
    SCIVS_ERR_SLAVE_ABORT                 = 0x0010, //<! Slave error: Abort received (reception of 18 stop bits or an early start bit)
    SCIVS_ERR_SLAVE_WRITE_ERR             = 0x0011, //<! Slave error: Error during wishbone write
    SCIVS_ERR_SLAVE_READ_ERR              = 0x0012, //<! Slave error: Error during wishbone read
    SCIVS_ERR_SLAVE_BOARD_RST             = 0x0013, //<! Slave info: First status after reset
    SCIVS_ERR_SLAVE_TIMEOUT               = 0x0014, //<! Slave error: User time window expired
    SCIVS_ERR_SLAVE_WISHBONE_RESET        = 0x0015, //<! Slave info: a wishbone reset has been received
    SCIVS_ERR_SLAVE_BAD_SLOT              = 0x0016, //<! Slave error: status received by unexpected slave
    SCIVS_TSK_ERR_BAD_COUNT_AND_OPERATION = 0x0017, //<! Slave error: unexpected count and operation
    SCIVS_ERR_BAD_ARG                     = 0x0018,
    SCIVS_ERR_WRONG_PARAM_IDX             = 0x0019,
    SCIVS_TSK_ERR_ACTION_NOT_RECOGNIZED   = 0x0070,
    SCIVS_TSK_ERR_BLK_ACCESS_DENIED       = 0x0080, //<! Access to this Data Block is not granted, the block can be protected during certain conditions, i.e. converter ON
    SCIVS_TSK_ERR_BLK_NOT_INSTALLED       = 0x0081, //<! Data Block is not installed
    SCIVS_TSK_ERR_EXT_TASK_TIMOUT         = 0x00B0, //<! Some External Task(s) did not finish on time, request later. Some values can be erroneous
    SCIVS_TSK_ERR_NO_DSP                  = 0x00C0, //<! DSP is not installed in the board.
    SCIVS_TSK_ERR_FLASHING_DSP            = 0x00C1, //<! Hardware setting disables updating DSP program in FPGA Flash from FGC3.
    SCIVS_TSK_ERR_LOADING_DSP             = 0x00C2, //<! Hardware setting disables downloading of program from FPGA Flash to DSP.
    SCIVS_TSK_ERR_DSP_LOAD_FAILED         = 0x00C3, //<! Download from FPGA Flash to DSP ended by Time-out, DSP-OK not detected
    SCIVS_TSK_ERR_DATA_LIMIT              = 0x00D0, //<! Data Batch out of limits
    SCIVS_TSK_ERR_DSP_PREV_PARAM_NOT_READ = 0x00E0,
    SCIVS_TSK_ERR_SLICE_INDEX_OVER_MAX    = 0x0100, //<! Slice index higher than maximum number of slices
    SCIVS_TSK_ERR_PRODBOOT_NOT_STORED     = 0x0101, //<! Production boot has not been stored yet
    SCIVS_TSK_ERR_DESTINATION_UNKNOWN     = 0x0102, //<! Destination unknown or not defined (!= FPGA or DSP)
    SCIVS_TSK_ERR_DB_NOT_ENABLED          = 0x0104, //<! Programming of the DB device has not been enabled
    SCIVS_TSK_ERR_EXEC_IN_PROGRESS        = 0xFFF0, //<! OP under process
    SCIVS_TSK_ERR_INTERNAL                = 0xFFFF, //<! Internal Error
};

//!
enum Scivs_task_code
{
    SCIVS_TASK_LOOPBACK          = 0x0020,
    SCIVS_TASK_UTC_TIME          = 0x0030,
    SCIVS_TASK_CVM_WRITE         = 0x0050,
    SCIVS_TASK_CVM_READBACK      = 0x0051,          //<! Reads parameters stored on write blocks on CVMs
    SCIVS_TASK_CVM_READ          = 0x0052,          //<! Reads parameters on read blocks on CVMs
    SCIVS_TASK_CVM_INST_BLOCKS   = 0x0053,
    SCIVS_TASK_CVM_DEFAULT_VALS  = 0x0054,
    SCIVS_TASK_SLICE_ACTION      = 0x0101,
    SCIVS_TASK_SWITCH_BOOT       = 0x0102,
    SCIVS_TASK_GET_HEADER        = 0x0103,
    SCIVS_TASK_INVALID_OP        = 0x0123,
    SCIVS_TASK_GENERAL_CONST     = 0x0800,
    SCIVS_TASK_GENERAL_CONST_EXT = 0x0801,
    SCIVS_TASK_TEST              = 0x0FF1,
    SCIVS_TASK_ABORT             = 0x0FFE
};

//!
enum Scivs_task_slice_action
{
    SCIVS_TASK_SLICE_ACTION_NONE              = 0x0000,  
    SCIVS_TASK_SLICE_ACTION_PROGRAM           = 0x0001,
    SCIVS_TASK_SLICE_ACTION_READBACK          = 0x0002,
    SCIVS_TASK_SLICE_ACTION_GETPARS           = 0x0003,
    SCIVS_TASK_SLICE_ACTION_CALCCRC           = 0x0004,
    SCIVS_TASK_SLICE_ACTION_ENABLE_CHANGE_DB  = 0x0005,
    SCIVS_TASK_SLICE_ACTION_SET_PRODBOOT_PARS = 0x0006
};

//! Wishbone memory map, common to every board (implemented by the SCIVS IP)

struct Scivs_wishbone_info
{
    uint16_t    board_address;
    uint16_t    board_type;
    uint16_t    ip_rev;
    uint16_t    base_module_rev;
    uint16_t    user_time;
    uint16_t    rx_delay;
    uint16_t    status;
    uint16_t    wadd;
    uint16_t    radd;
    uint16_t    length;
    uint16_t    crc;
    uint16_t    prog_variant;
    uint16_t    prog_variant_rev;
    uint16_t    gmc_variant;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;

//! SCIVS/CVM TX and RX packet structure

struct Scivs_packet
{
    struct
    {
        uint16_t        count_and_operation;        //!< count : 4 bits [b15..b12]; operation code : 12 bits [b11..b0]
        union
        {
            uint16_t    block_and_size;             //!< Transmitted frame
            uint16_t    error;                      //!< Received frame
        };
    }                   header;

    int32_t             data[SCIVS_N_PARAMS_PER_BLOCK];
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;


//! Union of SCIVS/CVM TX packets

union Scivs_tx_packet
{
    struct Scivs_packet     regfgc3;                //<! RegFGC3 packet
    uint16_t                w[SCIVS_MASTER_FRAME_W];
    uint32_t                dw[SCIVS_MASTER_FRAME_W / 2];
    float                   fp[SCIVS_MASTER_FRAME_W / 2];
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;


//! Union of SCIVS/CVM RX packets. Different format for the payload of the SCIVS packets.

union Scivs_rx_packet
{
    struct Scivs_wishbone_info  wishbone_info;                   //<!Used in ScivsReadWB (scivs_task)
    struct Scivs_packet         regfgc3;                         //<!RegFGC3 packet
    uint16_t                    w[SCIVS_MASTER_FRAME_W]; //<!Used in menuScivsBus
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;



// ---------- External variable declarations

//!

extern char * scivs_devices_names[SCIVS_N_DEVICE];

//! Union of SCIVS/CVM TX packets

extern volatile union Scivs_tx_packet * const scivs_tx;

//! Union of SCIVS/CVM RX packets

extern volatile union Scivs_rx_packet const * const scivs_rx;



// ---------- External function declarations

//! Initialise the SCIVS
//! This will release the reset on the SCIVS, thus release the reset on all the FGC3 cards

void scivsInit(void);


//! Translate a SCIVS/CVM error code to a FGC error code
//!
//! @param err SCIVS/CVM error code
//! @retval FGC error code

enum fgc_errno scivsConvertToFgcErr(enum Scivs_error_code err);


//! [scivsWriteCustom description]
//! @param  slot  [description]
//! @param  write_address [description]
//! @param  write_length  [description]
//! @param  read_address  [description]
//! @param  read_length   [description]
//! @return               [description]

enum Scivs_error_code scivsWriteCustom(uint16_t slot,
                                       uint16_t write_address,
                                       uint16_t write_length,
                                       uint16_t read_address,
                                       uint16_t read_length);


//! This function perform a full transaction over SCIVS (read, wait, check status), doing a **blocking sleep**.
//! It is meant to be used at start up only, during the initialization phase before the task is running.
//!
//! @param task_code CVM operation code
//! @param slot slot number of the

enum Scivs_error_code scivsTaskSend(enum Scivs_task_code task_code,
                                    uint16_t             slot,
                                    enum Scivs_device    dest,
                                    uint16_t             timeout_ms);


//![scivsTaskBlockSend description]
//! @param  task_code [description]
//! @param  slot      [description]
//! @param  dest      [description]
//! @param  block_idx [description]
//! @return           [description]

enum Scivs_error_code scivsTaskBlockSend(enum Scivs_task_code task_code,
                                         uint16_t             slot,
                                         enum Scivs_device    dest,
                                         uint16_t             block_idx);


// EOF
