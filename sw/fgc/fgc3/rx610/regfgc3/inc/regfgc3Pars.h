//! @file  regfgc3Pars.h
//! @brief SCIVS bus API
//!
//!  Description    :
//!  Task managing communication through SCIVS bus on REGFGC3 systems
//!  Functions to use the different services provided over SCIVS bus :
//!
//!   # REGFGC3 Parameters
//!       Parameters in REGFGC3 are access through prop REGFGC3.RAW
//!       The definition of parameters on the crate is handled by an external tool
//!
//!   # Firmware flash
//!       This is performed once at boot during the initialisation phase.
//!       An external manager programs the boards through a property interface To Be defined
//!
//!   # Logs from SDRAM
//!       retrieve post mortem and acquisition logs. Non real time task. To be defined
//!
//!
//!  SCIVS/CVM documentation is available at:
//!      G:\Departments\TE\Groups\EPC\Projects\RegFGC3\Design\_DesignDocs
//!  More documentation for each board at:
//!      G:\Departments\TE\Groups\EPC\Projects\RegFGC3\Design\Converters\*Converter Name*\Card Definitions
//!
//!  Specifications for RegFGC3 integration in SW are available at :
//!  https://wikis.cern.ch/display/TEEPCCCS/RegFGC3+integration+in+the+FGC+Framework

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdio.h>
#include <defconst.h>
#include <os.h>
#include <scivs.h>
#include <cmd.h>



// ---------- Constants

//! Total number of long words in REGFGC3.PARAMS
#define REGFGC3_PARAMS_LEN                 (sizeof(struct Regfgc3_pars_parameters) / sizeof(int32_t))
#define REGFGC3_PARAMS_SLOTS               16


// ---------- External structures, unions and enumerations

//! This structure is mapped to the property family REGFGC3.RAW

struct Regfgc3_pars_raw
{
    uint16_t slot;                          //!< REGFGC3.RAW.SLOT
    uint16_t block;                         //!< REGFGC3.RAW.BLOCK
    uint16_t access;                        //!< REGFGC3.RAW.ACCESS
};


struct Regfgc3_pars_parameters
{
    uint16_t version;                                                               //!< Version of the binary blob
    uint16_t dsp_slot;                                                              //!< DSP slot
    uint8_t  info  [SCIVS_N_SLOTS];                                                 //!< Metadata specifying if a block has data
    int32_t  values[REGFGC3_PARAMS_SLOTS][SCIVS_N_WRITE_BLOCKS][SCIVS_N_PARAMS_PER_BLOCK]; //!< Array with the parameter values
};



//! Structure for all SCIVS global variables

struct Regfgc3_pars_prop
{
    struct Regfgc3_pars_raw        raw;
    struct Regfgc3_pars_parameters parameters;       //!< REGFGGC3.PARAMS
  
    struct Regfgc3_pars_prop_bad
    {
        uint16_t count[SCIVS_N_SLOTS];               //!< REGFGGC3.BAD.COUNT
        uint8_t  params[SCIVS_N_SLOTS];              //!< REGFGGC3.BAD.PARAMS
    } bad;
};



// ---------- External variable declarations

extern struct Regfgc3_pars_prop regfgc3_pars_prop;



// ---------- External function declarations

//! Initiliazies this module

void regFgc3ParsInit(void);


//! Scan the SCIVS bus, for each existing block, initialise the cache.
//! Initialises the parameters for each existing block in all slots.
//!
//! The parameter values are retrieved from the non-volatile memory.

void regFgc3ParsBuildCache(void);


//! Gets a block already existing in the cache
//! @param  slot
//! @param  component
//! @param  block
//! @param  cache_block
//! @return             SCIVS error code from the transaction. The data block is pointed by 'cache_block'

uint16_t regFgc3ParsGetBlockFromHw(uint8_t           slot, 
                                   enum Scivs_device device, 
                                   uint8_t           block, 
                                   int32_t         * cache_block);


//! Set a full block at {slot,block index} position.
//! This function is meant to be used to save/recover blocks from/to the NVS/DB
//! The values are ignored for the parameter not instantiated

//! This function will get a block from {slot number, block index}.
//! /!\ block index is from 0 to 13!
//!
//! @param slot         Slot of the card
//! @param device       Device (fpga or dsp)
//! @param block        Block index from 0 to 13
//! @param params       Parameters to transfer to the board
//!
//! @retval error code.

uint16_t regFgc3ParsSendBlockToHw(uint8_t            slot,
                                  enum Scivs_device  device,
                                  uint8_t            block,
                                  int32_t    const * params);


//! This function is called after synchronization with the configuration database.
//!
//! It performs several functions:
//! 1. Initializes the parameters for each existing block in all slots (regFgc3ParsInitParams()),
//! which is retrieved from the non-volatile memory.
//! 2. Checks the blob's version and magic word. If they are not what they should be, the blob is reconstructed from the cache
//! and it is copied to non-volatile memory.
//! 3. If values are OK, REGFGC3.PARAMS are written to the different boards.

uint16_t regFgc3ParsSendParamsToHw(void);


//! This function scans a slot and records which blocks are instantiated.
//!
//! @param slot         Slot of the card

void regFgc3ParsScanBlocks(uint8_t slot);

//! Resets the RegFGC3 parameters. 
//!
//! The property REGFGC3.PARAMS is initialized with the RegFGC3 version
//! and all parameters set to zero.

void regFgc3ParsRawReset(struct cmd * c);


// EOF
