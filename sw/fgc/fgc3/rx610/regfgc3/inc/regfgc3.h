//! @file  regfgc3.h
//! @brief This header contains structures that encapsulate data to communicate with the RegFgc3 boards via tasks

#pragma once


// ---------- Includes

#include <stdint.h>

#include <defconst.h>
#include <scivs.h>



// ---------- External structures, unions and enumerations

//! SCIVS response buffer to SCIVS/CVM operation code SCIVS_TASK_GENERAL_CONST

struct Regfgc3_general_constants
{
    uint16_t    option_flags;                   //!< Flags summarizing the general status.
    uint16_t    mc_program_revision;            //!< Revision of the GMC Microcontroller program
    uint16_t    fpga_board_type;                //!< Definition of the type of board
    uint16_t    fpga_program_variant;           //!< Definition of the type of VHDL program for this Converter
    uint16_t    fpga_program_variant_revision;  //!< Revision of the VHDL Modules specific for this Converter
    uint16_t    fpga_fgc3cvm_variant_id;        //!< FGC3-CVM API Variant installed.
    uint16_t    fpga_fgc3cvm_api_revision;      //!< Revision of this FGC3-CVM API installed.
    uint16_t    fpga_base_module_revision;      //!< Revision of the boards Base VHDL Modules
    uint16_t    fpga_common_module_revision;    //!< Revision of the Common VHDL Modules
    uint16_t    dsp_program_variant;            //!< Definition of the type of DSP program for this Converter
    uint16_t    dsp_program_variant_revision;   //!< Revision of the DSP Modules for this Converter
    uint16_t    dsp_basemodule_api_revision;    //!< Revision of the Base part of DSP programs
    uint16_t    hardware_execution;             //!< Value of Execution input bits
    uint64_t    mezzanine_id;                   //!< Value of Mezzanines ID-chip. 64 bits
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;


//! SCIVS response buffer to SCIVS / CVM operation code SCIVS_TASK_GENERAL_CONST_EXT.

struct Regfgc3_device_data
{
    uint16_t device_type;
    uint16_t variant;
    uint16_t variant_revision;
    uint16_t api_revision;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;


//! SCIVS response buffer to SCIVS/CVM operation code SCIVS_TASK_GET_HEADER

struct Regfgc3_header
{
    char            data[128];                  //!< Raw header
};


//! Auxiliar structure for the task General constants

struct Regfgc3_device_data_ext
{
    struct   Regfgc3_device_data dev_data;
    uint16_t dsp_basemodule_api_revision;
    uint16_t info_0;
    uint16_t info_1;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;


//! Structure for the General constants extended task

struct Regfgc3_general_constants_ext
{
    uint16_t    installed_devices;              //!< Number of programmable devices
    uint16_t    board_type;                     //!< The board type according to symlist regfgc3_boards.xm
    uint16_t    hw_execution;                   //!< Value of execution input bit
    uint64_t    mezzanine_id;                   //!< Mezzanine ID
    uint16_t    undefined;
    struct Regfgc3_device_data_ext device_0;    //!< Data for device
    struct Regfgc3_device_data_ext device_1;    //!< Data for device
    struct Regfgc3_device_data_ext device_2;    //!< Data for device
    struct Regfgc3_device_data_ext device_3;    //!< Data for device
    struct Regfgc3_device_data_ext device_4;    //!< Data for device
    struct Regfgc3_device_data_ext device_5;    //!< Data for device
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;


//! Structure for the Installed blocks task

union Regfgc3_installed_blocks
{
    uint16_t all[SCIVS_N_TOTAL_BLOCKS];             //!< Number of parameters in all block
    struct
    {
        uint16_t rw[SCIVS_N_WRITE_BLOCKS];    //!< Number of parameters in write blocks
        uint16_t ro[SCIVS_N_READ_BLOCKS];     //!< Number of parameters in read blocks
    }
#ifdef __GNUC__
    __attribute__((__packed__, aligned(2)))
#endif
    access;
};


//! SCIVS response buffer to SCIVS/CVM operation code SCIVS_TASK_SLICE_ACTION, action SCIVS_TASK_SLICE_ACTION_GET_PRODBOOT_PARS

struct Regfgc3_slice_params
{
    uint16_t    number_bytes;
    uint16_t    number_batches;
    uint16_t    time_program;
    uint16_t    time_readback;
    uint16_t    time_crc;
    uint16_t    time_parameters;
    uint16_t    max_number;
    uint16_t    storage_start;
    uint16_t    user_1;
    uint16_t    user_2;
    uint16_t    user_3;
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;


//! Structure for the Get Slice parameters task

struct Regfgc3_slice_get_parameters
{
    uint16_t slice_number_bytes;            //!< Number of bytes per slice
    uint16_t number_of_batches;             //!< Number of batches required to transmit a slice
    uint16_t slice_time_program;            //!< Maximum time (msec) to program a slice
    uint16_t slice_time_readback;           //!< Maximum time (msec) to read a slice
    uint16_t slice_time_crc;                //!< Maximum time (msec) to calculate the full binary CRC
}
#ifdef __GNUC__
__attribute__((__packed__, aligned(2)))
#endif
;


// EOF
