//! @file  regfgc3Task.h
//! This is an OS task that handles requests of different nature regarding the RegFGC3 hardware


#pragma once



// ---------- Includes

#include <stdint.h>

#include <os.h>



// ---------- External structures, unions and enumerations

//! Enumerates the different types of subtasks this task can handle

enum Regfgc3_task_job_type
{
	REGFGC3_TASK_IDLE  = 0x00,      //!< No subtask set yet
	REGFGC3_TASK_PROG  = 0x01,      //!< RegFgc3 reprogramming task
	REGFGC3_TASK_SCAN  = 0x02,      //!< RegFgc3 crate rescan, action from the command line (S REGFGC3.SLOT_INFO)
	REGFGC3_TASK_IN_PB = 0x04       //!< RegFgc3 check if all boards are in production boot to send parameters
};


// ---------- External variable declarations

//! Semaphore used to awaken the task.

extern OS_SEM *regfgc3_task_sem_go;



// ---------- External function declarations

//! Sets the appropriate job for the RegFGC3 task to do.
//!
//! This function might be called from other regfgc3 related modules.
//!
//! @param[in] job	Type of job for the task to carry out.

void regFgc3TaskSetJob(enum Regfgc3_task_job_type const job);


//! Entry point for the RegFGC3 task.
//!
//! @param unused

void regFgc3Task(void * unused);


//! This function wakes up the RegFGC3 task, which will look for jobs to run.

void regFgc3TaskAwake(void);


// EOF
