//! @file   regfgc3Prog.h
//! @brief

#pragma once


// ---------- Includes

#include <stdint.h>

#include <cmd.h>
#include <scivs.h>



// ---------- External structures, unions and enumerations

struct Regfgc3_prog_device_info
{
    uint16_t name;
    uint16_t variant;
    uint16_t variant_revision;
    uint16_t api_revision;
    char     header_utc[11];
    char     git_sha[9];
};

struct Regfgc3_prog_slot_info
{
    bool               reprogrammable; //!< Board reprogrammable or not
    uint16_t           type;           //!< Board type found
    uint16_t           slot_state;     //!< Boards' state
    uint64_t           mezzanine_id;   //<! External mezzanine board's dallas ID
    struct Regfgc3_prog_device_info devices[SCIVS_N_DEVICE];
};

struct Regfgc3_prog_prog_data
{
    uint8_t     lock;               //!< FGC3 lock for the program manager
    uint8_t     manager;            //!< Switch on/off the program manager
    uint8_t     slot_state;         //!< State of the slot being programmed
    uint8_t     slot;               //!< Slot of the board that will be programmed
    uint8_t     board;
    uint8_t     device;             //!< Device on the slot that will be programmed
    uint16_t    variant;            //!< Variant the device will be programmed with
    uint16_t    api_revision;       //!< Revision of the variant's API
    uint16_t    variant_revision;   //!< Variant's revision
    uint16_t    bin_crc;            //!< Binary file's CRC the program manager calculates before sending it to the FGC3
    uint32_t    bin_size_bytes;     //!< Size in bytes of the binary to be reprogrammed
    uint8_t     state;              //!< Programming state: STANDALONE, UNSYNCHRONIZED, SYNCHRONIZED
    uint8_t     mode;               //!< Programming mode: NONE, FAILED
};

struct Regfgc3_prog_stats
{
    uint16_t slices_sent[FGC_SCIVS_N_SLOT];
    uint16_t batches_sent[FGC_SCIVS_N_SLOT];
    uint16_t slices_programmed[FGC_SCIVS_N_SLOT];
    uint16_t errors[FGC_SCIVS_N_SLOT];
};

struct Regfgc3_prog_debug
{
    uint16_t    action;
    uint16_t    board_error;
    uint16_t    slice_bytes;
    uint16_t    slice_batches;
    uint16_t    time_crc;
    uint16_t    time_params;
    uint16_t    time_prog;
    uint16_t    calc_crc;
};

struct Regfgc3_prog_prop
{
    //<! Crate info related variables
    struct Regfgc3_prog_slot_info detected[FGC_SCIVS_N_SLOT];

    //<! Reprogramming related variables
    struct Regfgc3_prog_prog_data prog_data;

    struct Regfgc3_prog_stats stats;

    struct Regfgc3_prog_debug debug;
};



// ---------- External variable declarations


extern struct Regfgc3_prog_prop regfgc3_prog_prop;



// ---------- External function declarations

// ---------- Functions related to remote reprogramming

//! Initializes the program manager after NVS initialization.

void regFgc3ProgMgrInit(void);


//! Sends the RegFgc3 boards parameters for the first time.
//!
//! The MsTask runs this function every 500 ms. It checks that all boards are in
//! production boot mode before sending the parameters. Once the FGC3 has send them,
//! it will not do it again.

void regFgc3ProgSetJobInPb(void);


//! Function executed when all the boards have transitioned to the production boot

void regFgc3ProgBoardsInProduction(void);


//! Sets the property REGFGC3.PROG.MODE to a value.
//!
//! @param mode[in] The program manager mode (NONE, WARNING, FAILED).

void regFgc3ProgSetMode(uint16_t const mode);


//! Gets the program manager mode fault
//!
//! @return true  Program manager mode failed
//! @return false Program manager mode did not fail

bool regFgc3ProgThereIsProgFault(void);


//! Returns true if the boards failed to switch to Production boot
//!
//! @return true  If the boards timed out when switching to Production boot
//! @return false If the boards did not time out when switching to Production boot

bool regFgc3ProgSwitchProductionFailed(void);


//! Returns whether all the boards are in Production boot
//!
//! @return true  If all the boards are in Production boot
//! @return false If not all the boards are in Production boot

bool regFgc3ProgAllBoardsInPb(void);


//! Sets the property REGFGC3.PROG.STATE to a value.
//!
//! @param state[in] The program manager state.
//! Initially, the state is set to STANDALONE or UNSYNCED, depending on the operational
//! conditions of the FGC3. The program manager calls this function with the value SYNCHRONIZED
//! if the reprogramming was successful or the RegFgc3 boards contain the expected FW.

void regFgc3ProgSetState(uint16_t const state);


//! Whether the FGC3 needs to wait for the program manager or not,
//! so that in can safely set all board's mode in production boot.
//!
//! @return true    If the FGC3 needs to wait for the program manager.
//! @return false   if the FGC3 does not need to wait for the program manager.

bool regFgc3ProgWaitForProgramManager(void);


//! Programming FSM related functions
//!
//! Programs a given RegFgc3 board.
//!
//! Sends slices to the board, and issues the PROGRAM task, taking into account
//! whether the device is the DB or any other.
//!
//! @return uint16_t Error code from this action.

uint16_t regFgc3ProgReprogram(void);


//!  Checks that the CRC computed by the board on the FW sent matches the one from its trailer.
//!
//! @return uint16_t    The tasks' error code.

uint16_t regFgc3ProgVerifyProgram(void);


//! TODO: rename and use this one from FSM.

void regFgc3ProgClean(void);


uint16_t regFgc3ProgSetAction(uint16_t const action); // added for debugging/commissioning


//! Gets the FGC3 lock status.
//!
//! The lock will be checked before any programming action takes place.
//! The program manager is in charge of unlocking an FGC3 before reprogramming any of its boards.
//!
//! @return uint16_t Whether the FGC3 is locked or not.

uint16_t regFgc3ProgGetOpState(void);



// ---------- Functions related to crate scanning ----------

//! Initializes regfgc3_prog_handler.
//!
//! The function is called from the RegFgc3 task.

void regFgc3ProgInitHandler(void);


//! Reads the dallas ID in Mezzanine boards, if such boards are found.
//!
//! The function is called periodically by the DallasTsk.
//!
//! @param slot[in]     Board slot where there is a mezzanine board.
//! @return uint64_t    The read Dallas ID.

uint64_t regFgc3ProgGetMezzanineId(uint8_t const slot);


//!  Scans the RegFgc3 crate.
//!
//! Stores RegFgc3 boards and their installed firmware in data structures.
//! The FGC3 sends the task SCIVS_TASK_GENERAL_CONST through the SCIVS bus, and the obtained
//! data are stored. In case there is no BIS board, the BIS channels are ignored. Also, the access
//! to the white rabbit data (B_MEAS) is granted if the board is present.

void regFgc3ProgScanBoards(void);


//! Prints the data of the RegFgc3 related HW/FW.
//!
//! @param fd[in]      File descriptor where to output the data.
//! @param delim[in]   Delimiter for the output string.

void regFgc3ProgSlotPrint(FILE * fd, char delim, uint8_t from, uint8_t to);


//! Gets information on how to slice a FW binary to send it to a RegFgc3 board.
//!
//! The FGC3 sends a task to a given device in an FGC3 board, and it returns data
//! that allow the FGC3 to split a given FW binary into chunks, that are then sent to
//! a given RegFgc3 board.
//!
//! @return uint16_t Error code of the RegFgc3 task sent to the card.

uint16_t regFgc3ProgGetSlicingInfo(void);


//! Returns whether a board has been found in a given slot.
//!
//! @param slot[in] Slot number.
//! @return true    If the board has been found in the slot.
//! @return false   If the board has not been found in the slot.

bool regFgc3ProgIsBoardPresent(uint8_t const slot);


//! Returns whether a device has been found in a given board.
//!
//! @param slot[in]     Slot number.
//! @param device[in]   Device number. //TODO: change to device enum type.
//! @return true    If the device has been found in the board.
//! @return false   If the device has not been found in the board.

bool regFgc3ProgIsDevicePresent(uint8_t const slot, uint8_t const device);


//! Maps a board type with its name.
//!
//! @param board_type[in]   Board type.
//! @return char*           Name of the board for the given board type.

char * regFgc3ProgGetBoardName(uint16_t const board_type);

bool regFgc3ProgIsBoardInDB(uint8_t const slot);


// EOF
