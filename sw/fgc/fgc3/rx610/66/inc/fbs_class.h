//! @file  fbs_class.h
//! @brief Handlers to published data variables

#pragma once


// ---------- Includes

#include <fbs.h>



// ---------- External structures, unions and enumerations

#define FAULTS          fbs.u.fgc_stat.class_data.c66.st_faults
#define WARNINGS        fbs.u.fgc_stat.class_data.c66.st_warnings
#define ST_LATCHED      fbs.u.fgc_stat.class_data.c66.st_latched
#define ST_UNLATCHED    fbs.u.fgc_stat.class_data.c66.st_unlatched
#define STATE_PLL       fbs.u.fgc_stat.class_data.c66.state_pll
#define STATE_OP        fbs.u.fgc_stat.class_data.c66.state_op
#define STATE_VS        fbs.u.fgc_stat.class_data.c66.state_vs
#define STATE_PC        fbs.u.fgc_stat.class_data.c66.state_pc
#define ST_MEAS_A       fbs.u.fgc_stat.class_data.c66.st_adc_a
#define ST_MEAS_B       fbs.u.fgc_stat.class_data.c66.st_adc_b
#define ST_MEAS_C       fbs.u.fgc_stat.class_data.c66.st_adc_c
#define ST_MEAS_D       fbs.u.fgc_stat.class_data.c66.st_adc_d


// EOF
