//! @file  prop_class.c
//! @brief FGC_64 class specific property initialisation


// ---------- Includes

#include <defprops.h>
#include <prop_class.h>



// ---------- Internal variable definitions

static char devtype[] =
{
    "RFNA"
};



// ---------- External variable definitions

// List of properties to be initialized with default values

// CAUTION: If the property is NON-VOLATILE, its default size (numels in the
//          XML definition) must be zero for it to be initialised based on the
//          data in prop_class_init_nvs

// Do not put in the external RAM! Initialisation will stop working!

struct init_prop prop_class_init_nvs[] =
{
    // Non-volatile, non-config properties

    {   &PROP_FGC_NAME,             sizeof(devtype), &devtype[0]     },
    {   &PROP_DEVICE_TYPE,          sizeof(devtype), &devtype[0]     },

    // Global config properties

    {   &PROP_BARCODE_FGC_CASSETTE, 0,               NULL            },

    // Normal properties

    {   NULL   }
};


uint32_t prop_class_init_spy_mpx[FGC_N_SPY_CHANS] =
{
    FGC_SPY_V_A,      FGC_SPY_V_B,
    FGC_SPY_V_C,      FGC_SPY_V_D,
    FGC_SPY_T_FGC_IN, FGC_SPY_T_FGC_OUT
};


// The DF_TIMESTAMP_SELECT dynamic flag is set for all the properties in the
// following list and their children. The same list is also used in function
// CmdPrintTimestamp to read the timestamp selector.

struct Prop_class_init_dynflag_timestamp_select prop_class_init_dynflag_timestamp_select[] =
{
    { NULL }
};


// EOF
