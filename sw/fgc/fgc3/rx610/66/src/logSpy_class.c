//! @file     logSpy_class.c
//! @brief    Class specific logging related functions


// ---------- Includes

#include <stdbool.h>

#include <fgc_errs.h>
#include <logSpy.h>

#include <defprops_inc_class.h>
#include <diag_class.h>
#include <logSpy_class.h>



// ---------- Internal function definitions



// ---------- Platform/class specific function definitions

void logSpyClassInit(void)
{
    ; // Do nothing
}



void logSpyClassEnableDisableLogs(void)
{
    ; // Do nothing
}



uint16_t logSpyClassProcessAlias(uint32_t const menu_idx)
{
	// Do nothing

    return FGC_OK_NO_RSP;
}



bool logSpyClassIsActiveAlias(uint32_t const menu_idx)
{
	// Do nothing

    return false;
}



void logSpyClassInterceptLogNames(union LOG_header * const log_header, enum LOG_menu_index const menu_index)
{
    ; // Do nothing
}
 

// EOF
