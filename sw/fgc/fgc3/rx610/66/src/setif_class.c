//! @file  setif_class.c
//! @brief Class specific Setif command functions


// ---------- Includes

#include <defprops.h>

#include <cmd.h>



// ---------- External function definitions

uint16_t SetifRegStateNone(struct cmd * c)
{
	return SetifPcOff(c);
}


// EOF
