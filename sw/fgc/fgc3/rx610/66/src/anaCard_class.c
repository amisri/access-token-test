//! @file      anaCard_class.c
//! @brief     FGC3 analogue card interface (stubbed for this class)


// Includes

#include "fgc3/rx610/inc/anaCard.h"



// External variable definitions

struct Ana_card_adc adc;

struct Ana_card_temp_regul __attribute__((section("sram"))) ana_temp_regul;



// External function definitions

void anaCardInit(void)
{
    // Analogue interface temperature regulation parameters

    ana_temp_regul.prop_gain = 0.0F;
    ana_temp_regul.int_gain  = 0.0F;
    ana_temp_regul.diff_gain = 0.0F;
    ana_temp_regul.adc_gain  = 0.0F;
}



void anaCardCpyMpx(void)
{
    // Do nothing;
}



void anaCardTempInit(void)
{
    // Do nothing;
}



void anaCardTempRegulStart(float ref_temp)
{
    // Do nothing;
}



void anaCardTempRegul(void)
{
    // Do nothing;
}



float anaCardGetTemp(void)
{
    float temp = 666.6;

    return temp;
}



bool anaCardTempRegulOn(void)
{
    return false;
}


// EOF
