//! @file  diag_class.c
//! @brief Class specific functionality for diagnostics


// ---------- Includes



// ---------- External function definitions

void diagClassInit(void)
{
    // Do nothing;
}



void diagClassCheckConverter(void)
{
    // Do nothing;
}



void diagClassClrAllFaults(void)
{
    // Do nothing;
}



void diagClassUpdateMeas(void)
{
    // Do nothing;
}




// EOF
