//! @file   statePc.c
//! @brief  Common code for the Power Converter state


// ---------- Includes

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <dpcom.h>
#include <status.h>
#include <statePc.h>
#include <pc_state.h>
#include <postMortem.h>
#include <status.h>



// ---------- Internal structures, unions and enumerations

//! Internal local variables for the module statePc

struct statePcLocal
{
    StatePC  * stateFunc;       //!< Pointer to the active state function
    uint32_t   stateTimeMs;     //!< Time in ms the state has bee running
};



// ---------- Internal variable definitions

static struct statePcLocal statePc;



// ---------- External variable declarations

struct StatePc state_pc;



// ------------------------------- Generic STATE FUNCTIONS --------------------------------

// STATE: FLT_OFF (FO)

uint32_t statePcFO(bool firstCall)
{
    if (firstCall == true)
    {
        postMortemTriggerPm();
    }

    return FGC_PC_FLT_OFF;
}



// STATE: OFF (OF)

uint32_t statePcOF(bool firstCall)
{

    return FGC_PC_OFF;
}



// ------------------------------- Generic TRANSITION FUNCTIONS --------------------------------


StatePC * FOtoOF(void)
{
    if (statusGetFaults() == 0)
    {
        return statePcOF;
    }

    return NULL;
}



StatePC * OFtoFO(void)
{
    if (statusGetFaults() != 0)
    {
        return statePcFO;
    }

    return NULL;
}



// ---------- Internal variable definitions

//! State transition functions : called in priority order, from left to right, return the state function.
//!
//! The initialization order of StateTransitions must match the STATE.PC constant values from the XML symlist.
//! This is checked with static assertions.

static StatePCtrans * StateTransitions[][STATE_PC_REF_MAX_TRANSITIONS + 1] =
{
    /*  0.FO */ { FOtoOF, NULL },
    /*  1.OF */ { OFtoFO, NULL },
};



// ---------- External function definitions

void statePcInit(void)
{
    CC_STATIC_ASSERT(FGC_PC_FLT_OFF       ==  0, Unexpected_STATE_PC_constant_value);
    CC_STATIC_ASSERT(FGC_PC_OFF           ==  1, Unexpected_STATE_PC_constant_value);

    // Initialize internal states to FLT_OFF

    pcStateSet(FGC_PC_OFF);

    statePc.stateFunc   = statePcOF;
    statePc.stateTimeMs = 0;

    // Initialize state machine parameters shared with the DSP

    dpcom.mcu.pc.state = FGC_PC_OFF;
}



void statePcProcess(void)
{
    StatePC       * nextState        = NULL;
    StatePCtrans  * transition       = NULL;
    StatePCtrans ** stateTransitions = &StateTransitions[STATE_PC][0];

    // Scan transitions for the current state, testing each condition in priority order

    while (   (transition = *(stateTransitions++)) != NULL
           && (nextState  = (*transition)())       == NULL)
    {
        ;
    }

    // If a transition condition was true then switch to the new state

    if (nextState != NULL)
    {
        // Reset the time since last state change

        statePc.stateTimeMs = 0;

        // Run new state function with firstCall flag set to true

        pcStateSet(nextState(true));

        // Cache the state function and bit mask

        statePc.stateFunc = nextState;
    }
    else
    {
        statePc.stateTimeMs += STATE_PC_PERIOD_MS;

        // Call the state function

        statePc.stateFunc(false);
    }
}




StatePC * statePcGetStateFunc(void)
{
    return statePc.stateFunc;
}



uint32_t statePcGetStateTime(void)
{
    return statePc.stateTimeMs;
}



char const * statePcGetStateName(uint8_t state)
{
    static const char stateName[] = "FOOF";

    return (&stateName[(uint16_t)state * 2]);
}


void statePcResetFaults(void)
{
    ; // Do nothing

}


// EOF
