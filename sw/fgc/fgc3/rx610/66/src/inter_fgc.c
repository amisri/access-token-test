//! @file  inter_fgc.c
//! @brief Overwrite generic functionality. This class does not support inter-FGC communication

#define INTER_FGC_GLOBALS


// ---------- Includes

#include <interFgc.h>



// ---------- External function definitions

void interFgcResetRuntimeFaults(void)
{
    ; // Do nothing
}



void interFgcSendPackets(void)
{
    ; // Do nothing
}



void interFgcStartMs(void)
{
    ; // Do nothing
}


void interFgcReceivePacket(void)
{
    ; // Do nothing
}



void interFgcPopulateStatus(void)
{
    ; // Do nothing
}



bool interFgcIsMaster(void)
{
    return false;
}



bool interFgcIsSlave(void)
{
    return false;
}



void interFgcProcess(bool reset)
{
    ; // Do nothing
}



uint16_t interFgcGetSlaveState(void)
{
    return 0;
}


// EOF
