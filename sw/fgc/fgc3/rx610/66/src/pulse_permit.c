//! @file  pulse_permit.c
//! @brief Overwrite generic functions since this class does not support pulse permit


// ---------- Includes

#include <stdbool.h>



// ---------- External function definitions

bool pulsePermitValid(void)
{
    return true;
}



void pulsePermitProcess(void)
{
    ; // Do nothing
}


// EOF
