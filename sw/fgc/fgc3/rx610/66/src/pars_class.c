//! @file     pars_class.c
//! @brief    Parsing of class specific property types


// ---------- Includes

#include <stdint.h>

#include <cmd.h>
#include <prop.h>
#include <fgc_errs.h>



// ---------- External function definitions

uint16_t parsClassValueGet(struct cmd * c, struct prop * p, void ** value)
{
    return FGC_UNKNOWN_ERROR_CODE;
}


// EOF
