//! @file events.c
//! @brief Process timing events


#define FGC_EVENT_GLOBALS


// ---------- Includes

#include <stdint.h>

#include <events.h>
#include <bitmap.h>
#include <cycSim.h>
#include <cycTime.h>
#include <defprops.h>
#include <logEvent.h>
#include <fbs.h>
#include <fgc_event.h>
#include <logSpy.h>
#include <logTime.h>
#include <memmap_mcu.h>
#include <msTask.h>
#include <postMortem.h>
#include <time_fgc.h>



// ---------- External function definitions

void eventsInit(void)
{
    ; // Do nothing
}



void eventsProcess(void)
{
    ; // Do nothing
}



void eventsTask(void * unused)
{
    ; // Do nothing
}


// EOF
