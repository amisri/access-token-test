//! @file  regfgc3_pars_class.c


// ---------- Includes

#include "fgc3/rx610/regfgc3/inc/regfgc3Pars.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3Prog.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3ProgFsm.h"
#include "fgc3/rx610/regfgc3/inc/regfgc3Task.h"

#include "inc/fgc_errs.h"



// ---------- External variable definitions

struct Regfgc3_pars_prop __attribute__((section("sram"))) regfgc3_pars_prop;
struct Regfgc3_prog_prop regfgc3_prog_prop __attribute__((section("sram")));
OS_SEM * regfgc3_task_sem_go;



// ---------- External function definitions

uint16_t regFgc3ParsGetBlockFromHw(uint8_t           slot, 
                                   enum Scivs_device device, 
                                   uint8_t           block, 
                                   int32_t         * cache_block)
{
    return FGC_NOT_IMPL;
}



uint16_t regFgc3ParsSendBlockToHw(uint8_t            slot,
                                  enum Scivs_device  device,
                                  uint8_t            block,
                                  int32_t    const * params)
{
    return FGC_NOT_IMPL;
}



void regFgc3ParsSendParamsToHw(void)
{
    ; // Do nothing
}



void regFgc3ProgSetJobInPb(void)
{
    ; // Do nothing
}



bool regFgc3ProgThereIsProgFault(void)
{
    return false;
}



bool regFgc3ProgSwitchProductionFailed(void)
{
    return false;
}



void regFgc3ProgSetState(uint16_t state)
{
    ; // Do nothing
}



void regFgc3ProgMgrInit(void)
{
    ; // Do nothing
}



uint64_t regFgc3ProgGetMezzanineId(uint8_t slot)
{
    return 0;
}



void regFgc3ProgSlotPrint(FILE *fd, char delim, uint8_t from, uint8_t to)
{
    ; // Do nothing
}



uint16_t regFgc3ProgSetAction(uint16_t action)
{
    return SCIVS_TSK_ERR_INVALID_OP;
}



void regFgc3ProgFsmInit(void)
{
    ; // Do nothing
}



uint16_t regFgc3ProgFsmSetMode(uint8_t mode)
{
    return FGC_BAD_STATE;
}



void regFgc3ProgSetMode(uint16_t mode)
{
    ; // Do nothing
}



void regFgc3TaskAwake(void)
{
    ; // Do nothing
}



void regFgc3TaskSetJob(enum Regfgc3_task_job_type job)
{
    ; // Do nothing
}



void regFgc3Task(void * unused)
{
    for (;;)
    {
        // Wait for the semaphore to be signalled and then... do nothing.
        // The point is, we must not return from the task, or it's a family trip to Crash City
        OSSemPend(regfgc3_task_sem_go);
    }
}


// EOF

