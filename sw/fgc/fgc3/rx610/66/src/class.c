//! @file  class.c
//! @brief This file contains all class related definitions


// ---------- Includes

#include <hash.h>


// Instantiate variables in headers with no associated source files

#define OS_GLOBALS
#define SHARED_MEMORY_GLOBALS
#define FGC_PARSER_CONSTS
#define FGC_COMPDB_GLOBALS
#define FGC_SYSDB_GLOBALS
#define FGC_DIMDB_GLOBALS
#define CLASS_GLOBALS
#define FGC_TASK_TRACE_GLOBALS
#define PC_STATE_GLOBALS
#define CRATE_GLOBALS
#define DEFPROPS
#define DEFSYMS


#include <os.h>
#include <dpcls.h>
#include <dpcom.h>
#include <dpcmd.h>
#include <sharedMemory.h>
#include <fgc_parser_consts.h>
#include <class.h>
#include <taskTrace.h>
#include <pc_state.h>
#include <modePc.h>
#include <statePc.h>
#include <crate.h>
#include <diag_class.h>
#include <defprops.h>
#include <defsyms.h>



// ---------- External variable definitions

volatile struct Dpcom __attribute__((section("dpcom"))) dpcom;
volatile struct Dpcls __attribute__((section("dpcls"))) dpcls;

OS_SEM * dpcmd_sem;



// ---------- Platform/class specific function definitions

void classInit(void)
{
    // Default initialisations

    device.max_user            = 0;
    device.max_sub_devs        = 0;
    vs.present                 = FGC_CTRL_DISABLED;
    dpcom.mcu.device_ppm       = FGC_CTRL_DISABLED;
    dpcom.mcu.device_multi_ppm = FGC_CTRL_DISABLED;
}


// EOF
