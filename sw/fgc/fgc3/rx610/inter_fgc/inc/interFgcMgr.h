//! @file  interFgcMgr.h
//! @brief

#pragma once


// ---------- Includes

#include <stdint.h>
#include <assert.h>



// ---------- External structures, unions and enumerations

//! Enumerates available inter-FGC signals

enum Inter_fgc_mgr_signal_index
{
    IFGC_SIGNAL_INDEX_NONE,
    IFGC_SIGNAL_INDEX_I_MEAS,
    IFGC_SIGNAL_INDEX_B_MEAS,
    IFGC_SIGNAL_INDEX_V_CAPA,
    IFGC_SIGNAL_INDEX_V_LOAD,
    IFGC_SIGNAL_INDEX_B_PROBE_A,
    IFGC_SIGNAL_INDEX_B_PROBE_B,
    IFGC_SIGNAL_INDEX_B_DOT,
    IFGC_SIGNAL_INDEX_V_REF,
    IFGC_SIGNAL_INDEX_COUNT,
};



//! Enumerates consumed inter-FGC signals

enum Inter_fgc_mgr_consumed_sigs
{
    CONSUMED_SIGS_NONE,
    CONSUMED_SIGS_I_DCCT_A,
    CONSUMED_SIGS_I_DCCT_B,
    CONSUMED_SIGS_V_CAPA    = IFGC_SIGNAL_INDEX_V_CAPA,
    CONSUMED_SIGS_V_LOAD    = IFGC_SIGNAL_INDEX_V_LOAD,
    CONSUMED_SIGS_B_PROBE_A = IFGC_SIGNAL_INDEX_B_PROBE_A,
    CONSUMED_SIGS_B_PROBE_B = IFGC_SIGNAL_INDEX_B_PROBE_B,
    CONSUMED_SIGS_B_DOT     = IFGC_SIGNAL_INDEX_B_DOT,
    CONSUMED_SIGS_V_REF     = IFGC_SIGNAL_INDEX_V_REF,
    CONSUMED_SIGS_COUNT,
};

// static_assert((uint16_t)IFGC_SIGNAL_INDEX_COUNT == (uint16_t)CONSUMED_SIGS_COUNT, "Enums not in sync");


//! Inter-FGC signal structure

struct Inter_fgc_mgr_sig
{
    uint32_t is_valid;
    float    signal;
};


//!

struct Inter_fgc_mgr
{
    uint32_t                 recv_time_us;          // Microsecond time within the second when the most recent packet was unpacked
    struct Inter_fgc_mgr_sig produced_sigs[IFGC_SIGNAL_INDEX_COUNT];
    struct Inter_fgc_mgr_sig consumed_sigs[CONSUMED_SIGS_COUNT];
    uint32_t                 is_enabled   [CONSUMED_SIGS_COUNT];
    uint32_t                 num_converters;
    float                    ext_ref;
    uint32_t                 is_master;
    uint32_t                 is_slave;
    uint32_t                 is_ref_slave;
    uint32_t                 is_producer;
    uint32_t                 is_consumer;
    uint32_t                 topology;
};


// EOF
