//! @file
//!@brief

#pragma once


// ---------- Includes

#include <stdint.h>
#include <assert.h>



// ---------- External structures, unions and enumerations

enum Synced_var_type
{
    SYNCED_VAR_TYPE_SIGNAL,
    SYNCED_VAR_TYPE_UINT16,
    SYNCED_VAR_TYPE_COUNT,
};



enum Synced_var_signal
{
    SYNCED_VAR_V_LOAD,
    SYNCED_VAR_PC_STATE,
    SYNCED_VAR_COUNT,
};


//! Inter-FGC signal structure

struct Synced_var_block_signal
{
    uint8_t  type;        //<! Signal type
    uint8_t  is_valid;    //<! Signal validity flag
    uint16_t reserved;    //<! Reserved for future use
    float    signal;      //<! Signal value
};

static_assert(sizeof(struct Synced_var_block_signal) % 4 == 0, "Structure isn't aligned to long word boundary");
static_assert(sizeof(struct Synced_var_block_signal) == 2 * sizeof(uint8_t) + sizeof(uint16_t) + sizeof(float), "Possible padding");


//! Holds information for a variable

struct Synced_var
{
    enum Synced_var_type   type;          //!< Type of partial data stored in the variable
    void                 * rx_array;      //!< Buffer for partial data from all sources
    uint16_t               rx_bitmap;     //!< Bitmap telling which pieces of data were received in this iteration
    uint16_t               rx_bitmask;    //!< Bitmask defining which elements of the Rx buffer are expected to be filled with partial data during one iteration
    void                (* processing_func)(struct Synced_var const * const);    //!< Function that processes data stored in the Rx buffer
};

static_assert(sizeof(((struct Synced_var *)0)->rx_bitmap) == sizeof(((struct Synced_var *)0)->rx_bitmask), "Mismatch in size of certain structure members");



// ---------- External function declarations

//! Initialises given synced variable
//!
//! @param[in]  signal           Signal to initialise
//! @param[in]  type             Type that will be assigned to the variable
//! @param[in]  rx_array_buffer  Pointer to the Rx buffer. Number of elements must agree with the number
//!                              of elements on the Rx list. Size of element must agree with the type.
//! @param[in]  processing_func  Pointer to the variable processing function

void syncedVarInit(enum Synced_var_signal signal,
                   enum Synced_var_type   type,
                   void                 * rx_array_buffer,
                   void                (* processing_func)(struct Synced_var const * const));



//! Stores given value in the Rx buffer of the variable
//!
//! This function must be protected from being called at the same time as
//! syncedVarProcess!
//!
//! @param[in]  synced_var  Synced variable that should be updated
//! @param[in]  rx_index    Index of the element in the synced var buffer
//! @param[in]  value       Pointer to the value that should be saved

void syncedVarStore(enum Synced_var_signal  signal,
                    uint16_t                rx_index,
                    void            const * value);


//! Processes all variables and prepares them for the next iteration
//!
//! If the data pieces have been received from all expected sources,
//! then the processing function will be called. If there are any pieces
//! missing, then all data will be discarded and no processing will be done.
//!
//! This function must be protected from being called at the same time as
//! syncedVarStore!

void syncedVarProcess(void);


//! Clears reception bitmasks of all synced variables

void syncedVarDisable(void);


//! Sets reception bitmask of the synced variable

void syncedVarSetRxBitmask(enum Synced_var_signal signal, uint16_t rx_bitmask);


// EOF
