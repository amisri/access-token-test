//! @file  interFgc.h
//! @brief Inter FGC communication

#pragma once


// ---------- Includes

#include <stdint.h>
#include <assert.h>

#include <interFgcMgr.h>
#include <fgc_errs.h>
#include <cmd.h>
#include <property.h>
#include <cclibs/libreg/inc/libreg/regDecoStructs.h>



// ---------- Constants

#define MAX_INTER_FGC_SLAVES         12
#define MAX_INTER_FGC_MASTERS        1
#define MAX_INTER_FGC_RX             MAX_INTER_FGC_SLAVES
#define MAX_INTER_FGC_UNEXPECTED     1
#define MAX_INTER_FGC_TX             6
#define MAX_INTER_FGC_SIG_SOURCES    4
#define MAX_INTER_FGC_CONSUMERS      MAX_INTER_FGC_TX
#define MAX_INTER_FGC_DECO           (REG_NUM_DECO-1)
#define MAX_INTER_FGC_PRODUCED_SIGS  4

static_assert(MAX_INTER_FGC_RX      >= MAX_INTER_FGC_SLAVES,      "Rx list can't hold all slaves");
static_assert(MAX_INTER_FGC_TX      >= MAX_INTER_FGC_CONSUMERS,   "Tx list can't hold all consumers");
static_assert(MAX_INTER_FGC_RX      >= MAX_INTER_FGC_SIG_SOURCES, "Rx list can't hold all signal sources");
static_assert(MAX_INTER_FGC_MASTERS == 1,                         "Only one master is allowed");

#define NUM_MAC_OCTETS      6

enum Inter_fgc_master_transitions
{
    MASTER_TRANSITION_NONE,
    MASTER_TRANSITION_FO_TO_OF,
    //                OF_TO_FO
    MASTER_TRANSITION_OF_TO_ST,
    MASTER_TRANSITION_FS_TO_FO,
    MASTER_TRANSITION_FS_TO_SP,
    MASTER_TRANSITION_SP_TO_OF,
    //                ST_TO_SP
    MASTER_TRANSITION_ST_TO_BK,
    //                BK_TO_SP
    //                XX_TO_FS
    //                XX_TO_XX
    MASTER_TRANSITION_COUNT,
};



// ---------- External structures, unions and enumerations

//! MAC address structure

struct Inter_fgc_mac
{
    uint8_t octets[NUM_MAC_OCTETS];
};


//! Contains INTER_FGC.STATUS.RX properties
//!
//! These properties contain the Rx list alongside with some handful
//! statistics on the received packets.

struct Inter_fgc_status_rx
{
    struct Inter_fgc_mac names       [MAX_INTER_FGC_RX];    //!< INTER_FGC.STATUS.RX.NAMES
    uint16_t             roles       [MAX_INTER_FGC_RX];    //!< INTER_FGC.STATUS.RX.ROLES
    uint16_t             signals     [MAX_INTER_FGC_RX];    //!< INTER_FGC.STATUS.RX.SIGNALS
    uint32_t             missing     [MAX_INTER_FGC_RX];    //!< INTER_FGC.STATUS.RX.MISSING
    uint32_t             excessive   [MAX_INTER_FGC_RX];    //!< INTER_FGC.STATUS.RX.EXCESSIVE
    uint32_t             invalid     [MAX_INTER_FGC_RX];    //!< INTER_FGC.STATUS.RX.INVALID
    uint16_t             invalid_type[MAX_INTER_FGC_RX];    //!< INTER_FGC.STATUS.RX.INVALID_TYPE
    uint32_t             n_elements;                        //!< Number of elements of INTER_FGC.STATUS.RX.* properties
};


//! Contains INTER_FGC.STATUS.UNEXPECTED properties
//!
//! These properties give informations about inter-FGC packets arriving
//! from unexpected MAC addresses, be it FGCs or others

struct Inter_fgc_status_unexpected
{
    struct Inter_fgc_mac names[MAX_INTER_FGC_UNEXPECTED];  //!< INTER_FGC.STATUS.UNEXPECTED.NAMES
    uint32_t             counter;                          //!< INTER_FGC.STATUS.UNEXPECTED.COUNTER
};


//! Contains INTER_FGC.STATUS.TX properties
//!
//! These properties contain the Tx list alongside with some statistics
//! on the transmitted packets.

struct Inter_fgc_status_tx
{
    struct Inter_fgc_mac names[MAX_INTER_FGC_TX];  //!< INTER_FGC.STATUS.TX.NAMES
    uint16_t             roles;                    //!< INTER_FGC.STATUS.TX.ROLES
    uint16_t             rate;                     //!< INTER_FGC.STATUS.TX.RATE
    uint32_t             sent;                     //!< INTER_FGC.STATUS.TX.SENT
    uint32_t             failed;                   //!< INTER_FGC.STATUS.TX.FAILED
};


//! Contains INTER_FGC.STATUS properties

struct Inter_fgc_status
{
    uint16_t                   faults;     //<! INTER_FGC.STATUS.FAULTS
    uint16_t                   warnings;   //<! INTER_FGC.STATUS.WARNINGS
    struct Inter_fgc_status_rx rx;         //<! INTER_FGC.STATUS.RX.*
    struct Inter_fgc_status_tx tx;         //<! INTER_FGC.STATUS.TX.*
    struct Inter_fgc_status_unexpected unexpected;    //<! INTER_FGC.STATUS.UNEXPECTED.*
};


//! Contains INTER_FGC.TEST properties
//!
//! These properties can be used to trigger special behavior,
//! which can be useful for testing, and would be difficult to obtain during normal operation.

struct Inter_fgc_test
{
    uint16_t action;         //!< INTER_FGC.TEST.ACTION
    uint32_t num_packets;    //!< INTER_FGC.TEST.NUM_PACKETS
};


//! Contains INTER_FGC.DEBUG properties
//!
//! These properties can be used by experts to get extra information
//! about inter-FGC communication.

struct Inter_fgc_debug
{
    uint16_t num_collisions;          //!< INTER_FGC.DEBUG.NUM_COLLISIONS
    uint16_t start_ms_duration_us;    //!< INTER_FGC.DEBUG.START_MS_DURATION_US
};


//! Main inter-FGC structure
//!
//! The structure contains all INTER_FGC properties

struct Inter_fgc_prop
{
    uint16_t                  cmd;                                           //!< INTER_FGC.DEBUG.COMMANDS
    uint16_t                  slave_simplified_state;                        //!< INTER_FGC.DEBUG.SLAVE_STATE
    uint16_t                  broadcast;                                     //!< INTER_FGC.BROADCAST
    uint16_t                  isr_offset_us;                                 //!< INTER_FGC.ISR_OFFSET_US
    uint16_t                  topology;                                      //!< INTER_FGC.TOPOLOGY
    char                    * sig_sources  [MAX_INTER_FGC_SIG_SOURCES];      //!< INTER_FGC.SIG_SOURCES
    char                    * slaves       [MAX_INTER_FGC_SLAVES];           //!< INTER_FGC.SLAVES
    char                    * master       [MAX_INTER_FGC_MASTERS];          //!< INTER_FGC.MASTERS
    char                    * consumers    [MAX_INTER_FGC_CONSUMERS];        //!< INTER_FGC.CONSUMERS
    char                    * deco         [MAX_INTER_FGC_DECO];             //!< INTER_FGC.DECO.PARTNERS
    uint16_t                  produced_sigs[MAX_INTER_FGC_PRODUCED_SIGS];    //!< INTER_FGC.PRODUCED_SIGS
    struct Inter_fgc_status   status;                                        //!< INTER_FGC.STATUS.*
    struct Inter_fgc_test     test;                                          //!< INTER_FGC.TEST.*
    struct Inter_fgc_debug    debug;                                         //!< INTER_FGC.DEBUG.*
};



// ---------- External Variables Declarations

extern struct Inter_fgc_prop inter_fgc_prop;



// External functions declarations

//! Resets runtime inter-FGC faults

void interFgcResetRuntimeFaults(void);


//! Initialises inter-FGC communication

void interFgcInit(struct Inter_fgc_mgr * inter_fgc_mgr);


//! Sends inter-FGC packets
//!
//! This function should be called from an ISR.

void interFgcSendPackets(void);


//! Performs health checks and packet reception bookkeeping
//!
//! This function should be called at start of each millisecond.

void interFgcStartMs(void);


//! Processes a single received inter-FGC packet
//!
//! This function should be called from an ISR.

void interFgcReceivePacket(void);


//! Processes role properties

void interFgcPopulateStatus(void);


//! Processes the the commands, status and state.
//!
//! @param[in]  reset  Indicates that reset command is active

void interFgcProcess(bool reset);


enum fgc_errno interFgcGetMac(struct cmd * c, struct prop * p);
enum fgc_errno interFgcSetDevName(struct cmd * c, struct prop * p);


bool interFgcIsMaster(void);

bool interFgcIsSlave(void);

uint16_t interFgcGetSlaveState(void);


// EOF
