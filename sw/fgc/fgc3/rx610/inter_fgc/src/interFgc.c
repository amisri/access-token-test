//! @file  inter_fgc.c
//! @brief Inter FGC communication

#define INTER_FGC_GLOBALS


// ---------- Includes

#include <string.h>
#include <assert.h>

#include <interFgc.h>
#include <syncedVar.h>

#include <sleep.h>
#include <databases.h>
#include <histogram.h>
#include <etherComm.h>
#include <fbs_class.h>
#include <lan92.h>
#include <msTask.h>
#include <pc_state.h>
#include <sta.h>
#include <time_fgc.h>
#include <defprops.h>
#include <defsyms.h>
#include <pars.h>
#include <digitalIo.h>
#include <statePc.h>
#include <modePc.h>
#include <status.h>



// ---------- Constants

#define INTER_FGC_ISR_INIT_OFFSET_US      400
#define INTER_FGC_NUM_EXPECTED_PACKETS_MS 1
#define INTER_FGC_MAX_INVALID_CONSECUTIVE 2
#define INTER_FGC_MAX_INVALID_PER_SECOND  10
#define INTER_FGC_VERSION                 1
#define INTER_FGC_DEV_NAME_STR_LEN        (ST_MAX_SYM_LEN + FGC_MAX_DEV_LEN + 1 + 1)  //<! Same as prop_type_size[PT_DEV_NAME]. One character for '@' and a second for '\0'
#define INTER_FGC_MAX_PAYLOAD_SIZE        0x40

#define INTER_FGC_CONFIG_FAULTS         ( FGC_IFGC_FAULTS_NUM_RX_DEVS   | \
                                          FGC_IFGC_FAULTS_NUM_TX_DEVS   | \
                                          FGC_IFGC_FAULTS_CLASHING_SIGS | \
                                          FGC_IFGC_FAULTS_TOPOLOGY      | \
                                          FGC_IFGC_FAULTS_VS_PRESENT    )

#define INTER_FGC_RUNTIME_FAULTS        ( FGC_IFGC_FAULTS_NUM_INV_CON  | \
                                          FGC_IFGC_FAULTS_NUM_INV_SEC  | \
                                          FGC_IFGC_FAULTS_ACTUATION    | \
                                          FGC_IFGC_FAULTS_VS_BLOCKABLE | \
                                          FGC_IFGC_FAULTS_SLAVE_FAULT  | \
                                          FGC_IFGC_FAULTS_DECO_INDEX   )



// ---------- Internal structures, unions and enumerations

enum Inter_fgc_slave_flag
{
    SLAVE_FLAG_FAULT         = 0x01,
    SLAVE_FLAG_WARNING       = 0x02,
    SLAVE_FLAG_FAST_ABORT    = 0x04,
    SLAVE_FLAG_SIM           = 0x08,
    SLAVE_FLAG_NON_BLOCKABLE = 0x10,
};


//! Wrapper around symlist constants

enum Inter_fgc_role
{
    ROLE_NONE     = 0,
    ROLE_MASTER   = FGC_IFGC_ROLES_MASTER,
    ROLE_SLAVE    = FGC_IFGC_ROLES_SLAVE,
    ROLE_PRODUCER = FGC_IFGC_ROLES_PRODUCER,
    ROLE_DECO     = FGC_IFGC_ROLES_DECO,
    ROLE_COUNT,
};


enum Inter_fgc_block_type
{
    BLOCK_NONE,
    BLOCK_MASTER,
    BLOCK_SLAVE,
    BLOCK_PRODUCER,
    BLOCK_DECO,
    BLOCK_COUNT,
};


//! Wrapper around symlist constants

enum Inter_fgc_signal_bitmask
{
    IFGC_SIGNAL_BITMASK_NONE      = 0,
    IFGC_SIGNAL_BITMASK_I_MEAS    = FGC_IFGC_SIGNALS_I_MEAS,
    IFGC_SIGNAL_BITMASK_B_MEAS    = FGC_IFGC_SIGNALS_B_MEAS,
    IFGC_SIGNAL_BITMASK_V_CAPA    = FGC_IFGC_SIGNALS_V_CAPA,
    IFGC_SIGNAL_BITMASK_V_LOAD    = FGC_IFGC_SIGNALS_V_LOAD,
    IFGC_SIGNAL_BITMASK_B_PROBE_A = FGC_IFGC_SIGNALS_B_PROBE_A,
    IFGC_SIGNAL_BITMASK_B_PROBE_B = FGC_IFGC_SIGNALS_B_PROBE_B,
    IFGC_SIGNAL_BITMASK_B_DOT     = FGC_IFGC_SIGNALS_B_DOT,
    IFGC_SIGNAL_BITMASK_V_REF     = FGC_IFGC_SIGNALS_V_REF,
    IFGC_SIGNAL_BITMASK_COUNT,
};


//! Wrapper around symlist constants

enum Inter_fgc_inv_type_bitmask
{
    IFGC_INV_TYPE_VERSION       = FGC_IFGC_INV_TYPE_VERSION,
    IFGC_INV_TYPE_MISSING_SIG   = FGC_IFGC_INV_TYPE_MISSING_SIG,
    IFGC_INV_TYPE_MISSING_BLOCK = FGC_IFGC_INV_TYPE_MISSING_BLOCK,
    IFGC_INV_TYPE_UNKNOWN_BLOCK = FGC_IFGC_INV_TYPE_UNKNOWN_BLOCK,
};


//! Wrapper around symlist constants

enum Inter_fgc_fault
{
    IFGC_FAULT_NUM_INV_CON   = FGC_IFGC_FAULTS_NUM_INV_CON,
    IFGC_FAULT_NUM_INV_SEC   = FGC_IFGC_FAULTS_NUM_INV_SEC,
    IFGC_FAULT_NUM_RX_DEVS   = FGC_IFGC_FAULTS_NUM_RX_DEVS,
    IFGC_FAULT_NUM_TX_DEVS   = FGC_IFGC_FAULTS_NUM_TX_DEVS,
    IFGC_FAULT_CLASHING_SIGS = FGC_IFGC_FAULTS_CLASHING_SIGS,
    IFGC_FAULT_TOPOLOGY      = FGC_IFGC_FAULTS_TOPOLOGY,
    IFGC_FAULT_ACTUATION     = FGC_IFGC_FAULTS_ACTUATION,
    IFGC_FAULT_VS_PRESENT    = FGC_IFGC_FAULTS_VS_PRESENT,
    IFGC_FAULT_VS_BLOCKABLE  = FGC_IFGC_FAULTS_VS_BLOCKABLE,
    IFGC_FAULT_DECO_INDEX    = FGC_IFGC_FAULTS_DECO_INDEX,
};


//! Wrapper around symlist constants

enum Inter_fgc_warning
{
    IFGC_WARNINGS_UNEXP_COMM = FGC_IFGC_WARNINGS_UNEXP_COMM,
    IFGC_WARNINGS_TEST       = FGC_IFGC_WARNINGS_TEST,
};


//! Wrapper around symlist constants

enum Inter_fgc_actuation
{
#if (FGC_CLASS_ID == 63)
    ACTUATION_CURRENT_REF = 0,
    ACTUATION_VOLTAGE_REF = FGC_ACT_TYPE_VOLTAGE_REF,
    ACTUATION_FIRING_REF  = FGC_ACT_TYPE_FIRING_REF,
#else
    ACTUATION_CURRENT_REF = 0,
    ACTUATION_VOLTAGE_REF = 1,
    ACTUATION_FIRING_REF  = 2,
#endif
    ACTUATION_NONE,    // Isn't defined on the symlist
};


//! Wrapper around symlist constants

enum Inter_fgc_topology
{
    IFGC_TOPOLOGY_NONE   = FGC_IFGC_TOPOLOGY_NONE,
    IFGC_TOPOLOGY_SERIES = FGC_IFGC_TOPOLOGY_SERIES,
    IFGC_TOPOLOGY_SINGLE = FGC_IFGC_TOPOLOGY_SINGLE,
    IFGC_TOPOLOGY_STATE  = FGC_IFGC_TOPOLOGY_STATE,
};


//! Enlists commands used to drive slaves' PC state machine

enum Inter_fgc_command_bitmask
{
    IFGC_CMD_VSRUN   = DIG_OP_SET_VSRUNCMD_MASK16,
    IFGC_CMD_RESET   = DIG_OP_SET_VSRESETCMD_MASK16,
    IFGC_CMD_UNBLOCK = DIG_OP_SET_UNBLOCKCMD_MASK16,
    IFGC_CMD_FGCOK   = DIG_OP_SET_FGCOKCMD_MASK16,
};



//! Helper structure used to pair properties with inter-FGC roles

struct Inter_fgc_prop_role
{
    struct prop const   * prop;    //!< Pointer to property structure
    enum Inter_fgc_role   role;    //!< Inter-FGC role assigned to the property
};


//! Inter-FGC master payload structure

struct Inter_fgc_payload_master
{
    float   ref;          //<! Reference value
    uint8_t actuation;    //<! Type of reference
    uint8_t reg_mode;     //<! Regulation mode of the master
    uint8_t commands;     //<! Control word, similar to FGC digital commands
    uint8_t flags;        //<! Various flags
};

static_assert(sizeof(struct Inter_fgc_payload_master) == 4 * sizeof(uint8_t) + sizeof(float), "Possible padding");
static_assert(sizeof(struct Inter_fgc_payload_master) % 4 == 0, "Structure isn't aligned to long word boundary");


//! Inter-FGC slave payload structure

struct Inter_fgc_payload_slave
{
    uint8_t  pc_state;    //!< STATE.PC of the slave
    uint8_t  flags;       //!< Various flags
    uint16_t reserved;    //!< Reserved for future use
};

static_assert(sizeof(struct Inter_fgc_payload_slave) == 2 * sizeof(uint8_t) + sizeof(uint16_t), "Possible padding");
static_assert(sizeof(struct Inter_fgc_payload_slave) % 4 == 0, "Structure isn't aligned to long word boundary");


//! Inter-FGC producer payload structure

struct Inter_fgc_payload_producer
{
    struct Synced_var_block_signal signals[MAX_INTER_FGC_PRODUCED_SIGS];    //!< Array of inter-FGC signals
};

static_assert(sizeof(struct Inter_fgc_payload_producer) % 4 == 0, "Structure isn't aligned to long word boundary");
static_assert(sizeof(struct Inter_fgc_payload_producer) == MAX_INTER_FGC_PRODUCED_SIGS * sizeof(struct Synced_var_block_signal), "Possible padding");


//! Inter-FGC deco payload structure

struct Inter_fgc_payload_deco
{
    //! Decoupling data shared between controllers

    uint32_t index;       //!< Controller decoupling index (0-3)
    float    v_ref;       //!< Voltage reference (V_REF_FF)
    float    i_meas;      //!< Current measurement (I_MEAS_REG)
};

static_assert(sizeof(struct Inter_fgc_payload_deco) % 4 == 0, "Structure isn't aligned to long word boundary");
static_assert(sizeof(struct Inter_fgc_payload_deco) == sizeof(uint32_t) + 2 * sizeof(float), "Possible padding");


//! Inter-FGC block payload union

union Inter_fgc_block_payload
{
    struct Inter_fgc_payload_master   master;
    struct Inter_fgc_payload_slave    slave;
    struct Inter_fgc_payload_producer producer;
    struct Inter_fgc_payload_deco     deco;
};


//! Inter-FGC block header structure

struct Inter_fgc_block_header
{
    uint8_t  block_type;    //!< Block type
    uint8_t  reserved;      //!< Reserved for future use
    uint16_t block_size;    //!< Block size (including block header) in bytes
};

static_assert(sizeof(struct Inter_fgc_block_header) % 4 == 0, "Structure isn't aligned to long word boundary");
static_assert(sizeof(struct Inter_fgc_block_header) == 2 * sizeof(uint16_t), "Possible padding");


//! Inter-FGC block structure

struct Inter_fgc_block
{
    struct Inter_fgc_block_header  header;
    union  Inter_fgc_block_payload payload;
};

static_assert(sizeof(struct Inter_fgc_block) % 4 == 0, "Structure isn't aligned to long word boundary");
static_assert(sizeof(struct Inter_fgc_block) == sizeof(struct Inter_fgc_block_header) + sizeof(union Inter_fgc_block_payload), "Possible padding");


//! Inter-FGC header structure

struct Inter_fgc_header
{
    uint8_t version;         //!< Version of the inter-FGC protocol
    uint8_t reserved[11];    //!< Reserved for future use
};

static_assert(sizeof(struct Inter_fgc_header) % 4 == 0, "Structure isn't aligned to long word boundary");
static_assert(sizeof(struct Inter_fgc_header) == sizeof(uint8_t) * 12, "Possible padding");


//! Inter-FGC packet structure

struct Inter_fgc_packet
{
    struct fgc_ether_header header;                       //!< FGC-Ether header
    struct Inter_fgc_header Inter_fgc_header;             //!< Inter-FGC header
    uint8_t                 payload[INTER_FGC_MAX_PAYLOAD_SIZE];    //!< Inter-FGC payload
};



//! Transmitted and received packets structure

struct Inter_fgc_packets
{
    struct Inter_fgc_packet tx;    //!< Packet structure used for data transmission
    struct Inter_fgc_packet rx;    //!< Packet structure used for data reception
};


//! Buffers for values of role properties and synced variables

struct Inter_fgc_buffers
{
    char                master     [MAX_INTER_FGC_MASTERS     * INTER_FGC_DEV_NAME_STR_LEN];    //!< Buffer for INTER_FGC.MASTERS property
    char                slaves     [MAX_INTER_FGC_SLAVES      * INTER_FGC_DEV_NAME_STR_LEN];    //!< Buffer for INTER_FGC.SLAVES property
    char                deco       [MAX_INTER_FGC_DECO        * INTER_FGC_DEV_NAME_STR_LEN];    //!< Buffer for INTER_FGC.DECO.PARTNERS property
    char                sig_sources[MAX_INTER_FGC_SIG_SOURCES * INTER_FGC_DEV_NAME_STR_LEN];    //!< Buffer for INTER_FGC.SIG_SOURCES property
    char                consumers  [MAX_INTER_FGC_CONSUMERS   * INTER_FGC_DEV_NAME_STR_LEN];    //!< Buffer for INTER_FGC.CONSUMERS property
    struct Synced_var_block_signal v_load[MAX_INTER_FGC_RX];                                    //!< Buffer for V_LOAD signals coming from the slaves
    uint16_t            pc_state   [MAX_INTER_FGC_RX];                                          //!< Buffer for STATE.PC coming from the slaves
};


//! Helper Rx statistics

struct Inter_fgc_rx_stats
{
    uint16_t received_ms        [MAX_INTER_FGC_RX];    //!< Number of received packets within millisecond
    uint16_t invalid_ms         [MAX_INTER_FGC_RX];    //!< Number of invalid packets within millisecond
    uint16_t invalid_s          [MAX_INTER_FGC_RX];    //!< Number of invalid packets within second
    uint16_t invalid_consecutive[MAX_INTER_FGC_RX];    //!< Number of consecutive invalid packets
};


//! Structure containing local variables

struct Inter_fgc
{
    void                        (*send_packet_function)(struct Inter_fgc_packet * const packet);    //!< Pointer to function for sending inter-FGC packets
    struct Inter_fgc_packets    packets;                //!< Rx and Tx packet structures
    struct Inter_fgc_buffers    buffers;                //!< Buffers for role properties and synced variables
    struct Inter_fgc_rx_stats   rx_stats;               //!< Rx statistics
    uint16_t                    slave_simplified_state; //!< Pc simplified as seen by the inter-FGC
    uint16_t                    test_action;            //!< Test action specified by the user
    bool                        is_enabled;             //!< Indicates that inter-FGC functionality is enabled
    bool                        start_ms_running;       //!< Creates an envelope around start of ms activities
    struct Inter_fgc_mgr      * inter_fgc_mgr;
};



// ---------- External variable definitions

struct Inter_fgc_prop __attribute__((section("sram"))) inter_fgc_prop;



// ---------- Internal variable definitions

static struct Inter_fgc inter_fgc;

static uint8_t const local_Inter_fgc_cmd_simplified[] =
{
    0                                                 ,   // FGC_PC_SIMPLIFIED_FAULT
    IFGC_CMD_FGCOK                                    ,   // FGC_PC_SIMPLIFIED_OFF
    IFGC_CMD_FGCOK | IFGC_CMD_VSRUN                   ,   // FGC_PC_SIMPLIFIED_BLOCKING
    IFGC_CMD_FGCOK | IFGC_CMD_VSRUN | IFGC_CMD_UNBLOCK,   // FGC_PC_SIMPLIFIED_ON
};



// ---------- Internal function definitions

void interFgcResetRuntimeFaults(void)
{
    clrBitmap(inter_fgc_prop.status.faults, INTER_FGC_RUNTIME_FAULTS);

    if (inter_fgc_prop.status.faults == 0)
    {
        statusClrFaults(FGC_FLT_SLAVE);
    }
}



static void interFgcResetConfigFaults(void)
{
    clrBitmap(inter_fgc_prop.status.faults, INTER_FGC_CONFIG_FAULTS);

    if (inter_fgc_prop.status.faults == 0)
    {
        statusClrFaults(FGC_FLT_SLAVE);
    }
}



static inline void interFgcSetFault(enum Inter_fgc_fault const fault)
{
    setBitmap(inter_fgc_prop.status.faults, fault);
}



static inline void interFgcSetWarning(enum Inter_fgc_warning const warning)
{
    setBitmap(inter_fgc_prop.status.warnings, warning);
}



static inline void interFgcClearWarning(enum Inter_fgc_warning const warning)
{
    clrBitmap(inter_fgc_prop.status.warnings, warning);
}



//! Processing function of the V_LOAD synced variable
//!
//! The function adds-up all received voltage measurements and passes that
//! value to the DSP.
//!
//! @param[in]  v_load  Pointer to the V_LOAD synced variable structure

static void interFgcProcessVLoad(struct Synced_var const * const v_load)
{
    bool  is_valid = true;
    float signal   = 0.0;

    uint16_t i;

    struct Synced_var_block_signal const * signals = (struct Synced_var_block_signal *)v_load->rx_array;

    // Iterate over the Rx buffer of the variable

    for (i = 0; i < MAX_INTER_FGC_RX; i++)
    {
        // Check if there's interesting data under that index

        if (testBitmap(v_load->rx_bitmap, (1 << i)))
        {
            // Generate a collective validity flag - mark the measurement as invalid
            // if any of the incoming signal is invalid

            is_valid  = is_valid && signals[i].is_valid;
            signal   += signals[i].signal;
        }
    }

    // Pass the measurement to the DSP

    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_V_LOAD].is_valid = is_valid;
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_V_LOAD].signal   = signal;
}



//! Processing function of the PC_STATE synced variable
//!
//! The function generates the so-called collective PC_STATE of the slaves.
//!
//! @param[in]  pc_state  Pointer to the PC_STATE synced variable structure

static void interFgcProcessPcState(const struct Synced_var * const pc_state)
{
    static uint16_t prev_slave_simplified_state = FGC_PC_SIMPLIFIED_OFF;

    static uint32_t const SLAVE_STATE_ON_BIT_MASK = ( FGC_STATE_IDLE_BIT_MASK
                                                    | FGC_STATE_TO_CYCLING_BIT_MASK
                                                    | FGC_STATE_CYCLING_BIT_MASK
                                                    | FGC_STATE_DIRECT_BIT_MASK);

    uint32_t pc_state_bitmap = 0;
    uint16_t i;

    // Iterate over the Rx buffer of the variable

    for (i = 0; i < MAX_INTER_FGC_RX; i++)
    {
        // Check if there's interesting data under that index

        if (testBitmap(pc_state->rx_bitmap, (1 << i)))
        {
            // Create a bitmap containing a summary of PC states of all slaves

            pc_state_bitmap |= (1 << ((uint16_t *)pc_state->rx_array)[i]);
        }
    }

    inter_fgc_prop.slave_simplified_state = prev_slave_simplified_state;

    if (testBitmap(pc_state_bitmap, FGC_STATE_FLT_STOPPING_BIT_MASK | FGC_STATE_FLT_OFF_BIT_MASK) == true)
    {
        inter_fgc_prop.slave_simplified_state = FGC_PC_SIMPLIFIED_FAULT;
    }

    // There is exactly one bit set in the state bitmap. All slaves have a common state

    else if ((pc_state_bitmap & (pc_state_bitmap - 1)) == 0)
    {

        if (testBitmap(pc_state_bitmap, FGC_STATE_OFF_BIT_MASK) == true)
        {
            inter_fgc_prop.slave_simplified_state = FGC_PC_SIMPLIFIED_OFF;
        }
        else if (testBitmap(pc_state_bitmap, FGC_STATE_BLOCKING_BIT_MASK) == true)
        {
            inter_fgc_prop.slave_simplified_state = FGC_PC_SIMPLIFIED_BLOCKING;
        }
        else if (testBitmap(pc_state_bitmap, SLAVE_STATE_ON_BIT_MASK) == true)
        {
            inter_fgc_prop.slave_simplified_state = FGC_PC_SIMPLIFIED_ON;
        }
    }

    inter_fgc_prop.slave_simplified_state = inter_fgc_prop.slave_simplified_state;
    prev_slave_simplified_state           = inter_fgc_prop.slave_simplified_state;
}



//! Initialises all synced variables

static void interFgcInitSyncedVars(void)
{
    syncedVarInit(SYNCED_VAR_V_LOAD,   SYNCED_VAR_TYPE_SIGNAL, inter_fgc.buffers.v_load,   interFgcProcessVLoad);
    syncedVarInit(SYNCED_VAR_PC_STATE, SYNCED_VAR_TYPE_UINT16, inter_fgc.buffers.pc_state, interFgcProcessPcState);
}



//! Sets reception bitmasks of all synced variables

static void interFgcEnableSyncedVars(void)
{
    static_assert(MAX_INTER_FGC_RX <= 16, "Synced variable reception bitmask too small");

    int i;
    uint16_t v_load_bitmask = 0;
    uint16_t slaves_bitmask = 0;

    for (i = 0; i < inter_fgc_prop.status.rx.n_elements; i++)
    {
        if (testBitmap(inter_fgc_prop.status.rx.signals[i], IFGC_SIGNAL_BITMASK_V_LOAD))
        {
            v_load_bitmask |= (1 << i);
        }

        if (testBitmap(inter_fgc_prop.status.rx.roles[i], ROLE_SLAVE))
        {
            slaves_bitmask |= (1 << i);
        }
    }

    syncedVarSetRxBitmask(SYNCED_VAR_V_LOAD,   v_load_bitmask);
    syncedVarSetRxBitmask(SYNCED_VAR_PC_STATE, slaves_bitmask);
}



//! Initializes histograms related to inter-FGC communication

static void interFgcInitHistograms(void)
{
    histogramComInit(&communication_histograms.rx.fgc,           0, 40);
    histogramComInit(&communication_histograms.tx.fgc,           0, 40);
    histogramComInit(&communication_histograms.rx.fgc_proc_time, 0,  2);
}



//! Initializes FGC ether and inter-FGC headers of the Tx packet

static void interFgcInitTxHeader(void)
{
    // Initialize Ethernet header

    inter_fgc.packets.tx.header                  = ethernet.tx_header;
    inter_fgc.packets.tx.header.fgc.payload_type = FGC_ETHER_PAYLOAD_INTER_FGC;

    // Initialize inter-FGC header

    inter_fgc.packets.tx.Inter_fgc_header.version = INTER_FGC_VERSION;
}



//! Links role properties to proper buffers

static void interFgcInitPropBuffers(void)
{
    int i;

    for (i = 0; i < MAX_INTER_FGC_MASTERS; i++)
    {
        inter_fgc_prop.master[i] = &inter_fgc.buffers.master[i * INTER_FGC_DEV_NAME_STR_LEN];
    }

    for (i = 0; i < MAX_INTER_FGC_SLAVES; i++)
    {
        inter_fgc_prop.slaves[i] = &inter_fgc.buffers.slaves[i * INTER_FGC_DEV_NAME_STR_LEN];
    }

    for (i = 0; i < MAX_INTER_FGC_DECO; i++)
    {
        inter_fgc_prop.deco[i] = &inter_fgc.buffers.deco[i * INTER_FGC_DEV_NAME_STR_LEN];
    }

    for (i = 0; i < MAX_INTER_FGC_SIG_SOURCES; i++)
    {
        inter_fgc_prop.sig_sources[i] = &inter_fgc.buffers.sig_sources[i * INTER_FGC_DEV_NAME_STR_LEN];
    }

    for (i = 0; i < MAX_INTER_FGC_CONSUMERS; i++)
    {
        inter_fgc_prop.consumers[i] = &inter_fgc.buffers.consumers[i * INTER_FGC_DEV_NAME_STR_LEN];
    }
}



//! Passes test inter-FGC packet to the LAN chip
//!
//! This version is used instead of the regular interFgcSendPacket when
//! user specififes a test action in INTER_FGC.TEST properties

static void interFgcSendPacketTest(struct Inter_fgc_packet * const packet)
{
    uint32_t const packet_tag  = packet->header.fgc.payload_type << 16;
    uint16_t const packet_size = sizeof(*packet);

    // TODO Encapsulate test_action constants in an enum

    if (inter_fgc.test_action == FGC_IFGC_TEST_ACTION_INJ_INV_VER)
    {
        inter_fgc.packets.tx.Inter_fgc_header.version = 0;
    }
    else
    {
        inter_fgc.packets.tx.Inter_fgc_header.version = INTER_FGC_VERSION;
    }

    uint8_t err;

    if (inter_fgc.test_action == FGC_IFGC_TEST_ACTION_INJ_EXCESSIVE)
    {
        err  = lan92SendPacketOpt(packet, packet_size, packet_size, TX_CMDA_FIRST_SEG | TX_CMDA_LAST_SEG, packet_tag);
        err |= lan92SendPacketOpt(packet, packet_size, packet_size, TX_CMDA_FIRST_SEG | TX_CMDA_LAST_SEG, packet_tag);
    }
    else
    {
        err  = lan92SendPacketOpt(packet, packet_size, packet_size, TX_CMDA_FIRST_SEG | TX_CMDA_LAST_SEG, packet_tag);
    }

    if (err != 0)
    {
        inter_fgc_prop.status.tx.failed++;
    }
    else
    {
        inter_fgc_prop.status.tx.sent++;
    }
}



//! Passes inter-FGC packet to the LAN chip

static void interFgcSendPacket(struct Inter_fgc_packet * const packet)
{
    uint32_t const packet_tag  = packet->header.fgc.payload_type << 16;
    uint16_t const packet_size = sizeof(*packet);

    uint8_t err = lan92SendPacketOpt(packet, packet_size, packet_size, TX_CMDA_FIRST_SEG | TX_CMDA_LAST_SEG, packet_tag);

    if (err != 0)
    {
        inter_fgc_prop.status.tx.failed++;
    }
    else
    {
        inter_fgc_prop.status.tx.sent++;
    }

    histogramComAddValue(&communication_histograms.tx.fgc, timeGetAbsUs());
}



//! Fetches inter-FGC part of incoming packet from the LAN chip
//!
//! The function reads-out inter-FGC header and payload from the Rx queue of the
//! LAN chip. The FGC Ether part has been already read-out in the Ethernet ISR.

static void interFgcReadPacket(struct Inter_fgc_packet * const packet)
{
    static uint16_t const Inter_fgc_max_size = sizeof(packet->Inter_fgc_header) + sizeof(packet->payload);

    // Read-out inter-FGC header and inter-FGC payload in one go

    lan92ReadFifo(&packet->Inter_fgc_header,
                  Inter_fgc_max_size,
                  &ethernet.current_rx_fifo_byte_length);
}



void interFgcInit(struct Inter_fgc_mgr * Inter_fgc_mgr)
{
    inter_fgc.inter_fgc_mgr = Inter_fgc_mgr;

    inter_fgc.send_packet_function = interFgcSendPacket;

    // ISR_OFFSET_US is a config property with default value, but it has to
    // be initialised to a non-zero value before the interrupts are enabled!
    // If it's not done, then both ms interrupt and inter-FGC interrupt will
    // be scheduled at the same time and the FGC will crash!

    inter_fgc_prop.isr_offset_us = INTER_FGC_ISR_INIT_OFFSET_US;

    inter_fgc_prop.slave_simplified_state = FGC_PC_SIMPLIFIED_OFF;

    interFgcInitSyncedVars();
    interFgcInitPropBuffers();
    interFgcInitTxHeader();
    interFgcInitHistograms();
}



//! Decomposes dev_name string into signal name and MAC address
//!
//! The function takes a single string that comes from a dev_name type property,
//! parses it and extracts MAC address and signal name from it.
//!
//! As a reminder, dev_name string can have one of the formats:
//! * <device_name>
//! * <signal>@<device_name>
//! where <device_name> is a MAC address in XX-XX-XX-XX-XX-XX or a NameDB name,
//! and <signal_name> is a name that is present in the symlist linked to the property.
//!
//! Bear in mind that error checking performed by the function is minimal!
//!
//! @param[in]   str     String that meets requirements of dev_name property type
//! @param[out]  mac     MAC address encoded in the input string, converted to mac structure
//! @param[out]  signal  Signal encoded in the input string, converted to an enum value

static void interFgcDecomposeDevNameString(char const * str, struct Inter_fgc_mac * mac, enum Inter_fgc_signal_bitmask * signal)
{
    int i;

    // Scan the string in search for '@'

    for (i = 0; i < INTER_FGC_DEV_NAME_STR_LEN && str[i] != '\0'; i++)
    {
        if (str[i] == '@')
        {
            // The part of the string before '@' is a signal name
            // Look for the name in the symlist

            struct sym_name const * signals = sym_names_ifgc_signals;

            while(signals->label != NULL)
            {
                if (memcmp(str, signals->label, i) == 0)
                {
                    *signal = signals->type;
                }

                signals++;
            }

            // Point to the first character after '@'

            str = &str[i + 1];

            break;
        }
    }

    // Check if the device is in NameDb

    int8_t const namedb_index = nameDbGetIndex(str);

    if (namedb_index > 0)
    {
        // Device is in NameDB, so it must be an FGC
        // FGC MAC addresses are always in 02-00-00-00-00-<id> format,
        // where id is their fieldbus id (and also index in NameDB)

        mac->octets[0] = 0x02;
        mac->octets[1] = 0x00;
        mac->octets[2] = 0x00;
        mac->octets[3] = 0x00;
        mac->octets[4] = 0x00;
        mac->octets[5] = namedb_index;
    }
    else
    {
        // Device isn't in NameDB, so convert full MAC from string to integers
        // Here we assume that the MAC from the string is a valid MAC in form XX-XX-XX-XX-XX-XX

        uint16_t i;

        // Iterate over MAC octets

        for (i = 0; i < NUM_MAC_OCTETS; i++)
        {
            char * endptr;

            // Convert each octet from string to int

            mac->octets[i] = strtol(str, &endptr, 16);

            // Skip '-' characters

            str = endptr + 1;
        }
    }
}



//! Informs user about unwanted communication
//!
//! The function will put the MAC address in INTER_FGC.STATUS.UNEXPECTED.NAMES
//! property, if there's still space. It will also increment value of
//! INTER_FGC.STATUS.UNEXPECTED.COUNTER property on each call.
//!
//! @param[in]  mac  Source of unwanted communication

static void interFgcAddUnexpected(uint8_t const * const mac)
{
    // Increment value of INTER_FGC.STATUS.UNEXPECTED.COUNTER property

    inter_fgc_prop.status.unexpected.counter++;

    // Put the MAC in INTER_FGC.STATUS.UNEXPECTED.NAMES property

    uint16_t num_elements = PROP_INTER_FGC_STATUS_UNEXPECTED_NAMES.n_elements;

    if (num_elements < MAX_INTER_FGC_UNEXPECTED)
    {
        memcpy(inter_fgc_prop.status.unexpected.names[num_elements].octets, mac, NUM_MAC_OCTETS);

        PropSetNumEls(NULL, &PROP_INTER_FGC_STATUS_UNEXPECTED_NAMES, num_elements + 1);
    }
}



//! Executes test procedure chosen by the user

static void interFgcProcessTest(void)
{
    if (inter_fgc_prop.test.action != FGC_IFGC_TEST_ACTION_NONE)
    {
        if (   inter_fgc_prop.test.num_packets != 0
           && (stateOpGetState() == FGC_OP_TEST
           ||  stateOpGetState() == FGC_OP_SIMULATION))
        {
            interFgcSetWarning(IFGC_WARNINGS_TEST);

            // Use a special function for packet sending during the test

            inter_fgc.send_packet_function = interFgcSendPacketTest;
            inter_fgc.test_action          = inter_fgc_prop.test.action;

            inter_fgc_prop.test.num_packets--;
        }
        else
        {
            // Restore proper function for packet sending

            inter_fgc.send_packet_function = interFgcSendPacket;
            inter_fgc_prop.test.action     = FGC_IFGC_TEST_ACTION_NONE;

            // Restore proper inter-FGC version in the Tx packet header

            inter_fgc.packets.tx.Inter_fgc_header.version = INTER_FGC_VERSION;

            inter_fgc_prop.test.num_packets = 0;

            interFgcClearWarning(IFGC_WARNINGS_TEST);
        }
    }
}



//! Processes Rx statistics for this millisecond
//!
//! The function will:
//! - calculate number of missing packets within millisecond
//! - calculate number of excessive packets within millisecond
//! - calculate number of invalid packets within millisecond and second
//! - calculate number of consecutive invalid packets
//! It will also set a fault if the number of invalid packets per second or
//! the number of consecutive invalid packets is too high.
//! Bear in mind that missing and consecutive packets also count as invalid!

static void interFgcPrepareRxStats(void)
{
    uint16_t rx_index;

    for (rx_index = 0; rx_index < inter_fgc_prop.status.rx.n_elements; rx_index++)
    {
        // Check if there are missing or excessive packets this millisecond

        uint16_t const received_ms = inter_fgc.rx_stats.received_ms[rx_index];

        if (received_ms < INTER_FGC_NUM_EXPECTED_PACKETS_MS)
        {
            inter_fgc_prop.status.rx.missing[rx_index]   += (INTER_FGC_NUM_EXPECTED_PACKETS_MS - received_ms);
        }
        else
        {
            inter_fgc_prop.status.rx.excessive[rx_index] += (received_ms - INTER_FGC_NUM_EXPECTED_PACKETS_MS);
        }

        // Add missing and consecutive packets to invalid packet counter

        inter_fgc.rx_stats.invalid_ms[rx_index] += abs(received_ms - INTER_FGC_NUM_EXPECTED_PACKETS_MS);

        uint16_t const invalid_ms = inter_fgc.rx_stats.invalid_ms[rx_index];

        if (invalid_ms != 0)
        {
            inter_fgc.rx_stats.invalid_s[rx_index]           += invalid_ms;
            inter_fgc.rx_stats.invalid_consecutive[rx_index] += invalid_ms;
        }
        else
        {
            inter_fgc.rx_stats.invalid_consecutive[rx_index] = 0;
        }

        // Set a fault if there are too many consecutive invalid packets

        if (inter_fgc.rx_stats.invalid_consecutive[rx_index] > INTER_FGC_MAX_INVALID_CONSECUTIVE)
        {
            interFgcSetFault(IFGC_FAULT_NUM_INV_CON);
        }

        // Zero all millisecond counters

        inter_fgc.rx_stats.invalid_ms[rx_index]  = 0;
        inter_fgc.rx_stats.received_ms[rx_index] = 0;

        // On second boundary, check if there were too many invalid packets within the second

        if (ms_task.ms == 0)
        {
            if (inter_fgc.rx_stats.invalid_s[rx_index] > INTER_FGC_MAX_INVALID_PER_SECOND)
            {
                //interFgcSetFault(IFGC_FAULT_NUM_INV_SEC);
            }

            inter_fgc.rx_stats.invalid_s[rx_index] = 0;
        }
    }
}



//! Calculates Tx rate

static void interFgcCalcTxRate(void)
{
    static uint32_t sent_prev = 0;

    if (ms_task.ms == 0)
    {
        inter_fgc_prop.status.tx.rate = inter_fgc_prop.status.tx.sent - sent_prev;
        sent_prev                = inter_fgc_prop.status.tx.sent;
    }
}



//! Measures maximum time from the start of millisecond

static void interFgcCalcMaxMsStartDuration(void)
{
    if (inter_fgc.is_enabled)
    {
        uint32_t time_us = timeGetAbsUs();

        if (time_us > inter_fgc_prop.debug.start_ms_duration_us)
        {
            inter_fgc_prop.debug.start_ms_duration_us = time_us;
        }
    }
}



//! Sets a warning if unexpected packets were received

static void interFgcProcessUnexpected(void)
{
    if (inter_fgc_prop.status.unexpected.counter != 0 || PROP_INTER_FGC_STATUS_UNEXPECTED_NAMES.n_elements != 0)
    {
        interFgcSetWarning(IFGC_WARNINGS_UNEXP_COMM);
    }
    else
    {
        interFgcClearWarning(IFGC_WARNINGS_UNEXP_COMM);
    }
}



void interFgcStartMs(void)
{
    inter_fgc.start_ms_running = true;

    __sync_synchronize();

    if (inter_fgc.is_enabled == false)
    {
        // Reset inter-FGC faults, but not warnings

        inter_fgc_prop.status.faults = 0;

        statusClrFaults(FGC_FLT_SLAVE);
    }
    else
    {
        syncedVarProcess();
        interFgcPrepareRxStats();
        interFgcCalcTxRate();

        // Set a global fault if there are any inter-FGC faults

        if (   inter_fgc_prop.status.faults != 0
            && stateOpGetState() == FGC_OP_NORMAL)
        {
            statusSetFaults(FGC_FLT_SLAVE);
        }
    }

    interFgcProcessTest();
    interFgcProcessUnexpected();

    TICK_MCU_COMM_DELAY_US_P = inter_fgc_prop.isr_offset_us;

    __sync_synchronize();

    inter_fgc.start_ms_running = false;

    interFgcCalcMaxMsStartDuration();
}



static uint16_t interFgcPrepareMasterPayload(union Inter_fgc_block_payload * payload)
{
    payload->master.commands = inter_fgc_prop.cmd;

    if (inter_fgc_prop.topology == IFGC_TOPOLOGY_SINGLE ||
        inter_fgc_prop.topology == IFGC_TOPOLOGY_SERIES)
    {
        payload->master.actuation = ACTUATION_VOLTAGE_REF;
        payload->master.ref       = inter_fgc.inter_fgc_mgr->ext_ref;
        payload->master.reg_mode  = getRegMode();
    }
    else
    {
        payload->master.actuation = ACTUATION_NONE;
        payload->master.ref       = 0.0;
        payload->master.reg_mode  = REG_NONE;
    }

    return sizeof(payload->master);
}



static uint16_t interFgcPrepareSlavePayload(union Inter_fgc_block_payload * payload)
{
    payload->slave.pc_state = STATE_PC;

    if (vsIsBlockable() == true)
    {
        clrBitmap(payload->slave.flags, SLAVE_FLAG_NON_BLOCKABLE);
    }
    else
    {
        setBitmap(payload->slave.flags, SLAVE_FLAG_NON_BLOCKABLE);
    }

    return sizeof(payload->slave);
}



//! Converts signal bitmask to signal index
//!
//! @param[in]  signal_bitmask  Bitmask to be converted
//!
//! @retval  Signal index

static enum Inter_fgc_mgr_signal_index interFgcSignalBitmaskToIndex(enum Inter_fgc_signal_bitmask const signal_bitmask)
{
    switch(signal_bitmask)
    {
        case IFGC_SIGNAL_BITMASK_I_MEAS:    return IFGC_SIGNAL_INDEX_I_MEAS;
        case IFGC_SIGNAL_BITMASK_B_MEAS:    return IFGC_SIGNAL_INDEX_B_MEAS;
        case IFGC_SIGNAL_BITMASK_V_CAPA:    return IFGC_SIGNAL_INDEX_V_CAPA;
        case IFGC_SIGNAL_BITMASK_V_LOAD:    return IFGC_SIGNAL_INDEX_V_LOAD;
        case IFGC_SIGNAL_BITMASK_B_PROBE_A: return IFGC_SIGNAL_INDEX_B_PROBE_A;
        case IFGC_SIGNAL_BITMASK_B_PROBE_B: return IFGC_SIGNAL_INDEX_B_PROBE_B;
        case IFGC_SIGNAL_BITMASK_B_DOT:     return IFGC_SIGNAL_INDEX_B_DOT;
        case IFGC_SIGNAL_BITMASK_V_REF:     return IFGC_SIGNAL_INDEX_V_REF;
        default:                            return IFGC_SIGNAL_INDEX_NONE;
    }
}



//! Converts signal index to signal bitmask
//!
//! @param[in]  signal_index  Index to be converted
//!
//! @retval  Signal bitmask

static inline enum Inter_fgc_mgr_signal_index interFgcSignalIndexToBitmask(enum Inter_fgc_mgr_signal_index const signal_index)
{
    return ((signal_index == 0) ? 0 : (1 << (signal_index - 1)));
}



static uint16_t interFgcPrepareProducerPayload(union Inter_fgc_block_payload * payload)
{
    uint16_t const num_signals  = PROP_INTER_FGC_PRODUCED_SIGS.n_elements;
    uint16_t const payload_size = num_signals * sizeof(struct Synced_var_block_signal);

    int i;

    for (i = 0; i < num_signals; i++)
    {
        uint16_t const signal_bitmask = inter_fgc_prop.produced_sigs[i];
        uint8_t  const signal_index   = interFgcSignalBitmaskToIndex(signal_bitmask);

        struct Inter_fgc_mgr_sig const * sig = &inter_fgc.inter_fgc_mgr->produced_sigs[signal_index];

        payload->producer.signals[i].type     = signal_index;
        payload->producer.signals[i].is_valid = (uint8_t)sig->is_valid;
        payload->producer.signals[i].signal   = sig->signal;
    }

    return payload_size;
}


#if (FGC_CLASS_ID == 63)
static uint16_t interFgcPrepareDecoPayload(union Inter_fgc_block_payload * payload)
{
    uint32_t const index = dpcls.deco.index;

    payload->deco.index = index;

    if (dpcls.deco.state == CC_ENABLED)
    {
        payload->deco.v_ref  = dpcls.deco.v_ref[index];
        payload->deco.i_meas = dpcls.deco.i_meas[index];
    }
    else
    {
        payload->deco.v_ref  = 10.0 + (float)index;
        payload->deco.i_meas = -payload->deco.v_ref;

        dpcls.deco.v_ref[index]  = 10.0 + payload->deco.v_ref;
        dpcls.deco.i_meas[index] = -dpcls.deco.v_ref[index];
    }

    return sizeof(payload->deco);
}
#else
static uint16_t interFgcPrepareDecoPayload(union Inter_fgc_block_payload * payload)
{
    payload->deco.index  = 0;
    payload->deco.v_ref  = 0.0F;
    payload->deco.i_meas = 0.0F;

    return sizeof(payload->deco);
}
#endif



static uint16_t interFgcPrepareDummyPayload(union Inter_fgc_block_payload * payload)
{
    return 0;
}



//! Prepares inter-FGC data block of given type
//!
//! @param[out]  buffer  Buffer into which the block should be put
//! @param[in]   type    Type of inter-FGC block to put into the buffer
//!
//! @return  Size of the block in bytes

static uint16_t interFgcPrepareBlock(uint8_t * buffer, enum Inter_fgc_block_type type)
{
    static uint16_t (*block_payload_prepare[])(union Inter_fgc_block_payload * payload) =
    {
        [BLOCK_NONE    ] = interFgcPrepareDummyPayload,
        [BLOCK_MASTER  ] = interFgcPrepareMasterPayload,
        [BLOCK_SLAVE   ] = interFgcPrepareSlavePayload,
        [BLOCK_PRODUCER] = interFgcPrepareProducerPayload,
        [BLOCK_DECO    ] = interFgcPrepareDecoPayload,
    };

    static_assert(ArrayLen(block_payload_prepare) == BLOCK_COUNT, "Array size doesn't match corresponding enum used for indexing");

    if (type >= BLOCK_COUNT)
    {
        return 0;
    }

    struct Inter_fgc_block * block = (struct Inter_fgc_block *)buffer;

    // Prepare block payload first

    uint16_t const payload_size = block_payload_prepare[type](&block->payload);

    // We don't fill the header if no payload has been prepared

    if (payload_size == 0)
    {
        return 0;
    }

    // Fill-in the header

    block->header.block_type = type;
    block->header.block_size = sizeof(block->header) + payload_size;

    return block->header.block_size;
}



//! Maps inter-FGC role to block type
//!
//! @param[in]  role  Inter-FGC role
//!
//! @return  Block type corresponding to the role

static enum Inter_fgc_block_type interFgcRoleToBlockType(enum Inter_fgc_role const role)
{
    switch(role)
    {
        default:            return BLOCK_NONE;
        case ROLE_MASTER:   return BLOCK_MASTER;
        case ROLE_SLAVE:    return BLOCK_SLAVE;
        case ROLE_PRODUCER: return BLOCK_PRODUCER;
        case ROLE_DECO:     return BLOCK_DECO;
    }
}



//! Prepares termination of inter-FGC packet
//!
//! When cast to inter-FGC block header, the termination will act as an invalid/empty structure
//! with block type equal NONE and block size equal 0. This can be used to determine the end of
//! inter-FGC packet.
//!
//! @param[out]  buffer  Inter-FGC packet buffer

static void interFgcPrepareTermination(uint8_t * buffer)
{
    buffer[0] = 0;
    buffer[1] = 0;
    buffer[2] = 0;
    buffer[3] = 0;
}



//! Prepares payload of inter-FGC packet

static void interFgcPreparePayload()
{
    uint8_t * payload = inter_fgc.packets.tx.payload;

    // Zero the payload buffer

    memset(payload, 0, sizeof(inter_fgc.packets.tx.payload));

    enum Inter_fgc_role tx_role;

    // Find which of the available roles are on the Tx list
    // and put blocks corresponding to these roles in the packet

    for (tx_role = 1; tx_role < ROLE_COUNT; tx_role <<= 1)
    {
        if (testBitmap(inter_fgc_prop.status.tx.roles, tx_role))
        {
            payload += interFgcPrepareBlock(payload, interFgcRoleToBlockType(tx_role));
        }
    }

    interFgcPrepareTermination(payload);
}



static void interFgcSend()
{
    struct Inter_fgc_packet * const packet = &inter_fgc.packets.tx;

    if (inter_fgc_prop.broadcast == FGC_CTRL_ENABLED)
    {
        // Send only one packet to the broadcast address

        memset(packet->header.ethernet.dest_addr, 0xFF, NUM_MAC_OCTETS);

        inter_fgc.send_packet_function(packet);
    }
    else
    {
        int i;

        // Send one packet per address in the Tx list

        for (i = 0; i < PROP_INTER_FGC_STATUS_TX_NAMES.n_elements; i++)
        {
            memcpy(packet->header.ethernet.dest_addr, inter_fgc_prop.status.tx.names[i].octets, NUM_MAC_OCTETS);

            inter_fgc.send_packet_function(packet);
        }
    }
}



void interFgcSendPackets(void)
{
    if (PROP_INTER_FGC_STATUS_TX_NAMES.n_elements == 0)
    {
        return;
    }

    interFgcPreparePayload();
    interFgcSend();
}



//! Checks if received packet was expected
//!
//! @param[out]  rx_index  Index in Rx list to which corresponds the source MAC address from the packet
//!
//! @retval  true   Packet was expected and should be processed
//! @retval  false  Packet wasn't expected and should be discarded

static bool interFgcPacketIsExpected(uint16_t * rx_index)
{
    int i;

    // Scan the Rx list in search of source MAC address from the packet

    for (i = 0; i < inter_fgc_prop.status.rx.n_elements; i++)
    {
        if (memcmp(inter_fgc_prop.status.rx.names[i].octets, ethernet.rx_header.ethernet.src_addr, NUM_MAC_OCTETS) == 0)
        {
            // The source MAC address is on the list, so return its index

            *rx_index = i;

            return true;
        }
    }

    return false;
}



//! Reports invalid packet coming from one of the devices from Rx list
//!
//! @param[in]  invalid_type  Indicates what's wrong with the packet
//! @param[in]  rx_index      Rx list index of the device that sent the packet

static inline void interFgcReportInvalidPacket(enum Inter_fgc_inv_type_bitmask const invalid_type, uint16_t const rx_index)
{
    inter_fgc_prop.status.rx.invalid[rx_index]++;
    inter_fgc_prop.status.rx.invalid_type[rx_index] |= invalid_type;
    inter_fgc.rx_stats.invalid_ms[rx_index]++;
}



static void interFgcReadDummyPayload(union Inter_fgc_block_payload const * payload, uint8_t const payload_size, uint16_t const rx_index)
{
    // Do nothing
}



static void interFgcReadMasterPayload(union Inter_fgc_block_payload const * payload, uint8_t const payload_size, uint16_t const rx_index)
{
    inter_fgc_prop.cmd = payload->master.commands;

    // TODO Use libreg enum here instead of magic number

    //if (payload->master.actuation != 1)
    //{
    //    interFgcSetFault(IFGC_FAULT_ACTUATION);
    //}

    inter_fgc.inter_fgc_mgr->ext_ref = payload->master.ref;
}



static void interFgcReadSlavePayload(union Inter_fgc_block_payload const * payload, uint8_t const payload_size, uint16_t const rx_index)
{
    uint16_t pc_state = (uint16_t)payload->slave.pc_state;

    syncedVarStore(SYNCED_VAR_PC_STATE, rx_index, &pc_state);

    bool const slave_is_blockable = testBitmap(payload->slave.flags, SLAVE_FLAG_NON_BLOCKABLE) == false;

    if (   (slave_is_blockable == true  && vsIsBlockable() == false)
        || (slave_is_blockable == false && vsIsBlockable() == true))
    {
        interFgcSetFault(IFGC_FAULT_VS_BLOCKABLE);
    }
}



static void interFgcSaveSignalDummy(struct Synced_var_block_signal const * signal, uint16_t const rx_index)
{
    // Do nothing
}



static void interFgcSaveSignalIMeas(struct Synced_var_block_signal const * signal, uint16_t const  rx_index)
{
    static struct Inter_fgc_mgr_sig i_meas;

    i_meas.is_valid = signal->is_valid;
    i_meas.signal   = signal->signal;

    // If I_MEAS signal is enabled, inject the value into both DCCTs

    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_I_DCCT_A] = i_meas;
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_I_DCCT_B] = i_meas;
}



static void interFgcSaveSignalBMeas(struct Synced_var_block_signal const * signal, uint16_t const rx_index)
{
    static struct Inter_fgc_mgr_sig b_meas;

    b_meas.is_valid = signal->is_valid;
    b_meas.signal   = signal->signal;

    // If B_MEAS signal is enabled, inject the value into both probes

    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_B_PROBE_A] = b_meas;
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_B_PROBE_B] = b_meas;
}



static void interFgcSaveSignalVCapa(struct Synced_var_block_signal const * signal, uint16_t const rx_index)
{
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_V_CAPA].is_valid = signal->is_valid;
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_V_CAPA].signal   = signal->signal;
}



static void interFgcSaveSignalVLoad(struct Synced_var_block_signal const * signal, uint16_t const rx_index)
{
    syncedVarStore(SYNCED_VAR_V_LOAD, rx_index, signal);
}



static void interFgcSaveSignalBProbeA(struct Synced_var_block_signal const * signal, uint16_t const rx_index)
{
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_B_PROBE_A].is_valid = signal->is_valid;
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_B_PROBE_A].signal   = signal->signal;
}



static void interFgcSaveSignalBProbeB(struct Synced_var_block_signal const * signal, uint16_t const rx_index)
{
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_B_PROBE_B].is_valid = signal->is_valid;
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_B_PROBE_B].signal   = signal->signal;
}



static void interFgcSaveSignalBDot(struct Synced_var_block_signal const * signal, uint16_t const rx_index)
{
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_B_DOT].is_valid = signal->is_valid;
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_B_DOT].signal   = signal->signal;
}



static void interFgcSaveSignalVRef(struct Synced_var_block_signal const * signal, uint16_t const rx_index)
{
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_V_REF].is_valid = signal->is_valid;
    inter_fgc.inter_fgc_mgr->consumed_sigs[CONSUMED_SIGS_V_REF].signal   = signal->signal;
}



static void interFgcReadProducerPayload(union Inter_fgc_block_payload const * payload, uint8_t const payload_size, uint16_t const rx_index)
{
    static void (*save_signal_functions[])(struct Synced_var_block_signal const * signal, uint16_t rx_index) =
    {
        [IFGC_SIGNAL_INDEX_NONE     ] = interFgcSaveSignalDummy,
        [IFGC_SIGNAL_INDEX_I_MEAS   ] = interFgcSaveSignalIMeas,
        [IFGC_SIGNAL_INDEX_B_MEAS   ] = interFgcSaveSignalBMeas,
        [IFGC_SIGNAL_INDEX_V_CAPA   ] = interFgcSaveSignalVCapa,
        [IFGC_SIGNAL_INDEX_V_LOAD   ] = interFgcSaveSignalVLoad,
        [IFGC_SIGNAL_INDEX_B_PROBE_A] = interFgcSaveSignalBProbeA,
        [IFGC_SIGNAL_INDEX_B_PROBE_B] = interFgcSaveSignalBProbeB,
        [IFGC_SIGNAL_INDEX_B_DOT    ] = interFgcSaveSignalBDot,
        [IFGC_SIGNAL_INDEX_V_REF    ] = interFgcSaveSignalVRef,
    };

    static_assert(ArrayLen(save_signal_functions) == IFGC_SIGNAL_INDEX_COUNT, "Array size doesn't match corresponding enum used for indexing");

    uint16_t signals_bitmask = 0;
    uint16_t num_bytes       = 0;
    uint16_t i               = 0;

    // Iterate over signals in the block

    while(num_bytes < payload_size)
    {
        uint8_t const signal_index = payload->producer.signals[i].type;

        signals_bitmask |= interFgcSignalIndexToBitmask(signal_index);

        // Call the proper signal processing function

        save_signal_functions[signal_index](&payload->producer.signals[i], rx_index);

        // Advance to next signal

        num_bytes += sizeof(payload->producer.signals[0]);
        i++;
    }

    // Check if we have received all necessary signals in the block

    if (testAllBitmap(signals_bitmask, inter_fgc_prop.status.rx.signals[rx_index]) == false)
    {
        interFgcReportInvalidPacket(IFGC_INV_TYPE_MISSING_SIG, rx_index);
    }
}


#if (FGC_CLASS_ID == 63)
static void interFgcReadDecoPayload(union Inter_fgc_block_payload const * payload, uint8_t const payload_size, uint16_t const rx_index)
{
    uint32_t const index = payload->deco.index;

    if (index >= REG_NUM_DECO || index == dpcls.deco.index)
    {
        interFgcSetFault(IFGC_FAULT_DECO_INDEX);
    }
    else
    {
        dpcls.deco.v_ref [index] = payload->deco.v_ref;
        dpcls.deco.i_meas[index] = payload->deco.i_meas;
    }
}
#else
static void interFgcReadDecoPayload(union Inter_fgc_block_payload const * payload, uint8_t const payload_size, uint16_t const rx_index)
{
    // Do nothing
}
#endif



static void interFgcReadPayload(uint16_t const rx_index)
{
    static void (*block_payload_read[])(union Inter_fgc_block_payload const * payload, uint8_t const payload_size, uint16_t const index) =
    {
        [BLOCK_NONE    ] = interFgcReadDummyPayload,
        [BLOCK_MASTER  ] = interFgcReadMasterPayload,
        [BLOCK_SLAVE   ] = interFgcReadSlavePayload,
        [BLOCK_PRODUCER] = interFgcReadProducerPayload,
        [BLOCK_DECO    ] = interFgcReadDecoPayload,
    };

    static_assert(ArrayLen(block_payload_read) == BLOCK_COUNT, "Array size doesn't match corresponding enum used for indexing");

    uint16_t rx_actual_roles = 0;

    struct Inter_fgc_block        const * block        = (struct Inter_fgc_block const *)&inter_fgc.packets.rx.payload;
    struct Inter_fgc_block_header const * block_header = &block->header;
    enum Inter_fgc_block_type             block_type   = block_header->block_type;

    // Iterate over all blocks in the packet

    while(block_type != BLOCK_NONE)
    {
        if (block_type >= BLOCK_COUNT)
        {
            interFgcReportInvalidPacket(IFGC_INV_TYPE_UNKNOWN_BLOCK, rx_index);

            return;
        }

        uint16_t const block_size         = block_header->block_size;
        uint16_t const block_payload_size = block_size - sizeof(struct Inter_fgc_block_header);

        enum Inter_fgc_role const role = (1 << (block_type - 1));
        rx_actual_roles |= role;

        // Process the block payload only if the role associated with it is on the Rx list
        // In other words, process it only if we are expecting it

        if (testBitmap(inter_fgc_prop.status.rx.roles[rx_index], role))
        {
            block_payload_read[block_type](&block->payload, block_payload_size, rx_index);
        }

        // Advance to the next block

        block        = (struct Inter_fgc_block const *)((uint8_t const *)block + block_size);
        block_header = &block->header;
        block_type   = block_header->block_type;
    }

    // Check if we have received all blocks that we were expecting

    if (testAllBitmap(rx_actual_roles, inter_fgc_prop.status.rx.roles[rx_index]) == false)
    {
        interFgcReportInvalidPacket(IFGC_INV_TYPE_MISSING_BLOCK, rx_index);
    }

    // Update packet received time to be the microsecond within the second

    inter_fgc.inter_fgc_mgr->recv_time_us = timeGetUtcTimeUs();
}



//! Clears Tx status properties
//!
//! The Tx status properties contain the Tx list and Tx statistics

static void interFgcClearTxStatus(void)
{
    memset(&inter_fgc_prop.status.tx, 0, sizeof(inter_fgc_prop.status.tx));

    PropSetNumEls(NULL, &PROP_INTER_FGC_STATUS_TX_NAMES, 0);
}




//! Clears Rx status properties
//!
//! The Rx status properties contain the Rx list and Rx statistics

static void interFgcClearRxStatus(void)
{
    memset(&inter_fgc_prop.status.rx, 0, sizeof(inter_fgc_prop.status.rx));
}



//! Clears class-dependent variables

static void interFgcClearClassVariables(void)
{
    enum Inter_fgc_mgr_consumed_sigs sig;

    struct Inter_fgc_mgr_sig const zero_meas = { .is_valid = false, .signal = 0.0 };

    // Zero values of all produced and consumed signals

    for (sig = CONSUMED_SIGS_NONE; sig < CONSUMED_SIGS_COUNT; sig++)
    {
        inter_fgc.inter_fgc_mgr->is_enabled[sig]    = false;
        inter_fgc.inter_fgc_mgr->consumed_sigs[sig] = zero_meas;
        inter_fgc.inter_fgc_mgr->produced_sigs[sig] = zero_meas;
    }

    inter_fgc.inter_fgc_mgr->is_master      = false;
    inter_fgc.inter_fgc_mgr->is_slave       = false;
    inter_fgc.inter_fgc_mgr->is_ref_slave   = false;
    inter_fgc.inter_fgc_mgr->is_producer    = false;
    inter_fgc.inter_fgc_mgr->ext_ref        = 0.0;
    inter_fgc.inter_fgc_mgr->num_converters = 0;
}



//! Adds or updates data on the Tx list
//!
//! @param[in]  mac      MAC address to be added to the Tx list
//! @param[in]  tx_role  Tx role to be added to the Tx list

static void interFgcUpdateTxList(struct Inter_fgc_mac const * mac, enum Inter_fgc_role tx_role)
{
    // Make sure that all casts in the function are to the correct type

    static_assert(sizeof(inter_fgc_prop.status.tx.roles) == sizeof(uint16_t), "Cast to invalid type");

    uintptr_t const num_tx_elements = PROP_INTER_FGC_STATUS_TX_NAMES.n_elements;
    uintptr_t const max_tx_elements = PROP_INTER_FGC_STATUS_TX_NAMES.max_elements;

    int i;

    // If the MAC address is already on the target list, then just add the role to Tx roles and return

    for (i = 0; i < num_tx_elements; i++)
    {
        if (memcmp(inter_fgc_prop.status.tx.names[i].octets, mac->octets, NUM_MAC_OCTETS) == 0)
        {
            inter_fgc_prop.status.tx.roles |= (uint16_t)tx_role;

            return;
        }
    }

    // The MAC isn't on the target list, so we have to append it

    if (num_tx_elements < max_tx_elements)
    {
        memcpy(inter_fgc_prop.status.tx.names[num_tx_elements].octets, mac->octets, NUM_MAC_OCTETS);
        inter_fgc_prop.status.tx.roles |= (uint16_t)tx_role;
        PropSetNumEls(NULL, &PROP_INTER_FGC_STATUS_TX_NAMES, num_tx_elements + 1);
    }
    else
    {
        // Set a fault if there's no more space on the list

        interFgcSetFault(IFGC_FAULT_NUM_TX_DEVS);
    }
}



//! Enables signal reception
//!
//! Calling this function will inform the DSP that one of the signals is going
//! to be received though inter-FGC communication.
//!
//! @param[in]  signal  Signal that will be received through inter-FGC

static void interFgcEnableRxSignal(enum Inter_fgc_signal_bitmask signal)
{
    enum Inter_fgc_mgr_signal_index const signal_index = interFgcSignalBitmaskToIndex(signal);

    // I_MEAS translates to I_DCCT_A and I_DCCT_B
    // B_MEAS translates to B_PROBE_A and B_PROBE_B
    // Other signals translate 1 to 1

    if (signal_index == IFGC_SIGNAL_INDEX_I_MEAS)
    {
        // I_MEAS is going to be injected into both I_DCCT_A and I_DCCT_B

        inter_fgc.inter_fgc_mgr->is_enabled[CONSUMED_SIGS_I_DCCT_A] = true;
        inter_fgc.inter_fgc_mgr->is_enabled[CONSUMED_SIGS_I_DCCT_B] = true;
    }
    else if (signal_index == IFGC_SIGNAL_INDEX_B_MEAS)
    {
        // B_MEAS is going to be injected into both B_PROBE_A and B_PROBE_B

        inter_fgc.inter_fgc_mgr->is_enabled[CONSUMED_SIGS_B_PROBE_A] = true;
        inter_fgc.inter_fgc_mgr->is_enabled[CONSUMED_SIGS_B_PROBE_B] = true;
    }
    else
    {
        inter_fgc.inter_fgc_mgr->is_enabled[signal_index] = true;
    }
}



//! Adds or updates data on the Rx list
//!
//! @param[in]  mac      MAC address to be added to the Rx list
//! @param[in]  rx_role  Tx role to be added to the Rx list
//! @param[in]  signal   Signal to be added to the Rx list

static void interFgcUpdateRxList(struct Inter_fgc_mac const * mac, enum Inter_fgc_role rx_role, enum Inter_fgc_signal_bitmask signal)
{
    // Make sure that all casts in the function are to the correct types

    static_assert(sizeof(inter_fgc_prop.status.rx.roles[0])   == sizeof(uint16_t), "Cast to invalid type");
    static_assert(sizeof(inter_fgc_prop.status.rx.signals[0]) == sizeof(uint16_t), "Cast to invalid type");

    int i;

    // If the MAC is already on the list, just update roles and signals that are associated with it

    for (i = 0; i < inter_fgc_prop.status.rx.n_elements; i++)
    {
        if (memcmp(inter_fgc_prop.status.rx.names[i].octets, mac->octets, NUM_MAC_OCTETS) == 0)
        {
            inter_fgc_prop.status.rx.roles[i]   |= (uint16_t)rx_role;
            inter_fgc_prop.status.rx.signals[i] |= (uint16_t)signal;

            interFgcEnableRxSignal(signal);

            return;
        }
    }

    // The MAC isn't on the list, so we have to append it

    if (inter_fgc_prop.status.rx.n_elements < PROP_INTER_FGC_STATUS_RX_NAMES.max_elements)
    {
        memcpy(inter_fgc_prop.status.rx.names[inter_fgc_prop.status.rx.n_elements].octets, mac, NUM_MAC_OCTETS);
        inter_fgc_prop.status.rx.roles[i]   = (uint16_t)rx_role;
        inter_fgc_prop.status.rx.signals[i] = (uint16_t)signal;
        inter_fgc_prop.status.rx.n_elements++;

        interFgcEnableRxSignal(signal);
    }
    else
    {
        // Set a fault if there's no more space on the list

        interFgcSetFault(IFGC_FAULT_NUM_RX_DEVS);
    }
}



//! Checks if there are no clashing signals on the Rx list
//!
//! Restrictions on received signals:
//! - no signal type should be received from multiple sources, unless it's
//!   a synced variable (currently only V_LOAD is)
//! - measurements that would be injected into the same transducer are forbidden
//!   (at the moment B_MEAS and B_PROBE_A/B_PROBE_B)
//!
//! @retval  Bitmask containing all Rx signals

static uint16_t interFgcValidateRxSignals(void)
{
    static_assert(IFGC_SIGNAL_INDEX_COUNT <= 16, "Signal bitmasks of the Rx list can hold only 16 signals");

    // Bitmask containing signals for which reception from multiple sources is allowed

    static uint16_t const multiple_rx_signals = IFGC_SIGNAL_BITMASK_V_LOAD;

    uint16_t signal_bitmask = 0;
    uint16_t i;

    // Iterate over all devices on the Rx list

    for (i = 0; i < inter_fgc_prop.status.rx.n_elements; i++)
    {
        // Iterate over all signals from that device

        uint16_t bit;

        for (bit = 0; bit < 16; bit++)
        {
            uint16_t signal = (inter_fgc_prop.status.rx.signals[i] & (1 << bit));

            // Check if the signal is already on the Rx list and, if it is,
            // check if its reception from multiple sources is allowed

            if ((signal & signal_bitmask)      != 0 &&
               (signal & multiple_rx_signals) == 0)
            {
                interFgcSetFault(IFGC_FAULT_CLASHING_SIGS);
            }
        }

        // Accumulate all signals from all devices on the Rx list

        signal_bitmask |= inter_fgc_prop.status.rx.signals[i];
    }

    if (   testBitmap(signal_bitmask, IFGC_SIGNAL_BITMASK_B_MEAS)
       && (   testBitmap(signal_bitmask, IFGC_SIGNAL_BITMASK_B_PROBE_A)
           || testBitmap(signal_bitmask, IFGC_SIGNAL_BITMASK_B_PROBE_B)))
    {
        interFgcSetFault(IFGC_FAULT_CLASHING_SIGS);
    }

    return signal_bitmask;
}



//! Checks if there are not repeated Tx signals
//!
//! @retval  Bitmask containing all Tx signals

static uint16_t interFgcValidateTxSignals(void)
{
    uint16_t signal_bitmask = 0;
    uint16_t i;

    // Iterate over all signals on the Tx list

    for (i = 0; i < PROP_INTER_FGC_PRODUCED_SIGS.n_elements; i++)
    {
        if ((signal_bitmask & inter_fgc_prop.produced_sigs[i]) != 0)
        {
            interFgcSetFault(IFGC_FAULT_CLASHING_SIGS);
        }

        signal_bitmask |= inter_fgc_prop.produced_sigs[i];
    }

    return signal_bitmask;
}



static void interFgcValidateSignals(void)
{
    uint16_t const rx_signals_bitmask = interFgcValidateRxSignals();
    uint16_t const tx_signals_bitmask = interFgcValidateTxSignals();

    // Set a fault if there are any signals on both Tx and Rx lists

    if ((rx_signals_bitmask & tx_signals_bitmask) != 0)
    {
        interFgcSetFault(IFGC_FAULT_CLASHING_SIGS);
    }
}



//! Checks if device configuration meets all criteria of the defined topology

static void interFgcValidateTopology(void)
{
    if (interFgcIsMaster())
    {
        // NONE isn't a valid topology

        if (inter_fgc_prop.topology == IFGC_TOPOLOGY_NONE)
        {
            interFgcSetFault(IFGC_FAULT_TOPOLOGY);
        }

        // SINGLE topology requirements:
        // - master without a converter
        // - single slave

        if (inter_fgc_prop.topology == IFGC_TOPOLOGY_SINGLE && (vs.present == FGC_CTRL_ENABLED || inter_fgc.inter_fgc_mgr->num_converters != 1))
        {
            interFgcSetFault(IFGC_FAULT_TOPOLOGY);
        }

        // SERIES topology requirements:
        // - each slave should provide voltage measurement in form of inter-FGC signal

        if (inter_fgc_prop.topology == IFGC_TOPOLOGY_SERIES)
        {
            int i;

            for (i = 0; i < inter_fgc_prop.status.rx.n_elements; i++)
            {
                if (testBitmap(inter_fgc_prop.status.rx.roles[i], ROLE_SLAVE))
                {
                    if (!testBitmap(inter_fgc_prop.status.rx.signals[i], IFGC_SIGNAL_BITMASK_V_LOAD))
                    {
                        interFgcSetFault(IFGC_FAULT_TOPOLOGY);
                    }
                }
            }
        }

        if (inter_fgc_prop.topology == IFGC_TOPOLOGY_STATE)
        {
            // No restrictions
        }
    }
    else if (interFgcIsSlave())
    {
        if (inter_fgc_prop.topology != IFGC_TOPOLOGY_NONE && inter_fgc_prop.topology != IFGC_TOPOLOGY_STATE)
        {
            //interFgcSetFault(IFGC_FAULT_TOPOLOGY);
        }
    }
    else
    {
        if (inter_fgc_prop.topology != IFGC_TOPOLOGY_NONE)
        {
            interFgcSetFault(IFGC_FAULT_TOPOLOGY);
        }
    }
}



//! Check if given element of property of dev_name type is empty
//!
//! @param[in]  prop   Pointer to dev_name property structure
//! @param[in]  index  Index of property element
//!
//! @retval  true   Element is empty
//! @retval  false  Element isn't empty

static inline bool interFgcIsPropElementEmpty(struct prop const * prop, uint16_t index)
{
    return (memcmp(((char **)prop->value)[index], "NONE", 4) == 0) || ((char **)prop->value)[index][0] == '\0';
}



//! Populates Rx list properties

static void interFgcPopulateRxList(void)
{
    // This structure maps role properties with their Rx roles

    static struct Inter_fgc_prop_role const props_rx_roles[] =
    {
        { &PROP_INTER_FGC_MASTER,        ROLE_MASTER,  },
        { &PROP_INTER_FGC_SLAVES,        ROLE_SLAVE,   },
        { &PROP_INTER_FGC_SIG_SOURCES,   ROLE_PRODUCER },
#if (FGC_CLASS_ID == 63)
        { &PROP_INTER_FGC_DECO_PARTNERS, ROLE_DECO     },
#endif
    };

    int i;
    bool is_consumer = false;

    // Iterate over all role properties that contribute to the Rx list

    for (i = 0; i < ArrayLen(props_rx_roles); i++)
    {
        struct prop const * prop = props_rx_roles[i].prop;
        int j;

        // Iterate over all elements in the property

        for (j = 0; j < prop->n_elements; j++)
        {
            // Skip empty elements

            if (interFgcIsPropElementEmpty(prop, j))
            {
                continue;
            }

            struct Inter_fgc_mac mac;
            enum Inter_fgc_signal_bitmask signal = IFGC_SIGNAL_BITMASK_NONE;

            // Extract mac address and, possibly, signal name from the property
            // and put it on the Rx list

            interFgcDecomposeDevNameString(((char **)prop->value)[j], &mac, &signal);
            interFgcUpdateRxList(&mac, props_rx_roles[i].role, signal);

            if (props_rx_roles[i].role == ROLE_PRODUCER)
            {
                is_consumer = true;
            }
        }
    }

    inter_fgc.inter_fgc_mgr->is_consumer = is_consumer;
}



//! Populates Tx list properties

static void interFgcPopulateTxList(void)
{
    // This structure maps role properties with their Tx roles

    static struct Inter_fgc_prop_role const props_tx_roles[] =
    {
        { &PROP_INTER_FGC_SLAVES,        ROLE_MASTER   },
        { &PROP_INTER_FGC_MASTER,        ROLE_SLAVE    },
        { &PROP_INTER_FGC_CONSUMERS,     ROLE_PRODUCER },
#if (FGC_CLASS_ID == 63)
        { &PROP_INTER_FGC_DECO_PARTNERS, ROLE_DECO     },
#endif
    };

    int i;

    // Iterate over all role properties that contribute to the Tx list

    for (i = 0; i < ArrayLen(props_tx_roles); i++)
    {
        struct prop const * prop = props_tx_roles[i].prop;
        int j;

        // Iterate over all elements in the property

        for (j = 0; j < prop->n_elements; j++)
        {
            // Skip empty elements

            if (interFgcIsPropElementEmpty(prop, j))
            {
                continue;
            }

            struct Inter_fgc_mac mac;

            // Extract mac address from the property and put it on the Tx list

            interFgcDecomposeDevNameString(((char **)prop->value)[j], &mac, NULL);
            interFgcUpdateTxList(&mac, props_tx_roles[i].role);
        }
    }
}



//! Calculates total number of converters under control of the FGC
//!
//! In case of master we check if the FGC is controlling a converter directly,
//! as well as how many slaves it has.

static void interFgcCountConverters(void)
{
    static uint32_t const IFGC_TOPOLOGY_NO_CONVERTERS = (IFGC_TOPOLOGY_NONE | IFGC_TOPOLOGY_STATE);

    inter_fgc.inter_fgc_mgr->num_converters = ((vs.present == FGC_CTRL_ENABLED) ? 1 : 0);

    if (interFgcIsMaster() == true && inter_fgc_prop.topology != IFGC_TOPOLOGY_NO_CONVERTERS)
    {
        int i;

        // We expect all slaves to control a converter

        for (i = 0; i < inter_fgc_prop.status.rx.n_elements; i++)
        {
            if (testBitmap(inter_fgc_prop.status.rx.roles[i], ROLE_SLAVE))
            {
                inter_fgc.inter_fgc_mgr->num_converters++;
            }
        }
    }
    else if (interFgcIsSlave() == true && inter_fgc.inter_fgc_mgr->num_converters == 0)
    {
        // Slave must have a converter

        interFgcSetFault(IFGC_FAULT_VS_PRESENT);
    }
}



//! Prepares control word that will drive slaves' PC state machine
//!
//! @param[in]  reset  Indicates that reset command is active

static void interFgcPrepareControlWord(bool const reset)
{
    static uint8_t  prev_cmd    = 0;
    static bool     fault_latch = false;

    uint16_t slave_simplified_state  = inter_fgc_prop.slave_simplified_state;
    uint16_t master_simplified_state = statePcGetSimplified();
    uint16_t master_simplified_mode  = modePcGetSimplified();
    uint8_t  cmd                     = prev_cmd;

    // The latched condition to force the command OFF is cleared when the user
    // explicitely requests to transition out of OFF.

    if (modePcGetInternal() != FGC_PC_OFF)
    {
        fault_latch = false;
    }

    // Configure the commands based on the master and slaves mode and state

    if (slave_simplified_state == FGC_PC_SIMPLIFIED_FAULT)
    {
        // This fault will set master_simplified_state to FAULT and force
        // the OFF command to all the slaves

        setBitmap(inter_fgc_prop.status.faults, FGC_IFGC_FAULTS_SLAVE_FAULT);
    }

    if (master_simplified_state == FGC_PC_SIMPLIFIED_FAULT || fault_latch == true)
    {
        cmd = local_Inter_fgc_cmd_simplified[FGC_PC_SIMPLIFIED_OFF];

        // The command OFF must be forced event after the fault in the master has been cleared.
        // Otherwise the slaves will automatically power on.

        if (master_simplified_mode != FGC_PC_SIMPLIFIED_OFF)
        {
            fault_latch = true;
        }
    }
    else
    {
        switch (master_simplified_mode)
        {
            case FGC_PC_SIMPLIFIED_OFF:
            {
                if (inter_fgc_prop.topology == FGC_IFGC_TOPOLOGY_SINGLE)
                {
                    cmd = local_Inter_fgc_cmd_simplified[master_simplified_state];
                }
                else
                {
                    cmd = local_Inter_fgc_cmd_simplified[FGC_PC_SIMPLIFIED_OFF];
                }

                // Force BLOCKING until the slaves have reached this state

                if (slave_simplified_state == FGC_PC_SIMPLIFIED_ON)
                {
                    modePcSetModeInternalFromSimplified(FGC_PC_SIMPLIFIED_BLOCKING);
                }
                else if (slave_simplified_state == FGC_PC_SIMPLIFIED_BLOCKING)
                {
                    modePcSetModeInternalFromSimplified(FGC_PC_SIMPLIFIED_OFF);
                }

                break;
            }

            case FGC_PC_SIMPLIFIED_BLOCKING:
            {
                if (   master_simplified_state == FGC_PC_SIMPLIFIED_OFF
                    || master_simplified_state == FGC_PC_SIMPLIFIED_BLOCKING)
                {
                    cmd = local_Inter_fgc_cmd_simplified[FGC_PC_SIMPLIFIED_BLOCKING];
                }

                break;
            }

            case FGC_PC_SIMPLIFIED_ON:
            {
                if (slave_simplified_state == FGC_PC_SIMPLIFIED_OFF)
                {
                    // Transition the slaves to BLOCKING

                    cmd = local_Inter_fgc_cmd_simplified[FGC_PC_SIMPLIFIED_BLOCKING];

                    modePcSetModeInternalFromSimplified(FGC_PC_SIMPLIFIED_BLOCKING);
                }
                else if (   master_simplified_state == FGC_PC_SIMPLIFIED_BLOCKING
                         && slave_simplified_state  == FGC_PC_SIMPLIFIED_BLOCKING)
                {
                    // Transition the slaves to ON

                    cmd = local_Inter_fgc_cmd_simplified[FGC_PC_SIMPLIFIED_ON];
                }

                if (   (   inter_fgc_prop.topology == FGC_IFGC_TOPOLOGY_STATE_ON
                        && slave_simplified_state  == FGC_PC_SIMPLIFIED_ON)
                    || (   inter_fgc_prop.topology != FGC_IFGC_TOPOLOGY_STATE_ON
                        && slave_simplified_state  == FGC_PC_SIMPLIFIED_BLOCKING))
                {
                    modePcSetModeInternalFromSimplified(FGC_PC_SIMPLIFIED_ON);
                }

                break;
            }
        }
    }

    // Add the reset command if enabled

    if (reset == true)
    {
        statusClrFaults(FGC_FLT_SLAVE);

        // Do not send reset command in states other than FLT_OFF
        // since the GE RXi controller in POPS-B doesn't like it

        updateBitmap(cmd, IFGC_CMD_RESET, pcStateGet() == FGC_PC_FLT_OFF);
    }

    // Update the commands to be sent to the slaves

    inter_fgc_prop.cmd = cmd;

    prev_cmd = cmd;
}



//! Consumes control word from master and sets PC mode based on it

static void interFgcProcessControlWord(void)
{
    static uint8_t prev_pc_simplified = 0xFF;

    uint8_t pc_simplified = 0;
    uint8_t cmd           = inter_fgc_prop.cmd;

    if (testBitmap(cmd, IFGC_CMD_RESET) == true)
    {
        statePcResetFaults();

        clrBitmap(cmd, IFGC_CMD_RESET);
    }

    for (pc_simplified = 0; pc_simplified < 3; ++pc_simplified)
    {
        if (cmd == local_Inter_fgc_cmd_simplified[pc_simplified])
        {
            break;
        }
    }

    if (prev_pc_simplified != pc_simplified && pc_simplified < 4)
    {
        modePcSetModeInternalFromSimplified(pc_simplified);

        prev_pc_simplified = pc_simplified;
    }
}



//! Monitors PC state of the slaves. Forces the master to ON if all the slaves
//! are synchronized or asserts a FAULT if any of the slaves has tripped
//!
//! @param[in]  reset  Indicates that reset command is active

void interFgcPopulateStatus(void)
{
    inter_fgc.is_enabled = false;

    interFgcClearRxStatus();
    interFgcClearTxStatus();

    syncedVarDisable();

    interFgcClearClassVariables();

    interFgcResetConfigFaults();

    interFgcPopulateRxList();
    interFgcPopulateTxList();

    inter_fgc.inter_fgc_mgr->is_master    = ((inter_fgc_prop.status.tx.roles & ROLE_MASTER  ) != 0);
    inter_fgc.inter_fgc_mgr->is_producer  = ((inter_fgc_prop.status.tx.roles & ROLE_PRODUCER) != 0);
    inter_fgc.inter_fgc_mgr->is_slave     = ((inter_fgc_prop.status.tx.roles & ROLE_SLAVE   ) != 0);
    inter_fgc.inter_fgc_mgr->is_ref_slave = (inter_fgc.inter_fgc_mgr->is_slave && inter_fgc_prop.topology != IFGC_TOPOLOGY_STATE);
    inter_fgc.inter_fgc_mgr->topology     = inter_fgc_prop.topology;

    interFgcValidateSignals();
    interFgcCountConverters();
    interFgcValidateTopology();
    interFgcEnableSyncedVars();

    inter_fgc.is_enabled = (PROP_INTER_FGC_STATUS_TX_NAMES.n_elements != 0 ||
                       PROP_INTER_FGC_STATUS_RX_NAMES.n_elements != 0);
}



void interFgcProcess(bool reset)
{
    if (reset == true)
    {
        interFgcResetRuntimeFaults();
    }

    if (interFgcIsMaster() == true)
    {
        interFgcPrepareControlWord(reset);
    }
    else if (interFgcIsSlave() == true)
    {
        interFgcProcessControlWord();
    }
}



void interFgcReceivePacket(void)
{
    // Save packet reception time in the histogram

    histogramComAddValue(&communication_histograms.rx.fgc, ethernet.rx_time_us);

    // Do not process the packet if the bookkeeping function is running

    if (inter_fgc.start_ms_running)
    {
        inter_fgc_prop.debug.num_collisions++;

        return;
    }

    uint16_t rx_index;    // Device index in the Rx list

    if (!interFgcPacketIsExpected(&rx_index))
    {
        if (!ethernet.rx_is_broadcast)
        {
            interFgcAddUnexpected(ethernet.rx_header.ethernet.src_addr);
        }
    }
    else
    {
        inter_fgc.rx_stats.received_ms[rx_index]++;

        // Read-out content of the packet from the LAN chip

        interFgcReadPacket(&inter_fgc.packets.rx);

        // Check if the inter-FGC protocol version matches

        if (inter_fgc.packets.rx.Inter_fgc_header.version != INTER_FGC_VERSION)
        {
            interFgcReportInvalidPacket(IFGC_INV_TYPE_VERSION, rx_index);

            return;
        }

        interFgcReadPayload(rx_index);

        // Save packet processing time in the histogram

        histogramComAddValue(&communication_histograms.rx.fgc_proc_time, timeGetAbsUs() - ethernet.rx_time_us);
    }
}



static bool interFgcIsFgcMac(struct Inter_fgc_mac const * mac)
{
    static uint8_t const fgc_mac_prefix[5] = { 0x02, 0x00, 0x00, 0x00, 0x00 };

    return (memcmp(mac->octets, fgc_mac_prefix, 5) == 0 && mac->octets[5] < 65);
}



static inline void interFgcPrintMacAsString(FILE * f, struct Inter_fgc_mac const * mac)
{
    fprintf(f, "%02X-%02X-%02X-%02X-%02X-%02X",
            mac->octets[0],
            mac->octets[1],
            mac->octets[2],
            mac->octets[3],
            mac->octets[4],
            mac->octets[5]);
}



enum fgc_errno interFgcGetMac(struct cmd * c, struct prop * p)
{
    prop_size_t n;

    enum fgc_errno errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum != 0)
    {
        return errnum;
    }

    bool const zero_f = (testBitmap(p->flags, PF_GET_ZERO) && testBitmap(c->getopts, GET_OPT_ZERO));

    struct Inter_fgc_mac * mac = p->value;
    uint16_t               i   = c->from;

    while(n--)
    {
        // Print index if required

        errnum = CmdPrintIdx(c, i);

        if (errnum != 0)
        {
            return errnum;
        }

        // Check if MAC address belongs to an FGC

        if (interFgcIsFgcMac(mac))
        {
            uint16_t     name_db_index = mac->octets[5];
            char const * name          = nameDbGetName(name_db_index);

            // If there's a name in NameDb that corresponds to the FGC dongle number,
            // then print it instead of MAC address. Otherwise, print the MAC directly.

            if (name != NULL)
            {
                fputs(name, c->f);
            }
            else
            {
                interFgcPrintMacAsString(c->f, mac);
            }
        }
        else
        {
            // Non-FGC devices aren't in NameDb, so print the MAC directly

            interFgcPrintMacAsString(c->f, mac);
        }

        // Clear the value if zero flag present

        if (zero_f)
        {
            memset(mac, 0, NUM_MAC_OCTETS);
        }

        // Advance to the next element

        mac += c->step;
        i++;
    }

    if (zero_f)
    {
        p->n_elements = 0;
    }

    return 0;
}



enum fgc_errno interFgcSetDevName(struct cmd * c, struct prop * p)
{
    // If the previous command is still active, return immediately

    if (dpcmdTest(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_UPDATE_REG_PARS) == true)
    {
        return FGC_DSP_PROPERTY_ISSUE;
    }

    enum fgc_errno errnum = ParsSet(c, p);

    if (errnum != 0)
    {
        return errnum;
    }

    interFgcPopulateStatus();

    // Tell the DSP to update libreg NUM_VSOURCES parameter

    uint16_t timeout = 100;

    dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_UPDATE_REG_PARS);

    while (   dpcmdTest(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_UPDATE_REG_PARS) == true
           && --timeout == 0)
    {
        sleepUs(1000);
    }

    if (timeout == 0)
    {
        return FGC_DSP_PROPERTY_ISSUE;
    }

    return 0;
}



bool interFgcIsMaster(void)
{
    return inter_fgc.inter_fgc_mgr->is_master;
}



bool interFgcIsSlave(void)
{
    return inter_fgc.inter_fgc_mgr->is_slave;
}


uint16_t interFgcGetSlaveState(void)
{
    return inter_fgc_prop.slave_simplified_state;
}


// EOF
