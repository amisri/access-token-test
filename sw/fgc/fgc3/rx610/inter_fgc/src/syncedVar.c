//! @file
//! @brief


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>

#include <syncedVar.h>
#include <bitmap.h>



// ---------- Constants

#define SYNCED_VAR_MAX_NUM_ELEMENTS (sizeof(((struct Synced_var *)0)->rx_bitmap) * 8)



// // ---------- Internal structures, unions and enumerations

// enum Synced_var_type
// {
//     SYNCED_VAR_V_LOAD,
//     SYNCED_VAR_PC_STATE,
//     SYNCED_VAR_COUNT,
// };




// ---------- Internal variable definitions

static struct Synced_var synced_vars[SYNCED_VAR_COUNT];









// ---------- Internal function definitions

//! Check if the variable was received from all expected sources
//!
//! @param[in]  synced_var  Variable to examine
//!
//! @retval  true   Data has been received from all sources and the variable can be processed
//! @retval  false  The information is incomplete and the variable shouldn't be processed

static inline bool syncedVarIsReady(struct Synced_var const * synced_var)
{
    return (synced_var->rx_bitmask != 0 && testAllBitmap(synced_var->rx_bitmap, synced_var->rx_bitmask));
}



// ---------- External function definitions

void syncedVarInit(enum Synced_var_signal const   signal,
                   enum Synced_var_type   const   type,
                   void                         * rx_array_buffer,
                   void                        (* processing_func)(struct Synced_var const * const))
{
    struct Synced_var * synced_var = &synced_vars[signal];
    
    synced_var->type            = type;
    synced_var->rx_array        = rx_array_buffer;
    synced_var->processing_func = processing_func;
}



void syncedVarStore(enum Synced_var_signal const   signal,
                    uint16_t               const   rx_index,
                    void                   const * value)
{
    // Protect against shifting too far left

    if (rx_index >= SYNCED_VAR_MAX_NUM_ELEMENTS)
    {
        return;
    }

    struct Synced_var * synced_var = &synced_vars[signal];
    uint16_t const rx_index_bitmask = (1 << rx_index);

    // Do not save the data if it's not expected from this device

    if ((synced_var->rx_bitmask & rx_index_bitmask) == 0)
    {
        return;
    }

    // Save the value in the buffer under given Rx list index

    if (synced_var->type == SYNCED_VAR_TYPE_SIGNAL)
    {
        ((struct Synced_var_block_signal *)synced_var->rx_array)[rx_index] = *(struct Synced_var_block_signal *)value;
    }
    else if (synced_var->type == SYNCED_VAR_TYPE_UINT16)
    {
        ((uint16_t *)synced_var->rx_array)[rx_index] = *(uint16_t *)value;
    }
    else
    {
        return;
    }

    // Mark reception of the variable from that Rx list index

    synced_var->rx_bitmap |= rx_index_bitmask;
}



void syncedVarProcess(void)
{
    enum Synced_var_signal signal;

    for (signal = 0; signal < SYNCED_VAR_COUNT; signal++)
    {
        struct Synced_var * synced_var = &synced_vars[signal];

        // Process the variable only if it was received from all expected sources

        if (syncedVarIsReady(synced_var) == true)
        {
            synced_var->processing_func(synced_var);
        }

        // Prepare the variable for the next iteration

        synced_var->rx_bitmap = 0;
    }
}



//! Clears reception bitmasks of all synced variables

void syncedVarDisable(void)
{
    enum Synced_var_signal signal;

    for (signal = 0; signal < SYNCED_VAR_COUNT; signal++)
    {
        syncedVarSetRxBitmask(signal, 0);
    }
}




void syncedVarSetRxBitmask(enum Synced_var_signal const signal, uint16_t const rx_bitmask)
{
    struct Synced_var * synced_var = &synced_vars[signal];

    synced_var->rx_bitmask = rx_bitmask;
    synced_var->rx_bitmap  = 0;
}


// EOF
