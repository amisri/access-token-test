//! @file  defprops_inc_class.h
//! @brief Adds all the class-specific headers needed by defprops.h

#pragma once


// ---------- Includes

#include <diag_class.h>
#include <function_class.h>


// ---------- External variable definitions

extern struct Diag_EPIC_config_properties epic_config;
extern struct Diag_EPIC_runtime_properties epic_properties;
extern struct Diag_EPIC_cycle_based_properties epic_cycle_properties;


// EOF
