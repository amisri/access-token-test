//! @file  class.h
//! @brief This file contains all class related definitions

#pragma once


// ---------- Includes

#include <stdint.h>

#include <dsp_65.h>



// ---------- Constants

#define DEV_STA_TSK_PHASE   2      //!< Millisecond to run stateTask()

// Status masks

#define FGC_FAULTS            ( FGC_FLT_FGC_HW          \
                              | FGC_FLT_FGC_STATE)

#define FGC_WARNINGS          ( FGC_WRN_FGC_HW          \
                              | FGC_WRN_TEMPERATURE     \
                              | FGC_WRN_CONFIG)

#define FGC_STRETCHED_WARNINGS (0)

#define FGC_LATCHED_FGC_HW_WRN ( FGC_LAT_SCIVS_EXP_FLT )


#define SYM_LST_FAULTS   &sym_lst_65_flt[0]
#define SYM_LST_REF      sym_lst_65_ref

#define DSP_CODE         dsp_65_code
#define DSP_CODE_SIZE    DSP_65_CODE_SIZE



// ---------- External function declarations

//! Class specific initialization

void classInit(void);


// EOF
