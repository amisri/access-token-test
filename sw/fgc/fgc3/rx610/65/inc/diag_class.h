//! @file  diag_class.c
//! @brief Class specific functionality for diagnostics

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdio.h>

#include <defconst.h>
#include <fgc_consts_gen.h>
#include <hash.h>
#include <fgc_parser_consts.h>
#include <time_fgc.h>


// ---------- Internal structures, unions and enumerations

// Note: by default, try to put in MCU SRAM

struct Diag_EPIC_DEVICE_config_properties
{
    // We cannot use the CHAR property type for these, because the PC_/FEI_IN names would then require 2D arrays

    char* device_name_ptr;
    char* group_name_ch1_ptr;
    char* group_name_ch2_ptr;
    char* epic_out_name_ptr;

    char* pc_in_name_ptrs[FGC_NUM_PC_IN_PER_EPIC];
    char* fei_in_name_ptrs[FGC_NUM_FEI_IN_PER_EPIC];
    char* cibu_out_name_ptrs[FGC_NUM_OUTPUTS_PER_EPIC];

    char device_name[PROP_DEV_NAME_SIZE];               // EPIC.DEVICE_n.NAME.DEV           string - 23 characters
    char group_name_ch1[PROP_DEV_NAME_SIZE];            // EPIC.DEVICE_n.NAME.GROUP.CH_1    string - 23 characters
    char group_name_ch2[PROP_DEV_NAME_SIZE];            // EPIC.DEVICE_n.NAME.GROUP.CH_2    string - 23 characters
    char epic_out_name[PROP_DEV_NAME_SIZE];             // EPIC.DEVICE_n.NAME.EPIC_OUT  string - 23 characters

    char pc_in_names[FGC_NUM_PC_IN_PER_EPIC][PROP_DEV_NAME_SIZE];       // EPIC.DEVICE_n.NAME.PC_IN     [1..10] string - 23 characters
    char fei_in_names[FGC_NUM_FEI_IN_PER_EPIC][PROP_DEV_NAME_SIZE];     // EPIC.DEVICE_n.NAME.FEI_IN    [1..2]  string - 23 characters
    char cibu_out_names[FGC_NUM_OUTPUTS_PER_EPIC][PROP_DEV_NAME_SIZE];  // EPIC.DEVICE_n.NAME.CIBU_OUT  [1..2] string - 23 characters (or more)

    char fei_1_out_ch[FGC_FEI_CHANNEL_NAME_LENGTH + 1];         // EPIC.DEVICE_n.FEI_1_OUT_CH   Char[4]
    char fei_2_out_ch[FGC_FEI_CHANNEL_NAME_LENGTH + 1];         // EPIC.DEVICE_n.FEI_2_OUT_CH   Char[4]

    bool result_mode_latched;                               // EPIC.RESULT_MODE	ENABLE / DISABLE
                                                            // Perhaps prop would deserve a better name?
};

struct Diag_EPIC_DEVICE_runtime_properties
{
    char version_pcb_a[FGC_VERSION_PCB_STRING_LENGTH + 1];      // EPIC.DEVICE_n.VERSION.PCB_A  string - 4 characters
    char version_pcb_b[FGC_VERSION_PCB_STRING_LENGTH + 1];      // EPIC.DEVICE_n.VERSION.PCB_B  string - 4 characters
    char version_vhdl_a[FGC_VERSION_VHDL_STRING_LENGTH + 1];    // EPIC.DEVICE_n.VERSION.VHDL_A string - 3 characters
    char version_vhdl_b[FGC_VERSION_VHDL_STRING_LENGTH + 1];    // EPIC.DEVICE_n.VERSION.VHDL_B string - 3 characters

    uint8_t enable_pc_in[FGC_NUM_PC_IN_PER_EPIC];           // EPIC.DEVICE_n.ENABLE.PC_IN[1..10]    Bit mask of 2
    uint8_t enable_fei_in[FGC_NUM_FEI_IN_PER_EPIC];         // EPIC.DEVICE_n.ENABLE.FEI_IN[1..2]    Bit mask of 2

    uint8_t status_pc_in[FGC_NUM_PC_IN_PER_EPIC];           // EPIC.DEVICE_n.STATUS.PC_IN   [1..10] Bit mask of 4
    uint8_t status_fei_in;                                  // EPIC.DEVICE_n.STATUS.FEI_IN  Bit mask of 4
    uint8_t status_out;                                     // EPIC.DEVICE_n.STATUS.OUT     Bit mask of 4

    uint8_t faults;
    uint8_t warnings;
};

struct Diag_EPIC_DEVICE_cycle_based_properties
{
    uint8_t result_pc_in[FGC_MAX_USER_PLUS_1][FGC_NUM_PC_IN_PER_EPIC];           // EPIC.DEVICE_n.RESULT.PC_IN   [1..10] Bit mask of 4
    uint8_t result_fei_in[FGC_MAX_USER_PLUS_1];                                  // EPIC.DEVICE_n.RESULT.FEI_IN  Bit mask of 4
    uint8_t result_out[FGC_MAX_USER_PLUS_1];                                     // EPIC.DEVICE_n.RESULT.OUT     Bit mask of 4
};

struct Diag_EPIC_config_properties
{
    struct Diag_EPIC_DEVICE_config_properties devices[FGC_MAX_EPICS];

    // This cannot be a dynamic char pointer, because that is not what GetString expects.
    // Somewhat confusignly, when the property is an *array* of strings, then it is in fact backed by an array of pointers.
    char dim_unresolved_signal[32];

    // Note: there is also dpcls.mcu.epic.configured_devices_mask, which is directly derived from configuration options
};

struct Diag_EPIC_runtime_properties
{
    struct Diag_EPIC_DEVICE_runtime_properties devices[FGC_MAX_EPICS];
};

struct Diag_EPIC_cycle_based_properties
{
    struct Diag_EPIC_DEVICE_cycle_based_properties devices[FGC_MAX_EPICS];
};

/////

//! Get the device name configured for the given EPIC.
//!
//! @return device name, or NULL if the device is unconfigured or the name is empty
char const * diagClassGetDeviceName(int32_t device_index);

bool diagClassIsDeviceConfigured(int32_t device_index);
void diagClassRefreshConfiguration(void);
void diagSchedulePublication(struct CC_us_time const * time, int32_t cyc_sel);

//! Set device fault bitfield to the specified value, and update global FAULTS (**including the latched copy**) accordingly.
//! Since it clears latched faults, this should only be done on explicit user request.
void diagClassResetDeviceFaults(int32_t device_index, uint8_t new_faults);

void diagClassClrAllFaults(void);

// EOF
