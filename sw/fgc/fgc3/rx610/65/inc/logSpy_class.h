//! @file  logSpy_class.h
//! @brief Class specific logging related functions

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External function declarations

//! Sets the acquisition group on the DSP card via the SCIVS bus
//!
//! @param[in]   group        Acquisition group
//!
//! @return                   FGC error

uint16_t logSpyClassSetMeasAcqGroup(uint32_t group);


//! Refresh configuration

void logSpyClassRefreshConfiguration(void);


// EOF
