//! @file    cycSim.c
//! @brief   Module for simulating a supercycle.


// ---------- Includes

#include <stdbool.h>

#include <fgc_event.h>
#include <time_fgc.h>



// ---------- External function definitions

void cycSimInit(void)
{
    // No action
}



bool cycSimIsEnabled(void)
{
    return false;
}



void cycSimProcess(struct CC_ms_time const * utcTime, struct fgc_event * simEvents)
{
    // No action
}


// EOF
