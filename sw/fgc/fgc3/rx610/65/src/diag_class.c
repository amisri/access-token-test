//! @file  diag_class.c
//! @brief Class specific functionality for diagnostics
//
// 1 physical crate
//   -> 8x CARD (bus positions A0..A3, B0..B3)
//     -> 4x DIM (EPIC_A0_DIM1, EPIC_A0_DIM2, EPIC_A0_DIM3, EPIC_A0_DIM4) (actually implemented in 2 FPGAs)
//       -> 2x BANK (0, 1)
//         -> 12x SIGNAL
//
// Note: FEI = Fast Extraction Interlock

// ---------- Includes

#include <stdint.h>
#include <string.h>

#include <diag_class.h>
#include <defprops.h>
#include <device.h>
#include <diag.h>
#include <pub.h>
#include <status.h>
#include <time_fgc.h>



// ---------- Constants

static const char dim_bus_by_epic_index[8][3] =
{
    "A0", "A1", "A2", "A3", "B0", "B1", "B2", "B3"
};



// ---------- Internal structures, unions and enumerations

struct DIM_signal_handle
{
    // Handle is invalid iff data_mask == 0
    // Otherwise data_mask has exactly 1 bit set

    uint16_t data_idx;
    uint16_t data_mask;
};

//! All signals read out by the DIMs shall be declared in this structure
//! This directly mirrors RECAM_DIM_A1.ods

struct Diag_device_signal_handles
{
    // DIM1
    struct DIM_signal_handle dim1_permit_in_w1_bx[FGC_NUM_PC_IN_PER_EPIC];
    struct DIM_signal_handle dim1_permit_in_w1_cl_b;
    struct DIM_signal_handle dim1_cpld_device;      // expect always '1'

    struct DIM_signal_handle dim1_enable_in_bx[FGC_NUM_PC_IN_PER_EPIC];
    struct DIM_signal_handle dim1_enable_in_fei_bx[FGC_NUM_FEI_IN_PER_EPIC];

    // DIM2
    struct DIM_signal_handle dim2_permit_in_w2_bx[FGC_NUM_PC_IN_PER_EPIC];
    struct DIM_signal_handle dim2_permit_in_w2_cl_b;
    struct DIM_signal_handle dim2_cpld_device;      // expect always '1'

    struct DIM_signal_handle dim2_permit_out_w2_b;
    struct DIM_signal_handle dim2_permit_out_w1_b;
    struct DIM_signal_handle dim2_diag_clk_fault;
    struct DIM_signal_handle dim2_diag_data_out_fault;
    struct DIM_signal_handle dim2_diag_data_in_fault;
    struct DIM_signal_handle dim2_vhdl_ver_bits[3];
    struct DIM_signal_handle dim2_pcb_ver_bits[4];

    // DIM3
    struct DIM_signal_handle dim3_permit_in_w1_ax[FGC_NUM_PC_IN_PER_EPIC];
    struct DIM_signal_handle dim3_permit_in_w1_cl_a;
    struct DIM_signal_handle dim3_cpld_device;      // expect always '0'

    struct DIM_signal_handle dim3_enable_in_ax[FGC_NUM_PC_IN_PER_EPIC];
    struct DIM_signal_handle dim3_enable_in_fei_ax[FGC_NUM_FEI_IN_PER_EPIC];

    // DIM4
    struct DIM_signal_handle dim4_permit_in_w2_ax[FGC_NUM_PC_IN_PER_EPIC];
    struct DIM_signal_handle dim4_permit_in_w2_cl_a;
    struct DIM_signal_handle dim4_cpld_device;      // expect always '0'

    struct DIM_signal_handle dim4_permit_out_w2_a;
    struct DIM_signal_handle dim4_permit_out_w1_a;
    struct DIM_signal_handle dim4_diag_clk_fault;
    struct DIM_signal_handle dim4_diag_data_out_fault;
    struct DIM_signal_handle dim4_diag_data_in_fault;
    struct DIM_signal_handle dim4_vhdl_ver_bits[3];
    struct DIM_signal_handle dim4_pcb_ver_bits[4];
};

struct diag_class_local
{
    struct Diag_device_signal_handles handles[FGC_MAX_EPICS];

    bool               publication_scheduled;
    struct CC_us_time  publication_scheduled_time;
    int32_t            publication_cyc_sel;

    char const * last_unresolved_signal;
    int32_t      events_missed;
    int32_t      last_delay_us;
};



// ---------- External variable definitions

struct Diag_EPIC_config_properties      epic_config;
struct Diag_EPIC_runtime_properties     epic_properties;
struct Diag_EPIC_cycle_based_properties epic_cycle_properties;



// ---------- Internal variable definitions

static struct diag_class_local local;

static uint16_t const fault_mask_by_device_index[FGC_MAX_EPICS] =
{
    FGC_FLT_EPIC_1, FGC_FLT_EPIC_2, FGC_FLT_EPIC_3, FGC_FLT_EPIC_4,
    FGC_FLT_EPIC_5, FGC_FLT_EPIC_6, FGC_FLT_EPIC_7, FGC_FLT_EPIC_8,
};

static uint16_t const warning_mask_by_device_index[FGC_MAX_EPICS] =
{
    FGC_WRN_EPIC_1, FGC_WRN_EPIC_2, FGC_WRN_EPIC_3, FGC_WRN_EPIC_4,
    FGC_WRN_EPIC_5, FGC_WRN_EPIC_6, FGC_WRN_EPIC_7, FGC_WRN_EPIC_8,
};



// ---------- Internal function declarations




// ---------- Internal function definitions

static bool diagGetDigChanHandle(int32_t logical_dim, char * chan_name, struct DIM_signal_handle * handle_out)
{
    if (logical_dim >= 0 && DiagFindDigChan(logical_dim, chan_name, &handle_out->data_idx, &handle_out->data_mask) == true)
    {
        return true;
    }
    else
    {
        if (logical_dim >= 0)
        {
            // If DIM exists, but signal was not found, remember it
            local.last_unresolved_signal = chan_name;
        }

        return false;
    }
}

// TODO: doxygen
static void diagGetDigChanHandleArray(int32_t                    logical_dim,
                                      char                     * chan_name_fmt,
                                      struct DIM_signal_handle * handles_out,
                                      int32_t                    base,
                                      int32_t                    count)
{
    for (int32_t i = 0; i < count; i++)
    {
        char signal_name[32];

        snprintf(signal_name, sizeof(signal_name), chan_name_fmt, base + i);
        diagGetDigChanHandle(logical_dim, signal_name, &handles_out[i]);
    }
}



//! Fina a DIM by name and return its logical index.
//!
//! @param sys_idx  System index in SysDB, typically `device.sys.sys_idx`
//! @param name     The sought DIM name, as a NUL-terminated string
//! @return         Index between 0 and FGC_MAX_DIMS-1, or -1 if the DIM was not found.

static int32_t diagFindLogicalDimByName(int32_t sys_idx, const char * name)
{
    // Algorithm based on DiagFindAnaChan code
    for (int32_t logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        // If DIM channel is in use
        if (SysDbDimEnabled(sys_idx, logical_dim))
        {
            // Name is pointer to dim_name_t, e.g. a NUL-terminated string
            const char * dim_name = SysDbDimName(sys_idx, logical_dim);

            if (name != NULL && strcmp(dim_name, name) == 0)
            {
                return logical_dim;
            }
        }
    }

    return -1;
}



static bool diagIsSet(struct DIM_signal_handle handle)
{
    return (handle.data_mask != 0 && testBitmap(diag.data.w[handle.data_idx], handle.data_mask) == true);
}



// ---------- External function definitions

void diagClassInit(void)
{
    memset(&local, 0, sizeof(local));

    dpcls.mcu.epic.configured_devices_mask = 0;

    // Find digital signals

    // Resolve logical DIMs
    // They are named like "EPIC_xx_DIMn" where
    //    xx is A0, A1, ... (position of EPIC on the local bus, per dim_bus_by_epic_index), and
    //    n in 1..4 (DIMs in each EPIC)

    int32_t device_index;       // "Local" index of the EPIC (0-7)
    int32_t logical_dims_by_epic[8][4];

    for (device_index = 0; device_index < FGC_MAX_EPICS; device_index++)
    {
        for (int32_t dim_index = 0; dim_index < 4; dim_index++)
        {
            char name[16+1];
            snprintf(name, sizeof(name), "EPIC_%s_DIM%d", dim_bus_by_epic_index[device_index], 1 + (int)dim_index);

            logical_dims_by_epic[device_index][dim_index] = diagFindLogicalDimByName(device.sys.sys_idx, name);
        }
    }

    // Resolve DIM signals

    for (device_index = 0; device_index < FGC_MAX_EPICS; device_index++)
    {
        struct Diag_device_signal_handles * handles = local.handles;

        // DIM1
        {
            int32_t dim1 = logical_dims_by_epic[device_index][0];
            diagGetDigChanHandleArray(dim1, "PERMIT_IN_W1_B%d",     &handles[device_index].dim1_permit_in_w1_bx[0], 1, FGC_NUM_PC_IN_PER_EPIC);
            diagGetDigChanHandle(     dim1, "PERMIT_IN_W1_FEI_B",   &handles[device_index].dim1_permit_in_w1_cl_b);
            diagGetDigChanHandle(     dim1, "CPLD_DEVICE",          &handles[device_index].dim1_cpld_device);

            diagGetDigChanHandleArray(dim1, "ENABLE_IN_B%d",        &handles[device_index].dim1_enable_in_bx[0],     1, FGC_NUM_PC_IN_PER_EPIC);
            diagGetDigChanHandleArray(dim1, "ENABLE_IN_FEI_B%d",    &handles[device_index].dim1_enable_in_fei_bx[0], 1, FGC_NUM_FEI_IN_PER_EPIC);
        }

        // DIM2
        {
            int32_t dim2 = logical_dims_by_epic[device_index][1];
            diagGetDigChanHandleArray(dim2, "PERMIT_IN_W2_B%d",     &handles[device_index].dim2_permit_in_w2_bx[0], 1, FGC_NUM_PC_IN_PER_EPIC);
            diagGetDigChanHandle(     dim2, "PERMIT_IN_W2_FEI_B",   &handles[device_index].dim2_permit_in_w2_cl_b);
            diagGetDigChanHandle(     dim2, "CPLD_DEVICE",          &handles[device_index].dim2_cpld_device);

            diagGetDigChanHandle(     dim2, "PERMIT_OUT_W2_B",      &handles[device_index].dim2_permit_out_w2_b);
            diagGetDigChanHandle(     dim2, "PERMIT_OUT_W1_B",      &handles[device_index].dim2_permit_out_w1_b);
            diagGetDigChanHandle(     dim2, "DIAG_CLK_FAULT",       &handles[device_index].dim2_diag_clk_fault);
            diagGetDigChanHandle(     dim2, "DIAG_DATA_OUT_FAULT",  &handles[device_index].dim2_diag_data_out_fault);
            diagGetDigChanHandle(     dim2, "DIAG_DATA_IN_FAULT",   &handles[device_index].dim2_diag_data_in_fault);

            diagGetDigChanHandleArray(dim2, "VHDL_VER_BIT%d",       &handles[device_index].dim2_vhdl_ver_bits[0], 0, 3);
            diagGetDigChanHandleArray(dim2, "PCB_VER_BIT%d",        &handles[device_index].dim2_pcb_ver_bits[0],  0, 4);
        }

        // DIM3
        {
            int32_t dim3 = logical_dims_by_epic[device_index][2];
            diagGetDigChanHandleArray(dim3, "PERMIT_IN_W1_A%d",     &handles[device_index].dim3_permit_in_w1_ax[0], 1, FGC_NUM_PC_IN_PER_EPIC);
            diagGetDigChanHandle(     dim3, "PERMIT_IN_W1_FEI_A",   &handles[device_index].dim3_permit_in_w1_cl_a);
            diagGetDigChanHandle(     dim3, "CPLD_DEVICE",          &handles[device_index].dim3_cpld_device);

            diagGetDigChanHandleArray(dim3, "ENABLE_IN_A%d",        &handles[device_index].dim3_enable_in_ax[0],     1, FGC_NUM_PC_IN_PER_EPIC);
            diagGetDigChanHandleArray(dim3, "ENABLE_IN_FEI_A%d",    &handles[device_index].dim3_enable_in_fei_ax[0], 1, FGC_NUM_FEI_IN_PER_EPIC);
        }

        // DIM4
        {
            int32_t dim4 = logical_dims_by_epic[device_index][3];
            diagGetDigChanHandleArray(dim4, "PERMIT_IN_W2_A%d",     &handles[device_index].dim4_permit_in_w2_ax[0], 1, FGC_NUM_PC_IN_PER_EPIC);
            diagGetDigChanHandle(     dim4, "PERMIT_IN_W2_FEI_A",   &handles[device_index].dim4_permit_in_w2_cl_a);
            diagGetDigChanHandle(     dim4, "CPLD_DEVICE",          &handles[device_index].dim4_cpld_device);

            diagGetDigChanHandle(     dim4, "PERMIT_OUT_W2_A",      &handles[device_index].dim4_permit_out_w2_a);
            diagGetDigChanHandle(     dim4, "PERMIT_OUT_W1_A",      &handles[device_index].dim4_permit_out_w1_a);
            diagGetDigChanHandle(     dim4, "DIAG_CLK_FAULT",       &handles[device_index].dim4_diag_clk_fault);
            diagGetDigChanHandle(     dim4, "DIAG_DATA_OUT_FAULT",  &handles[device_index].dim4_diag_data_out_fault);
            diagGetDigChanHandle(     dim4, "DIAG_DATA_IN_FAULT",   &handles[device_index].dim4_diag_data_in_fault);

            diagGetDigChanHandleArray(dim4, "VHDL_VER_BIT%d",       &handles[device_index].dim4_vhdl_ver_bits[0], 0, 3);
            diagGetDigChanHandleArray(dim4, "PCB_VER_BIT%d",        &handles[device_index].dim4_pcb_ver_bits[0],  0, 4);
        }
    }

    if (local.last_unresolved_signal != NULL)
    {
        // unlike strncpy, snprintf always ensures termination
        snprintf(epic_config.dim_unresolved_signal, sizeof(epic_config.dim_unresolved_signal), "%s", local.last_unresolved_signal);
    }
    else
    {
        epic_config.dim_unresolved_signal[0] = '\0';
    }

    // Call this after loading non-volatile configuration to ensure log menus are named properly etc.

    diagClassRefreshConfiguration();
}



void diagClassCheckConverter(void)
{
    // This function is called from stateTask() every 5ms.
}



void diagClassClrAllFaults(void)
{
    for (int32_t device_index = 0; device_index < FGC_MAX_EPICS; device_index++)
    {
        // If the "real" fault persists, it will be soon restored by diagClassUpdateMeas

        epic_properties.devices[device_index].faults = 0;
    }
}



char const * diagClassGetDeviceName(int32_t const device_index)
{
    if (   diagClassIsDeviceConfigured(device_index)
        && epic_config.devices[device_index].device_name_ptr != NULL
        && epic_config.devices[device_index].device_name_ptr[0] != 0)
    {
        return epic_config.devices[device_index].device_name_ptr;
    }
    else
    {
        return NULL;
    }
}



bool diagClassIsDeviceConfigured(int32_t const device_index)
{
    return testBitmap(dpcls.mcu.epic.configured_devices_mask, 1 << device_index);
}



void diagClassRefreshConfiguration(void)
{
    // Ingest & validate EPIC configuration

    int32_t last_active = -1;
    bool    gaps_found  = false;

    for (int32_t device_index = 0; device_index < FGC_MAX_EPICS; device_index++)
    {
        // EPIC name is set <=> EPIC is considered configured
        // Note: device name should never be set to an empty string

        bool device_configured = (strcmp(epic_config.devices[device_index].device_name, "NONE") != 0);

        updateBitmap(dpcls.mcu.epic.configured_devices_mask, 1 << device_index, device_configured);

        if (device_configured == true)
        {
            // Previous device is not configured, but this one is?
            if (last_active < device_index - 1)
            {
                gaps_found = true;
            }

            last_active = device_index;
        }
    }

    // Number of active EPICs is now equal to (last_active + 1)
    // If no EPICs have been configured, or there are gaps, raise the corresponding warning.

    statusEvaluateWarnings(FGC_WRN_EPIC_CONFIG, last_active < 0 || gaps_found == true);
}



void diagClassResetDeviceFaults(int32_t device_index, uint8_t new_faults)
{
    epic_properties.devices[device_index].faults = new_faults;
    statusEvaluateFaults(fault_mask_by_device_index[device_index], new_faults != 0);

    // Reset latched fault bit to reflect new value
    statusFaultsClr(fault_mask_by_device_index[device_index]);
}



void diagClassUpdateMeas(void)
{
    // Iterate every EPIC-related diag signal, check it, and put into the property backing structure

    int32_t            device_index;       // "Local" index of the EPIC (0-7)
    struct CC_ms_time cycle_start_time;

    DimGetCycleStartTime(&cycle_start_time);

    for (device_index = 0; device_index < FGC_MAX_EPICS; device_index++)
    {
        // If the device is deactivated, there is nothing for us to do
        // TODO: should we memset the values to zero or keep the last snapshot?
        if (diagClassIsDeviceConfigured(device_index) == false)
        {
            statusClrFaults(fault_mask_by_device_index[device_index]);
            statusClrWarnings(warning_mask_by_device_index[device_index]);
            continue;
        }

        struct Diag_EPIC_DEVICE_runtime_properties * dev         = &epic_properties.devices[device_index];
        struct Diag_device_signal_handles          * dev_handles = &local.handles[device_index];

        uint32_t log_en_wn    = 0;    // ENABLE + WARNINGS bitmap for log
        uint32_t log_res_w1   = 0;    // RESULT_W1 bitmap for log
        uint32_t log_res_w2   = 0;    // RESULT_W2 bitmap for log
        int32_t  new_warnings = 0;

        for (int32_t i = 0; i < FGC_VERSION_PCB_STRING_LENGTH; i++)
        {
            dev->version_pcb_a[i] = diagIsSet(dev_handles->dim2_pcb_ver_bits[i]) ? '1' : '0';
            dev->version_pcb_b[i] = diagIsSet(dev_handles->dim4_pcb_ver_bits[i]) ? '1' : '0';

            if (dev->version_pcb_a[i] != dev->version_pcb_b[i])
            {
                new_warnings |= FGC_EPIC_DEVICE_WARNINGS_A_B_MISMATCH;
            }
        }

        for (int32_t i = 0; i < FGC_VERSION_VHDL_STRING_LENGTH; i++)
        {
            dev->version_vhdl_a[i] = diagIsSet(dev_handles->dim2_vhdl_ver_bits[i]) ? '1' : '0';
            dev->version_vhdl_b[i] = diagIsSet(dev_handles->dim4_vhdl_ver_bits[i]) ? '1' : '0';

            if (dev->version_vhdl_a[i] != dev->version_vhdl_b[i])
            {
                new_warnings |= FGC_EPIC_DEVICE_WARNINGS_A_B_MISMATCH;
            }
        }

        for (int32_t i = 0; i < FGC_NUM_PC_IN_PER_EPIC; i++)
        {
            dev->enable_pc_in[i] = (diagIsSet(dev_handles->dim3_enable_in_ax[i]) ? FGC_EPIC_DEVICE_ENABLE_ENABLED_A : 0)
                                 | (diagIsSet(dev_handles->dim1_enable_in_bx[i]) ? FGC_EPIC_DEVICE_ENABLE_ENABLED_B : 0);

            // ENABLE + WARNINGS word:
            //  - enable_in_b in bits [0:10]
            //  - enable_in_a in bits [10:20]
            log_en_wn |= (diagIsSet(dev_handles->dim1_enable_in_bx[i]) ? (1 << i) : 0);
            log_en_wn |= (diagIsSet(dev_handles->dim3_enable_in_ax[i]) ? (1 << (10 + i)) : 0);

            dev->status_pc_in[i] = (diagIsSet(dev_handles->dim3_permit_in_w1_ax[i]) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_1A : 0)
                                 | (diagIsSet(dev_handles->dim1_permit_in_w1_bx[i]) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_1B : 0)
                                 | (diagIsSet(dev_handles->dim4_permit_in_w2_ax[i]) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_2A : 0)
                                 | (diagIsSet(dev_handles->dim2_permit_in_w2_bx[i]) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_2B : 0);

            // RESULT_W1 word:
            //  - RESULT_PCx_W1_B_IN in bits [0:10]
            //  - RESULT_PCx_W1_A_IN in bits [10:20]
            log_res_w1 |= (diagIsSet(dev_handles->dim1_permit_in_w1_bx[i]) ? (1 << i) : 0);
            log_res_w1 |= (diagIsSet(dev_handles->dim3_permit_in_w1_ax[i]) ? (1 << (10 + i)) : 0);

            // RESULT_W2 word:
            //  - RESULT_PCx_W2_B_IN in bits [0:10]
            //  - RESULT_PCx_W2_A_IN in bits [10:20]
            log_res_w2 |= (diagIsSet(dev_handles->dim2_permit_in_w2_bx[i]) ? (1 << i) : 0);
            log_res_w2 |= (diagIsSet(dev_handles->dim4_permit_in_w2_ax[i]) ? (1 << (10 + i)) : 0);

            if (   diagIsSet(dev_handles->dim3_enable_in_ax[i]) != diagIsSet(dev_handles->dim1_enable_in_bx[i])
                || diagIsSet(dev_handles->dim3_permit_in_w1_ax[i]) != diagIsSet(dev_handles->dim1_permit_in_w1_bx[i])
                || diagIsSet(dev_handles->dim4_permit_in_w2_ax[i]) != diagIsSet(dev_handles->dim2_permit_in_w2_bx[i]))
            {
                new_warnings |= FGC_EPIC_DEVICE_WARNINGS_A_B_MISMATCH;
            }

            // Raise warning if a permit is being given for a disabled channel
            bool mismatch_a =    !diagIsSet(dev_handles->dim3_enable_in_ax[i])
                              && (diagIsSet(dev_handles->dim3_permit_in_w1_ax[i]) || diagIsSet(dev_handles->dim4_permit_in_w2_ax[i]));
            bool mismatch_b =    !diagIsSet(dev_handles->dim1_enable_in_bx[i])
                              && (diagIsSet(dev_handles->dim1_permit_in_w1_bx[i]) || diagIsSet(dev_handles->dim2_permit_in_w2_bx[i]));
            if (mismatch_a || mismatch_b)
            {
                new_warnings |= FGC_EPIC_DEVICE_WARNINGS_CH_STATE_WRN;
            }
        }

        dev->status_fei_in = (diagIsSet(dev_handles->dim3_permit_in_w1_cl_a) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_1A : 0)
                           | (diagIsSet(dev_handles->dim1_permit_in_w1_cl_b) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_1B : 0)
                           | (diagIsSet(dev_handles->dim4_permit_in_w2_cl_a) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_2A : 0)
                           | (diagIsSet(dev_handles->dim2_permit_in_w2_cl_b) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_2B : 0);

        dev->status_out = (diagIsSet(dev_handles->dim4_permit_out_w1_a) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_1A : 0)
                        | (diagIsSet(dev_handles->dim2_permit_out_w1_b) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_1B : 0)
                        | (diagIsSet(dev_handles->dim4_permit_out_w2_a) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_2A : 0)
                        | (diagIsSet(dev_handles->dim2_permit_out_w2_b) ? FGC_EPIC_DEVICE_PERMIT_PERMIT_2B : 0);

        // RESULT_W1 word:
        //  - RESULT_FEI1_W1_B_IN in bit 20
        //  - RESULT_FEI1_W1_A_IN in bit 21
        //  - RESULT_W1_B_OUT in bit 22
        //  - RESULT_W1_A_OUT in bit 23

        log_res_w1 |= (diagIsSet(dev_handles->dim1_permit_in_w1_cl_b) ? (1 << 20) : 0);
        log_res_w1 |= (diagIsSet(dev_handles->dim3_permit_in_w1_cl_a) ? (1 << 21) : 0);
        log_res_w1 |= (diagIsSet(dev_handles->dim2_permit_out_w1_b) ? (1 << 22) : 0);
        log_res_w1 |= (diagIsSet(dev_handles->dim4_permit_out_w1_a) ? (1 << 23) : 0);

        // RESULT_W2 word:
        //  - RESULT_FEI2_W2_B_IN in bit 20
        //  - RESULT_FEI2_W2_A_IN in bit 21
        //  - RESULT_W2_B_OUT in bit 22
        //  - RESULT_W2_A_OUT in bit 23

        log_res_w2 |= (diagIsSet(dev_handles->dim2_permit_in_w2_cl_b) ? (1 << 20) : 0);
        log_res_w2 |= (diagIsSet(dev_handles->dim4_permit_in_w2_cl_a) ? (1 << 21) : 0);
        log_res_w2 |= (diagIsSet(dev_handles->dim2_permit_out_w2_b) ? (1 << 22) : 0);
        log_res_w2 |= (diagIsSet(dev_handles->dim4_permit_out_w2_a) ? (1 << 23) : 0);

        if (   diagIsSet(dev_handles->dim3_permit_in_w1_cl_a) != diagIsSet(dev_handles->dim1_permit_in_w1_cl_b)
            || diagIsSet(dev_handles->dim4_permit_in_w2_cl_a) != diagIsSet(dev_handles->dim2_permit_in_w2_cl_b)
            || diagIsSet(dev_handles->dim4_permit_out_w1_a)   != diagIsSet(dev_handles->dim2_permit_out_w1_b)
            || diagIsSet(dev_handles->dim4_permit_out_w2_a)   != diagIsSet(dev_handles->dim2_permit_out_w2_b))
        {
            new_warnings |= FGC_EPIC_DEVICE_WARNINGS_A_B_MISMATCH;
        }

        for (int32_t i = 0; i < FGC_NUM_FEI_IN_PER_EPIC; i++)
        {
            dev->enable_fei_in[i] = (diagIsSet(dev_handles->dim3_enable_in_fei_ax[i]) ? FGC_EPIC_DEVICE_ENABLE_ENABLED_A : 0)
                                  | (diagIsSet(dev_handles->dim1_enable_in_fei_bx[i]) ? FGC_EPIC_DEVICE_ENABLE_ENABLED_B : 0);

            // ENABLE + WARNINGS word:
            //  - ENABLE_FEIx_A_IN in bits [20:22]
            //  - ENABLE_FEIx_B_IN in bits [22:24]
            // (huh? why are A x B swapped only in this case?)
            log_en_wn |= (diagIsSet(dev_handles->dim3_enable_in_fei_ax[i]) ? (1 << (20 + i)) : 0);
            log_en_wn |= (diagIsSet(dev_handles->dim1_enable_in_fei_bx[i]) ? (1 << (22 + i)) : 0);

            if (diagIsSet(dev_handles->dim3_enable_in_fei_ax[i]) != diagIsSet(dev_handles->dim1_enable_in_fei_bx[i]))
            {
                new_warnings |= FGC_EPIC_DEVICE_WARNINGS_A_B_MISMATCH;
            }
        }

        // Raise warning if a permit is being given for a disabled channel
        if ((!diagIsSet(dev_handles->dim3_enable_in_fei_ax[0]) && diagIsSet(dev_handles->dim3_permit_in_w1_cl_a))
            || (!diagIsSet(dev_handles->dim3_enable_in_fei_ax[1]) && diagIsSet(dev_handles->dim4_permit_in_w2_cl_a))
            || (!diagIsSet(dev_handles->dim1_enable_in_fei_bx[0]) && diagIsSet(dev_handles->dim1_permit_in_w1_cl_b))
            || (!diagIsSet(dev_handles->dim1_enable_in_fei_bx[1]) && diagIsSet(dev_handles->dim2_permit_in_w2_cl_b)))
        {
            new_warnings |= FGC_EPIC_DEVICE_WARNINGS_CH_STATE_WRN;
        }

        int32_t new_faults = (diagIsSet(dev_handles->dim2_diag_data_in_fault)  ? FGC_EPIC_DEVICE_FAULTS_DIF_A : 0)
                           | (diagIsSet(dev_handles->dim2_diag_clk_fault)      ? FGC_EPIC_DEVICE_FAULTS_DCF_A : 0)
                           | (diagIsSet(dev_handles->dim2_diag_data_out_fault) ? FGC_EPIC_DEVICE_FAULTS_DOF_A : 0)
                           | (diagIsSet(dev_handles->dim4_diag_data_in_fault)  ? FGC_EPIC_DEVICE_FAULTS_DIF_B : 0)
                           | (diagIsSet(dev_handles->dim4_diag_clk_fault)      ? FGC_EPIC_DEVICE_FAULTS_DCF_B : 0)
                           | (diagIsSet(dev_handles->dim4_diag_data_out_fault) ? FGC_EPIC_DEVICE_FAULTS_DOF_B : 0);

        // Propagate device-level warnings

        if (   diagIsSet(dev_handles->dim2_diag_data_in_fault)  != diagIsSet(dev_handles->dim4_diag_data_in_fault)
            || diagIsSet(dev_handles->dim2_diag_clk_fault)      != diagIsSet(dev_handles->dim4_diag_clk_fault)
            || diagIsSet(dev_handles->dim2_diag_data_out_fault) != diagIsSet(dev_handles->dim4_diag_data_out_fault))
        {
            new_warnings |= FGC_EPIC_DEVICE_WARNINGS_FAULT_MSMATCH;
        }

        // EDMS 2151280, Figure 17 ilustrates the relation of the CPLDs to the virtual DIMs. Here we check that the values received are as expected.
        // If they are not, something is quite likely wrong, such as the EPIC not being powered at all or a wiring problem.
        if (   diagIsSet(dev_handles->dim1_cpld_device) != false
            || diagIsSet(dev_handles->dim2_cpld_device) != false
            || diagIsSet(dev_handles->dim3_cpld_device) != true
            || diagIsSet(dev_handles->dim4_cpld_device) != true)
        {
            new_warnings |= FGC_EPIC_DEVICE_WARNINGS_CPLD_MISMATCH;
        }

        // Add faults to those already registered, latch until manually cleared
        dev->faults = (dev->faults | new_faults);

        // On the other hand, warnings are *not* latched
        dev->warnings = new_warnings;

        // ENABLE + WARNINGS word:
        //  - A_B_MISMATCHED        in bit 24
        //  - FAULT_MISMATCHED      in bit 25
        //  - CPLD_MISMATCHED       in bit 26
        //  - CH_STATE_WRN          in bit 27
        log_en_wn |= ((new_warnings & FGC_EPIC_DEVICE_WARNINGS_A_B_MISMATCH) ? (1 << 24) : 0);
        log_en_wn |= ((new_warnings & FGC_EPIC_DEVICE_WARNINGS_FAULT_MSMATCH) ? (1 << 25) : 0);
        log_en_wn |= ((new_warnings & FGC_EPIC_DEVICE_WARNINGS_CPLD_MISMATCH) ? (1 << 26) : 0);
        log_en_wn |= ((new_warnings & FGC_EPIC_DEVICE_WARNINGS_CH_STATE_WRN) ? (1 << 27) : 0);

        // Propagate global fault/warning flags -- one bit per device aggregating any faults and likewise for warnings

        statusEvaluateFaults(fault_mask_by_device_index[device_index], new_faults != 0);
        statusEvaluateWarnings(warning_mask_by_device_index[device_index], new_warnings != 0);

        // Save log data

        dpcls.mcu.log_data.epic_en_wn[device_index]  = log_en_wn;
        dpcls.mcu.log_data.epic_res_w1[device_index] = log_res_w1;
        dpcls.mcu.log_data.epic_res_w2[device_index] = log_res_w2;
    }

    if (local.publication_scheduled == true)
    {
        // Calculate remaining time to publication
        struct CC_us_time now = { { timeGetUtcTime() }, timeGetUtcTimeMs() * 1000 };
        int32_t delay_us = timeUsDiff(&now, &local.publication_scheduled_time);

        // If the scheduled time has passed, go ahead with the publication
        // (it is unlikely that we would align with the event exactly, since the DIM update is triggered by an independent 50 Hz timer)
        if (delay_us <= 0)
        {
            // Trigger publication
            // TODO: ensure that the correct cycle stamp is reported (cmd.c: CmdPrintTimestamp)

            for (device_index = 0; device_index < FGC_MAX_EPICS; device_index++)
            {
                struct Diag_EPIC_DEVICE_runtime_properties * dev = &epic_properties.devices[device_index];
                struct Diag_EPIC_DEVICE_cycle_based_properties * dev_res = &epic_cycle_properties.devices[device_index];

                for (int32_t i = 0; i < FGC_NUM_PC_IN_PER_EPIC; i++)
                {
                    dev_res->result_pc_in[local.publication_cyc_sel][i] = dev->status_pc_in[i];
                }

                dev_res->result_fei_in[local.publication_cyc_sel] = dev->status_fei_in;
                dev_res->result_out[local.publication_cyc_sel] = dev->status_out;
            }

            pubPublishProperty(&PROP_EPIC_DEVICE_1_RESULT_PC_IN, PUB_NO_SUB_SEL, local.publication_cyc_sel, false);
            pubPublishProperty(&PROP_EPIC_DEVICE_2_RESULT_PC_IN, PUB_NO_SUB_SEL, local.publication_cyc_sel, false);
            pubPublishProperty(&PROP_EPIC_DEVICE_3_RESULT_PC_IN, PUB_NO_SUB_SEL, local.publication_cyc_sel, false);
            pubPublishProperty(&PROP_EPIC_DEVICE_4_RESULT_PC_IN, PUB_NO_SUB_SEL, local.publication_cyc_sel, false);
            pubPublishProperty(&PROP_EPIC_DEVICE_5_RESULT_PC_IN, PUB_NO_SUB_SEL, local.publication_cyc_sel, false);
            pubPublishProperty(&PROP_EPIC_DEVICE_6_RESULT_PC_IN, PUB_NO_SUB_SEL, local.publication_cyc_sel, false);
            pubPublishProperty(&PROP_EPIC_DEVICE_7_RESULT_PC_IN, PUB_NO_SUB_SEL, local.publication_cyc_sel, false);
            pubPublishProperty(&PROP_EPIC_DEVICE_8_RESULT_PC_IN, PUB_NO_SUB_SEL, local.publication_cyc_sel, false);

            local.publication_scheduled = false;
            local.last_delay_us         = delay_us;
        }
    }

    // Feed log data to the DSP
    dpcls.mcu.log_data.time_stamp = cycle_start_time;

    __sync_synchronize();

    // This triggers the DSP to start iterating over the devices and save log data
    dpcls.mcu.log_data.device_index = 0;
}



void diagSchedulePublication(struct CC_us_time const * time, int32_t cyc_sel)
{
    // (Note: this assumes that a race condition with diagClassUpdateMeas is impossible)

    if (local.publication_scheduled == true)
    {
        local.events_missed++;
    }

    // TODO: do we risk losing events this way?
    local.publication_scheduled      = true;
    local.publication_scheduled_time = *time;
    local.publication_cyc_sel        = cyc_sel;
}


// EOF
