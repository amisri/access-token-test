//! @file  modePC_class.c
//! @brief Class related handling of modePc


// ---------- Includes

#include <stdint.h>

#include <defprops.h>
#include <modePc_class.h>
#include <statePc.h>
#include <pc_state.h>
#include <fgc_errs.h>



// ---------- Platform/class specific function definitions

uint32_t modePcClassValidateMode(uint8_t const mode)
{
    switch (mode)
    {
        case FGC_PC_OFF:
        {
            if (pcStateTest(FGC_STATE_VALID_RESET_BIT_MASK) == true)
            {
                statePcResetFaults();
            }

            break;
        }

        case FGC_PC_SLOW_ABORT:  // Fall through
        case FGC_PC_ON_STANDBY:  // Fall through
        case FGC_PC_CYCLING:     // Fall through
        case FGC_PC_IDLE:        // Fall through
        case FGC_PC_DIRECT:      return FGC_BAD_STATE;

        case FGC_PC_BLOCKING:    // Fall through
        default:                 break;
    }

    return FGC_OK_NO_RSP;
}

// EOF
