//! @file  polSwitch.c
//! @brief This file provides the handling of the polarity switch.
//!
//!  Since class FGC_65 does not handle polarity switches, the functions
//!  are empty to satisfy the base code


// ---------- Includes

#include <stdint.h>



// ---------- Platform/class specific function definitions  -------------

void polSwitchInit(void)
{
    ; // Do nothing
}



void polSwitchProcess(void)
{
    ; // Do nothing
}



void polSwitchReset(void)
{
    ; // Do nothing
}



uint16_t polSwitchGetCommand(void)
{
    return 0;
}


uint16_t polSwitchGetState(void)
{
    return 0;
}


// EOF
