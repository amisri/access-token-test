//! @file  class.c
//! @brief This file contains all class related definitions


// ---------- Includes

#include <hash.h>


// Instantiate variables in headers with no associated source files

#define OS_GLOBALS
#define SHARED_MEMORY_GLOBALS
#define FGC_PARSER_CONSTS
#define FGC_COMPDB_GLOBALS
#define FGC_SYSDB_GLOBALS
#define FGC_DIMDB_GLOBALS
#define CLASS_GLOBALS
#define FGC_TASK_TRACE_GLOBALS
#define PC_STATE_GLOBALS
#define CRATE_GLOBALS
#define DEFPROPS
#define DEFSYMS


#include <os.h>
#include <dpcls.h>
#include <dpcom.h>
#include <dpcmd.h>
#include <sharedMemory.h>
#include <fgc_parser_consts.h>
#include <class.h>
#include <taskTrace.h>
#include <pc_state.h>
#include <modePc.h>
#include <statePc.h>
#include <crate.h>
#include <diag_class.h>
#include <defprops.h>
#include <defsyms.h>



// ---------- External variable definitions

volatile struct Dpcom __attribute__((section("dpcom"))) dpcom;
volatile struct Dpcls __attribute__((section("dpcls"))) dpcls;

OS_SEM * dpcmd_sem;



// ---------- Platform/class specific function definitions

void classInit(void)
{
    // Default initialisations

    device.max_user            = 0;
    device.max_sub_devs        = 0;
    vs.present                 = FGC_CTRL_ENABLED;
    dpcls.oasis.subsampling    = 1;
    dpcom.mcu.device_ppm       = FGC_CTRL_DISABLED;
    dpcom.mcu.device_multi_ppm = FGC_CTRL_DISABLED;

    dpcls.mcu.log_data.device_index = -1;

    // Initialize properties

    memset(&epic_config, 0, sizeof(epic_config));
    memset(&epic_properties, 0, sizeof(epic_properties));
    memset(&epic_cycle_properties, 0, sizeof(epic_cycle_properties));

    for (int32_t device_index = 0; device_index < FGC_MAX_EPICS; device_index++)
    {
        epic_config.devices[device_index].device_name_ptr = &epic_config.devices[device_index].device_name[0];
        epic_config.devices[device_index].group_name_ch1_ptr = &epic_config.devices[device_index].group_name_ch1[0];
        epic_config.devices[device_index].group_name_ch2_ptr = &epic_config.devices[device_index].group_name_ch2[0];
        epic_config.devices[device_index].epic_out_name_ptr = &epic_config.devices[device_index].epic_out_name[0];

        for (int32_t i = 0; i < FGC_NUM_PC_IN_PER_EPIC; i++)
        {
            epic_config.devices[device_index].pc_in_name_ptrs[i] = &epic_config.devices[device_index].pc_in_names[i][0];
        }

        for (int32_t i = 0; i < FGC_NUM_FEI_IN_PER_EPIC; i++)
        {
            epic_config.devices[device_index].fei_in_name_ptrs[i] = &epic_config.devices[device_index].fei_in_names[i][0];
        }

        for (int32_t i = 0; i < FGC_NUM_OUTPUTS_PER_EPIC; i++)
        {
            epic_config.devices[device_index].cibu_out_name_ptrs[i] = &epic_config.devices[device_index].cibu_out_names[i][0];
        }
    }
}


// EOF
