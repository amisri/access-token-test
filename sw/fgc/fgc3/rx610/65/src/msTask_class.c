//! @file  msTask_class.c
//! @brief Class specific millisecond actions


// ---------- Includes

#include <msTask.h>
#include <diag_class.h>
#include <fbs_class.h>
#include <function_class.h>
#include <status.h>



// ---------- Internal function declarations

static void msTaskClassPublishData(void);



// ---------- Platform/class specific function definitions

void msTaskClassProcess(void)
{
    msTaskClassPublishData();
}



// ---------- Internal function definitions

static void msTaskClassPublishData(void)
{
    ; // Do nothing
}


// EOF
