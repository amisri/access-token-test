//! @file  set_class.c
//! @brief Class specific Set command functions


// ---------- Includes

#include <set_class.h>
#include <diag_class.h>
#include <cmd.h>
#include <defprops.h>
#include <fgc_errs.h>
#include <logSpy_class.h>
#include <pars.h>
#include <status.h>



// ---------- External function definitions

uint16_t SetRefControlValue(struct cmd * c, struct prop * p)
{
    // @TODO Implement if needed for FGC_65

    return FGC_OK_NO_RSP;
}



uint16_t SetRegMode(struct cmd * c, struct prop * p)
{
    // @TODO Implement if needed for FGC_65

    return FGC_OK_NO_RSP;
}



uint16_t SetRef(struct cmd * c, struct prop * p)
{
    // SetRef provides support for S REF FUNC_TYPE,args,... and
    // S REF.FUNC.TYPE(user) FUNC_TYPE

    // @TODO FGC_65 implement if needed

    return FGC_OK_NO_RSP;
}



uint16_t SetResetEpicFaults(struct cmd * c, struct prop * p)
{
    uint16_t errnum;
    uint8_t* value;

    // ParsValueGet requires that the type pointed by `value` matches the property type.
    if (p->type != PT_INT8U)
    {
        return FGC_BAD_PARAMETER;
    }

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum)
    {
        return errnum;
    }

    switch (p->parent->sym_idx)
    {
        // This will:
        // 1. Set the property value
        // 2. Update relevant bit in non-latched global FAULT
        // 3. Update relevant bit in latched global FAULT
        case STP_DEVICE_1: diagClassResetDeviceFaults(0, *value); break;
        case STP_DEVICE_2: diagClassResetDeviceFaults(1, *value); break;
        case STP_DEVICE_3: diagClassResetDeviceFaults(2, *value); break;
        case STP_DEVICE_4: diagClassResetDeviceFaults(3, *value); break;
        case STP_DEVICE_5: diagClassResetDeviceFaults(4, *value); break;
        case STP_DEVICE_6: diagClassResetDeviceFaults(5, *value); break;
        case STP_DEVICE_7: diagClassResetDeviceFaults(6, *value); break;
        case STP_DEVICE_8: diagClassResetDeviceFaults(7, *value); break;

        default:
            return FGC_BAD_PARAMETER;           // In case one defined this function as the set function
                                                // for a property and forgot to add a switch case for it
            break;

    }

    return FGC_OK_NO_RSP;
}



uint16_t SetDevName(struct cmd * c, struct prop * p)
{
    if (p->value == NULL)
    {
        return FGC_NOT_IMPL;
    }

    // Note: the bulk of the work (including buffer size validation) is done in parsScanDevName!
    enum fgc_errno errnum = ParsSet(c, p);

    if (errnum != 0)
    {
        return errnum;
    }

    // Ugly: these are order-sensitive!
    diagClassRefreshConfiguration();
    logSpyClassRefreshConfiguration();

    return 0;
}



// EOF
