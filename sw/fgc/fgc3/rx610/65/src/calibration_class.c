/*!
 *  @file   calibration_class.c
 *  @brief  This file contains functions which support the automatic calibration of the ADCs, DCCTs and DAC. (stubbed for this class)
 */


// Includes

#include "fgc3/rx610/inc/calibration.h"



// External variable definitions

struct Calibration_properties cal;



// External function definitions

void calibrationSetup(enum Calibration_target target, uint32_t chan_mask, int32_t signal)
{
}



void calibrationRun(void)
{
}



bool calibrationIsActive(void)
{
    return false;
}



void calibrationInhibit(uint32_t time)
{
}



void calibrationProcess(void)
{
}


// EOF
