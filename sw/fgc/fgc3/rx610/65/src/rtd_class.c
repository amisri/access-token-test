//! @file   rtd_class.c
//! @brief  Class FGC_65 specific source file


// ---------- Includes

#include <rtd.h>

#include <diag_class.h>

// ---------- Constants


// ---------- Internal function declarations



// ---------- Internal variable definitions



// ---------- External function definitions

void rtdClassInit(void)
{
    ; // Do nothing
}



void rtdClassReset(FILE * f)
{
    // Move to RTD display area

    fputs(TERM_CSI "21;1" TERM_GOTO, f);

    //    0         10        20        30        40        50        60        70        80
    //    |---------+---------+---------+---------+---------+---------+---------+---------+|

    fputs("States: __.__.__.__                                                             \n\r", f);
    fputs("                                                                                \n\r", f);
    fputs("CFG:                                                                CPU:___%___%\n\r", f);
    fputs("PERMIT:                                                              __._C __._C", f);

    // Set up scroll window to protect RTD lines

    fputs(TERM_HOME "\33[1;19r\v", f);

}



bool rtdClassProcess(bool reset)
{
    return false;
}



void rtdClassUpdateErrors(void)
{
    ; // Do nothing
}



// ---------- Internal function definitions



// EOF
