//! @file  events_class.c
//! @brief File with the class-specific processing of events


// ---------- Includes

#include <stdint.h>

#include <diag_class.h>
#include <events.h>
#include <fbs_class.h>
#include <function_class.h>
#include <cycTime.h>
#include <dpcls.h>
#include <dpcom.h>
#include <pc_state.h>
#include <bitmap.h>
#include <modePc.h>
#include <time_fgc.h>
#include <transaction.h>



// ---------- Internal function definitions

static void eventsClassProcessRef(struct CC_us_time  const * start,
                                  struct Events_data const   data)
{
    // Calculate the delay based on the start time and the current time

    struct CC_us_time now = { { timeGetUtcTime() }, timeGetUtcTimeMs() * 1000 };
    int32_t delay_us = timeUsDiff(&now, start);

    if (delay_us < 0)
    {
        return; // @TODO output an error
    }

    diagSchedulePublication(start, data.payload.cycle.cyc_sel_acq);
}



static void eventsClassProcessAcquisition(struct CC_us_time  const * start,
                                          struct Events_data const   data)
{
    // Check whether REF.EVENT_CYC has been set. If so, we do not use this event type.

    if (events_prop.ref_event != 0)
    {
        return;
    }

    // Delegate to eventsClassProcessRef which already implements the necessary logic

    eventsClassProcessRef(start, data);
}




// ---------- Platform/class specific function definitions

void eventsClassInit(void)
{
    // These will be used if REF.EVENT_CYC is configured. Otherwise we fall back on the ACQUISITION event.

    eventsRegisterEventFunc(FGC_EVT_START_REF_1, eventsClassProcessRef);
    eventsRegisterEventFunc(FGC_EVT_START_REF_2, eventsClassProcessRef);
    eventsRegisterEventFunc(FGC_EVT_START_REF_3, eventsClassProcessRef);
    eventsRegisterEventFunc(FGC_EVT_START_REF_4, eventsClassProcessRef);
    eventsRegisterEventFunc(FGC_EVT_START_REF_5, eventsClassProcessRef);

    eventsRegisterEventFunc(FGC_EVT_ACQUISITION, eventsClassProcessAcquisition);
}



void eventsClassProcess(void)
{
    ; // Not used
}


// EOF
