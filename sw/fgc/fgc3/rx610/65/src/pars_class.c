//! @file     pars_class.c
//! @brief    Parsing of class specific property types


// ---------- Includes

#include <stdint.h>

#include <cmd.h>
#include <pars.h>
#include <prop.h>
#include <fgc_errs.h>



// ---------- Internal function definitions

static uint16_t parsScanDevName(struct cmd * c, struct prop * p, char ** value)
{
    // Use the pars_buf results area for the value

    char     * result = (char *)&c->pars_buf->results;
    char     * token;
    char     * head;
    uint16_t   errnum;

    *value = result;
    head   = result;

    errnum = ParsScanString(c, &token, "Y,");

    // Mask 'no symbol' error coming from the tokeniser

    if (errnum == FGC_NO_SYMBOL && testBitmap(p->flags, PF_SYM_LST))
    {
        return FGC_UNKNOWN_DEV;
    }

    // Mask 'buffer full' error coming from the tokeniser

    if (errnum == FGC_SYNTAX_ERROR)
    {
        return FGC_DEV_NAME_TOO_LONG;
    }

    if (errnum != 0)
    {
        return errnum;
    }

    if (strlen(token) > FGC_MAX_DEV_LEN)
    {
        return FGC_DEV_NAME_TOO_LONG;
    }

    strcpy(head, token);

    // The device (EPIC) will not be in NameDB, don't bother trying to validate it
    // To specify "no device", the special value "NONE" must be used

    return 0;
}



// ---------- External function definitions

uint16_t parsClassValueGet(struct cmd * c, struct prop * p, void ** value)
{
    switch (p->type)
    {
        case PT_DEV_NAME:
            return parsScanDevName(c, p, (char **)value);
    }

    return FGC_UNKNOWN_ERROR_CODE;
}


// EOF
