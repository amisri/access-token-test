//! @file     logSpy_class.c
//! @brief    Class specific logging related functions


// ---------- Includes

#include <stdbool.h>

#include <fgc_errs.h>
#include <logSpy.h>

#include <defprops_inc_class.h>
#include <diag_class.h>
#include <logSpy_class.h>



// ---------- Constants

enum { LOGSPY_MENU_NAME_MAX_LEN             = 32 };                             // chosen arbitrarily (but tested to work end-to-end)
enum { LOGSPY_TRUNCATED_DEVICE_NAME_MAX_LEN = LOGSPY_MENU_NAME_MAX_LEN - 7 };   // 7 = strlen(longest log suffix) = strlen("_ENABLE")

enum LogSpy_rename_list_entry_type
{
    LOGSPY_RENAME_TYPE_PC,
    LOGSPY_RENAME_TYPE_FEI
};



// ---------- Internal structures, unions and enumerations

//! Repeated menus are always grouped together. Store the index  of the menu
//! for the first EPIC, and the menus for other EPICs simply follow

struct LogSpy_menu_per_device
{
    int32_t         menu_index;
    char    const * suffix;
};


struct LogSpy_rename_list_entry
{
    enum LogSpy_rename_list_entry_type  type;
    enum LOG_menu_index                 menu_base;
    char                        const * suffix_a;
    char                        const * suffix_b;
    char                        const * template_a;
    char                        const * template_b;
    uint32_t                            signal_a;
    uint32_t                            signal_b;
    uint32_t                            offset;
    uint32_t                            count;
};



// ---------- Internal variable definitions

static struct LogSpy_menu_per_device const logSpy_menu_per_device[] =
{
    { LOG_MENU_EPIC_ENABLE,  "_ENABLE"  },
    { LOG_MENU_EPIC_WARN,    "_WARN"    },
    { LOG_MENU_EPIC_STAT_W1, "_STAT_W1" },
    { LOG_MENU_EPIC_STAT_W2, "_STAT_W2" },
};

enum { LOGSPY_NUM_LOG_MENUS_PER_DEVICE = ArrayLen(logSpy_menu_per_device) };


static const struct LogSpy_rename_list_entry rename_list[] =
{
    {
        LOGSPY_RENAME_TYPE_PC,
        LOG_MENU_EPIC_ENABLE,
        "_EN_A", "_EN_B",
        "ENABLE_PC%d_A_IN",
        "ENABLE_PC%d_B_IN",
        LOG_EPIC_EN_WN_ENABLE_PC1_A_IN,
        LOG_EPIC_EN_WN_ENABLE_PC1_B_IN,
        0,
        FGC_NUM_PC_IN_PER_EPIC
    },
    {
        LOGSPY_RENAME_TYPE_PC,
        LOG_MENU_EPIC_STAT_W1,
        "_W1_A",
        "_W1_B",
        "STATUS_PC%d_W1_A_IN",
        "STATUS_PC%d_W1_B_IN",
        LOG_EPIC_STAT_W1_STATUS_PC1_W1_A_IN,
        LOG_EPIC_STAT_W1_STATUS_PC1_W1_B_IN,
        0,
        FGC_NUM_PC_IN_PER_EPIC
    },
    {
        LOGSPY_RENAME_TYPE_PC,
        LOG_MENU_EPIC_STAT_W2,
        "_W2_A", "_W2_B",
        "STATUS_PC%d_W2_A_IN",
        "STATUS_PC%d_W2_B_IN",
        LOG_EPIC_STAT_W2_STATUS_PC1_W2_A_IN,
        LOG_EPIC_STAT_W2_STATUS_PC1_W2_B_IN,
        0,
        FGC_NUM_PC_IN_PER_EPIC
    },
    {
        LOGSPY_RENAME_TYPE_FEI,
        LOG_MENU_EPIC_ENABLE,
        "_EN_A", "_EN_B",
        "ENABLE_FEI%d_A_IN",
        "ENABLE_FEI%d_B_IN",
        LOG_EPIC_EN_WN_ENABLE_FEI1_A_IN,
        LOG_EPIC_EN_WN_ENABLE_FEI1_B_IN,
        0,
        FGC_NUM_FEI_IN_PER_EPIC
    },
    {
        LOGSPY_RENAME_TYPE_FEI,
        LOG_MENU_EPIC_STAT_W1,
        "_W1_A",
        "_W1_B",
        "STATUS_FEI%d_W1_A_IN",
        "STATUS_FEI%d_W1_B_IN",
        LOG_EPIC_STAT_W1_STATUS_FEI1_W1_A_IN,
        LOG_EPIC_STAT_W1_STATUS_FEI1_W1_B_IN,
        0,
        1
    },
    {
        LOGSPY_RENAME_TYPE_FEI,
        LOG_MENU_EPIC_STAT_W2,
        "_W2_A",
        "_W2_B",
        "STATUS_FEI%d_W2_A_IN",
        "STATUS_FEI%d_W2_A_IN",
        LOG_EPIC_STAT_W2_STATUS_FEI2_W2_A_IN,
        LOG_EPIC_STAT_W2_STATUS_FEI2_W2_B_IN,
        1,
        1
    },
};


static char logSpy_menu_name_buffers[FGC_MAX_EPICS][LOGSPY_NUM_LOG_MENUS_PER_DEVICE][LOGSPY_MENU_NAME_MAX_LEN + 1];



// ---------- Internal function definitions

static void logSpyClassInterceptEpicLogNames(union LOG_header         * const  log_header,
                                             enum LOG_menu_index        const   menu_index,
                                             enum LogSpy_rename_list_entry_type type,
                                             enum LOG_menu_index        const   menu_base,
                                             char                       const * suffix_a,
                                             char                       const * suffix_b,
                                             char                       const * template_a,
                                             char                       const * template_b,
                                             int32_t                            signal_a,
                                             int32_t                            signal_b,
                                             int32_t                            offset,
                                             int32_t                            count)
{
    // Check if this is a menu we care about

    if (menu_index >= menu_base && menu_index < menu_base + FGC_MAX_EPICS)
    {
        uint32_t const device_index = (menu_index - menu_base);

        // Iterate over the number of applicable converters

        for (uint32_t index = 0; index < count; index++)
        {
            char const * input_name = (type == LOGSPY_RENAME_TYPE_PC) ? epic_config.devices[device_index].pc_in_names [offset + index]
                                                                      : epic_config.devices[device_index].fei_in_names[offset + index];
            char name_buf_a[SPY_SIG_NAME_LEN];
            char name_buf_b[SPY_SIG_NAME_LEN];

            // Check if converter name is set

            size_t strlen_input_name = strlen(input_name);
            bool is_meaningful = !(strlen_input_name == 4 && strcmp(input_name, "NONE") == 0);

            if (diagClassIsDeviceConfigured(device_index) && strlen_input_name > 0 && is_meaningful == true)
            {
                // Copy converter name if it fits, or a truncated version

                size_t len_to_copy = MIN(strlen_input_name, SPY_SIG_NAME_LEN - 1 - strlen(suffix_a));
                memcpy(name_buf_a, input_name, len_to_copy);
                strcpy(name_buf_a + len_to_copy, suffix_a);

                len_to_copy = MIN(strlen_input_name, SPY_SIG_NAME_LEN - 1 - strlen(suffix_b));
                memcpy(name_buf_b, input_name, len_to_copy);
                strcpy(name_buf_b + len_to_copy, suffix_b);
            }
            else
            {
                snprintf(name_buf_a, SPY_SIG_NAME_LEN, template_a, 1 + offset + index);
                snprintf(name_buf_b, SPY_SIG_NAME_LEN, template_b, 1 + offset + index);
            }

            // Apply signal name change

            logChangeSpySignalNameSwap(&log_header->spy, signal_a + index, name_buf_a);
            logChangeSpySignalNameSwap(&log_header->spy, signal_b + index, name_buf_b);
        }
    }
}



// ---------- Platform/class specific function definitions

void logSpyClassInit(void)
{
    ; // Do nothing
}



void logSpyClassEnableDisableLogs(void)
{
    struct LOG_mgr * log_mgr = (struct LOG_mgr *)&dpcom.log.log_mgr;

    // Iterate all interlock concentrators, enable/disable menus correspondingly
    //
    // Reminder: 'logs' are the backing structure, while 'menus' provide more fine-grained views into the logs

    int32_t device_index;

    for (device_index = 0; device_index < FGC_MAX_EPICS; device_index++)
    {
        if (diagClassIsDeviceConfigured(device_index) == true)
        {
            logEnable(log_mgr, LOG_EPIC_EN_WN   + device_index);
            logEnable(log_mgr, LOG_EPIC_STAT_W1 + device_index);
            logEnable(log_mgr, LOG_EPIC_STAT_W2 + device_index);

            logMenuEnable(&log_menus, LOG_MENU_EPIC_ENABLE  + device_index);
            logMenuEnable(&log_menus, LOG_MENU_EPIC_WARN    + device_index);
            logMenuEnable(&log_menus, LOG_MENU_EPIC_STAT_W1 + device_index);
            logMenuEnable(&log_menus, LOG_MENU_EPIC_STAT_W2 + device_index);
        }
        else
        {
            logDisable(log_mgr, LOG_EPIC_EN_WN   + device_index);
            logDisable(log_mgr, LOG_EPIC_STAT_W1 + device_index);
            logDisable(log_mgr, LOG_EPIC_STAT_W2 + device_index);

            logMenuDisable(&log_menus, LOG_MENU_EPIC_ENABLE  + device_index);
            logMenuDisable(&log_menus, LOG_MENU_EPIC_WARN    + device_index);
            logMenuDisable(&log_menus, LOG_MENU_EPIC_STAT_W1 + device_index);
            logMenuDisable(&log_menus, LOG_MENU_EPIC_STAT_W2 + device_index);
        }
    }
}



uint16_t logSpyClassProcessAlias(uint32_t const menu_idx)
{
	// Do nothing

    return FGC_OK_NO_RSP;
}



bool logSpyClassIsActiveAlias(uint32_t const menu_idx)
{
	// Do nothing

    return false;
}



void logSpyClassInterceptLogNames(union LOG_header * const log_header, enum LOG_menu_index const menu_index)
{
    for (size_t i = 0; i < ArrayLen(rename_list); i++)
    {
        logSpyClassInterceptEpicLogNames(log_header,
                                         menu_index,
                                         rename_list[i].type,
                                         rename_list[i].menu_base,
                                         rename_list[i].suffix_a,
                                         rename_list[i].suffix_b,
                                         rename_list[i].template_a,
                                         rename_list[i].template_b,
                                         rename_list[i].signal_a,
                                         rename_list[i].signal_b,
                                         rename_list[i].offset,
                                         rename_list[i].count);
    }
}



// ---------- External function definitions

void logSpyClassRefreshConfiguration(void)
{
    int32_t device_index;

    // Iterate devices & generate appropriate log menu names

    for (device_index = 0; device_index < FGC_MAX_EPICS; device_index++)
    {
        char         truncated_device_name[LOGSPY_TRUNCATED_DEVICE_NAME_MAX_LEN + 1];
        char const * configured_device_name = diagClassGetDeviceName(device_index);

        if (configured_device_name != NULL)
        {
            // Device name is usable. Truncate it to the permitted length (usually it should fit)

            snprintf(truncated_device_name, sizeof(truncated_device_name), "%s", configured_device_name);
        }
        else
        {
            // Generate a placeholder name (EPIC_1, EPIC_2, ...)

            snprintf(truncated_device_name, sizeof(truncated_device_name), "EPIC_%d", 1 + (int)device_index);
        }

        // Now generate and attach the log menu names

        for (int32_t i = 0; i < LOGSPY_NUM_LOG_MENUS_PER_DEVICE; i++)
        {
            // Generate log menu name like EPIC_1_STAT_W1 etc

            snprintf(logSpy_menu_name_buffers[device_index][i],
                     sizeof(logSpy_menu_name_buffers[device_index][i]),
                     "%s%s", truncated_device_name, logSpy_menu_per_device[i].suffix);

            logChangeMenuName(&log_menus,
                              logSpy_menu_per_device[i].menu_index + device_index,
                              logSpy_menu_name_buffers[device_index][i]);
        }
    }
}


// EOF
