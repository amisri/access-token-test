//! @file   statePc.c
//! @brief  Manage the Power Converter state and the Reference state


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <defconst.h>

#include <statePc_class.h>
#include <fbs_class.h>
#include <modePc.h>
#include <statePc.h>
#include <dpcls.h>



// ---------- Platform/class specific function definitions

void statePcClassProcess(void)
{
    ;  // Do nothing
}



// ----------------------------- CLASS SPECIFIC STATE FUNCTIONS ------------------------------

// STATE: DIRECT (DT)

uint32_t statePcDT(bool firstCall)
{
    return FGC_PC_DIRECT;
}



// ------------------------ CLASS SPECIFIC TRANSITION CONDITION FUNCTIONS -------------------------

static StatePC * BKtoDT(void)
{
    if (modePcGetInternal() == FGC_PC_DIRECT && STATE_VS == FGC_VS_READY)
    {
        return statePcDT;
    }

    return NULL;
}



static StatePC * DTtoSA(void)
{
    if (modePcGetInternal() == FGC_PC_OFF)
    {
        return statePcSA;
    }

    return NULL;
}



static StatePC * SAtoSP(void)
{
    if (modePcGetInternal() == FGC_PC_OFF)
    {
        return statePcSP;
    }

    return NULL;
}



// ---------- External variable definitions

//! State transition functions : called in priority order, from left to right, return the state function.
//!
//! The initialization order of StateTransitions must match the STATE.PC constant values from the XML symlist.
//! This is checked with static assertions.

StatePCtrans * StateTransitions[][STATE_PC_REF_MAX_TRANSITIONS + 1] =
{
    /*  0.FO */ {         FOtoOF,         NULL },
    /*  1.OF */ {         OFtoFO, OFtoST, NULL },
    /*  2.FS */ {         FStoFO, FStoSP, NULL },
    /*  3.SP */ { XXtoFS, SPtoOF,         NULL },
    /*  4.ST */ { XXtoFS, STtoSP, STtoBK, NULL },
    /*  5.SA */ { XXtoFS, SAtoSP,         NULL },
    /*  6.TS */ {                         NULL },
    /*  7.SB */ {                         NULL },
    /*  8.IL */ {                         NULL },
    /*  9.TC */ {                         NULL },
    /* 10.AR */ {                         NULL },
    /* 11.RN */ {                         NULL },
    /* 12.AB */ {                         NULL },
    /* 13.CY */ {                         NULL },
    /* 14.PL */ {                         NULL },
    /* 15.BK */ { XXtoFS, BKtoSP, BKtoDT, NULL },
    /* 16.EC */ {                         NULL },
    /* 17.DT */ { XXtoFS,         DTtoSA, NULL },
    /* 18.PA */ {                         NULL }
};


// EOF
