//! @file  fbs_class.h
//! @brief Handlers to published data variables

#pragma once


// ---------- Includes

#include <fbs.h>



// ---------- External structures, unions and enumerations

#define FAULTS         fbs.u.fgc_stat.class_data.c64.st_faults
#define WARNINGS       fbs.u.fgc_stat.class_data.c64.st_warnings
#define ST_LATCHED     fbs.u.fgc_stat.class_data.c64.st_latched
#define ST_UNLATCHED   fbs.u.fgc_stat.class_data.c64.st_unlatched
#define STATE_PLL      fbs.u.fgc_stat.class_data.c64.state_pll
#define STATE_OP       fbs.u.fgc_stat.class_data.c64.state_op
#define STATE_VS       fbs.u.fgc_stat.class_data.c64.state_vs
#define STATE_PC       fbs.u.fgc_stat.class_data.c64.state_pc
#define REG_MODE       fbs.u.fgc_stat.class_data.c64.reg_mode
#define Q_REF          fbs.u.fgc_stat.class_data.c64.q_ref
#define V_REF          fbs.u.fgc_stat.class_data.c64.v_ref
#define Q_FEEDER       fbs.u.fgc_stat.class_data.c64.q_feeder
#define V_RS           fbs.u.fgc_stat.class_data.c64.v_rs
#define FREQ           fbs.u.fgc_stat.class_data.c64.freq
#define ALPHA          fbs.u.fgc_stat.class_data.c64.alpha


// EOF
