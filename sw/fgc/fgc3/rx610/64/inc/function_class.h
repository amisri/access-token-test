//! @file  function_class.h
//! @brief Class specific handling of function arming and ancillary functions

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External structures, unions and enumerations


//! Reference limits

struct FunctionClass_limits_pos_min
{
        float                             max;                    //!< Maximum value
        float                             min;                    //!< Minimum value
};


//! Structure holding all the PPM variables

struct FunctionClass_reference
{
    struct FunctionClass_reference_func
    {
        uint32_t                          reg_mode;               //!< REF.FUNC.REG_MODE
    } func;

    struct FunctionClass_reference_direct
    {
        float                             a_ref;                  //!< REF.DIRECT.A.VALUE
        float                             q_ref;                  //!< REF.DIRECT.Q.VALUE
        float                             v_ref;                  //!< REF.DIRECT.V.VALUE
        float                             pf_rate;                //!< REF.DIRECT.PF.RATE
        uint32_t                          pf_sign;                //!< REF.DIRECT.PF.SIGN
    } direct;

    struct FunctionClass_reference_limits
    {
        struct FunctionClass_limits_pos_min   v;                  //!< LIMITS.V.{POS,MIN}
        struct FunctionClass_limits_pos_min   q;                  //!< LIMITS.Q.{POS,MIN}
        struct FunctionClass_limits_pos_min   a;                  //!< LIMITS.A.{POS,MIN}
        struct FunctionClass_limits_pos_min   pf;                 //!< LIMITS.PF.{POS,MIN}
    } limits;
};



// ---------- External variable declarations

extern struct FunctionClass_reference reference;



// ---------- External variable declarations

//! Set the reference value
//!
//! @return   FGC error

uint16_t functionClassSetReference(void);


//! Set the regulation mdoe
//!
//! @return   FGC error

uint16_t functionClassSetRegMode(void);


//! Restores properties mapped to parameters

void functionClassRestore(void);


//! Backs-up properties mapped to parameters

void functionClassBackup(void);


// EOF
