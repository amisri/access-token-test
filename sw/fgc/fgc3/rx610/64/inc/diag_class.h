//! @file  diag_class.c
//! @brief Class specific functionality for diagnostics

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>


// ---------- External structures, unions and enumerations


//! DIM analogue signal

struct Diag_class_meas_svc_dim_analogue
{
    int32_t                          channel;      //!< DIM channel with the signal
    float                            value;        //!< Sample value for the signal
};


//! DIM digital signal

struct Diag_class_meas_svc_dim_digital
{
    int32_t                          channel;      //!< DIM channel with the signal
    uint16_t                         mask;         //!< Mask of the signal withing the digital bank
    bool                             value;        //!< Digital value
};


//! Three-phase components

struct Diag_class_meas_svc_feeder_load
{
    struct Diag_class_meas_svc_dim_analogue       sum;
    struct Diag_class_meas_svc_dim_analogue       r;
    struct Diag_class_meas_svc_dim_analogue       s;
    struct Diag_class_meas_svc_dim_analogue       t;
};


//! Three-phase TCR line components

struct Diag_class_meas_svc_tcr
{
    struct Diag_class_meas_svc_dim_analogue         sum;
    struct Diag_class_meas_svc_dim_analogue         rs;
    struct Diag_class_meas_svc_dim_analogue         st;
    struct Diag_class_meas_svc_dim_analogue         tr;
};

//! SVC DIM measurements

struct Diag_class_meas_svc
{
    struct Diag_class_meas_svc_dim_digital        pll_ok;

    struct Diag_class_meas_svc_p
    {
        struct Diag_class_meas_svc_feeder_load    feeder;
        struct Diag_class_meas_svc_feeder_load    load;
    } p;

    struct Diag_class_meas_svc_q
    {
        struct Diag_class_meas_svc_feeder_load    feeder;
        struct Diag_class_meas_svc_feeder_load    load;
        struct Diag_class_meas_svc_tcr            tcr;
    } q;

    struct Diag_class_meas_svc_v
    {
        struct Diag_class_meas_svc_dim_analogue     rs;
        struct Diag_class_meas_svc_dim_analogue     st;
        struct Diag_class_meas_svc_dim_analogue     tr;

        struct Diag_class_meas_svc_dim_analogue     r;
        struct Diag_class_meas_svc_dim_analogue     s;
        struct Diag_class_meas_svc_dim_analogue     t;
    } v;

    struct Diag_class_meas_svc_i
    {
        struct Diag_class_meas_svc_feeder_load    feeder;
        struct Diag_class_meas_svc_feeder_load    load;
        struct Diag_class_meas_svc_tcr            tcr;
    } i;

    struct Diag_class_meas_svc_reg
    {
        struct Diag_class_meas_svc_dim_analogue     alpha;
        struct Diag_class_meas_svc_dim_analogue     freq;
        struct Diag_class_meas_svc_dim_analogue     pll_ok;
        struct Diag_class_meas_svc_dim_analogue     pf;
        struct Diag_class_meas_svc_dim_analogue     piflag_v;
        struct Diag_class_meas_svc_dim_analogue     piflag_q;
    } reg;
};



// ---------- External variable declarations

extern struct Diag_class_meas_svc   meas_svc;


// EOF
