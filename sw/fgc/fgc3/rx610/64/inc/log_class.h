//! @file  log_class.h
//! @brief Class specific logging related functions

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External function declarations

//! Sets the acquisition group on the DSP card via the SCIVS bus
//!
//! @param[im]   group        Acquisition group
//!
//! @return                   FGC error

uint16_t logClassSetMeasAcqGroup(uint32_t group);


// EOF

