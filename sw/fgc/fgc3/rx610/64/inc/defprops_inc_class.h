//! @file  defprops_inc_class.h
//! @brief Adds all the class-specific headers needed by defprops.h

#pragma once


// ---------- Includes

#include <function_class.h>


// EOF
