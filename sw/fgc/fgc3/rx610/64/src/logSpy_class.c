//! @file     logSpy_class.c
//! @brief    Class specific logging related functions


// ---------- Includes

#include <string.h>

#include <fgc_errs.h>

#include <dpcls.h>
#include <logSpy.h>
#include <log_class.h>
#include <regfgc3Pars.h>



// ---------- Platform/class specific function definitions

void logSpyClassInit(void)
{
    // Initialize the MEAS log menu names

    static char const * menulNames[FGC_MAX_MEAS_ACQ_GROUP] =
    {
        "SPI_MEASUR"  , "SPI_FEEDER", "SPI_TCR" , "SPI_SYNC"   , "SPI_POWER",
        "SPI_RESISTOR", "SPI_REG"   , "SPI_LOAD", "SPI_REG_REF",
    };

    uint8_t menu_idx;

    for (menu_idx = 0; menu_idx < FGC_MAX_MEAS_ACQ_GROUP; menu_idx++)
    {
        logChangeMenuName(&log_menus, LOG_MENU_MEAS + menu_idx, menulNames[menu_idx]);
    }

}



void logSpyClassEnableDisableLogs(void)
{
    ; // Do nothing
}



uint16_t logSpyClassProcessAlias(uint32_t const menu_idx)
{
    // Mapping between regulation mode and SCVIS parameters:
    //   Property            Slot  Block  Parameter
    //   MEAS.ACQ_GROUP      6     2      4

    int32_t  data[SCIVS_N_PARAMS_PER_BLOCK];
    uint16_t retval;

    if ((retval = regFgc3ParsGetBlockFromHw(6, SCIVS_DEVICE_2, 2, data)) != FGC_OK_NO_RSP)
    {
        return retval;
    }

    data[4] = menu_idx - LOG_MENU_MEAS;

    if ((retval = regFgc3ParsSendBlockToHw(6, SCIVS_DEVICE_2, 2, data)) != FGC_OK_NO_RSP)
    {
        return retval;
    }

    // Update MEAS.DSP_GROUP

    dpcls.mcu.meas.acq_group = data[4];

    return retval;
}



bool logSpyClassIsActiveAlias(uint32_t const menu_idx)
{
    return (menu_idx == (dpcls.mcu.meas.acq_group + LOG_MENU_MEAS));
}



void logSpyClassInterceptLogNames(union LOG_header * const log_header, enum LOG_menu_index const menu_index)
{
    // The only log menu to intercept is DIM, and this is already handled by logInterceptLogNames.
}


// EOF
