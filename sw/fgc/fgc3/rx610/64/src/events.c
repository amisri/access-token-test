//! @file  events.c
//! @brief Overwrite generic functionality. This class does not handle events

#define EVENTS_GLOBALS
#define FGC_EVENT_GLOBALS


// ---------- Includes

#include <fgc_event.h>
#include <os.h>


// ---------- External function definitions

void eventsInit(void)
{
    ; // Do nothing
}



void eventsProcess(void)
{
    ; // Do nothing
}


void eventsTask(void * unused)
{
    for (;;)
    {
        OSTskSuspend();
    
         // Do nothing
    }
}


// EOF
