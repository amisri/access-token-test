//! @file  diag_class.c
//! @brief Class specific functionality for diagnostics


// ---------- Includes

#include <stdint.h>
#include <string.h>

#include <diag_class.h>
#include <bitmap.h>
#include <class.h>
#include <diag.h>
#include <digitalIo.h>
#include <fbs_class.h>
#include <function_class.h>
#include <logSpy.h>
#include <memmap_mcu.h>
#include <sta.h>
#include <stateOp.h>
#include <status.h>



// ---------- External variable definitions

struct Diag_class_meas_svc   meas_svc;



// ---------- External function definitions

void diagClassInit(void)
{
    // Assume the four channels below exist. If not, channel[] will contain
    // -1, which will default in a I earth value of 0.0.

    meas_i_earth.channel[0] = DiagFindAnaChan("I_EARTH_1MS");
    meas_i_earth.channel[1] = DiagFindAnaChan("I_EARTH_6MS");
    meas_i_earth.channel[2] = DiagFindAnaChan("I_EARTH_11MS");
    meas_i_earth.channel[3] = DiagFindAnaChan("I_EARTH_16MS");

    // Zero the structure with DIM information and values
    // @TODO meas_svc and meas_i_earth must be merged into a single strucutre

    memset(&meas_svc, 0, sizeof(meas_svc));

    // Find all the analogue signals in the DSP card

    meas_svc.p.feeder.sum.channel = DiagFindAnaChan("P_FEEDER_SUM");
    meas_svc.p.feeder.r.channel   = DiagFindAnaChan("P_FEEDER_R"  );
    meas_svc.p.feeder.s.channel   = DiagFindAnaChan("P_FEEDER_S"  );
    meas_svc.p.feeder.t.channel   = DiagFindAnaChan("P_FEEDER_T"  );
    meas_svc.p.load.sum.channel   = DiagFindAnaChan("P_LOAD_SUM"  );
    meas_svc.p.load.r.channel     = DiagFindAnaChan("P_LOAD_R"    );
    meas_svc.p.load.s.channel     = DiagFindAnaChan("P_LOAD_S"    );
    meas_svc.p.load.t.channel     = DiagFindAnaChan("P_LOAD_T"    );
    meas_svc.q.feeder.sum.channel = DiagFindAnaChan("Q_FEEDER_SUM");
    meas_svc.q.feeder.r.channel   = DiagFindAnaChan("Q_FEEDER_R"  );
    meas_svc.q.feeder.s.channel   = DiagFindAnaChan("Q_FEEDER_S"  );
    meas_svc.q.feeder.t.channel   = DiagFindAnaChan("Q_FEEDER_T"  );
    meas_svc.q.load.sum.channel   = DiagFindAnaChan("Q_LOAD_SUM"  );
    meas_svc.q.load.r.channel     = DiagFindAnaChan("Q_LOAD_R"    );
    meas_svc.q.load.s.channel     = DiagFindAnaChan("Q_LOAD_S"    );
    meas_svc.q.load.t.channel     = DiagFindAnaChan("Q_LOAD_T"    );
    meas_svc.q.tcr.sum.channel    = DiagFindAnaChan("Q_TCR_SUM"   );
    meas_svc.q.tcr.rs.channel     = DiagFindAnaChan("Q_TCR_RS"    );
    meas_svc.q.tcr.st.channel     = DiagFindAnaChan("Q_TCR_ST"    );
    meas_svc.q.tcr.tr.channel     = DiagFindAnaChan("Q_TCR_TR"    );
    meas_svc.v.rs.channel         = DiagFindAnaChan("V_RS"        );
    meas_svc.v.st.channel         = DiagFindAnaChan("V_ST"        );
    meas_svc.v.tr.channel         = DiagFindAnaChan("V_TR"        );
    meas_svc.v.r.channel          = DiagFindAnaChan("V_R"         );
    meas_svc.v.s.channel          = DiagFindAnaChan("V_S"         );
    meas_svc.v.t.channel          = DiagFindAnaChan("V_T"         );
    meas_svc.i.feeder.r.channel   = DiagFindAnaChan("I_FEEDER_R"  );
    meas_svc.i.feeder.s.channel   = DiagFindAnaChan("I_FEEDER_S"  );
    meas_svc.i.feeder.t.channel   = DiagFindAnaChan("I_FEEDER_T"  );
    meas_svc.i.load.r.channel     = DiagFindAnaChan("I_LOAD_R"    );
    meas_svc.i.load.s.channel     = DiagFindAnaChan("I_LOAD_S"    );
    meas_svc.i.load.t.channel     = DiagFindAnaChan("I_LOAD_T"    );
    meas_svc.i.tcr.rs.channel     = DiagFindAnaChan("I_TCR_RS"    );
    meas_svc.i.tcr.st.channel     = DiagFindAnaChan("I_TCR_ST"    );
    meas_svc.i.tcr.tr.channel     = DiagFindAnaChan("I_TCR_TR"    );
    meas_svc.reg.alpha.channel    = DiagFindAnaChan("REG_ALPHA"   );
    meas_svc.reg.freq.channel     = DiagFindAnaChan("REG_FREQ"    );
    meas_svc.reg.pf.channel       = DiagFindAnaChan("REG_PF"      );
    meas_svc.reg.piflag_v.channel = DiagFindAnaChan("REG_PIFLAG_V");
    meas_svc.reg.piflag_q.channel = DiagFindAnaChan("REG_PIFLAG_Q");

    // Find digital signals

    meas_svc.pll_ok.channel = -1;

    uint16_t logical_dim;

    for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        uint16_t channel;

        if (DiagFindDigChan(logical_dim, "PLL_OK", &channel, &meas_svc.pll_ok.mask) == true)
        {
            meas_svc.pll_ok.channel = channel;
            break;
        }
    }

}



void diagClassCheckConverter(void)
{
    // This function is called from stateTask() every 5ms.

    static uint16_t dim_trig_counter;

    if (   stateOpGetState() != FGC_OP_SIMULATION
        && ((uint16_t)sta.timestamp_ms.ms % 20 == (15 + DEV_STA_TSK_PHASE)))
    {
        if (   dim_collection.unlatched_trigger_fired                  == true
            && testBitmap(digitalIoGetInputs(), DIG_IP_VSRUN_MASK32)  == true
            && statusTestFaults (FGC_FLT_VS_FAULT)                    == false
            && statusTestFaults (FGC_FLT_VS_EXTINTLOCK)               == false
            && statusTestFaults (FGC_FLT_FAST_ABORT)                  == false
            && statusTestLatched(FGC_LAT_DIM_TRIG_FLT)                == false)
        {
            if (++dim_trig_counter > 5)
            {
                statusSetLatched(FGC_LAT_DIM_TRIG_FLT);
            }
        }
        else
        {
            dim_trig_counter = 0;
        }
    }
}



void diagClassUpdateMeas(void)
{
    meas_svc.p.feeder.sum.value = ((float *)&dim_collection.analogue_in_physical)[meas_svc.p.feeder.sum.channel];
    meas_svc.p.feeder.r.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.p.feeder.r.channel  ];
    meas_svc.p.feeder.s.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.p.feeder.s.channel  ];
    meas_svc.p.feeder.t.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.p.feeder.t.channel  ];
    meas_svc.p.load.sum.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.p.load.sum.channel  ];
    meas_svc.p.load.r.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.p.load.r.channel    ];
    meas_svc.p.load.s.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.p.load.s.channel    ];
    meas_svc.p.load.t.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.p.load.t.channel    ];
    meas_svc.q.feeder.sum.value = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.feeder.sum.channel];
    meas_svc.q.feeder.r.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.feeder.r.channel  ];
    meas_svc.q.feeder.s.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.feeder.s.channel  ];
    meas_svc.q.feeder.t.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.feeder.t.channel  ];
    meas_svc.q.load.sum.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.load.sum.channel  ];
    meas_svc.q.load.r.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.load.r.channel    ];
    meas_svc.q.load.s.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.load.s.channel    ];
    meas_svc.q.load.t.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.load.t.channel    ];
    meas_svc.q.tcr.sum.value    = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.tcr.sum.channel   ];
    meas_svc.q.tcr.rs.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.tcr.rs.channel    ];
    meas_svc.q.tcr.st.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.tcr.st.channel    ];
    meas_svc.q.tcr.tr.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.q.tcr.tr.channel    ];
    meas_svc.v.rs.value         = ((float *)&dim_collection.analogue_in_physical)[meas_svc.v.rs.channel        ];
    meas_svc.v.st.value         = ((float *)&dim_collection.analogue_in_physical)[meas_svc.v.st.channel        ];
    meas_svc.v.tr.value         = ((float *)&dim_collection.analogue_in_physical)[meas_svc.v.tr.channel        ];
    meas_svc.v.r.value          = ((float *)&dim_collection.analogue_in_physical)[meas_svc.v.r.channel         ];
    meas_svc.v.s.value          = ((float *)&dim_collection.analogue_in_physical)[meas_svc.v.s.channel         ];
    meas_svc.v.t.value          = ((float *)&dim_collection.analogue_in_physical)[meas_svc.v.t.channel         ];
    meas_svc.i.feeder.r.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.i.feeder.r.channel  ];
    meas_svc.i.feeder.s.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.i.feeder.s.channel  ];
    meas_svc.i.feeder.t.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.i.feeder.t.channel  ];
    meas_svc.i.load.r.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.i.load.r.channel    ];
    meas_svc.i.load.s.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.i.load.s.channel    ];
    meas_svc.i.load.t.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.i.load.t.channel    ];
    meas_svc.i.tcr.rs.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.i.tcr.rs.channel    ];
    meas_svc.i.tcr.st.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.i.tcr.st.channel    ];
    meas_svc.i.tcr.tr.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.i.tcr.tr.channel    ];
    meas_svc.reg.alpha.value    = ((float *)&dim_collection.analogue_in_physical)[meas_svc.reg.alpha.channel   ];
    meas_svc.reg.freq.value     = ((float *)&dim_collection.analogue_in_physical)[meas_svc.reg.freq.channel    ];
    meas_svc.reg.pll_ok.value   = ((float *)&dim_collection.analogue_in_physical)[meas_svc.reg.pll_ok.channel  ];
    meas_svc.reg.pf.value       = ((float *)&dim_collection.analogue_in_physical)[meas_svc.reg.pf.channel      ];
    meas_svc.reg.piflag_v.value = ((float *)&dim_collection.analogue_in_physical)[meas_svc.reg.piflag_v.channel];
    meas_svc.reg.piflag_q.value = ((float *)&dim_collection.analogue_in_physical)[meas_svc.reg.piflag_q.channel];


    meas_svc.pll_ok.value = (meas_svc.pll_ok.mask != 0 && testBitmap(diag.data.w[meas_svc.pll_ok.channel], meas_svc.pll_ok.mask) == true);

    meas_i_earth.simulated = meas_svc.v.rs.value * meas_i_earth.limit / reference.limits.v.max;
}


// EOF
