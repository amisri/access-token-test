//! @file  function_class.c
//! @brief Class specific handling of function arming and ancillary functions


// ---------- Includes

#include <string.h>

#include <function_class.h>
#include <dpcls.h>
#include <dpcom.h>
#include <pc_state.h>
#include <regfgc3Pars.h>
#include <fgc_errs.h>



// ---------- Internal structures, unions and enumerations

//! Internal global variables for the module bis_intlk

struct Function_class
{
    struct FunctionClass_reference_func      func;
    struct FunctionClass_reference_direct    direct;
};


// ---------- Internal variable definitions

static struct Function_class function_class;



// ---------- External variable definitions

struct FunctionClass_reference reference;



// ---------- Platform/class specific function definitions

char const * functionGetDefaultPrefix(void)
{
    static char       prefixDirect[]  = "!S REF.*CV.VALUE ";
    static char const prefixRegMode[] = "QAV";

    if (pcStateGet() == FGC_PC_DIRECT)
    {
        prefixDirect[7] = prefixRegMode[dpcls.mcu.ref.reg_mode];

        return (prefixDirect);
    }

    return ("");
}



// ---------- External function definitions

void functionClassBackup(void)
{
    function_class.func   = reference.func;
    function_class.direct = reference.direct;
}



void functionClassRestore(void)
{
    reference.func   = function_class.func;
    reference.direct = function_class.direct;
}



uint16_t functionClassSetReference(void)
{
    // Mapping between reference and SCVIS parameters:
    //   Property          Slot  Block  Parameter
    //   REF.DIRECT.V        6     2      0
    //   REF.DIRECT.Q        6     2      1
    //   REF.DIRECT.A        6     2      2
    //   REF.DIRECT.PF.RATE  6     2      5
    //   REF.DIRECT.PF.SIGN  6     2      6

    int32_t  data[SCIVS_N_PARAMS_PER_BLOCK];
    uint16_t retval;

    if ((retval = regFgc3ParsGetBlockFromHw(6, SCIVS_DEVICE_2, 2, data)) != FGC_OK_NO_RSP)
    {
        return retval;
    }

    union
    {
        int32_t value_int;
        float   value_float;
    } type_conv;

    type_conv.value_float = reference.direct.v_ref;
    data[0] = type_conv.value_int;

    type_conv.value_float = reference.direct.q_ref;
    data[1] = type_conv.value_int;

    type_conv.value_float = reference.direct.a_ref;
    data[2] = type_conv.value_int;

    type_conv.value_float = reference.direct.pf_rate;
    data[5] = type_conv.value_int;

    data[6] = reference.direct.pf_sign;

    return regFgc3ParsSendBlockToHw(6, SCIVS_DEVICE_2, 2, data);
}



uint16_t functionClassSetRegMode(void)
{
    // Mapping between regulation mode and SCVIS parameters:
    //   Property            Slot  Block  Parameter
    //   REF.FUNC.REG_MODE   6     2      3 { 0-> V, 1 -> Q, 2 -> A, 3 -> PF }

    int32_t  data[SCIVS_N_PARAMS_PER_BLOCK];
    uint16_t retval;

    if ((retval = regFgc3ParsGetBlockFromHw(6, SCIVS_DEVICE_2, 2, data)) != FGC_OK_NO_RSP)
    {
        return retval;
    }

    data[3] = reference.func.reg_mode;

    return regFgc3ParsSendBlockToHw(6, SCIVS_DEVICE_2, 2, data);
}


// EOF
