//! @file  transaction.c
//! @brief Provides the framework for transactional settings


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <fgc_errs.h>

#include <property.h>



// ---------- External function definitions


uint16_t transactionSetId(uint8_t const sub_sel, uint8_t const cyc_sel, uint16_t const id)
{
    // Not supported;

    return FGC_OK_NO_RSP;
}


uint16_t transactionGetId(uint8_t const sub_sel, uint8_t const cyc_sel)
{
    // Not supported;

    return 0;
}



uint16_t transactionValidateId(uint32_t const transaction_id)
{
    // Not supported;

    return FGC_OK_NO_RSP;
}



uint32_t transactionAccessCheck(uint16_t    const   cmd_type,
                                uint16_t    const   cmd_transaction_id,
                                uint32_t    const   sub_sel,
                                uint32_t    const   cyc_sel,
                                struct prop       * property)
{
    // Not supported;

    return FGC_OK_NO_RSP;
}



uint16_t transactionTest(uint8_t const sub_sel, uint16_t const id)
{
    // Not supported;

    return FGC_OK_NO_RSP;
}


uint16_t transactionCommit(uint8_t const sub_sel, uint16_t const id)
{
    // Not supported;

    return FGC_OK_NO_RSP;
}



uint16_t transactionRollback(uint8_t const sub_sel, uint16_t const id)
{
    // Not supported;

    return FGC_OK_NO_RSP;
}


void transactionAckCommitFailed(uint8_t const sub_sel, uint8_t const cyc_sel)
{
    // Not supported;
}


// EOF
