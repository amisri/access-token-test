//! @file  prop_class.c
//! @brief FGC_64 class specific property initialisation


// ---------- Includes

#include <defprops.h>
#include <prop_class.h>



// ---------- Internal variable definitions

static char devtype[] =
{
    "RFNA"
};



static int32_t conf_l[] =
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ 0,
    /* 3 */ 20000000,  // CAL.{A,B,C,D}.ADC.INTERNAL.GAIN (EDMS-1767404)
};



static uint16_t conf_s[] =
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ FGC_OP_NORMAL,
};



static float conf_cal[] =
{
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 8.0, -8.0, 80.0
};



// ---------- External variable definitions

// List of properties to be initialized with default values

// CAUTION: If the property is NON-VOLATILE, its default size (numels in the
//          XML definition) must be zero for it to be initialised based on the
//          data in prop_class_init_nvs

// Do not put in the external RAM! Initialisation will stop working!

struct init_prop prop_class_init_nvs[] =
{
    // Non-volatile, non-config properties

    {   &PROP_MODE_OP,                          1,                      &conf_s[2]      },
    {   &PROP_CAL_A_ADC_INTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_CAL_B_ADC_INTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_CAL_C_ADC_INTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_CAL_D_ADC_INTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_ADC_INTERNAL_LAST_CAL_TIME,       1,                      &conf_l[2]      },
    {   &PROP_FGC_NAME,                         sizeof(devtype),        &devtype[0]     },
    {   &PROP_DEVICE_TYPE,                      sizeof(devtype),        &devtype[0]     },

    // Global config properties

    {   &PROP_CAL_A_ADC_INTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_A_ADC_INTERNAL_TC,            3,                      &conf_cal[0]    },
    {   &PROP_CAL_A_ADC_INTERNAL_DTC,           3,                      &conf_cal[0]    },

    {   &PROP_CAL_B_ADC_INTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_B_ADC_INTERNAL_TC,            3,                      &conf_cal[0]    },
    {   &PROP_CAL_B_ADC_INTERNAL_DTC,           3,                      &conf_cal[0]    },

    {   &PROP_CAL_C_ADC_INTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_C_ADC_INTERNAL_TC,            3,                      &conf_cal[0]    },
    {   &PROP_CAL_C_ADC_INTERNAL_DTC,           3,                      &conf_cal[0]    },

    {   &PROP_CAL_D_ADC_INTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_D_ADC_INTERNAL_TC,            3,                      &conf_cal[0]    },
    {   &PROP_CAL_D_ADC_INTERNAL_DTC,           3,                      &conf_cal[0]    },

    {   &PROP_CAL_VREF_ERR,                     6,                      &conf_cal[0]    },
    {   &PROP_CAL_VREF_TC,                      3,                      &conf_cal[0]    },

    {   &PROP_BARCODE_FGC_CASSETTE,             0,                      NULL            },

    // Normal properties

    {   &PROP_CAL_A_DAC,                        3,                      &conf_cal[5]    },
    {   &PROP_CAL_B_DAC,                        3,                      &conf_cal[5]    },
    {   &PROP_ADC_INTERNAL_TAU_TEMP,            1,                      &conf_cal[8]    },

    {   NULL   }
};


uint32_t prop_class_init_spy_mpx[FGC_N_SPY_CHANS] =
{
    FGC_SPY_V_A,      FGC_SPY_V_B,
    FGC_SPY_V_C,      FGC_SPY_V_D,
    FGC_SPY_T_FGC_IN, FGC_SPY_T_FGC_OUT
};


// The DF_TIMESTAMP_SELECT dynamic flag is set for all the properties in the
// following list and their children. The same list is also used in function
// CmdPrintTimestamp to read the timestamp selector.

struct Prop_class_init_dynflag_timestamp_select prop_class_init_dynflag_timestamp_select[] =
{
    { &PROP_DIAG_ANA,  TIMESTAMP_DIMS },
    { &PROP_DIAG_DATA, TIMESTAMP_DIMS },
    { &PROP_DIAG_DIG,  TIMESTAMP_DIMS },
    { NULL }
};


// EOF
