//! @file  set_class.c
//! @brief Class specific Set command functions


#include <set_class.h>
#include <function_class.h>
#include <log_class.h>
#include <cmd.h>
#include <fgc_errs.h>
#include <pars.h>
#include <defprops.h>
#include <pub.h>



// ---------- External function definitions

uint16_t SetRefControlValue(struct cmd * c, struct prop * p)
{
    float     * fvalue_ptr;
    float       ref;
    uint16_t    errnum;

    if ((errnum = ParsValueGet(c, p, (void **) &fvalue_ptr)) != 0)
    {
        return errnum;
    }

    ref = *fvalue_ptr;

    // Get the limits for the reference

    struct prop * limit_pos_p = NULL;
    struct prop * limit_neg_p = NULL;

    if (p == &PROP_REF_DIRECT_V_VALUE)
    {
        limit_pos_p = &PROP_LIMITS_V_MAX;
        limit_neg_p = &PROP_LIMITS_V_MIN;
    }
    else if (p == &PROP_REF_DIRECT_Q_VALUE)
    {
        limit_pos_p = &PROP_LIMITS_Q_MAX;
        limit_neg_p = &PROP_LIMITS_Q_MIN;
    }
    else if (p == &PROP_REF_DIRECT_A_VALUE)
    {
        limit_pos_p = &PROP_LIMITS_A_MAX;
        limit_neg_p = &PROP_LIMITS_A_MIN;
    }
    else if (p == &PROP_REF_DIRECT_PF_RATE)
    {
        limit_pos_p = &PROP_LIMITS_PF_MAX;
        limit_neg_p = &PROP_LIMITS_PF_MIN;
    }
    else if (p == &PROP_REF_DIRECT_PF_SIGN)
    {
        ; // Do nothing
    }
    else
    {
        return FGC_NOT_IMPL;
    }

    if (limit_pos_p != NULL && limit_neg_p != NULL)
    {
        if (ref < *(float *)limit_neg_p->value || ref > *(float *)limit_pos_p->value)
        {
            return FGC_OUT_OF_LIMITS;
        }
    }

    functionClassBackup();

    PropSet(c, p, (void*)&ref, 1);

    if ((errnum = functionClassSetReference()) != FGC_OK_NO_RSP)
    {
        functionClassRestore();

        return errnum;
    }

    pubPublishProperty(p, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, TRUE);

    return FGC_OK_NO_RSP;
}



uint16_t SetRegMode(struct cmd * c, struct prop * p)
{
    uint16_t errnum;

    functionClassBackup();

    if ((errnum = ParsSet(c, p)) != 0)
    {
        functionClassRestore();

        return errnum;
    }

    if ((errnum = functionClassSetRegMode()) != FGC_OK_NO_RSP)
    {
        functionClassRestore();

        return errnum;
    }

    pubPublishProperty(p, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, TRUE);

    return FGC_OK_NO_RSP;
}



uint16_t SetRef(struct cmd * c, struct prop * p)
{
    // SetRef provides support for S REF FUNC_TYPE,args,... and
    // S REF.FUNC.TYPE(user) FUNC_TYPE

    // @TODO FGC_64 implement if needed

    return FGC_OK_NO_RSP;
}


// EOF
