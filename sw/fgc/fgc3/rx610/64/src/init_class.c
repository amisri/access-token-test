//! @file     init_class.c
//! @brief    Class specific initialization functions


// ---------- Includes

#include <function_class.h>



// ---------- Platform/class specific function definitions

void initClassPostNvs(void)
{
	// Do nothing
}



void initClassPostDb(void)
{
	// Do nothing
}



void initClassPostRegFgc3Cache(void)
{
	functionClassSetReference();
	functionClassSetRegMode();
}


// EOF
