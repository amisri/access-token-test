//! @file   digitalIo_class.c
//! @brief  Class specific digital inputs and outputs handling


// ---------- Includes

#include <stdint.h>

#include <digitalIo.h>



// ---------- Platform/class specific function definitions

void digitalIoSupInitClass(void)
{
	// Based on https://wikis.cern.ch/display/TEEPCCCE/FGC3+Definition+of+DIG-SUP+with+RegFGC3+cards

    // Bank A: data
    // Bank B: data

    for (uint16_t channel = 0; channel < 4; channel++)
    {
        DIG_SUP_A_CHAN_CTRL_A[channel] = DIG_SUP_CTRL_OUTMPX_DATA;
        DIG_SUP_B_CHAN_CTRL_A[channel] = DIG_SUP_CTRL_OUTMPX_DATA;
    }
}


// EOF
