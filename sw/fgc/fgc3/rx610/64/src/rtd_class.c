//! @file   rtd_class.c
//! @brief  Class FGC_64 specific source file


#include <rtd.h>
#include <function_class.h>
#include <diag_class.h>


// ---------- Constants

#define RTD_POS_V_REF           "21;32"
#define RTD_POS_Q_REF           "22;32"
#define RTD_POS_A_REF           "23;32"
#define RTD_POS_PF_RATE         "24;32"
#define RTD_POS_MEAS_QF         "21;45"
#define RTD_POS_MEAS_QT         "22;45"
#define RTD_POS_MEAS_QL         "23;45"
#define RTD_POS_MEAS_VRS        "21;59"
#define RTD_POS_MEAS_VST        "22;59"
#define RTD_POS_MEAS_VTR        "23;59"
#define RTD_POS_MEAS_PF         "21;76"
#define RTD_POS_MEAS_FREQ       "22;75"

#define RTD_POS_PLL_OK          "24;50"

#define RTD_POS_FUNC_TYPE_V     "21;28"
#define RTD_POS_FUNC_TYPE_Q     "22;28"
#define RTD_POS_FUNC_TYPE_A     "23;28"
#define RTD_POS_FUNC_TYPE_PF    "24;28"

#define RTD_POS_ERR_MEAS_QF     "21;44"
#define RTD_POS_ERR_MEAS_QT     "22;44"
#define RTD_POS_ERR_MEAS_QL     "23;44"
#define RTD_POS_ERR_MEAS_VRS    "21;58"
#define RTD_POS_ERR_MEAS_VST    "22;58"
#define RTD_POS_ERR_MEAS_VTR    "23;58"
#define RTD_POS_ERR_MEAS_PF     "21;71"
#define RTD_POS_ERR_MEAS_FREQ   "22;72"
#define RTD_POS_ERR_PLL_OK      "24;34"



// ---------- Internal function declarations

static bool rtdClassRegMode(bool reset);



// ---------- Internal variable definitions

static struct Rtd_field_func rtd_class_field_funcs[] =
{
    { rtdClassRegMode   },
    { NULL              },
};


static struct Rtd_field_float rtd_class_field_floats[] =
{
    { &reference.direct.v_ref,   "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_V_REF,   9 },
    { &reference.direct.q_ref,   "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_Q_REF,   9 },
    { &reference.direct.a_ref,   "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_A_REF,   9 },
    { &reference.direct.pf_rate, "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_PF_RATE, 9 },
    { NULL                                                                     },
};


static struct Rtd_field_error_float rtd_class_field_floats_errors[] =
{
    // Order is important as rtdClassUpdateErrors() depends on it

    { { &meas_svc.q.feeder.sum.value, "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_MEAS_QF  , 9 }, { 0x00, 0xFF } },
    { { &meas_svc.q.tcr.sum.value   , "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_MEAS_QT  , 9 }, { 0x00, 0xFF } },
    { { &meas_svc.q.load.sum.value  , "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_MEAS_QL  , 9 }, { 0x00, 0xFF } },
    { { &meas_svc.v.rs.value        , "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_MEAS_VRS , 9 }, { 0x00, 0xFF } },
    { { &meas_svc.v.st.value        , "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_MEAS_VST , 9 }, { 0x00, 0xFF } },
    { { &meas_svc.v.tr.value        , "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_MEAS_VTR , 9 }, { 0x00, 0xFF } },
    { { &meas_svc.reg.pf.value      , "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_MEAS_PF  , 5 }, { 0x00, 0xFF } },
    { { &meas_svc.reg.freq.value    , "%*.3f", 0xFFFFFFFF, 1000, RTD_POS_MEAS_FREQ, 6 }, { 0x00, 0xFF } },
    { { NULL                                                                                          } },
};


static struct Rtd_field_bool rtd_class_field_bools[] =
{
    { &meas_svc.pll_ok.value, false, true, RTD_POS_PLL_OK },
    { NULL                                                },
};


static struct Rtd_process rtd_class_process[] =
{
    { rtdProcessFuncs,        { (void *)rtd_class_field_funcs,         (void *)rtd_class_field_funcs         } },
    { rtdProcessFloats,       { (void *)rtd_class_field_floats,        (void *)rtd_class_field_floats        } },
    { rtdProcessFloatsErrors, { (void *)rtd_class_field_floats_errors, (void *)rtd_class_field_floats_errors } },
    { rtdProcessBools,        { (void *)rtd_class_field_bools,         (void *)rtd_class_field_bools         } },
    { NULL                                                                                                     }
};




// ---------- Internal function definitions

static bool rtdClassRegMode(bool reset)
{
    static uint32_t         prev_reg_mode  = 0xFFFFFFFF;
    static char     const * reg_mode_pos[] = { RTD_POS_FUNC_TYPE_V,
                                               RTD_POS_FUNC_TYPE_Q,
                                               RTD_POS_FUNC_TYPE_A,
                                               RTD_POS_FUNC_TYPE_PF };
    static char     const   reg_mod_ch[]   = { 'V', 'Q', 'A', 'P' };
    static bool             update_prev_reg_mode = true;

    // Reset reg mode

    if (reset == true)
    {
        prev_reg_mode = 0xFFFFFFFF;

        return false;
    }

    uint32_t reg_mode = reference.func.reg_mode;

    if (prev_reg_mode == reg_mode )
    {
        return false;
    }

    if (update_prev_reg_mode == true)
    {
        // Restore previous regulation mode

        rtdMoveCursor(reg_mode_pos[prev_reg_mode], "");

        fprintf(rtd.f, "%c", reg_mod_ch[prev_reg_mode]);

        rtdRestoreCursor();

        update_prev_reg_mode = false;
    }
    else
    {
        // Update current regulation mode

        rtdMoveCursor(reg_mode_pos[reg_mode], TERM_BOLD TERM_REVERSE);

        fprintf(rtd.f, "%c", reg_mod_ch[reg_mode]);

        rtdRestoreCursor();

        update_prev_reg_mode = true;

        prev_reg_mode = reg_mode;
    }

    return (prev_reg_mode == false);
}



// ---------- External function definitions

void rtdClassInit(void)
{
    // Do nothing

    ;
}



void rtdClassReset(FILE * f)
{
    // Move to RTD display area

    fputs("\33[21;1H", f);

    //    0         10        20        30        40        50        60        70        80
    //    |---------+---------+---------+---------+---------+---------+---------+---------+|

    fputs("States: __.__.__.__        Vrf:_____.___ Qf:_____.___ VRS:_____.___ PF:    _.___\n\r", f);
    fputs("                           Qrf:_____.___ Qt:_____.___ VST:_____.___ Frq:  __.___\n\r", f);
    fputs("CFG:                       Arf:_____.___ Ql:_____.___ VTR:_____.___ CPU:___%___%\n\r", f);
    fputs("PERMIT:                    Prf:_____.___ PLL OK: _                   __._C __._C", f);

    // Set up scroll window to protect RTD lines

    fputs("\33[H\33[1;19r\v", f);

   // Reset func fields

    struct Rtd_field_func * field_func;

    for (field_func = &rtd_class_field_funcs[0]; field_func->func != NULL; field_func++)
    {
        field_func->func(true);
    }

    // Reset float fields

    struct Rtd_field_float * field_float;

    for (field_float = &rtd_class_field_floats[0]; field_float->source != NULL; field_float++)
    {
        field_float->last_value = 0x6FFFFFFF;
    }

    struct Rtd_field_error_float * field_error_float;

    for (field_error_float = &rtd_class_field_floats_errors[0]; field_error_float->field.source != NULL; field_error_float++)
    {
        field_error_float->field.last_value = 0x6FFFFFFF;
        field_error_float->error.last_value = 0xFF;
    }

    // Reset bool fields

    struct Rtd_field_bool * field_bool;

    for (field_bool = &rtd_class_field_bools[0]; field_bool->source != NULL; field_bool++)
    {
        field_bool->first_print = true;
    }
}



bool rtdClassProcess(bool reset)
{
    static struct Rtd_process * prev_process = rtd_class_process;

    struct Rtd_process * field   = prev_process;
    bool                 updated = false;

    for (;  field->func != NULL && updated == false; field++)
    {
        updated = field->func(&field->args);
    }

    prev_process = (field->func == NULL ? rtd_class_process : field);

    return updated;
}



void rtdClassUpdateErrors(void)
{
    rtd_class_field_floats_errors[0].error.value = (meas_svc.q.feeder.sum.channel == -1) ? RTD_VALUE_NO_PRINT : 0;
    rtd_class_field_floats_errors[1].error.value = (meas_svc.q.tcr.sum.channel    == -1) ? RTD_VALUE_NO_PRINT : 0;
    rtd_class_field_floats_errors[2].error.value = (meas_svc.q.load.sum.channel   == -1) ? RTD_VALUE_NO_PRINT : 0;
    rtd_class_field_floats_errors[3].error.value = (meas_svc.v.rs.channel         == -1) ? RTD_VALUE_NO_PRINT : 0;
    rtd_class_field_floats_errors[4].error.value = (meas_svc.v.st.channel         == -1) ? RTD_VALUE_NO_PRINT : 0;
    rtd_class_field_floats_errors[5].error.value = (meas_svc.v.tr.channel         == -1) ? RTD_VALUE_NO_PRINT : 0;
    rtd_class_field_floats_errors[6].error.value = (meas_svc.reg.pf.channel       == -1) ? RTD_VALUE_NO_PRINT : 0;
    rtd_class_field_floats_errors[7].error.value = (meas_svc.reg.freq.channel     == -1) ? RTD_VALUE_NO_PRINT : 0;
}


// EOF
