//! @file  msTask_class.c
//! @brief Class specific millisecond actions


// ---------- Includes

#include <msTask.h>
#include <diag_class.h>
#include <digitalIo.h>
#include <fbs_class.h>
#include <function_class.h>
#include <trm.h>
#include <modePc.h>



// ---------- Internal structures, unions and enumerations

enum Mstask_class_external_commands
{
    MSTCLASS_EXTERNAL_COMMAND_RESET ,
    MSTCLASS_EXTERNAL_COMMAND_ON    ,
    MSTCLASS_EXTERNAL_COMMAND_OFF   ,
};

enum Mstask_class_external_commands_bitmask
{
    MSTCLASS_EXTERNAL_COMMAND_RESET_BIT_MASK = 1 << MSTCLASS_EXTERNAL_COMMAND_RESET,
    MSTCLASS_EXTERNAL_COMMAND_ON_BIT_MASK    = 1 << MSTCLASS_EXTERNAL_COMMAND_ON   ,
    MSTCLASS_EXTERNAL_COMMAND_OFF_BIT_MASK   = 1 << MSTCLASS_EXTERNAL_COMMAND_OFF  ,
};



// ---------- Internal function definitions

static void msTaskClassPublishData(void)
{
    //  Millisecond 1 of 20: Read DSP status and run Gateway PC_PERMIT timeout

    if (ms_task.ms_mod_20 == 1)
    {
        REG_MODE     = reference.func.reg_mode;
        Q_REF        = reference.direct.q_ref;
        V_REF        = reference.direct.v_ref;
        Q_FEEDER     = meas_svc.q.feeder.sum.value;
        V_RS         = meas_svc.v.rs.value;
        FREQ         = meas_svc.reg.freq.value;
        ALPHA        = meas_svc.reg.alpha.value;
    }
}


static void msTaskClassSendExternalCommand(enum Mstask_class_external_commands command)
{
    // The commands are copied to the terminal message queue as if they had been
    // sent using the serial terminal interface

    static char const * const external_commands[] =
    {
        "!S VS RESET;",                 // MSTCLASS_EXTERNAL_COMMAND_RESET
        "!S MODE.PC_SIMPLIFIED ON;",    // MSTCLASS_EXTERNAL_COMMAND_ON
        "!S MODE.PC_SIMPLIFIED OFF;"    // MSTCLASS_EXTERNAL_COMMAND_OFF
    };

    char const * c = external_commands[command];

    while (*c)
    {
        OSMsgPost(terminal.msgq, (void *)(intptr_t) *c++);
    }
}



static void msTaskClassProcessExternalCommand(void)
{
    static bool ch0_edge_rising_edge = false;
    static bool ch1_edge_rising_edge = false;
    static bool ch3_edge_rising_edge = false;

    uint8_t command = 0;

    // Mapping of DIG_SUPB channels and external commands
    //
    // CH_0 -> ON
    // CH_1 -> OFF
    // CH_3 -> RESET
    // CH_4 -> ENABLE
    //
    // All signals use inverse logic
    //
    // Channel priorities:
    //
    // 1) If ENABLE is active, ignore all commands.
    // 2) If OFF is active ignore ON
    // 3) RESET can be active whilst ON or OFF are active

    if (digitalIoSupGetChannelB(DIG_SUP_B_CHANNEL4) == true)
    {
        return;
    }

    // SUP_B0 edge detection: ON

    if (digitalIoSupGetChannelB(DIG_SUP_B_CHANNEL0) == false)
    {
        if (ch0_edge_rising_edge == false)
        {
            command |= MSTCLASS_EXTERNAL_COMMAND_ON_BIT_MASK;
        }

        ch0_edge_rising_edge = true;
    }
    else
    {
        ch0_edge_rising_edge = false;
    }

    // SUP_B1 edge detection: OFF

    if (digitalIoSupGetChannelB(DIG_SUP_B_CHANNEL1) == false)
    {
        if (ch1_edge_rising_edge == false)
        {
            command |= MSTCLASS_EXTERNAL_COMMAND_OFF_BIT_MASK;
        }

        ch1_edge_rising_edge = true;
    }
    else
    {
        ch1_edge_rising_edge = false;
    }

    // SUP_B3 edge detection: RESET

    if ((digitalIoSupGetChannelB(DIG_SUP_B_CHANNEL3) == false))
    {
        if (ch3_edge_rising_edge == false)
        {
            command |= MSTCLASS_EXTERNAL_COMMAND_RESET_BIT_MASK;
        }

        ch3_edge_rising_edge = true;
    }
    else
    {
        ch3_edge_rising_edge = false;
    }

    // Send commands based on signal edge detection

    if (testBitmap(command, MSTCLASS_EXTERNAL_COMMAND_RESET_BIT_MASK) == true)
    {
        msTaskClassSendExternalCommand(MSTCLASS_EXTERNAL_COMMAND_RESET);
    }

    if (testBitmap(command, MSTCLASS_EXTERNAL_COMMAND_OFF_BIT_MASK) == true)
    {
        msTaskClassSendExternalCommand(MSTCLASS_EXTERNAL_COMMAND_OFF);
    }
    else if (testBitmap(command, MSTCLASS_EXTERNAL_COMMAND_ON_BIT_MASK) == true)
    {
        msTaskClassSendExternalCommand(MSTCLASS_EXTERNAL_COMMAND_ON);
    }
}



// ---------- Platform/class specific function definitions

void msTaskClassProcess(void)
{
    msTaskClassProcessExternalCommand();

    msTaskClassPublishData();
}


// EOF

