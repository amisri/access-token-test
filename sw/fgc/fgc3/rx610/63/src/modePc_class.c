//! @file  modePC_class.c
//! @brief Class related handling of modePc


#define FGC_MODE_PC_CLASS_GLOBALS


// ---------- Includes

#include <stdint.h>

#include <defprops.h>
#include <modePc_class.h>
#include <statePc.h>
#include <pc_state.h>
#include <fgc_errs.h>
#include <digitalIo.h>
#include <polSwitch.h>



// ---------- Platform/class specific function definitions

uint32_t modePcClassValidateMode(uint8_t mode)
{
    switch (mode)
    {
        case FGC_PC_OFF:
        {
            if (pcStateTest(FGC_STATE_VALID_RESET_BIT_MASK) == true)
            {
                // Make sure the faults below are cleared

                if (vs.fw_diode == FGC_VDI_FAULT)
                {
                    return (FGC_FW_DIODE_FAULT);
                }

                if (vs.fabort_unsafe == FGC_VDI_FAULT)
                {
                    return (FGC_FABORT_UNSAFE);
                }

                statePcResetFaults();
            }

            // Power off the converter if S PC OFF is issued in SLOW ABORT

            if (pcStateGet() == FGC_PC_SLOW_ABORT)
            {
                // Postpone sending the commnad OFF until libref has transitioned to OFF

                dpcls.mcu.ref.force_off = true;
            }

            break;
        }

        case FGC_PC_BLOCKING:
        {
            if (dpcls.mcu.vs.blockable == FGC_VS_BLOCK_DISABLED)
            {
                return FGC_BLOCKING_NOT_ALLOWED;
            }

            // Fall through
        }
        case FGC_PC_IDLE:
        {
            if (   polSwitchGetState() == POLSWITCH_STATE_MOVING
                || polSwitchGetState() == POLSWITCH_STATE_FAULT
                || pcStateTest(  FGC_STATE_FLT_OFF_BIT_MASK
                               | FGC_STATE_FLT_STOPPING_BIT_MASK
                               | FGC_STATE_STOPPING_BIT_MASK) == true)
            {
                return FGC_BAD_STATE;
            }

            // If state is ARMED and the start event has not yet been received,
            // transition to LE by disarming the function

            if (pcStateGet() == FGC_PC_ARMED)
            {
                dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_REF_DISARM);
            }

            // If the converter is running, then IDLE aborts the function

            if (pcStateGet() == FGC_PC_RUNNING)
            {
                // Inform the DSP to abort the function

                dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_ABORT);
            }

            break;
        }
        case FGC_PC_ON_STANDBY:  // Fall through
        case FGC_PC_CYCLING:     // Fall through
        case FGC_PC_DIRECT:
        {
            if (   polSwitchGetState() == POLSWITCH_STATE_MOVING
                || polSwitchGetState() == POLSWITCH_STATE_FAULT
                || pcStateTest(  FGC_STATE_FLT_OFF_BIT_MASK
                               | FGC_STATE_FLT_STOPPING_BIT_MASK
                               | FGC_STATE_STOPPING_BIT_MASK) == true)
            {
                return FGC_BAD_STATE;
            }

            break;
        }

        case FGC_PC_SLOW_ABORT:   // Fall through
        default:
            return FGC_BAD_PARAMETER;

    }

    return FGC_OK_NO_RSP;
}


// EOF
