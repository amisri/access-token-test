//! @file   logCycle_class.c
//! @brief  Class FGC_63 specific code to interface to the library libcyclog


// ---------- Includes

#include <stdint.h>

#include <class.h>
#include <cmd.h>
#include <defprops.h>



// ---------- Platform/class specific function definitions

char * logCycleClassFuncTypeCallback(uint32_t const func_type)
{
    return CmdPrintSymLst(SYM_LST_REF_TYPE, func_type);
}



char * logCycleClassStatusCallback(uint32_t const status)
{
    return CmdPrintSymLst(SYM_LST_CYC_STATUS, status);
}



uint32_t logCycleClassGetStatusOk(void)
{
    return (  FGC_CYC_STATUS_REG_OK
            | FGC_CYC_STATUS_ABORTED
            | FGC_CYC_STATUS_NO_PRE_FUNC
            | FGC_CYC_STATUS_DYNEC_ARM_WRN
            | FGC_CYC_STATUS_DYNEC_RUN_WRN
            | FGC_CYC_STATUS_INCOMPLETE
            | FGC_CYC_STATUS_DYN_ECO
            | FGC_CYC_STATUS_DEST_ECO);
}



uint32_t logCycleClassGetStatusWarnings(void)
{
    return (  FGC_CYC_STATUS_REG_WARNING
            | FGC_CYC_STATUS_PRE_FUNC_WRN
            | FGC_CYC_STATUS_OVERRUN);
}



uint32_t logCycleClassGetStatusFaults(void)
{
    return FGC_CYC_STATUS_REG_FAULT;
}


// EOF
