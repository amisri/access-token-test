//! @file  setif_class.c
//! @brief Class specific Setif command functions


// ---------- Includes

#include <fgc_errs.h>

#include <dpcls.h>
#include <cmd.h>



// ---------- External function definitions

uint16_t SetifRegStateNone(struct cmd * c)
{
    if (dpcls.dsp.ref.reg_mode == FGC_REG_NONE)
    {
        return 0;
    }

    return FGC_BAD_STATE;
}


// EOF
