//! @file  get_class.c
//! @brief Class specific get functions


#define GET_CLASS_GLOBALS


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <class.h>
#include <cmd.h>
#include <defprops.h>
#include <function_class.h>
#include <interFgc.h>
#include <logSpy.h>
#include <logCycle.h>
#include <property.h>



// ---------- External function definitions

uint16_t GetLogCycles(struct cmd * c, struct prop * p)
{
    prop_size_t   n;
    uint16_t      errnum;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    // 20 characters for status

    uint32_t const buf_len = CYCLOG_READ_FGC_FIXED_LEN + 20;

    char read_buf[buf_len];

    while (n-- > 0)
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)
        {
            return (errnum);
        }

        logCycleGet(c->from++, read_buf, buf_len);

        fprintf(c->f, "%s", read_buf);
    }

    return 0;
}



uint16_t GetLogI(struct cmd * c, struct prop * p)
{
    static struct CC_us_time time_origin = { { 0 }, 0 };

    // Verify the get options supported

    if (testBitmap(c->getopts, (GET_OPT_HEX   | GET_OPT_IDX  | GET_OPT_NOIDX |
                          GET_OPT_RANGE | GET_OPT_ZERO)))
    {
        return (FGC_BAD_GET_OPT);
    }

    // Verify the command is sent via the fieldbus

    if (c == &tcm)
    {
        return (0);
    }

    uint16_t errnum;

    if (p == &PROP_LOG_I)
    {
        // When retrieveing LOG.I with the non-PPM selector the
        // time origin must be defiend to align REF and MEAS

        if (c->cyc_sel == 0)
        {
            time_origin = ms_task.time_us;
        }

        errnum = CmdParentGet(c, p, PARENT_GET_NOT_HIDDEN);

        time_origin.secs.abs = 0;
        time_origin.us = 0;
    }
    else
    {
        prop_size_t  n;

        errnum = CmdPrepareGet(c, p, 1, &n);

        if (errnum == FGC_OK_NO_RSP)
        {
            // Check if there is a lingering time_origin from an uncomplete
            // access. If the difference between the latched time origin and
            // now is greater than 2 s, discard the time. Assume that retrieving
            // LOG.I.REF and LOG.I.MEAS via LOG.I takes less than 1 s

            if (timeUsDiff(&time_origin, &ms_task.time_us) > 2000000)
            {
                time_origin.secs.abs = 0;
                time_origin.us = 0;
            }

            // When retrieveing logs with the non-PPM selector, OP
            // expected the logs to be 60 seconds.

            uint32_t duration_ms = c->cyc_sel == 0 ? 60000 : 0;

            errnum = logSpyGetBuffer(c, (uint32_t)p->value, 1, &time_origin, duration_ms);
        }
    }

    return errnum;
}



uint16_t GetPoint(struct cmd * c, struct prop * p)
{
    struct Point * value_p;
    struct Point   value;
    bool           bin_f;
    bool           hex_f;
    prop_size_t    n;
    uint16_t       errnum;

    bin_f = testBitmap(c->getopts, GET_OPT_BIN);
    hex_f = testBitmap(c->getopts, GET_OPT_HEX);

    errnum = CmdPrepareGet(c, p, 2, &n);

    if (errnum)
    {
        return (errnum);
    }

    if (n != 0 && bin_f == true)
    {
        CmdStartBin(c, p, (uint32_t)(sizeof(struct Point)) * (uint32_t)n);
    }

    // For all elements

    while (n--)
    {
        // Get element value from property

        errnum = PropValueGet(c, p, NULL, c->from, (uint8_t **)&value_p);

        if (errnum != 0)
        {
            return (errnum);
        }

        value = *value_p;

        if (errnum)
        {
            return (errnum);
        }

        if (bin_f)
        {
            fbsOutBuf((uint8_t *) &value, sizeof(struct Point), c);
        }
        else
        {
            // Print index if required

            errnum = CmdPrintIdx(c, c->from);

            if (errnum)
            {
                return (errnum);
            }

            if (hex_f)
            {
                // Display point time and reference as 0xTTTTTTTT|0xRRRRRRRR

                fprintf(c->f, "0x%08lX|0x%08lX", (uint32_t)value.time, (uint32_t)value.ref);
            }
            else
            {
                // Display point as n.nnnn|n.nnnnnnnE+/-nn

                fprintf(c->f, "%.4f|%.7E", (double)value.time, (double)value.ref);
            }
        }

        c->from += c->step;
    }

    return (0);
}



uint16_t GetMac(struct cmd * c, struct prop * p)
{
    return interFgcGetMac(c, p);
}


// EOF
