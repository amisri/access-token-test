//! @file  diag_class.c
//! @brief Class specific functionality for diagnostics


// ---------- Includes

#include <bitmap.h>
#include <class.h>
#include <diag.h>
#include <digitalIo.h>
#include <dpcls.h>
#include <fbs_class.h>
#include <logSpy.h>
#include <log_class.h>
#include <sta.h>
#include <stateOp.h>
#include <stateVs.h>
#include <status.h>



// ---------- External function definitions

void diagClassInit(void)
{
    // Some power converters are made from subconverters which can trip off without
    // stopping the whole converter. The state of the subconverters is available via
    // the diagnostic system and this function will find the relevant channels and
    // save them so that they can be checked during operation.

    static char vrw_name[] = "VS_REDUNDANCY_WARNING";
    static char fwd_name[] = "FREE WHEEL DIODE";
    static char fau_name[] = "FAST ABORT UNSAFE";

    // Find analogue channels

    // The I earth analogue channels come in two flavours:
    // One single channel (COABLAT and ACAPULCO): I_EARTH
    // Four channels (RegFGC3): I_EARTH_1MS, I_EARTH_1MS, I_EARTH_11MS and I_EARTH_16MS

    meas_i_earth.channel[0] = DiagFindAnaChan("I_EARTH");

    if (meas_i_earth.channel[0] != -1)
    {
        // Use the same value in log.c LogIearth(). Logging is done at 200 Hz
        // whilst sampling in this case is 50 Hz (20 ms) since there is only
        // one channel. This means that the same value will be logged four
        // time.

        meas_i_earth.channel[1] = meas_i_earth.channel[0];
        meas_i_earth.channel[2] = meas_i_earth.channel[0];
        meas_i_earth.channel[3] = meas_i_earth.channel[0];
    }
    else
    {
        // Assume the four channels below exist. If not, channel[] will contain
        // -1, which will default in a I earth value of 0.0.

        meas_i_earth.channel[0] = DiagFindAnaChan("I_EARTH_1MS");
        meas_i_earth.channel[1] = DiagFindAnaChan("I_EARTH_6MS");
        meas_i_earth.channel[2] = DiagFindAnaChan("I_EARTH_11MS");
        meas_i_earth.channel[3] = DiagFindAnaChan("I_EARTH_16MS");
    }

    // Do not log I_EARTH if it is not found in the system

    if (meas_i_earth.channel[0] == -1)
    {
        logClassDisableLog(DPCOM_LOG_IGNORE_I_EARTH_BIT_MASK);
    }

    uint16_t  logical_dim;

    // Find digital channels for sub-converters, free-wheel diode and fast abort unsafe

    vs.fabort_unsafe  = FGC_VDI_NOT_PRESENT;
    vs.fw_diode       = FGC_VDI_NOT_PRESENT;

    for (logical_dim = 0; logical_dim < FGC_MAX_DIMS; logical_dim++)
    {
        if (DiagFindDigChan(logical_dim, fau_name, &vs.fabort_unsafe_dig.channel, &vs.fabort_unsafe_dig.mask) == true)
        {
            vs.fabort_unsafe = FGC_VDI_NO_FAULT;
        }

        if (DiagFindDigChan(logical_dim, fwd_name, &vs.fw_diode_dig.channel, &vs.fw_diode_dig.mask) == true)
        {
            vs.fw_diode = FGC_VDI_NO_FAULT;
        }

        DiagFindDigChan(logical_dim, vrw_name, &vs.vs_redundancy_warning.channel, &vs.vs_redundancy_warning.mask);
    }
}



void diagClassCheckConverter(void)
{
    // This function is called from stateTask() every 5ms. Provided the converter is not being
    // simulated then every 4th iteration it will check the number of active subconverters
    // based on the diag data channels and masks identified by diagClassInit(). It provides
    // the number to the DSP so that the actual maximum current can be calculated.
    // It also checks to see if the VS DIM is reporting a FREE WHEEL DIODE fault and if so
    // it sets the VS.FW_DIODE property and asserts a FW_DIODE fault. Since this can trip
    // the converter and the signal comes from the DIM, a filter is included to avoid spurious
    // DIAG data from stopping the converter. This requires a VS_FAULT to be present and the
    // FW_DIODE fault bit in the DIM data to be active for 200ms.
    // It also checks to see if a DIM is reporting an unlatched trigger while the converter
    // is not reporting a fault. If this condition persists, the DIAG_TRIG_FLT flag will be
    // set in the latched status, which will generate an FGC_HW warning.

    static uint16_t fw_diode_counter;                     // Filter counter for FW_DIODE fault
    static uint16_t fabort_unsafe_counter;                // Filter counter for FABORT_UNSAFE fault
    static uint16_t dim_trig_counter;                     // Filter counter for DIM_TRIG_FLT latched status

    if ((stateOpGetState() != FGC_OP_SIMULATION)              // If not simulating the converter and
        && (((uint16_t) sta.timestamp_ms.ms % 20) == (15 + DEV_STA_TSK_PHASE))      // time is end of 20ms cycle
       )
    {
        // Check for voltage source redundancy warning

        if (   vs.vs_redundancy_warning.mask != 0
            && dim_collection.dims_are_valid)
        {
            if (testBitmap(diag.data.w[vs.vs_redundancy_warning.channel], vs.vs_redundancy_warning.mask))
            {
                statusSetWarnings(FGC_WRN_SUBCONVTR_FLT);
            }
            else
            {
                statusClrWarnings(FGC_WRN_SUBCONVTR_FLT);
            }
        }

        // Check for a Free Wheel Diode fault - with 200ms filter

        // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)

        if (   vs.fw_diode_dig.mask
            && (stateOpGetState() == FGC_OP_NORMAL)
            && testBitmap(digitalIoGetOutputs(), DIG_OP_SET_VSRESETCMD_MASK16)        == false
            && statusTestFaults(FGC_FLT_VS_FAULT)                                     == true
            && testBitmap(diag.data.w[vs.fw_diode_dig.channel], vs.fw_diode_dig.mask) == true)
        {
            fw_diode_counter++;

            if (fw_diode_counter > 10)                            // If fault present for > 200ms
            {
                vs.fw_diode = FGC_VDI_FAULT;
            }
        }
        else
        {
            fw_diode_counter = 0;
        }

        // Check for a Fast Abort fault - with 100ms filter

        // label DIAG_FLAT, to know that diag.data needs to be adjusted in this part of the code (in case of change)

        if (   vs.fabort_unsafe_dig.mask
            && (stateOpGetState() == FGC_OP_NORMAL)
            && testBitmap(digitalIoGetOutputs(), DIG_OP_SET_VSRESETCMD_MASK16)                  == false
            && statusTestFaults(FGC_FLT_VS_FAULT)                                               == true
            && testBitmap(diag.data.w[vs.fabort_unsafe_dig.channel], vs.fabort_unsafe_dig.mask) == true)
        {
            fabort_unsafe_counter++;

            if (fabort_unsafe_counter > 5)                     // If fault present for > 100ms
            {
                vs.fabort_unsafe = FGC_VDI_FAULT;
            }
        }
        else
        {
            fabort_unsafe_counter = 0;
        }

        if (   dim_collection.unlatched_trigger_fired                == true
            && testBitmap(digitalIoGetInputs(), DIG_IP_VSRUN_MASK32) == true
            && statusTestFaults (FGC_FLT_VS_FAULT)                   == false
            && statusTestFaults (FGC_FLT_VS_EXTINTLOCK)              == false
            && statusTestFaults (FGC_FLT_FAST_ABORT)                 == false
            && statusTestLatched(FGC_LAT_DIM_TRIG_FLT)               == false)
        {
            if (++dim_trig_counter > 5)                   // If fault present for > 100ms
            {
                statusSetLatched(FGC_LAT_DIM_TRIG_FLT);     // Set DIM_TRIG_FLT in latched status
            }
        }
        else
        {
            dim_trig_counter = 0;
        }
    }
}



void diagClassUpdateMeas(void)
{
    // Update the simulated earth current

    meas_i_earth.simulated = 0.8 * dpcls.dsp.meas.i * meas_i_earth.limit / dpcls.dsp.limits.i_pos;
}


// EOF
