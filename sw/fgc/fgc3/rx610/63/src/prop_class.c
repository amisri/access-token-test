//! @file  prop_class.c
//! @brief FGC_63 class specific property initialisation

// ---------- Includes

#include <defprops.h>
#include <prop_class.h>



// ---------- Internal variable definitions

static char devtype[] =
{
    "RFNA"
};



static int32_t conf_l[] =
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ 0,
    /* 3 */ 20000000,  // CAL.{A,B,C,D}.ADC.INTERNAL.GAIN (EDMS-1767404)
    /* 4 */ 20000000,  // CAL.{A,B}.ADC.EXTERNAL.GAIN     (EDMS-1767404)
};



static uint16_t conf_s[] =
{
    /* 0 */ FGC_CTRL_DISABLED,
    /* 1 */ FGC_CTRL_ENABLED,
    /* 2 */ FGC_OP_NORMAL,
};



static float conf_f[] =
{
    0.0
};



static float conf_cal[] =
{
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 8.0, -8.0, 80.0
};



static float conf_vs_sim[FGC_SIM_NUM_DEN_LEN] =
{
    1.0, 0.0, 0.0, 0.0
};



static float conf_vs_delay = 0.001;

static uint32_t  log_oasis_sub = 1;

static uint32_t mode_slow_abort_faults = (  FGC_REG_FAULTS_B_ERR      | FGC_REG_FAULTS_B_MEAS | FGC_REG_FAULTS_I_ERR
                                          | FGC_REG_FAULTS_I_MEAS     | FGC_REG_FAULTS_I_RMS  | FGC_REG_FAULTS_I_RMS_LOAD
                                          | FGC_REG_FAULTS_V_RATE_RMS);

static uint16_t inter_fgc_sig = 0;

static uint16_t inter_fgc_isr = 400;

static char inter_fgc_none[] = "NONE";


static uint32_t ext_adc_type[] =
{
    FGC_ADC_EXT_TYPE_NONE,
    FGC_ADC_EXT_TYPE_NONE,
};



static uint32_t ext_adc_signal[] =
{
    FGC_ADC_SIGNALS_NONE,
    FGC_ADC_SIGNALS_NONE,
};



// ---------- External variable definitions

// List of properties to be initialized with default values

// CAUTION: If the property is NON-VOLATILE, its default size (numels in the
//          XML definition) must be zero for it to be initialised based on the
//          data in prop_class_init_nvs

// Do not put in the external RAM! Initialisation will stop working!

struct init_prop prop_class_init_nvs[] =
{
    // Non-volatile, non-config properties

    {   &PROP_MODE_OP,                          1,                      &conf_s[2]      },
    {   &PROP_MODE_SLOW_ABORT_FAULTS,           1,                      &mode_slow_abort_faults },
    {   &PROP_MEAS_SIM,                         1,                      &conf_l[1]      },
    {   &PROP_CAL_A_ADC_INTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_CAL_B_ADC_INTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_CAL_C_ADC_INTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_CAL_D_ADC_INTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_CAL_A_ADC_EXTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_CAL_B_ADC_EXTERNAL_ERR,           6,                      &conf_cal[0]    },
    {   &PROP_ADC_INTERNAL_LAST_CAL_TIME,       1,                      &conf_l[2]      },
    {   &PROP_FGC_NAME,                         sizeof(devtype),        &devtype[0]     },
    {   &PROP_DEVICE_TYPE,                      sizeof(devtype),        &devtype[0]     },
    {   &PROP_REF_COAST,                        1,                      &conf_s[0]      },

    // Global config properties

    {   &PROP_MODE_RT,                          1,                      &conf_l[0]      },
    {   &PROP_DEVICE_PPM,                       1,                      &conf_s[0]      },

    {   &PROP_CAL_A_ADC_INTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_A_ADC_INTERNAL_TC,            3,                      &conf_cal[0]    },
    {   &PROP_CAL_A_ADC_INTERNAL_DTC,           3,                      &conf_cal[0]    },

    {   &PROP_CAL_A_ADC_EXTERNAL_GAIN,          1,                      &conf_l[4]      },
    {   &PROP_CAL_A_ADC_EXTERNAL_ERR,           6,                      &conf_cal[0]    },

    {   &PROP_CAL_B_ADC_INTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_B_ADC_INTERNAL_TC,            3,                      &conf_cal[0]    },
    {   &PROP_CAL_B_ADC_INTERNAL_DTC,           3,                      &conf_cal[0]    },

    {   &PROP_CAL_B_ADC_EXTERNAL_GAIN,          1,                      &conf_l[4]      },
    {   &PROP_CAL_B_ADC_EXTERNAL_ERR,           6,                      &conf_cal[0]    },

    {   &PROP_CAL_C_ADC_INTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_C_ADC_INTERNAL_TC,            3,                      &conf_cal[0]    },
    {   &PROP_CAL_C_ADC_INTERNAL_DTC,           3,                      &conf_cal[0]    },

    {   &PROP_CAL_D_ADC_INTERNAL_GAIN,          1,                      &conf_l[3]      },
    {   &PROP_CAL_D_ADC_INTERNAL_TC,            3,                      &conf_cal[0]    },
    {   &PROP_CAL_D_ADC_INTERNAL_DTC,           3,                      &conf_cal[0]    },

    {   &PROP_CAL_A_DCCT_HEADERR,               1,                      &conf_cal[0]    },
    {   &PROP_CAL_A_DCCT_ERR,                   6,                      &conf_cal[0]    },
    {   &PROP_CAL_A_DCCT_TC,                    3,                      &conf_cal[0]    },
    {   &PROP_CAL_A_DCCT_DTC,                   3,                      &conf_cal[0]    },

    {   &PROP_CAL_B_DCCT_HEADERR,               1,                      &conf_cal[0]    },
    {   &PROP_CAL_B_DCCT_ERR,                   6,                      &conf_cal[0]    },
    {   &PROP_CAL_B_DCCT_TC,                    3,                      &conf_cal[0]    },
    {   &PROP_CAL_B_DCCT_DTC,                   3,                      &conf_cal[0]    },

    {   &PROP_CAL_VREF_ERR,                     6,                      &conf_cal[0]    },
    {   &PROP_CAL_VREF_TC,                      3,                      &conf_cal[0]    },

    {   &PROP_VS_SIM_NUM,                       FGC_SIM_NUM_DEN_LEN,    &conf_vs_sim[0] },
    {   &PROP_VS_SIM_DEN,                       FGC_SIM_NUM_DEN_LEN,    &conf_vs_sim[0] },

    {   &PROP_VS_ACT_DELAY_ITERS,               1,                      &conf_vs_delay  },
    {   &PROP_VS_I_LIMIT_GAIN,                  1,                      &conf_f[0]      },

    {   &PROP_LOG_OASIS_SUBSAMPLE,              1,                      &log_oasis_sub  },

    {   &PROP_BARCODE_FGC_CASSETTE,             0,                      NULL            },
    {   &PROP_BARCODE_A_DCCT_HEAD,              0,                      NULL            },
    {   &PROP_BARCODE_A_DCCT_ELEC,              0,                      NULL            },
    {   &PROP_BARCODE_B_DCCT_HEAD,              0,                      NULL            },
    {   &PROP_BARCODE_B_DCCT_ELEC,              0,                      NULL            },

    {   &PROP_BARCODE_A_EXADC_CASSETTE,         0,                      NULL            },
    {   &PROP_BARCODE_A_EXADC_ELEC,             0,                      NULL            },
    {   &PROP_BARCODE_A_EXADC_PSU,              0,                      NULL            },
    {   &PROP_BARCODE_A_EXADC_MB,               0,                      NULL            },
    {   &PROP_BARCODE_B_EXADC_CASSETTE,         0,                      NULL            },
    {   &PROP_BARCODE_B_EXADC_ELEC,             0,                      NULL            },
    {   &PROP_BARCODE_B_EXADC_PSU,              0,                      NULL            },
    {   &PROP_BARCODE_B_EXADC_MB,               0,                      NULL            },

    // Normal properties

    {   &PROP_CAL_A_DAC,                        3,                      &conf_cal[5]    },
    {   &PROP_CAL_B_DAC,                        3,                      &conf_cal[5]    },
    {   &PROP_ADC_INTERNAL_TAU_TEMP,            1,                      &conf_cal[8]    },
    {   &PROP_VS_SIM_INTLKS,                    1,                      &conf_s[1]      },

    {   &PROP_INTER_FGC_SLAVES,                 1,                      &inter_fgc_none[0] },
    {   &PROP_INTER_FGC_MASTER,                 1,                      &inter_fgc_none[0] },
    {   &PROP_INTER_FGC_CONSUMERS,              1,                      &inter_fgc_none[0] },
    {   &PROP_INTER_FGC_SIG_SOURCES,            1,                      &inter_fgc_none[0] },

    {   &PROP_INTER_FGC_PRODUCED_SIGS,          1,                      &inter_fgc_sig  },

    {   &PROP_INTER_FGC_BROADCAST,              1,                      &conf_s[0]      },
    {   &PROP_INTER_FGC_ISR_OFFSET_US,          1,                      &inter_fgc_isr  },
    {   &PROP_INTER_FGC_TOPOLOGY,               1,                      &inter_fgc_sig  },

    {   &PROP_ADC_EXTERNAL_TYPE,                FGC_N_EXT_ADCS,         ext_adc_type    },
    {   &PROP_ADC_EXTERNAL_SIGNALS,             FGC_N_EXT_ADCS,         ext_adc_signal  },

    {   NULL   }
};


uint32_t prop_class_init_spy_mpx[FGC_N_SPY_CHANS] =
{
    FGC_SPY_I_REF_DELAYED, FGC_SPY_I_MEAS,
    FGC_SPY_V_REF,         FGC_SPY_V_MEAS,
    FGC_SPY_I_DCCT_A,      FGC_SPY_I_DCCT_B
};


// The DF_TIMESTAMP_SELECT dynamic flag is set for all the properties in the
// following list and their children. The same list is also used in function
// CmdPrintTimestamp to read the timestamp selector.

struct Prop_class_init_dynflag_timestamp_select prop_class_init_dynflag_timestamp_select[] =
{
    { &PROP_REF_CYC,   TIMESTAMP_PREVIOUS_CYCLE },
    { &PROP_LOG_OASIS, TIMESTAMP_LOG_OASIS      },
    { &PROP_DIAG_ANA,  TIMESTAMP_DIMS           },
    { &PROP_DIAG_DATA, TIMESTAMP_DIMS           },
    { &PROP_DIAG_DIG,  TIMESTAMP_DIMS           },
    { NULL }
};


// EOF
