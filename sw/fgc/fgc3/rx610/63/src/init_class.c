//! @file     init_class.c
//! @brief    Class specific initialization functions


// ---------- Includes

#include <device.h>
#include <dpcom.h>



// ---------- Platform/class specific function definitions

void initClassPostNvs(void)
{
    device.max_sub_devs = (dpcom.mcu.device_multi_ppm == FGC_CTRL_ENABLED ? FGC_MAX_SUB_DEVS - 1 : 0);
    device.max_user     = (dpcom.mcu.device_ppm       == FGC_CTRL_ENABLED ? FGC_MAX_USER         : 0);
}



void initClassPostDb(void)
{
	// Do nothing
}



void initClassPostRegFgc3Cache(void)
{
	// Do nothing
}


// EOF
