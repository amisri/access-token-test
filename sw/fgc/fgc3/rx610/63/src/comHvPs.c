//! @file  comHvPs.c
//! Implementation of the VSNOCABLE input polling function for ComHV-PS power converter
//!
//! The file contains implementation of the external VSNOCABLE input polling function for ComHV-PS power converter,
//! as well as some internal functions - for decoding the signal and sending necessary commands to the terminal.
//!
//! Duty cycles and corresponding commands:
//! 0    % (0 ms)  - bus error
//! 16.7 % (5 ms)  - power converter on
//! 33.3 % (10 ms) - power converter off
//! 50   % (15 ms) - power converter reset and on
//! 66.7 % (20 ms) - power converter reset and off
//! 83.3 % (25 ms) - external control disabled
//! 100  % (30 ms) - bus error
//!
//! PWM cycle duration is 30ms.


// ---------- Includes

#include <stdint.h>

#include <bitmap.h>
#include <comHvPs.h>
#include <defconst.h>
#include <fbs_class.h>
#include <fgc_runlog_entries.h>
#include <logRun.h>
#include <memmap_mcu.h>
#include <stateVs.h>
#include <status.h>
#include <trm.h>



// ---------- Constants

//! VSNOCABLE pulse-width modulation period (in milliseconds)

#define COMHVPS_VSNOCABLE_PWM_PERIOD                     30

//! Spacing between subsequent encoded states (in milliseconds)

#define COMHVPS_VSNOCABLE_PWM_SPACING_BETWEEN_STATES     5

//! Allowed offset of the PWM period

#define COMHVPS_VSNOCABLE_PWM_ALLOWED_OFFSET             1

//! Number of invalid periods after which an error should be signalized

#define COMHVPS_VSNOCABLE_PWM_ALLOWED_INVALID_PERIODS    3

//! Maximal valid PWM pulse duration (in milliseconds)

#define COMHVPS_VSNOCABLE_PWM_PULSE_UPPER_BOUND          ( COMHVPS_VSNOCABLE_PWM_PERIOD - 4 )

//! Mask which can be used to get only the three signal values (without any errors) from the property

#define COMHVPS_VSNOCABLE_MASK_SIGNALS  ( FGC_EXT_CONTROL_SIGNAL_NOEXT | FGC_EXT_CONTROL_SIGNAL_RST | FGC_EXT_CONTROL_SIGNAL_OFF )



// ---------- External structures, unions and enumerations

//! Contains indexes of different commands in the commands array.

enum ComHvPs_vsnocable_cmd
{
    COMHVPS_VSNOCABLE_CMD_RESET,        //! Reset command index in the commands array.
    COMHVPS_VSNOCABLE_CMD_POWER_ON,     //! Power on command index in the commands array.
    COMHVPS_VSNOCABLE_CMD_POWER_OFF,    //! Power off command index in the commands array.
};


//! Used to pass decoded signal values.
//!
//! The enum encodes three signals - OFF, RST, NOEXT. Each of them is saved on a single bit (OFF - LSB, RST - LSB+1, NOEXT - LSB+2).

enum ComHvPs_vsnocable_state
{
    COMHVPS_VSNOCABLE_STATE_NORST_ON,    //! State 1: OFF = 0, RST = 0, NOEXT = 0
    COMHVPS_VSNOCABLE_STATE_NORST_OFF,   //! State 2: OFF = 1, RST = 0, NOEXT = 0
    COMHVPS_VSNOCABLE_STATE_RST_ON,      //! State 3: OFF = 0, RST = 1, NOEXT = 0
    COMHVPS_VSNOCABLE_STATE_RST_OFF,     //! State 4: OFF = 1, RST = 1, NOEXT = 0
    COMHVPS_VSNOCABLE_STATE_NOEXT,       //! State 5: OFF = 0, RST = 0, NOEXT = 1
    COMHVPS_VSNOCABLE_STATE_UNKNOWN,     //! Unknown state
};


//! Enumerates errors which can be signalized by the polling function.
//!
//! Enumeration starts from 1 because 0 means that everything is fine.

enum ComHvPs_vsnocable_error
{
    COMHVPS_VSNOCABLE_ERROR_UNKNOWN_CMD = 1,    //! An unknown command was read from input
    COMHVPS_VSNOCABLE_ERROR_BUS_FAULT,          //! Signalizes broken bus
};



//! Array of possible external commands
//!
//! The array holds strings that can be send to the terminal queue in order to simulate external commands

char const * const vsnocable_ext_commands[] =
{
    "!S VS RESET;",                 // Power converter reset
    "!S MODE.PC_SIMPLIFIED ON;",    // Power converter on
    "!S MODE.PC_SIMPLIFIED OFF;"    // Power converter off
};



// ---------- Internal function definitions

static void comHvPsSetVsCommsWarning(uint16_t const error)
{
    // Do not set the status if there's no converter present,
    // like it is the case with remote measurement FGCs.
    // Also, prevent flooding of the run log

    if (vs.present == FGC_CTRL_ENABLED)
    {
        logRunAddEntry(FGC_RL_STATE_COMHV_CMD, &error);

        statusSetLatched(FGC_LAT_VS_COMMS);
    }
}



//! Sends command strings to the terminal message queue.
//!
//! The function gets a pointer to a string literal and sends it, character by character,
//! to the terminal message queue. It does not perform any validation of the given strings.
//!
//! @param[in] command A pointer to the command string literal

static void comHvPsSendTermCommand(char const * const command)
{
    char const * c = command;

    while (*c)
    {
        // Post characters to the terminal queue
        // Character value SHOULD be saved directly in the pointer
        // Two casts suppress compiler warnings
        OSMsgPost(terminal.msgq, (void *)(intptr_t) *c++);
    }
}



//! Decodes signals based on pulse duration.
//!
//! The function decodes three signals (OFF, RST, NOEXT), based on the given high pulse duration.
//! Signals values are on three least significant bits of the return value. If the pulse duration is in the range <TIME-1, TIME+1>,
//! where TIME is one of the defined values (5, 10, 15, 20, 25), then the function returns corresponding state.
//! Otherwise an unknown state is returned.
//!
//! @param[in] pulse_duration High pulse duration in milliseconds.
//!
//! @return Decoded signals presented as integer bits.

static enum ComHvPs_vsnocable_state comHvPsDecodePwmSignalState(uint16_t const pulse_duration)
{
    // The pulse duration is an invalid value if it satisfies the equation:
    // (pulse_duration - allowed_offset - 1) % spacing_between_states < spacing_between_states - (2 * allowed_offset + 1)
    // In our case allowed_offset = 1 and spacing_between_states = 5, so:
    // (pulse_duration - 2) % 5 < 2
    // All values greater than 26ms are also invalid.

    if ((pulse_duration > COMHVPS_VSNOCABLE_PWM_PULSE_UPPER_BOUND) ||
        ((pulse_duration - COMHVPS_VSNOCABLE_PWM_ALLOWED_OFFSET - 1) % COMHVPS_VSNOCABLE_PWM_SPACING_BETWEEN_STATES) <
        (COMHVPS_VSNOCABLE_PWM_SPACING_BETWEEN_STATES - (2 * COMHVPS_VSNOCABLE_PWM_ALLOWED_OFFSET + 1)))
    {
        return COMHVPS_VSNOCABLE_STATE_UNKNOWN;
    }

    return (pulse_duration - COMHVPS_VSNOCABLE_PWM_ALLOWED_OFFSET - 1) / COMHVPS_VSNOCABLE_PWM_SPACING_BETWEEN_STATES;
}



//! Triggers commands corresponding to the VSNOCABLE signal changes.
//!
//! This function is responsible for triggering commands which corresponds to the changes
//! in the VSNOCABLE input. There are three signals: OFF, RST, NOEXT. When OFF changes, power on/off
//! command is sent to the terminal queue. Reset command is triggered on the rising edge of RST.
//! When NOEXT is high no commands are sent. On the falling edge of NOEXT two commands are always triggered.
//! First, the converter is reset. After that either powered on or off command is sent, based on the current OFF value.
//!
//! Previous values of the three signals are saved as three least significant bits in vs.ext_control variable.
//!
//! @param[in] current_state Current state based on which commands should be triggered.

static void comHvPsTriggerStateCommands(enum ComHvPs_vsnocable_state const current_state)
{
    static bool error_signalized = FALSE;

    // Signalize an error if the state is unknown

    if (current_state == COMHVPS_VSNOCABLE_STATE_UNKNOWN)
    {
        if (!error_signalized)
        {
            error_signalized = TRUE;

            comHvPsSetVsCommsWarning(COMHVPS_VSNOCABLE_ERROR_UNKNOWN_CMD);

            setBitmap(vs.ext_control, FGC_EXT_CONTROL_UNKNOWN_CMD);
        }

        return;
    }

    clrBitmap(vs.ext_control, FGC_EXT_CONTROL_UNKNOWN_CMD);

    error_signalized = FALSE;

    // If the state has changed

    if ((vs.ext_control & COMHVPS_VSNOCABLE_MASK_SIGNALS) ^ current_state)
    {
        // If external control is enabled

        if (!(current_state & FGC_EXT_CONTROL_SIGNAL_NOEXT))
        {
            // If falling edge of NOEXT

            if (vs.ext_control & FGC_EXT_CONTROL_SIGNAL_NOEXT)
            {
                // Send reset command

                comHvPsSendTermCommand(vsnocable_ext_commands[COMHVPS_VSNOCABLE_CMD_RESET]);

                // Send on/off command based on current state

                if (current_state & FGC_EXT_CONTROL_SIGNAL_OFF)
                {
                    comHvPsSendTermCommand(vsnocable_ext_commands[COMHVPS_VSNOCABLE_CMD_POWER_OFF]);
                }
                else
                {
                    comHvPsSendTermCommand(vsnocable_ext_commands[COMHVPS_VSNOCABLE_CMD_POWER_ON]);
                }
            }
            else
            {
                // Reset if RST went from 0 to 1

                if (current_state & ~vs.ext_control & FGC_EXT_CONTROL_SIGNAL_RST)
                {
                    comHvPsSendTermCommand(vsnocable_ext_commands[COMHVPS_VSNOCABLE_CMD_RESET]);
                }

                // Power off if OFF went from 0 to 1

                if (current_state & ~vs.ext_control & FGC_EXT_CONTROL_SIGNAL_OFF)
                {
                    comHvPsSendTermCommand(vsnocable_ext_commands[COMHVPS_VSNOCABLE_CMD_POWER_OFF]);
                }

                // Power on if OFF went from 1 to 0

                else if (~current_state & vs.ext_control & FGC_EXT_CONTROL_SIGNAL_OFF)
                {
                    comHvPsSendTermCommand(vsnocable_ext_commands[COMHVPS_VSNOCABLE_CMD_POWER_ON]);
                }
            }
        }

        // Save current state but also keep all errors
        vs.ext_control = (vs.ext_control & ~COMHVPS_VSNOCABLE_MASK_SIGNALS) | (current_state &
                         COMHVPS_VSNOCABLE_MASK_SIGNALS);
    }
}



// ---------- External function definitions

void comHvPsPollVsNoCable(void)
{
    static int16_t  pulse_duration = 0;     // High pulse duration (from VSNOCABLE input) in milliseconds
    // If at the end of a period this value is:
    // Negative - received signal is ok (absolute value is the high pulse duration)
    // Zero     - either syncronization loss or broken bus (low input through the entire period)
    // Positive - the same as above (but input high through the entire period)

    static uint16_t counter        = 0;     // Current millisecond in actual PWM period
    static uint16_t error_counter  = 0;     // Used to count all-low or all-high periods

    bool input = testBitmap(DIG_IP_P, DIG_IP_VSNOCABLE_MASK32);  // Current input value


    // If it is a rising edge
    if (input && pulse_duration <= 0)
    {
        // If the period length falls out of the allowed range, set a flag in the property

        if (counter < COMHVPS_VSNOCABLE_PWM_PERIOD - COMHVPS_VSNOCABLE_PWM_ALLOWED_OFFSET && pulse_duration < 0)
        {
            setBitmap(vs.ext_control, FGC_EXT_CONTROL_OFFSET_NEG);
        }
        else if (pulse_duration == 0 && counter > COMHVPS_VSNOCABLE_PWM_ALLOWED_OFFSET)
        {
            setBitmap(vs.ext_control, FGC_EXT_CONTROL_OFFSET_POS);
        }

        // Clear the warnings if the period length falls into the allowed range

        else
        {
            clrBitmap(vs.ext_control, FGC_EXT_CONTROL_OFFSET_POS);
            clrBitmap(vs.ext_control, FGC_EXT_CONTROL_OFFSET_NEG);
        }

        // Synchronize with input (start a new period)
        counter = COMHVPS_VSNOCABLE_PWM_PERIOD;
    }

    // If it is a falling edge
    else if (!input && pulse_duration > 0)
    {
        // If there was a long (more than one period) high-state time
        if (pulse_duration >= COMHVPS_VSNOCABLE_PWM_PERIOD)
        {
            // Synchronize with the input, but signalize that the signal is invalid
            pulse_duration = 0;
        }

        // If everything works as it should
        else
        {
            // Signalize that both, rising and falling, edges occured and it is safe to decode the signal
            pulse_duration = -pulse_duration;
        }
    }

    // If the period has finished
    if (counter >= COMHVPS_VSNOCABLE_PWM_PERIOD)
    {
        // If both, rising and falling, edges were registered
        if (pulse_duration < 0)
        {
            // Clear all warnings and errors

            clrBitmap(vs.ext_control, FGC_EXT_CONTROL_STUCK_LOW);
            clrBitmap(vs.ext_control, FGC_EXT_CONTROL_STUCK_HIGH);

            error_counter = 0;

            // Trigger necessary commands
            comHvPsTriggerStateCommands(comHvPsDecodePwmSignalState(-pulse_duration));

            // Reset the pulse duration counter
            pulse_duration = 0;
        }

        // If something went wrong
        else
        {
            // If there was no falling edge or if the pulse was high through the entire period
            if (pulse_duration > 0)
            {
                // Set the pulse duration to period length. If the input will be high through the next period
                // the pulse duration counter will be equal to two period lengths before this assignment.
                pulse_duration = COMHVPS_VSNOCABLE_PWM_PERIOD;
            }

            if (   ++error_counter >= COMHVPS_VSNOCABLE_PWM_ALLOWED_INVALID_PERIODS
                && statusTestLatched(FGC_LAT_VS_COMMS) == false)
            {
                if (pulse_duration == 0)
                {
                    setBitmap(vs.ext_control, FGC_EXT_CONTROL_STUCK_LOW);
                }
                else
                {
                    setBitmap(vs.ext_control, FGC_EXT_CONTROL_STUCK_HIGH);
                }

                comHvPsSetVsCommsWarning(COMHVPS_VSNOCABLE_ERROR_BUS_FAULT);
            }
        }

        // Reset the counter
        counter = 0;
    }

    // If the input is high

    if (input)
    {
        // Increase the high pulse duration counter
        pulse_duration++;
    }

    // Increase the counter
    counter++;
}


// EOF
