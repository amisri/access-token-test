//! @file   rtd_class.c
//! @brief  Class FGC_63 specific source file


// ---------- Includes

#include <function_class.h>
#include <rtd.h>
#include <dpcls.h>
#include <dpcom.h>
#include <bitmap.h>
#include <polSwitch.h>
#include <modePc.h>



// ---------- Constants

#define RTD_POS_V_REF           "21;34"
#define RTD_POS_IB_REF          "22;34"
#define RTD_POS_MEAS_V          "21;46"
#define RTD_POS_MEAS_I          "22;46"
#define RTD_POS_MEAS_B          "23;46"
#define RTD_POS_I_RATE          "23;34"
#define RTD_POS_I_DIFF          "24;34"
#define RTD_POS_REF_TYPE        "24;49"
#define RTD_POS_POL_SW          "24;21"

#define RTD_POS_FUNC_TYPE_V     "21;28"
#define RTD_POS_FUNC_TYPE_I     "22;28"
#define RTD_POS_FUNC_TYPE_B     "22;28"

#define RTD_POS_VA              "21;59"
#define RTD_POS_VB              "22;59"
#define RTD_POS_VC              "23;59"
#define RTD_POS_VD              "24;59"
#define RTD_POS_EA              "21;72"
#define RTD_POS_EB              "22;72"

#define RTD_POS_ADCS_L_VOLTS    "20;11"
#define RTD_POS_ADCS_L_PP       "20;22"
#define RTD_POS_ADCS_L_AMPS     "20;30"
#define RTD_POS_ADCS_R_VOLTS    "20;52"
#define RTD_POS_ADCS_R_PP       "20;63"
#define RTD_POS_ADCS_R_AMPS     "20;71"
#define RTD_POS_ADCS_L_RAW      "20;01"
#define RTD_POS_ADCS_R_RAW      "20;42"

#define RTD_POS_CYC             "20;1"



// ---------- Internal function declarations

static bool rtdClassPolSwitch (bool reset);
static bool rtdClassRefFunc   (bool reset);
static bool rtdClassRegMode   (bool reset);
static bool rtdClassOptionCyc (bool reset);
static bool rtdOptionAdcsAB   (bool reset);
static bool rtdOptionAdcsCD   (bool reset);
static bool rtdPrintAdcsHelper(struct Rtd_field_float * adc_floats, struct Rtd_field_int * adc_ints, bool reset);



// ---------- Internal variable definitions

static struct Rtd_field_func rtd_class_field_funcs[] =
{
    { rtdClassRegMode   },
    { rtdClassPolSwitch },
    { rtdClassRefFunc   },
    { NULL              },
};


static struct Rtd_field_int rtd_class_field_ints[] =
{
    { (int32_t *)&dpcls.dsp.meas.i_diff_ma, 0x7FFFFFFF, "%6d", RTD_POS_I_DIFF },
    { NULL                                                                    },
};


static struct Rtd_field_float rtd_class_field_floats[] =
{
    { (float *)&dpcls.dsp.ref.v             , "%*.2f", 0x6FFFFFFF, 100,    RTD_POS_V_REF , 9 },
    { (float *)&dpcls.dsp.ref.i             , "%*.2f", 0x6FFFFFFF, 100,    RTD_POS_IB_REF, 9 },
    { (float *)&dpcls.dsp.meas.i_rate       , "%*.2f", 0x6FFFFFFF, 100,    RTD_POS_I_RATE, 9 },
    { NULL                                                                                   },
};


static struct Rtd_field_error_float rtd_class_field_floats_errors[] =
{
    // Order is important as rtdClassUpdateErrors() depends on it

    { { (float *)&dpcom.dsp.adc.volts_200ms[0], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_VA    , 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcom.dsp.adc.volts_200ms[1], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_VB    , 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcom.dsp.adc.volts_200ms[2], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_VC    , 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcom.dsp.adc.volts_200ms[3], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_VD    , 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcom.dsp.adc.volts_200ms[4], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_EA    , 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcom.dsp.adc.volts_200ms[5], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_EB    , 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcls.dsp.meas.v            , "%*.2f", 0x6FFFFFFF, 100,    RTD_POS_MEAS_V, 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcls.dsp.meas.i            , "%*.2f", 0x6FFFFFFF, 100,    RTD_POS_MEAS_I, 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcls.dsp.meas.b            , "%*.2f", 0x6FFFFFFF, 100,    RTD_POS_MEAS_B, 9 }, { 0x00, 0xFF } },
    { { NULL                                                                                                   } },
};


static struct Rtd_field_optional rtd_class_field_optional[] =
{
    { FGC_RTD_CYC,     rtdClassOptionCyc },
    { FGC_RTD_ADCS_AB, rtdOptionAdcsAB   },
    { FGC_RTD_ADCS_CD, rtdOptionAdcsCD   },
    { 0xFF,            NULL              },
};


static struct Rtd_process rtd_class_process[] =
{
    { rtdProcessFuncs,        { (void *)rtd_class_field_funcs,         (void *)rtd_class_field_funcs         } },
    { rtdProcessFloats,       { (void *)rtd_class_field_floats,        (void *)rtd_class_field_floats        } },
    { rtdProcessFloatsErrors, { (void *)rtd_class_field_floats_errors, (void *)rtd_class_field_floats_errors } },
    { rtdProcessInts,         { (void *)rtd_class_field_ints,          (void *)rtd_class_field_ints          } },
    { rtdProcessOptionLine,   { (void *)rtd_class_field_optional,      (void *)rtd_class_field_optional      } },
    { NULL                                                                                                     }
};



// ---------- Platform/class specific function definitions

void rtdClassInit(void)
{
    if (modePcGetPcOn() == FGC_PC_ON_CYCLING)
    {
        rtd.ctrl = FGC_RTD_CYC;
    }
}



void rtdClassReset(FILE * f)
{
    // Move to RTD display area

    fputs(TERM_CSI "21;1" TERM_GOTO, f);

    //    0         10        20        30        40        50        60        70        80
    //    |---------+---------+---------+---------+---------+---------+---------+---------+|

    fputs("States: __.__.__.__        Vref: ______.__ V:______.__ Va:___._____ Ea:___._____\n\r", f);
    fputs("                           Iref: ______.__ I:______.__ Vb:___._____ Eb:___._____\n\r", f);
    fputs("CFG:                       Irate:______.__ B:______.__ Vc:___._____ CPU:___%___%\n\r", f);
    fputs("PERMIT:       PolSw:       Idiff:______ mA FUNC:       Vd:___._____  __._C __._C", f);

    // Set up scroll window to protect RTD lines

    fputs(TERM_HOME "\33[1;19r\v", f);

    // Reset func fields

    struct Rtd_field_func * field_func;

    for (field_func = &rtd_class_field_funcs[0]; field_func->func != NULL; field_func++)
    {
        field_func->func(true);
    }

    // Reset int fields

    struct Rtd_field_int * field_int;

    for (field_int = &rtd_class_field_ints[0]; field_int->source != NULL; field_int++)
    {
        field_int->last_value = 0x7FFFFFFF;
    }

    // Reset float fields

    struct Rtd_field_float * field_float;

    for (field_float = &rtd_class_field_floats[0]; field_float->source != NULL; field_float++)
    {
        field_float->last_value = 0x6FFFFFFF;
    }

    struct Rtd_field_error_float * field_error_float;

    for (field_error_float = &rtd_class_field_floats_errors[0]; field_error_float->field.source != NULL; field_error_float++)
    {
        field_error_float->field.last_value = 0x6FFFFFFF;
        field_error_float->error.last_value = 0xFF;
    }

    // Reset all option line variables

    struct Rtd_field_optional * field;

    for (field = &rtd_class_field_optional[0]; field->func != NULL; field++)
    {
        field->func(true);
    }
}



bool rtdClassProcess(bool reset)
{
    static struct Rtd_process * prev_process = rtd_class_process;

    struct Rtd_process * field   = prev_process;
    bool                 updated = false;

    for (;  field->func != NULL && updated == false; field++)
    {
        updated = field->func(&field->args);
    }

    prev_process = (field->func == NULL ? rtd_class_process : field);

    return updated;
}



void rtdClassUpdateErrors(void)
{
    uint8_t   idx;

    for (idx = 0; idx < FGC_N_INT_ADCS; idx++)
    {
        if (testBitmap(dpcom.dsp.meas.adc_state[idx], FGC_ADC_STATUS_IN_USE) == false)
        {
            rtd_class_field_floats_errors[idx].error.value = RTD_VALUE_NO_PRINT;
        }
        else if (testBitmap(dpcom.dsp.meas.adc_state[idx], FGC_ADC_STATUS_V_MEAS_OK) == false)
        {
            rtd_class_field_floats_errors[idx].error.value = RTD_VALUE_FAULT;
        }
        else
        {
            rtd_class_field_floats_errors[idx].error.value = 0;
        }
    }

    // V measurement

    if (testBitmap(dpcls.dsp.status.meas_v, FGC_DCCT_FLTS_ADC_NOT_AVL) == true)
    {
        rtd_class_field_floats_errors[6].error.value = RTD_VALUE_NO_PRINT;
    }
    else if (testBitmap(dpcls.dsp.status.meas_v, FGC_DCCT_FLTS_ADC_FAULT) == true)
    {
        rtd_class_field_floats_errors[6].error.value = RTD_VALUE_FAULT;
    }
    else
    {
        rtd_class_field_floats_errors[6].error.value = 0;
    }

    // I measurement

    if (testBitmap(dpcls.dsp.status.meas_i, FGC_DCCT_FLTS_ADC_NOT_AVL) == true)
    {
        rtd_class_field_floats_errors[7].error.value = RTD_VALUE_NO_PRINT;
    }
    else if (testBitmap(dpcls.dsp.status.meas_i, FGC_DCCT_FLTS_DCCT_FAULT) == true)
    {
        rtd_class_field_floats_errors[7].error.value = RTD_VALUE_FAULT;
    }
    else
    {
        rtd_class_field_floats_errors[7].error.value = 0;
    }

    // B measurement

    if (testBitmap(dpcls.dsp.status.meas_b, FGC_DCCT_FLTS_ADC_NOT_AVL) == true)
    {
        rtd_class_field_floats_errors[8].error.value = RTD_VALUE_NO_PRINT;
    }
    else if (testBitmap(dpcls.dsp.status.meas_b, FGC_DCCT_FLTS_DCCT_FAULT) == true)
    {
        rtd_class_field_floats_errors[8].error.value = RTD_VALUE_FAULT;
    }
    else
    {
        rtd_class_field_floats_errors[8].error.value = 0;
    }
}



// ---------- Internal function definitions

static bool rtdClassRegMode(bool reset)
{
    static uint32_t         prev_reg_mode  = FGC_REG_NONE;
    static char     const * reg_mode_pos[] = { RTD_POS_FUNC_TYPE_V, RTD_POS_FUNC_TYPE_I, RTD_POS_FUNC_TYPE_B };
    static char     const   reg_mod_ch[]   = { 'V', 'I', 'B' };
    static bool             update_prev_reg_mode = true;

    // Reset reg mode

    if (reset == true)
    {
        prev_reg_mode = FGC_REG_NONE;

        return false;
    }

    uint32_t reg_mode = dpcls.dsp.ref.reg_mode;

    if (prev_reg_mode == reg_mode)
    {
        return false;
    }

    if (update_prev_reg_mode == true)
    {
        if (prev_reg_mode != FGC_REG_NONE)
        {
            // Restore previous regulation mode

            rtdMoveCursor(reg_mode_pos[prev_reg_mode], "");

            // If leaving B regulation mode, revert to I

            if (prev_reg_mode == FGC_REG_B)
            {
                fprintf(rtd.f, "%c", reg_mod_ch[FGC_REG_I]);
            }
            else
            {
                fprintf(rtd.f, "%c", reg_mod_ch[prev_reg_mode]);
            }

            rtdRestoreCursor();
        }

        update_prev_reg_mode = false;
    }
    else
    {
        if (reg_mode != FGC_REG_NONE)
        {
            // Update current regulation mode

            rtdMoveCursor(reg_mode_pos[reg_mode], TERM_BOLD TERM_REVERSE);

            fprintf(rtd.f, "%c", reg_mod_ch[reg_mode]);

            if (reg_mode == FGC_REG_I)
            {
                rtd_class_field_floats[1].source = (float *)&dpcls.dsp.ref.i;
            }
            else if (reg_mode == FGC_REG_B)
            {
                rtd_class_field_floats[1].source = (float *)&dpcls.dsp.ref.b;
            }

            rtdRestoreCursor();
        }

        update_prev_reg_mode = true;

        prev_reg_mode = reg_mode;
    }

    return (prev_reg_mode != FGC_REG_NONE);
}



static bool rtdClassPolSwitch(bool reset)
{
    static uint32_t                prev_pol_switch_state = 0xFFFFFFFF;
    static struct Rtd_field_string field_str             = { "", RTD_POS_POL_SW, TERM_BOLD };

    // Reset faults

    if (reset == true)
    {
        prev_pol_switch_state = 0xFFFFFFFF;
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));

        return false;
    }

    uint32_t pol_switch_state = polSwitchGetState();

    if (prev_pol_switch_state == pol_switch_state)
    {
        return false;
    }

    prev_pol_switch_state = pol_switch_state;

    switch (pol_switch_state)
    {
        case FGC_POL_SWITCH_MOVING:
            return rtdPrintString("MOVE ", &field_str);

        case FGC_POL_POSITIVE:
            return rtdPrintString("POS  ", &field_str);

        case FGC_POL_NEGATIVE:
            return rtdPrintString("NEG  ", &field_str);

        case FGC_POL_SWITCH_FAULT:
            return rtdPrintString("FAULT", &field_str);

        case FGC_POL_NO_SWITCH:
            return rtdPrintString("NONE ", &field_str);

        default:
            return false;
    }
}



static bool rtdClassRefFunc(bool reset)
{
    static struct Rtd_field_string field_str = { "", RTD_POS_REF_TYPE, TERM_BOLD };

    // Reset faults

    if (reset == true)
    {
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));

        return false;
    }

    // Reference function type (Cycling and non-Cycling)

    char ref[7];

    strncpy(ref, functionClassGetStr(), 6);

    ref[6] = '\0';

    return rtdPrintString(ref, &field_str);
}



static bool rtdClassOptionCyc(bool reset)
{
    static struct Rtd_field_string field_str = { "", RTD_POS_CYC, TERM_BOLD };
    static char                    ref_units[] = "VAG ";   // volts, amps, gauss, none
    static char                    cyc_str[27];


    if (reset == true)
    {
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));
        return false;
    }

    snprintf(cyc_str, 27, "PPM: %2u-%-2u Peak: %6d %c", (int)dpcom.dsp.cycle.current.sub_sel_acq, 
                                                        (int)dpcom.dsp.cycle.current.cyc_sel_acq, 
                                                        (int)dpcls.dsp.ref.peak, 
                                                        ref_units[dpcls.dsp.ref.reg_mode]);

    return rtdPrintString(cyc_str, (void *) &field_str);
}



static bool rtdOptionAdcsAB(bool reset)
{
    static struct Rtd_field_float adc_floats[] =
    {
        { (float *)&dpcom.dsp.adc.volts_1s   [0], "%*.6f", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_L_VOLTS, 10 },
        { (float *)&dpcom.dsp.adc.pp_volts_1s[0], "%*.1E", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_L_PP   , 7  },
        { (float *)&dpcom.dsp.adc.cal_meas_1s[0], "%*.3f", 0x6FFFFFFF, 1000   , RTD_POS_ADCS_L_AMPS , 10 },
        { (float *)&dpcom.dsp.adc.volts_1s   [1], "%*.6f", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_R_VOLTS, 10 },
        { (float *)&dpcom.dsp.adc.pp_volts_1s[1], "%*.1E", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_R_PP   , 7  },
        { (float *)&dpcom.dsp.adc.cal_meas_1s[1], "%*.3f", 0x6FFFFFFF, 1000   , RTD_POS_ADCS_R_AMPS , 10 },
        { NULL                                                                                                       },
    };

    static struct Rtd_field_int adc_ints[] =
    {
        { (int32_t *)&dpcom.dsp.adc.raw_1s[0], 0x7FFFFFFF, "%9ld", RTD_POS_ADCS_L_RAW },
        { (int32_t *)&dpcom.dsp.adc.raw_1s[1], 0x7FFFFFFF, "%9ld", RTD_POS_ADCS_R_RAW },
        { NULL                                                                        },
    };

    return (rtdPrintAdcsHelper(adc_floats, adc_ints, reset));
}



static bool rtdOptionAdcsCD(bool reset)
{
    static struct Rtd_field_float adc_floats[] =
    {
        { (float *)&dpcom.dsp.adc.volts_1s   [2], "%*.6f", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_L_VOLTS, 10 },
        { (float *)&dpcom.dsp.adc.pp_volts_1s[2], "%*.1E", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_L_PP   , 7  },
        { (float *)&dpcom.dsp.adc.cal_meas_1s[2], "%*.3f", 0x6FFFFFFF, 1000   , RTD_POS_ADCS_L_AMPS , 10 },
        { (float *)&dpcom.dsp.adc.volts_1s   [3], "%*.6f", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_R_VOLTS, 10 },
        { (float *)&dpcom.dsp.adc.pp_volts_1s[3], "%*.1E", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_R_PP   , 7  },
        { (float *)&dpcom.dsp.adc.cal_meas_1s[3], "%*.3f", 0x6FFFFFFF, 1000   , RTD_POS_ADCS_R_AMPS , 10 },
        { NULL                                                                                                       },
    };

    static struct Rtd_field_int adc_ints[] =
    {
        { (int32_t *)&dpcom.dsp.adc.raw_1s[2], 0x7FFFFFFF, "%9ld", RTD_POS_ADCS_L_RAW },
        { (int32_t *)&dpcom.dsp.adc.raw_1s[3], 0x7FFFFFFF, "%9ld", RTD_POS_ADCS_R_RAW },
        { NULL                                                                        },
    };

    return (rtdPrintAdcsHelper(adc_floats, adc_ints, reset));
}



static bool rtdPrintAdcsHelper(struct Rtd_field_float * adc_floats, struct Rtd_field_int * adc_ints, bool reset)
{
    static uint8_t adc_xy_idx = 0;

    if (reset == true)
    {
        struct Rtd_field_float * field_float = adc_floats;

        for (; field_float->source != NULL; field_float++)
        {
            field_float->last_value = 0x6FFFFFFF;
        }

        struct Rtd_field_int * field_int = adc_ints;

        for (; field_int->source != NULL; field_int++)
        {
            field_int->last_value = 0x7FFFFFFF;
        }

        adc_xy_idx = 0;

        return false;
    }

    bool updated;

    if (adc_xy_idx < 2)
    {
        updated = rtdPrintInt((void *)&adc_ints[adc_xy_idx]);
    }
    else
    {
        updated = rtdPrintFloat((void *)&adc_floats[adc_xy_idx - 2], NULL);
    }

    if (++adc_xy_idx >= 8)
    {
        adc_xy_idx = 0;
    }

    return updated;
}


// EOF
