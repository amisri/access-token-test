//! @file   digitalIo_class.c
//! @brief  Class specific digital inputs and outputs handling


// ---------- Includes

#include <stdint.h>

#include <crate.h>
#include <digitalIo.h>
#include <dpcls.h>
#include <mcu.h>



// ---------- Platform/class specific function definitions

void digitalIoSupInitClass(void)
{
	// Based on
	// https://wikis.cern.ch/display/TEEPCCCE/FGC3+Definition+of+DIG-SUP+with+RegFGC3+cards
    //
    // ----------------------------------
    // Generic configuration
    // ----------------------------------
    // CHANNEL   DIG_SUP_A	                           DIG_SUP_B
    //    0      White Rabbit Tx                  	   White Rabbit Rx or HPM7177 A Rx
    //    1      PWM Enable	                           PBAM1 or HPM7177 B Rx
    //    2                       	                   PBAM2
    //    3      SYNC PWM witch C0
    //    4      HPM ADC7177 clock (TBD with HPM)	   External Timing
    //
    // ----------------------------------
    // Specific configuration
    // ----------------------------------
    // COMMERCIAL   : DIG_SUP_A and DIG_SUP_B must be set as outputs to prevent faults,
    //                (VS_FAULT) from being latched
    // POPS-B     A2: 1KHz sync pulse
    // Thyristor  A0: Encode firing mode
    //            A1: Encode firing mode
    // HPM7177    A0: 10 KHz sync pulse channel A
    //            A1: 10 KHz sync pulse channel B
    // ----------------------------------

    if (crateGetType() == FGC_CRATE_TYPE_COMRCIAL_PC1)
    {
        // Bank A is already configured as output in digitalIo.c

        setBitmap(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_CTRL_MASK16);
    }
    else
    {
        // Generate a pulse of 20 us on A3 at start of cycle to synchronize the PWMs

        DIG_SUP_A_CHAN_CTRL_A     [DIG_SUP_A_CHANNEL3] = DIG_SUP_CTRL_OUTMPX_PULSE_C0;
        DIG_SUP_A_PULSE_WIDTH_US_A[DIG_SUP_A_CHANNEL3] = 20;

        // POPS-B

        if (crateGetPopsB() == true)
        {
            DIG_SUP_A_CHAN_CTRL_A     [DIG_SUP_A_CHANNEL2] = DIG_SUP_CTRL_OUTMPX_1KHZ_SYNC;
            DIG_SUP_A_PULSE_WIDTH_US_A[DIG_SUP_A_CHANNEL2] = 20;

            // Set delay of the pulse with respect to start of millisecond

            TICK_EXT_SYNC_DELAY_US_P = 670;

            // Enable sync generation in the tick control

            mcuSetExtSyncEnable();
        }

        // THYRISTOR

        if (crateGetType() == FGC_CRATE_TYPE_DSP_THYRISTOR)
        {
            // Thyristor converter use A0 and A1 to encode the firing mode

            DIG_SUP_A_CHAN_CTRL_A[DIG_SUP_A_CHANNEL0] = DIG_SUP_CTRL_OUTMPX_DATA;
            DIG_SUP_A_CHAN_CTRL_A[DIG_SUP_A_CHANNEL1] = DIG_SUP_CTRL_OUTMPX_DATA;
        }
        else
        {
            // Enable serial link on channel A_0 for data transmission with he the POF card

            DIG_SUP_A_CHAN_CTRL_A[DIG_SUP_A_CHANNEL0] = DIG_SUP_CTRL_OUTMPX_SERIAL;
            DIG_SUP_B_CHAN_CTRL_A[DIG_SUP_B_CHANNEL0] = DIG_SUP_CTRL_OUTMPX_DATA;
        
            // Override above if external ADC type is HPM7177, which requires a 10 KHz sync pulse.

            if (dpcls.mcu.adc.ext_type[0] == FGC_ADC_EXT_TYPE_HPM7177)
            {
                DIG_SUP_A_CHAN_CTRL_A     [DIG_SUP_A_CHANNEL0] = DIG_SUP_CTRL_OUTMPX_10KHZ_CLOCK;
                DIG_SUP_A_PULSE_WIDTH_US_A[DIG_SUP_A_CHANNEL0] = 1; 
            }

            if (dpcls.mcu.adc.ext_type[1] == FGC_ADC_EXT_TYPE_HPM7177)
            {
                DIG_SUP_A_CHAN_CTRL_A     [DIG_SUP_A_CHANNEL1] = DIG_SUP_CTRL_OUTMPX_10KHZ_CLOCK;
                DIG_SUP_A_PULSE_WIDTH_US_A[DIG_SUP_A_CHANNEL1] = 1; 
            }
        }
    }
}


// EOF
