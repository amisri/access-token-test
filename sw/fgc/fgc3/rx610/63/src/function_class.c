//! @file  function_class.c
//! @brief Class specific handling of function arming and ancillary functions


// ---------  Includes

#include <stdint.h>

#include <cclibs/libref/inc/libref/refFgParIdx.h>

#include <fgc_errs.h>

#include <bitmap.h>
#include <cmd.h>
#include <dpcls.h>
#include <dpcom.h>
#include <function_class.h>
#include <modePc.h>
#include <nvs.h>
#include <pars.h>
#include <pc_state.h>
#include <prop.h>
#include <pub.h>
#include <sleep.h>
#include <transaction.h>



// ---------  Internal structures, unions and enumerations

struct Function_class
{
    uint8_t func_type[FGC_MAX_USER_PLUS_1][FGC_MAX_SUB_DEVS];      //!< Function type within a transaction
};



// ---------  Internal variable definitions

static struct Function_class function_class;


static struct prop * const fg_ref_pars_to_prop[REF_NUM_FG_PARS] =
{
    [REF_FG_PAR_CTRL_PLAY                 ] = NULL                           ,
    [REF_FG_PAR_CTRL_DYN_ECO_END_TIME     ] = NULL                           ,
    [REF_FG_PAR_REF_FG_TYPE               ] = &PROP_REF_FUNC_TYPE            ,
    [REF_FG_PAR_RAMP_INITIAL_REF          ] = &PROP_REF_RAMP_INITIAL         ,
    [REF_FG_PAR_RAMP_FINAL_REF            ] = &PROP_REF_RAMP_FINAL           ,
    [REF_FG_PAR_RAMP_ACCELERATION         ] = &PROP_REF_RAMP_ACCELERATION    ,
    [REF_FG_PAR_RAMP_DECELERATION         ] = &PROP_REF_RAMP_DECELERATION    ,
    [REF_FG_PAR_RAMP_LINEAR_RATE          ] = &PROP_REF_RAMP_LINEAR_RATE     ,
    [REF_FG_PAR_PULSE_REF                 ] = &PROP_REF_PULSE_AMPLITUDE_VALUE,
    [REF_FG_PAR_PULSE_DURATION            ] = &PROP_REF_PULSE_DURATION       ,
    [REF_FG_PAR_PLEP_INITIAL_REF          ] = &PROP_REF_PLEP_INITIAL         ,
    [REF_FG_PAR_PLEP_FINAL_REF            ] = &PROP_REF_PLEP_FINAL           ,
    [REF_FG_PAR_PLEP_ACCELERATION         ] = &PROP_REF_PLEP_ACCELERATION    ,
    [REF_FG_PAR_PLEP_LINEAR_RATE          ] = &PROP_REF_PLEP_LINEAR_RATE     ,
    [REF_FG_PAR_PLEP_EXP_TC               ] = &PROP_REF_PLEP_EXP_TC          ,
    [REF_FG_PAR_PLEP_EXP_FINAL            ] = &PROP_REF_PLEP_EXP_FINAL       ,
    [REF_FG_PAR_PPPL_INITIAL_REF          ] = &PROP_REF_PPPL_INITIAL         ,
    [REF_FG_PAR_PPPL_ACCELERATION1        ] = &PROP_REF_PPPL_ACCELERATION1   ,
    [REF_FG_PAR_PPPL_ACCELERATION2        ] = &PROP_REF_PPPL_ACCELERATION2   ,
    [REF_FG_PAR_PPPL_ACCELERATION3        ] = &PROP_REF_PPPL_ACCELERATION3   ,
    [REF_FG_PAR_PPPL_RATE2                ] = &PROP_REF_PPPL_RATE2           ,
    [REF_FG_PAR_PPPL_RATE4                ] = &PROP_REF_PPPL_RATE4           ,
    [REF_FG_PAR_PPPL_REF4                 ] = &PROP_REF_PPPL_REF4            ,
    [REF_FG_PAR_PPPL_DURATION4            ] = &PROP_REF_PPPL_DURATION4       ,
    [REF_FG_PAR_PPPL_ACCELERATION1_NUM_ELS] = NULL                           ,
    [REF_FG_PAR_PPPL_ACCELERATION2_NUM_ELS] = NULL                           ,
    [REF_FG_PAR_PPPL_ACCELERATION3_NUM_ELS] = NULL                           ,
    [REF_FG_PAR_PPPL_RATE2_NUM_ELS        ] = NULL                           ,
    [REF_FG_PAR_PPPL_RATE4_NUM_ELS        ] = NULL                           ,
    [REF_FG_PAR_PPPL_REF4_NUM_ELS         ] = NULL                           ,
    [REF_FG_PAR_PPPL_DURATION4_NUM_ELS    ] = NULL                           ,
    [REF_FG_PAR_CUBEXP_REF                ] = &PROP_REF_CUBEXP_REF           ,
    [REF_FG_PAR_CUBEXP_RATE               ] = &PROP_REF_CUBEXP_RATE          ,
    [REF_FG_PAR_CUBEXP_TIME               ] = &PROP_REF_CUBEXP_TIME          ,
    [REF_FG_PAR_CUBEXP_REF_NUM_ELS        ] = NULL                           ,
    [REF_FG_PAR_CUBEXP_RATE_NUM_ELS       ] = NULL                           ,
    [REF_FG_PAR_CUBEXP_TIME_NUM_ELS       ] = NULL                           ,
    [REF_FG_PAR_TABLE_FUNCTION            ] = &PROP_REF_TABLE_FUNC_VALUE     ,
    [REF_FG_PAR_TABLE_FUNCTION_NUM_ELS    ] = NULL                           ,
    [REF_FG_PAR_TRIM_INITIAL_REF          ] = &PROP_REF_TRIM_INITIAL         ,
    [REF_FG_PAR_TRIM_FINAL_REF            ] = &PROP_REF_TRIM_FINAL           ,
    [REF_FG_PAR_TRIM_DURATION             ] = &PROP_REF_TRIM_DURATION        ,
    [REF_FG_PAR_TEST_INITIAL_REF          ] = &PROP_REF_TEST_INITIAL         ,
    [REF_FG_PAR_TEST_AMPLITUDE_PP         ] = &PROP_REF_TEST_AMPLITUDE       ,
    [REF_FG_PAR_TEST_PERIOD               ] = &PROP_REF_TEST_PERIOD          ,
    [REF_FG_PAR_TEST_NUM_PERIODS          ] = &PROP_REF_TEST_NUM_PERIODS     ,
    [REF_FG_PAR_TEST_WINDOW               ] = &PROP_REF_TEST_WINDOW          ,
    [REF_FG_PAR_TEST_EXP_DECAY            ] = &PROP_REF_TEST_EXP_DECAY       ,
    [REF_FG_PAR_PRBS_INITIAL_REF          ] = &PROP_REF_PRBS_INITIAL         ,
    [REF_FG_PAR_PRBS_AMPLITUDE_PP         ] = &PROP_REF_PRBS_AMPLITUDE_PP    ,
    [REF_FG_PAR_PRBS_PERIOD_ITERS         ] = &PROP_REF_PRBS_PERIOD_ITERS    ,
    [REF_FG_PAR_PRBS_NUM_SEQUENCES        ] = &PROP_REF_PRBS_NUM_SEQUENCES   ,
    [REF_FG_PAR_PRBS_K                    ] = &PROP_REF_PRBS_K               ,
};



// ---------  External variable definitions

struct Function_props ref;



// ---------  Internal function definitions

static uint16_t functionClassScanPars(struct cmd * c)
{
    // Scan the function parameters

    float     * value_ptr;
    uint16_t    errnum;
    uint16_t    idx;

    for (idx = 0; idx < REF_FG_PAR_MAX_ARM_PAR_VALUES && c->last_par_f == false; idx++)
    {
        errnum = ParsScanStd(c, &PROP_TEST_DEBUG_FLOAT, PKT_FLOAT, (void **)&value_ptr);

        if (errnum != FGC_NO_SYMBOL)
        {
            if (errnum != FGC_OK_NO_RSP)
            {
                c->device_par_err_idx = idx;

                return (errnum);
            }

            dpcls.mcu.ref.func.pars[idx] = *value_ptr;
        }
        else
        {
            // Force a NaN for libref to use the default value

            dpcls.mcu.ref.func.pars[idx] = NAN;
        }
    }

    // Check if more parameters remain

    if (c->last_par_f == false)
    {
        c->device_par_err_idx = idx;

        return (FGC_BAD_PARAMETER);
    }

    dpcls.mcu.ref.func.num_pars = idx;

    return FGC_OK_NO_RSP;
}



static uint16_t functionClassArmFunc(enum Dpcls_func_arm_type const arm_type,
                                     uint32_t                 const sub_sel,
                                     uint32_t                 const cyc_sel)
{
    uint16_t dsp_timeout = 500;

    dpcls.mcu.ref.func.sub_sel = sub_sel;
    dpcls.mcu.ref.func.cyc_sel = cyc_sel;

    if (arm_type == DPCLS_FUNC_ARM_TYPE_ARM)
    {
        dpcls.mcu.ref.func.type = function_class.func_type[sub_sel][cyc_sel];
    }

    // Protect arm_type with a memroy barrier to guarantee the paramters above
    // are ready before the DSP start arming a function.

    __sync_synchronize();

    dpcls.mcu.ref.func.arm_type = arm_type;

    // Wait for the DSP to arm the function or timeout
    // Resumed by the msTask() on the next ms.

    while (dpcls.mcu.ref.func.arm_type != DPCLS_FUNC_ARM_TYPE_NONE && dsp_timeout-- > 0)
    {
        OSTskSuspend();
    }

    if (dsp_timeout == 0)
    {
        dpcom.dsp.errnum = FGC_DSP_NOT_AVL;
    }

    if (dpcom.dsp.errnum != FGC_OK_NO_RSP || arm_type == DPCLS_FUNC_ARM_TYPE_TEST)
    {
        return dpcom.dsp.errnum;
    }

    // Store or restore all the properties in NVS and notify the properties.
    // These properties are listed by libfg in dpcls.dsp.ref.fg_pars_group.

    struct prop  * prop;
    uint8_t        idx;

    nvsUnlock();

    for (idx = 0; idx < REF_FG_PAR_MAX_PER_GROUP && dpcls.dsp.ref.fg_pars_group[idx] != REF_FG_PAR_NULL; idx++)
    {
        prop = fg_ref_pars_to_prop[dpcls.dsp.ref.fg_pars_group[idx]];

        if (prop != NULL)
        {
            if (testBitmap(prop->flags, PF_NON_VOLATILE) == true)
            {
                nvsStoreValuesForOneDspProperty(prop, sub_sel, cyc_sel);
            }

            pubPublishProperty(prop, sub_sel, cyc_sel, false);

            // Always publish the alias property of REF.TABLE.FUNC.VALUE: REF.TABLE.FUNCTLIST.VALUE

            if (prop == &PROP_REF_TABLE_FUNC_VALUE)
            {
                pubPublishProperty(&PROP_REF_TABLE_FUNCLIST_VALUE, sub_sel, cyc_sel, false);
            }
        }
    }

    nvsLock();

    // Publish the properties REF.INFO.*

    pubPublishProperty(&PROP_REF_INFO, sub_sel, cyc_sel, false);

    return FGC_OK_NO_RSP;
}



static uint16_t functionClassTest(uint32_t const sub_sel, uint32_t const cyc_sel)
{
    // For non-PPM user implemnent logic as described in
    // https://wikis.cern.ch/display/TEEPCCCS/Transactional+settings
    //
    // * For cycling devices:
    //   ** If in IDLE, arm the function if the device is PPM. Otherwise return
    //      BAD_STATE to prevent non-PPM settings from overwriting the all-PPM
    //      setting
    //   ** If not in IDLE, test the function if the device is not PPM since it
    //      is using the all-PPM slot. Otherwise return BAD_STATE
    // * For non-cycling devices:
    //   ** Test the settings if in IDLE. Otherwise BAD_STATE

    uint16_t errnum = FGC_BAD_STATE;

    if (cyc_sel == NON_PPM_USER)
    {
        if (pcStateGet() == FGC_PC_IDLE)
        {
            if (   modePcGetPcOn()      != FGC_PC_ON_CYCLING
                || dpcom.mcu.device_ppm == FGC_CTRL_ENABLED)
            {
                errnum = functionClassArmFunc(DPCLS_FUNC_ARM_TYPE_COMMIT, sub_sel, cyc_sel);
            }
        }
        else
        {
            if (   modePcGetPcOn()      == FGC_PC_ON_CYCLING
                && dpcom.mcu.device_ppm == FGC_CTRL_DISABLED)
            {
                errnum = functionClassArmFunc(DPCLS_FUNC_ARM_TYPE_TEST, sub_sel, cyc_sel);
            }
        }
    }
    else
    {
        errnum = functionClassArmFunc(DPCLS_FUNC_ARM_TYPE_TEST, sub_sel, cyc_sel);
    }

    return errnum;
}



static uint16_t functionClassCommit(uint32_t           const   sub_sel,
                                    uint32_t           const   cyc_sel,
                                    struct CC_us_time  const * commit_time)
{
    // For non-PPM user implemnent logic as described in
    // https://wikis.cern.ch/display/TEEPCCCS/Transactional+settings
    //
    // * For cycling devices:
    //   ** If in ARMED, run the function if the device is PPM. Otherwise return
    //      BAD_STATE
    //   ** If not in ARMED, arm the function if the device is not PPM since it
    //      is using the all-PPM slot. Otherwise return BAD_STATE
    // * For non-cycling devices:
    //   ** Run the function if in ARMED. Otherwise return BAD_STATE

    uint16_t errnum = FGC_BAD_STATE;

    if (cyc_sel == NON_PPM_USER)
    {
        if (pcStateGet() == FGC_PC_ARMED)
        {
            if (   modePcGetPcOn()      != FGC_PC_ON_CYCLING
                || dpcom.mcu.device_ppm == FGC_CTRL_ENABLED)
            {
                // Run on commit (the function has been already armed): send a START event

                dpcls.mcu.event.func_run_start_time_us = commit_time != NULL ? *commit_time : sta.timestamp_us;

                dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_START);

                errnum = FGC_OK_NO_RSP;
            }
        }
        else
        {
            if (   modePcGetPcOn()      == FGC_PC_ON_CYCLING
                && dpcom.mcu.device_ppm == FGC_CTRL_DISABLED)
            {
                errnum = functionClassArmFunc(DPCLS_FUNC_ARM_TYPE_COMMIT, sub_sel, cyc_sel);
            }
        }
    }
    else
    {
        errnum = functionClassArmFunc(DPCLS_FUNC_ARM_TYPE_COMMIT, sub_sel, cyc_sel);
    }

    return errnum;
}



static uint16_t functionClassRollback(uint32_t const sub_sel, uint32_t const cyc_sel)
{
    // Disarm the function to transition to IDLE

    if (cyc_sel == NON_PPM_USER && pcStateGet() == FGC_PC_ARMED)
    {
        dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_REF_DISARM);
    }

    return functionClassArmFunc(DPCLS_FUNC_ARM_TYPE_RESTORE, sub_sel, cyc_sel);
}



static void functionClassSetTransactionalProperty(uint32_t      const sub_sel,
                                                  uint32_t      const cyc_sel,
                                                  struct prop *       property)
{
    uint8_t param;

    // Hanlde the TABLE alias

    if (property == &PROP_REF_TABLE_FUNCLIST_VALUE)
    {
        property = &PROP_REF_TABLE_FUNC_VALUE;
    }

    // Find the parameter

    for (param = 0; param < REF_NUM_FG_PARS && fg_ref_pars_to_prop[param] != property; param++)
    {
        ;
    }

    if (param < REF_NUM_FG_PARS)
    {
        dpcls.mcu.transaction[cyc_sel].last_param[sub_sel] = param;

        // Disarm the function to transition to IDLE

        if (cyc_sel == NON_PPM_USER && pcStateGet() == FGC_PC_ARMED)
        {
            dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_REF_DISARM);
        }
    }
}



// ---------  Platform/class specific function definitions

char const * functionGetDefaultPrefix(void)
{
    static char const prefixIdle[]    = "!S REF NOW,";
    static char       prefixDirect[]  = "!S REF.DIRECT.*.VALUE ";
    static char const prefixRegMode[] = "VIB";

    if (pcStateGet() == FGC_PC_IDLE)
    {
        return (prefixIdle);
    }

    if (pcStateGet() == FGC_PC_DIRECT)
    {
        prefixDirect[14] = prefixRegMode[dpcls.dsp.ref.reg_mode];

        return (prefixDirect);
    }

    return ("");
}



// ---------  External function definitions

void functionClassInit(void)
{
    // Link the functions used by the transactional module

    transactionInit(&functionClassTest,
                    &functionClassCommit,
                    &functionClassRollback,
                    &functionClassSetTransactionalProperty);
}



uint16_t functionClassSetRef(struct cmd * c, uint16_t const func_type)
{
    // S REF FUNC_TYPE,arg1,...

    uint16_t errnum;

    // Scan the function type arguments

    if ((errnum = functionClassScanPars(c)) != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    // The function NOW is implemented with a RAMP

    function_class.func_type[NON_MULTI_PPM_SUB_DEV][NON_PPM_USER] = (func_type == FGC_REF_NOW ? FGC_REF_RAMP : func_type);

    if ((errnum = functionClassArmFunc(DPCLS_FUNC_ARM_TYPE_ARM, NON_MULTI_PPM_SUB_DEV, NON_PPM_USER)) != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    // If the function NOW is successfully armed, simulate a START event to
    // run immediately when the state transitions to ARMED.

    if (func_type == FGC_REF_NOW)
    {
        uint16_t dsp_timeout = 500;

        // Wait for the DSP to transition to ARMED or timeout.
        // Resumed by the mstTask() on the next ms.

        while (pcStateGet() != FGC_PC_ARMED && dsp_timeout-- > 0)
        {
            OSTskSuspend();
        }

        if (dsp_timeout != 0)
        {
            dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_START);

            errnum = FGC_OK_NO_RSP;
        }
        else
        {
            errnum = FGC_BAD_STATE;
        }
    }

    return errnum;
}



uint16_t functionClassSetFuncType(struct cmd * c, uint16_t const func_type)
{
    if (func_type == FGC_REF_NOW)
    {
        return FGC_BAD_PARAMETER;
    }

    // Set REF.FUNC.TYPE

    function_class.func_type[c->sub_sel][c->cyc_sel] = func_type;

    // If there is an ongoing transaction, arm on TEST/COMMIT. Otherwise, arm immediately.

    return (  transactionGetId(c->sub_sel, c->cyc_sel) == 0
            ? functionClassArmFunc(DPCLS_FUNC_ARM_TYPE_ARM, c->sub_sel, c->cyc_sel)
            : FGC_OK_NO_RSP);
}



char * functionClassGetStr(void)
{
    static uint16_t const const_to_stc_ref[] =
    {
        STC_NONE   ,    // FGC_REF_NONE
        STC_ZERO   ,    // FGC_REF_ZERO
        STC_RAMP   ,    // FGC_REF_RAMP
        STC_PULSE  ,    // FGC_REF_PULSE
        STC_PLEP   ,    // FGC_REF_PLEP
        STC_PPPL   ,    // FGC_REF_PPPL
        STC_CUBEXP ,    // FGC_REF_CUBEXP
        STC_TABLE  ,    // FGC_REF_TABLE
        STC_LTRIM  ,    // FGC_REF_LTRIM
        STC_CTRIM  ,    // FGC_REF_CTRIM
        STC_STEPS  ,    // FGC_REF_STEPS
        STC_SQUARE ,    // FGC_REF_SQUARE
        STC_SINE   ,    // FGC_REF_SINE
        STC_COSINE ,    // FGC_REF_COSINE
        STC_OFFCOS ,    // FGC_REF_OFFCOS
        STC_PRBS   ,    // FGC_REF_PRBS
    };

    // Crude check if the array content matches the constants, better than nothing...

    static_assert(ArrayLen(const_to_stc_ref) == FGC_REF_PRBS + 1, "Array length doesn't match the constants used for indexing");

    return (SYM_TAB_CONST[const_to_stc_ref[dpcls.dsp.ref.func_type]].key.c);
}


// EOF
