//! @file  events_class.c
//! @brief File with the class-specific processing of events


// ---------- Includes

#include <stdint.h>

#include <logCycle.h>
#include <bitmap.h>
#include <cycSim.h>
#include <cycTime.h>
#include <device.h>
#include <dpcls.h>
#include <dpcom.h>
#include <events.h>
#include <fbs_class.h>
#include <function_class.h>
#include <modePc.h>
#include <pc_state.h>
#include <time_fgc.h>
#include <transaction.h>



// ---------- Internal function definitions

static void eventsClassProcessStart(struct CC_us_time  const * event_time,
                                    struct Events_data const   data)
{
    if (data.payload.event_group == 0 || data.payload.event_group == events_prop.event_group)
    {
        dpcls.mcu.event.func_run_start_time_us = *event_time;

        __sync_synchronize();

        dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_START);
    }
}



static void eventsClassProcessAbort(struct CC_us_time  const * event_time,
                                    struct Events_data const   data)
{
    if (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_ABORT) == true)
    {
        if (data.payload.event_group == 0 || data.payload.event_group == events_prop.event_group)
        {
            dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_ABORT);
        }
    }
}



static void eventsClassProcessSSC(struct CC_us_time  const * event_time,
                                  struct Events_data const   data)
{
    logCycleStartSuperCycle();
}



static void eventsClassProcessCycleTag(struct CC_us_time  const * event_time,
                                       struct Events_data const   data)
{
    // Transfer the cyle tag to the DSP

    dpcls.oasis.cycle_tag_next = data.payload.cycle_tag;
}



static void eventsClassProcessRef(struct CC_us_time  const * event_time,
                                  struct Events_data const   data)
{
    // Calculate the delay based on the event_time time and the current time

    struct CC_us_time now = { { timeGetUtcTime() }, timeGetUtcTimeMs() * 1000 };
    int32_t delay_us = timeUsDiff(&now, event_time);

    if (delay_us < 0)
    {
        return; // @TODO output an error
    }

    // Set up the TimeTillEvent register

    if (eventsGetSource() != EVENTS_SOURCE_EXTERNAL)
    {
        cycTimeSetTimeTillEvent(delay_us, false);
    }

    // Inform the DSP of the next event. Always use PPM sub_sel and cyc_sel since
    // cclibs will zero these values if needed.

    dpcls.mcu.event.sub_sel = data.payload.cycle.sub_sel_acq;
    dpcls.mcu.event.cyc_sel = data.payload.cycle.cyc_sel_acq;

    dpcls.mcu.event.start_event_time_us = *event_time;

    __sync_synchronize();

    uint32_t evt_received_mask = DPCLS_EVT_RECEIVED_REF;

    // Check if the cycle has to be economized

    if (   cycSimIsEnabled()     == false
        && ref.economy_dest.mode == FGC_CTRL_ENABLED
        && testBitmap(ref.economy_dest.mask, data.next_destination) == false)
    {
        setBitmap(evt_received_mask, DPCLS_EVT_RECEIVED_ECONOMY_FULL_ONCE);
    }

    dpcmdSet(&dpcls.mcu.event.received, evt_received_mask);
}



static void eventsClassProcessPause(struct CC_us_time  const * event_time,
                                    struct Events_data const   data)
{
    dpcls.mcu.event.pause_event_time_us = *event_time;

    __sync_synchronize();

    dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_PAUSE);
}



static void eventsClassProcessResume(struct CC_us_time  const * event_time,
                                     struct Events_data const   data)
{
    dpcls.mcu.event.resume_event_time_us = *event_time;

    __sync_synchronize();

    dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_RESUME);
}



static void eventsClassProcessEconomyDynamic(struct CC_us_time  const * event_time,
                                             struct Events_data const   data)
{
    dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_ECONOMY_DYNAMIC);
}



static void eventsClassProcessAcquisition(struct CC_us_time  const * event_time,
                                          struct Events_data const   data)
{
    dpcls.mcu.event.acquisition_event_time_us = *event_time;
    dpcls.mcu.event.acquisition_event_cyc_sel = data.payload.data;

    __sync_synchronize();

    dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_ACQUISITION);
}



static void eventsClassProcessTransactionCommit(struct CC_us_time  const * event_time,
                                                struct Events_data const   data)
{
    for (uint32_t i = 0; i <= device.max_sub_devs; i++)
    {
        transactionCommit(i, data.payload.transaction_id, event_time);
    }
}



static void eventsClassProcessTransactionRollback(struct CC_us_time  const * event_time,
                                                  struct Events_data const   data)
{
    for (uint32_t i = 0; i <= device.max_sub_devs; i++)
    {
        transactionRollback(i, data.payload.transaction_id);
    }
}



static void eventsClassProcessCoast(struct CC_us_time  const * event_time,
                                    struct Events_data const   data)
{
    dpcls.mcu.event.coast_event_time_us = *event_time;

    __sync_synchronize();

    dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_COAST);
}



static void eventsClassProcessRecover(struct CC_us_time  const * event_time,
                                      struct Events_data const   data)
{
    dpcls.mcu.event.recover_event_time_us = *event_time;

    __sync_synchronize();

    dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_RECOVER);
}



static void eventsClassProcessStartHarmonics(struct CC_us_time  const * event_time,
                                             struct Events_data const   data)
{
    if (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_HARMONICS) == true)
    {
        dpcls.mcu.event.start_harmonics_event_time_us = *event_time;

        __sync_synchronize();

        dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_START_HARMONICS);
    }
}



static void eventsClassProcessStopHarmonics(struct CC_us_time  const * event_time,
                                            struct Events_data const   data)
{
    if (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_HARMONICS) == true)
    {
        dpcls.mcu.event.stop_harmonics_event_time_us = *event_time;

        __sync_synchronize();

        dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_STOP_HARMONICS);
    }
}


// ---------- Platform/class specific function definitions

void eventsClassInit(void)
{
    eventsRegisterEventFunc(FGC_EVT_START                 , eventsClassProcessStart              );
    eventsRegisterEventFunc(FGC_EVT_ABORT                 , eventsClassProcessAbort              );
    eventsRegisterEventFunc(FGC_EVT_SSC                   , eventsClassProcessSSC                );
    eventsRegisterEventFunc(FGC_EVT_CYCLE_TAG             , eventsClassProcessCycleTag           );
    eventsRegisterEventFunc(FGC_EVT_START_REF_1           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_START_REF_2           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_START_REF_3           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_START_REF_4           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_START_REF_5           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_1, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_2, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_3, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_4, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_5, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_CYCLE_START           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_CYCLE_START, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_PAUSE                 , eventsClassProcessPause              );
    eventsRegisterEventFunc(FGC_EVT_RESUME                , eventsClassProcessResume             );
    eventsRegisterEventFunc(FGC_EVT_ECONOMY_DYNAMIC       , eventsClassProcessEconomyDynamic     );
    eventsRegisterEventFunc(FGC_EVT_ACQUISITION           , eventsClassProcessAcquisition        );
    eventsRegisterEventFunc(FGC_EVT_TRANSACTION_COMMIT    , eventsClassProcessTransactionCommit  );
    eventsRegisterEventFunc(FGC_EVT_TRANSACTION_ROLLBACK  , eventsClassProcessTransactionRollback);
    eventsRegisterEventFunc(FGC_EVT_COAST                 , eventsClassProcessCoast              );
    eventsRegisterEventFunc(FGC_EVT_RECOVER               , eventsClassProcessRecover            );
    eventsRegisterEventFunc(FGC_EVT_START_HARMONICS       , eventsClassProcessStartHarmonics     );
    eventsRegisterEventFunc(FGC_EVT_STOP_HARMONICS        , eventsClassProcessStopHarmonics      );
}



void eventsClassProcess(void)
{
    static bool economy_full_state = false;

    if (testBitmap(fbs.time_v.flags, FGC_FLAGS_ECONOMY_FULL) != economy_full_state)
    {
        economy_full_state = testBitmap(fbs.time_v.flags, FGC_FLAGS_ECONOMY_FULL);

        dpcmdSet(&dpcls.mcu.event.received,  economy_full_state == true
                                           ? DPCLS_EVT_RECEIVED_ECONOMY_FULL_ENABLE
                                           : DPCLS_EVT_RECEIVED_ECONOMY_FULL_DISABLE);
    }
}


// EOF
