//! @file   statePc.c
//! @brief  Manage the Power Converter state and the Reference state


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <defconst.h>
#include <defprops.h>

#include <calibration.h>
#include <fbs_class.h>
#include <function_class.h>
#include <statePc_class.h>
#include <statePc.h>
#include <pc_state.h>
#include <modePc.h>
#include <dpcls.h>
#include <crate.h>
#include <sta.h>
#include <dimDataProcess.h>
#include <pulsePermit.h>
#include <postMortem.h>
#include <pub.h>
#include <pc_state.h>



// ---------- Platform/class specific function definitions

void statePcClassProcess(void)
{
    // Reset the reference event group if the new state is not ARMED or RUNNING

    if (   events_prop.event_group != 0
        && pcStateTest(FGC_STATE_ARMED_BIT_MASK | FGC_STATE_RUNNING_BIT_MASK) == false)
    {
        events_prop.event_group = 0;

        pubPublishProperty(&PROP_REF_EVENT_GROUP, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, true);
    }

    // Update REF.FAULT_USER

    if (pcStateGet() == FGC_PC_SLOW_ABORT)
    {
        if (ref.fault_user == 0 && dpcom.dsp.log.slow_abort_faults != 0)
        {
            ref.fault_user = dpcom.dsp.cycle.current.cyc_sel_acq;
            pubPublishProperty(&PROP_REF_FAULT_USER, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
        }
    }
    else if (pcStateTest(FGC_STATE_FLT_STOPPING_BIT_MASK | FGC_STATE_FLT_OFF_BIT_MASK) == true)
    {
        if (ref.fault_user == 0)
        {
            ref.fault_user = dpcom.dsp.cycle.current.cyc_sel_acq;
            pubPublishProperty(&PROP_REF_FAULT_USER, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
        }
    }
    else
    {
        if (ref.fault_user != 0)
        {
            ref.fault_user = 0;
            pubPublishProperty(&PROP_REF_FAULT_USER, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
        }
    }
}


// ----------------------------- CLASS SPECIFIC STATE FUNCTIONS ------------------------------

// STATE: TO_STANDBY (TS) - LIBREF STATE: TO_STANDBY

uint32_t statePcTS(bool firstCall)
{
    return FGC_PC_TO_STANDBY;
}



// STATE: ON_STANDBY (SB) - LIBREF STATE: STANDBY

uint32_t statePcSB(bool firstCall)
{
    return FGC_PC_ON_STANDBY;
}



// STATE: IDLE (IL) - LIBREF STATE: IDLE

uint32_t statePcIL(bool firstCall)
{
    return FGC_PC_IDLE;
}



// STATE: TO_CYCLING (TC) - LIBREF STATE: TO_CYCLING

uint32_t statePcTC(bool firstCall)
{
    return FGC_PC_TO_CYCLING;
}



// STATE: ARMED (AR) - LIBREF STATE: ARMED

uint32_t statePcAR(bool firstCall)
{
    return FGC_PC_ARMED;
}



// STATE: RUNNING (RN) - LIBREF STATE: RUNNING

uint32_t statePcRN(bool firstCall)
{
    return FGC_PC_RUNNING;
}



// STATE: ABORTING (AB) - LIBREF STATE: TO_IDLE

uint32_t statePcAB(bool firstCall)
{
    return FGC_PC_ABORTING;
}



// STATE: CYCLING (CY) - LIBREF STATE: CYCLING

uint32_t statePcCY(bool firstCall)
{
    return FGC_PC_CYCLING;
}



// STATE: POL_SWITCHING (PL) - LIBREF STATE: POL_SWITCHING

uint32_t statePcPL(bool firstCall)
{
    return FGC_PC_POL_SWITCHING;
}



// STATE: ECONOMY (ECL) - LIBREF STATE: ECONOMY

uint32_t statePcEC(bool firstCall)
{
    return FGC_PC_ECONOMY;
}



// STATE: DIRECT (DT) - LIBREF STATE: DIRECT

uint32_t statePcDT(bool firstCall)
{
    return FGC_PC_DIRECT;
}



// STATE: PAUSED (PA) - LIBREF STATE: PAUSED

uint32_t statePcPA(bool firstCall)
{
    return FGC_PC_PAUSED;
}



// ------------------------ CLASS SPECIFIC TRANSITION CONDITION FUNCTIONS -------------------------

static StatePC * XXtoXX(void)
{
    // Mapping between libref ref_state and state_pc - this must match the order of the ref state
    // constants, so they are checked above with static assert in case libref is changed in future.

    static StatePC * refStateToStatePc[] =
    {
        statePcBK,     //   0. [REF_OFF          ] - wait in actual state for voltage source to stop or a fault to appear
        statePcPL,     //   1. [REF_POL_SWITCHING]
        statePcSA,     //   2. [REF_TO_OFF       ]
        statePcTS,     //   3. [REF_TO_STANDBY   ]
        statePcTC,     //   4. [REF_TO_CYCLING   ]
        statePcAB,     //   5. [REF_TO_IDLE      ]
        statePcDT,     //   6. [REF_DIRECT       ]
        statePcSB,     //   7. [REF_STANDBY      ]
        statePcCY,     //   8. [REF_CYCLING      ]
        statePcEC,     //   9. [REF_DYN_ECO      ]
        statePcEC,     //  10. [REF_FULL_ECO     ]
        statePcPA,     //  11. [REF_PAUSED       ]
        statePcIL,     //  12. [REF_IDLE         ]
        statePcAR,     //  13. [REF_ARMED        ]
        statePcRN      //  14. [REF_RUNNING      ]
    };

    // Look up STATE.PC state function that corresponds to the current libref ref_state

    StatePC * next_state = refStateToStatePc[dpcls.dsp.state.ref];

    // Clear force_off once libref has transitioned to OFF

    if (   next_state              == statePcBK
        && dpcls.mcu.ref.force_off == true)
    {
        dpcls.mcu.ref.force_off = false;
    }

    // If REF_OFF (statePcBK), block the converter and stay in BLOCKING
    // until the VS STAE is BLOCKED if moving out of starting

    if (   next_state              == statePcBK
        && STATE_VS                == FGC_VS_READY
        && dpcls.mcu.vs.blockable  != FGC_VS_BLOCK_DISABLED
        && (   modePcGetInternal() == FGC_PC_OFF
            || modePcGetInternal() == FGC_PC_BLOCKING))
    {
        digitalIoSetCmdRequest(DDOP_CMD_BLOCKING);

        next_state = NULL;
    }

    // Return pointer to the new state function only if it has changed

    if (next_state != statePcGetStateFunc())
    {
        return next_state;
    }

    // Return NULL to indicate that state has not changed

    return NULL;
}



static StatePC * ARtoXX(void)
{
    StatePC * next_state = XXtoXX();

    if (next_state == statePcRN && dpcls.dsp.ref.fg_status_pre_func == true)
    {
        next_state = NULL;
    }

    return next_state;
}



// ---------- External variable definitions

//! State transition functions : called in priority order, from left to right,
//! return the state function.
//!
//!  The initialization order of StateTransitions must match the STATE.PC
//!  constant values from the XML symlist. This is checked with static assertions.

StatePCtrans * StateTransitions[][STATE_PC_REF_MAX_TRANSITIONS + 1] =
{                                                            // LIBREF STATE
    /*  0.FO */ {                 FOtoOF,         NULL },    //      NO
    /*  1.OF */ {                 OFtoFO, OFtoST, NULL },    //      NO
    /*  2.FS */ {                 FStoFO, FStoSP, NULL },    //      NO
    /*  3.SP */ { XXtoFS,         SPtoOF,         NULL },    //      NO
    /*  4.ST */ { XXtoFS,         STtoSP, STtoBK, NULL },    //      NO
    /*  5.SA */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /*  6.TS */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /*  7.SB */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /*  8.IL */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /*  9.TC */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /* 10.AR */ { XXtoFS, ARtoXX,                 NULL },    //      YES
    /* 11.RN */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /* 12.AB */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /* 13.CY */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /* 14.PL */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /* 15.BK */ { XXtoFS, XXtoXX, BKtoSP,         NULL },    //      NO
    /* 16.EC */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /* 17.DT */ { XXtoFS, XXtoXX,                 NULL },    //      YES
    /* 18.PA */ { XXtoFS, XXtoXX,                 NULL }     //      YES
};


// EOF
