//! @file  msTask_class.c
//! @brief Class specific millisecond actions


// ---------- Includes

#include <msTask.h>
#include <comHvPs.h>
#include <crate.h>
#include <logCycle.h>
#include <defconst.h>
#include <defprops.h>
#include <diag.h>
#include <dpcls.h>
#include <dpcom.h>
#include <fbs_class.h>
#include <pub.h>
#include <regfgc3Prog.h>
#include <stateOp.h>
#include <sharedMemory.h>
#include <time_fgc.h>



// ---------- Internal function declarations

static void msTaskClassPublishData(void);
static void msTaskClassPublish(void);



// ---------- Platform/class specific function definitions

void msTaskClassProcess(void)
{
    // Every Millisecond: Poll VSNOCABLE value and simulate commands according to its state
    // This functionality will be removed when the RF team finalizes using a software
    // interface. Currently on the RF25KV converter has this hardware interface

    if (crateGetType() == FGC_CRATE_TYPE_RF25KV && regFgc3ProgAllBoardsInPb() == true)
    {
        comHvPsPollVsNoCable();
    }

    // Every second - public B measurement

    if (timeGetUtcTimeMs() == 0)
    {
        pubPublishProperty(&PROP_MEAS_B_VALUE, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
    }

    logCycleStore();
    msTaskClassPublishData();
    msTaskClassPublish();
}



// ---------- Internal function definitions

static void msTaskClassPublishData(void)
{
    // Millisecond 1 of 10: Read analogue values.

    if (   ms_task.ms_mod_10 == 1
        && stateOpGetState() != FGC_OP_UNCONFIGURED)
    {
        I_REF  = dpcls.dsp.ref.i;
        V_REF  = dpcls.dsp.ref.v;
        I_MEAS = dpcls.dsp.meas.i;
    }

    //  Millisecond 1 of 20: Read DSP status and run Gateway PC_PERMIT timeout

    if (ms_task.ms_mod_20 == 1)
    {
        ST_MEAS_A     = (uint8_t) dpcom.dsp.meas.adc_state[0];
        ST_MEAS_B     = (uint8_t) dpcom.dsp.meas.adc_state[1];
        ST_MEAS_C     = (uint8_t) dpcom.dsp.meas.adc_state[2];
        ST_MEAS_D     = (uint8_t) dpcom.dsp.meas.adc_state[3];
        ST_DCCT_A     = (uint8_t) dpcom.dsp.meas.dcct_state[0];
        ST_DCCT_B     = (uint8_t) dpcom.dsp.meas.dcct_state[1];
        V_MEAS        = dpcls.dsp.meas.v;
        I_ERR_MA      = (int16_t) dpcls.dsp.reg.i_err_ma;
        I_DIFF_MA     = (int16_t) dpcls.dsp.meas.i_diff_ma;
        I_EARTH_CPCNT = (int16_t) meas_i_earth.cpcnt;
    }
}



static void msTaskClassPublish(void)
{
    // Publish properties at the acquisition time. These properties are
    // acquisitions and therefore never multi-PPM

    // Publish properties triggered by MEAS.TRIG.DELAY_MS or timing event

    if (dpcls.dsp.meas.pub_acq1_cyc_sel > 0)
    {
        uint8_t cyc_sel = dpcls.dsp.meas.pub_acq1_cyc_sel;

        pubPublishProperty(&PROP_MEAS_CUBEXP,          PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_MEAS_DIRECT_V,        PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_MEAS_DIRECT_I,        PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_MEAS_DIRECT_B,        PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_MEAS_PULSE,           PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_MEAS_TABLE_FUNC,      PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_MEAS_TABLE_FUNCLIST,  PUB_NO_SUB_SEL, cyc_sel, false);

        dpcls.dsp.meas.pub_acq1_cyc_sel = 0;
    }

    // Publish MEAS properties triggered by MEAS.TRIG.DELAY2_MS

    if (dpcls.dsp.meas.pub_acq2_cyc_sel > 0)
    {
        uint8_t cyc_sel = dpcls.dsp.meas.pub_acq2_cyc_sel;

        pubPublishProperty(&PROP_MEAS_CUBEXP2,     PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_MEAS_TABLE_FUNC2, PUB_NO_SUB_SEL, cyc_sel, false);

        dpcls.dsp.meas.pub_acq2_cyc_sel = 0;
    }

    // Publish INTERLOCK latched values

    if (dpcls.dsp.interlock.ready == true)
    {
        uint8_t cyc_sel = dpcls.dsp.interlock.cyc_sel;

        pubPublishProperty(&PROP_INTERLOCK_RESULT,     PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
        pubPublishProperty(&PROP_INTERLOCK_V_MEAS,     PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
        pubPublishProperty(&PROP_INTERLOCK_I_MEAS,     PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);

        pubPublishProperty(&PROP_INTERLOCK_PPM_RESULT, PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_INTERLOCK_PPM_V_MEAS, PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_INTERLOCK_PPM_I_MEAS, PUB_NO_SUB_SEL, cyc_sel, false);

        dpcls.dsp.interlock.ready = false;
    }

    // Publish properties at the end of the cycle. These properties are
    // acquisitions and therefore never multi-PPM

    if (dpcom.dsp.cycle.pub_previous_cycle == true)
    {
        uint8_t cyc_sel = dpcom.dsp.cycle.previous.cyc_sel_acq;

        // LOG.OASIS.*

        pubPublishProperty(&PROP_LOG_OASIS_V_REF_DATA,  PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_LOG_OASIS_V_MEAS_DATA, PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_LOG_OASIS_I_REF_DATA,  PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_LOG_OASIS_I_MEAS, PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_LOG_OASIS_B_REF_DATA,  PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_LOG_OASIS_B_MEAS_DATA, PUB_NO_SUB_SEL, cyc_sel, false);

        // REF.CYC.*

        pubPublishProperty(&PROP_REF_CYC_STATUS,        PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_REF_CYC_MAX_ABS_ERR,   PUB_NO_SUB_SEL, cyc_sel, false);

        // LOG.I.*

        pubPublishProperty(&PROP_LOG_I_REF,  PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_LOG_I_MEAS, PUB_NO_SUB_SEL, cyc_sel, false);

        dpcom.dsp.cycle.pub_previous_cycle = false;
    }
}


// EOF
