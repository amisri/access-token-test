//! @file  set_class.c
//! @brief Class specific Set command functions


// ---------- Includes

#include <fgc_errs.h>

#include <function_class.h>
#include <set_class.h>

#include <cmd.h>
#include <crate.h>
#include <defprops.h>
#include <digitalIo.h>
#include <dpcls.h>
#include <dpcom.h>
#include <pars.h>
#include <pc_state.h>
#include <prop.h>
#include <property.h>
#include <pub.h>



// ---------- External function definitions

uint16_t SetDevicePPM(struct cmd * c, struct prop * p)
{
    uint16_t  errnum;

    // If DEVICE.PPM has changed

    if ((errnum = ParsSet(c, p)) == 0)
    {
        if (p == &PROP_DEVICE_PPM)
        {
            device.max_user = (dpcom.mcu.device_ppm == FGC_CTRL_ENABLED ? FGC_MAX_USER : 0);
        }

        // PROP_DEVICE_MULTI_PPM

        else
        {
            device.max_sub_devs = (dpcom.mcu.device_multi_ppm == FGC_CTRL_ENABLED ? FGC_MAX_SUB_DEVS - 1 : 0);
        }
    }

    return (errnum);
}



uint16_t SetRef(struct cmd * c, struct prop * p)
{
    // SetRef provides support for S REF FUNC_TYPE,args,... and S REF.FUNC.TYPE(user) FUNC_TYPE

    struct sym_lst const * sym_const;
    uint16_t               errnum;

    // Scan first argument for settable function type symbol

    if ((errnum = ParsScanSymList(c, "Y, ", (struct sym_lst *)p->range, &sym_const)) == FGC_OK_NO_RSP)
    {
        errnum =  p->sym_idx == STP_REF
                ? functionClassSetRef     (c, sym_const->value)
                : functionClassSetFuncType(c, sym_const->value);
    }

    // Wait for the state to transition to ARMED or timeout (5 seconds) before
    // returning from the set command. This is a requirement from the LHC
    // sequencer (EPCCCS-9615).
    // Evaluate if this is necessary once transactional settings is rolled-out
    // in the LHC.

    if (errnum == FGC_OK_NO_RSP && c->cyc_sel == NON_PPM_USER)
    {
        uint32_t timeout_ms = 5000;

        while (pcStateGet() == FGC_PC_IDLE && timeout_ms-- > 0)
        {
            // Resumed by msTask() on next millisecond
            OSTskSuspend();        
        }

        if (timeout_ms == 0)
        {
            errnum = FGC_BAD_STATE;
        }
    }

    return errnum;
}



uint16_t SetPoint(struct cmd * c, struct prop * p)
{
    return (ParsSet(c, p));
}



uint16_t SetRefControlValue(struct cmd * c, struct prop * p)
{
    float       * fvalue_ptr;
    float         ref;
    struct prop * limit_pos_p;
    struct prop * limit_min_p;
    struct prop * limit_neg_p;
    float         limits_pos;
    float         limits_min;
    float         limits_neg;
    uint16_t      errnum;

    errnum  = ParsValueGet(c, p, (void **) &fvalue_ptr);

    if (errnum != 0)
    {
        return errnum;
    }

    ref = *fvalue_ptr;

    // Get the limits from the DSP

    if (p == &PROP_REF_DIRECT_B_VALUE)
    {
        limit_pos_p = &PROP_REF_DIRECT_B_VALUE_MAX;
        limit_min_p = &PROP_LIMITS_OP_B_STANDBY;
        limit_neg_p = &PROP_REF_DIRECT_B_VALUE_MIN;
    }
    else if (p == &PROP_REF_DIRECT_I_VALUE)
    {
        limit_pos_p = &PROP_REF_DIRECT_I_VALUE_MAX;
        limit_min_p = &PROP_LIMITS_OP_I_STANDBY;
        limit_neg_p = &PROP_REF_DIRECT_I_VALUE_MIN;
    }
    else
    {
        limit_pos_p = &PROP_REF_DIRECT_V_VALUE_MAX;
        limit_min_p = &PROP_REF_DIRECT_V_VALUE_MIN;
        limit_neg_p = &PROP_REF_DIRECT_V_VALUE_MIN;
    }

    errnum |= PropValueGet(c, limit_pos_p, NULL, 0, (uint8_t **)&fvalue_ptr);
    limits_pos = *fvalue_ptr;

    errnum |= PropValueGet(c, limit_min_p, NULL, 0, (uint8_t **)&fvalue_ptr);
    limits_min = *fvalue_ptr;

    errnum |= PropValueGet(c, limit_neg_p, NULL, 0, (uint8_t **)&fvalue_ptr);
    limits_neg = *fvalue_ptr;

    if (errnum != 0)
    {
        return errnum;
    }

    // Zero should always be allowed (it's needed for degaussing for example)

    if (ref != 0.0 && (ref < limits_neg || ref > limits_pos || fabs(ref) < limits_min))
    {
        return(FGC_OUT_OF_LIMITS);
    }

    PropSet(c, p, (void*)&ref, 1);

    pubPublishProperty(p, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, TRUE);

    return (FGC_OK_NO_RSP);
}



uint16_t SetTableFunc(struct cmd * c, struct prop * p)
{
    uint16_t errnum;

    // Auto-arm only if the device is in the LHC: the lsb of the omode mask is non-zero

    if ((deviceGetOmodeMask() & 0x00FF) == 0)
    {
        errnum = SetPoint(c, p);
    }
    else
    {
        errnum = ParsSet(c, p);

        if (errnum == 0)
        {
            // Attempt to arm the table function
            errnum = functionClassSetFuncType(c, FGC_REF_TABLE);
        }
    }

    return errnum;
}



uint16_t SetDevName(struct cmd * c, struct prop * p)
{
    return interFgcSetDevName(c, p);
}



uint16_t SetExternalAdcType(struct cmd * c, struct prop * p)
{
    uint16_t errnum;

    errnum = ParsSet(c, p);

    if (errnum != 0)
    {
        return errnum;
    }

    digitalIoSupInitClass();
    dpcmdSet(&dpcom.mcu.commands, DPCOM_MCU_COMMAND_ADC_EXT_LIMITS);

    return (errnum);
}


uint16_t SetVsFiring(struct cmd * c, struct prop * p)
{
    uint16_t errnum;

    errnum = ParsSet(c, p);

    if (errnum != 0)
    {
        return errnum;
    }

    // Get the value from the DSP

    uint32_t * value_ptr;

    errnum = PropValueGet(c, &PROP_VS_FIRING_MODE, NULL, 0, (uint8_t **)&value_ptr);

    if (errnum == 0)
    {
        uint32_t mode = *value_ptr;

        digitalIoSetFiringMode((uint16_t)mode);
    }

    return (errnum);
}



uint16_t SetCycSim(struct cmd * c, struct prop * p)
{
    uint16_t errnum = ParsSet(c, p);

    if (errnum != 0)
    {
        return errnum;
    }

    if (PROP_FGC_CYC_SIM.n_elements != 0)
    {
        uint8_t i;

        for (i = 0; i < PROP_FGC_CYC_SIM.n_elements; ++i)
        {
            uint32_t cycle  = cycsim_prop.cycles[i];
            uint8_t sub_sel = (uint8_t)((cycle % 1000000) / 10000);
            uint8_t cyc_sel = (uint8_t)((cycle %   10000) /   100);
            uint8_t length  = (uint8_t)((cycle %     100) /     1);

            if (cycSimSetCycleInfo(sub_sel, cyc_sel, length) == false)
            {
                return FGC_BAD_PARAMETER;
            }
        }
    }

    cycSimSetCycleComplete();

    return 0;
}



uint16_t SetAbsTime(struct cmd * c, struct prop * p)
{
    uint16_t errnum = ParsSet(c, p);

    if (errnum != FGC_OK_NO_RSP)
    {
        return errnum;
    }

    if (p == &PROP_REF_RUN)
    {
        // Simulate a START event

        dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_START);
    }
    else if (p == &PROP_REF_ABORT)
    {
        // Disregard the time and abort immediately by simulating START event

        dpcmdSet(&dpcls.mcu.event.received, DPCLS_EVT_RECEIVED_ABORT);
    }

    return errnum;
}


// EOF
