//! @file  class.h
//! @brief This file contains all class related definitions

#pragma once


// ---------- Includes

#include <stdint.h>

#include <dsp_63.h>



// ---------- Constants

#define DEV_STA_TSK_PHASE   2      //!< Millisecond to run stateTask()

// Status masks

#define FGC_FAULTS             ( FGC_FLT_FGC_HW          \
                               | FGC_FLT_MEAS            \
                               | FGC_FLT_REG_ERROR       \
                               | FGC_FLT_POL_SWITCH      \
                               | FGC_FLT_FGC_STATE       \
                               | FGC_FLT_LIMITS )

#define FGC_WARNINGS           ( FGC_WRN_FGC_HW          \
                               | FGC_WRN_MEAS            \
                               | FGC_WRN_REG_ERROR       \
                               | FGC_WRN_TEMPERATURE     \
                               | FGC_WRN_REF_LIM         \
                               | FGC_WRN_REF_RATE_LIM    \
                               | FGC_WRN_CONFIG )

#define FGC_STRETCHED_WARNINGS ( FGC_WRN_MEAS            \
                               | FGC_WRN_I_RMS_LIM       \
                               | FGC_WRN_REF_LIM         \
                               | FGC_WRN_REF_RATE_LIM    \
                               | FGC_WRN_REG_ERROR       \
                               | FGC_WRN_V_ERROR)

#define FGC_LATCHED_FGC_HW_WRN ( FGC_LAT_DAC_FLT       \
                               | FGC_LAT_SPIVS_FLT     \
                               | FGC_LAT_SCIVS_EXP_FLT)

#define SYM_LST_CYC_STATUS &sym_lst_63_cyc_status[0]
#define SYM_LST_REF_TYPE   &sym_lst_63_ref[0]
#define SYM_LST_FAULTS     &sym_lst_63_flt[0]
#define SYM_LST_REF        sym_lst_63_ref

#define DSP_CODE           dsp_63_code
#define DSP_CODE_SIZE      DSP_63_CODE_SIZE



// ---------- External function declarations

//! Class specific initialization

void classInit(void);


// EOF
