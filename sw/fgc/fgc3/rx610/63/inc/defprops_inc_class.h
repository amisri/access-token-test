//! @file  defprops_inc_class.h
//! @brief Adds all the class-specific headers needed by defprops.h

#pragma once


#include <interFgc.h>
#include <log_class.h>

#include <function_class.h>
#include <prop_class.h>


// EOF
