//! @file  comHvPs.h
//! @brief Provides VSNOCABLE input polling function for ComHV-PS power converter
//!
//! The function provided by this header is used by the ComHV-PS converter.
//! The State Card sends encoded information to the FGC3 using spare VSNOCABLE input.
//! Purpose of the function is to decode this information and trigger necessary commands.

#pragma once



// ---------- External function declarations

//! Poll VSNOCABLE and simulate commands based on its high state duration
//!
//! Pulse-width modulation is used to encode three signals (OFF, RST, NOEXT) in VSNOCABLE input. The function polls this input,
//! decodes the signal values and triggers commands based on their changes.
//!
//! PWM period is 30 ms. In each period both, rising and falling, edges should be registered. Three consecutive periods not
//! fulfilling this condition will cause a warning and will be signalized in VS.EXT_CONTROL property.
//! Period length offset larger than 2 will be signalized in the same property.
//!
//! @todo Refactor the algorithm to use boolean values for signalizing falling and rising edges

void comHvPsPollVsNoCable(void);


// EOF