//! @file  log_class.h
//! @brief Logging related functions for class 63

#pragma once


// ---------- Includes

#include <stdint.h>

#include <bitmap.h>
#include <dpcom.h>



// ---------- External function declarations

//! This function disables a log
//!
//! @param[in]    log              Log to disable

static inline void logClassDisableLog(enum Dpcom_log_ignore_bitmask const log)
{
    setBitmap(dpcom.mcu.log.ignore_mask, log);
}


//! This function enables a log
//!
//! @param[in]    log              Log to enable

static inline void logClassEnableLog(enum Dpcom_log_ignore_bitmask const log)
{
    clrBitmap(dpcom.mcu.log.ignore_mask, log);
}


// EOF

