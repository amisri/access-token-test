//! @file  function_class.h
//! @brief Class specific handling of function arming and ancillary functions

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <cmd.h>



// ---------- External structures, unions and enumerations


//! Variables associated to properties

struct Function_props
{
    struct Ref_economy_dest
    {
        uint32_t      mode;               //!< REF.ECONOMY.DEST.MODE
        uint32_t      mask;               //!< REF.ECONOMY.DEST.MASK
    } economy_dest;

    uint32_t          fault_user;         //!< REF.FAULTS_USER

    uint16_t          reg_mode_init;      //!< REG.MODE_INIT   Initial regulation mode
};


//! Table function point struct contains a pair of time and reference values

struct Point
{
    float time;   //!< Time value
    float ref;    //!< Reference value
};



// ---------- External variable declarations

extern struct Function_props ref;



// ---------- External function declarations

//! Function module initialization

void functionClassInit(void);


//! Processes the arming of a function with the command S REF FUNC_TYPE,arg1,...
//!
//! @param[in] c             Command structure.
//! @param[in] func_type     Function type to arm.
//!
//! @retval FGC_OK_NO_RSP if it succeeds.

uint16_t functionClassSetRef(struct cmd * c, uint16_t func_type);


//! Processes the arming of a function with the command S REF.FUNC.TYPE type
//!
//! @param[in] c             Command structure.
//! @param[in] func_type     Function type to arm.
//!
//! @retval FGC_OK_NO_RSP if it succeeds.

uint16_t functionClassSetFuncType(struct cmd * c, uint16_t func_type);


//! Returns the function type as a string

char * functionClassGetStr(void);


// EOF
