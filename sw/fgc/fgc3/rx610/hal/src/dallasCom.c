//! @file    dallasCom.c
//! @brief   Functions for communicating with the dallas devices microcontroller


// ---------- Includes

#include <bitmap.h>
#include <dallasCom.h>
#include <m16c62_link.h>
#include <memmap_mcu.h>
#include <os.h>
#include <time_fgc.h>



// ---------- Constants

#define C62_MAX_ISR_US  50  // Maximum ISR time in uS



// ---------- Internal function declarations

static uint16_t dallasComSendIsrCmd(uint8_t cmd, uint8_t arg);



// ---------- External function definitions

void dallasComSwitchOn(void)
{
	CPU_RESET_P &= ~CPU_RESET_CTRL_C62OFF_MASK16;
}



void dallasComSwitchOff(void)
{
	CPU_RESET_P |= CPU_RESET_CTRL_C62OFF_MASK16;
}



void dallasComSlaveMode(void)
{
	C62_P = C62_SWITCH_PROG;
}



bool dallasComPowerOn(void)
{
	return testBitmap(C62_P, C62_POWER_MASK16) == true ? true : false;
}



bool dallasComBoot(void)
{
	return ((C62_P ^ C62_SWITCH_PROG) & 0x00FF) == 0xFF ? true : false;
}



bool dallasComTestCmdBusy(void)
{
	return testBitmap(C62_P, C62_CMDBUSY_MASK16) == true ? true : false;
}



uint16_t dallasComSendByte(uint8_t tx, uint8_t * rx)
{
    OS_CPU_SR cpu_sr;
    uint16_t reg;
    uint32_t t0;

    OS_ENTER_CRITICAL();

    C62_P = (uint16_t) tx;            // Send byte
    t0 = timeGetUs();

    OS_EXIT_CRITICAL();

    do
    {
        reg = C62_P;
    }
    while (!(reg & (C62_DATARDY_MASK16 | C62_CMDERR_MASK16)) && ((timeGetUsDiffNow(t0)) < C62_MAX_ISR_US));

    reg = C62_P;

    *rx = (reg & 0xFF);                 // Return data byte

    if (testBitmap(reg, C62_DATARDY_MASK16) == true)       // If data ready
    {
        return DALLAS_TASK_ERROR_OK;
    }

    if (testBitmap(reg, C62_CMDERR_MASK16) == true)        // If command error
    {
        return *rx == M16C62_NO_MORE_DATA ? DALLAS_TASK_ERROR_NO_MORE_DATA : DALLAS_TASK_ERROR_ISR_CMD_ERR;
    }

    return DALLAS_TASK_ERROR_ISR_CMD_TIMEOUT;       // Timeout
}



uint16_t dallasComSendCmd(enum DallasTask_commands cmd, uint8_t * rx)
{
	switch (cmd)
	{
		case DALLAS_TASK_CMD_SWITCH_PROG:
			return dallasComSendByte(C62_SWITCH_PROG, rx);

		case DALLAS_TASK_CMD_READ_IDS:
			return dallasComSendByte(C62_READ_IDS, rx);

		case DALLAS_TASK_CMD_SET_EXTERNAL_ID:
			return dallasComSendByte(C62_SET_EXTERNAL_ID, rx);

		case DALLAS_TASK_CMD_GET_BARCODE_FOR_EXT_ID:
			return dallasComSendByte(C62_GET_BARCODE_FOR_EXT_ID, rx);

		case DALLAS_TASK_CMD_GET_RESPONSE:
			return dallasComSendByte(C62_GET_RESPONSE, rx);

		case DALLAS_TASK_CMD_READ_TEMPS_BRANCH:
			return dallasComSendByte(C62_READ_TEMPS_BRANCH, rx);

        default:
            return DALLAS_TASK_ERROR_ISR_CMD_ERR;
	}
}



uint16_t dallasComBranchIsrCmd(uint8_t arg)
{
	return dallasComSendIsrCmd(C62_SET_BRANCH, arg);
}



uint16_t dallasComDeviceIsrCmd(uint8_t arg)
{
	return dallasComSendIsrCmd(C62_SET_DEV, arg);
}



// ---------- Internal function definitions

static uint16_t dallasComSendIsrCmd(uint8_t cmd, uint8_t arg)
{
    uint16_t errnum;
    uint8_t  rx;

    errnum = dallasComSendByte(cmd, &rx);

    if (errnum != DALLAS_TASK_ERROR_OK)
    {
        return errnum;
    }

    return dallasComSendByte(arg, &rx);
}


// EOF
