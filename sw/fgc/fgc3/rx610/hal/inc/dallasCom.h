//! @file    dallasCom.h
//! @brief   Functions for communicating with the dallas devices microcontroller


// ---------- Includes

#include <stdint.h>

#include <dallasTask.h>



// ---------- External function declarations

//! Turns on microcontroller.

void dallasComSwitchOn(void);


//! Turns off microcontroller.

void dallasComSwitchOff(void);


//! Tells microcontroller boot to run in slave mode.

void dallasComSlaveMode(void);


//! Tests if microcontroller power is ON.
//!
//! @retval true    Power is on.
//! @retval false   Power is off.

bool dallasComPowerOn(void);


//! Tests if the microcontroller responded to the command to switch to boot.
//!
//! @retval false   Not responding to command.
//! @retval true    Request to switch to boot was successful.

bool dallasComBoot(void);


//! Returns 0 if the previously given command has been completed.
//!
//! @retval true    Command not completed
//! @retval false   Command completed

bool dallasComTestCmdBusy(void);


//! Sends a byte to the microcontroller and waits up to C62_MAX_ISR_US for an immediate response
//!
//! @param  tx                      Sent byte
//! @param  rx                      Response byte
//!
//! @retval DLS_OK                  Data is ready
//! @retval DLS_NO_MORE_DATA        No more data
//! @retval DLS_ISR_CMD_ERR         Command error
//! @retval DLS_ISR_CMD_TIMEOUT     Timeout error

uint16_t dallasComSendByte(uint8_t tx, uint8_t * rx);


//! Sends a command to microcontroller.
//!
//! @param  cmd                     Command
//! @param  rx                      Response byte
//!
//! @retval DLS_OK                  Data is ready
//! @retval DLS_NO_MORE_DATA        No more data
//! @retval DLS_ISR_CMD_ERR         Command error
//! @retval DLS_ISR_CMD_TIMEOUT     Timeout error

uint16_t dallasComSendCmd(enum DallasTask_commands cmd, uint8_t * rx);


//! Send a microcontroller ISR command to set branch.
//!
//! @param  arg                     Branch number
//!
//! @retval DLS_OK                  Data is ready
//! @retval DLS_NO_MORE_DATA        No more data
//! @retval DLS_ISR_CMD_ERR         Command error
//! @retval DLS_ISR_CMD_TIMEOUT     Timeout error

uint16_t dallasComBranchIsrCmd(uint8_t arg);


//! Send a microcontroller ISR command to set device.
//!
//! @param  arg                     Device number
//!
//! @retval DLS_OK                  Data is ready
//! @retval DLS_NO_MORE_DATA        No more data
//! @retval DLS_ISR_CMD_ERR         Command error
//! @retval DLS_ISR_CMD_TIMEOUT     Timeout error

uint16_t dallasComDeviceIsrCmd(uint8_t arg);


// EOF