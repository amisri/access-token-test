//! @file  pulse_class.h
//! @brief Pulse managment

#pragma once


// ---------- Includes

#include <stdint.h>
#include <defconst.h>
#include <events.h>
#include <time_fgc.h>



// ---------- External structures, unions and enumerations

//! This structure contains the pulse limits.

struct Pulse_limits
{
    float  nominal;                                                //!< LIMITS.PULSE.NOMINAL
    float  pos;                                                    //!< LIMITS.PULSE.POS
    float  min;                                                    //!< LIMITS.PULSE.MIN
    float  neg;                                                    //!< LIMITS.PULSE.NEG
    float  error_warning;                                          //!< LIMITS.PULSE.ERR_WARNING
    float  error_fault;                                            //!< LIMITS.PULSE.ERR_FAULT
};


//! This structure contains the pulse reference.

struct Pulse_ref
{
    uint32_t func_play[FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];     //!< REF.FUNC.PLAY
    float    duration [FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];     //!< REF.PULSE.REF.DURATION
    int32_t  offset_us[FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];     //!< REF.PULSE.OFFSET_US
    float    ref      [FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];     //!< REF.PULSE.REF.VALUE
    float    armed    [FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];     //!< REF.ARMED.PULSE.VALUE
    float    local;                                                //!< REF.PULSE.LOCAL_REF
    uint32_t mode_rt_ref;                                          //!< MODE.RT
};


//! This structure contains the pulse status.

struct Pulse_log
{
    uint32_t status     [FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];   //!< LOG.PULSE.STATUS
    float    max_abs_err[FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];   //!< LOG.PULSE.MAX_ABS_ERR
    uint32_t counter    [2];                                       //!< LOG.PULSE.COUNTER
};


//! This structure contains the reported pulse values.

struct Pulse_report
{
    float   ref;                                                   //!< Pulse reference
    float   meas_i;                                                //!< Current pulse measurement
    float   meas_v;                                                //!< Voltage pulse measurement
    float   error;                                                 //!< Error between the reference and measurement.
    int32_t error_mav;                                             //!< Error between the reference and measurement in mA or mV.
};



// ---------- External variable definitions

extern struct Pulse_limits   pulse_limits;
extern struct Pulse_ref      pulse_ref;
extern struct Pulse_log      pulse_log;
extern struct Pulse_report   pulse_report;
extern uint32_t              pulse_units;



// ---------- External function declarations

//! Initializes the pulse

void pulseInit(void);


//! Processes the generation of a pulse using the DIG_SUP interface

void pulseProcess(void);


//! Sets a timing event to generate a pulse
//!
//! @param  start_us               Event start time
//! @param  cycle                  Cycle selectors

void pulseSetEvent(struct CC_us_time                const * start_us,
                   struct Events_data_payload_cycle const * cycle);


//! Sets the pulse reference value
//!
//! @param  sub_sel                Sub-device selector
//! @param  cyc_sel                Cycle selector
//! @param  ref                    Pulse reference value
//! @param  txId                   Transactional ID
//!
//! @return FGC_OK_NO_RSP          The reference is correct.
//! @return FGC_OUT_OF_LIMITS      The reference is out of limits.
//! @return FGC_BAD_CYCLE_SELECTOR The cycle selector is not valid.
//! @return FGC_INVALID_SUBDEV     The subdevice selector is not valid.

uint16_t pulseSetRef(uint32_t sub_sel, uint32_t cyc_sel, float ref, uint16_t txId);


//! A rms all cycles and sub devives from the values recovered from NVS

void pulseArmAll(void);


//! Sets the local reference value
//!
//! @param  ref               Local reference value
//!
//! @return FGC_OK_NO_RSP     The reference is correct.
//! @return FGC_OUT_OF_LIMITS The reference is out of limits.

uint16_t pulseSetRefLocal(float ref);


//! Applies dynamic economy to the next pulse

void pulseSetEconomy(void);


//! Returns the pulse amplitude
//!
//! @param  sub_sel                Sub-device selector
//! @param  cyc_sel                Cycle selector

//! @retval                        The pulse amplitude

float pulseGetAmplitude(uint32_t sub_sel, uint32_t cyc_sel);


//! Logs in the event log any state update of DIG_SUP_B channels

void pulseLogDigSup(void);


//! Returns a human readalble string of the pulse status
//!
//! @param  status            Pulse status in numeric format
//!
//! @retval                   String with the pulse status

char const * pulseGetStatusStr(uint8_t status);


// EOF
