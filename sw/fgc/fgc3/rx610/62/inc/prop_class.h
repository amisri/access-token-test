//! @file  prop_class.h
//! @brief FGC_62 class specific property initialisation

#pragma once


// ---------- Includes

#include <defconst.h>
#include <property.h>



// ---------- External structures, unions and enumerations

struct Prop_class_init_dynflag_timestamp_select
{
    struct prop           * prop;                   // Pointer to property
    enum timestamp_select   timestamp_select;       // dynamic flags
};



// ---------- External variable declarations

extern struct init_prop prop_class_init_nvs[];

extern uint32_t prop_class_init_spy_mpx[FGC_N_SPY_CHANS];


// The DF_TIMESTAMP_SELECT dynamic flag is set for all the properties in the
// following list and their children. The same list is also used in function
// CmdPrintTimestamp to read the timestamp selector.

extern struct Prop_class_init_dynflag_timestamp_select prop_class_init_dynflag_timestamp_select[];


// EOF
