//! @file  timingPulse.h
//! @brief Timing pulse generation using the supplemental digital I/O.

#pragma once


// ---------- Includes

#include <stdint.h>

#include <defconst.h>
#include <digitalIo.h>



// ---------- External structures, unions and enumerations

//! Contains timing related parameters used to generate the
//! timing signals to the state control card.

struct Timing_pulse
{
    int32_t vs_pulses[6];                //!< A0,A1,A3 pulses: VS.PULSE.TIMING
    int32_t fast_acq[2];                 //!< Fast acquisition window: MEAS.FAST_ACQ
};


//! Pulse channels.

enum Timing_pulse_edge
{
    TIMING_PULSE_EDGE_RISE,
    TIMING_PULSE_EDGE_FALL,
};


//! Pulse channels.

enum Timing_pulse_channel
{
    PULSE_CHARGE  = DIG_SUP_A_CHANNEL0,
    PULSE_START   = DIG_SUP_A_CHANNEL1,
    PULSE_ACQ     = DIG_SUP_A_CHANNEL2,
    PULSE_BOUNCER = DIG_SUP_A_CHANNEL3,
    PULSE_NUM     = 4
};



// ---------- External variable declarations

extern struct Timing_pulse timing_pulse;



// ---------- External function declarations

//! Sends a timing pulse to the state control card via the digital
//! supplementary outputs.
//!
//! @param channel     Digital supplementary channel
//! @param offset      Offset to apply to the time pulse

void timingPulseSendPulse(enum Timing_pulse_channel channel, int32_t offset);


//! Sets the logical pulse used to define the fast-sampling window.
//!
//! @param start       Rising edge of the fast-sampling window
//! @param width       Width of the fast-sampling window

void timingPulseSetFastAcq(uint32_t const start, uint32_t const width);


//! Checks if the rising or falling edge has been detected.
//!
//! @param channel       Digital supplementary channel
//! @param edge_time_us  Time in microseconds with respect to the event of the
//!                      rising or falling edge of the pulse.
//!
//! @return True         The edge was detected
//! @return False        The edge was not detected

bool timingPulseDetectEdge(enum Timing_pulse_channel     channel,
                           enum Timing_pulse_edge        edge,
                           int32_t               * const edge_time_us);


// EOF
