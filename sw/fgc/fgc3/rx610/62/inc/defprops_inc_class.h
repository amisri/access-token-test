//! @file  defprops_inc_class.h
//! @brief Adds all the class-specific headers needed by defprops.h

#pragma once


#include <libpolswitch.h>

#include <function_class.h>
#include <fbs_class.h>
#include <timingPulse.h>
#include <polSwitch_class.h>


// EOF
