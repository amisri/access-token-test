//! @file  polSwitch_class.h
//! @brief Class specific handling of the polarity switch

#pragma once


// ---------- Includes

#include <libpolswitch.h>



// ---------- External function declarations

//! Sets the polarity switch mode
//!
//! @param[in]   mode        Polarity switch mode

void polSwitchClassSetMode(enum POLSWITCH_mode mode);


//! Returns the polarity swtich state
//!
//! @return                 The polarity switch state

enum POLSWITCH_state polSwitchClassGetState(void);


//! Returns the polarity swtich faults
//!
//! @return                 The polarity switch faults

enum POLSWITCH_fault_bits polSwitchClassGetFaults(void);


//! Returns whether the polarity swtich is enabled
//!
//! @retval      True       The polarity switch is enabled
//! @retval      False      The polarity switch is disabled

bool polSwitchClassIsEnabled(void);


// EOF
