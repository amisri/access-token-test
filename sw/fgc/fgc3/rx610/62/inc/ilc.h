//! @file  ilc.c
//! @brief Iterative learning controller
//!
//! https://issues.cern.ch/browse/EPCCCS-8384
//!
//! Before cycle:
//! REF = REF_USER + REF_ILC
//!
//! After cycle:
//! ERR_ILC = REF_USER – MEAS
//! REF_ILC += K_INT * ERR_ILC
//!
//! Translation into q-domain
//! REF      = u(k)
//! REF_USER = r(k)
//! REF_ILC  = r’(k)
//! ERR_ILC  = r(k-1) – y(k-1)
//! REF_ILC += K_INT * ERR_ILC → r’(k) = r’(k-1) + ki *[r(k-1) – y(k-1) ]
//!
//! MEAS refers to the value acquired at the event time
//!
//! Assuming
//! y(k) = u(k)
//! u(k) = r(k) + r’(k)
//!
//! Complementary Sensitivity Function
//! y(k)  = r(k) ☺
//!
//! Calculations omitted but verified
//! Sensitivity Function
//! y(k)  = (q – 1) / [q - (1-ki)] dy(k)
//!
//! Pole in (1- ki ) so 0 < ki <  2
//! Zero in 1  ok
//! DC gain : zero  ☺ → it rejects step disturbance
//! ki can be chosen starting from the modulus margin mm according to ki = 2(1-mm)
//!
//! Although one gets a stable pole also with ki > 1 the reaction to an offset change would be
//! oscillating in time; with ki = 1 a step change in the offset would be rejected after exaxctly
//! one pulse.
//!
//! Calculations omitted but verified
//!
//! Calling this controller an ILC is a bit of a stretch, it was agreed to do so only to profit
//! of structures and codes already available. This controller is a 2-dof controller like the RST.
//! Indeed an RST implementation could be as follows:
//!
//! T(z^-1) = 1/ki [1 + (ki-1) z^-1]
//! R(z^-1) = z^-1
//! S(z^-1) = ki / (1 - z^-1)
//!
//! Settable properties:
//!
//! MODE.ILC            {ENABLED, DISABLED}
//! REF.ILC.K_INT       [0.0..0.9]           Maybe not required
//! REF.ILC.LOG_USER    [0..32]              For ILC_CYC log

#pragma once


// ---------- Includes

#include <stdint.h>
#include <defconst.h>

#include <pulse.h>



// ---------- External structures, unions and enumerations

//! Variables associated to properties

struct Ilc_props
{
    uint32_t mode;                                              //!< MODE.ILC           Controls when to use the ILC
    float    k_init;                                            //!< REF.ILC.K_INIT     ILC integrator gain
    uint32_t log_user;                                          //!< REF.ILC.LOG_USER   User to capture in the ILC_CYC log
    float    reference[FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];  //!< REF.ILC.REFERENCE  ILC reference
    float    error    [FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];  //!< REF.ILC.ERROR      ILC error
    uint32_t state    [FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];  //!< REF.ILC.STATE      ILC state
};



// ---------- External variable declarations

extern struct Ilc_props ilc_props;



// ---------- External function declarations

//! ILC initialization

void ilcInit(void);


//! ILC processing
//!
//! @param[in]     sub_sel           Sub-device selector
//! @param[in]     cyc_sel           Cycle selector
//! @param[in]     acq_cyc_sel       Acquisition cycle selector
//! @param[in]     ref_user          Reference defined by the user
//! @param[in]     meas              Measurement

void ilcProcess(uint32_t sub_sel, uint32_t cyc_sel, uint32_t acq_cyc_sel, float ref_user, float meas);


//! Returns the reference to apply to the converter
//!
//! If MODE.ILC is enabled, this function adds to the user reference
//! the ILC reference. It clips the resulting value if need be
//!
//! @param[in]     sub_sel           Sub-device selector
//! @param[in]     cyc_sel           Cycle selector
//! @param[in]     ref_user          User reference
//! @param[in]     limits            Pulse limits
//!
//! @returns       The clipped reference

float ilcClipGetReference(uint32_t              sub_sel,
                          uint32_t              cyc_sel,
                          float                 ref_user,
                          struct Pulse_limits * limits);


//! Resets the data for all sub-devices and cycle selectors

void ilcResetDataAll(void);


// EOF
