//! @file  function_class.h
//! @brief Class specific handling of function arming and ancillary functions

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External structures, unions and enumerations

//! Variables associated to properties

struct Function_props
{
    struct Ref_economy_dest
    {
        uint32_t      mode;               //!< REF.ECONOMY.DEST.MODE
        uint32_t      mask;               //!< REF.ECONOMY.DEST.MASK
    } economy_dest;

    uint32_t          fault_user;         //!< REF.FAULTS_USER
};



// ---------- External variable declarations

extern struct Function_props ref;


// EOF
