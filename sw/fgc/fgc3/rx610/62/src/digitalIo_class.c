//! @file   digitalIo_class.c
//! @brief  Class specific digital inputs and outputs handling


// ---------- Includes

#include <stdint.h>

#include <bitmap.h>
#include <crate.h>
#include <digitalIo.h>



// ---------- Platform/class specific function definitions

void digitalIoSupInitClass(void)
{
    // Based on https://wikis.cern.ch/display/TEEPCCCE/FGC3+Definition+of+DIG-SUP+with+RegFGC3+cards

    // Bank A: timing pulses
    // Bank B: data

    for (uint16_t channel = 0; channel < 4; channel++)
    {
        DIG_SUP_A_CHAN_CTRL_A[channel] = DIG_SUP_CTRL_OUTMPX_PULSE;
        DIG_SUP_B_CHAN_CTRL_A[channel] = DIG_SUP_CTRL_OUTMPX_DATA;
    }

    // The Modulator requires the pulse timing in A1 and A3 to be inverted.

    if (crateGetType() == FGC_CRATE_TYPE_KLYSTRON_MOD)
    {
        setBitmap(DIG_SUP_A_CHAN_CTRL_A[DIG_SUP_A_CHANNEL1], DIG_SUP_A_CHAN_CTRL_INVERT_MASK16);
        setBitmap(DIG_SUP_B_CHAN_CTRL_A[DIG_SUP_A_CHANNEL1], DIG_SUP_B_CHAN_CTRL_INVERT_MASK16);

        setBitmap(DIG_SUP_A_CHAN_CTRL_A[DIG_SUP_A_CHANNEL3], DIG_SUP_A_CHAN_CTRL_INVERT_MASK16);
        setBitmap(DIG_SUP_B_CHAN_CTRL_A[DIG_SUP_A_CHANNEL3], DIG_SUP_B_CHAN_CTRL_INVERT_MASK16);
    }
}


// EOF
