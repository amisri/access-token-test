//! @file  pulse.c
//! @brief Pulse managment


// ---------- Includes

#include <defprops.h>

#include <bitmap.h>
#include <crate.h>
#include <cycTime.h>
#include <digitalIo.h>
#include <dpcls.h>
#include <logEvent.h>
#include <events.h>
#include <fgc_errs.h>
#include <ilc.h>
#include <msTask.h>
#include <nvs.h>
#include <pc_state.h>
#include <polSwitch.h>
#include <pub.h>
#include <pulse.h>
#include <stateVs.h>
#include <status.h>
#include <time_fgc.h>
#include <timingPulse.h>
#include <transaction.h>



// ---------- Internal structures, unions and enumerations

//! Type for state machine state function returning state number

typedef uint32_t PulseState(bool first_call);

//! Type for pointer to transition function returning pointer to state function

typedef PulseState * PulseTrans(void);

//! Internal local variables for the module pulse

struct Pulse_info
{
    struct CC_us_time     start_us;         //!< Event time
    struct Events_data_payload_cycle cycle; //!< Cycle selectors
    int32_t               offset_us;        //!< Pulse offset
};


struct Pulse
{
    PulseState          * stateFunc;        //!< Pointer to the active state function
    uint32_t              state;            //!< Current pulse state

    struct Pulse_info     info[2];
    struct Pulse_info   * current;          //!< Information of the current pulse
    struct Pulse_info   * next;             //!< Information of the next pulse

    float               * meas;             //!< Pointer to acquisition measurement (current or voltage)

    char const          * digsup_name[4];   //!< Digital supplementary name
    char const          * pc_names[4];      //!< Timing pulse names specific for each converter

    bool                  play;             //!< True if the reference is none zero
    bool                  new_event;        //!< True when a new event has been registered

    bool                  dynamic_economy;  //!< When true, do not generate the pulse
};


//! Pulse states

enum Pulse_states
{
    PULSE_STATE_WAITING,
    PULSE_STATE_SETTING,
    PULSE_STATE_REPORTING,
};



// ---------- External variable definitions

struct Pulse_limits   pulse_limits;
struct Pulse_ref      pulse_ref;
struct Pulse_log      pulse_log;
struct Pulse_report   pulse_report;
uint32_t              pulse_units;
char                  pulseLogProp[EVTLOG_PROPERTY_LEN];
char                  pulseLogValue[EVTLOG_VALUE_LEN];



// ---------- Internal variable definitions

static struct Pulse pulse = {  .digsup_name = { "B0", "B1", "B2", "B3" },
                               .pc_names    = { "CHARGE-ENABLED", "START",  "ACQ", "BOUNCER" },
                            };



// ---------- Internal function definitions

static void pulseGetReference(uint32_t const   sub_sel,
                              uint32_t const   cyc_sel,
                              float          * pulse_reference_user,
                              float          * pulse_reference,
                              float          * pulse_duration)
{
    float reference_user = 0.0F;
    float reference      = 0.0F;
    float duration       = 0.0F;

    if (pcStateTest(FGC_STATE_LT_BLOCKING_BIT_MASK) == true)
    {
        // Zero REF.PULSE.REF_LOCAL

        pulse_ref.local = 0.0;
    }
    else if (pcStateTest(FGC_STATE_TO_CYCLING_BIT_MASK | FGC_STATE_CYCLING_BIT_MASK) == true)
    {
        // If REF.PULSE.REF_LOCAL is non-zero overwrite REF.PULSE.REF.VALUE

        if (fabs(pulse_ref.local) > 0.0)
        {
            // Use the non-PPM user to test the ILC without affecting
            // the operational settings, except if DEVICE.PPM is DISABLED

            duration       = pulse_ref.duration[NON_MULTI_PPM_SUB_DEV][NON_PPM_USER];
            reference_user = pulse_ref.local;
            reference      = ilcClipGetReference(NON_PPM_USER,
                                                 NON_MULTI_PPM_SUB_DEV,
                                                 reference_user,
                                                 &pulse_limits);

            // Alwasy disable dynamic economy

            pulse.dynamic_economy = false;
        }
        else if (pulse.dynamic_economy == false)
        {
            if (pulse_ref.func_play[sub_sel][cyc_sel] == FGC_CTRL_ENABLED)
            {
                float rt_ref = pulse_ref.mode_rt_ref == FGC_MODE_RT_GATEWAY ? dpcom.mcu.ref.rt : 0.0F;

                duration       = pulse_ref.duration[sub_sel][cyc_sel];
                reference_user = pulse_ref.armed[sub_sel][cyc_sel] + rt_ref;
                reference      = ilcClipGetReference(sub_sel,
                                                    cyc_sel,
                                                    reference_user,
                                                    &pulse_limits);
            }
        }
    }
    else
    {
        // Between BLOCKING and TO_CYCLING the reference must be zero
        // even if REF.PULSE.REF_LOCAL != 0
    }

    statusEvaluateWarnings(FGC_WRN_REF_LOCAL, fabs(pulse_ref.local) > 0.0);

    *pulse_reference_user = reference_user;
    *pulse_reference      = reference;
    *pulse_duration       = duration;
}



static uint32_t pulseGetStatus(void)
{
    // Return the pulse status based on the following prioritized condition:
    //
    // OK            : the pulse was correctly generated or the state is below TO_CYCLING
    // POL_SWITCH    : the polarity switch is not in the expected position
    // STATUS_TIMEOUT: no acquisition was triggered
    // NOT_READY     : the VS_READY input is not active during ## consecutive cycles
    // DISABLED      : REF.FUNC.PLAY = DISABLED
    // FAULT         : abs(ref - meas) > Fault threshold
    // WARNING       : abs(ref - meas) > Warning threshold
    // ECONOMY       : dynamic economy asserted
    //
    // VS_READY signals the status of the power converter. It might happen
    // that the voltage source is not ready to generate the pulse with the
    // specified reference because the capacitors are not yet charged to a
    // nominal voltage.

    // Clear the unlatched fault. The latched faults FAULTS will only be
    // cleared if a reset is triggered

    statusClrFaults(FGC_FLT_REG_ERROR);

    uint32_t status;

    if (pcStateTest(FGC_STATE_TO_CYCLING_BIT_MASK | FGC_STATE_CYCLING_BIT_MASK) == false)
    {
        status = 0;
    }
    else if (polSwitchGetFaults() != 0)
    {
        vs.req_polarity = 0;
        status = FGC_PULSE_STATUS_POL_SWITCH;
        statusSetWarnings(FGC_WRN_POL_SWITCH);
    }
    else if (pulse.dynamic_economy == true)
    {
        status = FGC_PULSE_STATUS_DYN_ECO;
    }
    else if (pulse.play == false)
    {
        status = FGC_PULSE_STATUS_DISABLED;
    }
    else if (dpcls.dsp.meas.acq_ready == false)
    {
        status = FGC_PULSE_STATUS_TIMEOUT;
        statusSetWarnings(FGC_WRN_REG_ERROR);
    }
    else if (testBitmap(digitalIoGetInputs(), DIG_IP_VSREADY_MASK32) == false)
    {
        status = FGC_PULSE_STATUS_NOT_READY;
    }
    else if (   pulse_limits.error_fault != 0.0
             && pulse_report.error        > pulse_limits.error_fault)
    {
        status = FGC_PULSE_STATUS_REG_FAULT;
        statusSetFaults(FGC_FLT_REG_ERROR);
    }
    else if (   pulse_limits.error_warning != 0.0
             && pulse_report.error          > pulse_limits.error_warning)
    {
        status = FGC_PULSE_STATUS_REG_WARNING;
        statusSetWarnings(FGC_WRN_REG_ERROR);
    }
    else
    {
        status = FGC_PULSE_STATUS_REG_OK;
    }

    return status;
}



static uint16_t pulseTest(uint32_t const sub_sel, uint32_t const cyc_sel)
{
    float ref = pulse_ref.ref[sub_sel][cyc_sel];

    if (ref != 0.0)
    {
        if (ref > 0.0)
        {
            if (   ref > pulse_limits.pos
                || ref < pulse_limits.min)
            {
                return FGC_OUT_OF_LIMITS;
            }
        }
        else
        {
            if (   ref < pulse_limits.neg
                || ref > -pulse_limits.min)
            {
                return FGC_OUT_OF_LIMITS;
            }
        }
    }

    return FGC_OK_NO_RSP;
}



static uint16_t pulseCommit(uint32_t           const   sub_sel,
                            uint32_t           const   cyc_sel,
                            struct CC_us_time  const * commit_time)
{
    // commit_time is not used in class 62

    pulse_ref.armed[sub_sel][cyc_sel] = pulse_ref.ref[sub_sel][cyc_sel];

    pubPublishProperty(&PROP_REF_PULSE_REF_VALUE, sub_sel, cyc_sel, true);

    return FGC_OK_NO_RSP;
}



static uint16_t pulseRollback(uint32_t const sub_sel, uint32_t const cyc_sel)
{
    pulse_ref.ref[sub_sel][cyc_sel] = pulse_ref.armed[sub_sel][cyc_sel];

    pubPublishProperty(&PROP_REF_PULSE_REF_VALUE, sub_sel, cyc_sel, true);

    return FGC_OK_NO_RSP;
}



static void pulseAddEventLog(uint8_t const status)
{
    logEventStorePulse(&pulse.current->start_us,
                       pulseGetStatusStr(status),
                       pulse.current->offset_us,
                       dpcls.mcu.pulse.ref.user,
                       *pulse.meas);
}



static void pulseCheckExternalEvent(void)
{
    static bool prev_dig_sup_bo = false;

    // For external triggering the pulse sequence starts with the rising edge of B0

    if (prev_dig_sup_bo == false && digitalIoSupTestRiseEdge(DIG_SUP_B_CHANNEL0) == true)
    {
        struct CC_us_time start_us = ms_task.time_us;

        // Add the delay configured in VS.PULSE.TIMING for the rising edge of B0

        timeUsAddOffset(&start_us, -(timing_pulse.vs_pulses[0]));

        struct Events_data_payload_cycle cycle = { .sub_sel_ref = 0,
                                                   .cyc_sel_ref = 1,
                                                   .sub_sel_acq = 0,
                                                   .cyc_sel_acq = 1};

        pulseSetEvent(&start_us, &cycle);
    }

    prev_dig_sup_bo = digitalIoSupTestRiseEdge(DIG_SUP_B_CHANNEL0);
}



// ------------------------------- Generic STATE FUNCTIONS --------------------------------

static uint32_t pulseStateWA(bool const firstCall)
{
    if (firstCall == true)
    {
        // Clear the dynamic economy flag. If the pulse is to be ignored a
        // new event FGC_EVT_NEXT_DEST must be sent

        pulse.dynamic_economy = false;
    }

    return PULSE_STATE_WAITING;
}



static uint32_t pulseStateSE(bool const firstCall)
{
    if (firstCall == false)
    {
        return PULSE_STATE_SETTING;
    }

    // Make sure there is no stale acquisition

    dpcls.dsp.meas.acq_ready = false;

    // Switching to the next info pointer is protected from a race condition in
    // pulseSetEvent() with pulse.new_event

    struct Pulse_info * tmp = pulse.next;

    pulse.next    = pulse.current;
    pulse.current = tmp;

    __sync_synchronize();

    pulse.new_event = false;

    struct Events_data_payload_cycle const * cycle = &pulse.current->cycle;

    int32_t  offset_us = pulse_ref.offset_us[cycle->sub_sel_ref][cycle->cyc_sel_ref];

    // Use the start time as the reference for the fast acquisition window without
    // taking into account REF.PULSE.OFFSET to make sure the time origin of all
    // devices is the same: https://issues.cern.ch/browse/CCP-52

    dpcls.mcu.pulse.fast_acquisition.start = pulse.current->start_us;
    dpcls.mcu.pulse.fast_acquisition.end   = pulse.current->start_us;

    // Add the offset to the start of the pulse

    timeUsAddOffset(&pulse.current->start_us, offset_us);

    // Calculate the delay based on the start time and the current time

    struct CC_us_time now = { .secs.abs = timeGetUtcTime(),
                              .us = timeGetUtcTimeMs() * 1000
                            };
    int32_t time_till_event = timeUsDiff(&now, &pulse.current->start_us);

    if (time_till_event < 0)
    {
         // @TODO output an error?

        return PULSE_STATE_REPORTING;
    }

    // Set up the TimeTillEvent register

    if (eventsGetSource() != EVENTS_SOURCE_EXTERNAL)
    {
        cycTimeSetTimeTillEvent(time_till_event, false);
    }

    pulse.current->offset_us = offset_us;

    // Set the pulse reference

    float pulse_reference_user;
    float pulse_reference;
    float pulse_duration;

    pulseGetReference(cycle->sub_sel_ref, cycle->cyc_sel_ref, &pulse_reference_user, &pulse_reference, &pulse_duration);

    // Set the timing pulses on DIG SUP

    // Always send the acquisition timing pulse even when the converter
    // is OFF or the reference is 0.0

    timingPulseSendPulse(PULSE_ACQ, 0);

    // Hardcode the fast-acquisition window to +-20 ms around the event

    timeUsAddOffset((struct CC_us_time *)&dpcls.mcu.pulse.fast_acquisition.start, -20000);
    timeUsAddOffset((struct CC_us_time *)&dpcls.mcu.pulse.fast_acquisition.end  ,  20000);

    dpcls.mcu.pulse.fast_acquisition.ready = true;

    // Send the converter timing pulses if the reference is non-zero

    if (fabs(pulse_reference) > 0.0f)
    {
        pulse.play = true;

        // Set the timing pulses taking into account the offset due to the pulse duration
        // REF.PULSE.DURATION, which is in second. Multiple by 1,000,000 to convert into
        // microseconds and devide by two to center the pulse.

        int32_t offset_us = -(int32_t)(pulse_duration * 500000.0F);

        timingPulseSendPulse(PULSE_CHARGE , offset_us);
        timingPulseSendPulse(PULSE_START  , offset_us);
        timingPulseSendPulse(PULSE_BOUNCER, offset_us);

        // Change the polarity switch if need be

        if (polSwitchIsEnabled() == true)
        {
            if (pulse_reference > 0.0)
            {
                polSwitchSetMode(POLSWITCH_MODE_POSITIVE);
            }
            else if (pulse_reference < 0.0)
            {
                polSwitchSetMode(POLSWITCH_MODE_NEGATIVE);
            }
        }
    }
    else
    {
        pulse.play = false;
    }

    dpcls.mcu.pulse.ref.user     = pulse_reference_user;
    dpcls.mcu.pulse.ref.act      = pulse_reference;
    dpcls.mcu.pulse.ref.duration = pulse_duration;
    dpcls.mcu.pulse.ref.time_us  = pulse.current->start_us;
    dpcls.mcu.pulse.nominal      = pulse_limits.nominal;

    __sync_synchronize();

    dpcls.mcu.pulse.ref.ready = true;

    return PULSE_STATE_SETTING;
}



static uint32_t pulseStateRE(bool const firstCall)
{
    // If there is a timeout dpcls.dsp.meas.i_load could not be updated by the DSP
    // so force this value to zero

    if (dpcls.dsp.meas.acq_ready == false)

    {
        dpcls.dsp.meas.i_load = 0.0F;
        dpcls.dsp.meas.v_load = 0.0F;
    }

    // Update the pulse information to be reported

    pulse_report.ref       = dpcls.mcu.pulse.ref.user;
    pulse_report.meas_i    = dpcls.dsp.meas.i_load;
    pulse_report.meas_v    = dpcls.dsp.meas.v_load;
    pulse_report.error     = pulse_report.ref - *pulse.meas;
    pulse_report.error_mav = (int32_t)(1000.0 * pulse_report.error);
    pulse_report.error_mav = MAX(INT16_MIN, pulse_report.error_mav);
    pulse_report.error_mav = MIN(pulse_report.error_mav, INT16_MAX);
    pulse_report.error     = fabs(pulse_report.error);

    // Increment the counts of pulses where VS_READY was not asserted

    if (   dpcls.mcu.pulse.ref.act  > 0.0f
        && vs.vs_ready_timeout     != 0
        && testBitmap(digitalIoGetInputs(), DIG_IP_VSREADY_MASK32) == false)
    {
        if (++vs.vs_not_rdy_count >= vs.vs_ready_timeout)
        {
            statusSetFaults(FGC_FLT_REG_ERROR);
        }
    }
    else
    {
        vs.vs_not_rdy_count = 0;
    }

    statusClrWarnings(FGC_WRN_POL_SWITCH | FGC_WRN_REG_ERROR);

    uint32_t status = pulseGetStatus();

    // Update the pulse status

    uint32_t sub_sel_acq = pulse.current->cycle.sub_sel_acq;
    uint32_t cyc_sel_acq = pulse.current->cycle.cyc_sel_acq;

    pulse_log.max_abs_err[sub_sel_acq][cyc_sel_acq] = pulse_report.error;
    pulse_log.status     [sub_sel_acq][cyc_sel_acq] = status;

    // Update the pulse counter LOG.PULSE.COUNTER if the pulse was generated
    // This property is logged in Timber and used by the Magnet team (EPCCCS-7009)

    if (*pulse.meas > 0.01)
    {
        pulse_log.counter[(*pulse.meas <= (pulse_limits.pos / 2)) ? 0 : 1]++;
    }

    // Inform the DSP

    dpcls.mcu.pulse.status = status;

    // Add an entry in the event log with the pulse information

    pulseAddEventLog(status);

    // Publish status properties

    pubPublishProperty(&PROP_REF_CYC_STATUS,        sub_sel_acq, cyc_sel_acq, false);
    pubPublishProperty(&PROP_LOG_PULSE_STATUS,      sub_sel_acq, cyc_sel_acq, false);
    pubPublishProperty(&PROP_LOG_PULSE_MAX_ABS_ERR, sub_sel_acq, cyc_sel_acq, false);

    // Update the ILC

    uint32_t sub_sel_ilc;
    uint32_t cyc_sel_ilc;
    float    ref_ilc;

    // If the reference is REF.PULSE.REF_LOCAL, use the non-PPM user. This
    // allows testing the ILC without affecting the operational settings,
    // except if DEVICE.PPM is DISABLED

    if (pulse_ref.local == 0.0F)
    {
        sub_sel_ilc = pulse.current->cycle.sub_sel_ref;
        cyc_sel_ilc = pulse.current->cycle.cyc_sel_ref;
        ref_ilc     = dpcls.mcu.pulse.ref.user;
    }
    else
    {
        sub_sel_ilc = 0;
        cyc_sel_ilc = 0;
        ref_ilc     = pulse_ref.local;
    }

    ilcProcess(sub_sel_ilc, cyc_sel_ilc, cyc_sel_acq, ref_ilc, *pulse.meas);

    // Acknowledge the DSP

    dpcls.dsp.meas.acq_ready = false;

    return PULSE_STATE_REPORTING;
}



// ------------------------------- Generic TRANSITION FUNCTIONS --------------------------------

static PulseState * WAtoSE(void)
{
    // Condition: An event has been triggered by Gateway (CERN) or external labs (DIG_SUP_B4)

    return (pulse.new_event == true ? pulseStateSE : NULL);
}


static PulseState * SEtoRE(void)
{
    // Condition: the pulse has been generated or the event is in the past

    struct CC_us_time now = { .secs.abs = timeGetUtcTime(),
                              .us = timeGetUtcTimeMs() * 1000
                            };

    // The DSP must acknowldge the acquisition 10 ms after the event time

    timeUsAddOffset(&now, -10000);

    return (  (   dpcls.dsp.meas.acq_ready == true
               || timeUsGreaterThan(&now, &pulse.current->start_us) == true)
            ? pulseStateRE
            : NULL);
}



static PulseState * REtoWA(void)
{
    // Condition: always true

    return pulseStateWA;
}



//! Pulse transition functions : called in priority order, from left to right, return the state function.

static PulseTrans * pulseTransitions[][1 + 1] =
{
    /*  0.WA */ { WAtoSE, NULL },
    /*  1.SE */ { SEtoRE, NULL },
    /*  2.RE */ { REtoWA, NULL },
};



static bool pulseProcessStates(void)
{
    bool          retval;
    PulseState  * nextState        = NULL;
    PulseTrans  * transition       = NULL;
    PulseTrans ** stateTransitions = &pulseTransitions[pulse.state][0];

    // Scan transitions for the current state, testing each condition in priority order

    while (   (transition = *(stateTransitions++)) != NULL
           && (nextState  = (*transition)())       == NULL)
    {
        ;
    }

    // If a transition condition was true then switch to the new state

    if (nextState != NULL)
    {
        pulse.state     = nextState(true);
        pulse.stateFunc = nextState;

        retval = nextState != pulseStateWA;
    }
    else
    {
        pulse.stateFunc(false);

        retval = false;
    }

    return retval;
}



// ---------- External function definitions

void pulseInit(void)
{
    switch (crateGetType())
    {
        case FGC_CRATE_TYPE_KLYSTRON_MOD:
        case FGC_CRATE_TYPE_HMINUS_DISCAP:
            pulse.meas  = (float *)&pulse_report.meas_v;
            pulse_units = FGC_REG_MODE_UNITS_V;
            break;

        default:
            pulse.meas  = (float *)&pulse_report.meas_i;
            pulse_units = FGC_REG_MODE_UNITS_A;
            break;
    }

    pulse.stateFunc = pulseStateWA;
    pulse.state     = PULSE_STATE_WAITING;
    pulse.new_event = false;

    pulse.next    = &pulse.info[0];
    pulse.current = &pulse.info[1];

    // Link the functions used by the transactional module

    transactionInit(&pulseTest, &pulseCommit, &pulseRollback, NULL);
}



void pulseProcess(void)
{
    if (testBitmap(events_prop.ref_event, FGC_EVENT_CYC_EXTERNAL) == true)
    {
        pulseCheckExternalEvent();
    }

    // Transition through all states as soon as possible. This is important
    // for the 10Hz Mididiscap and Maxidicap which cannot afford to wait
    // 5ms per state. Otherwise, there might not be enough time to charge
    // their capacitors.
    // At the same time, do not transition consecutievely past WAIT to
    // prevent SET to update TIME_TILL_EVT before the DIG_SUPs time windows
    // have completed (see pulseProcessStates())

    while (pulseProcessStates() == true)
    {
        ;
    }
}



void pulseArmAll(void)
{
    memcpy(pulse_ref.armed, pulse_ref.ref, FGC_MAX_SUB_DEVS * FGC_MAX_USER_PLUS_1 * sizeof(float));
}



void pulseSetEvent(struct CC_us_time                const * start_us,
                   struct Events_data_payload_cycle const * cycle)
{
    // Return silentely if the previous event has not been processed
    // Is this the correct thing to do?

    if (pulse.new_event == true)
    {
        return;
    }

    pulse.next->start_us = *start_us;
    pulse.next->cycle    = *cycle;

    __sync_synchronize();

    pulse.new_event = true;
}



uint16_t pulseSetRef(uint32_t const sub_sel,
                     uint32_t const cyc_sel,
                     float    const ref,
                     uint16_t const txId)
{
    // To accept a cycle selector DEVICE.PPM must be ENABLED. A cycle selector
    // of 0 is only acceptable if DEVICE.PPM is DISABLED.

    if (   (cyc_sel != NON_PPM_USER && dpcom.mcu.device_ppm == FGC_CTRL_DISABLED)
        || (cyc_sel == NON_PPM_USER && dpcom.mcu.device_ppm == FGC_CTRL_ENABLED))
    {
        return FGC_BAD_CYCLE_SELECTOR;
    }

    // To accept a sub-device selector DEVICE.MULTI_PPM must be ENABLED

    if (sub_sel != NON_MULTI_PPM_SUB_DEV && dpcom.mcu.device_multi_ppm == FGC_CTRL_DISABLED)
    {
        return FGC_INVALID_SUBDEV;
    }

    pulse_ref.ref[sub_sel][cyc_sel] = ref;

    uint16_t errnum = FGC_OK_NO_RSP;

    if (transactionGetId(sub_sel, cyc_sel) == 0)
    {
        // Set reference outside a transaction

        if ((errnum = pulseTest(sub_sel, cyc_sel)) == FGC_OK_NO_RSP)
        {
            pulseCommit(sub_sel, cyc_sel, NULL);
        }
    }

    return errnum;
}



uint16_t pulseSetRefLocal(float const ref)
{
    if (ref > 0.0)
    {
        if (   ref > pulse_limits.pos
            || ref < pulse_limits.min)
        {
            return FGC_OUT_OF_LIMITS;
        }
    }
    else if (ref < 0.0)
    {
        if (   ref < pulse_limits.neg
            || ref > -pulse_limits.min)
        {
            return FGC_OUT_OF_LIMITS;
        }
    }
    else
    {
        // 0.0 is used to reset REF.PULSE.REF_LOCAL
    }

    pulse_ref.local = ref;

    return FGC_OK_NO_RSP;
}



void pulseSetEconomy(void)
{
    pulse.dynamic_economy = true;
}



float pulseGetAmplitude(uint32_t const sub_sel, uint32_t const cyc_sel)
{
    return pulse_ref.armed[sub_sel][cyc_sel];
}



void pulseLogDigSup(void)
{
    for (enum Timing_pulse_channel channel = 0; channel < PULSE_NUM; ++channel)
    {
        int32_t edge_offset;

        if (timingPulseDetectEdge(channel, TIMING_PULSE_EDGE_RISE, &edge_offset) == true)
        {
            struct CC_us_time edge_time_us = pulse.current->start_us;

            timeUsAddOffset(&edge_time_us, -edge_offset);

            logEventStoreTimingEdge(&edge_time_us, pulse.digsup_name[channel], pulse.pc_names[channel], true);

            // Communicate the DSP the precise time the raising edge for the acquisition was detected

            if (channel == PULSE_ACQ)
            {
                dpcls.mcu.pulse.acquisition.time_us = edge_time_us;
                __sync_synchronize();
                dpcls.mcu.pulse.acquisition.ready   = true;
            }
        }

        if (timingPulseDetectEdge(channel, TIMING_PULSE_EDGE_FALL, &edge_offset) == true)
        {
            struct CC_us_time edge_time_us = pulse.current->start_us;

            timeUsAddOffset(&edge_time_us, -edge_offset);

            logEventStoreTimingEdge(&edge_time_us, pulse.digsup_name[channel], pulse.pc_names[channel], false);
        }
    }
}



char const * pulseGetStatusStr(uint8_t const status)
{
    switch (status)
    {
        case 0:                            return "";                                   break;
        case FGC_PULSE_STATUS_REG_OK:      return SYM_TAB_CONST[STC_REG_OK].key.c;      break;
        case FGC_PULSE_STATUS_REG_WARNING: return SYM_TAB_CONST[STC_REG_WARNING].key.c; break;
        case FGC_PULSE_STATUS_REG_FAULT:   return SYM_TAB_CONST[STC_REG_FAULT].key.c;   break;
        case FGC_PULSE_STATUS_NOT_READY:   return SYM_TAB_CONST[STC_NOT_READY].key.c;   break;
        case FGC_PULSE_STATUS_TIMEOUT:     return SYM_TAB_CONST[STC_TIMEOUT].key.c;     break;
        case FGC_PULSE_STATUS_POL_SWITCH:  return SYM_TAB_CONST[STC_POL_SWITCH].key.c;  break;
        case FGC_PULSE_STATUS_DISABLED:    return SYM_TAB_CONST[STC_DISABLED].key.c;    break;
        case FGC_PULSE_STATUS_DYN_ECO:     return SYM_TAB_CONST[STC_DYN_ECO].key.c;     break;
        default:                           return SYM_TAB_CONST[STC_INVALID].key.c;     break;
    }
}


// EOF
