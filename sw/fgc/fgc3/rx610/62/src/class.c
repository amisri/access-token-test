//! @file  class.c
//! @brief This file contains all class related definitions


// ---------- Includes

#include <hash.h>


// Instantiate variables in headers with no associated source files

#define OS_GLOBALS
#define SHARED_MEMORY_GLOBALS
#define FGC_PARSER_CONSTS
#define FGC_COMPDB_GLOBALS
#define FGC_SYSDB_GLOBALS
#define FGC_DIMDB_GLOBALS
#define CLASS_GLOBALS
#define FGC_TASK_TRACE_GLOBALS
#define PC_STATE_GLOBALS
#define CRATE_GLOBALS
#define DEFPROPS
#define DEFSYMS


#include <os.h>

#include <sharedMemory.h>

#include <class.h>
#include <crate.h>
#include <logCycle.h>
#include <dpcls.h>
#include <dpcom.h>
#include <dpcmd.h>
#include <ilc.h>
#include <modePc.h>
#include <pc_state.h>
#include <polSwitch_class.h>
#include <pulse.h>
#include <statePc.h>
#include <taskTrace.h>
#include <transaction.h>

#include <fgc_parser_consts.h>
#include <defprops.h>
#include <defsyms.h>



// ---------- External variable definitions

volatile struct Dpcom __attribute__((section("dpcom"))) dpcom;
volatile struct Dpcls __attribute__((section("dpcls"))) dpcls;

OS_SEM * dpcmd_sem;



// ---------- Platform/class specific function definitions

void classInit(void)
{
    interFgcInit((struct Inter_fgc_mgr *)&dpcls.inter_fgc_mgr);
    pulseInit();
    ilcInit();
    logCycleInit();

    // Default initialisations

    vs.present              = FGC_CTRL_ENABLED;
    dpcls.oasis.subsampling = 1;
}


// EOF
