//! @file  timing_pulse_class.c
//! @brief Timing pulse generation using the supplemental digital I/O.

#define TIMING_PULSE_CLASS_GLOBALS


// ---------- Includes

#include <timingPulse.h>
#include <cycTime.h>
#include <digitalIo.h>


// ---------- Internal structures, unions and enumerations

//! This structure includes the rising and falling edge of a timing pulse.

struct Pulse_edges
{
    int32_t  rise;       //!< Delay wrt the event of the rising edge of the pulse.
    int32_t  fall;       //!< Delay wrt the event of the falling edge of the pulse.
};



// ---------- External variable definitions

struct Timing_pulse timing_pulse;



// ---------- External function definitions

void timingPulseSendPulse(enum Timing_pulse_channel const channel, int32_t const offset)
{
    struct Pulse_edges pulse;

    switch (channel)
    {
        case DIG_SUP_A_CHANNEL0:
            pulse.rise = timing_pulse.vs_pulses[0] + offset;
            pulse.fall = timing_pulse.vs_pulses[1] + offset;
            break;

        case DIG_SUP_A_CHANNEL1:
            pulse.rise = timing_pulse.vs_pulses[2] + offset;
            pulse.fall = timing_pulse.vs_pulses[3] + offset;
            break;

        case DIG_SUP_A_CHANNEL2:
            // Channel A2 is always associated to the acquisition time.
            // Hard-coded the duration to 500 us.

            pulse.rise =   0;
            pulse.fall = 500;
            break;

        case DIG_SUP_A_CHANNEL3:
            pulse.rise = timing_pulse.vs_pulses[4] + offset;
            pulse.fall = timing_pulse.vs_pulses[5] + offset;
            break;

        default:
            pulse.rise = 0;
            pulse.fall = 0;
            break;
    }

    digitalIoSupSetPulse(channel, -pulse.rise, pulse.fall - pulse.rise);
}



void timingPulseSetFastAcq(uint32_t const start, uint32_t const width)
{
    // Sends a soft pulse that sets a flag in the FPGA that can be polled
    // by the DSP to run the fast acquisition.

    if (cycTimeTstPulseArm() == true)
    {
        cycTimeClrPulseArm();
    }

    cycTimeSetPulseEtim(start);
    cycTimeSetPulseWidth(width);
    cycTimeSetPulseArm();
}



bool timingPulseDetectEdge(enum Timing_pulse_channel const channel,
                           enum Timing_pulse_edge    const edge,
                           int32_t                 * const edge_time_us)
{
    bool retval = false;

    if (edge == TIMING_PULSE_EDGE_RISE && digitalIoSupTestRiseEdge(channel) == true)
    {
        *edge_time_us = digitalIoSupGetRiseEtim(channel);

        retval = true;
    }

    if (edge == TIMING_PULSE_EDGE_FALL && digitalIoSupTestFallEdge(channel) == true)
    {
        *edge_time_us = digitalIoSupGetFallEtim(channel);

        retval = true;
    }

    return retval;
}


// EOF
