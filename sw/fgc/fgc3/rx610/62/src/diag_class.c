//! @file  diag_class.c
//! @brief Class specific functionality for diagnostics


// ---------- Includes

#include <stdint.h>

#include <diag.h>
#include <digitalIo.h>
#include <class.h>
#include <fbs_class.h>
#include <sta.h>
#include <logSpy.h>
#include <pulse.h>
#include <stateOp.h>
#include <status.h>



// ---------- External function definitions

void diagClassInit(void)
{
    // Assume the four channels below exist. If not, channel[] will contain
    // -1, which will default in a I earth value of 0.0.

    meas_i_earth.channel[0] = DiagFindAnaChan("I_EARTH_1MS");
    meas_i_earth.channel[1] = DiagFindAnaChan("I_EARTH_6MS");
    meas_i_earth.channel[2] = DiagFindAnaChan("I_EARTH_11MS");
    meas_i_earth.channel[3] = DiagFindAnaChan("I_EARTH_16MS");
}



void diagClassCheckConverter(void)
{
    // This function is called from stateTask() every 5ms.

    static uint16_t dim_trig_counter;

    if (   stateOpGetState() != FGC_OP_SIMULATION
        && ((uint16_t)sta.timestamp_ms.ms % 20 == (15 + DEV_STA_TSK_PHASE)))
    {
        if (   dim_collection.unlatched_trigger_fired  == true
            && testBitmap(digitalIoGetInputs(), DIG_IP_VSRUN_MASK32)  == true
            && statusTestFaults(FGC_FLT_VS_FAULT     ) == false
            && statusTestFaults(FGC_FLT_VS_EXTINTLOCK) == false
            && statusTestFaults(FGC_FLT_FAST_ABORT   ) == false
            && statusTestFaults(FGC_LAT_DIM_TRIG_FLT ) == false)
        {
            if (++dim_trig_counter > 5)
            {
                statusSetLatched(FGC_LAT_DIM_TRIG_FLT);
            }
        }
        else
        {
            dim_trig_counter = 0;
        }
    }
}


void diagClassUpdateMeas(void)
{
    // Update the simulated earth current

    meas_i_earth.simulated = pulse_report.meas_i * meas_i_earth.limit / pulse_limits.pos;
}


// EOF
