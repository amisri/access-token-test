//! @file  set_class.c
//! @brief Class specific Set command functions


// ---------- Includes

#include <set_class.h>
#include <fgc_errs.h>
#include <cmd.h>
#include <pars.h>
#include <defprops.h>
#include <pub.h>
#include <pulse.h>



// ---------- External function definitions

uint16_t SetRef(struct cmd * c, struct prop * p)
{
    // Not supported

    return FGC_UNKNOWN_CMD;
}



uint16_t SetRefPulse(struct cmd * c, struct prop * p)
{
    float    * value;
    uint16_t   errnum;

    errnum = ParsValueGet(c, p, (void**)&value);

    if (errnum == FGC_OK_NO_RSP)
    {
        switch (p->sym_idx)
        {
            case STP_VALUE:     errnum = pulseSetRef(c->sub_sel, c->cyc_sel, *value, 0); break;
            case STP_REF_LOCAL: errnum = pulseSetRefLocal(*value);        break;
            default:            errnum = FGC_BAD_PARAMETER;               break;
        }
    }

    return errnum;
}



uint16_t SetPulseOffset(struct cmd * c, struct prop * p)
{
    uint16_t errnum = ParsSet(c, p);

    if (errnum == FGC_OK_NO_RSP)
    {
        pubPublishProperty(p, c->sub_sel, c->cyc_sel, TRUE);

        errnum = nvsStoreValuesForOneProperty(c, p, c->sub_sel, c->cyc_sel);
    }

    return errnum;
}



uint16_t SetDevName(struct cmd * c, struct prop * p)
{
    return interFgcSetDevName(c, p);
}



uint16_t SetDevicePPM(struct cmd * c, struct prop * p)
{
    uint16_t  errnum;

    // If DEVICE.PPM has changed

    if ((errnum = ParsSet(c, p)) == 0)
    {
        if (p == &PROP_DEVICE_PPM)
        {
            device.max_user = (dpcom.mcu.device_ppm == FGC_CTRL_ENABLED ? FGC_MAX_USER : 0);
        }

        // PROP_DEVICE_MULTI_PPM

        else
        {
            device.max_sub_devs = (dpcom.mcu.device_multi_ppm == FGC_CTRL_ENABLED ? FGC_MAX_SUB_DEVS - 1 : 0);
        }
    }

    return (errnum);
}




uint16_t SetCycSim(struct cmd * c, struct prop * p)
{
    uint16_t errnum = ParsSet(c, p);

    if (errnum)
    {
        return errnum;
    }

    if (PROP_FGC_CYC_SIM.n_elements != 0)
    {
        uint8_t i;

        for (i = 0; i < PROP_FGC_CYC_SIM.n_elements; ++i)
        {
            uint32_t cycle  = cycsim_prop.cycles[i];
            uint8_t sub_sel = (uint8_t)((cycle % 1000000) / 10000);
            uint8_t cyc_sel = (uint8_t)((cycle %   10000) /   100);
            uint8_t length  = (uint8_t)((cycle %     100) /     1);

            if (cycSimSetCycleInfo(sub_sel, cyc_sel, length) == false)
            {
                return FGC_BAD_PARAMETER;
            }
        }
    }

    cycSimSetCycleComplete();

    return 0;
}


// EOF