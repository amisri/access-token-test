//! @file  events_class.c
//! @brief File with the class-specific processing of events


// ---------- Includes

#include <stdint.h>

#include <logCycle.h>
#include <cycSim.h>
#include <events.h>
#include <device.h>
#include <dpcls.h>
#include <dpcom.h>
#include <function_class.h>
#include <msTask.h>
#include <pulse.h>
#include <time_fgc.h>
#include <transaction.h>



// ---------- Internal function definitions

static void eventsClassProcessSSC(struct CC_us_time  const * event_time,
                                  struct Events_data const   data)
{
    logCycleStartSuperCycle();
}



static void eventsClassProcessCycleTag(struct CC_us_time  const * event_time,
                                       struct Events_data const   data)
{
    // Transfer the cyle tag to the DSP

    dpcls.oasis.cycle_tag_next = data.payload.cycle_tag;
}



static void eventsClassProcessRef(struct CC_us_time  const * event_time,
                                  struct Events_data const   data)
{
    if (   cycSimIsEnabled()     == false
        && ref.economy_dest.mode == FGC_CTRL_ENABLED
        && testBitmap(ref.economy_dest.mask, data.next_destination) == false)
    {
        pulseSetEconomy();
    }

    pulseSetEvent(event_time, &data.payload.cycle);
}



static void eventsClassProcessTransactionCommit(struct CC_us_time  const * event_time,
                                                struct Events_data const   data)
{
    for (uint32_t i = 0; i <= device.max_sub_devs; i++)
    {
        transactionCommit(i, data.payload.transaction_id, event_time);
    }
}



static void eventsClassProcessTransactionRollback(struct CC_us_time  const * event_time,
                                                  struct Events_data const   data)
{
    for (uint32_t i = 0; i <= device.max_sub_devs; i++)
    {
        transactionRollback(i, data.payload.transaction_id);
    }
}



// ---------- Platform/class specific function definitions

void eventsClassInit(void)
{
    eventsRegisterEventFunc(FGC_EVT_SSC                   , eventsClassProcessSSC                );
    eventsRegisterEventFunc(FGC_EVT_CYCLE_TAG             , eventsClassProcessCycleTag           );
    eventsRegisterEventFunc(FGC_EVT_START_REF_1           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_START_REF_2           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_START_REF_3           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_START_REF_4           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_START_REF_5           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_1, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_2, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_3, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_4, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_START_REF_5, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_CYCLE_START           , eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_SUB_DEVICE_CYCLE_START, eventsClassProcessRef                );
    eventsRegisterEventFunc(FGC_EVT_TRANSACTION_COMMIT    , eventsClassProcessTransactionCommit  );
    eventsRegisterEventFunc(FGC_EVT_TRANSACTION_ROLLBACK  , eventsClassProcessTransactionRollback);
}



void eventsClassProcess(void)
{
    // Do nothing;
}


// EOF
