//! @file   statePc.c
//! @brief  Manage the Power Converter state and the Reference state


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <defconst.h>
#include <defprops.h>

#include <bitmap.h>
#include <digitalIo.h>
#include <statePc_class.h>
#include <fbs_class.h>
#include <function_class.h>
#include <modePc.h>
#include <statePc.h>
#include <pc_state.h>
#include <pub.h>
#include <pulse.h>



// ---------- Platform/class specific function definitions

void statePcClassProcess(void)
{
    if (    STATE_VS != FGC_VS_BLOCKED
        && (modePcGetInternal() == FGC_PC_OFF || modePcGetInternal() == FGC_PC_BLOCKING))
    {
        digitalIoSetCmdRequest(DDOP_CMD_BLOCKING);
    }

    pulseProcess();

    // Update REF.FAULT_USER

    if (pcStateTest(FGC_STATE_FLT_STOPPING_BIT_MASK | FGC_STATE_FLT_OFF_BIT_MASK) == true)
    {
        if (ref.fault_user == 0)
        {
            ref.fault_user = dpcom.dsp.cycle.current.cyc_sel_acq;
            pubPublishProperty(&PROP_REF_FAULT_USER, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
        }
    }
    else
    {
        if (ref.fault_user != 0)
        {
            ref.fault_user = 0;
            pubPublishProperty(&PROP_REF_FAULT_USER, PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
        }
    }
}


// ----------------------------- CLASS SPECIFIC STATE FUNCTIONS ------------------------------

// STATE: CYCLING (CY) - LIBREF STATE: CYCLING

uint32_t statePcCY(bool firstCall)
{
    return FGC_PC_CYCLING;
}



// STATE: TO_CYCLING (TC) - LIBREF STATE: TO_CYCLING

uint32_t statePcTC(bool firstCall)
{
    return FGC_PC_TO_CYCLING;
}



// ------------------------ CLASS SPECIFIC TRANSITION CONDITION FUNCTIONS -------------------------

static StatePC * BKtoTC(void)
{
    if (   modePcGetInternal() == FGC_PC_CYCLING
        && testBitmap(digitalIoGetInputs(), DIG_IP_OPBLOCKED_MASK32) == false)
    {
        return statePcTC;
    }

    return NULL;
}


static StatePC * TCtoCY(void)
{
    if (STATE_VS == FGC_VS_READY)
    {
        return statePcCY;
    }

    return NULL;
}



static StatePC * CYtoTC(void)
{
    if (STATE_VS != FGC_VS_READY)
    {
        return statePcTC;
    }

    return NULL;
}



static StatePC * XXtoBK(void)
{
    if (STATE_VS == FGC_VS_BLOCKED)
    {
        return statePcBK;
    }

    return NULL;
}



// ---------- External variable definitions

//! State transition functions : called in priority order, from left to right, return the state function.
//!
//! The initialization order of StateTransitions must match the STATE.PC constant values from the XML symlist.
//! This is checked with static assertions.

StatePCtrans * StateTransitions[][STATE_PC_REF_MAX_TRANSITIONS + 1] =
{
    /*  0.FO */ {         FOtoOF,         NULL },
    /*  1.OF */ {         OFtoFO, OFtoST, NULL },
    /*  2.FS */ {         FStoFO, FStoSP, NULL },
    /*  3.SP */ { XXtoFS, SPtoOF,         NULL },
    /*  4.ST */ { XXtoFS, STtoSP, STtoBK, NULL },
    /*  5.SA */ {                         NULL },
    /*  6.TS */ {                         NULL },
    /*  7.SB */ {                         NULL },
    /*  8.IL */ {                         NULL },
    /*  9.TC */ { XXtoFS, XXtoBK, TCtoCY, NULL },
    /* 10.AR */ {                         NULL },
    /* 11.RN */ {                         NULL },
    /* 12.AB */ {                         NULL },
    /* 13.CY */ { XXtoFS, XXtoBK, CYtoTC, NULL },
    /* 14.PL */ {                         NULL },
    /* 15.BK */ { XXtoFS, BKtoSP, BKtoTC, NULL },
    /* 16.EC */ {                         NULL },
    /* 17.DT */ {                         NULL },
    /* 18.PA */ {                         NULL }
};


// EOF
