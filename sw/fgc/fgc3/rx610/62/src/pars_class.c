//! @file     pars_class.c
//! @brief    Parsing of class specific property types


// ---------- Includes

#include <stdint.h>

#include <pars.h>
#include <databases.h>
#include <cmd.h>
#include <prop.h>
#include <fgc_errs.h>



// ---------- Internal function definitions

static bool isMacAddress(const char * const str)
{
    unsigned i;
    for(i = 0; i < strlen(str); i++)
    {
        if(str[i] == '-')
        {
            return true;
        }
    }

    return false;
}



static uint16_t validateMacAddress(const char * const str)
{
    const size_t length = strlen(str);
          size_t i;

    if(length != 17)
    {
        return FGC_BAD_ADDRESS;
    }

    for(i = 0; i < length; i++)
    {
        if((i + 1) % 3 == 0)
        {
            if(str[i] != '-')
            {
                return FGC_BAD_ADDRESS;
            }
        }
        else if((str[i] < '0' || str[i] > '9') &&
                (str[i] < 'A' || str[i] > 'F') &&
                (str[i] < 'a' || str[i] > 'f'))
        {
            return FGC_BAD_ADDRESS;
        }
    }

    return 0;
}



static uint16_t parsScanDevName(struct cmd * c, struct prop * p, char ** value)
{
    // Use the pars_buf results area for the value

    char *   result = (char *)&c->pars_buf->results;

    uint16_t errnum;
    char *   token;
    char *   head;

    *value = result;
    head   = result;

    // If there's a symlist linked to the property, we expect
    // the string to be in <symlist_entry>@<device_name> format.
    // Otherwise, the string should contain only a device name.

    if(testBitmap(p->flags, PF_SYM_LST))
    {
        const struct sym_lst * sym_const;

        // Look for symlist entry followed by '@'

        errnum = ParsScanSymList(c, "Y\\@,", (struct sym_lst *)p->range, &sym_const);

        c->token_idx = c->n_token_chars;
        token        = c->token;

        // Mask 'buffer full' error coming from the tokeniser

        if(errnum == FGC_SYNTAX_ERROR)
        {
            return FGC_UNKNOWN_SYM;
        }

        if(errnum != 0)
        {
            return errnum;
        }

        if(c->token_delim == '@')
        {
            if(strlen(token) > ST_MAX_SYM_LEN)
            {
                return FGC_UNKNOWN_SYM;
            }

            strcpy(head, token);

            head   += c->n_token_chars;
            *head++ = '@';
        }
        else
        {
            if(memcmp(token, "NONE", 4) == 0)
            {
                strcpy(head, token);

                return 0;
            };
        }
    }

    errnum = ParsScanString(c, &token, "Y,");

    // Mask 'no symbol' error coming from the tokeniser

    if(errnum == FGC_NO_SYMBOL && testBitmap(p->flags, PF_SYM_LST))
    {
        return FGC_UNKNOWN_DEV;
    }

    // Mask 'buffer full' error coming from the tokeniser

    if(errnum == FGC_SYNTAX_ERROR)
    {
        return FGC_DEV_NAME_TOO_LONG;
    }

    if(errnum != 0)
    {
        return errnum;
    }

    if(strlen(token) > FGC_MAX_DEV_LEN)
    {
        return FGC_DEV_NAME_TOO_LONG;
    }

    strcpy(head, token);

    if(memcmp(token, "NONE", 4) == 0)
    {
        return 0;
    }

    if(isMacAddress(token))
    {
        errnum = validateMacAddress(token);

        if(errnum != 0)
        {
            return errnum;
        }

        return 0;
    }
    else if(nameDbGetIndex(token) >= 0)
    {
        return 0;
    }

    return FGC_UNKNOWN_DEV;
}



// ---------- External function definitions

uint16_t parsClassValueGet(struct cmd * c, struct prop * p, void ** value)
{
    if(p->type == PT_DEV_NAME)
    {
        return parsScanDevName(c, p, (char **)value);
    }

    return FGC_UNKNOWN_ERROR_CODE;
}


// EOF
