//! @file  msTask_class.c
//! @brief Class specific millisecond actions


// ---------- Includes

#include <msTask.h>
#include <logCycle.h>
#include <defconst.h>
#include <defprops.h>
#include <diag.h>
#include <dpcls.h>
#include <dpcom.h>
#include <fbs_class.h>
#include <pub.h>
#include <pulse.h>
#include <stateOp.h>
#include <sharedMemory.h>



// ---------- Internal function definitions

static void msTaskClassPublishData(void)
{
    // Millisecond 1 of 10: Read analogue values.

    if (   ms_task.ms_mod_10  == 1
        && stateOpGetState() != FGC_OP_UNCONFIGURED)
    {
        REF      = pulse_report.ref;
        I_MEAS   = dpcls.dsp.meas.i_load;
        V_MEAS   = dpcls.dsp.meas.v_load;
        V_CAPA   = dpcls.dsp.meas.v_capa;
        I_ERR_MA = (int16_t)pulse_report.error_mav;
    }


    //  Millisecond 1 of 20: Read DSP status and run Gateway PC_PERMIT timeout

    if (ms_task.ms_mod_20 == 1)
    {
        ST_MEAS_A     = (uint8_t) dpcom.dsp.meas.adc_state[0];
        ST_MEAS_B     = (uint8_t) dpcom.dsp.meas.adc_state[1];
        ST_MEAS_C     = (uint8_t) dpcom.dsp.meas.adc_state[2];
        ST_MEAS_D     = (uint8_t) dpcom.dsp.meas.adc_state[3];
        ST_DCCT_A     = (uint8_t) dpcom.dsp.meas.dcct_state[0];
        ST_DCCT_B     = (uint8_t) dpcom.dsp.meas.dcct_state[1];
        I_EARTH_CPCNT = (int16_t) meas_i_earth.cpcnt;
    }
}



static void msTaskClassPublish(void)
{
    // Publish properties at the acquisition time. These properties are
    // acquisitions and therefore never multi-PPM

    // Publish acquisition property

    if (dpcls.dsp.meas.pub_acq_cyc_sel > 0)
    {
        uint8_t cyc_sel = dpcls.dsp.meas.pub_acq_cyc_sel;

        pubPublishProperty(&PROP_MEAS_PULSE, PUB_NO_SUB_SEL, cyc_sel, false);

        dpcls.dsp.meas.pub_acq_cyc_sel = 0;
    }

    // Publish INTERLOCK latched values

    if (dpcls.dsp.interlock.ready == true)
    {
        uint8_t cyc_sel = dpcls.dsp.interlock.cyc_sel;

        pubPublishProperty(&PROP_INTERLOCK_RESULT,     PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
        pubPublishProperty(&PROP_INTERLOCK_V_MEAS,     PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);
        pubPublishProperty(&PROP_INTERLOCK_I_MEAS,     PUB_NO_SUB_SEL, PUB_NO_CYC_SEL, false);

        pubPublishProperty(&PROP_INTERLOCK_PPM_RESULT, PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_INTERLOCK_PPM_V_MEAS, PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_INTERLOCK_PPM_I_MEAS, PUB_NO_SUB_SEL, cyc_sel, false);

        dpcls.dsp.interlock.ready = false;
    }

    // Publish properties at the end of the cycle. These properties are
    // acquisitions and therefore never multi-PPM

    if (dpcom.dsp.cycle.pub_previous_cycle == true)
    {
        uint8_t cyc_sel = dpcom.dsp.cycle.previous.cyc_sel_acq;

        // LOG.MEAS.PULSES

        pubPublishProperty(&PROP_MEAS_PULSES, PUB_NO_SUB_SEL, cyc_sel, false);

        // LOG.PULSE.*

        pubPublishProperty(&PROP_LOG_PULSE_STATUS,      PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_LOG_PULSE_MAX_ABS_ERR, PUB_NO_SUB_SEL, cyc_sel, false);

        dpcom.dsp.cycle.pub_previous_cycle = false;
    }

    // Pulbish the fast acquisition data

    if (dpcls.dsp.meas.fast_meas_ready == true)
    {
        uint8_t cyc_sel = dpcom.dsp.cycle.current.cyc_sel_acq;

        pubPublishProperty(&PROP_LOG_OASIS_V_MEAS_FAST, PUB_NO_SUB_SEL, cyc_sel, false);
        pubPublishProperty(&PROP_LOG_OASIS_I_MEAS_FAST, PUB_NO_SUB_SEL, cyc_sel, false);

        dpcls.dsp.meas.fast_meas_ready = false;
    }
}



// ---------- Platform/class specific function definitions

void msTaskClassProcess(void)
{
    // Log the DIG_SUP_B timing pulses.

    pulseLogDigSup();
    logCycleStore();
    msTaskClassPublishData();
    msTaskClassPublish();
}


// EOF
