//! @file   cycSim_class.c
//! @brief  Class FGC_62 specific source file


// ---------- Includes

#include <cycSim.h>
#include <defconst.h>
#include <defprops.h>
#include <fgc_fieldbus.h>



// ---------- Platform/class specific function definitions

void cycSimClassInit(void)
{
    // Events

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,   FGC_EVT_START_REF_1, NONE, 900,  100);
    cycSimSetEventInfo(FGC_EVT_START_REF_1, 0,                   USER, 900,  100);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,   FGC_EVT_START_REF_2, NONE, 900, -200);
    cycSimSetEventInfo(FGC_EVT_START_REF_2, 0,                   USER, 900, -200);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,   FGC_EVT_START_REF_3, NONE, 2500, 200);
    cycSimSetEventInfo(FGC_EVT_START_REF_3, 0,                   USER, 2500, 200);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,   FGC_EVT_START_REF_4, NONE, 180,  200);
    cycSimSetEventInfo(FGC_EVT_START_REF_4, 0,                   USER, 180,  200);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,   FGC_EVT_START_REF_4, NONE, 180,  300);
    cycSimSetEventInfo(FGC_EVT_START_REF_4, 0,                   USER, 180,  300);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,   FGC_EVT_START_REF_4, NONE, 180,  400);
    cycSimSetEventInfo(FGC_EVT_START_REF_4, 0,                   USER, 180,  400);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,              FGC_EVT_SUB_DEVICE_START_REF_1, NONE, 900,  100);
    cycSimSetEventInfo(FGC_EVT_SUB_DEVICE_START_REF_1, 0,                              USER, 900,  100);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,              FGC_EVT_SUB_DEVICE_START_REF_2, NONE, 900, -200);
    cycSimSetEventInfo(FGC_EVT_SUB_DEVICE_START_REF_2, 0,                              USER, 900, -200);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,              FGC_EVT_SUB_DEVICE_START_REF_3, NONE, 2500, 200);
    cycSimSetEventInfo(FGC_EVT_SUB_DEVICE_START_REF_3, 0,                              USER, 2500, 200);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,              FGC_EVT_SUB_DEVICE_START_REF_4, NONE, 180,  200);
    cycSimSetEventInfo(FGC_EVT_SUB_DEVICE_START_REF_4, 0,                              USER, 180,  200);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,              FGC_EVT_SUB_DEVICE_START_REF_4, NONE, 180,  300);
    cycSimSetEventInfo(FGC_EVT_SUB_DEVICE_START_REF_4, 0,                              USER, 180,  300);

    cycSimSetEventInfo(FGC_EVT_NEXT_DEST,              FGC_EVT_SUB_DEVICE_START_REF_4, NONE, 180,  400);
    cycSimSetEventInfo(FGC_EVT_SUB_DEVICE_START_REF_4, 0,                              USER, 180,  400);

}


// EOF
