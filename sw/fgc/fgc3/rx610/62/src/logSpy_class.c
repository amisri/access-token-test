//! @file  logSpy_class.c
//! @brief Class specific logging related functions


// ---------- Includes

#include <fgc_errs.h>

#include <logSpy.h>
#include <dpcom.h>
#include <bitmap.h>



// ---------- Platform/class specific function definitions

void logSpyClassInit(void)
{
    ; // Do nothing
}



void logSpyClassEnableDisableLogs(void)
{
    static uint32_t local_log_ignore_mask = 0x00000000;

    if (local_log_ignore_mask == dpcom.mcu.log.ignore_mask)
    {
        return;
    }

    // Check all conditions

    struct LOG_mgr * log_mgr                 = (struct LOG_mgr *)&dpcom.log.log_mgr;
    uint32_t         updated_log_ignore_mask = local_log_ignore_mask ^ dpcom.mcu.log.ignore_mask;

    local_log_ignore_mask = dpcom.mcu.log.ignore_mask;

    // Log BIS_FLAGS and BIS_CHANNELS

    if (testBitmap(updated_log_ignore_mask, DPCOM_LOG_IGNORE_BIS_ALL_BIT_MASK) == true)
    {
        // If all the channels are OFF, disable the logs and consequently remove the log menus

        if (testAllBitmap(local_log_ignore_mask, DPCOM_LOG_IGNORE_BIS_ALL_BIT_MASK) == true)
        {
            logDisable(log_mgr, LOG_BIS_FLAGS);
            logDisable(log_mgr, LOG_BIS_LIMITS);
        }
        else
        {

            logEnable(log_mgr, LOG_BIS_FLAGS);
            logEnable(log_mgr, LOG_BIS_LIMITS);

            // BIS Channel 1

            if (testBitmap(local_log_ignore_mask, DPCOM_LOG_IGNORE_BIS_CH1_BIT_MASK) == true)
            {
                logMenuDisable(&log_menus, LOG_MENU_BIS_CH1_OK);
                logMenuDisable(&log_menus, LOG_MENU_BIS_CH1_TESTS);
                logMenuDisable(&log_menus, LOG_MENU_BIS_LIMITS_CH1);
            }
            else
            {
                logMenuEnable(&log_menus, LOG_MENU_BIS_CH1_OK);
                logMenuEnable(&log_menus, LOG_MENU_BIS_CH1_TESTS);
                logMenuEnable(&log_menus, LOG_MENU_BIS_LIMITS_CH1);
            }

            // BIS Channel 2

            if (testBitmap(local_log_ignore_mask, DPCOM_LOG_IGNORE_BIS_CH2_BIT_MASK) == true)
            {
                logMenuDisable(&log_menus, LOG_MENU_BIS_CH2_OK);
                logMenuDisable(&log_menus, LOG_MENU_BIS_CH2_TESTS);
                logMenuDisable(&log_menus, LOG_MENU_BIS_LIMITS_CH2);
            }
            else
            {
                logMenuEnable(&log_menus, LOG_MENU_BIS_CH2_OK);
                logMenuEnable(&log_menus, LOG_MENU_BIS_CH2_TESTS);
                logMenuEnable(&log_menus, LOG_MENU_BIS_LIMITS_CH2);
            }

            // BIS Channel 3

            if (testBitmap(local_log_ignore_mask, DPCOM_LOG_IGNORE_BIS_CH3_BIT_MASK) == true)
            {
                logMenuDisable(&log_menus, LOG_MENU_BIS_CH3_OK);
                logMenuDisable(&log_menus, LOG_MENU_BIS_CH3_TESTS);
                logMenuDisable(&log_menus, LOG_MENU_BIS_LIMITS_CH3);
            }
            else
            {
                logMenuEnable(&log_menus, LOG_MENU_BIS_CH3_OK);
                logMenuEnable(&log_menus, LOG_MENU_BIS_CH3_TESTS);
                logMenuEnable(&log_menus, LOG_MENU_BIS_LIMITS_CH3);
            }

            // BIS Channel 4

            if (testBitmap(local_log_ignore_mask, DPCOM_LOG_IGNORE_BIS_CH4_BIT_MASK) == true)
            {
                logMenuDisable(&log_menus, LOG_MENU_BIS_CH4_OK);
                logMenuDisable(&log_menus, LOG_MENU_BIS_CH4_TESTS);
                logMenuDisable(&log_menus, LOG_MENU_BIS_LIMITS_CH4);
            }
            else
            {
                logMenuEnable(&log_menus, LOG_MENU_BIS_CH4_OK);
                logMenuEnable(&log_menus, LOG_MENU_BIS_CH4_TESTS);
                logMenuEnable(&log_menus, LOG_MENU_BIS_LIMITS_CH4);
            }
        }
    }
}



uint16_t logSpyClassProcessAlias(uint32_t const menu_idx)
{
    return FGC_OK_NO_RSP; // Do nothing
}



bool logSpyClassIsActiveAlias(uint32_t const menu_idx)
{
    // Do nothing

    return false;
}



void logSpyClassInterceptLogNames(union LOG_header * const log_header, enum LOG_menu_index const menu_index)
{
    // The only log menu to intercept is DIM, and this is already handled by logInterceptLogNames.
}


// EOF
