//! @file   logCycle_class.c
//! @brief  Class FGC_63 specific code to interface to the library libcyclog


// ---------- Includes

#include <stdint.h>

#include <class.h>
#include <cmd.h>
#include <defprops.h>



// ---------- Platform/class specific function definitions

char * logCycleClassFuncTypeCallback(uint32_t const func_type)
{
    return "PULSE";
}



char * logCycleClassStatusCallback(uint32_t const status)
{
    return CmdPrintSymLst(SYM_LST_PULSE_STAT, status);
}



uint32_t logCycleClassGetStatusOk(void)
{
    return (  FGC_PULSE_STATUS_REG_OK
            | FGC_PULSE_STATUS_DISABLED
            | FGC_PULSE_STATUS_DYN_ECO);
}



uint32_t logCycleClassGetStatusWarnings(void)
{
    return (  FGC_PULSE_STATUS_REG_WARNING
            | FGC_PULSE_STATUS_NOT_READY
            | FGC_PULSE_STATUS_POL_SWITCH
            | FGC_PULSE_STATUS_TIMEOUT);
}



uint32_t logCycleClassGetStatusFaults(void)
{
    return FGC_PULSE_STATUS_REG_FAULT;
}


// EOF
