//! @file  get_class.c
//! @brief Class specific get functions


// ---------- Includes

#include <class.h>
#include <cmd.h>
#include <databases.h>
#include <defprops.h>
#include <logCycle.h>
#include <property.h>



// ---------- Internal function definitions

static bool isFgcMac(const struct Inter_fgc_mac * mac)
{
    static const uint8_t fgc_mac_prefix[5] = { 0x02, 0x00, 0x00, 0x00, 0x00 };

    return (memcmp(mac->octets, fgc_mac_prefix, 5) == 0 && mac->octets[5] < 65);
}



static inline void printMacAsString(FILE * f, const struct Inter_fgc_mac * mac)
{
    fprintf(f, "%02X-%02X-%02X-%02X-%02X-%02X",
            mac->octets[0],
            mac->octets[1],
            mac->octets[2],
            mac->octets[3],
            mac->octets[4],
            mac->octets[5]);
}



// ---------- External function definitions

uint16_t GetLogCycles(struct cmd * c, struct prop * p)
{
    prop_size_t   n;
    uint16_t      errnum;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if (errnum)
    {
        return (errnum);
    }

    // 30 characters for status

    uint32_t const buf_len = CYCLOG_READ_FGC_FIXED_LEN + 30;

    char read_buf[buf_len];

    while (n-- > 0)
    {
        errnum = CmdPrintIdx(c, c->from);

        if (errnum)
        {
            return (errnum);
        }

        logCycleGet(c->from++, read_buf, buf_len);

        fprintf(c->f, "%s", read_buf);
    }

    return 0;
}



uint16_t GetMac(struct cmd * c, struct prop * p)
{
    prop_size_t n;
    uint16_t    errnum;

    errnum = CmdPrepareGet(c, p, 1, &n);

    if(errnum != 0)
    {
        return errnum;
    }

    const bool zero_f = (testBitmap(p->flags, PF_GET_ZERO) && testBitmap(c->getopts, GET_OPT_ZERO));

    struct Inter_fgc_mac * mac = p->value;
    uint16_t               i   = c->from;

    while(n--)
    {
        // Print index if required

        errnum = CmdPrintIdx(c, i);

        if(errnum != 0)
        {
            return errnum;
        }

        // Check if MAC address belongs to an FGC

        if(isFgcMac(mac))
        {
            uint16_t     name_db_index = mac->octets[5];
            const char * name          = nameDbGetName(name_db_index);

            // If there's a name in NameDb that corresponds to the FGC dongle number,
            // then print it instead of MAC address. Otherwise, print the MAC directly.

            if(name != NULL)
            {
                fputs(name, c->f);
            }
            else
            {
                printMacAsString(c->f, mac);
            }
        }
        else
        {
            // Non-FGC devices aren't in NameDb, so print the MAC directly

            printMacAsString(c->f, mac);
        }

        // Clear the value if zero flag present

        if(zero_f)
        {
            memset(mac, 0, NUM_MAC_OCTETS);
        }

        // Advance to the next element

        mac += c->step;
        i++;
    }

    if(zero_f)
    {
        p->n_elements = 0;
    }

    return 0;
}


// EOF