//! @file   rtd_class.c
//! @brief  Class specific source file


// ---------- Includes

#include <rtd.h>
#include <dpcls.h>
#include <dpcom.h>
#include <pulse.h>
#include <bitmap.h>
#include <polSwitch.h>



// ---------- Constants

#define RTD_POS_POL_SW          "24;21"

#define RTD_POS_VA              "21;59"
#define RTD_POS_VB              "22;59"
#define RTD_POS_VC              "23;59"
#define RTD_POS_VD              "24;59"

#define RTD_POS_REF             "21;32"
#define RTD_POS_V_LOAD          "22;32"
#define RTD_POS_I_LOAD          "23;32"
#define RTD_POS_STATUS          "24;33"
#define RTD_POS_ERR             "21;46"
#define RTD_POS_V_CAPA          "22;46"
#define RTD_POS_I_CAPA          "23;46"

#define RTD_POS_ADCS_L_VOLTS    "20;11"
#define RTD_POS_ADCS_L_PP       "20;22"
#define RTD_POS_ADCS_L_AMPS     "20;30"
#define RTD_POS_ADCS_R_VOLTS    "20;52"
#define RTD_POS_ADCS_R_PP       "20;63"
#define RTD_POS_ADCS_R_AMPS     "20;71"
#define RTD_POS_ADCS_L_RAW      "20;01"
#define RTD_POS_ADCS_R_RAW      "20;42"

#define RTD_POS_ERR_ADC_A       "21;58"
#define RTD_POS_ERR_ADC_B       "22;58"
#define RTD_POS_ERR_ADC_C       "23;58"
#define RTD_POS_ERR_ADC_D       "24;58"
#define RTD_POS_ERR_MEAS_V      "21;45"
#define RTD_POS_ERR_MEAS_I      "22;45"

#define RTD_POS_CYC             "20;1"



// ---------- Internal function declarations

static bool rtdPrintStatus    (bool reset);
static bool rtdClassPolSwitch (bool reset);
static bool rtdClassOptionCyc (bool reset);
static bool rtdOptionAdcsAB   (bool reset);
static bool rtdOptionAdcsCD   (bool reset);
static bool rtdPrintAdcsHelper(struct Rtd_field_float * adc_floats, struct Rtd_field_int * adc_ints, bool reset);



// ---------- Internal variable definitions

static struct Rtd_field_func rtd_class_field_funcs[] =
{
    { rtdPrintStatus    },
    { rtdClassPolSwitch },
    { NULL              }
};


static struct Rtd_field_float rtd_class_field_floats[] =
{
    {          &pulse_report.ref     , "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_REF   , 9 },
    {          &pulse_report.meas_v  , "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_V_LOAD, 9 },
    {          &pulse_report.meas_i  , "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_I_LOAD, 9 },
    {          &pulse_report.error   , "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_ERR   , 9 },
    { (float *)&dpcls.dsp.meas.v_capa, "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_V_CAPA, 9 },
    { (float *)&dpcls.dsp.meas.i_capa, "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_I_CAPA, 9 },
    { NULL                                                                            },
};


static struct Rtd_field_error_float rtd_class_field_floats_errors[] =
{
    // Order is important as rtdClassUpdateErrors() depends on it

    { { (float *)&dpcom.dsp.adc.volts_200ms[0], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_VA, 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcom.dsp.adc.volts_200ms[1], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_VB, 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcom.dsp.adc.volts_200ms[2], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_VC, 9 }, { 0x00, 0xFF } },
    { { (float *)&dpcom.dsp.adc.volts_200ms[3], "%*.5f", 0x6FFFFFFF, 100000, RTD_POS_VD, 9 }, { 0x00, 0xFF } },
    { {  NULL                                                                                              } },
};


static struct Rtd_field_optional rtd_class_field_optional[] =
{
    { FGC_RTD_CYC,     rtdClassOptionCyc },
    { FGC_RTD_ADCS_AB, rtdOptionAdcsAB   },
    { FGC_RTD_ADCS_CD, rtdOptionAdcsCD   },
    { 0xFF,            NULL              },
};


static struct Rtd_process rtd_class_process[] =
{
    { rtdProcessFuncs,        { (void *)rtd_class_field_funcs,         (void *)rtd_class_field_funcs         } },
    { rtdProcessFloats,       { (void *)rtd_class_field_floats,        (void *)rtd_class_field_floats        } },
    { rtdProcessFloatsErrors, { (void *)rtd_class_field_floats_errors, (void *)rtd_class_field_floats_errors } },
    { rtdProcessOptionLine,   { (void *)rtd_class_field_optional,      (void *)rtd_class_field_optional      } },
    { NULL                                                                                                     }
};



// ---------- Platform/class specific function definitions

void rtdClassInit(void)
{
    rtd.ctrl = FGC_RTD_CYC;
}



void rtdClassReset(FILE * f)
{
    // Move to RTD display area

    fputs(TERM_CSI "21;1" TERM_GOTO, f);

    //    0         10        20        30        40        50        60        70        80
    //    |---------+---------+---------+---------+---------+---------+---------+---------+|

    fputs("States: __.__.__.__        Ref:______.__ Err:______.__ Va:___._____             \n\r", f);
    fputs("                           V:  ______.__ Vcp:______.__ Vb:___._____             \n\r", f);
    fputs("CFG:                       I:  ______.__ Icp:______.__ Vc:___._____ CPU:___%___%\n\r", f);
    fputs("PERMIT:       PolSw:       Sta:                        Vd:___._____  __._C __._C"    , f);

    // Set up scroll window to protect RTD lines

    fputs(TERM_HOME "\33[1;19r\v", f);

    // Reset func fields

    struct Rtd_field_func * field_func;

    for (field_func = &rtd_class_field_funcs[0]; field_func->func != NULL; field_func++)
    {
        field_func->func(true);
    }

    // Reset float fields

    struct Rtd_field_float * field_float;

    for (field_float = &rtd_class_field_floats[0]; field_float->source != NULL; field_float++)
    {
        field_float->last_value = 0x6FFFFFFF;
    }

    struct Rtd_field_error_float * field_error_float;

    for (field_error_float = &rtd_class_field_floats_errors[0]; field_error_float->field.source != NULL; field_error_float++)
    {
        field_error_float->field.last_value = 0x6FFFFFFF;
        field_error_float->error.last_value = 0xFF;
    }

    // Reset all option line variables

    struct Rtd_field_optional * field;

    for (field = &rtd_class_field_optional[0]; field->func != NULL; field++)
    {
        field->func(true);
    }
}



bool rtdClassProcess(bool reset)
{
    static struct Rtd_process * prev_process = rtd_class_process;

    struct Rtd_process * field   = prev_process;
    bool                 updated = false;

    for (;  field->func != NULL && updated == false; field++)
    {
        updated = field->func(&field->args);
    }

    prev_process = (field->func == NULL ? rtd_class_process : field);

    return updated;
}



void rtdClassUpdateErrors(void)
{
    uint8_t   idx;

    for (idx = 0; idx < FGC_N_INT_ADCS; idx++)
    {
        rtd_class_field_floats_errors[idx].error.value = (testBitmap(dpcom.dsp.meas.adc_state[idx], FGC_ADC_STATUS_V_MEAS_OK) == false)
                                                       ? RTD_VALUE_FAULT
                                                       : 0;
    }
}



// ---------- Internal function definitions

static bool rtdPrintStatus(bool reset)
{
    static struct Rtd_field_string field_str   = { "", RTD_POS_STATUS, TERM_BOLD };
    static uint32_t                prev_status = 0xFFFFFFFF;

    // Reset faults

    if (reset == true)
    {
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));
        prev_status = 0xFFFFFFFF;

        return false;
    }

    if (prev_status == dpcls.mcu.pulse.status)
    {
        return false;
    }

    prev_status = dpcls.mcu.pulse.status;

    return rtdPrintString(pulseGetStatusStr(dpcls.mcu.pulse.status), &field_str);
}



static bool rtdClassPolSwitch(bool reset)
{
    static uint32_t                prev_pol_switch_state = 0xFFFFFFFF;
    static struct Rtd_field_string field_str             = { "", RTD_POS_POL_SW, TERM_BOLD };

    // Reset faults

    if (reset == true)
    {
        prev_pol_switch_state = 0xFFFFFFFF;
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));

        return false;
    }

    uint32_t pol_switch_state = polSwitchGetState();

    if (prev_pol_switch_state == pol_switch_state)
    {
        return false;
    }

    prev_pol_switch_state = pol_switch_state;

    switch (pol_switch_state)
    {
        case FGC_POL_SWITCH_MOVING:
            return rtdPrintString("MOVE ", &field_str);

        case FGC_POL_POSITIVE:
            return rtdPrintString("POS  ", &field_str);

        case FGC_POL_NEGATIVE:
            return rtdPrintString("NEG  ", &field_str);

        case FGC_POL_SWITCH_FAULT:
            return rtdPrintString("FAULT", &field_str);

        case FGC_POL_NO_SWITCH:
            return rtdPrintString("NONE ", &field_str);

        default:
            return false;
    }
}



static bool rtdClassOptionCyc(bool reset)
{
    static struct Rtd_field_string field_str = { "", RTD_POS_CYC, TERM_BOLD };

    if (reset == true)
    {
        memset(field_str.last_value, '\0', sizeof(field_str.last_value));
        return false;
    }

    uint8_t sub_sel = dpcom.dsp.cycle.current.sub_sel_acq;
    uint8_t cyc_sel = dpcom.dsp.cycle.current.cyc_sel_acq;
    char    cyc_str[26];

    snprintf(cyc_str, 25, "PPM: %2d-%-2d", sub_sel, cyc_sel);

    return rtdPrintString(cyc_str, (void *) &field_str);
}




static bool rtdOptionAdcsAB(bool reset)
{
    static struct Rtd_field_float adc_floats[] =
    {
        { (float *)&dpcom.dsp.adc.volts_1s   [0], "%*.6f", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_L_VOLTS, 10 },
        { (float *)&dpcom.dsp.adc.pp_volts_1s[0], "%*.1E", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_L_PP   , 7  },
        { (float *)&dpcom.dsp.adc.cal_meas_1s[0], "%*.3f", 0x6FFFFFFF, 1000   , RTD_POS_ADCS_L_AMPS , 10 },
        { (float *)&dpcom.dsp.adc.volts_1s   [1], "%*.6f", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_R_VOLTS, 10 },
        { (float *)&dpcom.dsp.adc.pp_volts_1s[1], "%*.1E", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_R_PP   , 7  },
        { (float *)&dpcom.dsp.adc.cal_meas_1s[1], "%*.3f", 0x6FFFFFFF, 1000   , RTD_POS_ADCS_R_AMPS , 10 },
        { NULL                                                                                                       },
    };

    static struct Rtd_field_int adc_ints[] =
    {
        { (int32_t *)&dpcom.dsp.adc.raw_1s[0], 0x7FFFFFFF, "%9ld", RTD_POS_ADCS_L_RAW },
        { (int32_t *)&dpcom.dsp.adc.raw_1s[1], 0x7FFFFFFF, "%9ld", RTD_POS_ADCS_R_RAW },
        { NULL                                                                        },
    };

    return (rtdPrintAdcsHelper(adc_floats, adc_ints, reset));
}



static bool rtdOptionAdcsCD(bool reset)
{
    static struct Rtd_field_float adc_floats[] =
    {
        { (float *)&dpcom.dsp.adc.volts_1s   [2], "%*.6f", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_L_VOLTS, 10 },
        { (float *)&dpcom.dsp.adc.pp_volts_1s[2], "%*.1E", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_L_PP   , 7  },
        { (float *)&dpcom.dsp.adc.cal_meas_1s[2], "%*.3f", 0x6FFFFFFF, 1000   , RTD_POS_ADCS_L_AMPS , 10 },
        { (float *)&dpcom.dsp.adc.volts_1s   [3], "%*.6f", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_R_VOLTS, 10 },
        { (float *)&dpcom.dsp.adc.pp_volts_1s[3], "%*.1E", 0x6FFFFFFF, 1000000, RTD_POS_ADCS_R_PP   , 7  },
        { (float *)&dpcom.dsp.adc.cal_meas_1s[3], "%*.3f", 0x6FFFFFFF, 1000   , RTD_POS_ADCS_R_AMPS , 10 },
        { NULL                                                                                                       },
    };

    static struct Rtd_field_int adc_ints[] =
    {
        { (int32_t *)&dpcom.dsp.adc.raw_1s[2], 0x7FFFFFFF, "%9ld", RTD_POS_ADCS_L_RAW },
        { (int32_t *)&dpcom.dsp.adc.raw_1s[3], 0x7FFFFFFF, "%9ld", RTD_POS_ADCS_R_RAW },
        { NULL                                                                        },
    };

    return (rtdPrintAdcsHelper(adc_floats, adc_ints, reset));
}



static bool rtdPrintAdcsHelper(struct Rtd_field_float * adc_floats, struct Rtd_field_int * adc_ints, bool reset)
{
    static uint8_t adc_xy_idx = 0;

    if (reset == true)
    {
        struct Rtd_field_float * field_float = adc_floats;

        for (; field_float->source != NULL; field_float++)
        {
            field_float->last_value = 0x6FFFFFFF;
        }

        struct Rtd_field_int * field_int = adc_ints;

        for (; field_int->source != NULL; field_int++)
        {
            field_int->last_value = 0x7FFFFFFF;
        }

        adc_xy_idx = 0;

        return false;
    }

    bool updated;

    if (adc_xy_idx < 2)
    {
        updated = rtdPrintInt((void *)&adc_ints[adc_xy_idx]);
    }
    else
    {
        updated = rtdPrintFloat((void *)&adc_floats[adc_xy_idx - 2], NULL);
    }

    if (++adc_xy_idx >= 8)
    {
        adc_xy_idx = 0;
    }

    return updated;
}


// EOF
