//! @file  ilc.c
//! @brief Iterative learning controller
//!
//! https://issues.cern.ch/browse/EPCCCS-8384
//!
//! Before cycle:
//! REF = REF_USER + REF_ILC
//!
//! After cycle:
//! ERR_ILC = REF_USER – MEAS
//! REF_ILC += K_INT * ERR_ILC
//!
//! Translation into q-domain
//! REF      = u(k)
//! REF_USER = r(k)
//! REF_ILC  = r’(k)
//! ERR_ILC  = r(k-1) – y(k-1)
//! REF_ILC += K_INT * ERR_ILC → r’(k) = r’(k-1) + ki *[r(k-1) – y(k-1) ]
//!
//! MEAS refers to the value acquired at the event time
//!
//! Assuming
//! y(k) = u(k)
//! u(k) = r(k) + r’(k)
//!
//! Complementary Sensitivity Function
//! y(k)  = r(k)
//!
//! Calculations omitted but verified
//! Sensitivity Function
//! y(k)  = (q – 1) / [q - (1-ki)] dy(k)
//!
//! Pole in (1- ki ) so 0 < ki <  2
//! Zero in 1  ok
//! DC gain : zero → it rejects step disturbance
//! ki can be chosen starting from the modulus margin mm according to ki = 2(1-mm)
//!
//! Although one gets a stable pole also with ki > 1 the reaction to an offset change would be
//! oscillating in time; with ki = 1 a step change in the offset would be rejected after exaxctly
//! one pulse.
//!
//! Calculations omitted but verified
//!
//! Calling this controller an ILC is a bit of a stretch, it was agreed to do so only to profit
//! of structures and codes already available. This controller is a 2-dof controller like the RST.
//! Indeed an RST implementation could be as follows:
//!
//! T(z^-1) = 1/ki [1 + (ki-1) z^-1]
//! R(z^-1) = z^-1
//! S(z^-1) = ki / (1 - z^-1)
//!
//! Settable properties:
//!
//! MODE.ILC            {ENABLED, DISABLED}
//! REF.ILC.K_INT       [0.0..0.9]           Maybe not required
//! REF.ILC.LOG_USER    [0..32]              For ILC_CYC log


// ---------- Includes

#include <stdint.h>
#include <string.h>

#include <cclibs.h>

#include <ilc.h>
#include <pc_state.h>
#include <pulse.h>
#include <status.h>



// ---------- Internal structures, unions and enumerations

//! Internal local variables for the module log_class.

struct Ilc
{
    float error_pct[FGC_MAX_SUB_DEVS][FGC_MAX_USER_PLUS_1];  //!< Error in perecentage
};



// ---------- External variable definitions

struct Ilc_props ilc_props;



// ---------- Internal variable definitions

struct Ilc ilc;



// ---------- Internal function definitions


static void ilcSetState(uint32_t const state)
{
    uint8_t sub_sel_idx;
    uint8_t cyc_sel_idx;

    for (sub_sel_idx = 0; sub_sel_idx < FGC_MAX_SUB_DEVS; sub_sel_idx++)
    {
        for (cyc_sel_idx = 0; cyc_sel_idx < FGC_MAX_USER_PLUS_1; cyc_sel_idx++)
        {
            ilc_props.state[sub_sel_idx][cyc_sel_idx] = state;
        }
    }
}



// ---------- External function definitions


void ilcInit(void)
{
    ilcResetDataAll();

    ilcSetState(FGC_ILC_STATE_DISABLED);

    // Define the default value of the integrator gain and log user

    ilc_props.k_init   = 0.5;
    ilc_props.log_user = 0;
}



void ilcProcess(uint32_t const sub_sel,
                uint32_t const cyc_sel,
                uint32_t const cyc_sel_acq,
                float    const ref_user,
                float    const meas)
{
    static bool prev_mode_ilc   = CC_DISABLED;
    static bool exiting_cycling = false;

    // Reset the ILC when disabling MODE.ILC

    if (prev_mode_ilc == CC_ENABLED && ilc_props.mode == CC_DISABLED)
    {
        ilcResetDataAll();
        ilcSetState(FGC_ILC_STATE_DISABLED);
    }

    // Transition to NOT_READY when enabling MODE.ILC

    if (prev_mode_ilc == CC_DISABLED && ilc_props.mode == CC_ENABLED)
    {
        ilcSetState(FGC_ILC_STATE_NOT_READY);
    }

    prev_mode_ilc = ilc_props.mode;

    // The regulation only runs if
    //    1) MODE.ILC is ENABLED
    //    2) The state is CYCLING
    //    3) The user reference is not zero

    if (pcStateGet() == FGC_PC_CYCLING)
    {
        exiting_cycling = true;

        // Calculate the error and reference if the regulation is enabled

        if (ilc_props.mode == CC_ENABLED && ref_user != 0.0F)
        {
            // The ILC state is OK only if two consecutive errors are less than 7%
            // Thereafter if the error is larger than 5% a warning is thrown.

            static float const ILC_LIMIT_ACTIVE  = 0.07;
            static float const ILC_LIMIT_WARNING = 0.05;

            float    error     = ref_user - meas;
            float    error_pct = fabs(error / ref_user);

            if (ilc_props.state[sub_sel][cyc_sel] != FGC_ILC_STATE_CLIPPED)
            {
                if (   ilc.error_pct[sub_sel][cyc_sel] < ILC_LIMIT_ACTIVE
                    && error_pct                       < ILC_LIMIT_ACTIVE)
                {
                    ilc_props.state    [sub_sel][cyc_sel]  = FGC_ILC_STATE_OK;
                    ilc_props.reference[sub_sel][cyc_sel] += ilc_props.k_init * (ref_user - meas);
                }
                else
                {
                    ilc_props.state    [sub_sel][cyc_sel] = FGC_ILC_STATE_NOT_READY;
                    ilc_props.reference[sub_sel][cyc_sel] = 0;
                }
            }

            ilc_props.error[sub_sel][cyc_sel] = error;
            ilc.error_pct  [sub_sel][cyc_sel] = error_pct;

            bool ilc_error_warning = (   ilc_props.state[sub_sel][cyc_sel] == FGC_ILC_STATE_OK
                                      && error_pct > ILC_LIMIT_WARNING);

            statusEvaluateWarnings(FGC_WRN_ILC_STATE, (   ilc_props.state[sub_sel][cyc_sel] == FGC_ILC_STATE_CLIPPED
                                                       || ilc_props.state[sub_sel][cyc_sel] == FGC_ILC_STATE_NOT_READY
                                                       || ilc_error_warning == true));
        }
    }
    else
    {
        if (exiting_cycling == true)
        {
            statusClrWarnings(FGC_WRN_ILC_STATE);
            ilcSetState(ilc_props.mode == CC_DISABLED ? FGC_ILC_STATE_DISABLED : FGC_ILC_STATE_NOT_READY);

            exiting_cycling = false;
        }
    }

    // Update ILC_CYC log

    uint8_t cyc_sel_log = ilc_props.log_user;

    // dpcls.mcu.log.meas   = meas; // Available on the DSP
    dpcls.mcu.log.ref_user    = ref_user;
    dpcls.mcu.log.ref_ilc     = ilc_props.reference[sub_sel][cyc_sel_log];
    dpcls.mcu.log.ref         = dpcls.mcu.log.ref_user + dpcls.mcu.log.ref_ilc;
    dpcls.mcu.log.error       = ilc_props.error[sub_sel][cyc_sel_log];
    dpcls.mcu.log.error_pct   = ilc.error_pct  [sub_sel][cyc_sel_log];
    dpcls.mcu.log.state       = ilc_props.state[sub_sel][cyc_sel_log];
    dpcls.mcu.log.cyc_sel_log = cyc_sel_log;

    __sync_synchronize();

    dpcls.mcu.log.cyc_sel_acq = cyc_sel_acq;
}



float ilcClipGetReference(uint32_t      const   sub_sel,
                          uint32_t      const   cyc_sel,
                          float         const   ref_user,
                          struct Pulse_limits * limits)
{
    if (ilc_props.mode == CC_DISABLED)
    {
        return ref_user;
    }

    // Add 5% to LIMITS.PULSE.POS and LIMITS.PULSE.NEG but clip at LIMITS.PULSE.NOMINAL

    float reference     = ref_user + ilc_props.reference[sub_sel][cyc_sel];
    float limit_nominal = fabs(limits->nominal);
    float limit_pos     = limits->pos * 1.05F;
    float limit_neg     = limits->neg * 1.05F;

    if (limit_pos > limit_nominal)
    {
        limit_pos = limit_nominal;
    }

    if (limit_neg < -limit_nominal)
    {
        limit_neg = -limit_nominal;
    }

    // Warnings are latched for 10 seconds. If the conditions is still present,
    // the warning will persist. Otherwise, it will be cleared after that
    // timeout.

    if (reference > limit_pos)
    {
        reference = limit_pos;

        ilc_props.state[sub_sel][cyc_sel] = FGC_ILC_STATE_CLIPPED;
    }
    else if (reference < limit_neg)
    {
        reference = limit_neg;

        ilc_props.state[sub_sel][cyc_sel] = FGC_ILC_STATE_CLIPPED;
    }
    else
    {
        if (ilc_props.state[sub_sel][cyc_sel] == FGC_ILC_STATE_CLIPPED)
        {
            ilc_props.state[sub_sel][cyc_sel] = FGC_ILC_STATE_OK;
        }
    }

    return reference;
}


void ilcResetDataAll(void)
{
    memset(ilc_props.reference, 0, sizeof(ilc_props.reference ));
    memset(ilc_props.error,     0, sizeof(ilc_props.error     ));
    memset(ilc_props.state,     0, sizeof(ilc_props.state     ));
    memset(ilc.error_pct,       0, sizeof(ilc.error_pct ));
}


// EOF
