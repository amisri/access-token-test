//! @file  modePC_class.c
//! @brief Class related handling of modePc


// ---------- Includes

#include <stdint.h>

#include <defprops.h>
#include <modePc_class.h>
#include <statePc.h>
#include <pc_state.h>
#include <fgc_errs.h>



// ---------- Platform/class specific function definitions

uint32_t modePcClassValidateMode(uint8_t mode)
{
    switch (mode)
    {
        case FGC_PC_OFF:
        {
            if (pcStateTest(FGC_STATE_VALID_RESET_BIT_MASK) == true)
            {
                statePcResetFaults();
            }

            break;
        }

        case FGC_PC_ON_STANDBY:  // Fall through
        case FGC_PC_DIRECT:      // Fall through
        case FGC_PC_IDLE:
        {
            return FGC_BAD_STATE;
        }

        case FGC_PC_BLOCKING:
        {
            if (dpcls.mcu.vs.blockable == FGC_VS_BLOCK_DISABLED)
            {
                return FGC_BLOCKING_NOT_ALLOWED;
            }

            // Fall through
        }

        case FGC_PC_CYCLING:     // Fall through
        default:
        {
            if (pcStateTest(  FGC_STATE_FLT_OFF_BIT_MASK 
                            | FGC_STATE_FLT_STOPPING_BIT_MASK 
                            | FGC_STATE_STOPPING_BIT_MASK) == true)
            {
                return FGC_BAD_STATE;
            }

            break;
        }
    }

    return FGC_OK_NO_RSP;
}


// EOF