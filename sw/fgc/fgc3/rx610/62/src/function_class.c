//! @file  function_class.c
//! @brief Class specific handling of function arming and ancillary functions


// ---------- Includes

#include <function_class.h>
#include <pc_state.h>



// ---------- External variable definitions

struct Function_props ref;



// ---------- Platform/class specific function definitions

char const * functionGetDefaultPrefix(void)
{
    return (pcStateTest(FGC_STATE_GE_BLOCKING_BIT_MASK) == true ? "!S REF.PULSE.REF_LOCAL " : "");
}


// EOF
