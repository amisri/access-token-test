/*---------------------------------------------------------------------------------------------------------*\
  File:     menuExpert_PLL.c

  Purpose:  Functions for the Expert submenu

\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>           // basic typedefs
#include <iodefines.h>          // specific processor registers and constants
#include <stdio.h>              // for fprintf()
#include <analog.h>             // for ana_temp_regul
#include <defsyms.h>            // for sym_names_interface[]
#include <dev.h>                // for dev global variable
#include <dsp_loader.h>         // for dsp global variable
#include <fieldbus_boot.h>      // for fieldbus, stat_var global variable
#include <hardware_setup.h>     // for SoftwareReset())
#include <master_clk_pll.h> // for global variables
#include <mcuDependent.h>      // for MSLEEP(), USLEEP()
#include <mem.h>
#include <memmap_mcu.h>         // Include memory map constants
#include <menu.h>               // for MenuGetInt32U(), MenuGetInt16U(), MenuRspArg(), MenuRspError()
#include <pll.h>            // for pll global variable
#include <runlog_lib.h>         // for RunlogClrResets(), RunlogInit()
#include <term.h>
#include <trap.h>               // for trap global variable, IsrDummy1(), IsrUndefinedInstruction()

/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertPllSetPhase(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Set PLL phase
\*---------------------------------------------------------------------------------------------------------*/
{
    if (MenuGetFloat(argv[0], -100.0, +100.0, &pll.dac_ppm) != 0)
    {
        return;
    }

    pll.enabled = FALSE;

    PllDacVcxo();

    PLL_DACVCXO_INT_P = pll.dacvcxo_int;
    PLL_DACVCXO_FRAC_P = pll.dacvcxo_frac;

    fprintf(term.f, "\nVCxO ppm   : %lf - %hi %04hX\n", (double) pll.dac_ppm, pll.dacvcxo_int, pll.dacvcxo_frac);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertPllShow(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show PLL parameters
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(term.f, "\n");
    fprintf(term.f, RSP_DATA ", VCxO ppm        : %lf - %hi %04hX\n", (double) pll.dac_ppm, pll.dacvcxo_int,
            pll.dacvcxo_frac);
    fprintf(term.f, RSP_DATA ", integrator ppm  : %lf\n", (double) pll.integrator_ppm);
    fprintf(term.f, RSP_DATA ", eth sync cal    : %lf\n", (double) pll.eth_sync_cal);
    fprintf(term.f, RSP_DATA ", state           : %u\n", pll.state);
    fprintf(term.f, RSP_DATA ", ext sync        : %u\n", pll.ext_sync_f);

    fprintf(term.f, RSP_DATA ", no ext count    : %u\n", pll.no_ext_sync_count);

    fprintf(term.f, RSP_DATA ", pi error        : %lf\n", (double) pll.pi_err);
    fprintf(term.f, RSP_DATA ", net ph error avg: %lf\n", (double) pll.network.phase_err_average);
    fprintf(term.f, RSP_DATA ", net win ph error: %lf\n", (double) pll.network_windowed.phase_err_average);
    fprintf(term.f, RSP_DATA ", net syn count   : %u\n", pll.network.prev_sync_count);
    fprintf(term.f, RSP_DATA ", net win sync cnt: %u\n", pll.network_windowed.prev_sync_count);

    fprintf(term.f, RSP_DATA ", filt integ ppm  : %lf\n", (double) pll.filtered_integrator_ppm);

    fprintf(term.f, RSP_DATA ", ext ph error    : %lf\n", (double) pll.external.phase_err_average);
    fprintf(term.f, RSP_DATA ", ext syn count   : %u\n", pll.external.prev_sync_count);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertResetPll(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Resets PLL parameters
\*---------------------------------------------------------------------------------------------------------*/
{
    pll.enabled = TRUE;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuExpert_PLL.c
\*---------------------------------------------------------------------------------------------------------*/
