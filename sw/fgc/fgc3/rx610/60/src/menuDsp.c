/*---------------------------------------------------------------------------------------------------------*\
  File:         menuDsp.c

  Purpose:      Memory test functions

  Author:       daniel.calcoen@cern.ch

  Notes:
                These menus belongs to the [Manual Test] part and [Self Test] part

  History:

    25 jun 07    doc    Created
\*---------------------------------------------------------------------------------------------------------*/

#include <bitmap.h>
#include <boot_dual_port_ram.h> // for dpram global variable
#include <stdint.h>           // basic typedefs
#include <stdio.h>              // for fprintf()
#include <stdlib.h>             // for labs()
#include <term.h>
#include <boot_dsp_cmds.h>      // constants shared by DSP and CPU
#include <iodefines.h>          // specific processor registers and constants
#include <mcuDependent.h>      // for USLEEP()
#include <menu.h>               // for MenuRspError()
#include <dev.h>                // for global variable dev
#include <derived_clocks.h>     // for StartDspClock(), StopDspClock()
#include <dsp_cmds.h>           // for DSP_SLOW_CMD_TIMEOUT_10MS, SendDspSlowCmdAndWaitForRsp()
#include <dsp_FPU_benchmark.h>  // for Dsp_FPU_Benchmark()
#include <dsp_loader.h>         // for BootDspAndLoadWith(), dsp_error_lbl[]
#include <dsp_60.h>             // constant array with the TMS320C6727 code
#include <sleep.h>
#include <fgc_runlog_entries.h> // Include run log definition, for FGC_RL_
#include <runlog_lib.h>         // for RunlogWrite()
#include <structs_bits_big.h>   // MOTOROLA (big endian) bits, bytes, words, dwords ...

#include <boot_dual_port_ram.h> // for dpram global variable


/*---------------------------------------------------------------------------------------------------------*/
void MenuSTdspBoot(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function tests the dsp startup.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t status;

    // Test access to register

    if (dev.reg.dpram != 0)
    {
        MenuRspError("No access");
        return;
    }

    status = dspLoadImage(dsp_60_code, DSP_60_CODE_SIZE); // load DSP boot and run it

    if (status != 0)
    {
        fprintf(TERM_TX, RSP_ERROR ",%s\n", dspLoadErrorStr(status));
    }

    StartDspClock();  // Starts DSP tick (DSP NMI interrupt generation by the PLD)
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTdspIrq(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function checks the DSP receives interrupts.
  uses DSP_CMD_FAST_RETURN_NEG_DATA
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t test_w = 0xAA123455;
    uint32_t irq_counter = 0;

    // Test access to register

    if (dev.reg.dpram != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Check dsp running flag

    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    // Launch INV test in dsp at isr level

    SendDspFastCmdAndWait110us(DSP_CMD_FAST_RETURN_NEG_DATA, test_w);

    if (dpram->dsp.fastRsp.data != ~test_w)
    {
        MenuRspError("expect:0x%08lX rx:0x%08lX", ~test_w, dpram->dsp.fastRsp.data);
        //      MenuRspError("DSP ans=0x%08lX-[0x%08lX]", dpram->dsp.fastRsp.data, test_w);
        return;
    }

    // Check interrupt counter incremented by DSP in DPRAM

    irq_counter = dpram->dsp.irq_counter;
    USLEEP(1000); // 1ms
    irq_counter = dpram->dsp.irq_counter - irq_counter;

    MenuRspError("%lu irqs in 1ms", irq_counter);
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspStart(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Starts DSP NMI Tick
\*---------------------------------------------------------------------------------------------------------*/
{
    StartDspClock(); // Starts DSP tick (DSP NMI interrupt generation by the PLD)
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspStop(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Stops DSP tick (DSP NMI interrupt generation by the PLD) and puts the DSP in reset
\*---------------------------------------------------------------------------------------------------------*/
{
    // this will stop also, DAC and ADC as both are derived from DSP NMI Tick
    StopDspClock(); // Stops DSP tick (DSP NMI interrupt generation by the PLD)

    // DSP reset
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_DSP_MASK16;
    dsp.is_running = FALSE;     // remove tms320c6727 run flag
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspProg(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Write 1st 1Kbytes in DPRAM then release DSP reset.
\*---------------------------------------------------------------------------------------------------------*/
{
    union TUnion32Bits  error_code;

    error_code.part.lo = dspLoadImage(dsp_60_code, DSP_60_CODE_SIZE); // load DSP boot and run it

    if (error_code.part.lo != 0)
    {
        // from c32.h
        // #define C32_STATUS_FAILED_TO_START      0x0100
        error_code.part.hi = 0x0100;

        // ToDo: for the moment FGC_RL_DSP_LOAD only uses the first 16bits (hi)
        //       fgc_rl_length[19] is 1, need to check with FGC2 to enlarge to 2 so
        //       able to use this error code + status returned by BootDspAndLoadWith()
        RunlogWrite(FGC_RL_DSP_LOAD, &error_code);
        //        MenuRspError("%s", TypeLabel(dsp_error_lbl, error_code.part.lo) );
        fprintf(TERM_TX, RSP_ERROR ",%s\n", dspLoadErrorStr(error_code.part.lo));
    }

    dsp.status = error_code.part.lo;

    StartDspClock();  // Starts DSP tick (DSP NMI interrupt generation by the PLD)
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspResetFlt(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Reset faults in the DSP
\*---------------------------------------------------------------------------------------------------------*/
{
    // Check dsp is running
    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    if (SendDspFastCmdAndWait110us(DSP_CMD_FAST_CLEAR_INFO, 0) != 0)
    {
        MenuRspError("DSP rsp timeout");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTdspAlive(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function checks if DSP is alive.
  uses DSP_CMD_SLOW_RETURN_NEG_DATA
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t test_w = 0xAA123455;

    // Test access to register

    if (dev.reg.dpram != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Check dsp is running
    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    // Launch INV bg test in dsp

    // prepare argument for the command
    dpram->mcu.slowArg.data = test_w;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_RETURN_NEG_DATA, DSP_SLOW_CMD_TIMEOUT_100MS) != 0)
    {
        MenuRspError("DSP rsp timeout");
        return;
    }

    if (dpram->dsp.slowRsp.data != ~test_w)
    {
        MenuRspError("expect:0x%08lX rx:0x%08lX", ~test_w, dpram->dsp.slowRsp.data);
        //      MenuRspError("DSP ans=0x%08lX-[0x%08lX]", dpram->dsp.slowRsp.data, test_w);
        return;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspAlive(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Tests DSP is alive.
  uses DSP_CMD_SLOW_RETURN_NEG_DATA
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t test_w = 0xAA123455;
    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    // prepare argument for the command
    dpram->mcu.slowArg.data = test_w;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_RETURN_NEG_DATA, DSP_SLOW_CMD_TIMEOUT_5S) != 0)
    {
        MenuRspError("DSP rsp timeout");
        return;
    }

    if (dpram->dsp.slowRsp.data != ~test_w)
    {
        MenuRspError("expect:0x%08lX rx:0x%08lX", ~test_w, dpram->dsp.slowRsp.data);
        return;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspDPRAMStress(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Stress test for DPRAM.
  MCU <-> DSP DPRAM access, the DSP just write back what it reads continuously
  MCU checks what DSP writes is coherent.
  32 bits accesses can be wrong if we read new high word and old low word for instance (16 bit bus).
  Therefore, test is done on 16 bits only (atomic access).
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t dsp_read = 0;
    uint16_t dpram_w = 1;
    uint32_t mcu_read_error = 0;
    uint32_t count_word_loop = 0;

    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    dpram->mcu.slowArg.data = 0;
    SendDspFastCmdAndWait110us(DSP_CMD_FAST_STRESS_TST_PLD_DPRAM, 0);

    // Stop by ctrl+c or after 2^32 iterations
    while (!dev.abort_f)
    {

        // Write data
        dpram->mcu.slowArg.data = dpram_w;

        dsp_read = (uint16_t) dpram->dsp.slowRsp.data;

        if (dsp_read == dpram_w)
        {
            dpram_w++;
        }
        //     else if (dsp_read == (uint16_t)(dpram_w - 1))
        else if (dsp_read == dpram_w - 1) // Cast mandatory, gcc bug

        {
            // read previous value, ok
        }
        else
        {
            mcu_read_error++;
            fprintf(term.f, RSP_DATA ",[0x%04X] loop %lu, mcu read error :0x%04X\n", dpram_w, count_word_loop, dsp_read);
        }

        // Count number of 2^16 transactions
        if (dpram_w == 0)
        {
            count_word_loop++;

            if ((count_word_loop % 128) == 0)
            {
                fprintf(term.f, RSP_DATA ",Performed %lu read-write tests\r", count_word_loop * 0xFFFF + dpram_w);
            }

        }
    }

    MenuRspArg("Performed %u read-write tests, found %u error(s)",
               count_word_loop * 0xFFFF + dpram_w, mcu_read_error);
    dpram->mcu.slowArg.data = 0xFFFFFFFF; // This will stop the test on dsp side

}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspOutFuncDownload(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Copy functions to PLD DpRam
\*---------------------------------------------------------------------------------------------------------*/
{
    /*
        if ( dsp.is_running == FALSE )
        {
            MenuRspError("DSP does not run");
            return;
        }
    */
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspRunOutFunc(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Launch DSP function generation
\*---------------------------------------------------------------------------------------------------------*/
{
    /*
        if ( dsp.is_running == FALSE )
        {
            MenuRspError("DSP does not run");
            return;
        }
    */
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspStatistics(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Show time use of DSP
\*---------------------------------------------------------------------------------------------------------*/
{
    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_INFO, DSP_SLOW_CMD_TIMEOUT_100MS) != 0)
    {
        MenuRspError("DSP rsp timeout");
        return;
    }

    fprintf(term.f, RSP_DATA ", Tick Isr %-6lu - max %-6lu\n",
            dpram->dsp.slowRsp.get_info.time_consumed_in_msTickIsr,
            dpram->dsp.slowRsp.get_info.time_consumed_in_msTickIsr_max);
    fprintf(term.f, RSP_DATA ", Interpol %-6lu - max %-6lu\n",
            dpram->dsp.slowRsp.get_info.time_consumed_in_interpolation,
            dpram->dsp.slowRsp.get_info.time_consumed_in_interpolation_max);
    fprintf(term.f, RSP_DATA ", Tick     %-6lu\n", dpram->dsp.slowRsp.get_info.msTick);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspTestFPU(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] FPU benchmark
\*---------------------------------------------------------------------------------------------------------*/
{
    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    if (Dsp_FPU_Benchmark() == FALSE)
    {
        MenuRspError("DSP in standalone mode");
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuDsp.c
\*---------------------------------------------------------------------------------------------------------*/
