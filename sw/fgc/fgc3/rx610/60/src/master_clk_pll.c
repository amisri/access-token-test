/*!
 *  @file     master_clk_pll.h
 *  @defgroup FGC3:MCU
 *  @brief    RT Clock Phase Locked Loop Functions.
 *
 */

#define MASTER_CLK_PLL_GLOBALS      // define global variable

// Includes

#include <master_clk_pll.h>

#include <defconst.h>           // for FGC_PLL_NO_SYNC
#include <led.h>
#include <mcuDependent.h>      // for NVRAM_UNLOCK(), NVRAM_LOCK()
#include <memmap_mcu.h>
#include <pll.h>                // for pll global variable, PllInit()
#include <sharedMemory.h>      // for shared_mem global variable
#include <sleep.h>
#include <boot_dual_port_ram.h> // for dpram global variable
#include <dev.h>                // for dev global variable
#include <fieldbus_boot.h>      // for fieldbus global variable, stat_var
#include <lan92xx.h>
#include <pld.h>                // for pld_info global variable
#include <runlog_lib.h>

// Macros

// Macro arguments are expanded except if with # or ##, so we need 2 level of indirection
#define CLASS_FIELD_STRING(name)        c##name
#define CLASS_FIELD_STRING2(name)       CLASS_FIELD_STRING(name)
#define CLASS_FIELD                     CLASS_FIELD_STRING2(FGC_CLASS_ID)   // c##FGC_CLASS_ID

// External function definitions



void MasterClockAnaloguePllInit(void)
{
    pll.enabled = TRUE;

    stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll = FGC_PLL_NO_SYNC;

    PllInit(NVRAM_PLL_INTEGRATOR_PPM_P, NVRAM_PLL_ETH_SYNC_CAL_P);

    PLL_DACVCXO_INT_P = pll.dacvcxo_int;
    PLL_DACVCXO_FRAC_P = pll.dacvcxo_frac;
}



void MasterClockAnaloguePllIteration(void)
{
    uint32_t  int_sync_time;
    uint16_t  previous_state;

    pll.ext.sync_time = PLL_EXTSYNCTIME_P;

    if (pll.enabled)
    {
        previous_state = pll.state;

        int_sync_time  = PllCalc(PLL_INTSYNCTIME_P);

        // Log new PLL state

        if (previous_state != pll.state)
        {
            RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));
            RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));
            RunlogWrite(FGC_RL_PLL_STATE, &pll.state);
        }

        // Set the LED NET if the external sync is not available, or there are no network packets

        if (!pll.ext_sync_f || (pll.iter_count && pll.network.sync_count == 0))
        {
            LED_SET(NET_RED);
        }
        else
        {
            // Turn NET_RED on. It will appear as orange (see doc, NET_GREEN is lighted by FPGA)

            LED_RST(NET_RED);
        }

        // don't write/lock it until the algorithm is stabilised
        // signalled by the rst mask

        // TODO The code below won't run the PLL when physical link is present but there's no gateway!

        if (   (int_sync_time & PLL_INTSYNCTIME_RST_MASK32) == 0
            || dev.is_standalone == true
            || Lan92xxIsLinkUp() == false)
        {
            PLL_INTSYNCTIME_P = int_sync_time;
        }

        PLL_DACVCXO_INT_P  = pll.dacvcxo_int;
        PLL_DACVCXO_FRAC_P = pll.dacvcxo_frac;
    }

    if (pll.state == FGC_PLL_LOCKED)
    {
        // update NVRAM
        // Note : We write NVRAM at PLL iteration frequency.
        // The technology of the MRAM used in FGC3 does not have a limited number of write cycles

        NVRAM_UNLOCK();

        NVRAM_PLL_INTEGRATOR_PPM_P  = pll.integrator_ppm;
        NVRAM_PLL_ETH_SYNC_CAL_P    = pll.eth_sync_cal;

        NVRAM_LOCK();
    }

    // boot debugging with spy

    dpram->mcu.pll_debug_data[0] = pll.ext.phase_err;
    dpram->mcu.pll_debug_data[1] = pll.eth18.phase_err;
    dpram->mcu.pll_debug_data[2] = pll.eth19.phase_err;
    dpram->mcu.pll_debug_data[3] = (uint32_t) pll.pi_err;
    dpram->mcu.pll_debug_data[4] = pll.network.sync_count;
    dpram->mcu.pll_debug_data[5] = pll.network_windowed.sync_count;

    // update status

    stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll = pll.state;
}



bool Wait10secForStablePLL(void)
{
    uint16_t      timeout_ms = 10000;

    // only worth if the PLD is programmed
    if (!pld_info.ok)
    {
        return (FALSE);
    }

    // wait while PLL_INTSYNCTIME_P is pristine during 1 second
    while (((PLL_INTSYNCTIME_P & PLL_INTSYNCTIME_RST_MASK32) == PLL_INTSYNCTIME_RST_MASK32)
           && pll.state != FGC_PLL_LOCKED
           && (timeout_ms != 0)
          )
    {
        timeout_ms--;
        USLEEP(1000); // 1ms
    }

    // Even though the main PLL is LOCKED, the DSP bootloader may fail to initialise its internal PLL.
    // The additional sleep helps, but it's not a definitive solution

    sleepMs(1000);

    return (timeout_ms != 0);
}

// EOF
