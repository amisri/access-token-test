/*---------------------------------------------------------------------------------------------------------*\
  File:         term_boot.c

  Purpose:      Serial I/O functions

  Author:       Quentin.King@cern.ch

  Notes:

  History:

    27/08/03    stp     Created based upon v2 boot serial I/O
    10/03/05    qak     File renamed sci.c.  Rx/Tx buffers introduced with IsrMst() resp for transfers to SCI
    25/07/07    pfr     ported for FGC3 boot (no asm)
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>           // basic typedefs
#include <iodefines.h>          // specific processor registers and constants
#include <stdio.h>              // for NULL
#include <codes.h>              // for code global variable
#include <definfo.h>
#include <dev.h>                // for dev global variable
#include <diag_boot.h>          // for diag_bus global variable
#include <ethernet_boot.h>
#include <main.h>               // for CheckStatus(), timeout_ms global variable
#include <memmap_mcu.h>         // Include memory map consts
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <sleep.h>
#include <term_boot.h>
#include <term.h>
#include <initterm.h>

/*---------------------------------------------------------------------------------------------------------*/
enum FGC_term_status EnableTerminalStream(void)
/*---------------------------------------------------------------------------------------------------------*\
  This is called to set up the SCI interface and reset the terminal.

  For FGC2 it also inits QSPI bus
\*---------------------------------------------------------------------------------------------------------*/
{
    // Initialise FILE structures for 'term' stream

    term.f = fopen("term", "wb");

    if(term.f == NULL)
    {
        return TERM_FOPEN_FAILURE;
    }

    // Set buffering mode - not buffered, buffer size 0

    if(setvbuf(term.f, NULL, _IONBF, 0) != 0)
    {
        return TERM_SETVBUF_FAILURE;
    }

    return TERM_SUCCESS;
}
/*---------------------------------------------------------------------------------------------------------*/
void TermRxCh(uint8_t ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from the Mst or fieldbus interrupt functions to queue a received terminal character.
  The handling depends upon the code.rx_mode:

  If code.rx_mode is SERIAL, then the character is a byte of code and it must be stored in the correct
  place in the next code block buffer.

  If code.rx_mode is not SERIAL, then the character is treated as a keyboard character.  The function
  detects if Ctrl-C is pressed and sets the abort flag if it is.  It also detects Xon/Xoff and sets or
  clears the xoff timeout.  This is used to block character transmission in TermProcessHardware().  If the buffer
  is full then the new character is discarded.

  This function is called with XK:IX=0x00000 so it must not use IX.  This requires it to be written in
  assembler.  It is also quicker which is important because it is running at interrupt level.
\*---------------------------------------------------------------------------------------------------------*/
{

    if (code.rx.mode != CODES_RX_TERMINAL)         // Keyboard character
    {
        if ((ch == 0) || (ch > 127))                    // Ignore null/8-bit characters
        {
            return;
        }

        if (ch == '\3')                         // CTRL-C = Abort
        {
            dev.abort_f = true;
            return;
        }
        else
        {
            if (ch == 19)                       // CTRL-S = XOFF
            {
                term.xoff_timeout = TERM_XOFF_TIMEOUT_MS;
            }
            else
            {
                if (ch == 17)                   // CTRL-Q = XON
                {
                    term.xoff_timeout = 0;
                }
                else
                {
                    if (term.rx.n_ch < TERM_RX_BUF_SIZE)
                    {
                        term.rx.buf[term.rx.in++] = ch;

                        if (term.rx.in == TERM_RX_BUF_SIZE)
                        {
                            term.rx.in = 0;
                        }

                        term.rx.n_ch++;
                    }
                }
            }
        }
    }
    else    // Code download mode
    {
        static uint16_t rx_term_idx;    // Terminal reception index within block (0-35)

        if(code.rx.term_stream_timeout_ms != 0)
        {
            code.rx.term_stream_timeout_ms = 2000;
        }

        struct code_block * const block = CodesRxBufferGetNextFreeBlock();

        if(block == NULL)
        {
            return;
        }

        // Add new char to next free code block

        *((uint8_t *)block + rx_term_idx++) = ch;

        // If end of the block

        if(rx_term_idx == sizeof(struct code_block))
        {
            rx_term_idx = 0;

            CodesRxBufferMakeNextAvailable();
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TermRxFlush(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will flush the character reception buffer while checking for CTRl-Z.  If at least one is
  included, it returns 1.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;
    uint16_t      ctrl_z_f = 0;


    OS_ENTER_CRITICAL();                                // Disable interrupts, except timers that are level 7

    while (term.rx.n_ch != 0)                           // For all characters waiting in the buffer
    {
        if (term.rx.buf[term.rx.out++] == 0x1A)         // If character is CTRL-Z
        {
            ctrl_z_f = 1;                               // Set the ctrl-Z flag
        }

        if (term.rx.out == TERM_RX_BUF_SIZE)
        {
            term.rx.out = 0;
        }

        term.rx.n_ch--;                                 // Decrement the buffer occupancy counter
    }

    OS_EXIT_CRITICAL();                                 // Enable interrupts

    return (ctrl_z_f);
}
/*---------------------------------------------------------------------------------------------------------*/
uint8_t TermGetCh(uint16_t to_ms)
/*---------------------------------------------------------------------------------------------------------*\
  This function will wait for the specified timeout period for a keyboard character from the TermIsr or
  fbsIsr functions.  If the timeout expires, it will return zero, otherwise it returns the received
  character forced to lower case.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;
    uint8_t       ch = 0;                         // Initialise character to zero in case of timeout


    // ATTENTION timeout_ms is a global variable decremented in the 1ms tick
    code.rx.timeout_ms = to_ms;

    while (((code.rx.timeout_ms != 0) || (to_ms == 0))          // Wait for timeout to expire if set or
           && (term.rx.n_ch == 0)                       // for new keyboard character
          )
    {
        CheckStatus();                          // Poll status variables
        USLEEP(1000);                           // Sleep for 1ms
    }

    OS_ENTER_CRITICAL();                        // Disable interrupts, except timers that are level 7

    if (term.rx.n_ch != 0)                      // If character received
    {
        ch = term.rx.buf[term.rx.out++];        // Get character from the rx buffer
        term.rx.n_ch--;                         // Decrement the buffer occupancy counter

        if (term.rx.out == TERM_RX_BUF_SIZE)
        {
            term.rx.out = 0;
        }
    }

    OS_EXIT_CRITICAL();                         // Enable interrupts

    if ((ch >= 'A') && (ch <= 'Z'))             // If upper case character
    {
        ch |= 0x20;                             // force to lower case
    }

    return (ch);                                // Return character or zero if timeout occurred
}
/*---------------------------------------------------------------------------------------------------------*/
void TermPutc(char ch, FILE * f)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the callback for the terminal output stream.  It will add a carriage return before every
  newline.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (ch == '\n')
    {
        TermTxCh('\r');
    }

    TermTxCh(ch);
}
/*---------------------------------------------------------------------------------------------------------*/
void TermTxCh(uint8_t ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the callback for the terminal output stream.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR       cpu_sr;


    while (term.tx.n_ch == TERM_TX_BUF_SIZE);              // Wait for queue to not be full

    OS_ENTER_CRITICAL();                                // Disable interrupts, except timers that are level 7

    term.tx.buf[term.tx.in++] = ch;                     // Insert character in the queue

    if (term.tx.in == TERM_RX_BUF_SIZE)
    {
        term.tx.in = 0;
    }

    term.tx.n_ch++;                                     // Increment character counter

    OS_EXIT_CRITICAL();                                 // Enable interrupts
}
/*---------------------------------------------------------------------------------------------------------*/
void TermProcessHardware(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called every millisecond from the IsrMst() function to process Rx and Tx characters from
  the SCI.
  FGC2 : When called, XK:IX = 0x00000 to access HC16 registers.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;
    uint8_t  ch;

    // Chars to transmit

    if (SCI0.SSR.BIT.ORER == 1) // if overrun error, clear error flag
    {
        SCI0.SSR.BIT.ORER = 0;
    }

    if (SCI0.SSR.BIT.FER == 1) // if frame error, clear error flag
    {
        SCI0.SSR.BIT.FER = 0;
    }

    if (SCI0.SSR.BIT.PER == 1)  // if parity error, clear error flag
    {
        SCI0.SSR.BIT.PER = 0;
    }

    // TDRE Transmit Data Register Empty
    //   0: Transmit register is not empty.
    //   1: Transmit register is empty, data has been transfered to shift register.

    if (SCI0.SSR.BIT.TDRE == 1)                 // No data in transmit buffer
    {
        if (term.tx.n_ch != 0)                  // there are chars to send
        {
            if (term.xoff_timeout != 0)         // if XOFF is active
            {
                term.xoff_timeout--;
            }
            else
            {
                OS_ENTER_CRITICAL();            // Disable interrupts, except timers that are level 7

                ch = term.tx.buf[term.tx.out++];
                // as TERM_BUF_SIZE is 256 and term.tx.out is uint8_t the overflow goes correctly to 00
                SCI0.TDR = ch;                  // write char in transmit buffer
                term.tx.n_ch--;

                OS_EXIT_CRITICAL();             // Enable interrupts

                EthTxCh(ch);
            }
        }
    }

    // there is no "end of reception flag" in the uart registers
    // we can scan the SCI0 RXI interrupt flag or use directly the interrupt

    // SCI0_RXI0 - vector 215 - INT_Excep_SCI0_RXI0()

    // Receive char
    //  ICU.IR215.BIT.IR = 0;                       // Clear the interrupt request flag
    if (ICU.IR215.BIT.IR == 1)                  // interrupt generated? (equivalent to char received)
    {
        ICU.IR215.BIT.IR = 0;                   // Clear the interrupt request flag
        ch = SCI0.RDR;                          // Flush buffer
        TermRxCh(ch);                            // Store the new character in the rx buffer
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sci_boot.c
\*---------------------------------------------------------------------------------------------------------*/
