/*!
 *  @file   memTestMcu.c
 *  @brief  Function for MCU memory testing
 */


// Includes

#include <stdlib.h>
#include <string.h>

#include <memTest.h>
#include <memTestMcu.h>
#include <menu.h>
#include <term.h>



// Internal variable definitions

uint32_t __attribute__((section("SRAM_test"))) data_backup[MEM_TEST_MCU_MAX_BLOCK_SIZE/sizeof(uint32_t)];



// External function definitions

int memTestMcu(volatile uint32_t * base_address, uint32_t num_bytes)
{
    uint32_t   data_bus_result;
    uint32_t * address_bus_result;
    uint32_t * device_result = NULL;
    
    volatile uint32_t * test_address;


    // Data bus test

    data_bus_result = memTestDataBus(base_address, data_backup);

    if (data_bus_result != 0)
    {
        MenuRspError("Data bus test failed.");
        return (-1);
    }


    // Address bus test

    address_bus_result = memTestAddressBus(base_address, num_bytes, data_backup);

    if (address_bus_result != NULL)
    {
        MenuRspError("Address bus test failed at address 0x%08lX.", (uint32_t) address_bus_result);
        return (-1);
    }


    // Device test

    // Test memory blocks of 1KB at a time

    test_address = base_address;

    while(num_bytes > MEM_TEST_MCU_MAX_BLOCK_SIZE && device_result == NULL)
    {
        device_result  = memTestDevice(test_address, MEM_TEST_MCU_MAX_BLOCK_SIZE, data_backup);
        test_address  += (MEM_TEST_MCU_MAX_BLOCK_SIZE/sizeof(uint32_t));

        if (num_bytes > MEM_TEST_MCU_MAX_BLOCK_SIZE)
        {
            num_bytes -= MEM_TEST_MCU_MAX_BLOCK_SIZE;
        }
    }

    if (num_bytes != 0 && device_result == NULL)
    {
        device_result = memTestDevice(test_address, num_bytes, data_backup);
    }

    if (device_result != NULL)
    {
        MenuRspError("Device test failed at address 0x%08lX.", (uint32_t) device_result);
        return (-1);
    }

    return (0);
}

// EOF
