/*---------------------------------------------------------------------------------------------------------*\
 File:      diag_boot.c

 Purpose:   QSPI related functions

\*---------------------------------------------------------------------------------------------------------*/

#define DIAG_BOOT_GLOBALS       // for diag_bus, diag_ctrl_lbl[]

#include <diag_boot.h>
#include <string.h>
#include <defconst.h>
#include <fieldbus_boot.h>      // For CLASS_FIELD
#include <macros.h>             // for Test(), Set()
#include <time_fgc.h>


/*---------------------------------------------------------------------------------------------------------*/
void QSPIbusStateMachine(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called from the 1 millisecond Tick interrupt.
  It must quickly read any data waiting from a previous scan, and trigger the next scan if required.

  The reset process for the diag uses 3 states because there needs to be a delay between the end
  of the reset pulse (clock line high) and the start of the scan.

  reset process
  a) DIAG_RESET = clock high
  b) DIAG_START = clock low
  c) DIAG_SCAN  = trigger
  d) DIAG_RUN   = readout + next trigger

  for next DIM register only
     DIAG_RUN = readout + next trigger


  the readout data will be stored in
    FGC2  reception_buffer   [2][16]
    FGC3  reception_buffer[2][7][16]  // [A,B][registers] for [16 DIM cards]
        the 7 registers are stored in this order
        0 - Dig0    DIM register ID:0
        1 - Dig1    DIM register ID:1
        2 - Ana0    DIM register ID:4
        3 - Ana1    DIM register ID:5
        4 - Ana2    DIM register ID:6
        5 - Ana3    DIM register ID:7
        6 - TrigCounter DIM register ID:2
        there is no ID:3
\*---------------------------------------------------------------------------------------------------------*/
{
    static uint16_t   register_id_b;
    uint8_t           ii;
    static uint16_t   register_id_a;


    // ToDo: can this be moved to diag_boot.h as const instead of static ?
    // the DIM register arrive in this order, first, register with ID:0 then ID:1, ID:4, ID:5, ID:6, ID:7 and last ID:2
    static uint16_t rx_order_to_register_id[] = { 0, 1, 4, 5, 6, 7, 2 };
    // this is used to verify data consistency

    switch (diag_bus.state)
    {
        case DIAG_RESET:                        // 1st RESET state
            // Set QSPI_CLK high, start of RESET signal
            // when QSPI_CLK goes HIGH the DIM starts checking the line, if QSPI_CLK remains HIGH at least 15.7us
            // this is recognised as a RESET cmd by the DIM card
            // (for branches A & B simultaneously)
            QSM_CTRL_P |= (QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16);

            // fill the transmission buffer with a know pattern so later we can determine the
            // number of DIMs in the chain
            // ToDo: implement this detection in DiagCheckBus()

            for (ii = 0; ii < 16; ii++)
            {
                // Can never be returned by a DIM (DIM register ID:3 doesn't exist)
                ((volatile uint16_t *) QSM_TRA_32)[ii] = 0xBAB0 + ii;
                ((volatile uint16_t *) QSM_TRB_32)[ii] = 0xBAB0 + ii;
            }
            
            diag_bus.state = DIAG_START;        // DIAG_RESET state is done
            break;

        case DIAG_START:                        // 2nd RESET state
            // more than 15.7us has passed with QSPI_CLK in HIGH, so the DIM board already done the RESET cmd
            // we can pass to the low state for the LOAD cmd

            // when QSPI_CLK goes low the DIM starts checking the line, if QSPI_CLK remains low at least 15.7us
            // this is recognised as a LOAD cmd, so the next DIM register gets ready to by transmitted with the next
            // 256 clk burst
            // (for branches A & B simultaneously)
            QSM_CTRL_P &= ~(QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16);

            diag_bus.qspi_branch = 0;           // 0=A, 1=B
            diag_bus.scan_idx = 0;  // this is the ordinal of DIM internal register we are theoretically reading
            diag_bus.state    = DIAG_SCAN;      // DIAG_START state is done            
           
            break;

        case DIAG_SCAN:                         // 3rd RESET state
            // at least 15.7us must passed with QSPI_CLK low after the last read (256 clk burst) for the LOAD cmd
            // to be recognised, so this acquire with this new 256 clk burst the next DIM register

            // Set READA/READB bit to fire the state machine which generates the 256 QSPI_CLK to shift
            // the active register in the 16 DIM boards chain
            // (for branches A & B simultaneously)
            QSM_CTRL_P |= (QSM_CTRL_READA_MASK16 | QSM_CTRL_READB_MASK16);

            diag_bus.state = DIAG_RUN;          // 1st register read is done
            break;

        case DIAG_RUN:                      // Read and restart scan
            // we assume that the buffer is filled by a previous READ action
            // so we save the buffer and trigg a new READ action
            //      diag_bus.scan_idx is the ordinal of DIM internal register we are theoretically reading
            // ToDo: why not to use register_id_a/b instead of this scan sequence (reception order) ?

            // copy the data from the buffer even if it is garbage
            // because we are in the BOOT (not the MAIN) and we want any diagnostic info available

            // copy the received data from the 16 DIM boards to the buffer
            memcpy(&diag_bus.reception_buffer[0][diag_bus.scan_idx], (void *) QSM_RRA_32, 16 * sizeof(uint16_t));
            memcpy(&diag_bus.reception_buffer[1][diag_bus.scan_idx], (void *) QSM_RRB_32, 16 * sizeof(uint16_t));
            
            // fprintf(term.f, "Run %d\n", diag_bus.scan_idx);

            // check the consistency of the data

            // as I don't know the number of DIMs in the chain I only test the first one

            // extract register ID number from top nibble, only done for DIM board 0 in both branches
            register_id_a = (diag_bus.reception_buffer[0][diag_bus.scan_idx][0] >> 12) & 0x7;
            // ToDo: decouple the resets of branches A and B, as B most of the time is not connected
            // so forcing resets of the branch A
            //          register_id_b = (diag_bus.reception_buffer[1][diag_bus.scan_idx][0] >> 12) & 0x7;
            register_id_b = register_id_a;


            // If the register ID we read and the one we expect don't match (and no loop-back in use)
            if (
                ((register_id_a != rx_order_to_register_id[diag_bus.scan_idx]) && !(QSM_CTRL_P & QSM_CTRL_LOOPA_MASK16))
                && ((register_id_b != rx_order_to_register_id[diag_bus.scan_idx]) && !(QSM_CTRL_P & QSM_CTRL_LOOPB_MASK16))
            )
            {
                diag_bus.check_f = 1;           // Flag that the state should be checked

                diag_bus.n_resets++; // this can overflow, as MenuMTqspiShow() forces to pass here without putting diag_bus.n_resets = 0

                if (diag_bus.n_resets >= DIAG_MAX_RESETS)
                {
                    diag_bus.state = DIAG_FAILED;
                    //                  diag_bus.n_resets = DIAG_MAX_RESETS; // if we want to avoid the overflow
                }
                else
                {
                    diag_bus.state = DIAG_RESET;
                }

                break; // exit the switch()
            }

            // Check end of scan

            if ((register_id_a == 2)           // theoretically register_id = 2 = TrigCounter is the last read
                || (register_id_b == 2)
                || (diag_bus.scan_idx == 6)   // 7th read request to the DIM board?
               )
            {
                diag_bus.scan_idx = 0;  // RESET will start from register 0
                diag_bus.check_f  = 1;  // Flag that new data is available
                diag_bus.state = DIAG_OFF;
            }
            else
            {
                diag_bus.scan_idx++;    // is the ordinal of DIM internal register we are theoretically reading
            }

            // at least 15.7us must passed with QSPI_CLK low after the last read (256 clk burst) for the LOAD cmd
            // to be recognised, so this acquire with this new 256 clk burst the next DIM register

            // Set READA/READB bit to fire the state machine which generates the 256 QSPI_CLK to shift
            // the active register in the 16 DIM boards chain
            QSM_CTRL_P |= (QSM_CTRL_READA_MASK16 | QSM_CTRL_READB_MASK16);  // fire state machine to start next read
            break;

        case DIAG_FAILED:
            // continue fill the buffer with the garbage we receive, just to investigate

            // copy the received data from the 16 DIM boards to the buffer
            memcpy(&diag_bus.reception_buffer[0][diag_bus.scan_idx], (void *) QSM_RRA_32, 16 * sizeof(uint16_t));
            memcpy(&diag_bus.reception_buffer[1][diag_bus.scan_idx], (void *) QSM_RRB_32, 16 * sizeof(uint16_t));

            // at least 15.7us must passed with QSPI_CLK low after the last read (256 clk burst) for the LOAD cmd
            // to be recognised, so this acquire with this new 256 clk burst the next DIM register

            // Set READA/READB bit to fire the state machine which generates the 256 QSPI_CLK to shift
            // the active register in the 16 DIM boards chain
            QSM_CTRL_P |= (QSM_CTRL_READA_MASK16 | QSM_CTRL_READB_MASK16);  // fire state machine to start next read

            diag_bus.scan_idx++;

            if (diag_bus.scan_idx > 6)
            {
                diag_bus.scan_idx = 0;
            }

            break;

        case DIAG_OFF:
            break;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called from CheckStatus(), which is called when the boot is waiting for keyboard
  characters.  It will process the new DIPS-DIM data - analogue and digital.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (diag_bus.state)
    {
        case DIAG_FAILED:
            stat_var.fgc_stat.class_data.CLASS_FIELD.st_warnings |= FGC_WRN_FGC_HW;
            stat_var.fgc_stat.class_data.CLASS_FIELD.st_latched |= FGC_LAT_DIM_SYNC_FLT;
            break;

        case DIAG_RUN:
            if (timeGetUtcTime() != diag_bus.ana_check_time)       // Limit checking rate to 1 Hz
            {
                DiagCheckBus(); // calculate the number of DIMs in the chain of each branch
                DiagCheckAna();
                diag_bus.ana_check_time = timeGetUtcTime();         // Remember unix time for this check
            }

            break;
    }

    diag_bus.check_f = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagCheckBus(void)
/*---------------------------------------------------------------------------------------------------------*\
  calculate the number of DIMs in the chain of each branch

    FGC3  reception_buffer[2][7][16]  // [A,B][registers] for [16 DIM cards]
        the 7 registers are stored in this order
        0 - Dig0    DIM register ID:0
        1 - Dig1    DIM register ID:1
        2 - Ana0    DIM register ID:4
        3 - Ana1    DIM register ID:5
        4 - Ana2    DIM register ID:6
        5 - Ana3    DIM register ID:7
        6 - TrigCounter DIM register ID:2
        there is no ID:3

FGC2 note:
  This function will be called from DiagCheck() when new analogue data is ready to be processed.  This
  takes quite a bit of time because it uses floating point maths, so the function is only called at 1Hz.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      qspi_branch;    // 0=A, 1=B
    uint16_t      dim_board;
    uint16_t      number_of_boards_in_branch;
    uint16_t      dim_register;

    for (qspi_branch = 0; qspi_branch < 2; qspi_branch++)
    {
        number_of_boards_in_branch = 0;

        for (dim_board = 0; dim_board < 16; dim_board++)
        {
            // theorically the 7th read is TrigCounter
            dim_register = diag_bus.reception_buffer[qspi_branch][6][dim_board];

            // If register_id = 2 = TrigCounter, consider that the DIM board is present in the chain
            if (((dim_register >> 12) & 0x07) == 2)
            {
                number_of_boards_in_branch++;
            }
        }

        diag_bus.n_dims[qspi_branch] = number_of_boards_in_branch;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void DiagCheckAna(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called from DiagCheck() when new analogue data is ready to be processed.
  This takes quite a bit of time because it uses floating point maths, so the function is only called at 1Hz.

    FGC3  reception_buffer[2][7][16]  // [A,B][registers] for [16 DIM cards]
        the 7 registers are stored in this order
        0 - Dig0    DIM register ID:0
        1 - Dig1    DIM register ID:1
        2 - Ana0    DIM register ID:4
        3 - Ana1    DIM register ID:5
        4 - Ana2    DIM register ID:6
        5 - Ana3    DIM register ID:7
        6 - TrigCounter DIM register ID:2
        there is no ID:3

  FGC2:
  The DIPS board has an embedded DIM that is the first on both branch A and branch B.
  The analogue channels are different for the two scans (there are 8 channels in all)
  and are used to measure the voltage source output voltage (Vmeas) twice (x1 and x10)
  and the +5V and +/-15V PSU voltages for the FGC and the FGC's analogue interface.

\*---------------------------------------------------------------------------------------------------------*/
{
    float    r5p;
    float    r15p;
    float    r15n;

    // Calculate Vmeas
    // [B][Ana0] DIM board 0
    diag_bus.ana[DIAG_VMEAS1]  = 5.117E-3 * (float)(0x800 - (int16_t)(diag_bus.reception_buffer[1][2][0] & 0xFFF));
    // [A][Ana1] DIM board 0
    diag_bus.ana[DIAG_VMEAS10] = 5.117E-4 * (float)(0x800 - (int16_t)(diag_bus.reception_buffer[0][3][0] & 0xFFF));

    // Calculate FGC PSU voltage measurements

    r5p  = (float)(diag_bus.reception_buffer[0][4][0] & 0xFFF); // [A][Ana2] DIM board 0
    r15p = (float)(diag_bus.reception_buffer[0][2][0] & 0xFFF); // [A][Ana0] DIM board 0
    r15n = (float)(diag_bus.reception_buffer[1][3][0] & 0xFFF); // [B][Ana1] DIM board 0

    diag_bus.ana[DIAG_PSU5]   = 2.4414E-3 * r5p;
    diag_bus.ana[DIAG_PSU15P] = 7.3120E-3 * r15p;
    diag_bus.ana[DIAG_PSU15N] = 2.9282E-3 * r15n - 1.02280E-2 * r15p;

    // Calculate Analogue interface voltage measurements

    r5p  = (float)(diag_bus.reception_buffer[0][5][0] & 0xFFF); // [A][Ana3] DIM board 0
    r15p = (float)(diag_bus.reception_buffer[1][4][0] & 0xFFF); // [B][Ana2] DIM board 0
    r15n = (float)(diag_bus.reception_buffer[1][5][0] & 0xFFF); // [B][Ana3] DIM board 0

    diag_bus.ana[DIAG_ANA5]   = 2.5659E-3 * r5p;
    diag_bus.ana[DIAG_ANA15P] = 7.5860E-3 * r15p + 2.4952E-5 * r15n;
    diag_bus.ana[DIAG_ANA15N] = 2.9532E-3 * r15n - 1.0377E-2 * r15p;
}

void DiagInit()
{
    // Start running diag buses, only if DIMs are expected on this crate
    if (dev.crate_type != FGC_CRATE_TYPE_RECEPTION && dev.crate_type != FGC_CRATE_TYPE_COMRCIAL_PC1)
    {
        //  the function QSPIbusStateMachine() will be called from the millisecond task at the start of every millisecond
        //  with the previous state it will do a reset and a read of the devices attached to QSPI bus
        diag_bus.state = DIAG_RESET;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: diag_boot.c
\*---------------------------------------------------------------------------------------------------------*/
