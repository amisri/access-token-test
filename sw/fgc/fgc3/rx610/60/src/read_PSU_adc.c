/*---------------------------------------------------------------------------------------------------------*\
 File:      read_PSU_adc.c

 Purpose:   FGC3

\*---------------------------------------------------------------------------------------------------------*/

#define READ_PSU_ADC_GLOBALS

#include <read_PSU_adc.h>
#include <iodefines.h>          // specific processor registers and constants
#include <stdint.h>
#include <memmap_mcu.h>         // Include memory map constants
#include <defconst.h>           // for FGC_PSU_MAX_15V
#include <definfo.h>            // for FGC_CLASS_ID
#if FGC_CLASS_ID == 60
#include <fieldbus_boot.h>  // for stat_var global variable
#else
#include <dpcom.h>          // for dpcom global variable
#include <fbs_class.h>      // for ST_LATCHED
#include <macros.h>         // for Set
#endif

/*---------------------------------------------------------------------------------------------------------*/
void Read_PSU_voltage(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will read the ADCs responsible to monitor the external power supplies +5, +15 and -15V.
\*---------------------------------------------------------------------------------------------------------*/
{
    psu.v5    = (10.560 * AD0.ADDRC) / 1023.0;
    psu.vp15  = (30.300 * AD0.ADDRD) / 1023.0;
    psu.vm15  = (7.333 * AD1.ADDRA) / 1023.0 - (11.0 * psu.vp15) / 9.0;

    // Check limits

    if ((psu.v5   <   FGC_PSU_MIN_5V)
        || (psu.v5   >   FGC_PSU_MAX_5V)
        || (psu.vp15 <   FGC_PSU_MIN_15V)
        || (psu.vp15 >   FGC_PSU_MAX_15V)
        || (psu.vm15 < (-FGC_PSU_MAX_15V))
        || (psu.vm15 > (-FGC_PSU_MIN_15V))
       )
    {
#if FGC_CLASS_ID == 60
        // Set PSU LED red
        LED_SET(PSU_RED);
        LED_RST(PSU_GREEN);
#else
        psu.fails_counter++;

        if (psu.fails_counter > 50)
        {
            Set(ST_LATCHED, FGC_LAT_PSU_V_FAIL);
        }

#endif

    }
    else
    {
#if FGC_CLASS_ID == 60
        // if power is ok, set PSU led green (if an error occurred it stays orange)
        LED_SET(PSU_GREEN);
#else
        psu.fails_counter = 0;
#endif
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: read_PSU_adc.c
\*---------------------------------------------------------------------------------------------------------*/
