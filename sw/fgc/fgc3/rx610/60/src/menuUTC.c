/*!
 *  @file   menuUTC.c
 *  @brief  Functions for the Module UTC boot menu in Manual Tests (option 3-9)
 */


// Includes

#include <stdint.h>

#include <time_fgc.h>
#include <dig.h>
#include <dev.h>
#include <memmap_mcu.h>
#include <menu.h>
#include <scivs.h>
#include <time_fgc.h>



// Internal function declarations

static void menuUTCsendPulse(uint32_t delay);



// External function definitions

void MenuUTCdigSupPulse(uint16_t argc, char ** argv)
{
    // Test access to register
    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    menuUTCsendPulse(0);
}



void MenuUTCsendScivsData(uint16_t argc, char ** argv)
{
    uint16_t slot;

    if (MenuGetInt16U(argv[0], 0, FGC_SCIVS_N_SLOT - 1, &slot)  != 0)
    {
        return;
    }

    scivs_tx->dw[1] = timeGetUtcTime();
    scivs_tx->dw[2] = timeGetUtcTimeUs();

    if (scivsTaskSend(SCIVS_TASK_UTC_TIME, slot, SCIVS_DEVICE_2, SCIVS_MASTER_TIMEOUT_MS) != SCIVS_OK)
    {
        return;
    }
}



void MenuUTCsendScivsDataAndPulse(uint16_t argc, char ** argv)
{
    uint16_t slot;
    uint32_t delay;

    if (MenuGetInt16U(argv[0], 0, FGC_SCIVS_N_SLOT - 1, &slot)  != 0)
    {
        return;
    }

    // Test access to register
    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Set data to next second

    scivs_tx->dw[1] = timeGetUtcTime() + 1;
    scivs_tx->dw[2] = 0;

    if (scivsTaskSend(SCIVS_TASK_UTC_TIME, slot, SCIVS_DEVICE_2, SCIVS_MASTER_TIMEOUT_MS) != SCIVS_OK)
    {
        return;
    }

    // Adjust delay of pulse so that it occurs on the next second

    delay = 1000000 - timeGetUtcTimeUs();

    menuUTCsendPulse(delay);
}



// Read SCIVS return data

void MenuUTCreadData(uint16_t argc, char ** argv)
{
    uint8_t     ii;

    for (ii = 0; ii < 4; ii++)
    {
        MenuRspArg("%u", (uint32_t)scivs_rx->regfgc3.data[ii]);
    }

    // UTC time error code

    MenuRspArg("%u", scivs_rx->w[10]);

    MenuRspArg("\n");

    for (ii = 2; ii < 11; ii++)
    {
        MenuRspArg("0x%04X", scivs_rx->w[ii]);
    }
}



// Internal function definitions

static void menuUTCsendPulse(uint32_t delay)
{
    uint32_t const width   = 4;
    uint16_t const channel = 3;

    TIME_TILL_EVT_US_P = delay;

    Set(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_PERMIT_MASK16);  // Output enabled
    Set(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_CTRL_MASK16);    // Write
    DigSupSetSourceBankA(channel, DIG_SUP_SOURCE_PULSE_GEN);

    // Reset arming to unlock acces to PULSE_ETIM_US and PULSE_WIDTH_US

    DIG_SUP_A_ARM_P = 0;

    DIG_SUP_A_PULSE_ETIM_US_A[channel]  = 0;
    DIG_SUP_A_PULSE_WIDTH_US_A[channel] = width;

    DIG_SUP_A_ARM_P = 0xF;
}