/*---------------------------------------------------------------------------------------------------------*\
  File:         menuSpy.c

  Purpose:      Functions for Spy functions based on DMA

  Author:       daniel.calcoen@cern.ch

  Notes:

  History:

    30 oct 08    doc    Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>           // basic typedefs
#include <menu.h>               // for MenuRspError()
#include <boot_dual_port_ram.h> // for dpram global variable
#include <boot_dsp_cmds.h>      // constants shared by DSP and CPU, for DSP_STS_, DSP_CMD_
#include <dsp_cmds.h>           // for DSP_SLOW_CMD_TIMEOUT_10MS, SendDspSlowCmdAndWaitForRsp
#include <stdio.h>              // for fprintf()
#include <dsp_loader.h>         // for dsp global variable
#include <dev.h>                // for dev global variable
#include <mcuDependent.h>
#include <sleep.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuSpySelectOwner(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  owner;

    if (MenuGetInt16U(argv[0], 0, 7 /*(SPY_OWNER_LAST-1)*/, &owner) != 0)
    {
        return;
    }

    if (dev.reg.dpram != 0)             // Test access to register
    {
        MenuRspError("No access");
        return;
    }

    if (dsp.is_running == FALSE)            // Check dsp is running
    {
        MenuRspError("DSP does not run");
        return;
    }

    // prepare argument for the command
    dpram->mcu.slowArg.data = owner;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPY_OWNER, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
    {
        fprintf(term.f, RSP_DATA",Failed by timeout\n");
        return;
    }

    fprintf(term.f, RSP_DATA",set OK\n");
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSpyOneShot(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  DSP_CMD_SLOW_GET_SPY_INFO
\*---------------------------------------------------------------------------------------------------------*/
{
    if (dev.reg.dpram != 0)             // Test access to register
    {
        MenuRspError("No access");
        return;
    }

    if (dsp.is_running == FALSE)            // Check dsp is running
    {
        MenuRspError("DSP does not run");
        return;
    }

    // ToDo: all 4 ADC reading AT THE SAME TIME the internal bus !!! mmmm! impedance? for signal level?

    // prepare arguments for the command
    dpram->mcu.slowArg.adc_path.external_signal_access     = 0x0F;  // ANA_SWIN, INs connected to ADCs
    dpram->mcu.slowArg.adc_path.adc_selector_bus_or_signal = 0x00;  // SW_BUS   bus disconnected

    // 0x00 = [0 0000], multiplexer disabled
    dpram->mcu.slowArg.adc_path.internal_signal_bus_access = 0x00;   // MPX disabled

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_ADC_PATH, DSP_SLOW_CMD_TIMEOUT_1S))
    {
        fprintf(term.f, RSP_DATA ",DSP rsp timeout\r");
        return;
    }

    // ---------------------------------------------------------------------------------
    // generate a pulse 0v,5v,0v on DAC 2 (in the DSP program it will fire also DAC 1)

    // prepare argument for the command
    dpram->mcu.slowArg.data = 0x33333;   // This value will trigger one spy acquisition

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SPY_ONE_SHOT, DSP_SLOW_CMD_TIMEOUT_1S))
    {
        fprintf(term.f, RSP_DATA ",DSP rsp timeout\r");
        return;
    }

    MSLEEP(1000);

    // prepare argument for the command
    dpram->mcu.slowArg.data = 0x00000;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SPY_ONE_SHOT, DSP_SLOW_CMD_TIMEOUT_1S))
    {
        fprintf(term.f, RSP_DATA ",DSP rsp timeout\r");
        return;
    }

    fprintf(term.f, RSP_DATA",OK\n");
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSpyTest(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  DSP_CMD_SLOW_GET_SPY_INFO
\*---------------------------------------------------------------------------------------------------------*/
{
    if (dev.reg.dpram != 0)             // Test access to register
    {
        MenuRspError("No access");
        return;
    }

    if (dsp.is_running == FALSE)            // Check dsp is running
    {
        MenuRspError("DSP does not run");
        return;
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SPY_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        fprintf(term.f, RSP_DATA ",DSP rsp timeout\r");
        return;
    }

    fprintf(term.f, ", 0x%08lX", dpram->dsp.slowRsp.data);
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuSpy.c
\*---------------------------------------------------------------------------------------------------------*/
