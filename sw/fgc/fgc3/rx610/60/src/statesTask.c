/*---------------------------------------------------------------------------------------------------------*\
  File:     statesTask.c

  Purpose:  FGC3 - State Functions.

  Author:   Quentin.King@cern.ch

  Notes:    This file contains the functions to support the Power Converter
        State Machine

\*---------------------------------------------------------------------------------------------------------*/

#define STATESTASK_GLOBALS      // for sta global variable

#include <statesTask.h>
#include <stdint.h>           // basic typedefs
#include <memmap_mcu.h>         // for DIG_OP_P
#include <state_class.h>
#include <transitions_class.h>  // for stateBehav global variable
#include <macros.h>             // for Test(), TestAll(), Set(), Clr()
#include <defconst.h>           // for FGC_FLT_, FGC_OP_
#include <iodefines.h>          // specific processor registers and constants
#include <fieldbus_boot.h>      // for stat_var global variable
#include <main.h>               // for main_misc global variable
#include <led.h>

/*---------------------------------------------------------------------------------------------------------*/
void StaTsk(void * unused)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the PC State manager task.  It is triggered by OSTskResume() in MstTsk() on millisecond
  2 and 7, provided the FGC is configured.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (stat_var.fgc_stat.class_data.c60.state_op != FGC_OP_UNCONFIGURED)   // If configured
    {
        StaOpState();               // Control operational state
        StaDdips();                 // Read or simulate direct digital i/o
        StaVsState();               // Evaluate voltage source state
        StaCalControl();            // Control calibration sequences
        StaAdcControl();            // Control ADC multiplexors
        StaCheck();                 // Run health checks
        StaLeds();                  // Set front panel LEDs
        StaPcState();               // Control Power Converter state
        StaCmdControl();            // Control direct command outputs
    }
    else                            // else unconfigured
    {
        //      main_misc.state_pc = FGC_PC_FLT_OFF;    // Force
        StaDdips();                 // Read or simulate direct digital i/o
        StaCheck();                 // Run health checks
        StaLeds();                  // Set front panel LEDs
    }

    sta.watchdog = 0;               // Flag that StaTsk has completed it's iteration
}
/*---------------------------------------------------------------------------------------------------------*/
void StaInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function initialises the interface and states related to the power converter.
\*---------------------------------------------------------------------------------------------------------*/
{
    P7.DR.BIT.B6 = 0;       // Enable digital CMD output

    stat_var.fgc_stat.class_data.c60.state_op = FGC_OP_TEST; // FGC_OP_UNCONFIGURED;
    main_misc.state_pc = FGC_PC_FLT_OFF; // Start PC state in FAULT OFF

    sta.flags   = 0;
    sta.cmd_req = 0;                    // Clear command request register

    stat_var.fgc_stat.class_data.c60.st_warnings    = 0;// Reset warnings
    stat_var.fgc_stat.class_data.c60.st_faults      = 0;// Reset latched faults

    sta.faults &= ~FGC_FLT_FGC_STATE;           // Clear FGC_STATE fault

    DIG_OP_P = DIG_OP_SET_FGCOKCMD_MASK16; // activate FGC OK command (pin Z20, FGC_OK)
}
/*---------------------------------------------------------------------------------------------------------*/
void StaOpState(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to control the operational state.
\*---------------------------------------------------------------------------------------------------------*/
{
}
/*---------------------------------------------------------------------------------------------------------*/
void StaDdips(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will read and/or simulate the direct I/O registers
\*---------------------------------------------------------------------------------------------------------*/
{
    sta.inputs = DIG_IP_P; // Read inputs
}
/*---------------------------------------------------------------------------------------------------------*/
void StaLeds(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will set some of the LEDs.
\*---------------------------------------------------------------------------------------------------------*/
{
    // --- PSU LED ---
    if (Test(stat_var.fgc_stat.class_data.c60.st_warnings, FGC_WRN_FGC_PSU))
    {
        // ORANGE
        LED_SET(PSU_RED);
        LED_SET(PSU_GREEN);
    }
    else
    {
        LED_RST(PSU_RED);
        LED_SET(PSU_GREEN);
    }

    // --- Voltage Source (VS) LED ---

    if (Test(sta.inputs, DIG_IP_VSPOWERON_MASK32))
    {
        LED_SET(VS_GREEN);      // 1: VS power stage is powered
    }
    else
    {
        LED_RST(VS_GREEN);      // 0: VS power stage is not powered
    }


    if (Test(sta.inputs, (DIG_IP_VSFAULT_MASK32 | DIG_IP_VSEXTINTLK_MASK32))
        || Test(stat_var.fgc_stat.class_data.c60.st_faults, FGC_FLT_VS_STATE)
       )
    {
        LED_SET(VS_RED);        // VS has an internal fault or VS has an external fault
    }
    else
    {
        LED_RST(VS_RED);        // VS has no internal fault and VS has no external fault
    }

    // --- Powering Interlock Controller (PIC) LED ---

    if (Test(stat_var.fgc_stat.class_data.c60.st_faults, FGC_FLT_FAST_ABORT))
    {
        LED_SET(PIC_RED);
    }
    else
    {
        LED_RST(PIC_RED);
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StaVsState(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to determine the voltage source state from five direct digital inputs using
  a lookup table.
\*---------------------------------------------------------------------------------------------------------*/
{
}
/*---------------------------------------------------------------------------------------------------------*/
void StaAdcControl(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will control the ADC analogue and digital multiplexors (if card is present)
\*---------------------------------------------------------------------------------------------------------*/
{
}
/*---------------------------------------------------------------------------------------------------------*/
void StaCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to check the operational health of the system.

  check the fault bits and set accordingly

\*---------------------------------------------------------------------------------------------------------*/
{
    // --- Faults - info available in IP1

    if (Test(sta.inputs, DIG_IP_VSNOCABLE_MASK32))
    {
        Set(sta.faults, FGC_FLT_FGC_HW); // 1: FGC3-VS cable is disconnected
    }
    else
    {
        Clr(sta.faults, FGC_FLT_FGC_HW); // 0: FGC3-VS cable is connected
    }

    //    DIG_IP_OPBLOCKED_MASK32
    //  0: output power stage is enabled
    //  1: output power stage is blocked

    /*
        if ( Test(sta.inputs, DIG_IP_VSRUN_MASK32) ) // if the VSRUN command is active
        {
        }
    */

    /*
        DIG_IP_PWRFAILURE_MASK32 == 1 active

        when: VSFAULT present | VSEXTINTLK present | (DCCTAFLT and DCCTBFLT both present)
           | (DIG_OP_RST_FGCOKCMD executed)

        equivalent to
        FGC_FLT_VS_FAULT | FGC_FLT_VS_EXTINTLOCK | FGC_FLT_I_MEAS | (read DIG_OP_SET_FGCOKCMD_MASK16 == 0)

    */
    if (Test(sta.inputs, DIG_IP_PCPERMIT_MASK32))
    {
        Clr(sta.faults, FGC_FLT_NO_PC_PERMIT);  // 1: run of power converter allowed
    }
    else
    {
        // 0: power converter must go OFF controlled by the FGC3 (Slow Power Abort)
        Set(sta.faults, FGC_FLT_NO_PC_PERMIT);
    }


    if (Test(sta.inputs, DIG_IP_FASTABORT_MASK32))
    {
        Set(sta.faults, FGC_FLT_FAST_ABORT); // 1: VS has received the PC_FAST_ABORT signal from PIC
    }
    else
    {
        Clr(sta.faults, FGC_FLT_FAST_ABORT); // 0: PC_FAST_ABORT signal (from PIC) was not received by the VS
    }

    //  check that if we are commanding PolNeg the status is POLSWINEG == 1
    //  check that if we are commanding PolPos the status is POLSWIPOS == 1
    //  check the timeout
    //  check that not POLSWINEG == 1 and POLSWIPOS == 1 at the same time
    //  set fault FGC_FLT_POL_SWITCH


    if (TestAll(sta.inputs, DIG_IP_DCCTAFLT_MASK32 | DIG_IP_DCCTBFLT_MASK32))         // If both bits are set
    {
        Set(sta.faults, FGC_FLT_I_MEAS); // 1: both channel read not OK
    }
    else
    {
        Clr(sta.faults, FGC_FLT_I_MEAS); // 0: at least 1 channel read OK
    }


    if (Test(sta.inputs, DIG_IP_VSEXTINTLK_MASK32))
    {
        Set(sta.faults, FGC_FLT_VS_EXTINTLOCK); // 1: VS has an external fault
    }
    else
    {
        Clr(sta.faults, FGC_FLT_VS_EXTINTLOCK); // 0: VS has no external fault
    }

    if (Test(sta.inputs, DIG_IP_VSFAULT_MASK32))
    {
        Set(sta.faults, FGC_FLT_VS_FAULT); // 1: VS has an internal fault
    }
    else
    {
        Clr(sta.faults, FGC_FLT_VS_FAULT); // 0: VS has no internal fault
    }

    /*
        DIG_IP_VSREADY_MASK32
        0: VS is not ready
        1: VS has finished its initialisation

    */
    /*
        if ( Test(sta.inputs, DIG_IP_VSPOWERON_MASK32) )
        {
          Set(, ); // 1: VS power stage is powered
        }
        else
        {
          Clr(, ); // 0: VS power stage is not powered
        }
    */

    // --- Latch new faults ---

    Set(stat_var.fgc_stat.class_data.c60.st_faults, sta.faults);
}
/*---------------------------------------------------------------------------------------------------------*/
void StaPcState(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to control the power converter state.  It uses transistion functions from
  pcstates.h and state functions in state.c.
\*---------------------------------------------------------------------------------------------------------*/
{
    const uint8_t          *          tt;     // transition path
    uint16_t                          xx;
    enum pc_state_machine_states    next_state;


    // --- Run PC state machine ---

    tt = stateBehav[main_misc.state_pc].lst;        // Get transition (bifurcation) list for current state
    xx = stateBehav[main_misc.state_pc].number_of_paths;

    // check if the conditions to do a transition (bifurcation) is meet
    while ((xx != 0) && (transitionBehav[*tt].check() == 0))
    {
        xx--;
        tt++;
    }

    if (xx != 0)                    // if active transition found
    {
        sta.time_ms = 0L;               // reset state time
        next_state = transitionBehav[*tt].end_state;    // get target state

        // the actual state (main_misc.state_pc) has not being changed yet
        // so in the following call that is the first time the new state will be played
        // the "actual" state is still the originator state
        stateBehav[next_state].play_state(TRUE);    // run next state function for the first time

        main_misc.state_pc = next_state;                // update state machine
    }
    else                        // else no transitions are active
    {
        // in this call the played state and the actual state are always the same
        stateBehav[main_misc.state_pc].play_state(FALSE);       // run current state function
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StaCalControl(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to run the auto calibration of ADCs, DCCTs and DAC.  The function
  CalRunSequence() is called when sta.cal_f is set, which is only every 200ms.

  The function also checks if an automatic calibration of the internal ADCs is required.
\*---------------------------------------------------------------------------------------------------------*/
{
}
/*---------------------------------------------------------------------------------------------------------*/
void StaCmdControl(void)
/*---------------------------------------------------------------------------------------------------------*\
  Class 51: This function will drive or simulate the direct command outputs.
\*---------------------------------------------------------------------------------------------------------*/
{
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t StaStartPC(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from SetPC() to start the voltage source if this is possible.  This will be in
  response to either S PC SB, S PC IL or S PC PP.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: statesTask.c
\*---------------------------------------------------------------------------------------------------------*/
