/*---------------------------------------------------------------------------------------------------------*\
  File:         transitions_class.c

  Contents:

  Notes:


\*---------------------------------------------------------------------------------------------------------*/

#define TRANSITIONS_CLASS_GLOBALS   // define pc_str[], pc_transitions[], trans_FO[], trans_OF[], trans_FS[], trans_SP[], trans_ST[], trans_SA[], trans_TS[], trans_SB[], trans_IL[], trans_TC[], trans_AR[], trans_RN[], trans_AB[], trans_CY[], pc_states[]

#include <transitions_class.h>
#include <state_class.h>
#include <macros.h>             // for Test(), Set()
#include <mcuDependent.h>      // for
#include <stdint.h>           // basic typedefs
#include <memmap_mcu.h>         // Include memory map consts
#include <defconst.h>           // for FGC_FLT_
#include <statesTask.h>         // for sta global variable
#include <fieldbus_boot.h>      // for stat_var global variable

/*---------------------------------------------------------------------------------------------------------*\
  These functions returns

   0    for conditions not meet, the transition can NOT take place
   != 0 if conditions meet, everything is OK to go to the new state

\*---------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------*/
uint16_t FOtoOF(void)
/*---------------------------------------------------------------------------------------------------------*\
  fault_off --> off

  actual state : FGC_PC_FLT_OFF
  desired state: FGC_PC_OFF

  the actual state means that the PC is OFF but 1 or more faults were memorized
  so a reset of fault is expected before doing this check

  the requirement is to not have any fault still active and have all the permits


  DIG_IP_PWRFAILURE_MASK32 == 1 active

  when:  VSFAULT present | VSEXTINTLK present | (DCCTAFLT and DCCTBFLT both present)
        | (DIG_OP_RST_FGCOKCMD executed)

  equivalent to
  FGC_FLT_VS_FAULT | FGC_FLT_VS_EXTINTLOCK | FGC_FLT_MEAS | (read DIG_OP_SET_FGCOKCMD_MASK16 == 0)


  is checking against FGC_FLT_FAST_ABORT and not against DIG_IP_FASTABORT_MASK32
  is checking against FGC_FLT_NO_PC_PERMIT and not against DIG_IP_PCPERMIT_MASK32

  ToDo: what about DIG_IP_VSNOCABLE_MASK32? is included in FGC OK?

\*---------------------------------------------------------------------------------------------------------*/
{
    return (
               !((sta.inputs & DIG_IP_PWRFAILURE_MASK32)
                 || Test(stat_var.fgc_stat.class_data.c60.st_faults, (FGC_FLT_FAST_ABORT | FGC_FLT_NO_PC_PERMIT))
                )
           );
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t OFtoFO(void)
/*---------------------------------------------------------------------------------------------------------*\
  off --> fault (off)

  actual state : FGC_PC_OFF
  desired state: FGC_PC_FLT_OFF

  if any fault is present or the permits are not there then go FAULT


  DIG_IP_PWRFAILURE_MASK32 == 1 active

  when:  VSFAULT present | VSEXTINTLK present | (DCCTAFLT and DCCTBFLT both present)
        | (DIG_OP_RST_FGCOKCMD ejecuted)

  equivalent to
  FGC_FLT_VS_FAULT | FGC_FLT_VS_EXTINTLOCK | FGC_FLT_MEAS | (read DIG_OP_SET_FGCOKCMD_MASK16 == 0)


  is checking against FGC_FLT_FAST_ABORT and not against DIG_IP_FASTABORT_MASK32
  is checking against FGC_FLT_NO_PC_PERMIT and not against DIG_IP_PCPERMIT_MASK32

  ToDo: what about DIG_IP_VSNOCABLE_MASK32? is included in FGC OK?

\*---------------------------------------------------------------------------------------------------------*/
{
    return ((sta.inputs & DIG_IP_PWRFAILURE_MASK32)
            || Test(stat_var.fgc_stat.class_data.c60.st_faults, (FGC_FLT_FAST_ABORT | FGC_FLT_NO_PC_PERMIT))
           );
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t OFtoST(void)
/*---------------------------------------------------------------------------------------------------------*\
  off --> starting

  actual state : FGC_PC_OFF
  desired state: FGC_PC_STARTING

  if they want to go to FGC_PC_ON_STANDBY   >cmd: s pc sb
  or
  if they want to go to FGC_PC_IDLE         >cmd: s pc il

  then leave FGC_PC_OFF and go forward 1 position to FGC_PC_STARTING

  a check with OFtoFO() is expected to be done just before this check
\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_START);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t FStoFO(void)
/*---------------------------------------------------------------------------------------------------------*\
  fault (stopping) --> fault (off)

  actual state : FGC_PC_FLT_STOPPING
  desired state: FGC_PC_FLT_OFF

  PC is going OFF in fast mode, the current is not following FGC reference

  if VS power stage is not powered the power converter is OFF, but if we have faults it is FAULT_OFF

    DIG_IP_VSPOWERON_MASK32
        0: VS power stage is not powered
        1: VS power stage is powered

\*---------------------------------------------------------------------------------------------------------*/
{
    return (!(sta.inputs & DIG_IP_VSPOWERON_MASK32));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t XXtoFS(void)
/*---------------------------------------------------------------------------------------------------------*\
  ??? --> fault (stopping)

  actual state : FGC_PC_STOPPING, FGC_PC_SLOW_ABORT, FGC_PC_TO_STANDBY, FGC_PC_ON_STANDBY, FGC_PC_CYCLING,
                 FGC_PC_ABORTING, FGC_PC_IDLE, FGC_PC_TO_CYCLING, FGC_PC_ARMED, FGC_PC_RUNNING
  desired state: FGC_PC_FLT_STOPPING


  with any of these condition the PC is going OFF in fast mode, the current is not following FGC reference
  so the FGC need to follow him


  DIG_IP_PWRFAILURE_MASK32 == 1 active

  when:  VSFAULT present or VSEXTINTLK present or (DCCTAFLT and DCCTBFLT both present)
         or (DIG_OP_RST_FGCOKCMD ejecuted)

  equivalent to
  FGC_FLT_VS_FAULT or FGC_FLT_VS_EXTINTLOCK or FGC_FLT_MEAS or (read DIG_OP_SET_FGCOKCMD_MASK16 == 0)

  DIG_IP_FASTABORT_MASK32
    0: PC_FAST_ABORT signal (from PIC) was not received by the VS
    1: VS has received the PC_FAST_ABORT signal from PIC

  is checking against FGC_FLT_NO_PC_PERMIT and not against DIG_IP_PCPERMIT_MASK32

  ToDo: what about ? is included in FGC OK?

\*---------------------------------------------------------------------------------------------------------*/
{
    return (
               Test(sta.inputs, (DIG_IP_VSNOCABLE_MASK32
                                 | DIG_IP_PWRFAILURE_MASK32
                                 | DIG_IP_FASTABORT_MASK32))
               || (stat_var.fgc_stat.class_data.c60.st_faults & FGC_FLT_NO_PC_PERMIT)
           );
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SPtoOF(void)
/*---------------------------------------------------------------------------------------------------------*\
  stopping --> off

  actual state : FGC_PC_STOPPING
  desired state: FGC_PC_OFF

  if VS power stage is not powered (the power converter is OFF) and you don't plan to continue
  FGC_PC_STOPPING state then you can go OFF

    DIG_IP_VSPOWERON_MASK32
        0: VS power stage is not powered
        1: VS power stage is powered

  a check with XXtoFS() is expected to be done just before this check, to check for the faults

\*---------------------------------------------------------------------------------------------------------*/
{
    return (!(sta.inputs & DIG_IP_VSPOWERON_MASK32));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t STtoFS(void)
/*---------------------------------------------------------------------------------------------------------*\
  starting --> fault (stopping)

  actual state : FGC_PC_STARTING
  desired state: FGC_PC_FLT_STOPPING

  if any fault is present or the permits are not there then go FAULT


  DIG_IP_PWRFAILURE_MASK32 == 1 active

  when:  VSFAULT present | VSEXTINTLK present | (DCCTAFLT and DCCTBFLT both present)
        | (DIG_OP_RST_FGCOKCMD executed)

  equivalent to
  FGC_FLT_VS_FAULT | FGC_FLT_VS_EXTINTLOCK | FGC_FLT_MEAS | (read DIG_OP_SET_FGCOKCMD_MASK16 == 0)


  is checking against FGC_FLT_FAST_ABORT and not against DIG_IP_FASTABORT_MASK32
  is checking against FGC_FLT_NO_PC_PERMIT and not against DIG_IP_PCPERMIT_MASK32

  for the moment this is identical to OFtoFO()

  ToDo: what about DIG_IP_VSNOCABLE_MASK32? is included in FGC OK?
\*---------------------------------------------------------------------------------------------------------*/
{
    return ((sta.inputs & DIG_IP_PWRFAILURE_MASK32)
            || Test(stat_var.fgc_stat.class_data.c60.st_faults, (FGC_FLT_FAST_ABORT | FGC_FLT_NO_PC_PERMIT))
           );
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t STtoSP(void)
/*---------------------------------------------------------------------------------------------------------*\
  starting --> stopping

  actual state : FGC_PC_STARTING
  desired state: FGC_PC_STOPPING

  a check with STtoFS() is expected to be done just before this check, to check for the faults

  DIG_IP_VSRUN_MASK8 is the readback of the DIG_OP_SET_VSRUNCMD
        0: VSRUN command is inactive
        1: VSRUN command is active


  if they want to go to FGC_PC_STOPPING     >cmd:s pc of ?
  or
  (FGC_PC_STARTING is not demanded any more and VSRUN command is inactive)


\*---------------------------------------------------------------------------------------------------------*/
{
    return (
               (sta.flags & STAF_STOP)
               || (!(sta.flags & STAF_START) && !(sta.inputs & DIG_IP_VSRUN_MASK32))
           );
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t STtoTS(void)
/*---------------------------------------------------------------------------------------------------------*\
  starting --> to_standBy

  actual state : FGC_PC_STARTING
  desired state: FGC_PC_TO_STANDBY

  a check with STtoFS() is expected to be done just before this check, to check for the faults
  a check with STtoSP() is expected to be done just before this check, to check for going OFF?

  if they don't go directly to IDLE

\*---------------------------------------------------------------------------------------------------------*/
{
    return (
               !(sta.flags & STAF_IDLE)
               && ((sta.inputs & DIG_IP_VSPOWERON_MASK32)
                   && (sta.inputs & DIG_IP_VSREADY_MASK32)
                  )
           );
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t STtoSB(void)
/*---------------------------------------------------------------------------------------------------------*\
  starting --> on_standby

  actual state : FGC_PC_STARTING
  desired state: FGC_PC_ON_STANDBY

  a check with STtoFS() is expected to be done just before this check, to check for the faults
  a check with STtoSP() is expected to be done just before this check, to check for ?
  a check with STtoTS() is expected to be done just before this check, to check for ?

    DIG_IP_VSPOWERON_MASK32
        0: VS power stage is not powered
        1: VS power stage is powered

    DIG_IP_VSREADY_MASK32
        0: VS is not ready
        1: VS has finished its initialisation

  if VS power stage is powered and initialisation finished

  DIG_IP_VSRUN_MASK8 is the readback of the DIG_OP_SET_VSRUNCMD
        0: VSRUN command is inactive
        1: VSRUN command is active

\*---------------------------------------------------------------------------------------------------------*/
{
    return ((sta.inputs & DIG_IP_VSPOWERON_MASK32)
            && Test(sta.inputs, DIG_IP_VSREADY_MASK32)
            && (sta.inputs & DIG_IP_VSRUN_MASK32)
           );
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t XXtoSP(void)
/*---------------------------------------------------------------------------------------------------------*\
  ??? --> stopping

  actual state : FGC_PC_SLOW_ABORT, FGC_PC_TO_STANDBY, FGC_PC_ON_STANDBY, FGC_PC_CYCLING, FGC_PC_ABORTING,
                 FGC_PC_IDLE, FGC_PC_TO_CYCLING, FGC_PC_RUNNING, FGC_PC_ARMED

  desired state: FGC_PC_STOPPING

  DIG_IP_VSRUN_MASK8 is the readback of the DIG_OP_SET_VSRUNCMD
        0: VSRUN command is inactive
        1: VSRUN command is active

  if they want to go to FGC_PC_STOPPING     >cmd:s pc of ?
  or
  VSRUN command is inactive

\*---------------------------------------------------------------------------------------------------------*/
{
    return ((sta.flags & STAF_STOP)
            || !(sta.inputs & DIG_IP_VSRUN_MASK32)
           );
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SAtoAB(void)
/*---------------------------------------------------------------------------------------------------------*\
  slow abort --> aborting

  actual state : FGC_PC_SLOW_ABORT
  desired state: FGC_PC_ABORTING

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_SLOW_ABORT);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t XXtoSA(void)
/*---------------------------------------------------------------------------------------------------------*\
  ??? --> slow abort

  actual state : FGC_PC_TO_STANDBY, FGC_PC_ON_STANDBY, FGC_PC_ABORTING, FGC_PC_IDLE, FGC_PC_RUNNING, FGC_PC_ARMED
  desired state: FGC_PC_SLOW_ABORT

  if SLOW ABORT is needed

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_SLOW_ABORT);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TStoAB(void)
/*---------------------------------------------------------------------------------------------------------*\
  to_standBy --> aborting

  actual state : FGC_PC_TO_STANDBY
  desired state: FGC_PC_ABORTING

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed


\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_STOP);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TStoSB(void)
/*---------------------------------------------------------------------------------------------------------*\
  to_standBy --> standBy

  actual state : FGC_PC_TO_STANDBY
  desired state: FGC_PC_ON_STANDBY

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed
  a check with TStoAB() is expected to be done just before this check, to check for ?


\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_IDLE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SBtoIL(void)
/*---------------------------------------------------------------------------------------------------------*\
  standBy --> idle

  actual state : FGC_PC_ON_STANDBY
  desired state: FGC_PC_IDLE

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed

  if they want to go to idle    >cmd: s pc il ?

    OUTPUT_BLOCK_STATE - DIG_IPDIRECT:b8 - DIG_IP_OPBLOCKED_MASK32
    (in the FGC3 expansion panel STATUS is labelled : OUTPUT BLOCKED)
        0: output power stage is enabled
        1: output power stage is blocked


\*---------------------------------------------------------------------------------------------------------*/
{
    return ((sta.flags & STAF_IDLE)
            && !(sta.inputs & DIG_IP_OPBLOCKED_MASK32));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SBtoTC(void)
/*---------------------------------------------------------------------------------------------------------*\
  on_standby --> to_cycling

  actual state : FGC_PC_ON_STANDBY
  desired state: FGC_PC_TO_CYCLING

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed
  a check with SBtoIL() is expected to be done just before this check, to check if they want IDLE

  if they want to go to PPM    >cmd: s pc pp ?

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_CYCLING);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t XXtoTS(void)
/*---------------------------------------------------------------------------------------------------------*\
  ??? --> to_standBy

  actual state : FGC_PC_CYCLING, FGC_PC_TO_CYCLING, FGC_PC_ABORTING, FGC_PC_IDLE, FGC_PC_RUNNING, FGC_PC_ARMED
  desired state: FGC_PC_TO_STANDBY

  if they want to go to standBy    >cmd: s pc sb ?

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_TO_STANDBY);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ILtoAR(void)
/*---------------------------------------------------------------------------------------------------------*\
  idle --> armed

  actual state : FGC_PC_IDLE
  desired state: FGC_PC_ARMED (DC value and function to readch this value prepared)

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed
  a check with XXtoTS() is expected to be done just before this check, to check for To_StandBy

  if new ref arrived armed is activated

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_ARMED);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ILtoTC(void)
/*---------------------------------------------------------------------------------------------------------*\
  idle --> to_cycling

  actual state : FGC_PC_IDLE
  desired state: FGC_PC_TO_CYCLING

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed
  a check with XXtoTS() is expected to be done just before this check, to check for To_StandBy
  a check with ILtoAR() is expected to be done just before this check, to check for ARMED

  if they want to go to PPM    >cmd: s pc pp ?

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_CYCLING);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t TCtoCY(void)
/*---------------------------------------------------------------------------------------------------------*\
  to_cycling --> cycling (running pulse to pulse modulation)

  actual state : FGC_PC_TO_CYCLING
  desired state: FGC_PC_CYCLING

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoTS() is expected to be done just before this check, to check for To_StandBy

  if they want to go to PPM    >cmd: s pc pp ?

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_CYCLING);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ARtoIL(void)
/*---------------------------------------------------------------------------------------------------------*\
  armed --> idle

  actual state : FGC_PC_ARMED
  desired state: FGC_PC_IDLE

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed
  a check with XXtoTS() is expected to be done just before this check, to check for To_StandBy

  if they want to go to idle    >cmd: s pc il ?

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_IDLE);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ARtoRN(void)
/*---------------------------------------------------------------------------------------------------------*\
  armed --> running

  actual state : FGC_PC_ARMED   (DC value and function to readch this value prepared)
  desired state: FGC_PC_RUNNING (deliver pure DC value to the load)

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed
  a check with XXtoTS() is expected to be done just before this check, to check for To_StandBy
  a check with ARtoIL() is expected to be done just before this check, to check if they want IDLE

  if they want running

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_RUNNING);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RNtoIL(void)
/*---------------------------------------------------------------------------------------------------------*\
  running --> idle

  actual state : FGC_PC_RUNNING
  desired state: FGC_PC_IDLE

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed
  a check with XXtoTS() is expected to be done just before this check, to check for To_StandBy

  if they want to go to idle    >cmd: s pc il ?
  or
  they don't want running

\*---------------------------------------------------------------------------------------------------------*/
{
    return ((sta.flags & STAF_IDLE) || !(sta.flags & STAF_RUNNING));
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t RNtoAB(void)
/*---------------------------------------------------------------------------------------------------------*\
  running --> aborting

  actual state : FGC_PC_RUNNING  (deliver pure DC value to the load)
  desired state: FGC_PC_ABORTING

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed
  a check with XXtoTS() is expected to be done just before this check, to check for To_StandBy
  a check with RNtoIL() is expected to be done just before this check, to check for ?

  if need to abort

\*---------------------------------------------------------------------------------------------------------*/
{
    return (sta.flags & STAF_ABORTING);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t ABtoIL(void)
/*---------------------------------------------------------------------------------------------------------*\
  aborting --> idle

  actual state : FGC_PC_ABORTING
  desired state: FGC_PC_IDLE

  a check with XXtoFS() is expected to be done just before this check, to check for the faults
  a check with XXtoSP() is expected to be done just before this check, to check for ?
  a check with XXtoSA() is expected to be done just before this check, to check if SLOW ABORT is needed
  a check with XXtoTS() is expected to be done just before this check, to check for To_StandBy

  if they want to go to idle    >cmd: s pc il ?
  or
  not aborting

\*---------------------------------------------------------------------------------------------------------*/
{
    return ((sta.flags & STAF_IDLE) || !(sta.flags & STAF_ABORTING));
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: transitions_class.c
\*---------------------------------------------------------------------------------------------------------*/
