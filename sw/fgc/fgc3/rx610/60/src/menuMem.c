/*---------------------------------------------------------------------------------------------------------*\
  File:         menuMem.c

  Purpose:      Memory test functions

  Notes:        These menues belongs to the [Manual Test] part and [Self Test] part

\*---------------------------------------------------------------------------------------------------------*/

#include <bitmap.h>
#include <boot_dsp_cmds.h>
#include <boot_dual_port_ram.h>
#include <stdint.h>           // basic typedefs
#include <codes_holder.h>
#include <derived_clocks.h>     // for StopDspClock()
#include <dev.h>                // for dev global variable
#include <dsp_cmds.h>
#include <dsp_loader.h>         // for dsp global variable
#include <iodefines.h>          // specific processor registers and constants
#include <mcuDependent.h>      // for NVRAM_UNLOCK(), NVRAM_LOCK()
#include <mcu_flash.h>
#include <memTestMcu.h>
#include <mem.h>                // for MemSetWords()
#include <memmap_mcu.h>         // Include memory map consts
#include <menu.h>               // for MenuGetInt32U(), MenuGetInt16U(), MenuRspError()
#include <stdio.h>              // for fprintf()
#include <string.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuSTsram(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function tests SRAM.
\*---------------------------------------------------------------------------------------------------------*/
{
    //  Test access to register

    if (dev.reg.sram != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Skip the fisrt KB of SRAM as this is used for data backup during memory tests

    uint32_t * address = ((uint32_t *) SRAM_TMPCODEBUF_32) + (MEM_TEST_MCU_MAX_BLOCK_SIZE/sizeof(uint32_t));
    uint32_t   size    = SRAM_TMPCODEBUF_B - MEM_TEST_MCU_MAX_BLOCK_SIZE;

    memTestMcu(address, size);

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTsram(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  This function writes and reads back the Sram memory. Writes each location with data equal to its address
  (in the page) plus the page offset (for ex. location 0x130100 data: 0x103 = 0x100 + 0x13 - 0x10).
\*---------------------------------------------------------------------------------------------------------*/
{
    // Skip the fisrt KB of SRAM as this is used for data backup during memory tests

    uint32_t * address = ((uint32_t *) SRAM_TMPCODEBUF_32) + (MEM_TEST_MCU_MAX_BLOCK_SIZE/sizeof(uint32_t));
    uint32_t   size    = SRAM_TMPCODEBUF_B - MEM_TEST_MCU_MAX_BLOCK_SIZE;

    memTestMcu(address, size);

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTsramSeq(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This function writes and reads back the Sram memory. Writes memory by 64 word chunks.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Same as MenuMTsram. To be deleted.

    // Skip the fisrt KB of SRAM as this is used for data backup during memory tests

    uint32_t * address = ((uint32_t *) SRAM_TMPCODEBUF_32) + (MEM_TEST_MCU_MAX_BLOCK_SIZE/sizeof(uint32_t));
    uint32_t   size    = SRAM_TMPCODEBUF_B - MEM_TEST_MCU_MAX_BLOCK_SIZE;

    memTestMcu(address, size);

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTmram(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function tests Mram.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Test access to register

    if (dev.reg.nvram != 0)
    {
        MenuRspError("No access");
        return;
    }

    NVRAM_UNLOCK();

    memTestMcu((uint32_t *) NVRAM_32, NVRAM_W);

    NVRAM_LOCK();

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTnvRam(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This function writes and read back the NVRAM memory.
\*---------------------------------------------------------------------------------------------------------*/
{
    NVRAM_UNLOCK();

    memTestMcu((uint32_t *) NVRAM_32, NVRAM_W);

    NVRAM_LOCK();

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTnvRamLock(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This function protects NVRAM against writing.
\*---------------------------------------------------------------------------------------------------------*/
{
    NVRAM_LOCK();
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTnvRamUnlock(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This function enables NVRAM writing.
\*---------------------------------------------------------------------------------------------------------*/
{
    NVRAM_UNLOCK();
    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTnvRamSeq(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This function writes and read back the NVRAM memory. Writes memory by 64 word chunks.
\*---------------------------------------------------------------------------------------------------------*/
{
    // This function should be deleted at some point
    // It is now the same as MenuMTnvRam

    NVRAM_UNLOCK();

    memTestMcu((uint32_t *) NVRAM_32, NVRAM_W);

    NVRAM_LOCK();

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTnvRamEraseIndex(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] NVram zeros the property index
\*---------------------------------------------------------------------------------------------------------*/
{
    NVRAM_UNLOCK();

    MemSetWords((void *) NVSIDX_32, 0x0000, NVSIDX_W);

    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTnvRamEraseData(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] NVram zeros the property data
\*---------------------------------------------------------------------------------------------------------*/
{
    NVRAM_UNLOCK();

    MemSetWords((void *) NVSDATA_32, 0x0000, NVSDATA_W);

    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTnvRamEraseAll(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] NVram zeros all the NVRAM
\*---------------------------------------------------------------------------------------------------------*/
{
    NVRAM_UNLOCK();

    MemSetWords((void *)NVRAM_32, 0x0000, NVRAM_W);

    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuST_PLD_DpRam(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function tests PLD DpRam.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Test access to register

    if (dev.reg.dpram != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Make sure DSP is in reset
    // this will stop also, DAC and ADC as both are derived from DSP NMI Tick
    StopDspClock(); // Stops DSP tick (DSP NMI interrupt generation by the PLD)

    // DSP reset
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_DSP_MASK16;
    dsp.is_running = FALSE;     // remove tms320c6727 run flag

    memTestMcu((uint32_t *) DPRAM_START_32, DPRAM_B);

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMT_PLD_DpRam(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This function writes and reads back the PLD DpRam memory.
\*---------------------------------------------------------------------------------------------------------*/
{
    memTestMcu((uint32_t *) DPRAM_START_32, DPRAM_B);

    return;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMT_MCU_FlashBlockErase(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test, Memory Test, MCU Flash] This function erases 1 Block
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t block_addr_index;
    uint32_t address;

    if(argc != 1 || MenuGetInt16U(argv[0], 0, MCU_NUM_FLASH_BLOCKS - 1, &block_addr_index) != 0)
    {
        return;
    }

    address = MCU_BLOCK_ADDRESS[block_addr_index];

    enum FGC_flash_status status = MCU_rom_BlockErase(&address);

    if(status != FLASH_STATUS_SUCCESS)
    {
        fprintf(term.f, "$2,Flash status 0x%04X\r\n", status);
        fprintf(term.f, "$2,FMR0 0x%02X - FMR1 0x%02X\r\n", flashFunc_fmr0, flashFunc_fmr1);
    }
}

void MenuSTdspSdram(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function tests the SDRAM acces from the DSP.
\*---------------------------------------------------------------------------------------------------------*/
{

    // Test access to register

    if (dev.reg.dpram != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Check dsp is running

    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    // 5s timeout for 16MB test, 20s timeout for 64MB SDRAM test
    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_TST_SDRAM, DSP_SLOW_CMD_TIMEOUT_20S) != 0)
    {
        MenuRspError("DSP rsp timeout");
    }
    else
    {
        if (testBitmap(dpram->dsp.status, DSP_STS_MEM_DATA_FAULT) == true)
        {
            MenuRspError("Data bus test failed.");
        }
        else if (testBitmap(dpram->dsp.status, DSP_STS_MEM_ADDRESS_FAULT) == true)
        {
            MenuRspError("Address bus test failed at address 0x%08lX.", (uint32_t) dpram->dsp.slowRsp.data);
        }
        else if (testBitmap(dpram->dsp.status, DSP_STS_MEM_DEVICE_FAULT) == true)
        {
            MenuRspError("Device test failed at address 0x%08lX.", (uint32_t) dpram->dsp.slowRsp.data);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspSdram(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Tests DSP SDRAM
\*---------------------------------------------------------------------------------------------------------*/
{
    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_TST_SDRAM, DSP_SLOW_CMD_TIMEOUT_20S) != 0)
    {
        MenuRspError("DSP rsp timeout");
    }
    else
    {
        if (testBitmap(dpram->dsp.status, DSP_STS_MEM_DATA_FAULT) == true)
        {
            MenuRspError("Data bus test failed.");
        }
        else if (testBitmap(dpram->dsp.status, DSP_STS_MEM_ADDRESS_FAULT) == true)
        {
            MenuRspError("Address bus test failed at address 0x%08lX.", (uint32_t) dpram->dsp.slowRsp.data);
        }
        else if (testBitmap(dpram->dsp.status, DSP_STS_MEM_DEVICE_FAULT) == true)
        {
            MenuRspError("Device test failed at address 0x%08lX.", (uint32_t) dpram->dsp.slowRsp.data);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTdspDpram(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function tests the DPRAM access from the DSP.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Test access to register

    if (dev.reg.dpram != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Check dsp is running

    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    // Test

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_TST_PLD_DPRAM, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout");
    }
    else
    {
        if (testBitmap(dpram->dsp.status, DSP_STS_MEM_DATA_FAULT) == true)
        {
            MenuRspError("Data bus test failed.");
        }
        else if (testBitmap(dpram->dsp.status, DSP_STS_MEM_ADDRESS_FAULT) == true)
        {
            MenuRspError("Address bus test failed at address 0x%08lX.", (uint32_t) dpram->dsp.slowRsp.data);
        }
        else if (testBitmap(dpram->dsp.status, DSP_STS_MEM_DEVICE_FAULT) == true)
        {
            MenuRspError("Device test failed at address 0x%08lX.", (uint32_t) dpram->dsp.slowRsp.data);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTdspDpram(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Tests DSP DPRAM
\*---------------------------------------------------------------------------------------------------------*/
{
    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_TST_PLD_DPRAM, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout");
    }
    else
    {
        if (testBitmap(dpram->dsp.status, DSP_STS_MEM_DATA_FAULT) == true)
        {
            MenuRspError("Data bus test failed.");
        }
        else if (testBitmap(dpram->dsp.status, DSP_STS_MEM_ADDRESS_FAULT) == true)
        {
            MenuRspError("Address bus test failed at address 0x%08lX.", (uint32_t) dpram->dsp.slowRsp.data);
        }
        else if (testBitmap(dpram->dsp.status, DSP_STS_MEM_DEVICE_FAULT) == true)
        {
            MenuRspError("Device test failed at address 0x%08lX.", (uint32_t) dpram->dsp.slowRsp.data);
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuMem.c
\*---------------------------------------------------------------------------------------------------------*/
