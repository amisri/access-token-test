/*---------------------------------------------------------------------------------------------------------*\
 File:      interrupt_handlers.c

 Purpose:   60 - Boot - RX610 ISRs - Interrupt Service Routines

\*---------------------------------------------------------------------------------------------------------*/

#include <interrupt_handlers.h>

#include <stdint.h>
#include <iodefines.h>
#include <stdio.h>

#include <ana_temp_regul.h>
#include <boot_dsp_cmds.h>      // for DSP_CMD_FAST_SIGNAL_MEASURE
#include <boot_dual_port_ram.h> // for dpram global variable
#include <codes.h>              // for code global variable
#include <defconst.h>
#include <dev.h>                // for dev global variable
#include <diag_boot.h>          // for QSPIbusStateMachine()
#include <dig.h>                // for DIG_SUP_B_CHANNEL2, DigSupTstRiseEdge(), DigSupGetRiseEtim()
#include <dsp_loader.h>         // for dsp global variable
#include <ethernet_boot.h>      // for Lan9215WriteStatVar()
#include <fieldbus_boot.h>      // for fieldbus, stat_var global variable, FieldbusISR()
#include <led.h>
#include <macros.h>             // for Test(), Set()
#include <main.h>               // for global variables timeout_ms, main_misc
#include <master_clk_pll.h>
#include <mcuDependent.h>      // for REFRESH_SLOWWATCHDOG()
#include <mcu_panic.h>
#include <memmap_mcu.h>         // For RGLEDS_
#include <menutree.h>
#include <pld.h>                // for pld global variable
#include <pll.h>                // for pll global variable
#include <read_PSU_adc.h>       // for Read_PSU_voltage()
#include <renesas_bl.h>         // for gRenesasLoader global variable
#include <scivs.h>
#include <sharedMemory.h>      // for shared_mem global variable
#include <statesTask.h>         // for StaTsk()
#include <term_boot.h>
#include <time_fgc.h>

/*---------------------------------------------------------------------------------------------------------*/
static void ProcessMsTimeSlice(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function manages the MSTIME count and LEDs.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      ms_number;

    // check state machine every 16 ms
    if (main_misc.state_machine_is_running == TRUE)
    {
        main_misc.state_machine_slice_mask = main_misc.state_machine_slice_mask << 1;

        if (main_misc.state_machine_slice_mask == 0)
        {
            main_misc.state_machine_slice_mask = 1;
            StaTsk(NULL);
        }
    }

    // If the UTC register is running we use it, we generate ms otherwise
    // Todo Review this with new boot sequence, launching PLD after updating code.

    if (pld_info.ok && fieldbus.is_gateway_online && (timeGetUtcTime() != 0) && (timeGetUtcTimeMs() < 1000))
    {
        main_misc.local_ms = timeGetUtcTimeMs();
    }
    else
    {
        main_misc.local_ms++;                  // increment ms
    }

    ms_number = main_misc.local_ms % 20;

    LedManage(main_misc.local_ms);

    if (main_misc.local_ms == 500)          // check if 500
    {
        if (dev.reg.leds_rg == 1)           // if write & read access to LED regs
        {
            // Clear all LEDs except WORLDFIP and PSU

            RGLEDS_P &= 0x0033;
            BLEDS_P  &= 0x0000;
        }
    }
    else
    {
        // the value 1000 can only be reached when we are offline (not connected to the network)
        // because the network when receive time_var.ms it overwrites shared_mem.local_ms and never with 1000
        // ToDo we want to do this online too!!
        if (main_misc.local_ms == 1000)        // end of second
        {
            main_misc.local_ms = 0;    // reset millisecond
            shared_mem.power_time++;
            shared_mem.run_time++;

            // LEDs management
            if ((stat_var.fgc_stat.class_data.c60.st_faults & FGC_FLT_FGC_HW) != 0)     // HW FAULT
            {
                LED_SET(FGC_RED);
            }
            else        // no hw fault
            {
                LED_SET(FGC_GREEN);

                if (stat_var.fgc_stat.class_data.c60.st_warnings != 0)  // Warnings
                {
                    LED_SET(FGC_RED);       // Set FGC led red + green = orange
                }
            }
        }
    }

    if ((main_misc.local_ms == 10) && (AnaTempGetStatus()))          // Analogue interface, temperature regulation
    {
        AnaTempRegul();
    }

    if (main_misc.local_ms == 100)
    {
        Read_PSU_voltage();     // PSU Power Check
    }

    if (fieldbus.is_gateway_online && ms_number == 2)
    {
        EthWriteStatVar();
    }

    if (ms_number == 0)
    {
        eth_isr.timeVar_processed = FALSE;
    }

    if (ms_number == 5)
    {
        MasterClockAnaloguePllIteration();
    }

    if(main_misc.dig_sup_test)
    {
        if(main_misc.dig_sup_test_counter == 0)
        {
            // TODO Add a function for that?

            DIG_SUP_A_SET_P = 0x0001;
        }
        else if(main_misc.dig_sup_test_counter == main_misc.dig_sup_test_duty)
        {
            DIG_SUP_A_SET_P = (0x01 << 8) | 0x00;
        }

        if(++main_misc.dig_sup_test_counter == main_misc.dig_sup_test_duty * 2)
        {
            main_misc.dig_sup_test_counter = 0;
        }
    }

    if (main_misc.is_sci_test_continuous)
    {
        scivsTaskSend(scivs_tx->w[0], SCIVS_M_SLAVE_SLOT_P, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);
    }
}



static void msIsr(void)
{
    if (dev.allow_slow_watchdog_refresh)
    {
        REFRESH_SLOWWATCHDOG();     // Trigger slow watchdog SWD
    }

    if (dev.memory_test_in_progress == FALSE)
    {
        ProcessMsTimeSlice();   // Ms time management if no memory tests
    }

    // ATTENTION timeout_ms is a global variable decremented in the 1ms tick
    // and used by TermGetCh() and by CodesUpdateGetBlock()
    if(code.rx.timeout_ms != 0)
    {
        code.rx.timeout_ms--;
    }

    if(code.rx.term_stream_timeout_ms != 0)
    {
        code.rx.term_stream_timeout_ms--;
    }

    TermProcessHardware();
    QSPIbusStateMachine(); // QSPI bus, this executes the desired command in diag_bus.state, DIAG_RESET, DIAG_START, DIAG_SCAN, DIAG_RUN

    FieldbusCheckGatewayAlive();
}



/*=========================================================================================================*/
// FIXED VECTORS
/*=========================================================================================================*/
/*---------------------------------------------------------------------------------------------------------*/
//void PowerON_Reset(void)
/*---------------------------------------------------------------------------------------------------------*\
 RESET - Fixed vector
 declared in reset_program.S
\*---------------------------------------------------------------------------------------------------------*/
//{
//   __asm__ volatile ( "rte" );    // return from interrupt, if __attribute__ ((naked)) is used
//}
/*---------------------------------------------------------------------------------------------------------*/
/*=========================================================================================================*/
// RELOCATABLE VECTORS
/*=========================================================================================================*/
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ2(void) // not naked
/*---------------------------------------------------------------------------------------------------------*\
    network packet reception irq

    IRQ2 - vector 66 - FIP_~MSG_IRQ (in case of FIP board this an IRQ3 - vector 67 - FIP_~TIME_IRQ are generated simultaneously)
                       or Ethernet MSG IRQ

    ICU.IER08.BIT.IEN2      Interrupt Request Enable Register
    ICU.IPR22.BIT.IPR_20    Interrupt Priority Register
    ICU.IR066.BIT.IR        Interrupt request register 066 (status)
    ISELR066

  when FIP board

  The source of this interrupt is the MicroFIP interface IRQ.
  Only the Var 7 reception has an interrupt enabled, so there can be no other reason for the interrupt.
  Var 7 is used twice per WorldFIP cycle
    - first for the time variable (0x3000)
    and then
    - for the code variable (0x3004).
  When the time variable is received it first reads the time variable and then writes the status variable.
  It then runs the phase locked loop to maintain the phase between the millisecond interrupts and the
  FIP interrupts.
  If code reception from the FIP is enabled, the FipReadTimeVar function will set the Var 7 global
  variable identifier to the 0x3004 to receive the code variable
  which will return the identifier to 0x3000 for the time variable on the next cycle.


  when ETHERNET board

  The source of this interrupt is packet reception in the LAN chip

\*---------------------------------------------------------------------------------------------------------*/
{
    pll.irqs_count_all++;

    EthISR();
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ3(void)
/*---------------------------------------------------------------------------------------------------------*\
    network time synchronisation irq

    IRQ3 - vector 67 - FIP_~TIME_IRQ (in case of FIP board this an IRQ2 - vector 66 - FIP_~MSG_IRQ are generated simultaneously)
                       or synchronisation pulse irq (when Ethernet board)

    ICU.IER08.BIT.IEN3          Interrupt Request Enable Register
    ICU.IPR23.BIT.IPR_20        Interrupt Priority Register
    ICU.IR067.BIT.IR            Interrupt request register 067 (status)
    ISELR067

  when FIP board

  The source of this interrupt is the MicroFIP interface IRQ.
  Only the Var 7 reception has an interrupt enabled, so there can be no other reason for the interrupt.
  Var 7 is used twice per WorldFIP cycle
    - first for the time variable (0x3000)
    and then
    - for the code variable (0x3004).
  When the time variable is received it first reads the time variable and then writes the status variable.
  It then runs the phase locked loop to maintain the phase between the millisecond interrupts and the
  FIP interrupts.
  If code reception from the FIP is enabled, the FipReadTimeVar function will set the Var 7 global
  variable identifier to the 0x3004 to receive the code variable
  which will return the identifier to 0x3000 for the time variable on the next cycle.


  when ETHERNET board

  The source of this interrupt is the 50Hz pulse from the timing system in the gateway

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ4(void) // not naked
/*---------------------------------------------------------------------------------------------------------*\
    IRQ4 - vector 68 - MCU_~TICK - "hardware" 1ms Tick

    ICU.IER08.BIT.IEN4      Interrupt Request Enable Register
    ICU.IPR24.BIT.IPR_20    Interrupt Priority Register
    ICU.IR068.BIT.IR        Interrupt request register 068 (status)
    ISELR068

    1ms Tick used to schedule the Boot Program
    this hardware input comes from the Xilinx Spartan 3AN FPGA (XC3S 700AN - 4FG484C),
    it is the output of the digital PLL implemented in it

    (this interrupt must be similar to INT_Excep_TMR0_CMIA0() - vector 174, that is the backup of this
     interrupt, 1ms generated internally by TMR0)
\*---------------------------------------------------------------------------------------------------------*/
{
    msIsr();
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TGI0A(void)  // not naked
/*---------------------------------------------------------------------------------------------------------*\
    TPU0 TGI0A - vector 104

    ICU.IER0D.BIT.IEN0      Interrupt Request Enable Register
    ICU.IPR4C.BIT.IPR_20    Interrupt Priority Register
    ICU.IR104.BIT.IR        Interrupt request register 104 (status)
    ISELR104

 fired by TPU0.TGRA input capture/compare match
\*---------------------------------------------------------------------------------------------------------*/
{
    main_misc.timer_1ms++;
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR0_CMIA0(void) // not naked
/*---------------------------------------------------------------------------------------------------------*\
    CMIA0 - Compare Match A Interrupt - vector 174 - "software" 1ms Tick

    ICU.IER15.BIT.IEN6      Interrupt Request Enable Register
    ICU.IPR68.BIT.IPR_20    Interrupt Priority Register
    ICU.IR174.BIT.IR        Interrupt request register 174 (status)
    ISELR174

    TMR0 is used as a 16 bit counter incremented by PCLK 50MHz counting 50000 = 1ms

    it is programmed to generate an interrupt every 1ms

    (backup) internally generated 1ms Tick used to schedule the Boot Program
    (so this one must be similar to INT_Excep_IRQ4() - vector 68 - MCU_~TICK)

    It is used when the Xilinx Spartan 3AN FPGA (XC3S 700AN - 4FG484C) is unprogrammed and no tick is present.

\*---------------------------------------------------------------------------------------------------------*/
{
    msIsr();
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR3_OVI3(void) // not naked
/*---------------------------------------------------------------------------------------------------------*\
    OVI3 - Timer 3 Overflow Interrupt - vector 185

    TMR3.TCNT               overflow INT enable
    ICU.IER17.BIT.IEN1      Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20    Interrupt Priority Register
    ICU.IR185.BIT.IR        Interrupt request register 185 (status)

 TMR3 will count [0..255] incremented by TMR2 count match (1us)
 and generating an interrupt when overflows

 TMR2 will count [0..49] increment by PCLK (50MHz,20ns), the period is 1us

 we count the TMR3 overflows to enhance USLEEP()

 This interrupt is set to level 7, and never blocked

\*---------------------------------------------------------------------------------------------------------*/
{
    main_misc.timer_255us++;
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI6_RXI6(void) // not naked
/*---------------------------------------------------------------------------------------------------------*\
- vector 239 (edge)
    ICU.IER1D.BIT.IEN7      Interrupt Request Enable Register
    ICU.IPR86.BIT.IPR_20    Interrupt Priority Register
    ICU.IR239.BIT.IR        Interrupt request register 239 (status)

 ISELR239
 UART6
\*---------------------------------------------------------------------------------------------------------*/
{
    gRenesasLoader.buffer[gRenesasLoader.len++] = SCI6.RDR; // Get data

    if (gRenesasLoader.len > 4)
    {
        gRenesasLoader.len = 4;
    }
}
/*=========================================================================================================*/
// Not used interrupts
/*=========================================================================================================*/
/*=========================================================================================================*/
// FIXED VECTORS
/*=========================================================================================================*/
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SupervisorInst(void)
/*---------------------------------------------------------------------------------------------------------*\
 Exception(Supervisor Instruction) - Fixed vector
\*---------------------------------------------------------------------------------------------------------*/
{

    McuPanic(MCU_PANIC_EXCEP_SUPERVISOR_INSTR,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_UndefinedInst(void)
/*---------------------------------------------------------------------------------------------------------*\
 Exception(Undefined Instruction) - Fixed vector
\*---------------------------------------------------------------------------------------------------------*/
{

    McuPanic(MCU_PANIC_EXCEP_INVALID_INSTR,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_FloatingPoint(void)
/*---------------------------------------------------------------------------------------------------------*\
 Exception(Floating Point) - Fixed vector
\*---------------------------------------------------------------------------------------------------------*/
{

    McuPanic(MCU_PANIC_EXCEP_FLOATING_POINT,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_NonMaskableInterrupt(void)
/*---------------------------------------------------------------------------------------------------------*\
 NMI  - Fixed vector
\*---------------------------------------------------------------------------------------------------------*/
{

    McuPanic(MCU_PANIC_EXCEP_NMI,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
/*=========================================================================================================*/
// RELOCATABLE VECTORS
/*=========================================================================================================*/
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_BRK(void)
/*---------------------------------------------------------------------------------------------------------*\
 vector  0 - BRK
\*---------------------------------------------------------------------------------------------------------*/
{

    McuPanic(MCU_PANIC_EXCEP_BRK,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void OSTskCtxSwitch(void)
/*---------------------------------------------------------------------------------------------------------*\
 vector  1 pure software, hardware reserved OSTskCtxSwitch()
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_BUSERR(void)
/*---------------------------------------------------------------------------------------------------------*\
 BUSERR
\*---------------------------------------------------------------------------------------------------------*/
{

    McuPanic(MCU_PANIC_EXCEP_BUS_ERROR,
             RGLEDS_FGC_RED_MASK16 | RGLEDS_VS_RED_MASK16,
             RGLEDS_NET_RED_MASK16 | RGLEDS_PSU_RED_MASK16);

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_FCU_FCUERR(void)
/*---------------------------------------------------------------------------------------------------------*\
  vector 21 - FCU_FCUERR
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_FCU_FRDYI(void)
/*---------------------------------------------------------------------------------------------------------*\
 FCU_FRDYI
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_CMTU0_CMT0(void)
/*---------------------------------------------------------------------------------------------------------*\
 CMTU0_CMT0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_CMTU0_CMT1(void)
/*---------------------------------------------------------------------------------------------------------*\
 CMTU0_CMT1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_CMTU1_CMT2(void)
/*---------------------------------------------------------------------------------------------------------*\
 CMTU1_CMT2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_CMTU1_CMT3(void)
/*---------------------------------------------------------------------------------------------------------*\
 CMTU1_CMT3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ0(void)
/*---------------------------------------------------------------------------------------------------------*\
 0x0100 int  64  IRQ0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ1(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ5(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ5
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ6(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ6
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ7(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ7
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ8(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ8
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ9(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ9
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ10(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ10
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ11(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ11
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ12(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ12
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ13(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ13
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ14(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ14
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_IRQ15(void)
/*---------------------------------------------------------------------------------------------------------*\
 IRQ15
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_WDT_WOVI(void)
/*---------------------------------------------------------------------------------------------------------*\
 WDT_WOVI
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_AD0_ADI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 AD0_ADI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_AD1_ADI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 AD1_ADI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_AD2_ADI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 AD2_ADI2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_AD3_ADI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 AD3_ADI3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TGI0B(void)
/*---------------------------------------------------------------------------------------------------------*\
 fired by TPU0.TGRB input capture/compare match
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TGI0C(void)
/*---------------------------------------------------------------------------------------------------------*\
 fired by TPU0.TGRC input capture/compare match
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TGI0D(void)
/*---------------------------------------------------------------------------------------------------------*\
 fired by TPU0.TGRD input capture/compare match
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU0_TCI0V(void)
/*---------------------------------------------------------------------------------------------------------*\
 fired by TPU0.TCNT overflow
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU1_TGI1A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU1_TGI1A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU1_TGI1B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU1_TGI1B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU1_TCI1V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU1_TCI1V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU1_TCI1U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU1_TCI1U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU2_TGI2A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU2_TGI2A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU2_TGI2B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU2_TGI2B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU2_TCI2V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU2_TCI2V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU2_TCI2U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU2_TCI2U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TGI3A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TGI3A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TGI3B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TGI3B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TGI3C(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TGI3C
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TGI3D(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TGI3D
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU3_TCI3V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU3_TCI3V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU4_TGI4A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU4_TGI4A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU4_TGI4B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU4_TGI4B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU4_TCI4V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU4_TCI4V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU4_TCI4U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU4_TCI4U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU5_TGI5A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU5_TGI5A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU5_TGI5B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU5_TGI5B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU5_TCI5V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU5_TCI5V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU5_TCI5U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU5_TCI5U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TGI6A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TGI6A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TGI6B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TGI6B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TGI6C(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TGI6C
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TGI6D(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TGI6D
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU6_TCI6V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU6_TCI6V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU7_TGI7A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU7_TGI7A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU7_TGI7B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU7_TGI7B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU7_TCI7V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU7_TCI7V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU7_TCI7U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU7_TCI7U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU8_TGI8A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU8_TGI8A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU8_TGI8B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU8_TGI8B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU8_TCI8V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU8_TCI8V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU8_TCI8U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU8_TCI8U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TGI9A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TGI9A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TGI9B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TGI9B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TGI9C(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TGI9C
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TGI9D(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TGI9D
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU9_TCI9V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU9_TCI9V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU10_TGI10A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU10_TGI10A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU10_TGI10B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU10_TGI10B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU10_TCI10V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU10_TCI10V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU10_TCI10U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU10_TCI10U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU11_TGI11A(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU11_TGI11A
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU11_TGI11B(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU11_TGI11B
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU11_TCI11V(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU11_TCI11V
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TPU11_TCI11U(void)
/*---------------------------------------------------------------------------------------------------------*\
 TPU11_TCI11U
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR0_CMIB0(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIB0 - Compare Match B Interrupt - vector 175

    ICU.IER15.IEN7
    ICU.IPR68.BIT.IPR_20        Interrupt Priority Register
    ICU.IR175.BIT.IR            Interrupt request register 175 (status)
    ISELR175
\*---------------------------------------------------------------------------------------------------------*/
{
    // ICU.IR175.BIT.IR = 0;        // Clear the interrupt request flag
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR0_OVI0(void)
/*---------------------------------------------------------------------------------------------------------*\
    OVI0 - Timer 0 Overflow Interrupt - vector 176

    TMR0.TCNT                   overflow INT enable
    ICU.IER16.BIT.IEN0          Interrupt Request Enable Register
    ICU.IPR68.BIT.IPR_20        Interrupt Priority Register
    ICU.IR176.BIT.IR            Interrupt request register 176 (status)
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR176.BIT.IR = 0;        // Clear the interrupt request flag
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR1_CMIA1(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIA1 - Compare Match A Interrupt - vector 177

    ICU.IER16.BIT.IEN1          Interrupt Request Enable Register
    ICU.IPR69.BIT.IPR_20        Interrupt Priority Register
    ICU.IR177.BIT.IR            Interrupt request register 177 (status)
    ISELR177
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR1_CMIB1(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIB1 - Compare Match B Interrupt - vector 178

    ICU.IER16.BIT.IEN2          Interrupt Request Enable Register
    ICU.IPR69.BIT.IPR_20        Interrupt Priority Register
    ICU.IR178.BIT.IR            Interrupt request register 178 (status)
    ISELR178
\*---------------------------------------------------------------------------------------------------------*/
{
    // ICU.IR178.BIT.IR = 0;        // Clear the interrupt request flag
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR1_OVI1(void)
/*---------------------------------------------------------------------------------------------------------*\
    OVI1 - Timer 1 Overflow Interrupt - vector 179

    TMR1.TCNT                   overflow INT enable
    ICU.IER16.BIT.IEN3          Interrupt Request Enable Register
    ICU.IPR69.BIT.IPR_20        Interrupt Priority Register
    ICU.IR179.BIT.IR            Interrupt request register 179 (status) 8870B3h 8
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR179.BIT.IR = 0;        // Clear the interrupt request flag
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR2_CMIA2(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIA2 - Compare Match A Interrupt - vector 180

    ICU.IER16.BIT.IEN4          Interrupt Request Enable Register
    ICU.IPR6A.BIT.IPR_20        Interrupt Priority Register
    ICU.IR180.BIT.IR            Interrupt request register 180 (status)
    ISELR180
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR2_CMIB2(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIB2 - Compare Match B Interrupt - vector 181

    ICU.IER16.BIT.IEN5          Interrupt Request Enable Register
    ICU.IPR6A.BIT.IPR_20        Interrupt Priority Register
    ICU.IR181.BIT.IR            Interrupt request register 181 (status)
    ISELR181
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR2_OVI2(void)
/*---------------------------------------------------------------------------------------------------------*\
    OVI2 - Timer 2 Overflow Interrupt - vector 182

    TMR2.TCNT                   overflow INT enable
    ICU.IER16.BIT.IEN6          Interrupt Request Enable Register
    ICU.IPR6A.BIT.IPR_20        Interrupt Priority Register
    ICU.IR182.BIT.IR            Interrupt request register 182 (status)
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR3_CMIA3(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIA3 - Compare Match A Interrupt - vector 183

    ICU.IER16.BIT.IEN7          Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20        Interrupt Priority Register
    ICU.IR183.BIT.IR            Interrupt request register 183 (status)
    ISELR183
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_TMR3_CMIB3(void)
/*---------------------------------------------------------------------------------------------------------*\
    CMIB3 - Compare Match B Interrupt - vector 184

    ICU.IER17.BIT.IEN0          Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20        Interrupt Priority Register
    ICU.IR184.BIT.IR            Interrupt request register 184 (status)
    ISELR184
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_DMAC_DMTEND0(void)
/*---------------------------------------------------------------------------------------------------------*\
 DMAC_DMTEND0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_DMAC_DMTEND1(void)
/*---------------------------------------------------------------------------------------------------------*\
 DMAC_DMTEND1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_DMAC_DMTEND2(void)
/*---------------------------------------------------------------------------------------------------------*\
 DMAC_DMTEND2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_DMAC_DMTEND3(void)
/*---------------------------------------------------------------------------------------------------------*\
 DMAC_DMTEND3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI0_ERI0(void)
/*---------------------------------------------------------------------------------------------------------*\
  - vector 214
   ICU.IER1A.BIT.IEN6           Interrupt Request Enable Register
   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
 UART0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI0_RXI0(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 215
   ICU.IER1A.IEN7
   ICU.IPR80.BIT.IPR_20 Interrupt Priority Register
 ISELR215
 UART0
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR215.BIT.IR = 0;        // Clear the interrupt request flag

    //  SCI0.RDR is the received character
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI0_TXI0(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 216
   ICU.IER1B.BIT.IEN0           Interrupt Request Enable Register
   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
 ISELR216
 UART0
\*---------------------------------------------------------------------------------------------------------*/
{
    //  SCI0.TDR = char to transmit;
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI0_TEI0(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 217
   ICU.IER1B.BIT.IEN1           Interrupt Request Enable Register
   ICU.IPR80.BIT.IPR_20         Interrupt Priority Register
 UART0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI1_ERI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI1_ERI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI1_RXI1(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 219
    ICU.IER1B.IEN3              Interrupt Request Enable Register
    ICU.IPR81.BIT.IPR_20        Interrupt Priority Register
    ICU.IR220.BIT.IR            Interrupt request register 219 (status)
 ISELR219
 UART1
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR219.BIT.IR = 0;        // Clear the interrupt request flag

    //  SCI1.RDR is the received character
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI1_TXI1(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 220
    ICU.IER1B.BIT.IEN4          Interrupt Request Enable Register
    ICU.IPR81.BIT.IPR_20        Interrupt Priority Register
    ICU.IR220.BIT.IR            Interrupt request register 220 (status)

 ISELR220
 UART1
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR220.BIT.IR = 0;        // Clear the interrupt request flag

    //  SCI0.TDR = char to transmit;
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI1_TEI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI1_TEI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI2_ERI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI2_ERI2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI2_RXI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI2_RXI2
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IRxxx.BIT.IR = 0;        // Clear the interrupt request flag

    //  SCI2.RDR is the received character

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI2_TXI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI2_TXI2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI2_TEI2(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI2_TEI2
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI3_ERI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI3_ERI3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI3_RXI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI3_RXI3
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IRxxx.BIT.IR = 0;        // Clear the interrupt request flag

    //  SCI3.RDR is the received character

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI3_TXI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI3_TXI3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI3_TEI3(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI3_TEI3
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI4_ERI4(void)
/*---------------------------------------------------------------------------------------------------------*\
  - vector 230
   ICU.IER1C.BIT.IEN6           Interrupt Request Enable Register
   ICU.IPR84.BIT.IPR_20         Interrupt Priority Register
 UART4
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI4_RXI4(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 231
   ICU.IER1C.IEN7
   ICU.IPR84.BIT.IPR_20 Interrupt Priority Register
 ISELR231
 UART4
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IR231.BIT.IR = 0;        // Clear the interrupt request flag

    //  SCI4.RDR is the received character

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI4_TXI4(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 232
   ICU.IER1D.BIT.IEN0           Interrupt Request Enable Register
   ICU.IPR84.BIT.IPR_20         Interrupt Priority Register
 ISELR232
 UART4
\*---------------------------------------------------------------------------------------------------------*/
{
    //  SCI4.TDR = char to transmit;

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI4_TEI4(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 233
   ICU.IER1D.BIT.IEN1           Interrupt Request Enable Register
   ICU.IPR84.BIT.IPR_20         Interrupt Priority Register
 UART4
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI5_ERI5(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI5_ERI5
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI5_RXI5(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI5_RXI5
\*---------------------------------------------------------------------------------------------------------*/
{
    // not needed, done automatically by the hardware
    // ICU.IRxxx.BIT.IR = 0;        // Clear the interrupt request flag

    //  SCI5.RDR is the received character

    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI5_TXI5(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI5_TXI5
- vector 236

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI5_TEI5(void)
/*---------------------------------------------------------------------------------------------------------*\
 SCI5_TEI5
- vector 237

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI6_ERI6(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 238 (level)
    ICU.IER1D.BIT.IEN6          Interrupt Request Enable Register
    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
    ICU.IR238.BIT.IR            Interrupt request register 238 (status)

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI6_TXI6(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 240 (edge)
    ICU.IER1E.BIT.IEN0          Interrupt Request Enable Register
    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
    ICU.IR240.BIT.IR            Interrupt request register 240 (status)

 ISELR240
 UART6
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_SCI6_TEI6(void)
/*---------------------------------------------------------------------------------------------------------*\
- vector 241 (level)
    ICU.IER1E.BIT.IEN1          Interrupt Request Enable Register
    ICU.IPR86.BIT.IPR_20        Interrupt Priority Register
    ICU.IR241.BIT.IR            Interrupt request register 241 (status)

\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC0_EEI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC0_EEI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC0_RXI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC0_RXI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC0_TXI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC0_TXI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC0_TEI0(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC0_TEI0
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC1_EEI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC1_EEI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC1_RXI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC1_RXI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC1_TXI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC1_TXI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*/
void INT_Excep_RIIC1_TEI1(void)
/*---------------------------------------------------------------------------------------------------------*\
 RIIC1_TEI1
\*---------------------------------------------------------------------------------------------------------*/
{
    __asm__ volatile("rte");        // return from interrupt, if __attribute__ ((naked)) is used
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: interrupt_handlers.c
\*---------------------------------------------------------------------------------------------------------*/
