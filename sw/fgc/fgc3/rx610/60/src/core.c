/*!
 *  @file     core.c
 *  @defgroup FGC3:MCU
 *  @brief    FGC Core dump related functions
 *
 *  Description: The core dump system allows up to FGC_N_CORE_SEGS segments of memory to be saved to
 *              NVRAM in the event of an unexpected trap (e.g. bus error or illegal instruction).
 *              A core control table in NVRAM defines the memory segments to be saved.
 */

#define CORE_GLOBALS            // define core global variable

#include <core.h>

#include <iodefines.h>
#include <mcuDependent.h>
#include <mem.h>
#include <memmap_mcu.h>
#include <string.h>


void CoreDump(void)
{
    uint16_t                ii;
    struct fgc_corectrl     corectrl;

    CoreTableGet(&corectrl);                // Get Core control table from NVRAM

    NVRAM_UNLOCK();

    for (ii = 0; ii < FGC_N_CORE_SEGS; ii++)        // Write each memory segment to NVRAM
    {
        if (corectrl.seg[ii].n_words)
        {
            memcpy((void *) corectrl.addr_in_core[ii], (void *) corectrl.seg[ii].addr,
                   corectrl.seg[ii].n_words * sizeof(uint16_t));
        }
    }

    NVRAM_LOCK();
}


void CoreTableGet(struct fgc_corectrl * corectrl)
{
    uint16_t    ii;
    uint32_t    total_length_words = 0;

    // Get CORE control table from NVRAM

    memcpy(corectrl, (void *) NVRAM_CORECTRL_32, sizeof(struct fgc_corectrl));

    for (ii = 0; ii < FGC_N_CORE_SEGS; ii++)            // Check that table has valid lengths
    {
        if ((corectrl->seg[ii].pad != 0xFFFF)
            || (corectrl->seg[ii].n_words
                && (corectrl->addr_in_core[ii] <  NVRAM_COREDATA_32
                    || corectrl->addr_in_core[ii] > (NVRAM_COREDATA_32 + 2 * NVRAM_COREDATA_W)
                   )
               )
           )
        {
            break;                  // Terminate loop
        }

        total_length_words += corectrl->seg[ii].n_words;
    }

    if (ii < FGC_N_CORE_SEGS ||               // If core ctrl table is not valid
        total_length_words > NVRAM_COREDATA_W)
    {
        memset(corectrl, 0, sizeof(struct fgc_corectrl));  // Clear table

        corectrl->seg[0].n_words  = NVRAM_COREDATA_W;   // Set up default table
        corectrl->seg[0].addr     = (uint32_t) &shared_mem;           // with one max length segment from
        corectrl->seg[1].addr     = (uint32_t) &shared_mem;           // start of SRAM in page 0
        corectrl->seg[2].addr     = (uint32_t) &shared_mem;
        corectrl->seg[3].addr     = (uint32_t) &shared_mem;
        corectrl->seg[0].pad      = 0xFFFF;             // Padding word
        corectrl->seg[1].pad      = 0xFFFF;             // Padding word
        corectrl->seg[2].pad      = 0xFFFF;             // Padding word
        corectrl->seg[3].pad      = 0xFFFF;             // Padding word

        corectrl->addr_in_core[0] = NVRAM_COREDATA_32;

        CoreTableSet(corectrl);
    }
}


void CoreTableSet(struct fgc_corectrl * corectrl)
{
    NVRAM_UNLOCK();

    // Set CORE control table in NVRAM

    memcpy((void *) NVRAM_CORECTRL_32, corectrl, sizeof(struct fgc_corectrl));
    MemSetWords((void *) NVRAM_COREDATA_32, 0x0000, NVRAM_COREDATA_W);         // Clear CORE data area
    NVRAM_LOCK();
}

// EOF
