/*---------------------------------------------------------------------------------------------------------*\
  File:         menuJtag.c

  Purpose:      Jtag test functions

  Author:       daniel.calcoen@cern.ch

  Notes:

  History:

    22 jan 07    doc    Created
\*---------------------------------------------------------------------------------------------------------*/

#include <iodefines.h>
#include <stdint.h>
#include <codes.h>
#include <dev.h>
#include <defconst.h>
#include <mcuDependent.h>
#include <memmap_mcu.h>
#include <menu.h>
#include <sleep.h>
#include <xda.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuJtag(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  addr;

    if (MenuGetInt32U(argv[0], 0, 1, &addr))
    {
        return;
    }

    addr = (uint32_t) xsv_file;

    //    JtagRun((uint8_t*)addr);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTjtag(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test]
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t out_port;
    uint8_t error = 0;
    char str_result[2][4] = {"OK", "NOK"};

    // Test backplane type, test is done for reception crate
    if (FGC_CRATE_TYPE_RECEPTION != dev.crate_type)
    {
        MenuRspError("Wrong backplane type:0x%04X", dev.crate_type);
        return;
    }

    P7.DR.BIT.B6 = 0;           // Enable digital CMD output


    P1.DDR.BIT.B0 = 0;

    if (P1.DR.BIT.B0)
    {
        MenuRspError("JTAG /JTAG_RDY should be zero");
        return;
    }

    // TDI <= TCK xor TMS xor TDO
    JTAG_DIRECTION_OUT_P |= JTAG_DIRECTION_OUT_TCK_MASK8;
    JTAG_DIRECTION_OUT_P |= JTAG_DIRECTION_OUT_TMS_MASK8;
    JTAG_DIRECTION_OUT_P |= JTAG_DIRECTION_OUT_TDI_MASK8;

    JTAG_DIRECTION_IN_P &= ~JTAG_DIRECTION_IN_TDO_MASK8;
    P8.ICR.BIT.B4 = 1; // ICR, use input buffer for TDO

    // All lines to 0
    JTAG_OUT_PORT_OUT_P &= ~JTAG_OUT_PORT_OUT_TCK_MASK8 & ~JTAG_OUT_PORT_OUT_TMS_MASK8 &
                           ~JTAG_OUT_PORT_OUT_TDI_MASK8;
    USLEEP(2);  // wait

    if ((JTAG_IN_PORT_IN_P & JTAG_DIRECTION_IN_TDO_MASK8) != 0)         // 0 expected
    {
        error |= 0x01;
    }

    // All lines to 1
    JTAG_OUT_PORT_OUT_P |= JTAG_OUT_PORT_OUT_TMS_MASK8 | JTAG_OUT_PORT_OUT_TCK_MASK8 |
                           JTAG_OUT_PORT_OUT_TDI_MASK8;
    USLEEP(2);  // wait

    if ((JTAG_IN_PORT_IN_P & JTAG_DIRECTION_IN_TDO_MASK8) == 0)         // 1 expected
    {
        error |= 0x02;
    }

    // Only TDI
    out_port = JTAG_OUT_PORT_OUT_P;
    out_port |=  JTAG_OUT_PORT_OUT_TDI_MASK8;
    out_port &= ~JTAG_OUT_PORT_OUT_TCK_MASK8;
    out_port &= ~JTAG_OUT_PORT_OUT_TMS_MASK8;
    JTAG_OUT_PORT_OUT_P = out_port;

    USLEEP(2);

    if ((JTAG_IN_PORT_IN_P & JTAG_DIRECTION_IN_TDO_MASK8) == 0)         // test TDI, 1 expected
    {
        error |= 0x04;
    }

    // Only TMS
    out_port = JTAG_OUT_PORT_OUT_P;
    out_port &= ~JTAG_OUT_PORT_OUT_TDI_MASK8;
    out_port &= ~JTAG_OUT_PORT_OUT_TCK_MASK8;
    out_port |=  JTAG_OUT_PORT_OUT_TMS_MASK8;
    JTAG_OUT_PORT_OUT_P = out_port;


    USLEEP(2);

    if ((JTAG_IN_PORT_IN_P & JTAG_DIRECTION_IN_TDO_MASK8) == 0)         // test TMS, 1 expected
    {
        error |= 0x08;
    }

    // Only TCK
    out_port = JTAG_OUT_PORT_OUT_P;
    out_port &= ~JTAG_OUT_PORT_OUT_TDI_MASK8;
    out_port |=  JTAG_OUT_PORT_OUT_TCK_MASK8;
    out_port &= ~JTAG_OUT_PORT_OUT_TMS_MASK8;
    JTAG_OUT_PORT_OUT_P = out_port;

    USLEEP(2);

    if ((JTAG_IN_PORT_IN_P & JTAG_DIRECTION_IN_TDO_MASK8) == 0)         // test TCK, 1 expected
    {
        error |= 0x10;
    }

    if (error)
    {
        MenuRspError("Test fails: TDO at 0 %s; TDO at 1 %s; TDI %s; TMS %s; TCK %s",
                     str_result[(error >> 0) & 0x01], str_result[(error >> 1) & 0x01], str_result[(error >> 2) & 0x01],
                     str_result[(error >> 3) & 0x01], str_result[(error >> 4) & 0x01], str_result[(error >> 5) & 0x01]);
        return;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuJtag.c
\*---------------------------------------------------------------------------------------------------------*/
