/*!
 *  @file     codes.c
 *  @defgroup FGC3:MCU boot
 *  @brief    FGC Boot code update functions
nn *
 *  Description:
 */

#define CODES_GLOBALS           // for code global variable
#define CODES_FGC_GLOBALS       // for erase_dev[], program_dev[], write_dev[], check_dev[] global variables
#define CODES_HOLDER_GLOBALS    // for codes_holder_info[]
#define FGC_CRATES_GLOBALS
// Includes

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <codes.h>
#include <defsyms.h>
#include <dev.h>
#include <fgc_runlog_entries.h>
#include <fieldbus_boot.h>
#include <m16c62.h>
#include <mcuDependent.h>
#include <mem.h>
#include <memmap_mcu.h>
#include <menu.h>
#include <menutree.h>
#include <sharedMemory.h>
#include <sleep.h>
#include <term.h>

// Constants

/*! Initial timeout: 40 seconds */
#define CODE_TIMEOUT_INIT   40000       //
/*! Running timeout: 2 seconds */
#define CODE_TIMEOUT_RUN    2000        //

#define CODE_NVRAM_MAGIC    0x6651

// Internal structures, unions and enumerations

struct
{
    uint16_t rx_dev_idx;          //!< Device being programmed
    uint32_t request_mask;        /*!< Code request mask (Gateway slots 0-31) */
    uint32_t request_mask_code_id;/*!< Code request mask (Gateway slots 0-31) */
    uint16_t rx_class_id;         /*!< Reception code class */
    uint16_t rx_code_id;          /*!< Reception code id */
    uint16_t rx_prev_blk_idx;     //!< Previous block index for received code
    bool     rx_last_block;
    uint16_t n_devs_corrupted;    // Number of corrupted codes detected
    uint16_t n_devs_locked;       // Number of codes locked
    uint16_t n_rw_devs_to_update; // Number of flash devices to update
} code_local;

struct
{
    uint8_t class_id;
    uint8_t code_id;
} static const code_class_map[] =
{
    { 61, FGC_CODE_61_MAINPROG },
    { 62, FGC_CODE_62_MAINPROG },
    { 63, FGC_CODE_63_MAINPROG },
    { 64, FGC_CODE_64_MAINPROG },
    { 65, FGC_CODE_65_MAINPROG },
    {  0, FGC_CODE_NULL        }
};

static uint8_t const code_class_map_length = sizeof(code_class_map) / sizeof(code_class_map[0]);



// Internal function definitions



static enum FGC_codes_status_internal CodesRxInitCodeReception(const struct code_block * const code_block)
{
    code_local.rx_dev_idx      = FGC_CLASS_UNKNOWN;
    code_local.rx_class_id     = FGC_CLASS_UNKNOWN;
    code_local.rx_code_id      = FGC_CLASS_UNKNOWN;
    code_local.rx_prev_blk_idx = 0;

    enum fgc_code_ids_short_list dev_idx;

    for (dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
    {
        const struct TCodesHolderInfo * const code_info = codes_holder_info + dev_idx;

        const bool code_state_is_valid = (code_info->dev_state == CODE_STATE_VALID ||
                                          code_info->dev_state == CODE_STATE_CORRUPTED);

        bool code_id_is_ok;

        // If device code_id matches first block's code_id
        // Main program is an exception - there is more than one code that fits the slot
        // TODO Refactor this piece of code

        if(dev_idx == SHORT_LIST_CODE_MP_DSP)
        {
            if(dev.is_standalone)
            {
                code_id_is_ok = CodesIdIsValid(code_block->class_id);
            }
            else
            {
                code_id_is_ok = (code_info->update.code_id == code_block->code_id);
            }
        }
        else
        {
            code_id_is_ok = (code_info->update.code_id == code_block->code_id);
        }

        if(code_id_is_ok && code_state_is_valid)
        {
            fprintf(TERM_TX, RSP_DATA ",Programming %s %lu into %s to replace %lu\n",
                    fgc_code_names[code_info->update.code_id],
                    code_info->update.version,
                    code_info->label,
                    code_info->dev.version);

            // Prepare to receive new code

            code_local.rx_dev_idx      = dev_idx;
            code_local.rx_class_id     = code_block->class_id;
            code_local.rx_code_id      = code_block->code_id;
            code_local.rx_prev_blk_idx = 0;
            code.rx.last_blk_idx       = fgc_code_sizes_blks[code_local.rx_code_id] - 1;

            return CODES_STATUS_SUCCESS;
        }
    }

    fprintf(TERM_TX, RSP_ERROR ",Unknown code block: %d, %d, %d\n",
            code_block->block_idx,
            code_block->class_id,
            code_block->code_id);

    return CODES_STATUS_NO_MATCHING_CODE;
}



/*!
 * Stores code block into SRAM code buffer
 *
 * @param[in]  code_block  Pointer to code block to store in SRAM buffer
 *
 * @retval  CODES_STATUS_SRAM_BUFFER_OVERFLOW  Code block would overflow the buffer
 * @retval  CODES_STATUS_SUCCESS               On success
 */
static enum FGC_codes_status_internal CodesRxStoreBlockIntoSram(const struct code_block * const code_block)
{
    // Return with error code when block won't fit into the buffer

    if(code_block->block_idx * FGC_CODE_BLK_SIZE > SRAM_TMPCODEBUF_B - FGC_CODE_BLK_SIZE)
    {
        return CODES_STATUS_SRAM_BUFFER_OVERFLOW;
    }

    // calculate block address in sram buffer

    void * sram_address = (void *)SRAM_TMPCODEBUF_32 + code_block->block_idx * FGC_CODE_BLK_SIZE;

    memcpy(sram_address, (void *) code_block->code, FGC_CODE_BLK_SIZE);

    return CODES_STATUS_SUCCESS;
}



/*!
 * Validates CRC of received code
 *
 * First, the function calculates CRC of the code in SRAM buffer and compares it
 * against CRC from code info (structure at the end of received code).
 *
 * Additionally, in fieldbus reception mode, the function cross-checks the CRC against CRC from
 * advertised codes table. In case it couldn't find matching code in the table, it returns an error.
 *
 * @param[in]  rx_mode  Code reception mode
 * @param[in]  dev_idx  Device (flash zone) index
 *
 * @retval  CODES_STATUS_INVALID_CRC       Calculated code CRC doesn't match CRC received in code info, or
 *                                         calculated CRC doesn't match CRC from advertised codes table
 * @retval  CODES_STATUS_NO_MATCHING_CODE  In fieldbus reception mode, the function couldn't find
 *                                         matching code in advertised codes table
 * @retval  CODES_STATUS_SUCCESS           CRC of the code is ok
 */
static enum FGC_codes_status_internal CodesRxValidateCodeInSram(const enum FGC_codes_rx_mode rx_mode,
                                                                const enum fgc_code_ids_short_list dev_idx)
{
          uint32_t        code_size;
    const uint8_t * const sram_buffer = (uint8_t *)SRAM_TMPCODEBUF_32;
    struct fgc_code_info  code_info;

    if (dev_idx == SHORT_LIST_CODE_MP_DSP)
    {
        switch (dev.class_id)
        {
            case 61:
                code_size = (uint32_t)codes_mainprog_info[MAINPROG_LIST_61].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 62:
                code_size = (uint32_t)codes_mainprog_info[MAINPROG_LIST_62].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 63:
                code_size = (uint32_t)codes_mainprog_info[MAINPROG_LIST_63].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 64:
                code_size = (uint32_t)codes_mainprog_info[MAINPROG_LIST_64].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 65:
                code_size = (uint32_t)codes_mainprog_info[MAINPROG_LIST_65].len_blks * FGC_CODE_BLK_SIZE;
                break;
            default:
                code_size = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE;
                break;
        }
    }
    else
    {
        code_size = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE;
    }

    // Fill code_info structure with the data we have in the buffer

    memcpy(&code_info, sram_buffer + code_size - FGC_CODE_INFO_SIZE, FGC_CODE_INFO_SIZE);

    // Calculate CRC of SRAM buffer

    const uint16_t crc = MemCrc16(sram_buffer, (code_size / 2) - 1);

    // Cross-check calculated CRC with the one from received code

    if(code_info.crc != crc)
    {
        fprintf(term.f, RSP_DATA " CRC: expected 0x%04X calculated 0x%04X\n", code_info.crc, crc);

        return CODES_STATUS_INVALID_CRC;
    }

    if(rx_mode == CODES_RX_FIELDBUS)
    {
        uint16_t slot;

        for(slot = 0; slot < FGC_CODE_MAX_SLOTS; slot++)
        {
            volatile struct advertised_code_info * code_adv = code.adv + slot;

            if(code_info.code_id == code_adv->code_id)
            {
                if(code_info.crc != code_adv->crc)
                {
                    fprintf(term.f, RSP_ERROR ",CRC mismatch: calculated 0x%04X advertised 0x%04X\n", code_info.crc, code_adv->crc);

                    return CODES_STATUS_INVALID_CRC;
                }

                return CODES_STATUS_SUCCESS;
            }
        }

        fprintf(term.f, RSP_ERROR ",Downloaded unknown code\n");

        return CODES_STATUS_NO_MATCHING_CODE;
    }

    return CODES_STATUS_SUCCESS;
}



static void CodesReportCode(enum fgc_code_ids_short_list dev_idx)
{
    const struct TCodesHolderInfo * const code_info = codes_holder_info + dev_idx;

    if(code_info->update.version != 0 || code_info->update_state == UPDATE_STATE_UPDATE)
    {
        fputs(TERM_BOLD, TERM_TX);
    }

    fprintf(TERM_TX, RSP_DATA ",%2d:%-7s|%-17s:%10lu|%-10s|%-7s:%10lu\n" TERM_NORMAL,
            dev_idx,
            code_info->label,
            (code_info->dev_state != CODE_STATE_VALID) ? "Invalid code" : fgc_code_names[code_info->dev.code_id],
            code_info->dev.version,
            TypeLabel(code_state, code_info->dev_state),
            TypeLabel(update_state, code_info->update_state),
            code_info->update.version);
}



static void CodesReportUpdate(void)
{
    enum fgc_code_ids_short_list dev_idx;

    fputs(RSP_LABEL "," TERM_UL "FLASH ZONE|      INSTALLED VERSION     |CODE STATE|  UPDATE VERSION  \n" TERM_NORMAL, TERM_TX);

    for(dev_idx = SHORT_LIST_CODE_PLD; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
    {
        CodesReportCode(dev_idx);
    }

    fprintf(TERM_TX, RSP_ID_NAME "Writable devices:%u\n",
            "ST",
            "Update codes from Fieldbus - ",
            code_local.n_rw_devs_to_update);
}



//static void CodesReportNameDb(void)
//{
//    fputs(RSP_LABEL "," TERM_UL "FLASH ZONE|      INSTALLED VERSION     |CODE STATE|  UPDATE VERSION  \n" TERM_NORMAL, TERM_TX);
//
//    CodesReportCode(SHORT_LIST_CODE_NAMEDB);
//
//    fprintf(TERM_TX, RSP_ID_NAME "Writable devices:%u  Read-only devices:%u\n",
//            "ST",
//            "Update codes from Fieldbus - ",
//            code_local.n_rw_devs_to_update, code_local.n_ro_devs_to_update);
//}



// External function definitions



void CodesVerifyNvram(void)
{
    // Check Flash Locks magic number and reset all locks if not present

    if(NVRAM_MAGIC_FP != CODE_NVRAM_MAGIC)
    {
        NVRAM_UNLOCK();

        // Erase Flash locks area of NVRAM

        MemSetWords((void *) NVRAM_FLASHLOCKS_32, 0x0000, NVRAM_FLASHLOCKS_W);

        NVRAM_MAGIC_FP = CODE_NVRAM_MAGIC;

        NVRAM_LOCK();
    }
}



void CodesSelectMainProg(void)
{
    uint8_t i;

    // Look for code id for this class

    for(i = 0; i < code_class_map_length && code_class_map[i].class_id != dev.class_id; ++i);

    codes_holder_info[SHORT_LIST_CODE_MP_DSP].update.class_id = dev.class_id;
    codes_holder_info[SHORT_LIST_CODE_MP_DSP].update.code_id  = code_class_map[i].code_id;
}



void CodesCountFlashLocks(void)
{
    // TODO This function has to be called after InitSharedMem(), which can clear the
    // shared_mem structure. How to assert that dependency?

    const uint8_t                * dev_lock = (uint8_t *)NVRAM_FLASHLOCKS_32;
    enum fgc_code_ids_short_list   dev_idx;

    shared_mem.code_locked_count = 0;

    for(dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
    {
        struct TCodesHolderInfo * const code_info = codes_holder_info + dev_idx;

        if(*(dev_lock++) != 0)
        {
            code_info->locked_f = true;

            shared_mem.code_locked_count++;
        }
        else
        {
            code_info->locked_f = false;
        }
    }
}



void CodesUpdateInstalledInfo(enum fgc_code_ids_short_list code_id_start, enum fgc_code_ids_short_list code_id_end)
{
    enum fgc_code_ids_short_list dev_idx;

    for(dev_idx = code_id_start; dev_idx < code_id_end; dev_idx++)
    {
        // Check state of this device and fill the codes_holder_info[device].dev structure with the
        // info from the code already in FLASH

        check_dev[dev_idx](dev_idx);

        if(MenuErrorIsPending())
        {
            MenuPrintError();
        }
    }
}



void CodesSetNameFromNameDb(struct fgc_dev_name * const name, const uint16_t fieldbus_id)
{
    const struct fgc_dev_name * const name_db = (const struct fgc_dev_name *)codes_holder_info[SHORT_LIST_CODE_NAMEDB].base_addr;

    *name = name_db[fieldbus_id];
    name->name[FGC_MAX_DEV_LEN] = '\0';
}



enum FGC_codes_status CodesInitM16C62(void)
{
    // Do not check the IDBoot program if there were three failed attempts already
    // When NVRAM_M16C62_INIT_FAILURES_P == 3 the boot will not be flashed and the FGC will not be reset -
    // the function will only check if previous boot flashing was successful

    // Power off the M16C62, as it is on after a reset
    // If it is not powered off then C62PowerOn() call will return an error

    if(NVRAM_M16C62_INIT_FAILURES_P <= CODE_M16C62_MAX_INITS)
    {
        C62PowerOff();

        // Power on the M16C62, but stay in the IDBoot program (C62_SWITCH_PROG prevents running IDMain)
        // If there was a problem while starting IDBoot

        if(C62PowerOn(C62_SWITCH_PROG) != 0)
        {
            // Increase the failure counter

            NVRAM_UNLOCK();
            NVRAM_M16C62_INIT_FAILURES_P++;
            NVRAM_LOCK();

            // Flash IDBoot and trigger global reset

            if(NVRAM_M16C62_INIT_FAILURES_P <= CODE_M16C62_MAX_INITS)
            {
                MenuMTm16C62BootFlash(0, NULL);
            }
        }
        else    // If IDBoot is ok
        {
            // Reset the failure counter

            NVRAM_UNLOCK();
            NVRAM_M16C62_INIT_FAILURES_P = 0;
            NVRAM_LOCK();
        }

        // Power off the M16C62, it should be powered only when it's needed

        C62PowerOff();
    }

    return (NVRAM_M16C62_INIT_FAILURES_P > 0) ? CODES_M16C62_FAILURE : CODES_SUCCESS;
}



enum FGC_codes_status CodesVerifyInstalled(void)
{
    CodesUpdateInstalledInfo(0, SHORT_LIST_CODE_LAST);

    enum fgc_code_ids_short_list dev_idx;

    // Ensure that no code is corrupted or not present

    for(dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
    {
        const struct TCodesHolderInfo * const code_info = codes_holder_info + dev_idx;

        if(code_info->dev_state != CODE_STATE_VALID)
        {
            return CODES_CORRUPTED_CODE;
        }
    }

    // IDDB and PTDB versions have to match

    const struct TCodesHolderInfo * const iddb_info = codes_holder_info + SHORT_LIST_CODE_IDDB;
    const struct TCodesHolderInfo * const ptdb_info = codes_holder_info + SHORT_LIST_CODE_PTDB;

    if(iddb_info->dev.version != ptdb_info->dev.version)
    {
        return CODES_IDDB_PTDB_MISMATCH;
    }

    // SYSDB, COMPDB and DIMDB versions have to match

    const struct TCodesHolderInfo * const compdb_info = codes_holder_info + SHORT_LIST_CODE_COMPDB;
    const struct TCodesHolderInfo * const sysdb_info  = codes_holder_info + SHORT_LIST_CODE_SYSDB;
    const struct TCodesHolderInfo * const dimdb_info  = codes_holder_info + SHORT_LIST_CODE_DIMDB;

    if(compdb_info->dev.version != sysdb_info->dev.version ||
       sysdb_info->dev.version  != dimdb_info->dev.version)
    {
        return CODES_SYSDB_COMPDB_DIMDB_MISMATCH;
    }

    // Make sure that class ID of resident main program matches the one in NameDB

    const struct TCodesHolderInfo * const mainprog_info = codes_holder_info + SHORT_LIST_CODE_MP_DSP;

    if(dev.class_id != mainprog_info->dev.class_id)
    {
        return CODES_MAIN_NAMEDB_MISMATCH;
    }

    return CODES_SUCCESS;
}



void CodesUpdateSharedMemVersions(void)
{
    static_assert(ArrayLen(shared_mem.codes_version) <= ArrayLen(codes_holder_info), "Array lengths don't match");

    uint16_t dev_idx;

    for(dev_idx = 0; dev_idx < ArrayLen(shared_mem.codes_version); dev_idx++)
    {
        shared_mem.codes_version[dev_idx] = codes_holder_info[dev_idx].dev.version;
    }
}



void CodesUpdateAll(void)
{
    CodesUpdateInstalledInfo(0, SHORT_LIST_CODE_LAST);

    CodesUpdate(CODES_RX_FIELDBUS);

    CodesUpdateSharedMemVersions();
}



void CodesInitAdvTable(void)
{
    uint16_t slot;

    for(slot = 0; slot < FGC_CODE_MAX_SLOTS; slot++)
    {
        code.adv[slot].code_id = FGC_CODE_NULL;
    }
}



bool CodesIdIsValid(const uint16_t class_id)
{
    uint8_t i;

    if(class_id == FGC_CODE_NULL)
    {
        return false;
    }

    for(i = 0; i < code_class_map_length; ++i)
    {
        if(code_class_map[i].class_id == class_id)
        {
            return true;
        }
    }

    return false;
}



void CodesUpdateAdvertisedInfo(void)
{
    uint16_t code_id;
    uint16_t slot;

    // validate the information we receive from the Gateway PC
    // Check advertised code IDs and lengths and set the states

    // For each advertised code

    for(slot = 0; slot < FGC_CODE_MAX_SLOTS; slot++)
    {
        // Get advertised code ID

        code_id = code.adv[slot].code_id;

        // If code ID is NULL (0xFF)

        if(code_id == FGC_CODE_NULL)
        {
            code.adv_state[slot] = ADV_CODE_STATE_NOT_IN_USE;
        }
        else
        {
            // else if CODE ID is not valid

            if(code_id >= FGC_NUM_CODES)
            {
                code.adv_state[slot] = ADV_CODE_STATE_UNKNOWN_CODE;
            }
            else    // else code ID is valid
            {
                // If length is correct

                if(code.adv[slot].len_blks == fgc_code_sizes_blks[code_id])
                {
                    code.adv_state[slot] = ADV_CODE_STATE_OK;
                }
                else    // else length is wrong
                {
                    code.adv_state[slot] = ADV_CODE_STATE_BAD_LEN;
                }
            }
        }
    }
}



void CodesResetUpdateFailures(void)
{
    uint16_t dev_idx;

    for(dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
    {
        struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;

        code_info->update_state = UPDATE_STATE_UNKNOWN;
    }
}



static void CodesSetUpdateCodeInfo(void)
{
    uint16_t slot;
    uint16_t dev_idx;

    code_local.request_mask         = 0;    // Reset code request mask (GW slots 0-31)
    code_local.request_mask_code_id = 0;    // Reset code request mask (code ID)
    code_local.n_devs_corrupted     = 0;    // Reset count of corrupted devices
    code_local.n_devs_locked        = 0;    // Reset count of locked devices
    code_local.n_rw_devs_to_update  = 0;    // Reset count of devices to be updated

    // Check each of our code holders, if they are accessible and up to date

    for(dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
    {
        struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;

        // Skip the code if update failed

        if(code_info->update_state == UPDATE_STATE_FAILURE)
        {
            continue;
        }

        // fill with default states

        code_info->update.version = 0;
        code_info->update.crc     = 0;
        code_info->update_state   = UPDATE_STATE_OK;

        // Count corrupted codes

        if(code_info->dev_state == CODE_STATE_CORRUPTED ||
           code_info->dev_state == CODE_STATE_DEV_FAILED)
        {
            code_local.n_devs_corrupted++;
        }

        // Count locked codes

        if(code_info->locked_f == TRUE)
        {
            code_info->update_state = UPDATE_STATE_LOCKED;
            code_local.n_devs_locked++;

            // Prevent update

            continue;
        }

        // look in the previously validated slots if there is some code that match for this code holder

        for(slot = 0; slot < FGC_CODE_MAX_SLOTS; slot++)
        {
            volatile struct advertised_code_info * code_adv = code.adv + slot;

            if(code_info->update.code_id == code_adv->code_id)
            {
                const uint16_t dev_state        = code_info->dev_state;
                const bool     crc_mismatch     = (code_info->calc_crc != code_adv->crc);
                const bool     version_mismatch = (code_adv->version != 0 && code_info->dev.version != code_adv->version);

                if(code.adv_state[slot] == ADV_CODE_STATE_OK &&
                   (dev_state == CODE_STATE_CORRUPTED ||
                   (dev_state == CODE_STATE_VALID && (crc_mismatch || version_mismatch))))
                {
                    code_info->update.version = code_adv->version;
                    code_info->update.crc     = code.adv[slot].crc;

                    code_local.n_rw_devs_to_update++;
                    code_info->update_state  = UPDATE_STATE_UPDATE;
                    code_local.request_mask |= (1L << slot);
                    code_local.request_mask_code_id |= (1L << code_adv->code_id);
                }

                break;
            }
        }
    }
}



void CodesUpdateAdvCode(void)
{
    // TODO Why volatile?

    volatile struct advertised_code_info * ac = code.adv + time_var.adv_list_idx;

    ac->class_id    = time_var.adv_code_class_id;          // Transfer advertised code info
    ac->code_id     = time_var.adv_code_id;                // to adv code structure
    ac->version     = time_var.adv_code_version;           // Advertised code version
    ac->crc         = time_var.adv_code_crc;               // Code CRC16
    ac->len_blks    = time_var.adv_code_len_blk;
    ac->old_version = time_var.old_adv_code_version;
}



void CodesCheckUpdates(void)
{
    CodesUpdateAdvertisedInfo();

    CodesSetUpdateCodeInfo();
}



bool CodesNameDbInstalled(void)
{
    const struct TCodesHolderInfo * const code_info = codes_holder_info + SHORT_LIST_CODE_NAMEDB;

    check_dev[SHORT_LIST_CODE_NAMEDB](SHORT_LIST_CODE_NAMEDB);

    return (code_info->dev_state == CODE_STATE_VALID);
}



void CodesInstallNameDb(void)
{
    CodesVerifyNvram();

    CodesUpdateInstalledInfo(SHORT_LIST_CODE_NAMEDB, SHORT_LIST_CODE_NAMEDB + 1);

    CodesUpdate(CODES_RX_FIELDBUS);
}



/*!
 * Handles background part of end-of-data-stream check during code reception through terminal
 *
 * The function sets an initial timeout of 2s and waits until it exceeds.
 * The timeout will be re-set in the millisecond if any terminal character is
 * received and the timeout is not yet exceeded.
 *
 * The goal of the timeout is to prevent flooding boot menus with data stream in case
 * of failed code reception.
 *
 * In interactive mode, the function displays a spinner, indicating that the
 * FGC is still alive.
 */
static void CodesRxProcessTermStreamTimeout(void)
{
    uint16_t spinner_index = 0;

    // Set safety timeout

    code.rx.term_stream_timeout_ms = 2000;

    // Wait until safety timeout is exceeded

    if(term.edit_state == TERM_STATE_INTERACTIVE)
    {
        fprintf(TERM_TX, "   Waiting for data stream to end\n");
    }

    while(code.rx.term_stream_timeout_ms != 0)
    {
        static const char spinner[] = { '-', '\\', '|', '/' };

        if(term.edit_state == TERM_STATE_INTERACTIVE)
        {
            fprintf(TERM_TX, "   %c\r", spinner[spinner_index]);

            spinner_index++;

            if(spinner_index >= ArrayLen(spinner))
            {
                spinner_index = 0;
            }

            MSLEEP(1000);
        }
    }

    if(term.edit_state == TERM_STATE_INTERACTIVE)
    {
        fprintf(TERM_TX, "   Data stream ended\n");
    }
}



/*!
 * Disables code reception in given mode
 *
 * The function informs the proper ISR to stop processing code blocks.
 *
 * In terminal reception mode, it runs a safety timeout of at least 2s
 * to make sure that the data stream has stopped and won't flood the boot
 * menus with random characters.
 *
 * In fieldbus reception mode, the function cancels code request to
 * the gateway.
 *
 * @param[in]  rx_mode  Code reception mode that should be disabled
 */
static void CodesRxDisable(const enum FGC_codes_rx_mode rx_mode)
{
    if(rx_mode == CODES_RX_TERMINAL)
    {
        CodesRxProcessTermStreamTimeout();
    }
    else if(rx_mode == CODES_RX_FIELDBUS)
    {
        stat_var.fgc_stat.class_data.boot.code_request = 0;
    }

    // Disable code reception

    code.rx.mode = CODES_RX_NONE;

    code.rx.reception_mask = 0;
}



/*!
 * Validates code block header against header received with the first block
 *
 * @param[in]  code_block  Pointer to code block to validate
 *
 * @retval  CODES_STATUS_UNEXPECTED_BLOCK_INDEX  Code block index is smaller or equal to the previous one, or
 *                                               is 0, which means the code reception didn't start properly, or
 *                                               is greater than the maximum expected index
 * @retval  CODES_STATUS_CLASS_ID_MISMATCH       Class ID of the block didn't match the expected class ID
 * @retval  CODES_STATUS_CODE_ID_MISMATCH        Code ID of the block didn't match the expected code ID
 * @retval  CODES_STATUS_SUCCESS                 Code block header is ok
 */
static enum FGC_codes_status_internal CodesRxValidateBlock(const struct code_block * const code_block)
{
    const uint16_t blk_idx = code_block->block_idx;

    if((blk_idx != 0 && blk_idx <= code_local.rx_prev_blk_idx) ||
       (blk_idx == 0 && code_local.rx_prev_blk_idx != 0))
    {
        fprintf(TERM_TX, RSP_ERROR ",Unexpected code block index: received %d, previous: %d\n",
                blk_idx,
                code_local.rx_prev_blk_idx);

        return CODES_STATUS_UNEXPECTED_BLOCK_INDEX;
    }

    if(blk_idx > code.rx.last_blk_idx)
    {
        fprintf(TERM_TX, RSP_ERROR ",Code block index out of bounds: received %d, max: %d\n",
                blk_idx,
                code.rx.last_blk_idx);

        return CODES_STATUS_UNEXPECTED_BLOCK_INDEX;
    }

    if(code_block->class_id != code_local.rx_class_id)
    {
        fprintf(TERM_TX, RSP_ERROR ",Code block class ID mismatch: received %d, expected %d\n",
                code_block->class_id,
                code_local.rx_class_id);

        return CODES_STATUS_CLASS_ID_MISMATCH;
    }

    if(code_block->code_id  != code_local.rx_code_id)
    {
        fprintf(TERM_TX, RSP_ERROR ",Code block code ID mismatch: received %d, expected %d\n",
                code_block->code_id,
                code_local.rx_code_id);

        return CODES_STATUS_CODE_ID_MISMATCH;
    }

    return CODES_STATUS_SUCCESS;
}



/*
 * Copies received code from SRAM buffer to flash
 */
static enum FGC_codes_status_internal CodesRxReprogramFlash(const enum FGC_codes_rx_mode rx_mode)
{
    const uint16_t     rx_dev_idx = code_local.rx_dev_idx;
    const char * const code_label = codes_holder_info[rx_dev_idx].label;

    fprintf(term.f, RSP_DATA ",Download %s complete\n", code_label);

    enum FGC_codes_status_internal status = CodesRxValidateCodeInSram(rx_mode, rx_dev_idx);

    if(status != CODES_STATUS_SUCCESS)
    {
        fprintf(term.f, RSP_ERROR ",%s Reprogramming abandonned\n", code_label);

        return status;
    }

    status = erase_dev[rx_dev_idx](rx_dev_idx);

    if(status != CODES_STATUS_SUCCESS)
    {
        fprintf(term.f, RSP_ERROR ",%s flash erase failure\n", code_label);

        return status;
    }

    status = write_dev[rx_dev_idx](rx_dev_idx);

    if(status != CODES_STATUS_SUCCESS)
    {
        fprintf(term.f, RSP_ERROR ",%s flash write failure\n", code_label);
    }

    return status;
}



/*
 * Verifies code in flash after reprogramming
 *
 * TODO Fill retvals
 */
static enum FGC_codes_status_internal CodesRxVerifyFlash(void)
{
    const uint16_t     rx_dev_idx = code_local.rx_dev_idx;
    const char * const code_label = codes_holder_info[rx_dev_idx].label;

    // Check state of this device and fill the codes_holder_info[device].dev structure with the
    // info from the code already in FLASH

    enum FGC_codes_status_internal status = check_dev[rx_dev_idx](rx_dev_idx);

    if(status == CODES_STATUS_SUCCESS)
    {
        fprintf(term.f, RSP_DATA",%s Flash CRC OK\n", code_label);
        shared_mem.codes_version[rx_dev_idx] = codes_holder_info[rx_dev_idx].dev.version;
    }
    else
    {
        fprintf(term.f, RSP_DATA",%s Flash CRC BAD\n", code_label);
    }

    return status;
}



/*
 * Reports code update progress to the terminal
 *
 * @param[in]  code_block    Pointer to the latest code block
 * @param[in]  refresh_rate  Refresh rate in number of code blocks
 */
static inline void CodesRxReportProgress(const struct code_block * const code_block,
                                         const uint16_t                  refresh_rate)
{
    const uint16_t blk_idx = code_block->block_idx;

    if(term.edit_state == TERM_STATE_INTERACTIVE && (blk_idx % refresh_rate) == 0)
    {
        // Report block header on terminal

        fprintf(TERM_TX, "   %s:%u:%u%%        \r",
                fgc_code_names[code_block->code_id],
                blk_idx,
                (uint16_t)((uint32_t)blk_idx * 100L / fgc_code_sizes_blks[code_block->code_id]));
    }
}



/*
 * Returns code progress refresh rate in number of code blocks
 *
 * @param[in]  rx_mode  Code reception mode
 *
 * @return  Refresh rate in number of code blocks
 */
static inline uint16_t CodesRxGetRefreshRate(const enum FGC_codes_rx_mode rx_mode)
{
    if(rx_mode == CODES_RX_TERMINAL)
    {
        return 30;
    }

    return fieldbus.progress_refresh_rate;
}



/*!
 * Returns code block reception timeout
 *
 * The initial timeout (waiting for the first code block)
 * is long enough (40s) to allow user for starting transmission.
 * After the first block is received, the timeout will be reduced to 2s.
 *
 * @param[in]  first_block  First code block indicator
 */
static inline uint16_t CodesRxGetTimeout(const bool first_block)
{
    return first_block ? CODE_TIMEOUT_INIT : CODE_TIMEOUT_RUN;
}



/*!
 * Waits for next code block, validates it and saves it in SRAM buffer
 *
 * @param[in]  rx_mode      Code reception mode
 * @param[in]  first_block  First run indicator
 *
 * @retval  CODES_STATUS_TIMEOUT                 Timeout waiting for code block
 * @retval  CODES_STATUS_ABORTED_BY_USER         User pressed Ctrl-C in fieldbus reception mode
 * @retval  CODES_STATUS_RX_BUFFER_OVERFLOW      Code reception buffer was overflown
 * @retval  CODES_STATUS_NO_MATCHING_CODE        Couldn't find code matching first code block header
 * @retval  CODES_STATUS_SRAM_BUFFER_OVERFLOW    SRAM buffer was overflown
 * @retval  CODES_STATUS_UNEXPECTED_BLOCK_INDEX  Code block index is smaller or equal to the previous one, or
 *                                               is 0, which means the code reception didn't start properly, or
 *                                               is greater than the maximum expected index
 * @retval  CODES_STATUS_CLASS_ID_MISMATCH       Class ID of the block didn't match the expected class ID
 * @retval  CODES_STATUS_CODE_ID_MISMATCH        Code ID of the block didn't match the expected code ID
 * @retval  CODES_STATUS_SUCCESS                 Code block received and saved in SRAM
 */
static enum FGC_codes_status_internal CodesRxProcessBlock(const enum FGC_codes_rx_mode rx_mode,
                                                          const bool                   first_block)
{
    code.rx.timeout_ms             = CodesRxGetTimeout(first_block);
    code.rx.term_stream_timeout_ms = 0;

    while(true)
    {
        // Check if new code block is available or timeout has been exceeded or,
        // in case of fieldbus reception, if user has pressed Ctrl-C

        if(code.rx.timeout_ms == 0 || CodesRxBufferNextBlockAvailable())
        {
            if(code.rx.timeout_ms == 0)
            {
                return CODES_STATUS_TIMEOUT;
            }

            break;
        }

        if(rx_mode == CODES_RX_FIELDBUS && dev.abort_f)
        {
            return CODES_STATUS_ABORTED_BY_USER;
        }
    }

    const struct code_block * code_block = CodesRxBufferGetLatestBlock();

    // Return if the buffer has overflown

    if(code_block == NULL)
    {
        return CODES_STATUS_RX_BUFFER_OVERFLOW;
    }

    CodesRxReportProgress(code_block, CodesRxGetRefreshRate(rx_mode));

    if(first_block)
    {
        // Check if the block matches any of resident codes and initialise reception variables

        if(CodesRxInitCodeReception(code_block) != CODES_STATUS_SUCCESS)
        {
            return CODES_STATUS_NO_MATCHING_CODE;
        }

        // Clear request to the gateway

        stat_var.fgc_stat.class_data.boot.code_request = 0;
    }
    else
    {
        // Check if received block matched code details from the first block

        enum FGC_codes_status_internal status = CodesRxValidateBlock(code_block);

        if(status != CODES_STATUS_SUCCESS)
        {
            return status;
        }
    }

    // Store code block in SRAM buffer

    if(CodesRxStoreBlockIntoSram(code_block) != CODES_STATUS_SUCCESS)
    {
        return CODES_STATUS_SRAM_BUFFER_OVERFLOW;
    }

    code_local.rx_last_block   = (code_block->block_idx == code.rx.last_blk_idx);
    code_local.rx_prev_blk_idx = code_block->block_idx;

    return CODES_STATUS_SUCCESS;
}



/*
 * Enables code reception in requested mode
 *
 * The function informs the proper ISR (fieldbus/terminal) that it should expect code
 * packets coming and what codes it should accept. It also sends code request
 * to the gateway in fieldbus mode.
 *
 * @param[in]  rx_mode  Code reception mode to initialise
 */
static void CodesRxEnable(const enum FGC_codes_rx_mode rx_mode)
{
    code.rx.first_block    = false;
    code.rx.reception_mask = code_local.request_mask_code_id;
    code.rx.mode           = rx_mode;

    if(rx_mode == CODES_RX_FIELDBUS)
    {
        stat_var.fgc_stat.class_data.boot.code_request = code_local.request_mask;
    }
}



static void CodesRxMarkUpdateAsFailed(void)
{
    if(code_local.rx_dev_idx != FGC_CLASS_UNKNOWN)
    {
        struct TCodesHolderInfo * const code_info = codes_holder_info + code_local.rx_dev_idx;

        code_info->update_state = UPDATE_STATE_FAILURE;
    }
}



static inline void CodesRxReportFailure(enum FGC_codes_status_internal status)
{
    fprintf(TERM_TX, RSP_ERROR ",Update failed: %s\n", codes_status_internal_labels[status].label);
}



static inline bool CodesRxTransmissionStarted(void)
{
    return (code_local.rx_dev_idx != FGC_CLASS_UNKNOWN);
}



/*!
 * Sets all of SRAM code buffer to 0xFFFF
 *
 * This is crucial in case of serial code format (and possibly fieldbus in the future).
 * Code blocks that consists only of 0xFFFF values are discarded during code preparation, so they are
 * never received by the FGC. If the buffer isn't prepared in advance, there may be garbage
 * values in these memory addresses, resulting in bad CRC and corrupted code.
 */
static inline void CodesRxSramReset(void)
{
    MemSetWords((void *) SRAM_TMPCODEBUF_32, 0xFFFF, SRAM_TMPCODEBUF_W);
}



static void CodesRxSetFgcStatus(void)
{
    // Check update results and set fault or warning if required

    if(code_local.n_devs_corrupted != 0 || code_local.n_devs_locked != 0)
    {
        stat_var.fgc_stat.class_data.c60.st_faults  |= FGC_FLT_FGC_HW;
        stat_var.fgc_stat.class_data.c60.st_latched |= FGC_LAT_CODE_FLT;
    }
    else if(code_local.n_rw_devs_to_update != 0)
    {
        stat_var.fgc_stat.class_data.c60.st_latched |= FGC_LAT_CODE_FLT;
    }
}



static enum FGC_codes_status_internal CodesRxDownloadCode(const enum FGC_codes_rx_mode rx_mode)
{
    bool first_block = true;
    enum FGC_codes_status_internal status;

    do
    {
        status = CodesRxProcessBlock(rx_mode, first_block);

        first_block = false;
    }
    while(!code_local.rx_last_block && status == CODES_STATUS_SUCCESS);

    return status;
}



void CodesUpdate(const enum FGC_codes_rx_mode rx_mode)
{
    enum FGC_codes_status_internal status;

    if(rx_mode == CODES_RX_FIELDBUS)
    {
        CodesCheckUpdates();

        // Return if nothing to update or abort flag present (TEST LEDS or Ctrl-C)

        if(dev.abort_f || code_local.n_rw_devs_to_update == 0)
        {
            return;
        }

        CodesReportUpdate();
    }

    SetStateOp(FGC_OP_PROGRAMMING);

    do
    {
        CodesRxBufferReset();
        CodesRxSramReset();

        // Enable code reception and try to download code

        CodesRxEnable(rx_mode);

        status = CodesRxDownloadCode(rx_mode);

        // Process code download status

        if(status != CODES_STATUS_SUCCESS)
        {
            CodesRxReportFailure(status);

            // Bail-out on user request

            if(status == CODES_STATUS_ABORTED_BY_USER)
            {
                CodesRxDisable(rx_mode);

                break;
            }

            // If first code block has been received, we know which code to
            // mark as failed. It may happen however, that we didn't receive any packet.
            // In that case, we simply stop code reception.

            if(CodesRxTransmissionStarted())
            {
                CodesRxMarkUpdateAsFailed();
            }
            else
            {
                CodesRxDisable(rx_mode);

                break;
            }
        }

        // We need to stop reception if we want to reset the rx buffer safely

        CodesRxDisable(rx_mode);

        // Attempt to reprogram flash only if code reception was successful

        if(status == CODES_STATUS_SUCCESS)
        {
            status = CodesRxReprogramFlash(rx_mode);

            if(status != CODES_STATUS_SUCCESS)
            {
                CodesRxReportFailure(status);
                CodesRxMarkUpdateAsFailed();
            }

            // Verify flash even if programming failed

            status = CodesRxVerifyFlash();

            if(status != CODES_STATUS_SUCCESS)
            {
                CodesRxReportFailure(status);
                CodesRxMarkUpdateAsFailed();
            }
        }

        // In fieldbus reception mode, check if there are any other codes that need updating

        if(rx_mode == CODES_RX_FIELDBUS)
        {
            CodesCheckUpdates();
        }
    }
    while(code_local.n_rw_devs_to_update != 0);

    CodesRxSetFgcStatus();

    SetStateOp(FGC_OP_BOOT);
}



void CodesSetNameUnknown(struct fgc_dev_name * name)
{
    name->class_id   = FGC_CLASS_UNKNOWN;
    name->omode_mask = 0;

    strcpy(name->name, "UNKNOWN");
}



bool CodesNameDbUpdateIsPending(void)
{
    const struct TCodesHolderInfo * const code_info = codes_holder_info + SHORT_LIST_CODE_NAMEDB;

    return (code_info->update_state == UPDATE_STATE_UPDATE);
}



// TODO Make these two variables public?

uint16_t CodesGetNumRwDevsToUpdate(void)
{
    return(code_local.n_rw_devs_to_update);
}

// EOF
