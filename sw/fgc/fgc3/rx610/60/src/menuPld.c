/*---------------------------------------------------------------------------------------------------------*\
  File:         menuPld.c

  Purpose:      Functions to run for the general boot menu options

  Author:       daniel.calcoen@cern.ch

  Notes:
                These menues belongs to the [Manual Test] part and [Self Test] part

  History:

    13/04/04    stp     Created
    22/11/06    pfr     Ported for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>              // for fprintf()
#include <term.h>
#include <mcuDependent.h>      // for MSLEEP()
#include <interrupt_handlers.h>
#include <iodefines.h>          // specific processor registers and constants
#include <stdint.h>           // basic typedefs
#include <memmap_mcu.h>         // Include memory map consts
#include <pld.h>                // for PldBootISF()
#include <pld_spi.h>            // for PldSPIfeedByteToController(), PldSPIendTransaction(), PldSPIeraseSector(), PldSPIread()
#include <menu.h>               // for MenuGetInt16U(), MenuRspError()
#include <hardware_setup.h>     // for EnableTickInterrupt()
#include <dev.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuMTpldReset(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    DisableNetworkInterrupt();
    // pass to use locally generated ms tick in case of PLD crash
    Disable1msTickInterrupt();
    Enable1msTickInterruptFromMcuTimer();

    if(PldBootISF() != PLD_SUCCESS || !pld_info.ok)
    {
        MenuRspError("PLD reboot failed");
    }
    else
    {
        Disable1msTickInterrupt();
        Enable1msTickInterruptFromPLD();
    }

    EnableNetworkInterrupt();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTpldVersion(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t   reg[16];
    uint16_t  ii;

    fprintf(term.f, "Model       0x%04X\n", pld_info.hw_model);
    fprintf(term.f, "Vers        0x%04X\n", pld_info.hw_version);
    fprintf(term.f, "State       %s\n", TypeLabel(pld_stat_lbl, pld_info.state));
    fprintf(term.f, "ISF mem     %s\n", TypeLabel(pld_prog_lbl, pld_info.spi_state));
    fprintf(term.f, "FGC type    0x%04X\n", MID_FGCTYPE_P);
    fprintf(term.f, "HW vers     0x%04X\n", MID_FGCVER_P);
    fprintf(term.f, "Crate type  0x%04X\n", MID_CRATETYPE_P);

    // check the protection status of the xilinx internal EPROM

    PldSPIfeedByteToController(SPI_CMD_PROTECT_RD);
    PldSPIfeedByteToController(0x00); // don't care
    PldSPIfeedByteToController(0x00); // don't care
    PldSPIfeedByteToController(0x00); // don't care

    fprintf(term.f, "\nPLD protection state\n");

    for (ii = 0; ii < 16; ii++)  // we can now read protection state for sectors 0 to 15
    {
        reg[ii] = PldSPIfeedByteToController(0x00); // answer 0xFF means protected, 0x00 means unprotected
        fprintf(term.f, "%u:0x%02X ", ii, reg[ii]);

    }

    PldSPIendTransaction();     // Release CSB
    fprintf(term.f, "\n");
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTpldISFsectorErase(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Erase a sector of the PLD flash.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  sector;

    if (MenuGetInt16U(argv[0], 0, 15, &sector) != 0)
    {
        return;
    }

    if (PldSPIeraseSector(sector, dev.pld_hw_model) != SPI_STATE_OK)    // Erase sector
    {
        MenuRspError("Erase failed");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTpldISFread(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Read the PLD In System Flash
  WARNING!!!
  when in normal mode (not power 2) for the XC3S 700AN - 4FG484C we read the 264 bytes page each 512 bytes alignment
  so we have 264 bytes of data and 248 bytes of garbage
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  addr;
    uint16_t  size;
    uint16_t  column_counter;
    uint8_t   data;

    if ((MenuGetInt32U(argv[0], 0, 0x107FFF, &addr) != 0)
        || (MenuGetInt16U(argv[1], 0, 0xFFFF,   &size) != 0)
       )
    {
        return;
    }

    // Read & print

    column_counter = 0;

    while (size-- != 0)
    {
        if (column_counter++ == 0)
        {
            fprintf(term.f, RSP_DATA);
        }

        if (PldSPIread(addr++, &data) != SPI_STATE_OK)
        {
            MenuRspError("Read failed at 0x%08X", addr);
        }

        fprintf(term.f, ",0x%02X", data);

        if (column_counter == 16)           // add a new line each 16 columns
        {
            column_counter = 0;
            fprintf(term.f, "\n");
        }
    }

    fprintf(term.f, "\n");
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuPld.c
\*---------------------------------------------------------------------------------------------------------*/
