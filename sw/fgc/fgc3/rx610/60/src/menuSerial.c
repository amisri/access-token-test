/*---------------------------------------------------------------------------------------------------------*\
  File:         menuSerial.c

  Purpose:      Functions to run for the general boot menu options

  Author:       daniel.calcoen@cern.ch

  Notes:
                These menues belongs to the [Manual Test] part and [Self Test] part

  History:

    13/04/04    stp     Created
    22/11/06    pfr     Ported for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <iodefines.h>
#include <stdio.h>
#include <dev.h>
#include <mcuDependent.h>
#include <menu.h>
#include <sleep.h>
#include <term.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuMTserialScopeSend(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
   communicates with UART1 - SCOPE (?)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t tx_char;
    uint8_t rx_char;
    uint16_t timeout;

    if (MenuGetInt16U(argv[0], 0, 0xFF, &tx_char))
    {
        return;
    }

    rx_char  = SCI1.RDR;        // Flush receive buffer
    SCI1.TDR = (uint8_t)tx_char;  // Send char


    // there is no "end of reception flag" in the uart registers
    // we can scan the SCI1 RXI interrupt flag or use directly the interrupt

    // SCI1_RXI1 - vector 219 - INT_Excep_SCI1_RXI1()

    // Receive char
    timeout = 100;
    ICU.IR219.BIT.IR = 0;       // Clear the interrupt request flag

    while (timeout-- != 0)      // Wait for 100ms to receive a char
    {
        if (ICU.IR219.BIT.IR == 1)      // interrupt generated? (equivalent to char received)
        {
            ICU.IR219.BIT.IR = 0;       // Clear the interrupt request flag
            rx_char = SCI1.RDR;
            break;
        }
        else
        {
            USLEEP(1000); // 1ms
        }
    }

    fprintf(term.f, RSP_DATA ",Tx: %c\n", (uint8_t)tx_char);

    if (timeout != 0)
    {
        fprintf(term.f, RSP_DATA ",Rx: %c\n", rx_char);
    }
    else
    {
        fprintf(term.f, RSP_DATA ",No char received\n");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTserialScopeCtrl(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
    setup UART ???
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  ctrl;

    if (MenuGetInt16U(argv[0], 0, 0x7, &ctrl))
    {
        return;
    }

    /*
        prc2  = 1;          // Unlock pd9
        pd9_5 = 0x01;       // p9_5 output, CLK4 - MCU_CLK4

        pd12_0 = 0x01;      // p12_0 output, TXD6 - MCU_TXD6
        pd12_1 = 0x01;      // p12_1 output, CLK6 - MCU_CLK6
        pd12_2 = 0x00;      // p12_1 input,  RXD6 - MCU_RXD6

        p9_5   = ctrl & 0x01; // CLK4 - MCU_CLK4
        p12_0  = ctrl & 0x02; // TXD6 - MCU_TXD6
        p12_1  = ctrl & 0x04; // CLK6 - MCU_CLK6
    */
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTserialVsSend(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
   communicates with UART1 - POWER CONVERTER (?)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t tx_char;
    uint8_t rx_char;
    uint16_t timeout;

    if (MenuGetInt16U(argv[0], 0, 0xFF, &tx_char))
    {
        return;
    }

    rx_char  = SCI2.RDR;        // Flush receive buffer
    SCI2.TDR = (uint8_t)tx_char;  // Send char

    // there is no "end of reception flag" in the uart registers
    // we can scan the SCI1 RXI interrupt flag or use directly the interrupt

    // SCI2_RXI2 - vector 223 - INT_Excep_SCI2_RXI2()

    // Receive char
    timeout = 100;
    ICU.IR223.BIT.IR = 0;       // Clear the interrupt request flag

    while (timeout-- != 0)      // Wait for 100ms to receive a char
    {
        if (ICU.IR223.BIT.IR == 1)      // interrupt generated? (equivalent to char received)
        {
            ICU.IR223.BIT.IR = 0;       // Clear the interrupt request flag
            rx_char = SCI2.RDR;
            break;
        }
        else
        {
            USLEEP(1000); // 1ms
        }
    }

    fprintf(term.f, RSP_DATA ",Tx: %c\n", (uint8_t)tx_char);

    if (timeout)
    {
        fprintf(term.f, RSP_DATA ",Rx: %c\n", rx_char);
    }
    else
    {
        fprintf(term.f, RSP_DATA ",Rx: None\n");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTserialVsCtrl(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  ctrl;

    if (MenuGetInt16U(argv[0], 0, 1, &ctrl))
    {
        return;
    }

    /*
        pd15_1 = 0x01;      // p15_1 output, CLK5 - MCU_CLK5
        p15_1  = ctrl;      // CLK5 - MCU_CLK5
    */
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTserialScope(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
   Test Serial Scope
\*---------------------------------------------------------------------------------------------------------*/
{

    uint8_t rx_char;
    uint16_t timeout;

    if (FGC_CRATE_TYPE_RECEPTION != dev.crate_type)
    {
        MenuRspError("Not implemented on this backplane type:0x%04X", dev.crate_type);
        return;
    }
    else
    {
        // under the reception crate Tx is linked to Rx
        // so send 2 chars and check they are properly received

        rx_char  = SCI1.RDR;    // Flush receive buffer
        SCI1.TDR = (uint8_t)0xA5; // Send char

        // there is no "end of reception flag" in the uart registers
        // we can scan the SCI1 RXI interrupt flag or use directly the interrupt

        // SCI1_RXI1 - vector 219 - INT_Excep_SCI1_RXI1()

        // Receive char
        timeout = 100;
        ICU.IR219.BIT.IR = 0;   // Clear the interrupt request flag

        while (timeout-- != 0)          // Wait for 100ms to receive a char
        {
            if (ICU.IR219.BIT.IR == 1)          // interrupt generated? (equivalent to char received)
            {
                ICU.IR219.BIT.IR = 0;   // Clear the interrupt request flag
                rx_char = SCI1.RDR;
                break;
            }
            else
            {
                USLEEP(1000); // 1ms
            }
        }

        if (timeout == 0)
        {
            MenuRspError("Uart timeout char 1");
            return;
        }

        if (rx_char != 0xA5)
        {
            MenuRspError("timeout:%d got:%c exp:%c", timeout, rx_char, 0xa5);
            return;
        }

        // -----------------------------------------------------------------------------------------

        SCI1.TDR = (uint8_t)0x5A; // Send char

        // there is no "end of reception flag" in the uart registers
        // we can scan the SCI1 RXI interrupt flag or use directly the interrupt

        // SCI1_RXI1 - vector 219 - INT_Excep_SCI1_RXI1()

        // Receive char
        timeout = 100;
        ICU.IR219.BIT.IR = 0;   // Clear the interrupt request flag

        while (timeout-- != 0)          // Wait for 100ms to receive a char
        {
            if (ICU.IR219.BIT.IR == 1)          // interrupt generated? (equivalent to char received)
            {
                ICU.IR219.BIT.IR = 0;   // Clear the interrupt request flag
                rx_char = SCI1.RDR;
                break;
            }
            else
            {
                USLEEP(1000); // 1ms
            }
        }

        if (timeout == 0)
        {
            MenuRspError("Uart timeout char 2");
            return;
        }

        if (rx_char != 0x5A)
        {
            MenuRspError("timeout:%d got:%c exp:%c", timeout, rx_char, 0x5A);
            return;
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*/

void MenuSTserialVS(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
   Test Serial dig VS interface
\*---------------------------------------------------------------------------------------------------------*/
{

    //  uint8_t rx_char;
    //  uint16_t timeout;

    if (FGC_CRATE_TYPE_RECEPTION != dev.crate_type)
    {
        MenuRspError("Not implemented on this backplane type:0x%04X", dev.crate_type);
        return;
    }
    else
    {
        // Access directly I/O
        // DIGVS_RX = DIGVS_TX xor DIVVS_CLK

        USLEEP(1000); // 1ms

        //        if (0 != 0)
        //        {
        //            MenuRspError("timeout:%d got:%c exp:%c", timeout, rx_char, 0x5a);
        //            return;
        //        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuSerial.c
\*---------------------------------------------------------------------------------------------------------*/
