/*---------------------------------------------------------------------------------------------------------*\
  File:     fieldbus_boot.c

  Purpose:  Common Network Interface Functions
            Wraper functions to access the right hardware, FIP or ethernet

\*---------------------------------------------------------------------------------------------------------*/

#define FIELDBUS_BOOT_GLOBALS

#include <fieldbus_boot.h>      // for stat_var global variable
#include <codes.h>              // for code global variable
#include <dev.h>                // for dev global variable
#include <ethernet_boot.h>      // for EthISR()
#include <fgc_runlog_entries.h> // for FGC_RL_
#include <lan92xx.h>            // for Lan92xxInit()
#include <master_clk_pll.h>     // for GATEWAY_ALIVE_TIMEOUT_MS
#include <memmap_mcu.h>         // for SM_UXTIME_P, UTC_*
#include <runlog_lib.h>         // for RunlogWrite()
#include <sharedMemory.h>      // for shared_mem global variable
#include <string.h>
#include <term_boot.h>

/*---------------------------------------------------------------------------------------------------------*/
enum FGC_eth_status FieldbusInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main() and CheckStatus() to prepare the network interface.
\*---------------------------------------------------------------------------------------------------------*/
{
    stat_var.fgc_stat.class_data.CLASS_FIELD.state_op = FGC_OP_BOOT;    // Initialise operational state

    fieldbus.progress_refresh_rate = 800;

    const enum FGC_eth_status status = EthInit();

    stat_var.fieldbus_stat.class_id = FGC_CLASS_ID;

    if(devIdIsValid())
    {
        RunlogWrite(FGC_RL_FIP_ID, (void *)&dev.fieldbus_id);
    }

    CodesInitAdvTable();

    return status;
}

/*---------------------------------------------------------------------------------------------------------*/
void FieldbusEnableInterrupts(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function enables network interrupt
\*---------------------------------------------------------------------------------------------------------*/
{
    if(Lan92xxIsRxOverrun() || Lan92xxIsRxError())
    {
        Lan92xxRxDump();
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void FieldbusRxTime(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function process the time received data
\*---------------------------------------------------------------------------------------------------------*/
{
    // to say GW is present 2 consecutive packets are required

    if ((fieldbus.is_gateway_online == FALSE) && (fieldbus.gw_timeout_ms != 0))
    {
        fieldbus.is_gateway_online = TRUE;                  // Mark gateway as online

        if (shared_mem.power_on_utc == 0)               // If Power on time never set
        {
            shared_mem.power_on_utc = time_var.unix_time;
        }

        // Log gw state in run log
        RunlogWrite(FGC_RL_UXTIME, (void *) &time_var.unix_time);
        RunlogWrite(FGC_RL_MSTIME, (void *) &time_var.ms_time);
        RunlogWrite(FGC_RL_GW_STATE, &fieldbus.is_gateway_online);
    }

    // Update UTC registers
    if (time_var.ms_time > 980)
    {
        UTC_SET_UNIXTIME_P = time_var.unix_time + 1;
        UTC_SET_MS_P = 0;
    }
    else
    {
        UTC_SET_UNIXTIME_P = time_var.unix_time;
        UTC_SET_MS_P = ((uint16_t)(time_var.ms_time / 20) * 20) + 20;
    }

    // Update local_ms. local_ms is used if no FPGA
    main_misc.local_ms = time_var.ms_time; // ToDo /!\ unsafe write of global variable

    if (time_var.runlog_idx != fieldbus.last_runlog_idx)    // If runlog_idx has changed
    {
        stat_var.fieldbus_stat.ack   |= FGC_ACK_TIME;               // Ack time reception
        fieldbus.last_runlog_idx      = time_var.runlog_idx;        // Remember new runlog index

        // this is the byte by byte transmission of the runlog to the gateway
        stat_var.fieldbus_stat.runlog = NVRAM_RL_BUF_A[time_var.runlog_idx];  // Return one byte from runlog

        if (time_var.fgc_id == dev.fieldbus_id)                 // If remote terminal kbd char received
        {
            TermRxCh(time_var.fgc_char);                         // Submit ch to rx buffer
        }

        if (time_var.adv_list_idx < FGC_CODE_MAX_SLOTS)         // If adv list index is in valid range
        {
            CodesUpdateAdvCode();
        }

        fieldbus.gw_timeout_ms = GATEWAY_ALIVE_TIMEOUT_MS;      // Reset gw timeout counter
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void FieldbusRxCode(uint16_t nb_code_block, uint8_t p_code[][FGC_CODE_BLK_SIZE])
/*---------------------------------------------------------------------------------------------------------*\
    This function process the received code
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t ii;

    if(time_var.code_var_id < FGC_NUM_CODES)
    {
        // For each code block in the code variable

        for (ii = 0; ii < nb_code_block; ii++)
        {
            if(time_var.code_var_idx < fgc_code_sizes_blks[time_var.code_var_id])
            {
                struct code_block * const block = CodesRxBufferGetNextFreeBlock();

                if(block == NULL)
                {
                    return;
                }

                // Copy code header from time var

                block->class_id  = time_var.code_var_class_id;
                block->code_id   = time_var.code_var_id;
                block->block_idx = time_var.code_var_idx++;

                // Copy into the buffer the code contained in code_var

                memcpy(&block->code, p_code[ii], FGC_CODE_BLK_SIZE);

                CodesRxBufferMakeNextAvailable();
            }
        }
    }
}


// TODO for fgc2, done partly done in pll_boot.c GatewayAliveAndPllAlgoBlocking(). See if it can be merge
/*---------------------------------------------------------------------------------------------------------*/
void FieldbusCheckGatewayAlive(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    // check if we lost the gateway for a long time
    if (fieldbus.gw_timeout_ms != 0)
    {
        fieldbus.gw_timeout_ms--;
    }
    else
    {
        // we know we lost the gateway a long time ago
        // if it is the 1st time we enter here block the pll and put an entry in the runlog

        // to say GW is present 2 consecutive packets are required
        if (fieldbus.is_gateway_online == TRUE)
        {
            fieldbus.is_gateway_online = FALSE;                             // Clear GW active flag
            RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));    // Unix time
            RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));          // Millisecond time
            RunlogWrite(FGC_RL_GW_STATE, &fieldbus.is_gateway_online);      // Log gw state in run log
        }
    }
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: fieldbus_boot.c
\*---------------------------------------------------------------------------------------------------------*/
