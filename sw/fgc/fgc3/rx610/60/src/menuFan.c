/*---------------------------------------------------------------------------------------------------------*\
  File:         menuFan.c

  Purpose:      Self Tests menu functions

  Author:       daniel.calcoen@cern.ch

  Notes:
                These menus belongs to the [Self Test] part

  History:

    12 dec 2007    doc  Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <iodefines.h>
#include <stdio.h>

#include <dev.h>
#include <fan.h>
#include <mcuDependent.h>
#include <memmap_mcu.h>
#include <menu.h>
#include <sleep.h>
#include <term.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuSTfan(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function sets the fan speed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t fan_ctrl;
    uint16_t fan_ct;
    uint16_t fan_ct_prev;

    fan_ctrl = FAN_P;           // Save fan CTRL register
    FAN_P = 0;                  // Turn off fan
    MSLEEP(100);                // Wait for the fan to stop

    if (FGC_CRATE_TYPE_RECEPTION != dev.crate_type)
    {
        if (FAN_TACHY_P != 0)       // If fan still running
        {
            MenuRspError("FAN still working");
        }

        FAN_P = 0xFFFF;             // Turn on fans, full speed
        MSLEEP(100);                // Wait for the fan to start
        fan_ct = FanRead();         // 1s duration

        if (fan_ct < 3000)          // Full speed is about 3000 rpm
        {
            MenuRspError("FAN speed to low: %u rpm", fan_ct);
        }

        FAN_P = fan_ctrl;           // Set back saved ctrl value
        return;
    }
    else
    {
        // Reception backplane. No Fan, fan command is linked to tachymeter.
        FAN_P = 0x8000;        // anything  <0x93ce nor 0x0000 speed (flat signals)
        //50Hz signal
        fan_ct_prev = FAN_TACHY_P;
        MSLEEP(999); // Wait for barely 50 periods -> 49 pulses
        fan_ct = FAN_TACHY_P;

        FAN_P = fan_ctrl;               // Set back saved ctrl value
        return;

        if (fan_ct - fan_ct_prev != 49)
        {
            MenuRspError("FAN read back got:0x%04X exp:0x%04X", fan_ct, fan_ct_prev + 49);
            return;
        }
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTfanCtrl(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function sets the fan speed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  fan_ctrl;

    if (MenuGetInt16U(argv[0], 0, 0xffff, &fan_ctrl))
    {
        return;
    }

    FAN_P = fan_ctrl;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTfanRead(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function reads the fan speed.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  fan_ct;

    fan_ct = FanRead();         // 1s duration

    fprintf(term.f, RSP_DATA",Tachy : %u\n", fan_ct);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuFan.c
\*---------------------------------------------------------------------------------------------------------*/
