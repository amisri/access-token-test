/*---------------------------------------------------------------------------------------------------------*\
  File:         menuM16C62.c

  Purpose:      Test the Renesas M16C62 managing the Dallas 1-wire ID bus

  Author:       daniel.calcoen@cern.ch

  Notes:
                These menus belongs to the [Manual Test] part and [Self Test] part

  History:

    01 jun 2005    qak  Created
    12 may 2007    pfr  ported for FGC3

\*---------------------------------------------------------------------------------------------------------*/

#include <m16c62.h>             // for C62PowerOn()
#include <iodefines.h>          // specific processor registers and constants
#include <stdio.h>              // for fprintf()
#include <codes.h>              // for CODE_M16C62_MAX_INITS
#include <debug.h>              // for DEBUG_M16C62_STANDALONE
#include <dev.h>                // for dev global variable
#include <macros.h>             // for Test(), Set()
#include <mcuDependent.h>      // for MSLEEP(), USLEEP()
#include <memmap_mcu.h>         // Include memory map consts
#include <menu.h>               // for MenuGetInt16U(), MenuRspError(), menu global variable
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <renesas_bl.h>         // for RenesasBootLoaderFlash()
#include <sleep.h>
#include <term_boot.h>           // for TermPutc()
#include <term.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuMTm16C62Power(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This function will turn on the M16C62 and will check that the M16C62 is responding

  0 = off
  1 = on with initial bus value C62_SWITCH_PROG
  2 = on with initial bus value Not(C62_SWITCH_PROG)

\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      choice;
    uint16_t      pwr;

    if ((*argv[0] != 0) && (MenuGetInt16U(argv[0], 0, 2, &pwr) != 0))
    {
        return;
    }

    if (pwr != 0)
    {
        // when M16C62 boots if bus_initial_value is C62_SWITCH_PROG the M16C62 will remain in IdBoot
        // otherwise it will run IdProg which in turn will run the its monitor
        if (pwr == 1)
        {
            choice = C62_SWITCH_PROG;
        }
        else
        {
            // tell IdBoot to run IdProg which in turn will run its monitor
            choice = ~C62_SWITCH_PROG;
        }

        // we are working with the "word" (16 bits) that also contains the control part
        choice &= ~C62_PROG_MASK16; // clear PROG bit that is not READ ONLY!!! in the HI of the word

        fprintf(term.f, RSP_DATA "startup value : 0x%04X\n", choice);

        if (C62PowerOn(choice) != 0)
        {
        }
    }
    else
    {
        C62PowerOff();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTm16C62DebugLevel(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]

  [0..3]
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      parameter;
    uint16_t      stat;
    uint8_t       debug_level;

    if ((*argv[0] != 0) && (MenuGetInt16U(argv[0], 0, 3, &parameter) != 0))
    {
        return;
    }
    else
    {
        debug_level = (uint8_t) parameter;

        stat = C62SendIsrCmd(C62_SET_DEBUG_LEVEL, 1, 0, &debug_level);

        if (stat != 0)
        {
            fprintf(term.f, RSP_DATA "couldn't change debug level\n");
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTm16C62BootFlash(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This will flash the M16C62 with IdBootProg program
  through a serial link, the UART6 of the Rx610 connected to the UART1 of M16C62
  (development port of M16C62) and M16C62 booting in "programming" mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t stat;

    // prepare M16C62 to boot in "programming" mode

    if ((C62_P & C62_CMDBUSY_MASK16) != 0)              // if still busy
    {
        // 2 seconds delay before OFF to let the terminal print their debug info
        MSLEEP(2000);
    }

    CPU_RESET_CTRL_P |=  CPU_RESET_CTRL_C62OFF_MASK16;  // Turn off M16C62
    MSLEEP(30);                                         // Wait for 30ms

    // LO of C62_P is data and the HI bits (except PROG) are read-only
    // writing the register fires the reset of DATARDY and CMDERR bits in the PLD internal register
    // and the set of CMDBUSY
    C62_P = C62_PROG_MASK16;                            // PROG it to 0, Set M16C62 in prog mode
    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_C62OFF_MASK16;  // Turn on M16C62

    // Wait for the M16C62 to start

    MSLEEP(100);                                        // Wait for 100ms

    // Flash boot, only possible in program (Boot) mode

    stat = RenesasBootLoaderFlash(); // this will first erase ALL flash blocks

    if ((C62_P & C62_CMDBUSY_MASK16) != 0)              // if still busy
    {
        // 2 seconds delay before OFF to let the terminal print their debug info
        MSLEEP(2000);
    }

    // LO of C62_P is data and the HI bits (except PROG) are read-only
    // writing the register fires the reset of DATARDY and CMDERR bits in the PLD internal register
    // and the set of CMDBUSY
    C62_P = 0x00;                                       // PROG bit to 0, Set M16C62 in normal mode

    // Reset M16C62
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_C62OFF_MASK16;   // Turn off M16C62

    if (stat != 0)
    {
        MenuRspError("M16C62 boot flash failed");
    }
    else
    {
        MSLEEP(30);                                     // Wait for 30ms for the OFF

        // REMEMBER!!! that PROG bit is not READ ONLY!!! (for the FGC3)

        // writing the register fires the reset of DATARDY and CMDERR bits in the PLD internal register
        // and the set of CMDBUSY

        // when M16C62 boots if bus_initial_value is C62_SWITCH_PROG the M16C62 will remain in IdBoot
        // otherwise it will run IdProg which in turn will run the its monitor

        C62_P = C62_SWITCH_PROG & (~C62_PROG_MASK16);       // clear PROG bit that is not READ ONLY!!!

        CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_C62OFF_MASK16;  // Turn on M16C62

        // This part will run only when there were 3 failed attempts to flash the M16C62 boot
        // It means that MenuMTm16C62BootFlash function was run manually
        // This resets the failure counter if flashing the boot succeeded
        // See also CodesInitM16C62 function where the counter is incremented

        if (NVRAM_M16C62_INIT_FAILURES_P > CODE_M16C62_MAX_INITS)
        {
            NVRAM_UNLOCK();
            NVRAM_M16C62_INIT_FAILURES_P = 0;
            NVRAM_LOCK();
        }
    }


    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_GLOBAL_MASK16;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTm16C62SwitchProg(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  This will run the C62_SWITCH_PROG command to switch between the M16C62 IdBoot and IdProg (and
   back again).
\*---------------------------------------------------------------------------------------------------------*/
{
    C62SwitchProg();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTm16C62MonitorViaSlowCmd(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] Run ID Program monitor via a Slow Command
  the M16C62 "monitor" terminal is also copied to the MCU terminal
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;
    uint8_t       ch;
    uint16_t      err_number;

#ifndef DEBUG_M16C62_STANDALONE

    if (((C62_P & C62_POWER_MASK16) != 0) && (C62PowerOff() != 0))      // If Power Off fails
    {
        return;                                         // Return immediately
    }

#endif

    // when M16C62 boots if bus_initial_value is C62_SWITCH_PROG the M16C62 will remain in IdBoot
    // otherwise it will run IdProg which in turn will run the its monitor
    if (C62PowerOn(C62_SWITCH_PROG) != 0)               // If Power On fails
    {
        return;                                         // Return immediately
    }

    // switch from IdBoot to IdProg
    if (C62SwitchProg() != 0)                           // If switch to ID program fails
    {
        return;                                         // Return immediately
    }

    menu.response_buf_pos = 0;                          // Clear response from C62SwitchProg()

    if (C62SendBgCmd(C62_RUN_MONITOR, 2) != 0)          // Try to run the monitor, timeout 2ms
    {
        return;
    }

    while (dev.abort_f == false)                        // While CTRL-C not yet pressed
    {
        OS_ENTER_CRITICAL();                            // Disable interrupts, except timers that are level 7

        if (term.rx.n_ch != 0)                          // If character received
        {
            ch = term.rx.buf[term.rx.out++];            // Get character from the rx buffer
            term.rx.n_ch--;                             // Decrement the buffer occupancy counter

            if (term.rx.out == TERM_RX_BUF_SIZE)
            {
                term.rx.out = 0;
            }

            OS_EXIT_CRITICAL();                         // Enable interrupts

            if (C62SendIsrCmd(C62_SET_KBD_CHAR, 1, 0, &ch) != 0)        // If transmission of kbd char fails
            {
                break;                                  // Quit communication with M16C62
            }
        }
        else                                            // else
        {
            OS_EXIT_CRITICAL();                         // Enable interrupts
        }

        err_number = C62SendByte(C62_GET_TERM_CHAR, &ch);

        if (err_number != 0)    // If read of term char fails
        {
            MenuRspError("C62_GET_TERM_CHAR failed:%u", err_number);    // Report error
            break;                                      // Quit communication with M16C62
        }

        if (ch != 0)                                    // If a new M16C62 term char received
        {
            TermPutc(ch, term.f);                        // Write to HC16 terminal
        }

        USLEEP(600);                                    // Sleep for 600us
    } // end while

    if ((C62_P & C62_POWER_MASK16) != 0)                // If M16C62 is still on
    {
        C62PowerOff();                                  // Power Off M16C62
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTm16C62MonitorViaStartup(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  turn off and on the M16C62 with a startup value different to C62_SWITCH_PROG
  tu pass through IdBoot and starts automatically the Monitor in IdProg
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;
    uint8_t       ch;
    uint16_t      err_number;
    uint16_t      initial_value;

    // 0 and 2 are good answers
    if ((C62PowerOff() & 0xFFFD) != 0)                  // If Power Off fails
    {
        return;                                         // Return immediately
    }

    // when M16C62 boots if bus_initial_value is C62_SWITCH_PROG the M16C62 will remain in IdBoot
    // otherwise it will run IdProg which in turn will run the its monitor

    // prepare data to be DIFFERENT than C62_SWITCH_PROG
    // so IdBoot will run IdProg which in turn detects that bus_initial_value is NOT C62_SWITCH_PROG
    // and runs SaMonitor()

    initial_value = ~C62_SWITCH_PROG;
    initial_value &= ~C62_PROG_MASK16; // clear PROG bit that is not READ ONLY!!!

    if (C62PowerOn(initial_value) != 0)                 // If an attempt to start IdBoot fails
    {
        return;                                         // Return immediately
    }

    while (dev.abort_f == false)                        // While CTRL-C not yet pressed
    {
        OS_ENTER_CRITICAL();                            // Disable interrupts, except timers that are level 7

        if (term.rx.n_ch != 0)                          // If character received
        {
            ch = term.rx.buf[term.rx.out++];            // Get character from the rx buffer
            term.rx.n_ch--;                             // Decrement the buffer occupancy counter

            if (term.rx.out == TERM_RX_BUF_SIZE)
            {
                term.rx.out = 0;
            }

            OS_EXIT_CRITICAL();                         // Enable interrupts

            if (C62SendIsrCmd(C62_SET_KBD_CHAR, 1, 0, &ch) != 0)        // If transmission of kbd char fails
            {
                break;                                  // Quit communication with M16C62
            }
        }
        else                                            // else
        {
            OS_EXIT_CRITICAL();                         // Enable interrupts
        }

        err_number = C62SendByte(C62_GET_TERM_CHAR, &ch);

        if (err_number != 0)    // If read of term char fails
        {
            MenuRspError("C62_GET_TERM_CHAR failed:%u", err_number);    // Report error
            break;                                      // Quit communication with M16C62
        }

        if (ch != 0)                                    // If a new M16C62 term char received
        {
            TermPutc(ch, term.f);                        // Write to HC16 terminal
        }

        USLEEP(600);                                    // Sleep for 600us
    } // end while

    if ((C62_P & C62_POWER_MASK16) != 0)                // If M16C62 is still on
    {
        C62PowerOff();                                  // Power Off M16C62
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTm16C62startupValue(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  This function set value of M16C62 (Dallas) Port 2 to C62_SWITCH_PROG, the one that is read at
  startup, handy for start debugger or after reset when in standalone.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      choice = 0;

    if ((*argv[0] != 0) && (MenuGetInt16U(argv[0], 0, 1, &choice) != 0))
    {
        return;
    }

    if (choice == 0)
    {
        // when M16C62 boots if bus_initial_value is C62_SWITCH_PROG the M16C62 will remain in IdBoot
        // otherwise it will run IdProg which in turn will run the its monitor

        // REMEMBER!!! that PROG bit is not READ ONLY!!! (for the FGC3)

        // writing the register fires the reset of DATARDY and CMDERR bits in the PLD internal register
        // and the set of CMDBUSY
        C62_P = C62_SWITCH_PROG;
    }
    else
    {
        // tell IdBoot to run IdProg
        choice = ~C62_SWITCH_PROG;
        choice &= ~C62_PROG_MASK16;     // clear PROG bit that is not READ ONLY!!!

        // writing the register fires the reset of DATARDY and CMDERR bits in the PLD internal register
        // and the set of CMDBUSY
        C62_P = choice;                 // Send byte to M16C62
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuM16C62.c
\*---------------------------------------------------------------------------------------------------------*/
