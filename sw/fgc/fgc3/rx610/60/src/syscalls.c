/*---------------------------------------------------------------------------------------------------------*\
  File:         syscalls.c

  Purpose:      NewLib system calls

  Version:      0.0

  Author:       Daniel.Calcoen@cern.ch

  Notes:        NewLib system calls

  History:

    15 apr 09   doc     1st implementation.


  Method 1 (there 2 more ways of doing this, read reent.h of NewLib)

    Define the reentrant versions of the syscalls directly.
    (eg: _open_r, _close_r, etc.).  Please keep the namespace clean.
    When you do this, set "syscall_dir" to "syscalls" and add
    -DREENTRANT_SYSCALLS_PROVIDED to newlib_cflags in configure.host.


 based on the article by Bill Gatliff at Embedded.com - The Official Site of the Embedded Development Community

   Embedding GNU: Newlib, Part 2 - ( Embedded Systems Design, Porting newlib )


   also _exit is needed, in the FGC3 case it is implemented in assembler in the start-up file start.S

\*---------------------------------------------------------------------------------------------------------*/

#define SYSCALLS_GLOBALS        // for malloc_mngr[] global variable

#include <syscalls.h>
#include <stdint.h>   // basic typedefs
#include <sys/stat.h>   // for struct stat
#include <errno.h>      // for ENODEV, etc...
#include <stdio.h>      // for FILE
#include <stdlib.h>     // for exit()
#include <string.h>     // for strcmp()
#include <term_boot.h>   // for TermPutc()
#include <term.h>


/*---------------------------------------------------------------------------------------------------------*/
void InitMalloc(void)
/*---------------------------------------------------------------------------------------------------------*\
  ToDo: NewLib uses malloc to allocate the reentrant structure (1 per thread)
  analyse how to deal with this
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t       ii;

    for (ii = 0; ii < NEWLIB_MALLOC_BLOCK_COUNT; ii++)
    {
        malloc_mngr[ii].free = true;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
int _open_r(struct _reent * ptr, _CONST char * file, int flags, int mode)
/*---------------------------------------------------------------------------------------------------------*\
  Open a file.
  the reentrant version of open. It takes a pointer to the global data block, which holds errno.

  This stub's primary purpose is to translate a string device or file "name" to a file descriptor.
  With the exception of the standard input, standard output, and standard error devices, this function
  can also be used to provide advance notice of an impending write() or read() request.

  You can safely ignore the _open_r stub's flags and mode parameters, because they're only of interest
  when talking to a true filesystem.

  library standard is in  openr.c
\*---------------------------------------------------------------------------------------------------------*/
{
    int         fd      = -1;
    // fd = 0 = standard input (reserved)
    // fd = 1 = standard output (reserved)
    // fd = 2 = standard error (reserved)

    if (strcmp("tcm", file) == 0)    // used by FG3 Main Program
    {
        fd = 3;
    }
    else
    {
        if (strcmp("rtd", file) == 0)    // used by FG3 Main Program
        {
            fd = 4;
        }
        else
        {
            if (strcmp("term", file) == 0)    // used by FG3 Boot Program
            {
                fd = 5;
            }
        }
    }


    if (fd != -1)   // we found the device ?
    {
        // nothing more to do
    }
    else // it doesn't exist!
    {
        ptr->_errno = ENODEV;
    }

    return (fd);
}
/*---------------------------------------------------------------------------------------------------------*/
//int _open64_r (void *reent, const char *file, int flags, int mode)
/*---------------------------------------------------------------------------------------------------------*\
    the reentrant version of open64. It takes a pointer to the global data block, which holds errno.
\*---------------------------------------------------------------------------------------------------------*/
/*
{
}
*/
/*---------------------------------------------------------------------------------------------------------*/
int _close_r(struct _reent * ptr, int fd)
/*---------------------------------------------------------------------------------------------------------*\
  Close a file.
  the reentrant version of close. It takes a pointer to the global data block, which holds errno.

  library standard is in closer.c
\*---------------------------------------------------------------------------------------------------------*/
{
    return (0);  // nothing to do, they give as minimal implementation return(-1) ???
}
/*---------------------------------------------------------------------------------------------------------*/
_ssize_t _write_r(struct _reent * ptr, int fd, _CONST _PTR buf, size_t cnt)
/*---------------------------------------------------------------------------------------------------------*\
  Write to a file.
  libc subroutines will use this system routine for output to all files, including stdout
  so if you need to generate any output, for example to a serial port for debugging, you should make
  your minimal write capable of doing this.

  the reentrant version of write. It takes a pointer to the global data block, which holds errno.

  library standard is in  writer.c
  must return the number of characters written

  return 0 or negative indicates an error
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      written         = cnt;
    uint16_t      ch;
    FILE    *   serial_stream   = 0;


    // fd 0,1,2 are reserved

    switch (fd)
    {
        case 3: // tcm
            //          serial_stream = tcm.f;
            break;

        case 4: // rtd
            //          serial_stream = rtd.f;
            break;

        case 0: // 0 = standard input
        case 1: // 1 = standard output
        case 2: // 2 = standard error
        case 5: // term
            serial_stream = term.f;
            break;

        default :
            return (0); // error, 0 bytes written
    }

    // with newlib, when nonbuffered, we receive parts of the string (max 16 chars = BUFSIZ)
    // so this detection of string ending is not correct
    //  while ((ch != 0) && (err_code == 0))

    while (cnt > 0)
    {
        ch =  * ((char *) buf);
        buf++;
        TermPutc(ch, serial_stream);
        cnt--;
    }

    // must return the number of characters written as we don't handle any error and there is no condition
    // to no send cnt chars we return always cnt

    return (written);
}
/*---------------------------------------------------------------------------------------------------------*/
_ssize_t _read_r(struct _reent * ptr, int fd, _PTR buf, size_t cnt)
/*---------------------------------------------------------------------------------------------------------*\
__attribute__ ((noreturn))
  Read from a file.
  the reentrant version of read. It takes a pointer to the global data block, which holds errno.

  library standard is in readr.c
\*---------------------------------------------------------------------------------------------------------*/
{
    exit(2); // debugging, hang the software

    return (0);  // nothing to do
}
/*---------------------------------------------------------------------------------------------------------*/
//int _stat_r (struct _reent * ptr, _CONST char * file, struct stat * pstat)
/*---------------------------------------------------------------------------------------------------------*\
  Status of a file (by name).
  the reentrant version of stat. It takes a pointer to the global data block, which holds errno.

  library standard is in  statr.c

  we'll just tell the caller that the requested file or descriptor is a character device
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    pstat->st_mode = S_IFCHR;
    return(0);
}
*/
/*---------------------------------------------------------------------------------------------------------*/
int _fstat_r(struct _reent * ptr, int fd, struct stat * pstat)
/*---------------------------------------------------------------------------------------------------------*\
  Status of an open file.
  For consistency with other minimal implementations in these examples, all files are regarded as character
  special devices.
  The sys/stat.h header file required is distributed in the include subdirectory for this C library.
  the reentrant version of fstat. It takes a pointer to the global data block, which holds errno.

  library standard is in  fstatr.c

  we'll just tell the caller that the requested file or descriptor is a character device
\*---------------------------------------------------------------------------------------------------------*/
{
    pstat->st_mode = S_IFCHR;
    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
//int _fstat64_r (struct _reent *ptr, int fd, struct stat *pstat)
/*---------------------------------------------------------------------------------------------------------*\
  the reentrant version of fstat64. It takes a pointer to the global data block, which holds errno.
\*---------------------------------------------------------------------------------------------------------*/
//{
//}
/*---------------------------------------------------------------------------------------------------------*/
_off_t _lseek_r(struct _reent * ptr, int fd, _off_t pos, int whence)
/*---------------------------------------------------------------------------------------------------------*\
__attribute__ ((noreturn))

  Set position in a file.
  the reentrant version of lseek. It takes a pointer to the global data block, which holds errno.

  library standard is in  lseekr.c

  we'll just pretend that the request is always successful
\*---------------------------------------------------------------------------------------------------------*/
{
    ptr->_errno = ENOTSUP; //  return "not supported"

    exit(3); // debugging, hang the software

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
//off_t _lseek64_r(void *reent, int fd, off_t pos, int whence)
/*---------------------------------------------------------------------------------------------------------*\
    the reentrant version of lseek64. It takes a pointer to the global data block, which holds errno.
\*---------------------------------------------------------------------------------------------------------*/
/*
{
}
*/
/*---------------------------------------------------------------------------------------------------------*\
  An unfortunate limitation of newlib's stdio library is that it requires at least a minimal malloc()
  for proper operation. When the printf() machinery detects a floating-point format specifier,
  it allocates a few bytes of memory to use as a workspace for constructing the text
  representation of the number. It's actually a poor design decision, because in other places,
  newlib statically allocates memory to accomplish the same thing. But that's the way it is.

  the internal buffer for _vfprintf_r is defined as 40 charactes so printf() expansions is smaller than this
  will not need malloc()

\*---------------------------------------------------------------------------------------------------------*/
void * _malloc_r(struct _reent * ptr, size_t n)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    void    *   alloc = NULL;
    bool     found;
    uint8_t       ii;

    found = false;

    if (n <= NEWLIB_MALLOC_BLOCK_SIZE)
    {
        ii = 0;

        do
        {
            if (malloc_mngr[ii].free == true)
            {
                malloc_mngr[ii].free = false;
                alloc = malloc_mngr[ii].data; // (void *)
                found = true;
            }

            ii++;
        }
        while ((ii < NEWLIB_MALLOC_BLOCK_COUNT) && (found == false));
    }
    else
    {
        // exits with found == false, due to the default
        // exit(); // debugging, hang the software
    }

    if (found == false)
    {
        exit(4); // debugging, hang the software
    }

    return (alloc);
}
/*---------------------------------------------------------------------------------------------------------*/
void * _realloc_r(struct _reent * ptr,  void * aptr, size_t n)
/*---------------------------------------------------------------------------------------------------------*\
__attribute__ ((noreturn))
\*---------------------------------------------------------------------------------------------------------*/
{
    ptr->_errno = ENOTSUP; //  return "not supported"

    exit(5); // debugging, hang the software

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
void _free_r(struct _reent * ptr, void * aptr)
/*---------------------------------------------------------------------------------------------------------*\
__attribute__ ((noreturn))
\*---------------------------------------------------------------------------------------------------------*/
{
    ptr->_errno = ENOTSUP; //  return "not supported"

    exit(6); // debugging, hang the software

}
/*---------------------------------------------------------------------------------------------------------*/
void * _calloc_r(struct _reent * ptr, size_t n, size_t s)
/*---------------------------------------------------------------------------------------------------------*\
  Use calloc to request a block of memory sufficient to hold an array of n elements, each of which has size s.
  The memory allocated by calloc comes out of the same memory pool used by malloc,
  but the memory block is initialized to all zero bytes.
\*---------------------------------------------------------------------------------------------------------*/
{
    void    *   alloc = NULL;
    bool     found;
    uint8_t       ii;
    uint16_t      xx;

    found = false;

    if ((n * s) <= NEWLIB_MALLOC_BLOCK_SIZE)
    {
        ii = 0;

        do
        {
            if (malloc_mngr[ii].free == true)
            {
                malloc_mngr[ii].free = false;
                alloc = malloc_mngr[ii].data; // (void *)
                found = true;

                for (xx = 0; xx < NEWLIB_MALLOC_BLOCK_SIZE; xx++)
                {
                    malloc_mngr[ii].data[xx] = 0;
                }
            }

            ii++;
        }
        while ((ii < NEWLIB_MALLOC_BLOCK_COUNT) && (found == false));
    }
    else
    {
        // exits with found == false, due to the default
        // exit(); // debugging, hang the software
    }

    if (found == false)
    {
        exit(7); // debugging, hang the software
    }

    return (alloc);
}
/*---------------------------------------------------------------------------------------------------------*/
//int _link_r (struct _reent * ptr, _CONST char * old, _CONST char * new)
/*---------------------------------------------------------------------------------------------------------*\
  Establish a new name for an existing file.
  the reentrant version of link. It takes a pointer to the global data block, which holds errno.

  library standard is in  linkr.c

  we'll claim that the operation always fails
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = EMLINK;
    return(-1);
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _unlink_r (struct _reent * ptr, _CONST char * file)
/*---------------------------------------------------------------------------------------------------------*\
  Remove a file's directory entry.
  the reentrant version of unlink. It takes a pointer to the global data block, which holds errno.
  library standard is in  unlinkr.c

  we'll claim that the operation always fails
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = EMLINK;       // also ENOENT can be used
    return(-1);
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _fork_r ( struct _reent *ptr )
/*---------------------------------------------------------------------------------------------------------*\
  Create a new process.
  the reentrant version of fork. It takes a pointer to the global data block, which holds errno.

  library standard is in  execr.c

  Newlib calls upon this stub to do the work for the fork() system call, which, in POSIX environments,
  is used to create a clone of the current processing context.
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported", also EAGAIN can be used
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _execve_r (struct _reent * ptr, char * name, char **argv, char **env)
/*---------------------------------------------------------------------------------------------------------*\
  Transfer control to a new process.
  library standard is in  execr.c

  context management-related stubs
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported", also ENOMEM can be used
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _kill_r (struct _reent * ptr, int pid, int sig)
/*---------------------------------------------------------------------------------------------------------*\
  Send a signal.
  library standard is in  signalr.c

  context management-related stubs
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported", also EINVAL can be used
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _wait_r ( struct _reent * ptr, int * status)
/*---------------------------------------------------------------------------------------------------------*\
  Wait for a child process.
  the reentrant version of wait. It takes a pointer to the global data block, which holds errno.

  library standard is in  execr.c

  context management-related stubs
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported", also ECHILD can be used
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//int _getpid_r (struct _reent * ptr)
/*---------------------------------------------------------------------------------------------------------*\
  Process-ID;
  this is sometimes used to generate strings unlikely to conflict with other processes.
  library standard is in  signalr.c

  context management-related stubs

  This function returns the context's unique process ID,
  which we can emulate using uC/OS's OSTaskQuery() function.

\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported"
    return -1;

    // use our priority as a task id
//    OSTaskQuery( OS_PRIO_SELF, &tcb );
//    id = tcb.OSTCBPrio;
      return(id);
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//_CLOCK_T_ _times_r (struct _reent * ptr, struct tms * ptms)
/*---------------------------------------------------------------------------------------------------------*\
  Timing information for current process.
  library standard is in  timesr.c

  This stub returns various times for the current context.
  uC/OS doesn't keep statistics on a task's run time, so we leave this unimplemented:

\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported"
    return -1;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//void * _sbrk_r (struct _reent * ptr, ptrdiff_t incr)
//char * _sbrk_r (void *          reent,  size_t incr)
/*---------------------------------------------------------------------------------------------------------*\
  Increase program data space.
  As malloc and related functions depend on this, it is useful to have a working implementation.
  The following suffices for a standalone system;
  it exploits the symbol _end automatically defined by the GNU linker.

  the reentrant version of sbrk. It takes a pointer to the global data block, which holds errno.

  library standard is in  sbrkr.c
\*---------------------------------------------------------------------------------------------------------*/
/*
{
    ptr->_errno = ENOTSUP; //  return "not supported"
    return(0);

    or can be implemented like

    extern char         _end;           // Defined by the linker
    static char *       heap_end;
    char *              prev_heap_end;

    if (heap_end == 0)
    {
      heap_end = &_end;
    }
    prev_heap_end = heap_end;
    if (heap_end + incr > stack_ptr)
    {
      write (1, "Heap and stack collision\n", 25);
      abort ();
    }

    heap_end += incr;
    return (caddr_t) prev_heap_end;
}
*/
/*---------------------------------------------------------------------------------------------------------*/
int _isatty_r(struct _reent * ptr, int fd)
/*---------------------------------------------------------------------------------------------------------*\
  Query whether output stream is a terminal.
  the reentrant version of isatty. It takes a pointer to the global data block, which holds errno.

  library standard is in isattyr.c

\*---------------------------------------------------------------------------------------------------------*/
{
    //  ptr->_errno = 0; //  no error
    return (1);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: syscalls.c
\*---------------------------------------------------------------------------------------------------------*/
