/*---------------------------------------------------------------------------------------------------------*\
  File:         mcu_flash.c

  Contents:     Rx610 Flash functions


  Some of the following functions MUST reside and be executed in RAM because the On-Chip ROM (program ROM)
  is disabled during the flash procedures, but if you are ONLY working with On-chip ROM (data flash)
  this is the one disabled and not the On-Chip ROM (program ROM) so those functions can reside and be
  executed in the program ROM.

  Remember to disable interrupts as interrupt vectors can't be read from the flash memory when it is disabled

  The following instructions cannot be used because the CPU tries to read data in the flash memory
    - UND instruction
    - INTO instruction
    - BRK instruction



  For the on-chip ROM (program ROM) the difference between the READ and the ERASE/WRITE address
  is the 0xFF or 0x00 in the high nibble

  The erase is by block
  The write is by 256 bytes

    0xFFFFE000  // EB00,   8Kb
    0xFFFFC000  // EB01,   8Kb
    0xFFFFA000  // EB02,   8Kb
    0xFFFF8000  // EB03,   8Kb
    0xFFFF6000  // EB04,   8Kb
    0xFFFF4000  // EB05,   8Kb
    0xFFFF2000  // EB06,   8Kb
    0xFFFF0000  // EB07,   8Kb

    0xFFFE0000  // EB08,  64Kb
    0xFFFD0000  // EB09,  64Kb
    0xFFFC0000  // EB10,  64Kb
    0xFFFB0000  // EB11,  64Kb                selection with FLASH2.FENTRYR.FENTRY0
    0xFFFA0000  // EB12,  64Kb
    0xFFF90000  // EB13,  64Kb
    0xFFF80000  // EB14,  64Kb
    0xFFF70000  // EB15,  64Kb
    0xFFF60000  // EB16,  64Kb

    0xFFF40000  // EB17, 128Kb
    0xFFF20000  // EB18, 128Kb
    0xFFF00000  // EB19, 128Kb
   --------------------------------------- 1Mb boundary
    0xFFEE0000  // EB20, 128Kb
    0xFFEC0000  // EB21, 128Kb
    0xFFEA0000  // EB22, 128Kb
    0xFFE80000  // EB23, 128Kb
    0xFFE60000  // EB24, 128Kb                selection with FLASH2.FENTRYR.FENTRY1
    0xFFE40000  // EB25, 128Kb
    0xFFE20000  // EB26, 128Kb
    0xFFE00000  // EB27, 128Kb
    --------------------------------------
    0xFF7FFFFF
    0xFF7FC000  // USER boot 16Kb
    --------------------------------------
    0x00100000  // DB00
    0x00102000  // DB01
    0x00104000  // DB02                       selection with FLASH2.FENTRYR.FENTRYD
    0x00106000  // DB03

\*---------------------------------------------------------------------------------------------------------*/

#define MCU_FLASH_GLOBALS

#include <mcu_flash.h>
#include <structs_bits_big.h>   // MOTOROLA (big endian) bits, bytes, words, dwords ...
#include <iodefines.h>
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL()
#include <memmap_mcu.h>         // for SRAM_TMPCODEBUF_32
#include <hardware_setup.h>     // for SoftwareReset()
#include <mcuDependent.h>      // for REFRESH_SLOWWATCHDOG()

/*---------------------------------------------------------------------------------------------------------*/
bool MCU_rom_WaitForReady_inRam(uint32_t timeout)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      wasteTimeReset;

    // The operations is complete when FSTATR.FRDY goes back high.
    while (FLASH2.FSTATR0.BIT.FRDY == 0)        // while busy
    {
        timeout--;

        if (timeout == 0)   // if timeout
        {
            // Data written to the FRESET bit is valid only when it is written in word access and the FRKEY[7:0] bits are CCh.
            FLASH2.FRESETR.WORD = 0xCC01;       // Reset the FCU

            // Give FCU time to reset
            wasteTimeReset = 35 * 100; // 35 * ICLK_FREQUENCY, number of ICLK ticks needed for 35us delay

            while (wasteTimeReset != 0)
            {
                wasteTimeReset--;
            }

            // Data written to the FRESET bit is valid only when it is written in word access and the FRKEY[7:0] bits are CCh.
            FLASH2.FRESETR.WORD = 0xCC00;       // FCU is not reset anymore
            return (FALSE);                     // error
        }
    }

    return (TRUE);                              // OK
}
/*---------------------------------------------------------------------------------------------------------*/
void MCU_rom_FCU_init_inRam(void)
/*---------------------------------------------------------------------------------------------------------*\
  disable interrupts and copy FCU firmware to FCU RAM
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t    *   src;
    uint32_t    *   dst;
    uint16_t      ii;

    // Initialises the FCU peripheral block

    FLASH2.FRDYIE.BIT.FRDYIE = 0;       // Disable the flash ready interrupt (FRDYI)

    // Disable FCU interrupts in FCU block
    // FIFERR interrupt requests disabled when the ROMAE bit in FASTAT is set to 1
    FLASH2.FAEINT.BIT.ROMAEIE  = 0;
    // FIFERR interrupt requests disabled when the CMDLK bit in FASTAT is set to 1
    FLASH2.FAEINT.BIT.CMDLKIE  = 0;
    FLASH2.FAEINT.BIT.DFLAEIE  = 0;
    FLASH2.FAEINT.BIT.DFLRPEIE = 0;
    FLASH2.FAEINT.BIT.DFLWPEIE = 0;

    // Disable FCU interrupts in ICU

    // Disable flash interface error (FIFERR)
    ICU.IPR01.BIT.IPR_20 = 0;                   // set interrupt level to 0
    ICU.IER02.BIT.IEN5   = 0;

    // Disable flash ready interrupt (FRDYI)
    ICU.IPR02.BIT.IPR_20 = 0;                   // set interrupt level to 0
    ICU.IER02.BIT.IEN7   = 0;

    // Transfer Firmware to the FCU RAM

    // To use FCU commands, the FCU firmware must be stored in the FCU RAM.
    // Before writing data to the FCU RAM the FCU must be in STOP

    // Disable the FCU form accepting commands
    if (FLASH2.FENTRYR.WORD != 0x0000)   // is already in STOP
    {
        // Data written to FENTRYD, FENTRY1 and FENTRY0 bits is valid only when it is written in
        // word access and the FKEY[7:0] bits are AAh.
        // reset the 3 FENTRYD, FENTRY1 and FENTRY0 to 0 (read mode) so the FCU don't accept any command
        // then it is in STOP.
        FLASH2.FENTRYR.WORD = 0xAA00;
    }

    // Data written to the FCRME bit is valid only when it is written in word access and the KEY[7:0] bits are C4h.
    FLASH2.FCURAME.WORD = 0xC401;       // Enable the FCU RAM

    /*
     Copies the FCU firmware to the FCU RAM.
      0xFEFFE000 (FCU firmware area, 8Kb)
      0xFF000000
     to
      0x007F8000 (FCU RAM area, 8Kb)
      0x007FA000
     */

    src = (uint32_t *) 0xFEFFE000;
    dst = (uint32_t *) 0x007F8000;

    for (ii = 0; ii < (0x2000 / 4); ii++)       // 8Kb transferred in 32 bits writes
    {
        *dst = *src;
        src++;
        dst++;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_flash_status MCU_rom_BlockErase(uint32_t * erase_address)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases the flash block for which the READ address is within
  It returns the flash block status.

  the erase_address is incremented to point to the start of the following block
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      size;
    enum FGC_flash_status result;
    uint32_t      tmp_erase_address;

    tmp_erase_address = *erase_address;

    if (tmp_erase_address < 0xFFE00000)         // 2Mb flash version
    {
        return (FLASH_STATUS_INVALID_ADDRESS);
    }

    // the first ERASE/WRITE address of the desired block is where
    // the flash management state machine resides

    if (tmp_erase_address < 0xFFFF0000)   // EB27 to EB08
    {
        if (tmp_erase_address < 0xFFF60000)   // EB27 to EB17, 128Kb
        {
            tmp_erase_address &= 0x00FF0000; // mask to have the flash state machine address
            size = 131072; // 128Kb
        }
        else    // EB16 to EB08, 64Kb blocks
        {
            tmp_erase_address &= 0x00FFF000; // mask to have the flash state machine address
            size = 65536; // 64Kb
        }
    }
    else        // EB07 to EB00, 8Kb blocks
    {
        tmp_erase_address &= 0x00FFF000; // mask to have the flash state machine address
        size = 8192; // 8Kb
    }

    // this is needed because the INTs are disable and it is there where the watchdogs are usually refreshed
    REFRESH_SLOWWATCHDOG();     // 3s refresh

    DISABLE_INTERRUPTS();       // or OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running

    result = MCU_rom_BlockErase_inRam(tmp_erase_address);               // execute erase block command

    ENABLE_INTERRUPTS();        // or OS_EXIT_CRITICAL(); depending on what was used at the beginning

    *erase_address += size; // points to the next block
    return (result);
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_flash_status MCU_rom_BlockErase_inRam(volatile uint32_t erase_address)
/*---------------------------------------------------------------------------------------------------------*\
  Note this routine does not disable the interrupts, that must be done previous to call it
\*---------------------------------------------------------------------------------------------------------*/
{
    volatile uint8_t   *  pAddr;
    enum FGC_flash_status result;

    // =========================================================================
    // preparation
    // =========================================================================
    MCU_rom_FCU_init_inRam();

    // Enter Program Erase Normal Mode for ROM/Data Flash Operations
    // Data written to FENTRYD, FENTRY1 and FENTRY0 bits is valid only when it is written in
    // word access and the FKEY[7:0] bits are AAh.

    // reset FENTRYR to 0x00 before setting FENTRYD or FENTRY1 or FENTRY0, to avoid enter in
    // command-locked state because more than 1 of them was set
    FLASH2.FENTRYR.WORD = 0xAA00;

    __asm__ volatile("nop");
    __asm__ volatile("nop");

    if (erase_address >= 0x00F00000)    // blocks EB19 to EB00 ?
    {
        // set FENTRY0 - ROM Program/Erase Mode Entry 0
        // ROM within 1 Mbyte is in ROM Program/Erase mode
        // Address for reading,         [FF F 00000h..FF F FFFFFh]
        // Address for writing/erasing  [00 F 00000h..00 F FFFFFh] (the reading area is disabled)
        FLASH2.FENTRYR.WORD = 0xAA01;   // enter program/erase mode for EB19 to EB00
    }
    else        // blocks EB27 to EB20
    {
        // set FENTRY1 - ROM Program/Erase Mode Entry 1
        // ROM more than 1 Mbyte is in ROM Program/Erase mode
        // Address for reading,         [FF E 00000h..FF E FFFFFh]
        // Address for writing/erasing  [00 E 00000h..00 E FFFFFh] (the reading area is disabled)
        FLASH2.FENTRYR.WORD = 0xAA02;   // enter program/erase mode for EB27 to EB20
    }

    FLASH1.FWEPROR.BYTE = 0x01;         // enable Write/Erase

    // Check FCU error
    if ((FLASH2.FSTATR0.BIT.ILGLERR == 1)       // Illegal Command Error ?
        || (FLASH2.FSTATR0.BIT.ERSERR  == 1)   // Erasure Error ?
        || (FLASH2.FSTATR0.BIT.PRGERR  == 1)   // Programming Error ?
        || (FLASH2.FSTATR1.BIT.FCUERR  == 1)   // An error occurs in the FCU processing ?
       )
    {
        result = FLASH_STATUS_FCU_ERROR;
        goto _flsh_end;
    }

    // Inform FCU of peripheral clock speed

    // the setting value is the operating frequency represented in [MHz] units (rounded up)
    FLASH2.PCKAR.WORD = 50; //      PCLK_FREQUENCY =  50Mhz
    // Execute Peripheral Clock Notification Command
    *(uint8_t *)  erase_address = 0xE9;
    *(uint8_t *)  erase_address = 0x03;
    *(uint16_t *) erase_address = 0x0F0F;
    *(uint16_t *) erase_address = 0x0F0F;
    *(uint16_t *) erase_address = 0x0F0F;
    *(uint8_t *)  erase_address = 0xD0;

    // Check FCU error
    if (MCU_rom_WaitForReady_inRam(1000) == FALSE)      // 10us timeout
    {
        result = FLASH_STATUS_TIMEOUT;
        goto _flsh_end;
    }

    if (FLASH2.FSTATR0.BIT.ILGLERR == 1)                // Check ILGLERR, Illegal Command Error
    {
        result = FLASH_STATUS_ILLEGAL_COMMAND;
        goto _flsh_end;
    }

    // Data written to the FPROTCN bit is valid only when it is written in word access and the FPKEY[7:0] bits are 55h.
    FLASH2.FPROTR.WORD = 0x5501;        // protection with a lock bit disabled

    // =========================================================================
    // core
    // =========================================================================

    // send command to FCU
    *(uint8_t *) erase_address = 0x20;    // block erase (with the lock bit being erased simultaneously)
    *(uint8_t *) erase_address = 0xD0;    // validate command execution


    // According to HW Manual the Max Erasure Time for a 128kB block is 1750ms.
    if (MCU_rom_WaitForReady_inRam(175000000) == FALSE)
    {
        result = FLASH_STATUS_TIMEOUT;
        goto _flsh_end;
    }

    // Check if erase operation was successful
    if ((FLASH2.FSTATR0.BIT.ILGLERR == 1)       // Illegal Command Error ?
        || (FLASH2.FSTATR0.BIT.ERSERR  == 1)   // Erasure Error ?
       )
    {
        result = FLASH_STATUS_ILLEGAL_COMMAND;
        goto _flsh_end;
    }

    // Leave Program/Erase Mode
    // According to HW Manual the Max Erasure Time for a 128kB block is 1750ms.
    if (MCU_rom_WaitForReady_inRam(175000000) == FALSE)
    {
        result = FLASH_STATUS_TIMEOUT;
        goto _flsh_end;
    }

    if ((FLASH2.FSTATR0.BIT.ILGLERR == 1)       // Illegal Command Error ?
        || (FLASH2.FSTATR0.BIT.ERSERR  == 1)    // Erasure Error ?
        || (FLASH2.FSTATR0.BIT.PRGERR  == 1)    // Programming Error ?
       )
    {
        if (FLASH2.FSTATR0.BIT.ILGLERR == 1)    // Illegal Command Error ?
        {
            // To clear the command-locked state, a status clear command must be issued to
            // the FCU after setting FASTAT to 10h
            if (FLASH2.FASTAT.BYTE != 0x10)
            {
                FLASH2.FASTAT.BYTE = 0x10;
            }
        }

        // Issue a status register clear command to clear all error bits
        pAddr = (uint8_t *)0x00E00000;    // Bottom of User Flash Area
        *pAddr = 0x50; // Status register clear
        // clears the ILGLERR, ERSERR and PRGERR bits in FSTATR0 and releases the FCU from the command locked state
    }

    // Data written to FENTRYD, FENTRY1 and FENTRY0 bits is valid only when it is written in
    // word access and the FKEY[7:0] bits are AAh.
    // reset the 3 FENTRYD, FENTRY1 and FENTRY0 to 0 (read mode) so the FCU don't accept any command
    // then it is in STOP.
    FLASH2.FENTRYR.WORD = 0xAA00;

    FLASH1.FWEPROR.BYTE = 0x02; // disable Write/Erase

    result = FLASH_STATUS_SUCCESS;

_flsh_end:
    return (result);
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_flash_status MCU_rom_BlockWrite(uint32_t source_address, uint32_t destin_address, uint32_t size_in_words)
/*---------------------------------------------------------------------------------------------------------*\

This function performs the writing of the Rx610 internal flash

    destin_address : Flash address location to write to. This address must be on a 256 byte boundary.
    source_address : Address location of data buffer to write into flash.
    size_in_words  : The number of words to write. You must always pass a multiple of 256 bytes for programming.

\*---------------------------------------------------------------------------------------------------------*/
{
    enum FGC_flash_status result;

    // Take off upper byte since for programming addresses for ROM are same as read addresses
    // except upper byte is masked off to 0's.
    destin_address &= 0x00FFFFFF;

    // is inside On-chip ROM (program ROM)?
    if ((destin_address >= 0x00E00000)                          // Bottom of User Flash Area
        && (destin_address < (0x00E00000 + 2097151))   // The user ROM size on the RX610 is 2MB
       )
    {
        // Check if the number of bytes were passed is a multiple of 256
        if ((size_in_words & 0xFF) != 0)
        {
            // ERROR!! You must always pass a multiple of 256.

            return(FLASH_STATUS_SIZE_NOT_MULTIPLE_OF_256);
        }

        // Check for an address on a 256 byte page
        if (((uint16_t) destin_address & 0xFF) != 0)
        {
            // ERROR!! You must always pass a flash address on a 256 byte boundary

            return(FLASH_STATUS_ADDR_NOT_ON_256_BOUNDARY);
        }

        // Check for passing over 1MB boundary at 0xFFF00000
        if ((destin_address < 0x00F00000)
            && ((destin_address + size_in_words) > 0x00F00000)
           )
        {
            // ERROR!! You cannot write over 1MB boundary at 0xFFF00000.  You must split up the write.

            return (FLASH_STATUS_SIZE_EXCEEDS_1MB);
        }
    }
    else
    {
        // ERROR!! Invalid flash address, is not program ROM

        return (FLASH_STATUS_ADDR_NOT_IN_ROM);
    }

    // this is needed because the INTs are disable and it is there where the watchdogs are usually refreshed
    REFRESH_SLOWWATCHDOG();     // 3s refresh

    // Disable interrupts as may be we will write where the ISRs are
    DISABLE_INTERRUPTS();       // or OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running

    result = MCU_rom_BlockWrite_inRam(source_address, destin_address, size_in_words);

    ENABLE_INTERRUPTS();        // or OS_EXIT_CRITICAL(); depending on what was used at the beginning

    return (result);
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_flash_status MCU_rom_BlockWrite_inRam(uint32_t source_address, volatile uint32_t destin_address, uint32_t size_in_words)
/*---------------------------------------------------------------------------------------------------------*\

    This function performs the writing of the Rx610 internal flash
          Writes 256 bytes multiple into flash

        destin_address : Flash address location to write to. This address must be on a 256 byte boundary.
        source_address : Address location of data buffer to write into flash.
        size_in_words  : The number of words to write. You must always pass a multiple of 256 bytes for programming.

    variables defined externally
        uint8_t   flashFunc_fmr0
        uint8_t   flashFunc_fmr1


  we write several times to the same address (destin_address) where the flash state machine is, this is why
  it is volatile

  Note this routine does not disable the interrupts, that must be done previous to call it

\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t               result;
    volatile uint8_t   *  pAddr;
    uint8_t               ii;

    // =========================================================================
    // preparation
    // =========================================================================
    MCU_rom_FCU_init_inRam();

    // Enter Program Erase Normal Mode for ROM/Data Flash Operations
    // Data written to FENTRYD, FENTRY1 and FENTRY0 bits is valid only when it is written in
    // word access and the FKEY[7:0] bits are AAh.

    // reset FENTRYR to 0x00 before setting FENTRYD or FENTRY1 or FENTRY0, to avoid enter in
    // command-locked state because more than 1 of them was set
    FLASH2.FENTRYR.WORD = 0xAA00;

    __asm__ volatile("nop");
    __asm__ volatile("nop");

    if (destin_address >= 0x00F00000)
    {
        // Data written to FENTRYD, FENTRY1 and FENTRY0 bits is valid only when it is written in
        // word access and the FKEY[7:0] bits are AAh.

        // set FENTRY0 - ROM Program/Erase Mode Entry 0
        // ROM within 1 Mbyte is in ROM Program/Erase mode
        // Address for reading,         [FF F 00000h..FF F FFFFFh]
        // Address for writing/erasing  [00 F 00000h..00 F FFFFFh] (the reading area is disabled)
        FLASH2.FENTRYR.WORD = 0xAA01;   // enter program/erase mode for EB19 to EB00
    }
    else
    {
        // Data written to FENTRYD, FENTRY1 and FENTRY0 bits is valid only when it is written in
        // word access and the FKEY[7:0] bits are AAh.

        // set FENTRY1 - ROM Program/Erase Mode Entry 1
        // ROM more than 1 Mbyte is in ROM Program/Erase mode
        // Address for reading,         [FF E 00000h..FF E FFFFFh]
        // Address for writing/erasing  [00 E 00000h..00 E FFFFFh] (the reading area is disabled)
        FLASH2.FENTRYR.WORD = 0xAA02;   // enter program/erase mode for EB27 to EB20
    }

    FLASH1.FWEPROR.BYTE = 0x01; // enable Write/Erase

    // Check FCU error
    if ((FLASH2.FSTATR0.BIT.ILGLERR == 1)       // Illegal Command Error ?
        || (FLASH2.FSTATR0.BIT.ERSERR  == 1)   // Erasure Error ?
        || (FLASH2.FSTATR0.BIT.PRGERR  == 1)   // Programming Error ?
        || (FLASH2.FSTATR1.BIT.FCUERR  == 1)   // An error occurs in the FCU processing ?
       )
    {
        result = FLASH_STATUS_FCU_ERROR;
        goto _flsh_end;
    }

    // Data written to the FPROTCN bit is valid only when it is written in word access and the FPKEY[7:0] bits are 55h.
    FLASH2.FPROTR.WORD = 0x5501;        // protection with a lock bit disabled

    // =========================================================================
    // core
    // =========================================================================

    while (size_in_words != 0)
    {
        // Write the FCU Program command
        *(uint8_t *) destin_address = 0xE8;       // Programming (in 256-byte units)
        *(uint8_t *) destin_address = 0x80;

        // now, destin_address must be in 256 boundary and is the start address of the programming data

        // Write 256 bytes into flash, 16-bits at a time
        for (ii = 0; ii < 128; ii++)
        {
            *(uint16_t *) destin_address = *(uint16_t *) source_address;
            source_address += 2;
        }

        *(uint8_t *) destin_address = 0xD0; // validate command execution

        // this is needed because the INTs are disable and it is there where the watchdogs are usually refreshed
        REFRESH_SLOWWATCHDOG();     // 3s refresh

        // According to HW Manual the Max Programming Time for 256 bytes (ROM) is 12ms.
        if (MCU_rom_WaitForReady_inRam(1200000) == FALSE)
        {
            result = FLASH_STATUS_TIMEOUT;
            goto _flsh_end;
        }

        // Check for illegal command or programming errors
        if ((FLASH2.FSTATR0.BIT.ILGLERR == 1)           // Illegal Command Error ?
            || (FLASH2.FSTATR0.BIT.PRGERR  == 1)        // Programming Error ?
           )
        {
            result = FLASH_STATUS_FCU_ERROR;
            goto _flsh_end;
        }

        // Now do the next set of 256 bytes
        destin_address += 256;
        //      source_address += 256;  // Already incremented in the write loop
        size_in_words  -= 128; // 256 / 2
    }

    // Leave Program/Erase Mode
    // According to HW Manual the Max Erasure Time for a 128kB block is 1750ms.
    if (MCU_rom_WaitForReady_inRam(175000000) == FALSE)
    {
        result = FLASH_STATUS_TIMEOUT;
        goto _flsh_end;
    }

    if ((FLASH2.FSTATR0.BIT.ILGLERR == 1)       // Illegal Command Error ?
        || (FLASH2.FSTATR0.BIT.ERSERR  == 1)    // Erasure Error ?
        || (FLASH2.FSTATR0.BIT.PRGERR  == 1)    // Programming Error ?
       )
    {
        if (FLASH2.FSTATR0.BIT.ILGLERR == 1)    // Illegal Command Error ?
        {
            // To clear the command-locked state, a status clear command must be issued to
            // the FCU after setting FASTAT to 10h
            if (FLASH2.FASTAT.BYTE != 0x10)
            {
                FLASH2.FASTAT.BYTE = 0x10;
            }
        }

        // Issue a status register clear command to clear all error bits
        pAddr = (uint8_t *)0x00E00000;    // Bottom of User Flash Area
        *pAddr = 0x50;
    }

    // Data written to FENTRYD, FENTRY1 and FENTRY0 bits is valid only when it is written in
    // word access and the FKEY[7:0] bits are AAh.
    // reset the 3 FENTRYD, FENTRY1 and FENTRY0 to 0 (read mode) so the FCU don't accept any command
    // then it is in STOP.
    FLASH2.FENTRYR.WORD = 0xAA00;

    FLASH1.FWEPROR.BYTE = 0x02; // disable Write/Erase

    result = FLASH_STATUS_SUCCESS;
_flsh_end:
    return (result);
}
/*---------------------------------------------------------------------------------------------------------*/
void MCU_rom_FlashBoot(uint32_t destin_address, uint32_t size_in_bytes)
/*---------------------------------------------------------------------------------------------------------*\
    __attribute__ ((noreturn, section ("text_ram")))

    flash the boot, the vectors and do a software reset

    atomic(erase + write + reset CPU)
\*---------------------------------------------------------------------------------------------------------*/
{
    // 0 address it also counts, so to have the last byte ...
    MCU_rom_FlashBoot_inRam(destin_address + size_in_bytes - 1, destin_address, (size_in_bytes >> 1));
}
/*---------------------------------------------------------------------------------------------------------*/
void MCU_rom_FlashBoot_inRam(uint32_t destin_address_last_byte, uint32_t destin_address, uint32_t size_in_words)
/*---------------------------------------------------------------------------------------------------------*\
    __attribute__ ((noreturn, section ("text_ram")))
\*---------------------------------------------------------------------------------------------------------*/
{
    enum FGC_flash_status       result;
    uint32_t      destin_address_copy;


    // Disable interrupts as we will erase where the ISRs are
    DISABLE_INTERRUPTS();       // or OS_ENTER_CRITICAL()

    // just to be sure
    DisableNetworkInterrupt();  // Warning!!! this resides in ROM
    Disable1msTickInterrupt();  // Warning!!! this resides in ROM

    // this is needed because the INTs are disable and it is there where the watchdogs are usually refreshed
    REFRESH_SLOWWATCHDOG();     // 3s refresh

    destin_address_copy = destin_address;       // we work here with the copy

    // erase the BootProg area, EB27 to EB17 area is 128Kb chunks
    do
    {
        // mask to have the flash state machine address

        result = MCU_rom_BlockErase_inRam(destin_address_copy & 0x00FF0000);          // execute erase block command

        if (result != FLASH_STATUS_SUCCESS)
        {
            // ToDo: we delete something but fails, what to do here ???? the ROM code is not there anymore
            // return(1);
        }

        destin_address_copy += 131072; // 128Kb, points to the next block

    }
    while (destin_address_copy < destin_address_last_byte);

    REFRESH_SLOWWATCHDOG();     // 3s refresh

    // flash the BootProg area

    // source data is in SRAM buffer
    // Take off upper byte since for programming addresses for ROM are same as read addresses
    // except upper byte is masked off to 0's.

    result = MCU_rom_BlockWrite_inRam(SRAM_TMPCODEBUF_32, destin_address & 0x00FFFFFF, size_in_words);

    if (result != FLASH_STATUS_SUCCESS)
    {
        // ToDo: we write something but fails, what to do here ???? the ROM code is not there anymore
        // return(2);
    }

    REFRESH_SLOWWATCHDOG();     // 3s refresh


    // fixed vectors go to EB00 (8Kb) 0xFFFFE000
    // 0x00FFE000 mask to have the flash state machine address

    // erase fixed vectors
    result = MCU_rom_BlockErase_inRam(0x00FFE000);          // execute erase block command

    if (result != FLASH_STATUS_SUCCESS)
    {
        // ToDo : flash erase of fixed vectors fail, what to do here ????
        // return(3);
    }

    REFRESH_SLOWWATCHDOG();     // 3s refresh

    // flash fixed vectors, 32 x 4 bytes = 128 bytes = 64 words

    // in the code block the fixed vectors are stores after the variables vector table (1024 bytes region, vectors from 0 to 255)

    // SRAM_TMPCODEBUF_32 + 1024, SRAM buffer for fixed vectors (inside BOOT code)
    // 0xFFFFFF80, destin_address
    // but we need to write 256 bytes (128 words)
    // so will be some garbage before our vectors ...
    // Disable interrupts as may be we will write where the ISRs are
    result = MCU_rom_BlockWrite_inRam(SRAM_TMPCODEBUF_32 + 1024 - 128, 0x00FFFF00, 128);

    if (result != FLASH_STATUS_SUCCESS)
    {
        // ToDo : flash erase of fixed vectors fail, what to do here ????
        // return(4);
    }

    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_GLOBAL_MASK16;    // Trigger global reset

    while (1);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mcu_flash.c
\*---------------------------------------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------------------------------------*\
FLASH2.FMODR    Flash Mode Register             00h 007FC402h 8

        FMODR is a register to specify the method for the reading of lock bits.

        If the blank checking command for the data flash is to be used, this bit has to be set for
        the register read mode

            b7,b6,b5 - Reserved, these bits are read as 0. The write value should be 0

            b4 - FRDMD  FCU Read Mode Select
             0 : Memory Area Read Method
                 you can read a lock bit of ROM in ROM lock bit read mode.
             1 : Register Read Method
                 you can read a lock bit of ROM using the lock bit read 2 command.

            b3,b2,b1,b0 - Reserved, these bits are read as 0. The write value should be 0


FLASH2.FASTAT   Flash Access Status Register            00h 007FC410h 8

        FASTAT is a register to check if the access to the ROM/data flash is allowed.
        When one of the bits in FASTAT is set to 1, the FCU is placed in the command-locked state.
        To clear the command-locked state, a status clear command must be issued to the FCU
        after setting FASTAT to 10h.

            b7 - ROMAE  ROM Access Violation
             0 : No ROM access error
             1 : ROM access error (and the FCU is in command-locked state)

            b6,b5 - Reserved, these bits are read as 0. The write value should be 0

            b4 - CMDLK  FCU Command Lock
             0 : FCU is not in the command-locked state
             1 : FCU is in the command-locked state

            b3 - DFLAE  Data Flash Access Violation
             0 :
             1 :

            b2 - Reserved, this bit is read as 0. The write value should be 0

            b1 - DFLRPE Data Flash Read Protection Violation
             0 :
             1 :

            b0 - DFLWPE Data Flash Programming/Erasure Protection Violation
             0 :
             1 :


FLASH2.FAEINT   Flash Access Error Interrupt Enable Register            00h 007FC411h 8

        FAEINT is a register to enable and disable the flash interface error interrupt (FIFERR).

            b7 - ROMAEIE        ROM Access Violation Interrupt Enable
             0 : FIFERR interrupt requests disabled when the ROMAE bit in FASTAT is set to 1
             1 : FIFERR interrupt requests enabled  when the ROMAE bit in FASTAT is set to 1

            b6,b5 - Reserved, these bits are read as 0. The write value should be 0

            b4 - CMDLKIE        FCU Command Lock Interrupt Enable
             0 : FIFERR interrupt requests disabled when the CMDLK bit in FASTAT is set to 1
             1 : FIFERR interrupt requests enabled  when the CMDLK bit in FASTAT is set to 1

            b3 - DFLAEIE        Data Flash Access Violation Interrupt Enable
             0 :
             1 :

            b2 - Reserved, this bit is read as 0. The write value should be 0

            b1 - DFLRPEIE       Data Flash Read Protection Violation Interrupt Enable
             0 :
             1 :

            b0 - DFLWPEIE       Data Flash Programming/Erasure Protection Violation Interrupt Enable
             0 :
             1 :


FLASH2.FCURAME  FCU RAM Enable Register         0000h 007FC454h 16

        FCURAME is a register to enable and disable an access to the FCU RAM area.

        Only specific values written to the upper byte in word access are valid.
        Data written to the upper byte is not retained.
        Data written to the FCRME bit is valid only when it is written in word access and
        the KEY[7:0] bits are C4h.
        When programming data to the FCU RAM, set FENTRYR to 0000h (this prevents the FCU
        from accepting any command so it is in STOP)


            b15,b14,b13,b12,b11,b10,b9,b8 - KEY[7:0]    Key Code
                This byte is used to enable or disable rewriting of the FCRME bit.
                C4h is the magic number.

            b7,b6,b5,b4,b3,b2,b1 - Reserved, these bits are read as 0. The write value should be 0

            b0 - FCRME  FCU RAM Enable
             0 : access to the FCU RAM disabled
             1 : access to the FCU RAM enabled


FLASH2.FSTATR0  Flash Status Register 0         80h 007FFFB0h 8

            b7 - FRDY   Flash Ready
             0 : busy
             1 : ready

            b6 - ILGLERR        Illegal Command Error
             0 : FCU detects no illegal command or ROM/data flash access
             1 : FCU detects an illegal command or ROM/data flash access (FCU is in the command-locked state)

            b5 - ERSERR Erasure Error
             0 : Erasure terminates normally
             1 : An error occurs during erasure (FCU is in the command-locked state)

            b4 - PRGERR Programming Error
             0 : Programming terminates normally
             1 : An error occurs during programming (FCU is in the command-locked state)

            b3 - SUSRDY Suspend Ready
             0 : Program/Erase suspend commands cannot be received
             1 : Program/Erase suspend commands can be received

            b2 - Reserved, this bit is read as 0. The write value should be 0

            b1 - ERSSPD Erasure Suspend Status
             0 : Other than the status described below
             1 : When erasure suspend processing or erasure suspended

            b0 - PRGSPD Programming Suspend Status
             0 : Other than the status described below
             1 : During programming suspend processing or programming suspended


FLASH2.FSTATR1  Flash Status Register 1         0xh 007FFFB1h 8

            b7 - FCUERR FCU Error
             0 : No error occurs in the FCU processing
             1 : An error occurs in the FCU processing

            b6,b5 - Reserved, these bits are read as 0 and cannot be modified

            b4 - FLOCKST        Lock Bit Status
                    This bit is to reflect the read data of a lock bit when using the lock bit read 2 command
             0 : Protected
             1 : Not protected

            b3,b2 - Reserved, these bits are read as 0 and cannot be modified

            b1,b0 - Reserved, read value is undefined and cannot be modified


FLASH2.FRDYIE   Flash Ready Interrupt Enable Register           00h 007FC412h 8

        If the FRDYIE bit is set to 1, a flash ready interrupt request (FRDYI) is generated when
        execution of the FCU command has completed (FSTATR0.FRDY bit changes from 0 to 1).

            b7,b6,b5,b4,b3,b2,b1 - Reserved, these bits are read as 0. The write value should be 0

            b0 - FRDYIE Flash Ready Interrupt Enable
             0 : FRDYI interrupt requests disabled
             1 : FRDYI interrupt requests enabled


FLASH2.FENTRYR  Flash Program/Erase Mode Entry Register         0000h 007FFFB2h 16

        Only specific values written to the upper byte in word access are valid.
        Data written to the upper byte is not retained.
        To place the ROM/data flash in Program/Erase mode so that the FCU can accept commands, one of
        the FENTRYD, FENTRY1 or FENTRY0 bits must be set to 1.
        If more than one of these bits is set to 1, the ILGLERR bit in FSTATR0 is set and the FCU enters
        the command-locked state.
        When the 3 FENTRYD, FENTRY1 and FENTRY0 are 0, the FCU don't accept any command so it is in STOP.


            b15,b14,b13,b12,b11,b10,b9,b8 - FKEY[7:0]   Key Code
                This byte is used to enable or disable rewriting of the FENTRYD, FENTRY1 and FENTRY0 bits.
                AAh is the magic number.

            b7 - FENTRYD        Data Flash Program/Erase Mode Entry
             0 :
             1 :

            b6,b5,b4,b3,b2 - Reserved, these bits are read as 0. The write value should be 0

            b1 - FENTRY1        ROM Program/Erase Mode Entry 1
             0 : ROM more than 1 Mbyte is in ROM read mode
             1 : ROM more than 1 Mbyte is in ROM Program/Erase mode
                    Address for reading,         [FF E 00000h..FF E FFFFFh]
                    Address for writing/erasing, [00 E 00000h..00 E FFFFFh] (the reading area is disabled)

            b0 - FENTRY0        ROM Program/Erase Mode Entry 0
             0 : ROM within 1 Mbyte is in ROM read mode
             1 : ROM within 1 Mbyte is in ROM Program/Erase mode
                    Address for reading,         [FF F 00000h..FF F FFFFFh]
                    Address for writing/erasing  [00 F 00000h..00 F FFFFFh] (the reading area is disabled)


FLASH2.FPROTR   Flash Protection Register               0000h 007FFFB4h 16

        Only specific values written to the upper byte in word access are valid.
        Data written to the upper byte is not retained.
        Data written to the FPROTCN bit is valid only when it is written in word access and
        the FPKEY[7:0] bits are 55h.


            b15,b14,b13,b12,b11,b10,b9,b8 - FPKEY[7:0]  Key Code
                This byte is used to enable or disable rewriting of the FPROTCN bit.
                55h is the magic number.

            b7,b6,b5,b4,b3,b2,b1 - Reserved, these bits are read as 0. The write value should be 0

            b0 - FPROTCN        Lock Bit Protection Cancel
             0 : protection with a lock bit enabled
             1 : protection with a lock bit disabled


FLASH2.FRESETR  Flash Reset Register            0000h 007FFFB6h 16

        Only specific values written to the upper byte in word access are valid.
        Data written to the upper byte is not retained.
        Data written to the FRESET bit is valid only when it is written in word access and
        the FRKEY[7:0] bits are CCh.


            b15,b14,b13,b12,b11,b10,b9,b8 - FRKEY[7:0]  Key Code
                This byte is used to enable or disable rewriting of the FRESET bit.
                CCh is the magic number.

            b7,b6,b5,b4,b3,b2,b1 - Reserved, these bits are read as 0. The write value should be 0

            b0 - FRESET Flash Reset
             0 : FCU is not reset
             1 : FCU is reset
                        When the FRESET bit is set to 1, programming/erasure operations for the ROM/data
                        flash are forcibly terminated, and the FCU is initialized.


FLASH2.FCMDR    FCU Command Register            0000h 007FFFBAh 16

            b15,b14,b13,b12,b11,b10,b9,b8 - CMDR[7:0]   Command
                Store the last command received by the FCU.

            b7,b6,b5,b4,b3,b2,b1,b0 - PCMDR[7:0]        Precommand
                Store the command immediately before the last command received by the FCU.

when command                                            CMDR    PCMDR
-----------------------------------------------         ------- ----------------
Program/Erase to Normal mode transition                 FFh     Previous command
Status read mode transition                             70h     Previous command
Lock bit read mode transition (lock bit read 1)         71h     Previous command
Peripheral clock notification command                   E9h     Previous command
Programming (in 256-byte units)                         E8h     Previous command
Block erase                                             D0h     20h
Program/Erase suspend                                   B0h     Previous command
Program/Erase resume                                    D0h     Previous command
Status register clear                                   50h     Previous command
Lock bit read 2 / blank check                           D0h     71h
Lock bit programming                                    D0h     77h






FLASH2.FCPSR    FCU Processing Switching Register       0000h 007FFFC8h 16

            b15,b14,b13,b12,b11,b10,b9,b8
            b7,b6,b5,b4,b3,b2,b1 - Reserved, these bits are read as 0. The write value should be 0

            b0 - ESUSPMD        Erasure Suspend Mode
             0 : suspension priority mode
             1 : Erasure priority mode
                    This bit is to select the erasure suspend mode for when a Program/Erase suspend command
                    is issued while the FCU executes the erasure processing for the ROM/data flash


FLASH2.FPESTAT  Flash Program/Erase Status Register     0000h 007FFFCCh 16

            b15,b14,b13,b12,b11,b10,b9,b8  Reserved, these bits are read as 0. The write value should be 0
            b7,b6,b5,b4,b3,b2,b1,b0 - PEERRST[7:0]      Program/Erase Error Status
                01h : Programming error against areas protected by a lock bit
                02h : Programming error due to sources other than the lock bit protection
                11h : Erasure error against areas protected by a lock bit
                12h : Erasure error due to sources other than the lock bit protection
                (Values other than above are reserved)


FLASH2.PCKAR    Peripheral Clock Notification Register  0000h 007FFFE8h 16

            This setting is used to control programming and erasure times.
            Set the PCKA[7:0] bits to the PCLK frequency and issue a peripheral clock notification
            command before programming/erasure.
            Do not change the frequency during the programming/erasure processing for the ROM/data flash.

            the setting value is the operating frequency represented in [MHz] units (rounded up)

            i.e. if peripheral clock is 35.9 MHz, the setting value is 36 (0x24)
            the valid range is 8 to 50

            b15,b14,b13,b12,b11,b10,b9,b8  Reserved, these bits are read as 0. The write value should be 0
            b7,b6,b5,b4,b3,b2,b1,b0 - PCKA[7:0] Peripheral Clock Notification
                These bits are used to set the peripheral clock (PCLK) at the programming/erasure
                for the ROM/data flash.


FLASH1.FWEPROR  Flash Write Erase Protection Register           02h 0008C289h 8

            b7,b6,b5,b4,b3,b2,b2 - Reserved, these bits are read as 0. The write value should be 0

            b1,b0 - FLWE[1:0]   Flash Write/Erase
             0  0 : disabled
             0  1 : enabled
             1  0 : disabled (initial value)
             1  1 : disabled







Commands for the FCU are issued by write access to addresses within the range for programming and erasure.

block addr:FFh - Program/Erase to Normal mode transition

block addr:70h - Status read mode transition

block addr:71h - Lock bit read mode transition (lock bit read 1)

block addr:E9h - Peripheral clock notification command
block addr:03h
block addr:0F0Fh
block addr:0F0Fh
block addr:D0h - validate command execution

block addr:E8h - Programming (in 256-byte units)
block addr:80h
DESTIN ADR: data word[0]        (destin Adr must be in 256 boundary, is the start address of the programming data)
block addr: data word[1]
............
block addr: data word[128]
block addr:D0h - validate command execution


block addr:20h - Block erase (in block units, with the lock bit being erased simultaneously)
DESTIN ADR:D0h - validate command execution     (any address inside the block we want to erase)


block addr:B0h - Program/Erase suspend
block addr:D0h - Program/Erase resume, validate command execution

block addr:50h - Status register clear
(clears the ILGLERR, ERSERR and PRGERR bits in FSTATR0 and releases the FCU from the command locked state)

block addr:71h - Lock bit read 2 / blank check
DESTIN ADR:D0h - validate command execution     (any address inside the block we want to test)

(reads the lock bit of a specified erasure block (the value of the lock bit is reflected in the FLOCKST bit of FSTATR1)/blank checking of the data-flash memory)
The lock bit read 2 command is also used as the blank-checking command for the data-flash memory.
That is, when a lock bit read 2 command is issued for the data flash, blank checking is executed for the data-flash memory

block addr:77h - Lock bit programming
DESTIN ADR:D0h - validate command execution (any address inside the block we want to program)

\*---------------------------------------------------------------------------------------------------------*/
