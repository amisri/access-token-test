/*!
 *	@file	lssb.c
 *	@brief	Function for determining the Least Significant Set Bit
 */


// Includes

#include <lssb.h>



// External function definitions

uint16_t lssb(uint16_t data)
{
	uint16_t test = 0x8000;
    uint16_t i;

    for (i = 16; i != 0; i--)
    {
    	if ((data & test) != 0)
    	{
    		return i - 1; 
    	}

    	test >>= 1;
    }

   	return 0x1F;
}