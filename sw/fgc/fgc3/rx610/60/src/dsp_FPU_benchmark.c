/*---------------------------------------------------------------------------------------------------------*\
 File:      dsp_FPU_benchmark.c

 Purpose:   RX610 floating point speed testing vs DSP TMS320C6726

\*---------------------------------------------------------------------------------------------------------*/

#define DSP_FPU_BENCHMARK_GLOBALS

#include <dsp_FPU_benchmark.h>
#include <stdio.h>              // for fprintf()
#include <stdint.h>
#include <stdbool.h>
#include <structs_bits_big.h>   // MOTOROLA bits, bytes, words, dwords ...
#include <iodefines.h>          // specific processor registers and constants
#include <mcuDependent.h>      // for USLEEP()
#include <stdlib.h>             // for strtod() and strtof()
#include <syscalls.h>           // for InitMalloc()
#include <errno.h>              // for ENODEV, etc...
#include <string.h>             // for strcpy()
#include <boot_dual_port_ram.h> // for dpram global variable
#include <main.h>               // for global variables timeout_ms, main_misc
#include <boot_dsp_cmds.h>      // constants shared by DSP and CPU
#include <term.h>
#include <sleep.h>

/*---------------------------------------------------------------------------------------------------------*/
bool Dsp_FPU_Benchmark(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    struct TDspBenchmarkInfo    test[DSP_TEST_LEN];
    uint32_t                      ii;


    // don't continue if in standalone because if the dsp is halt the waiting for response loop will hang this
    // code

    if ((CPU_MODE_P & CPU_MODEL_DSPSTAL_MASK8) != 0)            // If DSP in standalone mode
    {
        return (FALSE);
    }

    for (ii = 0; ii < DSP_TEST_LEN; ii++)
    {
        //  ------------- Rx610 -------------

        GET_US(test[ii].rx610_tick_0.word);

        test[ii].rx610_fp64 = strtod(DSP_TEST_DATA[ii].txt, NULL);
        test[ii].errno_tod = errno;
        errno = 0;

        /*
         If no conversion could be performed,
            0 is returned.
         If the correct value is out of the range of representable values,
            plus or minus <<HUGE_VAL>> is returned, and ERANGE:34 is stored in errno.
         If the correct value would cause underflow,
            0 is returned and ERANGE:34 is stored in errno.
        */

        GET_US(test[ii].rx610_tick_1.word);

        test[ii].rx610_fp32 = strtof(DSP_TEST_DATA[ii].txt, NULL);
        test[ii].errno_tof = errno;
        errno = 0;

        GET_US(test[ii].rx610_tick_2.word);


        // ------------- TMS320C6721 --------
        strcpy((char *)dpram->mcu.slowArg.str, DSP_TEST_DATA[ii].txt);

        GET_US(test[ii].c6721_tick_0.word);

        //      SendDspSlowCmdAndWaitForRsp();
        // Write command
        dpram->mcu.slowCmd = DSP_CMD_SLOW_StrToFP32;

        // wait for DSP to answer the CMD
        while (dpram->mcu.slowCmd != DSP_CMD_NOTHING)
        {
        }

        test[ii].c6727_fp32 = dpram->dsp.slowRsp.data32.fp32;

        GET_US(test[ii].c6721_tick_1.word);

        //      SendDspSlowCmdAndWaitForRsp();
        // Write command
        dpram->mcu.slowCmd = DSP_CMD_SLOW_StrToFP64;

        // wait for DSP to answer the CMD
        while (dpram->mcu.slowCmd != DSP_CMD_NOTHING)
        {
        }

        // @TODO implement this not with an 8 byte variable but with a two 4 byte variables

        test[ii].c6727_fp64 = 0.0;
        // test[ii].c6727_fp64 = dpram->dsp.slowRsp.data64.fp64;


        GET_US(test[ii].c6721_tick_2.word);
        // ----------------------------------

        /*
            Note: Because a value of 0 can indicate either an error or a valid result,
            an application that checks for errors with the strtod, strtof, and strtold
            subroutines should set the errno global variable equal to 0 prior to the subroutine call.
            The application can check the errno global variable after the subroutine call.

            For the strtod, strtof, and strtold subroutines, if the value of the EndPointer parameter
            is not (char**) NULL, a pointer to the character that stopped the subroutine is stored in *EndPointer
        */


    } // end for


    // to do the conversion in the excel file easier, using the same structure as in the test of the M32C87B
    for (ii = 0; ii < DSP_TEST_LEN; ii++)
    {
        // Display results
        fprintf(term.f, RSP_DATA ", M %3d,%3d D %3d,%3d M %12g,%12g D %12g,%12g\n",
                test[ii].rx610_tick_1.word - test[ii].rx610_tick_0.word,
                test[ii].rx610_tick_2.word - test[ii].rx610_tick_1.word,
                test[ii].c6721_tick_1.word - test[ii].c6721_tick_0.word,
                test[ii].c6721_tick_2.word - test[ii].c6721_tick_1.word,
                (double) test[ii].rx610_fp32,
                (double) test[ii].rx610_fp64,
                (double) test[ii].c6727_fp32,
                (double) test[ii].c6727_fp64);
    }

    return (TRUE);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_FPU_benchmark.c
\*---------------------------------------------------------------------------------------------------------*/
