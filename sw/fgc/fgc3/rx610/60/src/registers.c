/*---------------------------------------------------------------------------------------------------------*\
  File:     registers.c

  Purpose:  Register access functions

  Notes:    This function tests access to a given resister and reports if failure.

\*---------------------------------------------------------------------------------------------------------*/

#include <registers.h>

#include <stdint.h>
#include <memmap_mcu.h>
#include <fgc_runlog_entries.h> // for FGC_RL_
#include <runlog_lib.h>         // for RunlogWrite
#include <mcuDependent.h>      // for NVRAM_UNLOCK(), NVRAM_LOCK()
#include <iodefines.h>          // specific processor registers and constants
#include <dev.h>                // for global var dev
#include <sleep.h>

/*---------------------------------------------------------------------------------------------------------*/
void VerifyPeripheralArePresent(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by main() to check access to the peripheral.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t          old_value;

    //---------------------------------------------------------------------------------------
    // performs quick test on NVRAM
    // CS3 0x0500 0000 16 bit bus
    //     0x05FF FFFF

    NVRAM_UNLOCK();

    old_value = *(uint16_t *)NVRAM_COREDATA_32;
    *(uint16_t *)NVRAM_COREDATA_32 = 0x55AA;

    // not used area at the end of CS3 just to to inverse data bus and avoid false positive due to bus capacitance
    //  *(uint16_t*)(0x05fffffe) = ~0x55AA;

    if ((*(uint16_t *)NVRAM_COREDATA_32) != 0x55AA)
    {
        dev.reg.nvram = 1;                                  // Set error bit for NVRAM
        RunlogWrite(FGC_RL_REG_ERR, (void *)NVRAM_COREDATA_32);
    }

    *(uint16_t *)NVRAM_COREDATA_32 = old_value;

    NVRAM_LOCK();
    //---------------------------------------------------------------------------------------
    // performs quick test on SRAM.
    // there are others tests : MenuSTsram(), MenuMTsram(), MenuMTsramSeq()
    // CS2 0x0600 0000 16 bit bus
    //     0x06FF FFFF

    old_value = *(uint16_t *)SRAM_TMPCODEBUF_32;
    *(uint16_t *)SRAM_TMPCODEBUF_32 = 0x55AA;

    // not used area at the end of CS2 just to to inverse data bus and avoid false positive due to bus capacitance
    //  *(uint16_t*)(0x06fffffe) = ~0x55AA;

    if (((*(uint16_t *)SRAM_TMPCODEBUF_32)) != 0x55AA)
    {
        dev.reg.sram = 1;                                   // Set error bit for SRAM
        RunlogWrite(FGC_RL_REG_ERR, (void *)SRAM_TMPCODEBUF_32);
    }

    *(uint16_t *)SRAM_TMPCODEBUF_32 = old_value;

    //---------------------------------------------------------------------------------------
    // [PLD] Dual Port RAM = DPRAM
    // CS4 0x0400 0000 8 bit bus
    //     0x04FF FFFF
    // there are others tests : MenuMT_PLD_DpRam()

    old_value = *(uint16_t *)DPRAM_START_32;
    *(uint16_t *)DPRAM_START_32 = 0x55AA;

    // not used area at the end of CS4 just to to inverse data bus and avoid false positive due to bus capacitance
    //  *(uint16_t*)(0x04fffffe) = ~0x55AA;

    if ((*(uint16_t *)DPRAM_START_32) != 0x55AA)
    {
        dev.reg.dpram = 1;
        RunlogWrite(FGC_RL_REG_ERR, (void *)DPRAM_START_32);
    }

    *(uint16_t *)DPRAM_START_32 = old_value;
    //---------------------------------------------------------------------------------------
    // [PLD] LEDS
    // CS4 0x0400 0000 8 bit bus
    //     0x04FF FFFF
    old_value = *(uint16_t *)RGLEDS_32;
    *(uint16_t *)RGLEDS_32 = 0x0FFF;

    // not used area at the end of CS4 just to to inverse data bus and avoid false positive due to bus capacitance
    //  *(uint16_t*)(0x04fffffe) = ~0x55AA;

    // mask is 0x0FFD because led FIP green can be forced by the hardware
    if (((*(uint16_t *)RGLEDS_32) & 0x0FFD) != (0x0FFF & 0x0FFD))
    {
        dev.reg.leds_rg = 1;
        RunlogWrite(FGC_RL_REG_ERR, (void *)RGLEDS_32);
    }

    // 002 FIP
    // 008 FGC
    // 020 PSU
    // 080 VS
    // 200 DCCT
    // 800 PIC

    old_value = *(uint16_t *)BLEDS_32;
    *(uint16_t *)BLEDS_32 = 0x0FFF;

    // not used area at the end of CS4 just to to inverse data bus and avoid false positive due to bus capacitance
    //  *(uint16_t*)(0x04fffffe) = ~0x55AA;

    // mask is 0x0AAA because led blue ...
    if (((*(uint16_t *)BLEDS_32) & 0x0AAA) != (0x0FFF & 0x0AAA))
    {
        dev.reg.leds_bl = 1;
        RunlogWrite(FGC_RL_REG_ERR, (void *)BLEDS_32);
    }

    *(uint16_t *)BLEDS_32 = old_value;
    //---------------------------------------------------------------------------------------
    // [PLD] DIG - Output enable is still low
    // CS4 0x0400 0000 8 bit bus
    //     0x04FF FFFF

    P7.DR.BIT.B6 = 1;   // disable digital CMD output

    DIG_OP_P = 0xFF00;      // Reset all CMDs

    old_value = *(uint16_t *)DIG_OP_32;
    *(uint16_t *)DIG_OP_32 = 0x005A;

    // not used area at the end of CS4 just to to inverse data bus and avoid false positive due to bus capacitance
    //  *(uint16_t*)(0x04fffffe) = ~0x55AA;

    // mask is 0x00FF because only write and read in SET bits
    if (((*(uint16_t *)DIG_OP_32) & 0x00FF) != (0x005A & 0x00FF))
    {
        dev.reg.io = 1;
        RunlogWrite(FGC_RL_REG_ERR, (void *)DIG_OP_32);
    }

    *(uint16_t *)DIG_OP_32 = old_value;

    DIG_OP_P = 0xFF00;      // Reset all CMDs
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: registers.c
\*---------------------------------------------------------------------------------------------------------*/
