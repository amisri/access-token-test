/*!
 *  @file     lan92xx.c
 *  @defgroup FGC3
 *  @brief      Ethernet eth Interface Functions.
 *
 */

#define LAN92XX_GLOBALS

#include <lan92xx.h>
#if defined(__RX610__)
#include <structs_bits_big.h>
#endif
#include <mcuDependent.h>
#include <sleep.h>
#include <stdio.h>

struct Tlocal_lan
{
    union TUnion32Bits extra_dword;
    uint8_t extra_dword_size;
    uint16_t stats_fifo_overrun; // Todo move this to eth.stats as a property.
} local_lan;



uint8_t Lan92xxSendPacket(const void * packet_data, uint16_t packet_size)
{
    // Just write Tx FIFO in one time (one segment = full packet)
    return Lan92xxSendPacketOpt(packet_data, packet_size, packet_size, TX_CMDA_FIRST_SEG | TX_CMDA_LAST_SEG, 0);
}


uint8_t Lan92xxSendPacketOpt(const void * segment, uint16_t segment_size, uint16_t packet_size, uint32_t optA,
                             uint32_t optB)
{
    uint32_t      TXcommandA;
    uint32_t      data;
    uint16_t      ii;
    uint16_t      segment_dword_size;
    uint16_t      timeout_us;

    // check size boundary
    if (packet_size > 0x07FF)
    {
        return 1;
    }

    // check enough space is available in the FIFO, if not, wait few us

    if (segment_size > (ETH_TX_FIFO_INF_P & 0xFFFF))
    {
        timeout_us = 80;
        local_lan.stats_fifo_overrun++;

        while ((segment_size > (ETH_TX_FIFO_INF_P & 0xFFFF)) && timeout_us--)
        {
            sleepUs(1);
        }

        if (timeout_us == 0)
        {
            // Timeout waiting for fifo to be empty
            return 2;
        }
    }

    if ((DMAC0.DMCRE.BIT.DEN == 1) || (DMAC1.DMCRE.BIT.DEN == 1)) // DMA is in progress
    {
        return 3;
    }

    if (!Lan92xxIsTxReady())
    {
        return 4;
    }

    // Write command A & B
    // /!\ Command A & B must not be byte swapped, because they're not data but processed by the chip
    // (even though header are also processed by the chip and need byte swapping!!!)
    // Thus, we use direct FIFO (using FIFO_SEL) w/o byte swapping for commands, port FIFO with byte swapping for data

    // Command A :  no Interrupt on Completion, no offset
    //              one buffer for packet (buffer is first and last segment)
    TXcommandA = segment_size | optA;
    *(REG32U *)(ETH_TX_DATA_FIFO_PORT_32 + 0x00000100) = TXcommandA;

    // Command B :  use CRC, use frame padding if < 64
    //              no packet tag by default
    *(REG32U *)(ETH_TX_DATA_FIFO_PORT_32 + 0x00000100) = (uint32_t) packet_size | optB;

    // Total number of bytes to be written is (segment_size+data_offset)
    // divide by 4 and ceil to get size in DWORDS
    segment_dword_size = (segment_size >> 2) + ((segment_size & 0x0003) ? 1 : 0);


    if (segment != NULL)
    {
        for (ii = 0; ii < segment_dword_size; ii++)
        {
            data = *(uint32_t *) segment;
            segment = ((uint32_t *) segment) + 1;
            ETH_TX_DATA_FIFO_PORT_P = data;
        }
    }

    if (optA & 0x80000000)
    {
        // TODO : Not implemented, useless so far
        // wait status  ..polling
        // read status
        // if error return error
    }

    return 0;
}


void lan92xxDiscardPacket(uint16_t byte_size)
{
    uint16_t dword_size = (byte_size >> 2) + (byte_size & 0xFF ? 1 : 0);

    if ((0 < dword_size) && (dword_size <= 4)) // 4 DWORDS
    {
        while (dword_size != 0)
        {
            // FGC3: ETH_RX_DATA_FIFO_PORT_P is volatile so is not removed by the optimization
            // read to reset
            if (ETH_RX_DATA_FIFO_PORT_P)
            {
            }

            dword_size--;
        }
    }
    else
    {
        ETH_RX_DP_CTL_P |= 0x80000000; //RX_FFWD bit ToDO define it in xml

        // wait for self-clear bit
        while (ETH_RX_DP_CTL_P & 0x80000000)
        {
            // FGC3: ETH_BYTE_TEST_P is volatile so is not removed by the optimization
            // read to reset
            if (ETH_BYTE_TEST_P)
            {
            }
        }
    }

    local_lan.extra_dword_size = 0; // Flush read buffer
}


void Lan92xxRxDump(void)
{
    local_lan.extra_dword_size = 0;             // Flush read buffer

    // halt RX
    ETH_MAC_CSR_CMD_P = 0xC0000001;

    while (ETH_MAC_CSR_CMD_P & 0x80000000);     // wait CSR busy flag is cleared

    ETH_MAC_CSR_DATA_P &= ~0x00000004;          // halt RX
    ETH_MAC_CSR_CMD_P = 0x80000001;             // write MAC_CR

    ETH_RX_CFG_P |= 0x00008000;                 // RX dump

    while (ETH_RX_CFG_P & 0x00008000);          // wait RX_DUMP bit is cleared

    //  restart RX
    ETH_MAC_CSR_CMD_P = 0xC0000001;

    while (ETH_MAC_CSR_CMD_P & 0x80000000);     // wait CSR busy flag is cleared

    ETH_MAC_CSR_DATA_P |= 0x00000004;
    ETH_MAC_CSR_CMD_P = 0x80000001;             // write MAC_CR

}


void Lan92xxReadFifo(const void * to, uint16_t read_length, uint16_t * fifo_length)
{
    /*
     * Todo check if portable for a little endian processor
     */

    uint16_t dword_length;
    uint16_t nb_byte_left;
    uint16_t ii = 0;

    // if bytes left from last read, recover them
    if (local_lan.extra_dword_size != 0)
    {
        while (local_lan.extra_dword_size && read_length)
        {
            (*(union TUnion32Bits *) to).byte[ii++] = local_lan.extra_dword.byte[4
                                                      - local_lan.extra_dword_size];
            local_lan.extra_dword_size--;
            read_length--;
        }

        to = (uint8_t *) to + ii;
    }

    if (read_length > *fifo_length)
    {
        read_length = *fifo_length; // truncate to number of data available
    }

    nb_byte_left = read_length & 0x0003;
    dword_length = read_length >> 2;

    // copy by DWORD access
    for (ii = 0; ii < dword_length; ii++)
    {
        (*(union TUnion32Bits *) to).int32u = ETH_RX_DATA_FIFO_PORT_P;
        to = ((union TUnion32Bits *) to) + 1;
    }

    // if non DWORD access, byte access for last one, and save extra bytes
    if (nb_byte_left)
    {

        dword_length++;
        local_lan.extra_dword.int32u = ETH_RX_DATA_FIFO_PORT_P;
        local_lan.extra_dword_size = (sizeof(uint32_t) / sizeof(uint8_t)) - nb_byte_left;

        for (ii = 0; ii < nb_byte_left; ii++)
        {
            (*(union TUnion32Bits *) to).byte[ii] = local_lan.extra_dword.byte[ii];  //
        }
    }
    else
    {
        local_lan.extra_dword_size = 0;
    }

    // update fifo_length
    *fifo_length -= dword_length * sizeof(uint32_t);
}


void Lan92xxSetMac(uint8_t mac[6])
{
    uint32_t mac_addr;
    uint8_t * p_mac_addr;
    uint16_t ii;

    // mac address "high" (two low bytes)
    p_mac_addr = (uint8_t *) &mac_addr;
    p_mac_addr += 2; // dont use high word

    for (ii = 0; ii < 2; ii++)
    {
        p_mac_addr[ii] = mac[5 - ii];
    }

    ETH_MAC_CSR_DATA_P = mac_addr & 0x0000FFFF;
    ETH_MAC_CSR_CMD_P = 0x80000002;

    // mac address "low" (four high bytes)
    p_mac_addr = (uint8_t *) &mac_addr;

    for (ii = 0; ii < 4; ii++)
    {
        p_mac_addr[ii] = mac[3 - ii];
    }

    ETH_MAC_CSR_DATA_P = mac_addr;
    ETH_MAC_CSR_CMD_P = 0x80000003;
}


void Lan92xxGetMac(uint8_t mac[6])
{
    uint32_t mac_addr;
    uint8_t * p_mac_addr;
    uint16_t ii;

    // mac address "high" (two low bytes)
    ETH_MAC_CSR_CMD_P = 0xC0000002;
    mac_addr = ETH_MAC_CSR_DATA_P & 0x0000FFFF;
    p_mac_addr = (uint8_t *) &mac_addr;
    p_mac_addr += 2; // discard high word

    for (ii = 0; ii < 2; ii++)
    {
        mac[5 - ii] = p_mac_addr[ii];
    }

    // mac address "low" (four high bytes)
    ETH_MAC_CSR_CMD_P = 0xC0000003;
    mac_addr = ETH_MAC_CSR_DATA_P;
    p_mac_addr = (uint8_t *) &mac_addr;

    for (ii = 0; ii < 4; ii++)
    {
        mac[3 - ii] = p_mac_addr[ii];
    }
}


uint8_t Lan92xxLoadMac(void)
{
    ETH_E2P_CMD_P = 0x8000000 | 0x70000000;     // Reload command

    while (ETH_E2P_CMD_P & 0x8000000);          // wait ready

    if (!(ETH_E2P_CMD_P & 0x00000100))          // not successfully loaded
    {
        return 1;
    }

    return 0;
}


uint16_t Lan92xxReadPhyReg(enum lan92_phy_registers const reg)
{
    // Wait busy flag is OK
    ETH_MAC_CSR_CMD_P = 0xC0000006;

    while (ETH_MAC_CSR_CMD_P & 0x80000000);     // wait CSR busy flag is cleared

    while (ETH_MAC_CSR_DATA_P & 0x00000001)
    {
        ETH_MAC_CSR_CMD_P = 0xC0000006;

        while (ETH_MAC_CSR_CMD_P & 0x80000000); // wait CSR busy flag is cleared
    }

    // Write MII_ACC with read command
    ETH_MAC_CSR_DATA_P = 0x000000801 | (((uint32_t)reg << 6) & 0x000007C0);
    ETH_MAC_CSR_CMD_P = 0x80000006;

    // Wait busy flag is ok
    ETH_MAC_CSR_CMD_P = 0xC0000006;

    while (ETH_MAC_CSR_CMD_P & 0x80000000);     // wait CSR busy flag is cleared

    while (ETH_MAC_CSR_DATA_P & 0x00000001)
    {
        ETH_MAC_CSR_CMD_P = 0xC0000006;

        while (ETH_MAC_CSR_CMD_P & 0x80000000); // wait CSR busy flag is cleared
    }

    // Read data
    ETH_MAC_CSR_CMD_P = 0xC0000007;

    while (ETH_MAC_CSR_CMD_P & 0x80000000);     // wait CSR busy flag is cleared

    return (ETH_MAC_CSR_DATA_P & 0x0000FFFF);
}


void Lan92xxWritePhyReg(uint16_t value, enum lan92_phy_registers const reg)
{
    // Wait busy flag is OK
    ETH_MAC_CSR_CMD_P = 0xC0000006;

    while (ETH_MAC_CSR_CMD_P & 0x80000000);     // wait CSR busy flag is cleared

    while (ETH_MAC_CSR_DATA_P & 0x00000001)
    {
        ETH_MAC_CSR_CMD_P = 0xC0000006;

        while (ETH_MAC_CSR_CMD_P & 0x80000000); // wait CSR busy flag is cleared
    }

    // Write value in MII_DATA
    // Write MII_ACC with read command
    ETH_MAC_CSR_DATA_P = value;
    ETH_MAC_CSR_CMD_P = 0x80000007;

    // Wait busy flag is OK
    ETH_MAC_CSR_CMD_P = 0xC0000006;

    while (ETH_MAC_CSR_CMD_P & 0x80000000);     // wait CSR busy flag is cleared

    while (ETH_MAC_CSR_DATA_P & 0x00000001)
    {
        ETH_MAC_CSR_CMD_P = 0xC0000006;

        while (ETH_MAC_CSR_CMD_P & 0x80000000); // wait CSR busy flag is cleared
    }

    // Write MII_ACC with write command
    ETH_MAC_CSR_DATA_P = 0x00000802 | (((uint32_t)reg << 6) & 0x000007C0);
    ETH_MAC_CSR_CMD_P = 0x80000006;
}

// EOF
