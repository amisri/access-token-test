/*---------------------------------------------------------------------------------------------------------*\
  File:         menuDig.c

  Purpose:      Digital interface functions

  Author:       daniel.calcoen@cern.ch

  Notes:
                These menues belongs to the [Manual Test] part and [Self Test] part

  History:

    25 jul 07    doc    Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdbool.h>

#include <iodefines.h>

#include <bitmap.h>
#include <dig.h>
#include <dev.h>
#include <lssb.h>
#include <mcuDependent.h>
#include <memmap_mcu.h>
#include <menu.h>
#include <sleep.h>



// Internal variable definitions

uint32_t const __attribute__((section("SRAM"))) extended_matrix[64][8] =
{
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 0
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 1
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 2
    { 0xEFFA0200, 0xEFF50200, 0xEFAF0200, 0xEF5F0200, 0xEAFF0200, 0xEFFF0200, 0xAFFF0000, 0x4FFF0200 }, // 3
    { 0xE6FA01C0, 0xE6F501C0, 0xE6AF01C0, 0xE65F01C0, 0xE2FF0140, 0xE6FF01C0, 0xA6FF00C0, 0x46FF01C0 }, // 4
    { 0xE3D8003D, 0xE3D1003E, 0xE389003F, 0xE359003B, 0xE2D90037, 0xE3D9003F, 0xA3D9002F, 0x43D9003F }, // 5
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 6
    { 0xE7DA1A00, 0xE7D51A00, 0xE78F1A00, 0xE75F0A00, 0xE2DF1A00, 0xE7DF1A00, 0xA7DF1800, 0x47DF1A00 }, // 7
    { 0xBFFA0000, 0xBFF50000, 0xBFAF0000, 0xBF5F0000, 0xBAFF0000, 0xBFFF0000, 0xAFFF0000, 0x1FFF0000 }, // 8
    { 0xBFFA0000, 0xBFF50000, 0xBFAF0000, 0xBF5F0000, 0xBAFF0000, 0xBFFF0000, 0xAFFF0000, 0x1FFF0000 }, // 9
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 10
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 11
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 12
    { 0xBFDA2001, 0xBFD52001, 0xBF8F2001, 0xBF5F0001, 0xBADF2001, 0xBFDF2001, 0xAFDF2000, 0x1FDF2001 }, // 13
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 14
    { 0x9FFA0000, 0x9FF50000, 0x9FAF0000, 0x9F5F0000, 0x9AFF0000, 0x9FFF0000, 0x8FFF0000, 0x1FFF0000 }, // 15
    { 0xA3FAC600, 0xA3F5C600, 0xA3AFC600, 0xA35FC600, 0xA2FF8600, 0xA1FFC200, 0xA3FF4400, 0x03FFC600 }, // 16
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 17
    { 0xA3FAC600, 0xA3F5C600, 0xA3AFC600, 0xA35FC600, 0xA2FF8600, 0xA3FFC600, 0xA3FF4400, 0x03FFC600 }, // 18
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 19
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 20
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 21
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 22
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 23
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 24
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 25
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 26
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 27
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 28
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 29
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 30
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 31
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 32
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 33
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 34
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 35
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 36
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 37
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 38
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 39
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 40
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 41
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 42
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 43
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 44
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 45
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 46
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 47
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 48
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 49
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 50
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 51
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 52
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 53
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 54
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 55
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 56
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 57
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 58
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 59
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 60
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }, // 61
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xF5FF0000, 0xAFFF0000, 0x5FFF0000 }, // 62
    { 0xFFFA0000, 0xFFF50000, 0xFFAF0000, 0xFF5F0000, 0xFAFF0000, 0xFFFF0000, 0xAFFF0000, 0x5FFF0000 }  // 63
};



// External function definitions



void MenuMTdigCtrl(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] enable/disable CMD output to the power converter
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t ena;

    if (MenuGetInt16U(argv[0], 0, 1, &ena) != 0)
    {
        return;
    }

    if (ena != 0)
    {
        P7.DR.BIT.B6 = 0;                                   // Enable digital CMD output
        CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_DIGITAL_MASK16; // Clear dig reset
    }
    else
    {
        P7.DR.BIT.B6 = 1;                                   // disable digital CMD output
        CPU_RESET_CTRL_P |= CPU_RESET_CTRL_DIGITAL_MASK16;  // set dig reset
    }
}



void MenuSTdigCmds(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function tests Dig interface (write & read back).

  Commands are tested by setting the equivalent bit in the DIG_OP register. In the reception crate, bit 0 of
  DIG_OP_SET and DIG_OP_RST is connected to bit 0 and 2 of DIG_IP, bit 1 to bits 1 and 3, ..., bit 7 to
  bits 13 and 15.

  DIG_IP is the primary digital inputs register. These software inputs can be a combination of more than one
  actual signals (e.g. PWRFAILURE). DIG_IPDIRECT is the raw inputs register and does not have a 1-1 connection
  with DIG_IP.

  Some signals are inverted in the hardware so when testing setting a command in DIG_OP, mask0 and mask1
  are used for the DIG_IP bits expected to be 0 and 1 each time.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t ii;
    uint32_t mask0;
    uint32_t mask1;

    // Test access to register

    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    if (dev.crate_type == FGC_CRATE_TYPE_RECEPTION)
    {
        // Reception crate : commands are linked to status. Commands are inverted.
        // "not" status are inverted (for instance VS_INIT_OK_NOT)
        P7.DR.BIT.B6 = 0;               // Enable digital CMD output
        CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_DIGITAL_MASK16;  // Clear dig reset

        DIG_OP_P = DIG_OP_RST_MASK16;   // Reset commands
        MSLEEP(5);

        // test IP input
        // mask 1 : status should be 1. mask0 : output should be 0
        // test IP 0 except on intlk
        mask0 = DIG_IP_VSPOWERON_MASK32 | DIG_IP_VSFAULT_MASK32 | DIG_IP_VSEXTINTLK_MASK32 |
                DIG_IP_DCCTBFLT_MASK32;
        mask0 |= DIG_IP_DCCTAFLT_MASK32 | DIG_IP_FASTABORT_MASK32 | DIG_IP_OPBLOCKED_MASK32 |
                 DIG_IP_VSNOCABLE_MASK32;
        mask1 = DIG_IP_VSREADY_MASK32 | DIG_IP_POLSWIPOS_MASK32 | DIG_IP_POLSWINEG_MASK32 | DIG_IP_VSRUN_MASK32;

        DIG_OP_P = DIG_OP_SET_MASK16;   // set IPDIRECT to 1 (output to 0)
        MSLEEP(5);

        if (((~DIG_IP_P & mask0) != mask0) || ((DIG_IP_P & mask1) != mask1))
        {
            MenuRspError("DIG IP test zero got:0x%08X exp:0x%08X", DIG_IP_P, mask1);
            return;
        }

        // IP VSRUN
        DIG_OP_P = DIG_OP_RST_MASK16;   // Reset commands
        MSLEEP(5);
        mask1 = DIG_IP_VSPOWERON_MASK32 | DIG_IP_VSEXTINTLK_MASK32;
        mask0 = DIG_IP_VSRUN_MASK32;
        DIG_OP_P = (~DIG_OP_SET_VSRUNCMD_MASK16) & DIG_OP_SET_MASK16;
        MSLEEP(5);

        if (((~DIG_IP_P & mask0) != mask0) || ((DIG_IP_P & mask1) != mask1))
        {
            MenuRspError("DIG IP VSRUN got:0x%08X exp:0x%08X", DIG_IP_P & mask1, mask1);
            return;
        }

        // IP RST DIG_OP_SET_VSRESETCMD_MASK16
        DIG_OP_P = DIG_OP_RST_MASK16;   // Reset commands
        MSLEEP(5);
        mask1 = DIG_IP_VSFAULT_MASK32;
        mask0 = DIG_IP_VSREADY_MASK32;
        DIG_OP_P = (~DIG_OP_SET_VSRESETCMD_MASK16) & DIG_OP_SET_MASK16;
        MSLEEP(5);

        if (((~DIG_IP_P & mask0) != mask0) || ((DIG_IP_P & mask1) != mask1))
        {
            MenuRspError("DIG IP VSRESET got:0x%08X exp:0x%08X", DIG_IP_P & mask1, mask1);
            return;
        }

        // IP DIG_OP_SET_UNBLOCKCMD_MASK16
        DIG_OP_P = DIG_OP_RST_MASK16;   // Reset commands
        MSLEEP(5);
        mask1 = DIG_IP_FASTABORT_MASK32 | DIG_IP_DCCTAFLT_MASK32;
        DIG_OP_P = (~DIG_OP_SET_UNBLOCKCMD_MASK16) & DIG_OP_SET_MASK16;
        MSLEEP(5);

        if ((DIG_IP_P & mask1) != mask1)
        {
            MenuRspError("DIG IP VSUNBLOCK got:0x%08X exp:0x%08X", DIG_IP_P & mask1, mask1);
            return;
        }

        // IP DIG_OP_SET_POLTOPOSCMD_MASK16
        DIG_OP_P = DIG_OP_RST_MASK16;   // Reset commands
        MSLEEP(5);
        mask1 = DIG_IP_VSNOCABLE_MASK32 | DIG_IP_DCCTBFLT_MASK32;
        DIG_OP_P = (~DIG_OP_SET_POLTOPOSCMD_MASK16) & DIG_OP_SET_MASK16;
        MSLEEP(5);

        if ((DIG_IP_P & mask1) != mask1)
        {
            MenuRspError("DIG IP POLTOPOS got:0x%08X exp:0x%08X", DIG_IP_P & mask1, mask1);
            return;
        }

        // IP DIG_OP_SET_POLTONEGCMD_MASK16
        DIG_OP_P = DIG_OP_RST_MASK16;   // Reset commands
        MSLEEP(5);
        mask1 = DIG_IP_OPBLOCKED_MASK32;
        mask0 = DIG_IP_POLSWIPOS_MASK32;
        DIG_OP_P = (~DIG_OP_SET_POLTONEGCMD_MASK16) & DIG_OP_SET_MASK16;
        MSLEEP(5);

        if (((~DIG_IP_P & mask0) != mask0) || ((DIG_IP_P & mask1) != mask1))
        {
            MenuRspError("DIG IP POLTONEG got:0x%08X exp:0x%08X", DIG_IP_P & mask1, mask1);
            return;
        }

        // IP DIG_OP_SET_AFRUNCMD_MASK16
        DIG_OP_P = DIG_OP_RST_MASK16;   // Reset commands
        MSLEEP(5);
        mask0 = DIG_IP_POLSWINEG_MASK32;
        DIG_OP_P = (~DIG_OP_SET_AFRUNCMD_MASK16) & DIG_OP_SET_MASK16;
        MSLEEP(5);

        if ((~DIG_IP_P & mask0) != mask0)
        {
            MenuRspError("DIG IP CMD5 got:0x%08X exp:0x%08X", DIG_IP_P & mask0, 0);
            return;
        }

        // test IP intlk
        // test 0
        mask0 = DIG_IP_PWRFAILURE_MASK32 | DIG_IP_INTLKOUT_MASK32;
        mask1 = DIG_IP_PCPERMIT_MASK32 | DIG_IP_INTLKSPARE_MASK32;
        DIG_INTLK_OUT_P = DIG_INTLK_OUT_RST_MASK16;     // reset interlocks
        MSLEEP(10);

        if (((~DIG_IP_P & mask0) != mask0) || ((DIG_IP_P & mask1) != mask1))
        {
            MenuRspError("DIG IP INTLK got:0x%08X exp:0x%08X", DIG_IP_P & mask1, mask1);
            return;
        }

        // test PWRFAILURE
        mask0 = DIG_IP_PCPERMIT_MASK32;
        mask1 = DIG_IP_PWRFAILURE_MASK32;
        DIG_INTLK_OUT_P = DIG_INTLK_OUT_SET_PWRFAILURE_MASK16;
        MSLEEP(10);

        if (((~DIG_IP_P & mask0) != mask0) || ((DIG_IP_P & mask1) != mask1))
        {
            MenuRspError("DIG IP INTLK got:0x%08X exp:0x%08X", DIG_IP_P & mask1, mask1);
            return;
        }

        DIG_INTLK_OUT_P = DIG_INTLK_OUT_RST_MASK16;     // Reset interlocks
        MSLEEP(10);
        // test INTLK_OUT
        mask0 = DIG_IP_INTLKSPARE_MASK32;
        mask1 = DIG_IP_INTLKOUT_MASK32;
        DIG_INTLK_OUT_P = DIG_INTLK_OUT_SET_SPARE_MASK16;
        MSLEEP(10);

        if (((~DIG_IP_P & mask0) != mask0) || ((DIG_IP_P & mask1) != mask1))
        {
            MenuRspError("DIG IP INTLK got:0x%08X exp:0x%08X", DIG_IP_P & mask1, mask1);
            return;
        }
    }
    else
    {
        P7 .DR.BIT.B6 = 1;       // disable digital CMD output

        for (ii = 0; ii <= 0xFF; ii++)
        {
            DIG_OP_P = DIG_OP_RST_MASK16;       // Reset commands
            DIG_OP_P = ii;                      // Set commands

            if (DIG_OP_P != ii)  // Readback fails
            {
                MenuRspError("DIG cmd rbck got:0x%04X exp:0x%04X", DIG_OP_P, ii);
                DIG_OP_P = DIG_OP_RST_MASK16;   // Reset commands
                return;
            }
        }

        DIG_OP_P = DIG_OP_RST_MASK16;   // Reset commands
    }
}



void MenuSTdigRawInputs(uint16_t argc, char ** argv)
{
    uint16_t offset;
    uint16_t crate_type;

    crate_type = dev.crate_type;

    // Enable digital CMD output

    P7.DR.BIT.B6 = 0;

    // Clear dig reset

    clrBitmap(CPU_RESET_CTRL_P, CPU_RESET_CTRL_DIGITAL_MASK16);

    // Reset commands

    DIG_OP_P = DIG_OP_RST_MASK16;
    MSLEEP(5);

    for (offset = 0; offset < 8; offset++)
    {
        if (   offset     == 0
            && crate_type != FGC_CRATE_TYPE_RECEPTION)
        {
            // IMPORTANT: Skip VSRUN command when not on reception crate
            continue;
        }

        DIG_OP_P = 1 << offset;
        MSLEEP(5);

        MenuRspArg("DIG 0x%04X cmd read back raw input got:0x%08lX exp:0x%08lX", DIG_OP_P,
                         DIG_IPDIRECT_P, extended_matrix[crate_type][offset]);

        if (DIG_IPDIRECT_P != extended_matrix[crate_type][offset])
        {
            MenuRspArg("FAILED");
        }
        else
        {
            MenuRspArg("OK");
        }

        DIG_OP_P = DIG_OP_RST_MASK16;
        MSLEEP(5);
    }
}



void MenuSTdigSup(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function tests supplemental I/O banks. It writes pulses on bank A and reads back the
  result on bank B, then repeats the procedure with the banks switched.
  There are two banks (A and B) of 5 supplemental I/O channels each.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t channel;
    uint8_t set, reset;

    // Test access to register

    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    if (dev.crate_type == FGC_CRATE_TYPE_RECEPTION)
    {
        P7.DR.BIT.B6 = 0;               // Enable digital CMD output

        // Select source for bank A and B ad Data Register
        for (channel = 0; channel < 5; channel++)
        {
            DIG_SUP_A_CHAN_CTRL_A[channel] = 0x0 | (DIG_SUP_A_CHAN_CTRL_A[channel] &
                                                    (~DIG_SUP_A_CHAN_CTRL_OUTMPX_MASK16));
            DIG_SUP_B_CHAN_CTRL_A[channel] = 0x0 | (DIG_SUP_B_CHAN_CTRL_A[channel] &
                                                    (~DIG_SUP_B_CHAN_CTRL_OUTMPX_MASK16));
        }

        // Write SUPA, read SUPB

        Set(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_PERMIT_MASK16);  // Output enabled
        Set(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_PERMIT_MASK16);  // Output enabled
        Set(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_CTRL_MASK16);    // Write
        Clr(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_CTRL_MASK16);    // Read

        fprintf(term.f, RSP_DATA ",Testing all values from 0x00 to 0x1F\n");
        fprintf(term.f, RSP_DATA ",Writing to DIG SUP A and reading back on B:\n");

        for (set = 0x00; set <= 0x1F ; set++)
        {
            reset = (~set) & 0x001F;
            DIG_SUP_A_SET_P = (reset << 8) | set;
            USLEEP(2);

            if (DIG_SUP_B_PORT_P != set)
            {
                MenuRspError("DIG SUP write A read B through backplane got:0x%04X exp:0x%04X", DIG_SUP_B_PORT_P, set);
                return;
            }
        }

        fprintf(term.f, RSP_DATA ",OK\n");

        // Write SUPB, read SUPA

        DIG_SUP_A_DIR_P &= ~DIG_SUP_A_DIR_OUT_CTRL_MASK16;       // Read
        DIG_SUP_B_DIR_P |= DIG_SUP_B_DIR_OUT_CTRL_MASK16;        // Write

        fprintf(term.f, RSP_DATA ",Writing to DIG SUP B and reading back on A:\n");

        for (set = 0x00; set <= 0x1F ; set++)
        {
            reset = (~set) & 0x001F;
            DIG_SUP_B_SET_P = (reset << 8) | set;
            USLEEP(2);

            if (DIG_SUP_A_PORT_P != set)
            {
                MenuRspError("DIG SUP write B read A through backplane got:0x%04X exp:0x%04X", DIG_SUP_A_PORT_P, set);
                return;
            }
        }

        fprintf(term.f, RSP_DATA ",OK\n");
    }
    else
    {
        MenuRspError("Not implemented for this backplane type:0x%04X", dev.crate_type);
        return;
    }
}



void MenuMTdigSupPulse(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This function generates 5 pulses on each pin of bank A. bank B is set as input
\*---------------------------------------------------------------------------------------------------------*/
{

    uint32_t delay, width;
    uint16_t channel;

    if ((MenuGetInt32U(argv[0], 0, 0xFFFFFFFF, &delay) != 0) ||
        (MenuGetInt32U(argv[1], 0, 0xFFFFFFFF, &width) != 0))
    {
        return;
    }

    // Test access to register
    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Set time till event to ~15 minutes to give (more than enough) time for setting up the test

    TIME_TILL_EVT_US_P = 1000000000;

    DigInit();

    DIG_SUP_A_ARM_P = 0;    // Reset arming to unlock acces to PULSE_ETIM_US and PULSE_WIDTH_US

    for (channel = 0; channel < 4; channel++)
    {
        DIG_SUP_A_PULSE_ETIM_US_A[channel]  = delay * channel;
        DIG_SUP_A_PULSE_WIDTH_US_A[channel] = width;
    }

    DIG_SUP_A_ARM_P = 0xF;

    // For some reason we need to wait some time before setting the TIME_TILL_EVT register again

    MSLEEP(10);

    // Set the event time just after all pulses are generated

    TIME_TILL_EVT_US_P = delay * 4;
}



void MenuMTdigSupSquare(uint16_t argc, char ** argv)
{
    // Test access to register

    if(dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    if(main_misc.dig_sup_test)
    {
        main_misc.dig_sup_test = false;

        main_misc.dig_sup_test_counter = 0;
        main_misc.dig_sup_test_duty    = 0;

        // TODO Reset Dig_sup state?
    }
    else
    {
        uint16_t duty;

        if(MenuGetInt16U(argv[0], 0, 5000, &duty) != 0)
        {
            return;
        }

        DigInit();

        if(DigSupSetSourceBankA(DIG_SUP_A_CHANNEL0, DIG_SUP_SOURCE_DIRECT) != DIG_SUP_STATUS_SUCCESS)
        {
            MenuRspError("Failed to set source");
            return;
        }

        main_misc.dig_sup_test_counter = 0;
        main_misc.dig_sup_test_duty    = duty;

        main_misc.dig_sup_test = true;
    }
}



void MenuMTdigSupRead(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test] This function shows supplemental I/O banks state.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Test access to register

    uint16_t channel;

    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    MenuRspArg("DIG SUP A : 0x%04X\n", DIG_SUP_A_PORT_P);
    MenuRspArg("DIG SUP B : 0x%04X\n", DIG_SUP_B_PORT_P);

    for (channel = 0; channel < 4; channel++)
    {
        MenuRspArg("  DIG SUP A CHAN %d CTRL : 0x%04X", channel, DIG_SUP_A_CHAN_CTRL_A[channel]);
        MenuRspArg("DIG SUP B CHAN %d CTRL : 0x%04X\n", channel, DIG_SUP_B_CHAN_CTRL_A[channel]);
    }

    for (channel = 0; channel < 4; channel++)
    {
        MenuRspArg("DIG SUP A channel %d, Pulse time to event : %d us", channel, DIG_SUP_A_PULSE_ETIM_US_A[channel]);
        MenuRspArg("                  %d, Pulse width         : %d us", channel, DIG_SUP_A_PULSE_WIDTH_US_A[channel]);
    }

    for (channel = 0; channel < 4; channel++)
    {
        MenuRspArg("DIG SUP B channel %d, Rise time to event : %d us", channel, DIG_SUP_B_RISE_ETIM_US_A[channel]);
        MenuRspArg("                  %d, Fall time to event : %d us", channel, DIG_SUP_B_FALL_ETIM_US_A[channel]);
    }
}



void MenuMTdigSupTest(uint16_t argc, char ** argv)
{
    uint32_t delay, width;
    uint32_t time_stamp;
    uint16_t channel;

    bool error = false;

    if ((MenuGetInt32U(argv[0], 0, 0xFFFFFFFF, &delay) != 0) ||
        (MenuGetInt32U(argv[1], 0, 0xFFFFFFFF, &width) != 0))
    {
        return;
    }

    // Test access to register
    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Set time till event to ~15 minutes to give (more than enough) time for setting up the test

    TIME_TILL_EVT_US_P = 1000000000;

    DigInit();

    DIG_SUP_A_ARM_P = 0;    // Reset arming to unlock acces to PULSE_ETIM_US and PULSE_WIDTH_US

    for (channel = 0; channel < 4; channel++)
    {
        DIG_SUP_A_PULSE_ETIM_US_A[channel]  = delay * channel;
        DIG_SUP_A_PULSE_WIDTH_US_A[channel] = width;
    }

    DIG_SUP_A_ARM_P = 0xF;

    // For some reason we need to wait some time before setting the TIME_TILL_EVT register again

    MSLEEP(10);

    // Set the event time just after all pulses are generated

    TIME_TILL_EVT_US_P = delay * 4;

    // Wait for event to occur before testing response

    MSLEEP(1000);


    // Test pulses

    for (channel = 0; channel < 4; channel++)
    {
        // Verify pulse information on bank A

        // Delay

        if (DIG_SUP_A_PULSE_ETIM_US_A[channel] != (delay * channel))
        {
            fprintf(term.f, RSP_DATA ",DIG SUP A channel %u, incorrect pulse time to event.\n", channel);
            fprintf(term.f, RSP_DATA ",Got %lu us and expected %lu us.\n", DIG_SUP_A_PULSE_ETIM_US_A[channel], (delay * channel));

            error = true;
        }

        // Width

        if (DIG_SUP_A_PULSE_WIDTH_US_A[channel] != width)
        {
            fprintf(term.f, RSP_DATA ",DIG SUP A channel %u, incorrect width.\n", channel);
            fprintf(term.f, RSP_DATA ",Got %lu us and expected %lu us.\n", DIG_SUP_A_PULSE_WIDTH_US_A[channel], width);

            error = true;
        }


        // Verify event info on bank B

        // Rise time to event

        if (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_RISING_EDGE_MASK16) == false)
        {
            fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, no rising edge detected.\n", channel);

            error = true;
        }
        else if ((time_stamp = (uint32_t) DIG_SUP_B_RISE_ETIM_US_A[channel]) != (delay * channel))
        {
            fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, incorrect rise time to event.\n", channel);
            fprintf(term.f, RSP_DATA ",Got %d us and expected %d us.\n", (int)time_stamp, (int)(delay * channel));

            error = true;
        }

        if (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_REPEATED_RISE_MASK16) == true)
        {
            fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, repeated rising edge detected.\n", channel);

            error = true;
        }

        // Fall time to event

        if (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_FALLING_EDGE_MASK16) == false)
        {
            fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, no falling edge detected.\n", channel);

            error = true;
        }
        else if ((time_stamp = (uint32_t) DIG_SUP_B_FALL_ETIM_US_A[channel]) != (delay * channel - width))
        {
            fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, incorrect fall time to event.\n", channel);
            fprintf(term.f, RSP_DATA ",Got %d us and expected %d us.\n", (int)time_stamp, (int)(delay * channel - width));

            error = true;
        }

        if (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_REPEATED_FALL_MASK16) == true)
        {
            fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, repeated falling edge detected.\n", channel);

            error = true;
        }
    }

    if (error == true)
    {
        MenuRspError("FAILED");
    }
}



void MenuMTdigSupSetA(uint16_t argc, char ** argv)
{
    uint16_t channel;
    uint16_t value;
    uint8_t  set, reset;

    // Test access to register

    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    if(MenuGetInt16U(argv[0], 0, 0x1F, &value) != 0)
    {
        return;
    }

    // Enable digital CMD output

    P7.DR.BIT.B6 = 0;

    // Select source for bank A and B as Data Register
    for (channel = 0; channel < 5; channel++)
    {
        DIG_SUP_A_CHAN_CTRL_A[channel] = 0x0 | (DIG_SUP_A_CHAN_CTRL_A[channel] &
                                                (~DIG_SUP_A_CHAN_CTRL_OUTMPX_MASK16));
        DIG_SUP_B_CHAN_CTRL_A[channel] = 0x0 | (DIG_SUP_B_CHAN_CTRL_A[channel] &
                                                (~DIG_SUP_B_CHAN_CTRL_OUTMPX_MASK16));
    }

    // Write SUPA, read SUPB

    Set(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_PERMIT_MASK16);  // Output enabled
    Set(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_PERMIT_MASK16);  // Output enabled
    Set(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_CTRL_MASK16);    // Write
    Clr(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_CTRL_MASK16);    // Read

    set   = value;
    reset = (~value) & 0x001F;
    DIG_SUP_A_SET_P = (reset << 8) | set;
    USLEEP(2);

    fprintf(term.f, RSP_DATA ",SET A: 0x%04X\n", DIG_SUP_A_SET_P);
}



void MenuSTdigSupTest(uint16_t argc, char ** argv)
{
    uint32_t delay, width;
    uint32_t time_stamp;
    double   progress;
    uint16_t channel;

    uint16_t num_iterations = 5;
    bool     error          = false;

    // Test access to register
    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    for (int i = 1; i < num_iterations + 1; ++i)
    {
        delay = width = 10 * i;

        // Set time till event to ~15 minutes to give (more than enough) time for setting up the test

        TIME_TILL_EVT_US_P = 1000000000;

        DigInit();

        DIG_SUP_A_ARM_P = 0;    // Reset arming to unlock acces to PULSE_ETIM_US and PULSE_WIDTH_US

        for (channel = 0; channel < 4; channel++)
        {
            DIG_SUP_A_PULSE_ETIM_US_A[channel]  = delay * channel;
            DIG_SUP_A_PULSE_WIDTH_US_A[channel] = width;
        }

        DIG_SUP_A_ARM_P = 0xF;

        // For some reason we need to wait some time before setting the TIME_TILL_EVT register again

        MSLEEP(10);

        // Set the event time just after all pulses are generated

        TIME_TILL_EVT_US_P = delay * 4;

        // Wait for event to occur before testing response

        MSLEEP(1000);


        // Test pulses

        for (channel = 0; channel < 4; channel++)
        {
            // Verify pulse information on bank A

            // Delay

            if (DIG_SUP_A_PULSE_ETIM_US_A[channel] != (delay * channel))
            {
                fprintf(term.f, RSP_DATA ",DIG SUP A channel %u, incorrect pulse time to event.\n", channel);
                fprintf(term.f, RSP_DATA ",Got %lu us and expected %lu us.\n", DIG_SUP_A_PULSE_ETIM_US_A[channel], (delay * channel));

                error = true;
            }

            // Width

            if (DIG_SUP_A_PULSE_WIDTH_US_A[channel] != width)
            {
                fprintf(term.f, RSP_DATA ",DIG SUP A channel %u, incorrect width.\n", channel);
                fprintf(term.f, RSP_DATA ",Got %lu us and expected %lu us.\n", DIG_SUP_A_PULSE_WIDTH_US_A[channel], width);

                error = true;
            }


            // Verify event info on bank B

            // Rise time to event

            if (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_RISING_EDGE_MASK16) == false)
            {
                fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, no rising edge detected.\n", channel);

                error = true;
            }
            else if ((time_stamp = (uint32_t) DIG_SUP_B_RISE_ETIM_US_A[channel]) != (delay * channel))
            {
                fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, incorrect rise time to event.\n", channel);
                fprintf(term.f, RSP_DATA ",Got %d us and expected %d us.\n", (int)time_stamp, (int)(delay * channel));

                error = true;
            }

            if (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_REPEATED_RISE_MASK16) == true)
            {
                fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, repeated rising edge detected.\n", channel);

                error = true;
            }

            // Fall time to event

            if (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_FALLING_EDGE_MASK16) == false)
            {
                fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, no falling edge detected.\n", channel);

                error = true;
            }
            else if ((time_stamp = (uint32_t) DIG_SUP_B_FALL_ETIM_US_A[channel]) != (delay * channel - width))
            {
                fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, incorrect fall time to event.\n", channel);
                fprintf(term.f, RSP_DATA ",Got %d us and expected %d us.\n", (int)time_stamp, (int)(delay * channel - width));

                error = true;
            }

            if (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_REPEATED_FALL_MASK16) == true)
            {
                fprintf(term.f, RSP_DATA ",DIG SUP B channel %u, repeated falling edge detected.\n", channel);

                error = true;
            }
        }

        progress = (double)i/num_iterations * 100;
        fprintf(term.f, RSP_PROGRESS ",%3u%%\n", (uint16_t)progress);
    }

    if (error == true)
    {
        MenuRspError("FAILED");
    }
}



void MenuMTdigCmd(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t cmd;

    if (MenuGetInt16U(argv[0], 0, 0xFFFF, &cmd) != 0)
    {
        return;
    }

    DIG_OP_P = cmd;

    USLEEP(2);

    MenuRspArg("DIG cmd:0x%04X", DIG_OP_P);
}



void MenuMTdigRaw(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
\*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("DIG RAW: 0x%08lX", DIG_IPDIRECT_P); // direct read of the raw input signals
}



void MenuSTdigIntlk(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test] This function tests Interlocks (write & read back on 2nd contact of relay).
  In the reception backplane, the output is connected to the input, but the FPGA inverts the input.
  So, when setting DIG_INTLK_OUT to 0x00, 0x01, 0x02 e.g., DIG_INTLK_IN is expected to be 0x03, 0x02, 0x01
  respectively.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  ii;

    // We don't play with the output signals if we are branched to a power converter
    if (FGC_CRATE_TYPE_RECEPTION != dev.crate_type)
    {
        MenuRspError("Wrong backplane type:0x%04X", dev.crate_type);
        return;
    }

    P7.DR.BIT.B6 = 0;           // Enable digital CMD output
    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_DIGITAL_MASK16;      // Clear dig reset

    // Test access to register

    if (dev.reg.io != 0)
    {
        MenuRspError("No access");
        return;
    }

    for (ii = 1; ii < 4; ii = ii << 1)
    {
        DIG_INTLK_OUT_P = DIG_INTLK_OUT_RST_MASK16;     // Reset interlocks
        MSLEEP(10);
        DIG_INTLK_OUT_P = ii;           // Set interlocks
        MSLEEP(10);                     // Let the relay activate (3ms max in datasheet)

        if (DIG_INTLK_RB_P != ii)       // Readback fails
        {
            MenuRspError("DIG intlk readback got:0x%04X exp:0x%04X", DIG_INTLK_RB_P, ii);
            return;
        }

        if ((~DIG_INTLK_IN_P & 0x03) != ii) // Intlk IN is link back to INTLK OUT
        {
            MenuRspError("DIG intlk input got:0x%04X exp:0x%04X", ~DIG_INTLK_IN_P, ii);
            return;
        }
    }
}



void MenuMTdigIntlk(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t cmd;

    if (MenuGetInt16U(argv[0], 0, 0x300, &cmd) != 0)
    {
        return;
    }

    DIG_INTLK_OUT_P = cmd;

    MSLEEP(5);                  // 3ms max in datasheet

    MenuRspArg("DIG Intlk cmd:0x%02X - cmd stat:0x%02X", cmd, DIG_INTLK_RB_P);
}



void MenuMTdigGetIntlk(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
\*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("DIG interlock entries :0x%02X", DIG_INTLK_IN_P);
}

// EOF
