/*---------------------------------------------------------------------------------------------------------*\
 File:          state_class.c

 Notes:         This file contains the functions for the PC state machine.  The generic state
                machine engine is in sta.c.  The states and transitions are defined in states.def.
                These functions are called every five milliseconds.  The first time a function is
                called, the state value in main_misc.state_pc contains the old state and first_f is TRUE.

    signals from ACAPULCO

    POWER_ON - DIG_IPDIRECT:b0 - DIG_IP_VSPOWERON_MASK32
        0: VS power stage is not powered
        1: VS power stage is powered

    VS_INIT_OK_NOT - DIG_IPDIRECT:b1 - inv(DIG_IP_VSREADY_MASK32)
        0: VS has finished its initialisation
        1: VS is not ready

    VS_EXTINTLK - DIG_IPDIRECT:b2 - DIG_IP_VSEXTINTLK_MASK32
        0: VS has no external fault
        1: VS has an external fault

    VS_FAULT - DIG_IPDIRECT:b3 - DIG_IP_VSFAULT_MASK32
        0: VS has no internal fault
        1: VS has an internal fault

    FAST_ABORT_MEM - DIG_IPDIRECT:b4 - DIG_IP_FASTABORT_MASK32
    (in the FGC3 extansion panel STATUS is labeled : FAST ABORT)
        0: PC_FAST_ABORT signal (from PIC) was not received by the VS
        1: VS has received the PC_FAST_ABORT signal from PIC

    VS_CABLE_OK_NOT - DIG_IPDIRECT:b5 - DIG_IP_VSNOCABLE_MASK32
    (in the FGC3 extansion panel STATUS is labeled : NO VS CABLE)
        0: FGC3-VS cable is connected
        1: FGC3-VS cable is disconnected

    IMEAS_A_FAULT - DIG_IPDIRECT:b6 - DIG_IP_DCCTAFLT_MASK32
    (in the FGC3 extansion panel STATUS is labeled : IMEAS A ERR)
        0: channel A read OK
        1: channel A read not OK

    IMEAS_B_FAULT - DIG_IPDIRECT:b7 - DIG_IP_DCCTBFLT_MASK32
    (in the FGC3 extansion panel STATUS is labeled : IMEAS B ERR)
        0: channel B read OK
        1: channel B read not OK

    OUTPUT_BLOCK_STATE - DIG_IPDIRECT:b8 - DIG_IP_OPBLOCKED_MASK32
    (in the FGC3 extansion panel STATUS is labeled : OUTPUT BLOCKED)
        0: output power stage is enabled
        1: output power stage is blocked


    PC_PERMIT_NOT - DIG_INTLK.IN:b0 - inv(DIG_IP_PCPERMIT_MASK32)
        0: run of power converter allowed
        1: power converter to OFF controlled by the FGC3 (Slow Power Abort)

\*---------------------------------------------------------------------------------------------------------*/

#define DEFPROPS_INC_ALL    // defprops.h

#include <state_class.h>
#include <stdio.h>              // for fprintf()
#include <macros.h>             // for Test(), Set()
#include <stdint.h>           // basic typedefs
#include <memmap_mcu.h>         // Include memory map consts
#include <boot_dsp_cmds.h>      // constants shared by DSP and CPU
#include <dsp_cmds.h>           // for DSP_SLOW_CMD_TIMEOUT_10MS, SendDspSlowCmdAndWaitForRsp()
#include <statesTask.h>         // for sta global variable
#include <boot_dual_port_ram.h> // for dpram global variable
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();

/*---------------------------------------------------------------------------------------------------------*/
void RefClr(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to clear a reference.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;


    OS_ENTER_CRITICAL();        // Disable interrupts, except timers that are level 7
    // prepare argument for the command
    dpram->mcu.slowArg.data = 0;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_DAC1, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
    {
        //      fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
    }

    OS_EXIT_CRITICAL();
}
/*---------------------------------------------------------------------------------------------------------*/
void StateFO(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  FO : FAULT_OFF state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)
    {
        sta.flags = 0;                                  // Clear all flags
        RefClr();                                       // set current to 0
    }

    // activate VS output stage blocking (pin C19, OUTPUT_BLOCK)
    DIG_OP_P = DIG_OP_RST_UNBLOCKCMD_MASK16;

    // OFF the Power Converter
    DIG_OP_P = DIG_OP_RST_VSRUNCMD_MASK16;
}
/*---------------------------------------------------------------------------------------------------------*/
void StateOF(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  OF : OFF state function.

  Setting DIG_OP_SET_VSRUNCMD_MASK16 will close the open collector VS_RUN_NOT output (pin D19).
  The hardwired logic includes a protection that prevents this action unless the PC_PERMIT_NOT external
  interlock is close.
  Note that if the external interlock PC_PERMIT_NOT becomes open, VS_RUN_NOT is NOT automatically deactivated,
  as this is considered to be a Slow Power Abort condition, and the voltage source must remain ON to
  allow for a controlled reduction in the current.

  Reading back this bit will show the state of the driver exciting the open collector.
  This state is copied to DIG_IP_VSRUN.

  Note: This action will be deactivated automatically by the hardwired logic if FASTABORT or
  PWRFAILURE become active.

  M32C87 p14.2 must be enable.
  RX610  P7.6  must be enable.

\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        //      sta.flags = 0;                                  // enable to go to starting
        RefClr();                                       // set current to 0
    }

    // OFF the Power Converter
    DIG_OP_P = DIG_OP_RST_VSRUNCMD_MASK16;

    // activate VS output stage blocking (pin C19, OUTPUT_BLOCK)
    DIG_OP_P = DIG_OP_RST_UNBLOCKCMD_MASK16;
}
/*---------------------------------------------------------------------------------------------------------*/
void StateFS(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  FS : FAULT_STOPPING state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        RefClr();                                       // set current to 0
        DIG_OP_P = DIG_OP_RST_UNBLOCKCMD_MASK16; // activate VS output stage blocking (pin C19, OUTPUT_BLOCK)
        // prepare argument for the command
        dpram->mcu.slowArg.data = FALSE;

        // it contains 1ms sleep
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_START_STOP_REGULATION, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            //          fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateSP(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  SP : STOPPING state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        sta.flags &= ~STAF_STOP;                        // clear STOP flag
        RefClr();                                       // set current to 0
        DIG_OP_P = DIG_OP_RST_UNBLOCKCMD_MASK16; // activate VS output stage blocking (pin C19, OUTPUT_BLOCK)
        DIG_OP_P = DIG_OP_RST_VSRUNCMD_MASK16;           // disable the Power Converter
        // prepare argument for the command
        dpram->mcu.slowArg.data = FALSE;

        // it contains 1ms sleep
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_START_STOP_REGULATION, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            //          fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateST(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  ST : STARTING state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        sta.flags |= STAF_IDLE;                         // enable to go to IDLE (DC)
    }

    // the Acapulco needs both POWER ON and VS RUN to be 00 to reset its logic
    // so if we set VS RUN while POWER ON is active, will not be detected by Acapulco
    if (!Test(sta.inputs, DIG_IP_VSPOWERON_MASK32))
    {
        DIG_OP_P = DIG_OP_SET_VSRUNCMD_MASK16;           // ON the Power Converter
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateSA(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  SA : SLOW_ABORT state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        RefClr();       // set current to 0
        sta.flags &= ~STAF_SLOW_ABORT;                  // Clear SLOW ABORT flag
        // prepare argument for the command
        dpram->mcu.slowArg.data = FALSE;

        // it contains 1ms sleep
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_START_STOP_REGULATION, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            //          fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateTS(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  TS : TO_STANDBY state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        RefClr();       // set current to 0
        DIG_OP_P = DIG_OP_RST_UNBLOCKCMD_MASK16; // activate VS output stage blocking (pin C19, OUTPUT_BLOCK)
        //      sta.flags |= STAF_TO_STANDBY;                   // Set TO_STANDBY flag to stay in this state
        //      sta.flags &= ~STAF_TO_STANDBY;                  // clear To_StandBy
        // prepare argument for the command
        dpram->mcu.slowArg.data = FALSE;

        // it contains 1ms sleep
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_START_STOP_REGULATION, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            //          fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateSB(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  SB : ON_STANDBY state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        RefClr();       // set current to 0
        sta.flags &= ~STAF_START;                       // Clear START flag used to arrive here
        DIG_OP_P = DIG_OP_SET_UNBLOCKCMD_MASK16; // deactivate VS output stage blocking (pin C19, OUTPUT_BLOCK)
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateIL(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  IL : IDLE state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        sta.flags &= ~STAF_IDLE;                        // Clear IDLE flag used to arrive here
        DIG_OP_P = DIG_OP_SET_UNBLOCKCMD_MASK16; // deactivate VS output stage blocking (pin C19, OUTPUT_BLOCK)
        // apply current value

        // prepare argument for the command
        //      dpram->mcu.slowArg.regulation_reference = 1.0;
        // it contains 1ms sleep
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_REFERENCE, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            //          fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
        }

        // prepare argument for the command
        dpram->mcu.slowArg.data = 0x00000001;

        // it contains 1ms sleep
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_START_STOP_REGULATION, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            //          fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateAR(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  AR : ARMED state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        sta.flags |= STAF_IDLE;                         // Set IDLE flag to move to IDLE state
        // prepare argument for the command
        dpram->mcu.slowArg.data = FALSE;

        // it contains 1ms sleep
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_START_STOP_REGULATION, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            //          fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateRN(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  RN : RUNNING state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state (ARMED)
    {
        sta.flags &= ~STAF_ARMED;                       // Clear ARMED flag
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateAB(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  AB : ABORTING state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state (RUNNING)
    {
        sta.flags &= ~STAF_RUNNING;                     // Clear RUNNING flag
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateTC(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  TC : TO_CYCLING state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state
    {
        DIG_OP_P = DIG_OP_SET_UNBLOCKCMD_MASK16; // deactivate VS output stage blocking (pin C19, OUTPUT_BLOCK)
        // prepare argument for the command
        dpram->mcu.slowArg.data = FALSE;

        // it contains 1ms sleep
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_START_STOP_REGULATION, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            //          fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void StateCY(bool first_f)
/*---------------------------------------------------------------------------------------------------------*\
  CY : CYCLING state function.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (first_f)                                        // If coming from another state (ON_STANDBY)
    {
        // apply current value

        // prepare argument for the command
        dpram->mcu.slowArg.data = 5;

        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_DAC1, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            //          fprintf(term.f,RSP_DATA ",DSP rsp timeout\r");
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: state_class.c
\*---------------------------------------------------------------------------------------------------------*/
