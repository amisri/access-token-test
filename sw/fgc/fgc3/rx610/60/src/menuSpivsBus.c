/*---------------------------------------------------------------------------------------------------------*\
  File:         menuSpiVBus.c

  Purpose:      SpiVs bus test functions

  Author:       hugo.lebreton@cern.ch

  Notes:
                These menus belongs to the [Manual Test] part and [Self Test] part
                SPIS is on MCU or DSP side depending on FPGA programming
\*---------------------------------------------------------------------------------------------------------*/


#include <bitmap.h>
#include <menu.h>               // for MenuGetInt16U(), MenuRspArg(), MenuRspError()
#include <boot_dsp_cmds.h>
#include <boot_dual_port_ram.h>
#include <dsp_cmds.h>
#include <memmap_mcu.h>         // for SPIVS_*
#include <sharedMemory.h>
#include <sleep.h>
#include <stdint.h>
#include <time_fgc.h>


#define SPIVS_MAX_LWORD 7
#define SPIVS_MAX_WORD  (7 * 2)


// Selects the SPIVS master

void MenuMTSpivsMaster(uint16_t argc, char ** argv)
{
    uint16_t select;

    if (MenuGetInt16U(argv[0], 0, 1, &select) != 0)
    {
        return;
    }

    // 0 - The SPIVS bus is controlled by the DSP
    // 1 - The SPIVS bus is controlled by the MCU

    if (select == 0)
    {
        clrBitmap(CPU_MODE_P, 0x0100);

        if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
        {
            fprintf(term.f, RSP_DATA ",DSP master.\n");
            return;
        }
    }
    else
    {
        setBitmap(CPU_MODE_P, 0x0100);

        if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == true)
        {
            fprintf(term.f, RSP_DATA ",MCU master.\n");
            return;
        }
    }

    MenuRspError("Failed to set master.");
}



// [Manual Test] Set data at given index (0-SPIVS_MAX_WORD)

void MenuMTSpiVSBusSetData(uint16_t argc, char ** argv)
{
    uint16_t idx;
    uint32_t data;

    if (MenuGetInt16U(argv[0], 0, SPIVS_MAX_LWORD - 1, &idx) != 0)
    {
        return;
    }

    if (MenuGetInt32U(argv[1], 0, UINT32_MAX, &data) != 0)
    {
        return;
    }

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        // DSP master

        *((volatile uint32_t *) dpram->mcu.slowArg.spivs.buf + idx) = data;

        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPIVS_SEND_VALUE, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            MenuRspError("DSP timeout");
            return;
        }
    }
    else
    {
        // MCU master

        *((volatile uint32_t *) SPIVS_TB_32 + idx) = data;
    }
}



// [Manual Test] Set number of long words to be transmitted

void MenuMTSciVSBusSetNData(uint16_t argc, char ** argv)
{
    uint16_t wlen;

    if (MenuGetInt16U(argv[0], 0, SPIVS_MAX_LWORD, &wlen) != 0)
    {
        return;
    }

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        // DSP master

        dpram->mcu.slowArg.spivs.num_lwords.part.lo = wlen;

        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPIVS_NUM_LWORDS, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            MenuRspError("DSP timeout");
            return;
        }
    }
    else
    {
        // MCU master

        SPIVS_NUM_LWORDS_P = wlen;
    }
}



// [Manual Test] Read back the TX buffer

void MenuMTSpiVSDisplayTB(uint16_t argc, char ** argv)
{
    volatile uint16_t * pdata;

    uint16_t spivs_nwords, spivs_ctrl;
    uint8_t  i;

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        // DSP master

        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_SPIVS_SEND_VALUE, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            MenuRspError("DSP timeout");
            return;
        }

        fprintf(term.f, RSP_DATA ",Master: DSP\n");

        pdata        = (volatile uint16_t *) dpram->dsp.slowRsp.spivs.buf;
        spivs_ctrl   = dpram->dsp.slowRsp.spivs.control.part.lo;
        spivs_nwords = dpram->dsp.slowRsp.spivs.num_lwords.part.lo;
    }
    else
    {
        // MCU master

        fprintf(term.f, RSP_DATA ",Master: MCU\n");

        pdata        = (volatile uint16_t *) SPIVS_TB_32;
        spivs_ctrl   = SPIVS_CTRL_P;
        spivs_nwords = SPIVS_NUM_LWORDS_P;
    }

    fprintf(term.f, RSP_DATA ",Number of long words: %d\n", spivs_nwords);
    fprintf(term.f, RSP_DATA ",Control: 0x%04X", spivs_ctrl);

    if (testBitmap(spivs_ctrl, SPIVS_CTRL_LOOP_MASK16) == true)
    {
        fprintf(term.f, "  LOOP");

        if (testBitmap(spivs_ctrl, SPIVS_CTRL_INV_MASK16) == true)
        {
            fprintf(term.f, "  INV");
        }
    }

    fprintf(term.f, "\n");


    // Print data in decimal then hexadecimal value

    fprintf(term.f, RSP_DATA ",Data:\n" RSP_DATA);

    for (i = 0; i < SPIVS_MAX_WORD; i++)
    {
        // Print a maximum of 8 words per line

        if (   i % 8 == 0
            && i     != 0)
        {
            fprintf(term.f, "\n" RSP_DATA);
        }

        fprintf(term.f, ",%d", *pdata++);
    }

    fprintf(term.f, "\n");


    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        pdata = (volatile uint16_t *) dpram->dsp.slowRsp.spivs.buf;
    }
    else
    {
        pdata = (volatile uint16_t *) SPIVS_TB_32;
    }


    fprintf(term.f, RSP_DATA);

    for (i = 0; i < SPIVS_MAX_WORD; i++)
    {
        // Print a maximum of 8 words per line

        if (   i % 8 == 0
            && i     != 0)
        {
            fprintf(term.f, "\n" RSP_DATA);
        }

        fprintf(term.f, ",0x%04X", *pdata++);
    }

    fprintf(term.f, "\n");
}



// [Manual Test] Print out the Rx buffer

void MenuMTSpiVSDisplayRB(uint16_t argc, char ** argv)
{
    volatile uint16_t * pdata;

    uint16_t spivs_nwords, spivs_ctrl;
    uint8_t  i;

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        // Send command to read from dsp
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_SPIVS_RETURN_VALUE, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            MenuRspError("DSP timeout");
            return;
        }

        fprintf(term.f, RSP_DATA ",Master: DSP\n");

        pdata        = (volatile uint16_t *) dpram->dsp.slowRsp.spivs.buf;
        spivs_ctrl   = dpram->dsp.slowRsp.spivs.control.part.lo;
        spivs_nwords = dpram->dsp.slowRsp.spivs.num_lwords.part.lo;
    }
    else
    {
        fprintf(term.f, RSP_DATA ",Master: MCU\n");

        pdata        = (volatile uint16_t *) SPIVS_RB_32;
        spivs_ctrl   = SPIVS_CTRL_P;
        spivs_nwords = SPIVS_NUM_LWORDS_P;
    }

    fprintf(term.f, RSP_DATA ",Number of long words: %d\n", spivs_nwords);
    fprintf(term.f, RSP_DATA ",Control: 0x%04X", spivs_ctrl);

    if (testBitmap(spivs_ctrl, SPIVS_CTRL_LOOP_MASK16) == true)
    {
        fprintf(term.f, "  LOOP");

        if (testBitmap(spivs_ctrl, SPIVS_CTRL_INV_MASK16) == true)
        {
            fprintf(term.f, "  INV");
        }
    }

    fprintf(term.f, "\n");


    // Print data in decimal then hexadecimal value

    fprintf(term.f, RSP_DATA ",Data:\n" RSP_DATA);

    for (i = 0; i < SPIVS_MAX_WORD; i++)
    {
        // Print a maximum of 8 words per line

        if (   i % 8 == 0
            && i     != 0)
        {
            fprintf(term.f, "\n" RSP_DATA);
        }

        fprintf(term.f, ",%d", *pdata++);
    }

    fprintf(term.f, "\n");


    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        pdata = (volatile uint16_t *) dpram->dsp.slowRsp.spivs.buf;
    }
    else
    {
        pdata = (volatile uint16_t *) SPIVS_RB_32;
    }


    fprintf(term.f, RSP_DATA);

    for (i = 0; i < SPIVS_MAX_WORD; i++)
    {
        // Print a maximum of 8 words per line

        if (   i % 8 == 0
            && i     != 0)
        {
            fprintf(term.f, "\n" RSP_DATA);
        }

        fprintf(term.f, ",0x%04X", *pdata++);
    }

    fprintf(term.f, "\n");
}



// [Manual Test] Send the current buffer

void MenuMTSpiVSSend(uint16_t argc, char ** argv)
{
    volatile uint16_t * spivs_ctrl;

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        spivs_ctrl = (volatile uint16_t *) &dpram->mcu.slowArg.spivs.control.part.lo;
    }
    else
    {
        spivs_ctrl = (volatile uint16_t *) SPIVS_CTRL_32;
    }

    if (testBitmap(*spivs_ctrl, SPIVS_CTRL_SOURCE_MASK16) == true)
    {
        MenuRspError("Continuous mode in progress");
        return;
    }

    clrBitmap(*spivs_ctrl, SPIVS_CTRL_RST_MASK16);
    setBitmap(*spivs_ctrl, SPIVS_CTRL_COMMIT_MASK16);   // Start 1 transaction

    // DSP master
    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPIVS_CTRL, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            MenuRspError("DSP timeout");
        }
    }
}



// [Manual Test] Toggle Inverted Mode

void MenuMTSpiVSToggleInvertedMode(uint16_t argc, char ** argv)
{
    volatile uint16_t * spivs_ctrl;

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        spivs_ctrl = (volatile uint16_t *) &dpram->mcu.slowArg.spivs.control.part.lo;
    }
    else
    {
        spivs_ctrl = (volatile uint16_t *) SPIVS_CTRL_32;
    }

    if (testBitmap(*spivs_ctrl, SPIVS_CTRL_INV_MASK16) == true)
    {
        clrBitmap(*spivs_ctrl, SPIVS_CTRL_INV_MASK16);

        fprintf(term.f, RSP_DATA ",Invert OFF\n");
    }
    else if (testBitmap(*spivs_ctrl, SPIVS_CTRL_LOOP_MASK16) == true)
    {
        // Inversion can only be set if the loopback is active

        setBitmap(*spivs_ctrl, SPIVS_CTRL_INV_MASK16);

        fprintf(term.f, RSP_DATA ",Invert ON\n");
    }
    else
    {
        MenuRspError("Failed to activate inversion. Loopback is not set.");
        return;
    }

    // DSP master
    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPIVS_CTRL, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            MenuRspError("DSP timeout");
        }
    }
}



// [Manual Test] Toggle Loopback

void MenuMTSpiVSToggleLoopback(uint16_t argc, char ** argv)
{
    volatile uint16_t * spivs_ctrl;

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        spivs_ctrl = (volatile uint16_t *) &dpram->mcu.slowArg.spivs.control.part.lo;
    }
    else
    {
        spivs_ctrl = (volatile uint16_t *) SPIVS_CTRL_32;
    }

    if (testBitmap(*spivs_ctrl, SPIVS_CTRL_LOOP_MASK16) == true)
    {
        // Inversion can only be active if the loopback is set, so it must be cleared as well

        clrBitmap(*spivs_ctrl, SPIVS_CTRL_INV_MASK16);
        clrBitmap(*spivs_ctrl, SPIVS_CTRL_LOOP_MASK16);

        fprintf(term.f, RSP_DATA ",Loopback OFF\n");
    }
    else
    {
        setBitmap(*spivs_ctrl, SPIVS_CTRL_LOOP_MASK16);

        fprintf(term.f, RSP_DATA ",Loopback ON\n");
    }

    // DSP master
    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPIVS_CTRL, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            MenuRspError("DSP timeout");
        }
    }
}



// [Manual Test] Switch operational mode. 0 single shot, 1 continuous mode

void MenuMTSpiVSToggleContinuousMode(uint16_t argc, char ** argv)
{
    volatile uint16_t * spivs_ctrl;

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        spivs_ctrl = (volatile uint16_t *) &dpram->mcu.slowArg.spivs.control.part.lo;
    }
    else
    {
        spivs_ctrl = (volatile uint16_t *) SPIVS_CTRL_32;
    }

    if (testBitmap(*spivs_ctrl, SPIVS_CTRL_SOURCE_MASK16) == true)
    {
        MenuRspArg("Stop continuous mode");     // Was running, we will stop continuous mode
    }
    else
    {
        MenuRspArg("Start continuous mode");    // Was not running, we will start continuous mode
    }

    *spivs_ctrl ^= SPIVS_CTRL_SOURCE_MASK16;    // Toggle source

    // DSP master
    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPIVS_CTRL, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            MenuRspError("DSP timeout");
        }
    }
}



// [Self Test] Test the SPIVS interface in inverted loop back mode

void MenuSTSpiVSBusTest(uint16_t argc, char ** argv)
{
    volatile uint16_t * spivs_ctrl;
    volatile uint16_t * rb_data;

    uint16_t timeout = 100;
    uint8_t  i;


    // MCU master test

    // Set the SPIVS bus to be controlled by the MCU

    setBitmap(CPU_MODE_P, 0x0100);

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        MenuRspError("Failed to set master.");
        return;
    }


    // Set TB data

    for (i = 0; i < SPIVS_MAX_WORD; i++)
    {
        *((volatile uint16_t *) SPIVS_TB_32 + i) = 0xCAF0 + i;
    }


    // Set number of long words to exchange to the maximum

    SPIVS_NUM_LWORDS_P = SPIVS_MAX_LWORD;


    // Set control register and send data

    spivs_ctrl = (volatile uint16_t *) SPIVS_CTRL_32;

    clrBitmap(*spivs_ctrl, SPIVS_CTRL_RST_MASK16);
    setBitmap(*spivs_ctrl, SPIVS_CTRL_LOOP_MASK16);
    setBitmap(*spivs_ctrl, SPIVS_CTRL_INV_MASK16);
    setBitmap(*spivs_ctrl, SPIVS_CTRL_COMMIT_MASK16);


    // Wait

    while (testBitmap(*spivs_ctrl, SPIVS_CTRL_COMMIT_MASK16) == true)
    {
        timeout--;
        USLEEP(10);
    }

    if (testBitmap(*spivs_ctrl, SPIVS_CTRL_COMMIT_MASK16) == true)
    {
        MenuRspError("Read bit Timeout");
        return;
    }


    // Read and validate RB data

    rb_data = (volatile uint16_t *) SPIVS_RB_32;

    for (i = 0; i < SPIVS_MAX_WORD; i++)
    {
        if (*rb_data != (uint16_t) ~(0xCAF0 + i))
        {
            MenuRspError("RB does not match: expected 0x%4X, got 0x%4X (MCU master)", (uint16_t) ~(0xCAF0 + i), (uint16_t) *rb_data);
            return;
        }

        rb_data++;
    }


    // DSP master test

    // Set the SPIVS bus to be controlled by the DSP

    clrBitmap(CPU_MODE_P, 0x0100);

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == true)
    {
        MenuRspError("Failed to set master.");
        return;
    }


    // Set TB data

    for (i = 0; i < SPIVS_MAX_WORD; i++)
    {
        *((volatile uint16_t *) dpram->mcu.slowArg.spivs.buf + i) = 0xCAF0 + i;
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPIVS_SEND_VALUE, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
    {
        MenuRspError("DSP timeout");
        return;
    }


    // Set number of long words to exchange to the maximum

    dpram->mcu.slowArg.spivs.num_lwords.part.lo = SPIVS_MAX_LWORD;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPIVS_NUM_LWORDS, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
    {
        MenuRspError("DSP timeout");
        return;
    }


    // Set control register and send data

    spivs_ctrl = (volatile uint16_t *) &dpram->mcu.slowArg.spivs.control.part.lo;

    clrBitmap(*spivs_ctrl, SPIVS_CTRL_RST_MASK16);
    setBitmap(*spivs_ctrl, SPIVS_CTRL_LOOP_MASK16);
    setBitmap(*spivs_ctrl, SPIVS_CTRL_INV_MASK16);
    setBitmap(*spivs_ctrl, SPIVS_CTRL_COMMIT_MASK16);

    if (testBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8) == false)
    {
        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_SPIVS_CTRL, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
        {
            MenuRspError("DSP timeout");
        }
    }


    // Read and validate RB data

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_SPIVS_RETURN_VALUE, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
    {
        MenuRspError("DSP timeout");
        return;
    }

    rb_data = (volatile uint16_t *) dpram->dsp.slowRsp.spivs.buf;

    for (i = 0; i < SPIVS_MAX_WORD; i++)
    {
        if (*rb_data != (uint16_t) ~(0xCAF0 + i))
        {
            MenuRspError("RB does not match: expected 0x%4X, got 0x%4X (DSP master)", (uint16_t) ~(0xCAF0 + i), *rb_data);
            return;
        }

        rb_data++;
    }

    MenuRspArg("OK");
}


// EOF