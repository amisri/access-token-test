/*---------------------------------------------------------------------------------------------------------*\
  File:         menuAna.c

  Purpose:      Analog interface test functions

  Notes:        These menues belongs to the [Manual Test] part and [Self Test] part

\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <iodefines.h>
#include <stdlib.h>
#include <stdio.h>

#include <ana_temp_regul.h>
#include <analog.h>
#include <boot_dsp_cmds.h>
#include <boot_dual_port_ram.h>
#include <debug.h>
#include <defconst.h>
#include <dev.h>
#include <dsp_cmds.h>
#include <dsp_loader.h>
#include <mcuDependent.h>
#include <menu.h>
#include <pll.h>
#include <sharedMemory.h>
#include <sleep.h>
#include <term.h>



// Internal function declarations

static void connectAdcInputs(void);


void MenuMTreadAdcs(uint16_t argc, char ** argv)
{
    int64_t  sum;
    uint16_t channel;
    uint16_t number_of_samples;
    uint16_t stat;

    if (MenuGetInt16U(argv[0], 1, 0xFFFF, &number_of_samples))
    {
        return;
    }

    // Check analogue card type

    if (   MID_ANATYPE_P != FGC_INTERFACE_ANA_101
        && MID_ANATYPE_P != FGC_INTERFACE_ANA_103
        && MID_ANATYPE_P != FGC_INTERFACE_ANA_104)
    {
        MenuRspError("Wrong analogue card type");
        return;
    }

    // Check dsp is running

    if (dsp.is_running == false)
    {
        MenuRspError("DSP does not run");
        return;
    }

    stat = waitAdcsReadProcess(number_of_samples);

    if (stat != ADC_ERROR_OK)
    {
        MenuRspError("ADC read failed [%s]", TypeLabel(adc_errors_str, stat));

        return;
    }

    for (channel = 0; channel < FGC_N_ADCS; ++channel)
    {
        // Place high and low part in the correct order

        sum = dpram->adc.sum[channel].part.lo;
        sum <<= 32;
        sum |= dpram->adc.sum[channel].part.hi;

        fprintf(term.f, RSP_DATA ", ADC%01i, average: %11lld, max: %11ld, min: %11ld\n",
            channel,
            sum / number_of_samples,
            dpram->adc.max[channel],
            dpram->adc.min[channel]);
    }
}



void MenuSTtestADCs(uint16_t argc, char ** argv)
{
    uint16_t stat;
    uint32_t number_of_samples = 256;

    // Check analogue card type

    if (   MID_ANATYPE_P != FGC_INTERFACE_ANA_101
        && MID_ANATYPE_P != FGC_INTERFACE_ANA_103
        && MID_ANATYPE_P != FGC_INTERFACE_ANA_104)
    {
        MenuRspError("Wrong analogue card type");
        return;
    }

    // Test access to register

    if (dev.reg.dpram != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Check dsp is running

    if (dsp.is_running == false)
    {
        MenuRspError("DSP does not run");
        return;
    }


    // 10V test

    // Select the 10V reference on the analogue multiplexer (0x06)

    dpram->mcu.slowArg.data = 0x06;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_ADC_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout");
        return;
    }

    // Wait for the voltage to be stable

    MSLEEP(100);

    stat = waitAdcsTestProcess(number_of_samples, ADC_TEST_10V);

    if (stat != ADC_ERROR_OK)
    {
        if (stat == ADC_ERROR_LIMITS)
        {
            MenuRspError("10V test failed. Got %ld and expected value between %ld and %ld.", dpram->adc.error.data, dpram->adc.error.limits[0],
            dpram->adc.error.limits[1]);
        }
        else
        {
            MenuRspError("10V test failed [%s]", TypeLabel(adc_errors_str, stat));
        }

        connectAdcInputs();

        return;
    }


    // -10V test

    // Select the -10V reference on the analogue multiplexer (0x07)

    dpram->mcu.slowArg.data = 0x07;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_ADC_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout");
        return;
    }

    // Wait for the voltage to be stable

    MSLEEP(100);

    stat = waitAdcsTestProcess(number_of_samples, ADC_TEST_N10V);

    if (stat != ADC_ERROR_OK)
    {
        if (stat == ADC_ERROR_LIMITS)
        {
            MenuRspError("-10V test failed. Got %ld and expected value between %ld and %ld.", dpram->adc.error.data, dpram->adc.error.limits[0],
            dpram->adc.error.limits[1]);
        }
        else
        {
            MenuRspError("-10V test failed [%s]", TypeLabel(adc_errors_str, stat));
        }

        connectAdcInputs();

        return;
    }


    // 0V test

    // Select zero on the analogue multiplexer (0x04)

    dpram->mcu.slowArg.data = 0x04;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_ADC_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout");
        return;
    }

    // Wait for the voltage to be stable

    MSLEEP(100);

    stat = waitAdcsTestProcess(number_of_samples, ADC_TEST_0V);

    if (stat != ADC_ERROR_OK)
    {
        if (stat == ADC_ERROR_LIMITS)
        {
            MenuRspError("0V test failed. Got %ld and expected value between %ld and %ld.", dpram->adc.error.data, dpram->adc.error.limits[0],
            dpram->adc.error.limits[1]);
        }
        else
        {
            MenuRspError("0V test failed [%s]", TypeLabel(adc_errors_str, stat));
        }

        connectAdcInputs();

        return;
    }

    // Reconnect ADC inputs

    connectAdcInputs();
}



void MenuSTtestDACs(uint16_t argc, char ** argv)
{
    uint32_t       number_of_samples = 256;
    uint16_t       set_cmd[2]        = { DSP_CMD_SLOW_SET_DAC1, DSP_CMD_SLOW_SET_DAC2 };
    uint16_t       stat;
    uint8_t        dac;
    uint8_t  const n_dacs = 2;

    // Check analogue card type

    if (   MID_ANATYPE_P != FGC_INTERFACE_ANA_101
        && MID_ANATYPE_P != FGC_INTERFACE_ANA_103
        && MID_ANATYPE_P != FGC_INTERFACE_ANA_104)
    {
        MenuRspError("Wrong analogue card type");
        return;
    }

    // Test access to register

    if (dev.reg.dpram != 0)
    {
        MenuRspError("No access");
        return;
    }

    // Check dsp is running

    if (dsp.is_running == false)
    {
        MenuRspError("DSP does not run");
        return;
    }


    for (dac = 0; dac < n_dacs; ++dac)
    {
        // Select DAC-A (0x03) or DAC-B (0x02) on the analogue multiplexer

        dpram->mcu.slowArg.data = 0x03 - dac;

        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_ADC_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
        {
            MenuRspError("DSP rsp timeout");
            return;
        }

        // 10V test

        dpram->mcu.slowArg.data = 420000;

        if (SendDspSlowCmdAndWaitForRsp(set_cmd[dac], DSP_SLOW_CMD_TIMEOUT_1S) != 0)
        {
            MenuRspError("DSP rsp timeout");
            return;
        }

        // Wait for the voltage to be stable

        MSLEEP(100);

        stat = waitAdcsTestProcess(number_of_samples, ADC_TEST_10V);

        if (stat != ADC_ERROR_OK)
        {
            if (stat == ADC_ERROR_LIMITS)
            {
                MenuRspError("10V test failed. Got %ld and expected value between %ld and %ld.", dpram->adc.error.data, dpram->adc.error.limits[0],
                dpram->adc.error.limits[1]);
            }
            else
            {
                MenuRspError("10V test failed [%s]", TypeLabel(adc_errors_str, stat));
            }

            connectAdcInputs();

            return;
        }


        // -10V test

        dpram->mcu.slowArg.data = -420000;

        if (SendDspSlowCmdAndWaitForRsp(set_cmd[dac], DSP_SLOW_CMD_TIMEOUT_1S) != 0)
        {
            MenuRspError("DSP rsp timeout");
            return;
        }

        // Wait for the voltage to be stable

        MSLEEP(100);

        stat = waitAdcsTestProcess(number_of_samples, ADC_TEST_N10V);

        if (stat != ADC_ERROR_OK)
        {
            if (stat == ADC_ERROR_LIMITS)
            {
                MenuRspError("-10V test failed. Got %ld and expected value between %ld and %ld.", dpram->adc.error.data, dpram->adc.error.limits[0],
                dpram->adc.error.limits[1]);
            }
            else
            {
                MenuRspError("-10V test failed [%s]", TypeLabel(adc_errors_str, stat));
            }

            connectAdcInputs();

            return;
        }


        // 0V test

        dpram->mcu.slowArg.data = 0;

        if (SendDspSlowCmdAndWaitForRsp(set_cmd[dac], DSP_SLOW_CMD_TIMEOUT_1S) != 0)
        {
            MenuRspError("DSP rsp timeout");
            return;
        }

        // Wait for the voltage to be stable

        MSLEEP(100);

        stat = waitAdcsTestProcess(number_of_samples, ADC_TEST_0V);

        if (stat != ADC_ERROR_OK)
        {
            if (stat == ADC_ERROR_LIMITS)
            {
                MenuRspError("0V test failed. Got %ld and expected value between %ld and %ld.", dpram->adc.error.data, dpram->adc.error.limits[0],
                dpram->adc.error.limits[1]);
            }
            else
            {
                MenuRspError("0V test failed [%s]", TypeLabel(adc_errors_str, stat));
            }

            connectAdcInputs();

            return;
        }

    }


    // Reconnect ADC inputs

    connectAdcInputs();
}



/*---------------------------------------------------------------------------------------------------------*/
void MenuMTsetDac(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]

  The DACs used are the ones in the analog board (managed by the DSP), not the ones in the MCU

 the DAC is unipolar (0 to 2.5v) but later the 0v is shifted to the middle of the range
 (there is a bit manipulation inside the PLD) so we end with

 7FFFF    max positive (+12.5v)
 66665   +10v                  .... +50A1
 00000    +0v
 FFFFF    -0v
 99999   -10v                  .... -50A
 80000    max negative (-12.5v)

\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  val0;
    uint32_t  val1;

    if (MenuGetInt32U(argv[0], 0, 0xFFFFF, &val0)
        || MenuGetInt32U(argv[1], 0, 0xFFFFF, &val1)
       )
    {
        return;
    }

    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    // prepare argument for the command
    dpram->mcu.slowArg.data = val0;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_DAC1, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
    {
        MenuRspError("Failed by timeout");
    }

    // prepare argument for the command
    dpram->mcu.slowArg.data = val1;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_SET_DAC2, DSP_SLOW_CMD_TIMEOUT_10MS))
    {
        MenuRspError("Failed by timeout");
    }
}



void MenuMTsetMux(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  set the switches (multiplexors) of the analog card (managed by the DSP)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  external_signal_access;
    uint16_t  adc_selector_bus_or_signal;
    uint16_t  internal_signal_bus_access;

    if (MenuGetInt16U(argv[0], 0, 0x0F, &external_signal_access) ||
        MenuGetInt16U(argv[1], 0, 0x0F, &adc_selector_bus_or_signal) ||
        MenuGetInt16U(argv[2], 0, 0x1F, &internal_signal_bus_access))
    {
        return;
    }

    if (dsp.is_running == FALSE)
    {
        MenuRspError("DSP does not run");
        return;
    }

    // prepare arguments for the command

    dpram->mcu.slowArg.adc_path.external_signal_access     = external_signal_access;        // ANA_SWIN
    dpram->mcu.slowArg.adc_path.adc_selector_bus_or_signal = adc_selector_bus_or_signal;    // SW_BUS
    dpram->mcu.slowArg.adc_path.internal_signal_bus_access = internal_signal_bus_access;    // MPX

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_ADC_PATH, DSP_SLOW_CMD_TIMEOUT_10MS) != 0)
    {
        MenuRspError("Failed by timeout");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTanaHeat(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test]
  This function tests the heating system of the analogue board heater.
  Heater is set by the DAC0 (DA.DADR0), corresponding to the HEATER_CTRL of the Main Board.
  Temperature corresponds to the TEMP_MEAS of the Main Board and can be read in the ADC0 (AD.ADDRA).
  First, we turn off the heater for 30 seconds and expect the temperature to drop.Then we turn on the heater
  for 30 seconds by setting the temperature reference to 49.0 degC and expect the temperature to rise.
\*---------------------------------------------------------------------------------------------------------*/
{

    float    temp;
    float    start_temp;
    uint16_t  count_s = 0;

    // Get starting temperature

    start_temp = AnaTempGetTemp();    // Read temperature (ADC0)
    fprintf(term.f,RSP_DATA ",Starting Temp: %.2f degC\n", (double) start_temp);
    temp = start_temp;

    // Turn heater OFF

    fprintf(term.f,RSP_DATA ",Turning heater OFF for 30 secs:\n");

    while (count_s++ != ANA_HEAT_TEST_TIME_S)
    {
        AnaTempRegulStart(0.0); // Turn off the heater and disable regulation

        MenuRspProgress(TRUE, count_s, ANA_HEAT_TEST_TIME_S);

        MSLEEP(1000);       // Wait for 1s
    }

    temp = AnaTempGetTemp();    // Read temperature (ADC0)
    fprintf(term.f,RSP_DATA ",Temp: %.2f degC", (double) temp);

    if (temp < start_temp)
    {
        fprintf(term.f, ", dropped %.2f degC\n", (double) (start_temp-temp));
    }
    else {
        fprintf(term.f, ", did not drop\n");
    }

    start_temp = temp;
    count_s = 0;

    // Turn heater ON

    fprintf(term.f,RSP_DATA ",Turning heater ON for 30 secs:\n");

    while (count_s++ != ANA_HEAT_TEST_TIME_S)
    {
        AnaTempRegulStart(49.0);

        MenuRspProgress(TRUE, count_s, ANA_HEAT_TEST_TIME_S);

        MSLEEP(1000);       // Wait for 1s
    }

    temp = AnaTempGetTemp();    // Read temperature (ADC0)
    fprintf(term.f, RSP_DATA ",Temp: %.2f degC", (double) temp);

    if (temp > start_temp)
    {
        fprintf(term.f,", raised %.2f degC\n", (double) (temp-start_temp));
    }
    else {
        fprintf(term.f,", did not raise\n");
    }

    AnaTempRegulStop();  // Remove heating
    MSLEEP(2000);       // Wait 2s
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTmcuAnalogue(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Self Test]
  This function tests the loop of the DAC1 = ADC1 and was originally designed to check the external heater.
  DAC1 (DA.DADR1) corresponds to the INLET_HEATER of the Main Board and ADC1 (AD0.ADDRB) to the INLET_MEAS.
  In the reception crate, DAC1 is connected to the ADC1.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t count;
    uint16_t test_value = 0x2AA;
    uint16_t max_divergence =  6;        // ADC1 is expected to be DAC1 +-6

    if (dev.crate_type == FGC_CRATE_TYPE_RECEPTION)
    {
        P7.DR.BIT.B6 = 1;               // Disable digital CMD output, needed??
        AnaTempRegulStop();

        // dac 1 is 10 bits DAC
        DA.DADR1 = test_value;
        DA.DACR.BIT.DAOE1 = 1;          // enable DA1 output
        MSLEEP(10);
        count = AD0.ADDRB & 0x3FF;      // Read  ADC1 (both ADC1 and DAC1 are 10 bits)

        if ((count < (test_value - max_divergence))
            || (count > (test_value + max_divergence))
           )
        {
            MenuRspError("Read back exp:0x%2X +-%u got:0x%4X", test_value, max_divergence, count);
            return;
        }

        DA.DADR1 = 0;
    }
    else
    {
        MenuRspError("This works only with the reception crate");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTanaSetHeater(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  Set Rx610 DAC0 to heat the analog board with the given power in mW.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  mW;
    float   ana_temp;

    if (MenuGetInt16U(argv[0], 0, 2000, &mW) != 0)
    {
        return;
    }

    // Turn off regulation

    AnaTempRegulStop();

    // Set DAC

    AnaTempSetPower(mW);
    ana_temp = AnaTempGetTemp();

    fprintf(term.f, RSP_DATA",Temp: %.2f degC (%u mW)\n", (double) ana_temp, mW);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTanaTemp(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  Rx610 ADC0 reads the output of a temperature sensor. 47mV -> 1degC
\*---------------------------------------------------------------------------------------------------------*/
{
    float    volt;
    float    ana_temp;
    uint16_t  heater;


    volt = (float)(AD0.ADDRA & 0x3FF) * 3.3 / 1023;
    heater = (uint16_t)((((uint32_t)(DA.DADR0 & 0xFF)) * 2000) / 0xFF);

    ana_temp = AnaTempGetTemp();

    MenuRspArg("Temp: %.2f C (%.2fV - %u mW - Regul: %u)", ana_temp, volt, heater, AnaTempGetStatus());
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTanaTempRegulParam(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  Set the temperature regulation parameters.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  prop_gain;
    uint16_t  int_gain;
    uint16_t  diff_gain;

    if ((MenuGetInt16U(argv[0], 0, 0xFFFF, &prop_gain) != 0)
        || (MenuGetInt16U(argv[1], 0, 0xFFFF, &int_gain)  != 0)
        || (MenuGetInt16U(argv[2], 0, 0xFFFF, &diff_gain)  != 0)
       )
    {
        return;
    }

    AnaTempSetPIDGain(prop_gain, int_gain, diff_gain);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTanaTempRegulParamShow(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  Show the temperature regulation parameters.
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(term.f, RSP_DATA ",Target temperature: %f\n", (double) NVRAM_TARGET_TEMP_P);
    fprintf(term.f, RSP_DATA ",Regulation is %s\n", AnaTempGetStatus() ? "on" : "off");
    fprintf(term.f, RSP_DATA ",Prop gain: %u\n", AnaTempGetPGain());
    fprintf(term.f, RSP_DATA ",Int. gain: %u\n", AnaTempGetIGain());
    fprintf(term.f, RSP_DATA ",Diff gain: %u\n\n", AnaTempGetDGain());

    fprintf(term.f, RSP_DATA ",Integrator: %li\n", AnaTempGetIntegrator());
    fprintf(term.f, RSP_DATA ",ADC ref   : %u\n", AnaTempGetAdcRef());
    fprintf(term.f, RSP_DATA ",ADC gain  : %0.2f\n", (double) AnaTempGetAdcGain());
    fprintf(term.f, RSP_DATA ",Proc. time: %u us\n", AnaTempGetProcessingTime());
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTanaTempRegul(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  Set the desired target temperature. 0 will turn off the temperature regulation.
\*---------------------------------------------------------------------------------------------------------*/
{
    float   target_temp;     // target temperature
    uint16_t heater;          // heating value in mW
    uint16_t idx = 0;         // plot index

    if (MenuGetFloat(argv[0], 0, 80, &target_temp) != 0)
    {
        return;
    }

    AnaTempRegulStart(target_temp);

    for (;;)
    {
        // Display data

        heater = (uint16_t)(((uint32_t)(DA .DADR0 & 0xFF) * 2000) / 0xFF);
        fprintf(term.f, RSP_DATA ",%u,%.2f,%u\n", idx++, (double) AnaTempGetTemp(), heater);
        MSLEEP(995);

        // Exit on Ctrl-C

        if (dev.abort_f == true)
        {
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTanaTempMonitor(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Manual Test]
  Reads the analog interface temperature (1sample/sec).
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  samples;        // Number of samples to monitor
    uint16_t  heater;     // heating value in mW

    if (MenuGetInt16U(argv[0], 0, 1000, &samples) != 0)
    {
        return;
    }

    while (samples-- != 0)     // Read temp every ~1s
    {
        heater = (uint16_t)(((uint32_t)(DA.DADR0 & 0xFF) * 2000) / 0xFF);
        fprintf(term.f, RSP_DATA ",%.2f,%u\n", (double) AnaTempGetTemp(), heater);
        MSLEEP(995);

        // Exit on Ctrl-C

        if (dev.abort_f == true)
        {
            return;
        }
    }
}



// Internal function definitions

// This function connects the ADC inputs and diconnects them from the analogue bus

static void connectAdcInputs(void)
{
    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_ADC_DEFAULT, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout");
        return;
    }
}

//EOF
