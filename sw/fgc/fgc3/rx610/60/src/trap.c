/*---------------------------------------------------------------------------------------------------------*\
  File:     trap.c

  Purpose:  Trap related interrupt functions

\*---------------------------------------------------------------------------------------------------------*/

#define TRAP_GLOBALS        // for isrdummy_count

#include <trap.h>
#include <stdint.h>
#include <memmap_mcu.h>         // Include memory map consts
#include <mcuDependent.h>      // for MSLEEP(), NVRAM_LOCK(), ENTER_SR(), EXIT_SR
#include <iodefines.h>          // specific processor registers and constants
#include <fgc_runlog_entries.h> // Include run log definition, for FGC_RL_
#include <runlog_lib.h>         // for struct TRunlog, RunlogInit(), RunlogWrite
#include <core.h>               // for CoreDump()
#include <os.h>                 // for OS_ENTER_CRITICAL(), OS_EXIT_CRITICAL();
#include <os_hardware.h>        // for OS_TID_CODE, OS_ISR_MASK
#include <sharedMemory.h>      // for shared_mem global variable
#include <memmap_mcu.h>         // for UTC*
#include <derived_clocks.h>     // for StopDerivedClocks()

/*---------------------------------------------------------------------------------------------------------*/
void IsrDummy(void)
/*---------------------------------------------------------------------------------------------------------*\
  All other interrupts
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR  cpu_sr;

    trap.isrdummy_count++;
    DISABLE_INTERRUPTS();       // or OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running
    OS_ENTER_CRITICAL_7();      // Disable ALL interrupts
    IsrTrap();                  // Safe mode & reg savings
    // flash red led
    PanicFGC3(RGLEDS_PIC_RED_MASK16, 0);
    // never returns
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrDummyFix(void)
/*---------------------------------------------------------------------------------------------------------*\
  All other interrupts
\*---------------------------------------------------------------------------------------------------------*/
{
    trap.isrdummy_fix_count++;
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrDummyEmpty(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    trap.isrdummy_empty_count++;
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrUndefinedInstruction(void)
/*---------------------------------------------------------------------------------------------------------*\
  This ISR will be called in response to an undefined instruction.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR  cpu_sr;

    DISABLE_INTERRUPTS();       // or OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running
    OS_ENTER_CRITICAL_7();      // Disable ALL interrupts
    IsrTrap();                  // Safe mode & reg savings
    // flash red : FIP & FGC
    PanicFGC3(RGLEDS_NET_RED_MASK16 | RGLEDS_FGC_RED_MASK16, 0);
    // never returns
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrOverflow(void)
/*---------------------------------------------------------------------------------------------------------*\
  This ISR will be called in response to an overflow and a INTO instruction
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR  cpu_sr;

    DISABLE_INTERRUPTS();       // or OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running
    OS_ENTER_CRITICAL_7();      // Disable ALL interrupts
    IsrTrap();                  // Safe mode & reg savings
    // flash red FIP,FGC & PSU
    PanicFGC3(RGLEDS_NET_RED_MASK16 | RGLEDS_FGC_RED_MASK16 | RGLEDS_PSU_RED_MASK16, 0);
    // never returns
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrWatchdogTimer(void)
/*---------------------------------------------------------------------------------------------------------*\
  This ISR will be called by the Rx610 watchdog timer.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR  cpu_sr;

    DISABLE_INTERRUPTS();       // or OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running
    OS_ENTER_CRITICAL_7();      // Disable ALL interrupts
    IsrTrap();                  // Safe mode & reg savings
    // flash red FIP,FGC,PSU & VS
    PanicFGC3(RGLEDS_NET_RED_MASK16 | RGLEDS_FGC_RED_MASK16 | RGLEDS_PSU_RED_MASK16 | RGLEDS_VS_RED_MASK16, 0);
    // never returns
}
/*---------------------------------------------------------------------------------------------------------*/
void IsrTrap(void)
/*---------------------------------------------------------------------------------------------------------*\
  IsrTrap will be called for all unexpected interrupts.  The interrupt jump function will set the
  exception ID in the register D so that it can be stored along with all the other registers in the run
  log.
\*---------------------------------------------------------------------------------------------------------*/
{
    struct TRunlog   *  runlog_ptr;


    // Disable interrupts

    // in boot there is a function DisableNetworkInterrupt(),Disable1msTickInterrupt()


    // __asm__ volatile ( "clrpsw I" ); // clear I flag in PSW

    //  IRQ2 - vector 66 - FIP_~MSG_IRQ - INT_Excep_IRQ2()
    ICU.IER08.BIT.IEN2 = 0;         // Interrupt Request Enable Register
    //  ICU.IPR22.BIT.IPR_20 = 0x00;    // Interrupt Priority Register

    //  IRQ3 - vector 67 - FIP_~TIME_IRQ or ETHERNET 50Hz pulse - INT_Excep_IRQ3()
    //  ICU.IER08.BIT.IEN3 = 0;         // Interrupt Request Enable Register
    //  ICU.IPR23.BIT.IPR_20 = 0x00;    // Interrupt Priority Register

    //  IRQ4 - vector 68 - MCU_~TICK - INT_Excep_IRQ4()
    ICU.IER08.BIT.IEN4 = 0;         // Interrupt Request Enable Register
    //  ICU.IPR24.BIT.IPR_20 = 0x00;    // Interrupt Priority Register


    //  TMR0 CMIA0 - Compare Match A Interrupt - vector 174 - INT_Excep_TMR0_CMIA0()
    //  TMR0 it is programmed to generate an interrupt every 1ms
    ICU.IER15.BIT.IEN6 = 0;         // Interrupt Request Enable Register
    //  ICU.IPR68.BIT.IPR_20 = 0x00;    // Interrupt Priority Register


    //  TMR3 OVI3 - Timer Overflow Interrupt - vector 185 - INT_Excep_TMR3_OVI3()
    //  TMR3 will count [0..255] incremented by TMR2 count match (1us) and generating an interrupt when overflows
    ICU.IER17.BIT.IEN1 = 0;         // Interrupt Request Enable Register
    //  ICU.IPR6B.BIT.IPR_20 = 0x00;    // Interrupt Priority Register



    // Reset peripherals

    P7.DR.BIT.B6 = 1;   // disable digital CMD output

    // FIP/ETHERNET reset is direct from M32C87, set FIP in reset
    P9.DR.BIT.B7 = 0;

    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_C62OFF_MASK16;    // Turn M16C62 off
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_DSP_MASK16;       // Reset TMS320C6727
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_ANALOG_MASK16;    // Reset Analog interface

    StopDerivedClocks();    // Dsp, Adc, Dac

    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_DIGITAL_MASK16;   // Reset Digital interface
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_QSM_MASK16;       // Reset diagnostics interface
    NVRAM_LOCK();

    // ToDo: Set temp regul heater to 0


    DIG_SUP_A_DIR_P &= ~DIG_SUP_A_DIR_OUT_CTRL_MASK16;   // SUP_A as input
    DIG_SUP_A_DIR_P &= ~DIG_SUP_A_DIR_OUT_PERMIT_MASK16; // output disabled
    DIG_SUP_B_DIR_P &= ~DIG_SUP_B_DIR_OUT_CTRL_MASK16;   // SUP_B as input
    DIG_SUP_B_DIR_P &= ~DIG_SUP_B_DIR_OUT_PERMIT_MASK16; // output disabled

    runlog_ptr = (struct TRunlog *) NVRAM_RL_BUF_32;

    if (runlog_ptr->magic != 0xBEEF)            // if run log not ready
    {
        RunlogInit();                   // initialise run log
    }

    //--- Write trap dump to run log ---

    RunlogWrite(FGC_RL_EXID, (void *)TRAP_EXID_32);         // Exception ID
    RunlogWrite(FGC_RL_PWRTIME, (void *) & (shared_mem.power_time));
    RunlogWrite(FGC_RL_RUNTIME, (void *) & (shared_mem.run_time));
    RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));
    RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));
    //  RunlogWrite(FGC_RL_PKPC,    (void*)SM_PKPC_16);     // PKPC
    //  RunlogWrite(FGC_RL_CCR,     (void*)SM_CCR_16);      // CCR
    //  RunlogWrite(FGC_RL_SKSP,    (void*)SM_SKSP_16);     // SKSP
    //  RunlogWrite(FGC_RL_XKIX,    (void*)SM_XKIX_16);     // XKIX
    //  RunlogWrite(FGC_RL_YKIY,    (void*)SM_YKIY_16);     // YKIY
    //  RunlogWrite(FGC_RL_ZKIZ,    (void*)SM_ZKIZ_16);     // ZKIZ
    //  RunlogWrite(FGC_RL_EK,      (void*)SM_EK_16);       // EK
    //  RunlogWrite(FGC_RL_E,       (void*)SM_E_16);        // E
    //  RunlogWrite(FGC_RL_MODE_REG,(void*)TRAP_MODE_16);       // cpu mode register

    //--- Write conditional run log records ---

    if (OS_TID_CODE < 16)
    {
        RunlogWrite(FGC_RL_TSK_ID, (void *) & (OS_TID_CODE));       // NanOS Task ID
    }

    if (OS_ISR_MASK != 0)
    {
        RunlogWrite(FGC_RL_ISR_ID, (void *) & (OS_ISR_MASK));       // NanOS ISR ID
    }

    //--- Stack dump if SP was valid (i.e. if PKPC was recoverable) ---

    /*
        CPU_RESET_P |= CPU_RESET_CTRL_ULKFRAM_MASK16;
        ENTER_SR();
        if ( SM_PKPC_P )                            // If stack pointer was valid
        {
            MemCpyWords(FRAM_CORESTACK_32,*((uint32_t*)SM_SKSP_16),FRAM_CORESTACK_W); // Save stack
        }
        else                                // else SP invalid
        {
            MemSetWords(FRAM_CORESTACK_32,0,FRAM_CORESTACK_W);      // Clear stack dump
        }
        EXIT_SR();
        CPU_RESET_P &= ~CPU_RESET_CTRL_ULKFRAM_MASK16;
    */

    //--- Core dump ---

    CoreDump();
}
/*---------------------------------------------------------------------------------------------------------*/
void PanicFGC3(uint32_t leds0, uint32_t leds1)
/*---------------------------------------------------------------------------------------------------------*\
  This function will try to use the front panel LEDs to flash a code that will indicate the reason for
  the panic.  The function never exits - it just waits for a watchdog or human to reset the system.

  we can't use MSLEEP() as it needs the interrupts
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;
    uint32_t      wait_count;

    // --- Stop interrupts and halt TMS320C6727 and M16C62 ---
    DISABLE_INTERRUPTS();   // or OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running
    OS_ENTER_CRITICAL_7();  // Disable ALL interrupts
    CPU_RESET_CTRL_P |= (CPU_RESET_CTRL_C62OFF_MASK16 | CPU_RESET_CTRL_DSP_MASK16);

    // --- Flash front panel LEDs if possible and wait for a reset ---

    for (;;)                // Loop while waiting for watchdog
    {
        RGLEDS_P = leds0;           // Set first pattern
        //      MSLEEP(500);
        wait_count = 0xFF000000;

        while (wait_count > 0)
        {
            wait_count--;
        }

        RGLEDS_P = leds1;           // Set second pattern
        //      MSLEEP(500);
        wait_count = 0xFF000000;

        while (wait_count > 0)
        {
            wait_count--;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: trap.c
\*---------------------------------------------------------------------------------------------------------*/
