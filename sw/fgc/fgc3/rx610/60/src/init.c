/*
 * @file  init.c
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

#include <init.h>
#include <iodefines.h>
#include <runlog_lib.h>
#include <pld.h>
#include <menutree.h>
#include <dev.h>
#include <term.h>
#include <codes.h>
#include <version.h>
#include <master_clk_pll.h>
#include <menu_node.h>
#include <menu.h>
#include <sharedMemory.h>
#include <fieldbus_boot.h>
#include <ethernet_boot.h>
#include <hardware_setup.h>
#include <sleep.h>
#include <time_fgc.h>



static enum FGC_init_status InitNameDb(void)
{
    if(fieldbus.is_gateway_online)
    {
        // TODO At the moment, the conditional doesn't work as expected
        // We always request to update the NameDB, but it won't be updated
        // if the CRC didn't change

        //if(!CodesNameDbInstalled() || CodesNameDbUpdateIsPending())
        //{
            CodesInstallNameDb();

            if(!CodesNameDbInstalled())
            {
                return INIT_MISSING_NAMEDB;
            }
        //}
    }
    else
    {
        if(!CodesNameDbInstalled())
        {
            return INIT_MISSING_NAMEDB;
        }
    }

    if(!CodesDevIsInNameDb(dev.fieldbus_id))
    {
        return INIT_DEV_NOT_IN_NAMEDB;
    }

    return INIT_SUCCESS;
}



static void InitFailureMode(void)
{
    CodesSetNameUnknown(&dev_name);
    CodesSetNameUnknown(&gw_name);

    dev.class_id = FGC_CLASS_UNKNOWN;

    CodesUpdateInstalledInfo(0, SHORT_LIST_CODE_LAST);

    if(fieldbus.is_gateway_online)
    {
        CodesUpdateAdvertisedInfo();
    }
}



// TODO Make sure that no member of shared_mem structure is set before calling this function,
//      because it will be wiped out when the device is power cycled.
void InitSharedMem(void)
{
    // If Power reset

    if(shared_mem.boot_seq == BOOT_PWR)
    {
        // Clear shared memory area

        memset((void *)&shared_mem, 0, sizeof(struct Shared_memory));

        shared_mem.network_card_model = MID_NETTYPE_P;
        if (   shared_mem.network_card_model > FGC_NETWORK_UNDEFINED_NET
            && shared_mem.network_card_model < FGC_NETWORK_NO_NET_BOARD)
        {
            shared_mem.network_card_model = FGC_NETWORK_UNDEFINED_NET;
        }
    }
    else
    {
        RunlogWrite(FGC_RL_PWRTIME, (void *)(shared_mem.power_time));

        if(timeGetUtcTime() != 0)
        {
            RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));
            RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));

            shared_mem.run_software_utc = timeGetUtcTime();
        }

        // Reset run time

        shared_mem.run_time = 0;
    }

    if(pld_info.ok)
    {
        shared_mem.analog_card_model = MID_ANATYPE_P;
    }
    else
    {
        shared_mem.analog_card_model = 0;
    }
}



enum FGC_init_status InitSelfTest(void)
{
    struct menu_node ** boot_menu_to_run;

    // Self-test functions - POWER BOOT

    // TODO The DSP test will fail if the DSP doesn't start, which happens fairly often (long-standing bug)
    // TODO The dsp_prog self-test not only tests the DSP, but loads the DSP code. Try to split it

    static struct menu_node * power_boot_tests[] =
    {
        // &pld_dpram_test,    // this is in the manual test submenu, calling MenuMT_PLD_DpRam()
        // &sram_test,         // this is in the manual test submenu, calling MenuMTsram()
        &dsp_prog,          // this is in the manual test submenu, calling MenuMTdspProg()
        NULL,
    };

    // Self-test functions - SLOW BOOT

    static struct menu_node * slow_boot_tests[] =
    {
        &dsp_prog,      // this is in the manual test submenu, calling MenuMTdspProg()
        NULL,
    };

    // Run through list of self test functions according to boot type

    fprintf(term.f, TERM_CLR_LINE "\r$R,%lu\n", version.unixtime);
    dev.self_test_in_progress_f = TRUE;         // boot state : self test

    boot_menu_to_run = slow_boot_tests;         // Slow boot functions (by default)

    if (shared_mem.boot_seq == BOOT_PWR)
    {
        boot_menu_to_run = power_boot_tests;    // Power boot functions
    }

    while (*boot_menu_to_run != 0)              // For all function in the list
    {
        fprintf(term.f, RSP_ID_NAME "\n", "ST", (*boot_menu_to_run)->name); // Report function name

        menu.response_buf_pos = 0;              // Clear response and error buffers
        menu.error_buf[0]     = '\0';
        dev.abort_f           = false;

        (*boot_menu_to_run)->function(0, NULL); // Run self-test function (no args permitted)
        boot_menu_to_run++;

        if (menu.response_buf_pos != 0)         // If rsp buffer is not empty
        {
            fprintf(term.f, RSP_DATA "%s\n", menu.response_buf); // Write last response buffer
        }

        if(MenuErrorIsPending())
        {
            MenuPrintError();

            dev.self_test_in_progress_f = FALSE;

            // TODO This return value is wrong! The function should return an error in case of
            // self-test failure! However, we don't want to prevent main from running due to the DSP not starting
            // (see TODO note above).

            return INIT_SUCCESS;
        }
        else
        {
            fputs(RSP_OK "\r", term.f);         // Report success
        }
    }

    dev.self_test_in_progress_f = FALSE;

    return INIT_SUCCESS;
}



static enum FGC_init_status InitNormalMode(void)
{
    enum FGC_init_status status = InitNameDb();

    if(status != INIT_SUCCESS)
    {
        return status;
    }

    CodesSetNameFromNameDb(&dev_name, dev.fieldbus_id);
    CodesSetNameFromNameDb(&gw_name,  0);

    if(!CodesIdIsValid(dev_name.class_id))
    {
        return INIT_INVALID_CLASS_ID;
    }

    dev.class_id = dev_name.class_id;

    CodesSelectMainProg();
    CodesUpdateInstalledInfo(0, SHORT_LIST_CODE_LAST);

    if(fieldbus.is_gateway_online)
    {
        CodesUpdateAll();

        MSLEEP(2000);
    }

    return INIT_SUCCESS;
}



static enum FGC_init_status InitStandaloneMode(void)
{
    dev_name.class_id   = FGC_CLASS_UNKNOWN;
    dev_name.omode_mask = 0;

    strcpy(dev_name.name, "STANDALONE");

    gw_name.class_id   = FGC_CLASS_UNKNOWN;
    gw_name.omode_mask = 0;

    strcpy(gw_name.name, "NONE");

    CodesUpdateInstalledInfo(SHORT_LIST_CODE_MP_DSP, SHORT_LIST_CODE_MP_DSP + 1);

    if(!CodesIdIsValid(codes_holder_info[SHORT_LIST_CODE_MP_DSP].dev.class_id))
    {
        return INIT_INVALID_CLASS_ID;
    }

    dev.class_id      = codes_holder_info[SHORT_LIST_CODE_MP_DSP].dev.class_id;
    dev_name.class_id = dev.class_id;

    CodesSelectMainProg();
    CodesUpdateInstalledInfo(0, SHORT_LIST_CODE_LAST);

    CodesUpdateSharedMemVersions();

    return INIT_SUCCESS;
}



enum FGC_init_status InitOperationalMode(void)
{
    enum FGC_init_status status;

    if(devIdIsValid())
    {
        if(DevIsStandalone())
        {
            status = InitStandaloneMode();
        }
        else
        {
            status = InitNormalMode();
        }
    }
    else
    {
        status = INIT_INVALID_CLASS_ID;
    }

    if(status != INIT_SUCCESS)
    {
        InitFailureMode();
    }

    return status;
}



static enum FGC_init_status EthStatusToInitStatus(const enum FGC_eth_status status)
{
    switch(status)
    {
        case ETH_STATUS_SUCCESS:           return INIT_SUCCESS;
        case ETH_STATUS_CHIP_INACCESSIBLE: return INIT_ETH_CHIP_INACCESSIBLE;
        case ETH_STATUS_UNKNOWN_CHIP:      return INIT_UNKNOWN_ETH_CHIP;
        case ETH_STATUS_INVALID_ID:        return INIT_INVALID_CLASS_ID;
    }

    return INIT_UNKNOWN_FAILURE;
}



enum FGC_init_status InitNetwork(void)
{
    const enum FGC_eth_status status = FieldbusInit();

    if(status != ETH_STATUS_SUCCESS)
    {
        return EthStatusToInitStatus(status);
    }

    EnableNetworkInterrupt();

    if(devIdIsValid() && !dev.is_standalone)
    {
        // Skip fieldbus connection if we are in standalone mode

        // Wait for fieldbus link to be up

        EthWaitLinkUp();

        // Wait enough time to receive ALL advertised codes

        MSLEEP((FGC_CODE_MAX_SLOTS + 1)*FGC_FIELDBUS_CYCLE_PERIOD_MS);
    }

    return INIT_SUCCESS;
}



void InitTick(void)
{
    if(devIdIsValid() && !dev.is_standalone)
    {
        if (Wait10secForStablePLL())
        {
            // we can take 1ms Tick from PLD

            Disable1msTickInterrupt();
            Enable1msTickInterruptFromPLD();
        }
        else
        {
            // Note: We did not manage to use the PLL
            // We will keep on running the boot with local millisecond (from MCU timer)
            // This will happen if FPGA is not programmed
        }
    }
}

// EOF
