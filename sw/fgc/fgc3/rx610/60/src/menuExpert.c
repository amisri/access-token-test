/*---------------------------------------------------------------------------------------------------------*\
  File:     menuExpert.c

  Purpose:  Functions for the Expert submenu

\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>           // basic typedefs
#include <iodefines.h>          // specific processor registers and constants
#include <stdio.h>              // for fprintf()
#include <stdbool.h>

#include <ana_temp_regul.h>
#include <defsyms.h>
#include <dev.h>                // for dev global variable
#include <dsp_loader.h>         // for dsp global variable
#include <fieldbus_boot.h>      // for fieldbus, stat_var global variable
#include <hardware_setup.h>     // for SoftwareReset())
#include <mcuDependent.h>      // for MSLEEP(), USLEEP(), NVRAM_LOCK()
#include <mem.h>
#include <memmap_mcu.h>         // Include memory map constants
#include <menu.h>               // for MenuGetInt32U(), MenuGetInt16U(), MenuRspArg(), MenuRspError()
#include <pld_spi.h>            // for pld_stat_lbl global variable
#include <pld.h>                // for pld_info global variable
#include <runlog_lib.h>         // for RunlogClrResets(), RunlogInit()
#include <sharedMemory.h>      // for shared_mem global variable
#include <sleep.h>
#include <term.h>
#include <trap.h>               // for trap global variable, IsrDummy1(), IsrUndefinedInstruction()

/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertResetFaults(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Reset faults and latched status
\*---------------------------------------------------------------------------------------------------------*/
{
    stat_var.fgc_stat.class_data.c60.st_faults   = FGC_FLT_FGC_STATE;
    stat_var.fgc_stat.class_data.c60.st_warnings = 0;
    stat_var.fgc_stat.class_data.c60.st_latched  = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertPowerCycle(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Trigger a FGC power cycle using the NDI power cycle register
\*---------------------------------------------------------------------------------------------------------*/
{
    POWER_CYCLE();
    USLEEP(200);  // we only need a value bigger than 16us
    MenuRspError("Function failed");
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertGlobalReset(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Write a global reset cmd in the reset register in the PLD. Works only if bistream 2 is programmed.
\*---------------------------------------------------------------------------------------------------------*/
{
//    shared_mem.mainprog_seq = FGC_MP_STAY_IN_BOOT;      // Global reset = set device.boot it should stay in boot!
    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_GLOBAL_MASK16;    // Trigger global reset
    MenuRspError("Function failed: %s - Bstrm 0x%04X", TypeLabel(pld_stat_lbl, pld_info.state), MID_PLDTYPE_P);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertMCUsoftwareReset(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Software Rx610 reset (doesn't reset part of SFR, ...)
\*---------------------------------------------------------------------------------------------------------*/
{
    SoftwareReset();
    MenuRspError("Function failed");
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertSlowWatchdog(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  When not allowed the slow watchdog will not be triggered and after about 3s the FGC will reset.
\*---------------------------------------------------------------------------------------------------------*/
{
    dev.allow_slow_watchdog_refresh = !dev.allow_slow_watchdog_refresh;
    MenuRspArg("%u", dev.allow_slow_watchdog_refresh);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertFastWatchdog(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
    Toggle between

    if P6.DR.BIT.B5 is 0 then when the network card
    receive a "reset SHORT pulse signal" (50) from the gateway it trigger
    the power cycle and if it is receive "reset LONG pulse signal" (100)
    it also trigger the power cycle.

    if P6.DR.BIT.B5 is 1 (longer integration time) then when the network card
    receive a "reset SHORT pulse signal" (50) from the gateway it is ignored.
    It waits for a "reset LONG pulse signal" (100) to trigger the power cycle.

\*---------------------------------------------------------------------------------------------------------*/
{
    P6.DR.BIT.B5 = ~P6.PORT.BIT.B5;
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertStayInBoot(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
    This function can be called before a reset in order to set a flag in the shared memory and
    force the FGC3 to stay in the Boot after the reset. This is used e.g. in the case of the
    FGC3 Reception Test. It has no effect if used before a power cycle as it clears the shared memory.

    0 = Do not stay in Boot
    1 = Stay in Boot
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      choice;

    if ((*argv[0] != 0) && (MenuGetInt16U(argv[0], 0, 2, &choice) != 0)) return ;

    shared_mem.mainprog_seq = choice ? SEQUENCE_TEST_IN_BOOT : SEQUENCE_STAY_IN_MAIN;

    MenuRspArg("%u",  shared_mem.mainprog_seq);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertUnexpIrq(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function will test the trap function and the run log.
\*---------------------------------------------------------------------------------------------------------*/
{
    IsrDummy();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertIllegalInstruction(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  This function includes an illegal instruction to test the illegal instruction trap.
\*---------------------------------------------------------------------------------------------------------*/
{
    // 24 Jun 2009
    // ToDo: this is wrong, is calling the function that was not defined
    // so I added an empty one at isr.c
    IsrUndefinedInstruction();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuExpertShowStats(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show statistics
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(term.f, RSP_DATA ",Temp Regul    : %u\n", AnaTempGetStatus());
    fprintf(term.f, RSP_DATA ",Temp Reg. Isr : %u us\n", AnaTempGetProcessingTime());

    fprintf(term.f, RSP_DATA ",DSP run flag  : %s\n", (dsp.is_running ? "RUN" : "STOPPED"));

    // INT_Excep_IRQ4() - vector 68 - MCU_~TICK interrupt level
    // 0 means inactive, 6 is the normal active value used

    fprintf(term.f, RSP_DATA ",PLD Tick Prio : %u\n", ICU.IPR24.BIT.IPR_20);

    fprintf(term.f, RSP_DATA ",IsrDummy      : %u\n", trap.isrdummy_count);

    fprintf(term.f, RSP_DATA ",Access reg    : 0x%02X\n", *(uint8_t *)&dev.reg); // Silent warning

    fprintf(term.f, RSP_DATA ",NVRAM protect : %u\n", NVRAM_LOCKED); // read status

    fprintf(term.f, RSP_DATA ",Net type      : %s\n", "ETHERNET");
    fprintf(term.f, RSP_DATA ",Ana type      : %s\n", TypeLabel(sym_names_interface, MID_ANATYPE_P));
    fprintf(term.f, RSP_DATA ",Crate type    : %s\n", sym_names_crate_type[dev.crate_type_idx].label);
}



void MenuExpertForceMain(uint16_t argc, char ** argv)
{
    main_misc.force_main = true;
}

// EOF
