/*---------------------------------------------------------------------------------------------------------*\
 File:      main.c

 Purpose:   60 - Boot - FGC3

 Author:    daniel.calcoen@cern.ch

 History:
    12 may 06   doc Created
    28 nov 06   pfr Modified for FGC3 maquette
    09 jul 09   doc ported to GNU gcc v902


   main chips

   Renesas Rx6108 processor
                   family: Rx600
                   code  : R5F56108VNFP
           25MHz

   Texas   TMS320c6727b DSP
                   family: Texas TMS 320 C 6000
           code  : TMS 320 C 6727B GDH 300

   Xilinx Spartan 3AN FPGA
                   family: Spartan
           code  : XC3S 700AN - 4FG484C

ADC
ADS1274IPAPT

DAC
MAX5541CSA
16 bits

\*---------------------------------------------------------------------------------------------------------*/

#define FGC_GLOBALS                 // to instantiate fgc_code_sizes_blks[], fgc_code_names[] in fgc_codes.h
#define GLOBAL_VARS                 // to instantiate fgc_rl_length[], fgc_rl_label[], global variables in fgc_runlog_entries.h
#define MENUTREE                    // to instantiate global variables in menutree.h
#define DEFSYMS                     // to instantiate global variables in defsyms.h

#define BOOT_DUAL_PORT_RAM_GLOBALS  // to instantiate dpram
#define MCU_TIME_CONSUMED_GLOBALS   // to instantiate mcu_time_consumed_in
#define SHARED_MEMORY_GLOBALS       // to instantiate shared_mem
#define DEV_GLOBALS                 // to instantiate dev
#define MAIN_GLOBALS                // to instantiate timeout_ms, main_misc

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <main.h>
#include <iodefines.h>
#include <syscalls.h>
#include <ana_temp_regul.h>
#include <analog.h>
#include <boot_dual_port_ram.h>
#include <codes.h>
#include <defsyms.h>
#include <derived_clocks.h>
#include <dev.h>
#include <diag_boot.h>
#include <fgc_runlog_entries.h>
#include <fieldbus_boot.h>
#include <hardware_setup.h>
#include <led.h>
#include <macros.h>
#include <master_clk_pll.h>
#include <memmap_mcu.h>
#include <menu_node.h>
#include <menu.h>
#include <menutree.h>
#include <pld_spi.h>
#include <pld.h>
#include <pll.h>
#include <registers.h>
#include <runlog_lib.h>
#include <scivs.h>
#include <sharedMemory.h>
#include <sleep.h>
#include <term_boot.h>
#include <term.h>
#include <time_fgc.h>
#include <trap.h>
#include <version.h>
#include <ethernet_boot.h>
#include <init.h>
#include <initterm.h>
#include <dsp_loader.h>



enum boot_status
{
    BOOT_RUN_MAIN,
    BOOT_RUN_MENU_FLAG,
    BOOT_ABORT_FLAG,
    BOOT_USER_REQUEST,
    BOOT_DSP_STANDALONE,
    BOOT_TEST,
    BOOT_FATAL_DSP_FAULT,
    BOOT_FATAL_CODE_FAULT,
    BOOT_FATAL_MEM_FAULT,
    BOOT_FATAL_INVALID_CLASS_ID,
    BOOT_FATAL_MISSING_NAMEDB,
    BOOT_FATAL_DEV_NOT_IN_NAMEDB,
    BOOT_FATAL_SELF_TEST_FAILURE,
    BOOT_FATAL_ETH_CHIP_INACCESSIBLE,
    BOOT_FATAL_UNKNOWN_ETH_CHIP,
    BOOT_FATAL_CORRUPTED_CODE,
    BOOT_FATAL_IDDB_PTDB_MISMATCH,
    BOOT_FATAL_SYSDB_COMPDB_DIMDB_MISMATCH,
    BOOT_FATAL_M16C62_FAULT,
    BOOT_FATAL_MAIN_NAMEDB_MISMATCH,
    BOOT_FATAL_UNKNOWN_PLD_HW,
    BOOT_FATAL_PLD_TIMEOUT,
    BOOT_FATAL_ANA_DSP_TIMEOUT,
    BOOT_FATAL_UNKNOWN,
    BOOT_STATUS_COUNT,
};



struct boot_status_struct
{
    char const * label;
    bool         is_fatal;
};


// ---------- External variable declarations

//! Memory area shared by boot and main, mapped to 0x00000560 by the linker

volatile struct Shared_memory __attribute__((section("shared_area"))) shared_mem;


static const struct boot_status_struct boot_status_labels[] =
{
    { "run main"                    , false },
    { "run menu flag"               , false },
    { "abort flag"                  , false },
    { "user request"                , false },
    { "dsp standalone"              , false },
    { "test"                        , false },
    { "dsp fault"                   , true  },
    { "code fault"                  , true  },
    { "mem fault"                   , true  },
    { "invalid class id"            , true  },
    { "missing namedb"              , true  },
    { "dev not in namedb"           , true  },
    { "self-test failure"           , true  },
    { "eth chip inaccessible"       , true  },
    { "unknown eth chip"            , true  },
    { "corrupted code"              , true  },
    { "iddb ptdb mismatch"          , true  },
    { "sysdb compdb dimdb mismatch" , true  },
    { "m16c62 fault"                , true  },
    { "main namedb mismatch"        , true  },
    { "unknown pld hw"              , true  },
    { "pld timeout"                 , true  },
    { "ana dsp timeout"             , true  },
    { "unknown"                     , true  },
};

static enum boot_status boot_status;

static_assert(sizeof(boot_status_labels) / sizeof(boot_status_labels[0]) == BOOT_STATUS_COUNT, "Size of stay in boot label array doesn't match corresponding enum");



static enum boot_status initStatusToBootStatus(const enum FGC_init_status status)
{
    switch(status)
    {
        case INIT_SUCCESS:               return BOOT_RUN_MAIN;
        case INIT_MISSING_NAMEDB:        return BOOT_FATAL_MISSING_NAMEDB;
        case INIT_INVALID_CLASS_ID:      return BOOT_FATAL_INVALID_CLASS_ID;
        case INIT_DEV_NOT_IN_NAMEDB:     return BOOT_FATAL_DEV_NOT_IN_NAMEDB;
        case INIT_SELF_TEST_FAILURE:     return BOOT_FATAL_SELF_TEST_FAILURE;
        case INIT_ETH_CHIP_INACCESSIBLE: return BOOT_FATAL_ETH_CHIP_INACCESSIBLE;
        case INIT_UNKNOWN_ETH_CHIP:      return BOOT_FATAL_UNKNOWN_ETH_CHIP;
        case INIT_UNKNOWN_FAILURE:       return BOOT_FATAL_UNKNOWN;
    }

    return BOOT_FATAL_UNKNOWN;
}



static enum boot_status codesStatusToBootStatus(const enum FGC_codes_status status)
{
    switch(status)
    {
        case CODES_SUCCESS:                     return BOOT_RUN_MAIN;
        case CODES_CORRUPTED_CODE:              return BOOT_FATAL_CORRUPTED_CODE;
        case CODES_IDDB_PTDB_MISMATCH:          return BOOT_FATAL_IDDB_PTDB_MISMATCH;
        case CODES_SYSDB_COMPDB_DIMDB_MISMATCH: return BOOT_FATAL_SYSDB_COMPDB_DIMDB_MISMATCH;
        case CODES_M16C62_FAILURE:              return BOOT_FATAL_M16C62_FAULT;
        case CODES_MAIN_NAMEDB_MISMATCH:        return BOOT_FATAL_MAIN_NAMEDB_MISMATCH;
    }

    return BOOT_FATAL_UNKNOWN;
}



static enum boot_status anaStatusToBootStatus(const enum FGC_ana_status status)
{
    switch(status)
    {
        case ANA_SUCCESS:              return BOOT_RUN_MAIN;
        case ANA_DSP_TIMEOUT:          return BOOT_FATAL_ANA_DSP_TIMEOUT;
    }

    return BOOT_FATAL_UNKNOWN;
}



static inline enum boot_status bootStatusGet(void)
{
    return boot_status;
}



static inline bool bootStatusIsFatal(void)
{
    return boot_status_labels[boot_status].is_fatal;
}



static inline char const * bootStatusGetLabel(void)
{
    return boot_status_labels[boot_status].label;
}



static inline void bootStatusSet(const enum boot_status status)
{
    if(boot_status == BOOT_RUN_MAIN)
    {
        boot_status = status;
    }
    else if(   boot_status_labels[boot_status].is_fatal == false
            && boot_status_labels[status     ].is_fatal == true)
    {
        boot_status = status;
    }
}



static void mainProcessMainProgSequence(void)
{
    // the main program has signalled to stay in boot s device.boot

    if(shared_mem.mainprog_seq == SEQUENCE_STAY_IN_BOOT)
    {
        // Next reset or power cycle should go to Main

        shared_mem.mainprog_seq = SEQUENCE_STAY_IN_MAIN;

        bootStatusSet(BOOT_USER_REQUEST);
    }

    // If the main program has signalled to stay in boot for testing

    if(shared_mem.mainprog_seq == SEQUENCE_TEST_IN_BOOT)
    {
        bootStatusSet(BOOT_TEST);
    }
}



static bool UserRunMain(void)
{
    uint16_t ii;
    uint16_t t;
    uint8_t  ch = 0;

    fputs("\nPress ESC to stay in boot [ ] ", term.f);

    for(ii = 0, t = 5; ii <= 50; ii++)
    {
        if((ii % 10) == 0)
        {
            fprintf(term.f, "\b\b\b%u] ", t--);
        }

        // Wait up to 100ms for a character

        ch = TermGetCh(100);

        // If ESC received

        if(ch == 0x1B || dev.abort_f == true)
        {
            // Run menus interactively

            fputs(TERM_CLR_LINE, term.f);

            return false;
        }

        // If CTRL-Z received or CTRL-C or TEST LEDS pressed

        if(ch == 0x1A)
        {
            // Run menus in direct mode

            term.edit_state = TERM_STATE_DIRECT;

            return false;
        }

        // Space - Run main program

        if(ch == ' ')
        {
            return true;
        }
    }

    // If menus not required

    fputs(TERM_CLR_LINE "\r", term.f);

    // All OK, the return and jump to the Main

    return true;
}



bool bootIsMainAllowed(const bool stop_on_non_fatal)
{
    // Always check the codes before requesting to run the main program

    enum FGC_codes_status code_status = CodesVerifyInstalled();

    if(code_status != CODES_SUCCESS)
    {
        bootStatusSet(codesStatusToBootStatus(code_status));
    }

    const enum boot_status status = bootStatusGet();

    if(status != BOOT_RUN_MAIN)
    {
        if(bootStatusIsFatal())
        {
            if(main_misc.force_main == true)
            {
                return true;
            }

            fprintf(term.f, RSP_ERROR ",Transition to main refused - fatal error: %s\n", bootStatusGetLabel());
            fprintf(term.f, RSP_ERROR ",Fix the problem and reset the device\n");
        }
        else
        {
            if(stop_on_non_fatal == false)
            {
                return true;
            }

            fprintf(term.f, RSP_ERROR ",Transition to main stopped: %s\n", bootStatusGetLabel());
            fprintf(term.f, RSP_ERROR ",You can still run the main program with menu option '0'\n");
        }

        RunlogWrite(FGC_RL_MAIN_REFUSED, &status);

        return false;
    }

    return true;
}



static void checkBootFaults(void)
{
    // Do not load the Main program if the stand-alone switch is asserted.
    // During operation this prevents to run the DSP Main program in stand-
    // alone if for example the physical line on the FGC backplane is short-
    // circuited. To run the DSP in stand-alone one has to use the menu
    // explicitly.

    if((CPU_MODE_P & CPU_MODEL_DSPSTAL_MASK8) != 0)
    {
        bootStatusSet(BOOT_DSP_STANDALONE);
    }

    // Too many resets due to DSP not running

    const struct TRunlog * const runlog_ptr = (struct TRunlog *)NVRAM_RL_BUF_32;

    if(runlog_ptr->resets.dsp == 3)
    {
        bootStatusSet(BOOT_FATAL_DSP_FAULT);
    }

    // TODO The CODE_FAULT is set every time there's a code update, which is annoying
    // as it doesn't allow to run main until reset is performed.
    // We should rethink if this fault is needed at all.

    if(stat_var.fgc_stat.class_data.c60.st_latched & FGC_LAT_CODE_FLT)
    {
        // bootStatusSet(BOOT_FATAL_CODE_FAULT);
    }

    if(stat_var.fgc_stat.class_data.c60.st_latched & FGC_LAT_MEM_FLT)
    {
        bootStatusSet(BOOT_FATAL_MEM_FAULT);
    }
}



static void checkDirectMode(void)
{
    // Check for direct mode operation (if CTRL-Z was pressed)

    if(TermRxFlush())
    {
        // Start menus in direct mode

        term.edit_state = TERM_STATE_DIRECT;

        bootStatusSet(BOOT_RUN_MENU_FLAG);
    }

    // Report edit_state
    // TODO Do we really need this?

    fprintf(term.f, TERM_CLR_LINE "$S,%d\n", term.edit_state);
}



static inline void checkAbortFlag(void)
{
    if(dev.abort_f)
    {
        bootStatusSet(BOOT_ABORT_FLAG);
    }
}



/*!
 * Runs boot menu system
 */
static void bootRunMenus(void)
{
    while(true)
    {
        // Reset receiving command flag

        term.recv_cmd_f = 0;

        MenuMenu(&root_menu);
    }
}



/*!
 * Starts boot menu system or runs the main program
 */
static void bootRun(void)
{
    checkBootFaults();
    checkDirectMode();
    checkAbortFlag();

    bool run_main = bootIsMainAllowed(true);

    if(run_main)
    {
        run_main = UserRunMain();

        if(run_main)
        {
            bootRunMain();
        }
    }

    bootRunMenus();
}


/*!
 * Determines cause of startup (PWR, SLOW or FAST)
 *
 * The cause is returned via the SM BOOTTYPE field and is recorded in the run log.
 */
static void DetectCauseOfStartup(void)
{
    // Turn off M16C62

    CPU_RESET_CTRL_P |= CPU_RESET_CTRL_C62OFF_MASK16;

    // Check reset type

    RunlogWrite(FGC_RL_RST_REG, (void *)&CPU_RESET_SRC_P);
    RunlogWrite(FGC_RL_MODE_REG, (void *)CPU_MODE_32);

    // If power cycle reset

    if (CPU_RESET_SRC_P & CPU_RESET_SRC_POWER_MASK16)
    {
        shared_mem.boot_seq = BOOT_PWR;
    }
    else
    {
        shared_mem.boot_seq = BOOT_NORMAL;
    }

    // Record boot type in run log

    RunlogWrite(FGC_RL_BOOT_TYPE, (void *) & (shared_mem.boot_seq));
}



static void InitClocks(void)
{
    // The ACT transfer clock is derived from DSP NMI Tick
    // so DSP NMI Tick must be correctly programmed for ACT
    // clock to work 20us from NMI generation to ACT transfer

    TICK_DSP_ACT_DELAY_US_P = 20;
}



static void testLeds(void)
{
    volatile uint32_t i;

    RGLEDS_P = 0x0000;

    // Test red LEDs

    REFRESH_SLOWWATCHDOG();

    RGLEDS_P = ALL_RED;

    for (i = 0; i < 0x005FFFFF; ++i)
    {
        ;
    }

    RGLEDS_P = 0x0000;

    // Test green LEDs

    REFRESH_SLOWWATCHDOG();

    RGLEDS_P = ALL_GREEN;

    for (i = 0; i < 0x005FFFFF; ++i)
    {
        ;
    }

    RGLEDS_P = 0x0000;

    // Test blue LEDs

    REFRESH_SLOWWATCHDOG();

    BLED_SET_ALL();

    for (i = 0; i < 0x005FFFFF; ++i)
    {
        ;
    }

    BLED_RST_ALL();

    REFRESH_SLOWWATCHDOG();
}



int main(void)
{
    LED_RST_ALL();      // LEDs OFF
    BLED_RST_ALL();     // Blue LEDs OFF

    testLeds();

    BLED_SET(PIC_BLUE);
    BLED_SET(PSU_BLUE);

    trap.isrdummy_count = 0;

    stat_var.fgc_stat.class_data.c60.st_faults = FGC_FLT_FGC_STATE;
    stat_var.fgc_stat.class_data.c60.state_pll = FGC_PLL_NO_SYNC;
    pll.state                                  = FGC_PLL_NO_SYNC;

    // Enable in interactive mode

    term.edit_state = TERM_STATE_INTERACTIVE;
    
    shared_mem.network_card_model = MID_NETTYPE_P;
    if (   shared_mem.network_card_model > FGC_NETWORK_UNDEFINED_NET
        && shared_mem.network_card_model < FGC_NETWORK_NO_NET_BOARD)
    {
        shared_mem.network_card_model = FGC_NETWORK_UNDEFINED_NET;
    }

    // when we restart without a power down, the PLD is programmed
    // and the clocks are running
    // the problem is specially with the DSP NMI interrupt that crash the boot of the DSP
    // Innocuous if the PLD is not programmed

    StopDerivedClocks();    // Dsp, Adc, Dac

    // in case of Power cycle, the resets are automatically set
    // in FGC3 we don't try to autorecover after a global reset so
    // we manually assert the resets
    // Reset peripherals

    CPU_RESET_CTRL_P |= (CPU_RESET_CTRL_C62OFF_MASK16 |
                         CPU_RESET_CTRL_DSP_MASK16    |
                         CPU_RESET_CTRL_ANALOG_MASK16 |
                         CPU_RESET_CTRL_QSM_MASK16    |
                         CPU_RESET_CTRL_DIGITAL_MASK16);   // Clear dig reset

    if(DevSetPldHwVersion() != DEV_SUCCESS)
    {
        bootStatusSet(BOOT_FATAL_UNKNOWN_PLD_HW);
    }

    dev.allow_slow_watchdog_refresh = true;

    // NewLib uses malloc to allocate the reentrant structure (1 per thread)

    InitMalloc();

    main_misc.state_machine_is_running = FALSE;
    main_misc.state_machine_slice_mask = 1;

    // To start using USLEEP() and MSLEEP() even with the PLD not programmed

    Enable1msTickInterruptFromMcuTimer();

    // Set I flag in PSW, global enable of interrupts

    __asm__ volatile("setpsw I");

    // Wait for the PLD to boot
    // When the PLD configures from internal SPI it takes about 109 ms

    if(PldWaitForDONEandReadModel() != PLD_SUCCESS)
    {
        bootStatusSet(BOOT_FATAL_PLD_TIMEOUT);
    }

    CodesVerifyNvram();
    CodesPLDSelect();

    // Run log has to be initialised after the PLD have started, because some of the registers are needed
    // TODO Reset source is processed in two places - in RunlogInit and below the call - try to put it together

    RunlogInit();
    RunlogWrite(FGC_RL_BOOT_START, NULL);

    DevSetCrateType(pld_info.ok);
    DevSetResetSource(pld_info.ok);

    // Clear peripherals resets

    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_ANALOG_MASK16;
    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_QSM_MASK16;
    CPU_RESET_CTRL_P &= ~CPU_RESET_CTRL_DIGITAL_MASK16;

    scivsInit();

    // Start analogue interface temperature regulation

    AnaTempInit();

    VerifyPeripheralArePresent();

    DetectCauseOfStartup();

    InitSharedMem();
    CodesCountFlashLocks();

    // Start SCI interfaces

    if(EnableTerminalStream() != TERM_SUCCESS)
    {
        // TODO How to handle this failure?
    }

    // Releases FIP/ETHERNET hw reset, reset line must have an internal pull-up

    P9.DR.BIT.B7 = 1;

    enum FGC_init_status init_status = InitNetwork();

    if(init_status != INIT_SUCCESS)
    {
        bootStatusSet(initStatusToBootStatus(init_status));
    }

    MasterClockAnaloguePllInit();

    InitTick();

    InitTerm(term.f);

    // On REGFGC3, this must be called ~800ms after the crate reset (SCIVS reset) is released
    // otherwise we will get trash reading DIMs before they are initialised

    DiagInit();

    BLED_RST(PIC_BLUE);
    BLED_RST(PSU_BLUE);
    BLED_SET(DCCT_BLUE);
    BLED_SET(FGC_BLUE);

    init_status = InitOperationalMode();

    if(init_status != INIT_SUCCESS)
    {
        bootStatusSet(initStatusToBootStatus(init_status));
    }

    BLED_RST(DCCT_BLUE);
    BLED_RST(FGC_BLUE);
    BLED_SET(FIP_BLUE);
    BLED_SET(VS_BLUE);

    enum FGC_codes_status codes_status = CodesInitM16C62();

    if(codes_status != CODES_SUCCESS)
    {
        bootStatusSet(codesStatusToBootStatus(codes_status));
    }

    InitClocks();

    StartActClock();

    init_status = InitSelfTest();

    if(init_status != INIT_SUCCESS)
    {
        bootStatusSet(initStatusToBootStatus(init_status));
    }

    // Analogue module needs DSP NMI tick already running for its ADC tick generation

    enum FGC_ana_status ana_status = AnalogInit();

    if(ana_status != ANA_SUCCESS)
    {
        // TODO Remove this special handling of DSP_TIMEOUT when the problem with DSP is solved
        // The initialisation routine will fail with timeout in case DSP isn't running
        // We don't care about DSP in the boot and we shouldn't stop transition to main

        if(ana_status != ANA_DSP_TIMEOUT)
        {
            bootStatusSet(anaStatusToBootStatus(ana_status));
        }
    }

    BLED_RST(FIP_BLUE);
    BLED_RST(VS_BLUE);
    BLED_SET(FGC_BLUE);

    mainProcessMainProgSequence();

    ReportBoot();

    CheckStatus();

    bootRun();

    // Never reached

    return(0);
}
/*---------------------------------------------------------------------------------------------------------*/
void CheckStatus(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called whenever the boot is waiting for keyboard characters.
  It will be called at up to 1kHz.
  It polls various status variables that are set by the ISR functions and set the appropriate
  faults, warnings and status bits.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (diag_bus.check_f != 0)              // If diag data/state should be checked
    {
        DiagCheck();                // Check analogue and digital values
    }


    // --- Set/Clr boot warnings ---

    if (Test(stat_var.fgc_stat.class_data.c60.st_latched, (FGC_LAT_CODE_FLT
                                                           | FGC_LAT_DSP_FLT
                                                           | FGC_LAT_DIM_SYNC_FLT
                                                           | FGC_LAT_MEM_FLT
                                                           | FGC_LAT_RFC_FLT))
       )
    {
        Set(stat_var.fgc_stat.class_data.c60.st_warnings, FGC_WRN_FGC_HW);
    }
    else
    {
        Clr(stat_var.fgc_stat.class_data.c60.st_warnings, FGC_WRN_FGC_HW);
    }


    if (Test(stat_var.fgc_stat.class_data.c60.st_latched, FGC_LAT_PSU_V_FAIL))
    {
        Set(stat_var.fgc_stat.class_data.c60.st_warnings, FGC_WRN_FGC_PSU);
    }
    else
    {
        Clr(stat_var.fgc_stat.class_data.c60.st_warnings, FGC_WRN_FGC_PSU);
    }


    if (Test(stat_var.fgc_stat.class_data.c60.st_warnings, FGC_WRN_FGC_PSU))
    {
        LED_SET(PSU_RED);
        LED_SET(PSU_GREEN);
    }
    else
    {
        LED_RST(PSU_RED);
        LED_SET(PSU_GREEN);
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void ReportBoot(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will report the state of the FGC.
\*---------------------------------------------------------------------------------------------------------*/
{
    const struct TRunlog * const runlog_ptr = (const struct TRunlog *)NVRAM_RL_BUF_32;

    fprintf(term.f, TERM_INIT "PLD STAT: " TERM_BOLD "%-30s" TERM_NORMAL,
            TypeLabel(pld_stat_lbl, pld_info.state));
    fprintf(term.f, "WATCHDOG: " TERM_BOLD "%s" TERM_NORMAL,
            ((CPU_MODE_P & CPU_MODEL_WDINHBT_MASK8) ? "INHIBITED" : "ENABLED"));
    fprintf(term.f, "\nBUILT:    " TERM_BOLD "%-30s" TERM_NORMAL, version.unixtime_a);
    fprintf(term.f, "DSP:      " TERM_BOLD "%s" TERM_NORMAL,
            ((CPU_MODE_P & CPU_MODEL_DSPSTAL_MASK8) ? "STANDALONE" : "SLAVE"));
    fprintf(term.f, "\nRESETS:   " TERM_BOLD "%-30u" TERM_NORMAL, runlog_ptr->resets.all);
    fprintf(term.f, "M16C62:   " TERM_BOLD "%s" TERM_NORMAL,
            ((C62_P & C62_STANDALONE_MASK16) ? "STANDALONE" : "SLAVE"));
    fprintf(term.f, "\nDSP STAT: " TERM_BOLD "%-30s" TERM_NORMAL,
            dspLoadErrorStr(dsp.status));
    fprintf(term.f, "RESET:    " TERM_BOLD "%s" TERM_NORMAL, TypeLabel(reset_source_lbl, dev.rst_src));

    // If pld programmed, display info from the pld
    if (pld_info.state == PLD_STATE_INIT_AND_DONE_ARE_HI) // PLD_STATE_INIT_AND_DONE_ARE_LOW is the ok condition
    {
        fprintf(term.f, "\nCRATE:    " TERM_BOLD "%3u:%-26s" TERM_NORMAL, dev.crate_type,
                sym_names_crate_type[dev.crate_type_idx].label);
        fprintf(term.f, "PLD VERS: " TERM_BOLD "%04x" TERM_NORMAL, MID_PLDVER_P);
        fprintf(term.f, "\nANA:      " TERM_BOLD "%-30s" TERM_NORMAL,
                TypeLabel(sym_names_interface, shared_mem.analog_card_model));
        fprintf(term.f, "BITSTRM:  " TERM_BOLD "%-30u" TERM_NORMAL, MID_PLDTYPE_P & 0x03);
        fprintf(term.f, "\nFGC3:     " TERM_BOLD "3.1.%-26u" TERM_NORMAL, dev.revision);
        fprintf(term.f, "PLD ISF:  " TERM_BOLD "%-30s\n" TERM_NORMAL,
                TypeLabel(pld_prog_lbl, pld_info.spi_state));
    }

    // these are the values reported by the gateway, according to the device name
    // i.e. dev_name.class_id
    // And can be different to the ones according to the FGC hardware
    // i.e. dev.class_id

    fprintf(term.f, "CLASS ID: " TERM_BOLD "%-30u" TERM_NORMAL, dev_name.class_id);
    fprintf(term.f, "DEVICE:   " TERM_BOLD "%s\n" TERM_NORMAL, dev_name.name);

    fprintf(term.f, "GATEWAY:  " TERM_BOLD "%-30s" TERM_NORMAL, gw_name.name);
    fprintf(term.f, "FIELDBUS ID:   " TERM_BOLD "%u" TERM_NORMAL "\n", dev.fieldbus_id);
}
/*---------------------------------------------------------------------------------------------------------*/
void bootRunMain(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called to run the main program
\*---------------------------------------------------------------------------------------------------------*/
{
    fputs(RSP_DATA ",Running main program...\n" RSP_OK "\n", term.f);

    // Wait to allow response to be sent (and read)

    MSLEEP(1000);

    BLED_RST_ALL();

    // or OS_ENTER_CRITICAL_7() not OS_ENTER_CRITICAL() because it leaves the timers running

    DISABLE_INTERRUPTS();

    DisableNetworkInterrupt();
    Disable1msTickInterrupt();

    // disable the 2 that are never disabled

    // TMR3 OVI3 - Timer Overflow Interrupt - vector 185 - INT_Excep_TMR3_OVI3()
    // TMR3 will count [0..255] incremented by TMR2 count match (1us) and generating an interrupt when overflows
    ICU.IER17.BIT.IEN1 = 0;             // Interrupt Request Enable Register
    ICU.IPR6B.BIT.IPR_20 = 0x00;        // Interrupt Priority Register

    // TPU0.TGRA input capture/compare match Interrupt - vector 104 - INT_Excep_TPU0_TGI0A()
    ICU.IER0D.BIT.IEN0 = 0;             // interrupt Request Enable Register
    ICU.IPR4C.BIT.IPR_20 = 0x00;        // Interrupt Priority Register


    // Turn off all LEDs

    RGLEDS_P = 0x0000;

    // Set main program start time to Unix time

    shared_mem.run_software_utc = timeGetUtcTime();

    // Jump to main program

    __asm__ volatile                  \
    (                                 \
     ".extern _entry_main    \n\t"    \
     "bra.a  _entry_main     \n\t"    \
    );
}

// EOF
