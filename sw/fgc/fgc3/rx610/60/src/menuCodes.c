/*---------------------------------------------------------------------------------------------------------*\
  File:         menuCodes.c

  Purpose:      Functions for the Codes submenu

  Author:       Quentin.King@cern.ch

  Notes:

  History:

    13/04/04    qak     Created
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdio.h>
#include <codes.h>
#include <dev.h>
#include <fgc_codes_gen.h>
#include <fieldbus_boot.h>
#include <mcuDependent.h>
#include <mem.h>
#include <menu.h>
#include <os.h>
#include <term.h>

// TODO Try to merge this function with CodesReport
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesShowInstalled(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Report installed codes
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dev_idx;

    CodesCheckUpdates();

    fputs(RSP_LABEL "," TERM_UL "FLASH ZONE|    RESIDENT CODE   |CLS:CODE:CALC:STATE     |CODE AFTER UPDATE |\n"
          TERM_NORMAL, TERM_TX);

    for (dev_idx = 0; dev_idx < SHORT_LIST_CODE_LAST; dev_idx++)
    {
        struct TCodesHolderInfo * code_info = codes_holder_info + dev_idx;

        if (code_info->update.version != 0 || code_info->update_state == UPDATE_STATE_UPDATE)
        {
            fputs(TERM_BOLD, TERM_TX);
        }

        fprintf(TERM_TX, RSP_DATA ",%2u:%-7s|%-20s|%3u:%04X:%04X:%-10s|%-7s|%04X      |\n"
                RSP_DATA "," TERM_UL "          |          %10lu|     %19s|       |%10lu|\n" TERM_NORMAL,
                dev_idx,
                code_info->label,
                code_info->dev_state != CODE_STATE_VALID ? "Invalid code" : fgc_code_names[code_info->dev.code_id],
                code_info->dev.class_id,
                code_info->dev.crc,
                code_info->calc_crc,
                TypeLabel(code_state, code_info->dev_state),
                TypeLabel(update_state, code_info->update_state),
                code_info->update.crc,
                (code_info->dev.version == 0xFFFFFFFF ? (uint32_t)code_info->dev.old_version : code_info->dev.version),
                (char *)code_info->dev.version_string,
                (code_info->update.version == 0xFFFFFFFF ? (uint32_t)code_info->update.old_version :
                 code_info->update.version));
    }

    fprintf(TERM_TX, RSP_DATA ",Writable devices:%u\n", CodesGetNumRwDevsToUpdate());
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesShowAdvertised(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Show advertised code table received from the gateway and indicate which code is being transmitted.
\*---------------------------------------------------------------------------------------------------------*/
{
    OS_CPU_SR   cpu_sr;
    uint16_t      adv_idx;
    uint16_t      fip_class_id;                   // Class ID in most recent time var
    uint16_t      fip_code_id;                    // Code ID in most recent time var
    uint16_t      fip_blk_idx;                    // Code block index in most recent time var
    uint16_t      blk_idx;

    if (!fieldbus.is_gateway_online)
    {
        MenuRspError("GW offline");
        return;
    }

    CodesCheckUpdates();

    OS_ENTER_CRITICAL();

    fip_class_id = time_var.code_var_class_id;  // Take local copy of most recent time var code header
    fip_code_id  = time_var.code_var_id;
    fip_blk_idx =  time_var.code_var_idx;

    OS_EXIT_CRITICAL();

    for (adv_idx = 0; adv_idx < FGC_CODE_MAX_SLOTS; adv_idx++)  // or FGC_NUM_CODES ?
    {
        struct advertised_code_info * code_adv = (struct advertised_code_info *)code.adv + adv_idx;

        if (code.adv_state[adv_idx] == ADV_CODE_STATE_OK &&
            code_adv->class_id  == fip_class_id &&
            code_adv->code_id   == fip_code_id)
        {
            fputs(TERM_BOLD, TERM_TX);
            blk_idx = fip_blk_idx;
        }
        else
        {
            blk_idx = 0;
        }

        fprintf(TERM_TX, RSP_DATA ",%2u:%-15s:%10lu:%04X:%5u:%5u:%s\n" TERM_NORMAL,
                adv_idx, (code_adv->code_id < FGC_NUM_CODES ? fgc_code_names[code_adv->code_id] : ""),
                code_adv->version != 0xFFFFFFFF ? code_adv->version : code_adv->old_version,
                code_adv->crc,
                blk_idx,
                code_adv->len_blks,
                TypeLabel(adv_code_state, code.adv_state[adv_idx])
               );
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesFieldbusUpdate(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Update all codes from Fieldbus.
\*---------------------------------------------------------------------------------------------------------*/
{
    CodesResetUpdateFailures();
    CodesUpdate(CODES_RX_FIELDBUS);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesTerminalUpdate(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Update one code from terminal.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (P2.PORT.BIT.B2 == 1)    // USB not ready
    {
        MenuRspError("No USB connection");
        return;
    }

    CodesResetUpdateFailures();
    CodesUpdate(CODES_RX_TERMINAL);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesToggleFlashLock(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Toggle flash device lock
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dev_idx;
    uint8_t  * dev_lock;

    if (MenuGetInt16U(argv[0], 0, (SHORT_LIST_CODE_LAST - 1), &dev_idx)) // Get flash device index
    {
        return;
    }

    NVRAM_UNLOCK();

    dev_lock = (uint8_t *)(NVRAM_FLASHLOCKS_32 + (uint32_t)dev_idx);

    if (*dev_lock)
    {
        *dev_lock = FALSE;
        codes_holder_info[dev_idx].locked_f = FALSE;

        shared_mem.code_locked_count--;
    }
    else
    {
        *dev_lock = TRUE;
        codes_holder_info[dev_idx].locked_f = TRUE;

        shared_mem.code_locked_count++;
    }

    NVRAM_LOCK();

    CodesCheckUpdates();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuCodesFlashErase(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Erase a CPU flash device
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      dev_idx;

    if (MenuGetInt16U(argv[0], 0, (SHORT_LIST_CODE_LAST - 1), &dev_idx)) // Get target flash device
    {
        return;
    }

    fprintf(TERM_TX, RSP_DATA ",Erasing flash device: %s\n", codes_holder_info[dev_idx].label);

    erase_dev[dev_idx](dev_idx);
    check_dev[dev_idx](dev_idx);
}



void MenuCodesPrintNameDb(uint16_t argc, char ** argv)
{
    const struct fgc_dev_name * const name_db = (const struct fgc_dev_name *)codes_holder_info[SHORT_LIST_CODE_NAMEDB].base_addr;

    if(codes_holder_info[SHORT_LIST_CODE_NAMEDB].dev_state != CODE_STATE_VALID)
    {
        fprintf(TERM_TX, RSP_DATA ",NameDB is corrupted\n");
    }

    uint16_t fieldbus_id;

    for(fieldbus_id = 0; fieldbus_id < FGC_CODE_NAMEDB_MAX_DEVS; fieldbus_id++)
    {
        // Skip empty entries

        if(name_db[fieldbus_id].class_id == FGC_CLASS_UNKNOWN &&
           name_db[fieldbus_id].name[0]  == '\0')
        {
            continue;
        }

        fprintf(TERM_TX, RSP_DATA ",Fieldbus ID: %d, class ID: %d, name: %s\n",
                fieldbus_id,
                name_db[fieldbus_id].class_id,
                name_db[fieldbus_id].name);
    }
}

// EOF
