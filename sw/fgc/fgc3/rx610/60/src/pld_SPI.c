/*---------------------------------------------------------------------------------------------------------*\
 File:          pld_SPI.c

 Purpose:       handle the FPGA ISF (it is a SPI based serial flash)
                Xilinx literature some times refer to it as
                 ISF = Internal System Flash
                and other as
                 ISF = In-System Flash

 Author:        daniel.calcoen@cern.ch

 Notes:         This file contains functions that deal with the FPGA Internal System Flash

                The Xilinx documentation are
                   UG331(v1.5) Spartan-3 Generation Configuration User Guide
                   UG333(v2.1) Spartan-3AN FPGA In-System Flash User Guide

 History:

   20 aug 08    doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#define PLD_SPI_GLOBALS
#include <stdint.h>           // basic typedefs
#include <iodefines.h>          // specific processor registers and constants

#include <codes.h>
#include <fgc_consts_gen.h>
#include <memmap_mcu.h>
#include <pld_spi.h>
#include <pld.h>
#include <sleep.h>
#include <term.h>

/*---------------------------------------------------------------------------------------------------------*\
   Xilinx Spartan 3AN FPGA
                   family: Spartan

   code  : XC3S 700AN

   ISF (Internal System Flash) has :

       1 ROM    = 16 sectors = 512 blocks = 4096 pages = 1081344 bytes = 8650752 bits
       1 sector =               32 blocks =  256 pages =   67584 bytes =  540672 bits
       1 block  =                              8 pages =    2112 bytes =   16896 bits
       1 page   =                                            264 bytes =    2112 bits
   the pages 0 to 1293 are reserved for the bitstream


   code  : XC3S 1400AN

   ISF (Internal System Flash) has :

       1 ROM    = 16 sectors = 512 blocks = 4096 pages = 2162688 bytes =17301504 bits
       1 sector =               32 blocks =  256 pages =  135168 bytes =  540672 bits
       1 block  =                              8 pages =    4224 bytes =   33792 bits
       1 page   =                                            528 bytes =    4224 bits
   the pages 0 to 1125 are reserved for the bitstream

  (there is a less efficient mode where pages can be power of 2 addressed)




  the PLD implements a serialiser/deserialiser to the Xilinx FPGA SPI ACCESS internal block

  MOSI.DATA.b8 (= MOSI.CS) goes to the CSB of the SPI ACCESS block
  MOSI.DATA.b7..b0 are serialised to MOSI of the SPI ACCESS block

  the return data from MISO is deserialised into MISO.DATA.b7..b0
  the status line MISO.RDY mark with 1 that the PLD state machine is ready to serialise, this state
  machine is lunched when a word is written to MOSI.DATA

  The CSB (CS) line of the SPI ACCESS internal block must be high
  for one macro operation (erase, read, etc.) and must NOT go back to low between
  each access.

\*---------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------*/
uint16_t PldSPIfeedByteToController(uint8_t data)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends a word via SPI interface MOSI and gets another on MISO line.
  leaving MOSI.DATA.b8 = 1 (MOSI.CS = CSB = 1)
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t timeout = 10;                // 10us timeout
    uint16_t word;


    word = data | 0x0100; // set CSB bit;
    PLD_FLASH_SPI_MOSI_P = word; // set CSB bit

    // while interface is busy
    while (((PLD_FLASH_SPI_MISO_P & PLD_FLASH_SPI_MISO_RDY_MASK16) == 0)
           && (timeout-- != 0)
          )
    {
        USLEEP(2); // at least 1 us for sure, because the timer resolution 1 can be 0.999us to 1.000us
    }

    // If timeout expired or blank rsp
    if ((timeout == 0) || (PLD_FLASH_SPI_MISO_P == 0xFFFF))
    {
        return (SPI_ERROR_NO_RESPONSE);
    }

    return (PLD_FLASH_SPI_MISO_P);
}
/*---------------------------------------------------------------------------------------------------------*/
void PldSPIendTransaction(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function deactivates the CSB (CS) line of the SPI ACCESS internal block.
  To do this the state machine implemented in the PLD need to process a word so we write a dummy word
  to satisfy it.
\*---------------------------------------------------------------------------------------------------------*/
{
    PLD_FLASH_SPI_MOSI_P = 0x00; // MOSI.DATA.b8 = MOSI.CS = CSB = 0
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t PldSPIready(uint16_t timeout)
/*---------------------------------------------------------------------------------------------------------*\
  This function checks the PLD flash gets OK within the given timeout
\*---------------------------------------------------------------------------------------------------------*/
{
    PldSPIfeedByteToController(SPI_CMD_GET_STATUS);            // CMD status

    while ((PldSPIfeedByteToController(0x00) & SPI_STS_READY_MASK) !=
           SPI_STS_READY_MASK)      // Wait until flash is ready
    {
        USLEEP(1000); // 1ms

        if (timeout-- == 0)
        {
            PldSPIendTransaction();             // Release CSB
            return (SPI_ERROR_PROGRAMING_PROBLEM);                      // Timeout return
        }
    }

    PldSPIendTransaction();             // Release CSB
    return (SPI_STATE_OK);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t PldSPIeraseSector(uint8_t sector, spartan_version_t version)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases a sector of the PLD internal flash.
  Can take 2.5 to 5 sec.

  sectors 0 to 5 (pages 0 to 1293) are reserved for the bitstream

\*---------------------------------------------------------------------------------------------------------*/
{
    if (sector == 0)
    {
        // erase part A
        PldSPIfeedByteToController(SPI_CMD_ERASE_SECTOR);       // CMD, 0x7C

        // High address byte
        // bits [b23..b16] must be 0 for Sector0A, Sector0B
        PldSPIfeedByteToController(0x00);

        // Mid  address byte
        // bits [b15..b13] must be 0 and bit b12 must be 0 for Sector0A, bits [b11..b8] are don't care,
        PldSPIfeedByteToController(0x00);

        // Low  address byte, bits [b7..b0] are don't care
        PldSPIfeedByteToController(0x00);
        PldSPIendTransaction();                         // Release CSB

        // Wait for erase complete
        if (PldSPIready(3000) != SPI_STATE_OK)              // flash with a timeout of 3 seconds
        {
            return (SPI_ERROR_PROGRAMING_PROBLEM);              // Timeout error
        }


        // erase part B
        PldSPIfeedByteToController(SPI_CMD_ERASE_SECTOR);       // CMD, 0x7C

        // High address byte
        // bits [b23..b16] must be 0 for Sector0A, Sector0B
        PldSPIfeedByteToController(0x00);

        // Mid  address byte
        // bits [b15..b13] must be 0 and bit b12 must be 1 for Sector0B, bits [b11..b8] are don't care,
        PldSPIfeedByteToController(spartan_info[version].sector_0b_mid_addr);

        // Low  address byte, bits [b7..b0] are don't care
        PldSPIfeedByteToController(0x00);
        PldSPIendTransaction();                         // Release CSB

        // Wait for erase complete
        if (PldSPIready(3000) != SPI_STATE_OK)              // flash with a timeout of 3 seconds
        {
            return (SPI_ERROR_PROGRAMING_PROBLEM);              // Timeout error
        }
    }
    else
    {
        PldSPIfeedByteToController(SPI_CMD_ERASE_SECTOR);       // CMD, 0x7C

        // High address byte
        // bits [b23..b21] must be 0, bits [b20..b17] are the sector number, bit b16 is are don't care for sectors 1..15
        PldSPIfeedByteToController(sector << spartan_info[version].sector_shift);

        // Mid  address byte
        // bits [b15..b8] are don't care for sectors 1..15
        PldSPIfeedByteToController(0x00);

        // Low  address byte, bits [b7..b0] are don't care
        PldSPIfeedByteToController(0x00);
        PldSPIendTransaction();                         // Release CSB

        // Wait for erase complete
        if (PldSPIready(3000) != SPI_STATE_OK)              // flash with a timeout of 3 seconds
        {
            return (SPI_ERROR_PROGRAMING_PROBLEM);              // Timeout error
        }
    }

    return (SPI_STATE_OK);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t PldSPIeraseBlock(uint16_t block, spartan_version_t version)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases a block of the PLD internal flash (8 pages).
  Can take 32 to 35 ms.
\*---------------------------------------------------------------------------------------------------------*/
{
    // bits [b23..b21] must be 0
    // bits [b20..b9] are the page number
    // bits [b8..b0] are don't care

    PldSPIfeedByteToController(SPI_CMD_ERASE_BLOCK);     // CMD 0x50

    // High address byte
    // bits [b23..b21] must be 0, bits [b20..b16] are part of block number
    PldSPIfeedByteToController(block >> (8 - spartan_info[version].block_shift));

    // Mid  address byte
    // bits [b15..b12] are part of block number, bits [b11..b8] are don't care
    PldSPIfeedByteToController(block << spartan_info[version].block_shift);

    // Low  address byte, bits [b7..b0] are don't care
    PldSPIfeedByteToController(0x00);

    PldSPIendTransaction();                             // Release CSB

    // Wait for erase complete
    if (PldSPIready(50) != SPI_STATE_OK)                // with 50ms as timeout (for erase)
    {
        return (SPI_ERROR_PROGRAMING_PROBLEM);          // Timeout error
    }

    return (SPI_STATE_OK);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t PldSPIerasePage(uint16_t page, spartan_version_t version)
/*---------------------------------------------------------------------------------------------------------*\
  This function erases a page of the PLD internal flash.
  Can take 32 to 35 ms.
\*---------------------------------------------------------------------------------------------------------*/
{
    // bits [b23..b21] must be 0
    // bits [b20..b9] are the page number
    // bits [b8..b0] are don't care

    PldSPIfeedByteToController(SPI_CMD_ERASE_PAGE);     // CMD 0x81

    // High address byte
    // bits [b23..b21] must be 0, bits [b20..b16] are part of page number
    PldSPIfeedByteToController(page >> (8 - spartan_info[version].page_shift));

    // Mid  address byte
    // bits [b15..b9] are part of page number, bit b8 is don't care
    PldSPIfeedByteToController(page << spartan_info[version].page_shift);

    // Low  address byte, bits [b7..b0] are don't care
    PldSPIfeedByteToController(0x00);

    PldSPIendTransaction();                             // Release CSB

    // Wait for erase complete
    if (PldSPIready(50) != SPI_STATE_OK)                // with 50ms as timeout (for erase)
    {
        return (SPI_ERROR_PROGRAMING_PROBLEM);          // Timeout error
    }

    return (SPI_STATE_OK);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t PldSPIwritePage(bool erase_before, spartan_version_t version, uint16_t page, uint16_t bytes_to_write,
                       uint8_t const * data_p)
/*---------------------------------------------------------------------------------------------------------*\
  This function writes data in a page of the internal PLD flash
  with contiguous data form address data_p

  If erase_before is TRUE the command issued will erase the page before writing (but Erasing all sectors in
  one go is more efficient), otherwise it will assume the page has been erased before.

  If addr + bytes_to_write overwrite next page, the data exceeding the page size will not be written.

  Note: here we write complete pages starting from byte 0000 of the page

\*---------------------------------------------------------------------------------------------------------*/
{
    if (bytes_to_write > spartan_info[version].isf_page_size)   // ouch! we don't do out of the page
    {
        return (SPI_ERROR_PROGRAMING_PROBLEM);
    }

    // I guess is better to change the use of CMD 0x88 SPI_CMD_BUF1_TO_PAGE (later in the code) with CMD 0x83
    // instead of erasing the page here
    if (erase_before == TRUE)                           // erase page (35ms max)
    {
        if (PldSPIerasePage(page, version) != SPI_STATE_OK)
        {
            return (SPI_ERROR_PROGRAMING_PROBLEM);      // erase failed
        }
    }

    // Write SRAM buffer 1
    PldSPIfeedByteToController(SPI_CMD_WRITE_BUF1);     // CMD 0x84
    // the buffer only needs the byte address in the page
    // and we start at byte 0
    PldSPIfeedByteToController(0x00);                   // High address byte
    PldSPIfeedByteToController(0x00);                   // Mid  address byte
    PldSPIfeedByteToController(0x00);                   // Low  address byte

    while (bytes_to_write-- != 0)
    {
        PldSPIfeedByteToController(*data_p++);          // send data to the buffer
    }

    PldSPIendTransaction();                             // Release CSB



    // Write buffer1 to page without erase, it takes 4-6 ms
    PldSPIfeedByteToController(SPI_CMD_BUF1_TO_PAGE);   // CMD 0x88

    // High address byte
    // Contains more or less bits of the page address depending of page size (and thus spartan version)
    PldSPIfeedByteToController(page >> (8 - spartan_info[version].page_shift));

    // Mid  address byte
    // Contains more or less bits of the page address depending of page size (and thus spartan version)
    PldSPIfeedByteToController(page << spartan_info[version].page_shift);

    // Low  address byte, bits [b7..b0] are don't care
    PldSPIfeedByteToController(0x00);
    PldSPIendTransaction();                             // Release CSB

    // Wait for operation to complete

    if (PldSPIready(10) != SPI_STATE_OK)                // with 10ms as timeout for SRAM to page write
    {
        return (SPI_ERROR_PROGRAMING_PROBLEM);          // Timeout error
    }

    /*
        // verify what was programmed in the page against what is in the buffer1
        PldSPIfeedByteToController(SPI_CMD_VERIFY_PAGE_BUFFER1);   // CMD 0x60

        // If one or more bits differ between the page and buffer, then the Compare bit (bit 6 in the Status Register) is '1'


        // High address byte
        // bits [b23..b21] must be 0, bits [b20..b16] are part of page number
        PldSPIfeedByteToController(page >> 7);

        // Mid  address byte
        // bits [b15..b9] are part of page number, bit b8 is don't care
        PldSPIfeedByteToController(page << 1);

        // Low  address byte, bits [b7..b0] are don't care
        status = PldSPIfeedByteToController(0x00);
        PldSPIendTransaction();                             // Release CSB
    */

    return (SPI_STATE_OK);
}
/*---------------------------------------------------------------------------------------------------------*/
void PldSPIunlockAllMemory(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function unlocks all sectors by disabling the protect mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Disable sector protect
    PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_0);
    PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_1);
    PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_2);
    PldSPIfeedByteToController(SPI_CMD_PROTECTION_DISABLE_SEQ_3);
    PldSPIendTransaction();     // Release CSB

    pld_info.spi_state = SPI_STATE_UNLOCKED;    // update PLD spi_state as unlocked

    /*
        // Erase protect register
        PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_0);
        PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_1);
        PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_2);
        PldSPIfeedByteToController(SPI_CMD_PROTECTION_ERASE_SEQ_3);
        PldSPIendTransaction();     // Release CSB

        // Wait for 50 ms max (datasheet:35ms)
        if ( PldSPIready(50) != SPI_STATE_OK )      // with 50ms as timeout (for erase)
        {
            return(SPI_ERROR_PROGRAMING_PROBLEM);           // Timeout error
        }
    */
}
/*---------------------------------------------------------------------------------------------------------*/
void PldSPIenableProtection(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function unlocks all sectors by disabling the protect mode.
\*---------------------------------------------------------------------------------------------------------*/
{
    // Enable sector protect
    PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_0);
    PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_1);
    PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_2);
    PldSPIfeedByteToController(SPI_CMD_PROTECTION_ENABLE_SEQ_3);
    PldSPIendTransaction();     // Release CSB

    pld_info.spi_state = SPI_STATE_LOCKED;    // update PLD spi_state as unlocked

    /*
        // Write protect register

        idx = 0;
        PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_0);
        PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_1);
        PldSPIfeedByteToController(SPI_CMD_PROTECTION_ACCESS_SEQ_2);
        PldSPIfeedByteToController(SPI_CMD_PROTECT_WR3);
        // 0x00 not protected, 0xFF protected (sector 0 is a special case)
        PldSPIfeedByteToController(0x00); // Sector 0
        PldSPIfeedByteToController(0x00); // Sector 1
        PldSPIfeedByteToController(0x00); // Sector 2
        PldSPIfeedByteToController(0x00); // Sector 3
        PldSPIfeedByteToController(0x00); // Sector 4
        PldSPIfeedByteToController(0x00); // Sector 5
        PldSPIfeedByteToController(0x00); // Sector 6
        PldSPIfeedByteToController(0x00); // Sector 7
    //  ... up to 15
        PldSPIendTransaction();     // Release CSB

        // Wait for 10 ms max (datasheet:6ms)
        if ( PldSPIready(10) != SPI_STATE_OK )      // with 10ms as timeout for SRAM to page write
        {
            return(SPI_ERROR_PROGRAMING_PROBLEM);           // Timeout error
        }
    */
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t PldSPIread(uint32_t addr, uint8_t * data_p)
/*---------------------------------------------------------------------------------------------------------*\
  This function reads 1 byte in flash, starting at address addr.

  WARNING!!!
  when in normal mode (not power 2) for the XC3S 700AN - 4FG484C we read the 264 bytes page each 512 bytes alignment
  so we have 264 bytes of data and 248 bytes of garbage
\*---------------------------------------------------------------------------------------------------------*/
{
    // Direct Flash read

    // ToDO 1400??
    PldSPIfeedByteToController(SPI_CMD_RANDOM_READ);    // CMD
    PldSPIfeedByteToController((uint8_t)(addr >> 16));    // high addr
    PldSPIfeedByteToController((uint8_t)(addr >> 8));     // mid addr
    PldSPIfeedByteToController((uint8_t)addr);            // low addr

    *data_p = 0xFF & PldSPIfeedByteToController(0x00);  // no latency to get data in this mode

    PldSPIendTransaction();                             // Release CSB
    return (SPI_STATE_OK);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pld_SPI.c
\*---------------------------------------------------------------------------------------------------------*/
