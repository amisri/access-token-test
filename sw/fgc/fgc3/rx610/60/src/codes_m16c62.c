/*---------------------------------------------------------------------------------------------------------*\
  File:     codes_m16c62.c

  Purpose:  FGC3 Boot code update functions

\*---------------------------------------------------------------------------------------------------------*/

#define BIG_ENDIAN_OFFSET_TO_BYTE3 1

#include <codes_m16c62.h>

#include <codes.h>
#include <fgc_consts_gen.h>         // Global FGC constants
#include <m16c62.h>             // for C62PowerOn()
#include <menu.h>               // for MenuRspError(), menu global variable
#include <mem.h>
#include <stdio.h>              // for NULL, fprintf()
#include <string.h>
#include <term.h>               // for TERM_TX


/*
 * This function will select the M16C62 flash device.
 * The function returns: 0 = Read/Write  2 = Not visible
 */
static uint16_t  SelectM16C62Dev(enum fgc_code_ids_short_list dev_idx);


/* This function fills the erase_mask (block_mask_p)
 * @param block_mask_p Bit map identifying which blocks of the M16C62 flash memory are going to be affected by a erase/program command.
 * Example: block_mask_p = 05h = 00000101b -> blocks 0 and 2 are selected.
 * @return 0 for success, 1 not successful, 2 unknown device
 */
static uint8_t SetBlockMask(enum fgc_code_ids_short_list dev_idx, uint8_t* block_mask_p);



enum FGC_codes_status_internal WriteM16C62dev(enum fgc_code_ids_short_list dev_idx)
{
    struct fgc_code_info    code_info;
    uint32_t                  len;        // can be bigger than 64Kb
    const char       *      sram_buffer;
    uint16_t                  crc;
    uint32_t                  done;       // can be bigger than 64Kb


    // the code info is stored after the "usable" data, appended at the end, by Code Delivery Software
    // so with the len we can calculate were it starts

    len = (uint32_t) codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE; // Code length in bytes

    sram_buffer = (char *) SRAM_TMPCODEBUF_32;

    /*
      NB: this is not more true as we don't swap any more between LITTLE and BIG ENDIAN

       I do a local copy of the code_info instead of just pointing to the structure
       as I need to convert crc to little endian without affecting the buffer, which
       will be written to flash unmodified (and this remains very similar to CheckMcuDev and WriteMcuBootDev functions)
       and the CRC showed in the fprintf will be the correct one

       in previous version was used a pointer to code_info in the buffer and the calculated SRAM crc
       was converted to big endian but in case of mismatch the the showed with fprintf was mirrored
       (the bigendian printed as little endian)

    */

    // fill the code_info structure with the data we have in the buffer
    memcpy(&code_info, (sram_buffer + len - FGC_CODE_INFO_SIZE), FGC_CODE_INFO_SIZE);

    // calculate CRC of SRAM buffer
    crc = MemCrc16(sram_buffer, (len / 2) - 1);

    if (code_info.crc != crc)
    {
        fprintf(TERM_TX, RSP_DATA " CRC: expected 0x%04X calculated 0x%04X\n", code_info.crc, crc);
        fprintf(TERM_TX, RSP_ERROR ",Abandoned due to SRAM buffer corruption\n");
        return (CODES_STATUS_INVALID_CRC);
    }

    // now we take care of the M16C62

    // check if device is available
    if (SelectM16C62Dev(dev_idx))
    {
        c62.code_address = 0;
        return (CODES_STATUS_C62_INACCESSIBLE);
    }


    done = 0;

    while (len > 0)
    {
        memcpy(c62.code_buf, sram_buffer + done, 0x100);      // fill buffer

        c62.code_address = codes_holder_info[dev_idx].base_addr + done; // prepare destination address

        // C62FlashCode() needs
        // c62.code_address : destination address in M16C62 memory map (3 bytes)
        // c62.code_buf     : with the 256 bytes to transfer
        // and it will send the 3 commands needed C62_SET_ADDRESS, C62_SET_DATA, C62_PROG_FLASH
        // and clear c62.code_address at return

        if (C62FlashCode() != 0)
        {
            return (CODES_STATUS_WRITE_FAILED);
        }

        // prepare for next chunk
        done += 0x100;
        len  -= 0x100;
    }

    return (CODES_STATUS_SUCCESS);
}



enum FGC_codes_status_internal EraseC62Dev(enum fgc_code_ids_short_list dev_idx)
{
    uint8_t erase_mask;
    uint16_t cmd_status;

    if (SelectM16C62Dev(dev_idx) != 0)
    {
        return (CODES_STATUS_C62_INACCESSIBLE);
    }

    if (SetBlockMask(dev_idx, &erase_mask) != 0)
    {
    	return (CODES_STATUS_C62_DEVICE_INACCESSIBLE);
    }


    cmd_status = C62SendIsrCmd(C62_SET_BLOCK_MASK, 1, 0, &erase_mask);

    if (cmd_status != 0)
	{
    	fprintf(TERM_TX, RSP_ERROR ", SET_BLOCK_MASK failed with status 0x%04X\n", cmd_status);
	}

    cmd_status = C62SendBgCmd(C62_ERASE_FLASH, 3000);

    if ( cmd_status != 0)
    {
    	fprintf(TERM_TX, RSP_ERROR ", C62_ERASE_FLASH failed; returned value 0x%04X \n", cmd_status);

    	// Set dev state to DEV_FAILED
        codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;

        if (*menu.error_buf != 0)
        {
            fprintf(TERM_TX, RSP_ERROR ",%s:%s\n",
                    codes_holder_info[dev_idx].label, (char *)menu.error_buf);
            *menu.error_buf = '\0';
        }

        return (CODES_STATUS_ERASE_FAILED);
    }

    return (CODES_STATUS_SUCCESS);
}



enum FGC_codes_status_internal CheckC62Dev(enum fgc_code_ids_short_list dev_idx)
{
    uint32_t          flash_address;
    uint32_t          code_info_addr;
    uint32_t          len;
    uint16_t          stat;

    // Clear code info

    codes_holder_info[dev_idx].dev.class_id = 0;
    codes_holder_info[dev_idx].dev.code_id  = FGC_CODE_NULL;
    codes_holder_info[dev_idx].dev.version  = 0;
    codes_holder_info[dev_idx].dev.crc      = 0;
    codes_holder_info[dev_idx].calc_crc     = 0;

    if (SelectM16C62Dev(dev_idx) != 0)
    {
        return (CODES_STATUS_C62_INACCESSIBLE);
    }

    // the code info is stored after the "usable" data, appended at the end, by Code Delivery Software
    // so with the len we can calculate were it starts

    len = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE; // Code length in bytes

    flash_address  = codes_holder_info[dev_idx].base_addr;
    code_info_addr = flash_address + len - FGC_CODE_INFO_SIZE;

    c62.rsp_bytes_in_buffer = FGC_CODE_INFO_SIZE;

    stat = C62SendIsrCmd(C62_SET_ADDRESS, 3, BIG_ENDIAN_OFFSET_TO_BYTE3, (uint8_t *) &code_info_addr);

    if (stat == 0)                              // if OK
    {
        stat = C62GetRsp((uint8_t *) & (c62.rsp.bytes), (uint16_t *) & (c62.rsp_bytes_in_buffer));

        if (stat == 0)                          // if OK
        {
            if (c62.rsp_bytes_in_buffer != FGC_CODE_INFO_SIZE)
            {
                stat = 1;
            }
        }
    }

    if (stat != 0)                              // if not OK
    {
        codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;   // Set dev state to DEV_FAILED
        return (CODES_STATUS_C62_INFO_ERROR);
    }

    codes_holder_info[dev_idx].dev.class_id = c62.rsp.code_info.class_id;   // Copy code info to device structure
    codes_holder_info[dev_idx].dev.code_id  = c62.rsp.code_info.code_id;
    codes_holder_info[dev_idx].dev.version  = c62.rsp.code_info.version;
    codes_holder_info[dev_idx].dev.crc      = c62.rsp.code_info.crc;
    codes_holder_info[dev_idx].calc_crc     = c62.rsp.code_info.crc;        // Preset calculated CRC to info CRC


    stat = C62SendIsrCmd(C62_SET_ADDRESS, 3, BIG_ENDIAN_OFFSET_TO_BYTE3,
                         (uint8_t *) &flash_address); // set block address

    if (stat == 0)                              // if OK
    {

        // set of block lengths
        stat = C62SendIsrCmd(C62_SET_CRC_LEN, 3, BIG_ENDIAN_OFFSET_TO_BYTE3,
                             (uint8_t *) &len);

        if (stat == 0)                          // if OK
        {
            stat = C62SendBgCmd(C62_CALC_CRC, 1500);
            //          measures with FGC3
            //          crc for PTDB     52ms
            //          crc for IdProg  222ms
            //          crc for IDDB   1310ms
        }
    }


    if (stat != 0)
    {
        // by default, later can be CODE_STATE_CORRUPTED
        codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;

        if (stat == M16C62_CRC_FAILED)
        {
            codes_holder_info[dev_idx].calc_crc  = c62.crc_calc;
            codes_holder_info[dev_idx].dev_state = CODE_STATE_CORRUPTED;
        }

        return (CODES_STATUS_INVALID_CRC);
    }

    codes_holder_info[dev_idx].dev_state = CODE_STATE_VALID;
    return (CODES_STATUS_SUCCESS);
}



static uint16_t SelectM16C62Dev(enum fgc_code_ids_short_list dev_idx)
{

#ifndef DEBUG_M16C62_STANDALONE

    if ((C62_P & C62_STANDALONE_MASK16) != 0)   // If M16C62 is in standalone
    {
        codes_holder_info[dev_idx].dev_state = CODE_STATE_NOT_AVL;  // Set dev state to NOT AVAILABLE
        return (2);
    }

#endif


    // while debugging, we need the PowerOn(), not to put M16C62 on but to set correctly C62_SET_LITTLE_ENDIAN
#ifndef DEBUG_M16C62_STANDALONE

    if ((C62_P & C62_POWER_MASK16) == 0)    // If M16C62 is off
#endif
    {
        // when M16C62 boots if bus_initial_value is C62_SWITCH_PROG the M16C62 will remain in IdBoot
        // otherwise it will run IdProg which in turn will run the its monitor
        if (C62PowerOn(C62_SWITCH_PROG) != 0)   // If an attempt to start IdBoot fails
        {
            codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;   // Set dev state to DEV_FAILED

            if (*menu.error_buf != 0)                   // If an error was reported
            {
                fprintf(TERM_TX, RSP_ERROR ",%s:%s\n",              // Write error report
                        codes_holder_info[dev_idx].label, (char *) menu.error_buf);
                *menu.error_buf = '\0';                     // Clear error buffer
            }

            return (2);
        }
    }

    return (0);
}


static uint8_t SetBlockMask(enum fgc_code_ids_short_list dev_idx, uint8_t* block_mask_p)
{
	uint8_t block_mask;

	switch (dev_idx)
	{
		case SHORT_LIST_CODE_PTDB:
			block_mask  = C62_BLK_PT_DB;
			break;

		case SHORT_LIST_CODE_IDPROG:
			block_mask  = C62_BLK_ID_PROG;
			break;

		case SHORT_LIST_CODE_IDDB:
			block_mask  = C62_BLK_ID_DB;
			break;

		default:
			return (2);
	}


	if (block_mask_p == 0)
	{
		return 1;
	}

	*block_mask_p = block_mask;

	return 0;

}
