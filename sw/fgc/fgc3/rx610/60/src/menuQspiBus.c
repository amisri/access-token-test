/*---------------------------------------------------------------------------------------------------------*\
  File:     menuQspiBus.c

  Purpose:  Functions to run for the diagnostic(DIM card in the QSPI bus) menu options

  These menus belongs to the [Manual Test] part

\*---------------------------------------------------------------------------------------------------------*/

// Includes

#include <stdint.h>
#include <iodefines.h>
#include <stdio.h>
#include <bitmap.h>
#include <dev.h>
#include <diag_boot.h>
#include <mcuDependent.h>
#include <memmap_mcu.h>
#include <menu.h>
#include <sleep.h>
#include <term.h>

// Constants

#define QSPI_A_CLK      0x0001  // In QSM_CTRL, branch A, 0x04080701:4 corresponds to CLK
#define QSPI_A_DIN      0x0020  // In QSM_CTRL, branch A, 0x04080701:5 corresponds to DIN
#define QSPI_A_DOUT     0x0040  // In QSM_CTRL, branch A, 0x04080701:6 corresponds to DOUT
#define QSPI_A_RBDOUT   0x0080  // In QSM_CTRL, branch A, 0x04080701:7 corresponds to RBDOUT
#define QSPI_B_CLK      0x1000  // In QSM_CTRL, branch B, 0x04080700:4 corresponds to CLK
#define QSPI_B_DIN      0x2000  // In QSM_CTRL, branch B, 0x04080700:5 corresponds to DIN
#define QSPI_B_DOUT     0x4000  // In QSM_CTRL, branch B, 0x04080700:6 corresponds to DOUT
#define QSPI_B_RBDOUT   0x8000  // In QSM_CTRL, branch B, 0x04080700:7 corresponds to RBDOUT



void MenuMTqspiShow(uint16_t argc, char ** argv)
{
    uint16_t  qspi_branch = 0;    // 0=A, 1=B
    uint16_t  dim_board;
    uint16_t  dim_register;

    if (argc != 0)
    {
        MenuGetInt16U(argv[0], 0, 1, &qspi_branch);
    }

    diag_bus.state    = DIAG_RESET;
    diag_bus.n_resets = 0;
    MSLEEP(40);

    // Show all data for selected branch - 7 channels, 16 DIMs
    // reception_buffer[2][7][16]    // [A,B][registers] for [16 DIM cards]
    //     the 7 registers are stored in this order
    //     0 - Dig0    DIM register ID:0
    //     1 - Dig1    DIM register ID:1
    //     2 - Ana0    DIM register ID:4
    //     3 - Ana1    DIM register ID:5
    //     4 - Ana2    DIM register ID:6
    //     5 - Ana3    DIM register ID:7
    //     6 - TrigCounter DIM register ID:2
    //                          there is no ID:3

    for (dim_board = 0; dim_board < 16; dim_board++)
    {
        fprintf(term.f, RSP_DATA);

        for (dim_register = 0; dim_register < 7; dim_register++)
        {
            fprintf(term.f, ", 0x%04X ", diag_bus.reception_buffer[qspi_branch][dim_register][dim_board]);
        }

        fprintf(term.f, "\n");
    }

    fprintf(term.f, RSP_DATA "\n");
}


void MenuMTqspiReset(uint16_t argc, char ** argv)
{
    diag_bus.n_resets = 0;

    QSM_CTRL_P |= (QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16);
    MSLEEP(1);
    QSM_CTRL_P &= ~(QSM_CTRL_RSTA_MASK16 | QSM_CTRL_RSTB_MASK16);

}



void MenuMTqspiGenerateVSReset(uint16_t argc, char ** argv)
{
    DIG_OP_P = (DIG_OP_P | DIG_OP_SET_VSRESETCMD_MASK16) & ~DIG_OP_RST_VSRESETCMD_MASK16;
    MSLEEP(10);
    DIG_OP_P = (DIG_OP_P | DIG_OP_RST_VSRESETCMD_MASK16) & ~DIG_OP_SET_VSRESETCMD_MASK16;
}



void MenuSTqspiLoop(uint16_t argc, char ** argv)
{
    uint16_t diag_state_old;

    if (FGC_CRATE_TYPE_RECEPTION != dev.crate_type)
    {
        MenuRspError("Not implemented on this backplane type:0x%04X", dev.crate_type);
        return;
    }
    else
    {
        diag_state_old = diag_bus.state;
        diag_bus.state = DIAG_OFF;

        // Clear reset/loopback/invert/read

        clrBitmap(QSM_CTRL_P, QSM_CTRL_RSTA_MASK16 | QSM_CTRL_LOOPA_MASK16 | QSM_CTRL_INVA_MASK16 | QSM_CTRL_READA_MASK16 |
                              QSM_CTRL_RSTB_MASK16 | QSM_CTRL_LOOPB_MASK16 | QSM_CTRL_INVB_MASK16 | QSM_CTRL_READB_MASK16);

        MSLEEP(2);

        // Bus A

        // Test Din, Clk
        // reset forces clk at 1

        setBitmap(QSM_CTRL_P, QSM_CTRL_RSTA_MASK16);
        USLEEP(10);

        if (   testBitmap(QSM_CTRL_P, (QSPI_A_CLK  | QSPI_A_DIN   )) == false
            || testBitmap(QSM_CTRL_P, (QSPI_A_DOUT | QSPI_A_RBDOUT)) == true)
        {
            MenuRspError("Set RST A bit failed. Expected QSM_CTRL_P: 0xC031, got: 0x%X", QSM_CTRL_P);
            return;
        }

        // Test DOUT/RBDOUT
        // standby, clock at 0

        clrBitmap(QSM_CTRL_P, QSM_CTRL_RSTA_MASK16);
        USLEEP(10);

        if (   testBitmap(QSM_CTRL_P, (QSPI_A_DOUT | QSPI_A_RBDOUT)) == false
            || testBitmap(QSM_CTRL_P, (QSPI_A_CLK  | QSPI_A_DIN   )) == true)
        {
            MenuRspError("Reset RST A bit failed. Expected QSM_CTRL_P: 0xC0C0, got: 0x%X", QSM_CTRL_P);
            return;
        }


        // Bus B

        // Test Din, Clk
        // reset forces clk at 1

        setBitmap(QSM_CTRL_P, QSM_CTRL_RSTB_MASK16);
        USLEEP(10);

        if (   testBitmap(QSM_CTRL_P, (QSPI_B_CLK  | QSPI_B_DIN))    == false
            || testBitmap(QSM_CTRL_P, (QSPI_B_DOUT | QSPI_B_RBDOUT)) == true)
        {
            MenuRspError("Set RST B bit failed. Expected QSM_CTRL_P: 0x31C0, got: 0x%X", QSM_CTRL_P);
            return;
        }

        // Test DOUT/RBDOUT
        // standby, clock at 0

        clrBitmap(QSM_CTRL_P, QSM_CTRL_RSTB_MASK16);
        USLEEP(10);

        if (   testBitmap(QSM_CTRL_P, (QSPI_B_DOUT | QSPI_B_RBDOUT)) == false
            || testBitmap(QSM_CTRL_P, (QSPI_B_CLK  | QSPI_B_DIN))    == true)
        {
            MenuRspError("Reset RST B bit failed. Expected QSM_CTRL_P: 0xC0C0, got: 0x%X", QSM_CTRL_P);
            return;
        }

        diag_bus.state = diag_state_old;
    }
}

// EOF
