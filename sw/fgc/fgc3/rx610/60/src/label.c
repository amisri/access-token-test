/*---------------------------------------------------------------------------------------------------------*\
 File:      label.c

 Purpose:   FGC

\*---------------------------------------------------------------------------------------------------------*/

#define LABEL_GLOBALS   // to instantiate warnings_lbl[], faults_lbl[], fgc3_type_lbl[], net_type_lbl[], reset_source_lbl[], st_latched_lbl[]

#include <label.h>
#include <stdio.h>      // for sprintf()
#include <menu.h>       // for MenuRspArg()

/*---------------------------------------------------------------------------------------------------------*/
void BitLabel(const struct sym_name * types, uint16_t bit_mask)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search the type list and will generate a response argument for each set bit in bit_mask.
\*---------------------------------------------------------------------------------------------------------*/
{
    while (types->label != 0)
    {
        if ((types->type & bit_mask) != 0)
        {
            MenuRspArg(types->label);
        }

        types++;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
char * TypeLabel(const struct sym_name const * types, uint16_t type)
/*---------------------------------------------------------------------------------------------------------*\
  This function will search for the type in the type list and will return a pointer the associated label,
  or if the type is unknown, it will return a string saying "[type_hex]"
\*---------------------------------------------------------------------------------------------------------*/
{
    static char buf[16];

    while (types->label != 0)
    {
        if (types->type == type)
        {
            return (types->label);
        }

        types++;
    }

    sprintf(buf, "[0x%04X]", type);
    return ((char *) buf);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: label.c
\*---------------------------------------------------------------------------------------------------------*/
