/*!
 *  @file      dsp_loader.c
 *  @brief     DSP boot loader. 
 */

#define DSP_LOADER_GLOBALS



// Includes

#include <stdint.h>
#include <stdbool.h>

#include <dsp_loader.h>
#include <memmap_mcu.h>
#include <sleep.h>
#include <dsp_60_boot_loader.h>
#include <structs_bits_big.h>
#include <common.h>

#if FGC_CLASS_ID == 60
#include <stdio.h>
#include <term.h>
#include <boot_dual_port_ram.h>
#endif



// Constants

/*! Timeout for the DSP to acknowldge a handshake: 10 ms * 100 = 1 s. */
#define  DSP_BOOT_TIMEOUT               300



// Internal function declarations

/*!
 * Wait for the DSP during 1 second to acknoweldge the handshake.
 *
 * @param  expectedValue Expected value returned by the DSP.
 * @retval True          The DSP acknowledge the handshake.
 * @retval False         The handshake timed out.
 */
static bool dspHandshakeWait(uint32_t const expectedValue);



// External function definitions

enum dsp_error dspLoadImage(uint8_t const * dsp_image, uint32_t const dsp_image_length)
{
    /*
     * Once GPIODAT1 is activated and the DSP is using 32bits access
     * to DPRAM this procedure does not work.
     *
     * The DSP boot reads 16 bits from each address. In the boot loader code
     * the 32 bit access to the DPRAM is activated. For the DSP boot loader
     * only the low 16 bits can be used.
     *
     * The DSP is little endian, whilst the MCU is big endian. The FPGA handles
     * the endian conversion for 32 bit words.
     */

    // If the stand-alone switch is asserted, assume that the DSP is running.

    if ((CPU_MODEL_P & CPU_MODEL_DSPSTAL_MASK8) != 0)
    {
        dsp.is_running = true;
        return (DSP_ERROR_OK);
     }

    dsp.is_running = false;

   // Put the DSP in reset state

    CPU_RESET_P |= CPU_RESET_CTRL_DSP_MASK16;

    // First, transfer the image of the DSP bootloader.

    union TUnion32Bits    data;
    volatile uint32_t   * dpram_p;
    uint32_t              idx;

    // At this point the DPRAM access is 16 bit wide only. The lower
    // half-word is hardcoded with the vlaue 0xFFFF.

    data.byte[1] = 0xFF;
    data.byte[0] = 0xFF;

    dpram_p = (uint32_t *) DPRAM_START_32;

    for (idx = 0; idx < DSP_60_BOOT_LOADER_CODE_SIZE; )
    {
        data.byte[3] = dsp_60_boot_loader_code[idx++];
        data.byte[2] = dsp_60_boot_loader_code[idx++];

        *dpram_p++ = data.int32u;
    }

    // Release the DSP reset

    CPU_RESET_P &= ~CPU_RESET_CTRL_DSP_MASK16;

    if (dspHandshakeWait(DSP_LOADER_DSP_ACK) == false)
    {
        return (DSP_ERROR_PLL_UNSTABLE);
    }

    /*
     * Transfer the DSP BOOT or MAIN image, whose format is as follows:
     *
     * EEEEEEEE
     * DATA SECION 1
     * AAAAAAAA,LLLLLLLL,DDDDDDDD,DDDDDDDD,
     * DDDDDDDD,DDDDDDDD,DDDDDDDD,DDDDDDDD,
     * DDDDDDDD,DDDDDDDD,DDDDDDDD,DDDDDDDD,
     * ...
     * DATA SECION 2
     * AAAAAAAA,LLLLLLLL,DDDDDDDD,DDDDDDDD,
     * DDDDDDDD,DDDDDDDD,DDDDDDDD,DDDDDDDD,
     * DDDDDDDD,DDDDDDDD,DDDDDDDD,DDDDDDDD,
     * ...
     * DATA SECION n
     *
     * Where:
     * EEEEEEEE: entry address of the DSP propgram
     * AAAAAAAA: section start address
     * LLLLLLLL: section length
     * DDDDDDDD: section data
     */

    union TUnion32Bits  entry_point;
    union TUnion32Bits  section_address;
    union TUnion32Bits  section_length;
    uint32_t            block_length;
    uint32_t            image_length = dsp_image_length;

    // Store the DSP BOOT or MAIN entry point address

    entry_point.byte[3] = *dsp_image++;
    entry_point.byte[2] = *dsp_image++;
    entry_point.byte[1] = *dsp_image++;
    entry_point.byte[0] = *dsp_image++;

    image_length -= 4;

    // Transfer the image

    while (image_length > 0)
    {
        section_address.byte[3] = *dsp_image++;
        section_address.byte[2] = *dsp_image++;
        section_address.byte[1] = *dsp_image++;
        section_address.byte[0] = *dsp_image++;

        section_length.byte[3] = *dsp_image++;
        section_length.byte[2] = *dsp_image++;
        section_length.byte[1] = *dsp_image++;
        section_length.byte[0] = *dsp_image++;

        // Decrement the section length and the spece taken by the section address and size

        image_length -= section_length.int32u + 4 + 4;

        while (section_length.int32u > 0)
        {
            dpram_p = (uint32_t *)(DPRAM_START_32);

            // Reserve the first word for the handshake (DPRAM_HANDSHAKE_A)

            dpram_p++;

             // Copy the destination

            *dpram_p++ = section_address.int32u;

            // The available space in DPRAM is: 16 kb * 1024 = 16384 - 12 = 16372 bytes
            // The 12 bytes account for the [0]handshake(4) [1]destination(4) [2]size(4)

            block_length = (section_length.int32u > 16372 ? 16372 : section_length.int32u);

            section_length.int32u  -= block_length;
            section_address.int32u += block_length;

            *dpram_p++ = block_length;

            while (block_length > 0)
            {
                data.byte[3] = *dsp_image++;
                data.byte[2] = *dsp_image++;
                data.byte[1] = *dsp_image++;
                data.byte[0] = *dsp_image++;

                *dpram_p++ = data.int32u;

                block_length -= 4;
            }

            // Signal the DSP to retrieve the block data

            *DPRAM_HANDSHAKE_A = DSP_LOADER_MCU_REQUEST;

            if (dspHandshakeWait(DSP_LOADER_DSP_ACK) == false)
            {
                return (DSP_ERROR_CODE_BLOCK_TIMEOUT);
            }
        }
    }

    // After transferring the DSP data, send the entry point of the DSP MAIN
    // This address is signaled with a block size of 0 bytes.

    dpram_p = (uint32_t *)(DPRAM_START_32);

    // Reserve the first word for the handshake (DPRAM_HANDSHAKE_A)

    dpram_p++;

    *dpram_p++ = entry_point.int32u;
    *dpram_p++ = 0;

    // Signal the DSP to retrieve the entry point

    *DPRAM_HANDSHAKE_A = DSP_LOADER_MCU_REQUEST;

    if (dspHandshakeWait(DSP_LOADER_RUNNING) == false)
    {
        fprintf(term.f, RSP_DATA ",Dsp running error %08x\n", (unsigned int)dpram->handshake);

        return (DSP_ERROR_NOT_RUNNING_TIMEOUT);
    }

    // Give time to the DSP to boot and write its vesrion in the DPRAM

    sleepMs(1000);

    dsp.is_running = true;
    fprintf(term.f, RSP_DATA ",DSP version %lu\n", dpram->dsp.version);

    return (DSP_ERROR_OK);
}



char const *  dspLoadErrorStr(enum dsp_error error)
{
    switch (error)
    {
        case DSP_ERROR_OK:                  return ("Ok");                  break;
        case DSP_ERROR_PLL_UNSTABLE:        return ("PLL Unstable");        break;
        case DSP_ERROR_CODE_BLOCK_TIMEOUT:  return ("Code block timeout");  break;
        case DSP_ERROR_ENTRY_POINT_TIMEOUT: return ("Entry point timeout"); break;
        case DSP_ERROR_NOT_RUNNING_TIMEOUT: return ("Not running");         break;
        case DSP_ERROR_STANDALONE:          return ("Standalone");          break;
        default:                            return ("Invalid");             break;
    }
}



// Internal function definitions

static bool dspHandshakeWait(uint32_t const expectedValue)

{
    uint16_t  timeout_counter = 0;

    sleepMs(10);

    // The first byte in DPRAM is used for the handshake between the MCU and DSP

    while (  *DPRAM_HANDSHAKE_A != expectedValue
           && ++timeout_counter < DSP_BOOT_TIMEOUT)
    {
        sleepMs(10);
    }

    return (timeout_counter < DSP_BOOT_TIMEOUT);
}


// EOF
