/*!
 *  @file     codes_pld.c
 *  @defgroup FGC3:MCU boot
 *  @brief    FPGA programming function
 *
 */

// Includes

#include <codes_pld.h>
#include <iodefines.h>
#include <stdio.h>
#include <codes.h>
#include <dev.h>
#include <defconst.h>
#include <hardware_setup.h>
#include <led.h>
#include <mcuDependent.h>
#include <mem.h>
#include <memmap_mcu.h>
#include <pld_spi.h>
#include <pld.h>
#include <term.h>

// Internal function declarations

void CodesPLDSelect(void)
{
    uint16_t selected_code_id = 0;

    // Select PLD code depending on the FGC3 revision (rev 1 -> 700, rev 2 -> 1400)

    if (dev.pld_hw_model == XC3S700AN)
    {
        codes_holder_info[SHORT_LIST_CODE_PLD].len_blks  = FGC_CODE_60_PLD_7_SIZE_BLKS;
        selected_code_id = FGC_CODE_60_PLD_7;
    }
    else if (dev.pld_hw_model == XC3S1400AN)
    {
        codes_holder_info[SHORT_LIST_CODE_PLD].len_blks  = FGC_CODE_60_PLD_14_SIZE_BLKS;
        selected_code_id = FGC_CODE_60_PLD_14;
    }
    else
    {
        pld_info.ok = FALSE;
    }

    codes_holder_info[SHORT_LIST_CODE_PLD].update.code_id = selected_code_id;
    codes_holder_info[SHORT_LIST_CODE_PLD].dev.code_id  = selected_code_id;
}

enum FGC_codes_status_internal CheckPldDev(enum fgc_code_ids_short_list dev_idx)
{
    // We can't access PLD flash freely, that's why code info is stored in non-volatile storage.
    // Copy version and CRC from NVS to code info structure.

    codes_holder_info[dev_idx].dev.class_id = 60;
    codes_holder_info[dev_idx].dev.version  = NVRAM_PLD_VERSION_P;
    codes_holder_info[dev_idx].dev.crc      = NVRAM_PLD_CRC_P;
    codes_holder_info[dev_idx].calc_crc     = NVRAM_PLD_CRC_P;

    if(!pld_info.ok ||
       codes_holder_info[dev_idx].dev.version == 0 ||
       codes_holder_info[dev_idx].dev.crc     == 0)
    {
        codes_holder_info[dev_idx].dev_state = CODE_STATE_CORRUPTED;

        return CODES_STATUS_CORRUPTED_PLD;
    }
    else
    {
        codes_holder_info[dev_idx].dev_state = CODE_STATE_VALID;

        shared_mem.codes_version[SHORT_LIST_CODE_PLD] = codes_holder_info[dev_idx].dev.version;

        return CODES_STATUS_SUCCESS;
    }
}

enum FGC_codes_status_internal ErasePldDev(enum fgc_code_ids_short_list dev_idx)
{
    // we can't erase until we fill the PLD with the bitstream, so this is done at the end in WritePldDev()
    return (CODES_STATUS_SUCCESS);
}

enum FGC_codes_status_internal WritePldDev(enum fgc_code_ids_short_list dev_idx)
{
    const struct fgc_code_info * code_info;
    uint32_t len;
    uint16_t crc;
    const uint8_t * bitstream_p;
    uint16_t isf_page;
    uint32_t ii;

    NVRAM_UNLOCK();
    NVRAM_PLD_VERSION_P = 0;
    NVRAM_PLD_CRC_P     = 0;
    NVRAM_LOCK();

    // the code info is stored after the "usable" data, appended at the end, by Code Delivery Software
    // so with the len we can calculate were it starts

    len = (uint32_t) codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE;     // Code length in bytes
    code_info = (struct fgc_code_info *)(SRAM_TMPCODEBUF_32 + len - FGC_CODE_INFO_SIZE);

    // calculate CRC of SRAM buffer
    crc = MemCrc16((void *) SRAM_TMPCODEBUF_32, (len / sizeof(uint16_t)) - 1);

    if (code_info->crc != crc)
    {
        fprintf(term.f, RSP_DATA " CRC: expected 0x%04X calculated 0x%04X\n", code_info->crc, crc);
        fprintf(term.f, RSP_ERROR ",Abandoned due to SRAM buffer corruption\n");
        return (CODES_STATUS_INVALID_CRC);
    }

    // Disable network (todo why?) and use locally generated ms tick in case of PLD crash

    DisableNetworkInterrupt();
    Disable1msTickInterrupt();
    Enable1msTickInterruptFromMcuTimer();

    // boot in slave serial to download the bitstream

    if (PldBootSlaveSerial())
    {
        // here the PLD is ready to receive data

        // The host then continues supplying data and clock signals until either the DONE pin goes
        // High, indicating a successful configuration, or until the INIT_B pin goes Low, indicating a
        // configuration error. The configuration process requires more clock cycles than indicated
        // from the configuration file size. Additional clocks are required during the start-up of the FPGA
        // sequence, especially if the FPGA is programmed to wait for selected Digital Clock
        // Managers (DCMs) to lock to their respective clock inputs

        PldFeedBitstreamViaSlaveSerial((uint8_t *) codes_holder_info[dev_idx].base_addr,
                                       spartan_info[dev.pld_hw_model].bistream_size);
    }
    else
    {
        fprintf(term.f, RSP_ERROR ",PLD failed to boot in slave serial mode\n");

        return (CODES_STATUS_PLD_INIT_ERROR);
    }

    // Go back to internal SPI configuration mode, for safety
    // by default M2=0, so [M2 M1 M0]=011 --> FPGA boot from internal SPI
    P9.DR.BIT.B4  = 0;      // back to default mode ( FPGA boot from internal SPI )
    P9.DDR.BIT.B4 = 0;      // P9.4, FPGA_M2, back as input for safety

    if(PldWaitForDONEandReadModel() != PLD_SUCCESS)
    {
        fprintf(term.f, RSP_ERROR ",PLD timeout\n");

        return CODES_STATUS_PLD_TIMEOUT;
    }

    // the PLD is up and running the new code at this point

    // Set again BLEDS to signify code update in progress

    BLED_RST(PIC_BLUE);
    BLED_RST(PSU_BLUE);
    BLED_SET(DCCT_BLUE);
    BLED_SET(FGC_BLUE);

    // PLD_STATE_INIT_AND_DONE_ARE_LOW is the OK condition
    if (pld_info.state != PLD_STATE_INIT_AND_DONE_ARE_HI)
    {
        fprintf(term.f, RSP_ERROR ",PLD FLS config error\n");

        return (CODES_STATUS_PLD_CONFIG_ERROR);
    }
    else
    {
        fprintf(term.f, RSP_DATA ",PLD flash update\n");
    }


    // Erase Xilinx internal SPI Flash sectors used for the bitstream

    PldSPIunlockAllMemory();    // just in case

    if ((PldSPIeraseSector(0, dev.pld_hw_model) != SPI_STATE_OK)
        || (PldSPIeraseSector(1, dev.pld_hw_model) != SPI_STATE_OK)
        || (PldSPIeraseSector(2, dev.pld_hw_model) != SPI_STATE_OK)
        || (PldSPIeraseSector(3, dev.pld_hw_model) != SPI_STATE_OK)
        || (PldSPIeraseSector(4, dev.pld_hw_model) != SPI_STATE_OK)
        || (PldSPIeraseSector(5, dev.pld_hw_model) != SPI_STATE_OK)   // ToDo 700 only
       )
    {
        return (CODES_STATUS_ERASE_FAILED);
    }

    // Write page by page the code in flash
    // for the PLD the received "code" size depends on the model
    // but the size we need to download as bitstream is BITSTREAM_SIZE (the original real size)
    // and the size we need to write to the xilinx ISF is ISF_SIZE (to have
    // a size multiple of the 264/528 bytes page size)

    bitstream_p = (uint8_t *)  codes_holder_info[dev_idx].base_addr;
    isf_page = 0; // sectors 0 to 5/4, pages 0 to 1293/1125, are the ones used for the bitstream for 700/1400
    ii = 0;

    while (ii < spartan_info[dev.pld_hw_model].isf_size)
    {
        if (PldSPIwritePage(FALSE, dev.pld_hw_model, isf_page, spartan_info[dev.pld_hw_model].isf_page_size,
                            (bitstream_p + ii)) != SPI_STATE_OK)
        {
            fprintf(term.f, RSP_ERROR ",PLD flash write fail (page 0x%04X)", isf_page);

            return (CODES_STATUS_WRITE_FAILED);
        }

        ii += spartan_info[dev.pld_hw_model].isf_page_size;
        isf_page++;
    }

    // Reboot PLD with the firmware just written in flash

    if(PldBootISF() != PLD_SUCCESS)
    {
        EnableNetworkInterrupt();
        fprintf(term.f, RSP_ERROR ",PLD Boot ISF Timeout\n");

        return (CODES_STATUS_PLD_TIMEOUT);
    }
    else
    {
        // PLD flashing succeeded !

        // Store PLD code version and CRC in non-volatile storage

        NVRAM_UNLOCK();
        NVRAM_PLD_VERSION_P = code_info->version;
        NVRAM_PLD_CRC_P     = code_info->crc;
        NVRAM_LOCK();

        // Perform a global reset so that FPGA is re-initialised properly
        // Note: the FPGA has just been reprogrammed and will still assert default Power cycle
        // The shared memory will be zeroed and the STAY_IN_BOOT flag will be lost

        CPU_RESET_CTRL_P |= CPU_RESET_CTRL_GLOBAL_MASK16;    // Trigger global reset
    }

    return (CODES_STATUS_SUCCESS);
}

//EOF

