/*---------------------------------------------------------------------------------------------------------*\
  File:         menuSciVBus.c

  Purpose:      Digital interface functions

  Author:       hugo.lebreton@cern.ch

  Notes:
                These menus belongs to the [Manual Test] part and [Self Test] part
\*---------------------------------------------------------------------------------------------------------*/

#include <float.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <menu.h>               // for MenuGetInt16U(), MenuRspArg(), MenuRspError()
#include <errno.h>
#include <main.h>               // for main_misc
#include <mem.h>                // for memset
#include <memmap_mcu.h>         // for SCIVS_*
#include <regfgc3.h>
#include <scivs.h>
#include <time.h>
#include <time_fgc.h>
#include <sleep.h>


// Internal variables
static struct Regfgc3_slice_params slice_params;

// Internal functions declaration


static bool scivsGMCShowCardInfo(uint16_t slot);

static bool scivsShowWBInfo(uint16_t slot_idx);

static void scivsPrintError(enum Scivs_error_code err_code);

static enum Scivs_error_code scivsSliceGetParameters(const uint16_t slot, const uint16_t device, struct Regfgc3_slice_params* s_params);

static enum Scivs_error_code scivsSliceReadBack(const uint16_t slot, const uint16_t device, const uint16_t slice_number);

// static enum Scivs_error_code scivsSliceEnableChangeDb(const uint16_t slot, const uint16_t device);

static enum Scivs_error_code scivsSliceProgram(const uint16_t slot, const uint16_t device, const uint16_t slice_number, const uint16_t time_out);

static enum Scivs_error_code scivsSliceCalculateCrc(const uint16_t slot, const uint16_t device, uint16_t * crc);

static enum Scivs_error_code scivsSliceTransferSingle(const uint16_t slot,
                                                 const uint16_t slice_number,
                                                 const uint16_t slice_number_bytes,
                                                 const uint16_t slice_number_batches);


static enum Scivs_error_code scivsTaskAbort(const uint16_t slot);


// External functions definitions


// Option 0: Read/Write
// Option 0-0: Set slot number
void MenuScivsSetSlotNumber(uint16_t argc, char ** argv)
{
    uint16_t    slot;

    if (MenuGetInt16U(argv[0], 0, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }

    SCIVS_M_SLAVE_SLOT_P = slot;
}



// Option 0-1: Access settings
void MenuScivsSetReadWrite(uint16_t argc, char ** argv)
{
    uint16_t    waddr;
    uint16_t    wlen;
    uint16_t    raddr;
    uint16_t    rlen;

    if (MenuGetInt16U(argv[0], 0, 0xFFFF, &waddr) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[1], 0, SCIVS_MASTER_FRAME_W, &wlen) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[2], 0, 0xFFFF, &raddr) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[3], 0, SCIVS_MASTER_FRAME_W, &rlen) != 0)
    {
        return;
    }

    SCIVS_M_LENGTH_P = 0;
    SCIVS_M_LENGTH_P = wlen << 8;
    SCIVS_M_WADD_P = waddr;
    SCIVS_M_LENGTH_P |= rlen;
    SCIVS_M_RADD_P = raddr;
}



// Option 0-2: Set data
void MenuScivsSetData(uint16_t argc, char ** argv)
{
    uint16_t    idx;
    uint16_t    data;

    if (MenuGetInt16U(argv[0], 0, SCIVS_MASTER_FRAME_W - 1, &idx) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[1], 0, 0xFFFF, &data) != 0)
    {
        return;
    }

    scivs_tx->w[idx] = data;
}


// Option 0-3: Display write block
void MenuScivsReadBackBlock(uint16_t argc, char ** argv)
{
    uint8_t     ii;

    MenuRspArg("Status: 0x%04X\n", SCIVS_M_STATUS_P);
    MenuRspArg("Slot %d; WAddr 0x%04X; WLength: %u; RAddr 0x%04X; RLength: %u",
               SCIVS_M_SLAVE_SLOT_P & (FGC_SCIVS_N_SLOT - 1), SCIVS_M_WADD_P, SCIVS_M_LENGTH_P >> 8, SCIVS_M_RADD_P,
               SCIVS_M_LENGTH_P & 0xFF);

    MenuRspArg("Data:");

    for (ii = 0; ii < SCIVS_MASTER_FRAME_W; ii++)
    {
        MenuRspArg("%d", scivs_tx->w[ii]);
    }

    MenuRspArg("\n");

    for (ii = 0; ii < SCIVS_MASTER_FRAME_W; ii++)
    {
        MenuRspArg("0x%04X", scivs_tx->w[ii]);
    }
}



// Option 0-4: Send block
void MenuScivsSendBlock(uint16_t argc, char ** argv)
{

    if (main_misc.is_sci_test_continuous)
    {
        MenuRspError("Error: sending in continuous mode");
        return;
    }

    SCIVS_S_STATUS_P = 0x0000; // Clear slave status

    if (SCIVS_M_COMMAND_P & (SCIVS_M_COMMAND_CLK_HIGH_MASK16 | SCIVS_M_COMMAND_CLK_LOW_MASK16))
    {
        return;
    }

    if (SCIVS_M_STATUS_P & SCIVS_M_STATUS_BUSY_MASK16)
    {
        return;
    }

    SCIVS_M_STATUS_P  = 0x0000;                     // clear status register
    SCIVS_M_COMMAND_P = SCIVS_M_COMMAND_GO_MASK16;  // Send GO
}



// Option 0-5: Display Received header
void MenuScivsReadHeader(uint16_t argc, char ** argv)
{
    MenuRspArg("Status 0x%04X; WLength: %d; RLength: %d",
        SCIVS_S_STATUS_P,
        SCIVS_S_LENGTH_P >> 8,
        SCIVS_S_LENGTH_P & 0xFF);
}



// Option 0-6: Display all received data
void MenuScivsReadAllData(uint16_t argc, char ** argv)
{
    uint8_t     ii;

    for (ii = 0; ii < SCIVS_MASTER_FRAME_W; ii++)
    {
        MenuRspArg("0x%04X", scivs_rx->w[ii]);
    }
}

// Option 0-7: Display received data
void MenuScivsReadData(uint16_t argc, char ** argv)
{
    uint16_t    idx;

    if (MenuGetInt16U(argv[0], 0, SCIVS_MASTER_FRAME_W - 1, &idx) != 0)
    {
        return;
    }

    MenuRspArg("%d", scivs_rx->w[idx]);
    MenuRspArg("0x%04X", scivs_rx->w[idx]);
}



// Option 0-8: Set/Get extra
// Option 0-8-0: Set data float
void MenuScivsSetDataFloat(uint16_t argc, char ** argv)
{
    uint16_t    idx;
    float        data;

    if (MenuGetInt16U(argv[0], 0, SCIVS_MASTER_FRAME_W - 1, &idx) != 0)
    {
        return;
    }

    if (MenuGetFloat(argv[1], -FLT_MAX, FLT_MAX, &data) != 0)
    {
        return;
    }

    *(&(scivs_tx->w[idx])) = data;
    fprintf(term.f, RSP_DATA ", Write %f as float 0x%08X\n", (double) data, *(&(scivs_tx->w[idx])));
}

// Option 0-8-1: Set data int
void MenuScivsSetDataInt(uint16_t argc, char ** argv)
{
    uint16_t    idx;
    int32_t     data;

    if (MenuGetInt16U(argv[0], 0, SCIVS_MASTER_FRAME_W - 1, &idx) != 0)
    {
        return;
    }

    if (MenuGetInt32S(argv[2], INT32_MIN, INT32_MAX, &data) != 0)
    {
        return;
    }

    switch (argv[1][0])
    {
        case 'v':
            scivs_tx->w[idx] = (uint16_t) data;
            fprintf(term.f, RSP_DATA ", Write %d as uint_16 0x%04X\n", (int) data, scivs_tx->w[idx]);
            break;

        case 'w':
            scivs_tx->w[idx] = (int16_t) data;
            fprintf(term.f, RSP_DATA ", Write %d as int_16 0x%04X\n", (int) data, scivs_tx->w[idx]);
            break;

        case 'u':
            idx /= 2;
            scivs_tx->dw[idx] = (uint32_t) data;
            fprintf(term.f, RSP_DATA ", Write %d as uint_32 0x%08X\n", (int) data, (unsigned int)scivs_tx->dw[idx]);
            break;

        case 's':
            idx /= 2;
            scivs_tx->dw[idx] = (int32_t) data;
            fprintf(term.f, RSP_DATA ", Write %d as int_32 0x%08X\n", (int) data, (unsigned int)scivs_tx->dw[idx]);
            break;

        default:
            MenuRspError("Wrong type specifier\n");
            break;

    }
}

// Option 0-8-2: Get data of given type
void MenuScivsGetDataOfGivenType(uint16_t argc, char ** argv)
{
    uint16_t    idx;

    if (MenuGetInt16U(argv[0], 0, SCIVS_MASTER_FRAME_W - 1, &idx) != 0)
    {
        return;
    }

    switch (argv[1][0])
    {
        case 'v':
            MenuRspArg("%u", scivs_rx->w[idx]);
            MenuRspArg("0x%04X", scivs_rx->w[idx]);
            break;

        case 'w':
            MenuRspArg("%d", scivs_rx->w[idx]);
            MenuRspArg("0x%04X", scivs_rx->w[idx]);
            break;

        case 'u':
            idx /= 2;
            MenuRspArg("%lu", scivs_tx->dw[idx]);
            MenuRspArg("0x%08X", scivs_tx->dw[idx]);
            break;

        case 's':
            idx /= 2;
            MenuRspArg("%ld", (int32_t)scivs_tx->dw[idx]);
            MenuRspArg("0x%08X", scivs_tx->dw[idx]);
            break;

        case 'f':
            idx /= 2;
            MenuRspArg("%f", (float)scivs_tx->dw[idx]);
            MenuRspArg("0x%08X", scivs_tx->dw[idx]);
            break;

        default:
            MenuRspError("Wrong type specifier\n");
            break;

    }
}


// Option 1: Show wishbone config
void MenuScivsShowWBInfo(uint16_t argc, char ** argv)
{
    uint16_t            slot;

    if (MenuGetInt16U(argv[0], 0, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }

    scivsShowWBInfo(slot);
}



// Option 2: Display RegFGC3 card information

void MenuScivsGMCShowCardInfo(uint16_t argc, char ** argv)
{
    uint16_t    slot_idx;

    if (MenuGetInt16U(argv[0], 0, FGC_SCIVS_N_SLOT - 1, &slot_idx) != 0)
    {
        return;
    }

    scivsGMCShowCardInfo(slot_idx);

}
// Option 3: SCIVS crate reset
//
void MenuScivsReset(uint16_t argc, char ** argv)
{
    scivsInit();
}


// Option 4: Extra settings
// Option 4-0: Mode line

void MenuScivsModeLine(uint16_t argc, char ** argv)
{
    uint16_t    mode;

    if (MenuGetInt16U(argv[0], 0, 1, &mode) != 0)
    {
        return;
    }

    if (mode == 0)
    {
        SCIVS_M_COMMAND_P = 0x0004;     // CLK low
    }
    else
    {
        SCIVS_M_COMMAND_P = 0x0000;     // CLK in free run, enable SCVS bus
    }
}


// Option 4-1: Operation mode

void MenuScivsOpMode(uint16_t argc, char ** argv)
{
    uint16_t    mode;

    if (MenuGetInt16U(argv[0], 0, 1, &mode) != 0)
    {
        return;
    }

    main_misc.is_sci_test_continuous = mode;
}



// Option 4-2: Show extra settings
void MenuScivsShowExtraSettings(uint16_t argc, char ** argv)
{
    char    mode_line_str[4][11] = {"Enable", "Enable", "Reset High", "Reset Low"};
    char    operation_mode_str[2][12] = {"Single shot", "Continuous"};

    MenuRspArg("Mode line %s, Operation mode %s"
               , mode_line_str[SCIVS_M_COMMAND_P], operation_mode_str[main_misc.is_sci_test_continuous]);
}

// Option 4-3: Generate VS reset
// Not used


void MenuSTSciVSBusGMCTest(uint16_t argc, char ** argv)
{
    uint16_t    slot;
    uint16_t    packet_idx;
    uint32_t    begin_time, elapsed_time;

    if (MenuGetInt16U(argv[0], 0, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }

    // loopback test ToDo MemSetDWords to check endianness!!

    MemSetWords((void *)scivs_tx->dw, 0x1234, sizeof(scivs_tx->dw));

    begin_time = timeGetUs();

    if (!scivsTaskSend(SCIVS_TASK_LOOPBACK, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS))
    {
        return;
    }

    elapsed_time = timeGetUsDiffNow(begin_time);

    // Check we received what we sent

    for (packet_idx = 0; packet_idx < (sizeof(scivs_tx->dw) / sizeof(scivs_tx->dw[0])); packet_idx++)
    {
        if (scivs_tx->dw[packet_idx] != 0x12341234)
        {
            MenuRspError("Unexpected loopback data at index %d, exp 0x%X, got 0x%X\n", packet_idx, 0x12341234,
                         scivs_tx->dw[packet_idx]);
            return;
        }
    }

    fprintf(term.f, RSP_DATA "Loopback test OK: %lu us elapsed\n", elapsed_time);

    // Send a operation code which does not exist

    begin_time = timeGetUs();
    if (!scivsTaskSend(SCIVS_TASK_INVALID_OP, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS))
    {
        // Do not return, the function returns false as the operation code does not exist. This is what we want to test.
    }

    elapsed_time = timeGetUsDiffNow(begin_time);
    // Check we received the expected error code

    if (scivs_rx->regfgc3.header.error != SCIVS_TSK_ERR_INVALID_OP)
    {
        MenuRspError("Unexpected error code, exp 0x%X, got 0x%X.\n", SCIVS_TSK_ERR_INVALID_OP,
                     scivs_rx->regfgc3.header.error, elapsed_time);
        return;
    }
    else
    {
        fprintf(term.f, RSP_DATA "Undefined operation code, test OK. %lu us elapsed\n", elapsed_time);
    }

}


// Option 5: Programming functions
// Option 5-0: Switch boot
void MenuScivsProgSwitchBoot(uint16_t argc, char ** argv)
{
    uint16_t boot_type;
    uint16_t slot;

    enum Scivs_error_code err_code;

    if ( MenuGetInt16U(argv[0], 1, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }


    if ( MenuGetInt16U(argv[1], 0, 1, &boot_type) != 0)
    {
        return;
    }

    // Word 1 is not used for this task anyways
    scivs_tx->w[1] = 0;
    scivs_tx->w[2] = boot_type;
    err_code = scivsTaskSend(SCIVS_TASK_SWITCH_BOOT, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);

    if (err_code == SCIVS_TSK_ERR_INVALID_OP)
    {
        MenuRspError("Card with no reprogramming capabilities, error code: 0x%04X \n", err_code);
        return;
    }

    // Switching to PB or DB does not return a packet to the FGC3, which will timeout
    if (err_code != SCIVS_ERR_MASTER_BUSY)
    {
        MenuRspError("Unexpected error from SCIVS transaction: 0x%04X \n", err_code);
        return;
    }

}

// Option 5-1: Slice Action Program
void MenuScivsProgSliceActionProgram(uint16_t argc, char ** argv)
{
    uint16_t slot;
    uint16_t device;
    uint16_t slice_number;
    enum Scivs_error_code err_code;

    if (MenuGetInt16U(argv[0], 1, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[1], 0, FGC_SCIVS_N_DEVICES, &device) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[2], 0, 65535, &slice_number) != 0)
    {
        return;
    }

    err_code = scivsSliceProgram(slot, device, slice_number, slice_params.time_program);

    if (err_code != SCIVS_OK)
    {
        MenuRspError("Unexpected error executing task: 0x%04X \n", err_code);
        return;
    }

}

// Option 5-2: Slice Action ReadBack
void MenuScivsProgSliceActionReadBack(uint16_t argc, char ** argv)
{
    uint16_t slot;
    uint16_t device;
    uint16_t slice_number;
    enum Scivs_error_code err_code;

    if (MenuGetInt16U(argv[0], 1, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[1], 0, FGC_SCIVS_N_DEVICES, &device) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[2], 0, 65535, &slice_number) != 0)
    {
        return;
    }

    err_code = scivsSliceReadBack(slot, device, slice_number);

    if (err_code != SCIVS_OK)
    {
        MenuRspError("Unexpected error executing task: 0x%04X \n", err_code);
        return;
    }

}

// Option 5-3: Slice Action Get Slice Parameters
void MenuScivsProgSliceActionGetSlicePars(uint16_t argc, char ** argv)
{
    uint16_t slot;
    uint16_t device;
    enum Scivs_error_code err_code;

    if (MenuGetInt16U(argv[0], 1, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[1], 0, FGC_SCIVS_N_DEVICES, &device) != 0)
    {
        return;
    }

    err_code = scivsSliceGetParameters(slot, device, &slice_params);

    fprintf(term.f, RSP_DATA "\n"\
    "slice n_bytes: %d\n; \
    slice n_batches: %d\n; \
    time slice prog: %d\n; \
    time readback: %d\n; \
    time parameters: %d\n; \
    time crc: %d\n;",
    slice_params.number_bytes,
    slice_params.number_batches,
    slice_params.time_program,
    slice_params.time_readback,
    slice_params.time_parameters,
    slice_params.time_crc
    );

    if (err_code != SCIVS_OK)
    {
        MenuRspError("Unexpected error executing task: 0x%04X \n", err_code);
        return;
    }

}

// Option 5-4: Slice Action Calculate CRC
void MenuScivsProgSliceActionCalcCRC(uint16_t argc, char ** argv)
{
    uint16_t slot;
    uint16_t device;
    uint16_t crc;
    enum Scivs_error_code err_code;

    if (MenuGetInt16U(argv[0], 1, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }

    if (MenuGetInt16U(argv[1], 0, FGC_SCIVS_N_DEVICES, &device) != 0)
    {
        return;
    }

    err_code = scivsSliceCalculateCrc(slot, device, &crc);

    if (err_code != SCIVS_OK)
    {
        MenuRspError("Unexpected error executing task: 0x%04X \n", err_code);
        return;
    }

    fprintf(term.f, RSP_DATA "CRC: 0x%04X\n", crc);
}


// Option 5-5: Constants
void MenuScivsProgConstantsExt(uint16_t argc, char ** argv)
{
    uint16_t            slot;
    enum Scivs_error_code err_code                        = SCIVS_OK;
    enum Scivs_task_code task_code                      = SCIVS_TASK_GENERAL_CONST_EXT;
    struct Regfgc3_general_constants_ext *gen_const_ext = NULL;
    uint8_t i;

    if ( MenuGetInt16U(argv[0], 1, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }

    gen_const_ext = (struct Regfgc3_general_constants_ext*) scivs_rx->regfgc3.data;
    err_code = scivsTaskSend(task_code, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);

    if (err_code != SCIVS_OK)
    {
        MenuRspError("Unexpected error executing task: 0x%04X \n", err_code);
        return;
    }

    fprintf(term.f, RSP_DATA \
        "installed devices: %d;\n \
        board_type: %d;\n \
        hw_execution: %d;\n \
        mezzanine_id: 0x%016llX\n\
           ",
        gen_const_ext->installed_devices,
        gen_const_ext->board_type,
        gen_const_ext->hw_execution,
        gen_const_ext->mezzanine_id
    );

    struct Regfgc3_device_data_ext * data_ext = (struct Regfgc3_device_data_ext *) &(gen_const_ext->device_0);
    for (i = 0; i <= gen_const_ext->installed_devices; i++)
    {
        if (data_ext == NULL)
        {
            break;
        }

        fprintf(term.f,
            RSP_DATA "device number: %d;\n" \
            "type: %d;\n" \
            "variant: %d\n" \
            "revision: %d\n" \
            "api_version: %d\n",
            i,
            data_ext->dev_data.device_type,
            data_ext->dev_data.variant,
            data_ext->dev_data.variant_revision,
            data_ext->dev_data.api_revision
            );

        data_ext++;
    }

}

// Option 5-6: send slice
void MenuScivsProgSendSlice(uint16_t argc, char ** argv)
{

    uint16_t slot;
    uint16_t device;
    uint16_t slice_number;

    // Will transfer and progam only the first slice
    enum Scivs_error_code err_code;

    if ( MenuGetInt16U(argv[0], 1, FGC_SCIVS_N_SLOT - 1, &slot) != 0)
    {
        return;
    }

    if ( MenuGetInt16U(argv[1], 1, FGC_SCIVS_N_DEVICES, &device) != 0)
    {
        return;
    }

    if ( MenuGetInt16U(argv[2], 0, 65535, &slice_number) != 0)
    {
        return;
    }

    err_code = scivsSliceTransferSingle(slot, slice_number, slice_params.number_bytes, slice_params.number_batches);
    if (err_code != SCIVS_OK)
    {
        MenuRspError("Unexpected error executing single slice transfer: 0x%04X \n", err_code);
        return;
    }

    err_code = scivsSliceProgram(slot, device, slice_number, slice_params.time_program);
    if (err_code != SCIVS_OK)
    {
        MenuRspError("Unexpected error executing slice program after transfer: 0x%04X \n", err_code);
        return;
    }
}


// Internal functions

static bool scivsShowWBInfo(uint16_t slot_idx)
{
    enum Scivs_error_code    err_code;
    uint32_t            begin_time;

    begin_time = timeGetUs();

    // Read wishbone info
    err_code = scivsWriteCustom(slot_idx, 0x0000, 0, 0x0000, 0x1A);

    scivsPrintError(err_code);

    fprintf(term.f, RSP_DATA ",Elapsed time is %lu us\n", timeGetUsDiffNow(begin_time));

    fprintf(term.f, RSP_DATA ",Received status from slot %d\n",
            (SCIVS_S_STATUS_P & SCIVS_S_STATUS_SLOT_NUM_MASK16));

    fprintf(term.f, RSP_DATA ",BOARD : address %d of type %d\n", scivs_rx->wishbone_info.board_address,
            scivs_rx->wishbone_info.board_type);

    fprintf(term.f, RSP_DATA ",TIME  : Rx delay %d, User time %d\n", scivs_rx->wishbone_info.rx_delay,
            scivs_rx->wishbone_info.user_time);

    fprintf(term.f, RSP_DATA ",STATUS: 0x%X, CRC 0x%X\n", scivs_rx->wishbone_info.status,
            scivs_rx->wishbone_info.crc);

    fprintf(term.f, RSP_DATA ",ACCESS: Wadd 0x%X Wlen 0x%X, Radd 0x%X Rlen 0x%X\n", scivs_rx->wishbone_info.wadd,
            (scivs_rx->wishbone_info.length & 0xFF00) >> 8, scivs_rx->wishbone_info.radd,
            scivs_rx->wishbone_info.length & 0xFF);


    return TRUE;
}


static bool scivsGMCShowCardInfo(uint16_t slot_idx)
{
    uint32_t begin_time, elapsed_time;
    enum Scivs_error_code scivs_err;

    const struct Regfgc3_general_constants * const general_constant = (struct Regfgc3_general_constants *)
            scivs_rx->regfgc3.data;

    begin_time = timeGetUs();
    scivs_err = scivsTaskSend(SCIVS_TASK_GENERAL_CONST, slot_idx, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);
    elapsed_time = timeGetUsDiffNow(begin_time);

    if (scivs_err != SCIVS_OK)
    {
        fprintf(term.f, RSP_DATA ",Skip slot %d: %lu us elapsed\n", slot_idx, elapsed_time);
        return FALSE;
    }

    fprintf(term.f, RSP_DATA ",Read slot %d: %lu us elapsed\n", slot_idx, elapsed_time);

    // Print what we received
    fprintf(term.f, RSP_DATA ",option_flags                   0x%04X\n"  , general_constant->option_flags);
    fprintf(term.f, RSP_DATA ",mc_program_revision            %d\n"      , general_constant->mc_program_revision);
    fprintf(term.f, RSP_DATA ",fpga_board_type                %d\n"      , general_constant->fpga_board_type);
    fprintf(term.f, RSP_DATA ",fpga_program_variant           %d\n"      , general_constant->fpga_program_variant);
    fprintf(term.f, RSP_DATA ",fpga_program_variant_revision  %d\n"      , general_constant->fpga_program_variant_revision);
    fprintf(term.f, RSP_DATA ",fpga_fgc3cvm_variant_id        %d\n"      , general_constant->fpga_fgc3cvm_variant_id);
    fprintf(term.f, RSP_DATA ",fpga_fgc3cvm_api_revision      %d\n"      , general_constant->fpga_fgc3cvm_api_revision);
    fprintf(term.f, RSP_DATA ",fpga_base_module_revision      %d\n"      , general_constant->fpga_base_module_revision);
    fprintf(term.f, RSP_DATA ",fpga_common_module_revision    %d\n"      , general_constant->fpga_common_module_revision);
    fprintf(term.f, RSP_DATA ",dsp_program_variant            %d\n"      , general_constant->dsp_program_variant);
    fprintf(term.f, RSP_DATA ",dsp_program_variant_revision   %d\n"      , general_constant->dsp_program_variant_revision);
    fprintf(term.f, RSP_DATA ",dsp_basemodule_api_revision    %d\n"      , general_constant->dsp_basemodule_api_revision);
    fprintf(term.f, RSP_DATA ",hardware_execution             %d\n"      , general_constant->hardware_execution);
    fprintf(term.f, RSP_DATA ",mezzanine_id                   0x%016llX\n", general_constant->mezzanine_id);
    return TRUE;
}



static enum Scivs_error_code scivsSliceGetParameters(const uint16_t slot, const uint16_t device, struct Regfgc3_slice_params* s_pars)
{
    enum Scivs_error_code err_code;
    struct Regfgc3_slice_params* slice_pars_local = (struct Regfgc3_slice_params *) scivs_rx->regfgc3.data;

    scivs_tx->w[1] = device;
    scivs_tx->w[2] = SCIVS_TASK_SLICE_ACTION_GETPARS;
    err_code = scivsTaskSend(SCIVS_TASK_SLICE_ACTION, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);

    if (err_code != SCIVS_OK)
    {
        return err_code;
    }
    memcpy((void *) s_pars, (const void *) slice_pars_local, sizeof(struct Regfgc3_slice_params));

    return SCIVS_OK;
}


static enum Scivs_error_code scivsSliceReadBack(const uint16_t slot, const uint16_t device, const uint16_t slice_number)
{
    enum Scivs_error_code err_code;
    uint16_t binary_word;
    uint16_t write_address;
    uint16_t read_address;
    uint8_t i;
    uint8_t j;

    scivs_tx->w[1] = device;
    scivs_tx->w[2] = SCIVS_TASK_SLICE_ACTION_READBACK;
    scivs_tx->w[3] = slice_number;

    err_code = scivsTaskSend(SCIVS_TASK_SLICE_ACTION, slot, SCIVS_MF, slice_params.time_readback);

    if (err_code != SCIVS_OK)
    {
        return err_code;
    }

    for (i = 0; i < slice_params.number_batches; i++)
    {
        // set addresses for reading
        write_address = SCIVS_SLICE_WRITE + 2 * i * SCIVS_MASTER_FRAME_W;
        read_address  = SCIVS_SLICE_WRITE + 2 * i * SCIVS_MASTER_FRAME_W;
        err_code = scivsWriteCustom(slot, write_address, SCIVS_NO_WRITE, read_address, SCIVS_MASTER_FRAME_W);

        if (err_code != SCIVS_OK)
        {
            return err_code;
        }

        // Build payload of 64 16-bit words
        for (j = 0; j < SCIVS_MASTER_FRAME_W; j++)
        {
            binary_word = scivs_rx->w[j];
            fprintf(term.f, RSP_DATA "Batch: %d, address: 0x%04X, word: 0x%04X\n", i, read_address, binary_word);
        }

        sleepMs(2);
    }

    return SCIVS_OK;
}


static enum Scivs_error_code scivsSliceTransferSingle(const uint16_t slot,
                                                 const uint16_t slice_number,
                                                 const uint16_t slice_number_bytes,
                                                 const uint16_t slice_number_batches)
{
    uint8_t i;
    uint8_t j;
    uint16_t write_address;
    uint16_t read_address;
    enum Scivs_error_code err_code;

    uint16_t binary_word = 0x0000;
    uint16_t shuffled_word;

    for (i = 0; i < slice_number_batches; i++)
    {
        // Build payload of 64 16-bit words
        for (j = 0; j < SCIVS_MASTER_FRAME_W; j++)
        {
            shuffled_word = ((binary_word & 0xFF00) >> 8) | ((binary_word & 0x00FF) << 8);
            scivs_tx->w[j] = shuffled_word;
            binary_word += 0x0002;
        }

        // send batch
        write_address = SCIVS_SLICE_WRITE + 2 * i * SCIVS_MASTER_FRAME_W;
        read_address  = SCIVS_SLICE_WRITE + 2 * i * SCIVS_MASTER_FRAME_W;
        err_code = scivsWriteCustom(slot, write_address, SCIVS_MASTER_FRAME_W, read_address, SCIVS_MASTER_FRAME_W);

        if (err_code != SCIVS_OK)
        {
            return err_code;
        }
    }

    return SCIVS_OK;
}




static enum Scivs_error_code scivsSliceProgram(const uint16_t slot, const uint16_t device, const uint16_t slice_number, const uint16_t time_out)
{

    enum Scivs_error_code err_code;

    scivs_tx->w[1] = device;
    scivs_tx->w[2] = SCIVS_TASK_SLICE_ACTION_PROGRAM;
    scivs_tx->w[3] = slice_number;

    err_code = scivsTaskSend(SCIVS_TASK_SLICE_ACTION, slot, SCIVS_MF, time_out);

    if (err_code != SCIVS_OK)
    {
        MenuRspError("Unexpected error from SCIVS transaction (task program): 0x%04X \n", err_code);
        fprintf(term.f, RSP_DATA "Sending abort task...\n");
        err_code = scivsTaskAbort(slot);

        if (err_code != SCIVS_OK)
        {
            MenuRspError("Unexpected error from SCIVS transaction (task abort): 0x%04X \n", err_code);
        }
    }

    return err_code;
}



static enum Scivs_error_code scivsSliceCalculateCrc(const uint16_t slot, const uint16_t device, uint16_t* crc)
{
    enum Scivs_error_code err_code    = SCIVS_OK;
    uint16_t crc_timeout            = slice_params.time_crc;

    // Prepare TX buffer
    scivs_tx->w[1] = device;
    scivs_tx->w[2] = SCIVS_TASK_SLICE_ACTION_CALCCRC;

    err_code = scivsTaskSend(SCIVS_TASK_SLICE_ACTION, slot, SCIVS_MF, crc_timeout);

    if (err_code != SCIVS_OK)
    {
        return err_code;
    }

    *crc = scivs_rx->w[2];

    return SCIVS_OK;
}


static enum Scivs_error_code scivsTaskAbort(const uint16_t slot)
{
    enum Scivs_error_code err_code;

    // This task does not need the device
    err_code = scivsTaskSend(SCIVS_TASK_ABORT, slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS);

    return err_code;
}



static void scivsPrintError(enum Scivs_error_code err_code)
{

    switch (err_code)
    {
        case SCIVS_OK:
            break;

        case SCIVS_TSK_ERR_OVERFLOW:
            MenuRspError("new OP while processing previous\n");
            break; // A new OP has been received before the last being finished. Note that the OP ABORT does not generate this error.

        case SCIVS_TSK_ERR_INVALID_OP:
            MenuRspError("OP not supported by this board\n");
            break; // OP not defined for the addressed board

        case SCIVS_TSK_ERR_WRONG_N_ELS:
            MenuRspError("incorrect number of elements\n");
            break; // incorrect Number of Elements (N_Elements) in packet

        case SCIVS_TSK_ERR_WRONG_BLK_IDX:
            MenuRspError("incorrect data block number\n");
            break; // incorrect Data Block number in packet

        case SCIVS_TSK_ENDED_BY_ABORT:
            MenuRspError("Task ended by abort task \n");
            break;

        case SCIVS_TSK_ABORT_WHEN_IDLE:
            MenuRspError("OP Abort received while Idle\n");
            break; // No OP under process when OP Abort has been received

        case SCIVS_ERR_WRONG_PARAM_IDX:
            MenuRspError("incorrect parameter index\n");
            break;

        case SCIVS_ERR_SET_NOT_PERM:
            MenuRspError("Set not permitted\n");
            break;

        case SCIVS_ERR_CLK_IN_RST:
            // Master clock in reset
            fprintf(term.f, RSP_DATA ",Wishbone write error: master clk in reset\n");
            break;

        case SCIVS_ERR_MASTER_BUSY:
            // Master status: not ready
            fprintf(term.f, RSP_DATA ",Wishbone write error: master not ready\n");
            break;

        case SCIVS_ERR_MASTER_TRANSFER_IN_PROGRESS:
            // Master status: transfer in progress
            fprintf(term.f, RSP_DATA ",Master error: transfer in progress. Master status 0x%X\n", SCIVS_M_STATUS_P);
            break;

        case SCIVS_ERR_MASTER_BAD_CRC:                    // Master error: Wrong CRC
            fprintf(term.f, RSP_DATA ",Master error: Bad CRC. Master status 0x%X\n", SCIVS_M_STATUS_P);
            break;

        case SCIVS_ERR_MASTER_TIMEOUT:                    // Master error: timeout on master wait
            fprintf(term.f, RSP_DATA ",Master error: timeout. Master status 0x%X\n", SCIVS_M_STATUS_P);
            break;

        case SCIVS_ERR_MASTER_ABORT:                      // Master status: Abort during reception
            fprintf(term.f, RSP_DATA ",Master error: timeout. Master status 0x%X\n", SCIVS_M_STATUS_P);
            break;

        case SCIVS_ERR_SLAVE_SLOT_ERR:                    // Slave error: invalid slot received (reported by first slave addressed after error)
            fprintf(term.f, RSP_DATA ",Slave error: slot error. Slave status 0x%X\n", SCIVS_S_STATUS_P);
            break;

        case SCIVS_ERR_SLAVE_CRC_ERR:                     // Slave error: CRC error (but valid slot address)
            fprintf(term.f, RSP_DATA ",Slave error: CRC error. Slave status 0x%X\n", SCIVS_S_STATUS_P);
            break;

        case SCIVS_ERR_SLAVE_ABORT:                       // Slave error: Abort received (reception of 18 stop bits or an early start bit)
            fprintf(term.f, RSP_DATA ",Slave error: Abort. Slave status 0x%X\n", SCIVS_S_STATUS_P);
            break;

        case SCIVS_ERR_SLAVE_WRITE_ERR:                   // Slave error: Error during wishbone write
            fprintf(term.f, RSP_DATA ",Slave error: error during write. Slave status 0x%X\n", SCIVS_S_STATUS_P);
            break;

        case SCIVS_ERR_SLAVE_READ_ERR:                    // Slave error: Error during wishbone read
            fprintf(term.f, RSP_DATA ",Slave error: error during read. Slave status 0x%X\n", SCIVS_S_STATUS_P);
            break;

        case SCIVS_ERR_SLAVE_BOARD_RST:                   // Slave info: First status after reset
            fprintf(term.f, RSP_DATA ",Slave info: out of reset. Slave status 0x%X\n", SCIVS_S_STATUS_P);
            break;

        case SCIVS_ERR_SLAVE_TIMEOUT:                     // Slave error: User time window expired
            fprintf(term.f, RSP_DATA ",Slave error: user timeout. Slave status 0x%X\n", SCIVS_S_STATUS_P);
            break;

        case SCIVS_ERR_SLAVE_WISHBONE_RESET:              // Slave info: a wishbone reset has been received
            fprintf(term.f, RSP_DATA ",Slave info: wishbone reset. Slave status 0x%X\n", SCIVS_S_STATUS_P);
            break;

        case SCIVS_ERR_SLAVE_BAD_SLOT:                    // Slave error: status received by unexpected slave
            fprintf(term.f, RSP_DATA ",Slave error: slot error. Slave status 0x%X\n", SCIVS_S_STATUS_P);
            break;

        case SCIVS_TSK_ERR_BAD_COUNT_AND_OPERATION:     // Slave error: unexpected count and operation
            fprintf(term.f, RSP_DATA ",REGFGC3 error: unexpected count and operation.\n");
            break;

        case SCIVS_ERR_BAD_ARG:
            fprintf(term.f, RSP_DATA ", Bad argument passed as input to SCIVS function.\n");
            break;

        case SCIVS_TSK_ERR_BLK_ACCESS_DENIED:
            MenuRspError("data block is protected\n");
            break; // Access to this Data Block is not granted, the block can be protected during certain conditions, i.e. converter ON

        case SCIVS_TSK_ERR_BLK_NOT_INSTALLED:
            MenuRspError("data block is not installed\n");
            break; // Data Block is not installed

        case SCIVS_TSK_ERR_EXT_TASK_TIMOUT:
            MenuRspError("external task timeout\n");
            break; // Some External Task(s) did not finish on time, request later. Some values can be erroneous

        case SCIVS_TSK_ERR_NO_DSP:
            MenuRspError("DSP not installed\n");
            break; // DSP is not installed in the board.

        case SCIVS_TSK_ERR_FLASHING_DSP:
            MenuRspError("DSP update disabled\n");
            break; // Hardware setting disables updating DSP program in FPGA Flash from FGC3.

        case SCIVS_TSK_ERR_LOADING_DSP:
            MenuRspError("DSP download disabled\n");
            break; // Hardware setting disables downloading of program from FPGA Flash to DSP.

        case SCIVS_TSK_ERR_DSP_LOAD_FAILED:
            MenuRspError("DSP load failed\n");
            break; // Download from FPGA Flash to DSP ended by Time-out, DSP-OK not detected

        case SCIVS_TSK_ERR_DATA_LIMIT:
            MenuRspError("data batch out of limits\n");
            break; // Data Batch out of limits

        case SCIVS_TSK_ERR_EXEC_IN_PROGRESS:
            MenuRspError("OP under process\n");
            break; // OP under process

        case SCIVS_TSK_ERR_INTERNAL:
            MenuRspError("internal rrror\n");
            break; // Internal Error

        case SCIVS_TSK_ERR_ACTION_NOT_RECOGNIZED:
        	MenuRspError("Action not recognized\n");
        	break;

        case SCIVS_TSK_ERR_DSP_PREV_PARAM_NOT_READ:
			MenuRspError("DSP previous parameter not read\n");
			break;

        case SCIVS_TSK_ERR_SLICE_INDEX_OVER_MAX:
			MenuRspError("Slice index greater than maximum slice index\n");
			break;

        case SCIVS_TSK_ERR_PRODBOOT_NOT_STORED:
			MenuRspError("Production boot not stored\n");
			break;

        case SCIVS_TSK_ERR_DESTINATION_UNKNOWN:
			MenuRspError("Task destination unknown (other than FPGA or DSP)\n");
			break;

        case SCIVS_TSK_ERR_DB_NOT_ENABLED:
            MenuRspError("Download boot programming not enabled\n");
            break;
    }

}
