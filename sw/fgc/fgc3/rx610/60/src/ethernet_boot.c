/*---------------------------------------------------------------------------------------------------------*\
  File:     ethernet.c

  Purpose:  FGC boot - Ethernet lan9221 Functions to process the different services.

        Feb 2011 hl created
\*---------------------------------------------------------------------------------------------------------*/

#define ETH_BOOT_GLOBALS

#include <ethernet_boot.h>
#include <lan92xx.h>
#include <fgc_ether.h>          // for FGC_FIELDBUS_CODE_BLOCKS_PER_MSG
#include <fieldbus_boot.h>      // for fieldbus, stat_var global variable
#include <codes.h>              // for code global variable
#include <dev.h>                // for dev global variable
#include <memmap_mcu.h>         // Include memory map constants
#include <macros.h>             // for Test(), Set()
#include <iodefines.h>          // for BSC.*
#include <string.h>             // for memcpy
#include <pll.h>                // for pll global variable
#include <sleep.h>
#include <led.h>
#include <dev.h>
#include <term.h>

#define ETH_LINKUP_TIMEOUT_MS 5000

/*---------------------------------------------------------------------------------------------------------*/
struct TlocalVarEth
{
    uint8_t  temp_code[FGC_FIELDBUS_CODE_BLOCKS_PER_MSG][FGC_CODE_BLK_SIZE]; // for Reception of code
    struct fgc_ether_header rx_header;         // for RX
    struct fgc_ether_header     tx_header;         // for TX
    char   rterm[FGC_ETHER_MAX_RTERM_CHARS];       // Remote terminal data
};
/*---------------------------------------------------------------------------------------------------------*/

// static variables for this file, can't be global in lan915.h because how fgc_fielbus, fgc_ether are included
struct TlocalVarEth local_eth;

/*---------------------------------------------------------------------------------------------------------*/
enum FGC_eth_status EthInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from main() and CheckStatus() to prepare the ethernet LAN9221 interface.
\*---------------------------------------------------------------------------------------------------------*/
{

    uint8_t       time_out        = 0;
    uint32_t      int_en_mask     = 0;

    // -- variables initialisation --
    eth_isr.dbg_nb_rx_packet = 0;
    eth_isr.dbg_nb_tx_packet = 0;
    eth_isr.dbg_nb_rx_error = 0;
    eth_isr.dbg_nb_tx_error = 0;
    fieldbus.is_gateway_online = FALSE;
    dev.fieldbus_id = 0;                // if zero, then fieldbus is not ready
    local_eth.rterm[0] = '\0';          // Reset rterm buffer
    fieldbus.rterm_ch_idx = 0;          // Reset rterm buffer index


    // -- wait for device ready with a timeout of 100ms --
    // note : look for 0x0001000 or 0x00000001 if we don't know endianness
    while (((ETH_PMT_CTRL_P & 0x00010001) == 0) && (time_out < 100))
    {
        MSLEEP(1);
        time_out++;
    }

    if (time_out == 100)
    {
        // 100ms time out, the chip is not responding

        return ETH_STATUS_CHIP_INACCESSIBLE;
    }

    // --- test if LAN9221 reg is accessible and set endianness ---
    if (ETH_BYTE_TEST_P == 0x87654321)
    {
        // right endianness
    }
    else
    {
        if (ETH_BYTE_TEST_P == 0x43218765)
        {
            // Switch endianness
            ETH_WORD_SWAP_P = 0xFFFFFFFF;
        }
        else
        {
            // register issue

            return ETH_STATUS_CHIP_INACCESSIBLE;
        }
    }

    // -- check if ID CODE is 9221 then overwrite bus access times --
    if ((ETH_ID_REV_P & 0xFFFF0000) == 0x92210000)   // lan9221
    {
        // Overwrite access timing
        // CS1 Wait Control Register 1
        BSC.CS1WCNT1.BIT.CSPWWAIT_20 = 1;  //page access
        BSC.CS1WCNT1.BIT.CSPRWAIT_20 = 1;  //page access
        BSC.CS1WCNT1.BIT.CSWWAIT_40 = 1;
        BSC.CS1WCNT1.BIT.CSRWAIT_40 = 1;

        // CS1 Wait Control Register 2
        BSC.CS1WCNT2.BIT.CSROFF_20 = 1;
        BSC.CS1WCNT2.BIT.CSWOFF_20 = 0;
        BSC.CS1WCNT2.BIT.WDOFF_20 = 1;
        BSC.CS1WCNT2.BIT.RDON_20 = 1;
        BSC.CS1WCNT2.BIT.WRON_20 = 0;
        BSC.CS1WCNT2.BIT.WDON_20 = 0;
        BSC.CS1WCNT2.BIT.CSON_20 = 0;
    }
    else
    {
        // Unexpected chip

        return ETH_STATUS_UNKNOWN_CHIP;
    }

    // -- Read fieldbus ID in external EEPROM, set mac address accordingly --

    dev.fieldbus_id = *((uint8_t *) ETH_MAC_ADDR_32) & 0xFF;

    if(dev.fieldbus_id == 127)
    {
        dev.is_standalone = true;
    }

    if(!devIdIsValid())
    {
        return ETH_STATUS_INVALID_ID;
    }

    if(dev.is_standalone)
    {
        return ETH_STATUS_SUCCESS;
    }

    // mac address is as follow: 02:00:00:00:00:XX where XX is the fieldbus id

    local_eth.tx_header.ethernet.src_addr[0] = 0x02;
    local_eth.tx_header.ethernet.src_addr[1] = 0x00;
    local_eth.tx_header.ethernet.src_addr[2] = 0x00;
    local_eth.tx_header.ethernet.src_addr[3] = 0x00;
    local_eth.tx_header.ethernet.src_addr[4] = 0x00;
    local_eth.tx_header.ethernet.src_addr[5] = dev.fieldbus_id;

    Lan92xxSetMac(local_eth.tx_header.ethernet.src_addr);



    //--------  device configuration --------------
    ETH_GPIO_CFG_P  =   0x30000000;     // activate LEDs 1 and 2
    // ETH_HW_CFG_P:    Byte swap for FIFO port only  (w/o FIFO_SEL)
    //                  Tune Rx/Tx FIFO size (maximal Rx size)
    //                  Set "Must Be One" bit (even though does not seem to affect operation)
    ETH_HW_CFG_P    = (ETH_HW_CFG_P & (~0x000F0000)) | 0x20000000 | 0x00020000 | 0x00100000;


    //--------  Interrupt setting --------------
    int_en_mask    =   ETH_RSFL_INT | ETH_TSFL_INT; // RX and TX status received
    int_en_mask    |=  ETH_RSFF_INT | ETH_TSFF_INT | ETH_TXE_INT  | ETH_RXE_INT; // Rx/TX fifo full, error
    int_en_mask    |=  ETH_TXSTOP_INT | ETH_RXSTOP_INT; // Rx/Tx stop
    int_en_mask    |=  ETH_TXSO_INT | ETH_TDFO_INT; // Tx status overflow, Tx data overrun
    int_en_mask    |=  ETH_RXDF_INT;    // Rx dropped frame
    ETH_INT_STS_P  =   int_en_mask;     // Clear all interrupt status
    ETH_INT_EN_P   =   int_en_mask;     // Enable interrupts
    ETH_IRQ_CFG_P  |=  0x01000100;      // enable interrupt line, minimal interrupt de-assertion time

    //--------  Enable TX and RX --------------
    // Configure MAC_CR : enable RX and TX
    ETH_MAC_CSR_DATA_P = 0x0010000C; // enable RX and TX, MAC Full Duplex
    ETH_MAC_CSR_CMD_P = 0x80000001; // write MAC_CR

    ETH_TX_CFG_P |= 0x00000002;      // Enable TX

    return ETH_STATUS_SUCCESS;
}

/*---------------------------------------------------------------------------------------------------------*/
void EthWaitLinkUp(void)
/*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t timeout_ms = ETH_LINKUP_TIMEOUT_MS;

    // For Ethernet, auto-negotiation can take a while (around 1,6s, no more than 1,7s)

    uint16_t phy_reg = Lan92xxReadPhyReg(LAN92_PHY_REG_STATUS);

    if(Test(phy_reg, LAN92_PHY_STATUS_LINK_UP) == false)
    {
        // TODO ?
    }

    if(Test(phy_reg, LAN92_PHY_STATUS_AUTO_NEGOTIATE_AVAILABLE) == false)
    {
        // TODO Set desired speed and duplex mode manually, report failure to the boot?
    }

    while(Test(phy_reg, LAN92_PHY_STATUS_AUTO_NEGOTIATE_COMPLETE) == false && --timeout_ms != 0)
    {
        MSLEEP(1);
        phy_reg = Lan92xxReadPhyReg(LAN92_PHY_REG_STATUS);
    }

    // TODO Report type of connection established by auto-negotiation and
    //      compare it against expected connection type (full duplex 100M)

    if(Test(phy_reg, LAN92_PHY_STATUS_AUTO_NEGOTIATE_COMPLETE) == true)
    {
        // Clear Rx FIFO

        // FGC3: ETH_RX_DROP_P is volatile so is not removed by the optimization
        // read to reset drop frame counter
        if (ETH_RX_DROP_P)
        {
        }

        while ((ETH_RX_FIFO_INF_P & 0x00FF0000) != 0)   // while packet available
        {
            // FGC3: ETH_RX_STATUS_PORT_P is volatile so is not removed by the optimization
            // read to reset
            if (ETH_RX_STATUS_PORT_P)
            {
            }

            lan92xxDiscardPacket(0);  // RXFWD
        }
    }
    else
    {
        // TODO Set desired speed and duplex mode manually, report failure to the boot?
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void EthCheck(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will be called every millisecond from the IsrMst() function.  It will check for a gateway
  timeout.
\*---------------------------------------------------------------------------------------------------------*/
{
    // ToDo: Useful??
}
/*---------------------------------------------------------------------------------------------------------*/
void EthISR(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will manage the interrupt from the lan921 chip for boot program
  All the processing of received packets takes place in the interrupt
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t  int_enable = ETH_INT_EN_P;

    // reception variables
    uint16_t rx_byte_length; //length in bytes

    if(!devIdIsValid() || dev.is_standalone)
    {
        return;
    }

    // Note: IRQ are sensitive to falling edge, therefore we need to clear ALL the lan92xx interrupts
    // so that the interrupt line goes inactive and IRQ is rearmed
    // If a new interrupt occurs during the routine, it MUST be processed and cleared as well.
    // With the de-assertion interval, we are sure no interrupt can arrive before leaving this routine.
    while (ETH_IRQ_CFG_P & 0x00001000) // While lan92xx interrupt is active
    {

        // Note : Interrupt status is set even if not enabled. Thus, test it is set AND enabled.
        //-------------------------------------------------
        //          RX
        //-------------------------------------------------
        if ((ETH_INT_STS_P & int_enable & ETH_RSFL_INT) != 0)       // new packet
        {

            while ((ETH_RX_FIFO_INF_P & 0x00FF0000) != 0)   // while packet available
            {
                //read RX status
                eth_isr.rx_status = ETH_RX_STATUS_PORT_P;

                if ((eth_isr.rx_status & 0x000090DA) != 0)    //error
                {
                    eth_isr.dbg_nb_crc_error++;
                }
                else
                {
                    rx_byte_length = (eth_isr.rx_status & 0x3FFF0000) >> 16; // in Bytes
                    eth_isr.current_rx_fifo_size = rx_byte_length;

                    // read and check rx_header
                    Lan92xxReadFifo(&(local_eth.rx_header),  sizeof(struct fgc_ether_header), &eth_isr.current_rx_fifo_size);

                    if (local_eth.rx_header.ethernet.ether_type != FGC_ETHER_TYPE)
                    {
                        eth_isr.dbg_nb_rx_error++;
                    }
                    else
                    {

                        switch ((enum fgc_ether_payload_type) local_eth.rx_header.fgc.payload_type)
                        {
                            case FGC_ETHER_PAYLOAD_TIME :
                                    if (fieldbus.is_gateway_online == FALSE)    // Link is now up, recover gateway address
                                    {

                                        memcpy(local_eth.tx_header.ethernet.dest_addr, local_eth.rx_header.ethernet.src_addr, FGC_ETHER_ADDR_SIZE);
                                        local_eth.tx_header.ethernet.ether_type = FGC_ETHER_TYPE;
                                        local_eth.tx_header.fgc.payload_type = FGC_ETHER_PAYLOAD_STATUS;
                                    }

                                EthReadTimeVar();
                                break;

                            default :
                                //                              eth_isr.current_rx_fifo_size -= Lan92xxReadFifo (eth_isr.dbg_dump_packet,  rx_byte_length); // to be removed
                                eth_isr.dbg_nb_rx_error++;
                                break;
                        }
                    }
                }

                // All data in the FIFO must be read
                // Discard end of packet if there is still data.
                // Note: CRC is discarded
                if (eth_isr.current_rx_fifo_size != 0)
                {
                    lan92xxDiscardPacket(eth_isr.current_rx_fifo_size);
                }

                eth_isr.dbg_nb_rx_packet++; // TO BE REMOVED
            }

            Lan92xxAckInterrupt(ETH_RSFL_INT);

        }

        if ((ETH_INT_STS_P & int_enable & ETH_RXE_INT) != 0)   // RX Error
        {
            eth_isr.dbg_nb_rx_error++;
            //          ETH_HW_CFG_P = 0x00000001; // Soft reset + Init required
            Lan92xxAckInterrupt(ETH_RXE_INT);
        }

        if ((ETH_INT_STS_P & int_enable & ETH_RSFF_INT) != 0)   // RX FIFO Full
        {
            eth_isr.dbg_nb_rx_error++;
            Lan92xxRxDump();
            Lan92xxAckInterrupt(ETH_RSFF_INT);
        }

        if ((ETH_INT_STS_P & int_enable & ETH_RXSTOP_INT) != 0)   //RX FIFO stop
        {
            eth_isr.dbg_nb_rx_error++;
            Lan92xxAckInterrupt(ETH_RXSTOP_INT);
        }

        if ((ETH_INT_STS_P & int_enable & ETH_RXDF_INT) != 0)   //RX drop frame
        {
            eth_isr.dbg_nb_rx_error++;

            // FGC3: ETH_RX_DROP_P is volatile so is not removed by the optimisation
            // read to reset drop frame counter
            if (ETH_RX_DROP_P)
            {
            }

            Lan92xxRxDump();
            Lan92xxAckInterrupt(ETH_RXDF_INT);
        }

        //-------------------------------------------------
        //                  TX
        //-------------------------------------------------
        if ((ETH_INT_STS_P & int_enable & ETH_TSFL_INT) != 0)   //TX FIFO Level
        {
            while ((ETH_TX_FIFO_INF_P & 0x00FF0000) != 0)   // while status available
            {
                //read TX status
                eth_isr.tx_status = ETH_TX_STATUS_PORT_P;
                eth_isr.dbg_nb_tx_packet++;

                if ((eth_isr.tx_status & 0x00008000) != 0)   // tx error
                {
                    eth_isr.dbg_nb_tx_error++;
                }

                Lan92xxAckInterrupt(ETH_TSFL_INT);
            }
        }

        if ((ETH_INT_STS_P & int_enable & ETH_TSFF_INT) != 0)   //TX FIFO Full
        {
            eth_isr.dbg_nb_tx_error++;
            Lan92xxAckInterrupt(ETH_TSFF_INT);
        }

        if ((ETH_INT_STS_P & int_enable & ETH_TXSO_INT) != 0)   //Tx status overflow
        {
            eth_isr.dbg_nb_tx_error++;
            Lan92xxAckInterrupt(ETH_TXSO_INT);
        }

        if ((ETH_INT_STS_P & int_enable & ETH_TDFO_INT) != 0)   //TX data FIFO overrun
        {
            eth_isr.dbg_nb_tx_error++;
            Lan92xxAckInterrupt(ETH_TDFO_INT);
        }

        if ((ETH_INT_STS_P & int_enable & ETH_TXE_INT) != 0)   //TX error
        {
            eth_isr.dbg_nb_tx_error++;
            Lan92xxAckInterrupt(ETH_TXE_INT);
        }

        if ((ETH_INT_STS_P & int_enable & ETH_TXSTOP_INT) != 0)   //TX FIFO stop
        {
            eth_isr.dbg_nb_tx_error++;
            Lan92xxAckInterrupt(ETH_TXSTOP_INT);
        }

        if ((ETH_INT_STS_P & int_enable & ETH_TDFO_INT) != 0)   //Tx data overrun
        {
            eth_isr.dbg_nb_tx_error++;
            Lan92xxAckInterrupt(ETH_TDFO_INT);
        }
    }

}
/*---------------------------------------------------------------------------------------------------------*/
void EthTxCh(char ch)
/*---------------------------------------------------------------------------------------------------------*\
    Write ch in the term buffer.
    Discarded if the buffer is full
\*---------------------------------------------------------------------------------------------------------*/
{
    if (fieldbus.rterm_ch_idx < FGC_ETHER_MAX_RTERM_CHARS - 1)  // if rterm buffer is not full
    {
        local_eth.rterm[fieldbus.rterm_ch_idx++] = ch;      // add new char in buffer
        local_eth.rterm[fieldbus.rterm_ch_idx]   = '\0';    // NULL terminated
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void EthReadTimeVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function read, and process a payload of FGC_ETHER_PAYLOAD_TIME type.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t   ii;

    Lan92xxReadFifo(&time_var, sizeof(time_var), &eth_isr.current_rx_fifo_size);

    // On Ethernet, time_var.ms_time must be rounded to a 20 ms boundary

    // we need to validate that this irq comes from MSG18 or MSG19
    if ((time_var.ms_time % 20) == 18)
    {
        pll.eth18.sync_time = PLL_NETIRQTIME_P;
    }
    else
    {
        pll.eth19.sync_time = PLL_NETIRQTIME_P;
    }

    // process time data. Only 1 over 2 in one cycle
    if ((fieldbus.is_gateway_online == FALSE) || (eth_isr.timeVar_processed == FALSE))
    {
        FieldbusRxTime();
        eth_isr.timeVar_processed = TRUE;

        // Process code only if needed.
        if ((time_var.code_var_id != FGC_CODE_NULL) &&
            (code.rx.mode == CODES_RX_FIELDBUS) &&
            ((code.rx.reception_mask & (1L << time_var.code_var_id)) != 0))
        {
            // This check is to prevent downloading code starting from the middle
            // We always wait for the block with index 0 to start

            if(!code.rx.first_block)
            {
                if(time_var.code_var_idx != 0)
                {
                    return;
                }

                code.rx.first_block = true;
            }

            // read rt_ref not used at boot time. Just read it
            for (ii = 0;  ii < sizeof(((struct fgc_ether_time *)0)->payload.rt_ref) / 4; ii++)
            {
                // FGC3: ETH_RX_DATA_FIFO_PORT_P is volatile so is not removed by the optimization
                // read to reset
                if (ETH_RX_DATA_FIFO_PORT_P)
                {
                }
            }

            eth_isr.current_rx_fifo_size -= sizeof(((struct fgc_ether_time *)0)->payload.rt_ref);

            EthReadCodeVar();
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void EthReadCodeVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function process the code data.
\*---------------------------------------------------------------------------------------------------------*/
{
    // local copy of code  to give it to FieldbusRxCode
    Lan92xxReadFifo(local_eth.temp_code, FGC_CODE_BLK_SIZE * FGC_FIELDBUS_CODE_BLOCKS_PER_MSG,
                    &eth_isr.current_rx_fifo_size);
    FieldbusRxCode(FGC_FIELDBUS_CODE_BLOCKS_PER_MSG, local_eth.temp_code);
}
/*---------------------------------------------------------------------------------------------------------*/
void EthWriteStatVar(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function send the status.
\*---------------------------------------------------------------------------------------------------------*/
{

    uint8_t err;

    // Prepare status variable

    if (stat_var.fgc_stat.class_data.CLASS_FIELD.state_pll == FGC_PLL_LOCKED)   // If PLL is "locked"
    {
        stat_var.fieldbus_stat.ack |= FGC_ACK_PLL;      // Set PLL bit in ACK byte
    }

    err =  Lan92xxSendPacketOpt(&local_eth.tx_header, sizeof(struct fgc_ether_header),
                                sizeof(struct fgc_ether_status), TX_CMDA_FIRST_SEG, 0);
    err |= Lan92xxSendPacketOpt(&stat_var, sizeof(union fgc_stat_var), sizeof(struct fgc_ether_status), 0, 0);
    err |= Lan92xxSendPacketOpt(local_eth.rterm,
                                sizeof(local_eth.rterm) + sizeof(((struct fgc_ether_status *)0)->payload.reserved),
                                sizeof(struct fgc_ether_status), TX_CMDA_LAST_SEG, 0);

    if (err)
    {
        // sending fail
        eth_isr.dbg_nb_tx_error++;
    }

    local_eth.rterm[0] = '\0';      // Reset rterm buffer
    fieldbus.rterm_ch_idx = 0;      // Reset rterm buffer index

    // Adjust status sequence number to go 0, 1, 2, 3, 1, 2, 3, 1, 2, 3, ...

    stat_var.fieldbus_stat.ack = (stat_var.fieldbus_stat.ack + 1) & FGC_ACK_STATUS; // Increment stat seq number

    if (stat_var.fieldbus_stat.ack == 0)            // If sequence has rolled back to zero
    {
        stat_var.fieldbus_stat.ack = 1;             // Advance to 1
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ethernet.c
\*---------------------------------------------------------------------------------------------------------*/
