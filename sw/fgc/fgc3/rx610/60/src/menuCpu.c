/*!
 *	@file 	menuCpu.c
 *	@brief	Tests for CPU registers
 */


// Includes

#include <stdint.h>
#include <stdio.h>

#include <bitmap.h>
#include <boot_dsp_cmds.h>
#include <boot_dual_port_ram.h>
#include <defconst.h>
#include <dsp_cmds.h>
#include <led.h>
#include <lssb.h>
#include <memmap_mcu.h>
#include <menu.h>
#include <scivs.h>
#include <sleep.h>
#include <term.h>
#include <time_fgc.h>


typedef bool (*irqctrl_func)(uint16_t scivs_slot);



// Internal function declarations

static bool irqctrlMissedInterrupts(uint16_t scivs_slot);

static bool irqctrlMsTick(uint16_t scivs_slot);

static bool irqctrlInterFgc(uint16_t scivs_slot);

static bool irqctrlScivs(uint16_t scivs_slot);

static bool irqctrlTriggerMcuInterrupt(uint16_t scivs_slot);

static bool irqctrlTriggerDspInterrupt(uint16_t scivs_slot);



// Internal variable definitions

static irqctrl_func irqctrl_test[] =
{
    irqctrlMissedInterrupts,
    irqctrlMsTick,
    irqctrlInterFgc,
    irqctrlScivs,
    irqctrlTriggerMcuInterrupt,
    irqctrlTriggerDspInterrupt
};



// Test for the LSSB register

void MenuSTlssb(uint16_t argc, char ** argv)
{
	volatile uint16_t * lssb_reg = (uint16_t *) CPU_LSSB_32;

	uint16_t pattern;

	// Perform a walking 1's test.

    for (pattern = 1; pattern != 0; pattern <<= 1)
    {
        // Write the test pattern.

        *lssb_reg = pattern;

        // Read it back the LSSB and compare with software computed value.

        if (*lssb_reg != lssb(pattern))
        {
            MenuRspError("LSSB test failed.");
            return;
        }
    }

    // Test for zero. Should return 0x1F.

    *lssb_reg = 0;

    if (*lssb_reg != lssb(pattern))
    {
        MenuRspError("LSSB test failed.");
        return;
    }
}



void MenuSTmid(uint16_t argc, char ** argv)
{
    uint32_t dsp_data;
    uint16_t test_value;

    bool error = false;


    // MCU Tests

    // Verify FGC TYPE

    if (MID_FGCTYPE_P != 0x0301)
    {
        fprintf(term.f, RSP_DATA ",MCU: Wrong FGC type.\n");
        fprintf(term.f, RSP_DATA ",MCU: Should be 0x0301 and got 0x%04X.\n", MID_FGCTYPE_P);

        error = true;
    }


    // Verify FGC VERSION

    if (MID_FGCVER_P != 0x0002)
    {
        fprintf(term.f, RSP_DATA ",MCU: Wrong FGC version.\n");
        fprintf(term.f, RSP_DATA ",MCU: Should be 0x0002 and got 0x%04X.\n", MID_FGCVER_P);

        error = true;
    }

    // Verify PLD TYPE

    if (   MID_PLDTYPE_P != 0x0700
        && MID_PLDTYPE_P != 0x1400)
    {
        fprintf(term.f, RSP_DATA ",MCU: Wrong PLD type.\n");
        fprintf(term.f, RSP_DATA ",MCU: Should be 0x0700 or 0x1400 and got 0x%04X.\n", MID_PLDTYPE_P);

        error = true;
    }


    // Verify PLD VERSION - example 0xXYZA

    // Nibble X : FPGA or CPLD Type (0 = XC3S700AN, 1 = XC3S1400AN)

    test_value = MID_PLDVER_P & 0xF000;

    if (   test_value != 0x0000
        && test_value != 0x1000)
    {
        fprintf(term.f, RSP_DATA ",MCU: Wrong PLD version.\n");
        fprintf(term.f, RSP_DATA ",MCU: Type (Nibble X) should be 0 or 1. Got %X.\n", test_value >> 12);

        error = true;
    }

    test_value = MID_PLDVER_P & 0x0F00;

    // Nibble Y : Mode (0 = Normal, 1 = Tester)

    if (   test_value != 0x0000
        && test_value != 0x0100)
    {
        fprintf(term.f, RSP_DATA ",MCU: Wrong PLD version.\n");
        fprintf(term.f, RSP_DATA ",MCU: Mode (Nibble Y) should be 0 or 1. Got %X.\n", test_value >> 8);

        error = true;
    }


    // Verify CRATE TYPE

    if (MID_CRATETYPE_P != FGC_CRATE_TYPE_RECEPTION)
    {
        fprintf(term.f, RSP_DATA ",MCU: Wrong crate type. This test only works on the reception crate.\n");
        fprintf(term.f, RSP_DATA ",MCU: Expected %u and got %u.\n", FGC_CRATE_TYPE_RECEPTION, MID_CRATETYPE_P);

        error = true;
    }


    // Verify NET TYPE

    if (   MID_NETTYPE_P != FGC_NETWORK_ETHER_101
        && MID_NETTYPE_P != FGC_NETWORK_ETHER_103)
    {
        fprintf(term.f, RSP_DATA ",MCU: Wrong Network Interface type.\n");
        fprintf(term.f, RSP_DATA ",MCU: Expected %u or %u and got %u.\n", FGC_NETWORK_ETHER_101, FGC_NETWORK_ETHER_103, MID_NETTYPE_P);

        error = true;
    }


    // Verify ANA TYPE

    if (   MID_ANATYPE_P != FGC_INTERFACE_ANA_101
        && MID_ANATYPE_P != FGC_INTERFACE_ANA_103
        && MID_ANATYPE_P != FGC_INTERFACE_ANA_104)
    {
        fprintf(term.f, RSP_DATA ",MCU: Wrong Analog Interface type.\n");
        fprintf(term.f, RSP_DATA ",MCU: Expected ANA-101, ANA-103 or ANA-104 and got ANA-10%u.\n", MID_ANATYPE_P);

        error = true;
    }

    // Check if ANA type and Nibble Z of PLD version match

    test_value = (MID_PLDVER_P & 0x00F0) >> 4;

    if (test_value != MID_ANATYPE_P)
    {
        fprintf(term.f, RSP_DATA ",MCU: ANA TYPE and PLD version do not match.\n");
        fprintf(term.f, RSP_DATA ",MCU: ANA TYPE: ANA-10%u - PLD version value: ANA-10%u.\n", MID_ANATYPE_P, test_value);

        error = true;
    }


    // Verify End Of MID

    if (MID_EOMID_P != 0xA55A)
    {
        fprintf(term.f, RSP_DATA ",MCU: Wrong End Of Module IDentifier.\n");
        fprintf(term.f, RSP_DATA ",MCU: Should be 0xA55A and got 0x%04X.\n", MID_EOMID_P);

        error = true;
    }



    // DSP Tests

    // Verify FGC TYPE

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_MID_FGCTYPE_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout.");
        return;
    }

    if (testBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR) == true)
    {
        dsp_data = dpram->dsp.slowRsp.data & ~DSP_MID_ERROR;

        fprintf(term.f, RSP_DATA ",DSP: Wrong FGC type.\n");
        fprintf(term.f, RSP_DATA ",DSP: Should be 0x0301 and got 0x%04lX.\n", dsp_data);

        error = true;
    }


    // Verify FGC VERSION

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_MID_FGCVER_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout.");
        return;
    }

    if (testBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR) == true)
    {
        dsp_data = dpram->dsp.slowRsp.data & ~DSP_MID_ERROR;

        fprintf(term.f, RSP_DATA ",DSP: Wrong FGC version.\n");
        fprintf(term.f, RSP_DATA ",DSP: Should be 0x0002 and got 0x%04lX.\n", dsp_data);

        error = true;
    }


    // Verify PLD TYPE

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_MID_PLDTYPE_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout.");
        return;
    }

    if (testBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR) == true)
    {
        dsp_data = dpram->dsp.slowRsp.data & ~DSP_MID_ERROR;

        fprintf(term.f, RSP_DATA ",DSP: Wrong PLD type.\n");
        fprintf(term.f, RSP_DATA ",DSP: Should be 0x0700 or 0x1400 and got 0x%04lX.\n", dsp_data);

        error = true;
    }


    // Verify PLD VERSION - example 0xXYZA

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_MID_PLDVER_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout.");
        return;
    }

    if (testBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR) == true)
    {
        dsp_data = dpram->dsp.slowRsp.data & ~DSP_MID_ERROR;

        fprintf(term.f, RSP_DATA ",DSP: Wrong PLD version.\n");

        // Nibble X : FPGA or CPLD Type (0 = XC3S700AN, 1 = XC3S1400AN)

        test_value = dsp_data & 0xF000;

        if (test_value != 0x0000)
        {
            fprintf(term.f, RSP_DATA ",DSP: Type (Nibble X) should be 0 or 1. Got %X.\n", test_value >> 12);
        }

        // Nibble Y : Mode (0 = Normal, 1 = Tester)

        test_value = dsp_data & 0x0F00;

        if (test_value != 0x0000)
        {
            fprintf(term.f, RSP_DATA ",DSP: Mode (Nibble Y) should be 0 or 1. Got %X.\n", test_value >> 8);
        }

        error = true;
    }


    // Verify CRATE TYPE

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_MID_CRATETYPE_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout.");
        return;
    }

    if (testBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR) == true)
    {
        dsp_data = dpram->dsp.slowRsp.data & ~DSP_MID_ERROR;

        fprintf(term.f, RSP_DATA ",DSP: Wrong crate type.\n");
        fprintf(term.f, RSP_DATA ",DSP: Expected %u and got %lu.\n", FGC_CRATE_TYPE_RECEPTION, dsp_data);

        error = true;
    }


    // Verify NET TYPE

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_MID_NETTYPE_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout.");
        return;
    }

    if (testBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR) == true)
    {
        dsp_data = dpram->dsp.slowRsp.data & ~DSP_MID_ERROR;

        fprintf(term.f, RSP_DATA ",DSP: Wrong Network Interface type.\n");
        fprintf(term.f, RSP_DATA ",DSP: Expected %u or %u and got %lu.\n", FGC_NETWORK_ETHER_101, FGC_NETWORK_ETHER_103, dsp_data);

        error = true;
    }


    // Verify ANA TYPE

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_MID_ANATYPE_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout.");
        return;
    }

    if (testBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR) == true)
    {
        dsp_data = dpram->dsp.slowRsp.data & ~DSP_MID_ERROR;

        fprintf(term.f, RSP_DATA ",DSP: Wrong Analog Interface type.\n");
        fprintf(term.f, RSP_DATA ",DSP: Expected ANA-101 or ANA-103 and got ANA-10%lu.\n", dsp_data);

        error = true;
    }


    // Check if ANA type and Nibble Z of PLD version match

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_MID_MISMATCH_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout.");
        return;
    }

    if (testBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR) == true)
    {
        dsp_data = dpram->dsp.slowRsp.data & ~DSP_MID_ERROR;

        fprintf(term.f, RSP_DATA ",DSP: ANA TYPE and PLD version do not match.\n");
        fprintf(term.f, RSP_DATA ",DSP: ANA TYPE: ANA-10%lu - PLD version value: ANA-10%lu.\n", dsp_data & 0x000F, (dsp_data & 0x00F0) >> 4);

        error = true;
    }


    // Verify End Of MID

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_MID_EOMID_TEST, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout.");
        return;
    }

    if (testBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR) == true)
    {
        dsp_data = dpram->dsp.slowRsp.data & ~DSP_MID_ERROR;

        fprintf(term.f, RSP_DATA ",DSP: Wrong End Of Module IDentifier.\n");
        fprintf(term.f, RSP_DATA ",DSP: Should be 0xA55A and got 0x%04lX.\n", dsp_data);

        error = true;
    }


    if (error == true)
    {
        MenuRspError("FAILED");
    }
}



void MenuSTirqctrl(uint16_t argc, char ** argv)
{
    uint16_t       scivs_slot;
    uint8_t  const num_tests = sizeof(irqctrl_test)/sizeof(irqctrl_func);


    if (MenuGetInt16U(argv[0], 0, FGC_SCIVS_N_SLOT - 1, &scivs_slot) != 0)
    {
        return;
    }

    // Disable periodic interrupts

    IRQCTRL_MSTICKLVL_P    = 0x0000;
    IRQCTRL_INTERFGCLVL_P  = 0x0000;

    // fprintf(term.f, RSP_DATA ",ms_tick_lvl   = 0x%04x, %u\n", IRQCTRL_MSTICKLVL_P, IRQCTRL_MSTICKLVL_P);
    // fprintf(term.f, RSP_DATA ",inter_fgc_lvl = 0x%04x, %u\n", IRQCTRL_INTERFGCLVL_P, IRQCTRL_INTERFGCLVL_P);
    // fprintf(term.f, RSP_DATA ",scivs_lvl     = 0x%04x, %u\n", IRQCTRL_SCIVSLVL_P, IRQCTRL_SCIVSLVL_P);
    // fprintf(term.f, RSP_DATA ",mcu_irq1_lvl  = 0x%04x, %u\n", IRQCTRL_MCUSWIRQ1LVL_P, IRQCTRL_MCUSWIRQ1LVL_P);
    // fprintf(term.f, RSP_DATA ",mcu_irq2_lvl  = 0x%04x, %u\n", IRQCTRL_MCUSWIRQ2LVL_P, IRQCTRL_MCUSWIRQ2LVL_P);
    // fprintf(term.f, RSP_DATA ",mcu_irq4_lvl  = 0x%04x, %u\n", IRQCTRL_MCUSWIRQ3LVL_P, IRQCTRL_MCUSWIRQ3LVL_P);
    // fprintf(term.f, RSP_DATA ",mcu_irq4_lvl  = 0x%04x, %u\n", IRQCTRL_MCUSWIRQ4LVL_P, IRQCTRL_MCUSWIRQ4LVL_P);
    // fprintf(term.f, RSP_DATA ",dsp_irq1_lvl  = 0x%04x, %u\n", IRQCTRL_DSPSWIRQ1LVL_P, IRQCTRL_DSPSWIRQ1LVL_P);
    // fprintf(term.f, RSP_DATA ",dsp_irq2_lvl  = 0x%04x, %u\n", IRQCTRL_DSPSWIRQ2LVL_P, IRQCTRL_DSPSWIRQ2LVL_P);
    // fprintf(term.f, RSP_DATA ",dsp_irq3_lvl  = 0x%04x, %u\n", IRQCTRL_DSPSWIRQ3LVL_P, IRQCTRL_DSPSWIRQ3LVL_P);

    for (uint8_t i = 0; i < num_tests; ++i)
    {
        if (irqctrl_test[i](scivs_slot) == false)
        {
            return;
        }
    }
}



static bool irqctrlMissedInterrupts(uint16_t scivs_slot)
{
    uint16_t missed;

    IRQCTRL_MSTICKLVL_P = 0x0001;

    for (uint8_t i = 0; i < 100; ++i)
    {
        MSLEEP(3);
        missed = IRQCTRL_MISSEDIRQS_P;

        if (missed == 0)
        {
            fprintf(term.f, RSP_ERROR ",Missed interrupts test: FAILED\n");
            IRQCTRL_MSTICKLVL_P = 0x0000;
            return false;
        }
    }

    IRQCTRL_MSTICKLVL_P = 0x0000;

    fprintf(term.f, RSP_DATA ",Missed interrupts test: OK\n");

    return true;
}



static bool irqctrlMsTick(uint16_t scivs_slot)
{
    uint16_t pending;
    uint16_t missed = 0;
    uint16_t iteration = 0;
    uint16_t sync_timeout;
    bool     test_ok = true;

    // Enable ms tick interrupts

    IRQCTRL_MSTICKLVL_P = 0x0001;

    // Clear missed interrupts register

    missed = IRQCTRL_MISSEDIRQS_P;

    // Sync with ms tick after enabling interrupt

    sync_timeout = 0x7FFF;

    do
    {
        pending = IRQCTRL_PENDINGIRQ_P;
        sync_timeout--;
        if (sync_timeout == 0)
        {
            fprintf(term.f, RSP_DATA ",Failed to sync.\n");
            test_ok = false;
            break;
        }
    }
    while(pending != 0);

    // Test consistency of the interrupt for 100 iterations

    while(   pending   == 0
          && missed    == 0
          && iteration <  100)
    {
        MSLEEP(1);
        pending = IRQCTRL_PENDINGIRQ_P;
        missed  = IRQCTRL_MISSEDIRQS_P;
        iteration++;
    }

    if (missed != 0)
    {
        fprintf(term.f, RSP_DATA ",Interrupt missed.\n");
        test_ok = false;
    }

    // Disable interrupt for ms tick

    IRQCTRL_MSTICKLVL_P = 0x0000;
    MSLEEP(1);

    // Read the pending interrupts register one last time after resetting level
    // in order to clear the interrupt for next test

    pending = IRQCTRL_PENDINGIRQ_P;

    if (test_ok == true)
    {
        fprintf(term.f, RSP_DATA ",Interrupt test for ms tick: OK\n");
    }
    else
    {
        fprintf(term.f, RSP_ERROR ",Interrupt test for ms tick: FAILED\n");
    }


    return test_ok;
}



static bool irqctrlInterFgc(uint16_t scivs_slot)
{
    uint16_t pending;
    uint16_t missed;
    uint16_t iteration = 0;
    uint16_t sync_timeout;
    bool     test_ok = true;

    // Enable inter FGC interrupts

    IRQCTRL_INTERFGCLVL_P = 0x8000;

    // Clear missed interrupts register

    missed = IRQCTRL_MISSEDIRQS_P;

    // Sync with ms tick after enabling interrupt

    sync_timeout = 0x7FFF;

    do
    {
        pending = IRQCTRL_PENDINGIRQ_P;
        sync_timeout--;
        if (sync_timeout == 0)
        {
            fprintf(term.f, RSP_DATA ",Failed to sync.\n");
            test_ok = false;
            break;
        }
    }
    while(pending != 15);

    // Test consistency of the interrupt for 100 iterations

    while(   pending   == 15
          && missed    == 0
          && iteration <  100)
    {
        MSLEEP(1);
        pending = IRQCTRL_PENDINGIRQ_P;
        missed  = IRQCTRL_MISSEDIRQS_P;
        iteration++;
    }

    if (missed != 0)
    {
        fprintf(term.f, RSP_DATA ",Interrupt missed.\n");
        test_ok = false;
    }

    // Disable interrupt for inter FGC

    IRQCTRL_INTERFGCLVL_P = 0x0000;
    MSLEEP(1);

    // Read the pending interrupts register one last time after resetting level
    // in order to clear the interrupt for next test

    pending = IRQCTRL_PENDINGIRQ_P;

    // Confirm that Inter FGC interrupts have stopped

    for (iteration = 0; iteration < 100; ++iteration)
    {
        MSLEEP(1);
        pending = IRQCTRL_PENDINGIRQ_P;

        if (pending != 31)
        {
            fprintf(term.f, RSP_DATA ",Unintentional interrupt detected.\n");
            test_ok = false;
        }
    }

    if (test_ok == true)
    {
        fprintf(term.f, RSP_DATA ",Inter FGC interrupt test: OK\n");
    }
    else
    {
        fprintf(term.f, RSP_ERROR ",Inter FGC interrupt test: FAILED\n");
    }

    return test_ok;
}



static bool irqctrlScivs(uint16_t scivs_slot)
{
    uint16_t pending;

    IRQCTRL_SCIVSLVL_P = 0x4000;

    if (scivsTaskSend(SCIVS_TASK_GENERAL_CONST, scivs_slot, SCIVS_MF, SCIVS_MASTER_TIMEOUT_MS) != SCIVS_OK)
    {
        fprintf(term.f, RSP_DATA  ",SCIVS Communication failed\n");
        fprintf(term.f, RSP_ERROR ",SCIVS interrupt test: FAILED\n");
        return false;
    }

    pending = IRQCTRL_PENDINGIRQ_P;

    if (pending != lssb(0x4000))
    {
        fprintf(term.f, RSP_DATA  ",SCIVS error. Expected level %u and got %u.\n", lssb(0x4000), pending);
        fprintf(term.f, RSP_ERROR ",SCIVS interrupt test: FAILED\n");
        return false;
    }

    fprintf(term.f, RSP_DATA ",SCIVS interrupt test: OK\n");

    return true;
}



static bool irqctrlTriggerMcuInterrupt(uint16_t scivs_slot)
{
    uint16_t pending;
    bool     error_detected = false;

    // Set the level registers

    IRQCTRL_MCUSWIRQ1LVL_P = 0x0010;
    IRQCTRL_MCUSWIRQ2LVL_P = 0x0020;
    IRQCTRL_MCUSWIRQ3LVL_P = 0x0040;
    IRQCTRL_MCUSWIRQ4LVL_P = 0x0080;

    // fprintf(term.f, RSP_DATA ",mcu_irq1_lvl  = 0x%04x, %u\n", IRQCTRL_MCUSWIRQ1LVL_P, IRQCTRL_MCUSWIRQ1LVL_P);
    // fprintf(term.f, RSP_DATA ",mcu_irq2_lvl  = 0x%04x, %u\n", IRQCTRL_MCUSWIRQ2LVL_P, IRQCTRL_MCUSWIRQ2LVL_P);
    // fprintf(term.f, RSP_DATA ",mcu_irq4_lvl  = 0x%04x, %u\n", IRQCTRL_MCUSWIRQ3LVL_P, IRQCTRL_MCUSWIRQ3LVL_P);
    // fprintf(term.f, RSP_DATA ",mcu_irq4_lvl  = 0x%04x, %u\n", IRQCTRL_MCUSWIRQ4LVL_P, IRQCTRL_MCUSWIRQ4LVL_P);

    // Trigger all interrupts at the same time

    IRQCTRL_MCUSWIRQ1TRIG_P = 1;
    IRQCTRL_MCUSWIRQ2TRIG_P = 1;
    IRQCTRL_MCUSWIRQ3TRIG_P = 1;
    IRQCTRL_MCUSWIRQ4TRIG_P = 1;

    // Test if interrupts are handled according to the priority set in the level registers.
    // If the levels are changed, this for loop must be adjusted accordingly.

    for (uint16_t level = 0x0010; level != 0x0100; level <<= 1)
    {
        pending = IRQCTRL_PENDINGIRQ_P;

        if (pending != lssb(level))
        {
            error_detected = true;

            fprintf(term.f, RSP_DATA ",Wrong MCU interrupt processed. Expected level %u and got %u.\n", lssb(level), pending);
        }
    }

    if (error_detected == false)
    {
        fprintf(term.f, RSP_DATA ",MCU software interrupt test: OK\n");
        return true;
    }
    else
    {
        fprintf(term.f, RSP_ERROR ",MCU software interrupt test: FAILED\n");
        return false;
    }
}



static bool irqctrlTriggerDspInterrupt(uint16_t scivs_slot)
{
    uint16_t pending;
    bool     error_detected = false;

    // Set the level registers

    IRQCTRL_DSPSWIRQ1LVL_P = 0x0100;
    IRQCTRL_DSPSWIRQ2LVL_P = 0x0200;
    IRQCTRL_DSPSWIRQ3LVL_P = 0x0400;

    // fprintf(term.f, RSP_DATA ",dsp_irq1_lvl  = 0x%04x, %u\n", IRQCTRL_DSPSWIRQ1LVL_P, IRQCTRL_DSPSWIRQ1LVL_P);
    // fprintf(term.f, RSP_DATA ",dsp_irq2_lvl  = 0x%04x, %u\n", IRQCTRL_DSPSWIRQ2LVL_P, IRQCTRL_DSPSWIRQ2LVL_P);
    // fprintf(term.f, RSP_DATA ",dsp_irq3_lvl  = 0x%04x, %u\n", IRQCTRL_DSPSWIRQ3LVL_P, IRQCTRL_DSPSWIRQ3LVL_P);

    // Trigger all interrupts at the same time

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_IRQTRL_DSP_TRIG, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
    {
        MenuRspError("DSP rsp timeout. DSP software interrupt test: FAILED");
        return false;
    }

    // Test if interrupts are handled according to the priority set in the level registers.
    // If the levels are changed, this for loop must be adjusted accordingly.

    for (uint16_t level = 0x0100; level != 0x0800; level <<= 1)
    {
        pending = IRQCTRL_PENDINGIRQ_P;

        if (pending != lssb(level))
        {
            error_detected = true;

            fprintf(term.f, RSP_DATA ",Wrong DSP interrupt processed. Expected level %u and got %u.\n", lssb(level), pending);
        }
    }

    if (error_detected == false)
    {
        fprintf(term.f, RSP_DATA ",DSP software interrupt test: OK\n");
        return true;
    }
    else
    {
        fprintf(term.f, RSP_ERROR ",DSP software interrupt test: FAILED\n");
        return false;
    }
}

// EOF