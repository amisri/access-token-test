/*!
 *  @file      dig.c
 *  @defgroup  FGC:MCU
 *  @brief     Interface to the digital input/output channels.
 */

#define DIG_GLOBALS

// Includes

#include <dig.h>
#include <runlog_lib.h>
#include <fgc_runlog_entries.h>

// Internal functions declaration

static inline void DigSupInit();

// Internal function definitions

static inline void DigSupInit(void)
{
    uint16_t channel;

    Set(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_PERMIT_MASK16);  // Output enabled
    Set(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_PERMIT_MASK16);  // Output enabled
    Set(DIG_SUP_A_DIR_P, DIG_SUP_A_DIR_OUT_CTRL_MASK16);    // Write
    Clr(DIG_SUP_B_DIR_P, DIG_SUP_B_DIR_OUT_CTRL_MASK16);    // Read

    // Select source for bank A and B ad Data Register

    for (channel = 0; channel < 4; channel++)
    {
        DigSupSetSourceBankA(channel, DIG_SUP_SOURCE_PULSE_GEN);
        DigSupSetSourceBankB(channel, DIG_SUP_SOURCE_DIRECT);
    }
}

// External function definitions

void DigInit(void)
{
    DigSupInit();
}



dig_sup_status_t DigSupSetSourceBankA(const dig_sup_channel_t channel, const dig_sup_source_t source)
{
    switch(source)
    {
        case DIG_SUP_SOURCE_SERIAL:

            if(channel != DIG_SUP_A_CHANNEL2 && channel != DIG_SUP_A_CHANNEL3)
            {
                return DIG_SUP_STATUS_NOT_SUPPORTED;
            }

            break;

        case DIG_SUP_SOURCE_CYCLE_START:

            if(channel != DIG_SUP_A_CHANNEL4)
            {
                return DIG_SUP_STATUS_NOT_SUPPORTED;
            }

            break;

        case DIG_SUP_SOURCE_PULSE_GEN:
        case DIG_SUP_SOURCE_50HZ:
        case DIG_SUP_SOURCE_1KHZ:
        case DIG_SUP_SOURCE_1MHZ:

            if(channel == DIG_SUP_A_CHANNEL4)
            {
                return DIG_SUP_STATUS_NOT_SUPPORTED;
            }

            break;

        case DIG_SUP_SOURCE_DIRECT:

            break;
    }

    DIG_SUP_A_CHAN_CTRL_A[channel] = source | (DIG_SUP_A_CHAN_CTRL_A[channel] &
                                               (~DIG_SUP_A_CHAN_CTRL_OUTMPX_MASK16));

    return DIG_SUP_STATUS_SUCCESS;
}



dig_sup_status_t DigSupSetSourceBankB(const dig_sup_channel_t channel, const dig_sup_source_t source)
{
    if(source != DIG_SUP_SOURCE_DIRECT)
    {
        return DIG_SUP_STATUS_NOT_SUPPORTED;
    }

    DIG_SUP_B_CHAN_CTRL_A[channel] = source | (DIG_SUP_B_CHAN_CTRL_A[channel] &
                                               (~DIG_SUP_B_CHAN_CTRL_OUTMPX_MASK16));

    return DIG_SUP_STATUS_SUCCESS;
}

// EOF
