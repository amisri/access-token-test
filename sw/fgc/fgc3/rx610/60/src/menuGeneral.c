/*---------------------------------------------------------------------------------------------------------*\
  File:         menuGeneral.c

  Purpose:      Functions to run for the general boot menu options

  Author:       Stephen.Page@cern.ch

  Notes:
                These menues belongs to the [Manual Test] part and [Self Test] part

  History:

    13/04/04    stp     Created
    22/11/06    pfr     Ported for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <boot_dsp_cmds.h>
#include <boot_dual_port_ram.h>
#include <codes.h>
#include <dev.h>
#include <dsp_cmds.h>
#include <main.h>
#include <mem.h>
#include <menu.h>
#include <term.h>
#include <version.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralShowBuildInfo(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(term.f, "--------------MCU--------------\n");
    //  fprintf(term.f, "%d\n" , version.unixtime );        // Build timestamp
    fprintf(term.f, "%s\n" , version.unixtime_a);       // Build timestamp in ASCII
    fprintf(term.f, "%s\n" , version.hostname); ;       // Host name of machine used for the build
    fprintf(term.f, "%s\n" , version.architecture);     // Architecture of the machine used for the build
    fprintf(term.f, "%s\n" , version.cc_version);       // C compiler version
    fprintf(term.f, "%s\n" , version.user_group);       // User and group that performed the build
    fprintf(term.f, "%s\n" , version.directory);        // Build directory
    fprintf(term.f, "--------------DSP--------------\n");

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_VERSION_UNIXTIME_A, DSP_SLOW_CMD_TIMEOUT_10MS) == 0)
    {
        fprintf(term.f, "%s\n" , dpram->dsp.slowRsp.ch);    // Build timestamp in ASCII
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_VERSION_HOSTNAME, DSP_SLOW_CMD_TIMEOUT_10MS) == 0)
    {
        fprintf(term.f, "%s\n" , dpram->dsp.slowRsp.ch);    // Host name of machine used for the build
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_VERSION_ARCHITECTURE, DSP_SLOW_CMD_TIMEOUT_10MS) == 0)
    {
        fprintf(term.f, "%s\n" , dpram->dsp.slowRsp.ch);    // Architecture of the machine used for the build
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_VERSION_CC_VERSION, DSP_SLOW_CMD_TIMEOUT_10MS) == 0)
    {
        fprintf(term.f, "%s\n" , dpram->dsp.slowRsp.ch);    // C compiler version
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_VERSION_USER_GROUP, DSP_SLOW_CMD_TIMEOUT_10MS) == 0)
    {
        fprintf(term.f, "%s\n" , dpram->dsp.slowRsp.ch);    // User and group that performed the build
    }

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_GET_VERSION_DIRECTORY, DSP_SLOW_CMD_TIMEOUT_10MS) == 0)
    {
        fprintf(term.f, "%s\n" , dpram->dsp.slowRsp.ch);    // Build directory
    }
    else
    {
        fprintf(term.f, RSP_DATA",DSP Failed by timeout\n");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuGeneralRunMain(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Manual Test
\*---------------------------------------------------------------------------------------------------------*/
{
    if(bootIsMainAllowed(false))
    {
        bootRunMain();
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTGeneralCheckBpType(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Manual Test
\*---------------------------------------------------------------------------------------------------------*/
{
    if (dev.crate_type != FGC_CRATE_TYPE_RECEPTION)
    {
        MenuRspError("FGC is not in a reception crate");
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuMTGeneralPeekWord(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Manual Test
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t i;
    uint16_t num_values = 1;
    uint32_t arg_address;

    // Get address argument

    if (MenuGetInt32U(argv[0], 0, 0xFFFFFFFF, &arg_address) != 0)
    {
        return;
    }

    // If address is not word aligned

    if ((arg_address & 0x00000001) != 0)
    {
        MenuRspError("Address is not word-aligned");
        return;
    }

    // Check optional num_values arg

    if (*argv[1] && MenuGetInt16U(argv[1], 1, 0xFFFF, &num_values))
    {
        return;
    }

    uint16_t * address = (uint16_t *)arg_address;

    fprintf(term.f, RSP_DATA);

    for (i = 0; i < num_values; i++)
    {
        // Print a maximum of 8 words per line

        if (   i % 8 == 0
            && i     != 0)
        {
            fprintf(term.f, "\n" RSP_DATA);
        }

        fprintf(term.f, ",0x%04X", *address++);
    }

    fprintf(term.f, "\n");
}



void MenuMTGeneralPokeWord(uint16_t argc, char ** argv)
{
    uint32_t i;
    uint16_t value;
    uint16_t num_values = 1;
    uint32_t address;

    if (MenuGetInt32U(argv[0], 0, 0x0FFFFFFF, &address) != 0)           // Get address argument
    {
        return;
    }

    if ((address & 0x00000001) != 0)                                    // If address is not word aligned
    {
        MenuRspError("Address is not word-aligned");
        return;
    }

    if (MenuGetInt16U(argv[1], 0, 0xFFFF, &value) != 0)                 // Get value
    {
        return;
    }

    if (*argv[2] && MenuGetInt16U(argv[2], 0, 0xFFFF, &num_values))     // Get optional num_values arg
    {
        return;
    }

    for (i = 0; i < num_values; i++, address += 2)
    {
        *(uint16_t *)address = value;
    }
}



void MenuMTGeneralPeekWordDsp(uint16_t argc, char ** argv)
{
    uint32_t i;
    uint16_t num_values = 1;
    uint32_t address;

    // Get address argument

    if (MenuGetInt32U(argv[0], 0, 0xFFFFFFFF, &address) != 0)
    {
        return;
    }

    // If address is not word aligned

    if ((address & 0x00000001) != 0)
    {
        MenuRspError("Address is not word-aligned");
        return;
    }

    // Check optional num_values arg

    if (*argv[1] && MenuGetInt16U(argv[1], 1, 0xFFFF, &num_values))
    {
        return;
    }

    fprintf(term.f, RSP_DATA);

    for (i = 0; i < num_values; i++, address += 4)
    {
        // Print a maximum of 4 words per line

        if (   i % 4 == 0
            && i     != 0)
        {
            fprintf(term.f, "\n" RSP_DATA);
        }

        dpram->mcu.slowArg.data = address;

        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_PEEK_WORD, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
        {
            MenuRspError("DSP rsp timeout.");
            return;
        }

        fprintf(term.f, ",0x%08lX", dpram->dsp.slowRsp.data);
    }

    fprintf(term.f, "\n");
}



void MenuMTGeneralPokeWordDsp(uint16_t argc, char ** argv)
{
    uint32_t i;
    uint32_t value;
    uint16_t num_values = 1;
    uint32_t address;

    // Get address argument

    if (MenuGetInt32U(argv[0], 0, 0xFFFFFFFF, &address) != 0)
    {
        return;
    }

    // If address is not word aligned

    if ((address & 0x00000001) != 0)
    {
        MenuRspError("Address is not word-aligned");
        return;
    }

    // Get value

    if (MenuGetInt32U(argv[1], 0, 0xFFFFFFFF, &value) != 0)
    {
        return;
    }

    // Get optional num_values arg

    if (*argv[2] && MenuGetInt16U(argv[2], 0, 0xFFFF, &num_values))
    {
        return;
    }

    for (i = 0; i < num_values; i++, address += 4)
    {
        dpram->mcu.slowArg.data = address;

        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_POKE_WORD_ADDRESS, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
        {
            MenuRspError("DSP rsp timeout.");
            return;
        }

        dpram->mcu.slowArg.data = value;

        if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_POKE_WORD_VALUE, DSP_SLOW_CMD_TIMEOUT_1S) != 0)
        {
            MenuRspError("DSP rsp timeout.");
            return;
        }
    }
}



void MenuMTGeneralCRC(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  Manual Test
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t address;
    uint32_t n_bytes;
    uint16_t crc;

    if ((argc != 2)
        || MenuGetInt32U(argv[0], 0, 0x0FFFFFFF, &address)
        || MenuGetInt32U(argv[1], 0, 0x0FFFFFFF, &n_bytes))
    {
        return;
    }

    crc = MemCrc16((void *) address, n_bytes / 2);
    fprintf(term.f, "$2,");
    fprintf(term.f, "CRC: 0x%04X\r\n", crc);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuGeneral.c
\*---------------------------------------------------------------------------------------------------------*/
