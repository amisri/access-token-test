/*---------------------------------------------------------------------------------------------------------*\
 File:      analog.c

 Purpose:   Analogue interface functions

\*---------------------------------------------------------------------------------------------------------*/

#define ANALOG_GLOBALS  // for ana_temp_regul global variable

#include <analog.h>

#include <stdint.h>
#include <iodefines.h>          // specific processor registers and constants
#include <stdio.h>              // for fprintf()

#include <boot_dsp_cmds.h>      // for DSP_CMD_*
#include <boot_dual_port_ram.h> // for dpram global variable
#include <codes.h>
#include <debug.h>              // for TEST_PLL_SWEEP
#include <derived_clocks.h>     // for StartAdcClock(), StartDacClock()
#include <dsp_cmds.h>           //
#include <dsp_loader.h>         // for dsp global variable
#include <main.h>               // for main_misc.timer_255us and for main_misc.timer_1ms used by USLEEP
#include <memmap_mcu.h>
#include <sharedMemory.h>      // for shared_mem global variable
#include <sleep.h>
#include <term.h>               // for term global variable

/*---------------------------------------------------------------------------------------------------------*/
enum FGC_ana_status AnalogInit(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function
   clear the analogue reset
   launches the ADCs tick
   launches the DACs tick

   Warning this doesn't work if the DSP tick is not enabled as these clocks are derived from the DSP tick
\*---------------------------------------------------------------------------------------------------------*/
{
    enum FGC_ana_status status = ANA_SUCCESS;

    // Release analogue reset signal
    // TODO Assert this?
    //  CPU_RESET_P &= ~CPU_RESET_CTRL_ANALOG_MASK16;   // this is already done at main.c

    StartAdcClock();
    StartActClock();

    // ANA_SWIN, INs connected to ADCs, SW_BUS bus disconnected, MPX disabled
    dpram->mcu.slowArg.adc_path.external_signal_access     = 0x0F;
    dpram->mcu.slowArg.adc_path.adc_selector_bus_or_signal = 0x00;
    dpram->mcu.slowArg.adc_path.internal_signal_bus_access = 0x00;

    if (SendDspSlowCmdAndWaitForRsp(DSP_CMD_SLOW_ADC_PATH, DSP_SLOW_CMD_TIMEOUT_1S))
    {
        fprintf(term.f, RSP_DATA ",DSP rsp timeout\r");

        if(status == ANA_SUCCESS)
        {
            status = ANA_DSP_TIMEOUT;
        }
    }

    return status;
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t WaitAndReadADCs(uint16_t number_of_samples)
/*---------------------------------------------------------------------------------------------------------*\
  This function launches the ADC acquisition and waits for it to finish
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  timeout;

    if (dsp.is_running == FALSE)
    {
        return (ADC_ERROR_NO_DSP);
    }

    switch (shared_mem.analog_card_model)
    {
        case FGC_INTERFACE_ANA_101:
        case FGC_INTERFACE_ANA_103:
        case FGC_INTERFACE_ANA_104:     // ANA_104 fall through
            // the DSP tick for the Boot is at 1KHz
            // so about 1 msec for each 6 conversions
            timeout = (number_of_samples / 6) + 1;

            // Check ADCs activity in DPRAM

            if (dpram->adc.n_samples != 0)
            {
                return (ADC_ERROR_BUSY);
            }

            // Acquire ADCs

            dpram->adc.n_samples  = number_of_samples; // Set number of samples to be acquired
            dpram->adc.reading_on = true;

            // Wait for end of conversion
            while (dpram->adc.n_samples != 0)
            {
#if defined(TEST_PLL_SWEEP)
                // refreshed at 625Khz = 1.6us
                PLL_DACVCXO_INT_P = 0; // 0 = -100ppm, 8192 = 0ppm, 16383 = +100ppm
                PLL_DACVCXO_FRAC_P = 0;
                USLEEP(1); // 50us
                PLL_DACVCXO_INT_P = 16383;
                PLL_DACVCXO_FRAC_P = 0;
                USLEEP(1); // 50us
#else
                USLEEP(1000); // 1ms
#endif
                timeout--;

                if ((timeout == 0) && (dpram->adc.n_samples != 0))       // n_samples can become 0 after the delay
                {
                    dpram->adc.n_samples  = 0;

                    return (ADC_ERROR_TIMEOUT);
                }
            }

            break;

        default:
            break;
    }

    return (ADC_ERROR_OK);
}



uint16_t waitAdcsReadProcess(uint32_t number_of_samples)
{
    uint32_t timeout;
    uint8_t  channel;

    if (dpram->adc.n_samples != 0)
    {
        return ADC_ERROR_BUSY;
    }

    // Timeout is based on ANA101 since it's the slowest with 1 sample from each channel each 100us.
    // ANA103 reads 50 samples each 100us.

    timeout = number_of_samples + 1;

    for (channel = 0; channel < FGC_N_ADCS; ++channel)
    {
        dpram->adc.sum[channel].int64s = 0;
        dpram->adc.max[channel]        = 0;
        dpram->adc.min[channel]        = 0;
    }

    dpram->adc.n_samples        = number_of_samples;
    dpram->adc.is_first_sample  = true;
    dpram->adc.reading_on       = true;


    while (dpram->adc.n_samples != 0 && timeout-- > 0)
    {
        USLEEP(100);
    }

    dpram->adc.n_samples = 0;

    if (timeout == 0)
    {
        return ADC_ERROR_TIMEOUT;
    }

    return ADC_ERROR_OK;
}



uint16_t waitAdcsTestProcess(uint32_t number_of_samples, enum Adc_test test)
{
    uint32_t timeout;

    if (dpram->adc.n_samples != 0)
    {
        return ADC_ERROR_BUSY;
    }

    // Timeout is based on ANA101 since it's the slowest with 1 sample from each channel each 100us.
    // ANA103 reads 50 samples each 100us.

    timeout               = number_of_samples + 1;
    dpram->adc.test       = test;
    dpram->adc.n_samples  = number_of_samples;
    dpram->adc.testing_on = true;


    while (dpram->adc.n_samples != 0 && timeout-- > 0)
    {
        USLEEP(100);
    }

    dpram->adc.n_samples = 0;

    if (dpram->adc.error.detected == true)
    {
        dpram->adc.error.detected = false;

        return ADC_ERROR_LIMITS;
    }

    if (timeout == 0)       // n_samples can become 0 after the delay
    {
        return ADC_ERROR_TIMEOUT;
    }

    return ADC_ERROR_OK;
}

// EOF
