/*---------------------------------------------------------------------------------------------------------*\
  File:     codes_mcu.c

  Purpose:  FGC Boot code update functions

  Notes:    These functions are shared by the FGC2 and FGC3 boot programs
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <codes_mcu.h>
#include <codes.h>
#include <dev.h>
#include <mcu_flash.h>
#include <mem.h>
#include <memmap_mcu.h>         // Include memory map consts
#include <menu.h>               // for menu global variable
#include <sharedMemory.h>      // for shared_mem global variable
#include <stdio.h>              // for fprintf(), NULL
#include <string.h>
#include <term.h>

/*---------------------------------------------------------------------------------------------------------*/
enum FGC_codes_status_internal CheckMcuDev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will check the code in an MCU flash device.
  It returns 0 if the code is valid, 1 otherwise.

  the codes_holder_info[device].dev structure is filled with the info from the code we already have in the FLASH

\*---------------------------------------------------------------------------------------------------------*/
{
    struct TCodesHolderInfo * const code_info = &codes_holder_info[dev_idx];

    if(code_info->update.code_id >= FGC_NUM_CODES)
    {
        code_info->dev_state = CODE_STATE_CORRUPTED;

        return(CODES_STATUS_INVALID_CODE_ID);
    }

    const uint32_t code_size      = (uint32_t)(fgc_code_sizes_blks[code_info->update.code_id] * FGC_CODE_BLK_SIZE);
    const uint32_t code_info_addr = code_info->base_addr + code_size - FGC_CODE_INFO_SIZE;

    // the code_info is stored after the "usable" data, appended at the end, by Code Delivery Software
    // so with the len we can calculate were it starts

    // fill the code_info structure with the data we have in the FLASH

    memcpy(&code_info->dev, (void *)code_info_addr, FGC_CODE_INFO_SIZE);

    // Make the string safe, in case not set

    code_info->dev.version_string[FGC_CODE_VERSION_STRING_LEN - 1] = '\0';

    if (code_info->dev.code_id >= FGC_NUM_CODES)
    {
        strncpy(code_info->dev.version_string, "            Invalid", FGC_CODE_VERSION_STRING_LEN - 1);
        code_info->dev_state = CODE_STATE_CORRUPTED;

        return(CODES_STATUS_INVALID_CODE_ID);
    }

    // calculate CRC

    code_info->calc_crc = MemCrc16((void *)code_info->base_addr, (code_size / 2) - 1);

    if(code_info->calc_crc == code_info->dev.crc)
    {
        code_info->dev_state = CODE_STATE_VALID;

        return(CODES_STATUS_SUCCESS);
    }
    else
    {
        code_info->dev_state = CODE_STATE_CORRUPTED;

        return(CODES_STATUS_INVALID_CRC);
    }
}

/*---------------------------------------------------------------------------------------------------------*/
enum FGC_codes_status_internal EraseMcuBootDev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
    we can not erase the boot here as it is the code we are running, we need to do it atomic at write
\*---------------------------------------------------------------------------------------------------------*/
{
    return (CODES_STATUS_SUCCESS);
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_codes_status_internal EraseMcuDev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
  This function will erase the storage device.
  If the erase fails, the device state is set to indicate DEV_FAILED and the function returns 1.
  Otherwise it returns zero.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t      block_address;
    uint32_t      end_address;
    uint32_t      len;

    block_address = codes_holder_info[dev_idx].base_addr;

    if (dev_idx == SHORT_LIST_CODE_MP_DSP)
    {
        switch (dev.class_id)
        {
            case 61:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_61].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 62:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_62].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 63:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_63].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 64:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_64].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 65:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_65].len_blks * FGC_CODE_BLK_SIZE;
                break;
            default:
                len = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE;
                break;
        }
    }
    else
    {
        len = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE; // Code length in bytes
    }

    end_address = block_address + len;

    while (block_address < end_address)
    {
        // block_address is modified by MCU_rom_BlockErase
        if (MCU_rom_BlockErase(&block_address) != FLASH_STATUS_SUCCESS)
        {
            codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;   // Set dev state to DEV_FAILED

            if (*menu.error_buf != 0)                       // If an error was reported
            {
                // Write error report
                fprintf(term.f, RSP_ERROR ",%s:%s\n", codes_holder_info[dev_idx].label, menu.error_buf);
                *menu.error_buf = '\0';                 // Clear error buffer
            }

            fprintf(term.f, RSP_ERROR ",Flash erase failed\n");

            return (CODES_STATUS_ERASE_FAILED);
        }
    }

    return (CODES_STATUS_SUCCESS);
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_codes_status_internal WriteMcuDev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    struct fgc_code_info    code_info;
    uint32_t                  len;
    const char       *      sram_buffer;
    uint16_t                  crc;
    enum FGC_flash_status   result;

    // the code info is stored after the "usable" data, appended at the end, by Code Delivery Software
    // so with the len we can calculate were it starts

    if (dev_idx == SHORT_LIST_CODE_MP_DSP)
    {
        switch (dev.class_id)
        {
            case 61:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_61].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 62:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_62].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 63:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_63].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 64:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_64].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 65:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_65].len_blks * FGC_CODE_BLK_SIZE;
                break;
            default:
                len = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE;
                break;
        }
    }
    else
    {
        len = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE; // Code length in bytes
    }

    sram_buffer = (char *) SRAM_TMPCODEBUF_32;

    /*
       I do a local copy of the code_info instead of just pointing to the structure
       as I need to convert crc to little endian without affecting the buffer which
       will be written to flash unmodified (and this remains very similar to CheckMcuDev and WriteMcuBootDev functions)
       and the CRC showed in the fprintf will be the correct one

       in previous version was used a pointer to code_info in the buffer and the calculated SRAM crc
       was converted to big endian but in case of mismatch the the showed with fprintf was mirrored
       (the bigendian printed as little endian)

    */

    // fill the code_info structure with the data we have in the buffer
    memcpy(&code_info, sram_buffer + len - FGC_CODE_INFO_SIZE, FGC_CODE_INFO_SIZE);

    // calculate CRC of SRAM buffer
    crc = MemCrc16(sram_buffer, (len / 2) - 1);

    if (code_info.crc != crc)
    {
        fprintf(term.f, RSP_DATA " CRC: expected 0x%04X calculated 0x%04X\n", code_info.crc, crc);
        fprintf(term.f, RSP_ERROR ",Abandoned due to SRAM buffer corruption\n");
        return (CODES_STATUS_INVALID_CRC);
    }

    // source is SRAM buffer (SRAM_TMPCODEBUF_32)
    result = MCU_rom_BlockWrite(SRAM_TMPCODEBUF_32, codes_holder_info[dev_idx].base_addr, len / 2);

    if (result != FLASH_STATUS_SUCCESS)
    {
        fprintf(term.f, RSP_ERROR ",Flash write failed :%04X\n", result);
        return (CODES_STATUS_WRITE_FAILED);
    }

    return (CODES_STATUS_SUCCESS);
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_codes_status_internal WriteMcuBootDev(enum fgc_code_ids_short_list dev_idx)
/*---------------------------------------------------------------------------------------------------------*\
    Atomic action Erase Write Reset of the boot
    this routine must be run in RAM can NOT access any ROM area (as the rom is being overwritten)
\*---------------------------------------------------------------------------------------------------------*/
{
    struct fgc_code_info    code_info;
    uint32_t                  len;
    const char       *      sram_buffer;
    uint16_t                  crc;

    // the code info is stored after the "usable" data, appended at the end, by Code Delivery Software
    // so with the len we can calculate were it starts

    if (dev_idx == SHORT_LIST_CODE_MP_DSP)
    {
        switch (dev.class_id)
        {
            case 61:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_61].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 62:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_62].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 63:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_63].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 64:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_64].len_blks * FGC_CODE_BLK_SIZE;
                break;
            case 65:
                len = (uint32_t)codes_mainprog_info[MAINPROG_LIST_65].len_blks * FGC_CODE_BLK_SIZE;
                break;
            default:
                len = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE;
                break;
        }
    }
    else
    {
        len = (uint32_t)codes_holder_info[dev_idx].len_blks * FGC_CODE_BLK_SIZE; // Code length in bytes
    }

    sram_buffer = (char *) SRAM_TMPCODEBUF_32;

    /*
       I do a local copy of the code_info instead of just pointing to the structure
       as I need to convert crc to little endian without affecting the buffer which
       will be written to flash unmodified (and this remains very similar to CheckMcuDev and WriteMcuDev functions)
       and the CRC showed in the fprintf will be the correct one

       in previous version was used a pointer to code_info in the buffer and the calculated SRAM crc
       was converted to big endian but in case of mismatch the the showed with fprintf was mirrored
       (the bigendian printed as little endian)

    */

    // fill the code_info structure with the data we have in the buffer
    memcpy(&code_info, sram_buffer + len - FGC_CODE_INFO_SIZE, FGC_CODE_INFO_SIZE);

    // calculate CRC of SRAM buffer
    crc = MemCrc16(sram_buffer, (len / 2) - 1);

    if (code_info.crc != crc)
    {
        fprintf(term.f, RSP_DATA " CRC: expected 0x%04X calculated 0x%04X\n", code_info.crc, crc);
        fprintf(term.f, RSP_ERROR ",Abandoned due to SRAM buffer corruption\n");
        return (CODES_STATUS_INVALID_CRC);
    }

    MCU_rom_FlashBoot(codes_holder_info[SHORT_LIST_CODE_BT].base_addr, len);

    // MCU_rom_FlashBoot erase this program, the interrupt vectors and jumps to software reset
    // so is almost impossible to return here

    fprintf(term.f, RSP_ERROR ",Programming %s abandoned due to FlashBoot() fail\n",
            codes_holder_info[dev_idx].label);

    codes_holder_info[dev_idx].dev_state = CODE_STATE_DEV_FAILED;

    return (CODES_STATUS_SUCCESS);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_mcu.c
\*---------------------------------------------------------------------------------------------------------*/
