/*---------------------------------------------------------------------------------------------------------*\
 File:      Pld.c

 Purpose:   FGC3 programmable logic device functions

 Notes:     This file contains functions that deal with PLDs

   12 jul 08    doc Created
\*---------------------------------------------------------------------------------------------------------*/

#define PLD_GLOBALS             // for pld_info global variable
#include <pld.h>
#include <iodefines.h>          // specific processor registers and constants

#include <codes.h>
#include <hardware_setup.h>     // for EnableTickInterrupt()
#include <interrupt_handlers.h>
#include <mcuDependent.h>
#include <memmap_mcu.h>         // for MID_PLDTYPE_P
#include <pld_spi.h>
#include <sleep.h>
#include <term.h>

/*---------------------------------------------------------------------------------------------------------*/
enum FGC_pld_status PldWaitForDONEandReadModel(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function reads PLD pins INIT and DONE and writes in pld_info variable the PLD its state

  After the reset (FPGA_PROG_B low) or powercycle, the line INIT_B goes low for less than 18 ms
  (PLD erasing its memory), then it goes high and in this moment it samples the mode pins.
  The DONE pin will go high at the very end of the configuration.

  When the PLD configures from internal SPI it takes about 109 ms

\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      timeout;

    pld_info.ok         = FALSE;    // by default
    pld_info.hw_model   = 0;
    pld_info.hw_version = 0;

    timeout = 1500;     // 1.5s configuration timeout (when configuring from internal SPI flash)

    while ((P8.PORT.BIT.B2 == 0) && (timeout != 0))   // P8.2 is FPGA_DONE
    {
        timeout--;
        USLEEP(1000); // 1ms
    }

    // P8.1 is FPGA_~INIT, FPGA_INIT_B
    // P8.2 is FPGA_DONE
    pld_info.state = P8.PORT.BIT.B2 | (P8.PORT.BIT.B1 << 1);

    // 11, PLD_STATE_INIT_AND_DONE_ARE_HI is the ok condition
    // 00, PLD_STATE_INIT_AND_DONE_ARE_LOW while programming

    //  if ( pld_info.state == PLD_STATE_INIT_AND_DONE_ARE_HI )
    //  {
    //  }

    if (timeout != 0)
    {
        pld_info.hw_model = MID_PLDTYPE_P;  // this only available after PLD successfully startup

        // 0x0700 = XC3S700AN or 0x1400 = XC3S1400AN are valid hardware models
        if ((pld_info.hw_model == 0x0700) || (pld_info.hw_model == 0x1400))
        {
            pld_info.hw_version = MID_PLDVER_P;
            pld_info.ok = TRUE;             // PLD is program with a valid code

              // PLD has a program in it

            return PLD_SUCCESS;
        }
    }

    return PLD_TIMEOUT;
}
/*---------------------------------------------------------------------------------------------------------*/
void PldReset(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will low the FPGA_PROG_B line reseting the PLD
  The mode pins must be setup previously to the desired mode

    // by default M2=0, so [M2 M1 M0]=011 --> FPGA boot from internal SPI
    P9.DR.BIT.B4  = 1;  // [M2 M1 M0]=111 --> FPGA boot in Slave Serial config mode


  After the reset (FPGA_PROG_B low) or powercycle, the line INIT_B goes low for less than 18 ms
  (PLD erasing its memory), then it goes high and in this moment it samples the mode pins.
  The DONE pin will go high at the very end of the configuration.

  When the PLD configures from internal SPI it takes about 109 ms

\*---------------------------------------------------------------------------------------------------------*/
{
    P7.DR.BIT.B6 = 1;   // disable digital CMD output

    // reset PLD by pulsing FPGA_PROG_B pin low

    P8.DDR.BIT.B0 = 1;  // P8.0, FPGA_~PROG, FPGA_PROG_B as output, was as input for safety
    P8.DR.BIT.B0  = 0;  // set FPGA_PROG_B low

    USLEEP(10);         // must remain at least 500ns in low

    P8.DR.BIT.B0  = 1;  // release FPGA_PROG_B pin
    // The mode select pins, M[2:0], are sampled when the FPGA_INIT_B output goes High and must be at
    // defined logic levels during this time.

    P8.DDR.BIT.B0 = 0;  // P8.0, FPGA_~PROG, FPGA_PROG_B back as input for safety
}
/*---------------------------------------------------------------------------------------------------------*/
enum FGC_pld_status PldBootISF(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will low the FPGA_PROG_B line reseting the PLD

    // by default M2=0, so [M2 M1 M0]=011 --> FPGA boot from internal SPI
    P15.4  = 1;         // [M2 M1 M0]=111 --> FPGA boot in Slave Serial config mode
\*---------------------------------------------------------------------------------------------------------*/
{
    // prepare PLD boot mode
    P9.DDR.BIT.B4 = 1;      // P9.4 as output, FPGA_M2, was as input for safety
    // by default M2=0, so [M2 M1 M0]=011 --> FPGA boot from internal SPI
    P9.DR.BIT.B4  = 0;      // [M2 M1 M0]=011 --> FPGA boot in internal SPI mode

    PldReset();

    //  After the reset (FPGA_PROG_B low) or powercycle, the line INIT_B goes low for less than 18 ms
    //  (PLD erasing its memory), then it goes high and in this moment it samples the mode pins.
    /*
        while ( ( P8.PORT.BIT.B1 == 0 ) && (timeout-- != 0) )   // P8.1 is FPGA_INIT_B
        {
            USLEEP(1000); // 1ms
        }
    */

    // The DONE pin will go high at the very end of the configuration.
    // When the PLD configures from internal SPI it takes about 109 ms

    enum FGC_pld_status status = PldWaitForDONEandReadModel();

    P9.DDR.BIT.B4 = 0;      // P9.4, FPGA_M2, back as input for safety
    return status;
}
/*---------------------------------------------------------------------------------------------------------*/
bool PldBootSlaveSerial(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function will low the FPGA_PROG_B line reseting the PLD

    // by default M2=0, so [M2 M1 M0]=011 --> FPGA boot from internal SPI
    P15.4  = 1;         // [M2 M1 M0]=111 --> FPGA boot in Slave Serial config mode
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      timeout = 50;
    bool     result;

    // prepare PLD boot mode
    P9.DDR.BIT.B4 = 1;      // P9.4 as output, FPGA_M2, was as input for safety
    // by default M2=0, so [M2 M1 M0]=011 --> FPGA boot from internal SPI
    P9.DR.BIT.B4  = 1;      // [M2 M1 M0]=111 --> FPGA boot in Slave Serial config mode

    // The intelligent host starts the configuration process by pulsing PROG_B and monitoring
    // that the INIT_B pin goes High, indicating that the FPGA is ready to receive its first data.
    PldReset();

    //  After the reset (FPGA_PROG_B low) or powercycle, the line INIT_B goes low for less than 18 ms
    //  (PLD erasing its memory), then it goes high and in this moment it samples the mode pins.

    while ((P8.PORT.BIT.B1 == 0) && (timeout-- != 0))       // P8.1 is FPGA_INIT_B
    {
        USLEEP(1000); // 1ms
    }

    result = TRUE;          // by default

    if (timeout == 0)
    {
        result = FALSE;     // PLD boot problem
    }

    P9.DDR.BIT.B4 = 0;      // P9.4, FPGA_M2, back as input for safety
    return (result);
}
/*---------------------------------------------------------------------------------------------------------*/
void PldFeedBitstreamViaSlaveSerial(uint8_t * bitstream_p, uint32_t len)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends the configuration code to the PLD in slave serial mode.
  It is assumed that the INIT_B pin was monitored and it is high.


    The intelligent host starts the configuration process by pulsing PROG_B [PldReset()] and monitoring
    that the INIT_B pin goes High, indicating that the FPGA is ready to receive its first data.

    The host then continues supplying data and clock signals until either the DONE pin goes
    High, indicating a successful configuration, or until the INIT_B pin goes Low, indicating a
    configuration error. The configuration process requires more clock cycles than indicated
    from the configuration file size. Additional clocks are required during the FPGA s start-up
    sequence, especially if the FPGA is programmed to wait for selected Digital Clock
    Managers (DCMs) to lock to their respective clock inputs

\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t   data;
    uint8_t   bit_mask;
    bool problem_found;
    uint32_t  ii;


    // prepare serial link
    P9.DDR.BIT.B6 = 1;  // P9.6 as output, FPGA_CCLK, was as input for safety

    P9.DR.BIT.B6 = 0;   // P9.6, FPGA_CCLK

    P9.DDR.BIT.B5 = 1;  // P9.5 as output, FPGA_DIN, was as input for safety

    // Send the whole configuration
    problem_found = FALSE;
    ii = 0;

    while ((ii < len) && (problem_found == FALSE))  // size of PLD configuration bitstream in bytes
    {
        data = *(bitstream_p + ii++);

        // Send 1 byte, MSB first
        bit_mask = 0x80;

        while ((bit_mask != 0) && (problem_found == FALSE))             // Loop on 8 bits
        {
            // [!!] trick to pass from any bit position to 0 or 1
            P9.DR.BIT.B5 = !!(data & bit_mask) ;    // P9.5 is FPGA_DIN, Set bit on DIN pin
            P9.DR.BIT.B6 = 0;                       // P9.6, FPGA_CCLK
            P9.DR.BIT.B6 = 1;                       // P9.6, FPGA_CCLK
            bit_mask >>= 1;                         // shift bit mask

            // check for problems
            if (
                //               ( P8.PORT.BIT.B2 != 0 ) ||     // P8.2 is FPGA_DONE, successful configuration, how? before the end
                (P8.PORT.BIT.B1 == 0)          // P8.1 is FPGA_INIT_B, configuration error
            )
            {
                problem_found = TRUE;
            }
        }
    }

    // Extra PLD config clocks
    for (ii = 0; ii < 16; ii++)
    {
        P9.DR.BIT.B6 = 0;   // P9.6, FPGA_CCLK
        P9.DR.BIT.B6 = 1;   // P9.6, FPGA_CCLK

        if (P8.PORT.BIT.B1 == 0)        // P8.1 is FPGA_INIT_B, configuration error
        {
            problem_found = TRUE;
        }
    }

    // deconfig back serial link to PLD

    P9.DDR.BIT.B6 = 0;      // set P9.6, FPGA_CCLK, back as input for safety
    P9.DDR.BIT.B5 = 0;      // set P9.5, FPGA_DIN, back as input for safety
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: Pld.c
\*---------------------------------------------------------------------------------------------------------*/
