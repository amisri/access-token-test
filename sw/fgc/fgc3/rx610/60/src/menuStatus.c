/*---------------------------------------------------------------------------------------------------------*\
  File:         menuStatus.c

  Purpose:      Functions to run for the general boot menu options

  Author:       daniel.calcoen@cern.ch

  Notes:

  History:

    13/04/04    stp     Created
    22/11/06    pfr     Ported for FGC3 maquette
\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>           // basic typedefs
#include <stdio.h>              // for fprintf()
#include <defconst.h>           // for FGC_PSU_MAX_15V
#include <fieldbus_boot.h>      // for stat_var global variable
#include <label.h>              // for BitLabel()
#include <main.h>               // for faults_lbl[], reset_source_lbl, power global variable
#include <memmap_mcu.h>         // Include memory map consts
#include <menu.h>               // for MenuRspArg()
#include <read_PSU_adc.h>       // for psu global variable
#include <term.h>

/*---------------------------------------------------------------------------------------------------------*/
void MenuSTSpsuVolts(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
   Menu Device Status - PSU Voltages
\*---------------------------------------------------------------------------------------------------------*/
{
    fprintf(term.f, RSP_DATA ",%6.2f [%5.2f,%5.2f]\n", (double) psu.v5,   FGC_PSU_MIN_5V,  FGC_PSU_MAX_5V);
    fprintf(term.f, RSP_DATA ",%6.2f [%5.2f,%5.2f]\n", (double) psu.vp15, FGC_PSU_MIN_15V, FGC_PSU_MAX_15V);
    fprintf(term.f, RSP_DATA ",%6.2f [%5.2f,%5.2f]\n", (double) psu.vm15, -FGC_PSU_MAX_15V, -FGC_PSU_MIN_15V);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTSshowResets(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
   Menu Device Status - Show Resets
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t      rst;


    rst = CPU_RESET_SRC_P;

    MenuRspArg("0x%04X", rst);

    BitLabel(reset_source_lbl, rst);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTSfaults(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
   Menu Device Status - Faults
\*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("0x%04X", stat_var.fgc_stat.class_data.c60.st_faults);
    BitLabel(faults_lbl, stat_var.fgc_stat.class_data.c60.st_faults);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTSwarnings(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
   Menu Device Status - Warnings
\*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("0x%04X", stat_var.fgc_stat.class_data.c60.st_warnings);
    BitLabel(warnings_lbl, stat_var.fgc_stat.class_data.c60.st_warnings);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuSTSlatchedStatus(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
   Menu Device Status - Latched Status
\*---------------------------------------------------------------------------------------------------------*/
{
    MenuRspArg("0x%04X", stat_var.fgc_stat.class_data.c60.st_latched);
    BitLabel(st_latched_lbl, stat_var.fgc_stat.class_data.c60.st_latched);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuStatus.c
\*---------------------------------------------------------------------------------------------------------*/
