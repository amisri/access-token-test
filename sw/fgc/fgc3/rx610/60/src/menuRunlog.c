/*---------------------------------------------------------------------------------------------------------*\
  File:     menuRunlog.c

  Purpose:  Functions for the Run Log submenu

\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdio.h>              // for fprintf(), fputs(), NULL
#include <term.h>
#include <menu.h>               // for MenuRspError()
#include <fgc_runlog.h>         // the item structure for the run log buffer and FGC_RUNLOG_MAX_DATA_LEN
#include <fgc_runlog_entries.h> // Include run log definitions
#include <main.h>               // for main_misc.runlog_show_index
#include <dev.h>                // for dev.abort_f
#include <runlog_lib.h>         // for RunlogRead()
#include <iodefines.h>          // processor specific registers and constants
#include <mcuDependent.h>      // for NVRAM_UNLOCK()
#include <memmap_mcu.h>         // for NVRAM_RL_BUF_32

/*---------------------------------------------------------------------------------------------------------*/
uint16_t DisplayRunlogEntry(uint16_t index, uint8_t * id_p)
/*---------------------------------------------------------------------------------------------------------*\
  Get and show one runlog entry and return the id in *id if defined.
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t              previous;
    struct fgc_runlog   runlog_entry;

    // if index is out of range it can be adjusted in RunlogRead()
    previous = RunlogRead(&index, &runlog_entry);

    if (id_p != NULL)
    {
        *id_p = runlog_entry.id;                // return id via the pointer
    }

    // display record
    fprintf(term.f, RSP_DATA ",[%02X] seq:%02X id:%02X ", index, runlog_entry.seq, runlog_entry.id);

    switch (fgc_rl_length[runlog_entry.id])
    {
        case 0:         // No data

            fputs("        ", term.f);
            break;

        case 1:         // One word

            fprintf(term.f, "%04X    ", runlog_entry.data[0]);
            break;

        case 2:         // Two words

            fprintf(term.f, "%04X%04X", runlog_entry.data[0], runlog_entry.data[1]);
            break;
    }

    fprintf(term.f, " %s\n", fgc_rl_label[runlog_entry.id]);

    return (previous);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogClear(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Run log] clear run log memory area
\*---------------------------------------------------------------------------------------------------------*/
{
    struct TRunlog     *    runlog_ptr;


    runlog_ptr = (struct TRunlog *) NVRAM_RL_BUF_32;

    NVRAM_UNLOCK();
    runlog_ptr->magic = 0;      // Wipe magic number
    RunlogInit();
    NVRAM_LOCK();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogZeroResetsCounters(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Run log] clear reset counters
\*---------------------------------------------------------------------------------------------------------*/
{
    RunlogClrResets();
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShowResets(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Run log] Show reset counters
\*---------------------------------------------------------------------------------------------------------*/
{
    struct TRunlog   *  runlog_ptr;

    runlog_ptr = (struct TRunlog *) NVRAM_RL_BUF_32;

    MenuRspArg("%u", runlog_ptr->resets.all);
    MenuRspArg("%u", runlog_ptr->resets.power);
    MenuRspArg("%u", runlog_ptr->resets.program);
    MenuRspArg("%u", runlog_ptr->resets.manual);
    MenuRspArg("%u", runlog_ptr->resets.dongle);
    MenuRspArg("%u", runlog_ptr->resets.fast_watchdog);
    MenuRspArg("%u", runlog_ptr->resets.slow_watchdog);
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShow10(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Run log] show 10 entries from newer to older
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  ii;

    for (ii = 0; ii < 10; ii++)
    {
        main_misc.runlog_show_index = DisplayRunlogEntry(main_misc.runlog_show_index, NULL);

        if (main_misc.runlog_show_index == FGC_RUNLOG_N_ELS) // no more items
        {
            break;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShowUntilReset(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Run log] display the entries from newer to older until a FGC_RL_RESETS is reach
  or the bottom of the buffer was reach
\*---------------------------------------------------------------------------------------------------------*/
{
    uint8_t   id = 0;

    while (id != FGC_RL_RESETS)
    {
        main_misc.runlog_show_index = DisplayRunlogEntry(main_misc.runlog_show_index, &id);

        if (main_misc.runlog_show_index == FGC_RUNLOG_N_ELS) // no more items
        {
            return;
        }

        if (dev.abort_f)
        {
            MenuRspError("Aborted by user");
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShowFromLast(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Run log] starting from last used show 10 entries
\*---------------------------------------------------------------------------------------------------------*/
{
    // force index out of the buffer, so RunlogRead() will adjust to the last used
    main_misc.runlog_show_index = FGC_RUNLOG_N_ELS;
    MenuRunlogShow10(argc, argv);       // Display 10 entries
}
/*---------------------------------------------------------------------------------------------------------*/
void MenuRunlogShowAll(uint16_t argc, char ** argv)
/*---------------------------------------------------------------------------------------------------------*\
  [Run log] Show the complete runlog buffer
\*---------------------------------------------------------------------------------------------------------*/
{
    uint16_t  ii;

    for (ii = 0; ii < FGC_RUNLOG_N_ELS; ii++)
    {
        DisplayRunlogEntry(ii, NULL);

        if (dev.abort_f == true)
        {
            MenuRspError("Aborted by user");
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menuRunlog.c
\*---------------------------------------------------------------------------------------------------------*/
