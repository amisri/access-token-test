/*---------------------------------------------------------------------------------------------------------*\
  File:     dsp_cmds.c

  Purpose:  Start, stop the Dsp (tms320c6727) boot test program.

  Author:   daniel.calcoen@cern.ch

  History:

    25 jul 07   doc Created
     2 oct 10   doc revisited
\*---------------------------------------------------------------------------------------------------------*/

#include <dsp_cmds.h>

#include <stdint.h>           // basic typedefs
#include <iodefines.h>          // specific processor registers and constants
#include <mcuDependent.h>      // for MSLEEP(), USLEEP()
#include <main.h>               // for main_misc global variable
#include <boot_dual_port_ram.h> // for dpram global variable
#include <boot_dsp_cmds.h>      // constants shared by DSP and CPU
#include <sleep.h>

/*---------------------------------------------------------------------------------------------------------*/
uint16_t SendDspSlowCmdAndWaitForRsp(uint16_t cmd, uint16_t timeout)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends cmds & args to Dsp through the [PLD]DPRAM.
  DSP runs the functions at background level
\*---------------------------------------------------------------------------------------------------------*/
{
    // Write command
    dpram->mcu.slowCmd = cmd;

    // wait for DSP to answer the CMD
    while (dpram->mcu.slowCmd != DSP_CMD_NOTHING)
    {
        if (!timeout--)
        {
            return (1);
        }

        USLEEP(1000); // 1ms
    }

    return (0);
}
/*---------------------------------------------------------------------------------------------------------*/
uint16_t SendDspFastCmdAndWait110us(uint16_t cmd, uint32_t arg)
/*---------------------------------------------------------------------------------------------------------*\
  This function sends cmds & args to Dsp through the DPRAM. DSP runs the functions at interrupt level
\*---------------------------------------------------------------------------------------------------------*/
{
    dpram->mcu.fastArg.data = arg;

    // Write command
    dpram->mcu.fastCmd = cmd;

    USLEEP(110);        // Wait for 1 DSP tick (if 10kHz)

    return (dpram->dsp.fastRsp.data);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_cmds.c
\*---------------------------------------------------------------------------------------------------------*/
