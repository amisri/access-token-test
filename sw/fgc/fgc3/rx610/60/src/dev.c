/*!
 * @file  dev.c
 */

#include <stdint.h>
#include <stdbool.h>

#include <dev.h>
#include <pld_spi.h>
#include <defsyms.h>
#include <runlog_lib.h>
#include <iodefines.h>



static inline void DevSetRevision(void)
{
    dev.revision = P7.PORT.BIT.B1 + (P7.PORT.BIT.B2 << 1) + (P7.PORT.BIT.B3 << 2);
}



enum FGC_dev_status DevSetPldHwVersion(void)
{
    DevSetRevision();

    switch(dev.revision)
    {
        case 1:
        case 3:

            dev.pld_hw_model = XC3S700AN;
            break;

        case 2:

            dev.pld_hw_model = XC3S1400AN;
            break;

        default:

            return DEV_UNKNOWN_PLD_HW;
    }

    return DEV_SUCCESS;
}



void DevSetCrateType(const bool pld_is_ok)
{
    if(pld_is_ok)
    {
        // Get the crate type (the bus type is routed through the PLD)

        dev.crate_type = MID_CRATETYPE_P;

        // Look for the crate type index in symlist

        uint16_t crate_type_idx = 0;

        while(sym_names_crate_type[crate_type_idx].label != 0)
        {
            if(dev.crate_type == sym_names_crate_type[crate_type_idx].type)
            {
                break;
            }

            crate_type_idx++;
        }

        if(dev.crate_type == sym_names_crate_type[crate_type_idx].type)
        {
            dev.crate_type_idx = crate_type_idx;
        }
        else
        {
            dev.crate_type_idx = FGC_CRATE_TYPE_UNKNOWN;
        }
    }
    else
    {
        dev.crate_type     = FGC_CRATE_TYPE_UNKNOWN;
        dev.crate_type_idx = 0;
    }
}



void DevSetResetSource(const bool pld_is_ok)
{
    if(pld_is_ok)
    {
        // get reset source from PLD

        dev.rst_src = (CPU_RESET_SRC_P & (CPU_RESET_SRC_POWER_MASK16   |
                                          CPU_RESET_SRC_PROGRAM_MASK16 |
                                          CPU_RESET_SRC_MANUAL_MASK16  |
                                          CPU_RESET_SRC_DONGLE_MASK16  |
                                          CPU_RESET_SRC_FASTWD_MASK16  |
                                          CPU_RESET_SRC_SLOWWD_MASK16));

        // Signal fast and slow watchdog events in the run log

        if (dev.rst_src & CPU_RESET_SRC_FASTWD_MASK16)
        {
            uint16_t counter = NVRAM_RL_RESETS_FASTWD_P;

            RunlogWrite(FGC_RL_RESET_FAST_WD, &counter);
        }

        // Slow watchdog

        if (dev.rst_src & CPU_RESET_SRC_SLOWWD_MASK16)
        {
            uint16_t counter = NVRAM_RL_RESETS_SLOWWD_P;

            RunlogWrite(FGC_RL_RESET_SLOW_WD, &counter);
        }
    }
    else
    {
        // Unknown reset source because PLD not programmed

        dev.rst_src = 0;
    }
}

// EOF
