/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp_cmds.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DSP_CMDS_H      // header encapsulation
#define DSP_CMDS_H

#ifdef DSP_CMDS_GLOBALS
#define DSP_CMDS_VARS_EXT
#else
#define DSP_CMDS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>   // basic typedefs

//-----------------------------------------------------------------------------------------------------------

#define DSP_SLOW_CMD_TIMEOUT_10MS               10
#define DSP_SLOW_CMD_TIMEOUT_100MS              100
#define DSP_SLOW_CMD_TIMEOUT_1S                 1000
#define DSP_SLOW_CMD_TIMEOUT_5S                 5000
#define DSP_SLOW_CMD_TIMEOUT_20S                20000
#define DSP_SLOW_CMD_TIMEOUT_30S                30000

//-----------------------------------------------------------------------------------------------------------

uint16_t  SendDspSlowCmdAndWaitForRsp(uint16_t cmd, uint16_t timeout);
uint16_t  SendDspFastCmdAndWait110us(uint16_t cmd, uint32_t arg);

//-----------------------------------------------------------------------------------------------------------

#endif  // DSP_CMDS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_cmds.h
\*---------------------------------------------------------------------------------------------------------*/
