/*---------------------------------------------------------------------------------------------------------*\
  File:         term.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef TERM_H  // header encapsulation
#define TERM_H

#ifdef TERM_GLOBALS
#define TERM_VARS_EXT
#define TERM_VARS_EXT_STATIC        static
#else
#define TERM_VARS_EXT               extern
#define TERM_VARS_EXT_STATIC        extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>   // basic typedefs
#include <stdio.h>      // for FILE
#include <definfo.h>    // for FGC_CLASS_ID

//-----------------------------------------------------------------------------------------------------------

enum FGC_term_edit_state
{
    TERM_STATE_DIRECT,
    TERM_STATE_INTERACTIVE,
    TERM_STATE_ESC,
    TERM_STATE_CURSOR_FUNCTION,
    TERM_STATE_FUNCTION,
    TERM_STATE_PF,
};

#define     TERM_TX                 term.f          // FILE stream

#if (FGC_CLASS_ID == 60)                // FGC3 boot
#define     TERM_RX_BUF_SIZE        8192            // Terminal rx buffer sizes
#else
#define     TERM_RX_BUF_SIZE        256             // Terminal rx buffer sizes
#endif

#define TERM_LINE_SIZE                  80              // Length of terminal line for line editor
#define TERM_TX_BUF_SIZE                256             // Terminal tx buffer sizes
#define TERM_XOFF_TIMEOUT_MS            60000           // Xoff timeout in milliseconds (60s)

//----- Boot response header constants -----

#define RSP_ID_NAME                     "$0,%s,%s"      // $0,{NODE_ID},{NODE_NAME}
#define RSP_PROGRESS                    "$1"            // $1,{% completed}
#define RSP_DATA                        "$2"            // $2,{DATA},{DATA},...
#define RSP_OK                          "$3"            // $3
#define RSP_ERROR                       "$4"            // $4,{ERROR MSG}
#define RSP_LABEL                       "$5"            // $5,{LABEL}

//#define IFTERM                        if(term.edit_state)

#define TERM_RESET                      "\r      \33c"          // 0D 20 20 20 20 20 20 1B 63
#define TERM_INIT                       "\33[2J  \a \33[?7h \r"
#define TERM_BOLD                       "\33[1m"
#define TERM_REVERSE                    "\33[7m"
#define TERM_UL                         "\33[4m"
#define TERM_NORMAL                     "\33[0m"
#define TERM_CLR_LINE                   "\33[2K"

//-----------------------------------------------------------------------------------------------------------

// ---- Terminal structures ----

struct term_tx_buf                                         // circular buffer
{
    volatile uint16_t     n_ch;                           // Number of characters in the buffer
    // volatile because is used in a while() (TermTxCh/TermRxFlush) and at that moment is externally
    // modified by the interrupt TermProcessHardware, if it is not volatile the compiler will get ride off it
    uint8_t               in;                             // Buffer input index
    uint8_t               out;                            // Buffer output index
    char                buf[TERM_TX_BUF_SIZE];
};

struct term_rx_buf                                         // circular buffer
{
    volatile uint16_t     n_ch;                           // Number of characters in the buffer
    // volatile because is used in a while() (SciTxCh/SciRxFlush) and at that moment is externally
    // modified by the interrupt DoTerminalHardware, if it is not volatile the compiler will get ride off it
    uint16_t              in;                             // Buffer input index
    uint16_t              out;                            // Buffer output index
    char                buf[TERM_RX_BUF_SIZE];
};

struct term
{
    FILE        *       f;                              // Pointer to buffered IO stream
    struct term_tx_buf  tx;                             // Transmission buffer
    struct term_rx_buf  rx;                             // Reception buffer

    uint16_t              xoff_timeout;                   // Xoff timeout flag (ms)

    uint16_t              recv_args_f;                    // Terminal input is active
    uint16_t              recv_cmd_f;                     // Start of comamnd ($) seen
    enum FGC_term_edit_state edit_state;
    char                linebuf[TERM_LINE_SIZE];
    uint16_t              line_end;
    uint16_t              line_idx;
};

//-----------------------------------------------------------------------------------------------------------

void    TermEnter(void);
void    TermInteractive(void);
void    TermDirect(void);
enum FGC_term_edit_state TermLE0(char ch);
enum FGC_term_edit_state TermLE1(char ch);
enum FGC_term_edit_state TermLE2(char ch);
enum FGC_term_edit_state TermLE3(char ch);
enum FGC_term_edit_state TermLE4(char ch);
enum FGC_term_edit_state TermLE5(char ch);
uint16_t  TermInsertChar(char ch);
void    TermCursorLeft(void);
void    TermCursorRight(void);
void    TermStartOfLine(void);
void    TermEndOfLine(void);
void    TermDeleteLeft(void);
void    TermDeleteRight(void);
void    TermShiftRemains(void);

#if (FGC_CLASS_ID == 60)                // FGC3 boot
// ToDo put in a seprate file for 60 only
struct menu_node;       // Forward declaration of menu_node
void    TermGetMenuOption(struct menu_node * node);
void    TermRunFuncWithArgs(struct menu_node * node);
#endif
//-----------------------------------------------------------------------------------------------------------

// ToDo: warning there is another edit_func[] in trm.c
// TODO Add static_assert to match with edit state enum
TERM_VARS_EXT_STATIC enum FGC_term_edit_state (* const edit_func[])(char)                         // Line editor functions
#ifdef TERM_GLOBALS
=
{
    TermLE0,            // State 0 function (Direct mode)
    TermLE1,            // State 1 function (Interactive mode)
    TermLE2,            // State 2 function (ESC pressed)
    TermLE3,            // State 3 function (Cursor key or function key)
    TermLE4,            // State 4 function (Function key)
    TermLE5,            // State 5 function (PF1-PF4)
}
#endif
;

TERM_VARS_EXT struct term   term;

//-----------------------------------------------------------------------------------------------------------

#endif  // TERM_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: term.h
\*---------------------------------------------------------------------------------------------------------*/

