/*!
 *	@file 	memTestMcu.h
 *	@brief	Memory test function for the MCU
 */

#pragma once


// Includes

#include <stdint.h>



// Constants

#define MEM_TEST_MCU_MAX_BLOCK_SIZE 1024



// External function declarations

/*!
 *	Performs a memory test on the specified memory block.
 *	The test consists of 3 parts: data bus test, address bus test and device test.
 *
 *	@param[in]	base_address	Address of the memory block.
 *	@param[in]	num_bytes		Memory block size.
 *
 *	@retval		0				The test was successful.
 *	@retval     -1				The test failed.
 */
int memTestMcu(volatile uint32_t * base_address, uint32_t num_bytes);


// EOF
