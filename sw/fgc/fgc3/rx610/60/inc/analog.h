/*---------------------------------------------------------------------------------------------------------*\
  File:     analog.h

  Purpose:  FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef ANALOG_H    // header encapsulation
#define ANALOG_H

#ifdef ANALOG_GLOBALS
#define ANALOG_VARS_EXT
#else
#define ANALOG_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <label.h>              // for struct sym_name

#include <boot_dual_port_ram.h>

enum FGC_ana_status
{
    ANA_SUCCESS,
    ANA_DSP_TIMEOUT,
};

//-----------------------------------------------------------------------------------------------------------

#define  ADC_RAW_REF_TOL            0x00043A37  // 5%
#define  ADC_RAW_PEEKTOPEEK_TOL     0x000006B0  // 200uV, peek to peek

// On ANA-101 the input of the ADCs are inverted, at the DSP the values
// are inverted after reading ANA_ADCS_A[] so this raw values must be inverted if used
// with already processed data

/*
    // these are raw ADC values (direct read)
    //#define  ADC_RAW_FULL_FIR         0x0C7FFFFE  // HI 32 bits of the 64 available on the ADC

    // after the shift of preprocess done in the DSP
    #define  ADC_RAW_FULL_FIR           0x0000C7FF  // HI 32 bits of the 64 available on the ADC
*/
// these are raw ADC values (after inverting) and the shift of preprocess done in the DSP
#define  ADC_RAW_FULL_FIR           0xFFFF3800  // HI 32 bits of the 64 available on the ADC

//------------------ Analogue interface constants --------------------

#define  ANA_TR_ADC_GAIN        14.57       // 0.047 * 1023 / 3.3
#define  ANA_TR_PROP_GAIN       400
#define  ANA_TR_INT_GAIN        7
#define  ANA_TR_DIF_GAIN        20

#define  ADC_ERROR_OK           0
#define  ADC_ERROR_BUSY         1
#define  ADC_ERROR_TIMEOUT      2
#define  ADC_ERROR_NO_DSP       3
#define  ADC_ERROR_LIMITS       4

#define FIR_MAX_LENGTH          16000
#define FIR_ORDER               4

//-----------------------------------------------------------------------------------------------------------

enum FGC_ana_status AnalogInit(void);
uint16_t  WaitAndReadADCs(uint16_t number_of_samples);
uint16_t waitAdcsReadProcess(uint32_t number_of_samples);
uint16_t waitAdcsTestProcess(uint32_t number_of_samples, enum Adc_test test);

//-----------------------------------------------------------------------------------------------------------
//  Analogue variable

// ToDo share this with main
ANALOG_VARS_EXT const float fir_factor[FIR_ORDER]  // fir factors
#ifdef ANALOG_GLOBALS
    = { 1.000,
        0.736,
        0.206,
        0.058
      }
#endif
      ;
ANALOG_VARS_EXT const float
adc_bitrate_mhz_map[16]  // Link between bitrate register value and bitrate value in mHz
#ifdef ANALOG_GLOBALS
    = { 8.000,
        0.675,
        0.100,
        0.500,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
      }
#endif
      ;


/*
For analog card ANA-100
#define  ANA_REF_POS            0x05190000      // +10v
#define  ANA_REF_ZERO           0               //   0v
#define  ANA_REF_NEG            0xFAE70000      // -10v

For analog card ANA-101
#define  ANA_REF_POS            0xFFFFD7AD      // +10v
#define  ANA_REF_ZERO           0               //   0v
#define  ANA_REF_NEG            0x00002850      // -10v
*/
ANALOG_VARS_EXT const uint32_t  adc_expected_raw_hex_for[2][3]
#ifdef ANALOG_GLOBALS
=
{
    // ANA_101
    {
        0,          //   0v
        0x00548C57, // +10v
        0xFFAB73A9  // -10v
    },

    // ANA_103
    {
       0,          //   0v
       0x015E5500, // +10v
       0xFEA1A440  // -10v
    }
}
#endif
;

#define DAC_MIN         (-12.5)
#define DAC_MAX         (12.5)
#define DAC_RANGE       0xFFFFF
#define DAC_RESOLUTION  20

/*
#define  DAC_RAW_HEX_FOR_pos10v 0xE665      // 0xFFFF corresponds to  12.5V
#define  DAC_RAW_HEX_FOR_0v     0x8000      // 0x0000 corresponds to -12.5V
#define  DAC_RAW_HEX_FOR_neg10v 0x1999

 the DAC is the MAX5541CSA, 16 bits

 the DAC is unipolar (0 to 2.5v)
 but later the 0v is shifted to the middle of the range so we end with

 FFFF    max positive (+12.5v)
 E665    +10v                  .... +50A
 8000    +0v
 7FFF    -0v
 1999    -10v                  .... -50A
 0000    max negative (-12.5v)

 but the PLD inverts the MSB so from the MCU we see

 7FFF    max positive (+12.5v)
 6665    +10v                  .... +50A
 0000    +0v
 FFFF    -0v
 9999    -10 v                  .... -50A
 8000    max negative (-12.5v)


2'scom  inMeM    PLD(Not b15)     True dac
+7FFF   7FFF     FFFF              FFFF    max positive (+12.5v)
+6665   6665     E665              E665    +10v                  .... +50A
+0000   0000     8000              8000    +0v
-0001   FFFF     7FFF              7FFF    -0v
-6666   999A     199A              199A    -10v                  .... -50A
-7FFF   8000     0000              0000    max negative (-12.5v)

for the end user looks like

 7FFF    max positive (+12.5v)
 6665   +10v                  .... +50A
 0000    +0v
 FFFF    -0v
 9999   -10v                  .... -50A
 8000    max negative (-12.5v)


ToDo: share this
these values are duplicated and used at ConvertCurrentToDACcount() in adc.c in the DSP program

*/

ANALOG_VARS_EXT const uint32_t dac_raw_hex_for[3]  // DAC values corresponding to an output for 0v, +10v, -10v
#ifdef ANALOG_GLOBALS
=   {
        0x00000,     //   0v
        0x66665,     // +10v
        0x99999      // -10v
    }
#endif
;

ANALOG_VARS_EXT const struct sym_name            adc_errors_str[]
#ifdef ANALOG_GLOBALS
        =
{
    { ADC_ERROR_OK,      "Ok"      }, // 00
    { ADC_ERROR_BUSY,    "Busy"    }, // 01
    { ADC_ERROR_TIMEOUT, "Timeout" }, // 02
    { ADC_ERROR_NO_DSP,  "No DSP"  }, // 03
    { 0                            }  // the label part == 0  marks the end of the table
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // ANALOG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: analog.h
\*---------------------------------------------------------------------------------------------------------*/
