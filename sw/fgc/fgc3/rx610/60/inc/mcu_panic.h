/*---------------------------------------------------------------------------------------------------------*\
  File:         mcu_panic.h

  Purpose:      FGC3 MCU Software - Panic functions
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MCU_PANIC_H   // Header encapsulation
#define MCU_PANIC_H

#ifdef MCU_PANIC_GLOBALS
#define MCU_PANIC_FGC_EXT
#else
#define MCU_PANIC_FGC_EXT extern
#endif

#include <stdint.h>

/*---------------------------------------------------------------------------------------------------------*/

// DSP panic codes

enum mcu_panic_codes
{
    MCU_PANIC_EXCEP_SUPERVISOR_INSTR    = 0x10000000,
    MCU_PANIC_EXCEP_INVALID_INSTR       = 0x10000001,
    MCU_PANIC_EXCEP_FLOATING_POINT      = 0x10000002,
    MCU_PANIC_EXCEP_NMI                 = 0x10000003,
    MCU_PANIC_EXCEP_BRK                 = 0x10000004,
    MCU_PANIC_EXCEP_BUS_ERROR           = 0x10000005,
    MCU_PANIC_ISR_TRAP                  = 0x10001000,
    MCU_PANIC_BAD_PARAMETER             = 0x10002000,
    MCU_PANIC_INDEX_OUT_OF_RANGE        = 0x10002001,
    MCU_PANIC_HW_ANATYPE                = 0x10003000,
    MCU_PANIC_HW_ANALOG_CARD            = 0x10003001,
    MCU_PANIC_BUS_ERR                   = 0x10004000,
    MCU_PANIC_SW_CRASH                  = 0x10005000,
    MCU_PANIC_SYSCALL_READ_R            = 0x10006000,
    MCU_PANIC_SYSCALL_LSEEK_R           = 0x10006001,
    MCU_PANIC_SYSCALL_MALLOC_SIZE_R     = 0x10006002,
    MCU_PANIC_SYSCALL_MALLOC_OUTOFMEM_R = 0x10006003,
    MCU_PANIC_SYSCALL_CALLOC_OUTOFMEM_R = 0x10006004,
    MCU_PANIC_SYSCALL_MALLOC_REENT_R    = 0x10006005,
    MCU_PANIC_SYSCALL_CALLOC_REENT_R    = 0x10006006,
    MCU_PANIC_SYSCALL_REALLOC_R         = 0x10006007,
    MCU_PANIC_SYSCALL_FREE_R            = 0x10006008,
    MCU_PANIC_PROP_REFUNLOCK_1          = 0x10007000,
    MCU_PANIC_PROP_REFUNLOCK_2          = 0x10007001,
    MCU_PANIC_PUB_TSK_NULL_PROP_PTR     = 0x10008000,
    MCU_PANIC_PUB_TSK_SUB_NOTIFY        = 0x10008001,
    MCU_PANIC_PUB_TSK_SUB_TABLE_NOTIFY  = 0x10008002

};

/*---------------------------------------------------------------------------------------------------------*/

void McuPanic(enum mcu_panic_codes panic_code, uint32_t leds0, uint32_t leds1);

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation MCU_PANIC_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mcu_panic.h
\*---------------------------------------------------------------------------------------------------------*/

