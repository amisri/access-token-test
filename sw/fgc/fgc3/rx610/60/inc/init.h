/*!
 *  @file     codes.h
 *  @defgroup FGC3:MCU boot
 *  @brief    FGC Boot code update functions
 *
 *  Description:
 */

#ifndef INIT_H // header encapsulation
#define INIT_H

#ifdef INIT_GLOBALS
#define INIT_VARS_EXT
#else
#define INIT_VARS_EXT extern
#endif



enum FGC_init_status
{
    INIT_SUCCESS,
    INIT_MISSING_NAMEDB,
    INIT_DEV_NOT_IN_NAMEDB,
    INIT_INVALID_CLASS_ID,
    INIT_SELF_TEST_FAILURE,
    INIT_ETH_CHIP_INACCESSIBLE,
    INIT_UNKNOWN_ETH_CHIP,
    INIT_UNKNOWN_FAILURE,
};



/*!
 * Initialises memory shared with the main program
 *
 * If starting from a power up, the whole shared memory is cleared, otherwise,
 * the unix time is recorded if not zero (for instance with a MCU reset through the debugger).
 */
void InitSharedMem(void);

enum FGC_init_status InitNetwork(void);

/*!
 * Performs FGC self-tests
 *
 * When power up:
 * - MenuMT_PLD_DpRam
 * - MenuMTsram
 * - MenuMTdspProg wich also starts the derived clocks, StartDspClock()
 * When reset:
 * - MenuMTdspProg wich also starts the derived clocks, StartDspClock()
 */
enum FGC_init_status InitSelfTest(void);

enum FGC_init_status InitOperationalMode(void);

void InitTick(void);

#endif  // INIT_H

// EOF
