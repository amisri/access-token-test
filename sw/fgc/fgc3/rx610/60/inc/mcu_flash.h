/*---------------------------------------------------------------------------------------------------------*\
  File:         mcu_flash.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MCU_FLASH_H     // header encapsulation
#define MCU_FLASH_H

#ifdef MCU_FLASH_GLOBALS
#define MCU_FLASH_VARS_EXT
#else
#define MCU_FLASH_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>

//-----------------------------------------------------------------------------------------------------------

enum FGC_flash_status
{
    FLASH_STATUS_SUCCESS,
    FLASH_STATUS_FCU_ERROR,
    FLASH_STATUS_TIMEOUT,
    FLASH_STATUS_ILLEGAL_COMMAND,
    FLASH_STATUS_INVALID_ADDRESS,
    FLASH_STATUS_INVALID_SIZE,
    FLASH_STATUS_SIZE_NOT_MULTIPLE_OF_256,
    FLASH_STATUS_ADDR_NOT_ON_256_BOUNDARY,
    FLASH_STATUS_SIZE_EXCEEDS_1MB,
    FLASH_STATUS_ADDR_NOT_IN_ROM,
};

#define MCU_NUM_FLASH_BLOCKS 28

//-----------------------------------------------------------------------------------------------------------
MCU_FLASH_VARS_EXT uint32_t MCU_BLOCK_ADDRESS[MCU_NUM_FLASH_BLOCKS]
#ifdef MCU_FLASH_GLOBALS
=
{
    0xFFFFE000,                    // EB0 - Flash block 0 - Fixed vectors
    0xFFFFC000,                    // EB1 - Flash block 1 - NameDB
    0xFFFFA000,                    // EB2 - Flash block 2
    0xFFFF8000,                    // EB3 - Flash block 3
    0xFFFF6000,                    // EB4 - Flash block 4
    0xFFFF4000,                    // EB5 - Flash block 5
    0xFFFF2000,                    // EB6 - Flash block 6
    0xFFFF0000,                    // EB7 - Flash block 7
    0xFFFE0000,                    // EB8 - Flash block 8
    0xFFFD0000,                    // EB9 - Flash block 9
    0xFFFC0000,                    // EB10 - Flash block 10
    0xFFFB0000,                    // EB11 - Flash block 11
    0xFFFA0000,                    // EB12 - Flash block 12
    0xFFF90000,                    // EB13 - Flash block 13
    0xFFF80000,                    // EB14 - Flash block 14 - SysDB
    0xFFF70000,                    // EB15 - Flash block 15 - DIM DB
    0xFFF60000,                    // EB16 - Flash block 16 - CompDB
    0xFFF40000,                    // EB17 - Flash block 17
    0xFFF20000,                    // EB18 - Flash block 18
    0xFFF00000,                    // EB19 - Flash block 19
    0xFFEE0000,                    // EB20 - Flash block 20
    0xFFEC0000,                    // EB21 - Flash block 21 - DSPPROG
    0xFFEA0000,                    // EB22 - Flash block 22 - Main Program (1)
    0xFFE80000,                    // EB23 - Flash block 23 - Main Program (0)
    0xFFE60000,                    // EB24 - Flash block 24
    0xFFE40000,                    // EB25 - Flash block 25
    0xFFE20000,                    // EB26 - Flash block 26 - Boot program (1)
    0xFFE00000                     // EB27 - Flash block 27 - Boot program (0)
}
#endif
;
//-----------------------------------------------------------------------------------------------------------

// void MCU_rom_FCU_init_inRam          (void) __attribute__ ((section (".text_ram")));
// void    MCU_rom_FCU_init_inRam          (void) __attribute__ ((section (".text_ram, \"ax\",@progbits ;")));
void    MCU_rom_FCU_init_inRam(void) __attribute__((section(".text_ram, \"ax\";")));
// the semicolon marks the rest of the line as a comment, so the assembler ignores it.

bool MCU_rom_WaitForReady_inRam(uint32_t timeout) __attribute__((section(".text_ram")));
enum FGC_flash_status   MCU_rom_BlockErase(uint32_t * erase_address);
enum FGC_flash_status   MCU_rom_BlockErase_inRam(volatile uint32_t erase_address) __attribute__((section(".text_ram")));
enum FGC_flash_status   MCU_rom_BlockWrite(uint32_t source_address, uint32_t destin_address, uint32_t size_in_words);
enum FGC_flash_status   MCU_rom_BlockWrite_inRam(uint32_t source_address, volatile uint32_t destin_address,
                                 uint32_t size_in_words) __attribute__((section(".text_ram")));
void    MCU_rom_FlashBoot(uint32_t destin_address, uint32_t size_in_bytes);
void    MCU_rom_FlashBoot_inRam(uint32_t destin_address_last_byte, uint32_t destin_address,
                                uint32_t size_in_words) __attribute__((noreturn, section(".text_ram")));

//-----------------------------------------------------------------------------------------------------------

MCU_FLASH_VARS_EXT uint8_t        flashFunc_fmr0;
MCU_FLASH_VARS_EXT uint8_t        flashFunc_fmr1;

//-----------------------------------------------------------------------------------------------------------

#endif  // MCU_FLASH_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: mcu_flash.h
\*---------------------------------------------------------------------------------------------------------*/

