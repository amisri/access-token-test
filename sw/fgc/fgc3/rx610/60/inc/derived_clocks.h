/*!
 *  @file     derived_clocks.h
 *  @defgroup FGC3:MCU
 *  @brief    Sub clock structures
 *
 *  Description: maps the registers existing in the PLD to control several clock outputs
 *
 */


#ifndef DERIVED_CLOCKS_H        // header encapsulation
#define DERIVED_CLOCKS_H

#ifdef DERIVED_CLOCKS_GLOBALS
#define DERIVED_CLOCKS_VARS_EXT
#else
#define DERIVED_CLOCKS_VARS_EXT extern
#endif


// Includes

#include <memmap_mcu.h>
#include <sleep.h>


// External function declarations

/*!
 * start DSP NMI Tick
 */
static inline void StartDspClock(void)
{
    // Enable DSP NMI Tick
    TICK_CTRL_P |= TICK_CTRL_DSP_MASK16;
    TICK_CTRL_P |= TICK_CTRL_SYNC_TO_C0_MASK16;

    // Set TIME_TILL_CO to align the DSP tick to millisecond boundary
    TIME_TILL_C0_US_P = 10000;

    // Wait 11 ms for the DSP tick is operational
    sleepUs(11000);
}


/*!
 * Stop Dsp, Adc, Dac derived clock generation
 */
static inline void StopDerivedClocks(void)
{
    TICK_CTRL_P &= ~TICK_CTRL_DSP_MASK16;   // disable DSP NMI Tick
    TICK_CTRL_P &= ~TICK_CTRL_ADC_MASK16;   // disable ADC transfer clock
    TICK_CTRL_P &= ~TICK_CTRL_ACT_MASK16;   // disable ACT transfer clock
}

/*!
 * Stop the generation of the DSP NMI interrupt (DSP tick) by the PLD
 */
static inline void StopDspClock(void)
{
    TICK_CTRL_P &= ~TICK_CTRL_DSP_MASK16; // disable DSP NMI Tick
}

/*!
 * ADC derived clock generation
 */
static inline void StartAdcClock(void)
{
    TICK_CTRL_P |= TICK_CTRL_ADC_MASK16; // enable ADC transfer clock
}

/*!
 * DAC derived clock generation
 */
static inline void StartActClock(void)
{
    TICK_CTRL_P |= TICK_CTRL_ACT_MASK16; // enable DAC transfer clock
}

#endif  // DERIVED_CLOCKS_H end of header encapsulation

//EOF

