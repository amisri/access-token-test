/*!
 *  @file     core.h
 *  @defgroup FGC3:MCU
 *  @brief    FGC Core dump related functions
 *
 *  Description:
 *      The core dump system allows multiple segments of memory to be recorded in MRAM in the event of
 *      an unexpected trap.  This is controlled by the core control table which specifies the start address
 *      and length of each segment.  The table defines FGC_N_CORE_SEGS segments.  If a segment is not in
 *      use, then the length will be set to zero words.  The table is stored in MRAM but is copied to RAM
 *      for use by the boot and the main programs.
 *
 *      Theoretically 4KB can be stored but in practice the fast watchdog will reset the FGC before more than
 *      about 3KB can be recorded, unless the system is in the lab and is running with the watchdog inhibited.
 *
 */

#ifndef CORE_H          // header encapsulation
#define CORE_H

#ifdef CORE_GLOBALS
#define CORE_VARS_EXT
#else
#define CORE_VARS_EXT extern
#endif


// Includes

#include <stdint.h>
#include <defconst.h>   // for FGC_N_CORE_SEGS


// Internal structures, unions and enumerations

struct core_vars
{
    uint32_t        addr   [FGC_N_CORE_SEGS];
    uint16_t        n_words[FGC_N_CORE_SEGS];
};


/*!
 * Core control structure - common to all classes
 * NB: the padding word was not needed in FGC2 (as a consequence, sizeof(fgc_coreseg) was equal to 6).
 * But it is required on FGC3.
 */
struct fgc_corectrl
{
    struct fgc_coreseg
    {
        uint16_t    n_words;                    //!< Length of memory segment in words
        uint16_t    pad;                        //!< Padding word (set to 0xFFFF)
        uint32_t    addr;                       //!< Address of start of memory segment
    }           seg[FGC_N_CORE_SEGS];

    uint32_t    addr_in_core[FGC_N_CORE_SEGS];  //!< Start address in the core data for each segment
};


// Internal function declarations

/*!
 * This function will save the memory segments specified in the core control table to NVRAM.  If the table
 * is corrupted, it will be overwritten with the default table first.
 */
void CoreDump(void);


/*!
 * This function will retrieve the core control table from NVRAM to *corectrl.  It will check that the table
 * is valid - if it is the function returns with the table in *corectrl.  If it finds that the table is
 * not valid, it writes the default table into *corectrl and into NVRAM and clears the Core data.
 *
 * @param corectrl fgc_corectrl structure to get
 */
void CoreTableGet(struct fgc_corectrl * corectrl);


/*!
 * This function will write the corectrl table to the NVRAM and clear the CORE data area.
 *
 * @param corectrl fgc_corectrl structure to set
 */
void CoreTableSet(struct fgc_corectrl * corectrl);


// External variable definitions

/*! Core dump variables structure */
CORE_VARS_EXT struct core_vars core;

#endif  // CORE_H end of header encapsulation

// EOF

