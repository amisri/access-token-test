/*---------------------------------------------------------------------------------------------------------*\
  File:         syscalls.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef SYSCALLS_H      // header encapsulation
#define SYSCALLS_H

#ifdef SYSCALLS_GLOBALS
#define SYSCALLS_VARS_EXT
#else
#define SYSCALLS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>

//-----------------------------------------------------------------------------------------------------------

// #define NEWLIB_MALLOC_BLOCK_SIZE     350     // in M32C87B, max used 330?
#define NEWLIB_MALLOC_BLOCK_SIZE        500     // in RX610, max used 428?

// I was needing only 10 but Dsp_FPU_Benchmark() has mounted to 18 when float=FP64
// then Dsp_FPU_Benchmark() has mounted to 27 when FP64 is 64 bits
#define NEWLIB_MALLOC_BLOCK_COUNT       40

//-----------------------------------------------------------------------------------------------------------
// fake heap to allow use of malloc() by the NewLib routines

struct TMallocManager
{
    bool             free;
    uint8_t               data[NEWLIB_MALLOC_BLOCK_SIZE];
};

//-----------------------------------------------------------------------------------------------------------
void    InitMalloc(void);
//-----------------------------------------------------------------------------------------------------------

SYSCALLS_VARS_EXT struct TMallocManager         malloc_mngr[NEWLIB_MALLOC_BLOCK_COUNT];

//-----------------------------------------------------------------------------------------------------------

#endif  // SYSCALLS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: syscalls.h
\*---------------------------------------------------------------------------------------------------------*/
