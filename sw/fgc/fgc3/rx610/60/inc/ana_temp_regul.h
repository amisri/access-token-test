/*!
 *  @file     ana_temp_regul.h
 *  @defgroup FGC3:MCU
 *  @brief    FGC3 MCU Analogue Interface Functions
 *
 *  WARNING: Common to BOOT and MAIN
 */

#ifndef ANA_TEMP_REGUL_H
#define ANA_TEMP_REGUL_H

#ifdef ANA_TEMP_REGUL_GLOBALS
#define ANA_TEMP_REGUL_VARS_EXT
#else
#define ANA_TEMP_REGUL_VARS_EXT extern
#endif

// Includes

#include <stdint.h>
#include <stdbool.h>

// Global structures, unions and enumerations

// TODO Make this variable static after ADC.INTERNAL.VREF_TEMP.PID is no longer needed

typedef struct ana_temp_regul
{
    float                adc_gain;       // ADC value over temperature value
    int32_t              integrator;     // Integral register
    int32_t              prev_error;     // Previous error register
    uint16_t              prop_gain;      // Proportional gain
    uint16_t              int_gain;       // Integral     gain
    uint16_t              diff_gain;      // Derivative   gain
    uint16_t              proc_time;      // Processing time for the regul algorithm
    uint16_t              adc_ref;        // Temperature reference (raw ADC units)
    uint16_t              on_f;           // Regulation ON flag
} ana_temp_regul_t;

// Global variable definitions

ANA_TEMP_REGUL_VARS_EXT ana_temp_regul_t ana_temp_regul;

// Constants

/*!
 * Duration of heater self testing (boot menu), in seconds
 *
 */
#define  ANA_HEAT_TEST_TIME_S           30

// External function declarations

/*!
 * This function initialises the temperature regulation.
 *
 * On the MAIN, this function must be called AFTER NvsInit so that ADC.INTERNAL.VREF_TEMP.REF is set.
 */
void AnaTempInit(void);

/*!
 * This function starts the temperature regulation.
 *
 * @param ref_temp Target temperature in C.
 */
void AnaTempRegulStart(float ref_temp);

/*!
 * This function stops the temperature regulation.
 *
 * To properly turn off the heater and disable regulation, call:
 * AnaTempRegulStart(0.0);
 */
void AnaTempRegulStop(void);

/*!
 * This function computes the analogue interface temperature regulation.
 *
 * Uses a PID controller running at 1Hz.
 */
void AnaTempRegul(void);

/*!
 * Reads the temperature value of the voltage reference on the analogue board.
 *
 * ADC values are 10 bit.
 * 1deg = 47mV
 * temp(deg) = ADC_val * 3.3 / 1023 / 0.047  (*0.068634)
 *
 * @retval The temperature value in oC.
 */
float AnaTempGetTemp(void);

/*!
 * * Sets the PID gains.
 *
 * @param p_gain Proportional gain.
 * @param i_gain Integral gain.
 * @param d_gain Derivative gain.
 */
void AnaTempSetPIDGain(uint16_t p_gain, uint16_t i_gain, uint16_t d_gain);

/*!
 * Returns True if the temperature regulation is on, False otherwise.
 *
 * @retval True if the temperature regulation is on, False otherwise.
 */
bool AnaTempGetStatus(void);

/*!
 * Sets the heater power. Maximum power is 2W.
 *
 * @param mW power to set.
 */
void AnaTempSetPower(uint16_t mW);

/*!
 * Returns the P Gain value.
 *
 * @retval The P Gain value.
 */
uint16_t AnaTempGetPGain(void);

/*!
 * Returns the I Gain value.
 *
 * @retval The I Gain value.
 */
uint16_t AnaTempGetIGain(void);

/*!
 * Returns the D Gain value.
 *
 * @retval The D Gain value.
 */
uint16_t AnaTempGetDGain(void);

/*!
 * Returns the PID integrator value.
 *
 * @retval The PID integrator value.
 */
int32_t AnaTempGetIntegrator(void);

/*!
 * Returns the ADC raw value.
 *
 * @retval The ADC raw value.
 */
uint16_t AnaTempGetAdcRef(void);

/*!
 * Returns the ADC gain.
 *
 * @retval The ADC gain.
 */
float AnaTempGetAdcGain(void);

/*!
 * Returns the processing time.
 *
 * @retval The processing time.
 */
uint16_t AnaTempGetProcessingTime(void);

#endif

// EOF
