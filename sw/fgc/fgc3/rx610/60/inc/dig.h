/*!
 *  @file      dig.h
 *  @defgroup  FGC:MCU
 *  @brief     Interface to the digital input/output channels.
 */

#ifndef FGC_DIG_H
#define FGC_DIG_H

#ifdef DIG_GLOBALS
#define DIG_VARS_EXT
//#define EXTERN_INLINE
#else
#define DIG_VARS_EXT extern
//#define EXTERN_INLINE extern inline
#endif

#define EXTERN_INLINE inline

// Includes

#include <stdint.h>
#include <stdbool.h>
#include <macros.h>
#include <memmap_mcu.h>

// External structures, unions and enumerations

/*!
 * Digital supplementary output and input channels.
 */
typedef enum dig_sup_channel
{
    DIG_SUP_A_CHANNEL0 = 0,
    DIG_SUP_A_CHANNEL1 = 1,
    DIG_SUP_A_CHANNEL2 = 2,
    DIG_SUP_A_CHANNEL3 = 3,
    DIG_SUP_A_CHANNEL4 = 4,
    DIG_SUP_B_CHANNEL0 = 0,
    DIG_SUP_B_CHANNEL1 = 1,
    DIG_SUP_B_CHANNEL2 = 2,
    DIG_SUP_B_CHANNEL3 = 3,
    DIG_SUP_B_CHANNEL4 = 4
} dig_sup_channel_t;

typedef enum dig_sup_source
{
    DIG_SUP_SOURCE_DIRECT,
    DIG_SUP_SOURCE_PULSE_GEN,
    DIG_SUP_SOURCE_SERIAL,
    DIG_SUP_SOURCE_CYCLE_START,
    DIG_SUP_SOURCE_50HZ,
    DIG_SUP_SOURCE_1KHZ,
    DIG_SUP_SOURCE_1MHZ,
} dig_sup_source_t;

typedef enum dig_sup_status
{
    DIG_SUP_STATUS_SUCCESS,
    DIG_SUP_STATUS_NOT_SUPPORTED,
} dig_sup_status_t;

// External function declarations

// DIG functions

/*!
 * Initializes the supplemental digital I/O.
 */
void DigInit(void);

dig_sup_status_t DigSupSetSourceBankA(dig_sup_channel_t channel, dig_sup_source_t source);
dig_sup_status_t DigSupSetSourceBankB(dig_sup_channel_t channel, dig_sup_source_t source);

// DIG_SUP functions

/*!
 *  Return TRUE if the ARM bit for channel is set. FALSE otherwise.
 *
 *  @param channel The supplemental digital channel to access.
 *  @retval TRUE if the ARM bit is set. FALSE otherwise.
 */
static inline bool DigSupTstArm(dig_sup_channel_t channel);

/*!
 *  Sets the ARM bit corresponding to the channel given in the parameter.
 *
 *  @param channel The supplemental digital channel to access.
 */
static inline void DigSupSetArm(dig_sup_channel_t channel);

/*!
 *  Clears the ARM bit corresponding to the channel given in the parameter.
 *
 *  @param channel The supplemental digital channel to access.
 */
static inline void DigSupClrArm(dig_sup_channel_t channel);

/*!
 *  Sets the width in microseconds of the pulse generated in the given channel.
 *
 *  Note: assumes block A is configured as output.
 *
 *  @param channel The supplemental digital channel to access.
 *  @param value_ms Value in milliseconds to set.
 */
static inline void DigSupSetPulseWidth(dig_sup_channel_t channel, int32_t value_ms);

/*!
 *  Sets the estimated time - with respect to the time till event register - for the
 *  rising edge of the pulse generated in the given channel.
 *
 *  Note: assumes block A is configured as output.
 *
 *  @param channel The supplemental digital channel to access.
 *  @param value_ms Value in milliseconds to set.
 */
static inline void DigSupSetPulseEtim(dig_sup_channel_t channel, int32_t value_ms);

/*!
 *  Returns TRUE if a rising edge has been detected on the given channel. FALSE otherwise.
 *
 *  @param channel The supplemental digital channel to access.
 *  @retval TRUE if the rising edge is detected. FALSE otherwise.
 */
static inline bool DigSupTstRiseEdge(dig_sup_channel_t channel);

/*!
 *  Returns the time in microseconds - with respect to the time till event register -
 *  of the rising edge detected in the given channel.
 *
 *  Note: assumes block B is configured as input. *
 *
 *  @param channel The supplemental digital channel to access.
 *  @retval The time in microseconds when the rising edge occurred.
 */
static inline int32_t DigSupGetRiseEtim(dig_sup_channel_t channel);

/*!
 *  Returns TRUE if a falling edge has been detected on the given channel. FALSE otherwise.
 *
 *  @param channel The supplemental digital channel to access.
 *  @retval TRUE if the falling edge is detected. FALSE otherwise.
 */
static inline bool DigSupTstFallEdge(dig_sup_channel_t channel);

/*!
 *  Returns the time in microseconds - with respect to the time till event register -
 *  of the falling edge detected in the given channel.
 *
 *  Note: assumes block B is configured as input. *
 *
 *  @param channel The supplemental digital channel to access.
 *  @retval The time in microseconds when the falling edge occurred.
 */
static inline int32_t DigSupGetFallEtim(dig_sup_channel_t channel);

/*!
 *  Returns TRUE if the falling and rising edges have been detected on the given channel.
 *  FALSE otherwise.
 *
 * @param channel The supplemental digital channel to access.
 * @retval TRUE if both edges have been detected. FALSE otherwise.
 */
static inline bool DigSupTstEdges(dig_sup_channel_t channel);

/*!
 *  Inverts the channel on SUP_A
 *
 *  @param channel The supplemental digital channel to access.
 */
static inline void DigSupInvertChannelA(dig_sup_channel_t channel);

/*!
 *  Inverts the channel on SUP_B
 *
 *  @param channel The supplemental digital channel to access.
 */
static inline void DigSupInvertChannelB(dig_sup_channel_t channel);

// External function definitions

static inline bool DigSupTstArm(dig_sup_channel_t channel)
{
    return Test(DIG_SUP_A_ARM_P, (1 << channel));
}

static inline void DigSupSetArm(dig_sup_channel_t channel)
{
    Set(DIG_SUP_A_ARM_P, (1 << channel));
}

static inline void DigSupClrArm(dig_sup_channel_t channel)
{
    Clr(DIG_SUP_A_ARM_P, (1 << channel));
}

static inline void DigSupSetPulseEtim(dig_sup_channel_t channel, int32_t value_us)
{
    DIG_SUP_A_PULSE_ETIM_US_A[channel] = value_us;
}

static inline void DigSupSetPulseWidth(dig_sup_channel_t channel, int32_t value_us)
{
    DIG_SUP_A_PULSE_WIDTH_US_A[channel] = value_us;
}

static inline bool DigSupTstRiseEdge(dig_sup_channel_t channel)
{
    return (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_RISING_EDGE_MASK16));
}

static inline int32_t DigSupGetRiseEtim(dig_sup_channel_t channel)
{
    // The pulse has to be at least 1.5 us high before the FPGA firmware acknowledges the
    // detection of the rising edge. The DIG_SUP_B_RISE_ETIM_US_A register is then set
    // with the current value of the TILL_EVT_US register, which is 1.5 us after the actual
    // edge occurred. That is why 1 us must be added.

    return (DIG_SUP_B_RISE_ETIM_US_A[channel] + 1);
}

static inline bool DigSupTstFallEdge(dig_sup_channel_t channel)
{
    return (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_FALLING_EDGE_MASK16));
}

static inline int32_t DigSupGetFallEtim(dig_sup_channel_t channel)
{
    // The pulse has to be at least 1.5 us low before the FPGA firmware acknowledges the
    // detection of the falling edge. The DIG_SUP_B_FALL_ETIM_US_A register is then set
    // with the current value of the TILL_EVT_US register, which 1.5 us after the actual
    // edge occurred. That is why 1 us must be added.

    return (DIG_SUP_B_FALL_ETIM_US_A[channel] + 1);
}

static inline bool DigSupTstEdges(dig_sup_channel_t channel)
{
    return (Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_FALLING_EDGE_MASK16) &&
            Test(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_RISING_EDGE_MASK16));
}

static inline void DigSupInvertChannelA(dig_sup_channel_t channel)
{
    Set(DIG_SUP_A_CHAN_CTRL_A[channel], DIG_SUP_A_CHAN_CTRL_INVERT_MASK16);
}

static inline void DigSupInvertChannelB(dig_sup_channel_t channel)
{
    Set(DIG_SUP_B_CHAN_CTRL_A[channel], DIG_SUP_B_CHAN_CTRL_INVERT_MASK16);
}

#endif

// EOF
