/*!
 *  @file     lan92xx.h
 *  @defgroup FGC3
 *  @brief      Ethernet eth Interface Functions.
 *
 */

#ifndef LAN92XX_H       // header encapsulation
#define LAN92XX_H

#ifdef LAN92XX_GLOBALS
#define LAN92XX_VARS_EXT
#else
#define LAN92XX_VARS_EXT extern
#endif

// includes

#include <stdint.h>
#include <stdbool.h>
#include <memmap_mcu.h>
#include <macros.h>

// Constants

// Interrupt in INT_STS and INT_EN registers
#define     ETH_RSFL_INT    0x00000008  // RX Status FIFO Level Interrupt
#define     ETH_RSFF_INT    0x00000010  // RX Status FIFO Full Interrupt
#define     ETH_RXDF_INT    0x00000040  // RX dropped frame Interrupt
#define     ETH_TSFL_INT    0x00000080  // TX Status FIFO Level Interrupt
#define     ETH_TSFF_INT    0x00000100  // TX Status FIFO Full Interrupt
#define     ETH_TDFA_INT    0x00000200  // TX Data FIFO Available Interrupt (not enough space)
#define     ETH_TDFO_INT    0x00000400  // TX Data FIFO Overrun Interrupt
#define     ETH_TXE_INT     0x00002000  // TX error
#define     ETH_RXE_INT     0x00004000  // RX error
#define     ETH_TXSO_INT    0x00010000  // TX status overflow
#define     ETH_PHY_INT     0x00040000  // Interrupt from PHY layer
#define     ETH_RXSTOP_INT  0x01000000  // RX stop
#define     ETH_TXSTOP_INT  0x02000000  // TX stop

// Options for TX commands A & B
#define TX_CMDA_IOC             0x80000000      // Wait for Interrupt on Completion
#define TX_CMDA_FIRST_SEG       0x00002000      // Indicates this is the first segment
#define TX_CMDA_LAST_SEG        0x00001000      // Indicates this is the last segment

/*!
 * PHY registers
 *
 * The PHY registers are not memory mapped. They are accessed
 * indirectly through the MAC via the MII_ACC and MII_DATA registers.
 * An index must be used to access individual PHY registers.
 */
enum lan92_phy_registers
{
    LAN92_PHY_REG_CONTROL                 = 0,     //!< Basic Control Register
    LAN92_PHY_REG_STATUS                  = 1,     //!< Basic Status Register
    LAN92_PHY_REG_ID1                     = 2,     //!< PHY Identifier 1
    LAN92_PHY_REG_ID2                     = 3,     //!< PHY Identifier 2
    LAN92_PHY_REG_AUTO_NEG_ADVERTISEMENT  = 4,     //!< Auto-Negotiation Advertisement Register
    LAN92_PHY_REG_AUTO_NEG_PARTNER        = 5,     //!< Auto-Negotiation Link Partner Ability Register
    LAN92_PHY_REG_AUTO_NEG_EXPANSION      = 6,     //!< Auto-Negotiation Expansion Register
    LAN92_PHY_REG_MODE_CTRL_STATUS        = 17,    //!< Mode Control/Status Register
    LAN92_PHY_REG_SPECIAL_MODES           = 18,    //!< Special Modes Register
    LAN92_PHY_REG_SPECIAL_CTRL_STATUS     = 27,    //!< Special Control/Status Indications
    LAN92_PHY_REG_INTERRUPT_SOURCE        = 29,    //!< Interrupt Source Register
    LAN92_PHY_REG_INTERRUPT_MASK          = 30,    //!< Interrupt Mask Register
    LAN92_PHY_REG_PHY_SPECIAL_CTRL_STATUS = 31,    //!< PHY Special Control/Status Register
};

/*!
 * Bitmasks used to access PHY Basic Status Register
 */
enum lan92_phy_status
{
    LAN92_PHY_STATUS_EXTENDED_CAPABILITIES    = 0x0001,
    LAN92_PHY_STATUS_JABBER_DETECTED          = 0x0002,
    LAN92_PHY_STATUS_LINK_UP                  = 0x0004,
    LAN92_PHY_STATUS_AUTO_NEGOTIATE_AVAILABLE = 0x0008,
    LAN92_PHY_STATUS_REMOTE_FAULT             = 0x0010,
    LAN92_PHY_STATUS_AUTO_NEGOTIATE_COMPLETE  = 0x0020,
    LAN92_PHY_STATUS_RESERVED                 = 0x07C0,
    LAN92_PHY_STATUS_10BASE_T_HALF_DUPLES     = 0x0800,
    LAN92_PHY_STATUS_10BASE_T_FULL_DUPLES     = 0x1000,
    LAN92_PHY_STATUS_100BASE_TX_HALF_DUPLEX   = 0x2000,
    LAN92_PHY_STATUS_100BASE_TX_FULL_DUPLEX   = 0x4000,
    LAN92_PHY_STATUS_100BASE_T4               = 0x8000,
};

// External function declarations

/*!
 * This function reads the number of bytes from the RX FIFO to the given pointer
 * Number of bytes available in fifo are given and decremented accordingly
 * If read_length is not a multiple of 4, extra words (not read) are saved for further function call
 * in static variable (non-reentrant)
 */
void Lan92xxReadFifo(const void * to, uint16_t read_length, uint16_t * fifo_length);

/*!
 * A RX dump reset the RX data and status FIFO.
 * Useful to restart RX after a FIFO overflow.
 */
void Lan92xxRxDump(void);

/*!
 * This function send the given data.
 * Packet_size is in byte and must be a multiple of 4 (32 bits access)
 * Return 0 if OK
 */
uint8_t Lan92xxSendPacket(const void * packet_data, uint16_t packet_size);

/*!
 *   This function sends the given data, with extra options optA and optB (not checked) for TXcommanA and B.
 * segment_size is the size of the current segment to be written, pointed by *segment
 * Packet_size is the size of the full packet in byte and must be a multiple of 4 (32 bits access)
 * Giving a NULL segment is not error, but no data will be written.
 * In this case, you must write the data by yourself and must be sure you write exactly segment size bytes
 * Return 0 if OK
 * If not OK, it is the duty of caller to recover. Either by trying again later or by reseting the chip.
 * Note that TX_STOP + TX data and status dump + TX_ON does not enable to recover (TX_STOP stays high).
 */
uint8_t Lan92xxSendPacketOpt(const void * segment, uint16_t segment_size, uint16_t packet_size, uint32_t optA,
                             uint32_t optB);

/*!
 * This function will discard the end of the current packet. MUST be called after reading a packet
 * byte_size is the number of remaining data in the packet in bytes
 * if size is between 1 and 4DWORDS, we must read the data to discard them,
 * if >4, use RX fast forward (RX_FFWD)
 * Note : byte_size = 0 will also use RX_FFWD.
 */
void lan92xxDiscardPacket(uint16_t nb_data);

/*!
 *  Set MAC address
 */
void Lan92xxSetMac(uint8_t mac[6]);

/*!
 * Get the MAC address
 */
void Lan92xxGetMac(uint8_t mac[6]);

/*!
 * Load the MAC address from the EEPROM
 * Note: EEPROM is automatically read at start-up
 */
uint8_t Lan92xxLoadMac(void);

/*!
 * Read the PHY register
 */
uint16_t Lan92xxReadPhyReg(enum lan92_phy_registers reg);

/*!
 * Write the PHY register
 */
void Lan92xxWritePhyReg(uint16_t value, enum lan92_phy_registers const reg);

// Inline function definition

/*!
 * return 0 if no error.
 */
static inline bool Lan92xxIsRxError(void)
{
    return ETH_INT_STS_P & ETH_RXE_INT;
}

/*!
 *     return 0 if no error.
 */
static inline bool Lan92xxIsTxError(void)
{
    return ETH_INT_STS_P & ETH_TXE_INT;
}

/*!
 * return 0 if no overrun.
 */
static inline bool Lan92xxIsRxOverrun(void)
{
    return ETH_RX_DROP_P;
}

/*!
 *     return FALSE if Tx not ready.
 */
static inline bool Lan92xxIsTxReady(void)
{
    return ETH_TX_CFG_P & 0x00000002; // TX_ON
}

/*!
 *     return TRUE if link is up.
 */
static inline bool Lan92xxIsLinkUp(void)
{
    return Test(Lan92xxReadPhyReg(LAN92_PHY_REG_STATUS), LAN92_PHY_STATUS_LINK_UP);
}

/*!
 *     Acknowledge interrupt
 */
static inline void Lan92xxAckInterrupt(uint32_t interrupt)
{
    ETH_INT_STS_P |= interrupt;
}

#endif  // LAN92XX_H end of header encapsulation

// EOF
