/*---------------------------------------------------------------------------------------------------------*\
  File:         trap.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef TRAP_H  // header encapsulation
#define TRAP_H

#ifdef TRAP_GLOBALS
#define TRAP_VARS_EXT
#else
#define TRAP_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------
struct TTrapData
{
    uint16_t              isrdummy_count;
    uint16_t              isrdummy_fix_count;
    uint16_t              isrdummy_empty_count;
};
//-----------------------------------------------------------------------------------------------------------

void    IsrDummy(void) __attribute__((interrupt));
void    IsrDummyFix(void) __attribute__((interrupt));
void    IsrDummyEmpty(void) __attribute__((interrupt));
void    IsrOverflow(void) __attribute__((interrupt));
void    IsrWatchdogTimer(void) __attribute__((interrupt));
void    IsrUndefinedInstruction(void) __attribute__((interrupt));
void    PanicFGC3(uint32_t leds0, uint32_t leds1);
void    IsrTrap(void);

//-----------------------------------------------------------------------------------------------------------

TRAP_VARS_EXT struct TTrapData  trap; // used by trap.c

//-----------------------------------------------------------------------------------------------------------

#endif  // TRAP_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: trap.h
\*---------------------------------------------------------------------------------------------------------*/
