/*---------------------------------------------------------------------------------------------------------*\
  File:         menu.h

  Purpose:      FGC3.x

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MENU_H  // header encapsulation
#define MENU_H

#ifdef MENU_GLOBALS
#define MENU_VARS_EXT
#else
#define MENU_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdbool.h>

#include <stdint.h>           // basic typedefs
#include <menu_node.h>
#include <term.h>               // for TERM_LINE_SIZE

//-----------------------------------------------------------------------------------------------------------

#define MENU_MAX_DEPTH                  8                       // Length of menu.node_id;
#define CMD_MAX_ARGS                    (TERM_LINE_SIZE/2)      // Max command args

//--- STATUS ---

#define  STAT_PASS              0
#define  STAT_FAIL              1
#define  STAT_ABORT             2
#define  STAT_ERR               0xFFFF

//-----------------------------------------------------------------------------------------------------------

struct menu
{
    struct menu_node    *   node;
    uint16_t                  depth;
    uint16_t                  up_lvls;
    char                    node_id[MENU_MAX_DEPTH];
    uint16_t                  argc;
    char          *         argv[CMD_MAX_ARGS];
    uint16_t                  response_buf_pos;
    char                    response_buf[TERM_LINE_SIZE];
    char                    error_buf[TERM_LINE_SIZE];
};

MENU_VARS_EXT   struct menu             menu;

//-----------------------------------------------------------------------------------------------------------

void    MenuMenu(struct menu_node * node);
void    MenuDisplay(struct menu_node * node);
uint16_t  MenuFollow(struct menu_node * node);
uint16_t  MenuRunFunc(struct menu_node * node, char * args_buf);
uint16_t  MenuConfirm(struct menu_node * node);
void    MenuRspArg(char   *   format, ...);
void    MenuRspError(char   *   format, ...);
uint16_t  MenuPrepareArgs(char * buff, uint16_t argc_exp);
uint16_t  MenuRspProgress(uint16_t allow_abort_f, uint16_t level, uint16_t total);
uint16_t  MenuGetInt16U(char * cp, uint16_t min, uint16_t max, uint16_t * result);
uint16_t  MenuGetInt32U(char * cp, uint32_t min, uint32_t max, uint32_t * result);
uint16_t  MenuGetInt32S(char * cp, int32_t min, int32_t max, int32_t * result);
uint16_t  MenuGetFloat(char * arg, float min, float max, float * result);

static inline bool MenuErrorIsPending(void)
{
    return(*menu.error_buf != 0);
}

static inline void MenuPrintError(void)
{
    // Write error report

    fprintf(TERM_TX, RSP_ERROR ",%s\n", (char *)menu.error_buf);

    // Clear error

    *menu.error_buf = '\0';
}

//-----------------------------------------------------------------------------------------------------------

#endif  // MENU_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: menu.h
\*---------------------------------------------------------------------------------------------------------*/

