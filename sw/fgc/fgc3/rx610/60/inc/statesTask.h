/*---------------------------------------------------------------------------------------------------------*\
  File:         statesTask.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef STATESTASK_H            // header encapsulation
#define STATESTASK_H

#ifdef STATESTASK_GLOBALS
#define STATESTASK_VARS_EXT
#else
#define STATESTASK_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

// ----- State Task Variables -----

struct sta_vars
{
    uint32_t              unix_time;                      // Unixtime for interation (for logging)
    uint32_t              us_time;                        // Microsecond time for iteration (for logging)
    uint16_t              ms_time;                        // Millisecond time for iteration
    bool             sec_f;                          // Start of new second flag
    uint16_t              cal_active;                     // CAl.ACTIVE property
    uint16_t              op_mode;                        // Operational mode
    uint16_t              config_mode;                    // Configuration mode
    uint16_t              config_state;                   // Configuration state
    uint16_t              watchdog;                       // Watchdog value zero for MstTsk
    uint32_t              time_ms;                        // Time since entering the state (ms)
    uint16_t              faults;                         // Active faults mask (unlatched)
#if FGC_CLASS_ID != 59
    uint16_t              gw_pc_permit;                   // GW PC_PERMIT down counter (20ms units)
    uint16_t              adc_type;                       // ADC type selector (INTERNAL or EXTERNAL)
    uint16_t              adc_ext_f;                      // ADC is SD_350 or SD_351 (i.e. with external inputs)
    uint16_t              adc_one_mhz[2];                 // ADC ONE_MHZ selector (ENABLED or DISABLED)
    uint16_t              last_adc_one_mhz[2];            // Last ADC ONE_MHZ selector (ENABLED or DISABLED)
    uint16_t              adc_ads1281[2];                 // External ADS1281 selector (ENABLED or DISABLED)
    uint16_t              last_adc_ads1281[2];            // Last external ADS1281 selector (ENABLED or DISABLED)
    uint16_t              adc_mpx[2];                     // ADC filter A/B digital input selector
    uint16_t              adc_fltr[2];                    // ADC filter A/B function selector
    uint16_t              last_adc_fltr[2];               // ADC filter A/B function selector
    uint16_t              internal_adc_mpx[2];                    // ADC16A/B analogue input selector

    // FGC_N_SD_FLTRS from z:/projects/fgc/sw/inc/classes/51/defconst.h
    // FGC_N_SD_FLTRS = 5
    uint16_t              sd_shift[5];    // ADC SD filter shifts
    uint16_t              sd_halftaps[5]; // ADC SD filter  half taps

    uint16_t              cal_type;           // Calibration type (CAL_REQ_ADC16, CAL_REQ_ADC16, CAL_REQ_DAC)
    uint16_t              cal_chan;           // Calibration channel (0=A, 1=B)
    uint16_t              cal_seq_idx;        // Calibration sequence index
    uint16_t              cal_counter;        // Calibration down counter
    uint16_t              dcct_flts;          // DCCT faults latch
    uint16_t              flags;              // State machine control flags
    uint16_t              inputs;             // Direct digital inputs
    uint16_t              cmd;                // Command request (ON/OFF/RESET/POLPOS/POLNEG)
    uint16_t              cmd_req;            // Requested command mask (read back from interface)
    uint8_t               pc_mode;            // MODE.PC property
#endif
};

//-----------------------------------------------------------------------------------------------------------

void    StaTsk(void * unused);
void    StaInit(void);
void    StaOpState(void);
void    StaDdips(void);
void    StaLeds(void);
void    StaVsState(void);
void    StaAdcControl(void);
void    StaCheck(void);
void    StaPcState(void);
void    StaCalControl(void);
void    StaCmdControl(void);
uint16_t  StaStartPC(void);

//-----------------------------------------------------------------------------------------------------------

STATESTASK_VARS_EXT struct sta_vars             sta;                    // State variables structure

//-----------------------------------------------------------------------------------------------------------

#endif  // STATESTASK_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: statesTask.h
\*---------------------------------------------------------------------------------------------------------*/

