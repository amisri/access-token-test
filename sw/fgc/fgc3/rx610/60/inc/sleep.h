/*!
 *  @file     sleep.h
 *  @defgroup FGC3:MCU:60
 *  @brief    Provides sleep functions.
 */

#ifndef FGC_SLEEP_H
#define FGC_SLEEP_H

#include <memmap.h>
#include <iodefines.h>
#include <main.h>

// Assigned value of us timer to usecs
// main_misc.timer_255us is updated when TMR3 overflows
// We need to wait to be sure main_misc.timer_255us has been incremented in case TMR3 has just overflowed.
// If too long we may read FF with main_misc.timer_255us incremented.
// Wait time set to 4 NOP experimentally.
// ToDo should be an inline function
#define GET_US(us)                                          \
    {                                                       \
        uint8_t tmr3_count = TMR3.TCNT;                       \
        __asm__ volatile ( "NOP" );                         \
        __asm__ volatile ( "NOP" );                         \
        __asm__ volatile ( "NOP" );                         \
        __asm__ volatile ( "NOP" );                         \
        uint8_t timer_255 = main_misc.timer_255us;            \
        us = (uint16_t) tmr3_count + (timer_255 << 8);        \
    }

// TMR3 will count [0..255] us then increments  x.timer_255us
// the (uint16_t) in the while is to force the difference to unsigned (absolute value)

// range 0..65535us
// ToDo should be an inline function
#define USLEEP(usecs)                                           \
    {                                                           \
        uint16_t t_start;                                         \
        GET_US(t_start);                                        \
        uint16_t t_delay = (uint16_t)(usecs);                       \
        uint16_t t_current = t_start;                             \
        while( (uint16_t)(t_current - t_start) < t_delay )        \
        {                                                       \
            GET_US(t_current);                                  \
        }                                                       \
    }

// TMR3.TCNT will count [0..255] us
// the (uint8_t) in the while is to force the difference to unsigned (absolute value)

// range 0..255us
// ToDo should be an inline function
#define SMALL_USLEEP(usecs)                                     \
    {                                                           \
        uint8_t t_start = TMR3.TCNT;                              \
        uint8_t t_delay = (uint8_t)(usecs);                         \
        while( (uint8_t)(TMR3.TCNT - t_start) < t_delay ) ;       \
    }

// x.timer_1ms is incremented in the 1ms Tick interrupt
// the (uint16_t) in the while is to force the difference to unsigned (absolute value)

// range 0..65535ms
// ToDo should be an inline function
#define MSLEEP(ms)                                                      \
    {                                                                   \
        uint16_t t_start = main_misc.timer_1ms;                          \
        uint16_t t_delay = (uint16_t) (ms);                                \
        while( (uint16_t)(main_misc.timer_1ms - t_start) < t_delay ) ;   \
    }

/*!
 * Sleeps during the number of microseconds specified in the argument.
 */
static inline void sleepUs(uint32_t time_us);

/*!
 * Sleeps during the number of milliseconds specified in the argument.
 */
static inline void sleepMs(uint32_t time_ms);

// External function definitions

static inline void sleepUs(uint32_t time_us)
{
    USLEEP(time_us);
}

static inline void sleepMs(uint32_t time_ms)
{
    MSLEEP(time_ms);
}

#endif

// EOF
