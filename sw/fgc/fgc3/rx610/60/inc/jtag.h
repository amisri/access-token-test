/*---------------------------------------------------------------------------------------------------------*\
  File:         jtag.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef JTAG_H          // header encapsulation
#define JTAG_H

#ifdef JTAG_GLOBALS
#define JTAG_VARS_EXT
#else
#define JTAG_VARS_EXT extern
#endif

#include <stdint.h>           // basic typedefs

//-----------------------------------------------------------------------------------------------------------

#define  LENVAL_MAX             600

//#define ENDOF_ALL_DEVICES     -1        // Mark, respectively, the end of devices, cells and errors in each array.
//#define ENDOF_ALL_CELLS       -1
#define ENDOF_ALL_ERRORS        -1

//--- JTAG port ---

#define  TCK_JTAG_LINE          0               // used for JtagSetPort function
#define  TMS_JTAG_LINE          1               // used for JtagSetPort function
#define  TDI_JTAG_LINE          2               // used for JtagSetPort function

#define  TCK_MASK               0x10
#define  TMS_MASK               0x20
#define  TDI_MASK               0x40
#define  TDO_MASK               0x80

//----- JTAG modes -----

#define  JTAG_PROG              0
#define  JTAG_VERIFY            1
#define  JTAG_ID_SIG            2
#define  JTAG_INFRA             3
#define  JTAG_INTER             4
#define  JTAG_CHECKSUM          5
#define  JTAG_MODE_LAST         6

//-----------------------------------------------------------------------------------------------------------

struct lenVal
{
    int16_t len;   // number of chars in this value
    uint8_t val[LENVAL_MAX + 1];  // bytes of data
};

union TJtag_port_union
{
    uint8_t value;
    struct jtag_port
    {
        uint8_t zero: 1;
        uint8_t one: 1;
        uint8_t bit2: 1;
        uint8_t bit3: 1;
        uint8_t tdi: 1;
        uint8_t tdo: 1;
        uint8_t tms: 1;
        uint8_t tck: 1;
    } bits;
};

/*
union inPortUnion
{
    uint8_t value;
    struct ipBitsStr
    {
        uint8_t bit0:1;
        uint8_t bit1:1;
        uint8_t bit2:1;
        uint8_t bit3:1;
        uint8_t bit4:1;
        uint8_t bit5:1;
        uint8_t bit6:1;
        uint8_t tdo:1;
    } bits;
};
 */

struct TJtag_vars
{
    bool     is_running;             // run flag
    uint16_t      state;                  // Sate in the JTAG TAP
    uint16_t      tdi;                    // value of TDI
    uint16_t      tms;                    // value of TMS
};

//-----------------------------------------------------------------------------------------------------------

uint16_t  JtagRun(uint8_t * file_p);
uint16_t  JtagInit(void);
uint16_t  JtagAnalysis(void);
uint8_t   JtagPortInp(void);
uint8_t   JtagReadTDOBit(void);                           //  read the TDO bit and store it in val
void    JtagZeroOutputs(void);
void    JtagPulseClock(void);                           //  make clock go down->up->down
void    JtagReadByte(uint8_t * data);                     //  read the next byte of data from the xsvf file
void    JtagPortOutp(uint8_t value);
void    JtagSetPort(int16_t line, uint8_t
                    val);                    //  set line (TCK_JTAG_LINE, TMS_JTAG_LINE, or TDI_JTAG_LINE) to val (0 or 1)
void    JtagWaitTime(uint32_t microsec);
uint32_t  JtagValue(struct lenVal * x);
int16_t  JtagEqualLenVal(struct lenVal * expected, struct lenVal * actual, struct lenVal * mask);
int16_t  JtagRetBit(struct lenVal * lv, uint16_t byte, uint16_t bit);
void    JtagInitLenVal(struct lenVal * x, uint32_t value);
void    JtagAddVal(struct lenVal * resVal, struct lenVal * val1, struct lenVal * val2);
void    JtagSetBit(struct lenVal * lv, uint16_t byte, uint16_t bit, int16_t val);
void    JtagReadVal(struct lenVal * x, int16_t numBytes);

//-----------------------------------------------------------------------------------------------------------

//JTAG_VARS_EXT  union inPortUnion      in_word;
JTAG_VARS_EXT union TJtag_port_union    gJtagOutPort;
JTAG_VARS_EXT struct TJtag_vars         gJtag;

JTAG_VARS_EXT uint16_t                    gJTAGportOnce
#ifdef JTAG_GLOBALS
    = 0
#endif
      ;

//-----------------------------------------------------------------------------------------------------------

#endif  // JTAG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: jtag.h
\*---------------------------------------------------------------------------------------------------------*/
