/*---------------------------------------------------------------------------------------------------------*\
  File:     label.h

  Purpose:  FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef LABEL_H // header encapsulation
#define LABEL_H

#ifdef LABEL_GLOBALS
#define LABEL_VARS_EXT
#else
#define LABEL_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <defconst.h>
#include <definfo.h>
#include <memmap_mcu.h>

//-----------------------------------------------------------------------------------------------------------

struct sym_name
{
    uint16_t          type;               // Type index
    char      *     label;              // Pointer to label (string constant)
};
// when using this structure in arrays
// remember to add at the end one line filled with 0s
// as the label part == 0  marks the end of the table for several functions like TypeLabel()

//-----------------------------------------------------------------------------------------------------------

void BitLabel(const struct sym_name * types, uint16_t bit_mask);
char * TypeLabel(const struct sym_name * types, uint16_t type);

//-----------------------------------------------------------------------------------------------------------

LABEL_VARS_EXT const struct sym_name warnings_lbl[]
#ifdef LABEL_GLOBALS
        =
{
    {  FGC_WRN_FGC_HW,          "FGC_HW"        },
    {  FGC_WRN_FGC_PSU,         "FGC_PSU"       },
    { 0 } // the label part == 0  marks the end of the table
}
#endif
;

LABEL_VARS_EXT const struct sym_name faults_lbl[]
#ifdef LABEL_GLOBALS
        =
{
    {  FGC_FLT_FGC_HW,          "FGC_HW"        },
    {  FGC_FLT_FGC_STATE,       "FGC_STATE"     },
    { 0 } // the label part == 0  marks the end of the table
}
#endif
;


LABEL_VARS_EXT const struct sym_name net_type_lbl[]
#ifdef LABEL_GLOBALS
        =
{
    { 0,    "FIP 2"     },
    { 1,    "ETHERNET"  },
    { 0                 }  // the label part == 0  marks the end of the table
}
#endif
;



LABEL_VARS_EXT const struct sym_name reset_source_lbl[]
#ifdef LABEL_GLOBALS
        =
{
    { CPU_RESET_SRC_POWER_MASK16,   "POWER CYCLE"   },
    { CPU_RESET_SRC_PROGRAM_MASK16, "SOFTWARE"  },
    { CPU_RESET_SRC_MANUAL_MASK16,  "PUSH BUTTON"   },
    { CPU_RESET_SRC_DONGLE_MASK16,  "DONGLE"   },
    { CPU_RESET_SRC_FASTWD_MASK16,  "FAST WATCHDOG" },
    { CPU_RESET_SRC_SLOWWD_MASK16,  "SLOW WATCHDOG" },
    { 0                         }  // the label part == 0  marks the end of the table
}
#endif
;


#if FGC_CLASS_ID == 60
LABEL_VARS_EXT const struct sym_name st_latched_lbl[]
#ifdef LABEL_GLOBALS
        =
{
    { FGC_LAT_PSU_V_FAIL,       "PSU_V_FAIL"    },
    //  { FGC_LAT_DIAG_FLT,         "DIAG_FLT"  },
    { FGC_LAT_DIM_SYNC_FLT,     "DIM_SYNC_FLT"  },
    { FGC_LAT_FBS_FLT,          "FBS_FLT"   },
    { FGC_LAT_DSP_FLT,          "DSP_FLT"   },
    { FGC_LAT_DIG_FLT,          "DIG_FLT"   },
    { FGC_LAT_RFC_FLT,          "RFC_FLT"   },
    { FGC_LAT_MEM_FLT,          "MEM_FLT"   },
    { FGC_LAT_CODE_FLT,         "CODE_FLT"  },
    { FGC_LAT_DAC_FLT,          "DAC_FLT"   },
    { 0                     }  // the label part == 0  marks the end of the table
}
#endif
;
#endif

//-----------------------------------------------------------------------------------------------------------

#endif  // LABEL_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: label.h
\*---------------------------------------------------------------------------------------------------------*/
