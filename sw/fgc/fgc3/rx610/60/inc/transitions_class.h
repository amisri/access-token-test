/*---------------------------------------------------------------------------------------------------------*\
  File:         transitions_class.h

  Contents:

  Notes:


\*---------------------------------------------------------------------------------------------------------*/

#ifndef TRANSITIONS_CLASS_H      // header encapsulation
#define TRANSITIONS_CLASS_H

#ifdef TRANSITIONS_CLASS_GLOBALS
#define TRANSITIONS_CLASS_VARS_EXT
#else
#define TRANSITIONS_CLASS_VARS_EXT extern
#endif
//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>           // basic typedefs
#include <state_class.h>        // for StateXX ...
#include <macros.h>             // for ArrayLen()

//-----------------------------------------------------------------------------------------------------------

// Transition function constants

// each one has it corresponding function
enum state_pc_transitions
{
    tr_OFtoFO,                  //  0 off to fault (off)
    tr_FStoFO,                  //  1 fault (stopping) to fault (off)
    tr_FOtoOF,                  //  2 fault (off) to off
    tr_SPtoOF,                  //  3 stopping to off
    tr_STtoFS,                  //  4 starting to fault (stopping)
    tr_XXtoFS,                  //  5 ??? to fault (stopping)
    tr_STtoSP,                  //  6 starting to stopping
    tr_XXtoSP,                  //  7 ??? to stopping
    tr_OFtoST,                  //  8 off to starting
    tr_XXtoSA,                  //  9 ??? to slow abort
    tr_STtoTS,                  // 10 starting to to_standBy
    tr_XXtoTS,                  // 11 ??? to to_standBy
    tr_TStoSB,                  // 12 to_standBy to standBy
    tr_TStoAB,                  // 13 to_standBy to aborting
    tr_SBtoIL,                  // 14 standBy to idle
    tr_ARtoIL,                  // 15 armed to idle
    tr_RNtoIL,                  // 16 running to idle
    tr_ABtoIL,                  // 17 aborting to idle
    tr_SAtoAB,                  // 18 slow_abort to aborting
    tr_ILtoTC,                  // 19 idle to to_cycling
    tr_ILtoAR,                  // 20 idle to armed
    tr_ARtoRN,                  // 21 armed to running
    tr_RNtoAB,                  // 22 running to aborting
    tr_SBtoTC,                  // 23 standBy to to_cycling
    tr_TCtoCY,                  // 24 to_cycling to cycling (running pulse to pulse modulation)
    tr_STtoSB,                  // 25 starting to on_standBy
    PC_STATE_MACHINE_TRANSITIONS_RANGE // for use with the loops
};

struct TTransitionBehaviour
{
    uint16_t(* const     check)(void);    // the function checks if the requirements for this transition are meet
    const uint8_t         end_state;      // target state of the transition
};

struct TStateBehaviour
{
    void (* const play_state)(bool);     // state function
    const uint8_t number_of_paths;            // number of possible transitions from this state
    const uint8_t * const lst;                // list of possible transitions (bifurcations) from this state
};

//-----------------------------------------------------------------------------------------------------------

// Transition functions

uint16_t  OFtoFO(void);           //  0
uint16_t  FStoFO(void);           //  1
uint16_t  FOtoOF(void);           //  2
uint16_t  SPtoOF(void);           //  3
uint16_t  STtoFS(void);           //  4
uint16_t  XXtoFS(void);           //  5
uint16_t  STtoSP(void);           //  6
uint16_t  XXtoSP(void);           //  7
uint16_t  OFtoST(void);           //  8
uint16_t  XXtoSA(void);           //  9
uint16_t  STtoTS(void);           // 10
uint16_t  XXtoTS(void);           // 11
uint16_t  TStoSB(void);           // 12
uint16_t  TStoAB(void);           // 13
uint16_t  SBtoIL(void);           // 14
uint16_t  ARtoIL(void);           // 15
uint16_t  RNtoIL(void);           // 16
uint16_t  ABtoIL(void);           // 17
uint16_t  SAtoAB(void);           // 18
uint16_t  ILtoTC(void);           // 19
uint16_t  ILtoAR(void);           // 20
uint16_t  ARtoRN(void);           // 21
uint16_t  RNtoAB(void);           // 22
uint16_t  SBtoTC(void);           // 23
uint16_t  TCtoCY(void);           // 24
uint16_t  STtoSB(void);           // 25

//-----------------------------------------------------------------------------------------------------------

//  State variables & functions(14)

// the order is based in enum pc_state_machine_states !!!!

TRANSITIONS_CLASS_VARS_EXT const char pc_str[2 * (PC_STATE_MACHINE_STATES_RANGE)]
#ifdef TRANSITIONS_CLASS_GLOBALS
    = "FOOFFSSPSTSATSSBILTCARRNABCY"
      /*
      FO    FGC_PC_FLT_OFF,           // 0 on fault, off
      OF    FGC_PC_OFF,               // 1 off
      FS    FGC_PC_FLT_STOPPING,      // 2 on fault, stopping
      SP    FGC_PC_STOPPING,          // 3 stopping
      ST    FGC_PC_STARTING,          // 4 starting
      SA    FGC_PC_SLOW_ABORT,        // 5 slow abort
      TS    FGC_PC_TO_STANDBY,        // 6 to_standBy
      SB    FGC_PC_ON_STANDBY,        // 7 on_standBy
      IL    FGC_PC_IDLE,              // 8 idle
      TC    FGC_PC_TO_CYCLING,        // 9 to_cycling
      AR    FGC_PC_ARMED,             // 10 armed
      RN    FGC_PC_RUNNING,           // 11 running (DC)
      AB    FGC_PC_ABORTING,          // 12 aborting
      CY    FGC_PC_CYCLING,           // 13 cycling (running pulse to pulse modulation)

      printed like this

          char *              p;

          p = pc_str + 2 * state_pc;
          TermPutc(*(p++),rtd.f);
          TermPutc(*p,rtd.f);

      */
#endif
      ;

// the order is based in enum state_pc_transitions !!!!
TRANSITIONS_CLASS_VARS_EXT const struct TTransitionBehaviour
        transitionBehav[PC_STATE_MACHINE_TRANSITIONS_RANGE]
#ifdef TRANSITIONS_CLASS_GLOBALS
        =
{
    {OFtoFO, FGC_PC_FLT_OFF     },          //  0     tr_OFtoFO      off to fault (off)
    {FStoFO, FGC_PC_FLT_OFF     },          //  1     tr_FStoFO      fault (stopping) to fault (off)
    {FOtoOF, FGC_PC_OFF         },          //  2     tr_FOtoOF      fault (off) to off
    {SPtoOF, FGC_PC_OFF         },          //  3     tr_SPtoOF      stopping to off
    {STtoFS, FGC_PC_FLT_STOPPING},          //  4     tr_STtoFS      starting to fault (stopping)
    {XXtoFS, FGC_PC_FLT_STOPPING},          //  5     tr_XXtoFS      ??? to fault (stopping)
    {STtoSP, FGC_PC_STOPPING    },          //  6     tr_STtoSP      starting to stopping
    {XXtoSP, FGC_PC_STOPPING    },          //  7     tr_XXtoSP      ??? to stopping
    {OFtoST, FGC_PC_STARTING    },          //  8     tr_OFtoST      off to starting
    {XXtoSA, FGC_PC_SLOW_ABORT  },          //  9     tr_XXtoSA      ??? to slow abort
    {STtoTS, FGC_PC_TO_STANDBY  },          // 10     tr_STtoTS      starting to to_standBy
    {XXtoTS, FGC_PC_TO_STANDBY  },          // 11     tr_XXtoTS      ??? to to_standBy
    {TStoSB, FGC_PC_ON_STANDBY  },          // 12     tr_TStoSB      to_standBy to on_standBy
    {TStoAB, FGC_PC_ABORTING    },          // 13     tr_TStoAB      to_standBy to aborting
    {SBtoIL, FGC_PC_IDLE        },          // 14     tr_SBtoIL      standBy to idle
    {ARtoIL, FGC_PC_IDLE        },          // 15     tr_ARtoIL      armed to idle
    {RNtoIL, FGC_PC_IDLE        },          // 16     tr_RNtoIL      running to idle
    {ABtoIL, FGC_PC_IDLE        },          // 17     tr_ABtoIL      aborting to idle
    {SAtoAB, FGC_PC_ABORTING    },          // 18     tr_SAtoAB      slow abort to aborting
    {ILtoTC, FGC_PC_TO_CYCLING  },          // 19     tr_ILtoTC      idle to to_cycling
    {ILtoAR, FGC_PC_ARMED       },          // 20     tr_ILtoAR      idle to armed
    {ARtoRN, FGC_PC_RUNNING     },          // 21     tr_ARtoRN      armed to running
    {RNtoAB, FGC_PC_ABORTING    },          // 22     tr_RNtoAB      running to aborting
    {SBtoTC, FGC_PC_TO_CYCLING  },          // 23     tr_SBtoTC      on_standby to to_cycling
    {TCtoCY, FGC_PC_CYCLING     },          // 24     tr_TCtoCY      to_cycling to cycling (running pulse to pulse modulation)
    {STtoSB, FGC_PC_ON_STANDBY  }           // 25     tr_STtoSB      starting to on_standby
}
#endif
;

// the 1st transitions checked is the rightmost and the last checked is the leftmost
// the first one that meet the requirements is the one chosen

#ifdef TRANSITIONS_CLASS_GLOBALS
const uint8_t transition_paths_from_FO[] = { tr_FOtoOF,};
const uint8_t transition_paths_from_OF[] = { tr_OFtoFO, tr_OFtoST,};
const uint8_t transition_paths_from_FS[] = { tr_FStoFO,};
const uint8_t transition_paths_from_SP[] = { tr_XXtoFS, tr_SPtoOF,};
const uint8_t transition_paths_from_ST[] = { tr_STtoFS, tr_STtoSP, tr_STtoTS, tr_STtoSB,};
const uint8_t transition_paths_from_SA[] = { tr_XXtoFS, tr_XXtoSP, tr_SAtoAB,};
const uint8_t transition_paths_from_TS[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_TStoAB, tr_TStoSB,};
const uint8_t transition_paths_from_SB[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_SBtoIL, tr_SBtoTC,};
const uint8_t transition_paths_from_IL[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS, tr_ILtoAR, tr_ILtoTC,};
const uint8_t transition_paths_from_TP[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoTS, tr_TCtoCY,};
const uint8_t transition_paths_from_AR[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS, tr_ARtoIL, tr_ARtoRN,};
const uint8_t transition_paths_from_RN[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS, tr_RNtoIL, tr_RNtoAB,};
const uint8_t transition_paths_from_AB[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoSA, tr_XXtoTS, tr_ABtoIL,};
const uint8_t transition_paths_from_PP[] = { tr_XXtoFS, tr_XXtoSP, tr_XXtoTS,};
#endif

// the order is based in enum pc_state_machine_states !!!!
// The order must match the state constant values in the XML
TRANSITIONS_CLASS_VARS_EXT const struct TStateBehaviour stateBehav[PC_STATE_MACHINE_STATES_RANGE]
#ifdef TRANSITIONS_CLASS_GLOBALS
        =
{
    {StateFO, ArrayLen(transition_paths_from_FO), transition_paths_from_FO},    // FGC_PC_FLT_OFF
    {StateOF, ArrayLen(transition_paths_from_OF), transition_paths_from_OF},    // FGC_PC_OFF
    {StateFS, ArrayLen(transition_paths_from_FS), transition_paths_from_FS},    // FGC_PC_FLT_STOPPING
    {StateSP, ArrayLen(transition_paths_from_SP), transition_paths_from_SP},    // FGC_PC_STOPPING
    {StateST, ArrayLen(transition_paths_from_ST), transition_paths_from_ST},    // FGC_PC_STARTING
    {StateSA, ArrayLen(transition_paths_from_SA), transition_paths_from_SA},    // FGC_PC_SLOW_ABORT
    {StateTS, ArrayLen(transition_paths_from_TS), transition_paths_from_TS},    // FGC_PC_TO_STANDBY
    {StateSB, ArrayLen(transition_paths_from_SB), transition_paths_from_SB},    // FGC_PC_ON_STANDBY
    {StateIL, ArrayLen(transition_paths_from_IL), transition_paths_from_IL},    // FGC_PC_IDLE
    {StateTC, ArrayLen(transition_paths_from_TP), transition_paths_from_TP},    // FGC_PC_TO_CYCLING
    {StateAR, ArrayLen(transition_paths_from_AR), transition_paths_from_AR},    // FGC_PC_ARMED
    {StateRN, ArrayLen(transition_paths_from_RN), transition_paths_from_RN},    // FGC_PC_RUNNING
    {StateAB, ArrayLen(transition_paths_from_AB), transition_paths_from_AB},    // FGC_PC_ABORTING
    {StateCY, ArrayLen(transition_paths_from_PP), transition_paths_from_PP}     // FGC_PC_CYCLING
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // TRANSITIONS_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: transitions_class.h
\*---------------------------------------------------------------------------------------------------------*/
