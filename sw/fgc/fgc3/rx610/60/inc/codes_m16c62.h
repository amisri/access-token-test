/*---------------------------------------------------------------------------------------------------------*\
  File:         codes_m16c62.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef CODES_M16C62_H
#define CODES_M16C62_H



#include <stdint.h>
#include <codes_holder.h>


/*
 * This function will erase M16C62 flash blocks.
 * This routine calls SelectM16C62Dev() to retrieve the list of block to erase.
 * ToDo: this must be done in an explicit way or better arrange in some way in codes_holder_info[]
 */
enum FGC_codes_status_internal EraseC62Dev(enum fgc_code_ids_short_list dev_idx);


/*
 * This function will check the code in an M16C62 flash device.  It return 0 if the code is valid, 1 otherwise.
 */
enum FGC_codes_status_internal CheckC62Dev(enum fgc_code_ids_short_list dev_idx);


/*!
 *  This function will program the code already in the SRAM code buffer to the M16C62 flash
 */
enum FGC_codes_status_internal WriteM16C62dev(enum fgc_code_ids_short_list dev_idx);   // only FGC3

#endif
