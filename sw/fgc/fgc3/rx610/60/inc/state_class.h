/*---------------------------------------------------------------------------------------------------------*\
  File:         state_class.h

  Purpose:

  Author:       Quentin.King@cern.ch

  Notes:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef STATE_CLASS_H      // header encapsulation
#define STATE_CLASS_H

#ifdef STATE_CLASS_GLOBALS
#define STATE_CLASS_VARS_EXT
#else
#define STATE_CLASS_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>           // basic typedefs
#include <state.h>

//-----------------------------------------------------------------------------------------------------------

enum pc_state_machine_states
{
    FGC_PC_FLT_OFF,             // 0 on fault, off
    FGC_PC_OFF,                 // 1 off
    FGC_PC_FLT_STOPPING,        // 2 on fault, stopping
    FGC_PC_STOPPING,            // 3 stopping
    FGC_PC_STARTING,            // 4 starting
    FGC_PC_SLOW_ABORT,          // 5 slow abort
    FGC_PC_TO_STANDBY,          // 6 to_standBy
    FGC_PC_ON_STANDBY,          // 7 on_standBy
    FGC_PC_IDLE,                // 8 idle
    FGC_PC_ARMED,               // 9 armed
    FGC_PC_RUNNING,             // 10 running (DC)
    FGC_PC_ABORTING,            // 11 aborting
    FGC_PC_TO_CYCLING,          // 12 to_ppm
    FGC_PC_CYCLING,             // 13 ppm (running pulse to pulse modulation)
    PC_STATE_MACHINE_STATES_RANGE // for use with the loops
};

/*

from defconst.h         but these are not generated for class 60 nor 50

#define FGC_PC_FLT_OFF           0
#define FGC_PC_OFF               1
#define FGC_PC_FLT_STOPPING      2
#define FGC_PC_STOPPING          3
#define FGC_PC_STARTING          4
#define FGC_PC_SLOW_ABORT        5
#define FGC_PC_TO_STANDBY        6
#define FGC_PC_ON_STANDBY        7
#define FGC_PC_IDLE              8
#define FGC_PC_TO_CYCLING        9
#define FGC_PC_ARMED             10
#define FGC_PC_RUNNING           11
#define FGC_PC_ABORTING          12
#define FGC_PC_CYCLING           13
#define FGC_PC_POL_SWITCHING     14

#define FGC_PC_WITH_ALIASES_FLT_OFF             0
#define FGC_PC_WITH_ALIASES_OF                  1
#define FGC_PC_WITH_ALIASES_OFF                 1
#define FGC_PC_WITH_ALIASES_FLT_STOPPING        2
#define FGC_PC_WITH_ALIASES_STOPPING            3
#define FGC_PC_WITH_ALIASES_STARTING            4
#define FGC_PC_WITH_ALIASES_SLOW_ABORT          5
#define FGC_PC_WITH_ALIASES_SA                  5
#define FGC_PC_WITH_ALIASES_TO_STANDBY          6
#define FGC_PC_WITH_ALIASES_ON_STANDBY          7
#define FGC_PC_WITH_ALIASES_SB                  7
#define FGC_PC_WITH_ALIASES_IDLE                8
#define FGC_PC_WITH_ALIASES_IL                  8
#define FGC_PC_WITH_ALIASES_TO_CYCLING          9
#define FGC_PC_WITH_ALIASES_ARMED               10
#define FGC_PC_WITH_ALIASES_RUNNING             11
#define FGC_PC_WITH_ALIASES_ABORTING            12
#define FGC_PC_WITH_ALIASES_CY                  13
#define FGC_PC_WITH_ALIASES_CYCLING             13
#define FGC_PC_WITH_ALIASES_POL                 14

*/


//-----------------------------------------------------------------------------------------------------------

void    RefClr(void);

//-----------------------------------------------------------------------------------------------------------

#endif  // STATE_CLASS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: state_class.h
\*---------------------------------------------------------------------------------------------------------*/
