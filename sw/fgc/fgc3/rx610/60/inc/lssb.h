/*!
 *	@file 	lssb.h
 *	@brief	Function for determining the Least Significant Set Bit
 */

#pragma once


// Includes

#include <stdint.h>



// External function declarations

/*!
 *	This function returns the Least Significant Set Bit (0-15).
 *	If zero is sent as an argument, it will return 0x1F.
 *
 *	@param[in]	data	16 bit word
 *
 *	@returns			Least Significant Set Bit
 */
uint16_t lssb(uint16_t data);