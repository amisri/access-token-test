/*---------------------------------------------------------------------------------------------------------*\
  File:         ethernet_boot.h

  Purpose:      FGC3

  History:

    Feb 2011    hl      Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef ETH_BOOT_H      // header encapsulation
#define ETH_BOOT_H

#ifdef ETH_BOOT_GLOBALS
#define ETH_BOOT_VARS_EXT
#else
#define ETH_BOOT_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include <structs_bits_big.h>

//-----------------------------------------------------------------------------------------------------------

enum FGC_eth_status
{
    ETH_STATUS_SUCCESS,
    ETH_STATUS_CHIP_INACCESSIBLE,
    ETH_STATUS_INVALID_ID,
    ETH_STATUS_UNKNOWN_CHIP,
};

// Interrupt in INT_STS and INT_EN registers
#define     ETH_RSFL_INT    0x00000008  // RX Status FIFO Level Interrupt
#define     ETH_RSFF_INT    0x00000010  // RX Status FIFO Full Interrupt
#define     ETH_RXDF_INT    0x00000040  // RX dropped frame Interrupt
#define     ETH_TSFL_INT    0x00000080  // TX Status FIFO Level Interrupt
#define     ETH_TSFF_INT    0x00000100  // TX Status FIFO Full Interrupt
#define     ETH_TDFA_INT    0x00000200  // TX Data FIFO Available Interrupt (not enough space)
#define     ETH_TDFO_INT    0x00000400  // TX Data FIFO Overrun Interrupt
#define     ETH_TXE_INT     0x00002000  // TX error
#define     ETH_RXE_INT     0x00004000  // RX error
#define     ETH_TXSO_INT    0x00010000  // TX status overflow
#define     ETH_PHY_INT     0x00040000  // Interrupt from PHY layer
#define     ETH_RXSTOP_INT  0x01000000  // RX stop
#define     ETH_TXSTOP_INT  0x02000000  // TX stop

// ToDo constant below should be in fgc_ether.h
#define FGC_ETHER_MAX_RTERM_CHARS       21          // Max number of remote terminal chars in a status packet (eth)


//-----------------------------------------------------------------------------------------------------------

struct Teth_isr      // for interrupt routine
{
    uint32_t              tx_status;              // status from TX status fifo
    uint32_t              rx_status;              // status from RX status fifo
    uint32_t              dbg_nb_rx_packet;       // count number of rx packet. to be removed
    uint32_t              dbg_nb_tx_packet;       // count number of tx packet. to be removed
    //  uint32_t              dbg_dump_packet[500];   // dump packet. TO BE REMOVED
    uint16_t              dbg_nb_crc_error;           // count number of RX crc error
    uint16_t              dbg_nb_rx_error;        // count number of RX error
    uint16_t              dbg_nb_tx_error;        // count number of TX error
    uint16_t              current_rx_fifo_size;   // track number of bytes read from rx fifo
    union TUnion32Bits  rx_fifo_byte_buf;       // remaining bytes from rx fifo
    uint16_t              rx_fifo_byte_buff_size; // number of remaining bytes from rx fifo
    // for reception of time packet
    bool timeVar_processed; // Flag to indicate if timeVar has been processed within the 20 Milliseconds
};

//-----------------------------------------------------------------------------------------------------------
// Interface function
enum FGC_eth_status EthInit(void);
void    EthCheck(void);
void    EthISR(void);

/*!
 * Wait for link to be up, without timeout. Wait forever or if ctrl+c is pressed (then goes into standalone mode)
 */
void    EthWaitLinkUp(void);
void    EthTxCh(char ch);

/*!
 * Process packets: (un)packetize data
 */
void    EthReadTimeVar(void);
void    EthReadCodeVar(void);
void    EthWriteStatVar(void);

//-----------------------------------------------------------------------------------------------------------

ETH_BOOT_VARS_EXT struct Teth_isr               eth_isr;                // eth code variable

//-----------------------------------------------------------------------------------------------------------

#endif  // ETH_BOOT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: ethernet_boot.h
\*---------------------------------------------------------------------------------------------------------*/
