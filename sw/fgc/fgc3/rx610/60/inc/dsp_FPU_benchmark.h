/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp_FPU_benchmark.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DSP_FPU_BENCHMARK_H     // header encapsulation
#define DSP_FPU_BENCHMARK_H

#ifdef DSP_FPU_BENCHMARK_GLOBALS
#define DSP_FPU_BENCHMARK_VARS_EXT
#else
#define DSP_FPU_BENCHMARK_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include <structs_bits_big.h>   // MOTOROLA (big endian) bits, bytes, words, dwords ...

//-----------------------------------------------------------------------------------------------------------

// from NewLib constants

// #define ERANGE 34    //  Math result not representable
// #define HUGE_VAL  (1.0e999999999)
// #define HUGE_VALF (1.0e999999999F)
// #define HUGE_VALL (1.0e999999999L)
// #define MAXFLOAT   3.40282347e+38F

// from optlibinc constants

// #define HUGE_VAL   1.7976931348623158e+308
// #define HUGE_VAL   3.402823466e+38

// DBL_DIG = 15         // Digits of precision
// FLT_DIG = 6          // Digits of precision


#define DSP_TEST_LEN 29


//-----------------------------------------------------------------------------------------------------------
struct TDspBenchmarkInfo
{
    float                rx610_fp32;
    FP64                rx610_fp64;
    float                c6727_fp32;
    FP64                c6727_fp64;

    union TUnion16Bits  rx610_tick_0;
    union TUnion16Bits  rx610_tick_1;
    union TUnion16Bits  rx610_tick_2;
    union TUnion16Bits  c6721_tick_0;
    union TUnion16Bits  c6721_tick_1;
    union TUnion16Bits  c6721_tick_2;

    int16_t      errno_tod;
    int16_t      errno_tof;
};


struct TDspBenchmarkData
{
    const char     *    txt;
};

//-----------------------------------------------------------------------------------------------------------

bool Dsp_FPU_Benchmark(void);

//-----------------------------------------------------------------------------------------------------------

DSP_FPU_BENCHMARK_VARS_EXT const struct TDspBenchmarkData       DSP_TEST_DATA[DSP_TEST_LEN]
#ifdef DSP_FPU_BENCHMARK_GLOBALS
        =
{
    /*  0 */    {"1"},
    /*  1 */    {"10.0"},
    /*  2 */    {"666.666"},
    /*  3 */    {"-666.666"},
    /*  4 */    {"+1.23456789E+12"},
    /*  5 */    {"-1.23456789E-12"},
    /*  6 */    {"1.7976931348623157E+308"},
    /*  7 */    {"1.7976931348623156E+308"},
    /*  8 */    {"1.7976931348623156E+307"},
    /*  9 */    {"2.2250738585072014E-308"},
    /* 10 */    {"2.2250738585072013E-308"},
    /* 11 */    {"2.2250738585072013E-307"},
    /* 12 */    {"3.40282347E+38"},
    /* 13 */    {"3.40282346E+38"},
    /* 14 */    {"3.40282346E+37"},
    /* 15 */    {"1.17549435E-38"},
    /* 16 */    {"1.17549434E-38"},
    /* 17 */    {"1.17549434E-37"},
    /* 18 */    {"1.17549434E+35"},
    /* 19 */    {"1.17549434E+36"},
    /* 20 */    {"-1.2345678e39"},
    /* 21 */    {"8.7654321e-45"},
    /* 22 */    {"1.2345678e-42"},
    /* 23 */    {"1.7976931348623157E+38"},
    /* 24 */    {"1.7976931348623157E+37"},
    /* 25 */    {"1.7976931348623157E+36"},
    /* 26 */    {"2.2250738585072014E-38"},
    /* 27 */    {"2.2250738585072014E-37"},
    /* 28 */    {"2.2250738585072014E-36"}
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // DSP_FPU_BENCHMARK_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_FPU_benchmark.h
\*---------------------------------------------------------------------------------------------------------*/
