/*---------------------------------------------------------------------------------------------------------*\
  File:         jtag_xsv.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef JTAG_XSV_H      // header encapsulation
#define JTAG_XSV_H

#ifdef JTAG_XSV_GLOBALS
#define JTAG_XSV_VARS_EXT
#else
#define JTAG_XSV_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>   // basic typedefs
#include <stdio.h>      // for FILE
#include <jtag.h>
#include <label.h>

//-----------------------------------------------------------------------------------------------------------
#define XC9536           0x29502093
#define XC9572           0x29504093
#define XC95108          0x29506093
#define XC95144          0x29508093
#define XILINX18V        0x05024093
#define XILINX18V01      0x05034093
#define XC3S700AN        0x22628093

// Do NOT change the following constants:
#define ERROR_STA0              0         // Stuck-at-0 error (always expected 1/received 0).
#define ERROR_STA1              1         // Stuck-at-1 error (always expected 0/received 1).
#define ERROR_UNKNOWN           2         // Unknown error (both expected 1/received 0 and expected 0/received 1).

#define  XSV_DEV_MAX            16        // Max nb of device in jtag chain
#define  XSV_FILE_MAX_NB        4
#define  XSV_FILE_NAME_SIZE     8
#define  BSCAN_ERR_PRINT        20
#define  XSV_CS_LOOP_SIZE       4         // Number of shift in checksum loop
#define  XSV_MAX_BSDEVICES      25        // Maximum number of boundary scan (BS) devices in chain.
#define  XSV_MAX_BSDL_TYPES     8         // Maximum number of BS types (BSDLs)in a JTAG chain.
#define  XSV_MAX_CELLS          300       // Maximum number of testable cells (input type cells) in each BS device type.
#define  XSV_MAX_FAIL_PINS      50        // Maximum number of different pins failing in a test (-1).
#define  XSV_MAX_CHARS          20        // Maximum number of chars in a string (bsdl entity names, etc).
#define  FILE_MAX_CHARS         32        // Maximum number of chars in a filename string (filenames, bsdl entity names, etc).
#define  XSVF_XILINX_TYPE       0         // Type of xsvf file.

//------------------ XSVF Command Bytes ---------------------

#define  XILINX36               0x29502093
#define  XILINX72               0x29504093
#define  XILINX108              0x29506093
#define  XILINX144              0x29508093
#define  XILINX18V              0x05024093
#define  XILINX18V01            0x05034093
#define  XC3S700AN              0x22628093

// encodings of xsvf instructions
#define  XCOMPLETE        0
#define  XTDOMASK         1
#define  XSIR             2
#define  XSDR             3
#define  XRUNTEST         4
// Reserved               5
// Reserved               6
#define  XREPEAT          7
#define  XSDRSIZE         8
#define  XSDRTDO          9
#define  XSETSDRMASKS     10
#define  XSDRINC          11
#define  XSDRB            12
#define  XSDRC            13
#define  XSDRE            14
#define  XSDRTDOB         15
#define  XSDRTDOC         16
#define  XSDRTDOE         17
#define  XSTATE           18         // 4.00
#define  XENDIR           19         // 4.04
#define  XENDDR           20         // 4.04
#define  XSIR2            21         // 4.10
#define  XCOMMENT         22         // 4.14
#define  XWAIT            23         // 5.00
#define  XCHECKSUM        24         // PFR added
// Insert new commands here
// and add corresponding xsvfDoCmd function to xsvf_pfDoCmd below.
#define  XLASTCMD         25         // Last command marker


//-------------- XSVF Command Parameter Values ----------------------

#define  XSTATE_RESET     0          // 4.00 parameter for XSTATE
#define  XSTATE_RUNTEST   1          // 4.00 parameter for XSTATE

#define  XENDXR_RUNTEST   0          // 4.04 parameter for XENDIR/DR
#define  XENDXR_PAUSE     1          // 4.04 parameter for XENDIR/DR

/* TAP states */
#define  XTAPSTATE_RESET     0x00
#define  XTAPSTATE_RUNTEST   0x01    // a.k.a. IDLE
#define  XTAPSTATE_SELECTDR  0x02
#define  XTAPSTATE_CAPTUREDR 0x03
#define  XTAPSTATE_SHIFTDR   0x04
#define  XTAPSTATE_EXIT1DR   0x05
#define  XTAPSTATE_PAUSEDR   0x06
#define  XTAPSTATE_EXIT2DR   0x07
#define  XTAPSTATE_UPDATEDR  0x08
#define  XTAPSTATE_IRSTATES  0x09    // All IR states begin here
#define  XTAPSTATE_SELECTIR  0x09
#define  XTAPSTATE_CAPTUREIR 0x0A
#define  XTAPSTATE_SHIFTIR   0x0B
#define  XTAPSTATE_EXIT1IR   0x0C
#define  XTAPSTATE_PAUSEIR   0x0D
#define  XTAPSTATE_EXIT2IR   0x0E
#define  XTAPSTATE_UPDATEIR  0x0F

// Legacy error codes for xsvfExecute from original XSVF player v2.0
#define XSVF_LEGACY_SUCCESS             1
#define XSVF_LEGACY_ERROR               0

#define XSVF_ERROR_NONE                 0
#define XSVF_ERROR_UNKNOWN              1
#define XSVF_ERROR_TDOMISMATCH          2
#define XSVF_ERROR_MAXRETRIES           3       // TDO mismatch after max retries
#define XSVF_ERROR_ILLEGALCMD           4
#define XSVF_ERROR_ILLEGALSTATE         5
#define XSVF_ERROR_DATAOVERFLOW         6       // Data > lenVal LENVAL_MAX buffer size
#define XSVF_ERROR_ILLEGALJUMP_CS       7       // should not jump to xsvfDoXCHECKSUM
#define XSVF_NO_POWER                   8
#define XSVF_BUS_NOT_RDY                9       // JTAG bus not ready
#define XSVF_ABORT                      10      // sequence aborted by user
#define XSVF_BAD_MODE                   11      // bad bscan mode
#define XSVF_ERROR_LAST                 12

//-----------------------------------------------------------------------------------------------------------

struct xsv_file
{
    uint8_t    *   name;                          // name of the file
    uint16_t      file_type;                      // xsv file type (prog, verify, infra or inter)
    uint8_t    *   start_addr;                    // pointer to 1st data of XSV file
    uint32_t      file_size;                      // XSV file size
};

struct xsv_vars
{
    uint16_t              error;
    uint16_t              mode;                   // sig, infra, inter, ...
    uint32_t              size;
    uint32_t              bytes_read;
    uint8_t        *       data;                  // pointer to the current data
    uint16_t              id_index;
    uint32_t              ids[XSV_DEV_MAX];
    uint16_t              step_flag;              // step by step option
    struct xsv_file     files[XSV_FILE_MAX_NB]; // table of xsv files
};


struct bscan
{
    uint16_t      error_ct;
    uint16_t      history_idx;
    uint16_t      history[XSV_MAX_FAIL_PINS];
    uint16_t      err_position[XSV_MAX_FAIL_PINS];        // position of the failing bit in bscan vector
    uint16_t
    rec_val[XSV_MAX_FAIL_PINS];             // received value of the failing bit (0,1 or 2 (2 for unknown))
};

// --- XSVF Type Declarations ---

struct SXsvfInfo
{
    // XSVF status information
    uint8_t   ucComplete;         // 0 = running; 1 = complete
    uint8_t   ucCommand;          // Current XSVF command byte
    uint32_t  lCommandCount;      // Number of commands processed
    uint16_t  xsv_error;         // An error code. 0 = no error.

    // TAP state/sequencing information
    uint8_t   ucTapState;         // Current TAP state
    uint8_t   ucEndIR;            // ENDIR TAP state (See SVF)
    uint8_t   ucEndDR;            // ENDDR TAP state (See SVF)

    // RUNTEST information
    uint8_t   ucMaxRepeat;        // Max repeat loops (for xc9500/xl)
    uint32_t  lRunTestTime;       // Pre-specified RUNTEST time (usec)

    // Shift Data Info and Buffers
    uint32_t           lShiftLengthBits;   // Len. current shift data in bits
    int16_t           sShiftLengthBytes;  // Len. current shift data in bytes

    struct lenVal          lvTdi;              // Current TDI shift data
    struct lenVal          lvTdoExpected;      // Expected TDO shift data
    struct lenVal          lvTdoCaptured;      // Captured TDO shift data
    struct lenVal          lvTdoMask;          // TDO mask: 0=dontcare; 1=compare
    struct lenVal          lvXOR;              // XOR of all the Captured vectors
    struct lenVal          lvPrevCap;          // Previous Captured vector

    struct lenVal          lvAddressMask;      // Address mask for XSDRINC
    struct lenVal          lvDataMask;         // Data mask for XSDRINC
    struct lenVal          lvNextData;         // Next data for XSDRINC
};


// Declare pointer to functions that perform XSVF commands
typedef uint16_t(*TXsvfDoCmdFuncPtr)(void);

// --- former testing.h vars ---
struct xsvfTInfo
{
    FILE * resultsFile;         // File where the results from the execution of the xsvf for test are stored.
    FILE * xdiagnosisFile;      // XDiagnosis file.
    FILE * jtagErrorFile;       // JTAG error file.
    FILE * jtagErrorTemp;       // Temporary jtag error file. It is necessary to write the total number of errors
    // in the JTAG error file before writing the errors. This temporary file stores the
    // errors until they are accounted and ready to be written in the JTAG error file.


    char xdiagnosisFileName[FILE_MAX_CHARS];   // Must be: design_name.XDA.
    char jtagErrorFileName[FILE_MAX_CHARS];    // Must be: test_name.ERR (inter.err or infra.err for example).
    char resultsFileName[FILE_MAX_CHARS];      // Must be: test_name.RST.
    char GENfileName[FILE_MAX_CHARS];
    char APLfileName[FILE_MAX_CHARS];

    char designName[XSV_MAX_CHARS];   // Must be known from the beginning
    char testName[XSV_MAX_CHARS];

    // Information about the errors
    uint16_t scanFailBit;         /* Bit that is failing within a data shift.
                                 Goes from 1 to chain[BSRegister] (Inter test) or chain[IDRegister] (Infra test).
     (sum of registers in all devices in the chain */
    uint16_t scanFailCell; /* Order of the cell, within a device, that is failing in a data shift.
     Goes from 0 to device[BSRegister] or device[IDRegister]. */
    uint16_t deviceIndex;         // Device in which a cell is failing (Index within the array of devices)
    uint16_t cellIndex;           // Input cell which is failing (Index within the array of cells)

    uint16_t interVectorNumber;   // Vector number in an interconnection test.

    uint16_t pass_fail;           // Indicates if the test passed or not (PASS or FAIL).

    uint16_t totalErrors;         // Total number of errors.

    uint16_t receivedBit;         // Bit actually received in a cell.

};

struct failPins
{
    uint16_t deviceIndex;           // Index of device where the error occurred, within the array of devices.
    uint16_t errorType;             // ERROR_STA0, ERROR_STA1 or ERROR_UNKNOWN.
    // In an INFRA test these have the meaning:
    //  -  ERROR_STA0: Bit expected 1 /received 0
    //  -  ERROR_STA1: Bit expected 0 /received 1
    uint16_t scanFailBit;           // Bit that is failing within a data shift (INTER test) or within a device (INFRA test).
    char pinDes[XSV_MAX_CHARS];   // Pin designator 1, 20, A23, F40, etc..
    // Not used in INFRA test.
};

//-----------------------------------------------------------------------------------------------------------

uint16_t  xsvfExecute(void);
void    xsvfResultsSummary(struct lenVal * plvXOR, struct lenVal * plvTDOCaptured);
void    xsvfPrintLenVal(struct lenVal * ptr_lv);
void    xsvfTmsTransition(uint8_t sTms);
void    xsvfShiftOnly(uint32_t lNumBits, struct lenVal * plvTdi,
                      struct lenVal * plvTdoCaptured, uint16_t iExitShift);
void    xsvfDoSDRMasking(struct lenVal * plvTdi, struct lenVal * plvNextData,
                         struct lenVal * plvAddressMask, struct lenVal * plvDataMask);
uint16_t  CheckFScanf(uint16_t temp);
uint16_t  xsvfStrCompare(const char * csta, const char * buff);
uint16_t  xsvfIdentifyFailingBits(uint16_t testChar, uint16_t testCap, uint16_t byteIndex);
int16_t  xsvfFindDataShiftErrors(struct lenVal * plvTdoExpected,
                                struct lenVal * plvTdoCaptured, struct lenVal * plvTdoMask,
                                struct lenVal * plvXOR, struct lenVal * plvPrevCap);
int16_t  xsvfGetAsNumBytes(uint32_t lNumBits);
uint16_t  xsvfGotoTapState(uint8_t * pucTapState, uint8_t ucTargetState);
uint16_t  xsvfShift(uint8_t * pucTapState, uint8_t ucStartState, uint32_t lNumBits,
                  struct lenVal    *    plvTdi, struct lenVal    *    plvTdoCaptured,
                  struct lenVal    *    plvTdoExpected, struct lenVal    *    plvTdoMask,
                  struct lenVal    *    plvXOR, struct lenVal    *    plvPrevCap,
                  uint8_t ucEndState, uint32_t lRunTestTime, uint8_t ucMaxRepeat);
uint16_t  xsvfBasicXSDRTDO(uint8_t * pucTapState, uint32_t lShiftLengthBits,
                         int16_t sShiftLengthBytes, struct lenVal * plvTdi,
                         struct lenVal * plvTdoCaptured, struct lenVal * plvTdoExpected,
                         struct lenVal * plvTdoMask, struct lenVal * plvXOR,
                         struct lenVal * plvPrevCap, uint8_t ucEndState, uint32_t lRunTestTime,
                         uint8_t  ucMaxRepeat);
uint16_t  xsvfInfoInit(void);
uint16_t  xsvfDoIllegalCmd(void);
uint16_t  xsvfDoXCOMPLETE(void);
uint16_t  xsvfDoXTDOMASK(void);
uint16_t  xsvfDoXSIR(void);
uint16_t  xsvfDoXSIR2(void);
uint16_t  xsvfDoXSDR(void);
uint16_t  xsvfDoXRUNTEST(void);
uint16_t  xsvfDoXREPEAT(void);
uint16_t  xsvfDoXSDRSIZE(void);
uint16_t  xsvfDoXSDRTDO(void);
uint16_t  xsvfDoXSETSDRMASKS(void);
uint16_t  xsvfDoXSDRINC(void);
uint16_t  xsvfDoXSDRBCE(void);
uint16_t  xsvfDoXSDRTDOBCE(void);
uint16_t  xsvfDoXSTATE(void);
uint16_t  xsvfDoXENDXR(void);
uint16_t  xsvfDoXCOMMENT(void);
uint16_t  xsvfDoXWAIT(void);
uint16_t  xsvfDoXCHECKSUM(void);
uint16_t  xsvfRun(void);

//-----------------------------------------------------------------------------------------------------------

JTAG_XSV_VARS_EXT struct bscan          bscan_s;


JTAG_XSV_VARS_EXT struct xsv_vars       xsv;
JTAG_XSV_VARS_EXT uint16_t                xsv_iDebugLevel;

// --- XSVF Type Declarations ---

JTAG_XSV_VARS_EXT struct SXsvfInfo      xsvfInfo;
JTAG_XSV_VARS_EXT struct xsvfTInfo      xsvfTestInfo;
JTAG_XSV_VARS_EXT struct failPins       failingPins[XSV_MAX_FAIL_PINS + 1];  // Array of errors;

// Array of XSVF command functions.  Must follow command byte value order!
// If your compiler cannot take this form, then convert to a switch statement


JTAG_XSV_VARS_EXT TXsvfDoCmdFuncPtr     xsvf_pfDoCmd[]
#ifdef JTAG_XSV_GLOBALS
=
{
    xsvfDoXCOMPLETE,        /*  0 */
    xsvfDoXTDOMASK,         /*  1 */
    xsvfDoXSIR,             /*  2 */
    xsvfDoXSDR,             /*  3 */
    xsvfDoXRUNTEST,         /*  4 */
    xsvfDoIllegalCmd,       /*  5 */
    xsvfDoIllegalCmd,       /*  6 */
    xsvfDoXREPEAT,          /*  7 */
    xsvfDoXSDRSIZE,         /*  8 */
    xsvfDoXSDRTDO,          /*  9 */
    xsvfDoXSETSDRMASKS,     /* 10 */
    xsvfDoXSDRINC,          /* 11 */
    xsvfDoXSDRBCE,          /* 12 */
    xsvfDoXSDRBCE,          /* 13 */
    xsvfDoXSDRBCE,          /* 14 */
    xsvfDoXSDRTDOBCE,       /* 15 */
    xsvfDoXSDRTDOBCE,       /* 16 */
    xsvfDoXSDRTDOBCE,       /* 17 */
    xsvfDoXSTATE,           /* 18 */
    xsvfDoXENDXR,           /* 19 */
    xsvfDoXENDXR,           /* 20 */
    xsvfDoXSIR2,            /* 21 */
    xsvfDoXCOMMENT,         /* 22 */
    xsvfDoXWAIT,            /* 23 */
    xsvfDoXCHECKSUM,        /* 24 */
    // Insert new command functions here
}
#endif
;

JTAG_XSV_VARS_EXT char     *    xsvf_pzCommandName[]
#ifdef JTAG_XSV_GLOBALS
=
{
    "XCOMPLETE",
    "XTDOMASK",
    "XSIR",
    "XSDR",
    "XRUNTEST",
    "Reserved5",
    "Reserved6",
    "XREPEAT",
    "XSDRSIZE",
    "XSDRTDO",
    "XSETSDRMASKS",
    "XSDRINC",
    "XSDRB",
    "XSDRC",
    "XSDRE",
    "XSDRTDOB",
    "XSDRTDOC",
    "XSDRTDOE",
    "XSTATE",
    "XENDIR",
    "XENDDR",
    "XSIR2",
    "XCOMMENT",
    "XWAIT",
    "XCHECKSUM",
}
#endif
;

JTAG_XSV_VARS_EXT char     *    xsvf_pzTapState[]
#ifdef JTAG_XSV_GLOBALS
=
{
    "RESET",        /* 0x00 */
    "RUNTEST/IDLE", /* 0x01 */
    "DRSELECT",     /* 0x02 */
    "DRCAPTURE",    /* 0x03 */
    "DRSHIFT",      /* 0x04 */
    "DREXIT1",      /* 0x05 */
    "DRPAUSE",      /* 0x06 */
    "DREXIT2",      /* 0x07 */
    "DRUPDATE",     /* 0x08 */
    "IRSELECT",     /* 0x09 */
    "IRCAPTURE",    /* 0x0A */
    "IRSHIFT",      /* 0x0B */
    "IREXIT1",      /* 0x0C */
    "IRPAUSE",      /* 0x0D */
    "IREXIT2",      /* 0x0E */
    "IRUPDATE"      /* 0x0F */
}
#endif
;

JTAG_XSV_VARS_EXT const struct sym_name xsv_err_lbl[]
#ifdef JTAG_XSV_GLOBALS
        =
{
    { XSVF_ERROR_NONE,                  "No error"                      },
    { XSVF_ERROR_UNKNOWN,               "Unknown"                       },
    { XSVF_ERROR_TDOMISMATCH,           "TDO mismatch"                  },
    { XSVF_ERROR_MAXRETRIES,            "Data error or timeout"         },
    { XSVF_ERROR_ILLEGALCMD,            "Bad XSV cmd"                   },
    { XSVF_ERROR_ILLEGALSTATE,          "Illegal state"                 },
    { XSVF_ERROR_DATAOVERFLOW,          "Overflw LENVAL_MAX size"       },
    { XSVF_ERROR_ILLEGALJUMP_CS,        "Illegal jump"                  },
    { XSVF_NO_POWER,                    "No power"                      },
    { XSVF_BUS_NOT_RDY,                 "Bus not ready"                 },
    { XSVF_ABORT,                       "Aborted by user"               },
    { XSVF_BAD_MODE,                    "Bad JTAG mode"                 },
    { 0, "" }  // the label part == 0  marks the end of the table
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

#endif  // JTAG_XSV_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: jtag_xsv.h
\*---------------------------------------------------------------------------------------------------------*/

