/*---------------------------------------------------------------------------------------------------------*\
  File:     runlog_lib.h

  Purpose:  FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef RUNLOG_LIB_H        // header encapsulation
#define RUNLOG_LIB_H

#include <stdint.h>
#include <fgc_runlog_entries.h>
#include <fgc_runlog.h>     // the item structure for the run log buffer
#include <memmap_mcu.h>

#ifdef RUNLOG_LIB_GLOBALS
#define RUNLOG_LIB_VARS_EXT
#else
#define RUNLOG_LIB_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

struct TRunlogResetCounters
{
    uint16_t      all;
    uint16_t      power;
    uint16_t      program;
    uint16_t      manual;
    uint16_t      fast_watchdog;
    uint16_t      slow_watchdog;
    uint16_t      dongle;
    uint16_t      dsp;
};


struct TRunlog
{
    struct fgc_runlog               buffer[FGC_RUNLOG_N_ELS];
    uint16_t                          index;
    struct TRunlogResetCounters     resets;
    uint16_t                          magic;
};

//-----------------------------------------------------------------------------------------------------------

void    RunlogInit(void);
void    RunlogClrResets(void);
void    RunlogWrite(uint8_t id, const void * data);
uint16_t  RunlogRead(uint16_t * index, struct fgc_runlog * runlog_entry);

/*---------------------------------------------------------------------------------------------------------*/
static inline void RunlogTimestamp(void)
/*---------------------------------------------------------------------------------------------------------*\
 Helper function to time-stamp logs.
\*---------------------------------------------------------------------------------------------------------*/
{
    RunlogWrite(FGC_RL_UXTIME, (void *) & (UTC_MCU_UNIXTIME_P));    // Unix time
    RunlogWrite(FGC_RL_MSTIME, (void *) & (UTC_MCU_MS_P));          // Millisecond time
}
//-----------------------------------------------------------------------------------------------------------

#endif  // RUNLOG_LIB_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: runlog_lib.h
\*---------------------------------------------------------------------------------------------------------*/
