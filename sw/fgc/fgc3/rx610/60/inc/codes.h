/*!
 *  @file     codes.h
 *  @defgroup FGC3:MCU boot
 *  @brief    FGC Boot code update functions
 *
 *  Description:
 */

#ifndef CODES_H // header encapsulation
#define CODES_H

#ifdef CODES_GLOBALS
#define CODES_VARS_EXT
#else
#define CODES_VARS_EXT extern
#endif

// Includes

#include <stdbool.h>
#include <assert.h>
#include <stddef.h>

#include <stdint.h>           // basic typedefs
#include <codes_holder.h>
#include <codes_m16c62.h>       // for EraseC62Dev(), CheckC62Dev(), WriteM16C62dev()
#include <codes_mcu.h>          // for EraseMcuBootDev(), EraseMcuDev(), WriteMcuDev(), WriteMcuBootDev(), CheckMcuDev()
#include <codes_pld.h>          // for CheckPldDev(), ErasePldDev(), WritePldDev()
#include <fgc_consts_gen.h>

// Constants

enum FGC_codes_rx_mode
{
    CODES_RX_NONE,
    CODES_RX_FIELDBUS,
    CODES_RX_TERMINAL,
};

enum FGC_codes_update_status
{
    CODES_UPDATE_ONGOING,
    CODES_UPDATE_FINISHED,
    CODES_UPDATE_ABORTED,
    CODES_UPDATE_ERROR,
};

enum FGC_codes_status_internal
{
    CODES_STATUS_SUCCESS,
    CODES_STATUS_NO_MATCHING_CODE,
    CODES_STATUS_TIMEOUT,
    CODES_STATUS_UPDATE_FAILURE,
    CODES_STATUS_ABORTED_BY_USER,
    CODES_STATUS_SRAM_BUFFER_OVERFLOW,
    CODES_STATUS_RX_BUFFER_OVERFLOW,
    CODES_STATUS_UNEXPECTED_BLOCK_INDEX,
    CODES_STATUS_CLASS_ID_MISMATCH,
    CODES_STATUS_CODE_ID_MISMATCH,
    CODES_STATUS_INVALID_CRC,
    CODES_STATUS_ERASE_FAILED,
    CODES_STATUS_WRITE_FAILED,
    CODES_STATUS_CORRUPTED_PLD,
    CODES_STATUS_PLD_INIT_ERROR,
    CODES_STATUS_PLD_CONFIG_ERROR,
    CODES_STATUS_PLD_TIMEOUT,
    CODES_STATUS_C62_INACCESSIBLE,
    CODES_STATUS_C62_INFO_ERROR,
    CODES_STATUS_C62_DEVICE_INACCESSIBLE,
    CODES_STATUS_INVALID_CODE_ID,
};

enum FGC_codes_status
{
    CODES_SUCCESS,
    CODES_M16C62_FAILURE,
    CODES_CORRUPTED_CODE,
    CODES_IDDB_PTDB_MISMATCH,
    CODES_SYSDB_COMPDB_DIMDB_MISMATCH,
    CODES_MAIN_NAMEDB_MISMATCH,
};

CODES_VARS_EXT struct sym_name codes_status_internal_labels[]
#ifdef CODES_GLOBALS
=
{
    { CODES_STATUS_SUCCESS,                 "success"                 },
    { CODES_STATUS_NO_MATCHING_CODE,        "no matching code"        },
    { CODES_STATUS_TIMEOUT,                 "timeout"                 },
    { CODES_STATUS_UPDATE_FAILURE,          "update failure"          },
    { CODES_STATUS_ABORTED_BY_USER,         "aborted by user"         },
    { CODES_STATUS_SRAM_BUFFER_OVERFLOW,    "sram buffer overflow"    },
    { CODES_STATUS_RX_BUFFER_OVERFLOW,      "rx buffer overflow"      },
    { CODES_STATUS_UNEXPECTED_BLOCK_INDEX,  "unexpected block index"  },
    { CODES_STATUS_CLASS_ID_MISMATCH,       "class id mismatch"       },
    { CODES_STATUS_CODE_ID_MISMATCH,        "code id mismatch"        },
    { CODES_STATUS_INVALID_CRC,             "invalid crc"             },
    { CODES_STATUS_ERASE_FAILED,            "erase failed"            },
    { CODES_STATUS_WRITE_FAILED,            "write failed"            },
    { CODES_STATUS_CORRUPTED_PLD,           "corrupted pld"           },
    { CODES_STATUS_PLD_INIT_ERROR,          "pld init error"          },
    { CODES_STATUS_PLD_CONFIG_ERROR,        "pld config error"        },
    { CODES_STATUS_PLD_TIMEOUT,             "pld timeout"             },
    { CODES_STATUS_C62_INACCESSIBLE,        "c62 inaccessible"        },
    { CODES_STATUS_C62_INFO_ERROR,          "c62 info error"          },
    { CODES_STATUS_C62_DEVICE_INACCESSIBLE, "c62 device inaccessible" },
};

static_assert(ArrayLen(codes_status_internal_labels) == CODES_STATUS_C62_DEVICE_INACCESSIBLE + 1,
              "Number of elements in codes_status_internal_labels array doesn't match corresponding enum");
#endif
;

// TODO Add static assert

/*! Maximum numbers of attempts to flash M16C62 IDBoot */
#define CODE_M16C62_MAX_INITS   3

// Internal structures, unions and enumerations

struct code_rx_buffer
{
    struct code_block * volatile in_blk;         //!< Block in pointer
    struct code_block *          next_in_blk;    //!< Next block in pointer
    struct code_block * volatile out_blk;        //!< Block out pointer
    bool                         overflow;       //!< Buffer overflow flag
};

struct code_rx
{
    enum FGC_codes_rx_mode               mode;                          //!< Reception mode: FIELDBUS or TERMINAL. If None, code vars are discarded
    bool                                 first_block;
    volatile uint16_t                    timeout_ms;                    //!< Millisecond timeout down counter
    volatile uint16_t                    term_stream_timeout_ms;        //!< Millisecond timeout down counter
    uint16_t                               last_blk_idx;                  //!< Last block index for received code
    uint32_t                             reception_mask;                //!< Mask of codes requested from the gateway
    struct code_rx_buffer                buffer;                        //!< Code reception buffer
};

struct code
{
    struct code_rx                       rx;
    struct advertised_code_info volatile adv      [FGC_CODE_MAX_SLOTS];    //!< Advertised code information per GW slot
    enum FGC_codes_adv_code_state        adv_state[FGC_CODE_MAX_SLOTS];    //!< State of each advertised codes
};

// Internal function declarations

/*!
 * This function will select the right codes depending on the crate type ("bus type")
 */
void CodesSelectMainProg(void);
void CodesUpdateAdvCode(void);
void CodesVerifyNvram(void);
bool CodesIdIsValid(const uint16_t class_id);
void CodesCountFlashLocks(void);
enum FGC_codes_status CodesVerifyInstalled(void);
void CodesResetUpdateFailures(void);

/*
 * Sets the device name and the GW name using the information in the NameDB
 */
void CodesSetNameFromNameDb(struct fgc_dev_name * const name, const uint16_t fieldbus_id);

/*!
 * This function is called when the fieldbus is initialised to mark all the advertised codes as NOT_IN_USE.
 * This is because the gateway may have fewer slots than the maximum allowed in the FGC.
 */
void CodesInitAdvTable(void);

/*!
 * This function will check all the installed codes against the advertised codes from the gateway.  It
 * counts how many writable and read-only devices need updating and sets the update state for each device.
 */
void CodesCheckUpdates(void);

/*!
 * This function will receive code blocks from the fieldbus or terminal interfaces and will process each block
 * according to it's header: class_id, code_id, block_idx.
 * When the rx_mode is TERMINAL, one code will be received and programmed.
 * When rx_mode is FIELDBUS, all outstanding codes that require updating will be updated.
 * This may take several revolutions of the gateway's code broadcast cycle if packets are dropped.
 *
 * @param rx_mode code reception mode
 */
void CodesUpdate(const enum FGC_codes_rx_mode rx_mode);

/*!
 *  @brief Flashes the M16C62 boot program (IDBoot) if it is corrupted
 *
 *  The function powers on the M16C62 and checks if the IDBoot program installed on it is responding.
 *  If it is not (or there are other problems with the chip) it tries to flash the boot. The M16C62 chip
 *  is powered off after the check. If the boot was flashed then the function restarts the FGC. The function
 *  assumes that the chip is broken if there were three failed attempts to flash the boot - it does not
 *  check it anymore after that. Also, the FGC is forced to remain in boot after a restart if that happens.
 */
enum FGC_codes_status CodesInitM16C62(void);

/*!
 * This function will store a 32 byte code block into the SRAM code buffer.
 *
 * @param dev_idx code id of device the block belongs to.
 * @param block pointer to block to be stored.
 *
 * @retval error code.
 */
enum FGC_codes_status_internal CodesStoreBlockIntoBuffer(enum fgc_code_ids_short_list dev_idx, const struct code_block * const block);


/*!
 * [CodesGetNumRwDevsToUpdate description]
 * @return  [description]
 */
uint16_t CodesGetNumRwDevsToUpdate(void);

/*
 * The function checks whether NameDB has been installed or not.
 */
bool CodesNameDbInstalled(void);
bool CodesNameDbUpdateIsPending(void);
void CodesUpdateAll(void);
void CodesUpdateInstalledInfo(enum fgc_code_ids_short_list code_id_start, enum fgc_code_ids_short_list code_id_end);
void CodesUpdateAdvertisedInfo(void);
void CodesUpdateSharedMemVersions(void);

/*
 *  Installs NameDB before any other code
 */
void CodesInstallNameDb(void);

void CodesSetNameUnknown(struct fgc_dev_name * name);

static inline bool CodesDevIsInNameDb(const uint16_t fieldbus_id)
{
    const struct fgc_dev_name * const name_db = (const struct fgc_dev_name *)codes_holder_info[SHORT_LIST_CODE_NAMEDB].base_addr;

    return (name_db[fieldbus_id].class_id != FGC_CLASS_UNKNOWN);
}

// External variable definitions

/*! Code functions arrays : Erase flash device functions */
CODES_VARS_EXT enum FGC_codes_status_internal (* const erase_dev[])(enum fgc_code_ids_short_list dev_idx)
#ifdef CODES_GLOBALS
=
{
    ErasePldDev,        // SHORT_LIST_CODE_PLD,         Xilinx FPGA bitstream eeprom
    EraseMcuBootDev,    // SHORT_LIST_CODE_BT,          Boot for operation
    EraseMcuDev,        // SHORT_LIST_CODE_MP_DSP,      Main programs
    EraseC62Dev,        // SHORT_LIST_CODE_IDPROG,      C62 block 3    (IdProg)
    EraseC62Dev,        // SHORT_LIST_CODE_IDDB,        C62 blocks 4-6 (IDDB)
    EraseC62Dev,        // SHORT_LIST_CODE_PTDB,        C62 block 1    (PTDB)
    EraseMcuDev,        // SHORT_LIST_CODE_SYSDB,       Sys Data base
    EraseMcuDev,        // SHORT_LIST_CODE_COMPDB,      Comp Data base
    EraseMcuDev,        // SHORT_LIST_CODE_DIMDB,       Dim Data base
    EraseMcuDev,        // SHORT_LIST_CODE_NAMEDB,      Name Data base
};

static_assert(ArrayLen(erase_dev) == SHORT_LIST_CODE_LAST, "Number of elements in erase_dev array doesn't match the corresponding enum")
#endif
;

/*! Code functions arrays : Check code in flash device functions */
CODES_VARS_EXT enum FGC_codes_status_internal (* const write_dev[])(enum fgc_code_ids_short_list dev_idx)
#ifdef CODES_GLOBALS
=
{
    WritePldDev,        // SHORT_LIST_CODE_PLD,         Xilinx FPGA bitstream eeprom
    WriteMcuBootDev,    // SHORT_LIST_CODE_BT,          Boot for operation
    WriteMcuDev,        // SHORT_LIST_CODE_MP_DSP,      Main programs
    WriteM16C62dev,     // SHORT_LIST_CODE_IDPROG,      C62 block 3    (IdProg)
    WriteM16C62dev,     // SHORT_LIST_CODE_IDDB,        C62 blocks 4-6 (IDDB)
    WriteM16C62dev,     // SHORT_LIST_CODE_PTDB,        C62 block 1    (PTDB)
    WriteMcuDev,        // SHORT_LIST_CODE_SYSDB,       Sys Data base
    WriteMcuDev,        // SHORT_LIST_CODE_COMPDB,      Comp Data base
    WriteMcuDev,        // SHORT_LIST_CODE_DIMDB,       Dim Data base
    WriteMcuDev,        // SHORT_LIST_CODE_NAMEDB,      Name Data base
};

static_assert(ArrayLen(write_dev) == SHORT_LIST_CODE_LAST, "Number of elements in write_dev array doesn't match the corresponding enum")
#endif
;

/*! Code functions arrays : Check code in flash device functions */
CODES_VARS_EXT enum FGC_codes_status_internal (* const check_dev[])(enum fgc_code_ids_short_list dev_idx)
#ifdef CODES_GLOBALS
=
{
    CheckPldDev,        // SHORT_LIST_CODE_PLD,         Xilinx FPGA bitstream eeprom
    CheckMcuDev,        // SHORT_LIST_CODE_BT,          Boot for operation
    CheckMcuDev,        // SHORT_LIST_CODE_MP_DSP,      Main programs
    CheckC62Dev,        // SHORT_LIST_CODE_IDPROG,      C62 block 3    (IdProg)
    CheckC62Dev,        // SHORT_LIST_CODE_IDDB,        C62 blocks 4-6 (IDDB)
    CheckC62Dev,        // SHORT_LIST_CODE_PTDB,        C62 block 1    (PTDB)
    CheckMcuDev,        // SHORT_LIST_CODE_SYSDB,       Sys Data base
    CheckMcuDev,        // SHORT_LIST_CODE_COMPDB,      Comp Data base
    CheckMcuDev,        // SHORT_LIST_CODE_DIMDB,       Dim Data base
    CheckMcuDev,        // SHORT_LIST_CODE_NAMEDB,      Name Data base
};

static_assert(ArrayLen(check_dev) == SHORT_LIST_CODE_LAST, "Number of elements in check_dev array doesn't match the corresponding enum")
#endif
;

/*! Code global variables */
CODES_VARS_EXT struct code code;

/*!
 * Initializes code reception buffer variables
 *
 * The function should be called before each code reception.
 */
static inline void CodesRxBufferReset(void)
{
    code.rx.buffer.in_blk      = RXCODEBUF_A;
    code.rx.buffer.out_blk     = RXCODEBUF_A;
    code.rx.buffer.next_in_blk = RXCODEBUF_A + 1;

    code.rx.buffer.overflow = false;
}

/*!
 * Increments given buffer pointer, respecting buffer boundaries
 *
 * @param[in, out]  Pointer to one of the buffer pointers
 */
static inline void CodesRxBufferIncrement(struct code_block ** pointer)
{
    ++(*pointer);

    if((uint32_t)*pointer >= (RXCODEBUF_32 + RXCODEBUF_B))
    {
        *pointer = RXCODEBUF_A;
    }
}

/*!
 * Returns pointer to next free code block to fill or NULL in case of overflow
 *
 * The function may be called multiple times before making the block available
 * to the background thread.
 *
 * @return  Pointer to next free code block
 *
 * @retval  NULL  In case the reception buffer has overflown
 */
static inline struct code_block * CodesRxBufferGetNextFreeBlock(void)
{
    return code.rx.buffer.overflow ? NULL : code.rx.buffer.next_in_blk;
}

/*!
 * Makes next code block available to the background thread
 *
 * The function will do nothing in case buffer overflow was detected.
 * It should be called only once per code block!
 */
static inline void CodesRxBufferMakeNextAvailable(void)
{
    if(code.rx.buffer.overflow)
    {
        return;
    }

    code.rx.buffer.in_blk = code.rx.buffer.next_in_blk;

    CodesRxBufferIncrement(&code.rx.buffer.next_in_blk);

    if(code.rx.buffer.out_blk == code.rx.buffer.next_in_blk)
    {
        code.rx.buffer.overflow = true;
    }
}

/*!
 * Tells the background thread if next code block is available
 *
 * @retval  true   Next code block available
 * @retval  false  There's no code block available
 */
static inline bool CodesRxBufferNextBlockAvailable(void)
{
    return (code.rx.buffer.out_blk != code.rx.buffer.in_blk);
}

/*!
 * Returns next available code block or NULL in case of overflow
 *
 * This function should be called only once per available code block!
 *
 * @return  Pointer to code block
 *
 * @retval  NULL  When buffer has overflown
 */
static inline struct code_block * CodesRxBufferGetLatestBlock(void)
{
    if(code.rx.buffer.overflow)
    {
        return NULL;
    }

    CodesRxBufferIncrement((struct code_block **)&code.rx.buffer.out_blk);

    return code.rx.buffer.out_blk;
}

#endif  // CODES_H end of header encapsulation

// EOF

