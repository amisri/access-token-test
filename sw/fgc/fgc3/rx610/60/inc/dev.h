/*---------------------------------------------------------------------------------------------------------*\
  File:         dev.h

  Purpose:      for class 60

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DEV_H   // header encapsulation
#define DEV_H

#ifdef DEV_GLOBALS
#define DEV_VARS_EXT
#else
#define DEV_VARS_EXT extern
#endif


//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>

#include <sharedMemory.h>
#include <pld_spi.h>

// for shared_mem.boot_seq
#define BOOT_PWR                0               // Power start - do full self test and update codes
#define BOOT_NORMAL             1               // Normal start  - do partial test and update codes
#define BOOT_UNKNOWN            0xFF            // Uninitialised value for boot type

enum FGC_dev_status
{
    DEV_SUCCESS,
    DEV_UNKNOWN_PLD_HW,
};

//-----------------------------------------------------------------------------------------------------------

// TODO Make these booleans?
struct TCheckeableRegs
{
    uint8_t leds_rg: 1; // b0
    uint8_t io     : 1; // b2
    uint8_t analog : 1; // b3
    uint8_t nvram  : 1; // b4
    uint8_t sram   : 1; // b5
    uint8_t dpram  : 1; // b6
    uint8_t leds_bl: 1; // b7
};


struct TBootMiscellaneusVariables
{
    // 'Consts'

    uint16_t                rst_src;                        // Reset source from reset register
    uint16_t                fieldbus_id;                    // Fieldbus address
    uint8_t                 revision;                       // Revision number
    spartan_version_t       pld_hw_model;                   // PLD HW model based on fgc revision
    uint16_t                class_id;                       // FGC class id based on crate type and nameDB
    uint16_t                crate_type;                     // Crate code number
    uint16_t                crate_type_idx;                 // index in def_crate[]
    struct TCheckeableRegs  reg;                            // result of availability of resources
    bool                    is_standalone;

    // Variables

    // volatile because is used in a while() (MenuC62RunMonitor/CodesUpdateGetBlock/xsvfRun)
    // and at that moment is externally
    // modified by the interrupt TermProcessHardware/TermRxCh,
    // if it is not volatile the compiler will get rid off it
    volatile bool           abort_f;                        // Abort operation flag (Ctrl-C pressed)
    bool                    allow_slow_watchdog_refresh;    // Enable triggering of slow watchdog
    bool                    memory_test_in_progress;        // flag to avoid certain things while memory test is in progress
    bool                    self_test_in_progress_f;        // Flag true when self test is in progress
    uint16_t                leds;                           // Front panel LED values
};

//-----------------------------------------------------------------------------------------------------------

DEV_VARS_EXT struct TBootMiscellaneusVariables dev;

//-----------------------------------------------------------------------------------------------------------

inline static bool DevIsStandalone(void)
{
    return(dev.is_standalone);
}

static inline bool devIdIsValid(void)
{
    return((dev.fieldbus_id >= 1 && dev.fieldbus_id <= 64) || dev.fieldbus_id == 127);
}

void DevSetResetSource(const bool pld_is_ok);
enum FGC_dev_status DevSetPldHwVersion(void);
void DevSetCrateType(const bool pld_is_ok);

#endif  // DEV_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dev.h
\*---------------------------------------------------------------------------------------------------------*/
