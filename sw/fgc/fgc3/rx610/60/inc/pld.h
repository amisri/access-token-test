/*---------------------------------------------------------------------------------------------------------*\
  File:     pld.h

  Purpose:  FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef PLD_H   // header encapsulation
#define PLD_H

#ifdef PLD_GLOBALS
#define PLD_VARS_EXT
#else
#define PLD_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>

enum FGC_pld_status
{
    PLD_SUCCESS,
    PLD_TIMEOUT,
};

//-----------------------------------------------------------------------------------------------------------

struct TPLDinfo
{
    uint16_t      state;      // Programming state
    uint16_t      hw_model;   // installed PLD model, this only available after PLD successfully startup
    uint16_t      hw_version;
    uint16_t      spi_state;  // internal SPI flash memory state (ok or unlocked)
    bool     ok;         // PLD passes all checks
};

//-----------------------------------------------------------------------------------------------------------

enum FGC_pld_status PldWaitForDONEandReadModel(void);
void    PldReset(void);
enum FGC_pld_status PldBootISF(void);
bool PldBootSlaveSerial(void);
void    PldFeedBitstreamViaSlaveSerial(uint8_t * bitstream_p, uint32_t len);

//-----------------------------------------------------------------------------------------------------------

PLD_VARS_EXT struct TPLDinfo    pld_info;

//-----------------------------------------------------------------------------------------------------------

#endif  // PLD_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pld.h
\*---------------------------------------------------------------------------------------------------------*/
