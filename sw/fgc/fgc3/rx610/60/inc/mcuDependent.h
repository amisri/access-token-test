/*!
 *  @file     mcuDependent.h
 *  @defgroup FGC3:MCU
 *  @brief    FGC3 constants.
 */


#ifndef MCU_DEPENDENT_H     // header encapsulation
#define MCU_DEPENDENT_H

#include <iodefines.h>
#include <main.h>
#include <os_hardware.h>

// FIND FIRST ONE:
// Macro to find the first bit set in a 16 bit mask (starting from least significant bit, i.e. bit 0)

#ifdef MCU_FF1_SOFTWARE_VERSION
#define MCU_FF1(mask,resp)                                              \
    {                                                                       \
        uint16_t local_mask = (mask);                                         \
        if(local_mask == 0)                                                 \
        {                                                                   \
            resp = 0x1F;    /* Means no bit set */                          \
        }                                                                   \
        else                                                                \
        {                                                                   \
            resp = 0;                                                       \
            while((local_mask & 1) == 0)                                    \
            {                                                               \
                local_mask >>= 1;                                           \
                resp++;                                                     \
            }                                                               \
        }                                                                   \
    }
#else
#define MCU_FF1(mask,resp)                                              \
    {                                                                       \
        OS_CPU_SR cpu_sr_suspend;                                           \
        OS_ENTER_CRITICAL_GENERIC(cpu_sr_suspend);                          \
        CPU_LSSB_P = (mask);                                                \
        resp = CPU_LSSB_P;                                                  \
        OS_EXIT_CRITICAL_GENERIC(cpu_sr_suspend);                           \
    }
#endif

// ToDo this is used by the boot only, with internal 1ms timer, in case PLD counter is not available
#define Get1msTickCountNow()    main_misc.timer_1ms

// In case a write instruction to NVRAM is done right before NVRAM_LOCK(), the CPU pipeline can result in
// the NVRAM becoming locked before the write is executed. To prevent this, a dummy read is executed
// to provide the necessary delay.
#define NVRAM_LOCK()            {   uint16_t dummy_read;             \
        (void) dummy_read;                      \
        if (--main_misc.n_nvs_unlock_request==0)\
        {                                       \
            dummy_read = NVRAM_SAFE_MODE_P;     \
            P7.DR.BIT.B5 = 1;                   \
        }                                       \
    }
#define NVRAM_UNLOCK()          { main_misc.n_nvs_unlock_request++; P7.DR.BIT.B5 = 0; }
#define NVRAM_LOCKED            P7.PORT.BIT.B5          // to read the status

#define POWER_CYCLE()           (P4.DR.BIT.B7 = 0)      // when it restarts it goes to 1 automatically

// MCU TEST POINTS MCU_TP0, MCU_TP1
#define TP0_SET()               P4.DR.BIT.B5 = 1        // TP0 = Port P4.5
#define TP0_CLR()               P4.DR.BIT.B5 = 0
#define TP0_TGL()               P4.DR.BIT.B5 ^= 1

#define TP1_SET()               P4.DR.BIT.B6 = 1        // TP1 = Port P4.6
#define TP1_CLR()               P4.DR.BIT.B6 = 0
#define TP1_TGL()               P4.DR.BIT.B6 ^= 1


/*
if P6.DR.BIT.B5 is 0 then when the network card
receive a "reset SHORT pulse signal" (50) from the gateway it trigger the power cycle
and
if it receive "reset LONG pulse signal" (100) it also trigger the power cycle.

if P6.DR.BIT.B5 is 1 (longer integration time) then when the network card
receive a "reset SHORT pulse signal" (50) from the gateway it is ignored.
It waits for a "reset LONG pulse signal" (100) to trigger the power cycle.

*/
#define ACCEPT_BOTH_NET_PULSES()        P6.DR.BIT.B5 = 0; // for boot and crash
#define ACCEPT_ONLY_NET_LONG_PULSE()    P6.DR.BIT.B5 = 1; // for main

#define REFRESH_SLOWWATCHDOG()  { P8.DR.BIT.B3 = 0; P8.DR.BIT.B3 = 1; } // 3s

// Get the ISP (Interrupt stack pointer). global_int_stack_ptr must be a GLOBAL variable.

#define  GET_ISP(global_int_stack_ptr)                                                                \
    __asm__ volatile                                                                                          \
    (                                                                                                     \
            /* save r5 */                                                                                     \
            "push       r5                      \n\t"                                                         \
            /* get the Interrupt Stack Pointer (ISP) */                                                       \
            "mvfc       isp, r5                 \n\t"                                                         \
            /* store the IPL into interrupt_stack_ptr */                                                      \
            "mov.l      r5, %0":"=m"(global_int_stack_ptr):/* no input operands */:/* clobbered registers */  \
    );                                                                                                    \
    __asm__ volatile                                                                                          \
    (                                                                                                     \
            /* restore r5 */                                                                                  \
            "pop        r5                      \n\t"                                                         \
    );


// Get the USP (User stack pointer). global_usr_stack_ptr must be a GLOBAL variable.

#define  GET_USP(global_usr_stack_ptr)                                                                \
    __asm__ volatile                                                                                          \
    (                                                                                                     \
            /* save r5 */                                                                                     \
            "push       r5                      \n\t"                                                         \
            /* get the Interrupt Stack Pointer (ISP) */                                                       \
            "mvfc       usp, r5                 \n\t"                                                         \
            /* store the IPL into interrupt_stack_ptr */                                                      \
            "mov.l      r5, %0":"=m"(global_usr_stack_ptr):/* no input operands */:/* clobbered registers */  \
    );                                                                                                    \
    __asm__ volatile                                                                                          \
    (                                                                                                     \
            /* restore r5 */                                                                                  \
            "pop        r5                      \n\t"                                                         \
    );

#endif

// EOF
