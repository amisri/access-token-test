/*---------------------------------------------------------------------------------------------------------*\
  File:         codes_mcu.h

  Purpose:      FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef CODES_MCU_H     // header encapsulation
#define CODES_MCU_H

#ifdef CODES_MCU_GLOBALS
#define CODES_MCU_VARS_EXT
#else
#define CODES_MCU_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>           // basic typedefs
#include <codes_holder.h>

//-----------------------------------------------------------------------------------------------------------

enum FGC_codes_status_internal EraseMcuDev(enum fgc_code_ids_short_list dev_idx);
enum FGC_codes_status_internal WriteMcuBootDev(enum fgc_code_ids_short_list dev_idx);
enum FGC_codes_status_internal CheckMcuDev(enum fgc_code_ids_short_list dev_idx);
enum FGC_codes_status_internal EraseMcuBootDev(enum fgc_code_ids_short_list dev_idx);          // only FGC3
enum FGC_codes_status_internal WriteMcuDev(enum fgc_code_ids_short_list dev_idx);              // only FGC3
uint16_t  RAM_WriteBoot(void);                                            // only FGC3

//-----------------------------------------------------------------------------------------------------------

#endif  // CODES_MCU_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: codes_mcu.h
\*---------------------------------------------------------------------------------------------------------*/
