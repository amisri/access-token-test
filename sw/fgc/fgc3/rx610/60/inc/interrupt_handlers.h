/*---------------------------------------------------------------------------------------------------------*\
  File:         interrupt_handlers.h

  Purpose:      60 - Boot - RX610 ISRs - Interrupt Service Routines

  History:

    02 nov 10   doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef INTERRUPT_HANDLERS_H            // header encapsulation
#define INTERRUPT_HANDLERS_H

//-----------------------------------------------------------------------------------------------------------
//void  IsrDummy1                       (void) __attribute__ ((interrupt, naked));
/*=========================================================================================================*/
void    INT_Excep_SupervisorInst(void) __attribute__((interrupt, naked));
void    INT_Excep_UndefinedInst(void) __attribute__((interrupt, naked));
void    INT_Excep_FloatingPoint(void) __attribute__((interrupt, naked));
void    INT_NonMaskableInterrupt(void) __attribute__((interrupt, naked));
//void  PowerON_Reset                   (void) __attribute__ ((interrupt, naked));
/*=========================================================================================================*/
// vector  0
void    INT_Excep_BRK(void) __attribute__((interrupt, naked));
// vector  1 pure software, hardware reserved OSTskCtxSwitch()
void    OSTskCtxSwitch(void) __attribute__((interrupt, naked));
// vector  2 reserved
// vector  3 reserved
// vector  4 reserved
// vector  5 reserved
// vector  6 reserved
// vector  7 reserved
// vector  8 reserved
// vector  9 reserved
// vector 10 reserved
// vector 11 reserved
// vector 12 reserved
// vector 13 reserved
// vector 14 reserved
// vector 15 reserved
void    INT_Excep_BUSERR(void) __attribute__((interrupt, naked));
// vector 17 reserved
// vector 18 reserved
// vector 19 reserved
// vector 20 reserved
void    INT_Excep_FCU_FCUERR(void) __attribute__((interrupt, naked));
// vector 22 reserved
void    INT_Excep_FCU_FRDYI(void) __attribute__((interrupt, naked));
// vector 24 reserved
// vector 25 reserved
// vector 26 reserved
// vector 27 reserved
void    INT_Excep_CMTU0_CMT0(void) __attribute__((interrupt, naked));
void    INT_Excep_CMTU0_CMT1(void) __attribute__((interrupt, naked));
void    INT_Excep_CMTU1_CMT2(void) __attribute__((interrupt, naked));
void    INT_Excep_CMTU1_CMT3(void) __attribute__((interrupt, naked));
// vector 32 reserved
// vector 33 reserved
// vector 34 reserved
// vector 35 reserved
// vector 36 reserved
// vector 37 reserved
// vector 38 reserved
// vector 39 reserved
// vector 40 reserved
// vector 41 reserved
// vector 42 reserved
// vector 43 reserved
// vector 44 reserved
// vector 45 reserved
// vector 46 reserved
// vector 47 reserved
// vector 48 reserved
// vector 49 reserved
// vector 50 reserved
// vector 51 reserved
// vector 52 reserved
// vector 53 reserved
// vector 54 reserved
// vector 55 reserved
// vector 56 reserved
// vector 57 reserved
// vector 58 reserved
// vector 59 reserved
// vector 60 reserved
// vector 61 reserved
// vector 62 reserved
// vector 63 reserved
void    INT_Excep_IRQ0(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ1(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ2(void) __attribute__((interrupt));                    // not naked
void    INT_Excep_IRQ3(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ4(void) __attribute__((interrupt));                    // not naked
void    INT_Excep_IRQ5(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ6(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ7(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ8(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ9(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ10(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ11(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ12(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ13(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ14(void) __attribute__((interrupt, naked));
void    INT_Excep_IRQ15(void) __attribute__((interrupt, naked));
// vector 80 reserved
// vector 81 reserved
// vector 82 reserved
// vector 83 reserved
// vector 84 reserved
// vector 85 reserved
// vector 86 reserved
// vector 87 reserved
// vector 88 reserved
// vector 89 reserved
// vector 90 reserved
// vector 91 reserved
// vector 92 reserved
// vector 93 reserved
// vector 94 reserved
// vector 95 reserved
void    INT_Excep_WDT_WOVI(void) __attribute__((interrupt, naked));
// WDT vector 97 reserved
void    INT_Excep_AD0_ADI0(void) __attribute__((interrupt, naked));
void    INT_Excep_AD1_ADI1(void) __attribute__((interrupt, naked));
void    INT_Excep_AD2_ADI2(void) __attribute__((interrupt, naked));
void    INT_Excep_AD3_ADI3(void) __attribute__((interrupt, naked));
// vector 102 reserved
// vector 103 reserved
void    INT_Excep_TPU0_TGI0A(void) __attribute__((interrupt));              // not naked
void    INT_Excep_TPU0_TGI0B(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU0_TGI0C(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU0_TGI0D(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU0_TCI0V(void) __attribute__((interrupt, naked));
// TPU0 vector 109 reserved
// TPU0 vector 110 reserved
void    INT_Excep_TPU1_TGI1A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU1_TGI1B(void) __attribute__((interrupt, naked));
// TPU1 vector 113 reserved
// TPU1 vector 114 reserved
void    INT_Excep_TPU1_TCI1V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU1_TCI1U(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU2_TGI2A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU2_TGI2B(void) __attribute__((interrupt, naked));
// TPU2 vector 119 reserved
void    INT_Excep_TPU2_TCI2V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU2_TCI2U(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU3_TGI3A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU3_TGI3B(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU3_TGI3C(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU3_TGI3D(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU3_TCI3V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU4_TGI4A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU4_TGI4B(void) __attribute__((interrupt, naked));
// vector 129 reserved
// vector 130 reserved
void    INT_Excep_TPU4_TCI4V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU4_TCI4U(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU5_TGI5A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU5_TGI5B(void) __attribute__((interrupt, naked));
// vector 135 reserved
void    INT_Excep_TPU5_TCI5V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU5_TCI5U(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU6_TGI6A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU6_TGI6B(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU6_TGI6C(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU6_TGI6D(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU6_TCI6V(void) __attribute__((interrupt, naked));
// vector 143 reserved
// vector 144 reserved
void    INT_Excep_TPU7_TGI7A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU7_TGI7B(void) __attribute__((interrupt, naked));
// vector 147 reserved
// vector 148 reserved
void    INT_Excep_TPU7_TCI7V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU7_TCI7U(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU8_TGI8A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU8_TGI8B(void) __attribute__((interrupt, naked));
// vector 153 reserved
void    INT_Excep_TPU8_TCI8V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU8_TCI8U(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU9_TGI9A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU9_TGI9B(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU9_TGI9C(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU9_TGI9D(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU9_TCI9V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU10_TGI10A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU10_TGI10B(void) __attribute__((interrupt, naked));
// vector 163 reserved
// vector 164 reserved
void    INT_Excep_TPU10_TCI10V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU10_TCI10U(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU11_TGI11A(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU11_TGI11B(void) __attribute__((interrupt, naked));
// vector 169 reserved
void    INT_Excep_TPU11_TCI11V(void) __attribute__((interrupt, naked));
void    INT_Excep_TPU11_TCI11U(void) __attribute__((interrupt, naked));
// vector 172 reserved
// vector 173 reserved
void    INT_Excep_TMR0_CMIA0(void) __attribute__((interrupt));              // not naked
void    INT_Excep_TMR0_CMIB0(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR0_OVI0(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR1_CMIA1(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR1_CMIB1(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR1_OVI1(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR2_CMIA2(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR2_CMIB2(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR2_OVI2(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR3_CMIA3(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR3_CMIB3(void) __attribute__((interrupt, naked));
void    INT_Excep_TMR3_OVI3(void) __attribute__((interrupt));               // not naked
// vector 186 reserved
// vector 187 reserved
// vector 188 reserved
// vector 189 reserved
// vector 190 reserved
// vector 191 reserved
// vector 192 reserved
// vector 193 reserved
// vector 194 reserved
// vector 195 reserved
// vector 196 reserved
// vector 197 reserved
void    INT_Excep_DMAC_DMTEND0(void) __attribute__((interrupt, naked));
void    INT_Excep_DMAC_DMTEND1(void) __attribute__((interrupt, naked));
void    INT_Excep_DMAC_DMTEND2(void) __attribute__((interrupt, naked));
void    INT_Excep_DMAC_DMTEND3(void) __attribute__((interrupt, naked));
// vector 202 reserved
// vector 203 reserved
// vector 204 reserved
// vector 205 reserved
// vector 206 reserved
// vector 207 reserved
// vector 208 reserved
// vector 209 reserved
// vector 210 reserved
// vector 211 reserved
// vector 212 reserved
// vector 213 reserved
void    INT_Excep_SCI0_ERI0(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI0_RXI0(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI0_TXI0(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI0_TEI0(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI1_ERI1(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI1_RXI1(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI1_TXI1(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI1_TEI1(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI2_ERI2(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI2_RXI2(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI2_TXI2(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI2_TEI2(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI3_ERI3(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI3_RXI3(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI3_TXI3(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI3_TEI3(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI4_ERI4(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI4_RXI4(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI4_TXI4(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI4_TEI4(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI5_ERI5(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI5_RXI5(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI5_TXI5(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI5_TEI5(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI6_ERI6(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI6_RXI6(void) __attribute__((interrupt));               // not naked
void    INT_Excep_SCI6_TXI6(void) __attribute__((interrupt, naked));
void    INT_Excep_SCI6_TEI6(void) __attribute__((interrupt, naked));
// vector 242 reserved
// vector 243 reserved
// vector 244 reserved
// vector 245 reserved
void    INT_Excep_RIIC0_EEI0(void) __attribute__((interrupt, naked));
void    INT_Excep_RIIC0_RXI0(void) __attribute__((interrupt, naked));
void    INT_Excep_RIIC0_TXI0(void) __attribute__((interrupt, naked));
void    INT_Excep_RIIC0_TEI0(void) __attribute__((interrupt, naked));
void    INT_Excep_RIIC1_EEI1(void) __attribute__((interrupt, naked));
void    INT_Excep_RIIC1_RXI1(void) __attribute__((interrupt, naked));
void    INT_Excep_RIIC1_TXI1(void) __attribute__((interrupt, naked));
void    INT_Excep_RIIC1_TEI1(void) __attribute__((interrupt, naked));
// vector 254 reserved
// vector 255 reserved
//-----------------------------------------------------------------------------------------------------------

#endif  // INTERRUPT_HANDLERS_H  end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: interrupt_handlers.h
\*---------------------------------------------------------------------------------------------------------*/
