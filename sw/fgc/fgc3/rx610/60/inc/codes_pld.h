/*!
 *  @file     codes_pld.h
 *  @defgroup FGC3:MCU boot
 *  @brief    FPGA programming function
 *
 */

#ifndef CODES_PLD_H     // header encapsulation
#define CODES_PLD_H

#ifdef CODES_PLD_GLOBALS
#define CODES_PLD_EXT
#else
#define CODES_PLD_EXT extern
#endif

// Includes

#include <stdint.h>
#include <codes_holder.h>

// External function declarations

/*!
 * Check PLD device
 * we don't store the PLD in accessible flash so valid is always assumed
 * the codes_holder_info[device].dev structure is filled with the info from the code we already have in the FLASH
 *
 * @param dev_idx device index for code information
 * @retval 0 if the code is valid, 1 otherwise.
 */
enum FGC_codes_status_internal CheckPldDev(enum fgc_code_ids_short_list dev_idx);

/*!
 * Erase PLD device. For PLD  this functions does nothing.
 * Actual erasing is perform after loading code in RAM in WritePldDev
 *
 * @param dev_idx device index for code information
 * @retval 0 if erase ok, 1 if the erase fails (device state is set to indicate DEV_FAILED)
 */
enum FGC_codes_status_internal ErasePldDev(enum fgc_code_ids_short_list dev_idx);

/*!
 * Write PLD device: load new code in RAM, check it runs, then load it in flash, finally reset FGC.
 *
 * @param dev_idx device index for code information
 * @retval 0 if write ok, 1 if the write fails
 */
enum FGC_codes_status_internal WritePldDev(enum fgc_code_ids_short_list dev_idx);

/*!
 * Select the PLD depending on FGC revision and analogue board
 *   ToDo LUT to get code_id from 2 dimension array [ana type][hw model]
 *
 */
void CodesPLDSelect(void);


#endif  // CODES_PLD_H end of header encapsulation

//EOF

