/*---------------------------------------------------------------------------------------------------------*\
  File:         debug.h

  Purpose:      FGC3

  History:

    8 feb 2011  doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DEBUG_H // header encapsulation
#define DEBUG_H

#if (!defined(__RX610__) && !defined(__M16C62__) && !defined(__HC16__)) \
    || defined(__RX610__) || defined(__M16C62__) || defined(__HC16__)
#endif

//-----------------------------------------------------------------------------------------------------------

// this avoid the test of stand alone, so we can use the M16C62 for debugging in stand alone as if
// it is in its normal state
// #define      DEBUG_M16C62_STANDALONE

// this avoid the test of QSPI bus (DIM boards) for errors, to avoid at the lab the refuse to pass to Main from the Boot
// #define      DEBUG_SKIP_QSPI_ERRORS

// for change between FGC3 clocks and timing v0 to clocks and timing v1
// #define      USE_CLOCKS_AND_TIMING_V1

// #define TEST_PLL_SWEEP
// #define TEST_ADC_PLL_SWEEP


//-----------------------------------------------------------------------------------------------------------

#endif  // DEBUG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: debug.h
\*---------------------------------------------------------------------------------------------------------*/
