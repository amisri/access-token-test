/*!
 *  @file      dsp_loader.h
 *  @brief     DSP boot loader.
 */

#ifndef FGC_DSP_LOADER_H    // header encapsulation
#define FGC_DSP_LOADER_H

#ifdef DSP_LOADER_GLOBALS
#define DSP_LOADER_VARS_EXT
#else
#define DSP_LOADER_VARS_EXT extern
#endif



// Includes

#include <stdint.h>
#include <stdbool.h>

#include <label.h>



// External function declarations

/*!
 * DSP boot loader error values
 */
enum dsp_error
{
    DSP_ERROR_OK,
    DSP_ERROR_PLL_UNSTABLE,           // Input reference clock is not stable
    DSP_ERROR_CODE_BLOCK_TIMEOUT,     // Timeout during reception of one of the DSP code blocks
    DSP_ERROR_ENTRY_POINT_TIMEOUT,    // Timeout during reception of the entry point
    DSP_ERROR_NOT_RUNNING_TIMEOUT,    // Timeout when waiting for the DSP to run
    DSP_ERROR_STANDALONE,             // The DSP is in standalone mode
    DSP_ERROR_LAST
};


#if FGC_CLASS_ID == 60
/*!
 * DSP information
 */
struct dsp_info
{
    uint32_t       * bl_data;        //!< Pointer to data for bootloader
    bool             is_running;     //!< Dsp is running flag
    uint16_t         status;         //!< Dsp status, not used by FGC3 boot
    uint16_t         bg_counter;     //!< Low word of bg counter
    uint16_t         irq_mask;       //!< IRQ trigger mask
    uint16_t         irq_fail;       //!< IRQ fail mask
};

DSP_LOADER_VARS_EXT struct dsp_info    dsp;

#endif



// External function declarations

/*!
 * Transfers the DSP image to the DSP
 *
 * @param  dsp_binary_image  The DSP binary image
 * @param
 * @retval DSP_ERROR_OK      The image was successfully transfered.
 * @retval !DSP_ERROR_OK     An error occurred when transferring the image.
 */
enum dsp_error dspLoadImage(uint8_t const * dsp_image, uint32_t const dsp_image_length);

/*!
 * Returns a human readable string with the DSP image load error
 */
char const *  dspLoadErrorStr(enum dsp_error error);


#endif  // FGC_DSP_LOADER_H end of header encapsulation

// EOF
