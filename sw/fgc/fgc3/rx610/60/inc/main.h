/*---------------------------------------------------------------------------------------------------------*\
  File:     main.h

  Purpose:

\*---------------------------------------------------------------------------------------------------------*/

#ifndef MAIN_H  // header encapsulation
#define MAIN_H

#ifdef MAIN_GLOBALS
#define MAIN_VARS_EXT
#else
#define MAIN_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdbool.h>

#include <stdint.h>
#include <stdint.h>
#include <state_class.h>
#include <structs_bits_big.h>
#include <definfo.h>

//-----------------------------------------------------------------------------------------------------------
struct TMainMiscellaneous
{
    // State Task Variables
    bool                                 state_machine_is_running;   // state machine running
    uint16_t                                state_machine_slice_mask;
    uint16_t                                n_nvs_unlock_request;       // Number of unlocking requests
    uint16_t                                local_ms;                   // local ms (= UTC register if available)
    volatile enum pc_state_machine_states   state_pc;                   // Power converter state
    // ----------
    volatile uint16_t                       timer_1ms;                  // incremented in the interrupt
    volatile uint8_t                        timer_255us;                // incremented in the interrupt
    uint16_t runlog_show_index;          // used as a global variable just for printing purposes
    bool                                 is_sci_test_continuous;     //continuous mode for sci test

    bool                                    dig_sup_test;               // Square wave generation enabled/disabled flag
    uint16_t                                dig_sup_test_counter;       // Square wave generator counter
    uint16_t                                dig_sup_test_duty;          // Square wave duty cycle in ms
    bool                                    force_main;
};

//-----------------------------------------------------------------------------------------------------------
int  main(void);
void CheckStatus(void);
bool bootIsMainAllowed(const bool stop_on_non_fatal);
void ReportBoot(void);
void bootRunMain(void);

//-----------------------------------------------------------------------------------------------------------

MAIN_VARS_EXT struct TMainMiscellaneous     main_misc;

//-----------------------------------------------------------------------------------------------------------

#endif  // MAIN_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.h
\*---------------------------------------------------------------------------------------------------------*/
