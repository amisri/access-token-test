/*---------------------------------------------------------------------------------------------------------*\
  File:         macros.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef MACROS_H        // header encapsulation
#define MACROS_H

#include <stdint.h>

//-----------------------------------------------------------------------------------------------------------
// Bit manipulation macros: Set, Clr, Test, TestAll
//

// Set(v,b): in v, set all bits identified by bitmask b

#define Set(v,b)                ( (v) |=  (b) )         // Atomic action on the FGC2

// Clr(v,b): in v, clear all bits identified by bitmask b

#define Clr(v,b)                ( (v) &= ~(b) )

// Implementation notes regarding the Test() macro:
//
//  On the FGC2 MCU, the result of the macro is not a boolean (i.e. 0 or 1). This is for
//  performance reason that we chose not to do the conversion to a boolean, knowing
//  that on HC16 the bool type is a 16-bit integer and the macro is used to compare
//  16-bit masks.
//  On the FGC3 MCU, however, the choice was to do an explicit conversion to a boolean
//  (this is the meaning of the double not operator !!). This way the bool type
//  can be freely defined and the macro will always work. Here is a code example to
//  appreciate the difference between the two implementations:
//
//      bool set_f;                      // Let us say that bool is a uint8_t
//      set_f = Test(value, 0x0100);        // set_f = 0 or 1 ?

// Test(v,b): Test if at least one bit in bitmask b is set in v. Returns a bool on FGC3, not FGC2.

#define Test(v,b)           ( !!( (v) & (b) ) )

// TestAll(v,b): Test if all bits in bitmask b are set in v. Returns a bool.

#define TestAll(v,b)          ( ( (v) & (b) ) == (b) )


//-----------------------------------------------------------------------------------------------------------

// ToDo: move this to dpcom.h or better don't use the macro
#define DEVICE_PPM                      dpcom.mcu.device_ppm            // DEVICE PPM flag

//-----------------------------------------------------------------------------------------------------------

#define ArrayLen(arr) ( sizeof(arr) / sizeof(arr[0]) )

//-----------------------------------------------------------------------------------------------------------

#endif  // MACROS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: macros.h
\*---------------------------------------------------------------------------------------------------------*/
