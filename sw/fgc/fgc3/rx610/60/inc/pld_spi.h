/*---------------------------------------------------------------------------------------------------------*\
  File:     pld_spi.h

            The Xilinx documentation are
               UG331(v1.5) Spartan-3 Generation Configuration User Guide
               UG333(v2.1) Spartan-3AN FPGA In-System Flash User Guide

\*---------------------------------------------------------------------------------------------------------*/

#ifndef PLD_SPI_H   // header encapsulation
#define PLD_SPI_H

#ifdef PLD_SPI_GLOBALS
#define PLD_SPI_VARS_EXT
#else
#define PLD_SPI_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <label.h>      // for struct sym_name

//-----------------------------------------------------------------------------------------------------------


#define  SPI_STATE_OK                   0
#define  SPI_ERROR_PROGRAMING_PROBLEM   1
#define  SPI_ERROR_NO_RESPONSE          2
#define  SPI_STATE_LOCKED               3
#define  SPI_STATE_UNLOCKED             4
#define  SPI_ERROR_UNKNOWN_LOCK_STATE   5
#define  SPI_ERROR_CODE_NOT_VALID       6
#define  PLD_FLS_REBOOT_FAILED          7
#define  PLD_FLS_PLD_INIT_ERR           8
#define  PLD_FLS_PLD_CONFIG_ERR         9

#define  SPI_STS_READY_MASK             0x01A4  // Mask for ready, ISF memory size and default page size (264b)


//--------- FPGA Internal System Flash ---------

// Reading bytes from randomly-addressed locations, all read operations at 33 MHz or less
#define  SPI_CMD_RANDOM_READ                    0x03
// Fast Read Reading a large block of contiguous data, if CLK frequency is above 33 MHz
//                                              0x0B

/*
    Read the contents of the Sector Protection Register to determine
    which ISF memory sectors are protected against accidental
    erase or program operations, assuming that protecting is
    enabled using the Sector Protection Enable command
 */
// Sector Protection Register Read [0x32, don't care, don't care, don't care] answer [sector0, sector1, sector2, ... sector15]
// answer 0xFF means protected, 0x00 means unprotected
#define  SPI_CMD_PROTECT_RD                     0x32

// [0x35, don't care, don't care, don't care] answer [sector0, sector1, sector2, ... sector15]
#define  SPI_CMD_SECTOR_LOCKDOWN_REGISTER_READ  0x35

#define  SPI_CMD_PROTECTION_ACCESS_SEQ_0        0x3D
#define  SPI_CMD_PROTECTION_ACCESS_SEQ_1        0x2A
#define  SPI_CMD_PROTECTION_ACCESS_SEQ_2        0x7F
// Erase the Sector Protection Register. Required before programming the Sector Protection Register
// [0x3D, 0x2A, 0x7F, 0xCF]
#define  SPI_CMD_PROTECTION_ERASE_SEQ_3         0xCF
// Program the control bytes within the Sector Protection Register to protect selected sectors against
// program or erase operations. Erase the Sector Protection Register before programming
// [0x3D, 0x2A, 0x7F, 0xFC]
#define  SPI_CMD_PROTECTION_PROGRAM_SEQ_3       0xCF
// Disables protection for all sectors
// [0x3D, 0x2A, 0x7F, 0x9A]
#define  SPI_CMD_PROTECTION_DISABLE_SEQ_3       0x9A
// Enables protection for the sectors specified in the Sector Protection Register
// [0x3D, 0x2A, 0x7F, 0xA9]
#define  SPI_CMD_PROTECTION_ENABLE_SEQ_3        0xA9


// Block Erase, 24 bit address, [High, Middle, Low] low is don't care
#define SPI_CMD_ERASE_BLOCK                     0x50
// Page to Buffer1 Transfer, Transfers the entire contents of a selected ISF memory page to the specified SRAM page buffer
//                                              0x53
// Page to Buffer2 Transfer, Transfers the entire contents of a selected ISF memory page to the specified SRAM page buffer
//                                              0x55
// Auto Page Rewrite (Buffer 1), 24 bit address, [High, Middle, Low] low is don't care
//                                              0x58
// Auto Page Rewrite (Buffer 2), 24 bit address, [High, Middle, Low] low is don't care
//                                              0x59
// Page to Buffer1 Compare, Verify that the ISF memory array was programmed correctly
#define  SPI_CMD_VERIFY_PAGE_BUFFER1            0x60
// Page to Buffer2 Compare, Verify that the ISF memory array was programmed correctly
#define  SPI_CMD_VERIFY_PAGE_BUFFER2            0x61
// Security Register Read, Performs a read on the contents of the security register.
//                                              0x77
// Sector Erase, Erases any unprotected, unlocked sector in the main memory, 24 bit address, [High, Middle, Low] low is don't care
#define  SPI_CMD_ERASE_SECTOR                   0x7C


// Page Erase, Erases any individual page in the ISF memory array
#define  SPI_CMD_ERASE_PAGE                     0x81
// Page Program Through Buffer1 with Erase, Combines Buffer Write with Buffer to Page Program with Built-in Erase command
//                                              0x82
// Buffer1 to Page Program with built-in Erase, First erases selected memory page and programs page with data from designated buffer
//                                              0x83
// Buffer1 Write, Write data to SRAM page buffer; when complete, transfer to ISF memory using Buffer to Page Program command
#define  SPI_CMD_WRITE_BUF1                     0x84
// Page Program Through Buffer2 with Erase, Combines Buffer Write with Buffer to Page Program with Built-in Erase command
//                                              0x85
// Buffer2 to Page Program with built-in Erase, First erases selected memory page and programs page with data from designated buffer
//                                              0x86
// Buffer2 Write, Write data to SRAM page buffer; when complete, transfer to ISF memory using Buffer to Page Program command
//                                              0x87
// Buffer1 to Page Program without built-in Erase, Program a previously erased page with data from designated buffer
#define  SPI_CMD_BUF1_TO_PAGE                   0x88
// Buffer2 to Page Program without built-in Erase, Program a previously erased page with data from designated buffer
//                                              0x89
// Security Register Program, Programs the User-Defined Field in the Security Register
//                                              0x9B

// Information Read, Read JEDEC Manufacturer and Device ID, answer [Manufacturer ID, device ID, device ID, Extended Info]
// manufacturer ID is 0x1F
//                                              0x9F

// Buffer 1 Read, 24 bit address, [High, Middle, Low] High is not used, must be 0, (Low Frequency, up to 33 MHz)
//                                              0xD1
// Buffer 2 Read, 24 bit address, [High, Middle, Low] High is not used, must be 0, (Low Frequency, up to 33 MHz)
//                                              0xD3
// Buffer 1 Read, 24 bit address, [High, Middle, Low] High is not used, must be 0, (High Frequency, up to 50 MHz)
//                                              0xD4
// Buffer 2 Read, 24 bit address, [High, Middle, Low] High is not used, must be 0, (High Frequency, up to 50 MHz)
//                                              0xD6

/*
b7      READY/BUSY
            0 = Busy
            1 = Ready
b6      COMPARE
            0 = Matches
            1 = Different
b5 b4 b3 b2     ISF Memory Size
            0011 =  1 Mbit: XC3S50AN
            0111 =  4 Mbit: XC3S200AN or XC3S400AN
            1001 =  8 Mbit: XC3S700AN
            1011 = 16 Mbit: XC3S1400AN
b1      SECTOR PROTECT
            0 = Open
            1 = Protected
b0      PAGE SIZE
            0 = Extended (Default)
            1 = Power-of-2

// Status Register Read Check
 */
#define  SPI_CMD_GET_STATUS                     0xD7

#define  SPI_CMD_PROTECT_WR3                    0xFC

//-----------------------------------------------------------------------------------------------------------

// pld_info.state comes from [P8.1,P8.2] [FPGA_~INIT,FPGA_DONE]

#define PLD_STATE_INIT_AND_DONE_ARE_LOW     0       // 00 Init, Done   pins low
#define PLD_STATE_INIT_IS_LOW               1       // 01 Init         pin  low
#define PLD_STATE_DONE_IS_LOW               2       // 10       Done   pin  low
#define PLD_STATE_INIT_AND_DONE_ARE_HI      3       // 11 Init, Done   pins high

//-----------------------------------------------------------------------------------------------------------
typedef enum
{
    XC3S700AN,
    XC3S1400AN,
    CODE_60_PLD_NUM
} spartan_version_t;

PLD_SPI_VARS_EXT const struct
{
    uint32_t    bistream_size;      // used for the PLD write bitstream length i bytes
    uint32_t    isf_size;           // used for the PLD write ISF length in bytes
    uint16_t    isf_page_size;      // Page size in bytes
    uint16_t    sector_0b_mid_addr; // Middle address to access sector 0b
    uint16_t    sector_shift;       // bit shift to access sector address (in high address)
    uint16_t    page_shift;         // bit shift to access page address (in middle address)
    uint16_t    block_shift;        // bit shift to access block address (in middle address)
} spartan_info[CODE_60_PLD_NUM]
#ifdef PLD_SPI_GLOBALS
=
{
    {
        // XC3S700AN
        .bistream_size = 341580,
        .isf_size = 341616,
        .isf_page_size = 264,
        .sector_0b_mid_addr = 0x10,
        .page_shift = 1,
        .sector_shift = 1,
        .block_shift = 4

    },
    {
        // XC3S1400AN
        .bistream_size = 594412,
        .isf_size = 594528,
        .isf_page_size = 528,
        .sector_0b_mid_addr = 0x20,
        .page_shift = 2,
        .sector_shift = 2,
        .block_shift = 5
    }
}
#endif
;


PLD_SPI_VARS_EXT const struct sym_name pld_prog_lbl[]
#ifdef PLD_SPI_GLOBALS
        =
{
    { SPI_STATE_OK,                 "PLD FLS OK & locked"           },
    { SPI_ERROR_PROGRAMING_PROBLEM, "PLD Flash prog error"          },
    { SPI_ERROR_NO_RESPONSE,        "PLD Flash no rsp"              },
    { SPI_STATE_LOCKED,             "PLD Flash locked"              },
    { SPI_STATE_UNLOCKED,           "PLD Flash unlocked"            },
    { SPI_ERROR_UNKNOWN_LOCK_STATE, "PLD Flash locking undefined"   },
    { SPI_ERROR_CODE_NOT_VALID,     "Code not valid"                },
    { PLD_FLS_REBOOT_FAILED,        "Bitstream reboot failed"       },
    { PLD_FLS_PLD_INIT_ERR,         "PLD init failed"               },
    { PLD_FLS_PLD_CONFIG_ERR,       "PLD serial config err"         },
    { 0                                 }  // the label part == 0  marks the end of the table
}
#endif
;

PLD_SPI_VARS_EXT const struct sym_name pld_stat_lbl[]
#ifdef PLD_SPI_GLOBALS
        =
{
    { PLD_STATE_INIT_AND_DONE_ARE_LOW,  "INIT,DONE low" },
    { PLD_STATE_INIT_IS_LOW,            "INIT low"      },
    { PLD_STATE_DONE_IS_LOW,            "DONE low"      },
    { PLD_STATE_INIT_AND_DONE_ARE_HI,   "OK"            },
    { 0                         }  // the label part == 0  marks the end of the table
}
#endif
;

//-----------------------------------------------------------------------------------------------------------

uint16_t  PldSPIfeedByteToController(uint8_t data);
void    PldSPIendTransaction(void);
uint16_t  PldSPIeraseSector(uint8_t sector, spartan_version_t version);
uint16_t  PldSPIeraseBlock(uint16_t block, spartan_version_t version);
uint16_t  PldSPIerasePage(uint16_t page, spartan_version_t version);
uint16_t  PldSPIread(uint32_t addr, uint8_t * data_p);
uint16_t  PldSPIwritePage(bool erase_before, spartan_version_t version, uint16_t page, uint16_t bytes_to_write,
                        uint8_t const * data_p);
void    PldSPIunlockAllMemory(void);
void    PldSPIenableProtection(void);

//-----------------------------------------------------------------------------------------------------------

#endif  // PLD_SPI_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: pld_spi.h
\*---------------------------------------------------------------------------------------------------------*/
