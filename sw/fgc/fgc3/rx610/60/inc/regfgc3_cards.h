/*---------------------------------------------------------------------------------------------------------*\
 File        :  regfgc3_cards.h
 Scope       : FGC3:MCU boot
 Description :  Definitions of structure for the regfgc3 system
                Definitions of existing regfgc3 cards
\*---------------------------------------------------------------------------------------------------------*/



#ifndef REGFGC3_CARDS_H    // header encapsulation
#define REGFGC3_CARDS_H

#ifdef REGFGC3_CARDS_GLOBALS
#define REGFGC3_CARDS_VARS_EXT
#else
#define REGFGC3_CARDS_VARS_EXT extern
#endif

// Includes

#include <stdint.h>
#ifdef __RX610__
#include <structs_bits_big.h>
#endif

// Constants

#define DIM_MODULES_IN_REGFGC3_CHASSIS  6
#define REGISTERS_IN_REGFGC3_DIM_MODULE 6

// Anode card related constants
#define ANODE_RESOLUTION    0xFFFF
#define ANODE_MAX           (+10)
#define ANODE_MIN           (-10)

// Structures, unions and enumerations

typedef enum
{
    SCIVS_L4_STATE_CONTROL      ,   // VS Control State card v3 (1 DIM embedded)
    SCIVS_L4_BIS_INTK           ,   // BIS Interlock
    SCIVS_L4_CHARGER            ,   // Charger and Bias interface
    SCIVS_L4_MSW                ,   // MSW Control
    SCIVS_L4_RF_INTERFACE       ,
    SCIVS_L4_DIGINTK            ,   // VS Digital INTK (1 DIM embedded)
    SCIVS_L4_ANAINTK            ,   // VS Analog INTK - v4, in chassis Linac 4 Klystron Modulator v2
    SCIVS_L4_SCOPE1             ,
    SCIVS_L4_SCOPE2             ,
    SCIVS_L4_SCOPE3             ,
    SCIVS_L4_ANODE1             ,   // Anode & Filament Supervision (Mod Anode & Heater), in chassis Linac 4 Klystron Modulator v1
    SCIVS_L4_ANODE2             ,   // Anode & Filament Supervision (Mod Anode & Heater), in chassis Linac 4 Klystron Modulator v1
    SCIVS_L4_TERMINAL           ,
    SCIVS_L4_ANAINTK2           ,
    SCIVS_L4_SIRAMATRIX         ,   // Siramatrix
    SCIVS_L4_NUM_CARD           ,
} reg_fgc3_card_t;


struct TRegFgcDimModuleRegisterInfo
{
    uint16_t    fault_mask;             // bits to be considered as Faults
    uint16_t    status_mask;            // bits to be considered as status
    char        active_str[12][24];     // string to display when active
    //  char        inactive_str[12][24];   // string to display when inactive
};

typedef struct
{
    char                *               const name;
    struct TRegFgcDimModuleRegisterInfo const * reg;
} regfgc3_dim_chain_t;

struct TRegFgcDimModuleData
{
    bool     is_present;
    uint16_t    logical_to_physical;
};

typedef struct
{
    struct TRegFgcDimModuleData     dim_module[DIM_MODULES_IN_REGFGC3_CHASSIS];
    uint16_t const         *        bus_id_from_card;
    regfgc3_dim_chain_t const   *   dim_info;
} regfgc3_t;


typedef struct
{
    //  card VS Control State card v3, in chassis Linac 4 Klystron Modulator v1 & v2

    uint16_t                    scaled_modulator_period;
    uint16_t                    scaled_measurement_period;
    uint16_t                    scaled_bouncer_duration;
    uint16_t                    scaled_modulator_duration;
    union TUnion32Bits          scaled_repetition_rate;
    uint16_t                    timing_mode;
    // on 16-9-2013 changed from
    //    0=free running, 1=single pulse, 15=disabled
    // to 0=local, 1=remote
    union TUnion32Bits          scaled_initial_delay;
} data_for_card_VsControlState_v3_km2_t ;

typedef struct
{
    //  card VS Control State card v3, in chassis MaxiDisCap
    //  block     : 0x00 - Window detection
    uint16_t    blk0_data0;
    uint32_t    blk0_data1;
    uint32_t    blk0_data2;
    uint32_t    blk0_data3;
    uint32_t    blk0_data4;
    uint32_t    blk0_data5;
    uint32_t    blk0_data6;
    uint32_t    blk0_data7;
    //  block     : 0x01 - State machine delays
    uint16_t    blk1_data0;
    uint16_t    blk1_data1;
    uint16_t    blk1_data2;
    uint16_t    blk1_data3;
    //  block     : 0x02 - Local Timing
    uint32_t    blk2_data0;
    uint32_t    blk2_data1;
    uint32_t    blk2_data2;
    uint32_t    blk2_data3;
    uint32_t    blk2_data4;
    uint32_t    blk2_data5;
} data_for_card_VsControlState_v3_mx_t ;

typedef union
{
    data_for_card_VsControlState_v3_km2_t   km2; // parameters used by Klystron Modulator 1 & 2
    data_for_card_VsControlState_v3_mx_t    mx;  // parameters used by MaxiDisCap
} data_for_card_VsControlState_v3_t ;

typedef struct
{
    //  card VS Analog INTK - v4, in chassis Linac 4 Klystron Modulator v2
    // block 0
    uint16_t    DACA_offset;
    uint16_t    Vin3_Ibcb_th_positive;      // [Hminus] Vin3_Ic_th_positive
    uint16_t    Vin3_Ibcb_th_negative;      // [Hminus] Vin3_Ic_th_negative
    uint16_t    Vin4_Iusnbc_th_positive;
    uint16_t    Vin4_Iusnbc_th_negative;
    uint16_t    Vin5_Icb_th_positive;
    uint16_t    Vin5_Icb_th_negative;
    uint16_t    Vin6_pressure_th_positive;
    uint16_t    Vin6_pressure_th_negative;
    uint16_t    Vin7_temp_th_positive;
    uint16_t    Vin7_temp_th_negative;
    uint16_t    Vin8_Ubouncer_th_positive;  // [Hminus] Vin8_AN_spare_th_positive
    uint16_t    Vin8_Ubouncer_th_negative;  // [Hminus] Vin8_AN_spare_th_negative
    // block 1
    uint16_t    DACB_offset;
    uint16_t    Vin13_Ip_th_positive;
    uint16_t    Vin13_Ip_th_negative;
    uint16_t    Vin14_Uk_th_positive;       // [Hminus] Vin14_Us_th_positive
    uint16_t    Vin14_Uk_th_negative;       // [Hminus] Vin14_Us_th_negative
    uint16_t    Vin15_Ik2_th_positive;      // [Hminus] Vin15_Uigbt_th_positive
    uint16_t    Vin15_Ik2_th_negative;      // [Hminus] Vin15_Uigbt_th_negative
    uint16_t    Vin16_Ik1_th_positive;      // [Hminus] Vin16_Us_GND_th_positive
    uint16_t    Vin16_Ik1_th_negative;      // [Hminus] Vin16_Us_GND_th_negative
    uint16_t    Vin17_Uc_th_positive;
    uint16_t    Vin17_Uc_th_negative;
    uint16_t    Vin18_Arc_th_positive;
    uint16_t    Vin18_Arc_th_negative;
    uint16_t    Vin19_Up_th_positive;
    uint16_t    Vin19_Up_th_negative;
    uint16_t    Vin20_Is_th_positive;      // [Hminus]
    uint16_t    Vin20_Is_th_negative;      // [Hminus]

    // block 2
    uint16_t    DigPot_Wiper1;
    uint16_t    DigPot_Wiper2;
}  data_for_card_VsAnalogIntk_v4_t ;

typedef struct
{
    //  card Siramatrix, in chassis MaxiDisCap
    // block 0
    uint16_t    data_00;                    // [02] ffff 65535
    uint16_t    data_01;                    // [04] 0bb8  3000
    uint16_t    data_02;                    // [06] 03e8  1000
    uint16_t    data_03;                    // [08] 0bb8  3000
    uint16_t    data_04;                    // [10] 3c9a 15514
    uint16_t    data_05;                    // [12] 3b66 15206
    uint16_t    data_06;                    // [14] 0556  1366
    uint16_t    data_07;                    // [16] 08e5  2277
    uint16_t    data_08;                    // [18] 0c74  3188
    uint16_t    data_09;                    // [20] 1003  4099
    uint16_t    data_10;                    // [22] 037b   891
    uint16_t    data_11;                    // [24] 0562  1378
    uint16_t    data_12;                    // [26] 08b2  2226
    uint16_t    data_13;                    // [28] 1245  4677
    uint16_t    data_14;                    // [30] 001d    29
    uint16_t    data_15;                    // [32] 0307   775
    uint16_t    data_16;                    // [34] 0a5c  2652
    uint16_t    data_17;                    // [36] 2359  9049
    uint16_t    data_18;                    // [38] 02c0   704
    uint16_t    data_19;                    // [40] 1cc0  7360
    uint16_t    data_20;                    // [42] 0000     0
} data_for_card_Siramatrix_t ;

float VAIv4_UnScaleFromDac_A(const uint16_t value);
float VAIv4_UnScaleFromDac_B(const uint16_t value);
uint16_t VAIv4_ScaleToDac_A(const float new_value);
uint16_t VAIv4_ScaleToDac_B(const float new_value);

// Global variable definitions

REGFGC3_CARDS_VARS_EXT data_for_card_VsControlState_v3_t    card_VCTv3;
REGFGC3_CARDS_VARS_EXT data_for_card_VsAnalogIntk_v4_t      card_VAIv4;
REGFGC3_CARDS_VARS_EXT data_for_card_Siramatrix_t           card_Siramatrix;

REGFGC3_CARDS_VARS_EXT regfgc3_t regfgc3;

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_empty[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[0].b00",                     // b00 ON label
            "r[0].b01",                     // b01 ON label
            "r[0].b02",                     // b02 ON label
            "r[0].b03",                     // b03 ON label
            "r[0].b04",                     // b04 ON label
            "r[0].b05",                     // b05 ON label
            "r[0].b06",                     // b06 ON label
            "r[0].b07",                     // b07 ON label
            "r[0].b08",                     // b08 ON label
            "r[0].b09",                     // b09 ON label
            "r[0].b10",                     // b10 ON label
            "r[0].b11"                      // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[1].b00",                     // b00 ON label
            "r[1].b01",                     // b01 ON label
            "r[1].b02",                     // b02 ON label
            "r[1].b03",                     // b03 ON label
            "r[1].b04",                     // b04 ON label
            "r[1].b05",                     // b05 ON label
            "r[1].b06",                     // b06 ON label
            "r[1].b07",                     // b07 ON label
            "r[1].b08",                     // b08 ON label
            "r[1].b09",                     // b09 ON label
            "r[1].b10",                     // b10 ON label
            "r[1].b11"                      // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[2].b00",                     // b00 ON label
            "r[2].b01",                     // b01 ON label
            "r[2].b02",                     // b02 ON label
            "r[2].b03",                     // b03 ON label
            "r[2].b04",                     // b04 ON label
            "r[2].b05",                     // b05 ON label
            "r[2].b06",                     // b06 ON label
            "r[2].b07",                     // b07 ON label
            "r[2].b08",                     // b08 ON label
            "r[2].b09",                     // b09 ON label
            "r[2].b10",                     // b10 ON label
            "r[2].b11"                      // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[3].b00",                     // b00 ON label
            "r[3].b01",                     // b01 ON label
            "r[3].b02",                     // b02 ON label
            "r[3].b03",                     // b03 ON label
            "r[3].b04",                     // b04 ON label
            "r[3].b05",                     // b05 ON label
            "r[3].b06",                     // b06 ON label
            "r[3].b07",                     // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[4].b00",                     // b00 ON label
            "r[4].b01",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// VS Control State card v3 (1 DIM embedded), in chassis Linac 4 Klystron Modulator v1 and v2

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_VsControlStateCard_v3[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Fault IN 1",                   // b00 ON label
            "Fault IN 2",                   // b01 ON label
            "Fault IN 3",                   // b02 ON label
            "Fault IN 4",                   // b03 ON label
            "Fault IN 5",                   // b04 ON label
            "Fault IN 6",                   // b05 ON label
            "Fault IN 7",                   // b06 ON label
            "Fault IN 8",                   // b07 ON label
            "Fault EXT 1 INTK 1",           // b08 ON label
            "Fault EXT 1 INTK 2",           // b09 ON label
            "Fault EXT 2 INTK 1",           // b10 ON label
            "RFI Fast Power Abort"          // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0007,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Fault EARTH",                  // b00 ON label
            "Fault INTERNAL",               // b01 ON label
            "Fault Fast-PA Internal",       // b02 ON label
            "r[1].b03",                     // b03 ON label
            "r[1].b04",                     // b04 ON label
            "r[1].b05",                     // b05 ON label
            "r[1].b06",                     // b06 ON label
            "r[1].b07",                     // b07 ON label
            "r[1].b08",                     // b08 ON label
            "r[1].b09",                     // b09 ON label
            "r[1].b10",                     // b10 ON label
            "r[1].b11"                      // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "SW_LB_MONITOR",                // b00 ON label
            "SW_SFGU3_STS",                 // b01 ON label
            "SW_SFGU2_STS",                 // b02 ON label
            "SW_SFGU1_STS",                 // b03 ON label
            "SW_NOREDUN_STS",               // b04 ON label
            "SW_SWITCH_STS",                // b05 ON label
            "CHI_CH_REFLIM_STS",            // b06 ON label
            "CHI_CH_L/R_STS",               // b07 ON label
            "CHI_CH_VREG_STS",              // b08 ON label
            "CHI_CH_IREG_STS",              // b09 ON label
            "D_SW_OK",                      // b10 ON label
            "CHI_CH_ON_STS"                 // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0000,
        .status_mask = 0x03FF,
        .active_str  =
        {
            "SW_SF_GU1",                    // b00 ON label
            "SW_SF_GU2",                    // b01 ON label
            "SW_SF_GU3",                    // b02 ON label
            "SW_SFA_GU1",                   // b03 ON label
            "SW_SFA_GU2",                   // b04 ON label
            "SW_SFA_GU3",                   // b05 ON label
            "MA1_OK",                       // b06 ON label
            "MA2_OK",                       // b07 ON label
            "RFI_PP",                       // b08 ON label
            "ACC_STS1",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[4].b00",                     // b00 ON label
            "r[4].b01",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// VS Control State card v3 (1 DIM embedded), in chassis MaxiDisCap

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_MaxiDisCap_VsControlStateCard_v3[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Fault charger 1",              // b00 ON label
            "Fault charger 2",              // b01 ON label
            "Fault charger spare",          // b02 ON label
            "Fault manual CC",              // b03 ON label
            "Fault over temperature",       // b04 ON label
            "Fault fuse",                   // b05 ON label
            "Red Button",                   // b06 ON label
            "Fault IN 8",                   // b07 ON label
            "MRA",                          // b08 ON label
            "external",                     // b09 ON label
            "MRB",                          // b10 ON label
            "Fast Power Abort"              // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0037,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Fault EARTH",                  // b00 ON label
            "Fault Fast-PA Internal",       // b01 ON label
            "MCB wrong state",              // b02 ON label
            "r[1].b03",                     // b03 ON label
            "Fault PSU",                    // b04 ON label
            "FGC3_Fault",                   // b05 ON label
            "r[1].b06",                     // b06 ON label
            "r[1].b07",                     // b07 ON label
            "r[1].b08",                     // b08 ON label
            "r[1].b09",                     // b09 ON label
            "r[1].b10",                     // b10 ON label
            "r[1].b11"                      // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "DCCT",                         // b00 ON label
            "PWR crate Not ON",             // b01 ON label
            "PWR crate Not OFF",            // b02 ON label
            "charging Not OK",              // b03 ON label
            "Dig Ctrl 2",                   // b04 ON label
            "End of charge Not OK",         // b05 ON label
            "charger 2",                    // b06 ON label
            "polarity Not plus",            // b07 ON label
            "polarity Not minus",           // b08 ON label
            "24vPS 2 Not OK",               // b09 ON label
            "24vPS 1 & Buffer Not OK",      // b10 ON label
            "Status IN 12"                  // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "FGC3: Polarity to PosNot",     // b00 ON label
            "FGC3: Polarity to NegNot",     // b01 ON label
            "FGC3_Fault",                   // b02 ON label
            "Status IN 16",                 // b03 ON label
            "Status IN 17",                 // b04 ON label
            "Status IN 18",                 // b05 ON label
            "Status IN 19",                 // b06 ON label
            "Status IN 20",                 // b07 ON label
            "Status IN 21",                 // b08 ON label
            "Status IN 22",                 // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0107,
        .active_str  =
        {
            "Warning Fw-W",                 // b00 ON label
            "Warning Start",                // b01 ON label
            "Warning Acq",                  // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "Warning PSU"                   // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// VS Control State card (2 DIM embedded), in chassis MidiDisCap

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_MidiDisCap_VsControlStateCard_A[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Lin ctr psu",                  // b00 ON label
            "out over I",                   // b01 ON label
            "Lin pwr psu",                  // b02 ON label
            "polarity Psu",                 // b03 ON label
            "r[0].b04",                     // b04 ON label
            "r[0].b05",                     // b05 ON label
            "r[0].b06",                     // b06 ON label
            "r[0].b07",                     // b07 ON label
            "r[0].b08",                     // b08 ON label
            "veto access",                  // b09 ON label
            "r[0].b10",                     // b10 ON label
            "Fast Power Abort"              // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "earth",                        // b00 ON label
            "equip stop state",             // b01 ON label
            "inverter timeout",             // b02 ON label
            "inputPwrFilter",               // b03 ON label
            "contactor",                    // b04 ON label
            "r[1].b05",                     // b05 ON label
            "r[1].b06",                     // b06 ON label
            "r[1].b07",                     // b07 ON label
            "r[1].b08",                     // b08 ON label
            "r[1].b09",                     // b09 ON label
            "r[1].b10",                     // b10 ON label
            "r[1].b11"                      // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[2].b00",                     // b00 ON label
            "r[2].b01",                     // b01 ON label
            "r[2].b02",                     // b02 ON label
            "r[2].b03",                     // b03 ON label
            "r[2].b04",                     // b04 ON label
            "r[2].b05",                     // b05 ON label
            "r[2].b06",                     // b06 ON label
            "r[2].b07",                     // b07 ON label
            "r[2].b08",                     // b08 ON label
            "r[2].b09",                     // b09 ON label
            "r[2].b10",                     // b10 ON label
            "r[2].b11"                      // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[3].b00",                     // b00 ON label
            "r[3].b01",                     // b01 ON label
            "r[3].b02",                     // b02 ON label
            "r[3].b03",                     // b03 ON label
            "r[3].b04",                     // b04 ON label
            "r[3].b05",                     // b05 ON label
            "r[3].b06",                     // b06 ON label
            "r[3].b07",                     // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[4].b00",                     // b00 ON label
            "r[4].b01",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_MidiDisCap_VsControlStateCard_B[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "HvDcLink over ripple W",       // b00 ON label
            "HvDcLink over volt",           // b01 ON label
            "HvDcLink over ripple F",       // b02 ON label
            "inputPwrFilter psu",           // b03 ON label
            "LvDcLink over volt",           // b04 ON label
            "inverter",                     // b05 ON label
            "inverter ctrl psu",            // b06 ON label
            "inverter pwr psu",             // b07 ON label
            "linear over T",                // b08 ON label
            "fan",                          // b09 ON label
            "linear over P",                // b10 ON label
            "r[0].b11"                      // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "inputPwrFilter preChar",       // b00 ON label
            "r[1].b01",                     // b01 ON label
            "r[1].b02",                     // b02 ON label
            "r[1].b03",                     // b03 ON label
            "r[1].b04",                     // b04 ON label
            "r[1].b05",                     // b05 ON label
            "r[1].b06",                     // b06 ON label
            "r[1].b07",                     // b07 ON label
            "r[1].b08",                     // b08 ON label
            "r[1].b09",                     // b09 ON label
            "r[1].b10",                     // b10 ON label
            "r[1].b11"                      // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[2].b00",                     // b00 ON label
            "r[2].b01",                     // b01 ON label
            "r[2].b02",                     // b02 ON label
            "r[2].b03",                     // b03 ON label
            "r[2].b04",                     // b04 ON label
            "r[2].b05",                     // b05 ON label
            "r[2].b06",                     // b06 ON label
            "r[2].b07",                     // b07 ON label
            "r[2].b08",                     // b08 ON label
            "r[2].b09",                     // b09 ON label
            "r[2].b10",                     // b10 ON label
            "r[2].b11"                      // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[3].b00",                     // b00 ON label
            "r[3].b01",                     // b01 ON label
            "r[3].b02",                     // b02 ON label
            "r[3].b03",                     // b03 ON label
            "r[3].b04",                     // b04 ON label
            "r[3].b05",                     // b05 ON label
            "r[3].b06",                     // b06 ON label
            "r[3].b07",                     // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[4].b00",                     // b00 ON label
            "r[4].b01",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// VS Digital INTK :(1 DIM embedded), in chassis Linac 4 Klystron Modulator v1 and v2

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_VsDigitalIntkCard[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "R_Doors",                      // b00 ON label
            "F_Doors",                      // b01 ON label
            "Oil_Level",                    // b02 ON label
            "Oil_Temp",                     // b03 ON label
            "Red_Button",                   // b04 ON label
            "MCC_Closed",                   // b05 ON label
            "SPARE1",                       // b06 ON label
            "SPARE2",                       // b07 ON label
            "EXT1",                         // b08 ON label
            "Fault IN1 10",                 // b09 ON label
            "Fault IN1 11",                 // b10 ON label
            "Fault IN1 12"                  // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "BUF_PS_Fault",                 // b00 ON label
            "Charger_Fault",                // b01 ON label
            "BIAS_Fault",                   // b02 ON label
            "SW_DSF_Fault",                 // b03 ON label
            "RFI_Fault_SPARE",              // b04 ON label
            "SW_ASF_Fault",                 // b05 ON label
            "SW_PS_Fault",                  // b06 ON label
            "SW_LB_Fault",                  // b07 ON label
            "SW_LBLIM_Fault",               // b08 ON label
            "CHI_CH_PS_Fault",              // b09 ON label
            "CHI_BI_PS_Fault",              // b10 ON label
            "Fault IN2 12"                  // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Fault IN3 1",                  // b00 ON label
            "Fault IN3 2",                  // b01 ON label
            "Fault IN3 3",                  // b02 ON label
            "Fault IN3 4",                  // b03 ON label
            "Fault IN3 5",                  // b04 ON label
            "Fault IN3 6",                  // b05 ON label
            "Fault IN3 7",                  // b06 ON label
            "Fault IN3 8",                  // b07 ON label
            "Fault IN3 9",                  // b08 ON label
            "Fault IN3 10",                 // b09 ON label
            "Fault IN3 11",                 // b10 ON label
            "Fault IN3 12"                  // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0001,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Fault INTERNAL",               // b00 ON label
            "r[3].b01",                     // b01 ON label
            "r[3].b02",                     // b02 ON label
            "r[3].b03",                     // b03 ON label
            "r[3].b04",                     // b04 ON label
            "r[3].b05",                     // b05 ON label
            "r[3].b06",                     // b06 ON label
            "r[3].b07",                     // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[4].b00",                     // b00 ON label
            "r[4].b01",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// VS Analog INTK v3 :(1 DIM embedded), in chassis Linac 4 Klystron Modulator v1 and v2

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_VsAnalogIntkCard_v3[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Ik1>>",                        // b00 ON label
            "Ibcb>>",                       // b01 ON label
            "Iusnb>>",                      // b02 ON label
            "Trf_pressure>>",               // b03 ON label
            "N/A",                          // b04 ON label
            "Icb>>",                        // b05 ON label
            "Ik2>>",                        // b06 ON label
            "Trf_temp>>",                   // b07 ON label
            "VIN 9",                        // b08 ON label
            "VIN 10",                       // b09 ON label
            "VIN 11",                       // b10 ON label
            "VIN 12"                        // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "VIN 13",                       // b00 ON label
            "VIN 14",                       // b01 ON label
            "VIN 15",                       // b02 ON label
            "VIN 16",                       // b03 ON label
            "VIN 17",                       // b04 ON label
            "VIN 18",                       // b05 ON label
            "VIN 19",                       // b06 ON label
            "VIN 20",                       // b07 ON label
            "VIN 21",                       // b08 ON label
            "VIN 22",                       // b09 ON label
            "VIN13",                        // b10 ON label
            "VIN24"                         // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0003,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Trf_temp<<",                   // b00 ON label
            "Fault Internal",               // b01 ON label
            "r[2].b02",                     // b02 ON label
            "r[2].b03",                     // b03 ON label
            "r[2].b04",                     // b04 ON label
            "r[2].b05",                     // b05 ON label
            "r[2].b06",                     // b06 ON label
            "r[2].b07",                     // b07 ON label
            "r[2].b08",                     // b08 ON label
            "r[2].b09",                     // b09 ON label
            "r[2].b10",                     // b10 ON label
            "r[2].b11"                      // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x00FF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Ik1<<",                        // b00 ON label
            "Ibcb<<",                       // b01 ON label
            "Iusnb<<",                      // b02 ON label
            "Trf_pressure<<",               // b03 ON label
            "Ik2<<",                        // b04 ON label
            "Icb<<",                        // b05 ON label
            "r[3].b06",                     // b06 ON label
            "r[3].b07",                     // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[4].b00",                     // b00 ON label
            "r[4].b01",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// VS Analog INTK v3 (mezzanine HV & Fast Interlock) :(1 DIM embedded), in chassis MaxiDisCap

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_VsAnalogIntkCard_v3_MaxiDisCap[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Im>>",                         // b00 ON label
            "Uigbt>>",                      // b01 ON label
            "Ic>>",                         // b02 ON label
            "Uc>>",                         // b03 ON label
            "VIN 5",                        // b04 ON label
            "VIN 6",                        // b05 ON label
            "VIN 7",                        // b06 ON label
            "VIN 8",                        // b07 ON label
            "VIN 9",                        // b08 ON label
            "VIN 10",                       // b09 ON label
            "VIN 11",                       // b10 ON label
            "VIN 12"                        // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "VIN 13",                       // b00 ON label
            "VIN 14",                       // b01 ON label
            "VIN 15",                       // b02 ON label
            "VIN 16",                       // b03 ON label
            "VIN 17",                       // b04 ON label
            "VIN 18",                       // b05 ON label
            "VIN 19",                       // b06 ON label
            "VIN 20",                       // b07 ON label
            "VIN 21",                       // b08 ON label
            "VIN 22",                       // b09 ON label
            "VIN 23",                       // b10 ON label
            "VIN 24"                        // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0003,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Mezzanine",                    // b00 ON label
            "Fault Internal",               // b01 ON label
            "r[2].b02",                     // b02 ON label
            "r[2].b03",                     // b03 ON label
            "r[2].b04",                     // b04 ON label
            "r[2].b05",                     // b05 ON label
            "r[2].b06",                     // b06 ON label
            "r[2].b07",                     // b07 ON label
            "r[2].b08",                     // b08 ON label
            "r[2].b09",                     // b09 ON label
            "r[2].b10",                     // b10 ON label
            "r[2].b11"                      // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[3].b00",                     // b00 ON label
            "r[3].b01",                     // b01 ON label
            "r[3].b02",                     // b02 ON label
            "r[3].b03",                     // b03 ON label
            "r[3].b04",                     // b04 ON label
            "r[3].b05",                     // b05 ON label
            "r[3].b06",                     // b06 ON label
            "r[3].b07",                     // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[4].b00",                     // b00 ON label
            "r[4].b01",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// VS Analog INTK :(1 DIM embedded), in chassis MiDiDisCap

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_VsAnalogIntkCard_MidiDisCap[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Imag>>",                       // b00 ON label
            "Umag>>",                       // b01 ON label
            "Ucapa>>",                      // b02 ON label
            "r[0].b03",                     // b03 ON label
            "r[0].b04",                     // b04 ON label
            "r[0].b05",                     // b05 ON label
            "r[0].b06",                     // b06 ON label
            "r[0].b07",                     // b07 ON label
            "r[0].b08",                     // b08 ON label
            "r[0].b09",                     // b09 ON label
            "r[0].b10",                     // b10 ON label
            "r[0].b11"                      // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[1].b00",                     // b00 ON label
            "r[1].b01",                     // b01 ON label
            "r[1].b02",                     // b02 ON label
            "r[1].b03",                     // b03 ON label
            "r[1].b04",                     // b04 ON label
            "r[1].b05",                     // b05 ON label
            "r[1].b06",                     // b06 ON label
            "r[1].b07",                     // b07 ON label
            "r[1].b08",                     // b08 ON label
            "r[1].b09",                     // b09 ON label
            "r[1].b10",                     // b10 ON label
            "r[1].b11"                      // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "Mezzanine",                    // b00 ON label
            "Fault Internal",               // b01 ON label
            "r[2].b02",                     // b02 ON label
            "r[2].b03",                     // b03 ON label
            "r[2].b04",                     // b04 ON label
            "r[2].b05",                     // b05 ON label
            "r[2].b06",                     // b06 ON label
            "r[2].b07",                     // b07 ON label
            "r[2].b08",                     // b08 ON label
            "r[2].b09",                     // b09 ON label
            "r[2].b10",                     // b10 ON label
            "r[2].b11"                      // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[3].b00",                     // b00 ON label
            "r[3].b01",                     // b01 ON label
            "r[3].b02",                     // b02 ON label
            "r[3].b03",                     // b03 ON label
            "r[3].b04",                     // b04 ON label
            "r[3].b05",                     // b05 ON label
            "r[3].b06",                     // b06 ON label
            "r[3].b07",                     // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "mezzanine",                    // b00 ON label
            "internal",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// VS Analog INTK v3 (mezzanine HV & Fast Interlock) :(1 DIM embedded), in chassis Linac 4 Klystron Modulator v1

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_VsAnalogIntkCard_v3_mezzHvFastIntlck[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Uc>>",                         // b00 ON label
            "Up>>",                         // b01 ON label
            "r[0].b02"                      // b02 ON label
            "Ip>>",                         // b03 ON label
            "r[0].b04"                      // b04 ON label
            "r[0].b05"                      // b05 ON label
            "Uk>>",                         // b06 ON label
            "r[0].b07"                      // b07 ON label
            "r[0].b08",                     // b08 ON label
            "r[0].b09",                     // b09 ON label
            "r[0].b10",                     // b10 ON label
            "r[0].b11"                      // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[1].b00",                     // b00 ON label
            "r[1].b01",                     // b01 ON label
            "r[1].b02",                     // b02 ON label
            "r[1].b03",                     // b03 ON label
            "r[1].b04",                     // b04 ON label
            "r[1].b05",                     // b05 ON label
            "r[1].b06",                     // b06 ON label
            "r[1].b07",                     // b07 ON label
            "r[1].b08",                     // b08 ON label
            "r[1].b09",                     // b09 ON label
            "r[1].b10",                     // b10 ON label
            "r[1].b11"                      // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0003,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[2].b00"                      // b00 ON label
            "Fault Internal",               // b01 ON label
            "r[2].b02",                     // b02 ON label
            "r[2].b03",                     // b03 ON label
            "r[2].b04",                     // b04 ON label
            "r[2].b05",                     // b05 ON label
            "r[2].b06",                     // b06 ON label
            "r[2].b07",                     // b07 ON label
            "r[2].b08",                     // b08 ON label
            "r[2].b09",                     // b09 ON label
            "r[2].b10",                     // b10 ON label
            "r[2].b11"                      // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x00FF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Uc<<",                         // b00 ON label
            "Up<<",                         // b01 ON label
            "r[3].b02"                      // b02 ON label
            "Ip<<",                         // b03 ON label
            "r[3].b04"                      // b04 ON label
            "Uk<<",                         // b05 ON label
            "Uerr<< == Arc",                // b06 ON label
            "Uerr>>",                       // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[4].b00",                     // b00 ON label
            "r[4].b01",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// VS Analog INTK v4 (mezzanine Fast) :(1 DIM embedded), in chassis Linac 4 Klystron Modulator v2

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_VsAnalogIntkCard_v4[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[0].b00",                     // b00 ON label
            "r[0].b01",                     // b01 ON label
            "r[0].b02",                     // b02 ON label
            "r[0].b03",                     // b03 ON label
            "Ibcb>>",                       // b04 ON label
            "Ibcb<<",                       // b05 ON label
            "Iusnbc>>",                     // b06 ON label
            "Iusnc<<",                      // b07 ON label
            "Icb>>",                        // b08 ON label
            "Icb<<",                        // b09 ON label
            "P_Trf>>",                      // b10 ON label
            "P_Trf<<"                       // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "T_Trf>>",                      // b00 ON label
            "T_trf<<",                      // b01 ON label
            "U_Bouncer>>",                  // b02 ON label
            "U_Bouncer<<",                  // b03 ON label
            "r[1].b04",                     // b04 ON label
            "r[1].b05",                     // b05 ON label
            "r[1].b06",                     // b06 ON label
            "r[1].b07",                     // b07 ON label
            "r[1].b08",                     // b08 ON label
            "r[1].b09",                     // b09 ON label
            "r[1].b10",                     // b10 ON label
            "r[1].b11"                      // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0FFF,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Ip>>",                         // b00 ON label
            "Ip<<",                         // b01 ON label
            "Uk>>",                         // b02 ON label
            "Uk<<",                         // b03 ON label
            "Ik_2>>",                       // b04 ON label
            "Ik_2<<",                       // b05 ON label
            "Ik_1>>",                       // b06 ON label
            "Ik_1<<",                       // b07 ON label
            "Uc>>",                         // b08 ON label
            "Uerr>>",                       // b09 ON label
            "Up>>",                         // b10 ON label
            "r[2].b11"                      // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0007,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[3].b00",                     // b00 ON label
            "r[3].b01",                     // b01 ON label
            "Fault Internal",               // b02 ON label
            "r[3].b03",                     // b03 ON label
            "r[3].b04",                     // b04 ON label
            "r[3].b05",                     // b05 ON label
            "r[3].b06",                     // b06 ON label
            "r[3].b07",                     // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x000F,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Uc<<",                         // b00 ON label
            "Uerr<<",                       // b01 ON label
            "Up<<",                         // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

// Anode & Filament Supervision (Mod Anode & Heater) (#1) :(1 DIM embedded), in chassis Linac 4 Klystron Modulator v1 and v2

REGFGC3_CARDS_VARS_EXT const struct TRegFgcDimModuleRegisterInfo
        dim_labels_VsAnodeFilamentSupervision[REGISTERS_IN_REGFGC3_DIM_MODULE]
#ifdef REGFGC3_CARDS_GLOBALS
        =
{
    {
        // reg[0]
        .fault_mask  = 0x007F,
        .status_mask = 0x0000,
        .active_str  =
        {
            "Fault IN1",                    // b00 ON label
            "Fault IN2",                    // b01 ON label
            "Fault IN3",                    // b02 ON label
            "Fault IN4",                    // b03 ON label
            "Low Sup Fault",                // b04 ON label
            "High Sup Fault",               // b05 ON label
            "Fault Sum Mem",                // b06 ON label
            "r[0].b07",                     // b07 ON label
            "r[0].b08",                     // b08 ON label
            "r[0].b09",                     // b09 ON label
            "r[0].b10",                     // b10 ON label
            "r[0].b11"                      // b11 ON label
        }
    },
    {
        // reg[1]
        .fault_mask  = 0x0000,
        .status_mask = 0x0FFF,
        .active_str  =
        {
            "STATUS IN 1",                  // b00 ON label
            "STATUS IN 2",                  // b01 ON label
            "STATUS IN 3",                  // b02 ON label
            "STATUS IN 4",                  // b03 ON label
            "STATUS IN 5",                  // b04 ON label
            "STATUS IN 6",                  // b05 ON label
            "STATUS IN 7",                  // b06 ON label
            "STATUS IN 8",                  // b07 ON label
            "STATUS IN 9",                  // b08 ON label
            "STATUS IN 10",                 // b09 ON label
            "STATUS IN 11",                 // b10 ON label
            "STATUS IN 12"                  // b11 ON label
        }
    },
    {
        // reg[2]
        .fault_mask  = 0x0004,
        .status_mask = 0x0003,
        .active_str  =
        {
            "Blocking Active",              // b00 ON label
            "Any Fault Detected",           // b01 ON label
            "VS Fault",                     // b02 ON label
            "r[2].b03",                     // b03 ON label
            "r[2].b04",                     // b04 ON label
            "r[2].b05",                     // b05 ON label
            "r[2].b06",                     // b06 ON label
            "r[2].b07",                     // b07 ON label
            "r[2].b08",                     // b08 ON label
            "r[2].b09",                     // b09 ON label
            "r[2].b10",                     // b10 ON label
            "r[2].b11"                      // b11 ON label
        }
    },
    {
        // reg[3]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[3].b00",                     // b00 ON label
            "r[3].b01",                     // b01 ON label
            "r[3].b02",                     // b02 ON label
            "r[3].b03",                     // b03 ON label
            "r[3].b04",                     // b04 ON label
            "r[3].b05",                     // b05 ON label
            "r[3].b06",                     // b06 ON label
            "r[3].b07",                     // b07 ON label
            "r[3].b08",                     // b08 ON label
            "r[3].b09",                     // b09 ON label
            "r[3].b10",                     // b10 ON label
            "r[3].b11"                      // b11 ON label
        }
    },
    {
        // reg[4]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[4].b00",                     // b00 ON label
            "r[4].b01",                     // b01 ON label
            "r[4].b02",                     // b02 ON label
            "r[4].b03",                     // b03 ON label
            "r[4].b04",                     // b04 ON label
            "r[4].b05",                     // b05 ON label
            "r[4].b06",                     // b06 ON label
            "r[4].b07",                     // b07 ON label
            "r[4].b08",                     // b08 ON label
            "r[4].b09",                     // b09 ON label
            "r[4].b10",                     // b10 ON label
            "r[4].b11"                      // b11 ON label
        }
    },
    {
        // reg[5]
        .fault_mask  = 0x0000,
        .status_mask = 0x0000,
        .active_str  =
        {
            "r[5].b00",                     // b00 ON label
            "r[5].b01",                     // b01 ON label
            "r[5].b02",                     // b02 ON label
            "r[5].b03",                     // b03 ON label
            "r[5].b04",                     // b04 ON label
            "r[5].b05",                     // b05 ON label
            "r[5].b06",                     // b06 ON label
            "r[5].b07",                     // b07 ON label
            "r[5].b08",                     // b08 ON label
            "r[5].b09",                     // b09 ON label
            "r[5].b10",                     // b10 ON label
            "r[5].b11"                      // b11 ON label
        }
    }
}
#endif
;

#endif  // REGFGC3_CARDS_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: regfgc3_cards.h
\*---------------------------------------------------------------------------------------------------------*/
