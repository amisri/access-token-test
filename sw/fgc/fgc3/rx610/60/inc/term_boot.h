/*---------------------------------------------------------------------------------------------------------*\
  File:         term_boot.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef TERM_BOOT_H      // header encapsulation
#define TERM_BOOT_H

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>               // basic typedefs
#include <stdio.h>                  // for FILE
#include <definfo.h>

enum FGC_term_status
{
    TERM_SUCCESS,
    TERM_FOPEN_FAILURE,
    TERM_SETVBUF_FAILURE,
};

//-----------------------------------------------------------------------------------------------------------

enum FGC_term_status    EnableTerminalStream(void);
void    TermRxCh(uint8_t ch);
uint16_t  TermRxFlush(void);
uint8_t   TermGetCh(uint16_t to_ms);
void    TermPutc(char ch, FILE * f);
void    TermProcessHardware(void);
void    TermTxCh(uint8_t ch);

//-----------------------------------------------------------------------------------------------------------

#endif  // TERM_BOOT_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: sci_boot.h
\*---------------------------------------------------------------------------------------------------------*/
