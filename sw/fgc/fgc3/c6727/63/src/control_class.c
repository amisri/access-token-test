//! @file  control_class.c
//! @brief Class specific converter control


// ---------- Includes

#include "inc/fgc_errs.h"

#include "fgc3/c6727/63/inc/log_class.h"
#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/inc/dsp.h"
#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/spivs.h"
#include "fgc3/inc/crate.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/pc_state.h"
#include "fgc3/inc/status.h"



// ---------- Constants

//! Number of consecutive SPIVS failures that trigger a warning.

#define MAX_NUM_SPIVS_FAILS       10



// ---------- Internal structures, unions and enumerations

typedef void (*send_ref_func_ptr)(void);



// Function memory allocation

#pragma CODE_SECTION(controlClassInit,        ".ext_code")
#pragma CODE_SECTION(controlClassBgp,         ".ext_code")



// ---------- Internal function declarations

//! Dummy function for reference sending

static void controlClassSendRefNone(void);

//! Sends the reference to the SPIVS.

static void controlClassSendRefSpivs(void);

//! Sends the reference to the DAC.

static void controlClassSendRefDaq(void);



// ---------- Internal variable definitions

static send_ref_func_ptr      controlClassSendRef = controlClassSendRefNone;



// ---------- External function definitions

bool controlClassInit(void)
{
    CC_STATIC_ASSERT(REF_OFF              ==  0, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_POL_SWITCHING    ==  1, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_TO_OFF           ==  2, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_TO_STANDBY       ==  3, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_TO_CYCLING       ==  4, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_TO_IDLE          ==  5, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_DIRECT           ==  6, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_STANDBY          ==  7, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_CYCLING          ==  8, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_DYN_ECO          ==  9, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_FULL_ECO         == 10, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_PAUSED           == 11, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_IDLE             == 12, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_ARMED            == 13, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_RUNNING          == 14, Unexpected_REF_STATE_constant_value);
    CC_STATIC_ASSERT(REF_NUM_STATES       == 15, Unexpected_REF_STATE_constant_value);

    // Initialze regulation and reference paramters

    regulationInit();

    bool retval = referenceInit();

    // Initialize libintlk

    interlockInit();

    // Treat all libreg parameters - this must be after logClassInit()

    regulationUpdatePars();

    return retval;
}



void controlClassRtp(void)
{
    // Prepare the signals needed by libreg

    regulationRtp();

    // Run part 1 : Reference generation and regulation

    referenceRegulationRtp();

    profileRegister(FGC_DSP_RT_PROFIL_REF_REG);

    // Output reference only if the converter is not OFF

    controlClassSendRef();

    // Run part 2 : Reference state machine, ILC and circuit simulation

    referenceStateRtp();

    profileRegister(FGC_DSP_RT_PROFIL_REF_STATE);

    // Only run the interlock check if at least a channel is active

    if (testBitmap(dpcom.mcu.log.ignore_mask, DPCOM_LOG_IGNORE_BIS_BIT_MASK) == false)
    {
        interlockRtp();
    }

    profileRegister(FGC_DSP_RT_PROFIL_INTERLOCK);
}



void controlClassBgp(void)
{
    regulationBgp();

    referenceBgp();
}



void controlsClassSetActDest(void)
{
    // POPS-B uses crate type 10, but it doesn't have a DSP card.
    // We have to suppress sending of the reference to the SPIVS,
    // otherwise we wouldn't be able to start the converter because of SPIVS fault

    if (crateGetActuationDest() == CRATE_ACTUATION_DEST_NONE)
    {
        controlClassSendRef = controlClassSendRefNone;
    }
    else
    {
        controlClassSendRef = (  (crateGetActuationDest() == CRATE_ACTUATION_DEST_SPIVS)
                               ? &controlClassSendRefSpivs
                               : &controlClassSendRefDaq);
    }
}



// ---------- Internal function definitions

static void controlClassSendRefNone(void)
{
    ; // Do nothing
}



static void controlClassSendRefSpivs(void)
{
    //! First retrieves the values in the SPIVS rx buffer and then the reference,
    //! measurement, etc are sent. This avoids having to wait for the transmission
    //! to complete in order to read the SPIVS rx contents.
    //!
    //! The caveat is that the words sent and the words received are offset by two
    //! transmissions: one due to reading before writing and the second due to the
    //! due to the DSP card sending the value it receives in the following
    //! transmission.
    //!
    //! Thus a buffer is needed to store the value sent two transmissions before
    //! and be able to check for errors.
    //!
    //! Because of the above the first two transmissions result in a mismatch of
    //! the value sent and received. Hence num_fails = - 2.
    //!
    //! The parameters sent over the SPIVS are the reference, current measurement
    //! and the inductance. The latter is used by some converters (e.g. SIRIUS) to
    //! implement in the DSP card power balance between the capacitors and the
    //! load. As a result, for different converters with the same crate type
    //! DSP_IGBT (APOLO, COMET, SATURN, SIRIUS, etc) the DSP card only needs two
    //! or three parameters and thus only sends back two or three words. For this
    //! reason the SPIVS transmission validation is only done on the first two
    //! words sent instead of all of them.
    //!
    //! The reference sent is the actual V value, without applying VS.GAIN.


    static float   tx_values[3][3];
    static uint8_t indx      =  0;
    static int8_t  num_fails = -2;

    // Output reference only if the converter is operating normally so
    // return immediately if not in NORMAL operational state or converter is off

    if (iter.state_op != FGC_OP_NORMAL || pcStateTest(FGC_STATE_EQ_OFF_BIT_MASK) == true)
    {
        // Register REF_SENT time for profiling even though nothing was sent

        profileRegister(FGC_DSP_RT_PROFIL_REF_SENT);

        // Provide the maximum value of the REF_SENT time in FGC.DEBUG.DSP.RT_MAX_US[1]

        profileMaxPoint(1, FGC_DSP_RT_PROFIL_REF_SENT);

        return;
    }

    // Read data received on the previous iteration

    uint32_t rx_values[3];

    spivsRead(rx_values, 3);

    // Prepare to transmit the Actuation, Imeas and Lsat

    tx_values[indx][0] = referenceGetVarValue(REF_ACTUATION);
    tx_values[indx][1] = regulationGetVarValue(MEAS_I_UNFILTERED);
    tx_values[indx][2] = regulationGetParValue(LOAD_HENRYS) * regulationGetVarValue(LOAD_SAT_FACTOR);

    spivsSend((uint32_t *)tx_values[indx], 3);

    // Register REF_SENT time for profiling

    profileRegister(FGC_DSP_RT_PROFIL_REF_SENT);

    // Provide the maximum value of the REF_SENT time in FGC.DEBUG.DSP.RT_MAX_US[1]

    profileMaxPoint(1, FGC_DSP_RT_PROFIL_REF_SENT);

    // Validate the data received on the previous iteration matches what was sent two iterations ago

    indx = (indx + 1) % 3;

    if (spivsValidate((uint32_t const *)tx_values[indx], rx_values, 1) == SPIVS_INVALID)
    {
        // The fails must be consecutive

        if (++num_fails > MAX_NUM_SPIVS_FAILS)
        {
            // statusSetLatched(FGC_LAT_SPIVS_FLT);
        }
    }
    else
    {
        num_fails = 0;
    }
}



static void controlClassSendRefDaq(void)
{
    // Skip writing the DACs when not in NORMAL or SIMULATION

    if (iter.state_op != FGC_OP_NORMAL && iter.state_op != FGC_OP_SIMULATION)
    {
        return;
    }

    // Write DAC reference to DAC 1

    signalsSetDac(SIGNALS_DAC_1, referenceGetVarValue(REF_DAC));

    // Register time REF_SENT time for profiling

    profileRegister(FGC_DSP_RT_PROFIL_REF_SENT);

    // Provide the maximum value of the REF_SENT time in FGC.DEBUG.DSP.RT_MAX_US[1]

    profileMaxPoint(1, FGC_DSP_RT_PROFIL_REF_SENT);

    // Allow the COMHV or reception crates to manually set the value of DAC-B

    if (   crateGetType() == FGC_CRATE_TYPE_COMHV_PS
        || crateGetType() == FGC_CRATE_TYPE_RECEPTION)
    {
        float ref2 = 0.0;

        if (   pcStateTest(FGC_STATE_GT_BLOCKING_BIT_MASK)
            && property.vs_i_limit.gain > 0.0)
        {
            ref2 = property.vs_i_limit.ref / property.vs_i_limit.gain;
        }

        signalsSetDac(SIGNALS_DAC_2, ref2);
    }

    // For the HL crates the Voltagre reference must also be sent on DAC2

    else if (   crateGetType() == FGC_CRATE_TYPE_HL_2KA_4Q
             || crateGetType() == FGC_CRATE_TYPE_HL_600A_4Q)
    {
        signalsSetDac(SIGNALS_DAC_2, referenceGetVarValue(REF_DAC));
    }
}


// EOF
