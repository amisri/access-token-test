//! @file  signals_class.c
//! @brief Class specific functionality related to signals


// ---------- Includes

#include <libsig.h>
#include <libreg.h>

#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/63/inc/signals_class.h"
#include "fgc3/c6727/inc/calibration.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/sig_structs.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/status.h"



// Function memory allocation

#pragma CODE_SECTION(signalsClassInit,       ".ext_code")
#pragma CODE_SECTION(signalsClassBgp1ms,     ".ext_code")



// ---------- Internal function definitions

static inline void signalClassRtpExtAdcPbam(uint32_t const adc_idx, int32_t adc_raw)
{
    bool adc_fault = false;

    // The two least significant bits in the raw value are fault flags

    // Set ADC fault flag in libsig and increment ADC.EXTERNAL.PBAM.ERRORS.{A,B}
    // counters if the corresponding fault bit is set in the raw value

    if (testBitmap(adc_raw, ANA_EXT_ADC_PBAM_A_SAMPLE_FRAME_ERROR_MASK32) == true)
    {
        property.ext_adc.pbam.errors[adc_idx][0]++;
        adc_fault = true;
    }

    if (testBitmap(adc_raw, ANA_EXT_ADC_PBAM_A_SAMPLE_NO_FRAME_MASK32) == true)
    {
        property.ext_adc.pbam.errors[adc_idx][1]++;
        adc_fault = true;
    }

    // Scale the 32-bit signed value into a 26-bit signed value (+/-33.5M)

    adc_raw >>= 6;

    sigVarValueByIdx(&sig_struct, adc, FGC_N_INT_ADCS + adc_idx, ADC_FAULT_FLAG) = adc_fault;
    sigVarValueByIdx(&sig_struct, adc, FGC_N_INT_ADCS + adc_idx, ADC_RAW)        = adc_raw;
}



static inline void signalClassRtpExtAdcHpm7177(uint32_t const adc_idx, 
                                               int32_t  const adc_raw,
                                               uint32_t const adc_status,
                                               uint32_t const com_status)
{
    bool adc_fault = false;

    if (testBitmap(com_status, ANA_EXT_ADC_HPM7177_A_COM_STATUS_CRC_ERROR_MASK32) == true)
    {
        property.ext_adc.hpm7177.errors[adc_idx][0]++;
        adc_fault = true;
    }
    if (testBitmap(com_status, ANA_EXT_ADC_HPM7177_A_COM_STATUS_EXT_TIMEOUT_MASK32) == true)
    {
        property.ext_adc.hpm7177.errors[adc_idx][1]++;
        adc_fault = true;
    }
    if (testBitmap(adc_status, ANA_EXT_ADC_HPM7177_A_STATUS_EXT_SYNC_GOOD_MASK32) == false)
    {
        property.ext_adc.hpm7177.errors[adc_idx][2]++;
        adc_fault = true;
    }
    if (testBitmap(adc_status, ANA_EXT_ADC_HPM7177_A_STATUS_TEMP_CTRL_LOCKED_MASK32) == false)
    {
        property.ext_adc.hpm7177.errors[adc_idx][3]++;
        adc_fault = true;
    }
    if (testBitmap(adc_status, ANA_EXT_ADC_HPM7177_A_STATUS_FAULT_MASK32) == true)
    {
        property.ext_adc.hpm7177.errors[adc_idx][4]++;
        adc_fault = true;
    }

    adc_fault = false;

    sigVarValueByIdx(&sig_struct, adc, FGC_N_INT_ADCS + adc_idx, ADC_FAULT_FLAG) = adc_fault;
    sigVarValueByIdx(&sig_struct, adc, FGC_N_INT_ADCS + adc_idx, ADC_RAW)        = adc_raw;
}



static void signalClassRtpAcqExtAdcs(void)
{
    // Get the external ADC A value

    uint32_t ext_adc_type_a = dpcls.mcu.adc.ext_type[0];

    if (ext_adc_type_a != FGC_ADC_EXT_TYPE_NONE)
    {
        if (ext_adc_type_a == FGC_ADC_EXT_TYPE_PBAM)
        {
                signalClassRtpExtAdcPbam(0, ANA_EXT_ADC_PBAM_A_SAMPLE_P);
        }
        else if (ext_adc_type_a == FGC_ADC_EXT_TYPE_HPM7177)
        {
            signalClassRtpExtAdcHpm7177(0, 
                                        ANA_EXT_ADC_HPM7177_A_SAMPLE_P, 
                                        ANA_EXT_ADC_HPM7177_A_STATUS_P,
                                        ANA_EXT_ADC_HPM7177_A_COM_STATUS_P);
        }
    }

    uint32_t ext_adc_type_b = dpcls.mcu.adc.ext_type[1];

    if (ext_adc_type_b != FGC_ADC_EXT_TYPE_NONE)
    {
        if (ext_adc_type_b == FGC_ADC_EXT_TYPE_PBAM)
        {
                signalClassRtpExtAdcPbam(1, ANA_EXT_ADC_PBAM_B_SAMPLE_P);
        }
        else if (ext_adc_type_b == FGC_ADC_EXT_TYPE_HPM7177)
        {
            signalClassRtpExtAdcHpm7177(1, 
                                        ANA_EXT_ADC_HPM7177_B_SAMPLE_P, 
                                        ANA_EXT_ADC_HPM7177_B_STATUS_P,
                                        ANA_EXT_ADC_HPM7177_B_COM_STATUS_P);
        }
    }
}



// ---------- Platform/class specific function definitions

void signalsClassInit(struct SIG_struct * const sig_struct)
{
    // Make sure all FGC and cclibs constants agree

    CC_STATIC_ASSERT((SIG_TRANSDUCER_DCCT_A    == FGC_ADC_SIGNALS_I_DCCT_A ), SIG_TRANSDUCER_DCCT_A   );
    CC_STATIC_ASSERT((SIG_TRANSDUCER_DCCT_B    == FGC_ADC_SIGNALS_I_DCCT_B ), SIG_TRANSDUCER_DCCT_B   );
    CC_STATIC_ASSERT((SIG_TRANSDUCER_B_PROBE_A == FGC_ADC_SIGNALS_B_PROBE_A), SIG_TRANSDUCER_B_PROBE_A);
    CC_STATIC_ASSERT((SIG_TRANSDUCER_B_PROBE_B == FGC_ADC_SIGNALS_B_PROBE_B), SIG_TRANSDUCER_B_PROBE_B);
    CC_STATIC_ASSERT((SIG_TRANSDUCER_V_MEAS    == FGC_ADC_SIGNALS_V_MEAS   ), SIG_TRANSDUCER_V_MEAS   );
    CC_STATIC_ASSERT((SIG_TRANSDUCER_V_CAPA    == FGC_ADC_SIGNALS_V_CAPA   ), SIG_TRANSDUCER_V_CAPA   );
    CC_STATIC_ASSERT((SIG_TRANSDUCER_I_CAPA    == FGC_ADC_SIGNALS_I_CAPA   ), SIG_TRANSDUCER_I_CAPA   );
    CC_STATIC_ASSERT((SIG_TRANSDUCER_EXT_REF   == FGC_ADC_SIGNALS_REF      ), SIG_TRANSDUCER_EXT_REF  );
    CC_STATIC_ASSERT((SIG_TRANSDUCER_AUX       == FGC_ADC_SIGNALS_AUX      ), SIG_TRANSDUCER_AUX      );

    sigStructsInit(sig_struct, (struct POLSWITCH_mgr *)&dpcls.polswitch_mgr);
}



// ---------- External function definitions

void signalsClassRtpAcq(void)
{
    // If FGC is an inter FGC consumer of signals transfer them from the dual port RAM to libsig
    // Currently, for the topology SERIES only V_LOAD is shared. The master however, does not need
    // to inject V_LOAD into the transducer since it has its own valid V_LOAD measurement, which is
    // added to the slave V_LOAD measurment in regulation.c::regulationRtp()

    if (   dpcls.inter_fgc_mgr.is_consumer == true
        && dpcls.inter_fgc_mgr.topology    != FGC_IFGC_TOPOLOGY_SERIES)
    {
        // Inject external measurements into proper transducers

        sigTransducerSetExtMeasRT(&sig_struct.transducer.named.v_meas,
                                  dpcls.inter_fgc_mgr.is_enabled[CONSUMED_SIGS_V_LOAD],
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_V_LOAD].is_valid,
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_V_LOAD].signal);

        sigTransducerSetExtMeasRT(&sig_struct.transducer.named.dcct_a,
                                  dpcls.inter_fgc_mgr.is_enabled[CONSUMED_SIGS_I_DCCT_A],
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_I_DCCT_A].is_valid,
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_I_DCCT_A].signal);

        sigTransducerSetExtMeasRT(&sig_struct.transducer.named.dcct_b,
                                  dpcls.inter_fgc_mgr.is_enabled[CONSUMED_SIGS_I_DCCT_B],
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_I_DCCT_B].is_valid,
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_I_DCCT_B].signal);

        sigTransducerSetExtMeasRT(&sig_struct.transducer.named.b_probe_a,
                                  dpcls.inter_fgc_mgr.is_enabled[CONSUMED_SIGS_B_PROBE_A],
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_B_PROBE_A].is_valid,
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_B_PROBE_A].signal);

        sigTransducerSetExtMeasRT(&sig_struct.transducer.named.b_probe_b,
                                  dpcls.inter_fgc_mgr.is_enabled[CONSUMED_SIGS_B_PROBE_B],
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_B_PROBE_B].is_valid,
                                  dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_B_PROBE_B].signal);
    }

    // Read external ADCs if connected

    signalClassRtpAcqExtAdcs();

    // If the WhiteRabbit board is present, inject Bmeas in both B_PROBEs to simplify things

    if (dpcom.mcu.wr.enabled == true)
    {
        bool b_meas_valid = WR_RX_STATUS_P == 0;

        if (b_meas_valid == false)
        {
            if (testBitmap(WR_RX_STATUS_P, WR_RX_STATUS_CRC_ERROR_MASK32) == true)
            {
                property.ext_adc.whiterabbit.errors[0]++;
            }
            if (testBitmap(WR_RX_STATUS_P, WR_RX_STATUS_NET_TIMEOUT_MASK32) == true)
            {
                property.ext_adc.whiterabbit.errors[1]++;
            }
            if (testBitmap(WR_RX_STATUS_P, WR_RX_STATUS_EXT_TIMEOUT_MASK32) == true)
            {
                property.ext_adc.whiterabbit.errors[2]++;
            }
        }

        // Convert the field measurement from 10 nT to G: 1 T = 10E4 G --> 10 nT = 10E-4 G

        float b_meas = (float)WR_RX_B_MEAS_P * 1E-04;
        float b_sim  = (float)WR_RX_B_SIM_P  * 1E-04;

        sigTransducerSetExtMeasRT(&sig_struct.transducer.named.b_probe_a, true, b_meas_valid, b_meas);
        sigTransducerSetExtMeasRT(&sig_struct.transducer.named.b_probe_b, true, b_meas_valid, b_sim);
    }
}



void signalsClassRtp(void)
{
    // Transmit status, Vmeas and Imeas if the WhiteRabbit board is present

    if (dpcom.mcu.wr.enabled == true)
    {
        uint32_t wr_status_value = 0;

        if (referenceGetParValue(MODE_SIM_MEAS) == true)  { wr_status_value |=  WR_TX_STATUS_SIMULATION_MASK16; }
        if (statusGetFaults()                   != 0   )  { wr_status_value |=  WR_TX_STATUS_FAULT_MASK32;      }

        WR_TX_V_MEAS_P = regulationGetVarValue(MEAS_V_UNFILTERED);
        WR_TX_I_MEAS_P = regulationGetVarValue(MEAS_I_UNFILTERED);

        // Use a memory barrier to guarantee that the writing into the STATUS register is done
        // last since this action triggers the transmission of the data

        CC_MEMORY_BARRIER;

        WR_TX_STATUS_P = wr_status_value;
    }
}



void signalsClassUpdateAdcToTrans(void)
{
    uint16_t ext_adc_idx;

    for (ext_adc_idx = 0; ext_adc_idx < FGC_N_EXT_ADCS; ext_adc_idx++)
    {
        // External ADCs follow the internal ADCs in sig_struct

        sigVarValueByIdx(&sig_struct, adc, FGC_N_INT_ADCS + ext_adc_idx, ADC_TRANSDUCER_INDEX) = property.ext_adc.signals[ext_adc_idx];
    }
}



void signalsClassBgp1ms(void)
{
    // If possible, use libreg values, which take into account if the simulation is
    // enabled and the source of the measurement (internal, inter-FGC, external).

    dpcls.dsp.meas.v      = regulationGetVarValue(MEAS_V_UNFILTERED);
    dpcls.dsp.meas.v_capa = sigVarValue(&sig_struct, transducer, v_capa, TRANSDUCER_MEAS_UNFILTERED);
    dpcls.dsp.meas.i      = regulationGetVarValue(MEAS_I_UNFILTERED);
    dpcls.dsp.meas.ia     = sigVarValue(&sig_struct, transducer, dcct_a, TRANSDUCER_MEAS_UNFILTERED);
    dpcls.dsp.meas.ib     = sigVarValue(&sig_struct, transducer, dcct_b, TRANSDUCER_MEAS_UNFILTERED);
    dpcls.dsp.meas.i_capa = regulationGetVarValue(MEAS_I_CAPA_UNFILTERED);
    dpcls.dsp.meas.b      = regulationGetVarValue(MEAS_B_UNFILTERED);

    if (iter.state_op != FGC_OP_SIMULATION || property.meas.sim == CC_DISABLED)
    {
        // Disable simulation

        referenceSetParValue(MODE_SIM_MEAS, CC_DISABLED);
    }
    else
    {
        // Enable simulation

        referenceSetParValue(MODE_SIM_MEAS, CC_ENABLED);
    }

    // Update MEAS.I_DIFF_MA

    dpcls.dsp.meas.i_diff_ma = sigVarValue(&sig_struct, select, i_meas,  SELECT_MILLI_ABS_DIFF);

    // Evaluate I_MEAS warning

    bool i_meas_wrn = (   sigVarValue(&sig_struct, select, i_meas, SELECT_WARNINGS) != 0
                       || sigVarValue(&sig_struct, select, b_meas, SELECT_WARNINGS) != 0);

    statusEvaluateWarnings(FGC_WRN_MEAS, i_meas_wrn);
}



void signalsClassInterFgc(void)
{
    // If FGC is an inter FGC producer, then transfer filtered measurements to the MCU

    if (dpcls.inter_fgc_mgr.is_producer == true)
    {
        if (iter.state_op != FGC_OP_SIMULATION || property.meas.sim == CC_DISABLED)
        {
            // Real measurements

            // I_MEAS

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_I_MEAS].signal   = sigVarValue(&sig_struct, select,     i_meas,    SELECT_MEAS_FILTERED);
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_I_MEAS].is_valid = sigVarValue(&sig_struct, select,     i_meas,    SELECT_MEAS_FILTERED_IS_VALID);

            // V_LOAD / V_OUT / V_MEAS

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_LOAD].signal   = sigVarValue(&sig_struct, transducer, v_meas,    TRANSDUCER_MEAS_FILTERED);
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_LOAD].is_valid = sigVarValue(&sig_struct, transducer, v_meas,    TRANSDUCER_MEAS_FILTERED_IS_VALID);

            // V_CAPA

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_CAPA].signal   = sigVarValue(&sig_struct, transducer, v_capa,    TRANSDUCER_MEAS_FILTERED);
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_CAPA].is_valid = sigVarValue(&sig_struct, transducer, v_capa,    TRANSDUCER_MEAS_FILTERED_IS_VALID);

            // B_MEAS

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_MEAS].signal   = sigVarValue(&sig_struct, select,     b_meas,    SELECT_MEAS_FILTERED);
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_MEAS].is_valid = sigVarValue(&sig_struct, select,     b_meas,    SELECT_MEAS_FILTERED_IS_VALID);

            // B_PROBE_A

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_PROBE_A].signal   = sigVarValue(&sig_struct, transducer, b_probe_a, TRANSDUCER_MEAS_FILTERED);
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_PROBE_A].is_valid = sigVarValue(&sig_struct, transducer, b_probe_a, TRANSDUCER_MEAS_FILTERED_IS_VALID);

            // B_PROBE_B

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_PROBE_B].signal   = sigVarValue(&sig_struct, transducer, b_probe_b, TRANSDUCER_MEAS_FILTERED);
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_PROBE_B].is_valid = sigVarValue(&sig_struct, transducer, b_probe_b, TRANSDUCER_MEAS_FILTERED_IS_VALID);
        }
        else
        {
            // Simulated measurements

            // I_MEAS

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_I_MEAS].signal   = dpcls.dsp.meas.i;
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_I_MEAS].is_valid = true;

            // V_LOAD / V_OUT / V_MEAS

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_LOAD].signal   = dpcls.dsp.meas.v;
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_LOAD].is_valid = true;

            // V_CAPA

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_CAPA].signal   = 0.0F;
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_CAPA].is_valid = true;

            // B_MEAS

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_MEAS].signal   = dpcls.dsp.meas.b;
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_MEAS].is_valid = true;

            // B_PROBE_A

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_PROBE_A].signal   = dpcls.dsp.meas.b;
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_PROBE_A].is_valid = true;

            // B_PROBE_B

            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_PROBE_B].signal   = dpcls.dsp.meas.b;
            dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_PROBE_B].is_valid = true;
        }

        // B_DOT - Convert the rate of change of field from uT/s to G/s: 1 T = 10E4 G --> 1 uT/s = 10E-2 G/s

        dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_DOT].signal   = (float)WR_RX_B_DOT_P * 1E-02F;
        dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_B_DOT].is_valid = sigVarValue(&sig_struct, transducer, b_probe_a, TRANSDUCER_EXT_MEAS_IS_VALID);

        // V_REF

        dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_REF].signal   =  regulationGetVarValue(V_REF);
        dpcls.inter_fgc_mgr.produced_sigs[IFGC_SIGNAL_INDEX_V_REF].is_valid =  true;
    }

    // Transfer voltage reference in case FGC is an inter FGC master

    if (dpcls.inter_fgc_mgr.is_master == true)
    {
        dpcls.inter_fgc_mgr.ext_ref = regulationGetVarValue(V_REF_VS);
    }
}



void signalsClassBgp20ms(void)
{
    // DCCT state

    enum SIG_selector selector = sigVarValue(&sig_struct, select, i_meas, SELECT_SELECTOR);
    uint32_t          faults;
    uint32_t          state;
    uint8_t           idx;

    for (idx = 0; idx < 2; idx++)
    {
        faults   = sigVarValueByIdx(&sig_struct, transducer, idx, TRANSDUCER_FAULTS);
        state    = FGC_DCCT_MEAS_OK;

        if (faults != 0)
        {
            clrBitmap(state, FGC_DCCT_MEAS_OK);

            // CAL_FAILED

            if (testBitmap(faults, SIG_TRANSDUCER_CAL_FLT_BIT_MASK))
            {
                setBitmap(state, FGC_DCCT_CAL_FLT);
            }

            // FAULT

            if (testBitmap(faults, SIG_TRANSDUCER_FLT_BIT_MASK))
            {
                setBitmap(state, FGC_DCCT_FAULT);
            }
        }

        // DCCT.SELECT

        if ((idx == 0 && selector == SIG_B) ||
            (idx == 1 && selector == SIG_A))
        {
            clrBitmap(state, FGC_DCCT_MEAS_OK);
        }

        dpcom.dsp.meas.dcct_state[idx] = state;
    }
}



void signalsClassBgp200ms(void)
{
    // Update DCCT.SELECT image seen by the MCU

    dpcls.dsp.dcct_select = sigVarValue(&sig_struct, select, i_meas, SELECT_SELECTOR);

    // Update measurement status for the RTD

    uint32_t faults;

    faults = sigVarValue(&sig_struct, transducer, v_meas, TRANSDUCER_FAULTS);
    dpcls.dsp.status.meas_v = (testBitmap(faults, SIG_TRANSDUCER_ADC_NOT_AVL_FLT_BIT_MASK) == true)
                            ? FGC_DCCT_FLTS_ADC_NOT_AVL
                            : (faults != 0 ? FGC_DCCT_FLTS_DCCT_FAULT : 0);

    faults = sigVarValue(&sig_struct, select, i_meas, SELECT_FAULTS);
    dpcls.dsp.status.meas_i = (testBitmap(faults, SIG_SELECT_TRANSDUCER_NOT_AVL_FLT_BIT_MASK) == true)
                            ? FGC_DCCT_FLTS_ADC_NOT_AVL
                            : (faults != 0 ? FGC_DCCT_FLTS_DCCT_FAULT : 0);

    faults = sigVarValue(&sig_struct, select, b_meas, SELECT_FAULTS);
    dpcls.dsp.status.meas_b = (testBitmap(faults, SIG_SELECT_TRANSDUCER_NOT_AVL_FLT_BIT_MASK) == true)
                            ? FGC_DCCT_FLTS_ADC_NOT_AVL
                            : (faults != 0 ? FGC_DCCT_FLTS_DCCT_FAULT : 0);
}



bool signalsClassGetIMeasDiffWarning(void)
{
    return testBitmap(sigVarValue(&sig_struct, select, i_meas, SELECT_WARNINGS), SIG_SELECT_DIFF_WARN_BIT_MASK);
}



cc_float signalsClassExtRefGetValue(void)
{
    return (sigVarValue(&sig_struct, transducer, ext_ref, TRANSDUCER_MEAS_UNFILTERED));
}



bool signalsClassExtRefIsEnabled(void)
{
    return (sigTransducerSourceIsAdc(&sig_struct.transducer.named.ext_ref));
}



void signalClassSetExtAdcLimits(void)
{
    sig_struct.adc.named.adc_ext_a.cal.cal_limit_index = dpcls.mcu.adc.ext_type[0];
    sig_struct.adc.named.adc_ext_b.cal.cal_limit_index = dpcls.mcu.adc.ext_type[1];
}

// EOF
