//! @file property_class.h
//! @brief Variables associated to class-specific properties


// ---------- Includes

#include <stdint.h>
#include <string.h>

#include "inc/fgc_parser_consts.h"

#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/inc/bitmap.h"



// ---------- External variable definitions

#pragma DATA_SECTION(property, ".ext_data")

struct Property     property;



// ---------- External function definitions

void propertyClassInit(void)
{
    memset(&property, 0, sizeof(property));
}



void propertyClassGetPointer(uint8_t   *  const base_address,
                             uint32_t     const flags,
                             uint8_t      const sub_sel,
                             uint8_t      const cyc_sel,
                             uint8_t  **        var_pointer,
                             uint32_t  *        n_elem_offset)
{
    uint32_t sub_sel_step = 0;
    uint32_t cyc_sel_step = 0;
    uint8_t  prop_sub_sel = 0;
    uint8_t  prop_cyc_sel = 0;

    if (testBitmap(flags, PF_SUB_DEV) == true)
    {
        sub_sel_step = sizeof(uint32_t);
        prop_sub_sel = sub_sel;
    }

    if (testBitmap(flags, PF_PPM) == true)
    {
        cyc_sel_step = sizeof(property.ppm[0]);
        prop_cyc_sel = cyc_sel;
    }

    referenceGetPointer(base_address,
                        prop_sub_sel,
                        prop_cyc_sel,
                        sub_sel_step,
                        cyc_sel_step,
                        var_pointer);

    *n_elem_offset = prop_sub_sel * sub_sel_step + prop_cyc_sel * cyc_sel_step;
}


// EOF
