//! @file  interlock_class.c
//! @brief Interface to cclibs libintlk


// ---------- Includes

#include <libintlk.h>
#include <libref.h>

#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/inc/pc_state.h"



// ---------- Platform/class specific function definitions

uint32_t interlockClassGetChannelTest(void)
{
    return property.intlk.channel_test;
}



uint32_t interlockClassGetOutput(uint8_t sub_sel, uint8_t cyc_sel)
{
    return property.ppm[cyc_sel].intlk.output[sub_sel];
}



// ---------- External function definitions

void interlockClassInit(struct Interlock * interlock, struct INTLK_mgr * intlk_mgr)
{
    uint8_t i;

    for (i = 0; i < FGC_MAX_USER_PLUS_1; i++)
    {
        memset(&property.ppm[i].intlk.pars, 0, sizeof(struct INTLK_pars));
    }

    memset(intlk_mgr, 0, sizeof(struct INTLK_mgr));

    // Initialize the libintlk library

    intlkInit(intlk_mgr,
              &interlock->sub_sel,
              &interlock->cyc_sel,
              (uint32_t *)&dpcom.mcu.pc.state_bitmask,
              regulationGetVarPointer(REG_ABS_ERR),
              regulationGetVarPointer(MEAS_V_UNFILTERED),
              regulationGetVarPointer(MEAS_I_UNFILTERED),
              regulationGetVarValue(ITER_PERIOD),
              &property.ppm[0].intlk.pars,
              sizeof(struct Property_ppm),
              &property.ppm[0].intlk.latch,
              sizeof(struct Property_ppm),
              (  FGC_STATE_DIRECT_BIT_MASK | FGC_STATE_CYCLING_BIT_MASK | FGC_STATE_ECONOMY_BIT_MASK
               | FGC_STATE_IDLE_BIT_MASK   | FGC_STATE_ARMED_BIT_MASK   | FGC_STATE_RUNNING_BIT_MASK));
}


// EOF
