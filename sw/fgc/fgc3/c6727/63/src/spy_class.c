//! @file  spy_class.c
//! @brief Class specific signals accessible through the serial port


// ---------- Includes

#include <stdint.h>

#include <libreg.h>
#include <libsig.h>

#include <defconst.h>

#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/inc/dpcom.h"



// ---------- Constants

//<! Number of ticks per second

#define PLL_TICKS_TO_SECONDS            25000000



// Function memory allocation

#pragma CODE_SECTION(spyClassGetSignal, ".ext_code")



// ---------- Platform/class specific function definitions

float spyClassGetSignal(uint32_t const idx)
{
    switch (idx)
    {
        //  INPUT SIGNALS

        case FGC_SPY_RAW_A:                 return (float)(sigVarValue(&sig_struct, adc,        adc_a,     ADC_RAW));
        case FGC_SPY_RAW_B:                 return (float)(sigVarValue(&sig_struct, adc,        adc_b,     ADC_RAW));
        case FGC_SPY_RAW_C:                 return (float)(sigVarValue(&sig_struct, adc,        adc_c,     ADC_RAW));
        case FGC_SPY_RAW_D:                 return (float)(sigVarValue(&sig_struct, adc,        adc_d,     ADC_RAW));
        case FGC_SPY_V_ADC_A:               return (float)(sigVarValue(&sig_struct, adc,        adc_a,     ADC_MEAS_UNFILTERED));
        case FGC_SPY_V_ADC_B:               return (float)(sigVarValue(&sig_struct, adc,        adc_b,     ADC_MEAS_UNFILTERED));
        case FGC_SPY_V_ADC_C:               return (float)(sigVarValue(&sig_struct, adc,        adc_c,     ADC_MEAS_UNFILTERED));
        case FGC_SPY_V_ADC_D:               return (float)(sigVarValue(&sig_struct, adc,        adc_d,     ADC_MEAS_UNFILTERED));
        case FGC_SPY_I_DCCT_A:              return (float)(sigVarValue(&sig_struct, transducer, dcct_a,    TRANSDUCER_MEAS_UNFILTERED));
        case FGC_SPY_I_DCCT_B:              return (float)(sigVarValue(&sig_struct, transducer, dcct_b,    TRANSDUCER_MEAS_UNFILTERED));
        case FGC_SPY_B_PROBE_A:             return (float)(sigVarValue(&sig_struct, transducer, b_probe_a, TRANSDUCER_MEAS_UNFILTERED));
        case FGC_SPY_B_PROBE_B:             return (float)(sigVarValue(&sig_struct, transducer, b_probe_b, TRANSDUCER_MEAS_UNFILTERED));
        case FGC_SPY_V_MEAS:                return (float)(sigVarValue(&sig_struct, transducer, v_meas,    TRANSDUCER_MEAS_UNFILTERED));
        case FGC_SPY_EXT_REF:               return (float)(sigVarValue(&sig_struct, transducer, ext_ref,   TRANSDUCER_MEAS_UNFILTERED));
        case FGC_SPY_DCCT_DIFF:             return (float)(sigVarValue(&sig_struct, select,     i_meas,    SELECT_ABS_DIFF));
        case FGC_SPY_V_FLTR_A:              return (float)(sigVarValue(&sig_struct, adc,        adc_a,     ADC_MEAS_FILTERED));
        case FGC_SPY_V_FLTR_B:              return (float)(sigVarValue(&sig_struct, adc,        adc_b,     ADC_MEAS_FILTERED));
        case FGC_SPY_V_FLTR_C:              return (float)(sigVarValue(&sig_struct, adc,        adc_c,     ADC_MEAS_FILTERED));
        case FGC_SPY_V_FLTR_D:              return (float)(sigVarValue(&sig_struct, adc,        adc_d,     ADC_MEAS_FILTERED));
        case FGC_SPY_DCCT_FLTR_A:           return (float)(sigVarValue(&sig_struct, transducer, dcct_a,    TRANSDUCER_MEAS_FILTERED));
        case FGC_SPY_DCCT_FLTR_B:           return (float)(sigVarValue(&sig_struct, transducer, dcct_b,    TRANSDUCER_MEAS_FILTERED));
        case FGC_SPY_BPROBE_FLTR_A:         return (float)(sigVarValue(&sig_struct, transducer, b_probe_a, TRANSDUCER_MEAS_FILTERED));
        case FGC_SPY_BPROBE_FLTR_B:         return (float)(sigVarValue(&sig_struct, transducer, b_probe_b, TRANSDUCER_MEAS_FILTERED));
        case FGC_SPY_V_MEAS_FLTR:           return (float)(sigVarValue(&sig_struct, transducer, v_meas,    TRANSDUCER_MEAS_FILTERED));
        case FGC_SPY_EXT_REF_FLTR:          return (float)(sigVarValue(&sig_struct, transducer, ext_ref,   TRANSDUCER_MEAS_FILTERED));

        //  FIELD SIGNALS

        case FGC_SPY_B_MEAS:                return (float)(regulationGetVarValue(MEAS_B_UNFILTERED));
        case FGC_SPY_B_MEAS_FLTR:           return (float)(regulationGetVarValue(MEAS_B_FILTERED));
        case FGC_SPY_B_MEAS_EXTR:           return (float)(regulationGetVarValue(MEAS_B_EXTRAPOLATED));
        case FGC_SPY_B_MEAS_RATE:           return (float)(regulationGetVarValue(MEAS_B_RATE));
        case FGC_SPY_B_REF_DELAYED:         return (float)(regulationGetVarValue(B_REF_DELAYED));
        case FGC_SPY_B_ERR:                 return (float)(regulationGetVarValue(B_ERR));
        case FGC_SPY_B_MAX_ABS_ERR:         return (float)(regulationGetVarValue(B_MAX_ABS_ERR));
        case FGC_SPY_B_MEAS_SIM:            return (float)(regulationGetVarValue(SIM_B_MEAS));
        case FGC_SPY_B_MEAS_REG:            return (float)(regulationGetVarValue(B_MEAS_REG));
        case FGC_SPY_B_REF_ADV:             return (float)(regulationGetVarValue(B_REF_ADV));
        case FGC_SPY_B_REF_LIMITED:         return (float)(regulationGetVarValue(B_REF_LIMITED));
        case FGC_SPY_B_REF_CLOSED:          return (float)(regulationGetVarValue(B_REF_CLOSED));
        case FGC_SPY_B_REF_OPEN:            return (float)(regulationGetVarValue(B_REF_OPEN));
        case FGC_SPY_B_TRACK_DELAY:         return (float)(regulationGetVarValue(B_TRACK_DELAY_PERIODS));
        case FGC_SPY_B_RMS_ERR:             return (float)(regulationGetVarValue(B_RMS_ERR));

        //  CURRENT SIGNALS

        case FGC_SPY_I_MEAS:                return (float)(regulationGetVarValue(MEAS_I_UNFILTERED));
        case FGC_SPY_I_MEAS_FLTR:           return (float)(regulationGetVarValue(MEAS_I_FILTERED));
        case FGC_SPY_I_MEAS_EXTR:           return (float)(regulationGetVarValue(MEAS_I_EXTRAPOLATED));
        case FGC_SPY_I_MEAS_RATE:           return (float)(regulationGetVarValue(MEAS_I_RATE));
        case FGC_SPY_I_REF_DELAYED:         return (float)(regulationGetVarValue(I_REF_DELAYED));
        case FGC_SPY_I_ERR:                 return (float)(regulationGetVarValue(I_ERR));
        case FGC_SPY_I_MAX_ABS_ERR:         return (float)(regulationGetVarValue(I_MAX_ABS_ERR));
        case FGC_SPY_I_MEAS_SIM:            return (float)(regulationGetVarValue(SIM_I_MEAS));
        case FGC_SPY_I_MAGNET_SIM:          return (float)(regulationGetVarValue(SIM_I_MAGNET));
        case FGC_SPY_I_RMS:                 return (float)(regulationGetVarValue(I_RMS));
        case FGC_SPY_I_RMS_LOAD:            return (float)(regulationGetVarValue(I_RMS_LOAD));
        case FGC_SPY_I_MEAS_REG:            return (float)(regulationGetVarValue(I_MEAS_REG));
        case FGC_SPY_I_REF_ADV:             return (float)(regulationGetVarValue(I_REF_ADV));
        case FGC_SPY_I_REF_LIMITED:         return (float)(regulationGetVarValue(I_REF_LIMITED));
        case FGC_SPY_I_REF_CLOSED:          return (float)(regulationGetVarValue(I_REF_CLOSED));
        case FGC_SPY_I_REF_OPEN:            return (float)(regulationGetVarValue(I_REF_OPEN));
        case FGC_SPY_I_TRACK_DELAY:         return (float)(regulationGetVarValue(I_TRACK_DELAY_PERIODS));
        case FGC_SPY_I_RMS_ERR:             return (float)(regulationGetVarValue(I_RMS_ERR));

        //  LOAD RELATED MEASUREMENT SIGNALS

        case FGC_SPY_SAT_FACTOR:            return (float)(regulationGetVarValue(LOAD_SAT_FACTOR));
        case FGC_SPY_I_MAG_SAT:             return (float)(regulationGetVarValue(LOAD_I_MAG_SAT));
        case FGC_SPY_MEAS_OHMS:             return (float)(regulationGetVarValue(LOAD_MEAS_OHMS));
        case FGC_SPY_MEAS_HENRYS:           return (float)(regulationGetVarValue(LOAD_MEAS_HENRYS));
        case FGC_SPY_POWER:                 return (float)(regulationGetVarValue(MEAS_POWER));

        //  VOLTAGE SIGNALS

        case FGC_SPY_V_REF_REG:             return (float)(regulationGetVarValue(V_REF_REG));
        case FGC_SPY_V_REF_SAT:             return (float)(regulationGetVarValue(V_REF_SAT));
        case FGC_SPY_V_REF_DECO:            return (float)(regulationGetVarValue(V_REF_DECO));
        case FGC_SPY_V_REF:                 return (float)(regulationGetVarValue(V_REF));
        case FGC_SPY_V_REF_VS:              return (float)(regulationGetVarValue(V_REF_VS));
        case FGC_SPY_V_REF_DAC:             return (float)(referenceGetVarValue(REF_DAC));
        case FGC_SPY_V_REF_RATE:            return (float)(regulationGetVarValue(V_REF_RATE));
        case FGC_SPY_V_RATE_RMS:            return (float)(regulationGetVarValue(V_RATE_RMS));
        case FGC_SPY_V_ERR:                 return (float)(regulationGetVarValue(V_ERR));
        case FGC_SPY_V_MAX_ABS_ERR:         return (float)(regulationGetVarValue(V_MAX_ABS_ERR));
        case FGC_SPY_V_MEAS_SIM:            return (float)(regulationGetVarValue(SIM_V_MEAS));

        //  FIRING SIGNALS

        case FGC_SPY_V_MEAS_REG:            return (float)(regulationGetVarValue(VFILTER_V_MEAS_REG));
        case FGC_SPY_V_REG_ERR:             return (float)(regulationGetVarValue(VREG_V_REG_ERR));
        case FGC_SPY_V_INTEGRATOR:          return (float)(regulationGetVarValue(VREG_V_INTEGRATOR));
        case FGC_SPY_D_REF:                 return (float)(regulationGetVarValue(VFILTER_D_REF));
        case FGC_SPY_D_MEAS:                return (float)(regulationGetVarValue(VFILTER_D_MEAS));
        case FGC_SPY_F_REF_REG:             return (float)(regulationGetVarValue(FIRING_F_REF_REG));
        case FGC_SPY_F_REF_LIMITED:         return (float)(regulationGetVarValue(FIRING_F_REF_LIMITED));
        case FGC_SPY_F_REF:                 return (float)(regulationGetVarValue(FIRING_F_REF));
        case FGC_SPY_I_CAPA:                return (float)(regulationGetVarValue(VFILTER_I_CAPA));
        case FGC_SPY_I_CAPA_MEAS:           return (float)(regulationGetVarValue(MEAS_I_CAPA_UNFILTERED));
        case FGC_SPY_I_CAPA_SIM:            return (float)(regulationGetVarValue(SIM_I_CAPA_MEAS));

        //  I_EARTH

        case FGC_SPY_I_EARTH:               return (float)(dpcom.mcu.meas.i_earth[0]);

        //  TEMPERATURE SIGNALS

        case FGC_SPY_T_FGC_IN:              return (float)(dpcom.mcu.temp.in);
        case FGC_SPY_T_FGC_OUT:             return (float)(dpcom.mcu.temp.out);
        case FGC_SPY_T_DCCT_A:              return (float)(dpcom.mcu.temp.dcct[0]);
        case FGC_SPY_T_DCCT_B:              return (float)(dpcom.mcu.temp.dcct[1]);

        //  PLL SIGNALS

        case FGC_SPY_PLL_STATE:             return (float)(dpcom.mcu.pll.state);
        case FGC_SPY_PLL_E:                 return (float)(dpcom.mcu.pll.error               / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_INTE:              return (float)(dpcom.mcu.pll.integrator          / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_FILT_INTE:         return (float)(dpcom.mcu.pll.filtered_integrator / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_DAC:               return (float)(dpcom.mcu.pll.dac);
        case FGC_SPY_PLL_ETH_SYNC:          return (float)(dpcom.mcu.pll.eth_sync_cal        / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_EXT_E:             return (float)(dpcom.mcu.pll.ext_error           / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_E18_E:             return (float)(dpcom.mcu.pll.e18_error           / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_E19_E:             return (float)(dpcom.mcu.pll.e19_error           / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_EXT_A_E:           return (float)(dpcom.mcu.pll.ext_avg_error       / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_NET_A_E:           return (float)(dpcom.mcu.pll.net_avg_error       / (float)PLL_TICKS_TO_SECONDS);

        //  DSP USAGE SIGNALS

        case FGC_SPY_DSPISR:                return (float)(iter.sig_cpu_usage_us);

        // Unknown signal

        default:                            return -1.23456789;
    }
}


// EOF
