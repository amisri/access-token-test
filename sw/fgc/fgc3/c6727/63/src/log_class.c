//! @file  log_class.c
//! @brief Class specific logging


// ---------- Includes

#include <math.h>
#include <string.h>

#include <cclibs.h>
#include <liblog.h>

#include "platforms/fgc3/memmap_dsp.h"

#include "fgc3/c6727/63/inc/log_class.h"
#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/63/inc/signals_class.h"
#include "fgc3/c6727/inc/cycle.h"
#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/spivs.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"



// ---------- Internal structures, unions and enumerations

enum Log_class_read_request
{
    LOG_CLASS_READ_REQUEST_NONE_BIT_MASK = 0x00,
    LOG_CLASS_READ_REQUEST_FCM_BIT_MASK  = 0x01,
    LOG_CLASS_READ_REQUEST_PCM_BIT_MASK  = 0x02,
};


// The variables below are used by logStructsInit() and associated
// to signals in in fgc/def/liblog/ods/class_63.ods

struct Log_class_signals
{
    cc_float   i_earth;            //!< Earth current.
    cc_float   dim_ana[4];         //!< DIM analogue values
    cc_float   dim_ana_offset[4];  //!< DIM analogue values

    struct CC_ns_time time_stamp_temp;
    struct CC_ns_time time_stamp_i_earth;
    struct CC_ns_time time_stamp_bis_limits;

    cc_float   dac_raw;            //!< DAC raw values

    cc_float   i_earth_time_offset;//!< I_EARTH log time offset

    struct Log_class_debug
    {
        cc_float    reg_mode;      //!< Regulation mode
        cc_float    ref_state;     //!< Refereance state
        cc_float    fg_type;       //!< Function type
        cc_float    fg_status;     //!< Function status
        cc_float    ramp_mgr_stat; //!< Ramp manager status
        cc_float    cyc_status;    //!< Cycle status
        cc_float    iter_time_us;  //!< Iteration time in microseconds
        cc_float    iter_index_i;  //!< Iteration index for current regulation
        cc_float    iter_index_b;  //!< Iteration index for field regulation
        cc_float    debug_sig;     //!< General purpose signal

    } debug;
};



// ---------- Internal variable definitions

static struct Log_class_signals  log_class_signals;


#include "inc/classes/63/logStructs.h"
#include "inc/classes/63/logStructsInit.h"



// ---------- Constants

static uint32_t const DIM_IDX_INVALID = 0xFFFFFFFF;



// ---------- External variable definitions

#pragma DATA_SECTION(log_read,     ".ext_data")
#pragma DATA_SECTION(log_buffers,  ".ext_log")

struct LOG_structs        log_structs;
struct LOG_buffers        log_buffers;
struct LOG_read           log_read;



// ---------- Internal function definitions

static void logClassSetAcqDebugSignals(void)
{
    log_class_signals.debug.iter_time_us   =  (cc_float)iter.time_us.us * 1.0E-6;
    log_class_signals.debug.iter_index_i   =  (cc_float)regMgrVarValue(&reg_mgr,IREG_ITER_INDEX);
    log_class_signals.debug.iter_index_b   =  (cc_float)regMgrVarValue(&reg_mgr,BREG_ITER_INDEX);
}



static void logClassSetRegDebugSignals(void)
{
    log_class_signals.debug.reg_mode       =  (cc_float)regulationGetVarValue(REG_MODE)          / 3.0 - 6.0;   // 0 ... 3  ->  -6.0  ... -5.0
    log_class_signals.debug.ref_state      =  (cc_float)referenceGetVarValue(REF_STATE)          * 0.1;         // 0 ... 14 ->   0.0  ...  1.4
    log_class_signals.debug.fg_type        = -(cc_float)referenceGetVarValue(REF_FG_TYPE)        * 0.1;         // 0 ... 13 ->   0.0  ... -1.3
    log_class_signals.debug.fg_status      = -(cc_float)referenceGetVarValue(REF_FG_STATUS)      / 4.0 - 2.0;   // 0 ... 3  ->  -2.0  ... -2.75
    log_class_signals.debug.ramp_mgr_stat  = -(cc_float)referenceGetVarValue(REF_RAMP_MGR)       / 3.0 - 3.0;   // 0 ... 2  ->  -3.0  ... -3.66
    log_class_signals.debug.cyc_status     = -(cc_float)referenceGetVarValue(REF_CYCLING_STATUS) / 3.0 - 4.0;   // 0 ... 2  ->  -4.0  ... -4.66
}



static void logClassContinuousSignals(void)
{
    struct LOG_log * const log = log_structs.log;

    // DEBUG SIG

    log_class_signals.debug.debug_sig = 1.0E-9 * (float)ref_mgr.fg.time.ns;

    // Update DAC raw values

    log_class_signals.dac_raw = (float)property_fgc.analog.dac[0];

    // Log continuous signals at 10 KHz

    logStoreContinuousRT(&log[LOG_ACQ]      , iter.time_ns);
    logStoreContinuousRT(&log[LOG_I_MEAS]   , iter.time_ns);
    logStoreContinuousRT(&log[LOG_V_REF]    , iter.time_ns);
    logStoreContinuousRT(&log[LOG_FLAGS]    , iter.time_ns);
    logStoreContinuousRT(&log[LOG_V_REG]    , iter.time_ns);
    logStoreContinuousRT(&log[LOG_B_MEAS]   , iter.time_ns);
    logStoreContinuousRT(&log[LOG_BIS_FLAGS], iter.time_ns);

    // Prepare REG debug signals on either BREG or IREG logging iterations

    bool const log_breg = regulationGetVarValue(BREG_ITER_INDEX) == 0;
    bool const log_ireg = regulationGetVarValue(IREG_ITER_INDEX) == 0;

    if (log_breg == true || log_ireg == true)
    {
        logClassSetRegDebugSignals();
    }

    // Log field regulation signals at field regulation rate

    if (log_breg == true)
    {
        logStoreContinuousRT(&log[LOG_B_REG], iter.time_ns);
    }

    // Log current regulation signals at current regulation rate

    if (log_ireg == true)
    {
        logStoreContinuousRT(&log[LOG_I_REG], iter.time_ns);
    }

    // Log current reference and measuremetn at 1 KHz for SPS

    if (iter.start_of_ms1_f == true)
    {
        logStoreContinuousRT(&log[LOG_I_1KHZ], iter.time_ns);
    }

    // Trigger BIS LIMITS at 5 Hz

    if (iter.start_of_ms200_f == true)
    {
        log_class_signals.time_stamp_bis_limits = iter.time_ns;
    }

    // Trigger I_EARTH at 50 Hz

    if (iter.start_of_ms20_f == true)
    {
        log_class_signals.time_stamp_i_earth = iter.time_ns;
    }

    // Trigger TEMP at 0.1 Hz

    if (iter.start_of_s10_f == true)
    {
        log_class_signals.time_stamp_temp = iter.time_ns;
    }
}



static void logClassBgpContinuousSignals(void)
{
    // LOG LIMITS

    if (log_class_signals.time_stamp_bis_limits.secs.abs != 0)
    {
        logStoreContinuousRT(&log_structs.log[LOG_BIS_LIMITS], log_class_signals.time_stamp_bis_limits);

        log_class_signals.time_stamp_bis_limits.secs.abs = 0;
    }

    // Log I_EARTH

    if (log_class_signals.time_stamp_i_earth.secs.abs != 0)
    {
        // Log the four I_EARTH values sampled every 5 ms.

        struct CC_ns_time time_stamp = log_class_signals.time_stamp_i_earth;
        uint8_t           idx;

        for (idx = 0; idx < 4; idx++)
        {
            log_class_signals.i_earth = dpcom.mcu.meas.i_earth[idx];

            logStoreContinuousRT(&log_structs.log[LOG_I_EARTH], time_stamp);

            // Increment timestamp by 5ms (5000us)

            time_stamp = cctimeNsAddDelayRT(time_stamp, 5000);
        }

        log_class_signals.time_stamp_i_earth.secs.abs = 0;
    }

    // Log TEMP

    if (log_class_signals.time_stamp_temp.secs.abs != 0)
    {
        logStoreContinuousRT(&log_structs.log[LOG_TEMP], log_class_signals.time_stamp_temp);

        log_class_signals.time_stamp_temp.secs.abs = 0;
    }

    // Store ILC RMS error signals when requested by refIlcStateToCycling

    struct CC_ns_time const ilc_log_time_stamp = referenceGetVarValue(ILC_LOG_TIME_STAMP);

    if(ilc_log_time_stamp.secs.abs != 0)
    {
        logStoreContinuousRT(&log_structs.log[LOG_ILC_CYC], ilc_log_time_stamp);

        // Acknowledge by zeroing the time stamp

        referenceGetVarValue(ILC_LOG_TIME_STAMP).secs.abs = 0;
    }
}



static void logClassDimsSignals(void)
{
    if (dpcom.mcu.dim_ana.dim_idx < FGC_NUM_DIMS_BRANCH)
    {
        uint8_t  i;

        for (i = 0; i < FGC_N_DIM_ANA_CHANS; ++i)
        {
            log_class_signals.dim_ana[i] = dpcom.mcu.dim_ana.values[i];
        }

        logStoreContinuousRT(&log_structs.log[LOG_DIM + dpcom.mcu.dim_ana.dim_idx],
                             cctimeUsToNsRT(dpcom.mcu.dim_ana.time_stamp));

        dpcom.mcu.dim_ana.dim_idx = DIM_IDX_INVALID;
    }
}



// ---------- External function definitions

void logClassInit(void)
{
    log_class_signals.time_stamp_temp.secs.abs       = 0;
    log_class_signals.time_stamp_i_earth.secs.abs    = 0;
    log_class_signals.time_stamp_bis_limits.secs.abs = 0;

    // Initialize liblog structures (log_mgr, log_structs and log_read)

    logStructsInit((struct LOG_mgr *)&dpcom.log.log_mgr, &log_structs, &log_buffers);

    logReadInit(&log_read, &log_structs, &log_buffers);

    dpcom.mcu.dim_ana.dim_idx = DIM_IDX_INVALID;

    // The I_EARTH log values are retrieved from the DIM analog signals, scanned
    // during the previous 20 ms DIM cycles. That is why the values are 20 ms old.

    log_class_signals.i_earth_time_offset = -0.02;
}



void logClassRtp(void)
{
    // Check if a new cycle has started

    if (cycleIsStarted() == true)
    {
        logStoreStartContinuousRT(log_read.log, iter.time_ns, cycleGetCycSelAcq());
    }

    logClassSetAcqDebugSignals();

    logClassContinuousSignals();
    logClassDimsSignals();

    profileRegister(FGC_DSP_RT_PROFIL_LOGGING);
}



void logClassBgp()
{
    regulationIgnoreLogs();

    interlockIgnoreLogs();

    logClassBgpContinuousSignals();
}



void logClassCacheSelectors(void)
{
    logCacheSelectors(&log_structs);
}


// EOF
