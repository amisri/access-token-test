//! @file  bgp_class.c
//! @brief Class specific background processing functions


// ---------- Includes

#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/63/inc/signals_class.h"
#include "fgc3/c6727/inc/control.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/dpcom.h"



// Function memory allocation

#pragma CODE_SECTION(bgpClassProcessCommands, ".ext_code")



// ---------- Platform/class specific function definitions

void bgpClassProcessCommands(enum Dpcom_mcu_commands command)
{
    switch (command)
    {
        case DPCOM_MCU_COMMAND_RESET:
        {
            referenceResetFaults();
            break;
        }

        case DPCOM_MCU_COMMAND_NVS_INIT:
        {
            referenceInitRegMode();
            controlsClassSetActDest();
            break;
        }

        case DPCOM_MCU_COMMAND_DB_INIT:
        {
            referenceInitRegMode();
            break;
        }

        case DPCOM_MCU_COMMAND_UPDATE_REG_PARS:
        {
            regulationUpdatePars();
            break;
        }

        case DPCOM_MCU_COMMAND_ILC_RESET:
        {
            referenceIlcReset();
            break;
        }

        case DPCOM_MCU_COMMAND_REF_DISARM:
        {
            referenceRefDisarm();
            break;
        }
        
        case DPCOM_MCU_COMMAND_ADC_EXT_LIMITS:
        {
            signalClassSetExtAdcLimits();
            break;
        }

        case DPCOM_MCU_COMMAND_NONE:   // Fall through
        default:                       break;
    }
}


// EOF
