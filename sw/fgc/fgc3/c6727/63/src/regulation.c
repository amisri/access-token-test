//! @file regulation.c
//! @brief Interfece to cclibs libreg


// ---------- Includes

#include <libreg.h>

#include "fgc3/c6727/63/inc/log_class.h"
#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/63/inc/signals_class.h"
#include "fgc3/c6727/inc/anaCard.h"
#include "fgc3/c6727/inc/log.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/common.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/pc_state.h"



// ---------- Constants

//! Size for the three FIR filters: FIR_1, FIR_2 and Extrapolated.
#define         REG_FIR_FILTER_SIZE     (FGC_MAX_PERIOD_ITERS * 3)

//! AC period in Europe (20ms) in iterations
#define         REG_AC_PERIOD_ITERS     200

//! DSP iteration period in nanoseconds (100 us) */
uint32_t const  DSP_ITER_PERIOD_US      = 100;

//! ANA101 delay (15us for analogue LPF, 1 iteration for the FPGA)
float    const  MEAS_DELAY_ITERS_ANA101 = 1.15;

//! ANA103 delay (2us for analogue LPF, 50 us for the FPGA acquisition)
float    const  MEAS_DELAY_ITERS_ANA103 = 0.52;

//! ANA104 delay (2us for analogue LPF, 50 us for the FPGA acquisition)
float    const  MEAS_DELAY_ITERS_ANA104 = 0.52;


// ---------- Internal structures, unions and enumerations

//! Internal local variables for the module regulation

struct Regulation
{
    // FIR filtrs

    int32_t   meas_fir_i[REG_FIR_FILTER_SIZE];        //!< Current measurement filter
    int32_t   meas_fir_b[REG_FIR_FILTER_SIZE];        //!< Field measurement filter
};



// ---------- External variable definitions

struct REG_mgr      reg_mgr;
struct REG_pars     reg_pars;


// Static variable definitions

static cc_float v_ac_buf[REG_AC_PERIOD_ITERS];          //!< Buffer for harmonics generator - in fast memory


// ---------- Internal variable definitions

#pragma DATA_SECTION(regulation, ".ext_data")

static struct Regulation regulation;



// Function memory allocation

#pragma CODE_SECTION(regulationInit,        ".ext_code")
#pragma CODE_SECTION(regulationBgp,         ".ext_code")
#pragma CODE_SECTION(regulationUpdatePars,  ".ext_code")



// ---------- External function definitions

void regulationInit(void)
{
    // Reset reg_pars structure

    memset(&reg_mgr,  0, sizeof(struct REG_mgr ));
    memset(&reg_pars, 0, sizeof(struct REG_pars));

    // Support field and current regulation and voltage (true, true, true)

    regMgrInit(&reg_mgr,                                // Reset by regMgrInit to zero
               &reg_pars,
               &dpcls.deco,
               NULL, NULL, NULL,                        // No mutex required on FGC3 since there is only one background thread
               v_ac_buf,
               REG_AC_PERIOD_ITERS,
               CC_ENABLED, CC_ENABLED, CC_ENABLED);     // Allow field, current and voltage regulation in libreg

    // Fix iter period

    regMgrParAppValue(&reg_pars, ITER_PERIOD_NS) = DSP_ITER_PERIOD_US * 1000;

    // Prepare measurement FIR buffers

    regMeasFilterInitBuffer(&reg_mgr.b.meas, regulation.meas_fir_b, REG_FIR_FILTER_SIZE);
    regMeasFilterInitBuffer(&reg_mgr.i.meas, regulation.meas_fir_i, REG_FIR_FILTER_SIZE);

    // Initialise MEAS.V_DELAY_ITERS with a constant parameter value (must be done after call to regMgrInit)

    cc_float           meas_v_delay_iters = 0.0F;
    enum Ana_card_type ana_type           = anaCardGetType();

    if      (ana_type == ANA_CARD_TYPE_101) { meas_v_delay_iters = MEAS_DELAY_ITERS_ANA101; }
    else if (ana_type == ANA_CARD_TYPE_103) { meas_v_delay_iters = MEAS_DELAY_ITERS_ANA103; }
    else if (ana_type == ANA_CARD_TYPE_104) { meas_v_delay_iters = MEAS_DELAY_ITERS_ANA104; }

    regMgrParAppValue(&reg_pars, MEAS_V_DELAY_ITERS) = meas_v_delay_iters;

    // Set default AC period in the parameter - this is not yet a property, but it could be if 60Hz operation needs to be supported

    regMgrParAppValue(&reg_pars, HARMONICS_AC_PERIOD_ITERS) = REG_AC_PERIOD_ITERS;

    // Link libreg to liblog to keep the current and field regulation log periods correct

    regMgrInitRegPeriodPointers(&reg_mgr,
                                (int32_t *)&dpcom.log.log_mgr.period[LOG_B_REG].ns,
                                (int32_t *)&dpcom.log.log_mgr.period[LOG_I_REG].ns);
}



void regulationRtp(void)
{
    // Update V meas - this will not work if the master has no internal V_LOAD measurement and depends on the INTER_FGC V_LOAD from slaves

    struct CC_meas_signal v_meas = sigVarValue(&sig_struct, transducer, v_meas, TRANSDUCER_MEAS_SIGNAL);

    if (   dpcls.inter_fgc_mgr.is_master == true
        && dpcls.inter_fgc_mgr.is_enabled[CONSUMED_SIGS_V_LOAD] == true)
    {
        v_meas.is_valid &= dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_V_LOAD].is_valid;
        v_meas.signal   += dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_V_LOAD].signal;
    }

    regMgrMeasSetVmeasRT(&reg_mgr, v_meas);

    // Update the remaining measurements

    regMgrMeasSetBmeasRT(&reg_mgr, sigVarValue(&sig_struct, select,     b_meas, SELECT_MEAS_SIGNAL));
    regMgrMeasSetImeasRT(&reg_mgr, sigVarValue(&sig_struct, select,     i_meas, SELECT_MEAS_SIGNAL));
    regMgrMeasSetIcapaRT(&reg_mgr, sigVarValue(&sig_struct, transducer, i_capa, TRANSDUCER_MEAS_SIGNAL));
    regMgrMeasSetVacRT  (&reg_mgr, sigVarValue(&sig_struct, transducer, v_ac,   TRANSDUCER_MEAS_SIGNAL));
}



void regulationBgp(void)
{
    // Update regulation error

    dpcls.dsp.reg.i_err_ma = pcStateGet() == FGC_PC_DIRECT
                           ? regMgrVarValue(&reg_mgr, REG_MILLI_RMS_ERR)
                           : regMgrVarValue(&reg_mgr, REG_MILLI_REG_ERR);

    // Update references

    dpcls.dsp.ref.reg_mode  = regMgrVarValue(&reg_mgr, REG_MODE);
    dpcls.dsp.ref.b         = regMgrVarValue(&reg_mgr, B_REF_LIMITED);
    dpcls.dsp.ref.i         = regMgrVarValue(&reg_mgr, I_REF_LIMITED);
    dpcls.dsp.ref.v         = regMgrVarValue(&reg_mgr, V_REF);

    // Update actuation

    dpcls.dsp.ref.actuation = regMgrParValue(&reg_mgr, VS_ACTUATION);

    // Update measurements

    dpcls.dsp.meas.b_rate     = regMgrVarValue(&reg_mgr, MEAS_B_RATE);
    dpcls.dsp.meas.i_rate     = regMgrVarValue(&reg_mgr, MEAS_I_RATE);
    dpcls.dsp.meas.i_zero_f   = regMgrVarValue(&reg_mgr, FLAG_I_MEAS_ZERO);
    dpcls.dsp.meas.i_low_f    = regMgrVarValue(&reg_mgr, FLAG_I_MEAS_LOW);
    dpcls.dsp.meas.i_access_f = (dpcls.dsp.meas.i > property.limits.i_access);

    dpcls.dsp.meas.i_rate = MAX(-999.99, dpcls.dsp.meas.i_rate);
    dpcls.dsp.meas.i_rate = MIN(dpcls.dsp.meas.i_rate, 999.99);

    // Process any pending changes in libreg parameters

    regMgrParsProcess(&reg_mgr);
}



void regulationUpdatePars(void)
{
    // Update any libreg parameters that are not exposed to the user as properties

    regMgrParAppValue(&reg_pars, VS_NUM_VSOURCES) = dpcls.inter_fgc_mgr.num_converters;

    // Update libreg parameters

    regMgrParsCheck(&reg_mgr);

    // Update the load select on the MCU

    dpcls.dsp.load_select = regMgrParValue(&reg_mgr, LOAD_SELECT);

    // Update the load select on the MCU

    dpcls.dsp.limits.i_pos = regMgrVarValue(&reg_mgr,LIMITS_I_POS);

    // Update OASIS signals metadata: LOG.OASIS.*.INTERNVAL_NS (100000 ns = 100 us = 1 iteration)
    // Update the log used for LOG.OASIS.{I,B}_MEAS and LOG.OASIS.{I,B}_REF based on the
    // regulation rate and the value of LOG.OASIS.SUBSAMPLE. If the property value is less
    // than the regualtion rate, use {I,B}_MEAS, otherwise use {I,B}_REG

    dpcls.oasis.v_interval_ns = dpcls.oasis.subsampling * regMgrParValue(&reg_mgr, ITER_PERIOD_NS);

    if (dpcls.oasis.subsampling < regMgrVarValue(&reg_mgr, IREG_PERIOD_ITERS))
    {
        dpcls.dsp.meas.oasis_subsample_i_reg = 0;
        dpcls.oasis.i_interval_ns            = 100000 * dpcls.oasis.subsampling;
    }
    else
    {
        dpcls.dsp.meas.oasis_subsample_i_reg  = dpcls.oasis.subsampling / regMgrVarValue(&reg_mgr, IREG_PERIOD_ITERS);
        dpcls.oasis.i_interval_ns             = regMgrVarValue(&reg_mgr, IREG_PERIOD_NS) * dpcls.dsp.meas.oasis_subsample_i_reg;
    }

    if (dpcls.oasis.subsampling < regMgrVarValue(&reg_mgr, BREG_PERIOD_ITERS))
    {
        dpcls.dsp.meas.oasis_subsample_b_reg = 0;
        dpcls.oasis.b_interval_ns            = 100000 * dpcls.oasis.subsampling;
    }
    else
    {
        dpcls.dsp.meas.oasis_subsample_b_reg  = dpcls.oasis.subsampling / regMgrVarValue(&reg_mgr, BREG_PERIOD_ITERS);
        dpcls.oasis.b_interval_ns             = regMgrVarValue(&reg_mgr, BREG_PERIOD_ITERS) * dpcls.dsp.meas.oasis_subsample_b_reg;
    }
}



void regulationIgnoreLogs(void)
{
    // Check if logs are to be ignored

    (regMgrParValue(&reg_mgr, VS_ACTUATION) != REG_FIRING_REF) ? logDisableLog(DPCOM_LOG_IGNORE_V_REG_BIT_MASK)
                                                               : logEnableLog (DPCOM_LOG_IGNORE_V_REG_BIT_MASK);

    // If the regulation is at 10 KHz, disable the V_REF log to prevent overrunning the real-time task

    bool const ten_kHz_regulation = regMgrParValue(&reg_mgr,IREG_PERIOD_ITERS) == 1;

    // If the regulation is at 10 KHz and the BIS is present, disable I_MEAS log to prevent
    // interlockRtp() overrun the real-time task

    (   testBitmap(dpcom.mcu.log.ignore_mask, DPCOM_LOG_IGNORE_BIS_BIT_MASK) == false
     && ten_kHz_regulation == true) ? logDisableLog(DPCOM_LOG_IGNORE_I_MEAS_BIT_MASK)
                                    : logEnableLog (DPCOM_LOG_IGNORE_I_MEAS_BIT_MASK);

    (regMgrVarValue(&reg_mgr, FLAG_B_MEAS_LIM_ENABLED) == false) ? logDisableLog(DPCOM_LOG_IGNORE_B_BIT_MASK)
                                                                    : logEnableLog (DPCOM_LOG_IGNORE_B_BIT_MASK);
}



struct REG_mgr * regulationGetHandler(void)
{
    return(&reg_mgr);
}


// EOF
