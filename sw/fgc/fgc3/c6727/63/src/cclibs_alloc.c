//! @file cclibs_alloc.c
//! @brief Allocates cclibs background functions in external memory


#include <lib/cclibs/libfg/ti/tms320c6727/inc/libfg_alloc.c>
#include <lib/cclibs/libref/ti/tms320c6727/inc/libref_alloc.c>
#include <lib/cclibs/libreg/ti/tms320c6727/inc/libreg_alloc.c>
#include <lib/cclibs/libsig/ti/tms320c6727/inc/libsig_alloc.c>
#include <lib/cclibs/libintlk/ti/tms320c6727/inc/libintlk_alloc.c>
#include <lib/cclibs/libpolswitch/ti/tms320c6727/inc/libpolswitch_alloc.c>


// EOF
