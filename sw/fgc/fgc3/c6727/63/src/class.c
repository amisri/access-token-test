//! @file  class.c
//! @brief This file contains all class related definitions


// Instantiate variables in headers with no associated source files

#define DEFPROPS
#define PC_STATE_GLOBALS


// ---------- Includes

#include "fgc3/c6727/63/inc/log_class.h"
#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/inc/dsp.h"
#include "fgc3/c6727/inc/log.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/pc_state.h"
#include "fgc3/inc/status.h"

#pragma DATA_SECTION(prop, ".ext_data")

#include <deftypes.h>
#include <defprops_dsp_fgc.h>
#include <defconst_assert.h>



// ---------- External variable definitions

#pragma DATA_SECTION(dpcom, ".dpram_area")
#pragma DATA_SECTION(dpcls, ".dpram_area")

volatile struct Dpcom dpcom;
volatile struct Dpcls dpcls;


// EOF
