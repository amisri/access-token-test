//! @file  cycle_class.c
//! @brief Class specific cycle functions


// ---------- Includes

#include <stdint.h>

#include <defprops_dsp_fgc.h>

#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/inc/property.h"



// ---------- Platform/class specific function definitions

uint32_t cycleClassGetStatus(uint8_t sub_sel, uint8_t cyc_sel)
{
    uint32_t * cyc_status;

    referenceGetPointer((uint8_t*)PROP_REF_CYC_STATUS.value,
                        sub_sel,
                        cyc_sel,
                        PROPS_PPM_SUB_DEV_STEP,
                        PROPS_PPM_CYCLE_STEP,
                        (uint8_t **)&cyc_status);

    return (*cyc_status);
}



uint32_t cycleClassGetFuncType(uint8_t sub_sel, uint8_t cyc_sel)
{
    return (refMgrFgParValueSubCyc(&ref_mgr, sub_sel, cyc_sel, REF_FG_TYPE));
}


// EOF
