//! @file refernce.c
//! @brief Interfece to cclibs libref

#define REFERENCE_FRIEND


// ---------- Includes

#include <libref.h>
#include <libref/refParsLinkStructs.h>

#include <defprops_dsp_fgc.h>

#include "inc/fgc_errs.h"

#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/63/inc/reference.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/63/inc/signals_class.h"
#include "fgc3/c6727/inc/cycle.h"
#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/dpcmd.h"
#include "fgc3/inc/pc_state.h"
#include "fgc3/inc/status.h"

#undef REFERENCE_FRIEND



// ---------- Constants

#define FGC_EVENT_TIME_INVALID    0xFFFFFFFF



// ---------- Internal structures, unions and enumerations

//! Internal local variables for the module reference in slow RAM

struct Reference
{
    // Cycling status

    struct FG_error          fg_error  [FGC_MAX_USER_PLUS_1 * FGC_MAX_SUB_DEVS];           //!< Function errors (multi-PPM)
    struct REF_armed         ref_armed [FGC_MAX_USER_PLUS_1 * FGC_MAX_SUB_DEVS];           //!< Armed functions (multi-PPM)
    struct REF_cyc_status    cyc_status[FGC_MAX_USER_PLUS_1 * FGC_MAX_SUB_DEVS];           //!< Cycle status    (multi-PPM)

    // TABLE function

    struct FG_point          armed_table_function  [REF_ARMED_TABLE_FUNCTION_LEN(FGC_MAX_USER_PLUS_1, 1, FGC_TABLE_LEN)];  //!< Armed Table functions
    struct FG_point          running_table_function[REF_RUNNING_TABLE_FUNCTION_LEN(FGC_TABLE_LEN)];                      //!< Buffered running Table functions

    // ILC cycle data store

   struct REF_ilc_cyc_data   ilc_cyc_data_store[REF_ILC_CYC_DATA_STORE_NUM_ELS(FGC_ILC_MAX_SAMPLES,FGC_MAX_USER)];

    // Acquisition

    bool                     acquisition_event;                                             //!< Acquisition event defined the acquisition time
};



// ---------- External variable definitions

struct REF_mgr ref_mgr;



// ---------- Internal variable definitions

#pragma DATA_SECTION(reference, ".ext_data")

static struct Reference reference;



// Function memory allocation

#pragma CODE_SECTION(referenceInit,          ".ext_code")
#pragma CODE_SECTION(referenceBgp,           ".ext_code")
#pragma CODE_SECTION(referenceArmFunction,   ".ext_code")
#pragma CODE_SECTION(referenceProcessEvents, ".ext_code")
#pragma CODE_SECTION(referenceUpdateState,   ".ext_code")



// ---------- Internal function declarations

//! Attempts to arm a function.
//!
//! @param[in]   sub_sel         Sub-device selector.
//! @param[in]   cyc_sel         Cycle selector.
//! @param[in]   arm_type        Function arm type: test, commit, restore.
//!
//! @retval      FGC_OK_NO_RSP   The function was correctly armed.
//! @retval      FGC_*           Error when arming the function.

static uint16_t referenceArmFunction(uint32_t                 sub_sel,
                                     uint32_t                 cyc_sel,
                                     enum Dpcls_func_arm_type arm_type);

//! This function is called to process timing events.

static void referenceProcessEvents(void);

//! Updates the libRef reference state with the PC state

static void referenceUpdateState(void);



// ---------- External function definitions

bool referenceInit(void)
{
    memset(&ref_mgr,     0, sizeof(ref_mgr  ));
    memset(&reference,   0, sizeof(reference));

    reference.acquisition_event = false;

    // Initialize the Interactive Learning Controller (ILC)

    refMgrParValue(&ref_mgr, ILC_L_FUNC_NUM_ELS) = &PROP_REF_ILC_FUNCTION.n_elements;
    refMgrParValue(&ref_mgr, ILC_Q_FUNC_NUM_ELS) = &PROP_REF_ILC_Q_FUNCTION.n_elements;

    // Initialize multi-PPM and variable array length parameters. These cannot use the refMgrInitXxxxPars() helper functions

    // TRANSACTION

    refMgrFgParInitPointer   (&ref_mgr, transaction_last_fg_par_index, (enum REF_fg_par_idx *)dpcls.mcu.transaction[0].last_param);
    refMgrFgParInitSubSelStep(&ref_mgr, transaction_last_fg_par_index, sizeof(dpcls.mcu.transaction[0].last_param[0]));
    refMgrFgParInitCycSelStep(&ref_mgr, transaction_last_fg_par_index, sizeof(dpcls.mcu.transaction[0]));

    // CTRL

    refMgrFgParInitPointer   (&ref_mgr, ctrl_play             , property.ppm[0].ctrl.play);
    refMgrFgParInitSubSelStep(&ref_mgr, ctrl_play             , sizeof(property.ppm[0].ctrl.play[0]));
    refMgrFgParInitCycSelStep(&ref_mgr, ctrl_play             , sizeof(property.ppm[0]));

    refMgrFgParInitPointer   (&ref_mgr, ctrl_dyn_eco_end_time , property.ppm[0].ctrl.dyn_eco_end_time);
    refMgrFgParInitSubSelStep(&ref_mgr, ctrl_dyn_eco_end_time , sizeof(property.ppm[0].ctrl.dyn_eco_end_time[0]));
    refMgrFgParInitCycSelStep(&ref_mgr, ctrl_dyn_eco_end_time , sizeof(property.ppm[0]));

    // REF

    refMgrFgParInitPointer   (&ref_mgr, ref_fg_type           , property.ppm[0].ref.fg_type);
    refMgrFgParInitSubSelStep(&ref_mgr, ref_fg_type           , sizeof(property.ppm[0].ref.fg_type[0]));
    refMgrFgParInitCycSelStep(&ref_mgr, ref_fg_type           , sizeof(property.ppm[0]));

    // PULSE

    refMgrFgParInitPointer   (&ref_mgr, pulse_ref             , property.ppm[0].fg.pulse.ref);
    refMgrFgParInitSubSelStep(&ref_mgr, pulse_ref             , sizeof(property.ppm[0].fg.pulse.ref[0]));
    refMgrFgParInitCycSelStep(&ref_mgr, pulse_ref             , sizeof(property.ppm[0]));

    refMgrFgParInitPointer   (&ref_mgr, pulse_duration        , property.ppm[0].fg.pulse.duration);
    refMgrFgParInitSubSelStep(&ref_mgr, pulse_duration        , sizeof(property.ppm[0].fg.pulse.duration[0]));
    refMgrFgParInitCycSelStep(&ref_mgr, pulse_duration        , sizeof(property.ppm[0]));

    // TABLE

    refMgrFgParInitPointer   (&ref_mgr, table_function        , property.ppm[0].fg.table.function);
    refMgrFgParInitCycSelStep(&ref_mgr, table_function        , sizeof(property.ppm[0]));

    refMgrFgParInitPointer   (&ref_mgr, table_function_num_els, &property.ppm[0].fg.table.function_num_elements);
    refMgrFgParInitCycSelStep(&ref_mgr, table_function_num_els, sizeof(property.ppm[0]));

    // Use helper functions for non-multi-PPM, fixed array length references: RAMP, PLEP, PPPL, CUBEXP, TRIM, TEST and PRBS

    refMgrInitRampPars  (&ref_mgr, &property.ppm[0].fg.ramp,   0, sizeof(property.ppm[0]));
    refMgrInitPlepPars  (&ref_mgr, &property.ppm[0].fg.plep,   0, sizeof(property.ppm[0]));
    refMgrInitPpplPars  (&ref_mgr, &property.ppm[0].fg.pppl,   0, sizeof(property.ppm[0]));
    refMgrInitCubexpPars(&ref_mgr, &property.ppm[0].fg.cubexp, 0, sizeof(property.ppm[0]));
    refMgrInitTrimPars  (&ref_mgr, &property.ppm[0].fg.trim,   0, sizeof(property.ppm[0]));
    refMgrInitTestPars  (&ref_mgr, &property.ppm[0].fg.test,   0, sizeof(property.ppm[0]));
    refMgrInitPrbsPars  (&ref_mgr, &property.ppm[0].fg.prbs,   0, sizeof(property.ppm[0]));

    // Initialize certain parameters with default values

    referenceSetParValue(MODE_USE_ARM_NOW , CC_DISABLED);
    referenceSetParValue(RT_NUM_STEPS, 0);

    // Initialize the ref manager structure

    int32_t retval = refMgrInit(&ref_mgr,
                                (struct POLSWITCH_mgr *)&dpcls.polswitch_mgr,
                                regulationGetHandler(),
                                NULL,
                                reference.fg_error,
                                reference.cyc_status,
                                reference.ref_armed,
                                reference.armed_table_function,
                                reference.running_table_function,
                                reference.ilc_cyc_data_store,
                                FGC_MAX_USER_PLUS_1,
                                FGC_TABLE_LEN,
                                FGC_ILC_MAX_SAMPLES,
                                4);                     // ref_to_tc_limit

    return (retval == 0);
}



void referenceRegulationRtp(void)
{
    // Part 1

    // Update the real-time reference based on MODE.RT

    switch (property.ref.mode.rt_ref)
    {
        case FGC_MODE_RT_GATEWAY:
        {
            referenceSetParValue(RT_REF, dpcom.mcu.ref.rt);
            break;
        }

        case FGC_MODE_RT_EXT_REF:
        {
            referenceSetParValue(RT_REF, signalsClassExtRefGetValue());
            break;
        }

        case FGC_MODE_RT_INTER_FGC:
        {
            referenceSetParValue(RT_REF, (  dpcls.inter_fgc_mgr.is_ref_slave == true
                                          ? dpcls.inter_fgc_mgr.ext_ref
                                          : 0.0F));
            break;
        }

        default:
        {
            referenceSetParValue(RT_REF, 0.0F);
            break;
        }
    }

    // Update the voltage reference feed forward signal when a new inter_fgc packet is received

    static uint32_t recv_time_us;

    if (dpcls.inter_fgc_mgr.recv_time_us != recv_time_us)
    {
        recv_time_us = dpcls.inter_fgc_mgr.recv_time_us;

        struct CC_meas_signal v_ff;

        switch (property.ref.feed_fwd.signal)
        {
            case FGC_FEED_FWD_SIGNALS_B_DOT:

                v_ff.signal   = dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_B_DOT].signal;
                v_ff.is_valid = dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_B_DOT].is_valid;
                break;

            case FGC_FEED_FWD_SIGNALS_V_REF:

                v_ff.signal   = dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_V_REF].signal;
                v_ff.is_valid = dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_V_REF].is_valid;
                break;

            case FGC_FEED_FWD_SIGNALS_V_LOAD:

                v_ff.signal   = dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_V_LOAD].signal;
                v_ff.is_valid = dpcls.inter_fgc_mgr.consumed_sigs[CONSUMED_SIGS_V_LOAD].is_valid;
                break;

            default:

                v_ff.signal   = 0.0F;
                v_ff.is_valid = false;
                break;
        }

        // The Inter-FGC communication is fixed at 1kHz, so it is normally the case the regulation
        // period of the source of the v_ff signal will be 1ms. Since the signal will be sent in the
        // middle of the millisecond, it will come from the start of that millisecond. So we adjust
        // the iteration time back to the start of the millisecond.

        struct CC_ns_time const start_of_ms_time = { { iter.time_ns.secs.abs }, (iter.time_ns.ns / 1000000) * 1000000 };

        refMgrSetVfeedfwdRT(&ref_mgr, v_ff, start_of_ms_time);
    }

    // Run reference generation and regulation

    refRtRegulationRT(&ref_mgr, iter.time_ns);
}



void referenceStateRtp(void)
{
    // Update state machine if the reference state machine will run on this iteration

    if(refMgrVarValue(&ref_mgr, FLAG_REG_COMPLETE_ITER))
    {
        referenceUpdateState();
    }

    refRtStateRT(&ref_mgr);

    // If previous iteration was the start of cycle then update the maximum reference for the armed cycle for the MCU RTD

    if (cycleWasStarted() == true)
    {
        float * max_value;
        float * min_value;

        referenceGetPointer((uint8_t *)&ref_mgr.REF_VAR_REFARMED_MAX_REF,
                            cycleGetSubSelRef(),
                            cycleGetCycSelRef(),
                            PROPS_PPM_SUB_DEV_STEP,
                            PROPS_PPM_CYCLE_STEP,
                            (uint8_t **)&max_value);

        referenceGetPointer((uint8_t *)&ref_mgr.REF_VAR_REFARMED_MIN_REF,
                            cycleGetSubSelRef(),
                            cycleGetCycSelRef(),
                            PROPS_PPM_SUB_DEV_STEP,
                            PROPS_PPM_CYCLE_STEP,
                            (uint8_t **)&min_value);

        dpcls.dsp.ref.peak = fabs(*max_value) > fabs(*min_value) ? *max_value: *min_value;
    }

    // Update MEAS.ACQ_VALUE.

    uint32_t const cyc_sel_acq = cycleGetCycSelAcq();
    uint32_t const cyc_sel_ref = cycleGetCycSelRef();
    bool           latch_meas;

    // Latch acquisitions

    float const * meas_delay = property.ppm[cyc_sel_ref].meas.acq_delay;
    float       * meas_acq   = property.ppm[cyc_sel_acq].meas.acq_value;

    if (meas_delay[0] > 0.0)
    {
        // Relative time defined by MEAS.TRIG.DELAY_MS

        latch_meas = (cycleGetTimeMs() >= meas_delay[0]);

        if (refMgrLatchUnfltrMeasRT(&ref_mgr, &meas_acq[0], 0, latch_meas) == true)
        {
            dpcls.dsp.meas.pub_acq1_cyc_sel = cyc_sel_acq;

            interlockLatchStatus();
        }

        // Only consider MEAS.TRIG.DELAY2_MS if MEAS.TRIG_DELAY_MS is non zero

        if (meas_delay[1] > 0.0)
        {
            // Relative time defined by MEAS.TRIG.DELAY2_MS

            latch_meas = (cycleGetTimeMs() >= meas_delay[1]);

            if (refMgrLatchUnfltrMeasRT(&ref_mgr, &meas_acq[1], 1, latch_meas) == true)
            {
                dpcls.dsp.meas.pub_acq2_cyc_sel = cyc_sel_acq;

                interlockLatchStatus();
            }
        }
    }
    else
    {
        // Acquisition based on the fieldbus event FGC_EVT_ACQUISITION

        if (reference.acquisition_event == true)
        {
            latch_meas = timeUsGreaterThanEqual(&iter.time_us, (struct CC_us_time *)&dpcls.mcu.event.acquisition_event_time_us);

            // Update MEAS.TRIG_DELAY_MS. If more than one event is received within a cycle, the property
            // is overwritten
            // For every fielbus event, update INTERLOCK.STATUS, INTERLOCK.{V,I}.MEASN and INTERLOCK.PPM.{V,I}.MEAS.
            // These proeprties are arrays of FGC_MAX_INTERLOCK_LATCH elements

            if (refMgrLatchUnfltrMeasRT(&ref_mgr, &meas_acq[0], 0, latch_meas) == true)
            {
                dpcls.dsp.meas.pub_acq1_cyc_sel = dpcls.mcu.event.acquisition_event_cyc_sel;

                interlockLatchStatus();

                reference.acquisition_event = false;
            }
        }
    }
}



void referenceBgp(void)
{
    // Check if the MCU has requested to arm a new function

    if (dpcls.mcu.ref.func.arm_type != DPCLS_FUNC_ARM_TYPE_NONE)
    {
        volatile struct Dpcls_mcu_ref_func * ref_func = &dpcls.mcu.ref.func;

        if (ref_func->arm_type == DPCLS_FUNC_ARM_TYPE_ARM)
        {
            *referenceFgGetParPointerSubCyc(ref_func->sub_sel, ref_func->cyc_sel, REF_FG_TYPE) = (enum FG_type)ref_func->type;
        }

        dpcom.dsp.errnum = referenceArmFunction(ref_func->sub_sel,
                                                ref_func->cyc_sel,
                                                (enum Dpcls_func_arm_type)ref_func->arm_type);

        // Rerort to the MCU that the function has been armed

        ref_func->arm_type = DPCLS_FUNC_ARM_TYPE_NONE;
        ref_func->type     = FGC_FG_TYPE_NONE;
    }

    // Run the ILC state machine

    refIlcState(&ref_mgr);

    // Manage the ILC_CYC log enabled/disabled state based on MODE.ILC property

    updateBitmap(dpcom.mcu.log.ignore_mask, DPCOM_LOG_IGNORE_ILC_CYC_BIT_MASK, refMgrParValue(&ref_mgr, MODE_ILC) == CC_DISABLED);

    // Manage the V_AC_HZ log menu enabled/disabled state based on V_AC signal validity

    updateBitmap(dpcom.mcu.log.ignore_mask, DPCOM_LOG_IGNORE_V_AC_HZ_BIT_MASK, regulationGetVarValue(V_AC_HZ_VALID) == false);

    // Update MODE_REF_CYC_SEL from DEVICE.PPM and MODE_REF_SUB_SEL from DEVICE.MULTI_PPM

    referenceSetParValue(MODE_REF_CYC_SEL, (enum CC_enabled_disabled)dpcom.mcu.device_ppm);
    referenceSetParValue(MODE_REF_SUB_SEL, (enum CC_enabled_disabled)dpcom.mcu.device_multi_ppm);

    // Update REF.DEFAULTS.OP.B.ACCELERATION and REF.DEFAULTS.B.MIN_RMS

    property.ref.defaults_op.b.acceleration = refMgrParValueByIndex(&ref_mgr, DEFAULT_B_ACCELERATION, dpcls.dsp.load_select);
    property.ref.defaults_op.b.min_rms      = refMgrParValueByIndex(&ref_mgr, DEFAULT_B_MINRMS      , dpcls.dsp.load_select);


    // Update mode RT used by CCLIBS which is ENABLED/DISABLED, whilst MODE.RT specifies the source

    referenceSetParValue(MODE_RT_REF, (  property.ref.mode.rt_ref == FGC_MODE_RT_DISABLED
                                       ? CC_DISABLED
                                       : CC_ENABLED));

    // Update STATUS.WARNINGS

    uint32_t warnings = (uint32_t)refMgrVarValue(&ref_mgr, REF_WARNINGS);

    statusEvaluateWarnings(FGC_WRN_REG_ERROR,    testBitmap(warnings,  REF_B_ERR_WARNING_BIT_MASK
                                                                     | REF_I_ERR_WARNING_BIT_MASK
                                                                     | REF_B_RST_WARNING_BIT_MASK
                                                                     | REF_I_RST_WARNING_BIT_MASK
                                                                     | REF_ILC_WARNING_BIT_MASK));

    statusEvaluateWarnings(FGC_WRN_I_RMS_LIM,    testBitmap(warnings,  REF_I_RMS_WARNING_BIT_MASK
                                                                     | REF_I_RMS_LOAD_WARNING_BIT_MASK));

    statusEvaluateWarnings(FGC_WRN_REF_LIM,      testBitmap(warnings,  REF_REF_CLIPPED_BIT_MASK));
    statusEvaluateWarnings(FGC_WRN_REF_RATE_LIM, testBitmap(warnings,  REF_REF_RATE_CLIPPED_BIT_MASK));
    statusEvaluateWarnings(FGC_WRN_V_ERROR,      testBitmap(warnings,  REF_V_ERR_WARNING_BIT_MASK));

    // Update STATUS.REG_WARNINGS

    dpcls.dsp.status.reg_warnings = warnings;

    // Update STATUS.FAULTS based on libreg faults in off

    uint32_t faults = refMgrVarValue(&ref_mgr, REF_FAULTS_IN_OFF);

    statusEvaluateFaults(FGC_FLT_MEAS,       testBitmap(faults,  REF_B_MEAS_FAULT_BIT_MASK
                                                               | REF_I_MEAS_FAULT_BIT_MASK
                                                               | REF_V_MEAS_FAULT_BIT_MASK
                                                               | REF_I_CAPA_FAULT_BIT_MASK));

    statusEvaluateFaults(FGC_FLT_REG_ERROR,  testBitmap(faults,  REF_B_ERR_FAULT_BIT_MASK
                                                               | REF_I_ERR_FAULT_BIT_MASK
                                                               | REF_B_RST_FAULT_BIT_MASK
                                                               | REF_I_RST_FAULT_BIT_MASK));

    statusEvaluateFaults(FGC_FLT_LIMITS, (   dpcom.mcu.cal.active == FGC_CTRL_DISABLED
                                          && testBitmap(faults,  REF_B_MEAS_TRIP_BIT_MASK
                                                               | REF_I_MEAS_TRIP_BIT_MASK
                                                               | REF_V_MEAS_TRIP_BIT_MASK)));

    statusEvaluateFaults(FGC_FLT_I_RMS_LIM,  testBitmap(faults,  REF_I_RMS_FAULT_BIT_MASK
                                                               | REF_I_RMS_LOAD_FAULT_BIT_MASK));

    statusEvaluateFaults(FGC_FLT_V_ERROR,    testBitmap(faults,  REF_V_ERR_FAULT_BIT_MASK
                                                               | REF_V_RATE_RMS_FAULT_BIT_MASK));

    // Update STATUS.REG_FAULTS to use the latched libreg faults, which can appear during a SLOW_ABORT and can be cleared

    dpcls.dsp.status.reg_faults = refMgrVarValue(&ref_mgr, REF_FAULTS);

    // Update STATUS.ST_UNLATCHED

    statusEvaluateUnlatched(FGC_UNL_I_MEAS_DIFF,   signalsClassGetIMeasDiffWarning());
    statusEvaluateUnlatched(FGC_UNL_REF_RT_ACTIVE, (property.ref.mode.rt_ref == CC_ENABLED));

    // Update STATE.DIRECT

    dpcls.dsp.state.direct = (uint32_t)refMgrVarValue(&ref_mgr, REF_DIRECT_STATE);

    // Update the function type and pre_func status

    dpcls.dsp.ref.func_type          = refMgrVarValue(&ref_mgr, REF_FG_TYPE);
    dpcls.dsp.ref.fg_status_pre_func = refMgrVarValue(&ref_mgr, REF_FG_STATUS) == FG_PRE_FUNC;

    // Update the MCU with LOG.MENU.SLOW_ABORT_TIME and faults that are not in MODE.SLOW_ABORT_FAULTS

    dpcom.dsp.log.slow_abort_time   = refMgrVarValue(&ref_mgr, REF_TO_OFF_TIME);
    dpcom.dsp.log.slow_abort_faults = ~refMgrParValue(&ref_mgr, MODE_TO_OFF_FAULTS) & refMgrVarValue(&ref_mgr, REF_FAULTS);

    // Process events to start a function

    referenceProcessEvents();
}



void referenceResetFaults(void)
{
    refMgrResetFaultsRT(&ref_mgr);
}



void referenceIlcReset(void)
{
    refIlcReset(&ref_mgr);
}



void referenceRefDisarm(void)
{
    // Disarm the function only if the start event has not yet been received.

    if (dpcls.mcu.event.func_run_start_time_us.secs.abs == 0)
    {
        property.ppm[0].ref.fg_type[0] = FG_NONE;

        refArmDisarm(&ref_mgr);
    }
}



void referenceInitRegMode(void)
{
    // Initialize the non-config REG.MODE with the config REG.MODE_CYC

    referenceSetParValue(MODE_REG_MODE, referenceGetParValue(MODE_REG_MODE_CYC));
}



void referenceGetPointer(uint8_t        * const base_address,
                         uint32_t const         sub_sel,
                         uint32_t const         cyc_sel,
                         uint32_t const         sub_sel_step,
                         uint32_t const         cyc_sel_step,
                         uint8_t             ** var_pointer)
{
    refMgrVarPointerFunc(&ref_mgr, base_address, sub_sel, cyc_sel, sub_sel_step, cyc_sel_step, var_pointer);
}



// ---------- Internal function definitions

static uint16_t referenceArmFunction(uint32_t                 const sub_sel,
                                     uint32_t                 const cyc_sel,
                                     enum Dpcls_func_arm_type const arm_type)
{
    uint16_t errnum;

    if (   arm_type == DPCLS_FUNC_ARM_TYPE_ARM
        || arm_type == DPCLS_FUNC_ARM_TYPE_TEST
        || arm_type == DPCLS_FUNC_ARM_TYPE_COMMIT)
    {
        enum FG_errno fg_errno = refArm(&ref_mgr,
                                        sub_sel,
                                        cyc_sel,
                                        arm_type == DPCLS_FUNC_ARM_TYPE_TEST ? CC_ENABLED : CC_DISABLED,
                                        dpcls.mcu.ref.func.num_pars,
                                        (cc_float *)dpcls.mcu.ref.func.pars,
                                        (enum REF_fg_par_idx *)dpcls.dsp.ref.fg_pars_group);

        // Map libfg errors to FGC errors

        switch(fg_errno)
        {
            case FG_OK:                   errnum = FGC_OK_NO_RSP;             break;
            case FG_BAD_ARRAY_LEN:        errnum = FGC_BAD_ARRAY_LEN;         break;
            case FG_BAD_PARAMETER:        errnum = FGC_BAD_PARAMETER;         break;
            case FG_INVALID_TIME:         errnum = FGC_INVALID_TIME;          break;
            case FG_OUT_OF_LIMITS:        errnum = FGC_OUT_OF_LIMITS;         break;
            case FG_OUT_OF_RATE_LIMITS:   errnum = FGC_OUT_OF_RATE_LIMITS;    break;
            case FG_INIT_REF_MISMATCH:    errnum = FGC_REF_MISMATCH;          break;
            case FG_INVALID_REG_MODE:     errnum = FGC_BAD_REG_MODE;          break;
            case FG_INVALID_REF_STATE:    errnum = FGC_BAD_STATE;             break;
            case FG_INVALID_SUB_SEL:      // Fall through
            case FG_INVALID_CYC_SEL:      errnum = FGC_BAD_CYCLE_SELECTOR;    break;
            default:                      errnum = FGC_NOT_IMPL;              break;
        }

        // Restore properties when arming via REF.FUNC.TYPE

        if (arm_type == DPCLS_FUNC_ARM_TYPE_ARM && errnum != FGC_OK_NO_RSP)
        {
            refRestore(&ref_mgr,
                    sub_sel,
                    cyc_sel,
                    (enum REF_fg_par_idx *)dpcls.dsp.ref.fg_pars_group);
        }
    }
    else if (arm_type == DPCLS_FUNC_ARM_TYPE_RESTORE)
    {
        refRestore(&ref_mgr,
                   sub_sel,
                   cyc_sel,
                   (enum REF_fg_par_idx *)dpcls.dsp.ref.fg_pars_group);

        errnum = FGC_OK_NO_RSP;
    }
    else
    {
        errnum = FGC_NOT_IMPL;
    }

    dpcls.mcu.ref.func.num_pars = 0;

    return errnum;
}



static void referenceProcessEvents(void)
{
    // ARMED      : Timing event FGC_EVT_START
    //              REF NOW,ref
    // RUNNING    : Timing event FGC_EVT_ABORT
    // TO_CYCLING
    // CYLING     : Timing event FGC_EVT_CYCLE_START, FGC_EVT_START_REF_#, FGC_EVT_PAUSE
    // PAUSE      : Timing event FGC_EVT_RESUME

    uint32_t events_received;

    if (dpcmdGet(&dpcls.mcu.event.received, &events_received) == true)
    {
        // RESUME event

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_RESUME) == true)
        {
            refEventUnpause(&ref_mgr, (struct CC_us_time *)&dpcls.mcu.event.resume_event_time_us);
        }

        // INJECTION, EXTRACTION and CYCLE START events

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_REF) == true)
        {
            // Check ECONOMY FULL_ONCE

            refEventStartFunc(&ref_mgr,
                              (struct CC_us_time *)&dpcls.mcu.event.start_event_time_us,
                              dpcls.mcu.event.sub_sel,
                              dpcls.mcu.event.cyc_sel,
                              testBitmap(events_received, DPCLS_EVT_RECEIVED_ECONOMY_FULL_ONCE));
        }

        // PAUSE event

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_PAUSE) == true)
        {
            refEventPause(&ref_mgr, (struct CC_us_time *)&dpcls.mcu.event.pause_event_time_us);
        }

        // Handle RUN event in ARMED state (including REF NOW)

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_START) == true)
        {
            // If time is set in func_run_start_time_us then use it - this will be used for START events

            if(dpcls.mcu.event.func_run_start_time_us.secs.abs != 0)
            {
                refMgrParValue(&ref_mgr, REF_RUN) = dpcls.mcu.event.func_run_start_time_us;

                dpcls.mcu.event.func_run_start_time_us.secs.abs = 0;
            }

            // Call libref to start the function at time defined by REF_RUN (linked to REF.RUN property)

            refEventRun(&ref_mgr);
        }

        // ABORT event

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_ABORT) == true)
        {
            refEventAbort(&ref_mgr);
        }

        // ECONOMY FULL ENABLE

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_ECONOMY_FULL_ENABLE) == true)
        {
            referenceSetParValue(ECONOMY_FULL, CC_ENABLED);
        }

        // ECONOMY FULL DISABLED

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_ECONOMY_FULL_DISABLE) == true)
        {
            referenceSetParValue(ECONOMY_FULL, CC_DISABLED);
        }

        // ECONOMY DYNAMIC

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_ECONOMY_DYNAMIC) == true)
        {
            referenceSetParValue(ECONOMY_DYNAMIC, CC_ENABLED);
        }

        // ACQUISITION

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_ACQUISITION) == true)
        {
            reference.acquisition_event = true;
        }

        // COAST

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_COAST) == true)
        {
            refEventCoast(&ref_mgr, (struct CC_us_time *)&dpcls.mcu.event.coast_event_time_us);
        }

        // RECOVER

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_RECOVER) == true)
        {
            refEventRecover(&ref_mgr, (struct CC_us_time *)&dpcls.mcu.event.recover_event_time_us);
        }

        // START HARMONICS

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_START_HARMONICS) == true)
        {
            refEventStartHarmonics(&ref_mgr, (struct CC_us_time *)&dpcls.mcu.event.start_harmonics_event_time_us);
        }

        // STOP HARMONICS

        if (testBitmap(events_received, DPCLS_EVT_RECEIVED_STOP_HARMONICS) == true)
        {
            refEventStopHarmonics(&ref_mgr, (struct CC_us_time *)&dpcls.mcu.event.stop_harmonics_event_time_us);
        }
    }
}



static void referenceUpdateState(void)
{
    static uint8_t const ref_to_pc_state[] =
    {
        FGC_PC_BLOCKING,         // REF_OFF
        FGC_PC_POL_SWITCHING,    // REF_POL_SWITCHING
        FGC_PC_SLOW_ABORT,       // REF_TO_OFF
        FGC_PC_TO_STANDBY,       // REF_TO_STANDBY
        FGC_PC_TO_CYCLING,       // REF_TO_CYCLING
        FGC_PC_ABORTING,         // REF_TO_IDLE
        FGC_PC_DIRECT,           // REF_DIRECT
        FGC_PC_ON_STANDBY,       // REF_STANDBY
        FGC_PC_CYCLING,          // REF_CYCLING
        FGC_PC_ECONOMY,          // REF_DYN_ECO
        FGC_PC_ECONOMY,          // REF_FULL_ECO
        FGC_PC_PAUSED,           // REF_PAUSED
        FGC_PC_IDLE,             // REF_IDLE
        FGC_PC_ARMED,            // REF_ARMED
        FGC_PC_RUNNING,          // REF_RUNNING
    };

    // Update REF mode with the MCU PC mode

    if (dpcom.mcu.pc.mode != ref_to_pc_state[referenceGetParValue(MODE_REF)])
    {
        switch (dpcom.mcu.pc.mode)
        {
            case FGC_PC_ON_STANDBY:  referenceSetParValue(MODE_REF, REF_STANDBY);  break;
            case FGC_PC_IDLE:        referenceSetParValue(MODE_REF, REF_IDLE   );  break;
            case FGC_PC_CYCLING:     referenceSetParValue(MODE_REF, REF_CYCLING);  break;
            case FGC_PC_DIRECT:      referenceSetParValue(MODE_REF, REF_DIRECT );  break;
            default:                 referenceSetParValue(MODE_REF, REF_OFF    );  break;
        }
    }

    // If the FGC doesn't control a converter directly, simulate
    // PC ON when in SLOW_ABORT or above to satisfy libref

    if (dpcom.mcu.state_vs == FGC_VS_NONE)
    {
        referenceSetParValue(PC_STATE, pcStateTest(FGC_STATE_GE_BLOCKING_BIT_MASK) == true
                                       ? REF_PC_ON
                                       : REF_PC_OFF);
    }
    else
    {
        bool ref_pc_off =    dpcom.mcu.state_vs      != FGC_VS_READY
                          || dpcls.mcu.ref.force_off == true;

        referenceSetParValue(PC_STATE, ref_pc_off == true ? REF_PC_OFF : REF_PC_ON);
    }

    // Transfer the REF sate to the MCU

    dpcls.dsp.state.ref = referenceGetVarValue(REF_STATE);
}


// EOF
