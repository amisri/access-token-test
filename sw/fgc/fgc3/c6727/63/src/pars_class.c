//! @file  pars_class.c
//! @brief Pars function invoked when the property is set


// ---------- Includes

#include "fgc3/c6727/63/inc/log_class.h"
#include "fgc3/c6727/63/inc/property_class.h"
#include "fgc3/c6727/63/inc/regulation.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/memmap.h"



// ---------- External function definitions

void ParsRegPars(void)
{
    // Regulation parameters have changed

    regulationUpdatePars();
}



void ParsLogMpx(void)
{
    // This function is called when a LOG.MPX property is modified. It needs to cache the
    // pointers to the signals to be logged.

    logClassCacheSelectors();
}


// EOF
