//! @file reference.h
//! @brief Interfece to cclibs libref

#pragma once


// ---------- Includes

#include <stdint.h>

#include <cclibs.h>
#include <libref.h>



// ---------- External variable declarations

extern struct REF_mgr ref_mgr;



// ---------- External function declarations

//! Initializes the reference module.
//!
//! @retval      true            Initialization was successful.
//! @retval      false           One or more parameter pointers are not set.

bool referenceInit(void);


//! Run part 1 of the real-time libref/libreg functions: reference generation, regulation

void referenceRegulationRtp(void);


//! Run part 2 of the real-time libref/libreg functions: reference state machine, ILC, circuit simulation

void referenceStateRtp(void);


//! Background task.
//!
//! Reports to the MCU the state and status of the polarity switch, the function
//! type and the warnings.

void referenceBgp(void);


//! Resets faults in libref

void referenceResetFaults(void);


//! Trigger an ILC reset

void referenceIlcReset(void);


//! Disarms non-cycling reference function

void referenceRefDisarm(void);


//! Initialize REG.MODE with REG.MODE_CYC

void referenceInitRegMode(void);


//! Pointer to a multi-ppm libref variable.
//!
//! @param[in]   base_address    Pointer to base address for variable.
//! @param[in]   sub_sel         Sub-device selector.
//! @param[in]   cyc_sel         Cycle selector.
//! @param[in]   sub_sel_step    Sub-device selector step in bytes.
//! @param[in]   cyc_sel_step    Cycle selector step in bytes.
//! @param[in]   var_pointer     Pointer to pointer to variable.

void referenceGetPointer(uint8_t        * const base_address,
                         uint32_t const         sub_sel,
                         uint32_t const         cyc_sel,
                         uint32_t const         sub_sel_step,
                         uint32_t const         cyc_sel_step,
                         uint8_t         ** var_pointer);

// Accessers to libref parametes and variables
#define referenceGetVarValue(VAR_NAME)        refMgrVarValue  (&ref_mgr, VAR_NAME)
#define referenceGetParValue(PAR_NAME)        refMgrParValue  (&ref_mgr, PAR_NAME)
#define referenceSetParValue(PAR_NAME, VALUE) refMgrParValue  (&ref_mgr, PAR_NAME)=VALUE
#define referenceGetVarPointer(VAR_NAME)      refMgrVarPointer(&ref_mgr, VAR_NAME)
#define referenceGetParPointer(PAR_NAME)      refMgrParPointer(&ref_mgr, PAR_NAME)

#define referenceFgGetParValueSubCyc  (SUB_SEL, CYC_SEL, PAR_NAME) refMgrFgParValueSubCyc  (&ref_mgr, SUB_SEL, CYC_SEL, PAR_NAME)
#define referenceFgGetParPointerSubCyc(SUB_SEL, CYC_SEL, PAR_NAME) refMgrFgParPointerSubCyc(&ref_mgr, SUB_SEL, CYC_SEL, PAR_NAME)

// EOF
