//! @file property_class.h
//! @brief Variables associated to class-specific properties

#pragma once


// ---------- Includes

#include <stdint.h>

#include <cclibs.h>
#include <libreg.h>
#include <libref.h>
#include <libintlk.h>

#include <defconst.h>

#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/histogram.h"



// ---------- Constants

//! Size required to step through the cycle selectors for PPM properties

#define PROPS_PPM_CYCLE_STEP    (sizeof(property.ppm[0]))

//! Size required to step through the sub selectors for PPM properties

#define PROPS_PPM_SUB_DEV_STEP  (sizeof(uint32_t))



// ---------- External structures, unions and enumerations

//! Structure holding all the PPM variables

struct Property_ppm
{
    struct Property_ppm_meas
    {
        float                          acq_delay[2];                    //!< MEAS.TRIG.DELAY_MS and DELAY2_MS
        float                          acq_value[2];                    //!< MEAS.{PULSE,FUNC{2},CUBEXP{2},etc}.VALUE
    } meas;

    struct Property_ppm_transaction
    {
        enum REF_fg_par_idx            last_fg_par_index[FGC_MAX_SUB_DEVS]; //!< No property connected at the moment
    } transaction;

    struct Property_ppm_func_ctrl
    {
        enum CC_enabled_disabled       play    [FGC_MAX_SUB_DEVS];      //!< REF.FUNC.PLAY            Play reference (ENABLED, DISABLED)
        cc_float                       dyn_eco_end_time[FGC_MAX_SUB_DEVS];  //!< REF.ECONOMY.DYN_END_TIME Cycle time at which the dynamic economy function must rejoin the operational function
    } ctrl;

    struct Property_ppm_func_ref
    {
        enum FG_type                   fg_type [FGC_MAX_SUB_DEVS];      //!< REF.FUNC.TYPE            Ref function type
    } ref;

    // Generated functions

    struct Property_ppm_func_fg
    {
        struct REF_ramp_pars           ramp;                            //!< REF.RAMP.*
        struct REF_plep_pars           plep;                            //!< REF.PLEP.*
        struct REF_pppl_pars           pppl;                            //!< REF.PPPL.*
        struct REF_cubexp_pars         cubexp;                          //!< REF.CUBEXP.*
        struct REF_trim_pars           trim;                            //!< REF.TRIM.*
        struct REF_test_pars           test;                            //!< REF.TEST.*
        struct REF_prbs_pars           prbs;                            //!< REF.PRBS.*

        struct property_ppm_func_pulse
        {
            cc_float                   ref     [FGC_MAX_SUB_DEVS];      //!< REF.PULSE.REF.VALUE       Pulse reference
            cc_float                   duration[FGC_MAX_SUB_DEVS];      //!< REF.PULSE.DURATION        Pulse duration
        } pulse;

        struct property_ppm_func_table
        {
            struct FG_point            function[FGC_TABLE_LEN];          //!< REF.TABLE.FUNCTION      Table function
            uintptr_t                  function_num_elements;            //!<                         Number of elements in table function
        } table;
    } fg;

    // Interlock parameters

    struct Property_ppm_intlk_ppm
    {
        struct INTLK_pars              pars;                            //!< INTERLOCK.*              Parameters for the interlock library
        uint32_t                       output[FGC_MAX_SUB_DEVS];        //!< INTERLOCK.OUTPUT
        struct INTLK_latch_values      latch;                           //!< INTERLOCK.(PPM.){V,I}.MEAS, INTERLOCK.(PPM.)  RESULT
    } intlk;
};

//! Structure holding the DSP properties

struct Property
{
    // PPM properties

    struct Property_ppm                ppm[FGC_MAX_USER_PLUS_1];        //!< Pointer to PPM properties allocated in external memory

    // EXTERNAL ADC properties
    struct Property_ext_adc
    {
        uint32_t                       signals[FGC_N_EXT_ADCS];         //!< ADC.EXTERNAL.SIGNALS

        struct Property_ext_adc_whiterabbit
        {
            uint32_t                   errors[3];                       //!< FGC.WHITERABBIT.ERRROS
        } whiterabbit;
        struct Property_ext_adc_pbam
        {
            uint32_t                   errors[FGC_N_EXT_ADCS][2];       //!< ADC.EXTERNAL.PBAM.ERRORS.{A,B}
        } pbam;
        struct Property_ext_adc_hpm7177
        {
            uint32_t                   errors[FGC_N_EXT_ADCS][5];       //!< ADC.EXTERNAL.HPM7177.ERRORS.{A,B}
        } hpm7177;    
    } ext_adc;


    // Fielbus properties

    struct Property_fieldbus
    {
        uint32_t                       rt_bad_values;                   //!< FIELDBUS.RT_BAD_VALUES
    } fieldbus;

    // Structure holding all the libref parameters

    struct Libref
    {
        // Mode properties

        struct Ref_mode
        {
            enum CC_enabled_disabled   sim_meas;                        //!<                          Simulate measurement mode based on MODE.OP and MEAS.SIM
            uint32_t                   rt_ref;                          //!< MODE.RT                  Real-time reference mode
        } mode;

        // Default operational properties

        struct Ref_defaults_mode
        {
            struct Ref_defaults_mode_b
            {
                float                      acceleration;                 //!< REF.DEFAULTS.OP.B.ACCELERATION
                float                      min_rms;                      //!< REF.DEFAULTS.OP.B.MIN_RMS
            } b;
        } defaults_op;

        // V feedforward properties

        struct V_feed_fwd
        {
            uint32_t                   signal;                          //!< REF.V_FEED_FWD.SIGNAL    Voltage feedfordward signal
        } feed_fwd;

        // The elements below are not mapped to libRef parameters

        struct CC_us_time              abort;                           //!< REF.ABORT
        cc_float                       remaining;                       //!< REF.REMAINING
    } ref;

    // Limits

    struct Limits
    {
        float                          i_access;                        //!< LIMITS.I.ACCESS
    } limits;

    // Voltage source current limits

    struct Property_vs_i_limits
    {
        float                         gain;                            //!< VS.I_LIMIT.GAIN
        float                         ref;                             //!< VS.I_LIMIT.REF
    } vs_i_limit;

    // Measurement properties

    struct Property_meas
    {
        float                          i_rms;                           //!< MEAS.I_RMS  - NOT USED NOW
        float                          i_rms_load;                      //!< MEAS.I_RMS_LOAD  - NOT USED NOW
        uint32_t                       sim;                             //!< MEAS.SIM

        struct Property_meas_max
        {
            float                      u_leads[2];                      //!< MEAS.MAX.U_LEADS
        } max;

    } meas;

    // Mode properties

    struct Property_mode
    {
        uint32_t                       rt;                              //!< MODE.RT
    } mode;

    // Non-ppm interlock parameters

    struct Intlk
    {
        uint32_t                       channel_test;                    //!< INTERLOCK.TEST           Property used to test each channel individually
    } intlk;
};



// ---------- External variable declarations

extern struct Property property;


// EOF
