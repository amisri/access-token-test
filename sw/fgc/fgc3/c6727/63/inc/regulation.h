//! @file regulation.h
//! @brief Interfece to cclibs libreg

#pragma once


// ---------- Includes

#include <libreg.h>
#include <libsig.h>



// ---------- External structures, unions and enumerations

struct Regulation_feedforward
{
    struct CC_meas_signal     disabled;  //!< Constant disabled signal
    struct CC_meas_signal     b_dot;     //!< Magnetic field flux used in voltage feed forward
    struct CC_meas_signal     v_ref;     //!< Voltage reference
    struct CC_meas_signal     v_load;    //!< Measured load voltage
};



// ---------- External variable declarations

extern struct REG_mgr                reg_mgr;
extern struct REG_pars               reg_pars;



// ---------- External function declarations

//! Initializes the reference module.

void regulationInit(void);


//! Updates cclibs parameters before the iteration.
//!
//! Updates measurements.

void regulationRtp(void);


//! Background task.
//!
//! Reports to the MCU the reference values and limits flags.

void regulationBgp(void);


//! Updates the libreg paramters.

void regulationUpdatePars(void);


//! Ignore logs based on the regulation parameters

void regulationIgnoreLogs(void);



// Accessers to libreg parametes and variables

#define regulationGetVarValue(VAR_NAME)    regMgrVarValue(&reg_mgr,  VAR_NAME)
#define regulationGetParValue(PAR_NAME)    regMgrParValue(&reg_mgr,  PAR_NAME)
#define regulationGetAppValue(APP_NAME)    regMgrParAppValue(&reg_pars, APP_NAME)

#define regulationGetVarPointer(VAR_NAME)  regMgrVarPointer(&reg_mgr, VAR_NAME)
#define regulationGetParPointer(PAR_NAME)  regMgrParPointer(&reg_mgr, PAR_NAME)

//! Returns a handler to regMgr. Only to be used by reference.c

#ifdef REFERENCE_FRIEND
struct REG_mgr * regulationGetHandler(void);
#endif


// EOF
