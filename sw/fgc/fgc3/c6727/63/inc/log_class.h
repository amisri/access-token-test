//! @file  log_class.h
//! @brief Class specific logging

#pragma once


// ---------- Includes

#include <liblog.h>

#include "inc/classes/63/logStructs.h"



// ---------- External variable declarations

extern struct LOG_structs   log_structs;
extern struct LOG_buffers   log_buffers;
extern struct LOG_read      log_read;



// ---------- External function declarations

//! Processes the cached selectors in liblog.

void logClassCacheSelectors(void);


// EOF
