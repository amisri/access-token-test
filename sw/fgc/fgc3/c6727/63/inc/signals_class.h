//! @file  signals_class.h
//! @brief Class specific functionality related to signals

#pragma once


// ---------- Includes

#include <stdbool.h>

#include <libsig.h>



// ---------- External function declarations

//! Returns true if the difference between the two current measurment is beyond
//! the threshold specified in LIMITS.I.MEAS_DIFF

bool signalsClassGetIMeasDiffWarning(void);


//! Returns the unfiltered reference transducer signal value.
//!
//! @return The unfiltered reference transducer signal value.

cc_float signalsClassExtRefGetValue(void);


//! Returns whether the external reference is enabled
//!
//! @retval      true            The extenral reference is enabled
//! @retval      false           The extenral reference is disabled

bool signalsClassExtRefIsEnabled(void);


//! Sets the limits for the external ADCs

void signalClassSetExtAdcLimits(void);


// EOF
