/*!
 *  @file      bgp.c
 *  @brief     Background Processing Functions
 */

// Includes

#include <stdint.h>
#include <stdbool.h>

#include <boot_dual_port_ram.h>

#include "fgc3/c6727/60/inc/bgp.h"
#include "fgc3/c6727/60/inc/commands.h"
#include "fgc3/c6727/60/inc/isr.h"
#include "fgc3/c6727/inc/dsp.h"



// Constants
/*
extern int _STACK_END;
extern int _STACK_SIZE;
extern int _stack;

static uint32_t * stack_init       = (uint32_t *)(uintptr_t)(&_stack);
static uint32_t * stack_end        = (uint32_t *)(uintptr_t)(&_STACK_END);
static uint32_t * stack_end_cached = (uint32_t *)(uintptr_t)(&_STACK_END);
static uint32_t   stack_size       = (uint32_t)(&_STACK_SIZE);
static uint32_t   stack_pattern    = 0x0BADBABE;
static uint32_t * stack_ptr;
*/

// Function memory allocation


// Internal functions declaration

/*!
 * Checks the stack usage every second
 */
static void bgpCheckStack(void);



// External function definitions

void bgpInit(void)
{
   ; // Do nothing
}




void bgpProcess(void)
{
    for (;;)
    {
        TGL_TP1();

        // Reset the counter incremented by the MCU.

        // dpcom.dsp_bgp_alive = 0;

        ExecuteSlowCommands();

        bgpCheckStack();
    }
}



// Internal function definitions

static void bgpCheckStack(void)
{
/* @TODO
    if (*stack_end_cached != stack_pattern)
    {
        stack_ptr = stack_init;

        while (stack_ptr < stack_end_cached && *stack_ptr == stack_pattern)
        {
            stack_ptr++;
        }
        
        dpcom.dsp.stack_usage = ((100 * sizeof(uint32_t)) * (uint32_t)(stack_end - stack_ptr)) / stack_size;
        stack_end_cached = stack_ptr - 1;
    }
*/
}


// EOF

