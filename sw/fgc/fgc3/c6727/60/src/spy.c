/*---------------------------------------------------------------------------------------------------------*\
  File:         spy.c

 Purpose:      spy functions, implemented in the TMS320c6727b

 \*---------------------------------------------------------------------------------------------------------*/

#define SPY_GLOBALS             // for spy global variable

#include <spy.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>              // for sprintf()
#include <memmap_dsp.h>         // for ANA_ADCS_A
#include <boot_dual_port_ram.h> // for dpram global variable
#include <boot_dsp_cmds.h>      // constants shared by DSP and CPU, for DSP_STS_, DSP_CMD_
#include <string.h>             // for size_t
#ifdef __TMS320C6727__
#include <structs_bits_little.h>
#endif
#include <dsp_dependent.h>      // for DISABLE_INTS/ENABLE_INTS
#include <adc.h>                // for gAdc[] global variable
#include <debug.h>


/*---------------------------------------------------------------------------------------------------------*/
void StoreSpySampleInLocalBuffer(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function stores analogue values in SDRAM buffer for the Spy interface.
 it is called inside the ISR
 \*---------------------------------------------------------------------------------------------------------*/
{
/*
    uint8_t xx;
    uint8_t adc_idx;

    if (dpram->mcu.slowCmd == DSP_CMD_SLOW_TST_SDRAM)
    {
        return; // don't send data to Spy if SDRAM test is running
    }

    if (spy.is_running)
    {
        // check if this sample must go or not
        spy.skipping_counter--;

        if (spy.skipping_counter == 0)
        {
            // reload with what the remote user has configured
            spy.skipping_counter = spy.samples_to_skip;

            // sanity check with the size
            if (spy.local_buffer.bytes_to_send <= SDRAM_CODE_B)
            {
                // store the sample
                switch (spy.owner)
                {
                    case SPY_OWNER_SDRamTest:
                        // Remember: for ANA-101, ADC values are inverted
                        spy.local_buffer.free_place_ptr->ram_test.item_tag = spy.sequential_tag;
                        spy.local_buffer.free_place_ptr->ram_test.data[0] = (float)(int32_t) ANA_FGC3_ADC_SAMPLE_A[0];
                        spy.local_buffer.free_place_ptr->ram_test.data[1] = (float)(int32_t) ANA_FGC3_ADC_SAMPLE_A[1];
                        spy.local_buffer.free_place_ptr->ram_test.data[2] = (float)(int32_t) ANA_FGC3_ADC_SAMPLE_A[2];
                        spy.local_buffer.free_place_ptr->ram_test.data[3] = (float)(int32_t) ANA_FGC3_ADC_SAMPLE_A[3];
                        spy.local_buffer.free_place_ptr->ram_test.data[4] = 0.0;
                        spy.local_buffer.free_place_ptr->ram_test.data[5] = 0.0;
                        break;

                    case SPY_OWNER_AnaloguePllTest:
                        // the pll data is updated in the dpram at 20 ms rate by the MCU

                        // the dsp is falling here every 100us (10 KHz)
                        // so we need to skip at least 200 samples
                        spy.local_buffer.free_place_ptr->analog_pll.item_tag = spy.sequential_tag;

                        for (xx = 0; xx < 6; xx++)
                        {
                            spy.local_buffer.free_place_ptr->analog_pll.data[xx] = dpram->mcu.pll_debug_data[xx];
                        }

                        break;

                    case SPY_OWNER_Ana_102_Test_Pulse1:
                        spy.local_buffer.free_place_ptr->ana_102_p.item_tag = spy.sequential_tag;

                        for (adc_idx = 0; adc_idx < ANA102_N_SLOW_ADCS; adc_idx++)
                        {
                            spy.local_buffer.free_place_ptr->ana_102_p.slow_adc[adc_idx] = gAdc[adc_idx].raw;
                        }

                        //   The 4 slow samples are read in adc.c. Here we read fast samples.
                        for (adc_idx = FGC_N_ADCS - ANA102_N_FAST_ADCS; adc_idx < FGC_N_ADCS; adc_idx++)
                        {
                            for (xx = 0; xx < ANA102_N_FAST_SAMPLES; xx++)
                            {

                                //                                spy.local_buffer.free_place_ptr->ana_102_p.fast_adc[adc_idx - ANA102_N_SLOW_ADCS][xx] =
                                //                                        ANA_FGC3_ADC_SAMPLE_A[adc_idx + first_channel];
                                spy.fast_acq_buf[xx] = ANA_FGC3_ADC_SAMPLE_A[adc_idx];

                            }

                            memcpy(spy.local_buffer.free_place_ptr->ana_102_p.fast_adc[adc_idx - (FGC_N_ADCS - ANA102_N_FAST_ADCS)],
                                   spy.fast_acq_buf, sizeof(spy.fast_acq_buf));
                        }

                        // only one shot of the buffer
                        if (spy.sequential_tag >= ANA102_N_BLOCK)
                        {
                            spy.is_running = FALSE;
                        }

                        break;

                    case SPY_OWNER_Ana_102_Test_Average:
                        for (xx = 0; xx < 4; xx++) // do the 4 ADC channels
                        {
                            // Shift by 20 bits ADCs + log2(n_samples) - 32
                            // 20 + |`log2(100*10000)`| - 32 = 20 + 20 - 32 = 8
                            gAdc[xx].sum.int64s = gAdc[xx].sum.int64s >> 8;
                        }

                        spy.local_buffer.free_place_ptr->ana_102.item_tag = spy.sequential_tag;

                        for (xx = 0; xx < FGC_N_ADCS; xx++)
                        {
                            spy.local_buffer.free_place_ptr->ana_102.adc[xx] = gAdc[xx].sum.part.lo;
                        }

                        break;

                    case SPY_OWNER_Ana_102_Test_Average_10s:
                        for (xx = 0; xx < 4; xx++) // do the 4 ADC channels
                        {
                            // Shift by : 20 bits ADCs + log2(n_samples) - 32
                            // 20 + |`log2(100*100000)`| - 32 = 20 + 24 - 32 = 12
                            gAdc[xx].sum.int64s = gAdc[xx].sum.int64s >> 12;
                        }

                        spy.local_buffer.free_place_ptr->ana_102.item_tag = spy.sequential_tag;

                        for (xx = 0; xx < FGC_N_ADCS; xx++)
                        {
                            spy.local_buffer.free_place_ptr->ana_102.adc[xx] = gAdc[xx].sum.part.lo;
                        }

                        break;

                    case SPY_OWNER_Ana_102_Test_Slow_Acq:

                        spy.local_buffer.free_place_ptr->ana_102.item_tag = spy.sequential_tag;

                        for (xx = 0; xx < FGC_N_ADCS; xx++)
                        {
                            spy.local_buffer.free_place_ptr->ana_102.adc[xx] = gAdc[xx].raw;
                        }

                        // only one shot of the buffer
                        if (spy.sequential_tag >= 50000) // 5s at 10kHz
                        {
                            spy.is_running = FALSE;
                        }

                        break;

                    default:
                        break;
                }

                spy.local_buffer.bytes_to_send += spy.local_buffer.item_size;

                // prepare for next
                spy.sequential_tag++;
                spy.local_buffer.free_place_ptr =
                    (union spy_packet *)((uint8_t *)(spy.local_buffer.free_place_ptr)
                                         + spy.local_buffer.item_size);

                if ((uint32_t) spy.local_buffer.free_place_ptr >= (SDRAM_CODE_32 + SDRAM_CODE_B))
                {
                    spy.local_buffer.free_place_ptr = (union spy_packet *) SDRAM_CODE_32;
                }

                // sanity check
                if (spy.local_buffer.bytes_to_send > SDRAM_CODE_B)
                {
                    spy.local_buffer.bytes_to_send = SDRAM_CODE_B;
                    dpram->dsp.status |= DSP_STS_SPY_OVERUN;
                }

                // for statistics trace which is the maximum reached
                if (spy.local_buffer.bytes_to_send > spy.local_buffer.max_size_reached)
                {
                    spy.local_buffer.max_size_reached = spy.local_buffer.bytes_to_send;
                }
            }
            else
            {
                // error we used more space that allocated in the local buffer
            }
        }
    }
*/
}
/*---------------------------------------------------------------------------------------------------------*/
void interruptible_memcpy(void * s1, const void * s2, size_t n)
/*---------------------------------------------------------------------------------------------------------*\
    Standard library memcpy is not interruptible on TMS320C67x. This can cause problems, with longer than expected
 critical sections in the Background (BGP) task. (The bug was first seen in PropComms function.)
 \*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t * s1_w, *s2_w;
    uint8_t * s1_b, *s2_b;

    // If target, source and size are all multiple of 4, do a 32-bit word copy.
    if (!((uint32_t) s1 & 3) && !((uint32_t) s2 & 3) && !(n & 3))
    {
        s1_w = (uint32_t *) s1;
        s2_w = (uint32_t *) s2;
        n /= 4;

        while (n--)
        {
            *s1_w++ = *s2_w++;          // Word copy
        }
    }
    // Else, do a byte-copy
    else
    {
        s1_b = (uint8_t *) s1;
        s2_b = (uint8_t *) s2;

        while (n--)
        {
            *s1_b++ = *s2_b++;          // Byte copy
        }
    }
}


void SpyInit(void)
/*---------------------------------------------------------------------------------------------------------*\

 \*---------------------------------------------------------------------------------------------------------*/
{
    spy.sequential_tag = 0;

    spy.samples_to_skip = spy.cmd_register & SPY_CTRL_CMD_MASK32; // Get sample rate in spy cmd register
    spy.skipping_counter = spy.samples_to_skip;

    //---------------------------------------
    // prepare local buffer
    switch (spy.owner)
    {
        case SPY_OWNER_SDRamTest:
            spy.local_buffer.item_size = sizeof(struct TSDRamTestPacket);
            break;

        case SPY_OWNER_AnaloguePllTest:
            spy.local_buffer.item_size = sizeof(struct TAnalogPLLPacket);
            break;

        case SPY_OWNER_Ana_102_Test_Pulse1:
            spy.local_buffer.item_size = sizeof(struct TAna102TestPacket_tst_pulse);
            spy.int32s_counts = 0;
            break;

        case SPY_OWNER_Ana_102_Test_Average:
        case SPY_OWNER_Ana_102_Test_Average_10s:
        case SPY_OWNER_Ana_102_Test_Slow_Acq:
            spy.local_buffer.item_size = sizeof(struct TAna102TestPacket);
            spy.int32s_counts = 0;
            break;

        default:
            spy.local_buffer.item_size = 7; // to force a SPY transmission
            break;
    }

    spy.local_buffer.bytes_to_send = 0;
    spy.local_buffer.max_size_reached = 0;
    spy.local_buffer.item_to_send_ptr = (uint8_t *) SDRAM_EXT_32;
    spy.local_buffer.free_place_ptr = (union spy_packet *) SDRAM_EXT_32;

    memset((void *) SDRAM_EXT_32, 0, SDRAM_EXT_B);
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: spy.c
 \*---------------------------------------------------------------------------------------------------------*/
