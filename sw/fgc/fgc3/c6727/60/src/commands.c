/*---------------------------------------------------------------------------------------------------------*\
  File:         commands.c

  Purpose:

\*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>

#include <bitmap.h>
#include <commands.h>
#include <memmap_dsp.h>         // for ANA_MPX_P, ANA_DAC1_RQST_P
#include <boot_dual_port_ram.h> // for dpram global variable
#include <boot_dsp_cmds.h>      // constants shared by DSP and CPU, for DSP_STS_, DSP_CMD_
#include <dsp_time_consumed.h>  // for dsp_time_consumed_in global variable
// #include <test_ram.h>           // for Test_SDRam(), Test_PLD_DPRam()
#include <main.h>               // for pll_m_count, pll_n_count, time global variables
#include <defconst.h>           // for FGC_N_ADCS
#include <stdlib.h>             // for strtod()
// strtof identical to strtod
// long double __cdecl strtold (const char * __restrict__, char ** __restrict__);

#ifdef __TMS320C6727__
#include <structs_bits_little.h>
#endif

#include <memTestDsp.h>
#include <spy.h>                // for spy global variable
#include <dac_max5541.h>        // for SetDAC_1(), SetDAC_2()
#include <debug.h>              // for
#include <string.h>             // for strncpy()
#include <version.h>            // for struct version and version variable
#include <adc.h>                // for global variable gAdcHistory_index_displacement, gAdcHistory_index
#include <lib.h>                // for interruptible_memcpy

/*---------------------------------------------------------------------------------------------------------*/
void PrepareVersionString(const char * original, volatile char * prepared)
/*---------------------------------------------------------------------------------------------------------*\
  the MCU is big endian, this DSP is little endian
\*---------------------------------------------------------------------------------------------------------*/
{
    char                len_fixed_str[4 * DPRAM_ARG_MAX];
    int32_t             len;
    uint32_t            xx;

    // the dual port memory buffer for this commands is uint32_t[DPRAM_ARG_MAX]

    strncpy(len_fixed_str, original, (4 * DPRAM_ARG_MAX));      // padding is innocuous
    // terminate the string for the case len bigger than ( 4 * DPRAM_ARG_MAX )
    len_fixed_str[(4 * DPRAM_ARG_MAX) - 1 ] = 0;

    len = strlen(len_fixed_str);

    // go through little to big endian + PLD crossing bytes
    xx = 0;

    while (len > 0)
    {
        prepared[xx + 0] = len_fixed_str[xx + 3];
        prepared[xx + 1] = len_fixed_str[xx + 2];
        prepared[xx + 2] = len_fixed_str[xx + 1];
        prepared[xx + 3] = len_fixed_str[xx + 0];

        xx  += 4;
        len -= 4;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void FastCommand_ResetStatus(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    dpram->dsp.status =  DSP_STS_NOTHING;       // DSP_HANDSHAKE_DSP_ACK during boot loading
    //
    //    dsp_time_consumed_in.msTickIsr.stamp_ini        = 0;
    //    dsp_time_consumed_in.interpolation.stamp_ini    = 0;
    //
    //    dsp_time_consumed_in.msTickIsr.actual       = 0;
    //    dsp_time_consumed_in.interpolation.actual   = 0;

    dsp_time_consumed_in.msTickIsr.max = 0;
    dsp_time_consumed_in.interpolation.max = 0;
}
/*---------------------------------------------------------------------------------------------------------*/
void ExecuteFastCommands(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function answers MCU RX610 fast commands.
  it is called from ISR
\*---------------------------------------------------------------------------------------------------------*/
{
    uint32_t mcu_read = 0;

    switch (dpram->mcu.fastCmd)
    {
        case DSP_CMD_NOTHING:                                   // do nothing
            break;

        case DSP_CMD_FAST_RETURN_NEG_DATA:                      // return negated data
            dpram->dsp.fastRsp.data = ~dpram->mcu.fastArg.data;
            break;

        case DSP_CMD_FAST_CLEAR_INFO:                           // Reset DSP status
            FastCommand_ResetStatus();
            break;

        case DSP_CMD_FAST_STRESS_TST_PLD_DPRAM:                 // DPRAM stress test
            dpram->dsp.slowRsp.data = 0;

            while (mcu_read != 0xFFFFFFFF)
            {
                mcu_read = dpram->mcu.slowArg.data;
                dpram->dsp.slowRsp.data = mcu_read;
            }

            break;

        case DSP_CMD_FAST_SIGNAL_MEASURE:                       // Reset DSP status

            // which sample to take [k-5..0..k+5]
            gAdcHistory_index_displacement = dpram->mcu.fastArg.int32s;

            // no problem with this global variable as it is used only inside the ISR
            // and we are already inside the ISR!
            gAdcHistory_index = 0;                              // k starts now
            break;

        default:
            dpram->dsp.status |= DSP_STS_CMD_UNKNOWN;           // report unknown cmd in dsp status
            break;
    }

    dpram->mcu.fastCmd = 0;     // clear command
}
/*---------------------------------------------------------------------------------------------------------*/
void ExecuteSlowCommands(void)
/*---------------------------------------------------------------------------------------------------------*\
  execute requested commands
\*---------------------------------------------------------------------------------------------------------*/
{
    static uint32_t    * poke_address;
    uint32_t           * mem_address;
    uint32_t             mem_size;
    char                 converted_str[32];  // this must match dpram->mcu.slowArg.str
    uint32_t             ii;


    switch (dpram->mcu.slowCmd)
    {
        case DSP_CMD_NOTHING:               // No cmd
            return;

        case DSP_CMD_SLOW_RETURN_NEG_DATA :     // inverse test

            dpram->dsp.slowRsp.data = ~dpram->mcu.slowArg.data;
            break;

        case DSP_CMD_SLOW_TST_SDRAM:

            // SDRAM test

            clrBitmap(dpram->dsp.status, DSP_STS_MEM_DATA_FAULT);
            clrBitmap(dpram->dsp.status, DSP_STS_MEM_ADDRESS_FAULT);
            clrBitmap(dpram->dsp.status, DSP_STS_MEM_DEVICE_FAULT);

            // Skip the first KB of SDRAM as this is used for data backup during memory tests

            mem_address = ((uint32_t *) SDRAM_32) + (MEM_TEST_DSP_MAX_BLOCK_SIZE/sizeof(uint32_t));
            mem_size    = SDRAM_B - MEM_TEST_DSP_MAX_BLOCK_SIZE;

            memTestDsp(mem_address, mem_size);
            break;

        case DSP_CMD_SLOW_TST_PLD_DPRAM:

            // DPRAM test

            clrBitmap(dpram->dsp.status, DSP_STS_MEM_DATA_FAULT);
            clrBitmap(dpram->dsp.status, DSP_STS_MEM_ADDRESS_FAULT);
            clrBitmap(dpram->dsp.status, DSP_STS_MEM_DEVICE_FAULT);

            mem_address = (uint32_t *) DPRAM_32;
            mem_size    = DPRAM_B;

            memTestDsp(mem_address, mem_size);
            break;

        case DSP_CMD_SLOW_ADC_TEST:
            // +10V reference test setup

            ANA_FGC3_CTRL_SWBUS_P = ANA_FGC3_CTRL_SWBUS_SHIFT;
            ANA_FGC3_CTRL_SWIN_P  = ANA_FGC3_CTRL_SWIN_SHIFT;
            ANA_FGC3_CTRL_MPX_P   = ANA_FGC3_CTRL_MPX_ENA_MASK8 | ((uint8_t)dpram->mcu.slowArg.data & ANA_FGC3_CTRL_MPX_SEL_MASK8);
            ANA_FGC3_CTRL_SWBUS_P = ANA_FGC3_CTRL_SWBUS_MASK8;
            break;

        case DSP_CMD_SLOW_ADC_DEFAULT:
            ANA_FGC3_CTRL_SWBUS_P = ANA_FGC3_CTRL_SWBUS_SHIFT;
            ANA_FGC3_CTRL_SWIN_P  = ANA_FGC3_CTRL_SWIN_MASK8;
            break;

        case DSP_CMD_SLOW_ADC_PATH:         // Set analogue Multiplexers
            ANA_FGC3_CTRL_SWIN_P  = (uint8_t) dpram->mcu.slowArg.adc_path.external_signal_access;
            ANA_FGC3_CTRL_SWBUS_P = (uint8_t) dpram->mcu.slowArg.adc_path.adc_selector_bus_or_signal;
            ANA_FGC3_CTRL_MPX_P   = (uint8_t) dpram->mcu.slowArg.adc_path.internal_signal_bus_access;
            break;

        case DSP_CMD_SLOW_SET_DAC1:
            SetDAC_1(dpram->mcu.slowArg.data);
            break;

        case DSP_CMD_SLOW_SET_DAC2:
            SetDAC_2(dpram->mcu.slowArg.data);
            break;

        case DSP_CMD_SLOW_SPY_ONE_SHOT:
            if (dpram->mcu.slowArg.data == 0x33333)
            {
                SpyInit();
                spy.is_running = TRUE;  // enable SPY taking samples from ANA-102

                register uint32_t      counter;

                counter = 0x00015F90;   // wait ~300us

                do
                {
                    counter--;
                }
                while (counter != 0x00000000);

                SetDAC_1(dpram->mcu.slowArg.data);
                SetDAC_2(dpram->mcu.slowArg.data);
            }
            else
            {
                SetDAC_1(dpram->mcu.slowArg.data);
                SetDAC_2(dpram->mcu.slowArg.data);
            }

            break;

        case DSP_CMD_SLOW_SPY_TEST: // ToDo get rid of this test?
            // dpram->dsp.slowRsp.data = SPY_CTRL_P;
            // if (((SPY_CTRL_P & SPY_CTRL_STATUS_USB_RDY_MASK32) == SPY_CTRL_STATUS_USB_RDY_MASK32)
            //     && ((SPY_CTRL_P & SPY_CTRL_CMD_MASK32) != 0))
            // {
            //     const char sample_str[] = "+0123456789,-0123456789,";
            //     ((uint32_t *)0x90004200)[0] = 0x2B303132;
            //     ((uint32_t *)0x90004200)[1] = 0x33343536;
            //     ((uint32_t *)0x90004200)[2] = 0x12345678;
            //     ((uint32_t *)0x90004200)[4] = 0x87654321;

            //     interruptible_memcpy((void *)(SPY_BUFFER_P + 0x20), (void *) sample_str, 16);

            //     interruptible_memcpy((void *)(SPY_BUFFER_P + 0x40), (void *) SPY_BUFFER_P, 16);

            //     ((uint32_t *)0x90004200)[20] = *((uint32_t *) sample_str);
            //     ((uint32_t *)0x90004200)[21] = *(((uint32_t *) sample_str) + 1);
            //     ((uint32_t *)0x90004200)[22] = *(((uint32_t *) sample_str) + 2);
            //     ((uint32_t *)0x90004200)[23] = *(((uint32_t *) sample_str) + 3);

            //     *((uint8_t *)0x90004260) = '+';
            //     *((uint8_t *)0x90004261) = '1';
            //     *((uint8_t *)0x90004262) = '2';
            //     *((uint8_t *)0x90004263) = '3';
            //     *((uint8_t *)0x90004264) = '4';


            //     ((uint32_t *)0x90004200)[SPY_BUFFER_B / 4 - 1] = 0; // Go

            // }

            // dpram->dsp.slowRsp.data |= SPY_CTRL_P << 16;
            break;

        case DSP_CMD_SLOW_GET_INFO:
            Get_time_consumed();
            break;

        case DSP_CMD_SLOW_SET_SPIVS_CTRL:

            while (SPIVS_CTRL_P & SPIVS_CTRL_COMMIT_MASK32);

            SPIVS_CTRL_P = dpram->mcu.slowArg.spivs.control.part.lo;

            break;

        case DSP_CMD_SLOW_SET_SPIVS_SEND_VALUE:

            while (SPIVS_CTRL_P & SPIVS_CTRL_COMMIT_MASK32);

            for (ii = 0; ii < 7; ii++)
            {
                *((REG32U *) SPIVS_TB_32 + ii) = *((uint32_t *) dpram->mcu.slowArg.spivs.buf + ii);
            }

            break;

        case DSP_CMD_SLOW_SET_SPIVS_NUM_LWORDS:

            while (SPIVS_CTRL_P & SPIVS_CTRL_COMMIT_MASK32);

            SPIVS_NUM_LWORDS_P = dpram->mcu.slowArg.spivs.num_lwords.part.lo;

            break;

        case DSP_CMD_SLOW_GET_SPIVS_SEND_VALUE:

            while (SPIVS_CTRL_P & SPIVS_CTRL_COMMIT_MASK32);

            dpram->dsp.slowRsp.spivs.control.int32u    = SPIVS_CTRL_P;
            dpram->dsp.slowRsp.spivs.num_lwords.int32u = SPIVS_NUM_LWORDS_P;

            for (ii = 0; ii < 7; ii++)
            {
                dpram->dsp.slowRsp.spivs.buf[ii] = *((REG32U *) SPIVS_TB_32 + ii);
            }

            break;

        case DSP_CMD_SLOW_GET_SPIVS_RETURN_VALUE:

            while (SPIVS_CTRL_P & SPIVS_CTRL_COMMIT_MASK32);

            dpram->dsp.slowRsp.spivs.control.int32u    = SPIVS_CTRL_P;
            dpram->dsp.slowRsp.spivs.num_lwords.int32u = SPIVS_NUM_LWORDS_P;

            for (ii = 0; ii < 7; ii++)
            {
                dpram->dsp.slowRsp.spivs.buf[ii] = *((REG32U *) SPIVS_RB_32 + ii);
            }

            break;

        case DSP_CMD_SLOW_GET_SPY_INFO:
            break;

        case DSP_CMD_SLOW_SET_SPY_OWNER:
            spy.owner = (spy_owner_t) dpram->mcu.slowArg.data;
            break;

        case DSP_CMD_SLOW_SET_REFERENCE:
            // this is exclusive for Acapulco, Cancun with its +50,-50 range
            gReference = dpram->mcu.slowArg.regulation_reference;
            break;

        case DSP_CMD_SLOW_START_STOP_REGULATION:
            gRegulationEnable = dpram->mcu.slowArg.data;
            break;

        case DSP_CMD_SLOW_SELECT_ALGORITHM:
            gRegulationAlgorithm = dpram->mcu.slowArg.data;
            break;

        case DSP_CMD_SLOW_StrToFP32:
            // due to the PLD implementation of the big endian / little endian conversion working
            // on 32bits words the string need to be "prepared" for the MCU

            for(ii = 0; ii < 32; ii += 4)
            {
                converted_str[ii + 0] = dpram->mcu.slowArg.str[ii + 3];
                converted_str[ii + 1] = dpram->mcu.slowArg.str[ii + 2];
                converted_str[ii + 2] = dpram->mcu.slowArg.str[ii + 1];
                converted_str[ii + 3] = dpram->mcu.slowArg.str[ii + 0];
            }

            dpram->dsp.slowRsp.data32.fp32 = (float) strtod(converted_str, (char **) 0); // NULL);
            break;

        case DSP_CMD_SLOW_StrToFP64:
            // due to the PLD implementation of the big endian / little endian conversion working
            // on 32bits words the string need to be "prepared" for the MCU

            for(ii = 0; ii < 32; ii += 4)
            {
                converted_str[ii + 0] = dpram->mcu.slowArg.str[ii + 3];
                converted_str[ii + 1] = dpram->mcu.slowArg.str[ii + 2];
                converted_str[ii + 2] = dpram->mcu.slowArg.str[ii + 1];
                converted_str[ii + 3] = dpram->mcu.slowArg.str[ii + 0];
            }

            // tmp_64bits.fp64 = strtod(converted_str, (char **) 0); // NULL);

            // due to the PLD implementation of the big endian / little endian conversion working
            // on 32bits words the fp64 need to be "prepared"

            // @TODO implement this not with an 8 byte variable but with a two 4 byte variables
            // dpram->dsp.slowRsp.data64.part.lo = tmp_64bits.part.hi;
            // dpram->dsp.slowRsp.data64.part.hi = tmp_64bits.part.lo;
            break;

        case DSP_CMD_SLOW_GET_VERSION_UNIXTIME_A:
            PrepareVersionString(version.unixtime_a, dpram->dsp.slowRsp.ch);      // Build timestamp in ASCII
            break;

        case DSP_CMD_SLOW_GET_VERSION_HOSTNAME:
            PrepareVersionString(version.hostname, dpram->dsp.slowRsp.ch);    // Host name of machine used for the build
            break;

        case DSP_CMD_SLOW_GET_VERSION_ARCHITECTURE:
            PrepareVersionString(version.architecture, dpram->dsp.slowRsp.ch); // Arch of the machine used for the build
            break;

        case DSP_CMD_SLOW_GET_VERSION_CC_VERSION:
            PrepareVersionString(version.cc_version, dpram->dsp.slowRsp.ch);    // C compiler version
            break;

        case DSP_CMD_SLOW_GET_VERSION_USER_GROUP:
            PrepareVersionString(version.user_group, dpram->dsp.slowRsp.ch);    // User and group that performed the build
            break;

        case DSP_CMD_SLOW_GET_VERSION_DIRECTORY:
            PrepareVersionString(version.directory, dpram->dsp.slowRsp.ch);    // Build directory
            break;

        case DSP_CMD_SLOW_MID_FGCTYPE_TEST:

            dpram->dsp.slowRsp.data = DSP_MID_OK;

            if (DSP_MID_FGCTYPE_P != 0x0301)
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_FGCTYPE_P);
            }

            break;

        case DSP_CMD_SLOW_MID_FGCVER_TEST:

            dpram->dsp.slowRsp.data = DSP_MID_OK;

            if (DSP_MID_FGCVER_P != 0x0002)
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_FGCVER_P);
            }

            break;

        case DSP_CMD_SLOW_MID_PLDTYPE_TEST:

            dpram->dsp.slowRsp.data = DSP_MID_OK;

            if (   DSP_MID_PLDTYPE_P != 0x0700
                && DSP_MID_PLDTYPE_P != 0x1400)
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_PLDTYPE_P);
            }

            break;

        case DSP_CMD_SLOW_MID_PLDVER_TEST:

            dpram->dsp.slowRsp.data = DSP_MID_OK;

            uint16_t test_value;

            // Nibble X : FPGA or CPLD Type (0 = XC3S700AN, 1 = XC3S1400AN)

            test_value = DSP_MID_PLDVER_P & 0xF000;

            if (   test_value != 0x0000
                && test_value != 0x1000)
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, test_value);
            }

            test_value = DSP_MID_PLDVER_P & 0x0F00;

            // Nibble Y : Mode (0 = Normal, 1 = Tester)

            if (   test_value != 0x0000
                && test_value != 0x0100)
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, test_value);
            }

            break;

        case DSP_CMD_SLOW_MID_CRATETYPE_TEST:

            dpram->dsp.slowRsp.data = DSP_MID_OK;

            if (DSP_MID_CRATETYPE_P != FGC_CRATE_TYPE_RECEPTION)
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_CRATETYPE_P);
            }

            break;

        case DSP_CMD_SLOW_MID_NETTYPE_TEST:

            dpram->dsp.slowRsp.data = DSP_MID_OK;

            if (   DSP_MID_NETTYPE_P != FGC_NETWORK_ETHER_101
                && DSP_MID_NETTYPE_P != FGC_NETWORK_ETHER_103)                
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_NETTYPE_P);
            }

            break;

        case DSP_CMD_SLOW_MID_ANATYPE_TEST:

            dpram->dsp.slowRsp.data = DSP_MID_OK;

            if (   DSP_MID_ANATYPE_P != FGC_INTERFACE_ANA_101
                && DSP_MID_ANATYPE_P != FGC_INTERFACE_ANA_103
                && DSP_MID_ANATYPE_P != FGC_INTERFACE_ANA_104)
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ANATYPE_P);
            }

            break;

        case DSP_CMD_SLOW_MID_MISMATCH_TEST:

            dpram->dsp.slowRsp.data = DSP_MID_OK;

            test_value = (DSP_MID_PLDVER_P & 0x00F0) >> 4;

            if (test_value != DSP_MID_ANATYPE_P)
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, test_value);
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ANATYPE_P);
            }

            break;

        case DSP_CMD_SLOW_MID_EOMID_TEST:

            dpram->dsp.slowRsp.data = DSP_MID_OK;

            if (DSP_MID_EOMID_P != 0xA55A)
            {
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_ERROR);
                setBitmap(dpram->dsp.slowRsp.data, DSP_MID_EOMID_P);
            }

            break;

        case DSP_CMD_SLOW_PEEK_WORD:
            dpram->dsp.slowRsp.data = *(uint32_t *)dpram->mcu.slowArg.data;
            break;

        case DSP_CMD_SLOW_POKE_WORD_ADDRESS:
            poke_address = (uint32_t *)dpram->mcu.slowArg.data;
            break;

        case DSP_CMD_SLOW_POKE_WORD_VALUE:
            *poke_address = dpram->mcu.slowArg.data;
            break;

        case DSP_CMD_SLOW_IRQTRL_DSP_TRIG:
            IRQCTRL_DSPSWIRQ1TRIG_P = 1;
            IRQCTRL_DSPSWIRQ2TRIG_P = 1;
            IRQCTRL_DSPSWIRQ3TRIG_P = 1;
            break;

        default:
            break;
    }

    // Clear cmd if non zero
    if (dpram->mcu.slowCmd != DSP_CMD_NOTHING)    // the M32C87 will wait for DSP_CMD_NOTHING as ack
    {
        dpram->mcu.slowCmd = DSP_CMD_NOTHING;
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: commands.c
\*---------------------------------------------------------------------------------------------------------*/
