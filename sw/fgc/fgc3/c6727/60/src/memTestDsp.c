/*!
 *  @file   memTestDsp.c
 *  @brief  Function for DSP memory testing
 */


// Includes

#include <stdlib.h>
#include <string.h>

#include <bitmap.h>
#include <boot_dsp_cmds.h>
#include <boot_dual_port_ram.h>
#include <memTest.h>
#include <memTestDsp.h>



// Internal variable definitions

#pragma DATA_SECTION(data_backup, ".ext_data");

uint32_t data_backup[MEM_TEST_DSP_MAX_BLOCK_SIZE/sizeof(uint32_t)];



// External function definitions

int memTestDsp(volatile uint32_t * base_address, uint32_t num_bytes)
{
    uint32_t   data_bus_result;
    uint32_t * address_bus_result;
    uint32_t * device_result = NULL;
    
    volatile uint32_t * test_address;


    // Data bus test

    data_bus_result = memTestDataBus(base_address, data_backup);

    if (data_bus_result != 0)
    {
        setBitmap(dpram->dsp.status, DSP_STS_MEM_DATA_FAULT);
        return (-1);
    }


    // Address bus test

    address_bus_result = memTestAddressBus(base_address, num_bytes, data_backup);

    if (address_bus_result != NULL)
    {
        setBitmap(dpram->dsp.status, DSP_STS_MEM_ADDRESS_FAULT);
        dpram->dsp.slowRsp.data = (uint32_t) address_bus_result;
        return (-1);
    }


    // Device test

    // Test memory blocks of 1KB at a time

    test_address = base_address;

    while(num_bytes > MEM_TEST_DSP_MAX_BLOCK_SIZE && device_result == NULL)
    {
        device_result  = memTestDevice(test_address, MEM_TEST_DSP_MAX_BLOCK_SIZE, data_backup);
        test_address  += (MEM_TEST_DSP_MAX_BLOCK_SIZE/sizeof(uint32_t));

        if (num_bytes > MEM_TEST_DSP_MAX_BLOCK_SIZE)
        {
            num_bytes -= MEM_TEST_DSP_MAX_BLOCK_SIZE;
        }
    }

    if (num_bytes != 0 && device_result == NULL)
    {
        device_result = memTestDevice(test_address, num_bytes, data_backup);
    }

    if (device_result != NULL)
    {
        setBitmap(dpram->dsp.status, DSP_STS_MEM_DEVICE_FAULT);
        dpram->dsp.slowRsp.data = (uint32_t) device_result;
        return (-1);
    }

    return (0);
}

// EOF
