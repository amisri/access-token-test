/*!
 *  @file      main.h
 *  @brief     FGC3 DSP main function
 */

// Includes

#define BOOT_DUAL_PORT_RAM_GLOBALS

#include <string.h>

#include <boot_dual_port_ram.h>

#include "fgc3/c6727/60/inc/adc.h"
#include "fgc3/c6727/60/inc/bgp.h"
#include "fgc3/c6727/60/inc/isr.h"
#include "fgc3/c6727/60/inc/main.h"
#include "fgc3/c6727/60/inc/spy.h"
#include "fgc3/c6727/inc/dsp.h"
#include "fgc3/inc/common.h"
#include "fgc3/inc/boot_dsp_cmds.h"
#include "fgc3/inc/crate.h"
#include "inc/version.h"



// External variable definitions

float     gReference;
float     gIntegral;
uint32_t  gRegulationEnable;
uint32_t  gRegulationAlgorithm;
uint32_t  gForced_ANA_type;



// Internal function definitions

static void mainCleanUp(void)
{

    gRegulationEnable = 0; // disabled by default
    gRegulationAlgorithm = 0;   // close loop PI, by default
    gReference = 0.0f;
    gIntegral = 0.0f;
    gForced_ANA_type = 0; // by default it is not forced
    gAdcHistory_index = 0;
    gAdcHistory_index_displacement = 0;

    spy.is_running = FALSE;
    spy.local_buffer.bytes_to_send = 0;
    spy.owner = SPY_OWNER_Ana_102_Test_Pulse1;

    wrong_irq_counter          = 0;

    dpram->dsp.irq_counter = 0;
    dpram->mcu.slowCmd = DSP_CMD_NOTHING;
    dpram->mcu.fastCmd = DSP_CMD_NOTHING;

    // initialises the adc interface

    adcInit();

    uint8_t channel;

    for (channel = 0; channel < FGC_N_ADCS; channel++)   // do the 4 ADC channels
    {
        gAdc[channel].count_when_0v = ADC_0v_COUNT;
        gAdc[channel].count_when_10vPos = ADC_10vPOS_COUNT;
        gAdc[channel].count_when_10vNeg = ADC_10vNEG_COUNT;
        /*
                gAdc[channel].range_pos = count_when_10vPos - count_when_0v;
                gAdc[channel].range_neg = count_when_0v - count_when_10vNeg;
        */
        gAdc[channel].range_pos = ADC_10vPOS_COUNT - ADC_0v_COUNT;
        gAdc[channel].range_neg = ADC_0v_COUNT - ADC_10vNEG_COUNT;

        gAdc[channel].physical_value_when_10vPos = 10.0f;
        gAdc[channel].physical_value_when_10vNeg = -10.0f;
    }

    gCurrent_k[3] = 0.0;
    gCurrent_k[2] = 0.0;
    gCurrent_k[1] = 0.0;
    gCurrent_k[0] = 0.0;

}



// External function definitions

void main(void)
{
    bgpInit();

    // Initialize the DSP

    dspInit();

    // Clear the Dual Port RAM, 16 K bytes = 4K_32bit_words. The last word
    // written is the handshake acknowledge expected by the MCU

    memset((void*)dpram, 0, sizeof(*dpram));

    // Write DSP software version number

    dpram->dsp.version = version.unixtime;

    // Use a memory barrier to protect the handshake

    _restore_interrupts(_disable_interrupts());

    dpram->handshake = DSP_LOADER_RUNNING;

    mainCleanUp();

    // Initialise modules: the order of initialization is important

    crateInit();

    // Ready to start the real-time task

    dspInitInterrupts();

    // Start the background processing

    bgpProcess();
}


// EOF

