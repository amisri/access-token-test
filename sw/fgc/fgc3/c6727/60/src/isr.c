/*!
 *  @file      isr.c
 *  @brief     FGC3 DSP interrupt service routines
 */

// Includes

#include <stdint.h>

#include <boot_dual_port_ram.h>

#include "fgc3/c6727/60/inc/isr.h"
#include "fgc3/c6727/inc/dsp.h"

#include <boot_dsp_cmds.h>
#include <dsp_time_consumed.h>
#include <spy.h>
#include <commands.h>
#include <adc.h>
#include <main.h>
#include <c6x.h>
#include <dac_max5541.h>
#include <memmap_dsp.h>



// Constants

//! Integral Gain: default value
#define PI_CONSTANT_GAIN_INTEGRAL       0.01f

//! Proportional Gain: default value
#define PI_CONSTANT_GAIN_PROPORTIONAL   0.125f



// External variable definitions

uint16_t  wrong_irq_counter;



// Internal function definitions

static void isrProcess(void)
{
    float regulation_error;

    // save timer value of previous ISR

    dsp_time_consumed_in.msTickIsr_prev = dsp_time_consumed_in.msTickIsr.stamp_ini;

    // DSP timer counting in us

    dsp_time_consumed_in.msTickIsr.stamp_ini = *(volatile uint32_t *) RTIFRC0;

    // ADC testing

    switch (DSP_MID_ANATYPE_P)
    {
        case FGC_INTERFACE_ANA_103:
        case FGC_INTERFACE_ANA_104:
            adcReadAna103();
            adcTestAna103();
            break;
        case FGC_INTERFACE_ANA_101:
            adcReadAna101();
            adcTestAna101();
            break;
        default:
            break;
    }


    // Check if it is the proper interrupt
    // Interrupt is wait rise: EIRR.WR == 0

    if ((*(uint32_t *)EMIF_EIRR) & 0x04)
    {
        switch (DSP_MID_ANATYPE_P)
        {
            case 0x000CCCC: // ANA_101Identification when Old PLD firmware ToDo A1A2 legacy to be removed
            case FGC_INTERFACE_ANA_103:
            case FGC_INTERFACE_ANA_104:

                StoreSpySampleInLocalBuffer();
                break;
            case FGC_INTERFACE_ANA_101:

                StoreSpySampleInLocalBuffer();
                break;
            default:
                break;
        }

        ExecuteFastCommands();


        // Regulation running

        if (gRegulationEnable != 0)
        {
            switch (gRegulationAlgorithm)
            {
                case 0: // close loop, PI
                    regulation_error = gReference - gCurrent_k[0];

                    gIntegral = gIntegral + PI_CONSTANT_GAIN_INTEGRAL  * regulation_error;

                    SetDAC_1(ConvertCurrentToDACcount(gIntegral + (regulation_error * PI_CONSTANT_GAIN_PROPORTIONAL)));

                    break;

                case 1: // open loop
                    SetDAC_1(ConvertCurrentToDACcount(gReference));
                    break;

                default:                // Go through here when no background activity is wanted
                    break;
            }
        }
        else
        {
            gIntegral = 0.0f;   // Initialise Integral term
        }

        dpram->dsp.irq_counter++;
    }
    else                    //  Other int
    {
        wrong_irq_counter++;

        if (wrong_irq_counter != 0)           // 1 bad irq at the start
        {
            dpram->dsp.status = DSP_STS_BAD_IRQ;    // report this bad interrupt in DSP status
        }
    }

    dsp_time_consumed_in.msTickIsr.stamp_end = *(volatile uint32_t *)RTIFRC0; // DSP timer counting in us

    Calc_time_consumed();

    *(int32_t *) EMIF_EIRR = 0x04;    // Clear the interrupt
}



// External function definitions

void isrRtp(void)
{
    // Acknowledge interrupt

    ACKNOWLEDGE_NMI();

    DISABLE_TICKS();

    // Set the DSP Test Point 0

    SET_TP0();

    // Update the iteration times

  //  iterStart();

    isrProcess();

    // Update the ieration processing time

   // iterEnd();

    // Clear DSP Test Point 0

    CLR_TP0();

    ENABLE_TICKS();
}



void isrTrap(void)
{
    dpram->dbg[0] = 0xBAD0FA17;
}




// EOF
