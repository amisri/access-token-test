/*---------------------------------------------------------------------------------------------------------*\
  File:         adc.c

  Purpose:      Analogue board adc functions

    the ADC is the ADS1274IPAPT

\*---------------------------------------------------------------------------------------------------------*/

#define ADC_GLOBALS                     // for gCurrent_k[] global variable

#include <stdint.h>

#include <adc.h>
#include <memmap_dsp.h>                 // for ANA_MPX_P
#include <defconst.h>                   // for FGC_N_ADCS
#include <boot_dual_port_ram.h>         // for dpram global variable
#ifdef __TMS320C6727__
#include <structs_bits_little.h>
#endif
#include <debug.h>      // for TEST_ADC_PLL_SWEEP
#include <c6x.h>        // for _itof()
#include <spy.h>


extern int   _fixfi(float x);   // float to int, it is in the library
extern float _fltif(int x);     // int to float, it is in the library
// DSP_MID_ANATYPE_P


void adcInit(void)
{
    dpram->adc.n_samples       = 0;
    dpram->adc.is_first_sample = true;
    dpram->adc.reading_on      = false;
    dpram->adc.testing_on      = false;
    dpram->adc.error.detected  = false;
}



void adcReadAna101(void)
{
    uint16_t channel;
    int32_t  adc_raw_data[FGC_N_ADCS];

    if (   dpram->adc.reading_on == true
        && dpram->adc.n_samples  >  0)
    {
        // read ADCs values

        for (channel = 0; channel < FGC_N_ADCS; channel++)
        {
            adc_raw_data[channel]           = ANA_FGC3_ADC_SAMPLE_A[channel];
            dpram->adc.sum[channel].int64s += adc_raw_data[channel];

            if (dpram->adc.is_first_sample == true)
            {
                dpram->adc.max[channel] = adc_raw_data[channel];
                dpram->adc.min[channel] = adc_raw_data[channel];
            }

            if (adc_raw_data[channel] > dpram->adc.max[channel])
            {
                dpram->adc.max[channel] = adc_raw_data[channel];
            }
            else if (adc_raw_data[channel] < dpram->adc.min[channel])
            {
                dpram->adc.min[channel] = adc_raw_data[channel];
            }
        }

        dpram->adc.is_first_sample = false;
        dpram->adc.n_samples--;

        if (dpram->adc.n_samples == 0)
        {
            dpram->adc.reading_on = false;
            return;
        }
    }
}



void adcReadAna103(void)
{
    uint16_t channel, i;
    int32_t  adc_raw_data[FGC_N_ADCS];

    if (   dpram->adc.reading_on == true
        && dpram->adc.n_samples  >  0)
    {
        // read ADCs values

        for (i = 0; i < 50; ++i)
        {
            for (channel = 0; channel < FGC_N_ADCS; channel++)
            {
                adc_raw_data[channel]           = ANA_FGC3_ADC_SAMPLE_A[channel];
                dpram->adc.sum[channel].int64s += adc_raw_data[channel];

                if (dpram->adc.is_first_sample == true)
                {
                    dpram->adc.max[channel] = adc_raw_data[channel];
                    dpram->adc.min[channel] = adc_raw_data[channel];
                }

                if (adc_raw_data[channel] > dpram->adc.max[channel])
                {
                    dpram->adc.max[channel] = adc_raw_data[channel];
                }
                else if (adc_raw_data[channel] < dpram->adc.min[channel])
                {
                    dpram->adc.min[channel] = adc_raw_data[channel];
                }
            }

            dpram->adc.is_first_sample = false;
            dpram->adc.n_samples--;

            if (dpram->adc.n_samples == 0)
            {
                dpram->adc.reading_on = false;
                return;
            }
        }
    }
}



void adcTestAna101(void)
{
    uint16_t channel;
    int32_t  adc_raw_data[FGC_N_ADCS];

    static int32_t const limits[6] = {5600000, 5500000, -5500000, -5600000, 10000, -10000};

    if (   dpram->adc.testing_on == true
        && dpram->adc.n_samples  >  0)
    {
        // read ADCs values

        for (channel = 0; channel < FGC_N_ADCS; channel++)
        {
            adc_raw_data[channel] = ANA_FGC3_ADC_SAMPLE_A[channel];

            if (   adc_raw_data[channel] > limits[dpram->adc.test]
                || adc_raw_data[channel] < limits[dpram->adc.test + 1])
            {
                dpram->adc.error.channel   = channel;
                dpram->adc.error.data      = adc_raw_data[channel];
                dpram->adc.error.limits[0] = limits[dpram->adc.test];
                dpram->adc.error.limits[1] = limits[dpram->adc.test + 1];
                dpram->adc.error.detected  = true;
                dpram->adc.testing_on      = false;
                return;
            }
        }

        dpram->adc.n_samples--;

        if (dpram->adc.n_samples == 0)
        {
            dpram->adc.testing_on = false;
            return;
        }
    }
}



void adcTestAna103(void)
{
    uint16_t channel, i;
    int32_t  adc_raw_data[FGC_N_ADCS];

    static int32_t const limits[6] = {23300000, 22830000, -22830000, -23300000, 10000, -10000};

    if (   dpram->adc.testing_on == true
        && dpram->adc.n_samples  >  0)
    {
        // read ADCs values

        for (i = 0; i < 50; ++i)
        {
            for (channel = 0; channel < FGC_N_ADCS; channel++)
            {
                adc_raw_data[channel] = ANA_FGC3_ADC_SAMPLE_A[channel];

                if (   adc_raw_data[channel] > limits[dpram->adc.test]
                    || adc_raw_data[channel] < limits[dpram->adc.test + 1])
                {
                    dpram->adc.error.channel   = channel;
                    dpram->adc.error.data      = adc_raw_data[channel];
                    dpram->adc.error.limits[0] = limits[dpram->adc.test];
                    dpram->adc.error.limits[1] = limits[dpram->adc.test + 1];
                    dpram->adc.error.detected  = true;
                    dpram->adc.testing_on      = false;
                    return;
                }
            }

            dpram->adc.n_samples--;

            if (dpram->adc.n_samples == 0)
            {
                dpram->adc.testing_on = false;
                return;
            }
        }
    }
}

// EOF
