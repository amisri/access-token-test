/*---------------------------------------------------------------------------------------------------------*\
  File:         time_consumed.c

  Purpose:

  Author:       daniel.calcoen@cern.ch

  Notes:

  History:

    04 dec 07   pfr       created
    22 jun 10   doc       working version

\*---------------------------------------------------------------------------------------------------------*/

#define DSP_TIME_CONSUMED_GLOBALS       // for dsp_time_consumed_in global variable

#include <dsp_time_consumed.h>
#include <boot_dual_port_ram.h>         // for dpram global variable

/*---------------------------------------------------------------------------------------------------------*/
void Calc_time_consumed(void)
/*---------------------------------------------------------------------------------------------------------*\
  all these values are taken inside the same ISR
\*---------------------------------------------------------------------------------------------------------*/
{
    dsp_time_consumed_in.interpolation.actual   = dsp_time_consumed_in.interpolation.stamp_end   -
                                                  dsp_time_consumed_in.interpolation.stamp_ini;
    dsp_time_consumed_in.msTickIsr.actual       = dsp_time_consumed_in.msTickIsr.stamp_end       -
                                                  dsp_time_consumed_in.msTickIsr.stamp_ini;

    if (dsp_time_consumed_in.interpolation.actual > dsp_time_consumed_in.interpolation.max)
    {
        dsp_time_consumed_in.interpolation.max = dsp_time_consumed_in.interpolation.actual;
    }

    if (dsp_time_consumed_in.msTickIsr.actual > dsp_time_consumed_in.msTickIsr.max)
    {
        dsp_time_consumed_in.msTickIsr.max = dsp_time_consumed_in.msTickIsr.actual;
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void Get_time_consumed(void)
/*---------------------------------------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------------------------------------*/
{
    // [us]
    dpram->dsp.slowRsp.get_info.time_consumed_in_msTickIsr = dsp_time_consumed_in.msTickIsr.actual;
    dpram->dsp.slowRsp.get_info.time_consumed_in_interpolation = dsp_time_consumed_in.interpolation.actual;

    // [us]
    dpram->dsp.slowRsp.get_info.time_consumed_in_msTickIsr_max = dsp_time_consumed_in.msTickIsr.max;
    dpram->dsp.slowRsp.get_info.time_consumed_in_interpolation_max = dsp_time_consumed_in.interpolation.max;

    // [us] between the last 2 ISRs
    dpram->dsp.slowRsp.get_info.msTick = dsp_time_consumed_in.msTickIsr.stamp_ini -
                                         dsp_time_consumed_in.msTickIsr_prev;
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: time_consumed.c
\*---------------------------------------------------------------------------------------------------------*/
