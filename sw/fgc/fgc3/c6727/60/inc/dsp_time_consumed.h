/*---------------------------------------------------------------------------------------------------------*\
  File:         dsp_time_consumed.h

  Purpose:      FGC3

  History:

    28 jun 2010 doc     Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DSP_TIME_CONSUMED_H     // header encapsulation
#define DSP_TIME_CONSUMED_H

#ifdef DSP_TIME_CONSUMED_GLOBALS
#define DSP_TIME_CONSUMED_VARS_EXT
#else
#define DSP_TIME_CONSUMED_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

//-----------------------------------------------------------------------------------------------------------

struct TDspTimeConsume
{
    uint32_t      stamp_ini;
    uint32_t      stamp_end;
    uint32_t      actual;
    uint32_t      max;
};

struct TDspTimeConsumeInfo
{
    uint32_t                      msTickIsr_prev;         // timer value of previous isr_start [us]
    struct TDspTimeConsume      msTickIsr;              // [us]
    struct TDspTimeConsume      interpolation;          // [us]
};

//-----------------------------------------------------------------------------------------------------------

void    Calc_time_consumed(void);
void    Get_time_consumed(void);

//-----------------------------------------------------------------------------------------------------------

DSP_TIME_CONSUMED_VARS_EXT struct TDspTimeConsumeInfo           dsp_time_consumed_in;

//-----------------------------------------------------------------------------------------------------------

#endif  // DSP_TIME_CONSUMED_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dsp_time_consumed.h
\*---------------------------------------------------------------------------------------------------------*/

