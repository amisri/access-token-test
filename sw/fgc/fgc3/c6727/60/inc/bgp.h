/*!
 *  @file      bgp.h
 *  @brief     Background processing functions
 */

#pragma once


// Includes


// External function declarations

/*!
 * Initializes the background process.
 */
void bgpInit(void);

/*!
 * This is the Background processing loop.
 */
void bgpProcess(void);


// EOF

