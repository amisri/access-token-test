/*---------------------------------------------------------------------------------------------------------*\
  File:     debug.h

  Purpose:  FGC

  History:

    8 feb 2011  doc Created
\*---------------------------------------------------------------------------------------------------------*/

#ifndef DEBUG_H // header encapsulation
#define DEBUG_H

#ifdef DEBUG_GLOBALS
#define DEBUG_VARS_EXT
#else
#define DEBUG_VARS_EXT extern
#endif

#if (!defined(__RX610__) && !defined(__M16C62__) && !defined(__HC16__)) \
    || defined(__RX610__) || defined(__M16C62__) || defined(__HC16__)
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

// this to encapsulate modifications during debugging
// #define      DEBUGGING_MODIFICATION

// #define TEST_PLL_SWEEP
// #define TEST_ADC_PLL_SWEEP

//-----------------------------------------------------------------------------------------------------------

#endif  // DEBUG_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: debug.h
\*---------------------------------------------------------------------------------------------------------*/
