/*!
 *  @file      main.h
 *  @brief     FGC3 DSP main function
 */

// Includes

#pragma once

#include <stdint.h>



// External variable declarations

extern float     gReference;
extern float     gIntegral;
extern uint32_t  gRegulationEnable;
extern uint32_t  gRegulationAlgorithm;
extern uint32_t  gForced_ANA_type;


// EOF
