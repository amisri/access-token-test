/*---------------------------------------------------------------------------------------------------------*\
  File:     adc.h

  Purpose:  FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef ADC_H   // header encapsulation
#define ADC_H

#ifdef ADC_GLOBALS
#define ADC_VARS_EXT
#else
#define ADC_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <boot_dual_port_ram.h>     // to have access to union TAdcValue
#include <structs_bits_little.h>    // INTEL bits, bytes, words, dwords ...
#include <defconst.h>               // for FGC_N_ADCS

//-----------------------------------------------------------------------------------------------------------

/*
    analogue board ANA-101

    values obtained with the internal 10v reference and PLD prior to version 16, means FIR implemented as a table

    FGC3.0(A)
    for +10v we read  x507 5C1A 3xxx xxxx
    for   0v we read  x000 26B7 Fxxx xxxx
    for -10v we read  xAF9 0470 Fxxx xxxx

    FGC3.0(B)
    for +10v we read  x507 1362 8xxx xxxx
    for   0v we read  xFFF CE50 Axxx xxxx
    for -10v we read  xAF8 A051 6xxx xxxx


#define     ADC_10vPOS_COUNT                        20593.0f   // 0x5071
#define     ADC_0v_COUNT                           -4.0f       // 0xFFFC
#define     ADC_10vNEG_COUNT                       -20599.0f   // 0xAF89

    values obtained with the internal 10v reference and PLD version 20 (means FIR via coefficients, not table)

    for +10v we read  0000 0000 0054 A77D
    for   0v we read  0000 0000 0000 1399
    for -10v we read  FFFF FFFF FFAB 80D4

*/
#define     ADC_10vPOS_COUNT                        5547901.0f
#define     ADC_0v_COUNT                            0.0f
#define     ADC_10vNEG_COUNT                       -5537582.0f
#define     ADC_HISTORY_BUFFER_SIZE                 8

/*
struct TAdcData
{
    union TUnion64Bits  raw;            // direct read from the ADC
    union TUnion64Bits  cut;            // sum of cut from raw value
    union TUnion64Bits  max;            // Max values for each channel
    union TUnion64Bits  min;            // Min values for each channel
    union TUnion64Bits  sum;            // Sum values for each channel
};
*/
struct TAdcData
{
    int32_t             raw;            // direct read from the ADC
    int32_t             max;            // Max values for each channel
    int32_t             min;            // Min values for each channel
    union TUnion64Bits  sum;            // Sum values for each channel

    float                count_when_0v;
    float                count_when_10vPos;
    float                count_when_10vNeg;
    float                physical_value_when_10vPos;
    float                physical_value_when_10vNeg;
    float                range_pos;
    float                range_neg;
};

//-----------------------------------------------------------------------------------------------------------

void adcInit(void);
void adcReadAna101(void);
void adcReadAna103(void);
void adcTestAna101(void);
void adcTestAna103(void);
//-----------------------------------------------------------------------------------------------------------

ADC_VARS_EXT float               gCurrent_k[5];
// allow 8 samples so we can ask for [k-7..0..k+7]
ADC_VARS_EXT int32_t gAdcHistory[FGC_N_ADCS][ADC_HISTORY_BUFFER_SIZE];
ADC_VARS_EXT int32_t            gAdcHistory_index;
ADC_VARS_EXT int32_t            gAdcHistory_index_displacement;
ADC_VARS_EXT struct TAdcData    gAdc[FGC_N_ADCS];

//-----------------------------------------------------------------------------------------------------------

#endif  // ADC_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: adc.h
\*---------------------------------------------------------------------------------------------------------*/
