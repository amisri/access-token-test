// originally bl1_defines.h

//
// $Source: /cvsstl/ti/pa/b/ra1/bl1/inc/bl1_defines.h,v $
// $Revision: 1.4 $
//
// DA7xx Bootloader Definitions
//
// Copyright 2004, Texas Instruments India, Inc.  All rights reserved.
//
// $Log: bl1_defines.h,v $
// Revision 1.4  2004/11/16 09:08:22  dkapoor
// Divij: 14:40 IST 16/11/04
// Modifications after code review
//
// Revision 1.3  2004/10/26 12:29:09  dkapoor
// Divij: 18:00 IST 26/10/04
// Initial version
//
//

#ifndef __BL1_DEFINES_H
#define __BL1_DEFINES_H

#ifdef BL_TEST

#define BL_DEVICE_PIN_REGISTER                   0x00000700
#define BL_DEVICE_BCFGR_REGISTER                 0x00000704
#define BL_ERROR_CODE_REGISTER                   0x00000708
#define BL_STATUS_REGISTER1                      0x0000070C
#define BL_STATUS_REGISTER2                      0x00000710
#define BL_HPI_JUMP_ADDRESS_REGISTER             0x00000714
#define BL_HPI_XFER_DONE_REGISTER                0x00000718

#define BL_CONFIG_REGISTER                       0x00000720

#define BL_BOOTMODE_REGISTER                     0x00000720
#define BL_PERIPHERAL_REGISTER                   0x00000724
#define BL_BITS_PER_SHOT_REGISTER                0x00000728
#define BL_DEVICE_NUMBER_REGISTER                0x0000072C
#define BL_IS_MASTER_MODE                        0x00000730
#define BL_NO_OF_ELEMENTS                        0x00000734
#define BL_WRITE_VALUE                           0x00000738
#define BL_TO_WRITE_SPI                          0x0000073C
#define BL_WRITE_MASK                            0x00000740
#define BL_PERIPHERAL_TIMEOUT_VALUE              0x00000744
#define BL_MASTER_READ_OPCODE_REGISTER           0x00000748
#define BL_FROM_ADDRESS_REGISTER                 0x0000074C
#define BL_TO_ADDRESS_REGISTER                   0x00000750
#define BL_RESERVED1_REGISTER                    0x00000754
#define BL_RESERVED2_REGISTER                    0x00000758
#define BL_RESERVED3_REGISTER                    0x0000075C
#define BL_RESERVED4_REGISTER                    0x00000760
#define BL_RESERVED5_REGISTER                    0x00000764
#define BL_PROTOCOL_CALL_REGISTER                0x00000768
#define BL_UPDATE_CRC_REGISTER                   0x0000076C
#define BL_READ_SYNC_WORD_REGISTER               0x00000770
#define BL_OPEN_FUNCTION_REGISTER                0x00000774
#define BL_READ_FUNCTION_REGISTER                0x00000778
#define BL_WRITE_FUNCTION_REGISTER               0x0000077C
#define BL_COPY_FUNCTION_REGISTER                0x00000780
#define BL_CLOSE_FUNCTION_REGISTER               0x00000784
#define BL_UPDATE_STATE_MACHINE_REGISTER         0x00000788

#define BL_MASTER_CLK_ITERATION_REGISTER         0x0000078C
#define BL_MASTER_CS_SETUP_ITERATION_REGISTER    0x0000078F

#define IRAM_START_ADDRESS                       0x00000000

#define DEVICE_BCFGR_REGISTER                    0x80000000
#define DEVICE_CFGPIN0_REGISTER                  0x80000004
#define DEVICE_CFGPIN1_REGISTER                  0x80000008
#define DEVICE_CFGHPI_REGISTER                   0x8000000C
#define DEVICE_CFGHPIAMSB_REGISTER               0x80000010
#define DEVICE_CFGHPIAUMB_REGISTER               0x80000014
#define DEVICE_CFGBRIDGE_REGISTER                0x80000018
#define DEVICE_ASYNC_CONFIG_REG1_ASIZE_REGISTER  0x8000001C

#else

#define BL_DEVICE_PIN_REGISTER                   0x10000700
#define BL_DEVICE_BCFGR_REGISTER                 0x10000704
#define BL_ERROR_CODE_REGISTER                   0x10000708
#define BL_STATUS_REGISTER1                      0x1000070C
#define BL_STATUS_REGISTER2                      0x10000710
#define BL_HPI_JUMP_ADDRESS_REGISTER             0x10000714
#define BL_HPI_XFER_DONE_REGISTER                0x10000718

#define BL_CONFIG_REGISTER                       0x10000720

#define BL_BOOTMODE_REGISTER                     0x10000720
#define BL_PERIPHERAL_REGISTER                   0x10000724
#define BL_BITS_PER_SHOT_REGISTER                0x10000728
#define BL_DEVICE_NUMBER_REGISTER                0x1000072C
#define BL_IS_MASTER_MODE                        0x10000730
#define BL_NO_OF_ELEMENTS                        0x10000734
#define BL_WRITE_VALUE                           0x10000738
#define BL_TO_WRITE_SPI                          0x1000073C
#define BL_WRITE_MASK                            0x10000740
#define BL_PERIPHERAL_TIMEOUT_VALUE              0x10000744
#define BL_MASTER_READ_OPCODE_REGISTER           0x10000748
#define BL_FROM_ADDRESS_REGISTER                 0x1000074C
#define BL_TO_ADDRESS_REGISTER                   0x10000750
#define BL_RESERVED1_REGISTER                    0x10000754
#define BL_RESERVED2_REGISTER                    0x10000758
#define BL_RESERVED3_REGISTER                    0x1000075C
#define BL_RESERVED4_REGISTER                    0x10000760
#define BL_RESERVED5_REGISTER                    0x10000764
#define BL_PROTOCOL_CALL_REGISTER                0x10000768
#define BL_UPDATE_CRC_REGISTER                   0x1000076C
#define BL_READ_SYNC_WORD_REGISTER               0x10000770
#define BL_OPEN_FUNCTION_REGISTER                0x10000774
#define BL_READ_FUNCTION_REGISTER                0x10000778
#define BL_WRITE_FUNCTION_REGISTER               0x1000077C
#define BL_COPY_FUNCTION_REGISTER                0x10000780
#define BL_CLOSE_FUNCTION_REGISTER               0x10000784
#define BL_UPDATE_STATE_MACHINE_REGISTER         0x10000788

#define BL_MASTER_CLK_ITERATION_REGISTER         0x1000078C
#define BL_MASTER_CS_SETUP_ITERATION_REGISTER    0x1000078F

#define IRAM_START_ADDRESS                       0x10000000

#define DEVICE_BCFGR_REGISTER                    0x2000000C
#define DEVICE_CFGPIN0_REGISTER                  0x40000000
#define DEVICE_CFGPIN1_REGISTER                  0x40000004
#define DEVICE_CFGHPI_REGISTER                   0x40000008
#define DEVICE_CFGHPIAMSB_REGISTER               0x4000000C
#define DEVICE_CFGHPIAUMB_REGISTER               0x40000010
#define DEVICE_CFGBRIDGE_REGISTER                0x40000024
#define DEVICE_GPIOGCR0_REGISTER                 0x60000000
#define DEVICE_GPIODIR0_REGISTER                 0x60000030

#define DEVICE_ASYNC_CONFIG_REG1_ASIZE_REGISTER  0xF0000010

#endif

// Begin Code added by KSB

#define DMAX_PROGRAM_SIZE                        0x00000400
#define DMAX_DATARAM_SIZE                        0x00000400

#define DMAX0_PROGRAM_START_ADDRESS              0x61000000
#define DMAX0_DATARAM_START_ADDRESS              0x61008000
#define DMAX0_ROM_CODE_ADDRESS                   0x00010000



#define DMAX1_PROGRAM_START_ADDRESS              0x62000000
#define DMAX1_DATARAM_START_ADDRESS              0x62008000
#define DMAX1_ROM_CODE_ADDRESS                   0x00018000

#define HDMAX0_ROM_CODE_ADDRESS                  ((unsigned int *)DMAX0_ROM_CODE_ADDRESS)
#define HDMAX1_ROM_CODE_ADDRESS                  ((unsigned int *)DMAX1_ROM_CODE_ADDRESS)
#define HDEVICE_DMAX0_DATARAM_ADDRESS            ((unsigned int *)DMAX0_DATARAM_START_ADDRESS)
#define HDEVICE_DMAX0_PROGRAM_ADDRESS            ((unsigned int *)DMAX0_PROGRAM_START_ADDRESS)
#define HDEVICE_DMAX1_DATARAM_ADDRESS            ((unsigned int *)DMAX1_DATARAM_START_ADDRESS)
#define HDEVICE_DMAX1_PROGRAM_ADDRESS            ((unsigned int *)DMAX1_PROGRAM_START_ADDRESS)
#define HDEVICE_CFGBRIDGE_REGISTER               ((unsigned int *)DEVICE_CFGBRIDGE_REGISTER)
#define HDEVICE_GPIOGCR0_REGISTER                ((unsigned int *)DEVICE_GPIOGCR0_REGISTER)
#define HDEVICE_GPIODIR0_REGISTER                ((unsigned int *)DEVICE_GPIODIR0_REGISTER)

// End Code added by KSB

#define HBL_DEVICE_PIN_REGISTER                  ((unsigned int *)BL_DEVICE_PIN_REGISTER)
#define HBL_DEVICE_BCFGR_REGISTER                ((unsigned int *)BL_DEVICE_BCFGR_REGISTER)
#define HBL_BOOTMODE_REGISTER                    ((unsigned int *)BL_BOOTMODE_REGISTER)

#define HDEVICE_BCFGR_REGISTER                   ((unsigned int *)DEVICE_BCFGR_REGISTER)
#define HDEVICE_CFGPIN0_REGISTER                 ((unsigned int *)DEVICE_CFGPIN0_REGISTER)
#define HDEVICE_CFGPIN1_REGISTER                 ((unsigned int *)DEVICE_CFGPIN1_REGISTER)
#define HDEVICE_ASYNC_CONFIG_REG1_ASIZE_REGISTER ((unsigned char *)DEVICE_ASYNC_CONFIG_REG1_ASIZE_REGISTER)

#define HBL_STATUS_REGISTER1                     ((unsigned int *)BL_STATUS_REGISTER1)
#define HBL_STATUS_REGISTER2                     ((unsigned int *)BL_STATUS_REGISTER2)

#define PF_START_ADDRESS                       0x90000000

#define SPI_READ_COMMAND                       0x3

#define BL_PROTOCOL_XMT_START32                (unsigned int)0x58535441     /* XSTA */
#define BL_PROTOCOL_RECV_START32               (unsigned int)0x52535454     /* RSTT */

#define BL_PROTOCOL_XMT_SYNC                   (unsigned int)0x58535900     /* XSY */
#define BL_PROTOCOL_RECV_SYNC                  (unsigned int)0x52535900     /* RSY */

/* BootLoader1 Parameters */
#define BL_PROTOCOL_CRC_ERROR_TOLERENCE        2

/* BootLoader1 Protocol Sequences */
#define BL_PROTOCOL_SECTION_LOAD               1
#define BL_PROTOCOL_REQUEST_CRC                2
#define BL_PROTOCOL_ENABLE_CRC                 3
#define BL_PROTOCOL_DISABLE_CRC                4
#define BL_PROTOCOL_JUMP                       5
#define BL_PROTOCOL_JUMP_CLOSE                 6
#define BL_PROTOCOL_BOOT_TABLE                 7
#define BL_PROTOCOL_STARTOVER                  8
#define BL_PROTOCOL_COMPSECTION_LOAD           9
#define BL_PROTOCOL_SECTION_FILL              10
#define BL_PROTOCOL_PING_DEVICE               11

#define BL_PROTOCOL_MAX_OPCODES               20

/* Error Codes */
typedef enum _BOOT_Error_Codes
{
    BL_INCORRECT_KEYWORD,
    BL_XMT_SYNC_ERROR,
    BL_CRC_ERROR,
    BL_UNREACHABLE_CODE_ERROR,
    BL_INCORRECT_BOOT_MODE,
    BL_UNSUPPORTED_BOOT_MODE,
    BL_BLOCKED_BOOT_MODE,
    BL_INCORRECT_BCFGR_VALUE
} BOOT_Error_Codes;

typedef enum _BOOT_Mode
{
    IROM = 0,
    SPIMASTER16,
    PARALLELFLASH,
    SPISLAVE16,
    RESERVED1,
    I2CMASTER16,
    RESERVED2,
    I2CSLAVE8,
    HPI
} BOOT_Mode;

typedef enum _BOOT_Peripheral
{
    BOOT_PERIPHERAL_NONE,
    BOOT_PERIPHERAL_HPI,
    BOOT_PERIPHERAL_EMIF,
    BOOT_PERIPHERAL_SPI,
    BOOT_PERIPHERAL_I2C
} BOOT_Peripheral;

typedef enum _BOOT_Table_Type
{
    BOOT_TABLE_TYPE_Byte,
    BOOT_TABLE_TYPE_Short,
    BOOT_TABLE_TYPE_Int,
    BOOT_TABLE_TYPE_Field,
    BOOT_TABLE_TYPE_Bits
} BOOT_Table_Type;

typedef enum _BOOT_Type
{
    BOOT_TYPE_MASTER,
    BOOT_TYPE_SLAVE
} BOOT_Type;

typedef enum _BOOT_Section_Fill_Type
{
    PATTERN_TYPE_08_BIT,
    PATTERN_TYPE_16_BIT,
    PATTERN_TYPE_32_BIT
} BOOT_Section_Fill_Type;

typedef enum _BL_State
{
    BL_MST_KEYWORD_CHECK = 1,
    BL_SLAVE_STARTWORD_XCHANGE,
    BL_XMT_SYNC_PARSE,
    BL_SECTION_LOAD,
    BL_CRC_REQ_READ,
    BL_CRC_REQ_CRC_XMT,
    BL_CRC_DISABLE,
    BL_CRC_ENABLE,
    BL_JUMP_INVOKE,
    BL_JUMP_CLOSE_INVOKE,
    BL_STARTOVER_RESET_STATE_VARIABLES,
    BL_BOOT_TABLE,
    BL_COMPSECTION_LOAD,
    BL_SECTION_FILL,
    BL_PING_DEVICE,
    BL_UPDATE_STATE_MACHINE
} Bl_State;

#endif
