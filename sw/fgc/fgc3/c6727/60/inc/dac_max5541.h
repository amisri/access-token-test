/*---------------------------------------------------------------------------------------------------------*\
  File:         dac_max5541.h

  Purpose:      FGC3

\*---------------------------------------------------------------------------------------------------------*/

#ifndef DAC_MAX5541_H   // header encapsulation
#define DAC_MAX5541_H

#ifdef DAC_MAX5541_GLOBALS
#define DAC_MAX5541_VARS_EXT
#else
#define DAC_MAX5541_VARS_EXT extern
#endif

//-----------------------------------------------------------------------------------------------------------

#include <stdint.h>

//-----------------------------------------------------------------------------------------------------------

/*
  the DAC is the MAX5541CSA, 16 bits

  the DAC is unipolar (0 to 2.5v) but in the board the 0v is shifted to the middle of the range
  so we end with [+1.25,-1.25] then multiplied by 10 with OpAmps

  1) First implementation, 16 bits

     FFFF    max positive (+12.5v)
     E665    +10v                  .... +50A
     8000    +0v
     7FFF    -0v
     1999    -10v                  .... -50A
     0000    max negative (-12.5v)

     but the PLD inverts the MSB so from the MCU we see

     7FFF    max positive (+12.5v)
     6665    +10v                  .... +50A
     0000    +0v
     FFFF    -0v
     9999    -10v                  .... -50A
     8000    max negative (-12.5v)


    2'scom  inMeM    PLD(Not b15)     True dac
    +7FFF   7FFF     FFFF              FFFF    max positive (+12.5v)
    +6665   6665     E665              E665    +10v                  .... +50A
    +0000   0000     8000              8000    +0v
    -0001   FFFF     7FFF              7FFF    -0v
    -6666   999A     199A              199A    -10v                  .... -50A
    -7FFF   8000     0000              0000    max negative (-12.5v)

    for the end user looks like

     7FFF    max positive (+12.5v)
     6665   +10v                  .... +50A
     0000    +0v
     FFFF    -0v
     9999   -10v                  .... -50A
     8000    max negative (-12.5v)

  2) Second implementation, 4 bits extension with "PWM"

  the DAC is 16 bits but PLD uses a kind of "PWM" so it handles "20bits"
  (the 32bits container is left shifted by 12 in the DSP software)

    7FFFF   +12.5v  +524287.0f  max positive,
    66665   +10.0v  +419429.0f  used for calibration (verified linear range)
    00000    +0.0v
    FFFFF    -0.0v
    99999   -10.0v              used for calibration (verified linear range)
    80000   -12.5v              max negative
*/

#define DAC_HIGHEST_COUNT           +419429.0f  // 0x66665
#define DAC_0v_COUNT                0.0f        // 0x00000

#define DAC_MAX_POSITIVE_COUNT      +524287.0f  // 0x7FFFF
#define DAC_MAX_POSITIVE_VOLTAGE    12.5f

//  this is exclusive for Acapulco, Cancun with its +50,-50 range for DAC [+10v,-10v]
#define DAC_PHYSICAL_TOP_VALUE      50.0f

//-----------------------------------------------------------------------------------------------------------

int32_t  ConvertCurrentToDACcount(float current);
void    SetDAC_1(uint32_t raw_value);
void    SetDAC_2(uint32_t raw_value);

//-----------------------------------------------------------------------------------------------------------

#endif  // DAC_MAX5541_H end of header encapsulation
/*---------------------------------------------------------------------------------------------------------*\
  End of file: dac_max5541.h
\*---------------------------------------------------------------------------------------------------------*/
