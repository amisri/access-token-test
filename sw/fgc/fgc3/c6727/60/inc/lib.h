/*---------------------------------------------------------------------------------------------------------*\
  File:         lib.h

  Purpose:
\*---------------------------------------------------------------------------------------------------------*/

#ifndef LIB_H   // Header encapsulation
#define LIB_H

#ifdef LIB_GLOBALS
#define LIB_VARS_EXT
#else
#define LIB_VARS_EXT extern
#endif

/*---------------------------------------------------------------------------------------------------------*/

#include <stdint.h>
#include <defconst.h>   // for FGC_MAX_DIM_BUS_ADDR
#include <string.h>     // for size_t

/*---------------------------------------------------------------------------------------------------------*/

struct TDspDebug
{
    uint32_t        buf[FGC_LOG_DIAG_DBUG_LEN];           // DIAG.DEBUGLOG
    uint32_t        idx;                                  // DIAG.DEBUGIDX
    uint32_t        samples_to_acq;                       // Debug samples to acquire
    uint32_t        stop_log;                             // Debug log stopping flag
};

/*---------------------------------------------------------------------------------------------------------*/

void            InitDsp(void);
void            InitLib(uint32_t n_props, uint32_t max_user, uint32_t ppm_start, uint32_t ppm_length);
void            DebugMemTransfer(void);
bool         BgpPropComms(void);
void            interruptible_memcpy(void * _s1, const void * _s2, size_t _n);
/*---------------------------------------------------------------------------------------------------------*/

LIB_VARS_EXT struct TDspDebug           dsp_debug;

/*---------------------------------------------------------------------------------------------------------*/
#endif  // End of header encapsulation LIB_H
/*---------------------------------------------------------------------------------------------------------*\
  End of file: lib.h
\*---------------------------------------------------------------------------------------------------------*/
