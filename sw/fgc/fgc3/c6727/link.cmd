// ----------------------------------------------------------------------------
//  File:       link.cmd
//  Purpose:    DSP Program Linker file for FGC3 Hardware
// ----------------------------------------------------------------------------

-e _c_int00       // Command line : --entry_point _c_int00 inside rts6700.lib
-heap   400h      // Command line : --heap_size  0x400
-stack 1000h      // Command line : --stack_size 0x1000


// ----------------------------------------------------------------------------
//  Specify the Memory Configuration
// ----------------------------------------------------------------------------


MEMORY
{
    // 384 Kb: Internal ROM

    IROM_BOOT     (RX)   : origin = 00000000h,   length = 00020000h
    IROM_DSPLIB   (RX)   : origin = 00020000h,   length = 0000C000h
    IROM_FASTRTS  (RX)   : origin = 0002C000h,   length = 00004000h
    IROM_BIOS     (RX)   : origin = 00030000h,   length = 00030000h

    // 256 Kb: Internal RAM (no cache in use)

    IRAM_BOOT     (RWXI) : origin = 10000000h,   length = 000003FCh    // Boot program
    IRAM_START    (RWXI) : origin = 100003FCh,   length = 00000004h    // Entry point function for the Boot or Main
    IRAM_VEC      (RWXI) : origin = 10000400h,   length = 00000200h
    IRAM          (RWXI) : origin = 10000600h,   length = 0003fa00h

    // The SDRAM memory areas must coincide with the DSP memory definition 
    // of the SDRAM in fgc/def/src/platforms/fgc3/memmaps_inc/dsp/sdram.yaml
    //  64.0 Mb: External SDRAM (CE0):
    //   4.5 MB: code + data
    //   1.2 MB: REGFGC3 binary program
    //  58.3 MB: LOG signals from the liblog library

    SDRAM_EXT     (RWXI) : origin = 80000000h,   length = 00480000h
    SDRAM_PROG    (RWXI) : origin = 80480000h,   length = 00133334h
    SDRAM_LOG     (RWXI) : origin = 805B3334h,   length = 03A4CCCCh

    //  16 Kb: External DPRAM (CE1)

    DPRAM         (RWXI) : origin = 90000000h,   length = 00004000h

    //  32 Kb: External expansion range

    ExtCE2        (RWXI) : origin = 90200000h,   length = 00008000h    // Expansion CE2-space
    ExtCE3        (RWXI) : origin = 90280000h,   length = 00008000h    // Expansion CE3-space
    ExtCE4        (RWXI) : origin = 90300000h,   length = 00008000h    // Expansion CE4-space
}


// ----------------------------------------------------------------------------
//  Specify the Output Sections
// ----------------------------------------------------------------------------

SECTIONS
{
    // The boot loader needs to have a destination and an entry point in the
    // first 16 bytes this is done in start.asm which defines dspStartSection
    // with that information

    .dspStartSection	> IRAM_START
    {
	   *(.dspStartSection)
	   code_destination = .;                                           // Get the destination as a global variable
    }

    .vector:       load > IRAM_VEC
    {
	   *(.vector)
    }

    .text:         load > IRAM
    {
    	*(.text)
    }

    .ext_data:     load > SDRAM_EXT
    {

    }

    .ext_code:     load > SDRAM_EXT
    {

    }

    .ext_log:      load > SDRAM_LOG
    {

    }

    .dpram_area:   load > DPRAM
    {

    }

    GROUP (NEARDP)
    {
       .bss
       {
           	*(.bss)
       }
       .int_far
       {
                *(.far)
       }
    } > IRAM

    .cinit:        load > IRAM
    .pinit:        load > IRAM
    .cio:          load > IRAM
    .const:	       load > IRAM
    .data:         load > IRAM
    .switch:       load > IRAM
    .sysmem:       load > IRAM
    .tables:       load > IRAM
    .stack:        load > IRAM , fill = 0x0BADBABE
}

// EOF
