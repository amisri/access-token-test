//! @file  sig_structs.h
//! @brief Proxy to include the class specific sigStruct.h header file

#pragma once


// ---------- Includes

#include <definfo.h>

#include "fgc3/c6727/inc/calibration.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/inc/dpcom.h"


#if (FGC_CLASS_ID == 62)

#include "inc/classes/62/sigStructs.h"
#include "inc/classes/62/sigStructsInit.h"

#elif (FGC_CLASS_ID == 63)

#include "inc/classes/63/sigStructs.h"
#include "inc/classes/63/sigStructsInit.h"

#elif (FGC_CLASS_ID == 64)

#include "inc/classes/64/sigStructs.h"
#include "inc/classes/64/sigStructsInit.h"

#elif (FGC_CLASS_ID == 65)

#include "inc/classes/65/sigStructs.h"
#include "inc/classes/65/sigStructsInit.h"

#elif (FGC_CLASS_ID == 66)

#include "inc/classes/66/sigStructs.h"
#include "inc/classes/66/sigStructsInit.h"

#else

#error "FGC_CLASS_ID is not defined"

#endif


// EOF
