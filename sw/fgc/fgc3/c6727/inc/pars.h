//! @file  pars.h
//! @brief Pars function invoked when the property is set

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>



// ---------- External function declarations

//! Updates the parameter group set mask.
//!
//! @param[in]   pars_idx        Parameter group index.

void parsUpdateMask(uint32_t const pars_idx);

//! Checks if a property group must be updated
//!
//! @retval      true            A property group was modified.
//! @retval      false           A property group was not modified.

bool parsGroupSet(void);


//! Run all pars functions.
//!
//! This function is called after the properties have been recovered from NVS.
//! This is necessary for devices with no config manager where set commands 
//! are not explicitly issue and as result the pars functions would not be 
//! called otherwise.

void parsRunAllFunc(void);


// EOF
