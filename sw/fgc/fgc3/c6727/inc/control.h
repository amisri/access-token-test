//! @file  control.h
//! @brief Converter control functions

#pragma once


// ---------- Includes

#include <stdbool.h>



// ---------- Platform/class specific function declarations

//! Class specific initialization of the converter control.
//!
//! @retval      true            The initialization was successful.
//! @retval      false           A parameter pointer was not set.

bool controlClassInit(void);


//! Class specific real-time processing of the converter control.

void controlClassRtp(void);


//! Class specific background processing of the converter control.

void controlClassBgp(void);


//! Class specific initialisation of actuation destination

void controlsClassSetActDest(void);



// ---------- External function declarations

//! Initialization of the converter control.

void controlInit(void);


//! This function processes in real-time an iteration of the converter control:
//! function generation, regulation, ADC acquisition and DAC setting, etc.

void controlRtp(void);


//! This function processes the background task of the converter control.

void controlBgp(void);


// EOF
