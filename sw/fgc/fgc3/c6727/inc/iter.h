//! @file  iter.h
//! @brief Functionality processed at each iteration

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <cclibs.h>



// ---------- External structures, unions and enumerations


//! Structure with variables updated at the beginning of each iteration

struct Iter
{
    // Time and watchdog

    struct CC_ms_time   time_ms;                        //!< Time with millisecond resolution
    struct CC_us_time   time_us;                        //!< Time with microsecond resolution
    struct CC_ns_time   time_ns;                        //!< Time with nanosecond resolution

    uint32_t            counter;                        //!< Counter
    uint32_t            state_op;                       //!< Cached copy of dpcom.mcu.state_op

    bool                start_of_ms1_f;                 //!< Flag for the start of a new millisecond
    bool                start_of_ms5_f;                 //!< Flag for the start of   5 ms
    bool                start_of_ms10_f;                //!< Flag for the start of  10 ms
    bool                start_of_ms20_f;                //!< Flag for the start of  20 ms
    bool                start_of_ms200_f;               //!< Flag for the start of 200 ms
    bool                start_of_s1_f;                  //!< Flag for the start of   1 second
    bool                start_of_s10_f;                 //!< Flag for the start of  10 seconds

    cc_float            sig_cpu_usage_us;               //!< DSP RT cpu signal (us)
    cc_float            sig_cpu_usage_offset;           //!< DSP RT cpu signal offset (100 us)
};



// ---------- External variable declarations

extern struct Iter  iter;



// ---------- External function declarations

//! Initializes the iteration.

void iterInit(void);


//! Invoked at the begining of the iteration to update the iteration timing.

void iterStart(void);


//! Invoked at the end of the iteration to update the iteration processing time
//! and check for iteration overruns.

void iterEnd(void);


//EOF
