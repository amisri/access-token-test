//! @file  utils.h
//! @brief Miscellaneous functions

#pragma once


// ---------- Includes

#include <string.h>



// ---------- External function declarations

//! The standard library memcpy is not interruptible on C67x. This can cause
//! problems with longer than expected critical sections in the background
//! task.
//!
//! @param[out]  dest            Destination to copy to.
//! @param[in]   src             Source to copy from.
//! @param[in]   n               Size in bytes to copy.

void interruptibleMemcpy(void * dest, void const * src, size_t const n);



// ---------- External function definitions

//! These macro functions are not inline functions because often FPGA registers
//! (type REG16U or REG32U) are set or clear. This causes a compilation error
//! unless the argument is converted to uint32_t *.


//! Sets a bit mask in a value

#define setBitmap(value, bit_mask)       ((value) |= (bit_mask))

//! Clears the bit mask from a value

#define clrBitmap(value, bit_mask)       ((value) &= ~(bit_mask))

//! Tests if at least one bit in the bit mask is set in value

#define testBitmap(value, bit_mask)      (!!((value) & (bit_mask)))

//! Test if all bits in bit mask are set in a value.

#define testAllBitmap(value, bit_mask)   (((value) & (bit_mask)) == (bit_mask))


// EOF
