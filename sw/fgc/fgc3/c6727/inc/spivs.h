//! @file  spivs.h
//! @brief Interface to the SPIVS bus
//!
//!        The SPIVS is a Serial Protocol Interface whereby each long word
//!        sent implies a log word received and vice-versa. A long word is
//!        defined as 32 bits.
//!
//!        Currently, only one-shot communication is implemented. If
//!        continuous communication is required, supporting code must
//!        be added.

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External structures, unions and enumerations

//! Possible result values of SPIVS-related operations.

enum Spivs_result
{
    SPIVS_OK,         //!< The Tx/Rx operation succeeded.
    SPIVS_BUSY,       //!< A Tx/Rx operation was already in progress.
    SPIVS_INVALID,    //!< Tx and Rx values are not the same.
};


// This structure is public so that read_0 can be logged for debugging

struct Spivs
{
    bool      sync;                    //!< If true, the tranmission is synchornized with the SPIVS
    float     sent_0;                  //!< First value received from the SPI as a float
    float     received_0;              //!< First value sent to the SPI as a float
    float     received_time_offset;    //!< Time offset for received_0 signal (2 iters)
};



// ---------- External variable declarations

extern struct Spivs spivs;



// ---------- External function declarations

//! Initializes the SPIVS interface.
//!
//! @param[in]   sync         If true, the tranmission is synchornized with the SPIVS tick

void spivsInit(bool sync);


//! Sends up to 7 words to the SPIVS interface.
//!
//! If length is larger than 7, the data is silently trimmed. The
//! received data are disregarded.
//!
//! @param[in]   data         Buffer with the data to send.
//! @param[in]   length       Number of words to send.
//!

void spivsSend(uint32_t const * data, uint8_t length);


//! Reads up to 7 words from the SPIVS rx buffer.
//!
//! A previous transmission must have been triggered to read the values.
//! If length is larger than 7, the data is silently trimmed.
//!
//! @param[out]  data         Buffer containing the read data.
//! @param[in]   length       Number of words to read.
//!
//! @retval      SPIVS_OK     Transmission successful.
//! @retval      SPIVS_BUSY   An ongoing transmission is already active.

void spivsRead(uint32_t * const data, uint8_t const length);


//! Validates that the transmitted and received values are the same and updates
//! the SPIVS bookkeeping accordingly.
//!
//! @param[out]  data         Address where the word received is written into.

//! @param[in]   tx_value     Pointer to array of values transmitted.
//! @param[in]   rx_value     Pointer to array of values received.
//! @param[in]   num_validate Number of values to validate.
//!
//! @retval      SPIVS_OK     Validation correct.
//! @retval      SPIVS_INVALID Validation failed.

enum Spivs_result spivsValidate(uint32_t const * tx_value,
                                uint32_t const * rx_value,
                                uint8_t  const   num_validate);


//! Wait for the transmission of a number of words to complete
//!
//! @param[in]   length       Number of words to wait for.
//!
//! @retval      SPIVS_OK     Bus if free.
//! @retval      SPIVS_BUSY   An ongoing transmission is active.

enum Spivs_result spivsWait(uint8_t const length);


//! Test function to validate the transmission of data over the SPIVS bus.
//!
//! @retval      SPIVS_OK     Test succeeded.
//! @retval      SPIVS_INVALID Test failed.

enum Spivs_result spivsTest(void);


// EOF
