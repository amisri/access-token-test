//! @file  calibration.h
//! @brief FGC3 analogue calibration: ADCs and DACs

#pragma once


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>



// ---------- Constants

//! DAC raw calibration value (~8V)

#define CALIBRATION_DAC_REF        367000

//! Number of samples to acquire.

#define CALIBRATION_NUM_SAMPLES    10000



// ---------- External function declarations

//! Initialze the analogue calibration.

void calibrationInit(void);


//! Processes a calibration request (ADCs, DACs or DCCTs) originating
//! either in the MCU or the DSP itself.

void calibrationBgp(void);


//! Returns the calibration status.
//!
//! @retval      true            A calibration is active.
//! @retval      false           A calibration is not active.

bool calibrationIsActive(void);


//! Returns the calibration action.
//!
//! @retval      CAL_REQ_INTERNAL_ADC   Calibrating the ADCs.
//! @retval      CAL_REQ_EXTERNAL_ADC   Calibrating the ADCs.
//! @retval      CAL_REQ_DCCT           Calibrating the DCCTs.
//! @retval      CAL_REQ_DAC            Calibrating the DACs.
//! @retval      CAL_REQ_NONE           No ongoing calibration.

enum Dpcom_cal_action calibrationGetAction(void);


// EOF
