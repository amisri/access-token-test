//! @file  anaCard.h
//! @brief FGC3 analogue card interface (ADCs, DAC, crossbar)


#pragma once


// ---------- Includes

#include <stdint.h>

#include <defconst.h>

#include "fgc3/c6727/inc/signals.h"



// ---------- External structures, unions and enumerations

//! Analogue card type

enum Ana_card_type
{
    ANA_CARD_NO_CARD  = FGC_INTERFACE_NO_CARD,
    ANA_CARD_TYPE_101 = FGC_INTERFACE_ANA_101,
    ANA_CARD_TYPE_103 = FGC_INTERFACE_ANA_103,
    ANA_CARD_TYPE_104 = FGC_INTERFACE_ANA_104,
};


//! Analogue bus input selector.
//!
//! On FGC3 the analogue bus is always connected to the COM port on the
//! front panel, and the later can always be used to spy whatever signal
//! is connected to the analogue bus. Yet there can be use-cases were we
//! need the COM port to be used to measure an input signal. The enum value
//! ANA_INPUT_COM was added for that purpose. And it is also the reason
//! why ANA_INPUT_NONE is a different enum.
//! The syymbol list "INTERNAL_ADC_MPX" MUST be aligned with this enum.

enum Ana_card_crossbar_input
{
    ANA_CARD_INPUT_NONE,                      //!< The nominal case: no signal goes to the analogue bus
    ANA_CARD_INPUT_COM,                       //!< Use the COM port (on the front panel) as an input
    ANA_CARD_INPUT_CH_1,                      //!< Input channel 1
    ANA_CARD_INPUT_CH_2,                      //!< Input channel 2
    ANA_CARD_INPUT_CH_3,                      //!< Input channel 3
    ANA_CARD_INPUT_CH_4,                      //!< Input channel 4
    ANA_CARD_INPUT_MPX_RANGE_START,
    ANA_CARD_INPUT_CH1_MINUS = ANA_CARD_INPUT_MPX_RANGE_START,
    ANA_CARD_INPUT_CH2_MINUS,
    ANA_CARD_INPUT_DAC2,                      //!< DAC 2
    ANA_CARD_INPUT_DAC1,                      //!< DAC 1
    ANA_CARD_INPUT_UNUSED,                    //!< In fact this is a redundant GNDSENSE
    ANA_CARD_INPUT_CAL_ZERO,                  //!<   0V, a.k.a. GNDSENSE
    ANA_CARD_INPUT_CAL_POSREF,                //!< +10V
    ANA_CARD_INPUT_CAL_NEGREF,                //!< -10V
    ANA_CARD_INPUT_MAX_VALUE                  //!< Not a valid input, used for range check
};



// ---------- External function declarations

//! Initialze the analogue calibration.

void anaCardInit(void);


//! Returns the analogue card type.
//!
//! @retval      ANA_CARD_TYPE_101 If ana card ANA_101 present.
//! @retval      ANA_CARD_TYPE_103 If ana card ANA_103 present.

enum Ana_card_type anaCardGetType(void);


//! Outputs a raw value to the DAC.
//!
//! @param[in]   idx             DAC index (DAC_1 or DAC_2).
//! @param[in]   raw_value       Raw value to write to the DAC.

void anaCardSetDacRaw(enum Signals_dac const idx,
                      int32_t          const raw_value);


//! This function processes a request to chagne the analogue muliplexer triggered
//! by the property ADC.INTERNAL.MPX.

void anaCardProcessMpxRequest(void);

//! Connect zero or one input signal with a subset of the ADCs via the crossbar.
//!
//! @param[in]   input           The input to connect to the analogue bus.
//! @param[in]   adcs_mask       Mask of the ADCs to connect to the crossbar.

void anaCardConfigCrossbar(enum Ana_card_crossbar_input const input,
                           uint8_t                      const adcs_mask);


//! This function updates the following properties: ADC.INTERNAL.MPX, ADC.ANALOG_BUS.SIGNAL,
//! ADC.ANALOG_BUS.SWIN and ADC.ANALOG_BUS.SWBUS according to the current state of the hardware.

void anaCardUpdateProperties(void);


//! Saves the current state of the analogue card crossbar.

void anaCardSaveState(void);


//! Restores the state of the analogue card crossbar.

void anaCardRestoreState(void);


// EOF
