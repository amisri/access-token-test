//! @file  isr.h
//! @brief FGC3 DSP interrupt service routines

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- Constants

//! ISR rate: 10 KHz

static uint32_t const ISR_RATE_HZ   = 10000;

//! ISR period: 100,000 nanoseconds*/

static uint32_t const ISR_PERIOD_NS = 100000;



// ---------- External function declarations

//! This function is the Interrupt Service Routine for the CLK_DSP external
//! interrupt request. This will be triggered at the start of each DSP
//! regulation period.

#pragma NMI_INTERRUPT (isrRtp);
void isrRtp(void);


//! This function is used as the target for all unexpected interrupts.

#pragma INTERRUPT (isrTrap);
void isrTrap(void);


//! This function is run in background.

void isrBgp(void);


// EOF
