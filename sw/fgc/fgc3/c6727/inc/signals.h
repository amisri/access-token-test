//! @file  signals.h
//! @brief Functionality related to signal processing
//!
//!        ADC, DAC, DCCT calibrations, multiplexing of the analogue bus and libSig library.

#pragma once


// ---------- Constants

//! Constant used in sigStructsInit.h.

static uint32_t const ADC_STUCK_LIMIT = 10;



// ---------- Includes

#include <stdint.h>

#include "fgc3/c6727/inc/sig_structs.h"



// ---------- External structures, unions and enumerations

//! DAC index

enum Signals_dac
{
    SIGNALS_DAC_1,
    SIGNALS_DAC_2,
    SIGNALS_DAC_NUM,
    SIGNALS_DAC_NONE,
};

//! DAC bitmask

enum Signals_dac_bitmask
{
    SIGNALS_DAC_ZERO_BIT_MASK = 0,
    SIGNALS_DAC_1_BIT_MASK    = (1 << SIGNALS_DAC_1),
    SIGNALS_DAC_2_BIT_MASK    = (1 << SIGNALS_DAC_2),
    SIGNALS_DACS_BIT_MASK     = (SIGNALS_DAC_1_BIT_MASK | SIGNALS_DAC_2_BIT_MASK),
};


//! ADC index

enum Signals_adc
{
    SIGNALS_INTERNAL_ADC_1,
    SIGNALS_INTERNAL_ADC_2,
    SIGNALS_INTERNAL_ADC_3,
    SIGNALS_INTERNAL_ADC_4,
    SIGNALS_INTERNAL_ADC_NUM,
    SIGNALS_EXTERNAL_ADC_1  = SIGNALS_INTERNAL_ADC_NUM,
    SIGNALS_EXTERNAL_ADC_2,
    SIGNALS_EXTERNAL_NUM    = SIGNALS_EXTERNAL_ADC_2 - SIGNALS_INTERNAL_ADC_4,
};


//! ADC bitmask

enum Signals_adc_bitmask
{
    SIGNALS_INTERNAL_ADC_ZERO_BIT_MASK = 0,
    SIGNALS_INTERNAL_ADC_1_BIT_MASK    = (1 << SIGNALS_INTERNAL_ADC_1),
    SIGNALS_INTERNAL_ADC_2_BIT_MASK    = (1 << SIGNALS_INTERNAL_ADC_2),
    SIGNALS_INTERNAL_ADC_3_BIT_MASK    = (1 << SIGNALS_INTERNAL_ADC_3),
    SIGNALS_INTERNAL_ADC_4_BIT_MASK    = (1 << SIGNALS_INTERNAL_ADC_4),
    SIGNALS_INTERNAL_ADCS_BIT_MASK     = (SIGNALS_INTERNAL_ADC_1_BIT_MASK | SIGNALS_INTERNAL_ADC_2_BIT_MASK |
                                          SIGNALS_INTERNAL_ADC_3_BIT_MASK | SIGNALS_INTERNAL_ADC_4_BIT_MASK),
    SIGNALS_EXTERNAL_ADC_ZERO_BIT_MASK = 0,
    SIGNALS_EXTERNAL_ADC_1_BIT_MASK    = (1 << SIGNALS_EXTERNAL_ADC_1),
    SIGNALS_EXTERNAL_ADC_2_BIT_MASK    = (1 << SIGNALS_EXTERNAL_ADC_2),
    SIGNALS_EXTERNAL_ADCS_BIT_MASK     = (SIGNALS_EXTERNAL_ADC_1_BIT_MASK | SIGNALS_EXTERNAL_ADC_2_BIT_MASK),
};


//! DCCT index

enum Signals_dcct
{
    SIGNALS_DCCT_1,
    SIGNALS_DCCT_2,
    SIGNALS_DCCT_NUM,
};


//! DCCT bitmask

enum Signals_dcct_bitmask
{
    SIGNALS_DCCT_ZERO_BIT_MASK = 0,
    SIGNALS_DCCT_1_BIT_MASK    = (1 << SIGNALS_DCCT_1),
    SIGNALS_DCCT_2_BIT_MASK    = (1 << SIGNALS_DCCT_2),
    SIGNALS_DCCTS_BIT_MASK     = (SIGNALS_DCCT_1_BIT_MASK | SIGNALS_DCCT_2_BIT_MASK),
};




// Make sure the ADC calibration, CAL_ZERO, CAL_POS and CAL_NEG are the
// same as those defined in the library libSig

#if (ADC_SOURCE_CAL_ZERO != SIG_CAL_OFFSET)
#error "Constants ADC_SOURCE_CAL_ZERO and SIG_CAL_OFFSET are different"
#endif

#if (ADC_SOURCE_CAL_POS != SIG_CAL_POSITIVE)
#error "Constants ADC_SOURCE_CAL_POS and SIG_CAL_POSITIVE are different"
#endif

#if (ADC_SOURCE_CAL_NEG != SIG_CAL_NEGATIVE)
#error "Constants ADC_SOURCE_CAL_NEG and SIG_CAL_NEGATIVE are different"
#endif



// ---------- External variable declarations

extern struct SIG_struct  sig_struct;
extern struct SIG_dac     dac_struct[SIGNALS_DAC_NUM];



// ---------- Platform/class specific function declarations

//! Class specific initialization.
//!
//! param[in] sig_struct      Pointer to the signals data.

void signalsClassInit(struct SIG_struct * const sig_struct);


//! Class specific signal acquisition

void signalsClassRtpAcq(void);


//! Class specific real-time processing of the signal handling.

void signalsClassRtp(void);


//! Updates the class specific measurements every 1 ms.

void signalsClassBgp1ms(void);


//! Updates the class specific measurements every 20 ms.

void signalsClassBgp20ms(void);


//! Updates the class specific measurements every 200 ms.

void signalsClassBgp200ms(void);


//! Class specific ADC to transducer linking

void signalsClassUpdateAdcToTrans(void);



// ---------- External function declarations

//! Initializes the signals.
//!
//! The 4 inputs are routed to the four ADCs. The analogue bus (or crossbar)
//! is floating and the MPX is disabled.

void signalsInit(void);


//! Real-time processing of the signal handling.

void signalsRtp(void);


//! Background processing of the signal handling.

void signalsBgp(void);


//! Updates the associated of ADCs to transducers.

void signalsUpdateAdcToTrans(void);


//! Updates the internal ADC temperature thresholds.

void signalsUpdateAdcTempThresholds(void);


//! Outputs a voltage to the DAC.
//!
//! @param[in]   idx             DAC index (DAC_1 or DAC_2).
//! @param[in]   value           Value in voltage to set.

void signalsSetDac(enum Signals_dac const idx, float const value);


//! Outputs a raw value to the DAC. Used only when MODE.OP is TEST
//!
//! @param[in]   idx             DAC index (DAC_1 or DAC_2).
//! @param[in]   ref_dac         Raw DAC value.

void signalsSetDacTest(enum Signals_dac const idx, int32_t const ref_dac);


//! Calibrates the DAC using the updated gain values.
//!
//! @param[in]   idx             DAC index (DAC_1 or DAC_2).

void signalsCalDacTrigger(enum Signals_dac const idx);


//! Notifies libsig whether the internal ADCs are used for calibration
//!
//! @param[in]   idx             ADC index (internal or external)
//! @param[in]   status          True if the ADC calibration is active. False otherwise.

void signalsCalAdcStatus(enum Signals_adc idx, bool status);


//! This function is called once the ADC calibration is complete.
//!
//! @param[in]   cal_level       Expected calibration level.

void signalsCalAdcReady(enum Dpcom_cal_level const cal_level);


//! This function is called once the ADC calibration is complete.
//!
//! @param[in]   dac             DAC calibrated.
//! @param[in]   raw_dac         Raw value used to calibrate the DAC.
//! @param[in]   cal_level       Expected calibration level.

void signalsCalDacReady(enum Signals_dac     const dac,
                        int32_t              const raw_dac,
                        enum Dpcom_cal_level const cal_level);


//! This function is called once the ADC calibration is complete.
//!
//! @param[in]   dcct            DCCT calibrated.
//! @param[in]   cal_level       Expected calibration level.

void signalsCalDcctReady(enum Signals_dcct    const dcct,
                         enum Dpcom_cal_level const cal_level);


//! Updates the associated of ADCs to transducers.

void signalUpdateAdcToTrans(void);


//! Updates inter-FGC signals
//!
//! The function must be called from the ISR

void signalsClassInterFgc(void);


// Accessers to libsig parametes and variables

#define signalsVarGetPointer(STRUCT_TYPE, STRUCT_NAME, VAR_NAME)   sigVarPointer(&sig_struct, STRUCT_TYPE, STRUCT_NAME, VAR_NAME)

// EOF
