//! @file  cycle.c
//! @brief Functionality related to cycles

#pragma once


// ---------- Includes

#include <stdbool.h>
#include <stdint.h>



// ---------- Platform/class specific function declarations

//! Returns the status for a given cycle.
//!
//! @param[in]   sub_sel         Sub-device selector.
//! @param[in]   cyc_sel         Cycle selector.
//!
//! @return                      The cycle status.

uint32_t cycleClassGetStatus(uint8_t sub_sel, uint8_t cyc_sel);


//! Returns the function type for a given cycle.
//!
//! @param[in]   sub_sel         Sub-device selector.
//! @param[in]   cyc_sel         Cycle selector.
//!
//! @return                      The funciton type.

uint32_t cycleClassGetFuncType(uint8_t sub_sel, uint8_t cyc_sel);



// ---------- External function declarations

//! Initializes the cycle module

void cycleInit(void);


//! Real-time functionality related to cycles.

void cycleRtp(void);


//! Background functionality related to cycles.

void cycleBgp(void);


//! Return the current reference sub-device selector
//!
//! @return                      The current sub-device selector.

uint32_t cycleGetSubSelRef(void);


//! Return the current reference cycle selector.
//!
//! @return                      The current cycle selector.

uint32_t cycleGetCycSelRef(void);


//! Return the current acquisition sub-device selector
//!
//! @return                      The current sub-device selector.

uint32_t cycleGetSubSelAcq(void);


//! Return the current acquisition cycle selector.
//!
//! @return                      The current cycle selector.

uint32_t cycleGetCycSelAcq(void);


//! Returns the cycle time in microseconds.
//!
//! @return                      The current cycle time in microseconds.

uint32_t cycleGetTimeUs(void);


//! Returns the cycle time in milliseconds.
//!
//! @return                      The current cycle time in milliseconds.

uint32_t cycleGetTimeMs(void);


//! Returns true during one iteration when a cycle starts.
//!
//! @retval      true            The cycle started on this iteration.
//! @retval      false           The cycle did not start on this iteration.

bool cycleIsStarted(void);


//! Returns true during the iteration three iterations after the start of the cycle.
//!
//! @retval      true            The cycle started three iterations in the past.
//! @retval      false           The cycle did not start three iterations in the past.

bool cycleWasStarted(void);


// EOF

