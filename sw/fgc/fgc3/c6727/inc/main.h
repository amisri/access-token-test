//! @file  main.h
//! @brief FGC3 DSP main function

#pragma once


// ---------- External function declarations

//! This is the main function for the FGC DSP Program.
//!
//! The DSP is controlled by the MCU, and will be started only after the MCU has
//! prepared the Dual Port RAM.This function is does all the initialisation duties.
//! It then starts the operating system to launch multi-tasking.

void main(void);


// EOF
