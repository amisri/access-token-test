//! @file  dsp.h
//! @brief Functionality specific to the DSP TMS320C6727

#pragma once


// ---------- Includes

#include <stdint.h>

#include <defconst.h>

#include "fgc3/inc/memmap.h"



// ---------- Constants

// Address of peripherals

#define  INTERNAL_ROM_PAGE_0                 0X00000000                                       //!< Start: Internal ROM Page 0 (256K Bytes) End: 0x0003FFFF Byte and Word
#define  INTERNAL_ROM PAGE_1                 0x00040000                                       //!< Start: Internal ROM Page 1 (128K Bytes) End: 0x0005FFFF Byte and Word
#define  INTERNAL_RAM_PAGE_0                 0x10000000                                       //!< Start: Internal RAM Page 0 (256K Bytes) End: 0x1003FFFF Byte and Word
#define  MEMORY_N_CACHE_CONTROL_REG_START    0x20000000                                       //!< Start: Memory and Cache Control Registers End: 0x2000001F Word Only
#define  EMULATION_CONTROL_REG_START         0x30000000                                       //!< Start: Emulation Control Registers (Do Not Access) End: 0x3FFFFFFF Word Only
#define  DEVICE_CONFIG_REG_START             0x40000000                                       //!< Start: Device Configuration Registers End: 0x40000083 Word Only
#define  PLL_REG_START                       0x41000000                                       //!< Start: PLL Control Registers End: 0x4100015F Word Only
#define  RTI_REG_START                       0x42000000                                       //!< Start: Real-time Interrupt (RTI) Control Registers End: 0x420000A3 Word Only
#define  UHPI_REG_START                      0x43000000                                       //!< Start: Universal Host-Port Interface (UHPI) Registers End: 0x43000043 Word Only
#define  McASP0_REG_START                    0x44000000                                       //!< Start: McASP0 Control Registers End: 0x440002BF Word Only
#define  McASP1_REG_START                    0x45000000                                       //!< Start: McASP1 Control Registers End: 0x450002BF Word Only
#define  McASP2_REG_START                    0x46000000                                       //!< Start: McASP2 Control Registers End: 0x460002BF Word Only
#define  SPI0_REG_START                      0x47000000                                       //!< Start: SPI0 Control Registers End: 0x4700007F Word Only
#define  SPI1_REG_START                      0x48000000                                       //!< Start: SPI1 Control Registers End: 0x4800007F Word Only
#define  I2C0_REG_START                      0x49000000                                       //!< Start: I2C0 Control Registers End: 0x4900007F Word Only
#define  I2C1_REG_START                      0x4A000000                                       //!< Start: I2C1 Control Registers End: 0x4A00007F Word Only
#define  McASP0_DMA_REG_START                0x54000000                                       //!< Start: McASP0 DMA Port (any address in this range) End: 0x54FFFFFF Word Only
#define  McASP1_DMA_REG_START                0x55000000                                       //!< Start: McASP1 DMA Port (any address in this range) End: 0x55FFFFFF Word Only
#define  McASP2_DMA_REG_START                0x56000000                                       //!< Start: McASP2 DMA Port (any address in this range) End: 0x56FFFFFF Word Only
#define  dMAX_REG_START                      0x60000000                                       //!< Start: dMAX Control Registers End: 0x6000008F Word Only
#define  MAX0_EVENT_ENTRY                    0x61008000                                       //!< Start: MAX0 (HiMAX) Event Entry Table End: 0x6100807F Byte and Word
#define  MAX0_TRANSFER_ENTRY                 0x610080A0                                       //!< Start: MAX0 (HiMAX) Transfer Entry Table End: 0x610081FF Byte and Word
#define  MAX1_EVENT_ENTRY                    0x62008000                                       //!< Start: MAX1 (LoMAX) Event Entry Table End: 0x6200807F Byte and Word
#define  MAX1_TRANSFER_ENTRY                 0x620080A0                                       //!< Start: MAX1 (LoMAX) Transfer Entry Table End: 0x620081FF Byte and Word
#define  SDRAM_SPACE_START                   0x80000000                                       //!< Start: External SDRAM space on EMIF End: 0x8FFFFFFF Byte and Word
#define  FLASH_SPACE_START                   0x90000000                                       //!< Start: External Asynchronous / Flash space on EMIF End: 0x9FFFFFFF Byte and Word
#define  EMIF_REG_START                      0xF0000000                                       //!< Start: EMIF Control Registers End: 0xF00000BF Word Only(

// C6727 peripheral definitions

#define MCASP0_PFUNC                        (volatile uint32_t *)(McASP0_REG_START + 0x010)
#define MCASP0_PDIR                         (volatile uint32_t *)(McASP0_REG_START + 0x014)
#define MCASP0_PDOUT                        (volatile uint32_t *)(McASP0_REG_START + 0x018)
#define MCASP0_PDIN                         (volatile uint32_t *)(McASP0_REG_START + 0x01c)
#define MCASP0_PDSET                        (volatile uint32_t *)(McASP0_REG_START + 0x01c)
#define MCASP0_PDCLR                        (volatile uint32_t *)(McASP0_REG_START + 0x020)
#define MCASP1_PFUNC                        (volatile uint32_t *)(McASP1_REG_START + 0x010)
#define MCASP1_PDIR                         (volatile uint32_t *)(McASP1_REG_START + 0x014)
#define MCASP1_PDOUT                        (volatile uint32_t *)(McASP1_REG_START + 0x018)
#define MCASP1_PDIN                         (volatile uint32_t *)(McASP1_REG_START + 0x01c)
#define MCASP1_PDSET                        (volatile uint32_t *)(McASP1_REG_START + 0x01c)
#define MCASP1_PDCLR                        (volatile uint32_t *)(McASP1_REG_START + 0x020)
#define MCASP2_PFUNC                        (volatile uint32_t *)(McASP2_REG_START + 0x010)
#define MCASP2_PDIR                         (volatile uint32_t *)(McASP2_REG_START + 0x014)
#define MCASP2_PDOUT                        (volatile uint32_t *)(McASP2_REG_START + 0x018)
#define MCASP2_PDIN                         (volatile uint32_t *)(McASP2_REG_START + 0x01c)
#define MCASP2_PDSET                        (volatile uint32_t *)(McASP2_REG_START + 0x01c)
#define MCASP2_PDCLR                        (volatile uint32_t *)(McASP2_REG_START + 0x020)

#define PLL_PID                             (volatile uint32_t *)(PLL_REG_START + 0x000)   // identification
#define PLL_CSR                             (volatile uint32_t *)(PLL_REG_START + 0x100)   // control/status
#define PLL_M                               (volatile uint32_t *)(PLL_REG_START + 0x110)   // multiplier 1...25
#define PLL_DIV0                            (volatile uint32_t *)(PLL_REG_START + 0x114)   //
#define PLL_DIV1                            (volatile uint32_t *)(PLL_REG_START + 0x118)   //
#define PLL_DIV2                            (volatile uint32_t *)(PLL_REG_START + 0x11C)   //
#define PLL_DIV3                            (volatile uint32_t *)(PLL_REG_START + 0x120)   //
#define PLL_CMD                             (volatile uint32_t *)(PLL_REG_START + 0x138)   // controller command
#define PLL_STAT                            (volatile uint32_t *)(PLL_REG_START + 0x13c)   // controller status
#define ALN_CTL                             (volatile uint32_t *)(PLL_REG_START + 0x140)   // controller clock align control
#define CKEN                                (volatile uint32_t *)(PLL_REG_START + 0x148)   // clock enable control
#define CKSTAT                              (volatile uint32_t *)(PLL_REG_START + 0x14c)   // clock status
#define SYSTAT                              (volatile uint32_t *)(PLL_REG_START + 0x150)   // SYSCLK status

#define CSR_PLLEN                            0x00000001
#define CSR_PLLPWRDN                         0x00000010
#define CSR_OSCPWRDN                         0x00000004
#define CSR_PLLRST                           0x00000008
#define CSR_PLLPWRDN                         0x00000010
#define CSR_PLLSTABLE                        0x00000040
#define DIV_ENABLE                           0x00008000
#define CMD_GOSET                            0x00000001


#define EMIF_AWCCR                          (volatile uint32_t *)(EMIF_REG_START + 0x004)           // async wait cycle config
#define EMIF_SDCR                           (volatile uint32_t *)(EMIF_REG_START + 0x008)           // SDRAM config reg
#define EMIF_SDRCR                          (volatile uint32_t *)(EMIF_REG_START + 0x00c)           // SDRAM refresh control
#define EMIF_A1CR                           (volatile uint32_t *)(EMIF_REG_START + 0x010)           // asynchronous 1 config
#define EMIF_SDTIMR                         (volatile uint32_t *)(EMIF_REG_START + 0x020)           // SDRAM timing register
#define EMIF_SDSRETR                        (volatile uint32_t *)(EMIF_REG_START + 0x03c)           // SDRAM self refresh exit timing register
#define EMIF_EIRR                           (volatile uint32_t *)(EMIF_REG_START + 0x040)           // interrupt raw
#define EMIF_EIMR                           (volatile uint32_t *)(EMIF_REG_START + 0x044)           // interrupt mask reg
#define EMIF_EIMSR                          (volatile uint32_t *)(EMIF_REG_START + 0x048)           // interrupt mask set
#define EMIF_EIMCR                          (volatile uint32_t *)(EMIF_REG_START + 0x04c)           // interrupt mask clear reg

// UHPI Internal Registers

#define  UHPI                               (volatile uint32_t *)(UHPI_REG_START + 0x008)   // Configuration Register
#define  CFGHPIAMSB                         (volatile uint32_t *)(UHPI_REG_START + 0x00C)   // Most Significant Byte of UHPI Address
#define  CFGHPIAUMB                         (volatile uint32_t *)(UHPI_REG_START + 0x010)   // Upper Middle Byte of UHPI Address
#define  PID                                (volatile uint32_t *)(UHPI_REG_START + 0x000)   // Peripheral ID Register
#define  PWREMU                             (volatile uint32_t *)(UHPI_REG_START + 0x004)   // Power and Emulation Management Register
#define  GPIOINT                            (volatile uint32_t *)(UHPI_REG_START + 0x008)   // General Purpose I/O Interrupt Control Register
#define  GPIOEN                             (volatile uint32_t *)(UHPI_REG_START + 0x00C)   // General Purpose I/O Enable Register
#define  GPIODIR1                           (volatile uint32_t *)(UHPI_REG_START + 0x010)   // General Purpose I/O Direction Register 1
#define  GPIODAT1                           (volatile uint32_t *)(UHPI_REG_START + 0x014)   // General Purpose I/O Data Register 1
#define  GPIODIR2                           (volatile uint32_t *)(UHPI_REG_START + 0x018)   // General Purpose I/O Direction Register 2
#define  GPIODAT2                           (volatile uint32_t *)(UHPI_REG_START + 0x01C)   // General Purpose I/O Data Register 2
#define  GPIODIR3                           (volatile uint32_t *)(UHPI_REG_START + 0x020)   // General Purpose I/O Direction Register 3
#define  GPIODAT3                           (volatile uint32_t *)(UHPI_REG_START + 0x024)   // General Purpose I/O Data Register 3
#define  HPIC                               (volatile uint32_t *)(UHPI_REG_START + 0x030)   // Control Register
#define  HPIAW                              (volatile uint32_t *)(UHPI_REG_START + 0x034)   // Write Address Register
#define  HPIAR                              (volatile uint32_t *)(UHPI_REG_START + 0x038)   // Read Address Register

#define  UHPI_BYTEAD                        (1 << 4)     // 0 R/W UHPI Host Address Type
#define  UHPI_FULL                          (1 << 3)       // 0 R/W UHPI Multiplexing Mode (when NMUX = 0)
#define  UHPI_NMUX                          (1 << 2)       // 0 R/W UHPI Non-Multiplexed Mode Enable
#define  UHPI_PAGEM                         (1 << 1)      // 0 R/W UHPI Page Mode Enable (Only for Multiplexed Host Address and Data Mode).
#define  UHPI_ENA                           (1 << 0)        // 0 R/W UHPI Enable
#define  UHPI_HPIAMSB                       (1 << 0)    // R/W UHPI most significant byte of DSP address to access in non-multiplexed host

// RTI registers

#define  RTIGCTRL                           (volatile uint32_t *)(RTI_REG_START  + 0x000)  // Global Control Register. Starts / stops the counters.
#define  RTICAPCTRL                         (volatile uint32_t *)(RTI_REG_START  + 0x008)  // Capture Control. Controls the capture source for the counters.
#define  RTICOMPCTRL                        (volatile uint32_t *)(RTI_REG_START  + 0x00C)  // Compare Control. Controls the source for the compare registers.
#define  RTIFRC0                            (volatile uint32_t *)(RTI_REG_START  + 0x010)  // Free-Running Counter 0. Current value of free running counter 0.
#define  RTIUC0                             (volatile uint32_t *)(RTI_REG_START  + 0x014)  // Up-Counter 0. Current value of prescale counter 0.
#define  RTICPUC0                           (volatile uint32_t *)(RTI_REG_START  + 0x018)  // Compare Up-Counter 0. Compare value compared with prescale counter 0.
#define  RTICAFRC0                          (volatile uint32_t *)(RTI_REG_START  + 0x020)  // Capture Free-Running Counter 0. Current value of free running counter 0 on external event.
#define  RTICAUC0                           (volatile uint32_t *)(RTI_REG_START  + 0x024)  // Capture Up-Counter 0. Current value of prescale counter 0 on external event.
#define  RTIFRC1                            (volatile uint32_t *)(RTI_REG_START  + 0x030)  // Free-Running Counter 1. Current value of free running counter 1.
#define  RTIUC1                             (volatile uint32_t *)(RTI_REG_START  + 0x034)  // Up-Counter 1. Current value of prescale counter 1.
#define  RTICPUC1                           (volatile uint32_t *)(RTI_REG_START  + 0x038)  // Compare Up-Counter 1. Compare value compared with prescale counter 1.
#define  RTICAFRC1                          (volatile uint32_t *)(RTI_REG_START  + 0x040)  // Capture Free-Running Counter 1. Current value of free running counter 1 on external event.
#define  RTICAUC1                           (volatile uint32_t *)(RTI_REG_START  + 0x044)  // Capture Up-Counter 1. Current value of prescale counter 1 on external event.
#define  RTICOMP0                           (volatile uint32_t *)(RTI_REG_START  + 0x050)  // Compare 0. Compare value to be compared with the counters.
#define  RTIUDCP0                           (volatile uint32_t *)(RTI_REG_START  + 0x054)  // Update Compare 0. Value to be added to the compare register 0 value on compare match.
#define  RTICOMP1                           (volatile uint32_t *)(RTI_REG_START  + 0x058)  // Compare 1. Compare value to be compared with the counters.
#define  RTIUDCP1                           (volatile uint32_t *)(RTI_REG_START  + 0x05C)  // Update Compare 1. Value to be added to the compare register 1 value on compare match.
#define  RTICOMP2                           (volatile uint32_t *)(RTI_REG_START  + 0x060)  // Compare 2. Compare value to be compared with the counters.
#define  RTIUDCP2                           (volatile uint32_t *)(RTI_REG_START  + 0x064)  // Update Compare 2. Value to be added to the compare register 2 value on compare match.
#define  RTICOMP3                           (volatile uint32_t *)(RTI_REG_START  + 0x068)  // Compare 3. Compare value to be compared with the counters.
#define  RTIUDCP3                           (volatile uint32_t *)(RTI_REG_START  + 0x06C)  // Update Compare 3. Value to be added to the compare register 3 value on compare match.
#define  RTISETINT                          (volatile uint32_t *)(RTI_REG_START  + 0x080)  // Set Interrupt Enable. Sets interrupt enable bits int RTIINTCTRL without having to do aread-modify-write operation.
#define  RTICLEARINT                        (volatile uint32_t *)(RTI_REG_START  + 0x084)  // Clear Interrupt Enable. Clears interrupt enable bits int RTIINTCTRL without having to do a read-modify-write operation.
#define  RTIINTFLAG                         (volatile uint32_t *)(RTI_REG_START  + 0x088)  // Interrupt Flags. Interrupt pending bits.
#define  RTIDWDCTRL                         (volatile uint32_t *)(RTI_REG_START  + 0x090)  // Digital Watchdog Control. Enables the Digital Watchdog.
#define  RTIDWDPRLD                         (volatile uint32_t *)(RTI_REG_START  + 0x094)  // Digital Watchdog Preload. Sets the experation time of the Digital Watchdog.
#define  RTIWDSTATUS                        (volatile uint32_t *)(RTI_REG_START  + 0x098)  // Watchdog Status. Reflects the status of Analog and Digital Watchdog.
#define  RTIWDKEY                           (volatile uint32_t *)(RTI_REG_START  + 0x09C)  // Watchdog Key. Correct written key values discharge the external capacitor.
#define  RTIDWDCNTR                         (volatile uint32_t *)(RTI_REG_START  + 0x0A0)  // Digital Watchdog Down-Counter

#define DMAX_DEHPR                          (volatile uint32_t *)(dMAX_REG_START + 0x014)  //
#define DMAX_DEPR                           (volatile uint32_t *)(dMAX_REG_START + 0x008)  //
#define DMAX_DEER                           (volatile uint32_t *)(dMAX_REG_START + 0x01C)  //
#define DMAX_DEDR                           (volatile uint32_t *)(dMAX_REG_START + 0x010)  //
#define DMAX_DEHPR                          (volatile uint32_t *)(dMAX_REG_START + 0x014)  //
#define DMAX_DELPR                          (volatile uint32_t *)(dMAX_REG_START + 0x018)  //
#define DMAX_DEFR                           (volatile uint32_t *)(dMAX_REG_START + 0x01C)  //
#define DMAX_DER0                           (volatile uint32_t *)(dMAX_REG_START + 0x034)  //
#define DMAX_DER1                           (volatile uint32_t *)(dMAX_REG_START + 0x054)  //
#define DMAX_DER2                           (volatile uint32_t *)(dMAX_REG_START + 0x074)  //
#define DMAX_DER3                           (volatile uint32_t *)(dMAX_REG_START + 0x094)  //
#define DMAX_DFSR0                          (volatile uint32_t *)(dMAX_REG_START + 0x040)  //
#define DMAX_DFSR1                          (volatile uint32_t *)(dMAX_REG_START + 0x060)  //
#define DMAX_DTCR0                          (volatile uint32_t *)(dMAX_REG_START + 0x080)  //
#define DMAX_DTCR1                          (volatile uint32_t *)(dMAX_REG_START + 0x0A0)  //


// Cache registers (In addition to the CSR core register)

#define L1PSAR                              (volatile uint32_t *)(MEMORY_N_CACHE_CONTROL_REG_START + 0x000)
#define L1PICR                              (volatile uint32_t *)(MEMORY_N_CACHE_CONTROL_REG_START + 0x004)

#define L1P_INVALIDATE                      (0x80000000u)

// CSR[7:5] controls the L1 Program Cache

#define CACHE_ENABLE                        (0x00000040u)
#define CACHE_FREEZE                        (0x00000060u)
#define CACHE_BYPASS                        (0x00000080u)
#define CACHE_CTRL_MASK                     (0x000000E0u)

// DSP Test points (for debugging)

#define SET_TP0()                           ((*GPIODAT1) |= 0x00000001)
#define CLR_TP0()                           ((*GPIODAT1) &= 0xFFFFFFFE)
#define TGL_TP0()                           ((*GPIODAT1) ^= 0x00000001)
#define SET_TP1()                           ((*GPIODAT1) |= 0x00000002)
#define CLR_TP1()                           ((*GPIODAT1) &= 0xFFFFFFFD)
#define TGL_TP1()                           ((*GPIODAT1) ^= 0x00000002)

// DSP time flag

#define TST_TIME_DSP_FLAG()                 (!!((*GPIODAT1) & 0x00000020))


// On FGC3 the periodic interrupt is ISR2 (the NMI) and originates from the EM_WAIT interrupt generated by the EMIF.
// As a consequence, the acknowledgement, disabling and enabling of the periodic interrupt must be done on the EMIF.
// Note 1: Although only the EM_WAIT interrupts is used on FGC3, the macros below also apply the same operations to
// the Asynchronous Timeout interrupt.
// Note 2: The reason for the "NOP 2" assembly instruction in macro DISABLE_NMI is to wait for the register write to
// complete (when running the store instruction, the memory write occurs on pipeline stage E3)

#define ACKNOWLEDGE_NMI()                   ((EMIF_EIRR_P)  = (EMIF_EIRR_WR_MASK32 | EMIF_EIRR_AT_MASK32))
#define ENABLE_NMI()                        ((EMIF_EIMSR_P) = (EMIF_EIRR_WR_MASK32 | EMIF_EIRR_AT_MASK32))
#define DISABLE_NMI()                     { ((EMIF_EIMCR_P) = (EMIF_EIRR_WR_MASK32 | EMIF_EIRR_AT_MASK32)); asm("    NOP 2"); }


// Use TI compiler intrinsics

#define ENABLE_INTS()                     {                _enable_interrupts();  ENABLE_NMI(); }
#define DISABLE_INTS()                    { DISABLE_NMI(); _disable_interrupts();               }

// Interrupts are not disabled using the EMIF control registers since they might be lost
// (it is not clear on the documentation). Instead the GPIO UHPI_INT/AMUTE2 is commanded
// to signal the FPGA whether the DSP 10 KHz tick can be generated or should be held until
// interrupts are enabled again.

#define ENABLE_TICKS()                      ((*GPIODAT2) &= 0xFFFFFEFF)
#define DISABLE_TICKS()                   { ((*GPIODAT2) |= 0x00000100); asm("    NOP 2"); }



// ---------- External function declarations

//! Initialises the DSP.

void dspInit(void);

//! Realocates the interrupt vector table and enables interrupts.

void dspInitInterrupts(void);


// EOF
