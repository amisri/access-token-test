//! @file  spy.h
//! @brief Provides access to FGC3 signals through the serial port

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- Platform/class specific function declarations

//! Gets a signal value.
//!
//! @param[in]   idx             Index of the signal to retrieve.
//!
//! @return                      The signals value.

float spyClassGetSignal(uint32_t const idx);



// ---------- External function declarations

//! Sends values to the SPY serial interface.
//!
//! There are six channels, all of which can be selected from a range of choices
//! by the user through the property MEAS.SPY.MPX.

void spySend(void);


// EOF
