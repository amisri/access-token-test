//! @file  log.h
//! @brief Functionality related to logging


// ---------- Includes

#include <liblog.h>

#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/dpcom.h"



// ---------- External variable declarations

extern struct LOG_read_control   log_read_control[2];



// ---------- Platform/class specific function declarations

//! Class specific initialization of signal logging.

bool logClassInit(void);


//! Class specific real-time processing for signal logging.

void logClassRtp(void);


//! Class specific background processing for signal logging.

void logClassBgp(void);



// ---------- External function definitions

//! Initialization of signal logging.

void logInit(void);


//! Real-time processing for signal logging.

void logRtp(void);


//! Background processing for signal logging.

void logBgp(void);


//! Process a read request from the MCU

void logParsReadRequest(void);



// ---------- External function definitions

//! This function commands the MCU to disables a log
//!
//! @param[in]    log              Log to disable

static inline void logDisableLog(enum Dpcom_log_ignore_bitmask const log)
{
    setBitmap(dpcom.mcu.log.ignore_mask, log);
}


//! This function commands the MCU to enable a log
//!
//! @param[in]    log              Log to enable

static inline void logEnableLog(enum Dpcom_log_ignore_bitmask const log)
{
    clrBitmap(dpcom.mcu.log.ignore_mask, log);
}


// EOF
