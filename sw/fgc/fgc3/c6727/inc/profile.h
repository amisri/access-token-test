//! @file     profile.h
//! @brief    Functions to profile the execution time of a section of code.
//!
//!           There are two profiling systems:
//!
//!           1. FGC.DEBUG.DSP.ELAPSED_TIME
//!           2. ACQ log signals DSP_RT_PROF_0 and DSP_RT_PROF_1
//!
//!           System 1 uses a buffer of length FGC_PROFILE_LEN to
//!           report elapsed-time measurements through the property FGC.DEBUG.DSP.ELAPSED_TIME.
//!           Measurements are made using profileStart(idx) and profileStop(idx)
//!           where idx is the slot in the buffer. The free-running timer used for the measurement
//!           runs at 75MHz, so each tick is 13.3333ns. It is a debugging system which is
//!           not permanently in the code.
//!
//!           System 2 is permanently available and contributes two signals to the
//!           ACQ log: DSP_RT_PROF_0 and DSP_RT_PROF_1. They report the time
//!           since the start of the DSP iteration in microseconds. The
//!           property FGC.DEBUG.DSP.RT_PROFILE is an array of two and it allows
//!           the user to select which profile points within the real-time
//!           processing to expose through the two signals.

#pragma once


// ---------- Includes

#include <stdint.h>
#include <string.h>
#include "platforms/fgc3/memmap_dsp.h"
#include "defconst.h"
#include "fgc3/c6727/inc/dsp.h"



// ---------- Constants

#define PROFILE_CLOCK_HZ    75000000



// ---------- External structures, unions and enumerations

//! Structure for the module Profile

struct Profile
{
    // Array for Profile system 1

    struct Profile_dsp_timings
    {
        uint32_t          start_time  [FGC_PROFILE_LEN];    //!< Profiling measurements
        uint32_t          elapsed_time[FGC_PROFILE_LEN];    //!< Profiling measurements
    } dsp_timing;

    // Structure for Profile system 2

    struct Profile_dsp_rt
    {
        uint32_t      mpx   [2];                            //!< Multiplexer for the DSP RT dsp_rt_profile.signal
        float         signal[2];                            //!< Pointer to the selected DSP RT dsp_rt_profile signal (microseconds in a float for liblog)
        uint32_t      max_us[3];                            //!< DSP RT max times in microseconds (FGC.DEBUG.DSP.RT_MAX_US)
        uint32_t      time  [FGC_DSP_RT_PROFIL_END+1];      //!< DSP RT dsp_rt_profile times (time in the iteration in microseconds)
    } dsp_rt;

    struct Profile_watch_point
    {
        uint32_t      control;                              //!< Watchpoint control: 0=OFF 1=EQUALS 2=NOT EQUALS
        uint32_t      address;                              //!< Watchpoint address in page 0
        uint32_t      value;                                //!< Watchpoint value to check at address
        uint32_t      mask;                                 //!< Watchpoint value mask
        uint32_t      hit_count;                            //!< Hit counter
        uint32_t      bgp_check_id;                         //!< Background check ID
        uint32_t      isr_check_id;                         //!< ISR check ID
        uint32_t      timestamp_s;                          //!< UTC time in seconds when the hit occurred
        uint32_t      timestamp_us;                         //!< UTC time in microseconds when the hit occurred
    }                 watch_point;                          //!< FGC.DEBGUG.DSP.WATCH
};



// ---------- External variable declarations

extern struct Profile  profile;



// ---------- External function declaration

//! Initializes the profile module

void profileInit(void);


//! Checks the watch point in the backgrdoun task

void profileCheckWatchPointBgp(uint8_t const id);


//! Checks the watch point in the real-time task

void profileCheckWatchPointRtp(uint8_t const id);



// ---------- External function definitions

// Profile System 1 : Static inline function declarations

static inline void profileStart(uint8_t const idx)
{
	// Save the free-running counter

    profile.dsp_timing.start_time[idx] = *RTIFRC0;
}



static inline void profileStop(uint8_t const idx)
{
	// Calculate elapsed time since call to profileStart()

    profile.dsp_timing.elapsed_time[idx] = *RTIFRC0 - profile.dsp_timing.start_time[idx];
}



// Profile System 2 : Static inline function declarations

static inline void profileRegister(uint8_t const idx)
{
    // Save the microsecond time since the start of the iteration as an integer

    profile.dsp_rt.time[idx] = TIME_DSP_ITER_US_P;
}



static inline void profileSaveSignals(void)
{
    // Convert the microsecond time from integer to float to be compatible with the ACQ log

    profile.dsp_rt.signal[0] = (float)profile.dsp_rt.time[profile.dsp_rt.mpx[0]];
    profile.dsp_rt.signal[1] = (float)profile.dsp_rt.time[profile.dsp_rt.mpx[1]];
}



static inline void profileMaxPoint(uint8_t const max_index, uint8_t const idx)
{
    // Save the maximum us time for the profiling point idx in the max array

    if(profile.dsp_rt.time[idx] > profile.dsp_rt.max_us[max_index])
    {
        profile.dsp_rt.max_us[max_index] = profile.dsp_rt.time[idx];
    }
}



static inline void profileMaxTime(uint8_t const max_index, uint32_t time_us)
{
    // Save the maximum us time provided in time_us in the max array

    if(time_us > profile.dsp_rt.max_us[max_index])
    {
        profile.dsp_rt.max_us[max_index] = time_us;
    }
}





// EOF
