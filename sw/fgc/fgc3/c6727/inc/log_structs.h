//! @file  log_structs.h
//! @brief Proxy to include the class specific logStruct.h header file

#pragma once


// ---------- Includes

#if (FGC_CLASS_ID == 62)

#include "fgc3/c6727/62/inc/log_class.h"

#elif (FGC_CLASS_ID == 63)

#include "fgc3/c6727/63/inc/log_class.h"

#elif (FGC_CLASS_ID == 64)

#include "fgc3/c6727/64/inc/log_class.h"

#elif (FGC_CLASS_ID == 65)

#include "fgc3/c6727/65/inc/log_class.h"

#elif (FGC_CLASS_ID == 66)

#include "fgc3/c6727/66/inc/log_class.h"

#else

#error "FGC_CLASS_ID is not defined"

#endif


// EOF
