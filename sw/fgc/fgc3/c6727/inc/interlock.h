//! @file  interlock.h
//! @brief Interface to cclibs libintlk

#pragma once


// ---------- Includes

#include <stdint.h>

#include <libintlk.h>



// ---------- External structures, unions and enumerations

//! Internal global variables for the module interlock

struct Interlock
{
    uint32_t           sub_sel;        //!< Sub-device selector used by libintlk
    uint32_t           cyc_sel;        //!< Cycle selector used by libintlk
    bool               latch;          //!< If true, the interlock status and measurements are latched in the library
};



// ---------- External variable declarations

extern struct Interlock   interlock;
extern struct INTLK_mgr   intlk_mgr;



// ---------- Platform/class specific function declarations

//! Class specific initialization of the interlock module.

void interlockClassInit(struct Interlock * interlock, struct INTLK_mgr * intlk_mgr);


//! Return the value of INTERLOCK.TEST

uint32_t interlockClassGetChannelTest(void);


//! Return the value of INTERLOCK.OUTPUT

uint32_t interlockClassGetOutput(uint8_t sub_sel, uint8_t cyc_sel);



// ---------- External function declarations

//! Initializes the interlock module.

void interlockInit(void);


//! Run the interlock check in real-time.

void interlockRtp(void);


//! Latches the  status

void interlockLatchStatus(void);


//! Ignore logs based on the interlock parameters

void interlockIgnoreLogs(void);


// EOF
