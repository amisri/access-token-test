//! @file  bgp.h
//! @brief Background processing functions

#pragma once


// ---------- Includes

#include "fgc3/inc/dpcom.h"



// ---------- Platform/class specific function declarations

//! Class specific command processing.

void bgpClassProcessCommands(enum Dpcom_mcu_commands command);



// ---------- External function declarations

//! Initializes the background process.

void bgpInit(void);


//! This is the Background processing loop.

void bgpProcess(void);


// EOF
