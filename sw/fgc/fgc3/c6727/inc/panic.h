//! @file  panic.h
//! @brief DSP panic function

#pragma once



// ---------- External structures, unions and enumerations

//! DSP panic codes

enum Panic_codes
{
    DSP_PANIC_ISR_TRAP              = 0x00801000,
    DSP_PANIC_BAD_PARAMETER         = 0x00802000,
    DSP_PANIC_INDEX_OUT_OF_RANGE    = 0x00802001,
    DSP_PANIC_HW_ANATYPE            = 0x00803000,
    DSP_PANIC_HW_ANALOG_CARD        = 0x00803001,
    DSP_PANIC_ISR_SWITCH            = 0x00804000,
    DSP_PANIC_BGP_1HZ_OVERRUN       = 0x00805000,
    DSP_PANIC_REG_V_PERIOD_DIV      = 0x00806000,
    DSP_PANIC_FILTER_20MS_LEN       = 0x00807000,
    DSP_PANIC_ASSERT                = 0x00808000
};



// ---------- External function declarations

//! DSP panic function.
//!
//! It disables interrupts and runs an infinite loop.
//!
//! @param[in]   panic_code      Code that cause the panic.

void panic(enum Panic_codes const panic_code);


//! DSP assert function.
//!
//! @param[in]   file            File name.
//! @param[in]   line            Line number.

void assert_func(char const * file, int const line);



// ---------- External function definitions

#ifdef NDEBUG

# define dsp_assert(condition) ((void)0)

#else

#include <string.h>

# define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
# define dsp_assert(condition) ((condition) ? (void)0 : assert_func(__FILENAME__, __LINE__))

#endif

// EOF
