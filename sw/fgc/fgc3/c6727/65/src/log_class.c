//! @file  log_class.c
//! @brief Class specific logging


// ---------- Includes

#include <liblog.h>
#include <libsig.h>

#include "platforms/fgc3/memmap_dsp.h"

#include "fgc3/c6727/65/inc/log_class.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/spivs.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"


// The variables below are used by logStructsInit() and associated
// to signals in in fgc/def/liblog/ods/class_65.ods

struct Log_class_signals
{
    cc_float   dim_ana[4];         //!< DIM analogue values

    // Set in this module and referenced by the logs (initially the data comes in through DPCLS)
    // Multiplexed by dpcls.mcu.log_data.device_index
    uint32_t    epic_en_wn;
    uint32_t    epic_res_w1;
    uint32_t    epic_res_w2;
};

static struct Log_class_signals  log_class_signals;

#include "inc/classes/65/logStructs.h"
#include "inc/classes/65/logStructsInit.h"


// ---------- Constants

static uint32_t const DIM_IDX_INVALID = 0xFFFFFFFF;



// ---------- External variable definitions

#pragma DATA_SECTION(log_read,     ".ext_data")
#pragma DATA_SECTION(log_buffers,  ".ext_log")

struct LOG_structs  log_structs;
struct LOG_buffers  log_buffers;
struct LOG_read     log_read;



// ---------- Internal function declarations

//! Class specific logging of continuous signals

static void logClassContinuousSignals(void);

//! Class specific logging of DIMs signals

static void logClassDimsSignals(void);



// ---------- External function definitions

void logClassInit(void)
{
    // Initialize liblog structures (log_mgr, log_structs and log_read)

    logStructsInit((struct LOG_mgr *)&dpcom.log.log_mgr, &log_structs, &log_buffers);
    logReadInit(&log_read, &log_structs, &log_buffers);

    dpcom.mcu.dim_ana.dim_idx = DIM_IDX_INVALID;
}



void logClassRtp(void)
{
    // Log the signals

    logClassContinuousSignals();
    logClassDimsSignals();
}



void logClassBgp()
{
    ; // Do nothing
}



// ---------- Internal function definitions

static void logClassContinuousSignals(void)
{
    // static uint8_t log_i_earth_idx = 0;

    struct LOG_log * const log = log_structs.log;

    // Log continuous signals at 10 KHz

    logStoreContinuousRT(&log[LOG_ACQ],  iter.time_ns);      // this can be prolly removed as well
}



static void logClassDimsSignals(void)
{
    // Log EPIC signals. In one pass, we only process one EPIC, to prevent overloading the real-time task.

    int32_t device_multiplexer = dpcls.mcu.log_data.device_index;

    if (device_multiplexer >= 0 && device_multiplexer < FGC_MAX_EPICS)
    {
        // Update the log only if this EPIC is enabled. Otherwise, skip it.
        if (testBitmap(dpcls.mcu.epic.configured_devices_mask, 1 << device_multiplexer) == true)
        {
            log_class_signals.epic_en_wn = dpcls.mcu.log_data.epic_en_wn[device_multiplexer];
            log_class_signals.epic_res_w1 = dpcls.mcu.log_data.epic_res_w1[device_multiplexer];
            log_class_signals.epic_res_w2 = dpcls.mcu.log_data.epic_res_w2[device_multiplexer];

            struct CC_ns_time const timestamp = cctimeMsToNsRT(*(struct CC_ms_time const *)&dpcls.mcu.log_data.time_stamp);

            logStoreContinuousRT(&log_structs.log[LOG_EPIC_EN_WN   + device_multiplexer], timestamp);
            logStoreContinuousRT(&log_structs.log[LOG_EPIC_STAT_W1 + device_multiplexer], timestamp);
            logStoreContinuousRT(&log_structs.log[LOG_EPIC_STAT_W2 + device_multiplexer], timestamp);
        }

        dpcls.mcu.log_data.device_index++;
    }

    profileRegister(FGC_DSP_RT_PROFIL_LOGGING);
}


// EOF
