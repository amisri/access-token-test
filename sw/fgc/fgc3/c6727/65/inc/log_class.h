//! @file  log_class.h
//! @brief Class specific logging

#pragma once


// ---------- Includes

#include <stdint.h>

#include "inc/classes/65/logStructs.h"



// ---------- External variable declarations

extern struct LOG_structs       log_structs;
extern struct LOG_buffers       log_buffers;
extern struct LOG_read          log_read;


// EOF
