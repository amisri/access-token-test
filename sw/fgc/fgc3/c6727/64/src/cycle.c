//! @file  cycle.c
//! @brief Overwrite generic functions since this class does not support cycling mode


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>



// ---------- External function definitions

void cycleInit(void)
{
    ; // Do nothing
}



void cycleRtp(void)
{
    ; // Do nothing
}



void cycleBgp(void)
{
    ; // Do nothing
}


uint32_t cycleGetCycSelAcq(void)
{
    return 0;
}



uint32_t cycleGetTimeMs(void)
{
    return 0;
}



bool cycleIsStarted(void)
{
    return false;
}



bool cycleWasStarted(void)
{
    return false;
}


// EOF
