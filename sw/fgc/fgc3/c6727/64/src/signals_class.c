//! @file  signals_class.c
//! @brief Class specific functionality related to signals


// ---------- Includes

#include <libsig.h>

#include "fgc3/c6727/64/inc/signals_class.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/sig_structs.h"
#include "fgc3/inc/dpcls.h"



// ---------- Platform/class specific function definitions

void signalsClassInit(struct SIG_struct * const sig_struct)
{
    // Make sure all FGC and cclibs constants agree

    CC_STATIC_ASSERT((SIG_TRANSDUCER_DCCT_A == FGC_ADC_SIGNALS_I_DCCT_A ), SIG_TRANSDUCER_DCCT_A   );
    CC_STATIC_ASSERT((SIG_TRANSDUCER_DCCT_B == FGC_ADC_SIGNALS_I_DCCT_B ), SIG_TRANSDUCER_DCCT_B   );
    CC_STATIC_ASSERT((SIG_TRANSDUCER_AUX    == FGC_ADC_SIGNALS_AUX      ), SIG_TRANSDUCER_AUX      );

    sigStructsInit(sig_struct, NULL);
}



void signalsClassRtpAcq(void)
{
    ; // Do nothing
}



void signalsClassRtp(void)
{
    ; // Do nothing
}



void signalsClassUpdateAdcToTrans(void)
{
    ; // Do nothing
}



void signalsClassBgp1ms(void)
{
// @TODO FGC_64: retrive if needed

    // dpcls.dsp.meas.i = 0.0;

    // Update MEAS.I_DIFF_MA

// @TODO FGC_64: needed?    dpcls.dsp.meas.i_diff_ma = sigVarValue(&sig_struct, select, i_meas,  SELECT_MILLI_ABS_DIFF);
}



void signalsClassBgp20ms(void)
{
    ; // Do nothing
}



void signalsClassBgp200ms(void)
{
    ; // Do nothing
}



void signalsClassInterFgc(void)
{
    ; // Do nothing
}


// EOF
