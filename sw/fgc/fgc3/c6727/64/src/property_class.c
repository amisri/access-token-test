//! @file property_class.h
//! @brief Variables associated to class-specific properties


// ---------- Includes

#include <stdint.h>
#include <string.h>

#include "fgc3/c6727/64/inc/property_class.h"



// ---------- External variable definitions

#pragma DATA_SECTION(property,     ".ext_data")

struct Property     property;



// ---------- External function definitions

void propertyClassInit(void)
{
    memset(&property, 0, sizeof(property));
}



void propertyClassGetPointer(uint8_t   *  const base_address,
                             uint32_t     const flags,
                             uint8_t      const sub_sel,
                             uint8_t      const cyc_sel,
                             uint8_t  **        var_pointer,
                             uint32_t  *        n_elem_offset)
{
    // Class 64 does not have an PPM/SUB_DEV properties

    *var_pointer = base_address;

    *n_elem_offset = 0;
}


// EOF
