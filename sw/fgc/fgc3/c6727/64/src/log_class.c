//! @file  log_class.c
//! @brief Class specific logging


// ---------- Includes

#include <liblog.h>
#include <libsig.h>

#include "platforms/fgc3/memmap_dsp.h"

#include "fgc3/c6727/64/inc/log_class.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/spivs.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"


// The variables below are used by logStructsInit() and associated
// to signals in in fgc/def/liblog/ods/class_64.ods

struct Log_class_signals
{
    cc_float   dim_ana[4];         //!< DIM analogue values
    cc_float   tx_buf[7];          //!< SPIVS TX buffer
    cc_float   meas[7];            //!< SPIVS RX buffer: measurement values
    cc_float   i_earth;            //!< Earth current.

    cc_float   i_earth_time_offset;//!< I_EARTH log time offset
};



// ---------- Internal variable definitions

static struct Log_class_signals  log_class_signals;

#include "inc/classes/64/logStructs.h"
#include "inc/classes/64/logStructsInit.h"



// ---------- Constants

static uint32_t const DIM_IDX_INVALID = 0xFFFFFFFF;



// ---------- External variable definitions

#pragma DATA_SECTION(log_read,     ".ext_data")
#pragma DATA_SECTION(log_buffers,  ".ext_log")

struct LOG_structs  log_structs;
struct LOG_buffers  log_buffers;
struct LOG_read     log_read;



// ---------- Internal function declarations

//! Class specific logging of continuous signals

static void logClassContinuousSignals(void);

//! Class specific logging of DIMs signals

static void logClassDimsSignals(void);



// ---------- External function definitions

void logClassInit(void)
{
    // Initialize liblog structures (log_mgr, log_structs and log_read)

    logStructsInit((struct LOG_mgr *)&dpcom.log.log_mgr, &log_structs, &log_buffers);
    logReadInit(&log_read, &log_structs, &log_buffers);

    dpcom.mcu.dim_ana.dim_idx = DIM_IDX_INVALID;

    // Initialize the MEAS log signal names

    static char const * signalNames[FGC_MAX_MEAS_ACQ_GROUP][7] =
    {
        { "VBUSBAR_RS", "ITCR_RS"   , "IFEEDER_R" , "PFEEDER"  , "QFEEDER"  , "ALPHA"    , "QTCR"    },
        { "VBUSBAR_R" , "VBUSBAR_S" , "VBUSBAR_T" , "IFEEDER_R", "IFEEDER_S", "IFEEDER_T", "QFEEDER" },
        { "VBUSBAR_RS", "VBUSBAR_ST", "VBUSBAR_TR", "ITCR_RS"  , "ITCR_ST"  , "ITCR_TR"  , "QTCR"    },
        { "VBUSBAR_RS", "VLL_THETA" , "VLL_FREQ"  , "PLL_OK"   , "VD_TCR"   , "V_CONTROL", "VQ_TCR"  },
        { "ALPHA"     , "PFEEDER"   , "QFEEDER"   , "PLOAD"    , "QLOAD"    , "QTCR"     , "PF"      },
        { "IRES_R"    , "IRES_S"    , "IRES_T"    , "NONE"     , "NONE"     , "NONE"     , "NONE"    },
        { "B_TCR"     , "ALPHA"     , "PI_flagV"  , "PI_flagQ" , "NONE"     , "NONE"     , "NONE"    },
        { "VBUSBAR_R" , "ILOAD_R"   , "ILOAD_S"   , "ILOAD_T"  , "PLOAD"    , "QLOAD"    , "PF"      },
        { "V_CONTROL" , "V_REF_FGC" , "V_REF_INT" , "Q_FEEDER" , "Q_REF_FGC", "Q_REF_INT", "ALPHA"   },
    };

    uint8_t log_idx;

    for (log_idx = 0; log_idx < FGC_MAX_MEAS_ACQ_GROUP; log_idx++)
    {
        uint8_t signal_index;

        for (signal_index = 0; signal_index < 7; signal_index++)
        {
            logStoreSigNameAndUnits(&log_structs.log[LOG_MEAS + log_idx], signal_index, signalNames[log_idx][signal_index], "");
        }
    }

    memset(log_class_signals.tx_buf, 0, 7);
    memset(log_class_signals.meas,   0, 7);

    // The I_EARTH log values are retrieved from the DIM analog signals, scanned
    // during the previous 20 ms DIM cycles. That is why the values are 20 ms old.

    log_class_signals.i_earth_time_offset = -0.02;
}



void logClassRtp(void)
{
    // Log the signals

    logClassContinuousSignals();
    logClassDimsSignals();
}



void logClassBgp()
{
    ; // Do nothing
}



// ---------- Internal function definitions

static void logClassContinuousSignals(void)
{
    static uint8_t log_i_earth_idx = 0;

    struct LOG_log * const log = log_structs.log;

    // Log continuous signals at 10 KHz

    logStoreContinuousRT(&log[LOG_ACQ],  iter.time_ns);

    // Log continuous signals at 5 KHz

    if ((iter.time_us.us % 200) == 0)
    {
        // Retreive the measurements from the SPIVS. First force a transmit to clock in the data.

        spivsSend((uint32_t *)log_class_signals.tx_buf, 7);
        spivsWait(7);
        spivsRead((uint32_t *)log_class_signals.meas, 7);
        spivsValidate((uint32_t *)log_class_signals.meas, (uint32_t *)log_class_signals.meas, 7);

        logStoreContinuousRT(&log[LOG_MEAS + dpcls.mcu.meas.acq_group], iter.time_ns);
    }

    // Log I_EARTH

    if (   iter.start_of_ms5_f == true)
    {
        log_class_signals.i_earth = dpcom.mcu.meas.i_earth[log_i_earth_idx++];

        // The index range is 0..3.

        log_i_earth_idx &= 0x03;

        logStoreContinuousRT(&log[LOG_I_EARTH], iter.time_ns);
    }

    if (iter.start_of_s10_f == true)
    {
        logStoreContinuousRT(&log[LOG_TEMP], iter.time_ns);
    }
}



static void logClassDimsSignals(void)
{
    if (dpcom.mcu.dim_ana.dim_idx < FGC_NUM_DIMS_BRANCH)
    {
        uint8_t  i;

        for (i = 0; i < FGC_N_DIM_ANA_CHANS; ++i)
        {
            log_class_signals.dim_ana[i] = dpcom.mcu.dim_ana.values[i];
        }

        logStoreContinuousRT(&log_structs.log[LOG_DIM + dpcom.mcu.dim_ana.dim_idx],
                             cctimeUsToNsRT(*(struct CC_us_time *)&dpcom.mcu.dim_ana.time_stamp));

        dpcom.mcu.dim_ana.dim_idx = DIM_IDX_INVALID;
    }
}


// EOF
