//! @file property_class.h
//! @brief Variables associated to class-specific properties

#pragma once


// ---------- Includes

#include <stdint.h>



// ---------- External structures, unions and enumerations


//! Structure holding the DSP properties

struct Property
{
    // FIELDBUS properties

    struct Property_fieldbus
    {
        uint32_t                       rt_bad_values;                   //!< FIELDBUS.RT_BAD_VALUES
    } fieldbus;
};



// ---------- External variable declarations

extern struct Property     property;


// EOF
