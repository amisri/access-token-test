//! @file  log_class.h
//! @brief Class specific logging

#pragma once


// ---------- Includes

#include <stdint.h>

#include "inc/classes/62/logStructs.h"



// ---------- Constants

static float const LOG_CLASS_FAST_ACQUISITION_PERIOD = 0.000002;




// ---------- External structures, unions and enumerations

//! This structure contains variables to hold the fast measurements. They are
//! mapped to the signals of LOG_MEAS_FAST and accessed directly by signals_class.c

struct Log_class_fast_meas
{
    cc_float   v;     //!< Fast acquisition voltage measurement
    cc_float   ia;    //!< Fast acquisition I_A measurement
    cc_float   ib;    //!< Fast acquisition I_B measurement
    cc_float   i;     //!< Fast acquisition load measurement
};


// ---------- External variable declarations

extern struct LOG_structs         log_structs;
extern struct LOG_buffers         log_buffers;
extern struct LOG_read            log_read;
extern struct Log_class_fast_meas log_fast_meas;



// ---------- External function declarations

//! Processes the cached selectors in liblog.

void logClassCacheSelectors(void);

//! Sets the name of the acquisition signals for a given group
//!
//! @param[im]   group        Acquisition group

void logClassChangeMeasSignalNames(uint32_t group);

//! Prepares the discontinuos log for fast measuremnts
//!
//! @param[im]   timestamp    Time-stamp of the first sample

void logClassStartFastMeasurement(struct CC_ns_time timestamp);


//! Ends the logging of the discontinuos log for fast measuremnts
//!
//! @param[im]   timestamp    Time-stamp of the last sample

void logClassEndFastMeasurement(struct CC_ns_time timestamp);


//! Stores the fast measurements in the discontinuos log

void logClassStoreFastMeasurement(void);


// EOF
