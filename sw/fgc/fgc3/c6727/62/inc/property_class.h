//! @file  property_class.h
//! @brief Variables associated to class-specific properties

#pragma once


// ---------- Includes

#include <stdint.h>

#include <libintlk.h>

#include <defconst.h>



// ---------- External structures, unions and enumerations

//! Structure holding all the PPM variables

struct Property_ppm
{
    // BIS interlock parameters

    struct Property_ppm_intlk_ppm_t
    {
        struct INTLK_pars              pars;                            //<! INTERLOCK.*                    Parameters for the interlock library
        uint32_t                       output[FGC_MAX_SUB_DEVS];        //!< INTERLOCK.OUTPUT
        struct INTLK_latch_values      latch;                           //!< INTERLOCK.(PPM.){V,I}.MEAS, INTERLOCK.(PPM.)  RESULT
    } intlk;

    struct Property_ppm_meas_t
    {
        float                          pulse;                           //!< MEAS.PULSE.VALUE
        float                          pulses[FGC_N_ACQ_PULSES];        //!< MEAS.PULSES.VALUE
        uintptr_t                      pulses_num_elements;             //!< Number of elements of MEAS.PULSES.VALUE
    } meas;
};


//! Structure holding the DSP properties

struct Property
{
    // PPM properties

    struct Property_ppm                ppm[FGC_MAX_USER_PLUS_1];        //!< Pointer to PPM properties allocated in external memory

    // FIELDBUS properties

    struct Property_fieldbus
    {
        uint32_t                       rt_bad_values;                   //!< FIELDBUS.RT_BAD_VALUES
    } fieldbus;

    // Limits

    struct Property_limits
    {
        float                          i_meas_diff;                     //!< LIMITS.I.MEAS_DIFF       Limit between the measurements of the two DCCTs.
    } limits;

    struct Property_v_probe
    {
        struct Property_v_probe_capa
        {
            uint32_t                   position;                        //!< V_PROBE.CAPA.POSITION
        } capa;

        struct Property_v_probe_load
        {
            uint32_t                   position;                        //!< V_PROBE.LOAD.POSITION
        } load;
    } v_probe;

    struct Property_vs
    {
        float                          offset;                          //!< VS.OFFSET
        float                          gain;                            //!< VS.GAIN
    } vs;

    // Non-ppm BIS interlock parameters

    struct Intlk
    {
        uint32_t                       channel_test;                    //!< BIS.TEST                 Property used to test each channel individuallyuichannel_test
    } intlk;
};



// ---------- External variable declarations

extern struct Property     property;


// EOF
