//! @file  control_class.c
//! @brief Class specific converter control


// ---------- Includes

#include <libsig.h>

#include "fgc3/c6727/62/inc/property_class.h"
#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/log.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/spivs.h"
#include "fgc3/inc/crate.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/pc_state.h"
#include "fgc3/inc/status.h"



// ---------- Constants

static uint8_t const MAX_NUM_SPIVS_FAILS = 2;



// ---------- Internal structures, unions and enumerations

typedef void (*send_ref_func_ptr)(const float, const float);



// ---------- Internal variable definitions

static send_ref_func_ptr controlClassSendRef;



// ---------- Internal function definitions

static void controlClassSendRefVsDsp(float const ref, float const duration)
{

    static uint32_t prev_values_tx[2] = { 0 };
    static int8_t   num_fails         = 0;

    uint32_t values_tx[2];
    uint32_t values_rx[2];

    // Convert the floating point value to a 32 bit signed integer

    values_tx[0] = *(uint32_t *)&ref;
    values_tx[1] = *(uint32_t *)&duration;

    spivsSend(values_tx, 2);

    spivsWait(2);

    spivsRead(values_rx, 2);

    // An error in any of the functions above will be picked up by the
    // condition below.

    // Verify the transmission over the SPIVS bus. The Siramatrix might round
    // down the value sent, which will result in a failure.

    if (spivsValidate(prev_values_tx, values_rx, 2) == SPIVS_INVALID)
    {
        num_fails++;

        if (num_fails > MAX_NUM_SPIVS_FAILS)
        {
            statusSetLatched(FGC_LAT_SPIVS_FLT);
        }
    }
    else
    {
        num_fails = 0;
    }

    prev_values_tx[0] = values_tx[0];
    prev_values_tx[1] = values_tx[1];
}



static void controlClassSendRefSiramatrix(float const ref, float const duration)
{
    // The value sent is a 32 bits signed value. The range is as follows:
    //
    // 7FFF FFFF   + Nominal current     REF.PULSE.NOMINAL
    //
    // 0000 0000                           0.0
    // FFFF FFFF                          -0.....
    //
    // 8000 0000   - Nominal current    -REF.PULSE.NOMINAL

    static uint32_t prev_ref_sent = 0;
    static int8_t   num_fails     = 0;

    uint32_t ref_read;
    uint32_t ref_int;

    // Convert the floating point value to a 32 bit signed integer

    ref_int = (uint32_t)(((float)INT32_MAX / fabs(dpcls.mcu.pulse.nominal)) * ref + 0.5);

    spivsSend(&ref_int, 1);

    spivsWait(1);

    spivsRead(&ref_read, 1);

    // An error in any of the functions above will be picked up by the
    // condition below.

    // Verify the transmission over the SPIVS bus. The Siramatrix might round
    // down the value sent, which will result in a failure.

    if (spivsValidate((uint32_t const *)&prev_ref_sent, &ref_read, 1) == SPIVS_INVALID)
    {
        num_fails++;

        if (num_fails > MAX_NUM_SPIVS_FAILS)
        {
            statusSetLatched(FGC_LAT_SPIVS_FLT);
        }
    }
    else
    {
        num_fails = 0;
    }

    prev_ref_sent = ref_int;
}



static void controlClassSendRefModulator(float const ref, float const duration)
{
    // Apply nominal calibration gain from VS. The modulators are special in
    // that the reference value sent must be positive, even when the user
    // reference is negative.

    float dac_ref = -(ref - property.vs.offset) / property.vs.gain;

    signalsSetDac(SIGNALS_DAC_1, dac_ref);
}



static void controlClassSendRefMididiscap(float const ref, float const duration)
{
    // The reference value is in amps. The gain applied must be that of the DCCT_A.
    // The reference is always positive.

    float dac_ref = fabs(ref / sigVarValue(&sig_struct, transducer, dcct_a, TRANSDUCER_NOMINAL_GAIN));

    signalsSetDac(SIGNALS_DAC_2, dac_ref);
}



// ---------- External function definitions

bool controlClassInit(void)
{
    // Set the function to send the reference

    switch (crateGetType())
    {
        case FGC_CRATE_TYPE_MIDIDISCAP:   controlClassSendRef = &controlClassSendRefMididiscap; break;
        case FGC_CRATE_TYPE_KLYSTRON_MOD: controlClassSendRef = &controlClassSendRefModulator;  break;
        case FGC_CRATE_TYPE_DSP_IGBT:     controlClassSendRef = &controlClassSendRefVsDsp;      break;
        default:                          controlClassSendRef = &controlClassSendRefSiramatrix; break;
    }

    // Initialize libintlk

    interlockInit();

    return true;
}



void controlClassRtp(void)
{
    // Output reference only if the converter is not OFF

    if (   pcStateTest(FGC_STATE_GT_BLOCKING_BIT_MASK) == true
        && iter.state_op != FGC_OP_SIMULATION)
    {
        if (dpcls.mcu.pulse.ref.ready == true)
        {
            controlClassSendRef(dpcls.mcu.pulse.ref.act, dpcls.mcu.pulse.ref.duration);

            dpcls.mcu.pulse.ref.ready = false;
        }
    }

    profileRegister(FGC_DSP_RT_PROFIL_REF_SENT);

    // Only run the interlock check if at least a channel is active

    if (testBitmap(dpcom.mcu.log.ignore_mask, DPCOM_LOG_IGNORE_BIS_BIT_MASK) == false)
    {
        interlockRtp();
    }

    profileRegister(FGC_DSP_RT_PROFIL_INTERLOCK);
}



void controlClassBgp(void)
{
    ; // Do nothing
}


// EOF
