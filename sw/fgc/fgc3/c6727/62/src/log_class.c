//! @file  log_class.c
//! @brief Class specific logging


// ---------- Includes

#include <liblog.h>
#include <libsig.h>

#include "platforms/fgc3/memmap_dsp.h"

#include "fgc3/c6727/62/inc/log_class.h"
#include "fgc3/c6727/62/inc/property_class.h"
#include "fgc3/c6727/inc/cycle.h"
#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/spivs.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"



// ---------- Constants

static uint32_t const DIM_IDX_INVALID = 0xFFFFFFFF;



// ---------- Internal structures, unions and enumerations

//! Internal local variables for the module log_class.

struct Log_class
{
    struct CC_ns_time cycle_start;        //!< Timestamp of the cycle start (C0)
    uint32_t          cycle_sel;          //!< Cycle selector
};


// The variables below are used by logStructsInit() and associated
// to signals in in fgc/def/liblog/ods/class_62.ods

struct Log_class_signals
{
    uint32_t   timing;             //!< Timing pulses

    cc_float   ref;                //!< Reference

    cc_float   i_earth;            //!< Earth current.
    cc_float   dim_ana[4];         //!< DIM analogue values

    cc_float   dac_raw[2];         //!< DAC raw values

    cc_float   i_earth_time_offset;//!< I_EARTH log time offset
};



// ---------- Internal variable definitions

static struct Log_class_signals  log_class_signals;
static struct Log_class          log_class;

#include "inc/classes/62/logStructs.h"
#include "inc/classes/62/logStructsInit.h"



// ---------- External variable definitions

#pragma DATA_SECTION(log_read,     ".ext_data")
#pragma DATA_SECTION(log_buffers,  ".ext_log")

struct LOG_structs          log_structs;
struct LOG_buffers          log_buffers;
struct LOG_read             log_read;
struct Log_class_fast_meas  log_fast_meas;



// ---------- Internal function definitions

static void logClassContinuousSignals(void)
{
    static uint8_t log_i_earth_idx = 0;

    struct LOG_log * const log = log_structs.log;

    // Log continuous signals at 10 KHz

    logStoreContinuousRT(&log[LOG_ACQ]        , iter.time_ns);
    logStoreContinuousRT(&log[LOG_TIME_WINDOW], iter.time_ns);
    logStoreContinuousRT(&log[LOG_MEAS]       , iter.time_ns);
    logStoreContinuousRT(&log[LOG_BIS_FLAGS]  , iter.time_ns);

    // Log BIS_LIMITS

    if (iter.start_of_ms200_f == true)
    {
        logStoreContinuousRT(&log_structs.log[LOG_BIS_LIMITS], iter.time_ns);
    }

    // Log I_EARTH

    if (iter.start_of_ms5_f == true)
    {
        log_class_signals.i_earth = dpcom.mcu.meas.i_earth[log_i_earth_idx++];

        // The index range is 0..3.

        log_i_earth_idx &= 0x03;

        logStoreContinuousRT(&log[LOG_I_EARTH], iter.time_ns);
    }

    // Log ILC_CYC

    if (dpcls.mcu.log.cyc_sel_acq != -1)
    {
        dpcls.mcu.log.meas = property.ppm[dpcls.mcu.log.cyc_sel_acq].meas.pulse;

        logStoreContinuousRT(&log[LOG_ILC_CYC], iter.time_ns);

        dpcls.mcu.log.cyc_sel_acq = -1;
    }

    if (iter.start_of_s10_f == true)
    {
        logStoreContinuousRT(&log[LOG_TEMP], iter.time_ns);
    }
}



static void logClassDimsSignals(void)
{
    if (dpcom.mcu.dim_ana.dim_idx < FGC_NUM_DIMS_BRANCH)
    {
        uint8_t  i;

        for (i = 0; i < FGC_N_DIM_ANA_CHANS; ++i)
        {
            log_class_signals.dim_ana[i] = dpcom.mcu.dim_ana.values[i];
        }

        logStoreContinuousRT(&log_structs.log[LOG_DIM + dpcom.mcu.dim_ana.dim_idx],
                             cctimeUsToNsRT(*(struct CC_us_time *)&dpcom.mcu.dim_ana.time_stamp));

        dpcom.mcu.dim_ana.dim_idx = DIM_IDX_INVALID;
    }
}



// ---------- External function definitions

void logClassInit(void)
{
    // Initialize liblog structures (log_mgr, log_structs and log_read)

    logStructsInit((struct LOG_mgr *)&dpcom.log.log_mgr, &log_structs, &log_buffers);
    logReadInit(&log_read, &log_structs, &log_buffers);

    dpcom.mcu.dim_ana.dim_idx = DIM_IDX_INVALID;

    // Hardcode the LOG.OASIS.*.INTERVAL_NS values since they never change

    dpcls.oasis.fast_interval_ns = 1000000000 * LOG_CLASS_FAST_ACQUISITION_PERIOD; // 500 ksps

    // The I_EARTH log values are retrieved from the DIM analog signals, scanned
    // during the previous 20 ms DIM cycles. That is why the values are 20 ms old.

    log_class_signals.i_earth_time_offset = -0.02;

    // Log ILC_CYC is driven by the MCU

    dpcls.mcu.log.cyc_sel_acq = -1;

    // Zero log_class

    memset(&log_class, 0, sizeof(log_class));
}



void logClassRtp(void)
{
    // Check if a new cycle has started

    if (cycleIsStarted() == true)
    {
        log_class.cycle_start = iter.time_ns;
        log_class.cycle_sel   = cycleGetCycSelAcq();

        logStoreStartContinuousRT(log_read.log, log_class.cycle_start, log_class.cycle_sel);
    }

    // Update timing signals

    log_class_signals.timing  = testBitmap(DIG_SUP_P, DIG_SUP_INP_B0_MASK16) << LOG_TIME_WINDOW_B_0;
    log_class_signals.timing |= testBitmap(DIG_SUP_P, DIG_SUP_INP_B1_MASK16) << LOG_TIME_WINDOW_B_1;
    log_class_signals.timing |= testBitmap(DIG_SUP_P, DIG_SUP_INP_B2_MASK16) << LOG_TIME_WINDOW_ACQ;
    log_class_signals.timing |= testBitmap(DIG_SUP_P, DIG_SUP_INP_B3_MASK16) << LOG_TIME_WINDOW_B_3;

    // Update DAC raw values

    log_class_signals.dac_raw[0] = (float)property_fgc.analog.dac[0];
    log_class_signals.dac_raw[1] = (float)property_fgc.analog.dac[1];

    // Update the reference

    if (testBitmap(DIG_SUP_P, DIG_SUP_INP_B2_MASK16) == true)
    {
        log_class_signals.ref = dpcls.mcu.pulse.ref.user;
    }
    else
    {
        log_class_signals.ref = 0.0F;
    }

    // Log the signals

    logClassContinuousSignals();
    logClassDimsSignals();

    profileRegister(FGC_DSP_RT_PROFIL_LOGGING);
}



void logClassBgp()
{
    interlockIgnoreLogs();
}



void logClassCacheSelectors(void)
{
    logCacheSelectors(&log_structs);
}



void logClassStartFastMeasurement(struct CC_ns_time const timestamp)
{
    logStoreStartDiscontinuousRT(&log_structs.log[LOG_FAST_MEAS],
                                 log_class.cycle_start,
                                 timestamp,
                                 log_class.cycle_sel);
}



void logClassEndFastMeasurement(struct CC_ns_time const timestamp)
{
    logStoreEndDiscontinuousRT(&log_structs.log[LOG_FAST_MEAS], timestamp);
}



void logClassStoreFastMeasurement()
{
    logStoreDiscontinuousRT(&log_structs.log[LOG_FAST_MEAS], 1);
}


// EOF
