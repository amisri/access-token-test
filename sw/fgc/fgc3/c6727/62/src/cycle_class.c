//! @file  cycle_class.c
//! @brief Class specific cycle functions


// ---------- Includes

#include <stdint.h>

#include "fgc3/inc/dpcls.h"



// ---------- Platform/class specific function definitions

uint32_t cycleClassGetStatus(uint8_t sub_sel, uint8_t cyc_sel)
{
    return dpcls.mcu.pulse.status;
}



uint32_t cycleClassGetFuncType(uint8_t sub_sel, uint8_t cyc_sel)
{
    return 0;
}


// EOF
