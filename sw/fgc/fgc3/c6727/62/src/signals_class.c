//! @file  signals_class.c
//! @brief Class specific functionality related to signals


// ---------- Includes

#include <stdbool.h>

#include <libsig.h>

#include "fgc3/c6727/62/inc/log_class.h"
#include "fgc3/c6727/62/inc/property_class.h"
#include "fgc3/c6727/62/inc/signals_class.h"
#include "fgc3/c6727/inc/cycle.h"
#include "fgc3/c6727/inc/dsp.h"
#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/sig_structs.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/crate.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/pc_state.h"
#include "fgc3/inc/status.h"
#include "fgc3/inc/time_fgc.h"



// ---------- Constants

//! Number of fast acquisition per iteration

#define SIGNALS_CLASS_FAST_ACQ_PER_ITER      50

//! Number of fast acquisition per millisecond: 50 acqs/iter * 10 iter/ms

#define SIGNALS_CLASS_FAST_ACQ_PER_MS        (SIGNALS_CLASS_FAST_ACQ_PER_ITER * 10)

//! Maximum duration of the fast acquisition in milliseconds

#define SIGNALS_CLASS_FAST_MAX_DURATION_MS    40

//! Maximum number of fast ADC raw acquisitions: 50 acqs/iter * 10 iter/ms * 40 ms

#define SIGNALS_CLASS_FAST_ACQ_MAX   (SIGNALS_CLASS_FAST_ACQ_PER_MS * SIGNALS_CLASS_FAST_MAX_DURATION_MS)



#include "fgc3/inc/time_fgc.h"


// ---------- Internal structures, unions and enumerations

//! Fast acquisition state

enum Signals_class_fast_acq_state
{
    SIGNALS_CLASS_FAST_ACQ_STATE_NONE,              //!< None
    SIGNALS_CLASS_FAST_ACQ_STATE_DETECTED,          //!< Detected
    SIGNALS_CLASS_FAST_ACQ_STATE_ACQUIRING,         //!< Acquiring
    SIGNALS_CLASS_FAST_ACQ_STATE_PROCESSING,        //!< Processing
};


//! Internal global variables for the module signal_class

struct Signal_class
{
    float               * pulse_acq;                //<! Pulse acquistion

    struct Signals_class_fast_acq
    {
        uint32_t          index;                    //!< Buffer index
        int32_t         * raw_adc_ia_src;           //!< ADC source with I_A
        int32_t         * raw_adc_ib_src;           //!< ADC source with I_B
        int32_t         * raw_adc_v_src;            //!< ADC source with V load
        int32_t           zero;                     //!< Value for NULL ADC source
        enum Signals_class_fast_acq_state state;    //!< Fast acquisiton state
        struct CC_us_time start;                    //!< Start time of fast acquisition window
        struct CC_us_time end;                      //!< End time of fast acquisition window
        struct CC_us_time start_comp;               //!< Compensated start time of fast acquisition
        struct CC_us_time end_comp;                 //!< Compensated end time of fast acquisition
    } fast_acq;
};


//! Structure containing buffers with the raw adc values for fast acquisition
//! In a separate structure to be allocated in external memory

struct Signals_class_fast_buf
{
    int32_t raw_adc_v_buf [SIGNALS_CLASS_FAST_ACQ_MAX];  //!< Buffer with fast acquistion of V load
    int32_t raw_adc_ia_buf[SIGNALS_CLASS_FAST_ACQ_MAX];  //!< Buffer with fast acquistion of I_A
    int32_t raw_adc_ib_buf[SIGNALS_CLASS_FAST_ACQ_MAX];  //!< Buffer with fast acquistion of I_B
};



// ---------- Internal variable definitions

#pragma DATA_SECTION(signals_class_fast_buf,  ".ext_log")

static struct Signals_class_fast_buf  signals_class_fast_buf;
static struct Signal_class            signal_class;



// ---------- Platform/class specific function definitions

void signalsClassInit(struct SIG_struct * const sig_struct)
{
    // Make sure all FGC and cclibs constants agree

    CC_STATIC_ASSERT((SIG_TRANSDUCER_DCCT_A == FGC_ADC_SIGNALS_I_DCCT_A ), SIG_TRANSDUCER_DCCT_A);
    CC_STATIC_ASSERT((SIG_TRANSDUCER_DCCT_B == FGC_ADC_SIGNALS_I_DCCT_B ), SIG_TRANSDUCER_DCCT_B);
    CC_STATIC_ASSERT((SIG_TRANSDUCER_V_MEAS == FGC_ADC_SIGNALS_V_MEAS   ), SIG_TRANSDUCER_V_MEAS);
    CC_STATIC_ASSERT((SIG_TRANSDUCER_V_CAPA == FGC_ADC_SIGNALS_V_CAPA   ), SIG_TRANSDUCER_V_CAPA);
    CC_STATIC_ASSERT((SIG_TRANSDUCER_I_CAPA == FGC_ADC_SIGNALS_I_CAPA   ), SIG_TRANSDUCER_I_CAPA);
    CC_STATIC_ASSERT((SIG_TRANSDUCER_AUX    == FGC_ADC_SIGNALS_AUX      ), SIG_TRANSDUCER_AUX   );

    // Zero internal structures and fast acquisition buffers

    memset(&signals_class_fast_buf, 0, sizeof(signals_class_fast_buf));
    memset(&signal_class,           0, sizeof(signal_class));

    // Ini    
    sigStructsInit(sig_struct, (struct POLSWITCH_mgr *)&dpcls.polswitch_mgr);

    // Set the pulse acquisition (current or voltage)

    if (   crateGetType() == FGC_CRATE_TYPE_KLYSTRON_MOD
        || crateGetType() == FGC_CRATE_TYPE_HMINUS_DISCAP)
    {
        signal_class.pulse_acq = (float *)&dpcls.dsp.meas.v_load;
    }
    else
    {
        signal_class.pulse_acq = (float *)&dpcls.dsp.meas.i_load;
    }

    signal_class.fast_acq.index = 0;
    signal_class.fast_acq.zero  = 0;
    signal_class.fast_acq.state = SIGNALS_CLASS_FAST_ACQ_STATE_NONE;

    signal_class.fast_acq.raw_adc_ia_src = &signal_class.fast_acq.zero;
    signal_class.fast_acq.raw_adc_ib_src = &signal_class.fast_acq.zero;
    signal_class.fast_acq.raw_adc_v_src  = &signal_class.fast_acq.zero;
}



void signalsClassRtpAcq(void)
{
    static bool prev_acq_window_edge = false;

    // Do not start an acquisition if the a post-mortem is ongoing

    if (   signal_class.fast_acq.state      == SIGNALS_CLASS_FAST_ACQ_STATE_NONE
        && dpcom.mcu.log.post_mortem_active == true)
    {
        return;
    }

    // Do not start an acquisition until the first cycle has been received
    
    if (cycleGetCycSelAcq() == 0)
    {
        return;
    }

    bool acq_window_edge = (   timeUsGreaterThanEqual(&iter.time_us, &signal_class.fast_acq.start_comp)
                            && timeUsLessThan        (&iter.time_us, &signal_class.fast_acq.end_comp));

    // Postive edge starts the fast acquisition

    if (prev_acq_window_edge == false && acq_window_edge == true)
    {
        logClassStartFastMeasurement(cctimeUsToNsRT(signal_class.fast_acq.start));
        signal_class.fast_acq.state = SIGNALS_CLASS_FAST_ACQ_STATE_ACQUIRING;
    }

    // Negative edge ends the fast acquisition

    if (prev_acq_window_edge == true && acq_window_edge == false)
    {
        signal_class.fast_acq.state = SIGNALS_CLASS_FAST_ACQ_STATE_PROCESSING;
    }

    prev_acq_window_edge = acq_window_edge;

    if (   signal_class.fast_acq.state == SIGNALS_CLASS_FAST_ACQ_STATE_ACQUIRING
        && signal_class.fast_acq.index  < SIGNALS_CLASS_FAST_ACQ_MAX)
    {
        // The FPGA stores the ADC raw values in a LIFO. Therefore the first value retrieved must
        // be buffered in the last position.

        uint32_t sample_idx_end = signal_class.fast_acq.index;

        signal_class.fast_acq.index += SIGNALS_CLASS_FAST_ACQ_PER_ITER;

        uint32_t sample_idx = signal_class.fast_acq.index;

        while (sample_idx-- > sample_idx_end)
        {
            signals_class_fast_buf.raw_adc_v_buf [sample_idx] = *signal_class.fast_acq.raw_adc_v_src;
            signals_class_fast_buf.raw_adc_ia_buf[sample_idx] = *signal_class.fast_acq.raw_adc_ia_src;
            signals_class_fast_buf.raw_adc_ib_buf[sample_idx] = *signal_class.fast_acq.raw_adc_ib_src;
        }
    }
}



static bool signalsClassDetectAcquisition(void)
{
    bool acq_detected = false;

    if (iter.state_op != FGC_OP_SIMULATION)
    {
        if (dpcls.mcu.pulse.acquisition.ready == true)
        {
            // If possible, retrieve the measurement from the fast acquisition buffer

            if (signal_class.fast_acq.state == SIGNALS_CLASS_FAST_ACQ_STATE_ACQUIRING)
            {
                // Retrieve the acquisition closest to the timing event. Assume that
                // the high-speed acquisitions are available in signals_class_fast_buf
                // since these are stored +-20 ms around the timing event.

                // The first sample corresponds to the time signal_class.fast_acq.start.
                // Calculate the index taking into account that each sample is acquired
                // every 2 us.

                int32_t diff_us = timeUsDiff((struct CC_us_time *)&signal_class.fast_acq.start,
                                             (struct CC_us_time *)&dpcls.mcu.pulse.acquisition.time_us);

                // Assume the difference is always positive

                uint32_t sample_idx = (uint32_t)diff_us / 2 - 1;

                if (sample_idx <= signal_class.fast_acq.index)
                {
                    sig_struct.transducer.named.v_meas.fast_raw = &signals_class_fast_buf.raw_adc_v_buf [sample_idx];
                    sig_struct.transducer.named.dcct_a.fast_raw = &signals_class_fast_buf.raw_adc_ia_buf[sample_idx];
                    sig_struct.transducer.named.dcct_b.fast_raw = &signals_class_fast_buf.raw_adc_ib_buf[sample_idx];

                    sigMgrFast(&sig_struct.mgr);

                    dpcls.dsp.meas.v_load = sigVarValue(&sig_struct, transducer, v_meas, TRANSDUCER_FAST_MEAS);
                    dpcls.dsp.meas.i_load = sigVarValue(&sig_struct, select,     i_meas, SELECT_FAST_MEAS);

                    dpcls.mcu.pulse.acquisition.ready = false;
                    acq_detected = true;                
                }
            }
            else
            {
                dpcls.dsp.meas.v_load = sigVarValue(&sig_struct, transducer, v_meas, TRANSDUCER_MEAS_UNFILTERED);
                dpcls.dsp.meas.i_load = sigVarValue(&sig_struct, select,     i_meas, SELECT_MEAS_UNFILTERED);
            
                dpcls.mcu.pulse.acquisition.ready = false;
                acq_detected = true;
            }
        }
    }
    else
    {
        static bool rising_edge = false;

        // Use DIG_SUP_A since there might not be a state control card
        // A2: simulate pulse
        // A2: acquisition

        if (testBitmap(DIG_SUP_P, DIG_SUP_INP_A2_MASK16) == true)
        {
            *sigVarPointer(&sig_struct, transducer, v_meas, TRANSDUCER_MEAS_UNFILTERED) = dpcls.mcu.pulse.ref.act;
            *sigVarPointer(&sig_struct, select,     i_meas, SELECT_MEAS_UNFILTERED)     = dpcls.mcu.pulse.ref.act;
        }
        else
        {
            *sigVarPointer(&sig_struct, transducer, v_meas, TRANSDUCER_MEAS_UNFILTERED) = 0.0F;
            *sigVarPointer(&sig_struct, select,     i_meas, SELECT_MEAS_UNFILTERED)     = 0.0F;
        }

        if (rising_edge == false && testBitmap(DIG_SUP_P, DIG_SUP_INP_A2_MASK16) == true)
        {
            dpcls.dsp.meas.v_load = sigVarValue(&sig_struct, transducer, v_meas, TRANSDUCER_MEAS_UNFILTERED);
            dpcls.dsp.meas.i_load = sigVarValue(&sig_struct, select,     i_meas, SELECT_MEAS_UNFILTERED);

            acq_detected = true;
        }

        rising_edge = testBitmap(DIG_SUP_P, DIG_SUP_INP_A2_MASK16);
    }

    return acq_detected;

}



void signalsClassRtp(void)
{
    // Offset between the acquisition time and the ADC values
    static int32_t const SIGNALS_CLASS_COMP_ACQ_OFFSET_US = 100;

    static float     acq_pulses[FGC_N_ACQ_PULSES] = { 0.0 };
    static uint32_t  acq_pulses_idx               = 0;

    if (   dpcls.mcu.pulse.fast_acquisition.ready == true
        && signal_class.fast_acq.state            == SIGNALS_CLASS_FAST_ACQ_STATE_NONE)
    {
        // Only latch this request if it is not too late

        if (timeUsLessThan(&iter.time_us, (struct CC_us_time *)&dpcls.mcu.pulse.fast_acquisition.start))
        {
            signal_class.fast_acq.start      = dpcls.mcu.pulse.fast_acquisition.start;
            signal_class.fast_acq.start_comp = dpcls.mcu.pulse.fast_acquisition.start;
            signal_class.fast_acq.end        = dpcls.mcu.pulse.fast_acquisition.end;
            signal_class.fast_acq.end_comp   = dpcls.mcu.pulse.fast_acquisition.end;

            // The samples read correspond to the previous iteration. As a result the acquisition
            // window must be offset by +100us

            timeUsAddOffset(&signal_class.fast_acq.start_comp, SIGNALS_CLASS_COMP_ACQ_OFFSET_US);
            timeUsAddOffset(&signal_class.fast_acq.end_comp,   SIGNALS_CLASS_COMP_ACQ_OFFSET_US);
        }

        dpcls.mcu.pulse.fast_acquisition.ready = false;
    }

    if (signalsClassDetectAcquisition() == true)
    {
        uint32_t cyc_sel = cycleGetCycSelAcq();

        // Update MEAS.PULSE.VALUE

        property.ppm[cyc_sel].meas.pulse = *signal_class.pulse_acq;

        if (acq_pulses_idx < FGC_N_ACQ_PULSES)
        {
            acq_pulses[acq_pulses_idx] = *signal_class.pulse_acq;

            acq_pulses_idx++;
        }

        // Latch the BIS status

        interlockLatchStatus();

        // Notify the MCU that an acquisition has been triggered

        dpcls.dsp.meas.pub_acq_cyc_sel = cyc_sel;

        CC_MEMORY_BARRIER;

        dpcls.dsp.meas.acq_ready = true;
    }

    if (cycleIsStarted() == true)
    {
        uint32_t cyc_sel = dpcom.dsp.cycle.previous.cyc_sel_acq;

        // Transfer the pulses for the cycle user to the property

        memcpy(&property.ppm[cyc_sel].meas.pulses, acq_pulses, sizeof(acq_pulses));

        property.ppm[cyc_sel].meas.pulses_num_elements = acq_pulses_idx;

        // Zero the pulses for the next cycle

        memset(acq_pulses, 0, sizeof(acq_pulses));

        acq_pulses_idx = 0;
    }
}



void signalsClassUpdateAdcToTrans(void)
{
    // This function is only called when the FGC3 is OFF so there is no
    // race condition with signalsClassRtp() when initializing the ADC
    // sources to zero

    signal_class.fast_acq.raw_adc_ia_src = &signal_class.fast_acq.zero;
    signal_class.fast_acq.raw_adc_ib_src = &signal_class.fast_acq.zero;
    signal_class.fast_acq.raw_adc_v_src  = &signal_class.fast_acq.zero;

    // Search for DCCTA_A, DCCT_B and V_MEAS

    uint8_t  idx;

    for (idx = 0; idx < FGC_N_ADCS; idx++)
    {
        switch (property_fgc.analog.internal_signals[idx])
        {
            case FGC_ADC_SIGNALS_I_DCCT_A:
                signal_class.fast_acq.raw_adc_ia_src = (int32_t*)&ANA_FGC3_ADC_SAMPLE_A[idx];
                break;

            case FGC_ADC_SIGNALS_I_DCCT_B:
                signal_class.fast_acq.raw_adc_ib_src = (int32_t*)&ANA_FGC3_ADC_SAMPLE_A[idx];
                break;

            case FGC_ADC_SIGNALS_V_MEAS:
                signal_class.fast_acq.raw_adc_v_src  = (int32_t*)&ANA_FGC3_ADC_SAMPLE_A[idx];
                break;

            default:
                // Do nothing
                break;
        }
    }
}



void signalsClassBgp1ms(void)
{
    // Update the capacitor measurements

    dpcls.dsp.meas.v_capa = sigVarValue(&sig_struct, transducer, v_capa, TRANSDUCER_MEAS_UNFILTERED);
    dpcls.dsp.meas.i_capa = sigVarValue(&sig_struct, transducer, i_capa, TRANSDUCER_MEAS_UNFILTERED);

    // Evaluate I_MEAS warning

    bool i_meas_wrn = sigVarValue(&sig_struct, select, i_meas, SELECT_WARNINGS) != 0;

    statusEvaluateWarnings(FGC_WRN_MEAS, i_meas_wrn);

    // Evaluate I_MEAS fault

    bool i_meas_flt = sigVarValue(&sig_struct, select, i_meas, SELECT_FAULTS);

    statusEvaluateFaults(FGC_FLT_MEAS, i_meas_flt);

    // Convert the fast-acquisition values to physical values

    if (signal_class.fast_acq.state == SIGNALS_CLASS_FAST_ACQ_STATE_PROCESSING)
    {
        // Update the time stamp for the OASIS properties

        dpcls.oasis.timestamp_fast = signal_class.fast_acq.start;

        uint32_t raw_idx = 0;

        while (signal_class.fast_acq.index > 0)
        {
            sig_struct.transducer.named.v_meas.fast_raw = &signals_class_fast_buf.raw_adc_v_buf [raw_idx];
            sig_struct.transducer.named.dcct_a.fast_raw = &signals_class_fast_buf.raw_adc_ia_buf[raw_idx];
            sig_struct.transducer.named.dcct_b.fast_raw = &signals_class_fast_buf.raw_adc_ib_buf[raw_idx];

            raw_idx++;

            sigMgrFast(&sig_struct.mgr);

            log_fast_meas.v  = sigVarValue(&sig_struct, transducer, v_meas, TRANSDUCER_FAST_MEAS);
            log_fast_meas.ia = sigVarValue(&sig_struct, transducer, dcct_a, TRANSDUCER_FAST_MEAS);
            log_fast_meas.ib = sigVarValue(&sig_struct, transducer, dcct_b, TRANSDUCER_FAST_MEAS);
            log_fast_meas.i  = sigVarValue(&sig_struct, select    , i_meas, SELECT_FAST_MEAS);

            logClassStoreFastMeasurement();

            signal_class.fast_acq.index--;
        }

        logClassEndFastMeasurement(cctimeUsToNsRT(signal_class.fast_acq.end));

        signal_class.fast_acq.index = 0;
        signal_class.fast_acq.state = SIGNALS_CLASS_FAST_ACQ_STATE_NONE;

        // Notify the MCU the fast acquisition is ready

        dpcls.dsp.meas.fast_meas_ready = true;
    }
}



void signalsClassBgp20ms(void)
{
    ; // Do nothing
}



void signalsClassBgp200ms(void)
{
    // Update measurement status for the RTD

    dpcls.dsp.status.meas_v = sigVarValue(&sig_struct, transducer, v_meas, TRANSDUCER_FAULTS);
    dpcls.dsp.status.meas_i = sigVarValue(&sig_struct, select,     i_meas, TRANSDUCER_FAULTS);
}



void signalsClassInterFgc(void)
{
    ; // Do nothing
}


// EOF
