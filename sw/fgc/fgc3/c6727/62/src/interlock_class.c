//! @file  interlock_class.c
//! @brief Interface to cclibs libintlk


// ---------- Includes

#include <libintlk.h>

#include "fgc3/c6727/62/inc/property_class.h"
#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/pc_state.h"



// ---------- Internal structures, unions and enumerations

//! Internal variables for the module interlock

struct Interlock_class
{
    cc_float           disable;        //!< Value used with libintlk to disable a check
};



// ---------- Internal variable definitions

static struct Interlock_class interlock_class = { .disable = 0.0F };



// ---------- Platform/class specific function definitions

uint32_t interlockClassGetChannelTest(void)
{
    return property.intlk.channel_test;
}



uint32_t interlockClassGetOutput(uint8_t sub_sel, uint8_t cyc_sel)
{
    return property.ppm[cyc_sel].intlk.output[sub_sel];
}



// ---------- External function definitions

void interlockClassInit(struct Interlock * interlock, struct INTLK_mgr * intlk_mgr)
{
    uint8_t i;

    for (i = 0; i < FGC_MAX_USER_PLUS_1; i++)
    {
        memset(&property.ppm[i].intlk.pars, 0, sizeof(struct INTLK_pars));
    }

    memset(intlk_mgr, 0, sizeof(struct INTLK_mgr));

    // Initialize the libintlk library

    intlkInit(intlk_mgr,
              &interlock->sub_sel,
              &interlock->cyc_sel,
              (uint32_t *)&dpcom.mcu.pc.state_bitmask,
              &interlock_class.disable,
              signalsVarGetPointer(transducer, v_meas, TRANSDUCER_MEAS_UNFILTERED),
              signalsVarGetPointer(select, i_meas, SELECT_MEAS_UNFILTERED),
              0.0001,
              &property.ppm[0].intlk.pars,
              sizeof(struct Property_ppm),
              &property.ppm[0].intlk.latch,
              sizeof(struct Property_ppm),
              FGC_STATE_CYCLING_BIT_MASK);
}


// EOF
