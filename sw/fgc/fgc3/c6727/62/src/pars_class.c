//! @file  pars_class.c
//! @brief Pars function invoked when the property is set


// ---------- Includes

#include "fgc3/c6727/62/inc/log_class.h"



// ---------- External function definitions

void ParsLogMpx(void)
{
    // This function is called when a LOG.MPX property is modified. It needs to cache the
    // pointers to the signals to be logged.

    logClassCacheSelectors();
}



// EOF
