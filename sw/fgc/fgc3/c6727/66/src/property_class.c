//! @file property_class.h
//! @brief Variables associated to class-specific properties


// ---------- Includes

#include <stdint.h>
#include <string.h>



// ---------- External function definitions

void propertyClassInit(void)
{
    ; // Do nothing
}



void propertyClassGetPointer(uint8_t   *  const base_address,
                             uint32_t     const flags,
                             uint8_t      const sub_sel,
                             uint8_t      const cyc_sel,
                             uint8_t  **        var_pointer,
                             uint32_t  *        n_elem_offset)
{
    // Class 66 does not have an PPM/SUB_DEV properties

    *var_pointer = base_address;

    *n_elem_offset = 0;
}


// EOF
