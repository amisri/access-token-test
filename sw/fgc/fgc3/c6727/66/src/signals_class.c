//! @file  signals_class.c
//! @brief Class specific functionality related to signals


// ---------- Includes

#include <libsig.h>

#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/sig_structs.h"



// ---------- Platform/class specific function definitions

void signalsClassInit(struct SIG_struct * const sig_struct)
{
    sigStructsInit(sig_struct, NULL);
}



void signalsClassRtpAcq(void)
{
    ; // Do nothing
}



void signalsClassRtp(void)
{
    ; // Do nothing
}



void signalsClassUpdateAdcToTrans(void)
{
    ; // Do nothing
}



void signalsClassBgp1ms(void)
{
    ; // Do nothing
}



void signalsClassBgp20ms(void)
{
    ; // Do nothing
}



void signalsClassBgp200ms(void)
{
    ; // Do nothing
}



void signalsClassInterFgc(void)
{
    ; // Do nothing
}


// EOF
