//! @file  class.c
//! @brief This file contains all class related definitions


// Instantiate variables in headers with no associated source files

#define DEFPROPS


// ---------- Includes

#include "fgc3/c6727/66/inc/log_class.h"
#include "fgc3/c6727/inc/log.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/status.h"

#pragma DATA_SECTION(prop, ".ext_data")

#include <defprops_dsp_fgc.h>



// ---------- External variable definitions

#pragma DATA_SECTION(dpcom, ".dpram_area")
#pragma DATA_SECTION(dpcls, ".dpram_area")

volatile struct Dpcom dpcom;
volatile struct Dpcls dpcls;



// ---------- Platform/class specific function definitions

void classInit(void)
{
    ; // Do nothing
}



void cycleInit(void)
{
    ; // Do nothing
}



void spivsInit(void)
{
    ; // Do nothing
}

// EOF
