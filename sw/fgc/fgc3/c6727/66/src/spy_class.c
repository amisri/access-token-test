//! @file  spy_class.c
//! @brief Class specific signals accessible through the serial port


// ---------- Includes

#include <stdint.h>

#include <defconst.h>

#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/inc/dpcom.h"



// ---------- Constants

//! Number of ticks per second

#define PLL_TICKS_TO_SECONDS            25000000



// Function memory allocation

#pragma CODE_SECTION(spyClassGetSignal, ".ext_code")



// ---------- Platform/class specific function definitions

float spyClassGetSignal(uint32_t const idx)
{
    switch (idx)
    {
        case FGC_SPY_RAW_A:             return (float)(sigVarValue(&sig_struct, adc, adc_a, ADC_RAW));
        case FGC_SPY_RAW_B:             return (float)(sigVarValue(&sig_struct, adc, adc_b, ADC_RAW));
        case FGC_SPY_RAW_C:             return (float)(sigVarValue(&sig_struct, adc, adc_c, ADC_RAW));
        case FGC_SPY_RAW_D:             return (float)(sigVarValue(&sig_struct, adc, adc_d, ADC_RAW));
        case FGC_SPY_V_A:               return (float)(sigVarValue(&sig_struct, adc, adc_a, ADC_MEAS_UNFILTERED));
        case FGC_SPY_V_B:               return (float)(sigVarValue(&sig_struct, adc, adc_b, ADC_MEAS_UNFILTERED));
        case FGC_SPY_V_C:               return (float)(sigVarValue(&sig_struct, adc, adc_c, ADC_MEAS_UNFILTERED));
        case FGC_SPY_V_D:               return (float)(sigVarValue(&sig_struct, adc, adc_d, ADC_MEAS_UNFILTERED));
        case FGC_SPY_V_FLTR_A:          return (float)(sigVarValue(&sig_struct, adc, adc_a, ADC_MEAS_FILTERED));
        case FGC_SPY_V_FLTR_B:          return (float)(sigVarValue(&sig_struct, adc, adc_b, ADC_MEAS_FILTERED));
        case FGC_SPY_V_FLTR_C:          return (float)(sigVarValue(&sig_struct, adc, adc_c, ADC_MEAS_FILTERED));
        case FGC_SPY_V_FLTR_D:          return (float)(sigVarValue(&sig_struct, adc, adc_d, ADC_MEAS_FILTERED));
        case FGC_SPY_I_EARTH:           return (float)(dpcom.mcu.meas.i_earth[0]);
        case FGC_SPY_T_FGC_IN:          return (float)(dpcom.mcu.temp.in);
        case FGC_SPY_T_FGC_OUT:         return (float)(dpcom.mcu.temp.out);
        case FGC_SPY_DSPISR:            return (float)(iter.sig_cpu_usage_us);
        case FGC_SPY_PLL_STATE:         return (float)(dpcom.mcu.pll.state);
        case FGC_SPY_PLL_E:             return (float)(dpcom.mcu.pll.error         / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_INTE:          return (float)(dpcom.mcu.pll.integrator    / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_FILT_INTE:     return (float)(dpcom.mcu.pll.filtered_integrator / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_DAC:           return (float)(dpcom.mcu.pll.dac);
        case FGC_SPY_PLL_ETH_SYNC:      return (float)(dpcom.mcu.pll.eth_sync_cal  / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_EXT_E:         return (float)(dpcom.mcu.pll.ext_error     / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_E18_E:         return (float)(dpcom.mcu.pll.e18_error     / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_E19_E:         return (float)(dpcom.mcu.pll.e19_error     / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_EXT_A_E:       return (float)(dpcom.mcu.pll.ext_avg_error / (float)PLL_TICKS_TO_SECONDS);
        case FGC_SPY_PLL_NET_A_E:       return (float)(dpcom.mcu.pll.net_avg_error / (float)PLL_TICKS_TO_SECONDS);
        default:                        return -1.23456789;
    }
}


// EOF
