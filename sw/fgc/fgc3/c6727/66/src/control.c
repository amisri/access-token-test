//! @file  control.c
//! @brief Interface to the cclibs libraries


// ---------- Includes


#include "fgc3/c6727/inc/log.h"
#include "fgc3/c6727/inc/property.h"



// Function memory allocation

#pragma CODE_SECTION(controlBgp, ".ext_code")



// ---------- External function definitions

void controlInit(void)
{
    memset(&property_fgc.test.dsp, 0, sizeof(property_fgc.test.dsp));

    logInit();
}



void controlRtp(void)
{
    logRtp();
}



void controlBgp(void)
{
    logBgp();
}



// EOF
