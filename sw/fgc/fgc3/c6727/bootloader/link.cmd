// ----------------------------------------------------------------------------
//  File:       link.cmd
//  Purpose:    DSP Program Linker file for FGC3 Hardware bootloader 
// ----------------------------------------------------------------------------

-e _bootEntryPoint  // Command line : --entry_point _bootEntryPoint inside rts6700.lib
-heap  400h         // Command line : --heap_size  0x400
-stack 800h         // Command line : --stack_size 0x800


// ----------------------------------------------------------------------------
//  Specify the Memory Configuration
// ----------------------------------------------------------------------------


MEMORY
{
    // 384 Kb: Internal ROM

    IROM_BOOT     (RX)   : origin = 00000000h,   length = 00020000h
    IROM_DSPLIB   (RX)   : origin = 00020000h,   length = 0000C000h
    IROM_FASTRTS  (RX)   : origin = 0002C000h,   length = 00004000h
    IROM_BIOS     (RX)   : origin = 00030000h,   length = 00030000h

    // 256 Kb: Internal RAM (no cache in use)

    IRAM_BOOT     (RWXI) : origin = 10000000h,   length = 000003FCh    // Boot program
    IRAM_START    (RWXI) : origin = 100003FCh,   length = 00000004h    // Entry point function for the Boot or Main
    IRAM_VEC      (RWXI) : origin = 10000400h,   length = 00000200h
    IRAM          (RWXI) : origin = 10000600h,   length = 0003fa00h

    //  64 Mb: External SDRAM (CE0):
    //   1 MB: code
    //   2 MB: data
    //  60 MB: LOG signals from the liblog library

    SDRAM_CODE    (RWXI) : origin = 80000000H,   length = 00100000h
    SDRAM_DATA    (RWXI) : origin = 80100000h,   length = 00200000h
    SDRAM_LOG     (RWXI) : origin = 80300000h,   length = 03D00000h

    //  16 Kb: External DPRAM (CE1)

    DPRAM         (RWXI) : origin = 90000000h,   length = 00004000h

    //  32 Kb: External expansion range

    ExtCE2        (RWXI) : origin = 90200000h,   length = 00008000h    // Expansion CE2-space
    ExtCE3        (RWXI) : origin = 90280000h,   length = 00008000h    // Expansion CE3-space
    ExtCE4        (RWXI) : origin = 90300000h,   length = 00008000h    // Expansion CE4-space
}


// ----------------------------------------------------------------------------
//  Specify the Output Sections
// ----------------------------------------------------------------------------

SECTIONS
{
    .myBootSection: load > IRAM_BOOT
    {
        *(.myBootSection)
        code_size = .;                                                 // Get the destination as a global variable
    }

    .ext_data:     load > SDRAM_DATA
    {

    }

    .ext_code:     load > SDRAM_CODE
    {

    }

    .ext_log:      load > SDRAM_LOG
    {

    }
    
    .dpram_area:   load > DPRAM
    {

    }

    .cinit:        load > IRAM
    .pinit:        load > IRAM
    .cio:          load > IRAM
    .const:        load > IRAM
    .data:         load > IRAM
    .switch:       load > IRAM
    .sysmem:       load > IRAM
    .tables:       load > IRAM
    .stack:        load > IRAM , fill = 0x0BADBABE   
}

// EOF
