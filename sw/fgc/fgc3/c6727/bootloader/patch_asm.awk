#! /usr/bin/awk -f

###################################################################################
# This script removes the first three assembly lines of DspBootLoader. The original:
#
# _DspBootLoader:
# ;*----------------------------------------------------------------------------*
# ;*   SOFTWARE PIPELINE INFORMATION
# ;*      Disqualified loop: Software pipelining disabled
# ;*----------------------------------------------------------------------------*
#            STW     .D2T2   B11,*SP--(8)      ; |96| 
#            STW     .D2T2   B10,*+SP(4)       ; |96| 
#            NOP             1
#    .word 0x01010101
#    .global _bootEntryPoint
#
# is transformed as
#
#
# _DspBootLoader:
# ;*----------------------------------------------------------------------------*
# ;*   SOFTWARE PIPELINE INFORMATION
# ;*      Disqualified loop: Software pipelining disabled
# ;*----------------------------------------------------------------------------*
#    .word 0x01010101
#    .global _bootEntryPoint
###################################################################################

BEGIN { RS = "\n" }
{ 
    while (getline > 0) 
    { 
        if($1 == "_dspBootLoader:") 
        {
            print $0

            while (getline > 0 && $1 != "STW") 
            {
                print $0
            }
            while (getline > 0 && $1 != "NOP") 
            {

            }            
        } 
        else 
        { 
            print $0 
        } 
    } 
}