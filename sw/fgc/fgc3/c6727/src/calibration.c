//! @file  calibration.c
//! @brief FGC3 analogue calibration: ADCs and DACs


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <defconst.h>

#include "fgc3/c6727/inc/anaCard.h"
#include "fgc3/c6727/inc/calibration.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcom.h"

#include "fgc3/inc/time_fgc.h"



// ---------- Constants

//! Number of iterations to wait before starting the samples acquisition.

#define CALIBRATION_SETTLING_TIME         10000 * 5

//! Number of iterations to wait until the ADC acquisition is ready.

#define CALIBRATION_WAIT_ADC_READY        (CALIBRATION_SETTLING_TIME + CALIBRATION_NUM_SAMPLES)

//! Number of iterations to wait until the DAC acquisition is ready.

#define CALIBRATION_WAIT_DAC_DCCT_READY   CALIBRATION_NUM_SAMPLES

//! Internal ADC used for DAC calibration.

#define CAL_ADC_FOR_DAC_MEAS              SIGNALS_INTERNAL_ADC_4_BIT_MASK



// ---------- Internal structures, unions and enumerations

//! Calibration states

enum Calibration_state
{
    CAL_STATE_WAIT,                                  //!< Wait for calibration request
    CAL_STATE_PREPARE,                               //!< Prepare HW for calibration
    CAL_STATE_ACQUIRE,                               //!< Acquire samples
    CAL_STATE_READY,                                 //!< Samples are ready
    CAL_STATE_TERMINATE,                             //!< HW is restored to its initial state
    CAL_NUM_STATES
};

//! Internal global variables for the module calibration

struct Calibration
{
    volatile bool                      active_f;     //!< Ongoing calibration
    enum Calibration_state             state;        //!< Calibration state
    enum Dpcom_cal_level               level;        //!< The signal being sampled (OFFSET/POS/NEG)
    enum Dpcom_cal_action              action;       //!< Calibration action
    enum Signals_dac                   dac;          //!< Which DAC is being calibrated
    enum Signals_adc_bitmask           ext_adc;      //!< Which external ADC is being calibrated
    enum Signals_dcct_bitmask          dcct;         //!< Which DCCT is being calibrated
    uint32_t                           avg_steps;    //!< Averaging period in 200-sample steps
    uint32_t                           acquire_cnt;  //!< Acquire counter
};



// ---------- Internal variable definitions

static struct Calibration calibration;


static int32_t const calibration_dac_ref[DPCOM_CAL_NUM_CALS] =
{
    0,
    CALIBRATION_DAC_REF,
    -CALIBRATION_DAC_REF
};



// Function memory allocation

#pragma CODE_SECTION(calibrationInit,           ".ext_code")
#pragma CODE_SECTION(calibrationBgp,            ".ext_code")
#pragma CODE_SECTION(calibrationStateWait,      ".ext_code")
#pragma CODE_SECTION(calibrationStatePrepare,   ".ext_code")
#pragma CODE_SECTION(calibrationStateAcquire,   ".ext_code")
#pragma CODE_SECTION(calibrationStateReady,     ".ext_code")
#pragma CODE_SECTION(calibrationStateTerminate, ".ext_code")



// ---------- Internal function declarations

//! Waiting for a calibration request

static void calibrationStateWait(void);

//! Prepare the calibration

static void calibrationStatePrepare(void);

//! Acquire the samples

static void calibrationStateAcquire(void);

//! The samples are ready

static void calibrationStateReady(void);

//! The calibration is complete

static void calibrationStateTerminate(void);



// ---------- External function definitions

void calibrationInit(void)
{
    calibration.action   = DPCOM_CAL_REQ_NONE;
    calibration.state    = CAL_STATE_WAIT;
    calibration.dac      = SIGNALS_DAC_NONE;
    calibration.ext_adc  = SIGNALS_EXTERNAL_ADC_ZERO_BIT_MASK;
    calibration.dcct     = SIGNALS_DCCT_ZERO_BIT_MASK;
    calibration.active_f = false;
}



bool calibrationIsActive(void)
{
    return (calibration.active_f);
}



enum Dpcom_cal_action calibrationGetAction(void)
{
    return (calibration.action);
}



void calibrationBgp(void)
{
    // All channels will be processed in parallel (e.g. calibrate all internal ADCs in
    // parallel). A calibration typically requires a number of steps that are abstracted
    // thanks to the state machine pictured below.
    //
    // A simple case is when only one level is measured (OFFSET, POS or NEG) that will
    // involve a loop through the state machine in one sequence. Examples of this is the
    // DCCT calibration requests.
    //
    // Another case will involve acquiring several reference signals (an offset reference,
    // a positive reference and a negative one) automatically. Internal ADC calibration is
    // an example of such case. Then the state machine will loop on the sequence of states
    // to measure each signal and calculate the calibration coefficients in the end.
    //
    // Finally, it is also possible to calibrate the two internal DACs (in FGC3) in one
    // request. DACs are calibrated in sequence by measuring the three reference voltages
    // (OFFSET/POS/NEG). The internal ADC used for DAC calibration is hard-coded, identified
    // by constant CAL_ADC_FOR_DAC_MEAS. On FGC3, this is usually ADC4 (AUX channel) that is
    // dedicated to that.
    //
    // State machine:
    //
    //        CAL_REQUEST_WAIT
    //         |
    //         v
    //   /--- CAL_REQUEST_PREPARE <---------\ Loop on signal type (OFFSET/POS/NEG)
    //   |     |                            | and loop on DAC id for DAC calibration.
    //   |     v                            |
    //   |    CAL_REQUEST_ACQUIRE(*)        |    (*) Transition to CAL_REQUEST_READY done in the ISR
    //   |     |                            |
    //   |     v                            |
    //   |    CAL_REQUEST_READY     --------/
    //   |     |
    //   |     v
    //   \--> CAL_REQUEST_TERMINATE
    //         |
    //         v

    typedef void (*state_func_ptr_t) (void);

    static state_func_ptr_t stateFuncs[CAL_NUM_STATES] =
    {
        calibrationStateWait,
        calibrationStatePrepare,
        calibrationStateAcquire,
        calibrationStateReady,
        calibrationStateTerminate,
    };

    stateFuncs[calibration.state]();
}



// ---------- Internal function definitions

static void calibrationStateWait(void)
{
    // Check if the MCU has requested a new calibration.

    if (dpcom.mcu.cal.req_f == false)
    {
        return;
    }

    calibration.avg_steps = dpcom.mcu.cal.ave_steps;
    calibration.active_f  = true;
    calibration.action    = (enum Dpcom_cal_action)dpcom.mcu.cal.action;

    // Set the calibration level based on the type of calibration

    if (calibration.action == DPCOM_CAL_REQ_DCCT)
    {
        calibration.level = (enum Dpcom_cal_level)dpcom.mcu.cal.idx;
        calibration.dcct  = (enum Signals_dcct_bitmask)dpcom.mcu.cal.chan_mask;
    }
    else if (calibration.action == DPCOM_CAL_REQ_EXTERNAL_ADC)
    {
        calibration.level   = (enum Dpcom_cal_level)dpcom.mcu.cal.idx;
        calibration.ext_adc = (enum Signals_adc_bitmask)dpcom.mcu.cal.chan_mask;
    }
    else
    {
        calibration.level = DPCOM_CAL_OFFSET;

        if (calibration.action == DPCOM_CAL_REQ_DAC)
        {
            calibration.dac = SIGNALS_DAC_1;
        }
    }

    // Save the current state of the analogue crossbar

    anaCardSaveState();

    // Acknowledge MCU: request is copied to the DSP

    dpcom.dsp.cal.complete_f = false;
    dpcom.mcu.cal.req_f      = false;

    // Transition to PREPARE

    calibration.state = CAL_STATE_PREPARE;
}



static void calibrationStatePrepare(void)
{
    static enum Ana_card_crossbar_input const crossbar_input[DPCOM_CAL_NUM_CALS] =
    {
        ANA_CARD_INPUT_CAL_ZERO,
        ANA_CARD_INPUT_CAL_POSREF,
        ANA_CARD_INPUT_CAL_NEGREF,
    };

    switch (calibration.action)
    {
        case DPCOM_CAL_REQ_INTERNAL_ADC:

            anaCardConfigCrossbar(crossbar_input[calibration.level], SIGNALS_INTERNAL_ADCS_BIT_MASK);

            // Notify libsig that the ADC will be used for calibration

            signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_1, true);
            signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_2, true);
            signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_3, true);
            signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_4, true);

            break;

        case DPCOM_CAL_REQ_EXTERNAL_ADC:

            if (calibration.ext_adc == SIGNALS_EXTERNAL_ADC_1_BIT_MASK)
            {
                signalsCalAdcStatus(SIGNALS_EXTERNAL_ADC_1, true);
            }
            else if (calibration.ext_adc == SIGNALS_EXTERNAL_ADC_2_BIT_MASK)
            {
                signalsCalAdcStatus(SIGNALS_EXTERNAL_ADC_2, true);
            }

            break;

        case DPCOM_CAL_REQ_DCCT:

            anaCardConfigCrossbar(ANA_CARD_INPUT_NONE, SIGNALS_INTERNAL_ADC_ZERO_BIT_MASK);
            break;

        case DPCOM_CAL_REQ_DAC:

            signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_4, true);

            if (calibration.dac == SIGNALS_DAC_1)
            {
                anaCardConfigCrossbar(ANA_CARD_INPUT_DAC1, CAL_ADC_FOR_DAC_MEAS);
                anaCardSetDacRaw(SIGNALS_DAC_1, calibration_dac_ref[calibration.level]);
            }
            else
            {
                anaCardConfigCrossbar(ANA_CARD_INPUT_DAC2, CAL_ADC_FOR_DAC_MEAS);
                anaCardSetDacRaw(SIGNALS_DAC_2, calibration_dac_ref[calibration.level]);
            }
            break;

        default:
            break;
    }

    // Prepare the acquire counter for the acquire state

    calibration.acquire_cnt = iter.counter;

    switch (calibration.action)
    {
        case DPCOM_CAL_REQ_INTERNAL_ADC:   // Fall through
        case DPCOM_CAL_REQ_EXTERNAL_ADC: calibration.acquire_cnt += CALIBRATION_WAIT_ADC_READY;      break;
        case DPCOM_CAL_REQ_DCCT:           // Fall through
        case DPCOM_CAL_REQ_DAC:          calibration.acquire_cnt += CALIBRATION_WAIT_DAC_DCCT_READY; break;
        default:                         calibration.acquire_cnt += 0;                               break;
    }

    calibration.state = CAL_STATE_ACQUIRE;
}



static void calibrationStateAcquire(void)
{
    if (iter.counter >= calibration.acquire_cnt)
    {
        calibration.state = CAL_STATE_READY;
    }
}



static void calibrationStateReady(void)
{
    switch (calibration.action)
    {
        case DPCOM_CAL_REQ_INTERNAL_ADC:

            signalsCalAdcReady(calibration.level);
            break;

        case DPCOM_CAL_REQ_DAC:

            signalsCalDacReady(calibration.dac,
                               calibration_dac_ref[calibration.level],
                               calibration.level);
            break;

        case DPCOM_CAL_REQ_DCCT:

            if (testBitmap(calibration.dcct, SIGNALS_DCCT_1_BIT_MASK))
            {
                signalsCalDcctReady(SIGNALS_DCCT_1, calibration.level);
            }

            if (testBitmap(calibration.dcct, SIGNALS_DCCT_2_BIT_MASK))
            {
                signalsCalDcctReady(SIGNALS_DCCT_2, calibration.level);
            }

            break;

        default:
            break;
    }

    // Change calibration level (offset/pos/neg) based on the table below:
    //
    // -----------------------------------
    // |       | Calibration |   Level   |
    // ----------------------------------|
    // | ADCs  |  Parallel   | Automatic |
    // | DACs  | Sequential  | Automatic |
    // | DCCTs |  Parallel   |  Manual   |
    // -----------------------------------

    if (calibration.action == DPCOM_CAL_REQ_INTERNAL_ADC)
    {
        calibration.state = (++calibration.level >= DPCOM_CAL_NUM_CALS ?
                             CAL_STATE_TERMINATE:
                             CAL_STATE_PREPARE);
    }
    else if (calibration.action == DPCOM_CAL_REQ_DAC)
    {
        if (++calibration.level >= DPCOM_CAL_NUM_CALS)
        {
            calibration.state = (++calibration.dac >= SIGNALS_DAC_NUM ?
                                 CAL_STATE_TERMINATE : CAL_STATE_PREPARE);
            calibration.level = DPCOM_CAL_OFFSET;

            // Restore the DAC values

            anaCardSetDacRaw(SIGNALS_DAC_1, calibration_dac_ref[DPCOM_CAL_OFFSET]);
            anaCardSetDacRaw(SIGNALS_DAC_2, calibration_dac_ref[DPCOM_CAL_OFFSET]);
        }
        else
        {
        	calibration.state = CAL_STATE_PREPARE;
        }
    }
    else // DCCT
    {
        calibration.state = CAL_STATE_TERMINATE;
    }
}



static void calibrationStateTerminate(void)
{
    // Restore the state of the analogue crossbar

    anaCardRestoreState();

    // Notify libsig that the ADC is no longer being used for calibration

    if (calibration.action == DPCOM_CAL_REQ_INTERNAL_ADC)
    {
        signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_1, false);
        signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_2, false);
        signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_3, false);
        signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_4, false);
    }
    else if (calibration.action == DPCOM_CAL_REQ_EXTERNAL_ADC)
    {
        signalsCalAdcStatus(SIGNALS_EXTERNAL_ADC_1, false);
        signalsCalAdcStatus(SIGNALS_EXTERNAL_ADC_2, false);
    }
    else if (calibration.action == DPCOM_CAL_REQ_DAC)
    {
        signalsCalAdcStatus(SIGNALS_INTERNAL_ADC_4, false);
    }

    // Reset the internal state

    calibration.action   = DPCOM_CAL_REQ_NONE;
    calibration.state    = CAL_STATE_WAIT;
    calibration.dac      = SIGNALS_DAC_NONE;
    calibration.ext_adc  = SIGNALS_EXTERNAL_ADC_ZERO_BIT_MASK;
    calibration.dcct     = SIGNALS_DCCT_ZERO_BIT_MASK;
    calibration.active_f = false;

    // Acknowledge the MCU

    dpcom.dsp.cal.complete_f = true;
}


// EOF
