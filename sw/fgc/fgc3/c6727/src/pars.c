//! @file  pars.c
//! @brief These functions are called when a group of properties is set/modified.
//!
//! The functions are invoked  at the end of the millisecond ISR provided it is not a
//! 10 millisecond boundary when lots of parameters are sent to the MCU.


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <defprops_dsp_fgc.h>

#include "fgc3/c6727/inc/anaCard.h"
#include "fgc3/c6727/inc/log.h"
#include "fgc3/c6727/inc/pars.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/bitmap.h"



// ---------- Internal structures, unions and enumerations

//! Internal global variables for the module pars

struct Pars
{
    uint32_t group_set_mask;       //!< Parameter group set mask
};



// ---------- Internal variable definitions

extern void (*pars_func[])(void);

static struct Pars pars = { .group_set_mask = 0 };



// ---------- External function definitions

void parsUpdateMask(uint32_t const pars_idx)
{
    pars.group_set_mask |= 1 << (pars_idx - 1);
}



bool parsGroupSet(void)
{
    // This function is called in the background task if the par_group_set_mask is
    // non-zero, which indicates that at least one parameter group has had a property
    // set. It does not necessarily mean that the property has changed value. The
    // function identifies the related property group from the bit set in the mask
    // and calls the appropriate function. Only one function is ever executed per
    // millisecond to reduce the risk of overrun. If a function is run, the associated
    // bit is cleared in the change mask.

    uint32_t  idx;
    uint32_t  mask;

    if (pars.group_set_mask == 0)
    {
        return false;
    }

    // Search for LSB which is set

    for (idx = 0, mask = 1; !(pars.group_set_mask & mask); mask <<= 1, idx++)
    {
        ;
    }

    // Run parameter group function

    pars_func[idx]();

    clrBitmap(pars.group_set_mask, mask);

    return true;
}



void parsRunAllFunc(void)
{
    uint32_t i; 

    for (i = 0; i < NUM_PARS_FUNC - 1; i++)
    {
        pars_func[i]();
    }
}



// ---------- Pars function definitions

void ParsAnalogBus(void)
{
    // This function is called when ADC.ANALOG_BUS.SIGNAL is modified.
    // Particular values:
    //     "NC" (=ANABUS_INPUT_NONE): floating analogue bus
    //     "COM" (=ANABUS_INPUT_COM): COM port on the front panel used as an input
    //
    // Note that if the user is trying to set the property equal to "COM" when the SWBUS
    // register on the analogue card is zero, the read-back of the property will be "NC".

    enum Ana_card_crossbar_input anabus_input = (enum Ana_card_crossbar_input)property_fgc.analog.bus.signal;

    if (anabus_input < ANA_CARD_INPUT_MAX_VALUE)
    {
        // Use the current SWBUS value as the ADC mask. (Yes, it works.)

        anaCardConfigCrossbar(anabus_input, property_fgc.analog.bus.swbus);
    }
    else
    {
        // If not in the valid range, restore the property value based on the HW state

        anaCardUpdateProperties();
    }
}



void ParsCalFactors(void)
{
    // This function is called when a calibration property that impacts the
    // calibration factors is modified. The values will be immediately
    // taken by libSig. No need to call any specific function.
}



void ParsCalDac(void)
{
    // DAC calibration parameters have changed.


    signalsCalDacTrigger(SIGNALS_DAC_1);
    signalsCalDacTrigger(SIGNALS_DAC_2);
}



void ParsAdcSignals(void)
{
    // This function is called when ADC.INTERNAL.SIGNALS is modified

    signalsUpdateAdcToTrans();
}



void ParsLogReadRequest(void)
{
    // This function is called when accessing the log buffers

    logParsReadRequest();
}



void ParsTest(void)
{
    ; // Do nothing
}


// EOF
