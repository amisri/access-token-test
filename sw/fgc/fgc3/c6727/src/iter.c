//! @file  iter.c
//! @brief Functionality processed at each iteration


// ---------- Includes

#include <stdint.h>
#include <string.h>

#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/histogram.h"
#include "fgc3/inc/time_fgc.h"



// Global variable definitions

struct Iter  iter;



// ---------- External function definitions

void iterInit(void)
{
    // The DSP_RT_CPU signal has an offset of 100 us

    iter.sig_cpu_usage_offset = -1.0E-4;
}



void iterStart(void)
{
    // Start measurement of the elapsed time for the real-time processing - visible in FGC.DEBUG.DSP.ELAPSED_TIME[0]

    profileStart(0);

    // Save the profile times selected by FGC.DEBUG.DSP.RT_PROFILE in signals for the ACQ log
    // PowerSpy will time shift them by one iteration to appear for the previous iteration.

    profileSaveSignals();

    // Register microseconds since the start of the iteration as the START of RT processing

    profileRegister(FGC_DSP_RT_PROFIL_START);

    // Keep track of the maximum value of this start time in FGC.DEBUG.DSP.RT_MAX_US[0]

    profileMaxPoint(0,FGC_DSP_RT_PROFIL_START);

    iter.counter++;

    // Update times and time boundary flags

    uint32_t ms_time = timeGetUtcTimeMs();

    iter.time_us.us = timeGetUtcTimeUs();

    if (iter.time_ms.ms != ms_time)
    {
        iter.time_ms.ms  = ms_time;

        iter.start_of_ms1_f   = true;
        iter.start_of_ms5_f   = ((ms_time %  5) == 0);
        iter.start_of_ms10_f  = ((ms_time % 10) == 0);

        if (iter.start_of_ms10_f == true)
        {
            iter.start_of_ms20_f  = ((ms_time %  20) == 0);
            iter.start_of_ms200_f = ((ms_time % 200) == 0);
        }

        uint32_t s_time = timeGetUtcTime();

        if (iter.time_ms.secs.abs != s_time)
        {
            iter.time_us.secs.abs = s_time;
            iter.time_ms.secs.abs = s_time;

            iter.start_of_s1_f  = true;
            iter.start_of_s10_f = (iter.time_ms.secs.abs % 10) == 0;
        }
    }
    else
    {
        iter.start_of_ms1_f    = false;
        iter.start_of_ms5_f    = false;
        iter.start_of_ms10_f   = false;
        iter.start_of_ms20_f   = false;
        iter.start_of_ms200_f  = false;
        iter.start_of_s1_f     = false;
        iter.start_of_s10_f    = false;
    }

    // Calculate time as a double in seconds and as a CC_ns_time

    iter.time_ns = cctimeUsToNsRT(iter.time_us);

    // Cache STATE.OP for efficiency

    iter.state_op = dpcom.mcu.state_op;
}



void iterEnd(void)
{
    // Increment the watch-dog in the MCU

    dpcom.dsp_rtp_alive++;

    // Calculate the time taken by the ISR on this iteration using elapsed_time profile [0]

    profileStop(0);

    iter.sig_cpu_usage_us = (cc_float)profile.dsp_timing.elapsed_time[0] * (1.0E6 / PROFILE_CLOCK_HZ);

    // Return the average DSP RT usage to the MCU over the last second

    static float accu_cpu_usage_us = 0.0;

    accu_cpu_usage_us += iter.sig_cpu_usage_us;

    if (iter.start_of_s1_f == true)
    {
        dpcom.dsp.cpu_usage = (uint32_t)(accu_cpu_usage_us * 1.0E-4);

        accu_cpu_usage_us = 0.0;
    }

    // Provide the maximum value of cpu_usage in FGC.DEBUG.DSP.RT_MAX_US[2]

    profileMaxTime(2, (uint32_t)iter.sig_cpu_usage_us);

    // Register microsecond time since the start of the iteration of the end of RT processing

    profileRegister(FGC_DSP_RT_PROFIL_END);
}


//EOF
