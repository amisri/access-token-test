//! @file  spy.c
//! @brief Provides access to FGC3 signals through the serial port


#define CONTROL_GLOBALS


// ---------- Includes

#include <stdint.h>

#include <defconst.h>

#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/spy.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/memmap.h"



// ---------- Constants

//! Synchronizations word

#define SPY_SYNC_VALUE             0xFFFFFFFF



// ---------- External function definitions

void spySend(void)
{
    // Values are only sent if someone is listening on the port, that is if the
    // command field in the control register is non-zero.

    static uint32_t const sync_value = SPY_SYNC_VALUE;

    // Reset the internal state if the USB interface is not ready or an external
    // reset was triggered.


    if ((SPY_CTRL_P & SPY_CTRL_CMD_MASK32) == 0)
    {
        return;
    }

    if (testBitmap(SPY_CTRL_P, SPY_CTRL_STATUS_USB_RDY_MASK32) == false)
    {
        property_fgc.spy.errors++;

        return;
    }

    // Write the synch word

    SPY_BUFFER_P = *(float *)&sync_value;

    // Write the six signals

    uint8_t  channel;

    for (channel = 0; channel < FGC_N_SPY_CHANS; channel++)
    {
        SPY_BUFFER_P = spyClassGetSignal(property_fgc.spy.mpx[channel]);
    }
}


// EOF
