//! @file  signals.c
//! @brief Functionality related to signal processing
//!
//! ADC, DAC, DCCT calibrations, multiplexing of the analogue bus and libSig library.


// ---------- Includes

#include <stdint.h>

#include <libsig.h>

#include "platforms/fgc3/memmap_dsp.h"

#include "fgc3/c6727/inc/anaCard.h"
#include "fgc3/c6727/inc/calibration.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/spy.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/status.h"



// ---------- Constants

//! DAC resolution (bits)

static uint16_t const DAC_RESOLUTION_BITS = 20;

//! Nominal DAC voltage for positive full scale

static float const    DAC_NOMINAL_V       = 12.5;

//! DAC calibration voltage tolerance

static float const    DAC_MAX_V_ERROR     = 0.1;



// ---------- Internal structures, unions and enumerations

//! Internal global variables for the module signals

struct Signals
{
    bool           bgp_guard_1ms;              //!<   1 ms flag to signal the background processing
    bool           bgp_guard_20ms;             //!<  20 ms flag to signal the background processing
    bool           bgp_guard_200ms;            //!< 200 ms flag to signal the background processing
    bool           bgp_guard_1s;               //!<   1  s flag to signal the background processing
};



// ---------- External variable definitions

struct SIG_struct  sig_struct;
struct SIG_dac     dac_struct[SIGNALS_DAC_NUM];



// ---------- Internal variable definitions

static struct Signals signals;



// Function memory allocation

#pragma CODE_SECTION(signalsInit,       ".ext_code")
#pragma CODE_SECTION(signalsBgp1ms,     ".ext_code")
#pragma CODE_SECTION(signalsBgp20ms,    ".ext_code")
#pragma CODE_SECTION(signalsBgp200ms,   ".ext_code")



// Internal functions declaration

//! Bacground task processed every 1 ms.
//!
//! Updates the measurmenets.

static void signalsBgp1ms(void);


//! Bacground task processed every 20 ms.
//!
//! Updates the MCU with the ADC and DCCT data.

static void signalsBgp20ms(void);

//! Bacground task processed every 200 ms.
//!
//! Update the DSP with the MCU data and the ADC.FIFO property.

static void signalsBgp200ms(void);



// ---------- External function definitions

void signalsInit(void)
{
    CC_STATIC_ASSERT(CC_ARRAY_LEN(dpcom.dsp.adc.raw_200ms)      == FGC_N_ADCS, Index_out_of_bounds);
    CC_STATIC_ASSERT(CC_ARRAY_LEN(dpcom.dsp.adc.raw_1s)         == FGC_N_ADCS, Index_out_of_bounds);
    CC_STATIC_ASSERT(CC_ARRAY_LEN(dpcom.dsp.adc.volts_200ms)    == FGC_N_ADCS, Index_out_of_bounds);
    CC_STATIC_ASSERT(CC_ARRAY_LEN(dpcom.dsp.adc.volts_1s)       == FGC_N_ADCS, Index_out_of_bounds);
    CC_STATIC_ASSERT(CC_ARRAY_LEN(dpcom.dsp.adc.pp_volts_1s)    == FGC_N_ADCS, Index_out_of_bounds);
    CC_STATIC_ASSERT(CC_ARRAY_LEN(dpcom.dsp.adc.cal_meas_200ms) == FGC_N_ADCS, Index_out_of_bounds);
    CC_STATIC_ASSERT(CC_ARRAY_LEN(dpcom.dsp.adc.cal_meas_1s)    == FGC_N_ADCS, Index_out_of_bounds);

    CC_STATIC_ASSERT(CC_ARRAY_LEN(sig_struct.adc.array)         == FGC_N_ADCS, Index_out_of_bounds);


    // Initialize the libsig signals

    memset(&sig_struct, 0, sizeof(sig_struct));
    memset(&dac_struct, 0, sizeof(dac_struct));

    signalsClassInit(&sig_struct);

    sigDacInit(&dac_struct[0], DAC_RESOLUTION_BITS, DAC_NOMINAL_V, DAC_MAX_V_ERROR);
    sigDacInit(&dac_struct[1], DAC_RESOLUTION_BITS, DAC_NOMINAL_V, DAC_MAX_V_ERROR);

    // The ADC calibration limits and gains are specified in EMDS-1767404.

    uint32_t           sig_cal_limit = 0;
    uint32_t           adc_cal_gain  = 0;
    enum Ana_card_type ana_type      = anaCardGetType();

    if (ana_type == ANA_CARD_TYPE_101)
    {
        sig_cal_limit = SIG_CAL_LIMIT_ANA101;
        adc_cal_gain  = 6000000;
    }
    else if (ana_type == ANA_CARD_TYPE_103)
    {
        sig_cal_limit = SIG_CAL_LIMIT_ANA103;
        adc_cal_gain  = 20000000;
    }
    else if (ana_type == ANA_CARD_TYPE_104)
    {
        sig_cal_limit = SIG_CAL_LIMIT_ANA104;
        adc_cal_gain  = 20000000;
    }

    uint8_t idx;

    for (idx = 0; idx < SIGNALS_INTERNAL_ADC_NUM; idx++)
    {
        sigVarValueByIdx(&sig_struct, adc, idx, ADC_CAL_LIMIT_INDEX) = sig_cal_limit;
        sigVarValueByIdx(&sig_struct, adc, idx, ADC_NOMINAL_GAIN)    = adc_cal_gain;
    }

    // Initialize the default values for ADC.INTERNAL.TAU_TEMP and ADC.DCCT.TAU_TEMP

    property_fgc.analog.tau_temp = 80;
    property_fgc.dcct.tau_temp   = 80;

    // Initialize time background time guards.

    signals.bgp_guard_1ms   = false;
    signals.bgp_guard_20ms  = false;
    signals.bgp_guard_200ms = false;
    signals.bgp_guard_1s    = false;
}



void signalsRtp(void)
{
    // Update boundary flags

    signals.bgp_guard_1ms   |= iter.start_of_ms1_f;
    signals.bgp_guard_20ms  |= iter.start_of_ms20_f;
    signals.bgp_guard_200ms |= iter.start_of_ms200_f;
    signals.bgp_guard_1s    |= iter.start_of_s1_f;

    // Report DCCT faults to libSig.

    uint32_t const dcct_flt = dpcom.mcu.meas.dcct_flt;

    sigVarValue(&sig_struct, transducer, dcct_a, TRANSDUCER_FAULT_FLAG) = testBitmap(dcct_flt, 0x01);
    sigVarValue(&sig_struct, transducer, dcct_b, TRANSDUCER_FAULT_FLAG) = testBitmap(dcct_flt, 0x02);

    // Get the internal ADC values

    profileStart(1);

    // Read the average measurement from each ADC

    uint8_t int_adc_idx;

    for (int_adc_idx = 0; int_adc_idx < FGC_N_INT_ADCS; int_adc_idx++)
    {
        sigVarValueByIdx(&sig_struct, adc, int_adc_idx, ADC_RAW) = ANA_FGC3_ADC_SAMPLE_A[int_adc_idx];
    }

    profileStop(1);
    profileCheckWatchPointRtp(10);

    // Acquire class-specific signals

    signalsClassRtpAcq();

    // Process the signals

    sigMgrRT(&sig_struct.mgr, iter.start_of_ms200_f, iter.time_ns);

    // Class-specific post-processing

    signalsClassRtp();

    // Record time in the iteration in the profile

    profileRegister(FGC_DSP_RT_PROFIL_SIGNALS);
}



void signalsBgp(void)
{
    // Every ms write to SPY and update the signal measurements

    if (signals.bgp_guard_1ms == true)
    {
        spySend();

        signalsBgp1ms();

        signals.bgp_guard_1ms = false;
    }

    if (signals.bgp_guard_20ms == true)
    {
        signalsBgp20ms();

        signals.bgp_guard_20ms = false;
    }

    if (signals.bgp_guard_200ms == true)
    {
        signalsBgp200ms();

        // libSig background task

        sigMgr(&sig_struct.mgr, signals.bgp_guard_1s);

        signals.bgp_guard_200ms = false;
        signals.bgp_guard_1s    = false;
    }
}



void signalsUpdateAdcToTrans(void)
{
    // IMPORTANT: the array of transducers in sigStruct.h must coincide with
    // the symlist ADC_SIGNALS used by ADC.INTERNAL.SIGNALS

    uint16_t  int_adc_idx;

    for (int_adc_idx = 0; int_adc_idx < FGC_N_INT_ADCS; int_adc_idx++)
    {
        sigVarValueByIdx(&sig_struct, adc, int_adc_idx, ADC_TRANSDUCER_INDEX) = property_fgc.analog.internal_signals[int_adc_idx];
    }

    signalsClassUpdateAdcToTrans();
}



void signalsUpdateAdcTempThresholds(void)
{
    sigVarValue(&sig_struct, temp_filter, internal, TEMP_FILTER_WARNING_MAX_C) = dpcom.mcu.vref_temp.ref;
}



void signalsSetDac(enum Signals_dac const idx, float const value)
{
    if (calibrationIsActive() == false)
    {
        int32_t raw_value = sigDacRT(&dac_struct[idx], value, NULL);

        // Update REF.DAC

        property_fgc.analog.dac[idx] = raw_value;

        anaCardSetDacRaw(idx, raw_value);
    }
}



void signalsSetDacTest(enum Signals_dac const idx, int32_t const ref_dac)
{
    static int32_t prev_ref_dac_raw[2] = { 0 };

    // REF.DAC can generate a ramp if the absolute value is larger than 1000000. Then
    // it will add R to the DAC value every iteration, where R = abs(REF.DAC) - 1000000.
    // The result is a saw tooth from the max negative up to the max positive DAC value
    // if the value is positive or the same but with the opposite sign if the value is
    // negative.
    // For example if REF.DAC is +1000010, it will increment the DAC by 10 at 10kHz, which
    // will make a saw tooth with period ~10s. It can be helpful when testing the DAC for
    // glitches at zero crossing etc.
    //
    // Overun of the DAC value is handled in anaCardSetDacRaw()

    int32_t ref_dac_raw;

    if (ref_dac >= 1000000)
    {
        ref_dac_raw = prev_ref_dac_raw[idx] + (ref_dac - 1000000);
    }
    else if (ref_dac <= -1000000)
    {
        ref_dac_raw = prev_ref_dac_raw[idx] + (ref_dac + 1000000);
    }
    else
    {
        ref_dac_raw = ref_dac;
    }

    prev_ref_dac_raw[idx] = ref_dac_raw;

    anaCardSetDacRaw(idx, ref_dac_raw);
}



void signalsCalAdcReady(enum Dpcom_cal_level const cal_level)
{
    uint8_t idx;

    for (idx = 0; idx < SIGNALS_INTERNAL_ADC_NUM; idx++)
    {
        if (cal_level != sigAdcCal(&sig_struct.mgr, &sig_struct.adc.array[idx]))
        {
            // @TODO
        }
    }
}



void signalsCalDacReady(enum Signals_dac     const dac_idx,
                        int32_t              const raw_dac,
                        enum Dpcom_cal_level const cal_level)
{
    float avg_value = sigVarValue(&sig_struct, adc, adc_d, ADC_MEAS_AVE1_MEAN);

    if (sigDacCal(&dac_struct[dac_idx], raw_dac, avg_value) == SIG_CAL_FAULT)
    {
        statusSetLatched(FGC_LAT_DAC_FLT);
    }
}



void signalsCalDacTrigger(enum Signals_dac const idx)
{
    sigDacCal(&dac_struct[idx], 0,
              dac_struct[idx].cal_v_dac_meas[SIG_CAL_OFFSET]);

    sigDacCal(&dac_struct[idx], CALIBRATION_DAC_REF,
              dac_struct[idx].cal_v_dac_meas[SIG_CAL_POSITIVE]);

    sigDacCal(&dac_struct[idx], -CALIBRATION_DAC_REF,
              dac_struct[idx].cal_v_dac_meas[SIG_CAL_NEGATIVE]);
}


void signalsCalAdcStatus(enum Signals_adc const idx, bool const status)
{
    sigVarValueByIdx(&sig_struct, adc, idx, ADC_CALIBRATING_FLAG) = status;
}



void signalsCalDcctReady(enum Signals_dcct    const dcct,
                         enum Dpcom_cal_level const cal_level)
{
    if (cal_level != sigTransducerCal(&sig_struct.mgr,
                                      &sig_struct.transducer.array[dcct]))
    {
        // @TODO
    }
}



// ---------- Internal function definitions

static void signalsBgp1ms(void)
{
    signalsClassBgp1ms();

    // All ADCs

    uint16_t adc_idx;

    for (adc_idx = 0; adc_idx < FGC_N_ADCS; adc_idx++)
    {
        dpcom.dsp.adc.raw_200ms  [adc_idx] = sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_AVE1_RAW_MEAS);
        dpcom.dsp.adc.raw_1s     [adc_idx] = sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_AVE2_RAW_MEAS);
        dpcom.dsp.adc.volts_200ms[adc_idx] = sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_MEAS_AVE1_MEAN);
        dpcom.dsp.adc.volts_1s   [adc_idx] = sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_MEAS_AVE2_MEAN);
        dpcom.dsp.adc.pp_volts_1s[adc_idx] = sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_MEAS_AVE2_PP);

        uint16_t const trans_idx = sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_TRANSDUCER_INDEX);

        dpcom.dsp.adc.cal_meas_200ms[adc_idx] = trans_idx != SIG_NOT_IN_USE
                                              ? sigVarValueByIdx(&sig_struct, transducer, trans_idx, TRANSDUCER_MEAS_AVE1_MEAN)
                                              : 0.0;

        dpcom.dsp.adc.cal_meas_1s[adc_idx]    = trans_idx != SIG_NOT_IN_USE
                                              ? sigVarValueByIdx(&sig_struct, transducer, trans_idx, TRANSDUCER_MEAS_AVE2_MEAN)
                                              : 0.0;
    }

    // DCCT A & B mean values

    dpcom.dsp.adc.amps_200ms[0] = sigVarValue(&sig_struct, transducer, dcct_a, TRANSDUCER_MEAS_AVE1_MEAN);
    dpcom.dsp.adc.amps_200ms[1] = sigVarValue(&sig_struct, transducer, dcct_b, TRANSDUCER_MEAS_AVE1_MEAN);
}



static void signalsBgp20ms(void)
{
    uint32_t  faults;
    uint32_t  warnings;
    uint32_t  state;
    uint8_t   adc_idx;

    signalsClassBgp20ms();

    // ADC status for internal ADCs

    for (adc_idx = 0; adc_idx < FGC_N_INT_ADCS; adc_idx++)
    {
        faults   = sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_FAULTS);
        warnings = sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_WARNINGS);

        // Start with V_MEAS_OK if the ADC is linked to a transducer

        state = sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_TRANSDUCER_INDEX) != SIG_NOT_IN_USE
              ? FGC_ADC_STATUS_V_MEAS_OK
              : 0;

        // CAL_FAILED

        if (testBitmap(warnings, SIG_ADC_CAL_WARN_BIT_MASK))
        {
            setBitmap(state, FGC_ADC_STATUS_CAL_FAILED);
        }

        if (testBitmap(faults, SIG_ADC_CAL_FLT_BIT_MASK))
        {
            setBitmap(state, FGC_ADC_STATUS_CAL_FAILED);
            clrBitmap(state, FGC_ADC_STATUS_V_MEAS_OK);
        }

        // SIGNAL_STUCK

        if (testBitmap(faults, SIG_ADC_STUCK_FLT_BIT_MASK))
        {
            setBitmap(state, FGC_ADC_STATUS_SIGNAL_STUCK);
            clrBitmap(state, FGC_ADC_STATUS_V_MEAS_OK);
        }

        // V_MEAS_OK

        if (testBitmap(faults, SIG_ADC_FLT_BIT_MASK))
        {
            clrBitmap(state, FGC_ADC_STATUS_V_MEAS_OK);
        }

        // CAL_ACTIVE

        if (sigVarValueByIdx(&sig_struct, adc, adc_idx, ADC_CALIBRATING_FLAG) == true)
        {
            setBitmap(state, FGC_ADC_STATUS_CAL_ACTIVE);
            clrBitmap(state, FGC_ADC_STATUS_CAL_FAILED);
        }

        // IN_USE

        if (property_fgc.analog.internal_signals[adc_idx] != FGC_ADC_SIGNALS_NONE)
        {
            setBitmap(state, FGC_ADC_STATUS_IN_USE);
        }
       
        dpcom.dsp.meas.adc_state[adc_idx] = (uint8_t)state;
    }
}



static void signalsBgp200ms(void)
{
    // Update measurements

    if (dpcom.mcu.temp.adc     != 0) {  sigTempMeas(&sig_struct.temp_filter.named.internal, dpcom.mcu.temp.adc);     }
    if (dpcom.mcu.temp.dcct[0] != 0) {  sigTempMeas(&sig_struct.temp_filter.named.dcct_a,   dpcom.mcu.temp.dcct[0]); }
    if (dpcom.mcu.temp.dcct[1] != 0) {  sigTempMeas(&sig_struct.temp_filter.named.dcct_b,   dpcom.mcu.temp.dcct[1]); }

    signalsClassBgp200ms();
}


// EOF
