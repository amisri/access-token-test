//! @file  isr.c
//! @brief FGC3 DSP interrupt service routines


// ---------- Includes

#include "fgc3/c6727/inc/dsp.h"
#include "fgc3/c6727/inc/control.h"
#include "fgc3/c6727/inc/isr.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/panic.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/signals.h"



// ---------- Internal function definitions

static void isrDummy(void)
{
    ; // Do nothing
}



// ---------- Internal structures, unions and enumerations

typedef void (*isrFunc)(void);



// ---------- Internal variable definitions

static isrFunc signalsRtpFunc = isrDummy;
static isrFunc controlRtpFunc = isrDummy;



// ---------- External function definitions

void isrRtp(void)
{
    // Acknowledge interrupt

    ACKNOWLEDGE_NMI();

    DISABLE_TICKS();

    // Set the DSP Test Point 0

    SET_TP0();

    // Update the iteration times

    iterStart();

    profileCheckWatchPointRtp(1);

    // Process the real-time signal handling

    signalsRtpFunc();

    profileCheckWatchPointRtp(2);

    // Process the real-time converter control

    controlRtpFunc();

    profileCheckWatchPointRtp(3);

    // Process the inter-FGC signals

    signalsClassInterFgc();

    profileCheckWatchPointRtp(4);

    // Update the iteration processing time

    iterEnd();

    // Clear DSP Test Point 0

    CLR_TP0();

    ENABLE_TICKS();
}



void isrTrap(void)
{
    panic(DSP_PANIC_ISR_TRAP);
}



void isrBgp(void)
{
    // If the MCU has finished its initialization and configuring
    // the properties, call the real control functions

    DISABLE_TICKS();

    signalsRtpFunc = signalsRtp;
    controlRtpFunc = controlRtp;

    ENABLE_TICKS();
}


// EOF
