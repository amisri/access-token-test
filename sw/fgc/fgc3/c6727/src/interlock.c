//! @file  interlock.c
//! @brief Interface to cclibs libintlk


// ---------- Includes

#include <stdint.h>

#include <defconst.h>

#include "platforms/fgc3/memmap_dsp.h"

#include "fgc3/c6727/inc/interlock.h"
#include "fgc3/c6727/inc/cycle.h"
#include "fgc3/c6727/inc/log.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/pc_state.h"



// Function memory allocation

#pragma CODE_SECTION(interlockInit      , ".ext_code")
#pragma CODE_SECTION(interlockIgnoreLogs, ".ext_code")



// ---------- External variable definitions

struct Interlock  interlock;
struct INTLK_mgr  intlk_mgr;



// ---------- External function definitions

void interlockInit(void)
{
    interlock.latch = false;

    interlockClassInit(&interlock, &intlk_mgr);

    // Send one transmission with no BIS permit

    BIS_CONTROL_P = 0;
}



void interlockRtp(void)
{
    // Testing the BIS is only valid if the converter is OFF or FAULT_OFF

    if (   pcStateTest(FGC_STATE_EQ_OFF_BIT_MASK) == true
        && interlockClassGetChannelTest() != FGC_INTLK_TEST_DISABLED)
    {
        BIS_CONTROL_P = interlockClassGetChannelTest();
    }

    // During the first iteration of a cycle libintlk must process and cache
    // the parameters. This is notified in the second argument of intlkRT()
    // Also the cycle selector must be updated

    else if (cycleIsStarted() == true)
    {
        interlock.sub_sel = intlkParValue(&intlk_mgr, USE_CYC_SEL) == CC_ENABLED ? 0 : cycleGetSubSelAcq();
        interlock.cyc_sel = intlkParValue(&intlk_mgr, USE_CYC_SEL) == CC_ENABLED ? cycleGetCycSelAcq() : 0;

        intlkRT(&intlk_mgr, true, false);
    }

    // Compute the BIS output based on the BIS properties

    else
    {
        uint8_t bis_output = intlkRT(&intlk_mgr, false, interlock.latch);
        uint8_t bis_mpx    = interlockClassGetOutput(interlock.sub_sel, interlock.cyc_sel);

        if (interlock.latch == true)
        {
            // Publish INTERLOCK.(PPM.){V,I}.MEAS and INTERLOCK.(PPM.)RESULT

            dpcls.dsp.interlock.cyc_sel = interlock.cyc_sel;
            dpcls.dsp.interlock.ready   = true;

            interlock.latch = false;
        }

        // Copy the resulting BIS ouptut to the channels specified in BIS.OUTPUT, unless
        // the STATE.OP is not NORMAL, in which case the output is forzed to 0

        BIS_CONTROL_P = bis_output == 0 || dpcom.mcu.state_op != FGC_OP_NORMAL ? 0x0000 : bis_mpx;
    }
}



void interlockLatchStatus(void)
{
    interlock.latch = true;
}



void interlockIgnoreLogs(void)
{
    // @TODO at the moment the mask enabled all users. In the future only set
    //       the cycles are active in the super-cycle

    static uint32_t const  active_cyc_sel_mask = 0xFFFFFFFF;

    (intlkCheckChanIsActive(&intlk_mgr, active_cyc_sel_mask, 0) == false)
    ? logDisableLog(DPCOM_LOG_IGNORE_BIS_CH1_BIT_MASK)
    : logEnableLog(DPCOM_LOG_IGNORE_BIS_CH1_BIT_MASK);

    (intlkCheckChanIsActive(&intlk_mgr, active_cyc_sel_mask, 1) == false)
    ? logDisableLog(DPCOM_LOG_IGNORE_BIS_CH2_BIT_MASK)
    : logEnableLog(DPCOM_LOG_IGNORE_BIS_CH2_BIT_MASK);

    (intlkCheckChanIsActive(&intlk_mgr, active_cyc_sel_mask, 2) == false)
    ? logDisableLog(DPCOM_LOG_IGNORE_BIS_CH3_BIT_MASK)
    : logEnableLog(DPCOM_LOG_IGNORE_BIS_CH3_BIT_MASK);

    (intlkCheckChanIsActive(&intlk_mgr, active_cyc_sel_mask, 3) == false)
    ? logDisableLog(DPCOM_LOG_IGNORE_BIS_CH4_BIT_MASK)
    : logEnableLog(DPCOM_LOG_IGNORE_BIS_CH4_BIT_MASK);
}


// EOF
