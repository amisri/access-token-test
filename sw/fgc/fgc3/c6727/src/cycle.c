//! @file  cycle.c
//! @brief Functionality related to cycles


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include <defconst.h>

#include "fgc3/c6727/inc/cycle.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/pc_state.h"
#include "fgc3/inc/status.h"



// ---------- Constants

static uint32_t const CYCLE_PERIOD_US = 100;


// ---------- Internal structures, unions and enumerations

//! Internal local variables for the module cycle

struct Cycle
{
    uint32_t                time_us;             //!< Cycle time in microseconds
    uint32_t                time_ms;             //!< Cycle time in milliseconds
    struct Dpcom_cycle_info info;                //!< Information of the current cycle
    uint32_t                rtp_start_mask;      //!< Mask to shift-left the start of cycle flag
};



// ---------- Internal variable definitions

static struct Cycle cycle;



// Function memory allocation

#pragma CODE_SECTION(cycleBgp, ".ext_code")



// ---------- External function definitions

void cycleInit(void)
{
    memset(&cycle, 0, sizeof(cycle));
}



void cycleBgp(void)
{
    ; // Do nothing
}



void cycleRtp(void)
{
    // Update the cycle time

    cycle.time_us += CYCLE_PERIOD_US;
    cycle.time_ms  = cycle.time_us / 1000;

    // Check if a new cycle has started

    struct CC_us_time * cycle_start = (struct CC_us_time *)&dpcom.mcu.cycle.info.start_time;
    struct CC_us_time   time_now_us = iter.time_us;

    if (   timeUsGreaterThanEqual(&time_now_us, cycle_start) == true
        && dpcom.mcu.cycle.start_latched                     == true)
    {
        dpcom.mcu.cycle.start_latched = false;

        // Update OASIS signals meta data: LOG.OASIS.CYCLE_TAG and timestamp for the cycle just ended

        dpcls.oasis.timestamp                         = cycle.info.start_time;
        dpcls.oasis.cycle_tag[cycle.info.cyc_sel_acq] = dpcls.oasis.cycle_tag_next;

        // Update current cycle information

        cycle.info           = dpcom.mcu.cycle.info;
        cycle.info.func_type = cycleClassGetFuncType(cycle.info.sub_sel_ref, cycle.info.cyc_sel_ref);

        // Update previous and current cycle information used by the MCU for the
        // real-time display and for the publication of properties.

        dpcom.dsp.cycle.previous = dpcom.dsp.cycle.current;
        dpcom.dsp.cycle.current  = cycle.info;

        // Zero the cycle time

        cycle.time_us = 0;
        cycle.time_ms = 0;

        // Notify the background and real-time tasks that a cycle has started

        cycle.rtp_start_mask = 1;
    }
    else
    {
        // Shift start mask so we have the history of the start flag for up to 31 iterations

        cycle.rtp_start_mask <<= 1;
    }

    // Delay communication with the MCU until an iteration following the start of the cycle to
    // reduce the peak processing on the start of cycle

    if (cycleWasStarted() == true)
    {
        // Transfer to the MCU the information for LOG.TIMING for the cycle just ended

        volatile struct Dpcom_cycle_info * info = &dpcom.dsp.cycle.previous;

        dpcom.dsp.log.timing.info           = *info;
        dpcom.dsp.log.timing.status_bitmask = cycleClassGetStatus(info->sub_sel_acq, info->cyc_sel_acq);

        CC_MEMORY_BARRIER;

        // Trigger the MCU to publish properties

        dpcom.dsp.cycle.pub_previous_cycle = true;
    }
}



uint32_t cycleGetCycSelRef(void)
{
    return (cycle.info.cyc_sel_ref);
}



uint32_t cycleGetSubSelRef(void)
{
    return (cycle.info.sub_sel_ref);
}



uint32_t cycleGetCycSelAcq(void)
{
    return (cycle.info.cyc_sel_acq);
}



uint32_t cycleGetSubSelAcq(void)
{
    return (cycle.info.sub_sel_acq);
}



uint32_t cycleGetTimeUs(void)
{
    return (cycle.time_us);
}



uint32_t cycleGetTimeMs(void)
{
    return (cycle.time_ms);
}



bool cycleIsStarted(void)
{
    // True if cycle started on this iteration

    return ((cycle.rtp_start_mask & 0x01) != 0);
}



bool cycleWasStarted(void)
{
    // True if cycle started five iterations ago

    return ((cycle.rtp_start_mask & 0x20) != 0);
}


// EOF
