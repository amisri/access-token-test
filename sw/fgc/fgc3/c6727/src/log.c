//! @file  log_local.c
//! @brief Functionality related to logging


// ---------- Includes

#include <liblog.h>

#include "fgc3/c6727/inc/log.h"
#include "fgc3/c6727/inc/log_structs.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/dpcls.h"
#include "fgc3/inc/dpcom.h"



// ---------- Internal structures, unions and enumerations

//! Internal local variables for the module log_local.

struct Log
{
    uint8_t   blk_idx[2];
};



// ---------- External variable definitions
struct LOG_read_control   log_read_control[2];



// ---------- Internal variable definitions

// The variable log is already defined in math.h

static struct Log log_local;



// ---------- Internal function definitions

static void logProcessRequest(void)
{
    if (testBitmap(dpcom.mcu.log.signals_request[0], DPCOM_LOG_SIGNALS_REQUEST_FCM_BIT_MASK))
    {
        uint8_t idx = log_local.blk_idx[0];

        if (dpcom.dsp.log.comms[0].blk_len[idx] == 0)
        {
            uint32_t num_els = logOutput(&log_read_control[0],
                                         log_buffers.buffers,
                                         (uint32_t*)&dpcom.fcm_prop.blk.log[idx],
                                         DPCOM_LOG_BLK_LEN);

            dpcom.dsp.log.comms[0].blk_len[idx] = num_els;

            log_local.blk_idx[0] = idx ^ 0x01;

            if (logReadOutputFinished(&log_read_control[0]) == true || num_els == 0)
            {
                clrBitmap(dpcom.mcu.log.signals_request[0], DPCOM_LOG_SIGNALS_REQUEST_FCM_BIT_MASK);
            }
        }
    }

    if (testBitmap(dpcom.mcu.log.signals_request[1], DPCOM_LOG_SIGNALS_REQUEST_PCM_BIT_MASK))
    {
        uint8_t idx = log_local.blk_idx[1];

        if (dpcom.dsp.log.comms[1].blk_len[idx] == 0)
        {
            uint32_t num_els = logOutput(&log_read_control[1],
                                         log_buffers.buffers,
                                         (uint32_t*)&dpcom.pcm_prop.blk.log[idx],
                                         DPCOM_LOG_BLK_LEN);

            dpcom.dsp.log.comms[1].blk_len[idx] = num_els;

            log_local.blk_idx[1] = idx ^ 0x01;

            if (logReadOutputFinished(&log_read_control[1]) == true || num_els == 0)
            {
                clrBitmap(dpcom.mcu.log.signals_request[1], DPCOM_LOG_SIGNALS_REQUEST_PCM_BIT_MASK);
            }
        }
    }
}



// ---------- External function definitions


void logInit(void)
{
    log_local.blk_idx[1] = log_local.blk_idx[0] = 0;

    logClassInit();
}



void logRtp()
{
    logClassRtp();
}


void logBgp()
{
    logClassBgp();

    logProcessRequest();
}



void logParsReadRequest(void)
{
    // Process the read request to generate the read control and header

    uint8_t                    const   cmd_idx = dpcom.log.cmd_idx;
    struct LOG_read_control          * ctrl    = &log_read_control[cmd_idx];
    struct Dpcom_dsp_log_comms       * comms   = (struct Dpcom_dsp_log_comms *)&dpcom.dsp.log.comms[cmd_idx];

    logReadRequest(&log_read, ctrl);

    comms->read_req_status   = ctrl->status;
    comms->first_sample_time = cctimeNsToUsRT(ctrl->first_sample_time);
    comms->header_size       = ctrl->header_size;
    comms->data_size         = ctrl->data_size;
    comms->blk_len[0]        = 0;
    comms->blk_len[1]        = 0;

    log_local.blk_idx[cmd_idx] = 0;

    dpcom.mcu.log.signals_request[cmd_idx] = DPCOM_LOG_SIGNALS_REQUEST_NONE_BIT_MASK;
    dpcom.logl.read_request = false;
}


// EOF
