//! @file  anaCard.c
//! @brief FGC3 analogue card interface (ADCs, DAC, crossbar)


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include "fgc3/c6727/inc/anaCard.h"
#include "fgc3/c6727/inc/panic.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/memmap.h"
#include "fgc3/inc/sleep.h"



// ---------- Constants

//! Default mpx value

static uint32_t const ANA_CARD_MPX_SEL_DEFAULT = (ANA_CARD_INPUT_CAL_ZERO - ANA_CARD_INPUT_MPX_RANGE_START);



// ---------- Internal structures, unions and enumerations

//! Internal global variables for the module ana_card

struct Ana_card
{
    enum Ana_card_type       type;                     //!< Analogue interface type (ANA-101, ANA-102...)
    bool                     in_use_f;                 //!< Flag indicating if a MPX context is saved
    uint8_t                  swin;                     //!< Analogue input switch control
    uint8_t                  swbus;                    //!< Analogue bus switch control
    uint8_t                  mpx;                      //!< Analogue multiplexer control
    bool                     mpx_enable;               //!< Analogue multiplexer enable
};



// ---------- Internal variable definitions

static struct Ana_card ana_card;



// Function memory allocation

#pragma CODE_SECTION(anaCardInit,              ".ext_code")
#pragma CODE_SECTION(anaCardProcessMpxRequest, ".ext_code")
#pragma CODE_SECTION(anaCardConfigCrossbar,    ".ext_code")
#pragma CODE_SECTION(anaCardSaveState,         ".ext_code")
#pragma CODE_SECTION(anaCardRestoreState,      ".ext_code")
#pragma CODE_SECTION(anaCardSetMpx,            ".ext_code")
#pragma CODE_SECTION(anaCardUpdateProperties,  ".ext_code")



// ---------- Internal function declarations

//! This function is used to set the routing of the input analogue signals (input
//! channels, multiplexer) to the 4 ADCs.
//!
//! @param[in]   swin            Mask indicating which of the four input channels are connected.
//! @param[in]   swbus           Mask indicating which of the four ADCs are connected to the analogue bus.
//! @param[in]   mpx_enable      Flag indicating whether the multiplexer is connected to the analogue bus.
//! @param[in]   mpx             Multiplexer selector.
//!
//! @retval      true            The connection was successful.
//! @retval      false           The connection was not successful.

static bool anaCardSetMpx(uint8_t const swin,
                          uint8_t const swbus,
                          bool    const mpx_enable,
                          uint8_t const  mpx);



// ---------- External function definitions

void anaCardInit(void)
{
    ana_card.in_use_f = false;

    // Determine the analogue card type

    ana_card.type = (enum Ana_card_type)DSP_MID_ANATYPE_P;

    if (   ana_card.type != ANA_CARD_TYPE_101
        && ana_card.type != ANA_CARD_TYPE_103
        && ana_card.type != ANA_CARD_TYPE_104)
    {
        ana_card.type = ANA_CARD_NO_CARD;
        panic(DSP_PANIC_HW_ANATYPE);
    }

    // Configure the analogue card to its nominal state:
    //   CH1  ------->  ADC1
    //   CH2  ------->  ADC2
    //   CH3  ------->  ADC3
    //   CH4  ------->  ADC4

    anaCardConfigCrossbar(ANA_CARD_INPUT_NONE, SIGNALS_INTERNAL_ADC_ZERO_BIT_MASK);
}



enum Ana_card_type anaCardGetType(void)
{
    return (ana_card.type);
}



void anaCardSetDacRaw(enum Signals_dac const idx,
                      int32_t          const raw_value)
{
    // The DAC is 20-bit unipolar (0 to 2.5v) but later the 0v is shifted
    // to themiddle of the range (there is a bit manipulation inside the
    // FPGA).
    //
    //   7FFFF   +12.5 V (max positive)
    //   66665   +10.0 V
    //   00000    +0.0 V
    //   FFFFF    -0.0 V
    //   99999   -10.0 V
    //   80000   -12.5 V (max negative)
    //
    // libsig takes care of clipping the reference if necessary so raw_value
    // should be within the limits.


    // Output value to the DAC

    ANA_FGC3_DAC_A[idx] = (raw_value << 12) & 0xFFFFF000;
}



void anaCardProcessMpxRequest(void)
{
    enum Ana_card_crossbar_input  bus_input;
    uint8_t                       adc_mask;

    // If already in use (e.g. while calibrating), return immediately.

    if (ana_card.in_use_f == true)
    {
        return;
    }

    // Copy request

    bus_input = (enum Ana_card_crossbar_input)dpcom.mcu.ana.anabus_input;
    adc_mask  = (uint8_t)dpcom.mcu.ana.adc_mask;

    // Process request

    anaCardConfigCrossbar(bus_input, adc_mask);

    // Acknowledge the MCU request

    dpcom.mcu.ana.req_f = false;
}



void anaCardConfigCrossbar(enum Ana_card_crossbar_input const input,
                           uint8_t                      const adcs_mask)
{
    // The arguments input and adcs_mask are sufficient to express the full set of
    // the multiplexing solutions offered by the analogue card. Examples are provided
    // below in this comment section to help you choose the parameters for a variety
    // of multiplexing options.
    //
    // The ADCs not listed in adcs_mask are by default connected to their respective
    // channel  inputs, not going through the analogue bus. If the crossbar input is
    // one of the 4 input channels, that channel is also connected to its respective
    // ADC via a direct link (by design of the analogue card).
    //
    // Example 1: perhaps the most simple usage is as follows:
    //
    //   calibrationConfigAnalogueBus(ANA_CARD_INPUT_NONE, ANA_MASK_NO_ADC);
    //
    // This configures the analogue card to its nominal state. Every ADC is connected
    // to its default channel input. The analogue bus is not connected to any input,
    // as meant by the first argument "ANA_CARD_INPUT_NONE" (technically the analogue
    // bus is physically connected to the COM connector on the front panel, but the
    // signal is floating.)
    //
    //   CH1  ------->  ADC1
    //   CH2  ------->  ADC2
    //   CH3  ------->  ADC3
    //   CH4  ------->  ADC4
    //
    // Example 2: look at a channel with 2 ADCS
    //
    //   calibrationConfigAnalogueBus(ANA_CARD_INPUT_CH_B, (ANA_CARD_MASK_ADC2|ANA_CARD_MASK_ADC4));
    //
    // In the input parameters, ANA_CARD_MASK_ADC2 is not required in the mask but makes the
    // code more readable. As mentioned above, after this function call, ADC1 and ADC3
    // (which are not listed in the 2nd argument of the function) are mapped to their
    // default channel.
    //
    // This sets the following connections.
    //
    //   CH1  ------->  ADC1
    //   CH2  ----+-->  ADC2        (CH2 linked to ADC2 via direct link)
    //            |
    //   CH3  ------->  ADC3
    //            |
    //   CH4  -x  \-->  ADC4        (CH4 disconnected, CH2 linked to ADC4 via crossbar)
    //
    //            x    MPX_ENA = 0  (the multiplexer is disconnected)
    //            |
    //           0V    MPX_SEL = 5  (connected to GNDSENSE (CALZERO) by default)
    //
    // Example 3: the following call will typically be used during calibration
    //
    //   calibrationConfigAnalogueBus(ANA_CARD_INPUT_CAL_POSREF, ANA_CARD_MASK_ADC2);
    //
    //   CH1 ------->  ADC1
    //   CH2 -x  /-->  ADC2         (CH2 disconnected, ADC2 connected to +10V)
    //           |
    //   CH3 ------->  ADC3
    //   CH4 ------->  ADC4
    //           |
    //           +    MPX_ENA = 1   (the multiplexer is connected to the crossbar)
    //           |
    //         +10V   MPX_SEL = 6   (CAL_POSREF)
    //
    // Example 4: The following calls allow spying the channel 1 on the COM port.
    //            Notice that those two function calls are equivalent and will produce
    //            the same hardware multiplexing.
    //
    //   calibrationConfigAnalogueBus(ANA_CARD_INPUT_CH_A, ANA_MASK_ADC1);
    //                              -OR-
    //   calibrationConfigAnalogueBus(ANA_CARD_INPUT_CH_A, ANA_MASK_NO_ADC);
    //
    //                  COM  (Front panel COM port used as an OUTPUT, to spy CH1)
    //                   |
    //   CH1 ----+-->  ADC1
    //   CH2 ------->  ADC2
    //   CH3 ------->  ADC3
    //   CH4 ------->  ADC4)
    //
    // Example 5: The following call will connect ADC1 to the COM port and disconnect
    //            channel 1. To the contrary of example 3 above, in this case the COM
    //            port is used as an INPUT.
    //
    //   calibrationConfigAnalogueBus(ANA_CARD_INPUT_COM, ANA_MASK_ADC1);
    //
    //                  COM  (Front panel COM port used as an INPUT, measured on ADC1)
    //                   |
    //   CH1  -x  \-->  ADC1
    //   CH2  ------->  ADC2
    //   CH3  ------->  ADC3
    //   CH4  ------->  ADC4


    uint8_t swin;
    uint8_t swbus;
    uint8_t input_mask;

    // Analogue bus is not used

    if (input == ANA_CARD_INPUT_NONE)
    {
        anaCardSetMpx(0x0F, 0x00, false, ANA_CARD_MPX_SEL_DEFAULT);
    }
    else if (input == ANA_CARD_INPUT_COM)
    {
        // COM port used as an input

        swbus =  adcs_mask & ANA_FGC3_CTRL_SWBUS_MASK8;
        swin  = ~adcs_mask & ANA_FGC3_CTRL_SWIN_MASK8;

        if (anaCardSetMpx(swin, swbus, false, ANA_CARD_MPX_SEL_DEFAULT) == false)
        {
            panic(DSP_PANIC_HW_ANALOG_CARD);
        }
    }
    else if (input < ANA_CARD_INPUT_MPX_RANGE_START)
    {
        // Input is CH1, CH2, CH3 or CH4

        input_mask = (1 << (input - ANA_CARD_INPUT_CH_1)) & ANA_FGC3_CTRL_SWIN_MASK8;
        swbus      = ( adcs_mask | input_mask)       & ANA_FGC3_CTRL_SWBUS_MASK8;
        swin       = (~adcs_mask | input_mask)       & ANA_FGC3_CTRL_SWIN_MASK8;

        if (anaCardSetMpx(swin, swbus, false, ANA_CARD_MPX_SEL_DEFAULT) == false)
        {
            panic(DSP_PANIC_HW_ANALOG_CARD);
        }
    }
    else if (input < ANA_CARD_INPUT_MAX_VALUE)
    {
        // One of the multiplexer inputs

        swbus =  adcs_mask & ANA_FGC3_CTRL_SWBUS_MASK8;
        swin  = ~adcs_mask & ANA_FGC3_CTRL_SWIN_MASK8;

        if (!anaCardSetMpx(swin, swbus, true, (input - ANA_CARD_INPUT_MPX_RANGE_START)))
        {
            panic(DSP_PANIC_HW_ANALOG_CARD);
        }
    }
    else
    {
        panic(DSP_PANIC_BAD_PARAMETER);
    }

    // Sleep to allow the DAC to be set immediately after (Jira issue EPCCCS-3573)

    sleepUs(50);
}



void anaCardSaveState(void)
{
    if (ana_card.in_use_f == false)
    {
        ana_card.in_use_f    = true;
        ana_card.swin        = ANA_FGC3_CTRL_SWIN_P;
        ana_card.swbus       = ANA_FGC3_CTRL_SWBUS_P;
        ana_card.mpx         = ANA_FGC3_CTRL_MPX_P;
        ana_card.mpx_enable  = testBitmap(ana_card.mpx, ANA_FGC3_CTRL_MPX_ENA_MASK8);
        ana_card.mpx        &= ANA_FGC3_CTRL_MPX_SEL_MASK8;
    }
}



void anaCardRestoreState(void)
{
    if (ana_card.in_use_f)
    {
        if (!anaCardSetMpx(ana_card.swin,       ana_card.swbus,
                           ana_card.mpx_enable, ana_card.mpx))
        {
            // Not being able to restore the previous settings indicates a serious HW problem.

            panic(DSP_PANIC_HW_ANALOG_CARD);
        }

        ana_card.in_use_f = false;
    }
}



void anaCardUpdateProperties(void)
{
    enum Ana_card_crossbar_input bus_input;
    enum Ana_card_crossbar_input adc_mpx;
    uint8_t  idx;
    uint8_t  swin;
    uint8_t  swbus;
    uint8_t  mpx;
    uint8_t  mask;

    // Read the registers

    swin  = ANA_FGC3_CTRL_SWIN_P;
    swbus = ANA_FGC3_CTRL_SWBUS_P;
    mpx   = ANA_FGC3_CTRL_MPX_P;

    // Update property values

    property_fgc.analog.bus.swin  = swin;
    property_fgc.analog.bus.swbus = swbus;

    // Identify the analogue bus input. Default value (analogue bus is not connected)

    bus_input = ANA_CARD_INPUT_NONE;

    if (mpx & ANA_FGC3_CTRL_MPX_ENA_MASK8)
    {
        // Multiplexer enabled

        bus_input = (enum Ana_card_crossbar_input)((mpx & ANA_FGC3_CTRL_MPX_SEL_MASK8) + ANA_CARD_INPUT_MPX_RANGE_START);
    }
    else
    {
        for (idx = 0, mask = 1; idx < FGC_N_ADCS; idx++, mask <<= 1)
        {
            if (swin & swbus & mask)
            {
                switch (idx)
                {
                    case 0:
                        bus_input = ANA_CARD_INPUT_CH_1;
                        break;

                    case 1:
                        bus_input = ANA_CARD_INPUT_CH_2;
                        break;

                    case 2:
                        bus_input = ANA_CARD_INPUT_CH_3;
                        break;

                    case 3:
                        bus_input = ANA_CARD_INPUT_CH_4;
                        break;
                }

                break;
            }
        }
    }

    // If still the default value but SWBUS != 0

    if ((bus_input == ANA_CARD_INPUT_NONE) && swbus)
    {
        // The analogue bus is used as an input (the COM port on the front panel)

        bus_input = ANA_CARD_INPUT_COM;
    }

    // Update property ADC.ANALOG_BUS.SIGNAL

    property_fgc.analog.bus.signal = (uint32_t)bus_input;

    // For each ADC identify its input signal

    for (idx = 0, mask = 1; idx < FGC_N_ADCS; idx++, mask <<= 1)
    {
        if (swin & mask)
        {
            switch (idx)
            {
                case 0:   adc_mpx = ANA_CARD_INPUT_CH_1;   break;
                case 1:   adc_mpx = ANA_CARD_INPUT_CH_2;   break;
                case 2:   adc_mpx = ANA_CARD_INPUT_CH_3;   break;
                case 3:   adc_mpx = ANA_CARD_INPUT_CH_4;   break;
                default:  adc_mpx = ANA_CARD_INPUT_NONE;   break;
            }
        }
        else if (swbus & mask)
        {
            adc_mpx = bus_input;
        }
        else
        {
            adc_mpx = ANA_CARD_INPUT_NONE;
        }

        // Update property ADC.INTERNAL.MPX

        dpcom.dsp.ana.mpx[idx] = (int32_t)adc_mpx;
    }
}



// ---------- Internal function definitions

static bool anaCardSetMpx(uint8_t const swin,       uint8_t const swbus,
                          bool    const mpx_enable, uint8_t const  mpx)
{
    //  This function has the essential role of ensuring no two inputs are connected
    //  to the analogue bus (or crossbar) at the same time. At any given time, the
    //  analogue bus takes zero or one input signal and direct it to a subset of ADCs.
    //
    //  If the parameters passed to this function break the above rule, then the
    //  configuration is not modified and the function returns false to indicate that
    //  the configuration could not be set.
    //
    //  On the contrary, if the parameters correspond to a valid configuration the
    //  function will drive the HW to set that configuration. That requires several
    //  steps of changing the HW registers one by one in a specific order to ensure
    //  that the transition states of the analogue bus all comply with the above rule.
    //
    //  The function finally returns true when the job is done.
    //
    //  This function will not disturb channels that are directly connected to their
    //  respective ADCs (not going through the analogue bus), provided they are so
    //  in the current configuration and in the requested one. This is to allow for
    //  example to change the configuration of the fourth ADC (AUX channel) without
    //  disturbing the operation on CH_A and CH_B.
    //
    //  Outside of this function there should be no writing to registers ANA_SWIN,
    //  ANA_SWBUS and ANA_MPX

    bool retval;

    if (mpx_enable == false)
    {
        // No more than one input channel can be connected to the analogue bus.
        // To be connected to the analogue bus, an input channel must have its SWIN bit
        // and its SWBUS bit set. That leads to the test condition (SWIN & SWBUS) equals
        // to 0 or a power of two.

        if ((swin & swbus) != 0 &&
            (swin & swbus) != 1 &&
            (swin & swbus) != 2 &&
            (swin & swbus) != 4 &&
            (swin & swbus) != 8)
        {
            retval = false;
        }
        else
        {
            // The order of the following register writes is important

            ANA_FGC3_CTRL_MPX_P   = (mpx & ANA_FGC3_CTRL_MPX_SEL_MASK8);
            ANA_FGC3_CTRL_SWBUS_P = 0;
            ANA_FGC3_CTRL_SWIN_P  = swin;
            ANA_FGC3_CTRL_SWBUS_P = swbus;

            anaCardUpdateProperties();

            retval = true;
        }
    }
    else
    {
        // None of the input channels (CH1 to CH4) must be connected at the same time
        // to the analogue bus. Hence, SWIN & SWBUS must be zero.

        if ((swin & swbus) != 0)
        {
            retval = false;
        }
        else
        {
            // The order of the following register writes is important

            ANA_FGC3_CTRL_SWBUS_P = 0;
            ANA_FGC3_CTRL_SWIN_P  = swin;
            ANA_FGC3_CTRL_MPX_P   = ANA_FGC3_CTRL_MPX_ENA_MASK8 | (mpx & ANA_FGC3_CTRL_MPX_SEL_MASK8);
            ANA_FGC3_CTRL_SWBUS_P = swbus;

            anaCardUpdateProperties();

            retval = true;
        }
    }

    return retval;
}


// EOF
