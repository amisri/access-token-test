;---------------------------------------------------------------------------------
;  File:        start.asm
;  Purpose:
;---------------------------------------------------------------------------------

; set some global document options for assembler listing generation

    .option D,T
    .length 102
    .width  140

;---------------------------------------------------------------------------------
; Add the entry point (c_int00()) at a well defined address specified in link.cmd
;---------------------------------------------------------------------------------
    .sect ".dspStartSection"

    .ref  _c_int00      ; code_entry_point

    .word _c_int00      ; code_entry_point

    .end
;---------------------------------------------------------------------------------
; end of file: start.asm
;---------------------------------------------------------------------------------
