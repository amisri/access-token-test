//! @file  property.c
//! @brief DSP property structure and communications functions
//
// PPM Properties
//
// Properties with the PPM flag set are assumed to to be an array of
// properties which will be selected using the mux index. This is used for
// sub-device properties (1-16) for multi-PPM and for PPM properties
// (0-MAX_USER).
//
// Some PPM properties are part of a structure array. The address of this
// array and the length of the struct are given to this library so that the
// propertyComms() function can know if a property is included and if so, it
// uses the mux index to access the relevant element of the array. Note that
// all PPM properties must have the PPM flag set and have the INDIRECT_NELS
// flag set if the length of the property can be different for each user.
//
// INDIRECT_NELS flag
//
// If this flag is set for a property, the n_elements field defines a pointer
// to the number of elements in the property. If the property is not PPM, then
// it points to an array, while for PPM properties it points to a scalar value
// in a structure array.


// ---------- Includes

#include <string.h>

#include "inc/fgc_parser_consts.h"

#include "fgc3/c6727/inc/pars.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcom.h"



// Function memory allocation

#pragma CODE_SECTION(propertyBgp, ".ext_code")



// ---------- External variable definitions

#pragma DATA_SECTION(property_fgc, ".ext_data")

struct Property_fgc  property_fgc;



// ---------- Internal function definitions

//! This function is called when the MCU FcmTsk(), PubTsk() or ScmTsk() want to
//! Set or Get a block of property data.
//!
//! If the last block of data is set and the property has shrunk, the function
//! must clear all the excess elements to zero.
//!
//! This function is called in BGP context.
//!
//! @param[in,out]pbuf          A pointer to the property buffer.

static void propertyComms(volatile struct Dpcom_prop_buf * pbuf)
{
    uint32_t      get_f;           // Get action
    uint32_t      set_f;           // Set action
    uint32_t      last_pkt_f;      // Last packet flag
    uint32_t      n_els_old;       // Current number of elements in the property
    uint32_t      n_els_new;       // New number of elements in the property
    struct prop * ptbl;            // Property table entry
    size_t        copy_size;       // size to copy
    size_t        blk_offset;      // Block start index in property value
    size_t        prop_size;       // Size of property data for one user index
    size_t        elem_size;       // Element size
    uint32_t    * n_elem_addr;     // Pointer to the number of elements
    void        * dsp_prop_ptr;    // Pointer to float property value
    void        * dpram_ptr;       // Pointer to the shared memory
    char        * buf_addr = NULL; // Pointer to start of the value buffer

    pbuf->dsp_rsp = PROP_DSP_RSP_OK;

    get_f      = testBitmap(pbuf->action, FGC_FIELDBUS_FLAGS_GET_CMD);
    set_f      = testBitmap(pbuf->action, FGC_FIELDBUS_FLAGS_SET_CMD);
    last_pkt_f = testBitmap(pbuf->action, FGC_FIELDBUS_FLAGS_LAST_PKT);

    // Offset to start of specified block

    blk_offset = pbuf->blk_idx * PROP_BLK_SIZE;

    ptbl = &prop[pbuf->dsp_prop_idx];

    // Size of property for one user index

    elem_size = ptbl->el_size;

    prop_size = ptbl->max_elements * elem_size;

    // Sanity check on the DSP property

    // Verify that the pointer to the number of elements is not NULL and property is not SUB_DEV

    if (   testBitmap(ptbl->flags, PF_INDIRECT_N_ELS == true)
        && (   ptbl->n_elements == NULL
            || testBitmap(ptbl->flags, PF_SUB_DEV) == true))
    {
        // Indicate error to the MCU and cancel request to complete the action

        pbuf->dsp_rsp = PROP_DSP_RSP_ERR_NELEM;
        pbuf->action  = 0;

        return;
    }

    // Verify that the data pointer is not NULL

    if (ptbl->value == NULL)
    {
        // Indicate error to the MCU and cancel request to complete the action

        pbuf->dsp_rsp = PROP_DSP_RSP_ERR_BUF;
        pbuf->action  = 0;

        return;
    }

    // Default address holding the number of elements

    uint32_t n_elem_offset = 0;

    // Process the MCU request

    propertyClassGetPointer((uint8_t *)ptbl->value,
                            ptbl->flags,
                            pbuf->sub_sel,
                            pbuf->cyc_sel,
                            (uint8_t **)&buf_addr,
                            &n_elem_offset);

    if (testBitmap(ptbl->flags, PF_INDIRECT_N_ELS) == true)
    {
        n_elem_addr = (uint32_t *)(ptbl->n_elements + n_elem_offset);
    }
    else
    {
        n_elem_addr = (uint32_t *)&ptbl->n_elements;
    }

    n_els_old = *n_elem_addr;
    n_els_old = MIN(n_els_old, ptbl->max_elements);

    // Calculate the size of the block to be copied

    // Number of words from start of block to end of property.
    // Clip to size of one block

    copy_size = prop_size - blk_offset;
    copy_size = MIN(copy_size, PROP_BLK_SIZE);

    dpram_ptr    = (void *)&pbuf->blk;
    dsp_prop_ptr = (void *)(buf_addr + blk_offset);

    // Transfer data according to action

    if (get_f)
    {
        memcpy(dpram_ptr, dsp_prop_ptr, copy_size);

        pbuf->n_elements = n_els_old;
    }
    else
    {
        if (set_f)
        {
            memcpy(dsp_prop_ptr, dpram_ptr, copy_size);
        }

        // Clip to property's max_elements

        n_els_new = MIN(pbuf->n_elements, ptbl->max_elements);

        // If setting last block for property, check if data has shrunk

        if (last_pkt_f)
        {
            if (n_els_new < n_els_old)
            {
                if (testBitmap(ptbl->flags, PF_DONT_SHRINK) == true)
                {
                    n_els_new = n_els_old;
                }
                else
                {
                    // Zero excess values

                    memset((void *)(buf_addr + n_els_new * elem_size), 0, (n_els_old - n_els_new) * elem_size);
                }
            }
        }
        else
        {
            // Don't change the length until the last block

            if (n_els_new <= n_els_old)
            {
                n_els_new = n_els_old;
            }
        }

        // If the number of elements has changed, save it in n_elements (indirect)

        if (n_els_new != n_els_old)
        {
            *n_elem_addr = n_els_new;
        }

        // If the property is linked to a parameter group, set the change flag for the pars group

        if (last_pkt_f && ptbl->pars_idx)
        {
            parsUpdateMask(ptbl->pars_idx);
        }
    }

    // Cancel the request to indicate the action is completed

    pbuf->action = 0;
}



// ---------- External function definitions

void propertyInit(void)
{
    propertyClassInit();

    memset(&property_fgc, 0, sizeof(property_fgc));
}



bool propertyBgp(void)
{
    // Check for requests from the MCU to set/get DSP properties

    // Publication task action

    if (dpcom.pcm_prop.action != 0)
    {
        propertyComms(&dpcom.pcm_prop);

        return true;
    }

    // Fieldbus command task action

    if (dpcom.fcm_prop.action != 0)
    {
        propertyComms(&dpcom.fcm_prop);

        return true;
    }

    // Serial command task action

    if (dpcom.tcm_prop.action != 0)
    {
        propertyComms(&dpcom.tcm_prop);

        return true;
    }

    return (parsGroupSet());
}


// EOF
