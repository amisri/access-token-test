//! @file  main.h
//! @brief FGC3 DSP main function


// ---------- Includes

#define DEFINECCPRINTFDUMMY
#include <cclibs.h>

#include "fgc3/c6727/inc/anaCard.h"
#include "fgc3/c6727/inc/bgp.h"
#include "fgc3/c6727/inc/calibration.h"
#include "fgc3/c6727/inc/control.h"
#include "fgc3/c6727/inc/cycle.h"
#include "fgc3/c6727/inc/dsp.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/main.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/spivs.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/inc/crate.h"
#include "fgc3/inc/dpcom.h"
#include "inc/version.h"


// Function memory allocation

#pragma CODE_SECTION(main, ".ext_code")



// ---------- External function definitions

void main(void)
{
    bgpInit();

    // Initialize the DSP

    dspInit();

    // Wait for the MCU to start

    while (dpcom.mcu.handshake != MCU_STARTED)
    {
        ;
    }

    dpcom.dsp.version = version.unixtime;

    // Initialise modules: the order of initialization is important

    profileInit();

    iterInit();

    propertyInit();

    crateInit();

    spivsInit((FGC_CLASS_ID == 63));

    anaCardInit();

    calibrationInit();

    cycleInit();

    controlInit();

    signalsInit();

    dpcom.dsp.handshake = DSP_STARTED;

    // Ready to start the real-time task

    dspInitInterrupts();

    // Start the background processing

    bgpProcess();
}


// EOF

