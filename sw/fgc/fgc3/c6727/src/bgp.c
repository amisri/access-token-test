//! @file  bgp.c
//! @brief Background Processing Functions


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>

#include "fgc3/c6727/inc/anaCard.h"
#include "fgc3/c6727/inc/bgp.h"
#include "fgc3/c6727/inc/calibration.h"
#include "fgc3/c6727/inc/control.h"
#include "fgc3/c6727/inc/dsp.h"
#include "fgc3/c6727/inc/isr.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/pars.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcom.h"



// ---------- Constants

extern int _STACK_END;
extern int _STACK_SIZE;
extern int _stack;

static uint32_t * stack_init       = (uint32_t *)(uintptr_t)(&_stack);
static uint32_t * stack_end        = (uint32_t *)(uintptr_t)(&_STACK_END);
static uint32_t * stack_end_cached = (uint32_t *)(uintptr_t)(&_STACK_END);
static uint32_t   stack_size       = (uint32_t)(&_STACK_SIZE);
static uint32_t   stack_pattern    = 0x0BADBABE;
static uint32_t * stack_ptr;


// Function memory allocation

#pragma CODE_SECTION(bgpInit,             ".ext_code")
#pragma CODE_SECTION(bgpProcess,          ".ext_code")
#pragma CODE_SECTION(bgpDebugMemTransfer, ".ext_code")

uint32_t bgpTmp = 0;



// ---------- Internal function definitions

//! This function is called to transfer the debug zone of 32 words to the
//! MCU where it is mapped onto the property FGC.DEBUG.MEM.DSP. The user
//! can define the address of the 32 words to copy using the same property.

static void bgpDebugMemTransfer(void)
{
    uint32_t * dsp_addr = (uint32_t *)dpcom.mcu.debug_mem_dsp_addr;
    uint8_t    i;

    for (i = 0; i < DB_DSP_MEM_LEN; ++i, dsp_addr++)
    {
        dpcom.dsp.debug_mem_dsp_i[i] = *dsp_addr;
        dpcom.dsp.debug_mem_dsp_f[i] = *(float*)dsp_addr;
    }
}



//! Checks if new MCU command must be processed

static void bgpProcessCommands(void)
{
    uint32_t commands_received;

    if (dpcmdGet(&dpcom.mcu.commands, &commands_received) == true)
    {
        // Reset requested by the MCU

        if (testBitmap(commands_received, DPCOM_MCU_COMMAND_RESET) == true)
        {
            bgpClassProcessCommands(DPCOM_MCU_COMMAND_RESET);
        }

        // MCU has finished its initialization

        if (testBitmap(commands_received, DPCOM_MCU_COMMAND_INIT) == true)
        {
            bgpClassProcessCommands(DPCOM_MCU_COMMAND_INIT);

            isrBgp();
        }

        // NVS synchronisation completed

        if (testBitmap(commands_received, DPCOM_MCU_COMMAND_NVS_INIT) == true)
        {
            bgpClassProcessCommands(DPCOM_MCU_COMMAND_NVS_INIT);

            signalsUpdateAdcTempThresholds();

            // Run all the pars functions with the values recovered from NVS

            parsRunAllFunc();
        }

        // DB synchronisation completed

        if (testBitmap(commands_received, DPCOM_MCU_COMMAND_DB_INIT) == true)
        {
            bgpClassProcessCommands(DPCOM_MCU_COMMAND_DB_INIT);

            signalsUpdateAdcTempThresholds();
        }

        // Update the ADC temperature threshold, mapped to ADC.INTERNAL.VREF_TEMP.REF

        if (testBitmap(commands_received, DPCOM_MCU_COMMAND_ADC_TEMP) == true)
        {
            signalsUpdateAdcTempThresholds();
        }

        // If requested, update libreg parameters that aren't exposed to the user as properties

        if (testBitmap(commands_received, DPCOM_MCU_COMMAND_UPDATE_REG_PARS) == true)
        {
            bgpClassProcessCommands(DPCOM_MCU_COMMAND_UPDATE_REG_PARS);
        }

        // ILC reset requested by the MCU

        if (testBitmap(commands_received, DPCOM_MCU_COMMAND_ILC_RESET) == true)
        {
            bgpClassProcessCommands(DPCOM_MCU_COMMAND_ILC_RESET);
        }

        // Disarm a function

        if (testBitmap(commands_received, DPCOM_MCU_COMMAND_REF_DISARM) == true)
        {
            bgpClassProcessCommands(DPCOM_MCU_COMMAND_REF_DISARM);
        }
    }

    if (dpcom.mcu.ana.req_f == true)
    {
        anaCardProcessMpxRequest();
    }
}



//! Checks the stack usage every second

static void bgpCheckStack(void)
{
    if (*stack_end_cached != stack_pattern)
    {
        stack_ptr = stack_init;

        while (stack_ptr < stack_end_cached && *stack_ptr == stack_pattern)
        {
            stack_ptr++;
        }

        dpcom.dsp.stack_usage = ((100 * sizeof(uint32_t)) * (uint32_t)(stack_end - stack_ptr)) / stack_size;
        stack_end_cached = stack_ptr - 1;
    }
}



// ---------- External function definitions

void bgpInit(void)
{
   ; // Do nothing
}




void bgpProcess(void)
{
    for (;;)
    {

        TGL_TP1();

        profileCheckWatchPointBgp(1);

        // Reset the counter incremented by the MCU.

        dpcom.dsp_bgp_alive = 0;

        bgpProcessCommands();

        profileCheckWatchPointBgp(2);

        propertyBgp();

        profileCheckWatchPointBgp(3);

        bgpProcessCommands();

        signalsBgp();

        profileCheckWatchPointBgp(4);

        controlBgp();

        bgpProcessCommands();

        profileCheckWatchPointBgp(5);

        calibrationBgp();

        bgpDebugMemTransfer();
        bgpCheckStack();

        profileCheckWatchPointBgp(6);
    }
}


// EOF
