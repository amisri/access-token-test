//! @file  control.c
//! @brief Interface to the cclibs libraries


// ---------- Includes

#include <stdbool.h>

#include "fgc3/c6727/inc/control.h"
#include "fgc3/c6727/inc/cycle.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/log.h"
#include "fgc3/c6727/inc/profile.h"
#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/signals.h"
#include "fgc3/c6727/inc/spivs.h"



// Function memory allocation

#pragma CODE_SECTION(controlBgp, ".ext_code")



// ---------- Internal function declarations

static void controlResetRefDac();



// ---------- External function definitions

void controlInit(void)
{
    memset(&property_fgc.test.dsp, 0, sizeof(property_fgc.test.dsp));

    logInit();

    CC_ASSERT(controlClassInit() == true);
}



void controlRtp(void)
{
    if (iter.state_op == FGC_OP_TEST)
    {
        // Set DAC value from REF.DAC

        signalsSetDacTest(SIGNALS_DAC_1, property_fgc.analog.dac[0]);
        signalsSetDacTest(SIGNALS_DAC_2, property_fgc.analog.dac[1]);

        // Continuously test the SPIVS bus

        spivsTest();
    }

    // Process cycle related functionality

    cycleRtp();

    controlResetRefDac();

    // Class specific functionality

    profileCheckWatchPointRtp(20);

    controlClassRtp();

    // Log all signals

    profileCheckWatchPointRtp(21);

    logRtp();
}



void controlBgp(void)
{
    // Class specific processing

    controlClassBgp();

    logBgp();

    cycleBgp();
}



// ---------- Internal function definitions

static void controlResetRefDac()
{
    static uint32_t prev_state_op = FGC_OP_UNCONFIGURED;

    if (prev_state_op != dpcom.mcu.state_op)
    {
        // When done with TEST or SIMULATION, clear REF.DAC

        if (prev_state_op == FGC_OP_TEST)
        {
            property_fgc.analog.dac[0] = property_fgc.analog.dac[1] = 0;
        }

        prev_state_op = dpcom.mcu.state_op;
    }
}


// EOF
