//! @file     profile.c
//! @brief    Functions to profile the execution time of a section of code.
//!
//! There are two profiling systems:
//!
//! 1. FGC.DEBUG.DSP.ELAPSED_TIME
//! 2. ACQ log signals DSP_RT_PROF_0 and DSP_RT_PROF_1 selected by FGC.DEBUG.DSP.RT_PROFIL


// ---------- Includes

#include "fgc3/c6727/inc/profile.h"
#include "fgc3/inc/time_fgc.h"



// ---------- External variable definitions

struct Profile profile;



// ---------- Internal function definitions

static inline void profileCheckWatchPoint(struct Profile_watch_point * watch_point)
{
    uint32_t const value = *((uint32_t *)watch_point->address);

    if (   ((watch_point->control & 0x0001) != 0 && (value & ~watch_point->mask) == watch_point->value)
        || ((watch_point->control & 0x0002) != 0 && (value & ~watch_point->mask) != watch_point->value))
    {
        watch_point->hit_count++;
        watch_point->control       = 0;
        watch_point->timestamp_s   = timeGetUtcTime();
        watch_point->timestamp_us  = timeGetUtcTimeUs();
    }
}



// ---------- External function definitions

void profileInit(void)
{
    // Reset the profile structure

    memset(&profile, 0, sizeof(profile));

    // Save the free-running counter

    profile.dsp_rt.mpx[0] = FGC_DSP_RT_PROFIL_START;
    profile.dsp_rt.mpx[1] = FGC_DSP_RT_PROFIL_END;
}



void profileCheckWatchPointBgp(uint8_t const id)
{
    struct Profile_watch_point * watch_point = &profile.watch_point;

    if (watch_point->control != 0)
    {
        watch_point->bgp_check_id = id;

        profileCheckWatchPoint(watch_point);
    }
}



void profileCheckWatchPointRtp(uint8_t const id)
{
    struct Profile_watch_point * watch_point = &profile.watch_point;

    if (watch_point->control != 0)
    {
        watch_point->isr_check_id = id;

        profileCheckWatchPoint(watch_point);
    }
}


// EOF
