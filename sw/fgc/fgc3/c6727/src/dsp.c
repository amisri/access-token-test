//! @file  dsp.c
//! @brief Functionality specific to the DSP TMS320C6727


// ---------- Includes

#include <stdint.h>

#include <c6x.h>

#include "fgc3/c6727/inc/dsp.h"



// Internal variable declaration

//! Vector table address as defined by the linker

extern far int vector_start;



// Internal functions declaration

//! Invalidates and enables Cache

static void dspCacheEnable(void);

//! Initialise the SPI1 serial port.

static void dspInitSpi(void);

//! Initialise the DSP timers. RTIFRC0 counts in us.

static void dspInitTimers(void);

//! Initialises the DSP hardware.

static void dspInitHw(void);



// ---------- Internal function definitions

static void dspCacheEnable()
{
    *L1PSAR = 0x00000000;                                       // Invalidate start address
    *L1PICR = *L1PICR | L1P_INVALIDATE;                         // Invalidate all lines of cache by setting L1P bit

    while (*L1PICR != 0x00000000u);                             // Make Sure Cache Invalidate is complete

    CSR = (CSR & (~CACHE_CTRL_MASK)) | CACHE_ENABLE;            // Enable the Cache
}



static void dspInitSpi(void)
{
    *(volatile uint32_t *)(SPI1_REG_START + 0)    = 1;          // Release sw reset
    *(volatile uint32_t *)(SPI1_REG_START + 4)    = 0x3;        // Master mode
    *(volatile uint32_t *)(SPI1_REG_START + 0x14) = 0xE00;      // SPIPC0 (SIMO,SOMI,CLK enabled)
    *(volatile uint32_t *)(SPI1_REG_START + 0x50) = 0xFF10;     // SPIFMT0 (fmt0 selected by default)
    *(volatile uint32_t *)(SPI1_REG_START + 4)   |= 0x01000000; // Enable SPI1
}



static void dspInitTimers(void)
{
    *RTICPUC0 = 0x1;                                            // Profiler - 75 MHz - Set prescale value at 1 (SYSCLK2 runs at 150MHz)
    *RTICPUC1 = 0x249EF;                                        // MS       -  1 KHz - Set prescale value at 150000 (SYSCLK2 runs at 150MHz)
    *RTIGCTRL = 0x03;                                           // start counters 0 and 1
}



static void dspInitHw(void)
{
    // Initialise the DSP as the boot loader does in the source file
    // bootLoader.c:DspBootLoader() (1st 1 Kb loaded at boot time).
    // Added here for debugging purposes only.
    //
    // With ClkIn 25MHz, D0=0, D1=1, D2=3, D3=5, M=12
    //      PllOut  = 300 MHz
    //      SysClk1 = 150 Mhz
    //      SysClk2 =  75 MHz
    //      SysClk3 =  50 MHz
    //
    // The time for the test points were (needs to be measured again):
    //
    //  TP1 TP0
    //    0   1 Start
    //    0   1 ---------        +6us
    //          after PLLDIV1
    //          go alignment command
    //          quit pll reset state
    //    1   1 ---------        +9us
    //          wait for lock
    //    0   1 ---------        +6us
    //          EMIF config
    //    0   0 ---------        +434ns
    //          set mem in 32bits
    //    1   1 ---------        +100ns
    //          start loading
    //    0   0 ---------        +290ms
    //          jump to Prog
    //
    //
    // With ClkIn 25MHz, D0=0, D1=0, D2=1, D3=2, M=12
    //      PllOut  = 300 MHz
    //      SysClk1 = 300 Mhz
    //      SysClk2 = 150 MHz
    //      SysClk3 = 100 MHz
    //
    // The time for the test points were (needs to be measured again):
    //
    //  TP1 TP0
    //    0   1 Start
    //    0   1 ---------        +6us
    //          after PLLDIV1
    //          go alignment command
    //          quit pll reset state
    //    1   1 ---------        +5us
    //          wait for lock
    //    0   1 ---------        +3us
    //          EMIF config
    //    0   0 ---------        +177ns
    //          set mem in 32bits
    //    1   1 ---------        +31ns
    //          start loading
    //    0   0 ---------        +290ms
    //          jump to Prog


    // EMIF_SDCR : SDRAM Configuration Register

    *(volatile uint8_t *)(EMIF_SDCR + 3) = 0x80;                // Enter self-refresh mode


    // GPIOEN : General Purpose I/O Enable Register

    *(volatile uint32_t *)GPIOEN = 0x000000C0;                  // b6=1, UHPI_HINT pin function as GPIO pin
                                                                // b7=1, UHPI_HD[7:0] pins function as GPIO pins
    *GPIODAT1 = 0x0001;                                         // TP0 = 1, TP1 = 0


    // GPIODIR1 : General Purpose I/O Data Direction Register 1

    *(volatile uint32_t *)GPIODIR1 = 0x0007;                    // bits b2,b1,b0 as outputs

    // GPIODIR2 : General Purpose I/O Data Direction Register 2

    *(volatile uint32_t *)GPIODIR2 = 0x0100;                    // bits b8 as outputs


    // Universal Host Port Interface - Configure GPIO pins

    *GPIODAT1 = 0x0001;                                         // TP0 = 1, TP1 = 0

    // Init the PLL registers to have the full DSP CPU and EMIF speed
    // DSP CPU = 300MHz
    // EMIF = 100MHz
    // The external clock is 25MHz
    //
    // Following the steps described in spru879a.pdf 3.1.1:
    //
    //   1  PLLCSR.PLLEN = 0 (bypass mode) (not required when the device is coming out of reset)
    //   2  wait 4 cycles of the slowest of PLLOUT or reference clock source (CLKIN or OSCIN) (not required when the device is coming out of reset)
    //   3  PLLCSR.PLLRST = 1 (PLL is reset) (not required when the device is coming out of reset)
    //   4  program PLLDIV0
    //   5  program PLLM
    //   6  program PLLDIV1, apply the GO operation to change the divider to new ratio
    //   7  program PLLDIV2, apply the GO operation to change the divider to new ratio
    //   8  program PLLDIV3, apply the GO operation to change the divider to new ratio
    //   9  wait for PLL to properly reset
    //  10  PLLCSR.PLLRST = 0 (to bring PLL out of reset)
    //  11  wait for PLL to lock
    //  12  PLLCSR.PLLEN = 1 (enable PLL mode)


    // PLL_CSR : PLL Control/Status Register

    // When PLLEN is off DSP is running with CLKIN clock source, currently 25MHz or 40ns clk rate.

    *PLL_CSR  &= ~0x00000001;                                   // Clear the enable bit (Bypass mode)
    *PLL_CSR  &= ~0x00000010;                                   // Clear power-down, PLL is operational
    *PLL_CSR  |=  0x00000008;                                   // Activate PLL reset, PLL takes 125ns to reset

    // PLL_DIV0 : PLL Controller Divider Register 0
    // PLLOUT = CLKIN/(DIV0.RATIO+1) * PLLM
    // 300MHz = 25MHz/1 * 12

    *PLL_DIV0 = 0x00008000;                                     // Divider D0 enable with ratio 0 (so divides by 1)

    // PLL_M : PLL Multiplier Control Register

    *PLL_M = 12;


    // The DSP requires that peripheral clocks be less than 1/2 the CPU clock
    // at all times. The order is not critical since changes need a GO to
    // become effective.

    // PLL_DIV3 : PLL Controller Divider Register 3

    *PLL_DIV3 = 0x00008002;                                     // Divider D3 enable with ratio 2 (so divides by 3)


    // PLL_DIV2 : PLL Controller Divider Register 2

    *PLL_DIV2 = 0x00008001;                                     // Divider D2 enable with ratio 1 (so divides by 2)


    // PLL_DIV1 : PLL Controller Divider Register 1


    *PLL_DIV1 = 0x00008000;                                     // Divider D1 enable with ratio 0 (so divides by 1)


    // PLL_CMD : PLL Controller Command Register

    *GPIODAT1 = 0x0002;                                         // TP0 = 0, TP1 = 1


    // All SYSCLKs must be aligned. The ALNn bits in ALNCTL should
    // always be set to 1 before a GO operation

    // ALN_CTL : PLL Controller Clock Align Control Register

    *ALN_CTL  = 0x00000007;                                     // All SYSCLK3, SYSCLK2, SYSCLK1 will be aligned


    *PLL_CMD  = 0x00000001;                                     // Command GO


    // PLL_STAT : PLL Controller Status Register

    // Wait for the GO operation to complete

    while (*PLL_STAT  == 0x00000001) { ; }

    *PLL_CSR &= ~0x00000008;                                    // Clear b3 PLLRST to bring PLL out of reset


    *GPIODAT1 = 0x0003;                                         // TP0 = 1, TP1 = 1

    //  Wait for PLL to lock

    while ((*PLL_CSR & 0x00000040) != 0x00000040) { ; }

    *PLL_CSR |= 0x00000001;                                     // Set PLLCSR.PLLEN = 1 to enable PLL mode

    // EMIF peripheral: http://www.ti.com/lit/ug/spru711c/spru711c.pdf

    // The PLL is now running at 300MHz
    // SysClk1 : 300MHz        CPU and Memory (Max 300MHz)
    // SysClk2 : 150MHz        Peripheral and dMax
    // SysClk3 : 100MHz        EMIF (Max 133MHz when CPU 266MHz)
    // SDRAM   :               100 MHz

    *GPIODAT1 = 0x0001;                                         // TP0 = 1, TP1 = 0

    // EMIF setup

    *EMIF_SDTIMR   = 0x31114610;
    *EMIF_SDSRETR  = 0x00000006;
    *EMIF_SDRCR    = 0x0000061a;
    *EMIF_SDCR     = 0x00000721;                                // 32Bit, CAS=2, 4 banks, 9 columns
    *EMIF_A1CR     = 0x086225be;                                // WE strobe mode, 32bit


    // Universal Host Port Interface - Configure GPIO pins
    // UHPI_H[0] = TP0
    // UHPI_H[1] = TP1
    // UHPI_H[2] = 32bits access to PLD DPRAM

    // Signal the FPGA that the DSP EMIF data-bus size is now 32 bits

    // GPIODAT1 : General Purpose I/O Data Register 1

    *GPIODAT1 = 0x0004;                                         // b2=1 -> 32bits access to FPGA DPRAM


    // Enable L1 Program Cache

    dspCacheEnable();

    // EMIF_EIMSR : EMIF Interrupt Mask Set Register

    *EMIF_EIMSR = 0x00000005;                                   // Leave EMIF interrupts enabled
}



// ---------- External function definitions

void dspInit(void)
{
    // Initialises the DSP TMS320C6727. This function is declared in dsp.h.

    dspInitHw();
    dspInitTimers();
    dspInitSpi();
}



void dspInitInterrupts(void)
{
    // Relocate interrupt table

    ISTP = (unsigned int)&vector_start;

    // Clear all non-maskable interrupts and enable all interrupts including NMI.

    ICR = 0xFFF0;
    IER = 0x0002;

    ACKNOWLEDGE_NMI();

    ENABLE_INTS();

    ENABLE_TICKS();
}


// EOF
