//! @file     spivs.c
//! @brief    Interface to the SPIVS bus
//!
//! The SPIVS is a Serial Protocol Interface whereby each long word
//! sent implies a log word received and vice-versa. A long word is
//! defined as 32 bits.
//!
//! Currently, only one-shot communication is implemented. If
//! continuous communication is required, supporting code must
//! be added.


// ---------- Includes

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "fgc3/c6727/inc/property.h"
#include "fgc3/c6727/inc/spivs.h"
#include "fgc3/c6727/inc/utils.h"
#include "fgc3/inc/bitmap.h"
#include "fgc3/inc/dpcom.h"
#include "fgc3/inc/memmap.h"
#include "fgc3/inc/sleep.h"



// ---------- Constants

//! SPIVS clock: 6.25 MHz. Sending 32 bits -> 5,12 us < 6 us.

#define SPIVS_TRANS_TIME     6

//! Timeout for a transmission to complete: 24 us.

#define SPIVS_TRANS_TIMEOUT  SPIVS_TRANS_TIME * 4

//! SPIVS tx buffer size.

#define SPIVS_TRANS_LENGTH    7

//! SPIVS log buffer size.

#define SPIVS_LOG_SIZE       20

//! Length of the debugging buffers
#define SPIVS_DEBUG_LENGTH    10



// DSP constants

#define  SPIVS_CTRL_RST_MASK      SPIVS_CTRL_RST_MASK32
#define  SPIVS_CTRL_SOURCE_MASK   SPIVS_CTRL_SOURCE_MASK32
#define  SPIVS_CTRL_INV_MASK      SPIVS_CTRL_INV_MASK32
#define  SPIVS_CTRL_LOOP_MASK     SPIVS_CTRL_LOOP_MASK32
#define  SPIVS_CTRL_COMMIT_MASK   SPIVS_CTRL_COMMIT_MASK32



// ---------- Internal structures, unions and enumerations

//! Possible SPIVS test modes.

enum Spivs_test_modes
{
    SPIVS_TEST_NONE,                   //!< Does not do any test.
    SPIVS_TEST_SEND_ONE_INC_MONO,      //!< Sends one value incremented monotonically.
    SPIVS_TEST_SEND_ONE_INC_MONO_SYNC, //!< Sends one value incremented monotonically at the SPIVS tick.
    SPIVS_TEST_SEND_TWO_INC_MONO,      //!< Sends two values incremented monotonically .
    SPIVS_TEST_SEND_ONE_USER_DEFINED,  //!< Sends one value defined by the user.
    SPIVS_TEST_SEND_TWO_USER_DEFINED,  //!< Sends two values defined by the user.
    SPIVS_TEST_BASIC_GO_MODE,          //!< The two Tx buffers are filled in and asynchronously transmitted in alternative iterations..
    SPIVS_TEST_BASIC_SYNC_MODE,        //!< The two Tx buffers are filled in and synchronously transmitted in alternative iterations..
    SPIVS_TEST_SEND_ONE_SHOT,          //!< Sends one value incremented monotonically when specified by the user.
    SPIVS_TEST_LOOPBACK                //!< Configures the SPIVS in loopback mode and sends user defined values.
};




// ---------- External variable definitions

struct Spivs spivs;



// ---------- Internal function declarations

static enum Spivs_result spivsTestPeripheral(void);
static enum Spivs_result spivsTestTransmission(enum Spivs_test_modes mode);
static void              spivsTestInitTxBuffers(void);
static enum Spivs_result spivsTestGoMode(void);



// ---------- External function definitions

void spivsInit(bool const sync)
{
    // By default the SPIVS is governed by the DSP

// #ifdef __RX610__
//     setBitmap(CPU_MODEH_P, CPU_MODEH_SPIVS_MASK8);
// #endif

    // The reset signal must be high for at least 40 ns.

    setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_RST_MASK);

    sleepUs(10);

    clrBitmap(SPIVS_CTRL_P, SPIVS_CTRL_RST_MASK);

    if (sync == true)
    {
        // Start communication on reception of the SPIVS tick

        setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_SOURCE_MASK);
    }

    dpcom.spivs.dbg_idx = 0;

    spivs.sync = sync;

    // Set time offset for received SPI data - what is read on this iteration
    // was received on the previous iteration and it was a loop back by
    // the VSREG_DSP board of the value sent on the iteration before that.
    // So the offset it two iterations.

    spivs.received_time_offset = -200.0E-6;
}



void spivsSend(uint32_t const * tx_data, uint8_t length)
{
    volatile uint32_t * spivs_tx_ptr = (volatile uint32_t *)SPIVS_TB_32;
    float       * const tx_data_0    = (float *)tx_data;

    // Set the number of words to transmit

    SPIVS_NUM_LWORDS_P = length;

    while (length-- > 0)
    {
        *spivs_tx_ptr++ = *tx_data++;
    }

    setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_COMMIT_MASK);

    // Save first sent value for logging

    spivs.sent_0 = *tx_data_0;
}



void spivsRead(uint32_t * rx_data, uint8_t length)
{
    volatile uint32_t const * spivs_rx_ptr = (volatile uint32_t *)SPIVS_RB_32;
    float           * const   rx_data_0    = (float *)rx_data;

    while (length-- > 0)
    {
        *rx_data++ = *spivs_rx_ptr++;
    }

    // Save first received value for logging

    spivs.received_0 = *rx_data_0;
}



enum Spivs_result spivsValidate(uint32_t const * tx_value,
                                uint32_t const * rx_value,
                                uint8_t  const   num_validate)
{
    static uint8_t dbg_post_fault_idx = SPIVS_HISTORY_LENGTH / 2;
    static bool    dbg_post_fault     = false;
    static bool    enable_f           = false;

    // Update the SPIVS debug status (FGC.DEBUG.SPIVS)

    // When an error is detected SPIVS_HISTORY_LENGTH / 2 words will be
    // recorded and latched until the property is ZEROed which is
    // signaled by the MCU with dpcom.spivs.dbg_idx = 0xFFFFFFFF.
    // Ignore the first 5 validations since there are always errors.

    if (enable_f == false)
    {
        if (++dpcom.spivs.dbg_idx < 5)
        {
            return SPIVS_OK;
        }

        enable_f = true;
    }

    // Update FGC.DEBUG.SPIVS {Sent}

    dpcom.spivs.num_sent++;

    // Reset counters when G FGC.DEBUG.SPIVS ZERO

    if (dpcom.spivs.dbg_idx == 0xFFFFFFFF)
    {
        dpcom.spivs.dbg_idx = 0;
        dbg_post_fault_idx  = SPIVS_HISTORY_LENGTH / 2;
        dbg_post_fault      = false;
    }

    enum  Spivs_result  retval = SPIVS_OK;
    uint8_t             idx;

    // Validate the data transmitted

    for (idx = 0; idx < num_validate; ++idx)
    {
        if (tx_value[idx] != rx_value[idx])
        {
            // Update FGC.DEBUG.SPIVS {Faults}

            dpcom.spivs.num_faults++;

            // Once a fault is detected, continue filling FGC.DEBUG.SPIVS during
            // SPIVS_HISTORY_LENGTH / 2 entries, at which point the log is frozen
            // until G FGC.DEBUG.SPIVS ZERO is issued.

            dbg_post_fault = true;

            retval = SPIVS_INVALID;
        }
    }

    // Check if an error was detected and the log is now frozen

    if (dbg_post_fault_idx == 0)
    {
        return SPIVS_OK;
    }

    // Update FGC.DEBUG.SPIVS {Tx and Rx}

    struct Dpcom_spivs_dbg * const spiv_dbg = (struct Dpcom_spivs_dbg *)&dpcom.spivs.dbg[dpcom.spivs.dbg_idx];

    for (idx = 0; idx < SPIVS_DBG_NUM; ++idx)
    {
        spiv_dbg->tx[idx] = tx_value[idx];
        spiv_dbg->rx[idx] = rx_value[idx];
    }

    // Update FGC.DEBUG.SPIVS {Error}

    spiv_dbg->error = (retval == SPIVS_INVALID);

    // Decrease the post-error logs

    if (dbg_post_fault == true)
    {
        --dbg_post_fault_idx;
    }

    // Increment the log index and wrap over if necessary

    if (++dpcom.spivs.dbg_idx >= SPIVS_HISTORY_LENGTH)
    {
        dpcom.spivs.dbg_idx = 0;
    }

     return retval;
}



enum Spivs_result spivsWait(uint8_t length)
{
    sleepUs(SPIVS_TRANS_TIME * length);

    return (testBitmap(SPIVS_CTRL_P, SPIVS_CTRL_COMMIT_MASK) ? SPIVS_BUSY : SPIVS_OK);
}



enum Spivs_result spivsTest(void)
{
    static enum Spivs_test_modes prev_mode = SPIVS_TEST_NONE;

    enum Spivs_test_modes mode   = (enum Spivs_test_modes)property_fgc.test.dsp.test_int32s[0];
    enum Spivs_result     retval = SPIVS_OK;

    switch (mode)
    {
        case SPIVS_TEST_NONE:
            if (spivs.sync == true)
            {
                setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_SOURCE_MASK);
            }
            else
            {
                clrBitmap(SPIVS_CTRL_P, SPIVS_CTRL_SOURCE_MASK32);
            }
            clrBitmap(SPIVS_CTRL_P, SPIVS_CTRL_LOOP_MASK);
            break;

        case SPIVS_TEST_LOOPBACK:
            setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_LOOP_MASK);
            retval = spivsTestPeripheral();
            break;

        case SPIVS_TEST_BASIC_GO_MODE:
            if (prev_mode != mode)
            {
                clrBitmap(SPIVS_CTRL_P, SPIVS_CTRL_LOOP_MASK);
                clrBitmap(SPIVS_CTRL_P, SPIVS_CTRL_SOURCE_MASK32);

                spivsTestInitTxBuffers();
            }
            else
            {
                retval = spivsTestGoMode();
            }

            break;

        case SPIVS_TEST_BASIC_SYNC_MODE:
            if (prev_mode != mode)
            {
                clrBitmap(SPIVS_CTRL_P, SPIVS_CTRL_LOOP_MASK);
                clrBitmap(SPIVS_CTRL_P, SPIVS_CTRL_SOURCE_MASK32);

                spivsTestInitTxBuffers();

                setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_SOURCE_MASK32);
            }

            break;

        default:
            clrBitmap(SPIVS_CTRL_P, SPIVS_CTRL_LOOP_MASK);
            retval = spivsTestTransmission(mode);
            break;
    }

    prev_mode = mode;

    return retval;
}


// ---------- Internal function definitions

static enum Spivs_result spivsTestPeripheral(void)
{
    // property_fgc.test.dsp.test_int32s[0]     -> Test mode
    // property_fgc.test.dsp.test_int32s[1]     -> Number of user defined values
    // property_fgc.test.dsp.test_int32s[2..15] -> User defined values

    uint32_t    rx_data[SPIVS_TRANS_LENGTH];
    uint8_t     length;

    length = property_fgc.test.dsp.test_int32s[1];

    // Only update the number of words to transmit if it has changed

    if (1 != SPIVS_NUM_LWORDS_P)
    {
        SPIVS_NUM_LWORDS_P = 1;
    }

    spivsSend((uint32_t *)&property_fgc.test.dsp.test_int32s[2], length);

    setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_COMMIT_MASK);

    spivsWait(length);

    spivsRead(rx_data, length);

    // SPIVS bus in normal operation mode

    return (spivsValidate((uint32_t *)&property_fgc.test.dsp.test_int32s[2], rx_data, length));
}



static void spivsTestInitTxBuffers(void)
{
    // Set the Tx buffers to the values 1,2,3,4,5,6,7 and send the words in GO
    // mode. Do likewise for the second Tx buffer with 8,9,10,11,12,13,14.

    volatile uint32_t * tx_ptr = (volatile uint32_t *)SPIVS_TB_32;
    uint8_t  i;

    SPIVS_NUM_LWORDS_P = 7;

    for (i = 1; i < 8; ++i)
    {
        *tx_ptr++ = i;
    }

    setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_COMMIT_MASK);

    while (testBitmap(SPIVS_CTRL_P, SPIVS_CTRL_COMMIT_MASK))
    {
        ;
    }

    tx_ptr = (volatile uint32_t *)SPIVS_TB_32;

    for (i = 8; i < 15; ++i)
    {
        *tx_ptr++ = i;
    }

    setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_COMMIT_MASK);

    while (testBitmap(SPIVS_CTRL_P, SPIVS_CTRL_COMMIT_MASK))
    {
        ;
    }
}



static enum Spivs_result spivsTestGoMode(void)
{
    static uint32_t const tx_data[2][7] =
    {
        { 1, 2,  3,  4,  5,  6 , 7 },
        { 8, 9, 10, 11, 12, 13, 14 }
    };
    static int8_t  indx = -2;

    uint32_t          rx_data[7];
    enum Spivs_result retval;

    spivsRead(rx_data, 7);

    retval = spivsValidate(tx_data[indx], rx_data, 7);

    setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_COMMIT_MASK);

    if (++indx >= 2)
    {
        indx = 0;
    }

    return (retval);
}



static enum Spivs_result spivsTestTransmission(enum Spivs_test_modes mode)
{
    // property_fgc.test.dsp.test_int32s[0]     -> Test mode
    // property_fgc.test.dsp.test_int32s[1]     -> Number of user defined values
    // property_fgc.test.dsp.test_int32s[2..15] -> User defined values

    static uint32_t tx_data[2][2];
    static uint32_t rx_data[2];
    static uint32_t tx_value    = 0;
    static uint8_t  tx_indx     = 0;
    static uint8_t  indx        = 2;
    static uint8_t  const data_size[] =
    {
        0, // SPIVS_TEST_NONE
        1, // SPIVS_TEST_SEND_ONE_INC_MONO
        1, // SPIVS_TEST_SEND_ONE_INC_MONO_SYNC
        2, // SPIVS_TEST_SEND_TWO_INC_MONO
        1, // SPIVS_TEST_SEND_ONE_USER_DEFINED
        2, // SPIVS_TEST_SEND_TWO_USER_DEFINED
    };

    enum Spivs_result retval = SPIVS_OK;

    spivsRead(rx_data, data_size[mode]);

    retval = spivsValidate(&tx_data[tx_indx][0], &rx_data[0], data_size[mode]);

    switch (mode)
    {
        case SPIVS_TEST_SEND_ONE_INC_MONO_SYNC:

            // Fall through

        case SPIVS_TEST_SEND_ONE_INC_MONO:
            tx_data[tx_indx][0] = tx_value++;
            break;

        case SPIVS_TEST_SEND_TWO_INC_MONO:
            tx_data[tx_indx][0] = tx_value++;
            tx_data[tx_indx][1] = tx_value++;
            break;

        case SPIVS_TEST_SEND_ONE_USER_DEFINED:
            tx_data[tx_indx][0] = property_fgc.test.dsp.test_int32s[indx++];
            break;

        case SPIVS_TEST_SEND_TWO_USER_DEFINED:
            tx_data[tx_indx][0] = property_fgc.test.dsp.test_int32s[indx++];
            tx_data[tx_indx][1] = property_fgc.test.dsp.test_int32s[indx++];
            break;

        default:
            break;
    }

    if (mode == SPIVS_TEST_SEND_ONE_INC_MONO_SYNC)
    {
        setBitmap(SPIVS_CTRL_P, SPIVS_CTRL_SOURCE_MASK32);

        spivsSend(tx_data[tx_indx], data_size[mode]);
    }
    else if (mode == SPIVS_TEST_SEND_ONE_SHOT)
    {
        if (property_fgc.test.dsp.test_int32s[1] != 0)
        {
            property_fgc.test.dsp.test_int32s[1] = 0;

            tx_data[tx_indx][0] = tx_value++;

            spivsSend(tx_data[tx_indx], 1);
        }
    }
    else
    {
        clrBitmap(SPIVS_CTRL_P, SPIVS_CTRL_SOURCE_MASK32);

        spivsSend(tx_data[tx_indx], data_size[mode]);
    }

    tx_indx ^= 1;

    if (indx >= (property_fgc.test.dsp.test_int32s[1] + 2))
    {
        indx = 2;
    }

    return retval;
}


// EOF
