//! @file  panic.c
//! @brief DSP panic function


// ---------- Includes

#include <stdint.h>

#include "fgc3/c6727/inc/dsp.h"
#include "fgc3/c6727/inc/iter.h"
#include "fgc3/c6727/inc/panic.h"
#include "fgc3/inc/dpcom.h"



// Function memory allocation

#pragma CODE_SECTION(panic,        ".ext_code")
#pragma CODE_SECTION(assert_func,  ".ext_code")



// ---------- External function definitions

void panic(enum Panic_codes const panic_code)
{
    DISABLE_INTS();

    dpcom.dsp.run_code = RUNCODE_DSP_PANIC;

    dpcom.dsp.panic.panic_code = panic_code;

    for (;;)
    {
        ;
    }

    // Never returns
}



void assert_func(char const * file, int const line)
{
    dpcom.dsp.run_code    = RUNCODE_DSP_PANIC;
    dpcom.dsp.assert_line = line;

    // Convert the array from little endian to big endian

    dpcom.dsp.assert_file [0] = file[3];
    dpcom.dsp.assert_file [1] = file[2];
    dpcom.dsp.assert_file [2] = file[1];
    dpcom.dsp.assert_file [3] = file[0];
    dpcom.dsp.assert_file [4] = file[7];
    dpcom.dsp.assert_file [5] = file[6];
    dpcom.dsp.assert_file [6] = file[5];
    dpcom.dsp.assert_file [7] = file[4];
    dpcom.dsp.assert_file [8] = file[11];
    dpcom.dsp.assert_file [9] = file[10];
    dpcom.dsp.assert_file[10] = file[9];
    dpcom.dsp.assert_file[11] = file[8];
    dpcom.dsp.assert_file[12] = '\0';
    dpcom.dsp.assert_file[13] = file[14];
    dpcom.dsp.assert_file[14] = file[13];
    dpcom.dsp.assert_file[15] = file[12];

    panic(DSP_PANIC_ASSERT);
}


// EOF
