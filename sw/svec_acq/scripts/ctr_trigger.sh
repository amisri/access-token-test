#!/bin/bash

#
# Filename: ctr_trigger.sh
#
# Purpose:  Triggers a 1 second TTL pulse from the output O1 of the CTR
#
# Author:   Marc Magrans de Abril

set -e

echo 'ch1 (omsk 0x2,1 s1 omsk 0x2,0 s1)' | ctrtest
