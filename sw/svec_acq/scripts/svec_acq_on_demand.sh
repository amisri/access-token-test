#!/bin/bash

# Show help

while getopts "h" opt; do
    case $opt in
        h)
            echo "usage: $0 <hostname>"
            echo
            echo "Triggers an acquisition on the SVEC Acquistion system connected to $0"
            echo 
            echo "The Acquisition can be visualized by"
            echo "   1) Open the power navigator at https://cern.ch/powernav"
            echo "   2) Go to Data --> Open file"
            echo "   3) Open the following file:"
            echo "      Windows: \\cs-ccr-samba1\pclhc\var\log\svec_acq\data\$1"
            echo "      Linux:   /user/pclhc/var/log/svec_acq/data/$1"
            read -rsp $'Press any key to continue...\n' -n1 key
            exit 0
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            read -rsp $'Press any key to continue...\n' -n1 key
            exit 1
            ;;
    esac
done

# check missing hostname

if [ "$#" -ne 1 ]; then
    echo "usage: $0 <hostname>"
    echo
    echo "ERROR: Illegal number of parameters"
    read -rsp $'Press any key to continue...\n' -n1 key
    exit 1
fi

# check if svec_acq is running

ssh $1 "pgrep svec_acq"
if [ $? -ne 0 ]; then
    echo "ERROR: The service svec_acq is not running"
    echo
    read -rsp $'Press any key to continue...\n' -n1 key 
    exit 1
fi
 
echo "Executing on demmand trigger @$1..."
echo

ssh $1 "/user/pclhc/bin/Linux/x86_64/svec_acq_on_demand"

if [ $? -ne 0 ];
then
    echo "ERROR: ssh to $1 failed"
    echo
    read -rsp $'Press any key to continue...\n' -n1 key
    exit 1    
fi

pgrep svec_acq
retval=$?
if [ "$retval" = 1 ];
then
	echo "ERROR service svec_acq is not running"
	echo
    read -rsp $'Press any key to continue...\n' -n1 key
    exit 1 
fi 

echo
echo "Generating on demand acquisition..."
echo

# Show virtual progress

for i in {1..10}; do
    sleep 1
    printf "\r... ${i}0%% .."

done

# Show final message

echo
echo
echo "The Acquisition can be visualized by"
echo "   1) Open the power navigator at https://cern.ch/powernav"
echo "   2) Go to Data --> Open file"
echo "   3) Open the following file:"
echo "      Windows: \\\\cs-ccr-samba1\pclhc\var\log\svec_acq\data\\$1"
echo "      Linux:   /user/pclhc/var/log/svec_acq/data/$1"

echo 
read -rsp $'Press any key to continue...\n' -n1 key

exit 0
