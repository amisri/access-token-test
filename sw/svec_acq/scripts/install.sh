#!/bin/bash
#
# Filename: install.sh
#
# Purpose:  Install svec_acq binaries on /user/pclhc
#
# Author:   Marc Magrans de Abril

# Help Message

helpmsg='
    Usage: install.sh [options]

    This script installs the compiled version of the SVEC_ACQ program on /user/pclhc/bin/Linux/x86_64.
    The script also sends a notification email, modifies the fgcd.release file, and commits everyhting to the pclhc directory.

    Options:
    -h,-? This help

'

# Stop the script on the first error

set -e

# CPU and OS version

cpu=$(uname -m)
os=$(uname -s)

# Operational user, host, and path

op_path="/user/pclhc/bin"
op_user="pclhc"
op_host="cs-ccr-teepc2"

parse_args()
{
    # Parse options

    while getopts ":h" opt; do
        case "${opt}" in
            h)
                echo "${helpmsg}"
                exit 2;
                ;;
            \?)
                echo "Error: Invalid option: -${OPTARG}" >&2
                exit 1
                ;;
        esac
    done


    # Shift already parsed positional parameters

    shift $((OPTIND-1))
}

# Draw menu with description and available options given by a parameters

menu()
{
    # Get description of the menu

    local -r PS3="$1"
    shift

    # Print menu, handle the user choice

    select choice in "$@"; do break; done

    # Separator

    echo

    # Check if choice is valid

    if [ -z "${choice}" ]; then
        echo "Error: Invalid choice" >&2
        exit 1
    fi
}

choose_target()
{
    class="$1"

    # Load all possible destinations of the choosen class

    dests_svec_all_indirect="dests_${class}[@]"
    dests_all=( ${!dests_all_indirect} )

    # Remove duplicated destinations

    dests_all=( $( printf '%s\n' "${dests_all[@]}" | sort -u ) )

    # Choose target

    menu "Choose target: " "Development" "PRODUCTION"
    target="${choice}"

}

# Retrieve class number

package="svec_acq"

# recover base directories and read destinations

scripts_path=$(dirname "$0")

build_home=$(dirname "$0")/../../..

# Parse arguments and retireve class number

parse_args "$@"

package_fn="${build_home}/sw/svec_acq/svec_acq/${os}/${cpu}/${package}"

if [ ! -x "${package_fn}" ]; then
    echo "ERROR: ${package_fn} does not exist or is not executable" >&2
    exit 1
fi

# Check that all changes have been committed

if [ ! -z "`git status --porcelain 2>&1`" ]; then
    echo "ERROR: Commit your changes before installing the software" >&2
    exit 1
fi

# Check that all commits have been pushed to the remote repository

git fetch origin

if [ ! -z "`git diff --quiet origin`" ]; then
    echo "ERROR: Repository not up to date" >&2
    exit 1
fi

# Choose the destination target

choose_target

# Deploy new version

if [ "PRODUCTION" = "${target}" ]; then
    package_target=${package}
else
    package_target=${package}_${target}
fi

# Retrieve version

version=`date +%s -r ${package_fn}`

# Perorm installation

scp -p "${package_fn}" ${op_user}@${op_host}:${op_path}/${os}/${cpu}/${package_target}
scp -p "${build_home}/sw/svec_acq/on_demand/${os}/${cpu}/svec_acq_on_demand" ${op_user}@${op_host}:${op_path}/${os}/${cpu}/.
scp -p "${build_home}/sw/svec_acq/scripts/svec_acq_on_demand.sh" ${op_user}@${op_host}:${op_path}/.

# Update history file

${build_home}/sw/utilities/unix/history.sh ${version} "${build_home}/sw/svec_acq/svec_acq/history.txt" ${package}

# Send email

${build_home}/sw/utilities/unix/email.sh ${build_home}/sw/svec_acq/svec_acq/email_template.txt svec_acq ${version} ${target}

# EOF
