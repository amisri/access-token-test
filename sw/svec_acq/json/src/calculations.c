/*
 * fft.c
 *
 *  Created on: Jul 27, 2016
 *      Author: kcarli
 *      Source : https://rosettacode.org/wiki/Fast_Fourier_transform#C
 */


#include "calculations.h"

#include <stdio.h>
#include <math.h>



void _calculationsFft(cplx data[], cplx out[], int n, int step, const double PI)
{
    int i;
    if (step < n)
    {
        _calculationsFft(out, data, n, step * 2, PI);
        _calculationsFft(out + step, data + step, n, step * 2, PI);

        for (i = 0; i < n; i += 2 * step)
        {
            cplx t = cexp(-I * PI * i / n) * out[i + step];
            data[i / 2]     = out[i] + t;
            data[(i + n)/2] = out[i] - t;
        }
    }
}

void calculationsFft(cplx data[], int n, const double PI)
{
    cplx out[n];
    int i;
    for (i = 0; i < n; i++) out[i] = data[i];

    _calculationsFft(data, out, n, 1, PI);
}



