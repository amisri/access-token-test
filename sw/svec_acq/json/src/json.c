/*
 * json.c
 *
 *  Created on: Jun 21, 2016
 *      Author: kcarli
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <math.h>
#include <ctype.h>
#include <libgen.h>

#include "parser.h"
#include "log.h"
#include "json.h"
#include "calculations.h"




void jsonWriteJsons(struct channel_t   *t_channels,
                             size_t              number_of_active_channels,
                             char               *timestamp,
                             char               *raw_files,
                             uint32_t            number_of_samples_per_channel)
{
    int number_of_diff_groups = 0;
    FILE * json_stream;
    char *t_filenames[MAX_CHANNELS];
    int k, eof;
    int i;

    for (k = 0; k < number_of_active_channels; k++)
    {
        i = 0;

        // We first create a different file for each group
        char temp[MAX_CHANNELS + 1] = { 0 };

        snprintf(temp,MAX_CHANNELS, "%s.json",t_channels[k].group);

        if (!(json_stream = fopen64(temp, "a+")))
        {
            logPrintf("ERROR Failed to open Json_stream file: %s\n", strerror(errno));
            exit(1);

        }

        // Then we check If it's the first time we write in it
        // If it is we have to write the following in it

        if ((eof = fgetc(json_stream)) == EOF)
        {
            fseek(json_stream, 0, SEEK_SET);
            fprintf(json_stream,
                    "{ \n  \"source\": \"%s\",\n  \"timeOrigin\":%s.000000,\n  \"firstSampleTime\":%s.000000,\n  \"period\":%f, \n  \"version\":\"1.0\", \n  \"signals\":[\n    {\n",
                    raw_files, timestamp, timestamp, (float)1/DOWNSAMPLED_FREQUENCY);
            t_filenames[number_of_diff_groups] = t_channels[k].group;
            number_of_diff_groups++;
        }

        //Otherwise we have to write something different in it, which is

        else
        {
            fprintf(json_stream, ",\n    {\n");
        }

        fprintf(json_stream, "      \"name\":\"%s\",\n      \"samples\":",
                t_channels[k].name);

        //Writing of the first sample, who require an opening [

        fprintf(json_stream, "[%f,", t_channels[k].signals_data[i]);

        // From the second sample to the max samples -1

        for (i = i+1; i < number_of_samples_per_channel - 1; i++)
        {
            fprintf(json_stream, "%.5f,", t_channels[k].signals_data[i]);
        }

        // The last sample who require a closing ] and no comma

        fprintf(json_stream, "%f]", t_channels[k].signals_data[i]);
        fprintf(json_stream, "\n    }");

        fclose(json_stream);
    }

    // We will open all files to add the finishing string
    char temp_two[MAX_CHANNELS + 1];


    for (k = 0; k < number_of_diff_groups; k++)
    {
        memset(temp_two, 0, sizeof(temp_two));
        snprintf(temp_two,MAX_CHANNELS, "%s.json",t_filenames[k]);

        if (!(json_stream = fopen64(temp_two, "a+")))
        {
            logPrintf("ERROR Failed to open Json_stream file: %s\n", strerror(errno));
            exit(1);

        }
        fprintf(json_stream, "\n  ]\n}\n");
        fclose(json_stream);
    }
}


void jsonReadData(struct channel_t *t_channels, struct config_t *config,
        char *raw_files, size_t number_of_active_channels)
{
    int j = 0;
    int k;
    const short MAX_SHORT_VALUE = 32767;
    FILE * stream;
    short* buffer = NULL;
    buffer = calloc(number_of_active_channels, sizeof(short));

    // We try to open the binary file
    if (!(stream = fopen(raw_files, "r")))
    {
        logPrintf("ERROR Failed to open raw file: %s\n", strerror(errno));
        exit(1);
    }
    memset(t_channels, 0, sizeof(struct channel_t));

    /* We copy the content of our config struct coming from the parser function which contain all the parameters for all channels
     into our t_channles struct which is the same but with
     a data samples value field in addition */

    for (k = 0; k < number_of_active_channels; k++)
    {
        t_channels[k].channel_input = config[k].channel;
        snprintf(t_channels[k].name, SIZE_ALLOWED, "%s",config[k].name);
        snprintf(t_channels[k].group, SIZE_ALLOWED, "%s",config[k].group);
        t_channels[k].gain = config[k].gain;
        t_channels[k].offset = config[k].offset;
    }

    while (fread(buffer, sizeof(uint16_t), number_of_active_channels, stream)
            == number_of_active_channels)
    {

        for (k = 0; k < number_of_active_channels; k++)
        {
            // We stored it into the array of sample corresponding to the good channel.
            // All first samples for each channel, then all second samples, and so on

            t_channels[k].signals_data[j] = (float) ((float) buffer[k]//buffer[config[k].channel] FPGA BUG
                                        / (float) MAX_SHORT_VALUE) * (float) 11
                                        * (float) t_channels[k].gain + (float) t_channels[k].offset;

        }
        j = j + 1;

        //We remove all the first sample, because we don't need it anymore, and we will fetch all second samples
        memset(buffer, 0, number_of_active_channels * sizeof(short));


    }
    free(buffer);
    fclose(stream);
}

void jsonDeleteIfExist(char *status)
{
    char commande[50];
    snprintf(commande, 50, "rm -rf %s", status);
    system(commande);
    mkdir(status, 0777);
}
void jsonCheckCreateAndChangeDir(char *dir_to_check)
{
    struct stat st = { 0 };
    if (stat(dir_to_check, &st) == -1)
       {
           mkdir(dir_to_check, 0777);
       }
    chdir(dir_to_check);
}
void jsonMakeDirDayAndTime(time_t raw_time, char *status)
{
    struct stat st = { 0 };
    char day[SIZE_ALLOWED + 1];
    char hour[MAX_LINE_SIZE + 1];
    char full_path[MAX_LINE_SIZE + 1];

    struct tm * time = localtime(&raw_time);
    strftime(day, 1000, "%Y_%m_%d", time);
    strftime(hour, 1000, "%H_%M_%S", time);

    snprintf(full_path, sizeof(full_path), "%s_%s", hour, status);

    jsonCheckCreateAndChangeDir(day);

    if (stat(full_path, &st) == -1) // We check if we already have a rising edge or a falling edge for that precise time
    {
        mkdir(full_path, 0777);
    }
    else
    {
        jsonDeleteIfExist(full_path);
    }
    chdir(full_path);
}

void jsonMakeDirStructure(time_t raw_time, char *status, char * basepath)
{
    char hostname[SIZE_OF_MESSAGES];

    // We get the name of the host

    gethostname(hostname, SIZE_OF_MESSAGES);

    chdir(JSON_PATH);

    jsonCheckCreateAndChangeDir(hostname);
    snprintf(basepath,SIZE_OF_MESSAGES, "%s%s/",JSON_PATH,hostname );

    // If we have an On Demand Acquisition or an FFT

    if (strcmp(status, "on_demand")== 0 || strcmp(status, "harmonic_analyzes") == 0)
    {
        jsonCheckCreateAndChangeDir(status);
        jsonMakeDirDayAndTime(raw_time, status);
    }

    // If we have a normal acquisition

    else
    {
        jsonMakeDirDayAndTime(raw_time, status);
    }
}
void jsonRmsForEachHarmonic(int cpt, int index, int *num_of_calculated_signals, int n, int id_suffix_name)
{
    char temp[SIZE_ALLOWED + 1] = { 0 };

    snprintf(temp, SIZE_ALLOWED, "%s_RMS_%d", t_rms[cpt].name_signal, id_suffix_name);
    snprintf(t_calculated_signals[*num_of_calculated_signals].name, 15, "%s",temp);
    snprintf(t_calculated_signals[*num_of_calculated_signals].group, 15, "%s",temp);
    t_calculated_signals[*num_of_calculated_signals].signals_data[index] = t_rms[cpt].rms_data[n];
    (*num_of_calculated_signals)++;
}
int32_t jsonHarmonicAnalysesForSignals(struct channel_t *struct_in, int cpt, int step,  int *num_of_calculated_signals, int index, int x)
{
    /*
     * For cpt = 0 --> I_R
     * For cpt = 1 --> U_R
     * For cpt = 2 --> I_S
     * For cpt = 3 --> U_S
     * For cpt = 4 --> I_T
     * For cpt = 5 --> U_T
     */


    t_magnitude[cpt].name_signal = struct_in->name;
    t_phase[cpt].name_signal = struct_in->name;
    t_rms[cpt].name_signal = struct_in->name;




    int n,k,h;
    float real;
    float ima;
    float sum_rms = 0.0;
    int num_of_pq = 0;

    // For every harmonic

    for(n = 1; n <= 25; n++)
    {
        real = 0.0;
        ima  = 0.0;

    // For 500 samples of Data, we calculate the Fourier Coefficients

      for(k = 0; k < WINDOWS_SIZE; k++)
      {
         real += struct_in->signals_data[k + step] * cosf(2 * M_PI * (2*n) *((float)k/WINDOWS_SIZE)) * 0.00004 * (2 / 0.02);
         ima  += struct_in->signals_data[k + step] * sinf(2 * M_PI * (2*n) *((float)k/WINDOWS_SIZE)) * 0.00004 * (2 / 0.02);
      }

    // Calculation of the Magnitude / Phase / and RMS value

      t_magnitude[cpt].magnitude_data[n] = (float) sqrt(real * real + ima * ima);
      t_magnitude[cpt].sum += (float) sqrt(real * real + ima * ima);
      t_phase[cpt].phase_data[n] = (float) atan2(ima,real);
      t_phase[cpt].sum += (float) atan2(ima,real);
      t_rms[cpt].rms_data[n] = t_magnitude[cpt].magnitude_data[n] / sqrt(2);

      // We will generate a signal for each prime number up to the 13th harmonic, for the RMS

      switch(n)
      {

            case 1:
                jsonRmsForEachHarmonic(cpt, index, num_of_calculated_signals, n, 1);
                break;

            case 2:
                jsonRmsForEachHarmonic(cpt, index, num_of_calculated_signals, n, 2);
                break;

            case 3:
                jsonRmsForEachHarmonic(cpt, index, num_of_calculated_signals, n, 3);
                break;

            case 5:
                jsonRmsForEachHarmonic(cpt, index, num_of_calculated_signals, n, 5);
                break;

            case 7:
                jsonRmsForEachHarmonic(cpt, index, num_of_calculated_signals, n, 7);
                break;

            case 11:
                jsonRmsForEachHarmonic(cpt, index, num_of_calculated_signals, n, 11);
                break;

            case 13:
                jsonRmsForEachHarmonic(cpt, index, num_of_calculated_signals, n, 13);
                break;
      }
    }

    // We calculate the sum of the RMS value for the THD calculations afterwards

    for(h = 2; h <= 25; h++)
    {
        sum_rms += t_rms[cpt].rms_data[h] * t_rms[cpt].rms_data[h];
    }

    t_rms[cpt].sum = t_magnitude[cpt].sum / sqrt(2);

    // If we have both U and I for each R,S and T

    if((cpt % 2) != 0)
    {
        switch(cpt)
        {
            case 1 :
                t_p[num_of_pq].name_signal = "P_R";
                t_q[num_of_pq].name_signal = "Q_R";
                t_thd[num_of_pq].name_signal = "THD_R";
                break;
            case 3 :
                t_p[num_of_pq].name_signal = "P_S";
                t_q[num_of_pq].name_signal = "Q_S";
                t_thd[num_of_pq].name_signal = "THD_S";
                break;
            case 5 :
                t_p[num_of_pq].name_signal = "P_T";
                t_q[num_of_pq].name_signal = "Q_T";
                t_thd[num_of_pq].name_signal = "THD_T";
                break;
        }

        // We calculate the THD / P / and Q

        t_thd[num_of_pq].thd_data = sqrt(sum_rms) / t_rms[cpt].rms_data[1];
        t_p[num_of_pq].p_data =  t_rms[cpt].rms_data[1] *  t_rms[cpt-1].rms_data[1] * cos(t_phase[cpt].phase_data[1] - t_phase[cpt -1].phase_data[1] - M_PI/6);
        t_q[num_of_pq].q_data =  t_rms[cpt].rms_data[1] *  t_rms[cpt-1].rms_data[1] * sin(t_phase[cpt].phase_data[1] - t_phase[cpt -1].phase_data[1] - M_PI/6);


        // We store this signals in our structure of calculated signals, at the right position
        const int BASE = 17;
        const int SHIFT_P = 14;
        const int SHIFT_Q = 15;
        const int SHIFT_THD = 16;

        snprintf(t_calculated_signals[SHIFT_P + BASE * x].name, SIZE_ALLOWED, "%s",t_p[num_of_pq].name_signal);
        snprintf(t_calculated_signals[SHIFT_P + BASE * x].group, SIZE_ALLOWED, "%s",t_p[num_of_pq].name_signal);
        t_calculated_signals[SHIFT_P + BASE * x].signals_data[index]    = t_p[num_of_pq].p_data;
        (*num_of_calculated_signals)++;


        snprintf(t_calculated_signals[SHIFT_Q + BASE * x].name, SIZE_ALLOWED, "%s",t_q[num_of_pq].name_signal);
        snprintf(t_calculated_signals[SHIFT_Q + BASE * x].group, SIZE_ALLOWED, "%s",t_q[num_of_pq].name_signal);
        t_calculated_signals[SHIFT_Q + BASE * x].signals_data[index]    = t_q[num_of_pq].q_data;
        (*num_of_calculated_signals)++;


        snprintf(t_calculated_signals[SHIFT_THD + BASE * x].name, SIZE_ALLOWED, "%s",t_thd[num_of_pq].name_signal);
        snprintf(t_calculated_signals[SHIFT_THD + BASE * x].group, SIZE_ALLOWED, "%s",t_thd[num_of_pq].name_signal);
        t_calculated_signals[SHIFT_THD + BASE * x].signals_data[index]    = t_thd[num_of_pq].thd_data;
        (*num_of_calculated_signals)++;

        num_of_pq++;

    }

    return 0;
}

int jsonRawToJson(char *path_for_rawfiles)
{
    char hostname[SIZE_OF_MESSAGES];
    char file_path[SIZE_OF_MESSAGES];
    char *filename = basename(path_for_rawfiles);

    gethostname(hostname,SIZE_OF_MESSAGES);
    snprintf(file_path,SIZE_OF_MESSAGES,"%s%s.conf",CONFIG_PATH,hostname);
    time_t raw_time = 0;

    uint32_t number_of_samples_per_channel = (TIMES_BT_A + TIMES_AT_A)* DOWNSAMPLED_FREQUENCY;

    struct config_t config[MAX_CHANNELS] = { {0} };
    size_t number_of_active_channels;

    parserParse(file_path, config, &number_of_active_channels);
  //  number_of_active_channels = 64; // Thanks FPGA bug

    char timestamp[SIZE_ALLOWED + 1];
    char status[SIZE_ALLOWED + 1];

    char temp_path[SIZE_OF_MESSAGES];
    snprintf(temp_path, SIZE_OF_MESSAGES, "%s%s/", JSON_ARCH, hostname);

    sscanf(filename, "%*[^_]_%10s_%3s", timestamp, status);
    raw_time = atoi(timestamp);

    if (strcmp(status, "DEM") == 0)
    {
        snprintf(status, SIZE_ALLOWED, "on_demand");
    }
    char basepath[SIZE_OF_MESSAGES] = { 0 };

    jsonMakeDirStructure(raw_time, status, basepath);

    jsonReadData(t_channels, config, path_for_rawfiles, number_of_active_channels);

    jsonWriteJsons(t_channels, number_of_active_channels, timestamp, path_for_rawfiles, number_of_samples_per_channel);

    chdir(basepath);


    logPrintf("INFO Calculations In Process \n");

    int all,spe;
    int cpt = 0;
    int i = 0;

    // Hardcode the names of the signals to be calculated

    char *special_groups[] = {"I_R","U_R","I_S","U_S","I_T","U_T"};

    // Hardcode the number of signals to be calculated

    uint32_t number_of_signals_TBC = 6;

    memset(t_calculated_signals,0,sizeof(t_calculated_signals));
    memset(t_channels_special,0,sizeof(t_channels_special));

    //Initialisation Phase

    for (all = 0; all < number_of_active_channels; all++) // We compare all active channels name
    {
        for (spe = 0; spe < number_of_signals_TBC; spe++) // With all of our specials channels (I_R I_S I_T U_R ..)
        {
            if (strcmp(t_channels[all].name, special_groups[spe]) == 0) //If we found one of them
            {
                t_channels_special[spe].channel_input = t_channels[all].channel_input;
                t_channels_special[spe].gain = t_channels[all].gain;
                snprintf(t_channels_special[spe].name, SIZE_ALLOWED, "%s",t_channels[all].name);
                snprintf(t_channels_special[spe].group, SIZE_ALLOWED, "%s",t_channels[all].group);
                t_channels_special[spe].offset = t_channels[all].offset;
                memcpy(t_channels_special[spe].signals_data, t_channels[all].signals_data, sizeof(t_channels[all].signals_data));
                cpt++;
            }

        }
    }
    // Our t_channels_special look like this {"I_R","U_R","I_S","U_S","I_T","U_T"} if all signals are present

    // Processing Phase

    int step;
    int index = 0;
    int zero = 0;
    int x = 0;
    int *num_of_calculated_signals = &zero;

    if(cpt == 6)
    {
        for (step = 0; step < number_of_samples_per_channel; step += (WINDOWS_SIZE / 2))
        {
            (*num_of_calculated_signals) = 0;
            x = 0;
            for(i=0; i<cpt; i++)
            {
                jsonHarmonicAnalysesForSignals(&t_channels_special[i], i, step, num_of_calculated_signals, index, x);
                if ((i % 2) != 0)
                {
                    x++;
                }
            }
            index++;
        }
    }
    else
    {
        int x;
        for (x=0; x<number_of_signals_TBC; x++)
        {
            if(t_channels_special[x].name[0] == '\0' )
            {
                logPrintf("WARNING Some specials channels for the calculations are missing, check for this one : %s \n", special_groups[x]);
                break;
            }
        }

        return(1);
    }

    // Producing the Output Files Phase
    memset(basepath, 0, SIZE_OF_MESSAGES* sizeof(char));
    jsonMakeDirStructure(raw_time, "harmonic_analyzes", basepath);
    const size_t number_of_output_signals = 51;
    const uint32_t number_samples_for_processed_signals = 148;

    jsonWriteJsons(t_calculated_signals, number_of_output_signals, timestamp, path_for_rawfiles, number_samples_for_processed_signals);
    chdir(basepath);

    return (0);
}
