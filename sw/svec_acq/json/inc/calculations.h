/*
 * fft.h
 *
 *  Created on: Jul 27, 2016
 *      Author: kcarli
 */

#ifndef SW_SVEC_ACQ_UNIX_JSON_INC_CALCULATIONS_H_
#define SW_SVEC_ACQ_UNIX_JSON_INC_CALCULATIONS_H_

#include <complex.h>
#include <math.h>


typedef double complex cplx;


void _calculationsFft(cplx data[], cplx out[], int n, int step, const double PI);

void calculationsFft(cplx data[], int n, const double PI);



#endif /* SW_SVEC_ACQ_UNIX_JSON_INC_CALCULATIONS_H_ */
