/*
 * json.h
 *
 *  Created on: Jun 21, 2016
 *      Author: kcarli
 */

#ifndef SW_SVEC_ACQ_UNIX_JSON_INC_JSON_H_
#define SW_SVEC_ACQ_UNIX_JSON_INC_JSON_H_

#include "const.h"
#include "parser.h"

#define WINDOWS_SIZE            500

struct channel_t
{
    uint32_t channel_input;
    char name[20];
    char group[20];
    double gain;
    double offset;
    float signals_data[MAX_NUM_SAMPLES];
};

struct magnitude_t
{
    float magnitude_data[WINDOWS_SIZE];
    char *name_signal;
    float sum;
};
struct phase_t
{
    float phase_data[WINDOWS_SIZE];
    char *name_signal;
    float sum;
};
struct thd_t
{
    float thd_data;
    char *name_signal;
};
struct rms_t
{
    float rms_data[WINDOWS_SIZE];
    char *name_signal;
    float sum;
};
struct p_t
{
    float p_data;
    char *name_signal;
};
struct q_t
{
    float q_data;
    char *name_signal;
};

struct magnitude_t t_magnitude[6];
struct phase_t t_phase[6];
struct rms_t t_rms[42];
struct thd_t t_thd[3];
struct p_t t_p[3];
struct q_t t_q[3];

struct channel_t t_channels[MAX_CHANNELS];
struct channel_t t_channels_special[6];
struct channel_t t_calculated_signals[MAX_CHANNELS]; // 17 *3 necessary = 51


int jsonRawToJson(char *path_for_rawfiles);

void jsonWriteJsons(struct channel_t *t_channels,
        size_t number_of_active_channels, char *timestamp, char *raw_files,  uint32_t number_of_samples_per_channel);

void jsonReadData(struct channel_t *t_channels, struct config_t *config,
        char *raw_files, size_t number_of_active_channels);

void jsonChangeDirBackwards(char *status);

void jsonMakeDirStructure(time_t raw_time, char *status, char * basepath);

#endif /* SW_SVEC_ACQ_UNIX_JSON_INC_JSON_H_ */
