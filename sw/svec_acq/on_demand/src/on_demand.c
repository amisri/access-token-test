/*
 * svec_trigger.c
 *
 *  Created on: Jul 4, 2016
 *      Author: kcarli
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <sys/time.h>
#include <arpa/inet.h>




#include "svec_vme.h"
#include "log.h"

struct timeval tv, tvlast;


int main(int argc, char ** argv)
{
    struct vme_mapping map;

    svecInitVme(&map);

    svecVmeMemWrite((&svec_regs->buffer_a.trigger_enable), 0x00000003); //SW Trigger
    svecVmeMemWrite((&svec_regs->buffer_a.test), 666);

    logPrintf("INFO Trigger On Demand DONE\n");
    svecExit(&map);
    return (0);
}
