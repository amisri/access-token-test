/*
 * main.c
 *
 *  Created on: Jun 14, 2016
 *      Author: kcarli
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/stat.h>

#define GLOBALS

#include "svec_vme.h"
#include "parser.h"
#include "log.h"
#include "const.h"

struct vme_mapping map;

int svecEndAcquisition(char *hostname)
{
    char cwd[1024];
    getcwd(cwd, sizeof(cwd));
    const char * const COMMAND = "touch ../.last_acquisition_time";
    if(system(COMMAND) != 0)
    {
        logPrintf("INFO Try to touch the .last_acquisition_time here : %s\n", cwd);
        logPrintf("ERROR Unable to touch the file : ./.last_acquisition_time\n");
    }
    return(0);
}

void intHandler(int dummy)
{
    logPrintf("INFO The service svec_acq has been stopped, please restart it for further use\n");
    svecExit(&map);
    exit(0);
}

int main(int argc, char ** argv)
{
    logPrintf("INFO The service svec_acq is now running\n");
    struct stat st = { 0 };
    if (stat(RAW_PATH, &st) == -1)
    {
        mkdir(RAW_PATH, 0777);
    }

    struct address_event_t
    {
        unsigned  int address_event_A;
        unsigned  int address_curr_A;
        unsigned  int address_event_B;
        unsigned  int address_curr_B;
    };
    struct address_event_t events;

    char hostname[SIZE_OF_MESSAGES];
    char file_path[SIZE_OF_MESSAGES];

    uint32_t times_a;
    uint32_t times_b;
    unsigned int addr_to_read_a = 0;
    unsigned int addr_to_read_b = 0;
    unsigned int addr_to_read_a_on_demand = 0;
    uint64_t mask;
    uint32_t channels_low;
    uint32_t channels_high;

    gethostname(hostname,SIZE_OF_MESSAGES);
    snprintf(file_path,SIZE_OF_MESSAGES,"%s%s.conf",CONFIG_PATH,hostname);
    // Start the parsing of the file

    struct config_t config[MAX_CHANNELS] = { {0}};
    size_t number_of_active_channels;

    parserParse(file_path, config, &number_of_active_channels);

    // Should be removed when the FPGA bug will be resolved
   // number_of_active_channels = 64;

    // Let's get some usefull information, like which channel is activated

    mask = parserGetAllActiveChannels();
    channels_high = (mask >> 32);
    channels_low = (uint32_t) mask;

    // number of samples after trigger (We have to add number_of_active_channels*number_of_active_channels samples because we discard the first same amount, because the 64 samples are wrong due to HW issue, and we want to be aligned )

    trigger_sample.number_of_samples_at_a = DOWNSAMPLED_FREQUENCY * TIMES_AT_A * number_of_active_channels + (number_of_active_channels * number_of_active_channels);
    trigger_sample.number_of_samples_at_b = DOWNSAMPLED_FREQUENCY * TIMES_AT_B * number_of_active_channels + (number_of_active_channels * number_of_active_channels);


    // number of samples before trigger

    trigger_sample.number_of_samples_bt_a = DOWNSAMPLED_FREQUENCY * TIMES_BT_A * number_of_active_channels;
    trigger_sample.number_of_samples_bt_b = DOWNSAMPLED_FREQUENCY * TIMES_BT_B * number_of_active_channels;

    if(svecInitVme(&map) == 1)
    {
        logPrintf("ERROR an error has occured during the Mapping phase in svecInitVme function \n");
        exit(1);
    }

    svecVmeMemWrite(&svec_regs->buffer_b.test, 0);

    times_a = trigger_sample.number_of_samples_bt_a + trigger_sample.number_of_samples_at_a ;
    times_b = trigger_sample.number_of_samples_bt_b + trigger_sample.number_of_samples_at_b ;

    // Redirecting the CTRL+C event and the kill

    signal(SIGINT,  intHandler);
    signal(SIGKILL, intHandler);

    // Saving the address of the location of both buffer, to further know whether we are dealing with buffer A or buffer B

    snprintf(address_buffer_a, sizeof(address_buffer_a),"%p",&svec_regs->buffer_a );
    snprintf(address_buffer_b, sizeof(address_buffer_b),"%p",&svec_regs->buffer_b );

    // Software reset for both buffer, to be sure everything is set correctly

    svecResetRegistersA();
    svecResetRegistersB();

    //Initialization of both buffer
    uint32_t  LENGHT_OF_BUFF = (BASE_FOR_TOP / number_of_active_channels) * number_of_active_channels; // Must be aligned with total active channels

    svecSetupBufferA(channels_low, channels_high, LENGHT_OF_BUFF);
    svecSetupBufferB(channels_low, channels_high, LENGHT_OF_BUFF);


    while(1)
    {

        if (svecVmeMemRead(&svec_regs->buffer_a.trigger_status) == 0x00000002)
        {
            logPrintf("INFO Rising edge caught : In Process\n");

            // Saving the time of the event

            time_first_sample_a = (unsigned long) time(NULL);

            // Where were we when the trigger occurred ?

            events.address_event_A = svecVmeMemRead(&svec_regs->buffer_a.buffer_event);
            events.address_curr_A = svecVmeMemRead(&svec_regs->buffer_a.buffer_curr);

            events.address_event_A = events.address_event_A / number_of_active_channels * number_of_active_channels;

            // Let's go backwards to record from where we want
            addr_to_read_a = (uint32_t) (((int32_t) events.address_event_A + 4
                    - (int32_t) (trigger_sample.number_of_samples_bt_a * 4))
                    % LENGHT_OF_BUFF);

            if(svecReadingWithBuffSpyBlock(times_a, addr_to_read_a, &svec_regs->buffer_a, number_of_active_channels, LENGHT_OF_BUFF) == 1)
            {
                logPrintf("ERROR An error occured during rising edge \n");
                exit(1);
            }

            //We have to reset and reinitialize back again, to make the trigger_status register back to 0, only way because it is set in Read Only

            svecResetRegistersA();
            svecSetupBufferA(channels_low, channels_high, LENGHT_OF_BUFF);
            logPrintf("INFO Rising edge caught : Done\n");
            svecEndAcquisition(hostname);
        }

        if (svecVmeMemRead(&svec_regs->buffer_b.trigger_status) == 0x00000002)
        {
            logPrintf("INFO Falling edge caught : In Process\n");

            // Saving the time of the event

            time_first_sample_b = (unsigned long) time(NULL);

            // Where were we when the trigger occurred ?

            events.address_event_B = svecVmeMemRead(&svec_regs->buffer_b.buffer_event);
            events.address_curr_B = svecVmeMemRead(&svec_regs->buffer_b.buffer_curr);

            // Where are we reading from ? int32_t for the modulo, otherwise it is incorrect.
            events.address_event_B = events.address_event_B / number_of_active_channels * number_of_active_channels;

            addr_to_read_b = (uint32_t) (((int32_t) events.address_event_B + 4
                    - (int32_t) (trigger_sample.number_of_samples_bt_b * 4))
                    % LENGHT_OF_BUFF);


            // These functions will read by group of number_of_active_channels until it as read more then times_a samples
            // Example if we want 60 samples and we read by group of 8 we will have 64 samples eventually

            if(svecReadingWithBuffSpyBlock(times_b, addr_to_read_b,&svec_regs->buffer_b, number_of_active_channels, LENGHT_OF_BUFF) == 1)
            {
                logPrintf("Error An error Occured during falling edge \n");
                exit(1);
            }

            // We have to reset and reinitialize back again, to make the trigger_status register back to 0, only way because it is set in Read Only

            svecResetRegistersB();
            svecSetupBufferB(channels_low, channels_high, LENGHT_OF_BUFF);
            logPrintf("INFO Falling edge caught : Done\n");
            svecEndAcquisition(hostname);
        }

        if (svecVmeMemRead(&svec_regs->buffer_a.trigger_status) == 0x00000001)
        {
            // On Demand

            // We need an extra features to capture only one time, the event. (Maybe rising and falling edge activated)
            if(svecVmeMemRead(&svec_regs->buffer_a.test) == 666)
            {
                is_on_demand = 1;
                time_first_sample_on_demand = (unsigned long) time(NULL);
                logPrintf("INFO On demand trigger caught : In Process\n");

                events.address_event_A = svecVmeMemRead(&svec_regs->buffer_a.buffer_event);
                events.address_curr_A = svecVmeMemRead(&svec_regs->buffer_a.buffer_curr);

                addr_to_read_a_on_demand =
                        (uint32_t) (((int32_t) events.address_event_A + 4
                                - (int32_t) (trigger_sample.number_of_samples_bt_a
                                        * 4)) % LENGHT_OF_BUFF);

                uint32_t times_on_demand = times_a;

                logPrintf("Before : %08x,\n",addr_to_read_a_on_demand);
                addr_to_read_a_on_demand &= ~255;
                logPrintf("After : %08x,\n",addr_to_read_a_on_demand);

                if(svecReadingWithBuffSpyBlock(times_on_demand, addr_to_read_a_on_demand, &svec_regs->buffer_a, number_of_active_channels, LENGHT_OF_BUFF) == 1)
                {
                    logPrintf("ERROR an error occured during on_demand \n");
                    exit(1);
                }
                logPrintf("INFO On demand trigger : Done\n");
                svecEndAcquisition(hostname);

            }
            svecVmeMemWrite(&svec_regs->buffer_a.test, 0);
            svecResetRegistersA();
            svecSetupBufferA(channels_low, channels_high, LENGHT_OF_BUFF);
            is_on_demand = 0;
        }

        sleep(1);
    }



}

