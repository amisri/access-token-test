// Includes
#include "svec_vme.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/stat.h>


#include "log.h"
#include "json.h"



// Define where we will start reading, how many samples we want to acquire for each reading
// (How many samples will be fetch by the FPGA to be placed on the FIFO) and how many samples in total we want.

int svecReadingWithBuffSpyBlock(int times, unsigned int addr_to_read,
        struct svec_buffer_t * buffer_offset, int nb_sample, uint32_t  LENGHT_OF_BUFF)
{

    int nb_sample_read = 0;
    int j = 0;
    uint32_t value;
    int modulo;

    svecVmeMemWrite((&buffer_offset->buffer_spy_len), nb_sample - 1); // Number of sample -1 we want to acquire MAX 63

    value = svecVmeMemRead(&buffer_offset->buffer_spy_state);

    if ((value >> 25) & 0x1)
    {
        logPrintf("ERROR DDR ADC Write Port Error\n");
        return(1);
    }

    // While we can read
    int first_time = 0;
    while (nb_sample_read + nb_sample  <= times)
    {
        // If we don't reach the top of the Buffer

        if((uint32_t)((int32_t)addr_to_read + (int32_t)(4 * nb_sample * (j+1))) <= LENGHT_OF_BUFF )
        {
            svecVmeMemWrite((&buffer_offset->buffer_spy_len), nb_sample - 1); // Number of sample -1 we want to acquire MAX 63
            svecVmeMemWrite((&buffer_offset->buffer_spy_sa), (uint32_t)(((int32_t)addr_to_read + (int32_t)(4 * nb_sample * j)) % LENGHT_OF_BUFF));
            if ((value >> 9) & 0x1)
               {
                    logPrintf("ERROR DDR VME Read Port Error\n");
                    return(1);
               }

            // We call the write to Binary files function, with 64 as nb_sample and from which Buffer

            if (svecWrittingToFileBlock(nb_sample, buffer_offset, &first_time) == 1)
            {
                return (1);
            }
            nb_sample_read += nb_sample;
        }

        // If we will reach the top of the Buffer
        else
        {
            // We request the FPGA that we want (LENGHT_OF_BUFF - address where we have to read) samples

            svecVmeMemWrite((&buffer_offset->buffer_spy_len), LENGHT_OF_BUFF -(uint32_t)((int32_t)addr_to_read + (int32_t)(4 * nb_sample * j))  - 1);
            svecVmeMemWrite((&buffer_offset->buffer_spy_sa), (uint32_t)(((int32_t)addr_to_read + (int32_t)(4 * nb_sample * j)) % LENGHT_OF_BUFF));
            if ((value >> 9) & 0x1)
               {
                    logPrintf("ERROR DDR VME Read Port Error\n");
                    return(1);
               }

            // We call the write to Binary files function, with (LENGHT_OF_BUFF - address where we have to read) as nb_sample and from which Buffer

            if (svecWrittingToFileBlock(LENGHT_OF_BUFF -(uint32_t)((int32_t)addr_to_read + (int32_t)(4 * nb_sample * j)), buffer_offset,  &first_time) == 1)
            {
                return (1);
            }

            // We have reached the top of the Buffer so we continue from the bottom

            addr_to_read = START_ADD_OF_BUFF;
            nb_sample_read += LENGHT_OF_BUFF -(uint32_t)((int32_t)addr_to_read + (int32_t)(4 * nb_sample * j));
        }


        j++;
    }

    // End of While loop
    // If our total of sample is not a multiple of 64

    svecVmeMemWrite((&buffer_offset->buffer_spy_sa),(uint32_t) (((int32_t) addr_to_read + (int32_t) (4 * nb_sample * j)) % LENGHT_OF_BUFF));
    if ((value >> 9) & 0x1)
    {
        logPrintf("ERROR DDR VME Read Port Error\n");
        return(1);
    }

    // We read the extra samples needed

    modulo = times % nb_sample_read;
    if(modulo != 0)
    {
        svecVmeMemWrite((&buffer_offset->buffer_spy_len), modulo - 1);

        // We call the write to Binary files function, with modulo (The remaining extra samples) as nb_sample and from which Buffer

        if (svecWrittingToFileBlock(modulo, buffer_offset,  &first_time) == 1)
        {
            return (1);
        }
    }

    char hostname[SIZE_OF_MESSAGES + 1];
    char path_to_store_rawfiles[SIZE_OF_MESSAGES + 1];
    gethostname(hostname, SIZE_OF_MESSAGES);
    snprintf(path_to_store_rawfiles, SIZE_OF_MESSAGES, "%s%s/%s", RAW_PATH,hostname,DUMP_TO_FILE_RAW);

   jsonRawToJson(path_to_store_rawfiles);

    // Reading finished, Start acquisition again

    svecVmeMemWrite((&buffer_offset->status), 0x00000001);


    return(0);

}

// Perform a DMA access to address_to_read, read out nb_sample * 4 Bytes and store it to buf

int svecDmaAccesBlock(int nb_sample, int address_to_read, uint32_t *buf)
{
    struct vme_dma dmadesc;

    memset(&dmadesc, 0, sizeof(dmadesc));

    // Setting up the DMA Access

    dmadesc.dir = VME_DMA_FROM_DEVICE;
    dmadesc.length = nb_sample * 4; // Bytes
    dmadesc.src.data_width = VME_D32;
    dmadesc.src.am = VME_A32_USER_DATA_SCT;
    dmadesc.novmeinc = 1;
    dmadesc.src.addru = 0x0;
    dmadesc.src.addrl = address_to_read;
    dmadesc.dst.addru = (uint64_t) buf >> 32;
    dmadesc.dst.addrl = (uint64_t) buf & (((uint64_t) 1 << 32) - 1);

    if (vme_dma_read(&dmadesc) != 0)
    {
        logPrintf("ERROR DMA Acces Failed\n");
        return (1);
    }
    else
    {
        return 0;
    }
}

// Read the FIFO of the corresponding buffer, and write the data into a binary file

int svecWrittingToFileBlock(int nb_sample, struct svec_buffer_t * buffer_offset, int *first_time)
{
    struct stat st = { 0 };
    char hostname[SIZE_OF_MESSAGES +1];
    char path_to_store_rawfiles[SIZE_OF_MESSAGES +1];
    // We get the name of the host
    gethostname(hostname, SIZE_OF_MESSAGES);
    snprintf(path_to_store_rawfiles, SIZE_OF_MESSAGES, "%s%s/", RAW_PATH,hostname );

    if (stat(path_to_store_rawfiles, &st) == -1)
    {
        mkdir(path_to_store_rawfiles, 0777);
    }
    chdir(path_to_store_rawfiles);

    char tmp[1024] = { 0 };
    int j;
    char address_buffer[SIZE_ALLOWED + 1];
    uint32_t address_to_read = 0;

    //We need to know where our buffer_offset point

    snprintf(address_buffer, sizeof(address_buffer),"%p",buffer_offset );

    // We create the template for the filename regarding if it is a rising/falling edge or an on_demand


    if((strcmp(address_buffer, address_buffer_a) == 0) && !is_on_demand)
    {
        snprintf(tmp, sizeof(tmp),"rawfile_%lu_ON.raw",time_first_sample_a );
        address_to_read = 0x10000500; // Start address of FIFO Buffer A
    }
    else if((strcmp(address_buffer, address_buffer_b) == 0) && !is_on_demand)
    {
        snprintf(tmp, sizeof(tmp),"rawfile_%lu_OFF.raw",time_first_sample_b );
        address_to_read = 0x10000700; // Start address of FIFO Buffer B
    }
    else if(is_on_demand)
    {
        snprintf(tmp, sizeof(tmp),"rawfile_%lu_DEM.raw",time_first_sample_on_demand );
        address_to_read = 0x10000500;
    }

    snprintf(DUMP_TO_FILE_RAW, SIZE_OF_MESSAGES -1, "%s", tmp);

    FILE *raw;

    if (!(raw = fopen(DUMP_TO_FILE_RAW, "ab")))
     {
         logPrintf("Failed to open raw file: %s %s\n", DUMP_TO_FILE_RAW, strerror(errno));
         return(1);
     }


    uint32_t buf[100] = { 0 };


    int dma_access_error = 0;

    // First time we access via DMA we reject 64 samples because we have a problem with the first time we access the FIFO, 99% sure HW related

    if(*first_time < nb_sample)
    {
        dma_access_error = svecDmaAccesBlock(nb_sample, address_to_read, buf);
        (*first_time)++;
        if(dma_access_error)
        {
            fclose(raw);
            return 1;
        }
    }

    // Then we access and store the data normally

    else
    {
        dma_access_error = svecDmaAccesBlock(nb_sample, address_to_read, buf);
        if (dma_access_error)
        {
            fclose(raw);
            return 1;
        }
        uint32_t temp = 0;
        for (j = 0; j < nb_sample; j++)
        {
            temp = (uint16_t) ntohl(buf[j]);
            fwrite(&temp, sizeof(uint16_t), 1, raw);
        }

    }

     fclose(raw);
     chdir("..");

     return 0;
}


// Read data located at address address

unsigned int svecVmeMemRead(volatile void * address)
{
    return ntohl(*(uint32_t *) address);
}


// Write data value at address address

void svecVmeMemWrite(volatile void * address, uint32_t value)
{
    *(uint32_t *) address = htonl(value);
}


void svecResetRegistersA()
{
    //reset SW buffer one

    svecVmeMemWrite(&svec_regs->buffer_a.status, 0x00000004);

    //back to low MANDATORY

    svecVmeMemWrite(&svec_regs->buffer_a.status, 0x00000000);
}

void svecResetRegistersB()
{
    //reset SW buffer two

    svecVmeMemWrite(&svec_regs->buffer_b.status, 0x00000004);

    //back to low MANDATORY

    svecVmeMemWrite(&svec_regs->buffer_b.status, 0x00000000);
}


// Setup the buffer A or first buffer, which start at offset + 0x400

void svecSetupBufferA(uint32_t channels_low, uint32_t channels_high, uint32_t LENGHT_OF_BUFF)
{
  // Real one, when the firmware bug will be corrected
    svecVmeMemWrite((&svec_regs->buffer_a.chan_31_00), channels_low); // Activate the first x channels

    svecVmeMemWrite((&svec_regs->buffer_a.chan_63_32), channels_high); // Activate the last x  channels

    // Fake one
   // svecVmeMemWrite((&svec_regs->buffer_a.chan_31_00), 0xffffffff); // Activate the first x channels

    //svecVmeMemWrite((&svec_regs->buffer_a.chan_63_32), 0xffffffff); // Activate the last x  channels

    svecVmeMemWrite((&svec_regs->buffer_a.trigger_enable),0); // Disable all trigger

    svecVmeMemWrite((&svec_regs->buffer_a.trigger_rising_edge),0x00000002); //Set acquisition on rising edge

    svecVmeMemWrite((&svec_regs->buffer_a.trigger_falling_edge),0x00000000); // Set acquisition on falling edge

    svecVmeMemWrite((&svec_regs->buffer_a.buffer_sa), START_ADD_OF_BUFF); // Buffer start address

    svecVmeMemWrite((&svec_regs->buffer_a.buffer_size), LENGHT_OF_BUFF); // Define the size of the buffer.

    svecVmeMemWrite((&svec_regs->buffer_a.buffer_level), trigger_sample.number_of_samples_at_a); // number of sample we want to acquire after trigger event

    svecVmeMemWrite((&svec_regs->buffer_a.down_sample),10); // down sample by a factor of 10 = 12,5ks/s

    svecVmeMemWrite((&svec_regs->buffer_a.status), 0x00000001); //start acquisition

    svecVmeMemWrite((&svec_regs->buffer_a.trigger_enable),0x00000002); //Enable all trigger, in our case only HW trigger 1 is available so should be 0x00000002
}

// Setup the buffer B or second buffer, which start at offset + 0x600

void svecSetupBufferB(uint32_t channels_low, uint32_t channels_high, uint32_t LENGHT_OF_BUFF)
{

    // real one
    svecVmeMemWrite((&svec_regs->buffer_b.chan_31_00),channels_low); // Activate the first x channels

    svecVmeMemWrite((&svec_regs->buffer_b.chan_63_32),channels_high); // Activate the last x  channels

    //Fake one
   // svecVmeMemWrite((&svec_regs->buffer_b.chan_31_00),0xffffffff); // Activate the first x channels

    //svecVmeMemWrite((&svec_regs->buffer_b.chan_63_32),0xffffffff); // Activate the last x  channels


    svecVmeMemWrite((&svec_regs->buffer_b.trigger_enable),0); // Disable all trigger

    svecVmeMemWrite((&svec_regs->buffer_b.trigger_rising_edge),0); //Set acquisition on rising edge

    svecVmeMemWrite((&svec_regs->buffer_b.trigger_falling_edge),0x00000002); // Set acquisition on falling edge

    svecVmeMemWrite((&svec_regs->buffer_b.buffer_sa), START_ADD_OF_BUFF); // Buffer start address

    svecVmeMemWrite((&svec_regs->buffer_b.buffer_size), LENGHT_OF_BUFF); // Define the size of the buffer.

    svecVmeMemWrite((&svec_regs->buffer_b.buffer_level), trigger_sample.number_of_samples_at_b); // Number of sample we want to acquire after trigger event

    svecVmeMemWrite((&svec_regs->buffer_b.down_sample),10); // Down sample by a factor of 10 = 12,5ks/s

    svecVmeMemWrite((&svec_regs->buffer_b.status),0x00000001); //start acquisition

    svecVmeMemWrite((&svec_regs->buffer_b.status),0x00000001); //start acquisition

    svecVmeMemWrite((&svec_regs->buffer_b.trigger_enable),0x00000002); //Enable all trigger, in our case only HW trigger 1 is available so should be 0x00000002
}


// Initialize the mapping of the SVEC Board over the vmebus

int svecInitVme(struct vme_mapping * map)
{
    // For ranges of address modifier and data width
    // see /acc/local/L865/include/vmebus.h

    void * ptr_vme; // Pointer to mapped VME device
    memset(map, 0, sizeof(struct vme_mapping));

    // address modifier in our case we need the 0x09, so the corresponding one is VME_A32_USER_DATA_SCT

    map->am = VME_A32_USER_DATA_SCT;
   // map->am = VME_A32_USER_BLT;
    //Can be D8 / D16 / D32 / D64 depending on the size of data you want to send through the vme bus

    map->data_width = VME_D32;

    map->sizeu = 0;
    map->sizel = 0x80000;

    map->vme_addru = 0;
    map->vme_addrl = VME_BASE_ADDRESS;

    // Attempt to map VME device

    if ((ptr_vme = vme_map(map, 1)) == NULL)
    {
        logPrintf("ERROR Couldn't map at address :0x%08x", VME_BASE_ADDRESS);
        return(1);
    }

    svec_regs = (struct svec_registers_t *)ptr_vme;
    return(0);
}

void svecExit(struct vme_mapping * map)
{
    vme_unmap(map, 1); //Always unmap at the end
}
