// Includes
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdarg.h>
#include <stdint.h>
#include <sys/time.h>
#include "log.h"
/*
int32_t logError(const char *format, ...)
{
    va_list args;
    char temp[SIZE_OF_MESSAGES + 1];
    char final[SIZE_OF_MESSAGES + 1];

    va_start(args, format);
    vfprintf(temp, format, args);

    snprintf(final,SIZE_OF_MESSAGES, "ERROR %s", temp);

    logPrintf(final);

    va_end(args);

    return 0;
}
int32_t logWarning(const char *format, ...)
{
    va_list args;
    char temp[SIZE_OF_MESSAGES + 1];
    char final[SIZE_OF_MESSAGES + 1];

    va_start(args, format);
    vfprintf(temp, format, args);

    snprintf(final,SIZE_OF_MESSAGES, "WARNING %s", temp);
    logPrintf(final);

    va_end(args);

    return 0;
}
int32_t logInfo(const char *format, ...)
{
    va_list args;
    char temp[SIZE_OF_MESSAGES + 1];
    char final[SIZE_OF_MESSAGES + 1];

    va_start(args, format);
    vfprintf(temp, format, args);
    snprintf(final,SIZE_OF_MESSAGES, "INFO %s", temp);
    logPrintf(final);
    va_end(args);

    return 0;
}
int32_t logPrintf(const char *format)
{
    char timestamp[SIZE_OF_MESSAGES + 1];

    struct tm * tm ;
    struct timeval tv;
    long milliseconds;

     gettimeofday (&tv, NULL);
     tm = localtime (&tv.tv_sec);
     strftime (timestamp, sizeof(timestamp), "%d/%m/%Y %H:%M:%S", tm);
     milliseconds = tv.tv_usec / 1000;

    fprintf(stderr, "%s.%03ld: %s", timestamp, milliseconds, format);

    return 0;
}

*/
int32_t logPrintf(const char *format, ...)
{
    char timestamp[SIZE_OF_MESSAGES + 1];
    va_list args;

    struct tm * tm ;
    struct timeval tv;
    long milliseconds;

     gettimeofday (&tv, NULL);
     tm = localtime (&tv.tv_sec);
     strftime (timestamp, sizeof(timestamp), "%d/%m/%Y %H:%M:%S", tm);
     milliseconds = tv.tv_usec / 1000;

    fprintf(stderr, "%s.%03ld: ", timestamp,milliseconds);

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    return 0;
}
