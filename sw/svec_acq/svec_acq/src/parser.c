// Includes
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <regex.h>


#include "parser.h"
#include "log.h"

// Global Declarations

uint32_t t_line_check[MAX_CHANNELS]; // Table to know if an input is redundant


uint64_t parserGetAllActiveChannels()
{
    int i;
    uint64_t mask = 0;

    for (i = MAX_CHANNELS - 1; i >= 0; i--)
    {
        mask |= t_line_check[i];

        if (i > 0)
        {
            mask = mask << 1;
        }
    }

    return mask;
}

int parserCheckForRedundancy(struct config_t * s_line, struct config_t * config, int *j )
{
    if (t_line_check[s_line->channel] == 0)
    {
        config[*j] = *s_line;
        t_line_check[s_line->channel] = 1;
        *j += 1;
        return 0;
    }
    else
    {
        return 1;
    }
}


// This function will read the configuration file, in csv format and will parse it.
// All the data will be stocked in the global table t_line for afterwards utility

int parserParse(const char* filename, struct config_t* config, size_t* n)
{

    static const char * const REGEX_TEMPLATE = "[ ]*INPUT[ ]*,[ ]*NAME[ ]*,[ ]*GAIN[ ]*,[ ]*OFFSET[ ]*,[ ]*GROUP[ ]*,[ ]*COMMENTS[ ]*";
    static const char * const HEADER = "INPUT,NAME,GAIN,OFFSET,GROUP,COMMENTS";

    *n = 0;

    char line_read[MAX_LINE_SIZE + 1]; // The line we have just read
    FILE * fd;
    struct config_t s_line; // Where the line's data will be stocked

    regex_t regex;
    char temp_name[MAX_LINE_SIZE + 1];
    char temp_group[MAX_LINE_SIZE + 1];
    int reti;
    int j = 0;
    memset(t_line_check, 0, sizeof(t_line_check));

    if (!(fd = fopen(filename, "r")))
    {
        logPrintf("ERROR Failed to open config file: %s\n", strerror(errno));
        exit(1);

    }

    //Skip the first line

    fgets(line_read, MAX_LINE_SIZE, fd);

    //Let's prepare the regex comparison

    reti = regcomp(&regex, REGEX_TEMPLATE, 0);
    reti = regexec(&regex, line_read, 0, NULL, 0);

    // Does the Header in the file match our Header format ?

    if (reti == REG_NOMATCH)
    {
        logPrintf("ERROR Header of the file should be the following : ' %s'\n",
                HEADER);
        exit(1);
    }

    //While we can read a line

    while (fgets(line_read, MAX_LINE_SIZE, fd) != NULL)
    {
        memset(&s_line, 0, sizeof(struct config_t));

        // Does the line start with a space or a # or is blank ? If so this line is ignored

        if (line_read[0] == '#' || line_read[0] == ',' || line_read[0] == ' ')
        {
            logPrintf(
                    "INFO A Commented line has been detected, this line is ignored\n");
        }
        else
        {
            // Parse the line read, and stock it in our structure

            sscanf(line_read, "%u,%250[^,],%lf,%lf,%250[^,]", &s_line.channel,
                    temp_name, &s_line.gain, &s_line.offset, temp_group);
            strncpy(s_line.name, temp_name, SIZE_ALLOWED);
            strncpy(s_line.group, temp_group, SIZE_ALLOWED);

            //Does the value of the input is between [0,63] ?

            if (s_line.channel >= 0 && s_line.channel <= MAX_CHANNELS - 1)
            {
                // Is the line redundant ?

                if(parserCheckForRedundancy(&s_line, config, &j) != 0)
                {
                    logPrintf( "WARNING The Line no : %d is redundant, please check the configuration file. This line is ignored \n",
                                                s_line.channel);
                }
                else
                {
                    *n += 1;
                }
            }
            else
            {
                logPrintf(
                        "WARNING The number of the channel should be between 0 and 63 \n");
            }
        }

        memset(line_read, 0, sizeof(line_read));
    }

    fclose(fd);
    return 0;
}

