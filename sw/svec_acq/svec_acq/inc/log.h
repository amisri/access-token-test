/*
 * utilities.h
 *
 *  Created on: Jun 8, 2016
 *      Author: kcarli
 */

#ifndef SW_SVEC_ACQ_UNIX_UTILITIES_INC_LOG_H_
#define SW_SVEC_ACQ_UNIX_UTILITIES_INC_LOG_H_


#include "const.h"


int32_t logPrintf(const char *format, ...);

#endif /* SW_SVEC_ACQ_UNIX_UTILITIES_INC_LOG_H_ */
