/*
 * const.h
 *
 *  Created on: Jul 1, 2016
 *      Author: kcarli
 */

#ifndef SW_SVEC_ACQ_UNIX_SVEC_ACQ_INC_CONST_H_
#define SW_SVEC_ACQ_UNIX_SVEC_ACQ_INC_CONST_H_



#define MAX_LINE_SIZE               250
#define SIZE_ALLOWED                15
#define MAX_NUM_SAMPLES             524288
#define MAX_CHANNELS                64
#define SIZE_OF_MESSAGES            1024



static const char * const CONFIG_PATH          =       "/user/pclhc/etc/svec_acq/";
static const char * const JSON_PATH            =       "/user/pclhc/var/log/svec_acq/data/"; //real
static const char * const RAW_PATH             =       "/user/pclhc/var/log/svec_acq/data/";
static const char * const JSON_ARCH            =       "/user/pclhc/var/log/svec_acq/";


static const uint32_t DOWNSAMPLED_FREQUENCY = 12500;

static const double TIMES_BT_A = 2.5;
static const double TIMES_AT_A = 0.5;
static const double TIMES_BT_B = 2.5;
static const double TIMES_AT_B = 0.5;
static const int    SIXTY_FOUR = 64;

unsigned long time_first_sample_a;
unsigned long time_first_sample_b;
unsigned long time_first_sample_on_demand;
uint8_t is_on_demand;


#endif /* SW_SVEC_ACQ_UNIX_SVEC_ACQ_INC_CONST_H_ */
