/*
 * svec_acq.h
 *
 *  Created on: May 30, 2016
 *      Author: kcarli
 */

#ifndef SW_SVEC_ACQ_UNIX_SVEC_ACQ_INC_SVEC_VME_H_
#define SW_SVEC_ACQ_UNIX_SVEC_ACQ_INC_SVEC_VME_H_

#include <stdint.h>
#include <libvmebus.h>
#include <const.h>

#ifdef GLOBALS
#define SVEC_VME_EXT
#else
#define SVEC_VME_EXT extern
#endif

// Constant Declarations

//const char * DUMP_TO_FILE_RAW;
char  DUMP_TO_FILE_RAW[SIZE_OF_MESSAGES];

static const uint32_t VME_BASE_ADDRESS = 0x10000000;
static const uint32_t BASE_FOR_TOP = 0x03D09004;
static const uint32_t START_ADD_OF_BUFF = 0x00000000;

// Structure Declarations

struct svec_global_t // Global registers from 0x000 to 0x3FF
{
    uint32_t reserved0[4];              // [0x000] Reserved to System FPGA boot update
    uint32_t interrupt_status;          // [0x010] Interrupt FIFO [7:0]. Clear on READ.
    uint32_t interrupt_mask;            // [10:10] => hs_int_mode (R/W) [09:09] => ready for int (R/W) [08:08] => rora_roak (R/W) [07:00] => bit[x]='1' masks interrupt source[x] (R/W)
    uint32_t interrupt_debug;
    uint32_t fpga_info;
    uint32_t interrupt_config;
    uint32_t clock_config;
    uint32_t pll_config;
    uint32_t lemo_and_led;              // [0x02C] [31:28] fpio4 out source
    uint32_t reserved1[244];            // [0x030] Reserved
};

struct svec_buffer_t                    // Global registers from 0x000 to 0x3FF
{
    uint32_t buffer_number;             // Read only value, configurable at firmware compile time
    uint32_t buffer_type;               // Read only value, specifying the buffer implementation
    uint32_t test;                      // Spare register for write/read test
    uint32_t reserved2;
    uint32_t status;                    // Slave Command/Status register
    uint32_t chan_31_00;                // Channels [31..0]: bit ‘N’ activates the channel ‘N’ when set
    uint32_t chan_63_32;                // Channels [63..32]: bit ‘N’ activates the channels ‘N’ when set
    uint32_t reserved3;
    uint32_t trigger_enable;            // Triggers vector enable: bit ‘N’ set will enable trigger ‘N’. At reset all triggers are disabled.[5:5] Auto trigger [4:1] 4 HW triggers[0:0] SW trigger.
    uint32_t trigger_rising_edge;
    uint32_t trigger_falling_edge;
    uint32_t trigger_status;            // Current status of triggers (Read ONLY)
    uint32_t buffer_sa;                 // DDR3 buffer start address. It MUST be aligned on a ‘4x#ActiveChannels’ Byte boundary (i.e.: 0, 128, 256, etc.)
    uint32_t buffer_size;
    uint32_t buffer_level;
    uint32_t buffer_event;              // It contains the address of the circular buffer at which the event happened.
    uint32_t buffer_curr;               // Register containing the currently addressed  memory location.
    uint32_t down_sample;
    uint32_t time_event_s;
    uint32_t time_event_ms;
    uint32_t last_sample;               // Last sample written. When many channels are active, it is last sample of active channel with highest number.
    uint32_t time_s;                    // Unix time in seconds
    uint32_t time_ms;                   // Time in ms. Every 1000 ticks it resets to 0 and increments Unix time register.
    uint32_t counter_ms;
    uint32_t buffer_spy_sa;             // Register containing the DDR start address for VME acquisition of ‘Buffer_spy_len+1’ samples
    uint32_t buffer_spy_len;            // A value = ‘N’ in this will generate a request to DDR of ‘N+1’ samples. Min(‘N’)=0, max(‘N’)=63.
    uint32_t buffer_spy_state;          // Register containing the current VME transfer status
    uint32_t reserved4;
    uint32_t buffer_test_sa;            // Register containing the DDR address for a VME single write: when this register is written the DDR controller will write ‘Buffer_test_data’ into DDR addressed location (test port: VME_Write).
    uint32_t buffer_test_data;          // Register containing the data to be written into ‘Buffer_test_sa’ DDR address (test port: VME_Write).
    uint32_t buffer_test_state;         // Register containing the VME write channel status. See table 7 (test port: VME_Write).
    uint32_t reserved0;                 // Reserved
    uint32_t buffer_test_read_add;      // Register containing the address to fetch 1 sample data from (test port: ADC_Read).
    uint32_t buffer_test_read_data;
    uint32_t reserved5[2];
    uint32_t auto_trigger_high;         // [14:0] Register containing the positive limit of data samples above which the auto trigger, when enabled, will be set.
    uint32_t auto_trigger_low;          // [14:0] Register containing the negative limit of data samples below which the auto trigger, when enabled, will be set.
    uint32_t reserved1[26];             // Reserved
    uint32_t buffer_spy_read[64];       // FIFO addresses with buffer data (Read ONLY).

};

struct svec_registers_t
{
    struct svec_global_t global;
    struct svec_buffer_t buffer_a;
    struct svec_buffer_t buffer_b;

};

struct trigger_sample_t
{
    uint32_t number_of_samples_bt_a;
    uint32_t number_of_samples_at_a;
    uint32_t number_of_samples_bt_b;
    uint32_t number_of_samples_at_b;

};

//Global Declarations

struct svec_registers_t * svec_regs;
struct trigger_sample_t trigger_sample;

char address_buffer_a[SIZE_ALLOWED + 1];
char address_buffer_b[SIZE_ALLOWED + 1];

// Function Declarations

int svecReadingWithBuffSpyBlock(int times, unsigned int addr_to_read,
        struct svec_buffer_t * buffer_offset, int nb_sample, uint32_t  LENGHT_OF_BUFF);
int svecWrittingToFileBlock(int nb_sample,
        struct svec_buffer_t * buffer_offset, int *first_time);

/*!
 * Function svecInitVme
 * Initalise the mapping of the SVEC Board over the vmebus
 *
 * @param[in]  map            structure usefull to know how to map
 *
 * @param[out]  ptr_vme       pointer to our mapped device if the mapping process worked.
 *
 */
int svecInitVme(struct vme_mapping *map);

/*!
 * Function svecExit
 * Unmapp the SVEC board
 *
 */
void svecExit(struct vme_mapping *map);

/*!
 * Function svecVmeMemRead
 * Read data located at address address
 *
 * @param[in]  address       address where to read
 *
 * @param[out] data read
 *
 */
unsigned int svecVmeMemRead(volatile void * address);

/*!
 * Function svecVmeMemWrite
 * Write data value at address address
 *
 * @param[in]  address       address where to write
 * @param[in]  value      data to write
 *
 */
void svecVmeMemWrite(volatile void * address, uint32_t value);

/*!
 * Function svecResetRegisters
 *
 * Reset registers for buffer A and B
 *
 */
void svecResetRegistersA();
void svecResetRegistersB();

/*!
 * Function svecSetupBufferA and svecSetupBufferB
 * Setup the buffer A or first buffer, which start at offset + 0x400
 *
 * @param[in]  channels_low       Which 32 first channels are activated
 * @param[in]  channels_high       Which 32 last channels are activated
 *
 */
void svecSetupBufferA(uint32_t channels_low, uint32_t channels_high, uint32_t LENGHT_OF_BUFF);

void svecSetupBufferB(uint32_t channels_low, uint32_t channels_high, uint32_t LENGHT_OF_BUFF);

#endif /* SW_SVEC_ACQ_UNIX_SVEC_ACQ_INC_SVEC_VME_H_ */
