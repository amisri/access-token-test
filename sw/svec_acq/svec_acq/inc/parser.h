/*
 * parser.h
 *
 *  Created on: Jun 13, 2016
 *      Author: kcarli
 */

#ifndef SW_SVEC_ACQ_UNIX_PARSER_INC_PARSER_H_
#define SW_SVEC_ACQ_UNIX_PARSER_INC_PARSER_H_

#include "const.h"



/*!
 * Structure Declarations
 * Structure which will contain all parameters from a line
 */

struct config_t
{
    uint32_t      channel;
    char          name[SIZE_ALLOWED + 1];
    double        gain;
    double        offset;
    char          group[SIZE_ALLOWED + 1];
};



// Function Declarations

int parserParse(const char* filename, struct config_t* config, size_t* n);
uint64_t    parserGetAllActiveChannels();

#endif /* SW_SVEC_ACQ_UNIX_PARSER_INC_PARSER_H_ */
