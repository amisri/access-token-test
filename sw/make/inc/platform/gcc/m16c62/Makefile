# -----------------------------------------------------------------------------------------------------------
# File:		Makefile
#
# Purpose:  Make variables and targets for the m16c62
# -----------------------------------------------------------------------------------------------------------

compiler_version    = 4.7.2
tmp_tool_path       = $(subst \,/,$(COMPILERS_PATH))

# the following is a trick to accept spaces in tool_path directory names

# we change the spaces in the tool path  to ? (wildcard)
empty          :=
space          := $(empty) $(empty)
tool_path       = $(subst $(space),?,$(tmp_tool_path))/gcc/m32c/$(compiler_version)/m32c-elf

# Project variables

src_path        = src

# -----------------------------------------------------------------------------------------------------------
# Includes
# these are the most generic (usual), platform specific, the less generic
# are included in the project specific Makefile

# for structs_bits_little.h
glob_inc      	= $(fixed_PRJ_PATH)/inc
inc_dirs       += $(glob_inc)

# for cc_types.h
# for iodefines.h
glob_inc_up    	= $(fixed_PRJ_PATH)/inc/gcc/m16c62
inc_dirs       += $(glob_inc_up)

# for stdio.h
# for _ansi.h
# for newlib.h
ansi_inc	= $(tool_path)/bin/newlib/include
inc_dirs       += $(ansi_inc)

# for config.h
std1inc		= $(tool_path)/bin/newlib/include/sys
inc_dirs       += $(std1inc)

# for ieeefp.h
std2inc		= $(tool_path)/bin/newlib/include/machine
inc_dirs       += $(std2inc)

# for stddef.h
# for stdarg.h
#std3inc	= $(tool_path)/lib/gcc/m32c-elf/1.0-CERN.0/include
std3inc		= $(tool_path)/lib/gcc/m32c-elf/$(compiler_version)/include
inc_dirs       += $(std3inc)

# for errno.h
newLib0inc	= $(tool_path)/bin/newlib/include
inc_dirs       += $(newLib0inc)

# for stat.h
newLib1inc	= $(tool_path)/bin/newlib/include/sys
inc_dirs       += $(newLib1inc)

# for syscall.h
newLib2inc	= $(tool_path)/bin/newlib/libgloss
inc_dirs       += $(newLib2inc)

# for os.h
osinc		= $(fgc)/sw/fgc/nanos/inc
inc_dirs       += $(osinc)

# -----------------------------------------------------------------------------------------------------------
# Libraries
# warning !!! the linker is sensitive to the order of the parameters !!!

libs           = --library new --library gcc
lib_dirs       = $(tool_path)/../newlib \
                 $(tool_path)/lib/gcc/m32c-elf/$(compiler_version)/m32cm

# -----------------------------------------------------------------------------------------------------------
# Source and objects

asm_source     := $(filter %.S, $(source))
asm_objects    := $(asm_source:%.S=$(obj_path)/%.o)
c_objects      := $(c_source:%.c=$(obj_path)/%.o)
objects        += $(asm_objects)

# -----------------------------------------------------------------------------------------------------------
# Paths
# in general file the assembler files are "asm" so we need to add this here because assembler
# files are "S" in this platform. GNU GCC restriction

vpath %.S $(src_path)

# -----------------------------------------------------------------------------------------------------------
# Tool variables

LD		= $(tool_path)/bin/m32c-elf-ld.exe
LDFLAGS         =
# --verbose


OBJCOPY         = $(tool_path)/bin/m32c-elf-objcopy.exe

AR              = $(tool_path)/bin/m32c-elf-ar.exe

CC              = $(tool_path)/bin/m32c-elf-gcc.exe
# -fno-tree-vrp avoids bug in version 10.02
CFLAGS		= -c \
		-nostdinc \
		-Wall \
		-Werror \
		-gdwarf-2 \
		-O2 \
		-save-temps=obj \
		-mcpu=m32c \
		-D__M16C62__ \
		-Wa,-gdwarf2 \
		-Wa,-adlhn="$(obj_path)/$(basename $(notdir $<)).lst"

# -E = Outputs a source file after preprocessed.
# -S = Outputs an assembly language file.
# -c = Outputs a relocatable file
# -H = show the headers open by the compiled file
# -g \
# -ggdb3 \
# -g2
# -x c
# -x assembler-with-cpp

# -----------------------------------------------------------------------------------------------------------
# Targets

$(exec): $(objects) linker.txt linker_dbg.txt
	@[ -d $(@D) ] || mkdir -p $(@D)

	# Link

# warning !!! the linker is sensitive to the order of the parameters !!!

	$(LD) $(LDFLAGS) --script="linker.txt"     -Map prog.map     --output=prog.x     $(addprefix -L ,$(lib_dirs)) $(objects) $(libs)
	$(LD) $(LDFLAGS) --script="linker_dbg.txt" -Map prog_dbg.map --output=prog_dbg.x $(addprefix -L ,$(lib_dirs)) $(objects) $(libs)

	# Translate to Motorola and to binary

	$(OBJCOPY) -O srec   prog.x     prog.mot
	$(OBJCOPY) -O binary prog.x     prog.bin
	$(OBJCOPY) -O srec   prog_dbg.x prog_dbg.mot
	$(OBJCOPY) -O binary prog_dbg.x prog_dbg.bin

$(lib): $(objects)
	@[ -d $(@D) ] || mkdir -p $(@D)
	$(AR) rcv $@ $?

clean:
	$(RM) $(dep_path)/*.d $(obj_path)/*.i $(obj_path)/*.lst $(obj_path)/*.s $(obj_path)/*.o *.map $(exec) $(lib)

# -----------------------------------------------------------------------------------------------------------
# Assembler objects

$(obj_path)/%.o: %.S
	@[ -d $(@D) ]       || mkdir -p $(@D)
	@[ -d $(dep_path) ] || mkdir -p $(dep_path)
	$(CC) $(CFLAGS) -MD -MF $(@:$(obj_path)/%.o=$(dep_path)/%.d) $(addprefix -I ,$(inc_dirs)) -o $@ $<

# Dependencies

include $(wildcard $(dep_path)/*.d)

# C objects

$(obj_path)/%.o: %.c
	@[ -d $(@D) ]       || mkdir -p $(@D)
	@[ -d $(dep_path) ] || mkdir -p $(dep_path)
	$(CC) $(CFLAGS) -MD -MF $(@:$(obj_path)/%.o=$(dep_path)/%.d) $(addprefix -I ,$(inc_dirs)) -c -o $@ $<

# -----------------------------------------------------------------------------------------------------------
# Special targets

.PHONY: clean

# EOF
