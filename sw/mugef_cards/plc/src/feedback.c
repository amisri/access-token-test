/*---------------------------------------------------------------------------------------------------------*\
 File:          feedback.c

 Purpose:

 Author:

 Notes:

 History:

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

/*---------------------------------------------------------------------------------------------------------*/
void FdkTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
          FeedBack Task

Fonction   : Initialise la fonction FctSat (State)
             Si incoherence faire Stop

Active par : Interrupt Routine FeedBack (IrkFdk).

Active     : -.

\*---------------------------------------------------------------------------------------------------------*/
{

    INT8U err;


    OS_ENABLE_INTS();       // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)        // Main loop
    {

        //   OSTaskSuspend (OS_PRIO_SELF);
        OSSemPend(SemFeedBack, 0, &err);


        FctSat();

        if ((state.Supply   == INCOHERENCE) ||
            (state.Polarity == INCOHERENCE) ||
            (state.Mode     == INCOHERENCE) ||
            (state.Slave    == INCOHERENCE))                        // *VerP9*
        {


            OSTaskDel(STR_TSK_PRI);                                     // Delete Start Task
            OSTaskDel(OSB_TSK_PRI);                                     // Delete Stb Task
            ram_out.cmd_direct_communication.PhaseBack    = ON;         // Fast Stop
            ram_out.cmd_direct_communication.Block        = ON;
            ram_out.cmd_direct_communication.Soft         = ON;
            ram_out.cmd_direct_communication.Reconstitued = ON;
            ram_out.cmd_direct_relay.Start                = OFF;
            ram_out.cmd_direct_external.Spare             = OFF;        // *VerP9*
            *out = ram_out;
            err = OSTaskCreate(StrTsk, 0, &tsk_stk.str[STR_STK_SIZE - 1], STR_TSK_PRI); // Created Start task
            err = OSTaskCreate(OsbTsk, 0, &tsk_stk.osb[OSB_STK_SIZE - 1], OSB_TSK_PRI); // Created OnStb task
        }

        fdk.running++;          // Increment emt running counter

    }
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: Emt.c
\*---------------------------------------------------------------------------------------------------------*/

