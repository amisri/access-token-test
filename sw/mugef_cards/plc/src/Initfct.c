/*---------------------------------------------------------------------------------------------------------*\
 File:      initfct.c

 Purpose:       Initialisation des peripheriques internes et externes

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>


/***************************** Initialise Peripheral Functions ******************************/

void Init_timer()
{
    GPT_PDDR  = 0xff;               /* All port pins are output */
    GPT_MCR   = 0x0085;             /* SUPV = 1, IARB = 5 */
    GPT_ICR   = 0x0680;             /* Timer is highest priority at vector base 0x80 */
    GPT_TCTL  = 0x0000;             /* Timer OC2-3-4-5 disconnected from output logic */
    GPT_TMSK  = 0x0000;             /* Sysclk / 4  = 4MHz  counter frequency  */
    {int tmp  = GPT_TFLG;}          /* Read to clear all outstanding interrupts */
    GPT_TFLG  = 0;                  /* Clear all outstanding interrupts */
}

/* ---------------------------------------------------------------------------------------- */
/*              counter frequency = 1MHz                                                           */
/*              int_frequency = 1000   => all 1 msec interrupt timer occured               */
/*              with unsigned integer range 0.001 sec to 65.536 sec                         */
/* ---------------------------------------------------------------------------------------- */

void Timer1(float Delay)
{
    timer1_count = (INT16U)(Delay * CONVERT_FOR_SEC);            // Convert into ms
    GPT_TOC1  = GPT_TCNT + OS_TICK_PERIOD;                        // Comparator1 = Counter + Freq.
    GPT_TMSK  |=   0x0800;                                       // Timer1 Interrupt Enable
}

void Timer2(float Delay)
{
    timer2_count = (INT16U)(Delay * CONVERT_FOR_SEC);            // Convert into ms
    GPT_TOC2  = GPT_TCNT + OS_TICK_PERIOD;                        // Comparator2 = Counter + Freq.
    GPT_TMSK  |=   0x1000;                                       // Timer2 Interrupt Enable
}

void Timer3(float Delay)
{
    timer3_count = (INT16U)(Delay * CONVERT_FOR_SEC);            // Convert into ms
    GPT_TOC3  = GPT_TCNT + OS_TICK_PERIOD;                        // Comparator3 = Counter + Freq.
    GPT_TMSK  |=   0x2000;                                       // Timer3 Interrupt Enable
}

void Timer4(float Delay)
{
    timer4_count = (INT16U)(Delay * CONVERT_FOR_SEC);            // Convert into ms
    GPT_TOC4  = GPT_TCNT + OS_TICK_PERIOD;                        // Comparator4 = Counter + Freq.
    GPT_TMSK  |=   0x4000;                                       // Timer4 Interrupt Enable
}

void Timer5(float Delay)
{
    timer5_count = (INT16U)(Delay * CONVERT_FOR_SEC);            // Convert into ms
    GPT_TOC4  = GPT_TCNT + OS_TICK_PERIOD;                        // Comparator5 = Counter + Freq.
    GPT_TMSK  |=   0x8000;                                       // Timer5 Interrupt Enable
}


void Init_pwm()
{

    /* PWMB Clock is used to Input FPGA De-bounce   */
    GPT_PWMC  = 0x50;                                            // Fclk / 64, Fast Mode
    GPT_PWMB  = 0x80;                                            // PwmB = 1Khz, Duty Cycle 50%

}

void Init_external_interrupt()
{
    SIM_PFPAR = 0x00FE;                                           // FPGA Interrupt Enable
}


void Init_serial_communication_interface()
{
    QSM_MCR   = 0x0004;                 // QSM clock operation stopped, IARB = 4
    QSM_QILR  = 0x06;                   // QSPI priority = 1
    QSM_QIVR  = 0x40;                   // Vector base 0x40
    QSM_SCCR0 = 0x0034;                 // SCI Baud Rate = SysClk/32*SCCR0 = 9600
    QSM_SCCR1 = 0x002C;                 // No parity, Transmitter Enabled
    // Receiver Enabled
    Data_Serial_Register = QSM_SCSR;
    Data_Serial_Register = QSM_SCDR;
}


