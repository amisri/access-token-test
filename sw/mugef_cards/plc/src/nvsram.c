/*---------------------------------------------------------------------------------------------------------*\
 File:      nvsram.c

 Purpose:       Gestion de la memoire NVSRAM :
                 - Appui sur RST FAULT + RESET MICRO en LOCAL  MODE : Store
                 - Appui sur RST FAULT + RESET MICRO en REMOTE MODE : Recall

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>


/****************************  NVSRAM memory Function  ***************************/

#pragma NO_FRAME
void  Nvsram_Store()
{

    asm
    {
        PSHM  K
        LDAB  #0
        TBEK

        LDAA   0x0E38      // Read Chip Address 0E38 (Start of command sequence)
        LDAA   0x31C7      // Read Chip Address 31C7
        LDAA   0x03E0      // Read Chip Address 03E0
        LDAA   0x3C1F      // Read Chip Address 3C1F
        LDAA   0x303F      // Read Chip Address 303F (End of command sequence)
        LDAA   0x0FC0    // Read Chip Address 0FC0 (Store command)
        PULM   K
    }
}

#pragma NO_FRAME
void Nvsram_Recall()
{
    asm
    {
        PSHM  K
        LDAB  #0
        TBEK

        LDAA   0x0E38      // Read Chip Address 0E38 (Start of command sequence)
        LDAA   0x31C7      // Read Chip Address 31C7
        LDAA   0x03E0      // Read Chip Address 03E0
        LDAA   0x3C1F      // Read Chip Address 3C1F
        LDAA   0x303F      // Read Chip Address 303F (End of command sequence)
        LDAA   0x0C63     // Read Chip Address 0FC0 (Recall command)
        PULM  K
    }
}

void Nvsram()
{
    long int Pt_from;

    *((INT16U *)&in->enable_cmd_local)    = REG_ENABLE;
    ram_in = *in;

    /* Local and First_Fault Button pushed then Store SRAM data in EEPROM */
    if ((ram_in.cmd_local.ResetFlt == ON) && (ram_in.fb.Local == ON))
    {
        Pt_from = (long int)Nvsram_Store;
        MemCpy(0xF2000, Pt_from, 0x80);
        asm JSR   0xF2000;
    }

    /* Remote and First_Fault pushed then Recall EEPROM data in SRAM */
    if ((ram_in.cmd_local.ResetFlt == ON) && (ram_in.fb.Local == OFF))
    {
        Pt_from = (long int)Nvsram_Recall;
        MemCpy(0xF2100, Pt_from, 0x80);
        asm JSR    0xF2100;
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void MemCpy(long int to, long int from, INT16U n_words)
/*---------------------------------------------------------------------------------------------------------*\
  By Quentin King.
  This assembler function copies the specified number of words from anywhere
  in the 1MB HC16 memory maps to anywhere else.  It uses the page registers
  XK and YK which must be reset by interrupt routines.  ZK is not affected.
  n_words can be in the range 0x0000 to 0xFFFF.  Note that to copy one page,
  n_words = 0x8000.
\*---------------------------------------------------------------------------------------------------------*/
{
    asm
    {
        /* Set up XK:IX = from address, YK:IY = to address */

        PSHM  K               // Save page registers
        LDX   from:2           // IX = address within "from" page
        LDAB  from:1        // B  = number of "from" page
        TBXK                     // XK = B
        LDY   to:2            // IY = address within "to" page
        LDAB  to:1      // B  = number of "to" page
        TBYK                     // YK = B

        /* Copy words in excess of an 8 word block individually */

        LDE   n_words          // E  = number of words to copy
        ANDE  #7                 // E  = E & 0x07
        BEQ   blockcopy     // Jump if there are no excess word to copy

        wordcopyloop:
        LDD   0, X            // Copy 1 word...
        STD   0, Y            // from (IX) to (IY)
        AIX   #2                 // Increment IX by 2 bytes
        AIY   #2                 // Increment IY by 2 bytes
        SUBE  #1                 // Decrement loop counter
        BNE   wordcopyloop  // Loop back until zero

        /* Copy blocks of 8 words */

        blockcopy:
        LDE   n_words          // E = number of words to copy
        ANDE  #0xFFF8          // E = E & ~7
        BEQ   end             // Jump if there are no 8 word blocks to copy

        blockcopyloop:
        LDD   0, X            // Copy 8 words from (IX) to (IY)
        STD   0, Y
        LDD   2, X
        STD   2, Y
        LDD   4, X
        STD   4, Y
        LDD   6, X
        STD   6, Y
        LDD   8, X
        STD   8, Y
        LDD  10, X
        STD  10, Y
        LDD  12, X
        STD  12, Y
        LDD  14, X
        STD  14, Y
        AIX   #16             // Increment IX by 16 bytes
        AIY   #16             // Increment IY by 16 bytes
        SUBE  #8                 // Decrement loop counter by 8 words
        BNE   blockcopyloop // Loop back until zero
        end:
        PULM  K               // Restore page registers
    }
}
/* End of file: nvsram.c */
