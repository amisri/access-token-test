/*---------------------------------------------------------------------------------------------------------*\
 File:      Communication.c

 Purpose:

 Author:

 Notes:

 History:

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

/*---------------------------------------------------------------------------------------------------------*/
void CtnTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
       Communication Task

Fonction   : Gestion des demandes de status et des commandes provenants du systeme MUGEF, de la
             liaison serie et des commandes locales (PB).

Active par : Serial Communication Interface Task (RxTsk).
             Command Interrupt Routine (CmdIrq).

Active     : Command Task (CmdTsk).
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U * ch = stdin;
    INT16U i;
    INT16U counter = 0;
    INT16U DWord = REG_DISABLE;
    INT16U Remote_Cmd;
    BOOLEAN MemExtCmd = OFF;                                                     //        *VerP9*
    INT8U s1[5], s1h[3];
    static INT8U err;

    OS_ENABLE_INTS();        // All tasks start with interrupts disabled

    /* Task loop */


    for (;;)         // Main loop
    {

        //OSTaskSuspend (OS_PRIO_SELF);
        OSSemPend(SemCtn, 0, &err);

        FctSat();

        /************************ Local Command ***********************/
        if (state.Control == LOCAL)
        {
            if (ram_in.cmd_local.ResetFlt == ON)
            {
                cmd.value = RESET;
            }

            if (ram_in.cmd_local.PolPlus == ON)
            {
                cmd.value = POLPLUS;
            }

            if (ram_in.cmd_local.PolMoins == ON)
            {
                cmd.value = POLMOINS;
            }

            if (ram_in.cmd_local.Mode1 == ON)
            {
                cmd.value = MODE1;
            }

            if (ram_in.cmd_local.Mode2 == ON)
            {
                cmd.value = MODE2;
            }

            if (ram_in.cmd_local.Mode3 == ON)
            {
                cmd.value = MODE3;
            }

            if (ram_in.cmd_local.Start == ON)
            {
                cmd.value = START;
            }
        }

        /************************* End Local Command ************************/


        /************************** External command ************************/
        if (state.Control == REMOTE)                     //  Command only remote mode   begin   *VerP9*
        {
            if (ram_in.cmr.Extcmd != MemExtCmd)             // External remote command added
            {
                if (ram_in.cmr.Extcmd == ON)
                {
                    cmd.value = START;
                }
                else
                {
                    cmd.value = STOP;
                }

                MemExtCmd = ram_in.cmr.Extcmd;
            }
        }                                                   //             end         *VerP9*

        /************************ End External Command **********************/


        /******************* MUGEF Status & Command -- MPC Network***********/
        if (Flag_Irq_Network == TRUE)                // begin VER20
        {
            Remote_Cmd = Ni_data_word;
        }
        else
        {
            Remote_Cmd = (INT16U) ram_in.cmr.Value;    // end VER20
        }

        if (Remote_Cmd != 0x00)
        {
            if (Flag_Irq_Network == FALSE)      // VER20 - Command from daisy chain
            {
                OSTimeDly(1);                    // Synchro MUGEF cmd, wait cmr stable
                ram_in = *in;
            }

            DWord = Status0();

            switch (Remote_Cmd)
            {
                case (STA_BIT0) : DWord = Status0();                     // Default Status
                    break;

                case (STA_BIT1) : DWord = Status1();                     // List 1 : Fault detail Digital
                    break;

                case (STA_BIT2) : DWord = Status2();                     // List 2 : Fault detail Analog
                    break;

                case (STA_BIT3) : DWord = Status3();                     // List 3 : First Fault detail
                    break;

                case (STA_BIT4) : DWord = Status4();                     // List 4 : First Fault detail
                    break;

                case (STA_BIT5) : DWord = Status5();                     // Channel
                    break;

                case (STA_BIT6) : DWord  = 0x0000;               // MUGEF Incoherence Return

                    if (state.Supply == INCOHERENCE)
                    {
                        DWord = 0x0001;
                    }

                    if (state.Polarity == INCOHERENCE)
                    {
                        DWord |= 0x0002;
                    }

                    if (state.Mode == INCOHERENCE)
                    {
                        DWord |= 0x0004;
                    }

                    if (state.Slave == INCOHERENCE)
                    {
                        DWord |= 0x0008;
                    }

                    break;

                case (STA_BIT7) : DWord  = 0x0000;

                    if (!strcmp(Tmo, tab_tmo[2]))
                    {
                        DWord = 0x0001;
                    }

                    if (!strcmp(Tmo, tab_tmo[1]))
                    {
                        DWord |= 0x0002;
                    }

                    if (!strcmp(Tmo, tab_tmo[4]))
                    {
                        DWord |= 0x0004;
                    }

                    if (!strcmp(Tmo, tab_tmo[5]))
                    {
                        DWord |= 0x0008;
                    }

                    if (!strcmp(Tmo, tab_tmo[3]))
                    {
                        DWord |= 0x0010;
                    }

                    if (!strcmp(Tmo, tab_tmo[6]))
                    {
                        DWord |= 0x0020;
                    }

                    break;

                case (STA_BIT8) : DWord = Status8();                     // List 1 : Active Fault
                    break;

                case (STA_BIT9) : DWord = Status9();                   // List 2 : Active Fault
                    break;

                case (STA_BIT10) : DWord = Status10();                   // LHC Status
                    break;

                case (STA_NB0)  : DWord = VerP;    // uP Prog Version
                    break;

                case (STA_NB1)  : DWord = VerF;    // FPGA Prog Version
                    break;

                case (STA_NB2)  : DWord = pc_conf; // Type
                    break;

                case (STA_NB3)  : DWord = VerT;    // Type Prog Version
                    break;

                case (STA_NB4)  : counter = 0;
                    DWord = counter;    // erase counter
                    break;

                case (STA_NB5)  : counter ++;
                    DWord = counter;    // inc counter
                    break;

                case (TST_BUS_5)  : DWord = 0x5555;    // Test bus
                    break;

                case (TST_BUS_A)  : DWord = 0xAAAA;    // Test bus
                    break;

                default         : break;
            }

            *((INT16U *)&out->mugef_status) = DWord;
            ram_out.mugef_status = out->mugef_status;

            if (state.Control == REMOTE)                     //  Command only remote mode
            {
                switch (Remote_Cmd)
                {
                    case (STOP)     : cmd.value = STOP;
                        break;

                    case (START)    : cmd.value = START;
                        break;

                    case (STB)      : cmd.value = STB;
                        break;

                    case (RESET)    : cmd.value = RESET;
                        break;

                    case (POLPLUS)  : cmd.value = POLPLUS;
                        break;

                    case (POLMOINS) : cmd.value = POLMOINS;
                        break;

                    case (MODE1)    : cmd.value = MODE1;
                        break;

                    case (MODE2)    : cmd.value = MODE2;
                        break;

                    case (MODE3)    : cmd.value = MODE3;
                        break;

                    case (DCCT1)    : cmd.value = DCCT1;
                        break;                     // analog mux

                    case (DCCT2)    : cmd.value = DCCT2;
                        break;

                    case (IREF)     : cmd.value = IREF;
                        break;

                    case (BRDGP)    : cmd.value = BRDGP;
                        break;

                    case (BRDGM)    : cmd.value = BRDGM;
                        break;

                    case (VREF)     : cmd.value = VREF;
                        break;

                    case (VFB)      : cmd.value = VFB;
                        break;

                    case (FRREFP)   : cmd.value = FRREFP;
                        break;

                    case (FRREFM)   : cmd.value = FRREFM;
                        break;

                    case (FRREF)    : cmd.value = FRREF;
                        break;

                    case (ICIRC)    : cmd.value = ICIRC;
                        break;

                    case (VCMD)     : cmd.value = VCMD;
                        break;

                    case (ICAPA)    : cmd.value = ICAPA;
                        break;

                    case (P5V)      : cmd.value = P5V;
                        break;

                    case (M15V)     : cmd.value = M15V;
                        break;

                    case (P15V)     : cmd.value = P15V;
                        break;

                    default         : break;
                }
            }
        }

        /********************** MUGEF Status & Command ********************/


        /*********************** Communication from/to serial link *********************/
        if (stdin[0] != 0x00)
        {
            /*** Local & Remote ***/
            if (!strcmp(stdin, "FIRSTFAULT"))
            {
                sprintf(stdout, "\n\r %X %X", Status3(), Status4());
            }
            else if (!strcmp(stdin, "FAULT"))
            {
                sprintf(stdout, "\n\r %X %X", Status1(), Status2());
            }
            else if (!strcmp(stdin, "ACTIVEFAULT"))
            {
                sprintf(stdout, "\n\r %X %X", Status8(), Status9());
            }
            else if (!strcmp(stdin, "TMO"))
            {
                sprintf(stdout, "\n\r %s", Tmo);
            }
            else if (!strcmp(stdin, "TYPE"))
            {
                sprintf(stdout, "\n\r %X", pc_conf);
            }
            else if (!strcmp(stdin, "VERP"))
            {
                sprintf(stdout, "\n\r %u", (INT16U)(VerP));
            }
            else if (!strcmp(stdin, "VERF"))
            {
                sprintf(stdout, "\n\r %u", (INT16U)(VerF));
            }
            else if (!strcmp(stdin, "VERT"))
            {
                sprintf(stdout, "\n\r %u", (INT16U)(VerT));
            }
            else if (Cmp_element(MAXL))                // Start, Stop, Stb Delay
            {
            }
            else if (Cmp_element(MAXS))                      // Mode, Polarity  Delay
            {
            }
            else if (!strcmp(stdin, "CHANNEL"))
            {
                DWord =  *((INT16U *)&out->cmd_controlled_communication);
                DWord = DWord >> 12;
                sprintf(stdout, "\n\r %X", DWord);
            }
            else if (!strcmp(stdin, "STATE"))
            {
                sprintf(stdout, "\n\r%s\n\r%s\n\r%s\n\r%s", state_s[state.Supply], state_c[state.Control],
                        state_p[state.Polarity], state_m[state.Mode]);
            }
            else if (!strcmp(stdin, "NI")) // Network data         // VER20
            {
                //
                sprintf(stdout, "\n\r %X", Ni_data_word);               // VER20
            }
            /* status added for ps control */
            else if (!strcmp(stdin, "S1"))                 // STATUS 1
            {
                //
                sprintf(s1, ".....");

                switch (state.Supply)
                {
                    case (0) : s1[0] = '!'; break;   // off

                    case (1) : s1[1] = '!'; break;   // on

                    case (2) : s1[2] = '!'; break;   // onstb

                    case (3) :               // fault
                    case (4) : s1[3] = '!'; break;   // incoherence
                }

                if (!state.Control)
                {
                    s1[4] = '!';
                }

                sprintf(stdout, "%c%c......%c%c..%c...........",        //
                        s1[0], s1[4], s1[2], s1[3], s1[1]);
            }                              //
            else if (!strcmp(stdin, "S1H"))                // STATUS 1 HEXA
            {
                //
                s1h[0] = s1h[1] = s1h[2] = 0x00;

                switch (state.Supply)
                {
                    case (0) : s1h[0] = 0x8; break;  // off

                    case (1) : s1h[1] = 0x8; break;  // on

                    case (2) : s1h[2] = 0x8; break;  // onstb

                    case (3) :               // fault
                    case (4) : s1h[2] = 0x4; break;  // incoherence
                }

                if (!state.Control)
                {
                    s1h[0] |= 0x4;
                }

                sprintf(stdout, "%1X%1X%1X%1X%1X%1X",                   //
                        s1h[0], 0, s1h[2], s1h[1], 0, 0);       //
            }
            else if (!strcmp(stdin, "BAUD ch"))            // Baud rate setup read
            {
                //
                strcat(stdout, "0,9600,0,0,0,0");               //
            }
            else if (!strcmp(stdin, "VER") && (state.Control == REMOTE)) // STATUS 1
            {
                //
                sprintf(stdout, "%u", (INT16U)(VerP));              //
            }
            /*** Local Mode ***/
            else if (state.Control == LOCAL)           // string equal
            {
                for (i = 0; i < 27; i++)
                {
                    if (!strcmp(stdin, commandl[i].s))
                    {
                        cmd.value = commandl[i].cd;
                        stdin [0] = 0;
                        strcat(stdout, "R!");
                        break;
                    }
                }
            }
            /*** Serial Link com for PS Control system ***/
            else if (state.Control == REMOTE)          // string equal
            {
                for (i = 0; i < 4; i++)
                {
                    if (!strcmp(stdin, commandr[i].s))
                    {
                        cmd.value = commandr[i].cd;
                        //      strcat (stdout,"OK");
                        break;
                    }
                }
            }
            else
            {
                if (state.Control == LOCAL)
                {
                    strcat(stdout, "W!");
                }
                else
                {
                    strcat(stdout, "1");
                }
            }

        }

        /********************** End Serial Communication ********************/

        /*********************  Local Stop HIGH PRIORITY  *******************/
        if (ram_in.cmd_local.Stop == ON)
        {
            cmd.value = STOP;
        }

        if (cmd.value != NO_CMD)
        {
            OSTaskResume(CMD_TSK_PRI);
        }

        if (Flag_Irq_Network == TRUE)
        {
            Flag_Irq_Network = FALSE;     //VER20
            *Ni_data = DWord;             // ver20
        }

        ctn.running++;                            // Increment emt running counter
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: communication.c
\*---------------------------------------------------------------------------------------------------------*/
