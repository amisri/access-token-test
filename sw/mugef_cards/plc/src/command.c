/*---------------------------------------------------------------------------------------------------------*\
 File:      Command.c

 Purpose:

 Author:

 Notes:

 History:

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

/*---------------------------------------------------------------------------------------------------------*/
void CmdTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
          Command Task

Fonction   : Suivant la valeur de cmd.value controle puis active la sequence correspondante

Active par : Communication Task (CtnTsk).

Active     : Start Task (StrTsk).
             Stop Task (StpTsk).
             Polarity Task (PolTsk).
             Mode Task (MdeTsk).
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U err;



    OS_ENABLE_INTS();       // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)        // Main loop
    {
        OSTaskSuspend(OS_PRIO_SELF);


        FctSat();

        if ((state.Supply == INCOHERENCE)  || (state.Polarity == INCOHERENCE)
            || (state.Mode == INCOHERENCE) || (ram_in.digflt2.Spare1Warn == ON))
            if (cmd.value   != RESET)
            {
                cmd.value = STOP ;
            }


        switch (cmd.value)
        {

            case (STOP)  : if (state.Supply != OFF)
                {
                    OSTaskResume(STP_TSK_PRI);
                }

                break;

            case (START) : if (((state.Supply == OFF) || (state.Supply == ONSTB))
                                   && (Flag_mode == FALSE) && (Flag_polarity == FALSE))
                {
                    Flag_reset = ON;
                    UnLighted_LedFault();                        // Unlighted led Fault
                    ram_out.mode_led.Timeout = OFF;
                    Flag_FirstFault = FALSE;                     // Prepare next First Fault
                    sprintf(Tmo, tab_tmo[0]);
                    OSTaskResume(STR_TSK_PRI);
                }

                break;

            case (STB) :  if ((state.Supply == OFF) || (state.Supply == ON))
                {
                    Flag_reset = ON;
                    UnLighted_LedFault();                        // Unlighted led Fault
                    ram_out.mode_led.Timeout = OFF;
                    Flag_FirstFault = FALSE;                     // Prepare next First Fault
                    sprintf(Tmo, tab_tmo[0]);
                    OSTaskResume(OSB_TSK_PRI);
                }

                break;

            case (RESET)  : UnLighted_LedFault();
                ram_out.mode_led.Timeout = OFF;

                if (state.Supply != FAULT)
                {
                    Flag_reset = ON;
                    Flag_FirstFault = FALSE;                             // Prepare next First Fault
                    sprintf(Tmo, tab_tmo[0]);
                }

                break;

            case (POLPLUS)  : if ((state.Polarity == MINUS) && (ram_in.digflt2.Spare1Warn == OFF)
                                      && (Flag_polarity == FALSE) && (Polarity == YES))
                {
                    OSTaskResume(POL_TSK_PRI);
                }

                break;

            case (POLMOINS) :  if ((state.Polarity == PLUS) && (ram_in.digflt2.Spare1Warn == OFF)
                                       && (Flag_polarity == FALSE) && (Polarity == YES))
                {
                    OSTaskResume(POL_TSK_PRI);
                }

                break;

            case (MODE1) :     if ((state.Mode != MODE_1) && (ram_in.digflt2.Spare1Warn == OFF)
                                       && (Flag_mode == FALSE) && (Mode == YES))
                {
                    mde.value = MODE1;
                    OSTaskResume(MDE_TSK_PRI);
                }

                break;

            case (MODE2) :     if ((state.Mode != MODE_2) && (ram_in.digflt2.Spare1Warn == OFF)
                                       && (Flag_mode == FALSE) && (Mode == YES))
                {
                    mde.value = MODE2;
                    OSTaskResume(MDE_TSK_PRI);
                }

                break;

            case (MODE3) :    if ((state.Mode != MODE_3) && (ram_in.digflt2.Spare1Warn == OFF)
                                      && (Flag_mode == FALSE) && (Mode == YES))
                {
                    mde.value = MODE3;
                    OSTaskResume(MDE_TSK_PRI);
                }

                break;

            case (DCCT1)    :
            case (DCCT2)    :
            case (IREF)     :
            case (BRDGP)    :
            case (BRDGM)    :
            case (VREF)     :
            case (VFB)      :
            case (FRREFP)   :
            case (FRREFM)   :
            case (FRREF)    :
            case (ICIRC)    :
            case (VCMD)     :
            case (ICAPA)    :
            case (P5V)      :
            case (M15V)     :
            case (P15V)     : Switch_Mux((INT16U)cmd.value);
                break;

            case (NETWORK_T1) : *Ni_data = 0XAA55;             // begin VER20
                // *Ni_data = 0X55;
                break;

            case (NETWORK_T2) : *Ni_data = 0XBB11;
                // *Ni_data = 0X11;
                break;                           // end VER20

            default         : break;
        }


        cmd.value = NO_CMD;

        *out = ram_out;

        cmd.running++;          // Increment emt running counter


    }
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: command.c
\*---------------------------------------------------------------------------------------------------------*/

