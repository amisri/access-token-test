/*---------------------------------------------------------------------------------------------------------*\
 File:       Stop.c

 Purpose:

 Author:

 Notes:


\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

/*---------------------------------------------------------------------------------------------------------*/
void StpTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
          Stop Task

Fonction   : Sequence STOP du convertisseur.
                 Apres appui sur STOP PB on a :
                    - > Phase Back actif apres un delai Stop_delay.PhaseBack,
                    - > Blocked actif apres un delai Stop_delay.Blocked,
                    - > SoftStart et Reconstitued actif apres un delai Stop_delay.SoftStart,
                    - > relache du relai Start apres un delai Spd4.
              L'ordre des delais Stop_delay.PhaseBack-2-3 n'est pas defini.
              La relache du relai Start se fera toujours apres Stop_delay.PhaseBack-2-3.

Active par : Command Task (CmdTsk).

Active     : -.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U err = OS_NO_ERR;
    INT16U i;

    OS_ENABLE_INTS();       // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)
    {
        OSTaskSuspend(OS_PRIO_SELF);


        OSTaskDel(STR_TSK_PRI);                                     // Delete Start Task
        OSTaskDel(OSB_TSK_PRI);                                     // Delete Stb Task


        ram_out.cmd_direct_external.BeamDump     = ON;

        *out = ram_out;


        OSTimeDly(stop_delay[0].valeur);
        FctStp(stop_delay[0].nom);

        for (i = 1; i < MAXL; i++)
        {
            if (stop_delay[i].valeur != MAXINT)
            {
                OSTimeDly(stop_delay[i].valeur - stop_delay[i - 1].valeur);
                FctStp(stop_delay[i].nom);
            }
        }


        //  OSMboxPost (MboxModeFb,0);                                       // Mode request  *VerP8*
        err = OSTaskCreate(StrTsk, 0, &tsk_stk.str[STR_STK_SIZE - 1], STR_TSK_PRI); // Created Start task
        err = OSTaskCreate(OsbTsk, 0, &tsk_stk.osb[OSB_STK_SIZE - 1], OSB_TSK_PRI); // Created OnStb task

        stp.running++;                                // Increment Stop running counter

    }
}


void FctStp(INT8S * pt_nom)
/*---------------------------------------------------------------------------------------------------------*\
          StpIde Function

Fonction   : Active Phaseback,Block,SoftStart et Reconstitued suivant le parametre delay.

utilise par : Command Task (CmdTsk).

utilise     : -.
\*---------------------------------------------------------------------------------------------------------*/
{

    if (pt_nom == tab_stop_delay[2])
    {
        ram_out.cmd_direct_communication.PhaseBack = ON;
    }
    else if (pt_nom == tab_stop_delay[1])
    {
        ram_out.cmd_direct_communication.Block     = ON;
    }
    else if (pt_nom == tab_stop_delay[3])
    {
        ram_out.cmd_direct_communication.Soft      = ON;
    }
    else if (pt_nom == tab_stop_delay[4])
    {
        ram_out.cmd_direct_communication.Reconstitued = ON;
    }
    else if (pt_nom == tab_stop_delay[0])
    {
        if (state.Supply != OFF)
        {
            ram_out.mode_led.Timeout = ON;
            sprintf(Tmo, tab_tmo[1]);
        }
    }
    else if (pt_nom ==  tab_stop_delay[5])
    {
        ram_out.cmd_direct_relay.Start = OFF;
        ram_out.cmd_direct_external.Spare        = OFF;             // *VerP9*
    }

    *out = ram_out;
}


/*---------------------------------------------------------------------------------------------------------*\
  End of file: Stop.c
\*---------------------------------------------------------------------------------------------------------*/

