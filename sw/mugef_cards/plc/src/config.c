
/*
*  File:        Config.c
*
*  Purpose:     I/O mapping and initialise variables
*
*  History:     dd/mm/yy af
*
*/

#include <kermit.h>

void Disable_Polarity()
{
    ram_in.enable_cmd_local.PolMoins  = DISABLE;
    ram_in.enable_cmd_local.PolPlus   = DISABLE;

    ram_in.enable_fb.PolMoins     = DISABLE;
    ram_in.enable_fb.PolPlus      = DISABLE;
}

void Disable_Mode()
{
    ram_in.enable_cmd_local.Mode1     = DISABLE;
    ram_in.enable_cmd_local.Mode2     = DISABLE;
    ram_in.enable_cmd_local.Mode3     = DISABLE;

    ram_in.enable_fb.Mode1        = DISABLE;
    ram_in.enable_fb.Mode2        = DISABLE;
    ram_in.enable_fb.Mode3        = DISABLE;
}

void Disable_Master_Slave()                             //      *VerP9*
{
    //      *VerP9*
    ram_in.enable_cmr.Extcmd      = DISABLE;              //      *VerP9*

    ram_in.enable_fb.Spare1       = DISABLE;              //      *VerP9*
}



void Config()
{
    INT16U offset12, offset6, i;
    INT16U * far data_fp;

    *((INT16U *)&pc_conf) = PORTADA;      // Read Converter type from Hc16 port A



    /* **** DEFAULT VALUE *** */

    *((INT16U *)&ram_in.enable_cmr)          = REG_ENABLE;    // Enable all Faults
    *((INT16U *)&ram_in.enable_cmd_local)    = REG_ENABLE;
    *((INT16U *)&ram_in.enable_digflt1)      = REG_ENABLE;
    *((INT16U *)&ram_in.enable_digflt2)      = REG_ENABLE;
    *((INT16U *)&ram_in.enable_analogflt)    = REG_ENABLE;
    *((INT16U *)&ram_in.enable_externalflt)  = REG_ENABLE;
    *((INT16U *)&ram_in.enable_fb)           = REG_ENABLE;

    Polarity = YES;                       // Enable polarity changer
    Mode     = YES;                       // Enable mode changer
    Master   = YES;                       //              *VerP9*
    state.Polarity = PLUS;
    state.Mode     = NO_MODE;
    sprintf(Tmo, tab_tmo[0]);                 // No Timeout

    /* Hc16 program Version */
    /* VERP7   xx/xx/xxxx     Prog which had been used during 2001/2002 run       */
    /* VERP8   06/03/2002     Delete Mode post box in stop task (MBE2103 access pb)   */
    /* VERP9   01/10/2002     Add Master Slave functionality              */
    /* VERP10  08/11/2004     Add Status and Command for PS control (communication.c) */
    /*                        rewrite serial link code (sci.c)                */
    /* VERP11  26/04/2005     Allow polarity and mode changer  in MAster/SLave mode   */
    /* VERP12  05/01/2006     Add Status10 () for AuxPS/LHC adaptation            */
    /* VERP13  31/07/2006     Mcb off when pol and mode changer are enabled (MBI8160) */
    /* VERP20  03/05/2008     Version with new hardware -- MPC Network Interface -- */
    VerP = 22;

    /* Xcs30 program Version */
    /* Read constants from internal pld register */
    VerF = (INT16U)(*((INT16U *)&in->enable_digflt2)) & 0x00FF;

    /* Convertors Setup Version */
    /* Controled by  "TYPE SELECTED" user interface program */
    /* Converter type variable are stored at the end of ROM memory from 0x7200 to 0x7FFF */
    VerT = (INT16U)(*((INT16U * far)VERT));

    offset12 = (pc_conf / 12) * 12;   // Reach memory variable
    offset6  = (pc_conf / 6) * 6;


    switch (pc_conf)
    {

        case (0x96) :           // jump to program test card
            Test();
            break;

        default :               // initialise power converter
            *((INT16U *)&ram_in.enable_externalflt) = *(INT16U * far)(REG_EXT    + pc_conf) ;
            *((INT16U *)&ram_in.enable_analogflt)   = *(INT16U * far)(REG_ANALOG + pc_conf) ;
            *((INT16U *)&ram_in.enable_digflt1)     = *(INT16U * far)(REG_FLT1   + 2 * pc_conf) ;
            *((INT16U *)&ram_in.enable_digflt2)     = *(INT16U * far)(REG_FLT2   + pc_conf) ;
            *((INT16U *)&ram_in.enable_fb)          = *(INT16U * far)(REG_FB     + 2 * pc_conf) ;
            *in = ram_in;

            data_fp = (INT16U * far)(S_STOP + offset12);    // Read Stop delay

            for (i = 0; i < MAXL; i++)
            {
                stop_delay[i].valeur = *(data_fp++) ;         // Initialise local variable
                stop_delay[i].nom = tab_stop_delay[i];
            }

            data_fp = (INT16U * far)(S_START + offset12);       // Read Start delay

            for (i = 0; i < MAXL; i++)
            {
                start_delay[i].valeur = *(data_fp++) ;
                c_start_delay[i].valeur = start_delay[i].valeur ;
                start_delay[i].nom = tab_start_delay[i];
                c_start_delay[i].nom = start_delay[i].nom;
            }


            data_fp = (INT16U * far)(S_STB + offset12);     // Read StandBy delay

            for (i = 0; i < MAXL; i++)
            {
                stb_delay[i].valeur = *(data_fp++) ;
                stb_delay[i].nom = tab_stb_delay[i];
            }

            data_fp = (INT16U * far)(S_POL + offset6);      // Read Polarity delay

            for (i = 0; i < MAXS; i++)
            {
                polarity_delay[i].valeur = *(data_fp++) ;
                polarity_delay[i].nom = tab_pol_delay[i];
            }

            data_fp = (INT16U * far)(S_MODE + offset6);     // Read Mode delay

            for (i = 0; i < MAXS; i++)
            {
                mode_delay[i].valeur = *(data_fp++) ;
                mode_delay[i].nom = tab_mde_delay[i];
            }

            data_fp = (INT16U * far)(S_SPARE1 + offset12);      // Read Spare1 delay

            for (i = 0; i < MAXL; i++)
            {
                spare1_delay[i].valeur = *(data_fp++) ;
                spare1_delay[i].nom = tab_sp1_delay[i];
            }

            data_fp = (INT16U * far)(S_SPARE2 + offset6);       // Read Spare2 delay

            for (i = 0; i < MAXS; i++)
            {
                spare2_delay[i].valeur = *(data_fp++) ;
                spare2_delay[i].nom = tab_sp2_delay[i];
            }

            break;

    };


    Order(stop_delay, MAXL);          // Classifed stop    delay

    Order(start_delay, MAXL);             // Classifed start   delay

    Order(stb_delay, MAXL);           // Classifed standBy delay


    if (ram_in.enable_fb.PolMoins == DISABLE) // If Polarity status is not actived
    {
        // then disable polarity changer
        Polarity = NO;
        Disable_Polarity();
    }


    if (ram_in.enable_fb.Mode1 == DISABLE)    // If Mode status is not actived then disable Mode changer
    {
        Mode = NO;
        Disable_Mode();
    }


    if (ram_in.enable_fb.Spare1 == DISABLE)   // MasterSlave setup ?              *VerP9*
    {
        //                      *VerP9*
        Master = NO;                // NO => MasterSlave functionality disable  *VerP9*
        Disable_Master_Slave();             // and MasterSlave input disable        *VerP9*
    }                     //                      *VerP9*
    else if (spare2_delay[0].valeur == 0)     // YES but Master or Slave ?            *VerP9*
    {
        //                      *VerP9*
        Master = NO;                // Slave because no delay           *VerP9*
    }                     // spare2 setup                 *VerP9*

    ram_out.cmd_direct_communication.PhaseBack        = ON;   // Power Converter must be OFF after a Reset
    ram_out.cmd_direct_communication.Block            = ON;
    ram_out.cmd_direct_communication.Soft             = ON;
    ram_out.cmd_direct_communication.Reconstitued     = ON;
    ram_out.cmd_direct_external.BeamDump     = ON;
    ram_out.cmd_direct_relay.Start = OFF;

    *in = ram_in;

    FctSat();
    FirstFault();

    Switch_Mux(DCCT1);

    *((INT16U *)&out->mugef_status) = Status0();
    ram_out.mugef_status = out->mugef_status;

    Lighted_LedFault();

    *out = ram_out;

    if (state.Supply != FAULT)
    {
        Flag_reset = ON;
    }

}

/*  End Config.c */
