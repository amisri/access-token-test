/*
*********************************************************************************************************
*                                START UP FUNCTIONS FOR Kermit (Beta) BOARD
*
* Purpose:      This file contains the start up codes for the Hiware HC16 compiler/linker.
*               The functions initialise the HC16 registers (chip selects), zeros global
*               data areas, copies initialisation data from ROM into RAM and calls the main()
*               function.
*
* Note:         This version assumes:
*
*                   Medium memory model
*                   Code is in page 0 (CSBOOT)
*                   Default data page is F (CS0)
*                   Internal 1K RAM at F4000
*                   FPGA register are at FE800 (CS1)
*********************************************************************************************************
*/

#include <start16.h>

/* Start up data is initialised by the linker and allocated into the MY_ROM section */

struct _tagStartup _startupData;

/*
*********************************************************************************************************
*                                          STARTUP FUNCTION
*
* Description: This function is called after a reset of the HC16.
*
* Arguments  : None
*
* Returns    : Jumps direct to main()
*
* Note(s)    : 1) Starts with interrupts disabled by the linker's pre-startup code.
*              2) Assumes MEDIUM memory model and Y-based segments *NOT* in use.
*              3) Chip selects are for DigCon2.0 board.
*
*********************************************************************************************************
*/

#pragma NO_FRAME
void _Startup(void)
{
    for (;;)
    {
        asm
        {

            /* Set HC16 registers - in particular the chip selects */

            LDAB #0xF       // Select page F to access HC16 registers using EXT addressing
            TBEK

            LDE #0x0000     // watch dog disable, bm enable 64 clk, swp = , swt = 00 (0s)
            STE 0xFA20

            LDE #0x7F00     // syncr w=0, x=1 16.7 mhz
            STE 0xFA04

            LDE #0x40CF     // mcr frzsw enable, frzmb enable, mm at ffff
            STE 0xFA00

            LDE #0x8000     // disable internal ram
            STE 0xFB00

            LDE #0x00FF     // set internal ram at f4000 H
            STE 0xFB04
            LDE #0x4000
            STE 0xFB06

            LDE #0x0000     // enable internal ram
            STE 0xFB00

            LDE #0x03ff     //chipselects active : cs10,cs9,cs8,cs7,cs6 to stop alternate pin function.
            STE 0xFA46
            LDE #0x3FBa     //chipselects active : cs5,cs4,cs3,to stop alternate pin function
            STE 0xFA44      //chipselect : cs2 8bit port, cs1 16bit port, cs0 & csboot 8bit port   VER20

            LDE #0x0003     //CSBOOT (Code) a 00000,64k,as,both,rd/wr,as,1 wait
            STE 0xFA48
            LDE #0x7870
            STE 0xFA4A

            LDE #0xFF02     //CS0 (Variables RAM) a F0000,16k,as,both,rd/w,as,1 wait
            STE 0xFA4C
            LDE #0x7870
            STE 0xFA4E

            LDE #0xFFE8     //CS1 (FPGA registers) a FE800,2K,as,both,rd/w,as,1 wait
            STE 0xFA50
            LDE #0x7870
            STE 0xFA52

            LDE #0xFFE0     //CS2 (Networks registers) a FE000,2K,as,both,rd/w,as,0 wait
            STE 0xFA54
            LDE #0x5830     // upper byte  VER20
            STE 0xFA56

            LDZ   #0        // terminate dynamic link (for debugger)

            /* Zero out global variables */

            LDAB  @_startupData:PAGE         // load page of _startupData
            TBEK                             // into EK
            TST   _startupData.flags         // Test startup data flags
            BNE   ZeroOut
            LDAB  _startupData.stackOffset   // init SP
            LDS   _startupData.stackOffset:1
            TBSK
            TBZK
            ZeroOut:                             // Zero Global variable
            LDAB  _startupData.pZeroOut      // Get pointer to first descriptor
            LDY   _startupData.pZeroOut:1
            TBYK
            LDE   _startupData.nofZeroOuts   // Get 'nofZeroOuts'
            BEQ   CopyDown
            NextZeroOut:
            LDAB  0, Y                       // Get start address of block to clear
            LDX   1, Y
            TBXK
            LDD   3, Y                       // Get byte count
            AIY   #5                         // Set ptr to next descriptor
            BITB  #1
            BEQ   TestForZero
            CLR   0, X                       // ODD: clear one byte
            AIX   #1
            SUBD  #1
            TestForZero:
            TSTD
            BEQ   TestNextBlock
            NextWord:
            CLRW  0, X                       // Clear word
            AIX   #2                         // Inc pointer
            SUBD  #2                         // Decrement byte count
            BNE   NextWord
            TestNextBlock:
            SUBE  #1                         // Decrement 'nofZeroOuts'
            BNE   NextZeroOut

            /* Copy initialization data */

            CopyDown:
            LDAB  _startupData.toCopyDownBeg // Get adress of first descriptor
            LDY   _startupData.toCopyDownBeg:1
            TBYK
            NextBlock:
            LDE   0, Y                       // Load counter
            BEQ   CallMain                   // Zero terminates!
            LDAB  3, Y                       // Get destination address
            LDX   4, Y
            TBXK
            AIY   #6                         // Data is following immediately!
            Copy:
            SUBE  #2                         // Decrement byte counter
            BMI   CopyByte                   // If < 0 --> 1 byte left
            LDD   0, Y
            STD   0, X
            AIX   #2                         // Increment addresses
            AIY   #2
            TSTE
            BNE   Copy                       // Not finished yet
            BRA   NextBlock                  // Copy next block
            CopyByte:
            LDAB  0, Y                       // Copy last byte
            STAB  0, X
            AIY   #2                         // Set Y to next counter
            BRA   NextBlock                  // Copy next block

            /* Set up page registers before calling main() */

            CallMain:
            LDX   _startupData.main          // load address of main() function
            LDE   _startupData.main:2
            LDAB  _startupData.dataPage      // Setup default data page
            TBXK
            TBYK
            TBEK
            BSR  DoCall                      // call main function indirectly!

        } /* end asm */
    } /* end loop forever */

    asm
    {
        DoCall:
        ADDE #2                          // correct address for pipline
        PSHM E, X                        // push PK:PC
        RTS                              // return is call to main
    }
}

/* End of file: startupevb.c */
