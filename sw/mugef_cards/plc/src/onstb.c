/*---------------------------------------------------------------------------------------------------------*\
 File:      Onstb.c

 Purpose: Sequence STB .
                 Apres demande de STB  on a :
                   si l'etat du convertisseur est OFF
                    - > Activation du relai Start.
                   si l'etat du convertisseur est ON
                    - > SoftStart actif pendant un temps Dly,
                    - > Phase Back actif jusqu'a ZeroI actif sinon TimeOut,
                    - > Blocked actif si pas de timeOut ,
              L'etat ONSTB est actif si Le MCB est ON et Phase Back ,Block, Soft start
              ZeroI sont TRUE

 Author:

 Notes:

 History:

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

/*---------------------------------------------------------------------------------------------------------*/
void OsbTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\

\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U err = OS_NO_ERR;
    INT16U i;

    OS_ENABLE_INTS();       // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)        // Main loop - process each character received
    {
        OSTaskSuspend(OS_PRIO_SELF);

        OSTaskDel(STR_TSK_PRI);                                     // Delete Start Task

        if (state.Supply == OFF)
        {
            ram_out.cmd_direct_relay.Start = ON;                         // Do MCB On
            *out = ram_out;
        }
        else if (state.Supply == ON)
        {
            OSTimeDly(stb_delay[0].valeur);
            FctStb(stb_delay[0].nom);

            for (i = 1; i < MAXL; i++)
            {
                if (stb_delay[i].valeur != MAXINT)
                {
                    OSTimeDly(stb_delay[i].valeur - stb_delay[i - 1].valeur);

                    if (FctStb(stb_delay[i].nom)) { break; }

                }
            }
        }

        OSMboxPost(MboxOnStb, 0);

        err = OSTaskCreate(StrTsk, 0, &tsk_stk.str[STR_STK_SIZE - 1], STR_TSK_PRI); // Created Start task

        osb.running++;          // Increment emt running counter

    }
}


INT8U FctStb(INT8S  * pt_nom)
/*---------------------------------------------------------------------------------------------------------*\
          FctStb Function

Fonction   : Active Phaseback,Block,SoftStart et Reconstitued suivant le parametre delay.

utilise par : On StandBy Task (StbTsk).

utilise     : -.
\*---------------------------------------------------------------------------------------------------------*/
{
    FctSat();

    if (pt_nom == tab_stb_delay[0])
    {
        if (state.Supply != ONSTB)
        {
            ram_out.mode_led.Timeout = ON;
            sprintf(Tmo, tab_tmo[3]);
        }
    }
    else if (pt_nom == tab_stb_delay[1])
    {
        ram_out.cmd_direct_communication.Block     = ON;
    }
    else if (pt_nom == tab_stb_delay[2])
    {
        ram_out.cmd_direct_communication.PhaseBack = ON;
    }
    else if (pt_nom == tab_stb_delay[3])
    {
        ram_out.cmd_direct_communication.Soft      = ON;
    }
    else if (pt_nom == tab_stb_delay[4])
    {
        ram_out.cmd_direct_communication.Reconstitued = ON;
    }
    else if (pt_nom ==  tab_stb_delay[5])
    {
        if (state.Supply == ONSTB)
        {
            OSMboxPost(MboxOnStb, 0);
            return (TRUE);
        }
    }

    *out = ram_out;
    return (FALSE);

}


/*---------------------------------------------------------------------------------------------------------*\
  End of file: Emt.c
\*---------------------------------------------------------------------------------------------------------*/

