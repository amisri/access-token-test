
/*
*  File:        test.c
*
*  Purpose:     Function test
*
*  History:     12/15/00 af
*
*/

#include <kermit.h>

void Test()
/*---------------------------------------------------------------------------------------------------------*\
          Test Function

Fonction   : Si PORTADA est egale 0x96, cette fonction est activee.
             Voir file TEST.TXT pour plus de detail.

Utilise par : Command Task (CmdTsk).

Historic    : 21/06/02 : Oscillation sur les sorties RL dut a l'acces direct sur les registres
                         sans initialiser la structure memoire.
\*---------------------------------------------------------------------------------------------------------*/
{
    *in  = ram_in;
    ram_out = *out;


    for (;;)
    {

        ram_in = *in;

        /** Command local -> Relay controlled & Direct **/

        //       *((INT16U *)&out->cmd_direct_relay)= *((INT16U *)&in->cmd_local);
        *((INT16U *)&ram_out.cmd_direct_relay)     =  *((INT16U *)&ram_in.cmd_local);


        /** Fault digital type 1 -> Led digital type1 **/

        ram_out.digflt1_led     =  ram_in.digflt1;

        /** Fault digital type 2 -> Led digital type2 **/

        ram_out.digflt2_led     =  ram_in.digflt2;

        /** Fault analog -> Led analog **/

        ram_out.analogflt_led   =  ram_in.analogflt;

        /** Fault external -> Led external **/

        ram_out.externalflt_led =  ram_in.externalflt;


        /** Mode feedback -> Led Mode **/
        //        *((INT16U *)&out->mode_led)  =   *((INT16U *)&in->fb);
        *((INT16U *)&ram_out.mode_led) =  *((INT16U *)&ram_in.fb);

        /** Command Mugef -> Statut Mugef & Electronic Bk **/


        if (ram_in.cmr.Value == 0x01)
        {
            ram_out.mugef_status.sta0  = ON;
        }
        else { ram_out.mugef_status.sta0  = OFF; }

        if (ram_in.cmr.Value == 0x02)
        {
            ram_out.mugef_status.sta1  = ON;
        }
        else { ram_out.mugef_status.sta1  = OFF; }

        if (ram_in.cmr.Value == 0x04)
        {
            ram_out.mugef_status.sta2  = ON;
        }
        else { ram_out.mugef_status.sta2  = OFF; }

        if (ram_in.cmr.Value == 0x08)
        {
            ram_out.mugef_status.sta3  = ON;
        }
        else { ram_out.mugef_status.sta3  = OFF; }

        if (ram_in.cmr.Value == 0x10)
        {
            ram_out.mugef_status.sta4  = ON;
        }
        else { ram_out.mugef_status.sta4  = OFF; }

        if (ram_in.cmr.Value == 0x20)
        {
            ram_out.mugef_status.sta5  = ON;
        }
        else { ram_out.mugef_status.sta5  = OFF; }

        if (ram_in.cmr.Value == 0x40)
        {
            ram_out.mugef_status.sta6  = ON;
        }
        else { ram_out.mugef_status.sta6  = OFF; }

        if (ram_in.cmr.Value == 0x80)
        {
            ram_out.mugef_status.sta7  = ON;
        }
        else { ram_out.mugef_status.sta7  = OFF; }

        if (ram_in.cmr.Value == 0x81)
        {
            ram_out.mugef_status.sta8  = ON;
        }
        else { ram_out.mugef_status.sta8  = OFF; }

        if (ram_in.cmr.Value == 0x82)
        {
            ram_out.mugef_status.sta9  = ON;
        }
        else { ram_out.mugef_status.sta9  = OFF; }

        if (ram_in.cmr.Value == 0x83)
        {
            ram_out.mugef_status.sta10 = ON;
        }
        else { ram_out.mugef_status.sta10 = OFF; }

        if (ram_in.cmr.Value == 0x84)
        {
            ram_out.mugef_status.sta11 = ON;
        }
        else { ram_out.mugef_status.sta11 = OFF; }

        if (ram_in.cmr.Value == 0x85)
        {
            ram_out.mugef_status.sta12 = ON;
        }
        else { ram_out.mugef_status.sta12 = OFF; }

        if (ram_in.cmr.Value == 0x86)
        {
            ram_out.mugef_status.sta13 = ON;
        }
        else { ram_out.mugef_status.sta13 = OFF; }

        if (ram_in.cmr.Value == 0x87)
        {
            ram_out.mugef_status.sta14 = ON;
        }
        else { ram_out.mugef_status.sta14 = OFF; }

        if (ram_in.cmr.Value == 0x88)
        {
            ram_out.mugef_status.sta15 = ON;
        }
        else { ram_out.mugef_status.sta15 = OFF; }

        if (ram_in.cmr.Value == 0xf1)
        {
            ram_out.cmd_direct_communication.Bk0 = ON;
        }
        else
        {
            ram_out.cmd_direct_communication.Bk0 = OFF;
        }

        if (ram_in.cmr.Value == 0xf2)
        {
            ram_out.cmd_direct_communication.Bk1 = ON;
        }
        else
        {
            ram_out.cmd_direct_communication.Bk1 = OFF;
        }

        if (ram_in.cmr.Value == 0xf3)
        {
            ram_out.cmd_direct_communication.Bk2 = ON;
        }
        else
        {
            ram_out.cmd_direct_communication.Bk2 = OFF;
        }

        if (ram_in.cmr.Value == 0xf4)
        {
            ram_out.cmd_direct_communication.Bk3 = ON;
        }
        else
        {
            ram_out.cmd_direct_communication.Bk3 = OFF;
        }

        /** Command local Stop pb -> Communication Direct **/

        if (ram_in.cmd_local.Stop == ON)
        {
            ram_out.cmd_direct_communication.Reconstitued     = ON;
        }
        else
        {
            ram_out.cmd_direct_communication.Reconstitued     = OFF;
        }

        /** Fault digital type 1  SumADig SumBdig SumDig ->
            Command External Direct & Controlled **/

        if (ram_in.digflt1.SumADig == ON)
        {
            ram_out.cmd_direct_external.Spare     = ON;
        }
        else
        {
            ram_out.cmd_direct_external.Spare     = OFF;
        }

        if (ram_in.digflt1.SumBDig == ON)
        {
            ram_out.cmd_direct_external.BeamDump     = ON;
        }
        else
        {
            ram_out.cmd_direct_external.BeamDump     = OFF;
        }

        if (ram_in.digflt1.SumDig == ON)
        {
            ram_out.cmd_direct_external.Sum      = ON;
        }
        else
        {
            ram_out.cmd_direct_external.Sum     = OFF;
        }

        /** Command local Reset Fault -> Led Mode TimeOut & Incoherence **/
        if (ram_in.cmd_local.ResetFlt == ON)
        {
            ram_out.mode_led.Incoherence     = ON;
            ram_out.mode_led.Timeout         = ON;
        }
        else
        {
            ram_out.mode_led.Incoherence     = OFF;
            ram_out.mode_led.Timeout         = OFF;
        }

        /** Mode feedback StdConfig ZeroI McbON ->
        Command communication Direct & Controlled **/

        if (ram_in.fb.StdConfig == ON)
        {
            ram_out.cmd_direct_communication.Soft     = ON;
        }
        else
        {
            ram_out.cmd_direct_communication.Soft     = OFF;
        }

        if (ram_in.fb.ZeroI == ON)
        {
            ram_out.cmd_direct_communication.PhaseBack     = ON;
        }
        else
        {
            ram_out.cmd_direct_communication.PhaseBack     = OFF;
        }

        if (ram_in.fb.MCBON == ON)
        {
            ram_out.cmd_direct_communication.Block            = ON;
        }
        else
        {
            ram_out.cmd_direct_communication.Block            = OFF;
        }

        *out = ram_out;


    }       /* End for */

}

/* End test.c*/
