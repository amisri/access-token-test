/*---------------------------------------------------------------------------------------------------------*\
 File:      fuction.c

 Purpose:       divers

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>

/********************************************************************************************/
/*                                       Functions                                          */
/********************************************************************************************/






/******************************** Converter State Functions *********************************/

int Mcbon()                                                      // Mcb ON => Return TRUE
{
    if (ram_in.fb.MCBON == ON)
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}

int Off()                                                        // Converter Off => Return TRUE
{
    if ((ram_in.fb.MCBON                            == OFF)
        && (ram_out.cmd_direct_communication.PhaseBack == ON)
        && (ram_out.cmd_direct_communication.Block     == ON)
        && (ram_out.cmd_direct_communication.Soft      == ON))
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}

int On()                                                         // Converter On => return TRUE
{
    if (((ram_in.fb.MCBON                            == ON
         ) || (c_start_delay[5].valeur) == MAXINT)
        && (ram_out.cmd_direct_communication.PhaseBack == OFF)
        && (ram_out.cmd_direct_communication.Block     == OFF)
        && (ram_out.cmd_direct_communication.Soft      == OFF))
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}

int Onstb()                                 // Converter OnstandBy
{
    // => Return TRUE
    if ((ram_in.fb.MCBON                            == ON)
        && (ram_out.cmd_direct_communication.PhaseBack == ON)
        && (ram_out.cmd_direct_communication.Block     == ON)
        && (ram_out.cmd_direct_communication.Soft      == ON)
        && (ram_in.fb.ZeroI                            == ON))
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}

int Fault()                                 // Fault => Return TRUE
{
    if ((*((INT16U *)&ram_in.digflt1)     != REG_DISABLE)
        || (*((INT16U *)&ram_in.analogflt)   != REG_DISABLE)
        || (*((INT16U *)&ram_in.externalflt) != REG_DISABLE)
        || (ram_in.digflt2.Spare1Dig    == ON)
        || (ram_in.digflt2.Spare2Dig    == ON)
        || (ram_in.digflt2.Spare3Dig    == ON))
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}

INT8U Slave_Incoherence()                                 // Incoherence => Return TRUE   // *VerP9*
// Extcmd on but slaves off    => Incoherence                                           // *VerP9*
// Extcmd off but slaves on    => Incoherence                                           // *VerP9*
{
    // *VerP9*
    OS_TCB task_data;                                                                       // *VerP9*

    // *VerP9*
    if (((ram_in.fb.Spare1 == ON) && (ram_out.cmd_direct_external.Spare == OFF)) ||         // *VerP9*
        ((ram_in.fb.Spare1 == OFF) && (ram_out.cmd_direct_external.Spare  == ON)))         // *VerP9*
    {
        if (OSTaskQuery(STR_TSK_PRI, &task_data) == OS_NO_ERR)  // Start Task is Suspended ?  // *VerP9*
            if (task_data.OSTCBStat == OS_STAT_SUSPEND)                                         // *VerP9*
            {
                return (TRUE);    // *VerP9*
            }
    }                                                                                     // *VerP9*
    else                                                                                  // *VerP9*
    {
        return (FALSE);    // *VerP9*
    }

    // *VerP9*
}                                                                                       // *VerP9*


INT8U Supply_Incoherence()                                                // Incoherence => Return TRUE
// Relay Start OFF and Mcb ON  => Incoherence
// Relay Start ON  and Mcb OFF => Incoherence
{
    OS_TCB task_data;

    if (((Mcbon() == TRUE) && (ram_out.cmd_direct_relay.Start == OFF))
        || ((Mcbon() == FALSE) && (ram_out.cmd_direct_relay.Start == ON)))
    {
        if (OSTaskQuery(STR_TSK_PRI, &task_data) == OS_NO_ERR)        // Start Task is Suspended ?
            if (task_data.OSTCBStat == OS_STAT_SUSPEND)
            {
                return (TRUE);
            }
    }
    else
    {
        return (FALSE);
    }

}

INT8U Polarity_Incoherence()                                       // Incoherence => Return TRUE
// Polmoins Polarity and Polplus Polarity => Incoherence
// No Polmoins Polarity and No Polplus Polarity => Incoherence if No Polarity Change requested
{
    if ((Polarity == YES)
        && (((ram_in.fb.PolMoins == ON) && (ram_in.fb.PolPlus == ON))
            || ((ram_in.fb.PolMoins == OFF) && (ram_in.fb.PolPlus == OFF) && (Flag_polarity == FALSE))))
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}

INT8U Mode_Incoherence()
// Incoherence => Return TRUE
// More one active Mode  => Incoherence
// No Mode only during Mode change running else Incoherence
{
    INT8U SMode;

    SMode = ram_in.fb.Mode1 + ram_in.fb.Mode2 + ram_in.fb.Mode3;

    if ((Mode == YES) && ((SMode > TRUE) || ((SMode == FALSE) && (Flag_mode == FALSE))))
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}




void FctSat()                                           // Initialise state of converter

{
    ram_in = *in;
    ram_out.mode_led.Incoherence = OFF;

    // Master-Slave modification                                                            *VerP9*
    ram_out.mode_led.Spare1 = OFF;                       // Feedback for next converter     *VerP9*
    ram_out.cmd_direct_external.Sum = OFF;               // Fault    for next converter     *VerP9*

    /* Supply state */
    if (Supply_Incoherence() == TRUE)
    {
        state.Supply = INCOHERENCE;
        ram_out.mode_led.Incoherence = ON;
        ram_out.cmd_direct_external.Sum = ON;              // Fault    for next converter     *VerP9*
    }

    if (Fault() == TRUE)
    {
        state.Supply = FAULT;
        ram_out.cmd_direct_external.Sum = ON;              // Fault    for next converter     *VerP9*
    }
    else if (Off() == TRUE)
    {
        state.Supply = OFF;
    }
    else if (Onstb() == TRUE)
    {
        if (state.Supply != INCOHERENCE)
        {
            state.Supply = ONSTB;
        }
    }
    else if (On() == TRUE)
    {
        state.Supply = ON;
        ram_out.mode_led.Spare1 = ram_in.fb.Spare1;        // Feedback for next converter     *VerP9*
    }
    else
    {
        if (state.Supply != INCOHERENCE)
        {
            state.Supply = BUSY;
        }
    }

    /* Polarity state */
    if (Polarity == NO)
    {
        state.Polarity = NO_POLARITY;
    }
    else if (Polarity_Incoherence() == TRUE)
    {
        state.Polarity = INCOHERENCE;
        ram_out.mode_led.Incoherence = ON;
        ram_out.cmd_direct_external.Sum = ON;              // Fault    for next converter     *VerP9*
    }
    else if (ram_in.fb.PolPlus == ON)
    {
        state.Polarity = PLUS;
    }
    else if (ram_in.fb.PolMoins == ON)
    {
        state.Polarity = MINUS;
    }
    else
    {
        state.Polarity = BUSY;
    }

    ram_out.mode_led.PolPlus  = ram_in.fb.PolPlus;
    ram_out.mode_led.PolMoins = ram_in.fb.PolMoins;

    /* Mode state */
    if (Mode == NO)
    {
        state.Mode = NO_MODE;
    }
    else if (Mode_Incoherence() == TRUE)
    {
        state.Mode = INCOHERENCE;
        ram_out.mode_led.Incoherence = ON;
        ram_out.cmd_direct_external.Sum = ON;              // Fault    for next converter     *VerP9*
    }
    else if (ram_in.fb.Mode1 == ON)
    {
        state.Mode            = MODE_1;
    }
    else if (ram_in.fb.Mode2 == ON)
    {
        state.Mode            = MODE_2;
    }
    else if (ram_in.fb.Mode3 == ON)
    {
        state.Mode            = MODE_3;
    }
    else
    {
        state.Mode = BUSY;
    }

    ram_out.mode_led.Mode1 = ram_in.fb.Mode1;
    ram_out.mode_led.Mode2 = ram_in.fb.Mode2;
    ram_out.mode_led.Mode3 = ram_in.fb.Mode3;

    /* Control State */

    if (ram_in.fb.Local == ON)
    {
        state.Control          = LOCAL;
        ram_out.mode_led.Local = ON;
    }
    else
    {
        state.Control           = REMOTE;
        ram_out.mode_led.Local  = OFF;
    }


    /* Slave state */                                 //                              *VerP9*

    if (Master == NO)                           //                              *VerP9*
    {
        //                              *VerP9*
        state.Slave = NO_MASTERSLAVE;                   //                              *VerP9*
    }                                                 //                              *VerP9*
    else if (Slave_Incoherence() == TRUE)             //                              *VerP9*
    {
        state.Slave = INCOHERENCE;                      //                              *VerP9*
        ram_out.mode_led.Incoherence = ON;              //                              *VerP9*
        ram_out.cmd_direct_external.Sum = ON;           // Fault    for next converter  *VerP9*
    }                                                 //                              *VerP9*
    else if (ram_in.fb.Spare1 == ON)                  //                              *VerP9*
    {
        state.Slave            = ON;    //                              *VerP9*
    }
    else                                              //                              *VerP9*
    {
        state.Slave            = OFF;    //                              *VerP9*
    }




    if (ram_in.cmr.Value == 0x00)      // Status0 Default Value
    {
        Status0();
    }

    *out = ram_out;

}



/*******************************  Status Remote Functions  ************************************/

INT16U Status0()                    // Default Status
{
    struct Status ini;

    ini.sta0 = ON;                    // BIT0 : 380V Present

    if (state.Supply != FAULT)                // BIT1 : Internal Interlock
    {
        if (Flag_reset == ON)               // Reset executed
        {
            ini.sta1 = OFF;
        }
        else
        {
            ini.sta1 = ON;
        }
    }
    else
    {
        ini.sta1 = ON;
    }

    if (ram_in.fb.Local == FALSE)             // BIT 2 : Local / Remote
    {
        ini.sta2 = ON;
    }
    else
    {
        ini.sta2 = OFF;
    }

    if (Mcbon() == TRUE)                  // BIT 3 : Main Cut Breaker status
    {
        ini.sta3 = ON;
    }
    else
    {
        ini.sta3 = OFF;
    }

    if ((state.Polarity == PLUS) || (Polarity == NO)) // BIT 4 : Positive Polarity
    {
        ini.sta4 = ON;    // By Default : PolPlus
    }
    else
    {
        ini.sta4 = OFF;
    }

    if (state.Polarity == MINUS)              // BIT 5 : Minus Polarity
    {
        ini.sta5 = ON;
    }
    else
    {
        ini.sta5 = OFF;
    }

    ini.sta6 = ON;                    // BIT 6 : Filter Fault

    if (state.Mode == MODE_3)             // BIT 7 : Mode position 3
    {
        ini.sta7 = ON;    // MODE3 : P+E
    }
    else
    {
        ini.sta7 = OFF;
    }

    if (ram_in.externalflt.Magnet == ON)          // BIT 8 : Magnet Fault
    {
        ini.sta8 = ON;
    }
    else
    {
        ini.sta8 = OFF;
    }

    if (ram_in.externalflt.Chain == ON)           // BIT 9 : External Chain Fault
    {
        ini.sta9 = ON;
    }
    else
    {
        ini.sta9 = OFF;
    }

    if (state.Mode == MODE_2)             // BIT 10 : Mode position 2
    {
        ini.sta10 = ON;    // MODE2 : Collider
    }
    else
    {
        ini.sta10 = OFF;
    }

    if (state.Mode == MODE_1)             // BIT 11 : Mode position 1
    {
        ini.sta11 = ON;    // MODE1 : Fixe Target
    }
    else
    {
        ini.sta11 = OFF;
    }

    ini.sta12 = OFF;                  // BIT 12 : Not Used
    ini.sta13 = OFF;                  // BIT 13 : Not Used
    ini.sta14 = OFF;                  // BIT 14 : Not Used

    ini.sta15 = ON;                   // BIT 15  :New Auxps


    return (*((INT16U *)&ini));
}


INT16U Status1()                                                   // List 1 : Fault detail Digital
{

    return ((*((INT16U *)&ram_out.digflt1_led) & 0x1FFF)          // Reg_diglt1 or  Reg_diglt2
            | (*((INT16U *)&ram_out.digflt2_led) & 0xE000));

}


INT16U Status2()                                                   // List 2 : Fault Analog + External
{
    struct Status ini;

    *((INT16U *)&ini) =
        (*((INT16U *)&ram_out.analogflt_led) >> 10) | (*((INT16U *)&ram_out.externalflt_led) >> 5);


    ini.sta13  = ram_out.digflt2_led.MCBWarn;
    ini.sta14  = ram_out.digflt2_led.DCCT2Warn;
    ini.sta15  = ram_out.digflt2_led.Spare1Warn;

    return (*((INT16U *)&ini));
}

INT16U Status3()                                                    // List3: First Fault detail Digital
{
    return ((*((INT16U *)&ram_out.firstflt_digflt1) & 0x1FFF)           // Reg_diglt1 or  Reg_diglt2
            | (*((INT16U *)&ram_out.firstflt_digflt2) & 0xE000));
}


INT16U Status4()                                                 // List 4 : First Fault detail
{
    //          Analog + External +
    return (
               (*((INT16U *)&ram_out.firstflt_analog) >> 10) | (*((INT16U *)&ram_out.firstflt_external) >> 5));

}

INT16U Status5()                                             // List 5 : Mux
{
    struct Status ini;

    *((INT16U *)&ini) = REG_DISABLE;
    ini.sta0  = ram_out.cmd_direct_communication.Bk0;
    ini.sta1  = ram_out.cmd_direct_communication.Bk1;
    ini.sta2  = ram_out.cmd_direct_communication.Bk2;
    ini.sta3  = ram_out.cmd_direct_communication.Bk3;

    return (*((INT16U *)&ini));
}

INT16U Status8()                                                   // Active Fault Digital
{
    return ((*((INT16U *)&ram_in.enable_digflt1) & 0x1FFF)
            | (*((INT16U *)&ram_in.enable_digflt2) & 0xE000));
}


INT16U Status9()                                                   // Active Fault Analog + External
{
    //              + Warning
    struct Status ini;

    *((INT16U *)&ini) =
        (*((INT16U *)&ram_in.enable_analogflt) >> 10) | (*((INT16U *)&ram_in.enable_externalflt) >> 5);

    ini.sta13  = ram_in.enable_digflt2.MCBWarn;
    ini.sta14  = ram_in.enable_digflt2.DCCT2Warn;
    ini.sta15  = ram_in.enable_digflt2.Spare1Warn;

    return (*((INT16U *)&ini));
}

INT16U Status10()                   // VERP12 : LHC Status
{
    struct Status ini;

    ini.sta0 = OFF;                   // BIT0 : Not used
    ini.sta1 = OFF;                   // BIT1 : Not used
    ini.sta2 = OFF;                                       // BIT2 : Not used

    if (state.Supply != FAULT)                // BIT3 : Internal Interlock
    {

        ini.sta3 = ON;
    }
    else
    {
        ini.sta3 = OFF;
    }

    ini.sta4 = OFF;                   // BIT 4 : Not Used

    ini.sta5 = ~ram_out.digflt1_led.DCCT1Sat;     // BIT 5 : DCCT1 SAT FAULT

    ini.sta6 = OFF;                   // BIT 6 : Not Used

    if ((state.Polarity == PLUS) || (Polarity == NO)) // BIT 7 : Positive Polarity
    {
        ini.sta7 = ON;    // By Default : PolPlus
    }
    else
    {
        ini.sta7 = OFF;
    }

    if (state.Polarity == MINUS)              // BIT 8 : Minus Polarity
    {
        ini.sta8 = ON;
    }
    else
    {
        ini.sta8 = OFF;
    }

    ini.sta9 = OFF;                   // BIT 9 : Not Used

    ini.sta10 = ram_in.fb.ZeroI;              // BIT 10 : ZeroI

    ini.sta11 = OFF;                  // BIT 11 : Not Used
    ini.sta12 = OFF;                  // BIT 11 : Not Used
    ini.sta13 = OFF;                  // BIT 12 : Not Used
    ini.sta14 = OFF;                  // BIT 13 : Not Used
    ini.sta15 = OFF;                  // BIT 14 : Not Used


    return (*((INT16U *)&ini));
}

/******************************* Ligthed Led Fault Function *********************************/

void Lighted_LedFault()
{

    *((INT16U *)&ram_out.digflt1_led)     = *((INT16U *)&ram_in.digflt1)     | *((INT16U *)&ram_out.digflt1_led) ;
    *((INT16U *)&ram_out.digflt2_led)     = *((INT16U *)&ram_in.digflt2)     | *((INT16U *)&ram_out.digflt2_led) ;
    *((INT16U *)&ram_out.analogflt_led)   = *((INT16U *)&ram_in.analogflt)   | *((
                                                INT16U *)&ram_out.analogflt_led) ;
    *((INT16U *)&ram_out.externalflt_led) = *((INT16U *)&ram_in.externalflt) | *((
                                                INT16U *)&ram_out.externalflt_led) ;

}
/****************************** UnLigthed Led Fault Function ********************************/

void UnLighted_LedFault()
{
    ram_out.digflt1_led     =  ram_in.digflt1;
    ram_out.digflt2_led     =  ram_in.digflt2;
    ram_out.analogflt_led   =  ram_in.analogflt;
    ram_out.externalflt_led =  ram_in.externalflt;
}


/****************************  Initialise First Fault Function *******************************/

void FirstFault()
{
    ram_out.firstflt_digflt1  = ram_in.digflt1;
    ram_out.firstflt_digflt2.Spare1Dig = ram_in.digflt2.Spare1Dig;
    ram_out.firstflt_digflt2.Spare2Dig = ram_in.digflt2.Spare2Dig;
    ram_out.firstflt_digflt2.Spare3Dig = ram_in.digflt2.Spare3Dig;
    ram_out.firstflt_analog   = ram_in.analogflt;
    ram_out.firstflt_external = ram_in.externalflt;
}


/****************************  Functions Initialise Analog Mux      *******************************/
void Switch_Mux(INT16U Bank)
{
    INT16U Temp;

    Temp = ((Bank << 12) & 0xF000) | 0xFFF;

    *((INT16U *)&ram_out.cmd_direct_communication) =
        0xF000 | (*((INT16U *)&ram_out.cmd_direct_communication));

    *((INT16U *)&ram_out.cmd_direct_communication) =
        Temp &   *((INT16U *)&ram_out.cmd_direct_communication);

}


/****************************  Functions Initialise tab element   *******************************/
void Order(struct element * tab_init, int nb_elts)
{
    int i, j, k;
    struct element  tab_res[6];

    //determine le nombre d'elements

    for (i = 0; i < nb_elts; i++) // Pour chaque element de depart
    {
        tab_res[i].valeur = MAXINT;
    }

    // Debut du tri
    for (i = 0; i < nb_elts; i++) // pour chaque element de depart
    {
        for (j = 0; j < nb_elts; j++) // pour chaque element du tableau resultat
        {
            // si l'element du tableau initial est inferieur a celui du resultat
            if (tab_init[i].valeur <= tab_res[j].valeur)
            {
                // decalage des elements de tab_res
                for (k = (nb_elts - 1); k > j; k--)
                {
                    tab_res[k].valeur = tab_res[k - 1].valeur;
                    tab_res[k].nom    = tab_res[k - 1].nom;
                }

                // copie de la valeur de tab_init dans tab_res
                tab_res[j].valeur = tab_init[i].valeur;
                tab_res[j].nom    = tab_init[i].nom;
                break;
            }
        }
    }

    for (i = 0; i < nb_elts; i++)
    {
        tab_init[i].valeur = tab_res[i].valeur;
        tab_init[i].nom = tab_res[i].nom;
        //   strcpy(tab_init[i].nom,tab_res[i].nom);
    }

}

/****************************  Functions compare string element   *******************************/
BOOLEAN Cmp_element(INT16U nb_element)
{
    INT16U i;

    if (nb_element == MAXL)
    {
        for (i = 0; i < nb_element; i++)
        {
            if (!strcmp(stdin, start_delay[i].nom))
            {
                sprintf(stdout, "\n\r %u", (INT16U)(start_delay[i].valeur));
                return (TRUE);
            }

            else if (!strcmp(stdin, stop_delay[i].nom))
            {
                sprintf(stdout, "\n\r %u", (INT16U)(stop_delay[i].valeur));
                return (TRUE);
            }
            else if (!strcmp(stdin, stb_delay[i].nom))
            {
                sprintf(stdout, "\n\r %u", (INT16U)(stb_delay[i].valeur));
                return (TRUE);
            }
        }
    }
    else if (nb_element == MAXS)
    {
        for (i = 0; i < nb_element; i++)
        {
            if (!strcmp(stdin, mode_delay[i].nom))
            {
                sprintf(stdout, "\n\r %u", (INT16U)(mode_delay[i].valeur));
                return (TRUE);
            }

            else if (!strcmp(stdin, polarity_delay[i].nom))
            {
                sprintf(stdout, "\n\r %u", (INT16U)(polarity_delay[i].valeur));
                return (TRUE);
            }

            else if (!strcmp(stdin, spare2_delay[i].nom))
            {
                sprintf(stdout, "\n\r %u", (INT16U)(spare2_delay[i].valeur));
                return (TRUE);
            }
        }
    }

    return (FALSE);
}

/* End of file: function.c */
