/*---------------------------------------------------------------------------------------------------------*\
 File:      Flt.c

 Purpose:

 Author:

 Notes:

 History:

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

/*---------------------------------------------------------------------------------------------------------*/
void FltTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
          Fault Task

Fonction   :

Active par : Interrupt Routine Fault (IrqFlt).

Active     : -.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U err;



    OS_ENABLE_INTS();       // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)        // Main loop
    {
        //    OSTaskSuspend (OS_PRIO_SELF);

        OSSemPend(SemFault, 0, &err);

        Lighted_LedFault();

        OSTaskDel(STR_TSK_PRI);                                   // Delete Start Task

        ram_out.cmd_direct_external.BeamDump     = ON;
        FctSat();                   // Return state

        if (Flag_FirstFault == FALSE)               // FirstFault
        {
            FirstFault();
            Flag_FirstFault = TRUE;
        }



        if (Mcbon() == TRUE)
        {
            OSMboxPend(MboxMcbFb, stop_delay[MAXL].valeur, &err);
        }

        if (err == OS_TIMEOUT)                                      // Time Out Mcb Off
        {
            ram_out.mode_led.Timeout = ON;
            *out = ram_out;
            sprintf(Tmo, tab_tmo[1]);
        }



        Flag_reset = OFF;
        *out = ram_out;

        OSTaskCreate(StrTsk, 0, &tsk_stk.str[STR_STK_SIZE - 1], STR_TSK_PRI); // Created Start task

        flt.running++;          // Increment emt running counter


    }
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: Emt.c
\*---------------------------------------------------------------------------------------------------------*/

