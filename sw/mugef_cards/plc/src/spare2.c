/*---------------------------------------------------------------------------------------------------------*\
 File:          spare2.c

 Purpose:

 Author:

 Notes:

 History:

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

/*---------------------------------------------------------------------------------------------------------*/
void Sp2Tsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\

Fonction   : Spare

Active par : -.

Active     : -.

\*---------------------------------------------------------------------------------------------------------*/
{

    INT8U err;


    OS_ENABLE_INTS();     // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)          // Main loop
    {

        OSTaskSuspend(OS_PRIO_SELF);
    }
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: spare2.c
\*---------------------------------------------------------------------------------------------------------*/

