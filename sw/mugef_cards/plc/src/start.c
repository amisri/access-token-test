/*------------------------------------------------------------------------------------------------*\
 File:       Start.c

 Purpose:

 Author:

 Notes:

 History:

\*------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

/*------------------------------------------------------------------------------------------------*/
void StrTsk(void * data)
/*------------------------------------------------------------------------------------------------*\
          Start Task

Fonction   : Sequence START .
                 Apres demande de START  on a :
                    - > Activation du relai Start.
                    - > Phase Back inactif apres un delai Spd1,
                    - > Blocked inactif apres un delai Spd2,
                    - > SoftStart et Reconstitued inactif apres un delai Spd3,
              L'ordre des delais Spd1-2-3 n'est pas defini.
              La relache du relai Start se fera toujours apres Spd1-2-3.

Active par : Command Task (CmdTsk).

Historic   : 02/08/2002 : Modification liee au mode master slave (VerP8)
             Une temporisation est ajoutee au debut de la sequence.
             Ce retard permet au convertisseur slave de s'enclencher. La sequence start peut
             continuer uniquement si les convertisseurs slaves sont ON.
\*------------------------------------------------------------------------------------------------*/
{
    INT8U err = OS_NO_ERR;
    INT16U i;
    OS_TCB task_data;


    OS_ENABLE_INTS();       // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)
    {
        OSTaskSuspend(OS_PRIO_SELF);                             // Suspend current task

        Flag_start = TRUE;


        // Master-Slave modification                                                             *VerP9*
        if (Master == YES)                                         //                            *VerP9*
        {
            //                            *VerP9*
            ram_out.cmd_direct_external.Spare = ON;                  // Start slaves               *VerP9*
            *out = ram_out;                                          //                            *VerP9*
            OSMboxPend(MboxSlaveFb, spare2_delay[0].valeur, &err);   // Wait feedback slaves'on    *VerP9*
        }                                                          //                            *VerP9*


        if (err == OS_NO_ERR)                                      // feedback expected or *VerP9*
        {
            // no slave master mode *VerP9*
            // supply start sequence begin
            OSTimeDly(start_delay[0].valeur);                      // waiting first delay
            FctStd(start_delay[0].nom);                            // and execute command

            for (i = 1; i < MAXL; i++)
            {
                if (start_delay[i].valeur != MAXINT)
                {
                    OSTimeDly(start_delay[i].valeur - start_delay[i - 1].valeur);
                    FctStd(start_delay[i].nom);
                }
            }
        }                                                                //                 *VerP9*
        else                                                             //                 *VerP9*
        {
            //                 *VerP9*
            ram_out.mode_led.Timeout = ON;                               //                 *VerP9*
            ram_out.cmd_direct_external.Spare = OFF;                      //                 *VerP9*
            *out = ram_out;                                              //                 *VerP9*
            sprintf(Tmo, tab_tmo[6]);                                    //                 *VerP9*
        }                                                                //                 *VerP9*



        Flag_start = FALSE;
        str.running++;                                         // Increment Stop running counter

    }
}



void FctStd(INT8S * pt_nom)
/*---------------------------------------------------------------------------------------------------------*\
          StpIde Function

Fonction   : Active Phaseback,Block,SoftStart et Reconstitued suivant le parametre delay.

utilise par : Command Task (CmdTsk).

utilise     : -.
\*---------------------------------------------------------------------------------------------------------*/
{
    FctSat();

    if (pt_nom == tab_start_delay[2])
    {
        ram_out.cmd_direct_communication.PhaseBack = OFF;
    }
    else if (pt_nom == tab_start_delay[1])
    {
        ram_out.cmd_direct_communication.Block     = OFF;
    }
    else if (pt_nom == tab_start_delay[3])
    {
        ram_out.cmd_direct_communication.Soft      = OFF;
        ram_out.cmd_direct_external.BeamDump          = OFF;
    }
    else if (pt_nom == tab_start_delay[4])
    {
        ram_out.cmd_direct_communication.Reconstitued = OFF;
    }
    else if (pt_nom == tab_start_delay[0])
    {
        if (state.Supply != ON)
        {
            ram_out.mode_led.Timeout = ON;
            ram_out.cmd_direct_communication.Reconstitued = ON;
            ram_out.cmd_direct_communication.Soft      = ON;
            ram_out.cmd_direct_communication.Block     = ON;
            ram_out.cmd_direct_communication.PhaseBack = ON;
            ram_out.cmd_direct_relay.Start = OFF;
            sprintf(Tmo, tab_tmo[2]);
        }
    }
    else if (pt_nom ==  tab_start_delay[5])
    {
        ram_out.cmd_direct_relay.Start = ON;
    }

    *out = ram_out;
}


/*---------------------------------------------------------------------------------------------------------*\
  End of file: Start.c
\*---------------------------------------------------------------------------------------------------------*/

