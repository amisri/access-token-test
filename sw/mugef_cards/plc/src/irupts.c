/*
Interrupts program
*/

/* Include files */

#include <kermit.h>

/********************************************************************************************/
/*                                  Interrupt Functions                                     */
/********************************************************************************************/


/********************************* FPGA Interrupts Routine  *********************************/

#pragma TRAP_PROC
#pragma NO_FRAME
void IrqFlt(void)
{
    OS_INT_ENTER();

    ram_out.cmd_direct_communication.PhaseBack    = ON;        // Fast Stop
    ram_out.cmd_direct_communication.Block        = ON;
    ram_out.cmd_direct_communication.Soft         = ON;
    ram_out.cmd_direct_communication.Reconstitued = ON;
    ram_out.cmd_direct_relay.Start                = OFF;
    ram_out.cmd_direct_external.Spare        = OFF;             // *VerP9*
    ram_out.cmd_direct_external.BeamDump     = ON;
    ram_out.cmd_direct_external.Sum = ON;
    *out = ram_out;

    ram_in = *in;

    //  OSTaskResume (FLT_TSK_PRI);
    OSSemPost(SemFault);                                        // Active FltTask


    *((INT16U *)&in->digflt1)     = REG_ENABLE;               // Clear Interrupt
    *((INT16U *)&in->digflt2)     = REG_ENABLE;               // Clear Interrupt
    *((INT16U *)&in->analogflt)   = REG_ENABLE;               // Clear Interrupt
    *((INT16U *)&in->externalflt) = REG_ENABLE;               // Clear Interrupt

    OS_INT_EXIT();
}

#pragma TRAP_PROC
#pragma NO_FRAME
void IrqFdk(void)
{

    OS_INT_ENTER();

    ram_in = *in;

    if (ram_in.fb_plus.PolPlus == ON)
    {
        OSMboxPost(MboxPolFb, 0);    // POLPLUS OFF -> ON
    }

    if (ram_in.fb_plus.PolMoins == ON)
    {
        OSMboxPost(MboxPolFb, 0);    // POLMOINS OFF -> ON
    }

    if (ram_in.fb_plus.Mode1 == ON)
    {
        OSMboxPost(MboxModeFb, 0);    // MODE1 OFF -> ON
    }

    if (ram_in.fb_plus.Mode2 == ON)
    {
        OSMboxPost(MboxModeFb, 0);    // MODE2 OFF -> ON
    }

    if (ram_in.fb_plus.Mode3 == ON)
    {
        OSMboxPost(MboxModeFb, 0);    // MODE3 OFF -> ON
    }

    if (ram_in.fb_minus.MCBON == ON)
    {
        OSMboxPost(MboxMcbFb, 0);    // MCB ON -> OFF
    }

    if (ram_in.fb_plus.MCBON == ON)
    {
        OSMboxPost(MboxMcbFb, 0);    // MCB OFF -> ON
    }

    if (ram_in.fb_plus.Spare1 == ON)                               //                     *VerP9*
    {
        OSMboxPost(MboxSlaveFb, 0);    // SLAVES -> ON        *VerP9*
    }


    if ((ram_in.fb_minus.ZeroI == ON) && (((ram_in.fb.PolMoins == OFF) && (ram_in.fb.PolPlus == OFF)
                                           && (Polarity  == ON)) || ((ram_in.fb.Mode1 == OFF) && (ram_in.fb.Mode2 == OFF)
                                                   && (ram_in.fb.Mode3 == OFF) && (Mode == ON))))
    {
        ram_out.cmd_direct_communication.PhaseBack    = ON;        // Fast Stop
        ram_out.cmd_direct_communication.Block        = ON;
        ram_out.cmd_direct_communication.Soft         = ON;
        ram_out.cmd_direct_communication.Reconstitued = ON;
        ram_out.cmd_direct_relay.Start                = OFF;
        *out = ram_out;
    }

    *((INT16U *)&in->fb)          = REG_ENABLE;               // Clear Interrupt
    *((INT16U *)&in->fb_plus)     = REG_ENABLE;
    *((INT16U *)&in->fb_minus)    = REG_ENABLE;

    //  OSTaskResume (FDK_TSK_PRI);
    OSSemPost(SemFeedBack);                                        // Active FdkTask


    OS_INT_EXIT();
}

#pragma TRAP_PROC
#pragma NO_FRAME
void IrqCmd(void)
{

    OS_INT_ENTER();

    ram_in = *in;

    //OSTaskResume (CTN_TSK_PRI);
    OSSemPost(SemCtn);

    *((INT16U *)&in->cmr)         = REG_ENABLE;               // Clear Interrupt
    *((INT16U *)&in->cmd_local)   = REG_ENABLE;               // Clear Interrupt


    OS_INT_EXIT();
}



#pragma TRAP_PROC                       // BEGIN VER20
#pragma NO_FRAME
void IrqNetwork(void)
{

    OS_INT_ENTER();
    Ni_data_word = *(Ni_data);       // Get network data and release the IRQ
    Flag_Irq_Network = TRUE;
    //OSTaskResume (CTN_TSK_PRI);
    OSSemPost(SemCtn);

    OS_INT_EXIT();
}                                     // END VER20
/**************************** Timer Interrupts Routine  ********************************/

#pragma TRAP_PROC
#pragma NO_FRAME
void GptTimer2(void)
{

    OS_INT_ENTER();

    GPT_TFLG  &= oc2_msk;                                          // Clear Oc2 Interrupt
    timer2_count -= 1;                                             // Decrease to zero

    if (timer2_count == 0)
    {
        Flag_timer = TIMER2;                                         // Timer2 selected
        GPT_TMSK &= oc2_msk;                                         // Disable Oc2 Interrupt Disable
    }
    else
    {
        GPT_TOC2  = GPT_TCNT + OS_TICK_PERIOD;
    }

    OS_INT_EXIT();
}

#pragma TRAP_PROC
#pragma NO_FRAME
void GptTimer3(void)
{

    OS_INT_ENTER();

    GPT_TFLG  &= oc3_msk;                                           // Clear Oc3 Interrupt
    timer3_count -= 1;                                              // Decrease to zero

    if (timer3_count == 0)
    {
        Flag_timer = TIMER3;                                          // Timer3 selected
        GPT_TMSK &= oc3_msk;                                          // Disable Oc3 Interrupt Disable
    }
    else
    {
        GPT_TOC3  = GPT_TCNT + OS_TICK_PERIOD;
    }


    OS_INT_EXIT();

}

#pragma TRAP_PROC
#pragma NO_FRAME
void GptTimer4(void)
{

    OS_INT_ENTER();

    GPT_TFLG  &= oc4_msk;                                          // Clear Oc4 Interrupt
    timer4_count -= 1;                                             // Decrease to zero

    if (timer4_count == 0)
    {
        Flag_timer = TIMER4;                                         // Timer4 selected
        GPT_TMSK &= oc4_msk;                                         // Disable Oc4 Interrupt Disable
    }
    else
    {
        GPT_TOC4  = GPT_TCNT + OS_TICK_PERIOD;
    }
}

#pragma TRAP_PROC
#pragma NO_FRAME
void GptTimer5(void)
{

    OS_INT_ENTER();

    GPT_TFLG  &= oc5_msk;                     // Clear Oc5 Interrupt
    timer5_count -= 1;                                             // Decrease to zero

    if (timer5_count == 0)
    {
        Flag_timer = TIMER5;                                         // Timer5 selected
        GPT_TMSK &= oc5_msk;                                         // Disable Oc5 Interrupt Disable
    }
    else
    {
        GPT_TI4O5  = GPT_TCNT + OS_TICK_PERIOD;
    }

    OS_INT_EXIT();
}


#pragma TRAP_PROC
#pragma NO_FRAME
/*---------------------------------------------------------------------------------------------------------*/
void IrqSci(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the ISR for the sci receiver interrupt. It reads the
  received character and sends it to the Rx Task via a message queue.
\*---------------------------------------------------------------------------------------------------------*/
{

    OS_INT_ENTER();

    Data_Serial_Register  = QSM_SCSR;                 // Read received data (+clear RDRF)
    Data_Serial_Register  = QSM_SCDR;

    OSQPost(MsgQRx, (void *)Data_Serial_Register);     // send data to sci task

    OS_INT_EXIT();
}


#pragma TRAP_PROC
#pragma NO_FRAME
void IrqQspi(void)
{
    OS_INT_ENTER();

    Data_Serial_Tx = QSM_SCSR;
    Data_Serial_Tx = QSM_SCDR;

    OS_INT_EXIT();
}
/***************************************** System ***************************************/

#pragma TRAP_PROC
#pragma NO_FRAME
void BusError(void)
{
    OS_INT_ENTER();

    n_buserr++;

    OS_INT_EXIT();
}

#pragma TRAP_PROC
#pragma NO_FRAME
void SpuriousInt(void)
{
    OS_INT_ENTER();

    n_spuriousint++;

    OS_INT_EXIT();
}

/* End of file: irupts.c */
