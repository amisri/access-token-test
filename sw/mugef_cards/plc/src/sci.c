/*---------------------------------------------------------------------------------------------------------*\
 File:      sci.c

 Purpose:       Serial Communication Interface

 Author:

 Notes:

 History:      01/01/2000 : created
               02/12/2004 : add C83 lib VT100/ANSI terminal support functions (qak)

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

static INT16U(* const edit_func[])(INT16U) =        // Line editor functions
{
    TermLE0,                        // State 0 function (Idle)
    TermLE1,                        // State 1 function (Line editor)
    TermLE2,                        // State 2 function (ESC pressed)
    TermLE3,                        // State 3 function (Cursor key or function key)
    TermLE4,                        // State 4 function (Function key)
    TermLE5,                        // State 5 function (PF1-PF4)
};

/*---------------------------------------------------------------------------------------------------------*/
void RxTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
          Serial Communication Interface Receiver Task

Fonction   : Gestion des caracteres contenus dans stdin.
             Si liaison serie utilise avec un terminal VT100, les touches backspace et carriage
             return sont gerees.

Active par : Interrupt Routine Sci (IrqSci).

Active     : Communication Task (CtnTsk).
             Serial Transmitter Task (SctTsk).
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U ch, err;
    static INT16U g;
    static FP32   gf;

    OS_ENABLE_INTS();       // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)        // Main loop - process each character received
    {
        ch = (INT8U) OSQPend(MsgQRx, 0, &err);      // waiting for a message from uart

        term.edit_state = edit_func[term.edit_state](ch);   // state machine to interpret character

        OSSemPost(SemTx);               // print message

        sci.running++;      // Increment sci running counter
    }
}

/*---------------------------------------------------------------------------------------------------------*/
void TxTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
       Serial Communication Interface Transmitter Task

fonction   : Gestion de la liaison serie en mode transmition.
             Ecrit dans registre de transmition (SCDR) les caracteres contenues
             dans stdout .

Active par : Tache Communication (CtnTsk).

Active     : -.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U err;
    INT16U i;

    OS_ENABLE_INTS();       // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)        // Main loop - process each character received
    {
        OSSemPend(SemTx, 0, &err);      // Wake up when a character string is waiting to be sent

        i = 0;

        while (stdout[i])
        {
            *scdr = stdout [i++];

            while (scsr->TC == OFF) {};  // wait SCI transmitter idle
        }

        stdout [0] = 0x00;

        sct.running++;          // Increment sci running counter

    }
}

/*---------------------------------------------------------------------------------------------------------*/
void TermInitLineEditor(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE0() if ESC is pressed once, or from TermLE2() if ECS is pressed for
  a second time.  The function initialises the ANSI/VT100 terminal connected to UART0.

  Set up string uses the following ANSI/VT100 escape codes (ESC='\33'):

     ESC c      Reset terminal to default settings
     ESC [?7h       Enable line wrap
\*---------------------------------------------------------------------------------------------------------*/
{

    term.edit_state = 1;                // Enable line editor

    term.line_idx = 0;                  // Reset line buffer cursor index
    term.line_end = 0;                  // Reset line buffer end index

    sprintf(stdout, "\33c\33[?7h\aSPSPLC VerP:%d VerF:%d VerT:%d\n\r>", VerP, VerF, VerT);
}

/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE0(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 0. In this state, output to the terminal is
  suppressed and the only character that is recognised is ESC, which starts the line editor.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (ch == 0x1B)         // If character is ESC
    {
        TermInitLineEditor();           // Reset line editor and display the prompt
        return (1);             // Start line editor
    }

    return (0);
}

/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE1(INT8U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 1. In this state, the character is analysed
  directly.  If the ESC code (0x1B) is received, the state changes to 1, otherwise the character is
  processed and the state remains 0.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)                 // Switch according to the key pressed
    {
        case 0x08:                      // [Backspace]  Delete left

        case 0x7F:  TermDeleteLeft();   break;      // [Delete] Delete left

        case 0x0D:  TermSendCmd();      break;      // [Return] Send command

        case 0x1A:  return (0);      break;     // [CTRL-Z] Line editor off

        case 0x1B:  return (2);      break;     // [ESC]    Start escape sequence

        default:    TermInsertChar(ch); break;      // [others] Insert character
    }

    return (1);                 // Continue with edit state 1
}

/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE2(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 2. The previous character was [ESC].
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)                 // Switch according to next code
    {
        case 0x1B: TermInitLineEditor();    return (1); // [ESC]    Restart editor

        case 0x5B:              return (3); // [Cursor/Fxx] Change state to analyse

        case 0x4F:              return (5); // [PF1-4]  Change state to ignore
    }

    return (1);                 // All other characters - return to edit state 1
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE3(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 3. The key was either a cursor key or a function
  key.  Cursors keys have the code sequence "ESC[A" to "ESC[D", while function keys have the code
  sequence "ESC[???~" where ??? is a variable number of alternative codes.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)                 // Switch according to next code
    {
        case 0x41:              return (1); // [Up]     Previous history line

        case 0x42:              return (1); // [Down]   Next history line

        case 0x43:  TermCursorRight();  return (1); // [Right]  Move cursor right

        case 0x44:  TermCursorLeft();   return (1); // [Left]   Move cursor left
    }

    return (4);                 // Function key - change to edit state 4
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE4(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 4.  Orignial Key was a Function with sequence
  terminated by 0x7E (~).  However, the function will also accept a new [ESC] to allow an escape route
  in case of corrupted reception.
\*---------------------------------------------------------------------------------------------------------*/
{
    switch (ch)                 // Switch according to next code
    {
        case 0x1B:              return (1); // [ESC]    Escape to edit state 1

        case 0x7E:              return (1); // [~]      Sequence complete - return to state 1
    }

    return (4);                 // No change to edit state
}
/*---------------------------------------------------------------------------------------------------------*/
INT16U TermLE5(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called when the line editor state is 5.  The original key was PF1-4 and one more
  character must be ignored.
\*---------------------------------------------------------------------------------------------------------*/
{
    return (1);                 // Return to edit state 1
}
/*---------------------------------------------------------------------------------------------------------*/
void TermCursorLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE3() if the Cursor left key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (!term.line_idx)         // If cursor is already at the start of the line
    {
        sprintf(stdout, "\a");             // Just beep
    }
    else                // else
    {
        sprintf(stdout, "\b");             // Move cursor left one character
        term.line_idx--;               // Decrement cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermCursorRight(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE3() if the Cursor right key has been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    if (term.line_idx == term.line_end)     // If cursor is already at the end of the line
    {
        sprintf(stdout, "\a");             // Just beep
    }
    else                    // else
    {
        sprintf(stdout, "\33[C");       // Move cursor right one character
        term.line_idx++;            // Increment cursor index in line
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermInsertChar(INT16U ch)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if a standard character has been received.  It will try to enter
  the character into the current line under the current cursor position.  The rest of the line will be
  moved to the right to may space for the new character.  If the line is full, the bell will ring.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  i;

    if (ch >= 0x20 && ch < 0x80)        // If character is printable 7-bit ASCII symbol
    {
        if (term.line_end >= (SRMAX - 2)) // If line buffer is full...
        {
            sprintf(stdout, "\a");              // Just beep
        }
        else                    // else
        {

            for (i = term.line_end++; i > term.line_idx; i--) // For the rest of the line (backwards)
            {
                stdin[i] = stdin[i - 1];    // Shift characters one column in buffer
            }

            stdin[term.line_idx ++] = ch;       // Save new character in line buffer

            stdin[term.line_end] = '\0';        // Nul terminate line buffer

            if ((i = term.line_end - term.line_idx)) // If cursor is now offset from true position
            {
                sprintf(stdout, "%s\33[%uD", &stdin[term.line_idx - 1],
                        i);             // Move cursor the required number of columns left
            }
            else if (state.Control == LOCAL)        // echo disable for remote control
            {
                sprintf(stdout, "%c", ch);      // Output chars to screen
            }
        }
    }
}
/*---------------------------------------------------------------------------------------------------------*/
void TermSendCmd(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if Enter or Return been received.  It will send the command to
  be executed by the communication task.
\*---------------------------------------------------------------------------------------------------------*/
{
    stdin[term.line_end] = '\0';            // Nul terminate line buffer

    //OSTaskResume (CTN_TSK_PRI);           // Send command to com task
    OSSemPost(SemCtn);

    term.line_idx = term.line_end = 0;      // Reset cursor and end of line indexes

    strcat(stdout, "\n\r");

}

/*---------------------------------------------------------------------------------------------------------*/
void TermDeleteLeft(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called from TermLE1() if the backspace or delete keys have been pressed.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT16U  i;

    if (!term.line_idx)             // If cursor is already at the start of the line
    {
        sprintf(stdout, "\a");          // Just beep
    }
    else                    // else
    {
        term.line_idx--;            // Adjust cursor position index

        for (i = term.line_idx; i < term.line_end; i++) // For the remainder of the line
        {
            stdin[i] = stdin[i + 1];        // Shift character in buffer
        }

        stdin[term.line_end--] = '\0';      // Nul terminate line buffer

        sprintf(stdout, "\b%s \33[%uD", &stdin[term.line_idx],
                (1 + term.line_end - term.line_idx)); // Move cursor the required number of columns left
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: Sci.c
\*---------------------------------------------------------------------------------------------------------*/

