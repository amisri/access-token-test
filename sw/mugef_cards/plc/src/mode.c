/*---------------------------------------------------------------------------------------------------------*\
 File:      mode.c

 Purpose:

 Author:

 Notes:

 History:

\*---------------------------------------------------------------------------------------------------------*/

#include <kermit.h>         // Include all header files

/*---------------------------------------------------------------------------------------------------------*/
void MdeTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\

\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U err = OS_NO_ERR;
    INT16U SaveContextSupply;

    OS_ENABLE_INTS();     // All tasks start with interrupts disabled

    /* Task loop */

    for (;;)          // Main loop -
    {

        OSTaskSuspend(OS_PRIO_SELF);                                  // Suspend current task

        Flag_mode = TRUE;

        SaveContextSupply = state.Supply;                            // 1. Save Supply State


        // Master-Slave modification                                        *VerP11*
        if (Master == YES)                      //              *VerP11*
        {
            //              *VerP11*
            ram_out.cmd_direct_external.Spare = OFF;          //  Stop slaves     *VerP11*
            *out = ram_out;                       //              *VerP11*
        }

        if (state.Supply == ON)                                      // 2. If Supply ON done ONSTB
        {
            OSTaskResume(OSB_TSK_PRI);
            OSMboxPend(MboxOnStb, 50000, &err);
            FctSat();
        }

        if ((state.Supply != ON) && (ram_in.fb.ZeroI == ON))         // 3. If state is ok
        {
            //    done Mode1,Mode2 or Mode3 relay

            if ((Mode == YES) && (Polarity == YES))         // Simplify mode and polarity   *VerP13*
            {
                // change for MBI8160M      *VerP13*
                ram_out.cmd_direct_relay.Start = OFF;
            }

            if (mde.value == MODE1)
            {
                ram_out.cmd_direct_relay.Mode1 = ON;
            }
            else if (mde.value == MODE2)
            {
                ram_out.cmd_direct_relay.Mode2 = ON;
            }
            else if (mde.value == MODE3)
            {
                ram_out.cmd_direct_relay.Mode3 = ON;
            }

            *out = ram_out;

            OSMboxPend(MboxModeFb, mode_delay[MAXS - 3].valeur, &err);  // Wait feedback mode


            ram_out.cmd_direct_relay.Mode1 = OFF;               // Mode Relay Off
            ram_out.cmd_direct_relay.Mode2 = OFF;
            ram_out.cmd_direct_relay.Mode3 = OFF;
            *out = ram_out;

            if (err == OS_TIMEOUT) { sprintf(Tmo, tab_tmo[5]); }

            OSTimeDly(mode_delay[MAXS - 2].valeur);                     // 6. Wait Status delay
        }

        if (err == OS_TIMEOUT)
        {
            ram_out.mode_led.Timeout = ON;
            *out = ram_out;
        }

        if ((err == OS_NO_ERR) && (SaveContextSupply == ON))         // 7. Return state
        {
            OSTaskDel(OSB_TSK_PRI);
            err = OSTaskCreate(OsbTsk, 0, &tsk_stk.osb[OSB_STK_SIZE - 1], OSB_TSK_PRI);
            err = OSTaskCreate(StrTsk, 0, &tsk_stk.str[STR_STK_SIZE - 1], STR_TSK_PRI);

            if (state.Supply == ONSTB)
            {
                OSTaskResume(STR_TSK_PRI);
            }
        }

        err = OS_NO_ERR;
        Flag_mode = FALSE;                                       // End Sequence mde
        mde.running++;                                       // Increment mde running counter

    }
}

/*---------------------------------------------------------------------------------------------------------*\
  End of file: polarity.c
\*---------------------------------------------------------------------------------------------------------*/

