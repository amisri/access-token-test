/*---------------------------------------------------------------------------------------------------------*\
 File:      main.c

 Purpose:       Set globale variable
                start all OStask


 Historic :     Ver 5 : First Version.
                Ver 6 : Bdump added.
                Ver 7 : AOB.
                Ver 8 : Master Slave Modification
                        ExtSpare Fault has become Extcmd remote command ( VerF = 3)


\*---------------------------------------------------------------------------------------------------------*/

#define KER_GLOBALS

#include <kermit.h>             // Include all header files

/*---------------------------------------------------------------------------------------------------------*/
void main(void)
/*---------------------------------------------------------------------------------------------------------*\
  This function is called by startup() and is the function that does all the initialisation duties:
  This function never exits, so "local" variables are static to save stack space.
\*---------------------------------------------------------------------------------------------------------*/
{
    INT8U err;

    /** Initialisation des variables globales **/

    /* Pointeur sur les registres du FPGA */
    out = (struct lca_output *)ADDRESS_LCA_OUTPUT;
    in  = (struct lca_input *) ADDRESS_LCA_INPUT;

    Ni_data = (INT16U *)ADDRESS_NETWORK_INPUT;
    Ni_ds   = (INT16U *)ADDRESS_NETWORK_DS;

    /* SCI Status register pointer */
    scsr = (struct Scsr *)0xFFC0C;               // SCI Status Register
    scdr = (INT8U *)0xFFC0F;                     // SCI Data Register
    term.line_idx = 0;
    term.line_end = 0;
    term.edit_state = 1;

    /* variables programme */

    Flag_FirstFault = FALSE;
    Flag_polarity   = FALSE;
    Flag_reset      = OFF;
    Flag_mode       = FALSE;


    /** Initialisation des peripheriques internes et externes **/

    /* PWM Generator */
    Init_pwm();                             // PWMB = 1Khz  Digital Filter

    /* GPT Timer */
    Init_timer();


    /* QSM  */
    Init_serial_communication_interface() ;


    Init_external_interrupt();              // Enable interrupt from FPGA

    /** Initialise uC/OS-II **/

    OSInit();

    /** Creation des taches **/

    OSTaskCreate(MstTsk, 0, &tsk_stk.mst[MST_STK_SIZE - 1], MST_TSK_PRI);     //  0. Millisecond task
    OSTaskCreate(FltTsk, 0, &tsk_stk.flt[FLT_STK_SIZE - 1], FLT_TSK_PRI);     //  1. Fault task
    OSTaskCreate(Sp1Tsk, 0, &tsk_stk.sp1[SP1_STK_SIZE - 1], SP1_TSK_PRI);     //  2. Spare1 task
    OSTaskCreate(FdkTsk, 0, &tsk_stk.fdk[FDK_STK_SIZE - 1], FDK_TSK_PRI);     //  3. FeedBack task
    OSTaskCreate(PolTsk, 0, &tsk_stk.pol[POL_STK_SIZE - 1], POL_TSK_PRI);     //  4. Polarity task
    OSTaskCreate(StpTsk, 0, &tsk_stk.stp[STP_STK_SIZE - 1], STP_TSK_PRI);     //  5. Stop task
    OSTaskCreate(MdeTsk, 0, &tsk_stk.mde[MDE_STK_SIZE - 1], MDE_TSK_PRI);     //  6. Mode task
    OSTaskCreate(CmdTsk, 0, &tsk_stk.cmd[CMD_STK_SIZE - 1], CMD_TSK_PRI);     //  7. Command task
    OSTaskCreate(CtnTsk, 0, &tsk_stk.ctn[CTN_STK_SIZE - 1], CTN_TSK_PRI);     //  8. communication task
    OSTaskCreate(OsbTsk, 0, &tsk_stk.osb[OSB_STK_SIZE - 1], OSB_TSK_PRI);     //  9. OnStb task
    OSTaskCreate(StrTsk, 0, &tsk_stk.str[STR_STK_SIZE - 1], STR_TSK_PRI);     //  10. Start task
    OSTaskCreate(Sp2Tsk, 0, &tsk_stk.sp2[SP2_STK_SIZE - 1], SP2_TSK_PRI);     //  11. Spare2 task
    OSTaskCreate(RxTsk, 0, &tsk_stk.sci[SCI_STK_SIZE - 1], SCI_TSK_PRI);     //  12. SCI task
    OSTaskCreate(TxTsk, 0, &tsk_stk.sct[SCT_STK_SIZE - 1], SCT_TSK_PRI);     //  13. SCT task


    /** Creation des MailBox **/
    MboxPolFb    = OSMboxCreate(0);        // FeedBack Polarity
    MboxModeFb   = OSMboxCreate(0);        // FeedBack Mode
    MboxMcbFb    = OSMboxCreate(0);        // FeedBack Mcb
    MboxOnStb    = OSMboxCreate(0);        // OnStb Task
    MboxSlaveFb  = OSMboxCreate(0);        // FeedBack Slave    *VerP9*

    /** Creation des Semaphores **/
    SemFault    = OSSemCreate(0);       // Synchronise fault irq and fault task
    SemFeedBack = OSSemCreate(0);       // Synchronise feedback irq and feeback task
    SemTx   = OSSemCreate(0);       // Synchronise Sci task and sct task
    SemCtn =  OSSemCreate(0);       // Synchronisation for communication task

    /** Creating message queue **/
    MsgQRx      = OSQCreate(&msg[0], 10);

    /* NVSRAM  Read/Write */
    Nvsram();

    /* Converter configuration */
    Config();

    /* Start multitasking (calls highest priority task first) */

    OSStart();
}
/*---------------------------------------------------------------------------------------------------------*/
void MstTsk(void * data)
/*---------------------------------------------------------------------------------------------------------*\
  This function is the Millisecond Task.  It waits for the Idle Task to start before enabling GPT
  interrrupts.  The main loop is controlled by ISRmst via a TaskResume() call.

  This function never exits, so "local" variables are static to save stack space.
\*---------------------------------------------------------------------------------------------------------*/
{
    /* Wait for other task to initialise */

    /* Start GPT timer */

    GPT_TOC1  = GPT_TCNT + OS_TICK_PERIOD;                       // Comparator1 = Counter + Freq.
    GPT_TMSK  |=   0x0800;                                       // Timer1 Interrupt Enable

    OS_ENABLE_INTS();           // Enable all interrupts



    /* Main Loop - synchronised to ms interrupts */

    for (;;)
    {

        OSTimeDly(1);              // Wait for next OS Tick

    }
}


/*
*********************************************************************************************************
*                                              IDLE TASK
*
* Description: This task is internal to uC/OS-II and executes whenever no other higher priority tasks
*              executes because they are waiting for event(s) to occur.
*
* Arguments  : none
*
* Returns    : none
*********************************************************************************************************
*/

void OSTaskIdle(void * pdata)
{
    INT8U DBPol = OFF;
    INT8U DBMod = OFF;
    INT16U mem1, mem2;

    mem1 = mem2 = 0;
    pdata = pdata;                              /* Prevent compiler warning for not using 'pdata'     */
    OS_ENABLE_INTS();               /* Essential - all tasks start with interrupts disabled */

    for (;;)
    {
        OS_ENTER_CRITICAL();

        /* Read FPGA Register */
        ram_in = *in;

        /* StatusO must be the default Value on MUGEF Status */
        if (ram_in.cmr.Value == 0x00)                // No more MUGEF command
        {
            mem1 = *((INT16U *)&ram_out.mugef_status) = Status0();
        }

        if (mem1 != mem2)                        // VER20
        {
            mem2 = *Ni_ds = mem1;                 //Default status VER20
        }

        /* Warning */
        *((INT16U *)&ram_out.digflt2_led) = *((INT16U *)&ram_in.digflt2) | *((INT16U *)&ram_out.digflt2_led);

        /* Autorise de manipuler le changement de polarite en cas d'incoherence.
           Appui sur bouton = Relai actif jusqu'a polarite trouvee              */
        if ((state.Polarity == INCOHERENCE) && (ram_in.fb.ZeroI == ON))
        {
            DBPol = ON;

            if (ram_in.cmd_local.PolPlus == ON) { ram_out.cmd_direct_relay.PolPlus = ON; }
            else { ram_out.cmd_direct_relay.PolPlus = OFF; }

            if (ram_in.cmd_local.PolMoins == ON) { ram_out.cmd_direct_relay.PolMoins = ON; }
            else { ram_out.cmd_direct_relay.PolMoins = OFF; }
        }
        else if (DBPol == ON)
        {
            ram_out.cmd_direct_relay.PolPlus = OFF;
            ram_out.cmd_direct_relay.PolMoins = OFF;
            DBPol = OFF;
        }

        /* Autorise de manipuler le changement de polarite en cas d'incoherence.
           Appui sur bouton = Relai actif jusqu'a polarite trouvee              */
        if ((state.Mode == INCOHERENCE) && (ram_in.fb.ZeroI == ON))
        {
            DBMod = ON;

            if (ram_in.cmd_local.Mode1 == ON) { ram_out.cmd_direct_relay.Mode1 = ON; }
            else { ram_out.cmd_direct_relay.Mode1 = OFF; }

            if (ram_in.cmd_local.Mode2 == ON) { ram_out.cmd_direct_relay.Mode2 = ON; }
            else { ram_out.cmd_direct_relay.Mode2 = OFF; }

            if (ram_in.cmd_local.Mode3 == ON) { ram_out.cmd_direct_relay.Mode3 = ON; }
            else { ram_out.cmd_direct_relay.Mode3 = OFF; }
        }
        else if (DBMod == ON)
        {
            ram_out.cmd_direct_relay.Mode1 = OFF;
            ram_out.cmd_direct_relay.Mode2 = OFF;
            ram_out.cmd_direct_relay.Mode3 = OFF;
            DBMod = OFF;
        }


        *out = ram_out;
        FctSat();

        OSIdleCtr++;
        OS_EXIT_CRITICAL();
    }
}
/*---------------------------------------------------------------------------------------------------------*\
  End of file: main.c
\*---------------------------------------------------------------------------------------------------------*/

