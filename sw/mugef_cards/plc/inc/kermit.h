/*
*  File:        Kermit.h
*
*  Purpose:     Define interface structures for Kermit controller
*
*  History:     09/06/99 af
*
*/



/*----------------------------------------------------------------------------*\
                   Include Files
\*----------------------------------------------------------------------------*/

#ifdef KER_GLOBALS
#define KER_EXT
#else
#define KER_EXT extern
#endif
/*----- Include all other header files -----*/

#include <stdio.h>              // Include ANSI header files
#include <stdlib.h>
#include <string.h>


#include <includes.h>                           // Operating Systeme MicroC

#include <adc_hc16.h>                           // HC16 Structure
#include <hc16.h>

#include <consts.h>             // Include global constant definitions
#include <term.h>
/*----------------------------------------------------------------------------*\
                Definition des structures
\*----------------------------------------------------------------------------*/

/* State structure */

struct State
{
    INT8U  Supply ;
    INT8U  Polarity;
    INT8U  Mode;
    INT8U  Slave;
    INT8U  Control;
};


typedef struct element
{
    unsigned int valeur;
    INT8S  * nom;
};

/** Field bit  **/


/* Detailed structure */
struct Remote
{
    INT16U dummy8to14   : 7;
    INT16U Extcmd       : 1;            // Extcmd added *VerP9*

    INT16U Value    : 8;
};

struct Local
{
    /* Address byte fe811 */
    INT16U Mode1    : 1;            /* LSB = 8 */
    INT16U Mode2    : 1;
    INT16U Mode3    : 1;
    INT16U dummy11  : 1;
    INT16U dummy12  : 1;
    INT16U PolMoins : 1;
    INT16U PolPlus  : 1;
    INT16U dummy15  : 1;        /* MSB = 15 */
    /* Address byte fe810 */
    INT16U Stop     : 1;            /* LSB = 0 */
    INT16U Start    : 1;
    INT16U dummy2   : 1;
    INT16U dummy3   : 1;
    INT16U dummy4   : 1;
    INT16U ResetFlt : 1;
    INT16U Spare1   : 1;
    INT16U Spare2   : 1;            /* MSB = 7 */
};

struct Digital1
{
    INT16U ThyrFuses   : 1;
    INT16U AuxFuses    : 1;
    INT16U DCCT1Sat    : 1;
    INT16U V5V15       : 1;
    INT16U MCB         : 1;
    INT16U SumBDig     : 1;
    INT16U SumADig     : 1;
    INT16U SumDig      : 1;

    INT16U FastStop    : 1;
    INT16U Doors       : 1;
    INT16U Water       : 1;
    INT16U FilterCapa  : 1;
    INT16U Suppress    : 1;
    INT16U ChokeTemp   : 1;
    INT16U BridgeTemp  : 1;
    INT16U TransfoTemp : 1;
};

struct Digital2
{
    INT16U dummy8     : 1;
    INT16U dummy9     : 1;
    INT16U Spare1Warn : 1;
    INT16U DCCT2Warn  : 1;
    INT16U MCBWarn    : 1;
    INT16U Spare1Dig  : 1;
    INT16U Spare2Dig  : 1;
    INT16U Spare3Dig  : 1;

    INT16U dummy0to7  : 8;
};

struct Analog
{
    INT16U dummy8       : 1;
    INT16U dummy9       : 1;
    INT16U OverUDC  : 1;
    INT16U OverIAC  : 1;
    INT16U OverRipple   : 1;
    INT16U OverIRMS : 1;
    INT16U OverIDC  : 1;
    INT16U EarthLeak    : 1;

    INT16U dummy0to7    : 8;
};


struct External
{
    INT16U dummy8       : 1;
    INT16U dummy9       : 1;
    INT16U dummy10      : 1;
    //    INT16U Spare  : 1;
    INT16U dummy11      : 1;    //Extspare fault deleted *VerP9*
    INT16U dummy12      : 1;
    INT16U Magnet   : 1;
    INT16U Chain    : 1;
    INT16U Sum      : 1;

    INT16U dummy0to7    : 8;
};

struct Feedback
{
    INT16U Mode1    : 1;
    INT16U Mode2    : 1;
    INT16U Mode3    : 1;
    INT16U ZeroI    : 1;
    INT16U MCBON    : 1;
    INT16U PolMoins : 1;
    INT16U PolPlus  : 1;
    INT16U Local    : 1;

    INT16U dummy0to2    : 3;
    INT16U StdConfig    : 1;
    INT16U dummy4to5    : 2;
    INT16U Spare1   : 1;
    INT16U Spare2   : 1;
};

struct Status
{
    INT16U sta8     : 1;
    INT16U sta9     : 1;
    INT16U sta10    : 1;
    INT16U sta11    : 1;
    INT16U sta12    : 1;
    INT16U sta13    : 1;
    INT16U sta14    : 1;
    INT16U sta15    : 1;

    INT16U sta0     : 1;
    INT16U sta1     : 1;
    INT16U sta2     : 1;
    INT16U sta3     : 1;
    INT16U sta4     : 1;
    INT16U sta5     : 1;
    INT16U sta6     : 1;
    INT16U sta7     : 1;
};


struct Mode
{
    INT16U Mode1    : 1;
    INT16U Mode2    : 1;
    INT16U Mode3    : 1;
    INT16U dummy11to12  : 2;
    INT16U PolMoins : 1;
    INT16U PolPlus  : 1;
    INT16U Local    : 1;

    INT16U Incoherence  : 1;
    INT16U Timeout      : 1;
    INT16U dummy2to5    : 4;
    INT16U Spare1   : 1;
    INT16U Spare2   : 1;
};

struct Relay
{
    INT16U Mode1    : 1;
    INT16U Mode2    : 1;
    INT16U Mode3    : 1;
    INT16U dummt11to12  : 2;
    INT16U PolMoins : 1;
    INT16U PolPlus  : 1;
    INT16U dummy15  : 1;

    INT16U dummy0   : 1;
    INT16U Start    : 1;
    INT16U dummt2to5    : 4;
    INT16U Spare1   : 1;
    INT16U Spare2   : 1;
};

struct Externalc
{
    INT16U dummy8to10   : 3;
    INT16U Spare    : 1;
    INT16U BeamDump : 1;
    INT16U dummt13to14  : 2;
    INT16U Sum      : 1;

    INT16U dummy0to7 : 8;
};

struct Communication
{
    INT16U Soft     : 1;
    INT16U PhaseBack    : 1;
    INT16U Block    : 1;
    INT16U Reconstitued : 1;
    INT16U Bk0          : 1;
    INT16U Bk1          : 1;
    INT16U Bk2          : 1;
    INT16U Bk3          : 1;

    INT16U dummy0to7    : 8;
};



/* Medium structure */

struct lca_input
{
    struct Remote enable_cmr;
    struct Local enable_cmd_local;
    struct Digital1 enable_digflt1;
    struct Digital2 enable_digflt2;
    struct Analog enable_analogflt;
    struct External enable_externalflt;
    struct Feedback enable_fb;
    struct Remote cmr;
    struct Local cmd_local;
    struct Digital1 digflt1;
    struct Digital2 digflt2;
    struct Analog analogflt;
    struct External externalflt;
    struct Feedback fb;
    struct Feedback fb_plus;
    struct Feedback fb_minus;
};



struct lca_output
{
    struct Status mugef_status;
    struct Digital1 firstflt_digflt1;
    struct Digital2 firstflt_digflt2;
    struct Analog firstflt_analog;
    struct External firstflt_external;
    struct Digital1 digflt1_led;
    struct Digital2 digflt2_led;
    struct Analog analogflt_led;
    struct External externalflt_led;
    struct Mode mode_led;
    struct Relay cmd_direct_relay;
    struct Relay cmd_controlled_relay;
    struct Externalc cmd_direct_external;
    struct Externalc cmd_controlled_external;
    struct Communication cmd_direct_communication;
    struct Communication cmd_controlled_communication;
};


/** System structure **/

struct tsk_stk
{
    OS_STK      mst[MST_STK_SIZE];      // Millisecond tick task stack
    OS_STK      osb[OSB_STK_SIZE];
    OS_STK      flt[FLT_STK_SIZE];
    OS_STK      sp1[SP1_STK_SIZE];
    OS_STK      fdk[FDK_STK_SIZE];
    OS_STK      cmd[CMD_STK_SIZE];
    OS_STK      pol[POL_STK_SIZE];
    OS_STK              stp[STP_STK_SIZE];
    OS_STK              mde[MDE_STK_SIZE];
    OS_STK              ctn[CTN_STK_SIZE];
    OS_STK              str[STR_STK_SIZE];
    OS_STK              sp2[SP2_STK_SIZE];
    OS_STK              sct[SCT_STK_SIZE];
    OS_STK              sci[SCI_STK_SIZE];
    OS_STK      idl[IDL_STK_SIZE];      // Idle task stack
};


/* Mst Task structure */
struct mst_vars
{
    INT16U      running;            // Millisecond running counter
};


/* Idle Task structure */
struct idl_vars
{
    INT16U      running;            // idle running counter
};

/* Osb Task structure */
struct osb_vars
{
    INT16U      running;            // Task1 running counter
};

/* Stop Task structure */
struct stp_vars
{
    INT16U      running;            // Task2 running counter
};


/* Communication Task structure */
struct  ctn_vars
{
    INT16U      running;            // communication running counter
};

/* Start Task structure */
struct str_vars
{
    INT16U      running;            // Task2 running counter

};

/* Fault Task structure */
struct flt_vars
{
    INT16U      running;            // Task2 running counter
};

/* Feedback Task structure */
struct fdk_vars
{
    INT16U      running;            // fdk running counter
    INT8U               identifier;
};

/* Command Task structure */
struct cmd_vars
{
    INT16U      running;            // cmd running counter
    INT8U               value;
};

/* Pol Task structure */
struct pol_vars
{
    INT16U      running;            // pol running counter
};

/* Mde Task structure */
struct mde_vars
{
    INT16U      running;            // mde running counter
    INT8U               value;
};

/* sci Task structure */

struct Scsr                      // SCI Status Register Structure
{

    INT16U TDRE  : 1;            // Transmit Data Register Empty
    INT16U dummy9to15    : 7;

    INT16U PF    : 1;            // Parity error
    INT16U FE    : 1;            // Framing Error
    INT16U NF    : 1;            // Noise Error
    INT16U OR    : 1;            // Overrun Error
    INT16U IDLE  : 1;            // Idle-Line Detected
    INT16U RAF   : 1;            // Receiver Active
    INT16U RDRF  : 1;            // Receive Data Register Full
    INT16U TC    : 1;            // Transmit Complete
};

KER_EXT struct Scsr * scsr;


struct sci_vars
{
    INT16U      running;            // sci running counter
};

struct sct_vars
{
    INT16U      running;            // sci running counter
};

/*----- Terminal Variables -----*/

struct term_vars
{
    INT16U      line_idx;           // Line editor buffer index
    INT16U      line_end;           // Line editor end of buffer contents index
    INT16U      edit_state;             // Line editor state machine
};

KER_EXT struct term_vars    term;           // Term variables structure

KER_EXT INT8U  stdin  [SRMAX];
KER_EXT INT8U  stdout [STMAX];

KER_EXT INT8U * scdr;




/*----------------------------------------------------------------------------*\
                Definition des variables globales
\*----------------------------------------------------------------------------*/

/** System variables **/

/* Task Variables */
KER_EXT struct tsk_stk  tsk_stk;            // Task Stack Buffers Structure
KER_EXT struct idl_vars idl;                // Idle task variables structure
KER_EXT struct osb_vars osb;                    // Onstb task variables structure
KER_EXT struct flt_vars flt;                    // Emt task variables structure
KER_EXT struct fdk_vars fdk;                    // Fdb task variables structure
KER_EXT struct cmd_vars cmd;                    // Cmd task variables structure
KER_EXT struct stp_vars stp;                    // Stop task variables structure
KER_EXT struct ctn_vars ctn;                    // communication task variables structure
KER_EXT struct str_vars str;                    // Start task variables structure
KER_EXT struct pol_vars pol;                    // pol task variables structure
KER_EXT struct mde_vars mde;                    // pol task variables structure
KER_EXT struct sci_vars sci;                    // sci task variables structure
KER_EXT struct sct_vars sct;                    // sct task variables structure

/* MailBox Variable */
KER_EXT OS_EVENT  *  MboxPolFb;
KER_EXT OS_EVENT  *  MboxModeFb;
KER_EXT OS_EVENT  *  MboxMcbFb;
KER_EXT OS_EVENT  *  MboxOnStb;
KER_EXT OS_EVENT  *  MboxSlaveFb;                       //  *VerP9*
/* Semaphore Variable */
KER_EXT OS_EVENT  *  SemFault;
KER_EXT OS_EVENT  *  SemFeedBack;
KER_EXT OS_EVENT  *  SemTx;
KER_EXT OS_EVENT  *  SemCtn;
/* Message queue variable */
KER_EXT OS_EVENT  *  MsgQRx;
KER_EXT void    *    msg [10];



/** Program variables **/

/*  Retard et temporisation */

KER_EXT struct element         start_delay    [MAXL];
KER_EXT struct element         c_start_delay  [MAXL];
KER_EXT struct element         stop_delay     [MAXL];
KER_EXT struct element         stb_delay      [MAXL];
KER_EXT struct element         spare1_delay   [MAXL];
KER_EXT struct element         polarity_delay [MAXS];
KER_EXT struct element         mode_delay     [MAXS];
KER_EXT struct element         spare2_delay   [MAXS];



/** Configuration **/

KER_EXT unsigned char Polarity;
KER_EXT unsigned char Mode;
KER_EXT unsigned char Master;                     //                 *VerP9*
KER_EXT INT16U  VerP;  // Version Programme uP
KER_EXT INT16U  VerF;  // Version Programme FPGA
KER_EXT INT16U  VerT;  // Version Programme Type

KER_EXT struct State         state ;


/** Registre du FPGA */
KER_EXT struct lca_input   *  in;
KER_EXT struct lca_input     ram_in;              /* For loading LCA registers in  to memory */
KER_EXT struct lca_output  *  out;
KER_EXT struct lca_output    ram_out;             /* For loading LCA registers out to memory */

/** Networks variable ver20 */
KER_EXT INT16U * Ni_data;
KER_EXT INT16U * Ni_ds;
KER_EXT INT8U Flag_Irq_Network;
KER_EXT INT16U Ni_data_word;


/** Variable f **/

KER_EXT unsigned char Flag_polarity;
KER_EXT unsigned char Flag_start;
KER_EXT unsigned char Flag_mode;
KER_EXT unsigned char Flag_timer;
KER_EXT unsigned char Flag_reset;
KER_EXT unsigned char Flag_FirstFault;
KER_EXT char Tmo[10];
KER_EXT unsigned int  pc_conf;                     /* Use for read section type */




KER_EXT int n_buserr, n_spuriousint;


/* globales variables for Timer */

KER_EXT  unsigned int timer1_count, timer2_count, timer3_count, timer4_count, timer5_count ;

/* globales variables for SCI */
KER_EXT INT8U  Data_Serial_Register;
KER_EXT INT8U  Data_Serial_Tx;


/* ---------------------------------------------------------------------------- */
/*              Declaration des fonctions                               */
/* -----------------------------------------------------------------------------*/
void Init_timer();
void Timer1(float delay);
void Timer2(float delay);
void Timer3(float delay);
void Timer4(float delay);
void Timer5(float delay);
void Init_pwm();
void Init_external_interrupt();
void Init_serial_communication_interface();

int  Mcbon();
int  Off();
int  On();
int  Onstb();
int  Fault() ;
INT8U Slave_Incoherence();                // *VerP9*
INT8U Supply_Incoherence();
INT8U Polarity_Incoherence();
INT8U Mode_Incoherence();
void FctSat();
INT16U Status0();
INT16U Status1();
INT16U Status2();
INT16U Status3();
INT16U Status4();
INT16U Status5();
INT16U Status8();
INT16U Status9();
INT16U Status10();
void Lighted_LedFault();
void UnLighted_LedFault();
void FirstFault();
void Switch_Mux(INT16U);

void Config();

void Nvsram();
void Nvsram_Recall();
void MemCpy(long int , long int , INT16U);

void Test();

void    Order(struct element *, int);
BOOLEAN Cmp_element(INT16U);

void   FctStd(INT8S *);
INT8U  FctStb(INT8S *);
void   FctStp(INT8S *);

void TermInitLineEditor(void);
INT16U TermLE0(INT8U ch);
INT16U TermLE1(INT8U ch);
INT16U TermLE2(INT16U ch);
INT16U TermLE3(INT16U ch);
INT16U TermLE4(INT16U ch);
INT16U TermLE5(INT16U ch);
void TermCursorLeft(void);
void TermCursorRight(void);
void TermInsertChar(INT16U ch);
void TermSendCmd(void);
void TermDeleteLeft(void);

void MstTsk(void *);
void IdlTsk(void *);
void PolTsk(void *);
void StpTsk(void *);
void Sp1Tsk(void *);
void MdeTsk(void *);
void CtnTsk(void *);
void StrTsk(void *);
void OsbTsk(void *);
void FltTsk(void *);
void FdkTsk(void *);
void CmdTsk(void *);
void Sp2Tsk(void *);
void TxTsk(void *);
void RxTsk(void *);

/* End of file: kermit.h */



