/*---------------------------------------------------------------------------------------------------------*\
  File:     term.h

  Contents: This file contains some commands and status available from a terminal

  Notes:

  History:

    15/12/2004  af  Created

\*---------------------------------------------------------------------------------------------------------*/

struct command_
{
    INT8U  * s;
    INT8U   cd;
};

KER_EXT struct command_ commandl[]
#ifdef KER_GLOBALS
        = { {"STOP", STOP},
    {"START", START},
    {"RESET", RESET},
    {"STB", STB},
    {"POL+", POLPLUS},
    {"POL-", POLMOINS},
    {"MODE1", MODE1},
    {"MODE2", MODE2},
    {"MODE3", MODE3},
    {"M /0", DCCT1},
    {"M /1", DCCT2},
    {"M /2", IREF},
    {"M /3", BRDGP},
    {"M /4", BRDGM},
    {"M /5", VREF},
    {"M /6", VFB},
    {"M /7", FRREFP},
    {"M /8", FRREFM},
    {"M /9", FRREF},
    {"M /A", ICIRC},
    {"M /B", VCMD},
    {"M /C", ICAPA},
    {"M /D", P5V},
    {"M /E", M15V},
    {"M /F", P15V},
    {"N T1", NETWORK_T1},
    {"N T2", NETWORK_T2}
}
#endif
;

KER_EXT struct command_ commandr[]
#ifdef KER_GLOBALS
        = { {"F", STOP},
    {"N", START},
    {"RS", RESET},
    {"STB", STB}
}
#endif
;

KER_EXT INT8S * status[]
#ifdef KER_GLOBALS
    = {"FIRSTFAULT", "FAULT", "ACTIVEFAULT", "TMO", "TYPE", "VERP", "VERF", "VERT", "CHANNEL", "STATE"}
#endif
      ;

KER_EXT INT8S * state_s[]
#ifdef KER_GLOBALS
    = {"OFF", "ON", "ONSTB", "FAULT", "INCOHERENCE", "BUSY"}
#endif
      ;

KER_EXT INT8S * state_c[]
#ifdef KER_GLOBALS
    = {"LOCAL", "REMOTE"}
#endif
      ;

KER_EXT INT8S * state_p[]
#ifdef KER_GLOBALS
    = {"NO_POLARITY", "PLUS", "MINUS", " ", "INCOHERENCE", "BUSY"}
#endif
      ;

KER_EXT INT8S * state_m[]
#ifdef KER_GLOBALS
    = {"NO_MODE", "MODE_1", "MODE_2", "MODE_3", "INCOHERENCE", "BUSY"}
#endif
      ;

/*---------------------------------------------------------------------------------------------------------*\
  End of file: term.h
\*---------------------------------------------------------------------------------------------------------*/

