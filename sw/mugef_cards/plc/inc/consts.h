/*---------------------------------------------------------------------------------------------------------*\
  File:     consts.h

  Contents: This file contains all the global #define constants for the PLCC&Intlk card

  Notes:

  History:

    31/03/2000  af  Created from dc2.h

\*---------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*\
                Definition des constantes
\*----------------------------------------------------------------------------*/
#define MAXINT  -1

#define MAXL     6
#define MAXS     3

/* Constantes for Timer */
#define oc1_msk      0xF7FF /* to be AND-ed with TMSK1 */
#define oc2_msk      0xEFFF
#define oc3_msk      0xDFFF
#define oc4_msk      0xBFFF
#define oc5_msk      0x7FFF

#define TIMER1         1
#define TIMER2         2
#define TIMER3         3
#define TIMER4         4
#define TIMER5         5
/* */

#define ON       1
#define OFF          0
#define YES          1
#define NO           0
#define TRUE         1
#define FALSE        0
#define ENABLE       1
#define DISABLE      0
#define NOT_USED     -1
#define REG_ENABLE   0xffff
#define REG_DISABLE  0x0000

#define SRMAX         30
#define STMAX         80

/* Peripheral address  */

#define ADDRESS_LCA_INPUT    0xFE800
#define ADDRESS_LCA_OUTPUT   0xFE820
#define ADDRESS_NETWORK_INPUT    0xFE000
#define ADDRESS_NETWORK_DS       0xFE002


#define   CONVERT_FOR_SEC   1000    /* Convert delay in seconde                      */
/* CONVERT_FOR_SEC = 1 sec * interrupt frequency */
#define   DELAY_MAX             65

/* Variables Address  */
#define   VERT          0x71FE
#define   REG_EXT       0x7200
#define   REG_ANALOG    0x7300
#define   REG_FLT1      0x7400
#define   REG_FLT2      0x7600
#define   REG_FB        0x7700
#define   S_START       0x7900
#define   S_STOP        0x7A00
#define   S_POL         0x7B00
#define   S_MODE        0x7C00
#define   S_STB         0x7D00
#define   S_SPARE1      0x7E00
#define   S_SPARE2      0x7F00


/* Remote command value */
#define   NO_CMD        0x0000
#define   START         0x0001
#define   STOP          0x0002
#define   RESET         0x0003
#define   STB           0x0004
#define   POLPLUS       0x0008
#define   POLMOINS      0x0010
#define   MODE1     0x0040
#define   MODE2     0x0020
#define   MODE3     0x0080

#define   STA_BIT0       0x00B0   /* Remote Status Bit */
#define   STA_BIT1       0x00B1
#define   STA_BIT2       0x00B2
#define   STA_BIT3       0x00B3
#define   STA_BIT4       0x00B4
#define   STA_BIT5       0x00B5
#define   STA_BIT6       0x00B6
#define   STA_BIT7       0x00B7
#define   STA_BIT8       0x00B8
#define   STA_BIT9       0x00B9
#define   STA_BIT10      0x00BA
#define   STA_BIT11      0x00BB
#define   STA_BIT12      0x00BC
#define   STA_BIT13      0x00BD
#define   STA_BIT14      0x00BE
#define   STA_BIT15      0x00BF


#define   STA_NB0       0x00C0   /* Remote Satus Number*/
#define   STA_NB1       0x00C1
#define   STA_NB2       0x00C2
#define   STA_NB3       0x00C3
#define   STA_NB4       0x00C4
#define   STA_NB5       0x00C5
#define   STA_NB6       0x00C6
#define   STA_NB7       0x00C7
#define   STA_NB8       0x00C8
#define   STA_NB9       0x00C9
#define   STA_NB10      0x00CA
#define   STA_NB11      0x00CB
#define   STA_NB12      0x00CC
#define   STA_NB13      0x00CD
#define   STA_NB14      0x00CE
#define   STA_NB15      0x00CF

#define   TST_BUS_5     0x0055
#define   TST_BUS_A     0x00AA

#define   DCCT1     0x0030   /* BANK */
#define   DCCT2     0x0031
#define   IREF      0x0032
#define   BRDGP     0x0033
#define   BRDGM     0x0034
#define   VREF      0x0035
#define   VFB       0x0036
#define   FRREFP    0x0037
#define   FRREFM    0x0038
#define   FRREF     0x0039
#define   ICIRC     0x003A
#define   VCMD      0x003B
#define   ICAPA     0x003C
#define   P5V       0x003D
#define   M15V      0x003E
#define   P15V      0x003F

#define   NETWORK_T1    0x00D1
#define   NETWORK_T2    0x00D2

/*----- Supply STATUS -----*/
#define OFF                             0
#define ON                              1
#define ONSTB                           2
#define FAULT                           3
#define INCOHERENCE                     4
#define BUSY                            5

/*----- Polarity STATUS -----*/
#define NO_POLARITY                     0
#define PLUS                            1
#define MINUS                           2
#define INCOHERENCE                     4
#define BUSY                            5

/*----- Mode STATUS    -----*/
#define NO_MODE                         0
#define MODE_1                          1
#define MODE_2                          2
#define MODE_3                          3
#define INCOHERENCE                     4
#define BUSY                            5

/*----- Slave STATUS -----*/                         //                              *VerP9*
#define NO_MASTERSLAVE                  0            //                              *VerP9*
#define SLAVE_ON                        1            //                              *VerP9*
#define SLAVE_OFF                       2            //                              *VerP9*
#define INCOHERENCE                     4            //                              *VerP9*
#define BUSY                            5            //                              *VerP9*

/*----- Control STATUS ------*/
#define LOCAL                           0
#define REMOTE                          1



/*----- Task Priorities - used as Task Identifiers -----*/

#define MST_TSK_PRI         5
#define FLT_TSK_PRI                     6
#define SP1_TSK_PRI                     7
#define FDK_TSK_PRI                     8
#define POL_TSK_PRI                     9
#define STP_TSK_PRI                     10
#define MDE_TSK_PRI                     11
#define CMD_TSK_PRI                     12
#define CTN_TSK_PRI                     13
#define OSB_TSK_PRI                     14
#define STR_TSK_PRI                     15
#define SP2_TSK_PRI                     16
#define SCI_TSK_PRI                     17
#define SCT_TSK_PRI                     18
#define IDL_TSK_PRI         19

/*----- Task Stack sizes (words) -----*/

#define MST_STK_SIZE            128
#define FLT_STK_SIZE                    128
#define SP1_STK_SIZE                    128
#define SP2_STK_SIZE                    128
#define FDK_STK_SIZE                    128
#define POL_STK_SIZE                    128
#define STP_STK_SIZE                    128
#define MDE_STK_SIZE                    128
#define CMD_STK_SIZE                    128
#define CTN_STK_SIZE                    128
#define OSB_STK_SIZE                    128
#define STR_STK_SIZE                    128
#define SCT_STK_SIZE                    128
#define SCI_STK_SIZE                    128
#define IDL_STK_SIZE            64



/*----- String Char definition -----*/

KER_EXT INT8S * tab_stop_delay[]
#ifdef KER_GLOBALS
    = {"TMO_STOP", "SPD_BLOCKED", "SPD_PHASEBACK", "SPD_SOFTSTART", "SPD_RECONSTITUED", "SPD_MCBOFF"}
#endif
      ;

KER_EXT INT8S * tab_start_delay[]
#ifdef KER_GLOBALS
    = {"TMO_START", "STD_BLOCKED", "STD_PHASEBACK", "STD_SOFTSTART", "STD_RECONSTITUED", "STD_MCBON"}
#endif
      ;

KER_EXT INT8S * tab_stb_delay[]
#ifdef KER_GLOBALS
    = {"TMO_STB", "SBD_BLOCKED", "SBD_PHASEBACK", "SBD_SOFTSTART", "SBD_RECONSTITUED", "SBD_ZEROI"}
#endif
      ;

KER_EXT INT8S * tab_sp1_delay[]
#ifdef KER_GLOBALS
    = {"SP1_D1", "SP1_D2", "SP1_D3", "SP1_D4", "SP1_D5", "SP1_D6"}
#endif
      ;

KER_EXT INT8S * tab_pol_delay[]
#ifdef KER_GLOBALS
    = {"TMO_POL", "PLD_WAIT", "PLD_SPARE"}
#endif
      ;

KER_EXT INT8S * tab_mde_delay[]
#ifdef KER_GLOBALS
    = {"TMO_MDE", "MED_WAIT", "MED_SPARE"}
#endif
      ;

KER_EXT INT8S * tab_sp2_delay[]
#ifdef KER_GLOBALS
    = {"SP2_D1", "SP2_D2", "SP2_D3"}
#endif
      ;

KER_EXT INT8S * tab_tmo[]
#ifdef KER_GLOBALS
    = {"NO", "STOP", "START", "ONSTB", "POLARITY", "MODE", "SLAVE"}
#endif
      ;
/*---------------------------------------------------------------------------------------------------------*\
  End of file: consts.h
\*---------------------------------------------------------------------------------------------------------*/

