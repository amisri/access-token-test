/*****************************************************
     ieemath.h - ANSI-C library: IEEE handling
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!
 *****************************************************/

#pragma ONCE

#ifndef _H_IEEEMATH_
#define _H_IEEEMATH_

typedef union {
#if defined(__DOUBLE_IS_IEEE32__)
  unsigned long i;
#else
  unsigned long l[2];
  unsigned int i[4];
#endif
  double f;
} intdouble;

typedef union {
  unsigned long i;
  float f;
} intfloat;

extern const intfloat infinityf;
extern const intdouble infinity;

#ifdef __cplusplus
 }
#endif

#endif
