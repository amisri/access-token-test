/*****************************************************
 hidef.h - ANSI-C library: machine dependent stuff
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved
                  Do not modify!

 15/05/99       qak     HALT and HALT(X) modified to not use SWI
 *****************************************************/

#pragma ONCE

#ifndef _H_HIDEF_
#define _H_HIDEF_


/**** Version for MC68HC16 */

#include <stddef.h>
#include <stdtypes.h>

  #define FAR    far
  #define _FAR   far
  #define NEAR
  #define _NEAR

#endif

/*****************************************************/
/* end hidef.h */
