/*----------------------------------------------------------------------------*\
  File:		hc16.h

  Contents:     This file defines constants to access the HC16 registers.

  History:

    01/06/99	qak	Created from peripherals.h
\*----------------------------------------------------------------------------*/

typedef volatile unsigned char * reg8u;
typedef volatile unsigned int  * reg16u;

/*----------------------------------------------------------------------------*\
  FF700	2.25K		Internal HC16 registers
\*----------------------------------------------------------------------------*/

#define GPT_MCR     *(reg16u)0xFF900   /* GPT module configuration	     */
#define GPT_ICR     *(reg16u)0xFF904   /* GPT interrupt configuration reg.   */
#define GPT_PDDR    *(reg8u )0xFF906   /* Parallel data direction register   */
#define GPT_PDR     *(reg8u )0xFF907   /* Parallel data register	     */
#define GPT_OC1M    *(reg8u )0xFF908   /* OC1 action mask register	     */
#define GPT_OC1D    *(reg8u )0xFF909   /* OC1 action data register	     */
#define GPT_TCNT    *(reg16u)0xFF90A   /* Timer counter register	     */
#define GPT_PACLT   *(reg8u )0xFF90C   /* PA control                         */
#define GPT_PACNT   *(reg8u )0xFF90D   /* PA counter                         */
#define GPT_TIC1    *(reg16u)0xFF90E   /* Input capture 1                    */
#define GPT_TIC2    *(reg16u)0xFF910   /* Input capture 2                    */
#define GPT_TIC3    *(reg16u)0xFF912   /* Input capture 3                    */
#define GPT_TOC1    *(reg16u)0xFF914   /* Output compare 1                   */
#define GPT_TOC2    *(reg16u)0xFF916   /* Output compare 2                   */
#define GPT_TOC3    *(reg16u)0xFF918   /* Output compare 3                   */
#define GPT_TOC4    *(reg16u)0xFF91A   /* Output compare 4                   */
#define GPT_TI4O5   *(reg16u)0xFF91C   /* Input capture 4 /output compare 5  */
#define GPT_TCTL    *(reg16u)0xFF91E   /* Timer control 1 and 2              */
#define GPT_TMSK    *(reg16u)0xFF920   /* Timer mask 1 and 2                 */
#define GPT_TFLG    *(reg16u)0xFF922   /* timer flag 1 and 2                 */
#define GPT_CFORC   *(reg8u )0xFF924   /* Force compare                      */
#define GPT_PWMC    *(reg8u )0xFF925   /* Pwm control c                      */
#define GPT_PWMA    *(reg8u )0xFF926   /* Pwm control a                      */
#define GPT_PWMB    *(reg8u )0xFF927   /* Pwm control b                      */
#define GPT_PWMCNT  *(reg16u)0xFF928   /* Pwm count                          */
#define GPT_PWMBUFA *(reg8u )0xFF92A   /* Pwma buffer                        */
#define GPT_PWMBUFB *(reg8u )0xFF92B   /* Pwmb buffer                        */
#define GPT_PRESCL  *(reg16u)0xFF92C   /* GPT prescaler			     */

#define SIM_MCR     *(reg16u)0xFFA00   /* SIM module configuration	     */
#define SIM_SYNCR   *(reg16u)0xFFA04   /* Clock synthesizer control	     */
#define SIM_RSR     *(reg16u)0xFFA06   /* Reset status register	      	     */
#define SIM_SIMTRE  *(reg16u)0xFFA08   /* System integration test register   */
#define SIM_PORTE0  *(reg16u)0xFFA10   /* Port E0 data register		     */
#define SIM_PORTE1  *(reg16u)0xFFA12   /* Port E1 data register		     */
#define SIM_DDRE    *(reg16u)0xFFA14   /* Port E data direction              */
#define SIM_PEPAR   *(reg16u)0xFFA16   /* Port E pin assignment              */
#define SIM_PORTF0  *(reg16u)0xFFA18   /* Port F0 data register              */
#define SIM_PORTF1  *(reg16u)0xFFA1A   /* Port F1 data register              */
#define SIM_PORTF   *(reg8u )0xFFA19   /* Port F data register (byte)        */
#define SIM_DDRF    *(reg16u)0xFFA1C   /* Port F data direction              */
#define SIM_PFPAR   *(reg16u)0xFFA1E   /* Port F pin assignment              */
#define SIM_SYPCR   *(reg16u)0xFFA20   /* Systeme protection control         */
#define SIM_PICR    *(reg16u)0xFFA22   /* Periodic interrupt control         */
#define SIM_PITR    *(reg16u)0xFFA24   /* Periodic interrupt timing          */
#define SIM_SWSR    *(reg16u)0xFFA26   /* Software service                   */
#define SIM_TSTMSRA *(reg16u)0xFFA30   /* Test module master shift A         */
#define SIM_TSTMSRB *(reg16u)0xFFA32   /* Test module master shift B         */
#define SIM_TSTSC   *(reg16u)0xFFA34   /* Test module shift count            */
#define SIM_TSTRC   *(reg16u)0xFFA36   /* Test module repetition counter     */
#define SIM_CREG    *(reg16u)0xFFA38   /* Test module control                */
#define SIM_DREG    *(reg16u)0xFFA3A   /* Test module distibuted             */
#define SIM_PORTC   *(reg16u)0xFFA40   /* Port C data   		     */
#define SIM_CSPAR0  *(reg16u)0xFFA44   /* Chip select pin assignment 0       */
#define SIM_CSPAR1  *(reg16u)0xFFA46   /* Chip select pin assignment 1       */
#define SIM_CSBARBT *(reg16u)0xFFA48   /* Chip select base boot              */
#define SIM_CSORBT  *(reg16u)0xFFA4A   /* Chip select option boot            */
#define SIM_CSBAR0  *(reg16u)0xFFA4C   /* Chip select base 0                 */
#define SIM_CSOR0   *(reg16u)0xFFA4E   /* Chip select option 0               */
#define SIM_CSBAR1  *(reg16u)0xFFA50   /* Chip select base 1                 */
#define SIM_CSOR1   *(reg16u)0xFFA52   /* Chip select option 1               */
#define SIM_CSBAR2  *(reg16u)0xFFA54   /* Chip select base 2                 */
#define SIM_CSOR2   *(reg16u)0xFFA56   /* Chip select option 2               */
#define SIM_CSBAR3  *(reg16u)0xFFA58   /* Chip select base 3                 */
#define SIM_CSOR3   *(reg16u)0xFFA5A   /* Chip select option 3               */
#define SIM_CSBAR4  *(reg16u)0xFFA5C   /* Chip select base 4                 */
#define SIM_CSOR4   *(reg16u)0xFFA5E   /* Chip select option 4               */
#define SIM_CSBAR5  *(reg16u)0xFFA60   /* Chip select base 5                 */
#define SIM_CSOR5   *(reg16u)0xFFA62   /* Chip select option 5               */
#define SIM_CSBAR6  *(reg16u)0xFFA64   /* Chip select base 6                 */
#define SIM_CSOR6   *(reg16u)0xFFA66   /* Chip select option 6               */
#define SIM_CSBAR7  *(reg16u)0xFFA68   /* Chip select base 7                 */
#define SIM_CSOR7   *(reg16u)0xFFA6A   /* Chip select option 7               */
#define SIM_CSBAR8  *(reg16u)0xFFA6C   /* Chip select base 8                 */
#define SIM_CSOR8   *(reg16u)0xFFA6E   /* Chip select option 8               */
#define SIM_CSBAR9  *(reg16u)0xFFA70   /* Chip select base 9                 */
#define SIM_CSOR9   *(reg16u)0xFFA72   /* Chip select option 9               */
#define SIM_CSBAR10 *(reg16u)0xFFA74   /* Chip select base 10                */
#define SIM_CSOR10  *(reg16u)0xFFA76   /* Chip select option 10              */

#define RAM_MCR     *(reg16u)0xFFB00   /* RAM module configuration	     */
#define RAM_BAH     *(reg16u)0xFFB04   /* Array base address register high   */
#define RAM_BAL     *(reg16u)0xFFB06   /* Array base address register low    */

#define QSM_MCR     *(reg16u)0xFFC00   /* QSM module configuration	     */
#define QSM_QILR    *(reg8u )0xFFC04   /* QSM interrupt level register	     */
#define QSM_QIVR    *(reg8u )0xFFC05   /* QSM interrupt vector register      */
#define QSM_SCCR0   *(reg16u)0xFFC08   /* Sci control 0                      */
#define QSM_SCCR1   *(reg16u)0xFFC0A   /* Sci control 1                      */
#define QSM_SCSR    *(reg16u)0xFFC0C   /* Sci status                         */
#define QSM_SCDR    *(reg16u)0xFFC0E   /* Sci data                           */
#define QSM_PORTQS  *(reg8u )0xFFC15   /* Pqs data                           */
#define QSM_PQSPAR  *(reg8u )0xFFC16   /* Pqs pin assignment                 */
#define QSM_DDRQS   *(reg8u )0xFFC17   /* Pqs data direction                 */
#define QSM_SPCR0   *(reg16u)0xFFC18   /* Spi control 0                      */
#define QSM_SPCR1   *(reg16u)0xFFC1A   /* Spi control 1                      */
#define QSM_SPCR2   *(reg16u)0xFFC1C   /* Spi control 2                      */
#define QSM_SPCR3   *(reg8u )0xFFC1E   /* Spi control 3                      */
#define QSM_SPSR    *(reg8u )0xFFC1F   /* Spi status                         */
#define QSM_RR      ((reg16u)0xFFD00)  /* Receive data RAM		[16] */
#define QSM_TR      ((reg16u)0xFFD20)  /* Transmit data RAM		[16] */
#define QSM_CR      ((reg8u )0xFFD40)  /* Command RAM			[16] */

/*----------------------------------------------------------------------------*\
  End of file: hc16.h
\*----------------------------------------------------------------------------*/

