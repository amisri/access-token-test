/* ADC_HC16.H
This file defines the internal register addresses of the AD-converter module
Version 0.1
*/

/* Module Address Map, Base Addresses, high order nibble is assumed 0xF */

#define ADC_base  0xFF700
#define GPT_base  0xFF900
#define SIM_base  0xFFA00
#define SRAM_base 0xFFB00
#define QSM_base  0xFFC00


/* Analog to Digital Converter */

#define ADCMCR   (* (volatile unsigned int  *) ( 0xFF700 ))
#define ADTEST   (* (volatile unsigned int  *) ( 0xFF702 ))
#define PORTADA  (* (volatile unsigned int  *) ( 0xFF706 ))
#define ADCTL0   (* (volatile unsigned char *) ( 0xFF70B )) /* control 0 */
#define ADCTL1   (* (volatile unsigned char *) ( 0xFF70D )) /* control 1 */
#define ADSTAT   (* (volatile unsigned int  *) ( 0xFF70E )) /* status */
#define ADRJURR0 (* (volatile unsigned int  *) ( 0xFF710 )) /* right-just. unsigned result 0 */
#define ADRJURR1 (* (volatile unsigned int  *) ( 0xFF712 )) /* right-just. unsigned result 1 */
#define ADRJURR2 (* (volatile unsigned int  *) ( 0xFF714 )) /* right-just. unsigned result 2 */
#define ADRJURR3 (* (volatile unsigned int  *) ( 0xFF716 )) /* right-just. unsigned result 3 */
#define ADRJURR4 (* (volatile unsigned int  *) ( 0xFF718 )) /* right-just. unsigned result 4 */
#define ADRJURR5 (* (volatile unsigned int  *) ( 0xFF71A )) /* right-just. unsigned result 5 */
#define ADRJURR6 (* (volatile unsigned int  *) ( 0xFF71C )) /* right-just. unsigned result 6 */
#define ADRJURR7 (* (volatile unsigned int  *) ( 0xFF71E )) /* right-just. unsigned result 7 */
#define ADLJSRR0 (* (volatile unsigned int  *) ( 0xFF720 )) /* left-just. signed result 0 */
#define ADLJSRR1 (* (volatile unsigned int  *) ( 0xFF722 )) /* left-just. signed result 1 */
#define ADLJSRR2 (* (volatile unsigned int  *) ( 0xFF724 )) /* left-just. signed result 2 */
#define ADLJSRR3 (* (volatile unsigned int  *) ( 0xFF726 )) /* left-just. signed result 3 */
#define ADLJSRR4 (* (volatile unsigned int  *) ( 0xFF728 )) /* left-just. signed result 4 */
#define ADLJSRR5 (* (volatile unsigned int  *) ( 0xFF72A )) /* left-just. signed result 5 */
#define ADLJSRR6 (* (volatile unsigned int  *) ( 0xFF72C )) /* left-just. signed result 6 */
#define ADLJSRR7 (* (volatile unsigned int  *) ( 0xFF72E )) /* left-just. signed result 7 */
#define ADLJURR0 (* (volatile unsigned int  *) ( 0xFF730 )) /* left-just. unsigned result 0 */
#define ADLJURR1 (* (volatile unsigned int  *) ( 0xFF732 )) /* left-just. unsigned result 1 */
#define ADLJURR2 (* (volatile unsigned int  *) ( 0xFF734 )) /* left-just. unsigned result 2 */
#define ADLJURR3 (* (volatile unsigned int  *) ( 0xFF736 )) /* left-just. unsigned result 3 */
#define ADLJURR4 (* (volatile unsigned int  *) ( 0xFF738 )) /* left-just. unsigned result 4 */
#define ADLJURR5 (* (volatile unsigned int  *) ( 0xFF73A )) /* left-just. unsigned result 5 */
#define ADLJURR6 (* (volatile unsigned int  *) ( 0xFF73C )) /* left-just. unsigned result 6 */
#define ADLJURR7 (* (volatile unsigned int  *) ( 0xFF73E )) /* left-just. unsigned result 7 */

/* Bit definitions of the ADC Module Configuration Register ADCMCR at FF700 */
#define SET_ADCMCR_STOP   0x8000
#define CLR_ADCMCR_STOP   0x7Fff
#define RESET_ADC_FREEZE      0x9fFF

/* Bit definitions of the AD Control Register 0 ADCTL0 at FF70A*/
#define SET_ADCTL0_RES10 0x80 /* 10 bit resolution */
#define CLR_ADCTL0_RES10 0x7F /* 8 bit resolution */

#define RESET_ADCTL0_PRS    0xE0 /* Reset Prescaler */
#define ADC_SYSTEM_CLOCK_4  0x01 /* System clock/4  */
#define ADC_SYSTEM_CLOCK_6  0x02 /* System clock/6  */
#define ADC_SYSTEM_CLOCK_8  0x03 /* System clock/8  */
#define ADC_SYSTEM_CLOCK_10 0x04 /* System clock/10 */
#define ADC_SYSTEM_CLOCK_12 0x05 /* System clock/12 */
#define ADC_SYSTEM_CLOCK_14 0x06 /* System clock/14 */
#define ADC_SYSTEM_CLOCK_16 0x07 /* System clock/16 */
#define ADC_SYSTEM_CLOCK_18 0x08 /* System clock/18 */
#define ADC_SYSTEM_CLOCK_20 0x09 /* System clock/20 */
#define ADC_SYSTEM_CLOCK_22 0x0A /* System clock/22 */
#define ADC_SYSTEM_CLOCK_24 0x0B /* System clock/24 */
#define ADC_SYSTEM_CLOCK_26 0x0C /* System clock/26 */
#define ADC_SYSTEM_CLOCK_28 0x0D /* System clock/28 */
#define ADC_SYSTEM_CLOCK_30 0x0E /* System clock/30 */
#define ADC_SYSTEM_CLOCK_32 0x0F /* System clock/32 */
#define ADC_SYSTEM_CLOCK_34 0x10 /* System clock/34 */
#define ADC_SYSTEM_CLOCK_36 0x11 /* System clock/36 */
#define ADC_SYSTEM_CLOCK_38 0x12 /* System clock/38 */
#define ADC_SYSTEM_CLOCK_40 0x13 /* System clock/40 */
#define ADC_SYSTEM_CLOCK_42 0x14 /* System clock/42 */
#define ADC_SYSTEM_CLOCK_44 0x15 /* System clock/44 */
#define ADC_SYSTEM_CLOCK_46 0x16 /* System clock/46 */
#define ADC_SYSTEM_CLOCK_48 0x17 /* System clock/48 */
#define ADC_SYSTEM_CLOCK_50 0x18 /* System clock/50 */
#define ADC_SYSTEM_CLOCK_52 0x19 /* System clock/52 */
#define ADC_SYSTEM_CLOCK_54 0x1A /* System clock/54 */
#define ADC_SYSTEM_CLOCK_56 0x1B /* System clock/56 */
#define ADC_SYSTEM_CLOCK_58 0x1C /* System clock/58 */
#define ADC_SYSTEM_CLOCK_60 0x1D /* System clock/60 */
#define ADC_SYSTEM_CLOCK_62 0x1E /* System clock/62 */
#define ADC_SYSTEM_CLOCK_64 0x1F /* System clock/64 */

#define RESET_ADCTL0_STS    0x9F /* sample time is 2 clock periods */
#define ADCTL0_STS_4clk     0x20 /* sample time is 4 clock periods */
#define ADCTL0_STS_8clk     0x40 /* sample time is 8 clock periods */
#define ADCTL0_STS_16clk    0x60 /* sample time is 16 clock periods */

/* Bit definitions of the AD Control Register 1 ADCTL1 at FF70C*/
#define SET_S8CM            0x10 /* select 8 conversion sequence mode  */
#define CLR_S8CM            0xEF /* select 4 conversion sequence mode  */
#define SET_MULT            0x20 /* multiple channel */
#define CLR_MULT            0xDF /* single channel */
#define SET_SCAN            0x40 /* continous conversion */
#define CLR_SCAN            0xBF /* single conversion sequence */
#define RESET_CD_CA         0xF0
#define CLR_CA              0xFE
#define SET_CA              0x01
#define CLR_CB              0xFD
#define SET_CB              0x02
#define CLR_CC              0xFB
#define SET_CC              0x04
#define CLR_CD              0xF7
#define SET_CD              0x08
#define SET_VRH             0x0C
#define SET_VRL             0x0D
#define SET_AN0             0x00 /* not important */
#define SET_AN1             0x01
#define SET_AN2             0x02
#define SET_AN3             0x03
#define SET_AN4             0x04
#define SET_AN5             0x05
#define SET_AN6             0x06
#define SET_AN7             0x07



/* Bit definitions of the ADC Status Register at FF70E*/
#define ADSTAT_SCF      0x8000
#define ADSTAT_CCF0     0x0001
#define ADSTAT_CCF1     0x0002
#define ADSTAT_CCF2     0x0004
#define ADSTAT_CCF3     0x0008
#define ADSTAT_CCF4     0x0010
#define ADSTAT_CCF5     0x0020
#define ADSTAT_CCF6     0x0040
#define ADSTAT_CCF7     0x0080
