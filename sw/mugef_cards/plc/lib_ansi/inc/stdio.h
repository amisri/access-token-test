/*****************************************************
    stdio.h - ANSI-C library: standard I/O
 ----------------------------------------------------
   Copyright (c) HIWARE AG, Basel, Switzerland
               All rights reserved

   Notes: The non re-entrant compatibility functions
   	  sprintf() and vsprintf() are limited to
	  255 character buffers.

   qak   15/06/99    Modifed to match new printf.c
 *****************************************************/

#pragma ONCE

#ifndef _H_STDIO_
#define _H_STDIO_

#include <stdarg.h>

/********************************* CONSTANTS *******************************/

#define EOF		(-1)
#define	NL		0x01		/* FILE flags */
#define	INHIBIT		0x02		/* FILE flags */
#define NULL		((void*)0)

/**************************** CALLBACK STRUCTURE ***************************/

typedef void (*fputc_cb)(char c, struct chnl *f);

typedef struct chnl {
    unsigned char	flags;	/* Stream destination */
    unsigned char	idx;	/* Next free space in buffer */
    fputc_cb		cb;	/* Callback for character output */
    char		*buf;   /* Start of output buffer */
} FILE;

/**************************** PRINTF FUNCTIONS  ****************************/

#define fputc(c,f)	if(f)f->cb(c,f)
#define fputca(c,f)	f->cb(c,f)

extern int  fputs	(char *s, FILE *f);
extern int  fprintf	(FILE *f, char *format, ...);
extern int  vfprintf	(FILE *f, char *format, va_list args);
extern int  sprintf	(char *s, char *format, ...);
extern int  vsprintf	(char *s, char *format, va_list args);

/***************************** SCANF FUNCTIONS *****************************/

extern int  sscanf	(const char *s, const char *format, ...);
extern int  vsscanf	(const char *ps, const char *format, va_list args);

#endif

/* end of file: stdio.h */
